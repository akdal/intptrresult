	.text
	.file	"tables.bc"
	.type	hs,@object              # @hs
	.data
	.globl	hs
	.p2align	2
hs:
	.long	64                      # 0x40
	.size	hs, 4

	.type	t1HB,@object            # @t1HB
	.p2align	4
t1HB:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.size	t1HB, 144

	.type	t1l,@object             # @t1l
	.p2align	4
t1l:
	.ascii	"\001\003\000\000\000\000\000\000\000\000\000\000\000\000\000\000\002\003"
	.size	t1l, 18

	.type	t2HB,@object            # @t2HB
	.p2align	4
t2HB:
	.quad	1                       # 0x1
	.quad	2                       # 0x2
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	3                       # 0x3
	.quad	1                       # 0x1
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	3                       # 0x3
	.quad	2                       # 0x2
	.quad	0                       # 0x0
	.size	t2HB, 280

	.type	t2l,@object             # @t2l
	.p2align	4
t2l:
	.ascii	"\001\003\006\000\000\000\000\000\000\000\000\000\000\000\000\000\003\003\005\000\000\000\000\000\000\000\000\000\000\000\000\000\005\005\006"
	.size	t2l, 35

	.type	t3HB,@object            # @t3HB
	.p2align	4
t3HB:
	.quad	3                       # 0x3
	.quad	2                       # 0x2
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	1                       # 0x1
	.quad	1                       # 0x1
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	3                       # 0x3
	.quad	2                       # 0x2
	.quad	0                       # 0x0
	.size	t3HB, 280

	.type	t3l,@object             # @t3l
	.p2align	4
t3l:
	.ascii	"\002\002\006\000\000\000\000\000\000\000\000\000\000\000\000\000\003\002\005\000\000\000\000\000\000\000\000\000\000\000\000\000\005\005\006"
	.size	t3l, 35

	.type	t5HB,@object            # @t5HB
	.p2align	4
t5HB:
	.quad	1                       # 0x1
	.quad	2                       # 0x2
	.quad	6                       # 0x6
	.quad	5                       # 0x5
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	3                       # 0x3
	.quad	1                       # 0x1
	.quad	4                       # 0x4
	.quad	4                       # 0x4
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	7                       # 0x7
	.quad	5                       # 0x5
	.quad	7                       # 0x7
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	6                       # 0x6
	.quad	1                       # 0x1
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.size	t5HB, 416

	.type	t5l,@object             # @t5l
	.p2align	4
t5l:
	.ascii	"\001\003\006\007\000\000\000\000\000\000\000\000\000\000\000\000\003\003\006\007\000\000\000\000\000\000\000\000\000\000\000\000\006\006\007\b\000\000\000\000\000\000\000\000\000\000\000\000\007\006\007\b"
	.size	t5l, 52

	.type	t6HB,@object            # @t6HB
	.p2align	4
t6HB:
	.quad	7                       # 0x7
	.quad	3                       # 0x3
	.quad	5                       # 0x5
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	6                       # 0x6
	.quad	2                       # 0x2
	.quad	3                       # 0x3
	.quad	2                       # 0x2
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	5                       # 0x5
	.quad	4                       # 0x4
	.quad	4                       # 0x4
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	3                       # 0x3
	.quad	3                       # 0x3
	.quad	2                       # 0x2
	.quad	0                       # 0x0
	.size	t6HB, 416

	.type	t6l,@object             # @t6l
	.p2align	4
t6l:
	.ascii	"\003\003\005\007\000\000\000\000\000\000\000\000\000\000\000\000\003\002\004\005\000\000\000\000\000\000\000\000\000\000\000\000\004\004\005\006\000\000\000\000\000\000\000\000\000\000\000\000\006\005\006\007"
	.size	t6l, 52

	.type	t7HB,@object            # @t7HB
	.p2align	4
t7HB:
	.quad	1                       # 0x1
	.quad	2                       # 0x2
	.quad	10                      # 0xa
	.quad	19                      # 0x13
	.quad	16                      # 0x10
	.quad	10                      # 0xa
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	3                       # 0x3
	.quad	3                       # 0x3
	.quad	7                       # 0x7
	.quad	10                      # 0xa
	.quad	5                       # 0x5
	.quad	3                       # 0x3
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	11                      # 0xb
	.quad	4                       # 0x4
	.quad	13                      # 0xd
	.quad	17                      # 0x11
	.quad	8                       # 0x8
	.quad	4                       # 0x4
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	12                      # 0xc
	.quad	11                      # 0xb
	.quad	18                      # 0x12
	.quad	15                      # 0xf
	.quad	11                      # 0xb
	.quad	2                       # 0x2
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	7                       # 0x7
	.quad	6                       # 0x6
	.quad	9                       # 0x9
	.quad	14                      # 0xe
	.quad	3                       # 0x3
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	6                       # 0x6
	.quad	4                       # 0x4
	.quad	5                       # 0x5
	.quad	3                       # 0x3
	.quad	2                       # 0x2
	.quad	0                       # 0x0
	.size	t7HB, 688

	.type	t7l,@object             # @t7l
	.p2align	4
t7l:
	.ascii	"\001\003\006\b\b\t\000\000\000\000\000\000\000\000\000\000\003\004\006\007\007\b\000\000\000\000\000\000\000\000\000\000\006\005\007\b\b\t\000\000\000\000\000\000\000\000\000\000\007\007\b\t\t\t\000\000\000\000\000\000\000\000\000\000\007\007\b\t\t\n\000\000\000\000\000\000\000\000\000\000\b\b\t\n\n\n"
	.size	t7l, 86

	.type	t8HB,@object            # @t8HB
	.p2align	4
t8HB:
	.quad	3                       # 0x3
	.quad	4                       # 0x4
	.quad	6                       # 0x6
	.quad	18                      # 0x12
	.quad	12                      # 0xc
	.quad	5                       # 0x5
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	5                       # 0x5
	.quad	1                       # 0x1
	.quad	2                       # 0x2
	.quad	16                      # 0x10
	.quad	9                       # 0x9
	.quad	3                       # 0x3
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	7                       # 0x7
	.quad	3                       # 0x3
	.quad	5                       # 0x5
	.quad	14                      # 0xe
	.quad	7                       # 0x7
	.quad	3                       # 0x3
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	19                      # 0x13
	.quad	17                      # 0x11
	.quad	15                      # 0xf
	.quad	13                      # 0xd
	.quad	10                      # 0xa
	.quad	4                       # 0x4
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	13                      # 0xd
	.quad	5                       # 0x5
	.quad	8                       # 0x8
	.quad	11                      # 0xb
	.quad	5                       # 0x5
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	12                      # 0xc
	.quad	4                       # 0x4
	.quad	4                       # 0x4
	.quad	1                       # 0x1
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.size	t8HB, 688

	.type	t8l,@object             # @t8l
	.p2align	4
t8l:
	.ascii	"\002\003\006\b\b\t\000\000\000\000\000\000\000\000\000\000\003\002\004\b\b\b\000\000\000\000\000\000\000\000\000\000\006\004\006\b\b\t\000\000\000\000\000\000\000\000\000\000\b\b\b\t\t\n\000\000\000\000\000\000\000\000\000\000\b\007\b\t\n\n\000\000\000\000\000\000\000\000\000\000\t\b\t\t\013\013"
	.size	t8l, 86

	.type	t9HB,@object            # @t9HB
	.p2align	4
t9HB:
	.quad	7                       # 0x7
	.quad	5                       # 0x5
	.quad	9                       # 0x9
	.quad	14                      # 0xe
	.quad	15                      # 0xf
	.quad	7                       # 0x7
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	6                       # 0x6
	.quad	4                       # 0x4
	.quad	5                       # 0x5
	.quad	5                       # 0x5
	.quad	6                       # 0x6
	.quad	7                       # 0x7
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	7                       # 0x7
	.quad	6                       # 0x6
	.quad	8                       # 0x8
	.quad	8                       # 0x8
	.quad	8                       # 0x8
	.quad	5                       # 0x5
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	15                      # 0xf
	.quad	6                       # 0x6
	.quad	9                       # 0x9
	.quad	10                      # 0xa
	.quad	5                       # 0x5
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	11                      # 0xb
	.quad	7                       # 0x7
	.quad	9                       # 0x9
	.quad	6                       # 0x6
	.quad	4                       # 0x4
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	14                      # 0xe
	.quad	4                       # 0x4
	.quad	6                       # 0x6
	.quad	2                       # 0x2
	.quad	6                       # 0x6
	.quad	0                       # 0x0
	.size	t9HB, 688

	.type	t9l,@object             # @t9l
	.p2align	4
t9l:
	.ascii	"\003\003\005\006\b\t\000\000\000\000\000\000\000\000\000\000\003\003\004\005\006\b\000\000\000\000\000\000\000\000\000\000\004\004\005\006\007\b\000\000\000\000\000\000\000\000\000\000\006\005\006\007\007\b\000\000\000\000\000\000\000\000\000\000\007\006\007\007\b\t\000\000\000\000\000\000\000\000\000\000\b\007\b\b\t\t"
	.size	t9l, 86

	.type	t10HB,@object           # @t10HB
	.p2align	4
t10HB:
	.quad	1                       # 0x1
	.quad	2                       # 0x2
	.quad	10                      # 0xa
	.quad	23                      # 0x17
	.quad	35                      # 0x23
	.quad	30                      # 0x1e
	.quad	12                      # 0xc
	.quad	17                      # 0x11
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	3                       # 0x3
	.quad	3                       # 0x3
	.quad	8                       # 0x8
	.quad	12                      # 0xc
	.quad	18                      # 0x12
	.quad	21                      # 0x15
	.quad	12                      # 0xc
	.quad	7                       # 0x7
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	11                      # 0xb
	.quad	9                       # 0x9
	.quad	15                      # 0xf
	.quad	21                      # 0x15
	.quad	32                      # 0x20
	.quad	40                      # 0x28
	.quad	19                      # 0x13
	.quad	6                       # 0x6
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	14                      # 0xe
	.quad	13                      # 0xd
	.quad	22                      # 0x16
	.quad	34                      # 0x22
	.quad	46                      # 0x2e
	.quad	23                      # 0x17
	.quad	18                      # 0x12
	.quad	7                       # 0x7
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	20                      # 0x14
	.quad	19                      # 0x13
	.quad	33                      # 0x21
	.quad	47                      # 0x2f
	.quad	27                      # 0x1b
	.quad	22                      # 0x16
	.quad	9                       # 0x9
	.quad	3                       # 0x3
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	31                      # 0x1f
	.quad	22                      # 0x16
	.quad	41                      # 0x29
	.quad	26                      # 0x1a
	.quad	21                      # 0x15
	.quad	20                      # 0x14
	.quad	5                       # 0x5
	.quad	3                       # 0x3
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	14                      # 0xe
	.quad	13                      # 0xd
	.quad	10                      # 0xa
	.quad	11                      # 0xb
	.quad	16                      # 0x10
	.quad	6                       # 0x6
	.quad	5                       # 0x5
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	9                       # 0x9
	.quad	8                       # 0x8
	.quad	7                       # 0x7
	.quad	8                       # 0x8
	.quad	4                       # 0x4
	.quad	4                       # 0x4
	.quad	2                       # 0x2
	.quad	0                       # 0x0
	.size	t10HB, 960

	.type	t10l,@object            # @t10l
	.p2align	4
t10l:
	.ascii	"\001\003\006\b\t\t\t\n\000\000\000\000\000\000\000\000\003\004\006\007\b\t\b\b\000\000\000\000\000\000\000\000\006\006\007\b\t\n\t\t\000\000\000\000\000\000\000\000\007\007\b\t\n\n\t\n\000\000\000\000\000\000\000\000\b\b\t\n\n\n\n\n\000\000\000\000\000\000\000\000\t\t\n\n\013\013\n\013\000\000\000\000\000\000\000\000\b\b\t\n\n\n\013\013\000\000\000\000\000\000\000\000\t\b\t\n\n\013\013\013"
	.size	t10l, 120

	.type	t11HB,@object           # @t11HB
	.p2align	4
t11HB:
	.quad	3                       # 0x3
	.quad	4                       # 0x4
	.quad	10                      # 0xa
	.quad	24                      # 0x18
	.quad	34                      # 0x22
	.quad	33                      # 0x21
	.quad	21                      # 0x15
	.quad	15                      # 0xf
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	5                       # 0x5
	.quad	3                       # 0x3
	.quad	4                       # 0x4
	.quad	10                      # 0xa
	.quad	32                      # 0x20
	.quad	17                      # 0x11
	.quad	11                      # 0xb
	.quad	10                      # 0xa
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	11                      # 0xb
	.quad	7                       # 0x7
	.quad	13                      # 0xd
	.quad	18                      # 0x12
	.quad	30                      # 0x1e
	.quad	31                      # 0x1f
	.quad	20                      # 0x14
	.quad	5                       # 0x5
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	25                      # 0x19
	.quad	11                      # 0xb
	.quad	19                      # 0x13
	.quad	59                      # 0x3b
	.quad	27                      # 0x1b
	.quad	18                      # 0x12
	.quad	12                      # 0xc
	.quad	5                       # 0x5
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	35                      # 0x23
	.quad	33                      # 0x21
	.quad	31                      # 0x1f
	.quad	58                      # 0x3a
	.quad	30                      # 0x1e
	.quad	16                      # 0x10
	.quad	7                       # 0x7
	.quad	5                       # 0x5
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	28                      # 0x1c
	.quad	26                      # 0x1a
	.quad	32                      # 0x20
	.quad	19                      # 0x13
	.quad	17                      # 0x11
	.quad	15                      # 0xf
	.quad	8                       # 0x8
	.quad	14                      # 0xe
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	14                      # 0xe
	.quad	12                      # 0xc
	.quad	9                       # 0x9
	.quad	13                      # 0xd
	.quad	14                      # 0xe
	.quad	9                       # 0x9
	.quad	4                       # 0x4
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	11                      # 0xb
	.quad	4                       # 0x4
	.quad	6                       # 0x6
	.quad	6                       # 0x6
	.quad	6                       # 0x6
	.quad	3                       # 0x3
	.quad	2                       # 0x2
	.quad	0                       # 0x0
	.size	t11HB, 960

	.type	t11l,@object            # @t11l
	.p2align	4
t11l:
	.ascii	"\002\003\005\007\b\t\b\t\000\000\000\000\000\000\000\000\003\003\004\006\b\b\007\b\000\000\000\000\000\000\000\000\005\005\006\007\b\t\b\b\000\000\000\000\000\000\000\000\007\006\007\t\b\n\b\t\000\000\000\000\000\000\000\000\b\b\b\t\t\n\t\n\000\000\000\000\000\000\000\000\b\b\t\n\n\013\n\013\000\000\000\000\000\000\000\000\b\007\007\b\t\n\n\n\000\000\000\000\000\000\000\000\b\007\b\t\n\n\n\n"
	.size	t11l, 120

	.type	t12HB,@object           # @t12HB
	.p2align	4
t12HB:
	.quad	9                       # 0x9
	.quad	6                       # 0x6
	.quad	16                      # 0x10
	.quad	33                      # 0x21
	.quad	41                      # 0x29
	.quad	39                      # 0x27
	.quad	38                      # 0x26
	.quad	26                      # 0x1a
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	7                       # 0x7
	.quad	5                       # 0x5
	.quad	6                       # 0x6
	.quad	9                       # 0x9
	.quad	23                      # 0x17
	.quad	16                      # 0x10
	.quad	26                      # 0x1a
	.quad	11                      # 0xb
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	17                      # 0x11
	.quad	7                       # 0x7
	.quad	11                      # 0xb
	.quad	14                      # 0xe
	.quad	21                      # 0x15
	.quad	30                      # 0x1e
	.quad	10                      # 0xa
	.quad	7                       # 0x7
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	17                      # 0x11
	.quad	10                      # 0xa
	.quad	15                      # 0xf
	.quad	12                      # 0xc
	.quad	18                      # 0x12
	.quad	28                      # 0x1c
	.quad	14                      # 0xe
	.quad	5                       # 0x5
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	32                      # 0x20
	.quad	13                      # 0xd
	.quad	22                      # 0x16
	.quad	19                      # 0x13
	.quad	18                      # 0x12
	.quad	16                      # 0x10
	.quad	9                       # 0x9
	.quad	5                       # 0x5
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	40                      # 0x28
	.quad	17                      # 0x11
	.quad	31                      # 0x1f
	.quad	29                      # 0x1d
	.quad	17                      # 0x11
	.quad	13                      # 0xd
	.quad	4                       # 0x4
	.quad	2                       # 0x2
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	27                      # 0x1b
	.quad	12                      # 0xc
	.quad	11                      # 0xb
	.quad	15                      # 0xf
	.quad	10                      # 0xa
	.quad	7                       # 0x7
	.quad	4                       # 0x4
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	27                      # 0x1b
	.quad	12                      # 0xc
	.quad	8                       # 0x8
	.quad	12                      # 0xc
	.quad	6                       # 0x6
	.quad	3                       # 0x3
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.size	t12HB, 960

	.type	t12l,@object            # @t12l
	.p2align	4
t12l:
	.ascii	"\004\003\005\007\b\t\t\t\000\000\000\000\000\000\000\000\003\003\004\005\007\007\b\b\000\000\000\000\000\000\000\000\005\004\005\006\007\b\007\b\000\000\000\000\000\000\000\000\006\005\006\006\007\b\b\b\000\000\000\000\000\000\000\000\007\006\007\007\b\b\b\t\000\000\000\000\000\000\000\000\b\007\b\b\b\t\b\t\000\000\000\000\000\000\000\000\b\007\007\b\b\t\t\n\000\000\000\000\000\000\000\000\t\b\b\t\t\t\t\n"
	.size	t12l, 120

	.type	t13HB,@object           # @t13HB
	.p2align	4
t13HB:
	.quad	1                       # 0x1
	.quad	5                       # 0x5
	.quad	14                      # 0xe
	.quad	21                      # 0x15
	.quad	34                      # 0x22
	.quad	51                      # 0x33
	.quad	46                      # 0x2e
	.quad	71                      # 0x47
	.quad	42                      # 0x2a
	.quad	52                      # 0x34
	.quad	68                      # 0x44
	.quad	52                      # 0x34
	.quad	67                      # 0x43
	.quad	44                      # 0x2c
	.quad	43                      # 0x2b
	.quad	19                      # 0x13
	.quad	3                       # 0x3
	.quad	4                       # 0x4
	.quad	12                      # 0xc
	.quad	19                      # 0x13
	.quad	31                      # 0x1f
	.quad	26                      # 0x1a
	.quad	44                      # 0x2c
	.quad	33                      # 0x21
	.quad	31                      # 0x1f
	.quad	24                      # 0x18
	.quad	32                      # 0x20
	.quad	24                      # 0x18
	.quad	31                      # 0x1f
	.quad	35                      # 0x23
	.quad	22                      # 0x16
	.quad	14                      # 0xe
	.quad	15                      # 0xf
	.quad	13                      # 0xd
	.quad	23                      # 0x17
	.quad	36                      # 0x24
	.quad	59                      # 0x3b
	.quad	49                      # 0x31
	.quad	77                      # 0x4d
	.quad	65                      # 0x41
	.quad	29                      # 0x1d
	.quad	40                      # 0x28
	.quad	30                      # 0x1e
	.quad	40                      # 0x28
	.quad	27                      # 0x1b
	.quad	33                      # 0x21
	.quad	42                      # 0x2a
	.quad	16                      # 0x10
	.quad	22                      # 0x16
	.quad	20                      # 0x14
	.quad	37                      # 0x25
	.quad	61                      # 0x3d
	.quad	56                      # 0x38
	.quad	79                      # 0x4f
	.quad	73                      # 0x49
	.quad	64                      # 0x40
	.quad	43                      # 0x2b
	.quad	76                      # 0x4c
	.quad	56                      # 0x38
	.quad	37                      # 0x25
	.quad	26                      # 0x1a
	.quad	31                      # 0x1f
	.quad	25                      # 0x19
	.quad	14                      # 0xe
	.quad	35                      # 0x23
	.quad	16                      # 0x10
	.quad	60                      # 0x3c
	.quad	57                      # 0x39
	.quad	97                      # 0x61
	.quad	75                      # 0x4b
	.quad	114                     # 0x72
	.quad	91                      # 0x5b
	.quad	54                      # 0x36
	.quad	73                      # 0x49
	.quad	55                      # 0x37
	.quad	41                      # 0x29
	.quad	48                      # 0x30
	.quad	53                      # 0x35
	.quad	23                      # 0x17
	.quad	24                      # 0x18
	.quad	58                      # 0x3a
	.quad	27                      # 0x1b
	.quad	50                      # 0x32
	.quad	96                      # 0x60
	.quad	76                      # 0x4c
	.quad	70                      # 0x46
	.quad	93                      # 0x5d
	.quad	84                      # 0x54
	.quad	77                      # 0x4d
	.quad	58                      # 0x3a
	.quad	79                      # 0x4f
	.quad	29                      # 0x1d
	.quad	74                      # 0x4a
	.quad	49                      # 0x31
	.quad	41                      # 0x29
	.quad	17                      # 0x11
	.quad	47                      # 0x2f
	.quad	45                      # 0x2d
	.quad	78                      # 0x4e
	.quad	74                      # 0x4a
	.quad	115                     # 0x73
	.quad	94                      # 0x5e
	.quad	90                      # 0x5a
	.quad	79                      # 0x4f
	.quad	69                      # 0x45
	.quad	83                      # 0x53
	.quad	71                      # 0x47
	.quad	50                      # 0x32
	.quad	59                      # 0x3b
	.quad	38                      # 0x26
	.quad	36                      # 0x24
	.quad	15                      # 0xf
	.quad	72                      # 0x48
	.quad	34                      # 0x22
	.quad	56                      # 0x38
	.quad	95                      # 0x5f
	.quad	92                      # 0x5c
	.quad	85                      # 0x55
	.quad	91                      # 0x5b
	.quad	90                      # 0x5a
	.quad	86                      # 0x56
	.quad	73                      # 0x49
	.quad	77                      # 0x4d
	.quad	65                      # 0x41
	.quad	51                      # 0x33
	.quad	44                      # 0x2c
	.quad	43                      # 0x2b
	.quad	42                      # 0x2a
	.quad	43                      # 0x2b
	.quad	20                      # 0x14
	.quad	30                      # 0x1e
	.quad	44                      # 0x2c
	.quad	55                      # 0x37
	.quad	78                      # 0x4e
	.quad	72                      # 0x48
	.quad	87                      # 0x57
	.quad	78                      # 0x4e
	.quad	61                      # 0x3d
	.quad	46                      # 0x2e
	.quad	54                      # 0x36
	.quad	37                      # 0x25
	.quad	30                      # 0x1e
	.quad	20                      # 0x14
	.quad	16                      # 0x10
	.quad	53                      # 0x35
	.quad	25                      # 0x19
	.quad	41                      # 0x29
	.quad	37                      # 0x25
	.quad	44                      # 0x2c
	.quad	59                      # 0x3b
	.quad	54                      # 0x36
	.quad	81                      # 0x51
	.quad	66                      # 0x42
	.quad	76                      # 0x4c
	.quad	57                      # 0x39
	.quad	54                      # 0x36
	.quad	37                      # 0x25
	.quad	18                      # 0x12
	.quad	39                      # 0x27
	.quad	11                      # 0xb
	.quad	35                      # 0x23
	.quad	33                      # 0x21
	.quad	31                      # 0x1f
	.quad	57                      # 0x39
	.quad	42                      # 0x2a
	.quad	82                      # 0x52
	.quad	72                      # 0x48
	.quad	80                      # 0x50
	.quad	47                      # 0x2f
	.quad	58                      # 0x3a
	.quad	55                      # 0x37
	.quad	21                      # 0x15
	.quad	22                      # 0x16
	.quad	26                      # 0x1a
	.quad	38                      # 0x26
	.quad	22                      # 0x16
	.quad	53                      # 0x35
	.quad	25                      # 0x19
	.quad	23                      # 0x17
	.quad	38                      # 0x26
	.quad	70                      # 0x46
	.quad	60                      # 0x3c
	.quad	51                      # 0x33
	.quad	36                      # 0x24
	.quad	55                      # 0x37
	.quad	26                      # 0x1a
	.quad	34                      # 0x22
	.quad	23                      # 0x17
	.quad	27                      # 0x1b
	.quad	14                      # 0xe
	.quad	9                       # 0x9
	.quad	7                       # 0x7
	.quad	34                      # 0x22
	.quad	32                      # 0x20
	.quad	28                      # 0x1c
	.quad	39                      # 0x27
	.quad	49                      # 0x31
	.quad	75                      # 0x4b
	.quad	30                      # 0x1e
	.quad	52                      # 0x34
	.quad	48                      # 0x30
	.quad	40                      # 0x28
	.quad	52                      # 0x34
	.quad	28                      # 0x1c
	.quad	18                      # 0x12
	.quad	17                      # 0x11
	.quad	9                       # 0x9
	.quad	5                       # 0x5
	.quad	45                      # 0x2d
	.quad	21                      # 0x15
	.quad	34                      # 0x22
	.quad	64                      # 0x40
	.quad	56                      # 0x38
	.quad	50                      # 0x32
	.quad	49                      # 0x31
	.quad	45                      # 0x2d
	.quad	31                      # 0x1f
	.quad	19                      # 0x13
	.quad	12                      # 0xc
	.quad	15                      # 0xf
	.quad	10                      # 0xa
	.quad	7                       # 0x7
	.quad	6                       # 0x6
	.quad	3                       # 0x3
	.quad	48                      # 0x30
	.quad	23                      # 0x17
	.quad	20                      # 0x14
	.quad	39                      # 0x27
	.quad	36                      # 0x24
	.quad	35                      # 0x23
	.quad	53                      # 0x35
	.quad	21                      # 0x15
	.quad	16                      # 0x10
	.quad	23                      # 0x17
	.quad	13                      # 0xd
	.quad	10                      # 0xa
	.quad	6                       # 0x6
	.quad	1                       # 0x1
	.quad	4                       # 0x4
	.quad	2                       # 0x2
	.quad	16                      # 0x10
	.quad	15                      # 0xf
	.quad	17                      # 0x11
	.quad	27                      # 0x1b
	.quad	25                      # 0x19
	.quad	20                      # 0x14
	.quad	29                      # 0x1d
	.quad	11                      # 0xb
	.quad	17                      # 0x11
	.quad	12                      # 0xc
	.quad	16                      # 0x10
	.quad	8                       # 0x8
	.quad	1                       # 0x1
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.quad	1                       # 0x1
	.size	t13HB, 2048

	.type	t13l,@object            # @t13l
	.p2align	4
t13l:
	.ascii	"\001\004\006\007\b\t\t\n\t\n\013\013\f\f\r\r\003\004\006\007\b\b\t\t\t\t\n\n\013\f\f\f\006\006\007\b\t\t\n\n\t\n\n\013\013\f\r\r\007\007\b\t\t\n\n\n\n\013\013\013\013\f\r\r\b\007\t\t\n\n\013\013\n\013\013\f\f\r\r\016\t\b\t\n\n\n\013\013\013\013\f\013\r\r\016\016\t\t\n\n\013\013\013\013\013\f\f\f\r\r\016\016\n\t\n\013\013\013\f\f\f\f\r\r\r\016\020\020\t\b\t\n\n\013\013\f\f\f\f\r\r\016\017\017\n\t\n\n\013\013\013\r\f\r\r\016\016\016\020\017\n\n\n\013\013\f\f\r\f\r\016\r\016\017\020\021\013\n\n\013\f\f\f\f\r\r\r\016\017\017\017\020\013\013\013\f\f\r\f\r\016\016\017\017\017\020\020\020\f\013\f\r\r\r\016\016\016\016\016\017\020\017\020\020\r\f\f\r\r\r\017\016\016\021\017\017\017\021\020\020\f\f\r\016\016\016\017\016\017\017\020\020\023\022\023\020"
	.size	t13l, 256

	.type	t15HB,@object           # @t15HB
	.p2align	4
t15HB:
	.quad	7                       # 0x7
	.quad	12                      # 0xc
	.quad	18                      # 0x12
	.quad	53                      # 0x35
	.quad	47                      # 0x2f
	.quad	76                      # 0x4c
	.quad	124                     # 0x7c
	.quad	108                     # 0x6c
	.quad	89                      # 0x59
	.quad	123                     # 0x7b
	.quad	108                     # 0x6c
	.quad	119                     # 0x77
	.quad	107                     # 0x6b
	.quad	81                      # 0x51
	.quad	122                     # 0x7a
	.quad	63                      # 0x3f
	.quad	13                      # 0xd
	.quad	5                       # 0x5
	.quad	16                      # 0x10
	.quad	27                      # 0x1b
	.quad	46                      # 0x2e
	.quad	36                      # 0x24
	.quad	61                      # 0x3d
	.quad	51                      # 0x33
	.quad	42                      # 0x2a
	.quad	70                      # 0x46
	.quad	52                      # 0x34
	.quad	83                      # 0x53
	.quad	65                      # 0x41
	.quad	41                      # 0x29
	.quad	59                      # 0x3b
	.quad	36                      # 0x24
	.quad	19                      # 0x13
	.quad	17                      # 0x11
	.quad	15                      # 0xf
	.quad	24                      # 0x18
	.quad	41                      # 0x29
	.quad	34                      # 0x22
	.quad	59                      # 0x3b
	.quad	48                      # 0x30
	.quad	40                      # 0x28
	.quad	64                      # 0x40
	.quad	50                      # 0x32
	.quad	78                      # 0x4e
	.quad	62                      # 0x3e
	.quad	80                      # 0x50
	.quad	56                      # 0x38
	.quad	33                      # 0x21
	.quad	29                      # 0x1d
	.quad	28                      # 0x1c
	.quad	25                      # 0x19
	.quad	43                      # 0x2b
	.quad	39                      # 0x27
	.quad	63                      # 0x3f
	.quad	55                      # 0x37
	.quad	93                      # 0x5d
	.quad	76                      # 0x4c
	.quad	59                      # 0x3b
	.quad	93                      # 0x5d
	.quad	72                      # 0x48
	.quad	54                      # 0x36
	.quad	75                      # 0x4b
	.quad	50                      # 0x32
	.quad	29                      # 0x1d
	.quad	52                      # 0x34
	.quad	22                      # 0x16
	.quad	42                      # 0x2a
	.quad	40                      # 0x28
	.quad	67                      # 0x43
	.quad	57                      # 0x39
	.quad	95                      # 0x5f
	.quad	79                      # 0x4f
	.quad	72                      # 0x48
	.quad	57                      # 0x39
	.quad	89                      # 0x59
	.quad	69                      # 0x45
	.quad	49                      # 0x31
	.quad	66                      # 0x42
	.quad	46                      # 0x2e
	.quad	27                      # 0x1b
	.quad	77                      # 0x4d
	.quad	37                      # 0x25
	.quad	35                      # 0x23
	.quad	66                      # 0x42
	.quad	58                      # 0x3a
	.quad	52                      # 0x34
	.quad	91                      # 0x5b
	.quad	74                      # 0x4a
	.quad	62                      # 0x3e
	.quad	48                      # 0x30
	.quad	79                      # 0x4f
	.quad	63                      # 0x3f
	.quad	90                      # 0x5a
	.quad	62                      # 0x3e
	.quad	40                      # 0x28
	.quad	38                      # 0x26
	.quad	125                     # 0x7d
	.quad	32                      # 0x20
	.quad	60                      # 0x3c
	.quad	56                      # 0x38
	.quad	50                      # 0x32
	.quad	92                      # 0x5c
	.quad	78                      # 0x4e
	.quad	65                      # 0x41
	.quad	55                      # 0x37
	.quad	87                      # 0x57
	.quad	71                      # 0x47
	.quad	51                      # 0x33
	.quad	73                      # 0x49
	.quad	51                      # 0x33
	.quad	70                      # 0x46
	.quad	30                      # 0x1e
	.quad	109                     # 0x6d
	.quad	53                      # 0x35
	.quad	49                      # 0x31
	.quad	94                      # 0x5e
	.quad	88                      # 0x58
	.quad	75                      # 0x4b
	.quad	66                      # 0x42
	.quad	122                     # 0x7a
	.quad	91                      # 0x5b
	.quad	73                      # 0x49
	.quad	56                      # 0x38
	.quad	42                      # 0x2a
	.quad	64                      # 0x40
	.quad	44                      # 0x2c
	.quad	21                      # 0x15
	.quad	25                      # 0x19
	.quad	90                      # 0x5a
	.quad	43                      # 0x2b
	.quad	41                      # 0x29
	.quad	77                      # 0x4d
	.quad	73                      # 0x49
	.quad	63                      # 0x3f
	.quad	56                      # 0x38
	.quad	92                      # 0x5c
	.quad	77                      # 0x4d
	.quad	66                      # 0x42
	.quad	47                      # 0x2f
	.quad	67                      # 0x43
	.quad	48                      # 0x30
	.quad	53                      # 0x35
	.quad	36                      # 0x24
	.quad	20                      # 0x14
	.quad	71                      # 0x47
	.quad	34                      # 0x22
	.quad	67                      # 0x43
	.quad	60                      # 0x3c
	.quad	58                      # 0x3a
	.quad	49                      # 0x31
	.quad	88                      # 0x58
	.quad	76                      # 0x4c
	.quad	67                      # 0x43
	.quad	106                     # 0x6a
	.quad	71                      # 0x47
	.quad	54                      # 0x36
	.quad	38                      # 0x26
	.quad	39                      # 0x27
	.quad	23                      # 0x17
	.quad	15                      # 0xf
	.quad	109                     # 0x6d
	.quad	53                      # 0x35
	.quad	51                      # 0x33
	.quad	47                      # 0x2f
	.quad	90                      # 0x5a
	.quad	82                      # 0x52
	.quad	58                      # 0x3a
	.quad	57                      # 0x39
	.quad	48                      # 0x30
	.quad	72                      # 0x48
	.quad	57                      # 0x39
	.quad	41                      # 0x29
	.quad	23                      # 0x17
	.quad	27                      # 0x1b
	.quad	62                      # 0x3e
	.quad	9                       # 0x9
	.quad	86                      # 0x56
	.quad	42                      # 0x2a
	.quad	40                      # 0x28
	.quad	37                      # 0x25
	.quad	70                      # 0x46
	.quad	64                      # 0x40
	.quad	52                      # 0x34
	.quad	43                      # 0x2b
	.quad	70                      # 0x46
	.quad	55                      # 0x37
	.quad	42                      # 0x2a
	.quad	25                      # 0x19
	.quad	29                      # 0x1d
	.quad	18                      # 0x12
	.quad	11                      # 0xb
	.quad	11                      # 0xb
	.quad	118                     # 0x76
	.quad	68                      # 0x44
	.quad	30                      # 0x1e
	.quad	55                      # 0x37
	.quad	50                      # 0x32
	.quad	46                      # 0x2e
	.quad	74                      # 0x4a
	.quad	65                      # 0x41
	.quad	49                      # 0x31
	.quad	39                      # 0x27
	.quad	24                      # 0x18
	.quad	16                      # 0x10
	.quad	22                      # 0x16
	.quad	13                      # 0xd
	.quad	14                      # 0xe
	.quad	7                       # 0x7
	.quad	91                      # 0x5b
	.quad	44                      # 0x2c
	.quad	39                      # 0x27
	.quad	38                      # 0x26
	.quad	34                      # 0x22
	.quad	63                      # 0x3f
	.quad	52                      # 0x34
	.quad	45                      # 0x2d
	.quad	31                      # 0x1f
	.quad	52                      # 0x34
	.quad	28                      # 0x1c
	.quad	19                      # 0x13
	.quad	14                      # 0xe
	.quad	8                       # 0x8
	.quad	9                       # 0x9
	.quad	3                       # 0x3
	.quad	123                     # 0x7b
	.quad	60                      # 0x3c
	.quad	58                      # 0x3a
	.quad	53                      # 0x35
	.quad	47                      # 0x2f
	.quad	43                      # 0x2b
	.quad	32                      # 0x20
	.quad	22                      # 0x16
	.quad	37                      # 0x25
	.quad	24                      # 0x18
	.quad	17                      # 0x11
	.quad	12                      # 0xc
	.quad	15                      # 0xf
	.quad	10                      # 0xa
	.quad	2                       # 0x2
	.quad	1                       # 0x1
	.quad	71                      # 0x47
	.quad	37                      # 0x25
	.quad	34                      # 0x22
	.quad	30                      # 0x1e
	.quad	28                      # 0x1c
	.quad	20                      # 0x14
	.quad	17                      # 0x11
	.quad	26                      # 0x1a
	.quad	21                      # 0x15
	.quad	16                      # 0x10
	.quad	10                      # 0xa
	.quad	6                       # 0x6
	.quad	8                       # 0x8
	.quad	6                       # 0x6
	.quad	2                       # 0x2
	.quad	0                       # 0x0
	.size	t15HB, 2048

	.type	t15l,@object            # @t15l
	.p2align	4
t15l:
	.ascii	"\003\004\005\007\007\b\t\t\t\n\n\013\013\013\f\r\004\003\005\006\007\007\b\b\b\t\t\n\n\n\013\013\005\005\005\006\007\007\b\b\b\t\t\n\n\013\013\013\006\006\006\007\007\b\b\t\t\t\n\n\n\013\013\013\007\006\007\007\b\b\t\t\t\t\n\n\n\013\013\013\b\007\007\b\b\b\t\t\t\t\n\n\013\013\013\f\t\007\b\b\b\t\t\t\t\n\n\n\013\013\f\f\t\b\b\t\t\t\t\n\n\n\n\n\013\013\013\f\t\b\b\t\t\t\t\n\n\n\n\013\013\f\f\f\t\b\t\t\t\t\n\n\n\013\013\013\013\f\f\f\n\t\t\t\n\n\n\n\n\013\013\013\013\f\r\f\n\t\t\t\n\n\n\n\013\013\013\013\f\f\f\r\013\n\t\n\n\n\013\013\013\013\013\013\f\f\r\r\013\n\n\n\n\013\013\013\013\f\f\f\f\f\r\r\f\013\013\013\013\013\013\013\f\f\f\f\r\r\f\r\f\013\013\013\013\013\013\f\f\f\f\f\r\r\r\r"
	.size	t15l, 256

	.type	t16HB,@object           # @t16HB
	.p2align	4
t16HB:
	.quad	1                       # 0x1
	.quad	5                       # 0x5
	.quad	14                      # 0xe
	.quad	44                      # 0x2c
	.quad	74                      # 0x4a
	.quad	63                      # 0x3f
	.quad	110                     # 0x6e
	.quad	93                      # 0x5d
	.quad	172                     # 0xac
	.quad	149                     # 0x95
	.quad	138                     # 0x8a
	.quad	242                     # 0xf2
	.quad	225                     # 0xe1
	.quad	195                     # 0xc3
	.quad	376                     # 0x178
	.quad	17                      # 0x11
	.quad	3                       # 0x3
	.quad	4                       # 0x4
	.quad	12                      # 0xc
	.quad	20                      # 0x14
	.quad	35                      # 0x23
	.quad	62                      # 0x3e
	.quad	53                      # 0x35
	.quad	47                      # 0x2f
	.quad	83                      # 0x53
	.quad	75                      # 0x4b
	.quad	68                      # 0x44
	.quad	119                     # 0x77
	.quad	201                     # 0xc9
	.quad	107                     # 0x6b
	.quad	207                     # 0xcf
	.quad	9                       # 0x9
	.quad	15                      # 0xf
	.quad	13                      # 0xd
	.quad	23                      # 0x17
	.quad	38                      # 0x26
	.quad	67                      # 0x43
	.quad	58                      # 0x3a
	.quad	103                     # 0x67
	.quad	90                      # 0x5a
	.quad	161                     # 0xa1
	.quad	72                      # 0x48
	.quad	127                     # 0x7f
	.quad	117                     # 0x75
	.quad	110                     # 0x6e
	.quad	209                     # 0xd1
	.quad	206                     # 0xce
	.quad	16                      # 0x10
	.quad	45                      # 0x2d
	.quad	21                      # 0x15
	.quad	39                      # 0x27
	.quad	69                      # 0x45
	.quad	64                      # 0x40
	.quad	114                     # 0x72
	.quad	99                      # 0x63
	.quad	87                      # 0x57
	.quad	158                     # 0x9e
	.quad	140                     # 0x8c
	.quad	252                     # 0xfc
	.quad	212                     # 0xd4
	.quad	199                     # 0xc7
	.quad	387                     # 0x183
	.quad	365                     # 0x16d
	.quad	26                      # 0x1a
	.quad	75                      # 0x4b
	.quad	36                      # 0x24
	.quad	68                      # 0x44
	.quad	65                      # 0x41
	.quad	115                     # 0x73
	.quad	101                     # 0x65
	.quad	179                     # 0xb3
	.quad	164                     # 0xa4
	.quad	155                     # 0x9b
	.quad	264                     # 0x108
	.quad	246                     # 0xf6
	.quad	226                     # 0xe2
	.quad	395                     # 0x18b
	.quad	382                     # 0x17e
	.quad	362                     # 0x16a
	.quad	9                       # 0x9
	.quad	66                      # 0x42
	.quad	30                      # 0x1e
	.quad	59                      # 0x3b
	.quad	56                      # 0x38
	.quad	102                     # 0x66
	.quad	185                     # 0xb9
	.quad	173                     # 0xad
	.quad	265                     # 0x109
	.quad	142                     # 0x8e
	.quad	253                     # 0xfd
	.quad	232                     # 0xe8
	.quad	400                     # 0x190
	.quad	388                     # 0x184
	.quad	378                     # 0x17a
	.quad	445                     # 0x1bd
	.quad	16                      # 0x10
	.quad	111                     # 0x6f
	.quad	54                      # 0x36
	.quad	52                      # 0x34
	.quad	100                     # 0x64
	.quad	184                     # 0xb8
	.quad	178                     # 0xb2
	.quad	160                     # 0xa0
	.quad	133                     # 0x85
	.quad	257                     # 0x101
	.quad	244                     # 0xf4
	.quad	228                     # 0xe4
	.quad	217                     # 0xd9
	.quad	385                     # 0x181
	.quad	366                     # 0x16e
	.quad	715                     # 0x2cb
	.quad	10                      # 0xa
	.quad	98                      # 0x62
	.quad	48                      # 0x30
	.quad	91                      # 0x5b
	.quad	88                      # 0x58
	.quad	165                     # 0xa5
	.quad	157                     # 0x9d
	.quad	148                     # 0x94
	.quad	261                     # 0x105
	.quad	248                     # 0xf8
	.quad	407                     # 0x197
	.quad	397                     # 0x18d
	.quad	372                     # 0x174
	.quad	380                     # 0x17c
	.quad	889                     # 0x379
	.quad	884                     # 0x374
	.quad	8                       # 0x8
	.quad	85                      # 0x55
	.quad	84                      # 0x54
	.quad	81                      # 0x51
	.quad	159                     # 0x9f
	.quad	156                     # 0x9c
	.quad	143                     # 0x8f
	.quad	260                     # 0x104
	.quad	249                     # 0xf9
	.quad	427                     # 0x1ab
	.quad	401                     # 0x191
	.quad	392                     # 0x188
	.quad	383                     # 0x17f
	.quad	727                     # 0x2d7
	.quad	713                     # 0x2c9
	.quad	708                     # 0x2c4
	.quad	7                       # 0x7
	.quad	154                     # 0x9a
	.quad	76                      # 0x4c
	.quad	73                      # 0x49
	.quad	141                     # 0x8d
	.quad	131                     # 0x83
	.quad	256                     # 0x100
	.quad	245                     # 0xf5
	.quad	426                     # 0x1aa
	.quad	406                     # 0x196
	.quad	394                     # 0x18a
	.quad	384                     # 0x180
	.quad	735                     # 0x2df
	.quad	359                     # 0x167
	.quad	710                     # 0x2c6
	.quad	352                     # 0x160
	.quad	11                      # 0xb
	.quad	139                     # 0x8b
	.quad	129                     # 0x81
	.quad	67                      # 0x43
	.quad	125                     # 0x7d
	.quad	247                     # 0xf7
	.quad	233                     # 0xe9
	.quad	229                     # 0xe5
	.quad	219                     # 0xdb
	.quad	393                     # 0x189
	.quad	743                     # 0x2e7
	.quad	737                     # 0x2e1
	.quad	720                     # 0x2d0
	.quad	885                     # 0x375
	.quad	882                     # 0x372
	.quad	439                     # 0x1b7
	.quad	4                       # 0x4
	.quad	243                     # 0xf3
	.quad	120                     # 0x78
	.quad	118                     # 0x76
	.quad	115                     # 0x73
	.quad	227                     # 0xe3
	.quad	223                     # 0xdf
	.quad	396                     # 0x18c
	.quad	746                     # 0x2ea
	.quad	742                     # 0x2e6
	.quad	736                     # 0x2e0
	.quad	721                     # 0x2d1
	.quad	712                     # 0x2c8
	.quad	706                     # 0x2c2
	.quad	223                     # 0xdf
	.quad	436                     # 0x1b4
	.quad	6                       # 0x6
	.quad	202                     # 0xca
	.quad	224                     # 0xe0
	.quad	222                     # 0xde
	.quad	218                     # 0xda
	.quad	216                     # 0xd8
	.quad	389                     # 0x185
	.quad	386                     # 0x182
	.quad	381                     # 0x17d
	.quad	364                     # 0x16c
	.quad	888                     # 0x378
	.quad	443                     # 0x1bb
	.quad	707                     # 0x2c3
	.quad	440                     # 0x1b8
	.quad	437                     # 0x1b5
	.quad	1728                    # 0x6c0
	.quad	4                       # 0x4
	.quad	747                     # 0x2eb
	.quad	211                     # 0xd3
	.quad	210                     # 0xd2
	.quad	208                     # 0xd0
	.quad	370                     # 0x172
	.quad	379                     # 0x17b
	.quad	734                     # 0x2de
	.quad	723                     # 0x2d3
	.quad	714                     # 0x2ca
	.quad	1735                    # 0x6c7
	.quad	883                     # 0x373
	.quad	877                     # 0x36d
	.quad	876                     # 0x36c
	.quad	3459                    # 0xd83
	.quad	865                     # 0x361
	.quad	2                       # 0x2
	.quad	377                     # 0x179
	.quad	369                     # 0x171
	.quad	102                     # 0x66
	.quad	187                     # 0xbb
	.quad	726                     # 0x2d6
	.quad	722                     # 0x2d2
	.quad	358                     # 0x166
	.quad	711                     # 0x2c7
	.quad	709                     # 0x2c5
	.quad	866                     # 0x362
	.quad	1734                    # 0x6c6
	.quad	871                     # 0x367
	.quad	3458                    # 0xd82
	.quad	870                     # 0x366
	.quad	434                     # 0x1b2
	.quad	0                       # 0x0
	.quad	12                      # 0xc
	.quad	10                      # 0xa
	.quad	7                       # 0x7
	.quad	11                      # 0xb
	.quad	10                      # 0xa
	.quad	17                      # 0x11
	.quad	11                      # 0xb
	.quad	9                       # 0x9
	.quad	13                      # 0xd
	.quad	12                      # 0xc
	.quad	10                      # 0xa
	.quad	7                       # 0x7
	.quad	5                       # 0x5
	.quad	3                       # 0x3
	.quad	1                       # 0x1
	.quad	3                       # 0x3
	.size	t16HB, 2048

	.type	t16l,@object            # @t16l
	.p2align	4
t16l:
	.ascii	"\001\004\006\b\t\t\n\n\013\013\013\f\f\f\r\t\003\004\006\007\b\t\t\t\n\n\n\013\f\013\f\b\006\006\007\b\t\t\n\n\013\n\013\013\013\f\f\t\b\007\b\t\t\n\n\n\013\013\f\f\f\r\r\n\t\b\t\t\n\n\013\013\013\f\f\f\r\r\r\t\t\b\t\t\n\013\013\f\013\f\f\r\r\r\016\n\n\t\t\n\013\013\013\013\f\f\f\f\r\r\016\n\n\t\n\n\013\013\013\f\f\r\r\r\r\017\017\n\n\n\n\013\013\013\f\f\r\r\r\r\016\016\016\n\013\n\n\013\013\f\f\r\r\r\r\016\r\016\r\013\013\013\n\013\f\f\f\f\r\016\016\016\017\017\016\n\f\013\013\013\f\f\r\016\016\016\016\016\016\r\016\013\f\f\f\f\f\r\r\r\r\017\016\016\016\016\020\013\016\f\f\f\r\r\016\016\016\020\017\017\017\021\017\013\r\r\013\f\016\016\r\016\016\017\020\017\021\017\016\013\t\b\b\t\t\n\n\n\013\013\013\013\013\013\013\b"
	.size	t16l, 256

	.type	t24HB,@object           # @t24HB
	.p2align	4
t24HB:
	.quad	15                      # 0xf
	.quad	13                      # 0xd
	.quad	46                      # 0x2e
	.quad	80                      # 0x50
	.quad	146                     # 0x92
	.quad	262                     # 0x106
	.quad	248                     # 0xf8
	.quad	434                     # 0x1b2
	.quad	426                     # 0x1aa
	.quad	669                     # 0x29d
	.quad	653                     # 0x28d
	.quad	649                     # 0x289
	.quad	621                     # 0x26d
	.quad	517                     # 0x205
	.quad	1032                    # 0x408
	.quad	88                      # 0x58
	.quad	14                      # 0xe
	.quad	12                      # 0xc
	.quad	21                      # 0x15
	.quad	38                      # 0x26
	.quad	71                      # 0x47
	.quad	130                     # 0x82
	.quad	122                     # 0x7a
	.quad	216                     # 0xd8
	.quad	209                     # 0xd1
	.quad	198                     # 0xc6
	.quad	327                     # 0x147
	.quad	345                     # 0x159
	.quad	319                     # 0x13f
	.quad	297                     # 0x129
	.quad	279                     # 0x117
	.quad	42                      # 0x2a
	.quad	47                      # 0x2f
	.quad	22                      # 0x16
	.quad	41                      # 0x29
	.quad	74                      # 0x4a
	.quad	68                      # 0x44
	.quad	128                     # 0x80
	.quad	120                     # 0x78
	.quad	221                     # 0xdd
	.quad	207                     # 0xcf
	.quad	194                     # 0xc2
	.quad	182                     # 0xb6
	.quad	340                     # 0x154
	.quad	315                     # 0x13b
	.quad	295                     # 0x127
	.quad	541                     # 0x21d
	.quad	18                      # 0x12
	.quad	81                      # 0x51
	.quad	39                      # 0x27
	.quad	75                      # 0x4b
	.quad	70                      # 0x46
	.quad	134                     # 0x86
	.quad	125                     # 0x7d
	.quad	116                     # 0x74
	.quad	220                     # 0xdc
	.quad	204                     # 0xcc
	.quad	190                     # 0xbe
	.quad	178                     # 0xb2
	.quad	325                     # 0x145
	.quad	311                     # 0x137
	.quad	293                     # 0x125
	.quad	271                     # 0x10f
	.quad	16                      # 0x10
	.quad	147                     # 0x93
	.quad	72                      # 0x48
	.quad	69                      # 0x45
	.quad	135                     # 0x87
	.quad	127                     # 0x7f
	.quad	118                     # 0x76
	.quad	112                     # 0x70
	.quad	210                     # 0xd2
	.quad	200                     # 0xc8
	.quad	188                     # 0xbc
	.quad	352                     # 0x160
	.quad	323                     # 0x143
	.quad	306                     # 0x132
	.quad	285                     # 0x11d
	.quad	540                     # 0x21c
	.quad	14                      # 0xe
	.quad	263                     # 0x107
	.quad	66                      # 0x42
	.quad	129                     # 0x81
	.quad	126                     # 0x7e
	.quad	119                     # 0x77
	.quad	114                     # 0x72
	.quad	214                     # 0xd6
	.quad	202                     # 0xca
	.quad	192                     # 0xc0
	.quad	180                     # 0xb4
	.quad	341                     # 0x155
	.quad	317                     # 0x13d
	.quad	301                     # 0x12d
	.quad	281                     # 0x119
	.quad	262                     # 0x106
	.quad	12                      # 0xc
	.quad	249                     # 0xf9
	.quad	123                     # 0x7b
	.quad	121                     # 0x79
	.quad	117                     # 0x75
	.quad	113                     # 0x71
	.quad	215                     # 0xd7
	.quad	206                     # 0xce
	.quad	195                     # 0xc3
	.quad	185                     # 0xb9
	.quad	347                     # 0x15b
	.quad	330                     # 0x14a
	.quad	308                     # 0x134
	.quad	291                     # 0x123
	.quad	272                     # 0x110
	.quad	520                     # 0x208
	.quad	10                      # 0xa
	.quad	435                     # 0x1b3
	.quad	115                     # 0x73
	.quad	111                     # 0x6f
	.quad	109                     # 0x6d
	.quad	211                     # 0xd3
	.quad	203                     # 0xcb
	.quad	196                     # 0xc4
	.quad	187                     # 0xbb
	.quad	353                     # 0x161
	.quad	332                     # 0x14c
	.quad	313                     # 0x139
	.quad	298                     # 0x12a
	.quad	283                     # 0x11b
	.quad	531                     # 0x213
	.quad	381                     # 0x17d
	.quad	17                      # 0x11
	.quad	427                     # 0x1ab
	.quad	212                     # 0xd4
	.quad	208                     # 0xd0
	.quad	205                     # 0xcd
	.quad	201                     # 0xc9
	.quad	193                     # 0xc1
	.quad	186                     # 0xba
	.quad	177                     # 0xb1
	.quad	169                     # 0xa9
	.quad	320                     # 0x140
	.quad	303                     # 0x12f
	.quad	286                     # 0x11e
	.quad	268                     # 0x10c
	.quad	514                     # 0x202
	.quad	377                     # 0x179
	.quad	16                      # 0x10
	.quad	335                     # 0x14f
	.quad	199                     # 0xc7
	.quad	197                     # 0xc5
	.quad	191                     # 0xbf
	.quad	189                     # 0xbd
	.quad	181                     # 0xb5
	.quad	174                     # 0xae
	.quad	333                     # 0x14d
	.quad	321                     # 0x141
	.quad	305                     # 0x131
	.quad	289                     # 0x121
	.quad	275                     # 0x113
	.quad	521                     # 0x209
	.quad	379                     # 0x17b
	.quad	371                     # 0x173
	.quad	11                      # 0xb
	.quad	668                     # 0x29c
	.quad	184                     # 0xb8
	.quad	183                     # 0xb7
	.quad	179                     # 0xb3
	.quad	175                     # 0xaf
	.quad	344                     # 0x158
	.quad	331                     # 0x14b
	.quad	314                     # 0x13a
	.quad	304                     # 0x130
	.quad	290                     # 0x122
	.quad	277                     # 0x115
	.quad	530                     # 0x212
	.quad	383                     # 0x17f
	.quad	373                     # 0x175
	.quad	366                     # 0x16e
	.quad	10                      # 0xa
	.quad	652                     # 0x28c
	.quad	346                     # 0x15a
	.quad	171                     # 0xab
	.quad	168                     # 0xa8
	.quad	164                     # 0xa4
	.quad	318                     # 0x13e
	.quad	309                     # 0x135
	.quad	299                     # 0x12b
	.quad	287                     # 0x11f
	.quad	276                     # 0x114
	.quad	263                     # 0x107
	.quad	513                     # 0x201
	.quad	375                     # 0x177
	.quad	368                     # 0x170
	.quad	362                     # 0x16a
	.quad	6                       # 0x6
	.quad	648                     # 0x288
	.quad	322                     # 0x142
	.quad	316                     # 0x13c
	.quad	312                     # 0x138
	.quad	307                     # 0x133
	.quad	302                     # 0x12e
	.quad	292                     # 0x124
	.quad	284                     # 0x11c
	.quad	269                     # 0x10d
	.quad	261                     # 0x105
	.quad	512                     # 0x200
	.quad	376                     # 0x178
	.quad	370                     # 0x172
	.quad	364                     # 0x16c
	.quad	359                     # 0x167
	.quad	4                       # 0x4
	.quad	620                     # 0x26c
	.quad	300                     # 0x12c
	.quad	296                     # 0x128
	.quad	294                     # 0x126
	.quad	288                     # 0x120
	.quad	282                     # 0x11a
	.quad	273                     # 0x111
	.quad	266                     # 0x10a
	.quad	515                     # 0x203
	.quad	380                     # 0x17c
	.quad	374                     # 0x176
	.quad	369                     # 0x171
	.quad	365                     # 0x16d
	.quad	361                     # 0x169
	.quad	357                     # 0x165
	.quad	2                       # 0x2
	.quad	1033                    # 0x409
	.quad	280                     # 0x118
	.quad	278                     # 0x116
	.quad	274                     # 0x112
	.quad	267                     # 0x10b
	.quad	264                     # 0x108
	.quad	259                     # 0x103
	.quad	382                     # 0x17e
	.quad	378                     # 0x17a
	.quad	372                     # 0x174
	.quad	367                     # 0x16f
	.quad	363                     # 0x16b
	.quad	360                     # 0x168
	.quad	358                     # 0x166
	.quad	356                     # 0x164
	.quad	0                       # 0x0
	.quad	43                      # 0x2b
	.quad	20                      # 0x14
	.quad	19                      # 0x13
	.quad	17                      # 0x11
	.quad	15                      # 0xf
	.quad	13                      # 0xd
	.quad	11                      # 0xb
	.quad	9                       # 0x9
	.quad	7                       # 0x7
	.quad	6                       # 0x6
	.quad	4                       # 0x4
	.quad	7                       # 0x7
	.quad	5                       # 0x5
	.quad	3                       # 0x3
	.quad	1                       # 0x1
	.quad	3                       # 0x3
	.size	t24HB, 2048

	.type	t24l,@object            # @t24l
	.p2align	4
t24l:
	.ascii	"\004\004\006\007\b\t\t\n\n\013\013\013\013\013\f\t\004\004\005\006\007\b\b\t\t\t\n\n\n\n\n\b\006\005\006\007\007\b\b\t\t\t\t\n\n\n\013\007\007\006\007\007\b\b\b\t\t\t\t\n\n\n\n\007\b\007\007\b\b\b\b\t\t\t\n\n\n\n\013\007\t\007\b\b\b\b\t\t\t\t\n\n\n\n\n\007\t\b\b\b\b\t\t\t\t\n\n\n\n\n\013\007\n\b\b\b\t\t\t\t\n\n\n\n\n\013\013\b\n\t\t\t\t\t\t\t\t\n\n\n\n\013\013\b\n\t\t\t\t\t\t\n\n\n\n\n\013\013\013\b\013\t\t\t\t\n\n\n\n\n\n\013\013\013\013\b\013\n\t\t\t\n\n\n\n\n\n\013\013\013\013\b\013\n\n\n\n\n\n\n\n\n\013\013\013\013\013\b\013\n\n\n\n\n\n\n\013\013\013\013\013\013\013\b\f\n\n\n\n\n\n\013\013\013\013\013\013\013\013\b\b\007\007\007\007\007\007\007\007\007\007\b\b\b\b\004"
	.size	t24l, 256

	.type	t32HB,@object           # @t32HB
	.p2align	4
t32HB:
	.quad	1                       # 0x1
	.quad	5                       # 0x5
	.quad	4                       # 0x4
	.quad	5                       # 0x5
	.quad	6                       # 0x6
	.quad	5                       # 0x5
	.quad	4                       # 0x4
	.quad	4                       # 0x4
	.quad	7                       # 0x7
	.quad	3                       # 0x3
	.quad	6                       # 0x6
	.quad	0                       # 0x0
	.quad	7                       # 0x7
	.quad	2                       # 0x2
	.quad	3                       # 0x3
	.quad	1                       # 0x1
	.size	t32HB, 128

	.type	t32l,@object            # @t32l
	.p2align	4
t32l:
	.ascii	"\001\004\004\005\004\006\005\006\004\005\005\006\005\006\006\006"
	.size	t32l, 16

	.type	t33HB,@object           # @t33HB
	.p2align	4
t33HB:
	.quad	15                      # 0xf
	.quad	14                      # 0xe
	.quad	13                      # 0xd
	.quad	12                      # 0xc
	.quad	11                      # 0xb
	.quad	10                      # 0xa
	.quad	9                       # 0x9
	.quad	8                       # 0x8
	.quad	7                       # 0x7
	.quad	6                       # 0x6
	.quad	5                       # 0x5
	.quad	4                       # 0x4
	.quad	3                       # 0x3
	.quad	2                       # 0x2
	.quad	1                       # 0x1
	.quad	0                       # 0x0
	.size	t33HB, 128

	.type	t33l,@object            # @t33l
	.p2align	4
t33l:
	.zero	16,4
	.size	t33l, 16

	.type	ht,@object              # @ht
	.globl	ht
	.p2align	4
ht:
	.zero	24
	.long	2                       # 0x2
	.long	0                       # 0x0
	.quad	t1HB
	.quad	t1l
	.long	3                       # 0x3
	.long	0                       # 0x0
	.quad	t2HB
	.quad	t2l
	.long	3                       # 0x3
	.long	0                       # 0x0
	.quad	t3HB
	.quad	t3l
	.zero	24
	.long	4                       # 0x4
	.long	0                       # 0x0
	.quad	t5HB
	.quad	t5l
	.long	4                       # 0x4
	.long	0                       # 0x0
	.quad	t6HB
	.quad	t6l
	.long	6                       # 0x6
	.long	0                       # 0x0
	.quad	t7HB
	.quad	t7l
	.long	6                       # 0x6
	.long	0                       # 0x0
	.quad	t8HB
	.quad	t8l
	.long	6                       # 0x6
	.long	0                       # 0x0
	.quad	t9HB
	.quad	t9l
	.long	8                       # 0x8
	.long	0                       # 0x0
	.quad	t10HB
	.quad	t10l
	.long	8                       # 0x8
	.long	0                       # 0x0
	.quad	t11HB
	.quad	t11l
	.long	8                       # 0x8
	.long	0                       # 0x0
	.quad	t12HB
	.quad	t12l
	.long	16                      # 0x10
	.long	0                       # 0x0
	.quad	t13HB
	.quad	t13l
	.zero	24
	.long	16                      # 0x10
	.long	0                       # 0x0
	.quad	t15HB
	.quad	t15l
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	t16HB
	.quad	t16l
	.long	2                       # 0x2
	.long	3                       # 0x3
	.quad	t16HB
	.quad	t16l
	.long	3                       # 0x3
	.long	7                       # 0x7
	.quad	t16HB
	.quad	t16l
	.long	4                       # 0x4
	.long	15                      # 0xf
	.quad	t16HB
	.quad	t16l
	.long	6                       # 0x6
	.long	63                      # 0x3f
	.quad	t16HB
	.quad	t16l
	.long	8                       # 0x8
	.long	255                     # 0xff
	.quad	t16HB
	.quad	t16l
	.long	10                      # 0xa
	.long	1023                    # 0x3ff
	.quad	t16HB
	.quad	t16l
	.long	13                      # 0xd
	.long	8191                    # 0x1fff
	.quad	t16HB
	.quad	t16l
	.long	4                       # 0x4
	.long	15                      # 0xf
	.quad	t24HB
	.quad	t24l
	.long	5                       # 0x5
	.long	31                      # 0x1f
	.quad	t24HB
	.quad	t24l
	.long	6                       # 0x6
	.long	63                      # 0x3f
	.quad	t24HB
	.quad	t24l
	.long	7                       # 0x7
	.long	127                     # 0x7f
	.quad	t24HB
	.quad	t24l
	.long	8                       # 0x8
	.long	255                     # 0xff
	.quad	t24HB
	.quad	t24l
	.long	9                       # 0x9
	.long	511                     # 0x1ff
	.quad	t24HB
	.quad	t24l
	.long	11                      # 0xb
	.long	2047                    # 0x7ff
	.quad	t24HB
	.quad	t24l
	.long	13                      # 0xd
	.long	8191                    # 0x1fff
	.quad	t24HB
	.quad	t24l
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	t32HB
	.quad	t32l
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	t33HB
	.quad	t33l
	.size	ht, 816

	.type	psy_data,@object        # @psy_data
	.globl	psy_data
	.p2align	4
psy_data:
	.quad	4676829883349860352     # double 48000
	.quad	4633781804099174400     # double 61
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	4627589354611539968     # double 24.5
	.quad	4616788596805198676     # double 4.532
	.quad	4606912202822375178     # double 0.96999999999999997
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4627589354611539968     # double 24.5
	.quad	4616788596805198676     # double 4.532
	.quad	4604975654982605865     # double 0.755
	.quad	4602120372818852970     # double 0.46899999999999997
	.quad	4611686018427387904     # double 2
	.quad	4607182418800017408     # double 1
	.quad	4627589354611539968     # double 24.5
	.quad	4616788596805198676     # double 4.532
	.quad	4604822532595275268     # double 0.73799999999999999
	.quad	4606623972446223466     # double 0.93799999999999994
	.quad	4613937818241073152     # double 3
	.quad	4607182418800017408     # double 1
	.quad	4627589354611539968     # double 24.5
	.quad	4606317727671562273     # double 0.90400000000000003
	.quad	4604750475001237340     # double 0.72999999999999998
	.quad	4609010880248729829     # double 1.4059999999999999
	.quad	4616189618054758400     # double 4
	.quad	4607182418800017408     # double 1
	.quad	4627589354611539968     # double 24.5
	.quad	4606317727671562273     # double 0.90400000000000003
	.quad	4604696431805708894     # double 0.72399999999999998
	.quad	4611123068473966592     # double 1.875
	.quad	4617315517961601024     # double 5
	.quad	4607182418800017408     # double 1
	.quad	4626322717216342016     # double 20
	.quad	4591149604126578442     # double 0.089999999999999996
	.quad	4604687424606454153     # double 0.72299999999999998
	.quad	4612460637563295629     # double 2.3439999999999999
	.quad	4618441417868443648     # double 6
	.quad	4607182418800017408     # double 1
	.quad	4626322717216342016     # double 20
	.quad	4591149604126578442     # double 0.089999999999999996
	.quad	4604687424606454153     # double 0.72299999999999998
	.quad	4613516731675914011     # double 2.8130000000000002
	.quad	4619567317775286272     # double 7
	.quad	4607182418800017408     # double 1
	.quad	4626322717216342016     # double 20
	.quad	4584015902316823577     # double 0.029000000000000001
	.quad	4604687424606454153     # double 0.72299999999999998
	.quad	4614570573988718707     # double 3.2810000000000001
	.quad	4620693217682128896     # double 8
	.quad	4607182418800017408     # double 1
	.quad	4626322717216342016     # double 20
	.quad	4584015902316823577     # double 0.029000000000000001
	.quad	4604642388610180448     # double 0.71799999999999997
	.quad	4615626668101337088     # double 3.75
	.quad	4621256167635550208     # double 9
	.quad	4607182418800017408     # double 1
	.quad	4626322717216342016     # double 20
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4604390187031047700     # double 0.68999999999999995
	.quad	4616413672136220082     # double 4.1989999999999998
	.quad	4621819117588971520     # double 10
	.quad	4607182418800017408     # double 1
	.quad	4626322717216342016     # double 20
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4604119971053405471     # double 0.66000000000000003
	.quad	4616893305496535040     # double 4.625
	.quad	4622382067542392832     # double 11
	.quad	4607182418800017408     # double 1
	.quad	4625759767262920704     # double 18
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4603948834267565392     # double 0.64100000000000001
	.quad	4617368435257222627     # double 5.0469999999999997
	.quad	4622945017495814144     # double 12
	.quad	4607182418800017408     # double 1
	.quad	4625759767262920704     # double 18
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4603579539098121011     # double 0.59999999999999998
	.quad	4617808662120798093     # double 5.4379999999999997
	.quad	4623507967449235456     # double 13
	.quad	4607182418800017408     # double 1
	.quad	4625759767262920704     # double 18
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4603435423910045155     # double 0.58399999999999996
	.quad	4618247763084466717     # double 5.8280000000000003
	.quad	4624070917402656768     # double 14
	.quad	4607182418800017408     # double 1
	.quad	4622945017495814144     # double 12
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4602967049548798624     # double 0.53200000000000003
	.quad	4618653087050930061     # double 6.1879999999999997
	.quad	4624633867356078080     # double 15
	.quad	4607182418800017408     # double 1
	.quad	4622945017495814144     # double 12
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4603012085545072329     # double 0.53700000000000003
	.quad	4619029137619815498     # double 6.5220000000000002
	.quad	4625196817309499392     # double 16
	.quad	4611686018427387904     # double 2
	.quad	4618441417868443648     # double 6
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605894389306589446     # double 0.85699999999999998
	.quad	4619763224359076889     # double 7.1740000000000004
	.quad	4625478292286210048     # double 17
	.quad	4611686018427387904     # double 2
	.quad	4618441417868443648     # double 6
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605903396505844187     # double 0.85799999999999998
	.quad	4620469163600667214     # double 7.8010000000000002
	.quad	4625759767262920704     # double 18
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605858360509570482     # double 0.85299999999999998
	.quad	4620919523563404263     # double 8.4019999999999992
	.quad	4626041242239631360     # double 19
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605597151731182993     # double 0.82399999999999995
	.quad	4621237027337133883     # double 8.9659999999999993
	.quad	4626322717216342016     # double 20
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605182820565464908     # double 0.77800000000000002
	.quad	4621528635413006123     # double 9.4839999999999999
	.quad	4626604192193052672     # double 21
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4604840546993784750     # double 0.73999999999999999
	.quad	4621799977290555195     # double 9.9659999999999993
	.quad	4626885667169763328     # double 22
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4604561323816887779     # double 0.70899999999999996
	.quad	4622058934269128999     # double 10.426
	.quad	4627167142146473984     # double 23
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4604264086241481327     # double 0.67600000000000005
	.quad	4622306632248634376     # double 10.866
	.quad	4627448617123184640     # double 24
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4603867769474272723     # double 0.63200000000000001
	.quad	4622539130579397378     # double 11.279
	.quad	4627730092099895296     # double 25
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4603507481504083083     # double 0.59199999999999997
	.quad	4622758681061231690     # double 11.669
	.quad	4628011567076605952     # double 26
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4603156200733148185     # double 0.55300000000000005
	.quad	4622968661393857839     # double 12.042
	.quad	4628293042053316608     # double 27
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4602768891165194322     # double 0.51000000000000001
	.quad	4623162316177834770     # double 12.385999999999999
	.quad	4628574517030027264     # double 28
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4602795912762958545     # double 0.51300000000000001
	.quad	4623350904412230910     # double 12.721
	.quad	4628855992006737920     # double 29
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4603651596692158939     # double 0.60799999999999998
	.quad	4623572706693878907     # double 13.115
	.quad	4629137466983448576     # double 30
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4604237064643717104     # double 0.67300000000000004
	.quad	4623824345323058233     # double 13.561999999999999
	.quad	4629418941960159232     # double 31
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4603912805470546428     # double 0.63700000000000001
	.quad	4624061910203402027     # double 13.984
	.quad	4629700416936869888     # double 32
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4603453438308554637     # double 0.58599999999999997
	.quad	4624279771835376075     # double 14.371
	.quad	4629841154425225216     # double 33
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4603318330319733522     # double 0.57099999999999995
	.quad	4624488063318141960     # double 14.741
	.quad	4629981891913580544     # double 34
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585348967806525243     # double 0.035999999999999997
	.quad	4603723654286196867     # double 0.61599999999999999
	.quad	4624712680349557064     # double 15.140000000000001
	.quad	4630122629401935872     # double 35
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585348967806525243     # double 0.035999999999999997
	.quad	4603939827068310651     # double 0.64000000000000001
	.quad	4624950808179854279     # double 15.563000000000001
	.quad	4630263366890291200     # double 36
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585348967806525243     # double 0.035999999999999997
	.quad	4603561524699611529     # double 0.59799999999999998
	.quad	4625175425211269382     # double 15.962
	.quad	4630404104378646528     # double 37
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585348967806525243     # double 0.035999999999999997
	.quad	4603021092744327070     # double 0.53800000000000003
	.quad	4625288015201953645     # double 16.324000000000002
	.quad	4630544841867001856     # double 38
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585348967806525243     # double 0.035999999999999997
	.quad	4602786905563703804     # double 0.51200000000000001
	.quad	4625383998169011978     # double 16.664999999999999
	.quad	4630685579355357184     # double 39
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586646004499207946     # double 0.044999999999999998
	.quad	4602931020751779660     # double 0.52800000000000002
	.quad	4625483921785744261     # double 17.02
	.quad	4630826316843712512     # double 40
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586646004499207946     # double 0.044999999999999998
	.quad	4602831941559977509     # double 0.51700000000000002
	.quad	4625583282452523123     # double 17.373000000000001
	.quad	4630967054332067840     # double 41
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586646004499207946     # double 0.044999999999999998
	.quad	4602552718383080538     # double 0.49299999999999999
	.quad	4625677576569721192     # double 17.707999999999998
	.quad	4631107791820423168     # double 42
	.quad	4618441417868443648     # double 6
	.quad	0                       # double 0
	.quad	4587943041191890649     # double 0.053999999999999999
	.quad	4602660804774137430     # double 0.499
	.quad	4625772433636872684     # double 18.045000000000002
	.quad	4631248529308778496     # double 43
	.quad	4619567317775286272     # double 7
	.quad	0                       # double 0
	.quad	4589204049087554388     # double 0.063
	.quad	4602903999154015437     # double 0.52500000000000002
	.quad	4625871794303651545     # double 18.398
	.quad	4631389266797133824     # double 44
	.quad	4619567317775286272     # double 7
	.quad	0                       # double 0
	.quad	4589204049087554388     # double 0.063
	.quad	4603048114342091293     # double 0.54100000000000004
	.quad	4625974251195174224     # double 18.762
	.quad	4631530004285489152     # double 45
	.quad	4620693217682128896     # double 8
	.quad	0                       # double 0
	.quad	4589852567433895739     # double 0.071999999999999995
	.quad	4602931020751779660     # double 0.52800000000000002
	.quad	4626075019236836639     # double 19.120000000000001
	.quad	4631670741773844480     # double 46
	.quad	4620693217682128896     # double 8
	.quad	0                       # double 0
	.quad	4589852567433895739     # double 0.071999999999999995
	.quad	4602768891165194322     # double 0.51000000000000001
	.quad	4626172409578778526     # double 19.466000000000001
	.quad	4631811479262199808     # double 47
	.quad	4620693217682128896     # double 8
	.quad	0                       # double 0
	.quad	4589852567433895739     # double 0.071999999999999995
	.quad	4602732862368175358     # double 0.50600000000000001
	.quad	4626268392545836859     # double 19.806999999999999
	.quad	4631952216750555136     # double 48
	.quad	4621819117588971520     # double 10
	.quad	0                       # double 0
	.quad	4595653203753948938     # double 0.17999999999999999
	.quad	4602903999154015437     # double 0.52500000000000002
	.quad	4626367471737639010     # double 20.158999999999999
	.quad	4632092954238910464     # double 49
	.quad	4621819117588971520     # double 10
	.quad	0                       # double 0
	.quad	4595653203753948938     # double 0.17999999999999999
	.quad	4603003078345817588     # double 0.53600000000000003
	.quad	4626469647154184978     # double 20.521999999999998
	.quad	4632233691727265792     # double 50
	.quad	4621819117588971520     # double 10
	.quad	0                       # double 0
	.quad	4595653203753948938     # double 0.17999999999999999
	.quad	4602840948759232250     # double 0.51800000000000002
	.quad	4626568726345987129     # double 20.873999999999999
	.quad	4632374429215621120     # double 51
	.quad	4623507967449235456     # double 13
	.quad	0                       # double 0
	.quad	4600372976163433218     # double 0.372
	.quad	4602687826371901653     # double 0.501
	.quad	4626664427838068752     # double 21.213999999999999
	.quad	4632515166703976448     # double 52
	.quad	4623507967449235456     # double 13
	.quad	0                       # double 0
	.quad	4600372976163433218     # double 0.372
	.quad	4602624775977118466     # double 0.497
	.quad	4626759847855173665     # double 21.553000000000001
	.quad	4632655904192331776     # double 53
	.quad	4624070917402656768     # double 14
	.quad	0                       # double 0
	.quad	4600877379321698714     # double 0.40000000000000002
	.quad	4602624775977118466     # double 0.497
	.quad	4626855267872278577     # double 21.891999999999999
	.quad	4632796641680687104     # double 54
	.quad	4625759767262920704     # double 18
	.quad	0                       # double 0
	.quad	4610006175766378709     # double 1.627
	.quad	4602588747180099502     # double 0.495
	.quad	4626950687889383490     # double 22.231000000000002
	.quad	4632937379169042432     # double 55
	.quad	4625759767262920704     # double 18
	.quad	0                       # double 0
	.quad	4610006175766378709     # double 1.627
	.quad	4602570732781590020     # double 0.49399999999999999
	.quad	4627045826431511691     # double 22.568999999999999
	.quad	4633078116657397760     # double 56
	.quad	4626322717216342016     # double 20
	.quad	0                       # double 0
	.quad	4610821327298932769     # double 1.8080000000000001
	.quad	4602624775977118466     # double 0.497
	.quad	4627141527923593314     # double 22.908999999999999
	.quad	4633218854145753088     # double 57
	.quad	4627730092099895296     # double 25
	.quad	0                       # double 0
	.quad	4627056522480626696     # double 22.606999999999999
	.quad	4602570732781590020     # double 0.49399999999999999
	.quad	4627236947940698227     # double 23.248000000000001
	.quad	4633359591634108416     # double 58
	.quad	4627730092099895296     # double 25
	.quad	0                       # double 0
	.quad	4627056522480626696     # double 22.606999999999999
	.quad	4602444631992023646     # double 0.48699999999999999
	.quad	4627331242057896296     # double 23.582999999999998
	.quad	4633500329122463744     # double 59
	.quad	4630122629401935872     # double 35
	.quad	0                       # double 0
	.quad	4629601900695021158     # double 31.649999999999999
	.quad	4602372574397985718     # double 0.48299999999999998
	.quad	4627424691750164234     # double 23.914999999999999
	.quad	4633641066610819072     # double 60
	.quad	4634415122796773376     # double 67
	.quad	0                       # double 0
	.quad	4648540478310067470     # double 605.86699999999996
	.quad	4602354559999476236     # double 0.48199999999999998
	.quad	4627517859967455461     # double 24.245999999999999
	.quad	4633781804099174400     # double 61
	.quad	4634415122796773376     # double 67
	.quad	0                       # double 0
	.quad	4648540478310067470     # double 605.86699999999996
	.quad	4602894991954760696     # double 0.52400000000000002
	.quad	4627610746709769978     # double 24.576000000000001
	.quad	4676293871431319552     # double 44100
	.quad	4633922541587529728     # double 62
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	4627589354611539968     # double 24.5
	.quad	4616788596805198676     # double 4.532
	.quad	4606741066036535099     # double 0.95099999999999996
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4627589354611539968     # double 24.5
	.quad	4616788596805198676     # double 4.532
	.quad	4604480259023595110     # double 0.69999999999999996
	.quad	4601435825675492655     # double 0.43099999999999999
	.quad	4611686018427387904     # double 2
	.quad	4607182418800017408     # double 1
	.quad	4627589354611539968     # double 24.5
	.quad	4616788596805198676     # double 4.532
	.quad	4604309122237755032     # double 0.68100000000000005
	.quad	4605930418103608410     # double 0.86099999999999999
	.quad	4613937818241073152     # double 3
	.quad	4607182418800017408     # double 1
	.quad	4627589354611539968     # double 24.5
	.quad	4606317727671562273     # double 0.90400000000000003
	.quad	4604255079042226586     # double 0.67500000000000004
	.quad	4608497469891209593     # double 1.292
	.quad	4616189618054758400     # double 4
	.quad	4607182418800017408     # double 1
	.quad	4627589354611539968     # double 24.5
	.quad	4606317727671562273     # double 0.90400000000000003
	.quad	4604183021448188658     # double 0.66700000000000004
	.quad	4610438521330606277     # double 1.7230000000000001
	.quad	4617315517961601024     # double 5
	.quad	4607182418800017408     # double 1
	.quad	4626322717216342016     # double 20
	.quad	4591149604126578442     # double 0.089999999999999996
	.quad	4604165007049679176     # double 0.66500000000000004
	.quad	4612030543798881747     # double 2.153
	.quad	4618441417868443648     # double 6
	.quad	4607182418800017408     # double 1
	.quad	4626322717216342016     # double 20
	.quad	4591149604126578442     # double 0.089999999999999996
	.quad	4604155999850424435     # double 0.66400000000000003
	.quad	4613001069518580089     # double 2.5840000000000001
	.quad	4619567317775286272     # double 7
	.quad	4607182418800017408     # double 1
	.quad	4626322717216342016     # double 20
	.quad	4584015902316823577     # double 0.029000000000000001
	.quad	4604155999850424435     # double 0.66400000000000003
	.quad	4613971595238278431     # double 3.0150000000000001
	.quad	4620693217682128896     # double 8
	.quad	4607182418800017408     # double 1
	.quad	4626322717216342016     # double 20
	.quad	4584015902316823577     # double 0.029000000000000001
	.quad	4604155999850424435     # double 0.66400000000000003
	.quad	4614939869158163087     # double 3.4449999999999998
	.quad	4621256167635550208     # double 9
	.quad	4607182418800017408     # double 1
	.quad	4626322717216342016     # double 20
	.quad	4584015902316823577     # double 0.029000000000000001
	.quad	4604074935057131766     # double 0.65500000000000003
	.quad	4615910394877861429     # double 3.8759999999999999
	.quad	4621819117588971520     # double 10
	.quad	4607182418800017408     # double 1
	.quad	4626322717216342016     # double 20
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4603723654286196867     # double 0.61599999999999999
	.quad	4616503744128767492     # double 4.2789999999999999
	.quad	4622382067542392832     # double 11
	.quad	4607182418800017408     # double 1
	.quad	4626322717216342016     # double 20
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4603552517500356788     # double 0.59699999999999998
	.quad	4616943970992342958     # double 4.6699999999999999
	.quad	4622945017495814144     # double 12
	.quad	4607182418800017408     # double 1
	.quad	4625759767262920704     # double 18
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4603381380714516709     # double 0.57799999999999996
	.quad	4617379694256291054     # double 5.0570000000000004
	.quad	4623507967449235456     # double 13
	.quad	4607182418800017408     # double 1
	.quad	4625759767262920704     # double 18
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4603048114342091293     # double 0.54100000000000004
	.quad	4617783892322847556     # double 5.4160000000000004
	.quad	4624070917402656768     # double 14
	.quad	4607182418800017408     # double 1
	.quad	4625759767262920704     # double 18
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4603354359116752486     # double 0.57499999999999996
	.quad	4618186964489497215     # double 5.774
	.quad	4624633867356078080     # double 15
	.quad	4611686018427387904     # double 2
	.quad	4622945017495814144     # double 12
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605885382107334705     # double 0.85599999999999998
	.quad	4618916547629131235     # double 6.4219999999999997
	.quad	4625196817309499392     # double 16
	.quad	4611686018427387904     # double 2
	.quad	4618441417868443648     # double 6
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605795310114787295     # double 0.84599999999999997
	.quad	4619596591172864180     # double 7.0259999999999998
	.quad	4625478292286210048     # double 17
	.quad	4611686018427387904     # double 2
	.quad	4618441417868443648     # double 6
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605741266919258849     # double 0.83999999999999997
	.quad	4620252990818553430     # double 7.609
	.quad	4625759767262920704     # double 18
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605579137332673511     # double 0.82199999999999995
	.quad	4620787793274303676     # double 8.1679999999999992
	.quad	4626041242239631360     # double 19
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605380978949069210     # double 0.80000000000000004
	.quad	4621092912149058028     # double 8.7100000000000008
	.quad	4626322717216342016     # double 20
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4604957640584096383     # double 0.753
	.quad	4621372698275908420     # double 9.2070000000000007
	.quad	4626604192193052672     # double 21
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4604516287820614074     # double 0.70399999999999996
	.quad	4621628840504715117     # double 9.6620000000000008
	.quad	4626885667169763328     # double 22
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4604246071842971845     # double 0.67400000000000004
	.quad	4621874849634360230     # double 10.099
	.quad	4627167142146473984     # double 23
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4603939827068310651     # double 0.64000000000000001
	.quad	4622109036814983496     # double 10.515000000000001
	.quad	4627448617123184640     # double 24
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4603660603891413680     # double 0.60899999999999999
	.quad	4622335342696258863     # double 10.917
	.quad	4627730092099895296     # double 25
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4603273294323459817     # double 0.56599999999999995
	.quad	4622547011878745276     # double 11.292999999999999
	.quad	4628011567076605952     # double 26
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4602994071146562847     # double 0.53500000000000003
	.quad	4622749110912023527     # double 11.651999999999999
	.quad	4628293042053316608     # double 27
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4602958042349543883     # double 0.53100000000000003
	.quad	4622943328645953880     # double 11.997
	.quad	4628574517030027264     # double 28
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4603714647086942126     # double 0.61499999999999999
	.quad	4623166819777462141     # double 12.394
	.quad	4628855992006737920     # double 29
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4604354158234028737     # double 0.68600000000000005
	.quad	4623423524956222259     # double 12.85
	.quad	4629137466983448576     # double 30
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4604029899060858061     # double 0.65000000000000002
	.quad	4623663904586333159     # double 13.276999999999999
	.quad	4629418941960159232     # double 31
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4603687625489177903     # double 0.61199999999999999
	.quad	4623891336367515369     # double 13.680999999999999
	.quad	4629700416936869888     # double 32
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4603282301522714558     # double 0.56699999999999995
	.quad	4624105820299768889     # double 14.061999999999999
	.quad	4629841154425225216     # double 33
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4602858963157741732     # double 0.52000000000000002
	.quad	4624302289833512927     # double 14.411
	.quad	4629981891913580544     # double 34
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4602795912762958545     # double 0.51300000000000001
	.quad	4624493692817676173     # double 14.750999999999999
	.quad	4630122629401935872     # double 35
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585348967806525243     # double 0.035999999999999997
	.quad	4603192229530167149     # double 0.55700000000000005
	.quad	4624700858400535216     # double 15.119
	.quad	4630263366890291200     # double 36
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585348967806525243     # double 0.035999999999999997
	.quad	4603435423910045155     # double 0.58399999999999996
	.quad	4624919845932416106     # double 15.507999999999999
	.quad	4630404104378646528     # double 37
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585348967806525243     # double 0.035999999999999997
	.quad	4603309323120478781     # double 0.56999999999999995
	.quad	4625130952164949098     # double 15.882999999999999
	.quad	4630544841867001856     # double 38
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586646004499207946     # double 0.044999999999999998
	.quad	4603390387913771450     # double 0.57899999999999996
	.quad	4625270845228374295     # double 16.263000000000002
	.quad	4630685579355357184     # double 39
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586646004499207946     # double 0.044999999999999998
	.quad	4603444431109299896     # double 0.58499999999999996
	.quad	4625380901944268161     # double 16.654
	.quad	4630826316843712512     # double 40
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586646004499207946     # double 0.044999999999999998
	.quad	4603111164736874480     # double 0.54800000000000004
	.quad	4625483921785744261     # double 17.02
	.quad	4630967054332067840     # double 41
	.quad	4618441417868443648     # double 6
	.quad	0                       # double 0
	.quad	4587943041191890649     # double 0.053999999999999999
	.quad	4603003078345817588     # double 0.53600000000000003
	.quad	4625583563927499833     # double 17.373999999999999
	.quad	4631107791820423168     # double 42
	.quad	4618441417868443648     # double 6
	.quad	0                       # double 0
	.quad	4587943041191890649     # double 0.053999999999999999
	.quad	4603129179135383962     # double 0.55000000000000004
	.quad	4625687709668882776     # double 17.744
	.quad	4631248529308778496     # double 43
	.quad	4619567317775286272     # double 7
	.quad	0                       # double 0
	.quad	4589204049087554388     # double 0.063
	.quad	4602967049548798624     # double 0.53200000000000003
	.quad	4625789040660498612     # double 18.103999999999999
	.quad	4631389266797133824     # double 44
	.quad	4619567317775286272     # double 7
	.quad	0                       # double 0
	.quad	4589204049087554388     # double 0.063
	.quad	4602714847969665876     # double 0.504
	.quad	4625885586577510367     # double 18.446999999999999
	.quad	4631530004285489152     # double 45
	.quad	4619567317775286272     # double 7
	.quad	0                       # double 0
	.quad	4589204049087554388     # double 0.063
	.quad	4602606761578608984     # double 0.496
	.quad	4625979880694708437     # double 18.782
	.quad	4631670741773844480     # double 46
	.quad	4621256167635550208     # double 9
	.quad	0                       # double 0
	.quad	4590501085780237091     # double 0.081000000000000003
	.quad	4602831941559977509     # double 0.51700000000000002
	.quad	4626077833986603745     # double 19.129999999999999
	.quad	4631811479262199808     # double 47
	.quad	4621256167635550208     # double 9
	.quad	0                       # double 0
	.quad	4590501085780237091     # double 0.081000000000000003
	.quad	4602922013552524919     # double 0.52700000000000002
	.quad	4626178320553289449     # double 19.486999999999998
	.quad	4631952216750555136     # double 48
	.quad	4621256167635550208     # double 9
	.quad	0                       # double 0
	.quad	4590501085780237091     # double 0.081000000000000003
	.quad	4602822934360722768     # double 0.51600000000000001
	.quad	4626277118270114890     # double 19.838000000000001
	.quad	4632092954238910464     # double 49
	.quad	4621819117588971520     # double 10
	.quad	0                       # double 0
	.quad	4595653203753948938     # double 0.17999999999999999
	.quad	4602624775977118466     # double 0.497
	.quad	4626373101237173223     # double 20.178999999999998
	.quad	4632233691727265792     # double 50
	.quad	4621819117588971520     # double 10
	.quad	0                       # double 0
	.quad	4595653203753948938     # double 0.17999999999999999
	.quad	4602480660789042610     # double 0.48899999999999999
	.quad	4626466269454464451     # double 20.510000000000002
	.quad	4632374429215621120     # double 51
	.quad	4622382067542392832     # double 11
	.quad	0                       # double 0
	.quad	4596301722100290290     # double 0.19800000000000001
	.quad	4602696833571156394     # double 0.502
	.quad	4626562533896499495     # double 20.852
	.quad	4632515166703976448     # double 52
	.quad	4624070917402656768     # double 14
	.quad	0                       # double 0
	.quad	4600877379321698714     # double 0.40000000000000002
	.quad	4602687826371901653     # double 0.501
	.quad	4626659361288487961     # double 21.196000000000002
	.quad	4632655904192331776     # double 53
	.quad	4624070917402656768     # double 14
	.quad	0                       # double 0
	.quad	4600877379321698714     # double 0.40000000000000002
	.quad	4602516689586061574     # double 0.49099999999999999
	.quad	4626753655405686030     # double 21.530999999999999
	.quad	4632796641680687104     # double 54
	.quad	4624633867356078080     # double 15
	.quad	0                       # double 0
	.quad	4601399796878473691     # double 0.42899999999999999
	.quad	4602624775977118466     # double 0.497
	.quad	4626849075422790943     # double 21.870000000000001
	.quad	4632937379169042432     # double 55
	.quad	4626322717216342016     # double 20
	.quad	0                       # double 0
	.quad	4610821327298932769     # double 1.8080000000000001
	.quad	4602714847969665876     # double 0.504
	.quad	4626945902814779408     # double 22.213999999999999
	.quad	4633078116657397760     # double 56
	.quad	4626322717216342016     # double 20
	.quad	0                       # double 0
	.quad	4610821327298932769     # double 1.8080000000000001
	.quad	4602714847969665876     # double 0.504
	.quad	4627042730206767874     # double 22.558
	.quad	4633218854145753088     # double 57
	.quad	4626604192193052672     # double 21
	.quad	0                       # double 0
	.quad	4611226651265396113     # double 1.8979999999999999
	.quad	4602588747180099502     # double 0.495
	.quad	4627138431698849497     # double 22.898
	.quad	4633359591634108416     # double 58
	.quad	4628293042053316608     # double 27
	.quad	0                       # double 0
	.quad	4627565710713496273     # double 24.416
	.quad	4602426617593514164     # double 0.48599999999999999
	.quad	4627232444341070856     # double 23.231999999999999
	.quad	4633500329122463744     # double 59
	.quad	4628293042053316608     # double 27
	.quad	0                       # double 0
	.quad	4627565710713496273     # double 24.416
	.quad	4602390588796495200     # double 0.48399999999999999
	.quad	4627325894033338794     # double 23.564
	.quad	4633641066610819072     # double 60
	.quad	4630263366890291200     # double 36
	.quad	0                       # double 0
	.quad	4629778385505418740     # double 32.554000000000002
	.quad	4602372574397985718     # double 0.48299999999999998
	.quad	4627419625200583442     # double 23.896999999999998
	.quad	4633781804099174400     # double 61
	.quad	4634837335261839360     # double 73
	.quad	0                       # double 0
	.quad	4649017727929173410     # double 660.12400000000002
	.quad	4602228459209909862     # double 0.47499999999999998
	.quad	4627513074892851380     # double 24.228999999999999
	.quad	4633922541587529728     # double 62
	.quad	4625759767262920704     # double 18
	.quad	0                       # double 0
	.quad	4639930976809139569     # double 162.77000000000001
	.quad	4602813927161468027     # double 0.51500000000000001
	.quad	4627573029062890750     # double 24.442
	.quad	4674525306978041856     # double 32000
	.quad	4633359591634108416     # double 58
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	4627589354611539968     # double 24.5
	.quad	4616788596805198676     # double 4.532
	.quad	4607155397202253185     # double 0.99699999999999999
	.quad	4599310126651373781     # double 0.313
	.quad	4607182418800017408     # double 1
	.quad	4611686018427387904     # double 2
	.quad	4627589354611539968     # double 24.5
	.quad	4616788596805198676     # double 4.532
	.quad	4606218648479760122     # double 0.89300000000000002
	.quad	4606623972446223466     # double 0.93799999999999994
	.quad	4611686018427387904     # double 2
	.quad	4611686018427387904     # double 2
	.quad	4627589354611539968     # double 24.5
	.quad	4610825830898560139     # double 1.8089999999999999
	.quad	4606110562088703230     # double 0.88100000000000001
	.quad	4609717945390226997     # double 1.5629999999999999
	.quad	4613937818241073152     # double 3
	.quad	4611686018427387904     # double 2
	.quad	4626322717216342016     # double 20
	.quad	4595689232550967902     # double 0.18099999999999999
	.quad	4606038504494665302     # double 0.873
	.quad	4612109356792360731     # double 2.1880000000000002
	.quad	4616189618054758400     # double 4
	.quad	4611686018427387904     # double 2
	.quad	4626322717216342016     # double 20
	.quad	4595689232550967902     # double 0.18099999999999999
	.quad	4606029497295410561     # double 0.872
	.quad	4613516731675914011     # double 2.8130000000000002
	.quad	4617315517961601024     # double 5
	.quad	4611686018427387904     # double 2
	.quad	4626322717216342016     # double 20
	.quad	4588375386756118217     # double 0.057000000000000002
	.quad	4606020490096155820     # double 0.87099999999999999
	.quad	4614924106559467291     # double 3.4380000000000002
	.quad	4618441417868443648     # double 6
	.quad	4611686018427387904     # double 2
	.quad	4626322717216342016     # double 20
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605921410904353669     # double 0.85999999999999999
	.quad	4616240283550566318     # double 4.0449999999999999
	.quad	4619567317775286272     # double 7
	.quad	4611686018427387904     # double 2
	.quad	4626322717216342016     # double 20
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605732259720004108     # double 0.83899999999999996
	.quad	4616893305496535040     # double 4.625
	.quad	4620693217682128896     # double 8
	.quad	4611686018427387904     # double 2
	.quad	4625759767262920704     # double 18
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605489065340126102     # double 0.81200000000000006
	.quad	4617510298645484798     # double 5.173
	.quad	4621256167635550208     # double 9
	.quad	4611686018427387904     # double 2
	.quad	4625759767262920704     # double 18
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605236863760993354     # double 0.78400000000000003
	.quad	4618101396096577176     # double 5.6980000000000004
	.quad	4621819117588971520     # double 10
	.quad	4611686018427387904     # double 2
	.quad	4622945017495814144     # double 12
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4604849554193039491     # double 0.74099999999999999
	.quad	4618649709351209533     # double 6.1849999999999996
	.quad	4622382067542392832     # double 11
	.quad	4611686018427387904     # double 2
	.quad	4622945017495814144     # double 12
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4604453237425830887     # double 0.69699999999999995
	.quad	4619155238409381872     # double 6.6340000000000003
	.quad	4622945017495814144     # double 12
	.quad	4611686018427387904     # double 2
	.quad	4618441417868443648     # double 6
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4604246071842971845     # double 0.67400000000000004
	.quad	4619646130768765256     # double 7.0700000000000003
	.quad	4623507967449235456     # double 13
	.quad	4611686018427387904     # double 2
	.quad	4618441417868443648     # double 6
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4604038906260112802     # double 0.65100000000000002
	.quad	4620121260529452843     # double 7.4919999999999999
	.quad	4624070917402656768     # double 14
	.quad	4611686018427387904     # double 2
	.quad	4618441417868443648     # double 6
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4603876776673527464     # double 0.63300000000000001
	.quad	4620586257190978847     # double 7.9050000000000002
	.quad	4624633867356078080     # double 15
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4603678618289923162     # double 0.61099999999999999
	.quad	4620864917417922396     # double 8.3049999999999997
	.quad	4625196817309499392     # double 16
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4603480459906318860     # double 0.58899999999999997
	.quad	4621084467899756708     # double 8.6950000000000002
	.quad	4625478292286210048     # double 17
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4603354359116752486     # double 0.57499999999999996
	.quad	4621292196432569172     # double 9.064
	.quad	4625759767262920704     # double 18
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4583439441564520153     # double 0.027
	.quad	4604065927857877025     # double 0.65400000000000003
	.quad	4621528635413006123     # double 9.4839999999999999
	.quad	4626041242239631360     # double 19
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4583439441564520153     # double 0.027
	.quad	4604696431805708894     # double 0.72399999999999998
	.quad	4621799977290555195     # double 9.9659999999999993
	.quad	4626322717216342016     # double 20
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4604489266222849851     # double 0.70099999999999996
	.quad	4622058934269128999     # double 10.426
	.quad	4626604192193052672     # double 21
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4604237064643717104     # double 0.67300000000000004
	.quad	4622306632248634376     # double 10.866
	.quad	4626885667169763328     # double 22
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4603858762275017982     # double 0.63100000000000001
	.quad	4622539130579397378     # double 11.279
	.quad	4627167142146473984     # double 23
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4603507481504083083     # double 0.59199999999999997
	.quad	4622758681061231690     # double 11.669
	.quad	4627448617123184640     # double 24
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4603156200733148185     # double 0.55300000000000005
	.quad	4622968661393857839     # double 12.042
	.quad	4627730092099895296     # double 25
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4602768891165194322     # double 0.51000000000000001
	.quad	4623162316177834770     # double 12.385999999999999
	.quad	4628011567076605952     # double 26
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583439441564520153     # double 0.027
	.quad	4602732862368175358     # double 0.50600000000000001
	.quad	4623350904412230910     # double 12.721
	.quad	4628293042053316608     # double 27
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585348967806525243     # double 0.035999999999999997
	.quad	4603237265526440854     # double 0.56200000000000006
	.quad	4623559195894996795     # double 13.090999999999999
	.quad	4628574517030027264     # double 28
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585348967806525243     # double 0.035999999999999997
	.quad	4603561524699611529     # double 0.59799999999999998
	.quad	4623782687026505056     # double 13.488
	.quad	4628855992006737920     # double 29
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585348967806525243     # double 0.035999999999999997
	.quad	4603480459906318860     # double 0.58899999999999997
	.quad	4623999422758572261     # double 13.872999999999999
	.quad	4629137466983448576     # double 30
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586646004499207946     # double 0.044999999999999998
	.quad	4603642589492904198     # double 0.60699999999999998
	.quad	4624221787990173680     # double 14.268000000000001
	.quad	4629418941960159232     # double 31
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586646004499207946     # double 0.044999999999999998
	.quad	4603759683083215831     # double 0.62
	.quad	4624453160421029839     # double 14.679
	.quad	4629700416936869888     # double 32
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586646004499207946     # double 0.044999999999999998
	.quad	4603399395113026191     # double 0.57999999999999996
	.quad	4624671585002957308     # double 15.067
	.quad	4629841154425225216     # double 33
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586646004499207946     # double 0.044999999999999998
	.quad	4602967049548798624     # double 0.53200000000000003
	.quad	4624872558136328716     # double 15.423999999999999
	.quad	4629981891913580544     # double 34
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586646004499207946     # double 0.044999999999999998
	.quad	4602831941559977509     # double 0.51700000000000002
	.quad	4625067901770165912     # double 15.771000000000001
	.quad	4630122629401935872     # double 35
	.quad	4618441417868443648     # double 6
	.quad	0                       # double 0
	.quad	4587943041191890649     # double 0.053999999999999999
	.quad	4602831941559977509     # double 0.51700000000000002
	.quad	4625230594306704671     # double 16.120000000000001
	.quad	4630263366890291200     # double 36
	.quad	4618441417868443648     # double 6
	.quad	0                       # double 0
	.quad	4587943041191890649     # double 0.053999999999999999
	.quad	4602759883965939581     # double 0.50900000000000001
	.quad	4625327984648646558     # double 16.466000000000001
	.quad	4630404104378646528     # double 37
	.quad	4618441417868443648     # double 6
	.quad	0                       # double 0
	.quad	4587943041191890649     # double 0.053999999999999999
	.quad	4602732862368175358     # double 0.50600000000000001
	.quad	4625423967615704891     # double 16.806999999999999
	.quad	4630544841867001856     # double 38
	.quad	4620693217682128896     # double 8
	.quad	0                       # double 0
	.quad	4589852567433895739     # double 0.071999999999999995
	.quad	4602876977556251214     # double 0.52200000000000002
	.quad	4625522765332530332     # double 17.158000000000001
	.quad	4630685579355357184     # double 39
	.quad	4620693217682128896     # double 8
	.quad	0                       # double 0
	.quad	4589852567433895739     # double 0.071999999999999995
	.quad	4602958042349543883     # double 0.53100000000000003
	.quad	4625624096324146168     # double 17.518000000000001
	.quad	4630826316843712512     # double 40
	.quad	4620693217682128896     # double 8
	.quad	0                       # double 0
	.quad	4589852567433895739     # double 0.071999999999999995
	.quad	4602849955958486991     # double 0.51900000000000002
	.quad	4625722894040971608     # double 17.869
	.quad	4630967054332067840     # double 41
	.quad	4621819117588971520     # double 10
	.quad	0                       # double 0
	.quad	4591149604126578442     # double 0.089999999999999996
	.quad	4602786905563703804     # double 0.51200000000000001
	.quad	4625820284382913495     # double 18.215
	.quad	4631107791820423168     # double 42
	.quad	4621819117588971520     # double 10
	.quad	0                       # double 0
	.quad	4591149604126578442     # double 0.089999999999999996
	.quad	4602759883965939581     # double 0.50900000000000001
	.quad	4625918237674808803     # double 18.562999999999999
	.quad	4631248529308778496     # double 43
	.quad	4621819117588971520     # double 10
	.quad	0                       # double 0
	.quad	4591149604126578442     # double 0.089999999999999996
	.quad	4602642790375627948     # double 0.498
	.quad	4626013657691913716     # double 18.902000000000001
	.quad	4631389266797133824     # double 44
	.quad	4622945017495814144     # double 12
	.quad	0                       # double 0
	.quad	4592518698413299073     # double 0.109
	.quad	4602570732781590020     # double 0.49399999999999999
	.quad	4626108514759065207     # double 19.239000000000001
	.quad	4631530004285489152     # double 45
	.quad	4622945017495814144     # double 12
	.quad	0                       # double 0
	.quad	4592518698413299073     # double 0.109
	.quad	4602687826371901653     # double 0.501
	.quad	4626204497726123540     # double 19.579999999999998
	.quad	4631670741773844480     # double 46
	.quad	4623507967449235456     # double 13
	.quad	0                       # double 0
	.quad	4593167216759640424     # double 0.11799999999999999
	.quad	4602750876766684840     # double 0.50800000000000001
	.quad	4626301606593088717     # double 19.925000000000001
	.quad	4631811479262199808     # double 47
	.quad	4624070917402656768     # double 14
	.quad	0                       # double 0
	.quad	4598211248342295380     # double 0.252
	.quad	4602696833571156394     # double 0.502
	.quad	4626398433985077182     # double 20.268999999999998
	.quad	4631952216750555136     # double 48
	.quad	4624070917402656768     # double 14
	.quad	0                       # double 0
	.quad	4598211248342295380     # double 0.252
	.quad	4602552718383080538     # double 0.49299999999999999
	.quad	4626493291052228674     # double 20.606000000000002
	.quad	4632092954238910464     # double 49
	.quad	4625196817309499392     # double 16
	.quad	0                       # double 0
	.quad	4598859766688636731     # double 0.28799999999999998
	.quad	4602624775977118466     # double 0.497
	.quad	4626588429594356875     # double 20.943999999999999
	.quad	4632233691727265792     # double 50
	.quad	4626322717216342016     # double 20
	.quad	0                       # double 0
	.quad	4603327337518988263     # double 0.57199999999999995
	.quad	4602732862368175358     # double 0.50600000000000001
	.quad	4626685256986345341     # double 21.288
	.quad	4632374429215621120     # double 51
	.quad	4626322717216342016     # double 20
	.quad	0                       # double 0
	.quad	4603327337518988263     # double 0.57199999999999995
	.quad	4602768891165194322     # double 0.51000000000000001
	.quad	4626782928803263939     # double 21.635000000000002
	.quad	4632515166703976448     # double 52
	.quad	4627167142146473984     # double 23
	.quad	0                       # double 0
	.quad	4604101956654895989     # double 0.65800000000000003
	.quad	4602714847969665876     # double 0.504
	.quad	4626880037670229115     # double 21.98
	.quad	4632655904192331776     # double 53
	.quad	4628293042053316608     # double 27
	.quad	0                       # double 0
	.quad	4612679062145223098     # double 2.4409999999999998
	.quad	4602606761578608984     # double 0.496
	.quad	4626975457687334027     # double 22.318999999999999
	.quad	4632796641680687104     # double 54
	.quad	4628293042053316608     # double 27
	.quad	0                       # double 0
	.quad	4612679062145223098     # double 2.4409999999999998
	.quad	4602552718383080538     # double 0.49299999999999999
	.quad	4627070314754485518     # double 22.655999999999999
	.quad	4632937379169042432     # double 55
	.quad	4629700416936869888     # double 32
	.quad	0                       # double 0
	.quad	4613696875661008830     # double 2.8929999999999998
	.quad	4602498675187552092     # double 0.48999999999999999
	.quad	4627165171821637009     # double 22.992999999999999
	.quad	4633078116657397760     # double 56
	.quad	4630404104378646528     # double 37
	.quad	0                       # double 0
	.quad	4629905612194891956     # double 33.457999999999998
	.quad	4602354559999476236     # double 0.48199999999999998
	.quad	4627258902988881658     # double 23.326000000000001
	.quad	4633218854145753088     # double 57
	.quad	4630404104378646528     # double 37
	.quad	0                       # double 0
	.quad	4629905612194891956     # double 33.457999999999998
	.quad	4601922214435248669     # double 0.45800000000000002
	.quad	4627351789731196174     # double 23.655999999999999
	.quad	4633359591634108416     # double 58
	.quad	4622945017495814144     # double 12
	.quad	0                       # double 0
	.quad	4622298187999333057     # double 10.851000000000001
	.quad	4602678819172646912     # double 0.5
	.quad	4627430884199651869     # double 23.937000000000001
	.quad	4672326283722489856     # double 24000
	.quad	4633500329122463744     # double 59
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	4624633867356078080     # double 15
	.quad	4625710514180398422     # double 17.825017899999999
	.quad	4604456607604540038     # double 0.69737416500000005
	.quad	4597702322164880921     # double 0.23687446100000001
	.quad	4607182418800017408     # double 1
	.quad	4611686018427387904     # double 2
	.quad	4624633867356078080     # double 15
	.quad	4625710514180398422     # double 17.825017899999999
	.quad	4601868618158932844     # double 0.45502480899999997
	.quad	4604571781445438511     # double 0.71016102999999997
	.quad	4611686018427387904     # double 2
	.quad	4611686018427387904     # double 2
	.quad	4624633867356078080     # double 15
	.quad	4610706493704986143     # double 1.78250182
	.quad	4601443759991215367     # double 0.43144044300000001
	.quad	4608001771335339875     # double 1.1819328099999999
	.quad	4613937818241073152     # double 3
	.quad	4611686018427387904     # double 2
	.quad	4624633867356078080     # double 15
	.quad	4610706493704986143     # double 1.78250182
	.quad	4601308216180051112     # double 0.42391624999999999
	.quad	4610114385195777421     # double 1.6510273200000001
	.quad	4616189618054758400     # double 4
	.quad	4611686018427387904     # double 2
	.quad	4623507967449235456     # double 13
	.quad	4595590160348752373     # double 0.178250194
	.quad	4601205363530207579     # double 0.41820678100000003
	.quad	4611947960436830725     # double 2.11632562
	.quad	4617315517961601024     # double 5
	.quad	4611686018427387904     # double 2
	.quad	4623507967449235456     # double 13
	.quad	4595590160348752373     # double 0.178250194
	.quad	4601086084234910392     # double 0.41158545000000002
	.quad	4612984783241319621     # double 2.5767674399999998
	.quad	4618441417868443648     # double 6
	.quad	4611686018427387904     # double 2
	.quad	4623507967449235456     # double 13
	.quad	4588284256684271473     # double 0.056367658100000002
	.quad	4600974822031497068     # double 0.40540915700000002
	.quad	4614008443600357584     # double 3.0313639600000002
	.quad	4619567317775286272     # double 7
	.quad	4611686018427387904     # double 2
	.quad	4623507967449235456     # double 13
	.quad	4588284256684271473     # double 0.056367658100000002
	.quad	4600871893685150997     # double 0.39969548599999999
	.quad	4615016901383313412     # double 3.4792091799999998
	.quad	4620693217682128896     # double 8
	.quad	4611686018427387904     # double 2
	.quad	4623507967449235456     # double 13
	.quad	4588284256684271473     # double 0.056367658100000002
	.quad	4600764847535536035     # double 0.39375323099999998
	.quad	4616008319977223070     # double 3.9194874799999999
	.quad	4621256167635550208     # double 9
	.quad	4611686018427387904     # double 2
	.quad	4622945017495814144     # double 12
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4600649631802153828     # double 0.38735747300000001
	.quad	4616585348824842489     # double 4.3514795299999998
	.quad	4621819117588971520     # double 10
	.quad	4611686018427387904     # double 2
	.quad	4622945017495814144     # double 12
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4600525280156639529     # double 0.38045456999999999
	.quad	4617061699894195025     # double 4.7745642699999999
	.quad	4622382067542392832     # double 11
	.quad	4611686018427387904     # double 2
	.quad	4621819117588971520     # double 10
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4600391961682137549     # double 0.37305390799999999
	.quad	4617527434864584941     # double 5.1882200200000002
	.quad	4622945017495814144     # double 12
	.quad	4611686018427387904     # double 2
	.quad	4621819117588971520     # double 10
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4600250264278543245     # double 0.365188122
	.quad	4617982075408695814     # double 5.5920219400000004
	.quad	4623507967449235456     # double 13
	.quad	4611686018427387904     # double 2
	.quad	4621819117588971520     # double 10
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4600100918591845762     # double 0.35689777099999997
	.quad	4618425250002076383     # double 5.9856400499999998
	.quad	4624070917402656768     # double 14
	.quad	4611686018427387904     # double 2
	.quad	4621256167635550208     # double 9
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4599953251720988574     # double 0.34870061299999999
	.quad	4618856685366301261     # double 6.3688316299999999
	.quad	4624633867356078080     # double 15
	.quad	4611686018427387904     # double 2
	.quad	4621256167635550208     # double 9
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4599801199677159817     # double 0.34026002900000002
	.quad	4619276202742242238     # double 6.7414379100000001
	.quad	4625196817309499392     # double 16
	.quad	4611686018427387904     # double 2
	.quad	4618441417868443648     # double 6
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4599658543943593605     # double 0.332341045
	.quad	4619683707092688177     # double 7.1033744800000003
	.quad	4625478292286210048     # double 17
	.quad	4611686018427387904     # double 2
	.quad	4618441417868443648     # double 6
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4599624709264284299     # double 0.33046284300000001
	.quad	4620079179097196676     # double 7.4546241799999997
	.quad	4625759767262920704     # double 18
	.quad	4611686018427387904     # double 2
	.quad	4618441417868443648     # double 6
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4599896826049199655     # double 0.34556835899999999
	.quad	4620462669207342562     # double 7.7952318199999997
	.quad	4626041242239631360     # double 19
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600478534269688101     # double 0.37785965199999999
	.quad	4620808373272189880     # double 8.2045574200000004
	.quad	4626322717216342016     # double 20
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600817741124209200     # double 0.39668941499999999
	.quad	4621073997672386018     # double 8.6764011399999994
	.quad	4626604192193052672     # double 21
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600719535090302804     # double 0.39123788500000001
	.quad	4621326879976231943     # double 9.1256103500000005
	.quad	4626885667169763328     # double 22
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600474086838955283     # double 0.37761276999999999
	.quad	4621567468477664061     # double 9.5529813800000002
	.quad	4627167142146473984     # double 23
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600207906771126321     # double 0.36283680800000001
	.quad	4621796267287106662     # double 9.9594097099999992
	.quad	4627448617123184640     # double 24
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4599958836256584108     # double 0.34901061700000002
	.quad	4622013814899967192     # double 10.3458519
	.quad	4627730092099895296     # double 25
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4599790633511858065     # double 0.339673489
	.quad	4622220669678156951     # double 10.7132998
	.quad	4628011567076605952     # double 26
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4599865793833837672     # double 0.34384572499999999
	.quad	4622417396874094670     # double 11.0627575
	.quad	4628293042053316608     # double 27
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4600081551510166602     # double 0.355822682
	.quad	4622633991024248590     # double 11.447506000000001
	.quad	4628574517030027264     # double 28
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4600122654350756138     # double 0.35810434800000002
	.quad	4622867757456076668     # double 11.862758599999999
	.quad	4628855992006737920     # double 29
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599930799781651056     # double 0.34745428
	.quad	4623086895295595122     # double 12.2520256
	.quad	4629137466983448576     # double 30
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599705144886694626     # double 0.33492791700000002
	.quad	4623292581277091588     # double 12.6173973
	.quad	4629418941960159232     # double 31
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599645973658443275     # double 0.331643254
	.quad	4623485914954415088     # double 12.960826900000001
	.quad	4629700416936869888     # double 32
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586582961079599862     # double 0.044562548399999999
	.quad	4599677060079130373     # double 0.333368897
	.quad	4623689195225580603     # double 13.321925200000001
	.quad	4629841154425225216     # double 33
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586582961079599862     # double 0.044562548399999999
	.quad	4599658038747801805     # double 0.332313001
	.quad	4623900728230473283     # double 13.6976833
	.quad	4629981891913580544     # double 34
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586582961079599862     # double 0.044562548399999999
	.quad	4599642342784366095     # double 0.33144170000000001
	.quad	4624097827536330214     # double 14.047802000000001
	.quad	4630122629401935872     # double 35
	.quad	4618441417868443648     # double 6
	.quad	0                       # double 0
	.quad	4587867388759778319     # double 0.053475055799999997
	.quad	4599633434502173569     # double 0.33094719099999997
	.quad	4624299082144678333     # double 14.405302000000001
	.quad	4630263366890291200     # double 36
	.quad	4618441417868443648     # double 6
	.quad	0                       # double 0
	.quad	4587867388759778319     # double 0.053475055799999997
	.quad	4599661005503063535     # double 0.33247768900000002
	.quad	4624503533351746964     # double 14.7684803
	.quad	4630404104378646528     # double 37
	.quad	4619567317775286272     # double 7
	.quad	0                       # double 0
	.quad	4589151816987594491     # double 0.062387566999999998
	.quad	4599664065122549581     # double 0.33264753200000002
	.quad	4624707949092968530     # double 15.131595600000001
	.quad	4630544841867001856     # double 38
	.quad	4619567317775286272     # double 7
	.quad	0                       # double 0
	.quad	4589151816987594491     # double 0.062387566999999998
	.quad	4599631529677689575     # double 0.33084145199999998
	.quad	4624911991710960968     # double 15.494048100000001
	.quad	4630685579355357184     # double 39
	.quad	4620693217682128896     # double 8
	.quad	0                       # double 0
	.quad	4589802132486359946     # double 0.071300074500000005
	.quad	4599576196543141459     # double 0.32776984599999998
	.quad	4625113284487315929     # double 15.851615900000001
	.quad	4630826316843712512     # double 40
	.quad	4620693217682128896     # double 8
	.quad	0                       # double 0
	.quad	4589802132486359946     # double 0.071300074500000005
	.quad	4599518602097177245     # double 0.32457271199999999
	.quad	4625254414971033740     # double 16.204628
	.quad	4630967054332067840     # double 41
	.quad	4621256167635550208     # double 9
	.quad	0                       # double 0
	.quad	4590444346593062272     # double 0.080212585599999997
	.quad	4599505132551267720     # double 0.32382500199999997
	.quad	4625351692751132441     # double 16.550228100000002
	.quad	4631107791820423168     # double 42
	.quad	4621819117588971520     # double 10
	.quad	0                       # double 0
	.quad	4591086560706970358     # double 0.089125096799999997
	.quad	4599461714175922573     # double 0.321414798
	.quad	4625452044125386831     # double 16.906747800000002
	.quad	4631248529308778496     # double 43
	.quad	4621819117588971520     # double 10
	.quad	0                       # double 0
	.quad	4591086560706970358     # double 0.089125096799999997
	.quad	4599403608643458246     # double 0.31818929299999998
	.quad	4625549708989873503     # double 17.253723099999998
	.quad	4631389266797133824     # double 44
	.quad	4622382067542392832     # double 11
	.quad	0                       # double 0
	.quad	4591728774280446489     # double 0.098037600500000002
	.quad	4599362983616774776     # double 0.315934151
	.quad	4625644393709896755     # double 17.590110800000001
	.quad	4631530004285489152     # double 45
	.quad	4622945017495814144     # double 12
	.quad	0                       # double 0
	.quad	4592370988415971853     # double 0.106950112
	.quad	4599357671819172676     # double 0.31563928699999999
	.quad	4625740459768368213     # double 17.931405999999999
	.quad	4631670741773844480     # double 46
	.quad	4623507967449235456     # double 13
	.quad	0                       # double 0
	.quad	4593013202515468420     # double 0.115862623
	.quad	4599374435081676878     # double 0.31656983500000002
	.quad	4625837193175861955     # double 18.275072099999999
	.quad	4631811479262199808     # double 47
	.quad	4624070917402656768     # double 14
	.quad	0                       # double 0
	.quad	4593655416614964987     # double 0.124775134
	.quad	4599374257910067537     # double 0.31656000000000001
	.quad	4625934045225058381     # double 18.619159700000001
	.quad	4631952216750555136     # double 48
	.quad	4624633867356078080     # double 15
	.quad	0                       # double 0
	.quad	4593984625316183737     # double 0.13368764499999999
	.quad	4599354549383479028     # double 0.31546595700000002
	.quad	4626030595561227270     # double 18.9621754
	.quad	4632092954238910464     # double 49
	.quad	4625196817309499392     # double 16
	.quad	0                       # double 0
	.quad	4594305732113730442     # double 0.14260014900000001
	.quad	4599320509087825134     # double 0.31357634099999998
	.quad	4626126518264493090     # double 19.3029613
	.quad	4632233691727265792     # double 50
	.quad	4625478292286210048     # double 17
	.quad	0                       # double 0
	.quad	4594626838911277146     # double 0.151512653
	.quad	4599285554489389291     # double 0.31163597100000001
	.quad	4626221551422390011     # double 19.640586899999999
	.quad	4632374429215621120     # double 51
	.quad	4625759767262920704     # double 18
	.quad	0                       # double 0
	.quad	4594947946213227009     # double 0.16042517100000001
	.quad	4599275299720980174     # double 0.31106671699999999
	.quad	4626315474837043753     # double 19.974269899999999
	.quad	4632515166703976448     # double 52
	.quad	4626322717216342016     # double 20
	.quad	0                       # double 0
	.quad	4600078546654452025     # double 0.35565587900000001
	.quad	4599282486277035156     # double 0.31146565100000001
	.quad	4626410422595432740     # double 20.311592099999999
	.quad	4632655904192331776     # double 53
	.quad	4626604192193052672     # double 21
	.quad	0                       # double 0
	.quad	4600398893226367231     # double 0.37343868600000002
	.quad	4599289806499927078     # double 0.31187200500000001
	.quad	4626505895417243284     # double 20.650779700000001
	.quad	4632796641680687104     # double 54
	.quad	4627167142146473984     # double 23
	.quad	0                       # double 0
	.quad	4601039585847780085     # double 0.40900427099999997
	.quad	4599274385436212622     # double 0.31101596399999998
	.quad	4626601119133699438     # double 20.9890823
	.quad	4632937379169042432     # double 55
	.quad	4627448617123184640     # double 24
	.quad	0                       # double 0
	.quad	4604267796550048734     # double 0.67641192699999997
	.quad	4599241810792021461     # double 0.30920770800000003
	.quad	4626695703986400952     # double 21.325115199999999
	.quad	4633078116657397760     # double 56
	.quad	4628011567076605952     # double 26
	.quad	0                       # double 0
	.quad	4604775511070012247     # double 0.73277956200000005
	.quad	4599222086682978241     # double 0.30811280000000002
	.quad	4626789007846483456     # double 21.656597099999999
	.quad	4633218854145753088     # double 57
	.quad	4628574517030027264     # double 28
	.quad	0                       # double 0
	.quad	4605283225598982958     # double 0.78914719799999999
	.quad	4599256202152717114     # double 0.310006589
	.quad	4626882333211254181     # double 21.9881554
	.quad	4633359591634108416     # double 58
	.quad	4629418941960159232     # double 31
	.quad	0                       # double 0
	.quad	4613403866830544490     # double 2.7628779400000001
	.quad	4599564365514862763     # double 0.32711309199999999
	.quad	4626976382248190029     # double 22.322284700000001
	.quad	4633500329122463744     # double 59
	.quad	4629981891913580544     # double 34
	.quad	0                       # double 0
	.quad	4614005942323642540     # double 3.0302531699999999
	.quad	4601167093219596478     # double 0.41608235199999999
	.quad	4627071586627315283     # double 22.6605186
	.quad	4671790271803949056     # double 22050
	.quad	4633500329122463744     # double 59
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	4624633867356078080     # double 15
	.quad	4625710514180398422     # double 17.825017899999999
	.quad	4604108113958292131     # double 0.65868359799999998
	.quad	4597009242753868168     # double 0.21763764299999999
	.quad	4607182418800017408     # double 1
	.quad	4611686018427387904     # double 2
	.quad	4624633867356078080     # double 15
	.quad	4625710514180398422     # double 17.825017899999999
	.quad	4601463836281749447     # double 0.43255490099999999
	.quad	4604052988592809224     # double 0.65256345299999996
	.quad	4611686018427387904     # double 2
	.quad	4611686018427387904     # double 2
	.quad	4624633867356078080     # double 15
	.quad	4610706493704986143     # double 1.78250182
	.quad	4600969497353600033     # double 0.40511357799999997
	.quad	4607571220725779792     # double 1.0863313699999999
	.quad	4613937818241073152     # double 3
	.quad	4611686018427387904     # double 2
	.quad	4624633867356078080     # double 15
	.quad	4610706493704986143     # double 1.78250182
	.quad	4600827511629557585     # double 0.397231787
	.quad	4609515441798594220     # double 1.5180351700000001
	.quad	4616189618054758400     # double 4
	.quad	4611686018427387904     # double 2
	.quad	4624633867356078080     # double 15
	.quad	4610706493704986143     # double 1.78250182
	.quad	4600734861668497077     # double 0.39208868099999999
	.quad	4611446414001880863     # double 1.94679713
	.quad	4617315517961601024     # double 5
	.quad	4611686018427387904     # double 2
	.quad	4623507967449235456     # double 13
	.quad	4595590160348752373     # double 0.178250194
	.quad	4600639378114608622     # double 0.38678827900000001
	.quad	4612523189161902087     # double 2.3717784900000001
	.quad	4618441417868443648     # double 6
	.quad	4611686018427387904     # double 2
	.quad	4623507967449235456     # double 13
	.quad	4595590160348752373     # double 0.178250194
	.quad	4600527443217526157     # double 0.38057464400000002
	.quad	4613469868659343474     # double 2.79218864
	.quad	4619567317775286272     # double 7
	.quad	4611686018427387904     # double 2
	.quad	4623507967449235456     # double 13
	.quad	4588284256684271473     # double 0.056367658100000002
	.quad	4600432599589115954     # double 0.37530976500000002
	.quad	4614404599589059490     # double 3.20729256
	.quad	4620693217682128896     # double 8
	.quad	4611686018427387904     # double 2
	.quad	4623507967449235456     # double 13
	.quad	4588284256684271473     # double 0.056367658100000002
	.quad	4600338525860069249     # double 0.370087624
	.quad	4615325864688335674     # double 3.61641645
	.quad	4621256167635550208     # double 9
	.quad	4611686018427387904     # double 2
	.quad	4622945017495814144     # double 12
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4600239103259788312     # double 0.36456856100000001
	.quad	4616210958676844669     # double 4.01895428
	.quad	4621819117588971520     # double 10
	.quad	4611686018427387904     # double 2
	.quad	4622945017495814144     # double 12
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4600138059868044679     # double 0.35895952599999997
	.quad	4616656154046637039     # double 4.4143672
	.quad	4622382067542392832     # double 11
	.quad	4611686018427387904     # double 2
	.quad	4622945017495814144     # double 12
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4600029590472861077     # double 0.35293826499999997
	.quad	4617092802428761590     # double 4.8021888700000002
	.quad	4622945017495814144     # double 12
	.quad	4611686018427387904     # double 2
	.quad	4621819117588971520     # double 10
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4599913661243197105     # double 0.3465029
	.quad	4617520457696639234     # double 5.1820230499999997
	.quad	4623507967449235456     # double 13
	.quad	4611686018427387904     # double 2
	.quad	4621819117588971520     # double 10
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4599790767737141359     # double 0.33968093999999999
	.quad	4617938751534633447     # double 5.55354261
	.quad	4624070917402656768     # double 14
	.quad	4611686018427387904     # double 2
	.quad	4621819117588971520     # double 10
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4599662695577902897     # double 0.33257150699999999
	.quad	4618347390296789524     # double 5.9164867399999999
	.quad	4624633867356078080     # double 15
	.quad	4611686018427387904     # double 2
	.quad	4621256167635550208     # double 9
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4599544595775348119     # double 0.32601565100000002
	.quad	4618746153317984724     # double 6.2706594500000001
	.quad	4625196817309499392     # double 16
	.quad	4611686018427387904     # double 2
	.quad	4621256167635550208     # double 9
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4599534262626291087     # double 0.32544204599999998
	.quad	4619134885437952884     # double 6.61592293
	.quad	4625478292286210048     # double 17
	.quad	4611686018427387904     # double 2
	.quad	4621256167635550208     # double 9
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4599820204903616117     # double 0.34131503099999999
	.quad	4619513494321642645     # double 6.9521951700000004
	.quad	4625759767262920704     # double 18
	.quad	4613937818241073152     # double 3
	.quad	4618441417868443648     # double 6
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600426739108964052     # double 0.374984443
	.quad	4619970885788254905     # double 7.3584404000000001
	.quad	4626041242239631360     # double 19
	.quad	4613937818241073152     # double 3
	.quad	4618441417868443648     # double 6
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600807809552123742     # double 0.39613810199999999
	.quad	4620500792943800190     # double 7.8290924999999998
	.quad	4626322717216342016     # double 20
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600746217567015086     # double 0.39271906000000001
	.quad	4620850706618920703     # double 8.2797565500000001
	.quad	4626604192193052672     # double 21
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600530692348484522     # double 0.38075500699999998
	.quad	4621093379785954835     # double 8.7108306899999999
	.quad	4626885667169763328     # double 22
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600289863331188405     # double 0.36738631100000002
	.quad	4621325320903224441     # double 9.12284088
	.quad	4627167142146473984     # double 23
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600055058004494684     # double 0.35435199699999997
	.quad	4621546880547813014     # double 9.5164098700000004
	.quad	4627448617123184640     # double 24
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4599823688131683114     # double 0.34150838900000002
	.quad	4621758447954577348     # double 9.8922290799999998
	.quad	4627730092099895296     # double 25
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4599680825160476449     # double 0.33357790100000001
	.quad	4621960439757148471     # double 10.251038599999999
	.quad	4628011567076605952     # double 26
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4599762445643919940     # double 0.33810874800000001
	.quad	4622153285919812308     # double 10.593602199999999
	.quad	4628293042053316608     # double 27
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599990070863713345     # double 0.35074448600000002
	.quad	4622366501525820770     # double 10.9723492
	.quad	4628574517030027264     # double 28
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4600058074695669082     # double 0.35451945699999998
	.quad	4622597519748566237     # double 11.382720000000001
	.quad	4628855992006737920     # double 29
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599891523907315156     # double 0.34527403099999998
	.quad	4622814974986968909     # double 11.768998099999999
	.quad	4629137466983448576     # double 30
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599685345621580824     # double 0.33382883699999999
	.quad	4623019843902902979     # double 12.132918399999999
	.quad	4629418941960159232     # double 31
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599642252063855201     # double 0.33143666399999999
	.quad	4623213054575661657     # double 12.476129500000001
	.quad	4629700416936869888     # double 32
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586582961079599862     # double 0.044562548399999999
	.quad	4599691537890924474     # double 0.33417257700000003
	.quad	4623416876573567349     # double 12.8381901
	.quad	4629841154425225216     # double 33
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586582961079599862     # double 0.044562548399999999
	.quad	4599688876623832668     # double 0.33402484700000001
	.quad	4623629580007708188     # double 13.2160273
	.quad	4629981891913580544     # double 34
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586582961079599862     # double 0.044562548399999999
	.quad	4599687101178758769     # double 0.33392629000000001
	.quad	4623828312938034951     # double 13.569047899999999
	.quad	4630122629401935872     # double 35
	.quad	4618441417868443648     # double 6
	.quad	0                       # double 0
	.quad	4587867388759778319     # double 0.053475055799999997
	.quad	4599692365202176021     # double 0.33421850199999997
	.quad	4624031733327443873     # double 13.9303951
	.quad	4630263366890291200     # double 36
	.quad	4618441417868443648     # double 6
	.quad	0                       # double 0
	.quad	4587867388759778319     # double 0.053475055799999997
	.quad	4599731754351049749     # double 0.33640503900000002
	.quad	4624238785138117329     # double 14.298192999999999
	.quad	4630404104378646528     # double 37
	.quad	4619567317775286272     # double 7
	.quad	0                       # double 0
	.quad	4589151816987594491     # double 0.062387566999999998
	.quad	4599743920375083127     # double 0.33708038899999998
	.quad	4624446159012459138     # double 14.666563
	.quad	4630544841867001856     # double 38
	.quad	4619567317775286272     # double 7
	.quad	0                       # double 0
	.quad	4589151816987594491     # double 0.062387566999999998
	.quad	4599717315738586805     # double 0.33560353500000001
	.quad	4624653396596617223     # double 15.034690899999999
	.quad	4630685579355357184     # double 39
	.quad	4620693217682128896     # double 8
	.quad	0                       # double 0
	.quad	4589802132486359946     # double 0.071300074500000005
	.quad	4599661693238755431     # double 0.33251586599999999
	.quad	4624857999687583288     # double 15.398139
	.quad	4630826316843712512     # double 40
	.quad	4620693217682128896     # double 8
	.quad	0                       # double 0
	.quad	4589802132486359946     # double 0.071300074500000005
	.quad	4599575435254660449     # double 0.32772758600000002
	.quad	4625060046197630885     # double 15.757045700000001
	.quad	4630967054332067840     # double 41
	.quad	4621256167635550208     # double 9
	.quad	0                       # double 0
	.quad	4590444346593062272     # double 0.080212585599999997
	.quad	4599478504279880554     # double 0.32234683600000003
	.quad	4625227313181048652     # double 16.108343099999999
	.quad	4631107791820423168     # double 42
	.quad	4621256167635550208     # double 9
	.quad	0                       # double 0
	.quad	4590444346593062272     # double 0.080212585599999997
	.quad	4599392545875232784     # double 0.31757518600000001
	.quad	4625324283871947761     # double 16.452852199999999
	.quad	4631248529308778496     # double 43
	.quad	4621819117588971520     # double 10
	.quad	0                       # double 0
	.quad	4591086560706970358     # double 0.089125096799999997
	.quad	4599370102528763355     # double 0.31632933000000002
	.quad	4625418791431620671     # double 16.788610500000001
	.quad	4631389266797133824     # double 44
	.quad	4622382067542392832     # double 11
	.quad	0                       # double 0
	.quad	4591728774280446489     # double 0.098037600500000002
	.quad	4599393043018588450     # double 0.317602783
	.quad	4625515446983135855     # double 17.132000000000001
	.quad	4631530004285489152     # double 45
	.quad	4622945017495814144     # double 12
	.quad	0                       # double 0
	.quad	4592370988415971853     # double 0.106950112
	.quad	4599435239999700256     # double 0.31994518599999999
	.quad	4625613288473170413     # double 17.479602799999999
	.quad	4631670741773844480     # double 46
	.quad	4623507967449235456     # double 13
	.quad	0                       # double 0
	.quad	4593013202515468420     # double 0.115862623
	.quad	4599452111708883485     # double 0.32088175400000002
	.quad	4625711569148611134     # double 17.8287659
	.quad	4631811479262199808     # double 47
	.quad	4624070917402656768     # double 14
	.quad	0                       # double 0
	.quad	4593655416614964987     # double 0.124775134
	.quad	4599442470042513240     # double 0.32034653400000002
	.quad	4625809726847634529     # double 18.177492099999998
	.quad	4631952216750555136     # double 48
	.quad	4624633867356078080     # double 15
	.quad	0                       # double 0
	.quad	4593984625316183737     # double 0.13368764499999999
	.quad	4599411523737817748     # double 0.31862866899999998
	.quad	4625907349321989710     # double 18.524316800000001
	.quad	4632092954238910464     # double 49
	.quad	4625196817309499392     # double 16
	.quad	0                       # double 0
	.quad	4594305732113730442     # double 0.14260014900000001
	.quad	4599366435175529193     # double 0.31612575100000001
	.quad	4626004136406761510     # double 18.868173599999999
	.quad	4632233691727265792     # double 50
	.quad	4625478292286210048     # double 17
	.quad	0                       # double 0
	.quad	4594626838911277146     # double 0.151512653
	.quad	4599323568707311180     # double 0.31374618399999998
	.quad	4626099865849308321     # double 19.208272900000001
	.quad	4632374429215621120     # double 51
	.quad	4625759767262920704     # double 18
	.quad	0                       # double 0
	.quad	4594947946213227009     # double 0.16042517100000001
	.quad	4599309618122918257     # double 0.31297177100000001
	.quad	4626194365330649399     # double 19.544002500000001
	.quad	4632515166703976448     # double 52
	.quad	4626322717216342016     # double 20
	.quad	0                       # double 0
	.quad	4595590160348752373     # double 0.178250194
	.quad	4599315151101305256     # double 0.31327891299999999
	.quad	4626289837617657486     # double 19.883188199999999
	.quad	4632655904192331776     # double 53
	.quad	4626604192193052672     # double 21
	.quad	0                       # double 0
	.quad	4600398893226367231     # double 0.37343868600000002
	.quad	4599323377574542995     # double 0.31373557400000002
	.quad	4626385837135444450     # double 20.224246999999998
	.quad	4632796641680687104     # double 54
	.quad	4627167142146473984     # double 23
	.quad	0                       # double 0
	.quad	4601039585847780085     # double 0.40900427099999997
	.quad	4599311656614239191     # double 0.31308492999999998
	.quad	4626481646038377186     # double 20.564628599999999
	.quad	4632937379169042432     # double 55
	.quad	4627448617123184640     # double 24
	.quad	0                       # double 0
	.quad	4601359931897277734     # double 0.42678704899999997
	.quad	4599284226810204743     # double 0.31156226999999997
	.quad	4626576928808283455     # double 20.903141000000002
	.quad	4633078116657397760     # double 56
	.quad	4628011567076605952     # double 26
	.quad	0                       # double 0
	.quad	4604775511070012247     # double 0.73277956200000005
	.quad	4599263933085880654     # double 0.31043574200000001
	.quad	4626671091673699884     # double 21.237674699999999
	.quad	4633218854145753088     # double 57
	.quad	4628574517030027264     # double 28
	.quad	0                       # double 0
	.quad	4605283225598982958     # double 0.78914719799999999
	.quad	4599279907137586155     # double 0.31132248000000001
	.quad	4626765493989879001     # double 21.573059099999998
	.quad	4633359591634108416     # double 58
	.quad	4629137466983448576     # double 30
	.quad	0                       # double 0
	.quad	4605790940668385625     # double 0.84551489400000002
	.quad	4599567793979158291     # double 0.32730341000000002
	.quad	4626859400234559164     # double 21.9066811
	.quad	4633500329122463744     # double 59
	.quad	4629841154425225216     # double 33
	.quad	0                       # double 0
	.quad	4613805250327477870     # double 2.9411280199999998
	.quad	4601141465702089709     # double 0.41465973900000003
	.quad	4626953535177657904     # double 22.241115600000001
	.quad	4670021707350671360     # double 16000
	.quad	4632937379169042432     # double 55
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	4624633867356078080     # double 15
	.quad	4628219162696934413     # double 26.737527799999999
	.quad	4604456607604540038     # double 0.69737416500000005
	.quad	4597702322164880921     # double 0.23687446100000001
	.quad	4607182418800017408     # double 1
	.quad	4613937818241073152     # double 3
	.quad	4624633867356078080     # double 15
	.quad	4628219162696934413     # double 26.737527799999999
	.quad	4601868618158932844     # double 0.45502480899999997
	.quad	4604571781445438511     # double 0.71016102999999997
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4624633867356078080     # double 15
	.quad	4613203174811861822     # double 2.6737527800000001
	.quad	4601443759991215367     # double 0.43144044300000001
	.quad	4608001771335339875     # double 1.1819328099999999
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4624633867356078080     # double 15
	.quad	4613203174811861822     # double 2.6737527800000001
	.quad	4601308216180051112     # double 0.42391624999999999
	.quad	4610114385195777421     # double 1.6510273200000001
	.quad	4616189618054758400     # double 4
	.quad	4613937818241073152     # double 3
	.quad	4623507967449235456     # double 13
	.quad	4598488224943554233     # double 0.26737528999999999
	.quad	4601205363530207579     # double 0.41820678100000003
	.quad	4611947960436830725     # double 2.11632562
	.quad	4617315517961601024     # double 5
	.quad	4613937818241073152     # double 3
	.quad	4623507967449235456     # double 13
	.quad	4598488224943554233     # double 0.26737528999999999
	.quad	4601086084234910392     # double 0.41158545000000002
	.quad	4612984783241319621     # double 2.5767674399999998
	.quad	4618441417868443648     # double 6
	.quad	4613937818241073152     # double 3
	.quad	4623507967449235456     # double 13
	.quad	4590756997662532927     # double 0.084551490800000004
	.quad	4600974822031497068     # double 0.40540915700000002
	.quad	4614008443600357584     # double 3.0313639600000002
	.quad	4619567317775286272     # double 7
	.quad	4613937818241073152     # double 3
	.quad	4623507967449235456     # double 13
	.quad	4590756997662532927     # double 0.084551490800000004
	.quad	4600871893685150997     # double 0.39969548599999999
	.quad	4615016901383313412     # double 3.4792091799999998
	.quad	4620693217682128896     # double 8
	.quad	4613937818241073152     # double 3
	.quad	4623507967449235456     # double 13
	.quad	4590756997662532927     # double 0.084551490800000004
	.quad	4600764847535536035     # double 0.39375323099999998
	.quad	4616008319977223070     # double 3.9194874799999999
	.quad	4621256167635550208     # double 9
	.quad	4613937818241073152     # double 3
	.quad	4622945017495814144     # double 12
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600649631802153828     # double 0.38735747300000001
	.quad	4616585348824842489     # double 4.3514795299999998
	.quad	4621819117588971520     # double 10
	.quad	4613937818241073152     # double 3
	.quad	4622945017495814144     # double 12
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600525280156639529     # double 0.38045456999999999
	.quad	4617061699894195025     # double 4.7745642699999999
	.quad	4622382067542392832     # double 11
	.quad	4613937818241073152     # double 3
	.quad	4621819117588971520     # double 10
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600391961682137549     # double 0.37305390799999999
	.quad	4617527434864584941     # double 5.1882200200000002
	.quad	4622945017495814144     # double 12
	.quad	4613937818241073152     # double 3
	.quad	4621819117588971520     # double 10
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600250264278543245     # double 0.365188122
	.quad	4617982075408695814     # double 5.5920219400000004
	.quad	4623507967449235456     # double 13
	.quad	4613937818241073152     # double 3
	.quad	4621819117588971520     # double 10
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600100918069428206     # double 0.35689774200000002
	.quad	4618425250002076383     # double 5.9856400499999998
	.quad	4624070917402656768     # double 14
	.quad	4613937818241073152     # double 3
	.quad	4621256167635550208     # double 9
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4599953232931970929     # double 0.34869957000000001
	.quad	4618856685366301261     # double 6.3688316299999999
	.quad	4624633867356078080     # double 15
	.quad	4613937818241073152     # double 3
	.quad	4621256167635550208     # double 9
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4599800866284686602     # double 0.34024152200000002
	.quad	4619276202742242238     # double 6.7414379100000001
	.quad	4625196817309499392     # double 16
	.quad	4613937818241073152     # double 3
	.quad	4618441417868443648     # double 6
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4599654011683058206     # double 0.33208945400000001
	.quad	4619683707092688177     # double 7.1033744800000003
	.quad	4625478292286210048     # double 17
	.quad	4613937818241073152     # double 3
	.quad	4618441417868443648     # double 6
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4599585614866998984     # double 0.32829266800000001
	.quad	4620079179097196676     # double 7.4546241799999997
	.quad	4625759767262920704     # double 18
	.quad	4613937818241073152     # double 3
	.quad	4618441417868443648     # double 6
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4599734812367254327     # double 0.33657479299999998
	.quad	4620462669207342562     # double 7.7952318199999997
	.quad	4626041242239631360     # double 19
	.quad	4616189618054758400     # double 4
	.quad	4613937818241073152     # double 3
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4600059534438409103     # double 0.35460048900000002
	.quad	4620793579257036442     # double 8.1782779699999999
	.quad	4626322717216342016     # double 20
	.quad	4616189618054758400     # double 4
	.quad	4613937818241073152     # double 3
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4600235046129013600     # double 0.36434334499999999
	.quad	4621030959416612020     # double 8.5999498400000007
	.quad	4626604192193052672     # double 21
	.quad	4616189618054758400     # double 4
	.quad	4613937818241073152     # double 3
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4600145442907144204     # double 0.359369367
	.quad	4621258214724242831     # double 9.0036363599999998
	.quad	4626885667169763328     # double 22
	.quad	4616189618054758400     # double 4
	.quad	4613937818241073152     # double 3
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599936592617736555     # double 0.34777584700000003
	.quad	4621475650664721101     # double 9.3898801800000004
	.quad	4627167142146473984     # double 23
	.quad	4616189618054758400     # double 4
	.quad	4613937818241073152     # double 3
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599716578084996639     # double 0.33556258700000002
	.quad	4621683611369353355     # double 9.7592926000000002
	.quad	4627448617123184640     # double 24
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599562122470046753     # double 0.32698857799999997
	.quad	4621882465108740123     # double 10.112527800000001
	.quad	4627730092099895296     # double 25
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599579733976533571     # double 0.32796621300000001
	.quad	4622072599034823371     # double 10.4502735
	.quad	4628011567076605952     # double 26
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586582961079599862     # double 0.044562548399999999
	.quad	4599696549046229849     # double 0.33445075200000002
	.quad	4622276015652467605     # double 10.811614000000001
	.quad	4628293042053316608     # double 27
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586582961079599862     # double 0.044562548399999999
	.quad	4599710553313530330     # double 0.33522814499999998
	.quad	4622491013163963631     # double 11.1935263
	.quad	4628574517030027264     # double 28
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586582961079599862     # double 0.044562548399999999
	.quad	4599609086853637703     # double 0.329595625
	.quad	4622694464684504977     # double 11.554928800000001
	.quad	4628855992006737920     # double 29
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4586582961079599862     # double 0.044562548399999999
	.quad	4599556621683389891     # double 0.32668322300000002
	.quad	4622887114884290028     # double 11.897144300000001
	.quad	4629137466983448576     # double 30
	.quad	4618441417868443648     # double 6
	.quad	0                       # double 0
	.quad	4587867388759778319     # double 0.053475055799999997
	.quad	4599562085954860975     # double 0.32698655100000001
	.quad	4623086895295595122     # double 12.2520256
	.quad	4629418941960159232     # double 31
	.quad	4618441417868443648     # double 6
	.quad	0                       # double 0
	.quad	4587867388759778319     # double 0.053475055799999997
	.quad	4599527600055045551     # double 0.32507219900000001
	.quad	4623292581277091588     # double 12.6173973
	.quad	4629700416936869888     # double 32
	.quad	4618441417868443648     # double 6
	.quad	0                       # double 0
	.quad	4587867388759778319     # double 0.053475055799999997
	.quad	4599500363527492711     # double 0.32356026799999998
	.quad	4623485914954415088     # double 12.960826900000001
	.quad	4629841154425225216     # double 33
	.quad	4619567317775286272     # double 7
	.quad	0                       # double 0
	.quad	4589151816987594491     # double 0.062387566999999998
	.quad	4599481160719113559     # double 0.32249429800000001
	.quad	4623682136452409648     # double 13.3093863
	.quad	4629981891913580544     # double 34
	.quad	4619567317775286272     # double 7
	.quad	0                       # double 0
	.quad	4589151816987594491     # double 0.062387566999999998
	.quad	4599497538509518454     # double 0.32340344799999998
	.quad	4623880484437558262     # double 13.6617231
	.quad	4630122629401935872     # double 35
	.quad	4620693217682128896     # double 8
	.quad	0                       # double 0
	.quad	4589802132486359946     # double 0.071300074500000005
	.quad	4599494456336005474     # double 0.32323235299999997
	.quad	4624078498537089502     # double 14.0134668
	.quad	4630263366890291200     # double 36
	.quad	4620693217682128896     # double 8
	.quad	0                       # double 0
	.quad	4589802132486359946     # double 0.071300074500000005
	.quad	4599484188669287025     # double 0.322662383
	.quad	4624275819025983132     # double 14.363978400000001
	.quad	4630404104378646528     # double 37
	.quad	4621256167635550208     # double 9
	.quad	0                       # double 0
	.quad	4590444346593062272     # double 0.080212585599999997
	.quad	4599509262153967636     # double 0.32405424100000002
	.quad	4624470525456768049     # double 14.709846499999999
	.quad	4630544841867001856     # double 38
	.quad	4621819117588971520     # double 10
	.quad	0                       # double 0
	.quad	4591086560706970358     # double 0.089125096799999997
	.quad	4599494392438933961     # double 0.32322880599999998
	.quad	4624672516178475262     # double 15.0686541
	.quad	4630685579355357184     # double 39
	.quad	4621819117588971520     # double 10
	.quad	0                       # double 0
	.quad	4591086560706970358     # double 0.089125096799999997
	.quad	4599449771494401915     # double 0.32075184600000001
	.quad	4624869801708176784     # double 15.4191036
	.quad	4630826316843712512     # double 40
	.quad	4622382067542392832     # double 11
	.quad	0                       # double 0
	.quad	4591728774280446489     # double 0.098037600500000002
	.quad	4599415030582761191     # double 0.31882333800000001
	.quad	4625061374421750987     # double 15.7594051
	.quad	4630967054332067840     # double 41
	.quad	4622945017495814144     # double 12
	.quad	0                       # double 0
	.quad	4592370988415971853     # double 0.106950112
	.quad	4599407740948317938     # double 0.31841868200000001
	.quad	4625226247488639328     # double 16.104557
	.quad	4631107791820423168     # double 42
	.quad	4623507967449235456     # double 13
	.quad	0                       # double 0
	.quad	4593013202515468420     # double 0.115862623
	.quad	4599413927849370831     # double 0.31876212399999998
	.quad	4625323879617586209     # double 16.451415999999998
	.quad	4631248529308778496     # double 43
	.quad	4624070917402656768     # double 14
	.quad	0                       # double 0
	.quad	4593655416614964987     # double 0.124775134
	.quad	4599396710930268966     # double 0.31780639300000002
	.quad	4625421304524655237     # double 16.797538800000002
	.quad	4631389266797133824     # double 44
	.quad	4624633867356078080     # double 15
	.quad	0                       # double 0
	.quad	4593984625316183737     # double 0.13368764499999999
	.quad	4599357932739720688     # double 0.31565377100000003
	.quad	4625518008912078880     # double 17.141101800000001
	.quad	4631530004285489152     # double 45
	.quad	4625196817309499392     # double 16
	.quad	0                       # double 0
	.quad	4594305732113730442     # double 0.14260014900000001
	.quad	4599316774612942126     # double 0.31336903599999999
	.quad	4625613629930464661     # double 17.4808159
	.quad	4631670741773844480     # double 46
	.quad	4625478292286210048     # double 17
	.quad	0                       # double 0
	.quad	4594626838911277146     # double 0.151512653
	.quad	4599301369095653584     # double 0.31251385799999998
	.quad	4625707925398742619     # double 17.8158207
	.quad	4631811479262199808     # double 47
	.quad	4626041242239631360     # double 19
	.quad	0                       # double 0
	.quad	4595269053010773713     # double 0.16933767499999999
	.quad	4599306255699407665     # double 0.312785119
	.quad	4625803209238253799     # double 18.154336900000001
	.quad	4631952216750555136     # double 48
	.quad	4626322717216342016     # double 20
	.quad	0                       # double 0
	.quad	4595590160348752373     # double 0.178250194
	.quad	4599318012094033337     # double 0.31343773000000003
	.quad	4625899057350650790     # double 18.494857799999998
	.quad	4632092954238910464     # double 49
	.quad	4626885667169763328     # double 22
	.quad	0                       # double 0
	.quad	4596232373907816985     # double 0.196075201
	.quad	4599314788183232884     # double 0.31325876699999999
	.quad	4625994802893566269     # double 18.835014300000001
	.quad	4632233691727265792     # double 50
	.quad	4627167142146473984     # double 23
	.quad	0                       # double 0
	.quad	4596553481245795645     # double 0.20498772000000001
	.quad	4599302388080105273     # double 0.31257042299999999
	.quad	4626090238701417375     # double 19.174070400000002
	.quad	4632374429215621120     # double 51
	.quad	4627730092099895296     # double 25
	.quad	0                       # double 0
	.quad	4597195694804860256     # double 0.22281272699999999
	.quad	4599302430485999365     # double 0.31257277700000002
	.quad	4626184912106146562     # double 19.5104179
	.quad	4632515166703976448     # double 52
	.quad	4628293042053316608     # double 27
	.quad	0                       # double 0
	.quad	4597837908904356824     # double 0.24063774900000001
	.quad	4599310988748428851     # double 0.31304785600000001
	.quad	4626280430583198328     # double 19.849767700000001
	.quad	4632655904192331776     # double 53
	.quad	4628855992006737920     # double 29
	.quad	0                       # double 0
	.quad	4602820241712548759     # double 0.51570105600000005
	.quad	4599346681000454861     # double 0.31502917400000002
	.quad	4626376215335578062     # double 20.190063500000001
	.quad	4632796641680687104     # double 54
	.quad	4629418941960159232     # double 31
	.quad	0                       # double 0
	.quad	4603140587753039208     # double 0.55126661099999996
	.quad	4599627425295147573     # double 0.33061361299999997
	.quad	4626471756865430420     # double 20.529495199999999
	.quad	4632937379169042432     # double 55
	.quad	4629841154425225216     # double 33
	.quad	0                       # double 0
	.quad	4603460934324954414     # double 0.58683222499999999
	.quad	4601205232007084061     # double 0.41819948000000001
	.quad	4626566598338868201     # double 20.866439799999998
	.quad	4676829883349860352     # double 48000
	.quad	4630404104378646528     # double 37
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	4616788596805198676     # double 4.532
	.quad	4607182418800017408     # double 1
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4606317727671562273     # double 0.90400000000000003
	.quad	4607083339608215257     # double 0.98899999999999999
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4611123068473966592     # double 1.875
	.quad	4611686018427387904     # double 2
	.quad	4607182418800017408     # double 1
	.quad	4584015902316823577     # double 0.029000000000000001
	.quad	4607083339608215257     # double 0.98899999999999999
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4615626668101337088     # double 3.75
	.quad	4613937818241073152     # double 3
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4607011282014177329     # double 0.98099999999999998
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4617808662120798093     # double 5.4379999999999997
	.quad	4616189618054758400     # double 4
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4607047310811196293     # double 0.98499999999999998
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4619406314088607777     # double 6.8570000000000002
	.quad	4617315517961601024     # double 5
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4607038303611941552     # double 0.98399999999999999
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4620754579227051819     # double 8.109
	.quad	4618441417868443648     # double 6
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4607002274814922588     # double 0.97999999999999998
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4621389586774511059     # double 9.2370000000000001
	.quad	4619567317775286272     # double 7
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4606894188423865696     # double 0.96799999999999997
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4621932833479562625     # double 10.202
	.quad	4620693217682128896     # double 8
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4606768087634299322     # double 0.95399999999999996
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4622428792388526801     # double 11.083
	.quad	4621256167635550208     # double 9
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4606542907652930798     # double 0.92900000000000005
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4622869019252102267     # double 11.865
	.quad	4621819117588971520     # double 10
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4606335742070071755     # double 0.90600000000000003
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4623256891770009551     # double 12.554
	.quad	4622382067542392832     # double 11
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4606128576487212712     # double 0.88300000000000001
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4623617742690152612     # double 13.195
	.quad	4622945017495814144     # double 12
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4605777295716277813     # double 0.84399999999999997
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4623947631362857501     # double 13.781000000000001
	.quad	4623507967449235456     # double 13
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4605308921355031282     # double 0.79200000000000004
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4624244868938263953     # double 14.308999999999999
	.quad	4624070917402656768     # double 14
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4604903597388567937     # double 0.747
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4624522966215254082     # double 14.803000000000001
	.quad	4624633867356078080     # double 15
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4604381179831792959     # double 0.68899999999999995
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4624774604844433408     # double 15.25
	.quad	4625196817309499392     # double 16
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4603975855865329615     # double 0.64400000000000002
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4625009354975010095     # double 15.667
	.quad	4625478292286210048     # double 17
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4603507481504083083     # double 0.59199999999999997
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4625215957607915717     # double 16.068000000000001
	.quad	4625759767262920704     # double 18
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4603156200733148185     # double 0.55300000000000005
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4625311940574974050     # double 16.408999999999999
	.quad	4626041242239631360     # double 19
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605831338911806259     # double 0.84999999999999998
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4625490958660162028     # double 17.045000000000002
	.quad	4626322717216342016     # double 20
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605480058140871361     # double 0.81100000000000005
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4625649147597073416     # double 17.606999999999999
	.quad	4626604192193052672     # double 21
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4604804518196765786     # double 0.73599999999999999
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4625787070335661638     # double 18.097000000000001
	.quad	4626885667169763328     # double 22
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4604165007049679176     # double 0.66500000000000004
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4625908386050623930     # double 18.527999999999999
	.quad	4627167142146473984     # double 23
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4603669611090668421     # double 0.60999999999999999
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4626021820466238325     # double 18.931000000000001
	.quad	4627448617123184640     # double 24
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4603075135939855516     # double 0.54400000000000004
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4626124277357761004     # double 19.295000000000002
	.quad	4627730092099895296     # double 25
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4602931020751779660     # double 0.52800000000000002
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4626220260324819337     # double 19.635999999999999
	.quad	4628011567076605952     # double 26
	.quad	4613937818241073152     # double 3
	.quad	4587943041191890649     # double 0.053999999999999999
	.quad	4603768690282470572     # double 0.621
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4626333413265457021     # double 20.038
	.quad	4628293042053316608     # double 27
	.quad	4613937818241073152     # double 3
	.quad	4587943041191890649     # double 0.053999999999999999
	.quad	4604237064643717104     # double 0.67300000000000004
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4626459514055023395     # double 20.486000000000001
	.quad	4628574517030027264     # double 28
	.quad	4613937818241073152     # double 3
	.quad	4587943041191890649     # double 0.053999999999999999
	.quad	4603894791072036946     # double 0.63500000000000001
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4626576044695381606     # double 20.899999999999999
	.quad	4628855992006737920     # double 29
	.quad	4616189618054758400     # double 4
	.quad	4592878986383488713     # double 0.114
	.quad	4603813726278744277     # double 0.626
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4626690323535926133     # double 21.306000000000001
	.quad	4629137466983448576     # double 30
	.quad	4616189618054758400     # double 4
	.quad	4592878986383488713     # double 0.114
	.quad	4603903798271291687     # double 0.63600000000000001
	.quad	-4604908100988195308    # double -6.0199999999999996
	.quad	4626807417126237766     # double 21.722000000000001
	.quad	4629418941960159232     # double 31
	.quad	4617315517961601024     # double 5
	.quad	4601814128044191777     # double 0.45200000000000001
	.quad	4603714647086942126     # double 0.61499999999999999
	.quad	-4604908100988195308    # double -6.0199999999999996
	.quad	4626921695966782292     # double 22.128
	.quad	4629700416936869888     # double 32
	.quad	4617315517961601024     # double 5
	.quad	4601814128044191777     # double 0.45200000000000001
	.quad	4603390387913771450     # double 0.57899999999999996
	.quad	-4604908100988195308    # double -6.0199999999999996
	.quad	4627030063832815895     # double 22.513000000000002
	.quad	4629841154425225216     # double 33
	.quad	4617315517961601024     # double 5
	.quad	4601814128044191777     # double 0.45200000000000001
	.quad	4603138186334638703     # double 0.55100000000000005
	.quad	-4604908100988195308    # double -6.0199999999999996
	.quad	4627132520724338573     # double 22.876999999999999
	.quad	4629981891913580544     # double 34
	.quad	4619567317775286272     # double 7
	.quad	4618812964837701714     # double 6.3300000000000001
	.quad	4603147193533893444     # double 0.55200000000000005
	.quad	-4605798687814507823    # double -5.2290000000000001
	.quad	4627234977615861252     # double 23.241
	.quad	4630122629401935872     # double 35
	.quad	4619567317775286272     # double 7
	.quad	4618812964837701714     # double 6.3300000000000001
	.quad	4603210243928676631     # double 0.55900000000000005
	.quad	-4605798687814507823    # double -5.2290000000000001
	.quad	4627340530732127748     # double 23.616
	.quad	4630263366890291200     # double 36
	.quad	4622382067542392832     # double 11
	.quad	4621789281241440190     # double 9.9469999999999992
	.quad	4602931020751779660     # double 0.52800000000000002
	.quad	-4605798687814507823    # double -5.2290000000000001
	.quad	4627441298773790163     # double 23.974
	.quad	4630404104378646528     # double 37
	.quad	4625478292286210048     # double 17
	.quad	4639612804532340261     # double 153.727
	.quad	4602300516803947790     # double 0.47899999999999998
	.quad	-4605798687814507823    # double -5.2290000000000001
	.quad	4627536718790895075     # double 24.312999999999999
	.quad	4676293871431319552     # double 44100
	.quad	4630544841867001856     # double 38
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	4616788596805198676     # double 4.532
	.quad	4607182418800017408     # double 1
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4606317727671562273     # double 0.90400000000000003
	.quad	4607029296412686811     # double 0.98299999999999998
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4610438521330606277     # double 1.7230000000000001
	.quad	4611686018427387904     # double 2
	.quad	4607182418800017408     # double 1
	.quad	4584015902316823577     # double 0.029000000000000001
	.quad	4607029296412686811     # double 0.98299999999999998
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4614939869158163087     # double 3.4449999999999998
	.quad	4613937818241073152     # double 3
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4607020289213432070     # double 0.98199999999999998
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4617379694256291054     # double 5.0570000000000004
	.quad	4616189618054758400     # double 4
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4607047310811196293     # double 0.98499999999999998
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4618916547629131235     # double 6.4219999999999997
	.quad	4617315517961601024     # double 5
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4607029296412686811     # double 0.98299999999999998
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4620252990818553430     # double 7.609
	.quad	4618441417868443648     # double 6
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4606984260416413106     # double 0.97799999999999998
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4621092912149058028     # double 8.7100000000000008
	.quad	4619567317775286272     # double 7
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4606885181224610955     # double 0.96699999999999997
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4621628840504715117     # double 9.6620000000000008
	.quad	4620693217682128896     # double 8
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4606714044438770876     # double 0.94799999999999995
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4622109036814983496     # double 10.515000000000001
	.quad	4621256167635550208     # double 9
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4606551914852185539     # double 0.93000000000000005
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4622547011878745276     # double 11.292999999999999
	.quad	4621819117588971520     # double 10
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4606407799664109683     # double 0.91400000000000003
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4622950084045394936     # double 12.009
	.quad	4622382067542392832     # double 11
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4606011482896901079     # double 0.87
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4623296861216702464     # double 12.625
	.quad	4622945017495814144     # double 12
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4605786302915532554     # double 0.84499999999999997
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4623626186939453932     # double 13.210000000000001
	.quad	4623507967449235456     # double 13
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4605380978949069210     # double 0.80000000000000004
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4623929054014394597     # double 13.747999999999999
	.quad	4624070917402656768     # double 14
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4604921611787077419     # double 0.749
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4624206588341431304     # double 14.241
	.quad	4624633867356078080     # double 15
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4604489266222849851     # double 0.70099999999999996
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4624462167620284580     # double 14.695
	.quad	4625196817309499392     # double 16
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4604056920658622284     # double 0.65300000000000002
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4624704236100255744     # double 15.125
	.quad	4625478292286210048     # double 17
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4603489467105573601     # double 0.58999999999999997
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4624919845932416106     # double 15.507999999999999
	.quad	4625759767262920704     # double 18
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4603723654286196867     # double 0.61599999999999999
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4625135455764576469     # double 15.891
	.quad	4626041242239631360     # double 19
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605921410904353669     # double 0.85999999999999999
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4625347969371993014     # double 16.536999999999999
	.quad	4626322717216342016     # double 20
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605588144531928252     # double 0.82299999999999995
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4625509817483601641     # double 17.111999999999998
	.quad	4626604192193052672     # double 21
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605038705377389052     # double 0.76200000000000001
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4625653088246747365     # double 17.620999999999999
	.quad	4626885667169763328     # double 22
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4604372172632538218     # double 0.68799999999999994
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4625780314936220582     # double 18.073
	.quad	4627167142146473984     # double 23
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4603687625489177903     # double 0.61199999999999999
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4625892060501974712     # double 18.469999999999999
	.quad	4627448617123184640     # double 24
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4603525495902592565     # double 0.59399999999999997
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4625998739518148051     # double 18.849
	.quad	4627730092099895296     # double 25
	.quad	4613937818241073152     # double 3
	.quad	4583439441564520153     # double 0.027
	.quad	4604101956654895989     # double 0.65800000000000003
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4626117521958319948     # double 19.271000000000001
	.quad	4628011567076605952     # double 26
	.quad	4613937818241073152     # double 3
	.quad	4583439441564520153     # double 0.027
	.quad	4604534302219123556     # double 0.70599999999999996
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4626249815197373956     # double 19.741
	.quad	4628293042053316608     # double 27
	.quad	4613937818241073152     # double 3
	.quad	4587943041191890649     # double 0.053999999999999999
	.quad	4604119971053405471     # double 0.66000000000000003
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4626372538287219802     # double 20.177
	.quad	4628574517030027264     # double 28
	.quad	4613937818241073152     # double 3
	.quad	4587943041191890649     # double 0.053999999999999999
	.quad	4603633582293649457     # double 0.60599999999999998
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4626484846802927354     # double 20.576000000000001
	.quad	4628855992006737920     # double 29
	.quad	4613937818241073152     # double 3
	.quad	4587943041191890649     # double 0.053999999999999999
	.quad	4603264287124205076     # double 0.56499999999999995
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4626590118444217139     # double 20.949999999999999
	.quad	4629137466983448576     # double 30
	.quad	4616189618054758400     # double 4
	.quad	4592878986383488713     # double 0.114
	.quad	4603219251127931372     # double 0.56000000000000005
	.quad	-4604908100988195308    # double -6.0199999999999996
	.quad	4626693138285693239     # double 21.315999999999999
	.quad	4629418941960159232     # double 31
	.quad	4616189618054758400     # double 4
	.quad	4592878986383488713     # double 0.114
	.quad	4603390387913771450     # double 0.57899999999999996
	.quad	-4604908100988195308    # double -6.0199999999999996
	.quad	4626800943201773421     # double 21.699000000000002
	.quad	4629700416936869888     # double 32
	.quad	4617315517961601024     # double 5
	.quad	4601814128044191777     # double 0.45200000000000001
	.quad	4603282301522714558     # double 0.56699999999999995
	.quad	-4604908100988195308    # double -6.0199999999999996
	.quad	4626907622217946759     # double 22.077999999999999
	.quad	4629841154425225216     # double 33
	.quad	4617315517961601024     # double 5
	.quad	4601814128044191777     # double 0.45200000000000001
	.quad	4602985063947308106     # double 0.53400000000000003
	.quad	-4604908100988195308    # double -6.0199999999999996
	.quad	4627008953209562595     # double 22.437999999999999
	.quad	4629981891913580544     # double 34
	.quad	4617315517961601024     # double 5
	.quad	4601814128044191777     # double 0.45200000000000001
	.quad	4602804919962213286     # double 0.51400000000000001
	.quad	-4605798687814507823    # double -5.2290000000000001
	.quad	4627105780601551061     # double 22.782
	.quad	4630122629401935872     # double 35
	.quad	4619567317775286272     # double 7
	.quad	4618812964837701714     # double 6.3300000000000001
	.quad	4602858963157741732     # double 0.52000000000000002
	.quad	-4605798687814507823    # double -5.2290000000000001
	.quad	4627204578318376501     # double 23.132999999999999
	.quad	4630263366890291200     # double 36
	.quad	4619567317775286272     # double 7
	.quad	4618812964837701714     # double 6.3300000000000001
	.quad	4602840948759232250     # double 0.51800000000000002
	.quad	-4605798687814507823    # double -5.2290000000000001
	.quad	4627303376035201942     # double 23.484000000000002
	.quad	4630404104378646528     # double 37
	.quad	4619567317775286272     # double 7
	.quad	4618812964837701714     # double 6.3300000000000001
	.quad	4602741869567430099     # double 0.50700000000000001
	.quad	-4605798687814507823    # double -5.2290000000000001
	.quad	4627400203427190407     # double 23.827999999999999
	.quad	4630544841867001856     # double 38
	.quad	4626041242239631360     # double 19
	.quad	4640249149085938876     # double 171.81299999999999
	.quad	4601724056051644367     # double 0.44700000000000001
	.quad	-4606553040752092381    # double -4.5590000000000002
	.quad	4627497312294155583     # double 24.172999999999998
	.quad	4674525306978041856     # double 32000
	.quad	4630967054332067840     # double 41
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	4616788596805198676     # double 4.532
	.quad	4607182418800017408     # double 1
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4606317727671562273     # double 0.90400000000000003
	.quad	4607047310811196293     # double 0.98499999999999998
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4608308318706860032     # double 1.25
	.quad	4611686018427387904     # double 2
	.quad	4607182418800017408     # double 1
	.quad	4591149604126578442     # double 0.089999999999999996
	.quad	4607029296412686811     # double 0.98299999999999998
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4612811918334230528     # double 2.5
	.quad	4613937818241073152     # double 3
	.quad	4607182418800017408     # double 1
	.quad	4584015902316823577     # double 0.029000000000000001
	.quad	4607029296412686811     # double 0.98299999999999998
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4615626668101337088     # double 3.75
	.quad	4616189618054758400     # double 4
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4607011282014177329     # double 0.98099999999999998
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4617213061070078345     # double 4.9089999999999998
	.quad	4617315517961601024     # double 5
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4606957238818648883     # double 0.97499999999999997
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4618394130072356258     # double 5.9580000000000002
	.quad	4618441417868443648     # double 6
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4606813123630573027     # double 0.95899999999999996
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4619406314088607777     # double 6.8570000000000002
	.quad	4619567317775286272     # double 7
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4606678015641751912     # double 0.94399999999999995
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4620355447710076109     # double 7.7000000000000001
	.quad	4620693217682128896     # double 8
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4606578936449949762     # double 0.93300000000000005
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4620974692658839552     # double 8.5
	.quad	4621256167635550208     # double 9
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4606461842859638129     # double 0.92000000000000004
	.quad	-4602543711183825797    # double -8.2400000000000002
	.quad	4621389586774511059     # double 9.2370000000000001
	.quad	4621819117588971520     # double 10
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4606209641280505381     # double 0.89200000000000002
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4621760007843862282     # double 9.8949999999999996
	.quad	4622382067542392832     # double 11
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4605948432502117892     # double 0.86299999999999998
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4622100592565682176     # double 10.5
	.quad	4622945017495814144     # double 12
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4605732259720004108     # double 0.83899999999999996
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4622428792388526801     # double 11.083
	.quad	4623507967449235456     # double 13
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4605254878159502836     # double 0.78600000000000003
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4622722089314259304     # double 11.603999999999999
	.quad	4624070917402656768     # double 14
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4604975654982605865     # double 0.755
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4623005253140830224     # double 12.106999999999999
	.quad	4624633867356078080     # double 15
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4604462244625085628     # double 0.69799999999999995
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4623256891770009551     # double 12.554
	.quad	4625196817309499392     # double 16
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4604237064643717104     # double 0.67300000000000004
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4623507967449235456     # double 13
	.quad	4625478292286210048     # double 17
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4603624575094394716     # double 0.60499999999999998
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4623728080881023189     # double 13.391
	.quad	4625759767262920704     # double 18
	.quad	4607182418800017408     # double 1
	.quad	4576341768551784251     # double 0.0089999999999999993
	.quad	4603840747876508500     # double 0.629
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4623947631362857501     # double 13.781000000000001
	.quad	4626041242239631360     # double 19
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4606128576487212712     # double 0.88300000000000001
	.quad	-4603301441821130883    # double -7.4470000000000001
	.quad	4624337755680578470     # double 14.474
	.quad	4626322717216342016     # double 20
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605903396505844187     # double 0.85799999999999998
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4624687910551606526     # double 15.096
	.quad	4626604192193052672     # double 21
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605642187727456698     # double 0.82899999999999996
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4625009354975010095     # double 15.667
	.quad	4626885667169763328     # double 22
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4605083741373662757     # double 0.76700000000000002
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4625246638380377178     # double 16.177
	.quad	4627167142146473984     # double 23
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4604525295019868815     # double 0.70499999999999996
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4625375835394687369     # double 16.635999999999999
	.quad	4627448617123184640     # double 24
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4603912805470546428     # double 0.63700000000000001
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4625494336359882555     # double 17.056999999999999
	.quad	4627730092099895296     # double 25
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4603255279924950335     # double 0.56399999999999995
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4625599045051218919     # double 17.428999999999998
	.quad	4628011567076605952     # double 26
	.quad	4611686018427387904     # double 2
	.quad	4580845368179154747     # double 0.017999999999999999
	.quad	4603129179135383962     # double 0.55000000000000004
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4625699531617904624     # double 17.786000000000001
	.quad	4628293042053316608     # double 27
	.quad	4613937818241073152     # double 3
	.quad	4583439441564520153     # double 0.027
	.quad	4603606560695885234     # double 0.60299999999999998
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4625809588333798490     # double 18.177
	.quad	4628574517030027264     # double 28
	.quad	4613937818241073152     # double 3
	.quad	4583439441564520153     # double 0.027
	.quad	4603894791072036946     # double 0.63500000000000001
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4625927807824016966     # double 18.597000000000001
	.quad	4628855992006737920     # double 29
	.quad	4613937818241073152     # double 3
	.quad	4583439441564520153     # double 0.027
	.quad	4603507481504083083     # double 0.59199999999999997
	.quad	-4603815978078557962    # double -6.9900000000000002
	.quad	4626039553389771096     # double 18.994
	.quad	4629137466983448576     # double 30
	.quad	4613937818241073152     # double 3
	.quad	4583439441564520153     # double 0.027
	.quad	4602976056748053365     # double 0.53300000000000003
	.quad	-4604908100988195308    # double -6.0199999999999996
	.quad	4626140321431433511     # double 19.352
	.quad	4629418941960159232     # double 31
	.quad	4613937818241073152     # double 3
	.quad	4583439441564520153     # double 0.027
	.quad	4602840948759232250     # double 0.51800000000000002
	.quad	-4604908100988195308    # double -6.0199999999999996
	.quad	4626236304398491845     # double 19.693000000000001
	.quad	4629700416936869888     # double 32
	.quad	4616189618054758400     # double 4
	.quad	4589852567433895739     # double 0.071999999999999995
	.quad	4603291308721969299     # double 0.56799999999999995
	.quad	-4604908100988195308    # double -6.0199999999999996
	.quad	4626341294564804919     # double 20.065999999999999
	.quad	4629841154425225216     # double 33
	.quad	4616189618054758400     # double 4
	.quad	4589852567433895739     # double 0.071999999999999995
	.quad	4603525495902592565     # double 0.59399999999999997
	.quad	-4604908100988195308    # double -6.0199999999999996
	.quad	4626452758655582339     # double 20.462
	.quad	4629981891913580544     # double 34
	.quad	4616189618054758400     # double 4
	.quad	4589852567433895739     # double 0.071999999999999995
	.quad	4603291308721969299     # double 0.56799999999999995
	.quad	-4605798687814507823    # double -5.2290000000000001
	.quad	4626559437671755678     # double 20.841000000000001
	.quad	4630122629401935872     # double 35
	.quad	4617315517961601024     # double 5
	.quad	4594320138264247271     # double 0.14299999999999999
	.quad	4603003078345817588     # double 0.53600000000000003
	.quad	-4605798687814507823    # double -5.2290000000000001
	.quad	4626660768663371514     # double 21.201000000000001
	.quad	4630263366890291200     # double 36
	.quad	4617315517961601024     # double 5
	.quad	4594320138264247271     # double 0.14299999999999999
	.quad	4602876977556251214     # double 0.52200000000000002
	.quad	-4605798687814507823    # double -5.2290000000000001
	.quad	4626758721955266822     # double 21.548999999999999
	.quad	4630404104378646528     # double 37
	.quad	4618441417868443648     # double 6
	.quad	4595364973377797226     # double 0.17199999999999999
	.quad	4603057121541346034     # double 0.54200000000000004
	.quad	-4605798687814507823    # double -5.2290000000000001
	.quad	4626860615896836080     # double 21.911000000000001
	.quad	4630544841867001856     # double 38
	.quad	4619567317775286272     # double 7
	.quad	4603876776673527464     # double 0.63300000000000001
	.quad	4603030099943581811     # double 0.53900000000000003
	.quad	-4606553040752092381    # double -4.5590000000000002
	.quad	4626963072788358758     # double 22.274999999999999
	.quad	4630685579355357184     # double 39
	.quad	4619567317775286272     # double 7
	.quad	4603876776673527464     # double 0.63300000000000001
	.quad	4602849955958486991     # double 0.51900000000000002
	.quad	-4606553040752092381    # double -4.5590000000000002
	.quad	4627061589030207488     # double 22.625
	.quad	4630826316843712512     # double 40
	.quad	4620693217682128896     # double 8
	.quad	4604687424606454153     # double 0.72299999999999998
	.quad	4602804919962213286     # double 0.51400000000000001
	.quad	-4607227454796291113    # double -3.98
	.quad	4627158979372149375     # double 22.971
	.quad	4630967054332067840     # double 41
	.quad	4621819117588971520     # double 10
	.quad	4621280374483547324     # double 9.0429999999999992
	.quad	4602840948759232250     # double 0.51800000000000002
	.quad	-4607227454796291113    # double -3.98
	.quad	4627257495613998105     # double 23.321000000000002
	.quad	4672326283722489856     # double 24000
	.quad	4631389266797133824     # double 44
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	4621206914558657426     # double 8.9125089600000003
	.quad	4606928866483270021     # double 0.97185003800000002
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4621206914558657426     # double 8.9125089600000003
	.quad	4606054068610718321     # double 0.87472796399999997
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4606701192408427858     # double 0.94657313799999998
	.quad	4611686018427387904     # double 2
	.quad	4607182418800017408     # double 1
	.quad	4606202894059601248     # double 0.89125090799999995
	.quad	4605901590832609589     # double 0.85779952999999998
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4611167033379140919     # double 1.8847621699999999
	.quad	4613937818241073152     # double 3
	.quad	4607182418800017408     # double 1
	.quad	4591086560706970358     # double 0.089125096799999997
	.quad	4605738959752191345     # double 0.83974385299999998
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4613500171264544225     # double 2.8056456999999999
	.quad	4616189618054758400     # double 4
	.quad	4607182418800017408     # double 1
	.quad	4583780657042489458     # double 0.028183829000000001
	.quad	4605584564800728441     # double 0.82260257000000003
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4615517086852973881     # double 3.7013361499999999
	.quad	4617315517961601024     # double 5
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4605382651946258785     # double 0.80018573999999998
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4616826111801353671     # double 4.5653200099999998
	.quad	4618441417868443648     # double 6
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4605124050355738777     # double 0.77147519600000003
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4617757581224219546     # double 5.3926310500000003
	.quad	4619567317775286272     # double 7
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4604817038041600290     # double 0.73738998200000005
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4618643930410980685     # double 6.1798672699999999
	.quad	4620693217682128896     # double 8
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4604490266193103913     # double 0.70111101899999995
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4619482965151603639     # double 6.9250798199999997
	.quad	4621256167635550208     # double 9
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4604117956413148163     # double 0.65977633000000002
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4620273909689793595     # double 7.6275796900000001
	.quad	4621819117588971520     # double 10
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4603714984325489423     # double 0.61503744100000002
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4620855180359682544     # double 8.2877035100000001
	.quad	4622382067542392832     # double 11
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4603297235945467679     # double 0.56865805400000002
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4621203572538704946     # double 8.9065723400000003
	.quad	4622945017495814144     # double 12
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4602879325426852150     # double 0.52226066599999998
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4621529688872512959     # double 9.4858713199999993
	.quad	4623507967449235456     # double 13
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4602298771478948199     # double 0.47890311499999999
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4621834686870653312     # double 10.0276566
	.quad	4624070917402656768     # double 14
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4601563484350242129     # double 0.43808648
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4622119844778549241     # double 10.5341988
	.quad	4624633867356078080     # double 15
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4601102660670087651     # double 0.41250562699999999
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4622386495650431449     # double 11.007865900000001
	.quad	4625196817309499392     # double 16
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4600709988990316652     # double 0.39070797000000002
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4622635974184344502     # double 11.4510288
	.quad	4625478292286210048     # double 17
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4600370942662100671     # double 0.37188711800000002
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4622869584453855502     # double 11.866004
	.quad	4625759767262920704     # double 18
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4600294030529938009     # double 0.367617637
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4623088574631601173     # double 12.255008699999999
	.quad	4626041242239631360     # double 19
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4601277669849045727     # double 0.42222058800000001
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4623294123197014009     # double 12.6201363
	.quad	4626322717216342016     # double 20
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4603264197448529296     # double 0.56499004399999997
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4623664021848808457     # double 13.2772083
	.quad	4626604192193052672     # double 21
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4602856269500761406     # double 0.519700944
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4623998323317313230     # double 13.871047000000001
	.quad	4626885667169763328     # double 22
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4601874668690989021     # double 0.45536068099999999
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4624297470475256683     # double 14.4024391
	.quad	4627167142146473984     # double 23
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4601037129656600912     # double 0.40886792500000002
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4624566971112393100     # double 14.8811684
	.quad	4627448617123184640     # double 24
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4600544810989201141     # double 0.38153874900000001
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4624811383715970311     # double 15.315332400000001
	.quad	4627730092099895296     # double 25
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4600199272812194298     # double 0.36235752700000001
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625034471831606917     # double 15.7116165
	.quad	4628011567076605952     # double 26
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4600260132493989145     # double 0.36573591799999999
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625218080069977603     # double 16.075540499999999
	.quad	4628293042053316608     # double 27
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600528620566555142     # double 0.38063999999999998
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625334235870109329     # double 16.488208799999999
	.quad	4628574517030027264     # double 28
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600502380469427058     # double 0.37918338200000001
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625461713184901809     # double 16.9410992
	.quad	4628855992006737920     # double 29
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600168921883369545     # double 0.36067271200000001
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625577183903087719     # double 17.3513336
	.quad	4629137466983448576     # double 30
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4599851747142628701     # double 0.34306597700000002
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625682767615684183     # double 17.726442299999999
	.quad	4629418941960159232     # double 31
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4599783729889946073     # double 0.33929026099999998
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625780102872973128     # double 18.0722466
	.quad	4629700416936869888     # double 32
	.quad	4616189618054758400     # double 4
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599849909980225508     # double 0.34296399399999999
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625884364272424001     # double 18.442657499999999
	.quad	4629841154425225216     # double 33
	.quad	4616189618054758400     # double 4
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599852872430045994     # double 0.34312844300000001
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625994632178992894     # double 18.834407800000001
	.quad	4629981891913580544     # double 34
	.quad	4616189618054758400     # double 4
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599868365983700052     # double 0.34398850800000003
	.quad	4598175219545276416     # double 0.25
	.quad	4626096292974838942     # double 19.195579500000001
	.quad	4630122629401935872     # double 35
	.quad	4617315517961601024     # double 5
	.quad	4586582961079599862     # double 0.044562548399999999
	.quad	4599867283120191248     # double 0.343928397
	.quad	4598175219545276416     # double 0.25
	.quad	4626201599124960872     # double 19.569702100000001
	.quad	4630263366890291200     # double 36
	.quad	4617315517961601024     # double 5
	.quad	4586582961079599862     # double 0.044562548399999999
	.quad	4599788007138656172     # double 0.33952769599999999
	.quad	4598175219545276416     # double 0.25
	.quad	4626310084112732284     # double 19.955118200000001
	.quad	4630404104378646528     # double 37
	.quad	4617315517961601024     # double 5
	.quad	4591071347396108153     # double 0.088913969699999998
	.quad	4599734213748791857     # double 0.33654156299999999
	.quad	4598715651518575274     # double 0.280000001
	.quad	4626410422595432740     # double 20.311592099999999
	.quad	4630544841867001856     # double 38
	.quad	4618441417868443648     # double 6
	.quad	4592352732624522344     # double 0.106696762
	.quad	4599705635580895625     # double 0.33495515599999998
	.quad	4598715651518575274     # double 0.280000001
	.quad	4626512367934332745     # double 20.673774699999999
	.quad	4630685579355357184     # double 39
	.quad	4618441417868443648     # double 6
	.quad	4595260597304314942     # double 0.16910298200000001
	.quad	4599717280304264937     # double 0.33560156800000002
	.quad	4599075939686923297     # double 0.30000001199999998
	.quad	4626615591028889528     # double 21.0404968
	.quad	4630826316843712512     # double 40
	.quad	4619567317775286272     # double 7
	.quad	4596276026362256365     # double 0.19728680000000001
	.quad	4599701343290162771     # double 0.33471688599999999
	.quad	4599075939686923297     # double 0.30000001199999998
	.quad	4626718476972719207     # double 21.4060211
	.quad	4630967054332067840     # double 41
	.quad	4619567317775286272     # double 7
	.quad	4596276026362256365     # double 0.19728680000000001
	.quad	4599646575483468680     # double 0.33167666200000001
	.quad	4599075939686923297     # double 0.30000001199999998
	.quad	4626820840020484650     # double 21.769687699999999
	.quad	4631107791820423168     # double 42
	.quad	4620693217682128896     # double 8
	.quad	4604597359585478966     # double 0.71300077399999995
	.quad	4599590267932090787     # double 0.32855096499999997
	.quad	4600877379429785105     # double 0.40000000600000002
	.quad	4626921336326204549     # double 22.126722300000001
	.quad	4631248529308778496     # double 43
	.quad	4620693217682128896     # double 8
	.quad	4604597359585478966     # double 0.71300077399999995
	.quad	4599782859632368479     # double 0.33924195200000001
	.quad	4600877379429785105     # double 0.40000000600000002
	.quad	4627019909594883560     # double 22.4769249
	.quad	4631389266797133824     # double 44
	.quad	4621256167635550208     # double 9
	.quad	4605400127092756085     # double 0.80212587099999999
	.quad	4601331479343782309     # double 0.42520761499999998
	.quad	4600877379429785105     # double 0.40000000600000002
	.quad	4627115487660187895     # double 22.816486399999999
	.quad	4671790271803949056     # double 22050
	.quad	4631389266797133824     # double 44
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	4621206914558657426     # double 8.9125089600000003
	.quad	4606768499380398854     # double 0.95404571299999996
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4621206914558657426     # double 8.9125089600000003
	.quad	4605681654149092832     # double 0.833381653
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4606010146669884439     # double 0.869851649
	.quad	4611686018427387904     # double 2
	.quad	4607182418800017408     # double 1
	.quad	4606202894059601248     # double 0.89125090799999995
	.quad	4605524607054830967     # double 0.81594592300000002
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4610484724119471441     # double 1.7332590800000001
	.quad	4613937818241073152     # double 3
	.quad	4607182418800017408     # double 1
	.quad	4591086560706970358     # double 0.089125096799999997
	.quad	4605329140409673550     # double 0.79424476600000005
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4612999317415663058     # double 2.5832219099999998
	.quad	4616189618054758400     # double 4
	.quad	4607182418800017408     # double 1
	.quad	4583780657042489458     # double 0.028183829000000001
	.quad	4605169189925796712     # double 0.776486695
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4614868778712145137     # double 3.4134294999999999
	.quad	4617315517961601024     # double 5
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4604978000529349394     # double 0.75526040800000005
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4616435632172140101     # double 4.2185044300000003
	.quad	4618441417868443648     # double 6
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4604760120063321704     # double 0.73107081699999998
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4617308929476821158     # double 4.9941482500000003
	.quad	4619567317775286272     # double 7
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4604496254449384443     # double 0.70177584900000001
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4618145516634895626     # double 5.7371869100000001
	.quad	4620693217682128896     # double 8
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4604190912871628518     # double 0.66787612399999996
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4618943042136854070     # double 6.4455318500000001
	.quad	4621256167635550208     # double 9
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4603852313759862734     # double 0.63028407099999995
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4619700259892974592     # double 7.1180763200000001
	.quad	4621819117588971520     # double 10
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4603491001247779466     # double 0.59017032400000002
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4620416872508055798     # double 7.7545561799999998
	.quad	4622382067542392832     # double 11
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4603118267345832407     # double 0.54878854799999999
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4620893285310500226     # double 8.3553914999999996
	.quad	4622945017495814144     # double 12
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4602749034911530836     # double 0.50779551300000003
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4621211992288424293     # double 8.9215288200000007
	.quad	4623507967449235456     # double 13
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4602129663042322694     # double 0.469515711
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4621511916829587925     # double 9.4543018300000003
	.quad	4624070917402656768     # double 14
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4601459095162277729     # double 0.43229171599999999
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4621793955521863939     # double 9.9553031900000004
	.quad	4624633867356078080     # double 15
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4601077900707998700     # double 0.41113117300000002
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4622059094597275733     # double 10.426284799999999
	.quad	4625196817309499392     # double 16
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4600711134129601102     # double 0.39077153799999997
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4622308363770101109     # double 10.869075799999999
	.quad	4625478292286210048     # double 17
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4600396719446913490     # double 0.373318017
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4622542801857518615     # double 11.2855215
	.quad	4625759767262920704     # double 18
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4600329029677981367     # double 0.36956048000000002
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4622763431120348668     # double 11.6774378
	.quad	4626041242239631360     # double 19
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4601344989692304458     # double 0.42595759
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4622971239366874537     # double 12.046579400000001
	.quad	4626322717216342016     # double 20
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4603371473380804446     # double 0.57690006500000002
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4623347031823206329     # double 12.714120899999999
	.quad	4626604192193052672     # double 21
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4602977090153031060     # double 0.53311473099999995
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4623687963097017549     # double 13.319736499999999
	.quad	4626885667169763328     # double 22
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4602137802001612473     # double 0.46996751399999998
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4623994069160810220     # double 13.8634901
	.quad	4627167142146473984     # double 23
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4601188461754993238     # double 0.41726854400000002
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4624270451917422208     # double 14.3544445
	.quad	4627448617123184640     # double 24
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4600684609477034162     # double 0.389299124
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4624521422944251772     # double 14.800258599999999
	.quad	4627730092099895296     # double 25
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4600207691481049734     # double 0.362824857
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4624750607807883932     # double 15.207372700000001
	.quad	4628011567076605952     # double 26
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4599919036397438763     # double 0.34680128100000002
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4624961044524037320     # double 15.5811834
	.quad	4628293042053316608     # double 27
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4599965851531738061     # double 0.34940004299999999
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625155281736036061     # double 15.926218
	.quad	4628574517030027264     # double 28
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600229346175166817     # double 0.36402693400000002
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625286746397201126     # double 16.3194923
	.quad	4628855992006737920     # double 29
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600257764357189886     # double 0.36560446000000002
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625408740663889775     # double 16.752903
	.quad	4629137466983448576     # double 30
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600053675561538668     # double 0.35427525599999998
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625519692019849619     # double 17.147081400000001
	.quad	4629418941960159232     # double 31
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4599998626441997453     # double 0.35121941600000001
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625621456426634594     # double 17.5086212
	.quad	4629700416936869888     # double 32
	.quad	4616189618054758400     # double 4
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4600055275978716649     # double 0.35436409699999999
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625729878589191204     # double 17.8938141
	.quad	4629841154425225216     # double 33
	.quad	4616189618054758400     # double 4
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599957130076872478     # double 0.34891590500000003
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625844009289455487     # double 18.299287799999998
	.quad	4629981891913580544     # double 34
	.quad	4616189618054758400     # double 4
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599754169775201488     # double 0.33764934499999999
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625948749055629280     # double 18.671398199999999
	.quad	4630122629401935872     # double 35
	.quad	4616189618054758400     # double 4
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599653785115968152     # double 0.33207687699999999
	.quad	4598175219545276416     # double 0.25
	.quad	4626045646197116975     # double 19.015646
	.quad	4630263366890291200     # double 36
	.quad	4617315517961601024     # double 5
	.quad	4586582961079599862     # double 0.044562548399999999
	.quad	4599630658879680026     # double 0.33079311300000003
	.quad	4598175219545276416     # double 0.25
	.quad	4626146345446295082     # double 19.373401600000001
	.quad	4630404104378646528     # double 37
	.quad	4617315517961601024     # double 5
	.quad	4586582961079599862     # double 0.044562548399999999
	.quad	4599571844660778336     # double 0.32752826800000001
	.quad	4598175219545276416     # double 0.25
	.quad	4626250388899671488     # double 19.743038200000001
	.quad	4630544841867001856     # double 38
	.quad	4617315517961601024     # double 5
	.quad	4591071347396108153     # double 0.088913969699999998
	.quad	4599535499566950342     # double 0.32551070999999998
	.quad	4598715651518575274     # double 0.280000001
	.quad	4626346912917930055     # double 20.085960400000001
	.quad	4630685579355357184     # double 39
	.quad	4618441417868443648     # double 6
	.quad	4592352732624522344     # double 0.106696762
	.quad	4599516154498866160     # double 0.32443684299999997
	.quad	4598715651518575274     # double 0.280000001
	.quad	4626445299343519525     # double 20.435499199999999
	.quad	4630826316843712512     # double 40
	.quad	4618441417868443648     # double 6
	.quad	4592352732624522344     # double 0.106696762
	.quad	4599541342338948509     # double 0.32583504899999999
	.quad	4598715651518575274     # double 0.280000001
	.quad	4626545239482832941     # double 20.7905579
	.quad	4630967054332067840     # double 41
	.quad	4619567317775286272     # double 7
	.quad	4596276026362256365     # double 0.19728680000000001
	.quad	4599548309857932014     # double 0.32622182399999999
	.quad	4599075939686923297     # double 0.30000001199999998
	.quad	4626645232764621960     # double 21.1458054
	.quad	4631107791820423168     # double 42
	.quad	4619567317775286272     # double 7
	.quad	4596276026362256365     # double 0.19728680000000001
	.quad	4599543611162368786     # double 0.325960994
	.quad	4599075939686923297     # double 0.30000001199999998
	.quad	4626745097187166641     # double 21.500595100000002
	.quad	4631248529308778496     # double 43
	.quad	4620693217682128896     # double 8
	.quad	4597291455924600946     # double 0.225470632
	.quad	4599778850275778614     # double 0.33901938799999998
	.quad	4599075939686923297     # double 0.30000001199999998
	.quad	4626843573262536194     # double 21.850452400000002
	.quad	4631389266797133824     # double 44
	.quad	4620693217682128896     # double 8
	.quad	4604597359585478966     # double 0.71300077399999995
	.quad	4601361072208703385     # double 0.42685034900000002
	.quad	4600877379429785105     # double 0.40000000600000002
	.quad	4626940584767306926     # double 22.195106500000001
	.quad	4670021707350671360     # double 16000
	.quad	4631530004285489152     # double 45
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	4621206914558657426     # double 8.9125089600000003
	.quad	4605693884070197724     # double 0.83473944700000002
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4621206914558657426     # double 8.9125089600000003
	.quad	4603793523175851889     # double 0.62375700499999998
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4603863432895141133     # double 0.63151854299999999
	.quad	4611686018427387904     # double 2
	.quad	4607182418800017408     # double 1
	.quad	4606202894059601248     # double 0.89125090799999995
	.quad	4603617404282924032     # double 0.60420388000000003
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4608356468491916101     # double 1.2606914
	.quad	4613937818241073152     # double 3
	.quad	4607182418800017408     # double 1
	.quad	4606202894059601248     # double 0.89125090799999995
	.quad	4603507249640759868     # double 0.59197425800000003
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4611167033379140919     # double 1.8847621699999999
	.quad	4616189618054758400     # double 4
	.quad	4607182418800017408     # double 1
	.quad	4591086560706970358     # double 0.089125096799999997
	.quad	4603357075579961325     # double 0.57530158799999997
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4612814440012251883     # double 2.5011198499999998
	.quad	4617315517961601024     # double 5
	.quad	4607182418800017408     # double 1
	.quad	4583780657042489458     # double 0.028183829000000001
	.quad	4603233191543196336     # double 0.56154769699999996
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4614179572820150288     # double 3.1073605999999998
	.quad	4618441417868443648     # double 6
	.quad	4607182418800017408     # double 1
	.quad	4583780657042489458     # double 0.028183829000000001
	.quad	4603099147754967169     # double 0.54666584699999998
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4615517086852973881     # double 3.7013361499999999
	.quad	4619567317775286272     # double 7
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4602947814044286177     # double 0.52986443000000005
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4616506207068590708     # double 4.2811875300000004
	.quad	4620693217682128896     # double 8
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4602779550654087552     # double 0.51118344100000002
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4617141414485801410     # double 4.8453650499999998
	.quad	4621256167635550208     # double 9
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4602514930191816747     # double 0.490902334
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4617757581224219546     # double 5.3926310500000003
	.quad	4621819117588971520     # double 10
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4602127248194188099     # double 0.46938165999999998
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4618353661480457644     # double 5.9220566699999999
	.quad	4622382067542392832     # double 11
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4601724124182099530     # double 0.44700378200000002
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4618928931019400629     # double 6.43299866
	.quad	4622945017495814144     # double 12
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4601384855592234739     # double 0.42817059200000002
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4619482965151603639     # double 6.9250798199999997
	.quad	4623507967449235456     # double 13
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4601139247877431612     # double 0.41453662499999999
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4620015602299171044     # double 7.3981566399999998
	.quad	4624070917402656768     # double 14
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4600895989528424498     # double 0.40103307399999999
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4620526908496132380     # double 7.8522877700000002
	.quad	4624633867356078080     # double 15
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4600657436306120880     # double 0.38779070999999998
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4620855180359682544     # double 8.2877035100000001
	.quad	4625196817309499392     # double 16
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4600413156270502301     # double 0.374230444
	.quad	4594572340059552801     # double 0.15000000599999999
	.quad	4621089969046701541     # double 8.7047720000000002
	.quad	4625478292286210048     # double 17
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4600166666498690556     # double 0.36054751299999999
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4621314699450607764     # double 9.1039733900000001
	.quad	4625759767262920704     # double 18
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4599945254480971869     # double 0.34825667700000001
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4621529688872512959     # double 9.4858713199999993
	.quad	4626041242239631360     # double 19
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4599982550374753193     # double 0.35032701500000002
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4621735290028710443     # double 9.8510923399999992
	.quad	4626322717216342016     # double 20
	.quad	4607182418800017408     # double 1
	.quad	4576291333602807306     # double 0.0089125093100000004
	.quad	4600991425308128106     # double 0.40633082399999998
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4621931878716441622     # double 10.200303999999999
	.quad	4626604192193052672     # double 21
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4603166092871715305     # double 0.55409824799999996
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4622295671050091310     # double 10.846529
	.quad	4626885667169763328     # double 22
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4602933835537575563     # double 0.52831250399999996
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4622632448034721257     # double 11.4447651
	.quad	4627167142146473984     # double 23
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4602255967286505834     # double 0.476527005
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4622940986661557657     # double 11.9928398
	.quad	4627448617123184640     # double 24
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4601385487483291256     # double 0.42820566900000001
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4623224209710463677     # double 12.495945000000001
	.quad	4627730092099895296     # double 25
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4600918306702046767     # double 0.402271926
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4623484814330961154     # double 12.958871800000001
	.quad	4628011567076605952     # double 26
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4600481502628231298     # double 0.378024429
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4623725248792397517     # double 13.3859692
	.quad	4628293042053316608     # double 27
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4600202685694035115     # double 0.36254698000000002
	.quad	4595653204006150517     # double 0.18000000699999999
	.quad	4623947709838081008     # double 13.781139400000001
	.quad	4628574517030027264     # double 28
	.quad	4611686018427387904     # double 2
	.quad	4580794933224413194     # double 0.017825018599999999
	.quad	4600301974069032758     # double 0.36805859200000002
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4624154151185824974     # double 14.1478529
	.quad	4628855992006737920     # double 29
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600624515911579126     # double 0.385963261
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4624390354515411423     # double 14.5674343
	.quad	4629137466983448576     # double 30
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600632476636410053     # double 0.38640517000000002
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4624651028997998119     # double 15.030485199999999
	.quad	4629418941960159232     # double 31
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4600297929278121025     # double 0.36783406099999999
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4624887950088775180     # double 15.451341599999999
	.quad	4629700416936869888     # double 32
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4599971013017198998     # double 0.34968656300000001
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625104649454275395     # double 15.836277000000001
	.quad	4629841154425225216     # double 33
	.quad	4613937818241073152     # double 3
	.quad	4583363789132407823     # double 0.026737527899999999
	.quad	4599899366511735056     # double 0.34570938299999998
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625250429763870978     # double 16.190469700000001
	.quad	4629981891913580544     # double 34
	.quad	4616189618054758400     # double 4
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599953585653893744     # double 0.34871914999999998
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625356794091020354     # double 16.568351700000001
	.quad	4630122629401935872     # double 35
	.quad	4616189618054758400     # double 4
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599923594958995986     # double 0.34705433200000002
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625468729539793773     # double 16.966026299999999
	.quad	4630263366890291200     # double 36
	.quad	4616189618054758400     # double 4
	.quad	4585298532851783690     # double 0.035650037199999998
	.quad	4599910546319507635     # double 0.34632998700000001
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625571305185609126     # double 17.330448199999999
	.quad	4630404104378646528     # double 37
	.quad	4617315517961601024     # double 5
	.quad	4586582961079599862     # double 0.044562548399999999
	.quad	4599880435378499825     # double 0.34465849399999998
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625676889433008046     # double 17.705558799999999
	.quad	4630544841867001856     # double 38
	.quad	4617315517961601024     # double 5
	.quad	4586582961079599862     # double 0.044562548399999999
	.quad	4599774524712451713     # double 0.33877927099999999
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625785094690947603     # double 18.089981099999999
	.quad	4630685579355357184     # double 39
	.quad	4617315517961601024     # double 5
	.quad	4586582961079599862     # double 0.044562548399999999
	.quad	4599704257965798410     # double 0.33487868300000001
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625884757239638987     # double 18.4440536
	.quad	4630826316843712512     # double 40
	.quad	4618441417868443648     # double 6
	.quad	4587867388759778319     # double 0.053475055799999997
	.quad	4599667019519933933     # double 0.33281153400000002
	.quad	4596373779802414609     # double 0.20000000300000001
	.quad	4625985793132889240     # double 18.803005200000001
	.quad	4630967054332067840     # double 41
	.quad	4618441417868443648     # double 6
	.quad	4587867388759778319     # double 0.053475055799999997
	.quad	4599683341483717847     # double 0.33371758499999998
	.quad	4598175219545276416     # double 0.25
	.quad	4626088115366783060     # double 19.1665268
	.quad	4631107791820423168     # double 42
	.quad	4619567317775286272     # double 7
	.quad	4589151816987594491     # double 0.062387566999999998
	.quad	4599688184060281971     # double 0.33398640200000002
	.quad	4598175219545276416     # double 0.25
	.quad	4626190405906594503     # double 19.529935800000001
	.quad	4631248529308778496     # double 43
	.quad	4619567317775286272     # double 7
	.quad	4589151816987594491     # double 0.062387566999999998
	.quad	4599690998323660318     # double 0.33414262500000003
	.quad	4598175219545276416     # double 0.25
	.quad	4626292737260277569     # double 19.893489800000001
	.quad	4631389266797133824     # double 44
	.quad	4620693217682128896     # double 8
	.quad	4594293561766241421     # double 0.14226235400000001
	.quad	4599918549990664608     # double 0.34677427999999999
	.quad	4598715651518575274     # double 0.280000001
	.quad	4626394090995071523     # double 20.2535706
	.quad	4631530004285489152     # double 45
	.quad	4620693217682128896     # double 8
	.quad	4594293561766241421     # double 0.14226235400000001
	.quad	4601530481828057570     # double 0.436254472
	.quad	4598715651518575274     # double 0.280000001
	.quad	4626494577111397265     # double 20.610569000000002
	.quad	4676829883349860352     # double 48000
	.quad	4626322717216342016     # double 20
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4616189618054758400     # double 4
	.quad	4607182418800017408     # double 1
	.quad	4588231271568042361     # double 0.056000000000000001
	.quad	4607182418800017408     # double 1
	.quad	4613937818241073152     # double 3
	.quad	4616189618054758400     # double 4
	.quad	4619567317775286272     # double 7
	.quad	4606678015641751912     # double 0.94399999999999995
	.quad	4603678618289923162     # double 0.61099999999999999
	.quad	4611686018427387904     # double 2
	.quad	4616189618054758400     # double 4
	.quad	4619567317775286272     # double 7
	.quad	4622382067542392832     # double 11
	.quad	4600679220938094412     # double 0.38900000000000001
	.quad	4595184829392702407     # double 0.16700000000000001
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4622382067542392832     # double 11
	.quad	4624070917402656768     # double 14
	.quad	4605678216524475662     # double 0.83299999999999996
	.quad	4604678417407199412     # double 0.72199999999999998
	.quad	4616189618054758400     # double 4
	.quad	4613937818241073152     # double 3
	.quad	4624070917402656768     # double 14
	.quad	4625478292286210048     # double 17
	.quad	4598679622703541912     # double 0.27800000000000002
	.quad	4603930819869055910     # double 0.63900000000000001
	.quad	4617315517961601024     # double 5
	.quad	4611686018427387904     # double 2
	.quad	4625478292286210048     # double 17
	.quad	4626041242239631360     # double 19
	.quad	4600174817779828916     # double 0.36099999999999999
	.quad	4601183624096359907     # double 0.41699999999999998
	.quad	4618441417868443648     # double 6
	.quad	4613937818241073152     # double 3
	.quad	4626041242239631360     # double 19
	.quad	4626885667169763328     # double 22
	.quad	4603426416710790414     # double 0.58299999999999996
	.quad	4590645200968312947     # double 0.083000000000000004
	.quad	4619567317775286272     # double 7
	.quad	4611686018427387904     # double 2
	.quad	4626885667169763328     # double 22
	.quad	4627448617123184640     # double 24
	.quad	4606434821261873906     # double 0.91700000000000004
	.quad	4604930618986332160     # double 0.75
	.quad	4620693217682128896     # double 8
	.quad	4613937818241073152     # double 3
	.quad	4627448617123184640     # double 24
	.quad	4628293042053316608     # double 27
	.quad	4598175219545276416     # double 0.25
	.quad	4601183624096359907     # double 0.41699999999999998
	.quad	4621256167635550208     # double 9
	.quad	4613937818241073152     # double 3
	.quad	4628293042053316608     # double 27
	.quad	4629137466983448576     # double 30
	.quad	4603426416710790414     # double 0.58299999999999996
	.quad	4604011884662348579     # double 0.64800000000000002
	.quad	4621819117588971520     # double 10
	.quad	4613937818241073152     # double 3
	.quad	4629137466983448576     # double 30
	.quad	4629841154425225216     # double 33
	.quad	4600012688193243578     # double 0.35199999999999998
	.quad	4603678618289923162     # double 0.61099999999999999
	.quad	4622382067542392832     # double 11
	.quad	4613937818241073152     # double 3
	.quad	4629841154425225216     # double 33
	.quad	4630263366890291200     # double 36
	.quad	4600679220938094412     # double 0.38900000000000001
	.quad	4603804719079489536     # double 0.625
	.quad	4622945017495814144     # double 12
	.quad	4616189618054758400     # double 4
	.quad	4630263366890291200     # double 36
	.quad	4630826316843712512     # double 40
	.quad	4600427019358961664     # double 0.375
	.quad	4594356167061266235     # double 0.14399999999999999
	.quad	4623507967449235456     # double 13
	.quad	4613937818241073152     # double 3
	.quad	4630826316843712512     # double 40
	.quad	4631248529308778496     # double 43
	.quad	4605885382107334705     # double 0.85599999999999998
	.quad	4600679220938094412     # double 0.38900000000000001
	.quad	4624070917402656768     # double 14
	.quad	4613937818241073152     # double 3
	.quad	4631248529308778496     # double 43
	.quad	4631670741773844480     # double 46
	.quad	4603678618289923162     # double 0.61099999999999999
	.quad	4594932627813569659     # double 0.16
	.quad	4624633867356078080     # double 15
	.quad	4613937818241073152     # double 3
	.quad	4631670741773844480     # double 46
	.quad	4632092954238910464     # double 49
	.quad	4605741266919258849     # double 0.83999999999999997
	.quad	4596986269243650605     # double 0.217
	.quad	4625196817309499392     # double 16
	.quad	4613937818241073152     # double 3
	.quad	4632092954238910464     # double 49
	.quad	4632515166703976448     # double 52
	.quad	4605227856561738613     # double 0.78300000000000003
	.quad	4595797318942024794     # double 0.184
	.quad	4625478292286210048     # double 17
	.quad	4611686018427387904     # double 2
	.quad	4632515166703976448     # double 52
	.quad	4632796641680687104     # double 54
	.quad	4605525094137145065     # double 0.81599999999999995
	.quad	4606155598084976935     # double 0.88600000000000001
	.quad	4625759767262920704     # double 18
	.quad	4613937818241073152     # double 3
	.quad	4632796641680687104     # double 54
	.quad	4633218854145753088     # double 57
	.quad	4592878986383488713     # double 0.114
	.quad	4599310126651373781     # double 0.313
	.quad	4626041242239631360     # double 19
	.quad	4611686018427387904     # double 2
	.quad	4633218854145753088     # double 57
	.quad	4633500329122463744     # double 59
	.quad	4604363165433283478     # double 0.68700000000000006
	.quad	4601814128044191777     # double 0.45200000000000001
	.quad	4626322717216342016     # double 20
	.quad	4607182418800017408     # double 1
	.quad	4633500329122463744     # double 59
	.quad	4633641066610819072     # double 60
	.quad	4603111164736874480     # double 0.54800000000000004
	.quad	4606353756468581237     # double 0.90800000000000003
	.quad	4676293871431319552     # double 44100
	.quad	4626322717216342016     # double 20
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4616189618054758400     # double 4
	.quad	4607182418800017408     # double 1
	.quad	4588231271568042361     # double 0.056000000000000001
	.quad	4607182418800017408     # double 1
	.quad	4613937818241073152     # double 3
	.quad	4616189618054758400     # double 4
	.quad	4619567317775286272     # double 7
	.quad	4606678015641751912     # double 0.94399999999999995
	.quad	4603678618289923162     # double 0.61099999999999999
	.quad	4611686018427387904     # double 2
	.quad	4616189618054758400     # double 4
	.quad	4619567317775286272     # double 7
	.quad	4622382067542392832     # double 11
	.quad	4600679220938094412     # double 0.38900000000000001
	.quad	4595184829392702407     # double 0.16700000000000001
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4622382067542392832     # double 11
	.quad	4624070917402656768     # double 14
	.quad	4605678216524475662     # double 0.83299999999999996
	.quad	4604678417407199412     # double 0.72199999999999998
	.quad	4616189618054758400     # double 4
	.quad	4613937818241073152     # double 3
	.quad	4624070917402656768     # double 14
	.quad	4625478292286210048     # double 17
	.quad	4598679622703541912     # double 0.27800000000000002
	.quad	4594176023076171416     # double 0.13900000000000001
	.quad	4617315517961601024     # double 5
	.quad	4607182418800017408     # double 1
	.quad	4625478292286210048     # double 17
	.quad	4625759767262920704     # double 18
	.quad	4605930418103608410     # double 0.86099999999999999
	.quad	4606434821261873906     # double 0.91700000000000004
	.quad	4618441417868443648     # double 6
	.quad	4613937818241073152     # double 3
	.quad	4625759767262920704     # double 18
	.quad	4626604192193052672     # double 21
	.quad	4590645200968312947     # double 0.083000000000000004
	.quad	4603426416710790414     # double 0.58299999999999996
	.quad	4619567317775286272     # double 7
	.quad	4613937818241073152     # double 3
	.quad	4626604192193052672     # double 21
	.quad	4627448617123184640     # double 24
	.quad	4601183624096359907     # double 0.41699999999999998
	.quad	4598175219545276416     # double 0.25
	.quad	4620693217682128896     # double 8
	.quad	4613937818241073152     # double 3
	.quad	4627448617123184640     # double 24
	.quad	4628293042053316608     # double 27
	.quad	4604930618986332160     # double 0.75
	.quad	4605426014945342915     # double 0.80500000000000005
	.quad	4621256167635550208     # double 9
	.quad	4613937818241073152     # double 3
	.quad	4628293042053316608     # double 27
	.quad	4629137466983448576     # double 30
	.quad	4596157606912214434     # double 0.19400000000000001
	.quad	4603345351917497745     # double 0.57399999999999995
	.quad	4621819117588971520     # double 10
	.quad	4613937818241073152     # double 3
	.quad	4629137466983448576     # double 30
	.quad	4629841154425225216     # double 33
	.quad	4601345753682945245     # double 0.42599999999999999
	.quad	4603012085545072329     # double 0.53700000000000003
	.quad	4622382067542392832     # double 11
	.quad	4613937818241073152     # double 3
	.quad	4629841154425225216     # double 33
	.quad	4630263366890291200     # double 36
	.quad	4602012286427796079     # double 0.46300000000000002
	.quad	4605552115734909288     # double 0.81899999999999995
	.quad	4622945017495814144     # double 12
	.quad	4616189618054758400     # double 4
	.quad	4630263366890291200     # double 36
	.quad	4630826316843712512     # double 40
	.quad	4595653203753948938     # double 0.17999999999999999
	.quad	4591870180066957722     # double 0.10000000000000001
	.quad	4623507967449235456     # double 13
	.quad	4613937818241073152     # double 3
	.quad	4630826316843712512     # double 40
	.quad	4631248529308778496     # double 43
	.quad	4606281698874543309     # double 0.90000000000000002
	.quad	4602102358420343489     # double 0.46800000000000003
	.quad	4624070917402656768     # double 14
	.quad	4613937818241073152     # double 3
	.quad	4631248529308778496     # double 43
	.quad	4631670741773844480     # double 46
	.quad	4602967049548798624     # double 0.53200000000000003
	.quad	4603786704680980054     # double 0.623
	.quad	4624633867356078080     # double 15
	.quad	4613937818241073152     # double 3
	.quad	4631670741773844480     # double 46
	.quad	4632092954238910464     # double 49
	.quad	4600445033757471146     # double 0.376
	.quad	4601778099247172813     # double 0.45000000000000001
	.quad	4625196817309499392     # double 16
	.quad	4613937818241073152     # double 3
	.quad	4632092954238910464     # double 49
	.quad	4632515166703976448     # double 52
	.quad	4603129179135383962     # double 0.55000000000000004
	.quad	4603147193533893444     # double 0.55200000000000005
	.quad	4625478292286210048     # double 17
	.quad	4613937818241073152     # double 3
	.quad	4632515166703976448     # double 52
	.quad	4632937379169042432     # double 55
	.quad	4601742070450153849     # double 0.44800000000000001
	.quad	4600931422517227160     # double 0.40300000000000002
	.quad	4625759767262920704     # double 18
	.quad	4611686018427387904     # double 2
	.quad	4632937379169042432     # double 55
	.quad	4633218854145753088     # double 57
	.quad	4603552517500356788     # double 0.59699999999999998
	.quad	4603966848666074874     # double 0.64300000000000002
	.quad	4626041242239631360     # double 19
	.quad	4611686018427387904     # double 2
	.quad	4633218854145753088     # double 57
	.quad	4633500329122463744     # double 59
	.quad	4600102760185790988     # double 0.35699999999999998
	.quad	4604678417407199412     # double 0.72199999999999998
	.quad	4626322717216342016     # double 20
	.quad	4611686018427387904     # double 2
	.quad	4633500329122463744     # double 59
	.quad	4633781804099174400     # double 61
	.quad	4598679622703541912     # double 0.27800000000000002
	.quad	4606822130829827768     # double 0.95999999999999996
	.quad	4674525306978041856     # double 32000
	.quad	4626322717216342016     # double 20
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	4607182418800017408     # double 1
	.quad	4602931020751779660     # double 0.52800000000000002
	.quad	4607182418800017408     # double 1
	.quad	4611686018427387904     # double 2
	.quad	4611686018427387904     # double 2
	.quad	4616189618054758400     # double 4
	.quad	4602174416014381416     # double 0.47199999999999998
	.quad	4599166011463297925     # double 0.30499999999999999
	.quad	4611686018427387904     # double 2
	.quad	4611686018427387904     # double 2
	.quad	4616189618054758400     # double 4
	.quad	4618441417868443648     # double 6
	.quad	4604426215828066664     # double 0.69399999999999995
	.quad	4590645200968312947     # double 0.083000000000000004
	.quad	4613937818241073152     # double 3
	.quad	4607182418800017408     # double 1
	.quad	4618441417868443648     # double 6
	.quad	4619567317775286272     # double 7
	.quad	4606434821261873906     # double 0.91700000000000004
	.quad	4605930418103608410     # double 0.86099999999999999
	.quad	4616189618054758400     # double 4
	.quad	4611686018427387904     # double 2
	.quad	4619567317775286272     # double 7
	.quad	4621256167635550208     # double 9
	.quad	4594176023076171416     # double 0.13900000000000001
	.quad	4603930819869055910     # double 0.63900000000000001
	.quad	4617315517961601024     # double 5
	.quad	4611686018427387904     # double 2
	.quad	4621256167635550208     # double 9
	.quad	4622382067542392832     # double 11
	.quad	4600174817779828916     # double 0.36099999999999999
	.quad	4601183624096359907     # double 0.41699999999999998
	.quad	4618441417868443648     # double 6
	.quad	4613937818241073152     # double 3
	.quad	4622382067542392832     # double 11
	.quad	4624070917402656768     # double 14
	.quad	4603426416710790414     # double 0.58299999999999996
	.quad	4590645200968312947     # double 0.083000000000000004
	.quad	4619567317775286272     # double 7
	.quad	4611686018427387904     # double 2
	.quad	4624070917402656768     # double 14
	.quad	4625196817309499392     # double 16
	.quad	4606434821261873906     # double 0.91700000000000004
	.quad	4604930618986332160     # double 0.75
	.quad	4620693217682128896     # double 8
	.quad	4613937818241073152     # double 3
	.quad	4625196817309499392     # double 16
	.quad	4626041242239631360     # double 19
	.quad	4598175219545276416     # double 0.25
	.quad	4606011482896901079     # double 0.87
	.quad	4621256167635550208     # double 9
	.quad	4613937818241073152     # double 3
	.quad	4626041242239631360     # double 19
	.quad	4626885667169763328     # double 22
	.quad	4593851763903000740     # double 0.13
	.quad	4605678216524475662     # double 0.83299999999999996
	.quad	4621819117588971520     # double 10
	.quad	4616189618054758400     # double 4
	.quad	4626885667169763328     # double 22
	.quad	4628011567076605952     # double 26
	.quad	4595184829392702407     # double 0.16700000000000001
	.quad	4600679220938094412     # double 0.38900000000000001
	.quad	4622382067542392832     # double 11
	.quad	4616189618054758400     # double 4
	.quad	4628011567076605952     # double 26
	.quad	4629137466983448576     # double 30
	.quad	4603678618289923162     # double 0.61099999999999999
	.quad	4602282502405438308     # double 0.47799999999999998
	.quad	4622945017495814144     # double 12
	.quad	4616189618054758400     # double 4
	.quad	4629137466983448576     # double 30
	.quad	4629981891913580544     # double 34
	.quad	4602876977556251214     # double 0.52200000000000002
	.quad	4584916622242297676     # double 0.033000000000000002
	.quad	4623507967449235456     # double 13
	.quad	4613937818241073152     # double 3
	.quad	4629981891913580544     # double 34
	.quad	4630404104378646528     # double 37
	.quad	4606885181224610955     # double 0.96699999999999997
	.quad	4606434821261873906     # double 0.91700000000000004
	.quad	4624070917402656768     # double 14
	.quad	4616189618054758400     # double 4
	.quad	4630404104378646528     # double 37
	.quad	4630967054332067840     # double 41
	.quad	4590645200968312947     # double 0.083000000000000004
	.quad	4603732661485451608     # double 0.61699999999999999
	.quad	4624633867356078080     # double 15
	.quad	4613937818241073152     # double 3
	.quad	4630967054332067840     # double 41
	.quad	4631389266797133824     # double 44
	.quad	4600571134547037520     # double 0.38300000000000001
	.quad	4607137382803743703     # double 0.99499999999999999
	.quad	4625196817309499392     # double 16
	.quad	4616189618054758400     # double 4
	.quad	4631389266797133824     # double 44
	.quad	4631952216750555136     # double 48
	.quad	4572414629676717179     # double 0.0050000000000000001
	.quad	4598607565109503984     # double 0.27400000000000002
	.quad	4625478292286210048     # double 17
	.quad	4613937818241073152     # double 3
	.quad	4631952216750555136     # double 48
	.quad	4632374429215621120     # double 51
	.quad	4604714446204218376     # double 0.72599999999999998
	.quad	4602318531202457272     # double 0.47999999999999998
	.quad	4625759767262920704     # double 18
	.quad	4613937818241073152     # double 3
	.quad	4632374429215621120     # double 51
	.quad	4632796641680687104     # double 54
	.quad	4602849955958486991     # double 0.51900000000000002
	.quad	4598373377928880718     # double 0.26100000000000001
	.quad	4626041242239631360     # double 19
	.quad	4611686018427387904     # double 2
	.quad	4632796641680687104     # double 54
	.quad	4633078116657397760     # double 56
	.quad	4604831539794530009     # double 0.73899999999999999
	.quad	4606137583686467453     # double 0.88400000000000001
	.quad	4626322717216342016     # double 20
	.quad	4611686018427387904     # double 2
	.quad	4633078116657397760     # double 56
	.quad	4633359591634108416     # double 58
	.quad	4593023101571564569     # double 0.11600000000000001
	.quad	4607182418800017408     # double 1
	.quad	4672326283722489856     # double 24000
	.quad	4626322717216342016     # double 20
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	4607182418800017408     # double 1
	.quad	4606431819576693466     # double 0.91666674599999998
	.quad	4607182418800017408     # double 1
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4618441417868443648     # double 6
	.quad	4590669214442550703     # double 0.083333253900000001
	.quad	4603429420539684276     # double 0.58333349199999995
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4618441417868443648     # double 6
	.quad	4621256167635550208     # double 9
	.quad	4601177616438572183     # double 0.41666650799999999
	.quad	4598175219545276416     # double 0.25
	.quad	4613937818241073152     # double 3
	.quad	4611686018427387904     # double 2
	.quad	4621256167635550208     # double 9
	.quad	4622382067542392832     # double 11
	.quad	4604930618986332160     # double 0.75
	.quad	4606431821729414088     # double 0.91666698499999999
	.quad	4616189618054758400     # double 4
	.quad	4613937818241073152     # double 3
	.quad	4622382067542392832     # double 11
	.quad	4624070917402656768     # double 14
	.quad	4590669197256814525     # double 0.083333015400000002
	.quad	4603429424836118321     # double 0.58333396900000001
	.quad	4617315517961601024     # double 5
	.quad	4613937818241073152     # double 3
	.quad	4624070917402656768     # double 14
	.quad	4625478292286210048     # double 17
	.quad	4601177607845704094     # double 0.41666603099999999
	.quad	4598175219545276416     # double 0.25
	.quad	4618441417868443648     # double 6
	.quad	4613937818241073152     # double 3
	.quad	4625478292286210048     # double 17
	.quad	4626322717216342016     # double 20
	.quad	4604930618986332160     # double 0.75
	.quad	4603012417874696032     # double 0.53703689600000004
	.quad	4619567317775286272     # double 7
	.quad	4613937818241073152     # double 3
	.quad	4626322717216342016     # double 20
	.quad	4627167142146473984     # double 23
	.quad	4602011621768548673     # double 0.46296310400000001
	.quad	4602678819172646912     # double 0.5
	.quad	4620693217682128896     # double 8
	.quad	4616189618054758400     # double 4
	.quad	4627167142146473984     # double 23
	.quad	4628293042053316608     # double 27
	.quad	4602678819172646912     # double 0.5
	.quad	4588167284323656049     # double 0.055555999299999999
	.quad	4621256167635550208     # double 9
	.quad	4613937818241073152     # double 3
	.quad	4628293042053316608     # double 27
	.quad	4629137466983448576     # double 30
	.quad	4606682014847228217     # double 0.94444400100000003
	.quad	4600927434579757123     # double 0.402778625
	.quad	4621819117588971520     # double 10
	.quad	4613937818241073152     # double 3
	.quad	4629137466983448576     # double 30
	.quad	4629841154425225216     # double 33
	.quad	4603554511469091807     # double 0.59722137500000005
	.quad	4605080743660657189     # double 0.76666718700000003
	.quad	4622382067542392832     # double 11
	.quad	4613937818241073152     # double 3
	.quad	4629841154425225216     # double 33
	.quad	4630263366890291200     # double 36
	.quad	4597574721388408256     # double 0.23333282799999999
	.quad	4605431022939121351     # double 0.80555599899999997
	.quad	4622945017495814144     # double 12
	.quad	4613937818241073152     # double 3
	.quad	4630263366890291200     # double 36
	.quad	4630685579355357184     # double 39
	.quad	4596173604274551606     # double 0.194444016
	.quad	4605109339014289599     # double 0.76984190900000005
	.quad	4623507967449235456     # double 13
	.quad	4613937818241073152     # double 3
	.quad	4630685579355357184     # double 39
	.quad	4631107791820423168     # double 42
	.quad	4597460340478281775     # double 0.23015811999999999
	.quad	4603679619025789161     # double 0.61111110400000002
	.quad	4624070917402656768     # double 14
	.quad	4613937818241073152     # double 3
	.quad	4631107791820423168     # double 42
	.quad	4631530004285489152     # double 45
	.quad	4600677219466362414     # double 0.38888889599999998
	.quad	4601768997994743454     # double 0.44949477900000001
	.quad	4624633867356078080     # double 15
	.quad	4613937818241073152     # double 3
	.quad	4631530004285489152     # double 45
	.quad	4631952216750555136     # double 48
	.quad	4603133729761598641     # double 0.55050522099999999
	.quad	4596173638069563210     # double 0.194444954
	.quad	4625196817309499392     # double 16
	.quad	4611686018427387904     # double 2
	.quad	4631952216750555136     # double 48
	.quad	4632233691727265792     # double 50
	.quad	4605431014355260462     # double 0.80555504600000005
	.quad	4606400545770233073     # double 0.91319465600000005
	.quad	4625478292286210048     # double 17
	.quad	4613937818241073152     # double 3
	.quad	4632233691727265792     # double 50
	.quad	4632655904192331776     # double 53
	.quad	4590919404872616574     # double 0.086805343600000001
	.quad	4603404400746918423     # double 0.58055573699999996
	.quad	4625759767262920704     # double 18
	.quad	4613937818241073152     # double 3
	.quad	4632655904192331776     # double 53
	.quad	4633078116657397760     # double 56
	.quad	4601227656024103889     # double 0.41944426299999998
	.quad	4592837640312490502     # double 0.113426208
	.quad	4626041242239631360     # double 19
	.quad	4611686018427387904     # double 2
	.quad	4633078116657397760     # double 56
	.quad	4633359591634108416     # double 58
	.quad	4606160766343851711     # double 0.88657379199999997
	.quad	4602982637642016059     # double 0.53373062599999999
	.quad	4626322717216342016     # double 20
	.quad	4611686018427387904     # double 2
	.quad	4633359591634108416     # double 58
	.quad	4633641066610819072     # double 60
	.quad	4602071181693476662     # double 0.46626934399999997
	.quad	4604400783766783723     # double 0.69117647400000004
	.quad	4671790271803949056     # double 22050
	.quad	4626322717216342016     # double 20
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	4607182418800017408     # double 1
	.quad	4606431819576693466     # double 0.91666674599999998
	.quad	4607182418800017408     # double 1
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4618441417868443648     # double 6
	.quad	4590669214442550703     # double 0.083333253900000001
	.quad	4603429420539684276     # double 0.58333349199999995
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4618441417868443648     # double 6
	.quad	4621256167635550208     # double 9
	.quad	4601177616438572183     # double 0.41666650799999999
	.quad	4598175219545276416     # double 0.25
	.quad	4613937818241073152     # double 3
	.quad	4611686018427387904     # double 2
	.quad	4621256167635550208     # double 9
	.quad	4622382067542392832     # double 11
	.quad	4604930618986332160     # double 0.75
	.quad	4606431821729414088     # double 0.91666698499999999
	.quad	4616189618054758400     # double 4
	.quad	4613937818241073152     # double 3
	.quad	4622382067542392832     # double 11
	.quad	4624070917402656768     # double 14
	.quad	4590669197256814525     # double 0.083333015400000002
	.quad	4603429424836118321     # double 0.58333396900000001
	.quad	4617315517961601024     # double 5
	.quad	4613937818241073152     # double 3
	.quad	4624070917402656768     # double 14
	.quad	4625478292286210048     # double 17
	.quad	4601177607845704094     # double 0.41666603099999999
	.quad	4598175219545276416     # double 0.25
	.quad	4618441417868443648     # double 6
	.quad	4613937818241073152     # double 3
	.quad	4625478292286210048     # double 17
	.quad	4626322717216342016     # double 20
	.quad	4604930618986332160     # double 0.75
	.quad	4596507215298449306     # double 0.20370358199999999
	.quad	4619567317775286272     # double 7
	.quad	4613937818241073152     # double 3
	.quad	4626322717216342016     # double 20
	.quad	4627167142146473984     # double 23
	.quad	4605347620048038937     # double 0.79629641799999995
	.quad	4595172820526281625     # double 0.16666668700000001
	.quad	4620693217682128896     # double 8
	.quad	4613937818241073152     # double 3
	.quad	4627167142146473984     # double 23
	.quad	4628011567076605952     # double 26
	.quad	4605681218741080858     # double 0.83333331300000002
	.quad	4604680423184372653     # double 0.72222268599999995
	.quad	4621256167635550208     # double 9
	.quad	4616189618054758400     # double 4
	.quad	4628011567076605952     # double 26
	.quad	4629137466983448576     # double 30
	.quad	4598675611689627384     # double 0.27777734399999998
	.quad	4594672450359496838     # double 0.152778625
	.quad	4621819117588971520     # double 10
	.quad	4613937818241073152     # double 3
	.quad	4629137466983448576     # double 30
	.quad	4629841154425225216     # double 33
	.quad	4605806311282777055     # double 0.84722137500000005
	.quad	4603279303917795381     # double 0.56666719899999995
	.quad	4622382067542392832     # double 11
	.quad	4613937818241073152     # double 3
	.quad	4629841154425225216     # double 33
	.quad	4630263366890291200     # double 36
	.quad	4601477850222781928     # double 0.43333283099999997
	.quad	4606598623755576827     # double 0.93518573000000005
	.quad	4622945017495814144     # double 12
	.quad	4616189618054758400     # double 4
	.quad	4630263366890291200     # double 36
	.quad	4630826316843712512     # double 40
	.quad	4589334779375776435     # double 0.064814247199999994
	.quad	4593171273386011978     # double 0.118056297
	.quad	4623507967449235456     # double 13
	.quad	4613937818241073152     # double 3
	.quad	4630826316843712512     # double 40
	.quad	4631248529308778496     # double 43
	.quad	4606119062209661527     # double 0.88194370300000002
	.quad	4591336502033933640     # double 0.092593729499999999
	.quad	4624070917402656768     # double 14
	.quad	4611686018427387904     # double 2
	.quad	4631248529308778496     # double 43
	.quad	4631530004285489152     # double 45
	.quad	4606348408633174919     # double 0.90740627100000004
	.quad	4606591043674986405     # double 0.93434417199999997
	.quad	4624633867356078080     # double 15
	.quad	4613937818241073152     # double 3
	.quad	4631530004285489152     # double 45
	.quad	4631952216750555136     # double 48
	.quad	4589395419480067851     # double 0.065655797700000004
	.quad	4603357944234257452     # double 0.57539802799999995
	.quad	4625196817309499392     # double 16
	.quad	4613937818241073152     # double 3
	.quad	4631952216750555136     # double 48
	.quad	4632374429215621120     # double 51
	.quad	4601320569049425831     # double 0.42460197199999999
	.quad	4597527671634582770     # double 0.23202693499999999
	.quad	4625478292286210048     # double 17
	.quad	4611686018427387904     # double 2
	.quad	4632374429215621120     # double 51
	.quad	4632655904192331776     # double 53
	.quad	4605092505964005571     # double 0.76797306499999995
	.quad	4605005688038361719     # double 0.75833433900000002
	.quad	4625759767262920704     # double 18
	.quad	4613937818241073152     # double 3
	.quad	4632655904192331776     # double 53
	.quad	4633078116657397760     # double 56
	.quad	4597874942796726226     # double 0.24166564600000001
	.quad	4595923464839645036     # double 0.18750125200000001
	.quad	4626041242239631360     # double 19
	.quad	4611686018427387904     # double 2
	.quad	4633078116657397760     # double 56
	.quad	4633359591634108416     # double 58
	.quad	4605493557662740005     # double 0.81249874799999999
	.quad	4602982647306740859     # double 0.53373169899999995
	.quad	4626322717216342016     # double 20
	.quad	4611686018427387904     # double 2
	.quad	4633359591634108416     # double 58
	.quad	4633641066610819072     # double 60
	.quad	4602071162364027062     # double 0.46626827100000001
	.quad	4598311725415393070     # double 0.25757759800000002
	.quad	4670021707350671360     # double 16000
	.quad	4626322717216342016     # double 20
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	4607182418800017408     # double 1
	.quad	4606682019143662261     # double 0.94444447799999998
	.quad	4607182418800017408     # double 1
	.quad	4611686018427387904     # double 2
	.quad	4611686018427387904     # double 2
	.quad	4616189618054758400     # double 4
	.quad	4588167212914580358     # double 0.055555503800000003
	.quad	4604680419959795320     # double 0.72222232799999997
	.quad	4611686018427387904     # double 2
	.quad	4611686018427387904     # double 2
	.quad	4616189618054758400     # double 4
	.quad	4618441417868443648     # double 6
	.quad	4598675617598350095     # double 0.27777767199999998
	.quad	4602678819172646912     # double 0.5
	.quad	4613937818241073152     # double 3
	.quad	4611686018427387904     # double 2
	.quad	4618441417868443648     # double 6
	.quad	4620693217682128896     # double 8
	.quad	4602678819172646912     # double 0.5
	.quad	4598675622966640851     # double 0.27777796999999999
	.quad	4616189618054758400     # double 4
	.quad	4611686018427387904     # double 2
	.quad	4620693217682128896     # double 8
	.quad	4621819117588971520     # double 10
	.quad	4604680417275649942     # double 0.72222202999999996
	.quad	4588167284323656049     # double 0.055555999299999999
	.quad	4617315517961601024     # double 5
	.quad	4607182418800017408     # double 1
	.quad	4621819117588971520     # double 10
	.quad	4622382067542392832     # double 11
	.quad	4606682014847228217     # double 0.94444400100000003
	.quad	4605681218741080858     # double 0.83333331300000002
	.quad	4618441417868443648     # double 6
	.quad	4613937818241073152     # double 3
	.quad	4622382067542392832     # double 11
	.quad	4624070917402656768     # double 14
	.quad	4595172819985849669     # double 0.16666667199999999
	.quad	4596507215298449306     # double 0.20370358199999999
	.quad	4619567317775286272     # double 7
	.quad	4613937818241073152     # double 3
	.quad	4624070917402656768     # double 14
	.quad	4625478292286210048     # double 17
	.quad	4605347620048038937     # double 0.79629641799999995
	.quad	4595172820526281625     # double 0.16666668700000001
	.quad	4620693217682128896     # double 8
	.quad	4613937818241073152     # double 3
	.quad	4625478292286210048     # double 17
	.quad	4626322717216342016     # double 20
	.quad	4605681218741080858     # double 0.83333331300000002
	.quad	4603054121963850220     # double 0.54166698000000002
	.quad	4621256167635550208     # double 9
	.quad	4613937818241073152     # double 3
	.quad	4626322717216342016     # double 20
	.quad	4627167142146473984     # double 23
	.quad	4601928213500168304     # double 0.45833301500000001
	.quad	4604054926689887265     # double 0.65277862499999995
	.quad	4621819117588971520     # double 10
	.quad	4616189618054758400     # double 4
	.quad	4627167142146473984     # double 23
	.quad	4628293042053316608     # double 27
	.quad	4599926604138166205     # double 0.347221375
	.quad	4595172837675989006     # double 0.16666716300000001
	.quad	4622382067542392832     # double 11
	.quad	4613937818241073152     # double 3
	.quad	4628293042053316608     # double 27
	.quad	4629137466983448576     # double 30
	.quad	4605681214453654013     # double 0.83333283700000005
	.quad	4604680425328086076     # double 0.72222292399999999
	.quad	4622945017495814144     # double 12
	.quad	4616189618054758400     # double 4
	.quad	4629137466983448576     # double 30
	.quad	4629981891913580544     # double 34
	.quad	4598675607402200539     # double 0.277777106
	.quad	4598675634766071875     # double 0.277778625
	.quad	4623507967449235456     # double 13
	.quad	4613937818241073152     # double 3
	.quad	4629981891913580544     # double 34
	.quad	4630404104378646528     # double 37
	.quad	4604680411375934431     # double 0.72222137500000005
	.quad	4603617080546168418     # double 0.60416793800000002
	.quad	4624070917402656768     # double 14
	.quad	4613937818241073152     # double 3
	.quad	4630404104378646528     # double 37
	.quad	4630826316843712512     # double 40
	.quad	4600802296425603900     # double 0.39583206199999998
	.quad	4603829746924691523     # double 0.62777864900000002
	.quad	4624633867356078080     # double 15
	.quad	4613937818241073152     # double 3
	.quad	4630826316843712512     # double 40
	.quad	4631248529308778496     # double 43
	.quad	4600376964190975247     # double 0.37222137999999999
	.quad	4603063754001524461     # double 0.54273635099999995
	.quad	4625196817309499392     # double 16
	.quad	4613937818241073152     # double 3
	.quad	4631248529308778496     # double 43
	.quad	4631670741773844480     # double 46
	.quad	4601908949514891813     # double 0.45726364899999999
	.quad	4600364484626335811     # double 0.371528625
	.quad	4625478292286210048     # double 17
	.quad	4613937818241073152     # double 3
	.quad	4631670741773844480     # double 46
	.quad	4632092954238910464     # double 49
	.quad	4603835986445802463     # double 0.62847137500000005
	.quad	4575958040920713154     # double 0.0083343386700000001
	.quad	4625759767262920704     # double 18
	.quad	4611686018427387904     # double 2
	.quad	4632092954238910464     # double 49
	.quad	4632374429215621120     # double 51
	.quad	4607107349747987849     # double 0.99166566099999997
	.quad	4602678830981085135     # double 0.500001311
	.quad	4626041242239631360     # double 19
	.quad	4611686018427387904     # double 2
	.quad	4632374429215621120     # double 51
	.quad	4632655904192331776     # double 53
	.quad	4602678795015338511     # double 0.49999865900000001
	.quad	4606163100118185814     # double 0.88683289300000001
	.quad	4626322717216342016     # double 20
	.quad	4611686018427387904     # double 2
	.quad	4632655904192331776     # double 53
	.quad	4632937379169042432     # double 55
	.quad	4592818969036953770     # double 0.113167092
	.quad	4603841056111874196     # double 0.62903422099999995
	.quad	4676829883349860352     # double 48000
	.quad	4622382067542392832     # double 11
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	4607182418800017408     # double 1
	.quad	4595184829392702407     # double 0.16700000000000001
	.quad	4607182418800017408     # double 1
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4617315517961601024     # double 5
	.quad	4605678216524475662     # double 0.83299999999999996
	.quad	4605678216524475662     # double 0.83299999999999996
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4617315517961601024     # double 5
	.quad	4620693217682128896     # double 8
	.quad	4595184829392702407     # double 0.16700000000000001
	.quad	4602678819172646912     # double 0.5
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4620693217682128896     # double 8
	.quad	4622382067542392832     # double 11
	.quad	4602678819172646912     # double 0.5
	.quad	4595184829392702407     # double 0.16700000000000001
	.quad	4616189618054758400     # double 4
	.quad	4616189618054758400     # double 4
	.quad	4622382067542392832     # double 11
	.quad	4624633867356078080     # double 15
	.quad	4605678216524475662     # double 0.83299999999999996
	.quad	4595184829392702407     # double 0.16700000000000001
	.quad	4617315517961601024     # double 5
	.quad	4616189618054758400     # double 4
	.quad	4624633867356078080     # double 15
	.quad	4626041242239631360     # double 19
	.quad	4605678216524475662     # double 0.83299999999999996
	.quad	4603426416710790414     # double 0.58299999999999996
	.quad	4618441417868443648     # double 6
	.quad	4613937818241073152     # double 3
	.quad	4626041242239631360     # double 19
	.quad	4626885667169763328     # double 22
	.quad	4601183624096359907     # double 0.41699999999999998
	.quad	4606434821261873906     # double 0.91700000000000004
	.quad	4619567317775286272     # double 7
	.quad	4616189618054758400     # double 4
	.quad	4626885667169763328     # double 22
	.quad	4628011567076605952     # double 26
	.quad	4590645200968312947     # double 0.083000000000000004
	.quad	4606678015641751912     # double 0.94399999999999995
	.quad	4620693217682128896     # double 8
	.quad	4616189618054758400     # double 4
	.quad	4628011567076605952     # double 26
	.quad	4629137466983448576     # double 30
	.quad	4588087156379966505     # double 0.055
	.quad	4586213658934980379     # double 0.042000000000000003
	.quad	4621256167635550208     # double 9
	.quad	4611686018427387904     # double 2
	.quad	4629137466983448576     # double 30
	.quad	4629700416936869888     # double 32
	.quad	4606804116431318286     # double 0.95799999999999996
	.quad	4603282301522714558     # double 0.56699999999999995
	.quad	4621819117588971520     # double 10
	.quad	4613937818241073152     # double 3
	.quad	4629700416936869888     # double 32
	.quad	4630122629401935872     # double 35
	.quad	4601471854472511619     # double 0.433
	.quad	4595184829392702407     # double 0.16700000000000001
	.quad	4622382067542392832     # double 11
	.quad	4611686018427387904     # double 2
	.quad	4630122629401935872     # double 35
	.quad	4630404104378646528     # double 37
	.quad	4605678216524475662     # double 0.83299999999999996
	.quad	4603741668684706349     # double 0.61799999999999999
	.quad	4676293871431319552     # double 44100
	.quad	4622382067542392832     # double 11
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	4607182418800017408     # double 1
	.quad	4595184829392702407     # double 0.16700000000000001
	.quad	4607182418800017408     # double 1
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4617315517961601024     # double 5
	.quad	4605678216524475662     # double 0.83299999999999996
	.quad	4605678216524475662     # double 0.83299999999999996
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4617315517961601024     # double 5
	.quad	4620693217682128896     # double 8
	.quad	4595184829392702407     # double 0.16700000000000001
	.quad	4602678819172646912     # double 0.5
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4620693217682128896     # double 8
	.quad	4622382067542392832     # double 11
	.quad	4602678819172646912     # double 0.5
	.quad	4595184829392702407     # double 0.16700000000000001
	.quad	4616189618054758400     # double 4
	.quad	4616189618054758400     # double 4
	.quad	4622382067542392832     # double 11
	.quad	4624633867356078080     # double 15
	.quad	4605678216524475662     # double 0.83299999999999996
	.quad	4595184829392702407     # double 0.16700000000000001
	.quad	4617315517961601024     # double 5
	.quad	4617315517961601024     # double 5
	.quad	4624633867356078080     # double 15
	.quad	4626322717216342016     # double 20
	.quad	4605678216524475662     # double 0.83299999999999996
	.quad	4598175219545276416     # double 0.25
	.quad	4618441417868443648     # double 6
	.quad	4613937818241073152     # double 3
	.quad	4626322717216342016     # double 20
	.quad	4627167142146473984     # double 23
	.quad	4604930618986332160     # double 0.75
	.quad	4603426416710790414     # double 0.58299999999999996
	.quad	4619567317775286272     # double 7
	.quad	4616189618054758400     # double 4
	.quad	4627167142146473984     # double 23
	.quad	4628293042053316608     # double 27
	.quad	4601183624096359907     # double 0.41699999999999998
	.quad	4588087156379966505     # double 0.055
	.quad	4620693217682128896     # double 8
	.quad	4613937818241073152     # double 3
	.quad	4628293042053316608     # double 27
	.quad	4629137466983448576     # double 30
	.quad	4606678015641751912     # double 0.94399999999999995
	.quad	4600427019358961664     # double 0.375
	.quad	4621256167635550208     # double 9
	.quad	4613937818241073152     # double 3
	.quad	4629137466983448576     # double 30
	.quad	4629841154425225216     # double 33
	.quad	4603804719079489536     # double 0.625
	.quad	4599075939470750515     # double 0.29999999999999999
	.quad	4621819117588971520     # double 10
	.quad	4613937818241073152     # double 3
	.quad	4629841154425225216     # double 33
	.quad	4630263366890291200     # double 36
	.quad	4604480259023595110     # double 0.69999999999999996
	.quad	4595184829392702407     # double 0.16700000000000001
	.quad	4622382067542392832     # double 11
	.quad	4611686018427387904     # double 2
	.quad	4630263366890291200     # double 36
	.quad	4630544841867001856     # double 38
	.quad	4605678216524475662     # double 0.83299999999999996
	.quad	4607182418800017408     # double 1
	.quad	4674525306978041856     # double 32000
	.quad	4622382067542392832     # double 11
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	4607182418800017408     # double 1
	.quad	4595184829392702407     # double 0.16700000000000001
	.quad	4607182418800017408     # double 1
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4617315517961601024     # double 5
	.quad	4605678216524475662     # double 0.83299999999999996
	.quad	4605678216524475662     # double 0.83299999999999996
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4617315517961601024     # double 5
	.quad	4620693217682128896     # double 8
	.quad	4595184829392702407     # double 0.16700000000000001
	.quad	4602678819172646912     # double 0.5
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4620693217682128896     # double 8
	.quad	4622382067542392832     # double 11
	.quad	4602678819172646912     # double 0.5
	.quad	4595184829392702407     # double 0.16700000000000001
	.quad	4616189618054758400     # double 4
	.quad	4616189618054758400     # double 4
	.quad	4622382067542392832     # double 11
	.quad	4624633867356078080     # double 15
	.quad	4605678216524475662     # double 0.83299999999999996
	.quad	4595184829392702407     # double 0.16700000000000001
	.quad	4617315517961601024     # double 5
	.quad	4617315517961601024     # double 5
	.quad	4624633867356078080     # double 15
	.quad	4626322717216342016     # double 20
	.quad	4605678216524475662     # double 0.83299999999999996
	.quad	4598175219545276416     # double 0.25
	.quad	4618441417868443648     # double 6
	.quad	4616189618054758400     # double 4
	.quad	4626322717216342016     # double 20
	.quad	4627448617123184640     # double 24
	.quad	4604930618986332160     # double 0.75
	.quad	4598175219545276416     # double 0.25
	.quad	4619567317775286272     # double 7
	.quad	4617315517961601024     # double 5
	.quad	4627448617123184640     # double 24
	.quad	4628855992006737920     # double 29
	.quad	4604930618986332160     # double 0.75
	.quad	4588087156379966505     # double 0.055
	.quad	4620693217682128896     # double 8
	.quad	4616189618054758400     # double 4
	.quad	4628855992006737920     # double 29
	.quad	4629841154425225216     # double 33
	.quad	4606678015641751912     # double 0.94399999999999995
	.quad	4600427019358961664     # double 0.375
	.quad	4621256167635550208     # double 9
	.quad	4616189618054758400     # double 4
	.quad	4629841154425225216     # double 33
	.quad	4630404104378646528     # double 37
	.quad	4603804719079489536     # double 0.625
	.quad	4602174416014381416     # double 0.47199999999999998
	.quad	4621819117588971520     # double 10
	.quad	4613937818241073152     # double 3
	.quad	4630404104378646528     # double 37
	.quad	4630826316843712512     # double 40
	.quad	4602931020751779660     # double 0.52800000000000002
	.quad	4606614965246968726     # double 0.93700000000000006
	.quad	4622382067542392832     # double 11
	.quad	4607182418800017408     # double 1
	.quad	4630826316843712512     # double 40
	.quad	4630967054332067840     # double 41
	.quad	4589095962696497496     # double 0.062
	.quad	4607182418800017408     # double 1
	.quad	4672326283722489856     # double 24000
	.quad	4622382067542392832     # double 11
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4616189618054758400     # double 4
	.quad	4607182418800017408     # double 1
	.quad	4595172822651980649     # double 0.166666746
	.quad	4607182418800017408     # double 1
	.quad	4611686018427387904     # double 2
	.quad	4616189618054758400     # double 4
	.quad	4618441417868443648     # double 6
	.quad	4605681218209656102     # double 0.83333325400000002
	.quad	4605681220353369524     # double 0.83333349199999995
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4618441417868443648     # double 6
	.quad	4621256167635550208     # double 9
	.quad	4595172814077126958     # double 0.16666650799999999
	.quad	4602678819172646912     # double 0.5
	.quad	4613937818241073152     # double 3
	.quad	4616189618054758400     # double 4
	.quad	4621256167635550208     # double 9
	.quad	4623507967449235456     # double 13
	.quad	4602678819172646912     # double 0.5
	.quad	4602678819172646912     # double 0.5
	.quad	4616189618054758400     # double 4
	.quad	4617315517961601024     # double 5
	.quad	4623507967449235456     # double 13
	.quad	4625759767262920704     # double 18
	.quad	4602678819172646912     # double 0.5
	.quad	4605681224649803569     # double 0.83333396900000001
	.quad	4617315517961601024     # double 5
	.quad	4617315517961601024     # double 5
	.quad	4625759767262920704     # double 18
	.quad	4627167142146473984     # double 23
	.quad	4595172796891390780     # double 0.16666603099999999
	.quad	4598175219545276416     # double 0.25
	.quad	4618441417868443648     # double 6
	.quad	4616189618054758400     # double 4
	.quad	4627167142146473984     # double 23
	.quad	4628293042053316608     # double 27
	.quad	4604930618986332160     # double 0.75
	.quad	4598175219545276416     # double 0.25
	.quad	4619567317775286272     # double 7
	.quad	4613937818241073152     # double 3
	.quad	4628293042053316608     # double 27
	.quad	4629137466983448576     # double 30
	.quad	4604930618986332160     # double 0.75
	.quad	4603679627087232494     # double 0.61111199900000002
	.quad	4620693217682128896     # double 8
	.quad	4616189618054758400     # double 4
	.quad	4629137466983448576     # double 30
	.quad	4629981891913580544     # double 34
	.quad	4600677203883907704     # double 0.38888803100000002
	.quad	4596674042571791556     # double 0.20833396900000001
	.quad	4621256167635550208     # double 9
	.quad	4613937818241073152     # double 3
	.quad	4629981891913580544     # double 34
	.quad	4630404104378646528     # double 37
	.quad	4605305913229703375     # double 0.79166603099999999
	.quad	4605080743660657189     # double 0.76666718700000003
	.quad	4621819117588971520     # double 10
	.quad	4616189618054758400     # double 4
	.quad	4630404104378646528     # double 37
	.quad	4630967054332067840     # double 41
	.quad	4597574721388408256     # double 0.23333282799999999
	.quad	4601821003599670891     # double 0.45238167000000001
	.quad	4622382067542392832     # double 11
	.quad	4616189618054758400     # double 4
	.quad	4630967054332067840     # double 41
	.quad	4631530004285489152     # double 45
	.quad	4603107726959134923     # double 0.54761833000000004
	.quad	4598675634766071875     # double 0.277778625
	.quad	4671790271803949056     # double 22050
	.quad	4622382067542392832     # double 11
	.quad	0                       # double 0
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4616189618054758400     # double 4
	.quad	4607182418800017408     # double 1
	.quad	4595172822651980649     # double 0.166666746
	.quad	4607182418800017408     # double 1
	.quad	4611686018427387904     # double 2
	.quad	4616189618054758400     # double 4
	.quad	4618441417868443648     # double 6
	.quad	4605681218209656102     # double 0.83333325400000002
	.quad	4605681220353369524     # double 0.83333349199999995
	.quad	4611686018427387904     # double 2
	.quad	4613937818241073152     # double 3
	.quad	4618441417868443648     # double 6
	.quad	4621256167635550208     # double 9
	.quad	4595172814077126958     # double 0.16666650799999999
	.quad	4602678819172646912     # double 0.5
	.quad	4613937818241073152     # double 3
	.quad	4616189618054758400     # double 4
	.quad	4621256167635550208     # double 9
	.quad	4623507967449235456     # double 13
	.quad	4602678819172646912     # double 0.5
	.quad	4602678819172646912     # double 0.5
	.quad	4616189618054758400     # double 4
	.quad	4616189618054758400     # double 4
	.quad	4623507967449235456     # double 13
	.quad	4625478292286210048     # double 17
	.quad	4602678819172646912     # double 0.5
	.quad	4602678819172646912     # double 0.5
	.quad	4617315517961601024     # double 5
	.quad	4616189618054758400     # double 4
	.quad	4625478292286210048     # double 17
	.quad	4626604192193052672     # double 21
	.quad	4602678819172646912     # double 0.5
	.quad	4606431821729414088     # double 0.91666698499999999
	.quad	4618441417868443648     # double 6
	.quad	4616189618054758400     # double 4
	.quad	4626604192193052672     # double 21
	.quad	4627730092099895296     # double 25
	.quad	4590669197256814525     # double 0.083333015400000002
	.quad	4598175219545276416     # double 0.25
	.quad	4619567317775286272     # double 7
	.quad	4616189618054758400     # double 4
	.quad	4627730092099895296     # double 25
	.quad	4628855992006737920     # double 29
	.quad	4604930618986332160     # double 0.75
	.quad	4603679627087232494     # double 0.61111199900000002
	.quad	4620693217682128896     # double 8
	.quad	4616189618054758400     # double 4
	.quad	4628855992006737920     # double 29
	.quad	4629841154425225216     # double 33
	.quad	4600677203883907704     # double 0.38888803100000002
	.quad	4601928230685904482     # double 0.45833396900000001
	.quad	4621256167635550208     # double 9
	.quad	4616189618054758400     # double 4
	.quad	4629841154425225216     # double 33
	.quad	4630404104378646528     # double 37
	.quad	4603054113416018127     # double 0.54166603099999999
	.quad	4603879788131519095     # double 0.63333433900000002
	.quad	4621819117588971520     # double 10
	.quad	4616189618054758400     # double 4
	.quad	4630404104378646528     # double 37
	.quad	4630967054332067840     # double 41
	.quad	4600276881254902546     # double 0.36666566099999998
	.quad	4603429430204409077     # double 0.58333456500000003
	.quad	4622382067542392832     # double 11
	.quad	4616189618054758400     # double 4
	.quad	4630967054332067840     # double 41
	.quad	4631530004285489152     # double 45
	.quad	4601177596568690627     # double 0.41666540499999999
	.quad	4601552936451540466     # double 0.437500954
	.quad	4670021707350671360     # double 16000
	.quad	4622382067542392832     # double 11
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	4616189618054758400     # double 4
	.quad	4607182418800017408     # double 1
	.quad	4595172822651980649     # double 0.166666746
	.quad	4607182418800017408     # double 1
	.quad	0                       # double 0
	.quad	4616189618054758400     # double 4
	.quad	4618441417868443648     # double 6
	.quad	4605681218209656102     # double 0.83333325400000002
	.quad	4605681220353369524     # double 0.83333349199999995
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4618441417868443648     # double 6
	.quad	4621256167635550208     # double 9
	.quad	4595172814077126958     # double 0.16666650799999999
	.quad	4602678819172646912     # double 0.5
	.quad	4613937818241073152     # double 3
	.quad	0                       # double 0
	.quad	4621256167635550208     # double 9
	.quad	4623507967449235456     # double 13
	.quad	4602678819172646912     # double 0.5
	.quad	4602678819172646912     # double 0.5
	.quad	4616189618054758400     # double 4
	.quad	0                       # double 0
	.quad	4623507967449235456     # double 13
	.quad	4625759767262920704     # double 18
	.quad	4602678819172646912     # double 0.5
	.quad	4605681224649803569     # double 0.83333396900000001
	.quad	4617315517961601024     # double 5
	.quad	0                       # double 0
	.quad	4625759767262920704     # double 18
	.quad	4627167142146473984     # double 23
	.quad	4595172796891390780     # double 0.16666603099999999
	.quad	4604930618986332160     # double 0.75
	.quad	4618441417868443648     # double 6
	.quad	0                       # double 0
	.quad	4627167142146473984     # double 23
	.quad	4628293042053316608     # double 27
	.quad	4598175219545276416     # double 0.25
	.quad	4604930618986332160     # double 0.75
	.quad	4619567317775286272     # double 7
	.quad	0                       # double 0
	.quad	4628293042053316608     # double 27
	.quad	4629418941960159232     # double 31
	.quad	4598175219545276416     # double 0.25
	.quad	4603679627087232494     # double 0.61111199900000002
	.quad	4620693217682128896     # double 8
	.quad	0                       # double 0
	.quad	4629418941960159232     # double 31
	.quad	4630122629401935872     # double 35
	.quad	4600677203883907704     # double 0.38888803100000002
	.quad	4601928230685904482     # double 0.45833396900000001
	.quad	4621256167635550208     # double 9
	.quad	0                       # double 0
	.quad	4630122629401935872     # double 35
	.quad	4630685579355357184     # double 39
	.quad	4603054113416018127     # double 0.54166603099999999
	.quad	4595172837675989006     # double 0.16666716300000001
	.quad	4621819117588971520     # double 10
	.quad	0                       # double 0
	.quad	4630685579355357184     # double 39
	.quad	4631107791820423168     # double 42
	.quad	4605681214453654013     # double 0.83333283700000005
	.quad	4605431022939121351     # double 0.80555599899999997
	.quad	4622382067542392832     # double 11
	.quad	0                       # double 0
	.quad	4631107791820423168     # double 42
	.quad	4631670741773844480     # double 46
	.quad	4596173604274551606     # double 0.194444016
	.quad	4601552919265804288     # double 0.4375
	.size	psy_data, 39408


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
