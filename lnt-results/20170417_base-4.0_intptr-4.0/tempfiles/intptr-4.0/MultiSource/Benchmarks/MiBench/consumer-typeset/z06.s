	.text
	.file	"z06.bc"
	.globl	OptimizeCase
	.p2align	4, 0x90
	.type	OptimizeCase,@function
OptimizeCase:                           # @OptimizeCase
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 64
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r13, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	cmpb	$52, 32(%r14)
	je	.LBB0_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_2:
	movq	(%r14), %rcx
	addq	$16, %rcx
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rbx
	movzbl	32(%rbx), %eax
	leaq	16(%rbx), %rcx
	testb	%al, %al
	je	.LBB0_3
# BB#4:
	movl	$1, 4(%rsp)
	movq	$0, 8(%rsp)
	cmpb	$55, %al
	je	.LBB0_16
# BB#5:
	cmpb	$17, %al
	jne	.LBB0_15
# BB#6:                                 # %.preheader32
	movq	8(%rbx), %r13
	movl	$1, %eax
	cmpq	%rbx, %r13
	je	.LBB0_17
# BB#7:                                 # %.preheader.preheader
	leaq	8(%rsp), %r15
	leaq	4(%rsp), %r12
.LBB0_8:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_9 Depth 2
	movq	%r13, %rdi
	.p2align	4, 0x90
.LBB0_9:                                #   Parent Loop BB0_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	movzbl	32(%rdi), %ecx
	testb	%cl, %cl
	je	.LBB0_9
# BB#10:                                #   in Loop: Header=BB0_8 Depth=1
	cmpb	$1, %cl
	je	.LBB0_13
# BB#11:                                #   in Loop: Header=BB0_8 Depth=1
	cmpb	$55, %cl
	jne	.LBB0_15
# BB#12:                                #   in Loop: Header=BB0_8 Depth=1
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	check_yield
	movl	4(%rsp), %eax
.LBB0_13:                               # %.backedge
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	8(%r13), %r13
	cmpq	%rbx, %r13
	je	.LBB0_17
# BB#14:                                # %.backedge
                                        #   in Loop: Header=BB0_8 Depth=1
	testl	%eax, %eax
	jne	.LBB0_8
	jmp	.LBB0_17
.LBB0_15:
	movl	$0, 4(%rsp)
	movq	$0, 8(%rsp)
.LBB0_27:
	movq	%r14, %rbx
	jmp	.LBB0_28
.LBB0_16:
	leaq	8(%rsp), %rsi
	leaq	4(%rsp), %rdx
	movq	%rbx, %rdi
	callq	check_yield
	movl	4(%rsp), %eax
.LBB0_17:                               # %.loopexit33
	testl	%eax, %eax
	je	.LBB0_27
# BB#18:                                # %.loopexit33
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_27
# BB#19:
	movq	(%rax), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB0_20:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rbx
	leaq	16(%rbx), %rax
	cmpb	$0, 32(%rbx)
	je	.LBB0_20
# BB#21:
	movq	24(%rbx), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_23
# BB#22:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB0_23:
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_25
# BB#24:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_25:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	%r14, %rdi
	callq	DisposeObject
.LBB0_28:                               # %.loopexit33.thread
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	OptimizeCase, .Lfunc_end0-OptimizeCase
	.cfi_endproc

	.p2align	4, 0x90
	.type	check_yield,@function
check_yield:                            # @check_yield
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 64
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	8(%r15), %rbx
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB1_1
# BB#2:
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$2, %cl
	jae	.LBB1_3
# BB#11:
	addq	$64, %rbx
	movq	BackEnd(%rip), %rax
	movq	8(%rax), %rsi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_13
# BB#12:
	movl	$.L.str.57, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB1_20
.LBB1_13:
	cmpq	$0, (%r14)
	jne	.LBB1_20
# BB#14:
	movq	%r15, (%r14)
	jmp	.LBB1_20
.LBB1_3:
	cmpb	$17, %al
	jne	.LBB1_10
# BB#4:                                 # %.preheader
	movq	8(%rbx), %rbp
	cmpq	%rbx, %rbp
	je	.LBB1_20
# BB#5:                                 # %.lr.ph
	leaq	64(%rbx), %r13
.LBB1_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_7 Depth 2
	leaq	16(%rbp), %rdi
	jmp	.LBB1_7
	.p2align	4, 0x90
.LBB1_15:                               #   in Loop: Header=BB1_7 Depth=2
	addq	$16, %rdi
.LBB1_7:                                #   Parent Loop BB1_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %rdi
	movzbl	32(%rdi), %eax
	testb	%al, %al
	je	.LBB1_15
# BB#8:                                 #   in Loop: Header=BB1_6 Depth=1
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$2, %cl
	jae	.LBB1_9
# BB#16:                                #   in Loop: Header=BB1_6 Depth=1
	addq	$64, %rdi
	movq	BackEnd(%rip), %rax
	movq	8(%rax), %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_18
# BB#17:                                #   in Loop: Header=BB1_6 Depth=1
	movl	$.L.str.57, %esi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB1_19
.LBB1_18:                               #   in Loop: Header=BB1_6 Depth=1
	cmpq	$0, (%r14)
	jne	.LBB1_19
# BB#21:                                #   in Loop: Header=BB1_6 Depth=1
	movq	%r15, (%r14)
	jmp	.LBB1_19
	.p2align	4, 0x90
.LBB1_9:                                #   in Loop: Header=BB1_6 Depth=1
	cmpb	$1, %al
	jne	.LBB1_10
.LBB1_19:                               # %.backedge
                                        #   in Loop: Header=BB1_6 Depth=1
	movq	8(%rbp), %rbp
	cmpq	%rbx, %rbp
	jne	.LBB1_6
	jmp	.LBB1_20
.LBB1_10:                               # %.sink.split
	movl	$0, (%r12)
	movq	$0, (%r14)
.LBB1_20:                               # %.loopexit104
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	check_yield, .Lfunc_end1-check_yield
	.cfi_endproc

	.globl	SetScope
	.p2align	4, 0x90
	.type	SetScope,@function
SetScope:                               # @SetScope
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 48
.Lcfi29:
	.cfi_offset %rbx, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %rbp
	testq	%rbp, %rbp
	je	.LBB2_2
# BB#1:
	cmpb	$82, 32(%rbp)
	je	.LBB2_3
.LBB2_2:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_3:
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB2_16
# BB#4:                                 # %.preheader.preheader
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB2_5:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB2_5
# BB#6:
	cmpq	%rbx, (%rbx)
	jne	.LBB2_8
# BB#7:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rbp), %rax
.LBB2_8:
	cmpq	%rax, (%rbp)
	cmoveq	%rbx, %rbp
	movq	(%rbp), %rbp
	.p2align	4, 0x90
.LBB2_9:                                # =>This Inner Loop Header: Depth=1
	addq	$16, %rbp
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB2_9
# BB#10:
	cmpb	$82, %al
	je	.LBB2_12
# BB#11:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_12:                               # %.loopexit
	xorl	%edx, %edx
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	SetScope
	testl	%r14d, %r14d
	je	.LBB2_14
# BB#13:                                # %.loopexit._crit_edge
	movq	80(%rbx), %rdi
	movb	$1, %al
	jmp	.LBB2_15
.LBB2_14:
	movq	80(%rbx), %rdi
	cmpq	$0, 112(%rdi)
	setne	%al
.LBB2_15:
	movzbl	%al, %edx
	xorl	%esi, %esi
	callq	PushScope
	incl	(%r15)
.LBB2_16:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	SetScope, .Lfunc_end2-SetScope
	.cfi_endproc

	.globl	InitParser
	.p2align	4, 0x90
	.type	InitParser,@function
InitParser:                             # @InitParser
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 16
.Lcfi34:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	strlen
	cmpq	$2048, %rax             # imm = 0x800
	jb	.LBB3_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$6, %edi
	movl	$10, %esi
	movl	$.L.str.5, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r9
	callq	Error
.LBB3_2:
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movq	%rbx, %rsi
	callq	MakeWord
	movq	%rax, cross_name(%rip)
	movslq	ttop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, ttop(%rip)
	cmpl	$100, %ecx
	jge	.LBB3_4
# BB#3:
	movq	no_fpos(%rip), %rsi
	movq	StartSym(%rip), %r9
	movl	$110, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$100, %r8d
	callq	NewToken
	movslq	ttop(%rip), %rcx
	movq	%rax, tok_stack(,%rcx,8)
	popq	%rbx
	retq
.LBB3_4:
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	popq	%rbx
	jmp	Error                   # TAILCALL
.Lfunc_end3:
	.size	InitParser, .Lfunc_end3-InitParser
	.cfi_endproc

	.globl	Parse
	.p2align	4, 0x90
	.type	Parse,@function
Parse:                                  # @Parse
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi41:
	.cfi_def_cfa_offset 160
.Lcfi42:
	.cfi_offset %rbx, -56
.Lcfi43:
	.cfi_offset %r12, -48
.Lcfi44:
	.cfi_offset %r13, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movl	%ecx, 68(%rsp)          # 4-byte Spill
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	ttop(%rip), %edx
	movq	(%r15), %rax
	movb	32(%rax), %cl
	cmpb	$102, %cl
	movl	%edx, 20(%rsp)          # 4-byte Spill
	movl	%edx, %eax
	je	.LBB4_3
# BB#1:
	cmpb	$104, %cl
	movl	20(%rsp), %eax          # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	je	.LBB4_3
# BB#2:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	ttop(%rip), %eax
.LBB4_3:
	leal	1(%rax), %ecx
	movl	%ecx, ttop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_5
# BB#4:
	movq	(%r15), %rax
	movslq	%ecx, %rcx
	movq	%rax, tok_stack(,%rcx,8)
	jmp	.LBB4_6
.LBB4_5:
	cltq
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_6:
	callq	LexGetToken
	movq	%rax, 8(%rsp)
	testl	%ebx, %ebx
	movq	%r14, 24(%rsp)          # 8-byte Spill
	je	.LBB4_55
# BB#7:
	leaq	8(%rsp), %rdi
	movl	$143, %edx
	movq	%r14, %rsi
	callq	ReadDefinitions
	callq	ErrorSeen
	testl	%eax, %eax
	je	.LBB4_9
# BB#8:
	movq	8(%rsp), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$14, %esi
	movl	$.L.str.9, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_9:
	cmpq	%r14, StartSym(%rip)
	jne	.LBB4_55
# BB#10:
	movzbl	zz_lengths+82(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_12
# BB#11:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB4_13
.LBB4_12:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB4_13:
	movb	$82, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, 32(%rsp)
	leaq	8(%rsp), %r12
	leaq	80(%rsp), %rbx
	leaq	56(%rsp), %r13
	jmp	.LBB4_16
	.p2align	4, 0x90
.LBB4_15:                               # %.backedge1547
                                        #   in Loop: Header=BB4_16 Depth=1
	movq	8(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	callq	LexGetToken
	movq	%rax, 8(%rsp)
.LBB4_16:                               # %.backedge1547
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_24 Depth 2
	movq	8(%rsp), %rbp
	movb	32(%rbp), %al
	movzbl	%al, %edi
	cmpb	$115, %dil
	jg	.LBB4_29
# BB#17:                                # %.backedge1547
                                        #   in Loop: Header=BB4_16 Depth=1
	movl	%eax, %ecx
	addb	$-114, %cl
	cmpb	$2, %cl
	jb	.LBB4_14
# BB#18:                                # %.backedge1547
                                        #   in Loop: Header=BB4_16 Depth=1
	cmpb	$11, %al
	je	.LBB4_31
# BB#19:                                # %.backedge1547
                                        #   in Loop: Header=BB4_16 Depth=1
	cmpb	$106, %al
	jne	.LBB4_51
# BB#20:                                #   in Loop: Header=BB4_16 Depth=1
	movq	%rbp, zz_hold(%rip)
	movzbl	32(%rbp), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbp), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbp)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	callq	LexGetToken
	movq	%rax, %rbp
	movq	%rbp, 8(%rsp)
	cmpb	$102, 32(%rbp)
	je	.LBB4_22
# BB#21:                                #   in Loop: Header=BB4_16 Depth=1
	addq	$32, %rbp
	movq	$.L.str.18, (%rsp)
	movl	$6, %edi
	movl	$15, %esi
	movl	$.L.str.16, %edx
	movl	$1, %ecx
	movl	$.L.str.17, %r9d
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
.LBB4_22:                               #   in Loop: Header=BB4_16 Depth=1
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	Parse
	movq	%rax, %r14
	movb	32(%r14), %al
	movq	%r14, %rbp
	addq	$32, %rbp
	movl	%eax, %ecx
	addb	$-6, %cl
	cmpb	$2, %cl
	jae	.LBB4_42
# BB#23:                                #   in Loop: Header=BB4_16 Depth=1
	movq	8(%r14), %rax
	.p2align	4, 0x90
.LBB4_24:                               #   Parent Loop BB4_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rax), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB4_24
# BB#25:                                #   in Loop: Header=BB4_16 Depth=1
	cmpb	$2, %cl
	jne	.LBB4_47
# BB#26:                                #   in Loop: Header=BB4_16 Depth=1
	movq	$0, 72(%rsp)
	movq	32(%rsp), %rsi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	leaq	72(%rsp), %rcx
	movq	%r13, %r8
	callq	CrossExpand
	movq	%rax, %r14
	cmpq	$0, 72(%rsp)
	je	.LBB4_28
# BB#27:                                #   in Loop: Header=BB4_16 Depth=1
	movq	%r14, %r8
	addq	$32, %r8
	movq	$.L.str.21, (%rsp)
	movl	$6, %edi
	movl	$16, %esi
	movl	$.L.str.19, %edx
	movl	$1, %ecx
	movl	$.L.str.20, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_28:                               #   in Loop: Header=BB4_16 Depth=1
	movq	%r14, %rdi
	callq	HuntCommandOptions
	movq	56(%rsp), %rdi
	movq	%r14, %rsi
	callq	AttachEnv
	movq	32(%rsp), %rsi
	jmp	.LBB4_46
	.p2align	4, 0x90
.LBB4_29:                               # %.backedge1547
                                        #   in Loop: Header=BB4_16 Depth=1
	addb	$-116, %al
	cmpb	$2, %al
	jae	.LBB4_51
# BB#30:                                #   in Loop: Header=BB4_16 Depth=1
	movq	%r14, %rsi
	callq	ReadDatabaseDef
	jmp	.LBB4_15
	.p2align	4, 0x90
.LBB4_14:                               #   in Loop: Header=BB4_16 Depth=1
	movq	%r14, %rsi
	callq	ReadPrependDef
	jmp	.LBB4_15
.LBB4_31:                               #   in Loop: Header=BB4_16 Depth=1
	cmpb	$100, 64(%rbp)
	jne	.LBB4_35
# BB#32:                                #   in Loop: Header=BB4_16 Depth=1
	cmpb	$101, 65(%rbp)
	jne	.LBB4_35
# BB#33:                                #   in Loop: Header=BB4_16 Depth=1
	cmpb	$102, 66(%rbp)
	jne	.LBB4_35
# BB#34:                                #   in Loop: Header=BB4_16 Depth=1
	cmpb	$0, 67(%rbp)
	je	.LBB4_40
.LBB4_35:                               # %.thread1763
                                        #   in Loop: Header=BB4_16 Depth=1
	addq	$64, %rbp
	movl	$.L.str.11, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_40
# BB#36:                                #   in Loop: Header=BB4_16 Depth=1
	movl	$.L.str.12, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_40
# BB#37:                                #   in Loop: Header=BB4_16 Depth=1
	movl	$.L.str.13, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_40
# BB#38:                                #   in Loop: Header=BB4_16 Depth=1
	movl	$.L.str.14, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_40
# BB#39:                                #   in Loop: Header=BB4_16 Depth=1
	movl	$.L.str.15, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB4_51
.LBB4_40:                               #   in Loop: Header=BB4_16 Depth=1
	movl	$143, %edx
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	ReadDefinitions
	callq	ErrorSeen
	testl	%eax, %eax
	je	.LBB4_16
# BB#41:                                #   in Loop: Header=BB4_16 Depth=1
	movq	8(%rsp), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$39, %esi
	movl	$.L.str.9, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB4_16
.LBB4_42:                               #   in Loop: Header=BB4_16 Depth=1
	cmpb	$2, %al
	jne	.LBB4_48
# BB#43:                                #   in Loop: Header=BB4_16 Depth=1
	movq	80(%r14), %rdi
	cmpq	$0, 112(%rdi)
	je	.LBB4_45
# BB#44:                                #   in Loop: Header=BB4_16 Depth=1
	callq	SymName
	movq	%rbx, %r10
	movq	%rax, %rbx
	movq	$.L.str.18, (%rsp)
	movl	$6, %edi
	movl	$18, %esi
	movl	$.L.str.23, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	movq	%rbx, %r9
	movq	%r10, %rbx
	callq	Error
	movq	80(%r14), %rdi
.LBB4_45:                               #   in Loop: Header=BB4_16 Depth=1
	movq	%r14, 112(%rdi)
	movq	%r14, %rdi
	callq	HuntCommandOptions
	movq	32(%rsp), %rdi
	movq	%r14, %rsi
	callq	AttachEnv
	xorl	%esi, %esi
.LBB4_46:                               #   in Loop: Header=BB4_16 Depth=1
	movq	%r14, %rdi
	callq	SetEnv
	movq	%rax, 32(%rsp)
	jmp	.LBB4_50
.LBB4_47:                               #   in Loop: Header=BB4_16 Depth=1
	movl	$6, %edi
	movl	$17, %esi
	jmp	.LBB4_49
.LBB4_48:                               #   in Loop: Header=BB4_16 Depth=1
	movl	$6, %edi
	movl	$19, %esi
.LBB4_49:                               #   in Loop: Header=BB4_16 Depth=1
	movl	$.L.str.22, %edx
	movl	$1, %ecx
	movl	$.L.str.18, %r9d
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
.LBB4_50:                               #   in Loop: Header=BB4_16 Depth=1
	movq	80(%r14), %rdi
	xorl	%esi, %esi
	movl	$1, %edx
	callq	PushScope
	callq	LexGetToken
	movq	%rax, 8(%rsp)
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB4_16
.LBB4_51:                               # %.thread1764
	cmpl	$0, AllowCrossDb(%rip)
	je	.LBB4_53
# BB#52:
	movq	cross_name(%rip), %rsi
	addq	$64, %rsi
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	callq	MakeWord
	movq	%rax, %rdi
	callq	DbCreate
	movq	%rax, NewCrossDb(%rip)
	movq	cross_name(%rip), %rdi
	movl	InMemoryDbIndexes(%rip), %r8d
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	DbLoad
	jmp	.LBB4_54
.LBB4_53:
	movq	$0, NewCrossDb(%rip)
	xorl	%eax, %eax
.LBB4_54:
	movq	%rax, OldCrossDb(%rip)
	callq	FlattenUses
	movq	32(%rsp), %rdi
	callq	TransferInit
.LBB4_55:                               # %.preheader1545
                                        # implicit-def: %EAX
	movl	%eax, 44(%rsp)          # 4-byte Spill
	jmp	.LBB4_66
.LBB4_56:                               # %.critedge70
                                        #   in Loop: Header=BB4_66 Depth=1
	movl	%ebp, %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_58
# BB#57:                                #   in Loop: Header=BB4_66 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rdx
	movq	%rdx, zz_free(,%rdi,8)
	jmp	.LBB4_59
.LBB4_58:                               #   in Loop: Header=BB4_66 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	ttop(%rip), %ecx
.LBB4_59:                               #   in Loop: Header=BB4_66 Depth=1
	movb	%bpl, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movb	%bl, 40(%rax)
	movq	8(%rsp), %rdx
	movb	42(%rdx), %bl
	movb	%bl, 42(%rax)
	movb	41(%rdx), %bl
	movb	%bl, 41(%rax)
	movzwl	44(%rax), %esi
	andl	$64767, %esi            # imm = 0xFCFF
	orl	$512, %esi              # imm = 0x200
	movw	%si, 44(%rax)
	movzwl	34(%rdx), %esi
	movw	%si, 34(%rax)
	movl	36(%rdx), %esi
	movl	$1048575, %edi          # imm = 0xFFFFF
	andl	%edi, %esi
	movl	36(%rax), %edi
	movl	$-1048576, %ebp         # imm = 0xFFF00000
	andl	%ebp, %edi
	orl	%esi, %edi
	movl	%edi, 36(%rax)
	movl	36(%rdx), %edx
	andl	%ebp, %edx
	orl	%esi, %edx
	movl	%edx, 36(%rax)
	leal	1(%rcx), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_61
# BB#60:                                #   in Loop: Header=BB4_66 Depth=1
	movslq	%edx, %rcx
	movq	%rax, tok_stack(,%rcx,8)
	jmp	.LBB4_62
.LBB4_61:                               #   in Loop: Header=BB4_66 Depth=1
	movslq	%ecx, %rax
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_62:                               # %.critedge1389
                                        #   in Loop: Header=BB4_66 Depth=1
	movslq	ttop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, ttop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_64
# BB#63:                                #   in Loop: Header=BB4_66 Depth=1
	movq	8(%rsp), %rcx
	movq	%rcx, tok_stack+8(,%rax,8)
	jmp	.LBB4_65
.LBB4_64:                               #   in Loop: Header=BB4_66 Depth=1
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_65:                               #   in Loop: Header=BB4_66 Depth=1
	callq	LexGetToken
	movq	%rax, 8(%rsp)
	.p2align	4, 0x90
.LBB4_66:                               # %.backedge1546
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_451 Depth 2
                                        #       Child Loop BB4_222 Depth 3
                                        #       Child Loop BB4_483 Depth 3
                                        #         Child Loop BB4_486 Depth 4
                                        #       Child Loop BB4_477 Depth 3
                                        #         Child Loop BB4_480 Depth 4
                                        #       Child Loop BB4_82 Depth 3
                                        #       Child Loop BB4_586 Depth 3
                                        #         Child Loop BB4_588 Depth 4
                                        #       Child Loop BB4_70 Depth 3
                                        #       Child Loop BB4_555 Depth 3
                                        #       Child Loop BB4_653 Depth 3
                                        #       Child Loop BB4_75 Depth 3
                                        #       Child Loop BB4_614 Depth 3
                                        #       Child Loop BB4_648 Depth 3
                                        #       Child Loop BB4_643 Depth 3
                                        #       Child Loop BB4_638 Depth 3
                                        #       Child Loop BB4_515 Depth 3
                                        #         Child Loop BB4_518 Depth 4
                                        #       Child Loop BB4_512 Depth 3
                                        #       Child Loop BB4_504 Depth 3
                                        #         Child Loop BB4_507 Depth 4
                                        #       Child Loop BB4_501 Depth 3
                                        #       Child Loop BB4_126 Depth 3
                                        #       Child Loop BB4_465 Depth 3
                                        #         Child Loop BB4_469 Depth 4
                                        #       Child Loop BB4_534 Depth 3
                                        #       Child Loop BB4_625 Depth 3
                                        #       Child Loop BB4_459 Depth 3
                                        #         Child Loop BB4_462 Depth 4
                                        #       Child Loop BB4_456 Depth 3
                                        #       Child Loop BB4_609 Depth 3
                                        #       Child Loop BB4_100 Depth 3
                                        #       Child Loop BB4_262 Depth 3
                                        #         Child Loop BB4_274 Depth 4
                                        #           Child Loop BB4_275 Depth 5
                                        #         Child Loop BB4_288 Depth 4
                                        #         Child Loop BB4_290 Depth 4
                                        #           Child Loop BB4_292 Depth 5
                                        #       Child Loop BB4_302 Depth 3
                                        #         Child Loop BB4_303 Depth 4
                                        #         Child Loop BB4_309 Depth 4
                                        #           Child Loop BB4_310 Depth 5
                                        #       Child Loop BB4_337 Depth 3
                                        #       Child Loop BB4_379 Depth 3
                                        #       Child Loop BB4_400 Depth 3
                                        #       Child Loop BB4_418 Depth 3
                                        #       Child Loop BB4_423 Depth 3
                                        #       Child Loop BB4_329 Depth 3
                                        #       Child Loop BB4_658 Depth 3
                                        #     Child Loop BB4_666 Depth 2
	xorl	%r12d, %r12d
	jmp	.LBB4_451
.LBB4_67:                               # %.critedge124
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%ebp, %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_191
# BB#68:                                #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rdx
	movq	%rdx, zz_free(,%rdi,8)
	jmp	.LBB4_192
.LBB4_69:                               #   in Loop: Header=BB4_451 Depth=2
	movl	$7, %ebp
	movl	$100, %r14d
	.p2align	4, 0x90
.LBB4_70:                               # %.lr.ph1610
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	ttop(%rip), %rcx
	movq	tok_stack(,%rcx,8), %rax
	movzbl	40(%rax), %eax
	cmpl	%ebp, %eax
	jb	.LBB4_72
# BB#71:                                #   in Loop: Header=BB4_70 Depth=3
	callq	Reduce
	testl	%eax, %eax
	jne	.LBB4_70
	jmp	.LBB4_202
.LBB4_72:                               # %.critedge117
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%r14d, %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_198
# BB#73:                                #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rdx
	movq	%rdx, zz_free(,%rdi,8)
	jmp	.LBB4_199
.LBB4_74:                               #   in Loop: Header=BB4_451 Depth=2
	movl	$7, %ebx
	movl	$100, %ebp
	.p2align	4, 0x90
.LBB4_75:                               # %.lr.ph1614
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	ttop(%rip), %rcx
	movq	tok_stack(,%rcx,8), %rax
	movzbl	40(%rax), %eax
	cmpl	%ebx, %eax
	jb	.LBB4_77
# BB#76:                                #   in Loop: Header=BB4_75 Depth=3
	callq	Reduce
	testl	%eax, %eax
	jne	.LBB4_75
	jmp	.LBB4_209
.LBB4_77:                               # %.critedge105
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%ebp, %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_205
# BB#78:                                #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rdx
	movq	%rdx, zz_free(,%rdi,8)
	jmp	.LBB4_206
.LBB4_79:                               #   in Loop: Header=BB4_451 Depth=2
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_80:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	otop(%rip), %rax
	movq	obj_stack(,%rax,8), %rcx
	movb	32(%rcx), %al
	movl	%eax, %edx
	addb	$-6, %dl
	cmpb	$2, %dl
	jae	.LBB4_87
# BB#81:                                #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB4_93
	.p2align	4, 0x90
.LBB4_82:                               # %.preheader1541
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rax), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB4_82
# BB#83:                                # %.preheader1541
                                        #   in Loop: Header=BB4_451 Depth=2
	cmpb	$2, %cl
	jne	.LBB4_93
# BB#84:                                # %.loopexit1542.loopexit
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, %rcx
	jmp	.LBB4_88
.LBB4_85:                               #   in Loop: Header=BB4_451 Depth=2
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB4_86
.LBB4_87:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$2, %al
	jne	.LBB4_93
.LBB4_88:                               # %.loopexit1542
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	80(%rcx), %r14
	callq	LexGetToken
	movq	%rax, %rbp
	movq	%rbp, 8(%rsp)
	testq	%r14, %r14
	je	.LBB4_94
# BB#89:                                #   in Loop: Header=BB4_451 Depth=2
	movb	32(%rbp), %al
	cmpb	$102, %al
	je	.LBB4_91
# BB#90:                                #   in Loop: Header=BB4_451 Depth=2
	cmpb	$104, %al
	jne	.LBB4_360
.LBB4_91:                               #   in Loop: Header=BB4_451 Depth=2
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	PushScope
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	8(%rsp), %rdi
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rsi
	callq	Parse
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_361
# BB#92:                                #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, obj_stack+8(,%rcx,8)
	jmp	.LBB4_362
.LBB4_93:                               # %.thread1471
                                        #   in Loop: Header=BB4_451 Depth=2
	callq	LexGetToken
	movq	%rax, 8(%rsp)
.LBB4_94:                               #   in Loop: Header=BB4_451 Depth=2
	xorl	%r12d, %r12d
	movl	$6, %edi
	movl	$35, %esi
	movl	$.L.str.53, %edx
	movl	$2, %ecx
	movl	$.L.str.54, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
.LBB4_95:                               # %.backedge1546
                                        #   in Loop: Header=BB4_451 Depth=2
	callq	Error
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB4_451
.LBB4_96:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$53, %al
	je	.LBB4_215
# BB#97:                                #   in Loop: Header=BB4_451 Depth=2
	cmpb	$54, %al
	jne	.LBB4_221
# BB#98:                                #   in Loop: Header=BB4_451 Depth=2
	movq	RawVerbatimSym(%rip), %rax
	movq	8(%rsp), %rcx
	movq	%rax, 80(%rcx)
	movq	%rbx, zz_hold(%rip)
	movzbl	zz_lengths+54(%rip), %eax
	jmp	.LBB4_216
.LBB4_99:                               #   in Loop: Header=BB4_451 Depth=2
	movl	$7, %ebx
	movl	$100, %r14d
	.p2align	4, 0x90
.LBB4_100:                              # %.lr.ph
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	ttop(%rip), %rcx
	movq	tok_stack(,%rcx,8), %rax
	movzbl	40(%rax), %eax
	cmpl	%ebx, %eax
	jb	.LBB4_102
# BB#101:                               #   in Loop: Header=BB4_100 Depth=3
	callq	Reduce
	testl	%eax, %eax
	jne	.LBB4_100
	jmp	.LBB4_235
.LBB4_102:                              # %.critedge127
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%r14d, %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_231
# BB#103:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rdx
	movq	%rdx, zz_free(,%rdi,8)
	jmp	.LBB4_232
.LBB4_104:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	ttop(%rip), %ecx
.LBB4_105:                              #   in Loop: Header=BB4_451 Depth=2
	movb	%bpl, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movb	%bl, 40(%rax)
	movq	8(%rsp), %rdx
	movb	42(%rdx), %bl
	movb	%bl, 42(%rax)
	movb	41(%rdx), %bl
	movb	%bl, 41(%rax)
	movzwl	44(%rax), %esi
	andl	$64767, %esi            # imm = 0xFCFF
	orl	$512, %esi              # imm = 0x200
	movw	%si, 44(%rax)
	movzwl	34(%rdx), %esi
	movw	%si, 34(%rax)
	movl	36(%rdx), %esi
	movl	$1048575, %edi          # imm = 0xFFFFF
	andl	%edi, %esi
	movl	36(%rax), %edi
	movl	$-1048576, %ebp         # imm = 0xFFF00000
	andl	%ebp, %edi
	orl	%esi, %edi
	movl	%edi, 36(%rax)
	movl	36(%rdx), %edx
	andl	%ebp, %edx
	orl	%esi, %edx
	movl	%edx, 36(%rax)
	leal	1(%rcx), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_107
# BB#106:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	%edx, %rcx
	movq	%rax, tok_stack(,%rcx,8)
	jmp	.LBB4_108
.LBB4_107:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	%ecx, %rax
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_108:                              # %.critedge
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_211
# BB#109:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, obj_stack+8(,%rcx,8)
	jmp	.LBB4_213
.LBB4_110:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	ttop(%rip), %ecx
.LBB4_111:                              #   in Loop: Header=BB4_451 Depth=2
	movb	%r14b, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movb	%bpl, 40(%rax)
	movq	8(%rsp), %rsi
	movb	42(%rsi), %dl
	movb	%dl, 42(%rax)
	movb	41(%rsi), %dl
	movb	%dl, 41(%rax)
	movzwl	44(%rax), %edx
	andl	$64767, %edx            # imm = 0xFCFF
	orl	$512, %edx              # imm = 0x200
	movw	%dx, 44(%rax)
	movzwl	34(%rsi), %edx
	movw	%dx, 34(%rax)
	movl	36(%rsi), %edx
	movl	$1048575, %edi          # imm = 0xFFFFF
	andl	%edi, %edx
	movl	36(%rax), %edi
	movl	$-1048576, %ebp         # imm = 0xFFF00000
	andl	%ebp, %edi
	orl	%edx, %edi
	movl	%edi, 36(%rax)
	movl	36(%rsi), %esi
	andl	%ebp, %esi
	orl	%edx, %esi
	movl	%esi, 36(%rax)
	leal	1(%rcx), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_113
# BB#112:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	%edx, %rcx
	movq	%rax, tok_stack(,%rcx,8)
	jmp	.LBB4_114
.LBB4_113:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	%ecx, %rax
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_114:                              # %.critedge1383
                                        #   in Loop: Header=BB4_451 Depth=2
	movslq	ttop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, ttop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_116
# BB#115:                               #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rsp), %rcx
	movq	%rcx, tok_stack+8(,%rax,8)
	jmp	.LBB4_117
.LBB4_116:                              #   in Loop: Header=BB4_451 Depth=2
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	8(%rsp), %rcx
.LBB4_117:                              #   in Loop: Header=BB4_451 Depth=2
	movq	80(%rcx), %rax
	movzwl	41(%rax), %eax
	xorl	%ebp, %ebp
	testb	$8, %al
	movq	24(%rsp), %r14          # 8-byte Reload
	jne	.LBB4_119
# BB#118:                               #   in Loop: Header=BB4_451 Depth=2
	callq	Reduce
	movl	%eax, %ebp
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, ttop(%rip)
	je	.LBB4_148
.LBB4_119:                              #   in Loop: Header=BB4_451 Depth=2
	callq	LexGetToken
	movq	%rax, 8(%rsp)
	movb	32(%rax), %cl
	cmpb	$102, %cl
	je	.LBB4_122
# BB#120:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$104, %cl
	je	.LBB4_122
# BB#121:                               #   in Loop: Header=BB4_451 Depth=2
	movq	$.L.str.30, (%rsp)
	movl	$6, %edi
	movl	$40, %esi
	movl	$.L.str.28, %edx
	movl	$1, %ecx
	movl	$.L.str.29, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	movq	8(%rsp), %rax
.LBB4_122:                              #   in Loop: Header=BB4_451 Depth=2
	cmpb	$53, 32(%r13)
	movl	$RawVerbatimSym, %ecx
	movl	$VerbatimSym, %edx
	cmoveq	%rdx, %rcx
	movq	(%rcx), %rcx
	movq	%rcx, 80(%rax)
	testl	%ebp, %ebp
	je	.LBB4_134
# BB#123:                               #   in Loop: Header=BB4_451 Depth=2
	movzbl	41(%rax), %ecx
	movzbl	42(%rax), %edx
	addl	%ecx, %edx
	jne	.LBB4_125
# BB#124:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$102, 32(%rax)
	movl	$103, %ebx
	movl	$7, %eax
	cmovel	%eax, %ebx
	cmpl	$2, %ebp
	cmovel	%eax, %ebx
	movl	$101, %ebp
	jmp	.LBB4_126
.LBB4_125:                              #   in Loop: Header=BB4_451 Depth=2
	movl	$7, %ebx
	movl	$100, %ebp
	.p2align	4, 0x90
.LBB4_126:                              # %.lr.ph1622
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	ttop(%rip), %rcx
	movq	tok_stack(,%rcx,8), %rax
	movzbl	40(%rax), %eax
	cmpl	%ebx, %eax
	jb	.LBB4_128
# BB#127:                               #   in Loop: Header=BB4_126 Depth=3
	callq	Reduce
	testl	%eax, %eax
	jne	.LBB4_126
	jmp	.LBB4_134
.LBB4_128:                              # %.critedge61
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%ebp, %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_130
# BB#129:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rdx
	movq	%rdx, zz_free(,%rdi,8)
	jmp	.LBB4_131
.LBB4_130:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	ttop(%rip), %ecx
.LBB4_131:                              #   in Loop: Header=BB4_451 Depth=2
	movb	%bpl, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movb	%bl, 40(%rax)
	movq	8(%rsp), %rdx
	movb	42(%rdx), %bl
	movb	%bl, 42(%rax)
	movb	41(%rdx), %bl
	movb	%bl, 41(%rax)
	movzwl	44(%rax), %esi
	andl	$64767, %esi            # imm = 0xFCFF
	orl	$512, %esi              # imm = 0x200
	movw	%si, 44(%rax)
	movzwl	34(%rdx), %esi
	movw	%si, 34(%rax)
	movl	36(%rdx), %esi
	movl	$1048575, %edi          # imm = 0xFFFFF
	andl	%edi, %esi
	movl	36(%rax), %edi
	movl	$-1048576, %ebp         # imm = 0xFFF00000
	andl	%ebp, %edi
	orl	%esi, %edi
	movl	%edi, 36(%rax)
	movl	36(%rdx), %edx
	andl	%ebp, %edx
	orl	%esi, %edx
	movl	%edx, 36(%rax)
	leal	1(%rcx), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_133
# BB#132:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	%edx, %rcx
	movq	%rax, tok_stack(,%rcx,8)
	jmp	.LBB4_134
.LBB4_133:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	%ecx, %rax
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_134:                              # %.critedge1385
                                        #   in Loop: Header=BB4_451 Depth=2
	movslq	ttop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, ttop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_136
# BB#135:                               #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rsp), %rcx
	movq	%rcx, tok_stack+8(,%rax,8)
	jmp	.LBB4_137
.LBB4_136:                              #   in Loop: Header=BB4_451 Depth=2
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	8(%rsp), %rcx
.LBB4_137:                              #   in Loop: Header=BB4_451 Depth=2
	xorl	%esi, %esi
	cmpb	$104, 32(%rcx)
	leaq	32(%rcx), %rdx
	sete	%sil
	xorl	%ecx, %ecx
	cmpb	$54, 32(%r13)
	sete	%cl
	xorl	%edi, %edi
	callq	LexScanVerbatim
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_211
# BB#138:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, obj_stack+8(,%rcx,8)
	jmp	.LBB4_213
.LBB4_139:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	ttop(%rip), %ecx
.LBB4_140:                              #   in Loop: Header=BB4_451 Depth=2
	movb	%bpl, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movb	%bl, 40(%rax)
	movq	8(%rsp), %rdx
	movb	42(%rdx), %bl
	movb	%bl, 42(%rax)
	movb	41(%rdx), %bl
	movb	%bl, 41(%rax)
	movzwl	44(%rax), %esi
	andl	$64767, %esi            # imm = 0xFCFF
	orl	$512, %esi              # imm = 0x200
	movw	%si, 44(%rax)
	movzwl	34(%rdx), %esi
	movw	%si, 34(%rax)
	movl	36(%rdx), %esi
	movl	$1048575, %edi          # imm = 0xFFFFF
	andl	%edi, %esi
	movl	36(%rax), %edi
	movl	$-1048576, %ebp         # imm = 0xFFF00000
	andl	%ebp, %edi
	orl	%esi, %edi
	movl	%edi, 36(%rax)
	movl	36(%rdx), %edx
	andl	%ebp, %edx
	orl	%esi, %edx
	movl	%edx, 36(%rax)
	leal	1(%rcx), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_142
# BB#141:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	%edx, %rcx
	movq	%rax, tok_stack(,%rcx,8)
	jmp	.LBB4_143
.LBB4_142:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	%ecx, %rax
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_143:                              # %.critedge1387
                                        #   in Loop: Header=BB4_451 Depth=2
	movslq	ttop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, ttop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_145
# BB#144:                               #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rsp), %rcx
	movq	%rcx, tok_stack+8(,%rax,8)
	jmp	.LBB4_146
.LBB4_145:                              #   in Loop: Header=BB4_451 Depth=2
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	8(%rsp), %rcx
.LBB4_146:                              #   in Loop: Header=BB4_451 Depth=2
	movq	80(%rcx), %rax
	movzwl	41(%rax), %eax
	xorl	%r12d, %r12d
	testb	$8, %al
	jne	.LBB4_450
.LBB4_86:                               #   in Loop: Header=BB4_451 Depth=2
	callq	Reduce
	movl	%eax, %r12d
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, ttop(%rip)
	jne	.LBB4_450
	jmp	.LBB4_148
.LBB4_149:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	ttop(%rip), %ecx
.LBB4_150:                              #   in Loop: Header=BB4_451 Depth=2
	movb	%bpl, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movb	%bl, 40(%rax)
	movq	8(%rsp), %rdx
	movb	42(%rdx), %bl
	movb	%bl, 42(%rax)
	movb	41(%rdx), %bl
	movb	%bl, 41(%rax)
	movzwl	44(%rax), %esi
	andl	$64767, %esi            # imm = 0xFCFF
	orl	$512, %esi              # imm = 0x200
	movw	%si, 44(%rax)
	movzwl	34(%rdx), %esi
	movw	%si, 34(%rax)
	movl	36(%rdx), %esi
	movl	$1048575, %edi          # imm = 0xFFFFF
	andl	%edi, %esi
	movl	36(%rax), %edi
	movl	$-1048576, %ebp         # imm = 0xFFF00000
	andl	%ebp, %edi
	orl	%esi, %edi
	movl	%edi, 36(%rax)
	movl	36(%rdx), %edx
	andl	%ebp, %edx
	orl	%esi, %edx
	movl	%edx, 36(%rax)
	leal	1(%rcx), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_152
# BB#151:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	%edx, %rcx
	movq	%rax, tok_stack(,%rcx,8)
	jmp	.LBB4_153
.LBB4_152:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	%ecx, %rax
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_153:                              # %.critedge1375
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_211
# BB#154:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, obj_stack+8(,%rcx,8)
	jmp	.LBB4_213
.LBB4_155:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	ttop(%rip), %ecx
.LBB4_156:                              #   in Loop: Header=BB4_451 Depth=2
	movb	%r14b, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movb	%bpl, 40(%rax)
	movb	42(%rbx), %dl
	movb	%dl, 42(%rax)
	movb	41(%rbx), %dl
	movb	%dl, 41(%rax)
	movzwl	44(%rax), %edx
	andl	$64767, %edx            # imm = 0xFCFF
	orl	$512, %edx              # imm = 0x200
	movw	%dx, 44(%rax)
	movzwl	34(%rbx), %edx
	movw	%dx, 34(%rax)
	movl	36(%rbx), %edx
	movl	$1048575, %esi          # imm = 0xFFFFF
	andl	%esi, %edx
	movl	36(%rax), %esi
	movl	$-1048576, %edi         # imm = 0xFFF00000
	andl	%edi, %esi
	orl	%edx, %esi
	movl	%esi, 36(%rax)
	movl	36(%rbx), %esi
	andl	%edi, %esi
	orl	%edx, %esi
	movl	%esi, 36(%rax)
	leal	1(%rcx), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_158
# BB#157:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	%edx, %rcx
	movq	%rax, tok_stack(,%rcx,8)
	jmp	.LBB4_159
.LBB4_158:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	%ecx, %rax
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_159:                              # %.critedge1397
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, zz_hold(%rip)
	movslq	otop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, otop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_161
# BB#160:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, obj_stack+8(,%rax,8)
	jmp	.LBB4_190
.LBB4_161:                              #   in Loop: Header=BB4_451 Depth=2
	movq	obj_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$1, %r12d
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB4_451
.LBB4_162:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	ttop(%rip), %ecx
.LBB4_163:                              #   in Loop: Header=BB4_451 Depth=2
	movb	%r14b, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movb	%bpl, 40(%rax)
	movb	42(%rbx), %dl
	movb	%dl, 42(%rax)
	movb	41(%rbx), %dl
	movb	%dl, 41(%rax)
	movzwl	44(%rax), %edx
	andl	$64767, %edx            # imm = 0xFCFF
	orl	$512, %edx              # imm = 0x200
	movw	%dx, 44(%rax)
	movzwl	34(%rbx), %edx
	movw	%dx, 34(%rax)
	movl	36(%rbx), %edx
	movl	$1048575, %esi          # imm = 0xFFFFF
	andl	%esi, %edx
	movl	36(%rax), %esi
	movl	$-1048576, %edi         # imm = 0xFFF00000
	andl	%edi, %esi
	orl	%edx, %esi
	movl	%esi, 36(%rax)
	movl	36(%rbx), %esi
	andl	%edi, %esi
	orl	%edx, %esi
	movl	%esi, 36(%rax)
	leal	1(%rcx), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_165
# BB#164:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	%edx, %rcx
	movq	%rax, tok_stack(,%rcx,8)
	jmp	.LBB4_166
.LBB4_165:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	%ecx, %rax
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_166:                              # %.critedge1391
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, zz_hold(%rip)
	movslq	otop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, otop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_188
# BB#167:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, obj_stack+8(,%rax,8)
	jmp	.LBB4_189
.LBB4_168:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	ttop(%rip), %ecx
.LBB4_169:                              #   in Loop: Header=BB4_451 Depth=2
	movb	%bpl, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movb	%bl, 40(%rax)
	movq	32(%rsp), %rdx
	movb	42(%rdx), %bl
	movb	%bl, 42(%rax)
	movb	41(%rdx), %bl
	movb	%bl, 41(%rax)
	movzwl	44(%rax), %esi
	andl	$64767, %esi            # imm = 0xFCFF
	orl	$512, %esi              # imm = 0x200
	movw	%si, 44(%rax)
	movzwl	34(%rdx), %esi
	movw	%si, 34(%rax)
	movl	36(%rdx), %esi
	movl	$1048575, %edi          # imm = 0xFFFFF
	andl	%edi, %esi
	movl	36(%rax), %edi
	movl	$-1048576, %ebp         # imm = 0xFFF00000
	andl	%ebp, %edi
	orl	%esi, %edi
	movl	%edi, 36(%rax)
	movl	36(%rdx), %edx
	andl	%ebp, %edx
	orl	%esi, %edx
	movl	%edx, 36(%rax)
	leal	1(%rcx), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_171
# BB#170:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	%edx, %rcx
	movq	%rax, tok_stack(,%rcx,8)
	jmp	.LBB4_172
.LBB4_171:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	%ecx, %rax
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_172:                              # %.critedge1393
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	32(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_180
# BB#173:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, obj_stack+8(,%rcx,8)
	jmp	.LBB4_181
.LBB4_174:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	ttop(%rip), %ecx
.LBB4_175:                              #   in Loop: Header=BB4_451 Depth=2
	movb	%bpl, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movb	%bl, 40(%rax)
	movq	32(%rsp), %rdx
	movb	42(%rdx), %bl
	movb	%bl, 42(%rax)
	movb	41(%rdx), %bl
	movb	%bl, 41(%rax)
	movzwl	44(%rax), %esi
	andl	$64767, %esi            # imm = 0xFCFF
	orl	$512, %esi              # imm = 0x200
	movw	%si, 44(%rax)
	movzwl	34(%rdx), %esi
	movw	%si, 34(%rax)
	movl	36(%rdx), %esi
	movl	$1048575, %edi          # imm = 0xFFFFF
	andl	%edi, %esi
	movl	36(%rax), %edi
	movl	$-1048576, %ebp         # imm = 0xFFF00000
	andl	%ebp, %edi
	orl	%esi, %edi
	movl	%edi, 36(%rax)
	movl	36(%rdx), %edx
	andl	%ebp, %edx
	orl	%esi, %edx
	movl	%edx, 36(%rax)
	leal	1(%rcx), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_177
# BB#176:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	%edx, %rcx
	movq	%rax, tok_stack(,%rcx,8)
	jmp	.LBB4_178
.LBB4_177:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	%ecx, %rax
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_178:                              # %.critedge1395
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	32(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_180
# BB#179:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, obj_stack+8(,%rcx,8)
	jmp	.LBB4_181
.LBB4_180:                              #   in Loop: Header=BB4_451 Depth=2
	movq	obj_stack(,%rcx,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_181:                              #   in Loop: Header=BB4_451 Depth=2
	callq	LexGetToken
	movq	%rax, 8(%rsp)
	movl	80(%rsp), %esi
	movq	32(%rsp), %rdx
	movzwl	34(%rax), %edi
	callq	EnvReadInsert
	movl	$1, %r12d
	jmp	.LBB4_451
.LBB4_182:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	ttop(%rip), %ecx
.LBB4_183:                              #   in Loop: Header=BB4_451 Depth=2
	movb	%r14b, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movb	%bpl, 40(%rax)
	movb	42(%r13), %dl
	movb	%dl, 42(%rax)
	movb	41(%r13), %dl
	movb	%dl, 41(%rax)
	movzwl	44(%rax), %edx
	andl	$64767, %edx            # imm = 0xFCFF
	orl	$512, %edx              # imm = 0x200
	movw	%dx, 44(%rax)
	movzwl	34(%r13), %edx
	movw	%dx, 34(%rax)
	movl	36(%r13), %edx
	movl	$1048575, %esi          # imm = 0xFFFFF
	andl	%esi, %edx
	movl	36(%rax), %esi
	movl	$-1048576, %edi         # imm = 0xFFF00000
	andl	%edi, %esi
	orl	%edx, %esi
	movl	%esi, 36(%rax)
	movl	36(%r13), %esi
	andl	%edi, %esi
	orl	%edx, %esi
	movl	%esi, 36(%rax)
	leal	1(%rcx), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_185
# BB#184:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	%edx, %rcx
	movq	%rax, tok_stack(,%rcx,8)
	jmp	.LBB4_186
.LBB4_185:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	%ecx, %rax
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_186:                              # %.critedge1401
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	%r13, zz_hold(%rip)
	movslq	otop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, otop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_188
# BB#187:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%r13, obj_stack+8(,%rax,8)
	jmp	.LBB4_189
.LBB4_188:                              #   in Loop: Header=BB4_451 Depth=2
	movq	obj_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_189:                              #   in Loop: Header=BB4_451 Depth=2
	callq	LexGetToken
	movq	%rax, 8(%rsp)
.LBB4_190:                              # %.backedge1546
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	$1, %r12d
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB4_451
.LBB4_191:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	ttop(%rip), %ecx
.LBB4_192:                              #   in Loop: Header=BB4_451 Depth=2
	movb	%bpl, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movb	%bl, 40(%rax)
	movb	42(%r13), %dl
	movb	%dl, 42(%rax)
	movb	41(%r13), %dl
	movb	%dl, 41(%rax)
	movzwl	44(%rax), %edx
	andl	$64767, %edx            # imm = 0xFCFF
	orl	$512, %edx              # imm = 0x200
	movw	%dx, 44(%rax)
	movzwl	34(%r13), %edx
	movw	%dx, 34(%rax)
	movl	36(%r13), %edx
	movl	$1048575, %esi          # imm = 0xFFFFF
	andl	%esi, %edx
	movl	36(%rax), %esi
	movl	$-1048576, %edi         # imm = 0xFFF00000
	andl	%edi, %esi
	orl	%edx, %esi
	movl	%esi, 36(%rax)
	movl	36(%r13), %esi
	andl	%edi, %esi
	orl	%edx, %esi
	movl	%esi, 36(%rax)
	leal	1(%rcx), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_194
# BB#193:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	%edx, %rcx
	movq	%rax, tok_stack(,%rcx,8)
	jmp	.LBB4_195
.LBB4_194:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	%ecx, %rax
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_195:                              # %.critedge1405
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	%r13, zz_hold(%rip)
	movslq	otop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, otop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_197
# BB#196:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%r13, obj_stack+8(,%rax,8)
	movl	$1, %r12d
	jmp	.LBB4_451
.LBB4_197:                              #   in Loop: Header=BB4_451 Depth=2
	movq	obj_stack(,%rax,8), %r8
	jmp	.LBB4_359
.LBB4_198:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	ttop(%rip), %ecx
.LBB4_199:                              #   in Loop: Header=BB4_451 Depth=2
	movb	%r14b, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movb	%bpl, 40(%rax)
	movb	42(%rbx), %dl
	movb	%dl, 42(%rax)
	movb	41(%rbx), %dl
	movb	%dl, 41(%rax)
	movzwl	44(%rax), %edx
	andl	$64767, %edx            # imm = 0xFCFF
	orl	$512, %edx              # imm = 0x200
	movw	%dx, 44(%rax)
	movzwl	34(%rbx), %edx
	movw	%dx, 34(%rax)
	movl	36(%rbx), %edx
	movl	$1048575, %esi          # imm = 0xFFFFF
	andl	%esi, %edx
	movl	36(%rax), %esi
	movl	$-1048576, %edi         # imm = 0xFFF00000
	andl	%edi, %esi
	orl	%edx, %esi
	movl	%esi, 36(%rax)
	movl	36(%rbx), %esi
	andl	%edi, %esi
	orl	%edx, %esi
	movl	%esi, 36(%rax)
	leal	1(%rcx), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_201
# BB#200:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	%edx, %rcx
	movq	%rax, tok_stack(,%rcx,8)
	jmp	.LBB4_202
.LBB4_201:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	%ecx, %rax
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_202:                              # %.critedge1403
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, zz_hold(%rip)
	movslq	otop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, otop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_204
# BB#203:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, obj_stack+8(,%rax,8)
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB4_213
.LBB4_204:                              #   in Loop: Header=BB4_451 Depth=2
	movq	obj_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB4_213
.LBB4_205:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	ttop(%rip), %ecx
.LBB4_206:                              #   in Loop: Header=BB4_451 Depth=2
	movb	%bpl, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movb	%bl, 40(%rax)
	movq	32(%rsp), %rdx
	movb	42(%rdx), %bl
	movb	%bl, 42(%rax)
	movb	41(%rdx), %bl
	movb	%bl, 41(%rax)
	movzwl	44(%rax), %esi
	andl	$64767, %esi            # imm = 0xFCFF
	orl	$512, %esi              # imm = 0x200
	movw	%si, 44(%rax)
	movzwl	34(%rdx), %esi
	movw	%si, 34(%rax)
	movl	36(%rdx), %esi
	movl	$1048575, %edi          # imm = 0xFFFFF
	andl	%edi, %esi
	movl	36(%rax), %edi
	movl	$-1048576, %ebp         # imm = 0xFFF00000
	andl	%ebp, %edi
	orl	%esi, %edi
	movl	%edi, 36(%rax)
	movl	36(%rdx), %edx
	andl	%ebp, %edx
	orl	%esi, %edx
	movl	%edx, 36(%rax)
	leal	1(%rcx), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_208
# BB#207:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	%edx, %rcx
	movq	%rax, tok_stack(,%rcx,8)
	jmp	.LBB4_209
.LBB4_208:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	%ecx, %rax
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_209:                              # %.critedge1399
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	32(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_211
# BB#210:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, obj_stack+8(,%rcx,8)
	jmp	.LBB4_213
.LBB4_211:                              #   in Loop: Header=BB4_451 Depth=2
	movq	obj_stack(,%rcx,8), %r8
.LBB4_212:                              #   in Loop: Header=BB4_451 Depth=2
	addq	$32, %r8
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_213:                              #   in Loop: Header=BB4_451 Depth=2
	callq	LexGetToken
	movq	%rax, 8(%rsp)
	movl	$1, %r12d
	jmp	.LBB4_451
.LBB4_221:                              #   in Loop: Header=BB4_451 Depth=2
	movl	$6, %edi
	movl	$25, %esi
	movl	$.L.str.36, %edx
	movl	$2, %ecx
	movl	$.L.str.35, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rsp), %rax
	movq	$0, 80(%rax)
	testl	%r12d, %r12d
	jne	.LBB4_219
	jmp	.LBB4_222
.LBB4_214:                              #   in Loop: Header=BB4_451 Depth=2
	movq	80(%rbx), %rax
	movq	8(%rsp), %rcx
	movq	%rax, 80(%rcx)
	movq	%rbx, zz_hold(%rip)
	movzbl	zz_lengths+2(%rip), %eax
	jmp	.LBB4_216
.LBB4_215:                              #   in Loop: Header=BB4_451 Depth=2
	movq	VerbatimSym(%rip), %rax
	movq	8(%rsp), %rcx
	movq	%rax, 80(%rcx)
	movq	%rbx, zz_hold(%rip)
	movzbl	zz_lengths+53(%rip), %eax
.LBB4_216:                              # %.preheader1516
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	xorl	%ebx, %ebx
	testl	%r12d, %r12d
	jne	.LBB4_219
	jmp	.LBB4_222
.LBB4_217:                              #   in Loop: Header=BB4_451 Depth=2
	movq	obj_stack(,%rcx,8), %r8
	addq	$32, %r8
	movl	$1, %r12d
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	.p2align	4, 0x90
.LBB4_218:                              # %.backedge1517
                                        #   in Loop: Header=BB4_451 Depth=2
	testl	%r12d, %r12d
	je	.LBB4_222
.LBB4_219:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	ttop(%rip), %rax
	movq	tok_stack(,%rax,8), %r8
	movb	40(%r8), %dl
	movq	8(%rsp), %rcx
	cmpb	40(%rcx), %dl
	jb	.LBB4_224
# BB#220:                               #   in Loop: Header=BB4_451 Depth=2
	callq	Reduce
	movl	%eax, %r12d
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, ttop(%rip)
	jne	.LBB4_218
	jmp	.LBB4_468
	.p2align	4, 0x90
.LBB4_222:                              #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rsp), %rdx
	addq	$32, %rdx
	movl	$11, %edi
	movl	$.L.str.27, %esi
	callq	MakeWord
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_217
# BB#223:                               #   in Loop: Header=BB4_222 Depth=3
	movq	%rax, obj_stack+8(,%rcx,8)
	movl	$1, %r12d
	testl	%r12d, %r12d
	jne	.LBB4_219
	jmp	.LBB4_222
.LBB4_224:                              #   in Loop: Header=BB4_451 Depth=2
	leal	1(%rax), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_226
# BB#225:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rcx, tok_stack+8(,%rax,8)
	jmp	.LBB4_227
.LBB4_226:                              #   in Loop: Header=BB4_451 Depth=2
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_227:                              #   in Loop: Header=BB4_451 Depth=2
	callq	Reduce
	movl	%eax, %r12d
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, ttop(%rip)
	je	.LBB4_148
# BB#228:                               #   in Loop: Header=BB4_451 Depth=2
	testq	%rbx, %rbx
	jne	.LBB4_230
# BB#229:                               #   in Loop: Header=BB4_451 Depth=2
	callq	LexGetToken
	movq	%rax, %rbx
.LBB4_230:                              #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, 8(%rsp)
	jmp	.LBB4_451
.LBB4_231:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	ttop(%rip), %ecx
.LBB4_232:                              #   in Loop: Header=BB4_451 Depth=2
	movb	%r14b, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movb	%bl, 40(%rax)
	movb	42(%r13), %dl
	movb	%dl, 42(%rax)
	movb	41(%r13), %dl
	movb	%dl, 41(%rax)
	movzwl	44(%rax), %edx
	andl	$64767, %edx            # imm = 0xFCFF
	orl	$512, %edx              # imm = 0x200
	movw	%dx, 44(%rax)
	movzwl	34(%r13), %edx
	movw	%dx, 34(%rax)
	movl	36(%r13), %edx
	movl	$1048575, %esi          # imm = 0xFFFFF
	andl	%esi, %edx
	movl	36(%rax), %esi
	movl	$-1048576, %edi         # imm = 0xFFF00000
	andl	%edi, %esi
	orl	%edx, %esi
	movl	%esi, 36(%rax)
	movl	36(%r13), %esi
	andl	%edi, %esi
	orl	%edx, %esi
	movl	%esi, 36(%rax)
	leal	1(%rcx), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_234
# BB#233:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	%edx, %rcx
	movq	%rax, tok_stack(,%rcx,8)
	jmp	.LBB4_235
.LBB4_234:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	%ecx, %rax
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_235:                              # %.critedge1407
                                        #   in Loop: Header=BB4_451 Depth=2
	movslq	ttop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, ttop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_237
# BB#236:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%r13, tok_stack+8(,%rax,8)
	jmp	.LBB4_238
.LBB4_237:                              #   in Loop: Header=BB4_451 Depth=2
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_238:                              #   in Loop: Header=BB4_451 Depth=2
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movzwl	41(%rax), %eax
	xorl	%r12d, %r12d
	testb	$8, %al
	jne	.LBB4_240
# BB#239:                               #   in Loop: Header=BB4_451 Depth=2
	callq	Reduce
	movl	%eax, %r12d
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, ttop(%rip)
	je	.LBB4_409
.LBB4_240:                              #   in Loop: Header=BB4_451 Depth=2
	testq	%r14, %r14
	je	.LBB4_249
# BB#241:                               #   in Loop: Header=BB4_451 Depth=2
	cmpq	%r14, StartSym(%rip)
	je	.LBB4_249
# BB#242:                               #   in Loop: Header=BB4_451 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	movzwl	41(%rax), %eax
	testb	$64, %al
	jne	.LBB4_245
# BB#243:                               #   in Loop: Header=BB4_451 Depth=2
	testl	%ebp, %ebp
	je	.LBB4_249
# BB#244:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%r14, %rdi
	movq	48(%rsp), %rsi          # 8-byte Reload
	callq	InsertUses
	jmp	.LBB4_249
.LBB4_245:                              #   in Loop: Header=BB4_451 Depth=2
	movzwl	41(%r14), %ecx
	movzbl	43(%r14), %eax
	shll	$16, %eax
	orl	%ecx, %eax
	orl	$1048576, %eax          # imm = 0x100000
	movw	%cx, 41(%r14)
	movl	%eax, %edx
	shrl	$16, %edx
	movb	%dl, 43(%r14)
	testw	%cx, %cx
	js	.LBB4_247
# BB#246:                               #   in Loop: Header=BB4_451 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	movzwl	41(%rcx), %ecx
	shrl	$15, %ecx
	jmp	.LBB4_248
.LBB4_247:                              #   in Loop: Header=BB4_451 Depth=2
	movb	$1, %cl
.LBB4_248:                              #   in Loop: Header=BB4_451 Depth=2
	movzbl	%cl, %ecx
	shll	$15, %ecx
	andl	$16744447, %eax         # imm = 0xFF7FFF
	orl	%eax, %ecx
	shrl	$16, %eax
	movb	%al, 43(%r14)
	movw	%cx, 41(%r14)
.LBB4_249:                              # %.preheader1536
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	%r15, 96(%rsp)          # 8-byte Spill
	movl	$0, 64(%rsp)            # 4-byte Folded Spill
	movq	48(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB4_262
	.p2align	4, 0x90
.LBB4_296:                              # %.thread1468
                                        #   in Loop: Header=BB4_262 Depth=3
	callq	SymName
	movq	%rax, %rbp
	movq	80(%r13), %rdi
	callq	SymName
	movq	%rax, (%rsp)
	movl	$6, %edi
	movl	$31, %esi
	movl	$.L.str.46, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbp, %r9
	callq	Error
	movq	%r15, %rdi
	callq	DisposeObject
	jmp	.LBB4_297
	.p2align	4, 0x90
.LBB4_250:                              #   in Loop: Header=BB4_262 Depth=3
	testq	%r15, %r15
	je	.LBB4_297
# BB#251:                               #   in Loop: Header=BB4_262 Depth=3
	movq	80(%r15), %rax
	movb	126(%rax), %bl
	shrb	$6, %bl
	andb	$1, %bl
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	je	.LBB4_253
# BB#252:                               #   in Loop: Header=BB4_262 Depth=3
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB4_254
	.p2align	4, 0x90
.LBB4_297:                              #   in Loop: Header=BB4_262 Depth=3
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB4_259
.LBB4_253:                              #   in Loop: Header=BB4_262 Depth=3
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB4_254:                              #   in Loop: Header=BB4_262 Depth=3
	movzbl	%bl, %ecx
	testq	%r13, %r13
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	je	.LBB4_257
# BB#255:                               #   in Loop: Header=BB4_262 Depth=3
	testq	%rax, %rax
	je	.LBB4_257
# BB#256:                               #   in Loop: Header=BB4_262 Depth=3
	movq	(%r13), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rax, 8(%rsi)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rdx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rdx
	movq	%rax, 8(%rdx)
	movq	xx_link(%rip), %rax
.LBB4_257:                              #   in Loop: Header=BB4_262 Depth=3
	addl	%ecx, 64(%rsp)          # 4-byte Folded Spill
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB4_259
# BB#258:                               #   in Loop: Header=BB4_262 Depth=3
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	.p2align	4, 0x90
.LBB4_259:                              #   in Loop: Header=BB4_262 Depth=3
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbp, %rdi
	callq	PushScope
	cmpq	$0, 8(%rsp)
	jne	.LBB4_261
# BB#260:                               #   in Loop: Header=BB4_262 Depth=3
	callq	LexGetToken
	movq	%rax, 8(%rsp)
.LBB4_261:                              # %.thread1469
                                        #   in Loop: Header=BB4_262 Depth=3
	callq	PopScope
.LBB4_262:                              #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_274 Depth 4
                                        #           Child Loop BB4_275 Depth 5
                                        #         Child Loop BB4_288 Depth 4
                                        #         Child Loop BB4_290 Depth 4
                                        #           Child Loop BB4_292 Depth 5
	movq	8(%rsp), %r15
	movb	32(%r15), %al
	cmpb	$2, %al
	jne	.LBB4_265
# BB#263:                               #   in Loop: Header=BB4_262 Depth=3
	movq	80(%r15), %rcx
	cmpq	%rbp, 48(%rcx)
	jne	.LBB4_265
# BB#264:                               #   in Loop: Header=BB4_262 Depth=3
	cmpb	$-111, 32(%rcx)
	je	.LBB4_268
	.p2align	4, 0x90
.LBB4_265:                              #   in Loop: Header=BB4_262 Depth=3
	cmpb	$102, 32(%r15)
	jne	.LBB4_298
# BB#266:                               #   in Loop: Header=BB4_262 Depth=3
	cmpb	$3, 40(%r15)
	je	.LBB4_298
# BB#267:                               # %.critedge130
                                        #   in Loop: Header=BB4_262 Depth=3
	cmpb	$2, %al
	jne	.LBB4_270
.LBB4_268:                              # %.critedge130.thread
                                        #   in Loop: Header=BB4_262 Depth=3
	callq	LexGetToken
	movq	%rax, 8(%rsp)
	cmpb	$102, 32(%rax)
	je	.LBB4_271
	jmp	.LBB4_269
	.p2align	4, 0x90
.LBB4_270:                              #   in Loop: Header=BB4_262 Depth=3
	movzbl	40(%r15), %esi
	movzbl	41(%r15), %ebp
	movzbl	42(%r15), %r14d
	leaq	32(%r15), %rbx
	movq	%r13, %rdi
	callq	ChildSymWithCode
	movl	$2, %edi
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	movl	%r14d, %edx
	movq	24(%rsp), %r14          # 8-byte Reload
	movl	%ebp, %ecx
	movq	%rax, %r9
	callq	NewToken
	movq	%rax, %r15
	movq	8(%rsp), %rax
	movb	$3, 40(%rax)
.LBB4_271:                              #   in Loop: Header=BB4_262 Depth=3
	movq	80(%r15), %rdi
	movq	96(%rdi), %rbp
	movl	$0, 44(%rsp)            # 4-byte Folded Spill
	testq	%rbp, %rbp
	je	.LBB4_278
# BB#272:                               # %.preheader1496
                                        #   in Loop: Header=BB4_262 Depth=3
	movq	8(%rbp), %rbx
	cmpq	%rbp, %rbx
	je	.LBB4_278
# BB#273:                               # %.preheader1489.preheader
                                        #   in Loop: Header=BB4_262 Depth=3
	movl	$0, 44(%rsp)            # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB4_274:                              # %.preheader1489
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        #       Parent Loop BB4_262 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB4_275 Depth 5
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB4_275:                              #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        #       Parent Loop BB4_262 Depth=3
                                        #         Parent Loop BB4_274 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	16(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB4_275
# BB#276:                               #   in Loop: Header=BB4_274 Depth=4
	movq	80(%rax), %rdi
	xorl	%esi, %esi
	movl	$1, %edx
	callq	PushScope
	incl	44(%rsp)                # 4-byte Folded Spill
	movq	8(%rbx), %rbx
	cmpq	%rbp, %rbx
	jne	.LBB4_274
# BB#277:                               # %.loopexit1497.loopexit
                                        #   in Loop: Header=BB4_262 Depth=3
	movq	80(%r15), %rdi
.LBB4_278:                              # %.loopexit1497
                                        #   in Loop: Header=BB4_262 Depth=3
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	PushScope
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	Parse
	movq	%rax, %rbp
	callq	PopScope
	movb	$10, 32(%r15)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_280
# BB#279:                               #   in Loop: Header=BB4_262 Depth=3
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB4_281
.LBB4_280:                              #   in Loop: Header=BB4_262 Depth=3
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB4_281:                              #   in Loop: Header=BB4_262 Depth=3
	movl	44(%rsp), %esi          # 4-byte Reload
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB4_283
# BB#282:                               #   in Loop: Header=BB4_262 Depth=3
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB4_283:                              #   in Loop: Header=BB4_262 Depth=3
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB4_286
# BB#284:                               #   in Loop: Header=BB4_262 Depth=3
	testq	%rax, %rax
	je	.LBB4_286
# BB#285:                               #   in Loop: Header=BB4_262 Depth=3
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB4_286:                              # %.preheader1495
                                        #   in Loop: Header=BB4_262 Depth=3
	testl	%esi, %esi
	jle	.LBB4_289
# BB#287:                               # %.lr.ph1593.preheader
                                        #   in Loop: Header=BB4_262 Depth=3
	movl	%esi, %ebx
	.p2align	4, 0x90
.LBB4_288:                              # %.lr.ph1593
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        #       Parent Loop BB4_262 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	callq	PopScope
	decl	%ebx
	jne	.LBB4_288
.LBB4_289:                              # %.preheader1494.preheader
                                        #   in Loop: Header=BB4_262 Depth=3
	leaq	32(%r15), %r14
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB4_290:                              # %.preheader1494
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        #       Parent Loop BB4_262 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB4_292 Depth 5
	movq	8(%rbp), %rbp
	cmpq	%r13, %rbp
	je	.LBB4_250
# BB#291:                               # %.preheader1487.preheader
                                        #   in Loop: Header=BB4_290 Depth=4
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB4_292:                              # %.preheader1487
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        #       Parent Loop BB4_262 Depth=3
                                        #         Parent Loop BB4_290 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB4_292
# BB#293:                               # %.preheader1487
                                        #   in Loop: Header=BB4_290 Depth=4
	cmpb	$10, %al
	je	.LBB4_295
# BB#294:                               #   in Loop: Header=BB4_290 Depth=4
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.45, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_295:                              # %.loopexit1488
                                        #   in Loop: Header=BB4_290 Depth=4
	movq	80(%r15), %rdi
	cmpq	80(%rbx), %rdi
	jne	.LBB4_290
	jmp	.LBB4_296
.LBB4_298:                              # %.critedge131.loopexit
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	44(%rsp), %eax          # 4-byte Reload
	movl	%eax, 56(%rsp)
	jmp	.LBB4_299
.LBB4_269:                              #   in Loop: Header=BB4_451 Depth=2
	leaq	32(%r15), %rbp
	movl	44(%rsp), %eax          # 4-byte Reload
	movl	%eax, 56(%rsp)
	movq	80(%r15), %rdi
	callq	SymName
	movq	%rax, (%rsp)
	movl	$6, %edi
	movl	$30, %esi
	movl	$.L.str.44, %edx
	movl	$2, %ecx
	movl	$.L.str.17, %r9d
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
	movq	%r15, zz_hold(%rip)
	movzbl	32(%r15), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%r15), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r15)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	48(%rsp), %rbp          # 8-byte Reload
.LBB4_299:                              # %.critedge131
                                        #   in Loop: Header=BB4_451 Depth=2
	movzwl	122(%rbp), %eax
	cmpl	%eax, 64(%rsp)          # 4-byte Folded Reload
	movq	96(%rsp), %r15          # 8-byte Reload
	jge	.LBB4_314
# BB#300:                               # %.preheader1533
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rbp), %rbx
	cmpq	%rbp, %rbx
	je	.LBB4_314
# BB#301:                               # %.preheader1492.lr.ph
                                        #   in Loop: Header=BB4_451 Depth=2
	leaq	32(%r13), %r14
	.p2align	4, 0x90
.LBB4_302:                              # %.preheader1492
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_303 Depth 4
                                        #         Child Loop BB4_309 Depth 4
                                        #           Child Loop BB4_310 Depth 5
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB4_303:                              #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        #       Parent Loop BB4_302 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	16(%rdi), %rdi
	movzbl	32(%rdi), %eax
	testb	%al, %al
	je	.LBB4_303
# BB#304:                               #   in Loop: Header=BB4_302 Depth=3
	cmpb	$-111, %al
	jne	.LBB4_313
# BB#305:                               #   in Loop: Header=BB4_302 Depth=3
	testb	$64, 126(%rdi)
	je	.LBB4_313
# BB#306:                               # %.preheader1491
                                        #   in Loop: Header=BB4_302 Depth=3
	movq	8(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB4_307
	.p2align	4, 0x90
.LBB4_309:                              # %.preheader
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        #       Parent Loop BB4_302 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB4_310 Depth 5
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB4_310:                              #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        #       Parent Loop BB4_302 Depth=3
                                        #         Parent Loop BB4_309 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB4_310
# BB#311:                               #   in Loop: Header=BB4_309 Depth=4
	cmpb	$10, %dl
	jne	.LBB4_308
# BB#312:                               #   in Loop: Header=BB4_309 Depth=4
	cmpq	%rdi, 80(%rcx)
	je	.LBB4_313
.LBB4_308:                              # %.backedge
                                        #   in Loop: Header=BB4_309 Depth=4
	movq	8(%rax), %rax
	cmpq	%r13, %rax
	jne	.LBB4_309
.LBB4_307:                              # %.thread1470
                                        #   in Loop: Header=BB4_302 Depth=3
	callq	SymName
	movq	%rax, %rbp
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	SymName
	movq	%rax, (%rsp)
	movl	$6, %edi
	movl	$38, %esi
	movl	$.L.str.47, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%rbp, %r9
	movq	48(%rsp), %rbp          # 8-byte Reload
	callq	Error
	.p2align	4, 0x90
.LBB4_313:                              # %.backedge1535
                                        #   in Loop: Header=BB4_302 Depth=3
	movq	8(%rbx), %rbx
	cmpq	%rbp, %rbx
	jne	.LBB4_302
.LBB4_314:                              # %.loopexit1534
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rsp), %rax
	cmpb	$104, 32(%rax)
	movq	24(%rsp), %r14          # 8-byte Reload
	jne	.LBB4_318
# BB#315:                               #   in Loop: Header=BB4_451 Depth=2
	movzwl	41(%rbp), %ecx
	testb	$8, %cl
	jne	.LBB4_317
# BB#316:                               #   in Loop: Header=BB4_451 Depth=2
	leaq	32(%r13), %rbx
	movq	%rbp, %rdi
	callq	SymName
	movq	%rax, (%rsp)
	movl	$6, %edi
	movl	$32, %esi
	movl	$.L.str.48, %edx
	movl	$2, %ecx
	movl	$.L.str.33, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	cmpl	$0, 68(%rsp)            # 4-byte Folded Reload
	jne	.LBB4_319
	jmp	.LBB4_321
.LBB4_317:                              #   in Loop: Header=BB4_451 Depth=2
	movq	%rbp, 80(%rax)
.LBB4_318:                              #   in Loop: Header=BB4_451 Depth=2
	cmpl	$0, 68(%rsp)            # 4-byte Folded Reload
	je	.LBB4_321
.LBB4_319:                              #   in Loop: Header=BB4_451 Depth=2
	movzwl	41(%rbp), %eax
	movzbl	43(%rbp), %esi
	shll	$16, %esi
	orl	%eax, %esi
	movl	%esi, %eax
	andl	$16448, %eax            # imm = 0x4040
	cmpl	$64, %eax
	jne	.LBB4_321
# BB#320:                               #   in Loop: Header=BB4_451 Depth=2
	cmpq	$0, 104(%rbp)
	je	.LBB4_333
.LBB4_321:                              #   in Loop: Header=BB4_451 Depth=2
	cmpq	$0, 104(%rbp)
	je	.LBB4_442
.LBB4_322:                              #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rsp), %rdx
	movb	32(%rdx), %al
	cmpb	$104, %al
	je	.LBB4_324
# BB#323:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$102, %al
	jne	.LBB4_327
.LBB4_324:                              #   in Loop: Header=BB4_451 Depth=2
	xorl	%edi, %edi
	cmpb	$104, %al
	sete	%dil
	addq	$32, %rdx
	movq	%rbp, %rsi
	callq	FilterCreate
	movq	%rax, %rbx
	testl	%r12d, %r12d
	je	.LBB4_370
# BB#325:                               #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rsp), %rax
	movzbl	41(%rax), %ecx
	movzbl	42(%rax), %edx
	addl	%ecx, %edx
	jne	.LBB4_328
# BB#326:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$102, 32(%rax)
	movl	$103, %ebp
	movl	$7, %eax
	cmovel	%eax, %ebp
	cmpl	$2, %r12d
	cmovel	%eax, %ebp
	movl	$101, %r14d
	jmp	.LBB4_329
.LBB4_327:                              #   in Loop: Header=BB4_451 Depth=2
	addq	$32, %r13
	movq	%rbp, %rdi
	callq	SymName
	movq	%rax, %rbp
	movl	$6, %edi
	movl	$33, %esi
	movl	$.L.str.51, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r13, %r8
	jmp	.LBB4_475
.LBB4_328:                              #   in Loop: Header=BB4_451 Depth=2
	movl	$7, %ebp
	movl	$100, %r14d
.LBB4_329:                              # %.lr.ph1604
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	ttop(%rip), %rax
	movq	tok_stack(,%rax,8), %rcx
	movzbl	40(%rcx), %ecx
	cmpl	%ebp, %ecx
	jb	.LBB4_331
# BB#330:                               #   in Loop: Header=BB4_329 Depth=3
	callq	Reduce
	testl	%eax, %eax
	jne	.LBB4_329
	jmp	.LBB4_369
.LBB4_331:                              # %.critedge148
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%r14d, %ecx
	movzbl	zz_lengths(%rcx), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB4_365
# BB#332:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB4_366
.LBB4_333:                              #   in Loop: Header=BB4_451 Depth=2
	testb	$8, %sil
	je	.LBB4_336
# BB#334:                               #   in Loop: Header=BB4_451 Depth=2
	movl	$146, %esi
	movq	%rbp, %rdi
	callq	ChildSym
	cmpb	$1, 124(%rax)
	ja	.LBB4_321
# BB#335:                               # %._crit_edge1677
                                        #   in Loop: Header=BB4_451 Depth=2
	movzwl	41(%rbp), %eax
	movzbl	43(%rbp), %esi
	shll	$16, %esi
	orl	%eax, %esi
.LBB4_336:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	ttop(%rip), %rax
	movl	%esi, %ecx
	shll	$28, %ecx
	sarl	$31, %ecx
	shlq	$40, %rcx
	sarq	$40, %rcx
	addq	%rax, %rcx
	leaq	tok_stack(,%rcx,8), %rdx
	shlq	$32, %rcx
	movabsq	$-4294967296, %rdi      # imm = 0xFFFFFFFF00000000
.LBB4_337:                              #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdx), %rax
	movzbl	32(%rax), %eax
	movl	%eax, %ebx
	addb	$-17, %bl
	addq	%rdi, %rcx
	addq	$-8, %rdx
	cmpb	$102, %al
	jb	.LBB4_337
# BB#338:                               #   in Loop: Header=BB4_337 Depth=3
	cmpb	$3, %bl
	jb	.LBB4_337
# BB#339:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$104, %al
	je	.LBB4_341
# BB#340:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$102, %al
	jne	.LBB4_321
.LBB4_341:                              #   in Loop: Header=BB4_451 Depth=2
	sarq	$29, %rcx
	movq	tok_stack(%rcx), %rax
	cmpb	$110, 32(%rax)
	jne	.LBB4_321
# BB#342:                               #   in Loop: Header=BB4_451 Depth=2
	testb	$8, %sil
	je	.LBB4_392
# BB#343:                               #   in Loop: Header=BB4_451 Depth=2
	movzbl	zz_lengths+2(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB4_376
# BB#344:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB4_377
.LBB4_345:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB4_346:                              #   in Loop: Header=BB4_451 Depth=2
	movb	$5, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	8(%rsp), %rcx
	movzwl	34(%rcx), %edx
	movw	%dx, 34(%rax)
	movl	36(%rcx), %edx
	movl	$1048575, %esi          # imm = 0xFFFFF
	andl	%esi, %edx
	movl	36(%rax), %esi
	movl	$-1048576, %edi         # imm = 0xFFF00000
	andl	%edi, %esi
	orl	%edx, %esi
	movl	%esi, 36(%rax)
	movl	36(%rcx), %ecx
	andl	%edi, %ecx
	orl	%edx, %ecx
	movl	%ecx, 36(%rax)
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_348
# BB#347:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, obj_stack+8(,%rcx,8)
	jmp	.LBB4_349
.LBB4_348:                              #   in Loop: Header=BB4_451 Depth=2
	movq	obj_stack(,%rcx,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_349:                              #   in Loop: Header=BB4_451 Depth=2
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB4_351
# BB#350:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB4_352
.LBB4_351:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB4_352:                              #   in Loop: Header=BB4_451 Depth=2
	movb	$1, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movq	8(%rsp), %rax
	movq	80(%rax), %rcx
	movzbl	43(%rcx), %ecx
	movzwl	44(%rbx), %edx
	andl	$2, %ecx
	shll	$7, %ecx
	andl	$65279, %edx            # imm = 0xFEFF
	orl	%ecx, %edx
	movw	%dx, 44(%rbx)
	movq	80(%rax), %rcx
	movzbl	43(%rcx), %ecx
	andl	$4, %ecx
	shll	$7, %ecx
	andl	$-513, %edx             # imm = 0xFDFF
	orl	%ecx, %edx
	movw	%dx, 44(%rbx)
	movb	41(%rax), %cl
	movb	%cl, 41(%rbx)
	movb	42(%rax), %cl
	movb	%cl, 42(%rbx)
	movb	$102, 40(%rbx)
	movzwl	34(%rax), %ecx
	movw	%cx, 34(%rbx)
	movl	36(%rax), %ecx
	movl	$1048575, %edx          # imm = 0xFFFFF
	andl	%edx, %ecx
	movl	36(%rbx), %edx
	movl	$-1048576, %esi         # imm = 0xFFF00000
	andl	%esi, %edx
	orl	%ecx, %edx
	movl	%edx, 36(%rbx)
	movl	36(%rax), %eax
	andl	%esi, %eax
	orl	%ecx, %eax
	movl	%eax, 36(%rbx)
	movslq	ttop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, ttop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_354
# BB#353:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, tok_stack+8(,%rax,8)
	jmp	.LBB4_355
.LBB4_354:                              #   in Loop: Header=BB4_451 Depth=2
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_355:                              #   in Loop: Header=BB4_451 Depth=2
	callq	LexGetToken
	movq	%rax, 8(%rsp)
	movzbl	41(%rax), %ecx
	movzbl	42(%rax), %eax
	addl	%ecx, %eax
	movl	$0, %r12d
	je	.LBB4_451
# BB#356:                               #   in Loop: Header=BB4_451 Depth=2
	addq	$32, %rbx
	movl	$11, %edi
	movl	$.L.str.27, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_358
# BB#357:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, obj_stack+8(,%rcx,8)
	movl	$1, %r12d
	jmp	.LBB4_451
.LBB4_358:                              #   in Loop: Header=BB4_451 Depth=2
	movq	obj_stack(,%rcx,8), %r8
.LBB4_359:                              # %.backedge1546
                                        #   in Loop: Header=BB4_451 Depth=2
	addq	$32, %r8
	movl	$1, %r12d
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB4_451
.LBB4_360:                              #   in Loop: Header=BB4_451 Depth=2
	addq	$32, %rbp
	xorl	%r12d, %r12d
	movl	$6, %edi
	movl	$36, %esi
	movl	$.L.str.51, %edx
	movl	$2, %ecx
	movl	$.L.str.54, %r9d
	xorl	%eax, %eax
	movq	%rbp, %r8
	jmp	.LBB4_95
.LBB4_361:                              #   in Loop: Header=BB4_451 Depth=2
	movq	obj_stack(,%rcx,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_362:                              #   in Loop: Header=BB4_451 Depth=2
	callq	PopScope
	cmpq	$0, 8(%rsp)
	jne	.LBB4_364
# BB#363:                               #   in Loop: Header=BB4_451 Depth=2
	callq	LexGetToken
	movq	%rax, 8(%rsp)
.LBB4_364:                              #   in Loop: Header=BB4_451 Depth=2
	callq	Reduce
	movl	%eax, %r12d
	jmp	.LBB4_451
.LBB4_365:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
	movl	ttop(%rip), %eax
.LBB4_366:                              #   in Loop: Header=BB4_451 Depth=2
	movb	%r14b, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movb	%bpl, 40(%rbx)
	movq	8(%rsp), %rcx
	movb	42(%rcx), %dl
	movb	%dl, 42(%rbx)
	movb	41(%rcx), %dl
	movb	%dl, 41(%rbx)
	movzwl	44(%rbx), %edx
	andl	$64767, %edx            # imm = 0xFCFF
	orl	$512, %edx              # imm = 0x200
	movw	%dx, 44(%rbx)
	movzwl	34(%rcx), %edx
	movw	%dx, 34(%rbx)
	movl	36(%rcx), %edx
	movl	$1048575, %esi          # imm = 0xFFFFF
	andl	%esi, %edx
	movl	36(%rbx), %esi
	movl	$-1048576, %edi         # imm = 0xFFF00000
	andl	%edi, %esi
	orl	%edx, %esi
	movl	%esi, 36(%rbx)
	movl	36(%rcx), %ecx
	andl	%edi, %ecx
	orl	%edx, %ecx
	movl	%ecx, 36(%rbx)
	leal	1(%rax), %ecx
	movl	%ecx, ttop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_368
# BB#367:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	%ecx, %rax
	movq	%rbx, tok_stack(,%rax,8)
	jmp	.LBB4_369
.LBB4_368:                              #   in Loop: Header=BB4_451 Depth=2
	cltq
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_369:                              # %.critedge1423
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB4_370:                              # %.critedge1423
                                        #   in Loop: Header=BB4_451 Depth=2
	movslq	ttop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, ttop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_372
# BB#371:                               #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rsp), %rcx
	movq	%rcx, tok_stack+8(,%rax,8)
	jmp	.LBB4_373
.LBB4_372:                              #   in Loop: Header=BB4_451 Depth=2
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_373:                              #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, zz_hold(%rip)
	movslq	otop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, otop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_375
# BB#374:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, obj_stack+8(,%rax,8)
	jmp	.LBB4_213
.LBB4_375:                              #   in Loop: Header=BB4_451 Depth=2
	movq	obj_stack(,%rax,8), %r8
	jmp	.LBB4_212
.LBB4_376:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB4_377:                              #   in Loop: Header=BB4_451 Depth=2
	movb	$2, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movq	InputSym(%rip), %rax
	movq	%rax, 80(%rbx)
	movq	8(%rsp), %rax
	movzwl	34(%rax), %ecx
	movw	%cx, 34(%rbx)
	movl	36(%rax), %ecx
	movl	$1048575, %edx          # imm = 0xFFFFF
	andl	%edx, %ecx
	movl	36(%rbx), %edx
	movl	$-1048576, %esi         # imm = 0xFFF00000
	andl	%esi, %edx
	orl	%ecx, %edx
	movl	%edx, 36(%rbx)
	movl	36(%rax), %eax
	andl	%esi, %eax
	orl	%ecx, %eax
	movl	%eax, 36(%rbx)
	testl	%r12d, %r12d
	je	.LBB4_388
# BB#378:                               #   in Loop: Header=BB4_451 Depth=2
	movzbl	41(%rbx), %eax
	movzbl	42(%rbx), %ecx
	cmpl	$2, %r12d
	movl	$103, %ebp
	movl	$7, %edx
	cmovel	%edx, %ebp
	xorl	%r14d, %r14d
	addl	%eax, %ecx
	sete	%r14b
	cmovnel	%edx, %ebp
	orl	$100, %r14d
.LBB4_379:                              # %.lr.ph1603
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	ttop(%rip), %rax
	movq	tok_stack(,%rax,8), %rcx
	movzbl	40(%rcx), %ecx
	cmpl	%ebp, %ecx
	jb	.LBB4_381
# BB#380:                               #   in Loop: Header=BB4_379 Depth=3
	callq	Reduce
	testl	%eax, %eax
	jne	.LBB4_379
	jmp	.LBB4_387
.LBB4_381:                              # %.critedge136
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%r14d, %ecx
	movzbl	zz_lengths(%rcx), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB4_383
# BB#382:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB4_384
.LBB4_383:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
	movl	ttop(%rip), %eax
.LBB4_384:                              #   in Loop: Header=BB4_451 Depth=2
	movb	%r14b, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movb	%bpl, 40(%rbx)
	movzwl	44(%rbx), %ecx
	andl	$64767, %ecx            # imm = 0xFCFF
	orl	$512, %ecx              # imm = 0x200
	movw	%cx, 44(%rbx)
	leal	1(%rax), %ecx
	movl	%ecx, ttop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_386
# BB#385:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	%ecx, %rax
	movq	%rbx, tok_stack(,%rax,8)
	jmp	.LBB4_387
.LBB4_386:                              #   in Loop: Header=BB4_451 Depth=2
	cltq
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_387:                              # %.critedge1421
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
.LBB4_388:                              # %.critedge1421
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, zz_hold(%rip)
	movslq	otop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, otop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_390
# BB#389:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, obj_stack+8(,%rax,8)
	jmp	.LBB4_391
.LBB4_390:                              #   in Loop: Header=BB4_451 Depth=2
	movq	obj_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_391:                              #   in Loop: Header=BB4_451 Depth=2
	callq	Reduce
.LBB4_392:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	otop(%rip), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, otop(%rip)
	movq	obj_stack(,%rax,8), %rdi
	callq	TransferBegin
	movq	%rax, %r13
	cmpb	$2, 32(%r13)
	jne	.LBB4_396
# BB#393:                               #   in Loop: Header=BB4_451 Depth=2
	movzwl	41(%rbp), %eax
	testb	$8, %al
	jne	.LBB4_398
# BB#394:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%r13, zz_hold(%rip)
	movslq	otop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, otop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_410
# BB#395:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%r13, obj_stack+8(,%rax,8)
	movl	$1, %r12d
	cmpq	$0, 104(%rbp)
	jne	.LBB4_322
	jmp	.LBB4_442
.LBB4_396:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	ttop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, ttop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_406
# BB#397:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%r13, tok_stack+8(,%rax,8)
	jmp	.LBB4_407
.LBB4_398:                              #   in Loop: Header=BB4_451 Depth=2
	movq	(%r13), %rax
	leaq	16(%rax), %rcx
	jmp	.LBB4_400
.LBB4_399:                              #   in Loop: Header=BB4_400 Depth=3
	addq	$16, %rcx
.LBB4_400:                              #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB4_399
# BB#401:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$10, %dl
	jne	.LBB4_403
# BB#402:                               #   in Loop: Header=BB4_451 Depth=2
	movq	80(%rcx), %rcx
	cmpb	$-110, 32(%rcx)
	je	.LBB4_404
.LBB4_403:                              # %.loopexit1531
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.49, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%r13), %rax
.LBB4_404:                              #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB4_411
# BB#405:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB4_412
.LBB4_406:                              #   in Loop: Header=BB4_451 Depth=2
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_407:                              #   in Loop: Header=BB4_451 Depth=2
	movzwl	41(%rbp), %eax
	xorl	%r12d, %r12d
	testb	$8, %al
	jne	.LBB4_321
# BB#408:                               #   in Loop: Header=BB4_451 Depth=2
	callq	Reduce
	movl	%eax, %r12d
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, ttop(%rip)
	jne	.LBB4_321
	jmp	.LBB4_409
.LBB4_410:                              #   in Loop: Header=BB4_451 Depth=2
	movq	obj_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$1, %r12d
	movl	$6, %edi
	movl	$1, %esi
	jmp	.LBB4_441
.LBB4_411:                              #   in Loop: Header=BB4_451 Depth=2
	xorl	%ecx, %ecx
.LBB4_412:                              #   in Loop: Header=BB4_451 Depth=2
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB4_414
# BB#413:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB4_414:                              #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB4_416
# BB#415:                               #   in Loop: Header=BB4_451 Depth=2
	callq	DisposeObject
.LBB4_416:                              #   in Loop: Header=BB4_451 Depth=2
	movzwl	41(%rbp), %eax
	testb	$4, %al
	je	.LBB4_438
# BB#417:                               #   in Loop: Header=BB4_451 Depth=2
	movq	8(%r13), %rbx
.LBB4_418:                              #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB4_418
# BB#419:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$10, %al
	jne	.LBB4_421
# BB#420:                               #   in Loop: Header=BB4_451 Depth=2
	movq	80(%rbx), %rax
	cmpb	$-112, 32(%rax)
	je	.LBB4_422
.LBB4_421:                              # %.loopexit1530
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.50, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_422:                              #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rbx), %rax
	addq	$16, %rax
.LBB4_423:                              #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rbx
	leaq	16(%rbx), %rax
	cmpb	$0, 32(%rbx)
	je	.LBB4_423
# BB#424:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, zz_hold(%rip)
	movslq	otop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, otop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_426
# BB#425:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, obj_stack+8(,%rax,8)
	jmp	.LBB4_427
.LBB4_426:                              #   in Loop: Header=BB4_451 Depth=2
	movq	obj_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_427:                              #   in Loop: Header=BB4_451 Depth=2
	movq	24(%rbx), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB4_429
# BB#428:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB4_429:                              #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB4_431
# BB#430:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB4_431:                              #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	8(%r13), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB4_433
# BB#432:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB4_434
.LBB4_433:                              #   in Loop: Header=BB4_451 Depth=2
	xorl	%ecx, %ecx
.LBB4_434:                              #   in Loop: Header=BB4_451 Depth=2
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB4_436
# BB#435:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB4_436:                              #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB4_438
# BB#437:                               #   in Loop: Header=BB4_451 Depth=2
	callq	DisposeObject
.LBB4_438:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	ttop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, ttop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_440
# BB#439:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%r13, tok_stack+8(,%rax,8)
	xorl	%r12d, %r12d
	cmpq	$0, 104(%rbp)
	jne	.LBB4_322
	jmp	.LBB4_442
.LBB4_440:                              #   in Loop: Header=BB4_451 Depth=2
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	xorl	%r12d, %r12d
	movl	$6, %edi
	movl	$2, %esi
.LBB4_441:                              #   in Loop: Header=BB4_451 Depth=2
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	cmpq	$0, 104(%rbp)
	jne	.LBB4_322
.LBB4_442:                              #   in Loop: Header=BB4_451 Depth=2
	movzwl	41(%rbp), %eax
	testb	$1, %ah
	je	.LBB4_451
# BB#443:                               #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rsp), %rbx
	movb	32(%rbx), %al
	cmpb	$104, %al
	je	.LBB4_445
# BB#444:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$102, %al
	jne	.LBB4_447
.LBB4_445:                              #   in Loop: Header=BB4_451 Depth=2
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rbp, %rdi
	callq	PushScope
	movl	$146, %esi
	movq	%rbp, %rdi
	callq	ChildSym
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	PushScope
	xorl	%edx, %edx
	movl	$1, %ecx
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	Parse
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_448
# BB#446:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, obj_stack+8(,%rcx,8)
	jmp	.LBB4_449
.LBB4_447:                              #   in Loop: Header=BB4_451 Depth=2
	addq	$32, %rbx
	movq	%rbp, %rdi
	callq	SymName
	movq	%rax, %rbp
	movl	$6, %edi
	movl	$34, %esi
	movl	$.L.str.52, %edx
	movl	$2, %ecx
	jmp	.LBB4_474
.LBB4_448:                              #   in Loop: Header=BB4_451 Depth=2
	movq	obj_stack(,%rcx,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB4_449:                              #   in Loop: Header=BB4_451 Depth=2
	callq	Reduce
	movl	%eax, %r12d
	callq	PopScope
	callq	PopScope
	cmpq	$0, 8(%rsp)
	jne	.LBB4_451
	.p2align	4, 0x90
.LBB4_450:                              #   in Loop: Header=BB4_451 Depth=2
	callq	LexGetToken
	movq	%rax, 8(%rsp)
.LBB4_451:                              # %.backedge1546
                                        #   Parent Loop BB4_66 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_222 Depth 3
                                        #       Child Loop BB4_483 Depth 3
                                        #         Child Loop BB4_486 Depth 4
                                        #       Child Loop BB4_477 Depth 3
                                        #         Child Loop BB4_480 Depth 4
                                        #       Child Loop BB4_82 Depth 3
                                        #       Child Loop BB4_586 Depth 3
                                        #         Child Loop BB4_588 Depth 4
                                        #       Child Loop BB4_70 Depth 3
                                        #       Child Loop BB4_555 Depth 3
                                        #       Child Loop BB4_653 Depth 3
                                        #       Child Loop BB4_75 Depth 3
                                        #       Child Loop BB4_614 Depth 3
                                        #       Child Loop BB4_648 Depth 3
                                        #       Child Loop BB4_643 Depth 3
                                        #       Child Loop BB4_638 Depth 3
                                        #       Child Loop BB4_515 Depth 3
                                        #         Child Loop BB4_518 Depth 4
                                        #       Child Loop BB4_512 Depth 3
                                        #       Child Loop BB4_504 Depth 3
                                        #         Child Loop BB4_507 Depth 4
                                        #       Child Loop BB4_501 Depth 3
                                        #       Child Loop BB4_126 Depth 3
                                        #       Child Loop BB4_465 Depth 3
                                        #         Child Loop BB4_469 Depth 4
                                        #       Child Loop BB4_534 Depth 3
                                        #       Child Loop BB4_625 Depth 3
                                        #       Child Loop BB4_459 Depth 3
                                        #         Child Loop BB4_462 Depth 4
                                        #       Child Loop BB4_456 Depth 3
                                        #       Child Loop BB4_609 Depth 3
                                        #       Child Loop BB4_100 Depth 3
                                        #       Child Loop BB4_262 Depth 3
                                        #         Child Loop BB4_274 Depth 4
                                        #           Child Loop BB4_275 Depth 5
                                        #         Child Loop BB4_288 Depth 4
                                        #         Child Loop BB4_290 Depth 4
                                        #           Child Loop BB4_292 Depth 5
                                        #       Child Loop BB4_302 Depth 3
                                        #         Child Loop BB4_303 Depth 4
                                        #         Child Loop BB4_309 Depth 4
                                        #           Child Loop BB4_310 Depth 5
                                        #       Child Loop BB4_337 Depth 3
                                        #       Child Loop BB4_379 Depth 3
                                        #       Child Loop BB4_400 Depth 3
                                        #       Child Loop BB4_418 Depth 3
                                        #       Child Loop BB4_423 Depth 3
                                        #       Child Loop BB4_329 Depth 3
                                        #       Child Loop BB4_658 Depth 3
	movq	8(%rsp), %r13
	movzbl	32(%r13), %edi
	movl	%edi, %eax
	addb	$-2, %al
	movzbl	%al, %eax
	cmpb	$115, %al
	ja	.LBB4_522
# BB#452:                               # %.backedge1546
                                        #   in Loop: Header=BB4_451 Depth=2
	leaq	32(%r13), %rbx
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_453:                              #   in Loop: Header=BB4_451 Depth=2
	movq	80(%r13), %rax
	movzwl	41(%rax), %eax
	testb	$4, %al
	jne	.LBB4_459
# BB#454:                               #   in Loop: Header=BB4_451 Depth=2
	testl	%r12d, %r12d
	je	.LBB4_492
# BB#455:                               #   in Loop: Header=BB4_451 Depth=2
	movzbl	41(%r13), %eax
	movzbl	42(%r13), %ecx
	cmpl	$2, %r12d
	movl	$103, %ebx
	movl	$7, %edx
	cmovel	%edx, %ebx
	xorl	%ebp, %ebp
	addl	%eax, %ecx
	sete	%bpl
	cmovnel	%edx, %ebx
	orl	$100, %ebp
	.p2align	4, 0x90
.LBB4_456:                              # %.lr.ph1623
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	ttop(%rip), %rcx
	movq	tok_stack(,%rcx,8), %rax
	movzbl	40(%rax), %eax
	cmpl	%ebx, %eax
	jb	.LBB4_471
# BB#457:                               #   in Loop: Header=BB4_456 Depth=3
	callq	Reduce
	testl	%eax, %eax
	jne	.LBB4_456
	jmp	.LBB4_492
.LBB4_458:                              #   in Loop: Header=BB4_459 Depth=3
	movq	obj_stack(,%rcx,8), %r8
	addq	$32, %r8
	movl	$1, %r12d
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	.p2align	4, 0x90
.LBB4_459:                              # %.preheader1503
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_462 Depth 4
	testl	%r12d, %r12d
	je	.LBB4_462
.LBB4_460:                              #   in Loop: Header=BB4_459 Depth=3
	movslq	ttop(%rip), %rax
	movq	tok_stack(,%rax,8), %rax
	movzbl	40(%rax), %eax
	movq	8(%rsp), %rcx
	movzbl	40(%rcx), %ecx
	incl	%ecx
	cmpl	%ecx, %eax
	jb	.LBB4_492
# BB#461:                               #   in Loop: Header=BB4_459 Depth=3
	callq	Reduce
	movl	%eax, %r12d
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, ttop(%rip)
	jne	.LBB4_459
	jmp	.LBB4_468
	.p2align	4, 0x90
.LBB4_462:                              #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        #       Parent Loop BB4_459 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rsp), %rdx
	addq	$32, %rdx
	movl	$11, %edi
	movl	$.L.str.27, %esi
	callq	MakeWord
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_458
# BB#463:                               #   in Loop: Header=BB4_462 Depth=4
	movq	%rax, obj_stack+8(,%rcx,8)
	movl	$1, %r12d
	testl	%r12d, %r12d
	jne	.LBB4_460
	jmp	.LBB4_462
.LBB4_464:                              #   in Loop: Header=BB4_465 Depth=3
	movq	obj_stack(,%rcx,8), %r8
	addq	$32, %r8
	movl	$1, %r12d
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	.p2align	4, 0x90
.LBB4_465:                              # %.preheader1500
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_469 Depth 4
	testl	%r12d, %r12d
	je	.LBB4_469
.LBB4_466:                              #   in Loop: Header=BB4_465 Depth=3
	movslq	ttop(%rip), %rax
	movq	tok_stack(,%rax,8), %r8
	movb	40(%r8), %dl
	movq	8(%rsp), %rcx
	cmpb	40(%rcx), %dl
	jb	.LBB4_520
# BB#467:                               #   in Loop: Header=BB4_465 Depth=3
	callq	Reduce
	movl	%eax, %r12d
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, ttop(%rip)
	jne	.LBB4_465
	jmp	.LBB4_468
	.p2align	4, 0x90
.LBB4_469:                              #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        #       Parent Loop BB4_465 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rsp), %rdx
	addq	$32, %rdx
	movl	$11, %edi
	movl	$.L.str.27, %esi
	callq	MakeWord
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_464
# BB#470:                               #   in Loop: Header=BB4_469 Depth=4
	movq	%rax, obj_stack+8(,%rcx,8)
	movl	$1, %r12d
	testl	%r12d, %r12d
	jne	.LBB4_466
	jmp	.LBB4_469
.LBB4_471:                              # %.critedge55
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%ebp, %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_488
# BB#472:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rdx
	movq	%rdx, zz_free(,%rdi,8)
	jmp	.LBB4_489
.LBB4_473:                              #   in Loop: Header=BB4_451 Depth=2
	movq	80(%r13), %rdi
	callq	SymName
	movq	%rax, %rbp
	movl	$6, %edi
	movl	$26, %esi
	movl	$.L.str.37, %edx
	movl	$1, %ecx
.LBB4_474:                              # %.backedge1546
                                        #   in Loop: Header=BB4_451 Depth=2
	xorl	%eax, %eax
	movq	%rbx, %r8
.LBB4_475:                              # %.backedge1546
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	%rbp, %r9
	callq	Error
	jmp	.LBB4_451
.LBB4_476:                              #   in Loop: Header=BB4_477 Depth=3
	movq	obj_stack(,%rcx,8), %r8
	addq	$32, %r8
	movl	$1, %r12d
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	.p2align	4, 0x90
.LBB4_477:                              # %.preheader1543
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_480 Depth 4
	testl	%r12d, %r12d
	je	.LBB4_480
.LBB4_478:                              #   in Loop: Header=BB4_477 Depth=3
	movslq	ttop(%rip), %rax
	movq	tok_stack(,%rax,8), %r8
	movzbl	40(%r8), %edx
	movq	8(%rsp), %rcx
	movzbl	40(%rcx), %esi
	incl	%esi
	cmpl	%esi, %edx
	jb	.LBB4_598
# BB#479:                               #   in Loop: Header=BB4_477 Depth=3
	callq	Reduce
	movl	%eax, %r12d
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, ttop(%rip)
	jne	.LBB4_477
	jmp	.LBB4_468
	.p2align	4, 0x90
.LBB4_480:                              #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        #       Parent Loop BB4_477 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rsp), %rdx
	addq	$32, %rdx
	movl	$11, %edi
	movl	$.L.str.27, %esi
	callq	MakeWord
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_476
# BB#481:                               #   in Loop: Header=BB4_480 Depth=4
	movq	%rax, obj_stack+8(,%rcx,8)
	movl	$1, %r12d
	testl	%r12d, %r12d
	jne	.LBB4_478
	jmp	.LBB4_480
.LBB4_482:                              #   in Loop: Header=BB4_483 Depth=3
	movq	obj_stack(,%rcx,8), %r8
	addq	$32, %r8
	movl	$1, %r12d
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	.p2align	4, 0x90
.LBB4_483:                              # %.preheader1518
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_486 Depth 4
	testl	%r12d, %r12d
	je	.LBB4_486
.LBB4_484:                              #   in Loop: Header=BB4_483 Depth=3
	movslq	ttop(%rip), %rax
	movq	tok_stack(,%rax,8), %r8
	movb	40(%r8), %dl
	movq	8(%rsp), %rcx
	cmpb	40(%rcx), %dl
	jb	.LBB4_600
# BB#485:                               #   in Loop: Header=BB4_483 Depth=3
	callq	Reduce
	movl	%eax, %r12d
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, ttop(%rip)
	jne	.LBB4_483
	jmp	.LBB4_468
	.p2align	4, 0x90
.LBB4_486:                              #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        #       Parent Loop BB4_483 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rsp), %rdx
	addq	$32, %rdx
	movl	$11, %edi
	movl	$.L.str.27, %esi
	callq	MakeWord
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_482
# BB#487:                               #   in Loop: Header=BB4_486 Depth=4
	movq	%rax, obj_stack+8(,%rcx,8)
	movl	$1, %r12d
	testl	%r12d, %r12d
	jne	.LBB4_484
	jmp	.LBB4_486
.LBB4_488:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	ttop(%rip), %ecx
.LBB4_489:                              #   in Loop: Header=BB4_451 Depth=2
	movb	%bpl, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movb	%bl, 40(%rax)
	movq	8(%rsp), %rdx
	movb	42(%rdx), %bl
	movb	%bl, 42(%rax)
	movb	41(%rdx), %bl
	movb	%bl, 41(%rax)
	movzwl	44(%rax), %esi
	andl	$64767, %esi            # imm = 0xFCFF
	orl	$512, %esi              # imm = 0x200
	movw	%si, 44(%rax)
	movzwl	34(%rdx), %esi
	movw	%si, 34(%rax)
	movl	36(%rdx), %esi
	movl	$1048575, %edi          # imm = 0xFFFFF
	andl	%edi, %esi
	movl	36(%rax), %edi
	movl	$-1048576, %ebp         # imm = 0xFFF00000
	andl	%ebp, %edi
	orl	%esi, %edi
	movl	%edi, 36(%rax)
	movl	36(%rdx), %edx
	andl	%ebp, %edx
	orl	%esi, %edx
	movl	%edx, 36(%rax)
	leal	1(%rcx), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_491
# BB#490:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	%edx, %rcx
	movq	%rax, tok_stack(,%rcx,8)
	jmp	.LBB4_492
.LBB4_491:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	%ecx, %rax
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	.p2align	4, 0x90
.LBB4_492:                              # %.critedge1381
                                        #   in Loop: Header=BB4_451 Depth=2
	movslq	ttop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, ttop(%rip)
	cmpl	$99, %ecx
	jg	.LBB4_494
# BB#493:                               #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rsp), %rcx
	movq	%rcx, tok_stack+8(,%rax,8)
	jmp	.LBB4_495
	.p2align	4, 0x90
.LBB4_494:                              #   in Loop: Header=BB4_451 Depth=2
	movq	tok_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	8(%rsp), %rcx
.LBB4_495:                              #   in Loop: Header=BB4_451 Depth=2
	movq	80(%rcx), %rax
	movzwl	41(%rax), %eax
	xorl	%r12d, %r12d
	testb	$8, %al
	jne	.LBB4_450
# BB#496:                               #   in Loop: Header=BB4_451 Depth=2
	callq	Reduce
	movl	%eax, %r12d
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, ttop(%rip)
	jne	.LBB4_450
	jmp	.LBB4_497
.LBB4_498:                              #   in Loop: Header=BB4_451 Depth=2
	movq	80(%r13), %rax
	movzwl	41(%rax), %eax
	testb	$4, %al
	jne	.LBB4_504
# BB#499:                               #   in Loop: Header=BB4_451 Depth=2
	testl	%r12d, %r12d
	je	.LBB4_114
# BB#500:                               #   in Loop: Header=BB4_451 Depth=2
	movzbl	41(%r13), %eax
	movzbl	42(%r13), %ecx
	cmpl	$2, %r12d
	movl	$103, %ebp
	movl	$7, %edx
	cmovel	%edx, %ebp
	xorl	%r14d, %r14d
	addl	%eax, %ecx
	sete	%r14b
	cmovnel	%edx, %ebp
	orl	$100, %r14d
	.p2align	4, 0x90
.LBB4_501:                              # %.lr.ph1621
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	ttop(%rip), %rcx
	movq	tok_stack(,%rcx,8), %rax
	movzbl	40(%rax), %eax
	cmpl	%ebp, %eax
	jb	.LBB4_629
# BB#502:                               #   in Loop: Header=BB4_501 Depth=3
	callq	Reduce
	testl	%eax, %eax
	jne	.LBB4_501
	jmp	.LBB4_114
.LBB4_503:                              #   in Loop: Header=BB4_504 Depth=3
	movq	obj_stack(,%rcx,8), %r8
	addq	$32, %r8
	movl	$1, %r12d
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	.p2align	4, 0x90
.LBB4_504:                              # %.preheader1508
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_507 Depth 4
	testl	%r12d, %r12d
	je	.LBB4_507
.LBB4_505:                              #   in Loop: Header=BB4_504 Depth=3
	movslq	ttop(%rip), %rax
	movq	tok_stack(,%rax,8), %rax
	movzbl	40(%rax), %eax
	movq	8(%rsp), %rcx
	movzbl	40(%rcx), %ecx
	incl	%ecx
	cmpl	%ecx, %eax
	jb	.LBB4_114
# BB#506:                               #   in Loop: Header=BB4_504 Depth=3
	callq	Reduce
	movl	%eax, %r12d
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, ttop(%rip)
	jne	.LBB4_504
	jmp	.LBB4_468
	.p2align	4, 0x90
.LBB4_507:                              #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        #       Parent Loop BB4_504 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rsp), %rdx
	addq	$32, %rdx
	movl	$11, %edi
	movl	$.L.str.27, %esi
	callq	MakeWord
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_503
# BB#508:                               #   in Loop: Header=BB4_507 Depth=4
	movq	%rax, obj_stack+8(,%rcx,8)
	movl	$1, %r12d
	testl	%r12d, %r12d
	jne	.LBB4_505
	jmp	.LBB4_507
.LBB4_509:                              #   in Loop: Header=BB4_451 Depth=2
	movq	80(%r13), %rax
	movzwl	41(%rax), %eax
	testb	$4, %al
	jne	.LBB4_515
# BB#510:                               #   in Loop: Header=BB4_451 Depth=2
	testl	%r12d, %r12d
	je	.LBB4_143
# BB#511:                               #   in Loop: Header=BB4_451 Depth=2
	movzbl	41(%r13), %eax
	movzbl	42(%r13), %ecx
	cmpl	$2, %r12d
	movl	$103, %ebx
	movl	$7, %edx
	cmovel	%edx, %ebx
	xorl	%ebp, %ebp
	addl	%eax, %ecx
	sete	%bpl
	cmovnel	%edx, %ebx
	orl	$100, %ebp
	.p2align	4, 0x90
.LBB4_512:                              # %.lr.ph1620
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	ttop(%rip), %rcx
	movq	tok_stack(,%rcx,8), %rax
	movzbl	40(%rax), %eax
	cmpl	%ebx, %eax
	jb	.LBB4_631
# BB#513:                               #   in Loop: Header=BB4_512 Depth=3
	callq	Reduce
	testl	%eax, %eax
	jne	.LBB4_512
	jmp	.LBB4_143
.LBB4_514:                              #   in Loop: Header=BB4_515 Depth=3
	movq	obj_stack(,%rcx,8), %r8
	addq	$32, %r8
	movl	$1, %r12d
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	.p2align	4, 0x90
.LBB4_515:                              # %.preheader1512
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_518 Depth 4
	testl	%r12d, %r12d
	je	.LBB4_518
.LBB4_516:                              #   in Loop: Header=BB4_515 Depth=3
	movslq	ttop(%rip), %rax
	movq	tok_stack(,%rax,8), %rax
	movb	40(%rax), %al
	movq	8(%rsp), %rcx
	cmpb	40(%rcx), %al
	jb	.LBB4_143
# BB#517:                               #   in Loop: Header=BB4_515 Depth=3
	callq	Reduce
	movl	%eax, %r12d
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, ttop(%rip)
	jne	.LBB4_515
	jmp	.LBB4_468
	.p2align	4, 0x90
.LBB4_518:                              #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        #       Parent Loop BB4_515 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rsp), %rdx
	addq	$32, %rdx
	movl	$11, %edi
	movl	$.L.str.27, %esi
	callq	MakeWord
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_514
# BB#519:                               #   in Loop: Header=BB4_518 Depth=4
	movq	%rax, obj_stack+8(,%rcx,8)
	movl	$1, %r12d
	testl	%r12d, %r12d
	jne	.LBB4_516
	jmp	.LBB4_518
.LBB4_520:                              #   in Loop: Header=BB4_451 Depth=2
	leal	1(%rax), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_579
# BB#521:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rcx, tok_stack+8(,%rax,8)
	cmpl	$0, 68(%rsp)            # 4-byte Folded Reload
	jne	.LBB4_580
	jmp	.LBB4_349
.LBB4_522:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rbx
	callq	Image
	movq	%rax, (%rsp)
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.55, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.56, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	jmp	.LBB4_451
.LBB4_523:                              #   in Loop: Header=BB4_451 Depth=2
	cmpb	$64, 64(%r13)
	jne	.LBB4_528
# BB#524:                               #   in Loop: Header=BB4_451 Depth=2
	cmpl	$1, %r12d
	jne	.LBB4_526
# BB#525:                               #   in Loop: Header=BB4_451 Depth=2
	movzbl	42(%r13), %eax
	movzbl	41(%r13), %ecx
	addl	%eax, %ecx
	je	.LBB4_530
.LBB4_526:                              #   in Loop: Header=BB4_451 Depth=2
	addq	$64, %r13
	movl	$6, %edi
	movl	$20, %esi
	movl	$.L.str.24, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%r13, %r9
	callq	Error
	movl	unknown_count(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, unknown_count(%rip)
	cmpl	$25, %eax
	jl	.LBB4_528
# BB#527:                               #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rsp), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$21, %esi
	movl	$.L.str.25, %edx
	movl	$1, %ecx
	movl	$.L.str.26, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_528:                              #   in Loop: Header=BB4_451 Depth=2
	testl	%r12d, %r12d
	je	.LBB4_108
# BB#529:                               # %..thread_crit_edge
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rsp), %r13
.LBB4_530:                              # %.thread
                                        #   in Loop: Header=BB4_451 Depth=2
	movzbl	41(%r13), %eax
	movzbl	42(%r13), %ecx
	addl	%eax, %ecx
	jne	.LBB4_624
# BB#531:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$102, 32(%r13)
	movl	$103, %ebx
	movl	$7, %eax
	cmovel	%eax, %ebx
	cmpl	$2, %r12d
	cmovel	%eax, %ebx
	movl	$101, %ebp
	jmp	.LBB4_625
.LBB4_532:                              #   in Loop: Header=BB4_451 Depth=2
	testl	%r12d, %r12d
	je	.LBB4_153
# BB#533:                               # %.preheader1499
                                        #   in Loop: Header=BB4_451 Depth=2
	movzbl	41(%r13), %eax
	movzbl	42(%r13), %ecx
	cmpl	$2, %r12d
	movl	$103, %ebx
	movl	$7, %edx
	cmovel	%edx, %ebx
	xorl	%ebp, %ebp
	addl	%eax, %ecx
	sete	%bpl
	cmovnel	%edx, %ebx
	orl	$100, %ebp
	.p2align	4, 0x90
.LBB4_534:                              # %.lr.ph1624
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	ttop(%rip), %rcx
	movq	tok_stack(,%rcx,8), %rax
	movzbl	40(%rax), %eax
	cmpl	%ebx, %eax
	jb	.LBB4_633
# BB#535:                               #   in Loop: Header=BB4_534 Depth=3
	callq	Reduce
	testl	%eax, %eax
	jne	.LBB4_534
	jmp	.LBB4_153
.LBB4_536:                              #   in Loop: Header=BB4_451 Depth=2
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	ParseEnvClosure
	movq	%rax, %rbx
	testl	%r12d, %r12d
	je	.LBB4_166
# BB#537:                               #   in Loop: Header=BB4_451 Depth=2
	movzbl	41(%rbx), %eax
	movzbl	42(%rbx), %ecx
	addl	%eax, %ecx
	jne	.LBB4_637
# BB#538:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$102, 32(%rbx)
	movl	$103, %ebp
	movl	$7, %eax
	cmovel	%eax, %ebp
	cmpl	$2, %r12d
	cmovel	%eax, %ebp
	movl	$101, %r14d
	jmp	.LBB4_638
.LBB4_539:                              #   in Loop: Header=BB4_451 Depth=2
	callq	LexNextTokenPos
	movl	$4294967291, %ecx       # imm = 0xFFFFFFFB
	addl	%ecx, %eax
	movl	%eax, 80(%rsp)
	movq	8(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	callq	LexGetToken
	movq	%rax, 8(%rsp)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	Parse
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	SetEnv
	movq	%rax, 32(%rsp)
	testl	%r12d, %r12d
	je	.LBB4_172
# BB#540:                               #   in Loop: Header=BB4_451 Depth=2
	movzbl	41(%rax), %ecx
	movzbl	42(%rax), %edx
	addl	%ecx, %edx
	jne	.LBB4_642
# BB#541:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$102, 32(%rax)
	movl	$103, %ebx
	movl	$7, %eax
	cmovel	%eax, %ebx
	cmpl	$2, %r12d
	cmovel	%eax, %ebx
	movl	$101, %ebp
	jmp	.LBB4_643
.LBB4_542:                              #   in Loop: Header=BB4_451 Depth=2
	callq	LexNextTokenPos
	movl	$4294967291, %ecx       # imm = 0xFFFFFFFB
	addl	%ecx, %eax
	movl	%eax, 80(%rsp)
	movq	8(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	callq	LexGetToken
	movq	%rax, 8(%rsp)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	8(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	Parse
	movq	%rax, 32(%rsp)
	callq	LexGetToken
	movq	%rax, 8(%rsp)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	Parse
	movq	32(%rsp), %rsi
	movq	%rax, %rdi
	callq	SetEnv
	movq	%rax, 32(%rsp)
	testl	%r12d, %r12d
	je	.LBB4_178
# BB#543:                               #   in Loop: Header=BB4_451 Depth=2
	movzbl	41(%rax), %ecx
	movzbl	42(%rax), %edx
	addl	%ecx, %edx
	jne	.LBB4_647
# BB#544:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$102, 32(%rax)
	movl	$103, %ebx
	movl	$7, %eax
	cmovel	%eax, %ebx
	cmpl	$2, %r12d
	cmovel	%eax, %ebx
	movl	$101, %ebp
	jmp	.LBB4_648
.LBB4_545:                              #   in Loop: Header=BB4_451 Depth=2
	movq	%r13, zz_hold(%rip)
	movzbl	zz_lengths+85(%rip), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r13)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	callq	LexGetToken
	movq	%rax, 8(%rsp)
	movzbl	zz_lengths+82(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB4_611
# BB#546:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB4_612
.LBB4_547:                              #   in Loop: Header=BB4_451 Depth=2
	movq	%r13, zz_hold(%rip)
	movzbl	zz_lengths+86(%rip), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r13)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	callq	LexGetToken
	movq	%rax, %rbp
	movq	%rbp, 8(%rsp)
	cmpb	$12, 32(%rbp)
	jne	.LBB4_550
# BB#548:                               #   in Loop: Header=BB4_451 Depth=2
	addq	$64, %rbp
	movl	$.L.str.38, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	leaq	80(%rsp), %rdx
	leaq	72(%rsp), %rcx
	callq	sscanf
	cmpl	$2, %eax
	je	.LBB4_551
# BB#549:                               # %._crit_edge1694
                                        #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rsp), %rbp
.LBB4_550:                              #   in Loop: Header=BB4_451 Depth=2
	addq	$32, %rbp
	movl	$6, %edi
	movl	$37, %esi
	movl	$.L.str.39, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
.LBB4_551:                              #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rsp), %rax
	movl	80(%rsp), %esi
	movzwl	34(%rax), %edi
	leaq	32(%rsp), %rdx
	callq	EnvReadRetrieve
	movq	8(%rsp), %rdx
	testl	%eax, %eax
	je	.LBB4_616
# BB#552:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rdx, zz_hold(%rip)
	movzbl	32(%rdx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rdx), %rsi
	cmpb	$2, %al
	cmovbq	%rsi, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rdx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	testl	%r12d, %r12d
	jne	.LBB4_617
	jmp	.LBB4_209
.LBB4_553:                              #   in Loop: Header=BB4_451 Depth=2
	movq	%r13, zz_hold(%rip)
	movzbl	zz_lengths+87(%rip), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r13)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	callq	LexGetToken
	movq	%rax, 8(%rsp)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	8(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	Parse
	movq	%rax, 32(%rsp)
	movl	$0, 56(%rsp)
	xorl	%edx, %edx
	movq	%rax, %rdi
	leaq	56(%rsp), %rsi
	callq	SetScope
	callq	LexGetToken
	movq	%rax, 8(%rsp)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	Parse
	movq	%rax, %r13
	movl	56(%rsp), %ebx
	testl	%ebx, %ebx
	jle	.LBB4_556
# BB#554:                               # %.lr.ph1612.preheader
                                        #   in Loop: Header=BB4_451 Depth=2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_555:                              # %.lr.ph1612
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	callq	PopScope
	incl	%ebp
	cmpl	%ebx, %ebp
	jl	.LBB4_555
.LBB4_556:                              # %._crit_edge
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%ebx, 44(%rsp)          # 4-byte Spill
	movq	32(%rsp), %rdi
	movq	%r13, %rsi
	callq	AttachEnv
	testl	%r12d, %r12d
	je	.LBB4_186
# BB#557:                               #   in Loop: Header=BB4_451 Depth=2
	movzbl	41(%r13), %eax
	movzbl	42(%r13), %ecx
	addl	%eax, %ecx
	jne	.LBB4_652
# BB#558:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$102, 32(%r13)
	movl	$103, %ebp
	movl	$7, %eax
	cmovel	%eax, %ebp
	cmpl	$2, %r12d
	cmovel	%eax, %ebp
	movl	$101, %r14d
	jmp	.LBB4_653
.LBB4_559:                              #   in Loop: Header=BB4_451 Depth=2
	callq	SuppressVisible
	movq	8(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	callq	LexGetToken
	movq	%rax, 8(%rsp)
	callq	UnSuppressVisible
	movq	8(%rsp), %r13
	cmpb	$2, 32(%r13)
	je	.LBB4_561
# BB#560:                               #   in Loop: Header=BB4_451 Depth=2
	addq	$32, %r13
	movl	$6, %edi
	movl	$29, %esi
	movl	$.L.str.40, %edx
	movl	$1, %ecx
	movl	$.L.str.43, %r9d
	xorl	%eax, %eax
	movq	%r13, %r8
	callq	Error
	movq	8(%rsp), %r13
.LBB4_561:                              #   in Loop: Header=BB4_451 Depth=2
	movq	80(%r13), %rdi
	movl	$1, %ebp
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	callq	PushScope
	callq	LexGetToken
	movq	%rax, 8(%rsp)
	cmpb	$107, 32(%rax)
	jne	.LBB4_563
# BB#562:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	zz_lengths+107(%rip), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	callq	LexGetToken
	movq	%rax, 8(%rsp)
	xorl	%ebp, %ebp
.LBB4_563:                              #   in Loop: Header=BB4_451 Depth=2
	callq	PopScope
	movq	8(%rsp), %rax
	movb	32(%rax), %al
	andb	$-2, %al
	cmpb	$6, %al
	jne	.LBB4_575
# BB#564:                               #   in Loop: Header=BB4_451 Depth=2
	testl	%r12d, %r12d
	je	.LBB4_195
# BB#565:                               #   in Loop: Header=BB4_451 Depth=2
	movzbl	41(%r13), %eax
	movzbl	42(%r13), %ecx
	addl	%eax, %ecx
	jne	.LBB4_657
# BB#566:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$102, 32(%r13)
	movl	$103, %ebx
	movl	$7, %eax
	cmovel	%eax, %ebx
	cmpl	$2, %r12d
	cmovel	%eax, %ebx
	movl	$101, %ebp
	jmp	.LBB4_658
.LBB4_567:                              #   in Loop: Header=BB4_451 Depth=2
	movq	%r13, zz_hold(%rip)
	movzbl	zz_lengths+90(%rip), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r13)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	callq	LexGetToken
	movq	%rax, %rbp
	movq	%rbp, 8(%rsp)
	cmpb	$2, 32(%rbp)
	je	.LBB4_569
# BB#568:                               #   in Loop: Header=BB4_451 Depth=2
	addq	$32, %rbp
	movl	$6, %edi
	movl	$27, %esi
	movl	$.L.str.40, %edx
	movl	$1, %ecx
	movl	$.L.str.41, %r9d
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
	movq	8(%rsp), %rbp
.LBB4_569:                              #   in Loop: Header=BB4_451 Depth=2
	movq	80(%rbp), %rbx
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB4_571
# BB#570:                               #   in Loop: Header=BB4_451 Depth=2
	addq	$32, %rbp
	movl	$6, %edi
	movl	$28, %esi
	movl	$.L.str.42, %edx
	movl	$1, %ecx
	movl	$.L.str.18, %r9d
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
	movq	112(%rbx), %rdi
.LBB4_571:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
	callq	CopyObject
	movq	%rax, %rbx
	movq	(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB4_595
.LBB4_586:                              # %.lr.ph1609
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_588 Depth 4
	leaq	16(%rax), %rcx
	jmp	.LBB4_588
	.p2align	4, 0x90
.LBB4_587:                              #   in Loop: Header=BB4_588 Depth=4
	addq	$16, %rcx
.LBB4_588:                              #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        #       Parent Loop BB4_586 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB4_587
# BB#589:                               #   in Loop: Header=BB4_586 Depth=3
	cmpb	$82, %dl
	je	.LBB4_590
# BB#585:                               # %.loopexit1490
                                        #   in Loop: Header=BB4_586 Depth=3
	movq	(%rax), %rax
	cmpq	%rbx, %rax
	jne	.LBB4_586
	jmp	.LBB4_595
.LBB4_572:                              #   in Loop: Header=BB4_451 Depth=2
	cmpq	$0, 80(%r13)
	je	.LBB4_619
# BB#573:                               #   in Loop: Header=BB4_451 Depth=2
	xorl	%ebx, %ebx
	testl	%r12d, %r12d
	jne	.LBB4_219
	jmp	.LBB4_222
.LBB4_574:                              #   in Loop: Header=BB4_451 Depth=2
	movl	$6, %edi
	movl	$22, %esi
	movl	$.L.str.31, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	jmp	.LBB4_451
.LBB4_575:                              #   in Loop: Header=BB4_451 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	movzwl	41(%rax), %eax
	testb	$4, %al
	jne	.LBB4_608
# BB#576:                               #   in Loop: Header=BB4_451 Depth=2
	testl	%r12d, %r12d
	je	.LBB4_235
# BB#577:                               #   in Loop: Header=BB4_451 Depth=2
	movzbl	41(%r13), %eax
	movzbl	42(%r13), %ecx
	addl	%eax, %ecx
	jne	.LBB4_99
# BB#578:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$102, 32(%r13)
	movl	$103, %ebx
	movl	$7, %eax
	cmovel	%eax, %ebx
	cmpl	$2, %r12d
	cmovel	%eax, %ebx
	movl	$101, %r14d
	jmp	.LBB4_100
.LBB4_579:                              #   in Loop: Header=BB4_451 Depth=2
	addq	$32, %r8
	movl	$6, %edi
	movl	$2, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	cmpl	$0, 68(%rsp)            # 4-byte Folded Reload
	je	.LBB4_349
.LBB4_580:                              #   in Loop: Header=BB4_451 Depth=2
	movq	8(%rsp), %rax
	cmpb	$19, 32(%rax)
	jne	.LBB4_349
# BB#581:                               #   in Loop: Header=BB4_451 Depth=2
	movq	80(%rax), %rax
	movzbl	43(%rax), %eax
	shll	$16, %eax
	testl	$262144, %eax           # imm = 0x40000
	jne	.LBB4_349
# BB#582:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	ttop(%rip), %rax
	movq	tok_stack-16(,%rax,8), %rax
	cmpb	$110, 32(%rax)
	jne	.LBB4_349
# BB#583:                               #   in Loop: Header=BB4_451 Depth=2
	movslq	otop(%rip), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, otop(%rip)
	movq	obj_stack(,%rax,8), %rdi
	callq	TransferComponent
	movzbl	zz_lengths+5(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_345
# BB#584:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB4_346
.LBB4_590:                              #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB4_592
# BB#591:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB4_592:                              #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB4_594
# BB#593:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB4_594:                              #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
.LBB4_595:                              # %.loopexit1527
                                        #   in Loop: Header=BB4_451 Depth=2
	testl	%r12d, %r12d
	je	.LBB4_202
# BB#596:                               #   in Loop: Header=BB4_451 Depth=2
	movzbl	41(%rbx), %eax
	movzbl	42(%rbx), %ecx
	addl	%eax, %ecx
	jne	.LBB4_69
# BB#597:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$102, 32(%rbx)
	movl	$103, %ebp
	movl	$7, %eax
	cmovel	%eax, %ebp
	cmpl	$2, %r12d
	cmovel	%eax, %ebp
	movl	$101, %r14d
	jmp	.LBB4_70
.LBB4_598:                              #   in Loop: Header=BB4_451 Depth=2
	leal	1(%rax), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_79
# BB#599:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rcx, tok_stack+8(,%rax,8)
	jmp	.LBB4_80
.LBB4_600:                              #   in Loop: Header=BB4_451 Depth=2
	leal	1(%rax), %edx
	movl	%edx, ttop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_85
# BB#601:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rcx, tok_stack+8(,%rax,8)
	jmp	.LBB4_86
.LBB4_608:                              # %.preheader1538
                                        #   in Loop: Header=BB4_451 Depth=2
	leaq	32(%r13), %rbx
	testl	%r12d, %r12d
	jne	.LBB4_604
	jmp	.LBB4_609
.LBB4_602:                              #   in Loop: Header=BB4_451 Depth=2
	movq	obj_stack(,%rcx,8), %r8
	addq	$32, %r8
	movl	$1, %r12d
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	.p2align	4, 0x90
.LBB4_603:                              # %.backedge1540
                                        #   in Loop: Header=BB4_451 Depth=2
	testl	%r12d, %r12d
	je	.LBB4_609
.LBB4_604:                              #   in Loop: Header=BB4_451 Depth=2
	movslq	ttop(%rip), %rax
	movq	tok_stack(,%rax,8), %rax
	movzbl	40(%rax), %eax
	movzbl	40(%r13), %ecx
	movq	48(%rsp), %rdx          # 8-byte Reload
	movzwl	41(%rdx), %edx
	shrl	$4, %edx
	andl	$1, %edx
	addl	%ecx, %edx
	cmpl	%edx, %eax
	jb	.LBB4_235
# BB#605:                               #   in Loop: Header=BB4_451 Depth=2
	callq	Reduce
	movl	%eax, %r12d
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, ttop(%rip)
	jne	.LBB4_603
	jmp	.LBB4_606
	.p2align	4, 0x90
.LBB4_609:                              #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$11, %edi
	movl	$.L.str.27, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB4_602
# BB#610:                               #   in Loop: Header=BB4_609 Depth=3
	movq	%rax, obj_stack+8(,%rcx,8)
	movl	$1, %r12d
	testl	%r12d, %r12d
	jne	.LBB4_604
	jmp	.LBB4_609
.LBB4_611:                              #   in Loop: Header=BB4_451 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB4_612:                              #   in Loop: Header=BB4_451 Depth=2
	movb	$82, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	testl	%r12d, %r12d
	je	.LBB4_159
# BB#613:                               #   in Loop: Header=BB4_451 Depth=2
	movzbl	41(%rbx), %eax
	movzbl	42(%rbx), %ecx
	cmpl	$2, %r12d
	movl	$103, %ebp
	movl	$7, %edx
	cmovel	%edx, %ebp
	xorl	%r14d, %r14d
	addl	%eax, %ecx
	sete	%r14b
	cmovnel	%edx, %ebp
	orl	$100, %r14d
	.p2align	4, 0x90
.LBB4_614:                              # %.lr.ph1615
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	ttop(%rip), %rcx
	movq	tok_stack(,%rcx,8), %rax
	movzbl	40(%rax), %eax
	cmpl	%ebp, %eax
	jb	.LBB4_635
# BB#615:                               #   in Loop: Header=BB4_614 Depth=3
	callq	Reduce
	testl	%eax, %eax
	jne	.LBB4_614
	jmp	.LBB4_159
.LBB4_616:                              #   in Loop: Header=BB4_451 Depth=2
	movl	80(%rsp), %esi
	movl	72(%rsp), %ecx
	movzwl	34(%rdx), %edi
	movl	$3, %edx
	movl	$1, %r8d
	callq	LexPush
	movq	8(%rsp), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	callq	LexGetToken
	movq	%rax, 8(%rsp)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	Parse
	movq	%rax, 32(%rsp)
	callq	LexPop
	testl	%r12d, %r12d
	je	.LBB4_209
.LBB4_617:                              #   in Loop: Header=BB4_451 Depth=2
	movq	32(%rsp), %rax
	movzbl	41(%rax), %ecx
	movzbl	42(%rax), %edx
	addl	%ecx, %edx
	jne	.LBB4_74
# BB#618:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$102, 32(%rax)
	movl	$103, %ebx
	movl	$7, %eax
	cmovel	%eax, %ebx
	cmpl	$2, %r12d
	cmovel	%eax, %ebx
	movl	$101, %ebp
	jmp	.LBB4_75
.LBB4_619:                              #   in Loop: Header=BB4_451 Depth=2
	callq	LexGetToken
	movq	%rax, %rbx
	leaq	32(%rbx), %r8
	movb	32(%rbx), %al
	cmpb	$52, %al
	jg	.LBB4_96
# BB#620:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$2, %al
	je	.LBB4_214
# BB#621:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$11, %al
	jne	.LBB4_221
# BB#622:                               #   in Loop: Header=BB4_451 Depth=2
	cmpb	$64, 64(%rbx)
	jne	.LBB4_221
# BB#623:                               #   in Loop: Header=BB4_451 Depth=2
	leaq	64(%rbx), %r9
	movq	$.L.str.35, (%rsp)
	movl	$6, %edi
	movl	$24, %esi
	movl	$.L.str.34, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	8(%rsp), %rax
	movq	$0, 80(%rax)
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	movq	%rbx, %rdx
	addq	$33, %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	jmp	.LBB4_216
.LBB4_624:                              #   in Loop: Header=BB4_451 Depth=2
	movl	$7, %ebx
	movl	$100, %ebp
	.p2align	4, 0x90
.LBB4_625:                              # %.lr.ph1625
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	ttop(%rip), %rcx
	movq	tok_stack(,%rcx,8), %rax
	movzbl	40(%rax), %eax
	cmpl	%ebx, %eax
	jb	.LBB4_627
# BB#626:                               #   in Loop: Header=BB4_625 Depth=3
	callq	Reduce
	testl	%eax, %eax
	jne	.LBB4_625
	jmp	.LBB4_108
.LBB4_627:                              # %.critedge43
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%ebp, %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_104
# BB#628:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rdx
	movq	%rdx, zz_free(,%rdi,8)
	jmp	.LBB4_105
.LBB4_629:                              # %.critedge58
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%r14d, %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_110
# BB#630:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rdx
	movq	%rdx, zz_free(,%rdi,8)
	jmp	.LBB4_111
.LBB4_631:                              # %.critedge67
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%ebp, %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_139
# BB#632:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rdx
	movq	%rdx, zz_free(,%rdi,8)
	jmp	.LBB4_140
.LBB4_633:                              # %.critedge46
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%ebp, %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_149
# BB#634:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rdx
	movq	%rdx, zz_free(,%rdi,8)
	jmp	.LBB4_150
.LBB4_635:                              # %.critedge96
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%r14d, %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_155
# BB#636:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rdx
	movq	%rdx, zz_free(,%rdi,8)
	jmp	.LBB4_156
.LBB4_637:                              #   in Loop: Header=BB4_451 Depth=2
	movl	$7, %ebp
	movl	$100, %r14d
	.p2align	4, 0x90
.LBB4_638:                              # %.lr.ph1618
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	ttop(%rip), %rcx
	movq	tok_stack(,%rcx,8), %rax
	movzbl	40(%rax), %eax
	cmpl	%ebp, %eax
	jb	.LBB4_640
# BB#639:                               #   in Loop: Header=BB4_638 Depth=3
	callq	Reduce
	testl	%eax, %eax
	jne	.LBB4_638
	jmp	.LBB4_166
.LBB4_640:                              # %.critedge81
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%r14d, %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_162
# BB#641:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rdx
	movq	%rdx, zz_free(,%rdi,8)
	jmp	.LBB4_163
.LBB4_642:                              #   in Loop: Header=BB4_451 Depth=2
	movl	$7, %ebx
	movl	$100, %ebp
	.p2align	4, 0x90
.LBB4_643:                              # %.lr.ph1617
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	ttop(%rip), %rcx
	movq	tok_stack(,%rcx,8), %rax
	movzbl	40(%rax), %eax
	cmpl	%ebx, %eax
	jb	.LBB4_645
# BB#644:                               #   in Loop: Header=BB4_643 Depth=3
	callq	Reduce
	testl	%eax, %eax
	jne	.LBB4_643
	jmp	.LBB4_172
.LBB4_645:                              # %.critedge86
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%ebp, %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_168
# BB#646:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rdx
	movq	%rdx, zz_free(,%rdi,8)
	jmp	.LBB4_169
.LBB4_647:                              #   in Loop: Header=BB4_451 Depth=2
	movl	$7, %ebx
	movl	$100, %ebp
	.p2align	4, 0x90
.LBB4_648:                              # %.lr.ph1616
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	ttop(%rip), %rcx
	movq	tok_stack(,%rcx,8), %rax
	movzbl	40(%rax), %eax
	cmpl	%ebx, %eax
	jb	.LBB4_650
# BB#649:                               #   in Loop: Header=BB4_648 Depth=3
	callq	Reduce
	testl	%eax, %eax
	jne	.LBB4_648
	jmp	.LBB4_178
.LBB4_650:                              # %.critedge91
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%ebp, %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_174
# BB#651:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rdx
	movq	%rdx, zz_free(,%rdi,8)
	jmp	.LBB4_175
.LBB4_652:                              #   in Loop: Header=BB4_451 Depth=2
	movl	$7, %ebp
	movl	$100, %r14d
	.p2align	4, 0x90
.LBB4_653:                              # %.lr.ph1613
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	ttop(%rip), %rcx
	movq	tok_stack(,%rcx,8), %rax
	movzbl	40(%rax), %eax
	cmpl	%ebp, %eax
	jb	.LBB4_655
# BB#654:                               #   in Loop: Header=BB4_653 Depth=3
	callq	Reduce
	testl	%eax, %eax
	jne	.LBB4_653
	jmp	.LBB4_186
.LBB4_655:                              # %.critedge110
                                        #   in Loop: Header=BB4_451 Depth=2
	movl	%r14d, %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB4_182
# BB#656:                               #   in Loop: Header=BB4_451 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rdx
	movq	%rdx, zz_free(,%rdi,8)
	jmp	.LBB4_183
.LBB4_657:                              #   in Loop: Header=BB4_451 Depth=2
	movl	$7, %ebx
	movl	$100, %ebp
	.p2align	4, 0x90
.LBB4_658:                              # %.lr.ph1605
                                        #   Parent Loop BB4_66 Depth=1
                                        #     Parent Loop BB4_451 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	ttop(%rip), %rcx
	movq	tok_stack(,%rcx,8), %rax
	movzbl	40(%rax), %eax
	cmpl	%ebx, %eax
	jb	.LBB4_67
# BB#659:                               #   in Loop: Header=BB4_658 Depth=3
	callq	Reduce
	testl	%eax, %eax
	jne	.LBB4_658
	jmp	.LBB4_195
.LBB4_660:                              #   in Loop: Header=BB4_66 Depth=1
	cmpq	$0, 80(%r13)
	jne	.LBB4_662
# BB#661:                               #   in Loop: Header=BB4_66 Depth=1
	movq	$.L.str.17, (%rsp)
	movl	$6, %edi
	movl	$23, %esi
	movl	$.L.str.32, %edx
	movl	$2, %ecx
	movl	$.L.str.33, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	movq	8(%rsp), %r13
	movb	$102, 32(%r13)
.LBB4_662:                              #   in Loop: Header=BB4_66 Depth=1
	testl	%r12d, %r12d
	je	.LBB4_62
# BB#663:                               #   in Loop: Header=BB4_66 Depth=1
	movzbl	41(%r13), %eax
	movzbl	42(%r13), %ecx
	addl	%eax, %ecx
	jne	.LBB4_665
# BB#664:                               #   in Loop: Header=BB4_66 Depth=1
	cmpb	$102, 32(%r13)
	movl	$103, %ebx
	movl	$7, %eax
	cmovel	%eax, %ebx
	cmpl	$2, %r12d
	cmovel	%eax, %ebx
	movl	$101, %ebp
	jmp	.LBB4_666
.LBB4_665:                              #   in Loop: Header=BB4_66 Depth=1
	movl	$7, %ebx
	movl	$100, %ebp
	.p2align	4, 0x90
.LBB4_666:                              # %.lr.ph1619
                                        #   Parent Loop BB4_66 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	ttop(%rip), %rcx
	movq	tok_stack(,%rcx,8), %rax
	movzbl	40(%rax), %eax
	cmpl	%ebx, %eax
	jb	.LBB4_56
# BB#667:                               #   in Loop: Header=BB4_666 Depth=2
	callq	Reduce
	testl	%eax, %eax
	jne	.LBB4_666
	jmp	.LBB4_62
.LBB4_468:
	movq	8(%rsp), %rax
	movq	%rax, (%r15)
.LBB4_607:
	movslq	otop(%rip), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, otop(%rip)
	movq	obj_stack(,%rax,8), %rax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_497:
	movq	$0, (%r15)
	jmp	.LBB4_607
.LBB4_606:
	movq	%r13, (%r15)
	jmp	.LBB4_607
.LBB4_148:
	movq	$0, (%r15)
	jmp	.LBB4_607
.LBB4_409:
	movq	$0, (%r15)
	jmp	.LBB4_607
.Lfunc_end4:
	.size	Parse, .Lfunc_end4-Parse
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_561
	.quad	.LBB4_522
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_522
	.quad	.LBB4_522
	.quad	.LBB4_522
	.quad	.LBB4_523
	.quad	.LBB4_532
	.quad	.LBB4_522
	.quad	.LBB4_522
	.quad	.LBB4_522
	.quad	.LBB4_522
	.quad	.LBB4_465
	.quad	.LBB4_465
	.quad	.LBB4_465
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_498
	.quad	.LBB4_498
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_522
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_509
	.quad	.LBB4_509
	.quad	.LBB4_522
	.quad	.LBB4_536
	.quad	.LBB4_539
	.quad	.LBB4_542
	.quad	.LBB4_545
	.quad	.LBB4_547
	.quad	.LBB4_553
	.quad	.LBB4_522
	.quad	.LBB4_559
	.quad	.LBB4_567
	.quad	.LBB4_522
	.quad	.LBB4_477
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_453
	.quad	.LBB4_522
	.quad	.LBB4_522
	.quad	.LBB4_662
	.quad	.LBB4_483
	.quad	.LBB4_660
	.quad	.LBB4_572
	.quad	.LBB4_473
	.quad	.LBB4_473
	.quad	.LBB4_522
	.quad	.LBB4_522
	.quad	.LBB4_522
	.quad	.LBB4_574
	.quad	.LBB4_522
	.quad	.LBB4_522
	.quad	.LBB4_473
	.quad	.LBB4_473
	.quad	.LBB4_473
	.quad	.LBB4_473

	.text
	.p2align	4, 0x90
	.type	Reduce,@function
Reduce:                                 # @Reduce
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 48
.Lcfi53:
	.cfi_offset %rbx, -48
.Lcfi54:
	.cfi_offset %r12, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movslq	ttop(%rip), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, ttop(%rip)
	movq	tok_stack(,%rax,8), %rbx
	movzbl	32(%rbx), %edi
	movl	%edi, %edx
	decb	%dl
	cmpb	$109, %dl
	ja	.LBB5_217
# BB#1:
	leaq	32(%rbx), %r14
	movzbl	%dl, %edx
	jmpq	*.LJTI5_0(,%rdx,8)
.LBB5_14:
	movq	80(%rbx), %rax
	movzwl	41(%rax), %eax
	testb	$8, %al
	je	.LBB5_23
# BB#15:
	movslq	otop(%rip), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, otop(%rip)
	movq	obj_stack(,%rax,8), %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_16
# BB#17:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_18
.LBB5_16:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_18:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB5_20
# BB#19:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_20:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB5_23
# BB#21:
	testq	%rax, %rax
	je	.LBB5_23
# BB#22:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_23:
	movq	80(%rbx), %rax
	movzwl	41(%rax), %eax
	testb	$4, %al
	je	.LBB5_34
# BB#24:
	movslq	otop(%rip), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, otop(%rip)
	movq	obj_stack(,%rax,8), %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_25
# BB#26:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_27
.LBB5_25:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_27:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%rbx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB5_30
# BB#28:
	testq	%rcx, %rcx
	je	.LBB5_30
# BB#29:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_30:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB5_34
# BB#31:
	testq	%rax, %rax
	je	.LBB5_34
# BB#32:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
.LBB5_33:
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_34:
	movq	%rbx, zz_hold(%rip)
	movslq	otop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, otop(%rip)
	cmpl	$99, %ecx
	jg	.LBB5_36
# BB#35:
	movq	%rbx, obj_stack+8(,%rax,8)
	movl	$1, %ebp
	jmp	.LBB5_218
.LBB5_156:
	movslq	otop(%rip), %rax
	movq	obj_stack(,%rax,8), %r14
	movq	obj_stack-8(,%rax,8), %r15
	leal	-3(%rax), %ecx
	movl	%ecx, otop(%rip)
	movq	obj_stack-16(,%rax,8), %rbp
	cmpb	%dil, 32(%rbp)
	jne	.LBB5_158
# BB#157:
	movq	%rbx, zz_hold(%rip)
	movl	%edi, %eax
	addb	$-11, %al
	leaq	33(%rbx), %rcx
	cmpb	$2, %al
	leaq	zz_lengths(%rdi), %rax
	cmovbq	%rcx, %rax
	movzbl	(%rax), %eax
	movl	%eax, zz_size(%rip)
	leaq	zz_free(,%rax,8), %rcx
	movq	zz_free(,%rax,8), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	jmp	.LBB5_166
.LBB5_59:
	movslq	otop(%rip), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, otop(%rip)
	movq	obj_stack(,%rax,8), %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_60
# BB#61:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_62
.LBB5_183:
	movslq	otop(%rip), %rax
	movq	obj_stack(,%rax,8), %r12
	leal	-2(%rax), %ecx
	movl	%ecx, otop(%rip)
	movq	obj_stack-8(,%rax,8), %rbp
	cmpb	$17, 32(%rbp)
	jne	.LBB5_185
# BB#184:
	movq	%rbp, %r15
	jmp	.LBB5_198
.LBB5_2:
	movslq	otop(%rip), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, otop(%rip)
	movq	obj_stack(,%rax,8), %rdi
	callq	TransferEnd
	movzbl	zz_lengths+5(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#4:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_5
.LBB5_158:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_159
# BB#160:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_161
.LBB5_144:
	movslq	otop(%rip), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, otop(%rip)
	movq	obj_stack(,%rax,8), %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_145
# BB#146:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_147
.LBB5_79:
	movq	80(%rbx), %rax
	movzwl	41(%rax), %eax
	testb	$8, %al
	je	.LBB5_100
# BB#80:
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r14
	testq	%r14, %r14
	je	.LBB5_81
# BB#82:
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB5_83
.LBB5_217:
	movq	no_fpos(%rip), %r14
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	Image
	movq	%rax, %rbx
	subq	$8, %rsp
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	movl	$1, %ebp
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.55, %edx
	movl	$0, %ecx
	movl	$.L.str.68, %r9d
	xorl	%eax, %eax
	movq	%r14, %r8
	pushq	%rbx
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi60:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB5_218
.LBB5_38:
	movq	80(%rbx), %rax
	movzwl	41(%rax), %eax
	testb	$8, %al
	je	.LBB5_47
# BB#39:
	movslq	otop(%rip), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, otop(%rip)
	movq	obj_stack(,%rax,8), %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_40
# BB#41:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_42
.LBB5_122:
	subq	$8, %rsp
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	movl	$2, %ebp
	movl	$6, %edi
	movl	$4, %esi
	movl	$.L.str.59, %edx
	movl	$2, %ecx
	movl	$.L.str.17, %r9d
	xorl	%eax, %eax
	movq	%r14, %r8
	pushq	$.L.str.60
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi63:
	.cfi_adjust_cfa_offset -16
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	jmp	.LBB5_218
.LBB5_124:
	movq	tok_stack-8(,%rax,8), %rdi
	movb	32(%rdi), %cl
	cmpb	$104, %cl
	je	.LBB5_127
# BB#125:
	cmpb	$102, %cl
	jne	.LBB5_130
# BB#126:
	addl	$-2, %eax
	movl	%eax, ttop(%rip)
	movq	%rdi, zz_hold(%rip)
	movzbl	32(%rdi), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rdi), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rdi)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	jmp	.LBB5_131
.LBB5_123:
	movq	no_fpos(%rip), %r8
	subq	$8, %rsp
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	movl	$1, %ebp
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.55, %edx
	movl	$0, %ecx
	movl	$.L.str.61, %r9d
	xorl	%eax, %eax
	pushq	$.L.str.33
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi66:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB5_218
.LBB5_132:
	movslq	%ecx, %rax
	movq	tok_stack(,%rax,8), %rdx
	cmpb	$104, 32(%rdx)
	jne	.LBB5_133
# BB#134:
	movq	80(%rbx), %rax
	movq	80(%rdx), %rdi
	cmpq	%rdi, %rax
	je	.LBB5_143
# BB#135:
	cmpq	StartSym(%rip), %rax
	je	.LBB5_136
# BB#137:
	testq	%rax, %rax
	je	.LBB5_138
# BB#142:
	movq	%rax, %rdi
	callq	SymName
	movq	%rax, %r15
	movslq	ttop(%rip), %rax
	movq	tok_stack(,%rax,8), %rax
	movq	80(%rax), %rdi
	callq	SymName
	movq	%rax, %r12
	movslq	ttop(%rip), %rax
	movq	tok_stack(,%rax,8), %rdi
	addq	$32, %rdi
	callq	EchoFilePos
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi67:
	.cfi_adjust_cfa_offset 8
	movl	$6, %edi
	movl	$9, %esi
	movl	$.L.str.67, %edx
	movl	$2, %ecx
	movl	$.L.str.35, %r9d
	movl	$0, %eax
	movq	%r14, %r8
	pushq	%rbp
.Lcfi68:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.33
.Lcfi69:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi70:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.35
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi72:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$48, %rsp
.Lcfi73:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB5_143
.LBB5_9:
	movzbl	zz_lengths+5(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_10
# BB#11:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_12
.LBB5_185:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r15
	testq	%r15, %r15
	je	.LBB5_186
# BB#187:
	movq	%r15, zz_hold(%rip)
	movq	(%r15), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB5_188
.LBB5_60:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_62:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB5_65
# BB#63:
	testq	%rax, %rax
	je	.LBB5_65
# BB#64:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_65:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB5_68
# BB#66:
	testq	%rax, %rax
	je	.LBB5_68
# BB#67:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_68:
	movslq	otop(%rip), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, otop(%rip)
	movq	obj_stack(,%rax,8), %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_69
# BB#70:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_71
.LBB5_3:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_5:
	movb	$5, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB5_7
# BB#6:
	movq	%rax, obj_stack+8(,%rcx,8)
	jmp	.LBB5_8
.LBB5_69:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_71:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%rbx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB5_74
# BB#72:
	testq	%rcx, %rcx
	je	.LBB5_74
# BB#73:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_74:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB5_77
# BB#75:
	testq	%rax, %rax
	je	.LBB5_77
# BB#76:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_77:
	cmpb	$2, 32(%rbp)
	je	.LBB5_34
# BB#78:
	addq	$32, %rbp
	movzbl	(%r14), %edi
	callq	Image
	movq	%rax, %r9
	movl	$6, %edi
	movl	$3, %esi
	movl	$.L.str.58, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
	jmp	.LBB5_34
.LBB5_159:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_161:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB5_163
# BB#162:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_163:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB5_167
# BB#164:
	testq	%rax, %rax
	je	.LBB5_167
# BB#165:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	addq	$24, %rcx
	movq	%rbx, %rbp
.LBB5_166:                              # %.sink.split
	movq	%rax, (%rcx)
	movq	%rbp, %rbx
.LBB5_167:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_168
# BB#169:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_170
.LBB5_168:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_170:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB5_173
# BB#171:
	testq	%rax, %rax
	je	.LBB5_173
# BB#172:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_173:
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB5_176
# BB#174:
	testq	%rax, %rax
	je	.LBB5_176
# BB#175:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_176:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_177
# BB#178:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_179
.LBB5_177:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_179:
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB5_182
# BB#180:
	testq	%rax, %rax
	je	.LBB5_182
# BB#181:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_182:
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB5_34
.LBB5_120:
	testq	%rax, %rax
	je	.LBB5_34
# BB#121:
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	jmp	.LBB5_33
.LBB5_133:
	movq	no_fpos(%rip), %r8
	subq	$8, %rsp
.Lcfi74:
	.cfi_adjust_cfa_offset 8
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.55, %edx
	movl	$0, %ecx
	movl	$.L.str.61, %r9d
	xorl	%eax, %eax
	pushq	$.L.str.35
.Lcfi75:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi76:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB5_131
.LBB5_145:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_147:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB5_150
# BB#148:
	testq	%rax, %rax
	je	.LBB5_150
# BB#149:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_150:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB5_153
# BB#151:
	testq	%rax, %rax
	je	.LBB5_153
# BB#152:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_153:
	movq	%rbx, zz_hold(%rip)
	movslq	otop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, otop(%rip)
	cmpl	$99, %ecx
	jg	.LBB5_155
# BB#154:
	movq	%rbx, obj_stack+8(,%rax,8)
	xorl	%ebp, %ebp
	jmp	.LBB5_218
.LBB5_155:
	movq	obj_stack(,%rax,8), %r8
	addq	$32, %r8
	xorl	%ebp, %ebp
	jmp	.LBB5_37
.LBB5_10:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_12:
	movb	$5, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, zz_hold(%rip)
	movslq	otop(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, otop(%rip)
	cmpl	$99, %edx
	jg	.LBB5_7
# BB#13:
	movq	%rax, obj_stack+8(,%rcx,8)
	jmp	.LBB5_8
.LBB5_7:
	movq	obj_stack(,%rcx,8), %r8
	addq	$32, %r8
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB5_8:
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movl	$1, %ebp
	jmp	.LBB5_218
.LBB5_186:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r15
	movq	%r15, zz_hold(%rip)
.LBB5_188:
	movb	$17, 32(%r15)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	movq	%r15, 8(%r15)
	movq	%r15, (%r15)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_189
# BB#190:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_191
.LBB5_189:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_191:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB5_194
# BB#192:
	testq	%rax, %rax
	je	.LBB5_194
# BB#193:
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_194:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB5_197
# BB#195:
	testq	%rax, %rax
	je	.LBB5_197
# BB#196:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_197:
	movzwl	34(%rbp), %eax
	movw	%ax, 34(%r15)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%rbp), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%r15), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%r15)
	andl	36(%rbp), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%r15)
.LBB5_198:
	movb	$1, (%r14)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_199
# BB#200:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_201
.LBB5_199:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_201:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB5_204
# BB#202:
	testq	%rax, %rax
	je	.LBB5_204
# BB#203:
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_204:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB5_206
# BB#205:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_206:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_207
# BB#208:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_209
.LBB5_207:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_209:
	testq	%r15, %r15
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	je	.LBB5_212
# BB#210:
	testq	%rax, %rax
	je	.LBB5_212
# BB#211:
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_212:
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB5_215
# BB#213:
	testq	%rax, %rax
	je	.LBB5_215
# BB#214:
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_215:
	movq	%r15, zz_hold(%rip)
	movslq	otop(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, otop(%rip)
	cmpl	$99, %ecx
	jg	.LBB5_36
# BB#216:
	movq	%r15, obj_stack+8(,%rax,8)
	movl	$1, %ebp
	jmp	.LBB5_218
.LBB5_36:
	movq	obj_stack(,%rax,8), %r8
	addq	$32, %r8
	movl	$1, %ebp
.LBB5_37:
	movl	$6, %edi
	movl	$1, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB5_218:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_127:
	cmpw	$0, 34(%rdi)
	je	.LBB5_129
# BB#128:
	addq	$32, %rdi
	callq	EchoFilePos
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi77:
	.cfi_adjust_cfa_offset 8
	movl	$6, %edi
	movl	$5, %esi
	movl	$.L.str.62, %edx
	movl	$2, %ecx
	movl	$.L.str.60, %r9d
	movl	$0, %eax
	movq	%r14, %r8
	pushq	$.L.str.33
.Lcfi78:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi79:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.17
.Lcfi80:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$32, %rsp
.Lcfi81:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB5_131
.LBB5_130:
	movq	no_fpos(%rip), %r8
	subq	$8, %rsp
.Lcfi82:
	.cfi_adjust_cfa_offset 8
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.55, %edx
	movl	$0, %ecx
	movl	$.L.str.61, %r9d
	xorl	%eax, %eax
	pushq	$.L.str.60
.Lcfi83:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi84:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB5_131
.LBB5_81:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
.LBB5_83:
	movb	$10, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movslq	otop(%rip), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, otop(%rip)
	movq	obj_stack(,%rax,8), %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_84
# BB#85:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_86
.LBB5_40:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_42:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB5_44
# BB#43:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_44:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB5_47
# BB#45:
	testq	%rax, %rax
	je	.LBB5_47
# BB#46:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_47:
	movq	80(%rbx), %rax
	movzwl	41(%rax), %eax
	testb	$4, %al
	je	.LBB5_34
# BB#48:
	movslq	otop(%rip), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, otop(%rip)
	movq	obj_stack(,%rax,8), %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_49
# BB#50:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_51
.LBB5_84:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_86:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB5_89
# BB#87:
	testq	%rax, %rax
	je	.LBB5_89
# BB#88:
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_89:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB5_92
# BB#90:
	testq	%rax, %rax
	je	.LBB5_92
# BB#91:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_92:
	movzwl	34(%rbp), %eax
	movw	%ax, 34(%r14)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%rbp), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%r14), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%r14)
	andl	36(%rbp), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%r14)
	movq	80(%rbx), %rdi
	movl	$146, %esi
	callq	ChildSym
	movq	%rax, 80(%r14)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_93
# BB#94:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_95
.LBB5_49:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_51:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%rbx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB5_54
# BB#52:
	testq	%rcx, %rcx
	je	.LBB5_54
# BB#53:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_54:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB5_57
# BB#55:
	testq	%rax, %rax
	je	.LBB5_57
# BB#56:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_57:
	cmpb	$56, 32(%rbp)
	jne	.LBB5_34
# BB#58:
	movq	%rbx, %rdi
	callq	OptimizeCase
	movq	%rax, %rbx
	jmp	.LBB5_34
.LBB5_93:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_95:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB5_97
# BB#96:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_97:
	testq	%r14, %r14
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	je	.LBB5_100
# BB#98:
	testq	%rax, %rax
	je	.LBB5_100
# BB#99:
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_100:
	movq	80(%rbx), %rax
	movzwl	41(%rax), %eax
	testb	$4, %al
	je	.LBB5_34
# BB#101:
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r14
	testq	%r14, %r14
	je	.LBB5_102
# BB#103:
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB5_104
.LBB5_102:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
.LBB5_104:
	movb	$10, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movslq	otop(%rip), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, otop(%rip)
	movq	obj_stack(,%rax,8), %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_105
# BB#106:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_107
.LBB5_105:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_107:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB5_110
# BB#108:
	testq	%rax, %rax
	je	.LBB5_110
# BB#109:
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_110:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB5_113
# BB#111:
	testq	%rax, %rax
	je	.LBB5_113
# BB#112:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_113:
	movzwl	34(%rbp), %eax
	movw	%ax, 34(%r14)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%rbp), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%r14), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%r14)
	andl	36(%rbp), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%r14)
	movq	80(%rbx), %rdi
	movl	$144, %esi
	callq	ChildSym
	movq	%rax, 80(%r14)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_114
# BB#115:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_116
.LBB5_114:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_116:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%rbx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB5_119
# BB#117:
	testq	%rcx, %rcx
	je	.LBB5_119
# BB#118:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_119:
	testq	%r14, %r14
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	jne	.LBB5_120
	jmp	.LBB5_34
.LBB5_129:
	movl	$6, %edi
	movl	$6, %esi
	movl	$.L.str.63, %edx
	movl	$1, %ecx
	movl	$.L.str.60, %r9d
	xorl	%eax, %eax
	movq	%r14, %r8
	callq	Error
	jmp	.LBB5_131
.LBB5_136:
	callq	SymName
	movq	%rax, %r15
	movslq	ttop(%rip), %rax
	movq	tok_stack(,%rax,8), %rdi
	addq	$32, %rdi
	callq	EchoFilePos
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi85:
	.cfi_adjust_cfa_offset 8
	movl	$6, %edi
	movl	$7, %esi
	movl	$.L.str.64, %edx
	movl	$2, %ecx
	movl	$.L.str.35, %r9d
	movl	$0, %eax
	movq	%r14, %r8
	pushq	%rbp
.Lcfi86:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.33
.Lcfi87:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi88:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$32, %rsp
.Lcfi89:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB5_143
.LBB5_138:
	testq	%rdi, %rdi
	je	.LBB5_139
# BB#140:
	callq	SymName
	movq	%rax, %r15
	movl	ttop(%rip), %ecx
	jmp	.LBB5_141
.LBB5_139:
	movl	$.L.str.66, %r15d
.LBB5_141:
	movslq	%ecx, %rax
	movq	tok_stack(,%rax,8), %rdi
	addq	$32, %rdi
	callq	EchoFilePos
	movq	%rax, %rbp
	movl	$6, %edi
	movl	$8, %esi
	movl	$.L.str.65, %edx
	movl	$2, %ecx
	movl	$.L.str.35, %r9d
	movl	$0, %eax
	movq	%r14, %r8
	pushq	%rbp
.Lcfi90:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.33
.Lcfi91:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi92:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.35
.Lcfi93:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$32, %rsp
.Lcfi94:
	.cfi_adjust_cfa_offset -32
.LBB5_143:
	movslq	ttop(%rip), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, ttop(%rip)
	movq	tok_stack(,%rax,8), %rax
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
.LBB5_131:
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movl	$2, %ebp
	jmp	.LBB5_218
.Lfunc_end5:
	.size	Reduce, .Lfunc_end5-Reduce
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI5_0:
	.quad	.LBB5_144
	.quad	.LBB5_79
	.quad	.LBB5_217
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_59
	.quad	.LBB5_59
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_156
	.quad	.LBB5_156
	.quad	.LBB5_156
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_38
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_217
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_14
	.quad	.LBB5_183
	.quad	.LBB5_183
	.quad	.LBB5_122
	.quad	.LBB5_124
	.quad	.LBB5_123
	.quad	.LBB5_132
	.quad	.LBB5_217
	.quad	.LBB5_217
	.quad	.LBB5_9
	.quad	.LBB5_2
	.quad	.LBB5_2

	.text
	.p2align	4, 0x90
	.type	HuntCommandOptions,@function
HuntCommandOptions:                     # @HuntCommandOptions
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi98:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi99:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi101:
	.cfi_def_cfa_offset 64
.Lcfi102:
	.cfi_offset %rbx, -56
.Lcfi103:
	.cfi_offset %r12, -48
.Lcfi104:
	.cfi_offset %r13, -40
.Lcfi105:
	.cfi_offset %r14, -32
.Lcfi106:
	.cfi_offset %r15, -24
.Lcfi107:
	.cfi_offset %rbp, -16
	movq	%rdi, (%rsp)            # 8-byte Spill
	movq	CommandOptions(%rip), %rax
	movq	8(%rax), %r12
	cmpq	%rax, %r12
	je	.LBB6_40
# BB#1:                                 # %.lr.ph
	movq	(%rsp), %rax            # 8-byte Reload
	movq	80(%rax), %r13
	jmp	.LBB6_2
.LBB6_23:                               #   in Loop: Header=BB6_2 Depth=1
	xorl	%ecx, %ecx
.LBB6_25:                               #   in Loop: Header=BB6_2 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_27
# BB#26:                                #   in Loop: Header=BB6_2 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB6_27:                               #   in Loop: Header=BB6_2 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB6_29
# BB#28:                                #   in Loop: Header=BB6_2 Depth=1
	callq	DisposeObject
.LBB6_29:                               #   in Loop: Header=BB6_2 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB6_30
# BB#31:                                #   in Loop: Header=BB6_2 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB6_32
.LBB6_30:                               #   in Loop: Header=BB6_2 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB6_32:                               #   in Loop: Header=BB6_2 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB6_36
# BB#33:                                #   in Loop: Header=BB6_2 Depth=1
	testq	%rax, %rax
	je	.LBB6_36
# BB#34:                                #   in Loop: Header=BB6_2 Depth=1
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	jmp	.LBB6_35
	.p2align	4, 0x90
.LBB6_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_3 Depth 2
                                        #     Child Loop BB6_5 Depth 2
                                        #     Child Loop BB6_8 Depth 2
                                        #       Child Loop BB6_9 Depth 3
                                        #     Child Loop BB6_15 Depth 2
                                        #       Child Loop BB6_16 Depth 3
	leaq	16(%r12), %rax
	.p2align	4, 0x90
.LBB6_3:                                #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %r15
	leaq	16(%r15), %rax
	cmpb	$0, 32(%r15)
	je	.LBB6_3
# BB#4:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	8(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB6_5:                                #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %r14
	leaq	16(%r14), %rax
	cmpb	$0, 32(%r14)
	je	.LBB6_5
# BB#6:                                 # %.preheader110
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	8(%r13), %rbp
	cmpq	%r13, %rbp
	je	.LBB6_39
# BB#7:                                 # %.preheader109.lr.ph
                                        #   in Loop: Header=BB6_2 Depth=1
	addq	$64, %r15
	.p2align	4, 0x90
.LBB6_8:                                # %.preheader109
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_9 Depth 3
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB6_9:                                #   Parent Loop BB6_2 Depth=1
                                        #     Parent Loop BB6_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB6_9
# BB#10:                                #   in Loop: Header=BB6_8 Depth=2
	cmpb	$-111, %al
	jne	.LBB6_12
# BB#11:                                #   in Loop: Header=BB6_8 Depth=2
	movq	%rbx, %rdi
	callq	SymName
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB6_13
.LBB6_12:                               # %.backedge
                                        #   in Loop: Header=BB6_8 Depth=2
	movq	8(%rbp), %rbp
	cmpq	%r13, %rbp
	jne	.LBB6_8
	jmp	.LBB6_39
	.p2align	4, 0x90
.LBB6_13:                               # %.thread.preheader
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	8(%rdx), %rax
	cmpq	%rdx, %rax
	jne	.LBB6_15
	jmp	.LBB6_20
	.p2align	4, 0x90
.LBB6_19:                               # %.thread.backedge
                                        #   in Loop: Header=BB6_15 Depth=2
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	je	.LBB6_20
.LBB6_15:                               # %.preheader
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_16 Depth 3
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB6_16:                               #   Parent Loop BB6_2 Depth=1
                                        #     Parent Loop BB6_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB6_16
# BB#17:                                #   in Loop: Header=BB6_15 Depth=2
	cmpb	$10, %cl
	jne	.LBB6_19
# BB#18:                                #   in Loop: Header=BB6_15 Depth=2
	cmpq	%rbx, 80(%rbp)
	jne	.LBB6_19
# BB#22:                                #   in Loop: Header=BB6_2 Depth=1
	movq	8(%rbp), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_23
# BB#24:                                #   in Loop: Header=BB6_2 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB6_25
.LBB6_20:                               # %.thread._crit_edge
                                        #   in Loop: Header=BB6_2 Depth=1
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r15
	testq	%r15, %r15
	je	.LBB6_21
# BB#41:                                #   in Loop: Header=BB6_2 Depth=1
	movq	%r15, zz_hold(%rip)
	movq	(%r15), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB6_42
.LBB6_21:                               #   in Loop: Header=BB6_2 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r15
	movq	%r15, zz_hold(%rip)
.LBB6_42:                               #   in Loop: Header=BB6_2 Depth=1
	movb	$10, 32(%r15)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	movq	%r15, 8(%r15)
	movq	%r15, (%r15)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB6_43
# BB#44:                                #   in Loop: Header=BB6_2 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB6_45
.LBB6_43:                               #   in Loop: Header=BB6_2 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB6_45:                               #   in Loop: Header=BB6_2 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	%rdx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB6_47
# BB#46:                                #   in Loop: Header=BB6_2 Depth=1
	movq	(%rdx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB6_47:                               #   in Loop: Header=BB6_2 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB6_50
# BB#48:                                #   in Loop: Header=BB6_2 Depth=1
	testq	%rax, %rax
	je	.LBB6_50
# BB#49:                                #   in Loop: Header=BB6_2 Depth=1
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB6_50:                               #   in Loop: Header=BB6_2 Depth=1
	movq	%rbx, 80(%r15)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB6_51
# BB#52:                                #   in Loop: Header=BB6_2 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB6_53
.LBB6_51:                               #   in Loop: Header=BB6_2 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB6_53:                               #   in Loop: Header=BB6_2 Depth=1
	testq	%r15, %r15
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	je	.LBB6_36
# BB#54:                                #   in Loop: Header=BB6_2 Depth=1
	testq	%rax, %rax
	je	.LBB6_36
# BB#55:                                #   in Loop: Header=BB6_2 Depth=1
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
.LBB6_35:                               #   in Loop: Header=BB6_2 Depth=1
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB6_36:                               #   in Loop: Header=BB6_2 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB6_39
# BB#37:                                #   in Loop: Header=BB6_2 Depth=1
	testq	%rax, %rax
	je	.LBB6_39
# BB#38:                                #   in Loop: Header=BB6_2 Depth=1
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	.p2align	4, 0x90
.LBB6_39:                               # %.backedge112
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	8(%r12), %rax
	movq	8(%rax), %r12
	cmpq	CommandOptions(%rip), %r12
	jne	.LBB6_2
.LBB6_40:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	HuntCommandOptions, .Lfunc_end6-HuntCommandOptions
	.cfi_endproc

	.p2align	4, 0x90
	.type	ParseEnvClosure,@function
ParseEnvClosure:                        # @ParseEnvClosure
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi111:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi112:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi114:
	.cfi_def_cfa_offset 80
.Lcfi115:
	.cfi_offset %rbx, -56
.Lcfi116:
	.cfi_offset %r12, -48
.Lcfi117:
	.cfi_offset %r13, -40
.Lcfi118:
	.cfi_offset %r14, -32
.Lcfi119:
	.cfi_offset %r15, -24
.Lcfi120:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	movq	%r14, 16(%rsp)
	cmpb	$82, 32(%r14)
	je	.LBB7_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.69, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_2:                                # %.outer.preheader
	leaq	16(%rsp), %r12
	jmp	.LBB7_3
	.p2align	4, 0x90
.LBB7_13:                               # %.outer
                                        #   in Loop: Header=BB7_3 Depth=1
	callq	SetEnv
	movq	%rax, %r14
.LBB7_3:                                # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_4 Depth 2
                                        #     Child Loop BB7_11 Depth 2
	callq	LexGetToken
	movq	%rax, %rbx
	movq	%rbx, 16(%rsp)
	movq	%rbx, %rbp
	addq	$32, %rbp
	jmp	.LBB7_4
	.p2align	4, 0x90
.LBB7_15:                               #   in Loop: Header=BB7_4 Depth=2
	movl	$6, %edi
	movl	$12, %esi
	movl	$.L.str.39, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
.LBB7_4:                                #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbp), %eax
	cmpb	$82, %al
	je	.LBB7_14
# BB#5:                                 #   in Loop: Header=BB7_4 Depth=2
	cmpb	$88, %al
	je	.LBB7_16
# BB#6:                                 #   in Loop: Header=BB7_4 Depth=2
	cmpb	$102, %al
	jne	.LBB7_15
# BB#7:                                 #   in Loop: Header=BB7_3 Depth=1
	movl	$0, 12(%rsp)
	xorl	%edx, %edx
	movq	%r14, %rdi
	leaq	12(%rsp), %rsi
	callq	SetScope
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	Parse
	movq	%rax, %rbx
	cmpb	$2, 32(%rbx)
	je	.LBB7_9
# BB#8:                                 #   in Loop: Header=BB7_3 Depth=1
	leaq	32(%rbx), %r8
	movl	$6, %edi
	movl	$11, %esi
	movl	$.L.str.70, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB7_9:                                # %.preheader41
                                        #   in Loop: Header=BB7_3 Depth=1
	movl	12(%rsp), %ebp
	testl	%ebp, %ebp
	jle	.LBB7_12
# BB#10:                                # %.lr.ph54.preheader
                                        #   in Loop: Header=BB7_3 Depth=1
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB7_11:                               # %.lr.ph54
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	PopScope
	incl	%r15d
	cmpl	%ebp, %r15d
	jl	.LBB7_11
.LBB7_12:                               # %._crit_edge55
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	AttachEnv
	xorl	%esi, %esi
	movq	%rbx, %rdi
	jmp	.LBB7_13
	.p2align	4, 0x90
.LBB7_14:                               #   in Loop: Header=BB7_3 Depth=1
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	ParseEnvClosure
	movq	%rax, %rdi
	movq	%r14, %rsi
	jmp	.LBB7_13
.LBB7_16:
	movq	%rbx, zz_hold(%rip)
	movzbl	zz_lengths+88(%rip), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	8(%r14), %rcx
	cmpq	%r14, %rcx
	je	.LBB7_18
# BB#17:
	cmpq	(%r14), %rcx
	je	.LBB7_19
.LBB7_18:
	leaq	32(%r14), %r8
	movl	$6, %edi
	movl	$13, %esi
	movl	$.L.str.39, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	8(%r14), %rcx
.LBB7_19:                               # %._crit_edge66
	movq	%rcx, %rdx
	addq	$16, %rdx
	.p2align	4, 0x90
.LBB7_20:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rbx
	movzbl	32(%rbx), %eax
	leaq	16(%rbx), %rdx
	testb	%al, %al
	je	.LBB7_20
# BB#21:
	movq	%r14, xx_hold(%rip)
	movq	24(%r14), %rdx
	cmpq	%r14, %rdx
	je	.LBB7_28
	.p2align	4, 0x90
.LBB7_22:                               # %.lr.ph51
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, xx_link(%rip)
	movq	%rdx, zz_hold(%rip)
	movq	24(%rdx), %rcx
	cmpq	%rdx, %rcx
	je	.LBB7_24
# BB#23:                                #   in Loop: Header=BB7_22 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rdx), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rdx), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 24(%rdx)
	movq	%rdx, 16(%rdx)
.LBB7_24:                               #   in Loop: Header=BB7_22 Depth=1
	movq	%rdx, zz_hold(%rip)
	movq	8(%rdx), %rcx
	cmpq	%rdx, %rcx
	je	.LBB7_26
# BB#25:                                #   in Loop: Header=BB7_22 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	zz_res(%rip), %rcx
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	xx_link(%rip), %rdx
.LBB7_26:                               #   in Loop: Header=BB7_22 Depth=1
	movq	%rdx, zz_hold(%rip)
	movzbl	32(%rdx), %ecx
	leaq	zz_lengths(%rcx), %rsi
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rdx), %rdi
	cmpb	$2, %cl
	cmovbq	%rdi, %rsi
	movzbl	(%rsi), %ecx
	movq	zz_free(,%rcx,8), %rsi
	movq	%rsi, (%rdx)
	movq	zz_hold(%rip), %rdx
	movq	%rdx, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %r14
	movq	24(%r14), %rdx
	cmpq	%r14, %rdx
	jne	.LBB7_22
# BB#27:                                # %..preheader_crit_edge
	movl	%ecx, zz_size(%rip)
	movq	8(%r14), %rcx
.LBB7_28:                               # %.preheader
	cmpq	%r14, %rcx
	je	.LBB7_29
	.p2align	4, 0x90
.LBB7_30:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB7_32
# BB#31:                                #   in Loop: Header=BB7_30 Depth=1
	movq	%rdx, zz_res(%rip)
	movq	16(%rcx), %rsi
	movq	%rsi, 16(%rdx)
	movq	16(%rcx), %rsi
	movq	%rdx, 24(%rsi)
	movq	%rcx, 24(%rcx)
	movq	%rcx, 16(%rcx)
.LBB7_32:                               #   in Loop: Header=BB7_30 Depth=1
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB7_34
# BB#33:                                #   in Loop: Header=BB7_30 Depth=1
	movq	%rdx, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
	movq	zz_res(%rip), %rcx
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	xx_link(%rip), %rcx
.LBB7_34:                               #   in Loop: Header=BB7_30 Depth=1
	movq	%rcx, zz_hold(%rip)
	movzbl	32(%rcx), %edx
	leaq	zz_lengths(%rdx), %rsi
                                        # kill: %DL<def> %DL<kill> %RDX<kill>
	addb	$-11, %dl
	leaq	33(%rcx), %rdi
	cmpb	$2, %dl
	cmovbq	%rdi, %rsi
	movzbl	(%rsi), %esi
	movq	zz_free(,%rsi,8), %rdx
	movq	%rdx, (%rcx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rsi,8)
	movq	xx_hold(%rip), %rdx
	movq	8(%rdx), %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_30
# BB#35:                                # %._crit_edge
	movl	%esi, zz_size(%rip)
	jmp	.LBB7_36
.LBB7_29:
	movq	%rcx, %rdx
.LBB7_36:
	movq	%rdx, zz_hold(%rip)
	movzbl	32(%rdx), %ecx
	leaq	zz_lengths(%rcx), %rsi
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rdx), %rdi
	cmpb	$2, %cl
	cmovbq	%rdi, %rsi
	movzbl	(%rsi), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rsi
	movq	%rsi, (%rdx)
	movq	zz_hold(%rip), %rdx
	movq	%rdx, zz_free(,%rcx,8)
	cmpb	$2, %al
	je	.LBB7_38
# BB#37:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.71, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_38:
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	ParseEnvClosure, .Lfunc_end7-ParseEnvClosure
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"OptimizeCase:  type(x) != CASE!"
	.size	.L.str.1, 32

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"SetScope: type(env) != ENV!"
	.size	.L.str.2, 28

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"SetScope: LastDown(y)!"
	.size	.L.str.3, 23

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"SetScope: type(yenv) != ENV!"
	.size	.L.str.4, 29

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"cross reference database file name %s is too long"
	.size	.L.str.5, 50

	.type	cross_name,@object      # @cross_name
	.local	cross_name
	.comm	cross_name,8,8
	.type	ttop,@object            # @ttop
	.data
	.p2align	2
ttop:
	.long	4294967295              # 0xffffffff
	.size	ttop, 4

	.type	tok_stack,@object       # @tok_stack
	.local	tok_stack
	.comm	tok_stack,800,16
	.type	.L.str.6,@object        # @.str.6
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.6:
	.asciz	"expression is too deeply nested"
	.size	.L.str.6, 32

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Parse: *token!"
	.size	.L.str.7, 15

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"exiting now"
	.size	.L.str.9, 12

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"def"
	.size	.L.str.10, 4

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"langdef"
	.size	.L.str.11, 8

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"macro"
	.size	.L.str.12, 6

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"import"
	.size	.L.str.13, 7

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"extend"
	.size	.L.str.14, 7

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"export"
	.size	.L.str.15, 7

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"%s expected after %s"
	.size	.L.str.16, 21

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"{"
	.size	.L.str.17, 2

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"@Use"
	.size	.L.str.18, 5

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"%s or %s tag not allowed here"
	.size	.L.str.19, 30

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"preceding"
	.size	.L.str.20, 10

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"following"
	.size	.L.str.21, 10

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"invalid parameter of %s"
	.size	.L.str.22, 24

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"symbol %s occurs in two %s clauses"
	.size	.L.str.23, 35

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"symbol %s unknown or misspelt"
	.size	.L.str.24, 30

	.type	unknown_count,@object   # @unknown_count
	.local	unknown_count
	.comm	unknown_count,4,4
	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"too many errors (%s lines missing or out of order?)"
	.size	.L.str.25, 52

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"@SysInclude"
	.size	.L.str.26, 12

	.type	otop,@object            # @otop
	.data
	.p2align	2
otop:
	.long	4294967295              # 0xffffffff
	.size	otop, 4

	.type	obj_stack,@object       # @obj_stack
	.local	obj_stack
	.comm	obj_stack,800,16
	.type	.L.str.27,@object       # @.str.27
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.27:
	.zero	1
	.size	.L.str.27, 1

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"right parameter of %s or %s must be enclosed in braces"
	.size	.L.str.28, 55

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"@Verbatim"
	.size	.L.str.29, 10

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"@RawVerbatim"
	.size	.L.str.30, 13

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"unexpected end of input"
	.size	.L.str.31, 24

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"%s replaced by %s"
	.size	.L.str.32, 18

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"@Begin"
	.size	.L.str.33, 7

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"unknown or misspelt symbol %s after %s deleted"
	.size	.L.str.34, 47

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"@End"
	.size	.L.str.35, 5

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"symbol expected after %s"
	.size	.L.str.36, 25

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"%s symbol out of place"
	.size	.L.str.37, 23

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"%d %d"
	.size	.L.str.38, 6

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"error in cross reference database"
	.size	.L.str.39, 34

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"symbol expected following %s"
	.size	.L.str.40, 29

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"@LUse"
	.size	.L.str.41, 6

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"%s clause(s) changed from previous run"
	.size	.L.str.42, 39

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"@@V"
	.size	.L.str.43, 4

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"%s must follow named parameter %s"
	.size	.L.str.44, 34

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"Parse: type(y) != PAR!"
	.size	.L.str.45, 23

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"named parameter %s of %s appears twice"
	.size	.L.str.46, 39

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"compulsory option %s missing from %s"
	.size	.L.str.47, 37

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"%s out of place here (%s has no right parameter)"
	.size	.L.str.48, 49

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"Parse: cannot undo rpar"
	.size	.L.str.49, 24

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"Parse: cannot undo lpar"
	.size	.L.str.50, 24

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"right parameter of %s must be enclosed in braces"
	.size	.L.str.51, 49

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"body parameter of %s must be enclosed in braces"
	.size	.L.str.52, 48

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"invalid left parameter of %s"
	.size	.L.str.53, 29

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"@Open"
	.size	.L.str.54, 6

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"assert failed in %s %s"
	.size	.L.str.55, 23

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"Parse:"
	.size	.L.str.56, 7

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"else"
	.size	.L.str.57, 5

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"left parameter of %s is not a symbol (or not visible)"
	.size	.L.str.58, 54

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"unmatched %s (inserted %s)"
	.size	.L.str.59, 27

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"}"
	.size	.L.str.60, 2

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"Reduce: unmatched"
	.size	.L.str.61, 18

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"unmatched %s; inserted %s at%s (after %s)"
	.size	.L.str.62, 42

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"unmatched %s not enclosed in anything"
	.size	.L.str.63, 38

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"%s %s appended at end of file to match %s at%s"
	.size	.L.str.64, 47

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"%s replaced by %s %s to match %s at%s"
	.size	.L.str.65, 38

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"??"
	.size	.L.str.66, 3

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"%s %s replaced by %s %s to match %s at%s"
	.size	.L.str.67, 41

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"Reduce:"
	.size	.L.str.68, 8

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"ParseEnvClosure: type(t) != ENV!"
	.size	.L.str.69, 33

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"syntax error in cross reference database"
	.size	.L.str.70, 41

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"ParseEnvClosure: type(res) != CLOSURE!"
	.size	.L.str.71, 39


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
