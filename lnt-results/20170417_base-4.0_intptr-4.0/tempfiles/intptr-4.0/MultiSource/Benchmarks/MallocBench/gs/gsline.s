	.text
	.file	"gsline.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	gs_setlinewidth
	.p2align	4, 0x90
	.type	gs_setlinewidth,@function
gs_setlinewidth:                        # @gs_setlinewidth
	.cfi_startproc
# BB#0:
	movl	$-15, %eax
	xorps	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	ja	.LBB0_2
# BB#1:
	mulsd	.LCPI0_0(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movq	280(%rdi), %rax
	movss	%xmm0, (%rax)
	xorl	%eax, %eax
.LBB0_2:
	retq
.Lfunc_end0:
	.size	gs_setlinewidth, .Lfunc_end0-gs_setlinewidth
	.cfi_endproc

	.globl	gs_currentlinewidth
	.p2align	4, 0x90
	.type	gs_currentlinewidth,@function
gs_currentlinewidth:                    # @gs_currentlinewidth
	.cfi_startproc
# BB#0:
	movq	280(%rdi), %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm0
	retq
.Lfunc_end1:
	.size	gs_currentlinewidth, .Lfunc_end1-gs_currentlinewidth
	.cfi_endproc

	.globl	gs_setlinecap
	.p2align	4, 0x90
	.type	gs_setlinecap,@function
gs_setlinecap:                          # @gs_setlinecap
	.cfi_startproc
# BB#0:
	movq	280(%rdi), %rax
	movl	%esi, 4(%rax)
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	gs_setlinecap, .Lfunc_end2-gs_setlinecap
	.cfi_endproc

	.globl	gs_currentlinecap
	.p2align	4, 0x90
	.type	gs_currentlinecap,@function
gs_currentlinecap:                      # @gs_currentlinecap
	.cfi_startproc
# BB#0:
	movq	280(%rdi), %rax
	movl	4(%rax), %eax
	retq
.Lfunc_end3:
	.size	gs_currentlinecap, .Lfunc_end3-gs_currentlinecap
	.cfi_endproc

	.globl	gs_setlinejoin
	.p2align	4, 0x90
	.type	gs_setlinejoin,@function
gs_setlinejoin:                         # @gs_setlinejoin
	.cfi_startproc
# BB#0:
	movq	280(%rdi), %rax
	movl	%esi, 8(%rax)
	xorl	%eax, %eax
	retq
.Lfunc_end4:
	.size	gs_setlinejoin, .Lfunc_end4-gs_setlinejoin
	.cfi_endproc

	.globl	gs_currentlinejoin
	.p2align	4, 0x90
	.type	gs_currentlinejoin,@function
gs_currentlinejoin:                     # @gs_currentlinejoin
	.cfi_startproc
# BB#0:
	movq	280(%rdi), %rax
	movl	8(%rax), %eax
	retq
.Lfunc_end5:
	.size	gs_currentlinejoin, .Lfunc_end5-gs_currentlinejoin
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4607182418800017408     # double 1
.LCPI6_2:
	.quad	4611686243607369273     # double 2.0001000000000002
.LCPI6_3:
	.quad	4611685568067425167     # double 1.9999
.LCPI6_4:
	.quad	-4616189618054758400    # double -1
.LCPI6_5:
	.quad	-4611686018427387904    # double -2
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_1:
	.long	1232348160              # float 1.0E+6
	.text
	.globl	gs_setmiterlimit
	.p2align	4, 0x90
	.type	gs_setmiterlimit,@function
gs_setmiterlimit:                       # @gs_setmiterlimit
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movapd	%xmm0, %xmm2
	movq	%rdi, %rbx
	movl	$-15, %eax
	movsd	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm2, %xmm0
	ja	.LBB6_8
# BB#1:
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm2, %xmm0
	movq	280(%rbx), %rax
	movss	%xmm0, 12(%rax)
	mulsd	%xmm2, %xmm2
	movsd	.LCPI6_2(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm2, %xmm0
	jbe	.LBB6_4
# BB#2:
	ucomisd	.LCPI6_3(%rip), %xmm2
	jbe	.LBB6_4
# BB#3:
	movss	.LCPI6_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB6_7
.LBB6_4:
	movsd	.LCPI6_4(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB6_6
# BB#5:                                 # %call.sqrt
	movapd	%xmm1, %xmm0
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	callq	sqrt
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB6_6:                                # %.split
	addsd	%xmm0, %xmm0
	addsd	.LCPI6_5(%rip), %xmm2
	divsd	%xmm2, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movq	280(%rbx), %rax
.LBB6_7:
	movss	%xmm0, 16(%rax)
	xorl	%eax, %eax
.LBB6_8:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end6:
	.size	gs_setmiterlimit, .Lfunc_end6-gs_setmiterlimit
	.cfi_endproc

	.globl	gs_currentmiterlimit
	.p2align	4, 0x90
	.type	gs_currentmiterlimit,@function
gs_currentmiterlimit:                   # @gs_currentmiterlimit
	.cfi_startproc
# BB#0:
	movq	280(%rdi), %rax
	movss	12(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end7:
	.size	gs_currentmiterlimit, .Lfunc_end7-gs_currentmiterlimit
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_1:
	.long	0                       # float 0
	.text
	.globl	gs_setdash
	.p2align	4, 0x90
	.type	gs_setdash,@function
gs_setdash:                             # @gs_setdash
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi6:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi7:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi9:
	.cfi_def_cfa_offset 96
.Lcfi10:
	.cfi_offset %rbx, -56
.Lcfi11:
	.cfi_offset %r12, -48
.Lcfi12:
	.cfi_offset %r13, -40
.Lcfi13:
	.cfi_offset %r14, -32
.Lcfi14:
	.cfi_offset %r15, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movaps	%xmm0, %xmm3
	movq	%rsi, %rbx
	movq	280(%rdi), %r12
	movl	%edx, %ebp
	xorps	%xmm1, %xmm1
	xorl	%eax, %eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB8_1:                                # =>This Inner Loop Header: Depth=1
	cmpl	%eax, %ebp
	je	.LBB8_4
# BB#2:                                 #   in Loop: Header=BB8_1 Depth=1
	movss	(%rbx,%rax,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	incq	%rax
	ucomiss	%xmm2, %xmm1
	addss	%xmm2, %xmm0
	jbe	.LBB8_1
# BB#3:
	movl	$-15, %eax
	jmp	.LBB8_16
.LBB8_4:
	xorl	%r13d, %r13d
	testl	%ebp, %ebp
	je	.LBB8_5
# BB#6:
	movl	$-15, %eax
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB8_7
	jnp	.LBB8_16
.LBB8_7:
	movl	%edx, 4(%rsp)           # 4-byte Spill
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm0, %xmm1
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movaps	%xmm3, %xmm0
	divsd	%xmm1, %xmm0
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	callq	floor
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	subss	(%rbx), %xmm1
	ucomiss	.LCPI8_1, %xmm1
	jae	.LBB8_9
# BB#8:
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	xorl	%r13d, %r13d
	movb	$1, %r14b
	jmp	.LBB8_12
.LBB8_5:
	xorps	%xmm1, %xmm1
	movb	$1, %r14b
	xorl	%r15d, %r15d
	jmp	.LBB8_15
.LBB8_9:                                # %.lr.ph.preheader
	movb	$1, %r14b
	xorl	%r13d, %r13d
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB8_10:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorb	$1, %r14b
	subss	4(%rbx,%r13,4), %xmm1
	incq	%r13
	ucomiss	%xmm0, %xmm1
	jae	.LBB8_10
# BB#11:                                # %._crit_edge.loopexit
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
.LBB8_12:                               # %._crit_edge
	movl	4(%rsp), %edi           # 4-byte Reload
	movl	$4, %esi
	movl	$.L.str, %edx
	callq	gs_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB8_13
# BB#14:
	shlq	$2, %rbp
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movl	4(%rsp), %edx           # 4-byte Reload
	movaps	16(%rsp), %xmm1         # 16-byte Reload
.LBB8_15:
	movq	%r15, 24(%r12)
	movl	%edx, 32(%r12)
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm3, %xmm0
	movss	%xmm0, 36(%r12)
	movsbl	%r14b, %eax
	movl	%eax, 40(%r12)
	movl	%r13d, 44(%r12)
	xorps	.LCPI8_0(%rip), %xmm1
	movss	%xmm1, 48(%r12)
	xorl	%eax, %eax
.LBB8_16:                               # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_13:
	movl	$-25, %eax
	jmp	.LBB8_16
.Lfunc_end8:
	.size	gs_setdash, .Lfunc_end8-gs_setdash
	.cfi_endproc

	.globl	gs_currentdash_length
	.p2align	4, 0x90
	.type	gs_currentdash_length,@function
gs_currentdash_length:                  # @gs_currentdash_length
	.cfi_startproc
# BB#0:
	movq	280(%rdi), %rax
	movl	32(%rax), %eax
	retq
.Lfunc_end9:
	.size	gs_currentdash_length, .Lfunc_end9-gs_currentdash_length
	.cfi_endproc

	.globl	gs_currentdash_pattern
	.p2align	4, 0x90
	.type	gs_currentdash_pattern,@function
gs_currentdash_pattern:                 # @gs_currentdash_pattern
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 16
	movq	280(%rdi), %rcx
	movq	24(%rcx), %rax
	movl	32(%rcx), %edx
	shlq	$2, %rdx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	memcpy
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end10:
	.size	gs_currentdash_pattern, .Lfunc_end10-gs_currentdash_pattern
	.cfi_endproc

	.globl	gs_currentdash_offset
	.p2align	4, 0x90
	.type	gs_currentdash_offset,@function
gs_currentdash_offset:                  # @gs_currentdash_offset
	.cfi_startproc
# BB#0:
	movq	280(%rdi), %rax
	movss	36(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end11:
	.size	gs_currentdash_offset, .Lfunc_end11-gs_currentdash_offset
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"dash pattern"
	.size	.L.str, 13


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
