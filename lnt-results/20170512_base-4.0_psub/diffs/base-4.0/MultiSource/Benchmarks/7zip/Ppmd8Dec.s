	.text
	.file	"Ppmd8Dec.bc"
	.globl	Ppmd8_RangeDec_Init
	.p2align	4, 0x90
	.type	Ppmd8_RangeDec_Init,@function
Ppmd8_RangeDec_Init:                    # @Ppmd8_RangeDec_Init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	$0, 112(%r15)
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 104(%r15)
	movq	120(%r15), %rdi
	callq	*(%rdi)
	movzbl	%al, %r14d
	movl	%r14d, 108(%r15)
	shll	$8, %r14d
	movq	120(%r15), %rdi
	callq	*(%rdi)
	movzbl	%al, %ebp
	orl	%r14d, %ebp
	movl	%ebp, 108(%r15)
	shll	$8, %ebp
	movq	120(%r15), %rdi
	callq	*(%rdi)
	movzbl	%al, %ebx
	orl	%ebp, %ebx
	movl	%ebx, 108(%r15)
	shll	$8, %ebx
	movq	120(%r15), %rdi
	callq	*(%rdi)
	movzbl	%al, %ecx
	orl	%ebx, %ecx
	movl	%ecx, 108(%r15)
	xorl	%eax, %eax
	cmpl	$-1, %ecx
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	Ppmd8_RangeDec_Init, .Lfunc_end0-Ppmd8_RangeDec_Init
	.cfi_endproc

	.globl	Ppmd8_DecodeSymbol
	.p2align	4, 0x90
	.type	Ppmd8_DecodeSymbol,@function
Ppmd8_DecodeSymbol:                     # @Ppmd8_DecodeSymbol
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$2376, %rsp             # imm = 0x948
.Lcfi15:
	.cfi_def_cfa_offset 2432
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	movzbl	(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB1_29
# BB#1:
	movq	56(%rbx), %r14
	movl	4(%rax), %esi
	movzwl	2(%rax), %r9d
	movl	108(%rbx), %ebp
	leaq	104(%rbx), %r13
	movl	104(%rbx), %eax
	xorl	%edx, %edx
	divl	%r9d
	movl	%eax, %r8d
	movl	%r8d, 104(%rbx)
	xorl	%edx, %edx
	movl	%ebp, %eax
	divl	%r8d
	movzbl	1(%r14,%rsi), %edx
	cmpl	%edx, %eax
	jae	.LBB1_7
# BB#2:
	addq	%rsi, %r14
	movl	112(%rbx), %eax
	imull	%r8d, %edx
	movl	%edx, 104(%rbx)
	jmp	.LBB1_3
	.p2align	4, 0x90
.LBB1_6:                                # %.critedge.i195
                                        #   in Loop: Header=BB1_3 Depth=1
	shll	$8, %ebp
	movq	120(%rbx), %rdi
	callq	*(%rdi)
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 108(%rbx)
	movl	104(%rbx), %edx
	shll	$8, %edx
	movl	%edx, 104(%rbx)
	movl	112(%rbx), %eax
	shll	$8, %eax
	movl	%eax, 112(%rbx)
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rax), %ecx
	xorl	%eax, %ecx
	cmpl	$16777216, %ecx         # imm = 0x1000000
	jb	.LBB1_6
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=1
	cmpl	$32767, %edx            # imm = 0x7FFF
	ja	.LBB1_75
# BB#5:                                 #   in Loop: Header=BB1_3 Depth=1
	negl	%eax
	andl	$32767, %eax            # imm = 0x7FFF
	movl	%eax, (%r13)
	jmp	.LBB1_6
.LBB1_29:
	movzbl	3(%rax), %ecx
	movzbl	855(%rbx,%rcx), %ecx
	movq	56(%rbx), %rdx
	movl	8(%rax), %esi
	movzbl	(%rdx,%rsi), %edx
	movzbl	600(%rbx,%rdx), %edx
	addl	32(%rbx), %edx
	movzbl	1(%rax), %eax
	addl	%edx, %eax
	movl	40(%rbx), %edi
	shrl	$26, %edi
	andl	$32, %edi
	addl	%eax, %edi
	shlq	$7, %rcx
	addq	%rbx, %rcx
	leaq	4192(%rcx,%rdi,2), %r15
	movl	108(%rbx), %ebp
	leaq	104(%rbx), %r13
	movl	104(%rbx), %esi
	shrl	$14, %esi
	movl	%esi, 104(%rbx)
	xorl	%edx, %edx
	movl	%ebp, %eax
	divl	%esi
	movzwl	4192(%rcx,%rdi,2), %ecx
	cmpl	%ecx, %eax
	jae	.LBB1_35
# BB#30:
	movl	112(%rbx), %eax
	imull	%esi, %ecx
	movl	%ecx, 104(%rbx)
	jmp	.LBB1_31
	.p2align	4, 0x90
.LBB1_34:                               # %.critedge.i201
                                        #   in Loop: Header=BB1_31 Depth=1
	shll	$8, %ebp
	movq	120(%rbx), %rdi
	callq	*(%rdi)
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 108(%rbx)
	movl	104(%rbx), %ecx
	shll	$8, %ecx
	movl	%ecx, 104(%rbx)
	movl	112(%rbx), %eax
	shll	$8, %eax
	movl	%eax, 112(%rbx)
.LBB1_31:                               # =>This Inner Loop Header: Depth=1
	leal	(%rcx,%rax), %edx
	xorl	%eax, %edx
	cmpl	$16777216, %edx         # imm = 0x1000000
	jb	.LBB1_34
# BB#32:                                #   in Loop: Header=BB1_31 Depth=1
	cmpl	$32767, %ecx            # imm = 0x7FFF
	ja	.LBB1_77
# BB#33:                                #   in Loop: Header=BB1_31 Depth=1
	negl	%eax
	andl	$32767, %eax            # imm = 0x7FFF
	movl	%eax, (%r13)
	jmp	.LBB1_34
.LBB1_7:
	leaq	56(%rbx), %rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	108(%rbx), %r10
	movl	$0, 32(%rbx)
	negl	%ecx
	leaq	6(%r14,%rsi), %r15
	.p2align	4, 0x90
.LBB1_8:                                # =>This Inner Loop Header: Depth=1
	movzbl	1(%r15), %esi
	leal	(%rsi,%rdx), %edi
	cmpl	%eax, %edi
	ja	.LBB1_9
# BB#15:                                #   in Loop: Header=BB1_8 Depth=1
	addq	$6, %r15
	incl	%ecx
	movl	%edi, %edx
	jne	.LBB1_8
# BB#16:
	movl	$-2, %r14d
	cmpl	%r9d, %eax
	jae	.LBB1_76
# BB#17:
	leaq	-6(%r15), %r12
	subl	%edi, %r9d
	imull	%r8d, %edi
	leaq	112(%rbx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	subl	%edi, %ebp
	addl	112(%rbx), %edi
	movl	%edi, 112(%rbx)
	movl	%ebp, 108(%rbx)
	imull	%r8d, %r9d
	movl	%r9d, 104(%rbx)
	leaq	120(%rbx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%r10, %r14
	jmp	.LBB1_18
	.p2align	4, 0x90
.LBB1_28:                               # %.critedge.i199
                                        #   in Loop: Header=BB1_18 Depth=1
	shll	$8, %ebp
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	callq	*(%rdi)
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, (%r14)
	movl	(%r13), %r9d
	shll	$8, %r9d
	movl	%r9d, (%r13)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %edi
	shll	$8, %edi
	movl	%edi, (%rax)
.LBB1_18:                               # =>This Inner Loop Header: Depth=1
	leal	(%r9,%rdi), %eax
	xorl	%edi, %eax
	cmpl	$16777216, %eax         # imm = 0x1000000
	jb	.LBB1_28
# BB#19:                                #   in Loop: Header=BB1_18 Depth=1
	cmpl	$32768, %r9d            # imm = 0x8000
	jae	.LBB1_20
# BB#27:                                #   in Loop: Header=BB1_18 Depth=1
	negl	%edi
	andl	$32767, %edi            # imm = 0x7FFF
	movl	%edi, (%r13)
	jmp	.LBB1_28
.LBB1_75:                               # %RangeDec_Decode.exit196
	movq	%r14, 16(%rbx)
	movzbl	(%r14), %r14d
	movq	%rbx, %rdi
	callq	Ppmd8_Update1_0
	jmp	.LBB1_76
.LBB1_35:
	leaq	56(%rbx), %r12
	leaq	108(%rbx), %r14
	movl	$16384, %eax            # imm = 0x4000
	subl	%ecx, %eax
	imull	%esi, %ecx
	leaq	112(%rbx), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	subl	%ecx, %ebp
	addl	112(%rbx), %ecx
	movl	%ecx, 112(%rbx)
	movl	%ebp, 108(%rbx)
	imull	%esi, %eax
	movl	%eax, 104(%rbx)
	leaq	120(%rbx), %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	jmp	.LBB1_36
	.p2align	4, 0x90
.LBB1_39:                               # %.critedge.i203
                                        #   in Loop: Header=BB1_36 Depth=1
	shll	$8, %ebp
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	callq	*(%rdi)
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, (%r14)
	movl	(%r13), %eax
	shll	$8, %eax
	movl	%eax, (%r13)
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx), %ecx
	shll	$8, %ecx
	movl	%ecx, (%rdx)
.LBB1_36:                               # =>This Inner Loop Header: Depth=1
	leal	(%rax,%rcx), %edx
	xorl	%ecx, %edx
	cmpl	$16777216, %edx         # imm = 0x1000000
	jb	.LBB1_39
# BB#37:                                #   in Loop: Header=BB1_36 Depth=1
	cmpl	$32767, %eax            # imm = 0x7FFF
	ja	.LBB1_40
# BB#38:                                #   in Loop: Header=BB1_36 Depth=1
	negl	%ecx
	andl	$32767, %ecx            # imm = 0x7FFF
	movl	%ecx, (%r13)
	jmp	.LBB1_39
.LBB1_77:
	movzwl	(%r15), %eax
	leal	32(%rax), %ecx
	shrl	$7, %ecx
	negl	%ecx
	leal	128(%rax,%rcx), %eax
	movw	%ax, (%r15)
	movq	(%rbx), %rax
	leaq	2(%rax), %rcx
	movq	%rcx, 16(%rbx)
	movzbl	2(%rax), %r14d
	movq	%rbx, %rdi
	callq	Ppmd8_UpdateBin
	jmp	.LBB1_76
.LBB1_40:                               # %RangeDec_Decode.exit204
	movzwl	(%r15), %eax
	leal	32(%rax), %ecx
	shrl	$7, %ecx
	subl	%ecx, %eax
	movw	%ax, (%r15)
	shrl	$10, %eax
	andl	$63, %eax
	movzbl	PPMD8_kExpEscape(%rax), %eax
	movl	%eax, 28(%rbx)
	pcmpeqd	%xmm0, %xmm0
	movdqa	%xmm0, 304(%rsp)
	movdqa	%xmm0, 288(%rsp)
	movdqa	%xmm0, 272(%rsp)
	movdqa	%xmm0, 256(%rsp)
	movdqa	%xmm0, 240(%rsp)
	movdqa	%xmm0, 224(%rsp)
	movdqa	%xmm0, 208(%rsp)
	movdqa	%xmm0, 192(%rsp)
	movdqa	%xmm0, 176(%rsp)
	movdqa	%xmm0, 160(%rsp)
	movdqa	%xmm0, 144(%rsp)
	movdqa	%xmm0, 128(%rsp)
	movdqa	%xmm0, 112(%rsp)
	movdqa	%xmm0, 96(%rsp)
	movdqa	%xmm0, 80(%rsp)
	movdqa	%xmm0, 64(%rsp)
	movq	(%rbx), %rax
	movzbl	2(%rax), %ecx
	movb	$0, 64(%rsp,%rcx)
	movl	$0, 32(%rbx)
	movq	%r12, %r8
.LBB1_41:                               # %.preheader
	leaq	12(%rsp), %rdx
	jmp	.LBB1_42
	.p2align	4, 0x90
.LBB1_73:                               #   in Loop: Header=BB1_42 Depth=1
	movq	(%rbx), %rax
.LBB1_42:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_43 Depth 2
                                        #     Child Loop BB1_46 Depth 2
                                        #     Child Loop BB1_62 Depth 2
                                        #     Child Loop BB1_69 Depth 2
                                        #     Child Loop BB1_72 Depth 2
	movzbl	(%rax), %esi
	movl	24(%rbx), %edi
	.p2align	4, 0x90
.LBB1_43:                               #   Parent Loop BB1_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%rax), %ebp
	incl	%edi
	testq	%rbp, %rbp
	je	.LBB1_74
# BB#44:                                #   in Loop: Header=BB1_43 Depth=2
	movq	(%r8), %rcx
	leaq	(%rcx,%rbp), %rax
	movq	%rax, (%rbx)
	movzbl	(%rcx,%rbp), %r12d
	cmpb	%sil, %r12b
	je	.LBB1_43
# BB#45:                                #   in Loop: Header=BB1_42 Depth=1
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movl	%edi, 24(%rbx)
	movl	4(%rax), %eax
	addq	%rax, %rcx
	movl	%r12d, %r15d
	subl	%esi, %r15d
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_46:                               #   Parent Loop BB1_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %edi
	movsbl	64(%rsp,%rdi), %edi
	movzbl	1(%rcx), %ebp
	andl	%edi, %ebp
	addl	%ebp, %r14d
	movl	%eax, %ebp
	movq	%rcx, 320(%rsp,%rbp,8)
	subl	%edi, %eax
	addq	$6, %rcx
	cmpl	%r15d, %eax
	jne	.LBB1_46
# BB#47:                                #   in Loop: Header=BB1_42 Depth=1
	movq	%rbx, %rdi
	movl	%esi, 52(%rsp)          # 4-byte Spill
	callq	Ppmd8_MakeEscFreq
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	12(%rsp), %esi
	leal	(%rsi,%r14), %edi
	movl	%edi, 12(%rsp)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %ebp
	movl	(%r13), %eax
	xorl	%edx, %edx
	divl	%edi
	movl	%eax, %r8d
	movl	%r8d, (%r13)
	xorl	%edx, %edx
	movl	%ebp, %eax
	divl	%r8d
	cmpl	%r14d, %eax
	jb	.LBB1_48
# BB#59:                                #   in Loop: Header=BB1_42 Depth=1
	cmpl	%edi, %eax
	jae	.LBB1_60
# BB#61:                                #   in Loop: Header=BB1_42 Depth=1
	imull	%r8d, %r14d
	subl	%r14d, %ebp
	movq	24(%rsp), %rax          # 8-byte Reload
	addl	(%rax), %r14d
	movl	%r14d, (%rax)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%ebp, (%rax)
	imull	%r8d, %esi
	movl	%esi, (%r13)
	jmp	.LBB1_62
	.p2align	4, 0x90
.LBB1_65:                               # %.critedge.i
                                        #   in Loop: Header=BB1_62 Depth=2
	shll	$8, %ebp
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	callq	*(%rdi)
	movzbl	%al, %eax
	orl	%eax, %ebp
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%ebp, (%rax)
	movl	(%r13), %esi
	shll	$8, %esi
	movl	%esi, (%r13)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r14d
	shll	$8, %r14d
	movl	%r14d, (%rax)
.LBB1_62:                               #   Parent Loop BB1_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rsi,%r14), %eax
	xorl	%r14d, %eax
	cmpl	$16777216, %eax         # imm = 0x1000000
	jb	.LBB1_65
# BB#63:                                #   in Loop: Header=BB1_62 Depth=2
	cmpl	$32767, %esi            # imm = 0x7FFF
	ja	.LBB1_66
# BB#64:                                #   in Loop: Header=BB1_62 Depth=2
	negl	%r14d
	andl	$32767, %r14d           # imm = 0x7FFF
	movl	%r14d, (%r13)
	jmp	.LBB1_65
	.p2align	4, 0x90
.LBB1_66:                               # %RangeDec_Decode.exit
                                        #   in Loop: Header=BB1_42 Depth=1
	movq	56(%rsp), %rcx          # 8-byte Reload
	movzwl	(%rcx), %eax
	addl	12(%rsp), %eax
	movw	%ax, (%rcx)
	decl	%r12d
	subl	52(%rsp), %r12d         # 4-byte Folded Reload
	movl	%r15d, %eax
	andl	$3, %eax
	je	.LBB1_67
# BB#68:                                # %.prol.preheader
                                        #   in Loop: Header=BB1_42 Depth=1
	negl	%eax
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	leaq	12(%rsp), %rdx
	.p2align	4, 0x90
.LBB1_69:                               #   Parent Loop BB1_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%r15d
	movq	320(%rsp,%r15,8), %rcx
	movzbl	(%rcx), %ecx
	movb	$0, 64(%rsp,%rcx)
	incl	%eax
	jne	.LBB1_69
	jmp	.LBB1_70
.LBB1_67:                               #   in Loop: Header=BB1_42 Depth=1
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	leaq	12(%rsp), %rdx
.LBB1_70:                               # %.prol.loopexit
                                        #   in Loop: Header=BB1_42 Depth=1
	cmpl	$3, %r12d
	jb	.LBB1_73
# BB#71:                                # %RangeDec_Decode.exit.new
                                        #   in Loop: Header=BB1_42 Depth=1
	addl	$-2, %r15d
	.p2align	4, 0x90
.LBB1_72:                               #   Parent Loop BB1_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1(%r15), %eax
	movq	320(%rsp,%rax,8), %rax
	movzbl	(%rax), %eax
	movb	$0, 64(%rsp,%rax)
	movl	%r15d, %eax
	movq	320(%rsp,%rax,8), %rax
	movzbl	(%rax), %eax
	movb	$0, 64(%rsp,%rax)
	leal	-1(%r15), %eax
	movq	320(%rsp,%rax,8), %rax
	movzbl	(%rax), %eax
	movb	$0, 64(%rsp,%rax)
	leal	-2(%r15), %eax
	movq	320(%rsp,%rax,8), %rax
	movzbl	(%rax), %eax
	movb	$0, 64(%rsp,%rax)
	addl	$-4, %r15d
	cmpl	$-2, %r15d
	jne	.LBB1_72
	jmp	.LBB1_73
.LBB1_74:                               # %.thread209.loopexit
	movl	%edi, 24(%rbx)
	movl	$-1, %r14d
	jmp	.LBB1_76
.LBB1_9:
	imull	%r8d, %edx
	subl	%edx, %ebp
	addl	112(%rbx), %edx
	movl	%edx, 112(%rbx)
	movl	%ebp, 108(%rbx)
	imull	%r8d, %esi
	movl	%esi, 104(%rbx)
	jmp	.LBB1_10
	.p2align	4, 0x90
.LBB1_13:                               # %.critedge.i197
                                        #   in Loop: Header=BB1_10 Depth=1
	shll	$8, %ebp
	movq	120(%rbx), %rdi
	callq	*(%rdi)
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 108(%rbx)
	movl	104(%rbx), %esi
	shll	$8, %esi
	movl	%esi, 104(%rbx)
	movl	112(%rbx), %edx
	shll	$8, %edx
	movl	%edx, 112(%rbx)
.LBB1_10:                               # =>This Inner Loop Header: Depth=1
	leal	(%rsi,%rdx), %eax
	xorl	%edx, %eax
	cmpl	$16777216, %eax         # imm = 0x1000000
	jb	.LBB1_13
# BB#11:                                #   in Loop: Header=BB1_10 Depth=1
	cmpl	$32767, %esi            # imm = 0x7FFF
	ja	.LBB1_14
# BB#12:                                #   in Loop: Header=BB1_10 Depth=1
	negl	%edx
	andl	$32767, %edx            # imm = 0x7FFF
	movl	%edx, (%r13)
	jmp	.LBB1_13
.LBB1_14:                               # %RangeDec_Decode.exit198
	movq	%r15, 16(%rbx)
	movzbl	(%r15), %r14d
	movq	%rbx, %rdi
	callq	Ppmd8_Update1
.LBB1_76:                               # %.thread
	movl	%r14d, %eax
	addq	$2376, %rsp             # imm = 0x948
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_20:                               # %RangeDec_Decode.exit200.preheader
	pcmpeqd	%xmm0, %xmm0
	movdqa	%xmm0, 304(%rsp)
	movdqa	%xmm0, 288(%rsp)
	movdqa	%xmm0, 272(%rsp)
	movdqa	%xmm0, 256(%rsp)
	movdqa	%xmm0, 240(%rsp)
	movdqa	%xmm0, 224(%rsp)
	movdqa	%xmm0, 208(%rsp)
	movdqa	%xmm0, 192(%rsp)
	movdqa	%xmm0, 176(%rsp)
	movdqa	%xmm0, 160(%rsp)
	movdqa	%xmm0, 144(%rsp)
	movdqa	%xmm0, 128(%rsp)
	movdqa	%xmm0, 112(%rsp)
	movdqa	%xmm0, 96(%rsp)
	movdqa	%xmm0, 80(%rsp)
	movdqa	%xmm0, 64(%rsp)
	movzbl	-6(%r15), %eax
	movb	$0, 64(%rsp,%rax)
	movq	(%rbx), %rax
	movzbl	(%rax), %ecx
	leal	-1(%rcx), %edx
	movl	%ecx, %esi
	andl	$3, %esi
	je	.LBB1_21
# BB#22:                                # %.prol.preheader335
	negl	%esi
	movq	32(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB1_23:                               # =>This Inner Loop Header: Depth=1
	movzbl	-6(%r12), %edi
	addq	$-6, %r12
	movb	$0, 64(%rsp,%rdi)
	decl	%ecx
	incl	%esi
	jne	.LBB1_23
	jmp	.LBB1_24
.LBB1_21:
	movq	32(%rsp), %r8           # 8-byte Reload
.LBB1_24:                               # %.prol.loopexit336
	cmpl	$3, %edx
	jb	.LBB1_41
# BB#25:                                # %RangeDec_Decode.exit200.preheader.new
	addq	$-6, %r12
	.p2align	4, 0x90
.LBB1_26:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%r12), %edx
	movb	$0, 64(%rsp,%rdx)
	movzbl	-6(%r12), %edx
	movb	$0, 64(%rsp,%rdx)
	movzbl	-12(%r12), %edx
	movb	$0, 64(%rsp,%rdx)
	movzbl	-18(%r12), %edx
	movb	$0, 64(%rsp,%rdx)
	addq	$-24, %r12
	addl	$-4, %ecx
	jne	.LBB1_26
	jmp	.LBB1_41
.LBB1_48:
	xorl	%ecx, %ecx
	leaq	320(%rsp), %rdi
	.p2align	4, 0x90
.LBB1_49:                               # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	movq	(%rdi), %r14
	movzbl	1(%r14), %esi
	leal	(%rsi,%rdx), %ecx
	addq	$8, %rdi
	cmpl	%eax, %ecx
	jbe	.LBB1_49
# BB#50:
	imull	%r8d, %edx
	subl	%edx, %ebp
	movq	24(%rsp), %rax          # 8-byte Reload
	addl	(%rax), %edx
	movl	%edx, (%rax)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%ebp, (%rax)
	imull	%r8d, %esi
	movl	%esi, (%r13)
	jmp	.LBB1_51
	.p2align	4, 0x90
.LBB1_54:                               # %.critedge.i205
                                        #   in Loop: Header=BB1_51 Depth=1
	shll	$8, %ebp
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	callq	*(%rdi)
	movzbl	%al, %eax
	orl	%eax, %ebp
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%ebp, (%rax)
	movl	(%r13), %esi
	shll	$8, %esi
	movl	%esi, (%r13)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %edx
	shll	$8, %edx
	movl	%edx, (%rax)
.LBB1_51:                               # =>This Inner Loop Header: Depth=1
	leal	(%rsi,%rdx), %eax
	xorl	%edx, %eax
	cmpl	$16777216, %eax         # imm = 0x1000000
	jb	.LBB1_54
# BB#52:                                #   in Loop: Header=BB1_51 Depth=1
	cmpl	$32767, %esi            # imm = 0x7FFF
	ja	.LBB1_55
# BB#53:                                #   in Loop: Header=BB1_51 Depth=1
	negl	%edx
	andl	$32767, %edx            # imm = 0x7FFF
	movl	%edx, (%r13)
	jmp	.LBB1_54
.LBB1_55:                               # %RangeDec_Decode.exit206
	movq	56(%rsp), %rdx          # 8-byte Reload
	movzbl	2(%rdx), %ecx
	cmpl	$6, %ecx
	ja	.LBB1_58
# BB#56:
	decb	3(%rdx)
	jne	.LBB1_58
# BB#57:
	shlw	(%rdx)
	movl	%ecx, %eax
	incb	%al
	movb	%al, 2(%rdx)
	movl	$3, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movb	%al, 3(%rdx)
.LBB1_58:
	movq	%r14, 16(%rbx)
	movzbl	(%r14), %r14d
	movq	%rbx, %rdi
	callq	Ppmd8_Update2
	jmp	.LBB1_76
.LBB1_60:
	movl	$-2, %r14d
	jmp	.LBB1_76
.Lfunc_end1:
	.size	Ppmd8_DecodeSymbol, .Lfunc_end1-Ppmd8_DecodeSymbol
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
