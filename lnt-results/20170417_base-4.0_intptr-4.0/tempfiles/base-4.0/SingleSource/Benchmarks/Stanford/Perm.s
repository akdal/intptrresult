	.text
	.file	"Perm.bc"
	.globl	Initrand
	.p2align	4, 0x90
	.type	Initrand,@function
Initrand:                               # @Initrand
	.cfi_startproc
# BB#0:
	movq	$74755, seed(%rip)      # imm = 0x12403
	retq
.Lfunc_end0:
	.size	Initrand, .Lfunc_end0-Initrand
	.cfi_endproc

	.globl	Rand
	.p2align	4, 0x90
	.type	Rand,@function
Rand:                                   # @Rand
	.cfi_startproc
# BB#0:
	imull	$1309, seed(%rip), %eax # imm = 0x51D
	addl	$13849, %eax            # imm = 0x3619
	movzwl	%ax, %eax
	movq	%rax, seed(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end1:
	.size	Rand, .Lfunc_end1-Rand
	.cfi_endproc

	.globl	Swap
	.p2align	4, 0x90
	.type	Swap,@function
Swap:                                   # @Swap
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	movl	(%rsi), %ecx
	movl	%ecx, (%rdi)
	movl	%eax, (%rsi)
	retq
.Lfunc_end2:
	.size	Swap, .Lfunc_end2-Swap
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.text
	.globl	Initialize
	.p2align	4, 0x90
	.type	Initialize,@function
Initialize:                             # @Initialize
	.cfi_startproc
# BB#0:
	movaps	.LCPI3_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movups	%xmm0, permarray+4(%rip)
	movabsq	$21474836484, %rax      # imm = 0x500000004
	movq	%rax, permarray+20(%rip)
	movl	$6, permarray+28(%rip)
	retq
.Lfunc_end3:
	.size	Initialize, .Lfunc_end3-Initialize
	.cfi_endproc

	.globl	Permute
	.p2align	4, 0x90
	.type	Permute,@function
Permute:                                # @Permute
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	incl	pctr(%rip)
	cmpl	$1, %r14d
	je	.LBB4_4
# BB#1:
	leal	-1(%r14), %r15d
	movl	%r15d, %edi
	callq	Permute
	cmpl	$2, %r14d
	jl	.LBB4_4
# BB#2:                                 # %.lr.ph
	movslq	%r14d, %rbp
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB4_3:                                # =>This Inner Loop Header: Depth=1
	movl	permarray(,%rbp,4), %eax
	movl	permarray-4(,%rbx,4), %ecx
	movl	%ecx, permarray(,%rbp,4)
	movl	%eax, permarray-4(,%rbx,4)
	movl	%r15d, %edi
	callq	Permute
	movl	permarray(,%rbp,4), %eax
	movl	permarray-4(,%rbx,4), %ecx
	movl	%ecx, permarray(,%rbp,4)
	movl	%eax, permarray-4(,%rbx,4)
	decq	%rbx
	cmpq	$1, %rbx
	jg	.LBB4_3
.LBB4_4:                                # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	Permute, .Lfunc_end4-Permute
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI5_1:
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.text
	.globl	Perm
	.p2align	4, 0x90
	.type	Perm,@function
Perm:                                   # @Perm
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movl	$0, pctr(%rip)
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movups	%xmm0, permarray+4(%rip)
	movabsq	$21474836484, %rbx      # imm = 0x500000004
	movq	%rbx, permarray+20(%rip)
	movl	$6, permarray+28(%rip)
	movl	$7, %edi
	callq	Permute
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movups	%xmm0, permarray+4(%rip)
	movq	%rbx, permarray+20(%rip)
	movl	$6, permarray+28(%rip)
	movl	$7, %edi
	callq	Permute
	movl	$0, permarray+4(%rip)
	movl	$1, permarray+8(%rip)
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [2,3,4,5]
	movups	%xmm0, permarray+12(%rip)
	movl	$6, permarray+28(%rip)
	movl	$7, %edi
	callq	Permute
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movups	%xmm0, permarray+4(%rip)
	movq	%rbx, permarray+20(%rip)
	movl	$6, permarray+28(%rip)
	movl	$7, %edi
	callq	Permute
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movups	%xmm0, permarray+4(%rip)
	movq	%rbx, permarray+20(%rip)
	movl	$6, permarray+28(%rip)
	movl	$7, %edi
	callq	Permute
	movl	$43300, %esi            # imm = 0xA924
	cmpl	$43300, pctr(%rip)      # imm = 0xA924
	je	.LBB5_2
# BB#1:
	movl	$.Lstr, %edi
	callq	puts
	movl	pctr(%rip), %esi
.LBB5_2:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	popq	%rbx
	jmp	printf                  # TAILCALL
.Lfunc_end5:
	.size	Perm, .Lfunc_end5-Perm
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
.Lcfi12:
	.cfi_offset %rbx, -16
	movl	$100, %ebx
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	callq	Perm
	decl	%ebx
	jne	.LBB6_1
# BB#2:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	seed,@object            # @seed
	.comm	seed,8,8
	.type	permarray,@object       # @permarray
	.comm	permarray,44,16
	.type	pctr,@object            # @pctr
	.comm	pctr,4,4
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"%d\n"
	.size	.L.str.1, 4

	.type	value,@object           # @value
	.comm	value,4,4
	.type	fixed,@object           # @fixed
	.comm	fixed,4,4
	.type	floated,@object         # @floated
	.comm	floated,4,4
	.type	tree,@object            # @tree
	.comm	tree,8,8
	.type	stack,@object           # @stack
	.comm	stack,16,16
	.type	cellspace,@object       # @cellspace
	.comm	cellspace,152,16
	.type	freelist,@object        # @freelist
	.comm	freelist,4,4
	.type	movesdone,@object       # @movesdone
	.comm	movesdone,4,4
	.type	ima,@object             # @ima
	.comm	ima,6724,16
	.type	imb,@object             # @imb
	.comm	imb,6724,16
	.type	imr,@object             # @imr
	.comm	imr,6724,16
	.type	rma,@object             # @rma
	.comm	rma,6724,16
	.type	rmb,@object             # @rmb
	.comm	rmb,6724,16
	.type	rmr,@object             # @rmr
	.comm	rmr,6724,16
	.type	piececount,@object      # @piececount
	.comm	piececount,16,16
	.type	class,@object           # @class
	.comm	class,52,16
	.type	piecemax,@object        # @piecemax
	.comm	piecemax,52,16
	.type	puzzl,@object           # @puzzl
	.comm	puzzl,2048,16
	.type	p,@object               # @p
	.comm	p,26624,16
	.type	n,@object               # @n
	.comm	n,4,4
	.type	kount,@object           # @kount
	.comm	kount,4,4
	.type	sortlist,@object        # @sortlist
	.comm	sortlist,20004,16
	.type	biggest,@object         # @biggest
	.comm	biggest,4,4
	.type	littlest,@object        # @littlest
	.comm	littlest,4,4
	.type	top,@object             # @top
	.comm	top,4,4
	.type	z,@object               # @z
	.comm	z,2056,16
	.type	w,@object               # @w
	.comm	w,2056,16
	.type	e,@object               # @e
	.comm	e,1040,16
	.type	zr,@object              # @zr
	.comm	zr,4,4
	.type	zi,@object              # @zi
	.comm	zi,4,4
	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	" Error in Perm."
	.size	.Lstr, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
