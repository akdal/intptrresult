	.text
	.file	"assign.bc"
	.globl	AllocAssign
	.p2align	4, 0x90
	.type	AllocAssign,@function
AllocAssign:                            # @AllocAssign
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	channelNets(%rip), %r13
	leaq	8(,%r13,8), %r14
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, %r15
	movq	%r15, costMatrix(%rip)
	movq	channelTracks(%rip), %rax
	leaq	16(,%rax,8), %r12
	movq	%r12, %rdi
	callq	malloc
	testq	%r13, %r13
	je	.LBB0_1
# BB#2:                                 # %.lr.ph
	movq	%rax, 8(%r15)
	movq	%r12, %rdi
	callq	malloc
	cmpq	$1, %r13
	je	.LBB0_5
# BB#3:                                 # %._crit_edge6.preheader
	movl	$2, %ebx
	.p2align	4, 0x90
.LBB0_4:                                # %._crit_edge6
                                        # =>This Inner Loop Header: Depth=1
	movq	costMatrix(%rip), %rcx
	movq	%rax, (%rcx,%rbx,8)
	incq	%rbx
	movq	%r12, %rdi
	callq	malloc
	cmpq	%r13, %rbx
	jbe	.LBB0_4
	jmp	.LBB0_5
.LBB0_1:
	movl	$8, %r14d
.LBB0_5:                                # %._crit_edge
	movq	%rax, tracksNotPref(%rip)
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, tracksTopNotPref(%rip)
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, tracksBotNotPref(%rip)
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, tracksNoHCV(%rip)
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, tracksAssign(%rip)
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, netsAssign(%rip)
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, netsAssignCopy(%rip)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	AllocAssign, .Lfunc_end0-AllocAssign
	.cfi_endproc

	.globl	FreeAssign
	.p2align	4, 0x90
	.type	FreeAssign,@function
FreeAssign:                             # @FreeAssign
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	cmpq	$0, channelNets(%rip)
	movq	costMatrix(%rip), %rdi
	je	.LBB1_3
# BB#1:                                 # %.lr.ph.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rdi
	callq	free
	incq	%rbx
	movq	costMatrix(%rip), %rdi
	cmpq	channelNets(%rip), %rbx
	jbe	.LBB1_2
.LBB1_3:                                # %._crit_edge
	callq	free
	movq	tracksNotPref(%rip), %rdi
	callq	free
	movq	tracksTopNotPref(%rip), %rdi
	callq	free
	movq	tracksBotNotPref(%rip), %rdi
	callq	free
	movq	tracksNoHCV(%rip), %rdi
	callq	free
	movq	tracksAssign(%rip), %rdi
	callq	free
	movq	netsAssign(%rip), %rdi
	callq	free
	movq	netsAssignCopy(%rip), %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end1:
	.size	FreeAssign, .Lfunc_end1-FreeAssign
	.cfi_endproc

	.globl	NetsAssign
	.p2align	4, 0x90
	.type	NetsAssign,@function
NetsAssign:                             # @NetsAssign
	.cfi_startproc
# BB#0:
	cmpq	$0, channelNets(%rip)
	je	.LBB2_3
# BB#1:                                 # %.lr.ph
	movq	netsAssign(%rip), %rax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movq	$0, (%rax,%rcx,8)
	incq	%rcx
	cmpq	channelNets(%rip), %rcx
	jbe	.LBB2_2
.LBB2_3:                                # %._crit_edge
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 16
	callq	MaxNetsAssign
	callq	LeftNetsAssign
	popq	%rax
	jmp	RightNetsAssign         # TAILCALL
.Lfunc_end2:
	.size	NetsAssign, .Lfunc_end2-NetsAssign
	.cfi_endproc

	.globl	MaxNetsAssign
	.p2align	4, 0x90
	.type	MaxNetsAssign,@function
MaxNetsAssign:                          # @MaxNetsAssign
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r12, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	cmpq	$0, channelNets(%rip)
	je	.LBB3_22
# BB#1:                                 # %.lr.ph21
	movq	FIRST(%rip), %rax
	movq	CROSSING(%rip), %rcx
	movq	LAST(%rip), %rdx
	movl	$1, %esi
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movq	channelDensityColumn(%rip), %rdi
	cmpq	%rdi, (%rax,%rsi,8)
	ja	.LBB3_8
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	cmpq	%rdi, (%rdx,%rsi,8)
	jae	.LBB3_4
.LBB3_8:                                #   in Loop: Header=BB3_2 Depth=1
	movq	$0, (%rcx,%rsi,8)
	jmp	.LBB3_9
	.p2align	4, 0x90
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	movq	$1, (%rcx,%rsi,8)
	incq	%r15
.LBB3_9:                                #   in Loop: Header=BB3_2 Depth=1
	incq	%rsi
	cmpq	channelNets(%rip), %rsi
	jbe	.LBB3_2
# BB#5:                                 # %.preheader
	testq	%r15, %r15
	je	.LBB3_22
	.p2align	4, 0x90
.LBB3_6:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_12 Depth 2
                                        #       Child Loop BB3_14 Depth 3
                                        #     Child Loop BB3_18 Depth 2
	movq	VCG(%rip), %rdi
	movq	HCG(%rip), %rsi
	movq	netsAssign(%rip), %rdx
	movq	CROSSING(%rip), %r14
	movq	%r14, %rcx
	callq	BuildCostMatrix
	movq	channelNets(%rip), %r9
	testq	%r9, %r9
	je	.LBB3_7
# BB#10:                                # %.lr.ph42.i
                                        #   in Loop: Header=BB3_6 Depth=1
	movq	channelTracks(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB3_17
# BB#11:                                # %.lr.ph42.split.i.preheader
                                        #   in Loop: Header=BB3_6 Depth=1
	movq	costMatrix(%rip), %r8
	movq	$-1, %rsi
	movl	$1, %edi
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_12:                               # %.lr.ph42.split.i
                                        #   Parent Loop BB3_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_14 Depth 3
	cmpq	$0, (%r14,%rdi,8)
	je	.LBB3_16
# BB#13:                                # %.lr.ph.i
                                        #   in Loop: Header=BB3_12 Depth=2
	movq	(%r8,%rdi,8), %rax
	xorl	%edx, %edx
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB3_14:                               #   Parent Loop BB3_6 Depth=1
                                        #     Parent Loop BB3_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addq	(%rax,%rbx,8), %rdx
	incq	%rbx
	cmpq	%rcx, %rbx
	jbe	.LBB3_14
# BB#15:                                # %._crit_edge.i
                                        #   in Loop: Header=BB3_12 Depth=2
	cmpq	%rsi, %rdx
	cmovgq	%rdi, %r12
	cmovgeq	%rdx, %rsi
.LBB3_16:                               #   in Loop: Header=BB3_12 Depth=2
	incq	%rdi
	cmpq	%r9, %rdi
	jbe	.LBB3_12
	jmp	.LBB3_21
	.p2align	4, 0x90
.LBB3_7:                                #   in Loop: Header=BB3_6 Depth=1
	xorl	%r12d, %r12d
	jmp	.LBB3_21
	.p2align	4, 0x90
.LBB3_17:                               # %.lr.ph42.split.us.i.preheader
                                        #   in Loop: Header=BB3_6 Depth=1
	movq	$-1, %rcx
	movl	$1, %edx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_18:                               # %.lr.ph42.split.us.i
                                        #   Parent Loop BB3_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$0, (%r14,%rdx,8)
	je	.LBB3_20
# BB#19:                                #   in Loop: Header=BB3_18 Depth=2
	testq	%rcx, %rcx
	cmovsq	%rdx, %r12
	movl	$0, %eax
	cmovsq	%rax, %rcx
.LBB3_20:                               #   in Loop: Header=BB3_18 Depth=2
	incq	%rdx
	cmpq	%r9, %rdx
	jbe	.LBB3_18
.LBB3_21:                               # %Select.exit
                                        #   in Loop: Header=BB3_6 Depth=1
	movq	VCG(%rip), %rdi
	movq	netsAssign(%rip), %rsi
	movq	%r12, %rdx
	callq	Assign
	movq	CROSSING(%rip), %rax
	movq	$0, (%rax,%r12,8)
	decq	%r15
	jne	.LBB3_6
.LBB3_22:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	MaxNetsAssign, .Lfunc_end3-MaxNetsAssign
	.cfi_endproc

	.globl	LeftNetsAssign
	.p2align	4, 0x90
	.type	LeftNetsAssign,@function
LeftNetsAssign:                         # @LeftNetsAssign
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 48
.Lcfi27:
	.cfi_offset %rbx, -48
.Lcfi28:
	.cfi_offset %r12, -40
.Lcfi29:
	.cfi_offset %r13, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	cmpq	$0, channelNets(%rip)
	je	.LBB4_3
# BB#1:                                 # %.lr.ph50
	movq	CROSSING(%rip), %rax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	$0, (%rax,%rcx,8)
	incq	%rcx
	cmpq	channelNets(%rip), %rcx
	jbe	.LBB4_2
.LBB4_3:                                # %._crit_edge51
	movq	channelDensityColumn(%rip), %r15
	decq	%r15
	je	.LBB4_24
# BB#4:                                 # %.lr.ph.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_27 Depth 2
                                        #       Child Loop BB4_29 Depth 3
                                        #     Child Loop BB4_33 Depth 2
	movq	TOP(%rip), %rax
	movq	(%rax,%r15,8), %rcx
	movq	BOT(%rip), %rax
	movq	(%rax,%r15,8), %rax
	cmpq	%rax, %rcx
	jne	.LBB4_6
# BB#12:                                #   in Loop: Header=BB4_5 Depth=1
	testq	%rcx, %rcx
	je	.LBB4_16
# BB#13:                                #   in Loop: Header=BB4_5 Depth=1
	movq	LAST(%rip), %rdx
	cmpq	%r15, (%rdx,%rcx,8)
	jne	.LBB4_16
# BB#14:                                #   in Loop: Header=BB4_5 Depth=1
	movq	CROSSING(%rip), %rdx
	movq	$1, (%rdx,%rcx,8)
	jmp	.LBB4_15
	.p2align	4, 0x90
.LBB4_6:                                #   in Loop: Header=BB4_5 Depth=1
	testq	%rcx, %rcx
	je	.LBB4_9
# BB#7:                                 #   in Loop: Header=BB4_5 Depth=1
	movq	LAST(%rip), %rdx
	cmpq	%r15, (%rdx,%rcx,8)
	jne	.LBB4_9
# BB#8:                                 #   in Loop: Header=BB4_5 Depth=1
	movq	CROSSING(%rip), %rdx
	movq	$1, (%rdx,%rcx,8)
	incq	%r12
.LBB4_9:                                #   in Loop: Header=BB4_5 Depth=1
	testq	%rax, %rax
	je	.LBB4_16
# BB#10:                                #   in Loop: Header=BB4_5 Depth=1
	movq	LAST(%rip), %rdx
	cmpq	%r15, (%rdx,%rax,8)
	jne	.LBB4_16
# BB#11:                                #   in Loop: Header=BB4_5 Depth=1
	movq	CROSSING(%rip), %rdx
	movq	$1, (%rdx,%rax,8)
.LBB4_15:                               #   in Loop: Header=BB4_5 Depth=1
	incq	%r12
.LBB4_16:                               #   in Loop: Header=BB4_5 Depth=1
	movq	FIRST(%rip), %rdx
	cmpq	%r15, (%rdx,%rcx,8)
	je	.LBB4_21
# BB#17:                                #   in Loop: Header=BB4_5 Depth=1
	testq	%r12, %r12
	je	.LBB4_23
# BB#18:                                #   in Loop: Header=BB4_5 Depth=1
	cmpq	%r15, (%rdx,%rax,8)
	jne	.LBB4_23
	.p2align	4, 0x90
.LBB4_19:                               #   in Loop: Header=BB4_5 Depth=1
	movq	VCG(%rip), %rdi
	movq	HCG(%rip), %rsi
	movq	netsAssign(%rip), %rdx
	movq	CROSSING(%rip), %r14
	movq	%r14, %rcx
	callq	BuildCostMatrix
	movq	channelNets(%rip), %r9
	testq	%r9, %r9
	je	.LBB4_20
# BB#25:                                # %.lr.ph42.i
                                        #   in Loop: Header=BB4_5 Depth=1
	movq	channelTracks(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB4_32
# BB#26:                                # %.lr.ph42.split.i.preheader
                                        #   in Loop: Header=BB4_5 Depth=1
	movq	costMatrix(%rip), %r8
	movq	$-1, %rsi
	movl	$1, %edi
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_27:                               # %.lr.ph42.split.i
                                        #   Parent Loop BB4_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_29 Depth 3
	cmpq	$0, (%r14,%rdi,8)
	je	.LBB4_31
# BB#28:                                # %.lr.ph.i
                                        #   in Loop: Header=BB4_27 Depth=2
	movq	(%r8,%rdi,8), %rax
	xorl	%edx, %edx
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB4_29:                               #   Parent Loop BB4_5 Depth=1
                                        #     Parent Loop BB4_27 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addq	(%rax,%rbx,8), %rdx
	incq	%rbx
	cmpq	%rcx, %rbx
	jbe	.LBB4_29
# BB#30:                                # %._crit_edge.i
                                        #   in Loop: Header=BB4_27 Depth=2
	cmpq	%rsi, %rdx
	cmovgq	%rdi, %r13
	cmovgeq	%rdx, %rsi
.LBB4_31:                               #   in Loop: Header=BB4_27 Depth=2
	incq	%rdi
	cmpq	%r9, %rdi
	jbe	.LBB4_27
	jmp	.LBB4_36
	.p2align	4, 0x90
.LBB4_20:                               #   in Loop: Header=BB4_5 Depth=1
	xorl	%r13d, %r13d
	jmp	.LBB4_36
	.p2align	4, 0x90
.LBB4_32:                               # %.lr.ph42.split.us.i.preheader
                                        #   in Loop: Header=BB4_5 Depth=1
	movq	$-1, %rcx
	movl	$1, %edx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_33:                               # %.lr.ph42.split.us.i
                                        #   Parent Loop BB4_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$0, (%r14,%rdx,8)
	je	.LBB4_35
# BB#34:                                #   in Loop: Header=BB4_33 Depth=2
	testq	%rcx, %rcx
	cmovsq	%rdx, %r13
	movl	$0, %eax
	cmovsq	%rax, %rcx
.LBB4_35:                               #   in Loop: Header=BB4_33 Depth=2
	incq	%rdx
	cmpq	%r9, %rdx
	jbe	.LBB4_33
.LBB4_36:                               # %Select.exit
                                        #   in Loop: Header=BB4_5 Depth=1
	movq	VCG(%rip), %rdi
	movq	netsAssign(%rip), %rsi
	movq	%r13, %rdx
	callq	Assign
	movq	CROSSING(%rip), %rax
	movq	$0, (%rax,%r13,8)
	decq	%r12
.LBB4_21:                               #   in Loop: Header=BB4_5 Depth=1
	testq	%r12, %r12
	jne	.LBB4_19
# BB#22:                                #   in Loop: Header=BB4_5 Depth=1
	xorl	%r12d, %r12d
.LBB4_23:                               # %.backedge
                                        #   in Loop: Header=BB4_5 Depth=1
	decq	%r15
	jne	.LBB4_5
.LBB4_24:                               # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	LeftNetsAssign, .Lfunc_end4-LeftNetsAssign
	.cfi_endproc

	.globl	RightNetsAssign
	.p2align	4, 0x90
	.type	RightNetsAssign,@function
RightNetsAssign:                        # @RightNetsAssign
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 48
.Lcfi37:
	.cfi_offset %rbx, -48
.Lcfi38:
	.cfi_offset %r12, -40
.Lcfi39:
	.cfi_offset %r13, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %r15, -16
	cmpq	$0, channelNets(%rip)
	je	.LBB5_3
# BB#1:                                 # %.lr.ph50
	movq	CROSSING(%rip), %rax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movq	$0, (%rax,%rcx,8)
	incq	%rcx
	cmpq	channelNets(%rip), %rcx
	jbe	.LBB5_2
.LBB5_3:                                # %._crit_edge51
	movq	channelDensityColumn(%rip), %r15
	incq	%r15
	cmpq	channelColumns(%rip), %r15
	ja	.LBB5_24
# BB#4:                                 # %.lr.ph.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_27 Depth 2
                                        #       Child Loop BB5_29 Depth 3
                                        #     Child Loop BB5_33 Depth 2
	movq	TOP(%rip), %rax
	movq	(%rax,%r15,8), %rcx
	movq	BOT(%rip), %rax
	movq	(%rax,%r15,8), %rax
	cmpq	%rax, %rcx
	jne	.LBB5_6
# BB#12:                                #   in Loop: Header=BB5_5 Depth=1
	testq	%rcx, %rcx
	je	.LBB5_16
# BB#13:                                #   in Loop: Header=BB5_5 Depth=1
	movq	FIRST(%rip), %rdx
	cmpq	%r15, (%rdx,%rcx,8)
	jne	.LBB5_16
# BB#14:                                #   in Loop: Header=BB5_5 Depth=1
	movq	CROSSING(%rip), %rdx
	movq	$1, (%rdx,%rcx,8)
	jmp	.LBB5_15
	.p2align	4, 0x90
.LBB5_6:                                #   in Loop: Header=BB5_5 Depth=1
	testq	%rcx, %rcx
	je	.LBB5_9
# BB#7:                                 #   in Loop: Header=BB5_5 Depth=1
	movq	FIRST(%rip), %rdx
	cmpq	%r15, (%rdx,%rcx,8)
	jne	.LBB5_9
# BB#8:                                 #   in Loop: Header=BB5_5 Depth=1
	movq	CROSSING(%rip), %rdx
	movq	$1, (%rdx,%rcx,8)
	incq	%r12
.LBB5_9:                                #   in Loop: Header=BB5_5 Depth=1
	testq	%rax, %rax
	je	.LBB5_16
# BB#10:                                #   in Loop: Header=BB5_5 Depth=1
	movq	FIRST(%rip), %rdx
	cmpq	%r15, (%rdx,%rax,8)
	jne	.LBB5_16
# BB#11:                                #   in Loop: Header=BB5_5 Depth=1
	movq	CROSSING(%rip), %rdx
	movq	$1, (%rdx,%rax,8)
.LBB5_15:                               #   in Loop: Header=BB5_5 Depth=1
	incq	%r12
.LBB5_16:                               #   in Loop: Header=BB5_5 Depth=1
	movq	LAST(%rip), %rdx
	cmpq	%r15, (%rdx,%rcx,8)
	je	.LBB5_21
# BB#17:                                #   in Loop: Header=BB5_5 Depth=1
	testq	%r12, %r12
	je	.LBB5_23
# BB#18:                                #   in Loop: Header=BB5_5 Depth=1
	cmpq	%r15, (%rdx,%rax,8)
	jne	.LBB5_23
	.p2align	4, 0x90
.LBB5_19:                               #   in Loop: Header=BB5_5 Depth=1
	movq	VCG(%rip), %rdi
	movq	HCG(%rip), %rsi
	movq	netsAssign(%rip), %rdx
	movq	CROSSING(%rip), %r14
	movq	%r14, %rcx
	callq	BuildCostMatrix
	movq	channelNets(%rip), %r9
	testq	%r9, %r9
	je	.LBB5_20
# BB#25:                                # %.lr.ph42.i
                                        #   in Loop: Header=BB5_5 Depth=1
	movq	channelTracks(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB5_32
# BB#26:                                # %.lr.ph42.split.i.preheader
                                        #   in Loop: Header=BB5_5 Depth=1
	movq	costMatrix(%rip), %r8
	movq	$-1, %rsi
	movl	$1, %edi
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_27:                               # %.lr.ph42.split.i
                                        #   Parent Loop BB5_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_29 Depth 3
	cmpq	$0, (%r14,%rdi,8)
	je	.LBB5_31
# BB#28:                                # %.lr.ph.i
                                        #   in Loop: Header=BB5_27 Depth=2
	movq	(%r8,%rdi,8), %rax
	xorl	%edx, %edx
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB5_29:                               #   Parent Loop BB5_5 Depth=1
                                        #     Parent Loop BB5_27 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addq	(%rax,%rbx,8), %rdx
	incq	%rbx
	cmpq	%rcx, %rbx
	jbe	.LBB5_29
# BB#30:                                # %._crit_edge.i
                                        #   in Loop: Header=BB5_27 Depth=2
	cmpq	%rsi, %rdx
	cmovgq	%rdi, %r13
	cmovgeq	%rdx, %rsi
.LBB5_31:                               #   in Loop: Header=BB5_27 Depth=2
	incq	%rdi
	cmpq	%r9, %rdi
	jbe	.LBB5_27
	jmp	.LBB5_36
	.p2align	4, 0x90
.LBB5_20:                               #   in Loop: Header=BB5_5 Depth=1
	xorl	%r13d, %r13d
	jmp	.LBB5_36
	.p2align	4, 0x90
.LBB5_32:                               # %.lr.ph42.split.us.i.preheader
                                        #   in Loop: Header=BB5_5 Depth=1
	movq	$-1, %rcx
	movl	$1, %edx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_33:                               # %.lr.ph42.split.us.i
                                        #   Parent Loop BB5_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$0, (%r14,%rdx,8)
	je	.LBB5_35
# BB#34:                                #   in Loop: Header=BB5_33 Depth=2
	testq	%rcx, %rcx
	cmovsq	%rdx, %r13
	movl	$0, %eax
	cmovsq	%rax, %rcx
.LBB5_35:                               #   in Loop: Header=BB5_33 Depth=2
	incq	%rdx
	cmpq	%r9, %rdx
	jbe	.LBB5_33
.LBB5_36:                               # %Select.exit
                                        #   in Loop: Header=BB5_5 Depth=1
	movq	VCG(%rip), %rdi
	movq	netsAssign(%rip), %rsi
	movq	%r13, %rdx
	callq	Assign
	movq	CROSSING(%rip), %rax
	movq	$0, (%rax,%r13,8)
	decq	%r12
.LBB5_21:                               #   in Loop: Header=BB5_5 Depth=1
	testq	%r12, %r12
	jne	.LBB5_19
# BB#22:                                #   in Loop: Header=BB5_5 Depth=1
	xorl	%r12d, %r12d
.LBB5_23:                               # %.backedge
                                        #   in Loop: Header=BB5_5 Depth=1
	incq	%r15
	cmpq	channelColumns(%rip), %r15
	jbe	.LBB5_5
.LBB5_24:                               # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	RightNetsAssign, .Lfunc_end5-RightNetsAssign
	.cfi_endproc

	.globl	Select
	.p2align	4, 0x90
	.type	Select,@function
Select:                                 # @Select
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 32
.Lcfi45:
	.cfi_offset %rbx, -32
.Lcfi46:
	.cfi_offset %r14, -24
.Lcfi47:
	.cfi_offset %r15, -16
	movq	%r8, %r15
	movq	%rcx, %r14
	movq	%r15, %rcx
	callq	BuildCostMatrix
	movq	channelNets(%rip), %r9
	testq	%r9, %r9
	je	.LBB6_1
# BB#2:                                 # %.lr.ph42
	movq	channelTracks(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB6_9
# BB#3:                                 # %.lr.ph42.split.preheader
	movq	costMatrix(%rip), %r8
	movq	$-1, %rdi
	xorl	%r10d, %r10d
	movl	$1, %esi
	.p2align	4, 0x90
.LBB6_4:                                # %.lr.ph42.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_6 Depth 2
	cmpq	$0, (%r15,%rsi,8)
	je	.LBB6_8
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB6_4 Depth=1
	movq	(%r8,%rsi,8), %rax
	xorl	%ebx, %ebx
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB6_6:                                #   Parent Loop BB6_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	(%rax,%rcx,8), %rbx
	incq	%rcx
	cmpq	%rdx, %rcx
	jbe	.LBB6_6
# BB#7:                                 # %._crit_edge
                                        #   in Loop: Header=BB6_4 Depth=1
	cmpq	%rdi, %rbx
	cmovgq	%rsi, %r10
	cmovgeq	%rbx, %rdi
.LBB6_8:                                #   in Loop: Header=BB6_4 Depth=1
	incq	%rsi
	cmpq	%r9, %rsi
	jbe	.LBB6_4
	jmp	.LBB6_13
.LBB6_1:
	xorl	%r10d, %r10d
	jmp	.LBB6_13
.LBB6_9:                                # %.lr.ph42.split.us.preheader
	movq	$-1, %rdx
	xorl	%r10d, %r10d
	movl	$1, %esi
	.p2align	4, 0x90
.LBB6_10:                               # %.lr.ph42.split.us
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%r15,%rsi,8)
	je	.LBB6_12
# BB#11:                                #   in Loop: Header=BB6_10 Depth=1
	testq	%rdx, %rdx
	cmovsq	%rsi, %r10
	movl	$0, %eax
	cmovsq	%rax, %rdx
.LBB6_12:                               #   in Loop: Header=BB6_10 Depth=1
	incq	%rsi
	cmpq	%r9, %rsi
	jbe	.LBB6_10
.LBB6_13:                               # %._crit_edge43
	movq	%r10, (%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	Select, .Lfunc_end6-Select
	.cfi_endproc

	.globl	Assign
	.p2align	4, 0x90
	.type	Assign,@function
Assign:                                 # @Assign
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi54:
	.cfi_def_cfa_offset 80
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %rsi
	callq	LongestPathVCG
	movq	HCG(%rip), %rdi
	movq	tracksNoHCV(%rip), %rcx
	movq	%rbx, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	NoHCV
	movq	cardTopNotPref(%rip), %rax
	movq	cardBotNotPref(%rip), %rdx
	movq	%rdx, %rsi
	addq	%rax, %rsi
	movq	channelTracks(%rip), %rcx
	je	.LBB7_2
# BB#1:
	movq	%rcx, %rdi
	subq	%rdx, %rdi
	imulq	%rax, %rdi
	incq	%rax
	imulq	%rdx, %rax
	addq	%rdi, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	%rax, %r13
	testq	%rcx, %rcx
	jne	.LBB7_3
	jmp	.LBB7_28
.LBB7_2:
	movl	$1, %r13d
	testq	%rcx, %rcx
	je	.LBB7_28
.LBB7_3:                                # %.lr.ph108
	movq	tracksAssign(%rip), %rcx
	movl	$1, %edx
	.p2align	4, 0x90
.LBB7_4:                                # =>This Inner Loop Header: Depth=1
	movq	$0, (%rcx,%rdx,8)
	incq	%rdx
	movq	channelTracks(%rip), %rax
	cmpq	%rax, %rdx
	jbe	.LBB7_4
# BB#5:                                 # %.preheader88
	testq	%rax, %rax
	je	.LBB7_28
# BB#6:                                 # %.lr.ph104
	movq	tracksNoHCV(%rip), %rcx
	movq	tracksNotPref(%rip), %rdx
	movq	tracksAssign(%rip), %rdi
	xorl	%esi, %esi
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB7_7:                                # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rcx,%rbp,8)
	je	.LBB7_10
# BB#8:                                 #   in Loop: Header=BB7_7 Depth=1
	cmpq	$0, (%rdx,%rbp,8)
	jne	.LBB7_10
# BB#9:                                 #   in Loop: Header=BB7_7 Depth=1
	movq	$1, (%rdi,%rbp,8)
	incq	%rsi
	movq	channelTracks(%rip), %rax
.LBB7_10:                               #   in Loop: Header=BB7_7 Depth=1
	incq	%rbp
	cmpq	%rax, %rbp
	jbe	.LBB7_7
# BB#11:                                # %._crit_edge105
	testq	%rsi, %rsi
	jne	.LBB7_33
# BB#12:                                # %.preheader87
	testq	%rax, %rax
	je	.LBB7_28
# BB#13:                                # %.lr.ph99
	movq	tracksNoHCV(%rip), %rcx
	movq	tracksTopNotPref(%rip), %rdx
	movq	tracksBotNotPref(%rip), %rsi
	movq	tracksAssign(%rip), %rbp
	xorl	%edi, %edi
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB7_14:                               # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rcx,%rbx,8)
	je	.LBB7_18
# BB#15:                                #   in Loop: Header=BB7_14 Depth=1
	cmpq	$0, (%rdx,%rbx,8)
	je	.LBB7_18
# BB#16:                                #   in Loop: Header=BB7_14 Depth=1
	cmpq	$0, (%rsi,%rbx,8)
	je	.LBB7_18
# BB#17:                                #   in Loop: Header=BB7_14 Depth=1
	movq	$1, (%rbp,%rbx,8)
	incq	%rdi
	movq	channelTracks(%rip), %rax
.LBB7_18:                               #   in Loop: Header=BB7_14 Depth=1
	incq	%rbx
	cmpq	%rax, %rbx
	jbe	.LBB7_14
# BB#19:                                # %._crit_edge100
	testq	%rdi, %rdi
	jne	.LBB7_33
# BB#20:                                # %.preheader
	cmpq	$3, %rax
	jb	.LBB7_29
# BB#21:                                # %.lr.ph95
	movq	tracksNoHCV(%rip), %rcx
	movq	tracksNotPref(%rip), %rdx
	movq	tracksAssign(%rip), %rdi
	xorl	%esi, %esi
	movl	$2, %ebp
	.p2align	4, 0x90
.LBB7_22:                               # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rcx,%rbp,8)
	je	.LBB7_25
# BB#23:                                #   in Loop: Header=BB7_22 Depth=1
	cmpq	$0, (%rdx,%rbp,8)
	je	.LBB7_25
# BB#24:                                #   in Loop: Header=BB7_22 Depth=1
	movq	$1, (%rdi,%rbp,8)
	incq	%rsi
	movq	channelTracks(%rip), %rax
.LBB7_25:                               #   in Loop: Header=BB7_22 Depth=1
	incq	%rbp
	cmpq	%rax, %rbp
	jb	.LBB7_22
# BB#26:                                # %._crit_edge
	testq	%rsi, %rsi
	jne	.LBB7_33
	jmp	.LBB7_29
.LBB7_28:
	xorl	%eax, %eax
.LBB7_29:                               # %._crit_edge.thread
	movq	tracksNoHCV(%rip), %rcx
	cmpq	$0, 8(%rcx)
	je	.LBB7_31
# BB#30:
	movq	tracksAssign(%rip), %rdx
	movq	$1, 8(%rdx)
.LBB7_31:
	cmpq	$0, (%rcx,%rax,8)
	je	.LBB7_33
# BB#32:
	movq	tracksAssign(%rip), %rcx
	movq	$1, (%rcx,%rax,8)
	movq	channelTracks(%rip), %rax
.LBB7_33:                               # %.thread86.preheader
	testq	%rax, %rax
	movq	%r14, 8(%rsp)           # 8-byte Spill
	je	.LBB7_41
# BB#34:                                # %.lr.ph.preheader
	movq	%r13, %r15
	negq	%r15
	decq	%r13
	xorl	%r14d, %r14d
	movl	$1, %ebx
	movl	$1000000, %r12d         # imm = 0xF4240
                                        # implicit-def: %RBP
	.p2align	4, 0x90
.LBB7_35:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	tracksAssign(%rip), %rax
	cmpq	$0, (%rax,%rbx,8)
	je	.LBB7_40
# BB#36:                                #   in Loop: Header=BB7_35 Depth=1
	movq	netsAssign(%rip), %rcx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%rbx, %rdx
	callq	VCV
	cmpq	%r12, %rax
	jae	.LBB7_38
# BB#37:                                #   in Loop: Header=BB7_35 Depth=1
	movq	%r15, %rbp
	addq	%rbx, %rbp
	cmovsq	%r13, %rbp
	movq	%rbx, %r14
	movq	%rax, %r12
	jmp	.LBB7_40
	.p2align	4, 0x90
.LBB7_38:                               #   in Loop: Header=BB7_35 Depth=1
	jne	.LBB7_40
# BB#39:                                #   in Loop: Header=BB7_35 Depth=1
	movq	%r15, %rax
	addq	%rbx, %rax
	cmovsq	%r13, %rax
	cmpq	%rbp, %rax
	cmovlq	%rbx, %r14
	cmovleq	%rax, %rbp
	.p2align	4, 0x90
.LBB7_40:                               # %.thread86
                                        #   in Loop: Header=BB7_35 Depth=1
	incq	%rbx
	decq	%r13
	cmpq	channelTracks(%rip), %rbx
	jbe	.LBB7_35
	jmp	.LBB7_42
.LBB7_41:
	xorl	%r14d, %r14d
.LBB7_42:                               # %.thread86._crit_edge
	movq	(%rsp), %rax            # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%r14, (%rcx,%rax,8)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	Assign, .Lfunc_end7-Assign
	.cfi_endproc

	.globl	IdealTrack
	.p2align	4, 0x90
	.type	IdealTrack,@function
IdealTrack:                             # @IdealTrack
	.cfi_startproc
# BB#0:
	movq	%rdx, %r8
	addq	%rsi, %r8
	je	.LBB8_1
# BB#2:
	subq	%rdx, %rdi
	imulq	%rsi, %rdi
	incq	%rsi
	imulq	%rdx, %rsi
	addq	%rdi, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r8
	movq	%rax, (%rcx)
	retq
.LBB8_1:
	movl	$1, %eax
	movq	%rax, (%rcx)
	retq
.Lfunc_end8:
	.size	IdealTrack, .Lfunc_end8-IdealTrack
	.cfi_endproc

	.globl	BuildCostMatrix
	.p2align	4, 0x90
	.type	BuildCostMatrix,@function
BuildCostMatrix:                        # @BuildCostMatrix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi64:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi65:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi67:
	.cfi_def_cfa_offset 80
.Lcfi68:
	.cfi_offset %rbx, -56
.Lcfi69:
	.cfi_offset %r12, -48
.Lcfi70:
	.cfi_offset %r13, -40
.Lcfi71:
	.cfi_offset %r14, -32
.Lcfi72:
	.cfi_offset %r15, -24
.Lcfi73:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r12
	movq	channelNets(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB9_29
# BB#1:                                 # %.lr.ph73
	movq	costMatrix(%rip), %rax
	movq	channelTracks(%rip), %rsi
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB9_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_10 Depth 2
	testq	%rsi, %rsi
	je	.LBB9_3
# BB#9:                                 # %.lr.ph69.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	(%rax,%rcx,8), %rdx
	movl	$1, %edi
	.p2align	4, 0x90
.LBB9_10:                               # %.lr.ph69
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	$0, (%rdx,%rdi,8)
	incq	%rdi
	movq	channelTracks(%rip), %rsi
	cmpq	%rsi, %rdi
	jbe	.LBB9_10
# BB#11:                                # %._crit_edge70.loopexit
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	channelNets(%rip), %rdx
	jmp	.LBB9_12
	.p2align	4, 0x90
.LBB9_3:                                #   in Loop: Header=BB9_2 Depth=1
	xorl	%esi, %esi
.LBB9_12:                               # %._crit_edge70
                                        #   in Loop: Header=BB9_2 Depth=1
	incq	%rcx
	cmpq	%rdx, %rcx
	jbe	.LBB9_2
# BB#4:                                 # %.preheader
	testq	%rdx, %rdx
	je	.LBB9_29
# BB#5:                                 # %.lr.ph66.preheader
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB9_6:                                # %.lr.ph66
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_16 Depth 2
	cmpq	$0, (%r13,%r14,8)
	je	.LBB9_28
# BB#7:                                 #   in Loop: Header=BB9_6 Depth=1
	movq	costMatrix(%rip), %rax
	movq	(%rax,%r14,8), %r15
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	LongestPathVCG
	movq	tracksNoHCV(%rip), %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	NoHCV
	movq	cardTopNotPref(%rip), %rax
	movq	cardBotNotPref(%rip), %rdx
	movq	%rdx, %rcx
	addq	%rax, %rcx
	movq	channelTracks(%rip), %rbp
	je	.LBB9_8
# BB#13:                                #   in Loop: Header=BB9_6 Depth=1
	movq	%rbp, %rsi
	subq	%rdx, %rsi
	imulq	%rax, %rsi
	incq	%rax
	imulq	%rdx, %rax
	addq	%rsi, %rax
	xorl	%edx, %edx
	divq	%rcx
	testq	%rbp, %rbp
	jne	.LBB9_15
	jmp	.LBB9_28
	.p2align	4, 0x90
.LBB9_8:                                #   in Loop: Header=BB9_6 Depth=1
	movl	$1, %eax
	testq	%rbp, %rbp
	je	.LBB9_28
.LBB9_15:                               # %.lr.ph
                                        #   in Loop: Header=BB9_6 Depth=1
	movq	tracksNoHCV(%rip), %r9
	movq	tracksNotPref(%rip), %r8
	leaq	-1(%rax), %rsi
	negq	%rax
	movl	$8, %edi
	movl	$1, %edx
	jmp	.LBB9_16
.LBB9_23:                               # %select.unfold
                                        #   in Loop: Header=BB9_16 Depth=2
	imulq	$100, %rcx, %rcx
	movq	%rcx, (%r15,%rdx,8)
	cmpq	$999999, %rcx           # imm = 0xF423F
	jg	.LBB9_27
# BB#24:                                #   in Loop: Header=BB9_16 Depth=2
	leaq	(%r15,%rdi), %rbx
	jmp	.LBB9_25
	.p2align	4, 0x90
.LBB9_16:                               #   Parent Loop BB9_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$0, (%r9,%rdx,8)
	je	.LBB9_26
# BB#17:                                #   in Loop: Header=BB9_16 Depth=2
	movq	cardNotPref(%rip), %rcx
	cmpq	%rbp, %rcx
	jne	.LBB9_18
# BB#20:                                #   in Loop: Header=BB9_16 Depth=2
	movq	cardBotNotPref(%rip), %rcx
	movq	%rbp, %rbx
	subq	%rcx, %rbx
	cmpq	%rbx, %rdx
	jbe	.LBB9_19
# BB#21:                                #   in Loop: Header=BB9_16 Depth=2
	movq	cardTopNotPref(%rip), %rbx
	cmpq	%rbx, %rdx
	ja	.LBB9_19
# BB#22:                                #   in Loop: Header=BB9_16 Depth=2
	addq	%rbp, %rbp
	subq	%rcx, %rbp
	subq	%rbx, %rbp
	movq	%rbp, %rcx
	jmp	.LBB9_23
	.p2align	4, 0x90
.LBB9_26:                               #   in Loop: Header=BB9_16 Depth=2
	movq	$1000000, (%r15,%rdx,8) # imm = 0xF4240
	jmp	.LBB9_27
	.p2align	4, 0x90
.LBB9_18:                               #   in Loop: Header=BB9_16 Depth=2
	cmpq	$0, (%r8,%rdx,8)
	je	.LBB9_23
.LBB9_19:                               # %.thread
                                        #   in Loop: Header=BB9_16 Depth=2
	leaq	(%r15,%rdx,8), %rbx
	movq	$10000, (%r15,%rdx,8)   # imm = 0x2710
	movl	$10000, %ecx            # imm = 0x2710
.LBB9_25:                               #   in Loop: Header=BB9_16 Depth=2
	leaq	(%rax,%rdx), %rbp
	testq	%rsi, %rsi
	cmovnsq	%rsi, %rbp
	addq	%rcx, %rbp
	movq	%rbp, (%rbx)
.LBB9_27:                               #   in Loop: Header=BB9_16 Depth=2
	incq	%rdx
	movq	channelTracks(%rip), %rbp
	decq	%rsi
	addq	$8, %rdi
	cmpq	%rbp, %rdx
	jbe	.LBB9_16
.LBB9_28:                               # %.loopexit
                                        #   in Loop: Header=BB9_6 Depth=1
	incq	%r14
	cmpq	channelNets(%rip), %r14
	jbe	.LBB9_6
.LBB9_29:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	BuildCostMatrix, .Lfunc_end9-BuildCostMatrix
	.cfi_endproc

	.type	costMatrix,@object      # @costMatrix
	.comm	costMatrix,8,8
	.type	tracksNotPref,@object   # @tracksNotPref
	.comm	tracksNotPref,8,8
	.type	tracksTopNotPref,@object # @tracksTopNotPref
	.comm	tracksTopNotPref,8,8
	.type	tracksBotNotPref,@object # @tracksBotNotPref
	.comm	tracksBotNotPref,8,8
	.type	tracksNoHCV,@object     # @tracksNoHCV
	.comm	tracksNoHCV,8,8
	.type	tracksAssign,@object    # @tracksAssign
	.comm	tracksAssign,8,8
	.type	netsAssign,@object      # @netsAssign
	.comm	netsAssign,8,8
	.type	netsAssignCopy,@object  # @netsAssignCopy
	.comm	netsAssignCopy,8,8
	.type	cardTopNotPref,@object  # @cardTopNotPref
	.comm	cardTopNotPref,8,8
	.type	cardBotNotPref,@object  # @cardBotNotPref
	.comm	cardBotNotPref,8,8
	.type	cardNotPref,@object     # @cardNotPref
	.comm	cardNotPref,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
