	.text
	.file	"shell.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$5432, %rsp             # imm = 0x1538
.Lcfi6:
	.cfi_def_cfa_offset 5488
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movl	%edi, %ebx
	movq	$0, 8(%rsp)
	movq	(%r15), %rax
	movq	%rax, Argv0(%rip)
	leaq	24(%rsp), %rdi
	xorl	%r12d, %r12d
	xorl	%esi, %esi
	movl	$5408, %edx             # imm = 0x1520
	callq	memset
	movl	$2, 48(%rsp)
	movw	$124, 72(%rsp)
	movl	$0, 56(%rsp)
	movl	$20, %edi
	movl	$mainPrompt, %esi
	movl	$.L.str.28, %edx
	xorl	%eax, %eax
	callq	sqlite3_snprintf
	movl	$20, %edi
	movl	$continuePrompt, %esi
	movl	$.L.str.29, %edx
	xorl	%eax, %eax
	callq	sqlite3_snprintf
	xorl	%edi, %edi
	callq	isatty
	movl	%eax, stdin_is_interactive(%rip)
	movl	$2, %edi
	movl	$interrupt_handler, %esi
	callq	signal
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	leal	-1(%rbx), %r13d
	movl	$1, %ebx
	cmpl	$2, %r13d
	jl	.LBB0_9
# BB#1:                                 # %.lr.ph728.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph728
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %r14
	movq	(%r15,%r14,8), %rbp
	cmpb	$45, (%rbp)
	jne	.LBB0_9
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_7
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.1, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_7
# BB#5:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.2, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB0_8
# BB#6:                                 #   in Loop: Header=BB0_2 Depth=1
	leal	1(%r14), %ebx
	movq	8(%r15,%r14,8), %r12
	jmp	.LBB0_8
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_2 Depth=1
	incl	%ebx
.LBB0_8:                                #   in Loop: Header=BB0_2 Depth=1
	incl	%ebx
	cmpl	%r13d, %ebx
	jl	.LBB0_2
.LBB0_9:                                # %._crit_edge
	movq	16(%rsp), %r14          # 8-byte Reload
	cmpl	%r14d, %ebx
	jge	.LBB0_11
# BB#10:
	movslq	%ebx, %rax
	incl	%ebx
	movq	(%r15,%rax,8), %rdi
	jmp	.LBB0_12
.LBB0_11:
	movl	$.L.str.3, %edi
.LBB0_12:
	movq	%rdi, 5424(%rsp)
	cmpl	%r14d, %ebx
	jge	.LBB0_14
# BB#13:
	movslq	%ebx, %rax
	movq	(%r15,%rax,8), %rax
	jmp	.LBB0_15
.LBB0_14:
	xorl	%eax, %eax
.LBB0_15:
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	stdout(%rip), %rax
	movq	%rax, 40(%rsp)
	xorl	%esi, %esi
	callq	access
	testl	%eax, %eax
	jne	.LBB0_17
# BB#16:
	leaq	24(%rsp), %rdi
	callq	open_db
.LBB0_17:
	movabsq	$4294967296, %rbx       # imm = 0x100000000
	testq	%r12, %r12
	je	.LBB0_19
# BB#18:
	xorl	%r13d, %r13d
	jmp	.LBB0_25
.LBB0_19:
	callq	getuid
	movl	%eax, %edi
	callq	getpwuid
	testq	%rax, %rax
	je	.LBB0_21
# BB#20:
	movq	32(%rax), %r12
	testq	%r12, %r12
	jne	.LBB0_22
.LBB0_21:                               # %.thread.i.i
	movl	$.L.str.160, %edi
	callq	getenv
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB0_85
.LBB0_22:                               # %.thread20.i.i
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rbp
	shlq	$32, %rbp
	addq	%rbx, %rbp
	sarq	$32, %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_85
# BB#23:
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %rbp
	addq	$16, %rbp
	movslq	%ebp, %rdi
	callq	malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB0_91
# BB#24:
	movl	$.L.str.34, %edx
	xorl	%eax, %eax
	movl	%ebp, %edi
	movq	%r12, %rsi
	movq	%r13, %rcx
	callq	sqlite3_snprintf
	movq	%r13, %rdi
	callq	free
	movq	%r12, %r13
.LBB0_25:
	movl	$.L.str.35, %esi
	movq	%r12, %rdi
	callq	fopen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_29
# BB#26:
	cmpl	$0, stdin_is_interactive(%rip)
	je	.LBB0_28
# BB#27:
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	printf
.LBB0_28:
	leaq	24(%rsp), %rdi
	movq	%rbp, %rsi
	callq	process_input
	movq	%rbp, %rdi
	callq	fclose
.LBB0_29:
	movq	%rbx, %r12
	movq	%r13, %rdi
	callq	free
	cmpl	$2, %r14d
	jl	.LBB0_62
.LBB0_30:                               # %.lr.ph
	movl	$1, %r13d
	.p2align	4, 0x90
.LBB0_31:                               # =>This Inner Loop Header: Depth=1
	movslq	%r13d, %rbx
	movq	(%r15,%rbx,8), %rax
	cmpb	$45, (%rax)
	jne	.LBB0_62
# BB#32:                                #   in Loop: Header=BB0_31 Depth=1
	leaq	1(%rax), %rbp
	cmpb	$45, 1(%rax)
	cmovneq	%rax, %rbp
	movl	$.L.str.2, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_48
# BB#33:                                #   in Loop: Header=BB0_31 Depth=1
	movl	$.L.str.4, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_49
# BB#34:                                #   in Loop: Header=BB0_31 Depth=1
	movl	$.L.str.5, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_50
# BB#35:                                #   in Loop: Header=BB0_31 Depth=1
	movl	$.L.str.6, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_51
# BB#36:                                #   in Loop: Header=BB0_31 Depth=1
	movl	$.L.str.7, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_52
# BB#37:                                #   in Loop: Header=BB0_31 Depth=1
	movl	$.L.str.8, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_53
# BB#38:                                #   in Loop: Header=BB0_31 Depth=1
	movl	$.L.str, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_54
# BB#39:                                #   in Loop: Header=BB0_31 Depth=1
	movl	$.L.str.1, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_55
# BB#40:                                #   in Loop: Header=BB0_31 Depth=1
	movl	$.L.str.11, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_56
# BB#41:                                #   in Loop: Header=BB0_31 Depth=1
	movl	$.L.str.12, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_57
# BB#42:                                #   in Loop: Header=BB0_31 Depth=1
	movl	$.L.str.13, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_58
# BB#43:                                #   in Loop: Header=BB0_31 Depth=1
	movl	$.L.str.14, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_59
# BB#44:                                #   in Loop: Header=BB0_31 Depth=1
	movl	$.L.str.15, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_86
# BB#45:                                #   in Loop: Header=BB0_31 Depth=1
	movl	$.L.str.17, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_60
# BB#46:                                #   in Loop: Header=BB0_31 Depth=1
	movl	$.L.str.18, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB0_87
# BB#47:                                #   in Loop: Header=BB0_31 Depth=1
	movl	$0, stdin_is_interactive(%rip)
	jmp	.LBB0_61
	.p2align	4, 0x90
.LBB0_48:                               #   in Loop: Header=BB0_31 Depth=1
	incl	%r13d
	jmp	.LBB0_61
	.p2align	4, 0x90
.LBB0_49:                               #   in Loop: Header=BB0_31 Depth=1
	movl	$4, 48(%rsp)
	jmp	.LBB0_61
.LBB0_50:                               #   in Loop: Header=BB0_31 Depth=1
	movl	$2, 48(%rsp)
	jmp	.LBB0_61
.LBB0_51:                               #   in Loop: Header=BB0_31 Depth=1
	movl	$0, 48(%rsp)
	jmp	.LBB0_61
.LBB0_52:                               #   in Loop: Header=BB0_31 Depth=1
	movl	$1, 48(%rsp)
	jmp	.LBB0_61
.LBB0_53:                               #   in Loop: Header=BB0_31 Depth=1
	movl	$7, 48(%rsp)
	movw	$44, 72(%rsp)
	jmp	.LBB0_61
.LBB0_54:                               #   in Loop: Header=BB0_31 Depth=1
	leal	1(%rbx), %r13d
	movq	8(%r15,%rbx,8), %r8
	movl	$20, %edi
	movl	$.L.str.10, %edx
	movl	$19, %ecx
	xorl	%eax, %eax
	leaq	72(%rsp), %rsi
	callq	sqlite3_snprintf
	jmp	.LBB0_61
.LBB0_55:                               #   in Loop: Header=BB0_31 Depth=1
	leal	1(%rbx), %r13d
	movq	8(%r15,%rbx,8), %r8
	movl	$20, %edi
	movl	$.L.str.10, %edx
	movl	$19, %ecx
	xorl	%eax, %eax
	leaq	892(%rsp), %rsi
	callq	sqlite3_snprintf
	jmp	.LBB0_61
.LBB0_56:                               #   in Loop: Header=BB0_31 Depth=1
	movl	$1, 56(%rsp)
	jmp	.LBB0_61
.LBB0_57:                               #   in Loop: Header=BB0_31 Depth=1
	movl	$0, 56(%rsp)
	jmp	.LBB0_61
.LBB0_58:                               #   in Loop: Header=BB0_31 Depth=1
	movl	$1, 32(%rsp)
	jmp	.LBB0_61
.LBB0_59:                               #   in Loop: Header=BB0_31 Depth=1
	movl	$1, bail_on_error(%rip)
	jmp	.LBB0_61
.LBB0_60:                               #   in Loop: Header=BB0_31 Depth=1
	movl	$1, stdin_is_interactive(%rip)
	.p2align	4, 0x90
.LBB0_61:                               # %process_sqliterc.exit
                                        #   in Loop: Header=BB0_31 Depth=1
	incl	%r13d
	cmpl	%r14d, %r13d
	jl	.LBB0_31
.LBB0_62:                               # %.critedge
	movq	(%rsp), %rbp            # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB0_67
# BB#63:
	cmpb	$46, (%rbp)
	je	.LBB0_90
# BB#64:
	leaq	24(%rsp), %rbx
	movq	%rbx, %rdi
	callq	open_db
	movq	24(%rsp), %rdi
	leaq	8(%rsp), %r8
	movl	$callback, %edx
	movq	%rbp, %rsi
	movq	%rbx, %rcx
	callq	sqlite3_exec
	xorl	%ebx, %ebx
	testl	%eax, %eax
	je	.LBB0_79
# BB#65:
	movq	8(%rsp), %rdx
	testq	%rdx, %rdx
	je	.LBB0_79
# BB#66:
	movq	stderr(%rip), %rdi
	movl	$.L.str.23, %esi
	jmp	.LBB0_92
.LBB0_67:
	cmpl	$0, stdin_is_interactive(%rip)
	je	.LBB0_74
# BB#68:
	callq	sqlite3_libversion
	movq	%rax, %rcx
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	callq	getuid
	movl	%eax, %edi
	callq	getpwuid
	testq	%rax, %rax
	je	.LBB0_70
# BB#69:
	movq	32(%rax), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_71
.LBB0_70:                               # %.thread.i
	movl	$.L.str.160, %edi
	callq	getenv
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_76
.LBB0_71:                               # %.thread20.i
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rbp
	shlq	$32, %rbp
	addq	%r12, %rbp
	sarq	$32, %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_76
# BB#72:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
	addq	$20, %rbx
	movslq	%ebx, %rdi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_77
# BB#73:
	movl	$.L.str.25, %edx
	xorl	%eax, %eax
	movl	%ebx, %edi
	movq	%r14, %rsi
	movq	%r15, %rcx
	callq	sqlite3_snprintf
	leaq	24(%rsp), %rdi
	xorl	%esi, %esi
	callq	process_input
	movl	%eax, %ebx
	movq	%r14, %rdi
	callq	free
	jmp	.LBB0_78
.LBB0_74:
	movq	stdin(%rip), %rsi
	leaq	24(%rsp), %rdi
	callq	process_input
	movl	%eax, %ebx
	jmp	.LBB0_79
.LBB0_76:
	xorl	%r15d, %r15d
.LBB0_77:                               # %.thread
	leaq	24(%rsp), %rdi
	xorl	%esi, %esi
	callq	process_input
	movl	%eax, %ebx
.LBB0_78:
	movq	%r15, %rdi
	callq	free
.LBB0_79:
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_81
# BB#80:
	callq	free
	movq	$0, 64(%rsp)
.LBB0_81:                               # %set_table_name.exit
	movq	db(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_83
# BB#82:
	callq	sqlite3_close
	testl	%eax, %eax
	jne	.LBB0_84
.LBB0_83:
	movl	%ebx, %eax
	addq	$5432, %rsp             # imm = 0x1538
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_84:
	movq	stderr(%rip), %rbp
	movq	db(%rip), %rdi
	callq	sqlite3_errmsg
	movq	%rax, %rcx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	fprintf
	jmp	.LBB0_83
.LBB0_85:
	movq	%rbx, %r12
	movq	stderr(%rip), %rdi
	movq	Argv0(%rip), %rdx
	movl	$.L.str.32, %esi
	xorl	%eax, %eax
	callq	fprintf
	cmpl	$2, %r14d
	jge	.LBB0_30
	jmp	.LBB0_62
.LBB0_86:
	callq	sqlite3_libversion
	movq	%rax, %rdi
	callq	puts
	xorl	%ebx, %ebx
	jmp	.LBB0_83
.LBB0_87:
	movl	$.L.str.19, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_93
# BB#88:
	movl	$.L.str.20, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_93
# BB#89:
	movq	stderr(%rip), %rdi
	movq	Argv0(%rip), %rdx
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	movq	%rbp, %rcx
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.22, %edi
	movl	$33, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %ebx
	jmp	.LBB0_83
.LBB0_90:
	leaq	24(%rsp), %rsi
	movq	%rbp, %rdi
	callq	do_meta_command
	xorl	%edi, %edi
	callq	exit
.LBB0_91:
	movq	stderr(%rip), %rdi
	movq	Argv0(%rip), %rdx
	movl	$.L.str.33, %esi
.LBB0_92:
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB0_93:
	movq	stderr(%rip), %rdi
	movq	Argv0(%rip), %rdx
	movl	$.L.str.37, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.38, %esi
	movl	$zOptions, %edx
	jmp	.LBB0_92
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.p2align	4, 0x90
	.type	interrupt_handler,@function
interrupt_handler:                      # @interrupt_handler
	.cfi_startproc
# BB#0:
	movl	$1, seenInterrupt(%rip)
	movq	db(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_1
# BB#2:
	jmp	sqlite3_interrupt       # TAILCALL
.LBB1_1:
	retq
.Lfunc_end1:
	.size	interrupt_handler, .Lfunc_end1-interrupt_handler
	.cfi_endproc

	.p2align	4, 0x90
	.type	open_db,@function
open_db:                                # @open_db
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	cmpq	$0, (%rbx)
	jne	.LBB2_2
# BB#1:
	movq	5400(%rbx), %rdi
	movq	%rbx, %rsi
	callq	sqlite3_open
	movq	(%rbx), %rdi
	movq	%rdi, db(%rip)
	movl	$.L.str.30, %esi
	movl	$0, %edx
	movl	$1, %ecx
	movl	$0, %r8d
	movl	$shellstaticFunc, %r9d
	pushq	$0
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	callq	sqlite3_create_function
	addq	$16, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset -16
	movq	db(%rip), %rdi
	callq	sqlite3_errcode
	testl	%eax, %eax
	jne	.LBB2_3
.LBB2_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_3:
	movq	stderr(%rip), %r14
	movq	5400(%rbx), %rbx
	movq	db(%rip), %rdi
	callq	sqlite3_errmsg
	movq	%rax, %rcx
	movl	$.L.str.31, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end2:
	.size	open_db, .Lfunc_end2-open_db
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	4                       # 0x4
	.long	13                      # 0xd
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI3_1:
	.long	4                       # 0x4
	.long	13                      # 0xd
	.long	2                       # 0x2
	.long	13                      # 0xd
	.text
	.p2align	4, 0x90
	.type	do_meta_command,@function
do_meta_command:                        # @do_meta_command
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	subq	$5928, %rsp             # imm = 0x1728
.Lcfi27:
	.cfi_def_cfa_offset 5984
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	cmpb	$0, 1(%rbp)
	je	.LBB3_1
# BB#2:                                 # %.lr.ph759.preheader
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	leaq	-1(%rbp), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	1(%rbp), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	movl	$1, %r14d
	movabsq	$4294967296, %r13       # imm = 0x100000000
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph759
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_5 Depth 2
                                        #     Child Loop BB3_38 Depth 2
                                        #     Child Loop BB3_43 Depth 2
                                        #     Child Loop BB3_11 Depth 2
                                        #     Child Loop BB3_18 Depth 2
	cmpl	$49, %r12d
	ja	.LBB3_64
# BB#4:                                 # %.preheader699
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movslq	%r12d, %r12
	callq	__ctype_b_loc
	movq	(%rax), %r9
	movslq	%r14d, %rsi
	movq	%rsi, %rdx
	shlq	$32, %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rsi), %r10
	leal	1(%rsi), %ecx
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	leaq	(%rbp,%rsi), %rbp
	movl	%r14d, %r8d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_5:                                #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %r11d
	movl	%r8d, %edi
	movq	%rbp, %r15
	movzbl	1(%r10,%rsi), %ebx
	addq	%r13, %rdx
	incq	%rsi
	leal	1(%r11), %ecx
	leal	1(%rdi), %r8d
	leaq	1(%r15), %rbp
	testb	$32, 1(%r9,%rbx,2)
	jne	.LBB3_5
# BB#6:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpb	$34, %bl
	je	.LBB3_10
# BB#7:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpb	$39, %bl
	je	.LBB3_10
# BB#8:                                 #   in Loop: Header=BB3_3 Depth=1
	testb	%bl, %bl
	movq	48(%rsp), %rbp          # 8-byte Reload
	je	.LBB3_9
# BB#35:                                #   in Loop: Header=BB3_3 Depth=1
	leaq	(%r10,%rsi), %rcx
	movq	%rcx, 112(%rsp,%r12,8)
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	addq	%rcx, %rdx
	sarq	$32, %rdx
	movb	(%rbp,%rdx), %dl
	testb	%dl, %dl
	je	.LBB3_36
# BB#37:                                # %.lr.ph754
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	(%rax), %r9
	movslq	%edi, %rax
	addq	64(%rsp), %rax          # 8-byte Folded Reload
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	56(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB3_38:                               #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%dl, %ecx
	testb	$32, 1(%r9,%rcx,2)
	jne	.LBB3_287
# BB#39:                                #   in Loop: Header=BB3_38 Depth=2
	movzbl	(%rax), %edx
	incl	%r11d
	incq	%rax
	testb	%dl, %dl
	jne	.LBB3_38
# BB#40:                                # %.critedge2.thread.loopexit
                                        #   in Loop: Header=BB3_3 Depth=1
	decl	%r11d
	jmp	.LBB3_41
	.p2align	4, 0x90
.LBB3_10:                               #   in Loop: Header=BB3_3 Depth=1
	sarq	$32, %rdx
	movq	48(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rdx), %rax
	movq	%rax, 112(%rsp,%r12,8)
	decl	%edx
	movq	%rax, %rsi
	movl	%edx, %r11d
	.p2align	4, 0x90
.LBB3_11:                               #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rsi), %ecx
	incl	%r11d
	incq	%rsi
	testb	%cl, %cl
	je	.LBB3_13
# BB#12:                                #   in Loop: Header=BB3_11 Depth=2
	cmpb	%bl, %cl
	jne	.LBB3_11
.LBB3_13:                               # %.critedge1
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpb	%bl, %cl
	movq	8(%rsp), %r12           # 8-byte Reload
	jne	.LBB3_15
# BB#14:                                #   in Loop: Header=BB3_3 Depth=1
	incl	%r11d
	movb	$0, -1(%rsi)
.LBB3_15:                               #   in Loop: Header=BB3_3 Depth=1
	incl	%r12d
	cmpb	$34, %bl
	jne	.LBB3_61
# BB#16:                                #   in Loop: Header=BB3_3 Depth=1
	movb	(%rax), %bl
	xorl	%edx, %edx
	testb	%bl, %bl
	je	.LBB3_34
# BB#17:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	%rax, %rsi
	xorl	%edi, %edi
	jmp	.LBB3_18
.LBB3_21:                               #   in Loop: Header=BB3_18 Depth=2
	movl	$10, %ebp
	jmp	.LBB3_32
.LBB3_25:                               #   in Loop: Header=BB3_18 Depth=2
	movl	$13, %ebp
	jmp	.LBB3_32
.LBB3_26:                               #   in Loop: Header=BB3_18 Depth=2
	movl	%ebp, %ecx
	andb	$-8, %cl
	cmpb	$48, %cl
	jne	.LBB3_32
# BB#27:                                #   in Loop: Header=BB3_18 Depth=2
	addl	$-48, %ebp
	movsbl	2(%rax,%rbx), %r8d
	movl	%r8d, %ecx
	andb	$-8, %cl
	cmpb	$48, %cl
	jne	.LBB3_32
# BB#28:                                #   in Loop: Header=BB3_18 Depth=2
	leal	-48(%r8,%rbp,8), %ebp
	movsbl	3(%rax,%rbx), %edi
	movl	%edi, %ecx
	andb	$-8, %cl
	cmpb	$48, %cl
	jne	.LBB3_29
# BB#30:                                #   in Loop: Header=BB3_18 Depth=2
	addq	$3, %rbx
	leal	-48(%rdi,%rbp,8), %ebp
	jmp	.LBB3_31
.LBB3_29:                               #   in Loop: Header=BB3_18 Depth=2
	addq	$2, %rbx
.LBB3_31:                               #   in Loop: Header=BB3_18 Depth=2
	movl	%ebx, %edi
	jmp	.LBB3_32
	.p2align	4, 0x90
.LBB3_18:                               # %.lr.ph.i
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$92, %bl
	jne	.LBB3_19
# BB#20:                                #   in Loop: Header=BB3_18 Depth=2
	movslq	%edi, %rbx
	leaq	1(%rbx), %rdi
	movsbl	1(%rax,%rbx), %ebp
	cmpl	$110, %ebp
	je	.LBB3_21
# BB#22:                                #   in Loop: Header=BB3_18 Depth=2
	cmpb	$114, %bpl
	je	.LBB3_25
# BB#23:                                #   in Loop: Header=BB3_18 Depth=2
	cmpb	$116, %bpl
	jne	.LBB3_26
# BB#24:                                #   in Loop: Header=BB3_18 Depth=2
	movl	$9, %ebp
	jmp	.LBB3_32
	.p2align	4, 0x90
.LBB3_19:                               #   in Loop: Header=BB3_18 Depth=2
	movsbl	%bl, %ebp
.LBB3_32:                               #   in Loop: Header=BB3_18 Depth=2
	movb	%bpl, (%rsi)
	movslq	%edi, %rdi
	movzbl	1(%rax,%rdi), %ebx
	incq	%rdi
	addq	%r13, %rdx
	incq	%rsi
	testb	%bl, %bl
	jne	.LBB3_18
# BB#33:                                # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB3_3 Depth=1
	sarq	$32, %rdx
	movq	48(%rsp), %rbp          # 8-byte Reload
.LBB3_34:                               # %resolve_backslashes.exit
                                        #   in Loop: Header=BB3_3 Depth=1
	addq	%rdx, %rax
	jmp	.LBB3_60
.LBB3_36:                               # %..critedge2.thread_crit_edge
                                        #   in Loop: Header=BB3_3 Depth=1
	decl	%r14d
	addq	%rsi, %r14
	movl	%r14d, %r11d
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	56(%rsp), %r8           # 8-byte Reload
	jmp	.LBB3_41
.LBB3_287:                              #   in Loop: Header=BB3_3 Depth=1
	movb	$0, -1(%rax)
	movb	(%r10,%rsi), %bl
.LBB3_41:                               # %.critedge2.thread
                                        #   in Loop: Header=BB3_3 Depth=1
	xorl	%eax, %eax
	testb	%bl, %bl
	je	.LBB3_59
# BB#42:                                # %.lr.ph.i664.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	xorl	%edx, %edx
	jmp	.LBB3_43
.LBB3_46:                               #   in Loop: Header=BB3_43 Depth=2
	movl	$10, %ebx
	jmp	.LBB3_57
.LBB3_50:                               #   in Loop: Header=BB3_43 Depth=2
	movl	$13, %ebx
	jmp	.LBB3_57
.LBB3_51:                               #   in Loop: Header=BB3_43 Depth=2
	movl	%ebx, %ecx
	andb	$-8, %cl
	cmpb	$48, %cl
	jne	.LBB3_57
# BB#52:                                #   in Loop: Header=BB3_43 Depth=2
	addl	$-48, %ebx
	leaq	2(%r10,%r9), %rcx
	movsbl	(%rsi,%rcx), %edi
	movl	%edi, %ecx
	andb	$-8, %cl
	cmpb	$48, %cl
	jne	.LBB3_57
# BB#53:                                #   in Loop: Header=BB3_43 Depth=2
	leal	-48(%rdi,%rbx,8), %ebx
	leaq	3(%r10,%r9), %rcx
	movsbl	(%rsi,%rcx), %ecx
	movl	%ecx, %edx
	andb	$-8, %dl
	cmpb	$48, %dl
	jne	.LBB3_54
# BB#55:                                #   in Loop: Header=BB3_43 Depth=2
	addq	$3, %r9
	leal	-48(%rcx,%rbx,8), %ebx
	jmp	.LBB3_56
.LBB3_54:                               #   in Loop: Header=BB3_43 Depth=2
	addq	$2, %r9
.LBB3_56:                               #   in Loop: Header=BB3_43 Depth=2
	movl	%r9d, %edx
	jmp	.LBB3_57
	.p2align	4, 0x90
.LBB3_43:                               # %.lr.ph.i664
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$92, %bl
	jne	.LBB3_44
# BB#45:                                #   in Loop: Header=BB3_43 Depth=2
	movslq	%edx, %r9
	leaq	1(%r9), %rdx
	leaq	1(%r10,%r9), %rcx
	movsbl	(%rsi,%rcx), %ebx
	cmpl	$110, %ebx
	je	.LBB3_46
# BB#47:                                #   in Loop: Header=BB3_43 Depth=2
	cmpb	$114, %bl
	je	.LBB3_50
# BB#48:                                #   in Loop: Header=BB3_43 Depth=2
	cmpb	$116, %bl
	jne	.LBB3_51
# BB#49:                                #   in Loop: Header=BB3_43 Depth=2
	movl	$9, %ebx
	jmp	.LBB3_57
	.p2align	4, 0x90
.LBB3_44:                               #   in Loop: Header=BB3_43 Depth=2
	movsbl	%bl, %ebx
.LBB3_57:                               #   in Loop: Header=BB3_43 Depth=2
	movb	%bl, (%r15)
	movslq	%edx, %rdx
	leaq	1(%r10,%rdx), %rcx
	incq	%rdx
	movb	(%rsi,%rcx), %bl
	incq	%r15
	addq	%r13, %rax
	testb	%bl, %bl
	jne	.LBB3_43
# BB#58:                                # %._crit_edge.loopexit.i670
                                        #   in Loop: Header=BB3_3 Depth=1
	sarq	$32, %rax
.LBB3_59:                               # %resolve_backslashes.exit672
                                        #   in Loop: Header=BB3_3 Depth=1
	incl	%r12d
	addq	%r8, %rax
	addq	24(%rsp), %rax          # 8-byte Folded Reload
	addq	%rsi, %rax
.LBB3_60:                               # %.backedge.sink.split
                                        #   in Loop: Header=BB3_3 Depth=1
	movb	$0, (%rax)
.LBB3_61:                               # %.backedge
                                        #   in Loop: Header=BB3_3 Depth=1
	movslq	%r11d, %rax
	cmpb	$0, (%rbp,%rax)
	movl	%r11d, %r14d
	jne	.LBB3_3
	jmp	.LBB3_62
.LBB3_1:
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_9:
	movq	8(%rsp), %r12           # 8-byte Reload
.LBB3_62:                               # %.critedge
	testl	%r12d, %r12d
	je	.LBB3_63
.LBB3_64:                               # %.critedge.thread
	movq	112(%rsp), %r15
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbp
	movb	(%r15), %bl
	cmpb	$98, %bl
	jne	.LBB3_69
# BB#65:                                # %.critedge.thread
	cmpl	$2, %ebp
	jl	.LBB3_69
# BB#66:
	movslq	%ebp, %rdx
	movl	$.L.str.40, %esi
	movq	%r15, %rdi
	callq	strncmp
	cmpl	$2, %r12d
	jl	.LBB3_284
# BB#67:
	testl	%eax, %eax
	jne	.LBB3_284
# BB#68:
	movq	120(%rsp), %rdi
	callq	booleanValue
	movl	%eax, bail_on_error(%rip)
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_69:
	cmpb	$100, %bl
	jne	.LBB3_75
# BB#70:
	cmpl	$2, %ebp
	jl	.LBB3_75
# BB#71:
	movslq	%ebp, %rdx
	movl	$.L.str.41, %esi
	movq	%r15, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_72
.LBB3_75:
	movq	%r12, %r13
	cmpb	$100, %bl
	movq	16(%rsp), %r12          # 8-byte Reload
	jne	.LBB3_91
# BB#76:
	movslq	%ebp, %rdx
	movl	$.L.str.44, %esi
	movq	%r15, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_83
# BB#77:
	xorl	%ecx, %ecx
.LBB3_78:                               # %.critedge658.thread691
	cmpl	$5, %ebp
	jl	.LBB3_277
# BB#79:                                # %.critedge658.thread691
	testb	%cl, %cl
	je	.LBB3_277
# BB#80:
	movslq	%ebp, %r14
	movl	$.L.str.116, %esi
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	strncmp
	cmpl	$2, %r13d
	jl	.LBB3_274
# BB#81:
	testl	%eax, %eax
	jne	.LBB3_274
# BB#82:
	movq	%r12, %rdi
	callq	open_db
	movq	(%r12), %rbx
	movq	120(%rsp), %rdi
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	sqlite3_busy_timeout
	jmp	.LBB3_286
.LBB3_63:
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_91:
	movl	%ebx, %eax
	addb	$-101, %al
	cmpb	$14, %al
	ja	.LBB3_251
# BB#92:
	movzbl	%al, %eax
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_93:
	movslq	%ebp, %rbx
	movl	$.L.str.52, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	strncmp
	cmpl	$2, %r13d
	jl	.LBB3_96
# BB#94:
	testl	%eax, %eax
	jne	.LBB3_96
# BB#95:
	movq	120(%rsp), %rdi
	callq	booleanValue
	movl	%eax, 8(%r12)
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_72:
	movq	$0, 32(%rsp)
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	open_db
	leaq	520(%rsp), %rbx
	movl	$5408, %edx             # imm = 0x1520
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	memcpy
	movl	$1, 552(%rsp)
	movl	$1, 544(%rsp)
	movl	$3, 588(%rsp)
	movl	$15, 592(%rsp)
	movl	$58, 596(%rsp)
	movl	$0, 532(%rsp)
	movq	(%rbp), %rdi
	leaq	32(%rsp), %r8
	movl	$.L.str.42, %esi
	movl	$callback, %edx
	movq	%rbx, %rcx
	callq	sqlite3_exec
	movq	32(%rsp), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_73
.LBB3_74:
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_83:
	movq	%r12, %rdi
	callq	open_db
	movq	16(%r12), %rcx
	movl	$.L.str.45, %edi
	movl	$19, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$0, 28(%r12)
	cmpl	$1, %r13d
	jne	.LBB3_84
# BB#288:
	movl	$.L.str.46, %esi
	movq	%r12, %rdi
	callq	run_schema_dump_query
	movq	(%r12), %rsi
	movq	16(%r12), %rdi
	movl	$.L.str.47, %edx
	callq	run_table_dump_query
	cmpl	$0, 28(%r12)
	jne	.LBB3_88
	jmp	.LBB3_89
.LBB3_84:                               # %.preheader693
	cmpl	$2, %r13d
	jl	.LBB3_87
# BB#85:                                # %.lr.ph723
	movl	%r13d, %ebx
	leaq	120(%rsp), %rbp
	decq	%rbx
	.p2align	4, 0x90
.LBB3_86:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	%rax, zShellStatic(%rip)
	movl	$.L.str.48, %esi
	movq	%r12, %rdi
	callq	run_schema_dump_query
	movq	(%r12), %rsi
	movq	16(%r12), %rdi
	movl	$.L.str.49, %edx
	callq	run_table_dump_query
	movq	$0, zShellStatic(%rip)
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB3_86
.LBB3_87:                               # %.loopexit
	cmpl	$0, 28(%r12)
	je	.LBB3_89
.LBB3_88:
	movq	16(%r12), %rcx
	movl	$.L.str.50, %edi
	movl	$28, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$0, 28(%r12)
.LBB3_89:
	movq	16(%r12), %rcx
	movl	$.L.str.51, %edi
	movl	$8, %esi
.LBB3_90:                               # %.critedge78
	movl	$1, %edx
	callq	fwrite
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_274:
	movl	$.L.str.117, %esi
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	strncmp
	cmpl	$2, %r13d
	jl	.LBB3_277
# BB#275:
	testl	%eax, %eax
	jne	.LBB3_277
# BB#276:
	movq	120(%rsp), %rdi
	callq	booleanValue
	movl	%eax, enableTimer(%rip)
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_277:                              # %.thread690
	cmpb	$119, %bl
	jne	.LBB3_284
# BB#278:
	movslq	%ebp, %rdx
	movl	$.L.str.108, %esi
	movq	%r15, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB3_284
# BB#279:                               # %.preheader
	xorl	%r14d, %r14d
	cmpl	$2, %r13d
	jl	.LBB3_286
# BB#280:                               # %.lr.ph.preheader
	movslq	%r13d, %rbx
	xorl	%r14d, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_281:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	120(%rsp,%rbp,8), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, 68(%r12,%rbp,4)
	leaq	2(%rbp), %rax
	cmpq	%rbx, %rax
	jge	.LBB3_286
# BB#282:                               # %.lr.ph
                                        #   in Loop: Header=BB3_281 Depth=1
	incq	%rbp
	cmpq	$100, %rax
	jb	.LBB3_281
	jmp	.LBB3_286
.LBB3_251:                              # %.critedge658
	cmpb	$116, %bl
	sete	%cl
	jne	.LBB3_78
# BB#252:                               # %.critedge658
	cmpl	$2, %ebp
	jl	.LBB3_78
# BB#253:
	movslq	%ebp, %rdx
	movl	$.L.str.110, %esi
	movq	%r15, %rdi
	callq	strncmp
	movb	$1, %cl
	testl	%eax, %eax
	jne	.LBB3_78
# BB#254:
	movq	%r12, %rdi
	callq	open_db
	movq	(%r12), %rdi
	cmpl	$1, %r13d
	jne	.LBB3_256
# BB#255:
	leaq	520(%rsp), %rdx
	leaq	80(%rsp), %rcx
	leaq	32(%rsp), %r9
	movl	$.L.str.111, %esi
	xorl	%r8d, %r8d
	callq	sqlite3_get_table
	movl	%eax, %ebx
	jmp	.LBB3_257
.LBB3_107:
	movslq	%ebp, %rbx
	movl	$.L.str.55, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_112
# BB#108:
	movl	$.L.str.56, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	strncmp
	cmpl	$2, %r13d
	jl	.LBB3_110
# BB#109:
	testl	%eax, %eax
	je	.LBB3_113
	jmp	.LBB3_110
.LBB3_114:
	movslq	%ebp, %rbx
	movl	$.L.str.58, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	strncmp
	cmpl	$3, %r13d
	jl	.LBB3_173
# BB#115:
	testl	%eax, %eax
	jne	.LBB3_173
# BB#116:
	movq	120(%rsp), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	128(%rsp), %r13
	movq	%r12, %rdi
	callq	open_db
	leaq	48(%r12), %r15
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbp
	testl	%ebp, %ebp
	je	.LBB3_117
# BB#118:
	xorl	%r14d, %r14d
	movl	$.L.str.60, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	sqlite3_mprintf
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_286
# BB#119:
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%r13, %r12
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r13
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	xorl	%r14d, %r14d
	leaq	520(%rsp), %rcx
	movl	$-1, %edx
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	callq	sqlite3_prepare
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	sqlite3_free
	testl	%ebp, %ebp
	jne	.LBB3_120
# BB#121:
	movq	520(%rsp), %rdi
	callq	sqlite3_column_count
	movl	%eax, %ebx
	movq	520(%rsp), %rdi
	callq	sqlite3_finalize
	testl	%ebx, %ebx
	je	.LBB3_286
# BB#122:
	leal	20(%r13,%rbx,2), %eax
	movslq	%eax, %rdi
	callq	malloc
	movq	%r13, %rdi
	movq	%rax, %r13
	testq	%r13, %r13
	movq	%r12, %rcx
	movq	24(%rsp), %r12          # 8-byte Reload
	je	.LBB3_286
# BB#123:
	addl	$20, %edi
	movl	$.L.str.61, %edx
	xorl	%eax, %eax
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r13, %rsi
	callq	sqlite3_snprintf
	movq	%r13, %rdi
	callq	strlen
	movslq	%eax, %rdx
	leal	1(%rax), %ecx
	addq	%r13, %rdx
	cmpl	$2, %ebx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	jl	.LBB3_133
# BB#124:                               # %.lr.ph742.preheader
	movslq	%eax, %r9
	leal	-2(%rbx), %esi
	leal	3(%rbx), %edi
	andl	$3, %edi
	je	.LBB3_125
# BB#126:                               # %.lr.ph742.prol.preheader
	xorl	%ebp, %ebp
	movq	%r9, %rax
.LBB3_127:                              # %.lr.ph742.prol
                                        # =>This Inner Loop Header: Depth=1
	movb	$44, (%r13,%rax)
	addq	$2, %rax
	movslq	%ecx, %rcx
	movb	$63, (%r13,%rcx)
	addl	$2, %ecx
	incl	%ebp
	cmpl	%ebp, %edi
	jne	.LBB3_127
# BB#128:                               # %.lr.ph742.prol.loopexit.unr-lcssa
	leaq	(%r13,%rax), %rdx
	incl	%ebp
	jmp	.LBB3_129
.LBB3_178:
	movslq	%ebp, %rdx
	movl	$.L.str.69, %esi
	movq	%r15, %rdi
	callq	strncmp
	cmpl	$2, %r13d
	jl	.LBB3_284
# BB#179:
	testl	%eax, %eax
	jne	.LBB3_284
# BB#180:
	movq	120(%rsp), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movslq	%eax, %r14
	movl	$.L.str.70, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_182
# BB#181:
	movl	$.L.str.71, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_182
# BB#183:
	movl	$.L.str.72, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_185
# BB#184:
	movl	$.L.str.73, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_185
# BB#186:
	movl	$.L.str.74, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_187
# BB#188:
	movl	$.L.str.75, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_189
# BB#190:
	movl	$.L.str.76, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_191
# BB#192:
	movl	$.L.str.77, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_193
# BB#195:
	movl	$.L.str.78, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_196
# BB#197:
	movl	$.L.str.80, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB3_202
# BB#198:
	movl	$5, 24(%r12)
	cmpl	$3, %r13d
	jl	.LBB3_200
# BB#199:
	movq	128(%rsp), %rsi
	jmp	.LBB3_201
.LBB3_203:
	movslq	%ebp, %rdx
	movl	$.L.str.83, %esi
	movq	%r15, %rdi
	callq	strncmp
	cmpl	$2, %r13d
	jne	.LBB3_284
# BB#204:
	testl	%eax, %eax
	jne	.LBB3_284
# BB#205:
	addq	$868, %r12              # imm = 0x364
	jmp	.LBB3_206
.LBB3_207:
	movslq	%ebp, %rdx
	movl	$.L.str.84, %esi
	movq	%r15, %rdi
	callq	strncmp
	cmpl	$2, %r13d
	jne	.LBB3_284
# BB#208:
	testl	%eax, %eax
	jne	.LBB3_284
# BB#209:
	movq	16(%r12), %rdi
	cmpq	stdout(%rip), %rdi
	je	.LBB3_211
# BB#210:
	callq	fclose
.LBB3_211:
	movq	120(%rsp), %rbx
	movl	$.L.str.85, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_212
# BB#213:
	movl	$.L.str.86, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.LBB3_214
# BB#215:
	addq	$1300, %r12             # imm = 0x514
	xorl	%r14d, %r14d
	movl	$4096, %edi             # imm = 0x1000
	movl	$.L.str.88, %edx
	xorl	%eax, %eax
	movq	%r12, %rsi
	movq	%rbx, %rcx
	callq	sqlite3_snprintf
	jmp	.LBB3_286
.LBB3_216:
	movslq	%ebp, %rdx
	movl	$.L.str.89, %esi
	movq	%r15, %rdi
	callq	strncmp
	movl	%r13d, %ecx
	orl	$1, %ecx
	cmpl	$3, %ecx
	jne	.LBB3_284
# BB#217:
	testl	%eax, %eax
	jne	.LBB3_284
# BB#218:
	xorl	%r14d, %r14d
	cmpl	$2, %r13d
	jl	.LBB3_286
# BB#219:
	movq	120(%rsp), %rsi
	movl	$mainPrompt, %edi
	movl	$19, %edx
	callq	strncpy
	cmpl	$2, %r13d
	je	.LBB3_286
# BB#220:
	movq	128(%rsp), %rsi
	movl	$continuePrompt, %edi
	movl	$19, %edx
	callq	strncpy
	jmp	.LBB3_286
.LBB3_283:
	movslq	%ebp, %rdx
	movl	$.L.str.90, %esi
	movq	%r15, %rdi
	callq	strncmp
	movl	$2, %r14d
	testl	%eax, %eax
	je	.LBB3_286
	jmp	.LBB3_284
.LBB3_221:
	movslq	%ebp, %rdx
	movl	$.L.str.91, %esi
	movq	%r15, %rdi
	callq	strncmp
	cmpl	$2, %r13d
	jne	.LBB3_284
# BB#222:
	testl	%eax, %eax
	jne	.LBB3_284
# BB#223:
	movq	120(%rsp), %rbx
	movl	$.L.str.35, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_224
# BB#225:
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	process_input
	movq	%rbp, %rdi
.LBB3_140:                              # %.critedge78
	callq	fclose
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_226:
	movslq	%ebp, %rbx
	movl	$.L.str.93, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_227
# BB#242:
	movl	$.L.str.101, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	strncmp
	cmpl	$2, %r13d
	jne	.LBB3_245
# BB#243:
	testl	%eax, %eax
	jne	.LBB3_245
# BB#244:
	addq	$48, %r12
.LBB3_206:                              # %.critedge78
	movq	120(%rsp), %r8
	xorl	%r14d, %r14d
	movl	$20, %edi
	movl	$.L.str.10, %edx
	movl	$19, %ecx
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	sqlite3_snprintf
	jmp	.LBB3_286
.LBB3_182:
	movl	$0, 24(%r12)
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_96:
	movl	$.L.str.53, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_97
# BB#98:                                # %.critedge648
	movl	$.L.str.54, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB3_284
# BB#99:
	cmpl	$1, %r13d
	jle	.LBB3_100
# BB#101:
	movq	120(%rsp), %rdi
	callq	booleanValue
	movl	888(%r12), %ecx
	cmpl	$1, %eax
	jne	.LBB3_105
# BB#102:
	testl	%ecx, %ecx
	jne	.LBB3_104
	jmp	.LBB3_103
.LBB3_173:
	movl	$.L.str.67, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	strncmp
	cmpl	$2, %r13d
	jl	.LBB3_284
# BB#174:
	testl	%eax, %eax
	jne	.LBB3_284
# BB#175:
	movq	$0, 32(%rsp)
	movq	%r12, %rdi
	callq	open_db
	leaq	520(%rsp), %rbx
	movl	$5408, %edx             # imm = 0x1520
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcpy
	movl	$0, 552(%rsp)
	movl	$2, 544(%rsp)
	movq	120(%rsp), %rax
	movq	%rax, zShellStatic(%rip)
	movq	(%r12), %rdi
	leaq	32(%rsp), %r8
	movl	$.L.str.68, %esi
	movl	$callback, %edx
	movq	%rbx, %rcx
	callq	sqlite3_exec
	movq	$0, zShellStatic(%rip)
	movq	32(%rsp), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_176
.LBB3_177:
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_112:
	cmpl	$2, %r13d
	jl	.LBB3_110
.LBB3_113:
	movq	120(%rsp), %rdi
	callq	booleanValue
	movl	%eax, 32(%r12)
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_227:
	movq	$0, 72(%rsp)
	movq	%r12, %rdi
	callq	open_db
	leaq	520(%rsp), %rdi
	movl	$5408, %edx             # imm = 0x1520
	movq	%r12, %rsi
	callq	memcpy
	movl	$0, 552(%rsp)
	movl	$3, 544(%rsp)
	cmpl	$2, %r13d
	jl	.LBB3_238
# BB#228:                               # %.preheader697
	movq	120(%rsp), %r14
	movb	(%r14), %bl
	testb	%bl, %bl
	je	.LBB3_229
# BB#230:                               # %.lr.ph747
	movl	$1, %ebp
	callq	__ctype_tolower_loc
.LBB3_231:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movsbq	%bl, %rdx
	movzbl	(%rcx,%rdx,4), %ecx
	movb	%cl, (%r14)
	movq	120(%rsp), %r15
	leaq	(%r15,%rbp), %r14
	movzbl	(%r15,%rbp), %ebx
	incq	%rbp
	testb	%bl, %bl
	jne	.LBB3_231
	jmp	.LBB3_232
.LBB3_212:
	movq	stdout(%rip), %rax
	movq	%rax, 16(%r12)
	addq	$1300, %r12             # imm = 0x514
	xorl	%r14d, %r14d
	movl	$4096, %edi             # imm = 0x1000
	movl	$.L.str.85, %edx
.LBB3_194:                              # %.critedge78
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	sqlite3_snprintf
	jmp	.LBB3_286
.LBB3_245:                              # %.critedge657
	movl	$.L.str.102, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB3_284
# BB#246:
	movq	16(%r12), %rdi
	cmpl	$0, 8(%r12)
	movl	$.L.str.104, %ebp
	movl	$.L.str.105, %ebx
	movl	$.L.str.105, %ecx
	cmovneq	%rbp, %rcx
	movl	$.L.str.103, %esi
	movl	$.L.str.52, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	16(%r12), %rdi
	cmpl	$0, 888(%r12)
	movl	$.L.str.105, %ecx
	cmovneq	%rbp, %rcx
	movl	$.L.str.103, %esi
	movl	$.L.str.54, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	16(%r12), %rdi
	cmpl	$0, 32(%r12)
	cmovneq	%rbp, %rbx
	movl	$.L.str.103, %esi
	movl	$.L.str.56, %edx
	xorl	%eax, %eax
	movq	%rbx, %rcx
	callq	fprintf
	movq	16(%r12), %rdi
	movslq	24(%r12), %rax
	movq	modeDescr(,%rax,8), %rcx
	movl	$.L.str.103, %esi
	movl	$.L.str.69, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	16(%r12), %rdi
	movl	$.L.str.106, %esi
	movl	$.L.str.83, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	16(%r12), %rdi
	leaq	868(%r12), %rsi
	callq	output_c_string
	movq	16(%r12), %rsi
	movl	$10, %edi
	callq	fputc
	movq	16(%r12), %rbx
	leaq	1300(%r12), %rbp
	movq	%rbp, %rdi
	callq	strlen
	testq	%rax, %rax
	movl	$.L.str.85, %ecx
	cmovneq	%rbp, %rcx
	movl	$.L.str.103, %esi
	movl	$.L.str.84, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	16(%r12), %rdi
	movl	$.L.str.106, %esi
	movl	$.L.str.101, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	16(%r12), %rdi
	leaq	48(%r12), %rsi
	callq	output_c_string
	movq	16(%r12), %rsi
	movl	$10, %edi
	callq	fputc
	movq	16(%r12), %rdi
	movl	$.L.str.106, %esi
	movl	$.L.str.108, %edx
	xorl	%eax, %eax
	callq	fprintf
	xorl	%ebx, %ebx
.LBB3_247:                              # =>This Inner Loop Header: Depth=1
	movl	68(%r12,%rbx,4), %edx
	testl	%edx, %edx
	movq	16(%r12), %rcx
	je	.LBB3_250
# BB#248:                               #   in Loop: Header=BB3_247 Depth=1
	movl	$.L.str.109, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	fprintf
	incq	%rbx
	cmpq	$100, %rbx
	jl	.LBB3_247
# BB#249:                               # %.thread687
	movq	16(%r12), %rcx
.LBB3_250:                              # %.loopexit698
	movl	$10, %edi
	movq	%rcx, %rsi
	callq	fputc
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_73:
	movq	stderr(%rip), %rdi
	movl	$.L.str.43, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	32(%rsp), %rdi
	callq	sqlite3_free
	jmp	.LBB3_74
.LBB3_185:
	movl	$1, 24(%r12)
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_97:
	movl	$2, %r14d
	jmp	.LBB3_286
.LBB3_238:
	movq	(%r12), %rdi
	leaq	520(%rsp), %rcx
	leaq	72(%rsp), %r8
	movl	$.L.str.100, %esi
	movl	$callback, %edx
	callq	sqlite3_exec
	jmp	.LBB3_239
.LBB3_100:                              # %.thread675
	cmpl	$0, 888(%r12)
	jne	.LBB3_104
.LBB3_103:
	movl	$1, 888(%r12)
	movl	24(%r12), %eax
	movl	%eax, 892(%r12)
	movl	32(%r12), %eax
	movl	%eax, 896(%r12)
	leaq	900(%r12), %rdi
	leaq	68(%r12), %rsi
	movl	$400, %edx              # imm = 0x190
	callq	memcpy
.LBB3_104:
	movl	$9, 24(%r12)
	movl	$1, 32(%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 148(%r12)
	movups	%xmm0, 132(%r12)
	movups	%xmm0, 116(%r12)
	movups	%xmm0, 100(%r12)
	movl	$0, 164(%r12)
	movaps	.LCPI3_0(%rip), %xmm0   # xmm0 = [4,13,4,4]
	movups	%xmm0, 68(%r12)
	movdqa	.LCPI3_1(%rip), %xmm0   # xmm0 = [4,13,2,13]
	movdqu	%xmm0, 84(%r12)
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_256:
	movq	120(%rsp), %rax
	movq	%rax, zShellStatic(%rip)
	leaq	520(%rsp), %rdx
	leaq	80(%rsp), %rcx
	leaq	32(%rsp), %r9
	movl	$.L.str.112, %esi
	xorl	%r8d, %r8d
	callq	sqlite3_get_table
	movl	%eax, %ebx
	movq	$0, zShellStatic(%rip)
.LBB3_257:
	movq	32(%rsp), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_258
# BB#259:
	testl	%ebx, %ebx
	jne	.LBB3_273
.LBB3_260:                              # %.preheader695
	movslq	80(%rsp), %rsi
	testq	%rsi, %rsi
	jle	.LBB3_261
# BB#262:                               # %.lr.ph731
	xorl	%ebx, %ebx
	movq	520(%rsp), %r14
	xorl	%r13d, %r13d
.LBB3_263:                              # =>This Inner Loop Header: Depth=1
	movq	8(%r14,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB3_265
# BB#264:                               #   in Loop: Header=BB3_263 Depth=1
	movq	%rsi, %rbp
	callq	strlen
	movq	%rbp, %rsi
	cmpl	%r13d, %eax
	cmovgel	%eax, %r13d
.LBB3_265:                              #   in Loop: Header=BB3_263 Depth=1
	incq	%rbx
	cmpq	%rsi, %rbx
	jl	.LBB3_263
	jmp	.LBB3_266
.LBB3_105:
	xorl	%r14d, %r14d
	testl	%ecx, %ecx
	je	.LBB3_286
# BB#106:
	movl	$0, 888(%r12)
	movl	892(%r12), %eax
	movl	%eax, 24(%r12)
	movl	896(%r12), %eax
	movl	%eax, 32(%r12)
	leaq	68(%r12), %rdi
	addq	$900, %r12              # imm = 0x384
	movl	$400, %edx              # imm = 0x190
	movq	%r12, %rsi
	callq	memcpy
	jmp	.LBB3_286
.LBB3_229:
	movq	%r14, %r15
.LBB3_232:                              # %._crit_edge748
	movl	$.L.str.94, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_233
# BB#235:
	movl	$.L.str.97, %esi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_236
# BB#237:
	movq	%r15, zShellStatic(%rip)
	movq	(%r12), %rdi
	leaq	520(%rsp), %rcx
	leaq	72(%rsp), %r8
	movl	$.L.str.99, %esi
	movl	$callback, %edx
	callq	sqlite3_exec
	movq	$0, zShellStatic(%rip)
	jmp	.LBB3_239
.LBB3_233:
	movl	$.L.str.95, %eax
	jmp	.LBB3_234
.LBB3_110:
	movl	$.L.str.57, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB3_284
# BB#111:
	movq	stderr(%rip), %rdi
	xorl	%r14d, %r14d
	movl	$zHelp, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB3_286
.LBB3_284:                              # %.thread690.thread
	movq	stderr(%rip), %rdi
	xorl	%r14d, %r14d
	movl	$.L.str.118, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
.LBB3_285:                              # %.critedge78
	callq	fprintf
.LBB3_286:                              # %.critedge78
	movl	%r14d, %eax
	addq	$5928, %rsp             # imm = 0x1728
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_224:
	movq	stderr(%rip), %rdi
	xorl	%r14d, %r14d
	movl	$.L.str.92, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	jmp	.LBB3_285
.LBB3_187:
	movl	$2, 24(%r12)
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_236:
	movl	$.L.str.98, %eax
.LBB3_234:
	movd	%rax, %xmm0
	movdqa	%xmm0, 32(%rsp)
	movl	$.L.str.96, %eax
	movd	%rax, %xmm0
	movdqa	%xmm0, 80(%rsp)
	leaq	520(%rsp), %rdi
	leaq	32(%rsp), %rdx
	leaq	80(%rsp), %rcx
	movl	$1, %esi
	callq	callback
.LBB3_239:
	movq	72(%rsp), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_240
.LBB3_241:
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_214:
	movq	stderr(%rip), %rdi
	xorl	%r14d, %r14d
	movl	$.L.str.87, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	fprintf
	movq	stdout(%rip), %rax
	movq	%rax, 16(%r12)
	jmp	.LBB3_286
.LBB3_189:
	movl	$4, 24(%r12)
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_117:
	movq	stderr(%rip), %rcx
	movl	$.L.str.59, %edi
	movl	$39, %esi
	jmp	.LBB3_90
.LBB3_176:
	movq	stderr(%rip), %rdi
	movl	$.L.str.43, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	32(%rsp), %rdi
	callq	sqlite3_free
	jmp	.LBB3_177
.LBB3_261:
	xorl	%r13d, %r13d
.LBB3_266:                              # %._crit_edge732
	leal	2(%r13), %ecx
	movl	$80, %eax
	xorl	%edx, %edx
	idivl	%ecx
	testl	%eax, %eax
	movl	$1, %ecx
	cmovgl	%eax, %ecx
	leal	-1(%rsi,%rcx), %eax
	cltd
	idivl	%ecx
	movl	%eax, 8(%rsp)           # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB3_273
# BB#267:                               # %.lr.ph728.preheader
	movslq	8(%rsp), %rbp           # 4-byte Folded Reload
	movl	$1, %r12d
	movl	$.L.str.113, %r15d
	xorl	%r14d, %r14d
	jmp	.LBB3_268
.LBB3_272:                              # %._crit_edge..lr.ph728_crit_edge
                                        #   in Loop: Header=BB3_268 Depth=1
	incq	%r12
	movl	80(%rsp), %esi
.LBB3_268:                              # %.lr.ph728
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_270 Depth 2
	movl	%r14d, %eax
	cmpl	%esi, %eax
	leal	1(%rax), %r14d
	jge	.LBB3_271
# BB#269:                               # %.lr.ph725.preheader
                                        #   in Loop: Header=BB3_268 Depth=1
	movq	%r12, %rbx
.LBB3_270:                              # %.lr.ph725
                                        #   Parent Loop BB3_268 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbp, %rbx
	movl	$.L.str.114, %esi
	cmovleq	%r15, %rsi
	movq	520(%rsp), %rax
	movq	(%rax,%rbx,8), %rcx
	testq	%rcx, %rcx
	cmoveq	%r15, %rcx
	movl	$.L.str.115, %edi
	xorl	%eax, %eax
	movl	%r13d, %edx
	callq	printf
	addq	%rbp, %rbx
	movslq	80(%rsp), %rax
	cmpq	%rax, %rbx
	jle	.LBB3_270
.LBB3_271:                              # %._crit_edge
                                        #   in Loop: Header=BB3_268 Depth=1
	movl	$10, %edi
	callq	putchar
	cmpl	8(%rsp), %r14d          # 4-byte Folded Reload
	jne	.LBB3_272
.LBB3_273:                              # %.loopexit694
	movq	520(%rsp), %rdi
	callq	sqlite3_free_table
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_240:
	movq	stderr(%rip), %rdi
	movl	$.L.str.43, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	72(%rsp), %rdi
	callq	sqlite3_free
	jmp	.LBB3_241
.LBB3_191:
	movl	$6, 24(%r12)
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_125:
	movl	$1, %ebp
	movq	%r9, %rax
.LBB3_129:                              # %.lr.ph742.prol.loopexit
	leaq	(%r9,%rsi,2), %r8
	leaq	2(%r9,%rsi,2), %r9
	cmpl	$3, %esi
	jb	.LBB3_132
# BB#130:                               # %.lr.ph742.preheader.new
	leal	9(%rax), %edi
	leaq	8(%r13,%rax), %rbx
	shlq	$32, %rax
	movq	8(%rsp), %rsi           # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	subl	%ebp, %esi
	movabsq	$12884901888, %r10      # imm = 0x300000000
	movabsq	$21474836480, %r11      # imm = 0x500000000
	movabsq	$30064771072, %r14      # imm = 0x700000000
	movabsq	$34359738368, %r12      # imm = 0x800000000
.LBB3_131:                              # %.lr.ph742
                                        # =>This Inner Loop Header: Depth=1
	movl	%edi, %ebp
	movb	$44, (%rdx)
	movslq	%ecx, %rcx
	movb	$63, (%r13,%rcx)
	movb	$44, -6(%rbx)
	leaq	(%rax,%r10), %rcx
	sarq	$32, %rcx
	movb	$63, (%r13,%rcx)
	movb	$44, -4(%rbx)
	leaq	(%rax,%r11), %rcx
	sarq	$32, %rcx
	movb	$63, (%r13,%rcx)
	movb	$44, -2(%rbx)
	leaq	(%rax,%r14), %rcx
	sarq	$32, %rcx
	movb	$63, (%r13,%rcx)
	movl	%ebp, %ecx
	leal	8(%rbp), %edi
	addq	%r12, %rax
	movq	%rbx, %rdx
	leaq	8(%rbx), %rbx
	addl	$-4, %esi
	jne	.LBB3_131
.LBB3_132:                              # %._crit_edge743.loopexit
	addl	$3, %r8d
	movq	%r13, %rdx
	addq	%r9, %rdx
	movl	%r8d, %ecx
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	24(%rsp), %r12          # 8-byte Reload
.LBB3_133:                              # %._crit_edge743
	movb	$41, (%rdx)
	movslq	%ecx, %rax
	movb	$0, (%r13,%rax)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	leaq	520(%rsp), %rcx
	movl	$-1, %edx
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	callq	sqlite3_prepare
	movl	%eax, %ebp
	movq	%r13, %rdi
	callq	free
	testl	%ebp, %ebp
	jne	.LBB3_134
# BB#135:
	movl	$.L.str.35, %esi
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	fopen
	movq	%rax, 64(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB3_136
# BB#138:
	movslq	%ebx, %rax
	leaq	8(,%rax,8), %rdi
	callq	malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	movq	16(%rsp), %rax          # 8-byte Reload
	je	.LBB3_139
# BB#141:
	movq	(%rax), %rdi
	movl	$.L.str.63, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	callq	sqlite3_exec
	movq	%r12, %rcx
	shlq	$32, %rcx
	movslq	%r12d, %r14
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	addq	%rax, %rcx
	sarq	$32, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	testl	%ebx, %ebx
	jle	.LBB3_142
# BB#152:                               # %.split.us.preheader
	movl	%ebx, %r13d
	xorl	%ebx, %ebx
.LBB3_153:                              # %.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_156 Depth 2
                                        #     Child Loop BB3_160 Depth 2
	xorl	%edi, %edi
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	local_getline
	testq	%rax, %rax
	je	.LBB3_154
# BB#155:                               #   in Loop: Header=BB3_153 Depth=1
	incl	%ebx
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movq	%rax, (%rbp)
	xorl	%ebx, %ebx
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	%rax, %r12
	jmp	.LBB3_156
.LBB3_166:                              #   in Loop: Header=BB3_156 Depth=2
	incq	%r12
.LBB3_156:                              #   Parent Loop BB3_153 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r12), %eax
	cmpq	$13, %rax
	ja	.LBB3_162
# BB#157:                               #   in Loop: Header=BB3_156 Depth=2
	movl	$9217, %ecx             # imm = 0x2401
	btq	%rax, %rcx
	jb	.LBB3_158
.LBB3_162:                              # %.critedge653.us
                                        #   in Loop: Header=BB3_156 Depth=2
	cmpb	(%r15), %al
	jne	.LBB3_166
# BB#163:                               #   in Loop: Header=BB3_156 Depth=2
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB3_166
# BB#164:                               #   in Loop: Header=BB3_156 Depth=2
	movb	$0, (%r12)
	incl	%ebx
	cmpl	8(%rsp), %ebx           # 4-byte Folded Reload
	jge	.LBB3_166
# BB#165:                               #   in Loop: Header=BB3_156 Depth=2
	leaq	(%r12,%r14), %rax
	movslq	%ebx, %rcx
	movq	%rax, (%rbp,%rcx,8)
	addq	24(%rsp), %r12          # 8-byte Folded Reload
	jmp	.LBB3_166
.LBB3_158:                              # %.critedge652.us
                                        #   in Loop: Header=BB3_153 Depth=1
	movb	$0, (%r12)
	incl	%ebx
	movq	8(%rsp), %r8            # 8-byte Reload
	cmpl	%r8d, %ebx
	jne	.LBB3_170
# BB#159:                               # %.preheader696.us
                                        #   in Loop: Header=BB3_153 Depth=1
	movq	520(%rsp), %rdi
	xorl	%ebx, %ebx
.LBB3_160:                              #   Parent Loop BB3_153 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp,%rbx,8), %rdx
	incq	%rbx
	movl	$-1, %ecx
	xorl	%r8d, %r8d
	movl	%ebx, %esi
	callq	sqlite3_bind_text
	movq	520(%rsp), %rdi
	cmpq	%rbx, %r13
	jne	.LBB3_160
# BB#161:                               # %._crit_edge736.us
                                        #   in Loop: Header=BB3_153 Depth=1
	callq	sqlite3_step
	movq	520(%rsp), %rdi
	callq	sqlite3_reset
	movl	%eax, %ebx
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	free
	testl	%ebx, %ebx
	movq	56(%rsp), %rbx          # 8-byte Reload
	je	.LBB3_153
	jmp	.LBB3_169
.LBB3_139:
	movq	64(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB3_140
.LBB3_142:                              # %.split.preheader
	movl	$9217, %r13d            # imm = 0x2401
	xorl	%ebx, %ebx
.LBB3_143:                              # %.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_145 Depth 2
	xorl	%edi, %edi
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	local_getline
	testq	%rax, %rax
	je	.LBB3_154
# BB#144:                               #   in Loop: Header=BB3_143 Depth=1
	incl	%ebx
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movq	%rax, (%rbp)
	xorl	%ebx, %ebx
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	%rax, %r12
	jmp	.LBB3_145
.LBB3_151:                              #   in Loop: Header=BB3_145 Depth=2
	incq	%r12
.LBB3_145:                              #   Parent Loop BB3_143 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r12), %eax
	cmpq	$13, %rax
	ja	.LBB3_147
# BB#146:                               #   in Loop: Header=BB3_145 Depth=2
	btq	%rax, %r13
	jb	.LBB3_167
.LBB3_147:                              # %.critedge653
                                        #   in Loop: Header=BB3_145 Depth=2
	cmpb	(%r15), %al
	jne	.LBB3_151
# BB#148:                               #   in Loop: Header=BB3_145 Depth=2
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB3_151
# BB#149:                               #   in Loop: Header=BB3_145 Depth=2
	movb	$0, (%r12)
	incl	%ebx
	cmpl	8(%rsp), %ebx           # 4-byte Folded Reload
	jge	.LBB3_151
# BB#150:                               #   in Loop: Header=BB3_145 Depth=2
	leaq	(%r12,%r14), %rax
	movslq	%ebx, %rcx
	movq	%rax, (%rbp,%rcx,8)
	addq	24(%rsp), %r12          # 8-byte Folded Reload
	jmp	.LBB3_151
.LBB3_167:                              # %.critedge652
                                        #   in Loop: Header=BB3_143 Depth=1
	movb	$0, (%r12)
	incl	%ebx
	movq	8(%rsp), %r8            # 8-byte Reload
	cmpl	%r8d, %ebx
	jne	.LBB3_170
# BB#168:                               # %.preheader696
                                        #   in Loop: Header=BB3_143 Depth=1
	movq	520(%rsp), %rdi
	callq	sqlite3_step
	movq	520(%rsp), %rdi
	callq	sqlite3_reset
	movl	%eax, %ebx
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	free
	testl	%ebx, %ebx
	movq	56(%rsp), %rbx          # 8-byte Reload
	je	.LBB3_143
.LBB3_169:                              # %.us-lcssa738.us
	movq	stderr(%rip), %rbx
	movq	db(%rip), %rdi
	callq	sqlite3_errmsg
	movq	%rax, %rcx
	movl	$.L.str.43, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	fprintf
	jmp	.LBB3_171
.LBB3_170:                              # %.us-lcssa.us
	movq	stderr(%rip), %rdi
	movl	$.L.str.65, %esi
	xorl	%eax, %eax
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%ebx, %r9d
	callq	fprintf
.LBB3_171:                              # %.thread677
	movl	$.L.str.66, %r15d
	movq	16(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB3_172
.LBB3_120:                              # %.thread676
	movq	stderr(%rip), %rbx
	movq	db(%rip), %rdi
	callq	sqlite3_errmsg
	movq	%rax, %rcx
	movl	$.L.str.43, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	jmp	.LBB3_137
.LBB3_193:
	movl	$7, 24(%r12)
	addq	$48, %r12
	xorl	%r14d, %r14d
	movl	$20, %edi
	movl	$.L.str.9, %edx
	jmp	.LBB3_194
.LBB3_154:
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	$.L.str.64, %r15d
.LBB3_172:                              # %.thread677
	movq	%rbp, %rdi
	callq	free
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	movq	520(%rsp), %rdi
	callq	sqlite3_finalize
	movq	(%rbx), %rdi
	xorl	%r14d, %r14d
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	callq	sqlite3_exec
	jmp	.LBB3_286
.LBB3_258:
	movq	stderr(%rip), %rdi
	movl	$.L.str.43, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	32(%rsp), %rdi
	callq	sqlite3_free
	testl	%ebx, %ebx
	jne	.LBB3_273
	jmp	.LBB3_260
.LBB3_196:
	movl	$2, 24(%r12)
	addq	$48, %r12
	xorl	%r14d, %r14d
	movl	$20, %edi
	movl	$.L.str.79, %edx
	jmp	.LBB3_194
.LBB3_200:
	movl	$.L.str.81, %esi
.LBB3_201:                              # %.critedge78
	movq	%r12, %rdi
	callq	set_table_name
	xorl	%r14d, %r14d
	jmp	.LBB3_286
.LBB3_134:
	movq	stderr(%rip), %rbx
	movq	db(%rip), %rdi
	callq	sqlite3_errmsg
	movq	%rax, %rcx
	movl	$.L.str.43, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	fprintf
	movq	520(%rsp), %rdi
	callq	sqlite3_finalize
	movl	$1, %r14d
	jmp	.LBB3_286
.LBB3_136:
	movq	stderr(%rip), %rdi
	xorl	%r14d, %r14d
	movl	$.L.str.62, %esi
	xorl	%eax, %eax
	movq	48(%rsp), %rdx          # 8-byte Reload
.LBB3_137:                              # %.thread679
	callq	fprintf
	movq	520(%rsp), %rdi
	callq	sqlite3_finalize
	jmp	.LBB3_286
.LBB3_202:
	movq	stderr(%rip), %rcx
	movl	$.L.str.82, %edi
	movl	$65, %esi
	jmp	.LBB3_90
.Lfunc_end3:
	.size	do_meta_command, .Lfunc_end3-do_meta_command
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_93
	.quad	.LBB3_251
	.quad	.LBB3_251
	.quad	.LBB3_107
	.quad	.LBB3_114
	.quad	.LBB3_251
	.quad	.LBB3_251
	.quad	.LBB3_251
	.quad	.LBB3_178
	.quad	.LBB3_203
	.quad	.LBB3_207
	.quad	.LBB3_216
	.quad	.LBB3_283
	.quad	.LBB3_221
	.quad	.LBB3_226

	.text
	.p2align	4, 0x90
	.type	callback,@function
callback:                               # @callback
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi40:
	.cfi_def_cfa_offset 96
.Lcfi41:
	.cfi_offset %rbx, -56
.Lcfi42:
	.cfi_offset %r12, -48
.Lcfi43:
	.cfi_offset %r13, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%esi, %ebx
	movq	%rdi, (%rsp)            # 8-byte Spill
	movl	24(%rdi), %eax
	cmpq	$9, %rax
	ja	.LBB4_158
# BB#1:
	movabsq	$4294967296, %r12       # imm = 0x100000000
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_12:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	12(%rdx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 12(%rdx)
	testl	%eax, %eax
	je	.LBB4_13
.LBB4_37:                               # %.loopexit285
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	je	.LBB4_158
# BB#38:                                # %.loopexit285
	testl	%ecx, %ecx
	jle	.LBB4_158
# BB#39:                                # %.lr.ph309
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	868(%rax), %r14
	leal	-1(%rcx), %r15d
	movl	%ecx, %r12d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_40:                               # =>This Inner Loop Header: Depth=1
	movl	$10, %ebx
	cmpq	$99, %r13
	ja	.LBB4_42
# BB#41:                                #   in Loop: Header=BB4_40 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	468(%rax,%r13,4), %ebx
.LBB4_42:                               #   in Loop: Header=BB4_40 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%r13,8), %rbp
	movq	(%rsp), %rax            # 8-byte Reload
	cmpl	$9, 24(%rax)
	jne	.LBB4_46
# BB#43:                                #   in Loop: Header=BB4_40 Depth=1
	testq	%rbp, %rbp
	je	.LBB4_44
# BB#45:                                #   in Loop: Header=BB4_40 Depth=1
	movq	%rbp, %rdi
	callq	strlen
	movslq	%ebx, %rbx
	cmpq	%rbx, %rax
	cmoval	%eax, %ebx
	jmp	.LBB4_46
.LBB4_44:                               #   in Loop: Header=BB4_40 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_46:                               # %._crit_edge421
                                        #   in Loop: Header=BB4_40 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	16(%rax), %rdi
	testq	%rbp, %rbp
	cmoveq	%r14, %rbp
	cmpq	%r13, %r15
	movl	$.L.str.114, %r9d
	movl	$.L.str.107, %eax
	cmoveq	%rax, %r9
	movl	$.L.str.142, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	movl	%ebx, %ecx
	movq	%rbp, %r8
	callq	fprintf
	incq	%r13
	cmpq	%r13, %r12
	jne	.LBB4_40
	jmp	.LBB4_158
.LBB4_47:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	(%rsp), %r13            # 8-byte Reload
	movl	12(%r13), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 12(%r13)
	testl	%eax, %eax
	jne	.LBB4_52
# BB#48:
	cmpl	$0, 32(%r13)
	je	.LBB4_52
# BB#49:
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_52
# BB#50:                                # %.lr.ph318
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %ebp
	leaq	48(%r13), %r14
	movl	%eax, %ebx
	movl	$.L.str.107, %r12d
	.p2align	4, 0x90
.LBB4_51:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r13), %rdi
	movq	(%r15), %rdx
	testq	%rbp, %rbp
	movq	%r14, %rcx
	cmoveq	%r12, %rcx
	movl	$.L.str.144, %esi
	xorl	%eax, %eax
	callq	fprintf
	addq	$8, %r15
	decq	%rbp
	decq	%rbx
	jne	.LBB4_51
.LBB4_52:                               # %.loopexit290
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	je	.LBB4_158
# BB#53:                                # %.loopexit290
	testl	%edx, %edx
	jle	.LBB4_158
# BB#54:                                # %.lr.ph316
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	868(%rcx), %r15
	leal	-1(%rdx), %eax
	leaq	48(%rcx), %r14
	movslq	%eax, %r12
	movl	%edx, %r13d
	xorl	%ebx, %ebx
	movq	(%rsp), %rbp            # 8-byte Reload
	.p2align	4, 0x90
.LBB4_55:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	cmoveq	%r15, %rdi
	movq	16(%rbp), %rsi
	callq	fputs
	cmpq	%r12, %rbx
	jge	.LBB4_57
# BB#56:                                #   in Loop: Header=BB4_55 Depth=1
	movq	16(%rbp), %rsi
	movq	%r14, %rdi
	callq	fputs
	jmp	.LBB4_60
	.p2align	4, 0x90
.LBB4_57:                               #   in Loop: Header=BB4_55 Depth=1
	movq	16(%rbp), %rax
	cmpl	$3, 24(%rbp)
	jne	.LBB4_59
# BB#58:                                #   in Loop: Header=BB4_55 Depth=1
	movl	$.L.str.145, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	jmp	.LBB4_60
	.p2align	4, 0x90
.LBB4_59:                               #   in Loop: Header=BB4_55 Depth=1
	movl	$10, %edi
	movq	%rax, %rsi
	callq	fputc
.LBB4_60:                               #   in Loop: Header=BB4_55 Depth=1
	incq	%rbx
	cmpq	%rbx, %r13
	jne	.LBB4_55
	jmp	.LBB4_158
.LBB4_2:
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB4_158
# BB#3:                                 # %.preheader281
	testl	%ebx, %ebx
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	jle	.LBB4_4
# BB#5:                                 # %.lr.ph307.preheader
	movl	%ebx, %ebx
	movl	$5, %r12d
	movl	$.L.str.113, %r14d
	movq	%r15, %rbp
	movq	(%rsp), %r13            # 8-byte Reload
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph307
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	cmoveq	%r14, %rdi
	callq	strlen
	cmpl	%r12d, %eax
	cmovgel	%eax, %r12d
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB4_6
	jmp	.LBB4_7
.LBB4_61:
	movq	(%rsp), %rbp            # 8-byte Reload
	movl	12(%rbp), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 12(%rbp)
	testl	%eax, %eax
	movq	8(%rsp), %r14           # 8-byte Reload
	jne	.LBB4_68
# BB#62:
	cmpl	$0, 32(%rbp)
	je	.LBB4_68
# BB#63:
	movq	16(%rbp), %rcx
	movl	$.L.str.146, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	movq	16(%rbp), %rcx
	testl	%ebx, %ebx
	jle	.LBB4_64
# BB#65:                                # %.lr.ph326.preheader
	movq	%rbx, %r13
	movl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_66:                               # %.lr.ph326
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rdx
	movl	$.L.str.147, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	fprintf
	movq	16(%rbp), %rcx
	addq	$8, %r15
	decq	%rbx
	jne	.LBB4_66
	jmp	.LBB4_67
.LBB4_108:
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB4_158
# BB#109:
	movq	(%rsp), %r13            # 8-byte Reload
	movq	16(%r13), %rdi
	movq	40(%r13), %rdx
	movl	$.L.str.151, %esi
	xorl	%eax, %eax
	callq	fprintf
	testl	%ebx, %ebx
	jle	.LBB4_156
# BB#110:                               # %.lr.ph348.preheader
	movl	%ebx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	jmp	.LBB4_111
.LBB4_86:
	movq	(%rsp), %rbp            # 8-byte Reload
	movl	12(%rbp), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 12(%rbp)
	testl	%eax, %eax
	movq	8(%rsp), %r13           # 8-byte Reload
	jne	.LBB4_92
# BB#87:
	cmpl	$0, 32(%rbp)
	je	.LBB4_92
# BB#88:                                # %.preheader292
	movq	16(%rbp), %rax
	testl	%ebx, %ebx
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	jle	.LBB4_91
# BB#89:                                # %.lr.ph334
	leaq	48(%rbp), %r12
	movl	%ebx, %ebx
	movl	$.L.str.113, %r14d
	.p2align	4, 0x90
.LBB4_90:                               # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	cmoveq	%r14, %rsi
	movq	%rax, %rdi
	callq	output_c_string
	movq	16(%rbp), %rsi
	movq	%r12, %rdi
	callq	fputs
	movq	16(%rbp), %rax
	addq	$8, %r15
	decq	%rbx
	jne	.LBB4_90
.LBB4_91:                               # %._crit_edge335
	movl	$10, %edi
	movq	%rax, %rsi
	callq	fputc
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB4_92:
	testq	%r13, %r13
	je	.LBB4_158
# BB#93:                                # %.preheader291
	movq	(%rsp), %r12            # 8-byte Reload
	movq	16(%r12), %rax
	testl	%ebx, %ebx
	jle	.LBB4_96
# BB#94:                                # %.lr.ph330
	leaq	868(%r12), %r14
	leaq	48(%r12), %r15
	movl	%ebx, %ebp
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB4_95:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	cmoveq	%r14, %rsi
	movq	%rax, %rdi
	callq	output_c_string
	movq	16(%r12), %rsi
	movq	%r15, %rdi
	callq	fputs
	movq	16(%r12), %rax
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB4_95
.LBB4_96:                               # %._crit_edge331
	movl	$10, %edi
	movq	%rax, %rsi
	callq	fputc
	jmp	.LBB4_158
.LBB4_97:
	movq	(%rsp), %rbp            # 8-byte Reload
	movl	12(%rbp), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 12(%rbp)
	testl	%eax, %eax
	jne	.LBB4_103
# BB#98:
	cmpl	$0, 32(%rbp)
	je	.LBB4_103
# BB#99:                                # %.preheader294
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	testl	%ebx, %ebx
	jle	.LBB4_102
# BB#100:                               # %.lr.ph341
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	-1(%rcx), %eax
	movslq	%eax, %r14
	movl	%ecx, %r13d
	xorl	%ebx, %ebx
	movl	$.L.str.113, %r12d
	.p2align	4, 0x90
.LBB4_101:                              # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbx,8), %rsi
	testq	%rsi, %rsi
	cmoveq	%r12, %rsi
	xorl	%edx, %edx
	cmpq	%r14, %rbx
	setl	%dl
	movq	%rbp, %rdi
	callq	output_csv
	incq	%rbx
	cmpq	%rbx, %r13
	jne	.LBB4_101
.LBB4_102:                              # %._crit_edge342
	movq	16(%rbp), %rsi
	movl	$10, %edi
	callq	fputc
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB4_103:
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB4_158
# BB#104:                               # %.preheader293
	testl	%ebx, %ebx
	movq	(%rsp), %r15            # 8-byte Reload
	movq	8(%rsp), %r12           # 8-byte Reload
	jle	.LBB4_107
# BB#105:                               # %.lr.ph338
	leal	-1(%rbx), %eax
	movslq	%eax, %r14
	movl	%ebx, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_106:                              # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbx,8), %rsi
	xorl	%edx, %edx
	cmpq	%r14, %rbx
	setl	%dl
	movq	%r15, %rdi
	callq	output_csv
	incq	%rbx
	cmpq	%rbx, %rbp
	jne	.LBB4_106
.LBB4_107:                              # %._crit_edge339
	movq	16(%r15), %rsi
	movl	$10, %edi
	callq	fputc
	jmp	.LBB4_158
.LBB4_13:                               # %.preheader286
	movq	16(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	jle	.LBB4_158
# BB#14:                                # %.lr.ph313
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	868(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leal	-1(%rcx), %r12d
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movl	%ecx, %r13d
	movl	$10, %ebx
	je	.LBB4_23
# BB#15:                                # %.lr.ph313.split.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_16:                               # %.lr.ph313.split
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$99, %r14
	ja	.LBB4_18
# BB#17:                                #   in Loop: Header=BB4_16 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	68(%rax,%r14,4), %ebp
	testl	%ebp, %ebp
	jg	.LBB4_19
.LBB4_18:                               # %.thread
                                        #   in Loop: Header=BB4_16 Depth=1
	movq	(%r15,%r14,8), %rdi
	testq	%rdi, %rdi
	movl	$.L.str.113, %eax
	cmoveq	%rax, %rdi
	callq	strlen
	movq	%rax, %rbp
	cmpl	$9, %ebp
	cmovlel	%ebx, %ebp
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%r14,8), %rdi
	testq	%rdi, %rdi
	cmoveq	24(%rsp), %rdi          # 8-byte Folded Reload
	callq	strlen
	cmpl	%eax, %ebp
	cmovll	%eax, %ebp
	cmpq	$99, %r14
	ja	.LBB4_20
.LBB4_19:                               # %.thread278
                                        #   in Loop: Header=BB4_16 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%ebp, 468(%rax,%r14,4)
.LBB4_20:                               #   in Loop: Header=BB4_16 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	cmpl	$0, 32(%rax)
	je	.LBB4_22
# BB#21:                                #   in Loop: Header=BB4_16 Depth=1
	movq	16(%rax), %rdi
	movq	(%r15,%r14,8), %r8
	cmpq	%r14, %r12
	movl	$.L.str.114, %r9d
	movl	$.L.str.107, %eax
	cmoveq	%rax, %r9
	movl	$.L.str.142, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	movl	%ebp, %ecx
	callq	fprintf
.LBB4_22:                               #   in Loop: Header=BB4_16 Depth=1
	incq	%r14
	cmpq	%r14, %r13
	jne	.LBB4_16
	jmp	.LBB4_31
.LBB4_132:                              #   in Loop: Header=BB4_111 Depth=1
	movq	16(%r13), %rdi
	movl	$.L.str.144, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	movq	%rbx, %rcx
	callq	fprintf
	jmp	.LBB4_155
	.p2align	4, 0x90
.LBB4_111:                              # %.lr.ph348
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_118 Depth 2
                                        #     Child Loop BB4_122 Depth 2
                                        #     Child Loop BB4_130 Depth 2
                                        #     Child Loop BB4_136 Depth 2
                                        #     Child Loop BB4_144 Depth 2
                                        #       Child Loop BB4_146 Depth 3
	testq	%r14, %r14
	movl	$.L.str.113, %r15d
	movl	$.L.str.9, %eax
	cmovgq	%rax, %r15
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%r14,8), %rbx
	testq	%rbx, %rbx
	je	.LBB4_112
# BB#113:                               #   in Loop: Header=BB4_111 Depth=1
	movb	(%rbx), %r13b
	cmpb	$45, %r13b
	je	.LBB4_115
# BB#114:                               #   in Loop: Header=BB4_111 Depth=1
	cmpb	$43, %r13b
	movq	%rbx, %rbp
	jne	.LBB4_116
.LBB4_115:                              #   in Loop: Header=BB4_111 Depth=1
	leaq	1(%rbx), %rbp
	movb	1(%rbx), %r13b
.LBB4_116:                              #   in Loop: Header=BB4_111 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rdi
	movsbq	%r13b, %rcx
	testb	$8, 1(%rdi,%rcx,2)
	movq	(%rsp), %r13            # 8-byte Reload
	je	.LBB4_133
# BB#117:                               # %.preheader3.i.preheader
                                        #   in Loop: Header=BB4_111 Depth=1
	addq	$2, %rbp
	.p2align	4, 0x90
.LBB4_118:                              # %.preheader3.i
                                        #   Parent Loop BB4_111 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rcx
	movsbq	-1(%rcx), %rdx
	leaq	1(%rcx), %rbp
	testb	$8, 1(%rdi,%rdx,2)
	jne	.LBB4_118
# BB#119:                               #   in Loop: Header=BB4_111 Depth=1
	cmpb	$46, %dl
	jne	.LBB4_120
# BB#121:                               #   in Loop: Header=BB4_111 Depth=1
	movsbq	-1(%rbp), %rdx
	testb	$8, 1(%rdi,%rdx,2)
	je	.LBB4_133
	.p2align	4, 0x90
.LBB4_122:                              # %._crit_edge.i
                                        #   Parent Loop BB4_111 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	1(%rcx), %rdx
	incq	%rcx
	testb	$8, 1(%rdi,%rdx,2)
	jne	.LBB4_122
	jmp	.LBB4_123
	.p2align	4, 0x90
.LBB4_112:                              #   in Loop: Header=BB4_111 Depth=1
	movq	16(%r13), %rdi
	movl	$.L.str.152, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	callq	fprintf
	jmp	.LBB4_155
.LBB4_120:                              #   in Loop: Header=BB4_111 Depth=1
	addq	$-2, %rbp
	movq	%rbp, %rcx
.LBB4_123:                              # %.loopexit2.i
                                        #   in Loop: Header=BB4_111 Depth=1
	movl	%edx, %eax
	orb	$32, %al
	cmpb	$101, %al
	jne	.LBB4_131
# BB#124:                               #   in Loop: Header=BB4_111 Depth=1
	movb	1(%rcx), %dl
	cmpb	$45, %dl
	je	.LBB4_127
# BB#125:                               #   in Loop: Header=BB4_111 Depth=1
	cmpb	$43, %dl
	jne	.LBB4_126
.LBB4_127:                              #   in Loop: Header=BB4_111 Depth=1
	movb	2(%rcx), %dl
	movl	$2, %esi
	jmp	.LBB4_128
.LBB4_126:                              #   in Loop: Header=BB4_111 Depth=1
	movl	$1, %esi
.LBB4_128:                              #   in Loop: Header=BB4_111 Depth=1
	movsbq	%dl, %rax
	testb	$8, 1(%rdi,%rax,2)
	je	.LBB4_133
# BB#129:                               # %._crit_edge9.i.preheader
                                        #   in Loop: Header=BB4_111 Depth=1
	leaq	1(%rcx,%rsi), %rcx
	.p2align	4, 0x90
.LBB4_130:                              # %._crit_edge9.i
                                        #   Parent Loop BB4_111 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	(%rcx), %rdx
	incq	%rcx
	testb	$8, 1(%rdi,%rdx,2)
	jne	.LBB4_130
.LBB4_131:                              # %isNumber.exit
                                        #   in Loop: Header=BB4_111 Depth=1
	testb	%dl, %dl
	je	.LBB4_132
	.p2align	4, 0x90
.LBB4_133:                              # %isNumber.exit.thread
                                        #   in Loop: Header=BB4_111 Depth=1
	cmpb	$0, (%r15)
	je	.LBB4_135
# BB#134:                               #   in Loop: Header=BB4_111 Depth=1
	movq	16(%r13), %rsi
	movq	%r15, %rdi
	callq	fputs
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%r14,8), %rbx
.LBB4_135:                              #   in Loop: Header=BB4_111 Depth=1
	movq	16(%r13), %r15
	xorl	%eax, %eax
	movq	%rbx, %rcx
	jmp	.LBB4_136
	.p2align	4, 0x90
.LBB4_141:                              #   in Loop: Header=BB4_136 Depth=2
	incq	%rcx
.LBB4_136:                              #   Parent Loop BB4_111 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %edx
	cmpb	$39, %dl
	je	.LBB4_140
# BB#137:                               #   in Loop: Header=BB4_136 Depth=2
	testb	%dl, %dl
	jne	.LBB4_141
	jmp	.LBB4_138
	.p2align	4, 0x90
.LBB4_140:                              #   in Loop: Header=BB4_136 Depth=2
	incl	%eax
	jmp	.LBB4_141
	.p2align	4, 0x90
.LBB4_138:                              #   in Loop: Header=BB4_111 Depth=1
	testl	%eax, %eax
	je	.LBB4_139
# BB#142:                               #   in Loop: Header=BB4_111 Depth=1
	movl	$39, %edi
	movq	%r15, %rsi
	callq	fputc
	movb	(%rbx), %al
	testb	%al, %al
	jne	.LBB4_144
	jmp	.LBB4_154
	.p2align	4, 0x90
.LBB4_152:                              # %.backedge.i
                                        #   in Loop: Header=BB4_144 Depth=2
	movb	(%rbx,%rbp), %al
	leaq	(%rbx,%rbp), %rbx
	testb	%al, %al
	je	.LBB4_154
.LBB4_144:                              # %.preheader.i270
                                        #   Parent Loop BB4_111 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_146 Depth 3
	movq	%r12, %rbp
	xorl	%edx, %edx
	testb	%al, %al
	jne	.LBB4_146
	jmp	.LBB4_148
	.p2align	4, 0x90
.LBB4_147:                              #   in Loop: Header=BB4_146 Depth=3
	movzbl	1(%rbx,%rdx), %eax
	incq	%rdx
	addq	%r12, %rbp
	testb	%al, %al
	je	.LBB4_148
.LBB4_146:                              #   Parent Loop BB4_111 Depth=1
                                        #     Parent Loop BB4_144 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$39, %al
	jne	.LBB4_147
.LBB4_148:                              # %.critedge.i275
                                        #   in Loop: Header=BB4_144 Depth=2
	testl	%edx, %edx
	je	.LBB4_149
# BB#150:                               #   in Loop: Header=BB4_144 Depth=2
	cmpb	$39, %al
	jne	.LBB4_153
# BB#151:                               #   in Loop: Header=BB4_144 Depth=2
	movl	$.L.str.159, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%rbx, %rcx
	callq	fprintf
	sarq	$32, %rbp
	jmp	.LBB4_152
	.p2align	4, 0x90
.LBB4_149:                              #   in Loop: Header=BB4_144 Depth=2
	movl	$.L.str.158, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movl	$1, %ebp
	jmp	.LBB4_152
	.p2align	4, 0x90
.LBB4_139:                              #   in Loop: Header=BB4_111 Depth=1
	movl	$.L.str.156, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	fprintf
	jmp	.LBB4_155
.LBB4_153:                              #   in Loop: Header=BB4_111 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	fputs
.LBB4_154:                              # %.loopexit.i277
                                        #   in Loop: Header=BB4_111 Depth=1
	movl	$39, %edi
	movq	%r15, %rsi
	callq	fputc
.LBB4_155:                              # %output_quoted_string.exit
                                        #   in Loop: Header=BB4_111 Depth=1
	incq	%r14
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	jne	.LBB4_111
.LBB4_156:                              # %._crit_edge349
	movq	16(%r13), %rcx
	movl	$.L.str.153, %edi
	movl	$3, %esi
	jmp	.LBB4_157
.LBB4_4:
	movl	$5, %r12d
	movq	(%rsp), %r13            # 8-byte Reload
.LBB4_7:                                # %._crit_edge
	movl	12(%r13), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 12(%r13)
	testl	%eax, %eax
	jle	.LBB4_9
# BB#8:
	movq	16(%r13), %rsi
	movl	$10, %edi
	callq	fputc
.LBB4_9:                                # %.preheader
	movq	16(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	jle	.LBB4_158
# BB#10:                                # %.lr.ph
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	868(%rax), %r14
	movl	%ecx, %ebx
	.p2align	4, 0x90
.LBB4_11:                               # =>This Inner Loop Header: Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	16(%rax), %rdi
	movq	(%r15), %rcx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	(%rbp), %r8
	testq	%r8, %r8
	cmoveq	%r14, %r8
	movl	$.L.str.141, %esi
	xorl	%eax, %eax
	movl	%r12d, %edx
	callq	fprintf
	addq	$8, %r15
	addq	$8, %rbp
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	decq	%rbx
	jne	.LBB4_11
	jmp	.LBB4_158
.LBB4_23:                               # %.lr.ph313.split.us.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_24:                               # %.lr.ph313.split.us
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$99, %r14
	ja	.LBB4_26
# BB#25:                                #   in Loop: Header=BB4_24 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	68(%rax,%r14,4), %ebp
	testl	%ebp, %ebp
	jg	.LBB4_27
.LBB4_26:                               # %.thread.us
                                        #   in Loop: Header=BB4_24 Depth=1
	movq	(%r15,%r14,8), %rdi
	testq	%rdi, %rdi
	movl	$.L.str.113, %eax
	cmoveq	%rax, %rdi
	callq	strlen
	movq	%rax, %rbp
	cmpl	$9, %ebp
	cmovlel	%ebx, %ebp
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	strlen
	cmpl	%eax, %ebp
	cmovll	%eax, %ebp
	cmpq	$99, %r14
	ja	.LBB4_28
.LBB4_27:                               # %.thread278.us
                                        #   in Loop: Header=BB4_24 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%ebp, 468(%rax,%r14,4)
.LBB4_28:                               #   in Loop: Header=BB4_24 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	cmpl	$0, 32(%rax)
	je	.LBB4_30
# BB#29:                                #   in Loop: Header=BB4_24 Depth=1
	movq	16(%rax), %rdi
	movq	(%r15,%r14,8), %r8
	cmpq	%r14, %r12
	movl	$.L.str.114, %r9d
	movl	$.L.str.107, %eax
	cmoveq	%rax, %r9
	movl	$.L.str.142, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	movl	%ebp, %ecx
	callq	fprintf
.LBB4_30:                               #   in Loop: Header=BB4_24 Depth=1
	incq	%r14
	cmpq	%r14, %r13
	jne	.LBB4_24
.LBB4_31:                               # %._crit_edge314
	movq	(%rsp), %rax            # 8-byte Reload
	cmpl	$0, 32(%rax)
	je	.LBB4_37
# BB#32:                                # %._crit_edge314
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_37
# BB#33:                                # %.lr.ph311
	movl	$.L.str.107, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_34:                               # =>This Inner Loop Header: Depth=1
	movl	$10, %edx
	cmpq	$99, %rbp
	ja	.LBB4_36
# BB#35:                                #   in Loop: Header=BB4_34 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	468(%rax,%rbp,4), %edx
.LBB4_36:                               #   in Loop: Header=BB4_34 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	16(%rax), %rdi
	cmpq	%rbp, %r12
	movl	$.L.str.114, %r9d
	cmoveq	%rbx, %r9
	movl	$.L.str.142, %esi
	movl	$.L.str.143, %r8d
	xorl	%eax, %eax
	movl	%edx, %ecx
	callq	fprintf
	incq	%rbp
	cmpq	%rbp, %r13
	jne	.LBB4_34
	jmp	.LBB4_37
.LBB4_64:
	movq	%rbx, %r13
.LBB4_67:                               # %._crit_edge327
	movl	$.L.str.148, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r13, %rbx
.LBB4_68:
	testq	%r14, %r14
	je	.LBB4_158
# BB#69:
	movq	%rbx, %rbp
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	16(%rbx), %rcx
	movl	$.L.str.146, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	movq	16(%rbx), %rcx
	testl	%ebp, %ebp
	jle	.LBB4_85
# BB#70:                                # %.lr.ph321
	leaq	868(%rbx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%ebp, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movabsq	$1152921779484753921, %r14 # imm = 0x1000004000000001
	xorl	%ebp, %ebp
	movq	8(%rsp), %r13           # 8-byte Reload
	.p2align	4, 0x90
.LBB4_71:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_72 Depth 2
                                        #       Child Loop BB4_73 Depth 3
	movl	$.L.str.149, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	movq	16(%rbx), %r15
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	(%r13,%rbp,8), %rbp
	testq	%rbp, %rbp
	cmoveq	24(%rsp), %rbp          # 8-byte Folded Reload
	movb	(%rbp), %al
	testb	%al, %al
	je	.LBB4_84
	.p2align	4, 0x90
.LBB4_72:                               # %.preheader.i
                                        #   Parent Loop BB4_71 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_73 Depth 3
	movq	%r12, %r13
	xorl	%ebx, %ebx
	jmp	.LBB4_73
	.p2align	4, 0x90
.LBB4_75:                               #   in Loop: Header=BB4_73 Depth=3
	movzbl	1(%rbp,%rbx), %eax
	incq	%rbx
	addq	%r12, %r13
.LBB4_73:                               #   Parent Loop BB4_71 Depth=1
                                        #     Parent Loop BB4_72 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	%al, %ecx
	cmpb	$60, %cl
	ja	.LBB4_75
# BB#74:                                #   in Loop: Header=BB4_73 Depth=3
	btq	%rcx, %r14
	jae	.LBB4_75
# BB#76:                                # %.critedge.i
                                        #   in Loop: Header=BB4_72 Depth=2
	testl	%ebx, %ebx
	jle	.LBB4_78
# BB#77:                                #   in Loop: Header=BB4_72 Depth=2
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movl	%ebx, %edx
	movq	%rbp, %rcx
	callq	fprintf
	movb	(%rbp,%rbx), %al
.LBB4_78:                               #   in Loop: Header=BB4_72 Depth=2
	cmpb	$38, %al
	je	.LBB4_81
# BB#79:                                #   in Loop: Header=BB4_72 Depth=2
	cmpb	$60, %al
	jne	.LBB4_83
# BB#80:                                #   in Loop: Header=BB4_72 Depth=2
	movl	$.L.str.154, %edi
	movl	$4, %esi
	jmp	.LBB4_82
	.p2align	4, 0x90
.LBB4_81:                               #   in Loop: Header=BB4_72 Depth=2
	movl	$.L.str.155, %edi
	movl	$5, %esi
.LBB4_82:                               #   in Loop: Header=BB4_72 Depth=2
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	sarq	$32, %r13
	movb	(%rbp,%r13), %al
	leaq	(%rbp,%r13), %rbp
	testb	%al, %al
	jne	.LBB4_72
.LBB4_83:                               # %output_html_string.exit.loopexit
                                        #   in Loop: Header=BB4_71 Depth=1
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	16(%rbx), %r15
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB4_84:                               # %output_html_string.exit
                                        #   in Loop: Header=BB4_71 Depth=1
	movl	$.L.str.150, %edi
	movl	$6, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	16(%rsp), %rbp          # 8-byte Reload
	incq	%rbp
	movq	16(%rbx), %rcx
	cmpq	32(%rsp), %rbp          # 8-byte Folded Reload
	jne	.LBB4_71
.LBB4_85:                               # %._crit_edge322
	movl	$.L.str.148, %edi
	movl	$6, %esi
.LBB4_157:                              # %.loopexit
	movl	$1, %edx
	callq	fwrite
.LBB4_158:                              # %.loopexit
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	callback, .Lfunc_end4-callback
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_2
	.quad	.LBB4_12
	.quad	.LBB4_47
	.quad	.LBB4_47
	.quad	.LBB4_61
	.quad	.LBB4_108
	.quad	.LBB4_86
	.quad	.LBB4_97
	.quad	.LBB4_158
	.quad	.LBB4_12

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.text
	.p2align	4, 0x90
	.type	process_input,@function
process_input:                          # @process_input
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi53:
	.cfi_def_cfa_offset 288
.Lcfi54:
	.cfi_offset %rbx, -56
.Lcfi55:
	.cfi_offset %r12, -48
.Lcfi56:
	.cfi_offset %r13, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	xorl	%ebp, %ebp
	movl	$0, 44(%rsp)            # 4-byte Folded Spill
	movl	$0, 36(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB5_1
.LBB5_103:                              # %.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	testb	%r12b, %r12b
	movl	$0, %r13d
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %r14d
	movl	40(%rsp), %ebp          # 4-byte Reload
	je	.LBB5_1
# BB#104:                               # %.lr.ph
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB5_105:                              #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%r12b, %ecx
	testb	$32, 1(%rax,%rcx,2)
	je	.LBB5_108
# BB#106:                               #   in Loop: Header=BB5_105 Depth=2
	movzbl	(%r15), %r12d
	incq	%r15
	testb	%r12b, %r12b
	jne	.LBB5_105
# BB#107:                               #   in Loop: Header=BB5_1 Depth=1
	xorl	%r13d, %r13d
	movl	%edx, %r14d
	jmp	.LBB5_1
.LBB5_116:                              # %_contains_semicolon.exit
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	%r13, %rdi
	callq	sqlite3_complete
	testl	%eax, %eax
	je	.LBB5_1
# BB#117:                               #   in Loop: Header=BB5_1 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	$0, 12(%rdi)
	callq	open_db
	cmpl	$0, enableTimer(%rip)
	je	.LBB5_119
# BB#118:                               #   in Loop: Header=BB5_1 Depth=1
	xorl	%edi, %edi
	movl	$sBegin, %esi
	callq	getrusage
.LBB5_119:                              # %beginTimer.exit
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rdi
	movl	$callback, %edx
	movq	%r13, %rsi
	leaq	48(%rsp), %r8
	callq	sqlite3_exec
	movl	%eax, %r14d
	cmpl	$0, enableTimer(%rip)
	je	.LBB5_121
# BB#120:                               #   in Loop: Header=BB5_1 Depth=1
	xorl	%edi, %edi
	leaq	80(%rsp), %rsi
	callq	getrusage
	movl	80(%rsp), %eax
	movl	88(%rsp), %ecx
	subl	sBegin+8(%rip), %ecx
	subl	sBegin(%rip), %eax
	imull	$1000000, %eax, %eax    # imm = 0xF4240
	addl	%ecx, %eax
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI5_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	mulsd	%xmm2, %xmm0
	movl	96(%rsp), %eax
	movl	104(%rsp), %ecx
	subl	sBegin+24(%rip), %ecx
	subl	sBegin+16(%rip), %eax
	imull	$1000000, %eax, %eax    # imm = 0xF4240
	addl	%ecx, %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm2, %xmm1
	movl	$.L.str.167, %edi
	movb	$2, %al
	callq	printf
.LBB5_121:                              # %endTimer.exit
                                        #   in Loop: Header=BB5_1 Depth=1
	testl	%r14d, %r14d
	jne	.LBB5_123
# BB#122:                               # %endTimer.exit
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	48(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_131
.LBB5_123:                              #   in Loop: Header=BB5_1 Depth=1
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	jne	.LBB5_125
# BB#124:                               #   in Loop: Header=BB5_1 Depth=1
	movl	stdin_is_interactive(%rip), %eax
	testl	%eax, %eax
	je	.LBB5_125
# BB#126:                               #   in Loop: Header=BB5_1 Depth=1
	movl	$100, %edi
	movl	$.L.str.164, %edx
	xorl	%eax, %eax
	leaq	80(%rsp), %rsi
	callq	sqlite3_snprintf
	jmp	.LBB5_127
.LBB5_108:                              # %.critedge4
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r14
	movq	%r14, %rbp
	shlq	$32, %rbp
	movabsq	$4294967296, %rax       # imm = 0x100000000
	addq	%rax, %rbp
	sarq	$32, %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB5_142
# BB#109:                               #   in Loop: Header=BB5_1 Depth=1
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movl	44(%rsp), %eax          # 4-byte Reload
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movq	56(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB5_112
.LBB5_54:                               #   in Loop: Header=BB5_1 Depth=1
                                        # kill: %R14D<def> %R14D<kill> %R14<kill> %R14<def>
	jmp	.LBB5_1
.LBB5_125:                              #   in Loop: Header=BB5_1 Depth=1
	movl	$100, %edi
	movl	$.L.str.163, %edx
	xorl	%eax, %eax
	leaq	80(%rsp), %rsi
	movl	36(%rsp), %ecx          # 4-byte Reload
	callq	sqlite3_snprintf
.LBB5_127:                              #   in Loop: Header=BB5_1 Depth=1
	movq	48(%rsp), %rdx
	testq	%rdx, %rdx
	je	.LBB5_129
# BB#128:                               #   in Loop: Header=BB5_1 Depth=1
	movl	$.L.str.165, %edi
	xorl	%eax, %eax
	leaq	80(%rsp), %rsi
	callq	printf
	movq	48(%rsp), %rdi
	callq	sqlite3_free
	movq	$0, 48(%rsp)
	jmp	.LBB5_130
.LBB5_129:                              #   in Loop: Header=BB5_1 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	callq	sqlite3_errmsg
	movq	%rax, %rcx
	movl	$.L.str.165, %edi
	xorl	%eax, %eax
	leaq	80(%rsp), %rsi
	movq	%rcx, %rdx
	callq	printf
.LBB5_130:                              #   in Loop: Header=BB5_1 Depth=1
	incl	%ebp
.LBB5_131:                              #   in Loop: Header=BB5_1 Depth=1
	movq	%r13, %rdi
	callq	free
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	jmp	.LBB5_1
.LBB5_58:                               #   in Loop: Header=BB5_1 Depth=1
	incl	%ebp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_1:                                # %_all_whitespace.exit127
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_41 Depth 2
                                        #       Child Loop BB5_46 Depth 3
                                        #       Child Loop BB5_51 Depth 3
                                        #     Child Loop BB5_61 Depth 2
                                        #     Child Loop BB5_66 Depth 2
                                        #       Child Loop BB5_71 Depth 3
                                        #       Child Loop BB5_77 Depth 3
                                        #     Child Loop BB5_85 Depth 2
                                        #       Child Loop BB5_90 Depth 3
                                        #       Child Loop BB5_96 Depth 3
                                        #     Child Loop BB5_105 Depth 2
                                        #     Child Loop BB5_115 Depth 2
                                        # kill: %R14D<def> %R14D<kill> %R14<kill> %R14<def>
	movl	%ebp, %ebx
	testl	%ebp, %ebp
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %r15           # 8-byte Reload
	je	.LBB5_5
# BB#2:                                 # %_all_whitespace.exit127
                                        #   in Loop: Header=BB5_1 Depth=1
	movl	bail_on_error(%rip), %eax
	testl	%eax, %eax
	je	.LBB5_5
# BB#3:                                 #   in Loop: Header=BB5_1 Depth=1
	testq	%rbp, %rbp
	jne	.LBB5_14
# BB#4:                                 #   in Loop: Header=BB5_1 Depth=1
	movl	stdin_is_interactive(%rip), %eax
	testl	%eax, %eax
	je	.LBB5_14
.LBB5_5:                                # %.critedge
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rdi
	callq	fflush
	movq	%r15, %rdi
	callq	free
	testq	%rbp, %rbp
	je	.LBB5_7
# BB#6:                                 #   in Loop: Header=BB5_1 Depth=1
	xorl	%edi, %edi
	movq	%rbp, %rsi
	jmp	.LBB5_12
	.p2align	4, 0x90
.LBB5_7:                                #   in Loop: Header=BB5_1 Depth=1
	testq	%r13, %r13
	je	.LBB5_10
# BB#8:                                 #   in Loop: Header=BB5_1 Depth=1
	cmpb	$0, (%r13)
	je	.LBB5_10
# BB#9:                                 #   in Loop: Header=BB5_1 Depth=1
	movl	$continuePrompt, %edi
	jmp	.LBB5_11
	.p2align	4, 0x90
.LBB5_10:                               #   in Loop: Header=BB5_1 Depth=1
	movl	$mainPrompt, %edi
.LBB5_11:                               #   in Loop: Header=BB5_1 Depth=1
	movq	stdin(%rip), %rsi
.LBB5_12:                               # %one_input_line.exit
                                        #   in Loop: Header=BB5_1 Depth=1
	callq	local_getline
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB5_13
# BB#26:                                #   in Loop: Header=BB5_1 Depth=1
	cmpl	$0, seenInterrupt(%rip)
	je	.LBB5_29
# BB#27:                                #   in Loop: Header=BB5_1 Depth=1
	testq	%rbp, %rbp
	jne	.LBB5_14
# BB#28:                                #   in Loop: Header=BB5_1 Depth=1
	movl	$0, seenInterrupt(%rip)
.LBB5_29:                               #   in Loop: Header=BB5_1 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 8(%rax)
	movl	%ebx, %ebp
	je	.LBB5_31
# BB#30:                                #   in Loop: Header=BB5_1 Depth=1
	movq	%r15, %rdi
	callq	puts
.LBB5_31:                               #   in Loop: Header=BB5_1 Depth=1
	incl	44(%rsp)                # 4-byte Folded Spill
	testq	%r13, %r13
	movq	%r15, 8(%rsp)           # 8-byte Spill
	je	.LBB5_39
# BB#32:                                #   in Loop: Header=BB5_1 Depth=1
	cmpb	$0, (%r13)
	je	.LBB5_39
# BB#33:                                # %._all_whitespace.exit127.thread_crit_edge
                                        #   in Loop: Header=BB5_1 Depth=1
	movb	(%r15), %r12b
	testl	%r14d, %r14d
	je	.LBB5_35
	jmp	.LBB5_59
	.p2align	4, 0x90
.LBB5_39:                               #   in Loop: Header=BB5_1 Depth=1
	movb	(%r15), %r12b
	testb	%r12b, %r12b
	movq	%r14, %rbx
	je	.LBB5_1
# BB#40:                                # %.lr.ph.i115
                                        #   in Loop: Header=BB5_1 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movl	%r12d, %edx
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	%r15, %rcx
	movq	%rbx, %r14
.LBB5_41:                               #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_46 Depth 3
                                        #       Child Loop BB5_51 Depth 3
	movzbl	%dl, %esi
	testb	$32, 1(%rax,%rsi,2)
	jne	.LBB5_56
# BB#42:                                #   in Loop: Header=BB5_41 Depth=2
	cmpb	$45, %dl
	je	.LBB5_49
# BB#43:                                #   in Loop: Header=BB5_41 Depth=2
	cmpb	$47, %dl
	jne	.LBB5_34
# BB#44:                                #   in Loop: Header=BB5_41 Depth=2
	cmpb	$42, 1(%rcx)
	jne	.LBB5_34
# BB#45:                                #   in Loop: Header=BB5_41 Depth=2
	addq	$2, %rcx
	jmp	.LBB5_46
	.p2align	4, 0x90
.LBB5_48:                               # %..critedge18_crit_edge.i120
                                        #   in Loop: Header=BB5_46 Depth=3
	incq	%rcx
.LBB5_46:                               # %.critedge18.i118
                                        #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_41 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rcx), %edx
	cmpb	$42, %dl
	je	.LBB5_55
# BB#47:                                # %.critedge18.i118
                                        #   in Loop: Header=BB5_46 Depth=3
	testb	%dl, %dl
	jne	.LBB5_48
	jmp	.LBB5_34
	.p2align	4, 0x90
.LBB5_55:                               #   in Loop: Header=BB5_46 Depth=3
	cmpb	$47, 1(%rcx)
	leaq	1(%rcx), %rcx
	jne	.LBB5_46
	jmp	.LBB5_56
	.p2align	4, 0x90
.LBB5_49:                               #   in Loop: Header=BB5_41 Depth=2
	cmpb	$45, 1(%rcx)
	jne	.LBB5_34
# BB#50:                                #   in Loop: Header=BB5_41 Depth=2
	incq	%rcx
	.p2align	4, 0x90
.LBB5_51:                               #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_41 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	1(%rcx), %edx
	incq	%rcx
	testb	%dl, %dl
	setne	%bl
	cmpb	$10, %dl
	je	.LBB5_53
# BB#52:                                #   in Loop: Header=BB5_51 Depth=3
	testb	%bl, %bl
	jne	.LBB5_51
.LBB5_53:                               #   in Loop: Header=BB5_41 Depth=2
	testb	%dl, %dl
	je	.LBB5_54
.LBB5_56:                               # %.critedge.i125
                                        #   in Loop: Header=BB5_41 Depth=2
	movb	1(%rcx), %dl
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB5_41
# BB#57:                                #   in Loop: Header=BB5_1 Depth=1
                                        # kill: %R14D<def> %R14D<kill> %R14<kill> %R14<def>
	jmp	.LBB5_1
	.p2align	4, 0x90
.LBB5_34:                               # %_all_whitespace.exit127.thread
                                        #   in Loop: Header=BB5_1 Depth=1
	testl	%r14d, %r14d
	jne	.LBB5_59
.LBB5_35:                               # %_all_whitespace.exit127.thread
                                        #   in Loop: Header=BB5_1 Depth=1
	cmpb	$46, %r12b
	jne	.LBB5_59
# BB#36:                                #   in Loop: Header=BB5_1 Depth=1
	movq	%r15, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	do_meta_command
	testl	%eax, %eax
	movl	$0, %r14d
	je	.LBB5_1
# BB#37:                                #   in Loop: Header=BB5_1 Depth=1
	cmpl	$2, %eax
	jne	.LBB5_58
	jmp	.LBB5_38
	.p2align	4, 0x90
.LBB5_59:                               #   in Loop: Header=BB5_1 Depth=1
	movq	%r14, 56(%rsp)          # 8-byte Spill
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movl	%ebp, 40(%rsp)          # 4-byte Spill
	callq	__ctype_b_loc
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	(%rax), %r14
	movzbl	%r12b, %eax
	movq	%r15, %rcx
	leaq	1(%rcx), %r15
	testb	$32, 1(%r14,%rax,2)
	movq	%rcx, %rax
	movb	%r12b, %r13b
	movq	%rax, %rbp
	movq	%r15, %rbx
	je	.LBB5_63
# BB#60:                                # %._crit_edge.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB5_61:                               # %._crit_edge
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbx), %r13d
	incq	%rbx
	testb	$32, 1(%r14,%r13,2)
	jne	.LBB5_61
# BB#62:                                # %._crit_edge219.loopexit
                                        #   in Loop: Header=BB5_1 Depth=1
	leaq	-1(%rbx), %rbp
.LBB5_63:                               # %._crit_edge219
                                        #   in Loop: Header=BB5_1 Depth=1
	cmpb	$47, %r13b
	jne	.LBB5_81
# BB#64:                                #   in Loop: Header=BB5_1 Depth=1
	movb	(%rbx), %cl
	testb	%cl, %cl
	je	.LBB5_100
# BB#65:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	%rbx, %rax
.LBB5_66:                               # %.lr.ph.i.i
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_71 Depth 3
                                        #       Child Loop BB5_77 Depth 3
	movzbl	%cl, %edx
	testb	$32, 1(%r14,%rdx,2)
	jne	.LBB5_80
# BB#67:                                #   in Loop: Header=BB5_66 Depth=2
	cmpb	$45, %cl
	je	.LBB5_75
# BB#68:                                #   in Loop: Header=BB5_66 Depth=2
	cmpb	$47, %cl
	jne	.LBB5_81
# BB#69:                                #   in Loop: Header=BB5_66 Depth=2
	cmpb	$42, 1(%rax)
	jne	.LBB5_81
# BB#70:                                #   in Loop: Header=BB5_66 Depth=2
	addq	$2, %rax
	jmp	.LBB5_71
	.p2align	4, 0x90
.LBB5_73:                               # %..critedge18_crit_edge.i.i
                                        #   in Loop: Header=BB5_71 Depth=3
	incq	%rax
.LBB5_71:                               # %.critedge18.i.i
                                        #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %ecx
	cmpb	$42, %cl
	je	.LBB5_74
# BB#72:                                # %.critedge18.i.i
                                        #   in Loop: Header=BB5_71 Depth=3
	testb	%cl, %cl
	jne	.LBB5_73
	jmp	.LBB5_81
	.p2align	4, 0x90
.LBB5_74:                               #   in Loop: Header=BB5_71 Depth=3
	cmpb	$47, 1(%rax)
	leaq	1(%rax), %rax
	jne	.LBB5_71
	jmp	.LBB5_80
	.p2align	4, 0x90
.LBB5_75:                               #   in Loop: Header=BB5_66 Depth=2
	cmpb	$45, 1(%rax)
	jne	.LBB5_81
# BB#76:                                #   in Loop: Header=BB5_66 Depth=2
	incq	%rax
	.p2align	4, 0x90
.LBB5_77:                               #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	1(%rax), %ecx
	incq	%rax
	testb	%cl, %cl
	setne	%dl
	cmpb	$10, %cl
	je	.LBB5_79
# BB#78:                                #   in Loop: Header=BB5_77 Depth=3
	testb	%dl, %dl
	jne	.LBB5_77
.LBB5_79:                               #   in Loop: Header=BB5_66 Depth=2
	testb	%cl, %cl
	je	.LBB5_100
.LBB5_80:                               # %.critedge.i.i
                                        #   in Loop: Header=BB5_66 Depth=2
	movb	1(%rax), %cl
	incq	%rax
	testb	%cl, %cl
	jne	.LBB5_66
	jmp	.LBB5_100
	.p2align	4, 0x90
.LBB5_81:                               # %_all_whitespace.exit.thread.i
                                        #   in Loop: Header=BB5_1 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movsbq	%r13b, %rcx
	cmpl	$103, (%rax,%rcx,4)
	jne	.LBB5_101
# BB#82:                                #   in Loop: Header=BB5_1 Depth=1
	movsbq	(%rbx), %rcx
	cmpl	$111, (%rax,%rcx,4)
	jne	.LBB5_101
# BB#83:                                #   in Loop: Header=BB5_1 Depth=1
	movb	2(%rbp), %al
	testb	%al, %al
	je	.LBB5_100
# BB#84:                                # %.lr.ph.i14.i.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	addq	$2, %rbp
.LBB5_85:                               # %.lr.ph.i14.i
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_90 Depth 3
                                        #       Child Loop BB5_96 Depth 3
	movzbl	%al, %ecx
	testb	$32, 1(%r14,%rcx,2)
	jne	.LBB5_99
# BB#86:                                #   in Loop: Header=BB5_85 Depth=2
	cmpb	$45, %al
	je	.LBB5_94
# BB#87:                                #   in Loop: Header=BB5_85 Depth=2
	cmpb	$47, %al
	movq	16(%rsp), %r13          # 8-byte Reload
	jne	.LBB5_102
# BB#88:                                #   in Loop: Header=BB5_85 Depth=2
	cmpb	$42, 1(%rbp)
	jne	.LBB5_102
# BB#89:                                #   in Loop: Header=BB5_85 Depth=2
	addq	$2, %rbp
	jmp	.LBB5_90
	.p2align	4, 0x90
.LBB5_92:                               # %..critedge18_crit_edge.i19.i
                                        #   in Loop: Header=BB5_90 Depth=3
	incq	%rbp
.LBB5_90:                               # %.critedge18.i17.i
                                        #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_85 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rbp), %eax
	cmpb	$42, %al
	je	.LBB5_93
# BB#91:                                # %.critedge18.i17.i
                                        #   in Loop: Header=BB5_90 Depth=3
	testb	%al, %al
	jne	.LBB5_92
	jmp	.LBB5_101
	.p2align	4, 0x90
.LBB5_93:                               #   in Loop: Header=BB5_90 Depth=3
	cmpb	$47, 1(%rbp)
	leaq	1(%rbp), %rbp
	jne	.LBB5_90
	jmp	.LBB5_99
	.p2align	4, 0x90
.LBB5_94:                               #   in Loop: Header=BB5_85 Depth=2
	cmpb	$45, 1(%rbp)
	movq	16(%rsp), %r13          # 8-byte Reload
	jne	.LBB5_102
# BB#95:                                #   in Loop: Header=BB5_85 Depth=2
	incq	%rbp
	.p2align	4, 0x90
.LBB5_96:                               #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_85 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	1(%rbp), %eax
	incq	%rbp
	testb	%al, %al
	setne	%cl
	cmpb	$10, %al
	je	.LBB5_98
# BB#97:                                #   in Loop: Header=BB5_96 Depth=3
	testb	%cl, %cl
	jne	.LBB5_96
.LBB5_98:                               #   in Loop: Header=BB5_85 Depth=2
	testb	%al, %al
	je	.LBB5_100
.LBB5_99:                               # %.critedge.i24.i
                                        #   in Loop: Header=BB5_85 Depth=2
	movb	1(%rbp), %al
	incq	%rbp
	testb	%al, %al
	jne	.LBB5_85
.LBB5_100:                              # %_is_command_terminator.exit
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movw	$59, (%rax)
	movb	$59, %r12b
.LBB5_101:                              # %_is_command_terminator.exit.thread.loopexit
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB5_102:                              # %_is_command_terminator.exit.thread
                                        #   in Loop: Header=BB5_1 Depth=1
	testq	%r13, %r13
	je	.LBB5_103
# BB#110:                               #   in Loop: Header=BB5_1 Depth=1
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	56(%rsp), %rbx          # 8-byte Reload
	leal	2(%rbx,%rbp), %eax
	movslq	%eax, %rsi
	movq	%r13, %rdi
	callq	realloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB5_143
# BB#111:                               #   in Loop: Header=BB5_1 Depth=1
	movslq	%ebx, %rax
	movb	$10, (%r13,%rax)
	leaq	1(%r13,%rax), %rdi
	movq	%rbp, %rdx
	shlq	$32, %rdx
	movabsq	$4294967296, %rax       # imm = 0x100000000
	addq	%rax, %rdx
	sarq	$32, %rdx
	movq	%r14, %rsi
	callq	memcpy
	leal	1(%rbp,%rbx), %r14d
.LBB5_112:                              # %.thread
                                        #   in Loop: Header=BB5_1 Depth=1
	movl	%r14d, %ecx
	subl	%ebx, %ecx
	movl	40(%rsp), %ebp          # 4-byte Reload
	jle	.LBB5_1
# BB#113:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB5_1 Depth=1
	movslq	%ebx, %rax
	addq	%r13, %rax
	movslq	%ecx, %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_115:                              # %.lr.ph.i129
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$59, (%rax,%rdx)
	je	.LBB5_116
# BB#114:                               #   in Loop: Header=BB5_115 Depth=2
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB5_115
	jmp	.LBB5_1
.LBB5_13:
	xorl	%r15d, %r15d
.LBB5_14:
	movl	%ebx, %ebp
	testq	%r13, %r13
	je	.LBB5_141
.LBB5_16:
	movq	%r13, %r14
	movb	(%r13), %bl
	testb	%bl, %bl
	je	.LBB5_140
# BB#17:                                # %.lr.ph.i
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movq	%r14, %rcx
.LBB5_18:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_23 Depth 2
                                        #     Child Loop BB5_135 Depth 2
	movzbl	%bl, %edx
	testb	$32, 1(%rax,%rdx,2)
	jne	.LBB5_138
# BB#19:                                #   in Loop: Header=BB5_18 Depth=1
	cmpb	$45, %bl
	je	.LBB5_133
# BB#20:                                #   in Loop: Header=BB5_18 Depth=1
	cmpb	$47, %bl
	jne	.LBB5_139
# BB#21:                                #   in Loop: Header=BB5_18 Depth=1
	cmpb	$42, 1(%rcx)
	jne	.LBB5_139
# BB#22:                                #   in Loop: Header=BB5_18 Depth=1
	addq	$2, %rcx
	jmp	.LBB5_23
	.p2align	4, 0x90
.LBB5_25:                               # %..critedge18_crit_edge.i
                                        #   in Loop: Header=BB5_23 Depth=2
	incq	%rcx
.LBB5_23:                               # %.critedge18.i
                                        #   Parent Loop BB5_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %edx
	cmpb	$42, %dl
	je	.LBB5_132
# BB#24:                                # %.critedge18.i
                                        #   in Loop: Header=BB5_23 Depth=2
	testb	%dl, %dl
	jne	.LBB5_25
	jmp	.LBB5_139
	.p2align	4, 0x90
.LBB5_132:                              #   in Loop: Header=BB5_23 Depth=2
	cmpb	$47, 1(%rcx)
	leaq	1(%rcx), %rcx
	jne	.LBB5_23
	jmp	.LBB5_138
	.p2align	4, 0x90
.LBB5_133:                              #   in Loop: Header=BB5_18 Depth=1
	cmpb	$45, 1(%rcx)
	jne	.LBB5_139
# BB#134:                               #   in Loop: Header=BB5_18 Depth=1
	incq	%rcx
	.p2align	4, 0x90
.LBB5_135:                              #   Parent Loop BB5_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	1(%rcx), %edx
	incq	%rcx
	testb	%dl, %dl
	setne	%bl
	cmpb	$10, %dl
	je	.LBB5_137
# BB#136:                               #   in Loop: Header=BB5_135 Depth=2
	testb	%bl, %bl
	jne	.LBB5_135
.LBB5_137:                              #   in Loop: Header=BB5_18 Depth=1
	testb	%dl, %dl
	je	.LBB5_140
.LBB5_138:                              # %.critedge.i
                                        #   in Loop: Header=BB5_18 Depth=1
	movb	1(%rcx), %bl
	incq	%rcx
	testb	%bl, %bl
	jne	.LBB5_18
	jmp	.LBB5_140
.LBB5_139:                              # %.loopexit
	movl	$.L.str.166, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	printf
.LBB5_140:                              # %_all_whitespace.exit
	movq	%r14, %rdi
	callq	free
.LBB5_141:
	movq	%r15, %rdi
	callq	free
	movl	%ebp, %eax
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_38:
	movq	8(%rsp), %r15           # 8-byte Reload
	testq	%r13, %r13
	jne	.LBB5_16
	jmp	.LBB5_141
.LBB5_143:
	movq	stderr(%rip), %rdi
	movq	Argv0(%rip), %rdx
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB5_142:
	movq	stderr(%rip), %rcx
	movl	$.L.str.162, %edi
	movl	$14, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	process_input, .Lfunc_end5-process_input
	.cfi_endproc

	.p2align	4, 0x90
	.type	set_table_name,@function
set_table_name:                         # @set_table_name
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 48
.Lcfi65:
	.cfi_offset %rbx, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r14
	movq	40(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB6_2
# BB#1:
	callq	free
	movq	$0, 40(%r14)
.LBB6_2:
	testq	%rbp, %rbp
	je	.LBB6_21
# BB#3:
	callq	__ctype_b_loc
	movq	(%rax), %r8
	movzbl	(%rbp), %edx
	cmpq	$95, %rdx
	setne	%cl
	movzwl	(%r8,%rdx,2), %eax
	testb	$4, %ah
	sete	%bl
	andb	%cl, %bl
	testq	%rdx, %rdx
	je	.LBB6_4
# BB#5:                                 # %.lr.ph58.preheader
	cmpb	$95, %dl
	setne	%sil
	xorl	%ecx, %ecx
	testb	$8, %al
	sete	%cl
	andb	%cl, %sil
	orb	%sil, %bl
	incl	%ecx
	cmpb	$39, %dl
	movzbl	%bl, %r15d
	movl	$1, %edx
	cmovnel	%edx, %ecx
	movb	1(%rbp), %bl
	testb	%bl, %bl
	je	.LBB6_8
# BB#6:                                 # %._crit_edge64.preheader
	leaq	2(%rbp), %rdx
	.p2align	4, 0x90
.LBB6_7:                                # %._crit_edge64
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%bl, %esi
	movw	(%r8,%rsi,2), %di
	cmpb	$39, %sil
	sete	%bl
	andw	$8, %di
	sete	%al
	andb	%bl, %al
	testw	$-8, %di
	movl	$1, %edi
	cmovnel	%r15d, %edi
	cmpb	$95, %sil
	movzbl	%al, %eax
	cmovnel	%edi, %r15d
	leal	1(%rcx,%rax), %ecx
	movzbl	(%rdx), %ebx
	incq	%rdx
	testb	%bl, %bl
	jne	.LBB6_7
	jmp	.LBB6_8
.LBB6_4:
	xorl	%ecx, %ecx
	movzbl	%bl, %r15d
.LBB6_8:                                # %._crit_edge59
	leal	2(%rcx), %eax
	testl	%r15d, %r15d
	cmovel	%ecx, %eax
	incl	%eax
	movslq	%eax, %rdi
	callq	malloc
	movq	%rax, 40(%r14)
	testq	%rax, %rax
	je	.LBB6_22
# BB#9:
	testl	%r15d, %r15d
	je	.LBB6_10
# BB#11:
	movb	$39, (%rax)
	movl	$1, %edx
	jmp	.LBB6_12
.LBB6_10:
	xorl	%edx, %edx
.LBB6_12:                               # %.preheader
	movb	(%rbp), %bl
	testb	%bl, %bl
	je	.LBB6_13
# BB#14:                                # %.lr.ph.preheader
	incq	%rbp
	.p2align	4, 0x90
.LBB6_15:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	%edx, %rcx
	movb	%bl, (%rax,%rcx)
	incq	%rcx
	cmpb	$39, %bl
	jne	.LBB6_17
# BB#16:                                #   in Loop: Header=BB6_15 Depth=1
	addl	$2, %edx
	movb	$39, (%rax,%rcx)
	movl	%edx, %ecx
.LBB6_17:                               #   in Loop: Header=BB6_15 Depth=1
	movzbl	(%rbp), %ebx
	incq	%rbp
	testb	%bl, %bl
	movl	%ecx, %edx
	jne	.LBB6_15
	jmp	.LBB6_18
.LBB6_13:
	movl	%edx, %ecx
.LBB6_18:                               # %._crit_edge
	testl	%r15d, %r15d
	je	.LBB6_20
# BB#19:
	movslq	%ecx, %rdx
	incl	%ecx
	movb	$39, (%rax,%rdx)
.LBB6_20:
	movslq	%ecx, %rcx
	movb	$0, (%rax,%rcx)
.LBB6_21:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_22:
	movq	stderr(%rip), %rcx
	movl	$.L.str.168, %edi
	movl	$15, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	set_table_name, .Lfunc_end6-set_table_name
	.cfi_endproc

	.p2align	4, 0x90
	.type	shellstaticFunc,@function
shellstaticFunc:                        # @shellstaticFunc
	.cfi_startproc
# BB#0:
	movq	zShellStatic(%rip), %rsi
	movl	$-1, %edx
	xorl	%ecx, %ecx
	jmp	sqlite3_result_text     # TAILCALL
.Lfunc_end7:
	.size	shellstaticFunc, .Lfunc_end7-shellstaticFunc
	.cfi_endproc

	.p2align	4, 0x90
	.type	booleanValue,@function
booleanValue:                           # @booleanValue
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi73:
	.cfi_def_cfa_offset 48
.Lcfi74:
	.cfi_offset %rbx, -40
.Lcfi75:
	.cfi_offset %r12, -32
.Lcfi76:
	.cfi_offset %r14, -24
.Lcfi77:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	xorl	%r12d, %r12d
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r14
	movb	(%r15), %bl
	testb	%bl, %bl
	je	.LBB8_6
# BB#1:                                 # %.lr.ph
	callq	__ctype_tolower_loc
	leaq	1(%r15), %rcx
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdx
	movsbq	%bl, %rsi
	movzbl	(%rdx,%rsi,4), %edx
	movb	%dl, -1(%rcx)
	movzbl	(%rcx), %ebx
	incq	%rcx
	testb	%bl, %bl
	jne	.LBB8_2
# BB#3:                                 # %._crit_edge
	movb	(%r15), %r12b
	cmpb	$111, %r12b
	jne	.LBB8_6
# BB#4:
	movb	$111, %r12b
	cmpb	$110, 1(%r15)
	jne	.LBB8_6
# BB#5:
	cmpb	$0, 2(%r15)
	je	.LBB8_11
.LBB8_6:                                # %.thread
	movzbl	%r12b, %ecx
	movl	$121, %eax
	subl	%ecx, %eax
	jne	.LBB8_10
# BB#7:
	movzbl	1(%r15), %ecx
	movl	$101, %eax
	subl	%ecx, %eax
	jne	.LBB8_10
# BB#8:
	movzbl	2(%r15), %ecx
	movl	$115, %eax
	subl	%ecx, %eax
	jne	.LBB8_10
# BB#9:
	movzbl	3(%r15), %eax
	negl	%eax
.LBB8_10:
	testl	%eax, %eax
	movl	$1, %eax
	cmovel	%eax, %r14d
	movl	%r14d, %eax
.LBB8_12:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB8_11:
	movl	$1, %eax
	jmp	.LBB8_12
.Lfunc_end8:
	.size	booleanValue, .Lfunc_end8-booleanValue
	.cfi_endproc

	.p2align	4, 0x90
	.type	run_schema_dump_query,@function
run_schema_dump_query:                  # @run_schema_dump_query
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 32
.Lcfi81:
	.cfi_offset %rbx, -32
.Lcfi82:
	.cfi_offset %r14, -24
.Lcfi83:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	movl	$dump_callback, %edx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	callq	sqlite3_exec
	cmpl	$11, %eax
	jne	.LBB9_2
# BB#1:
	movq	%r14, %rdi
	callq	strlen
	shlq	$32, %rax
	movabsq	$429496729600, %rdi     # imm = 0x6400000000
	addq	%rax, %rdi
	sarq	$32, %rdi
	callq	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB9_2
# BB#3:                                 # %.critedge
	movl	$8, %edi
	movl	$.L.str.120, %edx
	xorl	%eax, %eax
	movq	%r15, %rsi
	movq	%r14, %rcx
	callq	sqlite3_snprintf
	movq	(%rbx), %rdi
	movl	$dump_callback, %edx
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	movq	%rbx, %rcx
	callq	sqlite3_exec
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB9_2:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	run_schema_dump_query, .Lfunc_end9-run_schema_dump_query
	.cfi_endproc

	.p2align	4, 0x90
	.type	run_table_dump_query,@function
run_table_dump_query:                   # @run_table_dump_query
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi85:
	.cfi_def_cfa_offset 32
.Lcfi86:
	.cfi_offset %rbx, -16
	movq	%rdx, %rax
	movq	%rdi, %rbx
	leaq	8(%rsp), %rcx
	movl	$-1, %edx
	xorl	%r8d, %r8d
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	sqlite3_prepare
	testl	%eax, %eax
	jne	.LBB10_5
# BB#1:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_5
# BB#2:                                 # %.preheader
	callq	sqlite3_step
	movq	8(%rsp), %rdi
	cmpl	$100, %eax
	jne	.LBB10_4
	.p2align	4, 0x90
.LBB10_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	callq	sqlite3_column_text
	movq	%rax, %rcx
	movl	$.L.str.129, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	fprintf
	movq	8(%rsp), %rdi
	callq	sqlite3_step
	cmpl	$100, %eax
	movq	8(%rsp), %rdi
	je	.LBB10_3
.LBB10_4:                               # %._crit_edge
	callq	sqlite3_finalize
.LBB10_5:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end10:
	.size	run_table_dump_query, .Lfunc_end10-run_table_dump_query
	.cfi_endproc

	.p2align	4, 0x90
	.type	local_getline,@function
local_getline:                          # @local_getline
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi89:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi90:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi91:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi93:
	.cfi_def_cfa_offset 64
.Lcfi94:
	.cfi_offset %rbx, -56
.Lcfi95:
	.cfi_offset %r12, -48
.Lcfi96:
	.cfi_offset %r13, -40
.Lcfi97:
	.cfi_offset %r14, -32
.Lcfi98:
	.cfi_offset %r15, -24
.Lcfi99:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rcx
	testq	%rcx, %rcx
	je	.LBB11_3
# BB#1:
	cmpb	$0, (%rcx)
	je	.LBB11_3
# BB#2:
	movl	$.L.str.88, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB11_3:
	movl	$100, %edi
	callq	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB11_16
# BB#4:                                 # %.lr.ph.split.us.preheader
	movl	$100, %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_5:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_9 Depth 2
	leal	100(%rbx), %eax
	cmpl	%r13d, %eax
	jle	.LBB11_7
# BB#6:                                 #   in Loop: Header=BB11_5 Depth=1
	leal	100(%r13,%r13), %r13d
	movslq	%r13d, %rsi
	movq	%r15, %rdi
	callq	realloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB11_16
.LBB11_7:                               #   in Loop: Header=BB11_5 Depth=1
	movslq	%ebx, %rbp
	leaq	(%r15,%rbp), %r12
	movl	%r13d, %esi
	subl	%ebx, %esi
	movq	%r12, %rdi
	movq	%r14, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB11_14
# BB#8:                                 # %.preheader.us.preheader
                                        #   in Loop: Header=BB11_5 Depth=1
	decl	%ebx
	addq	%r15, %rbp
	.p2align	4, 0x90
.LBB11_9:                               # %.preheader.us
                                        #   Parent Loop BB11_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebx
	cmpb	$0, (%rbp)
	leaq	1(%rbp), %rbp
	jne	.LBB11_9
# BB#10:                                #   in Loop: Header=BB11_5 Depth=1
	testl	%ebx, %ebx
	jle	.LBB11_5
# BB#11:                                #   in Loop: Header=BB11_5 Depth=1
	leal	-1(%rbx), %eax
	movslq	%eax, %r12
	cmpb	$10, (%r15,%r12)
	jne	.LBB11_5
# BB#12:                                # %.outer.loopexit
	addq	%r15, %r12
	movl	%eax, %ebx
	jmp	.LBB11_13
.LBB11_14:                              # %.us-lcssa.us
	testl	%ebx, %ebx
	je	.LBB11_15
.LBB11_13:                              # %.outer
	movb	$0, (%r12)
	incl	%ebx
	movslq	%ebx, %rsi
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	realloc                 # TAILCALL
.LBB11_15:
	movq	%r15, %rdi
	callq	free
.LBB11_16:                              # %.loopexit45
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	local_getline, .Lfunc_end11-local_getline
	.cfi_endproc

	.p2align	4, 0x90
	.type	output_c_string,@function
output_c_string:                        # @output_c_string
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi100:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi101:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi102:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi104:
	.cfi_def_cfa_offset 48
.Lcfi105:
	.cfi_offset %rbx, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$34, %edi
	jmp	.LBB12_1
	.p2align	4, 0x90
.LBB12_5:                               #   in Loop: Header=BB12_1 Depth=1
	movl	%ebp, %edi
.LBB12_1:                               # %.backedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_2 Depth 2
	movq	%r14, %rsi
	callq	fputc
	jmp	.LBB12_2
.LBB12_10:                              #   in Loop: Header=BB12_2 Depth=2
	movzbl	%bl, %edx
	movl	$.L.str.140, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	.p2align	4, 0x90
.LBB12_2:                               # %.backedge
                                        #   Parent Loop BB12_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r15), %ebx
	incq	%r15
	cmpq	$13, %rbx
	movsbl	%bl, %ebp
	ja	.LBB12_3
# BB#12:                                # %.backedge
                                        #   in Loop: Header=BB12_2 Depth=2
	jmpq	*.LJTI12_0(,%rbx,8)
	.p2align	4, 0x90
.LBB12_3:                               # %.backedge
                                        #   in Loop: Header=BB12_2 Depth=2
	cmpb	$92, %bl
	je	.LBB12_4
.LBB12_9:                               #   in Loop: Header=BB12_2 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movslq	%ebp, %rcx
	testb	$64, 1(%rax,%rcx,2)
	jne	.LBB12_5
	jmp	.LBB12_10
	.p2align	4, 0x90
.LBB12_6:                               #   in Loop: Header=BB12_1 Depth=1
	movl	$92, %edi
	movq	%r14, %rsi
	callq	fputc
	movl	$116, %edi
	jmp	.LBB12_1
	.p2align	4, 0x90
.LBB12_7:                               #   in Loop: Header=BB12_1 Depth=1
	movl	$92, %edi
	movq	%r14, %rsi
	callq	fputc
	movl	$110, %edi
	jmp	.LBB12_1
	.p2align	4, 0x90
.LBB12_8:                               #   in Loop: Header=BB12_1 Depth=1
	movl	$92, %edi
	movq	%r14, %rsi
	callq	fputc
	movl	$114, %edi
	jmp	.LBB12_1
.LBB12_4:                               #   in Loop: Header=BB12_1 Depth=1
	movl	%ebp, %edi
	movq	%r14, %rsi
	callq	fputc
	jmp	.LBB12_5
.LBB12_11:
	movl	$34, %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fputc                   # TAILCALL
.Lfunc_end12:
	.size	output_c_string, .Lfunc_end12-output_c_string
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI12_0:
	.quad	.LBB12_11
	.quad	.LBB12_9
	.quad	.LBB12_9
	.quad	.LBB12_9
	.quad	.LBB12_9
	.quad	.LBB12_9
	.quad	.LBB12_9
	.quad	.LBB12_9
	.quad	.LBB12_9
	.quad	.LBB12_6
	.quad	.LBB12_7
	.quad	.LBB12_9
	.quad	.LBB12_9
	.quad	.LBB12_8

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_0:
	.long	34                      # 0x22
	.long	34                      # 0x22
	.long	34                      # 0x22
	.long	34                      # 0x22
.LCPI13_1:
	.long	39                      # 0x27
	.long	39                      # 0x27
	.long	39                      # 0x27
	.long	39                      # 0x27
	.text
	.p2align	4, 0x90
	.type	dump_callback,@function
dump_callback:                          # @dump_callback
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi112:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi113:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi115:
	.cfi_def_cfa_offset 80
.Lcfi116:
	.cfi_offset %rbx, -56
.Lcfi117:
	.cfi_offset %r12, -48
.Lcfi118:
	.cfi_offset %r13, -40
.Lcfi119:
	.cfi_offset %r14, -32
.Lcfi120:
	.cfi_offset %r15, -24
.Lcfi121:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movl	$1, %ebx
	cmpl	$3, %esi
	jne	.LBB13_207
# BB#1:
	movq	(%rdx), %r14
	movq	8(%rdx), %rbx
	movq	16(%rdx), %rbp
	movl	$.L.str.121, %esi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB13_6
# BB#2:
	movl	$.L.str.123, %esi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB13_7
# BB#3:
	movl	$.L.str.125, %esi
	movl	$7, %edx
	movq	%r14, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB13_206
# BB#4:
	movl	$.L.str.126, %esi
	movl	$20, %edx
	movq	%rbp, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB13_16
# BB#5:
	movq	16(%r12), %rdi
	movl	$.L.str.129, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	fprintf
	jmp	.LBB13_9
.LBB13_6:
	movq	16(%r12), %rcx
	movl	$.L.str.122, %edi
	movl	$29, %esi
	jmp	.LBB13_8
.LBB13_7:
	movq	16(%r12), %rcx
	movl	$.L.str.124, %edi
	movl	$23, %esi
.LBB13_8:
	movl	$1, %edx
	callq	fwrite
.LBB13_9:
	movl	$.L.str.81, %esi
	movq	%rbx, %rdi
	callq	strcmp
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jne	.LBB13_207
# BB#10:
	movq	$0, 8(%rsp)
	xorl	%r15d, %r15d
	xorl	%edi, %edi
	movl	$19, %esi
	callq	realloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB13_12
# BB#11:
	movdqu	.L.str.130(%rip), %xmm0
	movdqu	%xmm0, (%rbx)
	movw	$10351, 16(%rbx)        # imm = 0x286F
	movb	$0, 18(%rbx)
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r15
	jmp	.LBB13_13
.LBB13_12:                              # %appendText.exit.thread
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbp
	xorl	%ebx, %ebx
.LBB13_13:
	leal	3(%rbp,%r15), %edx
	testl	%ebp, %ebp
	jle	.LBB13_26
# BB#14:                                # %.lr.ph62.preheader.i
	movl	%ebp, %eax
	cmpq	$8, %rax
	jb	.LBB13_23
# BB#19:                                # %min.iters.checked
	movl	%ebp, %esi
	andl	$7, %esi
	movq	%rax, %rcx
	subq	%rsi, %rcx
	je	.LBB13_23
# BB#20:                                # %vector.ph
	movd	%edx, %xmm0
	leaq	4(%r14), %rdx
	pxor	%xmm2, %xmm2
	movdqa	.LCPI13_0(%rip), %xmm3  # xmm3 = [34,34,34,34]
	movq	%rcx, %rdi
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB13_21:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	-4(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3],xmm4[4],xmm2[4],xmm4[5],xmm2[5],xmm4[6],xmm2[6],xmm4[7],xmm2[7]
	punpcklwd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3]
	movd	(%rdx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3],xmm5[4],xmm2[4],xmm5[5],xmm2[5],xmm5[6],xmm2[6],xmm5[7],xmm2[7]
	punpcklwd	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3]
	pcmpeqd	%xmm3, %xmm4
	psrld	$31, %xmm4
	pcmpeqd	%xmm3, %xmm5
	psrld	$31, %xmm5
	paddd	%xmm4, %xmm0
	paddd	%xmm5, %xmm1
	addq	$8, %rdx
	addq	$-8, %rdi
	jne	.LBB13_21
# BB#22:                                # %middle.block
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %edx
	testq	%rsi, %rsi
	jne	.LBB13_24
	jmp	.LBB13_26
.LBB13_23:
	xorl	%ecx, %ecx
.LBB13_24:                              # %.lr.ph62.i.preheader
	subq	%rcx, %rax
	addq	%r14, %rcx
	.p2align	4, 0x90
.LBB13_25:                              # %.lr.ph62.i
                                        # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	cmpb	$34, (%rcx)
	sete	%sil
	addl	%esi, %edx
	incq	%rcx
	decq	%rax
	jne	.LBB13_25
.LBB13_26:                              # %.loopexit.i
	movq	%r14, (%rsp)            # 8-byte Spill
	movslq	%edx, %rsi
	movq	%rbx, %rdi
	callq	realloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB13_30
# BB#27:
	movslq	%r15d, %rcx
	leaq	1(%r14,%rcx), %rax
	movb	$34, (%r14,%rcx)
	testl	%ebp, %ebp
	jle	.LBB13_43
# BB#28:                                # %.lr.ph.preheader.i
	movl	%ebp, %ecx
	testb	$1, %bpl
	jne	.LBB13_31
# BB#29:
	xorl	%edx, %edx
	cmpq	$1, %rcx
	jne	.LBB13_35
	jmp	.LBB13_43
.LBB13_30:
	xorl	%r14d, %r14d
	xorl	%ebp, %ebp
	jmp	.LBB13_44
.LBB13_16:
	cmpl	$0, 28(%r12)
	jne	.LBB13_18
# BB#17:
	movq	16(%r12), %rcx
	movl	$.L.str.127, %edi
	movl	$27, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, 28(%r12)
.LBB13_18:                              # %._crit_edge
	xorl	%ebx, %ebx
	movl	$.L.str.128, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	movq	%r14, %rdx
	movq	%rbp, %rcx
	callq	sqlite3_mprintf
	movq	%rax, %rbp
	movq	16(%r12), %rdi
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	fprintf
	movq	%rbp, %rdi
	callq	sqlite3_free
	jmp	.LBB13_207
.LBB13_31:                              # %.lr.ph.i.prol
	movq	(%rsp), %rdx            # 8-byte Reload
	movb	(%rdx), %dl
	movb	%dl, (%rax)
	cmpb	$34, %dl
	jne	.LBB13_33
# BB#32:
	movb	$34, 1(%rax)
	addq	$2, %rax
	jmp	.LBB13_34
.LBB13_33:
	incq	%rax
.LBB13_34:                              # %.lr.ph.i.prol.loopexit
	movl	$1, %edx
	cmpq	$1, %rcx
	je	.LBB13_43
.LBB13_35:                              # %.lr.ph.preheader.i.new
	subq	%rdx, %rcx
	movq	(%rsp), %rsi            # 8-byte Reload
	leaq	1(%rsi,%rdx), %rdx
	.p2align	4, 0x90
.LBB13_36:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rdx), %ebx
	movb	%bl, (%rax)
	cmpb	$34, -1(%rdx)
	jne	.LBB13_38
# BB#37:                                #   in Loop: Header=BB13_36 Depth=1
	movb	$34, 1(%rax)
	addq	$2, %rax
	jmp	.LBB13_39
	.p2align	4, 0x90
.LBB13_38:                              #   in Loop: Header=BB13_36 Depth=1
	incq	%rax
.LBB13_39:                              # %.lr.ph.i.1464
                                        #   in Loop: Header=BB13_36 Depth=1
	movzbl	(%rdx), %ebx
	movb	%bl, (%rax)
	cmpb	$34, (%rdx)
	jne	.LBB13_41
# BB#40:                                #   in Loop: Header=BB13_36 Depth=1
	movb	$34, 1(%rax)
	addq	$2, %rax
	jmp	.LBB13_42
	.p2align	4, 0x90
.LBB13_41:                              #   in Loop: Header=BB13_36 Depth=1
	incq	%rax
.LBB13_42:                              #   in Loop: Header=BB13_36 Depth=1
	addq	$2, %rdx
	addq	$-2, %rcx
	jne	.LBB13_36
.LBB13_43:                              # %.loopexit
	movw	$34, (%rax)
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbp
.LBB13_44:                              # %appendText.exit178.thread
	leal	3(%rbp), %eax
	movslq	%eax, %rsi
	movq	%r14, %rdi
	callq	realloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB13_46
# BB#45:
	movslq	%ebp, %rax
	movw	$15145, (%rbx,%rax)     # imm = 0x3B29
	movb	$0, 2(%rbx,%rax)
	movq	(%r12), %rdi
	leaq	8(%rsp), %rcx
	movl	$-1, %edx
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	callq	sqlite3_prepare
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB13_47
.LBB13_46:                              # %appendText.exit184.thread
	movq	(%r12), %rdi
	leaq	8(%rsp), %rcx
	xorl	%esi, %esi
	movl	$-1, %edx
	xorl	%r8d, %r8d
	callq	sqlite3_prepare
	movl	%eax, %ebp
.LBB13_47:
	movq	(%rsp), %rbx            # 8-byte Reload
	testl	%ebp, %ebp
	jne	.LBB13_161
# BB#48:
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.LBB13_161
# BB#49:
	xorl	%r15d, %r15d
	xorl	%edi, %edi
	movl	$26, %esi
	callq	realloc
	testq	%rax, %rax
	je	.LBB13_51
# BB#50:
	movups	.L.str.132+9(%rip), %xmm0
	movups	%xmm0, 9(%rax)
	movdqu	.L.str.132(%rip), %xmm0
	movdqu	%xmm0, (%rax)
	movb	$0, 25(%rax)
	movq	%rax, %r15
.LBB13_51:                              # %appendText.exit189
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rbp
	leal	3(%rbp), %edx
	testl	%ebp, %ebp
	jle	.LBB13_61
# BB#52:                                # %.lr.ph62.preheader.i191
	movl	%ebp, %eax
	cmpq	$8, %rax
	jb	.LBB13_58
# BB#54:                                # %min.iters.checked337
	movl	%ebp, %esi
	andl	$7, %esi
	movq	%rax, %rcx
	subq	%rsi, %rcx
	je	.LBB13_58
# BB#55:                                # %vector.ph341
	movd	%edx, %xmm0
	leaq	4(%rbx), %rdx
	pxor	%xmm2, %xmm2
	movdqa	.LCPI13_0(%rip), %xmm3  # xmm3 = [34,34,34,34]
	movq	%rcx, %rdi
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB13_56:                              # %vector.body333
                                        # =>This Inner Loop Header: Depth=1
	movd	-4(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3],xmm4[4],xmm2[4],xmm4[5],xmm2[5],xmm4[6],xmm2[6],xmm4[7],xmm2[7]
	punpcklwd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3]
	movd	(%rdx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3],xmm5[4],xmm2[4],xmm5[5],xmm2[5],xmm5[6],xmm2[6],xmm5[7],xmm2[7]
	punpcklwd	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3]
	pcmpeqd	%xmm3, %xmm4
	psrld	$31, %xmm4
	pcmpeqd	%xmm3, %xmm5
	psrld	$31, %xmm5
	paddd	%xmm4, %xmm0
	paddd	%xmm5, %xmm1
	addq	$8, %rdx
	addq	$-8, %rdi
	jne	.LBB13_56
# BB#57:                                # %middle.block334
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %edx
	testq	%rsi, %rsi
	jne	.LBB13_59
	jmp	.LBB13_61
.LBB13_58:
	xorl	%ecx, %ecx
.LBB13_59:                              # %.lr.ph62.i197.preheader
	subq	%rcx, %rax
	addq	%rbx, %rcx
	.p2align	4, 0x90
.LBB13_60:                              # %.lr.ph62.i197
                                        # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	cmpb	$34, (%rcx)
	sete	%sil
	addl	%esi, %edx
	incq	%rcx
	decq	%rax
	jne	.LBB13_60
.LBB13_61:                              # %.loopexit.i199
	movslq	%edx, %rsi
	xorl	%r14d, %r14d
	xorl	%edi, %edi
	callq	realloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB13_110
# BB#62:
	leaq	1(%r13), %rax
	testl	%ebp, %ebp
	movb	$34, (%r13)
	jle	.LBB13_65
# BB#63:                                # %.lr.ph.preheader.i201
	movl	%ebp, %ecx
	testb	$1, %bpl
	jne	.LBB13_66
# BB#64:
	xorl	%edx, %edx
	cmpq	$1, %rcx
	jne	.LBB13_70
	jmp	.LBB13_78
.LBB13_65:                              # %appendText.exit211.thread313
	leaq	2(%r13), %rcx
	jmp	.LBB13_79
.LBB13_66:                              # %.lr.ph.i204.prol
	movb	(%rbx), %al
	movb	%al, 1(%r13)
	cmpb	$34, %al
	jne	.LBB13_68
# BB#67:
	leaq	3(%r13), %rax
	movb	$34, 2(%r13)
	jmp	.LBB13_69
.LBB13_68:
	leaq	2(%r13), %rax
.LBB13_69:                              # %.lr.ph.i204.prol.loopexit
	movl	$1, %edx
	cmpq	$1, %rcx
	je	.LBB13_78
.LBB13_70:                              # %.lr.ph.preheader.i201.new
	subq	%rdx, %rcx
	leaq	1(%rbx,%rdx), %rdx
	.p2align	4, 0x90
.LBB13_71:                              # %.lr.ph.i204
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rdx), %ebx
	movb	%bl, (%rax)
	cmpb	$34, -1(%rdx)
	jne	.LBB13_73
# BB#72:                                #   in Loop: Header=BB13_71 Depth=1
	movb	$34, 1(%rax)
	addq	$2, %rax
	jmp	.LBB13_74
	.p2align	4, 0x90
.LBB13_73:                              #   in Loop: Header=BB13_71 Depth=1
	incq	%rax
.LBB13_74:                              # %.lr.ph.i204.1461
                                        #   in Loop: Header=BB13_71 Depth=1
	movzbl	(%rdx), %ebx
	movb	%bl, (%rax)
	cmpb	$34, (%rdx)
	jne	.LBB13_76
# BB#75:                                #   in Loop: Header=BB13_71 Depth=1
	movb	$34, 1(%rax)
	addq	$2, %rax
	jmp	.LBB13_77
	.p2align	4, 0x90
.LBB13_76:                              #   in Loop: Header=BB13_71 Depth=1
	incq	%rax
.LBB13_77:                              #   in Loop: Header=BB13_71 Depth=1
	addq	$2, %rdx
	addq	$-2, %rcx
	jne	.LBB13_71
.LBB13_78:                              # %appendText.exit211
	leaq	1(%rax), %rcx
.LBB13_79:
	movb	$34, (%rax)
	movb	$0, (%rcx)
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %rbp
	testq	%r15, %r15
	je	.LBB13_81
# BB#80:
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
	jmp	.LBB13_82
.LBB13_81:
	xorl	%ebx, %ebx
.LBB13_82:
	leal	3(%rbp,%rbx), %edx
	testl	%ebp, %ebp
	jle	.LBB13_92
# BB#83:                                # %.lr.ph62.preheader.i214
	movl	%ebp, %eax
	cmpq	$8, %rax
	jb	.LBB13_89
# BB#85:                                # %min.iters.checked364
	movl	%ebp, %esi
	andl	$7, %esi
	movq	%rax, %rcx
	subq	%rsi, %rcx
	je	.LBB13_89
# BB#86:                                # %vector.ph368
	movd	%edx, %xmm0
	leaq	4(%r13), %rdx
	pxor	%xmm2, %xmm2
	movdqa	.LCPI13_1(%rip), %xmm3  # xmm3 = [39,39,39,39]
	movq	%rcx, %rdi
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB13_87:                              # %vector.body360
                                        # =>This Inner Loop Header: Depth=1
	movd	-4(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3],xmm4[4],xmm2[4],xmm4[5],xmm2[5],xmm4[6],xmm2[6],xmm4[7],xmm2[7]
	punpcklwd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3]
	movd	(%rdx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3],xmm5[4],xmm2[4],xmm5[5],xmm2[5],xmm5[6],xmm2[6],xmm5[7],xmm2[7]
	punpcklwd	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3]
	pcmpeqd	%xmm3, %xmm4
	psrld	$31, %xmm4
	pcmpeqd	%xmm3, %xmm5
	psrld	$31, %xmm5
	paddd	%xmm4, %xmm0
	paddd	%xmm5, %xmm1
	addq	$8, %rdx
	addq	$-8, %rdi
	jne	.LBB13_87
# BB#88:                                # %middle.block361
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %edx
	testq	%rsi, %rsi
	jne	.LBB13_90
	jmp	.LBB13_92
.LBB13_89:
	xorl	%ecx, %ecx
.LBB13_90:                              # %.lr.ph62.i220.preheader
	subq	%rcx, %rax
	addq	%r13, %rcx
	.p2align	4, 0x90
.LBB13_91:                              # %.lr.ph62.i220
                                        # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	cmpb	$39, (%rcx)
	sete	%sil
	addl	%esi, %edx
	incq	%rcx
	decq	%rax
	jne	.LBB13_91
.LBB13_92:                              # %.loopexit.i222
	movslq	%edx, %rsi
	movq	%r15, %rdi
	callq	realloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB13_96
# BB#93:
	movslq	%ebx, %rcx
	leaq	1(%r15,%rcx), %rax
	movb	$39, (%r15,%rcx)
	testl	%ebp, %ebp
	jle	.LBB13_109
# BB#94:                                # %.lr.ph.preheader.i224
	movl	%ebp, %ecx
	testb	$1, %bpl
	jne	.LBB13_97
# BB#95:
	xorl	%edx, %edx
	cmpq	$1, %rcx
	jne	.LBB13_101
	jmp	.LBB13_109
.LBB13_96:
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	jmp	.LBB13_113
.LBB13_97:                              # %.lr.ph.i227.prol
	movb	(%r13), %dl
	movb	%dl, (%rax)
	cmpb	$39, %dl
	jne	.LBB13_99
# BB#98:
	movb	$39, 1(%rax)
	addq	$2, %rax
	jmp	.LBB13_100
.LBB13_99:
	incq	%rax
.LBB13_100:                             # %.lr.ph.i227.prol.loopexit
	movl	$1, %edx
	cmpq	$1, %rcx
	je	.LBB13_109
.LBB13_101:                             # %.lr.ph.preheader.i224.new
	subq	%rdx, %rcx
	leaq	1(%r13,%rdx), %rdx
	.p2align	4, 0x90
.LBB13_102:                             # %.lr.ph.i227
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rdx), %ebx
	movb	%bl, (%rax)
	cmpb	$39, -1(%rdx)
	jne	.LBB13_104
# BB#103:                               #   in Loop: Header=BB13_102 Depth=1
	movb	$39, 1(%rax)
	addq	$2, %rax
	jmp	.LBB13_105
.LBB13_104:                             #   in Loop: Header=BB13_102 Depth=1
	incq	%rax
.LBB13_105:                             # %.lr.ph.i227.1458
                                        #   in Loop: Header=BB13_102 Depth=1
	movzbl	(%rdx), %ebx
	movb	%bl, (%rax)
	cmpb	$39, (%rdx)
	jne	.LBB13_107
# BB#106:                               #   in Loop: Header=BB13_102 Depth=1
	movb	$39, 1(%rax)
	addq	$2, %rax
	jmp	.LBB13_108
.LBB13_107:                             #   in Loop: Header=BB13_102 Depth=1
	incq	%rax
.LBB13_108:                             #   in Loop: Header=BB13_102 Depth=1
	addq	$2, %rdx
	addq	$-2, %rcx
	jne	.LBB13_102
.LBB13_109:                             # %._crit_edge.i232
	movw	$39, (%rax)
.LBB13_110:                             # %appendText.exit234
	testq	%r15, %r15
	je	.LBB13_112
# BB#111:
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r14
	jmp	.LBB13_113
.LBB13_112:
	xorl	%r15d, %r15d
.LBB13_113:                             # %appendText.exit234.thread
	leal	19(%r14), %eax
	movslq	%eax, %rsi
	movq	%r15, %rdi
	callq	realloc
	movq	%r14, %rcx
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB13_115
# BB#114:
	movslq	%ecx, %rax
	movdqu	.L.str.133(%rip), %xmm0
	movdqu	%xmm0, (%r14,%rax)
	movw	$8316, 16(%r14,%rax)    # imm = 0x207C
	movb	$0, 18(%r14,%rax)
	jmp	.LBB13_116
.LBB13_115:
	xorl	%r14d, %r14d
.LBB13_116:                             # %appendText.exit240
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rdi
	callq	sqlite3_step
	movq	8(%rsp), %rdi
	cmpl	$100, %eax
	jne	.LBB13_158
# BB#117:                               # %.lr.ph.preheader
	movl	$12, %r12d
	.p2align	4, 0x90
.LBB13_118:                             # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_128 Depth 2
                                        #     Child Loop BB13_132 Depth 2
                                        #     Child Loop BB13_143 Depth 2
	movl	$1, %esi
	callq	sqlite3_column_text
	movq	%rax, %r15
	testq	%r14, %r14
	movl	$0, %ebp
	je	.LBB13_120
# BB#119:                               #   in Loop: Header=BB13_118 Depth=1
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbp
.LBB13_120:                             #   in Loop: Header=BB13_118 Depth=1
	leal	7(%rbp), %eax
	movslq	%eax, %rsi
	movq	%r14, %rdi
	callq	realloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB13_122
# BB#121:                               #   in Loop: Header=BB13_118 Depth=1
	movslq	%ebp, %rax
	movw	$10341, 4(%rbx,%rax)    # imm = 0x2865
	movl	$1953461617, (%rbx,%rax) # imm = 0x746F7571
	movb	$0, 6(%rbx,%rax)
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r13
	jmp	.LBB13_123
	.p2align	4, 0x90
.LBB13_122:                             # %appendText.exit246.thread
                                        #   in Loop: Header=BB13_118 Depth=1
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbp
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
.LBB13_123:                             #   in Loop: Header=BB13_118 Depth=1
	leal	3(%rbp,%r13), %edx
	testl	%ebp, %ebp
	jle	.LBB13_133
# BB#124:                               # %.lr.ph62.preheader.i249
                                        #   in Loop: Header=BB13_118 Depth=1
	movl	%ebp, %eax
	cmpq	$8, %rax
	jb	.LBB13_130
# BB#126:                               # %min.iters.checked391
                                        #   in Loop: Header=BB13_118 Depth=1
	movl	%ebp, %esi
	andl	$7, %esi
	movq	%rax, %rcx
	subq	%rsi, %rcx
	pxor	%xmm4, %xmm4
	movdqa	.LCPI13_0(%rip), %xmm5  # xmm5 = [34,34,34,34]
	je	.LBB13_130
# BB#127:                               # %vector.ph395
                                        #   in Loop: Header=BB13_118 Depth=1
	movd	%edx, %xmm1
	leaq	4(%r15), %rdx
	pxor	%xmm0, %xmm0
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB13_128:                             # %vector.body387
                                        #   Parent Loop BB13_118 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	-4(%rdx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1],xmm2[2],xmm4[2],xmm2[3],xmm4[3],xmm2[4],xmm4[4],xmm2[5],xmm4[5],xmm2[6],xmm4[6],xmm2[7],xmm4[7]
	punpcklwd	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1],xmm2[2],xmm4[2],xmm2[3],xmm4[3]
	movd	(%rdx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	punpcklbw	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1],xmm3[2],xmm4[2],xmm3[3],xmm4[3],xmm3[4],xmm4[4],xmm3[5],xmm4[5],xmm3[6],xmm4[6],xmm3[7],xmm4[7]
	punpcklwd	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1],xmm3[2],xmm4[2],xmm3[3],xmm4[3]
	pcmpeqd	%xmm5, %xmm2
	psrld	$31, %xmm2
	pcmpeqd	%xmm5, %xmm3
	psrld	$31, %xmm3
	paddd	%xmm2, %xmm1
	paddd	%xmm3, %xmm0
	addq	$8, %rdx
	addq	$-8, %rdi
	jne	.LBB13_128
# BB#129:                               # %middle.block388
                                        #   in Loop: Header=BB13_118 Depth=1
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %edx
	testq	%rsi, %rsi
	jne	.LBB13_131
	jmp	.LBB13_133
	.p2align	4, 0x90
.LBB13_130:                             #   in Loop: Header=BB13_118 Depth=1
	xorl	%ecx, %ecx
.LBB13_131:                             # %.lr.ph62.i255.preheader
                                        #   in Loop: Header=BB13_118 Depth=1
	subq	%rcx, %rax
	addq	%r15, %rcx
	.p2align	4, 0x90
.LBB13_132:                             # %.lr.ph62.i255
                                        #   Parent Loop BB13_118 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%esi, %esi
	cmpb	$34, (%rcx)
	sete	%sil
	addl	%esi, %edx
	incq	%rcx
	decq	%rax
	jne	.LBB13_132
.LBB13_133:                             # %.loopexit.i257
                                        #   in Loop: Header=BB13_118 Depth=1
	movslq	%edx, %rsi
	movq	%rbx, %rdi
	callq	realloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB13_137
# BB#134:                               #   in Loop: Header=BB13_118 Depth=1
	movslq	%r13d, %rcx
	leaq	1(%r14,%rcx), %rax
	movb	$34, (%r14,%rcx)
	testl	%ebp, %ebp
	jle	.LBB13_150
# BB#135:                               # %.lr.ph.preheader.i259
                                        #   in Loop: Header=BB13_118 Depth=1
	movl	%ebp, %ecx
	testb	$1, %bpl
	jne	.LBB13_138
# BB#136:                               #   in Loop: Header=BB13_118 Depth=1
	xorl	%edx, %edx
	cmpq	$1, %rcx
	jne	.LBB13_142
	jmp	.LBB13_150
	.p2align	4, 0x90
.LBB13_137:                             #   in Loop: Header=BB13_118 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB13_151
.LBB13_138:                             # %.lr.ph.i262.prol
                                        #   in Loop: Header=BB13_118 Depth=1
	movb	(%r15), %dl
	movb	%dl, (%rax)
	cmpb	$34, (%r15)
	jne	.LBB13_140
# BB#139:                               #   in Loop: Header=BB13_118 Depth=1
	movb	$34, 1(%rax)
	addq	$2, %rax
	jmp	.LBB13_141
.LBB13_140:                             #   in Loop: Header=BB13_118 Depth=1
	incq	%rax
.LBB13_141:                             # %.lr.ph.i262.prol.loopexit
                                        #   in Loop: Header=BB13_118 Depth=1
	movl	$1, %edx
	cmpq	$1, %rcx
	je	.LBB13_150
.LBB13_142:                             # %.lr.ph.preheader.i259.new
                                        #   in Loop: Header=BB13_118 Depth=1
	subq	%rdx, %rcx
	leaq	1(%r15,%rdx), %rdx
	.p2align	4, 0x90
.LBB13_143:                             # %.lr.ph.i262
                                        #   Parent Loop BB13_118 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rdx), %ebx
	movb	%bl, (%rax)
	cmpb	$34, -1(%rdx)
	jne	.LBB13_145
# BB#144:                               #   in Loop: Header=BB13_143 Depth=2
	movb	$34, 1(%rax)
	addq	$2, %rax
	jmp	.LBB13_146
	.p2align	4, 0x90
.LBB13_145:                             #   in Loop: Header=BB13_143 Depth=2
	incq	%rax
.LBB13_146:                             # %.lr.ph.i262.1455
                                        #   in Loop: Header=BB13_143 Depth=2
	movzbl	(%rdx), %ebx
	movb	%bl, (%rax)
	cmpb	$34, (%rdx)
	jne	.LBB13_148
# BB#147:                               #   in Loop: Header=BB13_143 Depth=2
	movb	$34, 1(%rax)
	addq	$2, %rax
	jmp	.LBB13_149
	.p2align	4, 0x90
.LBB13_148:                             #   in Loop: Header=BB13_143 Depth=2
	incq	%rax
.LBB13_149:                             #   in Loop: Header=BB13_143 Depth=2
	addq	$2, %rdx
	addq	$-2, %rcx
	jne	.LBB13_143
.LBB13_150:                             # %._crit_edge.i267
                                        #   in Loop: Header=BB13_118 Depth=1
	movw	$34, (%rax)
.LBB13_151:                             # %appendText.exit269
                                        #   in Loop: Header=BB13_118 Depth=1
	movq	8(%rsp), %rdi
	callq	sqlite3_step
	movl	%eax, %r15d
	cmpl	$100, %r15d
	movl	$.L.str.136, %r13d
	movl	$.L.str.135, %eax
	cmoveq	%rax, %r13
	movl	$2, %ebx
	cmoveq	%r12, %rbx
	testq	%r14, %r14
	je	.LBB13_153
# BB#152:                               #   in Loop: Header=BB13_118 Depth=1
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbp
	jmp	.LBB13_154
	.p2align	4, 0x90
.LBB13_153:                             #   in Loop: Header=BB13_118 Depth=1
	xorl	%ebp, %ebp
.LBB13_154:                             #   in Loop: Header=BB13_118 Depth=1
	leal	1(%rbp,%rbx), %eax
	movslq	%eax, %rsi
	movq	%r14, %rdi
	callq	realloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB13_156
# BB#155:                               #   in Loop: Header=BB13_118 Depth=1
	movslq	%ebp, %rdi
	leal	(%rbx,%rbp), %ebp
	addq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movslq	%ebp, %rax
	movb	$0, (%r14,%rax)
	jmp	.LBB13_157
	.p2align	4, 0x90
.LBB13_156:                             #   in Loop: Header=BB13_118 Depth=1
	xorl	%r14d, %r14d
.LBB13_157:                             # %appendText.exit275.backedge
                                        #   in Loop: Header=BB13_118 Depth=1
	cmpl	$100, %r15d
	movq	8(%rsp), %rdi
	je	.LBB13_118
.LBB13_158:                             # %appendText.exit275._crit_edge
	callq	sqlite3_finalize
	testl	%eax, %eax
	je	.LBB13_162
# BB#159:
	testq	%r14, %r14
	je	.LBB13_161
# BB#160:                               # %.critedge176
	movq	%r14, %rdi
	callq	free
.LBB13_161:                             # %.critedge175
	movl	$1, %ebx
	jmp	.LBB13_207
.LBB13_162:
	testq	%r14, %r14
	je	.LBB13_164
# BB#163:
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbp
	jmp	.LBB13_165
.LBB13_164:
	xorl	%ebp, %ebp
.LBB13_165:
	movq	16(%rsp), %r12          # 8-byte Reload
	leal	14(%rbp), %eax
	movslq	%eax, %rsi
	movq	%r14, %rdi
	callq	realloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB13_167
# BB#166:
	movslq	%ebp, %rax
	movabsq	$2314935211546517543, %rcx # imm = 0x20204D4F52462027
	movq	%rcx, 5(%rbx,%rax)
	movabsq	$5053081839613279356, %rcx # imm = 0x4620272927207C7C
	movq	%rcx, (%rbx,%rax)
	movb	$0, 13(%rbx,%rax)
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	strlen
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r14
	jmp	.LBB13_168
.LBB13_167:                             # %appendText.exit281.thread
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	strlen
	movq	%rax, %r15
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
.LBB13_168:
	leal	3(%r15,%r14), %edx
	testl	%r15d, %r15d
	jle	.LBB13_178
# BB#169:                               # %.lr.ph62.preheader.i284
	movl	%r15d, %eax
	cmpq	$8, %rax
	jb	.LBB13_175
# BB#171:                               # %min.iters.checked418
	movl	%r15d, %esi
	andl	$7, %esi
	movq	%rax, %rcx
	subq	%rsi, %rcx
	je	.LBB13_175
# BB#172:                               # %vector.ph422
	movd	%edx, %xmm0
	movq	(%rsp), %rdx            # 8-byte Reload
	leaq	4(%rdx), %rdx
	pxor	%xmm2, %xmm2
	movdqa	.LCPI13_0(%rip), %xmm3  # xmm3 = [34,34,34,34]
	movq	%rcx, %rdi
	pxor	%xmm1, %xmm1
.LBB13_173:                             # %vector.body414
                                        # =>This Inner Loop Header: Depth=1
	movd	-4(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3],xmm4[4],xmm2[4],xmm4[5],xmm2[5],xmm4[6],xmm2[6],xmm4[7],xmm2[7]
	punpcklwd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3]
	movd	(%rdx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3],xmm5[4],xmm2[4],xmm5[5],xmm2[5],xmm5[6],xmm2[6],xmm5[7],xmm2[7]
	punpcklwd	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3]
	pcmpeqd	%xmm3, %xmm4
	psrld	$31, %xmm4
	pcmpeqd	%xmm3, %xmm5
	psrld	$31, %xmm5
	paddd	%xmm4, %xmm0
	paddd	%xmm5, %xmm1
	addq	$8, %rdx
	addq	$-8, %rdi
	jne	.LBB13_173
# BB#174:                               # %middle.block415
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %edx
	testq	%rsi, %rsi
	jne	.LBB13_176
	jmp	.LBB13_178
.LBB13_175:
	xorl	%ecx, %ecx
.LBB13_176:                             # %.lr.ph62.i290.preheader
	subq	%rcx, %rax
	addq	(%rsp), %rcx            # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB13_177:                             # %.lr.ph62.i290
                                        # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	cmpb	$34, (%rcx)
	sete	%sil
	addl	%esi, %edx
	incq	%rcx
	decq	%rax
	jne	.LBB13_177
.LBB13_178:                             # %.loopexit.i292
	movslq	%edx, %rsi
	movq	%rbx, %rdi
	callq	realloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB13_182
# BB#179:
	movslq	%r14d, %rcx
	leaq	1(%rbp,%rcx), %rax
	movb	$34, (%rbp,%rcx)
	testl	%r15d, %r15d
	jle	.LBB13_195
# BB#180:                               # %.lr.ph.preheader.i294
	movl	%r15d, %ecx
	testb	$1, %r15b
	jne	.LBB13_183
# BB#181:
	xorl	%edx, %edx
	movq	(%rsp), %rsi            # 8-byte Reload
	cmpq	$1, %rcx
	jne	.LBB13_187
	jmp	.LBB13_195
.LBB13_182:
	xorl	%ebp, %ebp
	jmp	.LBB13_196
.LBB13_183:                             # %.lr.ph.i297.prol
	movq	(%rsp), %rsi            # 8-byte Reload
	movb	(%rsi), %dl
	movb	%dl, (%rax)
	cmpb	$34, (%rsi)
	jne	.LBB13_185
# BB#184:
	movb	$34, 1(%rax)
	addq	$2, %rax
	jmp	.LBB13_186
.LBB13_185:
	incq	%rax
.LBB13_186:                             # %.lr.ph.i297.prol.loopexit
	movl	$1, %edx
	cmpq	$1, %rcx
	je	.LBB13_195
.LBB13_187:                             # %.lr.ph.preheader.i294.new
	subq	%rdx, %rcx
	leaq	1(%rsi,%rdx), %rdx
.LBB13_188:                             # %.lr.ph.i297
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rdx), %ebx
	movb	%bl, (%rax)
	cmpb	$34, -1(%rdx)
	jne	.LBB13_190
# BB#189:                               #   in Loop: Header=BB13_188 Depth=1
	movb	$34, 1(%rax)
	addq	$2, %rax
	jmp	.LBB13_191
.LBB13_190:                             #   in Loop: Header=BB13_188 Depth=1
	incq	%rax
.LBB13_191:                             # %.lr.ph.i297.1452
                                        #   in Loop: Header=BB13_188 Depth=1
	movzbl	(%rdx), %ebx
	movb	%bl, (%rax)
	cmpb	$34, (%rdx)
	jne	.LBB13_193
# BB#192:                               #   in Loop: Header=BB13_188 Depth=1
	movb	$34, 1(%rax)
	addq	$2, %rax
	jmp	.LBB13_194
.LBB13_193:                             #   in Loop: Header=BB13_188 Depth=1
	incq	%rax
.LBB13_194:                             #   in Loop: Header=BB13_188 Depth=1
	addq	$2, %rdx
	addq	$-2, %rcx
	jne	.LBB13_188
.LBB13_195:                             # %._crit_edge.i302
	movw	$34, (%rax)
.LBB13_196:                             # %appendText.exit304
	movq	(%r12), %rsi
	movq	16(%r12), %rdi
	movq	%rbp, %rdx
	callq	run_table_dump_query
	cmpl	$11, %eax
	jne	.LBB13_204
# BB#197:
	testq	%rbp, %rbp
	je	.LBB13_199
# BB#198:
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rbx
	jmp	.LBB13_200
.LBB13_199:
	xorl	%ebx, %ebx
.LBB13_200:
	leal	21(%rbx), %eax
	movslq	%eax, %rsi
	movq	%rbp, %rdi
	callq	realloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB13_202
# BB#201:
	movslq	%ebx, %rax
	movdqu	.L.str.138(%rip), %xmm0
	movdqu	%xmm0, (%rbp,%rax)
	movl	$1129530692, 16(%rbp,%rax) # imm = 0x43534544
	movb	$0, 20(%rbp,%rax)
	jmp	.LBB13_203
.LBB13_202:
	xorl	%ebp, %ebp
.LBB13_203:                             # %appendText.exit310
	movq	(%r12), %rsi
	movq	16(%r12), %rdi
	movq	%rbp, %rdx
	callq	run_table_dump_query
.LBB13_204:
	testq	%rbp, %rbp
	je	.LBB13_206
# BB#205:
	movq	%rbp, %rdi
	callq	free
.LBB13_206:
	xorl	%ebx, %ebx
.LBB13_207:
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	dump_callback, .Lfunc_end13-dump_callback
	.cfi_endproc

	.p2align	4, 0x90
	.type	output_csv,@function
output_csv:                             # @output_csv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi124:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi125:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi126:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi127:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi128:
	.cfi_def_cfa_offset 64
.Lcfi129:
	.cfi_offset %rbx, -56
.Lcfi130:
	.cfi_offset %r12, -48
.Lcfi131:
	.cfi_offset %r13, -40
.Lcfi132:
	.cfi_offset %r14, -32
.Lcfi133:
	.cfi_offset %r15, -24
.Lcfi134:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	16(%r15), %rbp
	testq	%rbx, %rbx
	je	.LBB14_1
# BB#2:
	movl	%r14d, 4(%rsp)          # 4-byte Spill
	leaq	48(%r15), %r12
	movq	%r12, %rdi
	callq	strlen
	movb	(%rbx), %cl
	testb	%cl, %cl
	je	.LBB14_14
# BB#3:                                 # %.lr.ph
	cmpl	$1, %eax
	jne	.LBB14_4
# BB#9:                                 # %.lr.ph.split.us.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB14_10:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%cl, %eax
	cmpb	$0, needCsvQuote(%rax)
	jne	.LBB14_14
# BB#11:                                #   in Loop: Header=BB14_10 Depth=1
	cmpb	(%r12), %cl
	je	.LBB14_14
# BB#12:                                #   in Loop: Header=BB14_10 Depth=1
	movzbl	1(%rbx,%r14), %ecx
	incq	%r14
	testb	%cl, %cl
	jne	.LBB14_10
	jmp	.LBB14_13
.LBB14_1:
	leaq	868(%r15), %rdi
	movq	%rbp, %rsi
	callq	fputs
	testl	%r14d, %r14d
	jne	.LBB14_23
	jmp	.LBB14_22
.LBB14_4:                               # %.lr.ph.split.preheader
	movslq	%eax, %r13
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB14_5:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%cl, %eax
	cmpb	$0, needCsvQuote(%rax)
	jne	.LBB14_14
# BB#6:                                 #   in Loop: Header=BB14_5 Depth=1
	cmpb	(%r12), %cl
	jne	.LBB14_8
# BB#7:                                 #   in Loop: Header=BB14_5 Depth=1
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB14_14
.LBB14_8:                               #   in Loop: Header=BB14_5 Depth=1
	movzbl	1(%rbx,%r14), %ecx
	incq	%r14
	testb	%cl, %cl
	jne	.LBB14_5
.LBB14_13:                              # %._crit_edge
	testl	%r14d, %r14d
	je	.LBB14_14
# BB#20:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	fputs
	movl	4(%rsp), %r14d          # 4-byte Reload
	testl	%r14d, %r14d
	jne	.LBB14_23
	jmp	.LBB14_22
.LBB14_14:                              # %.thread
	movl	$34, %edi
	movq	%rbp, %rsi
	callq	_IO_putc
	movl	4(%rsp), %r14d          # 4-byte Reload
	jmp	.LBB14_15
	.p2align	4, 0x90
.LBB14_19:                              #   in Loop: Header=BB14_15 Depth=1
	movsbl	%al, %edi
	movq	%rbp, %rsi
	callq	_IO_putc
	incq	%rbx
.LBB14_15:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %eax
	cmpb	$34, %al
	je	.LBB14_18
# BB#16:                                #   in Loop: Header=BB14_15 Depth=1
	testb	%al, %al
	jne	.LBB14_19
	jmp	.LBB14_17
	.p2align	4, 0x90
.LBB14_18:                              #   in Loop: Header=BB14_15 Depth=1
	movl	$34, %edi
	movq	%rbp, %rsi
	callq	_IO_putc
	movzbl	(%rbx), %eax
	jmp	.LBB14_19
.LBB14_17:
	movl	$34, %edi
	movq	%rbp, %rsi
	callq	_IO_putc
	testl	%r14d, %r14d
	je	.LBB14_22
.LBB14_23:
	movq	16(%r15), %rsi
	addq	$48, %r15
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fputs                   # TAILCALL
.LBB14_22:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	output_csv, .Lfunc_end14-output_csv
	.cfi_endproc

	.type	Argv0,@object           # @Argv0
	.local	Argv0
	.comm	Argv0,8,8
	.type	stdin_is_interactive,@object # @stdin_is_interactive
	.data
	.p2align	2
stdin_is_interactive:
	.long	1                       # 0x1
	.size	stdin_is_interactive, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"-separator"
	.size	.L.str, 11

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"-nullvalue"
	.size	.L.str.1, 11

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"-init"
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	":memory:"
	.size	.L.str.3, 9

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"-html"
	.size	.L.str.4, 6

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"-list"
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"-line"
	.size	.L.str.6, 6

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"-column"
	.size	.L.str.7, 8

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"-csv"
	.size	.L.str.8, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	","
	.size	.L.str.9, 2

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%.*s"
	.size	.L.str.10, 5

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"-header"
	.size	.L.str.11, 8

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"-noheader"
	.size	.L.str.12, 10

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"-echo"
	.size	.L.str.13, 6

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"-bail"
	.size	.L.str.14, 6

	.type	bail_on_error,@object   # @bail_on_error
	.local	bail_on_error
	.comm	bail_on_error,4,4
	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"-version"
	.size	.L.str.15, 9

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"%s\n"
	.size	.L.str.16, 4

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"-interactive"
	.size	.L.str.17, 13

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"-batch"
	.size	.L.str.18, 7

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"-help"
	.size	.L.str.19, 6

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"--help"
	.size	.L.str.20, 7

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"%s: unknown option: %s\n"
	.size	.L.str.21, 24

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Use -help for a list of options.\n"
	.size	.L.str.22, 34

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"SQL error: %s\n"
	.size	.L.str.23, 15

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"SQLite version %s\nEnter \".help\" for instructions\n"
	.size	.L.str.24, 50

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"%s/.sqlite_history"
	.size	.L.str.25, 19

	.type	db,@object              # @db
	.local	db
	.comm	db,8,8
	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"error closing database: %s\n"
	.size	.L.str.26, 28

	.type	mainPrompt,@object      # @mainPrompt
	.local	mainPrompt
	.comm	mainPrompt,20,16
	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"sqlite> "
	.size	.L.str.28, 9

	.type	continuePrompt,@object  # @continuePrompt
	.local	continuePrompt
	.comm	continuePrompt,20,16
	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"   ...> "
	.size	.L.str.29, 9

	.type	seenInterrupt,@object   # @seenInterrupt
	.local	seenInterrupt
	.comm	seenInterrupt,4,4
	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"shellstatic"
	.size	.L.str.30, 12

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Unable to open database \"%s\": %s\n"
	.size	.L.str.31, 34

	.type	zShellStatic,@object    # @zShellStatic
	.local	zShellStatic
	.comm	zShellStatic,8,8
	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"%s: cannot locate your home directory!\n"
	.size	.L.str.32, 40

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"%s: out of memory!\n"
	.size	.L.str.33, 20

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"%s/.sqliterc"
	.size	.L.str.34, 13

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"rb"
	.size	.L.str.35, 3

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"-- Loading resources from %s\n"
	.size	.L.str.36, 30

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"Usage: %s [OPTIONS] FILENAME [SQL]\nFILENAME is the name of an SQLite database. A new database is created\nif the file does not previously exist.\n"
	.size	.L.str.37, 145

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"OPTIONS include:\n%s"
	.size	.L.str.38, 20

	.type	zOptions,@object        # @zOptions
	.section	.rodata,"a",@progbits
	.p2align	4
zOptions:
	.asciz	"   -init filename       read/process named file\n   -echo                print commands before execution\n   -[no]header          turn headers on or off\n   -bail                stop after hitting an error\n   -interactive         force interactive I/O\n   -batch               force batch I/O\n   -column              set output mode to 'column'\n   -csv                 set output mode to 'csv'\n   -html                set output mode to HTML\n   -line                set output mode to 'line'\n   -list                set output mode to 'list'\n   -separator 'x'       set output field separator (|)\n   -nullvalue 'text'    set text string for NULL values\n   -version             show SQLite version\n"
	.size	zOptions, 694

	.type	.L.str.40,@object       # @.str.40
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.40:
	.asciz	"bail"
	.size	.L.str.40, 5

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"databases"
	.size	.L.str.41, 10

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"PRAGMA database_list; "
	.size	.L.str.42, 23

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"Error: %s\n"
	.size	.L.str.43, 11

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"dump"
	.size	.L.str.44, 5

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"BEGIN TRANSACTION;\n"
	.size	.L.str.45, 20

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"SELECT name, type, sql FROM sqlite_master WHERE sql NOT NULL AND type=='table'"
	.size	.L.str.46, 79

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"SELECT sql FROM sqlite_master WHERE sql NOT NULL AND type IN ('index','trigger','view')"
	.size	.L.str.47, 88

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"SELECT name, type, sql FROM sqlite_master WHERE tbl_name LIKE shellstatic() AND type=='table'  AND sql NOT NULL"
	.size	.L.str.48, 112

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"SELECT sql FROM sqlite_master WHERE sql NOT NULL  AND type IN ('index','trigger','view')  AND tbl_name LIKE shellstatic()"
	.size	.L.str.49, 122

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"PRAGMA writable_schema=OFF;\n"
	.size	.L.str.50, 29

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"COMMIT;\n"
	.size	.L.str.51, 9

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"echo"
	.size	.L.str.52, 5

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"exit"
	.size	.L.str.53, 5

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"explain"
	.size	.L.str.54, 8

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"header"
	.size	.L.str.55, 7

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"headers"
	.size	.L.str.56, 8

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"help"
	.size	.L.str.57, 5

	.type	zHelp,@object           # @zHelp
	.data
	.p2align	4
zHelp:
	.asciz	".bail ON|OFF           Stop after hitting an error.  Default OFF\n.databases             List names and files of attached databases\n.dump ?TABLE? ...      Dump the database in an SQL text format\n.echo ON|OFF           Turn command echo on or off\n.exit                  Exit this program\n.explain ON|OFF        Turn output mode suitable for EXPLAIN on or off.\n.header(s) ON|OFF      Turn display of headers on or off\n.help                  Show this message\n.import FILE TABLE     Import data from FILE into TABLE\n.indices TABLE         Show names of all indices on TABLE\n.mode MODE ?TABLE?     Set output mode where MODE is one of:\n                         csv      Comma-separated values\n                         column   Left-aligned columns.  (See .width)\n                         html     HTML <table> code\n                         insert   SQL insert statements for TABLE\n                         line     One value per line\n                         list     Values delimited by .separator string\n                         tabs     Tab-separated values\n                         tcl      TCL list elements\n.nullvalue STRING      Print STRING in place of NULL values\n.output FILENAME       Send output to FILENAME\n.output stdout         Send output to the screen\n.prompt MAIN CONTINUE  Replace the standard prompts\n.quit                  Exit this program\n.read FILENAME         Execute SQL in FILENAME\n.schema ?TABLE?        Show the CREATE statements\n.separator STRING      Change separator used by output mode and .import\n.show                  Show the current values for various settings\n.tables ?PATTERN?      List names of tables matching a LIKE pattern\n.timeout MS            Try opening locked tables for MS milliseconds\n.timer ON|OFF          Turn the CPU timer measurement on or off\n.width NUM NUM ...     Set column widths for \"column\" mode\n"
	.size	zHelp, 1855

	.type	.L.str.58,@object       # @.str.58
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.58:
	.asciz	"import"
	.size	.L.str.58, 7

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"non-null separator required for import\n"
	.size	.L.str.59, 40

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"SELECT * FROM '%q'"
	.size	.L.str.60, 19

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"INSERT INTO '%q' VALUES(?"
	.size	.L.str.61, 26

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"cannot open file: %s\n"
	.size	.L.str.62, 22

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"BEGIN"
	.size	.L.str.63, 6

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"COMMIT"
	.size	.L.str.64, 7

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"%s line %d: expected %d columns of data but found %d\n"
	.size	.L.str.65, 54

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"ROLLBACK"
	.size	.L.str.66, 9

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"indices"
	.size	.L.str.67, 8

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"SELECT name FROM sqlite_master WHERE type='index' AND tbl_name LIKE shellstatic() UNION ALL SELECT name FROM sqlite_temp_master WHERE type='index' AND tbl_name LIKE shellstatic() ORDER BY 1"
	.size	.L.str.68, 190

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"mode"
	.size	.L.str.69, 5

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"line"
	.size	.L.str.70, 5

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"lines"
	.size	.L.str.71, 6

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"column"
	.size	.L.str.72, 7

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"columns"
	.size	.L.str.73, 8

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"list"
	.size	.L.str.74, 5

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"html"
	.size	.L.str.75, 5

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"tcl"
	.size	.L.str.76, 4

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"csv"
	.size	.L.str.77, 4

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"tabs"
	.size	.L.str.78, 5

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"\t"
	.size	.L.str.79, 2

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"insert"
	.size	.L.str.80, 7

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"table"
	.size	.L.str.81, 6

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"mode should be one of: column csv html insert line list tabs tcl\n"
	.size	.L.str.82, 66

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"nullvalue"
	.size	.L.str.83, 10

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"output"
	.size	.L.str.84, 7

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"stdout"
	.size	.L.str.85, 7

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"wb"
	.size	.L.str.86, 3

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"can't write to \"%s\"\n"
	.size	.L.str.87, 21

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"%s"
	.size	.L.str.88, 3

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"prompt"
	.size	.L.str.89, 7

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"quit"
	.size	.L.str.90, 5

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"read"
	.size	.L.str.91, 5

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"can't open \"%s\"\n"
	.size	.L.str.92, 17

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"schema"
	.size	.L.str.93, 7

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"sqlite_master"
	.size	.L.str.94, 14

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"CREATE TABLE sqlite_master (\n  type text,\n  name text,\n  tbl_name text,\n  rootpage integer,\n  sql text\n)"
	.size	.L.str.95, 105

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"sql"
	.size	.L.str.96, 4

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"sqlite_temp_master"
	.size	.L.str.97, 19

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"CREATE TEMP TABLE sqlite_temp_master (\n  type text,\n  name text,\n  tbl_name text,\n  rootpage integer,\n  sql text\n)"
	.size	.L.str.98, 115

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"SELECT sql FROM   (SELECT * FROM sqlite_master UNION ALL   SELECT * FROM sqlite_temp_master) WHERE tbl_name LIKE shellstatic() AND type!='meta' AND sql NOTNULL ORDER BY substr(type,2,1), name"
	.size	.L.str.99, 192

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"SELECT sql FROM   (SELECT * FROM sqlite_master UNION ALL   SELECT * FROM sqlite_temp_master) WHERE type!='meta' AND sql NOTNULL AND name NOT LIKE 'sqlite_%'ORDER BY substr(type,2,1), name"
	.size	.L.str.100, 188

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"separator"
	.size	.L.str.101, 10

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"show"
	.size	.L.str.102, 5

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"%9.9s: %s\n"
	.size	.L.str.103, 11

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"on"
	.size	.L.str.104, 3

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"off"
	.size	.L.str.105, 4

	.type	modeDescr,@object       # @modeDescr
	.section	.rodata,"a",@progbits
	.p2align	4
modeDescr:
	.quad	.L.str.70
	.quad	.L.str.72
	.quad	.L.str.74
	.quad	.L.str.139
	.quad	.L.str.75
	.quad	.L.str.80
	.quad	.L.str.76
	.quad	.L.str.77
	.size	modeDescr, 64

	.type	.L.str.106,@object      # @.str.106
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.106:
	.asciz	"%9.9s: "
	.size	.L.str.106, 8

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"\n"
	.size	.L.str.107, 2

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"width"
	.size	.L.str.108, 6

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"%d "
	.size	.L.str.109, 4

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"tables"
	.size	.L.str.110, 7

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"SELECT name FROM sqlite_master WHERE type IN ('table','view') AND name NOT LIKE 'sqlite_%'UNION ALL SELECT name FROM sqlite_temp_master WHERE type IN ('table','view') ORDER BY 1"
	.size	.L.str.111, 178

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"SELECT name FROM sqlite_master WHERE type IN ('table','view') AND name LIKE '%'||shellstatic()||'%' UNION ALL SELECT name FROM sqlite_temp_master WHERE type IN ('table','view') AND name LIKE '%'||shellstatic()||'%' ORDER BY 1"
	.size	.L.str.112, 226

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.zero	1
	.size	.L.str.113, 1

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"  "
	.size	.L.str.114, 3

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"%s%-*s"
	.size	.L.str.115, 7

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"timeout"
	.size	.L.str.116, 8

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"timer"
	.size	.L.str.117, 6

	.type	enableTimer,@object     # @enableTimer
	.local	enableTimer
	.comm	enableTimer,4,4
	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"unknown command or invalid arguments:  \"%s\". Enter \".help\" for help\n"
	.size	.L.str.118, 69

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"%s ORDER BY rowid DESC"
	.size	.L.str.120, 23

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"sqlite_sequence"
	.size	.L.str.121, 16

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"DELETE FROM sqlite_sequence;\n"
	.size	.L.str.122, 30

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"sqlite_stat1"
	.size	.L.str.123, 13

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"ANALYZE sqlite_master;\n"
	.size	.L.str.124, 24

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"sqlite_"
	.size	.L.str.125, 8

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"CREATE VIRTUAL TABLE"
	.size	.L.str.126, 21

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"PRAGMA writable_schema=ON;\n"
	.size	.L.str.127, 28

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"INSERT INTO sqlite_master(type,name,tbl_name,rootpage,sql)VALUES('table','%q','%q',0,'%q');"
	.size	.L.str.128, 92

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"%s;\n"
	.size	.L.str.129, 5

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"PRAGMA table_info("
	.size	.L.str.130, 19

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"SELECT 'INSERT INTO ' || "
	.size	.L.str.132, 26

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	" || ' VALUES(' || "
	.size	.L.str.133, 19

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"quote("
	.size	.L.str.134, 7

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	") || ',' || "
	.size	.L.str.135, 13

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	") "
	.size	.L.str.136, 3

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"|| ')' FROM  "
	.size	.L.str.137, 14

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	" ORDER BY rowid DESC"
	.size	.L.str.138, 21

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"semi"
	.size	.L.str.139, 5

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"\\%03o"
	.size	.L.str.140, 6

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"%*s = %s\n"
	.size	.L.str.141, 10

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"%-*.*s%s"
	.size	.L.str.142, 9

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"---------------------------------------------------------------------------------------------"
	.size	.L.str.143, 94

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"%s%s"
	.size	.L.str.144, 5

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	";\n"
	.size	.L.str.145, 3

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"<TR>"
	.size	.L.str.146, 5

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"<TH>%s</TH>"
	.size	.L.str.147, 12

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	"</TR>\n"
	.size	.L.str.148, 7

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"<TD>"
	.size	.L.str.149, 5

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"</TD>\n"
	.size	.L.str.150, 7

	.type	.L.str.151,@object      # @.str.151
.L.str.151:
	.asciz	"INSERT INTO %s VALUES("
	.size	.L.str.151, 23

	.type	.L.str.152,@object      # @.str.152
.L.str.152:
	.asciz	"%sNULL"
	.size	.L.str.152, 7

	.type	.L.str.153,@object      # @.str.153
.L.str.153:
	.asciz	");\n"
	.size	.L.str.153, 4

	.type	.L.str.154,@object      # @.str.154
.L.str.154:
	.asciz	"&lt;"
	.size	.L.str.154, 5

	.type	.L.str.155,@object      # @.str.155
.L.str.155:
	.asciz	"&amp;"
	.size	.L.str.155, 6

	.type	needCsvQuote,@object    # @needCsvQuote
	.section	.rodata,"a",@progbits
	.p2align	4
needCsvQuote:
	.ascii	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\000\001\000\000\000\000\001\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.size	needCsvQuote, 256

	.type	.L.str.156,@object      # @.str.156
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.156:
	.asciz	"'%s'"
	.size	.L.str.156, 5

	.type	.L.str.158,@object      # @.str.158
.L.str.158:
	.asciz	"''"
	.size	.L.str.158, 3

	.type	.L.str.159,@object      # @.str.159
.L.str.159:
	.asciz	"%.*s''"
	.size	.L.str.159, 7

	.type	.L.str.160,@object      # @.str.160
.L.str.160:
	.asciz	"HOME"
	.size	.L.str.160, 5

	.type	.L.str.162,@object      # @.str.162
.L.str.162:
	.asciz	"out of memory\n"
	.size	.L.str.162, 15

	.type	.L.str.163,@object      # @.str.163
.L.str.163:
	.asciz	"SQL error near line %d:"
	.size	.L.str.163, 24

	.type	.L.str.164,@object      # @.str.164
.L.str.164:
	.asciz	"SQL error:"
	.size	.L.str.164, 11

	.type	.L.str.165,@object      # @.str.165
.L.str.165:
	.asciz	"%s %s\n"
	.size	.L.str.165, 7

	.type	.L.str.166,@object      # @.str.166
.L.str.166:
	.asciz	"Incomplete SQL: %s\n"
	.size	.L.str.166, 20

	.type	sBegin,@object          # @sBegin
	.local	sBegin
	.comm	sBegin,144,8
	.type	.L.str.167,@object      # @.str.167
.L.str.167:
	.asciz	"CPU Time: user %f sys %f\n"
	.size	.L.str.167, 26

	.type	.L.str.168,@object      # @.str.168
.L.str.168:
	.asciz	"Out of memory!\n"
	.size	.L.str.168, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
