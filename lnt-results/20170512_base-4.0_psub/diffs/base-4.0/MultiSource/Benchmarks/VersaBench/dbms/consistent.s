	.text
	.file	"consistent.bc"
	.globl	consistentKey
	.p2align	4, 0x90
	.type	consistentKey,@function
consistentKey:                          # @consistentKey
	.cfi_startproc
# BB#0:
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	16(%rsi), %xmm0
	ja	.LBB0_8
# BB#1:
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	16(%rdi), %xmm0
	ja	.LBB0_8
# BB#2:
	movss	4(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	20(%rsi), %xmm0
	ja	.LBB0_8
# BB#3:
	movss	4(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	20(%rdi), %xmm0
	ja	.LBB0_8
# BB#4:
	movss	8(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	24(%rsi), %xmm0
	ja	.LBB0_8
# BB#5:
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	24(%rdi), %xmm0
	ja	.LBB0_8
# BB#6:
	movss	12(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	28(%rsi), %xmm0
	ja	.LBB0_8
# BB#7:
	movss	12(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movb	$1, %al
	ucomiss	28(%rdi), %xmm0
	jbe	.LBB0_9
.LBB0_8:
	xorl	%eax, %eax
.LBB0_9:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end0:
	.size	consistentKey, .Lfunc_end0-consistentKey
	.cfi_endproc

	.globl	consistentNonKey
	.p2align	4, 0x90
	.type	consistentNonKey,@function
consistentNonKey:                       # @consistentNonKey
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	callq	strstr
	testq	%rax, %rax
	setne	%al
	popq	%rcx
	retq
.Lfunc_end1:
	.size	consistentNonKey, .Lfunc_end1-consistentNonKey
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
