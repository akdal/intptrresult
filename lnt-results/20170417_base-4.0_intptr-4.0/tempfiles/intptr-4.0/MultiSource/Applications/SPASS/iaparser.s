	.text
	.file	"iaparser.bc"
	.globl	ia_parse
	.p2align	4, 0x90
	.type	ia_parse,@function
ia_parse:                               # @ia_parse
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Lcfi2:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2072, %rsp             # imm = 0x818
.Lcfi3:
	.cfi_offset %rbx, -56
.Lcfi4:
	.cfi_offset %r12, -48
.Lcfi5:
	.cfi_offset %r13, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
	leaq	-512(%rbp), %r14
	leaq	-2112(%rbp), %r15
	movl	$0, ia_nerrs(%rip)
	movl	$-2, ia_char(%rip)
	xorl	%r13d, %r13d
	movl	$200, %r12d
	movq	%r15, -112(%rbp)        # 8-byte Spill
	movq	%r14, %r8
	jmp	.LBB0_2
	.p2align	4, 0x90
.LBB0_1:                                #   in Loop: Header=BB0_2 Depth=1
	addq	$2, %r14
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_61 Depth 2
                                        #     Child Loop BB0_85 Depth 2
                                        #     Child Loop BB0_37 Depth 2
                                        #       Child Loop BB0_46 Depth 3
                                        #       Child Loop BB0_34 Depth 3
                                        #       Child Loop BB0_41 Depth 3
                                        #     Child Loop BB0_97 Depth 2
                                        #     Child Loop BB0_124 Depth 2
                                        #     Child Loop BB0_116 Depth 2
                                        #     Child Loop BB0_102 Depth 2
                                        #     Child Loop BB0_105 Depth 2
                                        #     Child Loop BB0_65 Depth 2
	movw	%r13w, (%r14)
	leaq	-2(%r8,%r12,2), %rax
	cmpq	%rax, %r14
	jb	.LBB0_6
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	cmpq	$9999, %r12             # imm = 0x270F
	ja	.LBB0_164
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	subq	%r8, %r14
	sarq	%r14
	incq	%r14
	addq	%r12, %r12
	cmpq	$10000, %r12            # imm = 0x2710
	movl	$10000, %eax            # imm = 0x2710
	cmovaeq	%rax, %r12
	leaq	(%r12,%r12,4), %rax
	leaq	22(%rax,%rax), %rax
	andq	$-16, %rax
	movq	%rsp, %r15
	subq	%rax, %r15
	movq	%r15, %rsp
	leaq	(%r14,%r14), %rdx
	movq	%r15, %rdi
	movq	%r8, %rsi
	callq	memcpy
	leaq	7(%r12,%r12), %rbx
	andq	$-8, %rbx
	addq	%r15, %rbx
	leaq	(,%r14,8), %rdx
	movq	%rbx, %rdi
	movq	-112(%rbp), %rsi        # 8-byte Reload
	callq	memcpy
	cmpq	%r12, %r14
	jge	.LBB0_144
# BB#5:                                 #   in Loop: Header=BB0_2 Depth=1
	leaq	-8(%rbx,%r14,8), %rax
	leaq	-2(%r15,%r14,2), %r14
	movq	%rbx, -112(%rbp)        # 8-byte Spill
	movq	%r15, %r8
	movq	%rax, %r15
.LBB0_6:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	%r13d, %rbx
	movsbl	yypact(%rbx), %r13d
	cmpl	$-29, %r13d
	je	.LBB0_20
# BB#7:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	ia_char(%rip), %eax
	cmpl	$-2, %eax
	jne	.LBB0_9
# BB#8:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	%r14, -48(%rbp)         # 8-byte Spill
	movq	%r13, %r14
	movq	%r15, %r13
	movq	%r12, %r15
	movq	%r8, %r12
	callq	ia_lex
	movq	%r12, %r8
	movq	%r15, %r12
	movq	%r13, %r15
	movq	%r14, %r13
	movq	-48(%rbp), %r14         # 8-byte Reload
	movl	%eax, ia_char(%rip)
.LBB0_9:                                #   in Loop: Header=BB0_2 Depth=1
	testl	%eax, %eax
	jle	.LBB0_12
# BB#10:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$2, %ecx
	cmpl	$271, %eax              # imm = 0x10F
	ja	.LBB0_13
# BB#11:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	%eax, %rcx
	movzbl	yytranslate(%rcx), %ecx
	jmp	.LBB0_13
.LBB0_12:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$0, ia_char(%rip)
	xorl	%eax, %eax
	xorl	%ecx, %ecx
.LBB0_13:                               #   in Loop: Header=BB0_2 Depth=1
	leal	(%rcx,%r13), %esi
	cmpl	$83, %esi
	ja	.LBB0_20
# BB#14:                                #   in Loop: Header=BB0_2 Depth=1
	movslq	%esi, %rdx
	movsbl	yycheck(%rdx), %edi
	cmpl	%ecx, %edi
	jne	.LBB0_20
# BB#15:                                #   in Loop: Header=BB0_2 Depth=1
	leaq	-80(%rdx), %rcx
	cmpq	$3, %rcx
	jb	.LBB0_137
# BB#16:                                #   in Loop: Header=BB0_2 Depth=1
	cmpl	$35, %esi
	je	.LBB0_162
# BB#17:                                #   in Loop: Header=BB0_2 Depth=1
	testl	%eax, %eax
	je	.LBB0_19
# BB#18:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$-2, ia_char(%rip)
.LBB0_19:                               #   in Loop: Header=BB0_2 Depth=1
	movzbl	yytable(%rdx), %r13d
	movq	ia_lval(%rip), %rax
	movq	%rax, 8(%r15)
	addq	$8, %r15
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_20:                               #   in Loop: Header=BB0_2 Depth=1
	movzbl	yydefact(%rbx), %esi
	testq	%rsi, %rsi
	je	.LBB0_137
# BB#21:                                #   in Loop: Header=BB0_2 Depth=1
	movzbl	yyr2(%rsi), %edi
	movl	$1, %eax
	subq	%rdi, %rax
	movq	(%r15,%rax,8), %rbx
	movl	%esi, %ecx
	addb	$-2, %cl
	cmpb	$34, %cl
	ja	.LBB0_133
# BB#22:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$1, %eax
	movzbl	%cl, %ecx
	jmpq	*.LJTI0_0(,%rcx,8)
.LBB0_23:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r15, %r13
	movq	(%r13), %rax
	movq	%rax, -56(%rbp)         # 8-byte Spill
	movq	%rdi, -48(%rbp)         # 8-byte Spill
	movl	$16, %edi
	movq	%r8, %rbx
	movq	%rsi, %r15
	callq	memory_Malloc
	movq	-48(%rbp), %rdi         # 8-byte Reload
	movq	%r15, %rsi
	movq	%rbx, %r8
	movq	%rax, %rbx
	movq	-56(%rbp), %rax         # 8-byte Reload
	movq	%rax, 8(%rbx)
	movq	%r13, %r15
	movq	$0, (%rbx)
	jmp	.LBB0_133
.LBB0_24:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r15, -88(%rbp)         # 8-byte Spill
	movq	ia_VARLIST(%rip), %rax
	movq	%rax, -56(%rbp)         # 8-byte Spill
	movq	%rdi, -48(%rbp)         # 8-byte Spill
	movl	$16, %edi
	movq	%r12, %r13
	movq	%r8, %r12
	movq	%rsi, %r15
	callq	memory_Malloc
	movq	-48(%rbp), %rdi         # 8-byte Reload
	movq	%r15, %rsi
	movq	%r12, %r8
	movq	%r13, %r12
	movq	$0, 8(%rax)
	movq	-56(%rbp), %rcx         # 8-byte Reload
	movq	%rcx, (%rax)
	movq	-88(%rbp), %r15         # 8-byte Reload
	movq	%rax, ia_VARLIST(%rip)
	movb	$1, ia_VARDECL(%rip)
	jmp	.LBB0_133
.LBB0_75:                               #   in Loop: Header=BB0_2 Depth=1
	movb	$0, ia_VARDECL(%rip)
	jmp	.LBB0_133
.LBB0_25:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r15, %rbx
	movl	fol_NOT(%rip), %eax
	movl	%eax, -56(%rbp)         # 4-byte Spill
	movq	-8(%rbx), %r13
	movq	%rdi, -48(%rbp)         # 8-byte Spill
	movl	$16, %edi
	movq	%r8, -72(%rbp)          # 8-byte Spill
	movq	%rsi, %r15
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	$0, (%rax)
	movl	-56(%rbp), %edi         # 4-byte Reload
	movq	%rax, %rsi
	callq	term_Create
	movq	-48(%rbp), %rdi         # 8-byte Reload
	movq	%r15, %rsi
	movq	%rbx, %r15
	jmp	.LBB0_73
.LBB0_26:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rdi, -48(%rbp)         # 8-byte Spill
	movl	-24(%r15), %edi
	movq	%rsi, %r13
	movq	-8(%r15), %rsi
	movq	%r8, %rbx
	callq	term_Create
	movq	-48(%rbp), %rdi         # 8-byte Reload
	movq	%r13, %rsi
	jmp	.LBB0_71
.LBB0_27:                               #   in Loop: Header=BB0_2 Depth=1
	movq	-24(%r15), %rax
	movq	%rax, -56(%rbp)         # 8-byte Spill
	movq	-8(%r15), %rbx
	movq	%rdi, -48(%rbp)         # 8-byte Spill
	movq	%rbx, %rdi
	movq	%r8, %r13
	movq	%r15, -88(%rbp)         # 8-byte Spill
	movq	%rsi, %r15
	callq	list_Length
	movq	-56(%rbp), %rdi         # 8-byte Reload
	movl	%eax, %esi
	callq	ia_Symbol
	movl	%eax, %edi
	movq	%rbx, %rsi
	jmp	.LBB0_67
.LBB0_28:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rdi, -48(%rbp)         # 8-byte Spill
	movq	%rsi, -64(%rbp)         # 8-byte Spill
	movq	%r8, -72(%rbp)          # 8-byte Spill
	movq	ia_VARLIST(%rip), %rax
	movq	8(%rax), %rdi
	movl	$ia_VarFree, %esi
	callq	list_DeleteWithElement
	movq	ia_VARLIST(%rip), %rax
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	movq	%rcx, ia_VARLIST(%rip)
	movl	-72(%r15), %eax
	movl	%eax, -100(%rbp)        # 4-byte Spill
	movq	-40(%r15), %rbx
	movq	%r15, -88(%rbp)         # 8-byte Spill
	movq	-8(%r15), %rax
	movq	%rax, -96(%rbp)         # 8-byte Spill
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB0_94
# BB#29:                                # %.lr.ph111.i.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, -80(%rbp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, -56(%rbp)         # 8-byte Spill
	jmp	.LBB0_37
.LBB0_30:                               #   in Loop: Header=BB0_37 Depth=2
	movq	%rax, -56(%rbp)         # 8-byte Spill
.LBB0_31:                               # %list_Nconc.exit99.i
                                        #   in Loop: Header=BB0_37 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	$0, (%rax)
	movq	-80(%rbp), %rcx         # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB0_36
# BB#32:                                #   in Loop: Header=BB0_37 Depth=2
	testq	%rax, %rax
	je	.LBB0_50
# BB#33:                                # %.preheader.i91.i.preheader
                                        #   in Loop: Header=BB0_37 Depth=2
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB0_34:                               # %.preheader.i91.i
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_37 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_34
# BB#35:                                #   in Loop: Header=BB0_37 Depth=2
	movq	%rax, (%rcx)
	jmp	.LBB0_50
.LBB0_36:                               #   in Loop: Header=BB0_37 Depth=2
	movq	%rax, -80(%rbp)         # 8-byte Spill
	jmp	.LBB0_50
	.p2align	4, 0x90
.LBB0_37:                               # %.lr.ph111.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_46 Depth 3
                                        #       Child Loop BB0_34 Depth 3
                                        #       Child Loop BB0_41 Depth 3
	movq	8(%rbx), %r13
	movslq	(%r13), %r15
	testq	%r15, %r15
	jle	.LBB0_43
# BB#38:                                #   in Loop: Header=BB0_37 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	cmpq	$0, -56(%rbp)           # 8-byte Folded Reload
	je	.LBB0_48
# BB#39:                                #   in Loop: Header=BB0_37 Depth=2
	testq	%rax, %rax
	je	.LBB0_49
# BB#40:                                # %.preheader.i79.i.preheader
                                        #   in Loop: Header=BB0_37 Depth=2
	movq	-56(%rbp), %rdx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_41:                               # %.preheader.i79.i
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_37 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_41
# BB#42:                                #   in Loop: Header=BB0_37 Depth=2
	movq	%rax, (%rcx)
	jmp	.LBB0_49
	.p2align	4, 0x90
.LBB0_43:                               #   in Loop: Header=BB0_37 Depth=2
	movq	16(%r13), %rax
	movq	8(%rax), %rax
	movslq	(%rax), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	cmpq	$0, -56(%rbp)           # 8-byte Folded Reload
	je	.LBB0_30
# BB#44:                                #   in Loop: Header=BB0_37 Depth=2
	testq	%rax, %rax
	je	.LBB0_31
# BB#45:                                # %.preheader.i97.i.preheader
                                        #   in Loop: Header=BB0_37 Depth=2
	movq	-56(%rbp), %rdx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_46:                               # %.preheader.i97.i
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_37 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_46
# BB#47:                                #   in Loop: Header=BB0_37 Depth=2
	movq	%rax, (%rcx)
	jmp	.LBB0_31
	.p2align	4, 0x90
.LBB0_48:                               #   in Loop: Header=BB0_37 Depth=2
	movq	%rax, -56(%rbp)         # 8-byte Spill
.LBB0_49:                               # %list_Nconc.exit81.i
                                        #   in Loop: Header=BB0_37 Depth=2
	movq	%r13, %rdi
	callq	term_Delete
.LBB0_50:                               # %list_Nconc.exit93.i
                                        #   in Loop: Header=BB0_37 Depth=2
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB0_37
	jmp	.LBB0_95
.LBB0_51:                               #   in Loop: Header=BB0_2 Depth=1
	movl	fol_EQUALITY(%rip), %eax
	jmp	.LBB0_79
.LBB0_52:                               #   in Loop: Header=BB0_2 Depth=1
	movl	fol_IMPLIED(%rip), %eax
	jmp	.LBB0_79
.LBB0_53:                               #   in Loop: Header=BB0_2 Depth=1
	movl	fol_AND(%rip), %eax
	jmp	.LBB0_79
.LBB0_54:                               #   in Loop: Header=BB0_2 Depth=1
	movl	fol_OR(%rip), %eax
	jmp	.LBB0_79
.LBB0_55:                               #   in Loop: Header=BB0_2 Depth=1
	movl	fol_ALL(%rip), %eax
	jmp	.LBB0_79
.LBB0_56:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rdi, -48(%rbp)         # 8-byte Spill
	movq	%r15, %rax
	movq	%rsi, -64(%rbp)         # 8-byte Spill
	movq	%r8, %r13
	movq	(%rax), %rdi
	xorl	%esi, %esi
	callq	ia_Symbol
	movl	%eax, %ebx
	testl	%ebx, %ebx
	jle	.LBB0_166
# BB#57:                                #   in Loop: Header=BB0_2 Depth=1
	xorl	%esi, %esi
	movl	%ebx, %edi
	callq	term_Create
	movq	%rax, %rbx
	movq	%r13, %r8
	jmp	.LBB0_132
.LBB0_58:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rdi, -48(%rbp)         # 8-byte Spill
	movq	%rsi, -64(%rbp)         # 8-byte Spill
	movq	%r8, -72(%rbp)          # 8-byte Spill
	movq	-16(%r15), %rbx
	movq	%r15, %r13
	movq	(%r15), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	testq	%rbx, %rbx
	je	.LBB0_110
# BB#59:                                #   in Loop: Header=BB0_2 Depth=1
	testq	%rax, %rax
	je	.LBB0_93
# BB#60:                                # %.preheader.i337.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, %rdx
	movq	%r13, %r15
	movq	-72(%rbp), %r8          # 8-byte Reload
	movq	-64(%rbp), %rsi         # 8-byte Reload
	movq	-48(%rbp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_61:                               # %.preheader.i337
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_61
	jmp	.LBB0_86
.LBB0_62:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rdi, -48(%rbp)         # 8-byte Spill
	movq	%rsi, -64(%rbp)         # 8-byte Spill
	movq	%r8, -72(%rbp)          # 8-byte Spill
	movq	-16(%r15), %rbx
	movq	%r15, %r13
	movq	(%r15), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	testq	%rbx, %rbx
	je	.LBB0_110
# BB#63:                                #   in Loop: Header=BB0_2 Depth=1
	testq	%rax, %rax
	je	.LBB0_93
# BB#64:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, %rdx
	movq	%r13, %r15
	movq	-72(%rbp), %r8          # 8-byte Reload
	movq	-64(%rbp), %rsi         # 8-byte Reload
	movq	-48(%rbp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_65:                               # %.preheader.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_65
	jmp	.LBB0_86
.LBB0_66:                               #   in Loop: Header=BB0_2 Depth=1
	movq	(%r15), %rbx
	movq	%rdi, -48(%rbp)         # 8-byte Spill
	xorl	%edi, %edi
	movq	%r8, %r13
	movq	%r15, -88(%rbp)         # 8-byte Spill
	movq	%rsi, %r15
	callq	list_Length
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	ia_Symbol
	xorl	%esi, %esi
	movl	%eax, %edi
.LBB0_67:                               #   in Loop: Header=BB0_2 Depth=1
	callq	term_Create
	movq	-48(%rbp), %rdi         # 8-byte Reload
	movq	%r15, %rsi
	movq	-88(%rbp), %r15         # 8-byte Reload
	movq	%r13, %r8
	movq	%rax, %rbx
	jmp	.LBB0_133
.LBB0_68:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rdi, -48(%rbp)         # 8-byte Spill
	movl	fol_TRUE(%rip), %edi
	jmp	.LBB0_70
.LBB0_69:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rdi, -48(%rbp)         # 8-byte Spill
	movl	fol_FALSE(%rip), %edi
.LBB0_70:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r15, %r13
	movq	%rsi, %r15
	xorl	%esi, %esi
	movq	%r8, %rbx
	callq	term_Create
	movq	-48(%rbp), %rdi         # 8-byte Reload
	movq	%r15, %rsi
	movq	%r13, %r15
.LBB0_71:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, %r8
	movq	%rax, %rbx
	jmp	.LBB0_133
.LBB0_72:                               #   in Loop: Header=BB0_2 Depth=1
	movl	-40(%r15), %eax
	movl	%eax, -56(%rbp)         # 4-byte Spill
	movq	-24(%r15), %rax
	movq	%rax, -80(%rbp)         # 8-byte Spill
	movq	-8(%r15), %r13
	movq	%rdi, -48(%rbp)         # 8-byte Spill
	movl	$16, %edi
	movq	%r8, -72(%rbp)          # 8-byte Spill
	movq	%rsi, -64(%rbp)         # 8-byte Spill
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r13, 8(%rbx)
	movq	$0, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	-80(%rbp), %rcx         # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%rbx, (%rax)
	movl	-56(%rbp), %edi         # 4-byte Reload
	movq	%rax, %rsi
	callq	term_Create
	movq	-48(%rbp), %rdi         # 8-byte Reload
	movq	-64(%rbp), %rsi         # 8-byte Reload
.LBB0_73:                               #   in Loop: Header=BB0_2 Depth=1
	movq	-72(%rbp), %r8          # 8-byte Reload
	movq	%rax, %rbx
	jmp	.LBB0_133
.LBB0_76:                               #   in Loop: Header=BB0_2 Depth=1
	movl	fol_EQUIV(%rip), %eax
	jmp	.LBB0_79
.LBB0_77:                               #   in Loop: Header=BB0_2 Depth=1
	movl	fol_IMPLIES(%rip), %eax
	jmp	.LBB0_79
.LBB0_78:                               #   in Loop: Header=BB0_2 Depth=1
	movl	fol_EXIST(%rip), %eax
.LBB0_79:                               #   in Loop: Header=BB0_2 Depth=1
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	andq	%rcx, %rbx
	orq	%rax, %rbx
	jmp	.LBB0_133
.LBB0_80:                               #   in Loop: Header=BB0_2 Depth=1
	movq	(%r15), %rbx
	jmp	.LBB0_133
.LBB0_81:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rdi, -48(%rbp)         # 8-byte Spill
	movl	(%r15), %edi
	movq	%r15, %r13
	movq	%r8, %r15
	movq	%rsi, %rbx
	callq	string_IntToString
	movq	-48(%rbp), %rdi         # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r15, %r8
	movq	%r13, %r15
	movq	%rax, %rbx
	jmp	.LBB0_133
.LBB0_82:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rdi, -48(%rbp)         # 8-byte Spill
	movq	%rsi, -64(%rbp)         # 8-byte Spill
	movq	%r8, -72(%rbp)          # 8-byte Spill
	movq	-16(%r15), %rbx
	movq	%r15, %r13
	movq	(%r15), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	testq	%rbx, %rbx
	je	.LBB0_110
# BB#83:                                #   in Loop: Header=BB0_2 Depth=1
	testq	%rax, %rax
	je	.LBB0_93
# BB#84:                                # %.preheader.i331.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbx, %rdx
	movq	%r13, %r15
	movq	-72(%rbp), %r8          # 8-byte Reload
	movq	-64(%rbp), %rsi         # 8-byte Reload
	movq	-48(%rbp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_85:                               # %.preheader.i331
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_85
.LBB0_86:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB0_133
.LBB0_110:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %rbx
	movq	%r13, %r15
	jmp	.LBB0_131
.LBB0_87:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rdi, -48(%rbp)         # 8-byte Spill
	movq	%r15, %rax
	movq	%rsi, -64(%rbp)         # 8-byte Spill
	movq	%r8, -72(%rbp)          # 8-byte Spill
	movq	-24(%rax), %rdi
	movl	$1, %esi
	callq	ia_Symbol
	testl	%eax, %eax
	jns	.LBB0_165
# BB#88:                                # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	%eax, %ecx
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$2, %ecx
	jne	.LBB0_165
# BB#89:                                #   in Loop: Header=BB0_2 Depth=1
	movl	%eax, %r13d
	movq	-8(%r15), %rdi
	xorl	%esi, %esi
	callq	ia_Symbol
	movl	%eax, %ebx
	testl	%ebx, %ebx
	jle	.LBB0_166
# BB#90:                                #   in Loop: Header=BB0_2 Depth=1
	xorl	%esi, %esi
	movl	%ebx, %edi
	callq	term_Create
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movl	%r13d, %edi
	movq	%rax, %rsi
	callq	term_Create
	jmp	.LBB0_130
.LBB0_91:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB0_133
.LBB0_92:                               #   in Loop: Header=BB0_2 Depth=1
	movq	-8(%r15), %rbx
	jmp	.LBB0_133
.LBB0_93:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %r15
	jmp	.LBB0_131
.LBB0_94:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, -56(%rbp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, -80(%rbp)         # 8-byte Spill
.LBB0_95:                               # %._crit_edge112.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	-56(%rbp), %rdi         # 8-byte Reload
	callq	list_PointerDeleteDuplicates
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_98
# BB#96:                                # %.lr.ph105.i.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB0_97:                               # %.lr.ph105.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%rbx), %edi
	xorl	%esi, %esi
	callq	term_Create
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_97
.LBB0_98:                               # %._crit_edge106.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	-80(%rbp), %rsi         # 8-byte Reload
	testq	%rsi, %rsi
	movq	%r15, -56(%rbp)         # 8-byte Spill
	je	.LBB0_109
# BB#99:                                #   in Loop: Header=BB0_2 Depth=1
	movl	-100(%rbp), %r13d       # 4-byte Reload
	cmpl	%r13d, fol_ALL(%rip)
	jne	.LBB0_112
# BB#100:                               #   in Loop: Header=BB0_2 Depth=1
	movl	fol_OR(%rip), %eax
	movq	-96(%rbp), %rcx         # 8-byte Reload
	cmpl	(%rcx), %eax
	jne	.LBB0_119
# BB#101:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rsi, %rbx
	.p2align	4, 0x90
.LBB0_102:                              # %.lr.ph.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	fol_NOT(%rip), %r13d
	movq	8(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	movl	%r13d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_102
# BB#103:                               #   in Loop: Header=BB0_2 Depth=1
	movq	-96(%rbp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	testq	%rax, %rax
	movq	-88(%rbp), %r15         # 8-byte Reload
	movl	-100(%rbp), %r13d       # 4-byte Reload
	je	.LBB0_107
# BB#104:                               # %.preheader.i85.i.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	-80(%rbp), %rdx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_105:                              # %.preheader.i85.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_105
# BB#106:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, (%rcx)
.LBB0_107:                              # %list_Nconc.exit87.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	-96(%rbp), %rbx         # 8-byte Reload
	movq	-80(%rbp), %rax         # 8-byte Reload
	movq	%rax, 16(%rbx)
	jmp	.LBB0_129
.LBB0_109:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-88(%rbp), %r15         # 8-byte Reload
	movl	-100(%rbp), %r13d       # 4-byte Reload
	movq	-96(%rbp), %rbx         # 8-byte Reload
	jmp	.LBB0_129
.LBB0_112:                              #   in Loop: Header=BB0_2 Depth=1
	cmpl	%r13d, fol_EXIST(%rip)
	movq	-88(%rbp), %r15         # 8-byte Reload
	jne	.LBB0_121
# BB#113:                               #   in Loop: Header=BB0_2 Depth=1
	movl	fol_AND(%rip), %eax
	movq	-96(%rbp), %rcx         # 8-byte Reload
	cmpl	(%rcx), %eax
	movq	%rcx, %rax
	jne	.LBB0_122
# BB#114:                               #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB0_118
# BB#115:                               # %.preheader.i73.i.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rsi, %rdx
	.p2align	4, 0x90
.LBB0_116:                              # %.preheader.i73.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_116
# BB#117:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, (%rcx)
	movq	-80(%rbp), %rsi         # 8-byte Reload
.LBB0_118:                              # %list_Nconc.exit75.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	-96(%rbp), %rbx         # 8-byte Reload
	movq	%rsi, 16(%rbx)
	jmp	.LBB0_129
.LBB0_119:                              #   in Loop: Header=BB0_2 Depth=1
	cmpq	$0, (%rsi)
	movq	-88(%rbp), %r15         # 8-byte Reload
	je	.LBB0_127
# BB#120:                               #   in Loop: Header=BB0_2 Depth=1
	movl	fol_AND(%rip), %edi
	callq	term_Create
	movq	%rax, %r13
	movl	fol_IMPLIES(%rip), %eax
	movl	%eax, -80(%rbp)         # 4-byte Spill
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	-96(%rbp), %rax         # 8-byte Reload
	movq	%rax, 8(%rbx)
	movq	$0, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%rbx, (%rax)
	movl	-100(%rbp), %r13d       # 4-byte Reload
	movl	-80(%rbp), %edi         # 4-byte Reload
	movq	%rax, %rsi
	jmp	.LBB0_128
.LBB0_121:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-96(%rbp), %rbx         # 8-byte Reload
	jmp	.LBB0_129
.LBB0_122:                              #   in Loop: Header=BB0_2 Depth=1
	movl	$16, %edi
	movq	%rax, %rbx
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	testq	%rax, %rax
	je	.LBB0_126
# BB#123:                               # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	-80(%rbp), %rdx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_124:                              # %.preheader.i.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_124
# BB#125:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, (%rcx)
.LBB0_126:                              # %list_Nconc.exit.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	fol_AND(%rip), %edi
	movq	-80(%rbp), %rsi         # 8-byte Reload
	jmp	.LBB0_128
.LBB0_127:                              #   in Loop: Header=BB0_2 Depth=1
	movl	$16, %edi
	movq	%rsi, %rbx
	callq	memory_Malloc
	movq	-96(%rbp), %rcx         # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, (%rbx)
	movl	fol_IMPLIES(%rip), %edi
	movq	%rbx, %rsi
.LBB0_128:                              # %ia_CreateQuantifier.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	callq	term_Create
	movq	%rax, %rbx
.LBB0_129:                              # %ia_CreateQuantifier.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movl	%r13d, %edi
	movq	-56(%rbp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	callq	fol_CreateQuantifier
.LBB0_130:                              #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %rbx
.LBB0_131:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-72(%rbp), %r8          # 8-byte Reload
.LBB0_132:                              #   in Loop: Header=BB0_2 Depth=1
	movq	-64(%rbp), %rsi         # 8-byte Reload
	movq	-48(%rbp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_133:                              #   in Loop: Header=BB0_2 Depth=1
	leaq	(,%rdi,8), %rax
	subq	%rax, %r15
	addq	%rdi, %rdi
	subq	%rdi, %r14
	movq	%rbx, 8(%r15)
	addq	$8, %r15
	movzbl	yyr1(%rsi), %eax
	movsbl	yypgoto-23(%rax), %edx
	movswl	(%r14), %ecx
	addl	%ecx, %edx
	cmpl	$83, %edx
	ja	.LBB0_136
# BB#134:                               #   in Loop: Header=BB0_2 Depth=1
	movslq	%edx, %rdx
	movsbl	yycheck(%rdx), %esi
	cmpl	%ecx, %esi
	jne	.LBB0_136
# BB#135:                               #   in Loop: Header=BB0_2 Depth=1
	movzbl	yytable(%rdx), %r13d
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_136:                              #   in Loop: Header=BB0_2 Depth=1
	movsbl	yydefgoto-23(%rax), %r13d
	jmp	.LBB0_1
.LBB0_137:
	movslq	%r13d, %r14
	incl	ia_nerrs(%rip)
	cmpb	$-28, %r14b
	jl	.LBB0_168
# BB#138:
	movslq	ia_char(%rip), %rax
	cmpq	$271, %rax              # imm = 0x10F
	ja	.LBB0_146
# BB#139:
	movzbl	yytranslate(%rax), %eax
	jmp	.LBB0_147
.LBB0_140:
	cmpq	$0, ia_VARLIST(%rip)
	jne	.LBB0_169
# BB#141:                               # %ia_VarCheck.exit
	movl	$0, symbol_STANDARDVARCOUNTER(%rip)
	movq	-48(%r15), %rbx
	movq	-32(%r15), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, ia_PROOFREQUEST(%rip)
	movq	ia_FLAGS(%rip), %r14
	movl	-16(%r15), %ebx
	movl	$7, %edi
	callq	flag_Minimum
	cmpl	%ebx, %eax
	jge	.LBB0_170
# BB#142:
	movl	$7, %edi
	callq	flag_Maximum
	cmpl	%ebx, %eax
	jle	.LBB0_171
# BB#143:                               # %flag_SetFlagValue.exit
	movl	%ebx, 28(%r14)
	xorl	%eax, %eax
	jmp	.LBB0_145
.LBB0_144:
	movl	$1, %eax
.LBB0_145:                              # %.loopexit
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_146:
	movl	$2, %eax
.LBB0_147:
	movq	%rax, -112(%rbp)        # 8-byte Spill
	negl	%r13d
	xorl	%r12d, %r12d
	testb	%r14b, %r14b
	cmovnsl	%r12d, %r13d
	cmpl	$39, %r13d
	jg	.LBB0_153
# BB#148:                               # %.lr.ph382.preheader
	movslq	%r13d, %r15
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_149:                              # %.lr.ph382
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$1, %r15d
	je	.LBB0_152
# BB#150:                               # %.lr.ph382
                                        #   in Loop: Header=BB0_149 Depth=1
	movsbl	yycheck(%r14,%r15), %eax
	cmpl	%eax, %r15d
	jne	.LBB0_152
# BB#151:                               #   in Loop: Header=BB0_149 Depth=1
	movq	yytname(,%r15,8), %rdi
	callq	strlen
	leaq	15(%rbx,%rax), %rbx
	incl	%r12d
.LBB0_152:                              #   in Loop: Header=BB0_149 Depth=1
	incq	%r15
	cmpq	$40, %r15
	jl	.LBB0_149
	jmp	.LBB0_154
.LBB0_153:
	xorl	%ebx, %ebx
.LBB0_154:                              # %._crit_edge
	movq	-112(%rbp), %rax        # 8-byte Reload
	movq	yytname(,%rax,8), %rdi
	movq	%rdi, -112(%rbp)        # 8-byte Spill
	callq	strlen
	leaq	40(%rbx,%rax), %rax
	andq	$-16, %rax
	movq	%rsp, %rcx
	movq	%rcx, %r15
	subq	%rax, %r15
	negq	%rax
	movq	%r15, %rsp
	leaq	24(%r15), %rdi
	movups	.L.str.4(%rip), %xmm0
	movups	%xmm0, (%rcx,%rax)
	movups	.L.str.4+9(%rip), %xmm0
	movups	%xmm0, 9(%r15)
	movq	-112(%rbp), %rsi        # 8-byte Reload
	callq	stpcpy
	cmpl	$4, %r12d
	jg	.LBB0_161
# BB#155:                               # %._crit_edge
	cmpl	$39, %r13d
	jg	.LBB0_161
# BB#156:                               # %.lr.ph.preheader
	movslq	%r13d, %rbx
	xorl	%r12d, %r12d
	movl	$.L.str.5, %r13d
	.p2align	4, 0x90
.LBB0_157:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$1, %ebx
	je	.LBB0_160
# BB#158:                               # %.lr.ph
                                        #   in Loop: Header=BB0_157 Depth=1
	movsbl	yycheck(%r14,%rbx), %ecx
	cmpl	%ecx, %ebx
	jne	.LBB0_160
# BB#159:                               #   in Loop: Header=BB0_157 Depth=1
	testl	%r12d, %r12d
	movl	$.L.str.6, %esi
	cmoveq	%r13, %rsi
	movq	%rax, %rdi
	callq	stpcpy
	movq	yytname(,%rbx,8), %rsi
	movq	%rax, %rdi
	callq	stpcpy
	incl	%r12d
.LBB0_160:                              #   in Loop: Header=BB0_157 Depth=1
	incq	%rbx
	cmpq	$40, %rbx
	jl	.LBB0_157
.LBB0_161:                              # %.loopexit341
	movq	%r15, %rdi
	callq	ia_error
.LBB0_162:
	xorl	%eax, %eax
	jmp	.LBB0_145
.LBB0_163:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %esi
	movq	-72(%r15), %rdx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB0_164:                              # %.thread
	movl	$.L.str.9, %edi
	callq	ia_error
.LBB0_165:                              # %symbol_IsPredicate.exit.thread
	movq	stdout(%rip), %rdi
	movl	%eax, %ebx
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %r14d
	movl	%ebx, %edi
	callq	symbol_Name
	movq	%rax, %rcx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	movq	%rcx, %rdx
	callq	misc_UserErrorReport
	movl	$.L.str.3, %edi
	jmp	.LBB0_167
.LBB0_166:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %r14d
	movl	%ebx, %edi
	callq	symbol_Name
	movq	%rax, %rcx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	movq	%rcx, %rdx
	callq	misc_UserErrorReport
	movl	$.L.str.2, %edi
.LBB0_167:
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB0_168:
	movl	$.L.str.8, %edi
	callq	ia_error
.LBB0_169:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.56, %esi
	movl	$.L.str.57, %edx
	movl	$450, %ecx              # imm = 0x1C2
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.58, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.59, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.60, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	callq	abort
.LBB0_170:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$7, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.11, %edi
	jmp	.LBB0_172
.LBB0_171:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$7, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.12, %edi
.LBB0_172:
	xorl	%eax, %eax
	movl	%ebx, %esi
	movq	%rcx, %rdx
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end0:
	.size	ia_parse, .Lfunc_end0-ia_parse
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_145
	.quad	.LBB0_140
	.quad	.LBB0_23
	.quad	.LBB0_62
	.quad	.LBB0_66
	.quad	.LBB0_68
	.quad	.LBB0_69
	.quad	.LBB0_25
	.quad	.LBB0_72
	.quad	.LBB0_26
	.quad	.LBB0_27
	.quad	.LBB0_24
	.quad	.LBB0_75
	.quad	.LBB0_28
	.quad	.LBB0_24
	.quad	.LBB0_75
	.quad	.LBB0_163
	.quad	.LBB0_51
	.quad	.LBB0_76
	.quad	.LBB0_52
	.quad	.LBB0_77
	.quad	.LBB0_53
	.quad	.LBB0_54
	.quad	.LBB0_78
	.quad	.LBB0_55
	.quad	.LBB0_80
	.quad	.LBB0_81
	.quad	.LBB0_23
	.quad	.LBB0_82
	.quad	.LBB0_56
	.quad	.LBB0_87
	.quad	.LBB0_91
	.quad	.LBB0_92
	.quad	.LBB0_23
	.quad	.LBB0_58

	.text
	.p2align	4, 0x90
	.type	misc_Error,@function
misc_Error:                             # @misc_Error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	misc_Error, .Lfunc_end1-misc_Error
	.cfi_endproc

	.p2align	4, 0x90
	.type	ia_Symbol,@function
ia_Symbol:                              # @ia_Symbol
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	callq	symbol_Lookup
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB2_11
# BB#1:
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rcx
	incq	%rcx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB2_2
# BB#7:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%r15, (%rax)
	jmp	.LBB2_8
.LBB2_11:
	testl	%r14d, %r14d
	jne	.LBB2_12
# BB#14:
	movq	ia_VARLIST(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_16
	jmp	.LBB2_21
	.p2align	4, 0x90
.LBB2_20:                               # %.critedge1.i
                                        #   in Loop: Header=BB2_16 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB2_21
.LBB2_16:                               # %.lr.ph43.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_18 Depth 2
	movq	8(%rbx), %rbp
	testq	%rbp, %rbp
	jne	.LBB2_18
	jmp	.LBB2_20
	.p2align	4, 0x90
.LBB2_19:                               #   in Loop: Header=BB2_18 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB2_20
.LBB2_18:                               # %.lr.ph.i
                                        #   Parent Loop BB2_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rax
	movq	(%rax), %rdi
	movq	%r15, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB2_19
# BB#23:                                # %.critedge.i
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rcx
	incq	%rcx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB2_24
# BB#29:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%r15, (%rax)
	jmp	.LBB2_30
.LBB2_2:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%r15, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %rdi
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rdx, %rbx
	movq	%rdi, (%rbx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB2_4
# BB#3:
	negq	%rax
	movq	-16(%r15,%rax), %rax
	movq	%rax, (%rcx)
.LBB2_4:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB2_6
# BB#5:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB2_6:
	addq	$-16, %r15
	movq	%r15, %rdi
	callq	free
.LBB2_8:                                # %ia_StringFree.exit
	movl	%ebp, %eax
	negl	%eax
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %rcx
	movslq	%eax, %rbx
	movq	(%rcx,%rbx,8), %rax
	movl	16(%rax), %eax
	cmpl	$-1, %eax
	je	.LBB2_32
# BB#9:                                 # %ia_StringFree.exit
	cmpl	%r14d, %eax
	je	.LBB2_32
# BB#10:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %r14d
	movl	%ebp, %edi
	callq	symbol_Name
	movq	%rax, %rcx
	movl	$.L.str.53, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	movq	%rcx, %rdx
	callq	misc_UserErrorReport
	movq	symbol_SIGNATURE(%rip), %rax
	movq	(%rax,%rbx,8), %rax
	movl	16(%rax), %esi
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB2_21:                               # %.loopexit.i
	cmpb	$1, ia_VARDECL(%rip)
	jne	.LBB2_33
# BB#22:
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r15, (%rbp)
	movl	symbol_STANDARDVARCOUNTER(%rip), %eax
	incl	%eax
	movl	%eax, symbol_STANDARDVARCOUNTER(%rip)
	movl	%eax, 8(%rbp)
	movq	ia_VARLIST(%rip), %rbx
	movq	8(%rbx), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	leaq	8(%rbp), %rcx
	movq	%r14, (%rax)
	movq	%rax, 8(%rbx)
	jmp	.LBB2_31
.LBB2_24:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%r15, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %rdi
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rdx, %rbx
	movq	%rdi, (%rbx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB2_26
# BB#25:
	negq	%rax
	movq	-16(%r15,%rax), %rax
	movq	%rax, (%rcx)
.LBB2_26:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB2_28
# BB#27:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB2_28:
	addq	$-16, %r15
	movq	%r15, %rdi
	callq	free
.LBB2_30:                               # %ia_StringFree.exit.i
	movq	8(%rbp), %rcx
	addq	$8, %rcx
.LBB2_31:                               # %ia_VarLookup.exit
	movl	(%rcx), %ebp
.LBB2_32:                               # %ia_SymCheck.exit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_12:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %esi
	movl	$.L.str.52, %edi
	jmp	.LBB2_13
.LBB2_33:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %esi
	movl	$.L.str.55, %edi
.LBB2_13:
	xorl	%eax, %eax
	movq	%r15, %rdx
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end2:
	.size	ia_Symbol, .Lfunc_end2-ia_Symbol
	.cfi_endproc

	.p2align	4, 0x90
	.type	symbol_Name,@function
symbol_Name:                            # @symbol_Name
	.cfi_startproc
# BB#0:
	negl	%edi
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %edi
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rax
	retq
.Lfunc_end3:
	.size	symbol_Name, .Lfunc_end3-symbol_Name
	.cfi_endproc

	.globl	ia_error
	.p2align	4, 0x90
	.type	ia_error,@function
ia_error:                               # @ia_error
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
.Lcfi19:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	dfg_LINENUMBER(%rip), %esi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end4:
	.size	ia_error, .Lfunc_end4-ia_error
	.cfi_endproc

	.globl	ia_GetNextRequest
	.p2align	4, 0x90
	.type	ia_GetNextRequest,@function
ia_GetNextRequest:                      # @ia_GetNextRequest
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 16
	movq	%rdi, ia_in(%rip)
	movq	$0, ia_PROOFREQUEST(%rip)
	movq	%rsi, ia_FLAGS(%rip)
	callq	ia_parse
	movq	ia_PROOFREQUEST(%rip), %rax
	popq	%rcx
	retq
.Lfunc_end5:
	.size	ia_GetNextRequest, .Lfunc_end5-ia_GetNextRequest
	.cfi_endproc

	.p2align	4, 0x90
	.type	ia_VarFree,@function
ia_VarFree:                             # @ia_VarFree
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -24
.Lcfi25:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	(%r14), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rcx
	incq	%rcx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB6_1
# BB#6:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rbx, (%rax)
	jmp	.LBB6_7
.LBB6_1:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rbx, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %edi
	cmovneq	%rdx, %rdi
	movq	%r8, (%rdi)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB6_3
# BB#2:
	negq	%rax
	movq	-16(%rbx,%rax), %rax
	movq	%rax, (%rcx)
.LBB6_3:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB6_5
# BB#4:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB6_5:
	addq	$-16, %rbx
	movq	%rbx, %rdi
	callq	free
.LBB6_7:                                # %ia_StringFree.exit
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r14, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	ia_VarFree, .Lfunc_end6-ia_VarFree
	.cfi_endproc

	.type	ia_nerrs,@object        # @ia_nerrs
	.comm	ia_nerrs,4,4
	.type	ia_char,@object         # @ia_char
	.comm	ia_char,4,4
	.type	yypact,@object          # @yypact
	.section	.rodata,"a",@progbits
	.p2align	4
yypact:
	.ascii	"\364\f#\000\343\343\343\343\343\343\343\343\343\023\343\343\343\343\024\026()*\000\037\000\000+'\022\b&,\007\343\343\343\t\343\343\343\373.\000\000\343\020\020\343\020\343/0\3435-\343-\3433\343\020\02024\3436\3439:\343\000\000;<\343\343"
	.size	yypact, 77

	.type	yytranslate,@object     # @yytranslate
	.p2align	4
yytranslate:
	.ascii	"\000\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\021\023\002\002\022\002\024\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\025\002\026\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\001\002\003\004\005\006\007\b\t\n\013\f\r\016\017\020"
	.size	yytranslate, 272

	.type	yycheck,@object         # @yycheck
	.p2align	4
yycheck:
	.ascii	"\003\r\036\003\004\005\006\007\b\t\n\013\f\022\016\017\020\026./\0271\031\017\020\022\023\022\023\021\026\017\020=>\000\021\023\022\021+,\003\004\005\006\007\b\t\n\013\f\025\016\017\020\022\021\021\021\025\017\022\022\025\034\023\023GH\021\024\026\023\026\022\022/\023\023\377\377\377>"
	.size	yycheck, 84

	.type	yytable,@object         # @yytable
	.p2align	4
yytable:
	.ascii	"\022\001(\005\006\007\b\t\n\013\f\r\0161\017\020\021266\035: \020\021,-,0\003'\020\021B6\004\027&\030\03145\005\006\007\b\t\n\013\f\r\016\036\017\020\021*\032\033\034$3+>#%;<IJ=ADFEGH9KL\000\000\000C"
	.size	yytable, 84

	.type	ia_lval,@object         # @ia_lval
	.comm	ia_lval,8,8
	.type	yydefact,@object        # @yydefact
	.p2align	4
yydefact:
	.ascii	"\002\000\000\000\001\027\023\024\031\b\032\025\026\000\030\007\034\033\000\000\000\000\006\000\000\000\000\000\000\000\000\000\000\000\004\r\020\000\t!#\000\000\000\000\013\000\000\f\000\"\000\000\005\037\016\035\021$\000\n\000\000\000\000\003\000\036\000\000 \000\000\000\000\017\022"
	.size	yydefact, 77

	.type	yyr2,@object            # @yyr2
	.p2align	4
yyr2:
	.ascii	"\000\002\000\t\001\003\001\001\001\004\006\004\004\000\000\n\000\000\n\001\001\001\001\001\001\001\001\001\001\001\003\001\004\002\003\001\003"
	.size	yyr2, 37

	.type	ia_PROOFREQUEST,@object # @ia_PROOFREQUEST
	.comm	ia_PROOFREQUEST,8,8
	.type	ia_FLAGS,@object        # @ia_FLAGS
	.comm	ia_FLAGS,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n Line %d: SPASS can't handle the quantifier %s.\n"
	.size	.L.str, 50

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\n Line %d: %s"
	.size	.L.str.1, 14

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" is not a variable.\n"
	.size	.L.str.2, 21

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" is not a predicate.\n"
	.size	.L.str.3, 22

	.type	yyr1,@object            # @yyr1
	.section	.rodata,"a",@progbits
	.p2align	4
yyr1:
	.ascii	"\000\027\030\030\031\031\032\032\032\032\032\032\032\033\034\032\035\036\032\037\037\037\037  !!\"\"##$$%%&&"
	.size	yyr1, 37

	.type	yypgoto,@object         # @yypgoto
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
yypgoto:
	.ascii	"\343\343%\375\343\343\343\343\343\343\343\344\036\025\343\343"
	.size	yypgoto, 16

	.type	yydefgoto,@object       # @yydefgoto
	.p2align	4
yydefgoto:
	.ascii	"\377\002!\".?/@\023\024\025\02678\037)"
	.size	yydefgoto, 16

	.type	yytname,@object         # @yytname
	.section	.rodata,"a",@progbits
	.p2align	4
yytname:
	.quad	.L.str.13
	.quad	.L.str.14
	.quad	.L.str.15
	.quad	.L.str.16
	.quad	.L.str.17
	.quad	.L.str.18
	.quad	.L.str.19
	.quad	.L.str.20
	.quad	.L.str.21
	.quad	.L.str.22
	.quad	.L.str.23
	.quad	.L.str.24
	.quad	.L.str.25
	.quad	.L.str.26
	.quad	.L.str.27
	.quad	.L.str.28
	.quad	.L.str.29
	.quad	.L.str.30
	.quad	.L.str.31
	.quad	.L.str.32
	.quad	.L.str.33
	.quad	.L.str.34
	.quad	.L.str.35
	.quad	.L.str.36
	.quad	.L.str.37
	.quad	.L.str.38
	.quad	.L.str.39
	.quad	.L.str.40
	.quad	.L.str.41
	.quad	.L.str.42
	.quad	.L.str.43
	.quad	.L.str.44
	.quad	.L.str.45
	.quad	.L.str.46
	.quad	.L.str.47
	.quad	.L.str.48
	.quad	.L.str.49
	.quad	.L.str.50
	.quad	.L.str.51
	.quad	0
	.size	yytname, 320

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"parse error, unexpected "
	.size	.L.str.4, 25

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	", expecting "
	.size	.L.str.5, 13

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	" or "
	.size	.L.str.6, 5

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"parse error"
	.size	.L.str.8, 12

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"parser stack overflow"
	.size	.L.str.9, 22

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"\n Line %i: %s\n"
	.size	.L.str.10, 15

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"\n Error: Flag value %d is too small for flag %s.\n"
	.size	.L.str.11, 50

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"\n Error: Flag value %d is too large for flag %s.\n"
	.size	.L.str.12, 50

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"$end"
	.size	.L.str.13, 5

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"error"
	.size	.L.str.14, 6

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"$undefined"
	.size	.L.str.15, 11

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"IA_AND"
	.size	.L.str.16, 7

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"IA_EQUAL"
	.size	.L.str.17, 9

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"IA_EQUIV"
	.size	.L.str.18, 9

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"IA_EXISTS"
	.size	.L.str.19, 10

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"IA_FALSE"
	.size	.L.str.20, 9

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"IA_FORALL"
	.size	.L.str.21, 10

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"IA_IMPLIED"
	.size	.L.str.22, 11

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"IA_IMPLIES"
	.size	.L.str.23, 11

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"IA_NOT"
	.size	.L.str.24, 7

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"IA_OR"
	.size	.L.str.25, 6

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"IA_PROVE"
	.size	.L.str.26, 9

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"IA_TRUE"
	.size	.L.str.27, 8

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"IA_NUM"
	.size	.L.str.28, 7

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"IA_ID"
	.size	.L.str.29, 6

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"'('"
	.size	.L.str.30, 4

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"','"
	.size	.L.str.31, 4

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"')'"
	.size	.L.str.32, 4

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"'.'"
	.size	.L.str.33, 4

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"'['"
	.size	.L.str.34, 4

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"']'"
	.size	.L.str.35, 4

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"$accept"
	.size	.L.str.36, 8

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"proofrequest"
	.size	.L.str.37, 13

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"termlist"
	.size	.L.str.38, 9

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"term"
	.size	.L.str.39, 5

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"@1"
	.size	.L.str.40, 3

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"@2"
	.size	.L.str.41, 3

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"@3"
	.size	.L.str.42, 3

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"@4"
	.size	.L.str.43, 3

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"binsymbol"
	.size	.L.str.44, 10

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"nsymbol"
	.size	.L.str.45, 8

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"quantsymbol"
	.size	.L.str.46, 12

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"id"
	.size	.L.str.47, 3

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"qtermlist"
	.size	.L.str.48, 10

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"qterm"
	.size	.L.str.49, 6

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"labellistopt"
	.size	.L.str.50, 13

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"labellist"
	.size	.L.str.51, 10

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"\n Line %d: Undefined symbol %s.\n"
	.size	.L.str.52, 33

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"\n Line %u: Symbol %s"
	.size	.L.str.53, 21

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	" was declared with arity %u.\n"
	.size	.L.str.54, 30

	.type	ia_VARLIST,@object      # @ia_VARLIST
	.local	ia_VARLIST
	.comm	ia_VARLIST,8,8
	.type	ia_VARDECL,@object      # @ia_VARDECL
	.local	ia_VARDECL
	.comm	ia_VARDECL,1,4
	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"\n Line %u: Free Variable %s.\n"
	.size	.L.str.55, 30

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"\n\tError in file %s at line %d\n"
	.size	.L.str.56, 31

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"iaparser.y"
	.size	.L.str.57, 11

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"\n In ia_VarCheck: List of variables should be empty!\n"
	.size	.L.str.58, 54

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"\n Please report this error via email to spass@mpi-sb.mpg.de including\n the SPASS version, input problem, options, operating system.\n"
	.size	.L.str.59, 133

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"\n\n"
	.size	.L.str.60, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
