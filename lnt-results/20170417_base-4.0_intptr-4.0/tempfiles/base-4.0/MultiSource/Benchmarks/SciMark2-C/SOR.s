	.text
	.file	"SOR.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	-4616189618054758400    # double -1
.LCPI0_1:
	.quad	4618441417868443648     # double 6
	.text
	.globl	SOR_num_flops
	.p2align	4, 0x90
	.type	SOR_num_flops,@function
SOR_num_flops:                          # @SOR_num_flops
	.cfi_startproc
# BB#0:
	cvtsi2sdl	%edi, %xmm1
	cvtsi2sdl	%esi, %xmm0
	cvtsi2sdl	%edx, %xmm2
	movsd	.LCPI0_0(%rip), %xmm3   # xmm3 = mem[0],zero
	addsd	%xmm3, %xmm1
	addsd	%xmm3, %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	%xmm2, %xmm0
	mulsd	.LCPI0_1(%rip), %xmm0
	retq
.Lfunc_end0:
	.size	SOR_num_flops, .Lfunc_end0-SOR_num_flops
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4598175219545276416     # double 0.25
.LCPI1_1:
	.quad	4607182418800017408     # double 1
	.text
	.globl	SOR_execute
	.p2align	4, 0x90
	.type	SOR_execute,@function
SOR_execute:                            # @SOR_execute
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	testl	%ecx, %ecx
	jle	.LBB1_13
# BB#1:                                 # %.preheader.lr.ph
	decl	%edi
	cmpl	$2, %edi
	jl	.LBB1_13
# BB#2:                                 # %.preheader.us.preheader
	movsd	.LCPI1_1(%rip), %xmm1   # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	mulsd	.LCPI1_0(%rip), %xmm0
	decl	%esi
	movl	%edi, %r10d
	movl	%esi, %r8d
	movl	%r8d, %r11d
	andl	$1, %r11d
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_5 Depth 2
                                        #       Child Loop BB1_10 Depth 3
	cmpl	$2, %esi
	jl	.LBB1_12
# BB#4:                                 # %.lr.ph.us.us.preheader
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	8(%rdx), %rax
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph.us.us
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_10 Depth 3
	movq	-8(%rdx,%r15,8), %r13
	movq	8(%rdx,%r15,8), %r14
	movsd	(%rax), %xmm3           # xmm3 = mem[0],zero
	movsd	8(%rax), %xmm2          # xmm2 = mem[0],zero
	testq	%r11, %r11
	jne	.LBB1_6
# BB#7:                                 #   in Loop: Header=BB1_5 Depth=2
	movsd	8(%r13), %xmm4          # xmm4 = mem[0],zero
	addsd	8(%r14), %xmm4
	addsd	%xmm3, %xmm4
	movsd	16(%rax), %xmm5         # xmm5 = mem[0],zero
	addsd	%xmm5, %xmm4
	mulsd	%xmm0, %xmm4
	mulsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm3
	addsd	%xmm4, %xmm3
	movsd	%xmm3, 8(%rax)
	movapd	%xmm5, %xmm2
	movl	$2, %ebx
	jmp	.LBB1_8
	.p2align	4, 0x90
.LBB1_6:                                #   in Loop: Header=BB1_5 Depth=2
	movl	$1, %ebx
.LBB1_8:                                # %.prol.loopexit
                                        #   in Loop: Header=BB1_5 Depth=2
	incq	%r15
	cmpl	$2, %esi
	je	.LBB1_11
# BB#9:                                 # %.lr.ph.us.us.new
                                        #   in Loop: Header=BB1_5 Depth=2
	movq	%r8, %r12
	subq	%rbx, %r12
	leaq	16(%rax,%rbx,8), %rdi
	leaq	8(%r13,%rbx,8), %rax
	leaq	8(%r14,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB1_10:                               #   Parent Loop BB1_3 Depth=1
                                        #     Parent Loop BB1_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rax), %xmm4         # xmm4 = mem[0],zero
	addsd	-8(%rbx), %xmm4
	addsd	%xmm3, %xmm4
	movsd	-8(%rdi), %xmm3         # xmm3 = mem[0],zero
	addsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	mulsd	%xmm1, %xmm2
	addsd	%xmm4, %xmm2
	movsd	%xmm2, -16(%rdi)
	movsd	(%rax), %xmm4           # xmm4 = mem[0],zero
	addsd	(%rbx), %xmm4
	addsd	%xmm2, %xmm4
	movsd	(%rdi), %xmm2           # xmm2 = mem[0],zero
	addsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	mulsd	%xmm1, %xmm3
	addsd	%xmm4, %xmm3
	movsd	%xmm3, -8(%rdi)
	addq	$16, %rdi
	addq	$16, %rax
	addq	$16, %rbx
	addq	$-2, %r12
	jne	.LBB1_10
.LBB1_11:                               # %..loopexit_crit_edge.us.us
                                        #   in Loop: Header=BB1_5 Depth=2
	cmpq	%r10, %r15
	movq	%r14, %rax
	jne	.LBB1_5
.LBB1_12:                               # %._crit_edge.us
                                        #   in Loop: Header=BB1_3 Depth=1
	incl	%r9d
	cmpl	%ecx, %r9d
	jne	.LBB1_3
.LBB1_13:                               # %._crit_edge57
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	SOR_execute, .Lfunc_end1-SOR_execute
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
