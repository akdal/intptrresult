	.text
	.file	"gsm_encode.bc"
	.globl	gsm_encode
	.p2align	4, 0x90
	.type	gsm_encode,@function
gsm_encode:                             # @gsm_encode
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$160, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 176
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdx, %rbx
	leaq	48(%rsp), %rax
	movq	%rsp, %r10
	leaq	32(%rsp), %rdx
	leaq	24(%rsp), %rcx
	leaq	8(%rsp), %r8
	leaq	16(%rsp), %r9
	pushq	%rax
.Lcfi3:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi4:
	.cfi_adjust_cfa_offset 8
	callq	Gsm_Coder
	addq	$16, %rsp
.Lcfi5:
	.cfi_adjust_cfa_offset -16
	movzwl	32(%rsp), %eax
	movl	%eax, %ecx
	shrb	$2, %cl
	andb	$15, %cl
	orb	$-48, %cl
	movb	%cl, (%rbx)
	shll	$6, %eax
	movzwl	34(%rsp), %ecx
	andl	$63, %ecx
	orl	%eax, %ecx
	movb	%cl, 1(%rbx)
	movzwl	36(%rsp), %eax
	movzwl	38(%rsp), %ecx
	movl	%ecx, %edx
	shrl	$2, %edx
	andl	$7, %edx
	leal	(%rdx,%rax,8), %eax
	movb	%al, 2(%rbx)
	shll	$6, %ecx
	movzwl	40(%rsp), %eax
	andl	$15, %eax
	leal	(%rcx,%rax,4), %eax
	movzwl	42(%rsp), %ecx
	movl	%ecx, %edx
	shrl	$2, %edx
	andl	$3, %edx
	orl	%eax, %edx
	movb	%dl, 3(%rbx)
	shll	$6, %ecx
	movzwl	44(%rsp), %eax
	andl	$7, %eax
	leal	(%rcx,%rax,8), %eax
	movzwl	46(%rsp), %ecx
	andl	$7, %ecx
	orl	%eax, %ecx
	movb	%cl, 4(%rbx)
	movzwl	24(%rsp), %eax
	movzwl	8(%rsp), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %edx
	leal	(%rdx,%rax,2), %eax
	movb	%al, 5(%rbx)
	shll	$7, %ecx
	movzwl	16(%rsp), %eax
	shll	$5, %eax
	andl	$96, %eax
	orl	%ecx, %eax
	movzwl	(%rsp), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$31, %edx
	orl	%eax, %edx
	movb	%dl, 6(%rbx)
	shll	$7, %ecx
	movzwl	48(%rsp), %eax
	shll	$4, %eax
	andl	$112, %eax
	orl	%ecx, %eax
	movzwl	50(%rsp), %ecx
	andl	$7, %ecx
	leal	(%rax,%rcx,2), %eax
	movl	52(%rsp), %ecx
	movl	%ecx, %edx
	shrl	$2, %edx
	andl	$1, %edx
	orl	%eax, %edx
	movb	%dl, 7(%rbx)
	shll	$6, %ecx
	movzwl	54(%rsp), %eax
	andl	$7, %eax
	leal	(%rcx,%rax,8), %eax
	movzwl	56(%rsp), %ecx
	andl	$7, %ecx
	orl	%eax, %ecx
	movb	%cl, 8(%rbx)
	movzwl	58(%rsp), %eax
	shll	$5, %eax
	movzwl	60(%rsp), %ecx
	andl	$7, %ecx
	leal	(%rax,%rcx,4), %eax
	movzwl	62(%rsp), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$3, %edx
	orl	%eax, %edx
	movb	%dl, 9(%rbx)
	shll	$7, %ecx
	movzwl	64(%rsp), %eax
	shll	$4, %eax
	andl	$112, %eax
	orl	%ecx, %eax
	movzwl	66(%rsp), %ecx
	andl	$7, %ecx
	leal	(%rax,%rcx,2), %eax
	movl	68(%rsp), %ecx
	movl	%ecx, %edx
	shrl	$2, %edx
	andl	$1, %edx
	orl	%eax, %edx
	movb	%dl, 10(%rbx)
	shll	$6, %ecx
	movzwl	70(%rsp), %eax
	andl	$7, %eax
	leal	(%rcx,%rax,8), %eax
	movzwl	72(%rsp), %ecx
	andl	$7, %ecx
	orl	%eax, %ecx
	movb	%cl, 11(%rbx)
	movzwl	26(%rsp), %eax
	movzwl	10(%rsp), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %edx
	leal	(%rdx,%rax,2), %eax
	movb	%al, 12(%rbx)
	shll	$7, %ecx
	movzwl	18(%rsp), %eax
	shll	$5, %eax
	andl	$96, %eax
	orl	%ecx, %eax
	movzwl	2(%rsp), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$31, %edx
	orl	%eax, %edx
	movb	%dl, 13(%rbx)
	shll	$7, %ecx
	movzwl	74(%rsp), %eax
	shll	$4, %eax
	andl	$112, %eax
	orl	%ecx, %eax
	movzwl	76(%rsp), %ecx
	andl	$7, %ecx
	leal	(%rax,%rcx,2), %eax
	movzwl	78(%rsp), %ecx
	movl	%ecx, %edx
	shrl	$2, %edx
	andl	$1, %edx
	orl	%eax, %edx
	movb	%dl, 14(%rbx)
	shll	$6, %ecx
	movzwl	80(%rsp), %eax
	andl	$7, %eax
	leal	(%rcx,%rax,8), %eax
	movzwl	82(%rsp), %ecx
	andl	$7, %ecx
	orl	%eax, %ecx
	movb	%cl, 15(%rbx)
	movzwl	84(%rsp), %eax
	shll	$5, %eax
	movzwl	86(%rsp), %ecx
	andl	$7, %ecx
	leal	(%rax,%rcx,4), %eax
	movl	88(%rsp), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$3, %edx
	orl	%eax, %edx
	movb	%dl, 16(%rbx)
	shll	$7, %ecx
	movzwl	90(%rsp), %eax
	shll	$4, %eax
	andl	$112, %eax
	orl	%ecx, %eax
	movzwl	92(%rsp), %ecx
	andl	$7, %ecx
	leal	(%rax,%rcx,2), %eax
	movzwl	94(%rsp), %ecx
	movl	%ecx, %edx
	shrl	$2, %edx
	andl	$1, %edx
	orl	%eax, %edx
	movb	%dl, 17(%rbx)
	shll	$6, %ecx
	movzwl	96(%rsp), %eax
	andl	$7, %eax
	leal	(%rcx,%rax,8), %eax
	movzwl	98(%rsp), %ecx
	andl	$7, %ecx
	orl	%eax, %ecx
	movb	%cl, 18(%rbx)
	movzwl	28(%rsp), %eax
	movzwl	12(%rsp), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %edx
	leal	(%rdx,%rax,2), %eax
	movb	%al, 19(%rbx)
	shll	$7, %ecx
	movzwl	20(%rsp), %eax
	shll	$5, %eax
	andl	$96, %eax
	orl	%ecx, %eax
	movzwl	4(%rsp), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$31, %edx
	orl	%eax, %edx
	movb	%dl, 20(%rbx)
	shll	$7, %ecx
	movzwl	100(%rsp), %eax
	shll	$4, %eax
	andl	$112, %eax
	orl	%ecx, %eax
	movzwl	102(%rsp), %ecx
	andl	$7, %ecx
	leal	(%rax,%rcx,2), %eax
	movl	104(%rsp), %ecx
	movl	%ecx, %edx
	shrl	$2, %edx
	andl	$1, %edx
	orl	%eax, %edx
	movb	%dl, 21(%rbx)
	shll	$6, %ecx
	movzwl	106(%rsp), %eax
	andl	$7, %eax
	leal	(%rcx,%rax,8), %eax
	movzwl	108(%rsp), %ecx
	andl	$7, %ecx
	orl	%eax, %ecx
	movb	%cl, 22(%rbx)
	movzwl	110(%rsp), %eax
	shll	$5, %eax
	movzwl	112(%rsp), %ecx
	andl	$7, %ecx
	leal	(%rax,%rcx,4), %eax
	movzwl	114(%rsp), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$3, %edx
	orl	%eax, %edx
	movb	%dl, 23(%rbx)
	shll	$7, %ecx
	movzwl	116(%rsp), %eax
	shll	$4, %eax
	andl	$112, %eax
	orl	%ecx, %eax
	movzwl	118(%rsp), %ecx
	andl	$7, %ecx
	leal	(%rax,%rcx,2), %eax
	movl	120(%rsp), %ecx
	movl	%ecx, %edx
	shrl	$2, %edx
	andl	$1, %edx
	orl	%eax, %edx
	movb	%dl, 24(%rbx)
	shll	$6, %ecx
	movzwl	122(%rsp), %eax
	andl	$7, %eax
	leal	(%rcx,%rax,8), %eax
	movzwl	124(%rsp), %ecx
	andl	$7, %ecx
	orl	%eax, %ecx
	movb	%cl, 25(%rbx)
	movzwl	30(%rsp), %eax
	movzwl	14(%rsp), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$1, %edx
	leal	(%rdx,%rax,2), %eax
	movb	%al, 26(%rbx)
	shll	$7, %ecx
	movzwl	22(%rsp), %eax
	shll	$5, %eax
	andl	$96, %eax
	orl	%ecx, %eax
	movzwl	6(%rsp), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$31, %edx
	orl	%eax, %edx
	movb	%dl, 27(%rbx)
	shll	$7, %ecx
	movzwl	126(%rsp), %eax
	shll	$4, %eax
	andl	$112, %eax
	orl	%ecx, %eax
	movzwl	128(%rsp), %ecx
	andl	$7, %ecx
	leal	(%rax,%rcx,2), %eax
	movzwl	130(%rsp), %ecx
	movl	%ecx, %edx
	shrl	$2, %edx
	andl	$1, %edx
	orl	%eax, %edx
	movb	%dl, 28(%rbx)
	shll	$6, %ecx
	movzwl	132(%rsp), %eax
	andl	$7, %eax
	leal	(%rcx,%rax,8), %eax
	movzwl	134(%rsp), %ecx
	andl	$7, %ecx
	orl	%eax, %ecx
	movb	%cl, 29(%rbx)
	movzwl	136(%rsp), %eax
	shll	$5, %eax
	movzwl	138(%rsp), %ecx
	andl	$7, %ecx
	leal	(%rax,%rcx,4), %eax
	movl	140(%rsp), %ecx
	movl	%ecx, %edx
	shrl	%edx
	andl	$3, %edx
	orl	%eax, %edx
	movb	%dl, 30(%rbx)
	shll	$7, %ecx
	movzwl	142(%rsp), %eax
	shll	$4, %eax
	andl	$112, %eax
	orl	%ecx, %eax
	movzwl	144(%rsp), %ecx
	andl	$7, %ecx
	leal	(%rax,%rcx,2), %eax
	movzwl	146(%rsp), %ecx
	movl	%ecx, %edx
	shrl	$2, %edx
	andl	$1, %edx
	orl	%eax, %edx
	movb	%dl, 31(%rbx)
	shll	$6, %ecx
	movzwl	148(%rsp), %eax
	andl	$7, %eax
	leal	(%rcx,%rax,8), %eax
	movzwl	150(%rsp), %ecx
	andl	$7, %ecx
	orl	%eax, %ecx
	movb	%cl, 32(%rbx)
	addq	$160, %rsp
	popq	%rbx
	retq
.Lfunc_end0:
	.size	gsm_encode, .Lfunc_end0-gsm_encode
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
