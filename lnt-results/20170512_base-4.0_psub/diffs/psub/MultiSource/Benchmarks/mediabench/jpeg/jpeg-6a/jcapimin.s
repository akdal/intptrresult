	.text
	.file	"jcapimin.bc"
	.globl	jpeg_CreateCompress
	.p2align	4, 0x90
	.type	jpeg_CreateCompress,@function
jpeg_CreateCompress:                    # @jpeg_CreateCompress
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movq	$0, 8(%rbx)
	cmpl	$61, %esi
	je	.LBB0_2
# BB#1:
	movq	(%rbx), %rax
	movl	$10, 40(%rax)
	movl	$61, 44(%rax)
	movl	%esi, 48(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB0_2:
	cmpq	$496, %r14              # imm = 0x1F0
	je	.LBB0_4
# BB#3:
	movq	(%rbx), %rax
	movl	$19, 40(%rax)
	movl	$496, 44(%rax)          # imm = 0x1F0
	movl	%r14d, 48(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB0_4:                                # %.preheader.preheader
	movq	(%rbx), %r14
	leaq	8(%rbx), %rdi
	xorl	%esi, %esi
	movl	$488, %edx              # imm = 0x1E8
	callq	memset
	movq	%r14, (%rbx)
	movl	$0, 24(%rbx)
	movq	%rbx, %rdi
	callq	jinit_memory_mgr
	movq	$0, 16(%rbx)
	movq	$0, 32(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 160(%rbx)
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movq	$0, 176(%rbx)
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, 56(%rbx)
	movl	$100, 28(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	jpeg_CreateCompress, .Lfunc_end0-jpeg_CreateCompress
	.cfi_endproc

	.globl	jpeg_destroy_compress
	.p2align	4, 0x90
	.type	jpeg_destroy_compress,@function
jpeg_destroy_compress:                  # @jpeg_destroy_compress
	.cfi_startproc
# BB#0:
	jmp	jpeg_destroy            # TAILCALL
.Lfunc_end1:
	.size	jpeg_destroy_compress, .Lfunc_end1-jpeg_destroy_compress
	.cfi_endproc

	.globl	jpeg_abort_compress
	.p2align	4, 0x90
	.type	jpeg_abort_compress,@function
jpeg_abort_compress:                    # @jpeg_abort_compress
	.cfi_startproc
# BB#0:
	jmp	jpeg_abort              # TAILCALL
.Lfunc_end2:
	.size	jpeg_abort_compress, .Lfunc_end2-jpeg_abort_compress
	.cfi_endproc

	.globl	jpeg_suppress_tables
	.p2align	4, 0x90
	.type	jpeg_suppress_tables,@function
jpeg_suppress_tables:                   # @jpeg_suppress_tables
	.cfi_startproc
# BB#0:
	movq	88(%rdi), %rax
	testq	%rax, %rax
	je	.LBB3_2
# BB#1:
	movl	%esi, 128(%rax)
.LBB3_2:
	movq	96(%rdi), %rax
	testq	%rax, %rax
	je	.LBB3_4
# BB#3:
	movl	%esi, 128(%rax)
.LBB3_4:
	movq	104(%rdi), %rax
	testq	%rax, %rax
	je	.LBB3_6
# BB#5:
	movl	%esi, 128(%rax)
.LBB3_6:
	movq	112(%rdi), %rax
	testq	%rax, %rax
	je	.LBB3_8
# BB#7:
	movl	%esi, 128(%rax)
.LBB3_8:                                # %.preheader.preheader25
	movq	120(%rdi), %rax
	testq	%rax, %rax
	je	.LBB3_10
# BB#9:
	movl	%esi, 276(%rax)
.LBB3_10:
	movq	152(%rdi), %rax
	testq	%rax, %rax
	je	.LBB3_12
# BB#11:
	movl	%esi, 276(%rax)
.LBB3_12:                               # %.preheader.122
	movq	128(%rdi), %rax
	testq	%rax, %rax
	je	.LBB3_14
# BB#13:
	movl	%esi, 276(%rax)
.LBB3_14:
	movq	160(%rdi), %rax
	testq	%rax, %rax
	je	.LBB3_16
# BB#15:
	movl	%esi, 276(%rax)
.LBB3_16:                               # %.preheader.223
	movq	136(%rdi), %rax
	testq	%rax, %rax
	je	.LBB3_18
# BB#17:
	movl	%esi, 276(%rax)
.LBB3_18:
	movq	168(%rdi), %rax
	testq	%rax, %rax
	je	.LBB3_20
# BB#19:
	movl	%esi, 276(%rax)
.LBB3_20:                               # %.preheader.324
	movq	144(%rdi), %rax
	testq	%rax, %rax
	je	.LBB3_22
# BB#21:
	movl	%esi, 276(%rax)
.LBB3_22:
	movq	176(%rdi), %rax
	testq	%rax, %rax
	je	.LBB3_24
# BB#23:
	movl	%esi, 276(%rax)
.LBB3_24:
	retq
.Lfunc_end3:
	.size	jpeg_suppress_tables, .Lfunc_end3-jpeg_suppress_tables
	.cfi_endproc

	.globl	jpeg_finish_compress
	.p2align	4, 0x90
	.type	jpeg_finish_compress,@function
jpeg_finish_compress:                   # @jpeg_finish_compress
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	leal	-101(%rax), %ecx
	cmpl	$2, %ecx
	jae	.LBB4_1
# BB#3:
	movl	296(%rbx), %eax
	cmpl	44(%rbx), %eax
	jae	.LBB4_5
# BB#4:
	movq	(%rbx), %rax
	movl	$66, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
	jmp	.LBB4_5
.LBB4_1:
	cmpl	$103, %eax
	je	.LBB4_6
# BB#2:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
	jmp	.LBB4_6
.LBB4_5:
	movq	424(%rbx), %rax
	movq	%rbx, %rdi
	callq	*16(%rax)
.LBB4_6:                                # %.preheader
	movq	424(%rbx), %rax
	cmpl	$0, 28(%rax)
	jne	.LBB4_14
# BB#7:
	movq	%rbx, %rdi
	callq	*(%rax)
	movl	312(%rbx), %eax
	testl	%eax, %eax
	je	.LBB4_5
# BB#8:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB4_11
# BB#10:                                #   in Loop: Header=BB4_9 Depth=1
	movl	%ebp, %edx
	movq	%rdx, 8(%rcx)
	movl	%eax, %eax
	movq	%rax, 16(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB4_11:                               #   in Loop: Header=BB4_9 Depth=1
	movq	448(%rbx), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	*8(%rax)
	testl	%eax, %eax
	jne	.LBB4_13
# BB#12:                                #   in Loop: Header=BB4_9 Depth=1
	movq	(%rbx), %rax
	movl	$22, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB4_13:                               #   in Loop: Header=BB4_9 Depth=1
	incl	%ebp
	movl	312(%rbx), %eax
	cmpl	%eax, %ebp
	jb	.LBB4_9
	jmp	.LBB4_5
.LBB4_14:                               # %._crit_edge40
	movq	456(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	32(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	jpeg_abort              # TAILCALL
.Lfunc_end4:
	.size	jpeg_finish_compress, .Lfunc_end4-jpeg_finish_compress
	.cfi_endproc

	.globl	jpeg_write_marker
	.p2align	4, 0x90
	.type	jpeg_write_marker,@function
jpeg_write_marker:                      # @jpeg_write_marker
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %r15
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	cmpl	$0, 296(%rbx)
	jne	.LBB5_2
# BB#1:
	leal	-101(%rax), %ecx
	cmpl	$3, %ecx
	jb	.LBB5_3
.LBB5_2:                                # %._crit_edge
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB5_3:
	movq	456(%rbx), %rax
	movq	(%rax), %rax
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	movl	%r14d, %ecx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.Lfunc_end5:
	.size	jpeg_write_marker, .Lfunc_end5-jpeg_write_marker
	.cfi_endproc

	.globl	jpeg_write_tables
	.p2align	4, 0x90
	.type	jpeg_write_tables,@function
jpeg_write_tables:                      # @jpeg_write_tables
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 16
.Lcfi20:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	cmpl	$100, %eax
	je	.LBB6_2
# BB#1:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB6_2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	32(%rbx), %rax
	movq	%rbx, %rdi
	callq	*16(%rax)
	movq	%rbx, %rdi
	callq	jinit_marker_writer
	movq	456(%rbx), %rax
	movq	%rbx, %rdi
	callq	*40(%rax)
	movq	32(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	%rbx, %rdi
	popq	%rbx
	jmp	jpeg_abort              # TAILCALL
.Lfunc_end6:
	.size	jpeg_write_tables, .Lfunc_end6-jpeg_write_tables
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
