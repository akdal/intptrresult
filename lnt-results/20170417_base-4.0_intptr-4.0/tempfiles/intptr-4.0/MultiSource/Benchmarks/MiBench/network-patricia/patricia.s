	.text
	.file	"patricia.bc"
	.globl	pat_insert
	.p2align	4, 0x90
	.type	pat_insert,@function
pat_insert:                             # @pat_insert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r9
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	testq	%r14, %r14
	je	.LBB0_29
# BB#1:
	testq	%r9, %r9
	je	.LBB0_29
# BB#2:
	movq	8(%r14), %r15
	testq	%r15, %r15
	je	.LBB0_3
# BB#4:
	movq	(%r15), %rdx
	movq	(%r14), %rdi
	andq	%rdx, %rdi
	movq	%rdi, (%r14)
	movb	17(%r9), %cl
	movsbl	%cl, %r8d
	movq	%r9, %rbx
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	movzbl	%cl, %esi
	movl	$31, %ecx
	subl	%esi, %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	movslq	%ebp, %rcx
	leaq	24(%rbx), %rbp
	addq	$32, %rbx
	testq	%rdi, %rcx
	cmoveq	%rbp, %rbx
	movq	(%rbx), %rbx
	movzbl	17(%rbx), %ecx
	cmpb	%cl, %sil
	jl	.LBB0_5
# BB#6:
	movq	(%rbx), %rbp
	cmpq	%rbp, %rdi
	jne	.LBB0_7
# BB#10:                                # %.preheader
	movzbl	16(%rbx), %edi
	testq	%rdi, %rdi
	je	.LBB0_15
# BB#11:                                # %.lr.ph102
	movq	8(%rbx), %rax
	addq	$8, %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_13:                               # =>This Inner Loop Header: Depth=1
	cmpq	-8(%rax), %rdx
	je	.LBB0_14
# BB#12:                                #   in Loop: Header=BB0_13 Depth=1
	incq	%rcx
	addq	$16, %rax
	cmpq	%rdi, %rcx
	jl	.LBB0_13
.LBB0_15:                               # %._crit_edge103
	shlq	$4, %rdi
	addq	$16, %rdi
	callq	malloc
	cmpb	$0, 16(%rbx)
	movq	%rax, (%rsp)            # 8-byte Spill
	je	.LBB0_16
# BB#17:                                # %.lr.ph
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	movq	%rax, %r13
	jmp	.LBB0_18
	.p2align	4, 0x90
.LBB0_22:                               # %._crit_edge111
                                        #   in Loop: Header=BB0_18 Depth=1
	addq	$16, %r13
	movq	8(%r14), %r15
.LBB0_18:                               # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rcx
	movq	8(%rbx), %rdi
	movslq	%r12d, %rax
	shlq	$4, %rax
	cmpq	(%rdi,%rax), %rcx
	jbe	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_18 Depth=1
	addq	%rax, %rdi
	movl	$16, %edx
	movq	%r13, %rsi
	callq	bcopy
	incl	%r12d
	jmp	.LBB0_21
	.p2align	4, 0x90
.LBB0_20:                               #   in Loop: Header=BB0_18 Depth=1
	movl	$16, %edx
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	bcopy
	movq	8(%r14), %rax
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	movq	%rcx, (%rax)
	movl	$1, %ebp
.LBB0_21:                               #   in Loop: Header=BB0_18 Depth=1
	movzbl	16(%rbx), %eax
	cmpl	%eax, %r12d
	jl	.LBB0_22
# BB#23:                                # %._crit_edge
	testl	%ebp, %ebp
	jne	.LBB0_26
# BB#24:
	addq	$16, %r13
	jmp	.LBB0_25
.LBB0_3:
	xorl	%ebx, %ebx
	jmp	.LBB0_29
.LBB0_7:                                # %.preheader88
	xorq	%rdi, %rbp
	movl	$1, %ebx
	movl	$29, %esi
	jmp	.LBB0_8
	.p2align	4, 0x90
.LBB0_31:                               #   in Loop: Header=BB0_8 Depth=1
	addl	$2, %ebx
	addl	$-2, %esi
.LBB0_8:                                # =>This Inner Loop Header: Depth=1
	leal	1(%rsi), %ecx
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	movslq	%edx, %rcx
	testq	%rcx, %rbp
	jne	.LBB0_9
# BB#27:                                #   in Loop: Header=BB0_8 Depth=1
	leal	1(%rbx), %edx
	cmpl	$32, %edx
	jge	.LBB0_28
# BB#30:                                #   in Loop: Header=BB0_8 Depth=1
	movl	$1, %eax
	movl	%esi, %ecx
	shll	%cl, %eax
	cltq
	testq	%rax, %rbp
	je	.LBB0_31
	jmp	.LBB0_28
.LBB0_9:
	movl	%ebx, %edx
.LBB0_28:                               # %.critedge
	movl	$31, %ecx
	subl	%r8d, %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movslq	%esi, %rcx
	leaq	24(%r9), %rsi
	leaq	32(%r9), %rbx
	testq	%rdi, %rcx
	cmoveq	%rsi, %rbx
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movq	%r9, %rcx
	callq	insertR
	movq	%rax, (%rbx)
	movq	%r14, %rbx
	jmp	.LBB0_29
.LBB0_14:
	movq	8(%r15), %rcx
	movq	%rcx, (%rax)
	movq	%r15, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	jmp	.LBB0_29
.LBB0_16:
	movq	%rax, %r13
.LBB0_25:                               # %._crit_edge.thread
	movq	8(%r14), %rdi
	movl	$16, %edx
	movq	%r13, %rsi
	callq	bcopy
.LBB0_26:
	movq	8(%r14), %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	incb	16(%rbx)
	movq	8(%rbx), %rdi
	callq	free
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, 8(%rbx)
.LBB0_29:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	pat_insert, .Lfunc_end0-pat_insert
	.cfi_endproc

	.p2align	4, 0x90
	.type	insertR,@function
insertR:                                # @insertR
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movsbl	17(%rbx), %eax
	cmpl	%edx, %eax
	jge	.LBB1_2
# BB#1:
	cmpb	17(%rcx), %al
	jle	.LBB1_2
# BB#4:
	movl	$31, %ecx
	subl	%eax, %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	cltq
	leaq	24(%rbx), %rcx
	leaq	32(%rbx), %r14
	testq	(%rsi), %rax
	cmoveq	%rcx, %r14
	movq	(%r14), %rdi
	movq	%rbx, %rcx
	callq	insertR
	movq	%rax, (%r14)
	movq	%rbx, %rax
	jmp	.LBB1_3
.LBB1_2:
	movb	%dl, 17(%rsi)
	movl	$31, %ecx
	subl	%edx, %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	cltq
	andq	(%rsi), %rax
	movq	%rsi, %rcx
	cmovneq	%rbx, %rcx
	testq	%rax, %rax
	movq	%rcx, 24(%rsi)
	cmovneq	%rsi, %rbx
	movq	%rbx, 32(%rsi)
	movq	%rsi, %rax
.LBB1_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	insertR, .Lfunc_end1-insertR
	.cfi_endproc

	.globl	pat_remove
	.p2align	4, 0x90
	.type	pat_remove,@function
pat_remove:                             # @pat_remove
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	pat_remove, .Lfunc_end2-pat_remove
	.cfi_endproc

	.globl	pat_search
	.p2align	4, 0x90
	.type	pat_search,@function
pat_search:                             # @pat_search
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB3_1
# BB#2:                                 # %.preheader.preheader
	movb	17(%rsi), %cl
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsi), %rdx
	movq	(%rdx), %rdx
	andq	%rdi, %rdx
	cmpq	%rdx, (%rsi)
	cmoveq	%rsi, %rax
	movzbl	%cl, %r8d
	movl	$31, %ecx
	subl	%r8d, %ecx
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	movslq	%edx, %rcx
	leaq	24(%rsi), %rdx
	addq	$32, %rsi
	testq	%rdi, %rcx
	cmoveq	%rdx, %rsi
	movq	(%rsi), %rsi
	movzbl	17(%rsi), %ecx
	cmpb	%cl, %r8b
	jl	.LBB3_3
# BB#4:
	movq	8(%rsi), %rcx
	andq	(%rcx), %rdi
	cmpq	%rdi, (%rsi)
	cmoveq	%rsi, %rax
	retq
.LBB3_1:
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	pat_search, .Lfunc_end3-pat_search
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
