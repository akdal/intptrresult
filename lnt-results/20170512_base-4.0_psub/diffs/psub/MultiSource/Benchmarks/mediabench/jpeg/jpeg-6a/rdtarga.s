	.text
	.file	"rdtarga.bc"
	.globl	jinit_read_targa
	.p2align	4, 0x90
	.type	jinit_read_targa,@function
jinit_read_targa:                       # @jinit_read_targa
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$112, %edx
	callq	*(%rax)
	movq	%rbx, 48(%rax)
	movq	$start_input_tga, (%rax)
	movq	$finish_input_tga, 16(%rax)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	jinit_read_targa, .Lfunc_end0-jinit_read_targa
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_input_tga,@function
start_input_tga:                        # @start_input_tga
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 112
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	movq	24(%r13), %rcx
	leaq	16(%rsp), %rdi
	movl	$1, %esi
	movl	$18, %edx
	callq	fread
	cmpq	$18, %rax
	je	.LBB1_2
# BB#1:
	movq	(%r14), %rax
	movl	$42, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB1_2:
	movb	32(%rsp), %al
	cmpb	$15, %al
	jne	.LBB1_4
# BB#3:
	movb	$16, 32(%rsp)
	movb	$16, %al
.LBB1_4:
	movzbl	16(%rsp), %ebp
	movb	17(%rsp), %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	movzbl	18(%rsp), %esi
	movzbl	21(%rsp), %ecx
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	movzbl	22(%rsp), %r12d
	movzbl	28(%rsp), %r9d
	movzbl	29(%rsp), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movzbl	30(%rsp), %r10d
	movzbl	31(%rsp), %r8d
	movzbl	%al, %edx
	movl	%edx, %ecx
	shrl	$3, %ecx
	movl	%ecx, 92(%r13)
	movzbl	33(%rsp), %edi
	cmpb	$39, %dl
	ja	.LBB1_9
# BB#5:
	cmpb	$1, 7(%rsp)             # 1-byte Folded Reload
	ja	.LBB1_9
# BB#6:
	testl	%ecx, %ecx
	je	.LBB1_9
# BB#7:
	testb	$-64, %dil
	jne	.LBB1_9
# BB#8:
	andb	$7, %al
	je	.LBB1_10
.LBB1_9:
	movq	(%r14), %rax
	movl	$1033, 40(%rax)         # imm = 0x409
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movq	%r14, %rdi
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movl	%r8d, %r14d
	movl	%esi, %r15d
	movl	%r9d, 52(%rsp)          # 4-byte Spill
	movl	%r10d, %ebx
	callq	*(%rax)
	movl	%ebx, %r10d
	movl	52(%rsp), %r9d          # 4-byte Reload
	movl	%r15d, %esi
	movl	12(%rsp), %edi          # 4-byte Reload
	movl	%r14d, %r8d
	movq	40(%rsp), %r14          # 8-byte Reload
.LBB1_10:
	shll	$8, 8(%rsp)             # 4-byte Folded Spill
	shll	$8, %r8d
	cmpb	$9, %sil
	jb	.LBB1_12
# BB#11:
	movq	$read_rle_pixel, 80(%r13)
	movq	$0, 96(%r13)
	addl	$-8, %esi
	jmp	.LBB1_13
.LBB1_12:
	movq	$read_non_rle_pixel, 80(%r13)
.LBB1_13:
	movl	8(%rsp), %r15d          # 4-byte Reload
	orl	%r9d, %r15d
	movl	%r15d, %ebx
	orl	%r10d, %r8d
	andl	$32, %edi
	movl	$2, 52(%r14)
	cmpl	$3, %esi
	movl	%r8d, 8(%rsp)           # 4-byte Spill
	je	.LBB1_28
# BB#14:
	cmpl	$2, %esi
	je	.LBB1_21
# BB#15:
	cmpl	$1, %esi
	jne	.LBB1_32
# BB#16:
	cmpb	$1, 7(%rsp)             # 1-byte Folded Reload
	jne	.LBB1_19
# BB#17:
	cmpl	$1, 92(%r13)
	jne	.LBB1_19
# BB#18:
	movl	%edi, %r15d
	movq	$get_8bit_row, 104(%r13)
	jmp	.LBB1_20
.LBB1_21:
	movl	92(%r13), %eax
	cmpl	$4, %eax
	movl	%edi, 12(%rsp)          # 4-byte Spill
	je	.LBB1_25
# BB#22:
	cmpl	$3, %eax
	je	.LBB1_25
# BB#23:
	cmpl	$2, %eax
	jne	.LBB1_26
# BB#24:
	movq	$get_16bit_row, 104(%r13)
	jmp	.LBB1_27
.LBB1_28:
	movl	$1, 52(%r14)
	cmpl	$1, 92(%r13)
	jne	.LBB1_30
# BB#29:
	movl	%edi, %r15d
	movq	$get_8bit_gray_row, 104(%r13)
	jmp	.LBB1_31
.LBB1_32:
	movq	(%r14), %rax
	movl	$1033, 40(%rax)         # imm = 0x409
	movl	%ebx, %r15d
	movl	%edi, %ebx
	movq	%r14, %rdi
	callq	*(%rax)
	movl	%ebx, %eax
	movl	%r15d, %ebx
	movl	$3, %ecx
	jmp	.LBB1_33
.LBB1_25:
	movq	$get_24bit_row, 104(%r13)
	jmp	.LBB1_27
.LBB1_19:
	movl	%edi, %r15d
	movq	(%r14), %rax
	movl	$1033, 40(%rax)         # imm = 0x409
	movq	%r14, %rdi
	callq	*(%rax)
	movl	8(%rsp), %r8d           # 4-byte Reload
.LBB1_20:
	movq	(%r14), %rax
	movl	$1037, 40(%rax)         # imm = 0x40D
	movl	%ebx, 44(%rax)
	movl	%r8d, 48(%rax)
	movl	$1, %esi
	movq	%r14, %rdi
	callq	*8(%rax)
	movl	$3, %ecx
	movl	%r15d, %eax
	jmp	.LBB1_33
.LBB1_30:
	movl	%edi, %r15d
	movq	(%r14), %rax
	movl	$1033, 40(%rax)         # imm = 0x409
	movq	%r14, %rdi
	callq	*(%rax)
	movl	8(%rsp), %r8d           # 4-byte Reload
.LBB1_31:
	movq	(%r14), %rax
	movl	$1036, 40(%rax)         # imm = 0x40C
	movl	%ebx, 44(%rax)
	movl	%r8d, 48(%rax)
	movl	$1, %esi
	movq	%r14, %rdi
	callq	*8(%rax)
	movl	$1, %ecx
	movl	%r15d, %eax
	jmp	.LBB1_33
.LBB1_26:
	movq	(%r14), %rax
	movl	$1033, 40(%rax)         # imm = 0x409
	movq	%r14, %rdi
	callq	*(%rax)
	movl	8(%rsp), %r8d           # 4-byte Reload
.LBB1_27:
	movq	(%r14), %rax
	movl	$1035, 40(%rax)         # imm = 0x40B
	movl	%ebx, 44(%rax)
	movl	%r8d, 48(%rax)
	movl	$1, %esi
	movq	%r14, %rdi
	callq	*8(%rax)
	movl	$3, %ecx
	movl	12(%rsp), %eax          # 4-byte Reload
.LBB1_33:
	shll	$8, %r12d
	testl	%eax, %eax
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	jne	.LBB1_37
# BB#34:
	movq	8(%r14), %rax
	imull	%ebx, %ecx
	movl	$1, %esi
	xorl	%edx, %edx
	movl	$1, %r9d
	movq	%r14, %rdi
	movl	8(%rsp), %r8d           # 4-byte Reload
	callq	*32(%rax)
	movq	%rax, 64(%r13)
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.LBB1_36
# BB#35:
	incl	36(%rax)
.LBB1_36:
	movl	$1, 40(%r13)
	movl	$preload_image, %eax
	jmp	.LBB1_38
.LBB1_37:
	movq	$0, 64(%r13)
	movq	8(%r14), %rax
	movl	%ecx, %edx
	imull	%ebx, %edx
	movl	$1, %esi
	movl	$1, %ecx
	movq	%r14, %rdi
	callq	*16(%rax)
	movq	%rax, 32(%r13)
	movl	$1, 40(%r13)
	movq	104(%r13), %rax
.LBB1_38:
	movl	36(%rsp), %ecx          # 4-byte Reload
	orl	%ecx, %r12d
	movq	%rax, 8(%r13)
	testb	%bpl, %bpl
	je	.LBB1_42
# BB#39:                                # %.lr.ph
	negl	%ebp
	.p2align	4, 0x90
.LBB1_40:                               # =>This Inner Loop Header: Depth=1
	movq	24(%r13), %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB1_41
# BB#60:                                #   in Loop: Header=BB1_40 Depth=1
	movq	48(%r13), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB1_41:                               # %read_byte.exit.backedge
                                        #   in Loop: Header=BB1_40 Depth=1
	incl	%ebp
	jne	.LBB1_40
.LBB1_42:                               # %read_byte.exit._crit_edge
	testl	%r12d, %r12d
	je	.LBB1_56
# BB#43:
	cmpl	$256, %r12d             # imm = 0x100
	ja	.LBB1_45
# BB#44:
	movzbl	19(%rsp), %eax
	movzbl	20(%rsp), %ecx
	shll	$8, %ecx
	orl	%eax, %ecx
	je	.LBB1_46
.LBB1_45:
	movq	(%r14), %rax
	movl	$1032, 40(%rax)         # imm = 0x408
	movq	%r14, %rdi
	callq	*(%rax)
.LBB1_46:
	movl	%ebx, %r15d
	movq	8(%r14), %rax
	movl	$1, %esi
	movl	$3, %ecx
	movq	%r14, %rdi
	movl	%r12d, %edx
	callq	*16(%rax)
	movq	%rax, 56(%r13)
	cmpb	$24, 23(%rsp)
	je	.LBB1_48
# BB#47:
	movq	48(%r13), %rdi
	movq	(%rdi), %rax
	movl	$1032, 40(%rax)         # imm = 0x408
	callq	*(%rax)
.LBB1_48:                               # %.lr.ph.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_49:                               # =>This Inner Loop Header: Depth=1
	movq	24(%r13), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB1_51
# BB#50:                                #   in Loop: Header=BB1_49 Depth=1
	movq	48(%r13), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB1_51:                               # %read_byte.exit.i
                                        #   in Loop: Header=BB1_49 Depth=1
	movq	56(%r13), %rax
	movq	16(%rax), %rax
	movb	%bpl, (%rax,%rbx)
	movq	24(%r13), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB1_53
# BB#52:                                #   in Loop: Header=BB1_49 Depth=1
	movq	48(%r13), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB1_53:                               # %read_byte.exit16.i
                                        #   in Loop: Header=BB1_49 Depth=1
	movq	56(%r13), %rax
	movq	8(%rax), %rax
	movb	%bpl, (%rax,%rbx)
	movq	24(%r13), %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB1_55
# BB#54:                                #   in Loop: Header=BB1_49 Depth=1
	movq	48(%r13), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB1_55:                               # %read_byte.exit17.i
                                        #   in Loop: Header=BB1_49 Depth=1
	movq	56(%r13), %rax
	movq	(%rax), %rax
	movb	%bpl, (%rax,%rbx)
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB1_49
	jmp	.LBB1_59
.LBB1_56:
	movl	%ebx, %r15d
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	je	.LBB1_58
# BB#57:
	movq	(%r14), %rax
	movl	$1033, 40(%rax)         # imm = 0x409
	movq	%r14, %rdi
	callq	*(%rax)
.LBB1_58:
	movq	$0, 56(%r13)
.LBB1_59:                               # %read_colormap.exit
	movl	40(%rsp), %eax          # 4-byte Reload
	movl	%eax, 48(%r14)
	movl	$8, 64(%r14)
	movl	%r15d, 40(%r14)
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	%eax, 44(%r14)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	start_input_tga, .Lfunc_end1-start_input_tga
	.cfi_endproc

	.p2align	4, 0x90
	.type	finish_input_tga,@function
finish_input_tga:                       # @finish_input_tga
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end2:
	.size	finish_input_tga, .Lfunc_end2-finish_input_tga
	.cfi_endproc

	.p2align	4, 0x90
	.type	read_rle_pixel,@function
read_rle_pixel:                         # @read_rle_pixel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -32
.Lcfi19:
	.cfi_offset %r14, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	100(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB3_2
# BB#1:
	decl	%eax
	movl	%eax, 100(%rbx)
	jmp	.LBB3_11
.LBB3_2:
	movq	24(%rbx), %r14
	movl	96(%rbx), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 96(%rbx)
	testl	%eax, %eax
	jg	.LBB3_8
# BB#3:
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB3_5
# BB#4:
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$42, 40(%rax)
	callq	*(%rax)
.LBB3_5:                                # %read_byte.exit
	movl	%ebp, %eax
	andl	$127, %ebp
	testb	%al, %al
	jns	.LBB3_7
# BB#6:
	movl	%ebp, 100(%rbx)
	xorl	%ebp, %ebp
.LBB3_7:
	movl	%ebp, 96(%rbx)
.LBB3_8:                                # %.preheader
	cmpl	$0, 92(%rbx)
	jle	.LBB3_11
# BB#9:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_10:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	_IO_getc
	movb	%al, 88(%rbx,%rbp)
	incq	%rbp
	movslq	92(%rbx), %rax
	cmpq	%rax, %rbp
	jl	.LBB3_10
.LBB3_11:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	read_rle_pixel, .Lfunc_end3-read_rle_pixel
	.cfi_endproc

	.p2align	4, 0x90
	.type	read_non_rle_pixel,@function
read_non_rle_pixel:                     # @read_non_rle_pixel
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	cmpl	$0, 92(%r15)
	jle	.LBB4_3
# BB#1:                                 # %.lr.ph.preheader
	movq	24(%r15), %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	_IO_getc
	movb	%al, 88(%r15,%rbx)
	incq	%rbx
	movslq	92(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB4_2
.LBB4_3:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	read_non_rle_pixel, .Lfunc_end4-read_non_rle_pixel
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_8bit_row,@function
get_8bit_row:                           # @get_8bit_row
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 48
.Lcfi32:
	.cfi_offset %rbx, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	40(%rdi), %ebp
	testl	%ebp, %ebp
	je	.LBB5_3
# BB#1:                                 # %.lr.ph
	movq	56(%r14), %r15
	movq	32(%r14), %rax
	movq	(%rax), %rbx
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	*80(%r14)
	movq	(%r15), %rax
	movzbl	88(%r14), %ecx
	movzbl	(%rax,%rcx), %eax
	movb	%al, (%rbx)
	movq	8(%r15), %rax
	movzbl	(%rax,%rcx), %eax
	movb	%al, 1(%rbx)
	movq	16(%r15), %rax
	movzbl	(%rax,%rcx), %eax
	movb	%al, 2(%rbx)
	addq	$3, %rbx
	decl	%ebp
	jne	.LBB5_2
.LBB5_3:                                # %._crit_edge
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	get_8bit_row, .Lfunc_end5-get_8bit_row
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_16bit_row,@function
get_16bit_row:                          # @get_16bit_row
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 32
.Lcfi39:
	.cfi_offset %rbx, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	40(%rdi), %ebp
	testl	%ebp, %ebp
	je	.LBB6_3
# BB#1:                                 # %.lr.ph
	movq	32(%r14), %rax
	movq	(%rax), %rbx
	addq	$2, %rbx
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	*80(%r14)
	movzbl	88(%r14), %eax
	movzbl	89(%r14), %ecx
	movl	%ecx, %edx
	shll	$8, %edx
	orl	%eax, %edx
	andl	$31, %eax
	movzbl	c5to8bits(%rax), %eax
	movb	%al, (%rbx)
	shrl	$5, %edx
	andl	$31, %edx
	movzbl	c5to8bits(%rdx), %eax
	movb	%al, -1(%rbx)
	shrl	$2, %ecx
	andl	$31, %ecx
	movzbl	c5to8bits(%rcx), %eax
	movb	%al, -2(%rbx)
	addq	$3, %rbx
	decl	%ebp
	jne	.LBB6_2
.LBB6_3:                                # %._crit_edge
	movl	$1, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end6:
	.size	get_16bit_row, .Lfunc_end6-get_16bit_row
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_24bit_row,@function
get_24bit_row:                          # @get_24bit_row
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 32
.Lcfi45:
	.cfi_offset %rbx, -32
.Lcfi46:
	.cfi_offset %r14, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	40(%rdi), %ebp
	testl	%ebp, %ebp
	je	.LBB7_3
# BB#1:                                 # %.lr.ph
	movq	32(%r14), %rax
	movq	(%rax), %rbx
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	*80(%r14)
	movzbl	90(%r14), %eax
	movb	%al, (%rbx)
	movzbl	89(%r14), %eax
	movb	%al, 1(%rbx)
	movzbl	88(%r14), %eax
	movb	%al, 2(%rbx)
	addq	$3, %rbx
	decl	%ebp
	jne	.LBB7_2
.LBB7_3:                                # %._crit_edge
	movl	$1, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end7:
	.size	get_24bit_row, .Lfunc_end7-get_24bit_row
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_8bit_gray_row,@function
get_8bit_gray_row:                      # @get_8bit_gray_row
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 32
.Lcfi51:
	.cfi_offset %rbx, -32
.Lcfi52:
	.cfi_offset %r14, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	40(%rdi), %ebp
	testl	%ebp, %ebp
	je	.LBB8_3
# BB#1:                                 # %.lr.ph
	movq	32(%r14), %rax
	movq	(%rax), %rbx
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	*80(%r14)
	movzbl	88(%r14), %eax
	movb	%al, (%rbx)
	incq	%rbx
	decl	%ebp
	jne	.LBB8_2
.LBB8_3:                                # %._crit_edge
	movl	$1, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end8:
	.size	get_8bit_gray_row, .Lfunc_end8-get_8bit_gray_row
	.cfi_endproc

	.p2align	4, 0x90
	.type	preload_image,@function
preload_image:                          # @preload_image
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi58:
	.cfi_def_cfa_offset 48
.Lcfi59:
	.cfi_offset %rbx, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	16(%rbx), %r15
	movl	44(%rbx), %edx
	testl	%edx, %edx
	je	.LBB9_1
# BB#2:                                 # %.lr.ph
	xorl	%ebp, %ebp
	testq	%r15, %r15
	je	.LBB9_3
	.p2align	4, 0x90
.LBB9_4:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %eax
	movq	%rax, 8(%r15)
	movl	%edx, %eax
	movq	%rax, 16(%r15)
	movq	%rbx, %rdi
	callq	*(%r15)
	movq	8(%rbx), %rax
	movq	64(%r14), %rsi
	movl	$1, %ecx
	movl	$1, %r8d
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	*56(%rax)
	movq	%rax, 32(%r14)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*104(%r14)
	incl	%ebp
	movl	44(%rbx), %edx
	cmpl	%edx, %ebp
	jb	.LBB9_4
	jmp	.LBB9_5
	.p2align	4, 0x90
.LBB9_3:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movq	64(%r14), %rsi
	movl	$1, %ecx
	movl	$1, %r8d
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	*56(%rax)
	movq	%rax, 32(%r14)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*104(%r14)
	incl	%ebp
	movl	44(%rbx), %edx
	cmpl	%edx, %ebp
	jb	.LBB9_3
	jmp	.LBB9_5
.LBB9_1:
	xorl	%edx, %edx
.LBB9_5:                                # %._crit_edge
	testq	%r15, %r15
	je	.LBB9_7
# BB#6:
	incl	32(%r15)
.LBB9_7:
	movq	$get_memory_row, 8(%r14)
	movl	$0, 72(%r14)
	decl	%edx
	movq	8(%rbx), %rax
	movq	64(%r14), %rsi
	movl	$1, %ecx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	callq	*56(%rax)
	movq	%rax, 32(%r14)
	incl	72(%r14)
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	preload_image, .Lfunc_end9-preload_image
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_memory_row,@function
get_memory_row:                         # @get_memory_row
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 16
.Lcfi64:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movl	44(%rdi), %edx
	decl	%edx
	subl	72(%rbx), %edx
	movq	8(%rdi), %rax
	movq	64(%rbx), %rsi
	movl	$1, %ecx
	xorl	%r8d, %r8d
	callq	*56(%rax)
	movq	%rax, 32(%rbx)
	incl	72(%rbx)
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end10:
	.size	get_memory_row, .Lfunc_end10-get_memory_row
	.cfi_endproc

	.type	c5to8bits,@object       # @c5to8bits
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
c5to8bits:
	.ascii	"\000\b\020\031!)1:BJRZcks{\204\214\224\234\245\255\265\275\305\316\326\336\346\357\367\377"
	.size	c5to8bits, 32


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
