	.text
	.file	"isqrt.bc"
	.globl	usqrt
	.p2align	4, 0x90
	.type	usqrt,@function
usqrt:                                  # @usqrt
	.cfi_startproc
# BB#0:
	xorl	%r8d, %r8d
	movl	$32, %r9d
	xorl	%eax, %eax
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	shldl	$2, %edi, %eax
	shll	$2, %edi
	leal	1(,%r10,4), %ecx
	xorl	%edx, %edx
	cmpl	%ecx, %eax
	setae	%dl
	leal	(%rdx,%r10,2), %r10d
	cmovbl	%r8d, %ecx
	subl	%ecx, %eax
	decl	%r9d
	jne	.LBB0_1
# BB#2:
	movl	%r10d, (%rsi)
	retq
.Lfunc_end0:
	.size	usqrt, .Lfunc_end0-usqrt
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
