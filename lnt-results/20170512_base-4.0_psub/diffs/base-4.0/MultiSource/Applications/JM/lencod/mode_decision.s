	.text
	.file	"mode_decision.bc"
	.globl	rc_store_diff
	.p2align	4, 0x90
	.type	rc_store_diff,@function
rc_store_diff:                          # @rc_store_diff
	.cfi_startproc
# BB#0:
	movslq	%edi, %rax
	movslq	%esi, %rcx
	shlq	$3, %rcx
	addq	imgY_org(%rip), %rcx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi), %rdi
	movq	(%rdi,%rax,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movq	(%rdx,%rsi,4), %xmm2    # xmm2 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	psubd	%xmm2, %xmm1
	movdqa	%xmm1, diffy(,%rsi,8)
	movq	8(%rdi,%rax,2), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movq	8(%rdx,%rsi,4), %xmm2   # xmm2 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	psubd	%xmm2, %xmm1
	movdqa	%xmm1, diffy+16(,%rsi,8)
	movq	16(%rdi,%rax,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movq	16(%rdx,%rsi,4), %xmm2  # xmm2 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	psubd	%xmm2, %xmm1
	movdqa	%xmm1, diffy+32(,%rsi,8)
	movq	24(%rdi,%rax,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movq	24(%rdx,%rsi,4), %xmm2  # xmm2 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	psubd	%xmm2, %xmm1
	movdqa	%xmm1, diffy+48(,%rsi,8)
	addq	$8, %rsi
	cmpq	$128, %rsi
	jne	.LBB0_1
# BB#2:
	retq
.Lfunc_end0:
	.size	rc_store_diff, .Lfunc_end0-rc_store_diff
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4568151222029473109     # double 0.0026041666666666665
.LCPI1_1:
	.quad	4580160821035794432     # double 0.015625
	.text
	.globl	fast_mode_intra_decision
	.p2align	4, 0x90
	.type	fast_mode_intra_decision,@function
fast_mode_intra_decision:               # @fast_mode_intra_decision
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rdi, %r14
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	leaq	32(%rsp), %r8
	movl	$-1, %esi
	movl	$-1, %edx
	xorl	%ecx, %ecx
	callq	*getNeighbour(%rip)
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	leaq	56(%rsp), %r8
	movl	$-1, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*getNeighbour(%rip)
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	leaq	8(%rsp), %r8
	xorl	%esi, %esi
	movl	$-1, %edx
	xorl	%ecx, %ecx
	callq	*getNeighbour(%rip)
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	.LCPI1_0(%rip), %xmm1
	movq	img(%rip), %r10
	movl	15344(%r10), %eax
	decl	%eax
	xorpd	%xmm0, %xmm0
	cmpl	%eax, 164(%r10)
	je	.LBB1_9
# BB#1:
	cmpl	$0, 8(%rsp)
	je	.LBB1_9
# BB#2:
	movl	56(%rsp), %eax
	testl	%eax, %eax
	je	.LBB1_9
# BB#3:
	movl	15336(%r10), %eax
	decl	%eax
	cmpl	%eax, 160(%r10)
	je	.LBB1_9
# BB#4:                                 # %.preheader51
	movq	imgY_org(%rip), %rax
	movslq	196(%r10), %rdx
	leaq	(%rax,%rdx,8), %r8
	movq	enc_picture(%rip), %r9
	movq	6440(%r9), %rsi
	movslq	180(%r10), %rdi
	movslq	176(%r10), %r11
	movslq	192(%r10), %r15
	leaq	(%r15,%r15), %rcx
	addq	(%rax,%rdx,8), %rcx
	leaq	(%r11,%r11), %rax
	addq	-8(%rsi,%rdi,8), %rax
	leaq	(%rsi,%rdi,8), %rdx
	xorl	%edi, %edi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_5:                                # =>This Inner Loop Header: Depth=1
	movzwl	(%rcx,%rdi,2), %ebp
	movzwl	(%rax,%rdi,2), %ebx
	subl	%ebx, %ebp
	movl	%ebp, %ebx
	negl	%ebx
	cmovll	%ebp, %ebx
	movslq	%ebx, %rbx
	addq	%rsi, %rbx
	movq	(%r8,%rdi,8), %rsi
	movzwl	(%rsi,%r15,2), %esi
	movq	(%rdx,%rdi,8), %rbp
	movzwl	-2(%rbp,%r11,2), %ebp
	subl	%ebp, %esi
	movl	%esi, %ebp
	negl	%ebp
	cmovll	%esi, %ebp
	movslq	%ebp, %rsi
	addq	%rbx, %rsi
	incq	%rdi
	cmpq	$16, %rdi
	jne	.LBB1_5
# BB#6:                                 # %.preheader
	movq	imgUV_org(%rip), %rax
	movslq	204(%r10), %rcx
	movq	(%rax), %r12
	movq	8(%rax), %r15
	leaq	(%r12,%rcx,8), %r8
	movq	6472(%r9), %rdi
	movslq	188(%r10), %rbx
	movslq	184(%r10), %rdx
	movslq	200(%r10), %rax
	leaq	(%r15,%rcx,8), %r9
	movq	(%rdi), %r13
	movq	8(%rdi), %rbp
	leaq	(%rax,%rax), %r11
	movq	(%r12,%rcx,8), %r10
	addq	%r11, %r10
	addq	(%r15,%rcx,8), %r11
	leaq	(%rdx,%rdx), %rdi
	movq	-8(%r13,%rbx,8), %r12
	addq	%rdi, %r12
	addq	-8(%rbp,%rbx,8), %rdi
	leaq	(%r13,%rbx,8), %r15
	leaq	(%rbp,%rbx,8), %r13
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_7:                                # =>This Inner Loop Header: Depth=1
	movzwl	(%r10,%rcx), %ebp
	movzwl	(%r12,%rcx), %ebx
	subl	%ebx, %ebp
	movl	%ebp, %ebx
	negl	%ebx
	cmovll	%ebp, %ebx
	movslq	%ebx, %rbx
	addq	%rsi, %rbx
	movq	(%r8,%rcx,4), %rsi
	movzwl	(%rsi,%rax,2), %esi
	movq	(%r15,%rcx,4), %rbp
	movzwl	-2(%rbp,%rdx,2), %ebp
	subl	%ebp, %esi
	movl	%esi, %ebp
	negl	%ebp
	cmovll	%esi, %ebp
	movslq	%ebp, %rsi
	addq	%rbx, %rsi
	movzwl	(%r11,%rcx), %ebx
	movzwl	(%rdi,%rcx), %ebp
	subl	%ebp, %ebx
	movl	%ebx, %ebp
	negl	%ebp
	cmovll	%ebx, %ebp
	movslq	%ebp, %rbx
	addq	%rsi, %rbx
	movq	(%r9,%rcx,4), %rsi
	movzwl	(%rsi,%rax,2), %esi
	movq	(%r13,%rcx,4), %rbp
	movzwl	-2(%rbp,%rdx,2), %ebp
	subl	%ebp, %esi
	movl	%esi, %ebp
	negl	%ebp
	cmovll	%esi, %ebp
	movslq	%ebp, %rsi
	addq	%rbx, %rsi
	addq	$2, %rcx
	cmpq	$16, %rcx
	jne	.LBB1_7
# BB#8:
	cvtsi2sdq	%rsi, %xmm0
	mulsd	.LCPI1_1(%rip), %xmm0
.LBB1_9:
	ucomisd	%xmm1, %xmm0
	jb	.LBB1_11
# BB#10:
	movw	$1, (%r14)
.LBB1_11:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	fast_mode_intra_decision, .Lfunc_end1-fast_mode_intra_decision
	.cfi_endproc

	.globl	init_enc_mb_params
	.p2align	4, 0x90
	.type	init_enc_mb_params,@function
init_enc_mb_params:                     # @init_enc_mb_params
	.cfi_startproc
# BB#0:
	movl	%ecx, %r9d
	movl	432(%rdi), %eax
	movw	%ax, 74(%rsi)
	incl	%eax
	movw	%ax, 76(%rsi)
	movq	img(%rip), %rax
	cmpl	$0, 15268(%rax)
	je	.LBB2_1
# BB#2:
	cmpl	$0, 424(%rdi)
	setne	%cl
	jmp	.LBB2_3
.LBB2_1:
	xorl	%ecx, %ecx
.LBB2_3:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
.Lcfi19:
	.cfi_offset %rbx, -56
.Lcfi20:
	.cfi_offset %r12, -48
.Lcfi21:
	.cfi_offset %r13, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movzbl	%cl, %ecx
	movw	%cx, 78(%rsi)
	movw	$0, 80(%rsi)
	movw	$-1, 82(%rsi)
	movq	input(%rip), %rdi
	movl	5100(%rdi), %ebp
	movw	%bp, 70(%rsi)
	xorl	%ecx, %ecx
	cmpl	$2, %ebp
	setne	%cl
	movw	%cx, 62(%rsi)
	movw	$1, 64(%rsi)
	movzwl	4076(%rdi), %ecx
	movw	%cx, 72(%rsi)
	xorl	%ecx, %ecx
	testl	%edx, %edx
	sete	%cl
	movw	%cx, 44(%rsi)
	je	.LBB2_4
# BB#6:                                 # %.thread199
	leaq	46(%rsi), %r11
	leaq	48(%rsi), %rcx
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	leaq	50(%rsi), %rcx
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	leaq	52(%rsi), %rcx
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	leaq	54(%rsi), %r14
	leaq	56(%rsi), %r15
	movl	$0, 54(%rsi)
	movq	$0, 46(%rsi)
	leaq	58(%rsi), %r12
	movw	$0, 58(%rsi)
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	jmp	.LBB2_7
.LBB2_4:
	cmpl	$2, %ebp
	setne	%r8b
	xorl	%ecx, %ecx
	cmpl	$0, 4020(%rdi)
	setne	%cl
	leaq	46(%rsi), %r11
	movw	%cx, 46(%rsi)
	xorl	%ecx, %ecx
	cmpl	$0, 4024(%rdi)
	setne	%cl
	leaq	48(%rsi), %rdx
	movq	%rdx, -8(%rsp)          # 8-byte Spill
	movw	%cx, 48(%rsi)
	xorl	%ecx, %ecx
	cmpl	$0, 4028(%rdi)
	setne	%cl
	leaq	50(%rsi), %rdx
	movq	%rdx, -16(%rsp)         # 8-byte Spill
	movw	%cx, 50(%rsi)
	movl	4032(%rdi), %ecx
	xorl	%edx, %edx
	cmpl	$0, %ecx
	setne	%dl
	leaq	52(%rsi), %rbp
	movq	%rbp, -24(%rsp)         # 8-byte Spill
	movw	%dx, 52(%rsi)
	cmpl	$0, 4036(%rdi)
	setne	%dl
	andb	%r8b, %dl
	movzbl	%dl, %ebp
	leaq	54(%rsi), %r14
	movw	%bp, 54(%rsi)
	cmpl	$0, 4040(%rdi)
	setne	%bl
	andb	%r8b, %bl
	movzbl	%bl, %r13d
	movw	%r13w, 56(%rsi)
	cmpl	$0, 4044(%rdi)
	leaq	56(%rsi), %r15
	setne	%bpl
	andb	%r8b, %bpl
	leaq	58(%rsi), %r12
	cmpl	$0, %ecx
	movzbl	%bpl, %ecx
	movw	%cx, 58(%rsi)
	movb	$1, %bl
	jne	.LBB2_8
# BB#5:
	testb	%dl, %dl
	jne	.LBB2_8
.LBB2_7:
	testw	%r13w, %r13w
	setne	%bl
	orb	%bpl, %bl
.LBB2_8:
	movzbl	%bl, %ecx
	movw	%cx, 60(%rsi)
	movslq	20(%rax), %r10
	xorl	%ecx, %ecx
	cmpq	$4, %r10
	sete	%cl
	cmpq	$3, %r10
	movw	%cx, 68(%rsi)
	jne	.LBB2_18
# BB#9:
	movl	si_frame_indicator(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB2_18
# BB#10:
	movw	$0, 70(%rsi)
	movw	$0, 72(%rsi)
	movw	$0, 44(%rsi)
	movw	$0, (%r11)
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movw	$0, (%rcx)
	movq	-16(%rsp), %rcx         # 8-byte Reload
	movw	$0, (%rcx)
	movq	-24(%rsp), %rcx         # 8-byte Reload
	movw	$0, (%rcx)
	movw	$0, (%r14)
	movw	$0, (%r15)
	movw	$0, (%r12)
	movw	$0, 60(%rsi)
	movw	$0, 68(%rsi)
	movslq	180(%rax), %rdi
	movslq	176(%rax), %rcx
	movq	lrec(%rip), %r13
	leaq	15(%rcx), %rbp
	decq	%rcx
	leaq	15(%rdi), %r8
.LBB2_11:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_13 Depth 2
	movq	(%r13,%rdi,8), %rdx
	movq	%rcx, %rbx
	.p2align	4, 0x90
.LBB2_13:                               #   Parent Loop BB2_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-16, 4(%rdx,%rbx,4)
	jne	.LBB2_14
# BB#12:                                #   in Loop: Header=BB2_13 Depth=2
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB2_13
# BB#15:                                # %._crit_edge.i
                                        #   in Loop: Header=BB2_11 Depth=1
	cmpq	%r8, %rdi
	leaq	1(%rdi), %rdi
	jl	.LBB2_11
# BB#16:
	xorl	%ecx, %ecx
	jmp	.LBB2_17
.LBB2_14:
	movb	$1, %cl
.LBB2_17:                               # %check_for_SI16.exit
	movzbl	%cl, %ecx
	movw	%cx, 62(%rsi)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	xorb	$1, %cl
	movzbl	%cl, %ecx
	movw	%cx, 64(%rsi)
.LBB2_18:
	cmpl	$3, %r10d
	jne	.LBB2_28
# BB#19:
	movl	sp2_frame_indicator(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB2_28
# BB#20:
	movq	%r11, %rcx
	movl	%r9d, %r11d
	movslq	180(%rax), %rbx
	movslq	176(%rax), %rbp
	movq	lrec(%rip), %r13
	leaq	15(%rbp), %rdi
	decq	%rbp
	leaq	15(%rbx), %r8
.LBB2_21:                               # %.lr.ph.i148
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_23 Depth 2
	movq	(%r13,%rbx,8), %r9
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB2_23:                               #   Parent Loop BB2_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-16, 4(%r9,%rdx,4)
	jne	.LBB2_24
# BB#22:                                #   in Loop: Header=BB2_23 Depth=2
	incq	%rdx
	cmpq	%rdi, %rdx
	jl	.LBB2_23
# BB#25:                                # %._crit_edge.i152
                                        #   in Loop: Header=BB2_21 Depth=1
	cmpq	%r8, %rbx
	leaq	1(%rbx), %rbx
	jl	.LBB2_21
# BB#26:
	movw	$0, 70(%rsi)
	movw	$0, 72(%rsi)
	movw	$0, 44(%rsi)
	movw	$0, (%rcx)
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movw	$0, (%rcx)
	movq	-16(%rsp), %rcx         # 8-byte Reload
	movw	$0, (%rcx)
	movq	-24(%rsp), %rcx         # 8-byte Reload
	movw	$0, (%rcx)
	movw	$0, (%r14)
	movw	$0, (%r15)
	movw	$0, (%r12)
	movw	$0, 68(%rsi)
	movl	$0, 60(%rsi)
	movw	$1, %cx
	jmp	.LBB2_27
.LBB2_24:                               # %check_for_SI16.exit154.thread
	movw	$0, 70(%rsi)
	movw	$0, 72(%rsi)
	movw	$0, 44(%rsi)
	xorl	%ecx, %ecx
.LBB2_27:
	movl	%r11d, %r9d
	movw	%cx, 64(%rsi)
.LBB2_28:
	testl	%r9d, %r9d
	je	.LBB2_31
# BB#29:
	cmpl	$0, 15360(%rax)
	je	.LBB2_31
# BB#30:
	movq	15480(%rax), %rcx
	movq	40(%rcx), %rcx
	movslq	36(%rax), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movq	%rcx, (%rsi)
	movq	15488(%rax), %rcx
	movq	40(%rcx), %rcx
	movq	(%rcx,%rdx,8), %rcx
	movq	(%rcx), %rdi
	movq	%rdi, 8(%rsi)
	movq	8(%rcx), %rcx
	movq	%rcx, 16(%rsi)
	movq	15496(%rax), %rcx
	movq	40(%rcx), %rcx
	movq	(%rcx,%rdx,8), %rdx
	cvtsi2sdl	8(%rdx), %xmm0
	movsd	%xmm0, 24(%rsi)
	movl	(%rdx), %edx
	movl	%edx, 32(%rsi)
	movslq	36(%rax), %rdx
	movq	(%rcx,%rdx,8), %rdx
	movl	4(%rdx), %edx
	movl	%edx, 36(%rsi)
	jmp	.LBB2_32
.LBB2_31:
	movq	15480(%rax), %rcx
	movq	(%rcx,%r10,8), %rcx
	movslq	36(%rax), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movq	%rcx, (%rsi)
	movq	15488(%rax), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%rdx,8), %rcx
	movq	(%rcx), %rdi
	movq	%rdi, 8(%rsi)
	movq	8(%rcx), %rdi
	movq	%rdi, 16(%rsi)
	movq	16(%rcx), %rcx
	movq	%rcx, 24(%rsi)
	movq	15496(%rax), %rcx
	movq	(%rcx,%r10,8), %rdi
	movq	(%rdi,%rdx,8), %rdx
	movl	(%rdx), %edx
	movl	%edx, 32(%rsi)
	movslq	20(%rax), %rdx
	movq	(%rcx,%rdx,8), %rdx
	movslq	36(%rax), %rdi
	movq	(%rdx,%rdi,8), %rdx
	movl	4(%rdx), %edx
	movl	%edx, 36(%rsi)
	movslq	20(%rax), %rdx
	movq	(%rcx,%rdx,8), %rcx
.LBB2_32:
	movslq	36(%rax), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movl	8(%rcx), %ecx
	movl	%ecx, 40(%rsi)
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 14422(%rax)
	movupd	%xmm0, 14408(%rax)
	cmpl	$0, 15268(%rax)
	je	.LBB2_33
# BB#48:
	cmpw	$0, 78(%rsi)
	movswq	74(%rsi), %rax
	movswq	76(%rsi), %rcx
	je	.LBB2_61
# BB#49:                                # %.preheader166
	cmpw	%cx, %ax
	jg	.LBB2_66
# BB#50:                                # %.preheader165.lr.ph
	movq	img(%rip), %r8
	.p2align	4, 0x90
.LBB2_51:                               # %.preheader165
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_53 Depth 2
	cmpl	$0, listXsize(,%rax,4)
	jle	.LBB2_60
# BB#52:                                # %.lr.ph176
                                        #   in Loop: Header=BB2_51 Depth=1
	movq	listX(,%rax,8), %rsi
	movl	12(%r8), %edi
	movl	%edi, %ebp
	andl	$1, %ebp
	movl	%edi, %edx
	shrl	$31, %edx
	addl	%edi, %edx
	andl	$-2, %edx
	subl	%edx, %edi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_53:                               #   Parent Loop BB2_51 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi,%rbx,8), %rdx
	movl	$0, 6424(%rdx)
	testl	%ebp, %ebp
	jne	.LBB2_56
# BB#54:                                #   in Loop: Header=BB2_53 Depth=2
	cmpl	$2, (%rdx)
	jne	.LBB2_56
# BB#55:                                #   in Loop: Header=BB2_53 Depth=2
	movl	$-2, 6424(%rdx)
.LBB2_56:                               #   in Loop: Header=BB2_53 Depth=2
	cmpl	$1, %edi
	jne	.LBB2_59
# BB#57:                                #   in Loop: Header=BB2_53 Depth=2
	cmpl	$1, (%rdx)
	jne	.LBB2_59
# BB#58:                                #   in Loop: Header=BB2_53 Depth=2
	movl	$2, 6424(%rdx)
.LBB2_59:                               #   in Loop: Header=BB2_53 Depth=2
	incq	%rbx
	movslq	listXsize(,%rax,4), %rdx
	cmpq	%rdx, %rbx
	jl	.LBB2_53
.LBB2_60:                               # %._crit_edge177
                                        #   in Loop: Header=BB2_51 Depth=1
	cmpq	%rcx, %rax
	leaq	1(%rax), %rax
	jl	.LBB2_51
	jmp	.LBB2_66
.LBB2_33:                               # %.preheader161
	movq	img(%rip), %rax
	movslq	listXsize(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB2_42
# BB#34:                                # %.lr.ph
	movq	listX(%rip), %rdx
	movl	24(%rax), %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_35:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rdi,8), %rbp
	movl	$0, 6424(%rbp)
	cmpl	$2, %esi
	je	.LBB2_39
# BB#36:                                #   in Loop: Header=BB2_35 Depth=1
	cmpl	$1, %esi
	jne	.LBB2_41
# BB#37:                                #   in Loop: Header=BB2_35 Depth=1
	cmpl	$1, (%rbp)
	je	.LBB2_41
# BB#38:                                #   in Loop: Header=BB2_35 Depth=1
	movl	$-2, 6424(%rbp)
	jmp	.LBB2_41
	.p2align	4, 0x90
.LBB2_39:                               #   in Loop: Header=BB2_35 Depth=1
	cmpl	$2, (%rbp)
	je	.LBB2_41
# BB#40:                                #   in Loop: Header=BB2_35 Depth=1
	movl	$2, 6424(%rbp)
	.p2align	4, 0x90
.LBB2_41:                               # %.thread200
                                        #   in Loop: Header=BB2_35 Depth=1
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB2_35
.LBB2_42:                               # %._crit_edge
	movslq	listXsize+4(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB2_66
# BB#43:                                # %.lr.ph.1
	movq	listX+8(%rip), %rdx
	movl	24(%rax), %eax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_44:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rdi
	movl	$0, 6424(%rdi)
	cmpl	$2, %eax
	je	.LBB2_67
# BB#45:                                #   in Loop: Header=BB2_44 Depth=1
	cmpl	$1, %eax
	jne	.LBB2_69
# BB#46:                                #   in Loop: Header=BB2_44 Depth=1
	cmpl	$1, (%rdi)
	je	.LBB2_69
# BB#47:                                #   in Loop: Header=BB2_44 Depth=1
	movl	$-2, 6424(%rdi)
	jmp	.LBB2_69
	.p2align	4, 0x90
.LBB2_67:                               #   in Loop: Header=BB2_44 Depth=1
	cmpl	$2, (%rdi)
	je	.LBB2_69
# BB#68:                                #   in Loop: Header=BB2_44 Depth=1
	movl	$2, 6424(%rdi)
	.p2align	4, 0x90
.LBB2_69:                               # %.thread202
                                        #   in Loop: Header=BB2_44 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB2_44
	jmp	.LBB2_66
.LBB2_61:                               # %.preheader163
	cmpw	%cx, %ax
	jg	.LBB2_66
	.p2align	4, 0x90
.LBB2_62:                               # %.preheader162
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_64 Depth 2
	cmpl	$0, listXsize(,%rax,4)
	jle	.LBB2_65
# BB#63:                                # %.lr.ph172
                                        #   in Loop: Header=BB2_62 Depth=1
	movq	listX(,%rax,8), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_64:                               #   Parent Loop BB2_62 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rsi,8), %rdi
	movl	$0, 6424(%rdi)
	incq	%rsi
	movslq	listXsize(,%rax,4), %rdi
	cmpq	%rdi, %rsi
	jl	.LBB2_64
.LBB2_65:                               # %._crit_edge173
                                        #   in Loop: Header=BB2_62 Depth=1
	cmpq	%rcx, %rax
	leaq	1(%rax), %rax
	jl	.LBB2_62
.LBB2_66:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	init_enc_mb_params, .Lfunc_end2-init_enc_mb_params
	.cfi_endproc

	.globl	check_for_SI16
	.p2align	4, 0x90
	.type	check_for_SI16,@function
check_for_SI16:                         # @check_for_SI16
	.cfi_startproc
# BB#0:                                 # %.lr.ph16
	movq	img(%rip), %rcx
	movslq	180(%rcx), %rax
	movslq	176(%rcx), %rdx
	movq	lrec(%rip), %r9
	leaq	15(%rdx), %rsi
	decq	%rdx
	leaq	15(%rax), %r8
.LBB3_1:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_3 Depth 2
	movq	(%r9,%rax,8), %rdi
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB3_3:                                #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-16, 4(%rdi,%rcx,4)
	jne	.LBB3_4
# BB#2:                                 #   in Loop: Header=BB3_3 Depth=2
	incq	%rcx
	cmpq	%rsi, %rcx
	jl	.LBB3_3
# BB#5:                                 # %._crit_edge
                                        #   in Loop: Header=BB3_1 Depth=1
	cmpq	%r8, %rax
	leaq	1(%rax), %rax
	jl	.LBB3_1
# BB#6:
	movl	$1, %eax
	retq
.LBB3_4:
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	check_for_SI16, .Lfunc_end3-check_for_SI16
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4616189618054758400     # double 4
	.text
	.globl	list_prediction_cost
	.p2align	4, 0x90
	.type	list_prediction_cost,@function
list_prediction_cost:                   # @list_prediction_cost
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 96
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%r8, %r9
	movq	%rcx, %r13
	movl	%edx, %r11d
	movl	%esi, %r10d
	movl	%edi, %eax
	leaq	96(%rsp), %r8
	xorl	%ecx, %ecx
	cmpl	$2, %eax
	movslq	%eax, %rbx
	cmovlq	%rbx, %rcx
	movswl	170(%rsp,%rcx,2), %ecx
	movslq	%ecx, %r14
	cmpl	$1, %ebx
	jg	.LBB4_45
# BB#1:                                 # %.preheader
	movl	listXsize(,%r14,4), %ecx
	testl	%ecx, %ecx
	jle	.LBB4_76
# BB#2:                                 # %.lr.ph
	testl	%eax, %eax
	movslq	%r11d, %r15
	movslq	%r10d, %r12
	je	.LBB4_22
# BB#3:                                 # %.lr.ph.split.us.preheader
	xorl	%edx, %edx
	movl	$1, %eax
	.p2align	4, 0x90
.LBB4_4:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	input(%rip), %rsi
	movl	2152(%rsi), %edi
	testl	%edi, %edi
	je	.LBB4_5
.LBB4_7:                                #   in Loop: Header=BB4_4 Depth=1
	movq	img(%rip), %rbp
	movl	20(%rbp), %ebp
	cmpl	$3, %ebp
	je	.LBB4_9
# BB#8:                                 #   in Loop: Header=BB4_4 Depth=1
	testl	%ebp, %ebp
	jne	.LBB4_14
.LBB4_9:                                #   in Loop: Header=BB4_4 Depth=1
	testl	%edi, %edi
	jne	.LBB4_11
	jmp	.LBB4_10
	.p2align	4, 0x90
.LBB4_5:                                #   in Loop: Header=BB4_4 Depth=1
	cmpl	$0, 2156(%rsi)
	je	.LBB4_14
# BB#6:                                 #   in Loop: Header=BB4_4 Depth=1
	cmpl	$0, 2156(%rsi)
	jne	.LBB4_7
.LBB4_10:                               # %.thread
                                        #   in Loop: Header=BB4_4 Depth=1
	cmpl	$0, 2156(%rsi)
	je	.LBB4_21
.LBB4_11:                               #   in Loop: Header=BB4_4 Depth=1
	movq	img(%rip), %rdi
	movl	20(%rdi), %edi
	cmpl	$3, %edi
	je	.LBB4_13
# BB#12:                                #   in Loop: Header=BB4_4 Depth=1
	testl	%edi, %edi
	jne	.LBB4_21
.LBB4_13:                               #   in Loop: Header=BB4_4 Depth=1
	testw	%dx, %dx
	jne	.LBB4_21
.LBB4_14:                               #   in Loop: Header=BB4_4 Depth=1
	cmpl	$0, 4168(%rsi)
	je	.LBB4_18
# BB#15:                                #   in Loop: Header=BB4_4 Depth=1
	movl	40(%r8), %edi
	xorl	%esi, %esi
	cmpl	$2, %ecx
	jl	.LBB4_17
# BB#16:                                #   in Loop: Header=BB4_4 Depth=1
	movq	refbits(%rip), %rsi
	movswq	%dx, %rbp
	movl	(%rsi,%rbp,4), %esi
.LBB4_17:                               #   in Loop: Header=BB4_4 Depth=1
	imull	%edi, %esi
	sarl	$16, %esi
	jmp	.LBB4_19
.LBB4_18:                               #   in Loop: Header=BB4_4 Depth=1
	movsd	24(%r8), %xmm0          # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm0
	cmpl	$2, %edx
	movl	%edx, %esi
	cmovgel	%eax, %esi
	cvtsi2sdl	%esi, %xmm1
	mulsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %esi
.LBB4_19:                               #   in Loop: Header=BB4_4 Depth=1
	movq	motion_cost(%rip), %rdi
	movq	(%rdi,%r15,8), %rdi
	movq	(%rdi,%rbx,8), %rdi
	movswq	%dx, %rbp
	movq	(%rdi,%rbp,8), %rdi
	addl	(%rdi,%r12,4), %esi
	cmpl	(%r13,%rbx,4), %esi
	jge	.LBB4_21
# BB#20:                                #   in Loop: Header=BB4_4 Depth=1
	movl	%esi, (%r13,%rbx,4)
	movb	%dl, (%r9,%rbx)
	movl	listXsize(,%r14,4), %ecx
.LBB4_21:                               #   in Loop: Header=BB4_4 Depth=1
	leal	1(%rdx), %edx
	movswl	%dx, %edx
	cmpl	%ecx, %edx
	jl	.LBB4_4
	jmp	.LBB4_76
.LBB4_45:
	cmpl	$2, %eax
	jne	.LBB4_67
# BB#46:
	movq	active_pps(%rip), %rax
	cmpl	$1, 196(%rax)
	jne	.LBB4_58
# BB#47:
	movq	wbp_weight(%rip), %rax
	movsbq	(%r9), %rcx
	movq	(%rax), %rdx
	movq	8(%rax), %rsi
	movq	(%rdx,%rcx,8), %rdx
	movsbq	1(%r9), %rax
	movq	(%rdx,%rax,8), %rdx
	movl	(%rdx), %edx
	movq	(%rsi,%rcx,8), %rsi
	movq	(%rsi,%rax,8), %rsi
	movl	(%rsi), %esi
	leal	128(%rdx,%rsi), %edx
	cmpl	$256, %edx              # imm = 0x100
	jb	.LBB4_49
# BB#48:
	movl	$2147483647, (%r13,%rbx,4) # imm = 0x7FFFFFFF
	jmp	.LBB4_76
.LBB4_67:
	movq	input(%rip), %rcx
	cmpl	$0, 4168(%rcx)
	je	.LBB4_73
# BB#68:
	movl	40(%r8), %r8d
	xorl	%ecx, %ecx
	cmpl	$2, listXsize(,%r14,4)
	movl	$0, %edx
	jl	.LBB4_70
# BB#69:
	movq	refbits(%rip), %rdx
	movl	(%rdx), %edx
.LBB4_70:
	imull	%r8d, %edx
	sarl	$16, %edx
	cmpl	$2, listXsize+4(,%r14,4)
	jl	.LBB4_72
# BB#71:
	movq	refbits(%rip), %rcx
	movl	(%rcx), %ecx
.LBB4_72:
	imull	%r8d, %ecx
	sarl	$16, %ecx
	addl	%edx, %ecx
	jmp	.LBB4_74
.LBB4_58:
	movq	input(%rip), %rax
	cmpl	$0, 4168(%rax)
	je	.LBB4_65
# BB#59:
	movl	40(%r8), %r8d
	xorl	%eax, %eax
	cmpl	$2, listXsize(,%r14,4)
	jl	.LBB4_61
# BB#60:
	movq	refbits(%rip), %rax
	movsbq	(%r9), %rcx
	movl	(%rax,%rcx,4), %eax
.LBB4_61:
	imull	%r8d, %eax
	sarl	$16, %eax
	cmpl	$1, listXsize+4(,%r14,4)
	jg	.LBB4_63
# BB#62:                                # %._crit_edge
	xorl	%ecx, %ecx
	jmp	.LBB4_64
.LBB4_22:                               # %.lr.ph.split.preheader
	xorl	%ebp, %ebp
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	%r12, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB4_23:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	testw	%bp, %bp
	je	.LBB4_27
# BB#24:                                # %.lr.ph.split
                                        #   in Loop: Header=BB4_23 Depth=1
	movq	img(%rip), %rax
	movzwl	15592(%rax), %eax
	testw	%ax, %ax
	je	.LBB4_27
# BB#25:                                #   in Loop: Header=BB4_23 Depth=1
	movq	input(%rip), %rax
	cmpl	$0, 4732(%rax)
	je	.LBB4_44
# BB#26:                                #   in Loop: Header=BB4_23 Depth=1
	xorl	%esi, %esi
	movl	%r10d, %edi
	movl	%ebp, %edx
	movl	%r11d, %ecx
	movq	%r13, %r15
	movq	%r9, %r13
	movl	%r10d, %r14d
	movl	%r11d, %r12d
	movq	%r8, %rbx
	callq	CheckReliabilityOfRef
	movq	%rbx, %r8
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	%r12d, %r11d
	movq	8(%rsp), %r12           # 8-byte Reload
	movl	%r14d, %r10d
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r13, %r9
	movq	%r15, %r13
	movq	16(%rsp), %r15          # 8-byte Reload
	testl	%eax, %eax
	je	.LBB4_44
.LBB4_27:                               #   in Loop: Header=BB4_23 Depth=1
	movq	input(%rip), %rax
	movl	2152(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB4_28
.LBB4_30:                               #   in Loop: Header=BB4_23 Depth=1
	movq	img(%rip), %rdx
	movl	20(%rdx), %edx
	cmpl	$3, %edx
	je	.LBB4_32
# BB#31:                                #   in Loop: Header=BB4_23 Depth=1
	testl	%edx, %edx
	jne	.LBB4_37
.LBB4_32:                               #   in Loop: Header=BB4_23 Depth=1
	testl	%ecx, %ecx
	jne	.LBB4_34
	jmp	.LBB4_33
	.p2align	4, 0x90
.LBB4_28:                               #   in Loop: Header=BB4_23 Depth=1
	cmpl	$0, 2156(%rax)
	je	.LBB4_37
# BB#29:                                #   in Loop: Header=BB4_23 Depth=1
	cmpl	$0, 2156(%rax)
	jne	.LBB4_30
.LBB4_33:                               # %.thread101
                                        #   in Loop: Header=BB4_23 Depth=1
	cmpl	$0, 2156(%rax)
	je	.LBB4_44
.LBB4_34:                               #   in Loop: Header=BB4_23 Depth=1
	movq	img(%rip), %rcx
	movl	20(%rcx), %ecx
	cmpl	$3, %ecx
	je	.LBB4_36
# BB#35:                                #   in Loop: Header=BB4_23 Depth=1
	testl	%ecx, %ecx
	jne	.LBB4_44
.LBB4_36:                               #   in Loop: Header=BB4_23 Depth=1
	testw	%bp, %bp
	jne	.LBB4_44
.LBB4_37:                               #   in Loop: Header=BB4_23 Depth=1
	cmpl	$0, 4168(%rax)
	je	.LBB4_41
# BB#38:                                #   in Loop: Header=BB4_23 Depth=1
	movl	40(%r8), %ecx
	xorl	%eax, %eax
	cmpl	$2, listXsize(,%r14,4)
	jl	.LBB4_40
# BB#39:                                #   in Loop: Header=BB4_23 Depth=1
	movq	refbits(%rip), %rax
	movswq	%bp, %rdx
	movl	(%rax,%rdx,4), %eax
.LBB4_40:                               #   in Loop: Header=BB4_23 Depth=1
	imull	%ecx, %eax
	sarl	$16, %eax
	jmp	.LBB4_42
.LBB4_41:                               #   in Loop: Header=BB4_23 Depth=1
	movsd	24(%r8), %xmm0          # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm0
	cmpl	$2, %ebp
	movl	%ebp, %eax
	movl	$1, %ecx
	cmovgel	%ecx, %eax
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %eax
.LBB4_42:                               #   in Loop: Header=BB4_23 Depth=1
	movq	motion_cost(%rip), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movswq	%bp, %rdx
	movq	(%rcx,%rdx,8), %rcx
	addl	(%rcx,%r12,4), %eax
	cmpl	(%r13,%rbx,4), %eax
	jge	.LBB4_44
# BB#43:                                #   in Loop: Header=BB4_23 Depth=1
	movl	%eax, (%r13,%rbx,4)
	movb	%bpl, (%r9,%rbx)
	.p2align	4, 0x90
.LBB4_44:                               #   in Loop: Header=BB4_23 Depth=1
	leal	1(%rbp), %eax
	movswl	%ax, %ebp
	cmpl	listXsize(,%r14,4), %ebp
	jl	.LBB4_23
	jmp	.LBB4_76
.LBB4_73:
	movsd	24(%r8), %xmm0          # xmm0 = mem[0],zero
	mulsd	.LCPI4_0(%rip), %xmm0
	cvttsd2si	%xmm0, %ecx
	movl	40(%r8), %r8d
.LBB4_74:
	movl	%ecx, (%r13,%rbx,4)
	notl	%eax
	andl	$1, %eax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	%r11d, %edi
	movl	%r10d, %esi
	movl	%eax, %r9d
	callq	BPredPartitionCost
	jmp	.LBB4_75
.LBB4_49:
	movq	input(%rip), %rdx
	cmpl	$0, 4168(%rdx)
	je	.LBB4_55
# BB#50:
	movl	40(%r8), %r8d
	xorl	%edx, %edx
	cmpl	$2, listXsize(,%r14,4)
	movl	$0, %esi
	jl	.LBB4_52
# BB#51:
	movq	refbits(%rip), %rsi
	movl	(%rsi,%rcx,4), %esi
.LBB4_52:
	imull	%r8d, %esi
	sarl	$16, %esi
	cmpl	$2, listXsize+4(,%r14,4)
	jl	.LBB4_54
# BB#53:
	movq	refbits(%rip), %rcx
	movl	(%rcx,%rax,4), %edx
.LBB4_54:
	imull	%r8d, %edx
	sarl	$16, %edx
	addl	%esi, %edx
	jmp	.LBB4_56
.LBB4_65:
	movsbl	(%r9), %eax
	cmpl	$2, %eax
	movl	$1, %ecx
	cmovgel	%ecx, %eax
	movsbl	1(%r9), %edx
	cmpl	$2, %edx
	cmovll	%edx, %ecx
	addl	%eax, %ecx
	cvtsi2sdl	%ecx, %xmm0
	mulsd	24(%r8), %xmm0
	addsd	%xmm0, %xmm0
	cvttsd2si	%xmm0, %ecx
	movl	40(%r8), %r8d
	movl	%ecx, (%r13,%rbx,4)
	jmp	.LBB4_57
.LBB4_63:
	movq	refbits(%rip), %rcx
	movsbq	1(%r9), %rdx
	movl	(%rcx,%rdx,4), %ecx
.LBB4_64:
	imull	%r8d, %ecx
	sarl	$16, %ecx
	addl	%eax, %ecx
	movl	%ecx, (%r13,%rbx,4)
	jmp	.LBB4_57
.LBB4_55:
	cmpl	$2, %ecx
	movl	$1, %edx
	cmovgel	%edx, %ecx
	cmpl	$2, %eax
	cmovll	%eax, %edx
	addl	%ecx, %edx
	cvtsi2sdl	%edx, %xmm0
	mulsd	24(%r8), %xmm0
	addsd	%xmm0, %xmm0
	cvttsd2si	%xmm0, %edx
	movl	40(%r8), %r8d
.LBB4_56:
	movl	%edx, (%r13,%rbx,4)
.LBB4_57:                               # %.loopexit
	movsbl	1(%r9), %ecx
	movsbl	(%r9), %edx
	movl	%r11d, %edi
	movl	%r10d, %esi
	callq	BIDPartitionCost
.LBB4_75:                               # %.loopexit
	addl	%eax, (%r13,%rbx,4)
.LBB4_76:                               # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	list_prediction_cost, .Lfunc_end4-list_prediction_cost
	.cfi_endproc

	.globl	compute_ref_cost
	.p2align	4, 0x90
	.type	compute_ref_cost,@function
compute_ref_cost:                       # @compute_ref_cost
	.cfi_startproc
# BB#0:
	movl	48(%rsp), %ecx
	movslq	%esi, %rax
	movswq	82(%rsp,%rax,2), %rdx
	xorl	%eax, %eax
	cmpl	$2, listXsize(,%rdx,4)
	jl	.LBB5_2
# BB#1:
	movq	refbits(%rip), %rax
	movslq	%edi, %rdx
	movl	(%rax,%rdx,4), %eax
.LBB5_2:
	imull	%ecx, %eax
	sarl	$16, %eax
	retq
.Lfunc_end5:
	.size	compute_ref_cost, .Lfunc_end5-compute_ref_cost
	.cfi_endproc

	.globl	determine_prediction_list
	.p2align	4, 0x90
	.type	determine_prediction_list,@function
determine_prediction_list:              # @determine_prediction_list
	.cfi_startproc
# BB#0:
	cmpl	$1, %edi
	jne	.LBB6_19
# BB#1:
	movq	input(%rip), %rax
	movl	2120(%rax), %eax
	testl	%eax, %eax
	je	.LBB6_19
# BB#2:
	movq	img(%rip), %rax
	movw	$0, 14410(%rax)
	movw	$0, (%r9)
	movl	(%rsi), %r11d
	movl	4(%rsi), %eax
	cmpl	%eax, %r11d
	jg	.LBB6_6
# BB#3:
	cmpl	8(%rsi), %r11d
	jg	.LBB6_6
# BB#4:
	cmpl	12(%rsi), %r11d
	jg	.LBB6_6
# BB#5:
	cmpl	16(%rsi), %r11d
	jle	.LBB6_31
.LBB6_6:
	cmpl	%r11d, %eax
	movl	8(%rsi), %r10d
	jg	.LBB6_10
# BB#7:
	cmpl	%r10d, %eax
	jg	.LBB6_10
# BB#8:
	cmpl	12(%rsi), %eax
	jg	.LBB6_10
# BB#9:
	cmpl	16(%rsi), %eax
	jle	.LBB6_29
.LBB6_10:                               # %._crit_edge
	cmpl	%r11d, %r10d
	jg	.LBB6_14
# BB#11:                                # %._crit_edge
	cmpl	%eax, %r10d
	jg	.LBB6_14
# BB#12:
	cmpl	12(%rsi), %r10d
	jg	.LBB6_14
# BB#13:
	cmpl	16(%rsi), %r10d
	jle	.LBB6_30
.LBB6_14:
	movl	12(%rsi), %edi
	cmpl	%r11d, %edi
	jg	.LBB6_18
# BB#15:
	cmpl	%eax, %edi
	jg	.LBB6_18
# BB#16:
	cmpl	%r10d, %edi
	jg	.LBB6_18
# BB#17:
	cmpl	16(%rsi), %edi
	jle	.LBB6_28
.LBB6_18:
	movb	$2, (%rcx)
	movl	16(%rsi), %eax
	addl	%eax, (%r8)
	movw	$2, (%r9)
	movw	$0, (%rdx)
	movq	img(%rip), %rax
	movw	$2, 14410(%rax)
	retq
.LBB6_19:
	movl	(%rsi), %edi
	movl	4(%rsi), %edx
	cmpl	%edx, %edi
	jg	.LBB6_21
# BB#20:
	cmpl	8(%rsi), %edi
	jle	.LBB6_24
.LBB6_21:
	leaq	8(%rsi), %rax
	cmpl	%edi, %edx
	jg	.LBB6_23
# BB#22:
	cmpl	(%rax), %edx
	jle	.LBB6_25
.LBB6_23:                               # %._crit_edge81
	movb	$2, %dl
	jmp	.LBB6_27
.LBB6_24:
	xorl	%edx, %edx
	jmp	.LBB6_26
.LBB6_25:
	addq	$4, %rsi
	movb	$1, %dl
.LBB6_26:
	movq	%rsi, %rax
.LBB6_27:
	movb	%dl, (%rcx)
	movl	(%rax), %eax
	addl	%eax, (%r8)
	retq
.LBB6_28:
	movb	$2, (%rcx)
	movl	12(%rsi), %eax
	addl	%eax, (%r8)
	movw	$1, (%r9)
	movq	img(%rip), %rax
	movw	$1, 14410(%rax)
	movw	$0, (%rdx)
	retq
.LBB6_29:
	movb	$1, (%rcx)
	movl	4(%rsi), %eax
	addl	%eax, (%r8)
	retq
.LBB6_30:
	movb	$2, (%rcx)
	movl	8(%rsi), %eax
	addl	%eax, (%r8)
	retq
.LBB6_31:
	movb	$0, (%rcx)
	movl	(%rsi), %eax
	addl	%eax, (%r8)
	retq
.Lfunc_end6:
	.size	determine_prediction_list, .Lfunc_end6-determine_prediction_list
	.cfi_endproc

	.globl	compute_mode_RD_cost
	.p2align	4, 0x90
	.type	compute_mode_RD_cost,@function
compute_mode_RD_cost:                   # @compute_mode_RD_cost
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 80
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movl	%r8d, (%rsp)            # 4-byte Spill
	movq	%rcx, %r15
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movq	176(%rsp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	input(%rip), %rcx
	xorl	%eax, %eax
	cmpl	$2, 5100(%rcx)
	jne	.LBB7_9
# BB#1:
	leal	-1(%rbp), %ecx
	movb	$1, %al
	cmpl	$3, %ecx
	jb	.LBB7_8
# BB#2:
	testl	%ebp, %ebp
	jne	.LBB7_5
# BB#3:
	testw	%r14w, %r14w
	je	.LBB7_5
# BB#4:
	movq	active_sps(%rip), %rcx
	cmpl	$0, 1156(%rcx)
	jne	.LBB7_8
.LBB7_5:
	cmpl	$8, %ebp
	jne	.LBB7_6
# BB#7:
	leaq	80(%rsp), %rax
	cmpw	$0, 52(%rax)
	setne	%al
	jmp	.LBB7_8
.LBB7_6:
	xorl	%eax, %eax
.LBB7_8:
	movzbl	%al, %eax
.LBB7_9:
	movl	%eax, 472(%rbx)
	movl	%ebp, %edi
	callq	SetModesAndRefframeForBlocks
	movq	img(%rip), %rax
	movl	$0, 15256(%rax)
	movq	input(%rip), %rax
	cmpl	$0, 4176(%rax)
	jne	.LBB7_13
# BB#10:
	cmpl	$0, 416(%rbx)
	je	.LBB7_13
# BB#11:
	movl	72(%rbx), %eax
	cmpl	$14, %eax
	ja	.LBB7_48
# BB#12:
	movl	$26112, %ecx            # imm = 0x6600
	btl	%eax, %ecx
	jae	.LBB7_48
.LBB7_13:                               # %.preheader
	leaq	80(%rsp), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	leal	-1(%rbp), %r12d
	testl	%ebp, %ebp
	setne	%al
	movl	%r14d, 4(%rsp)          # 4-byte Spill
	testw	%r14w, %r14w
	sete	%r14b
	orb	%al, %r14b
	jmp	.LBB7_14
	.p2align	4, 0x90
.LBB7_34:                               # %.backedge
                                        #   in Loop: Header=BB7_14 Depth=1
	movl	$1, 472(%rbx)
.LBB7_14:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_20 Depth 2
                                        #     Child Loop BB7_18 Depth 2
	movq	16(%rsp), %xmm0         # 8-byte Folded Reload
                                        # xmm0 = mem[0],zero
	movl	%ebp, %edi
	movq	%r13, %rsi
	movq	%r15, %rdx
	movl	(%rsp), %ecx            # 4-byte Reload
	callq	RDCost_for_macroblocks
	testl	%eax, %eax
	movq	input(%rip), %rax
	je	.LBB7_26
# BB#15:                                #   in Loop: Header=BB7_14 Depth=1
	cmpl	$0, 5116(%rax)
	pxor	%xmm2, %xmm2
	je	.LBB7_21
# BB#16:                                #   in Loop: Header=BB7_14 Depth=1
	movq	img(%rip), %rcx
	movslq	192(%rcx), %rax
	movslq	196(%rcx), %rcx
	cmpl	$8, %ebp
	jne	.LBB7_19
# BB#17:                                #   in Loop: Header=BB7_14 Depth=1
	cmpl	$1, 472(%rbx)
	movl	$tr4x4+6680, %edx
	movl	$tr8x8+6680, %esi
	cmoveq	%rsi, %rdx
	shlq	$3, %rcx
	addq	imgY_org(%rip), %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB7_18:                               # %vector.body
                                        #   Parent Loop BB7_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rsi), %rdi
	movq	(%rdi,%rax,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rdx,%rsi,4), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diffy(,%rsi,8)
	movq	8(%rdi,%rax,2), %xmm0   # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	8(%rdx,%rsi,4), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diffy+16(,%rsi,8)
	movq	16(%rdi,%rax,2), %xmm0  # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	16(%rdx,%rsi,4), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diffy+32(,%rsi,8)
	movq	24(%rdi,%rax,2), %xmm0  # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	24(%rdx,%rsi,4), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diffy+48(,%rsi,8)
	addq	$8, %rsi
	cmpq	$128, %rsi
	jne	.LBB7_18
	jmp	.LBB7_21
.LBB7_19:                               #   in Loop: Header=BB7_14 Depth=1
	shlq	$3, %rcx
	addq	imgY_org(%rip), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB7_20:                               # %vector.body79
                                        #   Parent Loop BB7_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rdx), %rsi
	movq	(%rsi,%rax,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	pred(,%rdx,4), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diffy(,%rdx,8)
	movq	8(%rsi,%rax,2), %xmm0   # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	pred+8(,%rdx,4), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diffy+16(,%rdx,8)
	movq	16(%rsi,%rax,2), %xmm0  # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	pred+16(,%rdx,4), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diffy+32(,%rdx,8)
	movq	24(%rsi,%rax,2), %xmm0  # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	pred+24(,%rdx,4), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diffy+48(,%rdx,8)
	addq	$8, %rdx
	cmpq	$128, %rdx
	jne	.LBB7_20
.LBB7_21:                               # %rc_store_diff.exit
                                        #   in Loop: Header=BB7_14 Depth=1
	movl	%ebp, %edi
	callq	store_macroblock_parameters
	movq	input(%rip), %rax
	testl	%ebp, %ebp
	jne	.LBB7_26
# BB#22:                                # %rc_store_diff.exit
                                        #   in Loop: Header=BB7_14 Depth=1
	cmpl	$2, 4168(%rax)
	jne	.LBB7_26
# BB#23:                                #   in Loop: Header=BB7_14 Depth=1
	cmpl	$0, 5744(%rax)
	je	.LBB7_26
# BB#24:                                #   in Loop: Header=BB7_14 Depth=1
	cmpl	$0, 364(%rbx)
	jne	.LBB7_26
# BB#25:                                #   in Loop: Header=BB7_14 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movw	$1, (%rcx)
	.p2align	4, 0x90
.LBB7_26:                               # %._crit_edge
                                        #   in Loop: Header=BB7_14 Depth=1
	cmpl	$1, 5100(%rax)
	jne	.LBB7_36
# BB#27:                                #   in Loop: Header=BB7_14 Depth=1
	cmpl	$2, %r12d
	jbe	.LBB7_33
# BB#28:                                #   in Loop: Header=BB7_14 Depth=1
	testb	%r14b, %r14b
	jne	.LBB7_31
# BB#29:                                #   in Loop: Header=BB7_14 Depth=1
	movq	active_sps(%rip), %rcx
	cmpl	$0, 1156(%rcx)
	je	.LBB7_31
# BB#30:                                #   in Loop: Header=BB7_14 Depth=1
	cmpl	$0, 472(%rbx)
	je	.LBB7_34
.LBB7_31:                               #   in Loop: Header=BB7_14 Depth=1
	cmpl	$8, %ebp
	jne	.LBB7_35
# BB#32:                                #   in Loop: Header=BB7_14 Depth=1
	leaq	80(%rsp), %rcx
	cmpw	$0, 52(%rcx)
	je	.LBB7_35
.LBB7_33:                               #   in Loop: Header=BB7_14 Depth=1
	cmpl	$0, 472(%rbx)
	je	.LBB7_34
.LBB7_35:                               # %.thread70
	movl	$0, 472(%rbx)
.LBB7_36:                               # %.loopexit
	testl	%ebp, %ebp
	movl	4(%rsp), %ecx           # 4-byte Reload
	jne	.LBB7_48
# BB#37:                                # %.loopexit
	testw	%cx, %cx
	je	.LBB7_48
# BB#38:
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpw	$0, (%rcx)
	je	.LBB7_39
.LBB7_48:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_39:
	leaq	80(%rsp), %rcx
	cmpw	$0, 44(%rcx)
	je	.LBB7_48
# BB#40:
	movl	364(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB7_48
# BB#41:
	andl	$15, %ecx
	cmpl	$15, %ecx
	je	.LBB7_48
# BB#42:
	cmpl	$0, 4184(%rax)
	jne	.LBB7_48
# BB#43:
	movq	img(%rip), %rax
	movl	$1, 15256(%rax)
	xorl	%edi, %edi
	movq	16(%rsp), %xmm0         # 8-byte Folded Reload
                                        # xmm0 = mem[0],zero
	movq	%r13, %rsi
	movq	%r15, %rdx
	movl	(%rsp), %ecx            # 4-byte Reload
	callq	RDCost_for_macroblocks
	testl	%eax, %eax
	je	.LBB7_48
# BB#44:
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	je	.LBB7_47
# BB#45:
	movq	img(%rip), %rcx
	movslq	192(%rcx), %rax
	movslq	196(%rcx), %rcx
	shlq	$3, %rcx
	addq	imgY_org(%rip), %rcx
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB7_46:                               # %vector.body93
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx), %rsi
	movq	(%rsi,%rax,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movq	pred(,%rdx,4), %xmm2    # xmm2 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	psubd	%xmm2, %xmm1
	movdqa	%xmm1, diffy(,%rdx,8)
	movq	8(%rsi,%rax,2), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movq	pred+8(,%rdx,4), %xmm2  # xmm2 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	psubd	%xmm2, %xmm1
	movdqa	%xmm1, diffy+16(,%rdx,8)
	movq	16(%rsi,%rax,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movq	pred+16(,%rdx,4), %xmm2 # xmm2 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	psubd	%xmm2, %xmm1
	movdqa	%xmm1, diffy+32(,%rdx,8)
	movq	24(%rsi,%rax,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movq	pred+24(,%rdx,4), %xmm2 # xmm2 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	psubd	%xmm2, %xmm1
	movdqa	%xmm1, diffy+48(,%rdx,8)
	addq	$8, %rdx
	cmpq	$128, %rdx
	jne	.LBB7_46
.LBB7_47:                               # %rc_store_diff.exit61
	xorl	%edi, %edi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	store_macroblock_parameters # TAILCALL
.Lfunc_end7:
	.size	compute_mode_RD_cost, .Lfunc_end7-compute_mode_RD_cost
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI8_0:
	.quad	5055640609639927018     # double 1.0E+30
	.text
	.globl	submacroblock_mode_decision
	.p2align	4, 0x90
	.type	submacroblock_mode_decision,@function
submacroblock_mode_decision:            # @submacroblock_mode_decision
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	subq	$520, %rsp              # imm = 0x208
.Lcfi57:
	.cfi_def_cfa_offset 576
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movl	%r8d, 156(%rsp)         # 4-byte Spill
	movq	%rcx, 352(%rsp)         # 8-byte Spill
	movq	%rdx, 360(%rsp)         # 8-byte Spill
	movq	%rdi, 240(%rsp)         # 8-byte Spill
	movl	.Lsubmacroblock_mode_decision.bmcost+16(%rip), %eax
	movl	%eax, 304(%rsp)
	movapd	.Lsubmacroblock_mode_decision.bmcost(%rip), %xmm0
	movapd	%xmm0, 288(%rsp)
	movl	$0, 204(%rsp)
	movq	img(%rip), %rax
	leaq	14176(%rax), %rcx
	leaq	14184(%rax), %rdx
	leaq	14192(%rax), %rdi
	leaq	14200(%rax), %rax
	leal	(,%r14,4), %ebp
	andl	$-8, %ebp
	movq	%rbp, 208(%rsp)         # 8-byte Spill
	movl	%r14d, %ebp
	andl	$1, %ebp
	movq	%rbp, 224(%rsp)         # 8-byte Spill
	leal	(,%rbp,8), %r15d
	cmpl	$0, 696(%rsp)
	cmovneq	%rdx, %rcx
	movq	(%rcx), %rcx
	movq	%rcx, 368(%rsp)         # 8-byte Spill
	cmoveq	%rdi, %rax
	movq	(%rax), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movb	$0, 111(%rsp)
	movw	$-256, 108(%rsp)
	je	.LBB8_2
# BB#1:
	movl	$1, 472(%rsi)
.LBB8_2:
	movq	680(%rsp), %rbp
	movq	cs_cm(%rip), %rdi
	callq	store_coding_state
	xorl	%r11d, %r11d
	cmpw	$0, 156(%rsp)           # 2-byte Folded Reload
	sete	%r11b
	movq	224(%rsp), %rax         # 8-byte Reload
	addl	%eax, %eax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movl	%r14d, %eax
	andl	$2, %eax
	movl	%eax, 200(%rsp)         # 4-byte Spill
	movl	%r14d, %eax
	shrl	$31, %eax
	addl	%r14d, %eax
	andl	$126, %eax
	movl	%r14d, %ecx
	subl	%eax, %ecx
	movq	208(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rcx,2), %ecx
	movl	$51, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	notl	%edx
	movl	%edx, 188(%rsp)         # 4-byte Spill
	movslq	%eax, %rdx
	xorl	%eax, %eax
	cmpl	$0, 696(%rsp)
	sete	%al
	leaq	2(%rax,%rax,2), %r13
	movq	%rdx, %r10
	shlq	$6, %r10
	movq	%rdx, %rax
	orq	$1, %rax
	movl	%edx, %r8d
	orl	$7, %r8d
	movq	%rax, 328(%rsp)         # 8-byte Spill
	movq	%rax, %r9
	shlq	$6, %r9
	movq	%rdx, %rax
	orq	$3, %rax
	movq	%rax, 344(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	shlq	$6, %rdi
	movq	%rdx, %rcx
	orq	$7, %rcx
	movq	%rcx, %rax
	movq	%rax, 336(%rsp)         # 8-byte Spill
	shlq	$6, %rcx
	movl	%r15d, %r12d
	orl	$7, %r12d
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	shlq	$5, %rdx
	movl	%r15d, 184(%rsp)        # 4-byte Spill
	movl	%r15d, %esi
	leaq	(%rdx,%rsi,2), %rax
	movq	240(%rsp), %rbx         # 8-byte Reload
	leaq	6680(%rbx,%rax), %r15
	leaq	(%r10,%rsi,4), %rax
	leaq	7192(%rbx,%rax), %rax
	leaq	576(%rsp), %rbx
	movq	%rax, 264(%rsp)         # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movsd	.LCPI8_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	%xmm1, 168(%rsp)        # 8-byte Spill
	movslq	%r14d, %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	%r10, %rax
	movq	%rax, 320(%rsp)         # 8-byte Spill
	leaq	submacroblock_mode_decision.fadjust(%r10,%rsi,4), %rax
	movq	%rax, 472(%rsp)         # 8-byte Spill
	movq	%r9, 384(%rsp)          # 8-byte Spill
	leaq	submacroblock_mode_decision.fadjust(%r9,%rsi,4), %rax
	movq	%rax, 424(%rsp)         # 8-byte Spill
	movq	%rdi, 392(%rsp)         # 8-byte Spill
	leaq	submacroblock_mode_decision.fadjust(%rdi,%rsi,4), %rax
	movq	%rax, 416(%rsp)         # 8-byte Spill
	movq	%rcx, 400(%rsp)         # 8-byte Spill
	leaq	submacroblock_mode_decision.fadjust(%rcx,%rsi,4), %rax
	movq	%rax, 408(%rsp)         # 8-byte Spill
	movl	%r8d, 192(%rsp)         # 4-byte Spill
	movslq	%r8d, %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	leaq	-1(%rsi), %rax
	movq	%rax, 504(%rsp)         # 8-byte Spill
	movq	%rsi, 216(%rsp)         # 8-byte Spill
	movq	%rdx, %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	leaq	12624(%rdx,%rsi,2), %rax
	movq	%rax, 464(%rsp)         # 8-byte Spill
	movq	%r13, %rsi
	movq	%r14, %r13
	movq	%r11, %rdx
	xorl	%eax, %eax
	movq	%rax, 376(%rsp)         # 8-byte Spill
	movl	$2147483647, 120(%rsp)  # 4-byte Folded Spill
                                        # imm = 0x7FFFFFFF
	movq	%r13, 128(%rsp)         # 8-byte Spill
	movq	%rsi, 488(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB8_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_29 Depth 2
                                        #     Child Loop BB8_73 Depth 2
                                        #       Child Loop BB8_74 Depth 3
                                        #     Child Loop BB8_86 Depth 2
	movl	b8_mode_table(,%rdx,4), %eax
	movl	$0, (%rbp)
	movl	%eax, 136(%rsp)         # 4-byte Spill
	movslq	%eax, %r14
	cmpw	$0, 44(%rbx,%r14,2)
	je	.LBB8_93
# BB#10:                                #   in Loop: Header=BB8_9 Depth=1
	testq	%rdx, %rdx
	setne	%al
	cmpq	$5, %rdx
	setne	%cl
	cmpl	$0, 696(%rsp)
	je	.LBB8_15
# BB#11:                                #   in Loop: Header=BB8_9 Depth=1
	andb	%cl, %al
	jne	.LBB8_15
# BB#12:                                #   in Loop: Header=BB8_9 Depth=1
	cmpl	$5, %edx
	je	.LBB8_14
# BB#13:                                #   in Loop: Header=BB8_9 Depth=1
	testl	%edx, %edx
	jne	.LBB8_93
.LBB8_14:                               #   in Loop: Header=BB8_9 Depth=1
	movq	active_sps(%rip), %rax
	cmpl	$0, 1156(%rax)
	je	.LBB8_93
.LBB8_15:                               # %._crit_edge508
                                        #   in Loop: Header=BB8_9 Depth=1
	movq	$0, 176(%rsp)
	movq	input(%rip), %rax
	cmpl	$5, %edx
	movq	%rdx, 496(%rsp)         # 8-byte Spill
	je	.LBB8_17
# BB#16:                                # %._crit_edge508
                                        #   in Loop: Header=BB8_9 Depth=1
	testl	%edx, %edx
	jne	.LBB8_21
.LBB8_17:                               #   in Loop: Header=BB8_9 Depth=1
	cmpl	$0, 4168(%rax)
	jne	.LBB8_60
# BB#18:                                #   in Loop: Header=BB8_9 Depth=1
	movsd	%xmm0, 112(%rsp)        # 8-byte Spill
	movl	$0, 124(%rsp)
	movl	%r13d, %edi
	leaq	124(%rsp), %rsi
	callq	GetDirectCost8x8
	cmpl	$2147483647, %eax       # imm = 0x7FFFFFFF
	je	.LBB8_20
# BB#19:                                #   in Loop: Header=BB8_9 Depth=1
	movq	672(%rsp), %rcx
	movl	(%rcx), %ecx
	cmpl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	jne	.LBB8_50
.LBB8_20:                               #   in Loop: Header=BB8_9 Depth=1
	cmpl	$0, 696(%rsp)
	movq	672(%rsp), %rcx
	movl	$2147483647, (%rcx)     # imm = 0x7FFFFFFF
	movl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	jne	.LBB8_52
	jmp	.LBB8_56
.LBB8_21:                               #   in Loop: Header=BB8_9 Depth=1
	movl	32(%rbx), %ecx
	cmpl	$0, 4172(%rax)
	movsd	%xmm0, 112(%rsp)        # 8-byte Spill
	je	.LBB8_23
# BB#22:                                #   in Loop: Header=BB8_9 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movsd	lambda_mf_factor(%rip), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	movl	%eax, 272(%rsp)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	36(%rbx), %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	movl	%eax, 276(%rsp)
	movl	40(%rbx), %eax
	movl	%eax, 196(%rsp)         # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	lambda_mf_factor(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	jmp	.LBB8_24
.LBB8_23:                               # %.thread510
                                        #   in Loop: Header=BB8_9 Depth=1
	movl	%ecx, 272(%rsp)
	movl	36(%rbx), %eax
	movl	%eax, 276(%rsp)
	movl	40(%rbx), %eax
	movl	%eax, 196(%rsp)         # 4-byte Spill
.LBB8_24:                               #   in Loop: Header=BB8_9 Depth=1
	movl	%eax, 280(%rsp)
	movl	136(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, %edi
	movl	%r13d, %esi
	leaq	272(%rsp), %rdx
	callq	PartitionMotionSearch
	movl	$2147483647, 288(%rsp)  # imm = 0x7FFFFFFF
	movups	80(%rbx), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	64(%rbx), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	(%rbx), %xmm0
	movupd	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movups	48(%rbx), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$0, %edi
	movl	%r13d, %esi
	movl	%ebp, %edx
	leaq	288(%rsp), %rcx
	leaq	108(%rsp), %r8
	callq	list_prediction_cost
	movq	img(%rip), %rax
	movslq	168(%rax), %rcx
	movslq	224(%rsp), %r8          # 4-byte Folded Reload
	addq	%rcx, %r8
	movslq	172(%rax), %r10
	movslq	200(%rsp), %rsi         # 4-byte Folded Reload
	leaq	(%r10,%rsi), %rax
	leal	2(%rax), %ecx
	movb	108(%rsp), %dl
	movswq	74(%rbx), %rdi
	movsbq	%dl, %r9
	movslq	%ecx, %r11
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rax,8), %rcx
	movb	%r9b, (%rcx,%r8)
	movq	enc_picture(%rip), %rbp
	imulq	$264, %rdi, %rcx        # imm = 0x108
	leaq	(%rbp,%rcx), %rdi
	movq	24(%rdi,%r9,8), %rdi
	movq	6496(%rbp), %rbx
	movq	(%rbx), %rbx
	movq	(%rbx,%rax,8), %rbx
	movq	%rdi, (%rbx,%r8,8)
	movq	6488(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	(%rdi,%rax,8), %rdi
	movb	%r9b, 1(%rdi,%r8)
	movq	enc_picture(%rip), %rdi
	leaq	(%rdi,%rcx), %rbp
	movq	24(%rbp,%r9,8), %rbp
	movq	6496(%rdi), %rbx
	movq	(%rbx), %rbx
	movq	(%rbx,%rax,8), %rbx
	movq	%rbp, 8(%rbx,%r8,8)
	leaq	1(%r10,%rsi), %rbp
	cmpq	%r11, %rbp
	jge	.LBB8_26
# BB#25:                                # %.preheader416.1.1
                                        #   in Loop: Header=BB8_9 Depth=1
	movq	6488(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	8(%rdx,%rax,8), %rdx
	movb	%r9b, (%rdx,%r8)
	movq	enc_picture(%rip), %rdx
	leaq	(%rdx,%rcx), %rsi
	movq	24(%rsi,%r9,8), %rsi
	movq	6496(%rdx), %rdi
	movq	(%rdi), %rdi
	movq	8(%rdi,%rax,8), %rdi
	movq	%rsi, (%rdi,%r8,8)
	movq	6488(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	8(%rdx,%rax,8), %rdx
	movb	%r9b, 1(%rdx,%r8)
	movq	enc_picture(%rip), %rdx
	addq	%rdx, %rcx
	movq	24(%rcx,%r9,8), %rsi
	movq	6496(%rdx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx,%rax,8), %rcx
	movq	%rsi, 8(%rcx,%r8,8)
.LBB8_26:                               # %.loopexit511
                                        #   in Loop: Header=BB8_9 Depth=1
	cmpw	$0, 156(%rsp)           # 2-byte Folded Reload
	movsd	112(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movl	196(%rsp), %r13d        # 4-byte Reload
	je	.LBB8_49
# BB#27:                                #   in Loop: Header=BB8_9 Depth=1
	movq	%rbp, 432(%rsp)         # 8-byte Spill
	movq	%r11, 440(%rsp)         # 8-byte Spill
	movq	%rax, 448(%rsp)         # 8-byte Spill
	movq	%r8, 456(%rsp)          # 8-byte Spill
	movabsq	$9223372034707292159, %rax # imm = 0x7FFFFFFF7FFFFFFF
	movq	%rax, 292(%rsp)
	leaq	576(%rsp), %rbx
	movswq	76(%rbx), %rax
	movl	listXsize(,%rax,4), %eax
	testl	%eax, %eax
	jle	.LBB8_47
# BB#28:                                # %.lr.ph.i
                                        #   in Loop: Header=BB8_9 Depth=1
	movsd	24(%rbx), %xmm0         # xmm0 = mem[0],zero
	movq	input(%rip), %rcx
	addsd	%xmm0, %xmm0
	movq	motion_cost(%rip), %r10
	movq	refbits(%rip), %rdx
	movq	%rdx, 480(%rsp)         # 8-byte Spill
	movq	img(%rip), %r11
	movb	109(%rsp), %dl
	movb	%dl, 232(%rsp)          # 1-byte Spill
	movl	2152(%rcx), %esi
	xorl	%ebp, %ebp
	movl	$2147483647, %r9d       # imm = 0x7FFFFFFF
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB8_29:                               # %.lr.ph.split.us.i
                                        #   Parent Loop BB8_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%esi, %esi
	jne	.LBB8_31
# BB#30:                                #   in Loop: Header=BB8_29 Depth=2
	cmpl	$0, 2156(%rcx)
	je	.LBB8_38
.LBB8_31:                               #   in Loop: Header=BB8_29 Depth=2
	movl	20(%r11), %ebx
	cmpl	$3, %ebx
	je	.LBB8_33
# BB#32:                                #   in Loop: Header=BB8_29 Depth=2
	testl	%ebx, %ebx
	jne	.LBB8_38
.LBB8_33:                               #   in Loop: Header=BB8_29 Depth=2
	testl	%esi, %esi
	jne	.LBB8_35
# BB#34:                                # %.thread.i
                                        #   in Loop: Header=BB8_29 Depth=2
	cmpl	$0, 2156(%rcx)
	je	.LBB8_45
.LBB8_35:                               #   in Loop: Header=BB8_29 Depth=2
	cmpl	$3, %ebx
	je	.LBB8_37
# BB#36:                                #   in Loop: Header=BB8_29 Depth=2
	testl	%ebx, %ebx
	jne	.LBB8_45
.LBB8_37:                               #   in Loop: Header=BB8_29 Depth=2
	testw	%dx, %dx
	jne	.LBB8_45
.LBB8_38:                               #   in Loop: Header=BB8_29 Depth=2
	cmpl	$0, 4168(%rcx)
	je	.LBB8_42
# BB#39:                                #   in Loop: Header=BB8_29 Depth=2
	xorl	%ebx, %ebx
	cmpl	$1, %eax
	je	.LBB8_41
# BB#40:                                #   in Loop: Header=BB8_29 Depth=2
	movswq	%dx, %rbx
	movq	480(%rsp), %rdi         # 8-byte Reload
	movl	(%rdi,%rbx,4), %ebx
.LBB8_41:                               #   in Loop: Header=BB8_29 Depth=2
	imull	%r13d, %ebx
	sarl	$16, %ebx
	jmp	.LBB8_43
	.p2align	4, 0x90
.LBB8_42:                               #   in Loop: Header=BB8_29 Depth=2
	cmpl	$2, %ebp
	movl	$1, %edi
	cmovgel	%edi, %ebp
	cvtsi2sdl	%ebp, %xmm1
	mulsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %ebx
.LBB8_43:                               #   in Loop: Header=BB8_29 Depth=2
	movq	(%r10,%r14,8), %rbp
	movq	8(%rbp), %rbp
	movswq	%dx, %r8
	movq	(%rbp,%r8,8), %rbp
	movq	144(%rsp), %rdi         # 8-byte Reload
	addl	(%rbp,%rdi,4), %ebx
	cmpl	%r9d, %ebx
	jge	.LBB8_45
# BB#44:                                #   in Loop: Header=BB8_29 Depth=2
	movl	%ebx, 292(%rsp)
	movl	%ebx, %r9d
	movl	%edx, %ebx
	movb	%bl, 232(%rsp)          # 1-byte Spill
.LBB8_45:                               #   in Loop: Header=BB8_29 Depth=2
	incl	%edx
	movswl	%dx, %ebp
	cmpl	%eax, %ebp
	jl	.LBB8_29
# BB#46:                                # %list_prediction_cost.exit.loopexit
                                        #   in Loop: Header=BB8_9 Depth=1
	movb	232(%rsp), %al          # 1-byte Reload
	movb	%al, 109(%rsp)
	leaq	576(%rsp), %rbx
.LBB8_47:                               # %.lr.ph432
                                        #   in Loop: Header=BB8_9 Depth=1
	movups	80(%rbx), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	64(%rbx), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	(%rbx), %xmm0
	movupd	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movups	48(%rbx), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movupd	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$2, %edi
	movq	128(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	136(%rsp), %r13d        # 4-byte Reload
	movl	%r13d, %edx
	leaq	288(%rsp), %rbp
	movq	%rbp, %rcx
	leaq	108(%rsp), %r14
	movq	%r14, %r8
	callq	list_prediction_cost
	movl	%r13d, %edi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	leaq	111(%rsp), %rcx
	movq	680(%rsp), %r8
	leaq	286(%rsp), %r9
	callq	determine_prediction_list
	movzbl	108(%rsp), %ecx
	movl	%ecx, %edx
	shll	$8, %edx
	addl	%ecx, %edx
	movzbl	109(%rsp), %r8d
	movl	%r8d, %esi
	shll	$8, %esi
	addl	%r8d, %esi
	movq	432(%rsp), %rax         # 8-byte Reload
	cmpq	440(%rsp), %rax         # 8-byte Folded Reload
	movq	enc_picture(%rip), %rdi
	movq	6488(%rdi), %rdi
	movq	(%rdi), %rdi
	movq	448(%rsp), %rax         # 8-byte Reload
	movq	(%rdi,%rax,8), %rdi
	movq	456(%rsp), %rbp         # 8-byte Reload
	movw	%dx, (%rdi,%rbp)
	movq	enc_picture(%rip), %rdx
	movq	6488(%rdx), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%rax,8), %rdx
	movw	%si, (%rdx,%rbp)
	movsd	112(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	jge	.LBB8_61
# BB#48:                                #   in Loop: Header=BB8_9 Depth=1
	movq	enc_picture(%rip), %rdx
	movq	6488(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	8(%rdx,%rax,8), %rdx
	movzbl	%cl, %ecx
	movl	%ecx, %esi
	shll	$8, %esi
	addl	%ecx, %esi
	movw	%si, (%rdx,%rbp)
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	8(%rcx,%rax,8), %rcx
	movzbl	%r8b, %eax
	movl	%eax, %edx
	shll	$8, %edx
	addl	%eax, %edx
	movw	%dx, (%rcx,%rbp)
	jmp	.LBB8_61
.LBB8_49:                               #   in Loop: Header=BB8_9 Depth=1
	movb	$0, 111(%rsp)
	movl	288(%rsp), %eax
	movq	680(%rsp), %rcx
	movl	%eax, (%rcx)
	leaq	576(%rsp), %rbx
	jmp	.LBB8_61
.LBB8_50:                               #   in Loop: Header=BB8_9 Depth=1
	addl	%eax, %ecx
	movq	672(%rsp), %rdx
	movl	%ecx, (%rdx)
	cmpl	$0, 696(%rsp)
	je	.LBB8_56
# BB#51:                                #   in Loop: Header=BB8_9 Depth=1
	movq	688(%rsp), %rcx
	movl	(%rcx), %ecx
	addl	124(%rsp), %ecx
.LBB8_52:                               # %.critedge
                                        #   in Loop: Header=BB8_9 Depth=1
	movq	688(%rsp), %rdx
	movl	%ecx, (%rdx)
	movq	352(%rsp), %rcx         # 8-byte Reload
	incl	(%rcx)
	movq	input(%rip), %rcx
	movl	5100(%rcx), %edx
	cmpl	$2, %edx
	je	.LBB8_57
# BB#53:                                # %.critedge
                                        #   in Loop: Header=BB8_9 Depth=1
	cmpl	$1, %edx
	movsd	112(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	jne	.LBB8_58
# BB#54:                                #   in Loop: Header=BB8_9 Depth=1
	movl	124(%rsp), %edx
	cmpl	%eax, %edx
	jge	.LBB8_3
# BB#55:                                #   in Loop: Header=BB8_9 Depth=1
	movl	%edx, %eax
	jmp	.LBB8_58
.LBB8_56:                               # %.sink.split
                                        #   in Loop: Header=BB8_9 Depth=1
	movq	352(%rsp), %rcx         # 8-byte Reload
	incl	(%rcx)
	movsd	112(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	jmp	.LBB8_59
.LBB8_57:                               #   in Loop: Header=BB8_9 Depth=1
	movl	124(%rsp), %eax
	movsd	112(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	jmp	.LBB8_58
.LBB8_3:                                #   in Loop: Header=BB8_9 Depth=1
	cmpw	$0, 54(%rbx)
	je	.LBB8_8
# BB#4:                                 #   in Loop: Header=BB8_9 Depth=1
	cmpw	$0, 56(%rbx)
	je	.LBB8_8
# BB#5:                                 #   in Loop: Header=BB8_9 Depth=1
	cmpw	$0, 58(%rbx)
	jne	.LBB8_58
.LBB8_8:                                #   in Loop: Header=BB8_9 Depth=1
	movl	%edx, %eax
.LBB8_58:                               #   in Loop: Header=BB8_9 Depth=1
	movl	%eax, (%rbp)
	cmpl	$2, 5100(%rcx)
	movl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	cmovel	%ecx, %eax
.LBB8_59:                               # %.sink.split384
                                        #   in Loop: Header=BB8_9 Depth=1
	movl	%eax, (%rbp)
.LBB8_60:                               #   in Loop: Header=BB8_9 Depth=1
	movq	img(%rip), %rax
	movslq	168(%rax), %rcx
	movslq	224(%rsp), %rdx         # 4-byte Folded Reload
	addq	%rcx, %rdx
	movslq	172(%rax), %rax
	movslq	200(%rsp), %rcx         # 4-byte Folded Reload
	addq	%rax, %rcx
	movq	direct_ref_idx(%rip), %rax
	movq	(%rax), %rsi
	movq	(%rsi,%rcx,8), %rsi
	movb	(%rsi,%rdx), %bl
	movb	%bl, 108(%rsp)
	leaq	576(%rsp), %rbx
	movq	8(%rax), %rax
	movq	(%rax,%rcx,8), %rax
	movb	(%rax,%rdx), %al
	movb	%al, 109(%rsp)
	movq	direct_pdir(%rip), %rax
	movq	(%rax,%rcx,8), %rax
	movb	(%rax,%rdx), %al
	movb	%al, 111(%rsp)
.LBB8_61:                               # %.loopexit421
                                        #   in Loop: Header=BB8_9 Depth=1
	movq	input(%rip), %rax
	cmpl	$0, 4168(%rax)
	je	.LBB8_63
# BB#62:                                #   in Loop: Header=BB8_9 Depth=1
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movsbl	108(%rsp), %r9d
	movsbl	111(%rsp), %r8d
	movsbl	109(%rsp), %eax
	movl	%eax, (%rsp)
	leaq	204(%rsp), %rdi
	leaq	176(%rsp), %rsi
	movq	128(%rsp), %r13         # 8-byte Reload
	movl	%r13d, %edx
	movl	136(%rsp), %ecx         # 4-byte Reload
	callq	RDCost_for_8x8blocks
	movq	680(%rsp), %rbp
	jmp	.LBB8_68
.LBB8_63:                               #   in Loop: Header=BB8_9 Depth=1
	movq	680(%rsp), %rbp
	movl	(%rbp), %eax
	cmpl	$2147483647, %eax       # imm = 0x7FFFFFFF
	jne	.LBB8_65
# BB#64:                                #   in Loop: Header=BB8_9 Depth=1
	movq	128(%rsp), %r13         # 8-byte Reload
	jmp	.LBB8_68
.LBB8_65:                               #   in Loop: Header=BB8_9 Depth=1
	movq	%rbx, %rdx
	movl	40(%rdx), %ebx
	movsbl	111(%rsp), %esi
	xorl	%ecx, %ecx
	testl	%esi, %esi
	setg	%cl
	movswq	74(%rdx,%rcx,2), %rdx
	xorl	%ecx, %ecx
	cmpl	$2, listXsize(,%rdx,4)
	movq	128(%rsp), %r13         # 8-byte Reload
	jl	.LBB8_67
# BB#66:                                #   in Loop: Header=BB8_9 Depth=1
	movq	refbits(%rip), %r13
	movl	136(%rsp), %edi         # 4-byte Reload
	movsd	%xmm0, 112(%rsp)        # 8-byte Spill
	callq	B8Mode2Value
	movsd	112(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cltq
	movl	(%r13,%rax,4), %ecx
	movq	128(%rsp), %r13         # 8-byte Reload
	movl	(%rbp), %eax
.LBB8_67:                               #   in Loop: Header=BB8_9 Depth=1
	imull	%ebx, %ecx
	sarl	$16, %ecx
	leal	-1(%rax,%rcx), %eax
	movl	%eax, (%rbp)
	leaq	576(%rsp), %rbx
	.p2align	4, 0x90
.LBB8_68:                               #   in Loop: Header=BB8_9 Depth=1
	movq	input(%rip), %rax
	movl	4168(%rax), %ecx
	movsd	168(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	movsd	%xmm0, 112(%rsp)        # 8-byte Spill
	jbe	.LBB8_90
# BB#69:                                #   in Loop: Header=BB8_9 Depth=1
	testl	%ecx, %ecx
	je	.LBB8_90
# BB#70:                                # %._crit_edge
                                        #   in Loop: Header=BB8_9 Depth=1
	movl	(%rbp), %edi
.LBB8_71:                               #   in Loop: Header=BB8_9 Depth=1
	movq	240(%rsp), %rdx         # 8-byte Reload
	movq	144(%rsp), %rbx         # 8-byte Reload
	movl	136(%rsp), %esi         # 4-byte Reload
	movw	%si, 6148(%rdx,%rbx,2)
	movb	111(%rsp), %cl
	movb	%cl, 6156(%rdx,%rbx)
	movb	108(%rsp), %cl
	movb	%cl, 6160(%rdx,%rbx)
	movb	109(%rsp), %cl
	movb	%cl, 6164(%rdx,%rbx)
	movq	img(%rip), %r13
	movslq	12(%r13), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	addq	14224(%r13), %rcx
	movl	%esi, 376(%rcx,%rbx,4)
	movl	204(%rsp), %ecx
	cmpl	$0, 4168(%rax)
	movq	%rcx, 376(%rsp)         # 8-byte Spill
	movl	%edi, 120(%rsp)         # 4-byte Spill
	je	.LBB8_79
# BB#72:                                # %.preheader415
                                        #   in Loop: Header=BB8_9 Depth=1
	movl	cbp_blk8x8(%rip), %eax
	andl	188(%rsp), %eax         # 4-byte Folded Reload
	orl	176(%rsp), %eax
	movl	%eax, cbp_blk8x8(%rip)
	movq	360(%rsp), %rbp         # 8-byte Reload
	movq	(%rbp), %rax
	movq	(%rax), %rdi
	movq	14160(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	(%rbp), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	8(%rbp), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	8(%rbp), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	16(%rbp), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	16(%rbp), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	24(%rbp), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	24(%rbp), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %r13
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %r8
	movl	si_frame_indicator(%rip), %r14d
	movq	lrec(%rip), %r11
	movq	464(%rsp), %rax         # 8-byte Reload
	leaq	(%r13,%rax), %rsi
	xorl	%r9d, %r9d
	movq	160(%rsp), %r10         # 8-byte Reload
	.p2align	4, 0x90
.LBB8_73:                               # %.preheader419
                                        #   Parent Loop BB8_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_74 Depth 3
	movslq	180(%r13), %rax
	movslq	%r10d, %rbx
	addq	%rax, %rbx
	movq	(%r8,%rbx,8), %rax
	movq	%r9, %rdi
	movq	504(%rsp), %rbp         # 8-byte Reload
	.p2align	4, 0x90
.LBB8_74:                               #   Parent Loop BB8_9 Depth=1
                                        #     Parent Loop BB8_73 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	176(%r13), %edx
	leal	1(%rbp,%rdx), %edx
	movslq	%edx, %rdx
	movzwl	(%rax,%rdx,2), %ecx
	movw	%cx, -512(%r15,%rdi)
	movzwl	(%rsi,%rdi), %ecx
	movw	%cx, (%r15,%rdi)
	cmpl	$3, 20(%r13)
	jne	.LBB8_77
# BB#75:                                #   in Loop: Header=BB8_74 Depth=3
	testl	%r14d, %r14d
	jne	.LBB8_77
# BB#76:                                #   in Loop: Header=BB8_74 Depth=3
	movq	(%r11,%rbx,8), %rcx
	movl	(%rcx,%rdx,4), %ecx
	movq	264(%rsp), %rdx         # 8-byte Reload
	movl	%ecx, (%rdx,%rdi,2)
.LBB8_77:                               #   in Loop: Header=BB8_74 Depth=3
	incq	%rbp
	addq	$2, %rdi
	cmpq	%r12, %rbp
	jl	.LBB8_74
# BB#78:                                #   in Loop: Header=BB8_73 Depth=2
	addq	$32, %r9
	cmpq	256(%rsp), %r10         # 8-byte Folded Reload
	leaq	1(%r10), %r10
	jl	.LBB8_73
.LBB8_79:                               # %.loopexit420
                                        #   in Loop: Header=BB8_9 Depth=1
	cmpl	$0, 15260(%r13)
	je	.LBB8_87
# BB#80:                                # %.preheader418.preheader
                                        #   in Loop: Header=BB8_9 Depth=1
	movq	160(%rsp), %rdx         # 8-byte Reload
	cmpl	192(%rsp), %edx         # 4-byte Folded Reload
	movq	368(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	216(%rsp), %rsi         # 8-byte Reload
	movups	16(%rax,%rsi,4), %xmm0
	movq	472(%rsp), %rdx         # 8-byte Reload
	movups	%xmm0, 16(%rdx)
	movupd	(%rax,%rsi,4), %xmm0
	movupd	%xmm0, (%rdx)
	jge	.LBB8_82
# BB#81:                                # %.preheader418.1
                                        #   in Loop: Header=BB8_9 Depth=1
	movq	(%rcx), %rax
	movq	328(%rsp), %rdx         # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movups	16(%rax,%rsi,4), %xmm0
	movq	424(%rsp), %rdi         # 8-byte Reload
	movups	%xmm0, 16(%rdi)
	movups	(%rax,%rsi,4), %xmm0
	movups	%xmm0, (%rdi)
	movq	(%rcx), %rax
	movq	8(%rax,%rdx,8), %rax
	movups	16(%rax,%rsi,4), %xmm0
	movups	%xmm0, 80(%rdi)
	movups	(%rax,%rsi,4), %xmm0
	movups	%xmm0, 64(%rdi)
	movq	(%rcx), %rax
	movq	344(%rsp), %rdx         # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movups	16(%rax,%rsi,4), %xmm0
	movq	416(%rsp), %rdi         # 8-byte Reload
	movups	%xmm0, 16(%rdi)
	movups	(%rax,%rsi,4), %xmm0
	movups	%xmm0, (%rdi)
	movq	(%rcx), %rax
	movq	8(%rax,%rdx,8), %rax
	movups	16(%rax,%rsi,4), %xmm0
	movups	%xmm0, 80(%rdi)
	movups	(%rax,%rsi,4), %xmm0
	movups	%xmm0, 64(%rdi)
	movq	(%rcx), %rax
	movq	16(%rax,%rdx,8), %rax
	movups	16(%rax,%rsi,4), %xmm0
	movups	%xmm0, 144(%rdi)
	movups	(%rax,%rsi,4), %xmm0
	movups	%xmm0, 128(%rdi)
	movq	(%rcx), %rax
	movq	24(%rax,%rdx,8), %rax
	movups	16(%rax,%rsi,4), %xmm0
	movups	%xmm0, 208(%rdi)
	movups	(%rax,%rsi,4), %xmm0
	movups	%xmm0, 192(%rdi)
	movq	(%rcx), %rax
	movq	336(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movups	16(%rax,%rsi,4), %xmm0
	movq	408(%rsp), %rcx         # 8-byte Reload
	movups	%xmm0, 16(%rcx)
	movupd	(%rax,%rsi,4), %xmm0
	movupd	%xmm0, (%rcx)
.LBB8_82:                               #   in Loop: Header=BB8_9 Depth=1
	movq	input(%rip), %rax
	cmpl	$0, 5660(%rax)
	je	.LBB8_87
# BB#83:                                #   in Loop: Header=BB8_9 Depth=1
	movl	15548(%r13), %eax
	movl	%eax, %ecx
	sarl	%ecx
	testl	%ecx, %ecx
	jle	.LBB8_87
# BB#84:                                # %.lr.ph439
                                        #   in Loop: Header=BB8_9 Depth=1
	imull	208(%rsp), %eax         # 4-byte Folded Reload
	movl	%eax, %esi
	sarl	$31, %esi
	shrl	$28, %esi
	addl	%eax, %esi
	sarl	$4, %esi
	movl	15544(%r13), %eax
	movl	%eax, %ecx
	imull	184(%rsp), %ecx         # 4-byte Folded Reload
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$28, %edx
	addl	%ecx, %edx
	sarl	$4, %edx
	movslq	%edx, %rdx
	movl	%esi, 168(%rsp)         # 4-byte Spill
	movslq	%esi, %rcx
	leaq	1(%rcx), %rbx
	shlq	$6, %rcx
	movq	%rdx, 232(%rsp)         # 8-byte Spill
	leaq	submacroblock_mode_decision.fadjustCr(%rcx,%rdx,4), %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	jmp	.LBB8_86
	.p2align	4, 0x90
.LBB8_85:                               # %._crit_edge496
                                        #   in Loop: Header=BB8_86 Depth=2
	movl	15544(%r13), %eax
	incq	%rbx
	addq	$64, 136(%rsp)          # 8-byte Folded Spill
.LBB8_86:                               #   Parent Loop BB8_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	248(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	232(%rsp), %rdx         # 8-byte Reload
	leaq	(,%rdx,4), %rbp
	movq	-8(%rcx,%rbx,8), %rsi
	addq	%rbp, %rsi
	sarl	%eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	movq	136(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rdi
	callq	memcpy
	leaq	1024(%r14), %rdi
	movq	248(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	movq	8(%rax), %rax
	addq	-8(%rax,%rbx,8), %rbp
	movl	15544(%r13), %eax
	sarl	%eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	movq	%rbp, %rsi
	callq	memcpy
	movl	15548(%r13), %eax
	sarl	%eax
	addl	168(%rsp), %eax         # 4-byte Folded Reload
	cltq
	cmpq	%rax, %rbx
	jl	.LBB8_85
.LBB8_87:                               # %.loopexit417
                                        #   in Loop: Header=BB8_9 Depth=1
	movq	128(%rsp), %r13         # 8-byte Reload
	cmpl	$2, %r13d
	jg	.LBB8_89
# BB#88:                                #   in Loop: Header=BB8_9 Depth=1
	movq	cs_b8(%rip), %rdi
	callq	store_coding_state
.LBB8_89:                               #   in Loop: Header=BB8_9 Depth=1
	movsd	112(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 168(%rsp)        # 8-byte Spill
	movq	680(%rsp), %rbp
	leaq	576(%rsp), %rbx
	jmp	.LBB8_92
	.p2align	4, 0x90
.LBB8_90:                               #   in Loop: Header=BB8_9 Depth=1
	testl	%ecx, %ecx
	jne	.LBB8_92
# BB#91:                                #   in Loop: Header=BB8_9 Depth=1
	movl	(%rbp), %edi
	cmpl	120(%rsp), %edi         # 4-byte Folded Reload
	jl	.LBB8_71
	.p2align	4, 0x90
.LBB8_92:                               #   in Loop: Header=BB8_9 Depth=1
	movq	cs_cm(%rip), %rdi
	callq	reset_coding_state
	movsd	112(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	496(%rsp), %rdx         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
.LBB8_93:                               #   in Loop: Header=BB8_9 Depth=1
	incq	%rdx
	cmpq	%rsi, %rdx
	jl	.LBB8_9
# BB#94:
	cmpl	$0, 696(%rsp)
	movq	240(%rsp), %r12         # 8-byte Reload
	jne	.LBB8_96
# BB#95:
	movl	120(%rsp), %eax         # 4-byte Reload
	addl	%eax, (%r12)
.LBB8_96:
	movq	input(%rip), %rax
	cmpl	$0, 4168(%rax)
	movl	184(%rsp), %r15d        # 4-byte Reload
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	376(%rsp), %rdx         # 8-byte Reload
	jne	.LBB8_107
# BB#97:
	cmpl	$0, 696(%rsp)
	je	.LBB8_99
# BB#98:
	movl	120(%rsp), %ecx         # 4-byte Reload
	addl	%ecx, (%r12)
.LBB8_99:
	movq	%rax, %rcx
	movsbl	6156(%r12,%rcx), %eax
	movswl	6148(%r12,%rcx,2), %r9d
	movq	$0, 176(%rsp)
	xorl	%ebx, %ebx
	movl	%eax, %ecx
	orb	$2, %cl
	cmpb	$2, %cl
	movl	$0, %r8d
	jne	.LBB8_101
# BB#100:
	movl	%r9d, %r8d
.LBB8_101:                              # %.preheader414
	movl	%eax, %ecx
	decb	%cl
	cmpb	$2, %cl
	cmovael	%ebx, %r9d
	movq	144(%rsp), %r14         # 8-byte Reload
	movsbl	6160(%r12,%r14), %ecx
	movsbl	6164(%r12,%r14), %edx
	movl	%edx, 8(%rsp)
	movl	%ecx, (%rsp)
	movswl	%ax, %ecx
	leaq	516(%rsp), %rdi
	leaq	176(%rsp), %rsi
	movl	%r13d, %edx
	callq	LumaResidualCoding8x8
	movl	%eax, %r13d
	movl	188(%rsp), %eax         # 4-byte Reload
	andl	cbp_blk8x8(%rip), %eax
	orl	176(%rsp), %eax
	movl	%eax, cbp_blk8x8(%rip)
	movq	360(%rsp), %rbp         # 8-byte Reload
	movq	(%rbp), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	(%rbp), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	8(%rbp), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	8(%rbp), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	16(%rbp), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	16(%rbp), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	24(%rbp), %rax
	movq	(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	24(%rbp), %rax
	movq	8(%rax), %rdi
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	enc_picture(%rip), %r9
	movq	img(%rip), %rcx
	movl	si_frame_indicator(%rip), %r8d
	movq	160(%rsp), %rsi         # 8-byte Reload
	leaq	-1(%rsi), %r10
	movq	216(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rax), %r14
	movq	312(%rsp), %rax         # 8-byte Reload
	leaq	(%r12,%rax), %rbp
	leaq	12624(%rcx,%rax), %rdx
	movl	%esi, %r11d
	.p2align	4, 0x90
.LBB8_102:                              # =>This Inner Loop Header: Depth=1
	movq	6440(%r9), %rsi
	leal	(%r11,%rbx), %eax
	addl	180(%rcx), %eax
	cltq
	movq	(%rsi,%rax,8), %rax
	movslq	176(%rcx), %rdi
	movslq	%r15d, %rsi
	addq	%rsi, %rdi
	movups	(%rax,%rdi,2), %xmm0
	movups	%xmm0, 6168(%rbp,%r14)
	movupd	(%rdx,%r14), %xmm0
	movupd	%xmm0, 6680(%rbp,%r14)
	cmpl	$3, 20(%rcx)
	jne	.LBB8_105
# BB#103:                               #   in Loop: Header=BB8_102 Depth=1
	testl	%r8d, %r8d
	jne	.LBB8_105
# BB#104:                               #   in Loop: Header=BB8_102 Depth=1
	movq	lrec(%rip), %rax
	leal	(%r11,%rbx), %edi
	addl	180(%rcx), %edi
	movslq	%edi, %rdi
	movq	(%rax,%rdi,8), %rax
	movslq	176(%rcx), %rdi
	addq	%rsi, %rdi
	movups	16(%rax,%rdi,4), %xmm0
	movq	264(%rsp), %rsi         # 8-byte Reload
	movups	%xmm0, 16(%rsi)
	movupd	(%rax,%rdi,4), %xmm0
	movupd	%xmm0, (%rsi)
.LBB8_105:                              #   in Loop: Header=BB8_102 Depth=1
	leaq	1(%r10,%rbx), %rax
	incq	%rbx
	addq	$32, %rbp
	addq	$32, %rdx
	addq	$64, 264(%rsp)          # 8-byte Folded Spill
	cmpq	256(%rsp), %rax         # 8-byte Folded Reload
	jl	.LBB8_102
# BB#106:
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	%r13, %rdx
	movq	%rcx, %r13
.LBB8_107:                              # %.loopexit413
	testl	%edx, %edx
	je	.LBB8_109
# BB#108:
	movl	$1, %eax
	movl	%r13d, %ecx
	shll	%cl, %eax
	orl	%eax, cbp8x8(%rip)
	movq	144(%rsp), %rax         # 8-byte Reload
	addl	%edx, cnt_nonz_8x8(%rip)
.LBB8_109:
	cmpl	$0, 696(%rsp)
	je	.LBB8_111
# BB#110:
	movq	%rax, %rsi
	movswl	156(%rsp), %eax         # 2-byte Folded Reload
	movswl	6148(%r12,%rsi,2), %edx
	movsbl	6160(%r12,%rsi), %ecx
	movsbl	6164(%r12,%rsi), %r8d
	movsbl	6156(%r12,%rsi), %r9d
	movl	%eax, (%rsp)
	xorl	%edi, %edi
	movl	%r13d, %esi
	callq	StoreNewMotionVectorsBlock8x8
	jmp	.LBB8_117
.LBB8_111:
	cmpl	$2, %r13d
	jg	.LBB8_119
# BB#112:                               # %.preheader410.preheader
	movq	160(%rsp), %rax         # 8-byte Reload
	decq	%rax
	movq	312(%rsp), %rcx         # 8-byte Reload
	leaq	6168(%r12,%rcx), %rcx
	movq	320(%rsp), %rdx         # 8-byte Reload
	leaq	7192(%r12,%rdx), %rdx
	movq	208(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	.p2align	4, 0x90
.LBB8_113:                              # %.preheader410
                                        # =>This Inner Loop Header: Depth=1
	movq	enc_picture(%rip), %rdi
	movq	6440(%rdi), %rdi
	movq	img(%rip), %rbp
	movl	180(%rbp), %ebx
	addl	%esi, %ebx
	movslq	%ebx, %rbx
	movq	(%rdi,%rbx,8), %rdi
	movslq	176(%rbp), %rbp
	movupd	(%rcx), %xmm0
	movupd	%xmm0, (%rdi,%rbp,2)
	movq	img(%rip), %rdi
	cmpl	$3, 20(%rdi)
	jne	.LBB8_116
# BB#114:                               # %.preheader410
                                        #   in Loop: Header=BB8_113 Depth=1
	movl	si_frame_indicator(%rip), %ebp
	testl	%ebp, %ebp
	jne	.LBB8_116
# BB#115:                               #   in Loop: Header=BB8_113 Depth=1
	movq	lrec(%rip), %rbp
	movl	180(%rdi), %ebx
	addl	%esi, %ebx
	movslq	%ebx, %rbx
	movq	(%rbp,%rbx,8), %rbp
	movslq	176(%rdi), %rdi
	movupd	(%rdx), %xmm0
	movupd	%xmm0, (%rbp,%rdi,4)
.LBB8_116:                              #   in Loop: Header=BB8_113 Depth=1
	incq	%rax
	addq	$32, %rcx
	addq	$64, %rdx
	incl	%esi
	cmpq	256(%rsp), %rax         # 8-byte Folded Reload
	jl	.LBB8_113
.LBB8_117:                              # %.loopexit411
	movq	144(%rsp), %rax         # 8-byte Reload
	movswl	6148(%r12,%rax,2), %esi
	movsbl	6156(%r12,%rax), %edx
	movsbl	6160(%r12,%rax), %ecx
	movsbl	6164(%r12,%rax), %r8d
	movq	128(%rsp), %rbx         # 8-byte Reload
	movl	%ebx, %edi
	callq	SetRefAndMotionVectors
	cmpl	$2, %ebx
	movq	216(%rsp), %rbx         # 8-byte Reload
	jg	.LBB8_120
# BB#118:
	movq	cs_b8(%rip), %rdi
	callq	reset_coding_state
	jmp	.LBB8_120
.LBB8_119:                              # %.critedge513
	movswl	6148(%r12,%rax,2), %esi
	movsbl	6156(%r12,%rax), %edx
	movsbl	6160(%r12,%rax), %ecx
	movsbl	6164(%r12,%rax), %r8d
	movl	%r13d, %edi
	callq	SetRefAndMotionVectors
	movq	216(%rsp), %rbx         # 8-byte Reload
.LBB8_120:
	movq	img(%rip), %rax
	cmpl	$0, 15260(%rax)
	je	.LBB8_128
# BB#121:                               # %.preheader
	xorl	%eax, %eax
	cmpl	$0, 696(%rsp)
	sete	%al
	movq	368(%rsp), %rsi         # 8-byte Reload
	movq	16(%rsi,%rax,8), %rcx
	movq	160(%rsp), %rdx         # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	movq	320(%rsp), %rdi         # 8-byte Reload
	movups	submacroblock_mode_decision.fadjust+16(%rdi,%rbx,4), %xmm0
	movups	%xmm0, 16(%rcx,%rbx,4)
	movupd	submacroblock_mode_decision.fadjust(%rdi,%rbx,4), %xmm0
	movupd	%xmm0, (%rcx,%rbx,4)
	cmpl	192(%rsp), %edx         # 4-byte Folded Reload
	jge	.LBB8_123
# BB#122:
	orq	$2, %rax
	movq	(%rsi,%rax,8), %rcx
	movq	328(%rsp), %rdx         # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	movq	384(%rsp), %rdi         # 8-byte Reload
	movups	submacroblock_mode_decision.fadjust+16(%rdi,%rbx,4), %xmm0
	movups	%xmm0, 16(%rcx,%rbx,4)
	movups	submacroblock_mode_decision.fadjust(%rdi,%rbx,4), %xmm0
	movups	%xmm0, (%rcx,%rbx,4)
	movq	(%rsi,%rax,8), %rcx
	movq	8(%rcx,%rdx,8), %rcx
	incq	%rdx
	shlq	$6, %rdx
	movups	submacroblock_mode_decision.fadjust+16(%rdx,%rbx,4), %xmm0
	movups	%xmm0, 16(%rcx,%rbx,4)
	movups	submacroblock_mode_decision.fadjust(%rdx,%rbx,4), %xmm0
	movups	%xmm0, (%rcx,%rbx,4)
	movq	(%rsi,%rax,8), %rcx
	movq	344(%rsp), %rdi         # 8-byte Reload
	movq	(%rcx,%rdi,8), %rcx
	movq	392(%rsp), %rdx         # 8-byte Reload
	movups	submacroblock_mode_decision.fadjust+16(%rdx,%rbx,4), %xmm0
	movups	%xmm0, 16(%rcx,%rbx,4)
	movups	submacroblock_mode_decision.fadjust(%rdx,%rbx,4), %xmm0
	movups	%xmm0, (%rcx,%rbx,4)
	leaq	1(%rdi), %rcx
	movq	(%rsi,%rax,8), %rdx
	movq	8(%rdx,%rdi,8), %rdx
	shlq	$6, %rcx
	movups	submacroblock_mode_decision.fadjust+16(%rcx,%rbx,4), %xmm0
	movups	%xmm0, 16(%rdx,%rbx,4)
	movups	submacroblock_mode_decision.fadjust(%rcx,%rbx,4), %xmm0
	movups	%xmm0, (%rdx,%rbx,4)
	leaq	2(%rdi), %rcx
	movq	(%rsi,%rax,8), %rdx
	movq	16(%rdx,%rdi,8), %rdx
	shlq	$6, %rcx
	movups	submacroblock_mode_decision.fadjust+16(%rcx,%rbx,4), %xmm0
	movups	%xmm0, 16(%rdx,%rbx,4)
	movups	submacroblock_mode_decision.fadjust(%rcx,%rbx,4), %xmm0
	movups	%xmm0, (%rdx,%rbx,4)
	movq	(%rsi,%rax,8), %rcx
	movq	24(%rcx,%rdi,8), %rcx
	addq	$3, %rdi
	shlq	$6, %rdi
	movups	submacroblock_mode_decision.fadjust+16(%rdi,%rbx,4), %xmm0
	movups	%xmm0, 16(%rcx,%rbx,4)
	movups	submacroblock_mode_decision.fadjust(%rdi,%rbx,4), %xmm0
	movups	%xmm0, (%rcx,%rbx,4)
	movq	(%rsi,%rax,8), %rax
	movq	336(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	400(%rsp), %rcx         # 8-byte Reload
	movups	submacroblock_mode_decision.fadjust+16(%rcx,%rbx,4), %xmm0
	movups	%xmm0, 16(%rax,%rbx,4)
	movupd	submacroblock_mode_decision.fadjust(%rcx,%rbx,4), %xmm0
	movupd	%xmm0, (%rax,%rbx,4)
.LBB8_123:
	movq	input(%rip), %rax
	cmpl	$0, 5660(%rax)
	je	.LBB8_128
# BB#124:
	movq	img(%rip), %rax
	movl	15548(%rax), %r14d
	movl	%r14d, %ecx
	sarl	%ecx
	testl	%ecx, %ecx
	jle	.LBB8_128
# BB#125:                               # %.lr.ph
	imull	208(%rsp), %r14d        # 4-byte Folded Reload
	sarl	$4, %r14d
	movl	15544(%rax), %eax
	imull	%eax, %r15d
	sarl	$4, %r15d
	movl	%r15d, %ecx
	xorl	%r15d, %r15d
	cmpl	$0, 696(%rsp)
	sete	%r15b
	addq	%r15, %r15
	movslq	%ecx, %r12
	movslq	%r14d, %rcx
	leaq	1(%rcx), %rbp
	shlq	$6, %rcx
	leaq	submacroblock_mode_decision.fadjustCr(%rcx,%r12,4), %r13
	shlq	$2, %r12
	jmp	.LBB8_127
	.p2align	4, 0x90
.LBB8_126:                              # %._crit_edge502
                                        #   in Loop: Header=BB8_127 Depth=1
	movl	15544(%rax), %eax
	incq	%rbp
	addq	$64, %r13
.LBB8_127:                              # =>This Inner Loop Header: Depth=1
	movq	248(%rsp), %rbx         # 8-byte Reload
	movq	(%rbx,%r15,8), %rcx
	movq	(%rcx), %rcx
	movq	-8(%rcx,%rbp,8), %rdi
	addq	%r12, %rdi
	sarl	%eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	movq	%r13, %rsi
	callq	memcpy
	movq	(%rbx,%r15,8), %rax
	movq	8(%rax), %rax
	movq	-8(%rax,%rbp,8), %rdi
	addq	%r12, %rdi
	leaq	1024(%r13), %rsi
	movq	img(%rip), %rax
	movl	15544(%rax), %eax
	sarl	%eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	callq	memcpy
	movq	img(%rip), %rax
	movl	15548(%rax), %ecx
	sarl	%ecx
	addl	%r14d, %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rbp
	jl	.LBB8_126
.LBB8_128:                              # %.loopexit
	addq	$520, %rsp              # imm = 0x208
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	submacroblock_mode_decision, .Lfunc_end8-submacroblock_mode_decision
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI9_0:
	.quad	4607182418800017408     # double 1
.LCPI9_1:
	.quad	4602678819172646912     # double 0.5
.LCPI9_2:
	.quad	4647714815446351872     # double 512
	.text
	.globl	get_initial_mb16x16_cost
	.p2align	4, 0x90
	.type	get_initial_mb16x16_cost,@function
get_initial_mb16x16_cost:               # @get_initial_mb16x16_cost
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	movq	14224(%rax), %rdx
	movslq	12(%rax), %rcx
	imulq	$536, %rcx, %rsi        # imm = 0x218
	cmpq	$0, 64(%rdx,%rsi)
	movq	56(%rdx,%rsi), %rdx
	je	.LBB9_3
# BB#1:
	testq	%rdx, %rdx
	movq	mb16x16_cost_frame(%rip), %rdx
	movsd	-8(%rdx,%rcx,8), %xmm0  # xmm0 = mem[0],zero
	je	.LBB9_8
# BB#2:
	movl	52(%rax), %esi
	sarl	$4, %esi
	movslq	%esi, %rsi
	subq	%rsi, %rcx
	addsd	(%rdx,%rcx,8), %xmm0
	addsd	.LCPI9_0(%rip), %xmm0
	mulsd	.LCPI9_1(%rip), %xmm0
	jmp	.LBB9_8
.LBB9_3:                                # %.thread
	testq	%rdx, %rdx
	je	.LBB9_4
# BB#7:
	movq	mb16x16_cost_frame(%rip), %rdx
	movl	52(%rax), %esi
	sarl	$4, %esi
	subl	%esi, %ecx
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm0    # xmm0 = mem[0],zero
.LBB9_8:
	movsd	%xmm0, mb16x16_cost(%rip)
	movsd	.LCPI9_2(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB9_5
# BB#9:
	movsd	.LCPI9_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	%xmm1, lambda_mf_factor(%rip)
	retq
.LBB9_4:                                # %.thread5
	movabsq	$4647714815446351872, %rcx # imm = 0x4080000000000000
	movq	%rcx, mb16x16_cost(%rip)
	movsd	.LCPI9_2(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB9_5:
	movq	15504(%rax), %rcx
	movslq	20(%rax), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	36(%rax), %rax
	movsd	(%rcx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	.LCPI9_2(%rip), %xmm1
	divsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jp	.LBB9_6
# BB#10:
	movsd	%xmm1, lambda_mf_factor(%rip)
	retq
.LBB9_6:                                # %call.sqrt
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 16
	callq	sqrt
	movapd	%xmm0, %xmm1
	addq	$8, %rsp
	movsd	%xmm1, lambda_mf_factor(%rip)
	retq
.Lfunc_end9:
	.size	get_initial_mb16x16_cost, .Lfunc_end9-get_initial_mb16x16_cost
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI10_0:
	.quad	4607182418800017408     # double 1
.LCPI10_1:
	.quad	4647714815446351872     # double 512
	.text
	.globl	adjust_mb16x16_cost
	.p2align	4, 0x90
	.type	adjust_mb16x16_cost,@function
adjust_mb16x16_cost:                    # @adjust_mb16x16_cost
	.cfi_startproc
# BB#0:
	cvtsi2sdl	%edi, %xmm0
	movsd	%xmm0, mb16x16_cost(%rip)
	movq	mb16x16_cost_frame(%rip), %rcx
	movq	img(%rip), %rax
	movslq	12(%rax), %rdx
	movsd	%xmm0, (%rcx,%rdx,8)
	movsd	mb16x16_cost(%rip), %xmm0 # xmm0 = mem[0],zero
	movsd	.LCPI10_1(%rip), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB10_2
# BB#1:
	movsd	.LCPI10_0(%rip), %xmm1  # xmm1 = mem[0],zero
	movsd	%xmm1, lambda_mf_factor(%rip)
	retq
.LBB10_2:
	movq	15504(%rax), %rcx
	movslq	20(%rax), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	36(%rax), %rax
	movsd	(%rcx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	.LCPI10_1(%rip), %xmm1
	divsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB10_4
# BB#3:                                 # %call.sqrt
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 16
	callq	sqrt
	movapd	%xmm0, %xmm1
	addq	$8, %rsp
.LBB10_4:
	movsd	%xmm1, lambda_mf_factor(%rip)
	retq
.Lfunc_end10:
	.size	adjust_mb16x16_cost, .Lfunc_end10-adjust_mb16x16_cost
	.cfi_endproc

	.type	b8_mode_table,@object   # @b8_mode_table
	.section	.rodata,"a",@progbits
	.globl	b8_mode_table
	.p2align	4
b8_mode_table:
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	0                       # 0x0
	.size	b8_mode_table, 24

	.type	mb_mode_table,@object   # @mb_mode_table
	.globl	mb_mode_table
	.p2align	4
mb_mode_table:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	8                       # 0x8
	.long	10                      # 0xa
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	14                      # 0xe
	.size	mb_mode_table, 36

	.type	diffy,@object           # @diffy
	.comm	diffy,1024,16
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	.Lsubmacroblock_mode_decision.bmcost,@object # @submacroblock_mode_decision.bmcost
	.p2align	4
.Lsubmacroblock_mode_decision.bmcost:
	.long	2147483647              # 0x7fffffff
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	.Lsubmacroblock_mode_decision.bmcost, 20

	.type	submacroblock_mode_decision.fadjust,@object # @submacroblock_mode_decision.fadjust
	.local	submacroblock_mode_decision.fadjust
	.comm	submacroblock_mode_decision.fadjust,1024,16
	.type	submacroblock_mode_decision.fadjustCr,@object # @submacroblock_mode_decision.fadjustCr
	.local	submacroblock_mode_decision.fadjustCr
	.comm	submacroblock_mode_decision.fadjustCr,2048,16
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	qp_mbaff,@object        # @qp_mbaff
	.comm	qp_mbaff,16,16
	.type	delta_qp_mbaff,@object  # @delta_qp_mbaff
	.comm	delta_qp_mbaff,16,16
	.type	generic_RC,@object      # @generic_RC
	.comm	generic_RC,8,8
	.type	generic_RC_init,@object # @generic_RC_init
	.comm	generic_RC_init,8,8
	.type	generic_RC_best,@object # @generic_RC_best
	.comm	generic_RC_best,8,8
	.type	McostState,@object      # @McostState
	.comm	McostState,8,8
	.type	SearchState,@object     # @SearchState
	.comm	SearchState,8,8
	.type	fastme_ref_cost,@object # @fastme_ref_cost
	.comm	fastme_ref_cost,8,8
	.type	fastme_l0_cost,@object  # @fastme_l0_cost
	.comm	fastme_l0_cost,8,8
	.type	fastme_l1_cost,@object  # @fastme_l1_cost
	.comm	fastme_l1_cost,8,8
	.type	fastme_l0_cost_bipred,@object # @fastme_l0_cost_bipred
	.comm	fastme_l0_cost_bipred,8,8
	.type	fastme_l1_cost_bipred,@object # @fastme_l1_cost_bipred
	.comm	fastme_l1_cost_bipred,8,8
	.type	bipred_flag,@object     # @bipred_flag
	.comm	bipred_flag,4,4
	.type	fastme_best_cost,@object # @fastme_best_cost
	.comm	fastme_best_cost,8,8
	.type	pred_SAD,@object        # @pred_SAD
	.comm	pred_SAD,4,4
	.type	pred_MV_ref,@object     # @pred_MV_ref
	.comm	pred_MV_ref,8,4
	.type	pred_MV_uplayer,@object # @pred_MV_uplayer
	.comm	pred_MV_uplayer,8,4
	.type	UMHEX_blocktype,@object # @UMHEX_blocktype
	.comm	UMHEX_blocktype,4,4
	.type	predict_point,@object   # @predict_point
	.comm	predict_point,40,16
	.type	SAD_a,@object           # @SAD_a
	.comm	SAD_a,4,4
	.type	SAD_b,@object           # @SAD_b
	.comm	SAD_b,4,4
	.type	SAD_c,@object           # @SAD_c
	.comm	SAD_c,4,4
	.type	SAD_d,@object           # @SAD_d
	.comm	SAD_d,4,4
	.type	Threshold_DSR_MB,@object # @Threshold_DSR_MB
	.comm	Threshold_DSR_MB,32,16
	.type	Bsize,@object           # @Bsize
	.comm	Bsize,32,16
	.type	AlphaFourth_1,@object   # @AlphaFourth_1
	.comm	AlphaFourth_1,32,16
	.type	AlphaFourth_2,@object   # @AlphaFourth_2
	.comm	AlphaFourth_2,32,16
	.type	flag_intra,@object      # @flag_intra
	.comm	flag_intra,8,8
	.type	flag_intra_SAD,@object  # @flag_intra_SAD
	.comm	flag_intra_SAD,4,4
	.type	SymmetricalCrossSearchThreshold1,@object # @SymmetricalCrossSearchThreshold1
	.comm	SymmetricalCrossSearchThreshold1,2,2
	.type	SymmetricalCrossSearchThreshold2,@object # @SymmetricalCrossSearchThreshold2
	.comm	SymmetricalCrossSearchThreshold2,2,2
	.type	ConvergeThreshold,@object # @ConvergeThreshold
	.comm	ConvergeThreshold,2,2
	.type	SubPelThreshold1,@object # @SubPelThreshold1
	.comm	SubPelThreshold1,2,2
	.type	SubPelThreshold3,@object # @SubPelThreshold3
	.comm	SubPelThreshold3,2,2
	.type	smpUMHEX_SearchState,@object # @smpUMHEX_SearchState
	.comm	smpUMHEX_SearchState,8,8
	.type	smpUMHEX_l0_cost,@object # @smpUMHEX_l0_cost
	.comm	smpUMHEX_l0_cost,8,8
	.type	smpUMHEX_l1_cost,@object # @smpUMHEX_l1_cost
	.comm	smpUMHEX_l1_cost,8,8
	.type	smpUMHEX_flag_intra,@object # @smpUMHEX_flag_intra
	.comm	smpUMHEX_flag_intra,8,8
	.type	smpUMHEX_flag_intra_SAD,@object # @smpUMHEX_flag_intra_SAD
	.comm	smpUMHEX_flag_intra_SAD,4,4
	.type	smpUMHEX_pred_SAD_uplayer,@object # @smpUMHEX_pred_SAD_uplayer
	.comm	smpUMHEX_pred_SAD_uplayer,4,4
	.type	smpUMHEX_pred_MV_uplayer_X,@object # @smpUMHEX_pred_MV_uplayer_X
	.comm	smpUMHEX_pred_MV_uplayer_X,2,2
	.type	smpUMHEX_pred_MV_uplayer_Y,@object # @smpUMHEX_pred_MV_uplayer_Y
	.comm	smpUMHEX_pred_MV_uplayer_Y,2,2
	.type	temp_imgY,@object       # @temp_imgY
	.comm	temp_imgY,512,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
