	.text
	.file	"z33.bc"
	.globl	DbCreate
	.p2align	4, 0x90
	.type	DbCreate,@function
DbCreate:                               # @DbCreate
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	32(%rbx), %al
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB0_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_2:
	movb	$0, 42(%rbx)
	movq	$0, 48(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	DbCreate, .Lfunc_end0-DbCreate
	.cfi_endproc

	.globl	DbInsert
	.p2align	4, 0x90
	.type	DbInsert,@function
DbInsert:                               # @DbInsert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$552, %rsp              # imm = 0x228
.Lcfi8:
	.cfi_def_cfa_offset 608
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movq	%r8, %r13
	movq	%rcx, %rbp
	movq	%rdx, %r12
	movl	%esi, 20(%rsp)          # 4-byte Spill
	movq	%rdi, %r15
	movb	32(%r15), %al
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB1_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_2:
	cmpb	$0, (%rbp)
	jne	.LBB1_4
# BB#3:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_4:
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	cmpb	$0, (%rbx)
	jne	.LBB1_6
# BB#5:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_6:
	movl	632(%rsp), %ebx
	cmpb	$0, 42(%r15)
	je	.LBB1_8
# BB#7:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.5, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_8:
	testl	%ebx, %ebx
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	je	.LBB1_30
# BB#9:
	cmpb	$1, DbCheckTableInit(%rip)
	jne	.LBB1_11
# BB#10:                                # %._crit_edge
	movq	DbCheckTable(%rip), %r14
	jmp	.LBB1_14
.LBB1_11:
	movl	$864, %edi              # imm = 0x360
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB1_13
# BB#12:
	movq	no_fpos(%rip), %r8
	movl	$33, %edi
	movl	$1, %esi
	movl	$.L.str.55, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB1_13:                               # %dtab_new.exit
	movl	$107, (%r14)
	movq	%r14, %rdi
	addq	$4, %rdi
	xorl	%esi, %esi
	movl	$860, %edx              # imm = 0x35C
	callq	memset
	movq	%r14, DbCheckTable(%rip)
	movb	$1, DbCheckTableInit(%rip)
.LBB1_14:
	movb	(%rbp), %cl
	testb	%cl, %cl
	je	.LBB1_15
# BB#16:                                # %.lr.ph.i.preheader
	leaq	1(%rbp), %rdx
	movq	%r12, %rax
	.p2align	4, 0x90
.LBB1_17:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%cl, %ecx
	addq	%rcx, %rax
	movzbl	(%rdx), %ecx
	incq	%rdx
	testb	%cl, %cl
	jne	.LBB1_17
	jmp	.LBB1_18
.LBB1_15:
	movq	%r12, %rax
.LBB1_18:                               # %._crit_edge.i
	movslq	(%r14), %rcx
	xorl	%edx, %edx
	divq	%rcx
	movq	8(%r14,%rdx,8), %r14
	testq	%r14, %r14
	je	.LBB1_25
# BB#19:                                # %.preheader40.i
	movq	8(%r14), %rbx
	cmpq	%r14, %rbx
	jne	.LBB1_21
	jmp	.LBB1_25
	.p2align	4, 0x90
.LBB1_24:                               # %.backedge.i
                                        #   in Loop: Header=BB1_21 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r14, %rbx
	je	.LBB1_25
.LBB1_21:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_22 Depth 2
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB1_22:                               #   Parent Loop BB1_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	cmpb	$0, 32(%rbp)
	je	.LBB1_22
# BB#23:                                #   in Loop: Header=BB1_21 Depth=1
	cmpq	%r12, 48(%rbp)
	jne	.LBB1_24
# BB#26:                                #   in Loop: Header=BB1_21 Depth=1
	leaq	64(%rbp), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB1_24
# BB#27:                                # %dtab_retrieve.exit
	movzwl	34(%rbp), %ebx
	movq	%r12, %rdi
	callq	SymName
	movq	%rax, %r14
	cmpw	$0, %bx
	je	.LBB1_29
# BB#28:
	addq	$32, %rbp
	movq	%rbp, %rdi
	callq	EchoFilePos
	movq	%rax, %rbp
	movl	$33, %edi
	movl	$4, %esi
	movl	$.L.str.6, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	%r13, %r8
	movq	%r14, %r9
	pushq	%rbp
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB1_30
.LBB1_25:                               # %.loopexit83
	movl	$11, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r13, %rdx
	callq	MakeWord
	movq	%r12, 48(%rax)
	movl	$DbCheckTable, %esi
	movq	%rax, %rdi
	callq	dtab_insert
.LBB1_30:
	movzwl	608(%rsp), %r13d
	cmpq	$0, 48(%r15)
	jne	.LBB1_35
# BB#31:
	leaq	64(%r15), %rbp
	movq	%rbp, %rdi
	callq	strlen
	addq	$4, %rax
	cmpq	$512, %rax              # imm = 0x200
	jb	.LBB1_33
# BB#32:
	movq	no_fpos(%rip), %r8
	subq	$8, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	movl	$33, %edi
	movl	$2, %esi
	movl	$.L.str.8, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r9
	pushq	$.L.str.9
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset -16
.LBB1_33:
	leaq	32(%rsp), %r14
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	strcpy
	movq	%r14, %rdi
	callq	strlen
	movb	$0, 36(%rsp,%rax)
	movl	$2020174894, 32(%rsp,%rax) # imm = 0x78696C2E
	movl	$.L.str.10, %esi
	movq	%r14, %rdi
	callq	fopen
	movq	%rax, 48(%r15)
	testq	%rax, %rax
	jne	.LBB1_35
# BB#34:
	leaq	32(%r15), %r8
	leaq	32(%rsp), %r9
	movl	$33, %edi
	movl	$3, %esi
	movl	$.L.str.11, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB1_35:
	testw	%r13w, %r13w
	je	.LBB1_37
# BB#36:
	movzwl	%r13w, %edi
	callq	FileName
	leaq	32(%rsp), %rbp
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	strcpy
	movq	%rbp, %rdi
	callq	strlen
	movb	$0, 29(%rsp,%rax)
	jmp	.LBB1_38
.LBB1_37:
	movw	$46, 32(%rsp)
.LBB1_38:                               # %.outer.preheader
	movq	%r15, %r13
	xorl	%r14d, %r14d
	jmp	.LBB1_39
.LBB1_47:                               #   in Loop: Header=BB1_39 Depth=1
	movzbl	%al, %eax
	cmpl	%r14d, %eax
	cmovgel	%eax, %r14d
	.p2align	4, 0x90
.LBB1_39:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_41 Depth 2
	movq	8(%r13), %r13
	cmpq	%r15, %r13
	je	.LBB1_48
# BB#40:                                # %.preheader.preheader
                                        #   in Loop: Header=BB1_39 Depth=1
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB1_41:                               # %.preheader
                                        #   Parent Loop BB1_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB1_41
# BB#42:                                # %.preheader
                                        #   in Loop: Header=BB1_39 Depth=1
	cmpb	$-116, %al
	je	.LBB1_45
# BB#43:                                # %.preheader
                                        #   in Loop: Header=BB1_39 Depth=1
	cmpb	$17, %al
	je	.LBB1_45
# BB#44:                                #   in Loop: Header=BB1_39 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.14, %r9d
	xorl	%eax, %eax
	callq	Error
	movb	32(%rbp), %al
.LBB1_45:                               # %.loopexit
                                        #   in Loop: Header=BB1_39 Depth=1
	cmpb	$-116, %al
	jne	.LBB1_39
# BB#46:                                #   in Loop: Header=BB1_39 Depth=1
	cmpq	%r12, 64(%rbp)
	movb	33(%r13), %al
	jne	.LBB1_47
	jmp	.LBB1_60
.LBB1_48:                               # %.thread
	cmpq	$0, 88(%r12)
	jne	.LBB1_50
# BB#49:
	movq	%r12, %rdi
	callq	CrossInit
.LBB1_50:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_51
# BB#52:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_53
.LBB1_51:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_53:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB1_56
# BB#54:
	testq	%rax, %rax
	je	.LBB1_56
# BB#55:
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_56:
	movq	%rax, zz_res(%rip)
	movq	88(%r12), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_59
# BB#57:
	testq	%rcx, %rcx
	je	.LBB1_59
# BB#58:
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB1_59:
	movq	(%r15), %r13
	incl	%r14d
	movb	%r14b, 33(%r13)
	movb	$0, 34(%r13)
	movl	%r14d, %eax
.LBB1_60:                               # %._crit_edge105
	movzbl	%al, %r14d
	movl	20(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	je	.LBB1_62
# BB#61:
	orb	$24, 42(%r12)
	movb	$1, 34(%r13)
.LBB1_62:
	testl	%eax, %eax
	movq	48(%r15), %r12
	movl	$.L.str.17, %eax
	movl	$.L.str.12, %ebp
	cmovneq	%rax, %rbp
	movl	DbInsert.extra_seq(%rip), %edi
	incl	%edi
	movl	%edi, DbInsert.extra_seq(%rip)
	callq	StringFiveInt
	movq	%rax, %rbx
	leaq	32(%rsp), %r10
	movl	$.L.str.16, %esi
	movl	$0, %eax
	movq	%r12, %rdi
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	%rbp, %rdx
	movl	%r14d, %ecx
	movq	24(%rsp), %r9           # 8-byte Reload
	pushq	%r10
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	movl	632(%rsp), %ebp
	pushq	%rbp
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	632(%rsp)
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$584, %rsp              # imm = 0x248
.Lcfi25:
	.cfi_adjust_cfa_offset -32
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_29:
	subq	$8, %rsp
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	movl	$33, %edi
	movl	$5, %esi
	movl	$.L.str.7, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r13, %r8
	movq	%r14, %r9
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB1_30
.Lfunc_end1:
	.size	DbInsert, .Lfunc_end1-DbInsert
	.cfi_endproc

	.p2align	4, 0x90
	.type	dtab_insert,@function
dtab_insert:                            # @dtab_insert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 80
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	(%rsi), %r13
	movl	4(%r13), %eax
	movslq	(%r13), %rbp
	leal	-1(%rbp), %ecx
	cmpl	%ecx, %eax
	jne	.LBB2_19
# BB#1:
	leal	(%rbp,%rbp), %r12d
	movq	%rbp, %rdi
	shlq	$4, %rdi
	orq	$8, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB2_3
# BB#2:
	movq	no_fpos(%rip), %r8
	movl	$33, %edi
	movl	$1, %esi
	movl	$.L.str.55, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB2_3:
	movl	%r12d, (%rbx)
	movl	$0, 4(%rbx)
	testl	%ebp, %ebp
	jle	.LBB2_5
# BB#4:                                 # %.lr.ph.i.i
	movq	%rbx, %rdi
	addq	$8, %rdi
	decl	%r12d
	leaq	8(,%r12,8), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB2_5:                                # %dtab_new.exit.i
	movq	%rbx, 16(%rsp)
	movl	(%r13), %eax
	testl	%eax, %eax
	jle	.LBB2_18
# BB#6:                                 # %.lr.ph72.preheader
	leaq	16(%rsp), %r12
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph72
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_12 Depth 2
                                        #       Child Loop BB2_13 Depth 3
	movq	8(%r13,%rbp,8), %rbx
	testq	%rbx, %rbx
	je	.LBB2_16
# BB#8:                                 #   in Loop: Header=BB2_7 Depth=1
	cmpb	$17, 32(%rbx)
	je	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_7 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.57, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_10:                               # %.preheader58
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	8(%rbx), %r14
	cmpq	%rbx, %r14
	jne	.LBB2_12
	jmp	.LBB2_15
	.p2align	4, 0x90
.LBB2_14:                               #   in Loop: Header=BB2_12 Depth=2
	movq	%r12, %rsi
	callq	dtab_insert
	movq	8(%r14), %r14
	cmpq	%rbx, %r14
	je	.LBB2_15
.LBB2_12:                               # %.preheader
                                        #   Parent Loop BB2_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_13 Depth 3
	movq	%r14, %rdi
	.p2align	4, 0x90
.LBB2_13:                               #   Parent Loop BB2_7 Depth=1
                                        #     Parent Loop BB2_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB2_13
	jmp	.LBB2_14
	.p2align	4, 0x90
.LBB2_15:                               # %._crit_edge70
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	%rbx, %rdi
	callq	DisposeObject
	movl	(%r13), %eax
.LBB2_16:                               #   in Loop: Header=BB2_7 Depth=1
	incq	%rbp
	movslq	%eax, %rcx
	cmpq	%rcx, %rbp
	jl	.LBB2_7
# BB#17:                                # %dtab_rehash.exit.loopexit
	movq	16(%rsp), %rbx
.LBB2_18:                               # %dtab_rehash.exit
	movq	%r13, %rdi
	callq	free
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rbx, (%rax)
	movl	4(%rbx), %eax
	movq	%rbx, %r13
.LBB2_19:
	incl	%eax
	movl	%eax, 4(%r13)
	movq	48(%r15), %rax
	movb	64(%r15), %cl
	testb	%cl, %cl
	je	.LBB2_22
# BB#20:                                # %.lr.ph65.preheader
	leaq	65(%r15), %rdx
	.p2align	4, 0x90
.LBB2_21:                               # %.lr.ph65
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%cl, %ecx
	addq	%rcx, %rax
	movzbl	(%rdx), %ecx
	incq	%rdx
	testb	%cl, %cl
	jne	.LBB2_21
.LBB2_22:                               # %._crit_edge66
	movslq	(%r13), %rcx
	xorl	%edx, %edx
	divq	%rcx
	movq	%rdx, %r12
	cmpq	$0, 8(%r13,%r12,8)
	jne	.LBB2_27
# BB#23:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_24
# BB#25:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_26
.LBB2_24:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_26:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rdx), %rcx
	movq	%rax, 8(%rcx,%r12,8)
	movq	(%rdx), %r13
.LBB2_27:
	movq	8(%r13,%r12,8), %rbp
	movq	8(%rbp), %rbx
	cmpq	%rbp, %rbx
	je	.LBB2_33
# BB#28:                                # %.lr.ph.preheader
	leaq	64(%r15), %r13
	.p2align	4, 0x90
.LBB2_29:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_30 Depth 2
	leaq	16(%rbx), %rax
	.p2align	4, 0x90
.LBB2_30:                               #   Parent Loop BB2_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rsi
	leaq	16(%rsi), %rax
	cmpb	$0, 32(%rsi)
	je	.LBB2_30
# BB#31:                                #   in Loop: Header=BB2_29 Depth=1
	movq	48(%r15), %rax
	cmpq	48(%rsi), %rax
	jne	.LBB2_32
# BB#35:                                #   in Loop: Header=BB2_29 Depth=1
	addq	$64, %rsi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB2_32
# BB#36:                                #   in Loop: Header=BB2_29 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.56, %r9d
	xorl	%eax, %eax
	callq	Error
	.p2align	4, 0x90
.LBB2_32:                               # %.backedge
                                        #   in Loop: Header=BB2_29 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%rbp, %rbx
	jne	.LBB2_29
.LBB2_33:                               # %._crit_edge
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_34
# BB#37:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_38
.LBB2_34:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_38:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx), %rcx
	movq	8(%rcx,%r12,8), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_41
# BB#39:
	testq	%rcx, %rcx
	je	.LBB2_41
# BB#40:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_41:
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB2_44
# BB#42:
	testq	%rax, %rax
	je	.LBB2_44
# BB#43:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_44:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	dtab_insert, .Lfunc_end2-dtab_insert
	.cfi_endproc

	.globl	DbConvert
	.p2align	4, 0x90
	.type	DbConvert,@function
DbConvert:                              # @DbConvert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
	subq	$1048, %rsp             # imm = 0x418
.Lcfi48:
	.cfi_def_cfa_offset 1104
.Lcfi49:
	.cfi_offset %rbx, -56
.Lcfi50:
	.cfi_offset %r12, -48
.Lcfi51:
	.cfi_offset %r13, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movl	%esi, %r12d
	movq	%rdi, %rbx
	cmpb	$0, 42(%rbx)
	je	.LBB3_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.18, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_2:
	leaq	64(%rbx), %rbp
	leaq	528(%rsp), %r15
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	strcpy
	movq	%r15, %rdi
	callq	strlen
	movl	$6908974, 528(%rsp,%rax) # imm = 0x696C2E
	movq	%rsp, %r15
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	strcpy
	movq	%r15, %rdi
	callq	strlen
	movb	$0, 4(%rsp,%rax)
	movl	$2020174894, (%rsp,%rax) # imm = 0x78696C2E
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_21
# BB#3:
	movl	$.L.str.20, %esi
	movl	$.L.str.21, %edx
	movl	$.L.str.22, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	8(%rbx), %r14
	cmpq	%rbx, %r14
	je	.LBB3_20
# BB#4:                                 # %.preheader33.lr.ph
	testl	%r12d, %r12d
	je	.LBB3_5
	.p2align	4, 0x90
.LBB3_13:                               # %.preheader33
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_14 Depth 2
	movq	%r14, %r13
	.p2align	4, 0x90
.LBB3_14:                               #   Parent Loop BB3_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %r13
	movzbl	32(%r13), %eax
	testb	%al, %al
	je	.LBB3_14
# BB#15:                                #   in Loop: Header=BB3_13 Depth=1
	cmpb	$-116, %al
	je	.LBB3_18
# BB#16:                                #   in Loop: Header=BB3_13 Depth=1
	cmpb	$17, %al
	je	.LBB3_18
# BB#17:                                #   in Loop: Header=BB3_13 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.23, %r9d
	xorl	%eax, %eax
	callq	Error
	movb	32(%r13), %al
.LBB3_18:                               # %.loopexit
                                        #   in Loop: Header=BB3_13 Depth=1
	cmpb	$-116, %al
	jne	.LBB3_19
# BB#37:                                #   in Loop: Header=BB3_13 Depth=1
	movq	48(%rbx), %rbp
	cmpb	$0, 34(%r14)
	movl	$.L.str.26, %r12d
	movl	$.L.str.25, %eax
	cmovneq	%rax, %r12
	movzbl	33(%r14), %r15d
	movq	64(%r13), %rdi
	movl	$.L.str.27, %esi
	callq	FullSymName
	movq	%rax, %r8
	movl	$.L.str.24, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r12, %rdx
	movl	%r15d, %ecx
	callq	fprintf
.LBB3_19:                               # %.backedge
                                        #   in Loop: Header=BB3_13 Depth=1
	movq	8(%r14), %r14
	cmpq	%rbx, %r14
	jne	.LBB3_13
	jmp	.LBB3_20
	.p2align	4, 0x90
.LBB3_5:                                # %.preheader33.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_6 Depth 2
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB3_6:                                #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB3_6
# BB#7:                                 #   in Loop: Header=BB3_5 Depth=1
	cmpb	$-116, %al
	je	.LBB3_10
# BB#8:                                 #   in Loop: Header=BB3_5 Depth=1
	cmpb	$17, %al
	je	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_5 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.23, %r9d
	xorl	%eax, %eax
	callq	Error
	movb	32(%rbp), %al
.LBB3_10:                               # %.loopexit.us
                                        #   in Loop: Header=BB3_5 Depth=1
	cmpb	$-116, %al
	jne	.LBB3_12
# BB#11:                                #   in Loop: Header=BB3_5 Depth=1
	movq	48(%rbx), %r13
	cmpb	$0, 34(%r14)
	movl	$.L.str.26, %r12d
	movl	$.L.str.25, %eax
	cmovneq	%rax, %r12
	movzbl	33(%r14), %r15d
	movq	64(%rbp), %rdi
	callq	SymName
	movq	%rax, %r8
	movl	$.L.str.24, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%r15d, %ecx
	callq	fprintf
.LBB3_12:                               # %.backedge.us
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	8(%r14), %r14
	cmpq	%rbx, %r14
	jne	.LBB3_5
.LBB3_20:                               # %._crit_edge47
	movq	48(%rbx), %rdi
	callq	fclose
	movq	%rsp, %rdi
	leaq	528(%rsp), %rsi
	callq	SortFile
	jmp	.LBB3_22
.LBB3_21:
	leaq	528(%rsp), %rdi
	callq	remove
.LBB3_22:
	movq	%rsp, %rdi
	callq	remove
	movq	%rbx, xx_hold(%rip)
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB3_29
	.p2align	4, 0x90
.LBB3_23:                               # %.lr.ph42
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_25
# BB#24:                                #   in Loop: Header=BB3_23 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB3_25:                               #   in Loop: Header=BB3_23 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_27
# BB#26:                                #   in Loop: Header=BB3_23 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_27:                               #   in Loop: Header=BB3_23 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbx
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB3_23
# BB#28:                                # %..preheader_crit_edge
	movl	%ecx, zz_size(%rip)
.LBB3_29:                               # %.preheader
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB3_36
	.p2align	4, 0x90
.LBB3_30:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_32
# BB#31:                                #   in Loop: Header=BB3_30 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB3_32:                               #   in Loop: Header=BB3_30 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_34
# BB#33:                                #   in Loop: Header=BB3_30 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_34:                               #   in Loop: Header=BB3_30 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbx
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB3_30
# BB#35:                                # %._crit_edge
	movl	%ecx, zz_size(%rip)
.LBB3_36:
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	addq	$1048, %rsp             # imm = 0x418
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	DbConvert, .Lfunc_end3-DbConvert
	.cfi_endproc

	.globl	DbClose
	.p2align	4, 0x90
	.type	DbClose,@function
DbClose:                                # @DbClose
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 16
.Lcfi56:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB4_4
# BB#1:
	cmpb	$0, 43(%rbx)
	jne	.LBB4_4
# BB#2:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_4
# BB#3:
	callq	fclose
	movq	$0, 48(%rbx)
.LBB4_4:
	popq	%rbx
	retq
.Lfunc_end4:
	.size	DbClose, .Lfunc_end4-DbClose
	.cfi_endproc

	.globl	DbLoad
	.p2align	4, 0x90
	.type	DbLoad,@function
DbLoad:                                 # @DbLoad
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	subq	$1112, %rsp             # imm = 0x458
.Lcfi63:
	.cfi_def_cfa_offset 1168
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movl	%r8d, 60(%rsp)          # 4-byte Spill
	movq	%rcx, %r12
	movl	%edx, %ebp
	movl	%esi, %r14d
	movq	%rdi, %rax
	leaq	64(%rax), %r15
	addq	$32, %rax
	movl	$.L.str.19, %esi
	movl	$4, %ecx
	movq	%r15, %rdi
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, %rdx
	movl	%r14d, %r8d
	callq	DefineFile
	movzwl	%ax, %edi
	xorl	%edx, %edx
	movl	%edi, 8(%rsp)           # 4-byte Spill
	movl	%ebp, %esi
	callq	OpenFile
	movq	%rax, %r13
	testq	%r13, %r13
	movq	%r12, 48(%rsp)          # 8-byte Spill
	je	.LBB5_4
# BB#1:
	leaq	80(%rsp), %rdi
	movl	$512, %esi              # imm = 0x200
	movq	%r13, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:
	leaq	83(%rsp), %rdi
	movl	$.L.str.21, %esi
	callq	StringBeginsWith
	testl	%eax, %eax
	jne	.LBB5_20
.LBB5_3:
	movl	8(%rsp), %edi           # 4-byte Reload
	callq	FileName
	movq	%rax, %rdi
	callq	remove
.LBB5_4:
	testl	%ebp, %ebp
	je	.LBB5_5
# BB#6:
	movl	$.L.str.28, %esi
	movl	$3, %ecx
	movq	%r15, %rdi
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%r14d, %r8d
	callq	DefineFile
	movzwl	%ax, %edi
	xorl	%esi, %esi
	movl	$3, %edx
	movl	$1, %ecx
	xorl	%r8d, %r8d
	movl	%edi, 12(%rsp)          # 4-byte Spill
	callq	LexPush
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB5_7
	.p2align	4, 0x90
.LBB5_60:                               #   in Loop: Header=BB5_7 Depth=1
	movq	80(%r14), %rdx
	xorl	%esi, %esi
	movl	$.L.str.17, %r9d
	movq	%rax, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%r15, %rcx
	movq	%r12, %r8
	pushq	$1
.Lcfi70:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi72:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi73:
	.cfi_adjust_cfa_offset 8
	callq	DbInsert
	addq	$32, %rsp
.Lcfi74:
	.cfi_adjust_cfa_offset -32
	movq	%r14, %rdi
	callq	DisposeObject
	callq	LexNextTokenPos
	movq	%rax, %rbx
	movq	48(%rsp), %r12          # 8-byte Reload
.LBB5_7:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_33 Depth 2
                                        #       Child Loop BB5_34 Depth 3
                                        #     Child Loop BB5_41 Depth 2
                                        #       Child Loop BB5_42 Depth 3
                                        #     Child Loop BB5_47 Depth 2
	callq	LexGetToken
	movq	%rax, %rbp
	movq	%rbp, 72(%rsp)
	movb	32(%rbp), %al
	cmpb	$102, %al
	jne	.LBB5_8
# BB#27:                                #   in Loop: Header=BB5_7 Depth=1
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movl	36(%rbp), %r13d
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	%eax, %r13d
	movq	StartSym(%rip), %rsi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	72(%rsp), %rdi
	callq	Parse
	movq	%rax, %r14
	cmpq	$0, 72(%rsp)
	jne	.LBB5_29
# BB#28:                                #   in Loop: Header=BB5_7 Depth=1
	cmpb	$2, 32(%r14)
	je	.LBB5_30
.LBB5_29:                               #   in Loop: Header=BB5_7 Depth=1
	leaq	32(%r14), %rbp
	movl	12(%rsp), %edi          # 4-byte Reload
	callq	FileName
	movq	%rax, %rbx
	movl	$33, %edi
	movl	$6, %esi
	movl	$.L.str.29, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r8
	movq	%rbx, %r9
	callq	Error
.LBB5_30:                               #   in Loop: Header=BB5_7 Depth=1
	testq	%r12, %r12
	je	.LBB5_118
# BB#31:                                # %.critedge262.preheader
                                        #   in Loop: Header=BB5_7 Depth=1
	movq	8(%r12), %rax
	cmpq	%r12, %rax
	jne	.LBB5_33
	jmp	.LBB5_38
	.p2align	4, 0x90
.LBB5_37:                               # %.critedge262.backedge
                                        #   in Loop: Header=BB5_33 Depth=2
	movq	8(%rax), %rax
	cmpq	%r12, %rax
	je	.LBB5_38
.LBB5_33:                               # %.preheader292
                                        #   Parent Loop BB5_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_34 Depth 3
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB5_34:                               #   Parent Loop BB5_7 Depth=1
                                        #     Parent Loop BB5_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB5_34
# BB#35:                                #   in Loop: Header=BB5_33 Depth=2
	cmpb	$2, %dl
	jne	.LBB5_37
# BB#36:                                #   in Loop: Header=BB5_33 Depth=2
	movq	80(%rcx), %rcx
	cmpq	80(%r14), %rcx
	jne	.LBB5_37
	jmp	.LBB5_39
	.p2align	4, 0x90
.LBB5_38:                               # %.thread277
                                        #   in Loop: Header=BB5_7 Depth=1
	leaq	32(%r14), %rbx
	movq	80(%r14), %rdi
	callq	SymName
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi75:
	.cfi_adjust_cfa_offset 8
	movl	$33, %edi
	movl	$7, %esi
	movl	$.L.str.31, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	pushq	$.L.str.32
.Lcfi76:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi77:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB5_39
	.p2align	4, 0x90
.LBB5_118:                              #   in Loop: Header=BB5_7 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.30, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB5_39:                               # %.preheader294
                                        #   in Loop: Header=BB5_7 Depth=1
	movq	8(%r14), %rax
	cmpq	%r14, %rax
	jne	.LBB5_41
	jmp	.LBB5_49
	.p2align	4, 0x90
.LBB5_46:                               # %.backedge
                                        #   in Loop: Header=BB5_41 Depth=2
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	je	.LBB5_49
.LBB5_41:                               # %.preheader290
                                        #   Parent Loop BB5_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_42 Depth 3
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB5_42:                               #   Parent Loop BB5_7 Depth=1
                                        #     Parent Loop BB5_41 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB5_42
# BB#43:                                #   in Loop: Header=BB5_41 Depth=2
	cmpb	$10, %dl
	jne	.LBB5_46
# BB#44:                                #   in Loop: Header=BB5_41 Depth=2
	movq	80(%rcx), %rdx
	movzwl	41(%rdx), %edx
	testb	$1, %dl
	je	.LBB5_46
# BB#45:                                #   in Loop: Header=BB5_41 Depth=2
	movq	8(%rcx), %rdi
	cmpq	%rcx, %rdi
	je	.LBB5_46
	.p2align	4, 0x90
.LBB5_47:                               # %.preheader293
                                        #   Parent Loop BB5_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB5_47
# BB#48:                                #   in Loop: Header=BB5_7 Depth=1
	testq	%rdi, %rdi
	jne	.LBB5_50
	.p2align	4, 0x90
.LBB5_49:                               # %.thread278
                                        #   in Loop: Header=BB5_7 Depth=1
	leaq	32(%r14), %rbx
	movq	80(%r14), %rdi
	callq	SymName
	movq	%rax, %rbp
	movl	$33, %edi
	movl	$8, %esi
	movl	$.L.str.33, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	callq	Error
	xorl	%edi, %edi
.LBB5_50:                               #   in Loop: Header=BB5_7 Depth=1
	movl	$1, %esi
	callq	ReplaceWithTidy
	movq	%rax, %r15
	movb	32(%r15), %al
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB5_52
# BB#51:                                #   in Loop: Header=BB5_7 Depth=1
	leaq	32(%r14), %r8
	movl	$33, %edi
	movl	$9, %esi
	movl	$.L.str.34, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB5_52:                               #   in Loop: Header=BB5_7 Depth=1
	cmpb	$0, 64(%r15)
	jne	.LBB5_54
# BB#53:                                #   in Loop: Header=BB5_7 Depth=1
	movq	%r14, %r8
	addq	$32, %r8
	movl	$33, %edi
	movl	$10, %esi
	movl	$.L.str.35, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB5_54:                               #   in Loop: Header=BB5_7 Depth=1
	leaq	32(%r15), %r12
	addq	$64, %r15
	movq	24(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	jne	.LBB5_60
# BB#55:                                #   in Loop: Header=BB5_7 Depth=1
	movl	12(%rsp), %edi          # 4-byte Reload
	callq	FileName
	leaq	80(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	strcpy
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rbp
	movl	$4294967293, %eax       # imm = 0xFFFFFFFD
	addq	%rax, %rbp
	testl	%ebp, %ebp
	jg	.LBB5_57
# BB#56:                                #   in Loop: Header=BB5_7 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.36, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB5_57:                               #   in Loop: Header=BB5_7 Depth=1
	movslq	%ebp, %rax
	movb	$0, 80(%rsp,%rax)
	movl	$11, %edi
	movq	%rbx, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movb	32(%rax), %cl
	addb	$-11, %cl
	cmpb	$2, %cl
	jb	.LBB5_59
# BB#58:                                #   in Loop: Header=BB5_7 Depth=1
	movq	no_fpos(%rip), %r8
	movq	%rax, %rbx
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%rbx, %rax
.LBB5_59:                               # %DbCreate.exit
                                        #   in Loop: Header=BB5_7 Depth=1
	movb	$0, 42(%rax)
	movq	$0, 48(%rax)
	jmp	.LBB5_60
.LBB5_8:
	cmpb	$105, %al
	je	.LBB5_10
# BB#9:
	addq	$32, %rbp
	movl	$33, %edi
	movl	$11, %esi
	movl	$.L.str.37, %edx
	movl	$1, %ecx
	movl	$.L.str.38, %r9d
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
.LBB5_10:                               # %.loopexit295
	callq	LexPop
	movq	24(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	jne	.LBB5_16
# BB#11:
	movl	12(%rsp), %edi          # 4-byte Reload
	callq	FileName
	leaq	80(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	strcpy
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rbp
	movl	$4294967293, %eax       # imm = 0xFFFFFFFD
	addq	%rax, %rbp
	testl	%ebp, %ebp
	jg	.LBB5_13
# BB#12:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.39, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB5_13:
	movslq	%ebp, %rax
	movb	$0, 80(%rsp,%rax)
	leaq	80(%rsp), %rsi
	movl	$11, %edi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movb	32(%rax), %cl
	addb	$-11, %cl
	cmpb	$2, %cl
	jb	.LBB5_15
# BB#14:
	movq	no_fpos(%rip), %r8
	movq	%rax, %rbx
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%rbx, %rax
.LBB5_15:                               # %DbCreate.exit275
	movb	$0, 42(%rax)
	movq	$0, 48(%rax)
.LBB5_16:
	xorl	%esi, %esi
	movq	%rax, %rbx
	movq	%rax, %rdi
	callq	DbConvert
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	8(%rsp), %edi           # 4-byte Reload
	callq	OpenFile
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB5_19
# BB#17:
	leaq	80(%rsp), %rdi
	movl	$512, %esi              # imm = 0x200
	movq	%r13, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB5_19
# BB#18:
	leaq	83(%rsp), %rdi
	movl	$.L.str.21, %esi
	callq	StringBeginsWith
	testl	%eax, %eax
	jne	.LBB5_20
.LBB5_19:
	addq	$32, %rbx
	movl	8(%rsp), %edi           # 4-byte Reload
	callq	FileName
	movq	%rax, %rbp
	movl	$33, %edi
	movl	$12, %esi
	movl	$.L.str.40, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	callq	Error
	jmp	.LBB5_20
.LBB5_5:
	xorl	%r13d, %r13d
.LBB5_20:                               # %.thread
	movl	8(%rsp), %edi           # 4-byte Reload
	callq	FileName
	leaq	80(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	strcpy
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rbp
	movl	$4294967293, %eax       # imm = 0xFFFFFFFD
	addq	%rax, %rbp
	testl	%ebp, %ebp
	jg	.LBB5_22
# BB#21:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.41, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB5_22:
	movslq	%ebp, %rax
	movb	$0, 80(%rsp,%rax)
	leaq	80(%rsp), %rsi
	movl	$11, %edi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	MakeWord
	movq	%rax, %rbx
	movb	$1, 42(%rbx)
	movl	60(%rsp), %eax          # 4-byte Reload
	movb	%al, 43(%rbx)
	testq	%r12, %r12
	movl	$4294967293, %ebp       # imm = 0xFFFFFFFD
	je	.LBB5_67
# BB#23:
	cmpb	$17, 32(%r12)
	je	.LBB5_25
# BB#24:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.42, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB5_25:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_26
# BB#61:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_62
.LBB5_26:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_62:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB5_65
# BB#63:
	testq	%rax, %rax
	je	.LBB5_65
# BB#64:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_65:
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB5_67
# BB#66:
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_67:
	testq	%r13, %r13
	je	.LBB5_68
# BB#69:
	leaq	80(%rsp), %rdi
	movl	$512, %esi              # imm = 0x200
	movq	%r13, %rdx
	callq	fgets
	testq	%rax, %rax
	sete	%al
	setne	%cl
	xorl	%edx, %edx
	cmpb	$48, 80(%rsp)
	jne	.LBB5_70
# BB#71:
	movq	%rbx, %r14
	testb	%cl, %cl
	je	.LBB5_112
# BB#72:                                # %.lr.ph319
	movl	$1, %ebx
	leaq	592(%rsp), %r15
	xorl	%edx, %edx
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
.LBB5_73:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_75 Depth 2
                                        #     Child Loop BB5_87 Depth 2
                                        #       Child Loop BB5_89 Depth 3
                                        #     Child Loop BB5_79 Depth 2
                                        #     Child Loop BB5_85 Depth 2
	cmpb	$48, 81(%rsp)
	jne	.LBB5_112
# BB#74:                                #   in Loop: Header=BB5_73 Depth=1
	incl	%ebx
	movq	%r13, %rdi
	callq	ftell
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$.L.str.43, %esi
	leaq	80(%rsp), %r13
	movq	%r13, %rdi
	callq	StringBeginsWith
	movl	%eax, 12(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	movl	$.L.str.45, %esi
	movl	$.L.str.44, %eax
	cmovneq	%rax, %rsi
	xorl	%eax, %eax
	movq	%r13, %rdi
	leaq	68(%rsp), %rdx
	callq	sscanf
	movabsq	$42949672960, %rax      # imm = 0xA00000000
	movl	$9, %r13d
	jmp	.LBB5_75
	.p2align	4, 0x90
.LBB5_76:                               #   in Loop: Header=BB5_75 Depth=2
	incq	%r13
	leaq	3(%rax,%rbp), %rax
.LBB5_75:                               #   Parent Loop BB5_73 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	80(%rsp,%r13), %ecx
	orb	$32, %cl
	cmpb	$32, %cl
	jne	.LBB5_76
# BB#77:                                # %.critedge
                                        #   in Loop: Header=BB5_73 Depth=1
	testq	%r12, %r12
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	je	.LBB5_78
# BB#86:                                #   in Loop: Header=BB5_73 Depth=1
	sarq	$32, %rax
	leaq	80(%rsp,%rax), %rdi
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	callq	sscanf
	movq	%r12, %rbp
	movq	40(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB5_87:                               #   Parent Loop BB5_73 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_89 Depth 3
	movq	8(%rbp), %rbp
	cmpq	%r12, %rbp
	je	.LBB5_119
# BB#88:                                # %.preheader.preheader
                                        #   in Loop: Header=BB5_87 Depth=2
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB5_89:                               # %.preheader
                                        #   Parent Loop BB5_73 Depth=1
                                        #     Parent Loop BB5_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB5_89
# BB#90:                                # %.preheader
                                        #   in Loop: Header=BB5_87 Depth=2
	cmpb	$2, %al
	je	.LBB5_92
# BB#91:                                #   in Loop: Header=BB5_87 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.47, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB5_92:                               # %.loopexit
                                        #   in Loop: Header=BB5_87 Depth=2
	movq	80(%rbx), %rdi
	callq	SymName
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB5_87
# BB#93:                                #   in Loop: Header=BB5_73 Depth=1
	movq	80(%rbx), %r14
	testq	%r14, %r14
	jne	.LBB5_95
	jmp	.LBB5_119
	.p2align	4, 0x90
.LBB5_78:                               #   in Loop: Header=BB5_73 Depth=1
	xorl	%ebx, %ebx
	movq	StartSym(%rip), %r14
	jmp	.LBB5_79
	.p2align	4, 0x90
.LBB5_83:                               #   in Loop: Header=BB5_79 Depth=2
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	PushScope
	incl	%ebx
	shlq	$32, %r13
	leaq	3(%r13,%rbp), %rax
	sarq	$32, %rax
	leaq	80(%rsp,%rax), %rdi
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	callq	sscanf
	movq	%r15, %rdi
	callq	strlen
	movq	%r15, %rdi
	movl	%eax, %esi
	callq	SearchSym
	movq	%rax, %r14
	movq	%r15, %rdi
	callq	strlen
	leaq	1(%r12,%rax), %r13
.LBB5_79:                               #   Parent Loop BB5_73 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%r13d, %r12
	movzbl	80(%rsp,%r12), %eax
	cmpb	$10, %al
	je	.LBB5_81
# BB#80:                                #   in Loop: Header=BB5_79 Depth=2
	testb	%al, %al
	jne	.LBB5_83
.LBB5_81:                               # %.critedge8.preheader
                                        #   in Loop: Header=BB5_73 Depth=1
	testl	%ebx, %ebx
	jle	.LBB5_82
# BB#84:                                # %.critedge8.preheader379
                                        #   in Loop: Header=BB5_73 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_85:                               # %.critedge8
                                        #   Parent Loop BB5_73 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	PopScope
	incl	%ebp
	cmpl	%ebp, %ebx
	jne	.LBB5_85
.LBB5_82:                               #   in Loop: Header=BB5_73 Depth=1
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	40(%rsp), %r13          # 8-byte Reload
	testq	%r14, %r14
	je	.LBB5_119
.LBB5_95:                               # %.loopexit288
                                        #   in Loop: Header=BB5_73 Depth=1
	cmpq	StartSym(%rip), %r14
	je	.LBB5_119
# BB#96:                                #   in Loop: Header=BB5_73 Depth=1
	cmpq	$0, 88(%r14)
	jne	.LBB5_98
# BB#97:                                #   in Loop: Header=BB5_73 Depth=1
	movq	%r14, %rdi
	callq	CrossInit
.LBB5_98:                               #   in Loop: Header=BB5_73 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	movq	16(%rsp), %rbx          # 8-byte Reload
	je	.LBB5_99
# BB#100:                               #   in Loop: Header=BB5_73 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_101
.LBB5_99:                               #   in Loop: Header=BB5_73 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_101:                              #   in Loop: Header=BB5_73 Depth=1
	movl	$4294967293, %ebp       # imm = 0xFFFFFFFD
	movl	12(%rsp), %edi          # 4-byte Reload
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB5_104
# BB#102:                               #   in Loop: Header=BB5_73 Depth=1
	testq	%rax, %rax
	je	.LBB5_104
# BB#103:                               #   in Loop: Header=BB5_73 Depth=1
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_104:                              #   in Loop: Header=BB5_73 Depth=1
	movq	%rax, zz_res(%rip)
	movq	88(%r14), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB5_107
# BB#105:                               #   in Loop: Header=BB5_73 Depth=1
	testq	%rcx, %rcx
	je	.LBB5_107
# BB#106:                               #   in Loop: Header=BB5_73 Depth=1
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB5_107:                              #   in Loop: Header=BB5_73 Depth=1
	movq	(%rbx), %rax
	movb	68(%rsp), %cl
	movb	%cl, 33(%rax)
	movb	%dil, 34(%rax)
	testl	%edi, %edi
	je	.LBB5_109
# BB#108:                               #   in Loop: Header=BB5_73 Depth=1
	orb	$24, 42(%r14)
.LBB5_109:                              #   in Loop: Header=BB5_73 Depth=1
	movq	%rbx, %r14
	movl	$512, %esi              # imm = 0x200
	leaq	80(%rsp), %rdi
	movq	%r13, %rdx
	callq	fgets
	testq	%rax, %rax
	sete	%al
	setne	%cl
	cmpb	$48, 80(%rsp)
	jne	.LBB5_110
# BB#111:                               #   in Loop: Header=BB5_73 Depth=1
	testb	%cl, %cl
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	jne	.LBB5_73
	jmp	.LBB5_112
.LBB5_68:
	movq	$0, 48(%rbx)
	jmp	.LBB5_117
.LBB5_119:                              # %.thread281
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rbx
	addq	$32, %rbx
	movl	8(%rsp), %edi           # 4-byte Reload
	callq	FileName
	movq	%rax, %r9
	subq	$8, %rsp
.Lcfi78:
	.cfi_adjust_cfa_offset 8
	movl	$33, %edi
	movl	$13, %esi
	movl	$.L.str.48, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %rbx
	pushq	32(%rsp)                # 8-byte Folded Reload
.Lcfi79:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi80:
	.cfi_adjust_cfa_offset -16
	movq	%r13, %rdi
	callq	fclose
	movb	$0, 43(%rbx)
	movq	$0, 48(%rbx)
	jmp	.LBB5_117
.LBB5_70:
	movq	%rbx, %r14
.LBB5_112:                              # %._crit_edge
	movq	%r14, %rbx
	cmpb	$0, 43(%rbx)
	je	.LBB5_116
# BB#113:
	testb	$1, %al
	je	.LBB5_115
# BB#114:
	movq	$0, 48(%rbx)
	jmp	.LBB5_117
.LBB5_116:
	movq	%r13, 48(%rbx)
	movw	%dx, 40(%rbx)
	jmp	.LBB5_117
.LBB5_115:
	movl	8(%rsp), %edi           # 4-byte Reload
	callq	FileName
	leaq	80(%rsp), %rdx
	leaq	64(%rsp), %rcx
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	ReadLines
	movq	%rax, 48(%rbx)
	movzwl	64(%rsp), %esi
	movw	%si, 40(%rbx)
	movq	%rax, %rdi
	callq	SortLines
.LBB5_117:
	movq	%rbx, %rax
	addq	$1112, %rsp             # imm = 0x458
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_110:
	movq	32(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB5_112
.Lfunc_end5:
	.size	DbLoad, .Lfunc_end5-DbLoad
	.cfi_endproc

	.globl	DbRetrieve
	.p2align	4, 0x90
	.type	DbRetrieve,@function
DbRetrieve:                             # @DbRetrieve
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi84:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi85:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 56
	subq	$1592, %rsp             # imm = 0x638
.Lcfi87:
	.cfi_def_cfa_offset 1648
.Lcfi88:
	.cfi_offset %rbx, -56
.Lcfi89:
	.cfi_offset %r12, -48
.Lcfi90:
	.cfi_offset %r13, -40
.Lcfi91:
	.cfi_offset %r14, -32
.Lcfi92:
	.cfi_offset %r15, -24
.Lcfi93:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r13
	movl	%esi, %r12d
	movq	%rdi, %r10
	xorl	%r14d, %r14d
	cmpb	$0, 42(%r10)
	je	.LBB6_67
# BB#1:
	cmpq	$0, 48(%r10)
	je	.LBB6_67
# BB#2:                                 # %.outer.preheader
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%r9, 32(%rsp)           # 8-byte Spill
	xorl	%r14d, %r14d
	movq	%r10, %rbp
	movq	%r10, 8(%rsp)           # 8-byte Spill
	jmp	.LBB6_3
.LBB6_11:                               #   in Loop: Header=BB6_3 Depth=1
	movzbl	%al, %eax
	cmpl	%r14d, %eax
	cmovgel	%eax, %r14d
	.p2align	4, 0x90
.LBB6_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_5 Depth 2
	movq	8(%rbp), %rbp
	cmpq	%r10, %rbp
	je	.LBB6_12
# BB#4:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB6_3 Depth=1
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB6_5:                                # %.preheader
                                        #   Parent Loop BB6_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB6_5
# BB#6:                                 # %.preheader
                                        #   in Loop: Header=BB6_3 Depth=1
	cmpb	$-116, %al
	je	.LBB6_9
# BB#7:                                 # %.preheader
                                        #   in Loop: Header=BB6_3 Depth=1
	cmpb	$17, %al
	je	.LBB6_9
# BB#8:                                 #   in Loop: Header=BB6_3 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.14, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rsp), %r10           # 8-byte Reload
	movb	32(%rbx), %al
.LBB6_9:                                # %.loopexit
                                        #   in Loop: Header=BB6_3 Depth=1
	cmpb	$-116, %al
	jne	.LBB6_3
# BB#10:                                #   in Loop: Header=BB6_3 Depth=1
	cmpq	%r13, 64(%rbx)
	movb	33(%rbp), %al
	jne	.LBB6_11
	jmp	.LBB6_24
.LBB6_12:                               # %.thread
	cmpq	$0, 88(%r13)
	jne	.LBB6_14
# BB#13:
	movq	%r13, %rdi
	callq	CrossInit
	movq	8(%rsp), %r10           # 8-byte Reload
.LBB6_14:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB6_15
# BB#16:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB6_17
.LBB6_15:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	%rax, zz_hold(%rip)
.LBB6_17:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r10, zz_hold(%rip)
	testq	%r10, %r10
	je	.LBB6_20
# BB#18:
	testq	%rax, %rax
	je	.LBB6_20
# BB#19:
	movq	(%r10), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r10)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB6_20:
	movq	%rax, zz_res(%rip)
	movq	88(%r13), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB6_23
# BB#21:
	testq	%rcx, %rcx
	je	.LBB6_23
# BB#22:
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB6_23:
	movq	(%r10), %rax
	incl	%r14d
	movb	%r14b, 33(%rax)
	movb	$0, 34(%rax)
	movl	%r14d, %eax
.LBB6_24:                               # %._crit_edge
	movq	%r10, %rbx
	movzbl	%al, %ecx
	testl	%r12d, %r12d
	movl	$.L.str.17, %eax
	movl	$.L.str.12, %edx
	cmovneq	%rax, %rdx
	leaq	48(%rsp), %rdi
	movl	$.L.str.49, %esi
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	sprintf
	cmpb	$0, 43(%rbx)
	je	.LBB6_38
# BB#25:
	movzwl	40(%rbx), %ebp
	testl	%ebp, %ebp
	je	.LBB6_36
# BB#26:
	movq	48(%rbx), %r15
	xorl	%ebx, %ebx
	decl	%ebp
	je	.LBB6_35
# BB#27:                                # %.lr.ph.i.preheader
	leaq	48(%rsp), %r14
.LBB6_28:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_29 Depth 2
	movl	%ebp, %r12d
	decl	%r12d
	.p2align	4, 0x90
.LBB6_29:                               #   Parent Loop BB6_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rbx,%r12), %eax
	movl	%eax, %ebp
	shrl	$31, %ebp
	addl	%eax, %ebp
	sarl	%ebp
	cmpl	$0, UseCollate(%rip)
	movslq	%ebp, %rax
	movq	(%r15,%rax,8), %rsi
	movq	%r14, %rdi
	je	.LBB6_31
# BB#30:                                #   in Loop: Header=BB6_29 Depth=2
	callq	strcollcmp
	testl	%eax, %eax
	jg	.LBB6_34
	jmp	.LBB6_33
	.p2align	4, 0x90
.LBB6_31:                               #   in Loop: Header=BB6_29 Depth=2
	callq	strcmp
	testl	%eax, %eax
	jle	.LBB6_33
.LBB6_34:                               #   in Loop: Header=BB6_29 Depth=2
	leal	1(%rbp), %ebx
	cmpl	%r12d, %ebp
	jl	.LBB6_29
	jmp	.LBB6_35
.LBB6_33:                               # %.outer.i
                                        #   in Loop: Header=BB6_28 Depth=1
	cmpl	%ebp, %ebx
	jl	.LBB6_28
.LBB6_35:                               # %.outer._crit_edge.i
	movslq	%ebx, %rbx
	movq	(%r15,%rbx,8), %rdi
	leaq	1072(%rsp), %rbp
	movl	$.L.str.58, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	sscanf
	leaq	48(%rsp), %rdi
	movq	%rbp, %rsi
	callq	strcmp
	testl	%eax, %eax
	movq	8(%rsp), %rax           # 8-byte Reload
	je	.LBB6_37
.LBB6_36:                               # %SearchLines.exit.thread
	xorl	%r14d, %r14d
	jmp	.LBB6_67
.LBB6_38:
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	movl	$2, %edx
	callq	fseek
	movq	48(%rbx), %rbp
	movzwl	40(%rbx), %r14d
	movq	%rbp, %rdi
	callq	ftell
	leal	-1(%rax), %ecx
	cmpl	%eax, %r14d
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	jge	.LBB6_39
# BB#40:                                # %.lr.ph.i95.preheader
	leaq	560(%rsp), %rbx
	movl	%ecx, %r12d
.LBB6_41:                               # %.lr.ph.i95
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_42 Depth 2
                                        #       Child Loop BB6_43 Depth 3
	leal	1(%r12), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	%r14d, %r13d
	.p2align	4, 0x90
.LBB6_42:                               #   Parent Loop BB6_41 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_43 Depth 3
	movl	%r13d, %r14d
	leal	(%r14,%r12), %eax
	movl	%eax, %r15d
	shrl	$31, %r15d
	addl	%eax, %r15d
	sarl	%r15d
	movslq	%r15d, %rsi
	xorl	%edx, %edx
	movq	%rbp, %rdi
	callq	fseek
	.p2align	4, 0x90
.LBB6_43:                               #   Parent Loop BB6_41 Depth=1
                                        #     Parent Loop BB6_42 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%r15d
	movq	%rbp, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	jne	.LBB6_43
# BB#44:                                #   in Loop: Header=BB6_42 Depth=2
	cmpl	%r15d, 20(%rsp)         # 4-byte Folded Reload
	jne	.LBB6_46
# BB#45:                                #   in Loop: Header=BB6_42 Depth=2
	movslq	%r14d, %rsi
	xorl	%edx, %edx
	movq	%rbp, %rdi
	callq	fseek
	movl	%r14d, %r15d
.LBB6_46:                               #   in Loop: Header=BB6_42 Depth=2
	movl	$512, %esi              # imm = 0x200
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	movq	%rbp, %rdi
	callq	ftell
	movq	%rax, %r13
	leal	-1(%r13), %ebx
	movq	%r14, 40(%rsp)          # 8-byte Spill
	cmpl	%r15d, %r14d
	jle	.LBB6_48
# BB#47:                                #   in Loop: Header=BB6_42 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.59, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_48:                               #   in Loop: Header=BB6_42 Depth=2
	cmpl	%ebx, %r15d
	jl	.LBB6_50
# BB#49:                                #   in Loop: Header=BB6_42 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.60, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_50:                               #   in Loop: Header=BB6_42 Depth=2
	cmpl	%r12d, %ebx
	jle	.LBB6_52
# BB#51:                                #   in Loop: Header=BB6_42 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.61, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_52:                               #   in Loop: Header=BB6_42 Depth=2
	cmpl	$0, UseCollate(%rip)
	leaq	560(%rsp), %rbx
	leaq	48(%rsp), %rdi
	movq	%rbx, %rsi
	je	.LBB6_54
# BB#53:                                #   in Loop: Header=BB6_42 Depth=2
	callq	strcollcmp
	testl	%eax, %eax
	jg	.LBB6_56
	jmp	.LBB6_57
	.p2align	4, 0x90
.LBB6_54:                               #   in Loop: Header=BB6_42 Depth=2
	callq	strcmp
	testl	%eax, %eax
	jle	.LBB6_57
.LBB6_56:                               # %.backedge.i
                                        #   in Loop: Header=BB6_42 Depth=2
	cmpl	%r12d, %r13d
	jle	.LBB6_42
	jmp	.LBB6_58
.LBB6_57:                               # %.outer.i96
                                        #   in Loop: Header=BB6_41 Depth=1
	leal	-1(%r15), %r12d
	movq	40(%rsp), %r14          # 8-byte Reload
	cmpl	%r15d, %r14d
	movl	%r14d, %r13d
	jl	.LBB6_41
	jmp	.LBB6_58
.LBB6_39:
	movl	%r14d, %r13d
.LBB6_58:                               # %.outer._crit_edge.i97
	cmpl	16(%rsp), %r13d         # 4-byte Folded Reload
	jge	.LBB6_59
# BB#60:                                # %SearchFile.exit
	movslq	%r13d, %rsi
	xorl	%r14d, %r14d
	xorl	%edx, %edx
	movq	%rbp, %rdi
	callq	fseek
	leaq	560(%rsp), %r12
	movl	$512, %esi              # imm = 0x200
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	fgets
	leaq	1072(%rsp), %rbp
	movl	$.L.str.58, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	sscanf
	leaq	48(%rsp), %rdi
	movq	%rbp, %rsi
	callq	strcmp
	testl	%eax, %eax
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	jne	.LBB6_67
# BB#61:
	leaq	560(%rsp), %rdi
	leaq	48(%rsp), %r9
	movl	$.L.str.50, %esi
	movq	%rax, %rdx
	xorl	%eax, %eax
	movq	1656(%rsp), %r8
	movq	1648(%rsp), %rcx
	callq	sscanf
	movq	48(%rbp), %rdi
	callq	ftell
	jmp	.LBB6_62
.LBB6_59:                               # %SearchFile.exit.thread
	xorl	%r14d, %r14d
	jmp	.LBB6_67
.LBB6_37:
	movq	48(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	leaq	48(%rsp), %r9
	movl	$.L.str.50, %esi
	xorl	%eax, %eax
	movq	1656(%rsp), %r8
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	1648(%rsp), %rcx
	callq	sscanf
	incl	%ebx
	movslq	%ebx, %rax
	movq	32(%rsp), %rbx          # 8-byte Reload
.LBB6_62:
	movq	1664(%rsp), %rcx
	movq	%rax, (%rcx)
	cmpb	$46, 48(%rsp)
	jne	.LBB6_65
# BB#63:
	cmpb	$0, 49(%rsp)
	jne	.LBB6_65
# BB#64:
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	64(%rax), %rsi
	leaq	48(%rsp), %rdi
	callq	strcpy
.LBB6_65:                               # %.thread101
	leaq	48(%rsp), %rdi
	movl	$.L.str.28, %esi
	callq	FileNum
	movw	%ax, (%rbx)
	movl	$1, %r14d
	testw	%ax, %ax
	jne	.LBB6_67
# BB#66:
	movq	8(%rsp), %rdx           # 8-byte Reload
	addq	$32, %rdx
	leaq	48(%rsp), %rdi
	movl	$.L.str.28, %esi
	movl	$3, %ecx
	xorl	%r8d, %r8d
	callq	DefineFile
	movw	%ax, (%rbx)
.LBB6_67:
	movl	%r14d, %eax
	addq	$1592, %rsp             # imm = 0x638
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	DbRetrieve, .Lfunc_end6-DbRetrieve
	.cfi_endproc

	.globl	DbRetrieveNext
	.p2align	4, 0x90
	.type	DbRetrieveNext,@function
DbRetrieveNext:                         # @DbRetrieveNext
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi94:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi95:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi96:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi97:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi98:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 56
	subq	$1048, %rsp             # imm = 0x418
.Lcfi100:
	.cfi_def_cfa_offset 1104
.Lcfi101:
	.cfi_offset %rbx, -56
.Lcfi102:
	.cfi_offset %r12, -48
.Lcfi103:
	.cfi_offset %r13, -40
.Lcfi104:
	.cfi_offset %r14, -32
.Lcfi105:
	.cfi_offset %r15, -24
.Lcfi106:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %r13
	movq	%rcx, %r12
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	cmpb	$0, 42(%rbx)
	jne	.LBB7_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.51, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_2:
	movq	48(%rbx), %rdi
	xorl	%r14d, %r14d
	testq	%rdi, %rdi
	je	.LBB7_27
# BB#3:
	movq	1120(%rsp), %rax
	movq	1104(%rsp), %r9
	cmpb	$0, 43(%rbx)
	movq	(%rax), %rsi
	je	.LBB7_6
# BB#4:
	movzwl	40(%rbx), %eax
	cmpq	%rax, %rsi
	jge	.LBB7_27
# BB#5:
	movq	(%rdi,%rsi,8), %rax
	xorl	%edi, %edi
	cmpb	$48, (%rax)
	sete	%dil
	movl	%edi, (%rbp)
	addq	%rax, %rdi
	leaq	16(%rsp), %rbp
	leaq	4(%rsp), %rdx
	movl	$.L.str.52, %esi
	movl	$0, %eax
	movq	%r13, %r8
	movq	%r12, %rcx
	pushq	%rbp
.Lcfi107:
	.cfi_adjust_cfa_offset 8
	pushq	1120(%rsp)
.Lcfi108:
	.cfi_adjust_cfa_offset 8
	callq	sscanf
	addq	$16, %rsp
.Lcfi109:
	.cfi_adjust_cfa_offset -16
	movq	1120(%rsp), %rcx
	movq	(%rcx), %rax
	incq	%rax
	jmp	.LBB7_10
.LBB7_6:
	testq	%rsi, %rsi
	jne	.LBB7_8
# BB#7:
	movzwl	40(%rbx), %esi
.LBB7_8:
	xorl	%r14d, %r14d
	xorl	%edx, %edx
	callq	fseek
	movq	48(%rbx), %rdx
	leaq	528(%rsp), %rdi
	movl	$512, %esi              # imm = 0x200
	callq	fgets
	testq	%rax, %rax
	movq	1112(%rsp), %r10
	movq	1104(%rsp), %r9
	je	.LBB7_27
# BB#9:
	xorl	%eax, %eax
	cmpb	$48, 528(%rsp)
	sete	%al
	movl	%eax, (%rbp)
	leaq	528(%rsp,%rax), %rdi
	leaq	16(%rsp), %rbp
	leaq	4(%rsp), %rdx
	movl	$.L.str.52, %esi
	movl	$0, %eax
	movq	%r13, %r8
	movq	%r12, %rcx
	pushq	%rbp
.Lcfi110:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi111:
	.cfi_adjust_cfa_offset 8
	callq	sscanf
	addq	$16, %rsp
.Lcfi112:
	.cfi_adjust_cfa_offset -16
	movq	48(%rbx), %rdi
	callq	ftell
	movq	1120(%rsp), %rcx
.LBB7_10:
	movq	%rax, (%rcx)
	cmpb	$46, 16(%rsp)
	jne	.LBB7_13
# BB#11:
	cmpb	$0, 17(%rsp)
	jne	.LBB7_13
# BB#12:
	leaq	64(%rbx), %rsi
	leaq	16(%rsp), %rdi
	callq	strcpy
.LBB7_13:                               # %.thread
	leaq	16(%rsp), %rdi
	movl	$.L.str.28, %esi
	callq	FileNum
	movw	%ax, (%r15)
	testw	%ax, %ax
	jne	.LBB7_15
# BB#14:
	leaq	32(%rbx), %rdx
	leaq	16(%rsp), %rdi
	movl	$.L.str.28, %esi
	movl	$3, %ecx
	xorl	%r8d, %r8d
	callq	DefineFile
	movw	%ax, (%r15)
.LBB7_15:                               # %.preheader88
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB7_16
# BB#17:                                # %.preheader.lr.ph
	movl	4(%rsp), %ecx
	.p2align	4, 0x90
.LBB7_18:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_19 Depth 2
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB7_19:                               #   Parent Loop BB7_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %edx
	testb	%dl, %dl
	je	.LBB7_19
# BB#20:                                #   in Loop: Header=BB7_18 Depth=1
	cmpb	$-116, %dl
	jne	.LBB7_22
# BB#21:                                #   in Loop: Header=BB7_18 Depth=1
	movzbl	33(%rax), %edx
	cmpl	%ecx, %edx
	je	.LBB7_24
.LBB7_22:                               # %.backedge
                                        #   in Loop: Header=BB7_18 Depth=1
	movq	8(%rax), %rax
	cmpq	%rbx, %rax
	jne	.LBB7_18
	jmp	.LBB7_23
.LBB7_16:
                                        # implicit-def: %RBP
.LBB7_23:                               # %.thread85
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.53, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_24:                               # %.loopexit109
	movq	8(%rsp), %rbx           # 8-byte Reload
	cmpb	$-116, 32(%rbp)
	je	.LBB7_26
# BB#25:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.54, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_26:
	movq	64(%rbp), %rax
	movq	%rax, (%rbx)
	movl	$1, %r14d
.LBB7_27:
	movl	%r14d, %eax
	addq	$1048, %rsp             # imm = 0x418
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	DbRetrieveNext, .Lfunc_end7-DbRetrieveNext
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"DbCreate: !is_word(type(x))"
	.size	.L.str.1, 28

	.type	DbInsert.extra_seq,@object # @DbInsert.extra_seq
	.local	DbInsert.extra_seq
	.comm	DbInsert.extra_seq,4,4
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"DbInsert: db!"
	.size	.L.str.2, 14

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"DbInsert: null tag!"
	.size	.L.str.3, 20

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"DbInsert: null seq!"
	.size	.L.str.4, 20

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"DbInsert: insert into reading database"
	.size	.L.str.5, 39

	.type	DbCheckTableInit,@object # @DbCheckTableInit
	.local	DbCheckTableInit
	.comm	DbCheckTableInit,1,4
	.type	DbCheckTable,@object    # @DbCheckTable
	.local	DbCheckTable
	.comm	DbCheckTable,8,8
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"cross reference %s&&%s used previously, at%s"
	.size	.L.str.6, 45

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"cross reference %s&&%s used previously"
	.size	.L.str.7, 39

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"database file name %s%s is too long"
	.size	.L.str.8, 36

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	".lix"
	.size	.L.str.9, 5

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"w"
	.size	.L.str.10, 2

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"cannot write to database file %s"
	.size	.L.str.11, 33

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.zero	1
	.size	.L.str.12, 1

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"."
	.size	.L.str.13, 2

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"SymToNum: yy!"
	.size	.L.str.14, 14

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"%s%d&%s\t%s\t%s\t%ld\t%d\t%s\n"
	.size	.L.str.16, 25

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"0"
	.size	.L.str.17, 2

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"DbConvert: reading database"
	.size	.L.str.18, 28

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	".li"
	.size	.L.str.19, 4

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"00 %s %s\n"
	.size	.L.str.20, 10

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"Basser Lout Version 3.24 (October 2000)"
	.size	.L.str.21, 40

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"database index file"
	.size	.L.str.22, 20

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"DbConvert: y!"
	.size	.L.str.23, 14

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"%s %d %s\n"
	.size	.L.str.24, 10

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"00target"
	.size	.L.str.25, 9

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"00symbol"
	.size	.L.str.26, 9

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	" "
	.size	.L.str.27, 2

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	".ld"
	.size	.L.str.28, 4

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"syntax error in database file %s"
	.size	.L.str.29, 33

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"DbLoad: create && symbs == nilobj!"
	.size	.L.str.30, 35

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"%s found in database but not declared in %s line"
	.size	.L.str.31, 49

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"@Database"
	.size	.L.str.32, 10

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"database symbol %s has no tag"
	.size	.L.str.33, 30

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"database symbol tag is not a simple word"
	.size	.L.str.34, 41

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"database symbol tag is an empty word"
	.size	.L.str.35, 37

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"DbLoad: FileName(dfnum) (1)!"
	.size	.L.str.36, 29

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"%s or end of file expected here"
	.size	.L.str.37, 32

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"{"
	.size	.L.str.38, 2

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"DbLoad: FileName(dfnum) (2)!"
	.size	.L.str.39, 29

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"cannot open database file %s"
	.size	.L.str.40, 29

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"DbLoad: FileName(index_fnum)!"
	.size	.L.str.41, 30

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"DbLoad: type(symbs)!"
	.size	.L.str.42, 21

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"00target "
	.size	.L.str.43, 10

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"00target %d"
	.size	.L.str.44, 12

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"00symbol %d"
	.size	.L.str.45, 12

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"%s"
	.size	.L.str.46, 3

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"DbLoad: type(y) != CLOSURE!"
	.size	.L.str.47, 28

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"undefined symbol in database file %s (line %d)"
	.size	.L.str.48, 47

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"%s%d&%s"
	.size	.L.str.49, 8

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"%*[^\t]\t%[^\t]\t%*[^\t]\t%ld\t%d\t%[^\n]"
	.size	.L.str.50, 33

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"DbRetrieveNext: not reading"
	.size	.L.str.51, 28

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"%d&%[^\t]\t%[^\t]\t%*[^\t]\t%ld\t%d\t%[^\n]"
	.size	.L.str.52, 35

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"NumToSym: no sym"
	.size	.L.str.53, 17

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"NumToSym: y!"
	.size	.L.str.54, 13

	.type	OldCrossDb,@object      # @OldCrossDb
	.comm	OldCrossDb,8,8
	.type	NewCrossDb,@object      # @NewCrossDb
	.comm	NewCrossDb,8,8
	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"run out of memory enlarging dbcheck table"
	.size	.L.str.55, 42

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"Dbcheck: entry inserted twice"
	.size	.L.str.56, 30

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"dtab_rehash: ACAT!"
	.size	.L.str.57, 19

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"%[^\t]"
	.size	.L.str.58, 6

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"SearchFile: l > mid!"
	.size	.L.str.59, 21

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"SearchFile: mid >= mid_end!"
	.size	.L.str.60, 28

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"SearchFile: mid_end > r!"
	.size	.L.str.61, 25


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
