	.text
	.file	"trie.bc"
	.globl	trie_init
	.p2align	4, 0x90
	.type	trie_init,@function
trie_init:                              # @trie_init
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$1, %edi
	movl	$216, %esi
	callq	calloc
	testq	%rax, %rax
	je	.LBB0_2
# BB#1:
	popq	%rcx
	retq
.LBB0_2:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	trie_init, .Lfunc_end0-trie_init
	.cfi_endproc

	.globl	trie_insert
	.p2align	4, 0x90
	.type	trie_insert,@function
trie_insert:                            # @trie_insert
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 48
.Lcfi6:
	.cfi_offset %rbx, -40
.Lcfi7:
	.cfi_offset %r12, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_2
# BB#1:
	movl	$1, %edi
	movl	$216, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB1_6
.LBB1_2:
	movsbq	(%r14), %r12
	testq	%r12, %r12
	je	.LBB1_3
# BB#5:
	callq	__ctype_tolower_loc
	movq	%rax, %r15
	movq	(%r15), %rax
	movslq	(%rax,%r12,4), %rax
	movq	-776(%rbx,%rax,8), %rdi
	leaq	1(%r14), %rsi
	callq	trie_insert
	movq	(%r15), %rcx
	movsbq	(%r14), %rdx
	movslq	(%rcx,%rdx,4), %rcx
	movq	%rax, -776(%rbx,%rcx,8)
	jmp	.LBB1_4
.LBB1_3:
	incl	208(%rbx)
.LBB1_4:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB1_6:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	trie_insert, .Lfunc_end1-trie_insert
	.cfi_endproc

	.globl	trie_lookup
	.p2align	4, 0x90
	.type	trie_lookup,@function
trie_lookup:                            # @trie_lookup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	je	.LBB2_5
# BB#1:                                 # %.lr.ph
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	movsbq	(%rbp), %rcx
	testq	%rcx, %rcx
	movslq	(%rax,%rcx,4), %rcx
	movq	-776(%rbx,%rcx,8), %rbx
	je	.LBB2_4
# BB#2:                                 # %tailrecurse
                                        #   in Loop: Header=BB2_3 Depth=1
	incq	%rbp
	testq	%rbx, %rbx
	jne	.LBB2_3
	jmp	.LBB2_5
.LBB2_4:
	movl	208(%rbx), %r14d
.LBB2_5:                                # %.loopexit
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	trie_lookup, .Lfunc_end2-trie_lookup
	.cfi_endproc

	.globl	trie_scan
	.p2align	4, 0x90
	.type	trie_scan,@function
trie_scan:                              # @trie_scan
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 48
.Lcfi21:
	.cfi_offset %rbx, -40
.Lcfi22:
	.cfi_offset %r12, -32
.Lcfi23:
	.cfi_offset %r14, -24
.Lcfi24:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	testq	%r12, %r12
	je	.LBB3_7
# BB#1:
	cmpl	$0, 208(%r12)
	je	.LBB3_3
# BB#2:
	movl	$trie_scan_buffer, %edi
	callq	charsequence_val
	movq	%rax, %r15
	movl	208(%r12), %edi
	movq	%r15, %rsi
	callq	*%r14
	movq	%r15, %rdi
	callq	free
.LBB3_3:                                # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_4:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%r12,%rbx,8)
	je	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=1
	leal	97(%rbx), %eax
	movsbl	%al, %esi
	movl	$trie_scan_buffer, %edi
	callq	charsequence_push
	movq	(%r12,%rbx,8), %rdi
	movq	%r14, %rsi
	callq	trie_scan
	movl	$trie_scan_buffer, %edi
	callq	charsequence_pop
.LBB3_6:                                #   in Loop: Header=BB3_4 Depth=1
	incq	%rbx
	cmpq	$26, %rbx
	jne	.LBB3_4
.LBB3_7:                                # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	trie_scan, .Lfunc_end3-trie_scan
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Out of memory"
	.size	.L.str, 14

	.type	trie_scan_buffer,@object # @trie_scan_buffer
	.bss
	.globl	trie_scan_buffer
	.p2align	3
trie_scan_buffer:
	.zero	24
	.size	trie_scan_buffer, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
