	.text
	.file	"libclamav_wwunpack.bc"
	.globl	wwunpack
	.p2align	4, 0x90
	.type	wwunpack,@function
wwunpack:                               # @wwunpack
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 176
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, 44(%rsp)          # 4-byte Spill
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movl	%ecx, %ebp
	movl	%edx, %r15d
	movl	%esi, %r13d
	movq	%rdi, %rbx
	movq	176(%rsp), %r12
	leaq	673(%r12), %r14
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%ebp, %r8d
	movq	%r14, %rsi
	movq	%r12, %rdx
	movl	%r13d, %r12d
	movl	184(%rsp), %ebp
	movq	%rbx, %rcx
	movq	%rbp, %r14
	addq	%rdx, %r14
	movl	%r15d, 40(%rsp)         # 4-byte Spill
	movl	%r15d, %eax
	addq	%rcx, %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	%r8d, %eax
	negq	%rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	%r12d, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rax), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
.LBB0_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_16 Depth 2
                                        #       Child Loop BB0_252 Depth 3
                                        #       Child Loop BB0_257 Depth 3
                                        #       Child Loop BB0_262 Depth 3
                                        #       Child Loop BB0_265 Depth 3
	cmpl	$17, %ebp
	jb	.LBB0_287
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	leaq	17(%rsi), %rdi
	cmpq	%r14, %rdi
	ja	.LBB0_287
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	cmpq	%rdx, %rdi
	jbe	.LBB0_287
# BB#4:                                 #   in Loop: Header=BB0_1 Depth=1
	movl	8(%rsi), %ebx
	movl	12(%rsi), %eax
	leal	(,%rbx,4), %r13d
	addl	$4, %eax
	cmpl	%eax, %r13d
	jne	.LBB0_289
# BB#5:                                 #   in Loop: Header=BB0_1 Depth=1
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movl	%r8d, 32(%rsp)          # 4-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movl	(%rsi), %ebp
	movl	%r13d, %r15d
	movl	$1, %esi
	movq	%r15, %rdi
	callq	cli_calloc
	movq	%rax, (%rsp)            # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_290
# BB#6:                                 #   in Loop: Header=BB0_1 Depth=1
	movq	%r14, 56(%rsp)          # 8-byte Spill
	testl	%r12d, %r12d
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	je	.LBB0_288
# BB#7:                                 #   in Loop: Header=BB0_1 Depth=1
	testl	%ebx, %ebx
	je	.LBB0_288
# BB#8:                                 #   in Loop: Header=BB0_1 Depth=1
	cmpl	%r12d, %r13d
	ja	.LBB0_288
# BB#9:                                 #   in Loop: Header=BB0_1 Depth=1
	movl	%eax, %r14d
	subl	%ebp, %r14d
	addq	112(%rsp), %r14         # 8-byte Folded Reload
	addq	104(%rsp), %r14         # 8-byte Folded Reload
	cmpq	%rcx, %r14
	jb	.LBB0_288
# BB#10:                                #   in Loop: Header=BB0_1 Depth=1
	leaq	(%r14,%r15), %rax
	cmpq	96(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_288
# BB#11:                                #   in Loop: Header=BB0_1 Depth=1
	cmpq	%rcx, %rax
	jbe	.LBB0_288
# BB#12:                                #   in Loop: Header=BB0_1 Depth=1
	movl	%r12d, 36(%rsp)         # 4-byte Spill
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	memcpy
	cmpl	$4, %r13d
	jb	.LBB0_282
# BB#13:                                #   in Loop: Header=BB0_1 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	subq	%r14, %rcx
	addq	88(%rsp), %rcx          # 8-byte Folded Reload
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	4(%rax), %r8
	movl	(%rax), %esi
	addq	%rax, %r15
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	%ecx, %eax
	addq	%r14, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movb	$32, %r9b
	movq	%r14, %r13
	jmp	.LBB0_16
	.p2align	4, 0x90
.LBB0_14:                               #   in Loop: Header=BB0_16 Depth=2
	movb	(%r8), %bl
	movb	%bl, (%r13)
	movq	%rax, %r8
	movl	%r11d, %esi
	movl	%edx, %r9d
	movq	%rcx, %r13
	jmp	.LBB0_16
.LBB0_15:                               # %.backedge.loopexit.i
                                        #   in Loop: Header=BB0_16 Depth=2
	leaq	1(%r13,%r11), %r13
	movq	80(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_16:                               # %.backedge.i
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_252 Depth 3
                                        #       Child Loop BB0_257 Depth 3
                                        #       Child Loop BB0_262 Depth 3
                                        #       Child Loop BB0_265 Depth 3
	leal	(%rsi,%rsi), %r11d
	movl	%r9d, %edx
	decb	%dl
	testl	%esi, %esi
	js	.LBB0_25
# BB#17:                                # %.backedge.i
                                        #   in Loop: Header=BB0_16 Depth=2
	testb	%dl, %dl
	je	.LBB0_25
# BB#18:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#19:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	1(%r8), %rax
	cmpq	%r15, %rax
	ja	.LBB0_282
# BB#20:                                #   in Loop: Header=BB0_16 Depth=2
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB0_282
# BB#21:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#22:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r14, %r13
	jb	.LBB0_282
# BB#23:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	1(%r13), %rcx
	cmpq	8(%rsp), %rcx           # 8-byte Folded Reload
	ja	.LBB0_282
# BB#24:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r14, %rcx
	ja	.LBB0_14
	jmp	.LBB0_282
	.p2align	4, 0x90
.LBB0_25:                               #   in Loop: Header=BB0_16 Depth=2
	testb	%dl, %dl
	je	.LBB0_28
# BB#26:                                #   in Loop: Header=BB0_16 Depth=2
	shrl	$29, %esi
	andl	$3, %esi
	cmpb	$2, %dl
	jbe	.LBB0_40
# BB#27:                                #   in Loop: Header=BB0_16 Depth=2
	movb	$2, %al
	movl	$2, %ebx
	movq	%r8, %rbp
	jmp	.LBB0_51
.LBB0_28:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#29:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rbp
	cmpq	%r15, %rbp
	ja	.LBB0_282
# BB#30:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rbp            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#31:                                #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %r11d
	testl	%esi, %esi
	js	.LBB0_49
# BB#32:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rbp            # 8-byte Folded Reload
	jb	.LBB0_282
# BB#33:                                #   in Loop: Header=BB0_16 Depth=2
	addq	$5, %r8
	cmpq	%r15, %r8
	ja	.LBB0_282
# BB#34:                                #   in Loop: Header=BB0_16 Depth=2
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB0_282
# BB#35:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#36:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r14, %r13
	jb	.LBB0_282
# BB#37:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	1(%r13), %rax
	cmpq	8(%rsp), %rax           # 8-byte Folded Reload
	ja	.LBB0_282
# BB#38:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r14, %rax
	jbe	.LBB0_282
# BB#39:                                #   in Loop: Header=BB0_16 Depth=2
	movb	(%rbp), %cl
	movb	%cl, (%r13)
	movl	%r11d, %esi
	movb	$32, %r9b
	movq	%rax, %r13
	jmp	.LBB0_16
.LBB0_40:                               #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_45
# BB#41:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#42:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rbp
	cmpq	%r15, %rbp
	ja	.LBB0_282
# BB#43:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rbp            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#44:                                # %getbitmap.exit41.i316.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %r11d
	movb	$32, %dl
	jmp	.LBB0_52
.LBB0_45:                               #   in Loop: Header=BB0_16 Depth=2
	movb	$3, %al
	subb	%r9b, %al
	movl	%eax, %ecx
	shrl	%cl, %esi
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#46:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rbp
	cmpq	%r15, %rbp
	ja	.LBB0_282
# BB#47:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rbp            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#48:                                #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %ebx
	movl	(%r8), %r11d
	movl	%eax, %ecx
	shldl	%cl, %r11d, %esi
	jmp	.LBB0_50
.LBB0_49:                               # %.thread.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%r11d, %esi
	shrl	$30, %esi
	movb	$2, %al
	movl	$2, %ebx
.LBB0_50:                               # %.sink.split.i319.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movb	$32, %dl
.LBB0_51:                               # %.sink.split.i319.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%ebx, %ecx
	shll	%cl, %r11d
	subb	%al, %dl
.LBB0_52:                               # %getbits.exit321.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movzbl	%sil, %r12d
	cmpl	$3, %r12d
	jb	.LBB0_55
# BB#53:                                #   in Loop: Header=BB0_16 Depth=2
	movl	%r11d, %edi
	shrl	$30, %edi
	cmpb	$2, %dl
	jbe	.LBB0_57
# BB#54:                                #   in Loop: Header=BB0_16 Depth=2
	movb	$2, %al
	movl	$2, %esi
	jmp	.LBB0_71
.LBB0_55:                               #   in Loop: Header=BB0_16 Depth=2
	movl	%r11d, %eax
	shrl	$29, %eax
	cmpb	$3, %dl
	jbe	.LBB0_62
# BB#56:                                #   in Loop: Header=BB0_16 Depth=2
	movb	$3, %bl
	movl	$3, %esi
	jmp	.LBB0_87
.LBB0_57:                               #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_67
# BB#58:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rbp            # 8-byte Folded Reload
	jb	.LBB0_282
# BB#59:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%rbp), %r9
	cmpq	%r15, %r9
	ja	.LBB0_282
# BB#60:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r9             # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#61:                                # %getbitmap.exit41.i308.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%rbp), %r11d
	movb	$32, %dl
	jmp	.LBB0_72
.LBB0_62:                               #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_83
# BB#63:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rbp            # 8-byte Folded Reload
	jb	.LBB0_282
# BB#64:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%rbp), %r8
	cmpq	%r15, %r8
	ja	.LBB0_282
# BB#65:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#66:                                # %getbitmap.exit41.i292.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%rbp), %r11d
	movb	$32, %dl
	jmp	.LBB0_88
.LBB0_67:                               #   in Loop: Header=BB0_16 Depth=2
	movb	$2, %al
	subb	%dl, %al
	movl	%eax, %ecx
	shrl	%cl, %edi
	cmpq	(%rsp), %rbp            # 8-byte Folded Reload
	jb	.LBB0_282
# BB#68:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%rbp), %rbx
	cmpq	%r15, %rbx
	ja	.LBB0_282
# BB#69:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rbx            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#70:                                #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %esi
	movl	(%rbp), %r11d
	movl	%eax, %ecx
	shldl	%cl, %r11d, %edi
	movb	$32, %dl
	movq	%rbx, %rbp
.LBB0_71:                               # %.sink.split.i311.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%esi, %ecx
	shll	%cl, %r11d
	subb	%al, %dl
	movq	%rbp, %r9
.LBB0_72:                               # %getbits.exit313.i
                                        #   in Loop: Header=BB0_16 Depth=2
	leal	5(%rdi), %eax
	addl	$65535, %edi            # imm = 0xFFFF
	testw	%di, %di
	jle	.LBB0_74
# BB#73:                                #   in Loop: Header=BB0_16 Depth=2
	incb	%al
	movl	$1, %r10d
	movl	%eax, %ecx
	shll	%cl, %r10d
	addl	$65377, %r10d           # imm = 0xFF61
	jmp	.LBB0_75
.LBB0_74:                               #   in Loop: Header=BB0_16 Depth=2
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	movl	%esi, %ecx
	andl	$65280, %ecx            # imm = 0xFF00
	addl	$225, %esi
	movzbl	%sil, %r10d
	orl	%ecx, %r10d
.LBB0_75:                               #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %esi
	movl	$32, %ecx
	subl	%esi, %ecx
	movl	%r11d, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebp
	movl	%esi, %ebx
	subb	%dl, %bl
	jae	.LBB0_77
# BB#76:                                #   in Loop: Header=BB0_16 Depth=2
	movq	%r9, %r8
	movl	%eax, %ebx
	jmp	.LBB0_82
.LBB0_77:                               #   in Loop: Header=BB0_16 Depth=2
	jbe	.LBB0_95
# BB#78:                                #   in Loop: Header=BB0_16 Depth=2
	movl	%ebx, %ecx
	shrl	%cl, %ebp
	cmpq	(%rsp), %r9             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#79:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r9), %r8
	cmpq	%r15, %r8
	ja	.LBB0_282
# BB#80:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#81:                                #   in Loop: Header=BB0_16 Depth=2
	movzbl	%bl, %esi
	movl	(%r9), %r11d
	movl	%ebx, %ecx
	shldl	%cl, %r11d, %ebp
	movb	$32, %dl
.LBB0_82:                               # %.sink.split.i303.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%esi, %ecx
	shll	%cl, %r11d
	subb	%bl, %dl
	movl	%r11d, %esi
	movl	%edx, %r9d
	jmp	.LBB0_99
.LBB0_83:                               #   in Loop: Header=BB0_16 Depth=2
	movb	$3, %bl
	subb	%dl, %bl
	movl	%ebx, %ecx
	shrl	%cl, %eax
	cmpq	(%rsp), %rbp            # 8-byte Folded Reload
	jb	.LBB0_282
# BB#84:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%rbp), %rdi
	cmpq	%r15, %rdi
	ja	.LBB0_282
# BB#85:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rdi            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#86:                                #   in Loop: Header=BB0_16 Depth=2
	movzbl	%bl, %esi
	movl	(%rbp), %r11d
	movl	%ebx, %ecx
	shldl	%cl, %r11d, %eax
	movb	$32, %dl
	movq	%rdi, %rbp
.LBB0_87:                               # %.sink.split.i295.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%esi, %ecx
	shll	%cl, %r11d
	subb	%bl, %dl
	movq	%rbp, %r8
.LBB0_88:                               # %getbits.exit297.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %esi
	cmpl	$3, %esi
	ja	.LBB0_92
# BB#89:                                #   in Loop: Header=BB0_16 Depth=2
	xorl	%ecx, %ecx
	cmpl	$3, %esi
	jne	.LBB0_116
# BB#90:                                #   in Loop: Header=BB0_16 Depth=2
	movl	%r11d, %ecx
	shrl	$31, %ecx
	decb	%dl
	je	.LBB0_112
# BB#91:                                #   in Loop: Header=BB0_16 Depth=2
	addl	%r11d, %r11d
	leal	5(%rax,%rcx), %eax
	jmp	.LBB0_122
.LBB0_92:                               #   in Loop: Header=BB0_16 Depth=2
	cmpl	$4, %esi
	jne	.LBB0_108
# BB#93:                                #   in Loop: Header=BB0_16 Depth=2
	decb	%dl
	je	.LBB0_117
# BB#94:                                #   in Loop: Header=BB0_16 Depth=2
	leal	(%r11,%r11), %ecx
	jmp	.LBB0_121
.LBB0_95:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r9             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#96:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r9), %r8
	cmpq	%r15, %r8
	ja	.LBB0_282
# BB#97:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#98:                                # %getbitmap.exit41.i300.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r9), %esi
	movb	$32, %r9b
.LBB0_99:                               # %getbits.exit305.i
                                        #   in Loop: Header=BB0_16 Depth=2
	cmpl	$2, 24(%rsp)            # 4-byte Folded Reload
	movzwl	%bp, %ebx
	jb	.LBB0_276
# BB#100:                               # %getbits.exit305.i
                                        #   in Loop: Header=BB0_16 Depth=2
	cmpl	$511, %ebx              # imm = 0x1FF
	je	.LBB0_276
# BB#101:                               #   in Loop: Header=BB0_16 Depth=2
	addl	%r10d, %ebp
	movzwl	%bp, %eax
	movq	%r13, %rcx
	subq	%rax, %rcx
	cmpq	%r14, %rcx
	jb	.LBB0_282
# BB#102:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r14, %r13
	jb	.LBB0_282
# BB#103:                               #   in Loop: Header=BB0_16 Depth=2
	addq	$2, %rcx
	cmpq	%r14, %rcx
	jbe	.LBB0_282
# BB#104:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	8(%rsp), %rcx           # 8-byte Folded Reload
	ja	.LBB0_282
# BB#105:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	2(%r13), %rcx
	cmpq	8(%rsp), %rcx           # 8-byte Folded Reload
	ja	.LBB0_282
# BB#106:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r14, %rcx
	jbe	.LBB0_282
# BB#107:                               #   in Loop: Header=BB0_16 Depth=2
	negq	%rax
	movb	(%r13,%rax), %dl
	movb	%dl, (%r13)
	movb	1(%r13,%rax), %al
	movb	%al, 1(%r13)
	movq	%rcx, %r13
	jmp	.LBB0_16
.LBB0_108:                              #   in Loop: Header=BB0_16 Depth=2
	addl	$7, %eax
	movzbl	%al, %ecx
	cmpl	$13, %ecx
	jb	.LBB0_122
# BB#109:                               #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_158
# BB#110:                               #   in Loop: Header=BB0_16 Depth=2
	movl	%r11d, %r10d
	shrl	$18, %r10d
	cmpb	$14, %dl
	jbe	.LBB0_168
# BB#111:                               #   in Loop: Header=BB0_16 Depth=2
	movb	$14, %al
	movl	$14, %esi
	jmp	.LBB0_187
.LBB0_112:                              #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#113:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rsi
	cmpq	%r15, %rsi
	ja	.LBB0_282
# BB#114:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rsi            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#115:                               # %getbitmap.exit289.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %r11d
	movb	$32, %dl
	movq	%rsi, %r8
.LBB0_116:                              # %.critedge.i
                                        #   in Loop: Header=BB0_16 Depth=2
	leal	5(%rax,%rcx), %eax
	jmp	.LBB0_122
.LBB0_117:                              #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#118:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rsi
	cmpq	%r15, %rsi
	ja	.LBB0_282
# BB#119:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rsi            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#120:                               # %getbitmap.exit286.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %ecx
	movb	$32, %dl
	movq	%rsi, %r8
.LBB0_121:                              # %.critedge215.i
                                        #   in Loop: Header=BB0_16 Depth=2
	shrl	$31, %r11d
	leal	6(%r11,%rax), %eax
	movl	%ecx, %r11d
.LBB0_122:                              #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %esi
	movl	$32, %ecx
	subl	%esi, %ecx
	movl	%r11d, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebp
	movl	%esi, %ebx
	subb	%dl, %bl
	jae	.LBB0_124
# BB#123:                               #   in Loop: Header=BB0_16 Depth=2
	movq	%r8, %rdi
	movl	%eax, %ebx
	jmp	.LBB0_129
.LBB0_124:                              #   in Loop: Header=BB0_16 Depth=2
	jbe	.LBB0_130
# BB#125:                               #   in Loop: Header=BB0_16 Depth=2
	movl	%ebx, %ecx
	shrl	%cl, %ebp
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#126:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rdi
	cmpq	%r15, %rdi
	ja	.LBB0_282
# BB#127:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rdi            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#128:                               #   in Loop: Header=BB0_16 Depth=2
	movzbl	%bl, %esi
	movl	(%r8), %r11d
	movl	%ebx, %ecx
	shldl	%cl, %r11d, %ebp
	movb	$32, %dl
.LBB0_129:                              # %.sink.split.i265.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%esi, %ecx
	shll	%cl, %r11d
	subb	%bl, %dl
	jmp	.LBB0_134
.LBB0_130:                              #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#131:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rdi
	cmpq	%r15, %rdi
	ja	.LBB0_282
# BB#132:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rdi            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#133:                               # %getbitmap.exit41.i262.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %r11d
	movb	$32, %dl
.LBB0_134:                              # %.critedge218.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	addl	$65505, %esi            # imm = 0xFFE1
	movzwl	%si, %r10d
	addl	%ebp, %r10d
	movq	%rdi, %r8
.LBB0_135:                              #   in Loop: Header=BB0_16 Depth=2
	movl	%r11d, %esi
	movl	%edx, %r9d
.LBB0_136:                              #   in Loop: Header=BB0_16 Depth=2
	xorl	%edi, %edi
	cmpl	$1, %r12d
	setne	%al
	testl	%r12d, %r12d
	je	.LBB0_138
# BB#137:                               #   in Loop: Header=BB0_16 Depth=2
	movb	%al, %dil
	addl	$3, %edi
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_237
	jmp	.LBB0_282
.LBB0_138:                              #   in Loop: Header=BB0_16 Depth=2
	decb	%r9b
	je	.LBB0_140
# BB#139:                               #   in Loop: Header=BB0_16 Depth=2
	leal	(%rsi,%rsi), %edi
	testl	%esi, %esi
	jns	.LBB0_144
	jmp	.LBB0_146
.LBB0_140:                              #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#141:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rax
	cmpq	%r15, %rax
	ja	.LBB0_282
# BB#142:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#143:                               # %getbitmap.exit259.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %edi
	movb	$32, %r9b
	movq	%rax, %r8
	testl	%esi, %esi
	js	.LBB0_146
.LBB0_144:                              #   in Loop: Header=BB0_16 Depth=2
	decb	%r9b
	je	.LBB0_148
# BB#145:                               #   in Loop: Header=BB0_16 Depth=2
	leal	(%rdi,%rdi), %esi
	jmp	.LBB0_152
.LBB0_146:                              #   in Loop: Header=BB0_16 Depth=2
	movl	%edi, %ebp
	shrl	$29, %ebp
	cmpb	$3, %r9b
	jbe	.LBB0_153
# BB#147:                               #   in Loop: Header=BB0_16 Depth=2
	movb	$3, %al
	movl	$3, %esi
	jmp	.LBB0_164
.LBB0_148:                              #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#149:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rax
	cmpq	%r15, %rax
	ja	.LBB0_282
# BB#150:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#151:                               # %getbitmap.exit256.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %esi
	movb	$32, %r9b
	movq	%rax, %r8
.LBB0_152:                              #   in Loop: Header=BB0_16 Depth=2
	shrl	$31, %edi
	addl	$5, %edi
	movq	%r8, %rdx
	jmp	.LBB0_236
.LBB0_153:                              #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_160
# BB#154:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#155:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rdx
	cmpq	%r15, %rdx
	ja	.LBB0_282
# BB#156:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rdx            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#157:                               # %getbitmap.exit41.i248.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %esi
	movb	$32, %r9b
	testb	%bpl, %bpl
	jne	.LBB0_165
	jmp	.LBB0_166
.LBB0_158:                              #   in Loop: Header=BB0_16 Depth=2
	movl	%r11d, %r10d
	shrl	$17, %r10d
	cmpb	$15, %dl
	jbe	.LBB0_173
# BB#159:                               #   in Loop: Header=BB0_16 Depth=2
	movb	$15, %al
	movl	$15, %esi
	jmp	.LBB0_192
.LBB0_160:                              #   in Loop: Header=BB0_16 Depth=2
	movb	$3, %al
	subb	%r9b, %al
	movl	%eax, %ecx
	shrl	%cl, %ebp
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#161:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rdx
	cmpq	%r15, %rdx
	ja	.LBB0_282
# BB#162:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rdx            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#163:                               #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %esi
	movl	(%r8), %edi
	movl	%eax, %ecx
	shldl	%cl, %edi, %ebp
	movb	$32, %r9b
	movq	%rdx, %r8
.LBB0_164:                              # %.sink.split.i251.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%esi, %ecx
	shll	%cl, %edi
	subb	%al, %r9b
	movq	%r8, %rdx
	movl	%edi, %esi
	testb	%bpl, %bpl
	je	.LBB0_166
.LBB0_165:                              #   in Loop: Header=BB0_16 Depth=2
	addl	$6, %ebp
	movl	%ebp, %edi
	jmp	.LBB0_236
.LBB0_166:                              #   in Loop: Header=BB0_16 Depth=2
	movl	%esi, %edi
	shrl	$28, %edi
	cmpb	$4, %r9b
	jbe	.LBB0_178
# BB#167:                               #   in Loop: Header=BB0_16 Depth=2
	movb	$4, %al
	movl	$4, %r8d
	jmp	.LBB0_197
.LBB0_168:                              #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_183
# BB#169:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#170:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rax
	cmpq	%r15, %rax
	ja	.LBB0_282
# BB#171:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#172:                               # %getbitmap.exit41.i278.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %esi
	movb	$32, %r9b
	addl	$8161, %r10d            # imm = 0x1FE1
	movq	%rax, %r8
	jmp	.LBB0_136
.LBB0_173:                              #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_188
# BB#174:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#175:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rax
	cmpq	%r15, %rax
	ja	.LBB0_282
# BB#176:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#177:                               # %getbitmap.exit41.i270.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %esi
	movb	$32, %r9b
	addl	$24545, %r10d           # imm = 0x5FE1
	movq	%rax, %r8
	jmp	.LBB0_136
.LBB0_178:                              #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_193
# BB#179:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rdx            # 8-byte Folded Reload
	jb	.LBB0_282
# BB#180:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%rdx), %r8
	cmpq	%r15, %r8
	ja	.LBB0_282
# BB#181:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#182:                               # %getbitmap.exit41.i240.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%rdx), %esi
	movb	$32, %r9b
	testb	%dil, %dil
	jne	.LBB0_198
	jmp	.LBB0_199
.LBB0_183:                              #   in Loop: Header=BB0_16 Depth=2
	movb	$14, %al
	subb	%dl, %al
	movl	%eax, %ecx
	shrl	%cl, %r10d
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#184:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rdi
	cmpq	%r15, %rdi
	ja	.LBB0_282
# BB#185:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rdi            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#186:                               #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %esi
	movl	(%r8), %r11d
	movl	%eax, %ecx
	shldl	%cl, %r11d, %r10d
	movb	$32, %dl
	movq	%rdi, %r8
.LBB0_187:                              # %.sink.split.i281.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%esi, %ecx
	shll	%cl, %r11d
	subb	%al, %dl
	addl	$8161, %r10d            # imm = 0x1FE1
	jmp	.LBB0_135
.LBB0_188:                              #   in Loop: Header=BB0_16 Depth=2
	movb	$15, %al
	subb	%dl, %al
	movl	%eax, %ecx
	shrl	%cl, %r10d
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#189:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rdi
	cmpq	%r15, %rdi
	ja	.LBB0_282
# BB#190:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rdi            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#191:                               #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %esi
	movl	(%r8), %r11d
	movl	%eax, %ecx
	shldl	%cl, %r11d, %r10d
	movb	$32, %dl
	movq	%rdi, %r8
.LBB0_192:                              # %.sink.split.i273.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%esi, %ecx
	shll	%cl, %r11d
	subb	%al, %dl
	addl	$24545, %r10d           # imm = 0x5FE1
	jmp	.LBB0_135
.LBB0_193:                              #   in Loop: Header=BB0_16 Depth=2
	movb	$4, %al
	subb	%r9b, %al
	movl	%eax, %ecx
	shrl	%cl, %edi
	cmpq	(%rsp), %rdx            # 8-byte Folded Reload
	jb	.LBB0_282
# BB#194:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%rdx), %rbp
	cmpq	%r15, %rbp
	ja	.LBB0_282
# BB#195:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rbp            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#196:                               #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %r8d
	movl	(%rdx), %esi
	movl	%eax, %ecx
	shldl	%cl, %esi, %edi
	movb	$32, %r9b
	movq	%rbp, %rdx
.LBB0_197:                              # %.sink.split.i243.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%r8d, %ecx
	shll	%cl, %esi
	subb	%al, %r9b
	movq	%rdx, %r8
	testb	%dil, %dil
	je	.LBB0_199
.LBB0_198:                              #   in Loop: Header=BB0_16 Depth=2
	addl	$13, %edi
	movq	%r8, %rdx
	jmp	.LBB0_236
.LBB0_199:                              #   in Loop: Header=BB0_16 Depth=2
	decb	%r9b
	je	.LBB0_201
# BB#200:                               #   in Loop: Header=BB0_16 Depth=2
	leal	(%rsi,%rsi), %ebx
	testl	%esi, %esi
	jns	.LBB0_205
	jmp	.LBB0_207
.LBB0_201:                              #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#202:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rax
	cmpq	%r15, %rax
	ja	.LBB0_282
# BB#203:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#204:                               # %getbitmap.exit237.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %ebx
	movb	$32, %r9b
	movq	%rax, %r8
	testl	%esi, %esi
	js	.LBB0_207
.LBB0_205:                              #   in Loop: Header=BB0_16 Depth=2
	decb	%r9b
	je	.LBB0_208
# BB#206:                               #   in Loop: Header=BB0_16 Depth=2
	leal	(%rbx,%rbx), %eax
	testl	%ebx, %ebx
	jns	.LBB0_212
	jmp	.LBB0_214
.LBB0_207:                              #   in Loop: Header=BB0_16 Depth=2
	movb	$5, %r11b
	movl	$29, %esi
	jmp	.LBB0_222
.LBB0_208:                              #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#209:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rcx
	cmpq	%r15, %rcx
	ja	.LBB0_282
# BB#210:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rcx            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#211:                               # %getbitmap.exit237.i.1
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %eax
	movb	$32, %r9b
	movq	%rcx, %r8
	testl	%ebx, %ebx
	js	.LBB0_214
.LBB0_212:                              #   in Loop: Header=BB0_16 Depth=2
	decb	%r9b
	je	.LBB0_215
# BB#213:                               #   in Loop: Header=BB0_16 Depth=2
	leal	(%rax,%rax), %ebx
	testl	%eax, %eax
	jns	.LBB0_219
	jmp	.LBB0_221
.LBB0_214:                              #   in Loop: Header=BB0_16 Depth=2
	movb	$6, %r11b
	movl	$61, %esi
	movl	%eax, %ebx
	jmp	.LBB0_222
.LBB0_215:                              #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#216:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rcx
	cmpq	%r15, %rcx
	ja	.LBB0_282
# BB#217:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rcx            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#218:                               # %getbitmap.exit237.i.2
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %ebx
	movb	$32, %r9b
	movq	%rcx, %r8
	testl	%eax, %eax
	js	.LBB0_221
.LBB0_219:                              # %.us-lcssa.us.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%ebx, %edi
	shrl	$18, %edi
	cmpb	$14, %r9b
	jbe	.LBB0_266
# BB#220:                               #   in Loop: Header=BB0_16 Depth=2
	movb	$14, %al
	movl	$14, %ebp
	jmp	.LBB0_275
.LBB0_221:                              #   in Loop: Header=BB0_16 Depth=2
	movb	$7, %r11b
	movl	$125, %esi
.LBB0_222:                              # %.us-lcssa640.us.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movzbl	%r11b, %ebp
	movl	$32, %ecx
	subl	%ebp, %ecx
	movl	%ebx, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edi
	movl	%ebp, %eax
	subb	%r9b, %al
	jae	.LBB0_224
# BB#223:                               #   in Loop: Header=BB0_16 Depth=2
	movq	%r8, %rdx
	movl	%r11d, %eax
	jmp	.LBB0_229
.LBB0_224:                              #   in Loop: Header=BB0_16 Depth=2
	jbe	.LBB0_230
# BB#225:                               #   in Loop: Header=BB0_16 Depth=2
	movl	%eax, %ecx
	shrl	%cl, %edi
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#226:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rdx
	cmpq	%r15, %rdx
	ja	.LBB0_282
# BB#227:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rdx            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#228:                               #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %ebp
	movl	(%r8), %ebx
	movl	%eax, %ecx
	shldl	%cl, %ebx, %edi
	movb	$32, %r9b
.LBB0_229:                              # %.sink.split.i232.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%ebp, %ecx
	shll	%cl, %ebx
	subb	%al, %r9b
	jmp	.LBB0_234
.LBB0_230:                              #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#231:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rdx
	cmpq	%r15, %rdx
	ja	.LBB0_282
# BB#232:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rdx            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#233:                               # %getbitmap.exit41.i229.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %ebx
	movb	$32, %r9b
.LBB0_234:                              # %getbits.exit234.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%edi, %eax
	andl	$-256, %eax
	addl	%edi, %esi
	movzbl	%sil, %edi
	orl	%eax, %edi
.LBB0_235:                              # %.critedge219.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%ebx, %esi
.LBB0_236:                              # %.critedge219.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movq	%rdx, %r8
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB0_282
.LBB0_237:                              #   in Loop: Header=BB0_16 Depth=2
	testw	%di, %di
	je	.LBB0_282
# BB#238:                               #   in Loop: Header=BB0_16 Depth=2
	movzwl	%di, %eax
	cmpl	24(%rsp), %eax          # 4-byte Folded Reload
	ja	.LBB0_282
# BB#239:                               #   in Loop: Header=BB0_16 Depth=2
	movzwl	%r10w, %ebp
	movq	%r13, %rcx
	subq	%rbp, %rcx
	cmpq	%r14, %rcx
	jb	.LBB0_282
# BB#240:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r14, %r13
	jb	.LBB0_282
# BB#241:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	(%rcx,%rax), %rdx
	cmpq	%r14, %rdx
	jbe	.LBB0_282
# BB#242:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	8(%rsp), %rdx           # 8-byte Folded Reload
	ja	.LBB0_282
# BB#243:                               #   in Loop: Header=BB0_16 Depth=2
	addq	%r13, %rax
	cmpq	8(%rsp), %rax           # 8-byte Folded Reload
	ja	.LBB0_282
# BB#244:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r14, %rax
	jbe	.LBB0_282
# BB#245:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movq	%r15, 80(%rsp)          # 8-byte Spill
	movq	%rbp, %rax
	negq	%rax
	leal	-1(%rdi), %edx
	movzwl	%dx, %r11d
	leaq	1(%r11), %r10
	cmpq	$32, %r10
	jae	.LBB0_247
# BB#246:                               #   in Loop: Header=BB0_16 Depth=2
	movq	%r13, %rcx
	jmp	.LBB0_260
.LBB0_247:                              # %min.iters.checked
                                        #   in Loop: Header=BB0_16 Depth=2
	movq	%r10, %r15
	andq	$131040, %r15           # imm = 0x1FFE0
	je	.LBB0_253
# BB#248:                               # %vector.memcheck
                                        #   in Loop: Header=BB0_16 Depth=2
	leal	-1(%rdi), %ebx
	movzwl	%bx, %edx
	movq	%rdx, %rbx
	subq	%rbp, %rbx
	leaq	1(%r13,%rbx), %rbp
	cmpq	%rbp, %r13
	jae	.LBB0_250
# BB#249:                               # %vector.memcheck
                                        #   in Loop: Header=BB0_16 Depth=2
	leaq	1(%r13,%rdx), %rdx
	cmpq	%rdx, %rcx
	movq	%r13, %rcx
	jb	.LBB0_260
.LBB0_250:                              # %vector.body.preheader
                                        #   in Loop: Header=BB0_16 Depth=2
	movq	%r11, %rdx
	leaq	-32(%r15), %rbx
	movl	%ebx, %ecx
	shrl	$5, %ecx
	incl	%ecx
	andq	$3, %rcx
	je	.LBB0_254
# BB#251:                               # %vector.body.prol.preheader
                                        #   in Loop: Header=BB0_16 Depth=2
	leaq	16(%r13), %r11
	leaq	16(%r13,%rax), %r12
	negq	%rcx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_252:                              # %vector.body.prol
                                        #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-16(%r12,%rbp), %xmm0
	movups	(%r12,%rbp), %xmm1
	movups	%xmm0, -16(%r11,%rbp)
	movups	%xmm1, (%r11,%rbp)
	addq	$32, %rbp
	incq	%rcx
	jne	.LBB0_252
	jmp	.LBB0_255
.LBB0_253:                              #   in Loop: Header=BB0_16 Depth=2
	movq	%r13, %rcx
	jmp	.LBB0_260
.LBB0_254:                              #   in Loop: Header=BB0_16 Depth=2
	xorl	%ebp, %ebp
.LBB0_255:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB0_16 Depth=2
	cmpq	$96, %rbx
	jb	.LBB0_258
# BB#256:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_16 Depth=2
	movq	%r15, %rcx
	subq	%rbp, %rcx
	leaq	112(%r13,%rbp), %rbp
	.p2align	4, 0x90
.LBB0_257:                              # %vector.body
                                        #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-112(%rax,%rbp), %xmm0
	movups	-96(%rax,%rbp), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rax,%rbp), %xmm0
	movups	-64(%rax,%rbp), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rax,%rbp), %xmm0
	movups	-32(%rax,%rbp), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rax,%rbp), %xmm0
	movups	(%rax,%rbp), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rbp
	addq	$-128, %rcx
	jne	.LBB0_257
.LBB0_258:                              # %middle.block
                                        #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r10
	movq	%rdx, %r11
	je	.LBB0_15
# BB#259:                               #   in Loop: Header=BB0_16 Depth=2
	subl	%r15d, %edi
	addq	%r13, %r15
	movq	%r15, %rcx
.LBB0_260:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_16 Depth=2
	leal	-1(%rdi), %edx
	movw	%di, %bp
	andw	$7, %bp
	je	.LBB0_263
# BB#261:                               # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB0_16 Depth=2
	negl	%ebp
	.p2align	4, 0x90
.LBB0_262:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	decl	%edi
	movzbl	(%rax,%rcx), %ebx
	movb	%bl, (%rcx)
	incq	%rcx
	incw	%bp
	jne	.LBB0_262
.LBB0_263:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB0_16 Depth=2
	movzwl	%dx, %edx
	cmpl	$7, %edx
	jb	.LBB0_15
# BB#264:                               # %.lr.ph.i.preheader.new
                                        #   in Loop: Header=BB0_16 Depth=2
	addq	$7, %rcx
	.p2align	4, 0x90
.LBB0_265:                              # %.lr.ph.i
                                        #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	-7(%rax,%rcx), %edx
	movb	%dl, -7(%rcx)
	movzbl	-6(%rax,%rcx), %edx
	movb	%dl, -6(%rcx)
	movzbl	-5(%rax,%rcx), %edx
	movb	%dl, -5(%rcx)
	movzbl	-4(%rax,%rcx), %edx
	movb	%dl, -4(%rcx)
	movzbl	-3(%rax,%rcx), %edx
	movb	%dl, -3(%rcx)
	movzbl	-2(%rax,%rcx), %edx
	movb	%dl, -2(%rcx)
	movzbl	-1(%rax,%rcx), %edx
	movb	%dl, -1(%rcx)
	addw	$-8, %di
	movzbl	(%rax,%rcx), %edx
	movb	%dl, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB0_265
	jmp	.LBB0_15
.LBB0_266:                              #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_271
# BB#267:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#268:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rdx
	cmpq	%r15, %rdx
	ja	.LBB0_282
# BB#269:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rdx            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#270:                               # %getbitmap.exit41.i.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %esi
	movb	$32, %r9b
	jmp	.LBB0_236
.LBB0_271:                              #   in Loop: Header=BB0_16 Depth=2
	movb	$14, %al
	subb	%r9b, %al
	movl	%eax, %ecx
	shrl	%cl, %edi
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jb	.LBB0_282
# BB#272:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rdx
	cmpq	%r15, %rdx
	ja	.LBB0_282
# BB#273:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	(%rsp), %rdx            # 8-byte Folded Reload
	jbe	.LBB0_282
# BB#274:                               #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %ebp
	movl	(%r8), %ebx
	movl	%eax, %ecx
	shldl	%cl, %ebx, %edi
	movb	$32, %r9b
	movq	%rdx, %r8
.LBB0_275:                              # %.sink.split.i.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%ebp, %ecx
	shll	%cl, %ebx
	subb	%al, %r9b
	movq	%r8, %rdx
	jmp	.LBB0_235
.LBB0_276:                              # %wunpsect.exit
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
	cmpl	$511, %ebx              # imm = 0x1FF
	jne	.LBB0_283
# BB#277:                               #   in Loop: Header=BB0_1 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	cmpb	$0, 16(%rax)
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	32(%rsp), %r8d          # 4-byte Reload
	movl	184(%rsp), %ebp
	movq	176(%rsp), %rdx
	movl	36(%rsp), %r12d         # 4-byte Reload
	movq	56(%rsp), %r14          # 8-byte Reload
	jne	.LBB0_1
# BB#278:
	movl	44(%rsp), %esi          # 4-byte Reload
	movw	192(%rsp), %ax
	movb	%al, 6(%rcx,%rsi)
	movb	%ah, 7(%rcx,%rsi)  # NOREX
	movq	%rax, %r12
	movq	%rsi, %r15
	movl	661(%rdx), %eax
	movq	48(%rsp), %rcx          # 8-byte Reload
	leal	665(%rcx,%rax), %r14d
	xorl	%ebx, %ebx
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	cli_dbgmsg
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%r14d, 40(%rsi,%r15)
	xorl	%eax, %eax
	testw	$4095, %bp              # imm = 0xFFF
	setne	%al
	shll	$12, %eax
	addl	%ebp, %eax
	andl	$-4096, %eax            # imm = 0xF000
	subl	%eax, 80(%rsi,%r15)
	movq	%r15, %rbp
	movzwl	20(%rsi,%r15), %eax
	movq	%r12, %rdx
	testw	%r12w, %r12w
	je	.LBB0_292
# BB#279:                               # %.lr.ph
	leaq	24(%rax), %rcx
	movl	40(%rsp), %edi          # 4-byte Reload
	subl	32(%rsp), %edi          # 4-byte Folded Reload
	movq	%rdx, %r8
	leal	-1(%r8), %edx
	movzwl	%dx, %edx
	leaq	(%rdx,%rdx,4), %rdx
	addq	%rbp, %rcx
	leaq	(%rbp,%rdx,8), %rdx
	leaq	64(%rax,%rdx), %rax
	leaq	8(%rsi,%rcx), %rcx
	movq	%r8, %rbp
.LBB0_280:                              # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	movl	8(%rcx), %esi
	cmpl	%edx, %esi
	cmoval	%esi, %edx
	xorl	%esi, %esi
	testw	$4095, %dx              # imm = 0xFFF
	setne	%sil
	shll	$12, %esi
	addl	%edx, %esi
	andl	$-4096, %esi            # imm = 0xF000
	movl	%esi, (%rcx)
	movl	%esi, 8(%rcx)
	movl	4(%rcx), %edx
	addl	%edi, %edx
	decw	%bp
	movl	%edx, 12(%rcx)
	leaq	40(%rcx), %rcx
	jne	.LBB0_280
# BB#281:                               # %._crit_edge.loopexit
	movq	16(%rsp), %rsi          # 8-byte Reload
	addq	%rax, %rsi
	jmp	.LBB0_293
.LBB0_282:                              # %wunpsect.exit.thread
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
.LBB0_283:
	movl	$.L.str.5, %edi
.LBB0_284:
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_285:
	movl	$1, %ebx
.LBB0_286:
	movl	%ebx, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_287:
	movl	$.L.str.1, %edi
	jmp	.LBB0_284
.LBB0_288:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
	movl	$.L.str.4, %edi
	jmp	.LBB0_284
.LBB0_289:
	movl	$.L.str.2, %edi
	jmp	.LBB0_284
.LBB0_290:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_dbgmsg
	jmp	.LBB0_285
.LBB0_292:
	addq	%rbp, %rsi
	leaq	24(%rax,%rsi), %rsi
.LBB0_293:                              # %._crit_edge
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rsi)
	movups	%xmm0, (%rsi)
	movq	$0, 32(%rsi)
	jmp	.LBB0_286
.Lfunc_end0:
	.size	wwunpack, .Lfunc_end0-wwunpack
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"in wwunpack\n"
	.size	.L.str, 13

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"WWPack: next chunk out ouf file, giving up.\n"
	.size	.L.str.1, 45

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"WWPack: inconsistent/hacked data, go figure!\n"
	.size	.L.str.2, 46

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"WWPack: Can't allocate %d bytes\n"
	.size	.L.str.3, 33

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"WWPack: packed data out of bounds, giving up.\n"
	.size	.L.str.4, 47

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"WWPack: unpacking failed.\n"
	.size	.L.str.5, 27

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"WWPack: found OEP @%x\n"
	.size	.L.str.6, 23


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
