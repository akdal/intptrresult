	.text
	.file	"DeltaFilter.bc"
	.globl	_ZN13CDeltaEncoder4InitEv
	.p2align	4, 0x90
	.type	_ZN13CDeltaEncoder4InitEv,@function
_ZN13CDeltaEncoder4InitEv:              # @_ZN13CDeltaEncoder4InitEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	addq	$28, %rdi
	callq	Delta_Init
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end0:
	.size	_ZN13CDeltaEncoder4InitEv, .Lfunc_end0-_ZN13CDeltaEncoder4InitEv
	.cfi_endproc

	.globl	_ZN13CDeltaEncoder6FilterEPhj
	.p2align	4, 0x90
	.type	_ZN13CDeltaEncoder6FilterEPhj,@function
_ZN13CDeltaEncoder6FilterEPhj:          # @_ZN13CDeltaEncoder6FilterEPhj
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 16
.Lcfi2:
	.cfi_offset %rbx, -16
	movl	%edx, %ebx
	movq	%rsi, %rax
	movl	24(%rdi), %esi
	addq	$28, %rdi
	movl	%ebx, %ecx
	movq	%rax, %rdx
	callq	Delta_Encode
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN13CDeltaEncoder6FilterEPhj, .Lfunc_end1-_ZN13CDeltaEncoder6FilterEPhj
	.cfi_endproc

	.globl	_ZN13CDeltaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZN13CDeltaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj,@function
_ZN13CDeltaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj: # @_ZN13CDeltaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_startproc
# BB#0:
	testl	%ecx, %ecx
	je	.LBB2_1
# BB#3:                                 # %.lr.ph.preheader
	addq	$8, %rdx
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$0, (%rsi,%r8,4)
	jne	.LBB2_8
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=1
	movzwl	-8(%rdx), %r9d
	cmpl	$19, %r9d
	jne	.LBB2_8
# BB#7:                                 #   in Loop: Header=BB2_5 Depth=1
	movl	(%rdx), %r9d
	leal	-1(%r9), %r10d
	cmpl	$255, %r10d
	ja	.LBB2_8
# BB#4:                                 #   in Loop: Header=BB2_5 Depth=1
	addq	$16, %rdx
	incq	%r8
	cmpl	%ecx, %r8d
	jb	.LBB2_5
	jmp	.LBB2_2
.LBB2_8:                                # %select.unfold
	retq
.LBB2_1:
	movl	24(%rdi), %r9d
.LBB2_2:                                # %._crit_edge
	movl	%r9d, 24(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	_ZN13CDeltaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj, .Lfunc_end2-_ZN13CDeltaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_endproc

	.globl	_ZThn8_N13CDeltaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZThn8_N13CDeltaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj,@function
_ZThn8_N13CDeltaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj: # @_ZThn8_N13CDeltaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_startproc
# BB#0:
	testl	%ecx, %ecx
	je	.LBB3_1
# BB#3:                                 # %.lr.ph.i.preheader
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%r8d, %r8d
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$0, (%rsi,%r8,4)
	jne	.LBB3_8
# BB#6:                                 #   in Loop: Header=BB3_5 Depth=1
	movq	%r8, %r9
	shlq	$4, %r9
	movzwl	(%rdx,%r9), %r10d
	cmpl	$19, %r10d
	jne	.LBB3_8
# BB#7:                                 #   in Loop: Header=BB3_5 Depth=1
	movl	8(%rdx,%r9), %r9d
	leal	-1(%r9), %r10d
	cmpl	$255, %r10d
	ja	.LBB3_8
# BB#4:                                 #   in Loop: Header=BB3_5 Depth=1
	incl	%r8d
	cmpl	%ecx, %r8d
	jb	.LBB3_5
	jmp	.LBB3_2
.LBB3_8:                                # %_ZN13CDeltaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj.exit
	retq
.LBB3_1:
	movl	16(%rdi), %r9d
.LBB3_2:                                # %._crit_edge.i
	movl	%r9d, 16(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	_ZThn8_N13CDeltaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj, .Lfunc_end3-_ZThn8_N13CDeltaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_endproc

	.globl	_ZN13CDeltaEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN13CDeltaEncoder20WriteCoderPropertiesEP20ISequentialOutStream,@function
_ZN13CDeltaEncoder20WriteCoderPropertiesEP20ISequentialOutStream: # @_ZN13CDeltaEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 16
	movl	$255, %eax
	addl	24(%rdi), %eax
	movb	%al, 7(%rsp)
	movq	(%rsi), %rax
	leaq	7(%rsp), %r8
	movl	$1, %edx
	xorl	%ecx, %ecx
	movq	%rsi, %rdi
	movq	%r8, %rsi
	callq	*40(%rax)
	popq	%rcx
	retq
.Lfunc_end4:
	.size	_ZN13CDeltaEncoder20WriteCoderPropertiesEP20ISequentialOutStream, .Lfunc_end4-_ZN13CDeltaEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZThn16_N13CDeltaEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZThn16_N13CDeltaEncoder20WriteCoderPropertiesEP20ISequentialOutStream,@function
_ZThn16_N13CDeltaEncoder20WriteCoderPropertiesEP20ISequentialOutStream: # @_ZThn16_N13CDeltaEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 16
	movl	$255, %eax
	addl	8(%rdi), %eax
	movb	%al, 7(%rsp)
	movq	(%rsi), %rax
	leaq	7(%rsp), %r8
	movl	$1, %edx
	xorl	%ecx, %ecx
	movq	%rsi, %rdi
	movq	%r8, %rsi
	callq	*40(%rax)
	popq	%rcx
	retq
.Lfunc_end5:
	.size	_ZThn16_N13CDeltaEncoder20WriteCoderPropertiesEP20ISequentialOutStream, .Lfunc_end5-_ZThn16_N13CDeltaEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZN13CDeltaDecoder4InitEv
	.p2align	4, 0x90
	.type	_ZN13CDeltaDecoder4InitEv,@function
_ZN13CDeltaDecoder4InitEv:              # @_ZN13CDeltaDecoder4InitEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	addq	$20, %rdi
	callq	Delta_Init
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end6:
	.size	_ZN13CDeltaDecoder4InitEv, .Lfunc_end6-_ZN13CDeltaDecoder4InitEv
	.cfi_endproc

	.globl	_ZN13CDeltaDecoder6FilterEPhj
	.p2align	4, 0x90
	.type	_ZN13CDeltaDecoder6FilterEPhj,@function
_ZN13CDeltaDecoder6FilterEPhj:          # @_ZN13CDeltaDecoder6FilterEPhj
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movl	%edx, %ebx
	movq	%rsi, %rax
	movl	16(%rdi), %esi
	addq	$20, %rdi
	movl	%ebx, %ecx
	movq	%rax, %rdx
	callq	Delta_Decode
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end7:
	.size	_ZN13CDeltaDecoder6FilterEPhj, .Lfunc_end7-_ZN13CDeltaDecoder6FilterEPhj
	.cfi_endproc

	.globl	_ZN13CDeltaDecoder21SetDecoderProperties2EPKhj
	.p2align	4, 0x90
	.type	_ZN13CDeltaDecoder21SetDecoderProperties2EPKhj,@function
_ZN13CDeltaDecoder21SetDecoderProperties2EPKhj: # @_ZN13CDeltaDecoder21SetDecoderProperties2EPKhj
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$1, %edx
	jne	.LBB8_2
# BB#1:
	movzbl	(%rsi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	xorl	%eax, %eax
.LBB8_2:
	retq
.Lfunc_end8:
	.size	_ZN13CDeltaDecoder21SetDecoderProperties2EPKhj, .Lfunc_end8-_ZN13CDeltaDecoder21SetDecoderProperties2EPKhj
	.cfi_endproc

	.globl	_ZThn8_N13CDeltaDecoder21SetDecoderProperties2EPKhj
	.p2align	4, 0x90
	.type	_ZThn8_N13CDeltaDecoder21SetDecoderProperties2EPKhj,@function
_ZThn8_N13CDeltaDecoder21SetDecoderProperties2EPKhj: # @_ZThn8_N13CDeltaDecoder21SetDecoderProperties2EPKhj
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$1, %edx
	jne	.LBB9_2
# BB#1:
	movzbl	(%rsi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	xorl	%eax, %eax
.LBB9_2:                                # %_ZN13CDeltaDecoder21SetDecoderProperties2EPKhj.exit
	retq
.Lfunc_end9:
	.size	_ZThn8_N13CDeltaDecoder21SetDecoderProperties2EPKhj, .Lfunc_end9-_ZThn8_N13CDeltaDecoder21SetDecoderProperties2EPKhj
	.cfi_endproc

	.section	.text._ZN13CDeltaEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN13CDeltaEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN13CDeltaEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN13CDeltaEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZN13CDeltaEncoder14QueryInterfaceERK4GUIDPPv: # @_ZN13CDeltaEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB10_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB10_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB10_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB10_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB10_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB10_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB10_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB10_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB10_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB10_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB10_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB10_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB10_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB10_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB10_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB10_16
.LBB10_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ICompressSetCoderProperties(%rip), %cl
	jne	.LBB10_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+1(%rip), %al
	jne	.LBB10_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+2(%rip), %al
	jne	.LBB10_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+3(%rip), %al
	jne	.LBB10_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+4(%rip), %al
	jne	.LBB10_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+5(%rip), %al
	jne	.LBB10_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+6(%rip), %al
	jne	.LBB10_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+7(%rip), %al
	jne	.LBB10_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+8(%rip), %al
	jne	.LBB10_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+9(%rip), %al
	jne	.LBB10_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+10(%rip), %al
	jne	.LBB10_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+11(%rip), %al
	jne	.LBB10_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+12(%rip), %al
	jne	.LBB10_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+13(%rip), %al
	jne	.LBB10_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+14(%rip), %al
	jne	.LBB10_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit8
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+15(%rip), %al
	jne	.LBB10_33
.LBB10_16:
	leaq	8(%rdi), %rax
	jmp	.LBB10_50
.LBB10_33:                              # %_ZeqRK4GUIDS1_.exit8.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressWriteCoderProperties(%rip), %cl
	jne	.LBB10_51
# BB#34:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+1(%rip), %cl
	jne	.LBB10_51
# BB#35:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+2(%rip), %cl
	jne	.LBB10_51
# BB#36:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+3(%rip), %cl
	jne	.LBB10_51
# BB#37:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+4(%rip), %cl
	jne	.LBB10_51
# BB#38:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+5(%rip), %cl
	jne	.LBB10_51
# BB#39:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+6(%rip), %cl
	jne	.LBB10_51
# BB#40:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+7(%rip), %cl
	jne	.LBB10_51
# BB#41:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+8(%rip), %cl
	jne	.LBB10_51
# BB#42:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+9(%rip), %cl
	jne	.LBB10_51
# BB#43:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+10(%rip), %cl
	jne	.LBB10_51
# BB#44:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+11(%rip), %cl
	jne	.LBB10_51
# BB#45:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+12(%rip), %cl
	jne	.LBB10_51
# BB#46:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+13(%rip), %cl
	jne	.LBB10_51
# BB#47:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+14(%rip), %cl
	jne	.LBB10_51
# BB#48:                                # %_ZeqRK4GUIDS1_.exit10
	movb	15(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+15(%rip), %cl
	jne	.LBB10_51
# BB#49:
	leaq	16(%rdi), %rax
.LBB10_50:                              # %_ZeqRK4GUIDS1_.exit10.thread
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB10_51:                              # %_ZeqRK4GUIDS1_.exit10.thread
	popq	%rcx
	retq
.Lfunc_end10:
	.size	_ZN13CDeltaEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end10-_ZN13CDeltaEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN13CDeltaEncoder6AddRefEv,"axG",@progbits,_ZN13CDeltaEncoder6AddRefEv,comdat
	.weak	_ZN13CDeltaEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN13CDeltaEncoder6AddRefEv,@function
_ZN13CDeltaEncoder6AddRefEv:            # @_ZN13CDeltaEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	284(%rdi), %eax
	incl	%eax
	movl	%eax, 284(%rdi)
	retq
.Lfunc_end11:
	.size	_ZN13CDeltaEncoder6AddRefEv, .Lfunc_end11-_ZN13CDeltaEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN13CDeltaEncoder7ReleaseEv,"axG",@progbits,_ZN13CDeltaEncoder7ReleaseEv,comdat
	.weak	_ZN13CDeltaEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN13CDeltaEncoder7ReleaseEv,@function
_ZN13CDeltaEncoder7ReleaseEv:           # @_ZN13CDeltaEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 16
	movl	284(%rdi), %eax
	decl	%eax
	movl	%eax, 284(%rdi)
	jne	.LBB12_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB12_2:
	popq	%rcx
	retq
.Lfunc_end12:
	.size	_ZN13CDeltaEncoder7ReleaseEv, .Lfunc_end12-_ZN13CDeltaEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN13CDeltaEncoderD2Ev,"axG",@progbits,_ZN13CDeltaEncoderD2Ev,comdat
	.weak	_ZN13CDeltaEncoderD2Ev
	.p2align	4, 0x90
	.type	_ZN13CDeltaEncoderD2Ev,@function
_ZN13CDeltaEncoderD2Ev:                 # @_ZN13CDeltaEncoderD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end13:
	.size	_ZN13CDeltaEncoderD2Ev, .Lfunc_end13-_ZN13CDeltaEncoderD2Ev
	.cfi_endproc

	.section	.text._ZN13CDeltaEncoderD0Ev,"axG",@progbits,_ZN13CDeltaEncoderD0Ev,comdat
	.weak	_ZN13CDeltaEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZN13CDeltaEncoderD0Ev,@function
_ZN13CDeltaEncoderD0Ev:                 # @_ZN13CDeltaEncoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end14:
	.size	_ZN13CDeltaEncoderD0Ev, .Lfunc_end14-_ZN13CDeltaEncoderD0Ev
	.cfi_endproc

	.section	.text._ZThn8_N13CDeltaEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N13CDeltaEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N13CDeltaEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N13CDeltaEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N13CDeltaEncoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N13CDeltaEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN13CDeltaEncoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end15:
	.size	_ZThn8_N13CDeltaEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end15-_ZThn8_N13CDeltaEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N13CDeltaEncoder6AddRefEv,"axG",@progbits,_ZThn8_N13CDeltaEncoder6AddRefEv,comdat
	.weak	_ZThn8_N13CDeltaEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N13CDeltaEncoder6AddRefEv,@function
_ZThn8_N13CDeltaEncoder6AddRefEv:       # @_ZThn8_N13CDeltaEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	276(%rdi), %eax
	incl	%eax
	movl	%eax, 276(%rdi)
	retq
.Lfunc_end16:
	.size	_ZThn8_N13CDeltaEncoder6AddRefEv, .Lfunc_end16-_ZThn8_N13CDeltaEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N13CDeltaEncoder7ReleaseEv,"axG",@progbits,_ZThn8_N13CDeltaEncoder7ReleaseEv,comdat
	.weak	_ZThn8_N13CDeltaEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N13CDeltaEncoder7ReleaseEv,@function
_ZThn8_N13CDeltaEncoder7ReleaseEv:      # @_ZThn8_N13CDeltaEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 16
	movl	276(%rdi), %eax
	decl	%eax
	movl	%eax, 276(%rdi)
	jne	.LBB17_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB17_2:                               # %_ZN13CDeltaEncoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end17:
	.size	_ZThn8_N13CDeltaEncoder7ReleaseEv, .Lfunc_end17-_ZThn8_N13CDeltaEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N13CDeltaEncoderD1Ev,"axG",@progbits,_ZThn8_N13CDeltaEncoderD1Ev,comdat
	.weak	_ZThn8_N13CDeltaEncoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N13CDeltaEncoderD1Ev,@function
_ZThn8_N13CDeltaEncoderD1Ev:            # @_ZThn8_N13CDeltaEncoderD1Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end18:
	.size	_ZThn8_N13CDeltaEncoderD1Ev, .Lfunc_end18-_ZThn8_N13CDeltaEncoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N13CDeltaEncoderD0Ev,"axG",@progbits,_ZThn8_N13CDeltaEncoderD0Ev,comdat
	.weak	_ZThn8_N13CDeltaEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N13CDeltaEncoderD0Ev,@function
_ZThn8_N13CDeltaEncoderD0Ev:            # @_ZThn8_N13CDeltaEncoderD0Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end19:
	.size	_ZThn8_N13CDeltaEncoderD0Ev, .Lfunc_end19-_ZThn8_N13CDeltaEncoderD0Ev
	.cfi_endproc

	.section	.text._ZThn16_N13CDeltaEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N13CDeltaEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N13CDeltaEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N13CDeltaEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N13CDeltaEncoder14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N13CDeltaEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN13CDeltaEncoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end20:
	.size	_ZThn16_N13CDeltaEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end20-_ZThn16_N13CDeltaEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N13CDeltaEncoder6AddRefEv,"axG",@progbits,_ZThn16_N13CDeltaEncoder6AddRefEv,comdat
	.weak	_ZThn16_N13CDeltaEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N13CDeltaEncoder6AddRefEv,@function
_ZThn16_N13CDeltaEncoder6AddRefEv:      # @_ZThn16_N13CDeltaEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	268(%rdi), %eax
	incl	%eax
	movl	%eax, 268(%rdi)
	retq
.Lfunc_end21:
	.size	_ZThn16_N13CDeltaEncoder6AddRefEv, .Lfunc_end21-_ZThn16_N13CDeltaEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N13CDeltaEncoder7ReleaseEv,"axG",@progbits,_ZThn16_N13CDeltaEncoder7ReleaseEv,comdat
	.weak	_ZThn16_N13CDeltaEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N13CDeltaEncoder7ReleaseEv,@function
_ZThn16_N13CDeltaEncoder7ReleaseEv:     # @_ZThn16_N13CDeltaEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 16
	movl	268(%rdi), %eax
	decl	%eax
	movl	%eax, 268(%rdi)
	jne	.LBB22_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB22_2:                               # %_ZN13CDeltaEncoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end22:
	.size	_ZThn16_N13CDeltaEncoder7ReleaseEv, .Lfunc_end22-_ZThn16_N13CDeltaEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N13CDeltaEncoderD1Ev,"axG",@progbits,_ZThn16_N13CDeltaEncoderD1Ev,comdat
	.weak	_ZThn16_N13CDeltaEncoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N13CDeltaEncoderD1Ev,@function
_ZThn16_N13CDeltaEncoderD1Ev:           # @_ZThn16_N13CDeltaEncoderD1Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end23:
	.size	_ZThn16_N13CDeltaEncoderD1Ev, .Lfunc_end23-_ZThn16_N13CDeltaEncoderD1Ev
	.cfi_endproc

	.section	.text._ZThn16_N13CDeltaEncoderD0Ev,"axG",@progbits,_ZThn16_N13CDeltaEncoderD0Ev,comdat
	.weak	_ZThn16_N13CDeltaEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N13CDeltaEncoderD0Ev,@function
_ZThn16_N13CDeltaEncoderD0Ev:           # @_ZThn16_N13CDeltaEncoderD0Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end24:
	.size	_ZThn16_N13CDeltaEncoderD0Ev, .Lfunc_end24-_ZThn16_N13CDeltaEncoderD0Ev
	.cfi_endproc

	.section	.text._ZN13CDeltaDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN13CDeltaDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN13CDeltaDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN13CDeltaDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZN13CDeltaDecoder14QueryInterfaceERK4GUIDPPv: # @_ZN13CDeltaDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB25_3
# BB#1:
	movl	$IID_ICompressSetDecoderProperties2, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB25_2
.LBB25_3:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB25_4:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB25_2:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB25_4
.Lfunc_end25:
	.size	_ZN13CDeltaDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end25-_ZN13CDeltaDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN13CDeltaDecoder6AddRefEv,"axG",@progbits,_ZN13CDeltaDecoder6AddRefEv,comdat
	.weak	_ZN13CDeltaDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN13CDeltaDecoder6AddRefEv,@function
_ZN13CDeltaDecoder6AddRefEv:            # @_ZN13CDeltaDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	276(%rdi), %eax
	incl	%eax
	movl	%eax, 276(%rdi)
	retq
.Lfunc_end26:
	.size	_ZN13CDeltaDecoder6AddRefEv, .Lfunc_end26-_ZN13CDeltaDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN13CDeltaDecoder7ReleaseEv,"axG",@progbits,_ZN13CDeltaDecoder7ReleaseEv,comdat
	.weak	_ZN13CDeltaDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN13CDeltaDecoder7ReleaseEv,@function
_ZN13CDeltaDecoder7ReleaseEv:           # @_ZN13CDeltaDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 16
	movl	276(%rdi), %eax
	decl	%eax
	movl	%eax, 276(%rdi)
	jne	.LBB27_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB27_2:
	popq	%rcx
	retq
.Lfunc_end27:
	.size	_ZN13CDeltaDecoder7ReleaseEv, .Lfunc_end27-_ZN13CDeltaDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN13CDeltaDecoderD2Ev,"axG",@progbits,_ZN13CDeltaDecoderD2Ev,comdat
	.weak	_ZN13CDeltaDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN13CDeltaDecoderD2Ev,@function
_ZN13CDeltaDecoderD2Ev:                 # @_ZN13CDeltaDecoderD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end28:
	.size	_ZN13CDeltaDecoderD2Ev, .Lfunc_end28-_ZN13CDeltaDecoderD2Ev
	.cfi_endproc

	.section	.text._ZN13CDeltaDecoderD0Ev,"axG",@progbits,_ZN13CDeltaDecoderD0Ev,comdat
	.weak	_ZN13CDeltaDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN13CDeltaDecoderD0Ev,@function
_ZN13CDeltaDecoderD0Ev:                 # @_ZN13CDeltaDecoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end29:
	.size	_ZN13CDeltaDecoderD0Ev, .Lfunc_end29-_ZN13CDeltaDecoderD0Ev
	.cfi_endproc

	.section	.text._ZThn8_N13CDeltaDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N13CDeltaDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N13CDeltaDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N13CDeltaDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N13CDeltaDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N13CDeltaDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 16
	movq	%rdi, %r8
	leaq	-8(%r8), %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB30_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB30_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB30_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB30_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB30_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB30_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB30_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB30_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB30_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB30_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB30_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB30_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB30_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB30_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB30_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB30_32
.LBB30_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressSetDecoderProperties2(%rip), %cl
	jne	.LBB30_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+1(%rip), %cl
	jne	.LBB30_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+2(%rip), %cl
	jne	.LBB30_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+3(%rip), %cl
	jne	.LBB30_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+4(%rip), %cl
	jne	.LBB30_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+5(%rip), %cl
	jne	.LBB30_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+6(%rip), %cl
	jne	.LBB30_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+7(%rip), %cl
	jne	.LBB30_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+8(%rip), %cl
	jne	.LBB30_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+9(%rip), %cl
	jne	.LBB30_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+10(%rip), %cl
	jne	.LBB30_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+11(%rip), %cl
	jne	.LBB30_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+12(%rip), %cl
	jne	.LBB30_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+13(%rip), %cl
	jne	.LBB30_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+14(%rip), %cl
	jne	.LBB30_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+15(%rip), %cl
	jne	.LBB30_33
.LBB30_32:
	movq	%r8, (%rdx)
	movq	-8(%r8), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB30_33:                              # %_ZN13CDeltaDecoder14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end30:
	.size	_ZThn8_N13CDeltaDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end30-_ZThn8_N13CDeltaDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N13CDeltaDecoder6AddRefEv,"axG",@progbits,_ZThn8_N13CDeltaDecoder6AddRefEv,comdat
	.weak	_ZThn8_N13CDeltaDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N13CDeltaDecoder6AddRefEv,@function
_ZThn8_N13CDeltaDecoder6AddRefEv:       # @_ZThn8_N13CDeltaDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	268(%rdi), %eax
	incl	%eax
	movl	%eax, 268(%rdi)
	retq
.Lfunc_end31:
	.size	_ZThn8_N13CDeltaDecoder6AddRefEv, .Lfunc_end31-_ZThn8_N13CDeltaDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N13CDeltaDecoder7ReleaseEv,"axG",@progbits,_ZThn8_N13CDeltaDecoder7ReleaseEv,comdat
	.weak	_ZThn8_N13CDeltaDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N13CDeltaDecoder7ReleaseEv,@function
_ZThn8_N13CDeltaDecoder7ReleaseEv:      # @_ZThn8_N13CDeltaDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 16
	movl	268(%rdi), %eax
	decl	%eax
	movl	%eax, 268(%rdi)
	jne	.LBB32_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB32_2:                               # %_ZN13CDeltaDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end32:
	.size	_ZThn8_N13CDeltaDecoder7ReleaseEv, .Lfunc_end32-_ZThn8_N13CDeltaDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N13CDeltaDecoderD1Ev,"axG",@progbits,_ZThn8_N13CDeltaDecoderD1Ev,comdat
	.weak	_ZThn8_N13CDeltaDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N13CDeltaDecoderD1Ev,@function
_ZThn8_N13CDeltaDecoderD1Ev:            # @_ZThn8_N13CDeltaDecoderD1Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end33:
	.size	_ZThn8_N13CDeltaDecoderD1Ev, .Lfunc_end33-_ZThn8_N13CDeltaDecoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N13CDeltaDecoderD0Ev,"axG",@progbits,_ZThn8_N13CDeltaDecoderD0Ev,comdat
	.weak	_ZThn8_N13CDeltaDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N13CDeltaDecoderD0Ev,@function
_ZThn8_N13CDeltaDecoderD0Ev:            # @_ZThn8_N13CDeltaDecoderD0Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end34:
	.size	_ZThn8_N13CDeltaDecoderD0Ev, .Lfunc_end34-_ZThn8_N13CDeltaDecoderD0Ev
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZL16CreateCodecDeltav,@function
_ZL16CreateCodecDeltav:                 # @_ZL16CreateCodecDeltav
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 16
	movl	$280, %edi              # imm = 0x118
	callq	_Znwm
	movl	$1, 16(%rax)
	movl	$0, 276(%rax)
	movl	$_ZTV13CDeltaDecoder+96, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTV13CDeltaDecoder+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rax)
	popq	%rcx
	retq
.Lfunc_end35:
	.size	_ZL16CreateCodecDeltav, .Lfunc_end35-_ZL16CreateCodecDeltav
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL19CreateCodecDeltaOutv,@function
_ZL19CreateCodecDeltaOutv:              # @_ZL19CreateCodecDeltaOutv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 16
	movl	$288, %edi              # imm = 0x120
	callq	_Znwm
	movl	$1, 24(%rax)
	movl	$0, 284(%rax)
	movl	$_ZTV13CDeltaEncoder+104, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTV13CDeltaEncoder+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rax)
	movq	$_ZTV13CDeltaEncoder+168, 16(%rax)
	popq	%rcx
	retq
.Lfunc_end36:
	.size	_ZL19CreateCodecDeltaOutv, .Lfunc_end36-_ZL19CreateCodecDeltaOutv
	.cfi_endproc

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB37_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB37_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB37_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB37_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB37_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB37_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB37_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB37_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB37_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB37_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB37_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB37_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB37_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB37_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB37_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB37_16:
	xorl	%eax, %eax
	retq
.Lfunc_end37:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end37-_ZeqRK4GUIDS1_
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_DeltaFilter.ii,@function
_GLOBAL__sub_I_DeltaFilter.ii:          # @_GLOBAL__sub_I_DeltaFilter.ii
	.cfi_startproc
# BB#0:
	movl	$_ZL12g_CodecsInfo, %edi
	jmp	_Z13RegisterCodecPK10CCodecInfo # TAILCALL
.Lfunc_end38:
	.size	_GLOBAL__sub_I_DeltaFilter.ii, .Lfunc_end38-_GLOBAL__sub_I_DeltaFilter.ii
	.cfi_endproc

	.type	_ZTV13CDeltaEncoder,@object # @_ZTV13CDeltaEncoder
	.section	.rodata,"a",@progbits
	.globl	_ZTV13CDeltaEncoder
	.p2align	3
_ZTV13CDeltaEncoder:
	.quad	0
	.quad	_ZTI13CDeltaEncoder
	.quad	_ZN13CDeltaEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN13CDeltaEncoder6AddRefEv
	.quad	_ZN13CDeltaEncoder7ReleaseEv
	.quad	_ZN13CDeltaEncoderD2Ev
	.quad	_ZN13CDeltaEncoderD0Ev
	.quad	_ZN13CDeltaEncoder4InitEv
	.quad	_ZN13CDeltaEncoder6FilterEPhj
	.quad	_ZN13CDeltaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.quad	_ZN13CDeltaEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.quad	-8
	.quad	_ZTI13CDeltaEncoder
	.quad	_ZThn8_N13CDeltaEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N13CDeltaEncoder6AddRefEv
	.quad	_ZThn8_N13CDeltaEncoder7ReleaseEv
	.quad	_ZThn8_N13CDeltaEncoderD1Ev
	.quad	_ZThn8_N13CDeltaEncoderD0Ev
	.quad	_ZThn8_N13CDeltaEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.quad	-16
	.quad	_ZTI13CDeltaEncoder
	.quad	_ZThn16_N13CDeltaEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N13CDeltaEncoder6AddRefEv
	.quad	_ZThn16_N13CDeltaEncoder7ReleaseEv
	.quad	_ZThn16_N13CDeltaEncoderD1Ev
	.quad	_ZThn16_N13CDeltaEncoderD0Ev
	.quad	_ZThn16_N13CDeltaEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.size	_ZTV13CDeltaEncoder, 216

	.type	_ZTS13CDeltaEncoder,@object # @_ZTS13CDeltaEncoder
	.globl	_ZTS13CDeltaEncoder
_ZTS13CDeltaEncoder:
	.asciz	"13CDeltaEncoder"
	.size	_ZTS13CDeltaEncoder, 16

	.type	_ZTS15ICompressFilter,@object # @_ZTS15ICompressFilter
	.section	.rodata._ZTS15ICompressFilter,"aG",@progbits,_ZTS15ICompressFilter,comdat
	.weak	_ZTS15ICompressFilter
	.p2align	4
_ZTS15ICompressFilter:
	.asciz	"15ICompressFilter"
	.size	_ZTS15ICompressFilter, 18

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI15ICompressFilter,@object # @_ZTI15ICompressFilter
	.section	.rodata._ZTI15ICompressFilter,"aG",@progbits,_ZTI15ICompressFilter,comdat
	.weak	_ZTI15ICompressFilter
	.p2align	4
_ZTI15ICompressFilter:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15ICompressFilter
	.quad	_ZTI8IUnknown
	.size	_ZTI15ICompressFilter, 24

	.type	_ZTS27ICompressSetCoderProperties,@object # @_ZTS27ICompressSetCoderProperties
	.section	.rodata._ZTS27ICompressSetCoderProperties,"aG",@progbits,_ZTS27ICompressSetCoderProperties,comdat
	.weak	_ZTS27ICompressSetCoderProperties
	.p2align	4
_ZTS27ICompressSetCoderProperties:
	.asciz	"27ICompressSetCoderProperties"
	.size	_ZTS27ICompressSetCoderProperties, 30

	.type	_ZTI27ICompressSetCoderProperties,@object # @_ZTI27ICompressSetCoderProperties
	.section	.rodata._ZTI27ICompressSetCoderProperties,"aG",@progbits,_ZTI27ICompressSetCoderProperties,comdat
	.weak	_ZTI27ICompressSetCoderProperties
	.p2align	4
_ZTI27ICompressSetCoderProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS27ICompressSetCoderProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI27ICompressSetCoderProperties, 24

	.type	_ZTS29ICompressWriteCoderProperties,@object # @_ZTS29ICompressWriteCoderProperties
	.section	.rodata._ZTS29ICompressWriteCoderProperties,"aG",@progbits,_ZTS29ICompressWriteCoderProperties,comdat
	.weak	_ZTS29ICompressWriteCoderProperties
	.p2align	4
_ZTS29ICompressWriteCoderProperties:
	.asciz	"29ICompressWriteCoderProperties"
	.size	_ZTS29ICompressWriteCoderProperties, 32

	.type	_ZTI29ICompressWriteCoderProperties,@object # @_ZTI29ICompressWriteCoderProperties
	.section	.rodata._ZTI29ICompressWriteCoderProperties,"aG",@progbits,_ZTI29ICompressWriteCoderProperties,comdat
	.weak	_ZTI29ICompressWriteCoderProperties
	.p2align	4
_ZTI29ICompressWriteCoderProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS29ICompressWriteCoderProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI29ICompressWriteCoderProperties, 24

	.type	_ZTS6CDelta,@object     # @_ZTS6CDelta
	.section	.rodata._ZTS6CDelta,"aG",@progbits,_ZTS6CDelta,comdat
	.weak	_ZTS6CDelta
_ZTS6CDelta:
	.asciz	"6CDelta"
	.size	_ZTS6CDelta, 8

	.type	_ZTI6CDelta,@object     # @_ZTI6CDelta
	.section	.rodata._ZTI6CDelta,"aG",@progbits,_ZTI6CDelta,comdat
	.weak	_ZTI6CDelta
	.p2align	3
_ZTI6CDelta:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS6CDelta
	.size	_ZTI6CDelta, 16

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI13CDeltaEncoder,@object # @_ZTI13CDeltaEncoder
	.section	.rodata,"a",@progbits
	.globl	_ZTI13CDeltaEncoder
	.p2align	4
_ZTI13CDeltaEncoder:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS13CDeltaEncoder
	.long	1                       # 0x1
	.long	5                       # 0x5
	.quad	_ZTI15ICompressFilter
	.quad	2                       # 0x2
	.quad	_ZTI27ICompressSetCoderProperties
	.quad	2050                    # 0x802
	.quad	_ZTI29ICompressWriteCoderProperties
	.quad	4098                    # 0x1002
	.quad	_ZTI6CDelta
	.quad	6144                    # 0x1800
	.quad	_ZTI13CMyUnknownImp
	.quad	72706                   # 0x11c02
	.size	_ZTI13CDeltaEncoder, 104

	.type	_ZTV13CDeltaDecoder,@object # @_ZTV13CDeltaDecoder
	.globl	_ZTV13CDeltaDecoder
	.p2align	3
_ZTV13CDeltaDecoder:
	.quad	0
	.quad	_ZTI13CDeltaDecoder
	.quad	_ZN13CDeltaDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN13CDeltaDecoder6AddRefEv
	.quad	_ZN13CDeltaDecoder7ReleaseEv
	.quad	_ZN13CDeltaDecoderD2Ev
	.quad	_ZN13CDeltaDecoderD0Ev
	.quad	_ZN13CDeltaDecoder4InitEv
	.quad	_ZN13CDeltaDecoder6FilterEPhj
	.quad	_ZN13CDeltaDecoder21SetDecoderProperties2EPKhj
	.quad	-8
	.quad	_ZTI13CDeltaDecoder
	.quad	_ZThn8_N13CDeltaDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N13CDeltaDecoder6AddRefEv
	.quad	_ZThn8_N13CDeltaDecoder7ReleaseEv
	.quad	_ZThn8_N13CDeltaDecoderD1Ev
	.quad	_ZThn8_N13CDeltaDecoderD0Ev
	.quad	_ZThn8_N13CDeltaDecoder21SetDecoderProperties2EPKhj
	.size	_ZTV13CDeltaDecoder, 144

	.type	_ZTS13CDeltaDecoder,@object # @_ZTS13CDeltaDecoder
	.globl	_ZTS13CDeltaDecoder
_ZTS13CDeltaDecoder:
	.asciz	"13CDeltaDecoder"
	.size	_ZTS13CDeltaDecoder, 16

	.type	_ZTS30ICompressSetDecoderProperties2,@object # @_ZTS30ICompressSetDecoderProperties2
	.section	.rodata._ZTS30ICompressSetDecoderProperties2,"aG",@progbits,_ZTS30ICompressSetDecoderProperties2,comdat
	.weak	_ZTS30ICompressSetDecoderProperties2
	.p2align	4
_ZTS30ICompressSetDecoderProperties2:
	.asciz	"30ICompressSetDecoderProperties2"
	.size	_ZTS30ICompressSetDecoderProperties2, 33

	.type	_ZTI30ICompressSetDecoderProperties2,@object # @_ZTI30ICompressSetDecoderProperties2
	.section	.rodata._ZTI30ICompressSetDecoderProperties2,"aG",@progbits,_ZTI30ICompressSetDecoderProperties2,comdat
	.weak	_ZTI30ICompressSetDecoderProperties2
	.p2align	4
_ZTI30ICompressSetDecoderProperties2:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS30ICompressSetDecoderProperties2
	.quad	_ZTI8IUnknown
	.size	_ZTI30ICompressSetDecoderProperties2, 24

	.type	_ZTI13CDeltaDecoder,@object # @_ZTI13CDeltaDecoder
	.section	.rodata,"a",@progbits
	.globl	_ZTI13CDeltaDecoder
	.p2align	4
_ZTI13CDeltaDecoder:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS13CDeltaDecoder
	.long	1                       # 0x1
	.long	4                       # 0x4
	.quad	_ZTI15ICompressFilter
	.quad	2                       # 0x2
	.quad	_ZTI30ICompressSetDecoderProperties2
	.quad	2050                    # 0x802
	.quad	_ZTI6CDelta
	.quad	4096                    # 0x1000
	.quad	_ZTI13CMyUnknownImp
	.quad	70658                   # 0x11402
	.size	_ZTI13CDeltaDecoder, 88

	.type	_ZL12g_CodecsInfo,@object # @_ZL12g_CodecsInfo
	.data
	.p2align	4
_ZL12g_CodecsInfo:
	.quad	_ZL16CreateCodecDeltav
	.quad	_ZL19CreateCodecDeltaOutv
	.quad	3                       # 0x3
	.quad	.L.str
	.long	1                       # 0x1
	.byte	1                       # 0x1
	.zero	3
	.size	_ZL12g_CodecsInfo, 40

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	68                      # 0x44
	.long	101                     # 0x65
	.long	108                     # 0x6c
	.long	116                     # 0x74
	.long	97                      # 0x61
	.long	0                       # 0x0
	.size	.L.str, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_DeltaFilter.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
