	.text
	.file	"ft.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	$9, %r14d
	movl	$10, %r15d
	cmpl	$2, %ebp
	jl	.LBB0_4
# BB#1:
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r15
	cmpl	$2, %ebp
	je	.LBB0_4
# BB#2:
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r14
	cmpl	$4, %ebp
	jl	.LBB0_4
# BB#3:
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, %edi
	callq	srandom
.LBB0_4:
	cmpl	$0, debug(%rip)
	je	.LBB0_6
# BB#5:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
.LBB0_6:
	movl	%r15d, %edi
	movl	%r14d, %esi
	callq	GenGraph
	movq	%rax, %rbx
	cmpl	$0, debug(%rip)
	je	.LBB0_8
# BB#7:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
.LBB0_8:
	movq	%rbx, %rdi
	callq	MST
	movq	%rax, %rbx
	cmpl	$0, debug(%rip)
	je	.LBB0_14
# BB#9:
	movl	$.Lstr.1, %edi
	callq	puts
	movq	%rbx, %rdi
	callq	PrintGraph
	movl	$.Lstr.2, %edi
	callq	puts
	movq	16(%rbx), %rbp
	cmpq	%rbx, %rbp
	je	.LBB0_12
	.p2align	4, 0x90
.LBB0_10:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %esi
	movq	32(%rbp), %rax
	movq	8(%rax), %rax
	movl	(%rax), %edx
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	printf
	movq	16(%rbp), %rbp
	cmpq	%rbx, %rbp
	jne	.LBB0_10
.LBB0_12:                               # %PrintMST.exit
	cmpl	$0, debug(%rip)
	je	.LBB0_14
# BB#13:
	movl	$.Lstr, %edi
	callq	puts
.LBB0_14:                               # %PrintMST.exit.thread
	xorl	%edi, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.globl	MST
	.p2align	4, 0x90
	.type	MST,@function
MST:                                    # @MST
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 48
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	xorl	%eax, %eax
	callq	InitFHeap
	movl	$0, 24(%r14)
	xorl	%eax, %eax
	callq	MakeHeap
	movq	%rax, 8(%rsp)
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	Insert
	movq	16(%r14), %rax
	cmpq	%r14, %rax
	je	.LBB1_2
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph34
                                        # =>This Inner Loop Header: Depth=1
	movl	$2147483647, 24(%rax)   # imm = 0x7FFFFFFF
	movq	16(%rax), %rax
	cmpq	%r14, %rax
	jne	.LBB1_4
.LBB1_2:                                # %.critedge.preheader
	movq	8(%rsp), %rdi
	callq	FindMin
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB1_11
# BB#3:
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB1_7:                                # %.lr.ph30
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_8 Depth 2
	movq	8(%rsp), %rdi
	callq	DeleteMin
	movq	%rax, 8(%rsp)
	movl	$-2147483648, 24(%rbx)  # imm = 0x80000000
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB1_6
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph
                                        #   Parent Loop BB1_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %eax
	movq	16(%rbx), %rsi
	cmpl	24(%rsi), %eax
	jge	.LBB1_10
# BB#9:                                 #   in Loop: Header=BB1_8 Depth=2
	movl	%eax, 24(%rsi)
	movq	%rbx, 32(%rsi)
	movq	%r15, %rdi
	callq	Insert
.LBB1_10:                               #   in Loop: Header=BB1_8 Depth=2
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_8
# BB#5:                                 # %.critedge.loopexit.loopexit
                                        #   in Loop: Header=BB1_7 Depth=1
	movq	8(%rsp), %rax
.LBB1_6:                                # %.critedge.loopexit
                                        #   in Loop: Header=BB1_7 Depth=1
	movq	%rax, %rdi
	callq	FindMin
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_7
.LBB1_11:                               # %.critedge._crit_edge
	movq	%r14, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	MST, .Lfunc_end1-MST
	.cfi_endproc

	.globl	PrintMST
	.p2align	4, 0x90
	.type	PrintMST,@function
PrintMST:                               # @PrintMST
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	16(%r14), %rbx
	cmpq	%r14, %rbx
	je	.LBB2_3
	.p2align	4, 0x90
.LBB2_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %esi
	movq	32(%rbx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %edx
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	printf
	movq	16(%rbx), %rbx
	cmpq	%r14, %rbx
	jne	.LBB2_1
.LBB2_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	PrintMST, .Lfunc_end2-PrintMST
	.cfi_endproc

	.type	debug,@object           # @debug
	.data
	.globl	debug
	.p2align	2
debug:
	.long	1                       # 0x1
	.size	debug, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Generating a connected graph ... "
	.size	.L.str, 34

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"done\nFinding the mininmum spanning tree ... "
	.size	.L.str.1, 45

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"vertex %d to %d\n"
	.size	.L.str.5, 17

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Time spent in finding the mininum spanning tree:"
	.size	.Lstr, 49

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.1:
	.asciz	"done\nThe graph:"
	.size	.Lstr.1, 16

	.type	.Lstr.2,@object         # @str.2
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.2:
	.asciz	"The minimum spanning tree:"
	.size	.Lstr.2, 27


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
