	.text
	.file	"relax.bc"
	.globl	hypre_BoomerAMGSeqRelax
	.p2align	4, 0x90
	.type	hypre_BoomerAMGSeqRelax,@function
hypre_BoomerAMGSeqRelax:                # @hypre_BoomerAMGSeqRelax
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	(%rdi), %r13
	movq	8(%rdi), %r15
	movq	16(%rdi), %r12
	movslq	24(%rdi), %r14
	movq	(%rdx), %rbx
	movq	(%rsi), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	$8, %esi
	movl	%r14d, %edi
	callq	hypre_CAlloc
	testq	%r14, %r14
	jle	.LBB0_27
# BB#1:                                 # %.lr.ph128.preheader
	movl	%r14d, %r9d
	cmpl	$4, %r14d
	jae	.LBB0_3
# BB#2:
	xorl	%edx, %edx
	jmp	.LBB0_11
.LBB0_3:                                # %min.iters.checked
	movl	%r9d, %r8d
	andl	$3, %r8d
	movq	%r9, %rdx
	subq	%r8, %rdx
	je	.LBB0_7
# BB#4:                                 # %vector.memcheck
	leaq	(%rbx,%r9,8), %rsi
	cmpq	%rsi, %rax
	jae	.LBB0_8
# BB#5:                                 # %vector.memcheck
	leaq	(%rax,%r9,8), %rsi
	cmpq	%rsi, %rbx
	jae	.LBB0_8
.LBB0_7:
	xorl	%edx, %edx
	jmp	.LBB0_11
.LBB0_8:                                # %vector.body.preheader
	leaq	16(%rbx), %rdi
	leaq	16(%rax), %rbp
	movq	%rdx, %rsi
	.p2align	4, 0x90
.LBB0_9:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdi), %xmm0
	movupd	(%rdi), %xmm1
	movups	%xmm0, -16(%rbp)
	movupd	%xmm1, (%rbp)
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$-4, %rsi
	jne	.LBB0_9
# BB#10:                                # %middle.block
	testl	%r8d, %r8d
	je	.LBB0_17
.LBB0_11:                               # %.lr.ph128.preheader141
	movl	%r9d, %edi
	subl	%edx, %edi
	leaq	-1(%r9), %rsi
	subq	%rdx, %rsi
	andq	$7, %rdi
	je	.LBB0_14
# BB#12:                                # %.lr.ph128.prol.preheader
	negq	%rdi
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph128.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rdx,8), %rbp
	movq	%rbp, (%rax,%rdx,8)
	incq	%rdx
	incq	%rdi
	jne	.LBB0_13
.LBB0_14:                               # %.lr.ph128.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB0_17
# BB#15:                                # %.lr.ph128.preheader141.new
	movq	%r9, %rsi
	subq	%rdx, %rsi
	leaq	56(%rax,%rdx,8), %rdi
	leaq	56(%rbx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB0_16:                               # %.lr.ph128
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rdx), %rbp
	movq	%rbp, -56(%rdi)
	movq	-48(%rdx), %rbp
	movq	%rbp, -48(%rdi)
	movq	-40(%rdx), %rbp
	movq	%rbp, -40(%rdi)
	movq	-32(%rdx), %rbp
	movq	%rbp, -32(%rdi)
	movq	-24(%rdx), %rbp
	movq	%rbp, -24(%rdi)
	movq	-16(%rdx), %rbp
	movq	%rbp, -16(%rdi)
	movq	-8(%rdx), %rbp
	movq	%rbp, -8(%rdi)
	movq	(%rdx), %rbp
	movq	%rbp, (%rdi)
	addq	$64, %rdi
	addq	$64, %rdx
	addq	$-8, %rsi
	jne	.LBB0_16
.LBB0_17:                               # %.loopexit
	testl	%r9d, %r9d
	jle	.LBB0_27
# BB#18:                                # %.lr.ph123.preheader
	xorl	%edx, %edx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_19:                               # %.lr.ph123
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_24 Depth 2
	movslq	(%r15,%rdx,4), %rdi
	movsd	(%r13,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jne	.LBB0_20
	jnp	.LBB0_26
.LBB0_20:                               #   in Loop: Header=BB0_19 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	movsd	(%rcx,%rdx,8), %xmm2    # xmm2 = mem[0],zero
	leaq	1(%rdi), %rbp
	movslq	4(%r15,%rdx,4), %rsi
	cmpl	%esi, %ebp
	jge	.LBB0_25
# BB#21:                                # %.lr.ph
                                        #   in Loop: Header=BB0_19 Depth=1
	movl	%esi, %ecx
	subl	%ebp, %ecx
	leaq	-1(%rsi), %r8
	testb	$1, %cl
	jne	.LBB0_23
# BB#22:                                #   in Loop: Header=BB0_19 Depth=1
	movq	%rbp, %rdi
	cmpq	%rbp, %r8
	jne	.LBB0_24
	jmp	.LBB0_25
.LBB0_23:                               #   in Loop: Header=BB0_19 Depth=1
	movslq	4(%r12,%rdi,4), %r10
	cmpl	%r14d, %r10d
	movq	%rax, %rcx
	cmovlq	%rbx, %rcx
	testl	%r10d, %r10d
	cmovsq	%rax, %rcx
	movsd	8(%r13,%rdi,8), %xmm3   # xmm3 = mem[0],zero
	mulsd	(%rcx,%r10,8), %xmm3
	subsd	%xmm3, %xmm2
	leaq	1(%rbp), %rdi
	cmpq	%rbp, %r8
	je	.LBB0_25
	.p2align	4, 0x90
.LBB0_24:                               #   Parent Loop BB0_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%r12,%rdi,4), %rcx
	cmpl	%r9d, %ecx
	movq	%rax, %rbp
	cmovlq	%rbx, %rbp
	testl	%ecx, %ecx
	cmovsq	%rax, %rbp
	movsd	(%r13,%rdi,8), %xmm3    # xmm3 = mem[0],zero
	mulsd	(%rbp,%rcx,8), %xmm3
	subsd	%xmm3, %xmm2
	movslq	4(%r12,%rdi,4), %rcx
	cmpl	%r14d, %ecx
	movq	%rax, %rbp
	cmovlq	%rbx, %rbp
	testl	%ecx, %ecx
	cmovsq	%rax, %rbp
	movsd	8(%r13,%rdi,8), %xmm3   # xmm3 = mem[0],zero
	mulsd	(%rbp,%rcx,8), %xmm3
	subsd	%xmm3, %xmm2
	addq	$2, %rdi
	cmpq	%rsi, %rdi
	jl	.LBB0_24
.LBB0_25:                               # %._crit_edge
                                        #   in Loop: Header=BB0_19 Depth=1
	divsd	%xmm1, %xmm2
	movsd	%xmm2, (%rbx,%rdx,8)
.LBB0_26:                               #   in Loop: Header=BB0_19 Depth=1
	incq	%rdx
	cmpq	%r14, %rdx
	jne	.LBB0_19
.LBB0_27:                               # %._crit_edge124
	movq	%rax, %rdi
	callq	hypre_Free
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_BoomerAMGSeqRelax, .Lfunc_end0-hypre_BoomerAMGSeqRelax
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
