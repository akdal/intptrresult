	.text
	.file	"btConvexCast.bc"
	.globl	_ZN12btConvexCastD2Ev
	.p2align	4, 0x90
	.type	_ZN12btConvexCastD2Ev,@function
_ZN12btConvexCastD2Ev:                  # @_ZN12btConvexCastD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	_ZN12btConvexCastD2Ev, .Lfunc_end0-_ZN12btConvexCastD2Ev
	.cfi_endproc

	.globl	_ZN12btConvexCastD0Ev
	.p2align	4, 0x90
	.type	_ZN12btConvexCastD0Ev,@function
_ZN12btConvexCastD0Ev:                  # @_ZN12btConvexCastD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end1:
	.size	_ZN12btConvexCastD0Ev, .Lfunc_end1-_ZN12btConvexCastD0Ev
	.cfi_endproc

	.type	_ZTV12btConvexCast,@object # @_ZTV12btConvexCast
	.section	.rodata,"a",@progbits
	.globl	_ZTV12btConvexCast
	.p2align	3
_ZTV12btConvexCast:
	.quad	0
	.quad	_ZTI12btConvexCast
	.quad	_ZN12btConvexCastD2Ev
	.quad	_ZN12btConvexCastD0Ev
	.quad	__cxa_pure_virtual
	.size	_ZTV12btConvexCast, 40

	.type	_ZTS12btConvexCast,@object # @_ZTS12btConvexCast
	.globl	_ZTS12btConvexCast
_ZTS12btConvexCast:
	.asciz	"12btConvexCast"
	.size	_ZTS12btConvexCast, 15

	.type	_ZTI12btConvexCast,@object # @_ZTI12btConvexCast
	.globl	_ZTI12btConvexCast
	.p2align	3
_ZTI12btConvexCast:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS12btConvexCast
	.size	_ZTI12btConvexCast, 16


	.globl	_ZN12btConvexCastD1Ev
	.type	_ZN12btConvexCastD1Ev,@function
_ZN12btConvexCastD1Ev = _ZN12btConvexCastD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
