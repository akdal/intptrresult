	.text
	.file	"idct.bc"
	.globl	Fast_IDCT
	.p2align	4, 0x90
	.type	Fast_IDCT,@function
Fast_IDCT:                              # @Fast_IDCT
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movswl	8(%rdi,%rax), %ecx
	shll	$11, %ecx
	movswl	12(%rdi,%rax), %r9d
	movswl	4(%rdi,%rax), %r8d
	movl	%r9d, %edx
	orl	%r8d, %edx
	orl	%ecx, %edx
	movswl	2(%rdi,%rax), %esi
	movswl	14(%rdi,%rax), %ebx
	movl	%esi, %ebp
	orl	%ebx, %ebp
	movswl	10(%rdi,%rax), %r10d
	orl	%r10d, %ebp
	orl	%edx, %ebp
	movswl	6(%rdi,%rax), %r11d
	movswl	(%rdi,%rax), %edx
	orl	%r11d, %ebp
	je	.LBB0_2
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	shll	$11, %edx
	leal	128(%rdx), %r14d
	leal	(%rbx,%rsi), %ebp
	imull	$565, %ebp, %ebp        # imm = 0x235
	imull	$2276, %esi, %esi       # imm = 0x8E4
	addl	%ebp, %esi
	imull	$-3406, %ebx, %r15d     # imm = 0xF2B2
	addl	%ebp, %r15d
	leal	(%r11,%r10), %ebx
	imull	$2408, %ebx, %ebx       # imm = 0x968
	imull	$-799, %r10d, %ebp      # imm = 0xFCE1
	addl	%ebx, %ebp
	imull	$-4017, %r11d, %r10d    # imm = 0xF04F
	addl	%ebx, %r10d
	leal	128(%rdx,%rcx), %r12d
	subl	%ecx, %r14d
	leal	(%r8,%r9), %ecx
	imull	$1108, %ecx, %ecx       # imm = 0x454
	imull	$-3784, %r9d, %ebx      # imm = 0xF138
	addl	%ecx, %ebx
	imull	$1568, %r8d, %edx       # imm = 0x620
	addl	%ecx, %edx
	leal	(%rbp,%rsi), %r8d
	subl	%ebp, %esi
	leal	(%r10,%r15), %r11d
	subl	%r10d, %r15d
	leal	(%r12,%rdx), %r9d
	subl	%edx, %r12d
	leal	(%r14,%rbx), %r10d
	subl	%ebx, %r14d
	leal	(%rsi,%r15), %ecx
	imull	$181, %ecx, %ecx
	subl	$-128, %ecx
	sarl	$8, %ecx
	subl	%r15d, %esi
	imull	$181, %esi, %esi
	subl	$-128, %esi
	sarl	$8, %esi
	leal	(%r9,%r8), %edx
	shrl	$8, %edx
	movw	%dx, (%rdi,%rax)
	leal	(%rcx,%r10), %edx
	shrl	$8, %edx
	movw	%dx, 2(%rdi,%rax)
	leal	(%rsi,%r14), %edx
	shrl	$8, %edx
	movw	%dx, 4(%rdi,%rax)
	leal	(%r12,%r11), %edx
	shrl	$8, %edx
	movw	%dx, 6(%rdi,%rax)
	subl	%r11d, %r12d
	shrl	$8, %r12d
	movw	%r12w, 8(%rdi,%rax)
	subl	%esi, %r14d
	shrl	$8, %r14d
	movw	%r14w, 10(%rdi,%rax)
	subl	%ecx, %r10d
	shrl	$8, %r10d
	movw	%r10w, 12(%rdi,%rax)
	subl	%r8d, %r9d
	shrl	$8, %r9d
	movw	%r9w, 14(%rdi,%rax)
	jmp	.LBB0_4
	.p2align	4, 0x90
.LBB0_2:                                #   in Loop: Header=BB0_1 Depth=1
	shll	$3, %edx
	movd	%edx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, (%rdi,%rax)
.LBB0_4:                                # %idctrow.exit
                                        #   in Loop: Header=BB0_1 Depth=1
	addq	$16, %rax
	cmpq	$128, %rax
	jne	.LBB0_1
# BB#5:                                 # %.preheader
	movq	$-8, %rax
	movq	iclp(%rip), %r9
	.p2align	4, 0x90
.LBB0_6:                                # =>This Inner Loop Header: Depth=1
	movswl	80(%rdi,%rax,2), %edx
	shll	$8, %edx
	movswl	112(%rdi,%rax,2), %r10d
	movswl	48(%rdi,%rax,2), %r8d
	movl	%r10d, %esi
	orl	%r8d, %esi
	orl	%edx, %esi
	movswl	32(%rdi,%rax,2), %ecx
	movswl	128(%rdi,%rax,2), %ebx
	movl	%ecx, %ebp
	orl	%ebx, %ebp
	movswl	96(%rdi,%rax,2), %r15d
	orl	%r15d, %ebp
	orl	%esi, %ebp
	movswl	64(%rdi,%rax,2), %r11d
	movswl	16(%rdi,%rax,2), %esi
	orl	%r11d, %ebp
	je	.LBB0_7
# BB#8:                                 #   in Loop: Header=BB0_6 Depth=1
	shll	$8, %esi
	leal	8192(%rsi), %r14d
	leal	(%rbx,%rcx), %ebp
	imull	$565, %ebp, %ebp        # imm = 0x235
	imull	$2276, %ecx, %ecx       # imm = 0x8E4
	leal	4(%rbp,%rcx), %ecx
	sarl	$3, %ecx
	imull	$-3406, %ebx, %ebx      # imm = 0xF2B2
	leal	4(%rbp,%rbx), %r12d
	sarl	$3, %r12d
	leal	(%r11,%r15), %ebp
	imull	$2408, %ebp, %ebp       # imm = 0x968
	imull	$-799, %r15d, %ebx      # imm = 0xFCE1
	leal	4(%rbp,%rbx), %r15d
	sarl	$3, %r15d
	imull	$-4017, %r11d, %ebx     # imm = 0xF04F
	leal	4(%rbp,%rbx), %ebp
	sarl	$3, %ebp
	leal	8192(%rsi,%rdx), %r13d
	subl	%edx, %r14d
	leal	(%r8,%r10), %edx
	imull	$1108, %edx, %edx       # imm = 0x454
	imull	$-3784, %r10d, %ebx     # imm = 0xF138
	leal	4(%rdx,%rbx), %ebx
	sarl	$3, %ebx
	imull	$1568, %r8d, %esi       # imm = 0x620
	leal	4(%rdx,%rsi), %edx
	sarl	$3, %edx
	leal	(%r15,%rcx), %r8d
	subl	%r15d, %ecx
	leal	(%rbp,%r12), %r15d
	subl	%ebp, %r12d
	leal	(%r13,%rdx), %r10d
	subl	%edx, %r13d
	leal	(%r14,%rbx), %r11d
	subl	%ebx, %r14d
	leal	(%rcx,%r12), %edx
	imull	$181, %edx, %edx
	subl	$-128, %edx
	sarl	$8, %edx
	subl	%r12d, %ecx
	imull	$181, %ecx, %ecx
	subl	$-128, %ecx
	sarl	$8, %ecx
	leal	(%r10,%r8), %esi
	sarl	$14, %esi
	movslq	%esi, %rsi
	movzwl	(%r9,%rsi,2), %esi
	movw	%si, 16(%rdi,%rax,2)
	leal	(%r11,%rdx), %esi
	sarl	$14, %esi
	movslq	%esi, %rsi
	movzwl	(%r9,%rsi,2), %esi
	movw	%si, 32(%rdi,%rax,2)
	leal	(%r14,%rcx), %esi
	sarl	$14, %esi
	movslq	%esi, %rsi
	movzwl	(%r9,%rsi,2), %esi
	movw	%si, 48(%rdi,%rax,2)
	leal	(%r13,%r15), %esi
	sarl	$14, %esi
	movslq	%esi, %rsi
	movzwl	(%r9,%rsi,2), %esi
	movw	%si, 64(%rdi,%rax,2)
	subl	%r15d, %r13d
	sarl	$14, %r13d
	movslq	%r13d, %rsi
	movzwl	(%r9,%rsi,2), %esi
	movw	%si, 80(%rdi,%rax,2)
	subl	%ecx, %r14d
	sarl	$14, %r14d
	movslq	%r14d, %rcx
	movzwl	(%r9,%rcx,2), %ecx
	movw	%cx, 96(%rdi,%rax,2)
	subl	%edx, %r11d
	sarl	$14, %r11d
	movslq	%r11d, %rcx
	movzwl	(%r9,%rcx,2), %ecx
	movw	%cx, 112(%rdi,%rax,2)
	subl	%r8d, %r10d
	sarl	$14, %r10d
	movslq	%r10d, %rcx
	movzwl	(%r9,%rcx,2), %ecx
	movw	%cx, 128(%rdi,%rax,2)
	incq	%rax
	jne	.LBB0_6
	jmp	.LBB0_10
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_6 Depth=1
	addl	$32, %esi
	sarl	$6, %esi
	movslq	%esi, %rcx
	movzwl	(%r9,%rcx,2), %ecx
	movw	%cx, 128(%rdi,%rax,2)
	movw	%cx, 112(%rdi,%rax,2)
	movw	%cx, 96(%rdi,%rax,2)
	movw	%cx, 80(%rdi,%rax,2)
	movw	%cx, 64(%rdi,%rax,2)
	movw	%cx, 48(%rdi,%rax,2)
	movw	%cx, 32(%rdi,%rax,2)
	movw	%cx, 16(%rdi,%rax,2)
	incq	%rax
	jne	.LBB0_6
.LBB0_10:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	Fast_IDCT, .Lfunc_end0-Fast_IDCT
	.cfi_endproc

	.globl	Initialize_Fast_IDCT
	.p2align	4, 0x90
	.type	Initialize_Fast_IDCT,@function
Initialize_Fast_IDCT:                   # @Initialize_Fast_IDCT
	.cfi_startproc
# BB#0:
	movq	$iclip+1024, iclp(%rip)
	movq	$-512, %rax             # imm = 0xFE00
	movw	$255, %cx
	movw	$-256, %dx
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	cmpq	$255, %rax
	movw	$255, %si
	cmovlw	%ax, %si
	cmpq	$-256, %rax
	cmovlw	%dx, %si
	movw	%si, iclip+1024(%rax,%rax)
	leaq	1(%rax), %rsi
	cmpq	$255, %rsi
	movl	%esi, %edi
	cmovgew	%cx, %di
	cmpq	$-256, %rsi
	cmovlw	%dx, %di
	movw	%di, iclip+1026(%rax,%rax)
	leal	2(%rax), %esi
	leaq	2(%rax), %rdi
	cmpq	$255, %rdi
	cmovgew	%cx, %si
	cmpq	$-256, %rdi
	cmovlw	%dx, %si
	movw	%si, iclip+1028(%rax,%rax)
	leal	3(%rax), %esi
	leaq	3(%rax), %rdi
	cmpq	$255, %rdi
	cmovgew	%cx, %si
	cmpq	$-256, %rdi
	cmovlw	%dx, %si
	movw	%si, iclip+1030(%rax,%rax)
	addq	$4, %rax
	cmpq	$512, %rax              # imm = 0x200
	jne	.LBB1_1
# BB#2:
	retq
.Lfunc_end1:
	.size	Initialize_Fast_IDCT, .Lfunc_end1-Initialize_Fast_IDCT
	.cfi_endproc

	.type	iclip,@object           # @iclip
	.local	iclip
	.comm	iclip,2048,16
	.type	iclp,@object            # @iclp
	.local	iclp
	.comm	iclp,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
