	.text
	.file	"rad2deg.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4640537203540230144     # double 180
.LCPI0_1:
	.quad	4607182418800017408     # double 1
.LCPI0_2:
	.quad	4614256656552045848     # double 3.1415926535897931
	.text
	.globl	rad2deg
	.p2align	4, 0x90
	.type	rad2deg,@function
rad2deg:                                # @rad2deg
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	mulsd	.LCPI0_0(%rip), %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	.LCPI0_1(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	atan
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	.LCPI0_2(%rip), %xmm0
	popq	%rax
	retq
.Lfunc_end0:
	.size	rad2deg, .Lfunc_end0-rad2deg
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4607182418800017408     # double 1
.LCPI1_1:
	.quad	4614256656552045848     # double 3.1415926535897931
.LCPI1_2:
	.quad	4640537203540230144     # double 180
	.text
	.globl	deg2rad
	.p2align	4, 0x90
	.type	deg2rad,@function
deg2rad:                                # @deg2rad
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi1:
	.cfi_def_cfa_offset 16
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	atan
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_1(%rip), %xmm0
	divsd	.LCPI1_2(%rip), %xmm0
	popq	%rax
	retq
.Lfunc_end1:
	.size	deg2rad, .Lfunc_end1-deg2rad
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
