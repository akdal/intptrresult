	.text
	.file	"z17.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_1:
	.long	1127481344              # float 180
.LCPI0_2:
	.long	3283353600              # float -360
.LCPI0_3:
	.long	3274964992              # float -180
.LCPI0_4:
	.long	1135869952              # float 360
.LCPI0_5:
	.long	1124073472              # float 128
.LCPI0_6:
	.long	1166016512              # float 4096
.LCPI0_7:
	.long	1123024896              # float 120
.LCPI0_8:
	.long	1101004800              # float 20
.LCPI0_9:
	.long	1152647168              # float 1440
.LCPI0_10:
	.long	1141751808              # float 567
	.text
	.globl	GetGap
	.p2align	4, 0x90
	.type	GetGap,@function
GetGap:                                 # @GetGap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %r12
	movq	%rsi, %rbp
	movq	%rdi, %r14
	movzwl	(%r12), %eax
	movw	$0, 2(%r12)
	andl	$895, %eax              # imm = 0x37F
	orl	$9216, %eax             # imm = 0x2400
	movw	%ax, (%r12)
	movl	$158, (%r13)
	leaq	32(%r14), %r15
	movb	32(%r14), %al
	addb	$-11, %al
	cmpb	$2, %al
	jae	.LBB0_1
# BB#2:
	movb	64(%r14), %al
	testb	%al, %al
	je	.LBB0_63
# BB#3:
	leaq	64(%r14), %rcx
	cmpb	$45, %al
	je	.LBB0_4
# BB#5:
	cmpb	$43, %al
	movq	%rcx, %rbx
	jne	.LBB0_8
# BB#6:
	movl	$159, %eax
	jmp	.LBB0_7
.LBB0_1:
	movl	$17, %edi
	movl	$1, %esi
	movl	$.L.str, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	Error
	jmp	.LBB0_63
.LBB0_4:
	movl	$160, %eax
.LBB0_7:                                # %.sink.split
	movl	%eax, (%r13)
	leaq	1(%rcx), %rbx
.LBB0_8:
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	leaq	4(%rsp), %rdx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB0_64
	.p2align	4, 0x90
.LBB0_9:                                # %.preheader.preheader
                                        # =>This Inner Loop Header: Depth=1
	incq	%rbx
	movzbl	-1(%rbx), %eax
	movl	%eax, %ecx
	addb	$-48, %cl
	cmpb	$10, %cl
	jb	.LBB0_9
# BB#10:                                #   in Loop: Header=BB0_9 Depth=1
	addb	$-46, %al
	cmpb	$76, %al
	ja	.LBB0_38
# BB#11:                                #   in Loop: Header=BB0_9 Depth=1
	movzbl	%al, %eax
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_22:
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI0_6(%rip), %xmm0
	movw	$2048, %ax              # imm = 0x800
	jmp	.LBB0_41
.LBB0_64:
	movl	$17, %edi
	movl	$2, %esi
	movl	$.L.str.2, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	movq	8(%rsp), %r9            # 8-byte Reload
	callq	Error
	movl	$17, %edi
	movl	$3, %esi
	movl	$.L.str.3, %edx
	movl	$2, %ecx
	movl	$.L.str.4, %r9d
	movl	$0, %eax
	movq	%r15, %r8
	pushq	$.L.str.6
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.5
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB0_63
.LBB0_24:
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	cmpl	$160, (%r13)
	jne	.LBB0_26
# BB#25:
	xorps	.LCPI0_0(%rip), %xmm0
	movss	%xmm0, 4(%rsp)
.LBB0_26:                               # %._crit_edge98
	movl	$158, (%r13)
	ucomiss	.LCPI0_1(%rip), %xmm0
	jbe	.LBB0_30
# BB#27:                                # %.lr.ph93.preheader
	movss	.LCPI0_2(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI0_1(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB0_28:                               # %.lr.ph93
                                        # =>This Inner Loop Header: Depth=1
	addss	%xmm1, %xmm0
	ucomiss	%xmm2, %xmm0
	ja	.LBB0_28
# BB#29:                                # %.thread-pre-split_crit_edge
	movss	%xmm0, 4(%rsp)
.LBB0_30:                               # %thread-pre-split
	movss	.LCPI0_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB0_34
# BB#31:                                # %.lr.ph.preheader
	movss	.LCPI0_4(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB0_32:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	addss	%xmm2, %xmm0
	ucomiss	%xmm0, %xmm1
	ja	.LBB0_32
# BB#33:                                # %._crit_edge
	movss	%xmm0, 4(%rsp)
.LBB0_34:
	ucomiss	.LCPI0_3(%rip), %xmm0
	jb	.LBB0_36
# BB#35:
	movss	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB0_37
.LBB0_36:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.7, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
.LBB0_37:
	mulss	.LCPI0_5(%rip), %xmm0
	movw	$4096, %ax              # imm = 0x1000
	jmp	.LBB0_41
.LBB0_20:
	movswl	10(%rbp), %eax
	jmp	.LBB0_17
.LBB0_38:
	movl	$17, %edi
	movl	$4, %esi
	movl	$.L.str.9, %edx
	jmp	.LBB0_62
.LBB0_39:
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI0_10(%rip), %xmm0
	jmp	.LBB0_40
.LBB0_15:
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 20(%rsp)         # 4-byte Spill
	movl	$4095, %edi             # imm = 0xFFF
	andl	12(%rbp), %edi
	movq	%r14, %rsi
	callq	FontSize
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	mulss	20(%rsp), %xmm0         # 4-byte Folded Reload
	jmp	.LBB0_40
.LBB0_12:
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI0_9(%rip), %xmm0
	jmp	.LBB0_40
.LBB0_14:
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI0_7(%rip), %xmm0
	jmp	.LBB0_40
.LBB0_13:
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI0_8(%rip), %xmm0
	jmp	.LBB0_40
.LBB0_23:
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI0_6(%rip), %xmm0
	movw	$3072, %ax              # imm = 0xC00
	jmp	.LBB0_41
.LBB0_16:
	movswl	6(%rbp), %eax
	jmp	.LBB0_17
.LBB0_18:
	movswl	2(%rbp), %eax
	jmp	.LBB0_17
.LBB0_21:
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI0_6(%rip), %xmm0
	movw	$5120, %ax              # imm = 0x1400
	jmp	.LBB0_41
.LBB0_19:
	movswl	8(%rbp), %eax
.LBB0_17:
	cvtsi2ssl	%eax, %xmm0
	mulss	4(%rsp), %xmm0
.LBB0_40:
	movw	$1024, %ax              # imm = 0x400
.LBB0_41:
	cvttss2si	%xmm0, %ecx
	movzwl	(%r12), %edx
	andl	$58367, %edx            # imm = 0xE3FF
	movl	%eax, %esi
	orl	%edx, %esi
	movw	%si, (%r12)
	movzwl	%ax, %eax
	cmpl	$3072, %eax             # imm = 0xC00
	jne	.LBB0_44
# BB#42:
	cmpl	$4097, %ecx             # imm = 0x1001
	jl	.LBB0_44
# BB#43:
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$17, %edi
	movl	$5, %esi
	movl	$.L.str.10, %edx
	movl	$2, %ecx
	movb	$1, %al
	movq	%r15, %r8
	callq	Error
	movl	$4096, %ecx             # imm = 0x1000
.LBB0_44:
	movw	%cx, 2(%r12)
	movb	(%rbx), %al
	movl	%eax, %ecx
	addb	$-101, %cl
	cmpb	$19, %cl
	ja	.LBB0_45
# BB#65:
	movzbl	%cl, %eax
	jmpq	*.LJTI0_1(,%rax,8)
.LBB0_50:
	movzwl	(%r12), %eax
	andl	$8191, %eax             # imm = 0x1FFF
	orl	$8192, %eax             # imm = 0x2000
	jmp	.LBB0_51
.LBB0_45:
	testb	%al, %al
	jne	.LBB0_57
.LBB0_46:
	movzwl	(%r12), %eax
	andl	$8191, %eax             # imm = 0x1FFF
	orl	$8192, %eax             # imm = 0x2000
	movw	%ax, (%r12)
	jmp	.LBB0_47
.LBB0_57:
	movl	$17, %edi
	movl	$7, %esi
	movl	$.L.str.11, %edx
	jmp	.LBB0_62
.LBB0_52:
	movzwl	(%r12), %eax
	andl	$8191, %eax             # imm = 0x1FFF
	orl	$16384, %eax            # imm = 0x4000
	jmp	.LBB0_51
.LBB0_55:
	movzwl	(%r12), %eax
	andl	$8191, %eax             # imm = 0x1FFF
	orl	$40960, %eax            # imm = 0xA000
	jmp	.LBB0_51
.LBB0_54:
	movzwl	(%r12), %eax
	andl	$8191, %eax             # imm = 0x1FFF
	orl	$32768, %eax            # imm = 0x8000
	jmp	.LBB0_51
.LBB0_56:
	movzwl	(%r12), %eax
	andl	$8191, %eax             # imm = 0x1FFF
	orl	$49152, %eax            # imm = 0xC000
	jmp	.LBB0_51
.LBB0_53:
	movzwl	(%r12), %eax
	andl	$8191, %eax             # imm = 0x1FFF
	orl	$24576, %eax            # imm = 0x6000
.LBB0_51:
	movw	%ax, (%r12)
	incq	%rbx
.LBB0_47:
	movb	(%rbx), %cl
	cmpb	$117, %cl
	jne	.LBB0_60
# BB#48:
	movl	%eax, %ecx
	andl	$57344, %ecx            # imm = 0xE000
	cmpl	$16384, %ecx            # imm = 0x4000
	jne	.LBB0_58
# BB#49:
	movl	$17, %edi
	movl	$9, %esi
	movl	$.L.str.12, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	movq	8(%rsp), %r9            # 8-byte Reload
	callq	Error
	jmp	.LBB0_59
.LBB0_58:
	orl	$128, %eax
	movw	%ax, (%r12)
.LBB0_59:
	movb	1(%rbx), %cl
.LBB0_60:
	testb	%cl, %cl
	je	.LBB0_63
# BB#61:
	movl	$17, %edi
	movl	$8, %esi
	movl	$.L.str.13, %edx
.LBB0_62:
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	movq	8(%rsp), %r9            # 8-byte Reload
	callq	Error
.LBB0_63:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	GetGap, .Lfunc_end0-GetGap
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_9
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_22
	.quad	.LBB0_39
	.quad	.LBB0_24
	.quad	.LBB0_38
	.quad	.LBB0_15
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_12
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_14
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_13
	.quad	.LBB0_38
	.quad	.LBB0_23
	.quad	.LBB0_16
	.quad	.LBB0_38
	.quad	.LBB0_38
	.quad	.LBB0_18
	.quad	.LBB0_21
	.quad	.LBB0_38
	.quad	.LBB0_19
	.quad	.LBB0_20
.LJTI0_1:
	.quad	.LBB0_50
	.quad	.LBB0_57
	.quad	.LBB0_57
	.quad	.LBB0_52
	.quad	.LBB0_57
	.quad	.LBB0_57
	.quad	.LBB0_55
	.quad	.LBB0_57
	.quad	.LBB0_57
	.quad	.LBB0_57
	.quad	.LBB0_54
	.quad	.LBB0_57
	.quad	.LBB0_57
	.quad	.LBB0_57
	.quad	.LBB0_57
	.quad	.LBB0_56
	.quad	.LBB0_46
	.quad	.LBB0_57
	.quad	.LBB0_57
	.quad	.LBB0_53

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4591870180066957722     # double 0.10000000000000001
	.text
	.globl	MinGap
	.p2align	4, 0x90
	.type	MinGap,@function
MinGap:                                 # @MinGap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 48
.Lcfi21:
	.cfi_offset %rbx, -48
.Lcfi22:
	.cfi_offset %r12, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movl	%esi, %r15d
	movl	%edi, %r14d
	movzwl	(%r12), %eax
	movl	%eax, %ecx
	shrl	$10, %ecx
	andb	$7, %cl
	xorl	%ebp, %ebp
	movl	%ecx, %ebx
	addb	$-2, %bl
	cmpb	$2, %bl
	jb	.LBB1_6
# BB#1:
	cmpb	$5, %cl
	je	.LBB1_4
# BB#2:
	cmpb	$1, %cl
	jne	.LBB1_5
# BB#3:
	movswl	2(%r12), %ebp
	jmp	.LBB1_6
.LBB1_4:
	movswl	2(%r12), %ecx
	addl	%r15d, %edx
	imull	%ecx, %edx
	movl	%edx, %ebp
	sarl	$31, %ebp
	shrl	$20, %ebp
	addl	%edx, %ebp
	sarl	$12, %ebp
	jmp	.LBB1_6
.LBB1_5:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.7, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.14, %r9d
	xorl	%eax, %eax
	callq	Error
	movw	(%r12), %ax
                                        # implicit-def: %EBP
.LBB1_6:
	andl	$57344, %eax            # imm = 0xE000
	shrl	$13, %eax
	cmpb	$7, %al
	je	.LBB1_8
# BB#7:
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_8:
	addl	%r14d, %r15d
	addl	%ebp, %r15d
	cmpl	$8388608, %r15d         # imm = 0x800000
	movl	$8388607, %ebp          # imm = 0x7FFFFF
.LBB1_15:
	cmovll	%r15d, %ebp
	jmp	.LBB1_16
.LBB1_9:
	movq	no_fpos(%rip), %r8
	xorl	%ebp, %ebp
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.7, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB1_16
.LBB1_10:
	movq	BackEnd(%rip), %rax
	addl	%r14d, %r15d
	cmpl	$0, 36(%rax)
	je	.LBB1_14
# BB#11:
	cvtsi2sdl	%ebp, %xmm0
	mulsd	.LCPI1_0(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	addl	%r15d, %eax
	cmpl	%eax, %ebp
	cmovll	%eax, %ebp
	jmp	.LBB1_16
.LBB1_12:
	cmpl	%r15d, %r14d
	cmovll	%r15d, %r14d
	cmpl	%ebp, %r14d
	cmovgel	%r14d, %ebp
	jmp	.LBB1_16
.LBB1_13:
	addl	%r14d, %r15d
	movl	%r15d, %ebp
.LBB1_16:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_14:
	cmpl	%r15d, %ebp
	jmp	.LBB1_15
.Lfunc_end1:
	.size	MinGap, .Lfunc_end1-MinGap
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_9
	.quad	.LBB1_8
	.quad	.LBB1_8
	.quad	.LBB1_10
	.quad	.LBB1_16
	.quad	.LBB1_12
	.quad	.LBB1_13

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4606281698874543309     # double 0.90000000000000002
	.text
	.globl	ExtraGap
	.p2align	4, 0x90
	.type	ExtraGap,@function
ExtraGap:                               # @ExtraGap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movzwl	(%rdx), %ebx
	movl	%ebx, %eax
	andl	$7168, %eax             # imm = 0x1C00
	xorl	%ebp, %ebp
	cmpl	$1024, %eax             # imm = 0x400
	movl	$0, %eax
	jne	.LBB2_2
# BB#1:
	movswl	2(%rdx), %eax
.LBB2_2:
	andl	$57344, %ebx            # imm = 0xE000
	shrl	$13, %ebx
	cmpb	$5, %bl
	ja	.LBB2_11
# BB#3:
	jmpq	*.LJTI2_0(,%rbx,8)
.LBB2_4:
	movq	no_fpos(%rip), %r8
	xorl	%ebp, %ebp
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.7, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.17, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB2_11
.LBB2_5:
	movq	BackEnd(%rip), %rcx
	cmpl	$0, 36(%rcx)
	je	.LBB2_7
# BB#6:
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI2_0(%rip), %xmm0
	cvttsd2si	%xmm0, %ebp
	subl	%edi, %ebp
	xorl	%eax, %eax
	subl	%esi, %ebp
	cmovsl	%eax, %ebp
	jmp	.LBB2_11
.LBB2_8:
	movl	$8388607, %ebp          # imm = 0x7FFFFF
	jmp	.LBB2_11
.LBB2_9:
	cmpl	%esi, %eax
	cmovll	%esi, %eax
	cmpl	%edi, %eax
	cmovll	%edi, %eax
	cmpl	$151, %ecx
	cmovel	%esi, %edi
	subl	%edi, %eax
	jmp	.LBB2_10
.LBB2_7:
	subl	%edi, %eax
	xorl	%ecx, %ecx
	subl	%esi, %eax
	cmovsl	%ecx, %eax
.LBB2_10:
	movl	%eax, %ebp
.LBB2_11:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end2:
	.size	ExtraGap, .Lfunc_end2-ExtraGap
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_4
	.quad	.LBB2_11
	.quad	.LBB2_11
	.quad	.LBB2_5
	.quad	.LBB2_8
	.quad	.LBB2_9

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4591870180066957722     # double 0.10000000000000001
	.text
	.globl	ActualGap
	.p2align	4, 0x90
	.type	ActualGap,@function
ActualGap:                              # @ActualGap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi35:
	.cfi_def_cfa_offset 48
.Lcfi36:
	.cfi_offset %rbx, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movq	%rcx, %rbp
	movl	%esi, %ebx
	movl	%edi, %r15d
	movzwl	(%rbp), %eax
	movl	%eax, %ecx
	shrl	$10, %ecx
	andb	$7, %cl
	decb	%cl
	cmpb	$4, %cl
	ja	.LBB3_6
# BB#1:
	movzbl	%cl, %ecx
	jmpq	*.LJTI3_0(,%rcx,8)
.LBB3_2:
	movswl	2(%rbp), %ebp
	jmp	.LBB3_9
.LBB3_3:
	movswl	2(%rbp), %ecx
	cmpl	$4096, %ecx             # imm = 0x1000
	movl	$8388607, %ebp          # imm = 0x7FFFFF
	jg	.LBB3_9
# BB#4:
	imull	%r8d, %ecx
	movl	%ecx, %ebp
	sarl	$31, %ebp
	shrl	$20, %ebp
	addl	%ecx, %ebp
	jmp	.LBB3_8
.LBB3_5:
	movswl	2(%rbp), %ecx
	addl	%ebx, %edx
	subl	%edx, %r8d
	imull	%ecx, %r8d
	movl	%r8d, %ebp
	sarl	$31, %ebp
	shrl	$20, %ebp
	addl	%r8d, %ebp
	xorl	%ecx, %ecx
	sarl	$12, %ebp
	cmovsl	%ecx, %ebp
	jmp	.LBB3_9
.LBB3_6:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.7, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.19, %r9d
	xorl	%eax, %eax
	callq	Error
	movw	(%rbp), %ax
                                        # implicit-def: %EBP
	jmp	.LBB3_9
.LBB3_7:
	movswl	2(%rbp), %ecx
	addl	%ebx, %edx
	imull	%ecx, %edx
	movl	%edx, %ebp
	sarl	$31, %ebp
	shrl	$20, %ebp
	addl	%edx, %ebp
.LBB3_8:
	sarl	$12, %ebp
.LBB3_9:
	andl	$57344, %eax            # imm = 0xE000
	shrl	$13, %eax
	cmpb	$7, %al
	je	.LBB3_11
# BB#10:
	jmpq	*.LJTI3_1(,%rax,8)
.LBB3_11:
	addl	%r15d, %ebx
	addl	%ebx, %ebp
	jmp	.LBB3_18
.LBB3_12:
	movq	no_fpos(%rip), %r8
	xorl	%ebp, %ebp
	movl	$17, %edi
	movl	$10, %esi
	movl	$.L.str.20, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.7, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.21, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB3_18
.LBB3_13:
	movq	BackEnd(%rip), %rax
	addl	%r15d, %ebx
	cmpl	$0, 36(%rax)
	je	.LBB3_17
# BB#14:
	cvtsi2sdl	%ebp, %xmm0
	mulsd	.LCPI3_0(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	addl	%ebx, %eax
	cmpl	%eax, %ebp
	cmovll	%eax, %ebp
	jmp	.LBB3_18
.LBB3_15:
	cmpl	%ebx, %r15d
	cmovll	%ebx, %r15d
	cmpl	%ebp, %r15d
	cmovgel	%r15d, %ebp
	jmp	.LBB3_18
.LBB3_16:
	movl	%ebx, %eax
	subl	%r14d, %eax
	addl	%ebp, %eax
	addl	%ebx, %r15d
	cmpl	%r15d, %eax
	cmovgel	%eax, %r15d
	movl	%r15d, %ebp
	jmp	.LBB3_18
.LBB3_17:
	cmpl	%ebx, %ebp
	cmovll	%ebx, %ebp
.LBB3_18:
	cmpl	$8388608, %ebp          # imm = 0x800000
	movl	$8388607, %eax          # imm = 0x7FFFFF
	cmovll	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	ActualGap, .Lfunc_end3-ActualGap
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_2
	.quad	.LBB3_3
	.quad	.LBB3_5
	.quad	.LBB3_6
	.quad	.LBB3_7
.LJTI3_1:
	.quad	.LBB3_12
	.quad	.LBB3_11
	.quad	.LBB3_11
	.quad	.LBB3_13
	.quad	.LBB3_18
	.quad	.LBB3_15
	.quad	.LBB3_16

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"gap is not a simple word"
	.size	.L.str, 25

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%f"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"width missing from %s"
	.size	.L.str.2, 22

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%s, %s and %s must be enclosed in double quotes"
	.size	.L.str.3, 48

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"/"
	.size	.L.str.4, 2

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"|"
	.size	.L.str.5, 2

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"&"
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"assert failed in %s"
	.size	.L.str.7, 20

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"GetGap: dg!"
	.size	.L.str.8, 12

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"units letter missing from %s"
	.size	.L.str.9, 29

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%.1fr too large (1.0r substituted)"
	.size	.L.str.10, 35

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"unknown gap mode in %s"
	.size	.L.str.11, 23

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"replacing self-contradictory gap %s by breakable version"
	.size	.L.str.12, 57

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"invalid width or gap %s"
	.size	.L.str.13, 24

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"MinGap: units"
	.size	.L.str.14, 14

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"MinGap: NO_MODE"
	.size	.L.str.15, 16

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"ExtraGap: NO_MODE"
	.size	.L.str.17, 18

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"ActualGap: units"
	.size	.L.str.19, 17

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"cannot continue after previous error(s)"
	.size	.L.str.20, 40

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"ActualGap: NO_MODE"
	.size	.L.str.21, 19


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
