	.text
	.file	"fmo.bc"
	.globl	FmoInit
	.p2align	4, 0x90
	.type	FmoInit,@function
FmoInit:                                # @FmoInit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r12
	movq	%rdi, %r15
	pcmpeqd	%xmm0, %xmm0
	movdqa	%xmm0, FirstMBInSlice+16(%rip)
	movdqa	%xmm0, FirstMBInSlice(%rip)
	movl	15336(%r15), %eax
	imull	15340(%r15), %eax
	movl	%eax, PicSizeInMapUnits(%rip)
	cmpl	$6, 64(%r12)
	jne	.LBB0_3
# BB#1:
	movl	172(%r12), %ecx
	incl	%ecx
	cmpl	%eax, %ecx
	je	.LBB0_3
# BB#2:
	movl	$.L.str, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB0_3:
	movq	MapUnitToSliceGroupMap(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_5
# BB#4:
	callq	free
.LBB0_5:
	movl	PicSizeInMapUnits(%rip), %ebx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %rcx
	movq	%rcx, MapUnitToSliceGroupMap(%rip)
	testq	%rcx, %rcx
	je	.LBB0_6
# BB#9:
	movl	60(%r12), %edi
	testl	%edi, %edi
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	je	.LBB0_10
# BB#11:
	movl	64(%r12), %esi
	cmpq	$6, %rsi
	ja	.LBB0_114
# BB#12:
	jmpq	*.LJTI0_0(,%rsi,8)
.LBB0_13:                               # %.preheader.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_14:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_16 Depth 2
                                        #       Child Loop BB0_17 Depth 3
	cmpl	%ebx, %eax
	jae	.LBB0_92
# BB#15:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB0_14 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_16:                               # %.preheader.i.i
                                        #   Parent Loop BB0_14 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_17 Depth 3
	movl	%ecx, %edx
	movl	%edx, %ecx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_17:                               # %._crit_edge.i.i
                                        #   Parent Loop BB0_14 Depth=1
                                        #     Parent Loop BB0_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rax,%rsi), %edi
	cmpl	%ebx, %edi
	jae	.LBB0_18
# BB#19:                                #   in Loop: Header=BB0_17 Depth=3
	movq	MapUnitToSliceGroupMap(%rip), %rbp
	movl	%edi, %edi
	movb	%dl, (%rbp,%rdi)
	incl	%esi
	movl	68(%r12,%rcx,4), %edi
	movl	PicSizeInMapUnits(%rip), %ebx
	cmpl	%edi, %esi
	jbe	.LBB0_17
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_18:                               # %..critedge1_crit_edge.i.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	68(%r12,%rcx,4), %edi
.LBB0_20:                               # %.critedge1.i.i
                                        #   in Loop: Header=BB0_16 Depth=2
	incl	%ecx
	leal	1(%rax,%rdi), %eax
	cmpl	60(%r12), %ecx
	ja	.LBB0_22
# BB#21:                                # %.critedge1.i.i
                                        #   in Loop: Header=BB0_16 Depth=2
	cmpl	%ebx, %eax
	jb	.LBB0_16
.LBB0_22:                               # %.critedge.i.i
                                        #   in Loop: Header=BB0_14 Depth=1
	cmpl	%ebx, %eax
	jb	.LBB0_14
	jmp	.LBB0_92
.LBB0_10:
	xorl	%esi, %esi
	movq	%rcx, %rdi
	movq	%rbx, %rdx
	callq	memset
.LBB0_92:                               # %FmoGenerateMapUnitToSliceGroupMap.exit
	movq	MBAmap(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_94
# BB#93:
	callq	free
.LBB0_94:
	movl	15348(%r15), %ebx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %rcx
	movq	%rcx, MBAmap(%rip)
	testq	%rcx, %rcx
	movq	16(%rsp), %rax          # 8-byte Reload
	je	.LBB0_95
# BB#96:
	cmpl	$0, 1148(%rax)
	jne	.LBB0_98
# BB#97:
	cmpl	$0, 15312(%r15)
	je	.LBB0_104
.LBB0_98:                               # %.preheader3.i
	testl	%ebx, %ebx
	je	.LBB0_103
# BB#99:                                # %.lr.ph9.i.preheader
	movq	MapUnitToSliceGroupMap(%rip), %rax
	movb	(%rax), %dl
	movb	%dl, (%rcx)
	cmpl	$1, %ebx
	je	.LBB0_103
# BB#100:                               # %.lr.ph9..lr.ph9_crit_edge.i.preheader
	movb	1(%rax), %al
	movb	%al, 1(%rcx)
	cmpl	$3, 15348(%r15)
	jb	.LBB0_103
# BB#101:                               # %.lr.ph9..lr.ph9_crit_edge.i..lr.ph9..lr.ph9_crit_edge.i_crit_edge.preheader
	movl	$2, %eax
	.p2align	4, 0x90
.LBB0_102:                              # %.lr.ph9..lr.ph9_crit_edge.i..lr.ph9..lr.ph9_crit_edge.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	MBAmap(%rip), %rcx
	movq	MapUnitToSliceGroupMap(%rip), %rdx
	movl	%eax, %esi
	movzbl	(%rdx,%rsi), %edx
	movb	%dl, (%rcx,%rsi)
	incl	%eax
	cmpl	15348(%r15), %eax
	jb	.LBB0_102
.LBB0_103:                              # %FmoGenerateMBAmap.exit
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_104:
	cmpl	$0, 1152(%rax)
	je	.LBB0_110
# BB#105:                               # %.preheader1.i
	testl	%ebx, %ebx
	je	.LBB0_103
# BB#106:                               # %.lr.ph7.i.preheader
	movq	MapUnitToSliceGroupMap(%rip), %rax
	movb	(%rax), %dl
	movb	%dl, (%rcx)
	cmpl	$1, %ebx
	je	.LBB0_103
# BB#107:                               # %.lr.ph7..lr.ph7_crit_edge.i.preheader
	movb	(%rax), %al
	movb	%al, 1(%rcx)
	cmpl	$3, 15348(%r15)
	jb	.LBB0_103
# BB#108:                               # %.lr.ph7..lr.ph7_crit_edge.i..lr.ph7..lr.ph7_crit_edge.i_crit_edge.preheader
	movl	$2, %eax
	.p2align	4, 0x90
.LBB0_109:                              # %.lr.ph7..lr.ph7_crit_edge.i..lr.ph7..lr.ph7_crit_edge.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	MBAmap(%rip), %rcx
	movq	MapUnitToSliceGroupMap(%rip), %rdx
	movl	%eax, %esi
	shrl	%esi
	movzbl	(%rdx,%rsi), %edx
	movl	%eax, %esi
	movb	%dl, (%rcx,%rsi)
	incl	%eax
	cmpl	15348(%r15), %eax
	jb	.LBB0_109
	jmp	.LBB0_103
.LBB0_23:
	testl	%ebx, %ebx
	je	.LBB0_92
# BB#24:                                # %.lr.ph.i22.i.preheader
	movl	$1, %ebp
	jmp	.LBB0_25
	.p2align	4, 0x90
.LBB0_26:                               # %.lr.ph.i22..lr.ph.i22_crit_edge.i
                                        #   in Loop: Header=BB0_25 Depth=1
	movl	60(%r12), %edi
	movq	MapUnitToSliceGroupMap(%rip), %rcx
	incl	%ebp
.LBB0_25:                               # %.lr.ph.i22.i
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rbp), %esi
	xorl	%edx, %edx
	movl	%esi, %eax
	divl	15336(%r15)
	incl	%edi
	imull	%edi, %eax
	shrl	%eax
	addl	%edx, %eax
	xorl	%edx, %edx
	divl	%edi
	movb	%dl, (%rcx,%rsi)
	cmpl	PicSizeInMapUnits(%rip), %ebp
	jb	.LBB0_26
	jmp	.LBB0_92
.LBB0_27:
	testl	%ebx, %ebx
	je	.LBB0_32
# BB#28:                                # %.lr.ph57.i.i.preheader
	movb	%dil, (%rcx)
	cmpl	$1, %ebx
	je	.LBB0_32
# BB#29:                                # %.lr.ph57.i..lr.ph57.i_crit_edge.i.preheader
	movb	%dil, 1(%rcx)
	movl	60(%r12), %edi
	cmpl	$3, PicSizeInMapUnits(%rip)
	jb	.LBB0_32
# BB#30:                                # %.lr.ph57.i..lr.ph57.i_crit_edge.i..lr.ph57.i..lr.ph57.i_crit_edge.i_crit_edge.preheader
	movl	$2, %eax
	.p2align	4, 0x90
.LBB0_31:                               # %.lr.ph57.i..lr.ph57.i_crit_edge.i..lr.ph57.i..lr.ph57.i_crit_edge.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	MapUnitToSliceGroupMap(%rip), %rcx
	movl	%eax, %edx
	movb	%dil, (%rcx,%rdx)
	incl	%eax
	movl	60(%r12), %edi
	cmpl	PicSizeInMapUnits(%rip), %eax
	jb	.LBB0_31
.LBB0_32:                               # %.preheader47.i.i
	decl	%edi
	js	.LBB0_92
# BB#33:                                # %.lr.ph53.i.i
	movslq	%edi, %rdi
	.p2align	4, 0x90
.LBB0_35:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_38 Depth 2
                                        #       Child Loop BB0_39 Depth 3
                                        #     Child Loop BB0_37 Depth 2
	movl	100(%r12,%rdi,4), %eax
	movl	15336(%r15), %esi
	xorl	%edx, %edx
	divl	%esi
	movl	%eax, %ecx
	movl	%edx, %r8d
	movl	132(%r12,%rdi,4), %eax
	xorl	%edx, %edx
	divl	%esi
	cmpl	%eax, %ecx
	ja	.LBB0_34
# BB#36:                                # %.preheader.lr.ph.i19.i
                                        #   in Loop: Header=BB0_35 Depth=1
	cmpl	%edx, %r8d
	jbe	.LBB0_38
	.p2align	4, 0x90
.LBB0_37:                               # %.preheader.us.i.i
                                        #   Parent Loop BB0_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ecx
	cmpl	%eax, %ecx
	jbe	.LBB0_37
	jmp	.LBB0_34
	.p2align	4, 0x90
.LBB0_38:                               # %.preheader.i20.i
                                        #   Parent Loop BB0_35 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_39 Depth 3
	movl	%r8d, %esi
	.p2align	4, 0x90
.LBB0_39:                               #   Parent Loop BB0_35 Depth=1
                                        #     Parent Loop BB0_38 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	MapUnitToSliceGroupMap(%rip), %rbx
	movl	15336(%r15), %ebp
	imull	%ecx, %ebp
	addl	%esi, %ebp
	movb	%dil, (%rbx,%rbp)
	incl	%esi
	cmpl	%edx, %esi
	jbe	.LBB0_39
# BB#40:                                # %._crit_edge.i21.i
                                        #   in Loop: Header=BB0_38 Depth=2
	incl	%ecx
	cmpl	%eax, %ecx
	jbe	.LBB0_38
.LBB0_34:                               # %.loopexit.i.i
                                        #   in Loop: Header=BB0_35 Depth=1
	decq	%rdi
	testl	%edi, %edi
	jns	.LBB0_35
	jmp	.LBB0_92
.LBB0_41:
	movl	168(%r12), %r10d
	incl	%r10d
	imull	15436(%r15), %r10d
	cmpl	%ebx, %r10d
	cmovgl	%ebx, %r10d
	testl	%ebx, %ebx
	je	.LBB0_42
# BB#43:                                # %.lr.ph109.i.i.preheader
	movb	$2, (%rcx)
	movl	$1, %eax
	cmpl	$1, %ebx
	je	.LBB0_47
# BB#44:                                # %.lr.ph109.i..lr.ph109.i_crit_edge.i.preheader
	movb	$2, 1(%rcx)
	movl	PicSizeInMapUnits(%rip), %eax
	cmpl	$3, %eax
	jb	.LBB0_47
# BB#45:                                # %.lr.ph109.i..lr.ph109.i_crit_edge.i..lr.ph109.i..lr.ph109.i_crit_edge.i_crit_edge.preheader
	movl	$2, %ecx
	.p2align	4, 0x90
.LBB0_46:                               # %.lr.ph109.i..lr.ph109.i_crit_edge.i..lr.ph109.i..lr.ph109.i_crit_edge.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	MapUnitToSliceGroupMap(%rip), %rax
	movl	%ecx, %edx
	movb	$2, (%rax,%rdx)
	incl	%ecx
	movl	PicSizeInMapUnits(%rip), %eax
	cmpl	%eax, %ecx
	jb	.LBB0_46
.LBB0_47:                               # %._crit_edge110.loopexit.i.i
	testl	%eax, %eax
	sete	%al
	testb	%al, %al
	jne	.LBB0_92
	jmp	.LBB0_49
.LBB0_67:
	movl	164(%r12), %edi
	movl	168(%r12), %edx
	incl	%edx
	imull	15436(%r15), %edx
	cmpl	%ebx, %edx
	cmovgl	%ebx, %edx
	movl	%ebx, %eax
	subl	%edx, %eax
	testl	%edi, %edi
	cmovel	%edx, %eax
	testl	%ebx, %ebx
	je	.LBB0_92
# BB#68:                                # %.lr.ph.i10.preheader.i
	movl	$1, %esi
	subl	%edi, %esi
	testl	%eax, %eax
	movl	%edi, %edx
	jne	.LBB0_70
# BB#69:                                # %.lr.ph.i10.preheader.i
	movl	%esi, %edx
.LBB0_70:                               # %.lr.ph.i10.preheader.i
	movb	%dl, (%rcx)
	cmpl	$1, %ebx
	je	.LBB0_92
# BB#71:                                # %.lr.ph..lr.ph_crit_edge.i12.i.preheader
	cmpl	$1, %eax
	movl	%edi, %edx
	ja	.LBB0_73
# BB#72:                                # %.lr.ph..lr.ph_crit_edge.i12.i.preheader
	movl	%esi, %edx
.LBB0_73:                               # %.lr.ph..lr.ph_crit_edge.i12.i.preheader
	movb	%dl, 1(%rcx)
	cmpl	$3, %ebx
	jb	.LBB0_92
# BB#74:                                # %.lr.ph..lr.ph_crit_edge.i12..lr.ph..lr.ph_crit_edge.i12_crit_edge.i.preheader
	cmpl	$2, %eax
	ja	.LBB0_76
# BB#75:                                # %.lr.ph..lr.ph_crit_edge.i12..lr.ph..lr.ph_crit_edge.i12_crit_edge.i.preheader
	movl	$1, %edx
	subl	%edi, %edx
	movb	%dl, %dil
.LBB0_76:                               # %.lr.ph..lr.ph_crit_edge.i12..lr.ph..lr.ph_crit_edge.i12_crit_edge.i.preheader
	movb	%dil, 2(%rcx)
	cmpl	$4, PicSizeInMapUnits(%rip)
	jb	.LBB0_92
# BB#77:                                # %.lr.ph..lr.ph_crit_edge.i12..lr.ph..lr.ph_crit_edge.i12_crit_edge.i..lr.ph..lr.ph_crit_edge.i12..lr.ph..lr.ph_crit_edge.i12_crit_edge.i_crit_edge.preheader
	movl	$3, %ecx
	.p2align	4, 0x90
.LBB0_78:                               # %.lr.ph..lr.ph_crit_edge.i12..lr.ph..lr.ph_crit_edge.i12_crit_edge.i..lr.ph..lr.ph_crit_edge.i12..lr.ph..lr.ph_crit_edge.i12_crit_edge.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movl	164(%r12), %edx
	movq	MapUnitToSliceGroupMap(%rip), %rsi
	movl	$1, %edi
	subl	%edx, %edi
	cmpl	%eax, %ecx
	cmovbl	%edx, %edi
	movl	%ecx, %edx
	movb	%dil, (%rsi,%rdx)
	incl	%ecx
	cmpl	PicSizeInMapUnits(%rip), %ecx
	jb	.LBB0_78
	jmp	.LBB0_92
.LBB0_79:
	movl	168(%r12), %eax
	incl	%eax
	imull	15436(%r15), %eax
	cmpl	%ebx, %eax
	cmovgl	%ebx, %eax
	subl	%eax, %ebx
	cmpl	$0, 164(%r12)
	cmovel	%eax, %ebx
	movl	15336(%r15), %eax
	testl	%eax, %eax
	je	.LBB0_92
# BB#80:                                # %.preheader.lr.ph.i.i
	movl	15340(%r15), %esi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_81:                               # %.preheader.i3.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_84 Depth 2
	testl	%esi, %esi
	je	.LBB0_82
# BB#83:                                # %.lr.ph.i4.i.preheader
                                        #   in Loop: Header=BB0_81 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_84:                               # %.lr.ph.i4.i
                                        #   Parent Loop BB0_81 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r8,%rdi), %esi
	movl	164(%r12), %ebp
	movl	$1, %ecx
	subl	%ebp, %ecx
	imull	%edi, %eax
	cmpl	%ebx, %esi
	cmovbl	%ebp, %ecx
	movq	MapUnitToSliceGroupMap(%rip), %rsi
	addl	%edx, %eax
	movb	%cl, (%rsi,%rax)
	incl	%edi
	movl	15336(%r15), %eax
	movl	15340(%r15), %esi
	cmpl	%esi, %edi
	jb	.LBB0_84
# BB#85:                                # %._crit_edge.i5.i.loopexit
                                        #   in Loop: Header=BB0_81 Depth=1
	addl	%edi, %r8d
	jmp	.LBB0_86
	.p2align	4, 0x90
.LBB0_82:                               #   in Loop: Header=BB0_81 Depth=1
	xorl	%esi, %esi
.LBB0_86:                               # %._crit_edge.i5.i
                                        #   in Loop: Header=BB0_81 Depth=1
	incl	%edx
	cmpl	%eax, %edx
	jb	.LBB0_81
	jmp	.LBB0_92
.LBB0_87:
	testl	%ebx, %ebx
	je	.LBB0_92
# BB#88:                                # %.lr.ph.i.i
	movq	176(%r12), %rax
	movb	(%rax), %dl
	movb	%dl, (%rcx)
	cmpl	$1, %ebx
	je	.LBB0_92
# BB#89:                                # %._crit_edge.i.preheader
	movb	1(%rax), %al
	movb	%al, 1(%rcx)
	cmpl	$3, PicSizeInMapUnits(%rip)
	jb	.LBB0_92
# BB#90:                                # %._crit_edge.i.._crit_edge.i_crit_edge.preheader
	movl	$2, %eax
	.p2align	4, 0x90
.LBB0_91:                               # %._crit_edge.i.._crit_edge.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	MapUnitToSliceGroupMap(%rip), %rcx
	movq	176(%r12), %rdx
	movl	%eax, %esi
	movzbl	(%rdx,%rsi), %edx
	movb	%dl, (%rcx,%rsi)
	incl	%eax
	cmpl	PicSizeInMapUnits(%rip), %eax
	jb	.LBB0_91
	jmp	.LBB0_92
.LBB0_110:                              # %.preheader.i8
	testl	%ebx, %ebx
	je	.LBB0_103
# BB#111:                               # %.lr.ph.i.preheader
	movl	$1, %ebp
	jmp	.LBB0_112
	.p2align	4, 0x90
.LBB0_113:                              # %._crit_edge.i9
                                        #   in Loop: Header=BB0_112 Depth=1
	movq	MBAmap(%rip), %rcx
	incl	%ebp
.LBB0_112:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rbp), %esi
	movq	MapUnitToSliceGroupMap(%rip), %r8
	movl	15336(%r15), %ebx
	leal	(%rbx,%rbx), %edi
	xorl	%edx, %edx
	movl	%esi, %eax
	divl	%edi
	movl	%eax, %edi
	imull	%ebx, %edi
	xorl	%edx, %edx
	movl	%esi, %eax
	divl	%ebx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	addl	%edi, %edx
	movzbl	(%r8,%rdx), %eax
	movb	%al, (%rcx,%rsi)
	cmpl	15348(%r15), %ebp
	jb	.LBB0_113
	jmp	.LBB0_103
.LBB0_42:
	movb	$1, %al
	testb	%al, %al
	jne	.LBB0_92
.LBB0_49:                               # %.lr.ph.preheader.i.i
	movl	15336(%r15), %ebx
	movl	15340(%r15), %ecx
	movl	164(%r12), %eax
	movl	%ebx, %esi
	subl	%eax, %esi
	shrl	%esi
	subl	%eax, %ecx
	shrl	%ecx
	leal	-1(%rax), %edi
	xorl	%edx, %edx
	movl	%ecx, %r8d
	movl	%esi, %r13d
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	%esi, %r11d
	jmp	.LBB0_50
	.p2align	4, 0x90
.LBB0_66:                               # %..lr.ph_crit_edge.i.i
                                        #   in Loop: Header=BB0_50 Depth=1
	movl	15336(%r15), %ebx
.LBB0_50:                               # %.lr.ph.i14.i
                                        # =>This Inner Loop Header: Depth=1
	movq	MapUnitToSliceGroupMap(%rip), %r9
	imull	%ecx, %ebx
	addl	%esi, %ebx
	xorl	%ebp, %ebp
	cmpb	$2, (%r9,%rbx)
	sete	%r14b
	jne	.LBB0_52
# BB#51:                                #   in Loop: Header=BB0_50 Depth=1
	cmpl	%r10d, %edx
	setae	(%r9,%rbx)
.LBB0_52:                               #   in Loop: Header=BB0_50 Depth=1
	movb	%r14b, %bpl
	cmpl	$-1, %edi
	jne	.LBB0_55
# BB#53:                                #   in Loop: Header=BB0_50 Depth=1
	cmpl	%r11d, %esi
	jne	.LBB0_55
# BB#54:                                #   in Loop: Header=BB0_50 Depth=1
	xorl	%edi, %edi
	decl	%esi
	cmovsl	%edi, %esi
	movl	164(%r12), %eax
	leal	-1(%rax,%rax), %eax
	movl	%esi, %r11d
	jmp	.LBB0_65
	.p2align	4, 0x90
.LBB0_55:                               #   in Loop: Header=BB0_50 Depth=1
	cmpl	$1, %edi
	jne	.LBB0_58
# BB#56:                                #   in Loop: Header=BB0_50 Depth=1
	cmpl	%r13d, %esi
	jne	.LBB0_58
# BB#57:                                #   in Loop: Header=BB0_50 Depth=1
	incl	%esi
	movl	15336(%r15), %r13d
	decl	%r13d
	cmpl	%r13d, %esi
	cmovlel	%esi, %r13d
	movl	164(%r12), %esi
	addl	%esi, %esi
	movl	$1, %eax
	subl	%esi, %eax
	xorl	%edi, %edi
	movl	%r13d, %esi
	jmp	.LBB0_65
	.p2align	4, 0x90
.LBB0_58:                               #   in Loop: Header=BB0_50 Depth=1
	cmpl	$-1, %eax
	jne	.LBB0_61
# BB#59:                                #   in Loop: Header=BB0_50 Depth=1
	cmpl	12(%rsp), %ecx          # 4-byte Folded Reload
	jne	.LBB0_61
# BB#60:                                #   in Loop: Header=BB0_50 Depth=1
	xorl	%eax, %eax
	decl	%ecx
	cmovsl	%eax, %ecx
	movl	164(%r12), %ebx
	addl	%ebx, %ebx
	movl	$1, %edi
	subl	%ebx, %edi
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	jmp	.LBB0_65
.LBB0_61:                               #   in Loop: Header=BB0_50 Depth=1
	cmpl	$1, %eax
	jne	.LBB0_64
# BB#62:                                #   in Loop: Header=BB0_50 Depth=1
	cmpl	%r8d, %ecx
	jne	.LBB0_64
# BB#63:                                #   in Loop: Header=BB0_50 Depth=1
	incl	%ecx
	movl	15340(%r15), %r8d
	decl	%r8d
	cmpl	%r8d, %ecx
	cmovlel	%ecx, %r8d
	movl	164(%r12), %eax
	leal	-1(%rax,%rax), %edi
	xorl	%eax, %eax
	movl	%r8d, %ecx
	jmp	.LBB0_65
.LBB0_64:                               #   in Loop: Header=BB0_50 Depth=1
	addl	%edi, %esi
	addl	%eax, %ecx
	.p2align	4, 0x90
.LBB0_65:                               #   in Loop: Header=BB0_50 Depth=1
	addl	%ebp, %edx
	cmpl	PicSizeInMapUnits(%rip), %edx
	jb	.LBB0_66
	jmp	.LBB0_92
.LBB0_6:
	movl	$.L.str.1, %edi
	jmp	.LBB0_7
.LBB0_95:
	movl	$.L.str.3, %edi
.LBB0_7:
	xorl	%eax, %eax
	movl	%ebx, %esi
	jmp	.LBB0_8
.LBB0_114:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
.LBB0_8:
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end0:
	.size	FmoInit, .Lfunc_end0-FmoInit
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_13
	.quad	.LBB0_23
	.quad	.LBB0_27
	.quad	.LBB0_41
	.quad	.LBB0_67
	.quad	.LBB0_79
	.quad	.LBB0_87

	.text
	.globl	FmoUninit
	.p2align	4, 0x90
	.type	FmoUninit,@function
FmoUninit:                              # @FmoUninit
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movq	MBAmap(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	callq	free
	movq	$0, MBAmap(%rip)
.LBB1_2:
	movq	MapUnitToSliceGroupMap(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#3:
	callq	free
	movq	$0, MapUnitToSliceGroupMap(%rip)
.LBB1_4:
	popq	%rax
	retq
.Lfunc_end1:
	.size	FmoUninit, .Lfunc_end1-FmoUninit
	.cfi_endproc

	.globl	FmoStartPicture
	.p2align	4, 0x90
	.type	FmoStartPicture,@function
FmoStartPicture:                        # @FmoStartPicture
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	movslq	15348(%rax), %rax
	testq	%rax, %rax
	jle	.LBB2_26
# BB#1:                                 # %.split.us.preheader.preheader
	movq	MBAmap(%rip), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_2:                                # %.split.us.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rcx,%rdx)
	je	.LBB2_4
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB2_2
.LBB2_4:                                # %FmoGetFirstMBOfSliceGroup.exit.loopexit.us
	cmpl	%eax, %edx
	movl	$-1, %esi
	cmovll	%edx, %esi
	movl	%esi, FirstMBInSlice(%rip)
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_5:                                # =>This Inner Loop Header: Depth=1
	cmpb	$1, (%rcx,%rdx)
	je	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB2_5
.LBB2_7:                                # %FmoGetFirstMBOfSliceGroup.exit.loopexit.us.1
	cmpl	%eax, %edx
	movl	$-1, %esi
	cmovll	%edx, %esi
	movl	%esi, FirstMBInSlice+4(%rip)
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_8:                                # =>This Inner Loop Header: Depth=1
	cmpb	$2, (%rcx,%rdx)
	je	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_8 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB2_8
.LBB2_10:                               # %FmoGetFirstMBOfSliceGroup.exit.loopexit.us.2
	cmpl	%eax, %edx
	movl	$-1, %esi
	cmovll	%edx, %esi
	movl	%esi, FirstMBInSlice+8(%rip)
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_11:                               # =>This Inner Loop Header: Depth=1
	cmpb	$3, (%rcx,%rdx)
	je	.LBB2_13
# BB#12:                                #   in Loop: Header=BB2_11 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB2_11
.LBB2_13:                               # %FmoGetFirstMBOfSliceGroup.exit.loopexit.us.3
	cmpl	%eax, %edx
	movl	$-1, %esi
	cmovll	%edx, %esi
	movl	%esi, FirstMBInSlice+12(%rip)
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_14:                               # =>This Inner Loop Header: Depth=1
	cmpb	$4, (%rcx,%rdx)
	je	.LBB2_16
# BB#15:                                #   in Loop: Header=BB2_14 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB2_14
.LBB2_16:                               # %FmoGetFirstMBOfSliceGroup.exit.loopexit.us.4
	cmpl	%eax, %edx
	movl	$-1, %esi
	cmovll	%edx, %esi
	movl	%esi, FirstMBInSlice+16(%rip)
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_17:                               # =>This Inner Loop Header: Depth=1
	cmpb	$5, (%rcx,%rdx)
	je	.LBB2_19
# BB#18:                                #   in Loop: Header=BB2_17 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB2_17
.LBB2_19:                               # %FmoGetFirstMBOfSliceGroup.exit.loopexit.us.5
	cmpl	%eax, %edx
	movl	$-1, %esi
	cmovll	%edx, %esi
	movl	%esi, FirstMBInSlice+20(%rip)
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_20:                               # =>This Inner Loop Header: Depth=1
	cmpb	$6, (%rcx,%rdx)
	je	.LBB2_22
# BB#21:                                #   in Loop: Header=BB2_20 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB2_20
.LBB2_22:                               # %FmoGetFirstMBOfSliceGroup.exit.loopexit.us.6
	cmpl	%eax, %edx
	movl	$-1, %esi
	cmovll	%edx, %esi
	movl	%esi, FirstMBInSlice+24(%rip)
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_23:                               # =>This Inner Loop Header: Depth=1
	cmpb	$7, (%rcx,%rdx)
	je	.LBB2_25
# BB#24:                                #   in Loop: Header=BB2_23 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB2_23
.LBB2_25:                               # %FmoGetFirstMBOfSliceGroup.exit.loopexit.us.7
	cmpl	%eax, %edx
	movl	$-1, %eax
	cmovll	%edx, %eax
	jmp	.LBB2_27
.LBB2_26:                               # %.split.preheader
	pcmpeqd	%xmm0, %xmm0
	movdqa	%xmm0, FirstMBInSlice(%rip)
	movq	$-1, FirstMBInSlice+16(%rip)
	movl	$-1, FirstMBInSlice+24(%rip)
	movl	$-1, %eax
.LBB2_27:                               # %.us-lcssa.us
	movl	%eax, FirstMBInSlice+28(%rip)
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	FmoStartPicture, .Lfunc_end2-FmoStartPicture
	.cfi_endproc

	.globl	FmoGetFirstMBOfSliceGroup
	.p2align	4, 0x90
	.type	FmoGetFirstMBOfSliceGroup,@function
FmoGetFirstMBOfSliceGroup:              # @FmoGetFirstMBOfSliceGroup
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	movslq	15348(%rax), %rax
	testq	%rax, %rax
	jle	.LBB3_1
# BB#2:                                 # %.lr.ph
	movq	MBAmap(%rip), %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx,%rcx), %esi
	cmpl	%edi, %esi
	je	.LBB3_5
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB3_3
	jmp	.LBB3_5
.LBB3_1:
	xorl	%ecx, %ecx
.LBB3_5:                                # %.critedge
	cmpl	%eax, %ecx
	movl	$-1, %eax
	cmovll	%ecx, %eax
	retq
.Lfunc_end3:
	.size	FmoGetFirstMBOfSliceGroup, .Lfunc_end3-FmoGetFirstMBOfSliceGroup
	.cfi_endproc

	.globl	FmoEndPicture
	.p2align	4, 0x90
	.type	FmoEndPicture,@function
FmoEndPicture:                          # @FmoEndPicture
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end4:
	.size	FmoEndPicture, .Lfunc_end4-FmoEndPicture
	.cfi_endproc

	.globl	FmoMB2SliceGroup
	.p2align	4, 0x90
	.type	FmoMB2SliceGroup,@function
FmoMB2SliceGroup:                       # @FmoMB2SliceGroup
	.cfi_startproc
# BB#0:
	movq	MBAmap(%rip), %rax
	movslq	%edi, %rcx
	movzbl	(%rax,%rcx), %eax
	retq
.Lfunc_end5:
	.size	FmoMB2SliceGroup, .Lfunc_end5-FmoMB2SliceGroup
	.cfi_endproc

	.globl	FmoGetNextMBNr
	.p2align	4, 0x90
	.type	FmoGetNextMBNr,@function
FmoGetNextMBNr:                         # @FmoGetNextMBNr
	.cfi_startproc
# BB#0:
	movq	MBAmap(%rip), %rax
	movslq	%edi, %rdi
	movb	(%rax,%rdi), %dl
	movq	img(%rip), %rcx
	movslq	15348(%rcx), %rcx
	incq	%rdi
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rsi
	cmpq	%rcx, %rsi
	jge	.LBB6_3
# BB#2:                                 #   in Loop: Header=BB6_1 Depth=1
	leaq	1(%rsi), %rdi
	cmpb	%dl, (%rax,%rsi)
	jne	.LBB6_1
.LBB6_3:                                # %.critedge
	cmpl	%ecx, %esi
	movl	$-1, %eax
	cmovll	%esi, %eax
	retq
.Lfunc_end6:
	.size	FmoGetNextMBNr, .Lfunc_end6-FmoGetNextMBNr
	.cfi_endproc

	.globl	FmoGetPreviousMBNr
	.p2align	4, 0x90
	.type	FmoGetPreviousMBNr,@function
FmoGetPreviousMBNr:                     # @FmoGetPreviousMBNr
	.cfi_startproc
# BB#0:
	movq	MBAmap(%rip), %rax
	movslq	%edi, %rsi
	movb	(%rax,%rsi), %dl
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rcx
	testq	%rcx, %rcx
	jle	.LBB7_3
# BB#2:                                 #   in Loop: Header=BB7_1 Depth=1
	leaq	-1(%rcx), %rsi
	cmpb	%dl, -1(%rax,%rcx)
	jne	.LBB7_1
.LBB7_3:                                # %.critedge
	leal	-1(%rcx), %edx
	testl	%ecx, %ecx
	movl	$-1, %eax
	cmovgl	%edx, %eax
	retq
.Lfunc_end7:
	.size	FmoGetPreviousMBNr, .Lfunc_end7-FmoGetPreviousMBNr
	.cfi_endproc

	.globl	FmoGetLastCodedMBOfSliceGroup
	.p2align	4, 0x90
	.type	FmoGetLastCodedMBOfSliceGroup,@function
FmoGetLastCodedMBOfSliceGroup:          # @FmoGetLastCodedMBOfSliceGroup
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	movslq	15348(%rax), %r9
	testq	%r9, %r9
	jle	.LBB8_1
# BB#2:                                 # %.lr.ph
	movq	MBAmap(%rip), %r11
	leaq	-1(%r9), %r8
	movq	%r9, %r10
	andq	$3, %r10
	je	.LBB8_3
# BB#4:                                 # %.prol.preheader
	movl	$-1, %eax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB8_5:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%r11,%rsi), %ecx
	cmpl	%edi, %ecx
	cmovel	%esi, %eax
	incq	%rsi
	cmpq	%rsi, %r10
	jne	.LBB8_5
	jmp	.LBB8_6
.LBB8_1:
	movl	$-1, %eax
	retq
.LBB8_3:
	xorl	%esi, %esi
	movl	$-1, %eax
.LBB8_6:                                # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB8_8
	.p2align	4, 0x90
.LBB8_7:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%r11,%rsi), %ecx
	cmpl	%edi, %ecx
	cmovel	%esi, %eax
	movzbl	1(%r11,%rsi), %ecx
	leal	1(%rsi), %edx
	cmpl	%edi, %ecx
	cmovnel	%eax, %edx
	movzbl	2(%r11,%rsi), %eax
	leal	2(%rsi), %ecx
	cmpl	%edi, %eax
	cmovnel	%edx, %ecx
	movzbl	3(%r11,%rsi), %edx
	leal	3(%rsi), %eax
	cmpl	%edi, %edx
	cmovnel	%ecx, %eax
	addq	$4, %rsi
	cmpq	%r9, %rsi
	jl	.LBB8_7
.LBB8_8:                                # %._crit_edge
	retq
.Lfunc_end8:
	.size	FmoGetLastCodedMBOfSliceGroup, .Lfunc_end8-FmoGetLastCodedMBOfSliceGroup
	.cfi_endproc

	.globl	FmoSetLastMacroblockInSlice
	.p2align	4, 0x90
	.type	FmoSetLastMacroblockInSlice,@function
FmoSetLastMacroblockInSlice:            # @FmoSetLastMacroblockInSlice
	.cfi_startproc
# BB#0:
	movq	MBAmap(%rip), %rcx
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %eax
	movq	img(%rip), %rdx
	movslq	15348(%rdx), %rdx
	incq	%rdi
	.p2align	4, 0x90
.LBB9_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rsi
	cmpq	%rdx, %rsi
	jge	.LBB9_3
# BB#2:                                 #   in Loop: Header=BB9_1 Depth=1
	leaq	1(%rsi), %rdi
	cmpb	%al, (%rcx,%rsi)
	jne	.LBB9_1
.LBB9_3:                                # %FmoGetNextMBNr.exit
	cmpl	%edx, %esi
	movl	$-1, %ecx
	cmovll	%esi, %ecx
	movl	%ecx, FirstMBInSlice(,%rax,4)
	retq
.Lfunc_end9:
	.size	FmoSetLastMacroblockInSlice, .Lfunc_end9-FmoSetLastMacroblockInSlice
	.cfi_endproc

	.globl	FmoGetFirstMacroblockInSlice
	.p2align	4, 0x90
	.type	FmoGetFirstMacroblockInSlice,@function
FmoGetFirstMacroblockInSlice:           # @FmoGetFirstMacroblockInSlice
	.cfi_startproc
# BB#0:
	movslq	%edi, %rax
	movl	FirstMBInSlice(,%rax,4), %eax
	retq
.Lfunc_end10:
	.size	FmoGetFirstMacroblockInSlice, .Lfunc_end10-FmoGetFirstMacroblockInSlice
	.cfi_endproc

	.globl	FmoSliceGroupCompletelyCoded
	.p2align	4, 0x90
	.type	FmoSliceGroupCompletelyCoded,@function
FmoSliceGroupCompletelyCoded:           # @FmoSliceGroupCompletelyCoded
	.cfi_startproc
# BB#0:
	movslq	%edi, %rax
	movl	FirstMBInSlice(,%rax,4), %eax
	shrl	$31, %eax
	retq
.Lfunc_end11:
	.size	FmoSliceGroupCompletelyCoded, .Lfunc_end11-FmoSliceGroupCompletelyCoded
	.cfi_endproc

	.type	MBAmap,@object          # @MBAmap
	.bss
	.globl	MBAmap
	.p2align	3
MBAmap:
	.quad	0
	.size	MBAmap, 8

	.type	MapUnitToSliceGroupMap,@object # @MapUnitToSliceGroupMap
	.globl	MapUnitToSliceGroupMap
	.p2align	3
MapUnitToSliceGroupMap:
	.quad	0
	.size	MapUnitToSliceGroupMap, 8

	.type	FirstMBInSlice,@object  # @FirstMBInSlice
	.local	FirstMBInSlice
	.comm	FirstMBInSlice,32,16
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	PicSizeInMapUnits,@object # @PicSizeInMapUnits
	.comm	PicSizeInMapUnits,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"wrong pps->pic_size_in_map_units_minus1 for used SPS and FMO type 6"
	.size	.L.str, 68

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"cannot allocated %d bytes for MapUnitToSliceGroupMap, exit\n"
	.size	.L.str.1, 60

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Illegal slice_group_map_type %d , exit \n"
	.size	.L.str.2, 41

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"cannot allocated %d bytes for MBAmap, exit\n"
	.size	.L.str.3, 44


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
