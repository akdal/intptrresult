	.text
	.file	"fft.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4614256656552045848     # double 3.1415926535897931
.LCPI0_1:
	.quad	4611686018427387904     # double 2
.LCPI0_2:
	.quad	4607182418800017408     # double 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_3:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	fft
	.p2align	4, 0x90
	.type	fft,@function
fft:                                    # @fft
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movl	%edi, %eax
	sarl	$31, %eax
	movq	%rdi, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leal	(%rdi,%rax), %r15d
	movl	%r15d, %ecx
	xorl	%eax, %ecx
	movl	%ecx, %r12d
	sarl	$31, %r12d
	shrl	$30, %r12d
	addl	%ecx, %r12d
	sarl	$2, %r12d
	xorl	%eax, %r15d
	je	.LBB0_2
# BB#1:
	cmpl	fft.last_n(%rip), %r15d
	je	.LBB0_38
.LBB0_2:
	movl	%r15d, fft.last_n(%rip)
	movq	fft.sintbl(%rip), %rdi
	leal	(%r12,%r15), %eax
	movslq	%eax, %rsi
	shlq	$2, %rsi
	callq	realloc
	movq	%rax, fft.sintbl(%rip)
	movq	fft.bitrev(%rip), %rdi
	movslq	%r15d, %rsi
	shlq	$2, %rsi
	callq	realloc
	movq	%rax, fft.bitrev(%rip)
	testq	%rax, %rax
	je	.LBB0_74
# BB#3:
	movq	fft.sintbl(%rip), %r13
	testq	%r13, %r13
	je	.LBB0_74
# BB#4:
	movl	%r15d, %ebx
	sarl	$31, %ebx
	shrl	$29, %ebx
	addl	%r15d, %ebx
	cvtsi2sdl	%r15d, %xmm1
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	callq	sin
	movapd	%xmm0, %xmm5
	addsd	%xmm5, %xmm5
	mulsd	%xmm0, %xmm5
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	subsd	%xmm5, %xmm1
	mulsd	%xmm5, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_6
# BB#5:                                 # %call.sqrt
	movapd	%xmm1, %xmm0
	movsd	%xmm5, 8(%rsp)          # 8-byte Spill
	callq	sqrt
	movsd	8(%rsp), %xmm5          # 8-byte Reload
                                        # xmm5 = mem[0],zero
.LBB0_6:                                # %.split
	movl	%r15d, %eax
	shrl	$31, %eax
	sarl	$3, %ebx
	movslq	%r12d, %rcx
	movl	$1065353216, (%r13,%rcx,4) # imm = 0x3F800000
	movl	$0, (%r13)
	cmpl	$16, %r15d
	jl	.LBB0_9
# BB#7:                                 # %.lr.ph82.preheader.i
	movapd	%xmm5, %xmm1
	addsd	%xmm1, %xmm1
	movslq	%ebx, %rdx
	leaq	-4(%r13,%rcx,4), %rsi
	movsd	.LCPI0_2(%rip), %xmm2   # xmm2 = mem[0],zero
	xorpd	%xmm3, %xmm3
	movl	$1, %edi
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph82.i
                                        # =>This Inner Loop Header: Depth=1
	subsd	%xmm5, %xmm2
	movapd	%xmm1, %xmm4
	mulsd	%xmm2, %xmm4
	addsd	%xmm4, %xmm5
	addsd	%xmm0, %xmm3
	movapd	%xmm1, %xmm4
	mulsd	%xmm3, %xmm4
	subsd	%xmm4, %xmm0
	xorps	%xmm4, %xmm4
	cvtsd2ss	%xmm3, %xmm4
	movss	%xmm4, (%r13,%rdi,4)
	xorps	%xmm4, %xmm4
	cvtsd2ss	%xmm2, %xmm4
	movss	%xmm4, (%rsi)
	incq	%rdi
	addq	$-4, %rsi
	cmpq	%rdx, %rdi
	jl	.LBB0_8
.LBB0_9:                                # %._crit_edge83.i
	addl	%r15d, %eax
	leal	7(%r15), %edx
	cmpl	$15, %edx
	jb	.LBB0_11
# BB#10:
	movslq	%ebx, %rdx
	movl	$1060439283, (%r13,%rdx,4) # imm = 0x3F3504F3
.LBB0_11:                               # %.preheader72.i
	sarl	%eax
	cmpl	$4, %r15d
	jl	.LBB0_14
# BB#12:                                # %.lr.ph75.preheader.i
	movslq	%eax, %rdx
	leaq	(%r13,%rdx,4), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph75.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rsi,4), %edi
	movl	%edi, (%rdx)
	incq	%rsi
	addq	$-4, %rdx
	cmpq	%rcx, %rsi
	jl	.LBB0_13
.LBB0_14:                               # %.preheader.i
	movl	%eax, %esi
	addl	%r12d, %esi
	jle	.LBB0_26
# BB#15:                                # %.lr.ph.preheader.i
	movslq	%eax, %rcx
	movl	%esi, %eax
	cmpl	$7, %esi
	jbe	.LBB0_19
# BB#16:                                # %min.iters.checked
	andl	$7, %esi
	movq	%rax, %rdx
	subq	%rsi, %rdx
	je	.LBB0_19
# BB#17:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rdi
	leaq	(%r13,%rax,4), %rbp
	cmpq	%rbp, %rdi
	jae	.LBB0_71
# BB#18:                                # %vector.memcheck
	leaq	(%rcx,%rax), %rdi
	leaq	(%r13,%rdi,4), %rdi
	cmpq	%rdi, %r13
	jae	.LBB0_71
.LBB0_19:
	xorl	%edx, %edx
.LBB0_20:                               # %.lr.ph.i.preheader
	movl	%eax, %edi
	subl	%edx, %edi
	leaq	-1(%rax), %rsi
	subq	%rdx, %rsi
	andq	$3, %rdi
	je	.LBB0_23
# BB#21:                                # %.lr.ph.i.prol.preheader
	leaq	(%r13,%rcx,4), %rbp
	negq	%rdi
	movapd	.LCPI0_3(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB0_22:                               # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movss	(%r13,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	xorpd	%xmm0, %xmm1
	movss	%xmm1, (%rbp,%rdx,4)
	incq	%rdx
	incq	%rdi
	jne	.LBB0_22
.LBB0_23:                               # %.lr.ph.i.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB0_26
# BB#24:                                # %.lr.ph.i.preheader.new
	subq	%rdx, %rax
	addq	%rdx, %rcx
	leaq	12(%r13,%rcx,4), %rcx
	leaq	12(%r13,%rdx,4), %rdx
	movapd	.LCPI0_3(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB0_25:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movss	-12(%rdx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorpd	%xmm0, %xmm1
	movss	%xmm1, -12(%rcx)
	movss	-8(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	xorpd	%xmm0, %xmm1
	movss	%xmm1, -8(%rcx)
	movss	-4(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	xorpd	%xmm0, %xmm1
	movss	%xmm1, -4(%rcx)
	movss	(%rdx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	xorpd	%xmm0, %xmm1
	movss	%xmm1, (%rcx)
	addq	$16, %rcx
	addq	$16, %rdx
	addq	$-4, %rax
	jne	.LBB0_25
.LBB0_26:                               # %make_sintbl.exit
	movq	fft.bitrev(%rip), %rax
	movl	$0, (%rax)
	cmpl	$2, %r15d
	jl	.LBB0_38
# BB#27:                                # %.preheader.preheader.i
	testb	$1, %r15b
	jne	.LBB0_31
# BB#28:                                # %.preheader.i135.prol
	xorl	%edx, %edx
	movl	%r15d, %ecx
	.p2align	4, 0x90
.LBB0_29:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	movl	%ecx, %edi
	shrl	$31, %edi
	addl	%edi, %ecx
	sarl	%ecx
	subl	%ecx, %edx
	jge	.LBB0_29
# BB#30:
	addl	%esi, %ecx
	movl	%ecx, 4(%rax)
	movl	$2, %edx
	cmpl	$2, %r15d
	jne	.LBB0_32
	jmp	.LBB0_38
.LBB0_31:
	xorl	%ecx, %ecx
	movl	$1, %edx
	cmpl	$2, %r15d
	je	.LBB0_38
.LBB0_32:                               # %.preheader.preheader.i.new
	movl	%r15d, %esi
	.p2align	4, 0x90
.LBB0_33:                               # %.preheader.i135
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_34 Depth 2
                                        #     Child Loop BB0_36 Depth 2
	movl	%r15d, %ebx
	.p2align	4, 0x90
.LBB0_34:                               #   Parent Loop BB0_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %ebp
	movl	%ebx, %edi
	shrl	$31, %edi
	addl	%edi, %ebx
	sarl	%ebx
	subl	%ebx, %ecx
	jge	.LBB0_34
# BB#35:                                # %.preheader.i135.1204
                                        #   in Loop: Header=BB0_33 Depth=1
	addl	%ebp, %ebx
	movl	%ebx, (%rax,%rdx,4)
	movl	%r15d, %ecx
	.p2align	4, 0x90
.LBB0_36:                               #   Parent Loop BB0_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %ebp
	movl	%ecx, %edi
	shrl	$31, %edi
	addl	%edi, %ecx
	sarl	%ecx
	subl	%ecx, %ebx
	jge	.LBB0_36
# BB#37:                                #   in Loop: Header=BB0_33 Depth=1
	addl	%ebp, %ecx
	movl	%ecx, 4(%rax,%rdx,4)
	addq	$2, %rdx
	cmpq	%rsi, %rdx
	jne	.LBB0_33
.LBB0_38:                               # %make_bitrev.exit.preheader
	testl	%r15d, %r15d
	jle	.LBB0_69
# BB#39:                                # %.lr.ph151
	movq	16(%rsp), %rax          # 8-byte Reload
	shrl	$31, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	fft.bitrev(%rip), %rax
	movl	%r15d, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	%ecx, %edi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB0_40:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rax,%rcx,4), %rsi
	cmpq	%rsi, %rcx
	jge	.LBB0_42
# BB#41:                                #   in Loop: Header=BB0_40 Depth=1
	shlq	$4, %rsi
	movupd	(%rdx), %xmm0
	movupd	(%r14,%rsi), %xmm1
	movupd	%xmm1, (%rdx)
	cvtpd2ps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm0
	movupd	%xmm0, (%r14,%rsi)
.LBB0_42:                               # %make_bitrev.exit
                                        #   in Loop: Header=BB0_40 Depth=1
	incq	%rcx
	addq	$16, %rdx
	cmpq	%rcx, %rdi
	jne	.LBB0_40
# BB#43:                                # %.preheader139
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	cmpl	$2, %r15d
	jl	.LBB0_58
# BB#44:                                # %.lr.ph148
	movq	fft.sintbl(%rip), %r13
	movslq	%r15d, %rsi
	movslq	%r12d, %r11
	movl	$1, %r10d
	movaps	.LCPI0_3(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB0_45:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_48 Depth 2
                                        #       Child Loop BB0_50 Depth 3
                                        #     Child Loop BB0_53 Depth 2
                                        #       Child Loop BB0_55 Depth 3
	movl	%r10d, %edi
	leal	(%rdi,%rdi), %r10d
	testl	%edi, %edi
	jle	.LBB0_57
# BB#46:                                # %.lr.ph146
                                        #   in Loop: Header=BB0_45 Depth=1
	movl	%r15d, %eax
	cltd
	idivl	%r10d
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movslq	%r10d, %rcx
	movslq	%edi, %rbx
	movslq	%eax, %r12
	movl	%edi, %r14d
	movq	%rcx, %rdx
	je	.LBB0_52
# BB#47:                                # %.lr.ph146.split.preheader
                                        #   in Loop: Header=BB0_45 Depth=1
	shlq	$4, %rdx
	shlq	$4, %rbx
	movq	8(%rsp), %r9            # 8-byte Reload
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB0_48:                               # %.lr.ph146.split
                                        #   Parent Loop BB0_45 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_50 Depth 3
	cmpl	%r15d, %r8d
	jge	.LBB0_51
# BB#49:                                # %.lr.ph142
                                        #   in Loop: Header=BB0_48 Depth=2
	movss	(%r13,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm1
	leaq	(%rax,%r11), %rdi
	movss	(%r13,%rdi,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	cvtps2pd	%xmm1, %xmm1
	movapd	%xmm1, %xmm2
	shufpd	$1, %xmm2, %xmm2        # xmm2 = xmm2[1,0]
	movq	%r9, %rdi
	movq	%r8, %rbp
	.p2align	4, 0x90
.LBB0_50:                               #   Parent Loop BB0_45 Depth=1
                                        #     Parent Loop BB0_48 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rbx,%rdi), %xmm3      # xmm3 = mem[0],zero
	movsd	8(%rbx,%rdi), %xmm4     # xmm4 = mem[0],zero
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	mulpd	%xmm1, %xmm4
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	mulpd	%xmm2, %xmm3
	movapd	%xmm4, %xmm5
	addpd	%xmm3, %xmm5
	subpd	%xmm3, %xmm4
	movsd	%xmm5, %xmm4            # xmm4 = xmm5[0],xmm4[1]
	cvtpd2ps	%xmm4, %xmm3
	movupd	(%rdi), %xmm4
	cvtps2pd	%xmm3, %xmm3
	subpd	%xmm3, %xmm4
	movupd	%xmm4, (%rbx,%rdi)
	movupd	(%rdi), %xmm4
	addpd	%xmm3, %xmm4
	movupd	%xmm4, (%rdi)
	addq	%rcx, %rbp
	addq	%rdx, %rdi
	cmpq	%rsi, %rbp
	jl	.LBB0_50
.LBB0_51:                               # %._crit_edge
                                        #   in Loop: Header=BB0_48 Depth=2
	addq	%r12, %rax
	incq	%r8
	addq	$16, %r9
	cmpq	%r14, %r8
	jne	.LBB0_48
	jmp	.LBB0_57
	.p2align	4, 0x90
.LBB0_52:                               # %.lr.ph146.split.us.preheader
                                        #   in Loop: Header=BB0_45 Depth=1
	shlq	$4, %rdx
	shlq	$4, %rbx
	movq	8(%rsp), %r8            # 8-byte Reload
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB0_53:                               # %.lr.ph146.split.us
                                        #   Parent Loop BB0_45 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_55 Depth 3
	cmpl	%r15d, %r9d
	jge	.LBB0_56
# BB#54:                                # %.lr.ph142.us
                                        #   in Loop: Header=BB0_53 Depth=2
	leaq	(%rdi,%r11), %rax
	movss	(%r13,%rdi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movss	(%r13,%rax,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	cvtps2pd	%xmm1, %xmm1
	movapd	%xmm1, %xmm2
	shufpd	$1, %xmm2, %xmm2        # xmm2 = xmm2[1,0]
	movq	%r8, %rax
	movq	%r9, %rbp
	.p2align	4, 0x90
.LBB0_55:                               #   Parent Loop BB0_45 Depth=1
                                        #     Parent Loop BB0_53 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rbx,%rax), %xmm3      # xmm3 = mem[0],zero
	movsd	8(%rbx,%rax), %xmm4     # xmm4 = mem[0],zero
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	mulpd	%xmm1, %xmm4
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	mulpd	%xmm2, %xmm3
	movapd	%xmm4, %xmm5
	addpd	%xmm3, %xmm5
	subpd	%xmm3, %xmm4
	movsd	%xmm5, %xmm4            # xmm4 = xmm5[0],xmm4[1]
	cvtpd2ps	%xmm4, %xmm3
	movupd	(%rax), %xmm4
	cvtps2pd	%xmm3, %xmm3
	subpd	%xmm3, %xmm4
	movupd	%xmm4, (%rbx,%rax)
	movupd	(%rax), %xmm4
	addpd	%xmm3, %xmm4
	movupd	%xmm4, (%rax)
	addq	%rcx, %rbp
	addq	%rdx, %rax
	cmpq	%rsi, %rbp
	jl	.LBB0_55
.LBB0_56:                               # %._crit_edge.us
                                        #   in Loop: Header=BB0_53 Depth=2
	addq	%r12, %rdi
	incq	%r9
	addq	$16, %r8
	cmpq	%r14, %r9
	jne	.LBB0_53
.LBB0_57:                               # %.loopexit138
                                        #   in Loop: Header=BB0_45 Depth=1
	cmpl	%r15d, %r10d
	jl	.LBB0_45
.LBB0_58:                               # %._crit_edge149
	xorl	%eax, %eax
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_70
# BB#59:                                # %._crit_edge149
	testl	%r15d, %r15d
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	jle	.LBB0_70
# BB#60:                                # %.lr.ph
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r15d, %xmm0
	cmpl	$2, %r15d
	jb	.LBB0_66
# BB#62:                                # %min.iters.checked188
	andl	$1, %r15d
	subq	%r15, %rsi
	je	.LBB0_66
# BB#63:                                # %vector.ph192
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movq	%rsi, %rax
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB0_64:                               # %vector.body184
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rcx), %xmm2
	movupd	16(%rcx), %xmm3
	movapd	%xmm2, %xmm4
	unpcklpd	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0]
	unpckhpd	%xmm3, %xmm2    # xmm2 = xmm2[1],xmm3[1]
	divpd	%xmm1, %xmm4
	divpd	%xmm1, %xmm2
	movapd	%xmm4, %xmm3
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	movhlps	%xmm4, %xmm2            # xmm2 = xmm4[1],xmm2[1]
	movups	%xmm2, 16(%rcx)
	movupd	%xmm3, (%rcx)
	addq	$32, %rcx
	addq	$-2, %rax
	jne	.LBB0_64
# BB#65:                                # %middle.block185
	xorl	%eax, %eax
	testl	%r15d, %r15d
	jne	.LBB0_67
	jmp	.LBB0_70
.LBB0_66:
	xorl	%esi, %esi
.LBB0_67:                               # %scalar.ph186
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movq	24(%rsp), %rax          # 8-byte Reload
	subq	%rsi, %rax
	shlq	$4, %rsi
	addq	%rsi, %rdx
	.p2align	4, 0x90
.LBB0_68:                               # =>This Inner Loop Header: Depth=1
	movupd	(%rdx), %xmm1
	divpd	%xmm0, %xmm1
	movupd	%xmm1, (%rdx)
	addq	$16, %rdx
	decq	%rax
	jne	.LBB0_68
.LBB0_69:
	xorl	%eax, %eax
.LBB0_70:                               # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_71:                               # %vector.body.preheader
	leaq	16(%r13), %rdi
	movapd	.LCPI0_3(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movq	%rdx, %rbp
	.p2align	4, 0x90
.LBB0_72:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rdi), %xmm1
	movupd	(%rdi), %xmm2
	xorpd	%xmm0, %xmm1
	xorpd	%xmm0, %xmm2
	movupd	%xmm1, -16(%rdi,%rcx,4)
	movupd	%xmm2, (%rdi,%rcx,4)
	addq	$32, %rdi
	addq	$-8, %rbp
	jne	.LBB0_72
# BB#73:                                # %middle.block
	testl	%esi, %esi
	jne	.LBB0_20
	jmp	.LBB0_26
.LBB0_74:
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movl	$1, %eax
	jmp	.LBB0_70
.Lfunc_end0:
	.size	fft, .Lfunc_end0-fft
	.cfi_endproc

	.type	fft.last_n,@object      # @fft.last_n
	.local	fft.last_n
	.comm	fft.last_n,4,4
	.type	fft.bitrev,@object      # @fft.bitrev
	.local	fft.bitrev
	.comm	fft.bitrev,8,8
	.type	fft.sintbl,@object      # @fft.sintbl
	.local	fft.sintbl
	.comm	fft.sintbl,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
