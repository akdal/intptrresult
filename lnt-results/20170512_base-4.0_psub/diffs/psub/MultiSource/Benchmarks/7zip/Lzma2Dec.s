	.text
	.file	"Lzma2Dec.bc"
	.globl	Lzma2Dec_AllocateProbs
	.p2align	4, 0x90
	.type	Lzma2Dec_AllocateProbs,@function
Lzma2Dec_AllocateProbs:                 # @Lzma2Dec_AllocateProbs
	.cfi_startproc
# BB#0:
	movq	%rdx, %r8
	movl	$4, %eax
	cmpb	$40, %sil
	ja	.LBB0_4
# BB#1:
	movl	$-1, %eax
	je	.LBB0_3
# BB#2:
	movl	%esi, %eax
	andb	$1, %al
	orb	$2, %al
	movzbl	%al, %eax
	shrb	%sil
	addb	$11, %sil
	movl	%esi, %ecx
	shll	%cl, %eax
.LBB0_3:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movb	$4, 3(%rsp)
	movb	%al, 4(%rsp)
	movb	%ah, 5(%rsp)  # NOREX
	movl	%eax, %ecx
	shrl	$16, %ecx
	movb	%cl, 6(%rsp)
	shrl	$24, %eax
	movb	%al, 7(%rsp)
	leaq	3(%rsp), %rsi
	movl	$5, %edx
	movq	%r8, %rcx
	callq	LzmaDec_AllocateProbs
	addq	$8, %rsp
.LBB0_4:                                # %Lzma2Dec_GetOldProps.exit.thread9
	retq
.Lfunc_end0:
	.size	Lzma2Dec_AllocateProbs, .Lfunc_end0-Lzma2Dec_AllocateProbs
	.cfi_endproc

	.globl	Lzma2Dec_Allocate
	.p2align	4, 0x90
	.type	Lzma2Dec_Allocate,@function
Lzma2Dec_Allocate:                      # @Lzma2Dec_Allocate
	.cfi_startproc
# BB#0:
	movq	%rdx, %r8
	movl	$4, %eax
	cmpb	$40, %sil
	ja	.LBB1_4
# BB#1:
	movl	$-1, %eax
	je	.LBB1_3
# BB#2:
	movl	%esi, %eax
	andb	$1, %al
	orb	$2, %al
	movzbl	%al, %eax
	shrb	%sil
	addb	$11, %sil
	movl	%esi, %ecx
	shll	%cl, %eax
.LBB1_3:
	pushq	%rax
.Lcfi1:
	.cfi_def_cfa_offset 16
	movb	$4, 3(%rsp)
	movb	%al, 4(%rsp)
	movb	%ah, 5(%rsp)  # NOREX
	movl	%eax, %ecx
	shrl	$16, %ecx
	movb	%cl, 6(%rsp)
	shrl	$24, %eax
	movb	%al, 7(%rsp)
	leaq	3(%rsp), %rsi
	movl	$5, %edx
	movq	%r8, %rcx
	callq	LzmaDec_Allocate
	addq	$8, %rsp
.LBB1_4:                                # %Lzma2Dec_GetOldProps.exit.thread9
	retq
.Lfunc_end1:
	.size	Lzma2Dec_Allocate, .Lfunc_end1-Lzma2Dec_Allocate
	.cfi_endproc

	.globl	Lzma2Dec_Init
	.p2align	4, 0x90
	.type	Lzma2Dec_Init,@function
Lzma2Dec_Init:                          # @Lzma2Dec_Init
	.cfi_startproc
# BB#0:
	movl	$0, 144(%rdi)
	movl	$1, 152(%rdi)
	movl	$1, 156(%rdi)
	movl	$1, 160(%rdi)
	jmp	LzmaDec_Init            # TAILCALL
.Lfunc_end2:
	.size	Lzma2Dec_Init, .Lfunc_end2-Lzma2Dec_Init
	.cfi_endproc

	.globl	Lzma2Dec_DecodeToDic
	.p2align	4, 0x90
	.type	Lzma2Dec_DecodeToDic,@function
Lzma2Dec_DecodeToDic:                   # @Lzma2Dec_DecodeToDic
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 112
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%rdx, %rbx
	movq	%rdi, %r12
	movq	(%rcx), %rbp
	movq	$0, (%rcx)
	movl	$0, (%r14)
	movl	144(%r12), %eax
	cmpl	$8, %eax
	jne	.LBB3_2
.LBB3_1:                                # %._crit_edge
	movl	$1, (%r14)
.LBB3_63:
	xorl	%eax, %eax
.LBB3_64:                               # %.thread148
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_2:                                # %.lr.ph
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	cmpl	$9, %eax
	je	.LBB3_59
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	48(%r12), %r13
	testl	%r8d, %r8d
	jne	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpq	%rsi, %r13
	je	.LBB3_60
.LBB3_6:                                #   in Loop: Header=BB3_3 Depth=1
	movl	%eax, %edx
	andl	$-2, %edx
	cmpl	$6, %edx
	jne	.LBB3_12
# BB#7:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	subq	%r13, %r14
	movq	(%rcx), %rdx
	movq	%rbp, %r15
	subq	%rdx, %r15
	movq	%r15, (%rsp)
	movl	140(%r12), %ecx
	movq	%rbp, %rsi
	xorl	%ebx, %ebx
	cmpq	%r14, %rcx
	setbe	%bpl
	cmovbeq	%rcx, %r14
	movzbl	148(%r12), %ecx
	testb	%cl, %cl
	js	.LBB3_18
# BB#8:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpq	%rdx, %rsi
	movq	%rsi, %rbp
	je	.LBB3_62
# BB#9:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpl	$6, %eax
	movq	32(%rsp), %rbx          # 8-byte Reload
	jne	.LBB3_37
# BB#10:                                #   in Loop: Header=BB3_3 Depth=1
	xorl	%esi, %esi
	cmpb	$1, %cl
	sete	%al
	jne	.LBB3_35
# BB#11:                                #   in Loop: Header=BB3_3 Depth=1
	movabsq	$4294967297, %rcx       # imm = 0x100000001
	movq	%rcx, 156(%r12)
	jmp	.LBB3_36
	.p2align	4, 0x90
.LBB3_12:                               #   in Loop: Header=BB3_3 Depth=1
	movq	(%rcx), %rdx
	cmpq	%rbp, %rdx
	je	.LBB3_61
# BB#13:                                #   in Loop: Header=BB3_3 Depth=1
	incq	%rdx
	movq	%rdx, (%rcx)
	movl	$9, %edx
	cmpl	$5, %eax
	ja	.LBB3_57
# BB#14:                                #   in Loop: Header=BB3_3 Depth=1
	movq	%rbp, %rdi
	movzbl	(%rbx), %r9d
	movzbl	%r9b, %ebp
	movl	%eax, %eax
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_15:                               #   in Loop: Header=BB3_3 Depth=1
	movb	%r9b, 148(%r12)
	testb	%r9b, %r9b
	je	.LBB3_54
# BB#16:                                #   in Loop: Header=BB3_3 Depth=1
	js	.LBB3_55
# BB#17:                                #   in Loop: Header=BB3_3 Depth=1
	andl	$127, %ebp
	xorl	%eax, %eax
	cmpl	$2, %ebp
	movq	%rdi, %rbp
	jbe	.LBB3_56
	jmp	.LBB3_57
.LBB3_18:                               #   in Loop: Header=BB3_3 Depth=1
	cmpl	$6, %eax
	jne	.LBB3_24
# BB#19:                                #   in Loop: Header=BB3_3 Depth=1
	shrl	$5, %ecx
	xorl	%edx, %edx
	andl	$3, %ecx
	setne	%dil
	xorl	%esi, %esi
	cmpl	$3, %ecx
	sete	%al
	je	.LBB3_21
# BB#20:                                #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, 152(%r12)
	jne	.LBB3_59
.LBB3_21:                               #   in Loop: Header=BB3_3 Depth=1
	testl	%ecx, %ecx
	jne	.LBB3_23
# BB#22:                                #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, 156(%r12)
	jne	.LBB3_59
.LBB3_23:                               #   in Loop: Header=BB3_3 Depth=1
	movb	%al, %sil
	movb	%dil, %dl
	movq	%r12, %rdi
	callq	LzmaDec_InitDicAndState
	movl	$0, 152(%r12)
	movl	$0, 156(%r12)
	movl	$7, 144(%r12)
	movq	(%rsp), %r15
.LBB3_24:                               #   in Loop: Header=BB3_3 Depth=1
	movb	%bpl, %bl
	movl	136(%r12), %eax
	cmpq	%rax, %r15
	jbe	.LBB3_26
# BB#25:                                #   in Loop: Header=BB3_3 Depth=1
	movq	%rax, (%rsp)
.LBB3_26:                               #   in Loop: Header=BB3_3 Depth=1
	addq	%r13, %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%rsp, %rcx
	movl	%ebx, %r8d
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%r14, %r9
	callq	LzmaDec_DecodeToDic
	movq	(%rsp), %r9
	movq	48(%rsp), %rcx          # 8-byte Reload
	addq	%r9, (%rcx)
	movl	136(%r12), %edx
	subl	%r9d, %edx
	movl	%edx, 136(%r12)
	movq	48(%r12), %rbp
	subq	%r13, %rbp
	movl	140(%r12), %edi
	subl	%ebp, %edi
	movl	%edi, 140(%r12)
	testl	%eax, %eax
	movl	12(%rsp), %r8d          # 4-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	jne	.LBB3_64
# BB#27:                                #   in Loop: Header=BB3_3 Depth=1
	movl	(%r14), %ebx
	cmpl	$3, %ebx
	je	.LBB3_63
# BB#28:                                #   in Loop: Header=BB3_3 Depth=1
	orq	%r9, %rbp
	jne	.LBB3_32
# BB#29:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$1, %eax
	orl	%edx, %edi
	jne	.LBB3_64
# BB#30:                                #   in Loop: Header=BB3_3 Depth=1
	cmpl	$4, %ebx
	jne	.LBB3_64
# BB#31:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$0, 144(%r12)
	movl	(%r14), %ebx
.LBB3_32:                               #   in Loop: Header=BB3_3 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	addq	%r9, %rax
	cmpl	$4, %ebx
	jne	.LBB3_34
# BB#33:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$2, (%r14)
.LBB3_34:                               #   in Loop: Header=BB3_3 Depth=1
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rax, %rbx
	movl	144(%r12), %edx
	jmp	.LBB3_58
.LBB3_35:                               #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, 152(%r12)
	jne	.LBB3_59
.LBB3_36:                               # %.thread
                                        #   in Loop: Header=BB3_3 Depth=1
	movb	%al, %sil
	movl	$0, 152(%r12)
	xorl	%edx, %edx
	movq	%r12, %rdi
	callq	LzmaDec_InitDicAndState
	movq	(%rsp), %r15
.LBB3_37:                               #   in Loop: Header=BB3_3 Depth=1
	cmpq	%r14, %r15
	jbe	.LBB3_39
# BB#38:                                #   in Loop: Header=BB3_3 Depth=1
	movq	%r14, (%rsp)
	movq	%r14, %r15
.LBB3_39:                               #   in Loop: Header=BB3_3 Depth=1
	testq	%r15, %r15
	je	.LBB3_59
# BB#40:                                #   in Loop: Header=BB3_3 Depth=1
	movq	24(%r12), %rdi
	addq	48(%r12), %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	memcpy
	addq	%r15, 48(%r12)
	cmpl	$0, 68(%r12)
	je	.LBB3_42
# BB#41:                                # %._crit_edge.i
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	64(%r12), %eax
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB3_44
.LBB3_42:                               #   in Loop: Header=BB3_3 Depth=1
	movl	12(%r12), %ecx
	movl	64(%r12), %eax
	movl	%ecx, %edx
	subl	%eax, %edx
	cmpq	%r15, %rdx
	movq	16(%rsp), %r14          # 8-byte Reload
	ja	.LBB3_44
# BB#43:                                #   in Loop: Header=BB3_3 Depth=1
	movl	%ecx, 68(%r12)
	.p2align	4, 0x90
.LBB3_44:                               # %LzmaDec_UpdateWithUncompressed.exit
                                        #   in Loop: Header=BB3_3 Depth=1
	addl	%eax, %r15d
	movl	%r15d, 64(%r12)
	movq	(%rsp), %rax
	addq	%rax, %rbx
	movq	48(%rsp), %rcx          # 8-byte Reload
	addq	%rax, (%rcx)
	movl	140(%r12), %edx
	subl	%eax, %edx
	movl	%edx, 140(%r12)
	movl	$7, %eax
	cmovnel	%eax, %edx
	movl	%edx, 144(%r12)
	movl	12(%rsp), %r8d          # 4-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	144(%r12), %edx
	jmp	.LBB3_58
.LBB3_45:                               #   in Loop: Header=BB3_3 Depth=1
	shll	$8, %ebp
	orl	%ebp, 140(%r12)
	movl	$2, %edx
	movq	%rdi, %rbp
	jmp	.LBB3_57
.LBB3_46:                               #   in Loop: Header=BB3_3 Depth=1
	orl	140(%r12), %ebp
	incl	%ebp
	movl	%ebp, 140(%r12)
	xorl	%eax, %eax
	cmpb	$0, 148(%r12)
	setns	%al
	leal	3(%rax,%rax,2), %edx
	movq	%rdi, %rbp
	jmp	.LBB3_57
.LBB3_47:                               #   in Loop: Header=BB3_3 Depth=1
	shll	$8, %ebp
	movl	%ebp, 136(%r12)
	movl	$4, %edx
	movq	%rdi, %rbp
	jmp	.LBB3_57
.LBB3_48:                               #   in Loop: Header=BB3_3 Depth=1
	orl	136(%r12), %ebp
	incl	%ebp
	movl	%ebp, 136(%r12)
	movl	$5, %edx
	testb	$64, 148(%r12)
	movq	%rdi, %rbp
	jne	.LBB3_57
# BB#49:                                #   in Loop: Header=BB3_3 Depth=1
	xorl	%eax, %eax
	cmpl	$0, 160(%r12)
	setne	%al
	leal	6(%rax,%rax,2), %edx
	jmp	.LBB3_57
.LBB3_50:                               #   in Loop: Header=BB3_3 Depth=1
	cmpb	$-32, %r9b
	jbe	.LBB3_52
# BB#51:                                #   in Loop: Header=BB3_3 Depth=1
	movq	%rdi, %rbp
	jmp	.LBB3_57
.LBB3_52:                               #   in Loop: Header=BB3_3 Depth=1
	imull	$57, %ebp, %edi
	andl	$15872, %edi            # imm = 0x3E00
	shrl	$9, %edi
	movl	%edi, %eax
	movq	%rbx, %r10
	movb	$9, %bl
	mulb	%bl
	movq	%r10, %r11
	movl	%r9d, %ebx
	subb	%al, %bl
	movzbl	%bl, %r10d
	movq	24(%rsp), %rbx          # 8-byte Reload
	imull	$109, %ebp, %eax
	movq	%rbx, %rbp
	shrl	$8, %eax
	subb	%al, %r9b
	shrb	%r9b
	addb	%al, %r9b
	shrb	$5, %r9b
	movzbl	%r9b, %eax
	movl	%eax, 8(%r12)
	imull	$205, %edi, %eax
	andl	$7168, %eax             # imm = 0x1C00
	shrl	$10, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	movb	$5, %bl
	mulb	%bl
	movq	%r11, %rbx
	subb	%al, %dil
	movzbl	%dil, %eax
	leal	(%rax,%r10), %edi
	cmpl	$4, %edi
	ja	.LBB3_57
# BB#53:                                #   in Loop: Header=BB3_3 Depth=1
	movl	%r10d, (%r12)
	movl	%eax, 4(%r12)
	movl	$0, 160(%r12)
	movl	$6, %edx
	jmp	.LBB3_57
.LBB3_54:                               #   in Loop: Header=BB3_3 Depth=1
	movl	$8, %edx
	movq	%rdi, %rbp
	jmp	.LBB3_57
.LBB3_55:                               #   in Loop: Header=BB3_3 Depth=1
	andl	$31, %ebp
	shll	$16, %ebp
	movl	%ebp, %eax
	movq	%rdi, %rbp
.LBB3_56:                               #   in Loop: Header=BB3_3 Depth=1
	movl	%eax, 140(%r12)
	movl	$1, %edx
	.p2align	4, 0x90
.LBB3_57:                               # %.thread149
                                        #   in Loop: Header=BB3_3 Depth=1
	incq	%rbx
	movl	%edx, 144(%r12)
.LBB3_58:                               # %.backedge
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	$8, %edx
	movl	%edx, %eax
	jne	.LBB3_3
	jmp	.LBB3_1
.LBB3_59:
	movl	$1, %eax
	jmp	.LBB3_64
.LBB3_60:
	movl	$2, (%r14)
	jmp	.LBB3_63
.LBB3_61:
	movl	$3, (%r14)
	jmp	.LBB3_63
.LBB3_62:
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	$3, (%rax)
	jmp	.LBB3_63
.Lfunc_end3:
	.size	Lzma2Dec_DecodeToDic, .Lfunc_end3-Lzma2Dec_DecodeToDic
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_15
	.quad	.LBB3_45
	.quad	.LBB3_46
	.quad	.LBB3_47
	.quad	.LBB3_48
	.quad	.LBB3_50

	.text
	.globl	Lzma2Dec_DecodeToBuf
	.p2align	4, 0x90
	.type	Lzma2Dec_DecodeToBuf,@function
Lzma2Dec_DecodeToBuf:                   # @Lzma2Dec_DecodeToBuf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 128
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movl	%r9d, 4(%rsp)           # 4-byte Spill
	movq	%rcx, %rbp
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r13
	movq	(%rdx), %rax
	movq	(%r8), %rcx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	$0, (%rdx)
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	$0, (%r8)
	movq	%r13, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 16(%rsp)
	movq	48(%r13), %r15
	movq	56(%r13), %rdx
	cmpq	%rdx, %r15
	jne	.LBB4_3
# BB#2:                                 #   in Loop: Header=BB4_1 Depth=1
	movq	$0, 48(%r13)
	xorl	%r15d, %r15d
.LBB4_3:                                #   in Loop: Header=BB4_1 Depth=1
	movq	%rdx, %rcx
	subq	%r15, %rcx
	leaq	(%r15,%rax), %rsi
	movq	%rax, 56(%rsp)          # 8-byte Spill
	cmpq	%rcx, %rax
	cmovaq	%rdx, %rsi
	movl	4(%rsp), %r8d           # 4-byte Reload
	movl	$0, %eax
	cmoval	%eax, %r8d
	movq	%r13, %rdi
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	%rbp, %rdx
	leaq	16(%rsp), %rcx
	movq	128(%rsp), %r9
	callq	Lzma2Dec_DecodeToDic
	movq	16(%rsp), %rbx
	movq	40(%rsp), %rcx          # 8-byte Reload
	addq	%rbx, (%rcx)
	movl	%eax, %r14d
	movq	48(%r13), %r12
	subq	%r15, %r12
	addq	24(%r13), %r15
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	memcpy
	movq	32(%rsp), %rax          # 8-byte Reload
	addq	%r12, (%rax)
	testl	%r14d, %r14d
	jne	.LBB4_6
# BB#4:                                 #   in Loop: Header=BB4_1 Depth=1
	xorl	%r14d, %r14d
	testq	%r12, %r12
	je	.LBB4_6
# BB#5:                                 #   in Loop: Header=BB4_1 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	subq	%r12, %rax
	addq	%r12, %rbp
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	48(%rsp), %rcx          # 8-byte Reload
	subq	%rbx, %rcx
	movq	64(%rsp), %rbp          # 8-byte Reload
	addq	%rbx, %rbp
	testq	%rax, %rax
	movq	24(%rsp), %r13          # 8-byte Reload
	jne	.LBB4_1
.LBB4_6:                                # %.loopexit
	movl	%r14d, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	Lzma2Dec_DecodeToBuf, .Lfunc_end4-Lzma2Dec_DecodeToBuf
	.cfi_endproc

	.globl	Lzma2Decode
	.p2align	4, 0x90
	.type	Lzma2Decode,@function
Lzma2Decode:                            # @Lzma2Decode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 240
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movq	%rsi, %rbx
	movq	240(%rsp), %rax
	movq	(%rbx), %r15
	movq	(%rbp), %rcx
	movq	$0, 32(%rsp)
	movq	$0, (%rbp)
	movq	$0, (%rbx)
	movl	$0, (%rax)
	movq	%rdi, 40(%rsp)
	movq	%r15, 72(%rsp)
	movl	$4, %r14d
	cmpb	$40, %r8b
	ja	.LBB5_7
# BB#1:
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %r12
	movl	%r9d, %r13d
	movq	248(%rsp), %r9
	movl	$-1, %eax
	je	.LBB5_3
# BB#2:
	movl	%r8d, %eax
	andb	$1, %al
	orb	$2, %al
	movzbl	%al, %eax
	shrb	%r8b
	addb	$11, %r8b
	movl	%r8d, %ecx
	shll	%cl, %eax
.LBB5_3:
	movb	$4, 3(%rsp)
	movb	%al, 4(%rsp)
	movb	%ah, 5(%rsp)  # NOREX
	movl	%eax, %ecx
	shrl	$16, %ecx
	movb	%cl, 6(%rsp)
	shrl	$24, %eax
	movb	%al, 7(%rsp)
	leaq	16(%rsp), %rdi
	leaq	3(%rsp), %rsi
	movl	$5, %edx
	movq	%r9, %rcx
	callq	LzmaDec_AllocateProbs
	movl	%eax, %r14d
	testl	%r14d, %r14d
	movq	8(%rsp), %rax           # 8-byte Reload
	jne	.LBB5_7
# BB#4:
	movq	%rax, (%rbp)
	leaq	16(%rsp), %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%rbp, %rcx
	movl	%r13d, %r8d
	movq	240(%rsp), %rbp
	movq	%rbp, %r9
	callq	Lzma2Dec_DecodeToDic
	movl	%eax, %r14d
	movq	64(%rsp), %rax
	movq	%rax, (%rbx)
	testl	%r14d, %r14d
	jne	.LBB5_6
# BB#5:
	xorl	%eax, %eax
	cmpl	$3, (%rbp)
	movl	$6, %r14d
	cmovnel	%eax, %r14d
.LBB5_6:
	leaq	16(%rsp), %rdi
	movq	248(%rsp), %rsi
	callq	LzmaDec_FreeProbs
.LBB5_7:                                # %Lzma2Dec_GetOldProps.exit.thread45
	movl	%r14d, %eax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	Lzma2Decode, .Lfunc_end5-Lzma2Decode
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
