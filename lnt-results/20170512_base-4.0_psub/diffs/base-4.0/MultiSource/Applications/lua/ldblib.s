	.text
	.file	"ldblib.bc"
	.globl	luaopen_debug
	.p2align	4, 0x90
	.type	luaopen_debug,@function
luaopen_debug:                          # @luaopen_debug
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$.L.str, %esi
	movl	$dblib, %edx
	callq	luaL_register
	movl	$1, %eax
	popq	%rcx
	retq
.Lfunc_end0:
	.size	luaopen_debug, .Lfunc_end0-luaopen_debug
	.cfi_endproc

	.p2align	4, 0x90
	.type	db_debug,@function
db_debug:                               # @db_debug
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 24
	subq	$264, %rsp              # imm = 0x108
.Lcfi3:
	.cfi_def_cfa_offset 288
.Lcfi4:
	.cfi_offset %rbx, -24
.Lcfi5:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	stderr(%rip), %rcx
	movl	$.L.str.14, %edi
	movl	$11, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdin(%rip), %rdx
	movq	%rsp, %rdi
	movl	$250, %esi
	callq	fgets
	testq	%rax, %rax
	je	.LBB1_7
# BB#1:                                 # %.lr.ph.preheader
	movq	%rsp, %r14
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.15, %esi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_7
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	%r14, %rdi
	callq	strlen
	movl	$.L.str.16, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	luaL_loadbuffer
	testl	%eax, %eax
	jne	.LBB1_5
# BB#4:                                 #   in Loop: Header=BB1_2 Depth=1
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	lua_pcall
	testl	%eax, %eax
	je	.LBB1_6
.LBB1_5:                                #   in Loop: Header=BB1_2 Depth=1
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_tolstring
	movq	stderr(%rip), %rsi
	movq	%rax, %rdi
	callq	fputs
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
.LBB1_6:                                # %.critedge
                                        #   in Loop: Header=BB1_2 Depth=1
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movq	stderr(%rip), %rcx
	movl	$.L.str.14, %edi
	movl	$11, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdin(%rip), %rdx
	movl	$250, %esi
	movq	%r14, %rdi
	callq	fgets
	testq	%rax, %rax
	jne	.LBB1_2
.LBB1_7:                                # %._crit_edge
	xorl	%eax, %eax
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	db_debug, .Lfunc_end1-db_debug
	.cfi_endproc

	.p2align	4, 0x90
	.type	db_getfenv,@function
db_getfenv:                             # @db_getfenv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 16
	movl	$1, %esi
	callq	lua_getfenv
	movl	$1, %eax
	popq	%rcx
	retq
.Lfunc_end2:
	.size	db_getfenv, .Lfunc_end2-db_getfenv
	.cfi_endproc

	.p2align	4, 0x90
	.type	db_gethook,@function
db_gethook:                             # @db_gethook
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi10:
	.cfi_def_cfa_offset 48
.Lcfi11:
	.cfi_offset %rbx, -32
.Lcfi12:
	.cfi_offset %r14, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	lua_type
	cmpl	$8, %eax
	movq	%rbx, %r14
	jne	.LBB3_2
# BB#1:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_tothread
	movq	%rax, %r14
.LBB3_2:                                # %getthread.exit
	movq	%r14, %rdi
	callq	lua_gethookmask
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	lua_gethook
	testq	%rax, %rax
	je	.LBB3_5
# BB#3:                                 # %getthread.exit
	movl	$hookf, %ecx
	cmpq	%rcx, %rax
	je	.LBB3_5
# BB#4:
	movl	$.L.str.18, %esi
	movl	$13, %edx
	movq	%rbx, %rdi
	callq	lua_pushlstring
	jmp	.LBB3_6
.LBB3_5:
	movq	%rbx, %rdi
	callq	gethooktable
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	lua_pushlightuserdata
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_rawget
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_remove
.LBB3_6:
	xorl	%eax, %eax
	testb	$1, %bpl
	je	.LBB3_8
# BB#7:
	movb	$99, 11(%rsp)
	movl	$1, %eax
.LBB3_8:
	testb	$2, %bpl
	je	.LBB3_10
# BB#9:
	movb	$114, 11(%rsp,%rax)
	leal	1(%rax), %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
.LBB3_10:
	testb	$4, %bpl
	je	.LBB3_12
# BB#11:
	movslq	%eax, %rcx
	incl	%eax
	movb	$108, 11(%rsp,%rcx)
.LBB3_12:                               # %unmakemask.exit
	cltq
	movb	$0, 11(%rsp,%rax)
	leaq	11(%rsp), %rsi
	movq	%rbx, %rdi
	callq	lua_pushstring
	movq	%r14, %rdi
	callq	lua_gethookcount
	movslq	%eax, %rsi
	movq	%rbx, %rdi
	callq	lua_pushinteger
	movl	$3, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	db_gethook, .Lfunc_end3-db_gethook
	.cfi_endproc

	.p2align	4, 0x90
	.type	db_getinfo,@function
db_getinfo:                             # @db_getinfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 48
	subq	$128, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 176
.Lcfi20:
	.cfi_offset %rbx, -48
.Lcfi21:
	.cfi_offset %r12, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	lua_type
	xorl	%ebp, %ebp
	cmpl	$8, %eax
	movq	%rbx, %r14
	jne	.LBB4_2
# BB#1:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_tothread
	movq	%rax, %r14
	movl	$1, %ebp
.LBB4_2:                                # %getthread.exit
	leal	2(%rbp), %r12d
	movl	$.L.str.24, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movl	%r12d, %esi
	callq	luaL_optlstring
	movq	%rax, %r15
	incl	%ebp
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	lua_isnumber
	movq	%rbx, %rdi
	testl	%eax, %eax
	je	.LBB4_5
# BB#3:
	movl	%ebp, %esi
	callq	lua_tointeger
	leaq	8(%rsp), %rdx
	movq	%r14, %rdi
	movl	%eax, %esi
	callq	lua_getstack
	testl	%eax, %eax
	jne	.LBB4_7
# BB#4:
	movq	%rbx, %rdi
	callq	lua_pushnil
	movl	$1, %ebp
	jmp	.LBB4_29
.LBB4_5:
	movl	%ebp, %esi
	callq	lua_type
	cmpl	$6, %eax
	jne	.LBB4_9
# BB#6:
	movl	$.L.str.25, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	lua_pushfstring
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_tolstring
	movq	%rax, %r15
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	lua_pushvalue
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	lua_xmove
.LBB4_7:
	leaq	8(%rsp), %rdx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	lua_getinfo
	testl	%eax, %eax
	je	.LBB4_8
# BB#11:
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	lua_createtable
	movl	$83, %esi
	movq	%r15, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB4_13
# BB#12:
	movq	40(%rsp), %rsi
	movq	%rbx, %rdi
	callq	lua_pushstring
	movl	$-2, %esi
	movl	$.L.str.28, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	leaq	64(%rsp), %rsi
	movq	%rbx, %rdi
	callq	lua_pushstring
	movl	$-2, %esi
	movl	$.L.str.29, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movslq	56(%rsp), %rsi
	movq	%rbx, %rdi
	callq	lua_pushinteger
	movl	$-2, %esi
	movl	$.L.str.30, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movslq	60(%rsp), %rsi
	movq	%rbx, %rdi
	callq	lua_pushinteger
	movl	$-2, %esi
	movl	$.L.str.31, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movq	32(%rsp), %rsi
	movq	%rbx, %rdi
	callq	lua_pushstring
	movl	$-2, %esi
	movl	$.L.str.32, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
.LBB4_13:
	movl	$108, %esi
	movq	%r15, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB4_15
# BB#14:
	movslq	48(%rsp), %rsi
	movq	%rbx, %rdi
	callq	lua_pushinteger
	movl	$-2, %esi
	movl	$.L.str.33, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
.LBB4_15:
	movl	$117, %esi
	movq	%r15, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB4_17
# BB#16:
	movslq	52(%rsp), %rsi
	movq	%rbx, %rdi
	callq	lua_pushinteger
	movl	$-2, %esi
	movl	$.L.str.34, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
.LBB4_17:
	movl	$110, %esi
	movq	%r15, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB4_19
# BB#18:
	movq	16(%rsp), %rsi
	movq	%rbx, %rdi
	callq	lua_pushstring
	movl	$-2, %esi
	movl	$.L.str.35, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movq	24(%rsp), %rsi
	movq	%rbx, %rdi
	callq	lua_pushstring
	movl	$-2, %esi
	movl	$.L.str.36, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
.LBB4_19:
	movl	$76, %esi
	movq	%r15, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB4_24
# BB#20:
	cmpq	%rbx, %r14
	je	.LBB4_21
# BB#22:
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	lua_xmove
	jmp	.LBB4_23
.LBB4_8:
	movl	$.L.str.27, %edx
	movq	%rbx, %rdi
	movl	%r12d, %esi
	jmp	.LBB4_10
.LBB4_9:
	movl	$.L.str.26, %edx
	movq	%rbx, %rdi
	movl	%ebp, %esi
.LBB4_10:
	callq	luaL_argerror
	movl	%eax, %ebp
	jmp	.LBB4_29
.LBB4_21:
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-3, %esi
	movq	%rbx, %rdi
	callq	lua_remove
.LBB4_23:                               # %treatstackoption.exit
	movl	$-2, %esi
	movl	$.L.str.37, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
.LBB4_24:
	movl	$102, %esi
	movq	%r15, %rdi
	callq	strchr
	movl	$1, %ebp
	testq	%rax, %rax
	je	.LBB4_29
# BB#25:
	cmpq	%rbx, %r14
	je	.LBB4_26
# BB#27:
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	lua_xmove
	jmp	.LBB4_28
.LBB4_26:
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-3, %esi
	movq	%rbx, %rdi
	callq	lua_remove
.LBB4_28:                               # %treatstackoption.exit39
	movl	$-2, %esi
	movl	$.L.str.38, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
.LBB4_29:
	movl	%ebp, %eax
	addq	$128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	db_getinfo, .Lfunc_end4-db_getinfo
	.cfi_endproc

	.p2align	4, 0x90
	.type	db_getlocal,@function
db_getlocal:                            # @db_getlocal
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 40
	subq	$120, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 160
.Lcfi30:
	.cfi_offset %rbx, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	$1, %esi
	callq	lua_type
	xorl	%ebx, %ebx
	cmpl	$8, %eax
	movq	%r15, %r14
	jne	.LBB5_2
# BB#1:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	lua_tothread
	movq	%rax, %r14
	movl	$1, %ebx
.LBB5_2:                                # %getthread.exit
	leal	1(%rbx), %ebp
	movq	%r15, %rdi
	movl	%ebp, %esi
	callq	luaL_checkinteger
	movq	%rsp, %rdx
	movq	%r14, %rdi
	movl	%eax, %esi
	callq	lua_getstack
	testl	%eax, %eax
	je	.LBB5_3
# BB#4:
	orl	$2, %ebx
	movq	%r15, %rdi
	movl	%ebx, %esi
	callq	luaL_checkinteger
	movq	%rsp, %rsi
	movq	%r14, %rdi
	movl	%eax, %edx
	callq	lua_getlocal
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB5_6
# BB#5:
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	lua_xmove
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	lua_pushstring
	movl	$-2, %esi
	movq	%r15, %rdi
	callq	lua_pushvalue
	movl	$2, %eax
	jmp	.LBB5_7
.LBB5_3:
	movl	$.L.str.39, %edx
	movq	%r15, %rdi
	movl	%ebp, %esi
	callq	luaL_argerror
	jmp	.LBB5_7
.LBB5_6:
	movq	%r15, %rdi
	callq	lua_pushnil
	movl	$1, %eax
.LBB5_7:
	addq	$120, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	db_getlocal, .Lfunc_end5-db_getlocal
	.cfi_endproc

	.p2align	4, 0x90
	.type	db_getregistry,@function
db_getregistry:                         # @db_getregistry
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 16
	movl	$-10000, %esi           # imm = 0xD8F0
	callq	lua_pushvalue
	movl	$1, %eax
	popq	%rcx
	retq
.Lfunc_end6:
	.size	db_getregistry, .Lfunc_end6-db_getregistry
	.cfi_endproc

	.p2align	4, 0x90
	.type	db_getmetatable,@function
db_getmetatable:                        # @db_getmetatable
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 16
.Lcfi36:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checkany
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_getmetatable
	testl	%eax, %eax
	jne	.LBB7_2
# BB#1:
	movq	%rbx, %rdi
	callq	lua_pushnil
.LBB7_2:
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end7:
	.size	db_getmetatable, .Lfunc_end7-db_getmetatable
	.cfi_endproc

	.p2align	4, 0x90
	.type	db_getupvalue,@function
db_getupvalue:                          # @db_getupvalue
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 32
.Lcfi40:
	.cfi_offset %rbx, -32
.Lcfi41:
	.cfi_offset %r14, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$2, %esi
	callq	luaL_checkinteger
	movq	%rax, %r14
	movl	$1, %esi
	movl	$6, %edx
	movq	%rbx, %rdi
	callq	luaL_checktype
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_iscfunction
	xorl	%ebp, %ebp
	testl	%eax, %eax
	jne	.LBB8_3
# BB#1:
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r14d, %edx
	callq	lua_getupvalue
	testq	%rax, %rax
	je	.LBB8_3
# BB#2:
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	lua_pushstring
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_insert
	movl	$2, %ebp
.LBB8_3:                                # %auxupvalue.exit
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end8:
	.size	db_getupvalue, .Lfunc_end8-db_getupvalue
	.cfi_endproc

	.p2align	4, 0x90
	.type	db_setfenv,@function
db_setfenv:                             # @db_setfenv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 16
.Lcfi44:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$2, %esi
	movl	$5, %edx
	callq	luaL_checktype
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_setfenv
	testl	%eax, %eax
	jne	.LBB9_2
# BB#1:
	movl	$.L.str.40, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
.LBB9_2:
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end9:
	.size	db_setfenv, .Lfunc_end9-db_setfenv
	.cfi_endproc

	.p2align	4, 0x90
	.type	db_sethook,@function
db_sethook:                             # @db_sethook
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 64
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	lua_type
	xorl	%ebp, %ebp
	cmpl	$8, %eax
	movq	%rbx, %r14
	jne	.LBB10_2
# BB#1:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_tothread
	movq	%rax, %r14
	movl	$1, %ebp
.LBB10_2:                               # %getthread.exit
	leal	1(%rbp), %r12d
	movq	%rbx, %rdi
	movl	%r12d, %esi
	callq	lua_type
	testl	%eax, %eax
	jle	.LBB10_3
# BB#4:
	leal	2(%rbp), %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	luaL_checklstring
	movq	%rax, %r13
	movl	$6, %edx
	movq	%rbx, %rdi
	movl	%r12d, %esi
	callq	luaL_checktype
	addl	$3, %ebp
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	luaL_optinteger
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	$99, %esi
	movq	%r13, %rdi
	callq	strchr
	xorl	%ebp, %ebp
	testq	%rax, %rax
	setne	%bpl
	movl	$114, %esi
	movq	%r13, %rdi
	callq	strchr
	movq	%r14, %r15
	leal	2(%rbp), %r14d
	testq	%rax, %rax
	cmovel	%ebp, %r14d
	movl	$108, %esi
	movq	%r13, %rdi
	callq	strchr
	movl	%r14d, %ecx
	orl	$4, %ecx
	testq	%rax, %rax
	cmovel	%r14d, %ecx
	movq	%r15, %r14
	movq	(%rsp), %r15            # 8-byte Reload
	movl	%ecx, %ebp
	orl	$8, %ebp
	testl	%r15d, %r15d
	cmovlel	%ecx, %ebp
	movl	$hookf, %r13d
	jmp	.LBB10_5
.LBB10_3:
	movq	%rbx, %rdi
	movl	%r12d, %esi
	callq	lua_settop
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
.LBB10_5:
	movq	%rbx, %rdi
	callq	gethooktable
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	lua_pushlightuserdata
	movq	%rbx, %rdi
	movl	%r12d, %esi
	callq	lua_pushvalue
	movl	$-3, %esi
	movq	%rbx, %rdi
	callq	lua_rawset
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	%ebp, %edx
	movl	%r15d, %ecx
	callq	lua_sethook
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	db_sethook, .Lfunc_end10-db_sethook
	.cfi_endproc

	.p2align	4, 0x90
	.type	db_setlocal,@function
db_setlocal:                            # @db_setlocal
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 40
	subq	$120, %rsp
.Lcfi62:
	.cfi_def_cfa_offset 160
.Lcfi63:
	.cfi_offset %rbx, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	lua_type
	xorl	%r15d, %r15d
	cmpl	$8, %eax
	movq	%rbx, %r14
	jne	.LBB11_2
# BB#1:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_tothread
	movq	%rax, %r14
	movl	$1, %r15d
.LBB11_2:                               # %getthread.exit
	leal	1(%r15), %ebp
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	luaL_checkinteger
	movq	%rsp, %rdx
	movq	%r14, %rdi
	movl	%eax, %esi
	callq	lua_getstack
	testl	%eax, %eax
	je	.LBB11_3
# BB#4:
	leal	3(%r15), %ebp
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	luaL_checkany
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	lua_settop
	movl	$1, %ebp
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	lua_xmove
	orl	$2, %r15d
	movq	%rbx, %rdi
	movl	%r15d, %esi
	callq	luaL_checkinteger
	movq	%rsp, %rsi
	movq	%r14, %rdi
	movl	%eax, %edx
	callq	lua_setlocal
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	lua_pushstring
	jmp	.LBB11_5
.LBB11_3:
	movl	$.L.str.39, %edx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	luaL_argerror
	movl	%eax, %ebp
.LBB11_5:
	movl	%ebp, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	db_setlocal, .Lfunc_end11-db_setlocal
	.cfi_endproc

	.p2align	4, 0x90
	.type	db_setmetatable,@function
db_setmetatable:                        # @db_setmetatable
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 16
.Lcfi68:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$2, %esi
	callq	lua_type
	testl	%eax, %eax
	je	.LBB12_3
# BB#1:
	cmpl	$5, %eax
	je	.LBB12_3
# BB#2:
	movl	$2, %esi
	movl	$.L.str.41, %edx
	movq	%rbx, %rdi
	callq	luaL_argerror
.LBB12_3:
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_setmetatable
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	lua_pushboolean
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end12:
	.size	db_setmetatable, .Lfunc_end12-db_setmetatable
	.cfi_endproc

	.p2align	4, 0x90
	.type	db_setupvalue,@function
db_setupvalue:                          # @db_setupvalue
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 32
.Lcfi72:
	.cfi_offset %rbx, -32
.Lcfi73:
	.cfi_offset %r14, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$3, %esi
	callq	luaL_checkany
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	luaL_checkinteger
	movq	%rax, %r14
	movl	$1, %esi
	movl	$6, %edx
	movq	%rbx, %rdi
	callq	luaL_checktype
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_iscfunction
	xorl	%ebp, %ebp
	testl	%eax, %eax
	jne	.LBB13_3
# BB#1:
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r14d, %edx
	callq	lua_setupvalue
	testq	%rax, %rax
	je	.LBB13_3
# BB#2:
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	lua_pushstring
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_insert
	movl	$1, %ebp
.LBB13_3:                               # %auxupvalue.exit
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end13:
	.size	db_setupvalue, .Lfunc_end13-db_setupvalue
	.cfi_endproc

	.p2align	4, 0x90
	.type	db_errorfb,@function
db_errorfb:                             # @db_errorfb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi78:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi79:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi81:
	.cfi_def_cfa_offset 192
.Lcfi82:
	.cfi_offset %rbx, -56
.Lcfi83:
	.cfi_offset %r12, -48
.Lcfi84:
	.cfi_offset %r13, -40
.Lcfi85:
	.cfi_offset %r14, -32
.Lcfi86:
	.cfi_offset %r15, -24
.Lcfi87:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$1, %esi
	callq	lua_type
	xorl	%ebp, %ebp
	cmpl	$8, %eax
	movq	%r14, %r15
	jne	.LBB14_2
# BB#1:
	movl	$1, %esi
	movq	%r14, %rdi
	callq	lua_tothread
	movq	%rax, %r15
	movl	$1, %ebp
.LBB14_2:                               # %getthread.exit
	leal	2(%rbp), %ebx
	movq	%r14, %rdi
	movl	%ebx, %esi
	callq	lua_isnumber
	testl	%eax, %eax
	je	.LBB14_4
# BB#3:
	movq	%r14, %rdi
	movl	%ebx, %esi
	callq	lua_tointeger
	movq	%rax, %rbx
	movl	$-2, %esi
	movq	%r14, %rdi
	callq	lua_settop
	jmp	.LBB14_5
.LBB14_4:
	xorl	%ebx, %ebx
	cmpq	%r14, %r15
	sete	%bl
.LBB14_5:
	movq	%r14, %rdi
	callq	lua_gettop
	cmpl	%ebp, %eax
	jne	.LBB14_7
# BB#6:
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movl	$.L.str.42, %esi
	xorl	%edx, %edx
	jmp	.LBB14_9
.LBB14_7:
	leal	1(%rbp), %esi
	movq	%r14, %rdi
	callq	lua_isstring
	testl	%eax, %eax
	je	.LBB14_32
# BB#8:
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movl	$.L.str.17, %esi
	movl	$1, %edx
.LBB14_9:
	movq	%r14, %rdi
	callq	lua_pushlstring
	movl	$.L.str.43, %esi
	movl	$16, %edx
	movq	%r14, %rdi
	callq	lua_pushlstring
	movl	$1, %r12d
	leaq	16(%rsp), %r13
	cmpl	$12, %ebx
	jl	.LBB14_30
	jmp	.LBB14_11
	.p2align	4, 0x90
.LBB14_29:
	incl	%ebx
	movq	%r14, %rdi
	callq	lua_gettop
	subl	8(%rsp), %eax           # 4-byte Folded Reload
	movq	%r14, %rdi
	movl	%eax, %esi
	callq	lua_concat
	cmpl	$12, %ebx
	jl	.LBB14_30
.LBB14_11:                              # %.outer.split.us.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_12 Depth 2
                                        #     Child Loop BB14_21 Depth 2
	leal	11(%rbx), %ebp
	.p2align	4, 0x90
.LBB14_12:                              # %.outer.split.us
                                        #   Parent Loop BB14_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rdi
	movl	%ebx, %esi
	movq	%r13, %rdx
	callq	lua_getstack
	testl	%eax, %eax
	je	.LBB14_31
# BB#13:                                #   in Loop: Header=BB14_12 Depth=2
	testl	%r12d, %r12d
	je	.LBB14_14
# BB#19:                                #   in Loop: Header=BB14_12 Depth=2
	movq	%r15, %rdi
	movl	%ebp, %esi
	movq	%r13, %rdx
	callq	lua_getstack
	xorl	%r12d, %r12d
	testl	%eax, %eax
	je	.LBB14_12
# BB#20:                                # %.us-lcssa50.us
                                        #   in Loop: Header=BB14_11 Depth=1
	movl	$.L.str.44, %esi
	movl	$5, %edx
	movq	%r14, %rdi
	callq	lua_pushlstring
	.p2align	4, 0x90
.LBB14_21:                              #   Parent Loop BB14_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %esi
	leal	1(%rsi), %ebx
	addl	$11, %esi
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r13, %rdx
	callq	lua_getstack
	testl	%eax, %eax
	jne	.LBB14_21
# BB#22:                                #   in Loop: Header=BB14_11 Depth=1
	xorl	%r12d, %r12d
	cmpl	$12, %ebx
	jge	.LBB14_11
	.p2align	4, 0x90
.LBB14_30:                              # %.outer.split
	movq	%r15, %rdi
	movl	%ebx, %esi
	movq	%r13, %rdx
	callq	lua_getstack
	testl	%eax, %eax
	jne	.LBB14_15
	jmp	.LBB14_31
	.p2align	4, 0x90
.LBB14_14:
	xorl	%r12d, %r12d
.LBB14_15:                              # %.us-lcssa49.us
	movl	$.L.str.45, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	callq	lua_pushlstring
	movl	$.L.str.46, %esi
	movq	%r15, %rdi
	movq	%r13, %rdx
	callq	lua_getinfo
	movl	$.L.str.47, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	leaq	72(%rsp), %rdx
	callq	lua_pushfstring
	movl	56(%rsp), %edx
	testl	%edx, %edx
	jle	.LBB14_17
# BB#16:
	movl	$.L.str.48, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	lua_pushfstring
.LBB14_17:
	movq	32(%rsp), %rax
	cmpb	$0, (%rax)
	je	.LBB14_23
# BB#18:
	movq	24(%rsp), %rdx
	movl	$.L.str.49, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	lua_pushfstring
	jmp	.LBB14_29
	.p2align	4, 0x90
.LBB14_23:
	movq	40(%rsp), %rax
	movb	(%rax), %al
	cmpb	$67, %al
	je	.LBB14_27
# BB#24:
	cmpb	$116, %al
	je	.LBB14_27
# BB#25:
	cmpb	$109, %al
	jne	.LBB14_28
# BB#26:
	movl	$.L.str.50, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	lua_pushfstring
	jmp	.LBB14_29
	.p2align	4, 0x90
.LBB14_27:
	movl	$.L.str.51, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	callq	lua_pushlstring
	jmp	.LBB14_29
.LBB14_28:
	movl	64(%rsp), %ecx
	movl	$.L.str.52, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	leaq	72(%rsp), %rdx
	callq	lua_pushfstring
	jmp	.LBB14_29
.LBB14_31:                              # %.us-lcssa.us
	movq	%r14, %rdi
	callq	lua_gettop
	subl	8(%rsp), %eax           # 4-byte Folded Reload
	movq	%r14, %rdi
	movl	%eax, %esi
	callq	lua_concat
.LBB14_32:
	movl	$1, %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	db_errorfb, .Lfunc_end14-db_errorfb
	.cfi_endproc

	.p2align	4, 0x90
	.type	hookf,@function
hookf:                                  # @hookf
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi90:
	.cfi_def_cfa_offset 32
.Lcfi91:
	.cfi_offset %rbx, -24
.Lcfi92:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$KEY_HOOK, %esi
	callq	lua_pushlightuserdata
	movl	$-10000, %esi           # imm = 0xD8F0
	movq	%rbx, %rdi
	callq	lua_rawget
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	callq	lua_pushlightuserdata
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_rawget
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	cmpl	$6, %eax
	jne	.LBB15_5
# BB#1:
	movslq	(%r14), %rax
	movq	hookf.hooknames(,%rax,8), %rsi
	movq	%rbx, %rdi
	callq	lua_pushstring
	movslq	40(%r14), %rsi
	movq	%rbx, %rdi
	testq	%rsi, %rsi
	js	.LBB15_3
# BB#2:
	callq	lua_pushinteger
	jmp	.LBB15_4
.LBB15_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB15_3:
	callq	lua_pushnil
.LBB15_4:
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	lua_call                # TAILCALL
.Lfunc_end15:
	.size	hookf, .Lfunc_end15-hookf
	.cfi_endproc

	.p2align	4, 0x90
	.type	gethooktable,@function
gethooktable:                           # @gethooktable
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 16
.Lcfi94:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$KEY_HOOK, %esi
	callq	lua_pushlightuserdata
	movl	$-10000, %esi           # imm = 0xD8F0
	movq	%rbx, %rdi
	callq	lua_rawget
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	cmpl	$5, %eax
	jne	.LBB16_2
# BB#1:
	popq	%rbx
	retq
.LBB16_2:
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_createtable
	movl	$KEY_HOOK, %esi
	movq	%rbx, %rdi
	callq	lua_pushlightuserdata
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-10000, %esi           # imm = 0xD8F0
	movq	%rbx, %rdi
	popq	%rbx
	jmp	lua_rawset              # TAILCALL
.Lfunc_end16:
	.size	gethooktable, .Lfunc_end16-gethooktable
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"debug"
	.size	.L.str, 6

	.type	dblib,@object           # @dblib
	.section	.rodata,"a",@progbits
	.p2align	4
dblib:
	.quad	.L.str
	.quad	db_debug
	.quad	.L.str.1
	.quad	db_getfenv
	.quad	.L.str.2
	.quad	db_gethook
	.quad	.L.str.3
	.quad	db_getinfo
	.quad	.L.str.4
	.quad	db_getlocal
	.quad	.L.str.5
	.quad	db_getregistry
	.quad	.L.str.6
	.quad	db_getmetatable
	.quad	.L.str.7
	.quad	db_getupvalue
	.quad	.L.str.8
	.quad	db_setfenv
	.quad	.L.str.9
	.quad	db_sethook
	.quad	.L.str.10
	.quad	db_setlocal
	.quad	.L.str.11
	.quad	db_setmetatable
	.quad	.L.str.12
	.quad	db_setupvalue
	.quad	.L.str.13
	.quad	db_errorfb
	.zero	16
	.size	dblib, 240

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"getfenv"
	.size	.L.str.1, 8

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"gethook"
	.size	.L.str.2, 8

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"getinfo"
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"getlocal"
	.size	.L.str.4, 9

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"getregistry"
	.size	.L.str.5, 12

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"getmetatable"
	.size	.L.str.6, 13

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"getupvalue"
	.size	.L.str.7, 11

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"setfenv"
	.size	.L.str.8, 8

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"sethook"
	.size	.L.str.9, 8

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"setlocal"
	.size	.L.str.10, 9

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"setmetatable"
	.size	.L.str.11, 13

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"setupvalue"
	.size	.L.str.12, 11

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"traceback"
	.size	.L.str.13, 10

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"lua_debug> "
	.size	.L.str.14, 12

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"cont\n"
	.size	.L.str.15, 6

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"=(debug command)"
	.size	.L.str.16, 17

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"\n"
	.size	.L.str.17, 2

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"external hook"
	.size	.L.str.18, 14

	.type	hookf.hooknames,@object # @hookf.hooknames
	.section	.rodata,"a",@progbits
	.p2align	4
hookf.hooknames:
	.quad	.L.str.19
	.quad	.L.str.20
	.quad	.L.str.21
	.quad	.L.str.22
	.quad	.L.str.23
	.size	hookf.hooknames, 40

	.type	.L.str.19,@object       # @.str.19
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.19:
	.asciz	"call"
	.size	.L.str.19, 5

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"return"
	.size	.L.str.20, 7

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"line"
	.size	.L.str.21, 5

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"count"
	.size	.L.str.22, 6

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"tail return"
	.size	.L.str.23, 12

	.type	KEY_HOOK,@object        # @KEY_HOOK
	.section	.rodata,"a",@progbits
KEY_HOOK:
	.byte	104                     # 0x68
	.size	KEY_HOOK, 1

	.type	.L.str.24,@object       # @.str.24
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.24:
	.asciz	"flnSu"
	.size	.L.str.24, 6

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	">%s"
	.size	.L.str.25, 4

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"function or level expected"
	.size	.L.str.26, 27

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"invalid option"
	.size	.L.str.27, 15

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"source"
	.size	.L.str.28, 7

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"short_src"
	.size	.L.str.29, 10

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"linedefined"
	.size	.L.str.30, 12

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"lastlinedefined"
	.size	.L.str.31, 16

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"what"
	.size	.L.str.32, 5

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"currentline"
	.size	.L.str.33, 12

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"nups"
	.size	.L.str.34, 5

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"name"
	.size	.L.str.35, 5

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"namewhat"
	.size	.L.str.36, 9

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"activelines"
	.size	.L.str.37, 12

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"func"
	.size	.L.str.38, 5

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"level out of range"
	.size	.L.str.39, 19

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"'setfenv' cannot change environment of given object"
	.size	.L.str.40, 52

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"nil or table expected"
	.size	.L.str.41, 22

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.zero	1
	.size	.L.str.42, 1

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"stack traceback:"
	.size	.L.str.43, 17

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"\n\t..."
	.size	.L.str.44, 6

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"\n\t"
	.size	.L.str.45, 3

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"Snl"
	.size	.L.str.46, 4

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"%s:"
	.size	.L.str.47, 4

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"%d:"
	.size	.L.str.48, 4

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	" in function '%s'"
	.size	.L.str.49, 18

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	" in main chunk"
	.size	.L.str.50, 15

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	" ?"
	.size	.L.str.51, 3

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	" in function <%s:%d>"
	.size	.L.str.52, 21


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
