	.text
	.file	"libclamav_rtf.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16
	.text
	.globl	cli_scanrtf
	.p2align	4, 0x90
	.type	cli_scanrtf,@function
cli_scanrtf:                            # @cli_scanrtf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$424, %rsp              # imm = 0x1A8
.Lcfi6:
	.cfi_def_cfa_offset 480
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, 120(%rsp)         # 8-byte Spill
	movl	%edi, %r12d
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 400(%rsp)
	movaps	%xmm0, 384(%rsp)
	movaps	%xmm0, 368(%rsp)
	movaps	%xmm0, 352(%rsp)
	movaps	%xmm0, 336(%rsp)
	movaps	%xmm0, 320(%rsp)
	movaps	%xmm0, 304(%rsp)
	movaps	%xmm0, 288(%rsp)
	movaps	%xmm0, 272(%rsp)
	movaps	%xmm0, 256(%rsp)
	movaps	%xmm0, 240(%rsp)
	movaps	%xmm0, 224(%rsp)
	movaps	%xmm0, 208(%rsp)
	movaps	%xmm0, 192(%rsp)
	movaps	%xmm0, 176(%rsp)
	movaps	%xmm0, 160(%rsp)
	movb	$1, 283(%rsp)
	movb	$1, 285(%rsp)
	movb	$1, 252(%rsp)
	movl	$1664, %edi             # imm = 0x680
	callq	cli_malloc
	movq	%rax, %rbp
	movl	$-114, %ebx
	testq	%rbp, %rbp
	je	.LBB0_159
# BB#1:
	movl	$8192, %edi             # imm = 0x2000
	callq	cli_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_4
# BB#2:
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r14
	movl	$448, %esi              # imm = 0x1C0
	movq	%r14, %rdi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB0_5
# BB#3:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movl	$-118, %ebx
	jmp	.LBB0_159
.LBB0_4:
	movq	%rbp, %rdi
	callq	free
	jmp	.LBB0_159
.LBB0_5:
	callq	tableCreate
	movq	%rax, %rbx
	movl	$.L.str.6, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	tableInsert
	cmpl	$-1, %eax
	je	.LBB0_86
# BB#6:                                 # %load_actions.exit
	movl	$.L.str.7, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	tableInsert
	cmpl	$-1, %eax
	je	.LBB0_86
# BB#7:
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	movq	%r14, 128(%rsp)         # 8-byte Spill
	movq	base_state+96(%rip), %rax
	movq	%rax, 96(%rsp)
	movups	base_state+80(%rip), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	base_state+64(%rip), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	base_state+48(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	base_state+32(%rip), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	base_state+16(%rip), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	base_state(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movl	$0, 24(%rsp)
	movq	$0, 8(%rsp)
	movl	$8192, %edx             # imm = 0x2000
	movl	%r12d, %edi
	movq	%r15, %rsi
	callq	cli_readn
	testl	%eax, %eax
	jle	.LBB0_89
# BB#8:                                 # %.lr.ph293
	movl	%r12d, 148(%rsp)        # 4-byte Spill
	movl	$16, %ecx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	xorl	%r12d, %r12d
	movl	$0, 144(%rsp)           # 4-byte Folded Spill
	movq	%r15, 136(%rsp)         # 8-byte Spill
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_9:                                # %.lr.ph276.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_11 Depth 2
                                        #       Child Loop BB0_56 Depth 3
	movslq	%eax, %r14
	addq	%r15, %r14
	movq	112(%rsp), %rbx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph276
                                        #   Parent Loop BB0_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_56 Depth 3
	movl	24(%rsp), %eax
	cmpq	$5, %rax
	ja	.LBB0_84
# BB#12:                                # %.lr.ph276
                                        #   in Loop: Header=BB0_11 Depth=2
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_13:                               #   in Loop: Header=BB0_11 Depth=2
	leaq	1(%r15), %r13
	movb	(%r15), %al
	cmpb	$92, %al
	je	.LBB0_48
# BB#14:                                #   in Loop: Header=BB0_11 Depth=2
	cmpb	$125, %al
	je	.LBB0_49
# BB#15:                                #   in Loop: Header=BB0_11 Depth=2
	cmpb	$123, %al
	jne	.LBB0_54
# BB#16:                                #   in Loop: Header=BB0_11 Depth=2
	cmpq	$0, 16(%rsp)
	jne	.LBB0_64
# BB#17:                                #   in Loop: Header=BB0_11 Depth=2
	movl	32(%rsp), %eax
	testl	%eax, %eax
	jne	.LBB0_64
# BB#18:                                #   in Loop: Header=BB0_11 Depth=2
	movl	$base_state+36, %esi
	movl	$33, %edx
	leaq	36(%rsp), %rdi
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB0_64
# BB#19:                                #   in Loop: Header=BB0_11 Depth=2
	movq	72(%rsp), %rax
	testq	%rax, %rax
	jne	.LBB0_64
# BB#20:                                #   in Loop: Header=BB0_11 Depth=2
	movq	80(%rsp), %rax
	testq	%rax, %rax
	jne	.LBB0_64
# BB#21:                                #   in Loop: Header=BB0_11 Depth=2
	movq	88(%rsp), %rax
	testq	%rax, %rax
	jne	.LBB0_64
# BB#22:                                #   in Loop: Header=BB0_11 Depth=2
	movq	96(%rsp), %rax
	testq	%rax, %rax
	jne	.LBB0_64
# BB#23:                                #   in Loop: Header=BB0_11 Depth=2
	incq	(%rsp)
	movq	%r13, %r15
	cmpq	%r14, %r15
	jb	.LBB0_11
	jmp	.LBB0_85
.LBB0_24:                               #   in Loop: Header=BB0_11 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	(%r15), %ecx
	testb	$4, 1(%rax,%rcx,2)
	jne	.LBB0_41
# BB#25:                                #   in Loop: Header=BB0_11 Depth=2
	movl	$3, 24(%rsp)
	cmpq	%r14, %r15
	jb	.LBB0_11
	jmp	.LBB0_85
.LBB0_26:                               #   in Loop: Header=BB0_11 Depth=2
	movq	8(%rsp), %rbp
	cmpq	$32, %rbp
	jne	.LBB0_42
# BB#27:                                #   in Loop: Header=BB0_11 Depth=2
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	leaq	36(%rsp), %rsi
	callq	cli_dbgmsg
	movl	$0, 24(%rsp)
	cmpq	%r14, %r15
	jb	.LBB0_11
	jmp	.LBB0_85
.LBB0_28:                               #   in Loop: Header=BB0_11 Depth=2
	incq	%r15
	movl	$0, 24(%rsp)
	cmpq	%r14, %r15
	jb	.LBB0_11
	jmp	.LBB0_85
.LBB0_29:                               #   in Loop: Header=BB0_11 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movzbl	(%r15), %eax
	movzwl	(%rcx,%rax,2), %ecx
	testb	$8, %ch
	jne	.LBB0_47
# BB#30:                                #   in Loop: Header=BB0_11 Depth=2
	testb	$4, %ch
	jne	.LBB0_59
# BB#31:                                #   in Loop: Header=BB0_11 Depth=2
	cmpl	$0, 28(%rsp)
	jns	.LBB0_33
# BB#32:                                #   in Loop: Header=BB0_11 Depth=2
	negq	16(%rsp)
.LBB0_33:                               #   in Loop: Header=BB0_11 Depth=2
	movl	$5, 24(%rsp)
	cmpq	%r14, %r15
	jb	.LBB0_11
	jmp	.LBB0_85
.LBB0_34:                               #   in Loop: Header=BB0_11 Depth=2
	movq	8(%rsp), %rax
	movb	$0, 36(%rsp,%rax)
	movq	%rbx, %rdi
	leaq	36(%rsp), %rsi
	callq	tableFind
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	je	.LBB0_63
# BB#35:                                #   in Loop: Header=BB0_11 Depth=2
	cmpq	$0, 96(%rsp)
	je	.LBB0_38
# BB#36:                                #   in Loop: Header=BB0_11 Depth=2
	movq	88(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_38
# BB#37:                                #   in Loop: Header=BB0_11 Depth=2
	movq	%rsp, %rdi
	movq	120(%rsp), %rsi         # 8-byte Reload
	callq	*%rax
	movq	$0, 72(%rsp)
	leaq	36(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, 52(%rax)
.LBB0_38:                               #   in Loop: Header=BB0_11 Depth=2
	movslq	%ebp, %rax
	cmpq	$1, %rax
	je	.LBB0_61
# BB#39:                                #   in Loop: Header=BB0_11 Depth=2
	testq	%rax, %rax
	jne	.LBB0_63
# BB#40:                                #   in Loop: Header=BB0_11 Depth=2
	orb	$1, 32(%rsp)
	movl	$0, 24(%rsp)
	cmpq	%r14, %r15
	jb	.LBB0_11
	jmp	.LBB0_85
.LBB0_41:                               #   in Loop: Header=BB0_11 Depth=2
	movl	$2, 24(%rsp)
	movq	$0, 8(%rsp)
	cmpq	%r14, %r15
	jb	.LBB0_11
	jmp	.LBB0_85
.LBB0_42:                               #   in Loop: Header=BB0_11 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movzbl	(%r15), %eax
	movzwl	(%rcx,%rax,2), %ecx
	testb	$4, %ch
	jne	.LBB0_60
# BB#43:                                #   in Loop: Header=BB0_11 Depth=2
	testb	$32, %ch
	jne	.LBB0_66
# BB#44:                                #   in Loop: Header=BB0_11 Depth=2
	testb	$8, %ch
	jne	.LBB0_78
# BB#45:                                #   in Loop: Header=BB0_11 Depth=2
	cmpb	$45, %al
	jne	.LBB0_10
# BB#46:                                #   in Loop: Header=BB0_11 Depth=2
	incq	%r15
	movl	$4, 24(%rsp)
	movq	$0, 16(%rsp)
	movl	$-1, 28(%rsp)
	cmpq	%r14, %r15
	jb	.LBB0_11
	jmp	.LBB0_85
.LBB0_47:                               #   in Loop: Header=BB0_11 Depth=2
	movq	16(%rsp), %rcx
	leaq	(%rcx,%rcx,4), %rcx
	incq	%r15
	leaq	-48(%rax,%rcx,2), %rax
	movq	%rax, 16(%rsp)
	cmpq	%r14, %r15
	jb	.LBB0_11
	jmp	.LBB0_85
.LBB0_48:                               #   in Loop: Header=BB0_11 Depth=2
	movl	$1, 24(%rsp)
	movq	%r13, %r15
	cmpq	%r14, %r15
	jb	.LBB0_11
	jmp	.LBB0_85
.LBB0_49:                               #   in Loop: Header=BB0_11 Depth=2
	cmpq	$0, 96(%rsp)
	movq	104(%rsp), %rbx         # 8-byte Reload
	je	.LBB0_52
# BB#50:                                #   in Loop: Header=BB0_11 Depth=2
	movq	88(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_52
# BB#51:                                #   in Loop: Header=BB0_11 Depth=2
	movq	%rsp, %rdi
	movq	120(%rsp), %rsi         # 8-byte Reload
	callq	*%rax
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jne	.LBB0_117
.LBB0_52:                               #   in Loop: Header=BB0_11 Depth=2
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_70
# BB#53:                                #   in Loop: Header=BB0_11 Depth=2
	decq	%rax
	movl	32(%rsp), %ecx
	movq	base_state+96(%rip), %rdx
	movq	%rdx, 96(%rsp)
	movups	base_state+80(%rip), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	base_state+64(%rip), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	base_state+48(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	base_state+32(%rip), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	base_state+16(%rip), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	base_state(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movq	%rax, (%rsp)
	movl	%ecx, 32(%rsp)
	jmp	.LBB0_82
.LBB0_54:                               #   in Loop: Header=BB0_11 Depth=2
	movq	120(%rsp), %rbp         # 8-byte Reload
	movq	%r14, %r13
	subq	%r15, %r13
	cmpq	$2, %r13
	jb	.LBB0_58
# BB#55:                                # %.lr.ph261.preheader
                                        #   in Loop: Header=BB0_11 Depth=2
	movl	$1, %eax
	.p2align	4, 0x90
.LBB0_56:                               # %.lr.ph261
                                        #   Parent Loop BB0_9 Depth=1
                                        #     Parent Loop BB0_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%r15,%rax), %ecx
	cmpb	$0, 160(%rsp,%rcx)
	jne	.LBB0_72
# BB#57:                                #   in Loop: Header=BB0_56 Depth=3
	incq	%rax
	cmpq	%r13, %rax
	jb	.LBB0_56
.LBB0_58:                               #   in Loop: Header=BB0_11 Depth=2
	movq	%r12, %rbx
	jmp	.LBB0_73
.LBB0_59:                               #   in Loop: Header=BB0_11 Depth=2
	incq	%r15
	cmpq	%r14, %r15
	jb	.LBB0_11
	jmp	.LBB0_85
.LBB0_60:                               #   in Loop: Header=BB0_11 Depth=2
	incq	%r15
	leaq	1(%rbp), %rcx
	movq	%rcx, 8(%rsp)
	movb	%al, 36(%rsp,%rbp)
	cmpq	%r14, %r15
	jb	.LBB0_11
	jmp	.LBB0_85
.LBB0_61:                               #   in Loop: Header=BB0_11 Depth=2
	testb	$1, 32(%rsp)
	je	.LBB0_63
# BB#62:                                #   in Loop: Header=BB0_11 Depth=2
	movq	$rtf_object_begin, 72(%rsp)
	movq	$rtf_object_process, 80(%rsp)
	movq	$rtf_object_end, 88(%rsp)
.LBB0_63:                               # %rtf_action.exit
                                        #   in Loop: Header=BB0_11 Depth=2
	movl	$0, 24(%rsp)
	cmpq	%r14, %r15
	jb	.LBB0_11
	jmp	.LBB0_85
.LBB0_64:                               # %compare_state.exit.thread.i
                                        #   in Loop: Header=BB0_11 Depth=2
	movq	152(%rsp), %rbp         # 8-byte Reload
	cmpq	%rbp, %r12
	jae	.LBB0_67
# BB#65:                                #   in Loop: Header=BB0_11 Depth=2
	movq	104(%rsp), %rbx         # 8-byte Reload
	jmp	.LBB0_69
.LBB0_66:                               #   in Loop: Header=BB0_11 Depth=2
	incq	%r15
	leaq	1(%rbp), %rcx
	movq	%rcx, 8(%rsp)
	movb	%al, 36(%rsp,%rbp)
	movl	$5, 24(%rsp)
	cmpq	%r14, %r15
	jb	.LBB0_11
	jmp	.LBB0_85
.LBB0_67:                               #   in Loop: Header=BB0_11 Depth=2
	subq	$-128, %rbp
	imulq	$104, %rbp, %rsi
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	cli_realloc2
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_131
# BB#68:                                #   in Loop: Header=BB0_11 Depth=2
	movq	%rbp, 152(%rsp)         # 8-byte Spill
.LBB0_69:                               # %compare_state.exit.thread._crit_edge.i
                                        #   in Loop: Header=BB0_11 Depth=2
	imulq	$104, %r12, %rax
	incq	%r12
	movq	96(%rsp), %rcx
	movq	%rcx, 96(%rbx,%rax)
	movaps	80(%rsp), %xmm0
	movups	%xmm0, 80(%rbx,%rax)
	movaps	64(%rsp), %xmm0
	movups	%xmm0, 64(%rbx,%rax)
	movaps	(%rsp), %xmm0
	movaps	16(%rsp), %xmm1
	movaps	32(%rsp), %xmm2
	movaps	48(%rsp), %xmm3
	movups	%xmm3, 48(%rbx,%rax)
	movups	%xmm2, 32(%rbx,%rax)
	movups	%xmm1, 16(%rbx,%rax)
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	movups	%xmm0, (%rbx,%rax)
	movl	32(%rsp), %eax
	movq	(%rsp), %rcx
	movq	base_state+96(%rip), %rdx
	movq	%rdx, 96(%rsp)
	movups	base_state+80(%rip), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	base_state+64(%rip), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	base_state+48(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	base_state+32(%rip), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	base_state+16(%rip), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	base_state(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movl	%eax, 32(%rsp)
	movq	%rcx, (%rsp)
	jmp	.LBB0_82
.LBB0_70:                               #   in Loop: Header=BB0_11 Depth=2
	testq	%r12, %r12
	je	.LBB0_79
# BB#71:                                #   in Loop: Header=BB0_11 Depth=2
	decq	%r12
	imulq	$104, %r12, %rax
	movq	96(%rbx,%rax), %rcx
	movq	%rcx, 96(%rsp)
	movups	80(%rbx,%rax), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	64(%rbx,%rax), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	(%rbx,%rax), %xmm0
	movups	16(%rbx,%rax), %xmm1
	movups	32(%rbx,%rax), %xmm2
	movups	48(%rbx,%rax), %xmm3
	movaps	%xmm3, 48(%rsp)
	movaps	%xmm2, 32(%rsp)
	movaps	%xmm1, 16(%rsp)
	movaps	%xmm0, (%rsp)
	jmp	.LBB0_82
.LBB0_72:                               #   in Loop: Header=BB0_11 Depth=2
	movq	%r12, %rbx
	movq	%rax, %r13
.LBB0_73:                               # %._crit_edge
                                        #   in Loop: Header=BB0_11 Depth=2
	movq	72(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_77
# BB#74:                                #   in Loop: Header=BB0_11 Depth=2
	cmpq	$0, 96(%rsp)
	jne	.LBB0_76
# BB#75:                                #   in Loop: Header=BB0_11 Depth=2
	movq	%rsp, %rdi
	movq	%rbp, %rsi
	movq	128(%rsp), %rdx         # 8-byte Reload
	callq	*%rax
	movl	%eax, %r12d
	testl	%r12d, %r12d
	jne	.LBB0_145
.LBB0_76:                               #   in Loop: Header=BB0_11 Depth=2
	movq	%rsp, %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	*80(%rsp)
	movl	%eax, %r12d
	testl	%r12d, %r12d
	jne	.LBB0_104
.LBB0_77:                               #   in Loop: Header=BB0_11 Depth=2
	addq	%r13, %r15
	movq	%rbx, %r12
	jmp	.LBB0_83
.LBB0_78:                               #   in Loop: Header=BB0_11 Depth=2
	movl	$4, 24(%rsp)
	movq	$0, 16(%rsp)
	movl	$1, 28(%rsp)
	cmpq	%r14, %r15
	jb	.LBB0_11
	jmp	.LBB0_85
.LBB0_79:                               #   in Loop: Header=BB0_11 Depth=2
	cmpl	$0, 144(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_81
# BB#80:                                #   in Loop: Header=BB0_11 Depth=2
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, 144(%rsp)           # 4-byte Folded Spill
.LBB0_81:                               #   in Loop: Header=BB0_11 Depth=2
	movq	base_state+96(%rip), %rax
	movq	%rax, 96(%rsp)
	movups	base_state+80(%rip), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	base_state+64(%rip), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	base_state+48(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	base_state+32(%rip), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	base_state+16(%rip), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	base_state(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	xorl	%r12d, %r12d
.LBB0_82:                               # %pop_state.exit.backedge
                                        #   in Loop: Header=BB0_11 Depth=2
	movq	%r13, %r15
.LBB0_83:                               # %pop_state.exit.backedge
                                        #   in Loop: Header=BB0_11 Depth=2
	movq	112(%rsp), %rbx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_84:                               # %pop_state.exit.backedge
                                        #   in Loop: Header=BB0_11 Depth=2
	cmpq	%r14, %r15
	jb	.LBB0_11
	jmp	.LBB0_85
.LBB0_10:                               #   in Loop: Header=BB0_11 Depth=2
	movl	$5, 24(%rsp)
	cmpq	%r14, %r15
	jb	.LBB0_11
	.p2align	4, 0x90
.LBB0_85:                               # %.loopexit
                                        #   in Loop: Header=BB0_9 Depth=1
	movl	$8192, %edx             # imm = 0x2000
	movl	148(%rsp), %edi         # 4-byte Reload
	movq	136(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rsi
	callq	cli_readn
	testl	%eax, %eax
	jg	.LBB0_9
	jmp	.LBB0_90
.LBB0_86:                               # %load_actions.exit.thread
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_88
# BB#87:
	movq	%r14, %rdi
	callq	cli_rmdirs
.LBB0_88:
	movq	%r14, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	tableDestroy
	movl	$-1, %ebx
	jmp	.LBB0_159
.LBB0_89:                               # %.._crit_edge294_crit_edge
	xorl	%r12d, %r12d
.LBB0_90:                               # %._crit_edge294
	cmpq	$0, 96(%rsp)
	je	.LBB0_93
# BB#91:
	movq	88(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_93
# BB#92:
	movq	%rsp, %rdi
	movq	120(%rsp), %rsi         # 8-byte Reload
	callq	*%rax
.LBB0_93:                               # %._crit_edge294._crit_edge
	movq	%rbx, %rdi
	callq	tableDestroy
	testq	%r12, %r12
	movq	128(%rsp), %r14         # 8-byte Reload
	movq	104(%rsp), %rbp         # 8-byte Reload
	je	.LBB0_101
# BB#94:                                # %.lr.ph259.preheader
	movq	%rsp, %rbx
	.p2align	4, 0x90
.LBB0_95:                               # %.lr.ph259
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_97
# BB#96:                                #   in Loop: Header=BB0_95 Depth=1
	decq	%rax
	movl	32(%rsp), %ecx
	movq	base_state+96(%rip), %rdx
	movq	%rdx, 96(%rsp)
	movups	base_state+80(%rip), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	base_state+64(%rip), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	base_state+48(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	base_state+32(%rip), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	base_state+16(%rip), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	base_state(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movq	%rax, (%rsp)
	movl	%ecx, 32(%rsp)
	cmpq	$0, 96(%rsp)
	jne	.LBB0_98
	jmp	.LBB0_100
	.p2align	4, 0x90
.LBB0_97:                               #   in Loop: Header=BB0_95 Depth=1
	decq	%r12
	imulq	$104, %r12, %rax
	movq	96(%rbp,%rax), %rcx
	movq	%rcx, 96(%rsp)
	movups	80(%rbp,%rax), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	64(%rbp,%rax), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	(%rbp,%rax), %xmm0
	movups	16(%rbp,%rax), %xmm1
	movups	32(%rbp,%rax), %xmm2
	movups	48(%rbp,%rax), %xmm3
	movaps	%xmm3, 48(%rsp)
	movaps	%xmm2, 32(%rsp)
	movaps	%xmm1, 16(%rsp)
	movaps	%xmm0, (%rsp)
	cmpq	$0, 96(%rsp)
	je	.LBB0_100
.LBB0_98:                               #   in Loop: Header=BB0_95 Depth=1
	movq	88(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_100
# BB#99:                                #   in Loop: Header=BB0_95 Depth=1
	movq	%rbx, %rdi
	movq	120(%rsp), %rsi         # 8-byte Reload
	callq	*%rax
.LBB0_100:                              # %.backedge.i135.backedge
                                        #   in Loop: Header=BB0_95 Depth=1
	testq	%r12, %r12
	jne	.LBB0_95
.LBB0_101:                              # %cleanup_stack.exit137
	movq	%r15, %rdi
	callq	free
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_103
# BB#102:
	movq	%r14, %rdi
	callq	cli_rmdirs
.LBB0_103:
	movq	%r14, %rdi
	callq	free
	movq	%rbp, %rdi
	callq	free
	xorl	%ebx, %ebx
	jmp	.LBB0_159
.LBB0_104:
	movq	88(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_106
# BB#105:
	movq	%rsp, %rdi
	movq	%rbp, %rsi
	callq	*%rax
.LBB0_106:
	cmpq	$0, 96(%rsp)
	movq	128(%rsp), %r14         # 8-byte Reload
	je	.LBB0_109
# BB#107:
	movq	88(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_109
# BB#108:
	movq	%rsp, %rdi
	movq	%rbp, %rsi
	callq	*%rax
.LBB0_109:
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	tableDestroy
	testq	%rbx, %rbx
	je	.LBB0_156
# BB#110:                               # %.lr.ph.preheader
	movq	%rsp, %r15
.LBB0_111:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_113
# BB#112:                               #   in Loop: Header=BB0_111 Depth=1
	decq	%rax
	movl	32(%rsp), %ecx
	movq	base_state+96(%rip), %rdx
	movq	%rdx, 96(%rsp)
	movups	base_state+80(%rip), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	base_state+64(%rip), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	base_state+48(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	base_state+32(%rip), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	base_state+16(%rip), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	base_state(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movq	%rax, (%rsp)
	movl	%ecx, 32(%rsp)
	cmpq	$0, 96(%rsp)
	jne	.LBB0_114
	jmp	.LBB0_116
.LBB0_113:                              #   in Loop: Header=BB0_111 Depth=1
	movq	%rbx, %rax
	decq	%rax
	movq	%rax, %rbx
	imulq	$104, %rax, %rax
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	96(%rdx,%rax), %rcx
	movq	%rcx, 96(%rsp)
	movups	80(%rdx,%rax), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	64(%rdx,%rax), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	(%rdx,%rax), %xmm0
	movups	16(%rdx,%rax), %xmm1
	movups	32(%rdx,%rax), %xmm2
	movups	48(%rdx,%rax), %xmm3
	movaps	%xmm3, 48(%rsp)
	movaps	%xmm2, 32(%rsp)
	movaps	%xmm1, 16(%rsp)
	movaps	%xmm0, (%rsp)
	cmpq	$0, 96(%rsp)
	je	.LBB0_116
.LBB0_114:                              #   in Loop: Header=BB0_111 Depth=1
	movq	88(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_116
# BB#115:                               #   in Loop: Header=BB0_111 Depth=1
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	*%rax
.LBB0_116:                              # %.backedge.i132.backedge
                                        #   in Loop: Header=BB0_111 Depth=1
	testq	%rbx, %rbx
	jne	.LBB0_111
	jmp	.LBB0_156
.LBB0_117:
	cmpq	$0, 96(%rsp)
	je	.LBB0_120
# BB#118:
	movq	88(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_120
# BB#119:
	movq	%rsp, %rdi
	movq	120(%rsp), %rsi         # 8-byte Reload
	callq	*%rax
.LBB0_120:
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	tableDestroy
	testq	%r12, %r12
	movq	128(%rsp), %rbp         # 8-byte Reload
	je	.LBB0_128
# BB#121:                               # %.lr.ph256.preheader
	movq	%rsp, %rbx
.LBB0_122:                              # %.lr.ph256
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_124
# BB#123:                               #   in Loop: Header=BB0_122 Depth=1
	decq	%rax
	movl	32(%rsp), %ecx
	movq	base_state+96(%rip), %rdx
	movq	%rdx, 96(%rsp)
	movups	base_state+80(%rip), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	base_state+64(%rip), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	base_state+48(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	base_state+32(%rip), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	base_state+16(%rip), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	base_state(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movq	%rax, (%rsp)
	movl	%ecx, 32(%rsp)
	cmpq	$0, 96(%rsp)
	jne	.LBB0_125
	jmp	.LBB0_127
.LBB0_124:                              #   in Loop: Header=BB0_122 Depth=1
	movq	%r12, %rax
	decq	%rax
	movq	%rax, %r12
	imulq	$104, %rax, %rax
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	96(%rdx,%rax), %rcx
	movq	%rcx, 96(%rsp)
	movups	80(%rdx,%rax), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	64(%rdx,%rax), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	(%rdx,%rax), %xmm0
	movups	16(%rdx,%rax), %xmm1
	movups	32(%rdx,%rax), %xmm2
	movups	48(%rdx,%rax), %xmm3
	movaps	%xmm3, 48(%rsp)
	movaps	%xmm2, 32(%rsp)
	movaps	%xmm1, 16(%rsp)
	movaps	%xmm0, (%rsp)
	cmpq	$0, 96(%rsp)
	je	.LBB0_127
.LBB0_125:                              #   in Loop: Header=BB0_122 Depth=1
	movq	88(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_127
# BB#126:                               #   in Loop: Header=BB0_122 Depth=1
	movq	%rbx, %rdi
	movq	120(%rsp), %rsi         # 8-byte Reload
	callq	*%rax
.LBB0_127:                              # %.backedge.i126.backedge
                                        #   in Loop: Header=BB0_122 Depth=1
	testq	%r12, %r12
	jne	.LBB0_122
.LBB0_128:                              # %cleanup_stack.exit128
	movq	136(%rsp), %rdi         # 8-byte Reload
	callq	free
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_130
# BB#129:
	movq	%rbp, %rdi
	callq	cli_rmdirs
.LBB0_130:
	movq	%rbp, %rdi
	callq	free
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	free
	movl	%r15d, %ebx
	jmp	.LBB0_159
.LBB0_131:                              # %push_state.exit
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpq	$0, 96(%rsp)
	je	.LBB0_134
# BB#132:
	movq	88(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_134
# BB#133:
	movq	%rsp, %rdi
	movq	120(%rsp), %rsi         # 8-byte Reload
	callq	*%rax
.LBB0_134:
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	tableDestroy
	testq	%r12, %r12
	movq	128(%rsp), %r14         # 8-byte Reload
	movq	136(%rsp), %r15         # 8-byte Reload
	je	.LBB0_142
# BB#135:                               # %.lr.ph253.preheader
	movq	%rsp, %rbp
.LBB0_136:                              # %.lr.ph253
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_138
# BB#137:                               #   in Loop: Header=BB0_136 Depth=1
	decq	%rax
	movl	32(%rsp), %ecx
	movq	base_state+96(%rip), %rdx
	movq	%rdx, 96(%rsp)
	movups	base_state+80(%rip), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	base_state+64(%rip), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	base_state+48(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	base_state+32(%rip), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	base_state+16(%rip), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	base_state(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movq	%rax, (%rsp)
	movl	%ecx, 32(%rsp)
	cmpq	$0, 96(%rsp)
	jne	.LBB0_139
	jmp	.LBB0_141
.LBB0_138:                              #   in Loop: Header=BB0_136 Depth=1
	decq	%r12
	imulq	$104, %r12, %rax
	movq	96(%rbx,%rax), %rcx
	movq	%rcx, 96(%rsp)
	movups	80(%rbx,%rax), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	64(%rbx,%rax), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	(%rbx,%rax), %xmm0
	movups	16(%rbx,%rax), %xmm1
	movups	32(%rbx,%rax), %xmm2
	movups	48(%rbx,%rax), %xmm3
	movaps	%xmm3, 48(%rsp)
	movaps	%xmm2, 32(%rsp)
	movaps	%xmm1, 16(%rsp)
	movaps	%xmm0, (%rsp)
	cmpq	$0, 96(%rsp)
	je	.LBB0_141
.LBB0_139:                              #   in Loop: Header=BB0_136 Depth=1
	movq	88(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_141
# BB#140:                               #   in Loop: Header=BB0_136 Depth=1
	movq	%rbp, %rdi
	movq	120(%rsp), %rsi         # 8-byte Reload
	callq	*%rax
.LBB0_141:                              # %.backedge.i.backedge
                                        #   in Loop: Header=BB0_136 Depth=1
	testq	%r12, %r12
	jne	.LBB0_136
.LBB0_142:                              # %cleanup_stack.exit
	movq	%r15, %rdi
	callq	free
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_144
# BB#143:
	movq	%r14, %rdi
	callq	cli_rmdirs
.LBB0_144:
	movq	%r14, %rdi
	callq	free
	movl	$-114, %ebx
	jmp	.LBB0_159
.LBB0_145:
	cmpq	$0, 96(%rsp)
	je	.LBB0_148
# BB#146:
	movq	88(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_148
# BB#147:
	movq	%rsp, %rdi
	movq	%rbp, %rsi
	callq	*%rax
.LBB0_148:
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	tableDestroy
	testq	%rbx, %rbx
	movq	128(%rsp), %r14         # 8-byte Reload
	je	.LBB0_156
# BB#149:                               # %.lr.ph250.preheader
	movq	%rsp, %r15
.LBB0_150:                              # %.lr.ph250
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_152
# BB#151:                               #   in Loop: Header=BB0_150 Depth=1
	decq	%rax
	movl	32(%rsp), %ecx
	movq	base_state+96(%rip), %rdx
	movq	%rdx, 96(%rsp)
	movups	base_state+80(%rip), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	base_state+64(%rip), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	base_state+48(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	base_state+32(%rip), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	base_state+16(%rip), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	base_state(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movq	%rax, (%rsp)
	movl	%ecx, 32(%rsp)
	cmpq	$0, 96(%rsp)
	jne	.LBB0_153
	jmp	.LBB0_155
.LBB0_152:                              #   in Loop: Header=BB0_150 Depth=1
	movq	%rbx, %rax
	decq	%rax
	movq	%rax, %rbx
	imulq	$104, %rax, %rax
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	96(%rdx,%rax), %rcx
	movq	%rcx, 96(%rsp)
	movups	80(%rdx,%rax), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	64(%rdx,%rax), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	(%rdx,%rax), %xmm0
	movups	16(%rdx,%rax), %xmm1
	movups	32(%rdx,%rax), %xmm2
	movups	48(%rdx,%rax), %xmm3
	movaps	%xmm3, 48(%rsp)
	movaps	%xmm2, 32(%rsp)
	movaps	%xmm1, 16(%rsp)
	movaps	%xmm0, (%rsp)
	cmpq	$0, 96(%rsp)
	je	.LBB0_155
.LBB0_153:                              #   in Loop: Header=BB0_150 Depth=1
	movq	88(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_155
# BB#154:                               #   in Loop: Header=BB0_150 Depth=1
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	*%rax
.LBB0_155:                              # %.backedge.i129.backedge
                                        #   in Loop: Header=BB0_150 Depth=1
	testq	%rbx, %rbx
	jne	.LBB0_150
.LBB0_156:                              # %cleanup_stack.exit131
	movq	136(%rsp), %rdi         # 8-byte Reload
	callq	free
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_158
# BB#157:
	movq	%r14, %rdi
	callq	cli_rmdirs
.LBB0_158:
	movq	%r14, %rdi
	callq	free
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	free
	movl	%r12d, %ebx
.LBB0_159:                              # %.thread
	movl	%ebx, %eax
	addq	$424, %rsp              # imm = 0x1A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	cli_scanrtf, .Lfunc_end0-cli_scanrtf
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_13
	.quad	.LBB0_24
	.quad	.LBB0_26
	.quad	.LBB0_28
	.quad	.LBB0_29
	.quad	.LBB0_34

	.text
	.p2align	4, 0x90
	.type	rtf_object_begin,@function
rtf_object_begin:                       # @rtf_object_begin
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	$64, %edi
	callq	cli_malloc
	testq	%rax, %rax
	je	.LBB1_1
# BB#2:
	movq	$0, 56(%rax)
	movl	$-1, %ecx
	movd	%ecx, %xmm0
	movdqu	%xmm0, 8(%rax)
	movq	%r15, 32(%rax)
	movq	%rbx, 40(%rax)
	movq	$0, (%rax)
	movq	$0, 24(%rax)
	movq	%rax, 96(%r14)
	xorl	%eax, %eax
	jmp	.LBB1_3
.LBB1_1:
	movl	$-114, %eax
.LBB1_3:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	rtf_object_begin, .Lfunc_end1-rtf_object_begin
	.cfi_endproc

	.p2align	4, 0x90
	.type	rtf_object_process,@function
rtf_object_process:                     # @rtf_object_process
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$8216, %rsp             # imm = 0x2018
.Lcfi25:
	.cfi_def_cfa_offset 8272
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r15
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	je	.LBB2_101
# BB#1:
	movq	96(%rdi), %r13
	testq	%r13, %r13
	je	.LBB2_101
# BB#2:
	cmpl	$0, 16(%r13)
	je	.LBB2_7
# BB#3:                                 # %.lr.ph298
	callq	__ctype_b_loc
	movq	(%rax), %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_4:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%r15,%rbp), %ecx
	testb	$16, 1(%rax,%rcx,2)
	jne	.LBB2_8
# BB#5:                                 #   in Loop: Header=BB2_4 Depth=1
	incq	%rbp
	cmpq	%rbx, %rbp
	jb	.LBB2_4
	jmp	.LBB2_100
.LBB2_7:
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	cmpq	%rbx, %rbp
	jb	.LBB2_9
	jmp	.LBB2_18
.LBB2_8:                                # %.critedge
	incq	%rbp
	movzwl	hextable(%rcx,%rcx), %eax
	orl	12(%r13), %eax
	movb	%al, 16(%rsp)
	movl	$0, 16(%r13)
	movl	$1, %r12d
	cmpq	%rbx, %rbp
	jae	.LBB2_18
.LBB2_9:                                # %.lr.ph295
	callq	__ctype_b_loc
	.p2align	4, 0x90
.LBB2_10:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_12 Depth 2
	movq	(%rax), %rdx
	movzbl	(%r15,%rbp), %ecx
	testb	$16, 1(%rdx,%rcx,2)
	je	.LBB2_16
# BB#11:                                #   in Loop: Header=BB2_10 Depth=1
	movzwl	hextable(%rcx,%rcx), %ecx
	shll	$4, %ecx
	incq	%rbp
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB2_12:                               #   Parent Loop BB2_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %rbp
	cmpq	%rbx, %rbp
	jae	.LBB2_14
# BB#13:                                #   in Loop: Header=BB2_12 Depth=2
	movzbl	(%r15,%rbp), %edi
	leaq	1(%rbp), %rsi
	testb	$16, 1(%rdx,%rdi,2)
	je	.LBB2_12
.LBB2_14:                               # %.critedge228
                                        #   in Loop: Header=BB2_10 Depth=1
	cmpq	%rbp, %rbx
	je	.LBB2_17
# BB#15:                                # %.thread
                                        #   in Loop: Header=BB2_10 Depth=1
	movzbl	(%r15,%rbp), %edx
	movzwl	hextable(%rdx,%rdx), %edx
	orl	%ecx, %edx
	movb	%dl, 16(%rsp,%r12)
	incq	%r12
.LBB2_16:                               #   in Loop: Header=BB2_10 Depth=1
	incq	%rbp
	cmpq	%rbx, %rbp
	jb	.LBB2_10
	jmp	.LBB2_18
.LBB2_17:
	andl	$240, %ecx
	movl	%ecx, 12(%r13)
	movl	$1, 16(%r13)
.LBB2_18:                               # %.preheader242
	testq	%r12, %r12
	je	.LBB2_100
# BB#19:                                # %.lr.ph281
	leaq	16(%rsp), %r15
	leaq	8(%r13), %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB2_20:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_42 Depth 2
                                        #     Child Loop BB2_23 Depth 2
	movl	20(%r13), %eax
	cmpq	$5, %rax
	ja	.LBB2_101
# BB#21:                                #   in Loop: Header=BB2_20 Depth=1
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_22:                               # %.lr.ph271.preheader
                                        #   in Loop: Header=BB2_20 Depth=1
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	56(%r13), %rcx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_23:                               # %.lr.ph271
                                        #   Parent Loop BB2_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$7, %rcx
	ja	.LBB2_27
# BB#24:                                #   in Loop: Header=BB2_23 Depth=2
	movzbl	(%r15,%rbp), %edx
	cmpb	%dl, rtf_data_magic(%rcx)
	je	.LBB2_26
# BB#25:                                #   in Loop: Header=BB2_23 Depth=2
	movzbl	rtf_data_magic(%rbp), %esi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	56(%r13), %rcx
.LBB2_26:                               #   in Loop: Header=BB2_23 Depth=2
	incq	%rbp
	incq	%rcx
	movq	%rcx, 56(%r13)
	cmpq	%rbp, %r12
	ja	.LBB2_23
.LBB2_27:                               # %.critedge4
                                        #   in Loop: Header=BB2_20 Depth=1
	subq	%rbp, %r12
	cmpq	$8, %rcx
	jne	.LBB2_98
# BB#28:                                #   in Loop: Header=BB2_20 Depth=1
	addq	%rbp, %r15
	movq	$0, 56(%r13)
	movl	$1, 20(%r13)
	testq	%r12, %r12
	jne	.LBB2_99
	jmp	.LBB2_101
.LBB2_29:                               #   in Loop: Header=BB2_20 Depth=1
	movq	56(%r13), %rax
	testq	%rax, %rax
	jne	.LBB2_31
# BB#30:                                #   in Loop: Header=BB2_20 Depth=1
	movq	$0, 48(%r13)
.LBB2_31:                               # %.preheader
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpq	$3, %rax
	ja	.LBB2_68
# BB#32:                                # %.lr.ph267
                                        #   in Loop: Header=BB2_20 Depth=1
	movzbl	(%r15), %edx
	movl	%eax, %ecx
	shlb	$3, %cl
	shlq	%cl, %rdx
	orq	48(%r13), %rdx
	movq	%rdx, 48(%r13)
	leaq	1(%rax), %rcx
	movq	%rcx, 56(%r13)
	cmpq	$2, %r12
	jb	.LBB2_79
# BB#33:                                # %.lr.ph267
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpq	$4, %rcx
	jae	.LBB2_79
# BB#34:                                # %.lr.ph267.1
                                        #   in Loop: Header=BB2_20 Depth=1
	movzbl	1(%r15), %esi
	shlb	$3, %cl
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rsi
	orq	%rdx, %rsi
	movq	%rsi, 48(%r13)
	leaq	2(%rax), %rcx
	movq	%rcx, 56(%r13)
	cmpq	$3, %r12
	jb	.LBB2_81
# BB#35:                                # %.lr.ph267.1
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpq	$3, %rcx
	ja	.LBB2_81
# BB#36:                                # %.lr.ph267.2
                                        #   in Loop: Header=BB2_20 Depth=1
	movzbl	2(%r15), %edx
	shlb	$3, %cl
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rdx
	orq	%rsi, %rdx
	movq	%rdx, 48(%r13)
	leaq	3(%rax), %rcx
	movq	%rcx, 56(%r13)
	cmpq	$4, %r12
	jb	.LBB2_83
# BB#37:                                # %.lr.ph267.2
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpq	$3, %rcx
	ja	.LBB2_83
# BB#38:                                # %.lr.ph267.3
                                        #   in Loop: Header=BB2_20 Depth=1
	movzbl	3(%r15), %esi
	shlb	$3, %cl
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rsi
	orq	%rdx, %rsi
	movq	%rsi, 48(%r13)
	leaq	4(%rax), %rcx
	movq	%rcx, 56(%r13)
	cmpq	$5, %r12
	jb	.LBB2_85
# BB#39:                                # %.lr.ph267.3
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpq	$-4, %rax
	jb	.LBB2_85
# BB#40:                                # %.lr.ph267.4
                                        #   in Loop: Header=BB2_20 Depth=1
	movzbl	4(%r15), %edx
	shlb	$3, %cl
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rdx
	orq	%rsi, %rdx
	movq	%rdx, 48(%r13)
	addq	$5, %rax
	movq	%rax, 56(%r13)
	movl	$5, %ebp
	jmp	.LBB2_87
.LBB2_41:                               # %.lr.ph261.preheader
                                        #   in Loop: Header=BB2_20 Depth=1
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	56(%r13), %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_42:                               # %.lr.ph261
                                        #   Parent Loop BB2_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$63, %rcx
	ja	.LBB2_45
# BB#43:                                # %.lr.ph261
                                        #   in Loop: Header=BB2_42 Depth=2
	cmpq	48(%r13), %rcx
	jae	.LBB2_45
# BB#44:                                #   in Loop: Header=BB2_42 Depth=2
	movzbl	(%r15,%rax), %edx
	movq	24(%r13), %rsi
	movb	%dl, (%rsi,%rcx)
	incq	%rax
	movq	56(%r13), %rcx
	incq	%rcx
	movq	%rcx, 56(%r13)
	cmpq	%rax, %r12
	ja	.LBB2_42
.LBB2_45:                               # %.critedge8
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpq	$63, %rcx
	ja	.LBB2_47
# BB#46:                                # %.critedge8
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpq	48(%r13), %rcx
	jb	.LBB2_102
.LBB2_47:                               #   in Loop: Header=BB2_20 Depth=1
	subq	%rax, %r12
	movq	24(%r13), %rdx
	movb	$0, (%rdx,%rcx)
	movq	48(%r13), %rcx
	movq	56(%r13), %rsi
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	movq	%r12, %rbp
	subq	%rdx, %rbp
	jb	.LBB2_104
# BB#48:                                #   in Loop: Header=BB2_20 Depth=1
	addq	%rax, %r15
	cmpq	%rsi, %rcx
	ja	.LBB2_49
# BB#72:                                #   in Loop: Header=BB2_20 Depth=1
	addq	%rdx, %r15
	movq	$0, 56(%r13)
	movq	24(%r13), %rsi
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	24(%r13), %rdi
	callq	free
	movq	$0, 24(%r13)
	movl	$3, 20(%r13)
.LBB2_49:                               #   in Loop: Header=BB2_20 Depth=1
	movq	%rbp, %r12
	testq	%r12, %r12
	jne	.LBB2_99
	jmp	.LBB2_101
.LBB2_50:                               #   in Loop: Header=BB2_20 Depth=1
	movq	56(%r13), %rax
	movl	$8, %ecx
	subq	%rax, %rcx
	subq	%rcx, %r12
	jae	.LBB2_69
# BB#51:                                #   in Loop: Header=BB2_20 Depth=1
	xorl	%r12d, %r12d
	cmpq	$8, %rax
	je	.LBB2_70
	jmp	.LBB2_100
.LBB2_52:                               #   in Loop: Header=BB2_20 Depth=1
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	56(%r13), %rax
	testq	%rax, %rax
	jne	.LBB2_54
# BB#53:                                #   in Loop: Header=BB2_20 Depth=1
	movq	$0, 48(%r13)
.LBB2_54:                               # %.preheader241
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpq	$3, %rax
	ja	.LBB2_71
# BB#55:                                # %.lr.ph
                                        #   in Loop: Header=BB2_20 Depth=1
	movzbl	(%r15), %edx
	movl	%eax, %ecx
	shlb	$3, %cl
	shlq	%cl, %rdx
	orq	48(%r13), %rdx
	movq	%rdx, 48(%r13)
	leaq	1(%rax), %rcx
	movq	%rcx, 56(%r13)
	cmpq	$2, %r12
	jb	.LBB2_80
# BB#56:                                # %.lr.ph
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpq	$4, %rcx
	jae	.LBB2_80
# BB#57:                                # %.lr.ph.1
                                        #   in Loop: Header=BB2_20 Depth=1
	movzbl	1(%r15), %esi
	shlb	$3, %cl
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rsi
	orq	%rdx, %rsi
	movq	%rsi, 48(%r13)
	leaq	2(%rax), %rcx
	movq	%rcx, 56(%r13)
	cmpq	$3, %r12
	jb	.LBB2_82
# BB#58:                                # %.lr.ph.1
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpq	$3, %rcx
	ja	.LBB2_82
# BB#59:                                # %.lr.ph.2
                                        #   in Loop: Header=BB2_20 Depth=1
	movzbl	2(%r15), %edx
	shlb	$3, %cl
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rdx
	orq	%rsi, %rdx
	movq	%rdx, 48(%r13)
	leaq	3(%rax), %rcx
	movq	%rcx, 56(%r13)
	cmpq	$4, %r12
	jb	.LBB2_84
# BB#60:                                # %.lr.ph.2
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpq	$3, %rcx
	ja	.LBB2_84
# BB#61:                                # %.lr.ph.3
                                        #   in Loop: Header=BB2_20 Depth=1
	movzbl	3(%r15), %esi
	shlb	$3, %cl
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rsi
	orq	%rdx, %rsi
	movq	%rsi, 48(%r13)
	leaq	4(%rax), %rcx
	movq	%rcx, 56(%r13)
	cmpq	$5, %r12
	jb	.LBB2_93
# BB#62:                                # %.lr.ph.3
                                        #   in Loop: Header=BB2_20 Depth=1
	cmpq	$-4, %rax
	jb	.LBB2_93
# BB#63:                                # %.lr.ph.4
                                        #   in Loop: Header=BB2_20 Depth=1
	movzbl	4(%r15), %edx
	shlb	$3, %cl
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rdx
	orq	%rsi, %rdx
	movq	%rdx, 48(%r13)
	addq	$5, %rax
	movq	%rax, 56(%r13)
	movl	$5, %ebp
	jmp	.LBB2_95
.LBB2_64:                               #   in Loop: Header=BB2_20 Depth=1
	movq	48(%r13), %rax
	cmpq	%rax, %r12
	movq	%rax, %rbp
	cmovbq	%r12, %rbp
	cmpq	$0, 56(%r13)
	jne	.LBB2_75
# BB#65:                                #   in Loop: Header=BB2_20 Depth=1
	cmpb	$-48, (%r15)
	jne	.LBB2_73
# BB#66:                                #   in Loop: Header=BB2_20 Depth=1
	cmpb	$-49, 1(%r15)
	jne	.LBB2_73
# BB#67:                                #   in Loop: Header=BB2_20 Depth=1
	movq	$2, 56(%r13)
	jmp	.LBB2_75
.LBB2_68:                               #   in Loop: Header=BB2_20 Depth=1
	xorl	%ebp, %ebp
	jmp	.LBB2_87
.LBB2_69:                               # %.thread232
                                        #   in Loop: Header=BB2_20 Depth=1
	movq	$8, 56(%r13)
.LBB2_70:                               #   in Loop: Header=BB2_20 Depth=1
	addq	$8, %r15
	movq	$0, 56(%r13)
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$4, 20(%r13)
	testq	%r12, %r12
	jne	.LBB2_99
	jmp	.LBB2_101
.LBB2_71:                               #   in Loop: Header=BB2_20 Depth=1
	xorl	%ebp, %ebp
	jmp	.LBB2_95
.LBB2_73:                               #   in Loop: Header=BB2_20 Depth=1
	movq	$1, 56(%r13)
	movl	%eax, 12(%rsp)
	movl	8(%r13), %edi
	movl	$4, %edx
	leaq	12(%rsp), %rsi
	callq	cli_writen
	cmpl	$4, %eax
	jne	.LBB2_109
# BB#74:                                # %._crit_edge
                                        #   in Loop: Header=BB2_20 Depth=1
	movq	48(%r13), %rax
.LBB2_75:                               #   in Loop: Header=BB2_20 Depth=1
	subq	%rbp, %rax
	movq	%rax, 48(%r13)
	movl	8(%r13), %edi
	movq	%r15, %rsi
	movl	%ebp, %edx
	callq	cli_writen
	cltq
	cmpq	%rbp, %rax
	jne	.LBB2_103
# BB#76:                                #   in Loop: Header=BB2_20 Depth=1
	addq	%rbp, %r15
	subq	%rbp, %r12
	cmpq	$0, 48(%r13)
	jne	.LBB2_98
# BB#77:                                #   in Loop: Header=BB2_20 Depth=1
	movq	40(%r13), %rsi
	movq	%r13, %rdi
	callq	decode_and_scan
	testl	%eax, %eax
	jne	.LBB2_106
# BB#78:                                # %.thread235
                                        #   in Loop: Header=BB2_20 Depth=1
	movq	$0, 56(%r13)
	movl	$0, 20(%r13)
	testq	%r12, %r12
	jne	.LBB2_99
	jmp	.LBB2_101
.LBB2_79:                               #   in Loop: Header=BB2_20 Depth=1
	movl	$1, %ebp
	jmp	.LBB2_86
.LBB2_80:                               #   in Loop: Header=BB2_20 Depth=1
	movl	$1, %ebp
	jmp	.LBB2_94
.LBB2_81:                               #   in Loop: Header=BB2_20 Depth=1
	movl	$2, %ebp
	jmp	.LBB2_86
.LBB2_82:                               #   in Loop: Header=BB2_20 Depth=1
	movl	$2, %ebp
	jmp	.LBB2_94
.LBB2_83:                               #   in Loop: Header=BB2_20 Depth=1
	movl	$3, %ebp
	jmp	.LBB2_86
.LBB2_84:                               #   in Loop: Header=BB2_20 Depth=1
	movl	$3, %ebp
	jmp	.LBB2_94
.LBB2_85:                               #   in Loop: Header=BB2_20 Depth=1
	movl	$4, %ebp
.LBB2_86:                               # %.critedge5
                                        #   in Loop: Header=BB2_20 Depth=1
	movq	%rcx, %rax
.LBB2_87:                               # %.critedge5
                                        #   in Loop: Header=BB2_20 Depth=1
	subq	%rbp, %r12
	cmpq	$4, %rax
	jne	.LBB2_98
# BB#88:                                #   in Loop: Header=BB2_20 Depth=1
	movq	$0, 56(%r13)
	movq	48(%r13), %rcx
	cmpq	$65, %rcx
	jb	.LBB2_90
# BB#89:                                #   in Loop: Header=BB2_20 Depth=1
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_dbgmsg
	movl	$65, %ecx
	jmp	.LBB2_91
.LBB2_90:                               #   in Loop: Header=BB2_20 Depth=1
	incq	%rcx
.LBB2_91:                               #   in Loop: Header=BB2_20 Depth=1
	movq	%rcx, %rdi
	callq	cli_malloc
	movq	%rax, 24(%r13)
	testq	%rax, %rax
	je	.LBB2_108
# BB#92:                                #   in Loop: Header=BB2_20 Depth=1
	addq	%rbp, %r15
	movl	$2, 20(%r13)
	movq	48(%r13), %rsi
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testq	%r12, %r12
	jne	.LBB2_99
	jmp	.LBB2_101
.LBB2_93:                               #   in Loop: Header=BB2_20 Depth=1
	movl	$4, %ebp
.LBB2_94:                               # %.critedge10
                                        #   in Loop: Header=BB2_20 Depth=1
	movq	%rcx, %rax
.LBB2_95:                               # %.critedge10
                                        #   in Loop: Header=BB2_20 Depth=1
	subq	%rbp, %r12
	cmpq	$4, %rax
	jne	.LBB2_98
# BB#96:                                #   in Loop: Header=BB2_20 Depth=1
	movq	$0, 56(%r13)
	movq	48(%r13), %rsi
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	32(%r13), %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	callq	cli_gentempfd
	testl	%eax, %eax
	jne	.LBB2_106
# BB#97:                                #   in Loop: Header=BB2_20 Depth=1
	addq	%rbp, %r15
	movl	$5, 20(%r13)
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	.p2align	4, 0x90
.LBB2_98:                               # %.backedge
                                        #   in Loop: Header=BB2_20 Depth=1
	testq	%r12, %r12
	je	.LBB2_101
.LBB2_99:                               # %.backedge
                                        #   in Loop: Header=BB2_20 Depth=1
	testq	%r15, %r15
	jne	.LBB2_20
	jmp	.LBB2_101
.LBB2_100:
	xorl	%r14d, %r14d
.LBB2_101:                              # %.critedge227
	movl	%r14d, %eax
	addq	$8216, %rsp             # imm = 0x2018
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_102:
	xorl	%r14d, %r14d
	movl	$.L.str.14, %edi
	jmp	.LBB2_105
.LBB2_103:
	movl	$-123, %r14d
	jmp	.LBB2_101
.LBB2_104:
	subq	%r12, %rcx
	movq	%rcx, 48(%r13)
	xorl	%r14d, %r14d
	movl	$.L.str.15, %edi
.LBB2_105:                              # %.critedge227
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB2_101
.LBB2_106:
	movl	%eax, %r14d
	jmp	.LBB2_101
.LBB2_108:
	movl	$-114, %r14d
	jmp	.LBB2_101
.LBB2_109:
	movl	$-123, %r14d
	jmp	.LBB2_101
.Lfunc_end2:
	.size	rtf_object_process, .Lfunc_end2-rtf_object_process
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_22
	.quad	.LBB2_29
	.quad	.LBB2_41
	.quad	.LBB2_50
	.quad	.LBB2_52
	.quad	.LBB2_64

	.text
	.p2align	4, 0x90
	.type	rtf_object_end,@function
rtf_object_end:                         # @rtf_object_end
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	96(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB3_1
# BB#2:
	cmpl	$0, 8(%rbx)
	jle	.LBB3_3
# BB#4:
	movq	%rbx, %rdi
	callq	decode_and_scan
	movl	%eax, %ebp
	jmp	.LBB3_5
.LBB3_1:
	xorl	%ebp, %ebp
	jmp	.LBB3_10
.LBB3_3:
	xorl	%ebp, %ebp
.LBB3_5:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_7
# BB#6:
	callq	free
.LBB3_7:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_9
# BB#8:
	callq	free
.LBB3_9:
	movq	%rbx, %rdi
	callq	free
	movq	$0, 96(%r14)
.LBB3_10:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	rtf_object_end, .Lfunc_end3-rtf_object_end
	.cfi_endproc

	.p2align	4, 0x90
	.type	decode_and_scan,@function
decode_and_scan:                        # @decode_and_scan
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 48
.Lcfi43:
	.cfi_offset %rbx, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	(%rbx), %rsi
	xorl	%r14d, %r14d
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	8(%rbx), %edi
	cmpq	$1, 56(%rbx)
	jne	.LBB4_4
# BB#1:
	testl	%edi, %edi
	jle	.LBB4_6
# BB#2:
	xorl	%r14d, %r14d
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	8(%rbx), %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	lseek
	movl	8(%rbx), %edi
	movq	32(%rbx), %rsi
	callq	cli_decode_ole_object
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB4_6
# BB#3:
	movl	%ebp, %edi
	movq	%r15, %rsi
	callq	cli_magic_scandesc
	movl	%eax, %r14d
	movl	%ebp, %edi
	callq	close
	jmp	.LBB4_6
.LBB4_4:
	testl	%edi, %edi
	jle	.LBB4_6
# BB#5:
	movq	%r15, %rsi
	callq	cli_magic_scandesc
	movl	%eax, %r14d
.LBB4_6:                                # %.thread
	movl	8(%rbx), %edi
	testl	%edi, %edi
	jle	.LBB4_8
# BB#7:
	callq	close
.LBB4_8:
	movl	$-1, 8(%rbx)
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_12
# BB#9:
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB4_11
# BB#10:
	callq	unlink
	movq	(%rbx), %rdi
.LBB4_11:
	callq	free
	movq	$0, (%rbx)
.LBB4_12:
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	decode_and_scan, .Lfunc_end4-decode_and_scan
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"in cli_scanrtf()\n"
	.size	.L.str, 18

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"ScanRTF -> Can't create temporary directory %s\n"
	.size	.L.str.1, 48

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"RTF: Unable to load rtf action table\n"
	.size	.L.str.2, 38

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"RTF:Push failure!\n"
	.size	.L.str.3, 19

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Invalid control word: maximum size exceeded:%s\n"
	.size	.L.str.5, 48

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"object"
	.size	.L.str.6, 7

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"objdata "
	.size	.L.str.7, 9

	.type	base_state,@object      # @base_state
	.section	.rodata,"a",@progbits
	.p2align	3
base_state:
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.quad	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.asciz	"                              \000\000"
	.zero	3
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.size	base_state, 104

	.type	.L.str.8,@object        # @.str.8
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.8:
	.asciz	"Warning: attempt to pop from empty stack!\n"
	.size	.L.str.8, 43

	.type	hextable,@object        # @hextable
	.section	.rodata,"a",@progbits
	.p2align	4
hextable:
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	10                      # 0xa
	.short	11                      # 0xb
	.short	12                      # 0xc
	.short	13                      # 0xd
	.short	14                      # 0xe
	.short	15                      # 0xf
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	10                      # 0xa
	.short	11                      # 0xb
	.short	12                      # 0xc
	.short	13                      # 0xd
	.short	14                      # 0xe
	.short	15                      # 0xf
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.size	hextable, 512

	.type	.L.str.9,@object        # @.str.9
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.9:
	.asciz	"RTF: waiting for magic\n"
	.size	.L.str.9, 24

	.type	rtf_data_magic,@object  # @rtf_data_magic
	.section	.rodata.cst8,"aM",@progbits,8
rtf_data_magic:
	.asciz	"\001\005\000\000\002\000\000"
	.size	rtf_data_magic, 8

	.type	.L.str.10,@object       # @.str.10
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.10:
	.asciz	"Warning: rtf objdata magic number not matched, expected:%d, got: %d, at pos:%lu\n"
	.size	.L.str.10, 81

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Description length too big (%lu), showing only 64 bytes of it\n"
	.size	.L.str.11, 63

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"RTF: description length:%lu\n"
	.size	.L.str.12, 29

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"RTF: in WAIT_DESC\n"
	.size	.L.str.13, 19

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"RTF: waiting for more data(1)\n"
	.size	.L.str.14, 31

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"RTF: waiting for more data(2)\n"
	.size	.L.str.15, 31

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Preparing to dump rtf embedded object, description:%s\n"
	.size	.L.str.16, 55

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"RTF: next state: wait_data_size\n"
	.size	.L.str.17, 33

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"RTF: in WAIT_DATA_SIZE\n"
	.size	.L.str.18, 24

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Dumping rtf embedded object of size:%lu\n"
	.size	.L.str.19, 41

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"RTF: next state: DUMP_DATA\n"
	.size	.L.str.20, 28

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"RTF:Scanning embedded object:%s\n"
	.size	.L.str.21, 33

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Decoding ole object\n"
	.size	.L.str.22, 21


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
