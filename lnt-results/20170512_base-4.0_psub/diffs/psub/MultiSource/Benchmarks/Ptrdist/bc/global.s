	.text
	.file	"global.bc"
	.type	libmath,@object         # @libmath
	.section	.rodata,"a",@progbits
	.globl	libmath
	.p2align	4
libmath:
	.asciz	"@iK20:s2:p@r@iF1,4.5,6,7,8,9,10,11,12[l4:0<Z0:1s10:pl4:ns4:pN0:l2:s12:pK4:l12:+K.44:l4:*+s2:pN1:l4:1>Z2:l8:1+s8:pl4:K2:/s4:pJ1:N2:1l4:+s11:pl4:s5:p1s6:pK2:s9:pN4:1B5:J3:N6:l9:i9:pJ4:N5:l5:l4:*s5:l6:l9:*s6:/s7:pl7:0=Z7:l8:0>Z8:N9:l8:d8:Z10:l11:l11:*s11:pJ9:N10:N8:l12:s2:pl10:Z11:1l11:/RN11:l11:1/RN7:l11:l7:+s11:pJ6:N3:0R]@r@iF2,4.7,8,9,10,13,11,12[l4:0{Z0:1K10:l2:^-RN0:l2:s12:pl2:K4:+s2:pK2:s8:p0s9:pN1:l4:K2:}Z2:l8:K2:*s8:pl4:cRs4:pJ1:N2:N3:l4:K.5:{Z4:l8:K2:*s8:pl4:cRs4:pJ3:N4:l4:1-l4:1+/s13:s11:pl13:l13:*s10:pK3:s9:pN6:1B7:J5:N8:l9:K2:+s9:pJ6:N7:l13:l10:*s13:l9:/s7:pl7:0=Z9:l8:l11:*s11:pl12:s2:pl11:1/RN9:l11:l7:+s11:pJ8:N5:0R]@r@iF3,4.7,9,10,13,14,11,12[l2:s12:pK1.1:l12:*1+s2:p1C4,0:s11:pl4:0<Z0:1s10:pl4:ns4:pN0:0s2:pl4:l11:/K2:+K4:/s13:pl4:K4:l13:*l11:*-s4:pl13:K2:%Z1:l4:ns4:pN1:l12:K2:+s2:pl4:s7:s11:pl4:nl4:*s14:pK3:s9:pN3:1B4:J2:N5:l9:K2:+s9:pJ3:N4:l7:l14:l9:l9:1-*/*s7:pl7:0=Z6:l12:s2:pl10:Z7:l11:n1/RN7:l11:1/RN6:l11:l7:+s11:pJ5:N2:0R]@r@iF5,4.11[l2:1+s2:pl4:1C4,0:K2:*+C3,0:s11:pl2:1-s2:pl11:1/R0R]@r@iF4,4.5,7,8,9,10,13,14,11,12[l4:1=Z0:l2:K25:{Z1:K.7853981633974483096156608:1/RN1:l2:K40:{Z2:K.7853981633974483096156608458198757210492:1/RN2:l2:K60:{Z3:K.785398163397448309615660845819875721049292349843776455243736:1/RN3:N0:l4:K.2:=Z4:l2:K25:{Z5:K.1973955598498807583700497:1/RN5:l2:K40:{Z6:K.1973955598498807583700497651947902934475:1/RN6:l2:K60:{Z7:K.197395559849880758370049765194790293447585103787852101517688:1/RN7:N4:l4:0<Z8:1s10:pl4:ns4:pN8:l2:s12:pl4:K.2:>Z9:l12:K4:+s2:pK.2:C4,0:s5:pN9:l12:K2:+s2:pN10:l4:K.2:>Z11:l8:1+s8:pl4:K.2:-1l4:K.2:*+/s4:pJ10:N11:l4:s13:s11:pl4:nl4:*s14:pK3:s9:pN13:1B14:J12:N15:l9:K2:+s9:pJ13:N14:l13:l14:*s13:l9:/s7:pl7:0=Z16:l12:s2:pl10:Z17:l8:l5:*l11:+1n/RN17:l8:l5:*l11:+1/RN16:l11:l7:+s11:pJ15:N12:0R]@r@iF6,13,4.5,6,7,8,9,10,14,11,12[l2:s12:p0s2:pl13:1/s13:pl13:0<Z0:l13:ns13:pl13:K2:%1=Z1:1s10:pN1:N0:1s8:pK2:s9:pN3:l9:l13:{B4:J2:N5:l9:i9:pJ3:N4:l8:l9:*s8:pJ5:N2:K1.5:l12:*s2:pl4:l13:^K2:l13:^/l8:/s8:p1s7:s11:pl4:nl4:*K4:/s14:pK1.5:l12:*s2:p1s9:pN7:1B8:J6:N9:l9:i9:pJ7:N8:l7:l14:*l9:/l13:l9:+/s7:pl7:0=Z10:l12:s2:pl10:Z11:l8:nl11:*1/RN11:l8:l11:*1/RN10:l11:l7:+s11:pJ9:N6:0R]@r"
	.size	libmath, 2140

	.type	break_label,@object     # @break_label
	.comm	break_label,4,4
	.type	if_label,@object        # @if_label
	.comm	if_label,4,4
	.type	continue_label,@object  # @continue_label
	.comm	continue_label,4,4
	.type	next_label,@object      # @next_label
	.comm	next_label,4,4
	.type	genstr,@object          # @genstr
	.comm	genstr,80,16
	.type	out_count,@object       # @out_count
	.comm	out_count,4,4
	.type	did_gen,@object         # @did_gen
	.comm	did_gen,1,1
	.type	interactive,@object     # @interactive
	.comm	interactive,1,1
	.type	compile_only,@object    # @compile_only
	.comm	compile_only,1,1
	.type	use_math,@object        # @use_math
	.comm	use_math,1,1
	.type	warn_not_std,@object    # @warn_not_std
	.comm	warn_not_std,1,1
	.type	std_only,@object        # @std_only
	.comm	std_only,1,1
	.type	functions,@object       # @functions
	.comm	functions,8,8
	.type	f_names,@object         # @f_names
	.comm	f_names,8,8
	.type	f_count,@object         # @f_count
	.comm	f_count,4,4
	.type	variables,@object       # @variables
	.comm	variables,8,8
	.type	v_names,@object         # @v_names
	.comm	v_names,8,8
	.type	v_count,@object         # @v_count
	.comm	v_count,4,4
	.type	arrays,@object          # @arrays
	.comm	arrays,8,8
	.type	a_names,@object         # @a_names
	.comm	a_names,8,8
	.type	a_count,@object         # @a_count
	.comm	a_count,4,4
	.type	ex_stack,@object        # @ex_stack
	.comm	ex_stack,8,8
	.type	fn_stack,@object        # @fn_stack
	.comm	fn_stack,8,8
	.type	i_base,@object          # @i_base
	.comm	i_base,4,4
	.type	o_base,@object          # @o_base
	.comm	o_base,4,4
	.type	scale,@object           # @scale
	.comm	scale,4,4
	.type	c_code,@object          # @c_code
	.comm	c_code,1,1
	.type	out_col,@object         # @out_col
	.comm	out_col,4,4
	.type	runtime_error,@object   # @runtime_error
	.comm	runtime_error,1,1
	.type	pc,@object              # @pc
	.comm	pc,8,4
	.type	line_no,@object         # @line_no
	.comm	line_no,4,4
	.type	had_error,@object       # @had_error
	.comm	had_error,4,4
	.type	next_array,@object      # @next_array
	.comm	next_array,4,4
	.type	next_func,@object       # @next_func
	.comm	next_func,4,4
	.type	next_var,@object        # @next_var
	.comm	next_var,4,4
	.type	name_tree,@object       # @name_tree
	.comm	name_tree,8,8
	.type	g_argv,@object          # @g_argv
	.comm	g_argv,8,8
	.type	g_argc,@object          # @g_argc
	.comm	g_argc,4,4
	.type	is_std_in,@object       # @is_std_in
	.comm	is_std_in,1,1

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
