	.text
	.file	"loopFilter.bc"
	.globl	DeblockPicture
	.p2align	4, 0x90
	.type	DeblockPicture,@function
DeblockPicture:                         # @DeblockPicture
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	cmpl	$0, 316912(%rbx)
	je	.LBB0_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movl	%ebp, %edx
	callq	DeblockMb
	incl	%ebp
	cmpl	316912(%rbx), %ebp
	jb	.LBB0_2
.LBB0_3:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	DeblockPicture, .Lfunc_end0-DeblockPicture
	.cfi_endproc

	.globl	DeblockMb
	.p2align	4, 0x90
	.type	DeblockMb,@function
DeblockMb:                              # @DeblockMb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 176
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %rbp
	movaps	.LDeblockMb.filterNon8x8LumaEdgesFlag(%rip), %xmm0
	movaps	%xmm0, 96(%rsp)
	movq	316920(%r14), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	316928(%r14), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	$1, 5628(%rbp)
	leaq	84(%rsp), %rsi
	leaq	80(%rsp), %rdx
	xorl	%ecx, %ecx
	movl	%ebx, %edi
	callq	get_mb_pos
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	cmpl	$0, 84(%rsp)
	setne	%sil
	movl	80(%rsp), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	setne	%al
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	5600(%rbp), %rbp
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	movslq	%ebx, %rcx
	imulq	$408, %rcx, %rbx        # imm = 0x198
	xorl	%ecx, %ecx
	cmpl	$0, 396(%rbp,%rbx)
	sete	%cl
	movl	%ecx, 108(%rsp)
	movl	%ecx, 100(%rsp)
	movl	316904(%r14), %ecx
	testl	%ecx, %ecx
	je	.LBB1_3
# BB#1:
	cmpl	$16, %edx
	jne	.LBB1_3
# BB#2:
	testl	%edx, %edx
	setne	%dl
	cmpl	$0, 356(%rbp,%rbx)
	sete	%al
	andb	%dl, %al
	movzbl	%al, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
.LBB1_3:
	cmpl	$0, (%r14)
	je	.LBB1_4
.LBB1_6:                                # %.thread
	movl	$2, 20(%rsp)            # 4-byte Folded Spill
.LBB1_7:                                # %.thread165
	movl	340(%rbp,%rbx), %edx
	cmpl	$1, %edx
	je	.LBB1_40
# BB#8:                                 # %.thread165
	cmpl	$2, %edx
	jne	.LBB1_9
# BB#10:
	movl	380(%rbp,%rbx), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	testl	%ecx, %ecx
	je	.LBB1_13
# BB#11:
	testb	$1, 8(%rsp)             # 1-byte Folded Reload
	je	.LBB1_13
# BB#12:
	movl	356(%rbp,%rbx), %eax
	movl	$1, 16(%rsp)            # 4-byte Folded Spill
	testl	%eax, %eax
	je	.LBB1_14
.LBB1_13:
	movl	384(%rbp,%rbx), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	jmp	.LBB1_14
.LBB1_4:
	movl	$4, 20(%rsp)            # 4-byte Folded Spill
	testl	%ecx, %ecx
	je	.LBB1_7
# BB#5:
	cmpl	$0, 356(%rbp,%rbx)
	jne	.LBB1_6
	jmp	.LBB1_7
.LBB1_9:
	movl	12(%rsp), %eax          # 4-byte Reload
	movb	%sil, %al
	movl	%eax, 12(%rsp)          # 4-byte Spill
.LBB1_14:
	movl	8(%rsp), %eax           # 4-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 4(%rcx)
	callq	CheckAvailabilityOfNeighbors
	leaq	344(%rbp,%rbx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	movq	%r14, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_15:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_19 Depth 2
                                        #       Child Loop BB1_22 Depth 3
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB1_17
# BB#16:                                #   in Loop: Header=BB1_15 Depth=1
	movb	$1, %r15b
	testq	%rbp, %rbp
	jne	.LBB1_18
.LBB1_17:                               #   in Loop: Header=BB1_15 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	setne	%al
	testq	%rbp, %rbp
	sete	%r15b
	andb	%al, %r15b
.LBB1_18:                               #   in Loop: Header=BB1_15 Depth=1
	xorl	%r12d, %r12d
	movq	40(%rsp), %r13          # 8-byte Reload
	movb	%r15b, 7(%rsp)          # 1-byte Spill
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_19:                               #   Parent Loop BB1_15 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_22 Depth 3
	testq	%r12, %r12
	setne	%al
	orb	%r15b, %al
	cmpb	$1, %al
	jne	.LBB1_38
# BB#20:                                #   in Loop: Header=BB1_19 Depth=2
	movslq	317044(%r14), %rax
	movq	%rbp, %rcx
	shlq	$4, %rcx
	leaq	(%rcx,%r12,4), %rcx
	movsbl	chroma_edge(%rax,%rcx), %ebx
	subq	$8, %rsp
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	leaq	72(%rsp), %rdi
	movq	%r13, %rsi
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	%ebp, %ecx
	movl	%r12d, %r8d
	movl	28(%rsp), %r9d          # 4-byte Reload
	pushq	%r14
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	callq	GetStrength
	addq	$16, %rsp
.Lcfi21:
	.cfi_adjust_cfa_offset -16
	cmpb	$0, 64(%rsp)
	jne	.LBB1_25
# BB#21:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_19 Depth=2
	movl	$1, %eax
	.p2align	4, 0x90
.LBB1_22:                               # %.lr.ph
                                        #   Parent Loop BB1_15 Depth=1
                                        #     Parent Loop BB1_19 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	64(%rsp,%rax), %ecx
	cmpq	$14, %rax
	jg	.LBB1_24
# BB#23:                                # %.lr.ph
                                        #   in Loop: Header=BB1_22 Depth=3
	incq	%rax
	testb	%cl, %cl
	je	.LBB1_22
.LBB1_24:                               # %._crit_edge
                                        #   in Loop: Header=BB1_19 Depth=2
	testb	%cl, %cl
	je	.LBB1_30
.LBB1_25:                               # %._crit_edge.thread
                                        #   in Loop: Header=BB1_19 Depth=2
	cmpl	$0, 96(%rsp,%r12,4)
	je	.LBB1_27
# BB#26:                                #   in Loop: Header=BB1_19 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r8d
	movl	4(%rax), %r9d
	movl	316864(%r14), %eax
	movq	88(%rsp), %rdi          # 8-byte Reload
	leaq	64(%rsp), %rsi
	movq	%r13, %rdx
	movl	8(%rsp), %ecx           # 4-byte Reload
	pushq	%r14
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	callq	EdgeLoop
	addq	$48, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset -48
.LBB1_27:                               #   in Loop: Header=BB1_19 Depth=2
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB1_30
# BB#28:                                #   in Loop: Header=BB1_19 Depth=2
	testb	%bl, %bl
	js	.LBB1_30
# BB#29:                                #   in Loop: Header=BB1_19 Depth=2
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	(%r15), %rdi
	movq	48(%rsp), %r14          # 8-byte Reload
	movl	(%r14), %r8d
	movl	4(%r14), %r9d
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	316872(%rax), %eax
	leaq	64(%rsp), %rcx
	movq	%rcx, %rsi
	movq	%r13, %rdx
	movq	%r13, %rbp
	movl	8(%rsp), %r13d          # 4-byte Reload
	movl	%r13d, %ecx
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	pushq	72(%rsp)                # 8-byte Folded Reload
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	callq	EdgeLoop
	addq	$48, %rsp
.Lcfi35:
	.cfi_adjust_cfa_offset -48
	movq	8(%r15), %rdi
	movb	7(%rsp), %r15b          # 1-byte Reload
	movl	(%r14), %r8d
	movl	4(%r14), %r9d
	movq	24(%rsp), %r14          # 8-byte Reload
	movl	316872(%r14), %eax
	leaq	64(%rsp), %rsi
	movq	%rbp, %rdx
	movl	%r13d, %ecx
	movq	%rbp, %r13
	movq	32(%rsp), %rbp          # 8-byte Reload
	pushq	%r14
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	callq	EdgeLoop
	addq	$48, %rsp
.Lcfi42:
	.cfi_adjust_cfa_offset -48
.LBB1_30:                               #   in Loop: Header=BB1_19 Depth=2
	testq	%rbp, %rbp
	je	.LBB1_38
# BB#31:                                #   in Loop: Header=BB1_19 Depth=2
	testq	%r12, %r12
	jne	.LBB1_38
# BB#32:                                #   in Loop: Header=BB1_19 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 12(%rax)
	jne	.LBB1_38
# BB#33:                                #   in Loop: Header=BB1_19 Depth=2
	movb	mixedModeEdgeFlag(%rip), %al
	testb	%al, %al
	je	.LBB1_38
# BB#34:                                #   in Loop: Header=BB1_19 Depth=2
	movl	$2, 5628(%r13)
	subq	$8, %rsp
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	movl	$4, %r8d
	leaq	72(%rsp), %rbp
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movl	16(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %edx
	movq	40(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	28(%rsp), %r9d          # 4-byte Reload
	pushq	%r14
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	callq	GetStrength
	addq	$16, %rsp
.Lcfi45:
	.cfi_adjust_cfa_offset -16
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r8d
	movl	4(%rax), %r9d
	movl	316864(%r14), %eax
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	movq	%r13, %rdx
	movq	32(%rsp), %rbp          # 8-byte Reload
	movl	%r15d, %ecx
	pushq	%r14
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	pushq	$4
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	callq	EdgeLoop
	addq	$48, %rsp
.Lcfi52:
	.cfi_adjust_cfa_offset -48
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB1_37
# BB#35:                                #   in Loop: Header=BB1_19 Depth=2
	testb	%bl, %bl
	js	.LBB1_37
# BB#36:                                #   in Loop: Header=BB1_19 Depth=2
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	(%r15), %rdi
	movq	48(%rsp), %rbp          # 8-byte Reload
	movl	(%rbp), %r8d
	movl	4(%rbp), %r9d
	movl	316872(%r14), %eax
	leaq	64(%rsp), %rbx
	movq	%rbx, %rsi
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdx
	movl	8(%rsp), %r14d          # 4-byte Reload
	movl	%r14d, %ecx
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	pushq	$4
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	pushq	72(%rsp)                # 8-byte Folded Reload
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	callq	EdgeLoop
	addq	$48, %rsp
.Lcfi59:
	.cfi_adjust_cfa_offset -48
	movq	8(%r15), %rdi
	movl	(%rbp), %r8d
	movl	4(%rbp), %r9d
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	316872(%rax), %eax
	movq	%rbx, %rsi
	movq	%r13, %rdx
	movq	32(%rsp), %rbp          # 8-byte Reload
	movl	%r14d, %ecx
	movq	24(%rsp), %r14          # 8-byte Reload
	pushq	%r14
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	pushq	$4
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	callq	EdgeLoop
	addq	$48, %rsp
.Lcfi66:
	.cfi_adjust_cfa_offset -48
.LBB1_37:                               #   in Loop: Header=BB1_19 Depth=2
	movq	40(%rsp), %r13          # 8-byte Reload
	movl	$1, 5628(%r13)
	movb	7(%rsp), %r15b          # 1-byte Reload
	.p2align	4, 0x90
.LBB1_38:                               #   in Loop: Header=BB1_19 Depth=2
	incq	%r12
	cmpq	$4, %r12
	jne	.LBB1_19
# BB#39:                                #   in Loop: Header=BB1_15 Depth=1
	incq	%rbp
	cmpq	$2, %rbp
	jne	.LBB1_15
.LBB1_40:                               # %.loopexit
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	$0, 5628(%rax)
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	DeblockMb, .Lfunc_end1-DeblockMb
	.cfi_endproc

	.globl	GetStrength
	.p2align	4, 0x90
	.type	GetStrength,@function
GetStrength:                            # @GetStrength
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi70:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi71:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi73:
	.cfi_def_cfa_offset 208
.Lcfi74:
	.cfi_offset %rbx, -56
.Lcfi75:
	.cfi_offset %r12, -48
.Lcfi76:
	.cfi_offset %r13, -40
.Lcfi77:
	.cfi_offset %r14, -32
.Lcfi78:
	.cfi_offset %r15, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movl	%r9d, 8(%rsp)           # 4-byte Spill
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movl	%ecx, %r13d
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	208(%rsp), %r10
	movq	316976(%r10), %rax
	movq	(%rax), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	316952(%r10), %rax
	movq	316960(%r10), %rcx
	movq	%rsi, 120(%rsp)         # 8-byte Spill
	movq	5600(%rsi), %rdi
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movslq	%edx, %rsi
	leal	(,%r8,4), %edx
	movq	%r8, 96(%rsp)           # 8-byte Spill
	cmpl	$4, %r8d
	movl	$1, %ebp
	movl	%edx, 52(%rsp)          # 4-byte Spill
	cmovll	%edx, %ebp
	movl	%ebp, 48(%rsp)          # 4-byte Spill
	imulq	$408, %rsi, %rsi        # imm = 0x198
	leaq	356(%rdi,%rsi), %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	(%rax), %rdx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	(%rcx), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	8(%rcx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	sete	7(%rsp)                 # 1-byte Folded Spill
	leal	-1(%r13), %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	xorl	%r12d, %r12d
	jmp	.LBB2_1
.LBB2_15:                               #   in Loop: Header=BB2_1 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	-316(%rax), %eax
	cmpl	$14, %eax
	ja	.LBB2_17
# BB#16:                                #   in Loop: Header=BB2_1 Depth=1
	movl	$26112, %edi            # imm = 0x6600
	btl	%eax, %edi
	jb	.LBB2_44
.LBB2_17:                               #   in Loop: Header=BB2_1 Depth=1
	andl	$-4, %ebx
	sarl	$2, %r14d
	addl	%ebx, %r14d
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	-52(%rax), %rax
	movb	$2, %dil
	btq	%r14, %rax
	jb	.LBB2_43
# BB#18:                                #   in Loop: Header=BB2_1 Depth=1
	andl	$-4, %esi
	sarl	$2, %r8d
	addl	%esi, %r8d
	movq	304(%rcx,%rdx), %rax
	btq	%r8, %rax
	jb	.LBB2_43
# BB#19:                                #   in Loop: Header=BB2_1 Depth=1
	movb	$1, %dil
	cmpb	$0, mixedModeEdgeFlag(%rip)
	jne	.LBB2_43
# BB#20:                                #   in Loop: Header=BB2_1 Depth=1
	movl	12(%rsp), %edi          # 4-byte Reload
	leaq	60(%rsp), %rsi
	leaq	56(%rsp), %rdx
	callq	*get_mb_block_pos(%rip)
	movl	56(%rsp), %eax
	movl	%r14d, %ecx
	sarl	$2, %ecx
	leal	(%rcx,%rax,4), %ecx
	movl	60(%rsp), %eax
	andl	$3, %r14d
	leal	(%r14,%rax,4), %esi
	movl	144(%rsp), %eax
	movl	148(%rsp), %edx
	sarl	$2, %edx
	sarl	$2, %eax
	movslq	%ecx, %r8
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi,%r8,8), %rdi
	movslq	%esi, %r14
	cmpb	$0, (%rdi,%r14)
	movabsq	$-9223372036854775808, %rcx # imm = 0x8000000000000000
	movq	%rcx, %r9
	movq	%r9, %rbx
	js	.LBB2_22
# BB#21:                                #   in Loop: Header=BB2_1 Depth=1
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi,%r8,8), %rsi
	movq	(%rsi,%r14,8), %rbx
.LBB2_22:                               #   in Loop: Header=BB2_1 Depth=1
	movslq	%edx, %rsi
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx,%rsi,8), %rdi
	movslq	%eax, %rdx
	cmpb	$0, (%rdi,%rdx)
	movq	%r9, %rcx
	js	.LBB2_24
# BB#23:                                #   in Loop: Header=BB2_1 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movq	(%rax,%rdx,8), %rcx
.LBB2_24:                               #   in Loop: Header=BB2_1 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r8,8), %rax
	cmpb	$0, (%rax,%r14)
	movq	%r9, %rax
	movq	%r15, %r10
	movb	$1, %dil
	js	.LBB2_26
# BB#25:                                #   in Loop: Header=BB2_1 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r8,8), %rax
	movq	(%rax,%r14,8), %rax
.LBB2_26:                               #   in Loop: Header=BB2_1 Depth=1
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp,%rsi,8), %rbp
	cmpb	$0, (%rbp,%rdx)
	movq	%r9, %rbp
	js	.LBB2_28
# BB#27:                                #   in Loop: Header=BB2_1 Depth=1
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp,%rsi,8), %rbp
	movq	(%rbp,%rdx,8), %rbp
.LBB2_28:                               #   in Loop: Header=BB2_1 Depth=1
	cmpq	%rcx, %rbx
	jne	.LBB2_30
# BB#29:                                #   in Loop: Header=BB2_1 Depth=1
	cmpq	%rbp, %rax
	je	.LBB2_32
.LBB2_30:                               #   in Loop: Header=BB2_1 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB2_43
# BB#31:                                #   in Loop: Header=BB2_1 Depth=1
	cmpq	%rbp, %rbx
	jne	.LBB2_43
.LBB2_32:                               #   in Loop: Header=BB2_1 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
	movb	$0, (%rbp,%r12)
	cmpq	%rax, %rbx
	movq	104(%rsp), %rbp         # 8-byte Reload
	movq	(%rbp,%r8,8), %rax
	movq	(%rax,%r14,8), %r11
	movswl	(%r11), %r10d
	movq	(%rbp,%rsi,8), %rax
	movq	(%rax,%rdx,8), %rbp
	jne	.LBB2_33
# BB#36:                                #   in Loop: Header=BB2_1 Depth=1
	movswl	(%rbp), %ecx
	movl	%r10d, %eax
	movl	%ecx, 44(%rsp)          # 4-byte Spill
	subl	%ecx, %eax
	movl	%eax, %r9d
	negl	%r9d
	cmovll	%eax, %r9d
	movswl	2(%r11), %ebx
	movswl	2(%rbp), %ecx
	movl	%ebx, %eax
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	subl	%ecx, %eax
	movl	%eax, %edi
	negl	%edi
	cmovll	%eax, %edi
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp,%r8,8), %rax
	movq	(%rax,%r14,8), %rcx
	movswl	(%rcx), %eax
	movq	(%rbp,%rsi,8), %rsi
	movq	(%rsi,%rdx,8), %rdx
	movswl	(%rdx), %r11d
	movl	%eax, %esi
	subl	%r11d, %esi
	movl	%esi, %ebp
	negl	%ebp
	cmovll	%esi, %ebp
	movswl	2(%rcx), %esi
	movswl	2(%rdx), %r14d
	movl	%esi, %ecx
	subl	%r14d, %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$3, %r9d
	jg	.LBB2_41
# BB#37:                                #   in Loop: Header=BB2_1 Depth=1
	cmpl	8(%rsp), %edi           # 4-byte Folded Reload
	jge	.LBB2_41
# BB#38:                                #   in Loop: Header=BB2_1 Depth=1
	cmpl	$3, %ebp
	jg	.LBB2_41
# BB#39:                                #   in Loop: Header=BB2_1 Depth=1
	cmpl	8(%rsp), %edx           # 4-byte Folded Reload
	jge	.LBB2_41
# BB#40:                                #   in Loop: Header=BB2_1 Depth=1
	xorl	%edi, %edi
	jmp	.LBB2_42
.LBB2_33:                               #   in Loop: Header=BB2_1 Depth=1
	cmpq	%rcx, %rbx
	jne	.LBB2_35
# BB#34:                                #   in Loop: Header=BB2_1 Depth=1
	movswl	(%rbp), %eax
	subl	%eax, %r10d
	movl	%r10d, %eax
	negl	%eax
	cmovll	%r10d, %eax
	cmpl	$3, %eax
	setg	%al
	movswl	2(%r11), %ebx
	movswl	2(%rbp), %ebp
	subl	%ebp, %ebx
	movl	%ebx, %edi
	negl	%edi
	cmovll	%ebx, %edi
	movl	8(%rsp), %ebp           # 4-byte Reload
	cmpl	%ebp, %edi
	setge	%bl
	orb	%al, %bl
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi,%r8,8), %rax
	movq	(%rax,%r14,8), %rax
	movswl	(%rax), %ecx
	movq	(%rdi,%rsi,8), %rsi
	movq	(%rsi,%rdx,8), %rdx
	movswl	(%rdx), %esi
	subl	%esi, %ecx
	movl	%ecx, %esi
	negl	%esi
	cmovll	%ecx, %esi
	cmpl	$3, %esi
	setg	%cl
	movswl	2(%rax), %eax
	movswl	2(%rdx), %edx
	subl	%edx, %eax
	movl	%eax, %edx
	negl	%edx
	cmovll	%eax, %edx
	cmpl	%ebp, %edx
	setge	%dil
	orb	%cl, %dil
	orb	%bl, %dil
	jmp	.LBB2_42
.LBB2_41:                               #   in Loop: Header=BB2_1 Depth=1
	subl	%r11d, %r10d
	movl	%r10d, %ecx
	negl	%ecx
	cmovll	%r10d, %ecx
	cmpl	$3, %ecx
	setg	%cl
	subl	%r14d, %ebx
	movl	%ebx, %edx
	negl	%edx
	cmovll	%ebx, %edx
	movl	8(%rsp), %edi           # 4-byte Reload
	cmpl	%edi, %edx
	setge	%dl
	orb	%cl, %dl
	subl	44(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	$3, %ecx
	setg	%al
	subl	40(%rsp), %esi          # 4-byte Folded Reload
	movl	%esi, %ecx
	negl	%ecx
	cmovll	%esi, %ecx
	cmpl	%edi, %ecx
	setge	%dil
	orb	%al, %dil
	orb	%dl, %dil
	jmp	.LBB2_42
.LBB2_35:                               #   in Loop: Header=BB2_1 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rsi,8), %rax
	movq	(%rax,%rdx,8), %rax
	movswl	(%rax), %edx
	subl	%edx, %r10d
	movl	%r10d, %edx
	negl	%edx
	cmovll	%r10d, %edx
	cmpl	$3, %edx
	setg	%dl
	movswl	2(%r11), %esi
	movswl	2(%rax), %eax
	subl	%eax, %esi
	movl	%esi, %eax
	negl	%eax
	cmovll	%esi, %eax
	movl	8(%rsp), %edi           # 4-byte Reload
	cmpl	%edi, %eax
	setge	%al
	orb	%dl, %al
	movq	(%rcx,%r8,8), %rcx
	movq	(%rcx,%r14,8), %rcx
	movswl	(%rcx), %edx
	movswl	(%rbp), %esi
	subl	%esi, %edx
	movl	%edx, %esi
	negl	%esi
	cmovll	%edx, %esi
	cmpl	$3, %esi
	setg	%dl
	movswl	2(%rcx), %ecx
	movswl	2(%rbp), %esi
	subl	%esi, %ecx
	movl	%ecx, %esi
	negl	%esi
	cmovll	%ecx, %esi
	cmpl	%edi, %esi
	setge	%dil
	orb	%dl, %dil
	orb	%al, %dil
.LBB2_42:                               #   in Loop: Header=BB2_1 Depth=1
	movq	%r15, %r10
.LBB2_43:                               #   in Loop: Header=BB2_1 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movb	%dil, (%rax,%r12)
	jmp	.LBB2_44
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	testl	%r13d, %r13d
	movl	52(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r14d
	cmovnel	%r12d, %r14d
	movl	%r12d, %ebx
	cmovnel	48(%rsp), %ebx          # 4-byte Folded Reload
	movq	112(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %esi
	movl	%ebx, %edx
	subl	%r13d, %edx
	xorl	%ecx, %ecx
	movl	12(%rsp), %edi          # 4-byte Reload
	leaq	128(%rsp), %r8
	movq	%r10, %r15
	callq	*getNeighbour(%rip)
	movq	%r15, %r10
	movl	136(%rsp), %r8d
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	5600(%rax), %rcx
	movslq	132(%rsp), %rax
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx), %ebp
	imulq	$408, %rax, %rdx        # imm = 0x198
	movl	356(%rcx,%rdx), %edi
	cmpl	%edi, %ebp
	setne	mixedModeEdgeFlag(%rip)
	movl	140(%rsp), %esi
	movl	317024(%r10), %eax
	addl	$-3, %eax
	cmpl	$1, %eax
	ja	.LBB2_8
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	movb	$3, %al
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	jne	.LBB2_7
# BB#3:                                 #   in Loop: Header=BB2_1 Depth=1
	cmpl	$0, 316904(%r10)
	je	.LBB2_4
# BB#5:                                 # %.thread
                                        #   in Loop: Header=BB2_1 Depth=1
	orl	%ebp, %edi
	sete	%cl
	orb	7(%rsp), %cl            # 1-byte Folded Reload
	cmpb	$1, %cl
	je	.LBB2_6
	jmp	.LBB2_7
	.p2align	4, 0x90
.LBB2_8:                                #   in Loop: Header=BB2_1 Depth=1
	movb	$3, %r9b
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	jne	.LBB2_13
# BB#9:                                 #   in Loop: Header=BB2_1 Depth=1
	cmpl	$0, 316904(%r10)
	je	.LBB2_10
# BB#11:                                # %.thread291
                                        #   in Loop: Header=BB2_1 Depth=1
	orl	%ebp, %edi
	sete	%al
	orb	7(%rsp), %al            # 1-byte Folded Reload
	cmpb	$1, %al
	je	.LBB2_12
	jmp	.LBB2_13
.LBB2_4:                                #   in Loop: Header=BB2_1 Depth=1
	cmpl	$0, (%r10)
	sete	%cl
	orb	7(%rsp), %cl            # 1-byte Folded Reload
	je	.LBB2_7
.LBB2_6:                                # %.thread287
                                        #   in Loop: Header=BB2_1 Depth=1
	movb	$4, %al
	.p2align	4, 0x90
.LBB2_7:                                # %.thread289
                                        #   in Loop: Header=BB2_1 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movb	%al, (%rcx,%r12)
	jmp	.LBB2_44
.LBB2_10:                               #   in Loop: Header=BB2_1 Depth=1
	cmpl	$0, (%r10)
	sete	%al
	orb	7(%rsp), %al            # 1-byte Folded Reload
	je	.LBB2_13
.LBB2_12:                               # %.thread295
                                        #   in Loop: Header=BB2_1 Depth=1
	movb	$4, %r9b
	.p2align	4, 0x90
.LBB2_13:                               # %.thread297
                                        #   in Loop: Header=BB2_1 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movb	%r9b, (%rax,%r12)
	movl	40(%rcx,%rdx), %eax
	cmpl	$14, %eax
	ja	.LBB2_15
# BB#14:                                # %.thread297
                                        #   in Loop: Header=BB2_1 Depth=1
	movl	$26112, %edi            # imm = 0x6600
	btl	%eax, %edi
	jae	.LBB2_15
.LBB2_44:                               #   in Loop: Header=BB2_1 Depth=1
	incq	%r12
	cmpq	$16, %r12
	jne	.LBB2_1
# BB#45:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	GetStrength, .Lfunc_end2-GetStrength
	.cfi_endproc

	.globl	EdgeLoop
	.p2align	4, 0x90
	.type	EdgeLoop,@function
EdgeLoop:                               # @EdgeLoop
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi83:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi84:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi86:
	.cfi_def_cfa_offset 320
.Lcfi87:
	.cfi_offset %rbx, -56
.Lcfi88:
	.cfi_offset %r12, -48
.Lcfi89:
	.cfi_offset %r13, -40
.Lcfi90:
	.cfi_offset %r14, -32
.Lcfi91:
	.cfi_offset %r15, -24
.Lcfi92:
	.cfi_offset %rbp, -16
	movl	%r9d, 112(%rsp)         # 4-byte Spill
	movl	%r8d, 108(%rsp)         # 4-byte Spill
	movq	%rsi, 200(%rsp)         # 8-byte Spill
	movq	%rdi, 192(%rsp)         # 8-byte Spill
	movl	344(%rsp), %ebx
	leaq	5880(%rdx), %rax
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	leaq	5876(%rdx), %rdx
	testl	%ebx, %ebx
	cmovneq	%rax, %rdx
	movl	(%rdx), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	testl	%ebx, %ebx
	movl	320(%rsp), %ebp
	je	.LBB3_1
# BB#2:
	movq	360(%rsp), %rax
	movslq	%ebp, %rdx
	movslq	317044(%rax), %rax
	shlq	$4, %rdx
	movl	EdgeLoop.pelnum_cr(%rdx,%rax,4), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	jg	.LBB3_3
	jmp	.LBB3_51
.LBB3_1:
	movl	$16, 32(%rsp)           # 4-byte Folded Spill
.LBB3_3:                                # %.lr.ph
	addl	$-8, 36(%rsp)           # 4-byte Folded Spill
	movl	328(%rsp), %eax
	testl	%ebx, %ebx
	setne	%dl
	leal	(,%rax,4), %esi
	cmpl	$4, %eax
	movl	$1, %eax
	movl	$1, %edi
	movl	%esi, 124(%rsp)         # 4-byte Spill
	cmovll	%esi, %edi
	movl	%edi, 120(%rsp)         # 4-byte Spill
	leal	-1(%rbp), %esi
	movl	%esi, 116(%rsp)         # 4-byte Spill
	cmpl	$8, 32(%rsp)            # 4-byte Folded Reload
	movl	%ebx, %esi
	sete	%bl
	andb	%dl, %bl
	movb	%bl, 15(%rsp)           # 1-byte Spill
	movl	%esi, %ebx
	testl	%ebp, %ebp
	movslq	%ecx, %rcx
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	movslq	352(%rsp), %rcx
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	cmovnel	336(%rsp), %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movl	$0, 60(%rsp)            # 4-byte Folded Spill
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB3_4
.LBB3_22:                               #   in Loop: Header=BB3_4 Depth=1
	movq	%rdi, 160(%rsp)         # 8-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%r13, 64(%rsp)          # 8-byte Spill
	addl	%r9d, %r9d
	movslq	%r9d, %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movzwl	(%rbp,%rax,2), %edx
	movq	88(%rsp), %rsi          # 8-byte Reload
	leal	(%rsi,%rsi), %ecx
	movslq	%ecx, %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	168(%rsp), %rdi         # 8-byte Reload
	movzwl	(%rdi,%rax,2), %r13d
	imull	$-3, 48(%rsp), %ecx     # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movzwl	(%rbp,%rcx,2), %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	leal	(%rsi,%rsi,2), %eax
	cltq
	movzwl	(%rdi,%rax,2), %ecx
	cmpl	$0, 344(%rsp)
	movl	16(%rsp), %esi          # 4-byte Reload
	movq	176(%rsp), %rbp         # 8-byte Reload
	jne	.LBB3_24
# BB#23:                                #   in Loop: Header=BB3_4 Depth=1
	movl	%ebp, %eax
	subl	%r13d, %eax
	movq	%rcx, %r8
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	xorl	%eax, %eax
	cmpl	%ebx, %ecx
	setl	%al
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%edx, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	xorl	%esi, %esi
	cmpl	%ebx, %ecx
	movq	%r8, %rcx
	setl	%sil
.LBB3_24:                               #   in Loop: Header=BB3_4 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rbp, %r8
	leal	(%rbp,%rax), %r9d
	cmpb	$4, %r14b
	movl	320(%rsp), %ebp
	jne	.LBB3_43
# BB#25:                                #   in Loop: Header=BB3_4 Depth=1
	movl	344(%rsp), %ebx
	testl	%ebx, %ebx
	je	.LBB3_27
# BB#26:                                #   in Loop: Header=BB3_4 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	addl	%edx, %r8d
	movq	64(%rsp), %rcx          # 8-byte Reload
	leal	2(%r8,%rcx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%rdi)
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rdx,2), %eax
	leal	2(%rcx,%rax), %r13d
	shrl	$2, %r13d
	movq	48(%rsp), %rdi          # 8-byte Reload
	movw	%r13w, (%rdi)
	jmp	.LBB3_50
.LBB3_43:                               #   in Loop: Header=BB3_4 Depth=1
	movq	%rdi, %r12
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %r13
	movq	160(%rsp), %r10         # 8-byte Reload
	leal	1(%r10), %ecx
	movq	24(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%r10), %edx
	addl	%esi, %edx
	movl	344(%rsp), %r14d
	testl	%r14d, %r14d
	cmovnel	%ecx, %edx
	movl	%esi, %ebx
	movl	%edx, %esi
	negl	%esi
	movq	72(%rsp), %rdi          # 8-byte Reload
	leal	4(%rdi), %ecx
	movq	64(%rsp), %rax          # 8-byte Reload
	subl	%eax, %ecx
	leal	(%rcx,%r11,4), %ecx
	sarl	$3, %ecx
	cmpl	%esi, %ecx
	cmovll	%esi, %ecx
	cmpl	%edx, %ecx
	cmovgl	%edx, %ecx
	testl	%r14d, %r14d
	je	.LBB3_44
# BB#49:                                #   in Loop: Header=BB3_4 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	5904(%rax), %eax
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rdi
	addl	%ecx, %edi
	movl	$0, %edx
	cmovsl	%edx, %edi
	cmpl	%eax, %edi
	cmovgew	%ax, %di
	movq	48(%rsp), %rsi          # 8-byte Reload
	movw	%di, (%rsi)
	subl	%ecx, %r8d
	cmovsl	%edx, %r8d
	cmpl	%eax, %r8d
	cmovgew	%ax, %r8w
	movw	%r8w, (%r12)
	movl	%ebx, %esi
	movl	344(%rsp), %ebx
	jmp	.LBB3_50
.LBB3_27:                               #   in Loop: Header=BB3_4 Depth=1
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r14
	sarl	$2, %r10d
	addl	$2, %r10d
	xorl	%ecx, %ecx
	cmpl	%r10d, %r12d
	setl	%cl
	andl	%ecx, %esi
	movq	24(%rsp), %rdx          # 8-byte Reload
	andl	%ecx, %edx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	je	.LBB3_29
# BB#28:                                #   in Loop: Header=BB3_4 Depth=1
	movq	64(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%r9), %r8d
	addl	%r8d, %r8d
	movl	$3, %ecx
	movl	$4, %eax
	movq	72(%rsp), %r12          # 8-byte Reload
	movl	%r12d, %ebx
	movq	%r13, %r11
	movl	%r13d, %edi
	jmp	.LBB3_30
.LBB3_44:                               #   in Loop: Header=BB3_4 Depth=1
	movq	96(%rsp), %rdx          # 8-byte Reload
	movl	5900(%rdx), %edx
	movq	40(%rsp), %rsi          # 8-byte Reload
	addl	%ecx, %esi
	movl	$0, %r14d
	cmovsl	%r14d, %esi
	cmpl	%edx, %esi
	cmovgew	%dx, %si
	movq	48(%rsp), %r11          # 8-byte Reload
	movw	%si, (%r11)
	subl	%ecx, %r8d
	cmovsl	%r14d, %r8d
	cmpl	%edx, %r8d
	cmovgew	%dx, %r8w
	movw	%r8w, (%r12)
	testl	%ebx, %ebx
	movl	%ebx, %esi
	je	.LBB3_46
# BB#45:                                #   in Loop: Header=BB3_4 Depth=1
	movl	%r10d, %ecx
	negl	%ecx
	leal	1(%r9), %edx
	shrl	%edx
	addl	%edi, %edi
	subl	%edi, %edx
	addl	%r13d, %edx
	sarl	%edx
	cmpl	%ecx, %edx
	cmovll	%ecx, %edx
	cmpl	%r10d, %edx
	cmovgl	%r10d, %edx
	movq	128(%rsp), %rdi         # 8-byte Reload
	movzwl	(%r11,%rdi,2), %ecx
	addl	%edx, %ecx
	movw	%cx, (%r11,%rdi,2)
.LBB3_46:                               #   in Loop: Header=BB3_4 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movl	344(%rsp), %ebx
	je	.LBB3_47
# BB#48:                                #   in Loop: Header=BB3_4 Depth=1
	movl	%r10d, %ecx
	negl	%ecx
	incl	%r9d
	shrl	%r9d
	addl	%eax, %eax
	subl	%eax, %r9d
	addl	16(%rsp), %r9d          # 4-byte Folded Reload
	sarl	%r9d
	cmpl	%ecx, %r9d
	cmovll	%ecx, %r9d
	cmpl	%r10d, %r9d
	cmovgl	%r10d, %r9d
	movq	80(%rsp), %rax          # 8-byte Reload
	movzwl	(%r12,%rax,2), %ecx
	addl	%r9d, %ecx
	movw	%cx, (%r12,%rax,2)
	jmp	.LBB3_50
.LBB3_29:                               #   in Loop: Header=BB3_4 Depth=1
	movq	%r13, %r11
	movq	64(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%rdx), %ebx
	movl	$2, %eax
	movq	72(%rsp), %r12          # 8-byte Reload
	movl	%r12d, %edi
	movl	$2, %ecx
.LBB3_30:                               #   in Loop: Header=BB3_4 Depth=1
	movq	80(%rsp), %r10          # 8-byte Reload
	addl	%ebx, %r8d
	addl	%edi, %r8d
	addl	%eax, %r8d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r8d
	movw	%r8w, (%r14)
	testl	%esi, %esi
	je	.LBB3_32
# BB#31:                                #   in Loop: Header=BB3_4 Depth=1
	leal	(%r12,%r9), %eax
	addl	%eax, %eax
	movl	$3, %ecx
	movl	$4, %r8d
	movl	%edx, %edi
	movq	16(%rsp), %rbx          # 8-byte Reload
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill>
	jmp	.LBB3_33
.LBB3_47:                               #   in Loop: Header=BB3_4 Depth=1
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB3_50
.LBB3_32:                               #   in Loop: Header=BB3_4 Depth=1
	leal	(%r12,%r12), %edi
	movl	$2, %r8d
	movl	%edx, %ebx
	movl	$2, %ecx
	movq	40(%rsp), %rax          # 8-byte Reload
.LBB3_33:                               #   in Loop: Header=BB3_4 Depth=1
	addl	%edi, %eax
	addl	%ebx, %eax
	addl	%r8d, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movw	%ax, (%rdi)
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movl	%edx, %ecx
	je	.LBB3_35
# BB#34:                                #   in Loop: Header=BB3_4 Depth=1
	leal	(%r9,%rdx), %ecx
	leal	2(%r11,%rcx), %ecx
	shrl	$2, %ecx
.LBB3_35:                               #   in Loop: Header=BB3_4 Depth=1
	movw	%cx, (%r14,%r10,2)
	testl	%esi, %esi
	movl	%r12d, %ecx
	movl	344(%rsp), %ebx
	movq	128(%rsp), %r8          # 8-byte Reload
	je	.LBB3_37
# BB#36:                                #   in Loop: Header=BB3_4 Depth=1
	leal	(%r9,%r12), %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	2(%rax,%rcx), %ecx
	shrl	$2, %ecx
.LBB3_37:                               #   in Loop: Header=BB3_4 Depth=1
	movw	%cx, (%rdi,%r8,2)
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB3_39
# BB#38:                                #   in Loop: Header=BB3_4 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	addl	%r11d, %eax
	addl	%r9d, %edx
	addl	%r11d, %edx
	leal	4(%rdx,%rax,2), %r11d
	shrl	$3, %r11d
.LBB3_39:                               #   in Loop: Header=BB3_4 Depth=1
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	152(%rsp), %rax         # 8-byte Reload
	leaq	(%rdi,%rax,2), %rdi
	movq	144(%rsp), %rax         # 8-byte Reload
	movw	%r11w, (%r14,%rax,2)
	testl	%esi, %esi
	je	.LBB3_40
# BB#41:                                #   in Loop: Header=BB3_4 Depth=1
	movq	136(%rsp), %rax         # 8-byte Reload
	addl	%r13d, %eax
	addl	%r9d, %r12d
	addl	%r13d, %r12d
	leal	4(%r12,%rax,2), %r13d
	shrl	$3, %r13d
	movw	%r13w, (%rdi)
	jmp	.LBB3_50
.LBB3_40:                               #   in Loop: Header=BB3_4 Depth=1
	xorl	%esi, %esi
	movw	%r13w, (%rdi)
	jmp	.LBB3_50
	.p2align	4, 0x90
.LBB3_4:                                # =>This Inner Loop Header: Depth=1
	movl	%esi, 16(%rsp)          # 4-byte Spill
	testl	%ebp, %ebp
	movl	124(%rsp), %r12d        # 4-byte Reload
	cmovnel	%r15d, %r12d
	movl	%r15d, %r13d
	cmovnel	120(%rsp), %r13d        # 4-byte Folded Reload
	movq	208(%rsp), %r14         # 8-byte Reload
	movl	%r14d, %edi
	movl	%r12d, %esi
	movl	%r13d, %edx
	movl	%ebx, %ecx
	leaq	240(%rsp), %r8
	callq	*getNeighbour(%rip)
	addl	116(%rsp), %r12d        # 4-byte Folded Reload
	subl	%ebp, %r13d
	movl	%r14d, %edi
	movl	%r12d, %esi
	movl	%r13d, %edx
	movl	%ebx, %r12d
	movl	%ebx, %ecx
	leaq	216(%rsp), %r8
	callq	*getNeighbour(%rip)
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	5600(%rax), %rbx
	movslq	220(%rsp), %r9
	imulq	$408, %r14, %rdi        # imm = 0x198
	movl	356(%rbx,%rdi), %ecx
	movb	$1, %al
	testl	%ecx, %ecx
	jne	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=1
	imulq	$408, %r9, %rax         # imm = 0x198
	cmpl	$0, 356(%rbx,%rax)
	setne	%al
.LBB3_6:                                #   in Loop: Header=BB3_4 Depth=1
	movb	%al, fieldModeFilteringFlag(%rip)
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	movl	%r15d, %esi
	je	.LBB3_10
# BB#7:                                 #   in Loop: Header=BB3_4 Depth=1
	testl	%ecx, %ecx
	je	.LBB3_9
# BB#8:                                 #   in Loop: Header=BB3_4 Depth=1
	imulq	$408, %r9, %rsi         # imm = 0x198
	cmpl	$0, 356(%rbx,%rsi)
	movl	60(%rsp), %esi          # 4-byte Reload
	je	.LBB3_10
.LBB3_9:                                #   in Loop: Header=BB3_4 Depth=1
	movl	%r15d, %esi
	andl	$2147483646, %esi       # imm = 0x7FFFFFFE
	movl	%r15d, %ebp
	andl	$1, %ebp
	leal	(%rbp,%rsi,2), %esi
	movl	320(%rsp), %ebp
.LBB3_10:                               #   in Loop: Header=BB3_4 Depth=1
	cmpl	$0, 216(%rsp)
	je	.LBB3_11
.LBB3_13:                               #   in Loop: Header=BB3_4 Depth=1
	testl	%ebp, %ebp
	setne	%dl
	andb	%al, %dl
	cmpb	$1, %dl
	movl	104(%rsp), %edx         # 4-byte Reload
	movl	%edx, %r13d
	movl	%edx, %r8d
	jne	.LBB3_15
# BB#14:                                #   in Loop: Header=BB3_4 Depth=1
	testl	%ecx, %ecx
	sete	%cl
	movl	336(%rsp), %eax
	movl	%eax, %edx
	movl	%edx, %r13d
	shll	%cl, %r13d
	imulq	$408, %r9, %rcx         # imm = 0x198
	cmpl	$0, 356(%rbx,%rcx)
	sete	%cl
	movl	%edx, %r8d
	shll	%cl, %r8d
.LBB3_15:                               # %.thread
                                        #   in Loop: Header=BB3_4 Depth=1
	movslq	260(%rsp), %rcx
	movslq	256(%rsp), %rax
	addq	%rax, %rax
	movq	192(%rsp), %rdx         # 8-byte Reload
	addq	(%rdx,%rcx,8), %rax
	movslq	236(%rsp), %rcx
	movslq	232(%rsp), %rbp
	addq	%rbp, %rbp
	addq	(%rdx,%rcx,8), %rbp
	imulq	$408, %r9, %rcx         # imm = 0x198
	testl	%r12d, %r12d
	je	.LBB3_17
# BB#16:                                #   in Loop: Header=BB3_4 Depth=1
	addq	%rbx, %rcx
	addq	%rbx, %rdi
	movq	184(%rsp), %rdx         # 8-byte Reload
	movl	4(%rdi,%rdx,4), %edi
	addl	4(%rcx,%rdx,4), %edi
	jmp	.LBB3_18
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_4 Depth=1
	cmpl	$0, 340(%rbx,%rdi)
	je	.LBB3_13
# BB#12:                                #   in Loop: Header=BB3_4 Depth=1
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	%r12d, %ebx
	jmp	.LBB3_50
	.p2align	4, 0x90
.LBB3_17:                               #   in Loop: Header=BB3_4 Depth=1
	movl	(%rbx,%rdi), %edi
	addl	(%rbx,%rcx), %edi
.LBB3_18:                               #   in Loop: Header=BB3_4 Depth=1
	incl	%edi
	sarl	%edi
	movl	%edi, %edx
	addl	108(%rsp), %edx         # 4-byte Folded Reload
	movl	$0, %ecx
	cmovsl	%ecx, %edx
	cmpl	$52, %edx
	movl	$51, %ebx
	cmovgel	%ebx, %edx
	addl	112(%rsp), %edi         # 4-byte Folded Reload
	cmovsl	%ecx, %edi
	cmpl	$52, %edi
	cmovgel	%ebx, %edi
	movzbl	ALPHA_TABLE(%rdx), %r10d
	movl	36(%rsp), %ecx          # 4-byte Reload
	shll	%cl, %r10d
	movzbl	BETA_TABLE(%rdi), %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	movslq	%esi, %rcx
	movq	200(%rsp), %rsi         # 8-byte Reload
	movzbl	(%rsi,%rcx), %r14d
	testq	%r14, %r14
	je	.LBB3_19
# BB#20:                                #   in Loop: Header=BB3_4 Depth=1
	movzwl	(%rbp), %esi
	movzwl	(%rax), %ecx
	movl	%ecx, %r11d
	subl	%esi, %r11d
	movl	%r11d, %r12d
	negl	%r12d
	cmovll	%r11d, %r12d
	cmpl	%r10d, %r12d
	jge	.LBB3_19
# BB#21:                                #   in Loop: Header=BB3_4 Depth=1
	movq	%r13, %rdi
	movq	%rcx, %r13
	movl	%r8d, %r9d
	negl	%r9d
	movslq	%r9d, %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movl	%r8d, 48(%rsp)          # 4-byte Spill
	movzwl	(%rbp,%rcx,2), %r8d
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	movslq	%edi, %rcx
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movzwl	(%rax,%rcx,2), %eax
	leaq	(%rdx,%rdx,4), %rcx
	movzbl	CLIP_TAB(%r14,%rcx), %edi
	movl	36(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	movq	%r13, 176(%rsp)         # 8-byte Spill
	movl	%r13d, %edx
	movq	%rax, %r13
	subl	%r13d, %edx
	movl	%edx, %ecx
	negl	%ecx
	cmovll	%edx, %ecx
	subl	%ebx, %ecx
	movq	%rsi, %rax
	movq	%r8, 72(%rsp)           # 8-byte Spill
	subl	%r8d, %esi
	movl	%esi, %edx
	negl	%edx
	cmovll	%esi, %edx
	subl	%ebx, %edx
	testl	%edx, %ecx
	js	.LBB3_22
.LBB3_19:                               #   in Loop: Header=BB3_4 Depth=1
	movl	344(%rsp), %ebx
	movl	320(%rsp), %ebp
	movl	16(%rsp), %esi          # 4-byte Reload
.LBB3_50:                               #   in Loop: Header=BB3_4 Depth=1
	incl	%r15d
	addl	$2, 60(%rsp)            # 4-byte Folded Spill
	cmpl	%r15d, 32(%rsp)         # 4-byte Folded Reload
	jne	.LBB3_4
.LBB3_51:                               # %._crit_edge
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	EdgeLoop, .Lfunc_end3-EdgeLoop
	.cfi_endproc

	.type	.LDeblockMb.filterNon8x8LumaEdgesFlag,@object # @DeblockMb.filterNon8x8LumaEdgesFlag
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LDeblockMb.filterNon8x8LumaEdgesFlag:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.size	.LDeblockMb.filterNon8x8LumaEdgesFlag, 16

	.type	chroma_edge,@object     # @chroma_edge
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
chroma_edge:
	.asciz	"\377\000\000"
	.ascii	"\377\377\377\001"
	.ascii	"\377\001\001\002"
	.ascii	"\377\377\377\003"
	.asciz	"\377\000\000"
	.ascii	"\377\377\001\001"
	.ascii	"\377\001\002\002"
	.ascii	"\377\377\003\003"
	.size	chroma_edge, 32

	.type	mixedModeEdgeFlag,@object # @mixedModeEdgeFlag
	.comm	mixedModeEdgeFlag,1,1
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	EdgeLoop.pelnum_cr,@object # @EdgeLoop.pelnum_cr
	.p2align	4
EdgeLoop.pelnum_cr:
	.long	0                       # 0x0
	.long	8                       # 0x8
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	0                       # 0x0
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	16                      # 0x10
	.size	EdgeLoop.pelnum_cr, 32

	.type	fieldModeFilteringFlag,@object # @fieldModeFilteringFlag
	.comm	fieldModeFilteringFlag,1,1
	.type	ALPHA_TABLE,@object     # @ALPHA_TABLE
	.section	.rodata,"a",@progbits
	.p2align	4
ALPHA_TABLE:
	.ascii	"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\004\004\005\006\007\b\t\n\f\r\017\021\024\026\031\034 $(-28?GPZeq\177\220\242\266\313\342\377\377"
	.size	ALPHA_TABLE, 52

	.type	BETA_TABLE,@object      # @BETA_TABLE
	.p2align	4
BETA_TABLE:
	.ascii	"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\002\002\002\003\003\003\003\004\004\004\006\006\007\007\b\b\t\t\n\n\013\013\f\f\r\r\016\016\017\017\020\020\021\021\022\022"
	.size	BETA_TABLE, 52

	.type	CLIP_TAB,@object        # @CLIP_TAB
	.p2align	4
CLIP_TAB:
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.zero	5
	.ascii	"\000\000\000\001\001"
	.ascii	"\000\000\000\001\001"
	.ascii	"\000\000\000\001\001"
	.ascii	"\000\000\000\001\001"
	.ascii	"\000\000\001\001\001"
	.ascii	"\000\000\001\001\001"
	.ascii	"\000\001\001\001\001"
	.ascii	"\000\001\001\001\001"
	.ascii	"\000\001\001\001\001"
	.ascii	"\000\001\001\001\001"
	.ascii	"\000\001\001\002\002"
	.ascii	"\000\001\001\002\002"
	.ascii	"\000\001\001\002\002"
	.ascii	"\000\001\001\002\002"
	.ascii	"\000\001\002\003\003"
	.ascii	"\000\001\002\003\003"
	.ascii	"\000\002\002\003\003"
	.ascii	"\000\002\002\004\004"
	.ascii	"\000\002\003\004\004"
	.ascii	"\000\002\003\004\004"
	.ascii	"\000\003\003\005\005"
	.ascii	"\000\003\004\006\006"
	.ascii	"\000\003\004\006\006"
	.ascii	"\000\004\005\007\007"
	.ascii	"\000\004\005\b\b"
	.ascii	"\000\004\006\t\t"
	.ascii	"\000\005\007\n\n"
	.ascii	"\000\006\b\013\013"
	.ascii	"\000\006\b\r\r"
	.ascii	"\000\007\n\016\016"
	.ascii	"\000\b\013\020\020"
	.ascii	"\000\t\f\022\022"
	.ascii	"\000\n\r\024\024"
	.ascii	"\000\013\017\027\027"
	.ascii	"\000\r\021\031\031"
	.size	CLIP_TAB, 260

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
