	.text
	.file	"7zExtract.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16
	.text
	.globl	_ZN8NArchive3N7z8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler7ExtractEPKjjiP23IArchiveExtractCallback,@function
_ZN8NArchive3N7z8CHandler7ExtractEPKjjiP23IArchiveExtractCallback: # @_ZN8NArchive3N7z8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$472, %rsp              # imm = 0x1D8
.Lcfi6:
	.cfi_def_cfa_offset 528
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, %rbp
	movl	%ecx, %ebx
	movl	%edx, %r13d
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r12
	testq	%rbp, %rbp
	je	.LBB0_2
# BB#1:
	movq	(%rbp), %rax
.Ltmp0:
.Lcfi13:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp1:
.LBB0_2:                                # %_ZN9CMyComPtrI23IArchiveExtractCallbackEC2EPS0_.exit
	cmpl	$-1, %r13d
	movl	%r13d, %ecx
	jne	.LBB0_4
# BB#3:
	movl	316(%r12), %ecx
.LBB0_4:
	testl	%ecx, %ecx
	je	.LBB0_5
# BB#8:                                 # %.lr.ph417
	movl	%ebx, 116(%rsp)         # 4-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rsp)
	movq	$8, 48(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE+16, 24(%rsp)
	leaq	136(%rsp), %rbp
	movl	%ecx, %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	movl	$1, %eax
	movd	%rax, %xmm0
	movdqa	%xmm0, 80(%rsp)         # 16-byte Spill
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%r12, 120(%rsp)         # 8-byte Spill
	movl	%r13d, 64(%rsp)         # 4-byte Spill
.LBB0_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_36 Depth 2
                                        #       Child Loop BB0_39 Depth 3
                                        #     Child Loop BB0_58 Depth 2
	cmpl	$-1, %r13d
	je	.LBB0_10
# BB#11:                                #   in Loop: Header=BB0_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax,%r15,4), %ebx
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_9 Depth=1
	movl	%r15d, %ebx
.LBB0_12:                               #   in Loop: Header=BB0_9 Depth=1
	movq	808(%r12), %rax
	movslq	%ebx, %rcx
	movslq	(%rax,%rcx,4), %r14
	cmpq	$-1, %r14
	je	.LBB0_13
# BB#27:                                #   in Loop: Header=BB0_9 Depth=1
	movslq	36(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_29
# BB#28:                                #   in Loop: Header=BB0_9 Depth=1
	movq	40(%rsp), %rcx
	movq	-8(%rcx,%rax,8), %r13
	cmpl	4(%r13), %r14d
	je	.LBB0_44
.LBB0_29:                               #   in Loop: Header=BB0_9 Depth=1
	movl	$-1, 192(%rsp)
	movl	%r14d, 196(%rsp)
	leaq	200(%rsp), %r13
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 8(%r13)
	movq	$_ZTV13CRecordVectorIbE+16, 200(%rsp)
	movl	$1, %eax
	movd	%rax, %xmm0
	movdqu	%xmm0, 224(%rsp)
.Ltmp3:
.Lcfi14:
	.cfi_escape 0x2e, 0x00
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp4:
# BB#30:                                # %.noexc312
                                        #   in Loop: Header=BB0_9 Depth=1
	movq	192(%rsp), %rax
	movq	%rax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp5:
.Lcfi15:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rsi
	callq	_ZN13CRecordVectorIbEC2ERKS0_
.Ltmp6:
# BB#31:                                #   in Loop: Header=BB0_9 Depth=1
	movq	232(%rsp), %rax
	movq	%rax, 40(%rbp)
.Ltmp8:
.Lcfi16:
	.cfi_escape 0x2e, 0x00
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp9:
# BB#32:                                #   in Loop: Header=BB0_9 Depth=1
	movq	40(%rsp), %rax
	movslq	36(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 36(%rsp)
.Ltmp13:
.Lcfi17:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp14:
# BB#33:                                #   in Loop: Header=BB0_9 Depth=1
	movq	256(%r12), %rax
	movq	(%rax,%r14,8), %rax
	movl	108(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB0_34
# BB#35:                                # %.preheader.i
                                        #   in Loop: Header=BB0_9 Depth=1
	leaq	136(%rsp), %rbp
.LBB0_36:                               #   Parent Loop BB0_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_39 Depth 3
	testl	%ecx, %ecx
	jle	.LBB0_46
# BB#37:                                #   in Loop: Header=BB0_36 Depth=2
	decl	%ecx
	movslq	44(%rax), %rdx
	testq	%rdx, %rdx
	jle	.LBB0_42
# BB#38:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB0_36 Depth=2
	movq	48(%rax), %rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_39:                               #   Parent Loop BB0_9 Depth=1
                                        #     Parent Loop BB0_36 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%ecx, 4(%rdi,%rsi,8)
	je	.LBB0_41
# BB#40:                                #   in Loop: Header=BB0_39 Depth=3
	incq	%rsi
	cmpq	%rdx, %rsi
	jl	.LBB0_39
	jmp	.LBB0_42
	.p2align	4, 0x90
.LBB0_41:                               # %_ZNK8NArchive3N7z7CFolder24FindBindPairForOutStreamEj.exit.i
                                        #   in Loop: Header=BB0_36 Depth=2
	testl	%esi, %esi
	jns	.LBB0_36
	.p2align	4, 0x90
.LBB0_42:                               # %_ZNK8NArchive3N7z7CFolder24FindBindPairForOutStreamEj.exit.thread.i
                                        #   in Loop: Header=BB0_9 Depth=1
	movq	112(%rax), %rax
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
	jmp	.LBB0_43
	.p2align	4, 0x90
.LBB0_13:                               #   in Loop: Header=BB0_9 Depth=1
	movl	%ebx, 128(%rsp)
	movl	$-1, 132(%rsp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 8(%rbp)
	movq	$_ZTV13CRecordVectorIbE+16, 136(%rsp)
	movdqa	80(%rsp), %xmm0         # 16-byte Reload
	movdqu	%xmm0, 160(%rsp)
	cmpl	$-1, %ebx
	je	.LBB0_17
# BB#14:                                #   in Loop: Header=BB0_9 Depth=1
.Ltmp22:
.Lcfi18:
	.cfi_escape 0x2e, 0x00
	movl	$1, %esi
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp23:
# BB#15:                                #   in Loop: Header=BB0_9 Depth=1
.Ltmp24:
.Lcfi19:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp25:
# BB#16:                                # %_ZN13CRecordVectorIbE3AddEb.exit.i
                                        #   in Loop: Header=BB0_9 Depth=1
	movq	152(%rsp), %rax
	movslq	148(%rsp), %rcx
	movb	$1, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 148(%rsp)
.LBB0_17:                               # %_ZN8NArchive3N7z18CExtractFolderInfoC2Ejj.exit
                                        #   in Loop: Header=BB0_9 Depth=1
.Ltmp30:
.Lcfi20:
	.cfi_escape 0x2e, 0x00
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp31:
# BB#18:                                # %.noexc305
                                        #   in Loop: Header=BB0_9 Depth=1
	movq	128(%rsp), %rax
	movq	%rax, (%r14)
	movq	%r14, %rdi
	addq	$8, %rdi
.Ltmp32:
.Lcfi21:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rsi
	callq	_ZN13CRecordVectorIbEC2ERKS0_
.Ltmp33:
# BB#19:                                #   in Loop: Header=BB0_9 Depth=1
	movq	168(%rsp), %rax
	movq	%rax, 40(%r14)
.Ltmp35:
.Lcfi22:
	.cfi_escape 0x2e, 0x00
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp36:
# BB#20:                                #   in Loop: Header=BB0_9 Depth=1
	movq	40(%rsp), %rax
	movslq	36(%rsp), %rcx
	movq	%r14, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 36(%rsp)
.Ltmp40:
.Lcfi23:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp41:
	jmp	.LBB0_61
.LBB0_34:                               #   in Loop: Header=BB0_9 Depth=1
	xorl	%eax, %eax
	leaq	136(%rsp), %rbp
.LBB0_43:                               #   in Loop: Header=BB0_9 Depth=1
	movslq	36(%rsp), %rcx
	movq	40(%rsp), %rdx
	movq	-8(%rdx,%rcx,8), %r13
	addq	%rax, 56(%rsp)          # 8-byte Folded Spill
	movq	%rax, 40(%r13)
.LBB0_44:                               #   in Loop: Header=BB0_9 Depth=1
	movq	776(%r12), %rax
	movl	20(%r13), %r12d
	subl	(%rax,%r14,4), %ebx
	cmpl	%ebx, %r12d
	jbe	.LBB0_57
# BB#45:                                #   in Loop: Header=BB0_9 Depth=1
	movq	120(%rsp), %r12         # 8-byte Reload
	movl	64(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB0_61
	.p2align	4, 0x90
.LBB0_57:                               # %.lr.ph
                                        #   in Loop: Header=BB0_9 Depth=1
	leaq	8(%r13), %rbp
	.p2align	4, 0x90
.LBB0_58:                               #   Parent Loop BB0_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp19:
.Lcfi24:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp20:
# BB#59:                                #   in Loop: Header=BB0_58 Depth=2
	cmpl	%r12d, %ebx
	movq	24(%r13), %rax
	movslq	20(%r13), %rcx
	sete	(%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 20(%r13)
	incl	%r12d
	cmpl	%ebx, %r12d
	jbe	.LBB0_58
# BB#60:                                #   in Loop: Header=BB0_9 Depth=1
	movq	120(%rsp), %r12         # 8-byte Reload
	movl	64(%rsp), %r13d         # 4-byte Reload
	leaq	136(%rsp), %rbp
	.p2align	4, 0x90
.LBB0_61:                               # %.loopexit360
                                        #   in Loop: Header=BB0_9 Depth=1
	incq	%r15
	cmpq	104(%rsp), %r15         # 8-byte Folded Reload
	jb	.LBB0_9
# BB#62:                                # %._crit_edge
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rax
.Ltmp43:
.Lcfi25:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	*40(%rax)
	movl	%eax, %ebx
.Ltmp44:
# BB#63:
	testl	%ebx, %ebx
	je	.LBB0_64
.LBB0_75:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE+16, 24(%rsp)
.Ltmp123:
.Lcfi26:
	.cfi_escape 0x2e, 0x00
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp124:
# BB#76:
.Ltmp129:
.Lcfi27:
	.cfi_escape 0x2e, 0x00
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp130:
	jmp	.LBB0_6
.LBB0_5:
	xorl	%ebx, %ebx
.LBB0_6:
	testq	%rbp, %rbp
	je	.LBB0_167
# BB#7:
	movq	(%rbp), %rax
.Ltmp135:
.Lcfi28:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	*16(%rax)
.Ltmp136:
	jmp	.LBB0_167
.LBB0_64:
.Ltmp45:
.Lcfi29:
	.cfi_escape 0x2e, 0x00
	leaq	240(%rsp), %rdi
	movl	$1, %esi
	callq	_ZN8NArchive3N7z8CDecoderC1Eb
.Ltmp46:
# BB#65:
.Ltmp48:
.Lcfi30:
	.cfi_escape 0x2e, 0x00
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp49:
# BB#66:
.Ltmp51:
.Lcfi31:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	_ZN14CLocalProgressC1Ev
.Ltmp52:
# BB#67:
	movq	(%r13), %rax
.Ltmp54:
.Lcfi32:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp55:
# BB#68:                                # %_ZN9CMyComPtrI21ICompressProgressInfoEC2EPS0_.exit
.Ltmp57:
.Lcfi33:
	.cfi_escape 0x2e, 0x00
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	_ZN14CLocalProgress4InitEP9IProgressb
.Ltmp58:
# BB#69:                                # %.preheader
	leaq	144(%r12), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%ebp, %ebp
	xorl	%ecx, %ecx
	movq	%r13, 80(%rsp)          # 8-byte Spill
	jmp	.LBB0_70
.LBB0_144:                              #   in Loop: Header=BB0_70 Depth=1
	movq	%rbp, %r14
	incq	%r14
	movq	64(%rsp), %rbp          # 8-byte Reload
	addq	56(%rsp), %rbp          # 8-byte Folded Reload
	movq	104(%rsp), %rcx         # 8-byte Reload
	addq	%r13, %rcx
	movq	%r15, %r12
	movq	80(%rsp), %r13          # 8-byte Reload
.LBB0_70:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_115 Depth 2
                                        #     Child Loop BB0_119 Depth 2
                                        #     Child Loop BB0_122 Depth 2
	movq	%rbp, 56(%r13)
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%rcx, 48(%r13)
.Ltmp60:
.Lcfi34:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	_ZN14CLocalProgress6SetCurEv
.Ltmp61:
# BB#71:                                #   in Loop: Header=BB0_70 Depth=1
	testl	%eax, %eax
	movl	%eax, %ebx
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmovel	%ecx, %ebx
	jne	.LBB0_72
# BB#84:                                #   in Loop: Header=BB0_70 Depth=1
	movslq	36(%rsp), %rax
	cmpq	%rax, %r14
	jge	.LBB0_85
# BB#86:                                #   in Loop: Header=BB0_70 Depth=1
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	%r12, %r15
	movq	40(%rsp), %rax
	movq	(%rax,%r14,8), %rbp
	movq	40(%rbp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
.Ltmp63:
.Lcfi35:
	.cfi_escape 0x2e, 0x00
	movl	$88, %edi
	callq	_Znwm
	movq	%rax, %r12
.Ltmp64:
# BB#87:                                #   in Loop: Header=BB0_70 Depth=1
.Ltmp66:
.Lcfi36:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z16CFolderOutStreamC1Ev
.Ltmp67:
# BB#88:                                #   in Loop: Header=BB0_70 Depth=1
	movq	(%r12), %rax
.Ltmp69:
.Lcfi37:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	*8(%rax)
.Ltmp70:
# BB#89:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit
                                        #   in Loop: Header=BB0_70 Depth=1
	movl	(%rbp), %ecx
	cmpl	$-1, %ecx
	jne	.LBB0_91
# BB#90:                                #   in Loop: Header=BB0_70 Depth=1
	movq	776(%r15), %rax
	movslq	4(%rbp), %rcx
	movl	(%rax,%rcx,4), %ecx
.LBB0_91:                               #   in Loop: Header=BB0_70 Depth=1
	xorl	%eax, %eax
	cmpl	$0, 116(%rsp)           # 4-byte Folded Reload
	setne	%al
	leaq	8(%rbp), %r8
	xorl	%edx, %edx
	cmpl	$0, 12(%r15)
	setne	%dl
.Ltmp72:
.Lcfi38:
	.cfi_escape 0x2e, 0x10
	subq	$16, %rsp
.Lcfi39:
	.cfi_adjust_cfa_offset 16
	movl	%edx, 8(%rsp)
	movl	%eax, (%rsp)
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	200(%rsp), %rsi         # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	callq	_ZN8NArchive3N7z16CFolderOutStream4InitEPKNS0_18CArchiveDatabaseExEjjPK13CRecordVectorIbEP23IArchiveExtractCallbackbb
	addq	$16, %rsp
.Lcfi40:
	.cfi_adjust_cfa_offset -16
.Ltmp73:
# BB#92:                                #   in Loop: Header=BB0_70 Depth=1
	testl	%eax, %eax
	movl	%eax, %edi
	cmovel	%ebx, %edi
	je	.LBB0_102
# BB#93:                                #   in Loop: Header=BB0_70 Depth=1
	movq	%r14, %rbp
	movl	$1, %r14d
	xorl	%r13d, %r13d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jmp	.LBB0_94
.LBB0_102:                              #   in Loop: Header=BB0_70 Depth=1
	cmpl	$-1, (%rbp)
	je	.LBB0_104
# BB#103:                               #   in Loop: Header=BB0_70 Depth=1
	movq	%r14, %rbp
	movl	$10, %r14d
	xorl	%r13d, %r13d
	movl	%ebx, %eax
.LBB0_94:                               #   in Loop: Header=BB0_70 Depth=1
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB0_95:                               #   in Loop: Header=BB0_70 Depth=1
	movq	(%r12), %rax
.Ltmp102:
.Lcfi41:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp103:
# BB#96:                                # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit336
                                        #   in Loop: Header=BB0_70 Depth=1
	cmpl	$10, %r14d
	je	.LBB0_144
# BB#97:                                # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit336
                                        #   in Loop: Header=BB0_70 Depth=1
	testl	%r14d, %r14d
	je	.LBB0_144
	jmp	.LBB0_98
.LBB0_104:                              #   in Loop: Header=BB0_70 Depth=1
	movslq	4(%rbp), %rax
	movq	%r15, %rsi
	movq	256(%rsi), %rcx
	movq	744(%rsi), %rdx
	movq	(%rcx,%rax,8), %r8
	movl	(%rdx,%rax,4), %edx
	movslq	%edx, %rcx
	movslq	76(%r8), %rbp
	testq	%rbp, %rbp
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	jle	.LBB0_105
# BB#106:                               # %.lr.ph.i
                                        #   in Loop: Header=BB0_70 Depth=1
	movq	160(%rsi), %rbx
	cmpl	$4, %ebp
	jb	.LBB0_107
# BB#108:                               # %min.iters.checked
                                        #   in Loop: Header=BB0_70 Depth=1
	movq	%rbp, %rsi
	andq	$-4, %rsi
	je	.LBB0_107
# BB#109:                               # %vector.scevcheck
                                        #   in Loop: Header=BB0_70 Depth=1
	leaq	-1(%rbp), %rax
	leal	(%rdx,%rax), %edi
	cmpl	%edx, %edi
	jl	.LBB0_107
# BB#110:                               # %vector.scevcheck
                                        #   in Loop: Header=BB0_70 Depth=1
	shrq	$32, %rax
	movl	$0, %edi
	movl	$0, %r13d
	jne	.LBB0_117
# BB#111:                               # %vector.body.preheader
                                        #   in Loop: Header=BB0_70 Depth=1
	leaq	-4(%rsi), %rdi
	movq	%rdi, %rax
	shrq	$2, %rax
	btl	$2, %edi
	jb	.LBB0_112
# BB#113:                               # %vector.body.prol
                                        #   in Loop: Header=BB0_70 Depth=1
	movdqu	(%rbx,%rcx,8), %xmm0
	movdqu	16(%rbx,%rcx,8), %xmm1
	movl	$4, %edi
	testq	%rax, %rax
	jne	.LBB0_115
	jmp	.LBB0_116
.LBB0_107:                              #   in Loop: Header=BB0_70 Depth=1
	xorl	%edi, %edi
	xorl	%r13d, %r13d
.LBB0_117:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_70 Depth=1
	movl	%ebp, %eax
	subl	%edi, %eax
	leaq	-1(%rbp), %rsi
	subq	%rdi, %rsi
	andq	$3, %rax
	je	.LBB0_120
# BB#118:                               # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB0_70 Depth=1
	leal	(%rdi,%rdx), %edx
	negq	%rax
.LBB0_119:                              # %scalar.ph.prol
                                        #   Parent Loop BB0_70 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rdx
	addq	(%rbx,%rdx,8), %r13
	incq	%rdi
	incl	%edx
	incq	%rax
	jne	.LBB0_119
.LBB0_120:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB0_70 Depth=1
	cmpq	$3, %rsi
	jb	.LBB0_123
# BB#121:                               # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB0_70 Depth=1
	subq	%rdi, %rbp
	movl	%ecx, %eax
	leaq	3(%rax,%rdi), %rax
	xorl	%edx, %edx
.LBB0_122:                              # %scalar.ph
                                        #   Parent Loop BB0_70 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-3(%rax,%rdx), %esi
	movslq	%esi, %rsi
	addq	(%rbx,%rsi,8), %r13
	leal	-2(%rax,%rdx), %esi
	movslq	%esi, %rsi
	addq	(%rbx,%rsi,8), %r13
	leal	-1(%rax,%rdx), %esi
	movslq	%esi, %rsi
	addq	(%rbx,%rsi,8), %r13
	leal	(%rax,%rdx), %esi
	movslq	%esi, %rsi
	addq	(%rbx,%rsi,8), %r13
	addq	$4, %rdx
	cmpq	%rdx, %rbp
	jne	.LBB0_122
	jmp	.LBB0_123
.LBB0_105:                              #   in Loop: Header=BB0_70 Depth=1
	xorl	%r13d, %r13d
.LBB0_123:                              # %.loopexit
                                        #   in Loop: Header=BB0_70 Depth=1
	movq	%r15, %rdx
	movq	712(%rdx), %rax
	movq	(%rax,%rcx,8), %rbx
	addq	648(%rdx), %rbx
	movq	16(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	movq	$0, 72(%rsp)
	je	.LBB0_124
# BB#125:                               #   in Loop: Header=BB0_70 Depth=1
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movq	%r8, %rbp
	movq	(%rdi), %rax
.Ltmp75:
.Lcfi42:
	.cfi_escape 0x2e, 0x00
	movl	$IID_ICryptoGetTextPassword, %esi
	leaq	72(%rsp), %rdx
	callq	*(%rax)
.Ltmp76:
# BB#126:                               # %._ZNK9CMyComPtrI23IArchiveExtractCallbackE14QueryInterfaceI22ICryptoGetTextPasswordEEiRK4GUIDPPT_.exit_crit_edge
                                        #   in Loop: Header=BB0_70 Depth=1
	movq	72(%rsp), %rax
	movq	%rbp, %r8
	movq	176(%rsp), %rcx         # 8-byte Reload
	jmp	.LBB0_127
.LBB0_124:                              #   in Loop: Header=BB0_70 Depth=1
	xorl	%eax, %eax
.LBB0_127:                              # %_ZNK9CMyComPtrI23IArchiveExtractCallbackE14QueryInterfaceI22ICryptoGetTextPasswordEEiRK4GUIDPPT_.exit
                                        #   in Loop: Header=BB0_70 Depth=1
	movq	%r15, %rdx
	movq	136(%rdx), %rsi
	shlq	$3, %rcx
	addq	160(%rdx), %rcx
	movl	8(%rdx), %ebp
.Ltmp77:
.Lcfi43:
	.cfi_escape 0x2e, 0x30
	subq	$8, %rsp
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	leaq	248(%rsp), %rdi
	movq	%rbx, %rdx
	movq	%r12, %r9
	pushq	%rbp
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	leaq	152(%rsp), %rbp
	pushq	%rbp
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)               # 8-byte Folded Reload
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	callq	_ZN8NArchive3N7z8CDecoder6DecodeEP9IInStreamyPKyRKNS0_7CFolderEP20ISequentialOutStreamP21ICompressProgressInfoP22ICryptoGetTextPasswordRbbj
	addq	$48, %rsp
.Lcfi50:
	.cfi_adjust_cfa_offset -48
.Ltmp78:
# BB#128:                               #   in Loop: Header=BB0_70 Depth=1
	cmpl	$-2147467263, %eax      # imm = 0x80004001
	je	.LBB0_134
# BB#129:                               #   in Loop: Header=BB0_70 Depth=1
	movq	%r14, %rbp
	testl	%eax, %eax
	je	.LBB0_135
# BB#130:                               #   in Loop: Header=BB0_70 Depth=1
	cmpl	$1, %eax
	jne	.LBB0_131
# BB#132:                               #   in Loop: Header=BB0_70 Depth=1
.Ltmp84:
.Lcfi51:
	.cfi_escape 0x2e, 0x00
	movl	$2, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z16CFolderOutStream14FlushCorruptedEi
.Ltmp85:
	jmp	.LBB0_137
.LBB0_135:                              #   in Loop: Header=BB0_70 Depth=1
	movl	72(%r12), %eax
	movq	48(%r12), %rcx
	xorl	%r14d, %r14d
	cmpl	12(%rcx), %eax
	je	.LBB0_141
# BB#136:                               #   in Loop: Header=BB0_70 Depth=1
.Ltmp80:
.Lcfi52:
	.cfi_escape 0x2e, 0x00
	movl	$2, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z16CFolderOutStream14FlushCorruptedEi
.Ltmp81:
	jmp	.LBB0_137
.LBB0_134:                              #   in Loop: Header=BB0_70 Depth=1
.Ltmp82:
	movq	%r14, %rbp
.Lcfi53:
	.cfi_escape 0x2e, 0x00
	movl	$1, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z16CFolderOutStream14FlushCorruptedEi
.Ltmp83:
.LBB0_137:                              #   in Loop: Header=BB0_70 Depth=1
	xorl	%r14d, %r14d
	testl	%eax, %eax
	setne	%r14b
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmovnel	%eax, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	$10, %eax
	cmovel	%eax, %r14d
	jmp	.LBB0_141
.LBB0_131:                              #   in Loop: Header=BB0_70 Depth=1
	movl	$1, %r14d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB0_141:                              #   in Loop: Header=BB0_70 Depth=1
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_95
# BB#142:                               #   in Loop: Header=BB0_70 Depth=1
	movq	(%rdi), %rax
.Ltmp97:
.Lcfi54:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp98:
	jmp	.LBB0_95
.LBB0_112:                              #   in Loop: Header=BB0_70 Depth=1
	pxor	%xmm0, %xmm0
	xorl	%edi, %edi
	pxor	%xmm1, %xmm1
	testq	%rax, %rax
	je	.LBB0_116
.LBB0_115:                              # %vector.body
                                        #   Parent Loop BB0_70 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rdx,%rdi), %rax
	cltq
	movdqu	(%rbx,%rax,8), %xmm2
	movdqu	16(%rbx,%rax,8), %xmm3
	paddq	%xmm0, %xmm2
	paddq	%xmm1, %xmm3
	addl	$4, %eax
	cltq
	movdqu	(%rbx,%rax,8), %xmm0
	movdqu	16(%rbx,%rax,8), %xmm1
	paddq	%xmm2, %xmm0
	paddq	%xmm3, %xmm1
	addq	$8, %rdi
	cmpq	%rdi, %rsi
	jne	.LBB0_115
.LBB0_116:                              # %middle.block
                                        #   in Loop: Header=BB0_70 Depth=1
	paddq	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddq	%xmm0, %xmm1
	movd	%xmm1, %r13
	cmpq	%rsi, %rbp
	movq	%rsi, %rdi
	jne	.LBB0_117
	jmp	.LBB0_123
.LBB0_133:                              #   in Loop: Header=BB0_70 Depth=1
.Ltmp86:
	jmp	.LBB0_139
.LBB0_138:                              #   in Loop: Header=BB0_70 Depth=1
.Ltmp79:
	movq	%r14, %rbp
.LBB0_139:                              #   in Loop: Header=BB0_70 Depth=1
.Lcfi55:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__cxa_begin_catch
.Ltmp87:
.Lcfi56:
	.cfi_escape 0x2e, 0x00
	movl	$2, %esi
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z16CFolderOutStream14FlushCorruptedEi
.Ltmp88:
# BB#140:                               #   in Loop: Header=BB0_70 Depth=1
	xorl	%r14d, %r14d
	testl	%eax, %eax
	setne	%r14b
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmovnel	%eax, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	$10, %eax
	cmovel	%eax, %r14d
.Ltmp92:
.Lcfi57:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
.Ltmp93:
	jmp	.LBB0_141
.LBB0_72:
	movl	$1, %r14d
	movl	%eax, %ecx
	jmp	.LBB0_73
.LBB0_85:
	movl	$8, %r14d
	jmp	.LBB0_73
.LBB0_98:
	movq	80(%rsp), %r13          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB0_73:
	xorl	%ebx, %ebx
	cmpl	$8, %r14d
	cmovnel	%ecx, %ebx
	movq	(%r13), %rax
.Ltmp107:
.Lcfi58:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp108:
# BB#74:                                # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit332
.Ltmp112:
.Lcfi59:
	.cfi_escape 0x2e, 0x00
	leaq	240(%rsp), %rdi
	movq	16(%rsp), %rbp          # 8-byte Reload
	callq	_ZN8NArchive3N7z8CDecoderD2Ev
.Ltmp113:
	jmp	.LBB0_75
.LBB0_46:
.Lcfi60:
	.cfi_escape 0x2e, 0x00
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1, (%rax)
.Ltmp16:
.Lcfi61:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp17:
# BB#47:                                # %.noexc319
.LBB0_51:
.Ltmp18:
	jmp	.LBB0_52
.LBB0_143:
.Ltmp89:
	movq	%rdx, %r15
	movq	%rax, %rbx
.Ltmp90:
.Lcfi62:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
.Ltmp91:
	jmp	.LBB0_147
.LBB0_155:
.Ltmp114:
	jmp	.LBB0_52
.LBB0_151:
.Ltmp109:
	jmp	.LBB0_78
.LBB0_81:
.Ltmp59:
	jmp	.LBB0_82
.LBB0_80:
.Ltmp56:
	jmp	.LBB0_78
.LBB0_79:
.Ltmp53:
	movq	%rdx, %r15
	movq	%rax, %rbx
.Lcfi63:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	_ZdlPv
	jmp	.LBB0_154
.LBB0_77:
.Ltmp50:
.LBB0_78:                               # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	movq	%rdx, %r15
	movq	%rax, %rbx
	jmp	.LBB0_154
.LBB0_158:
.Ltmp131:
	movq	%rdx, %r15
	movq	%rax, %rbx
	jmp	.LBB0_55
.LBB0_156:
.Ltmp125:
	movq	%rdx, %r15
	movq	%rax, %rbx
.Ltmp126:
.Lcfi64:
	.cfi_escape 0x2e, 0x00
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp127:
	jmp	.LBB0_55
.LBB0_157:
.Ltmp128:
.Lcfi65:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_174:
.Ltmp47:
	jmp	.LBB0_52
.LBB0_161:
.Ltmp137:
	jmp	.LBB0_162
.LBB0_171:
.Ltmp2:
.LBB0_162:                              # %_ZN9CMyComPtrI23IArchiveExtractCallbackED2Ev.exit
	movq	%rdx, %r15
	movq	%rax, %rbx
	jmp	.LBB0_163
.LBB0_145:
.Ltmp99:
	movq	%rdx, %r15
	movq	%rax, %rbx
	movq	80(%rsp), %r13          # 8-byte Reload
	jmp	.LBB0_149
.LBB0_146:
.Ltmp94:
	movq	%rdx, %r15
	movq	%rax, %rbx
.LBB0_147:
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	movq	80(%rsp), %r13          # 8-byte Reload
	je	.LBB0_149
# BB#148:
	movq	(%rdi), %rax
.Ltmp95:
.Lcfi66:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp96:
	jmp	.LBB0_149
.LBB0_150:
.Ltmp104:
	movq	%rdx, %r15
	movq	%rax, %rbx
	movq	80(%rsp), %r13          # 8-byte Reload
	jmp	.LBB0_153
.LBB0_101:
.Ltmp74:
	movq	%rdx, %r15
	movq	%rax, %rbx
.LBB0_149:
	movq	(%r12), %rax
.Ltmp100:
.Lcfi67:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp101:
	jmp	.LBB0_153
.LBB0_100:
.Ltmp71:
	jmp	.LBB0_82
.LBB0_152:
.Ltmp68:
	movq	%rdx, %r15
	movq	%rax, %rbx
.Lcfi68:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZdlPv
	jmp	.LBB0_153
.LBB0_99:
.Ltmp65:
	jmp	.LBB0_82
.LBB0_83:
.Ltmp62:
.LBB0_82:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	%rdx, %r15
	movq	%rax, %rbx
.LBB0_153:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	(%r13), %rax
.Ltmp105:
.Lcfi69:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp106:
.LBB0_154:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
.Ltmp110:
.Lcfi70:
	.cfi_escape 0x2e, 0x00
	leaq	240(%rsp), %rdi
	callq	_ZN8NArchive3N7z8CDecoderD2Ev
.Ltmp111:
	jmp	.LBB0_53
.LBB0_24:
.Ltmp42:
	jmp	.LBB0_52
.LBB0_23:
.Ltmp34:
	movq	%rdx, %r15
	movq	%rax, %rbx
.Lcfi71:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZdlPv
	jmp	.LBB0_26
.LBB0_48:
.Ltmp15:
	jmp	.LBB0_52
.LBB0_172:
.Ltmp7:
	movq	%rdx, %r15
	movq	%rax, %rbx
.Lcfi72:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB0_50
.LBB0_21:
.Ltmp26:
	movq	%rdx, %r15
	movq	%rax, %rbx
.Ltmp27:
.Lcfi73:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp28:
	jmp	.LBB0_53
.LBB0_22:
.Ltmp29:
.Lcfi74:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_25:
.Ltmp37:
	movq	%rdx, %r15
	movq	%rax, %rbx
.LBB0_26:                               # %.body307
.Ltmp38:
.Lcfi75:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp39:
	jmp	.LBB0_53
.LBB0_49:
.Ltmp10:
	movq	%rdx, %r15
	movq	%rax, %rbx
.LBB0_50:                               # %.body314
.Ltmp11:
.Lcfi76:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp12:
	jmp	.LBB0_53
.LBB0_173:
.Ltmp21:
.LBB0_52:
	movq	%rdx, %r15
	movq	%rax, %rbx
.LBB0_53:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE+16, 24(%rsp)
.Ltmp115:
.Lcfi77:
	.cfi_escape 0x2e, 0x00
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp116:
# BB#54:
.Ltmp121:
.Lcfi78:
	.cfi_escape 0x2e, 0x00
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp122:
.LBB0_55:
	movq	16(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB0_163
# BB#56:
	movq	(%rdi), %rax
.Ltmp132:
.Lcfi79:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp133:
.LBB0_163:                              # %_ZN9CMyComPtrI23IArchiveExtractCallbackED2Ev.exit
.Lcfi80:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r15d
	je	.LBB0_164
# BB#166:
.Lcfi81:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
	movl	$-2147024882, %ebx      # imm = 0x8007000E
.LBB0_167:                              # %_ZN9CMyComPtrI23IArchiveExtractCallbackED2Ev.exit301
	movl	%ebx, %eax
	addq	$472, %rsp              # imm = 0x1D8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_164:
.Lcfi82:
	.cfi_escape 0x2e, 0x00
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp138:
.Lcfi83:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp139:
# BB#170:
.LBB0_165:
.Ltmp140:
	movq	%rax, %rbx
.Lcfi84:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
.Lcfi85:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_159:
.Ltmp117:
	movq	%rax, %rbx
.Ltmp118:
.Lcfi86:
	.cfi_escape 0x2e, 0x00
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp119:
	jmp	.LBB0_169
.LBB0_160:
.Ltmp120:
.Lcfi87:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_168:
.Ltmp134:
	movq	%rax, %rbx
.LBB0_169:                              # %.body
.Lcfi88:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN8NArchive3N7z8CHandler7ExtractEPKjjiP23IArchiveExtractCallback, .Lfunc_end0-_ZN8NArchive3N7z8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\377\204"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\360\004"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	3                       #   On action: 2
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	3                       #   On action: 2
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	3                       #   On action: 2
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	3                       #   On action: 2
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin0   #     jumps to .Ltmp15
	.byte	3                       #   On action: 2
	.long	.Ltmp22-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp25-.Ltmp22         #   Call between .Ltmp22 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin0   #     jumps to .Ltmp26
	.byte	3                       #   On action: 2
	.long	.Ltmp30-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp37-.Lfunc_begin0   #     jumps to .Ltmp37
	.byte	3                       #   On action: 2
	.long	.Ltmp32-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin0   #     jumps to .Ltmp34
	.byte	3                       #   On action: 2
	.long	.Ltmp35-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin0   #     jumps to .Ltmp37
	.byte	3                       #   On action: 2
	.long	.Ltmp40-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin0   #     jumps to .Ltmp42
	.byte	3                       #   On action: 2
	.long	.Ltmp19-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin0   #     jumps to .Ltmp21
	.byte	3                       #   On action: 2
	.long	.Ltmp43-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp47-.Lfunc_begin0   #     jumps to .Ltmp47
	.byte	3                       #   On action: 2
	.long	.Ltmp123-.Lfunc_begin0  # >> Call Site 13 <<
	.long	.Ltmp124-.Ltmp123       #   Call between .Ltmp123 and .Ltmp124
	.long	.Ltmp125-.Lfunc_begin0  #     jumps to .Ltmp125
	.byte	3                       #   On action: 2
	.long	.Ltmp129-.Lfunc_begin0  # >> Call Site 14 <<
	.long	.Ltmp130-.Ltmp129       #   Call between .Ltmp129 and .Ltmp130
	.long	.Ltmp131-.Lfunc_begin0  #     jumps to .Ltmp131
	.byte	3                       #   On action: 2
	.long	.Ltmp135-.Lfunc_begin0  # >> Call Site 15 <<
	.long	.Ltmp136-.Ltmp135       #   Call between .Ltmp135 and .Ltmp136
	.long	.Ltmp137-.Lfunc_begin0  #     jumps to .Ltmp137
	.byte	3                       #   On action: 2
	.long	.Ltmp45-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin0   #     jumps to .Ltmp47
	.byte	3                       #   On action: 2
	.long	.Ltmp48-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin0   #     jumps to .Ltmp50
	.byte	3                       #   On action: 2
	.long	.Ltmp51-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin0   #     jumps to .Ltmp53
	.byte	3                       #   On action: 2
	.long	.Ltmp54-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin0   #     jumps to .Ltmp56
	.byte	3                       #   On action: 2
	.long	.Ltmp57-.Lfunc_begin0   # >> Call Site 20 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin0   #     jumps to .Ltmp59
	.byte	3                       #   On action: 2
	.long	.Ltmp60-.Lfunc_begin0   # >> Call Site 21 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin0   #     jumps to .Ltmp62
	.byte	3                       #   On action: 2
	.long	.Ltmp63-.Lfunc_begin0   # >> Call Site 22 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin0   #     jumps to .Ltmp65
	.byte	3                       #   On action: 2
	.long	.Ltmp66-.Lfunc_begin0   # >> Call Site 23 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin0   #     jumps to .Ltmp68
	.byte	3                       #   On action: 2
	.long	.Ltmp69-.Lfunc_begin0   # >> Call Site 24 <<
	.long	.Ltmp70-.Ltmp69         #   Call between .Ltmp69 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin0   #     jumps to .Ltmp71
	.byte	3                       #   On action: 2
	.long	.Ltmp72-.Lfunc_begin0   # >> Call Site 25 <<
	.long	.Ltmp73-.Ltmp72         #   Call between .Ltmp72 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin0   #     jumps to .Ltmp74
	.byte	3                       #   On action: 2
	.long	.Ltmp102-.Lfunc_begin0  # >> Call Site 26 <<
	.long	.Ltmp103-.Ltmp102       #   Call between .Ltmp102 and .Ltmp103
	.long	.Ltmp104-.Lfunc_begin0  #     jumps to .Ltmp104
	.byte	3                       #   On action: 2
	.long	.Ltmp75-.Lfunc_begin0   # >> Call Site 27 <<
	.long	.Ltmp76-.Ltmp75         #   Call between .Ltmp75 and .Ltmp76
	.long	.Ltmp94-.Lfunc_begin0   #     jumps to .Ltmp94
	.byte	3                       #   On action: 2
	.long	.Ltmp77-.Lfunc_begin0   # >> Call Site 28 <<
	.long	.Ltmp78-.Ltmp77         #   Call between .Ltmp77 and .Ltmp78
	.long	.Ltmp79-.Lfunc_begin0   #     jumps to .Ltmp79
	.byte	1                       #   On action: 1
	.long	.Ltmp84-.Lfunc_begin0   # >> Call Site 29 <<
	.long	.Ltmp83-.Ltmp84         #   Call between .Ltmp84 and .Ltmp83
	.long	.Ltmp86-.Lfunc_begin0   #     jumps to .Ltmp86
	.byte	1                       #   On action: 1
	.long	.Ltmp97-.Lfunc_begin0   # >> Call Site 30 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin0   #     jumps to .Ltmp99
	.byte	3                       #   On action: 2
	.long	.Ltmp98-.Lfunc_begin0   # >> Call Site 31 <<
	.long	.Ltmp87-.Ltmp98         #   Call between .Ltmp98 and .Ltmp87
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp87-.Lfunc_begin0   # >> Call Site 32 <<
	.long	.Ltmp88-.Ltmp87         #   Call between .Ltmp87 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin0   #     jumps to .Ltmp89
	.byte	3                       #   On action: 2
	.long	.Ltmp92-.Lfunc_begin0   # >> Call Site 33 <<
	.long	.Ltmp93-.Ltmp92         #   Call between .Ltmp92 and .Ltmp93
	.long	.Ltmp94-.Lfunc_begin0   #     jumps to .Ltmp94
	.byte	3                       #   On action: 2
	.long	.Ltmp107-.Lfunc_begin0  # >> Call Site 34 <<
	.long	.Ltmp108-.Ltmp107       #   Call between .Ltmp107 and .Ltmp108
	.long	.Ltmp109-.Lfunc_begin0  #     jumps to .Ltmp109
	.byte	3                       #   On action: 2
	.long	.Ltmp112-.Lfunc_begin0  # >> Call Site 35 <<
	.long	.Ltmp113-.Ltmp112       #   Call between .Ltmp112 and .Ltmp113
	.long	.Ltmp114-.Lfunc_begin0  #     jumps to .Ltmp114
	.byte	3                       #   On action: 2
	.long	.Ltmp113-.Lfunc_begin0  # >> Call Site 36 <<
	.long	.Ltmp16-.Ltmp113        #   Call between .Ltmp113 and .Ltmp16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 37 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin0   #     jumps to .Ltmp18
	.byte	3                       #   On action: 2
	.long	.Ltmp90-.Lfunc_begin0   # >> Call Site 38 <<
	.long	.Ltmp91-.Ltmp90         #   Call between .Ltmp90 and .Ltmp91
	.long	.Ltmp134-.Lfunc_begin0  #     jumps to .Ltmp134
	.byte	1                       #   On action: 1
	.long	.Ltmp126-.Lfunc_begin0  # >> Call Site 39 <<
	.long	.Ltmp127-.Ltmp126       #   Call between .Ltmp126 and .Ltmp127
	.long	.Ltmp128-.Lfunc_begin0  #     jumps to .Ltmp128
	.byte	1                       #   On action: 1
	.long	.Ltmp95-.Lfunc_begin0   # >> Call Site 40 <<
	.long	.Ltmp111-.Ltmp95        #   Call between .Ltmp95 and .Ltmp111
	.long	.Ltmp134-.Lfunc_begin0  #     jumps to .Ltmp134
	.byte	1                       #   On action: 1
	.long	.Ltmp27-.Lfunc_begin0   # >> Call Site 41 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin0   #     jumps to .Ltmp29
	.byte	1                       #   On action: 1
	.long	.Ltmp38-.Lfunc_begin0   # >> Call Site 42 <<
	.long	.Ltmp12-.Ltmp38         #   Call between .Ltmp38 and .Ltmp12
	.long	.Ltmp134-.Lfunc_begin0  #     jumps to .Ltmp134
	.byte	1                       #   On action: 1
	.long	.Ltmp115-.Lfunc_begin0  # >> Call Site 43 <<
	.long	.Ltmp116-.Ltmp115       #   Call between .Ltmp115 and .Ltmp116
	.long	.Ltmp117-.Lfunc_begin0  #     jumps to .Ltmp117
	.byte	1                       #   On action: 1
	.long	.Ltmp121-.Lfunc_begin0  # >> Call Site 44 <<
	.long	.Ltmp133-.Ltmp121       #   Call between .Ltmp121 and .Ltmp133
	.long	.Ltmp134-.Lfunc_begin0  #     jumps to .Ltmp134
	.byte	1                       #   On action: 1
	.long	.Ltmp133-.Lfunc_begin0  # >> Call Site 45 <<
	.long	.Ltmp138-.Ltmp133       #   Call between .Ltmp133 and .Ltmp138
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp138-.Lfunc_begin0  # >> Call Site 46 <<
	.long	.Ltmp139-.Ltmp138       #   Call between .Ltmp138 and .Ltmp139
	.long	.Ltmp140-.Lfunc_begin0  #     jumps to .Ltmp140
	.byte	0                       #   On action: cleanup
	.long	.Ltmp139-.Lfunc_begin0  # >> Call Site 47 <<
	.long	.Ltmp118-.Ltmp139       #   Call between .Ltmp139 and .Ltmp118
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp118-.Lfunc_begin0  # >> Call Site 48 <<
	.long	.Ltmp119-.Ltmp118       #   Call between .Ltmp118 and .Ltmp119
	.long	.Ltmp120-.Lfunc_begin0  #     jumps to .Ltmp120
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.section	.text._ZN8NArchive3N7z8CDecoderD2Ev,"axG",@progbits,_ZN8NArchive3N7z8CDecoderD2Ev,comdat
	.weak	_ZN8NArchive3N7z8CDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CDecoderD2Ev,@function
_ZN8NArchive3N7z8CDecoderD2Ev:          # @_ZN8NArchive3N7z8CDecoderD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi91:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi93:
	.cfi_def_cfa_offset 48
.Lcfi94:
	.cfi_offset %rbx, -40
.Lcfi95:
	.cfi_offset %r12, -32
.Lcfi96:
	.cfi_offset %r14, -24
.Lcfi97:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	leaq	200(%r12), %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE+16, 200(%r12)
.Ltmp141:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp142:
# BB#1:
.Ltmp147:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp148:
# BB#2:                                 # %_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev.exit
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp152:
	callq	*16(%rax)
.Ltmp153:
.LBB2_4:                                # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit
	leaq	8(%r12), %r15
	leaq	136(%r12), %rdi
.Ltmp196:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp197:
# BB#5:                                 # %_ZN8NArchive3N7z11CBindInfoExD2Ev.exit
	leaq	104(%r12), %rdi
.Ltmp218:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp219:
# BB#6:
	leaq	72(%r12), %rdi
.Ltmp223:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp224:
# BB#7:
	addq	$40, %r12
.Ltmp228:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp229:
# BB#8:                                 # %_ZN11NCoderMixer9CBindInfoD2Ev.exit
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB2_33:
.Ltmp154:
	movq	%rax, %r14
	jmp	.LBB2_34
.LBB2_25:
.Ltmp230:
	movq	%rax, %r14
	jmp	.LBB2_28
.LBB2_26:
.Ltmp225:
	movq	%rax, %r14
	jmp	.LBB2_27
.LBB2_24:
.Ltmp220:
	movq	%rax, %r14
	leaq	72(%r12), %rdi
.Ltmp221:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp222:
.LBB2_27:
	addq	$40, %r12
.Ltmp226:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp227:
.LBB2_28:
.Ltmp231:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp232:
	jmp	.LBB2_15
.LBB2_29:
.Ltmp233:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_11:
.Ltmp198:
	movq	%rax, %r14
	leaq	104(%r12), %rdi
.Ltmp199:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp200:
# BB#12:
	leaq	72(%r12), %rdi
.Ltmp204:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp205:
# BB#13:
	addq	$40, %r12
.Ltmp209:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp210:
# BB#14:
.Ltmp215:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp216:
	jmp	.LBB2_15
.LBB2_22:
.Ltmp217:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB2_17:
.Ltmp211:
	movq	%rax, %r14
	jmp	.LBB2_20
.LBB2_18:
.Ltmp206:
	movq	%rax, %r14
	jmp	.LBB2_19
.LBB2_16:
.Ltmp201:
	movq	%rax, %r14
	leaq	72(%r12), %rdi
.Ltmp202:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp203:
.LBB2_19:
	addq	$40, %r12
.Ltmp207:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp208:
.LBB2_20:
.Ltmp212:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp213:
# BB#23:                                # %.body14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB2_21:
.Ltmp214:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_30:
.Ltmp149:
	movq	%rax, %r14
	jmp	.LBB2_31
.LBB2_9:
.Ltmp143:
	movq	%rax, %r14
.Ltmp144:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp145:
.LBB2_31:                               # %.body
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB2_34
# BB#32:
	movq	(%rdi), %rax
.Ltmp150:
	callq	*16(%rax)
.Ltmp151:
.LBB2_34:                               # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit4
	leaq	8(%r12), %r15
	leaq	136(%r12), %rdi
.Ltmp155:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp156:
# BB#35:
	leaq	104(%r12), %rdi
.Ltmp177:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp178:
# BB#36:
	leaq	72(%r12), %rdi
.Ltmp182:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp183:
# BB#37:
	addq	$40, %r12
.Ltmp187:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp188:
# BB#38:
.Ltmp193:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp194:
.LBB2_15:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_10:
.Ltmp146:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_40:
.Ltmp189:
	movq	%rax, %r14
	jmp	.LBB2_43
.LBB2_41:
.Ltmp184:
	movq	%rax, %r14
	jmp	.LBB2_42
.LBB2_39:
.Ltmp179:
	movq	%rax, %r14
	leaq	72(%r12), %rdi
.Ltmp180:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp181:
.LBB2_42:
	addq	$40, %r12
.Ltmp185:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp186:
.LBB2_43:
.Ltmp190:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp191:
	jmp	.LBB2_58
.LBB2_44:
.Ltmp192:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_45:
.Ltmp157:
	movq	%rax, %r14
	leaq	104(%r12), %rdi
.Ltmp158:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp159:
# BB#46:
	leaq	72(%r12), %rdi
.Ltmp163:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp164:
# BB#47:
	addq	$40, %r12
.Ltmp168:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp169:
# BB#48:
.Ltmp174:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp175:
	jmp	.LBB2_58
.LBB2_55:
.Ltmp176:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB2_50:
.Ltmp170:
	movq	%rax, %r14
	jmp	.LBB2_53
.LBB2_51:
.Ltmp165:
	movq	%rax, %r14
	jmp	.LBB2_52
.LBB2_49:
.Ltmp160:
	movq	%rax, %r14
	leaq	72(%r12), %rdi
.Ltmp161:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp162:
.LBB2_52:
	addq	$40, %r12
.Ltmp166:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp167:
.LBB2_53:
.Ltmp171:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp172:
# BB#56:                                # %.body30
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB2_54:
.Ltmp173:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_57:
.Ltmp195:
	movq	%rax, %r14
.LBB2_58:                               # %.body6
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN8NArchive3N7z8CDecoderD2Ev, .Lfunc_end2-_ZN8NArchive3N7z8CDecoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\365\202\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\354\002"              # Call site table length
	.long	.Ltmp141-.Lfunc_begin1  # >> Call Site 1 <<
	.long	.Ltmp142-.Ltmp141       #   Call between .Ltmp141 and .Ltmp142
	.long	.Ltmp143-.Lfunc_begin1  #     jumps to .Ltmp143
	.byte	0                       #   On action: cleanup
	.long	.Ltmp147-.Lfunc_begin1  # >> Call Site 2 <<
	.long	.Ltmp148-.Ltmp147       #   Call between .Ltmp147 and .Ltmp148
	.long	.Ltmp149-.Lfunc_begin1  #     jumps to .Ltmp149
	.byte	0                       #   On action: cleanup
	.long	.Ltmp152-.Lfunc_begin1  # >> Call Site 3 <<
	.long	.Ltmp153-.Ltmp152       #   Call between .Ltmp152 and .Ltmp153
	.long	.Ltmp154-.Lfunc_begin1  #     jumps to .Ltmp154
	.byte	0                       #   On action: cleanup
	.long	.Ltmp196-.Lfunc_begin1  # >> Call Site 4 <<
	.long	.Ltmp197-.Ltmp196       #   Call between .Ltmp196 and .Ltmp197
	.long	.Ltmp198-.Lfunc_begin1  #     jumps to .Ltmp198
	.byte	0                       #   On action: cleanup
	.long	.Ltmp218-.Lfunc_begin1  # >> Call Site 5 <<
	.long	.Ltmp219-.Ltmp218       #   Call between .Ltmp218 and .Ltmp219
	.long	.Ltmp220-.Lfunc_begin1  #     jumps to .Ltmp220
	.byte	0                       #   On action: cleanup
	.long	.Ltmp223-.Lfunc_begin1  # >> Call Site 6 <<
	.long	.Ltmp224-.Ltmp223       #   Call between .Ltmp223 and .Ltmp224
	.long	.Ltmp225-.Lfunc_begin1  #     jumps to .Ltmp225
	.byte	0                       #   On action: cleanup
	.long	.Ltmp228-.Lfunc_begin1  # >> Call Site 7 <<
	.long	.Ltmp229-.Ltmp228       #   Call between .Ltmp228 and .Ltmp229
	.long	.Ltmp230-.Lfunc_begin1  #     jumps to .Ltmp230
	.byte	0                       #   On action: cleanup
	.long	.Ltmp229-.Lfunc_begin1  # >> Call Site 8 <<
	.long	.Ltmp221-.Ltmp229       #   Call between .Ltmp229 and .Ltmp221
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp221-.Lfunc_begin1  # >> Call Site 9 <<
	.long	.Ltmp232-.Ltmp221       #   Call between .Ltmp221 and .Ltmp232
	.long	.Ltmp233-.Lfunc_begin1  #     jumps to .Ltmp233
	.byte	1                       #   On action: 1
	.long	.Ltmp199-.Lfunc_begin1  # >> Call Site 10 <<
	.long	.Ltmp200-.Ltmp199       #   Call between .Ltmp199 and .Ltmp200
	.long	.Ltmp201-.Lfunc_begin1  #     jumps to .Ltmp201
	.byte	1                       #   On action: 1
	.long	.Ltmp204-.Lfunc_begin1  # >> Call Site 11 <<
	.long	.Ltmp205-.Ltmp204       #   Call between .Ltmp204 and .Ltmp205
	.long	.Ltmp206-.Lfunc_begin1  #     jumps to .Ltmp206
	.byte	1                       #   On action: 1
	.long	.Ltmp209-.Lfunc_begin1  # >> Call Site 12 <<
	.long	.Ltmp210-.Ltmp209       #   Call between .Ltmp209 and .Ltmp210
	.long	.Ltmp211-.Lfunc_begin1  #     jumps to .Ltmp211
	.byte	1                       #   On action: 1
	.long	.Ltmp215-.Lfunc_begin1  # >> Call Site 13 <<
	.long	.Ltmp216-.Ltmp215       #   Call between .Ltmp215 and .Ltmp216
	.long	.Ltmp217-.Lfunc_begin1  #     jumps to .Ltmp217
	.byte	1                       #   On action: 1
	.long	.Ltmp202-.Lfunc_begin1  # >> Call Site 14 <<
	.long	.Ltmp213-.Ltmp202       #   Call between .Ltmp202 and .Ltmp213
	.long	.Ltmp214-.Lfunc_begin1  #     jumps to .Ltmp214
	.byte	1                       #   On action: 1
	.long	.Ltmp144-.Lfunc_begin1  # >> Call Site 15 <<
	.long	.Ltmp145-.Ltmp144       #   Call between .Ltmp144 and .Ltmp145
	.long	.Ltmp146-.Lfunc_begin1  #     jumps to .Ltmp146
	.byte	1                       #   On action: 1
	.long	.Ltmp150-.Lfunc_begin1  # >> Call Site 16 <<
	.long	.Ltmp151-.Ltmp150       #   Call between .Ltmp150 and .Ltmp151
	.long	.Ltmp195-.Lfunc_begin1  #     jumps to .Ltmp195
	.byte	1                       #   On action: 1
	.long	.Ltmp155-.Lfunc_begin1  # >> Call Site 17 <<
	.long	.Ltmp156-.Ltmp155       #   Call between .Ltmp155 and .Ltmp156
	.long	.Ltmp157-.Lfunc_begin1  #     jumps to .Ltmp157
	.byte	1                       #   On action: 1
	.long	.Ltmp177-.Lfunc_begin1  # >> Call Site 18 <<
	.long	.Ltmp178-.Ltmp177       #   Call between .Ltmp177 and .Ltmp178
	.long	.Ltmp179-.Lfunc_begin1  #     jumps to .Ltmp179
	.byte	1                       #   On action: 1
	.long	.Ltmp182-.Lfunc_begin1  # >> Call Site 19 <<
	.long	.Ltmp183-.Ltmp182       #   Call between .Ltmp182 and .Ltmp183
	.long	.Ltmp184-.Lfunc_begin1  #     jumps to .Ltmp184
	.byte	1                       #   On action: 1
	.long	.Ltmp187-.Lfunc_begin1  # >> Call Site 20 <<
	.long	.Ltmp188-.Ltmp187       #   Call between .Ltmp187 and .Ltmp188
	.long	.Ltmp189-.Lfunc_begin1  #     jumps to .Ltmp189
	.byte	1                       #   On action: 1
	.long	.Ltmp193-.Lfunc_begin1  # >> Call Site 21 <<
	.long	.Ltmp194-.Ltmp193       #   Call between .Ltmp193 and .Ltmp194
	.long	.Ltmp195-.Lfunc_begin1  #     jumps to .Ltmp195
	.byte	1                       #   On action: 1
	.long	.Ltmp194-.Lfunc_begin1  # >> Call Site 22 <<
	.long	.Ltmp180-.Ltmp194       #   Call between .Ltmp194 and .Ltmp180
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp180-.Lfunc_begin1  # >> Call Site 23 <<
	.long	.Ltmp191-.Ltmp180       #   Call between .Ltmp180 and .Ltmp191
	.long	.Ltmp192-.Lfunc_begin1  #     jumps to .Ltmp192
	.byte	1                       #   On action: 1
	.long	.Ltmp158-.Lfunc_begin1  # >> Call Site 24 <<
	.long	.Ltmp159-.Ltmp158       #   Call between .Ltmp158 and .Ltmp159
	.long	.Ltmp160-.Lfunc_begin1  #     jumps to .Ltmp160
	.byte	1                       #   On action: 1
	.long	.Ltmp163-.Lfunc_begin1  # >> Call Site 25 <<
	.long	.Ltmp164-.Ltmp163       #   Call between .Ltmp163 and .Ltmp164
	.long	.Ltmp165-.Lfunc_begin1  #     jumps to .Ltmp165
	.byte	1                       #   On action: 1
	.long	.Ltmp168-.Lfunc_begin1  # >> Call Site 26 <<
	.long	.Ltmp169-.Ltmp168       #   Call between .Ltmp168 and .Ltmp169
	.long	.Ltmp170-.Lfunc_begin1  #     jumps to .Ltmp170
	.byte	1                       #   On action: 1
	.long	.Ltmp174-.Lfunc_begin1  # >> Call Site 27 <<
	.long	.Ltmp175-.Ltmp174       #   Call between .Ltmp174 and .Ltmp175
	.long	.Ltmp176-.Lfunc_begin1  #     jumps to .Ltmp176
	.byte	1                       #   On action: 1
	.long	.Ltmp161-.Lfunc_begin1  # >> Call Site 28 <<
	.long	.Ltmp172-.Ltmp161       #   Call between .Ltmp161 and .Ltmp172
	.long	.Ltmp173-.Lfunc_begin1  #     jumps to .Ltmp173
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi100:
	.cfi_def_cfa_offset 32
.Lcfi101:
	.cfi_offset %rbx, -24
.Lcfi102:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE+16, (%rbx)
.Ltmp234:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp235:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB3_2:
.Ltmp236:
	movq	%rax, %r14
.Ltmp237:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp238:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_4:
.Ltmp239:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED2Ev, .Lfunc_end3-_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp234-.Lfunc_begin2  # >> Call Site 1 <<
	.long	.Ltmp235-.Ltmp234       #   Call between .Ltmp234 and .Ltmp235
	.long	.Ltmp236-.Lfunc_begin2  #     jumps to .Ltmp236
	.byte	0                       #   On action: cleanup
	.long	.Ltmp235-.Lfunc_begin2  # >> Call Site 2 <<
	.long	.Ltmp237-.Ltmp235       #   Call between .Ltmp235 and .Ltmp237
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp237-.Lfunc_begin2  # >> Call Site 3 <<
	.long	.Ltmp238-.Ltmp237       #   Call between .Ltmp237 and .Ltmp238
	.long	.Ltmp239-.Lfunc_begin2  #     jumps to .Ltmp239
	.byte	1                       #   On action: 1
	.long	.Ltmp238-.Lfunc_begin2  # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp238    #   Call between .Ltmp238 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIbED0Ev,"axG",@progbits,_ZN13CRecordVectorIbED0Ev,comdat
	.weak	_ZN13CRecordVectorIbED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIbED0Ev,@function
_ZN13CRecordVectorIbED0Ev:              # @_ZN13CRecordVectorIbED0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi105:
	.cfi_def_cfa_offset 32
.Lcfi106:
	.cfi_offset %rbx, -24
.Lcfi107:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp240:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp241:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB4_2:
.Ltmp242:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN13CRecordVectorIbED0Ev, .Lfunc_end4-_ZN13CRecordVectorIbED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp240-.Lfunc_begin3  # >> Call Site 1 <<
	.long	.Ltmp241-.Ltmp240       #   Call between .Ltmp240 and .Ltmp241
	.long	.Ltmp242-.Lfunc_begin3  #     jumps to .Ltmp242
	.byte	0                       #   On action: cleanup
	.long	.Ltmp241-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Lfunc_end4-.Ltmp241    #   Call between .Ltmp241 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev,@function
_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev: # @_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi110:
	.cfi_def_cfa_offset 32
.Lcfi111:
	.cfi_offset %rbx, -24
.Lcfi112:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE+16, (%rbx)
.Ltmp243:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp244:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB5_2:
.Ltmp245:
	movq	%rax, %r14
.Ltmp246:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp247:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_4:
.Ltmp248:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev, .Lfunc_end5-_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp243-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp244-.Ltmp243       #   Call between .Ltmp243 and .Ltmp244
	.long	.Ltmp245-.Lfunc_begin4  #     jumps to .Ltmp245
	.byte	0                       #   On action: cleanup
	.long	.Ltmp244-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp246-.Ltmp244       #   Call between .Ltmp244 and .Ltmp246
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp246-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp247-.Ltmp246       #   Call between .Ltmp246 and .Ltmp247
	.long	.Ltmp248-.Lfunc_begin4  #     jumps to .Ltmp248
	.byte	1                       #   On action: 1
	.long	.Ltmp247-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp247    #   Call between .Ltmp247 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev,@function
_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev: # @_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi113:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi115:
	.cfi_def_cfa_offset 32
.Lcfi116:
	.cfi_offset %rbx, -24
.Lcfi117:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE+16, (%rbx)
.Ltmp249:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp250:
# BB#1:
.Ltmp255:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp256:
# BB#2:                                 # %_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB6_5:
.Ltmp257:
	movq	%rax, %r14
	jmp	.LBB6_6
.LBB6_3:
.Ltmp251:
	movq	%rax, %r14
.Ltmp252:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp253:
.LBB6_6:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_4:
.Ltmp254:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev, .Lfunc_end6-_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp249-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp250-.Ltmp249       #   Call between .Ltmp249 and .Ltmp250
	.long	.Ltmp251-.Lfunc_begin5  #     jumps to .Ltmp251
	.byte	0                       #   On action: cleanup
	.long	.Ltmp255-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp256-.Ltmp255       #   Call between .Ltmp255 and .Ltmp256
	.long	.Ltmp257-.Lfunc_begin5  #     jumps to .Ltmp257
	.byte	0                       #   On action: cleanup
	.long	.Ltmp252-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp253-.Ltmp252       #   Call between .Ltmp252 and .Ltmp253
	.long	.Ltmp254-.Lfunc_begin5  #     jumps to .Ltmp254
	.byte	1                       #   On action: 1
	.long	.Ltmp253-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Lfunc_end6-.Ltmp253    #   Call between .Ltmp253 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii,@function
_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii: # @_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi118:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi119:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi120:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi121:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi122:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi124:
	.cfi_def_cfa_offset 64
.Lcfi125:
	.cfi_offset %rbx, -56
.Lcfi126:
	.cfi_offset %r12, -48
.Lcfi127:
	.cfi_offset %r13, -40
.Lcfi128:
	.cfi_offset %r14, -32
.Lcfi129:
	.cfi_offset %r15, -24
.Lcfi130:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB7_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB7_6
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_5
# BB#4:                                 #   in Loop: Header=BB7_2 Depth=1
	movq	(%rdi), %rax
.Ltmp258:
	callq	*16(%rax)
.Ltmp259:
.LBB7_5:                                # %_ZN9CMyComPtrI8IUnknownED2Ev.exit
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB7_6:                                #   in Loop: Header=BB7_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB7_2
.LBB7_7:                                # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB7_8:
.Ltmp260:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii, .Lfunc_end7-_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp258-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp259-.Ltmp258       #   Call between .Ltmp258 and .Ltmp259
	.long	.Ltmp260-.Lfunc_begin6  #     jumps to .Ltmp260
	.byte	0                       #   On action: cleanup
	.long	.Ltmp259-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Lfunc_end7-.Ltmp259    #   Call between .Ltmp259 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi132:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi133:
	.cfi_def_cfa_offset 32
.Lcfi134:
	.cfi_offset %rbx, -24
.Lcfi135:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE+16, (%rbx)
.Ltmp261:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp262:
# BB#1:
.Ltmp267:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp268:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB8_5:
.Ltmp269:
	movq	%rax, %r14
	jmp	.LBB8_6
.LBB8_3:
.Ltmp263:
	movq	%rax, %r14
.Ltmp264:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp265:
.LBB8_6:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_4:
.Ltmp266:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED0Ev, .Lfunc_end8-_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp261-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp262-.Ltmp261       #   Call between .Ltmp261 and .Ltmp262
	.long	.Ltmp263-.Lfunc_begin7  #     jumps to .Ltmp263
	.byte	0                       #   On action: cleanup
	.long	.Ltmp267-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp268-.Ltmp267       #   Call between .Ltmp267 and .Ltmp268
	.long	.Ltmp269-.Lfunc_begin7  #     jumps to .Ltmp269
	.byte	0                       #   On action: cleanup
	.long	.Ltmp264-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp265-.Ltmp264       #   Call between .Ltmp264 and .Ltmp265
	.long	.Ltmp266-.Lfunc_begin7  #     jumps to .Ltmp266
	.byte	1                       #   On action: 1
	.long	.Ltmp265-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Lfunc_end8-.Ltmp265    #   Call between .Ltmp265 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE6DeleteEii
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi136:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi137:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi138:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi139:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi140:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi141:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi142:
	.cfi_def_cfa_offset 64
.Lcfi143:
	.cfi_offset %rbx, -56
.Lcfi144:
	.cfi_offset %r12, -48
.Lcfi145:
	.cfi_offset %r13, -40
.Lcfi146:
	.cfi_offset %r14, -32
.Lcfi147:
	.cfi_offset %r15, -24
.Lcfi148:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB9_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB9_5
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	leaq	8(%rbp), %rdi
.Ltmp270:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp271:
# BB#4:                                 # %_ZN8NArchive3N7z18CExtractFolderInfoD2Ev.exit
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB9_5:                                #   in Loop: Header=BB9_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB9_2
.LBB9_6:                                # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB9_7:
.Ltmp272:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end9:
	.size	_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE6DeleteEii, .Lfunc_end9-_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp270-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp271-.Ltmp270       #   Call between .Ltmp270 and .Ltmp271
	.long	.Ltmp272-.Lfunc_begin8  #     jumps to .Ltmp272
	.byte	0                       #   On action: cleanup
	.long	.Ltmp271-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Lfunc_end9-.Ltmp271    #   Call between .Ltmp271 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIbEC2ERKS0_,"axG",@progbits,_ZN13CRecordVectorIbEC2ERKS0_,comdat
	.weak	_ZN13CRecordVectorIbEC2ERKS0_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIbEC2ERKS0_,@function
_ZN13CRecordVectorIbEC2ERKS0_:          # @_ZN13CRecordVectorIbEC2ERKS0_
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%rbp
.Lcfi149:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi150:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi151:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi152:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi153:
	.cfi_def_cfa_offset 48
.Lcfi154:
	.cfi_offset %rbx, -48
.Lcfi155:
	.cfi_offset %r12, -40
.Lcfi156:
	.cfi_offset %r14, -32
.Lcfi157:
	.cfi_offset %r15, -24
.Lcfi158:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$1, 24(%r12)
	movq	$_ZTV13CRecordVectorIbE+16, (%r12)
.Ltmp273:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp274:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp275:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp276:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB10_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB10_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movzbl	(%rax,%rbx), %ebp
.Ltmp278:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp279:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB10_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movb	%bpl, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB10_4
.LBB10_6:                               # %_ZN13CRecordVectorIbEaSERKS0_.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_8:                               # %.loopexit.split-lp
.Ltmp277:
	jmp	.LBB10_9
.LBB10_7:                               # %.loopexit
.Ltmp280:
.LBB10_9:
	movq	%rax, %r14
.Ltmp281:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp282:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB10_11:
.Ltmp283:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end10:
	.size	_ZN13CRecordVectorIbEC2ERKS0_, .Lfunc_end10-_ZN13CRecordVectorIbEC2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp273-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp276-.Ltmp273       #   Call between .Ltmp273 and .Ltmp276
	.long	.Ltmp277-.Lfunc_begin9  #     jumps to .Ltmp277
	.byte	0                       #   On action: cleanup
	.long	.Ltmp278-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp279-.Ltmp278       #   Call between .Ltmp278 and .Ltmp279
	.long	.Ltmp280-.Lfunc_begin9  #     jumps to .Ltmp280
	.byte	0                       #   On action: cleanup
	.long	.Ltmp281-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp282-.Ltmp281       #   Call between .Ltmp281 and .Ltmp282
	.long	.Ltmp283-.Lfunc_begin9  #     jumps to .Ltmp283
	.byte	1                       #   On action: 1
	.long	.Ltmp282-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Lfunc_end10-.Ltmp282   #   Call between .Ltmp282 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.type	_ZTV13CRecordVectorIbE,@object # @_ZTV13CRecordVectorIbE
	.section	.rodata._ZTV13CRecordVectorIbE,"aG",@progbits,_ZTV13CRecordVectorIbE,comdat
	.weak	_ZTV13CRecordVectorIbE
	.p2align	3
_ZTV13CRecordVectorIbE:
	.quad	0
	.quad	_ZTI13CRecordVectorIbE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIbED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIbE, 40

	.type	_ZTS13CRecordVectorIbE,@object # @_ZTS13CRecordVectorIbE
	.section	.rodata._ZTS13CRecordVectorIbE,"aG",@progbits,_ZTS13CRecordVectorIbE,comdat
	.weak	_ZTS13CRecordVectorIbE
	.p2align	4
_ZTS13CRecordVectorIbE:
	.asciz	"13CRecordVectorIbE"
	.size	_ZTS13CRecordVectorIbE, 19

	.type	_ZTI13CRecordVectorIbE,@object # @_ZTI13CRecordVectorIbE
	.section	.rodata._ZTI13CRecordVectorIbE,"aG",@progbits,_ZTI13CRecordVectorIbE,comdat
	.weak	_ZTI13CRecordVectorIbE
	.p2align	4
_ZTI13CRecordVectorIbE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIbE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIbE, 24

	.type	_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE,@object # @_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE
	.section	.rodata._ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE,"aG",@progbits,_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE,comdat
	.weak	_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE
	.p2align	3
_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE
	.quad	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii
	.size	_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE, 40

	.type	_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE,@object # @_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE
	.section	.rodata._ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE,"aG",@progbits,_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE,comdat
	.weak	_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE
	.p2align	4
_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE:
	.asciz	"13CObjectVectorI9CMyComPtrI8IUnknownEE"
	.size	_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE, 39

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE,@object # @_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE
	.section	.rodata._ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE,"aG",@progbits,_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE,comdat
	.weak	_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE
	.p2align	4
_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE, 24

	.type	_ZTV13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE, 53

	.type	_ZTI13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z18CExtractFolderInfoEE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
