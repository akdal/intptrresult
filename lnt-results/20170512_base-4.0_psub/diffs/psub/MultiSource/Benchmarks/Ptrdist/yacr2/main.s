	.text
	.file	"main.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_2 Depth 2
                                        #       Child Loop BB0_4 Depth 3
                                        #       Child Loop BB0_6 Depth 3
                                        #         Child Loop BB0_14 Depth 4
                                        #         Child Loop BB0_18 Depth 4
	movl	%r15d, %edi
	movq	%r14, %rsi
	callq	Option
	callq	BuildChannel
	callq	BuildVCG
	callq	AcyclicVCG
	callq	BuildHCG
	jmp	.LBB0_2
.LBB0_22:                               # %.critedge39
                                        #   in Loop: Header=BB0_2 Depth=2
	callq	FreeAllocMaps
	callq	FreeAssign
	.p2align	4, 0x90
.LBB0_2:                                #   Parent Loop BB0_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_4 Depth 3
                                        #       Child Loop BB0_6 Depth 3
                                        #         Child Loop BB0_14 Depth 4
                                        #         Child Loop BB0_18 Depth 4
	callq	AllocAssign
	callq	NetsAssign
	callq	InitAllocMaps
	movq	channelTracks(%rip), %rax
	movq	%rax, channelTracksCopy(%rip)
	cmpq	$0, channelNets(%rip)
	je	.LBB0_5
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB0_2 Depth=2
	movq	netsAssign(%rip), %rax
	movq	netsAssignCopy(%rip), %rcx
	movl	$1, %edx
	.p2align	4, 0x90
.LBB0_4:                                #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax,%rdx,8), %rsi
	movq	%rsi, (%rcx,%rdx,8)
	incq	%rdx
	cmpq	channelNets(%rip), %rdx
	jbe	.LBB0_4
.LBB0_5:                                # %.preheader48.preheader
                                        #   in Loop: Header=BB0_2 Depth=2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_6:                                # %.preheader48
                                        #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_2 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_14 Depth 4
                                        #         Child Loop BB0_18 Depth 4
	callq	DrawNets
	movl	%eax, %ecx
	testl	%ecx, %ecx
	je	.LBB0_23
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=3
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	callq	Maze1
	movl	%eax, %ecx
	testl	%ecx, %ecx
	je	.LBB0_23
# BB#8:                                 #   in Loop: Header=BB0_6 Depth=3
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	callq	Maze2
	movl	%eax, %ecx
	testl	%ecx, %ecx
	je	.LBB0_23
# BB#9:                                 #   in Loop: Header=BB0_6 Depth=3
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	callq	Maze3
	movl	%eax, %ecx
	testl	%ecx, %ecx
	je	.LBB0_23
# BB#10:                                #   in Loop: Header=BB0_6 Depth=3
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	testq	%rbp, %rbp
	jne	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_6 Depth=3
	incq	channelTracks(%rip)
.LBB0_12:                               #   in Loop: Header=BB0_6 Depth=3
	incq	%rbp
	cmpq	$0, channelNets(%rip)
	je	.LBB0_21
# BB#13:                                # %.lr.ph51
                                        #   in Loop: Header=BB0_6 Depth=3
	movq	netsAssignCopy(%rip), %rcx
	movq	netsAssign(%rip), %rdx
	movl	$1, %esi
	.p2align	4, 0x90
.LBB0_14:                               #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_2 Depth=2
                                        #       Parent Loop BB0_6 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rcx,%rsi,8), %rax
	movq	%rax, (%rdx,%rsi,8)
	incq	%rsi
	movq	channelNets(%rip), %rax
	cmpq	%rax, %rsi
	jbe	.LBB0_14
# BB#15:                                # %._crit_edge
                                        #   in Loop: Header=BB0_6 Depth=3
	testq	%rbp, %rbp
	je	.LBB0_21
# BB#16:                                # %._crit_edge
                                        #   in Loop: Header=BB0_6 Depth=3
	testq	%rax, %rax
	je	.LBB0_21
# BB#17:                                # %.lr.ph53
                                        #   in Loop: Header=BB0_6 Depth=3
	movq	netsAssign(%rip), %rcx
	movl	$1, %edx
	.p2align	4, 0x90
.LBB0_18:                               #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_2 Depth=2
                                        #       Parent Loop BB0_6 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rcx,%rdx,8), %rsi
	cmpq	%rbp, %rsi
	jb	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_18 Depth=4
	incq	%rsi
	movq	%rsi, (%rcx,%rdx,8)
	movq	channelNets(%rip), %rax
.LBB0_20:                               #   in Loop: Header=BB0_18 Depth=4
	incq	%rdx
	cmpq	%rax, %rdx
	jbe	.LBB0_18
.LBB0_21:                               # %.loopexit
                                        #   in Loop: Header=BB0_6 Depth=3
	movq	channelTracksCopy(%rip), %rax
	incq	%rax
	cmpq	%rax, %rbp
	jbe	.LBB0_6
	jmp	.LBB0_22
	.p2align	4, 0x90
.LBB0_23:                               # %.critedge40
                                        #   in Loop: Header=BB0_1 Depth=1
	movl	$10, %edi
	callq	putchar
	callq	PrintChannel
	incl	%ebx
	cmpl	$20, %ebx
	jne	.LBB0_1
# BB#24:
	xorl	%edi, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Assignment could not route %d columns, trying maze1...\n"
	.size	.L.str, 56

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Maze1 could not route %d columns, trying maze2...\n"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Maze2 could not route %d columns, trying maze3...\n"
	.size	.L.str.2, 51

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Maze3 could not route %d columns, adding a track...\n"
	.size	.L.str.3, 53


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
