	.text
	.file	"dct64_i386.bc"
	.globl	dct64
	.p2align	4, 0x90
	.type	dct64,@function
dct64:                                  # @dct64
	.cfi_startproc
# BB#0:
	subq	$112, %rsp
.Lcfi0:
	.cfi_def_cfa_offset 120
	movsd	248(%rdx), %xmm0        # xmm0 = mem[0],zero
	movsd	(%rdx), %xmm2           # xmm2 = mem[0],zero
	movsd	8(%rdx), %xmm1          # xmm1 = mem[0],zero
	movapd	%xmm2, %xmm3
	addsd	%xmm0, %xmm3
	movsd	%xmm3, -64(%rsp)        # 8-byte Spill
	subsd	%xmm0, %xmm2
	movsd	%xmm2, 24(%rsp)         # 8-byte Spill
	movsd	240(%rdx), %xmm0        # xmm0 = mem[0],zero
	movapd	%xmm1, %xmm2
	addsd	%xmm0, %xmm2
	movsd	%xmm2, -56(%rsp)        # 8-byte Spill
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	movsd	16(%rdx), %xmm1         # xmm1 = mem[0],zero
	movsd	232(%rdx), %xmm0        # xmm0 = mem[0],zero
	movapd	%xmm1, %xmm2
	addsd	%xmm0, %xmm2
	movsd	%xmm2, -128(%rsp)       # 8-byte Spill
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movsd	24(%rdx), %xmm1         # xmm1 = mem[0],zero
	movsd	224(%rdx), %xmm0        # xmm0 = mem[0],zero
	movapd	%xmm1, %xmm14
	addsd	%xmm0, %xmm14
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	32(%rdx), %xmm1         # xmm1 = mem[0],zero
	movsd	216(%rdx), %xmm0        # xmm0 = mem[0],zero
	movapd	%xmm1, %xmm2
	addsd	%xmm0, %xmm2
	movsd	%xmm2, -112(%rsp)       # 8-byte Spill
	subsd	%xmm0, %xmm1
	movsd	%xmm1, -72(%rsp)        # 8-byte Spill
	movsd	40(%rdx), %xmm1         # xmm1 = mem[0],zero
	movsd	208(%rdx), %xmm0        # xmm0 = mem[0],zero
	movapd	%xmm1, %xmm2
	addsd	%xmm0, %xmm2
	movsd	%xmm2, -24(%rsp)        # 8-byte Spill
	subsd	%xmm0, %xmm1
	movsd	%xmm1, -80(%rsp)        # 8-byte Spill
	movsd	48(%rdx), %xmm1         # xmm1 = mem[0],zero
	movsd	200(%rdx), %xmm0        # xmm0 = mem[0],zero
	movapd	%xmm1, %xmm2
	addsd	%xmm0, %xmm2
	movsd	%xmm2, -120(%rsp)       # 8-byte Spill
	subsd	%xmm0, %xmm1
	movsd	%xmm1, -88(%rsp)        # 8-byte Spill
	movsd	56(%rdx), %xmm15        # xmm15 = mem[0],zero
	movsd	192(%rdx), %xmm0        # xmm0 = mem[0],zero
	movapd	%xmm15, %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, -32(%rsp)        # 8-byte Spill
	subsd	%xmm0, %xmm15
	movsd	64(%rdx), %xmm1         # xmm1 = mem[0],zero
	movsd	184(%rdx), %xmm0        # xmm0 = mem[0],zero
	movapd	%xmm1, %xmm2
	addsd	%xmm0, %xmm2
	movsd	%xmm2, -40(%rsp)        # 8-byte Spill
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movsd	72(%rdx), %xmm1         # xmm1 = mem[0],zero
	movsd	176(%rdx), %xmm0        # xmm0 = mem[0],zero
	movapd	%xmm1, %xmm2
	addsd	%xmm0, %xmm2
	movsd	%xmm2, -48(%rsp)        # 8-byte Spill
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 80(%rsp)         # 8-byte Spill
	movsd	80(%rdx), %xmm12        # xmm12 = mem[0],zero
	movsd	168(%rdx), %xmm0        # xmm0 = mem[0],zero
	movapd	%xmm12, %xmm13
	addsd	%xmm0, %xmm13
	subsd	%xmm0, %xmm12
	movsd	88(%rdx), %xmm9         # xmm9 = mem[0],zero
	movsd	160(%rdx), %xmm3        # xmm3 = mem[0],zero
	movapd	%xmm9, %xmm8
	addsd	%xmm3, %xmm8
	subsd	%xmm3, %xmm9
	movsd	96(%rdx), %xmm6         # xmm6 = mem[0],zero
	movsd	152(%rdx), %xmm5        # xmm5 = mem[0],zero
	movapd	%xmm6, %xmm4
	addsd	%xmm5, %xmm4
	subsd	%xmm5, %xmm6
	movsd	104(%rdx), %xmm11       # xmm11 = mem[0],zero
	movsd	144(%rdx), %xmm7        # xmm7 = mem[0],zero
	movapd	%xmm11, %xmm5
	addsd	%xmm7, %xmm5
	subsd	%xmm7, %xmm11
	movsd	112(%rdx), %xmm3        # xmm3 = mem[0],zero
	movsd	136(%rdx), %xmm10       # xmm10 = mem[0],zero
	movapd	%xmm3, %xmm7
	addsd	%xmm10, %xmm7
	subsd	%xmm10, %xmm3
	movsd	120(%rdx), %xmm2        # xmm2 = mem[0],zero
	movsd	128(%rdx), %xmm0        # xmm0 = mem[0],zero
	movapd	%xmm2, %xmm10
	addsd	%xmm0, %xmm10
	subsd	%xmm0, %xmm2
	movsd	-64(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm0
	addsd	%xmm10, %xmm0
	movsd	%xmm0, -8(%rsp)         # 8-byte Spill
	subsd	%xmm10, %xmm1
	movapd	%xmm1, %xmm10
	movsd	-56(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	addsd	%xmm7, %xmm1
	movsd	%xmm1, -16(%rsp)        # 8-byte Spill
	subsd	%xmm7, %xmm0
	movapd	%xmm0, %xmm7
	movsd	-128(%rsp), %xmm0       # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	addsd	%xmm5, %xmm1
	movsd	%xmm1, -96(%rsp)        # 8-byte Spill
	subsd	%xmm5, %xmm0
	movsd	%xmm0, -128(%rsp)       # 8-byte Spill
	movapd	%xmm14, %xmm0
	addsd	%xmm4, %xmm0
	movsd	%xmm0, 72(%rsp)         # 8-byte Spill
	subsd	%xmm4, %xmm14
	movsd	%xmm14, -104(%rsp)      # 8-byte Spill
	movsd	-112(%rsp), %xmm0       # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	addsd	%xmm8, %xmm1
	movsd	%xmm1, 64(%rsp)         # 8-byte Spill
	subsd	%xmm8, %xmm0
	movsd	%xmm0, -112(%rsp)       # 8-byte Spill
	movsd	-24(%rsp), %xmm5        # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movapd	%xmm5, %xmm14
	addsd	%xmm13, %xmm14
	subsd	%xmm13, %xmm5
	movsd	-120(%rsp), %xmm0       # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm13
	movsd	-48(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm13
	subsd	%xmm1, %xmm0
	movsd	%xmm0, -120(%rsp)       # 8-byte Spill
	movsd	-32(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm8
	movsd	-40(%rsp), %xmm4        # 8-byte Reload
                                        # xmm4 = mem[0],zero
	addsd	%xmm4, %xmm8
	subsd	%xmm4, %xmm0
	movapd	%xmm0, %xmm4
	movq	pnts(%rip), %rax
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	(%rax), %xmm0
	mulsd	120(%rax), %xmm2
	movapd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	movsd	%xmm1, -32(%rsp)        # 8-byte Spill
	subsd	%xmm2, %xmm0
	movq	pnts+8(%rip), %rcx
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm10
	movsd	%xmm10, -64(%rsp)       # 8-byte Spill
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movsd	32(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	8(%rax), %xmm1
	mulsd	112(%rax), %xmm3
	movapd	%xmm1, %xmm0
	addsd	%xmm3, %xmm0
	movsd	%xmm0, -40(%rsp)        # 8-byte Spill
	subsd	%xmm3, %xmm1
	movsd	8(%rcx), %xmm0          # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm7
	movsd	%xmm7, -56(%rsp)        # 8-byte Spill
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	16(%rax), %xmm1
	mulsd	104(%rax), %xmm11
	movapd	%xmm1, %xmm0
	addsd	%xmm11, %xmm0
	movsd	%xmm0, 96(%rsp)         # 8-byte Spill
	subsd	%xmm11, %xmm1
	movsd	16(%rcx), %xmm0         # xmm0 = mem[0],zero
	movsd	-128(%rsp), %xmm2       # 8-byte Reload
                                        # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -128(%rsp)       # 8-byte Spill
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	24(%rax), %xmm1
	mulsd	96(%rax), %xmm6
	movapd	%xmm1, %xmm0
	addsd	%xmm6, %xmm0
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	subsd	%xmm6, %xmm1
	movsd	24(%rcx), %xmm0         # xmm0 = mem[0],zero
	movsd	-104(%rsp), %xmm2       # 8-byte Reload
                                        # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -104(%rsp)       # 8-byte Spill
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	-72(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	32(%rax), %xmm1
	mulsd	88(%rax), %xmm9
	movapd	%xmm1, %xmm0
	addsd	%xmm9, %xmm0
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	subsd	%xmm9, %xmm1
	movsd	32(%rcx), %xmm0         # xmm0 = mem[0],zero
	movsd	-112(%rsp), %xmm11      # 8-byte Reload
                                        # xmm11 = mem[0],zero
	mulsd	%xmm0, %xmm11
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -72(%rsp)        # 8-byte Spill
	movsd	-80(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	40(%rax), %xmm1
	mulsd	80(%rax), %xmm12
	movapd	%xmm1, %xmm2
	addsd	%xmm12, %xmm2
	subsd	%xmm12, %xmm1
	movsd	40(%rcx), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm5
	movapd	%xmm5, %xmm12
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -80(%rsp)        # 8-byte Spill
	movsd	-88(%rsp), %xmm3        # 8-byte Reload
                                        # xmm3 = mem[0],zero
	mulsd	48(%rax), %xmm3
	movsd	80(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	72(%rax), %xmm0
	movapd	%xmm3, %xmm1
	addsd	%xmm0, %xmm1
	subsd	%xmm0, %xmm3
	movsd	48(%rcx), %xmm0         # xmm0 = mem[0],zero
	movsd	-120(%rsp), %xmm6       # 8-byte Reload
                                        # xmm6 = mem[0],zero
	mulsd	%xmm0, %xmm6
	mulsd	%xmm0, %xmm3
	movsd	%xmm3, -88(%rsp)        # 8-byte Spill
	mulsd	56(%rax), %xmm15
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	64(%rax), %xmm0
	movapd	%xmm15, %xmm7
	addsd	%xmm0, %xmm7
	subsd	%xmm0, %xmm15
	movsd	56(%rcx), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm4
	mulsd	%xmm0, %xmm15
	movapd	%xmm8, %xmm3
	movsd	-8(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm3
	movsd	%xmm3, (%rsp)           # 8-byte Spill
	subsd	%xmm8, %xmm0
	movsd	%xmm0, -8(%rsp)         # 8-byte Spill
	movapd	%xmm13, %xmm3
	movsd	-16(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm3
	movsd	%xmm3, -48(%rsp)        # 8-byte Spill
	subsd	%xmm13, %xmm0
	movsd	%xmm0, -16(%rsp)        # 8-byte Spill
	movapd	%xmm14, %xmm3
	movsd	-96(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm3
	movsd	%xmm3, 48(%rsp)         # 8-byte Spill
	subsd	%xmm14, %xmm0
	movsd	%xmm0, -96(%rsp)        # 8-byte Spill
	movsd	64(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movapd	%xmm3, %xmm10
	movsd	72(%rsp), %xmm13        # 8-byte Reload
                                        # xmm13 = mem[0],zero
	addsd	%xmm13, %xmm10
	subsd	%xmm3, %xmm13
	movsd	-64(%rsp), %xmm5        # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movapd	%xmm5, %xmm3
	addsd	%xmm4, %xmm3
	movsd	%xmm3, -24(%rsp)        # 8-byte Spill
	subsd	%xmm4, %xmm5
	movsd	-56(%rsp), %xmm9        # 8-byte Reload
                                        # xmm9 = mem[0],zero
	movapd	%xmm9, %xmm3
	addsd	%xmm6, %xmm3
	movsd	%xmm3, -120(%rsp)       # 8-byte Spill
	subsd	%xmm6, %xmm9
	movsd	-128(%rsp), %xmm0       # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm4
	movapd	%xmm12, %xmm3
	addsd	%xmm3, %xmm4
	movsd	%xmm4, 88(%rsp)         # 8-byte Spill
	subsd	%xmm3, %xmm0
	movsd	%xmm0, -128(%rsp)       # 8-byte Spill
	movsd	-104(%rsp), %xmm4       # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm3
	addsd	%xmm11, %xmm3
	subsd	%xmm11, %xmm4
	movapd	%xmm4, %xmm8
	movapd	%xmm7, %xmm0
	movsd	-32(%rsp), %xmm4        # 8-byte Reload
                                        # xmm4 = mem[0],zero
	addsd	%xmm4, %xmm0
	movsd	%xmm0, -112(%rsp)       # 8-byte Spill
	subsd	%xmm7, %xmm4
	movapd	%xmm4, %xmm6
	movapd	%xmm1, %xmm0
	movsd	-40(%rsp), %xmm14       # 8-byte Reload
                                        # xmm14 = mem[0],zero
	addsd	%xmm14, %xmm0
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	subsd	%xmm1, %xmm14
	movapd	%xmm2, %xmm0
	movsd	96(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 104(%rsp)        # 8-byte Spill
	subsd	%xmm2, %xmm1
	movsd	40(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm11
	movsd	56(%rsp), %xmm12        # 8-byte Reload
                                        # xmm12 = mem[0],zero
	addsd	%xmm12, %xmm11
	subsd	%xmm2, %xmm12
	movsd	24(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm0
	addsd	%xmm15, %xmm0
	movsd	%xmm0, -104(%rsp)       # 8-byte Spill
	subsd	%xmm15, %xmm4
	movq	pnts+16(%rip), %rax
	movsd	(%rax), %xmm2           # xmm2 = mem[0],zero
	movsd	-8(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	%xmm2, %xmm0
	mulsd	%xmm2, %xmm5
	movsd	%xmm5, -64(%rsp)        # 8-byte Spill
	mulsd	%xmm2, %xmm6
	movsd	%xmm6, -32(%rsp)        # 8-byte Spill
	mulsd	%xmm2, %xmm4
	movsd	%xmm4, 24(%rsp)         # 8-byte Spill
	movsd	32(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm2
	movsd	-88(%rsp), %xmm5        # 8-byte Reload
                                        # xmm5 = mem[0],zero
	addsd	%xmm5, %xmm2
	movsd	%xmm2, 72(%rsp)         # 8-byte Spill
	subsd	%xmm5, %xmm4
	movsd	8(%rax), %xmm2          # xmm2 = mem[0],zero
	movsd	-16(%rsp), %xmm5        # 8-byte Reload
                                        # xmm5 = mem[0],zero
	mulsd	%xmm2, %xmm5
	mulsd	%xmm2, %xmm9
	movsd	%xmm9, -56(%rsp)        # 8-byte Spill
	mulsd	%xmm2, %xmm14
	movsd	%xmm14, -40(%rsp)       # 8-byte Spill
	mulsd	%xmm2, %xmm4
	movsd	%xmm4, 32(%rsp)         # 8-byte Spill
	movsd	16(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm6
	movsd	-80(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm2, %xmm6
	movapd	%xmm6, %xmm15
	subsd	%xmm2, %xmm4
	movsd	16(%rax), %xmm2         # xmm2 = mem[0],zero
	movsd	-96(%rsp), %xmm9        # 8-byte Reload
                                        # xmm9 = mem[0],zero
	mulsd	%xmm2, %xmm9
	movsd	-128(%rsp), %xmm7       # 8-byte Reload
                                        # xmm7 = mem[0],zero
	mulsd	%xmm2, %xmm7
	mulsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm14
	mulsd	%xmm2, %xmm4
	movsd	%xmm4, 16(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm4          # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm2
	movsd	-72(%rsp), %xmm6        # 8-byte Reload
                                        # xmm6 = mem[0],zero
	addsd	%xmm6, %xmm2
	subsd	%xmm6, %xmm4
	movsd	24(%rax), %xmm6         # xmm6 = mem[0],zero
	mulsd	%xmm6, %xmm13
	mulsd	%xmm6, %xmm8
	mulsd	%xmm6, %xmm12
	mulsd	%xmm6, %xmm4
	movsd	%xmm4, 8(%rsp)          # 8-byte Spill
	movapd	%xmm10, %xmm1
	movsd	(%rsp), %xmm4           # 8-byte Reload
                                        # xmm4 = mem[0],zero
	addsd	%xmm4, %xmm1
	movsd	%xmm1, 80(%rsp)         # 8-byte Spill
	subsd	%xmm10, %xmm4
	movsd	%xmm4, (%rsp)           # 8-byte Spill
	movsd	48(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm1
	movsd	-48(%rsp), %xmm6        # 8-byte Reload
                                        # xmm6 = mem[0],zero
	addsd	%xmm6, %xmm1
	movsd	%xmm1, 40(%rsp)         # 8-byte Spill
	subsd	%xmm4, %xmm6
	movsd	%xmm6, -48(%rsp)        # 8-byte Spill
	movapd	%xmm0, %xmm6
	addsd	%xmm13, %xmm0
	movsd	%xmm0, -96(%rsp)        # 8-byte Spill
	subsd	%xmm13, %xmm6
	movapd	%xmm5, %xmm4
	movapd	%xmm4, %xmm1
	movapd	%xmm9, %xmm0
	addsd	%xmm0, %xmm1
	movsd	%xmm1, -8(%rsp)         # 8-byte Spill
	subsd	%xmm0, %xmm4
	movsd	%xmm4, -16(%rsp)        # 8-byte Spill
	movapd	%xmm3, %xmm0
	movsd	-24(%rsp), %xmm5        # 8-byte Reload
                                        # xmm5 = mem[0],zero
	addsd	%xmm5, %xmm0
	movsd	%xmm0, -88(%rsp)        # 8-byte Spill
	subsd	%xmm3, %xmm5
	movsd	88(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm10
	movsd	-120(%rsp), %xmm13      # 8-byte Reload
                                        # xmm13 = mem[0],zero
	addsd	%xmm13, %xmm10
	subsd	%xmm0, %xmm13
	movsd	-64(%rsp), %xmm9        # 8-byte Reload
                                        # xmm9 = mem[0],zero
	movapd	%xmm9, %xmm3
	addsd	%xmm8, %xmm3
	movsd	%xmm3, -120(%rsp)       # 8-byte Spill
	subsd	%xmm8, %xmm9
	movapd	%xmm9, %xmm8
	movsd	-56(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm3
	addsd	%xmm7, %xmm3
	movsd	%xmm3, 48(%rsp)         # 8-byte Spill
	subsd	%xmm7, %xmm0
	movsd	%xmm0, -56(%rsp)        # 8-byte Spill
	movapd	%xmm11, %xmm3
	movsd	-112(%rsp), %xmm9       # 8-byte Reload
                                        # xmm9 = mem[0],zero
	addsd	%xmm9, %xmm3
	movsd	%xmm3, -72(%rsp)        # 8-byte Spill
	subsd	%xmm11, %xmm9
	movsd	104(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm3
	movsd	64(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	addsd	%xmm4, %xmm3
	movsd	%xmm3, 56(%rsp)         # 8-byte Spill
	subsd	%xmm0, %xmm4
	movsd	-32(%rsp), %xmm7        # 8-byte Reload
                                        # xmm7 = mem[0],zero
	movapd	%xmm7, %xmm1
	addsd	%xmm12, %xmm1
	movsd	%xmm1, -80(%rsp)        # 8-byte Spill
	subsd	%xmm12, %xmm7
	movsd	-40(%rsp), %xmm11       # 8-byte Reload
                                        # xmm11 = mem[0],zero
	movapd	%xmm11, %xmm1
	addsd	%xmm14, %xmm1
	movsd	%xmm1, 96(%rsp)         # 8-byte Spill
	subsd	%xmm14, %xmm11
	movapd	%xmm2, %xmm1
	movsd	-104(%rsp), %xmm14      # 8-byte Reload
                                        # xmm14 = mem[0],zero
	addsd	%xmm14, %xmm1
	movsd	%xmm1, -128(%rsp)       # 8-byte Spill
	subsd	%xmm2, %xmm14
	movapd	%xmm15, %xmm0
	movapd	%xmm0, %xmm1
	movsd	72(%rsp), %xmm12        # 8-byte Reload
                                        # xmm12 = mem[0],zero
	addsd	%xmm12, %xmm1
	movsd	%xmm1, -64(%rsp)        # 8-byte Spill
	subsd	%xmm0, %xmm12
	movsd	24(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm15
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm15
	subsd	%xmm0, %xmm2
	movq	pnts+24(%rip), %rax
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	mulsd	%xmm1, %xmm6
	mulsd	%xmm1, %xmm5
	mulsd	%xmm1, %xmm8
	mulsd	%xmm1, %xmm9
	movsd	%xmm9, -112(%rsp)       # 8-byte Spill
	mulsd	%xmm1, %xmm7
	movsd	%xmm7, -32(%rsp)        # 8-byte Spill
	mulsd	%xmm1, %xmm14
	movsd	%xmm14, -104(%rsp)      # 8-byte Spill
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, 24(%rsp)         # 8-byte Spill
	movsd	32(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm1
	movsd	16(%rsp), %xmm7         # 8-byte Reload
                                        # xmm7 = mem[0],zero
	addsd	%xmm7, %xmm1
	movsd	%xmm1, 88(%rsp)         # 8-byte Spill
	subsd	%xmm7, %xmm2
	movsd	8(%rax), %xmm1          # xmm1 = mem[0],zero
	movsd	-48(%rsp), %xmm9        # 8-byte Reload
                                        # xmm9 = mem[0],zero
	mulsd	%xmm1, %xmm9
	movsd	-16(%rsp), %xmm3        # 8-byte Reload
                                        # xmm3 = mem[0],zero
	mulsd	%xmm1, %xmm3
	mulsd	%xmm1, %xmm13
	movsd	-56(%rsp), %xmm7        # 8-byte Reload
                                        # xmm7 = mem[0],zero
	mulsd	%xmm1, %xmm7
	mulsd	%xmm1, %xmm4
	mulsd	%xmm1, %xmm11
	movsd	%xmm11, -40(%rsp)       # 8-byte Spill
	mulsd	%xmm1, %xmm12
	movsd	%xmm12, 72(%rsp)        # 8-byte Spill
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, 32(%rsp)         # 8-byte Spill
	movsd	40(%rsp), %xmm11        # 8-byte Reload
                                        # xmm11 = mem[0],zero
	movapd	%xmm11, %xmm2
	movsd	80(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm2
	movsd	%xmm2, -56(%rsp)        # 8-byte Spill
	subsd	%xmm11, %xmm1
	movsd	%xmm1, 80(%rsp)         # 8-byte Spill
	movapd	%xmm0, %xmm2
	addsd	%xmm9, %xmm2
	movsd	%xmm2, 16(%rsp)         # 8-byte Spill
	subsd	%xmm9, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	-8(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	movsd	-96(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm2, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	subsd	%xmm0, %xmm2
	movsd	%xmm2, -96(%rsp)        # 8-byte Spill
	movapd	%xmm6, %xmm2
	addsd	%xmm3, %xmm2
	movsd	%xmm2, -16(%rsp)        # 8-byte Spill
	subsd	%xmm3, %xmm6
	movsd	%xmm6, -8(%rsp)         # 8-byte Spill
	movapd	%xmm10, %xmm2
	movsd	-88(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm2
	movsd	%xmm2, -48(%rsp)        # 8-byte Spill
	subsd	%xmm10, %xmm1
	movsd	%xmm1, -88(%rsp)        # 8-byte Spill
	movapd	%xmm5, %xmm0
	addsd	%xmm13, %xmm0
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	subsd	%xmm13, %xmm5
	movsd	%xmm5, -24(%rsp)        # 8-byte Spill
	movsd	48(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	movsd	-120(%rsp), %xmm0       # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm2
	movsd	%xmm2, 40(%rsp)         # 8-byte Spill
	subsd	%xmm1, %xmm0
	movsd	%xmm0, -120(%rsp)       # 8-byte Spill
	movapd	%xmm8, %xmm14
	movapd	%xmm14, %xmm9
	addsd	%xmm7, %xmm9
	subsd	%xmm7, %xmm14
	movsd	56(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	movsd	-72(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm2
	movsd	%xmm2, 48(%rsp)         # 8-byte Spill
	subsd	%xmm1, %xmm0
	movsd	%xmm0, -72(%rsp)        # 8-byte Spill
	movsd	-112(%rsp), %xmm1       # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm0
	addsd	%xmm4, %xmm0
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	subsd	%xmm4, %xmm1
	movsd	%xmm1, -112(%rsp)       # 8-byte Spill
	movsd	96(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm12
	movsd	-80(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm12
	subsd	%xmm1, %xmm0
	movsd	%xmm0, -80(%rsp)        # 8-byte Spill
	movsd	-32(%rsp), %xmm11       # 8-byte Reload
                                        # xmm11 = mem[0],zero
	movapd	%xmm11, %xmm10
	movsd	-40(%rsp), %xmm8        # 8-byte Reload
                                        # xmm8 = mem[0],zero
	addsd	%xmm8, %xmm10
	subsd	%xmm8, %xmm11
	movsd	-64(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm8
	movsd	-128(%rsp), %xmm0       # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm8
	subsd	%xmm1, %xmm0
	movsd	%xmm0, -128(%rsp)       # 8-byte Spill
	movsd	-104(%rsp), %xmm0       # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm6
	movsd	72(%rsp), %xmm13        # 8-byte Reload
                                        # xmm13 = mem[0],zero
	addsd	%xmm13, %xmm6
	subsd	%xmm13, %xmm0
	movsd	%xmm0, -104(%rsp)       # 8-byte Spill
	movsd	88(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm2
	addsd	%xmm15, %xmm2
	subsd	%xmm0, %xmm15
	movsd	24(%rsp), %xmm13        # 8-byte Reload
                                        # xmm13 = mem[0],zero
	movapd	%xmm13, %xmm1
	movsd	32(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	addsd	%xmm3, %xmm1
	subsd	%xmm3, %xmm13
	movq	pnts+32(%rip), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	80(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	movsd	(%rsp), %xmm4           # 8-byte Reload
                                        # xmm4 = mem[0],zero
	mulsd	%xmm0, %xmm4
	movsd	-96(%rsp), %xmm3        # 8-byte Reload
                                        # xmm3 = mem[0],zero
	mulsd	%xmm0, %xmm3
	movsd	%xmm3, -96(%rsp)        # 8-byte Spill
	movsd	-8(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	mulsd	%xmm0, %xmm3
	movsd	-88(%rsp), %xmm7        # 8-byte Reload
                                        # xmm7 = mem[0],zero
	mulsd	%xmm0, %xmm7
	movsd	%xmm7, -88(%rsp)        # 8-byte Spill
	movsd	-24(%rsp), %xmm7        # 8-byte Reload
                                        # xmm7 = mem[0],zero
	mulsd	%xmm0, %xmm7
	movsd	%xmm7, -24(%rsp)        # 8-byte Spill
	movsd	-120(%rsp), %xmm7       # 8-byte Reload
                                        # xmm7 = mem[0],zero
	mulsd	%xmm0, %xmm7
	movsd	%xmm7, -120(%rsp)       # 8-byte Spill
	mulsd	%xmm0, %xmm14
	movsd	%xmm14, -64(%rsp)       # 8-byte Spill
	movsd	-72(%rsp), %xmm14       # 8-byte Reload
                                        # xmm14 = mem[0],zero
	mulsd	%xmm0, %xmm14
	movsd	%xmm14, -72(%rsp)       # 8-byte Spill
	movsd	-112(%rsp), %xmm7       # 8-byte Reload
                                        # xmm7 = mem[0],zero
	mulsd	%xmm0, %xmm7
	movsd	%xmm7, -112(%rsp)       # 8-byte Spill
	movsd	-80(%rsp), %xmm7        # 8-byte Reload
                                        # xmm7 = mem[0],zero
	mulsd	%xmm0, %xmm7
	movsd	%xmm7, -80(%rsp)        # 8-byte Spill
	mulsd	%xmm0, %xmm11
	movsd	-128(%rsp), %xmm7       # 8-byte Reload
                                        # xmm7 = mem[0],zero
	mulsd	%xmm0, %xmm7
	movsd	%xmm7, -128(%rsp)       # 8-byte Spill
	movsd	-104(%rsp), %xmm7       # 8-byte Reload
                                        # xmm7 = mem[0],zero
	mulsd	%xmm0, %xmm7
	movsd	%xmm7, -104(%rsp)       # 8-byte Spill
	mulsd	%xmm0, %xmm15
	mulsd	%xmm0, %xmm13
	movsd	-56(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 2048(%rdi)
	movsd	-16(%rsp), %xmm14       # 8-byte Reload
                                        # xmm14 = mem[0],zero
	addsd	%xmm3, %xmm14
	movapd	%xmm3, %xmm7
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm14, %xmm0
	movsd	%xmm0, 1536(%rdi)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm4, %xmm0
	movapd	%xmm4, %xmm3
	movsd	%xmm0, 1024(%rdi)
	movsd	-96(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm14
	movsd	%xmm14, 512(%rdi)
	movsd	%xmm5, (%rdi)
	movsd	%xmm5, (%rsi)
	movapd	%xmm7, %xmm4
	addsd	%xmm4, %xmm0
	movsd	%xmm0, 512(%rsi)
	movsd	%xmm3, 1024(%rsi)
	movsd	%xmm4, 1536(%rsi)
	movsd	-64(%rsp), %xmm5        # 8-byte Reload
                                        # xmm5 = mem[0],zero
	addsd	%xmm5, %xmm9
	movsd	40(%rsp), %xmm7         # 8-byte Reload
                                        # xmm7 = mem[0],zero
	addsd	%xmm9, %xmm7
	movsd	-48(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm7, %xmm0
	movsd	%xmm0, 1792(%rdi)
	movsd	-24(%rsp), %xmm3        # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	64(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	addsd	%xmm3, %xmm4
	addsd	%xmm4, %xmm7
	movsd	%xmm7, 1280(%rdi)
	movsd	-120(%rsp), %xmm0       # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm9
	addsd	%xmm9, %xmm4
	movsd	%xmm4, 768(%rdi)
	movsd	-88(%rsp), %xmm4        # 8-byte Reload
                                        # xmm4 = mem[0],zero
	addsd	%xmm4, %xmm9
	movsd	%xmm9, 256(%rdi)
	addsd	%xmm5, %xmm0
	addsd	%xmm0, %xmm4
	movsd	%xmm4, 256(%rsi)
	addsd	%xmm3, %xmm0
	movsd	%xmm0, 768(%rsi)
	addsd	%xmm5, %xmm3
	movsd	%xmm3, 1280(%rsi)
	movsd	%xmm5, 1792(%rsi)
	addsd	%xmm13, %xmm1
	addsd	%xmm1, %xmm2
	addsd	%xmm2, %xmm8
	movsd	48(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm8, %xmm0
	movsd	%xmm0, 1920(%rdi)
	addsd	%xmm11, %xmm10
	addsd	%xmm10, %xmm12
	addsd	%xmm12, %xmm8
	movsd	%xmm8, 1664(%rdi)
	movsd	-104(%rsp), %xmm5       # 8-byte Reload
                                        # xmm5 = mem[0],zero
	addsd	%xmm5, %xmm6
	addsd	%xmm6, %xmm2
	addsd	%xmm2, %xmm12
	movsd	%xmm12, 1408(%rdi)
	movsd	-112(%rsp), %xmm3       # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	56(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm3, %xmm0
	addsd	%xmm0, %xmm2
	movsd	%xmm2, 1152(%rdi)
	addsd	%xmm15, %xmm1
	addsd	%xmm1, %xmm6
	addsd	%xmm6, %xmm0
	movsd	%xmm0, 896(%rdi)
	movsd	-80(%rsp), %xmm4        # 8-byte Reload
                                        # xmm4 = mem[0],zero
	addsd	%xmm4, %xmm10
	addsd	%xmm10, %xmm6
	movsd	%xmm6, 640(%rdi)
	movsd	-128(%rsp), %xmm0       # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm10
	movsd	%xmm10, 384(%rdi)
	movsd	-72(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm2, %xmm1
	movsd	%xmm1, 128(%rdi)
	addsd	%xmm13, %xmm15
	addsd	%xmm15, %xmm0
	addsd	%xmm0, %xmm2
	movsd	%xmm2, 128(%rsi)
	movapd	%xmm4, %xmm1
	addsd	%xmm11, %xmm1
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 384(%rsi)
	movapd	%xmm5, %xmm0
	addsd	%xmm0, %xmm15
	addsd	%xmm15, %xmm1
	movsd	%xmm1, 640(%rsi)
	movapd	%xmm3, %xmm1
	addsd	%xmm1, %xmm15
	movsd	%xmm15, 896(%rsi)
	addsd	%xmm13, %xmm0
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 1152(%rsi)
	addsd	%xmm11, %xmm0
	movsd	%xmm0, 1408(%rsi)
	addsd	%xmm13, %xmm11
	movsd	%xmm11, 1664(%rsi)
	movsd	%xmm13, 1920(%rsi)
	addq	$112, %rsp
	retq
.Lfunc_end0:
	.size	dct64, .Lfunc_end0-dct64
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
