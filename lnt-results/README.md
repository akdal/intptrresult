1. restrict branch:

- `20170819_base-4.0_restrict` : initial result (max. 38% slowdown)
- `20170825_base-4.0_restrict.2` :
    * Many restrict-related instcombine optimizations
    * Regard restrict as zero-cost function call
    * Let clang put more restricts in two special cases (see snippet1)
    * Let SimplifyCFG work well by simplifying redundant restrict calls when needed (see snippet2) 
```
<snippet1>
1. __fill_a
for(; p != pend; p++) {
  *p = e; // p is translated into 'call restrict(p, pend)'
} // p, q are all variables, e has no side effect
2. __copy_move_backward
for(n = pend - p; n > 0; --n)
  *(--r) = *(--pend); // pend is translated into 'call restrict(p, pend)'
```
```
<snippet2>
BB:
rp = call restrict p, nullptr // simplified into p
rq = call restrict nullptr, p // simplified into nullptr
c = icmp rp, rq
br c, ..
```

2. nogvn-only branch:                                                                            

- `20170813_base-4.0_nogvn` : nogvn branch (propagateEquality on p , q disabled if they are ptr types)
- `20170826_base-4.0_nogvn.2` : nogvn branch with (p == q) enabled if p and q has same underlying objects
- (until now branch name was 'nogvn')

3. noptri64punning branch :                                                                    

- `20170826_base-4.0_noptri64punning` : noptri64punning branch experimentation. Disable i64 <-> ty\* changes in InstCombine
- `20170828_base-4.0_noptri64punning` : disable inttoptr + ptrtoint -> bitcast.
- `20170828_base-4.0_noptri64punning` : disable inttoptr + ptrtoint generation in GVN
    * Why 6% slowdown in machine1? OTL There are less loads, nothing special, but why slower? I added IR & assembly of `yacr` benchmark into the directory.

4. psub branch :

- `20170512_base-4.0_psub` (contains capture implementation as well)

5. intptr-4.0 branch : 

- `20170501_intptr-4.0_fold-bitcast`

6. intptr-4.0-nocapture branch:

- `20170830.base-4.0.intptr-4.0-nocapture` : noptri64punning + psub (no capture) + restrict!
- `20170903.base-4.0.intptr-4.0-nocapture` : add psub optimizations, `disable icmp(ptrtoint p, ptrtoint q)->icmp(p, q)`

7. nogvn branch:

- `20170831.base-4.0.nogvn` : Low slowdown..!

8. restrict-pcmp branch:

- `20170904.base-4.0.restrict-pcmp` : good!
- `20170907.base-4.0.restrict-pcmp` : after removing illegal transformations, it has 15% slowdown.. why???

9. insts branch:

- `20170908.insts` : good.
- `20170909.insts`: 
```
Insert more newintoptr/newptrtoints
Disable folding 'select (icmp pred ptr1 ptr2) ptr1 ptr2',
Disable GVN for pointer comparison,
Disallow icmp (ptrtoint t) (ptrtoint s) -> icmp t s,
Let GVN do not generate redundant inttoptr(ptrtoint) pairs,
Disallow inttoptr(ptrtoint)->bitcast,   
Disable 'load-ptrtoint -> bitcast-load', 'load **p -> load i64*'
Add psub
```
- `20170910.insts`: Upto https://github.com/aqjune/llvm-intptr/commit/f638cf31eaf4bcee1d60d83af9a43af4bb8bb23c , https://github.com/aqjune/clang-intptr/commit/7f8d7701705081e963e36c428eedade67448de27
- `20170915.insts`: make modref conservative

10. insts2 branch:
- `20170916.insts2`: replace capture with newptrtoint

11. base-5.0 branch:
- `20170921.base-5.0`

12. insts-5.0 branch:
- `20170922.insts-5.0`

13. nocapture-5.0 branch:
- `20170922.nocapture-5.0`


---

- `20170924.base-5.0.insts-5.0.nocapture-5.0`: does not include instructionsimplify update
- `20170924.insts-5.0.nocapture-5.0.insts-opt-5.0.nocapture-opt-5.0`: insts-5.0, nocapture-5.0 include insructionsimplify update & \*-opt-\* has mod/ref initial implementation
- `20170928.insts-5.0.nocapture-5.0`: after disabling memset/memcpy optimizations
- `20170929.insts-5.0.nocapture-5.0`: after reenabling memset and store-load forwardings
- `20170929.insts-opt-5.0.nocapture-opt-5.0`: insts-opt-5.0, nocapture-opt-5.0
- `20170929.insts-opt-noprov-5.0.nocapture-opt-noprov-5.0`: Phy has no prov
