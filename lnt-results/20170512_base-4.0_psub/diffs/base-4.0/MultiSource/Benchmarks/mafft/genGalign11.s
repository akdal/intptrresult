	.text
	.file	"genGalign11.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4608533498688228557     # double 1.3
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_1:
	.long	3407386240              # float -1.0E+7
	.text
	.globl	genG__align11
	.p2align	4, 0x90
	.type	genG__align11,@function
genG__align11:                          # @genG__align11
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 208
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edx, 124(%rsp)         # 4-byte Spill
	movq	%rsi, %rbx
	cvtsi2ssl	penalty(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2ssl	penalty_OP(%rip), %xmm0
	movss	%xmm0, 64(%rsp)         # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2ssl	penalty_ex(%rip), %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	cmpl	$0, genG__align11.orlgth1(%rip)
	movq	%rdi, (%rsp)            # 8-byte Spill
	jne	.LBB0_2
# BB#1:
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, genG__align11.mseq1(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rax, genG__align11.mseq2(%rip)
.LBB0_2:
	movq	(%rdi), %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	(%rbx), %rdi
	callq	strlen
	movq	%rbp, %rdx
	movq	%rax, %r13
	testl	%edx, %edx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	jle	.LBB0_4
# BB#3:
	testl	%r13d, %r13d
	jle	.LBB0_4
.LBB0_5:
	movl	genG__align11.orlgth1(%rip), %r12d
	cmpl	%r12d, %edx
	movl	genG__align11.orlgth2(%rip), %r15d
	jg	.LBB0_7
# BB#6:
	cmpl	%r15d, %r13d
	jle	.LBB0_11
.LBB0_7:
	testl	%r12d, %r12d
	jle	.LBB0_10
# BB#8:
	testl	%r15d, %r15d
	jle	.LBB0_10
# BB#9:
	movq	genG__align11.w1(%rip), %rdi
	callq	FreeFloatVec
	movq	genG__align11.w2(%rip), %rdi
	callq	FreeFloatVec
	movq	genG__align11.match(%rip), %rdi
	callq	FreeFloatVec
	movq	genG__align11.initverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	genG__align11.lastverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	genG__align11.m(%rip), %rdi
	callq	FreeFloatVec
	movq	genG__align11.mp(%rip), %rdi
	callq	FreeIntVec
	movq	genG__align11.largeM(%rip), %rdi
	callq	FreeFloatVec
	movq	genG__align11.Mp(%rip), %rdi
	callq	FreeIntVec
	movq	genG__align11.mseq(%rip), %rdi
	callq	FreeCharMtx
	movq	genG__align11.cpmx1(%rip), %rdi
	callq	FreeFloatMtx
	movq	genG__align11.cpmx2(%rip), %rdi
	callq	FreeFloatMtx
	movq	genG__align11.floatwork(%rip), %rdi
	callq	FreeFloatMtx
	movq	genG__align11.intwork(%rip), %rdi
	callq	FreeIntMtx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	genG__align11.orlgth1(%rip), %r12d
	movl	genG__align11.orlgth2(%rip), %r15d
.LBB0_10:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r12d, %eax
	cmovgel	%eax, %r12d
	leal	100(%r12), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r15d, %eax
	cmovgel	%eax, %r15d
	leal	100(%r15), %ebp
	leal	102(%r15), %ebx
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, genG__align11.w1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, genG__align11.w2(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, genG__align11.match(%rip)
	leal	102(%r12), %r14d
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, genG__align11.initverticalw(%rip)
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, genG__align11.lastverticalw(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, genG__align11.m(%rip)
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, genG__align11.mp(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, genG__align11.largeM(%rip)
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, genG__align11.Mp(%rip)
	movl	njob(%rip), %edi
	leal	200(%r15,%r12), %esi
	callq	AllocateCharMtx
	movq	%rax, genG__align11.mseq(%rip)
	movl	$26, %edi
	movl	%r14d, %esi
	callq	AllocateFloatMtx
	movq	%rax, genG__align11.cpmx1(%rip)
	movl	$26, %edi
	movl	%ebx, %esi
	callq	AllocateFloatMtx
	movq	%rax, genG__align11.cpmx2(%rip)
	movl	40(%rsp), %eax          # 4-byte Reload
	cmpl	%ebp, %eax
	cmovgel	%eax, %ebp
	addl	$2, %ebp
	movl	$26, %edi
	movl	%ebp, %esi
	callq	AllocateFloatMtx
	movq	%rax, genG__align11.floatwork(%rip)
	movl	$26, %edi
	movl	%ebp, %esi
	callq	AllocateIntMtx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rax, genG__align11.intwork(%rip)
	movl	%r12d, genG__align11.orlgth1(%rip)
	movl	%r15d, genG__align11.orlgth2(%rip)
.LBB0_11:
	movq	(%rsp), %rsi            # 8-byte Reload
	movaps	48(%rsp), %xmm7         # 16-byte Reload
	movss	64(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm9          # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movq	genG__align11.mseq(%rip), %rax
	movq	(%rax), %rcx
	movq	genG__align11.mseq1(%rip), %rdi
	movq	%rcx, (%rdi)
	movq	8(%rax), %rax
	movq	genG__align11.mseq2(%rip), %rcx
	movq	%rax, (%rcx)
	movl	commonAlloc1(%rip), %ebp
	cmpl	%ebp, %r12d
	movl	commonAlloc2(%rip), %ebx
	jg	.LBB0_14
# BB#12:
	cmpl	%ebx, %r15d
	jg	.LBB0_14
# BB#13:                                # %._crit_edge357
	movq	commonJP(%rip), %rax
	jmp	.LBB0_18
.LBB0_14:                               # %._crit_edge351
	testl	%ebp, %ebp
	je	.LBB0_17
# BB#15:                                # %._crit_edge351
	testl	%ebx, %ebx
	je	.LBB0_17
# BB#16:
	movq	commonIP(%rip), %rdi
	callq	FreeIntMtx
	movq	commonJP(%rip), %rdi
	callq	FreeIntMtx
	movl	genG__align11.orlgth1(%rip), %r12d
	movl	commonAlloc1(%rip), %ebp
	movl	genG__align11.orlgth2(%rip), %r15d
	movl	commonAlloc2(%rip), %ebx
.LBB0_17:
	cmpl	%ebp, %r12d
	cmovgel	%r12d, %ebp
	cmpl	%ebx, %r15d
	cmovgel	%r15d, %ebx
	leal	10(%rbp), %r15d
	leal	10(%rbx), %r14d
	movl	%r15d, %edi
	movl	%r14d, %esi
	callq	AllocateIntMtx
	movq	%rax, commonIP(%rip)
	movl	%r15d, %edi
	movl	%r14d, %esi
	callq	AllocateIntMtx
	movq	%rax, commonJP(%rip)
	movl	%ebp, commonAlloc1(%rip)
	movl	%ebx, commonAlloc2(%rip)
	movq	(%rsp), %rsi            # 8-byte Reload
	movaps	48(%rsp), %xmm7         # 16-byte Reload
	movss	64(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm9          # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movq	16(%rsp), %rdx          # 8-byte Reload
.LBB0_18:
	movq	commonIP(%rip), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rcx, genG__align11.ijpi(%rip)
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, genG__align11.ijpj(%rip)
	movq	genG__align11.w1(%rip), %rcx
	movq	genG__align11.w2(%rip), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	genG__align11.initverticalw(%rip), %r8
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	(%r12), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	(%rsi), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	testl	%edx, %edx
	je	.LBB0_24
# BB#19:                                # %.lr.ph.i.preheader
	movq	32(%rsp), %rax          # 8-byte Reload
	movsbq	(%rax), %rbp
	testb	$1, %dl
	movq	%r8, %rdi
	movq	96(%rsp), %rbx          # 8-byte Reload
	movl	%edx, %esi
	je	.LBB0_21
# BB#20:                                # %.lr.ph.i.prol
	leal	-1(%rdx), %esi
	movq	96(%rsp), %rax          # 8-byte Reload
	leaq	1(%rax), %rbx
	movsbq	(%rax), %rdi
	movq	%rbp, %rax
	shlq	$9, %rax
	cvtsi2ssl	amino_dis(%rax,%rdi,4), %xmm0
	leaq	4(%r8), %rdi
	movss	%xmm0, (%r8)
.LBB0_21:                               # %.lr.ph.i.prol.loopexit
	cmpl	$1, %edx
	je	.LBB0_24
# BB#22:                                # %.lr.ph.i.preheader.new
	shlq	$9, %rbp
	.p2align	4, 0x90
.LBB0_23:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movsbq	(%rbx), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rbp,%rax,4), %xmm0
	movss	%xmm0, (%rdi)
	addl	$-2, %esi
	movsbq	1(%rbx), %rax
	leaq	2(%rbx), %rbx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rbp,%rax,4), %xmm0
	movss	%xmm0, 4(%rdi)
	leaq	8(%rdi), %rdi
	jne	.LBB0_23
.LBB0_24:                               # %match_calc.exit
	testl	%r13d, %r13d
	je	.LBB0_30
# BB#25:                                # %.lr.ph.i282.preheader
	movq	96(%rsp), %rax          # 8-byte Reload
	movsbq	(%rax), %rbp
	testb	$1, %r13b
	movq	%rcx, %rdi
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	%r13d, %esi
	je	.LBB0_27
# BB#26:                                # %.lr.ph.i282.prol
	leal	-1(%r13), %esi
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	1(%rax), %rbx
	movsbq	(%rax), %rdi
	movq	%rbp, %rax
	shlq	$9, %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rax,%rdi,4), %xmm0
	leaq	4(%rcx), %rdi
	movss	%xmm0, (%rcx)
.LBB0_27:                               # %.lr.ph.i282.prol.loopexit
	cmpl	$1, %r13d
	je	.LBB0_30
# BB#28:                                # %.lr.ph.i282.preheader.new
	shlq	$9, %rbp
	.p2align	4, 0x90
.LBB0_29:                               # %.lr.ph.i282
                                        # =>This Inner Loop Header: Depth=1
	movsbq	(%rbx), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rbp,%rax,4), %xmm0
	movss	%xmm0, (%rdi)
	addl	$-2, %esi
	movsbq	1(%rbx), %rax
	leaq	2(%rbx), %rbx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rbp,%rax,4), %xmm0
	movss	%xmm0, 4(%rdi)
	leaq	8(%rdi), %rdi
	jne	.LBB0_29
.LBB0_30:                               # %match_calc.exit284
	cmpl	$1, outgap(%rip)
	movq	%r13, 88(%rsp)          # 8-byte Spill
	movq	%r8, 144(%rsp)          # 8-byte Spill
	jne	.LBB0_43
# BB#31:                                # %.preheader293
	testl	%edx, %edx
	jle	.LBB0_40
# BB#32:                                # %.lr.ph328
	leal	1(%rdx), %eax
	leaq	-1(%rax), %rsi
	cmpq	$7, %rsi
	jbe	.LBB0_33
# BB#36:                                # %min.iters.checked
	movl	%edx, %edi
	andl	$7, %edi
	movq	%rsi, %rbx
	subq	%rdi, %rbx
	subq	%rdi, %rsi
	je	.LBB0_33
# BB#37:                                # %vector.ph
	incq	%rbx
	movaps	%xmm7, %xmm0
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	20(%r8), %rbp
	.p2align	4, 0x90
.LBB0_38:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbp), %xmm1
	movups	(%rbp), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	%xmm1, -16(%rbp)
	movups	%xmm2, (%rbp)
	addq	$32, %rbp
	addq	$-8, %rsi
	jne	.LBB0_38
# BB#39:                                # %middle.block
	testq	%rdi, %rdi
	jne	.LBB0_34
	jmp	.LBB0_40
.LBB0_33:
	movl	$1, %ebx
.LBB0_34:                               # %scalar.ph.preheader
	leaq	(%r8,%rbx,4), %rsi
	subq	%rbx, %rax
	.p2align	4, 0x90
.LBB0_35:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm0
	movss	%xmm0, (%rsi)
	addq	$4, %rsi
	decq	%rax
	jne	.LBB0_35
.LBB0_40:                               # %.preheader292
	testl	%r13d, %r13d
	jle	.LBB0_61
# BB#41:                                # %.lr.ph326.preheader
	leal	1(%r13), %eax
	leaq	-1(%rax), %rsi
	cmpq	$7, %rsi
	jbe	.LBB0_42
# BB#72:                                # %min.iters.checked380
	movl	%r13d, %edi
	andl	$7, %edi
	movq	%rsi, %rbx
	subq	%rdi, %rbx
	subq	%rdi, %rsi
	je	.LBB0_42
# BB#73:                                # %vector.ph384
	incq	%rbx
	movaps	%xmm7, %xmm0
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	20(%rcx), %rbp
	.p2align	4, 0x90
.LBB0_74:                               # %vector.body376
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbp), %xmm1
	movups	(%rbp), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	%xmm1, -16(%rbp)
	movups	%xmm2, (%rbp)
	addq	$32, %rbp
	addq	$-8, %rsi
	jne	.LBB0_74
# BB#75:                                # %middle.block377
	testq	%rdi, %rdi
	jne	.LBB0_76
	jmp	.LBB0_43
.LBB0_42:
	movl	$1, %ebx
.LBB0_76:                               # %.lr.ph326.preheader478
	leaq	(%rcx,%rbx,4), %rsi
	subq	%rbx, %rax
	.p2align	4, 0x90
.LBB0_77:                               # %.lr.ph326
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm0
	movss	%xmm0, (%rsi)
	addq	$4, %rsi
	decq	%rax
	jne	.LBB0_77
.LBB0_43:                               # %.preheader
	testl	%r13d, %r13d
	jle	.LBB0_61
# BB#44:                                # %.lr.ph323
	movq	genG__align11.m(%rip), %rax
	movq	genG__align11.mp(%rip), %r10
	movq	genG__align11.largeM(%rip), %rsi
	movq	genG__align11.Mp(%rip), %rdi
	leaq	1(%r13), %r9
	movl	%r9d, %r11d
	leaq	-1(%r11), %rbp
	cmpq	$7, %rbp
	jbe	.LBB0_45
# BB#50:                                # %min.iters.checked403
	movl	%r13d, %ebx
	andl	$7, %ebx
	movq	%rbp, %r14
	subq	%rbx, %r14
	je	.LBB0_45
# BB#51:                                # %vector.memcheck
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	leaq	4(%rax), %rbx
	leaq	(%rax,%r11,4), %r15
	leaq	4(%rsi), %r12
	leaq	(%rsi,%r11,4), %rbp
	cmpq	%rbp, %rbx
	sbbb	%r13b, %r13b
	cmpq	%r15, %r12
	sbbb	%r8b, %r8b
	andb	%r13b, %r8b
	leaq	-4(%rcx,%r11,4), %r13
	cmpq	%r13, %rbx
	sbbb	%bl, %bl
	cmpq	%r15, %rcx
	sbbb	%dl, %dl
	movb	%dl, 112(%rsp)          # 1-byte Spill
	cmpq	%r13, %r12
	leaq	4(%r10), %r15
	leaq	(%rdi,%r11,4), %r12
	sbbb	%r13b, %r13b
	cmpq	%rbp, %rcx
	sbbb	%dl, %dl
	cmpq	%r12, %r15
	leaq	(%r10,%r11,4), %r15
	leaq	4(%rdi), %rbp
	sbbb	%r12b, %r12b
	cmpq	%r15, %rbp
	sbbb	%bpl, %bpl
	testb	$1, %r8b
	movl	$1, %r15d
	jne	.LBB0_52
# BB#53:                                # %vector.memcheck
	movl	%edx, %r8d
	andb	112(%rsp), %bl          # 1-byte Folded Reload
	andb	$1, %bl
	movq	16(%rsp), %rdx          # 8-byte Reload
	jne	.LBB0_54
# BB#55:                                # %vector.memcheck
	movl	%ebp, %ebx
	andb	%r8b, %r13b
	andb	$1, %r13b
	movq	64(%rsp), %rbp          # 8-byte Reload
	jne	.LBB0_56
# BB#57:                                # %vector.memcheck
	andb	%bl, %r12b
	andb	$1, %r12b
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	88(%rsp), %r13          # 8-byte Reload
	jne	.LBB0_46
# BB#58:                                # %vector.body399.preheader
	leaq	1(%r14), %r15
	xorl	%ebx, %ebx
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_59:                               # %vector.body399
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rbx,4), %xmm1
	movups	16(%rcx,%rbx,4), %xmm2
	movups	%xmm1, 4(%rax,%rbx,4)
	movups	%xmm2, 20(%rax,%rbx,4)
	movupd	%xmm0, 4(%r10,%rbx,4)
	movupd	%xmm0, 20(%r10,%rbx,4)
	movupd	(%rcx,%rbx,4), %xmm1
	movups	16(%rcx,%rbx,4), %xmm2
	movupd	%xmm1, 4(%rsi,%rbx,4)
	movups	%xmm2, 20(%rsi,%rbx,4)
	movupd	%xmm0, 4(%rdi,%rbx,4)
	movupd	%xmm0, 20(%rdi,%rbx,4)
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.LBB0_59
# BB#60:                                # %middle.block400
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	jne	.LBB0_46
	jmp	.LBB0_61
.LBB0_45:
	movl	$1, %r15d
.LBB0_46:                               # %scalar.ph401.preheader
	subl	%r15d, %r9d
	testb	$1, %r9b
	movq	%r15, %rbx
	je	.LBB0_48
# BB#47:                                # %scalar.ph401.prol
	movl	-4(%rcx,%r15,4), %ebx
	movl	%ebx, (%rax,%r15,4)
	movl	$0, (%r10,%r15,4)
	movl	-4(%rcx,%r15,4), %ebx
	movl	%ebx, (%rsi,%r15,4)
	movl	$0, (%rdi,%r15,4)
	leaq	1(%r15), %rbx
.LBB0_48:                               # %scalar.ph401.prol.loopexit
	cmpq	%r15, %rbp
	je	.LBB0_61
	.p2align	4, 0x90
.LBB0_49:                               # %scalar.ph401
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rcx,%rbx,4), %ebp
	movl	%ebp, (%rax,%rbx,4)
	movl	$0, (%r10,%rbx,4)
	movl	-4(%rcx,%rbx,4), %ebp
	movl	%ebp, (%rsi,%rbx,4)
	movl	$0, (%rdi,%rbx,4)
	movl	(%rcx,%rbx,4), %ebp
	movl	%ebp, 4(%rax,%rbx,4)
	movl	$0, 4(%r10,%rbx,4)
	movl	(%rcx,%rbx,4), %ebp
	movl	%ebp, 4(%rsi,%rbx,4)
	movl	$0, 4(%rdi,%rbx,4)
	addq	$2, %rbx
	cmpq	%rbx, %r11
	jne	.LBB0_49
.LBB0_61:                               # %._crit_edge324
	testl	%r13d, %r13d
	xorps	%xmm6, %xmm6
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	xorpd	%xmm0, %xmm0
	je	.LBB0_63
# BB#62:
	movq	%r13, %rax
	shlq	$32, %rax
	addq	%rsi, %rax
	sarq	$30, %rax
	movss	(%rcx,%rax), %xmm0      # xmm0 = mem[0],zero,zero,zero
.LBB0_63:
	movq	genG__align11.lastverticalw(%rip), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movss	%xmm0, (%rax)
	cmpl	$1, outgap(%rip)
	sbbl	$-1, %edx
	cmpl	$2, %edx
	jl	.LBB0_97
# BB#64:                                # %.lr.ph318
	movq	genG__align11.m(%rip), %rax
	movq	genG__align11.largeM(%rip), %r10
	movq	genG__align11.mp(%rip), %r14
	movq	genG__align11.Mp(%rip), %r12
	movq	%r13, %rdi
	shlq	$32, %rdi
	addq	%rsi, %rdi
	sarq	$32, %rdi
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	leal	1(%r13), %r8d
	movl	%edx, %edx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movl	%r13d, %edx
	andl	$1, %edx
	movl	%edx, 132(%rsp)         # 4-byte Spill
	leal	-1(%r13), %edx
	movl	%edx, 128(%rsp)         # 4-byte Spill
	movq	32(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	addq	$-2, %r8
	xorps	%xmm6, %xmm6
	movl	$1, %edi
	movss	.LCPI0_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB0_65:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_69 Depth 2
                                        #     Child Loop BB0_79 Depth 2
	movq	%rcx, %r11
	testl	%r13d, %r13d
	movq	144(%rsp), %r9          # 8-byte Reload
	movl	-4(%r9,%rdi,4), %ecx
	movl	%ecx, (%r11)
	movq	%rdi, %r15
	je	.LBB0_70
# BB#66:                                # %.lr.ph.i288.preheader
                                        #   in Loop: Header=BB0_65 Depth=1
	movq	96(%rsp), %rcx          # 8-byte Reload
	movsbq	(%rcx,%r15), %rsi
	cmpl	$0, 132(%rsp)           # 4-byte Folded Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%r13d, %ebp
	je	.LBB0_68
# BB#67:                                # %.lr.ph.i288.prol
                                        #   in Loop: Header=BB0_65 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movsbq	(%rcx), %rdx
	movq	%rsi, %rdi
	shlq	$9, %rdi
	cvtsi2ssl	amino_dis(%rdi,%rdx,4), %xmm1
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	4(%rcx), %rdx
	movss	%xmm1, (%rcx)
	movq	136(%rsp), %rdi         # 8-byte Reload
	movl	128(%rsp), %ebp         # 4-byte Reload
.LBB0_68:                               # %.lr.ph.i288.prol.loopexit
                                        #   in Loop: Header=BB0_65 Depth=1
	cmpl	$1, %r13d
	je	.LBB0_70
	.p2align	4, 0x90
.LBB0_69:                               # %.lr.ph.i288
                                        #   Parent Loop BB0_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	(%rdi), %rbx
	movq	%rsi, %rcx
	shlq	$9, %rcx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	amino_dis(%rcx,%rbx,4), %xmm1
	movss	%xmm1, (%rdx)
	addl	$-2, %ebp
	movsbq	1(%rdi), %rbx
	leaq	2(%rdi), %rdi
	xorps	%xmm1, %xmm1
	cvtsi2ssl	amino_dis(%rcx,%rbx,4), %xmm1
	movss	%xmm1, 4(%rdx)
	leaq	8(%rdx), %rdx
	jne	.LBB0_69
.LBB0_70:                               # %match_calc.exit290
                                        #   in Loop: Header=BB0_65 Depth=1
	testl	%r13d, %r13d
	movl	(%r9,%r15,4), %ecx
	movq	48(%rsp), %rdx          # 8-byte Reload
	movl	%ecx, (%rdx)
	movl	(%r11), %ecx
	movl	%ecx, genG__align11.mi(%rip)
	movl	%ecx, genG__align11.Mi(%rip)
	movq	%r15, 64(%rsp)          # 8-byte Spill
	jle	.LBB0_71
# BB#78:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_65 Depth=1
	leaq	-1(%r15), %rdi
	movd	%ecx, %xmm1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%r15,8), %rcx
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx,%r15,8), %rbp
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	movdqa	%xmm1, %xmm2
	movdqa	%xmm1, %xmm4
	xorl	%r9d, %r9d
	xorl	%r13d, %r13d
	xorl	%edx, %edx
	movaps	%xmm0, %xmm3
	jmp	.LBB0_79
	.p2align	4, 0x90
.LBB0_140:                              # %..lr.ph_crit_edge
                                        #   in Loop: Header=BB0_79 Depth=2
	movss	4(%r11,%rbx,4), %xmm4   # xmm4 = mem[0],zero,zero,zero
	incq	%rbx
.LBB0_79:                               # %.lr.ph
                                        #   Parent Loop BB0_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, 4(%rbp,%rbx,4)
	movaps	%xmm7, %xmm6
	addss	%xmm2, %xmm6
	ucomiss	%xmm4, %xmm6
	movl	%ebx, %esi
	cmoval	%r9d, %esi
	maxss	%xmm4, %xmm6
	movl	%esi, 4(%rcx,%rbx,4)
	ucomiss	%xmm2, %xmm4
	jb	.LBB0_81
# BB#80:                                #   in Loop: Header=BB0_79 Depth=2
	movss	%xmm4, genG__align11.mi(%rip)
	movaps	%xmm4, %xmm2
	movl	%ebx, %r9d
.LBB0_81:                               #   in Loop: Header=BB0_79 Depth=2
	addss	%xmm9, %xmm2
	movss	%xmm2, genG__align11.mi(%rip)
	movss	4(%rax,%rbx,4), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm5
	addss	%xmm4, %xmm5
	ucomiss	%xmm6, %xmm5
	jbe	.LBB0_83
# BB#82:                                #   in Loop: Header=BB0_79 Depth=2
	movl	4(%r14,%rbx,4), %esi
	movl	%esi, 4(%rbp,%rbx,4)
	movl	%ebx, 4(%rcx,%rbx,4)
	movaps	%xmm5, %xmm6
.LBB0_83:                               #   in Loop: Header=BB0_79 Depth=2
	movss	(%r11,%rbx,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	ucomiss	%xmm4, %xmm5
	jb	.LBB0_85
# BB#84:                                #   in Loop: Header=BB0_79 Depth=2
	movss	%xmm5, 4(%rax,%rbx,4)
	movl	%edi, 4(%r14,%rbx,4)
.LBB0_85:                               #   in Loop: Header=BB0_79 Depth=2
	movss	4(%rax,%rbx,4), %xmm4   # xmm4 = mem[0],zero,zero,zero
	addss	%xmm9, %xmm4
	movss	%xmm4, 4(%rax,%rbx,4)
	movaps	%xmm8, %xmm4
	addss	%xmm3, %xmm4
	ucomiss	%xmm6, %xmm4
	jbe	.LBB0_87
# BB#86:                                #   in Loop: Header=BB0_79 Depth=2
	movl	%edx, 4(%rbp,%rbx,4)
	movl	%r13d, 4(%rcx,%rbx,4)
	movaps	%xmm4, %xmm6
.LBB0_87:                               #   in Loop: Header=BB0_79 Depth=2
	ucomiss	%xmm3, %xmm1
	movdqa	%xmm1, %xmm4
	maxss	%xmm3, %xmm4
	cmoval	%edi, %edx
	cmoval	%r15d, %r13d
	movss	4(%r10,%rbx,4), %xmm5   # xmm5 = mem[0],zero,zero,zero
	ucomiss	%xmm4, %xmm5
	movaps	%xmm4, %xmm3
	jbe	.LBB0_89
# BB#88:                                #   in Loop: Header=BB0_79 Depth=2
	movl	4(%r12,%rbx,4), %edx
	movaps	%xmm5, %xmm3
	movl	%ebx, %r13d
.LBB0_89:                               #   in Loop: Header=BB0_79 Depth=2
	movd	(%r11,%rbx,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm5, %xmm4
	jbe	.LBB0_91
# BB#90:                                #   in Loop: Header=BB0_79 Depth=2
	movd	%xmm4, 4(%r10,%rbx,4)
	movl	%edi, 4(%r12,%rbx,4)
	movd	(%r11,%rbx,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
.LBB0_91:                               #   in Loop: Header=BB0_79 Depth=2
	ucomiss	%xmm1, %xmm4
	jbe	.LBB0_93
# BB#92:                                #   in Loop: Header=BB0_79 Depth=2
	movd	%xmm4, genG__align11.Mi(%rip)
	movl	%ebx, %r15d
	movdqa	%xmm4, %xmm1
.LBB0_93:                               #   in Loop: Header=BB0_79 Depth=2
	movq	48(%rsp), %rsi          # 8-byte Reload
	movss	4(%rsi,%rbx,4), %xmm4   # xmm4 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm4
	movss	%xmm4, 4(%rsi,%rbx,4)
	cmpq	%rbx, %r8
	jne	.LBB0_140
# BB#94:                                #   in Loop: Header=BB0_65 Depth=1
	movq	88(%rsp), %r13          # 8-byte Reload
	jmp	.LBB0_95
	.p2align	4, 0x90
.LBB0_71:                               #   in Loop: Header=BB0_65 Depth=1
	xorl	%r15d, %r15d
	xorl	%r9d, %r9d
.LBB0_95:                               # %._crit_edge
                                        #   in Loop: Header=BB0_65 Depth=1
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movl	(%rdx,%rcx,4), %ecx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	%ecx, (%rsi,%rdi,4)
	incq	%rdi
	cmpq	104(%rsp), %rdi         # 8-byte Folded Reload
	movq	%rdx, %rcx
	movq	%r11, 48(%rsp)          # 8-byte Spill
	jne	.LBB0_65
# BB#96:                                # %._crit_edge319
	movl	%r15d, genG__align11.Mpi(%rip)
	movl	%r9d, genG__align11.mpi(%rip)
	movq	24(%rsp), %r12          # 8-byte Reload
.LBB0_97:
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	movq	genG__align11.mseq1(%rip), %r15
	movq	genG__align11.mseq2(%rip), %r13
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	strlen
	movq	%rax, %r14
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	strlen
	testl	%r14d, %r14d
	movq	(%rsp), %r11            # 8-byte Reload
	js	.LBB0_104
# BB#98:                                # %.lr.ph19.preheader.i
	movq	%r14, %rdi
	incq	%rdi
	movl	%edi, %ecx
	leaq	-1(%rcx), %r8
	xorl	%esi, %esi
	andq	$3, %rdi
	je	.LBB0_101
# BB#99:                                # %.lr.ph19.i.prol.preheader
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_100:                              # %.lr.ph19.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rsi,8), %rbp
	movl	$-1, (%rbp)
	movq	(%rbx,%rsi,8), %rbp
	movl	$-1, (%rbp)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB0_100
.LBB0_101:                              # %.lr.ph19.i.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB0_104
# BB#102:                               # %.lr.ph19.preheader.i.new
	subq	%rsi, %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	24(%rdx,%rsi,8), %rdx
	movq	40(%rsp), %rdi          # 8-byte Reload
	leaq	24(%rdi,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB0_103:                              # %.lr.ph19.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%rsi), %rdi
	movl	$-1, (%rdi)
	movq	-24(%rdx), %rdi
	movl	$-1, (%rdi)
	movq	-16(%rsi), %rdi
	movl	$-1, (%rdi)
	movq	-16(%rdx), %rdi
	movl	$-1, (%rdi)
	movq	-8(%rsi), %rdi
	movl	$-1, (%rdi)
	movq	-8(%rdx), %rdi
	movl	$-1, (%rdi)
	movq	(%rsi), %rdi
	movl	$-1, (%rdi)
	movq	(%rdx), %rdi
	movl	$-1, (%rdi)
	addq	$32, %rdx
	addq	$32, %rsi
	addq	$-4, %rcx
	jne	.LBB0_103
.LBB0_104:                              # %.preheader.i
	testl	%eax, %eax
	js	.LBB0_119
# BB#105:                               # %.lr.ph15.i
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %r9
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx), %r10
	movq	%rax, %rdi
	incq	%rdi
	movl	%edi, %ebx
	cmpq	$7, %rbx
	jbe	.LBB0_106
# BB#113:                               # %min.iters.checked455
	movl	%edi, %r8d
	andl	$7, %r8d
	movq	%rbx, %rcx
	subq	%r8, %rcx
	je	.LBB0_106
# BB#114:                               # %vector.memcheck468
	leaq	(%r10,%rbx,4), %rdx
	cmpq	%rdx, %r9
	jae	.LBB0_116
# BB#115:                               # %vector.memcheck468
	leaq	(%r9,%rbx,4), %rdx
	cmpq	%rdx, %r10
	jae	.LBB0_116
.LBB0_106:
	xorl	%ecx, %ecx
.LBB0_107:                              # %scalar.ph453.preheader
	subl	%ecx, %edi
	leaq	-1(%rbx), %r8
	subq	%rcx, %r8
	andq	$7, %rdi
	je	.LBB0_110
# BB#108:                               # %scalar.ph453.prol.preheader
	negq	%rdi
	.p2align	4, 0x90
.LBB0_109:                              # %scalar.ph453.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, (%r9,%rcx,4)
	movl	$-1, (%r10,%rcx,4)
	incq	%rcx
	incq	%rdi
	jne	.LBB0_109
.LBB0_110:                              # %scalar.ph453.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB0_119
# BB#111:                               # %scalar.ph453.preheader.new
	subq	%rcx, %rbx
	leaq	28(%r10,%rcx,4), %rsi
	leaq	28(%r9,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB0_112:                              # %scalar.ph453
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, -28(%rcx)
	movl	$-1, -28(%rsi)
	movl	$-1, -24(%rcx)
	movl	$-1, -24(%rsi)
	movl	$-1, -20(%rcx)
	movl	$-1, -20(%rsi)
	movl	$-1, -16(%rcx)
	movl	$-1, -16(%rsi)
	movl	$-1, -12(%rcx)
	movl	$-1, -12(%rsi)
	movl	$-1, -8(%rcx)
	movl	$-1, -8(%rsi)
	movl	$-1, -4(%rcx)
	movl	$-1, -4(%rsi)
	movl	$-1, (%rcx)
	movl	$-1, (%rsi)
	addq	$32, %rsi
	addq	$32, %rcx
	addq	$-8, %rbx
	jne	.LBB0_112
.LBB0_119:                              # %._crit_edge16.i
	leal	(%rax,%r14), %edx
	movq	(%r15), %rcx
	movl	%edx, 48(%rsp)          # 4-byte Spill
	movslq	%edx, %rdx
	leaq	(%rcx,%rdx), %rsi
	movq	%rsi, (%r15)
	movb	$0, (%rcx,%rdx)
	movq	(%r13), %rcx
	leaq	(%rcx,%rdx), %rsi
	movq	%rsi, (%r13)
	movb	$0, (%rcx,%rdx)
	testl	%edx, %edx
	js	.LBB0_136
# BB#120:                               # %.lr.ph11.i.preheader
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB0_121:                              # %.lr.ph11.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_125 Depth 2
                                        #     Child Loop BB0_131 Depth 2
	movslq	%r14d, %rdi
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rdi,8), %rcx
	movslq	%eax, %rbp
	movl	(%rcx,%rbp,4), %r10d
	movslq	%r10d, %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rdx,%rdi,8), %rbx
	movl	(%rbx,%rbp,4), %r9d
	movslq	%r9d, %rbx
	subl	%ecx, %edi
	decl	%edi
	je	.LBB0_127
# BB#122:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_121 Depth=1
	movslq	%edi, %rdi
	leal	-2(%r14), %r12d
	testb	$1, %dil
	je	.LBB0_124
# BB#123:                               # %.lr.ph.i278.prol
                                        #   in Loop: Header=BB0_121 Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	(%rdx), %r11
	leaq	(%rdi,%rcx), %rsi
	movb	(%r11,%rsi), %r11b
	movq	(%r15), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, (%r15)
	movb	%r11b, -1(%rsi)
	movq	(%rsp), %r11            # 8-byte Reload
	movq	(%r13), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, (%r13)
	movb	$45, -1(%rsi)
	decq	%rdi
.LBB0_124:                              # %.lr.ph.i278.prol.loopexit
                                        #   in Loop: Header=BB0_121 Depth=1
	cmpl	%r10d, %r12d
	je	.LBB0_126
	.p2align	4, 0x90
.LBB0_125:                              # %.lr.ph.i278
                                        #   Parent Loop BB0_121 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r11), %rsi
	addq	%rcx, %rsi
	movzbl	(%rdi,%rsi), %edx
	movq	(%r15), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, (%r15)
	movb	%dl, -1(%rsi)
	movq	(%r13), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r13)
	movb	$45, -1(%rdx)
	movq	(%r11), %rdx
	addq	%rcx, %rdx
	movzbl	-1(%rdi,%rdx), %edx
	movq	(%r15), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, (%r15)
	movb	%dl, -1(%rsi)
	movq	(%r13), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r13)
	movb	$45, -1(%rdx)
	addq	$-2, %rdi
	testl	%edi, %edi
	jne	.LBB0_125
.LBB0_126:                              # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB0_121 Depth=1
	leal	-1(%r8,%r14), %r8d
	subl	%ecx, %r8d
	movq	24(%rsp), %r12          # 8-byte Reload
.LBB0_127:                              # %._crit_edge.i
                                        #   in Loop: Header=BB0_121 Depth=1
	movl	%eax, %edi
	subl	%ebx, %edi
	decl	%edi
	je	.LBB0_133
# BB#128:                               # %.lr.ph4.preheader.i
                                        #   in Loop: Header=BB0_121 Depth=1
	movslq	%edi, %rdi
	leal	-2(%rax), %ebp
	testb	$1, %dil
	je	.LBB0_130
# BB#129:                               # %.lr.ph4.i.prol
                                        #   in Loop: Header=BB0_121 Depth=1
	movq	(%r15), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r15)
	movb	$45, -1(%rdx)
	movq	(%r12), %rdx
	leaq	(%rdi,%rbx), %rsi
	movb	(%rdx,%rsi), %r11b
	movq	(%r13), %rsi
	leaq	-1(%rsi), %rdx
	movq	%rdx, (%r13)
	movb	%r11b, -1(%rsi)
	movq	(%rsp), %r11            # 8-byte Reload
	decq	%rdi
.LBB0_130:                              # %.lr.ph4.i.prol.loopexit
                                        #   in Loop: Header=BB0_121 Depth=1
	cmpl	%ebx, %ebp
	je	.LBB0_132
	.p2align	4, 0x90
.LBB0_131:                              # %.lr.ph4.i
                                        #   Parent Loop BB0_121 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r15)
	movb	$45, -1(%rdx)
	movq	(%r12), %rdx
	addq	%rbx, %rdx
	movzbl	(%rdi,%rdx), %edx
	movq	(%r13), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, (%r13)
	movb	%dl, -1(%rsi)
	movq	(%r15), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r15)
	movb	$45, -1(%rdx)
	movq	(%r12), %rdx
	addq	%rbx, %rdx
	movzbl	-1(%rdi,%rdx), %edx
	movq	(%r13), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, (%r13)
	movb	%dl, -1(%rsi)
	addq	$-2, %rdi
	testl	%edi, %edi
	jne	.LBB0_131
.LBB0_132:                              # %._crit_edge5.loopexit.i
                                        #   in Loop: Header=BB0_121 Depth=1
	leal	-1(%rax), %edx
	subl	%ebx, %edx
	addl	%r8d, %edx
	movl	%edx, %r8d
.LBB0_133:                              # %._crit_edge5.i
                                        #   in Loop: Header=BB0_121 Depth=1
	testl	%r14d, %r14d
	jle	.LBB0_136
# BB#134:                               # %._crit_edge5.i
                                        #   in Loop: Header=BB0_121 Depth=1
	testl	%eax, %eax
	jle	.LBB0_136
# BB#135:                               #   in Loop: Header=BB0_121 Depth=1
	movq	(%r11), %rax
	movb	(%rax,%rcx), %al
	movq	(%r15), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%r15)
	movb	%al, -1(%rcx)
	movq	(%r12), %rax
	movb	(%rax,%rbx), %al
	movq	(%r13), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%r13)
	movb	%al, -1(%rcx)
	addl	$2, %r8d
	cmpl	48(%rsp), %r8d          # 4-byte Folded Reload
	movl	%r10d, %r14d
	movl	%r9d, %eax
	jle	.LBB0_121
.LBB0_136:                              # %genGtracking.exit
	movq	(%r15), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rcx
	movl	124(%rsp), %edx         # 4-byte Reload
	cmpl	%edx, %ecx
	jg	.LBB0_138
# BB#137:                               # %genGtracking.exit
	cmpl	$5000001, %ecx          # imm = 0x4C4B41
	jge	.LBB0_138
.LBB0_139:
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %rdi
	movq	%rbp, %rsi
	callq	strcpy
	movq	(%r12), %rdi
	movq	genG__align11.mseq2(%rip), %rax
	movq	(%rax), %rsi
	callq	strcpy
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_116:                              # %vector.body451.preheader
	leaq	16(%r9), %rbp
	leaq	16(%r10), %rdx
	pcmpeqd	%xmm0, %xmm0
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB0_117:                              # %vector.body451
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbp)
	movdqu	%xmm0, (%rbp)
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	addq	$32, %rbp
	addq	$32, %rdx
	addq	$-8, %rsi
	jne	.LBB0_117
# BB#118:                               # %middle.block452
	testq	%r8, %r8
	jne	.LBB0_107
	jmp	.LBB0_119
.LBB0_4:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r13d, %ecx
	callq	fprintf
	movq	16(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB0_5
.LBB0_138:
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	movl	$5000000, %r8d          # imm = 0x4C4B40
	xorl	%eax, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$.L.str.2, %edi
	callq	ErrorExit
	movq	genG__align11.mseq1(%rip), %rax
	movq	(%rax), %rbp
	jmp	.LBB0_139
.LBB0_56:
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	88(%rsp), %r13          # 8-byte Reload
	jmp	.LBB0_46
.LBB0_52:
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	88(%rsp), %r13          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_46
.LBB0_54:
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	88(%rsp), %r13          # 8-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_46
.Lfunc_end0:
	.size	genG__align11, .Lfunc_end0-genG__align11
	.cfi_endproc

	.type	genG__align11.mi,@object # @genG__align11.mi
	.local	genG__align11.mi
	.comm	genG__align11.mi,4,4
	.type	genG__align11.m,@object # @genG__align11.m
	.local	genG__align11.m
	.comm	genG__align11.m,8,8
	.type	genG__align11.Mi,@object # @genG__align11.Mi
	.local	genG__align11.Mi
	.comm	genG__align11.Mi,4,4
	.type	genG__align11.largeM,@object # @genG__align11.largeM
	.local	genG__align11.largeM
	.comm	genG__align11.largeM,8,8
	.type	genG__align11.ijpi,@object # @genG__align11.ijpi
	.local	genG__align11.ijpi
	.comm	genG__align11.ijpi,8,8
	.type	genG__align11.ijpj,@object # @genG__align11.ijpj
	.local	genG__align11.ijpj
	.comm	genG__align11.ijpj,8,8
	.type	genG__align11.mpi,@object # @genG__align11.mpi
	.local	genG__align11.mpi
	.comm	genG__align11.mpi,4,4
	.type	genG__align11.mp,@object # @genG__align11.mp
	.local	genG__align11.mp
	.comm	genG__align11.mp,8,8
	.type	genG__align11.Mpi,@object # @genG__align11.Mpi
	.local	genG__align11.Mpi
	.comm	genG__align11.Mpi,4,4
	.type	genG__align11.Mp,@object # @genG__align11.Mp
	.local	genG__align11.Mp
	.comm	genG__align11.Mp,8,8
	.type	genG__align11.w1,@object # @genG__align11.w1
	.local	genG__align11.w1
	.comm	genG__align11.w1,8,8
	.type	genG__align11.w2,@object # @genG__align11.w2
	.local	genG__align11.w2
	.comm	genG__align11.w2,8,8
	.type	genG__align11.match,@object # @genG__align11.match
	.local	genG__align11.match
	.comm	genG__align11.match,8,8
	.type	genG__align11.initverticalw,@object # @genG__align11.initverticalw
	.local	genG__align11.initverticalw
	.comm	genG__align11.initverticalw,8,8
	.type	genG__align11.lastverticalw,@object # @genG__align11.lastverticalw
	.local	genG__align11.lastverticalw
	.comm	genG__align11.lastverticalw,8,8
	.type	genG__align11.mseq1,@object # @genG__align11.mseq1
	.local	genG__align11.mseq1
	.comm	genG__align11.mseq1,8,8
	.type	genG__align11.mseq2,@object # @genG__align11.mseq2
	.local	genG__align11.mseq2
	.comm	genG__align11.mseq2,8,8
	.type	genG__align11.mseq,@object # @genG__align11.mseq
	.local	genG__align11.mseq
	.comm	genG__align11.mseq,8,8
	.type	genG__align11.cpmx1,@object # @genG__align11.cpmx1
	.local	genG__align11.cpmx1
	.comm	genG__align11.cpmx1,8,8
	.type	genG__align11.cpmx2,@object # @genG__align11.cpmx2
	.local	genG__align11.cpmx2
	.comm	genG__align11.cpmx2,8,8
	.type	genG__align11.intwork,@object # @genG__align11.intwork
	.local	genG__align11.intwork
	.comm	genG__align11.intwork,8,8
	.type	genG__align11.floatwork,@object # @genG__align11.floatwork
	.local	genG__align11.floatwork
	.comm	genG__align11.floatwork,8,8
	.type	genG__align11.orlgth1,@object # @genG__align11.orlgth1
	.local	genG__align11.orlgth1
	.comm	genG__align11.orlgth1,4,4
	.type	genG__align11.orlgth2,@object # @genG__align11.orlgth2
	.local	genG__align11.orlgth2
	.comm	genG__align11.orlgth2,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"WARNING (g11): lgth1=%d, lgth2=%d\n"
	.size	.L.str, 35

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"alloclen=%d, resultlen=%d, N=%d\n"
	.size	.L.str.1, 33

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"LENGTH OVER!\n"
	.size	.L.str.2, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
