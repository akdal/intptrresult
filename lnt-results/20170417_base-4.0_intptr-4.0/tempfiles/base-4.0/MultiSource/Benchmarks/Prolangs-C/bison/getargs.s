	.text
	.file	"getargs.bc"
	.globl	getargs
	.p2align	4, 0x90
	.type	getargs,@function
getargs:                                # @getargs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movq	(%rbx), %rdi
	movl	$0, verboseflag(%rip)
	movl	$0, definesflag(%rip)
	movl	$0, debugflag(%rip)
	movl	$0, fixed_outfiles(%rip)
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_4:                                #   in Loop: Header=BB0_1 Depth=1
	movq	%rax, %rdi
.LBB0_1:                                # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_2 Depth 2
	leaq	1(%rdi), %rax
	jmp	.LBB0_2
	.p2align	4, 0x90
.LBB0_5:                                # %._crit_edge
                                        #   in Loop: Header=BB0_2 Depth=2
	incq	%rax
.LBB0_2:                                #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rax), %ecx
	testb	%cl, %cl
	je	.LBB0_6
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=2
	cmpb	$47, %cl
	jne	.LBB0_5
	jmp	.LBB0_4
.LBB0_6:
	movl	$.L.str, %esi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB0_8
.LBB0_7:
	movl	$1, fixed_outfiles(%rip)
	jmp	.LBB0_8
.LBB0_17:                               #   in Loop: Header=BB0_8 Depth=1
	movq	optarg(%rip), %rax
	movq	%rax, spec_outfile(%rip)
	.p2align	4, 0x90
.LBB0_8:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.1, %edx
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	getopt
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-100(%rax), %ecx
	cmpl	$21, %ecx
	ja	.LBB0_9
# BB#12:                                # %.preheader
                                        #   in Loop: Header=BB0_8 Depth=1
	jmpq	*.LJTI0_0(,%rcx,8)
.LBB0_14:                               #   in Loop: Header=BB0_8 Depth=1
	movl	$1, definesflag(%rip)
	jmp	.LBB0_8
	.p2align	4, 0x90
.LBB0_9:                                # %.preheader
                                        #   in Loop: Header=BB0_8 Depth=1
	cmpl	$-1, %eax
	jne	.LBB0_8
	jmp	.LBB0_10
.LBB0_15:                               #   in Loop: Header=BB0_8 Depth=1
	movl	$1, nolinesflag(%rip)
	jmp	.LBB0_8
.LBB0_16:                               #   in Loop: Header=BB0_8 Depth=1
	movl	$1, debugflag(%rip)
	jmp	.LBB0_8
.LBB0_13:                               #   in Loop: Header=BB0_8 Depth=1
	movl	$1, verboseflag(%rip)
	jmp	.LBB0_8
.LBB0_10:
	movl	optind(%rip), %eax
	cmpl	%ebp, %eax
	jne	.LBB0_18
# BB#11:
	movl	$.L.str.2, %edi
	callq	fatal
	movl	optind(%rip), %eax
	jmp	.LBB0_19
.LBB0_18:
	movslq	%eax, %rcx
	movq	(%rbx,%rcx,8), %rcx
	movq	%rcx, infile(%rip)
.LBB0_19:
	decl	%ebp
	cmpl	%ebp, %eax
	jge	.LBB0_20
# BB#21:
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$40, %esi
	movl	$1, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.LBB0_20:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	getargs, .Lfunc_end0-getargs
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_14
	.quad	.LBB0_8
	.quad	.LBB0_8
	.quad	.LBB0_8
	.quad	.LBB0_8
	.quad	.LBB0_8
	.quad	.LBB0_8
	.quad	.LBB0_8
	.quad	.LBB0_15
	.quad	.LBB0_8
	.quad	.LBB0_8
	.quad	.LBB0_17
	.quad	.LBB0_8
	.quad	.LBB0_8
	.quad	.LBB0_8
	.quad	.LBB0_8
	.quad	.LBB0_16
	.quad	.LBB0_8
	.quad	.LBB0_13
	.quad	.LBB0_8
	.quad	.LBB0_8
	.quad	.LBB0_7

	.type	verboseflag,@object     # @verboseflag
	.comm	verboseflag,4,4
	.type	definesflag,@object     # @definesflag
	.comm	definesflag,4,4
	.type	debugflag,@object       # @debugflag
	.comm	debugflag,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"yacc"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"yvdlto:"
	.size	.L.str.1, 8

	.type	nolinesflag,@object     # @nolinesflag
	.comm	nolinesflag,4,4
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"grammar file not specified"
	.size	.L.str.2, 27

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"bison: warning: extra arguments ignored\n"
	.size	.L.str.3, 41


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
