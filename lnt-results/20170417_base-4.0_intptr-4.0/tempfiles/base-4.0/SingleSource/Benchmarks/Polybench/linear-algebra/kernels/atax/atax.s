	.text
	.file	"atax.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4614256656552045848     # double 3.1415926535897931
.LCPI6_1:
	.quad	4661014508095930368     # double 4000
.LCPI6_3:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_2:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 80
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$128000000, %edx        # imm = 0x7A12000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_50
# BB#1:
	movq	(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_50
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_50
# BB#3:                                 # %polybench_alloc_data.exit
	movq	(%rsp), %r15
	testq	%r15, %r15
	je	.LBB6_50
# BB#4:                                 # %polybench_alloc_data.exit40
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_50
# BB#5:                                 # %polybench_alloc_data.exit40
	movq	(%rsp), %r12
	testq	%r12, %r12
	je	.LBB6_50
# BB#6:                                 # %polybench_alloc_data.exit42
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_50
# BB#7:                                 # %polybench_alloc_data.exit42
	movq	(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_50
# BB#8:                                 # %polybench_alloc_data.exit44
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_50
# BB#9:                                 # %polybench_alloc_data.exit44
	movq	(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_50
# BB#10:                                # %polybench_alloc_data.exit46
	xorl	%eax, %eax
	movsd	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_11:                               # =>This Inner Loop Header: Depth=1
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%r15,%rax,8)
	leal	1(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%r15,%rax,8)
	leal	2(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 16(%r15,%rax,8)
	leal	3(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 24(%r15,%rax,8)
	leal	4(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 32(%r15,%rax,8)
	addq	$5, %rax
	cmpq	$4000, %rax             # imm = 0xFA0
	jne	.LBB6_11
# BB#12:                                # %.preheader.i.preheader
	leaq	8(%r14), %rax
	xorl	%ecx, %ecx
	movsd	.LCPI6_1(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_13:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_14 Depth 2
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movq	$-4000, %rdx            # imm = 0xF060
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_14:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	4001(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	divsd	%xmm0, %xmm2
	movsd	%xmm2, -8(%rsi)
	leal	4002(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	divsd	%xmm0, %xmm2
	movsd	%xmm2, (%rsi)
	addq	$16, %rsi
	addq	$2, %rdx
	jne	.LBB6_14
# BB#15:                                #   in Loop: Header=BB6_13 Depth=1
	incq	%rcx
	addq	$32000, %rax            # imm = 0x7D00
	cmpq	$4000, %rcx             # imm = 0xFA0
	jne	.LBB6_13
# BB#16:                                # %init_array.exit
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$32000, %edx            # imm = 0x7D00
	movq	%r12, %rdi
	callq	memset
	leaq	8(%r12), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	32000(%r12), %r9
	leaq	8(%r14), %r13
	leaq	16(%r14), %rsi
	.p2align	4, 0x90
.LBB6_17:                               # %.preheader1.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_18 Depth 2
                                        #     Child Loop BB6_23 Depth 2
                                        #     Child Loop BB6_26 Depth 2
	imulq	$32000, %rbp, %rax      # imm = 0x7D00
	leaq	8(%r14,%rax), %r10
	leaq	32000(%r14,%rax), %r11
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rbp,8), %rdi
	movq	$0, (%rcx,%rbp,8)
	xorpd	%xmm0, %xmm0
	movl	$1, %ecx
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB6_18:                               #   Parent Loop BB6_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	mulsd	-8(%r15,%rcx,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdi)
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%r15,%rcx,8), %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%rdi)
	addq	$16, %rdx
	addq	$2, %rcx
	cmpq	$4001, %rcx             # imm = 0xFA1
	jne	.LBB6_18
# BB#19:                                # %.preheader.i50.preheader
                                        #   in Loop: Header=BB6_17 Depth=1
	mulsd	(%r14,%rax), %xmm0
	addsd	(%r12), %xmm0
	movsd	%xmm0, (%r12)
	movq	8(%rsp), %rdx           # 8-byte Reload
	cmpq	%rdi, %rdx
	sbbb	%cl, %cl
	cmpq	%r9, %rdi
	sbbb	%r8b, %r8b
	andb	%cl, %r8b
	cmpq	%r11, %rdx
	sbbb	%cl, %cl
	cmpq	%r9, %r10
	sbbb	%dl, %dl
	testb	$1, %r8b
	jne	.LBB6_20
# BB#21:                                # %.preheader.i50.preheader
                                        #   in Loop: Header=BB6_17 Depth=1
	andb	%dl, %cl
	andb	$1, %cl
	movl	$1, %ecx
	jne	.LBB6_25
# BB#22:                                # %vector.body.preheader
                                        #   in Loop: Header=BB6_17 Depth=1
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_23:                               # %vector.body
                                        #   Parent Loop BB6_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	8(%r12,%rcx,8), %xmm1
	movupd	24(%r12,%rcx,8), %xmm2
	movupd	(%r13,%rcx,8), %xmm3
	movupd	16(%r13,%rcx,8), %xmm4
	mulpd	%xmm0, %xmm3
	mulpd	%xmm0, %xmm4
	addpd	%xmm1, %xmm3
	addpd	%xmm2, %xmm4
	movupd	%xmm3, 8(%r12,%rcx,8)
	movupd	%xmm4, 24(%r12,%rcx,8)
	addq	$4, %rcx
	cmpq	$3996, %rcx             # imm = 0xF9C
	jne	.LBB6_23
# BB#24:                                #   in Loop: Header=BB6_17 Depth=1
	movl	$3997, %ecx             # imm = 0xF9D
	jmp	.LBB6_25
	.p2align	4, 0x90
.LBB6_20:                               #   in Loop: Header=BB6_17 Depth=1
	movl	$1, %ecx
.LBB6_25:                               # %.preheader..preheader_crit_edge.i.preheader.new
                                        #   in Loop: Header=BB6_17 Depth=1
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	addq	%r14, %rax
	mulsd	(%rax,%rcx,8), %xmm0
	addsd	(%r12,%rcx,8), %xmm0
	movsd	%xmm0, (%r12,%rcx,8)
	leaq	(%rsi,%rcx,8), %rax
	addq	$2, %rcx
	.p2align	4, 0x90
.LBB6_26:                               # %.preheader..preheader_crit_edge.i
                                        #   Parent Loop BB6_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	mulsd	-8(%rax), %xmm0
	addsd	-8(%r12,%rcx,8), %xmm0
	movsd	%xmm0, -8(%r12,%rcx,8)
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rax), %xmm0
	addsd	(%r12,%rcx,8), %xmm0
	movsd	%xmm0, (%r12,%rcx,8)
	addq	$2, %rcx
	addq	$16, %rax
	cmpq	$4001, %rcx             # imm = 0xFA1
	jne	.LBB6_26
# BB#27:                                # %.loopexit140
                                        #   in Loop: Header=BB6_17 Depth=1
	incq	%rbp
	addq	$32000, %r13            # imm = 0x7D00
	addq	$32000, %rsi            # imm = 0x7D00
	cmpq	$4000, %rbp             # imm = 0xFA0
	jne	.LBB6_17
# BB#28:                                # %kernel_atax.exit
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$32000, %edx            # imm = 0x7D00
	movq	%rbx, %rdi
	callq	memset
	leaq	8(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	32000(%rbx), %r9
	leaq	8(%r14), %rsi
	leaq	16(%r14), %r13
	.p2align	4, 0x90
.LBB6_29:                               # %.preheader1.i52
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_30 Depth 2
                                        #     Child Loop BB6_35 Depth 2
                                        #     Child Loop BB6_38 Depth 2
	imulq	$32000, %rbp, %rax      # imm = 0x7D00
	leaq	8(%r14,%rax), %r10
	leaq	32000(%r14,%rax), %r11
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rbp,8), %rdi
	movq	$0, (%rcx,%rbp,8)
	xorpd	%xmm0, %xmm0
	movl	$1, %ecx
	movq	%rsi, %rdx
	.p2align	4, 0x90
.LBB6_30:                               #   Parent Loop BB6_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	mulsd	-8(%r15,%rcx,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdi)
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%r15,%rcx,8), %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%rdi)
	addq	$16, %rdx
	addq	$2, %rcx
	cmpq	$4001, %rcx             # imm = 0xFA1
	jne	.LBB6_30
# BB#31:                                # %.preheader.i59.preheader
                                        #   in Loop: Header=BB6_29 Depth=1
	mulsd	(%r14,%rax), %xmm0
	addsd	(%rbx), %xmm0
	movsd	%xmm0, (%rbx)
	movq	8(%rsp), %rdx           # 8-byte Reload
	cmpq	%rdi, %rdx
	sbbb	%cl, %cl
	cmpq	%r9, %rdi
	sbbb	%r8b, %r8b
	andb	%cl, %r8b
	cmpq	%r11, %rdx
	sbbb	%cl, %cl
	cmpq	%r9, %r10
	sbbb	%dl, %dl
	testb	$1, %r8b
	jne	.LBB6_32
# BB#33:                                # %.preheader.i59.preheader
                                        #   in Loop: Header=BB6_29 Depth=1
	andb	%dl, %cl
	andb	$1, %cl
	movl	$1, %ecx
	jne	.LBB6_37
# BB#34:                                # %vector.body106.preheader
                                        #   in Loop: Header=BB6_29 Depth=1
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_35:                               # %vector.body106
                                        #   Parent Loop BB6_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	8(%rbx,%rcx,8), %xmm1
	movupd	24(%rbx,%rcx,8), %xmm2
	movupd	(%rsi,%rcx,8), %xmm3
	movupd	16(%rsi,%rcx,8), %xmm4
	mulpd	%xmm0, %xmm3
	mulpd	%xmm0, %xmm4
	addpd	%xmm1, %xmm3
	addpd	%xmm2, %xmm4
	movupd	%xmm3, 8(%rbx,%rcx,8)
	movupd	%xmm4, 24(%rbx,%rcx,8)
	addq	$4, %rcx
	cmpq	$3996, %rcx             # imm = 0xF9C
	jne	.LBB6_35
# BB#36:                                #   in Loop: Header=BB6_29 Depth=1
	movl	$3997, %ecx             # imm = 0xF9D
	jmp	.LBB6_37
	.p2align	4, 0x90
.LBB6_32:                               #   in Loop: Header=BB6_29 Depth=1
	movl	$1, %ecx
.LBB6_37:                               # %.preheader..preheader_crit_edge.i61.preheader.new
                                        #   in Loop: Header=BB6_29 Depth=1
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	addq	%r14, %rax
	mulsd	(%rax,%rcx,8), %xmm0
	addsd	(%rbx,%rcx,8), %xmm0
	movsd	%xmm0, (%rbx,%rcx,8)
	leaq	(%r13,%rcx,8), %rax
	addq	$2, %rcx
	.p2align	4, 0x90
.LBB6_38:                               # %.preheader..preheader_crit_edge.i61
                                        #   Parent Loop BB6_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	mulsd	-8(%rax), %xmm0
	addsd	-8(%rbx,%rcx,8), %xmm0
	movsd	%xmm0, -8(%rbx,%rcx,8)
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rax), %xmm0
	addsd	(%rbx,%rcx,8), %xmm0
	movsd	%xmm0, (%rbx,%rcx,8)
	addq	$2, %rcx
	addq	$16, %rax
	cmpq	$4001, %rcx             # imm = 0xFA1
	jne	.LBB6_38
# BB#39:                                # %.loopexit
                                        #   in Loop: Header=BB6_29 Depth=1
	incq	%rbp
	addq	$32000, %rsi            # imm = 0x7D00
	addq	$32000, %r13            # imm = 0x7D00
	cmpq	$4000, %rbp             # imm = 0xFA0
	jne	.LBB6_29
# BB#40:                                # %kernel_atax_StrictFP.exit.preheader
	movl	$1, %edx
	movapd	.LCPI6_2(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_3(%rip), %xmm3   # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB6_41:                               # %kernel_atax_StrictFP.exit
                                        # =>This Inner Loop Header: Depth=1
	movsd	-8(%r12,%rdx,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%rbx,%rdx,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_42
# BB#44:                                # %kernel_atax_StrictFP.exit.1147
                                        #   in Loop: Header=BB6_41 Depth=1
	movsd	(%r12,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rbx,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_43
# BB#45:                                #   in Loop: Header=BB6_41 Depth=1
	leaq	2(%rdx), %rax
	incq	%rdx
	cmpq	$4000, %rdx             # imm = 0xFA0
	movq	%rax, %rdx
	jl	.LBB6_41
# BB#46:                                # %check_FP.exit
	movl	$64001, %edi            # imm = 0xFA01
	callq	malloc
	movq	%rax, %rbp
	movb	$0, 64000(%rbp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_47:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rax), %rcx
	movl	%ecx, %edx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, (%rbp,%rax,2)
	movb	%dl, 1(%rbp,%rax,2)
	movq	%rcx, %rdx
	shrq	$8, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 2(%rbp,%rax,2)
	movb	%dl, 3(%rbp,%rax,2)
	movq	%rcx, %rdx
	shrq	$16, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 4(%rbp,%rax,2)
	movb	%dl, 5(%rbp,%rax,2)
	movl	%ecx, %edx
	shrl	$24, %edx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 6(%rbp,%rax,2)
	movb	%dl, 7(%rbp,%rax,2)
	movq	%rcx, %rdx
	shrq	$32, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 8(%rbp,%rax,2)
	movb	%dl, 9(%rbp,%rax,2)
	movq	%rcx, %rdx
	shrq	$40, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 10(%rbp,%rax,2)
	movb	%dl, 11(%rbp,%rax,2)
	movq	%rcx, %rdx
	shrq	$48, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 12(%rbp,%rax,2)
	movb	%dl, 13(%rbp,%rax,2)
	shrq	$56, %rcx
	andb	$15, %cl
	orb	$48, %cl
	movb	%cl, 14(%rbp,%rax,2)
	movb	%cl, 15(%rbp,%rax,2)
	addq	$8, %rax
	cmpq	$32000, %rax            # imm = 0x7D00
	jne	.LBB6_47
# BB#48:                                # %print_array.exit
	movq	stderr(%rip), %rsi
	movq	%rbp, %rdi
	callq	fputs
	movq	%rbp, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_49
.LBB6_42:                               # %check_FP.exit.threadsplit
	decq	%rdx
.LBB6_43:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_3(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %ecx
	callq	fprintf
	movl	$1, %eax
.LBB6_49:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_50:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d] = %lf and B[%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 68


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
