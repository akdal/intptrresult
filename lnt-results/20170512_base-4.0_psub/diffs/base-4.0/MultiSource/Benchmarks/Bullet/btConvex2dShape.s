	.text
	.file	"btConvex2dShape.bc"
	.globl	_ZN15btConvex2dShapeC2EP13btConvexShape
	.p2align	4, 0x90
	.type	_ZN15btConvex2dShapeC2EP13btConvexShape,@function
_ZN15btConvex2dShapeC2EP13btConvexShape: # @_ZN15btConvex2dShapeC2EP13btConvexShape
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZN13btConvexShapeC2Ev
	movq	$_ZTV15btConvex2dShape+16, (%rbx)
	movq	%r14, 24(%rbx)
	movl	$18, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	_ZN15btConvex2dShapeC2EP13btConvexShape, .Lfunc_end0-_ZN15btConvex2dShapeC2EP13btConvexShape
	.cfi_endproc

	.globl	_ZN15btConvex2dShapeD2Ev
	.p2align	4, 0x90
	.type	_ZN15btConvex2dShapeD2Ev,@function
_ZN15btConvex2dShapeD2Ev:               # @_ZN15btConvex2dShapeD2Ev
	.cfi_startproc
# BB#0:
	jmp	_ZN13btConvexShapeD2Ev  # TAILCALL
.Lfunc_end1:
	.size	_ZN15btConvex2dShapeD2Ev, .Lfunc_end1-_ZN15btConvex2dShapeD2Ev
	.cfi_endproc

	.globl	_ZN15btConvex2dShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN15btConvex2dShapeD0Ev,@function
_ZN15btConvex2dShapeD0Ev:               # @_ZN15btConvex2dShapeD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp1:
# BB#1:                                 # %_ZN15btConvex2dShapeD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB2_2:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_4:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN15btConvex2dShapeD0Ev, .Lfunc_end2-_ZN15btConvex2dShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end3:
	.size	__clang_call_terminate, .Lfunc_end3-__clang_call_terminate

	.text
	.globl	_ZNK15btConvex2dShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btConvex2dShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK15btConvex2dShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK15btConvex2dShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	movq	104(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end4:
	.size	_ZNK15btConvex2dShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end4-_ZNK15btConvex2dShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.globl	_ZNK15btConvex2dShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK15btConvex2dShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK15btConvex2dShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK15btConvex2dShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	movq	112(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end5:
	.size	_ZNK15btConvex2dShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end5-_ZNK15btConvex2dShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc

	.globl	_ZNK15btConvex2dShape24localGetSupportingVertexERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btConvex2dShape24localGetSupportingVertexERK9btVector3,@function
_ZNK15btConvex2dShape24localGetSupportingVertexERK9btVector3: # @_ZNK15btConvex2dShape24localGetSupportingVertexERK9btVector3
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	movq	96(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end6:
	.size	_ZNK15btConvex2dShape24localGetSupportingVertexERK9btVector3, .Lfunc_end6-_ZNK15btConvex2dShape24localGetSupportingVertexERK9btVector3
	.cfi_endproc

	.globl	_ZNK15btConvex2dShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btConvex2dShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK15btConvex2dShape21calculateLocalInertiaEfR9btVector3: # @_ZNK15btConvex2dShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	movq	64(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end7:
	.size	_ZNK15btConvex2dShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end7-_ZNK15btConvex2dShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.globl	_ZNK15btConvex2dShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK15btConvex2dShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK15btConvex2dShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK15btConvex2dShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end8:
	.size	_ZNK15btConvex2dShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end8-_ZNK15btConvex2dShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.globl	_ZNK15btConvex2dShape11getAabbSlowERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK15btConvex2dShape11getAabbSlowERK11btTransformR9btVector3S4_,@function
_ZNK15btConvex2dShape11getAabbSlowERK11btTransformR9btVector3S4_: # @_ZNK15btConvex2dShape11getAabbSlowERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	movq	120(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end9:
	.size	_ZNK15btConvex2dShape11getAabbSlowERK11btTransformR9btVector3S4_, .Lfunc_end9-_ZNK15btConvex2dShape11getAabbSlowERK11btTransformR9btVector3S4_
	.cfi_endproc

	.globl	_ZN15btConvex2dShape15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN15btConvex2dShape15setLocalScalingERK9btVector3,@function
_ZN15btConvex2dShape15setLocalScalingERK9btVector3: # @_ZN15btConvex2dShape15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end10:
	.size	_ZN15btConvex2dShape15setLocalScalingERK9btVector3, .Lfunc_end10-_ZN15btConvex2dShape15setLocalScalingERK9btVector3
	.cfi_endproc

	.globl	_ZNK15btConvex2dShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK15btConvex2dShape15getLocalScalingEv,@function
_ZNK15btConvex2dShape15getLocalScalingEv: # @_ZNK15btConvex2dShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 16
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	callq	*56(%rax)
	popq	%rcx
	retq
.Lfunc_end11:
	.size	_ZNK15btConvex2dShape15getLocalScalingEv, .Lfunc_end11-_ZNK15btConvex2dShape15getLocalScalingEv
	.cfi_endproc

	.globl	_ZN15btConvex2dShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN15btConvex2dShape9setMarginEf,@function
_ZN15btConvex2dShape9setMarginEf:       # @_ZN15btConvex2dShape9setMarginEf
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	movq	80(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end12:
	.size	_ZN15btConvex2dShape9setMarginEf, .Lfunc_end12-_ZN15btConvex2dShape9setMarginEf
	.cfi_endproc

	.globl	_ZNK15btConvex2dShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK15btConvex2dShape9getMarginEv,@function
_ZNK15btConvex2dShape9getMarginEv:      # @_ZNK15btConvex2dShape9getMarginEv
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	jmpq	*88(%rax)               # TAILCALL
.Lfunc_end13:
	.size	_ZNK15btConvex2dShape9getMarginEv, .Lfunc_end13-_ZNK15btConvex2dShape9getMarginEv
	.cfi_endproc

	.globl	_ZNK15btConvex2dShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK15btConvex2dShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK15btConvex2dShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK15btConvex2dShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	jmpq	*128(%rax)              # TAILCALL
.Lfunc_end14:
	.size	_ZNK15btConvex2dShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end14-_ZNK15btConvex2dShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.globl	_ZNK15btConvex2dShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btConvex2dShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK15btConvex2dShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK15btConvex2dShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	movq	136(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end15:
	.size	_ZNK15btConvex2dShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end15-_ZNK15btConvex2dShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.section	.text._ZNK15btConvex2dShape7getNameEv,"axG",@progbits,_ZNK15btConvex2dShape7getNameEv,comdat
	.weak	_ZNK15btConvex2dShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK15btConvex2dShape7getNameEv,@function
_ZNK15btConvex2dShape7getNameEv:        # @_ZNK15btConvex2dShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end16:
	.size	_ZNK15btConvex2dShape7getNameEv, .Lfunc_end16-_ZNK15btConvex2dShape7getNameEv
	.cfi_endproc

	.type	_ZTV15btConvex2dShape,@object # @_ZTV15btConvex2dShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV15btConvex2dShape
	.p2align	3
_ZTV15btConvex2dShape:
	.quad	0
	.quad	_ZTI15btConvex2dShape
	.quad	_ZN15btConvex2dShapeD2Ev
	.quad	_ZN15btConvex2dShapeD0Ev
	.quad	_ZNK15btConvex2dShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN15btConvex2dShape15setLocalScalingERK9btVector3
	.quad	_ZNK15btConvex2dShape15getLocalScalingEv
	.quad	_ZNK15btConvex2dShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK15btConvex2dShape7getNameEv
	.quad	_ZN15btConvex2dShape9setMarginEf
	.quad	_ZNK15btConvex2dShape9getMarginEv
	.quad	_ZNK15btConvex2dShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK15btConvex2dShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK15btConvex2dShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK15btConvex2dShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK15btConvex2dShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK15btConvex2dShape32getPreferredPenetrationDirectionEiR9btVector3
	.size	_ZTV15btConvex2dShape, 160

	.type	_ZTS15btConvex2dShape,@object # @_ZTS15btConvex2dShape
	.globl	_ZTS15btConvex2dShape
	.p2align	4
_ZTS15btConvex2dShape:
	.asciz	"15btConvex2dShape"
	.size	_ZTS15btConvex2dShape, 18

	.type	_ZTI15btConvex2dShape,@object # @_ZTI15btConvex2dShape
	.globl	_ZTI15btConvex2dShape
	.p2align	4
_ZTI15btConvex2dShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15btConvex2dShape
	.quad	_ZTI13btConvexShape
	.size	_ZTI15btConvex2dShape, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Convex2dShape"
	.size	.L.str, 14


	.globl	_ZN15btConvex2dShapeC1EP13btConvexShape
	.type	_ZN15btConvex2dShapeC1EP13btConvexShape,@function
_ZN15btConvex2dShapeC1EP13btConvexShape = _ZN15btConvex2dShapeC2EP13btConvexShape
	.globl	_ZN15btConvex2dShapeD1Ev
	.type	_ZN15btConvex2dShapeD1Ev,@function
_ZN15btConvex2dShapeD1Ev = _ZN15btConvex2dShapeD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
