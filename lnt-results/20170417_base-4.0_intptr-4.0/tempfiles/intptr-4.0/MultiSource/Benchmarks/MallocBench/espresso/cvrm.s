	.text
	.file	"cvrm.bc"
	.globl	unravel_range
	.p2align	4, 0x90
	.type	unravel_range,@function
unravel_range:                          # @unravel_range
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 176
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movl	%esi, %ebx
	movq	%rdi, %r15
	movq	cube+80(%rip), %rax
	movq	8(%rax), %r14
	movq	cube+96(%rip), %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	set_copy
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	testl	%ebx, %ebx
	jle	.LBB0_3
# BB#1:                                 # %.lr.ph84.preheader
	movl	4(%rsp), %ebx           # 4-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph84
                                        # =>This Inner Loop Header: Depth=1
	movq	cube+72(%rip), %rax
	movq	(%rax,%rbp,8), %rdx
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r14, %rsi
	callq	set_or
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB0_2
.LBB0_3:                                # %.preheader67
	leal	1(%r12), %eax
	cmpl	cube+4(%rip), %eax
	jge	.LBB0_6
# BB#4:                                 # %.lr.ph80.preheader
	movslq	%eax, %rbx
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph80
                                        # =>This Inner Loop Header: Depth=1
	movq	cube+72(%rip), %rax
	movq	(%rax,%rbx,8), %rdx
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r14, %rsi
	callq	set_or
	incq	%rbx
	movslq	cube+4(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_5
.LBB0_6:                                # %._crit_edge81
	movslq	(%r15), %rax
	movslq	12(%r15), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%r14, 72(%rsp)          # 8-byte Spill
	jle	.LBB0_7
# BB#8:                                 # %.preheader.lr.ph
	movq	24(%r15), %rbx
	leaq	(%rbx,%rcx,4), %r14
	movl	4(%rsp), %ecx           # 4-byte Reload
	cmpl	%r12d, %ecx
	jle	.LBB0_11
# BB#9:                                 # %.preheader.us.preheader
	shlq	$2, %rax
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_10:                               # %.preheader.us
                                        # =>This Inner Loop Header: Depth=1
	incl	%r12d
	addq	%rax, %rbx
	cmpq	%r14, %rbx
	jb	.LBB0_10
	jmp	.LBB0_18
.LBB0_7:
	xorl	%r12d, %r12d
	jmp	.LBB0_18
.LBB0_11:                               # %.preheader.preheader
	movslq	%ecx, %rax
	movslq	%r12d, %r13
	decq	%rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_12:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_13 Depth 2
	movl	$1, %ebp
	movq	8(%rsp), %r15           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_13:                               #   Parent Loop BB0_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cube+72(%rip), %rax
	movq	8(%rax,%r15,8), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	set_dist
	cmpl	$2, %eax
	jl	.LBB0_16
# BB#14:                                #   in Loop: Header=BB0_13 Depth=2
	imull	%eax, %ebp
	cmpl	$1000001, %ebp          # imm = 0xF4241
	jl	.LBB0_16
# BB#15:                                #   in Loop: Header=BB0_13 Depth=2
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB0_16:                               #   in Loop: Header=BB0_13 Depth=2
	incq	%r15
	cmpq	%r13, %r15
	jl	.LBB0_13
# BB#17:                                # %._crit_edge73
                                        #   in Loop: Header=BB0_12 Depth=1
	addl	%ebp, %r12d
	movq	24(%rsp), %r15          # 8-byte Reload
	movslq	(%r15), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r14, %rbx
	jb	.LBB0_12
.LBB0_18:                               # %._crit_edge76
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	movl	%r12d, %edi
	callq	sf_new
	movq	%rax, %r12
	movslq	(%r15), %rcx
	movslq	12(%r15), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB0_55
# BB#19:                                # %.lr.ph
	movq	24(%r15), %r13
	leaq	(%r13,%rax,4), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movslq	4(%rsp), %rax           # 4-byte Folded Reload
	movslq	16(%rsp), %rcx          # 4-byte Folded Reload
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rax, 56(%rsp)          # 8-byte Spill
	decq	%rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_20:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_22 Depth 2
                                        #     Child Loop BB0_28 Depth 2
                                        #       Child Loop BB0_57 Depth 3
                                        #       Child Loop BB0_61 Depth 3
                                        #       Child Loop BB0_65 Depth 3
                                        #     Child Loop BB0_36 Depth 2
                                        #       Child Loop BB0_40 Depth 3
                                        #         Child Loop BB0_43 Depth 4
                                        #           Child Loop BB0_47 Depth 5
	movq	cube+80(%rip), %rax
	movq	(%rax), %r14
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	set_copy
	movl	4(%rsp), %eax           # 4-byte Reload
	cmpl	16(%rsp), %eax          # 4-byte Folded Reload
	movl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jg	.LBB0_26
# BB#21:                                # %.lr.ph134.i.preheader
                                        #   in Loop: Header=BB0_20 Depth=1
	movl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	48(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_22:                               # %.lr.ph134.i
                                        #   Parent Loop BB0_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cube+72(%rip), %rax
	movq	8(%rax,%rbx,8), %rsi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	set_dist
	cmpl	$1, %eax
	jg	.LBB0_24
# BB#23:                                #   in Loop: Header=BB0_22 Depth=2
	movq	cube+72(%rip), %rax
	movq	8(%rax,%rbx,8), %rdx
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r14, %rsi
	callq	set_or
	jmp	.LBB0_25
	.p2align	4, 0x90
.LBB0_24:                               #   in Loop: Header=BB0_22 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	imull	%eax, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
.LBB0_25:                               #   in Loop: Header=BB0_22 Depth=2
	incq	%rbx
	cmpq	32(%rsp), %rbx          # 8-byte Folded Reload
	jl	.LBB0_22
.LBB0_26:                               # %._crit_edge135.i
                                        #   in Loop: Header=BB0_20 Depth=1
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r14, %rdx
	callq	set_and
	movl	12(%r12), %edi
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	(%rdi,%rax), %ecx
	movl	%ecx, 12(%r12)
	movq	24(%r12), %rdx
	movslq	(%r12), %rsi
	movq	%rdi, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leal	-1(%rdi), %eax
	imull	%esi, %eax
	cltq
	leaq	(%rdx,%rax,4), %rax
	leaq	(%rax,%rsi,4), %rax
	imull	%esi, %ecx
	movslq	%ecx, %rcx
	leaq	(%rdx,%rcx,4), %r10
	cmpq	%r10, %rax
	jae	.LBB0_34
# BB#27:                                # %.lr.ph130.i.preheader
                                        #   in Loop: Header=BB0_20 Depth=1
	leaq	4(%r14), %r9
	leaq	-12(%r14), %r8
	.p2align	4, 0x90
.LBB0_28:                               # %.lr.ph130.i
                                        #   Parent Loop BB0_20 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_57 Depth 3
                                        #       Child Loop BB0_61 Depth 3
                                        #       Child Loop BB0_65 Depth 3
	movl	(%r14), %edi
	andl	$1023, %edi             # imm = 0x3FF
	leaq	1(%rdi), %rbx
	cmpq	$8, %rbx
	jb	.LBB0_64
# BB#29:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_28 Depth=2
	movq	%rbx, %r11
	andq	$2040, %r11             # imm = 0x7F8
	je	.LBB0_64
# BB#30:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_28 Depth=2
	leaq	(%r9,%rdi,4), %rcx
	cmpq	%rcx, %rax
	jae	.LBB0_32
# BB#31:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_28 Depth=2
	leaq	4(%rax,%rdi,4), %rcx
	cmpq	%rcx, %r14
	jb	.LBB0_64
.LBB0_32:                               # %vector.body.preheader
                                        #   in Loop: Header=BB0_28 Depth=2
	leaq	-8(%r11), %r15
	movl	%r15d, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB0_33
# BB#56:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB0_28 Depth=2
	leaq	-12(%rax,%rdi,4), %rsi
	leaq	(%r8,%rdi,4), %rcx
	negq	%rdx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_57:                               # %vector.body.prol
                                        #   Parent Loop BB0_20 Depth=1
                                        #     Parent Loop BB0_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	(%rcx,%rbp,4), %xmm0
	movups	-16(%rcx,%rbp,4), %xmm1
	movups	%xmm0, (%rsi,%rbp,4)
	movups	%xmm1, -16(%rsi,%rbp,4)
	addq	$-8, %rbp
	incq	%rdx
	jne	.LBB0_57
# BB#58:                                # %vector.body.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB0_28 Depth=2
	negq	%rbp
	cmpq	$24, %r15
	jae	.LBB0_60
	jmp	.LBB0_62
.LBB0_33:                               #   in Loop: Header=BB0_28 Depth=2
	xorl	%ebp, %ebp
	cmpq	$24, %r15
	jb	.LBB0_62
.LBB0_60:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_28 Depth=2
	movq	%rbx, %rdx
	andq	$-8, %rdx
	negq	%rdx
	negq	%rbp
	leaq	-12(%rax,%rdi,4), %rcx
	leaq	(%r8,%rdi,4), %rsi
	.p2align	4, 0x90
.LBB0_61:                               # %vector.body
                                        #   Parent Loop BB0_20 Depth=1
                                        #     Parent Loop BB0_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	(%rsi,%rbp,4), %xmm0
	movups	-16(%rsi,%rbp,4), %xmm1
	movups	%xmm0, (%rcx,%rbp,4)
	movups	%xmm1, -16(%rcx,%rbp,4)
	movups	-32(%rsi,%rbp,4), %xmm0
	movups	-48(%rsi,%rbp,4), %xmm1
	movups	%xmm0, -32(%rcx,%rbp,4)
	movups	%xmm1, -48(%rcx,%rbp,4)
	movups	-64(%rsi,%rbp,4), %xmm0
	movups	-80(%rsi,%rbp,4), %xmm1
	movups	%xmm0, -64(%rcx,%rbp,4)
	movups	%xmm1, -80(%rcx,%rbp,4)
	movups	-96(%rsi,%rbp,4), %xmm0
	movups	-112(%rsi,%rbp,4), %xmm1
	movups	%xmm0, -96(%rcx,%rbp,4)
	movups	%xmm1, -112(%rcx,%rbp,4)
	addq	$-32, %rbp
	cmpq	%rbp, %rdx
	jne	.LBB0_61
.LBB0_62:                               # %middle.block
                                        #   in Loop: Header=BB0_28 Depth=2
	cmpq	%r11, %rbx
	je	.LBB0_66
# BB#63:                                #   in Loop: Header=BB0_28 Depth=2
	subq	%r11, %rdi
	.p2align	4, 0x90
.LBB0_64:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_28 Depth=2
	incq	%rdi
	.p2align	4, 0x90
.LBB0_65:                               # %scalar.ph
                                        #   Parent Loop BB0_20 Depth=1
                                        #     Parent Loop BB0_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%r14,%rdi,4), %ecx
	movl	%ecx, -4(%rax,%rdi,4)
	decq	%rdi
	jg	.LBB0_65
.LBB0_66:                               # %.loopexit
                                        #   in Loop: Header=BB0_28 Depth=2
	movslq	(%r12), %rcx
	leaq	(%rax,%rcx,4), %rax
	cmpq	%r10, %rax
	jb	.LBB0_28
.LBB0_34:                               # %.preheader109.i
                                        #   in Loop: Header=BB0_20 Depth=1
	movl	4(%rsp), %eax           # 4-byte Reload
	cmpl	16(%rsp), %eax          # 4-byte Folded Reload
	jg	.LBB0_54
# BB#35:                                # %.lr.ph126.i.preheader
                                        #   in Loop: Header=BB0_20 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	1(%rax), %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %r15d
	movq	%r13, 88(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_36:                               # %.lr.ph126.i
                                        #   Parent Loop BB0_20 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_40 Depth 3
                                        #         Child Loop BB0_43 Depth 4
                                        #           Child Loop BB0_47 Depth 5
	movq	cube+72(%rip), %rax
	movq	(%rax,%r14,8), %rsi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	set_dist
	movl	%eax, %ecx
	cmpl	$2, %ecx
	jl	.LBB0_53
# BB#37:                                #   in Loop: Header=BB0_36 Depth=2
	movl	%r15d, %eax
	cltd
	idivl	%ecx
	movl	%eax, %r8d
	movq	cube+16(%rip), %rax
	movl	(%rax,%r14,4), %ecx
	movq	cube+24(%rip), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	(%rax,%r14,4), %esi
	cmpl	%esi, %ecx
	jg	.LBB0_52
# BB#38:                                #   in Loop: Header=BB0_36 Depth=2
	testl	%r8d, %r8d
	jle	.LBB0_52
# BB#39:                                # %.lr.ph116.split.us.i.preheader
                                        #   in Loop: Header=BB0_36 Depth=2
	movl	%r8d, %r10d
	andl	$1, %r10d
	xorl	%r9d, %r9d
	movq	%r14, 96(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_40:                               # %.lr.ph116.split.us.i
                                        #   Parent Loop BB0_20 Depth=1
                                        #     Parent Loop BB0_36 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_43 Depth 4
                                        #           Child Loop BB0_47 Depth 5
	movl	%ecx, %eax
	sarl	$5, %eax
	movslq	%eax, %rdx
	movl	4(%r13,%rdx,4), %eax
	movl	$1, %edi
	shll	%cl, %edi
	movl	%ecx, %ebx
	andb	$31, %bl
	movzbl	%bl, %ebp
	btl	%ebp, %eax
	jae	.LBB0_51
# BB#41:                                # %.preheader108.us.i
                                        #   in Loop: Header=BB0_40 Depth=3
	cmpl	8(%rsp), %r9d           # 4-byte Folded Reload
	jge	.LBB0_50
# BB#42:                                # %.preheader.us.us.preheader.i
                                        #   in Loop: Header=BB0_40 Depth=3
	incq	%rdx
	movq	24(%r12), %rsi
	movq	80(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r9), %r11d
	movq	%r9, 112(%rsp)          # 8-byte Spill
	movl	%r9d, %r13d
	.p2align	4, 0x90
.LBB0_43:                               # %.preheader.us.us.i
                                        #   Parent Loop BB0_20 Depth=1
                                        #     Parent Loop BB0_36 Depth=2
                                        #       Parent Loop BB0_40 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_47 Depth 5
	xorl	%ebx, %ebx
	testl	%r10d, %r10d
	je	.LBB0_45
# BB#44:                                #   in Loop: Header=BB0_43 Depth=4
	movslq	(%r12), %rax
	movq	40(%rsp), %rbp          # 8-byte Reload
	leal	(%r13,%rbp), %ebx
	movslq	%ebx, %rbx
	imulq	%rax, %rbx
	leaq	(%rsi,%rbx,4), %rax
	orl	%edi, (%rax,%rdx,4)
	movl	$1, %ebx
.LBB0_45:                               # %.prol.loopexit
                                        #   in Loop: Header=BB0_43 Depth=4
	cmpl	$1, %r8d
	je	.LBB0_48
# BB#46:                                # %.preheader.us.us.i.new
                                        #   in Loop: Header=BB0_43 Depth=4
	movl	%r8d, %eax
	subl	%ebx, %eax
	addl	%r11d, %ebx
	.p2align	4, 0x90
.LBB0_47:                               #   Parent Loop BB0_20 Depth=1
                                        #     Parent Loop BB0_36 Depth=2
                                        #       Parent Loop BB0_40 Depth=3
                                        #         Parent Loop BB0_43 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movslq	(%r12), %r14
	leal	-1(%rbx), %r9d
	movslq	%r9d, %rbp
	imulq	%r14, %rbp
	leaq	(%rsi,%rbp,4), %rbp
	orl	%edi, (%rbp,%rdx,4)
	movslq	(%r12), %rbp
	movslq	%ebx, %rbx
	imulq	%rbx, %rbp
	leaq	(%rsi,%rbp,4), %rbp
	orl	%edi, (%rbp,%rdx,4)
	addl	$2, %ebx
	addl	$-2, %eax
	jne	.LBB0_47
.LBB0_48:                               # %._crit_edge.us.us.i
                                        #   in Loop: Header=BB0_43 Depth=4
	addl	%r15d, %r13d
	addl	%r15d, %r11d
	cmpl	8(%rsp), %r13d          # 4-byte Folded Reload
	jl	.LBB0_43
# BB#49:                                # %._crit_edge112.us-lcssa.us.us.loopexit.i
                                        #   in Loop: Header=BB0_40 Depth=3
	movq	96(%rsp), %r14          # 8-byte Reload
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%r14,4), %esi
	movq	88(%rsp), %r13          # 8-byte Reload
	movq	112(%rsp), %r9          # 8-byte Reload
.LBB0_50:                               # %._crit_edge112.us-lcssa.us.us.i
                                        #   in Loop: Header=BB0_40 Depth=3
	addl	%r8d, %r9d
.LBB0_51:                               #   in Loop: Header=BB0_40 Depth=3
	cmpl	%esi, %ecx
	leal	1(%rcx), %eax
	movl	%eax, %ecx
	jl	.LBB0_40
.LBB0_52:                               #   in Loop: Header=BB0_36 Depth=2
	movl	%r8d, %r15d
.LBB0_53:                               # %.loopexit.i
                                        #   in Loop: Header=BB0_36 Depth=2
	cmpq	32(%rsp), %r14          # 8-byte Folded Reload
	leaq	1(%r14), %r14
	jl	.LBB0_36
.LBB0_54:                               # %cb_unravel.exit
                                        #   in Loop: Header=BB0_20 Depth=1
	movq	24(%rsp), %r15          # 8-byte Reload
	movslq	(%r15), %rax
	leaq	(%r13,%rax,4), %r13
	cmpq	64(%rsp), %r13          # 8-byte Folded Reload
	jb	.LBB0_20
.LBB0_55:                               # %._crit_edge
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sf_free
	movq	%r12, %rax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	unravel_range, .Lfunc_end0-unravel_range
	.cfi_endproc

	.globl	unravel
	.p2align	4, 0x90
	.type	unravel,@function
unravel:                                # @unravel
	.cfi_startproc
# BB#0:
	movl	cube+4(%rip), %edx
	decl	%edx
	jmp	unravel_range           # TAILCALL
.Lfunc_end1:
	.size	unravel, .Lfunc_end1-unravel
	.cfi_endproc

	.globl	lex_sort
	.p2align	4, 0x90
	.type	lex_sort,@function
lex_sort:                               # @lex_sort
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$lex_order, %esi
	xorl	%eax, %eax
	callq	sf_sort
	movq	%rax, %rcx
	movl	4(%rbx), %edx
	movl	12(%rbx), %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_unlist
	movq	%rax, %r14
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_free
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	lex_sort, .Lfunc_end2-lex_sort
	.cfi_endproc

	.globl	size_sort
	.p2align	4, 0x90
	.type	size_sort,@function
size_sort:                              # @size_sort
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$descend, %esi
	xorl	%eax, %eax
	callq	sf_sort
	movq	%rax, %rcx
	movl	4(%rbx), %edx
	movl	12(%rbx), %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_unlist
	movq	%rax, %r14
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_free
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	size_sort, .Lfunc_end3-size_sort
	.cfi_endproc

	.globl	mini_sort
	.p2align	4, 0x90
	.type	mini_sort,@function
mini_sort:                              # @mini_sort
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 48
.Lcfi28:
	.cfi_offset %rbx, -40
.Lcfi29:
	.cfi_offset %r12, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movl	cube(%rip), %r15d
	xorl	%eax, %eax
	callq	sf_count
	movslq	(%r12), %rcx
	movslq	12(%r12), %rdx
	imulq	%rcx, %rdx
	testl	%edx, %edx
	jle	.LBB4_17
# BB#1:                                 # %.preheader.lr.ph
	movq	24(%r12), %rcx
	leaq	(%rcx,%rdx,4), %r9
	testl	%r15d, %r15d
	jle	.LBB4_16
# BB#2:                                 # %.preheader.us.preheader
	movl	%r15d, %r8d
	andl	$1, %r8d
	.p2align	4, 0x90
.LBB4_3:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_10 Depth 2
	testq	%r8, %r8
	jne	.LBB4_5
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=1
	xorl	%edx, %edx
	xorl	%edi, %edi
	cmpl	$1, %r15d
	jne	.LBB4_10
	jmp	.LBB4_15
	.p2align	4, 0x90
.LBB4_5:                                #   in Loop: Header=BB4_3 Depth=1
	testb	$1, 4(%rcx)
	jne	.LBB4_7
# BB#6:                                 #   in Loop: Header=BB4_3 Depth=1
	xorl	%edi, %edi
	jmp	.LBB4_8
	.p2align	4, 0x90
.LBB4_7:                                #   in Loop: Header=BB4_3 Depth=1
	movl	(%rax), %edi
.LBB4_8:                                # %.prol.loopexit
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	$1, %edx
	cmpl	$1, %r15d
	je	.LBB4_15
	.p2align	4, 0x90
.LBB4_10:                               #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %esi
	sarl	$5, %esi
	movslq	%esi, %rsi
	movl	4(%rcx,%rsi,4), %esi
	btl	%edx, %esi
	jae	.LBB4_12
# BB#11:                                #   in Loop: Header=BB4_10 Depth=2
	addl	(%rax,%rdx,4), %edi
.LBB4_12:                               #   in Loop: Header=BB4_10 Depth=2
	leaq	1(%rdx), %rsi
	movl	%esi, %ebx
	sarl	$5, %ebx
	movslq	%ebx, %rbx
	movl	4(%rcx,%rbx,4), %ebx
	btl	%esi, %ebx
	jae	.LBB4_14
# BB#13:                                #   in Loop: Header=BB4_10 Depth=2
	addl	4(%rax,%rdx,4), %edi
.LBB4_14:                               #   in Loop: Header=BB4_10 Depth=2
	incq	%rsi
	cmpq	%r15, %rsi
	movq	%rsi, %rdx
	jne	.LBB4_10
.LBB4_15:                               # %._crit_edge.us
                                        #   in Loop: Header=BB4_3 Depth=1
	movw	%di, 2(%rcx)
	movslq	(%r12), %rdx
	leaq	(%rcx,%rdx,4), %rcx
	cmpq	%r9, %rcx
	jb	.LBB4_3
	jmp	.LBB4_17
	.p2align	4, 0x90
.LBB4_16:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rcx), %edx
	movl	%edx, (%rcx)
	movslq	(%r12), %rdx
	leaq	(%rcx,%rdx,4), %rcx
	cmpq	%r9, %rcx
	jb	.LBB4_16
.LBB4_17:                               # %._crit_edge49
	testq	%rax, %rax
	je	.LBB4_19
# BB#18:
	movq	%rax, %rdi
	callq	free
.LBB4_19:
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sf_list
	movq	%rax, %r15
	movslq	12(%r12), %rsi
	movl	$8, %edx
	movq	%r15, %rdi
	movq	%r14, %rcx
	callq	qsort
	movl	4(%r12), %edx
	movl	12(%r12), %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sf_unlist
	movq	%rax, %r14
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sf_free
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	mini_sort, .Lfunc_end4-mini_sort
	.cfi_endproc

	.globl	sort_reduce
	.p2align	4, 0x90
	.type	sort_reduce,@function
sort_reduce:                            # @sort_reduce
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 64
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	12(%r14), %ecx
	testl	%ecx, %ecx
	je	.LBB5_12
# BB#1:
	movl	cube+4(%rip), %r12d
	movq	24(%r14), %rbx
	movl	(%r14), %eax
	movl	%eax, %edx
	imull	%ecx, %edx
	testl	%edx, %edx
	jle	.LBB5_2
# BB#3:                                 # %.lr.ph60.preheader
	movslq	%edx, %rax
	leaq	(%rbx,%rax,4), %r13
	movl	$-1, %ebp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB5_4:                                # %.lr.ph60
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	set_ord
	cmpl	%ebp, %eax
	cmovgel	%eax, %ebp
	cmovgq	%rbx, %r15
	movslq	(%r14), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r13, %rbx
	jb	.LBB5_4
# BB#5:                                 # %._crit_edge61.loopexit
	movq	24(%r14), %rbx
	movl	12(%r14), %ecx
	jmp	.LBB5_6
.LBB5_12:
	movq	%r14, %rax
	jmp	.LBB5_13
.LBB5_2:
	xorl	%r15d, %r15d
.LBB5_6:                                # %._crit_edge61
	imull	%ecx, %eax
	testl	%eax, %eax
	jle	.LBB5_11
# BB#7:                                 # %.lr.ph.preheader
	cltq
	leaq	(%rbx,%rax,4), %r13
	.p2align	4, 0x90
.LBB5_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rbx), %eax
	movl	%eax, (%rbx)
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	cdist
	movl	%r12d, %ebp
	subl	%eax, %ebp
	shll	$7, %ebp
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	set_ord
	movl	%eax, %ecx
	movl	$127, %eax
	cmpl	$126, %ecx
	jg	.LBB5_10
# BB#9:                                 #   in Loop: Header=BB5_8 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	set_ord
.LBB5_10:                               #   in Loop: Header=BB5_8 Depth=1
	addl	%ebp, %eax
	shll	$16, %eax
	orl	%eax, (%rbx)
	movslq	(%r14), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r13, %rbx
	jb	.LBB5_8
.LBB5_11:                               # %._crit_edge
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_list
	movq	%rax, %rbx
	movslq	12(%r14), %rsi
	movl	$8, %edx
	movl	$descend, %ecx
	movq	%rbx, %rdi
	callq	qsort
	movl	4(%r14), %edx
	movl	12(%r14), %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_unlist
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_free
	movq	%rbx, %rax
.LBB5_13:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	sort_reduce, .Lfunc_end5-sort_reduce
	.cfi_endproc

	.globl	random_order
	.p2align	4, 0x90
	.type	random_order,@function
random_order:                           # @random_order
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 48
.Lcfi50:
	.cfi_offset %rbx, -48
.Lcfi51:
	.cfi_offset %r12, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	4(%r15), %ebp
	cmpl	$33, %ebp
	jge	.LBB6_2
# BB#1:
	movl	$8, %edi
	jmp	.LBB6_3
.LBB6_2:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB6_3:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %r14
	movl	12(%r15), %ebx
	cmpl	$2, %ebx
	jl	.LBB6_6
# BB#4:                                 # %.lr.ph
	imull	$23, %ebx, %r12d
	addl	$974, %r12d             # imm = 0x3CE
	.p2align	4, 0x90
.LBB6_5:                                # =>This Inner Loop Header: Depth=1
	decl	%ebx
	movl	%r12d, %eax
	cltd
	idivl	%ebx
	movq	24(%r15), %rax
	movslq	(%r15), %rcx
	movslq	%edx, %rbp
	imulq	%rbp, %rcx
	leaq	(%rax,%rcx,4), %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	set_copy
	movq	24(%r15), %rax
	movl	(%r15), %ecx
	imull	%ecx, %ebp
	movslq	%ebp, %rdx
	leaq	(%rax,%rdx,4), %rdi
	imull	%ebx, %ecx
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %rsi
	xorl	%eax, %eax
	callq	set_copy
	movq	24(%r15), %rax
	movslq	(%r15), %rcx
	movslq	%ebx, %rbp
	imulq	%rbp, %rcx
	leaq	(%rax,%rcx,4), %rdi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	set_copy
	addl	$-23, %r12d
	cmpl	$1, %ebp
	jg	.LBB6_5
.LBB6_6:                                # %._crit_edge
	testq	%r14, %r14
	je	.LBB6_8
# BB#7:
	movq	%r14, %rdi
	callq	free
.LBB6_8:
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	random_order, .Lfunc_end6-random_order
	.cfi_endproc

	.globl	cubelist_partition
	.p2align	4, 0x90
	.type	cubelist_partition,@function
cubelist_partition:                     # @cubelist_partition
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi61:
	.cfi_def_cfa_offset 128
.Lcfi62:
	.cfi_offset %rbx, -56
.Lcfi63:
	.cfi_offset %r12, -48
.Lcfi64:
	.cfi_offset %r13, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB7_3
# BB#1:                                 # %.lr.ph114.preheader
	leaq	24(%rbx), %rax
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph114
                                        # =>This Inner Loop Header: Depth=1
	andb	$-9, 1(%rcx)
	movq	(%rax), %rcx
	addq	$8, %rax
	testq	%rcx, %rcx
	jne	.LBB7_2
.LBB7_3:                                # %._crit_edge115
	subq	%rbx, %rdx
	movl	(%rbp), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	cmpl	$33, %eax
	jae	.LBB7_5
# BB#4:
	movl	$8, %edi
	jmp	.LBB7_6
.LBB7_5:
	decl	%eax
	shrl	$5, %eax
	leal	8(,%rax,4), %edi
.LBB7_6:
	shrq	$3, %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbp, %rsi
	callq	set_copy
	movq	%rax, %rbp
	movq	(%rbx), %r14
	movq	16(%rbx), %r15
	orl	$2048, (%r15)           # imm = 0x800
	testq	%r15, %r15
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	je	.LBB7_7
# BB#8:                                 # %.lr.ph102.preheader.lr.ph
	leaq	24(%rbx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	-4(%rbp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	4(%rbp), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	-12(%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB7_9:                                # %.lr.ph102.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_10 Depth 2
                                        #       Child Loop BB7_11 Depth 3
                                        #       Child Loop BB7_24 Depth 3
                                        #       Child Loop BB7_28 Depth 3
	movq	64(%rsp), %r12          # 8-byte Reload
	xorl	%r13d, %r13d
.LBB7_10:                               # %.lr.ph102
                                        #   Parent Loop BB7_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_11 Depth 3
                                        #       Child Loop BB7_24 Depth 3
                                        #       Child Loop BB7_28 Depth 3
	addq	$8, %r12
	.p2align	4, 0x90
.LBB7_11:                               #   Parent Loop BB7_9 Depth=1
                                        #     Parent Loop BB7_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testb	$8, 1(%r15)
	jne	.LBB7_12
# BB#14:                                #   in Loop: Header=BB7_11 Depth=3
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	ccommon
	testl	%eax, %eax
	jne	.LBB7_15
.LBB7_12:                               # %.backedge
                                        #   in Loop: Header=BB7_11 Depth=3
	movq	-8(%r12), %r15
	addq	$8, %r12
	testq	%r15, %r15
	jne	.LBB7_11
	jmp	.LBB7_13
	.p2align	4, 0x90
.LBB7_15:                               #   in Loop: Header=BB7_10 Depth=2
	movl	(%rbp), %ecx
	movl	%ecx, %eax
	andl	$1023, %eax             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movq	%rax, %rcx
	movl	$1, %edx
	cmoveq	%rdx, %rcx
	cmpq	$8, %rcx
	jb	.LBB7_27
# BB#16:                                # %min.iters.checked
                                        #   in Loop: Header=BB7_10 Depth=2
	movq	%rcx, %r8
	andq	$1016, %r8              # imm = 0x3F8
	je	.LBB7_27
# BB#17:                                # %vector.memcheck
                                        #   in Loop: Header=BB7_10 Depth=2
	leaq	1(%rax), %rdx
	testl	%eax, %eax
	movl	$2, %esi
	cmovneq	%rsi, %rdx
	movq	56(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,4), %rsi
	leaq	4(%r15,%rax,4), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB7_19
# BB#18:                                # %vector.memcheck
                                        #   in Loop: Header=BB7_10 Depth=2
	movq	24(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rax,4), %rsi
	leaq	-4(%r15,%rdx,4), %rdx
	cmpq	%rsi, %rdx
	jb	.LBB7_27
.LBB7_19:                               # %vector.body.preheader
                                        #   in Loop: Header=BB7_10 Depth=2
	movl	%ebx, %r9d
	leaq	-8(%r8), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	btl	$3, %esi
	jb	.LBB7_20
# BB#21:                                # %vector.body.prol
                                        #   in Loop: Header=BB7_10 Depth=2
	movups	-12(%rbp,%rax,4), %xmm0
	movups	-28(%rbp,%rax,4), %xmm1
	movups	-12(%r15,%rax,4), %xmm2
	movups	-28(%r15,%rax,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbp,%rax,4)
	movups	%xmm3, -28(%rbp,%rax,4)
	movl	$8, %esi
	testq	%rdx, %rdx
	jne	.LBB7_23
	jmp	.LBB7_25
.LBB7_20:                               #   in Loop: Header=BB7_10 Depth=2
	xorl	%esi, %esi
	testq	%rdx, %rdx
	je	.LBB7_25
.LBB7_23:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB7_10 Depth=2
	movq	%rcx, %rdi
	andq	$-8, %rdi
	negq	%rdi
	negq	%rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,4), %rdx
	leaq	-12(%r15,%rax,4), %rbx
	.p2align	4, 0x90
.LBB7_24:                               # %vector.body
                                        #   Parent Loop BB7_9 Depth=1
                                        #     Parent Loop BB7_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	(%rdx,%rsi,4), %xmm0
	movups	-16(%rdx,%rsi,4), %xmm1
	movups	(%rbx,%rsi,4), %xmm2
	movups	-16(%rbx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rdx,%rsi,4)
	movups	%xmm3, -16(%rdx,%rsi,4)
	movups	-32(%rdx,%rsi,4), %xmm0
	movups	-48(%rdx,%rsi,4), %xmm1
	movups	-32(%rbx,%rsi,4), %xmm2
	movups	-48(%rbx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rdx,%rsi,4)
	movups	%xmm3, -48(%rdx,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB7_24
.LBB7_25:                               # %middle.block
                                        #   in Loop: Header=BB7_10 Depth=2
	cmpq	%r8, %rcx
	movl	%r9d, %ebx
	je	.LBB7_29
# BB#26:                                #   in Loop: Header=BB7_10 Depth=2
	subq	%r8, %rax
	.p2align	4, 0x90
.LBB7_27:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB7_10 Depth=2
	incq	%rax
	.p2align	4, 0x90
.LBB7_28:                               # %scalar.ph
                                        #   Parent Loop BB7_9 Depth=1
                                        #     Parent Loop BB7_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%r15,%rax,4), %ecx
	andl	%ecx, -4(%rbp,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB7_28
.LBB7_29:                               # %.outer
                                        #   in Loop: Header=BB7_10 Depth=2
	orb	$8, 1(%r15)
	incl	%ebx
	movq	-8(%r12), %r15
	movl	$1, %r13d
	testq	%r15, %r15
	jne	.LBB7_10
	jmp	.LBB7_30
	.p2align	4, 0x90
.LBB7_13:                               # %.outer._crit_edge
                                        #   in Loop: Header=BB7_9 Depth=1
	testl	%r13d, %r13d
	je	.LBB7_31
.LBB7_30:                               # %.outer._crit_edge._crit_edge
                                        #   in Loop: Header=BB7_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	16(%rax), %r15
	testq	%r15, %r15
	jne	.LBB7_9
	jmp	.LBB7_31
.LBB7_7:
	movl	$1, %ebx
.LBB7_31:                               # %.outer._crit_edge.thread
	movl	%ebx, %r14d
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	$4294967293, %eax       # imm = 0xFFFFFFFD
	addq	%rax, %rbx
	testq	%rbp, %rbp
	je	.LBB7_33
# BB#32:
	movq	%rbp, %rdi
	callq	free
.LBB7_33:
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	40(%rsp), %r12          # 8-byte Reload
	movl	%r14d, %ebp
	je	.LBB7_35
# BB#34:
	movl	%ebx, %edx
	subl	%ebp, %edx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
.LBB7_35:
	movl	%ebx, %r14d
	subl	%ebp, %r14d
	movq	8(%rsp), %r13           # 8-byte Reload
	je	.LBB7_47
# BB#36:
	shlq	$32, %rbx
	movabsq	$12884901888, %rbp      # imm = 0x300000000
	addq	%rbx, %rbp
	sarq	$29, %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, (%r12)
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, (%r15)
	movq	(%r13), %rbp
	movl	(%rbp), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	cmpl	$33, %eax
	jae	.LBB7_38
# BB#37:
	movl	$8, %edi
	jmp	.LBB7_39
.LBB7_38:
	decl	%eax
	shrl	$5, %eax
	leal	8(,%rax,4), %edi
.LBB7_39:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbp, %rsi
	callq	set_copy
	movq	(%r12), %rcx
	movq	%rax, (%rcx)
	movq	(%r13), %rbp
	movl	(%rbp), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	cmpl	$33, %eax
	jae	.LBB7_41
# BB#40:
	movl	$8, %edi
	jmp	.LBB7_42
.LBB7_41:
	decl	%eax
	shrl	$5, %eax
	leal	8(,%rax,4), %edi
.LBB7_42:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbp, %rsi
	callq	set_copy
	movq	(%r15), %rcx
	movq	%rax, (%rcx)
	movq	(%r12), %rax
	addq	$16, %rax
	movq	(%r15), %rdx
	addq	$16, %rdx
	movq	16(%r13), %rcx
	testq	%rcx, %rcx
	je	.LBB7_43
# BB#44:                                # %.lr.ph.preheader
	addq	$24, %r13
	movl	$2048, %edi             # imm = 0x800
	.p2align	4, 0x90
.LBB7_45:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	8(%rax), %rbp
	leaq	8(%rdx), %rsi
	movl	(%rcx), %ebx
	andl	%edi, %ebx
	cmoveq	%rax, %rbp
	cmovneq	%rdx, %rsi
	testl	%ebx, %ebx
	cmoveq	%rdx, %rax
	movq	%rcx, (%rax)
	movq	(%r13), %rcx
	addq	$8, %r13
	testq	%rcx, %rcx
	movq	%rsi, %rdx
	movq	%rbp, %rax
	jne	.LBB7_45
	jmp	.LBB7_46
.LBB7_43:
	movq	%rax, %rbp
	movq	%rdx, %rsi
.LBB7_46:                               # %._crit_edge
	movq	$0, (%rbp)
	addq	$8, %rbp
	movq	(%r12), %rax
	movq	%rbp, 8(%rax)
	movq	$0, (%rsi)
	addq	$8, %rsi
	movq	(%r15), %rax
	movq	%rsi, 8(%rax)
.LBB7_47:
	movl	%r14d, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	cubelist_partition, .Lfunc_end7-cubelist_partition
	.cfi_endproc

	.globl	cof_output
	.p2align	4, 0x90
	.type	cof_output,@function
cof_output:                             # @cof_output
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi74:
	.cfi_def_cfa_offset 128
.Lcfi75:
	.cfi_offset %rbx, -56
.Lcfi76:
	.cfi_offset %r12, -48
.Lcfi77:
	.cfi_offset %r13, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	movq	cube+72(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movq	(%rax,%rcx,8), %rbp
	movl	12(%r15), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movl	(%r15), %esi
	movl	12(%r15), %ecx
	imull	%esi, %ecx
	testl	%ecx, %ecx
	jle	.LBB8_21
# BB#1:                                 # %.lr.ph
	movq	24(%r15), %rdx
	movslq	%ecx, %rcx
	leaq	(%rdx,%rcx,4), %r9
	movl	%r14d, %ecx
	sarl	$5, %ecx
	incl	%ecx
	movslq	%ecx, %r11
	movl	$1, %r13d
	movl	%r14d, %ecx
	shll	%cl, %r13d
	leaq	-4(%rbp), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	leaq	4(%rbp), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	leaq	-12(%rbp), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	$-1024, %r14d           # imm = 0xFC00
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	movl	%r13d, 12(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB8_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_14 Depth 2
                                        #     Child Loop BB8_18 Depth 2
	testl	(%rdx,%r11,4), %r13d
	je	.LBB8_20
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	24(%rax), %r8
	movl	12(%rax), %ecx
	leal	1(%rcx), %esi
	imull	(%rax), %ecx
	movl	%esi, 12(%rax)
	movslq	%ecx, %r10
	leaq	(%r8,%r10,4), %rcx
	movl	(%rdx), %esi
	movl	(%r8,%r10,4), %edi
	andl	%r14d, %edi
	andl	$1023, %esi             # imm = 0x3FF
	movq	%rsi, %r12
	movl	$1, %ebx
	cmoveq	%rbx, %r12
	movl	%esi, %ebx
	orl	%edi, %ebx
	movl	%ebx, (%r8,%r10,4)
	cmpq	$8, %r12
	jb	.LBB8_17
# BB#4:                                 # %min.iters.checked
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	%r12, %rdi
	andq	$1016, %rdi             # imm = 0x3F8
	je	.LBB8_17
# BB#5:                                 # %vector.memcheck
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movq	%rsi, %rdi
	notq	%rdi
	testl	%esi, %esi
	movq	$-2, %rbx
	cmovneq	%rbx, %rdi
	movq	%r10, %rbx
	subq	%rdi, %rbx
	leaq	-4(%r8,%rbx,4), %r11
	addq	%rsi, %r10
	leaq	4(%r8,%r10,4), %r9
	shlq	$2, %rdi
	movq	%rdx, %rbx
	subq	%rdi, %rbx
	addq	$-4, %rbx
	leaq	4(%rdx,%rsi,4), %r15
	movq	64(%rsp), %r13          # 8-byte Reload
	subq	%rdi, %r13
	movq	56(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rsi,4), %rdi
	cmpq	%r15, %r11
	sbbb	%r15b, %r15b
	cmpq	%r9, %rbx
	sbbb	%r14b, %r14b
	andb	%r15b, %r14b
	cmpq	%rdi, %r11
	sbbb	%bl, %bl
	cmpq	%r9, %r13
	sbbb	%dil, %dil
	testb	$1, %r14b
	jne	.LBB8_6
# BB#7:                                 # %vector.memcheck
                                        #   in Loop: Header=BB8_2 Depth=1
	andb	%dil, %bl
	andb	$1, %bl
	movq	32(%rsp), %r15          # 8-byte Reload
	jne	.LBB8_8
# BB#9:                                 # %vector.body.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	leaq	-8(%rdi), %rdi
	movq	%rdi, %rbx
	shrq	$3, %rbx
	btl	$3, %edi
	jb	.LBB8_10
# BB#11:                                # %vector.body.prol
                                        #   in Loop: Header=BB8_2 Depth=1
	movups	-12(%rdx,%rsi,4), %xmm0
	movups	-28(%rdx,%rsi,4), %xmm1
	movups	-12(%rbp,%rsi,4), %xmm2
	movups	-28(%rbp,%rsi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rcx,%rsi,4)
	movups	%xmm3, -28(%rcx,%rsi,4)
	movl	$8, %r11d
	jmp	.LBB8_12
.LBB8_6:                                #   in Loop: Header=BB8_2 Depth=1
	movq	32(%rsp), %r15          # 8-byte Reload
.LBB8_8:                                #   in Loop: Header=BB8_2 Depth=1
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	12(%rsp), %r13d         # 4-byte Reload
	movl	$-1024, %r14d           # imm = 0xFC00
	jmp	.LBB8_17
.LBB8_10:                               #   in Loop: Header=BB8_2 Depth=1
	xorl	%r11d, %r11d
.LBB8_12:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB8_2 Depth=1
	testq	%rbx, %rbx
	movl	$-1024, %r14d           # imm = 0xFC00
	je	.LBB8_15
# BB#13:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	%r12, %r9
	andq	$-8, %r9
	negq	%r9
	negq	%r11
	leaq	-12(%rdx,%rsi,4), %r15
	movq	48(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rsi,4), %r13
	leaq	(%r8,%r10,4), %rdi
	.p2align	4, 0x90
.LBB8_14:                               # %vector.body
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r15,%r11,4), %xmm0
	movups	-16(%r15,%r11,4), %xmm1
	movups	(%r13,%r11,4), %xmm2
	movups	-16(%r13,%r11,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rdi,%r11,4)
	movups	%xmm3, -28(%rdi,%r11,4)
	movups	-32(%r15,%r11,4), %xmm0
	movups	-48(%r15,%r11,4), %xmm1
	movups	-32(%r13,%r11,4), %xmm2
	movups	-48(%r13,%r11,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -44(%rdi,%r11,4)
	movups	%xmm3, -60(%rdi,%r11,4)
	addq	$-16, %r11
	cmpq	%r11, %r9
	jne	.LBB8_14
.LBB8_15:                               # %middle.block
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	cmpq	%rdi, %r12
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	12(%rsp), %r13d         # 4-byte Reload
	je	.LBB8_19
# BB#16:                                #   in Loop: Header=BB8_2 Depth=1
	subq	%rdi, %rsi
	.p2align	4, 0x90
.LBB8_17:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB8_18:                               # %scalar.ph
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rbp,%rsi,4), %edi
	orl	-4(%rdx,%rsi,4), %edi
	movl	%edi, -4(%rcx,%rsi,4)
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB8_18
.LBB8_19:                               # %.loopexit
                                        #   in Loop: Header=BB8_2 Depth=1
	andb	$127, 1(%rcx)
	movl	(%r15), %esi
.LBB8_20:                               #   in Loop: Header=BB8_2 Depth=1
	movslq	%esi, %rcx
	leaq	(%rdx,%rcx,4), %rdx
	cmpq	%r9, %rdx
	jb	.LBB8_2
.LBB8_21:                               # %._crit_edge
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	cof_output, .Lfunc_end8-cof_output
	.cfi_endproc

	.globl	uncof_output
	.p2align	4, 0x90
	.type	uncof_output,@function
uncof_output:                           # @uncof_output
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi84:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi85:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 56
.Lcfi87:
	.cfi_offset %rbx, -56
.Lcfi88:
	.cfi_offset %r12, -48
.Lcfi89:
	.cfi_offset %r13, -40
.Lcfi90:
	.cfi_offset %r14, -32
.Lcfi91:
	.cfi_offset %r15, -24
.Lcfi92:
	.cfi_offset %rbp, -16
	testq	%rdi, %rdi
	je	.LBB9_18
# BB#1:
	movslq	(%rdi), %rax
	movslq	12(%rdi), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	jle	.LBB9_18
# BB#2:                                 # %.lr.ph
	movq	cube+72(%rip), %rax
	movslq	cube+124(%rip), %rdx
	movq	(%rax,%rdx,8), %rax
	movq	24(%rdi), %rdx
	leaq	(%rdx,%rcx,4), %r8
	movl	$1, %r9d
	movl	%esi, %ecx
	shll	%cl, %r9d
	sarl	$5, %esi
	incl	%esi
	movslq	%esi, %r13
	leaq	-4(%rax), %rcx
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	leaq	4(%rax), %r10
	leaq	-12(%rax), %rcx
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	movl	$1, %r12d
	movl	$2, %r14d
	movq	%r10, -16(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB9_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_12 Depth 2
                                        #     Child Loop BB9_16 Depth 2
	movl	(%rdx), %ecx
	movl	%ecx, %esi
	andl	$1023, %esi             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movq	%rsi, %r11
	cmoveq	%r12, %r11
	cmpq	$8, %r11
	jb	.LBB9_15
# BB#4:                                 # %min.iters.checked
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	%r11, %r15
	andq	$1016, %r15             # imm = 0x3F8
	je	.LBB9_15
# BB#5:                                 # %vector.memcheck
                                        #   in Loop: Header=BB9_3 Depth=1
	leaq	1(%rsi), %rbp
	testl	%esi, %esi
	cmovneq	%r14, %rbp
	leaq	-4(%rdx,%rbp,4), %rbx
	leaq	(%r10,%rsi,4), %rcx
	cmpq	%rcx, %rbx
	jae	.LBB9_7
# BB#6:                                 # %vector.memcheck
                                        #   in Loop: Header=BB9_3 Depth=1
	leaq	4(%rdx,%rsi,4), %rcx
	movq	-8(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rbp,4), %rbp
	cmpq	%rcx, %rbp
	jb	.LBB9_15
.LBB9_7:                                # %vector.body.preheader
                                        #   in Loop: Header=BB9_3 Depth=1
	leaq	-8(%r15), %rcx
	movq	%rcx, %rbx
	shrq	$3, %rbx
	btl	$3, %ecx
	jb	.LBB9_8
# BB#9:                                 # %vector.body.prol
                                        #   in Loop: Header=BB9_3 Depth=1
	movups	-12(%rdx,%rsi,4), %xmm0
	movups	-28(%rdx,%rsi,4), %xmm1
	movups	-12(%rax,%rsi,4), %xmm2
	movups	-28(%rax,%rsi,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -12(%rdx,%rsi,4)
	movups	%xmm3, -28(%rdx,%rsi,4)
	movl	$8, %ebp
	testq	%rbx, %rbx
	jne	.LBB9_11
	jmp	.LBB9_13
.LBB9_8:                                #   in Loop: Header=BB9_3 Depth=1
	xorl	%ebp, %ebp
	testq	%rbx, %rbx
	je	.LBB9_13
.LBB9_11:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	%r11, %rbx
	andq	$-8, %rbx
	negq	%rbx
	negq	%rbp
	leaq	-12(%rdx,%rsi,4), %r14
	movq	-24(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi,4), %r10
	.p2align	4, 0x90
.LBB9_12:                               # %vector.body
                                        #   Parent Loop BB9_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r14,%rbp,4), %xmm0
	movups	-16(%r14,%rbp,4), %xmm1
	movups	(%r10,%rbp,4), %xmm2
	movups	-16(%r10,%rbp,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, (%r14,%rbp,4)
	movups	%xmm3, -16(%r14,%rbp,4)
	movups	-32(%r14,%rbp,4), %xmm0
	movups	-48(%r14,%rbp,4), %xmm1
	movups	-32(%r10,%rbp,4), %xmm2
	movups	-48(%r10,%rbp,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -32(%r14,%rbp,4)
	movups	%xmm3, -48(%r14,%rbp,4)
	addq	$-16, %rbp
	cmpq	%rbp, %rbx
	jne	.LBB9_12
.LBB9_13:                               # %middle.block
                                        #   in Loop: Header=BB9_3 Depth=1
	cmpq	%r15, %r11
	movq	-16(%rsp), %r10         # 8-byte Reload
	movl	$2, %r14d
	je	.LBB9_17
# BB#14:                                #   in Loop: Header=BB9_3 Depth=1
	subq	%r15, %rsi
	.p2align	4, 0x90
.LBB9_15:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB9_3 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB9_16:                               # %scalar.ph
                                        #   Parent Loop BB9_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rax,%rsi,4), %ecx
	notl	%ecx
	andl	%ecx, -4(%rdx,%rsi,4)
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB9_16
.LBB9_17:                               # %.loopexit56
                                        #   in Loop: Header=BB9_3 Depth=1
	orl	%r9d, (%rdx,%r13,4)
	movslq	(%rdi), %rcx
	leaq	(%rdx,%rcx,4), %rdx
	cmpq	%r8, %rdx
	jb	.LBB9_3
.LBB9_18:                               # %.loopexit
	movq	%rdi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	uncof_output, .Lfunc_end9-uncof_output
	.cfi_endproc

	.globl	foreach_output_function
	.p2align	4, 0x90
	.type	foreach_output_function,@function
foreach_output_function:                # @foreach_output_function
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi96:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi97:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi99:
	.cfi_def_cfa_offset 128
.Lcfi100:
	.cfi_offset %rbx, -56
.Lcfi101:
	.cfi_offset %r12, -48
.Lcfi102:
	.cfi_offset %r13, -40
.Lcfi103:
	.cfi_offset %r14, -32
.Lcfi104:
	.cfi_offset %r15, -24
.Lcfi105:
	.cfi_offset %rbp, -16
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movq	cube+32(%rip), %rax
	movslq	cube+124(%rip), %rcx
	cmpl	$0, (%rax,%rcx,4)
	jle	.LBB10_65
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB10_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_10 Depth 2
                                        #       Child Loop BB10_19 Depth 3
                                        #       Child Loop BB10_23 Depth 3
                                        #     Child Loop BB10_29 Depth 2
                                        #       Child Loop BB10_38 Depth 3
                                        #       Child Loop BB10_42 Depth 3
                                        #     Child Loop BB10_48 Depth 2
                                        #       Child Loop BB10_58 Depth 3
                                        #       Child Loop BB10_62 Depth 3
	xorl	%eax, %eax
	callq	new_PLA
	movq	%rax, %rbx
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	(%r14), %rdi
	movq	cube+16(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movl	(%rax,%rcx,4), %esi
	addl	%ebp, %esi
	callq	cof_output
	movq	%rax, (%rbx)
	movq	16(%r14), %rdi
	movq	cube+16(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movl	(%rax,%rcx,4), %esi
	addl	%ebp, %esi
	callq	cof_output
	movq	%rax, 16(%rbx)
	movq	8(%r14), %rdi
	movq	cube+16(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movl	(%rax,%rcx,4), %esi
	addl	%ebp, %esi
	callq	cof_output
	movq	%rax, 8(%rbx)
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	*56(%rsp)               # 8-byte Folded Reload
	testl	%eax, %eax
	je	.LBB10_4
# BB#5:                                 #   in Loop: Header=BB10_3 Depth=1
	movq	(%rbx), %r11
	movq	cube+16(%rip), %rsi
	movl	cube+124(%rip), %r8d
	movslq	%r8d, %rax
	movl	(%rsi,%rax,4), %ecx
	testq	%r11, %r11
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	je	.LBB10_6
# BB#7:                                 #   in Loop: Header=BB10_3 Depth=1
	movslq	(%r11), %rdi
	movslq	12(%r11), %rbp
	imulq	%rdi, %rbp
	testl	%ebp, %ebp
	jle	.LBB10_8
# BB#9:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	addl	4(%rsp), %ecx           # 4-byte Folded Reload
	movq	cube+72(%rip), %rsi
	movq	(%rsi,%rax,8), %rsi
	movq	24(%r11), %rdi
	leaq	(%rdi,%rbp,4), %rdx
	movl	$1, %r12d
	shll	%cl, %r12d
	sarl	$5, %ecx
	incl	%ecx
	movslq	%ecx, %r8
	leaq	-4(%rsi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	4(%rsi), %r9
	leaq	-12(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%r9, 8(%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB10_10:                              #   Parent Loop BB10_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_19 Depth 3
                                        #       Child Loop BB10_23 Depth 3
	movl	(%rdi), %ebx
	movl	%ebx, %ebp
	andl	$1023, %ebp             # imm = 0x3FF
	testw	$1023, %bx              # imm = 0x3FF
	movl	$1, %r15d
	cmovneq	%rbp, %r15
	cmpq	$8, %r15
	jb	.LBB10_22
# BB#11:                                # %min.iters.checked126
                                        #   in Loop: Header=BB10_10 Depth=2
	movq	%r15, %r10
	andq	$1016, %r10             # imm = 0x3F8
	je	.LBB10_22
# BB#12:                                # %vector.memcheck148
                                        #   in Loop: Header=BB10_10 Depth=2
	leaq	1(%rbp), %rbx
	testl	%ebp, %ebp
	movl	$2, %eax
	cmovneq	%rax, %rbx
	leaq	-4(%rdi,%rbx,4), %rcx
	leaq	(%r9,%rbp,4), %rax
	cmpq	%rax, %rcx
	jae	.LBB10_14
# BB#13:                                # %vector.memcheck148
                                        #   in Loop: Header=BB10_10 Depth=2
	leaq	4(%rdi,%rbp,4), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rbx,4), %rcx
	cmpq	%rax, %rcx
	jb	.LBB10_22
.LBB10_14:                              # %vector.body121.preheader
                                        #   in Loop: Header=BB10_10 Depth=2
	movq	%r8, %rcx
	leaq	-8(%r10), %rax
	movq	%rax, %rbx
	shrq	$3, %rbx
	btl	$3, %eax
	jb	.LBB10_15
# BB#16:                                # %vector.body121.prol
                                        #   in Loop: Header=BB10_10 Depth=2
	movups	-12(%rdi,%rbp,4), %xmm0
	movups	-28(%rdi,%rbp,4), %xmm1
	movups	-12(%rsi,%rbp,4), %xmm2
	movups	-28(%rsi,%rbp,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -12(%rdi,%rbp,4)
	movups	%xmm3, -28(%rdi,%rbp,4)
	movl	$8, %r14d
	testq	%rbx, %rbx
	jne	.LBB10_18
	jmp	.LBB10_20
.LBB10_15:                              #   in Loop: Header=BB10_10 Depth=2
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	je	.LBB10_20
.LBB10_18:                              # %vector.body121.preheader.new
                                        #   in Loop: Header=BB10_10 Depth=2
	movq	%r15, %r8
	andq	$-8, %r8
	negq	%r8
	negq	%r14
	leaq	-12(%rdi,%rbp,4), %r13
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbp,4), %r9
	.p2align	4, 0x90
.LBB10_19:                              # %vector.body121
                                        #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	(%r13,%r14,4), %xmm0
	movups	-16(%r13,%r14,4), %xmm1
	movups	(%r9,%r14,4), %xmm2
	movups	-16(%r9,%r14,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, (%r13,%r14,4)
	movups	%xmm3, -16(%r13,%r14,4)
	movups	-32(%r13,%r14,4), %xmm0
	movups	-48(%r13,%r14,4), %xmm1
	movups	-32(%r9,%r14,4), %xmm2
	movups	-48(%r9,%r14,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -32(%r13,%r14,4)
	movups	%xmm3, -48(%r13,%r14,4)
	addq	$-16, %r14
	cmpq	%r14, %r8
	jne	.LBB10_19
.LBB10_20:                              # %middle.block122
                                        #   in Loop: Header=BB10_10 Depth=2
	cmpq	%r10, %r15
	movq	%rcx, %r8
	movq	8(%rsp), %r9            # 8-byte Reload
	je	.LBB10_24
# BB#21:                                #   in Loop: Header=BB10_10 Depth=2
	subq	%r10, %rbp
	.p2align	4, 0x90
.LBB10_22:                              # %scalar.ph123.preheader
                                        #   in Loop: Header=BB10_10 Depth=2
	incq	%rbp
	.p2align	4, 0x90
.LBB10_23:                              # %scalar.ph123
                                        #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rsi,%rbp,4), %eax
	notl	%eax
	andl	%eax, -4(%rdi,%rbp,4)
	decq	%rbp
	cmpq	$1, %rbp
	jg	.LBB10_23
.LBB10_24:                              # %.loopexit172
                                        #   in Loop: Header=BB10_10 Depth=2
	orl	%r12d, (%rdi,%r8,4)
	movslq	(%r11), %rax
	leaq	(%rdi,%rax,4), %rdi
	cmpq	%rdx, %rdi
	jb	.LBB10_10
# BB#25:                                # %uncof_output.exit.loopexit
                                        #   in Loop: Header=BB10_3 Depth=1
	movslq	cube+124(%rip), %r8
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	(%rsi,%r8,4), %ecx
	movl	$1, %edi
	movl	4(%rsp), %ebp           # 4-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB10_26
	.p2align	4, 0x90
.LBB10_6:                               #   in Loop: Header=BB10_3 Depth=1
	movl	$1, %edi
	jmp	.LBB10_26
	.p2align	4, 0x90
.LBB10_8:                               #   in Loop: Header=BB10_3 Depth=1
	movl	$1, %edi
	movl	4(%rsp), %ebp           # 4-byte Reload
.LBB10_26:                              # %uncof_output.exit
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	16(%rbx), %r13
	testq	%r13, %r13
	je	.LBB10_45
# BB#27:                                #   in Loop: Header=BB10_3 Depth=1
	movslq	(%r13), %rdx
	movslq	12(%r13), %rax
	imulq	%rdx, %rax
	testl	%eax, %eax
	jle	.LBB10_45
# BB#28:                                # %.lr.ph.i31
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	addl	%ebp, %ecx
	movslq	%r8d, %rdx
	movq	cube+72(%rip), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	24(%r13), %rdi
	leaq	(%rdi,%rax,4), %r8
	movl	$1, %r14d
	shll	%cl, %r14d
	sarl	$5, %ecx
	incl	%ecx
	movslq	%ecx, %r15
	leaq	-4(%rsi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	4(%rsi), %r11
	leaq	-12(%rsi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB10_29:                              #   Parent Loop BB10_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_38 Depth 3
                                        #       Child Loop BB10_42 Depth 3
	movl	(%rdi), %eax
	movl	%eax, %ebp
	andl	$1023, %ebp             # imm = 0x3FF
	testw	$1023, %ax              # imm = 0x3FF
	movl	$1, %r10d
	cmovneq	%rbp, %r10
	cmpq	$8, %r10
	jb	.LBB10_41
# BB#30:                                # %min.iters.checked77
                                        #   in Loop: Header=BB10_29 Depth=2
	movq	%r10, %r12
	andq	$1016, %r12             # imm = 0x3F8
	je	.LBB10_41
# BB#31:                                # %vector.memcheck99
                                        #   in Loop: Header=BB10_29 Depth=2
	leaq	1(%rbp), %rax
	testl	%ebp, %ebp
	movl	$2, %edx
	cmovneq	%rdx, %rax
	leaq	-4(%rdi,%rax,4), %rdx
	leaq	(%r11,%rbp,4), %rbx
	cmpq	%rbx, %rdx
	jae	.LBB10_33
# BB#32:                                # %vector.memcheck99
                                        #   in Loop: Header=BB10_29 Depth=2
	leaq	4(%rdi,%rbp,4), %rdx
	movq	16(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rax,4), %rax
	cmpq	%rdx, %rax
	jb	.LBB10_41
.LBB10_33:                              # %vector.body72.preheader
                                        #   in Loop: Header=BB10_29 Depth=2
	leaq	-8(%r12), %rax
	movq	%rax, %rdx
	shrq	$3, %rdx
	btl	$3, %eax
	jb	.LBB10_34
# BB#35:                                # %vector.body72.prol
                                        #   in Loop: Header=BB10_29 Depth=2
	movups	-12(%rdi,%rbp,4), %xmm0
	movups	-28(%rdi,%rbp,4), %xmm1
	movups	-12(%rsi,%rbp,4), %xmm2
	movups	-28(%rsi,%rbp,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -12(%rdi,%rbp,4)
	movups	%xmm3, -28(%rdi,%rbp,4)
	movl	$8, %eax
	testq	%rdx, %rdx
	jne	.LBB10_37
	jmp	.LBB10_39
.LBB10_34:                              #   in Loop: Header=BB10_29 Depth=2
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.LBB10_39
.LBB10_37:                              # %vector.body72.preheader.new
                                        #   in Loop: Header=BB10_29 Depth=2
	movq	%r10, %rbx
	andq	$-8, %rbx
	negq	%rbx
	negq	%rax
	leaq	-12(%rdi,%rbp,4), %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%rbp,4), %r9
	.p2align	4, 0x90
.LBB10_38:                              # %vector.body72
                                        #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	(%rdx,%rax,4), %xmm0
	movups	-16(%rdx,%rax,4), %xmm1
	movups	(%r9,%rax,4), %xmm2
	movups	-16(%r9,%rax,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, (%rdx,%rax,4)
	movups	%xmm3, -16(%rdx,%rax,4)
	movups	-32(%rdx,%rax,4), %xmm0
	movups	-48(%rdx,%rax,4), %xmm1
	movups	-32(%r9,%rax,4), %xmm2
	movups	-48(%r9,%rax,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -32(%rdx,%rax,4)
	movups	%xmm3, -48(%rdx,%rax,4)
	addq	$-16, %rax
	cmpq	%rax, %rbx
	jne	.LBB10_38
.LBB10_39:                              # %middle.block73
                                        #   in Loop: Header=BB10_29 Depth=2
	cmpq	%r12, %r10
	je	.LBB10_43
# BB#40:                                #   in Loop: Header=BB10_29 Depth=2
	subq	%r12, %rbp
	.p2align	4, 0x90
.LBB10_41:                              # %scalar.ph74.preheader
                                        #   in Loop: Header=BB10_29 Depth=2
	incq	%rbp
	.p2align	4, 0x90
.LBB10_42:                              # %scalar.ph74
                                        #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rsi,%rbp,4), %eax
	notl	%eax
	andl	%eax, -4(%rdi,%rbp,4)
	decq	%rbp
	cmpq	$1, %rbp
	jg	.LBB10_42
.LBB10_43:                              # %.loopexit171
                                        #   in Loop: Header=BB10_29 Depth=2
	orl	%r14d, (%rdi,%r15,4)
	movslq	(%r13), %rax
	leaq	(%rdi,%rax,4), %rdi
	cmpq	%r8, %rdi
	jb	.LBB10_29
# BB#44:                                # %uncof_output.exit35.loopexit
                                        #   in Loop: Header=BB10_3 Depth=1
	movslq	cube+124(%rip), %r8
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r8,4), %ecx
	movl	$1, %edi
	movq	32(%rsp), %rbx          # 8-byte Reload
.LBB10_45:                              # %uncof_output.exit35
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	8(%rbx), %r13
	testq	%r13, %r13
	je	.LBB10_64
# BB#46:                                #   in Loop: Header=BB10_3 Depth=1
	movslq	(%r13), %rdx
	movslq	12(%r13), %rax
	imulq	%rdx, %rax
	testl	%eax, %eax
	jle	.LBB10_64
# BB#47:                                # %.lr.ph.i36
                                        #   in Loop: Header=BB10_3 Depth=1
	addl	4(%rsp), %ecx           # 4-byte Folded Reload
	movslq	%r8d, %rdx
	movq	cube+72(%rip), %rsi
	movq	(%rsi,%rdx,8), %rdx
	movq	24(%r13), %rsi
	leaq	(%rsi,%rax,4), %r11
	movl	$1, %r14d
	shll	%cl, %r14d
	sarl	$5, %ecx
	incl	%ecx
	movslq	%ecx, %r15
	leaq	-4(%rdx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	4(%rdx), %r10
	leaq	-12(%rdx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB10_48:                              #   Parent Loop BB10_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_58 Depth 3
                                        #       Child Loop BB10_62 Depth 3
	movl	(%rsi), %eax
	movl	%eax, %ebp
	andl	$1023, %ebp             # imm = 0x3FF
	testw	$1023, %ax              # imm = 0x3FF
	movq	%rbp, %r9
	cmoveq	%rdi, %r9
	cmpq	$8, %r9
	jb	.LBB10_61
# BB#49:                                # %min.iters.checked
                                        #   in Loop: Header=BB10_48 Depth=2
	movq	%r9, %r12
	andq	$1016, %r12             # imm = 0x3F8
	je	.LBB10_61
# BB#50:                                # %vector.memcheck
                                        #   in Loop: Header=BB10_48 Depth=2
	leaq	1(%rbp), %rax
	testl	%ebp, %ebp
	movl	$2, %edi
	cmovneq	%rdi, %rax
	leaq	-4(%rsi,%rax,4), %rdi
	leaq	(%r10,%rbp,4), %rbx
	cmpq	%rbx, %rdi
	jae	.LBB10_53
# BB#51:                                # %vector.memcheck
                                        #   in Loop: Header=BB10_48 Depth=2
	leaq	4(%rsi,%rbp,4), %rdi
	movq	16(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rax,4), %rax
	cmpq	%rdi, %rax
	jae	.LBB10_53
# BB#52:                                #   in Loop: Header=BB10_48 Depth=2
	movl	$1, %edi
	jmp	.LBB10_61
.LBB10_53:                              # %vector.body.preheader
                                        #   in Loop: Header=BB10_48 Depth=2
	leaq	-8(%r12), %rdi
	movq	%rdi, %rax
	shrq	$3, %rax
	btl	$3, %edi
	jb	.LBB10_54
# BB#55:                                # %vector.body.prol
                                        #   in Loop: Header=BB10_48 Depth=2
	movups	-12(%rsi,%rbp,4), %xmm0
	movups	-28(%rsi,%rbp,4), %xmm1
	movups	-12(%rdx,%rbp,4), %xmm2
	movups	-28(%rdx,%rbp,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -12(%rsi,%rbp,4)
	movups	%xmm3, -28(%rsi,%rbp,4)
	movl	$8, %ebx
	testq	%rax, %rax
	jne	.LBB10_57
	jmp	.LBB10_59
.LBB10_54:                              #   in Loop: Header=BB10_48 Depth=2
	xorl	%ebx, %ebx
	testq	%rax, %rax
	je	.LBB10_59
.LBB10_57:                              # %vector.body.preheader.new
                                        #   in Loop: Header=BB10_48 Depth=2
	movq	%r9, %rdi
	andq	$-8, %rdi
	negq	%rdi
	negq	%rbx
	leaq	-12(%rsi,%rbp,4), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%rbp,4), %r8
	.p2align	4, 0x90
.LBB10_58:                              # %vector.body
                                        #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_48 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	(%rax,%rbx,4), %xmm0
	movups	-16(%rax,%rbx,4), %xmm1
	movups	(%r8,%rbx,4), %xmm2
	movups	-16(%r8,%rbx,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, (%rax,%rbx,4)
	movups	%xmm3, -16(%rax,%rbx,4)
	movups	-32(%rax,%rbx,4), %xmm0
	movups	-48(%rax,%rbx,4), %xmm1
	movups	-32(%r8,%rbx,4), %xmm2
	movups	-48(%r8,%rbx,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -32(%rax,%rbx,4)
	movups	%xmm3, -48(%rax,%rbx,4)
	addq	$-16, %rbx
	cmpq	%rbx, %rdi
	jne	.LBB10_58
.LBB10_59:                              # %middle.block
                                        #   in Loop: Header=BB10_48 Depth=2
	cmpq	%r12, %r9
	movl	$1, %edi
	je	.LBB10_63
# BB#60:                                #   in Loop: Header=BB10_48 Depth=2
	subq	%r12, %rbp
	.p2align	4, 0x90
.LBB10_61:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB10_48 Depth=2
	incq	%rbp
	.p2align	4, 0x90
.LBB10_62:                              # %scalar.ph
                                        #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_48 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rdx,%rbp,4), %eax
	notl	%eax
	andl	%eax, -4(%rsi,%rbp,4)
	decq	%rbp
	cmpq	$1, %rbp
	jg	.LBB10_62
.LBB10_63:                              # %.loopexit170
                                        #   in Loop: Header=BB10_48 Depth=2
	orl	%r14d, (%rsi,%r15,4)
	movslq	(%r13), %rax
	leaq	(%rsi,%rax,4), %rsi
	cmpq	%r11, %rsi
	jb	.LBB10_48
.LBB10_64:                              # %uncof_output.exit40
                                        #   in Loop: Header=BB10_3 Depth=1
	xorl	%eax, %eax
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movl	4(%rsp), %ebp           # 4-byte Reload
	movl	%ebp, %esi
	callq	*40(%rsp)               # 8-byte Folded Reload
	movl	%eax, %r14d
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	free_PLA
	testl	%r14d, %r14d
	je	.LBB10_65
# BB#2:                                 #   in Loop: Header=BB10_3 Depth=1
	incl	%ebp
	movq	cube+32(%rip), %rax
	movslq	cube+124(%rip), %rcx
	cmpl	(%rax,%rcx,4), %ebp
	jl	.LBB10_3
	jmp	.LBB10_65
.LBB10_4:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	free_PLA
.LBB10_65:                              # %.loopexit
	xorl	%eax, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	foreach_output_function, .Lfunc_end10-foreach_output_function
	.cfi_endproc

	.globl	so_espresso
	.p2align	4, 0x90
	.type	so_espresso,@function
so_espresso:                            # @so_espresso
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi108:
	.cfi_def_cfa_offset 32
.Lcfi109:
	.cfi_offset %rbx, -24
.Lcfi110:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	movl	12(%rax), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, Fmin(%rip)
	testl	%ebp, %ebp
	je	.LBB11_1
# BB#2:
	movl	$so_do_exact, %esi
	jmp	.LBB11_3
.LBB11_1:
	movl	$so_do_espresso, %esi
.LBB11_3:
	movl	$so_save, %edx
	movq	%rbx, %rdi
	callq	foreach_output_function
	movq	(%rbx), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	Fmin(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end11:
	.size	so_espresso, .Lfunc_end11-so_espresso
	.cfi_endproc

	.globl	so_both_espresso
	.p2align	4, 0x90
	.type	so_both_espresso,@function
so_both_espresso:                       # @so_both_espresso
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi111:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 32
.Lcfi114:
	.cfi_offset %rbx, -32
.Lcfi115:
	.cfi_offset %r14, -24
.Lcfi116:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	cube+88(%rip), %rbp
	movl	(%rbp), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	cmpl	$33, %eax
	jae	.LBB12_2
# BB#1:
	movl	$8, %edi
	jmp	.LBB12_3
.LBB12_2:
	decl	%eax
	shrl	$5, %eax
	leal	8(,%rax,4), %edi
.LBB12_3:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbp, %rsi
	callq	set_copy
	movq	%rax, phase(%rip)
	movq	(%rbx), %rax
	movl	12(%rax), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, Fmin(%rip)
	testl	%r14d, %r14d
	je	.LBB12_4
# BB#5:
	movl	$so_both_do_exact, %esi
	jmp	.LBB12_6
.LBB12_4:
	movl	$so_both_do_espresso, %esi
.LBB12_6:
	movl	$so_both_save, %edx
	movq	%rbx, %rdi
	callq	foreach_output_function
	movq	(%rbx), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	Fmin(%rip), %rax
	movq	%rax, (%rbx)
	movq	phase(%rip), %rax
	movq	%rax, 40(%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end12:
	.size	so_both_espresso, .Lfunc_end12-so_both_espresso
	.cfi_endproc

	.globl	so_do_espresso
	.p2align	4, 0x90
	.type	so_do_espresso,@function
so_do_espresso:                         # @so_do_espresso
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi117:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi118:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi119:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi120:
	.cfi_def_cfa_offset 64
.Lcfi121:
	.cfi_offset %rbx, -32
.Lcfi122:
	.cfi_offset %r14, -24
.Lcfi123:
	.cfi_offset %r15, -16
	movl	%esi, %ecx
	movq	%rdi, %rbx
	movl	$1, skip_make_sparse(%rip)
	movq	%rsp, %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movl	%ecx, %edx
	callq	sprintf
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r14
	movq	(%rbx), %rdi
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	xorl	%eax, %eax
	callq	espresso
	movq	%rax, %r15
	movq	%r15, (%rbx)
	cmpl	$0, summary(%rip)
	je	.LBB13_2
# BB#1:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r14, %rcx
	movq	%rsp, %rsi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB13_2:
	movl	$1, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	so_do_espresso, .Lfunc_end13-so_do_espresso
	.cfi_endproc

	.globl	so_do_exact
	.p2align	4, 0x90
	.type	so_do_exact,@function
so_do_exact:                            # @so_do_exact
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi124:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi125:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi127:
	.cfi_def_cfa_offset 64
.Lcfi128:
	.cfi_offset %rbx, -32
.Lcfi129:
	.cfi_offset %r14, -24
.Lcfi130:
	.cfi_offset %r15, -16
	movl	%esi, %ecx
	movq	%rdi, %rbx
	movl	$1, skip_make_sparse(%rip)
	movq	%rsp, %rdi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movl	%ecx, %edx
	callq	sprintf
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r14
	movq	(%rbx), %rdi
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	minimize_exact
	movq	%rax, %r15
	movq	%r15, (%rbx)
	cmpl	$0, summary(%rip)
	je	.LBB14_2
# BB#1:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r14, %rcx
	movq	%rsp, %rsi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB14_2:
	movl	$1, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	so_do_exact, .Lfunc_end14-so_do_exact
	.cfi_endproc

	.globl	so_save
	.p2align	4, 0x90
	.type	so_save,@function
so_save:                                # @so_save
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi131:
	.cfi_def_cfa_offset 16
.Lcfi132:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	Fmin(%rip), %rdi
	movq	(%rbx), %rsi
	xorl	%eax, %eax
	callq	sf_append
	movq	%rax, Fmin(%rip)
	movq	$0, (%rbx)
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end15:
	.size	so_save, .Lfunc_end15-so_save
	.cfi_endproc

	.globl	so_both_do_espresso
	.p2align	4, 0x90
	.type	so_both_do_espresso,@function
so_both_do_espresso:                    # @so_both_do_espresso
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi133:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi134:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi135:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi136:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi137:
	.cfi_def_cfa_offset 80
.Lcfi138:
	.cfi_offset %rbx, -40
.Lcfi139:
	.cfi_offset %r14, -32
.Lcfi140:
	.cfi_offset %r15, -24
.Lcfi141:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movq	%rsp, %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	sprintf
	movl	$1, skip_make_sparse(%rip)
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r14
	movq	(%rbx), %rdi
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	xorl	%eax, %eax
	callq	espresso
	movq	%rax, %rbp
	movq	%rbp, (%rbx)
	cmpl	$0, summary(%rip)
	je	.LBB16_2
# BB#1:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r14, %rcx
	movq	%rsp, %rsi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB16_2:
	movq	%rsp, %rdi
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	sprintf
	movl	$1, skip_make_sparse(%rip)
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rbx), %rdx
	movq	8(%rbx), %rsi
	xorl	%eax, %eax
	callq	espresso
	movq	%rax, %rbp
	movq	%rbp, 16(%rbx)
	cmpl	$0, summary(%rip)
	je	.LBB16_4
# BB#3:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r14, %rcx
	movq	%rsp, %rsi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB16_4:
	movl	$1, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	so_both_do_espresso, .Lfunc_end16-so_both_do_espresso
	.cfi_endproc

	.globl	so_both_do_exact
	.p2align	4, 0x90
	.type	so_both_do_exact,@function
so_both_do_exact:                       # @so_both_do_exact
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi144:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi145:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi146:
	.cfi_def_cfa_offset 80
.Lcfi147:
	.cfi_offset %rbx, -40
.Lcfi148:
	.cfi_offset %r14, -32
.Lcfi149:
	.cfi_offset %r15, -24
.Lcfi150:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movq	%rsp, %rdi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	sprintf
	movl	$1, skip_make_sparse(%rip)
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r14
	movq	(%rbx), %rdi
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	minimize_exact
	movq	%rax, %rbp
	movq	%rbp, (%rbx)
	cmpl	$0, summary(%rip)
	je	.LBB17_2
# BB#1:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r14, %rcx
	movq	%rsp, %rsi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB17_2:
	movq	%rsp, %rdi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	sprintf
	movl	$1, skip_make_sparse(%rip)
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	movq	(%rbx), %rdx
	movq	8(%rbx), %rsi
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	minimize_exact
	movq	%rax, %rbp
	movq	%rbp, 16(%rbx)
	cmpl	$0, summary(%rip)
	je	.LBB17_4
# BB#3:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r14, %rcx
	movq	%rsp, %rsi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB17_4:
	movl	$1, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	so_both_do_exact, .Lfunc_end17-so_both_do_exact
	.cfi_endproc

	.globl	so_both_save
	.p2align	4, 0x90
	.type	so_both_save,@function
so_both_save:                           # @so_both_save
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi151:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi152:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi153:
	.cfi_def_cfa_offset 32
.Lcfi154:
	.cfi_offset %rbx, -24
.Lcfi155:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	movq	16(%rbx), %rcx
	movl	12(%rdi), %eax
	cmpl	12(%rcx), %eax
	jle	.LBB18_2
# BB#1:
	xorl	%eax, %eax
	callq	sf_free
	movq	16(%rbx), %rsi
	movq	%rsi, (%rbx)
	movq	$0, 16(%rbx)
	movq	cube+16(%rip), %rax
	movslq	cube+124(%rip), %rcx
	addl	(%rax,%rcx,4), %ebp
	movl	$-2, %eax
	movl	%ebp, %ecx
	roll	%cl, %eax
	movq	phase(%rip), %rcx
	sarl	$5, %ebp
	movslq	%ebp, %rdx
	andl	%eax, 4(%rcx,%rdx,4)
	jmp	.LBB18_3
.LBB18_2:
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_free
	movq	$0, 16(%rbx)
	movq	(%rbx), %rsi
.LBB18_3:
	movq	Fmin(%rip), %rdi
	xorl	%eax, %eax
	callq	sf_append
	movq	%rax, Fmin(%rip)
	movq	$0, (%rbx)
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end18:
	.size	so_both_save, .Lfunc_end18-so_both_save
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"unreasonable expansion in unravel"
	.size	.L.str, 34

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"COMPONENT_REDUCTION: split into %d %d\n"
	.size	.L.str.1, 39

	.type	Fmin,@object            # @Fmin
	.local	Fmin
	.comm	Fmin,8,8
	.type	phase,@object           # @phase
	.local	phase
	.comm	phase,8,8
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"ESPRESSO-POS(%d)"
	.size	.L.str.2, 17

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"EXACT-POS(%d)"
	.size	.L.str.3, 14

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"ESPRESSO-NEG(%d)"
	.size	.L.str.4, 17

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"EXACT-NEG(%d)"
	.size	.L.str.5, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
