	.text
	.file	"c4.bc"
	.globl	c4_init
	.p2align	4, 0x90
	.type	c4_init,@function
c4_init:                                # @c4_init
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	jmp	trans_init              # TAILCALL
.Lfunc_end0:
	.size	c4_init, .Lfunc_end0-c4_init
	.cfi_endproc

	.globl	ab
	.p2align	4, 0x90
	.type	ab,@function
ab:                                     # @ab
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 160
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	incq	nodes(%rip)
	movl	%esi, %r13d
	movl	%edi, %r12d
	movl	plycnt(%rip), %ecx
	xorl	%r14d, %r14d
	cmpl	$41, %ecx
	je	.LBB1_60
# BB#1:
	andl	$1, %ecx
	movl	%ecx, %r15d
	xorl	$1, %r15d
	movl	$1, %eax
	movl	$1, %r14d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r14d
	xorl	%ecx, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
.LBB1_2:                                # %.lr.ph174
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_3 Depth 2
	movslq	%eax, %rbx
	.p2align	4, 0x90
.LBB1_3:                                #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	height(,%rbx,4), %ebp
	cmpl	$7, %ebp
	jge	.LBB1_8
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=2
	movl	$3, %edx
	xorl	%eax, %eax
	movl	%ebx, %edi
	movl	%ebp, %esi
	callq	wins
	testl	%eax, %eax
	jne	.LBB1_13
# BB#5:                                 #   in Loop: Header=BB1_3 Depth=2
	movslq	columns(,%rbx,4), %rax
	cmpl	$0, colthr(,%rax,4)
	jne	.LBB1_13
# BB#6:                                 #   in Loop: Header=BB1_3 Depth=2
	incl	%ebp
	cmpl	$6, %ebp
	jg	.LBB1_9
# BB#7:                                 #   in Loop: Header=BB1_3 Depth=2
	xorl	%eax, %eax
	movl	%ebx, %edi
	movl	%ebp, %esi
	movl	%r14d, %edx
	callq	wins
	testl	%eax, %eax
	je	.LBB1_9
.LBB1_8:                                # %.backedge147
                                        #   in Loop: Header=BB1_3 Depth=2
	incq	%rbx
	cmpq	$8, %rbx
	jl	.LBB1_3
	jmp	.LBB1_10
	.p2align	4, 0x90
.LBB1_9:                                # %.outer
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%ebx, 48(%rsp,%rax,4)
	incq	%rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leal	1(%rbx), %eax
	cmpl	$8, %eax
	jl	.LBB1_2
.LBB1_10:                               # %.outer._crit_edge
	movq	8(%rsp), %rbx           # 8-byte Reload
	testl	%ebx, %ebx
	je	.LBB1_22
# BB#11:                                # %.outer._crit_edge
	cmpl	$1, %ebx
	jne	.LBB1_23
# BB#12:                                # %.outer._crit_edge..thread_crit_edge
	movl	48(%rsp), %ebx
	jmp	.LBB1_21
.LBB1_13:
	incl	%ebp
	cmpl	$6, %ebp
	jg	.LBB1_15
# BB#14:
	xorl	%eax, %eax
	movl	%ebx, %edi
	movl	%ebp, %esi
	movl	%r14d, %edx
	callq	wins
	movl	$-2, %r14d
	testl	%eax, %eax
	jne	.LBB1_60
.LBB1_15:
	movl	%ebx, 48(%rsp)
	leal	1(%rbx), %eax
	cmpl	$7, %eax
	jg	.LBB1_21
# BB#16:                                # %.lr.ph.preheader
	movslq	%eax, %rbp
	movl	$-2, %r14d
	.p2align	4, 0x90
.LBB1_17:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	height(,%rbp,4), %esi
	cmpl	$7, %esi
	jge	.LBB1_20
# BB#18:                                #   in Loop: Header=BB1_17 Depth=1
	movl	$3, %edx
	xorl	%eax, %eax
	movl	%ebp, %edi
	callq	wins
	testl	%eax, %eax
	jne	.LBB1_60
# BB#19:                                #   in Loop: Header=BB1_17 Depth=1
	movslq	columns(,%rbp,4), %rax
	cmpl	$0, colthr(,%rax,4)
	jne	.LBB1_60
.LBB1_20:                               # %.backedge
                                        #   in Loop: Header=BB1_17 Depth=1
	incq	%rbp
	cmpq	$8, %rbp
	jl	.LBB1_17
.LBB1_21:                               # %.thread
	xorl	%eax, %eax
	movl	%ebx, %edi
	callq	makemove
	negl	%r13d
	negl	%r12d
	movl	%r13d, %edi
	movl	%r12d, %esi
	callq	ab
	movl	%eax, %r14d
	negl	%r14d
	xorl	%eax, %eax
	callq	backmove
	jmp	.LBB1_60
.LBB1_22:
	movl	$-2, %r14d
	jmp	.LBB1_60
.LBB1_23:
	xorl	%eax, %eax
	callq	transpose
	cmpl	$-128, %eax
	je	.LBB1_29
# BB#24:
	movl	%eax, %r14d
	sarl	$5, %r14d
	cmpl	$1, %r14d
	je	.LBB1_28
# BB#25:
	cmpl	$-1, %r14d
	jne	.LBB1_60
# BB#26:
	movl	$-1, %r14d
	testl	%r12d, %r12d
	jns	.LBB1_60
# BB#27:
	xorl	%r13d, %r13d
	jmp	.LBB1_29
.LBB1_28:
	xorl	%r12d, %r12d
	testl	%r13d, %r13d
	jle	.LBB1_62
.LBB1_29:
	movq	posed(%rip), %rdx
	testl	%ebx, %ebx
	jle	.LBB1_48
# BB#30:                                # %.preheader145.lr.ph
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	%r12d, 20(%rsp)         # 4-byte Spill
	movl	%r13d, 36(%rsp)         # 4-byte Spill
	negl	%r13d
	movl	%r13d, 40(%rsp)         # 4-byte Spill
	movslq	%ebx, %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	%eax, %r13d
	leaq	-1(%r13), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movl	$-999999, %ecx          # imm = 0xFFF0BDC1
	xorl	%r12d, %r12d
	movq	%r15, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_31:                               # %.lr.ph164.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_34 Depth 2
	movl	%ebx, %eax
	subl	%ebp, %eax
	testb	$1, %al
	movl	%ecx, 44(%rsp)          # 4-byte Spill
	jne	.LBB1_33
# BB#32:                                #   in Loop: Header=BB1_31 Depth=1
	movq	%rbp, %rcx
	movl	$-999999, %eax          # imm = 0xFFF0BDC1
	cmpq	%rbp, 96(%rsp)          # 8-byte Folded Reload
	jne	.LBB1_34
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_33:                               # %.lr.ph164.prol
                                        #   in Loop: Header=BB1_31 Depth=1
	movslq	48(%rsp,%rbp,4), %rax
	movl	height(,%rax,4), %ecx
	shll	$3, %ecx
	orl	%eax, %ecx
	movslq	%ecx, %rax
	imulq	$224, %r15, %rcx
	movl	history(%rcx,%rax,4), %eax
	cmpl	$-999999, %eax          # imm = 0xFFF0BDC1
	cmovgl	%ebp, %r12d
	cmpl	$-1000000, %eax         # imm = 0xFFF0BDC0
	movl	$-999999, %ecx          # imm = 0xFFF0BDC1
	cmovlel	%ecx, %eax
	leaq	1(%rbp), %rcx
	cmpq	%rbp, 96(%rsp)          # 8-byte Folded Reload
	je	.LBB1_35
	.p2align	4, 0x90
.LBB1_34:                               # %.lr.ph164
                                        #   Parent Loop BB1_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	48(%rsp,%rcx,4), %rdx
	movl	height(,%rdx,4), %esi
	shll	$3, %esi
	orl	%edx, %esi
	movslq	%esi, %rdx
	imulq	$224, %r15, %rsi
	movl	history(%rsi,%rdx,4), %edx
	cmpl	%eax, %edx
	cmovgl	%ecx, %r12d
	cmovgel	%edx, %eax
	movslq	52(%rsp,%rcx,4), %rdx
	movl	height(,%rdx,4), %edi
	shll	$3, %edi
	orl	%edx, %edi
	movslq	%edi, %rdx
	movl	history(%rsi,%rdx,4), %edx
	leal	1(%rcx), %esi
	cmpl	%eax, %edx
	cmovgl	%esi, %r12d
	cmovgel	%edx, %eax
	addq	$2, %rcx
	cmpq	%r13, %rcx
	jne	.LBB1_34
.LBB1_35:                               # %._crit_edge165
                                        #   in Loop: Header=BB1_31 Depth=1
	movslq	%r12d, %rax
	movl	48(%rsp,%rax,4), %edi
	movl	%eax, %ecx
	cmpq	%rcx, %rbp
	je	.LBB1_37
# BB#36:                                #   in Loop: Header=BB1_31 Depth=1
	movl	48(%rsp,%rbp,4), %ecx
	movl	%ecx, 48(%rsp,%rax,4)
	movl	%edi, 48(%rsp,%rbp,4)
.LBB1_37:                               #   in Loop: Header=BB1_31 Depth=1
	xorl	%eax, %eax
	callq	makemove
	movl	20(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %esi
	negl	%esi
	movl	40(%rsp), %edi          # 4-byte Reload
	callq	ab
	movl	%eax, %ebx
	movl	%ebx, %r14d
	negl	%r14d
	xorl	%eax, %eax
	callq	backmove
	movl	44(%rsp), %eax          # 4-byte Reload
	cmpl	%r14d, %eax
	jge	.LBB1_41
# BB#38:                                #   in Loop: Header=BB1_31 Depth=1
	cmpl	%r14d, %r15d
	jge	.LBB1_42
# BB#39:                                #   in Loop: Header=BB1_31 Depth=1
	cmpl	%r14d, 36(%rsp)         # 4-byte Folded Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	jle	.LBB1_61
# BB#40:                                #   in Loop: Header=BB1_31 Depth=1
	movl	%ebp, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	%r14d, 20(%rsp)         # 4-byte Spill
	jmp	.LBB1_44
	.p2align	4, 0x90
.LBB1_41:                               #   in Loop: Header=BB1_31 Depth=1
	movl	%eax, %r14d
	jmp	.LBB1_43
	.p2align	4, 0x90
.LBB1_42:                               #   in Loop: Header=BB1_31 Depth=1
	movl	%ebp, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
.LBB1_43:                               #   in Loop: Header=BB1_31 Depth=1
	movq	24(%rsp), %r15          # 8-byte Reload
.LBB1_44:                               #   in Loop: Header=BB1_31 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	incq	%rbp
	cmpq	88(%rsp), %rbp          # 8-byte Folded Reload
	movl	%r14d, %ecx
	jl	.LBB1_31
.LBB1_45:                               # %.loopexit146
	movl	4(%rsp), %ebx           # 4-byte Reload
	testl	%ebx, %ebx
	movl	32(%rsp), %eax          # 4-byte Reload
	jle	.LBB1_53
# BB#46:                                # %.preheader
	movl	%ebx, %ebp
	testb	$1, %bpl
	jne	.LBB1_49
# BB#47:
	xorl	%ecx, %ecx
	cmpl	$1, %ebx
	jne	.LBB1_50
	jmp	.LBB1_52
.LBB1_48:
	movl	$-999999, %r14d         # imm = 0xFFF0BDC1
	jmp	.LBB1_54
.LBB1_49:
	movslq	48(%rsp), %rcx
	movl	height(,%rcx,4), %edx
	shll	$3, %edx
	orl	%ecx, %edx
	movslq	%edx, %rcx
	imulq	$224, %r15, %rdx
	decl	history(%rdx,%rcx,4)
	movl	$1, %ecx
	cmpl	$1, %ebx
	je	.LBB1_52
.LBB1_50:                               # %.preheader.new
	subq	%rcx, %rbp
	leaq	52(%rsp,%rcx,4), %rcx
	imulq	$224, %r15, %rdx
	.p2align	4, 0x90
.LBB1_51:                               # =>This Inner Loop Header: Depth=1
	movslq	-4(%rcx), %rsi
	movl	height(,%rsi,4), %edi
	shll	$3, %edi
	orl	%esi, %edi
	movslq	%edi, %rsi
	decl	history(%rdx,%rsi,4)
	movslq	(%rcx), %rsi
	movl	height(,%rsi,4), %edi
	shll	$3, %edi
	orl	%esi, %edi
	movslq	%edi, %rsi
	decl	history(%rdx,%rsi,4)
	addq	$8, %rcx
	addq	$-2, %rbp
	jne	.LBB1_51
.LBB1_52:                               # %._crit_edge
	movslq	%ebx, %rcx
	movslq	48(%rsp,%rcx,4), %rdx
	movl	height(,%rdx,4), %ecx
	shll	$3, %ecx
	orl	%edx, %ecx
	movslq	%ecx, %rdx
	imulq	$224, %r15, %rcx
	addl	%ebx, history(%rcx,%rdx,4)
.LBB1_53:                               # %.loopexit146.thread
	movq	80(%rsp), %rdx          # 8-byte Reload
.LBB1_54:                               # %.loopexit146.thread
	movq	posed(%rip), %rcx
	subq	%rdx, %rcx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_55:                               # =>This Inner Loop Header: Depth=1
	sarq	%rcx
	incl	%ebx
	testq	%rcx, %rcx
	jne	.LBB1_55
# BB#56:
	cmpl	$-128, %eax
	je	.LBB1_58
# BB#57:
	sarl	$5, %eax
	xorl	%ecx, %ecx
	addl	%r14d, %eax
	cmovel	%ecx, %r14d
	xorl	%eax, %eax
	movl	%r14d, %edi
	movl	%ebx, %esi
	callq	transrestore
	cmpl	$8, plycnt(%rip)
	je	.LBB1_59
	jmp	.LBB1_60
.LBB1_58:
	xorl	%eax, %eax
	movl	%r14d, %edi
	movl	%ebx, %esi
	callq	transtore
	cmpl	$8, plycnt(%rip)
	jne	.LBB1_60
.LBB1_59:
	xorl	%eax, %eax
	callq	printMoves
	movslq	%r14d, %rax
	movsbl	.L.str.1+4(%rax), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	printf
.LBB1_60:                               # %.loopexit
	movl	%r14d, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_61:
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	-1(%rax), %eax
	testl	%ebx, %ebx
	movl	$1, %ecx
	cmovnel	%r14d, %ecx
	cmpl	%eax, %ebp
	cmovll	%ecx, %r14d
	movl	%ebp, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jmp	.LBB1_45
.LBB1_62:
	movl	$1, %r14d
	jmp	.LBB1_60
.Lfunc_end1:
	.size	ab, .Lfunc_end1-ab
	.cfi_endproc

	.globl	solve
	.p2align	4, 0x90
	.type	solve,@function
solve:                                  # @solve
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	$0, nodes(%rip)
	movq	$1, msecs(%rip)
	movl	plycnt(%rip), %ebx
	notl	%ebx
	andl	$1, %ebx
	movl	$1, %ebp
	movl	%ebx, %ecx
	shll	%cl, %ebp
	movl	height+4(%rip), %esi
	cmpl	$6, %esi
	jg	.LBB2_3
# BB#1:
	movl	$1, %edi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	wins
	testl	%eax, %eax
	jne	.LBB2_5
# BB#2:
	movslq	columns+4(%rip), %rax
	cmpl	%ebp, colthr(,%rax,4)
	je	.LBB2_5
.LBB2_3:                                # %.backedge
	movl	height+8(%rip), %esi
	cmpl	$7, %esi
	jge	.LBB2_11
# BB#4:
	movl	$2, %edi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	wins
	testl	%eax, %eax
	jne	.LBB2_5
# BB#10:
	movslq	columns+8(%rip), %rax
	cmpl	%ebp, colthr(,%rax,4)
	je	.LBB2_5
.LBB2_11:                               # %.backedge.1
	movl	height+12(%rip), %esi
	cmpl	$6, %esi
	jg	.LBB2_14
# BB#12:
	movl	$3, %edi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	wins
	testl	%eax, %eax
	jne	.LBB2_5
# BB#13:
	movslq	columns+12(%rip), %rax
	cmpl	%ebp, colthr(,%rax,4)
	je	.LBB2_5
.LBB2_14:                               # %.backedge.2
	movl	height+16(%rip), %esi
	cmpl	$6, %esi
	jg	.LBB2_17
# BB#15:
	movl	$4, %edi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	wins
	testl	%eax, %eax
	jne	.LBB2_5
# BB#16:
	movslq	columns+16(%rip), %rax
	cmpl	%ebp, colthr(,%rax,4)
	je	.LBB2_5
.LBB2_17:                               # %.backedge.3
	movl	height+20(%rip), %esi
	cmpl	$6, %esi
	jg	.LBB2_20
# BB#18:
	movl	$5, %edi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	wins
	testl	%eax, %eax
	jne	.LBB2_5
# BB#19:
	movslq	columns+20(%rip), %rax
	cmpl	%ebp, colthr(,%rax,4)
	je	.LBB2_5
.LBB2_20:                               # %.backedge.4
	movl	height+24(%rip), %esi
	cmpl	$6, %esi
	jg	.LBB2_23
# BB#21:
	movl	$6, %edi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	wins
	testl	%eax, %eax
	jne	.LBB2_5
# BB#22:
	movslq	columns+24(%rip), %rax
	cmpl	%ebp, colthr(,%rax,4)
	je	.LBB2_5
.LBB2_23:                               # %.backedge.5
	movl	height+28(%rip), %esi
	cmpl	$6, %esi
	jg	.LBB2_26
# BB#24:
	movl	$7, %edi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	wins
	testl	%eax, %eax
	jne	.LBB2_5
# BB#25:
	movslq	columns+28(%rip), %rax
	cmpl	%ebp, colthr(,%rax,4)
	jne	.LBB2_26
.LBB2_5:
	testl	%ebx, %ebx
	movl	$64, %ecx
	movl	$-64, %eax
	cmovnel	%ecx, %eax
.LBB2_9:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB2_26:                               # %.backedge.6
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	callq	transpose
	cmpl	$-128, %eax
	je	.LBB2_6
# BB#27:                                # %.backedge.6
	movl	%eax, %ecx
	andl	$32, %ecx
	je	.LBB2_9
.LBB2_6:
	xorl	%eax, %eax
	callq	millisecs
	decq	%rax
	movq	%rax, msecs(%rip)
	movl	$-2, %edi
	movl	$2, %esi
	callq	ab
	movl	%eax, %ebx
	movq	posed(%rip), %rax
	.p2align	4, 0x90
.LBB2_7:                                # =>This Inner Loop Header: Depth=1
	sarq	%rax
	incl	%ebp
	testq	%rax, %rax
	jne	.LBB2_7
# BB#8:
	xorl	%eax, %eax
	callq	millisecs
	subq	msecs(%rip), %rax
	movq	%rax, msecs(%rip)
	shll	$5, %ebx
	orl	%ebx, %ebp
	movl	%ebp, %eax
	jmp	.LBB2_9
.Lfunc_end2:
	.size	solve, .Lfunc_end2-solve
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 16
	xorl	%eax, %eax
	callq	trans_init
	movl	$.L.str.2, %edi
	callq	puts
	movl	$.L.str.3, %edi
	movl	$1050011, %esi          # imm = 0x10059B
	movl	$8, %edx
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB3_1
.LBB3_11:                               #   in Loop: Header=BB3_1 Depth=1
	movl	plycnt(%rip), %esi
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	xorl	%eax, %eax
	callq	printMoves
	movl	$.L.str.5, %edi
	callq	puts
	xorl	%eax, %eax
	callq	emptyTT
	callq	solve
	movl	%eax, %ecx
	sarl	$5, %eax
	movslq	%eax, %rsi
	movsbl	.L.str.1+4(%rsi), %edx
	andl	$31, %ecx
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	movq	nodes(%rip), %rsi
	movq	msecs(%rip), %rdx
	cvtsi2sdq	%rsi, %xmm0
	cvtsi2sdq	%rdx, %xmm1
	divsd	%xmm1, %xmm0
	movl	$.L.str.7, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	callq	htstat
	.p2align	4, 0x90
.LBB3_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_2 Depth 2
	xorl	%eax, %eax
	callq	reset
	jmp	.LBB3_2
	.p2align	4, 0x90
.LBB3_7:                                # %.backedge
                                        #   in Loop: Header=BB3_2 Depth=2
	xorl	%eax, %eax
	movl	%ecx, %edi
	callq	makemove
.LBB3_2:                                #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	stdin(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ecx
	cmpl	$-1, %ecx
	je	.LBB3_12
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=2
	leal	-49(%rcx), %eax
	cmpl	$6, %eax
	ja	.LBB3_5
# BB#4:                                 #   in Loop: Header=BB3_2 Depth=2
	addl	$-48, %ecx
	jmp	.LBB3_7
	.p2align	4, 0x90
.LBB3_5:                                #   in Loop: Header=BB3_2 Depth=2
	leal	-65(%rcx), %eax
	cmpl	$6, %eax
	ja	.LBB3_8
# BB#6:                                 #   in Loop: Header=BB3_2 Depth=2
	addl	$-64, %ecx
	jmp	.LBB3_7
	.p2align	4, 0x90
.LBB3_8:                                #   in Loop: Header=BB3_2 Depth=2
	leal	-97(%rcx), %eax
	cmpl	$6, %eax
	ja	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_2 Depth=2
	addl	$-96, %ecx
	jmp	.LBB3_7
.LBB3_10:                               #   in Loop: Header=BB3_2 Depth=2
	cmpl	$10, %ecx
	jne	.LBB3_2
	jmp	.LBB3_11
.LBB3_12:                               # %._crit_edge
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.cfi_endproc

	.type	history,@object         # @history
	.data
	.globl	history
	.p2align	4
history:
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	5                       # 0x5
	.long	7                       # 0x7
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	4294967295              # 0xffffffff
	.long	2                       # 0x2
	.long	5                       # 0x5
	.long	8                       # 0x8
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	5                       # 0x5
	.long	2                       # 0x2
	.long	4294967295              # 0xffffffff
	.long	2                       # 0x2
	.long	5                       # 0x5
	.long	8                       # 0x8
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	5                       # 0x5
	.long	2                       # 0x2
	.long	4294967295              # 0xffffffff
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	5                       # 0x5
	.long	7                       # 0x7
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	5                       # 0x5
	.long	7                       # 0x7
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	4294967295              # 0xffffffff
	.long	2                       # 0x2
	.long	5                       # 0x5
	.long	8                       # 0x8
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	5                       # 0x5
	.long	2                       # 0x2
	.long	4294967295              # 0xffffffff
	.long	2                       # 0x2
	.long	5                       # 0x5
	.long	8                       # 0x8
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	5                       # 0x5
	.long	2                       # 0x2
	.long	4294967295              # 0xffffffff
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	5                       # 0x5
	.long	7                       # 0x7
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.size	history, 448

	.type	nodes,@object           # @nodes
	.comm	nodes,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%c%d\n"
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"##-<=>+#"
	.size	.L.str.1, 9

	.type	msecs,@object           # @msecs
	.comm	msecs,8,8
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Fhourstones 2.0"
	.size	.L.str.2, 16

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Using %d transposition table entries with %d probes.\n"
	.size	.L.str.3, 54

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Solving %d-ply position after "
	.size	.L.str.4, 31

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	" . . ."
	.size	.L.str.5, 7

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"score = %d (%c)  work = %d\n"
	.size	.L.str.6, 28

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%lu pos / %lu msec = %.1f Kpos/sec\n"
	.size	.L.str.7, 36


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
