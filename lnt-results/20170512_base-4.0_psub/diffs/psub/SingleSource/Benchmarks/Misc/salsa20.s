	.text
	.file	"salsa20.bc"
	.globl	salsa20
	.p2align	4, 0x90
	.type	salsa20,@function
salsa20:                                # @salsa20
	.cfi_startproc
# BB#0:                                 # %.preheader14
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, -88(%rsp)         # 8-byte Spill
	movups	(%rsi), %xmm0
	movups	16(%rsi), %xmm1
	movups	32(%rsi), %xmm2
	movq	%rsi, -96(%rsp)         # 8-byte Spill
	movups	48(%rsi), %xmm3
	movaps	%xmm3, -32(%rsp)
	movaps	%xmm2, -48(%rsp)
	movaps	%xmm1, -64(%rsp)
	movaps	%xmm0, -80(%rsp)
	movl	-80(%rsp), %ebx
	movl	-76(%rsp), %r10d
	movl	-32(%rsp), %r9d
	movl	-64(%rsp), %edi
	movl	-48(%rsp), %r8d
	movl	-60(%rsp), %r15d
	movl	-44(%rsp), %esi
	movl	-28(%rsp), %r13d
	movl	-40(%rsp), %r14d
	movl	-56(%rsp), %r11d
	movl	-24(%rsp), %ebp
	movl	-72(%rsp), %edx
	movl	-20(%rsp), %r12d
	movl	-36(%rsp), %ecx
	movq	%rcx, -120(%rsp)        # 8-byte Spill
	movl	-68(%rsp), %ecx
	movq	%rcx, -112(%rsp)        # 8-byte Spill
	movl	$-20, -100(%rsp)        # 4-byte Folded Spill
	movl	-52(%rsp), %ecx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, -128(%rsp)        # 8-byte Spill
	leal	(%r9,%rbx), %eax
	roll	$7, %eax
	xorl	%edi, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	leal	(%rax,%rbx), %edi
	roll	$9, %edi
	xorl	%r8d, %edi
	movq	%rdi, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leal	(%rcx,%rax), %edi
	roll	$13, %edi
	xorl	%r9d, %edi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leal	(%rdi,%rcx), %r9d
	roll	$18, %r9d
	xorl	%ebx, %r9d
	leal	(%r10,%r15), %ecx
	roll	$7, %ecx
	xorl	%esi, %ecx
	leal	(%rcx,%r15), %esi
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	roll	$9, %esi
	xorl	%r13d, %esi
	leal	(%rsi,%rcx), %ebx
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	roll	$13, %ebx
	xorl	%r10d, %ebx
	leal	(%rbx,%rsi), %r8d
	roll	$18, %r8d
	xorl	%r15d, %r8d
	leal	(%r11,%r14), %eax
	roll	$7, %eax
	xorl	%ebp, %eax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	leal	(%rax,%r14), %esi
	roll	$9, %esi
	xorl	%edx, %esi
	leal	(%rsi,%rax), %r15d
	roll	$13, %r15d
	xorl	%r11d, %r15d
	leal	(%r15,%rsi), %r13d
	roll	$18, %r13d
	xorl	%r14d, %r13d
	movq	-120(%rsp), %rcx        # 8-byte Reload
	leal	(%rcx,%r12), %ebp
	roll	$7, %ebp
	xorl	-112(%rsp), %ebp        # 4-byte Folded Reload
	leal	(%rbp,%r12), %edi
	roll	$9, %edi
	xorl	-128(%rsp), %edi        # 4-byte Folded Reload
	leal	(%rdi,%rbp), %r11d
	roll	$13, %r11d
	xorl	%ecx, %r11d
	leal	(%r11,%rdi), %eax
	roll	$18, %eax
	xorl	%r12d, %eax
	movq	%rax, %r12
	leal	(%rbp,%r9), %r10d
	roll	$7, %r10d
	xorl	%ebx, %r10d
	leal	(%r10,%r9), %edx
	roll	$9, %edx
	xorl	%esi, %edx
	leal	(%rdx,%r10), %ecx
	roll	$13, %ecx
	xorl	%ebp, %ecx
	movq	%rcx, -112(%rsp)        # 8-byte Spill
	leal	(%rcx,%rdx), %ebx
	roll	$18, %ebx
	xorl	%r9d, %ebx
	movq	(%rsp), %rsi            # 8-byte Reload
	leal	(%rsi,%r8), %eax
	roll	$7, %eax
	xorl	%r15d, %eax
	leal	(%rax,%r8), %ecx
	roll	$9, %ecx
	xorl	%edi, %ecx
	leal	(%rcx,%rax), %edi
	roll	$13, %edi
	xorl	%esi, %edi
	leal	(%rdi,%rcx), %r15d
	roll	$18, %r15d
	xorl	%r8d, %r15d
	movq	8(%rsp), %rbp           # 8-byte Reload
	leal	(%rbp,%r13), %esi
	roll	$7, %esi
	xorl	%r11d, %esi
	leal	(%rsi,%r13), %r8d
	roll	$9, %r8d
	xorl	24(%rsp), %r8d          # 4-byte Folded Reload
	movq	%rsi, -120(%rsp)        # 8-byte Spill
	leal	(%r8,%rsi), %esi
	roll	$13, %esi
	xorl	%ebp, %esi
	leal	(%rsi,%r8), %r14d
	roll	$18, %r14d
	xorl	%r13d, %r14d
	movq	-8(%rsp), %r11          # 8-byte Reload
	movq	%r12, -128(%rsp)        # 8-byte Spill
	leal	(%r11,%r12), %r9d
	roll	$7, %r9d
	xorl	32(%rsp), %r9d          # 4-byte Folded Reload
	leal	(%r9,%r12), %r13d
	roll	$9, %r13d
	xorl	16(%rsp), %r13d         # 4-byte Folded Reload
	leal	(%r13,%r9), %ebp
	roll	$13, %ebp
	xorl	%r11d, %ebp
	movq	%rax, %r11
	leal	(%rbp,%r13), %r12d
	roll	$18, %r12d
	xorl	-128(%rsp), %r12d       # 4-byte Folded Reload
	addl	$2, -100(%rsp)          # 4-byte Folded Spill
	jne	.LBB0_1
# BB#2:                                 # %.preheader
	movq	-96(%rsp), %rax         # 8-byte Reload
	addl	(%rax), %ebx
	movq	-88(%rsp), %rax         # 8-byte Reload
	movl	%ebx, (%rax)
	movq	-96(%rsp), %rax         # 8-byte Reload
	addl	4(%rax), %r10d
	movq	-88(%rsp), %rbx         # 8-byte Reload
	movl	%r10d, 4(%rbx)
	addl	8(%rax), %edx
	movq	%rbx, %r10
	movl	%edx, 8(%r10)
	movq	-112(%rsp), %rbx        # 8-byte Reload
	movq	%rax, %rdx
	addl	12(%rdx), %ebx
	movl	%ebx, 12(%r10)
	addl	16(%rdx), %edi
	movl	%edi, 16(%r10)
	addl	20(%rdx), %r15d
	movl	%r15d, 20(%r10)
	addl	24(%rdx), %r11d
	movl	%r11d, 24(%r10)
	addl	28(%rdx), %ecx
	movl	%ecx, 28(%r10)
	addl	32(%rdx), %r8d
	movl	%r8d, 32(%r10)
	addl	36(%rdx), %esi
	movl	%esi, 36(%r10)
	addl	40(%rdx), %r14d
	movl	%r14d, 40(%r10)
	movq	-120(%rsp), %rax        # 8-byte Reload
	addl	44(%rdx), %eax
	movl	%eax, 44(%r10)
	addl	48(%rdx), %r9d
	movl	%r9d, 48(%r10)
	addl	52(%rdx), %r13d
	movl	%r13d, 52(%r10)
	addl	56(%rdx), %ebp
	movl	%ebp, 56(%r10)
	addl	60(%rdx), %r12d
	movl	%r12d, 60(%r10)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	salsa20, .Lfunc_end0-salsa20
	.cfi_endproc

	.globl	salsa
	.p2align	4, 0x90
	.type	salsa,@function
salsa:                                  # @salsa
	.cfi_startproc
# BB#0:
	movl	ptr(%rip), %eax
	testl	%eax, %eax
	jne	.LBB1_2
# BB#1:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movl	$outbuf, %edi
	movl	$STATE, %esi
	callq	salsa20
	incl	STATE(%rip)
	movl	ptr(%rip), %eax
	addq	$8, %rsp
.LBB1_2:
	incl	%eax
	andl	$15, %eax
	movl	%eax, ptr(%rip)
	movl	outbuf(,%rax,4), %eax
	retq
.Lfunc_end1:
	.size	salsa, .Lfunc_end1-salsa
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	856036637               # 0x3306151d
	.long	856036637               # 0x3306151d
	.long	856036633               # 0x33061519
	.long	856036633               # 0x33061519
.LCPI2_1:
	.long	856036637               # 0x3306151d
	.long	856036637               # 0x3306151d
	.long	856036625               # 0x33061511
	.long	856036625               # 0x33061511
.LCPI2_2:
	.long	856036637               # 0x3306151d
	.long	856036637               # 0x3306151d
	.long	856036065               # 0x330612e1
	.long	856036065               # 0x330612e1
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:                                 # %.preheader.preheader21
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 16
.Lcfi15:
	.cfi_offset %rbx, -16
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [856036637,856036637,856036633,856036633]
	movaps	%xmm0, STATE(%rip)
	movaps	.LCPI2_1(%rip), %xmm1   # xmm1 = [856036637,856036637,856036625,856036625]
	movaps	%xmm1, STATE+16(%rip)
	movaps	%xmm0, STATE+32(%rip)
	movaps	.LCPI2_2(%rip), %xmm0   # xmm0 = [856036637,856036637,856036065,856036065]
	movaps	%xmm0, STATE+48(%rip)
	movl	$537919489, %ebx        # imm = 0x20100001
	movl	ptr(%rip), %eax
	.p2align	4, 0x90
.LBB2_1:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	testl	%eax, %eax
	jne	.LBB2_3
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	movl	$outbuf, %edi
	movl	$STATE, %esi
	callq	salsa20
	incl	STATE(%rip)
	movl	ptr(%rip), %eax
.LBB2_3:                                # %salsa.exit
                                        #   in Loop: Header=BB2_1 Depth=1
	incl	%eax
	andl	$15, %eax
	movl	%eax, ptr(%rip)
	decl	%ebx
	jne	.LBB2_1
# BB#4:
	movl	outbuf(,%rax,4), %ebx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movl	$.L.str.1, %edi
	movl	$-162172867, %esi       # imm = 0xF655703D
	xorl	%eax, %eax
	callq	printf
	xorl	%eax, %eax
	cmpl	$-162172867, %ebx       # imm = 0xF655703D
	setne	%al
	popq	%rbx
	retq
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc

	.type	ptr,@object             # @ptr
	.bss
	.globl	ptr
	.p2align	2
ptr:
	.long	0                       # 0x0
	.size	ptr, 4

	.type	outbuf,@object          # @outbuf
	.comm	outbuf,64,16
	.type	STATE,@object           # @STATE
	.comm	STATE,64,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"got:       %x\n"
	.size	.L.str, 15

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"should be: %x\n"
	.size	.L.str.1, 15


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
