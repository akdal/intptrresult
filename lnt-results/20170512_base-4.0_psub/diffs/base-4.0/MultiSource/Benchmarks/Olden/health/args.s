	.text
	.file	"args.bc"
	.globl	dealwithargs
	.p2align	4, 0x90
	.type	dealwithargs,@function
dealwithargs:                           # @dealwithargs
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	cmpl	$3, %edi
	jg	.LBB0_2
# BB#1:
	movl	$3, max_level(%rip)
	movq	$15, max_time(%rip)
	movl	$3, %esi
	movl	$15, %edx
	movl	$4, %ecx
	jmp	.LBB0_3
.LBB0_2:
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, max_level(%rip)
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, max_time(%rip)
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rcx
	movl	max_level(%rip), %esi
	movl	max_time(%rip), %edx
.LBB0_3:
	movq	%rcx, seed(%rip)
	movl	$.L.str, %edi
	xorl	%eax, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	popq	%rbx
	jmp	printf                  # TAILCALL
.Lfunc_end0:
	.size	dealwithargs, .Lfunc_end0-dealwithargs
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"max_level=%d  max_time=%d  seed=%d \n"
	.size	.L.str, 37


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
