	.text
	.file	"mv-search.bc"
	.globl	SetMotionVectorPredictor
	.p2align	4, 0x90
	.type	SetMotionVectorPredictor,@function
SetMotionVectorPredictor:               # @SetMotionVectorPredictor
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 240
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movl	240(%rsp), %ebx
	leal	(,%rbp,4), %r14d
	leal	(,%rbx,4), %edx
	movq	img(%rip), %rax
	movl	12(%rax), %r12d
	leal	-1(,%rbp,4), %r13d
	leaq	96(%rsp), %rcx
	movl	%r12d, %edi
	movl	%r13d, %esi
	movl	%edx, 128(%rsp)         # 4-byte Spill
	callq	getLuma4x4Neighbour
	leal	-1(,%rbx,4), %ebx
	leaq	72(%rsp), %rcx
	movl	%r12d, %edi
	movl	%r14d, %esi
	movl	%ebx, %edx
	callq	getLuma4x4Neighbour
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movl	248(%rsp), %eax
	leal	(%rax,%rbp,4), %r15d
	leaq	32(%rsp), %rcx
	movl	%r12d, %edi
	movl	%r15d, %esi
	movl	%ebx, %edx
	callq	getLuma4x4Neighbour
	leaq	160(%rsp), %rcx
	movl	%r12d, %edi
	movl	%r13d, %esi
	movl	%ebx, %edx
	callq	getLuma4x4Neighbour
	movl	240(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB0_7
# BB#1:
	cmpl	$7, %r14d
	jg	.LBB0_4
# BB#2:
	cmpl	$8, 128(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_5
# BB#3:
	movl	248(%rsp), %eax
	cmpl	$16, %eax
	je	.LBB0_6
	jmp	.LBB0_7
.LBB0_4:
	cmpl	$16, %r15d
	je	.LBB0_6
	jmp	.LBB0_7
.LBB0_5:
	cmpl	$8, %r15d
	jne	.LBB0_7
.LBB0_6:
	movl	$0, 32(%rsp)
.LBB0_7:
	cmpl	$0, 32(%rsp)
	jne	.LBB0_9
# BB#8:
	movq	176(%rsp), %rax
	movq	%rax, 48(%rsp)
	movups	160(%rsp), %xmm0
	movaps	%xmm0, 32(%rsp)
.LBB0_9:
	movq	img(%rip), %rsi
	movl	15268(%rsi), %r14d
	testl	%r14d, %r14d
	movl	248(%rsp), %r9d
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%rsi, 128(%rsp)         # 8-byte Spill
	je	.LBB0_17
# BB#10:
	movq	14224(%rsi), %rbx
	movslq	12(%rsi), %rax
	imulq	$536, %rax, %rax        # imm = 0x218
	cmpl	$0, 424(%rbx,%rax)
	movl	96(%rsp), %r11d
	movl	$-1, %r15d
	je	.LBB0_23
# BB#11:
	testl	%r11d, %r11d
	movl	$-1, %ebp
	je	.LBB0_13
# BB#12:
	movslq	100(%rsp), %rax
	imulq	$536, %rax, %rcx        # imm = 0x218
	movslq	116(%rsp), %rax
	movq	(%rdi,%rax,8), %rax
	movslq	112(%rsp), %rdx
	movsbl	(%rax,%rdx), %ebp
	cmpl	$0, 424(%rbx,%rcx)
	sete	%cl
	shll	%cl, %ebp
.LBB0_13:
	movl	72(%rsp), %r12d
	testl	%r12d, %r12d
	je	.LBB0_15
# BB#14:
	movslq	76(%rsp), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	movslq	92(%rsp), %rdx
	movq	(%rdi,%rdx,8), %rdx
	movslq	88(%rsp), %rsi
	movsbl	(%rdx,%rsi), %r15d
	cmpl	$0, 424(%rbx,%rcx)
	sete	%cl
	shll	%cl, %r15d
.LBB0_15:
	movl	32(%rsp), %r10d
	testl	%r10d, %r10d
	je	.LBB0_29
# BB#16:
	movslq	36(%rsp), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	movslq	52(%rsp), %rdx
	movq	(%rdi,%rdx,8), %rdx
	movslq	48(%rsp), %rsi
	movsbl	(%rdx,%rsi), %esi
	cmpl	$0, 424(%rbx,%rcx)
	sete	%cl
	shll	%cl, %esi
	jmp	.LBB0_30
.LBB0_17:
	movl	96(%rsp), %r11d
	movl	$-1, %r15d
	testl	%r11d, %r11d
	movl	$-1, %ebp
	je	.LBB0_19
# BB#18:
	movslq	116(%rsp), %rax
	movq	(%rdi,%rax,8), %rax
	movslq	112(%rsp), %rcx
	movsbl	(%rax,%rcx), %ebp
.LBB0_19:
	movl	72(%rsp), %r12d
	testl	%r12d, %r12d
	je	.LBB0_21
# BB#20:
	movslq	92(%rsp), %rcx
	movq	(%rdi,%rcx,8), %rcx
	movslq	88(%rsp), %rdx
	movsbl	(%rcx,%rdx), %r15d
.LBB0_21:
	movl	32(%rsp), %r10d
	testl	%r10d, %r10d
	je	.LBB0_29
# BB#22:
	movslq	52(%rsp), %rcx
	movq	(%rdi,%rcx,8), %rcx
	movslq	48(%rsp), %rdx
	movsbl	(%rcx,%rdx), %esi
	jmp	.LBB0_30
.LBB0_23:
	testl	%r11d, %r11d
	movl	$-1, %ebp
	je	.LBB0_25
# BB#24:
	movslq	100(%rsp), %rax
	imulq	$536, %rax, %rax        # imm = 0x218
	cmpl	$0, 424(%rbx,%rax)
	setne	%cl
	movslq	116(%rsp), %rax
	movq	(%rdi,%rax,8), %rax
	movslq	112(%rsp), %rdx
	movsbl	(%rax,%rdx), %ebp
	sarl	%cl, %ebp
.LBB0_25:
	movl	72(%rsp), %r12d
	testl	%r12d, %r12d
	je	.LBB0_27
# BB#26:
	movslq	76(%rsp), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	cmpl	$0, 424(%rbx,%rcx)
	setne	%cl
	movslq	92(%rsp), %rdx
	movq	(%rdi,%rdx,8), %rdx
	movslq	88(%rsp), %rsi
	movsbl	(%rdx,%rsi), %r15d
	sarl	%cl, %r15d
.LBB0_27:
	movl	32(%rsp), %r10d
	testl	%r10d, %r10d
	je	.LBB0_29
# BB#28:
	movslq	36(%rsp), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	cmpl	$0, 424(%rbx,%rcx)
	setne	%cl
	movslq	52(%rsp), %rdx
	movq	(%rdi,%rdx,8), %rdx
	movslq	48(%rsp), %rsi
	movsbl	(%rdx,%rsi), %esi
	sarl	%cl, %esi
	jmp	.LBB0_30
.LBB0_29:
	movl	$-1, %esi
	xorl	%r10d, %r10d
.LBB0_30:
	movl	16(%rsp), %eax          # 4-byte Reload
	movswl	%ax, %ecx
	cmpl	%ecx, %ebp
	setne	%dl
	cmpl	%ecx, %r15d
	je	.LBB0_33
# BB#31:
	testb	%dl, %dl
	jne	.LBB0_33
# BB#32:
	movl	$1, %r13d
	cmpl	%ecx, %esi
	jne	.LBB0_36
.LBB0_33:
	cmpl	%ecx, %r15d
	sete	%bl
	cmpl	%ecx, %ebp
	setne	%dl
	sete	%r8b
	andb	%bl, %dl
	cmpl	%ecx, %esi
	setne	%bl
	movl	%edx, %eax
	andb	%bl, %al
	movzbl	%al, %r13d
	addl	%r13d, %r13d
	andb	%bl, %dl
	jne	.LBB0_36
# BB#34:
	testb	%r8b, %r8b
	jne	.LBB0_36
# BB#35:
	xorl	%eax, %eax
	cmpl	%ecx, %esi
	sete	%al
	xorl	%edx, %edx
	cmpl	%ecx, %r15d
	leal	(%rax,%rax,2), %r13d
	cmovel	%edx, %r13d
.LBB0_36:
	movl	256(%rsp), %edx
	cmpl	$8, %r9d
	jne	.LBB0_40
# BB#37:
	cmpl	$16, %edx
	jne	.LBB0_40
# BB#38:
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	je	.LBB0_43
# BB#39:
	movl	%r14d, %r9d
	cmpl	%ecx, %esi
	movl	$3, %eax
	jmp	.LBB0_46
.LBB0_40:
	cmpl	$16, %r9d
	jne	.LBB0_44
# BB#41:
	cmpl	$8, %edx
	jne	.LBB0_44
# BB#42:
	movl	240(%rsp), %eax
	testl	%eax, %eax
	je	.LBB0_45
.LBB0_43:
	movl	%r14d, %r9d
	cmpl	%ecx, %ebp
	movl	$1, %eax
	jmp	.LBB0_46
.LBB0_44:
	movl	%r14d, %r9d
	jmp	.LBB0_47
.LBB0_45:
	movl	%r14d, %r9d
	cmpl	%ecx, %r15d
	movl	$2, %eax
.LBB0_46:
	cmovel	%eax, %r13d
.LBB0_47:
	movq	24(%rsp), %rbx          # 8-byte Reload
	xorl	%esi, %esi
	movl	%r11d, %ecx
	testl	%r11d, %r11d
	movslq	52(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movslq	48(%rsp), %rbp
	movslq	92(%rsp), %r15
	movslq	88(%rsp), %rdx
	movslq	116(%rsp), %r11
	movslq	112(%rsp), %r8
	movslq	100(%rsp), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movslq	36(%rsp), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movslq	76(%rsp), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	$0, %edi
	je	.LBB0_49
# BB#48:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%r8,8), %rax
	movswl	(%rax), %edi
.LBB0_49:
	testl	%r12d, %r12d
	je	.LBB0_51
# BB#50:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%rdx,8), %rax
	movswl	(%rax), %esi
.LBB0_51:
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	xorl	%r14d, %r14d
	testl	%r10d, %r10d
	movl	$0, %edx
	je	.LBB0_53
# BB#52:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	(%rax,%rbp,8), %rax
	movswl	(%rax), %edx
.LBB0_53:
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movl	%r10d, %eax
	orl	%r12d, %eax
	movl	%r13d, %ebp
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	cmpl	$3, %r13d
	ja	.LBB0_61
# BB#54:
	movq	64(%rsp), %rbp          # 8-byte Reload
	jmpq	*.LJTI0_0(,%rbp,8)
.LBB0_55:
	testl	%eax, %eax
	je	.LBB0_60
# BB#56:
	leal	(%rsi,%rdi), %r14d
	addl	%edx, %r14d
	cmpl	%edx, %esi
	movl	%edx, %ebp
	cmovlel	%esi, %ebp
	cmovgel	%esi, %edx
	cmpl	%ebp, %edi
	cmovlel	%edi, %ebp
	subl	%ebp, %r14d
	movq	24(%rsp), %rbx          # 8-byte Reload
	cmpl	%edx, %edi
	cmovgel	%edi, %edx
	subl	%edx, %r14d
	jmp	.LBB0_61
.LBB0_60:
	movl	%edi, %r14d
	jmp	.LBB0_61
.LBB0_58:
	movl	%esi, %r14d
	jmp	.LBB0_61
.LBB0_59:
	movl	%edx, %r14d
.LBB0_61:
	testl	%r9d, %r9d
	movq	%rbx, %rbp
	movw	%r14w, (%rbp)
	je	.LBB0_72
# BB#62:
	movq	128(%rsp), %rdx         # 8-byte Reload
	movq	14224(%rdx), %r9
	movslq	12(%rdx), %rdx
	imulq	$536, %rdx, %rdx        # imm = 0x218
	cmpl	$0, 424(%r9,%rdx)
	je	.LBB0_78
# BB#63:
	xorl	%esi, %esi
	testl	%ecx, %ecx
	movl	$0, %edx
	je	.LBB0_66
# BB#64:
	imulq	$536, 136(%rsp), %rcx   # 8-byte Folded Reload
                                        # imm = 0x218
	cmpl	$0, 424(%r9,%rcx)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%r8,8), %rcx
	movswl	2(%rcx), %edx
	jne	.LBB0_66
# BB#65:                                # %select.false.sink
	movl	%edx, %ecx
	shrl	$31, %ecx
	addl	%edx, %ecx
	sarl	%ecx
	movl	%ecx, %edx
.LBB0_66:
	testl	%r12d, %r12d
	je	.LBB0_69
# BB#67:
	imulq	$536, 144(%rsp), %rcx   # 8-byte Folded Reload
                                        # imm = 0x218
	cmpl	$0, 424(%r9,%rcx)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%r15,8), %rcx
	movq	120(%rsp), %rsi         # 8-byte Reload
	movq	(%rcx,%rsi,8), %rcx
	movswl	2(%rcx), %esi
	jne	.LBB0_69
# BB#68:                                # %select.false.sink183
	movl	%esi, %ecx
	shrl	$31, %ecx
	addl	%esi, %ecx
	sarl	%ecx
	movl	%ecx, %esi
.LBB0_69:
	testl	%r10d, %r10d
	je	.LBB0_85
# BB#70:
	imulq	$536, 152(%rsp), %rcx   # 8-byte Folded Reload
                                        # imm = 0x218
	cmpl	$0, 424(%r9,%rcx)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rcx,%rdi,8), %rcx
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	(%rcx,%rdi,8), %rcx
	movswl	2(%rcx), %edi
	jne	.LBB0_87
# BB#71:                                # %select.false.sink189
	movl	%edi, %ecx
	shrl	$31, %ecx
	addl	%edi, %ecx
	sarl	%ecx
	movl	%ecx, %edi
	cmpl	$3, %r13d
	jbe	.LBB0_88
	jmp	.LBB0_95
.LBB0_72:
	xorl	%esi, %esi
	testl	%ecx, %ecx
	movl	$0, %edx
	je	.LBB0_74
# BB#73:
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%r8,8), %rcx
	movswl	2(%rcx), %edx
.LBB0_74:
	testl	%r12d, %r12d
	je	.LBB0_76
# BB#75:
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%r15,8), %rcx
	movq	120(%rsp), %rsi         # 8-byte Reload
	movq	(%rcx,%rsi,8), %rcx
	movswl	2(%rcx), %esi
.LBB0_76:
	testl	%r10d, %r10d
	je	.LBB0_85
# BB#77:
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rcx,%rdi,8), %rcx
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	(%rcx,%rdi,8), %rcx
	movswl	2(%rcx), %edi
	cmpl	$3, %r13d
	jbe	.LBB0_88
	jmp	.LBB0_95
.LBB0_85:
	xorl	%edi, %edi
	cmpl	$3, %r13d
	jbe	.LBB0_88
	jmp	.LBB0_95
.LBB0_78:
	xorl	%esi, %esi
	testl	%ecx, %ecx
	movl	$0, %edx
	je	.LBB0_80
# BB#79:
	imulq	$536, 136(%rsp), %rcx   # 8-byte Folded Reload
                                        # imm = 0x218
	cmpl	$0, 424(%r9,%rcx)
	setne	%cl
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rdx,%r11,8), %rdx
	movq	(%rdx,%r8,8), %rdx
	movswl	2(%rdx), %edx
	shll	%cl, %edx
.LBB0_80:
	testl	%r12d, %r12d
	je	.LBB0_82
# BB#81:
	imulq	$536, 144(%rsp), %rcx   # 8-byte Folded Reload
                                        # imm = 0x218
	cmpl	$0, 424(%r9,%rcx)
	setne	%cl
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsi,%r15,8), %rsi
	movq	120(%rsp), %rdi         # 8-byte Reload
	movq	(%rsi,%rdi,8), %rsi
	movswl	2(%rsi), %esi
	shll	%cl, %esi
.LBB0_82:
	testl	%r10d, %r10d
	je	.LBB0_86
# BB#83:
	imulq	$536, 152(%rsp), %rcx   # 8-byte Folded Reload
                                        # imm = 0x218
	cmpl	$0, 424(%r9,%rcx)
	setne	%cl
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	(%rdi,%rbx,8), %rdi
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	(%rdi,%rbx,8), %rdi
	movswl	2(%rdi), %edi
	shll	%cl, %edi
	cmpl	$3, %r13d
	jbe	.LBB0_88
	jmp	.LBB0_95
.LBB0_86:
	xorl	%edi, %edi
.LBB0_87:
	cmpl	$3, %r13d
	ja	.LBB0_95
.LBB0_88:
	movq	64(%rsp), %rcx          # 8-byte Reload
	jmpq	*.LJTI0_1(,%rcx,8)
.LBB0_89:
	testl	%eax, %eax
	je	.LBB0_94
# BB#90:
	leal	(%rsi,%rdx), %r14d
	addl	%edi, %r14d
	cmpl	%edi, %esi
	movl	%edi, %eax
	cmovlel	%esi, %eax
	cmovgel	%esi, %edi
	cmpl	%eax, %edx
	cmovlel	%edx, %eax
	subl	%eax, %r14d
	cmpl	%edi, %edx
	cmovgel	%edx, %edi
	subl	%edi, %r14d
	jmp	.LBB0_95
.LBB0_94:
	movl	%edx, %r14d
	jmp	.LBB0_95
.LBB0_92:
	movl	%esi, %r14d
	jmp	.LBB0_95
.LBB0_93:
	movl	%edi, %r14d
.LBB0_95:
	movw	%r14w, 2(%rbp)
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	SetMotionVectorPredictor, .Lfunc_end0-SetMotionVectorPredictor
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_55
	.quad	.LBB0_60
	.quad	.LBB0_58
	.quad	.LBB0_59
.LJTI0_1:
	.quad	.LBB0_89
	.quad	.LBB0_94
	.quad	.LBB0_92
	.quad	.LBB0_93

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4604418534313441775     # double 0.69314718055994529
.LCPI1_1:
	.quad	4457293557087583675     # double 1.0E-10
	.text
	.globl	Init_Motion_Search_Module
	.p2align	4, 0x90
	.type	Init_Motion_Search_Module,@function
Init_Motion_Search_Module:              # @Init_Motion_Search_Module
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 176
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	input(%rip), %rax
	movl	28(%rax), %ebx
	leal	1(%rbx,%rbx), %eax
	imull	%eax, %eax
	cmpl	$9, %eax
	movl	$9, %r14d
	cmoval	%eax, %r14d
	movq	img(%rip), %rax
	movl	32(%rax), %eax
	incl	%eax
	cmpl	$15, %eax
	movl	$16, %ecx
	cmovgl	%eax, %ecx
	cvtsi2sdl	%ecx, %xmm0
	callq	log
	divsd	.LCPI1_0(%rip), %xmm0
	addsd	.LCPI1_1(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %r12d
	leal	1(%r12), %ecx
	movl	$1, %ebp
	movl	$1, %r13d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r13d
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	leal	13(,%rbx,8), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	callq	log
	divsd	.LCPI1_0(%rip), %xmm0
	addsd	.LCPI1_1(%rip), %xmm0
	callq	ceil
	cvttsd2si	%xmm0, %ebx
	leal	3(%rbx,%rbx), %r15d
	movl	%r15d, %ecx
	shrl	%ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	decl	%ebp
	movl	%ebp, max_mvd(%rip)
	movq	img(%rip), %rax
	movl	15520(%rax), %ecx
	movl	15524(%rax), %eax
	cmpl	%eax, %ecx
	cmovgel	%ecx, %eax
	shll	$6, %eax
	addl	$64, %eax
	movl	%eax, byte_abs_range(%rip)
	movl	$2, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, spiral_search_x(%rip)
	testq	%rax, %rax
	jne	.LBB1_2
# BB#1:
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB1_2:
	movl	$2, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, spiral_search_y(%rip)
	testq	%rax, %rax
	jne	.LBB1_4
# BB#3:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB1_4:
	movl	$2, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, spiral_hpel_search_x(%rip)
	testq	%rax, %rax
	jne	.LBB1_6
# BB#5:
	movl	$.L.str.2, %edi
	callq	no_mem_exit
.LBB1_6:
	movl	$2, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, spiral_hpel_search_y(%rip)
	testq	%rax, %rax
	jne	.LBB1_8
# BB#7:
	movl	$.L.str.3, %edi
	callq	no_mem_exit
.LBB1_8:
	decl	%r13d
	movl	max_mvd(%rip), %eax
	leal	1(%rax,%rax), %eax
	movslq	%eax, %rdi
	movl	$4, %esi
	callq	calloc
	movq	%rax, mvbits(%rip)
	testq	%rax, %rax
	jne	.LBB1_10
# BB#9:
	movl	$.L.str.4, %edi
	callq	no_mem_exit
.LBB1_10:
	movslq	%r13d, %rdi
	movl	$4, %esi
	callq	calloc
	movq	%rax, refbits(%rip)
	testq	%rax, %rax
	jne	.LBB1_12
# BB#11:
	movl	$.L.str.5, %edi
	callq	no_mem_exit
.LBB1_12:
	movslq	byte_abs_range(%rip), %rdi
	movl	$4, %esi
	callq	calloc
	movq	%rax, byte_abs(%rip)
	testq	%rax, %rax
	jne	.LBB1_14
# BB#13:
	movl	$.L.str.6, %edi
	callq	no_mem_exit
.LBB1_14:
	leal	1(%r12,%r12), %r12d
	movq	img(%rip), %rax
	movl	32(%rax), %ecx
	movl	$motion_cost, %edi
	movl	$8, %esi
	movl	$2, %edx
	movl	$4, %r8d
	callq	get_mem4Dint
	movq	mvbits(%rip), %rax
	movslq	max_mvd(%rip), %rcx
	leaq	(%rax,%rcx,4), %rdx
	movq	%rdx, mvbits(%rip)
	movl	byte_abs_range(%rip), %edx
	movl	%edx, %esi
	shrl	$31, %esi
	addl	%edx, %esi
	sarl	%esi
	movq	byte_abs(%rip), %rdx
	movslq	%esi, %rsi
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	leaq	(%rdx,%rsi,4), %r13
	movq	%r13, byte_abs(%rip)
	movl	$1, (%rax,%rcx,4)
	testl	%ebx, %ebx
	js	.LBB1_24
# BB#15:                                # %.lr.ph156
	movq	mvbits(%rip), %rbp
	leaq	12(%rbp), %r10
	movl	$3, %edi
	.p2align	4, 0x90
.LBB1_16:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_19 Depth 2
                                        #     Child Loop BB1_22 Depth 2
	movl	%edi, %ecx
	shrl	%ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movl	%esi, %eax
	sarl	%eax
	cmpl	%esi, %eax
	jge	.LBB1_23
# BB#17:                                # %.lr.ph152.preheader
                                        #   in Loop: Header=BB1_16 Depth=1
	movslq	%eax, %rbx
	movslq	%esi, %rcx
	subl	%eax, %esi
	leaq	-1(%rcx), %rax
	subq	%rbx, %rax
	andq	$3, %rsi
	je	.LBB1_20
# BB#18:                                # %.lr.ph152.prol.preheader
                                        #   in Loop: Header=BB1_16 Depth=1
	leaq	(,%rbx,4), %r8
	movq	%rbp, %rdx
	subq	%r8, %rdx
	negq	%rsi
	.p2align	4, 0x90
.LBB1_19:                               # %.lr.ph152.prol
                                        #   Parent Loop BB1_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, (%rbp,%rbx,4)
	movl	%edi, (%rdx)
	incq	%rbx
	addq	$-4, %rdx
	incq	%rsi
	jne	.LBB1_19
.LBB1_20:                               # %.lr.ph152.prol.loopexit
                                        #   in Loop: Header=BB1_16 Depth=1
	cmpq	$3, %rax
	jb	.LBB1_23
# BB#21:                                # %.lr.ph152.preheader.new
                                        #   in Loop: Header=BB1_16 Depth=1
	subq	%rbx, %rcx
	leaq	(,%rbx,4), %rdx
	movq	%rbp, %rax
	subq	%rdx, %rax
	leaq	(%r10,%rbx,4), %rsi
	.p2align	4, 0x90
.LBB1_22:                               # %.lr.ph152
                                        #   Parent Loop BB1_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, -12(%rsi)
	movl	%edi, (%rax)
	movl	%edi, -8(%rsi)
	movl	%edi, -4(%rax)
	movl	%edi, -4(%rsi)
	movl	%edi, -8(%rax)
	movl	%edi, (%rsi)
	movl	%edi, -12(%rax)
	addq	$-16, %rax
	addq	$16, %rsi
	addq	$-4, %rcx
	jne	.LBB1_22
.LBB1_23:                               # %._crit_edge153
                                        #   in Loop: Header=BB1_16 Depth=1
	addl	$2, %edi
	cmpl	%r15d, %edi
	jle	.LBB1_16
.LBB1_24:                               # %._crit_edge157
	movq	refbits(%rip), %rax
	movl	$1, (%rax)
	cmpl	$3, %r12d
	jl	.LBB1_41
# BB#25:                                # %.lr.ph148
	movq	refbits(%rip), %r14
	leaq	16(%r14), %r11
	leaq	112(%r14), %r10
	movl	$3, %esi
	.p2align	4, 0x90
.LBB1_26:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_32 Depth 2
                                        #     Child Loop BB1_35 Depth 2
                                        #     Child Loop BB1_39 Depth 2
	movl	%esi, %ecx
	shrl	%ecx
	incl	%ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	decl	%eax
	movl	%eax, %ecx
	sarl	%ecx
	cmpl	%eax, %ecx
	jge	.LBB1_40
# BB#27:                                # %.lr.ph144.preheader
                                        #   in Loop: Header=BB1_26 Depth=1
	movslq	%ecx, %rdx
	movslq	%eax, %rcx
	movq	%rcx, %rbp
	subq	%rdx, %rbp
	cmpq	$8, %rbp
	jb	.LBB1_38
# BB#28:                                # %min.iters.checked
                                        #   in Loop: Header=BB1_26 Depth=1
	movq	%rbp, %r9
	andq	$-8, %r9
	movq	%rbp, %rbx
	andq	$-8, %rbx
	je	.LBB1_38
# BB#29:                                # %vector.ph
                                        #   in Loop: Header=BB1_26 Depth=1
	movd	%esi, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	-8(%rbx), %r15
	movl	%r15d, %eax
	shrl	$3, %eax
	incl	%eax
	andq	$3, %rax
	je	.LBB1_30
# BB#31:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB1_26 Depth=1
	leaq	(%r11,%rdx,4), %rdi
	negq	%rax
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB1_32:                               # %vector.body.prol
                                        #   Parent Loop BB1_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -16(%rdi,%r8,4)
	movdqu	%xmm0, (%rdi,%r8,4)
	addq	$8, %r8
	incq	%rax
	jne	.LBB1_32
	jmp	.LBB1_33
.LBB1_30:                               #   in Loop: Header=BB1_26 Depth=1
	xorl	%r8d, %r8d
.LBB1_33:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB1_26 Depth=1
	cmpq	$24, %r15
	jb	.LBB1_36
# BB#34:                                # %vector.ph.new
                                        #   in Loop: Header=BB1_26 Depth=1
	movq	%rbx, %rax
	subq	%r8, %rax
	addq	%rdx, %r8
	leaq	(%r10,%r8,4), %rdi
	.p2align	4, 0x90
.LBB1_35:                               # %vector.body
                                        #   Parent Loop BB1_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -112(%rdi)
	movdqu	%xmm0, -96(%rdi)
	movdqu	%xmm0, -80(%rdi)
	movdqu	%xmm0, -64(%rdi)
	movdqu	%xmm0, -48(%rdi)
	movdqu	%xmm0, -32(%rdi)
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	subq	$-128, %rdi
	addq	$-32, %rax
	jne	.LBB1_35
.LBB1_36:                               # %middle.block
                                        #   in Loop: Header=BB1_26 Depth=1
	cmpq	%rbx, %rbp
	je	.LBB1_40
# BB#37:                                #   in Loop: Header=BB1_26 Depth=1
	addq	%r9, %rdx
	.p2align	4, 0x90
.LBB1_38:                               # %.lr.ph144.preheader183
                                        #   in Loop: Header=BB1_26 Depth=1
	leaq	(%r14,%rdx,4), %rax
	subq	%rdx, %rcx
	.p2align	4, 0x90
.LBB1_39:                               # %.lr.ph144
                                        #   Parent Loop BB1_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, (%rax)
	addq	$4, %rax
	decq	%rcx
	jne	.LBB1_39
.LBB1_40:                               # %._crit_edge145
                                        #   in Loop: Header=BB1_26 Depth=1
	addl	$2, %esi
	cmpl	%r12d, %esi
	jle	.LBB1_26
.LBB1_41:                               # %._crit_edge149
	movl	$0, (%r13)
	cmpl	$4, byte_abs_range(%rip)
	jl	.LBB1_44
# BB#42:                                # %.lr.ph140.preheader
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	-4(%rax,%rcx,4), %rax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB1_43:                               # %.lr.ph140
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, (%rax)
	movl	%ecx, (%r13,%rcx,4)
	incq	%rcx
	movl	byte_abs_range(%rip), %edx
	movl	%edx, %esi
	shrl	$31, %esi
	addl	%edx, %esi
	sarl	%esi
	movslq	%esi, %rdx
	addq	$-4, %rax
	cmpq	%rdx, %rcx
	jl	.LBB1_43
.LBB1_44:                               # %._crit_edge141
	movq	spiral_search_y(%rip), %rax
	movw	$0, (%rax)
	movq	spiral_search_x(%rip), %rax
	movw	$0, (%rax)
	movq	spiral_hpel_search_y(%rip), %rax
	movw	$0, (%rax)
	movq	spiral_hpel_search_x(%rip), %rax
	movw	$0, (%rax)
	movq	32(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	movl	$1, %r15d
	cmovlel	%r15d, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	spiral_search_x(%rip), %rax
	movq	spiral_search_y(%rip), %rcx
	movq	spiral_hpel_search_x(%rip), %rdx
	movq	spiral_hpel_search_y(%rip), %rsi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	leaq	10(%rsi), %rsi
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	leaq	10(%rdx), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	10(%rcx), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	10(%rax), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movw	$2, %ax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	$2, 4(%rsp)             # 4-byte Folded Spill
	movl	$1, %r12d
	movl	$2, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	$6, %ecx
	movl	$0, %ebp
	movl	$2, %r8d
	.p2align	4, 0x90
.LBB1_45:                               # %.prol.loopexit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_47 Depth 2
                                        #     Child Loop BB1_50 Depth 2
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movl	%r15d, %r9d
	negl	%r9d
	movl	$1, %r10d
	subl	%r15d, %r10d
	leal	(%r9,%r9), %r11d
	leal	(%r15,%r15), %r14d
	movq	%r12, 104(%rsp)         # 8-byte Spill
	movslq	%r12d, %rsi
	movq	16(%rsp), %rcx          # 8-byte Reload
	movw	%r10w, (%rcx,%rsi,2)
	movq	96(%rsp), %rdx          # 8-byte Reload
	movw	%r9w, (%rdx,%rsi,2)
	leal	(%r10,%r10), %eax
	movq	88(%rsp), %rdi          # 8-byte Reload
	movw	%ax, (%rdi,%rsi,2)
	movq	80(%rsp), %rbx          # 8-byte Reload
	movw	%r11w, (%rbx,%rsi,2)
	movw	%r10w, 2(%rcx,%rsi,2)
	movw	%r15w, 2(%rdx,%rsi,2)
	movw	%ax, 2(%rdi,%rsi,2)
	movw	%r14w, 2(%rbx,%rsi,2)
	movl	%ebp, 24(%rsp)          # 4-byte Spill
	testl	$2147483647, %ebp       # imm = 0x7FFFFFFF
	je	.LBB1_48
# BB#46:                                #   in Loop: Header=BB1_45 Depth=1
	movl	$2, %eax
	subl	%r15d, %eax
	movl	12(%rsp), %ecx          # 4-byte Reload
	movw	%cx, %bp
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_47:                               #   Parent Loop BB1_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%ax, -6(%rbx,%rsi,2)
	movw	%r9w, -6(%rcx,%rsi,2)
	movw	%bp, -6(%rdx,%rsi,2)
	movw	%r11w, -6(%rdi,%rsi,2)
	movw	%ax, -4(%rbx,%rsi,2)
	movw	%r15w, -4(%rcx,%rsi,2)
	movw	%bp, -4(%rdx,%rsi,2)
	movw	%r14w, -4(%rdi,%rsi,2)
	leal	1(%rax), %r12d
	movw	%r12w, -2(%rbx,%rsi,2)
	movw	%r9w, -2(%rcx,%rsi,2)
	leal	2(%rbp), %r13d
	movw	%r13w, -2(%rdx,%rsi,2)
	movw	%r11w, -2(%rdi,%rsi,2)
	movw	%r12w, (%rbx,%rsi,2)
	movw	%r15w, (%rcx,%rsi,2)
	movw	%r13w, (%rdx,%rsi,2)
	movw	%r14w, (%rdi,%rsi,2)
	addl	$2, %eax
	addq	$8, %rdi
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$8, %rbx
	leal	4(%rbp), %ebp
	cmpl	%r15d, %eax
	jne	.LBB1_47
.LBB1_48:                               # %.prol.loopexit185
                                        #   in Loop: Header=BB1_45 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	104(%rsp), %rdi         # 8-byte Reload
	leal	(%rax,%rdi), %eax
	movslq	%eax, %r12
	movq	16(%rsp), %rax          # 8-byte Reload
	movw	%r9w, (%rax,%r12,2)
	movq	96(%rsp), %rcx          # 8-byte Reload
	movw	%r9w, (%rcx,%r12,2)
	movq	88(%rsp), %rdx          # 8-byte Reload
	movw	%r11w, (%rdx,%r12,2)
	movq	80(%rsp), %rsi          # 8-byte Reload
	movw	%r11w, (%rsi,%r12,2)
	movw	%r15w, 2(%rax,%r12,2)
	movw	%r9w, 2(%rcx,%r12,2)
	movw	%r14w, 2(%rdx,%r12,2)
	movw	%r11w, 2(%rsi,%r12,2)
	movl	24(%rsp), %eax          # 4-byte Reload
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	cmpl	$2147483647, %eax       # imm = 0x7FFFFFFF
	je	.LBB1_51
# BB#49:                                # %.lr.ph134.new
                                        #   in Loop: Header=BB1_45 Depth=1
	addl	4(%rsp), %edi           # 4-byte Folded Reload
	movslq	%edi, %rax
	movl	8(%rsp), %ecx           # 4-byte Reload
	movw	%cx, %si
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_50:                               #   Parent Loop BB1_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%r9w, -6(%rbx,%rax,2)
	movw	%r10w, -6(%rcx,%rax,2)
	movw	%r11w, -6(%rdx,%rax,2)
	movw	%si, -6(%rdi,%rax,2)
	movw	%r15w, -4(%rbx,%rax,2)
	movw	%r10w, -4(%rcx,%rax,2)
	movw	%r14w, -4(%rdx,%rax,2)
	movw	%si, -4(%rdi,%rax,2)
	leal	1(%r10), %r13d
	movw	%r9w, -2(%rbx,%rax,2)
	movw	%r13w, -2(%rcx,%rax,2)
	movw	%r11w, -2(%rdx,%rax,2)
	leal	2(%rsi), %ebp
	movw	%bp, -2(%rdi,%rax,2)
	movw	%r15w, (%rbx,%rax,2)
	movw	%r13w, (%rcx,%rax,2)
	movw	%r14w, (%rdx,%rax,2)
	movw	%bp, (%rdi,%rax,2)
	addl	$2, %r10d
	addq	$8, %rdi
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$8, %rbx
	leal	4(%rsi), %esi
	cmpl	%r8d, %r10d
	jne	.LBB1_50
.LBB1_51:                               # %._crit_edge
                                        #   in Loop: Header=BB1_45 Depth=1
	movq	112(%rsp), %rcx         # 8-byte Reload
	addq	%rcx, %r12
	addq	$4, 72(%rsp)            # 8-byte Folded Spill
	addq	$4, %rcx
	incl	%r8d
	movl	24(%rsp), %ebp          # 4-byte Reload
	incl	%ebp
	addl	$-2, 12(%rsp)           # 4-byte Folded Spill
	addl	$4, 4(%rsp)             # 4-byte Folded Spill
	addl	$-2, 8(%rsp)            # 4-byte Folded Spill
	cmpl	32(%rsp), %r15d         # 4-byte Folded Reload
	leal	1(%r15), %eax
	movl	%eax, %r15d
	jl	.LBB1_45
# BB#52:
	movq	input(%rip), %rax
	cmpl	$1, 5776(%rax)
	jne	.LBB1_54
# BB#53:                                # %.thread
	movl	$0, start_me_refinement_hp(%rip)
	xorl	%ecx, %ecx
	jmp	.LBB1_55
.LBB1_54:
	movl	5784(%rax), %ecx
	xorl	%edx, %edx
	cmpl	%ecx, 5780(%rax)
	sete	%dl
	movl	%edx, start_me_refinement_hp(%rip)
	cmpl	5788(%rax), %ecx
	sete	%cl
.LBB1_55:
	movzbl	%cl, %ecx
	movl	%ecx, start_me_refinement_qp(%rip)
	movl	5780(%rax), %ecx
	cmpl	$1, %ecx
	je	.LBB1_56
# BB#57:
	testl	%ecx, %ecx
	jne	.LBB1_59
# BB#58:
	movl	$computeBiPredSAD2, %ecx
	movl	$computeBiPredSAD1, %edx
	movl	$computeSADWP, %esi
	movl	$computeSAD, %edi
	jmp	.LBB1_60
.LBB1_56:
	movl	$computeBiPredSSE2, %ecx
	movl	$computeBiPredSSE1, %edx
	movl	$computeSSEWP, %esi
	movl	$computeSSE, %edi
	jmp	.LBB1_60
.LBB1_59:
	movl	$computeBiPredSATD2, %ecx
	movl	$computeBiPredSATD1, %edx
	movl	$computeSATDWP, %esi
	movl	$computeSATD, %edi
.LBB1_60:
	movq	%rdi, computeUniPred(%rip)
	movq	%rsi, computeUniPred+24(%rip)
	movq	%rdx, computeBiPred1(%rip)
	movq	%rcx, computeBiPred2(%rip)
	movl	5784(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB1_64
# BB#61:
	cmpl	$1, %ecx
	jne	.LBB1_65
# BB#62:
	movl	$computeBiPredSSE2, %ecx
	movl	$computeBiPredSSE1, %edx
	movl	$computeSSEWP, %esi
	movl	$computeSSE, %edi
	jmp	.LBB1_66
.LBB1_64:
	movl	$computeBiPredSAD2, %ecx
	movl	$computeBiPredSAD1, %edx
	movl	$computeSADWP, %esi
	movl	$computeSAD, %edi
	jmp	.LBB1_66
.LBB1_65:
	movl	$computeBiPredSATD2, %ecx
	movl	$computeBiPredSATD1, %edx
	movl	$computeSATDWP, %esi
	movl	$computeSATD, %edi
.LBB1_66:
	movq	%rdi, computeUniPred+8(%rip)
	movq	%rsi, computeUniPred+32(%rip)
	movq	%rdx, computeBiPred1+8(%rip)
	movq	%rcx, computeBiPred2+8(%rip)
	movl	5788(%rax), %ecx
	cmpl	$1, %ecx
	je	.LBB1_67
# BB#68:
	testl	%ecx, %ecx
	jne	.LBB1_70
# BB#69:
	movl	$computeBiPredSAD2, %ecx
	movl	$computeBiPredSAD1, %edx
	movl	$computeSADWP, %esi
	movl	$computeSAD, %edi
	jmp	.LBB1_71
.LBB1_67:
	movl	$computeBiPredSSE2, %ecx
	movl	$computeBiPredSSE1, %edx
	movl	$computeSSEWP, %esi
	movl	$computeSSE, %edi
	jmp	.LBB1_71
.LBB1_70:
	movl	$computeBiPredSATD2, %ecx
	movl	$computeBiPredSATD1, %edx
	movl	$computeSATDWP, %esi
	movl	$computeSATD, %edi
.LBB1_71:
	movq	%rdi, computeUniPred+16(%rip)
	movq	%rsi, computeUniPred+40(%rip)
	movq	%rdx, computeBiPred1+16(%rip)
	movq	%rcx, computeBiPred2+16(%rip)
	movl	$UMVLine4X, %ecx
	movd	%rcx, %xmm0
	movl	$FastLine4X, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, get_line(%rip)
	movl	$UMVLine8X_chroma, %ecx
	movd	%rcx, %xmm0
	movl	$FastLine8X_chroma, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, get_crline(%rip)
	cmpl	$0, 5244(%rax)
	je	.LBB1_72
# BB#63:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_72:
	xorl	%eax, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	InitializeFastFullIntegerSearch # TAILCALL
.Lfunc_end1:
	.size	Init_Motion_Search_Module, .Lfunc_end1-Init_Motion_Search_Module
	.cfi_endproc

	.globl	Clear_Motion_Search_Module
	.p2align	4, 0x90
	.type	Clear_Motion_Search_Module,@function
Clear_Motion_Search_Module:             # @Clear_Motion_Search_Module
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 16
	movslq	max_mvd(%rip), %rax
	shlq	$2, %rax
	subq	%rax, mvbits(%rip)
	movl	byte_abs_range(%rip), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	shlq	$2, %rax
	subq	%rax, byte_abs(%rip)
	movq	spiral_search_x(%rip), %rdi
	callq	free
	movq	spiral_search_y(%rip), %rdi
	callq	free
	movq	spiral_hpel_search_x(%rip), %rdi
	callq	free
	movq	spiral_hpel_search_y(%rip), %rdi
	callq	free
	movq	mvbits(%rip), %rdi
	callq	free
	movq	refbits(%rip), %rdi
	callq	free
	movq	byte_abs(%rip), %rdi
	callq	free
	movq	motion_cost(%rip), %rdi
	movl	$8, %esi
	movl	$2, %edx
	callq	free_mem4Dint
	movq	input(%rip), %rax
	cmpl	$0, 5244(%rax)
	je	.LBB2_2
# BB#1:
	popq	%rax
	retq
.LBB2_2:
	popq	%rax
	jmp	ClearFastFullIntegerSearch # TAILCALL
.Lfunc_end2:
	.size	Clear_Motion_Search_Module, .Lfunc_end2-Clear_Motion_Search_Module
	.cfi_endproc

	.globl	BPredPartitionCost
	.p2align	4, 0x90
	.type	BPredPartitionCost,@function
BPredPartitionCost:                     # @BPredPartitionCost
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$1208, %rsp             # imm = 0x4B8
.Lcfi33:
	.cfi_def_cfa_offset 1264
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	movl	%edx, 64(%rsp)          # 4-byte Spill
	movq	input(%rip), %rdx
	movslq	%edi, %r10
	movl	72(%rdx,%r10,8), %eax
	cmpl	$9, %eax
	movl	$8, %ecx
	cmovgel	%ecx, %eax
	movl	%eax, 156(%rsp)         # 4-byte Spill
	movl	76(%rdx,%r10,8), %eax
	cmpl	$9, %eax
	cmovll	%eax, %ecx
	movl	%ecx, 152(%rsp)         # 4-byte Spill
	cmpl	$5, %r10d
	movl	$4, %eax
	movl	%edi, 16(%rsp)          # 4-byte Spill
	cmovll	%edi, %eax
	movslq	%eax, %r15
	movl	140(%rdx,%r15,8), %r14d
	movq	img(%rip), %rdi
	leaq	14400(%rdi), %rbp
	leaq	14392(%rdi), %rax
	movl	%r9d, 84(%rsp)          # 4-byte Spill
	testl	%r9d, %r9d
	cmovneq	%rax, %rbp
	movslq	%esi, %r9
	movq	%r15, %rax
	shlq	$4, %rax
	testl	%r14d, %r14d
	movl	136(%rdx,%r15,8), %esi
	movslq	PartitionMotionSearch.by0(%rax,%r9,4), %rbx
	movq	%r15, 128(%rsp)         # 8-byte Spill
	movq	%rsi, 120(%rsp)         # 8-byte Spill
	jle	.LBB3_7
# BB#1:                                 # %.lr.ph234
	addl	%ebx, %r14d
	testl	%esi, %esi
	jle	.LBB3_8
# BB#2:                                 # %.lr.ph234.split.us.preheader
	movl	%r8d, 88(%rsp)          # 4-byte Spill
	movslq	136(%rdx,%r10,8), %rcx
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	movslq	140(%rdx,%r10,8), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	(%rbp), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	14376(%rdi), %rdx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movq	%r9, 168(%rsp)          # 8-byte Spill
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movslq	PartitionMotionSearch.bx0(%rax,%r9,4), %rax
	movq	mvbits(%rip), %rdi
	movswq	64(%rsp), %r11          # 2-byte Folded Reload
	movswq	72(%rsp), %r9           # 2-byte Folded Reload
	movl	%r14d, 148(%rsp)        # 4-byte Spill
	movslq	%r14d, %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leal	(%rax,%rsi), %ebp
	movslq	%ebp, %r14
	xorl	%ebp, %ebp
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph234.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_4 Depth 2
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	movq	(%rax,%rsi,8), %r15
	movq	48(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_4:                                #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r13,8), %r12
	movq	(%r12), %r8
	movq	(%r8,%r11,8), %rax
	movq	(%rax,%r10,8), %rax
	movq	%rcx, %r8
	movswq	(%rax), %rcx
	movq	(%r15,%r13,8), %rsi
	movq	(%rsi), %rdx
	movq	(%rdx,%r11,8), %rdx
	movq	(%rdx,%r10,8), %rdx
	movswq	(%rdx), %rbx
	subq	%rbx, %rcx
	addl	(%rdi,%rcx,4), %ebp
	movq	8(%r12), %rcx
	movq	8(%rsi), %rsi
	movswq	2(%rax), %rax
	movswq	2(%rdx), %rdx
	subq	%rdx, %rax
	addl	(%rdi,%rax,4), %ebp
	movq	(%rcx,%r9,8), %rax
	movq	(%rax,%r10,8), %rax
	movswq	(%rax), %rcx
	movq	(%rsi,%r9,8), %rdx
	movq	(%rdx,%r10,8), %rdx
	movswq	(%rdx), %rsi
	subq	%rsi, %rcx
	addl	(%rdi,%rcx,4), %ebp
	movswq	2(%rax), %rax
	movswq	2(%rdx), %rcx
	subq	%rcx, %rax
	movq	%r8, %rcx
	addl	(%rdi,%rax,4), %ebp
	addq	%rcx, %r13
	cmpq	%r14, %r13
	jl	.LBB3_4
# BB#5:                                 # %._crit_edge229.us
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	112(%rsp), %rsi         # 8-byte Reload
	addq	8(%rsp), %rsi           # 8-byte Folded Reload
	cmpq	40(%rsp), %rsi          # 8-byte Folded Reload
	jl	.LBB3_3
# BB#6:
	movq	128(%rsp), %r15         # 8-byte Reload
	movq	120(%rsp), %rsi         # 8-byte Reload
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	136(%rsp), %rdx         # 8-byte Reload
	movl	88(%rsp), %r8d          # 4-byte Reload
	movl	148(%rsp), %r14d        # 4-byte Reload
	movq	168(%rsp), %r9          # 8-byte Reload
	movq	160(%rsp), %rax         # 8-byte Reload
	jmp	.LBB3_9
.LBB3_7:                                # %.._crit_edge235_crit_edge
	addl	%ebx, %r14d
.LBB3_8:
	xorl	%ebp, %ebp
.LBB3_9:                                # %._crit_edge235
	imull	%r8d, %ebp
	sarl	$16, %ebp
	cmpl	%r14d, %ebx
	jge	.LBB3_24
# BB#10:                                # %.lr.ph222
	movslq	PartitionMotionSearch.bx0(%rax,%r9,4), %rdx
	leal	(%rdx,%rsi), %eax
	cltq
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movslq	%r14d, %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	leaq	368(%rsp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leal	(,%rdx,4), %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	incq	%rdx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	testl	%esi, %esi
	jle	.LBB3_22
	.p2align	4, 0x90
.LBB3_12:                               # %.lr.ph
	movslq	196(%rdi), %r15
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	leal	(,%rbx,4), %eax
	movslq	%eax, %rbx
	addq	%rbx, %r15
	cmpl	$4, 16(%rsp)            # 4-byte Folded Reload
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	jle	.LBB3_16
# BB#13:                                # %.lr.ph.split.us.preheader
	movslq	%r15d, %r15
	movq	%rbx, %rax
	orq	$1, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rbx, %rax
	orq	$2, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rbx, %rax
	orq	$3, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	88(%rsp), %r13          # 8-byte Reload
	xorl	%r14d, %r14d
	jmp	.LBB3_15
	.p2align	4, 0x90
.LBB3_14:                               # %..lr.ph.split.us_crit_edge
                                        #   in Loop: Header=BB3_15 Depth=1
	addq	$4, %r14
	movq	img(%rip), %rdi
	incq	%r13
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB3_15:                               # %.preheader197.us
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14), %rbp
	movl	192(%rdi), %r12d
	addl	%ebp, %r12d
	movl	84(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
	movswl	64(%rsp), %r8d          # 2-byte Folded Reload
	movswl	72(%rsp), %r9d          # 2-byte Folded Reload
	movl	%ebp, %edi
	movl	%ebx, %esi
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	%edx, %ecx
	callq	LumaPrediction4x4Bi
	movq	imgY_org(%rip), %rsi
	movq	img(%rip), %rcx
	movslq	%r12d, %rdx
	movslq	%ebp, %rax
	movq	(%rsi,%r15,8), %rdi
	shlq	$5, %rbx
	leaq	12624(%rcx,%rbx), %rbp
	movq	(%rdi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	pxor	%xmm2, %xmm2
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rbp,%rax,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movq	56(%rsp), %rbx          # 8-byte Reload
	movdqa	%xmm0, -192(%rbx,%r14,4)
	movdqa	%xmm0, diff64(%rip)
	movq	8(%rsi,%r15,8), %rdi
	movq	48(%rsp), %rbp          # 8-byte Reload
	shlq	$5, %rbp
	leaq	12624(%rcx,%rbp), %rbp
	movq	(%rdi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rbp,%rax,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, -128(%rbx,%r14,4)
	movdqa	%xmm0, diff64+16(%rip)
	movq	16(%rsi,%r15,8), %rdi
	movq	40(%rsp), %rbp          # 8-byte Reload
	shlq	$5, %rbp
	leaq	12624(%rcx,%rbp), %rbp
	movq	(%rdi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rbp,%rax,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, -64(%rbx,%r14,4)
	movdqa	%xmm0, diff64+32(%rip)
	movq	24(%rsi,%r15,8), %rsi
	movq	32(%rsp), %rdi          # 8-byte Reload
	shlq	$5, %rdi
	leaq	12624(%rcx,%rdi), %rcx
	movq	(%rsi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rcx,%rax,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, (%rbx,%r14,4)
	movdqa	%xmm0, diff64+48(%rip)
	movl	$diff64, %edi
	callq	distortion4x4
	movl	%eax, %ebp
	addl	8(%rsp), %ebp           # 4-byte Folded Reload
	cmpq	112(%rsp), %r13         # 8-byte Folded Reload
	jl	.LBB3_14
	jmp	.LBB3_21
	.p2align	4, 0x90
.LBB3_16:                               # %.lr.ph.split.preheader
	movq	%rbx, %rax
	orq	$1, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rbx, %rax
	orq	$2, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	orq	$3, %rbx
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	%eax, %r14d
	movq	56(%rsp), %r12          # 8-byte Reload
	jmp	.LBB3_18
	.p2align	4, 0x90
.LBB3_17:                               # %..lr.ph.split_crit_edge
                                        #   in Loop: Header=BB3_18 Depth=1
	movq	img(%rip), %rdi
	addq	$16, %r12
	addl	$4, %r14d
	incq	%rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB3_18:                               # %.preheader197
                                        # =>This Inner Loop Header: Depth=1
	movl	192(%rdi), %ebx
	addl	%r14d, %ebx
	movl	84(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
	movswl	64(%rsp), %r8d          # 2-byte Folded Reload
	movswl	72(%rsp), %r9d          # 2-byte Folded Reload
	movl	%r14d, %edi
	movq	24(%rsp), %r13          # 8-byte Reload
	movl	%r13d, %esi
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	%edx, %ecx
	callq	LumaPrediction4x4Bi
	movq	imgY_org(%rip), %rsi
	movq	img(%rip), %rcx
	movslq	%r14d, %rax
	movslq	%ebx, %rdx
	movq	(%rsi,%r15,8), %rdi
	shlq	$5, %r13
	leaq	12624(%rcx,%r13), %rbx
	movq	(%rdi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	pxor	%xmm2, %xmm2
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rbx,%rax,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, -192(%r12)
	movdqa	%xmm0, diff64(%rip)
	movq	8(%rsi,%r15,8), %rdi
	movq	48(%rsp), %rbx          # 8-byte Reload
	shlq	$5, %rbx
	leaq	12624(%rcx,%rbx), %rbx
	movq	(%rdi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rbx,%rax,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, -128(%r12)
	movdqa	%xmm0, diff64+16(%rip)
	movq	16(%rsi,%r15,8), %rdi
	movq	40(%rsp), %rbx          # 8-byte Reload
	shlq	$5, %rbx
	leaq	12624(%rcx,%rbx), %rbx
	movq	(%rdi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rbx,%rax,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, -64(%r12)
	movdqa	%xmm0, diff64+32(%rip)
	movq	24(%rsi,%r15,8), %rsi
	movq	32(%rsp), %rdi          # 8-byte Reload
	shlq	$5, %rdi
	leaq	12624(%rcx,%rdi), %rcx
	movq	(%rsi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rcx,%rax,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, (%r12)
	movdqa	%xmm0, diff64+48(%rip)
	movq	input(%rip), %rax
	cmpl	$0, 5100(%rax)
	jne	.LBB3_20
# BB#19:                                #   in Loop: Header=BB3_18 Depth=1
	movl	$diff64, %edi
	callq	distortion4x4
	addl	%eax, %ebp
.LBB3_20:                               #   in Loop: Header=BB3_18 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpq	112(%rsp), %rax         # 8-byte Folded Reload
	jl	.LBB3_17
.LBB3_21:
	movq	128(%rsp), %r15         # 8-byte Reload
	movq	120(%rsp), %rsi         # 8-byte Reload
	movq	96(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB3_22
	.p2align	4, 0x90
.LBB3_11:                               # %._crit_edge214._crit_edge
                                        #   in Loop: Header=BB3_22 Depth=1
	movq	img(%rip), %rdi
	addq	$256, 56(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x100
	testl	%esi, %esi
	jg	.LBB3_12
.LBB3_22:                               # %._crit_edge214
                                        # =>This Inner Loop Header: Depth=1
	incq	%rbx
	cmpq	136(%rsp), %rbx         # 8-byte Folded Reload
	jl	.LBB3_11
# BB#23:                                # %._crit_edge223.loopexit
	movq	input(%rip), %rdx
.LBB3_24:                               # %._crit_edge223
	cmpl	$4, 16(%rsp)            # 4-byte Folded Reload
	jg	.LBB3_32
# BB#25:                                # %._crit_edge223
	movl	5100(%rdx), %eax
	testl	%eax, %eax
	je	.LBB3_32
# BB#26:                                # %.preheader196
	cmpl	$0, 76(%rdx,%r15,8)
	jle	.LBB3_32
# BB#27:                                # %.preheader195.preheader
	movl	152(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movslq	%eax, %rsi
	movl	156(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movslq	%eax, %r12
	leaq	-1(%rsi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	shlq	$6, %rsi
	addq	$-64, %rsi
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	leaq	-4(,%r12,4), %rbx
	decq	%r12
	leaq	176(%rsp), %r13
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_28:                               # %.preheader195
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_30 Depth 2
	cmpl	$0, 72(%rdx,%r15,8)
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	jle	.LBB3_31
# BB#29:                                # %.preheader
                                        #   in Loop: Header=BB3_28 Depth=1
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_30:                               #   Parent Loop BB3_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r13), %xmm0
	movups	16(%r13), %xmm1
	movaps	%xmm1, diff64+16(%rip)
	movaps	%xmm0, diff64(%rip)
	movups	64(%r13), %xmm0
	movups	80(%r13), %xmm1
	movaps	%xmm1, diff64+48(%rip)
	movaps	%xmm0, diff64+32(%rip)
	movups	128(%r13), %xmm0
	movups	144(%r13), %xmm1
	movaps	%xmm1, diff64+80(%rip)
	movaps	%xmm0, diff64+64(%rip)
	movups	192(%r13), %xmm0
	movups	208(%r13), %xmm1
	movaps	%xmm1, diff64+112(%rip)
	movaps	%xmm0, diff64+96(%rip)
	movups	256(%r13), %xmm0
	movups	272(%r13), %xmm1
	movaps	%xmm1, diff64+144(%rip)
	movaps	%xmm0, diff64+128(%rip)
	movups	320(%r13), %xmm0
	movups	336(%r13), %xmm1
	movaps	%xmm1, diff64+176(%rip)
	movaps	%xmm0, diff64+160(%rip)
	movups	384(%r13), %xmm0
	movups	400(%r13), %xmm1
	movaps	%xmm1, diff64+208(%rip)
	movaps	%xmm0, diff64+192(%rip)
	movdqu	448(%r13), %xmm0
	movdqu	464(%r13), %xmm1
	movdqa	%xmm1, diff64+240(%rip)
	movdqa	%xmm0, diff64+224(%rip)
	movl	$diff64, %edi
	callq	distortion8x8
	addl	%eax, %ebp
	addq	%r12, %r14
	movq	input(%rip), %rdx
	movslq	72(%rdx,%r15,8), %rax
	addq	%rbx, %r13
	cmpq	%rax, %r14
	jl	.LBB3_30
.LBB3_31:                               # %._crit_edge
                                        #   in Loop: Header=BB3_28 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	addq	64(%rsp), %rdi          # 8-byte Folded Reload
	movslq	76(%rdx,%r15,8), %rax
	movq	24(%rsp), %r13          # 8-byte Reload
	addq	72(%rsp), %r13          # 8-byte Folded Reload
	cmpq	%rax, %rdi
	jl	.LBB3_28
.LBB3_32:                               # %.loopexit
	movl	%ebp, %eax
	addq	$1208, %rsp             # imm = 0x4B8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	BPredPartitionCost, .Lfunc_end3-BPredPartitionCost
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.quad	2                       # 0x2
	.quad	2                       # 0x2
.LCPI4_1:
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_2:
	.quad	4615063718147915776     # double 3.5
	.text
	.globl	BlockMotionSearch
	.p2align	4, 0x90
	.type	BlockMotionSearch,@function
BlockMotionSearch:                      # @BlockMotionSearch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 272
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	%r9d, 100(%rsp)
	movslq	%ecx, %rbx
	movl	%ecx, %r11d
	sarl	$2, %r11d
	movq	input(%rip), %r9
	movslq	%r8d, %r13
	movslq	72(%r9,%r13,8), %r10
	movq	img(%rip), %r14
	movslq	192(%r14), %rax
	movslq	%edx, %rbp
	sarl	$2, %edx
	addq	%rax, %rbp
	movslq	196(%r14), %rax
	addq	%rax, %rbx
	movl	chroma_shift_x(%rip), %eax
	addl	$-2, %eax
	movq	%rbp, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill>
	movl	%eax, %ecx
	sarl	%cl, %ebp
	movl	%ebp, 152(%rsp)         # 4-byte Spill
	movl	chroma_shift_y(%rip), %ebp
	addl	$-2, %ebp
	movq	%rbx, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill>
	movl	%ebp, %ecx
	sarl	%cl, %ebx
	movl	%ebx, 144(%rsp)         # 4-byte Spill
	movq	%r10, %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
                                        # kill: %R10D<def> %R10D<kill> %R10<kill>
	movl	%eax, %ecx
	sarl	%cl, %r10d
	movl	76(%r9,%r13,8), %ebx
	movl	%ebx, %eax
	movl	%ebp, %ecx
	sarl	%cl, %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movslq	%r11d, %rbp
	movq	14376(%r14), %rax
	movq	(%rax,%rbp,8), %rax
	movq	%rdx, %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	%esi, %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	(%rax,%rdx,8), %rax
	movl	%edi, 16(%rsp)          # 4-byte Spill
	movslq	%edi, %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	(%rax,%rdx,8), %rax
	movq	(%rax,%r13,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	cmpl	$0, 15268(%r14)
	movq	14384(%r14), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movl	%r10d, 200(%rsp)        # 4-byte Spill
	je	.LBB4_3
# BB#1:
	movq	14224(%r14), %r10
	movslq	12(%r14), %rax
	imulq	$536, %rax, %rdx        # imm = 0x218
	cmpl	$0, 424(%r10,%rdx)
	je	.LBB4_3
# BB#2:
	andl	$1, %eax
	leal	2(%rax,%rax), %eax
	movq	%rax, 184(%rsp)         # 8-byte Spill
.LBB4_3:
	cmpl	$3, 5244(%r9)
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	movq	%rsi, 192(%rsp)         # 8-byte Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	%r11, 48(%rsp)          # 8-byte Spill
	jne	.LBB4_5
# BB#4:
	movq	EPZSDistortion(%rip), %rax
	movq	184(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%rsi), %ecx
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	-8(%rax,%r13,8), %rax
	jmp	.LBB4_6
.LBB4_5:
	xorl	%eax, %eax
.LBB4_6:
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movl	$BlockMotionSearch.tstruct1, %edi
	callq	ftime
	testl	%ebx, %ebx
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	jle	.LBB4_14
# BB#7:                                 # %.lr.ph506
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rax), %r14
	leaq	-1(%rbx), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	%rbx, %r12
	andq	$3, %r12
	je	.LBB4_10
# BB#8:                                 # %.prol.preheader554
	movl	$orig_pic, %ebp
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(,%rax,8), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	xorl	%ebx, %ebx
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rax), %r15
	.p2align	4, 0x90
.LBB4_9:                                # =>This Inner Loop Header: Depth=1
	movq	imgY_org(%rip), %rax
	addq	160(%rsp), %rax         # 8-byte Folded Reload
	movq	(%rax,%rbx,8), %rsi
	addq	%r15, %rsi
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	incq	%rbx
	addq	%r14, %rbp
	cmpq	%rbx, %r12
	jne	.LBB4_9
	jmp	.LBB4_11
.LBB4_10:
	xorl	%ebx, %ebx
	movl	$orig_pic, %ebp
.LBB4_11:                               # %.prol.loopexit555
	cmpq	$3, 168(%rsp)           # 8-byte Folded Reload
	jb	.LBB4_14
# BB#12:                                # %.lr.ph506.new
	movq	40(%rsp), %r15          # 8-byte Reload
	shlq	$3, %r15
	movq	24(%rsp), %r12          # 8-byte Reload
	addq	%r12, %r12
	.p2align	4, 0x90
.LBB4_13:                               # =>This Inner Loop Header: Depth=1
	movq	imgY_org(%rip), %rax
	addq	%r15, %rax
	movq	(%rax,%rbx,8), %rsi
	addq	%r12, %rsi
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	imgY_org(%rip), %rax
	addq	%r15, %rax
	movq	8(%rax,%rbx,8), %rsi
	addq	%r12, %rsi
	addq	%r14, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	imgY_org(%rip), %rax
	addq	%r15, %rax
	movq	16(%rax,%rbx,8), %rsi
	addq	%r12, %rsi
	addq	%r14, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	imgY_org(%rip), %rax
	addq	%r15, %rax
	movq	24(%rax,%rbx,8), %rsi
	addq	%r12, %rsi
	addq	%r14, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	112(%rsp), %rax         # 8-byte Reload
	addq	$4, %rbx
	addq	%r14, %rbp
	cmpq	%rbx, %rax
	jne	.LBB4_13
.LBB4_14:                               # %._crit_edge507
	movq	input(%rip), %rax
	movl	5776(%rax), %eax
	movl	%eax, ChromaMEEnable(%rip)
	testl	%eax, %eax
	je	.LBB4_31
# BB#15:                                # %.preheader463
	cmpl	$0, 104(%rsp)           # 4-byte Folded Reload
	jle	.LBB4_31
# BB#16:                                # %.lr.ph502
	movslq	152(%rsp), %rax         # 4-byte Folded Reload
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movslq	200(%rsp), %r14         # 4-byte Folded Reload
	addq	%r14, %r14
	movslq	144(%rsp), %rax         # 4-byte Folded Reload
	movl	104(%rsp), %r12d        # 4-byte Reload
	leaq	-1(%r12), %rcx
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	movq	%r12, 160(%rsp)         # 8-byte Spill
	andq	$3, %r12
	movq	%rax, 144(%rsp)         # 8-byte Spill
	je	.LBB4_19
# BB#17:                                # %.prol.preheader549
	leaq	(,%rax,8), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	xorl	%ebx, %ebx
	movl	$orig_pic+512, %ebp
	movq	168(%rsp), %r15         # 8-byte Reload
	.p2align	4, 0x90
.LBB4_18:                               # =>This Inner Loop Header: Depth=1
	movq	imgUV_org(%rip), %rax
	movq	(%rax), %rax
	addq	152(%rsp), %rax         # 8-byte Folded Reload
	leaq	(%r15,%r15), %rsi
	addq	(%rax,%rbx,8), %rsi
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	incq	%rbx
	addq	%r14, %rbp
	cmpq	%rbx, %r12
	jne	.LBB4_18
	jmp	.LBB4_20
.LBB4_19:
	xorl	%ebx, %ebx
	movl	$orig_pic+512, %ebp
.LBB4_20:                               # %.prol.loopexit550
	cmpq	$3, 200(%rsp)           # 8-byte Folded Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	jb	.LBB4_23
# BB#21:                                # %.lr.ph502.new
	movq	144(%rsp), %rax         # 8-byte Reload
	leaq	(,%rax,8), %r12
	movq	168(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rax), %r15
	.p2align	4, 0x90
.LBB4_22:                               # =>This Inner Loop Header: Depth=1
	movq	imgUV_org(%rip), %rax
	movq	(%rax), %rax
	addq	%r12, %rax
	movq	(%rax,%rbx,8), %rsi
	addq	%r15, %rsi
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	imgUV_org(%rip), %rax
	movq	(%rax), %rax
	addq	%r12, %rax
	movq	8(%rax,%rbx,8), %rsi
	addq	%r15, %rsi
	addq	%r14, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	imgUV_org(%rip), %rax
	movq	(%rax), %rax
	addq	%r12, %rax
	movq	16(%rax,%rbx,8), %rsi
	addq	%r15, %rsi
	addq	%r14, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	imgUV_org(%rip), %rax
	movq	(%rax), %rax
	addq	%r12, %rax
	movq	24(%rax,%rbx,8), %rsi
	addq	%r15, %rsi
	addq	%r14, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	160(%rsp), %rcx         # 8-byte Reload
	addq	$4, %rbx
	addq	%r14, %rbp
	cmpq	%rbx, %rcx
	jne	.LBB4_22
.LBB4_23:                               # %.preheader461
	cmpl	$0, 104(%rsp)           # 4-byte Folded Reload
	movq	144(%rsp), %rax         # 8-byte Reload
	jle	.LBB4_31
# BB#24:                                # %.lr.ph499
	leaq	-1(%rcx), %rdx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movq	%rcx, %rbx
	andq	$3, %rbx
	je	.LBB4_27
# BB#25:                                # %.prol.preheader
	leaq	(,%rax,8), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	xorl	%r12d, %r12d
	movl	$orig_pic+1024, %ebp
	movq	168(%rsp), %r15         # 8-byte Reload
	.p2align	4, 0x90
.LBB4_26:                               # =>This Inner Loop Header: Depth=1
	movq	imgUV_org(%rip), %rax
	movq	8(%rax), %rax
	addq	152(%rsp), %rax         # 8-byte Folded Reload
	leaq	(%r15,%r15), %rsi
	addq	(%rax,%r12,8), %rsi
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	incq	%r12
	addq	%r14, %rbp
	cmpq	%r12, %rbx
	jne	.LBB4_26
	jmp	.LBB4_28
.LBB4_27:
	xorl	%r12d, %r12d
	movl	$orig_pic+1024, %ebp
	movq	168(%rsp), %r15         # 8-byte Reload
.LBB4_28:                               # %.prol.loopexit
	cmpq	$3, 104(%rsp)           # 8-byte Folded Reload
	movq	144(%rsp), %rbx         # 8-byte Reload
	jb	.LBB4_31
# BB#29:                                # %.lr.ph499.new
	shlq	$3, %rbx
	addq	%r15, %r15
	.p2align	4, 0x90
.LBB4_30:                               # =>This Inner Loop Header: Depth=1
	movq	imgUV_org(%rip), %rax
	movq	8(%rax), %rax
	addq	%rbx, %rax
	movq	(%rax,%r12,8), %rsi
	addq	%r15, %rsi
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	imgUV_org(%rip), %rax
	movq	8(%rax), %rax
	addq	%rbx, %rax
	movq	8(%rax,%r12,8), %rsi
	addq	%r15, %rsi
	addq	%r14, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	imgUV_org(%rip), %rax
	movq	8(%rax), %rax
	addq	%rbx, %rax
	movq	16(%rax,%r12,8), %rsi
	addq	%r15, %rsi
	addq	%r14, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	imgUV_org(%rip), %rax
	movq	8(%rax), %rax
	addq	%rbx, %rax
	movq	24(%rax,%r12,8), %rsi
	addq	%r15, %rsi
	addq	%r14, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	160(%rsp), %rax         # 8-byte Reload
	addq	$4, %r12
	addq	%r14, %rbp
	cmpq	%r12, %rax
	jne	.LBB4_30
.LBB4_31:                               # %.loopexit462
	movq	input(%rip), %rcx
	movl	5244(%rcx), %eax
	cmpl	$2, %eax
	je	.LBB4_34
# BB#32:                                # %.loopexit462
	cmpl	$1, %eax
	movq	192(%rsp), %r15         # 8-byte Reload
	movq	88(%rsp), %r12          # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	112(%rsp), %rbx         # 8-byte Reload
	jne	.LBB4_35
# BB#33:
	movl	%r12d, UMHEX_blocktype(%rip)
	movl	$0, bipred_flag(%rip)
	movl	$1, %eax
	jmp	.LBB4_35
.LBB4_34:
	movswl	16(%rsp), %edi          # 2-byte Folded Reload
	movq	192(%rsp), %r15         # 8-byte Reload
	movl	%r15d, %esi
	movq	48(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %edx
	movq	136(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	88(%rsp), %r12          # 8-byte Reload
	movl	%r12d, %r8d
	movq	176(%rsp), %r9          # 8-byte Reload
	callq	smpUMHEX_setup
	movq	%rbx, %rbp
	movq	input(%rip), %rcx
	movl	5244(%rcx), %eax
	movq	112(%rsp), %rbx         # 8-byte Reload
.LBB4_35:
	movq	32(%rsp), %rdi          # 8-byte Reload
	cmpl	$0, 5100(%rcx)
	setne	%cl
	cmpl	$5, %r12d
	setl	%dl
	andb	%cl, %dl
	movzbl	%dl, %ecx
	movl	%ecx, test8x8transform(%rip)
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rdx
	movq	6512(%rcx), %rcx
	movq	(%rdx,%rdi,8), %rsi
	movq	(%rcx,%rdi,8), %rdx
	cmpl	$1, %eax
	jne	.LBB4_37
# BB#36:
	leaq	100(%rsp), %rax
	movswl	16(%rsp), %ecx          # 2-byte Folded Reload
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %r8d
	movq	136(%rsp), %r9          # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%rax
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	pushq	144(%rsp)               # 8-byte Folded Reload
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	callq	UMHEXSetMotionVectorPredictor
	addq	$32, %rsp
.Lcfi57:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB4_38
.LBB4_37:
	subq	$8, %rsp
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	movswl	24(%rsp), %ecx          # 2-byte Folded Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	144(%rsp), %r9          # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%rbx
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	pushq	144(%rsp)               # 8-byte Folded Reload
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	callq	SetMotionVectorPredictor
	addq	$32, %rsp
.Lcfi62:
	.cfi_adjust_cfa_offset -32
.LBB4_38:
	movq	input(%rip), %rax
	movl	5244(%rax), %ecx
	cmpq	$3, %rcx
	ja	.LBB4_41
# BB#39:
	jmpq	*.LJTI4_0(,%rcx,8)
.LBB4_40:
	leaq	10(%rsp), %r10
	movl	100(%rsp), %ebp
	movq	272(%rsp), %rax
	movl	(%rax), %ebx
	movq	56(%rsp), %rax          # 8-byte Reload
	movswl	(%rax), %r11d
	movswl	2(%rax), %eax
	subq	$8, %rsp
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	leaq	16(%rsp), %r14
	movswl	24(%rsp), %esi          # 2-byte Folded Reload
	movl	$orig_pic, %edi
	movl	%r15d, %edx
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	48(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%r12d, %r9d
	pushq	%rbx
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	pushq	$2147483647             # imm = 0x7FFFFFFF
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi67:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi68:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi69:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi70:
	.cfi_adjust_cfa_offset 8
	callq	FastFullPelBlockMotionSearch
	addq	$64, %rsp
.Lcfi71:
	.cfi_adjust_cfa_offset -64
	jmp	.LBB4_63
.LBB4_41:
	movq	56(%rsp), %rsi          # 8-byte Reload
	movzwl	(%rsi), %edx
	movw	%dx, %cx
	sarw	$15, %cx
	andl	$49152, %ecx            # imm = 0xC000
	shrl	$14, %ecx
	addl	%edx, %ecx
	sarw	$2, %cx
	movw	%cx, 8(%rsp)
	movzwl	2(%rsi), %esi
	movw	%si, %dx
	sarw	$15, %dx
	andl	$49152, %edx            # imm = 0xC000
	shrl	$14, %edx
	addl	%esi, %edx
	sarw	$2, %dx
	leaq	10(%rsp), %r10
	movw	%dx, 10(%rsp)
	movl	100(%rsp), %ebx
	cmpl	$0, 4168(%rax)
	jne	.LBB4_43
# BB#42:
	movswl	%dx, %eax
	movl	%ebx, %edx
	negl	%edx
	movswl	%cx, %ecx
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	cmpl	%ebx, %ecx
	cmovgew	%bx, %cx
	movw	%cx, 8(%rsp)
	cmpl	%edx, %eax
	cmovgel	%eax, %edx
	cmpl	%ebx, %edx
	cmovgew	%bx, %dx
	movw	%dx, 10(%rsp)
                                        # kill: %DX<def> %DX<kill> %EDX<kill> %EDX<def>
.LBB4_43:                               # %._crit_edge541
	leal	-2047(%rbx), %eax
	movl	$2047, %esi             # imm = 0x7FF
	subl	%ebx, %esi
	movswl	%cx, %ecx
	cmpl	%eax, %ecx
	cmovgel	%ecx, %eax
	cmpl	%esi, %eax
	cmovgew	%si, %ax
	movw	%ax, 8(%rsp)
	movq	img(%rip), %rax
	movslq	8(%rax), %rax
	shlq	$3, %rax
	movl	LEVELMVLIMIT(%rax,%rax,2), %ecx
	addl	%ebx, %ecx
	movl	LEVELMVLIMIT+4(%rax,%rax,2), %eax
	subl	%ebx, %eax
	movswl	%dx, %edx
	cmpl	%ecx, %edx
	cmovgel	%edx, %ecx
	cmpl	%eax, %ecx
	cmovlw	%cx, %ax
	movw	%ax, 10(%rsp)
	movq	272(%rsp), %rax
	movl	(%rax), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movswl	(%rcx), %r11d
	movswl	2(%rcx), %ebp
	subq	$8, %rsp
.Lcfi72:
	.cfi_adjust_cfa_offset 8
	leaq	16(%rsp), %r14
	movswl	24(%rsp), %esi          # 2-byte Folded Reload
	movl	$orig_pic, %edi
	movl	%r15d, %edx
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	48(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%r12d, %r9d
	pushq	%rax
.Lcfi73:
	.cfi_adjust_cfa_offset 8
	pushq	$2147483647             # imm = 0x7FFFFFFF
.Lcfi74:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi75:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi76:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi77:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi78:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi79:
	.cfi_adjust_cfa_offset 8
	callq	FullPelBlockMotionSearch
	addq	$64, %rsp
.Lcfi80:
	.cfi_adjust_cfa_offset -64
	jmp	.LBB4_63
.LBB4_44:
	movq	56(%rsp), %rsi          # 8-byte Reload
	movzwl	(%rsi), %edx
	movw	%dx, %cx
	sarw	$15, %cx
	andl	$49152, %ecx            # imm = 0xC000
	shrl	$14, %ecx
	addl	%edx, %ecx
	sarw	$2, %cx
	movw	%cx, 8(%rsp)
	movzwl	2(%rsi), %esi
	movw	%si, %dx
	sarw	$15, %dx
	andl	$49152, %edx            # imm = 0xC000
	shrl	$14, %edx
	addl	%esi, %edx
	sarw	$2, %dx
	leaq	10(%rsp), %r10
	movw	%dx, 10(%rsp)
	movl	100(%rsp), %ebx
	cmpl	$0, 4168(%rax)
	jne	.LBB4_46
# BB#45:
	movswl	%dx, %eax
	movl	%ebx, %edx
	negl	%edx
	movswl	%cx, %ecx
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	cmpl	%ebx, %ecx
	cmovgew	%bx, %cx
	movw	%cx, 8(%rsp)
	cmpl	%edx, %eax
	cmovgel	%eax, %edx
	cmpl	%ebx, %edx
	cmovgew	%bx, %dx
	movw	%dx, 10(%rsp)
                                        # kill: %DX<def> %DX<kill> %EDX<kill> %EDX<def>
.LBB4_46:                               # %._crit_edge539
	leal	-2047(%rbx), %eax
	movl	$2047, %esi             # imm = 0x7FF
	subl	%ebx, %esi
	movswl	%cx, %ecx
	cmpl	%eax, %ecx
	cmovgel	%ecx, %eax
	cmpl	%esi, %eax
	cmovgew	%si, %ax
	movw	%ax, 8(%rsp)
	movq	img(%rip), %rax
	movslq	8(%rax), %rax
	shlq	$3, %rax
	movl	LEVELMVLIMIT(%rax,%rax,2), %ecx
	addl	%ebx, %ecx
	movl	LEVELMVLIMIT+4(%rax,%rax,2), %eax
	subl	%ebx, %eax
	movswl	%dx, %edx
	cmpl	%ecx, %edx
	cmovgel	%edx, %ecx
	cmpl	%eax, %ecx
	cmovlw	%cx, %ax
	movw	%ax, 10(%rsp)
	movq	272(%rsp), %rax
	movl	(%rax), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movswl	(%rcx), %r11d
	movswl	2(%rcx), %ebp
	subq	$8, %rsp
.Lcfi81:
	.cfi_adjust_cfa_offset 8
	leaq	16(%rsp), %r14
	movswl	24(%rsp), %esi          # 2-byte Folded Reload
	movl	$orig_pic, %edi
	movl	%r15d, %edx
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	48(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%r12d, %r9d
	pushq	%rax
.Lcfi82:
	.cfi_adjust_cfa_offset 8
	pushq	$2147483647             # imm = 0x7FFFFFFF
.Lcfi83:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi84:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi85:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi86:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi87:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi88:
	.cfi_adjust_cfa_offset 8
	callq	UMHEXIntegerPelBlockMotionSearch
	addq	$64, %rsp
.Lcfi89:
	.cfi_adjust_cfa_offset -64
	jmp	.LBB4_63
.LBB4_47:
	movq	56(%rsp), %rsi          # 8-byte Reload
	movzwl	(%rsi), %edx
	movw	%dx, %cx
	sarw	$15, %cx
	andl	$49152, %ecx            # imm = 0xC000
	shrl	$14, %ecx
	addl	%edx, %ecx
	sarw	$2, %cx
	movw	%cx, 8(%rsp)
	movzwl	2(%rsi), %esi
	movw	%si, %dx
	sarw	$15, %dx
	andl	$49152, %edx            # imm = 0xC000
	shrl	$14, %edx
	addl	%esi, %edx
	sarw	$2, %dx
	leaq	10(%rsp), %r10
	movw	%dx, 10(%rsp)
	movl	100(%rsp), %ebx
	cmpl	$0, 4168(%rax)
	jne	.LBB4_49
# BB#48:
	movswl	%dx, %eax
	movl	%ebx, %edx
	negl	%edx
	movswl	%cx, %ecx
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	cmpl	%ebx, %ecx
	cmovgew	%bx, %cx
	movw	%cx, 8(%rsp)
	cmpl	%edx, %eax
	cmovgel	%eax, %edx
	cmpl	%ebx, %edx
	cmovgew	%bx, %dx
	movw	%dx, 10(%rsp)
                                        # kill: %DX<def> %DX<kill> %EDX<kill> %EDX<def>
.LBB4_49:                               # %._crit_edge535
	leal	-2047(%rbx), %eax
	movl	$2047, %esi             # imm = 0x7FF
	subl	%ebx, %esi
	movswl	%cx, %ecx
	cmpl	%eax, %ecx
	cmovgel	%ecx, %eax
	cmpl	%esi, %eax
	cmovgew	%si, %ax
	movw	%ax, 8(%rsp)
	movq	img(%rip), %rax
	movslq	8(%rax), %rax
	shlq	$3, %rax
	movl	LEVELMVLIMIT(%rax,%rax,2), %ecx
	addl	%ebx, %ecx
	movl	LEVELMVLIMIT+4(%rax,%rax,2), %eax
	subl	%ebx, %eax
	movswl	%dx, %edx
	cmpl	%ecx, %edx
	cmovgel	%edx, %ecx
	cmpl	%eax, %ecx
	cmovlw	%cx, %ax
	movw	%ax, 10(%rsp)
	movq	272(%rsp), %rax
	movl	(%rax), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movswl	(%rcx), %r11d
	movswl	2(%rcx), %ebp
	subq	$8, %rsp
.Lcfi90:
	.cfi_adjust_cfa_offset 8
	leaq	16(%rsp), %r14
	movswl	24(%rsp), %esi          # 2-byte Folded Reload
	movl	$orig_pic, %edi
	movl	%r15d, %edx
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	48(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%r12d, %r9d
	pushq	%rax
.Lcfi91:
	.cfi_adjust_cfa_offset 8
	pushq	$2147483647             # imm = 0x7FFFFFFF
.Lcfi92:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi93:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi94:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi95:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi96:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi97:
	.cfi_adjust_cfa_offset 8
	callq	smpUMHEXIntegerPelBlockMotionSearch
	addq	$64, %rsp
.Lcfi98:
	.cfi_adjust_cfa_offset -64
	movl	%eax, %r12d
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	%eax, %r8d
	sarl	$2, %r8d
	testl	%r8d, %r8d
	jle	.LBB4_64
# BB#50:                                # %.preheader460.lr.ph
	movq	112(%rsp), %rax         # 8-byte Reload
	movl	%eax, %r9d
	sarl	$2, %r9d
	testl	%r9d, %r9d
	jle	.LBB4_64
# BB#51:                                # %.preheader460.us.preheader
	xorl	%r10d, %r10d
	movq	48(%rsp), %r11          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_52:                               # %.preheader460.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_53 Depth 2
                                        #     Child Loop BB4_54 Depth 2
	movq	136(%rsp), %rax         # 8-byte Reload
	leal	(%r10,%rax), %esi
	testl	%r15d, %r15d
	movq	img(%rip), %rdi
	movl	%r11d, %ebp
	movl	%r9d, %ecx
	movl	%r11d, %eax
	movl	%r9d, %ebx
	je	.LBB4_54
	.p2align	4, 0x90
.LBB4_53:                               # %.lr.ph486..lr.ph486.split_crit_edge.us
                                        #   Parent Loop BB4_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	smpUMHEX_l1_cost(%rip), %rax
	movq	(%rax,%r13,8), %rax
	movl	176(%rdi), %ebx
	movl	180(%rdi), %edx
	sarl	$2, %edx
	addl	%ebp, %edx
	movslq	%edx, %rdx
	sarl	$2, %ebx
	movq	(%rax,%rdx,8), %rax
	addl	%esi, %ebx
	movslq	%ebx, %rdx
	movl	%r12d, (%rax,%rdx,4)
	incl	%ebp
	decl	%ecx
	jne	.LBB4_53
	jmp	.LBB4_55
	.p2align	4, 0x90
.LBB4_54:                               # %.lr.ph486.split.us.us
                                        #   Parent Loop BB4_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	smpUMHEX_l0_cost(%rip), %rcx
	movq	(%rcx,%r13,8), %rcx
	movl	176(%rdi), %edx
	movl	180(%rdi), %ebp
	sarl	$2, %ebp
	addl	%eax, %ebp
	movslq	%ebp, %rbp
	sarl	$2, %edx
	movq	(%rcx,%rbp,8), %rcx
	addl	%esi, %edx
	movslq	%edx, %rdx
	movl	%r12d, (%rcx,%rdx,4)
	incl	%eax
	decl	%ebx
	jne	.LBB4_54
.LBB4_55:                               # %._crit_edge487.us
                                        #   in Loop: Header=BB4_52 Depth=1
	incl	%r10d
	cmpl	%r8d, %r10d
	jne	.LBB4_52
# BB#56:
	movq	72(%rsp), %r8           # 8-byte Reload
	jmp	.LBB4_65
.LBB4_57:
	movl	4120(%rax), %ecx
	movq	56(%rsp), %rdx          # 8-byte Reload
	movd	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	pshufd	$196, %xmm0, %xmm0      # xmm0 = xmm0[0,1,0,3]
	pshufhw	$229, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,5,5,6,7]
	testl	%ecx, %ecx
	jne	.LBB4_59
# BB#58:
	psllq	$48, %xmm0
	movdqa	%xmm0, %xmm1
	psrad	$31, %xmm1
	pshufd	$237, %xmm1, %xmm1      # xmm1 = xmm1[1,3,2,3]
	psrad	$16, %xmm0
	pshufd	$237, %xmm0, %xmm0      # xmm0 = xmm0[1,3,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	paddq	.LCPI4_0(%rip), %xmm0
	pand	.LCPI4_1(%rip), %xmm0
	psrlq	$2, %xmm0
.LBB4_59:
	movd	%xmm0, %esi
	movw	%si, 8(%rsp)
	pextrw	$4, %xmm0, %edx
	movw	%dx, 10(%rsp)
	cmpl	$0, 4168(%rax)
	movl	100(%rsp), %eax
	je	.LBB4_61
# BB#60:                                # %._crit_edge
	addl	%ecx, %ecx
	jmp	.LBB4_62
.LBB4_61:
	movswl	%dx, %edx
	movl	%eax, %edi
	negl	%edi
	addl	%ecx, %ecx
	shll	%cl, %edi
	movl	%eax, %ebp
	shll	%cl, %ebp
	movswl	%si, %esi
	cmpl	%edi, %esi
	cmovll	%edi, %esi
	cmpl	%ebp, %esi
	cmovgew	%bp, %si
	movw	%si, 8(%rsp)
	cmpl	%edi, %edx
	cmovgel	%edx, %edi
	cmpl	%ebp, %edi
	cmovgew	%bp, %di
	movw	%di, 10(%rsp)
	movw	%di, %dx
.LBB4_62:
	leal	-2047(%rax), %edi
	shll	%cl, %edi
	movl	$2047, %ebp             # imm = 0x7FF
	subl	%eax, %ebp
	shll	%cl, %ebp
	movswl	%si, %esi
	cmpl	%edi, %esi
	cmovgel	%esi, %edi
	cmpl	%ebp, %edi
	cmovlw	%di, %bp
	movw	%bp, 8(%rsp)
	movq	img(%rip), %rsi
	movslq	8(%rsi), %rsi
	shlq	$3, %rsi
	movl	LEVELMVLIMIT(%rsi,%rsi,2), %edi
	addl	%eax, %edi
	shll	%cl, %edi
	movl	LEVELMVLIMIT+4(%rsi,%rsi,2), %esi
	subl	%eax, %esi
	shll	%cl, %esi
	movswl	%dx, %edx
	cmpl	%edi, %edx
	cmovgel	%edx, %edi
	cmpl	%esi, %edi
	cmovlw	%di, %si
	movw	%si, 10(%rsp)
	movq	enc_picture(%rip), %rdx
	movq	6488(%rdx), %r8
	movq	6512(%rdx), %r9
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movq	272(%rsp), %rcx
	movl	(%rcx), %ebp
	leaq	8(%rsp), %rbx
	movswl	16(%rsp), %esi          # 2-byte Folded Reload
	movl	$orig_pic, %edi
	movl	%r15d, %edx
	movq	184(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	pushq	%rbp
.Lcfi99:
	.cfi_adjust_cfa_offset 8
	pushq	$2147483647             # imm = 0x7FFFFFFF
.Lcfi100:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi101:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi102:
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)                # 8-byte Folded Reload
.Lcfi103:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi104:
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)                # 8-byte Folded Reload
.Lcfi105:
	.cfi_adjust_cfa_offset 8
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi106:
	.cfi_adjust_cfa_offset 8
	callq	EPZSPelBlockMotionSearch
	addq	$64, %rsp
.Lcfi107:
	.cfi_adjust_cfa_offset -64
.LBB4_63:                               # %.loopexit
	movl	%eax, %r12d
.LBB4_64:                               # %.loopexit
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	48(%rsp), %r11          # 8-byte Reload
.LBB4_65:                               # %.loopexit
	movq	32(%rsp), %r10          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	input(%rip), %rax
	cmpl	$0, 4120(%rax)
	je	.LBB4_67
# BB#66:
	cmpl	$3, 5244(%rax)
	je	.LBB4_68
.LBB4_67:
	shlw	$2, 8(%rsp)
	shlw	$2, 10(%rsp)
.LBB4_68:
	xorl	%edx, %edx
	cmpl	$2, 5776(%rax)
	sete	%dl
	movl	%edx, ChromaMEEnable(%rip)
	cmpl	$0, 24(%rax)
	jne	.LBB4_89
# BB#69:
	movl	5244(%rax), %esi
	cmpw	$0, 16(%rsp)            # 2-byte Folded Reload
	je	.LBB4_72
# BB#70:
	cmpl	$3, %esi
	jne	.LBB4_72
# BB#71:
	movq	img(%rip), %rdx
	cmpl	$0, 24(%rdx)
	je	.LBB4_84
.LBB4_72:
	cmpl	$0, start_me_refinement_hp(%rip)
	movl	$2147483647, %edx       # imm = 0x7FFFFFFF
	cmovel	%edx, %r12d
	cmpl	$3, %esi
	je	.LBB4_77
# BB#73:
	cmpl	$2, %esi
	je	.LBB4_81
# BB#74:
	cmpl	$1, %esi
	jne	.LBB4_79
# BB#75:
	movq	56(%rsp), %rax          # 8-byte Reload
	movzwl	(%rax), %ecx
	movzwl	2(%rax), %edx
	leaq	10(%rsp), %r11
	movq	88(%rsp), %r9           # 8-byte Reload
	cmpl	$4, %r9d
	jl	.LBB4_86
# BB#76:
	movq	272(%rsp), %rax
	movl	8(%rax), %ebp
	leaq	8(%rsp), %r10
	movswl	%dx, %ebx
	movswl	%cx, %eax
	movswl	16(%rsp), %esi          # 2-byte Folded Reload
	movl	$orig_pic, %edi
	movl	%r15d, %edx
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%rbp
.Lcfi108:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi109:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi110:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi111:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi112:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi113:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi114:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi115:
	.cfi_adjust_cfa_offset 8
	callq	UMHEXSubPelBlockMotionSearch
	addq	$64, %rsp
.Lcfi116:
	.cfi_adjust_cfa_offset -64
	jmp	.LBB4_83
.LBB4_77:
	cmpl	$0, 4124(%rax)
	je	.LBB4_79
# BB#78:
	leaq	8(%rsp), %rax
	movswl	16(%rsp), %esi          # 2-byte Folded Reload
	movl	$orig_pic, %edi
	movl	%r15d, %edx
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	88(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	272(%rsp)
.Lcfi117:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi118:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi119:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi120:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi121:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)                # 8-byte Folded Reload
.Lcfi122:
	.cfi_adjust_cfa_offset 8
	callq	EPZSSubPelBlockMotionSearch
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	80(%rsp), %r10          # 8-byte Reload
	movq	96(%rsp), %r11          # 8-byte Reload
	movq	120(%rsp), %r8          # 8-byte Reload
	addq	$48, %rsp
.Lcfi123:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB4_80
.LBB4_79:
	leaq	10(%rsp), %r10
	movq	56(%rsp), %rax          # 8-byte Reload
	movswl	(%rax), %ebp
	movswl	2(%rax), %ebx
	leaq	8(%rsp), %rax
	movswl	16(%rsp), %esi          # 2-byte Folded Reload
	movl	$orig_pic, %edi
	movl	%r15d, %edx
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	88(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	272(%rsp)
.Lcfi124:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi125:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi126:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi127:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi128:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi129:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi130:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi131:
	.cfi_adjust_cfa_offset 8
	callq	SubPelBlockMotionSearch
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	96(%rsp), %r10          # 8-byte Reload
	movq	112(%rsp), %r11         # 8-byte Reload
	movq	136(%rsp), %r8          # 8-byte Reload
	addq	$64, %rsp
.Lcfi132:
	.cfi_adjust_cfa_offset -64
.LBB4_80:
	movl	%eax, %r12d
	jmp	.LBB4_89
.LBB4_81:
	movq	56(%rsp), %rax          # 8-byte Reload
	movzwl	(%rax), %ecx
	movzwl	2(%rax), %edx
	leaq	10(%rsp), %r11
	movq	272(%rsp), %rax
	movl	8(%rax), %ebp
	movq	88(%rsp), %r9           # 8-byte Reload
	leaq	8(%rsp), %r10
	movswl	%dx, %ebx
	movswl	%cx, %eax
	cmpl	$2, %r9d
	jl	.LBB4_87
# BB#82:
	movswl	16(%rsp), %esi          # 2-byte Folded Reload
	movl	$orig_pic, %edi
	movl	%r15d, %edx
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%rbp
.Lcfi133:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi134:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi135:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi136:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi137:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi138:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi139:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi140:
	.cfi_adjust_cfa_offset 8
	callq	smpUMHEXSubPelBlockMotionSearch
	addq	$64, %rsp
.Lcfi141:
	.cfi_adjust_cfa_offset -64
.LBB4_83:                               # %.thread
	movl	%eax, 104(%rsp)         # 4-byte Spill
	xorl	%r14d, %r14d
	jmp	.LBB4_97
.LBB4_84:
	cmpw	$0, 16(%rsp)            # 2-byte Folded Reload
	jle	.LBB4_89
# BB#85:
	cvtsi2sdl	%r12d, %xmm0
	movq	24(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	sarl	$2, %edx
	movslq	%edx, %rdx
	movq	208(%rsp), %rdi         # 8-byte Reload
	cvtsi2sdl	(%rdi,%rdx,4), %xmm1
	mulsd	.LCPI4_2(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	ja	.LBB4_72
	jmp	.LBB4_89
.LBB4_86:
	leaq	8(%rsp), %rax
	movswl	%dx, %ebp
	movswl	%cx, %ebx
	movswl	16(%rsp), %esi          # 2-byte Folded Reload
	movl	$orig_pic, %edi
	movl	%r15d, %edx
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	272(%rsp)
.Lcfi142:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi143:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi144:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi145:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi146:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi147:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi148:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi149:
	.cfi_adjust_cfa_offset 8
	callq	SubPelBlockMotionSearch
	addq	$64, %rsp
.Lcfi150:
	.cfi_adjust_cfa_offset -64
	jmp	.LBB4_88
.LBB4_87:
	movswl	16(%rsp), %esi          # 2-byte Folded Reload
	movl	$orig_pic, %edi
	movl	%r15d, %edx
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%rbp
.Lcfi151:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi152:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi153:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi154:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi155:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi156:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi157:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi158:
	.cfi_adjust_cfa_offset 8
	callq	smpUMHEXFullSubPelBlockMotionSearch
	addq	$64, %rsp
.Lcfi159:
	.cfi_adjust_cfa_offset -64
.LBB4_88:
	movl	%eax, %r12d
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	48(%rsp), %r11          # 8-byte Reload
	movq	32(%rsp), %r10          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
.LBB4_89:
	cmpl	$1, 88(%rsp)            # 4-byte Folded Reload
	sete	%r14b
	jne	.LBB4_95
# BB#90:
	movq	input(%rip), %rax
	movl	4168(%rax), %eax
	testl	%eax, %eax
	jne	.LBB4_95
# BB#91:
	movq	img(%rip), %rax
	movl	20(%rax), %eax
	movb	$1, %r14b
	cmpl	$3, %eax
	je	.LBB4_93
# BB#92:
	testl	%eax, %eax
	jne	.LBB4_95
.LBB4_93:
	callq	FindSkipModeMotionVector
	callq	GetSkipCostMB
	movl	$4096, %edx             # imm = 0x1000
	movq	272(%rsp), %rcx
	addl	8(%rcx), %edx
	sarl	$13, %edx
	subl	%edx, %eax
	cmpl	%r12d, %eax
	jge	.LBB4_96
# BB#94:
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movq	img(%rip), %rax
	movq	14384(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movzwl	(%rax), %ecx
	movw	%cx, 8(%rsp)
	movzwl	2(%rax), %eax
	movw	%ax, 10(%rsp)
	jmp	.LBB4_97
.LBB4_95:
	movl	%r12d, %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movq	120(%rsp), %r12         # 8-byte Reload
	jmp	.LBB4_98
.LBB4_96:
	movl	%r12d, %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
.LBB4_97:                               # %.thread
	movq	120(%rsp), %r12         # 8-byte Reload
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	48(%rsp), %r11          # 8-byte Reload
	movq	32(%rsp), %r10          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
.LBB4_98:                               # %.thread
	movq	112(%rsp), %rax         # 8-byte Reload
	movl	%eax, %edx
	sarl	$2, %edx
	leal	(%rdx,%r11), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	testl	%edx, %edx
	jle	.LBB4_104
# BB#99:                                # %.preheader459.lr.ph
	movq	128(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	sarl	$2, %eax
	testl	%eax, %eax
	jle	.LBB4_104
# BB#100:                               # %.preheader459.us.preheader
	addl	136(%rsp), %eax         # 4-byte Folded Reload
	cltq
	movslq	48(%rsp), %r9           # 4-byte Folded Reload
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB4_101:                              # %.preheader459.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_102 Depth 2
	movq	176(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi,%rdx,8), %rsi
	movq	%r8, %rdi
	.p2align	4, 0x90
.LBB4_102:                              #   Parent Loop BB4_101 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	8(%rsp), %ebp
	movq	(%rsi,%rdi,8), %rbx
	movq	(%rbx,%r10,8), %rbx
	movq	(%rbx,%rcx,8), %rbx
	movq	(%rbx,%r13,8), %rbx
	movw	%bp, (%rbx)
	movzwl	10(%rsp), %ebp
	movw	%bp, 2(%rbx)
	incq	%rdi
	cmpq	%rax, %rdi
	jl	.LBB4_102
# BB#103:                               # %._crit_edge481.us
                                        #   in Loop: Header=BB4_101 Depth=1
	incq	%rdx
	cmpq	%r9, %rdx
	jl	.LBB4_101
.LBB4_104:                              # %._crit_edge483
	movq	img(%rip), %rax
	cmpl	$1, 20(%rax)
	jne	.LBB4_156
# BB#105:
	movq	input(%rip), %rcx
	cmpl	$0, 2120(%rcx)
	setne	%dl
	cmpw	$0, 16(%rsp)            # 2-byte Folded Reload
	jne	.LBB4_156
# BB#106:
	andb	%dl, %r14b
	je	.LBB4_156
# BB#107:
	leaq	14400(%rax), %rdx
	addq	$14392, %rax            # imm = 0x3838
	testl	%r15d, %r15d
	cmoveq	%rdx, %rax
	movq	(%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$0, 12(%rsp)
	movl	$0, 20(%rsp)
	cmpl	$1, 5244(%rcx)
	jne	.LBB4_109
# BB#108:
	movl	$1, bipred_flag(%rip)
	movq	enc_picture(%rip), %rax
	movl	%r15d, %ecx
	xorl	$1, %ecx
	movslq	%ecx, %rcx
	movq	6488(%rax), %rdx
	movq	6512(%rax), %rax
	movq	(%rdx,%rcx,8), %rsi
	xorl	%r8d, %r8d
	testl	%r15d, %r15d
	sete	%r8b
	movq	(%rax,%r8,8), %rdx
	leaq	100(%rsp), %rax
	leaq	68(%rsp), %rdi
	movl	$0, %ecx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	136(%rsp), %r9          # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%rax
.Lcfi160:
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)               # 8-byte Folded Reload
.Lcfi161:
	.cfi_adjust_cfa_offset 8
	pushq	144(%rsp)               # 8-byte Folded Reload
.Lcfi162:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi163:
	.cfi_adjust_cfa_offset 8
	callq	UMHEXSetMotionVectorPredictor
	addq	$32, %rsp
.Lcfi164:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB4_110
.LBB4_109:
	movq	enc_picture(%rip), %rax
	movl	%r15d, %ecx
	xorl	$1, %ecx
	movslq	%ecx, %rcx
	movq	6488(%rax), %rdx
	movq	6512(%rax), %rax
	movq	(%rdx,%rcx,8), %rsi
	xorl	%ecx, %ecx
	testl	%r15d, %r15d
	sete	%cl
	movq	(%rax,%rcx,8), %rdx
	subq	$8, %rsp
.Lcfi165:
	.cfi_adjust_cfa_offset 8
	leaq	76(%rsp), %rdi
	movl	$0, %ecx
	movq	144(%rsp), %r9          # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	120(%rsp)               # 8-byte Folded Reload
.Lcfi166:
	.cfi_adjust_cfa_offset 8
	pushq	144(%rsp)               # 8-byte Folded Reload
.Lcfi167:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi168:
	.cfi_adjust_cfa_offset 8
	callq	SetMotionVectorPredictor
	addq	$32, %rsp
.Lcfi169:
	.cfi_adjust_cfa_offset -32
.LBB4_110:
	movq	input(%rip), %rcx
	movl	5244(%rcx), %edx
	cmpl	$3, %edx
	jne	.LBB4_112
# BB#111:
	cmpl	$0, 4120(%rcx)
	jne	.LBB4_113
.LBB4_112:
	movswl	8(%rsp), %eax
	addl	$2, %eax
	shrl	$2, %eax
	movw	%ax, 8(%rsp)
	movswl	10(%rsp), %eax
	addl	$2, %eax
	shrl	$2, %eax
	movw	%ax, 10(%rsp)
.LBB4_113:                              # %.preheader458
	xorl	%r14d, %r14d
	cmpl	$0, 2124(%rcx)
	js	.LBB4_133
# BB#114:                               # %.lr.ph473
	movl	%r15d, %eax
	xorl	$1, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	$2147483647, %r11d      # imm = 0x7FFFFFFF
	.p2align	4, 0x90
.LBB4_115:                              # =>This Inner Loop Header: Depth=1
	testb	$1, %r14b
	jne	.LBB4_118
# BB#116:                               #   in Loop: Header=BB4_115 Depth=1
	testl	%r14d, %r14d
	je	.LBB4_119
# BB#117:                               #   in Loop: Header=BB4_115 Depth=1
	movzwl	12(%rsp), %edx
	movw	%dx, 20(%rsp)
	movzwl	14(%rsp), %edx
	movw	%dx, 22(%rsp)
	movw	8(%rsp), %dx
	movw	%dx, 12(%rsp)
	movw	10(%rsp), %si
	jmp	.LBB4_123
	.p2align	4, 0x90
.LBB4_118:                              #   in Loop: Header=BB4_115 Depth=1
	movzwl	12(%rsp), %edx
	movw	%dx, 20(%rsp)
	movzwl	14(%rsp), %edx
	movw	%dx, 22(%rsp)
	movw	8(%rsp), %dx
	movw	%dx, 12(%rsp)
	movw	10(%rsp), %si
	leaq	68(%rsp), %rbx
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	16(%rsp), %edi          # 4-byte Reload
                                        # kill: %DI<def> %DI<kill> %EDI<kill>
	jmp	.LBB4_124
.LBB4_119:                              #   in Loop: Header=BB4_115 Depth=1
	movzwl	8(%rsp), %edx
	movw	%dx, 20(%rsp)
	movzwl	10(%rsp), %edx
	movw	%dx, 22(%rsp)
	cmpl	$3, 5244(%rcx)
	jne	.LBB4_122
# BB#120:                               #   in Loop: Header=BB4_115 Depth=1
	cmpl	$0, 4120(%rcx)
	je	.LBB4_122
# BB#121:                               #   in Loop: Header=BB4_115 Depth=1
	movw	68(%rsp), %dx
	movw	%dx, 12(%rsp)
	movw	70(%rsp), %si
	jmp	.LBB4_123
.LBB4_122:                              #   in Loop: Header=BB4_115 Depth=1
	movswl	68(%rsp), %edx
	addl	$2, %edx
	shrl	$2, %edx
	movw	%dx, 12(%rsp)
	movswl	70(%rsp), %esi
	addl	$2, %esi
	shrl	$2, %esi
.LBB4_123:                              #   in Loop: Header=BB4_115 Depth=1
	movq	56(%rsp), %rbx          # 8-byte Reload
	leaq	68(%rsp), %rax
	movl	%r15d, %edi
.LBB4_124:                              #   in Loop: Header=BB4_115 Depth=1
	movw	%si, 14(%rsp)
	movw	%dx, 8(%rsp)
	movw	%si, 10(%rsp)
	movl	5244(%rcx), %esi
	cmpl	$1, %esi
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movw	%di, 160(%rsp)          # 2-byte Spill
	movswl	%di, %edx
	je	.LBB4_127
# BB#125:                               #   in Loop: Header=BB4_115 Depth=1
	cmpl	$3, %esi
	jne	.LBB4_128
# BB#126:                               #   in Loop: Header=BB4_115 Depth=1
	movq	enc_picture(%rip), %rsi
	movq	6488(%rsi), %r8
	movq	6512(%rsi), %r9
	movl	2128(%rcx), %ebp
	movzbl	4120(%rcx), %ecx
	addb	%cl, %cl
	shll	%cl, %ebp
	movl	%r14d, %ecx
	sarl	%cl, %ebp
	movq	272(%rsp), %rcx
	movl	(%rcx), %r10d
	movl	$orig_pic, %edi
	xorl	%esi, %esi
	movq	184(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	pushq	%r10
.Lcfi170:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi171:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi172:
	.cfi_adjust_cfa_offset 8
	leaq	44(%rsp), %rbp
	pushq	%rbp
.Lcfi173:
	.cfi_adjust_cfa_offset 8
	leaq	44(%rsp), %rbp
	pushq	%rbp
.Lcfi174:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi175:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi176:
	.cfi_adjust_cfa_offset 8
	pushq	144(%rsp)               # 8-byte Folded Reload
.Lcfi177:
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)               # 8-byte Folded Reload
.Lcfi178:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)                # 8-byte Folded Reload
.Lcfi179:
	.cfi_adjust_cfa_offset 8
	callq	EPZSBiPredBlockMotionSearch
	addq	$80, %rsp
.Lcfi180:
	.cfi_adjust_cfa_offset -80
	jmp	.LBB4_131
	.p2align	4, 0x90
.LBB4_127:                              #   in Loop: Header=BB4_115 Depth=1
	movl	2128(%rcx), %ebp
	movl	%r14d, %ecx
	sarl	%cl, %ebp
	movq	272(%rsp), %rcx
	movl	(%rcx), %r15d
	movswl	(%rbx), %ecx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movswl	2(%rbx), %r10d
	movswl	(%rax), %ebx
	movswl	2(%rax), %r12d
	subq	$8, %rsp
.Lcfi181:
	.cfi_adjust_cfa_offset 8
	movl	$orig_pic, %edi
	xorl	%esi, %esi
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	48(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	96(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%r15
	movq	208(%rsp), %r15         # 8-byte Reload
.Lcfi182:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi183:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi184:
	.cfi_adjust_cfa_offset 8
	leaq	54(%rsp), %rax
	pushq	%rax
.Lcfi185:
	.cfi_adjust_cfa_offset 8
	leaq	60(%rsp), %rax
	pushq	%rax
.Lcfi186:
	.cfi_adjust_cfa_offset 8
	leaq	62(%rsp), %rax
	pushq	%rax
.Lcfi187:
	.cfi_adjust_cfa_offset 8
	leaq	68(%rsp), %rax
	pushq	%rax
.Lcfi188:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
	movq	192(%rsp), %r12         # 8-byte Reload
.Lcfi189:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi190:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi191:
	.cfi_adjust_cfa_offset 8
	pushq	240(%rsp)               # 8-byte Folded Reload
.Lcfi192:
	.cfi_adjust_cfa_offset 8
	callq	UMHEXBipredIntegerPelBlockMotionSearch
	addq	$96, %rsp
.Lcfi193:
	.cfi_adjust_cfa_offset -96
	jmp	.LBB4_131
	.p2align	4, 0x90
.LBB4_128:                              #   in Loop: Header=BB4_115 Depth=1
	movl	2128(%rcx), %ebp
	movl	%r14d, %ecx
	sarl	%cl, %ebp
	movq	272(%rsp), %rcx
	cmpl	$2, %esi
	jne	.LBB4_130
# BB#129:                               #   in Loop: Header=BB4_115 Depth=1
	movl	(%rcx), %ebx
	movq	56(%rsp), %rcx          # 8-byte Reload
	movswl	(%rcx), %r10d
	movswl	2(%rcx), %eax
	subq	$8, %rsp
.Lcfi194:
	.cfi_adjust_cfa_offset 8
	movl	$orig_pic, %edi
	xorl	%esi, %esi
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	48(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	96(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%rbx
.Lcfi195:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi196:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi197:
	.cfi_adjust_cfa_offset 8
	leaq	54(%rsp), %rbp
	pushq	%rbp
.Lcfi198:
	.cfi_adjust_cfa_offset 8
	leaq	60(%rsp), %rbp
	pushq	%rbp
.Lcfi199:
	.cfi_adjust_cfa_offset 8
	leaq	62(%rsp), %rbp
	pushq	%rbp
.Lcfi200:
	.cfi_adjust_cfa_offset 8
	leaq	68(%rsp), %rbp
	pushq	%rbp
.Lcfi201:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi202:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi203:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi204:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi205:
	.cfi_adjust_cfa_offset 8
	callq	smpUMHEXBipredIntegerPelBlockMotionSearch
	addq	$96, %rsp
.Lcfi206:
	.cfi_adjust_cfa_offset -96
	jmp	.LBB4_131
.LBB4_130:                              #   in Loop: Header=BB4_115 Depth=1
	movl	(%rcx), %r10d
	movswl	(%rbx), %eax
	movswl	2(%rbx), %ebx
	movq	176(%rsp), %rcx         # 8-byte Reload
	movswl	(%rcx), %r15d
	movswl	2(%rcx), %r12d
	subq	$8, %rsp
.Lcfi207:
	.cfi_adjust_cfa_offset 8
	movl	$orig_pic, %edi
	xorl	%esi, %esi
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	48(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	96(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%r10
.Lcfi208:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi209:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi210:
	.cfi_adjust_cfa_offset 8
	leaq	54(%rsp), %rbp
	pushq	%rbp
.Lcfi211:
	.cfi_adjust_cfa_offset 8
	leaq	60(%rsp), %rbp
	pushq	%rbp
.Lcfi212:
	.cfi_adjust_cfa_offset 8
	leaq	62(%rsp), %rbp
	pushq	%rbp
.Lcfi213:
	.cfi_adjust_cfa_offset 8
	leaq	68(%rsp), %rbp
	pushq	%rbp
.Lcfi214:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
	movq	192(%rsp), %r12         # 8-byte Reload
.Lcfi215:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	movq	272(%rsp), %r15         # 8-byte Reload
.Lcfi216:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi217:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi218:
	.cfi_adjust_cfa_offset 8
	callq	FullPelBlockMotionBiPred
	addq	$96, %rsp
.Lcfi219:
	.cfi_adjust_cfa_offset -96
	.p2align	4, 0x90
.LBB4_131:                              #   in Loop: Header=BB4_115 Depth=1
	movl	%eax, %r11d
	movzwl	20(%rsp), %ecx
	movw	%cx, 8(%rsp)
	movzwl	22(%rsp), %ecx
	movw	%cx, 10(%rsp)
	movq	input(%rip), %rcx
	cmpl	2124(%rcx), %r14d
	leal	1(%r14), %edx
	movl	%edx, %r14d
	jl	.LBB4_115
# BB#132:                               # %._crit_edge474.loopexit
	movl	5244(%rcx), %edx
	movzwl	160(%rsp), %eax         # 2-byte Folded Reload
	movw	%ax, %r15w
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	176(%rsp), %rax         # 8-byte Reload
	cmpl	$3, %edx
	je	.LBB4_134
	jmp	.LBB4_135
.LBB4_133:
	movl	$2147483647, %r11d      # imm = 0x7FFFFFFF
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	cmpl	$3, %edx
	jne	.LBB4_135
.LBB4_134:
	cmpl	$0, 4120(%rcx)
	jne	.LBB4_136
.LBB4_135:
	movzwl	20(%rsp), %esi
	shlw	$2, %si
	movw	%si, 8(%rsp)
	movzwl	22(%rsp), %esi
	shlw	$2, %si
	movw	%si, 10(%rsp)
	shlw	$2, 12(%rsp)
	shlw	$2, 14(%rsp)
.LBB4_136:
	cmpl	$0, 2132(%rcx)
	je	.LBB4_139
# BB#137:
	cmpl	$0, 24(%rcx)
	je	.LBB4_140
.LBB4_139:
	movq	%r15, %r14
.LBB4_144:
	movq	input(%rip), %rcx
	cmpl	$2, 2132(%rcx)
	movq	32(%rsp), %r15          # 8-byte Reload
	jne	.LBB4_150
# BB#145:
	cmpl	$0, 24(%rcx)
	jne	.LBB4_150
# BB#146:
	cmpl	$0, start_me_refinement_qp(%rip)
	movl	$2147483647, %edx       # imm = 0x7FFFFFFF
	cmovel	%edx, %r11d
	cmpl	$0, start_me_refinement_hp(%rip)
	cmovel	%edx, %r11d
	cmpl	$3, 5244(%rcx)
	jne	.LBB4_149
# BB#147:
	cmpl	$0, 4128(%rcx)
	je	.LBB4_149
# BB#148:
	movswl	%r14w, %edx
	xorl	$1, %edx
	leaq	12(%rsp), %rbp
	leaq	8(%rsp), %r10
	movl	$orig_pic, %edi
	xorl	%esi, %esi
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	88(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	272(%rsp)
.Lcfi220:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi221:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi222:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi223:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi224:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi225:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi226:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi227:
	.cfi_adjust_cfa_offset 8
	callq	EPZSSubPelBlockSearchBiPred
	addq	$64, %rsp
.Lcfi228:
	.cfi_adjust_cfa_offset -64
	cmpl	$0, 144(%rsp)           # 4-byte Folded Reload
	jg	.LBB4_151
	jmp	.LBB4_156
.LBB4_140:
	cmpl	$0, start_me_refinement_hp(%rip)
	movl	$2147483647, %esi       # imm = 0x7FFFFFFF
	cmovel	%esi, %r11d
	cmpl	$3, %edx
	jne	.LBB4_143
# BB#141:
	cmpl	$0, 4128(%rcx)
	je	.LBB4_143
# BB#142:
	movq	%r15, %r14
	movswl	%r15w, %edx
	leaq	8(%rsp), %r10
	movq	%rax, %rbp
	leaq	12(%rsp), %rax
	movl	$orig_pic, %edi
	xorl	%esi, %esi
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	88(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	272(%rsp)
.Lcfi229:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi230:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi231:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi232:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi233:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi234:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi235:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi236:
	.cfi_adjust_cfa_offset 8
	callq	EPZSSubPelBlockSearchBiPred
	addq	$64, %rsp
.Lcfi237:
	.cfi_adjust_cfa_offset -64
	movl	%eax, %r11d
	movq	%rbp, %rax
	jmp	.LBB4_144
.LBB4_143:
	movq	%r15, %r14
	movswl	%r15w, %edx
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	movswl	(%rax), %ebp
	movswl	2(%rax), %ebx
	leaq	8(%rsp), %r10
	movq	%rax, %r15
	movl	$orig_pic, %edi
	xorl	%esi, %esi
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	88(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	272(%rsp)
.Lcfi238:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi239:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi240:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi241:
	.cfi_adjust_cfa_offset 8
	leaq	42(%rsp), %rax
	pushq	%rax
.Lcfi242:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi243:
	.cfi_adjust_cfa_offset 8
	leaq	62(%rsp), %rax
	pushq	%rax
.Lcfi244:
	.cfi_adjust_cfa_offset 8
	leaq	68(%rsp), %rax
	pushq	%rax
.Lcfi245:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi246:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi247:
	.cfi_adjust_cfa_offset 8
	callq	SubPelBlockSearchBiPred
	movq	192(%rsp), %rbx         # 8-byte Reload
	addq	$80, %rsp
.Lcfi248:
	.cfi_adjust_cfa_offset -80
	movl	%eax, %r11d
	movq	%r15, %rax
	jmp	.LBB4_144
.LBB4_149:
	movswl	%r14w, %edx
	xorl	$1, %edx
	movq	%r11, %rax
	leaq	14(%rsp), %r11
	movswl	(%rbx), %ebp
	movswl	2(%rbx), %ebx
	leaq	12(%rsp), %r10
	leaq	8(%rsp), %r15
	movl	$orig_pic, %edi
	xorl	%esi, %esi
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	40(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	88(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	272(%rsp)
.Lcfi249:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi250:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi251:
	.cfi_adjust_cfa_offset 8
	pushq	$9
.Lcfi252:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi253:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi254:
	.cfi_adjust_cfa_offset 8
	leaq	58(%rsp), %rax
	pushq	%rax
.Lcfi255:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	movq	96(%rsp), %r15          # 8-byte Reload
.Lcfi256:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi257:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi258:
	.cfi_adjust_cfa_offset 8
	callq	SubPelBlockSearchBiPred
	addq	$80, %rsp
.Lcfi259:
	.cfi_adjust_cfa_offset -80
.LBB4_150:                              # %.preheader457
	cmpl	$0, 144(%rsp)           # 4-byte Folded Reload
	jle	.LBB4_156
.LBB4_151:                              # %.preheader.lr.ph
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	sarl	$2, %ecx
	movq	%rcx, %rax
	testl	%ecx, %ecx
	jle	.LBB4_156
# BB#152:                               # %.preheader.us.preheader
	movswq	%r14w, %r9
	movq	%r9, %rcx
	xorq	$1, %rcx
	addl	136(%rsp), %eax         # 4-byte Folded Reload
	movslq	%eax, %rdx
	movslq	48(%rsp), %r8           # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB4_153:                              # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_154 Depth 2
	movq	(%r15,%r12,8), %rdi
	movq	72(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_154:                              #   Parent Loop BB4_153 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	8(%rsp), %ebx
	movq	(%rdi,%rbp,8), %rsi
	movq	(%rsi,%r9,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movw	%bx, (%rax)
	movzwl	10(%rsp), %ebx
	movw	%bx, 2(%rax)
	movzwl	12(%rsp), %eax
	movq	(%rsi,%rcx,8), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%r13,8), %rsi
	movw	%ax, (%rsi)
	movzwl	14(%rsp), %eax
	movw	%ax, 2(%rsi)
	incq	%rbp
	cmpq	%rdx, %rbp
	jl	.LBB4_154
# BB#155:                               # %._crit_edge.us
                                        #   in Loop: Header=BB4_153 Depth=1
	incq	%r12
	cmpq	%r8, %r12
	jl	.LBB4_153
.LBB4_156:
	movl	$BlockMotionSearch.tstruct2, %edi
	callq	ftime
	movq	BlockMotionSearch.tstruct2(%rip), %rax
	movzwl	BlockMotionSearch.tstruct2+8(%rip), %ecx
	movzwl	BlockMotionSearch.tstruct1+8(%rip), %edx
	subq	BlockMotionSearch.tstruct1(%rip), %rax
	imulq	$1000, %rax, %rax       # imm = 0x3E8
	subq	%rdx, %rcx
	addq	%rax, %rcx
	addq	%rcx, me_tot_time(%rip)
	addq	%rcx, me_time(%rip)
	movl	104(%rsp), %eax         # 4-byte Reload
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	BlockMotionSearch, .Lfunc_end4-BlockMotionSearch
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_40
	.quad	.LBB4_44
	.quad	.LBB4_47
	.quad	.LBB4_57

	.text
	.globl	FindSkipModeMotionVector
	.p2align	4, 0x90
	.type	FindSkipModeMotionVector,@function
FindSkipModeMotionVector:               # @FindSkipModeMotionVector
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi260:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi261:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi262:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi263:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi264:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi265:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi266:
	.cfi_def_cfa_offset 112
.Lcfi267:
	.cfi_offset %rbx, -56
.Lcfi268:
	.cfi_offset %r12, -48
.Lcfi269:
	.cfi_offset %r13, -40
.Lcfi270:
	.cfi_offset %r14, -32
.Lcfi271:
	.cfi_offset %r15, -24
.Lcfi272:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movq	14384(%rax), %rbx
	movq	enc_picture(%rip), %rcx
	movq	6512(%rcx), %rcx
	movq	(%rcx), %r14
	movq	14224(%rax), %r13
	movl	12(%rax), %eax
	movslq	%eax, %r15
	xorl	%r12d, %r12d
	leaq	32(%rsp), %rcx
	movl	$-1, %esi
	xorl	%edx, %edx
	movl	%r15d, %edi
	callq	getLuma4x4Neighbour
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	leaq	8(%rsp), %rcx
	xorl	%esi, %esi
	movl	$-1, %edx
	callq	getLuma4x4Neighbour
	movl	32(%rsp), %r9d
	testl	%r9d, %r9d
	movl	$0, %r8d
	movl	$0, %edx
	je	.LBB5_6
# BB#1:
	movslq	52(%rsp), %rcx
	imulq	$536, %r15, %rdx        # imm = 0x218
	movslq	36(%rsp), %rsi
	imulq	$536, %rsi, %rsi        # imm = 0x218
	cmpl	$0, 424(%r13,%rdx)
	movq	(%r14,%rcx,8), %rdx
	movq	enc_picture(%rip), %rdi
	movq	6488(%rdi), %rdi
	movq	(%rdi), %rdi
	movq	(%rdi,%rcx,8), %rdi
	movslq	48(%rsp), %rbp
	movq	(%rdx,%rbp,8), %rcx
	movswl	2(%rcx), %r8d
	movsbl	(%rdi,%rbp), %edx
	movq	img(%rip), %rdi
	movq	14224(%rdi), %rdi
	movl	424(%rdi,%rsi), %esi
	je	.LBB5_4
# BB#2:
	testl	%esi, %esi
	jne	.LBB5_6
# BB#3:
	movl	%r8d, %esi
	shrl	$31, %esi
	addl	%r8d, %esi
	sarl	%esi
	addl	%edx, %edx
	movl	%esi, %r8d
	jmp	.LBB5_6
.LBB5_4:                                # %.thread
	testl	%esi, %esi
	je	.LBB5_6
# BB#5:
	addl	%r8d, %r8d
	sarl	%edx
.LBB5_6:
	movl	8(%rsp), %esi
	testl	%esi, %esi
	movl	$0, %edi
	je	.LBB5_12
# BB#7:
	movslq	28(%rsp), %rdi
	movq	(%r14,%rdi,8), %rbp
	movslq	24(%rsp), %rcx
	movq	(%rbp,%rcx,8), %rbp
	movswl	2(%rbp), %r12d
	movq	enc_picture(%rip), %rbp
	movq	6488(%rbp), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%rdi,8), %rdi
	imulq	$536, %r15, %rbp        # imm = 0x218
	movslq	12(%rsp), %rax
	imulq	$536, %rax, %rax        # imm = 0x218
	cmpl	$0, 424(%r13,%rbp)
	movsbl	(%rdi,%rcx), %edi
	movq	img(%rip), %rcx
	movq	14224(%rcx), %rcx
	movl	424(%rcx,%rax), %ebp
	je	.LBB5_10
# BB#8:
	testl	%ebp, %ebp
	jne	.LBB5_12
# BB#9:
	movl	%r12d, %eax
	shrl	$31, %eax
	addl	%r12d, %eax
	sarl	%eax
	addl	%edi, %edi
	movl	%eax, %r12d
	testl	%r9d, %r9d
	jne	.LBB5_14
	jmp	.LBB5_13
.LBB5_10:                               # %.thread72
	testl	%ebp, %ebp
	je	.LBB5_12
# BB#11:
	addl	%r12d, %r12d
	sarl	%edi
.LBB5_12:
	testl	%r9d, %r9d
	je	.LBB5_13
.LBB5_14:
	testl	%edx, %edx
	je	.LBB5_16
# BB#15:
	xorl	%eax, %eax
	jmp	.LBB5_17
.LBB5_13:
	movl	$1, %ecx
	jmp	.LBB5_18
.LBB5_16:
	movslq	52(%rsp), %rax
	movq	(%r14,%rax,8), %rax
	movslq	48(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	cmpw	$0, (%rax)
	sete	%cl
	testl	%r8d, %r8d
	sete	%al
	andb	%cl, %al
.LBB5_17:
	movzbl	%al, %ecx
.LBB5_18:
	xorl	%eax, %eax
	testl	%esi, %esi
	je	.LBB5_24
# BB#19:
	testl	%edi, %edi
	movl	$0, %edx
	jne	.LBB5_21
# BB#20:
	movslq	28(%rsp), %rdx
	movq	(%r14,%rdx,8), %rdx
	movslq	24(%rsp), %rsi
	movq	(%rdx,%rsi,8), %rdx
	cmpw	$0, (%rdx)
	sete	%sil
	testl	%r12d, %r12d
	sete	%dl
	andb	%sil, %dl
.LBB5_21:
	testl	%ecx, %ecx
	jne	.LBB5_24
# BB#22:
	testb	%dl, %dl
	jne	.LBB5_24
# BB#23:                                # %.preheader77
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rax
	movq	(%rax), %rsi
	subq	$8, %rsp
.Lcfi273:
	.cfi_adjust_cfa_offset 8
	leaq	12(%rsp), %rdi
	movl	$0, %ecx
	movl	$0, %r9d
	movq	%r14, %rdx
	pushq	$16
.Lcfi274:
	.cfi_adjust_cfa_offset 8
	pushq	$16
.Lcfi275:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi276:
	.cfi_adjust_cfa_offset 8
	callq	SetMotionVectorPredictor
	addq	$32, %rsp
.Lcfi277:
	.cfi_adjust_cfa_offset -32
	movl	4(%rsp), %eax
.LBB5_24:                               # %.loopexit
	movq	(%rbx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	%eax, (%rcx)
	movq	(%rbx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	%eax, (%rcx)
	movq	(%rbx), %rcx
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	%eax, (%rcx)
	movq	(%rbx), %rcx
	movq	24(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	%eax, (%rcx)
	movq	8(%rbx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	%eax, (%rcx)
	movq	8(%rbx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	%eax, (%rcx)
	movq	8(%rbx), %rcx
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	%eax, (%rcx)
	movq	8(%rbx), %rcx
	movq	24(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	%eax, (%rcx)
	movq	16(%rbx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	%eax, (%rcx)
	movq	16(%rbx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	%eax, (%rcx)
	movq	16(%rbx), %rcx
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	%eax, (%rcx)
	movq	16(%rbx), %rcx
	movq	24(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	%eax, (%rcx)
	movq	24(%rbx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	%eax, (%rcx)
	movq	24(%rbx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	%eax, (%rcx)
	movq	24(%rbx), %rcx
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	%eax, (%rcx)
	movq	24(%rbx), %rcx
	movq	24(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	%eax, (%rcx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	FindSkipModeMotionVector, .Lfunc_end5-FindSkipModeMotionVector
	.cfi_endproc

	.globl	GetSkipCostMB
	.p2align	4, 0x90
	.type	GetSkipCostMB,@function
GetSkipCostMB:                          # @GetSkipCostMB
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi278:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi279:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi280:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi281:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi282:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi283:
	.cfi_def_cfa_offset 56
	subq	$328, %rsp              # imm = 0x148
.Lcfi284:
	.cfi_def_cfa_offset 384
.Lcfi285:
	.cfi_offset %rbx, -56
.Lcfi286:
	.cfi_offset %r12, -48
.Lcfi287:
	.cfi_offset %r13, -40
.Lcfi288:
	.cfi_offset %r14, -32
.Lcfi289:
	.cfi_offset %r15, -24
.Lcfi290:
	.cfi_offset %rbp, -16
	xorl	%edx, %edx
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB6_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_2 Depth 2
                                        #       Child Loop BB6_3 Depth 3
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	leal	(,%rax,4), %ecx
	andl	$-8, %ecx
	andl	$536870910, %eax        # imm = 0x1FFFFFFE
	movl	%edx, 16(%rsp)          # 4-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	subl	%eax, %edx
	leal	(,%rdx,8), %eax
	movslq	%eax, %rsi
	movslq	%ecx, %rbx
	leal	4(,%rdx,8), %eax
	cltq
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leal	4(%rbx), %eax
	cltq
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rbx, %rax
	shlq	$4, %rax
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	leaq	6360(%rax,%rsi), %r15
	leaq	160(%rsp), %r13
	.p2align	4, 0x90
.LBB6_2:                                #   Parent Loop BB6_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_3 Depth 3
	movq	img(%rip), %rax
	movslq	196(%rax), %rcx
	movslq	%ebx, %r12
	addq	%rcx, %r12
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB6_3
	.p2align	4, 0x90
.LBB6_7:                                # %._crit_edge
                                        #   in Loop: Header=BB6_3 Depth=3
	addq	$4, %rbp
	movq	img(%rip), %rax
	addq	$4, %r15
	addq	$16, %r13
.LBB6_3:                                # %.preheader
                                        #   Parent Loop BB6_1 Depth=1
                                        #     Parent Loop BB6_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	192(%rax), %r14d
	addl	%ebp, %r14d
	movl	$0, (%rsp)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	%ebp, %edi
	movl	%ebx, %esi
	callq	LumaPrediction4x4
	movq	imgY_org(%rip), %rcx
	movq	img(%rip), %rax
	movslq	%r14d, %rdx
	movq	(%rcx,%r12,8), %rsi
	movq	(%rsi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	pxor	%xmm2, %xmm2
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	-96(%rax,%r15,2), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, -96(%r13)
	movdqa	%xmm0, diff(%rip)
	movq	8(%rcx,%r12,8), %rsi
	movq	(%rsi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	-64(%rax,%r15,2), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, -64(%r13)
	movdqa	%xmm0, diff+16(%rip)
	movq	16(%rcx,%r12,8), %rsi
	movq	(%rsi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	-32(%rax,%r15,2), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, -32(%r13)
	movdqa	%xmm0, diff+32(%rip)
	movq	24(%rcx,%r12,8), %rcx
	movq	(%rcx,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rax,%r15,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, (%r13)
	movdqa	%xmm0, diff+48(%rip)
	movq	input(%rip), %rax
	cmpl	$0, 4168(%rax)
	je	.LBB6_4
.LBB6_5:                                #   in Loop: Header=BB6_3 Depth=3
	movl	$diff, %edi
	callq	distortion4x4
	addl	%eax, 20(%rsp)          # 4-byte Folded Spill
	cmpq	56(%rsp), %rbp          # 8-byte Folded Reload
	jl	.LBB6_7
	jmp	.LBB6_8
	.p2align	4, 0x90
.LBB6_4:                                #   in Loop: Header=BB6_3 Depth=3
	cmpl	$0, 5100(%rax)
	je	.LBB6_5
# BB#6:                                 #   in Loop: Header=BB6_3 Depth=3
	cmpq	56(%rsp), %rbp          # 8-byte Folded Reload
	jl	.LBB6_7
.LBB6_8:                                #   in Loop: Header=BB6_2 Depth=2
	movq	48(%rsp), %r15          # 8-byte Reload
	addq	$64, %r15
	movq	40(%rsp), %r13          # 8-byte Reload
	subq	$-128, %r13
	cmpq	24(%rsp), %rbx          # 8-byte Folded Reload
	leaq	4(%rbx), %rbx
	jl	.LBB6_2
# BB#9:                                 #   in Loop: Header=BB6_1 Depth=1
	movq	input(%rip), %rax
	cmpl	$0, 4168(%rax)
	jne	.LBB6_12
# BB#10:                                #   in Loop: Header=BB6_1 Depth=1
	cmpl	$0, 5100(%rax)
	je	.LBB6_12
# BB#11:                                # %.preheader75.preheader
                                        #   in Loop: Header=BB6_1 Depth=1
	movaps	64(%rsp), %xmm0
	movaps	80(%rsp), %xmm1
	movaps	%xmm1, diff64+16(%rip)
	movaps	%xmm0, diff64(%rip)
	leaq	96(%rsp), %rax
	movaps	(%rax), %xmm0
	movaps	16(%rax), %xmm1
	movaps	%xmm1, diff64+48(%rip)
	movaps	%xmm0, diff64+32(%rip)
	movaps	32(%rax), %xmm0
	movaps	48(%rax), %xmm1
	movaps	%xmm1, diff64+80(%rip)
	movaps	%xmm0, diff64+64(%rip)
	leaq	160(%rsp), %rcx
	movaps	(%rcx), %xmm0
	movaps	16(%rcx), %xmm1
	movaps	%xmm1, diff64+112(%rip)
	movaps	%xmm0, diff64+96(%rip)
	movaps	96(%rax), %xmm0
	movaps	112(%rax), %xmm1
	movaps	%xmm1, diff64+144(%rip)
	movaps	%xmm0, diff64+128(%rip)
	movaps	128(%rax), %xmm0
	movaps	144(%rax), %xmm1
	movaps	%xmm1, diff64+176(%rip)
	movaps	%xmm0, diff64+160(%rip)
	movaps	160(%rax), %xmm0
	movaps	176(%rax), %xmm1
	movaps	%xmm1, diff64+208(%rip)
	movaps	%xmm0, diff64+192(%rip)
	movdqa	192(%rax), %xmm0
	movdqa	208(%rax), %xmm1
	movdqa	%xmm1, diff64+240(%rip)
	movdqa	%xmm0, diff64+224(%rip)
	movl	$diff64, %edi
	callq	distortion8x8
	addl	%eax, 20(%rsp)          # 4-byte Folded Spill
.LBB6_12:                               #   in Loop: Header=BB6_1 Depth=1
	movl	16(%rsp), %edx          # 4-byte Reload
	incl	%edx
	cmpl	$4, %edx
	jne	.LBB6_1
# BB#13:
	movl	20(%rsp), %eax          # 4-byte Reload
	addq	$328, %rsp              # imm = 0x148
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	GetSkipCostMB, .Lfunc_end6-GetSkipCostMB
	.cfi_endproc

	.globl	BIDPartitionCost
	.p2align	4, 0x90
	.type	BIDPartitionCost,@function
BIDPartitionCost:                       # @BIDPartitionCost
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi291:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi292:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi293:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi294:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi295:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi296:
	.cfi_def_cfa_offset 56
	subq	$1192, %rsp             # imm = 0x4A8
.Lcfi297:
	.cfi_def_cfa_offset 1248
.Lcfi298:
	.cfi_offset %rbx, -56
.Lcfi299:
	.cfi_offset %r12, -48
.Lcfi300:
	.cfi_offset %r13, -40
.Lcfi301:
	.cfi_offset %r14, -32
.Lcfi302:
	.cfi_offset %r15, -24
.Lcfi303:
	.cfi_offset %rbp, -16
	movl	%ecx, 88(%rsp)          # 4-byte Spill
	movl	%edx, 80(%rsp)          # 4-byte Spill
	movq	input(%rip), %rcx
	movslq	%edi, %r9
	movl	72(%rcx,%r9,8), %eax
	cmpl	$9, %eax
	movl	$8, %edx
	cmovgel	%edx, %eax
	movl	%eax, 148(%rsp)         # 4-byte Spill
	movl	76(%rcx,%r9,8), %eax
	cmpl	$9, %eax
	cmovll	%eax, %edx
	movl	%edx, 144(%rsp)         # 4-byte Spill
	cmpl	$5, %r9d
	movl	$4, %eax
	movl	%edi, 24(%rsp)          # 4-byte Spill
	cmovll	%edi, %eax
	movslq	%eax, %r15
	movslq	140(%rcx,%r15,8), %rax
	testq	%rax, %rax
	jle	.LBB7_1
# BB#2:                                 # %.preheader196.lr.ph
	movslq	136(%rcx,%r15,8), %rdi
	movslq	%esi, %r10
	movq	%r15, %rsi
	shlq	$4, %rsi
	movslq	PartitionMotionSearch.by0(%rsi,%r10,4), %rbx
	leaq	(%rbx,%rax), %rbp
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movslq	PartitionMotionSearch.bx0(%rsi,%r10,4), %r11
	movq	img(%rip), %rsi
	testl	%edi, %edi
	movq	%r15, 128(%rsp)         # 8-byte Spill
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	jle	.LBB7_3
# BB#4:                                 # %.preheader196.us.preheader
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movl	%r8d, 136(%rsp)         # 4-byte Spill
	movslq	136(%rcx,%r9,8), %rax
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movslq	140(%rcx,%r9,8), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	14376(%rsi), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	14384(%rsi), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	mvbits(%rip), %rsi
	movswq	80(%rsp), %r10          # 2-byte Folded Reload
	movswq	88(%rsp), %r12          # 2-byte Folded Reload
	movq	%r11, 40(%rsp)          # 8-byte Spill
	leal	(%r11,%rdi), %ecx
	movslq	%ecx, %r8
	xorl	%ebp, %ebp
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB7_5:                                # %.preheader196.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_6 Depth 2
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	movq	(%rcx,%rdi,8), %r13
	movq	40(%rsp), %r11          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_6:                                #   Parent Loop BB7_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r11,8), %r14
	movq	(%r14), %r15
	movq	(%r15,%r10,8), %rbx
	movq	(%rbx,%r9,8), %rbx
	movswq	(%rbx), %rcx
	movq	(%r13,%r11,8), %rdi
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%r9,8), %rax
	movswq	(%rax), %rdx
	subq	%rdx, %rcx
	addl	(%rsi,%rcx,4), %ebp
	movq	8(%r14), %rcx
	movq	8(%rdi), %rdx
	movswq	2(%rbx), %rdi
	movswq	2(%rax), %rax
	subq	%rax, %rdi
	addl	(%rsi,%rdi,4), %ebp
	movq	(%rcx,%r12,8), %rax
	movq	(%rax,%r9,8), %rax
	movswq	(%rax), %rcx
	movq	(%rdx,%r12,8), %rdx
	movq	(%rdx,%r9,8), %rdx
	movswq	(%rdx), %rdi
	subq	%rdi, %rcx
	addl	(%rsi,%rcx,4), %ebp
	movswq	2(%rax), %rax
	movswq	2(%rdx), %rcx
	subq	%rcx, %rax
	addl	(%rsi,%rax,4), %ebp
	movq	%r15, %rax
	addq	%rax, %r11
	cmpq	%r8, %r11
	jl	.LBB7_6
# BB#7:                                 # %._crit_edge227.us
                                        #   in Loop: Header=BB7_5 Depth=1
	movq	112(%rsp), %rdi         # 8-byte Reload
	addq	72(%rsp), %rdi          # 8-byte Folded Reload
	cmpq	16(%rsp), %rdi          # 8-byte Folded Reload
	jl	.LBB7_5
# BB#8:                                 # %._crit_edge231
	imull	136(%rsp), %ebp         # 4-byte Folded Reload
	sarl	$16, %ebp
	cmpl	$0, 152(%rsp)           # 4-byte Folded Reload
	movq	128(%rsp), %r15         # 8-byte Reload
	movq	120(%rsp), %rdi         # 8-byte Reload
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	40(%rsp), %r11          # 8-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	jg	.LBB7_9
	jmp	.LBB7_24
.LBB7_1:
	xorl	%ebp, %ebp
	cmpl	$4, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB7_25
	jmp	.LBB7_32
.LBB7_3:
	xorl	%ebp, %ebp
.LBB7_9:                                # %.lr.ph221
	movslq	16(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 136(%rsp)         # 8-byte Spill
	leaq	352(%rsp), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leal	(,%r11,4), %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	(%r11,%rdi), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	incq	%r11
	movq	%r11, 40(%rsp)          # 8-byte Spill
	testl	%edi, %edi
	jg	.LBB7_11
	jmp	.LBB7_21
	.p2align	4, 0x90
.LBB7_22:                               # %._crit_edge213._crit_edge
                                        #   in Loop: Header=BB7_21 Depth=1
	movq	img(%rip), %rsi
	addq	$256, 72(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x100
	testl	%edi, %edi
	jle	.LBB7_21
.LBB7_11:                               # %.lr.ph
	movslq	196(%rsi), %r15
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	leal	(,%rbx,4), %eax
	cltq
	addq	%rax, %r15
	cmpl	$4, 24(%rsp)            # 4-byte Folded Reload
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jle	.LBB7_12
# BB#17:                                # %.lr.ph.split.us.preheader
	movslq	%r15d, %r14
	movq	%rax, %rcx
	orq	$1, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rax, %rcx
	orq	$2, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	orq	$3, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%r11, %r12
	xorl	%r15d, %r15d
	jmp	.LBB7_19
	.p2align	4, 0x90
.LBB7_18:                               # %..lr.ph.split.us_crit_edge
                                        #   in Loop: Header=BB7_19 Depth=1
	addq	$4, %r15
	movq	img(%rip), %rsi
	incq	%r12
.LBB7_19:                               # %.preheader195.us
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, 16(%rsp)          # 4-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r15), %rbx
	movl	192(%rsi), %ebp
	addl	%ebx, %ebp
	movswl	88(%rsp), %eax          # 2-byte Folded Reload
	movl	%eax, (%rsp)
	movswl	80(%rsp), %r9d          # 2-byte Folded Reload
	movl	$2, %edx
	movl	%ebx, %edi
	movq	32(%rsp), %r13          # 8-byte Reload
	movl	%r13d, %esi
	movl	24(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %r8d
	callq	LumaPrediction4x4
	movq	imgY_org(%rip), %rsi
	movq	img(%rip), %rcx
	movslq	%ebp, %rdx
	movslq	%ebx, %rax
	movq	(%rsi,%r14,8), %rdi
	shlq	$5, %r13
	leaq	12624(%rcx,%r13), %rbp
	movq	(%rdi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	pxor	%xmm2, %xmm2
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rbp,%rax,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movq	72(%rsp), %rbx          # 8-byte Reload
	movdqa	%xmm0, -192(%rbx,%r15,4)
	movdqa	%xmm0, diff64(%rip)
	movq	8(%rsi,%r14,8), %rdi
	movq	64(%rsp), %rbp          # 8-byte Reload
	shlq	$5, %rbp
	leaq	12624(%rcx,%rbp), %rbp
	movq	(%rdi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rbp,%rax,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, -128(%rbx,%r15,4)
	movdqa	%xmm0, diff64+16(%rip)
	movq	16(%rsi,%r14,8), %rdi
	movq	56(%rsp), %rbp          # 8-byte Reload
	shlq	$5, %rbp
	leaq	12624(%rcx,%rbp), %rbp
	movq	(%rdi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rbp,%rax,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, -64(%rbx,%r15,4)
	movdqa	%xmm0, diff64+32(%rip)
	movq	24(%rsi,%r14,8), %rsi
	movq	48(%rsp), %rdi          # 8-byte Reload
	shlq	$5, %rdi
	leaq	12624(%rcx,%rdi), %rcx
	movq	(%rsi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rcx,%rax,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, (%rbx,%r15,4)
	movdqa	%xmm0, diff64+48(%rip)
	movl	$diff64, %edi
	callq	distortion4x4
	movl	%eax, %ebp
	addl	16(%rsp), %ebp          # 4-byte Folded Reload
	cmpq	112(%rsp), %r12         # 8-byte Folded Reload
	jl	.LBB7_18
	jmp	.LBB7_20
	.p2align	4, 0x90
.LBB7_12:                               # %.lr.ph.split.preheader
	movq	%rax, %rcx
	orq	$1, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rax, %rcx
	orq	$2, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	orq	$3, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	%eax, %r13d
	movq	72(%rsp), %r14          # 8-byte Reload
	jmp	.LBB7_13
	.p2align	4, 0x90
.LBB7_16:                               # %..lr.ph.split_crit_edge
                                        #   in Loop: Header=BB7_13 Depth=1
	movq	img(%rip), %rsi
	addq	$16, %r14
	addl	$4, %r13d
	incq	%rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB7_13:                               # %.preheader195
                                        # =>This Inner Loop Header: Depth=1
	movl	192(%rsi), %ebx
	addl	%r13d, %ebx
	movswl	88(%rsp), %eax          # 2-byte Folded Reload
	movl	%eax, (%rsp)
	movswl	80(%rsp), %r9d          # 2-byte Folded Reload
	movl	$2, %edx
	movl	%r13d, %edi
	movq	32(%rsp), %r12          # 8-byte Reload
	movl	%r12d, %esi
	movl	24(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %r8d
	callq	LumaPrediction4x4
	movq	imgY_org(%rip), %rsi
	movq	img(%rip), %rcx
	movslq	%r13d, %rax
	movslq	%ebx, %rdx
	movq	(%rsi,%r15,8), %rdi
	shlq	$5, %r12
	leaq	12624(%rcx,%r12), %rbx
	movq	(%rdi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	pxor	%xmm2, %xmm2
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rbx,%rax,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, -192(%r14)
	movdqa	%xmm0, diff64(%rip)
	movq	8(%rsi,%r15,8), %rdi
	movq	64(%rsp), %rbx          # 8-byte Reload
	shlq	$5, %rbx
	leaq	12624(%rcx,%rbx), %rbx
	movq	(%rdi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rbx,%rax,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, -128(%r14)
	movdqa	%xmm0, diff64+16(%rip)
	movq	16(%rsi,%r15,8), %rdi
	movq	56(%rsp), %rbx          # 8-byte Reload
	shlq	$5, %rbx
	leaq	12624(%rcx,%rbx), %rbx
	movq	(%rdi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rbx,%rax,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, -64(%r14)
	movdqa	%xmm0, diff64+32(%rip)
	movq	24(%rsi,%r15,8), %rsi
	movq	48(%rsp), %rdi          # 8-byte Reload
	shlq	$5, %rdi
	leaq	12624(%rcx,%rdi), %rcx
	movq	(%rsi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rcx,%rax,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, (%r14)
	movdqa	%xmm0, diff64+48(%rip)
	movq	input(%rip), %rax
	cmpl	$0, 5100(%rax)
	jne	.LBB7_15
# BB#14:                                #   in Loop: Header=BB7_13 Depth=1
	movl	$diff64, %edi
	callq	distortion4x4
	addl	%eax, %ebp
.LBB7_15:                               #   in Loop: Header=BB7_13 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	112(%rsp), %rax         # 8-byte Folded Reload
	jl	.LBB7_16
.LBB7_20:
	movq	128(%rsp), %r15         # 8-byte Reload
	movq	120(%rsp), %rdi         # 8-byte Reload
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	40(%rsp), %r11          # 8-byte Reload
.LBB7_21:                               # %._crit_edge213
                                        # =>This Inner Loop Header: Depth=1
	incq	%rbx
	cmpq	136(%rsp), %rbx         # 8-byte Folded Reload
	jl	.LBB7_22
# BB#23:                                # %._crit_edge222.loopexit
	movq	input(%rip), %rcx
.LBB7_24:                               # %._crit_edge222
	cmpl	$4, 24(%rsp)            # 4-byte Folded Reload
	jg	.LBB7_32
.LBB7_25:                               # %._crit_edge222
	movl	5100(%rcx), %eax
	testl	%eax, %eax
	je	.LBB7_32
# BB#26:                                # %.preheader194
	cmpl	$0, 76(%rcx,%r15,8)
	jle	.LBB7_32
# BB#27:                                # %.preheader193.preheader
	movl	144(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movslq	%eax, %rsi
	movl	148(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movslq	%eax, %r12
	leaq	-1(%rsi), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	shlq	$6, %rsi
	addq	$-64, %rsi
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	leaq	-4(,%r12,4), %rbx
	decq	%r12
	leaq	160(%rsp), %r13
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB7_28:                               # %.preheader193
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_30 Depth 2
	cmpl	$0, 72(%rcx,%r15,8)
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	jle	.LBB7_31
# BB#29:                                # %.preheader
                                        #   in Loop: Header=BB7_28 Depth=1
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_30:                               #   Parent Loop BB7_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r13), %xmm0
	movups	16(%r13), %xmm1
	movaps	%xmm1, diff64+16(%rip)
	movaps	%xmm0, diff64(%rip)
	movups	64(%r13), %xmm0
	movups	80(%r13), %xmm1
	movaps	%xmm1, diff64+48(%rip)
	movaps	%xmm0, diff64+32(%rip)
	movups	128(%r13), %xmm0
	movups	144(%r13), %xmm1
	movaps	%xmm1, diff64+80(%rip)
	movaps	%xmm0, diff64+64(%rip)
	movups	192(%r13), %xmm0
	movups	208(%r13), %xmm1
	movaps	%xmm1, diff64+112(%rip)
	movaps	%xmm0, diff64+96(%rip)
	movups	256(%r13), %xmm0
	movups	272(%r13), %xmm1
	movaps	%xmm1, diff64+144(%rip)
	movaps	%xmm0, diff64+128(%rip)
	movups	320(%r13), %xmm0
	movups	336(%r13), %xmm1
	movaps	%xmm1, diff64+176(%rip)
	movaps	%xmm0, diff64+160(%rip)
	movups	384(%r13), %xmm0
	movups	400(%r13), %xmm1
	movaps	%xmm1, diff64+208(%rip)
	movaps	%xmm0, diff64+192(%rip)
	movdqu	448(%r13), %xmm0
	movdqu	464(%r13), %xmm1
	movdqa	%xmm1, diff64+240(%rip)
	movdqa	%xmm0, diff64+224(%rip)
	movl	$diff64, %edi
	callq	distortion8x8
	addl	%eax, %ebp
	addq	%r12, %r14
	movq	input(%rip), %rcx
	movslq	72(%rcx,%r15,8), %rax
	addq	%rbx, %r13
	cmpq	%rax, %r14
	jl	.LBB7_30
.LBB7_31:                               # %._crit_edge
                                        #   in Loop: Header=BB7_28 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	addq	80(%rsp), %rdi          # 8-byte Folded Reload
	movslq	76(%rcx,%r15,8), %rax
	movq	32(%rsp), %r13          # 8-byte Reload
	addq	88(%rsp), %r13          # 8-byte Folded Reload
	cmpq	%rax, %rdi
	jl	.LBB7_28
.LBB7_32:                               # %.loopexit
	movl	%ebp, %eax
	addq	$1192, %rsp             # imm = 0x4A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	BIDPartitionCost, .Lfunc_end7-BIDPartitionCost
	.cfi_endproc

	.globl	GetDirectCost8x8
	.p2align	4, 0x90
	.type	GetDirectCost8x8,@function
GetDirectCost8x8:                       # @GetDirectCost8x8
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi304:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi305:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi306:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi307:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi308:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi309:
	.cfi_def_cfa_offset 56
	subq	$360, %rsp              # imm = 0x168
.Lcfi310:
	.cfi_def_cfa_offset 416
.Lcfi311:
	.cfi_offset %rbx, -56
.Lcfi312:
	.cfi_offset %r12, -48
.Lcfi313:
	.cfi_offset %r13, -40
.Lcfi314:
	.cfi_offset %r14, -32
.Lcfi315:
	.cfi_offset %r15, -24
.Lcfi316:
	.cfi_offset %rbp, -16
	movq	%rsi, 24(%rsp)          # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movl	%edi, %eax
	shrl	$31, %eax
	addl	%edi, %eax
	leal	(,%rax,4), %ecx
	andl	$-8, %ecx
	andl	$536870910, %eax        # imm = 0x1FFFFFFE
	subl	%eax, %edi
	leal	(,%rdi,8), %edx
	movslq	%edx, %r12
	movslq	%ecx, %r10
	leal	4(,%rdi,8), %eax
	cltq
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leal	4(%r10), %eax
	cltq
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%r10, %rax
	shlq	$4, %rax
	leaq	6360(%rax,%r12), %r15
	leaq	192(%rsp), %r14
	xorl	%r13d, %r13d
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movq	%r12, 48(%rsp)          # 8-byte Spill
.LBB8_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_3 Depth 2
	movq	img(%rip), %rax
	movslq	196(%rax), %rcx
	movslq	%r10d, %rsi
	addq	%rcx, %rsi
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	movl	%esi, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rbp
	movl	192(%rax), %r9d
	addl	%edx, %r9d
	movq	direct_pdir(%rip), %rax
	movq	(%rax,%rbp,8), %rcx
	movl	%r9d, %eax
	sarl	$2, %eax
	cltq
	movb	(%rcx,%rax), %cl
	testb	%cl, %cl
	movq	%r14, 56(%rsp)          # 8-byte Spill
	movq	%r15, 64(%rsp)          # 8-byte Spill
	movl	%edx, %ebx
	movq	%r10, 32(%rsp)          # 8-byte Spill
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	js	.LBB8_2
	.p2align	4, 0x90
.LBB8_3:                                # %.preheader79
                                        #   Parent Loop BB8_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r13d, 20(%rsp)         # 4-byte Spill
	movsbl	%cl, %edx
	movq	direct_ref_idx(%rip), %rcx
	movq	(%rcx), %rsi
	movq	8(%rcx), %rcx
	movq	(%rsi,%rbp,8), %rsi
	movq	(%rcx,%rbp,8), %rcx
	movl	%r9d, %ebp
	movsbl	(%rsi,%rax), %r9d
	movsbl	(%rcx,%rax), %eax
	movl	%eax, (%rsp)
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	%ebx, %edi
	movl	%r10d, %esi
	callq	LumaPrediction4x4
	movq	imgY_org(%rip), %rcx
	movq	img(%rip), %rax
	movslq	%ebp, %rdx
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	(%rcx,%rdi,8), %rsi
	movq	(%rsi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	pxor	%xmm2, %xmm2
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	-96(%rax,%r15,2), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, -96(%r14)
	movdqa	%xmm0, diff(%rip)
	movq	8(%rcx,%rdi,8), %rsi
	movq	(%rsi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	-64(%rax,%r15,2), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, -64(%r14)
	movdqa	%xmm0, diff+16(%rip)
	movq	16(%rcx,%rdi,8), %rsi
	movq	(%rsi,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	-32(%rax,%r15,2), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, -32(%r14)
	movdqa	%xmm0, diff+32(%rip)
	movq	24(%rcx,%rdi,8), %rcx
	movq	(%rcx,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rax,%r15,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, (%r14)
	movdqa	%xmm0, diff+48(%rip)
	movl	$diff, %edi
	callq	distortion4x4
	movl	%eax, %r13d
	addl	20(%rsp), %r13d         # 4-byte Folded Reload
	cmpq	80(%rsp), %r12          # 8-byte Folded Reload
	jge	.LBB8_5
# BB#4:                                 # %._crit_edge
                                        #   in Loop: Header=BB8_3 Depth=2
	addq	$4, %r12
	movq	img(%rip), %rax
	movl	192(%rax), %eax
	leal	(%r12,%rax), %r9d
	leal	4(%rbx,%rax), %eax
	movq	direct_pdir(%rip), %rcx
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	(%rcx,%rbp,8), %rcx
	sarl	$2, %eax
	cltq
	movzbl	(%rcx,%rax), %ecx
	addl	$4, %ebx
	addq	$4, %r15
	addq	$16, %r14
	testb	%cl, %cl
	movq	32(%rsp), %r10          # 8-byte Reload
	jns	.LBB8_3
	jmp	.LBB8_2
	.p2align	4, 0x90
.LBB8_5:                                #   in Loop: Header=BB8_1 Depth=1
	movq	64(%rsp), %r15          # 8-byte Reload
	addq	$64, %r15
	movq	56(%rsp), %r14          # 8-byte Reload
	subq	$-128, %r14
	movq	32(%rsp), %r10          # 8-byte Reload
	cmpq	40(%rsp), %r10          # 8-byte Folded Reload
	leaq	4(%r10), %r10
	movl	16(%rsp), %edx          # 4-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	jl	.LBB8_1
# BB#6:
	movq	input(%rip), %rax
	cmpl	$0, 4168(%rax)
	jne	.LBB8_9
# BB#7:
	cmpl	$0, 5100(%rax)
	je	.LBB8_9
# BB#8:                                 # %.preheader.preheader
	movaps	96(%rsp), %xmm0
	movaps	112(%rsp), %xmm1
	movaps	%xmm1, diff64+16(%rip)
	movaps	%xmm0, diff64(%rip)
	movaps	128(%rsp), %xmm0
	movaps	144(%rsp), %xmm1
	movaps	%xmm1, diff64+48(%rip)
	movaps	%xmm0, diff64+32(%rip)
	movaps	160(%rsp), %xmm0
	movaps	176(%rsp), %xmm1
	movaps	%xmm1, diff64+80(%rip)
	movaps	%xmm0, diff64+64(%rip)
	movaps	192(%rsp), %xmm0
	movaps	208(%rsp), %xmm1
	movaps	%xmm1, diff64+112(%rip)
	movaps	%xmm0, diff64+96(%rip)
	movaps	224(%rsp), %xmm0
	movaps	240(%rsp), %xmm1
	movaps	%xmm1, diff64+144(%rip)
	movaps	%xmm0, diff64+128(%rip)
	movaps	256(%rsp), %xmm0
	movaps	272(%rsp), %xmm1
	movaps	%xmm1, diff64+176(%rip)
	movaps	%xmm0, diff64+160(%rip)
	movaps	288(%rsp), %xmm0
	movaps	304(%rsp), %xmm1
	movaps	%xmm1, diff64+208(%rip)
	movaps	%xmm0, diff64+192(%rip)
	movdqa	320(%rsp), %xmm0
	movdqa	336(%rsp), %xmm1
	movdqa	%xmm1, diff64+240(%rip)
	movdqa	%xmm0, diff64+224(%rip)
	movl	$diff64, %edi
	callq	distortion8x8
	movq	24(%rsp), %rcx          # 8-byte Reload
	addl	%eax, (%rcx)
	jmp	.LBB8_9
.LBB8_2:                                # %._crit_edge107
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	$2147483647, (%rax)     # imm = 0x7FFFFFFF
	movl	$2147483647, %r13d      # imm = 0x7FFFFFFF
.LBB8_9:
	movl	%r13d, %eax
	addq	$360, %rsp              # imm = 0x168
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	GetDirectCost8x8, .Lfunc_end8-GetDirectCost8x8
	.cfi_endproc

	.globl	GetDirectCostMB
	.p2align	4, 0x90
	.type	GetDirectCostMB,@function
GetDirectCostMB:                        # @GetDirectCostMB
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi317:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi318:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi319:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi320:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi321:
	.cfi_def_cfa_offset 48
.Lcfi322:
	.cfi_offset %rbx, -40
.Lcfi323:
	.cfi_offset %r14, -32
.Lcfi324:
	.cfi_offset %r15, -24
.Lcfi325:
	.cfi_offset %rbp, -16
	movl	$0, 4(%rsp)
	leaq	4(%rsp), %rsi
	xorl	%edi, %edi
	callq	GetDirectCost8x8
	movl	%eax, %ebp
	movl	$2147483647, %ebx       # imm = 0x7FFFFFFF
	cmpl	$2147483647, 4(%rsp)    # imm = 0x7FFFFFFF
	je	.LBB9_10
# BB#1:
	leaq	4(%rsp), %rsi
	movl	$1, %edi
	callq	GetDirectCost8x8
	movl	%eax, %r14d
	cmpl	$2147483647, 4(%rsp)    # imm = 0x7FFFFFFF
	je	.LBB9_10
# BB#2:
	leaq	4(%rsp), %rsi
	movl	$2, %edi
	callq	GetDirectCost8x8
	movl	%eax, %r15d
	cmpl	$2147483647, 4(%rsp)    # imm = 0x7FFFFFFF
	je	.LBB9_10
# BB#3:
	leaq	4(%rsp), %rsi
	movl	$3, %edi
	callq	GetDirectCost8x8
	movl	4(%rsp), %ecx
	cmpl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	je	.LBB9_10
# BB#4:
	addl	%ebp, %r14d
	addl	%r14d, %r15d
	movl	%eax, %ebx
	addl	%r15d, %ebx
	movq	input(%rip), %rax
	movl	5100(%rax), %edx
	cmpl	$1, %edx
	je	.LBB9_5
# BB#11:
	cmpl	$2, %edx
	je	.LBB9_9
	jmp	.LBB9_10
.LBB9_5:
	cmpl	%ebx, %ecx
	jl	.LBB9_9
# BB#6:
	cmpl	$0, 4036(%rax)
	je	.LBB9_9
# BB#7:
	cmpl	$0, 4040(%rax)
	je	.LBB9_9
# BB#8:
	cmpl	$0, 4044(%rax)
	jne	.LBB9_10
.LBB9_9:
	movl	%ecx, %ebx
.LBB9_10:                               # %.loopexit
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	GetDirectCostMB, .Lfunc_end9-GetDirectCostMB
	.cfi_endproc

	.globl	PartitionMotionSearch
	.p2align	4, 0x90
	.type	PartitionMotionSearch,@function
PartitionMotionSearch:                  # @PartitionMotionSearch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi326:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi327:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi328:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi329:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi330:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi331:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi332:
	.cfi_def_cfa_offset 288
.Lcfi333:
	.cfi_offset %rbx, -56
.Lcfi334:
	.cfi_offset %r12, -48
.Lcfi335:
	.cfi_offset %r13, -40
.Lcfi336:
	.cfi_offset %r14, -32
.Lcfi337:
	.cfi_offset %r15, -24
.Lcfi338:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	xorl	%ebp, %ebp
	cmpl	$1, 20(%rax)
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	sete	%bpl
	cmpl	$5, %edi
	movl	$4, %ecx
	cmovll	%edi, %ecx
	movq	input(%rip), %rdx
	movslq	%ecx, %rcx
	movl	136(%rdx,%rcx,8), %r9d
	movslq	140(%rdx,%rcx,8), %r10
	movslq	%edi, %rbx
	movslq	136(%rdx,%rbx,8), %r11
	movq	%r11, 40(%rsp)          # 8-byte Spill
	movq	14224(%rax), %r8
	movslq	12(%rax), %rax
	imulq	$536, %rax, %r14        # imm = 0x218
	movslq	%esi, %rsi
	shlq	$4, %rcx
	movslq	PartitionMotionSearch.by0(%rcx,%rsi,4), %rax
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movl	PartitionMotionSearch.bx0(%rcx,%rsi,4), %esi
	movq	%r10, %rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movq	%rax, 144(%rsp)         # 8-byte Spill
	addq	%rax, %r10
	movq	%r10, 184(%rsp)         # 8-byte Spill
	movq	%r9, 192(%rsp)          # 8-byte Spill
	leal	(%rsi,%r9), %eax
	cmpl	$3, %ebx
	movslq	432(%r8,%r14), %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movl	$2, %ecx
	movl	%edi, 32(%rsp)          # 4-byte Spill
	cmovll	%edi, %ecx
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	movslq	140(%rdx,%rbx,8), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	incq	%rbp
	movq	%rbp, 136(%rsp)         # 8-byte Spill
	movq	%rsi, 176(%rsp)         # 8-byte Spill
	movslq	%esi, %rcx
	movl	%eax, 84(%rsp)          # 4-byte Spill
	cltq
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	leal	(,%rcx,4), %eax
	movl	%eax, 76(%rsp)          # 4-byte Spill
	leal	(,%r11,4), %eax
	movl	%eax, 80(%rsp)          # 4-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB10_1:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_3 Depth 2
                                        #       Child Loop BB10_10 Depth 3
                                        #         Child Loop BB10_21 Depth 4
                                        #         Child Loop BB10_13 Depth 4
                                        #           Child Loop BB10_18 Depth 5
                                        #           Child Loop BB10_15 Depth 5
                                        #             Child Loop BB10_16 Depth 6
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	(%r13,%rax), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	cmpl	$0, listXsize(,%rax,4)
	jle	.LBB10_24
# BB#2:                                 # %.lr.ph147.preheader
                                        #   in Loop: Header=BB10_1 Depth=1
	xorl	%ecx, %ecx
	movq	%r13, 208(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB10_3:                               # %.lr.ph147
                                        #   Parent Loop BB10_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_10 Depth 3
                                        #         Child Loop BB10_21 Depth 4
                                        #         Child Loop BB10_13 Depth 4
                                        #           Child Loop BB10_18 Depth 5
                                        #           Child Loop BB10_15 Depth 5
                                        #             Child Loop BB10_16 Depth 6
	movq	motion_cost(%rip), %rax
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	(%rax,%r13,8), %rax
	movswq	%cx, %r14
	movq	(%rax,%r14,8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	input(%rip), %rax
	movl	4140(%rax), %edx
	cmpl	$2, %edx
	jne	.LBB10_5
# BB#4:                                 #   in Loop: Header=BB10_3 Depth=2
	movl	28(%rax), %eax
	jmp	.LBB10_8
	.p2align	4, 0x90
.LBB10_5:                               #   in Loop: Header=BB10_3 Depth=2
	movl	28(%rax), %eax
	cmpl	$2, %ecx
	movl	$1, %esi
	cmovgel	%esi, %ecx
	incl	%ecx
	cmpl	$1, %edx
	je	.LBB10_7
# BB#6:                                 #   in Loop: Header=BB10_3 Depth=2
	imull	72(%rsp), %ecx          # 4-byte Folded Reload
.LBB10_7:                               #   in Loop: Header=BB10_3 Depth=2
	cltd
	idivl	%ecx
.LBB10_8:                               #   in Loop: Header=BB10_3 Depth=2
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rcx
	movq	6512(%rax), %rax
	movq	(%rcx,%r13,8), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	(%rax,%r13,8), %r12
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	$0, (%rcx,%rax,4)
	cmpl	$0, 152(%rsp)           # 4-byte Folded Reload
	jle	.LBB10_23
# BB#9:                                 # %.lr.ph143
                                        #   in Loop: Header=BB10_3 Depth=2
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r14, 224(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB10_10:                              #   Parent Loop BB10_1 Depth=1
                                        #     Parent Loop BB10_3 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB10_21 Depth 4
                                        #         Child Loop BB10_13 Depth 4
                                        #           Child Loop BB10_18 Depth 5
                                        #           Child Loop BB10_15 Depth 5
                                        #             Child Loop BB10_16 Depth 6
	cmpl	$0, 192(%rsp)           # 4-byte Folded Reload
	jle	.LBB10_22
# BB#11:                                # %.lr.ph133
                                        #   in Loop: Header=BB10_10 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	(,%rax,4), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	movl	76(%rsp), %r15d         # 4-byte Reload
	movq	176(%rsp), %rax         # 8-byte Reload
	movl	%eax, %ebx
	jle	.LBB10_21
# BB#12:                                # %.lr.ph133.split.us.preheader
                                        #   in Loop: Header=BB10_10 Depth=3
	movq	img(%rip), %rax
	movl	172(%rax), %ecx
	addl	24(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rdx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	addl	48(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movq	168(%rsp), %rdi         # 8-byte Reload
	jmp	.LBB10_13
	.p2align	4, 0x90
.LBB10_18:                              # %.lr.ph129..lr.ph129.split_crit_edge.us
                                        #   Parent Loop BB10_1 Depth=1
                                        #     Parent Loop BB10_3 Depth=2
                                        #       Parent Loop BB10_10 Depth=3
                                        #         Parent Loop BB10_13 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbp,8), %rdi
	addq	%rbx, %rdi
	movzbl	%r14b, %esi
	callq	memset
	movq	40(%rsp), %rdx          # 8-byte Reload
	incq	%rbp
	cmpq	120(%rsp), %rbp         # 8-byte Folded Reload
	jl	.LBB10_18
.LBB10_19:                              # %._crit_edge130.us
                                        #   in Loop: Header=BB10_13 Depth=4
	movq	216(%rsp), %rdi         # 8-byte Reload
	addq	%rdx, %rdi
	cmpq	200(%rsp), %rdi         # 8-byte Folded Reload
	movq	208(%rsp), %r13         # 8-byte Reload
	jge	.LBB10_22
# BB#20:                                # %._crit_edge130.us..lr.ph133.split.us_crit_edge
                                        #   in Loop: Header=BB10_13 Depth=4
	movq	img(%rip), %rax
.LBB10_13:                              # %.lr.ph133.split.us
                                        #   Parent Loop BB10_1 Depth=1
                                        #     Parent Loop BB10_3 Depth=2
                                        #       Parent Loop BB10_10 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB10_18 Depth 5
                                        #           Child Loop BB10_15 Depth 5
                                        #             Child Loop BB10_16 Depth 6
	movq	14384(%rax), %rcx
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	(%rcx,%rdx,8), %r15
	movq	%r13, %rsi
	movl	168(%rax), %r13d
	addl	%edi, %r13d
	movq	%rdi, 216(%rsp)         # 8-byte Spill
	leal	(,%rdi,4), %edx
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	movl	%r14d, %edi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	36(%rsp), %ecx          # 4-byte Reload
	movl	32(%rsp), %r8d          # 4-byte Reload
	movl	20(%rsp), %r9d          # 4-byte Reload
	callq	BlockMotionSearch
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	addl	%eax, (%rdx,%rcx,4)
	movq	40(%rsp), %rdx          # 8-byte Reload
	testl	%edx, %edx
	movslq	%r13d, %rbx
	movq	104(%rsp), %rbp         # 8-byte Reload
	jle	.LBB10_18
# BB#14:                                # %.lr.ph.us.us.preheader
                                        #   in Loop: Header=BB10_13 Depth=4
	addl	%edx, %r13d
	movslq	%r13d, %rbp
	movq	104(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB10_15:                              # %.lr.ph.us.us
                                        #   Parent Loop BB10_1 Depth=1
                                        #     Parent Loop BB10_3 Depth=2
                                        #       Parent Loop BB10_10 Depth=3
                                        #         Parent Loop BB10_13 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB10_16 Depth 6
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r13,8), %rdi
	addq	%rbx, %rdi
	movzbl	%r14b, %esi
	callq	memset
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB10_16:                              #   Parent Loop BB10_1 Depth=1
                                        #     Parent Loop BB10_3 Depth=2
                                        #       Parent Loop BB10_10 Depth=3
                                        #         Parent Loop BB10_13 Depth=4
                                        #           Parent Loop BB10_15 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	(%r12,%r13,8), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movl	(%r15), %edx
	movl	%edx, (%rcx)
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB10_16
# BB#17:                                # %._crit_edge.us.us
                                        #   in Loop: Header=BB10_15 Depth=5
	incq	%r13
	cmpq	120(%rsp), %r13         # 8-byte Folded Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	224(%rsp), %r14         # 8-byte Reload
	movq	%rsi, %rbx
	jl	.LBB10_15
	jmp	.LBB10_19
	.p2align	4, 0x90
.LBB10_21:                              # %.lr.ph133.split
                                        #   Parent Loop BB10_1 Depth=1
                                        #     Parent Loop BB10_3 Depth=2
                                        #       Parent Loop BB10_10 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	movl	%r14d, %edi
	movl	%r13d, %esi
	movl	%r15d, %edx
	movl	36(%rsp), %ecx          # 4-byte Reload
	movl	32(%rsp), %r8d          # 4-byte Reload
	movl	20(%rsp), %r9d          # 4-byte Reload
	callq	BlockMotionSearch
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	addl	%eax, (%rdx,%rcx,4)
	addl	40(%rsp), %ebx          # 4-byte Folded Reload
	addl	80(%rsp), %r15d         # 4-byte Folded Reload
	cmpl	84(%rsp), %ebx          # 4-byte Folded Reload
	jl	.LBB10_21
.LBB10_22:                              # %._crit_edge134
                                        #   in Loop: Header=BB10_10 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	addq	48(%rsp), %rcx          # 8-byte Folded Reload
	movq	%rcx, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmpq	184(%rsp), %rcx         # 8-byte Folded Reload
	jl	.LBB10_10
.LBB10_23:                              # %._crit_edge144
                                        #   in Loop: Header=BB10_3 Depth=2
	incl	%r14d
	movswl	%r14w, %ecx
	movq	160(%rsp), %rax         # 8-byte Reload
	cmpl	listXsize(,%rax,4), %ecx
	jl	.LBB10_3
.LBB10_24:                              # %._crit_edge148
                                        #   in Loop: Header=BB10_1 Depth=1
	incq	%r13
	cmpq	136(%rsp), %r13         # 8-byte Folded Reload
	jl	.LBB10_1
# BB#25:
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	PartitionMotionSearch, .Lfunc_end10-PartitionMotionSearch
	.cfi_endproc

	.globl	Get_Direct_Motion_Vectors
	.p2align	4, 0x90
	.type	Get_Direct_Motion_Vectors,@function
Get_Direct_Motion_Vectors:              # @Get_Direct_Motion_Vectors
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi339:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi340:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi341:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi342:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi343:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi344:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi345:
	.cfi_def_cfa_offset 208
.Lcfi346:
	.cfi_offset %rbx, -56
.Lcfi347:
	.cfi_offset %r12, -48
.Lcfi348:
	.cfi_offset %r13, -40
.Lcfi349:
	.cfi_offset %r14, -32
.Lcfi350:
	.cfi_offset %r15, -24
.Lcfi351:
	.cfi_offset %rbp, -16
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rcx
	movq	(%rcx), %r13
	movq	8(%rcx), %r9
	movq	img(%rip), %rdx
	movq	14224(%rdx), %r15
	movslq	12(%rdx), %rdi
	imulq	$536, %rdi, %r12        # imm = 0x218
	movslq	432(%r15,%r12), %rcx
	testq	%rcx, %rcx
	je	.LBB11_4
# BB#1:
	testb	$1, %dil
	movq	Co_located(%rip), %rsi
	jne	.LBB11_2
# BB#3:
	leaq	3232(%rsi), %r8
	leaq	3216(%rsi), %rbx
	leaq	3240(%rsi), %rbp
	addq	$3224, %rsi             # imm = 0xC98
	cmpl	$0, 14452(%rdx)
	jne	.LBB11_6
	jmp	.LBB11_121
.LBB11_4:
	movq	Co_located(%rip), %rsi
	leaq	1624(%rsi), %rbp
	leaq	1616(%rsi), %r8
	leaq	1600(%rsi), %rbx
	leaq	1608(%rsi), %rsi
	cmpl	$0, 14452(%rdx)
	jne	.LBB11_6
	jmp	.LBB11_121
.LBB11_2:
	leaq	4848(%rsi), %r8
	leaq	4832(%rsi), %rbx
	leaq	4856(%rsi), %rbp
	addq	$4840, %rsi             # imm = 0x12E8
	cmpl	$0, 14452(%rdx)
	je	.LBB11_121
.LBB11_6:
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movq	(%rbp), %r14
	movl	$0, 28(%rsp)
	movl	$0, 24(%rsp)
	leaq	104(%rsp), %rcx
	movl	$-1, %esi
	xorl	%edx, %edx
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	getLuma4x4Neighbour
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	leaq	80(%rsp), %rcx
	xorl	%esi, %esi
	movl	$-1, %edx
	callq	getLuma4x4Neighbour
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	leaq	32(%rsp), %rcx
	movl	$16, %esi
	movl	$-1, %edx
	callq	getLuma4x4Neighbour
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	leaq	56(%rsp), %rcx
	movl	$-1, %esi
	movl	$-1, %edx
	callq	getLuma4x4Neighbour
	movq	img(%rip), %rdx
	cmpl	$0, 15268(%rdx)
	je	.LBB11_7
# BB#24:
	cmpl	$0, 424(%r15,%r12)
	movl	104(%rsp), %r11d
	je	.LBB11_43
# BB#25:
	testl	%r11d, %r11d
	je	.LBB11_26
# BB#27:
	movq	14224(%rdx), %rax
	movslq	108(%rsp), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	movslq	124(%rsp), %rdi
	movq	(%r13,%rdi,8), %rdi
	movslq	120(%rsp), %rbp
	movsbl	(%rdi,%rbp), %r15d
	testl	%r15d, %r15d
	setns	%bl
	cmpl	$0, 424(%rax,%rcx)
	sete	%cl
	andb	%bl, %cl
	shll	%cl, %r15d
	jmp	.LBB11_28
.LBB11_121:
	leaq	432(%r15,%r12), %r12
	movq	(%rsi), %rsi
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	movq	(%rbx), %r14
	movq	(%r8), %rsi
	movq	%rsi, 136(%rsp)         # 8-byte Spill
	imulq	$264, %rcx, %rcx        # imm = 0x108
	leaq	24(%rax,%rcx), %r10
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r12, 128(%rsp)         # 8-byte Spill
	jmp	.LBB11_122
	.p2align	4, 0x90
.LBB11_151:                             #   in Loop: Header=BB11_122 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpq	$4, %rcx
	je	.LBB11_120
# BB#152:                               # %._crit_edge742
                                        #   in Loop: Header=BB11_122 Depth=1
	movq	img(%rip), %rdx
.LBB11_122:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_123 Depth 2
                                        #       Child Loop BB11_126 Depth 3
                                        #       Child Loop BB11_147 Depth 3
	movl	180(%rdx), %eax
	movl	196(%rdx), %ecx
	sarl	$2, %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	%esi, %eax
	sarl	$2, %ecx
	addl	%esi, %ecx
	movslq	%ecx, %r15
	movslq	%eax, %r13
	xorl	%ecx, %ecx
	jmp	.LBB11_123
.LBB11_148:                             #   in Loop: Header=BB11_123 Depth=2
	movb	$-1, (%r8,%r9)
	movq	direct_ref_idx(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movb	$-1, (%rax,%r9)
	movq	direct_pdir(%rip), %rax
	movq	(%rax,%r13,8), %rax
	movb	$-1, (%rax,%r9)
	.p2align	4, 0x90
.LBB11_149:                             # %.loopexit
                                        #   in Loop: Header=BB11_123 Depth=2
	incq	%rcx
	cmpq	$4, %rcx
	jne	.LBB11_150
	jmp	.LBB11_151
.LBB11_130:                             #   in Loop: Header=BB11_123 Depth=2
	shlq	$7, %r9
	addq	%r9, %rdx
	movl	14472(%rdx,%rbp,4), %r9d
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbx,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%rsi,8), %rsi
	movzwl	(%rsi), %eax
	cmpl	$9999, %r9d             # imm = 0x270F
	jne	.LBB11_132
# BB#131:                               #   in Loop: Header=BB11_123 Depth=2
	movq	(%r8), %rdx
	movq	(%rdx), %rdi
	movq	(%rdi), %rdi
	movw	%ax, (%rdi)
	movzwl	2(%rsi), %eax
	movw	%ax, 2(%rdi)
	movq	8(%r8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movw	$0, (%rdi)
	movq	(%rdx,%rbp,8), %rax
	movq	(%rax), %r12
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.LBB11_133
.LBB11_132:                             #   in Loop: Header=BB11_123 Depth=2
	cwtl
	imull	%r9d, %eax
	subl	$-128, %eax
	shrl	$8, %eax
	movq	(%r8), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movq	(%rdx), %r12
	movw	%ax, (%r12)
	movswl	2(%rsi), %eax
	imull	%r9d, %eax
	subl	$-128, %eax
	shrl	$8, %eax
	movw	%ax, 2(%r12)
	addl	$-256, %r9d
	movswl	(%rsi), %edx
	imull	%r9d, %edx
	subl	$-128, %edx
	shrl	$8, %edx
	movq	8(%r8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movw	%dx, (%rdi)
	movswl	2(%rsi), %esi
	imull	%r9d, %esi
	subl	$-128, %esi
	shrl	$8, %esi
.LBB11_133:                             #   in Loop: Header=BB11_123 Depth=2
	movw	%si, 2(%rdi)
	movzwl	(%r12), %eax
	addl	$8192, %eax             # imm = 0x2000
	movzwl	%ax, %eax
	cmpl	$16383, %eax            # imm = 0x3FFF
	ja	.LBB11_139
# BB#134:                               #   in Loop: Header=BB11_123 Depth=2
	movswl	2(%r12), %eax
	movq	img(%rip), %rdi
	movslq	8(%rdi), %rdi
	leaq	(%rdi,%rdi,2), %rbx
	movl	LEVELMVLIMIT+16(,%rbx,8), %edi
	cmpl	%edi, %eax
	jge	.LBB11_135
.LBB11_139:                             #   in Loop: Header=BB11_123 Depth=2
	movq	direct_ref_idx(%rip), %rax
	movq	(%rax), %rax
	movslq	%r11d, %rdx
	movq	(%rax,%r13,8), %rsi
	addq	%rdx, %rsi
	movb	$-1, %bpl
	movb	$-1, %dil
	movb	$-1, %bl
.LBB11_141:                             #   in Loop: Header=BB11_123 Depth=2
	movq	128(%rsp), %r12         # 8-byte Reload
	jmp	.LBB11_142
.LBB11_135:                             #   in Loop: Header=BB11_123 Depth=2
	movl	LEVELMVLIMIT+20(,%rbx,8), %ebx
	cmpl	%ebx, %eax
	jg	.LBB11_139
# BB#136:                               #   in Loop: Header=BB11_123 Depth=2
	addl	$8192, %edx             # imm = 0x2000
	movzwl	%dx, %eax
	cmpl	$16383, %eax            # imm = 0x3FFF
	ja	.LBB11_139
# BB#137:                               #   in Loop: Header=BB11_123 Depth=2
	movswl	%si, %eax
	cmpl	%edi, %eax
	jl	.LBB11_139
# BB#138:                               #   in Loop: Header=BB11_123 Depth=2
	cmpl	%ebx, %eax
	jg	.LBB11_139
# BB#140:                               #   in Loop: Header=BB11_123 Depth=2
	movq	direct_ref_idx(%rip), %rax
	movq	(%rax), %rax
	movslq	%r11d, %rdx
	movq	(%rax,%r13,8), %rsi
	addq	%rdx, %rsi
	movb	$2, %bl
	xorl	%edi, %edi
	jmp	.LBB11_141
	.p2align	4, 0x90
.LBB11_150:                             # %.loopexit._crit_edge
                                        #   in Loop: Header=BB11_123 Depth=2
	movq	img(%rip), %rdx
.LBB11_123:                             #   Parent Loop BB11_122 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_126 Depth 3
                                        #       Child Loop BB11_147 Depth 3
	movl	176(%rdx), %r11d
	movl	192(%rdx), %eax
	sarl	$2, %r11d
	addl	%ecx, %r11d
	sarl	$2, %eax
	addl	%ecx, %eax
	movq	14384(%rdx), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rsi,%rdi,8), %rsi
	movq	(%rsi,%rcx,8), %r8
	movq	(%r14), %rsi
	movq	(%rsi,%r15,8), %rdi
	movslq	%eax, %rsi
	xorl	%ebx, %ebx
	cmpb	$-1, (%rdi,%rsi)
	sete	%bl
	movq	(%r14,%rbx,8), %rax
	movq	(%rax,%r15,8), %rax
	cmpb	$-1, (%rax,%rsi)
	je	.LBB11_153
# BB#124:                               # %.preheader
                                        #   in Loop: Header=BB11_123 Depth=2
	movl	14456(%rdx), %eax
	movslq	(%r12), %r9
	movl	listXsize(,%r9,4), %edi
	cmpl	%edi, %eax
	cmovlel	%eax, %edi
	testl	%edi, %edi
	jle	.LBB11_128
# BB#125:                               # %.lr.ph
                                        #   in Loop: Header=BB11_123 Depth=2
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbx,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%rsi,8), %rax
	movslq	%edi, %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_126:                             #   Parent Loop BB11_122 Depth=1
                                        #     Parent Loop BB11_123 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rax, (%r10,%rbp,8)
	je	.LBB11_129
# BB#127:                               #   in Loop: Header=BB11_126 Depth=3
	incq	%rbp
	cmpq	%rdi, %rbp
	jl	.LBB11_126
	jmp	.LBB11_128
	.p2align	4, 0x90
.LBB11_153:                             #   in Loop: Header=BB11_123 Depth=2
	movq	(%r8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movl	$0, (%rax)
	movq	8(%r8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movl	$0, (%rax)
	movq	direct_ref_idx(%rip), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movslq	%r11d, %rdx
	movb	$0, (%rax,%rdx)
	movb	$2, %bl
	xorl	%edi, %edi
	jmp	.LBB11_143
.LBB11_129:                             #   in Loop: Header=BB11_123 Depth=2
	cmpl	$-135792468, %ebp       # imm = 0xF7E7F8AC
	jne	.LBB11_130
	.p2align	4, 0x90
.LBB11_128:                             # %.thread
                                        #   in Loop: Header=BB11_123 Depth=2
	movq	direct_ref_idx(%rip), %rax
	movq	(%rax), %rax
	movslq	%r11d, %rdx
	movq	(%rax,%r13,8), %rsi
	addq	%rdx, %rsi
	movb	$-1, %bpl
	movb	$-1, %dil
	movb	$-1, %bl
.LBB11_142:                             #   in Loop: Header=BB11_123 Depth=2
	movb	%bpl, (%rsi)
.LBB11_143:                             #   in Loop: Header=BB11_123 Depth=2
	movq	direct_ref_idx(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movb	%dil, (%rax,%rdx)
	movq	direct_pdir(%rip), %rax
	movq	(%rax,%r13,8), %rax
	movb	%bl, (%rax,%rdx)
	movq	active_pps(%rip), %rax
	cmpl	$1, 196(%rax)
	jne	.LBB11_149
# BB#144:                               #   in Loop: Header=BB11_123 Depth=2
	movq	direct_pdir(%rip), %rax
	movq	(%rax,%r13,8), %rdx
	movslq	%r11d, %r9
	cmpb	$2, (%rdx,%r9)
	jne	.LBB11_149
# BB#145:                               #   in Loop: Header=BB11_123 Depth=2
	movq	direct_ref_idx(%rip), %rdx
	movq	(%rdx), %rsi
	movq	8(%rdx), %rdi
	movq	(%rsi,%r13,8), %r8
	movq	(%rdi,%r13,8), %rsi
	movq	wbp_weight(%rip), %rdi
	movsbq	(%r8,%r9), %rbp
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	(%rbx,%rbp,8), %rbx
	movsbq	(%rsi,%r9), %rdx
	movq	(%rbx,%rdx,8), %rsi
	movq	(%rdi,%rbp,8), %rdi
	movq	(%rdi,%rdx,8), %rdi
	movq	active_sps(%rip), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_147:                             #   Parent Loop BB11_122 Depth=1
                                        #     Parent Loop BB11_123 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rsi,%rbx,4), %edx
	movl	(%rdi,%rbx,4), %eax
	leal	128(%rdx,%rax), %eax
	cmpl	$256, %eax              # imm = 0x100
	jae	.LBB11_148
# BB#146:                               #   in Loop: Header=BB11_147 Depth=3
	incq	%rbx
	xorl	%edx, %edx
	cmpl	$0, 32(%rbp)
	setne	%dl
	leaq	1(%rdx,%rdx), %rdx
	cmpq	%rdx, %rbx
	jl	.LBB11_147
	jmp	.LBB11_149
.LBB11_7:
	movl	104(%rsp), %ecx
	testl	%ecx, %ecx
	movl	$-1, %r15d
	je	.LBB11_9
# BB#8:
	movslq	124(%rsp), %rax
	movq	(%r13,%rax,8), %rax
	movslq	120(%rsp), %rdx
	movsbl	(%rax,%rdx), %r15d
.LBB11_9:
	movl	80(%rsp), %eax
	movw	$-1, %r12w
	testl	%eax, %eax
	movw	$-1, %r10w
	movq	8(%rsp), %rbx           # 8-byte Reload
	je	.LBB11_11
# BB#10:
	movslq	100(%rsp), %rdx
	movq	(%r13,%rdx,8), %rdx
	movslq	96(%rsp), %rsi
	movsbl	(%rdx,%rsi), %r10d
.LBB11_11:
	movl	56(%rsp), %edx
	testl	%edx, %edx
	je	.LBB11_13
# BB#12:
	movslq	76(%rsp), %rsi
	movq	(%r13,%rsi,8), %rsi
	movslq	72(%rsp), %rdi
	movsbl	(%rsi,%rdi), %r12d
.LBB11_13:
	movl	32(%rsp), %edi
	testl	%edi, %edi
	je	.LBB11_15
# BB#14:
	movslq	52(%rsp), %rsi
	movq	(%r13,%rsi,8), %rsi
	movslq	48(%rsp), %rbp
	movsbl	(%rsi,%rbp), %r12d
.LBB11_15:
	testl	%ecx, %ecx
	je	.LBB11_16
# BB#17:
	movslq	124(%rsp), %rcx
	movq	(%rbx,%rcx,8), %rcx
	movslq	120(%rsp), %rsi
	movsbl	(%rcx,%rsi), %esi
	jmp	.LBB11_18
.LBB11_43:
	testl	%r11d, %r11d
	je	.LBB11_44
# BB#45:
	movq	14224(%rdx), %rax
	movslq	108(%rsp), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	cmpl	$0, 424(%rax,%rcx)
	setne	%al
	movslq	124(%rsp), %rcx
	movq	(%r13,%rcx,8), %rcx
	movslq	120(%rsp), %rsi
	movsbl	(%rcx,%rsi), %r15d
	movl	%r15d, %ecx
	shrb	$7, %cl
	orb	%al, %cl
	sarl	%cl, %r15d
	jmp	.LBB11_46
.LBB11_26:
	movl	$-1, %r15d
.LBB11_28:                              # %._crit_edge
	movl	80(%rsp), %eax
	movw	$-1, %r12w
	testl	%eax, %eax
	movw	$-1, %r10w
	je	.LBB11_30
# BB#29:
	movq	14224(%rdx), %rcx
	movslq	84(%rsp), %rdi
	imulq	$536, %rdi, %rdi        # imm = 0x218
	movslq	100(%rsp), %rbp
	movq	(%r13,%rbp,8), %rbp
	movslq	96(%rsp), %rbx
	movsbl	(%rbp,%rbx), %r10d
	testw	%r10w, %r10w
	setns	%bl
	cmpl	$0, 424(%rcx,%rdi)
	sete	%cl
	andb	%bl, %cl
	shll	%cl, %r10d
.LBB11_30:                              # %._crit_edge572
	movl	56(%rsp), %r8d
	testl	%r8d, %r8d
	je	.LBB11_32
# BB#31:
	movq	14224(%rdx), %rcx
	movslq	60(%rsp), %rdi
	imulq	$536, %rdi, %rdi        # imm = 0x218
	movslq	76(%rsp), %rbp
	movq	(%r13,%rbp,8), %rbp
	movslq	72(%rsp), %rbx
	movsbl	(%rbp,%rbx), %r12d
	testw	%r12w, %r12w
	setns	%bl
	cmpl	$0, 424(%rcx,%rdi)
	sete	%cl
	andb	%bl, %cl
	shll	%cl, %r12d
.LBB11_32:                              # %._crit_edge583
	movl	32(%rsp), %r9d
	testl	%r9d, %r9d
	je	.LBB11_34
# BB#33:
	movq	14224(%rdx), %rcx
	movslq	36(%rsp), %rbp
	imulq	$536, %rbp, %rbp        # imm = 0x218
	movslq	52(%rsp), %rbx
	movq	(%r13,%rbx,8), %rbx
	movslq	48(%rsp), %rsi
	movsbl	(%rbx,%rsi), %r12d
	testw	%r12w, %r12w
	setns	%bl
	cmpl	$0, 424(%rcx,%rbp)
	sete	%cl
	andb	%bl, %cl
	shll	%cl, %r12d
.LBB11_34:                              # %._crit_edge594
	testl	%r11d, %r11d
	je	.LBB11_35
# BB#36:
	movq	14224(%rdx), %rcx
	movslq	108(%rsp), %rsi
	imulq	$536, %rsi, %rbp        # imm = 0x218
	movslq	124(%rsp), %rsi
	movq	8(%rsp), %r11           # 8-byte Reload
	movq	(%r11,%rsi,8), %rsi
	movslq	120(%rsp), %rbx
	movsbl	(%rsi,%rbx), %esi
	testl	%esi, %esi
	setns	%bl
	cmpl	$0, 424(%rcx,%rbp)
	sete	%cl
	andb	%bl, %cl
	shll	%cl, %esi
	jmp	.LBB11_37
.LBB11_35:
	movl	$-1, %esi
	movq	8(%rsp), %r11           # 8-byte Reload
.LBB11_37:                              # %._crit_edge605
	movw	$-1, %r13w
	testl	%eax, %eax
	movw	$-1, %ax
	je	.LBB11_39
# BB#38:
	movq	14224(%rdx), %rcx
	movslq	84(%rsp), %rax
	imulq	$536, %rax, %rbp        # imm = 0x218
	movslq	100(%rsp), %rax
	movq	(%r11,%rax,8), %rax
	movslq	96(%rsp), %rbx
	movsbl	(%rax,%rbx), %eax
	testw	%ax, %ax
	setns	%bl
	cmpl	$0, 424(%rcx,%rbp)
	sete	%cl
	andb	%bl, %cl
	shll	%cl, %eax
.LBB11_39:                              # %._crit_edge616
	testl	%r8d, %r8d
	je	.LBB11_41
# BB#40:
	movq	14224(%rdx), %rcx
	movslq	60(%rsp), %rbp
	imulq	$536, %rbp, %rbp        # imm = 0x218
	movslq	76(%rsp), %rbx
	movq	(%r11,%rbx,8), %rbx
	movslq	72(%rsp), %rdi
	movsbl	(%rbx,%rdi), %r13d
	testw	%r13w, %r13w
	setns	%bl
	cmpl	$0, 424(%rcx,%rbp)
	sete	%cl
	andb	%bl, %cl
	shll	%cl, %r13d
.LBB11_41:                              # %._crit_edge627
	testl	%r9d, %r9d
	je	.LBB11_65
# BB#42:
	movq	14224(%rdx), %rcx
	movslq	36(%rsp), %rdx
	imulq	$536, %rdx, %rdx        # imm = 0x218
	movslq	52(%rsp), %rdi
	movq	(%r11,%rdi,8), %rdi
	movslq	48(%rsp), %rbp
	movsbl	(%rdi,%rbp), %r13d
	testw	%r13w, %r13w
	setns	%bl
	cmpl	$0, 424(%rcx,%rdx)
	sete	%cl
	andb	%bl, %cl
	shll	%cl, %r13d
	jmp	.LBB11_65
.LBB11_16:
	movl	$-1, %esi
.LBB11_18:
	movw	$-1, %r13w
	testl	%eax, %eax
	movw	$-1, %ax
	je	.LBB11_20
# BB#19:
	movslq	100(%rsp), %rax
	movq	(%rbx,%rax,8), %rax
	movslq	96(%rsp), %rcx
	movsbl	(%rax,%rcx), %eax
.LBB11_20:
	testl	%edx, %edx
	je	.LBB11_22
# BB#21:
	movslq	76(%rsp), %rcx
	movq	(%rbx,%rcx,8), %rcx
	movslq	72(%rsp), %rdx
	movsbl	(%rcx,%rdx), %r13d
.LBB11_22:
	testl	%edi, %edi
	je	.LBB11_65
# BB#23:
	movslq	52(%rsp), %rcx
	movq	(%rbx,%rcx,8), %rcx
	movslq	48(%rsp), %rdx
	movsbl	(%rcx,%rdx), %r13d
	jmp	.LBB11_65
.LBB11_44:
	movl	$-1, %r15d
.LBB11_46:                              # %._crit_edge649
	movl	80(%rsp), %edi
	testl	%edi, %edi
	je	.LBB11_47
# BB#48:
	movq	14224(%rdx), %rax
	movslq	84(%rsp), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	cmpl	$0, 424(%rax,%rcx)
	setne	%al
	movslq	100(%rsp), %rcx
	movq	(%r13,%rcx,8), %rcx
	movslq	96(%rsp), %rsi
	movsbl	(%rcx,%rsi), %r10d
	movl	%r10d, %ecx
	shrb	$7, %cl
	orb	%al, %cl
	sarl	%cl, %r10d
	jmp	.LBB11_49
.LBB11_47:
	movw	$-1, %r10w
.LBB11_49:                              # %._crit_edge660
	movl	56(%rsp), %r8d
	testl	%r8d, %r8d
	je	.LBB11_50
# BB#51:
	movq	14224(%rdx), %rax
	movslq	60(%rsp), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	cmpl	$0, 424(%rax,%rcx)
	setne	%al
	movslq	76(%rsp), %rcx
	movq	(%r13,%rcx,8), %rcx
	movslq	72(%rsp), %rsi
	movsbl	(%rcx,%rsi), %r12d
	movl	%r12d, %ecx
	shrb	$7, %cl
	orb	%al, %cl
	sarl	%cl, %r12d
	jmp	.LBB11_52
.LBB11_50:
	movl	$-1, %r12d
.LBB11_52:                              # %._crit_edge671
	movl	32(%rsp), %r9d
	testl	%r9d, %r9d
	je	.LBB11_54
# BB#53:
	movq	14224(%rdx), %rax
	movslq	36(%rsp), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	cmpl	$0, 424(%rax,%rcx)
	setne	%al
	movslq	52(%rsp), %rcx
	movq	(%r13,%rcx,8), %rcx
	movslq	48(%rsp), %rsi
	movsbl	(%rcx,%rsi), %r12d
	movl	%r12d, %ecx
	shrb	$7, %cl
	orb	%al, %cl
	sarl	%cl, %r12d
.LBB11_54:                              # %._crit_edge682
	testl	%r11d, %r11d
	je	.LBB11_55
# BB#56:
	movq	14224(%rdx), %rax
	movslq	108(%rsp), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	cmpl	$0, 424(%rax,%rcx)
	setne	%al
	movslq	124(%rsp), %rcx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	(%rbp,%rcx,8), %rcx
	movslq	120(%rsp), %rsi
	movsbl	(%rcx,%rsi), %esi
	movl	%esi, %ecx
	shrb	$7, %cl
	orb	%al, %cl
	sarl	%cl, %esi
	testl	%edi, %edi
	jne	.LBB11_59
	jmp	.LBB11_58
.LBB11_55:
	movl	$-1, %esi
	movq	8(%rsp), %rbp           # 8-byte Reload
	testl	%edi, %edi
	je	.LBB11_58
.LBB11_59:
	movq	14224(%rdx), %rax
	movslq	84(%rsp), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	cmpl	$0, 424(%rax,%rcx)
	setne	%bl
	movslq	100(%rsp), %rax
	movq	(%rbp,%rax,8), %rax
	movslq	96(%rsp), %rcx
	movsbl	(%rax,%rcx), %eax
	movl	%eax, %ecx
	shrb	$7, %cl
	orb	%bl, %cl
	sarl	%cl, %eax
	testl	%r8d, %r8d
	jne	.LBB11_62
	jmp	.LBB11_61
.LBB11_58:
	movw	$-1, %ax
	testl	%r8d, %r8d
	je	.LBB11_61
.LBB11_62:
	movq	14224(%rdx), %rcx
	movslq	60(%rsp), %rdi
	imulq	$536, %rdi, %rdi        # imm = 0x218
	cmpl	$0, 424(%rcx,%rdi)
	setne	%bl
	movslq	76(%rsp), %rcx
	movq	(%rbp,%rcx,8), %rcx
	movslq	72(%rsp), %rdi
	movsbl	(%rcx,%rdi), %r13d
	movl	%r13d, %ecx
	shrb	$7, %cl
	orb	%bl, %cl
	sarl	%cl, %r13d
	testl	%r9d, %r9d
	jne	.LBB11_64
	jmp	.LBB11_65
.LBB11_61:
	movl	$-1, %r13d
	testl	%r9d, %r9d
	je	.LBB11_65
.LBB11_64:
	movq	14224(%rdx), %rcx
	movslq	36(%rsp), %rdx
	imulq	$536, %rdx, %rdx        # imm = 0x218
	cmpl	$0, 424(%rcx,%rdx)
	setne	%dl
	movslq	52(%rsp), %rcx
	movq	(%rbp,%rcx,8), %rcx
	movslq	48(%rsp), %rdi
	movsbl	(%rcx,%rdi), %r13d
	movl	%r13d, %ecx
	shrb	$7, %cl
	orb	%dl, %cl
	sarl	%cl, %r13d
.LBB11_65:                              # %._crit_edge638
	movswl	%r10w, %ebp
	cmpl	%ebp, %r15d
	setg	%bl
	setl	%dl
	movl	%ebp, %edi
	orw	%r15w, %di
	jns	.LBB11_67
# BB#66:                                # %._crit_edge638
	movl	%ebx, %edx
.LBB11_67:                              # %._crit_edge638
	testb	%dl, %dl
	cmovnel	%r15d, %ebp
	movswl	%bp, %edx
	movswl	%r12w, %edi
	testw	%di, %di
	js	.LBB11_70
# BB#68:                                # %._crit_edge638
	movl	%ebp, %ecx
	shll	$16, %ecx
	cmpl	$-65535, %ecx           # imm = 0xFFFF0001
	jl	.LBB11_70
# BB#69:
	cmpl	%edi, %edx
	setl	%dl
	jmp	.LBB11_71
.LBB11_70:
	cmpl	%edi, %edx
	setg	%dl
.LBB11_71:
	testb	%dl, %dl
	cmovnew	%bp, %r12w
	cwtl
	cmpl	%eax, %esi
	setg	%dl
	setl	%cl
	movl	%eax, %edi
	orw	%si, %di
	jns	.LBB11_73
# BB#72:
	movl	%edx, %ecx
.LBB11_73:
	testb	%cl, %cl
	cmovnel	%esi, %eax
	movswl	%ax, %ecx
	movswl	%r13w, %edx
	testw	%dx, %dx
	js	.LBB11_76
# BB#74:
	movl	%eax, %esi
	shll	$16, %esi
	cmpl	$-65535, %esi           # imm = 0xFFFF0001
	jl	.LBB11_76
# BB#75:
	cmpl	%edx, %ecx
	setl	%cl
	jmp	.LBB11_77
.LBB11_76:
	cmpl	%edx, %ecx
	setg	%cl
.LBB11_77:
	testb	%cl, %cl
	cmovnew	%ax, %r13w
	testw	%r12w, %r12w
	js	.LBB11_79
# BB#78:
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rcx
	movq	6512(%rax), %rax
	movq	(%rcx), %rsi
	movq	(%rax), %rdx
	subq	$8, %rsp
.Lcfi352:
	.cfi_adjust_cfa_offset 8
	movswl	%r12w, %ecx
	leaq	36(%rsp), %rdi
	movl	$0, %r9d
	pushq	$16
.Lcfi353:
	.cfi_adjust_cfa_offset 8
	pushq	$16
.Lcfi354:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi355:
	.cfi_adjust_cfa_offset 8
	callq	SetMotionVectorPredictor
	addq	$32, %rsp
.Lcfi356:
	.cfi_adjust_cfa_offset -32
.LBB11_79:
	xorl	%r15d, %r15d
	testw	%r13w, %r13w
	js	.LBB11_80
# BB#81:
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rcx
	movq	6512(%rax), %rax
	movq	8(%rcx), %rsi
	movq	8(%rax), %rdx
	subq	$8, %rsp
.Lcfi357:
	.cfi_adjust_cfa_offset 8
	movswl	%r13w, %ecx
	leaq	32(%rsp), %rdi
	movl	$0, %r9d
	pushq	$16
.Lcfi358:
	.cfi_adjust_cfa_offset 8
	pushq	$16
.Lcfi359:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi360:
	.cfi_adjust_cfa_offset 8
	callq	SetMotionVectorPredictor
	addq	$32, %rsp
.Lcfi361:
	.cfi_adjust_cfa_offset -32
	movw	24(%rsp), %r9w
	movw	26(%rsp), %r8w
	jmp	.LBB11_82
.LBB11_80:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
.LBB11_82:                              # %.preheader535
	movzwl	28(%rsp), %r11d
	movzwl	30(%rsp), %eax
	movw	%ax, 8(%rsp)            # 2-byte Spill
	xorl	%r10d, %r10d
	movq	%r8, 136(%rsp)          # 8-byte Spill
	movq	%r9, 128(%rsp)          # 8-byte Spill
	movw	%r11w, 22(%rsp)         # 2-byte Spill
	.p2align	4, 0x90
.LBB11_83:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_84 Depth 2
                                        #       Child Loop BB11_114 Depth 3
	movq	img(%rip), %rcx
	movl	180(%rcx), %eax
	movl	196(%rcx), %edx
	sarl	$2, %eax
	addl	%r10d, %eax
	sarl	$2, %edx
	addl	%r10d, %edx
	movslq	%edx, %rdx
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	cltq
	xorl	%edx, %edx
	jmp	.LBB11_84
.LBB11_108:                             #   in Loop: Header=BB11_84 Depth=2
	xorl	%ecx, %ecx
	jmp	.LBB11_117
.LBB11_110:                             #   in Loop: Header=BB11_84 Depth=2
	movb	$1, %cl
	jmp	.LBB11_117
.LBB11_115:                             #   in Loop: Header=BB11_84 Depth=2
	movb	$-1, (%rdi,%rsi)
	movq	direct_ref_idx(%rip), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rax,8), %rcx
	movb	$-1, (%rcx,%rsi)
	movb	$-1, %cl
.LBB11_116:                             # %.critedge
                                        #   in Loop: Header=BB11_84 Depth=2
	movq	%r8, %r14
	xorl	%r15d, %r15d
	movq	136(%rsp), %r8          # 8-byte Reload
	movq	128(%rsp), %r9          # 8-byte Reload
	movzwl	22(%rsp), %r11d         # 2-byte Folded Reload
	jmp	.LBB11_117
	.p2align	4, 0x90
.LBB11_118:                             # %.critedge._crit_edge
                                        #   in Loop: Header=BB11_84 Depth=2
	movq	img(%rip), %rcx
.LBB11_84:                              #   Parent Loop BB11_83 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_114 Depth 3
	movl	176(%rcx), %esi
	movl	192(%rcx), %edi
	sarl	$2, %esi
	addl	%edx, %esi
	sarl	$2, %edi
	addl	%edx, %edi
	movq	14384(%rcx), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%rdx,8), %rcx
	testw	%r12w, %r12w
	js	.LBB11_89
# BB#85:                                #   in Loop: Header=BB11_84 Depth=2
	jne	.LBB11_88
# BB#86:                                #   in Loop: Header=BB11_84 Depth=2
	movq	144(%rsp), %rbx         # 8-byte Reload
	movq	(%r14,%rbx,8), %rbp
	movslq	%edi, %rbx
	cmpb	$0, (%rbp,%rbx)
	je	.LBB11_87
.LBB11_88:                              #   in Loop: Header=BB11_84 Depth=2
	movq	(%rcx), %rbp
	movswq	%r12w, %rbx
	movq	(%rbp,%rbx,8), %rbp
	movq	(%rbp), %rbp
	movw	%r11w, (%rbp)
	movzwl	8(%rsp), %ebx           # 2-byte Folded Reload
	movw	%bx, 2(%rbp)
	movb	%r12b, %bpl
	jmp	.LBB11_90
	.p2align	4, 0x90
.LBB11_89:                              #   in Loop: Header=BB11_84 Depth=2
	movq	(%rcx), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp), %rbp
	movl	$0, (%rbp)
	movb	$-1, %bpl
.LBB11_90:                              #   in Loop: Header=BB11_84 Depth=2
	movq	direct_ref_idx(%rip), %rbx
	movq	(%rbx), %rbx
	movq	(%rbx,%rax,8), %rbx
	movslq	%esi, %rsi
	movb	%bpl, (%rbx,%rsi)
	testw	%r13w, %r13w
	js	.LBB11_96
# BB#91:                                #   in Loop: Header=BB11_84 Depth=2
	jne	.LBB11_94
# BB#92:                                #   in Loop: Header=BB11_84 Depth=2
	movq	144(%rsp), %rbx         # 8-byte Reload
	movq	(%r14,%rbx,8), %rbp
	movslq	%edi, %rdi
	cmpb	$0, (%rbp,%rdi)
	je	.LBB11_93
.LBB11_94:                              #   in Loop: Header=BB11_84 Depth=2
	movq	8(%rcx), %rdi
	movswq	%r13w, %rbp
	movq	(%rdi,%rbp,8), %rdi
	movq	(%rdi), %rdi
	movw	%r9w, (%rdi)
	movw	%r8w, %bp
.LBB11_95:                              #   in Loop: Header=BB11_84 Depth=2
	movw	%bp, 2(%rdi)
	movq	direct_ref_idx(%rip), %rdi
	movq	8(%rdi), %rdi
	movq	(%rdi,%rax,8), %rdi
	movb	%r13b, (%rdi,%rsi)
	jmp	.LBB11_97
	.p2align	4, 0x90
.LBB11_96:                              #   in Loop: Header=BB11_84 Depth=2
	movq	direct_ref_idx(%rip), %rdi
	movq	8(%rdi), %rdi
	movq	(%rdi,%rax,8), %rdi
	movb	$-1, (%rdi,%rsi)
	movq	8(%rcx), %rdi
	movq	(%rdi), %rdi
	movq	(%rdi), %rdi
	movl	$0, (%rdi)
.LBB11_97:                              #   in Loop: Header=BB11_84 Depth=2
	movq	img(%rip), %rdi
	cmpl	$0, 15268(%rdi)
	je	.LBB11_105
# BB#98:                                #   in Loop: Header=BB11_84 Depth=2
	movswl	%r12w, %ebp
	movq	(%rcx), %rbx
	testl	%ebp, %ebp
	cmovsl	%r15d, %ebp
	movq	(%rbx,%rbp,8), %rbp
	movq	(%rbp), %rbp
	movzwl	(%rbp), %ebx
	addl	$8192, %ebx             # imm = 0x2000
	movzwl	%bx, %ebx
	cmpl	$16383, %ebx            # imm = 0x3FFF
	ja	.LBB11_104
# BB#99:                                #   in Loop: Header=BB11_84 Depth=2
	movswl	2(%rbp), %ebx
	movslq	8(%rdi), %rdi
	leaq	(%rdi,%rdi,2), %rbp
	movl	LEVELMVLIMIT+16(,%rbp,8), %edi
	cmpl	%edi, %ebx
	jl	.LBB11_104
# BB#100:                               #   in Loop: Header=BB11_84 Depth=2
	movl	LEVELMVLIMIT+20(,%rbp,8), %ebp
	cmpl	%ebp, %ebx
	jg	.LBB11_104
# BB#101:                               #   in Loop: Header=BB11_84 Depth=2
	movswl	%r13w, %ebx
	movq	8(%rcx), %rcx
	testl	%ebx, %ebx
	cmovsl	%r15d, %ebx
	movq	(%rcx,%rbx,8), %rcx
	movq	(%rcx), %rcx
	movzwl	(%rcx), %ebx
	addl	$8192, %ebx             # imm = 0x2000
	movzwl	%bx, %ebx
	cmpl	$16383, %ebx            # imm = 0x3FFF
	jbe	.LBB11_102
	.p2align	4, 0x90
.LBB11_104:                             #   in Loop: Header=BB11_84 Depth=2
	movq	direct_ref_idx(%rip), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rax,8), %rcx
	movb	$-1, (%rcx,%rsi)
	movq	direct_ref_idx(%rip), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rax,8), %rcx
	movb	$-1, (%rcx,%rsi)
	movb	$-1, %cl
.LBB11_117:                             # %.critedge
                                        #   in Loop: Header=BB11_84 Depth=2
	movq	direct_pdir(%rip), %rdi
	movq	(%rdi,%rax,8), %rdi
	movb	%cl, (%rdi,%rsi)
	incq	%rdx
	cmpq	$4, %rdx
	jne	.LBB11_118
	jmp	.LBB11_119
.LBB11_87:                              #   in Loop: Header=BB11_84 Depth=2
	movq	(%rcx), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp), %rbp
	movl	$0, (%rbp)
	xorl	%ebp, %ebp
	jmp	.LBB11_90
.LBB11_93:                              #   in Loop: Header=BB11_84 Depth=2
	movq	8(%rcx), %rdi
	movq	(%rdi), %rdi
	movq	(%rdi), %rdi
	movw	$0, (%rdi)
	xorl	%ebp, %ebp
	jmp	.LBB11_95
.LBB11_102:                             #   in Loop: Header=BB11_84 Depth=2
	movswl	2(%rcx), %ecx
	cmpl	%edi, %ecx
	jl	.LBB11_104
# BB#103:                               #   in Loop: Header=BB11_84 Depth=2
	cmpl	%ebp, %ecx
	jg	.LBB11_104
	.p2align	4, 0x90
.LBB11_105:                             #   in Loop: Header=BB11_84 Depth=2
	movl	%r13d, %ecx
	andl	%r12d, %ecx
	testw	%cx, %cx
	jns	.LBB11_107
# BB#106:                               #   in Loop: Header=BB11_84 Depth=2
	movq	direct_ref_idx(%rip), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rax,8), %rcx
	movb	$0, (%rcx,%rsi)
	movq	direct_ref_idx(%rip), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rax,8), %rcx
	movb	$0, (%rcx,%rsi)
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
.LBB11_107:                             #   in Loop: Header=BB11_84 Depth=2
	movq	direct_ref_idx(%rip), %rcx
	movq	8(%rcx), %rdi
	movq	(%rdi,%rax,8), %rdi
	cmpb	$-1, (%rdi,%rsi)
	je	.LBB11_108
# BB#109:                               #   in Loop: Header=BB11_84 Depth=2
	movq	(%rcx), %rcx
	movq	(%rcx,%rax,8), %rdi
	cmpb	$-1, (%rdi,%rsi)
	je	.LBB11_110
# BB#111:                               #   in Loop: Header=BB11_84 Depth=2
	movq	active_pps(%rip), %rbp
	movb	$2, %cl
	cmpl	$1, 196(%rbp)
	jne	.LBB11_117
# BB#112:                               # %.preheader534
                                        #   in Loop: Header=BB11_84 Depth=2
	movq	%r14, %r8
	movq	wbp_weight(%rip), %rbp
	movswq	%r12w, %rbx
	movq	(%rbp), %r9
	movq	8(%rbp), %r15
	movq	(%r9,%rbx,8), %r9
	movswq	%r13w, %rbp
	movq	(%r9,%rbp,8), %r14
	movq	(%r15,%rbx,8), %rbx
	movq	(%rbx,%rbp,8), %r15
	movq	active_sps(%rip), %rbp
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB11_114:                             #   Parent Loop BB11_83 Depth=1
                                        #     Parent Loop BB11_84 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r14,%r9,4), %ebx
	movl	(%r15,%r9,4), %r11d
	leal	128(%rbx,%r11), %ebx
	cmpl	$256, %ebx              # imm = 0x100
	jae	.LBB11_115
# BB#113:                               #   in Loop: Header=BB11_114 Depth=3
	incq	%r9
	xorl	%ebx, %ebx
	cmpl	$0, 32(%rbp)
	setne	%bl
	leaq	1(%rbx,%rbx), %rbx
	cmpq	%rbx, %r9
	jl	.LBB11_114
	jmp	.LBB11_116
	.p2align	4, 0x90
.LBB11_119:                             #   in Loop: Header=BB11_83 Depth=1
	incq	%r10
	cmpq	$4, %r10
	jne	.LBB11_83
.LBB11_120:                             # %.loopexit533
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	Get_Direct_Motion_Vectors, .Lfunc_end11-Get_Direct_Motion_Vectors
	.cfi_endproc

	.type	QP2QUANT,@object        # @QP2QUANT
	.section	.rodata,"a",@progbits
	.globl	QP2QUANT
	.p2align	4
QP2QUANT:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	16                      # 0x10
	.long	18                      # 0x12
	.long	20                      # 0x14
	.long	23                      # 0x17
	.long	25                      # 0x19
	.long	29                      # 0x1d
	.long	32                      # 0x20
	.long	36                      # 0x24
	.long	40                      # 0x28
	.long	45                      # 0x2d
	.long	51                      # 0x33
	.long	57                      # 0x39
	.long	64                      # 0x40
	.long	72                      # 0x48
	.long	81                      # 0x51
	.long	91                      # 0x5b
	.size	QP2QUANT, 160

	.type	LEVELMVLIMIT,@object    # @LEVELMVLIMIT
	.globl	LEVELMVLIMIT
	.p2align	4
LEVELMVLIMIT:
	.long	4294967233              # 0xffffffc1
	.long	63                      # 0x3f
	.long	4294967168              # 0xffffff80
	.long	127                     # 0x7f
	.long	4294967040              # 0xffffff00
	.long	255                     # 0xff
	.long	4294967233              # 0xffffffc1
	.long	63                      # 0x3f
	.long	4294967168              # 0xffffff80
	.long	127                     # 0x7f
	.long	4294967040              # 0xffffff00
	.long	255                     # 0xff
	.long	4294967169              # 0xffffff81
	.long	127                     # 0x7f
	.long	4294967040              # 0xffffff00
	.long	255                     # 0xff
	.long	4294966784              # 0xfffffe00
	.long	511                     # 0x1ff
	.long	4294967169              # 0xffffff81
	.long	127                     # 0x7f
	.long	4294967040              # 0xffffff00
	.long	255                     # 0xff
	.long	4294966784              # 0xfffffe00
	.long	511                     # 0x1ff
	.long	4294967169              # 0xffffff81
	.long	127                     # 0x7f
	.long	4294967040              # 0xffffff00
	.long	255                     # 0xff
	.long	4294966784              # 0xfffffe00
	.long	511                     # 0x1ff
	.long	4294967169              # 0xffffff81
	.long	127                     # 0x7f
	.long	4294967040              # 0xffffff00
	.long	255                     # 0xff
	.long	4294966784              # 0xfffffe00
	.long	511                     # 0x1ff
	.long	4294967041              # 0xffffff01
	.long	255                     # 0xff
	.long	4294966784              # 0xfffffe00
	.long	511                     # 0x1ff
	.long	4294966272              # 0xfffffc00
	.long	1023                    # 0x3ff
	.long	4294967041              # 0xffffff01
	.long	255                     # 0xff
	.long	4294966784              # 0xfffffe00
	.long	511                     # 0x1ff
	.long	4294966272              # 0xfffffc00
	.long	1023                    # 0x3ff
	.long	4294967041              # 0xffffff01
	.long	255                     # 0xff
	.long	4294966784              # 0xfffffe00
	.long	511                     # 0x1ff
	.long	4294966272              # 0xfffffc00
	.long	1023                    # 0x3ff
	.long	4294966785              # 0xfffffe01
	.long	511                     # 0x1ff
	.long	4294966272              # 0xfffffc00
	.long	1023                    # 0x3ff
	.long	4294965248              # 0xfffff800
	.long	2047                    # 0x7ff
	.long	4294966785              # 0xfffffe01
	.long	511                     # 0x1ff
	.long	4294966272              # 0xfffffc00
	.long	1023                    # 0x3ff
	.long	4294965248              # 0xfffff800
	.long	2047                    # 0x7ff
	.long	4294966785              # 0xfffffe01
	.long	511                     # 0x1ff
	.long	4294966272              # 0xfffffc00
	.long	1023                    # 0x3ff
	.long	4294965248              # 0xfffff800
	.long	2047                    # 0x7ff
	.long	4294966785              # 0xfffffe01
	.long	511                     # 0x1ff
	.long	4294966272              # 0xfffffc00
	.long	1023                    # 0x3ff
	.long	4294965248              # 0xfffff800
	.long	2047                    # 0x7ff
	.long	4294966785              # 0xfffffe01
	.long	511                     # 0x1ff
	.long	4294966272              # 0xfffffc00
	.long	1023                    # 0x3ff
	.long	4294965248              # 0xfffff800
	.long	2047                    # 0x7ff
	.long	4294966785              # 0xfffffe01
	.long	511                     # 0x1ff
	.long	4294966272              # 0xfffffc00
	.long	1023                    # 0x3ff
	.long	4294965248              # 0xfffff800
	.long	2047                    # 0x7ff
	.long	4294966785              # 0xfffffe01
	.long	511                     # 0x1ff
	.long	4294966272              # 0xfffffc00
	.long	1023                    # 0x3ff
	.long	4294965248              # 0xfffff800
	.long	2047                    # 0x7ff
	.long	4294966785              # 0xfffffe01
	.long	511                     # 0x1ff
	.long	4294966272              # 0xfffffc00
	.long	1023                    # 0x3ff
	.long	4294965248              # 0xfffff800
	.long	2047                    # 0x7ff
	.size	LEVELMVLIMIT, 408

	.type	max_mvd,@object         # @max_mvd
	.comm	max_mvd,4,4
	.type	byte_abs_range,@object  # @byte_abs_range
	.comm	byte_abs_range,4,4
	.type	spiral_search_x,@object # @spiral_search_x
	.comm	spiral_search_x,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Init_Motion_Search_Module: spiral_search_x"
	.size	.L.str, 43

	.type	spiral_search_y,@object # @spiral_search_y
	.comm	spiral_search_y,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Init_Motion_Search_Module: spiral_search_y"
	.size	.L.str.1, 43

	.type	spiral_hpel_search_x,@object # @spiral_hpel_search_x
	.comm	spiral_hpel_search_x,8,8
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Init_Motion_Search_Module: spiral_hpel_search_x"
	.size	.L.str.2, 48

	.type	spiral_hpel_search_y,@object # @spiral_hpel_search_y
	.comm	spiral_hpel_search_y,8,8
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Init_Motion_Search_Module: spiral_hpel_search_y"
	.size	.L.str.3, 48

	.type	mvbits,@object          # @mvbits
	.comm	mvbits,8,8
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Init_Motion_Search_Module: mvbits"
	.size	.L.str.4, 34

	.type	refbits,@object         # @refbits
	.comm	refbits,8,8
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Init_Motion_Search_Module: refbits"
	.size	.L.str.5, 35

	.type	byte_abs,@object        # @byte_abs
	.comm	byte_abs,8,8
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Init_Motion_Search_Module: byte_abs"
	.size	.L.str.6, 36

	.type	motion_cost,@object     # @motion_cost
	.comm	motion_cost,8,8
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	diff64,@object          # @diff64
	.local	diff64
	.comm	diff64,256,16
	.type	orig_pic,@object        # @orig_pic
	.local	orig_pic
	.comm	orig_pic,1536,16
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	BlockMotionSearch.tstruct1,@object # @BlockMotionSearch.tstruct1
	.local	BlockMotionSearch.tstruct1
	.comm	BlockMotionSearch.tstruct1,16,8
	.type	BlockMotionSearch.tstruct2,@object # @BlockMotionSearch.tstruct2
	.local	BlockMotionSearch.tstruct2
	.comm	BlockMotionSearch.tstruct2,16,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	UMHEX_blocktype,@object # @UMHEX_blocktype
	.comm	UMHEX_blocktype,4,4
	.type	bipred_flag,@object     # @bipred_flag
	.comm	bipred_flag,4,4
	.type	smpUMHEX_l0_cost,@object # @smpUMHEX_l0_cost
	.comm	smpUMHEX_l0_cost,8,8
	.type	smpUMHEX_l1_cost,@object # @smpUMHEX_l1_cost
	.comm	smpUMHEX_l1_cost,8,8
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	diff,@object            # @diff
	.local	diff
	.comm	diff,64,16
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	PartitionMotionSearch.bx0,@object # @PartitionMotionSearch.bx0
	.section	.rodata,"a",@progbits
	.p2align	4
PartitionMotionSearch.bx0:
	.zero	16
	.zero	16
	.zero	16
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	2                       # 0x2
	.size	PartitionMotionSearch.bx0, 80

	.type	PartitionMotionSearch.by0,@object # @PartitionMotionSearch.by0
	.p2align	4
PartitionMotionSearch.by0:
	.zero	16
	.zero	16
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.zero	16
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	2                       # 0x2
	.size	PartitionMotionSearch.by0, 80

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	height_pad,@object      # @height_pad
	.comm	height_pad,4,4
	.type	width_pad,@object       # @width_pad
	.comm	width_pad,4,4
	.type	height_pad_cr,@object   # @height_pad_cr
	.comm	height_pad_cr,4,4
	.type	width_pad_cr,@object    # @width_pad_cr
	.comm	width_pad_cr,4,4
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	McostState,@object      # @McostState
	.comm	McostState,8,8
	.type	SearchState,@object     # @SearchState
	.comm	SearchState,8,8
	.type	fastme_ref_cost,@object # @fastme_ref_cost
	.comm	fastme_ref_cost,8,8
	.type	fastme_l0_cost,@object  # @fastme_l0_cost
	.comm	fastme_l0_cost,8,8
	.type	fastme_l1_cost,@object  # @fastme_l1_cost
	.comm	fastme_l1_cost,8,8
	.type	fastme_l0_cost_bipred,@object # @fastme_l0_cost_bipred
	.comm	fastme_l0_cost_bipred,8,8
	.type	fastme_l1_cost_bipred,@object # @fastme_l1_cost_bipred
	.comm	fastme_l1_cost_bipred,8,8
	.type	fastme_best_cost,@object # @fastme_best_cost
	.comm	fastme_best_cost,8,8
	.type	pred_SAD,@object        # @pred_SAD
	.comm	pred_SAD,4,4
	.type	pred_MV_ref,@object     # @pred_MV_ref
	.comm	pred_MV_ref,8,4
	.type	pred_MV_uplayer,@object # @pred_MV_uplayer
	.comm	pred_MV_uplayer,8,4
	.type	predict_point,@object   # @predict_point
	.comm	predict_point,40,16
	.type	SAD_a,@object           # @SAD_a
	.comm	SAD_a,4,4
	.type	SAD_b,@object           # @SAD_b
	.comm	SAD_b,4,4
	.type	SAD_c,@object           # @SAD_c
	.comm	SAD_c,4,4
	.type	SAD_d,@object           # @SAD_d
	.comm	SAD_d,4,4
	.type	Threshold_DSR_MB,@object # @Threshold_DSR_MB
	.comm	Threshold_DSR_MB,32,16
	.type	Bsize,@object           # @Bsize
	.comm	Bsize,32,16
	.type	AlphaFourth_1,@object   # @AlphaFourth_1
	.comm	AlphaFourth_1,32,16
	.type	AlphaFourth_2,@object   # @AlphaFourth_2
	.comm	AlphaFourth_2,32,16
	.type	flag_intra,@object      # @flag_intra
	.comm	flag_intra,8,8
	.type	flag_intra_SAD,@object  # @flag_intra_SAD
	.comm	flag_intra_SAD,4,4
	.type	SymmetricalCrossSearchThreshold1,@object # @SymmetricalCrossSearchThreshold1
	.comm	SymmetricalCrossSearchThreshold1,2,2
	.type	SymmetricalCrossSearchThreshold2,@object # @SymmetricalCrossSearchThreshold2
	.comm	SymmetricalCrossSearchThreshold2,2,2
	.type	ConvergeThreshold,@object # @ConvergeThreshold
	.comm	ConvergeThreshold,2,2
	.type	SubPelThreshold1,@object # @SubPelThreshold1
	.comm	SubPelThreshold1,2,2
	.type	SubPelThreshold3,@object # @SubPelThreshold3
	.comm	SubPelThreshold3,2,2
	.type	smpUMHEX_SearchState,@object # @smpUMHEX_SearchState
	.comm	smpUMHEX_SearchState,8,8
	.type	smpUMHEX_flag_intra,@object # @smpUMHEX_flag_intra
	.comm	smpUMHEX_flag_intra,8,8
	.type	smpUMHEX_flag_intra_SAD,@object # @smpUMHEX_flag_intra_SAD
	.comm	smpUMHEX_flag_intra_SAD,4,4
	.type	smpUMHEX_pred_SAD_uplayer,@object # @smpUMHEX_pred_SAD_uplayer
	.comm	smpUMHEX_pred_SAD_uplayer,4,4
	.type	smpUMHEX_pred_MV_uplayer_X,@object # @smpUMHEX_pred_MV_uplayer_X
	.comm	smpUMHEX_pred_MV_uplayer_X,2,2
	.type	smpUMHEX_pred_MV_uplayer_Y,@object # @smpUMHEX_pred_MV_uplayer_Y
	.comm	smpUMHEX_pred_MV_uplayer_Y,2,2

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
