	.text
	.file	"OpenCallbackConsole.bc"
	.globl	_ZN20COpenCallbackConsole15Open_CheckBreakEv
	.p2align	4, 0x90
	.type	_ZN20COpenCallbackConsole15Open_CheckBreakEv,@function
_ZN20COpenCallbackConsole15Open_CheckBreakEv: # @_ZN20COpenCallbackConsole15Open_CheckBreakEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	callq	_ZN13NConsoleClose15TestBreakSignalEv
	xorl	%ecx, %ecx
	testb	%al, %al
	movl	$-2147467260, %eax      # imm = 0x80004004
	cmovel	%ecx, %eax
	popq	%rcx
	retq
.Lfunc_end0:
	.size	_ZN20COpenCallbackConsole15Open_CheckBreakEv, .Lfunc_end0-_ZN20COpenCallbackConsole15Open_CheckBreakEv
	.cfi_endproc

	.globl	_ZN20COpenCallbackConsole13Open_SetTotalEPKyS1_
	.p2align	4, 0x90
	.type	_ZN20COpenCallbackConsole13Open_SetTotalEPKyS1_,@function
_ZN20COpenCallbackConsole13Open_SetTotalEPKyS1_: # @_ZN20COpenCallbackConsole13Open_SetTotalEPKyS1_
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	jmpq	*(%rax)                 # TAILCALL
.Lfunc_end1:
	.size	_ZN20COpenCallbackConsole13Open_SetTotalEPKyS1_, .Lfunc_end1-_ZN20COpenCallbackConsole13Open_SetTotalEPKyS1_
	.cfi_endproc

	.globl	_ZN20COpenCallbackConsole17Open_SetCompletedEPKyS1_
	.p2align	4, 0x90
	.type	_ZN20COpenCallbackConsole17Open_SetCompletedEPKyS1_,@function
_ZN20COpenCallbackConsole17Open_SetCompletedEPKyS1_: # @_ZN20COpenCallbackConsole17Open_SetCompletedEPKyS1_
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	jmpq	*(%rax)                 # TAILCALL
.Lfunc_end2:
	.size	_ZN20COpenCallbackConsole17Open_SetCompletedEPKyS1_, .Lfunc_end2-_ZN20COpenCallbackConsole17Open_SetCompletedEPKyS1_
	.cfi_endproc

	.globl	_ZN20COpenCallbackConsole26Open_CryptoGetTextPasswordEPPw
	.p2align	4, 0x90
	.type	_ZN20COpenCallbackConsole26Open_CryptoGetTextPasswordEPPw,@function
_ZN20COpenCallbackConsole26Open_CryptoGetTextPasswordEPPw: # @_ZN20COpenCallbackConsole26Open_CryptoGetTextPasswordEPPw
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 80
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movb	$1, 17(%r15)
	movq	(%r15), %rax
	callq	*(%rax)
	testl	%eax, %eax
	jne	.LBB3_17
# BB#1:
	cmpb	$0, 16(%r15)
	jne	.LBB3_16
# BB#2:
	movq	8(%r15), %rsi
	leaq	8(%rsp), %rbx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	_Z11GetPasswordP13CStdOutStreamb
	leaq	24(%r15), %rax
	cmpq	%rax, %rbx
	je	.LBB3_3
# BB#4:
	movl	$0, 32(%r15)
	movq	24(%r15), %rbx
	movl	$0, (%rbx)
	movslq	16(%rsp), %r12
	incq	%r12
	movl	36(%r15), %ebp
	cmpl	%ebp, %r12d
	je	.LBB3_10
# BB#5:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp0:
	callq	_Znam
	movq	%rax, %r13
.Ltmp1:
# BB#6:                                 # %.noexc
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB3_9
# BB#7:                                 # %.noexc
	testl	%ebp, %ebp
	jle	.LBB3_9
# BB#8:                                 # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	32(%r15), %rax
.LBB3_9:                                # %._crit_edge16.i.i
	movq	%r13, 24(%r15)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 36(%r15)
	movq	%r13, %rbx
.LBB3_10:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_11:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB3_11
# BB#12:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	16(%rsp), %eax
	movl	%eax, 32(%r15)
	testq	%rdi, %rdi
	jne	.LBB3_14
	jmp	.LBB3_15
.LBB3_3:                                # %._ZN11CStringBaseIwEaSERKS0_.exit_crit_edge
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_15
.LBB3_14:
	callq	_ZdaPv
.LBB3_15:                               # %_ZN11CStringBaseIwED2Ev.exit
	movb	$1, 16(%r15)
.LBB3_16:
	movq	24(%r15), %rdi
	callq	SysAllocString
	movq	%rax, (%r14)
	xorl	%ecx, %ecx
	testq	%rax, %rax
	movl	$-2147024882, %eax      # imm = 0x8007000E
	cmovnel	%ecx, %eax
.LBB3_17:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_18:
.Ltmp2:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_20
# BB#19:
	callq	_ZdaPv
.LBB3_20:                               # %_ZN11CStringBaseIwED2Ev.exit9
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN20COpenCallbackConsole26Open_CryptoGetTextPasswordEPPw, .Lfunc_end3-_ZN20COpenCallbackConsole26Open_CryptoGetTextPasswordEPPw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end3-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN20COpenCallbackConsole21Open_GetPasswordIfAnyER11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN20COpenCallbackConsole21Open_GetPasswordIfAnyER11CStringBaseIwE,@function
_ZN20COpenCallbackConsole21Open_GetPasswordIfAnyER11CStringBaseIwE: # @_ZN20COpenCallbackConsole21Open_GetPasswordIfAnyER11CStringBaseIwE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 64
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	cmpb	$0, 16(%r14)
	je	.LBB4_10
# BB#1:
	leaq	24(%r14), %rbp
	cmpq	%r15, %rbp
	je	.LBB4_10
# BB#2:
	movl	$0, 8(%r15)
	movq	(%r15), %rbx
	movl	$0, (%rbx)
	movslq	32(%r14), %r12
	incq	%r12
	movl	12(%r15), %r13d
	cmpl	%r13d, %r12d
	je	.LBB4_7
# BB#3:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB4_6
# BB#4:
	testl	%r13d, %r13d
	jle	.LBB4_6
# BB#5:                                 # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movslq	8(%r15), %rcx
.LBB4_6:                                # %._crit_edge16.i.i
	movq	%rax, (%r15)
	movl	$0, (%rax,%rcx,4)
	movl	%r12d, 12(%r15)
	movq	%rax, %rbx
.LBB4_7:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%rbp), %rax
	.p2align	4, 0x90
.LBB4_8:                                # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB4_8
# BB#9:                                 # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	32(%r14), %eax
	movl	%eax, 8(%r15)
.LBB4_10:                               # %_ZN11CStringBaseIwEaSERKS0_.exit
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN20COpenCallbackConsole21Open_GetPasswordIfAnyER11CStringBaseIwE, .Lfunc_end4-_ZN20COpenCallbackConsole21Open_GetPasswordIfAnyER11CStringBaseIwE
	.cfi_endproc

	.globl	_ZN20COpenCallbackConsole21Open_WasPasswordAskedEv
	.p2align	4, 0x90
	.type	_ZN20COpenCallbackConsole21Open_WasPasswordAskedEv,@function
_ZN20COpenCallbackConsole21Open_WasPasswordAskedEv: # @_ZN20COpenCallbackConsole21Open_WasPasswordAskedEv
	.cfi_startproc
# BB#0:
	movb	17(%rdi), %al
	retq
.Lfunc_end5:
	.size	_ZN20COpenCallbackConsole21Open_WasPasswordAskedEv, .Lfunc_end5-_ZN20COpenCallbackConsole21Open_WasPasswordAskedEv
	.cfi_endproc

	.globl	_ZN20COpenCallbackConsole30Open_ClearPasswordWasAskedFlagEv
	.p2align	4, 0x90
	.type	_ZN20COpenCallbackConsole30Open_ClearPasswordWasAskedFlagEv,@function
_ZN20COpenCallbackConsole30Open_ClearPasswordWasAskedFlagEv: # @_ZN20COpenCallbackConsole30Open_ClearPasswordWasAskedFlagEv
	.cfi_startproc
# BB#0:
	movb	$0, 17(%rdi)
	retq
.Lfunc_end6:
	.size	_ZN20COpenCallbackConsole30Open_ClearPasswordWasAskedFlagEv, .Lfunc_end6-_ZN20COpenCallbackConsole30Open_ClearPasswordWasAskedFlagEv
	.cfi_endproc

	.type	_ZTV20COpenCallbackConsole,@object # @_ZTV20COpenCallbackConsole
	.section	.rodata,"a",@progbits
	.globl	_ZTV20COpenCallbackConsole
	.p2align	3
_ZTV20COpenCallbackConsole:
	.quad	0
	.quad	_ZTI20COpenCallbackConsole
	.quad	_ZN20COpenCallbackConsole15Open_CheckBreakEv
	.quad	_ZN20COpenCallbackConsole13Open_SetTotalEPKyS1_
	.quad	_ZN20COpenCallbackConsole17Open_SetCompletedEPKyS1_
	.quad	_ZN20COpenCallbackConsole26Open_CryptoGetTextPasswordEPPw
	.quad	_ZN20COpenCallbackConsole21Open_GetPasswordIfAnyER11CStringBaseIwE
	.quad	_ZN20COpenCallbackConsole21Open_WasPasswordAskedEv
	.quad	_ZN20COpenCallbackConsole30Open_ClearPasswordWasAskedFlagEv
	.size	_ZTV20COpenCallbackConsole, 72

	.type	_ZTS20COpenCallbackConsole,@object # @_ZTS20COpenCallbackConsole
	.globl	_ZTS20COpenCallbackConsole
	.p2align	4
_ZTS20COpenCallbackConsole:
	.asciz	"20COpenCallbackConsole"
	.size	_ZTS20COpenCallbackConsole, 23

	.type	_ZTS15IOpenCallbackUI,@object # @_ZTS15IOpenCallbackUI
	.section	.rodata._ZTS15IOpenCallbackUI,"aG",@progbits,_ZTS15IOpenCallbackUI,comdat
	.weak	_ZTS15IOpenCallbackUI
	.p2align	4
_ZTS15IOpenCallbackUI:
	.asciz	"15IOpenCallbackUI"
	.size	_ZTS15IOpenCallbackUI, 18

	.type	_ZTI15IOpenCallbackUI,@object # @_ZTI15IOpenCallbackUI
	.section	.rodata._ZTI15IOpenCallbackUI,"aG",@progbits,_ZTI15IOpenCallbackUI,comdat
	.weak	_ZTI15IOpenCallbackUI
	.p2align	3
_ZTI15IOpenCallbackUI:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS15IOpenCallbackUI
	.size	_ZTI15IOpenCallbackUI, 16

	.type	_ZTI20COpenCallbackConsole,@object # @_ZTI20COpenCallbackConsole
	.section	.rodata,"a",@progbits
	.globl	_ZTI20COpenCallbackConsole
	.p2align	4
_ZTI20COpenCallbackConsole:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20COpenCallbackConsole
	.quad	_ZTI15IOpenCallbackUI
	.size	_ZTI20COpenCallbackConsole, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
