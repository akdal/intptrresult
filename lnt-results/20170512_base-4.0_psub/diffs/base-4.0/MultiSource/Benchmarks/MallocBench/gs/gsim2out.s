	.text
	.file	"gsim2out.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4616189618054758400     # double 4
.LCPI0_1:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	gs_type1imagepath
	.p2align	4, 0x90
	.type	gs_type1imagepath,@function
gs_type1imagepath:                      # @gs_type1imagepath
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$328, %rsp              # imm = 0x148
.Lcfi6:
	.cfi_def_cfa_offset 384
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	movq	%r8, %rbx
	movsd	%xmm3, 24(%rsp)         # 8-byte Spill
	movsd	%xmm2, 40(%rsp)         # 8-byte Spill
	movsd	%xmm1, 72(%rsp)         # 8-byte Spill
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	movl	%ecx, %r13d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rsi, %r14
	leal	(,%r13,4), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	leaq	80(%rsp), %rsi
	callq	gs_currentmatrix
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	leaq	232(%rsp), %rdi
	movapd	%xmm0, %xmm1
	callq	gs_make_scaling
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB0_89
# BB#1:
	leaq	232(%rsp), %rdi
	leaq	80(%rsp), %rsi
	movq	%rsi, %rdx
	callq	gs_matrix_multiply
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB0_89
# BB#2:
	leaq	80(%rsp), %rdi
	movq	%rdi, %rsi
	callq	gs_matrix_invert
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB0_89
# BB#3:
	movq	(%rsp), %rax            # 8-byte Reload
	leal	2(%rax), %r15d
	leal	2(%r13), %ebp
	imull	%r15d, %ebp
	movl	$1, %esi
	movl	$.L.str, %edx
	movl	%ebp, %edi
	callq	gs_malloc
	testq	%rax, %rax
	je	.LBB0_4
# BB#5:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movl	%ebp, 36(%rsp)          # 4-byte Spill
	movslq	%ebp, %rdx
	xorl	%esi, %esi
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, %rdi
	callq	memset
	testl	%r13d, %r13d
	jle	.LBB0_25
# BB#6:                                 # %.preheader.lr.ph.i
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB0_25
# BB#7:                                 # %.preheader.us.preheader.i
	movq	(%rsp), %rcx            # 8-byte Reload
	leal	2(%rcx,%rcx), %eax
	movslq	%eax, %r8
	negq	%r8
	movl	%r15d, %eax
	imull	%r13d, %eax
	cltq
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	1(%rdx,%rax), %rax
	decq	%r14
	leal	-1(%rcx), %r10d
	leaq	1(%r10), %r9
	movl	%ecx, %r11d
	andl	$1, %r11d
	xorl	%ebx, %ebx
                                        # implicit-def: %EBP
	.p2align	4, 0x90
.LBB0_8:                                # %.preheader.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_15 Depth 2
	testl	%r11d, %r11d
	jne	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=1
	xorl	%edi, %edi
	xorl	%esi, %esi
	movq	%rax, %rcx
	testl	%r10d, %r10d
	jne	.LBB0_14
	jmp	.LBB0_24
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_8 Depth=1
	movzbl	1(%r14), %ebp
	testb	$-128, %bpl
	je	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_8 Depth=1
	movb	$1, (%rax)
.LBB0_12:                               #   in Loop: Header=BB0_8 Depth=1
	incq	%r14
	movl	$64, %esi
	leaq	1(%rax), %rcx
	movl	$1, %edi
	testl	%r10d, %r10d
	je	.LBB0_24
.LBB0_14:                               # %.preheader.us.i.new
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	subl	%edi, %edx
	incq	%rcx
	.p2align	4, 0x90
.LBB0_15:                               #   Parent Loop BB0_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%esi, %esi
	jne	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_15 Depth=2
	movzbl	1(%r14), %ebp
	incq	%r14
	movl	$128, %esi
.LBB0_17:                               #   in Loop: Header=BB0_15 Depth=2
	testl	%esi, %ebp
	je	.LBB0_19
# BB#18:                                #   in Loop: Header=BB0_15 Depth=2
	movb	$1, -1(%rcx)
.LBB0_19:                               #   in Loop: Header=BB0_15 Depth=2
	sarl	%esi
	jne	.LBB0_21
# BB#20:                                #   in Loop: Header=BB0_15 Depth=2
	movzbl	1(%r14), %ebp
	incq	%r14
	movl	$128, %esi
.LBB0_21:                               #   in Loop: Header=BB0_15 Depth=2
	testl	%esi, %ebp
	je	.LBB0_23
# BB#22:                                #   in Loop: Header=BB0_15 Depth=2
	movb	$1, (%rcx)
.LBB0_23:                               #   in Loop: Header=BB0_15 Depth=2
	sarl	%esi
	addq	$2, %rcx
	addl	$-2, %edx
	jne	.LBB0_15
.LBB0_24:                               # %._crit_edge.us.i
                                        #   in Loop: Header=BB0_8 Depth=1
	addq	%r9, %rax
	addq	%r8, %rax
	incl	%ebx
	cmpl	%r13d, %ebx
	jne	.LBB0_8
.LBB0_25:                               # %fill_cells.exit
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 192(%rsp)
	movl	%r12d, %eax
	addq	%rcx, %rax
	movq	%rax, 176(%rsp)
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	movl	$0, %r12d
	jle	.LBB0_33
# BB#26:                                # %.preheader.lr.ph
	testl	%r13d, %r13d
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, %r12d
	jle	.LBB0_33
# BB#27:                                # %.preheader.preheader
	movslq	%r13d, %rax
	movq	(%rsp), %rcx            # 8-byte Reload
	leal	3(%rcx), %ecx
	xorl	%edx, %edx
.LBB0_28:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_30 Depth 2
	leal	1(%rdx), %r12d
	movl	%ecx, %esi
	xorl	%edi, %edi
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_30:                               #   Parent Loop BB0_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%esi, %rbp
	cmpb	$0, (%rbx,%rbp)
	jne	.LBB0_31
# BB#29:                                #   in Loop: Header=BB0_30 Depth=2
	incq	%rdi
	addl	%r15d, %esi
	cmpq	%rax, %rdi
	jl	.LBB0_30
# BB#32:                                # %._crit_edge
                                        #   in Loop: Header=BB0_28 Depth=1
	incl	%ecx
	cmpl	(%rsp), %r12d           # 4-byte Folded Reload
	movl	%r12d, %edx
	jl	.LBB0_28
	jmp	.LBB0_33
.LBB0_4:
	movl	$-25, %ebp
	jmp	.LBB0_89
.LBB0_31:
	movl	%edx, %r12d
.LBB0_33:                               # %.loopexit
	movsd	.LCPI0_0(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	%xmm2, %xmm0
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm2, %xmm1
	leaq	80(%rsp), %rdi
	leaq	232(%rsp), %rsi
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	callq	gs_distance_transform
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB0_89
# BB#34:
	movsd	64(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	mulsd	%xmm2, %xmm0
	movsd	72(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm2, %xmm1
	leaq	80(%rsp), %rdi
	leaq	56(%rsp), %rsi
	callq	gs_distance_transform
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB0_89
# BB#35:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	subsd	40(%rsp), %xmm0         # 8-byte Folded Reload
	mulsd	.LCPI0_0(%rip), %xmm0
	leaq	80(%rsp), %rdi
	leaq	48(%rsp), %rsi
	xorpd	%xmm1, %xmm1
	callq	gs_distance_transform
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB0_89
# BB#36:
	movss	232(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm0, %xmm1
	movsd	.LCPI0_1(%rip), %xmm0   # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %rax
	cmpq	$-32767, %rax           # imm = 0x8001
	movl	$-32767, %esi           # imm = 0x8001
	movl	$-32767, %ecx           # imm = 0x8001
	cmovgl	%eax, %ecx
	cmpq	$32767, %rax            # imm = 0x7FFF
	movl	$32767, %edi            # imm = 0x7FFF
	cmovgl	%edi, %ecx
	movl	%ecx, 184(%rsp)
	movss	236(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %rax
	cmpq	$-32767, %rax           # imm = 0x8001
	movl	%eax, %ecx
	cmovlel	%esi, %ecx
	cmpq	$32767, %rax            # imm = 0x7FFF
	cmovgl	%edi, %ecx
	movl	%ecx, 188(%rsp)
	movss	56(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	60(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %rax
	cmpq	$-32767, %rax           # imm = 0x8001
	movl	%eax, %r14d
	cmovlel	%esi, %r14d
	cmpq	$32767, %rax            # imm = 0x7FFF
	cmovgl	%edi, %r14d
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %rax
	cmpq	$-32767, %rax           # imm = 0x8001
	movl	%eax, %edx
	cmovlel	%esi, %edx
	cmpq	$32767, %rax            # imm = 0x7FFF
	cmovgl	%edi, %edx
	movq	%rdx, %r10
	movss	48(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	52(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %rax
	cmpq	$-32767, %rax           # imm = 0x8001
	movl	%eax, %ebx
	cmovlel	%esi, %ebx
	cmpq	$32767, %rax            # imm = 0x7FFF
	cmovgl	%edi, %ebx
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %rax
	cmpq	$-32767, %rax           # imm = 0x8001
	movl	%eax, %edx
	cmovlel	%esi, %edx
	cmpq	$32767, %rax            # imm = 0x7FFF
	cmovgl	%edi, %edx
	movq	%rdx, %r9
	movq	192(%rsp), %rdx
	leaq	5(%rdx), %rax
	movl	$-13, %ebp
	cmpq	176(%rsp), %rax
	ja	.LBB0_89
# BB#37:
	leal	107(%rbx), %eax
	cmpl	$214, %eax
	ja	.LBB0_39
# BB#38:
	movq	%rbx, %r8
	leal	139(%rbx), %ebx
	movl	$1, %esi
	movq	%rdx, %rax
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB0_45
.LBB0_39:
	movq	%rbx, %rsi
	leal	-108(%rsi), %ebx
	cmpl	$1023, %ebx             # imm = 0x3FF
	movq	8(%rsp), %rdi           # 8-byte Reload
	ja	.LBB0_42
# BB#40:
	movq	%rsi, %r8
	movl	%ebx, %esi
	shrl	$8, %esi
	addl	$247, %esi
	jmp	.LBB0_41
.LBB0_42:
	leal	1131(%rsi), %eax
	cmpl	$1023, %eax             # imm = 0x3FF
	movq	%rsi, %rbx
	ja	.LBB0_44
# BB#43:
	movq	%rbx, %rax
	movl	$-108, %ebx
	movq	%rax, %r8
	subl	%eax, %ebx
	movl	%ebx, %esi
	shrl	$8, %esi
	addl	$251, %esi
.LBB0_41:
	leaq	1(%rdx), %rax
	movb	%sil, (%rdx)
	movl	$2, %esi
	jmp	.LBB0_45
.LBB0_44:
	movb	$-1, (%rdx)
	movslq	%ebx, %rax
	movq	%rax, %rsi
	shrq	$24, %rsi
	movb	%sil, 1(%rdx)
	shrq	$16, %rax
	movb	%al, 2(%rdx)
	leaq	4(%rdx), %rax
	movb	%bh, 3(%rdx)  # NOREX
	movl	$5, %esi
	movq	%rbx, %r8
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill>
.LBB0_45:
	leaq	(%rdx,%rsi), %rcx
	movb	%bl, (%rax)
	movq	%rcx, %r15
	movq	%rcx, 192(%rsp)
	movq	%r9, %r11
	movl	%r11d, %eax
	movq	%r10, %rbx
	orl	%ebx, %eax
	leaq	5(%rdx,%rsi), %rax
	movq	176(%rsp), %rsi
	je	.LBB0_75
# BB#46:
	cmpq	%rsi, %rax
	movq	%r14, %rcx
	ja	.LBB0_89
# BB#47:
	leal	107(%r11), %eax
	cmpl	$214, %eax
	ja	.LBB0_49
# BB#48:
	movq	%r11, %rbx
	leal	139(%rbx), %ebx
	movl	$1, %esi
	movq	%r15, %rax
	movq	%rax, %r11
	jmp	.LBB0_55
.LBB0_75:
	cmpq	%rsi, %rax
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	%r14, %rcx
	ja	.LBB0_89
# BB#76:
	leal	107(%rcx), %eax
	cmpl	$214, %eax
	ja	.LBB0_78
# BB#77:
	addl	$139, %ecx
	movl	$1, %esi
	movl	%ecx, %edx
	movq	%r15, %rbx
	movq	%rbx, %rax
	jmp	.LBB0_84
.LBB0_49:
	movq	%r11, %rdx
	leal	-108(%rdx), %ebx
	cmpl	$1023, %ebx             # imm = 0x3FF
	ja	.LBB0_52
# BB#50:
	movl	%ebx, %esi
	shrl	$8, %esi
	addl	$247, %esi
	jmp	.LBB0_51
.LBB0_78:
	leal	-108(%rcx), %edx
	cmpl	$1023, %edx             # imm = 0x3FF
	movq	%r15, %rbx
	ja	.LBB0_81
# BB#79:
	movl	%edx, %ecx
	shrl	$8, %ecx
	addl	$247, %ecx
	jmp	.LBB0_80
.LBB0_52:
	leal	1131(%rdx), %eax
	cmpl	$1023, %eax             # imm = 0x3FF
	ja	.LBB0_54
# BB#53:
	movl	$-108, %ebx
	subl	%edx, %ebx
	movl	%ebx, %esi
	shrl	$8, %esi
	addl	$251, %esi
.LBB0_51:
	movq	%r15, %rax
	leaq	1(%rax), %r11
	movb	%sil, (%rax)
	movl	$2, %esi
	jmp	.LBB0_55
.LBB0_81:
	leal	1131(%rcx), %eax
	cmpl	$1023, %eax             # imm = 0x3FF
	ja	.LBB0_83
# BB#82:
	movl	$-108, %edx
	subl	%ecx, %edx
	movl	%edx, %ecx
	shrl	$8, %ecx
	addl	$251, %ecx
.LBB0_80:
	leaq	1(%rbx), %rax
	movb	%cl, (%rbx)
	movl	$2, %esi
	jmp	.LBB0_84
.LBB0_54:
	movq	%r15, %rax
	movb	$-1, (%rax)
	movslq	%edx, %rbx
	movq	%rbx, %rsi
	shrq	$24, %rsi
	movb	%sil, 1(%rax)
	shrq	$16, %rbx
	movb	%bl, 2(%rax)
	leaq	4(%rax), %r11
	movb	%dh, 3(%rax)  # NOREX
	movl	$5, %esi
	movl	%edx, %ebx
.LBB0_55:
	leaq	(%rax,%rsi), %rdx
	movb	%bl, (%r11)
	movq	%rdx, 192(%rsp)
	leaq	5(%rax,%rsi), %rax
	cmpq	176(%rsp), %rax
	ja	.LBB0_89
# BB#56:
	leal	107(%rcx), %eax
	cmpl	$214, %eax
	ja	.LBB0_58
# BB#57:
	addl	$139, %ecx
	movl	$1, %ebx
	movl	%ecx, %esi
	movq	%rdx, %rax
	jmp	.LBB0_64
.LBB0_58:
	leal	-108(%rcx), %esi
	cmpl	$1023, %esi             # imm = 0x3FF
	ja	.LBB0_61
# BB#59:
	movl	%esi, %ecx
	shrl	$8, %ecx
	addl	$247, %ecx
	jmp	.LBB0_60
.LBB0_61:
	leal	1131(%rcx), %eax
	cmpl	$1023, %eax             # imm = 0x3FF
	ja	.LBB0_63
# BB#62:
	movl	$-108, %esi
	subl	%ecx, %esi
	movl	%esi, %ecx
	shrl	$8, %ecx
	addl	$251, %ecx
.LBB0_60:
	leaq	1(%rdx), %rax
	movb	%cl, (%rdx)
	movl	$2, %ebx
	jmp	.LBB0_64
.LBB0_83:
	movb	$-1, (%rbx)
	movslq	%ecx, %rax
	movq	%rax, %rdx
	shrq	$24, %rdx
	movb	%dl, 1(%rbx)
	shrq	$16, %rax
	movb	%al, 2(%rbx)
	leaq	4(%rbx), %rax
	movb	%ch, 3(%rbx)  # NOREX
	movl	$5, %esi
	movl	%ecx, %edx
.LBB0_84:
	leaq	(%rbx,%rsi), %rcx
	movb	%dl, (%rax)
	movq	%rcx, 192(%rsp)
	leaq	1(%rbx,%rsi), %rax
	cmpq	176(%rsp), %rax
	ja	.LBB0_89
# BB#85:
	movq	%r9, %r14
	movq	%rax, 192(%rsp)
	movb	$13, (%rcx)
	jmp	.LBB0_86
.LBB0_63:
	movb	$-1, (%rdx)
	movslq	%ecx, %rax
	movq	%rax, %rsi
	shrq	$24, %rsi
	movb	%sil, 1(%rdx)
	shrq	$16, %rax
	movb	%al, 2(%rdx)
	leaq	4(%rdx), %rax
	movb	%ch, 3(%rdx)  # NOREX
	movl	$5, %ebx
	movl	%ecx, %esi
.LBB0_64:
	leaq	(%rdx,%rbx), %rcx
	movb	%sil, (%rax)
	movq	%rcx, 192(%rsp)
	leaq	5(%rdx,%rbx), %rax
	cmpq	176(%rsp), %rax
	ja	.LBB0_89
# BB#65:
	movq	%r10, %rdx
	leal	107(%rdx), %eax
	cmpl	$214, %eax
	ja	.LBB0_67
# BB#66:
	addl	$139, %edx
	movl	$1, %ebx
	movl	%edx, %esi
	movq	%rcx, %rax
	movq	16(%rsp), %r10          # 8-byte Reload
	jmp	.LBB0_73
.LBB0_67:
	leal	-108(%rdx), %esi
	cmpl	$1023, %esi             # imm = 0x3FF
	movq	16(%rsp), %r10          # 8-byte Reload
	ja	.LBB0_70
# BB#68:
	movl	%esi, %edx
	shrl	$8, %edx
	addl	$247, %edx
	jmp	.LBB0_69
.LBB0_70:
	leal	1131(%rdx), %eax
	cmpl	$1023, %eax             # imm = 0x3FF
	ja	.LBB0_72
# BB#71:
	movl	$-108, %esi
	subl	%edx, %esi
	movl	%esi, %edx
	shrl	$8, %edx
	addl	$251, %edx
.LBB0_69:
	leaq	1(%rcx), %rax
	movb	%dl, (%rcx)
	movl	$2, %ebx
	jmp	.LBB0_73
.LBB0_72:
	movb	$-1, (%rcx)
	movslq	%edx, %rax
	movq	%rax, %rsi
	shrq	$24, %rsi
	movb	%sil, 1(%rcx)
	shrq	$16, %rax
	movb	%al, 2(%rcx)
	leaq	4(%rcx), %rax
	movb	%dh, 3(%rcx)  # NOREX
	movl	$5, %ebx
	movl	%edx, %esi
.LBB0_73:
	leaq	(%rcx,%rbx), %rdx
	movb	%sil, (%rax)
	movq	%rdx, 192(%rsp)
	leaq	2(%rcx,%rbx), %rax
	cmpq	176(%rsp), %rax
	movq	%r9, %r11
	ja	.LBB0_89
# BB#74:
	movq	%r10, %r14
	leaq	1(%rdx), %rax
	movq	%rax, 192(%rsp)
	movb	$12, (%rdx)
	movq	192(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 192(%rsp)
	movb	$7, (%rax)
.LBB0_86:
	shll	$2, %r12d
	movl	%r12d, 200(%rsp)
	cvttsd2si	24(%rsp), %eax  # 8-byte Folded Reload
	movl	%eax, 204(%rsp)
	addl	184(%rsp), %r8d
	movl	%r8d, 208(%rsp)
	addl	188(%rsp), %r11d
	movl	%r11d, 212(%rsp)
	leaq	80(%rsp), %rcx
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r13d, %edx
	movq	%rdi, %rbx
	callq	trace_cells
	movl	%eax, %ebp
	movl	$1, %edx
	movl	$.L.str, %ecx
	movq	%rbx, %rdi
	movl	36(%rsp), %esi          # 4-byte Reload
	callq	gs_free
	testl	%ebp, %ebp
	js	.LBB0_89
# BB#87:
	movq	192(%rsp), %rax
	movl	$-13, %ebp
	cmpq	176(%rsp), %rax
	jae	.LBB0_89
# BB#88:
	leaq	1(%rax), %rcx
	movq	%rcx, 192(%rsp)
	movb	$14, (%rax)
	movl	192(%rsp), %ebp
	subl	%r14d, %ebp
.LBB0_89:
	movl	%ebp, %eax
	addq	$328, %rsp              # imm = 0x148
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	gs_type1imagepath, .Lfunc_end0-gs_type1imagepath
	.cfi_endproc

	.globl	fill_cells
	.p2align	4, 0x90
	.type	fill_cells,@function
fill_cells:                             # @fill_cells
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -48
.Lcfi19:
	.cfi_offset %r12, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %r14d
	movq	%rsi, %rbx
	movq	%rdi, %r12
	leal	2(%r14), %ebp
	leal	2(%r15), %eax
	imull	%ebp, %eax
	movslq	%eax, %rdx
	xorl	%esi, %esi
	callq	memset
	testl	%r15d, %r15d
	jle	.LBB1_20
# BB#1:                                 # %.preheader.lr.ph
	testl	%r14d, %r14d
	jle	.LBB1_20
# BB#2:                                 # %.preheader.us.preheader
	leal	2(%r14,%r14), %eax
	movslq	%eax, %r8
	negq	%r8
	imull	%r15d, %ebp
	movslq	%ebp, %rax
	leaq	1(%r12,%rax), %rdx
	decq	%rbx
	leal	-1(%r14), %r10d
	leaq	1(%r10), %r9
	movl	%r14d, %r11d
	andl	$1, %r11d
	xorl	%eax, %eax
                                        # implicit-def: %ESI
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_10 Depth 2
	testl	%r11d, %r11d
	jne	.LBB1_5
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=1
	xorl	%r12d, %r12d
	xorl	%edi, %edi
	movq	%rdx, %rcx
	testl	%r10d, %r10d
	jne	.LBB1_9
	jmp	.LBB1_19
	.p2align	4, 0x90
.LBB1_5:                                #   in Loop: Header=BB1_3 Depth=1
	movzbl	1(%rbx), %esi
	testb	$-128, %sil
	je	.LBB1_7
# BB#6:                                 #   in Loop: Header=BB1_3 Depth=1
	movb	$1, (%rdx)
.LBB1_7:                                #   in Loop: Header=BB1_3 Depth=1
	incq	%rbx
	movl	$64, %edi
	leaq	1(%rdx), %rcx
	movl	$1, %r12d
	testl	%r10d, %r10d
	je	.LBB1_19
.LBB1_9:                                # %.preheader.us.new
                                        #   in Loop: Header=BB1_3 Depth=1
	movl	%r14d, %ebp
	subl	%r12d, %ebp
	incq	%rcx
	.p2align	4, 0x90
.LBB1_10:                               #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%edi, %edi
	jne	.LBB1_12
# BB#11:                                #   in Loop: Header=BB1_10 Depth=2
	movzbl	1(%rbx), %esi
	incq	%rbx
	movl	$128, %edi
.LBB1_12:                               #   in Loop: Header=BB1_10 Depth=2
	testl	%edi, %esi
	je	.LBB1_14
# BB#13:                                #   in Loop: Header=BB1_10 Depth=2
	movb	$1, -1(%rcx)
.LBB1_14:                               #   in Loop: Header=BB1_10 Depth=2
	sarl	%edi
	jne	.LBB1_16
# BB#15:                                #   in Loop: Header=BB1_10 Depth=2
	movzbl	1(%rbx), %esi
	incq	%rbx
	movl	$128, %edi
.LBB1_16:                               #   in Loop: Header=BB1_10 Depth=2
	testl	%edi, %esi
	je	.LBB1_18
# BB#17:                                #   in Loop: Header=BB1_10 Depth=2
	movb	$1, (%rcx)
.LBB1_18:                               #   in Loop: Header=BB1_10 Depth=2
	sarl	%edi
	addq	$2, %rcx
	addl	$-2, %ebp
	jne	.LBB1_10
.LBB1_19:                               # %._crit_edge.us
                                        #   in Loop: Header=BB1_3 Depth=1
	addq	%r9, %rdx
	addq	%r8, %rdx
	incl	%eax
	cmpl	%r15d, %eax
	jne	.LBB1_3
.LBB1_20:                               # %._crit_edge52
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	fill_cells, .Lfunc_end1-fill_cells
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	round_coord
	.p2align	4, 0x90
	.type	round_coord,@function
round_coord:                            # @round_coord
	.cfi_startproc
# BB#0:
	addsd	.LCPI2_0(%rip), %xmm0
	cvttsd2si	%xmm0, %rax
	cmpq	$-32767, %rax           # imm = 0x8001
	movl	$-32767, %ecx           # imm = 0x8001
	cmovgl	%eax, %ecx
	cmpq	$32767, %rax            # imm = 0x7FFF
	movl	$32767, %eax            # imm = 0x7FFF
	cmovlel	%ecx, %eax
	retq
.Lfunc_end2:
	.size	round_coord, .Lfunc_end2-round_coord
	.cfi_endproc

	.globl	put_int
	.p2align	4, 0x90
	.type	put_int,@function
put_int:                                # @put_int
	.cfi_startproc
# BB#0:
	movl	%esi, %edx
	movq	112(%rdi), %rcx
	leaq	5(%rcx), %rsi
	movl	$-13, %eax
	cmpq	96(%rdi), %rsi
	ja	.LBB3_10
# BB#1:
	leal	107(%rdx), %eax
	cmpl	$214, %eax
	ja	.LBB3_3
# BB#2:
	addl	$139, %edx
	movl	$1, %esi
	movl	%edx, %eax
	movq	%rcx, %r8
	jmp	.LBB3_9
.LBB3_3:
	leal	-108(%rdx), %eax
	cmpl	$1023, %eax             # imm = 0x3FF
	ja	.LBB3_6
# BB#4:
	movl	%eax, %edx
	shrl	$8, %edx
	addl	$247, %edx
	jmp	.LBB3_5
.LBB3_6:
	leal	1131(%rdx), %eax
	cmpl	$1023, %eax             # imm = 0x3FF
	ja	.LBB3_8
# BB#7:
	movl	$-108, %eax
	subl	%edx, %eax
	movl	%eax, %edx
	shrl	$8, %edx
	addl	$251, %edx
.LBB3_5:
	leaq	1(%rcx), %r8
	movb	%dl, (%rcx)
	movl	$2, %esi
	jmp	.LBB3_9
.LBB3_8:
	movb	$-1, (%rcx)
	movslq	%edx, %rax
	movq	%rax, %rsi
	shrq	$24, %rsi
	movb	%sil, 1(%rcx)
	shrq	$16, %rax
	movb	%al, 2(%rcx)
	leaq	4(%rcx), %r8
	movb	%dh, 3(%rcx)  # NOREX
	movl	$5, %esi
	movl	%edx, %eax
.LBB3_9:
	addq	%rsi, %rcx
	movb	%al, (%r8)
	movq	%rcx, 112(%rdi)
	xorl	%eax, %eax
.LBB3_10:
	retq
.Lfunc_end3:
	.size	put_int, .Lfunc_end3-put_int
	.cfi_endproc

	.globl	trace_cells
	.p2align	4, 0x90
	.type	trace_cells,@function
trace_cells:                            # @trace_cells
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 64
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movl	%esi, %r15d
	movq	%rdi, %r12
	leal	2(%r15), %esi
	incl	%edx
	imull	%esi, %edx
	movslq	%edx, %rcx
	leaq	-2(%r12,%rcx), %rbp
	xorl	%eax, %eax
	cmpq	%r12, %rbp
	jb	.LBB4_11
# BB#1:                                 # %.lr.ph
	movl	$-2, %ecx
	subl	%r15d, %ecx
	movslq	%ecx, %r13
	movslq	%esi, %r14
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	cmpb	$1, (%rbp)
	jne	.LBB4_9
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	cmpb	$0, (%r13,%rbp)
	jne	.LBB4_9
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	%rbp, %rax
	subq	%r12, %rax
	cqto
	idivq	%r14
	leal	-3(,%rdx,4), %esi
	subl	120(%rbx), %esi
	leal	-4(,%rax,4), %edx
	subl	124(%rbx), %edx
	movl	$1, %ecx
	movq	%rbx, %rdi
	callq	put_dxdy
	testl	%eax, %eax
	js	.LBB4_11
# BB#5:                                 #   in Loop: Header=BB4_2 Depth=1
	movl	$0, 144(%rbx)
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movl	%r15d, %edx
	callq	trace_from
	testl	%eax, %eax
	js	.LBB4_11
# BB#6:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	112(%rbx), %rax
	cmpq	96(%rbx), %rax
	jae	.LBB4_7
# BB#8:                                 #   in Loop: Header=BB4_2 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, 112(%rbx)
	movb	$9, (%rax)
	.p2align	4, 0x90
.LBB4_9:                                #   in Loop: Header=BB4_2 Depth=1
	decq	%rbp
	cmpq	%r12, %rbp
	jae	.LBB4_2
# BB#10:
	xorl	%eax, %eax
.LBB4_11:                               # %.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_7:
	movl	$-13, %eax
	jmp	.LBB4_11
.Lfunc_end4:
	.size	trace_cells, .Lfunc_end4-trace_cells
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	put_dxdy
	.p2align	4, 0x90
	.type	put_dxdy,@function
put_dxdy:                               # @put_dxdy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi41:
	.cfi_def_cfa_offset 64
.Lcfi42:
	.cfi_offset %rbx, -48
.Lcfi43:
	.cfi_offset %r12, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %r14d
	movl	%esi, %r12d
	movq	%rdi, %rbx
	addl	120(%rbx), %r12d
	addl	124(%rbx), %r14d
	cvtsi2sdl	%r12d, %xmm0
	cvtsi2sdl	%r14d, %xmm1
	leaq	8(%rsp), %rsi
	callq	gs_distance_transform
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movsd	.LCPI5_0(%rip), %xmm2   # xmm2 = mem[0],zero
	addsd	%xmm2, %xmm0
	cvttsd2si	%xmm0, %rax
	cmpq	$-32767, %rax           # imm = 0x8001
	movl	$-32767, %ecx           # imm = 0x8001
	movl	$-32767, %esi           # imm = 0x8001
	cmovgl	%eax, %esi
	cmpq	$32767, %rax            # imm = 0x7FFF
	movl	$32767, %eax            # imm = 0x7FFF
	cmovgl	%eax, %esi
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	cvttsd2si	%xmm0, %rdi
	cmpq	$-32767, %rdi           # imm = 0x8001
	movl	%edi, %edx
	cmovlel	%ecx, %edx
	cmpq	$32767, %rdi            # imm = 0x7FFF
	cmovgl	%eax, %edx
	movslq	128(%rbx), %rax
	movslq	%esi, %rdi
	subq	%rax, %rdi
	movslq	132(%rbx), %rax
	movslq	%edx, %rcx
	subq	%rax, %rcx
	movq	%rdi, %rax
	testl	%eax, %eax
	jne	.LBB5_2
# BB#1:
	testl	%ecx, %ecx
	jne	.LBB5_11
.LBB5_2:
	movq	%rax, %r9
	movq	112(%rbx), %rdi
	leaq	5(%rdi), %rbp
	movl	$-13, %eax
	cmpq	96(%rbx), %rbp
	ja	.LBB5_35
# BB#3:
	movq	%r9, %rbp
	leal	107(%rbp), %eax
	cmpl	$214, %eax
	ja	.LBB5_5
# BB#4:
	leal	139(%rbp), %r10d
	movl	$1, %r9d
	movq	%rdi, %r8
	movq	%rbp, %rax
	jmp	.LBB5_10
.LBB5_5:
	leal	-108(%rbp), %r10d
	cmpl	$1023, %r10d            # imm = 0x3FF
	ja	.LBB5_7
# BB#6:
	movl	%r10d, %eax
	shrl	$8, %eax
	addl	$247, %eax
	leaq	1(%rdi), %r8
	movb	%al, (%rdi)
	movq	%r9, %rax
	movl	$2, %r9d
	jmp	.LBB5_10
.LBB5_7:
	movq	%rbp, %r8
	leal	1131(%rbp), %eax
	cmpl	$1023, %eax             # imm = 0x3FF
	ja	.LBB5_9
# BB#8:
	movl	$-108, %r10d
	movq	%r8, %rbp
	subl	%ebp, %r10d
	movl	%r10d, %eax
	shrl	$8, %eax
	addl	$251, %eax
	leaq	1(%rdi), %r8
	movb	%al, (%rdi)
	movl	$2, %r9d
	movq	%rbp, %rax
	jmp	.LBB5_10
.LBB5_9:
	movb	$-1, (%rdi)
	movq	%r8, %rbp
	movq	%rbp, %rax
	shrq	$24, %rax
	movb	%al, 1(%rdi)
	movq	%rbp, %rax
	shrq	$16, %rax
	movb	%al, 2(%rdi)
	leaq	4(%rdi), %r8
	movq	%rbp, %rax
	movb	%ah, 3(%rdi)  # NOREX
	movl	$5, %r9d
	movl	%eax, %r10d
.LBB5_10:                               # %put_int.exit56
	addq	%r9, %rdi
	movb	%r10b, (%r8)
	movq	%rdi, 112(%rbx)
.LBB5_11:
	movq	%rax, %r11
	movq	112(%rbx), %rdi
	testl	%ecx, %ecx
	je	.LBB5_12
# BB#13:
	leaq	5(%rdi), %rbp
	movl	$-13, %eax
	cmpq	96(%rbx), %rbp
	ja	.LBB5_35
# BB#14:
	leal	107(%rcx), %eax
	cmpl	$214, %eax
	ja	.LBB5_16
# BB#15:
	leal	139(%rcx), %r10d
	movl	$1, %r9d
	movq	%rdi, %r8
	jmp	.LBB5_22
.LBB5_12:                               # %._crit_edge
	leaq	96(%rbx), %rax
	cmpq	(%rax), %rdi
	jne	.LBB5_25
	jmp	.LBB5_24
.LBB5_16:
	leal	-108(%rcx), %r10d
	cmpl	$1023, %r10d            # imm = 0x3FF
	ja	.LBB5_19
# BB#17:
	movl	%r10d, %eax
	shrl	$8, %eax
	addl	$247, %eax
	jmp	.LBB5_18
.LBB5_19:
	leal	1131(%rcx), %eax
	cmpl	$1023, %eax             # imm = 0x3FF
	ja	.LBB5_21
# BB#20:
	movl	$-108, %r10d
	subl	%ecx, %r10d
	movl	%r10d, %eax
	shrl	$8, %eax
	addl	$251, %eax
.LBB5_18:                               # %put_int.exit
	leaq	1(%rdi), %r8
	movb	%al, (%rdi)
	movl	$2, %r9d
	jmp	.LBB5_22
.LBB5_21:
	movb	$-1, (%rdi)
	movq	%rcx, %rax
	shrq	$24, %rax
	movb	%al, 1(%rdi)
	movq	%rcx, %rax
	shrq	$16, %rax
	movb	%al, 2(%rdi)
	leaq	4(%rdi), %r8
	movb	%ch, 3(%rdi)  # NOREX
	movl	$5, %r9d
	movl	%ecx, %r10d
.LBB5_22:                               # %put_int.exit
	leaq	96(%rbx), %rax
	addq	%r9, %rdi
	movb	%r10b, (%r8)
	movq	%rdi, 112(%rbx)
	cmpq	(%rax), %rdi
	je	.LBB5_24
.LBB5_25:
	testl	%ecx, %ecx
	je	.LBB5_26
# BB#28:
	testl	%r15d, %r15d
	movb	$4, %al
	jne	.LBB5_30
# BB#29:
	movb	$7, %al
.LBB5_30:
	movb	$21, %cl
	jne	.LBB5_32
# BB#31:
	movb	$5, %cl
.LBB5_32:
	testl	%r11d, %r11d
	je	.LBB5_34
# BB#33:
	movl	%ecx, %eax
	jmp	.LBB5_34
.LBB5_24:
	movl	$-13, %eax
	jmp	.LBB5_35
.LBB5_26:
	testl	%r15d, %r15d
	movb	$22, %al
	jne	.LBB5_34
# BB#27:
	movb	$6, %al
.LBB5_34:
	leaq	1(%rdi), %rcx
	movq	%rcx, 112(%rbx)
	movb	%al, (%rdi)
	movl	%r12d, 120(%rbx)
	movl	%r14d, 124(%rbx)
	movl	%esi, 128(%rbx)
	movl	%edx, 132(%rbx)
	xorl	%eax, %eax
.LBB5_35:                               # %put_int.exit56.thread
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	put_dxdy, .Lfunc_end5-put_dxdy
	.cfi_endproc

	.globl	trace_from
	.p2align	4, 0x90
	.type	trace_from,@function
trace_from:                             # @trace_from
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi53:
	.cfi_def_cfa_offset 96
.Lcfi54:
	.cfi_offset %rbx, -56
.Lcfi55:
	.cfi_offset %r12, -48
.Lcfi56:
	.cfi_offset %r13, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	addl	$2, %edx
	movl	$3, %ecx
	movl	$1, %r8d
	movl	%edx, 16(%rsp)          # 4-byte Spill
	jmp	.LBB6_1
	.p2align	4, 0x90
.LBB6_6:                                #   in Loop: Header=BB6_4 Depth=2
	movl	136(%rbp), %esi
	cmpl	%r15d, %esi
	jne	.LBB6_8
# BB#7:                                 #   in Loop: Header=BB6_4 Depth=2
	cmpl	%edi, 140(%rbp)
	jne	.LBB6_8
# BB#17:                                #   in Loop: Header=BB6_4 Depth=2
	movl	144(%rbp), %eax
	addl	$4, %eax
	jmp	.LBB6_18
	.p2align	4, 0x90
.LBB6_8:                                #   in Loop: Header=BB6_4 Depth=2
	movl	144(%rbp), %edx
	testl	%edx, %edx
	je	.LBB6_16
# BB#9:                                 #   in Loop: Header=BB6_4 Depth=2
	imull	%edx, %esi
	imull	140(%rbp), %edx
	xorl	%ecx, %ecx
	movq	%rbp, %rdi
	callq	put_dxdy
	movl	28(%rsp), %edi          # 4-byte Reload
	testl	%eax, %eax
	js	.LBB6_10
.LBB6_16:                               # %._crit_edge.i86.us
                                        #   in Loop: Header=BB6_4 Depth=2
	movl	%r15d, 136(%rbp)
	movl	%edi, 140(%rbp)
	movl	$4, %eax
.LBB6_18:                               # %add_dxdy.exit.thread93.us.backedge
                                        #   in Loop: Header=BB6_4 Depth=2
	movl	%eax, 144(%rbp)
	cmpb	$2, (%r13,%rbx)
	leaq	(%rbx,%r13), %rbx
	jne	.LBB6_4
	jmp	.LBB6_11
	.p2align	4, 0x90
.LBB6_12:                               # %add_dxdy.exit.thread93.preheader
                                        #   in Loop: Header=BB6_1 Depth=1
	movswl	trace_from.nesw+2(,%rsi,8), %eax
	movswl	trace_from.nesw(,%rsi,8), %ecx
	movl	%eax, 24(%rsp)          # 4-byte Spill
	imull	%edx, %eax
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	addl	%ecx, %eax
	movslq	%eax, %r14
	cmpb	$0, (%rbx,%r14)
	je	.LBB6_33
	jmp	.LBB6_14
	.p2align	4, 0x90
.LBB6_53:                               # %add_dxdy.exit.thread93.backedge
                                        #   in Loop: Header=BB6_33 Depth=2
	addq	%r13, %rbx
	movl	%eax, 144(%rbp)
	cmpb	$0, (%rbx,%r14)
	jne	.LBB6_14
.LBB6_33:                               # %.lr.ph
                                        #   Parent Loop BB6_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, (%rbx,%r13)
	je	.LBB6_34
# BB#50:                                #   in Loop: Header=BB6_33 Depth=2
	movl	136(%rbp), %esi
	cmpl	%r15d, %esi
	jne	.LBB6_54
# BB#51:                                #   in Loop: Header=BB6_33 Depth=2
	cmpl	%edi, 140(%rbp)
	jne	.LBB6_54
# BB#52:                                #   in Loop: Header=BB6_33 Depth=2
	movl	144(%rbp), %eax
	addl	$4, %eax
	jmp	.LBB6_53
	.p2align	4, 0x90
.LBB6_54:                               #   in Loop: Header=BB6_33 Depth=2
	movl	144(%rbp), %edx
	testl	%edx, %edx
	je	.LBB6_56
# BB#55:                                #   in Loop: Header=BB6_33 Depth=2
	imull	%edx, %esi
	imull	140(%rbp), %edx
	xorl	%ecx, %ecx
	movq	%rbp, %rdi
	callq	put_dxdy
	movl	28(%rsp), %edi          # 4-byte Reload
	movl	%eax, %r12d
	testl	%r12d, %r12d
	js	.LBB6_11
.LBB6_56:                               # %._crit_edge.i86
                                        #   in Loop: Header=BB6_33 Depth=2
	movl	%r15d, 136(%rbp)
	movl	%edi, 140(%rbp)
	movl	$4, %eax
	jmp	.LBB6_53
	.p2align	4, 0x90
.LBB6_14:                               # %.us-lcssa.us.loopexit
                                        #   in Loop: Header=BB6_1 Depth=1
	addq	%r14, %rbx
	movl	$1, %r14d
	subl	12(%rsp), %r14d         # 4-byte Folded Reload
	movl	136(%rbp), %esi
	je	.LBB6_15
# BB#19:                                #   in Loop: Header=BB6_1 Depth=1
	cmpl	%r15d, %esi
	jne	.LBB6_22
# BB#20:                                #   in Loop: Header=BB6_1 Depth=1
	cmpl	%edi, 140(%rbp)
	jne	.LBB6_22
# BB#21:                                #   in Loop: Header=BB6_1 Depth=1
	addl	144(%rbp), %r14d
	jmp	.LBB6_25
	.p2align	4, 0x90
.LBB6_22:                               #   in Loop: Header=BB6_1 Depth=1
	movl	144(%rbp), %edx
	testl	%edx, %edx
	je	.LBB6_24
# BB#23:                                #   in Loop: Header=BB6_1 Depth=1
	imull	%edx, %esi
	imull	140(%rbp), %edx
	xorl	%ecx, %ecx
	movq	%rbp, %rdi
	callq	put_dxdy
	movl	28(%rsp), %edi          # 4-byte Reload
	movl	%eax, %r12d
	testl	%r12d, %r12d
	js	.LBB6_11
.LBB6_24:                               # %._crit_edge.i
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	%r15d, 136(%rbp)
	movl	%edi, 140(%rbp)
.LBB6_25:                               # %.us-lcssa.us._crit_edge.sink.split
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	24(%rsp), %eax          # 4-byte Reload
	movl	20(%rsp), %ecx          # 4-byte Reload
	movl	%r14d, 144(%rbp)
	jmp	.LBB6_26
	.p2align	4, 0x90
.LBB6_34:                               # %.us-lcssa119.us
                                        #   in Loop: Header=BB6_1 Depth=1
	xorl	$3, 12(%rsp)            # 4-byte Folded Spill
	movl	136(%rbp), %esi
	movq	32(%rsp), %r14          # 8-byte Reload
	je	.LBB6_42
# BB#35:                                #   in Loop: Header=BB6_1 Depth=1
	cmpl	%r15d, %esi
	jne	.LBB6_38
# BB#36:                                #   in Loop: Header=BB6_1 Depth=1
	cmpl	%edi, 140(%rbp)
	jne	.LBB6_38
# BB#37:                                #   in Loop: Header=BB6_1 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
	addl	144(%rbp), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jmp	.LBB6_41
	.p2align	4, 0x90
.LBB6_15:                               #   in Loop: Header=BB6_1 Depth=1
	movl	%esi, %r15d
	movl	24(%rsp), %eax          # 4-byte Reload
	movl	20(%rsp), %ecx          # 4-byte Reload
.LBB6_26:                               # %.us-lcssa.us._crit_edge
                                        #   in Loop: Header=BB6_1 Depth=1
	movswl	%cx, %r14d
	movswl	%ax, %r13d
	cmpl	%r14d, %r15d
	jne	.LBB6_30
# BB#27:                                #   in Loop: Header=BB6_1 Depth=1
	cmpl	%r13d, 140(%rbp)
	jne	.LBB6_30
# BB#28:                                #   in Loop: Header=BB6_1 Depth=1
	movl	144(%rbp), %eax
	addl	$3, %eax
	jmp	.LBB6_29
	.p2align	4, 0x90
.LBB6_30:                               #   in Loop: Header=BB6_1 Depth=1
	movl	144(%rbp), %edx
	testl	%edx, %edx
	je	.LBB6_32
# BB#31:                                #   in Loop: Header=BB6_1 Depth=1
	imull	%edx, %r15d
	imull	140(%rbp), %edx
	xorl	%ecx, %ecx
	movq	%rbp, %rdi
	movl	%r15d, %esi
	callq	put_dxdy
	movl	%eax, %r12d
	testl	%r12d, %r12d
	js	.LBB6_11
.LBB6_32:                               # %._crit_edge.i71
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	%r14d, 136(%rbp)
	movl	%r13d, 140(%rbp)
	movl	$3, %eax
.LBB6_29:                               # %add_dxdy.exit.thread93.outer.backedge
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	$3, %r8d
	movl	16(%rsp), %edx          # 4-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB6_49
.LBB6_38:                               #   in Loop: Header=BB6_1 Depth=1
	movl	144(%rbp), %edx
	testl	%edx, %edx
	je	.LBB6_40
# BB#39:                                #   in Loop: Header=BB6_1 Depth=1
	imull	%edx, %esi
	imull	140(%rbp), %edx
	xorl	%ecx, %ecx
	movq	%rbp, %rdi
	callq	put_dxdy
	movl	28(%rsp), %edi          # 4-byte Reload
	movl	%eax, %r12d
	testl	%r12d, %r12d
	js	.LBB6_11
.LBB6_40:                               # %._crit_edge.i76
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	%r15d, 136(%rbp)
	movl	%edi, 140(%rbp)
.LBB6_41:                               # %.us-lcssa119.us._crit_edge.sink.split
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, 144(%rbp)
	movl	%r15d, %esi
.LBB6_42:                               # %.us-lcssa119.us._crit_edge
                                        #   in Loop: Header=BB6_1 Depth=1
	movq	%r14, %rax
	movswl	trace_from.nesw+12(,%rax,8), %r14d
	addl	%r15d, %r14d
	movq	%rax, %r13
	movswl	trace_from.nesw+14(,%rax,8), %r15d
	addl	%edi, %r15d
	cmpl	%r14d, %esi
	jne	.LBB6_45
# BB#43:                                #   in Loop: Header=BB6_1 Depth=1
	cmpl	%r15d, 140(%rbp)
	jne	.LBB6_45
# BB#44:                                #   in Loop: Header=BB6_1 Depth=1
	movl	144(%rbp), %eax
	incl	%eax
	jmp	.LBB6_48
.LBB6_45:                               #   in Loop: Header=BB6_1 Depth=1
	movl	144(%rbp), %edx
	testl	%edx, %edx
	je	.LBB6_47
# BB#46:                                #   in Loop: Header=BB6_1 Depth=1
	imull	%edx, %esi
	imull	140(%rbp), %edx
	xorl	%ecx, %ecx
	movq	%rbp, %rdi
	callq	put_dxdy
	movl	%eax, %r12d
	testl	%r12d, %r12d
	js	.LBB6_11
.LBB6_47:                               # %._crit_edge.i81
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	%r14d, 136(%rbp)
	movl	%r15d, 140(%rbp)
	movl	$1, %eax
.LBB6_48:                               # %add_dxdy.exit.thread93.outer.backedge
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	$1, %r8d
	movl	16(%rsp), %edx          # 4-byte Reload
	movq	%r13, %rcx
.LBB6_49:                               # %add_dxdy.exit.thread93.outer.backedge
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	%eax, 144(%rbp)
	addl	%r8d, %ecx
	andl	$3, %ecx
.LBB6_1:                                # %add_dxdy.exit.thread93.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_33 Depth 2
                                        #     Child Loop BB6_4 Depth 2
	movl	%ecx, %eax
	movl	%eax, %esi
	movswl	trace_from.nesw+4(,%rsi,8), %r15d
	movswl	trace_from.nesw+6(,%rsi,8), %edi
	movl	%edi, %ecx
	imull	%edx, %ecx
	addl	%r15d, %ecx
	movslq	%ecx, %r13
	cmpl	$3, %eax
	movl	%edi, 28(%rsp)          # 4-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	jne	.LBB6_12
# BB#2:                                 # %add_dxdy.exit.thread93.us.preheader
                                        #   in Loop: Header=BB6_1 Depth=1
	xorl	%r12d, %r12d
	cmpb	$2, (%rbx)
	je	.LBB6_11
# BB#3:                                 # %.lr.ph182.preheader
                                        #   in Loop: Header=BB6_1 Depth=1
	movswl	trace_from.nesw+2(,%rsi,8), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	imull	%edx, %eax
	movswl	trace_from.nesw(,%rsi,8), %ecx
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	addl	%ecx, %eax
	movslq	%eax, %r14
	.p2align	4, 0x90
.LBB6_4:                                # %.lr.ph182
                                        #   Parent Loop BB6_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$2, (%rbx)
	cmpb	$0, (%r14,%rbx)
	jne	.LBB6_14
# BB#5:                                 #   in Loop: Header=BB6_4 Depth=2
	cmpb	$0, (%rbx,%r13)
	jne	.LBB6_6
	jmp	.LBB6_34
.LBB6_10:
	movl	%eax, %r12d
.LBB6_11:                               # %add_dxdy.exit.thread
	movl	%r12d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	trace_from, .Lfunc_end6-trace_from
	.cfi_endproc

	.globl	add_dxdy
	.p2align	4, 0x90
	.type	add_dxdy,@function
add_dxdy:                               # @add_dxdy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 48
.Lcfi65:
	.cfi_offset %rbx, -48
.Lcfi66:
	.cfi_offset %r12, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movl	%edx, %r15d
	movl	%esi, %r12d
	movq	%rdi, %rbx
	xorl	%r14d, %r14d
	testl	%ebp, %ebp
	je	.LBB7_8
# BB#1:
	movl	136(%rbx), %esi
	cmpl	%r12d, %esi
	jne	.LBB7_4
# BB#2:
	cmpl	%r15d, 140(%rbx)
	jne	.LBB7_4
# BB#3:
	addl	%ebp, 144(%rbx)
	jmp	.LBB7_8
.LBB7_4:
	movl	144(%rbx), %edx
	testl	%edx, %edx
	je	.LBB7_7
# BB#5:
	imull	%edx, %esi
	imull	140(%rbx), %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	put_dxdy
	testl	%eax, %eax
	js	.LBB7_6
.LBB7_7:
	movl	%r12d, 136(%rbx)
	movl	%r15d, 140(%rbx)
	movl	%ebp, 144(%rbx)
	jmp	.LBB7_8
.LBB7_6:
	movl	%eax, %r14d
.LBB7_8:
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	add_dxdy, .Lfunc_end7-add_dxdy
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"gsim2out cells"
	.size	.L.str, 15

	.type	trace_from.nesw,@object # @trace_from.nesw
	.section	.rodata,"a",@progbits
	.p2align	4
trace_from.nesw:
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	65535                   # 0xffff
	.short	0                       # 0x0
	.short	65535                   # 0xffff
	.short	65535                   # 0xffff
	.short	65535                   # 0xffff
	.short	65535                   # 0xffff
	.short	0                       # 0x0
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.short	0                       # 0x0
	.short	1                       # 0x1
	.size	trace_from.nesw, 40


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
