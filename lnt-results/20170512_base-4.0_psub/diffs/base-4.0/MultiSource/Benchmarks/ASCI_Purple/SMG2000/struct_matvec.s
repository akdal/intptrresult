	.text
	.file	"struct_matvec.bc"
	.globl	hypre_StructMatvecCreate
	.p2align	4, 0x90
	.type	hypre_StructMatvecCreate,@function
hypre_StructMatvecCreate:               # @hypre_StructMatvecCreate
	.cfi_startproc
# BB#0:
	movl	$1, %edi
	movl	$24, %esi
	jmp	hypre_CAlloc            # TAILCALL
.Lfunc_end0:
	.size	hypre_StructMatvecCreate, .Lfunc_end0-hypre_StructMatvecCreate
	.cfi_endproc

	.globl	hypre_StructMatvecSetup
	.p2align	4, 0x90
	.type	hypre_StructMatvecSetup,@function
hypre_StructMatvecSetup:                # @hypre_StructMatvecSetup
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$72, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 112
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	8(%rbx), %r12
	movq	24(%rbx), %rsi
	leaq	8(%rsp), %rax
	leaq	16(%rsp), %r10
	leaq	48(%rsp), %rdx
	leaq	40(%rsp), %rcx
	leaq	32(%rsp), %r8
	leaq	24(%rsp), %r9
	movq	%r12, %rdi
	pushq	%rax
.Lcfi9:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi10:
	.cfi_adjust_cfa_offset 8
	callq	hypre_CreateComputeInfo
	addq	$16, %rsp
.Lcfi11:
	.cfi_adjust_cfa_offset -16
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 60(%rsp)
	movl	$1, 68(%rsp)
	movq	48(%rsp), %rdi
	movq	40(%rsp), %rsi
	movq	32(%rsp), %r8
	movq	24(%rsp), %r9
	subq	$8, %rsp
.Lcfi12:
	.cfi_adjust_cfa_offset 8
	leaq	8(%rsp), %rax
	leaq	68(%rsp), %rdx
	movq	%rdx, %rcx
	pushq	%rax
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	16(%r14)
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	%rdx
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	pushq	72(%rsp)
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	callq	hypre_ComputePkgCreate
	addq	$64, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset -64
	movq	%rbx, %rdi
	callq	hypre_StructMatrixRef
	movq	%rax, (%r15)
	movq	%r14, %rdi
	callq	hypre_StructVectorRef
	movq	%rax, 8(%r15)
	movq	(%rsp), %rax
	movq	%rax, 16(%r15)
	xorl	%eax, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	hypre_StructMatvecSetup, .Lfunc_end1-hypre_StructMatvecSetup
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	hypre_StructMatvecCompute
	.p2align	4, 0x90
	.type	hypre_StructMatvecCompute,@function
hypre_StructMatvecCompute:              # @hypre_StructMatvecCompute
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	subq	$792, %rsp              # imm = 0x318
.Lcfi27:
	.cfi_def_cfa_offset 848
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	%rcx, 584(%rsp)         # 8-byte Spill
	movq	%rdx, %r15
	movq	%rsi, %rbp
	movq	16(%rdi), %rax
	movq	%rax, 384(%rsp)         # 8-byte Spill
	xorpd	%xmm2, %xmm2
	ucomisd	%xmm2, %xmm0
	jne	.LBB2_29
	jp	.LBB2_29
# BB#1:
	movq	8(%rbp), %rax
	movq	8(%rax), %rcx
	cmpl	$0, 8(%rcx)
	jle	.LBB2_247
# BB#2:                                 # %.lr.ph3759
	movaps	%xmm1, %xmm3
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	xorl	%edx, %edx
	movaps	%xmm1, 688(%rsp)        # 16-byte Spill
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movaps	%xmm3, 352(%rsp)        # 16-byte Spill
	.p2align	4, 0x90
.LBB2_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_8 Depth 2
                                        #       Child Loop BB2_14 Depth 3
                                        #         Child Loop BB2_11 Depth 4
                                        #         Child Loop BB2_22 Depth 4
                                        #         Child Loop BB2_25 Depth 4
	movq	(%rcx), %r15
	leaq	(,%rdx,8), %rax
	leaq	(%rax,%rax,2), %rbx
	leaq	(%r15,%rbx), %r12
	movq	584(%rsp), %rcx         # 8-byte Reload
	movq	16(%rcx), %rax
	movq	24(%rcx), %rsi
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movq	(%rax), %r13
	movq	40(%rcx), %rax
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movslq	(%rax,%rdx,4), %r14
	movq	%r12, %rdi
	leaq	436(%rsp), %rsi
	callq	hypre_BoxGetSize
	movapd	352(%rsp), %xmm3        # 16-byte Reload
	movapd	688(%rsp), %xmm1        # 16-byte Reload
	movl	(%r13,%rbx), %edx
	movl	4(%r13,%rbx), %r8d
	movl	12(%r13,%rbx), %eax
	movl	%eax, %ecx
	subl	%edx, %ecx
	incl	%ecx
	cmpl	%edx, %eax
	movl	16(%r13,%rbx), %eax
	movl	$0, %esi
	cmovsl	%esi, %ecx
	movl	%eax, %ebp
	subl	%r8d, %ebp
	incl	%ebp
	cmpl	%r8d, %eax
	cmovsl	%esi, %ebp
	movl	436(%rsp), %eax
	movl	440(%rsp), %esi
	movl	444(%rsp), %edi
	cmpl	%eax, %esi
	movq	%rax, 8(%rsp)           # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%esi, 32(%rsp)          # 4-byte Spill
	cmovgel	%esi, %eax
	cmpl	%eax, %edi
	movl	%edi, 536(%rsp)         # 4-byte Spill
	cmovgel	%edi, %eax
	testl	%eax, %eax
	jle	.LBB2_28
# BB#4:                                 # %.lr.ph3721
                                        #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 536(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_28
# BB#5:                                 # %.lr.ph3721.split.us.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_28
# BB#6:                                 # %.lr.ph3721.split.us.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB2_28
# BB#7:                                 # %.preheader3665.us.us.us.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	384(%rsp), %rax         # 8-byte Reload
	movslq	24(%rax), %rsi
	movl	32(%rax), %edi
	movl	28(%rax), %eax
	imull	%ecx, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%ecx, %eax
	imull	%ebp, %eax
	imull	%edi, %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movl	(%r12), %r9d
	subl	%edx, %r9d
	movl	4(%r15,%rbx), %eax
	movl	8(%r15,%rbx), %edx
	subl	%r8d, %eax
	subl	8(%r13,%rbx), %edx
	imull	%ebp, %edx
	addl	%eax, %edx
	imull	%ecx, %edx
	addl	%r9d, %edx
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	-1(%rax), %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	incq	%rax
	imull	%ecx, %edi
	imull	%ebp, %edi
	movl	%edi, 376(%rsp)         # 4-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, %rcx
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	andq	%rax, %rcx
	leaq	-4(%rcx), %rax
	shrq	$2, %rax
	movq	%rsi, %rdi
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	imulq	%rcx, %rdi
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	movq	%rax, 64(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	%rsi, %r9
	shlq	$6, %r9
	movq	%rsi, %r8
	shlq	$5, %r8
	leaq	(%rsi,%rsi,2), %rax
	shlq	$3, %rax
	leaq	(%rax,%r14,8), %r10
	movq	%rsi, %rax
	shlq	$4, %rax
	leaq	(%rax,%r14,8), %r11
	movq	72(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r14,8), %r12
	leaq	(,%r14,8), %r13
	leaq	(%r13,%rsi,8), %r14
	xorl	%eax, %eax
	movl	%edx, 96(%rsp)          # 4-byte Spill
	movl	%edx, %ecx
	.p2align	4, 0x90
.LBB2_8:                                # %.preheader3665.us.us.us
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_14 Depth 3
                                        #         Child Loop BB2_11 Depth 4
                                        #         Child Loop BB2_22 Depth 4
                                        #         Child Loop BB2_25 Depth 4
	movl	376(%rsp), %edx         # 4-byte Reload
	movl	%eax, 344(%rsp)         # 4-byte Spill
	imull	%eax, %edx
	addl	96(%rsp), %edx          # 4-byte Folded Reload
	movl	%edx, 136(%rsp)         # 4-byte Spill
	movl	%ecx, 160(%rsp)         # 4-byte Spill
	movl	%ecx, %eax
	xorl	%ebx, %ebx
	jmp	.LBB2_14
.LBB2_9:                                #   in Loop: Header=BB2_14 Depth=3
	xorl	%eax, %eax
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	je	.LBB2_12
.LBB2_10:                               # %vector.ph5127.new
                                        #   in Loop: Header=BB2_14 Depth=3
	movq	48(%rsp), %rbp          # 8-byte Reload
	subq	%rax, %rbp
	movq	%rsi, %rdx
	imulq	%rax, %rdx
	addq	%r15, %rdx
	leaq	16(%r12,%rdx,8), %rdi
	addq	$4, %rax
	imulq	%rsi, %rax
	addq	%r15, %rax
	leaq	16(%r12,%rax,8), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_11:                               # %vector.body5116
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_8 Depth=2
                                        #       Parent Loop BB2_14 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movupd	-16(%rdi,%rdx), %xmm0
	movupd	(%rdi,%rdx), %xmm2
	mulpd	%xmm3, %xmm0
	mulpd	%xmm3, %xmm2
	movupd	%xmm0, -16(%rdi,%rdx)
	movupd	%xmm2, (%rdi,%rdx)
	movupd	-16(%rax,%rdx), %xmm0
	movupd	(%rax,%rdx), %xmm2
	mulpd	%xmm3, %xmm0
	mulpd	%xmm3, %xmm2
	movupd	%xmm0, -16(%rax,%rdx)
	movupd	%xmm2, (%rax,%rdx)
	addq	%r9, %rdx
	addq	$-8, %rbp
	jne	.LBB2_11
.LBB2_12:                               # %middle.block5117
                                        #   in Loop: Header=BB2_14 Depth=3
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, 16(%rsp)          # 8-byte Folded Reload
	je	.LBB2_26
# BB#13:                                #   in Loop: Header=BB2_14 Depth=3
	addq	128(%rsp), %rcx         # 8-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ebp
	jmp	.LBB2_20
	.p2align	4, 0x90
.LBB2_14:                               # %.preheader.us.us.us.us
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_8 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_11 Depth 4
                                        #         Child Loop BB2_22 Depth 4
                                        #         Child Loop BB2_25 Depth 4
	movslq	%eax, %r15
	xorl	%ebp, %ebp
	cmpq	$3, 16(%rsp)            # 8-byte Folded Reload
	jbe	.LBB2_19
# BB#15:                                # %min.iters.checked5120
                                        #   in Loop: Header=BB2_14 Depth=3
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB2_19
# BB#16:                                # %vector.scevcheck5126
                                        #   in Loop: Header=BB2_14 Depth=3
	cmpl	$1, %esi
	jne	.LBB2_19
# BB#17:                                # %vector.ph5127
                                        #   in Loop: Header=BB2_14 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%ebx, %eax
	addl	136(%rsp), %eax         # 4-byte Folded Reload
	movslq	%eax, %rcx
	cmpq	$0, 168(%rsp)           # 8-byte Folded Reload
	jne	.LBB2_9
# BB#18:                                # %vector.body5116.prol
                                        #   in Loop: Header=BB2_14 Depth=3
	movupd	(%r12,%rcx,8), %xmm0
	movupd	16(%r12,%rcx,8), %xmm2
	mulpd	%xmm3, %xmm0
	mulpd	%xmm3, %xmm2
	movupd	%xmm0, (%r12,%rcx,8)
	movupd	%xmm2, 16(%r12,%rcx,8)
	movl	$4, %eax
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	jne	.LBB2_10
	jmp	.LBB2_12
	.p2align	4, 0x90
.LBB2_19:                               #   in Loop: Header=BB2_14 Depth=3
	movq	%r15, %rcx
.LBB2_20:                               # %scalar.ph5118.preheader
                                        #   in Loop: Header=BB2_14 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%ebp, %eax
	movq	80(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %edi
	subl	%ebp, %edi
	andl	$3, %eax
	je	.LBB2_23
# BB#21:                                # %scalar.ph5118.prol.preheader
                                        #   in Loop: Header=BB2_14 Depth=3
	negl	%eax
	.p2align	4, 0x90
.LBB2_22:                               # %scalar.ph5118.prol
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_8 Depth=2
                                        #       Parent Loop BB2_14 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%r12,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%r12,%rcx,8)
	addq	%rsi, %rcx
	incl	%ebp
	incl	%eax
	jne	.LBB2_22
.LBB2_23:                               # %scalar.ph5118.prol.loopexit
                                        #   in Loop: Header=BB2_14 Depth=3
	cmpl	$3, %edi
	jb	.LBB2_26
# BB#24:                                # %scalar.ph5118.preheader.new
                                        #   in Loop: Header=BB2_14 Depth=3
	movq	72(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx,8), %rcx
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %edi
	subl	%ebp, %edi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_25:                               # %scalar.ph5118
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_8 Depth=2
                                        #       Parent Loop BB2_14 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leaq	(%r13,%rax), %rdx
	movsd	(%rcx,%rdx), %xmm0      # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rcx,%rdx)
	leaq	(%r14,%rax), %rdx
	movsd	(%rcx,%rdx), %xmm0      # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rcx,%rdx)
	leaq	(%r11,%rax), %rdx
	movsd	(%rcx,%rdx), %xmm0      # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rcx,%rdx)
	leaq	(%r10,%rax), %rdx
	movsd	(%rcx,%rdx), %xmm0      # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rcx,%rdx)
	addq	%r8, %rax
	addl	$-4, %edi
	jne	.LBB2_25
.LBB2_26:                               # %._crit_edge.us.us.us.us
                                        #   in Loop: Header=BB2_14 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	(%r15,%rax), %eax
	incl	%ebx
	cmpl	32(%rsp), %ebx          # 4-byte Folded Reload
	jne	.LBB2_14
# BB#27:                                # %._crit_edge3702.us-lcssa.us.us.us.us
                                        #   in Loop: Header=BB2_8 Depth=2
	movl	344(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movl	160(%rsp), %ecx         # 4-byte Reload
	addl	104(%rsp), %ecx         # 4-byte Folded Reload
	cmpl	536(%rsp), %eax         # 4-byte Folded Reload
	jne	.LBB2_8
	.p2align	4, 0x90
.LBB2_28:                               # %._crit_edge3722
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	56(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	movq	88(%rsp), %rcx          # 8-byte Reload
	movslq	8(%rcx), %rax
	cmpq	%rax, %rdx
	jl	.LBB2_3
	jmp	.LBB2_247
.LBB2_29:
	movq	24(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, 472(%rsp)         # 8-byte Spill
	divsd	%xmm0, %xmm1
	movapd	%xmm1, 688(%rsp)        # 16-byte Spill
	movslq	8(%rax), %rax
	movq	%rax, 672(%rsp)         # 8-byte Spill
	movapd	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movaps	%xmm1, 752(%rsp)        # 16-byte Spill
                                        # implicit-def: %RDX
	xorl	%eax, %eax
	movq	%rbp, 408(%rsp)         # 8-byte Spill
	movq	%r15, 648(%rsp)         # 8-byte Spill
	movapd	%xmm0, 768(%rsp)        # 16-byte Spill
	.p2align	4, 0x90
.LBB2_30:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_35 Depth 2
                                        #       Child Loop BB2_56 Depth 3
                                        #         Child Loop BB2_57 Depth 4
                                        #           Child Loop BB2_59 Depth 5
                                        #           Child Loop BB2_63 Depth 5
                                        #       Child Loop BB2_41 Depth 3
                                        #         Child Loop BB2_42 Depth 4
                                        #           Child Loop BB2_44 Depth 5
                                        #           Child Loop BB2_48 Depth 5
                                        #     Child Loop BB2_71 Depth 2
                                        #       Child Loop BB2_73 Depth 3
                                        #         Child Loop BB2_75 Depth 4
                                        #           Child Loop BB2_205 Depth 5
                                        #             Child Loop BB2_208 Depth 6
                                        #               Child Loop BB2_209 Depth 7
                                        #           Child Loop BB2_191 Depth 5
                                        #             Child Loop BB2_194 Depth 6
                                        #               Child Loop BB2_195 Depth 7
                                        #           Child Loop BB2_155 Depth 5
                                        #             Child Loop BB2_158 Depth 6
                                        #               Child Loop BB2_159 Depth 7
                                        #           Child Loop BB2_126 Depth 5
                                        #             Child Loop BB2_132 Depth 6
                                        #               Child Loop BB2_143 Depth 7
                                        #               Child Loop BB2_148 Depth 7
                                        #           Child Loop BB2_102 Depth 5
                                        #             Child Loop BB2_105 Depth 6
                                        #               Child Loop BB2_114 Depth 7
                                        #               Child Loop BB2_119 Depth 7
                                        #           Child Loop BB2_169 Depth 5
                                        #             Child Loop BB2_172 Depth 6
                                        #               Child Loop BB2_179 Depth 7
                                        #               Child Loop BB2_184 Depth 7
                                        #           Child Loop BB2_80 Depth 5
                                        #             Child Loop BB2_83 Depth 6
                                        #               Child Loop BB2_88 Depth 7
                                        #               Child Loop BB2_95 Depth 7
                                        #         Child Loop BB2_223 Depth 4
                                        #           Child Loop BB2_229 Depth 5
                                        #             Child Loop BB2_226 Depth 6
                                        #             Child Loop BB2_237 Depth 6
                                        #             Child Loop BB2_240 Depth 6
	cmpl	$1, %eax
	movl	%eax, 620(%rsp)         # 4-byte Spill
	je	.LBB2_68
# BB#31:                                #   in Loop: Header=BB2_30 Depth=1
	testl	%eax, %eax
	jne	.LBB2_69
# BB#32:                                #   in Loop: Header=BB2_30 Depth=1
	movq	24(%r15), %rsi
	movq	384(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rdi
	leaq	744(%rsp), %rdx
	callq	hypre_InitializeIndtComputations
	movq	8(%r14), %rdx
	movapd	688(%rsp), %xmm0        # 16-byte Reload
	ucomisd	.LCPI2_0(%rip), %xmm0
	jne	.LBB2_33
	jnp	.LBB2_69
.LBB2_33:                               #   in Loop: Header=BB2_30 Depth=1
	movq	8(%rbp), %rax
	movq	8(%rax), %rcx
	cmpl	$0, 8(%rcx)
	jle	.LBB2_69
# BB#34:                                # %.lr.ph3888
                                        #   in Loop: Header=BB2_30 Depth=1
	movq	%rdx, 576(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	movq	%rcx, 344(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_35:                               #   Parent Loop BB2_30 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_56 Depth 3
                                        #         Child Loop BB2_57 Depth 4
                                        #           Child Loop BB2_59 Depth 5
                                        #           Child Loop BB2_63 Depth 5
                                        #       Child Loop BB2_41 Depth 3
                                        #         Child Loop BB2_42 Depth 4
                                        #           Child Loop BB2_44 Depth 5
                                        #           Child Loop BB2_48 Depth 5
	movq	(%rcx), %r14
	leaq	(,%rdx,8), %rax
	leaq	(%rax,%rax,2), %rbx
	leaq	(%r14,%rbx), %rdi
	movq	584(%rsp), %rcx         # 8-byte Reload
	movq	16(%rcx), %rax
	movq	24(%rcx), %r15
	movq	(%rax), %rbp
	movq	40(%rcx), %rax
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movslq	(%rax,%rdx,4), %r13
	leaq	436(%rsp), %rsi
	callq	hypre_BoxGetSize
	xorpd	%xmm0, %xmm0
	movapd	688(%rsp), %xmm1        # 16-byte Reload
	movl	(%rbp,%rbx), %esi
	movl	4(%rbp,%rbx), %edi
	movl	12(%rbp,%rbx), %ecx
	movl	%ecx, %eax
	subl	%esi, %eax
	incl	%eax
	cmpl	%esi, %ecx
	movl	16(%rbp,%rbx), %ecx
	movl	$0, %r9d
	cmovsl	%r9d, %eax
	movl	%ecx, %r12d
	subl	%edi, %r12d
	incl	%r12d
	cmpl	%edi, %ecx
	leaq	(%r15,%r13,8), %rcx
	movl	(%r14,%rbx), %r8d
	movl	4(%r14,%rbx), %edx
	cmovsl	%r9d, %r12d
	subl	%esi, %r8d
	movl	8(%r14,%rbx), %esi
	subl	%edi, %edx
	movl	%esi, %edi
	subl	8(%rbp,%rbx), %edi
	imull	%r12d, %edi
	addl	%edx, %edi
	imull	%eax, %edi
	movq	384(%rsp), %rdx         # 8-byte Reload
	movslq	24(%rdx), %rsi
	movl	28(%rdx), %ebp
	imull	%eax, %ebp
	movl	%ebp, 24(%rsp)          # 4-byte Spill
	imull	%eax, %r12d
	imull	32(%rdx), %r12d
	movl	436(%rsp), %eax
	movl	440(%rsp), %edx
	movl	444(%rsp), %ebp
	cmpl	%eax, %edx
	movq	%rax, 16(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%edx, 8(%rsp)           # 4-byte Spill
	cmovgel	%edx, %eax
	cmpl	%eax, %ebp
	movl	%ebp, 72(%rsp)          # 4-byte Spill
	cmovgel	%ebp, %eax
	ucomisd	%xmm0, %xmm1
	jne	.LBB2_51
	jp	.LBB2_51
# BB#36:                                #   in Loop: Header=BB2_35 Depth=2
	testl	%eax, %eax
	jle	.LBB2_66
# BB#37:                                # %.lr.ph3847
                                        #   in Loop: Header=BB2_35 Depth=2
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_66
# BB#38:                                # %.lr.ph3847.split.us.preheader
                                        #   in Loop: Header=BB2_35 Depth=2
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB2_66
# BB#39:                                # %.lr.ph3847.split.us.preheader
                                        #   in Loop: Header=BB2_35 Depth=2
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_66
# BB#40:                                # %.preheader3691.us.us.us.preheader
                                        #   in Loop: Header=BB2_35 Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %eax
	imull	%esi, %eax
	subl	%eax, 24(%rsp)          # 4-byte Folded Spill
	movl	%esi, %r9d
	imull	%edx, %r9d
	addl	%r8d, %edi
	leal	-1(%rdx), %r14d
	movl	%edx, %ebx
	andl	$7, %ebx
	movq	%rsi, %r10
	shlq	$6, %r10
	leaq	(,%rsi,8), %rbp
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB2_41:                               # %.preheader3691.us.us.us
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_35 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_42 Depth 4
                                        #           Child Loop BB2_44 Depth 5
                                        #           Child Loop BB2_48 Depth 5
	movl	%edi, %r13d
	movl	%edi, %r15d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB2_42:                               # %.preheader3682.us.us.us.us
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_35 Depth=2
                                        #       Parent Loop BB2_41 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB2_44 Depth 5
                                        #           Child Loop BB2_48 Depth 5
	testl	%ebx, %ebx
	movslq	%r15d, %rax
	je	.LBB2_45
# BB#43:                                # %.prol.preheader5170
                                        #   in Loop: Header=BB2_42 Depth=4
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_44:                               #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_35 Depth=2
                                        #       Parent Loop BB2_41 Depth=3
                                        #         Parent Loop BB2_42 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	$0, (%rcx,%rax,8)
	addq	%rsi, %rax
	incl	%edi
	cmpl	%edi, %ebx
	jne	.LBB2_44
	jmp	.LBB2_46
	.p2align	4, 0x90
.LBB2_45:                               #   in Loop: Header=BB2_42 Depth=4
	xorl	%edi, %edi
.LBB2_46:                               # %.prol.loopexit5171
                                        #   in Loop: Header=BB2_42 Depth=4
	cmpl	$7, %r14d
	jb	.LBB2_49
# BB#47:                                # %.preheader3682.us.us.us.us.new
                                        #   in Loop: Header=BB2_42 Depth=4
	leaq	(%rcx,%rax,8), %rdx
	movq	16(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%edi, %eax
	.p2align	4, 0x90
.LBB2_48:                               #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_35 Depth=2
                                        #       Parent Loop BB2_41 Depth=3
                                        #         Parent Loop BB2_42 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	$0, (%rdx)
	leaq	(%rdx,%rbp), %rdi
	movq	$0, (%rdx,%rsi,8)
	movq	$0, (%rdi,%rsi,8)
	addq	%rbp, %rdi
	movq	$0, (%rdi,%rsi,8)
	addq	%rbp, %rdi
	movq	$0, (%rdi,%rsi,8)
	addq	%rbp, %rdi
	movq	$0, (%rdi,%rsi,8)
	addq	%rbp, %rdi
	movq	$0, (%rdi,%rsi,8)
	addq	%rbp, %rdi
	movq	$0, (%rdi,%rsi,8)
	addq	%r10, %rdx
	addl	$-8, %eax
	jne	.LBB2_48
.LBB2_49:                               # %._crit_edge3824.us.us.us.us
                                        #   in Loop: Header=BB2_42 Depth=4
	addl	%r9d, %r15d
	addl	24(%rsp), %r15d         # 4-byte Folded Reload
	incl	%r11d
	cmpl	8(%rsp), %r11d          # 4-byte Folded Reload
	jne	.LBB2_42
# BB#50:                                # %._crit_edge3828.us-lcssa.us.us.us.us
                                        #   in Loop: Header=BB2_41 Depth=3
	movl	%r13d, %edi
	addl	%r12d, %edi
	incl	%r8d
	cmpl	72(%rsp), %r8d          # 4-byte Folded Reload
	jne	.LBB2_41
	jmp	.LBB2_66
	.p2align	4, 0x90
.LBB2_51:                               #   in Loop: Header=BB2_35 Depth=2
	movq	%r15, 48(%rsp)          # 8-byte Spill
	testl	%eax, %eax
	jle	.LBB2_66
# BB#52:                                # %.lr.ph3786
                                        #   in Loop: Header=BB2_35 Depth=2
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_66
# BB#53:                                # %.lr.ph3786.split.us.preheader
                                        #   in Loop: Header=BB2_35 Depth=2
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB2_66
# BB#54:                                # %.lr.ph3786.split.us.preheader
                                        #   in Loop: Header=BB2_35 Depth=2
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_66
# BB#55:                                # %.preheader3692.us.us.us.preheader
                                        #   in Loop: Header=BB2_35 Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %eax
	imull	%esi, %eax
	subl	%eax, 24(%rsp)          # 4-byte Folded Spill
	movl	%esi, %eax
	imull	%edx, %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	addl	%r8d, %edi
	leal	-1(%rdx), %eax
	movl	%eax, 80(%rsp)          # 4-byte Spill
	movl	%edx, %ebx
	andl	$3, %ebx
	movq	%rsi, %rax
	shlq	$5, %rax
	leaq	(%rsi,%rsi,2), %rdx
	shlq	$3, %rdx
	leaq	(%rdx,%r13,8), %r14
	movq	%rsi, %rdx
	shlq	$4, %rdx
	leaq	(%rdx,%r13,8), %r15
	leaq	(,%r13,8), %r8
	leaq	(%r8,%rsi,8), %r9
	xorl	%edx, %edx
	movl	%r12d, 168(%rsp)        # 4-byte Spill
	.p2align	4, 0x90
.LBB2_56:                               # %.preheader3692.us.us.us
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_35 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_57 Depth 4
                                        #           Child Loop BB2_59 Depth 5
                                        #           Child Loop BB2_63 Depth 5
	movl	%edx, 64(%rsp)          # 4-byte Spill
	movl	%edi, 136(%rsp)         # 4-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_57:                               # %.preheader3683.us.us.us.us
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_35 Depth=2
                                        #       Parent Loop BB2_56 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB2_59 Depth 5
                                        #           Child Loop BB2_63 Depth 5
	testl	%ebx, %ebx
	movslq	%edi, %rdx
	je	.LBB2_60
# BB#58:                                # %.prol.preheader
                                        #   in Loop: Header=BB2_57 Depth=4
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB2_59:                               #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_35 Depth=2
                                        #       Parent Loop BB2_56 Depth=3
                                        #         Parent Loop BB2_57 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movsd	(%rcx,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rcx,%rdx,8)
	addq	%rsi, %rdx
	incl	%r10d
	cmpl	%r10d, %ebx
	jne	.LBB2_59
	jmp	.LBB2_61
	.p2align	4, 0x90
.LBB2_60:                               #   in Loop: Header=BB2_57 Depth=4
	xorl	%r10d, %r10d
.LBB2_61:                               # %.prol.loopexit
                                        #   in Loop: Header=BB2_57 Depth=4
	cmpl	$3, 80(%rsp)            # 4-byte Folded Reload
	jb	.LBB2_64
# BB#62:                                # %.preheader3683.us.us.us.us.new
                                        #   in Loop: Header=BB2_57 Depth=4
	movq	48(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rdx,8), %r11
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %r13d
	subl	%r10d, %r13d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_63:                               #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_35 Depth=2
                                        #       Parent Loop BB2_56 Depth=3
                                        #         Parent Loop BB2_57 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	leaq	(%r8,%rdx), %rbp
	movsd	(%r11,%rbp), %xmm0      # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%r11,%rbp)
	leaq	(%r9,%rdx), %rbp
	movsd	(%r11,%rbp), %xmm0      # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%r11,%rbp)
	leaq	(%r15,%rdx), %rbp
	movsd	(%r11,%rbp), %xmm0      # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%r11,%rbp)
	leaq	(%r14,%rdx), %rbp
	movsd	(%r11,%rbp), %xmm0      # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%r11,%rbp)
	addq	%rax, %rdx
	addl	$-4, %r13d
	jne	.LBB2_63
.LBB2_64:                               # %._crit_edge3763.us.us.us.us
                                        #   in Loop: Header=BB2_57 Depth=4
	addl	32(%rsp), %edi          # 4-byte Folded Reload
	addl	24(%rsp), %edi          # 4-byte Folded Reload
	incl	%r12d
	cmpl	8(%rsp), %r12d          # 4-byte Folded Reload
	jne	.LBB2_57
# BB#65:                                # %._crit_edge3767.us-lcssa.us.us.us.us
                                        #   in Loop: Header=BB2_56 Depth=3
	movl	168(%rsp), %r12d        # 4-byte Reload
	movl	136(%rsp), %edi         # 4-byte Reload
	addl	%r12d, %edi
	movl	64(%rsp), %edx          # 4-byte Reload
	incl	%edx
	cmpl	72(%rsp), %edx          # 4-byte Folded Reload
	jne	.LBB2_56
	.p2align	4, 0x90
.LBB2_66:                               # %.loopexit3694
                                        #   in Loop: Header=BB2_35 Depth=2
	movq	128(%rsp), %rdx         # 8-byte Reload
	incq	%rdx
	movq	344(%rsp), %rcx         # 8-byte Reload
	movslq	8(%rcx), %rax
	cmpq	%rax, %rdx
	jl	.LBB2_35
# BB#67:                                #   in Loop: Header=BB2_30 Depth=1
	movq	408(%rsp), %rbp         # 8-byte Reload
	movq	648(%rsp), %r15         # 8-byte Reload
	movq	576(%rsp), %rdx         # 8-byte Reload
	jmp	.LBB2_69
	.p2align	4, 0x90
.LBB2_68:                               #   in Loop: Header=BB2_30 Depth=1
	movq	744(%rsp), %rdi
	callq	hypre_FinalizeIndtComputations
	movq	384(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rdx
.LBB2_69:                               # %.loopexit3696
                                        #   in Loop: Header=BB2_30 Depth=1
	movl	8(%rdx), %eax
	testl	%eax, %eax
	jle	.LBB2_246
# BB#70:                                # %.lr.ph4303
                                        #   in Loop: Header=BB2_30 Depth=1
	xorl	%esi, %esi
	movq	%rdx, 576(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_71:                               #   Parent Loop BB2_30 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_73 Depth 3
                                        #         Child Loop BB2_75 Depth 4
                                        #           Child Loop BB2_205 Depth 5
                                        #             Child Loop BB2_208 Depth 6
                                        #               Child Loop BB2_209 Depth 7
                                        #           Child Loop BB2_191 Depth 5
                                        #             Child Loop BB2_194 Depth 6
                                        #               Child Loop BB2_195 Depth 7
                                        #           Child Loop BB2_155 Depth 5
                                        #             Child Loop BB2_158 Depth 6
                                        #               Child Loop BB2_159 Depth 7
                                        #           Child Loop BB2_126 Depth 5
                                        #             Child Loop BB2_132 Depth 6
                                        #               Child Loop BB2_143 Depth 7
                                        #               Child Loop BB2_148 Depth 7
                                        #           Child Loop BB2_102 Depth 5
                                        #             Child Loop BB2_105 Depth 6
                                        #               Child Loop BB2_114 Depth 7
                                        #               Child Loop BB2_119 Depth 7
                                        #           Child Loop BB2_169 Depth 5
                                        #             Child Loop BB2_172 Depth 6
                                        #               Child Loop BB2_179 Depth 7
                                        #               Child Loop BB2_184 Depth 7
                                        #           Child Loop BB2_80 Depth 5
                                        #             Child Loop BB2_83 Depth 6
                                        #               Child Loop BB2_88 Depth 7
                                        #               Child Loop BB2_95 Depth 7
                                        #         Child Loop BB2_223 Depth 4
                                        #           Child Loop BB2_229 Depth 5
                                        #             Child Loop BB2_226 Depth 6
                                        #             Child Loop BB2_237 Depth 6
                                        #             Child Loop BB2_240 Depth 6
	movq	(%rdx), %rcx
	movq	(%rcx,%rsi,8), %rdi
	cmpl	$0, 8(%rdi)
	jle	.LBB2_245
# BB#72:                                # %.lr.ph4267
                                        #   in Loop: Header=BB2_71 Depth=2
	movq	40(%rbp), %rax
	movq	(%rax), %rbp
	movq	16(%r15), %rax
	movq	24(%r15), %r10
	movq	(%rax), %rax
	movq	584(%rsp), %rdx         # 8-byte Reload
	movq	16(%rdx), %rcx
	movq	24(%rdx), %r9
	movq	(%rcx), %r8
	movq	40(%r15), %rcx
	movslq	(%rcx,%rsi,4), %rbx
	movq	%r10, 520(%rsp)         # 8-byte Spill
	leaq	(%r10,%rbx,8), %rcx
	movq	%rcx, 376(%rsp)         # 8-byte Spill
	movq	40(%rdx), %rcx
	movslq	(%rcx,%rsi,4), %rdx
	movq	%r9, 680(%rsp)          # 8-byte Spill
	leaq	(%r9,%rdx,8), %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movq	%rsi, 448(%rsp)         # 8-byte Spill
	leaq	(,%rsi,8), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	16(%rax,%rcx), %rsi
	movq	%rsi, 320(%rsp)         # 8-byte Spill
	addq	%rcx, %rax
	movq	%rax, 488(%rsp)         # 8-byte Spill
	leaq	4(%rbp,%rcx), %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	addq	%rcx, %rbp
	movq	%rbp, 496(%rsp)         # 8-byte Spill
	leaq	(%r8,%rcx), %rax
	movq	%rax, 464(%rsp)         # 8-byte Spill
	leaq	4(%r8,%rcx), %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	shlq	$3, %rbx
	movq	%rbx, 160(%rsp)         # 8-byte Spill
	movq	%rdx, %rbx
	shlq	$3, %rbx
	xorl	%ecx, %ecx
	movq	%rdi, 704(%rsp)         # 8-byte Spill
	movq	%rbx, 536(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_73:                               #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_75 Depth 4
                                        #           Child Loop BB2_205 Depth 5
                                        #             Child Loop BB2_208 Depth 6
                                        #               Child Loop BB2_209 Depth 7
                                        #           Child Loop BB2_191 Depth 5
                                        #             Child Loop BB2_194 Depth 6
                                        #               Child Loop BB2_195 Depth 7
                                        #           Child Loop BB2_155 Depth 5
                                        #             Child Loop BB2_158 Depth 6
                                        #               Child Loop BB2_159 Depth 7
                                        #           Child Loop BB2_126 Depth 5
                                        #             Child Loop BB2_132 Depth 6
                                        #               Child Loop BB2_143 Depth 7
                                        #               Child Loop BB2_148 Depth 7
                                        #           Child Loop BB2_102 Depth 5
                                        #             Child Loop BB2_105 Depth 6
                                        #               Child Loop BB2_114 Depth 7
                                        #               Child Loop BB2_119 Depth 7
                                        #           Child Loop BB2_169 Depth 5
                                        #             Child Loop BB2_172 Depth 6
                                        #               Child Loop BB2_179 Depth 7
                                        #               Child Loop BB2_184 Depth 7
                                        #           Child Loop BB2_80 Depth 5
                                        #             Child Loop BB2_83 Depth 6
                                        #               Child Loop BB2_88 Depth 7
                                        #               Child Loop BB2_95 Depth 7
                                        #         Child Loop BB2_223 Depth 4
                                        #           Child Loop BB2_229 Depth 5
                                        #             Child Loop BB2_226 Depth 6
                                        #             Child Loop BB2_237 Depth 6
                                        #             Child Loop BB2_240 Depth 6
	movq	(%rdi), %rax
	movq	%rcx, 712(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rcx,2), %rcx
	movq	%rax, 664(%rsp)         # 8-byte Spill
	movq	%rcx, 656(%rsp)         # 8-byte Spill
	leaq	(%rax,%rcx,8), %rdi
	movq	%rdi, 456(%rsp)         # 8-byte Spill
	leaq	436(%rsp), %rsi
	callq	hypre_BoxGetSize
	movapd	768(%rsp), %xmm7        # 16-byte Reload
	movq	672(%rsp), %rcx         # 8-byte Reload
	testl	%ecx, %ecx
	jle	.LBB2_217
# BB#74:                                # %.lr.ph4172
                                        #   in Loop: Header=BB2_73 Depth=3
	movq	664(%rsp), %rax         # 8-byte Reload
	movq	656(%rsp), %rdx         # 8-byte Reload
	leaq	4(%rax,%rdx,8), %rax
	movq	%rax, 480(%rsp)         # 8-byte Spill
	movl	436(%rsp), %eax
	movl	440(%rsp), %edx
	movl	444(%rsp), %esi
	cmpl	%eax, %edx
	movl	%eax, %edi
	cmovgel	%edx, %edi
	cmpl	%edi, %esi
	cmovgel	%esi, %edi
	movl	%edi, 308(%rsp)         # 4-byte Spill
	leal	-1(%rdx), %edi
	movl	%edi, 304(%rsp)         # 4-byte Spill
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leal	-1(%rax), %edi
	leaq	1(%rdi), %rbp
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	8(%rax,%rdi,8), %rax
	movq	%rax, 568(%rsp)         # 8-byte Spill
	movq	376(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 528(%rsp)         # 8-byte Spill
	leaq	8(%rax,%rdi,8), %rax
	movq	%rax, 560(%rsp)         # 8-byte Spill
	movl	%esi, 212(%rsp)         # 4-byte Spill
	testl	%esi, %esi
	setle	%al
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	testl	%edx, %edx
	setle	%dl
	orb	%al, %dl
	movb	%dl, 47(%rsp)           # 1-byte Spill
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	leaq	2(%rax), %rdx
	andq	%rbp, %rdx
	movq	%rdx, 400(%rsp)         # 8-byte Spill
	movq	%rbp, 344(%rsp)         # 8-byte Spill
	andq	%rax, %rbp
	movq	%rbp, 608(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_75:                               #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB2_205 Depth 5
                                        #             Child Loop BB2_208 Depth 6
                                        #               Child Loop BB2_209 Depth 7
                                        #           Child Loop BB2_191 Depth 5
                                        #             Child Loop BB2_194 Depth 6
                                        #               Child Loop BB2_195 Depth 7
                                        #           Child Loop BB2_155 Depth 5
                                        #             Child Loop BB2_158 Depth 6
                                        #               Child Loop BB2_159 Depth 7
                                        #           Child Loop BB2_126 Depth 5
                                        #             Child Loop BB2_132 Depth 6
                                        #               Child Loop BB2_143 Depth 7
                                        #               Child Loop BB2_148 Depth 7
                                        #           Child Loop BB2_102 Depth 5
                                        #             Child Loop BB2_105 Depth 6
                                        #               Child Loop BB2_114 Depth 7
                                        #               Child Loop BB2_119 Depth 7
                                        #           Child Loop BB2_169 Depth 5
                                        #             Child Loop BB2_172 Depth 6
                                        #               Child Loop BB2_179 Depth 7
                                        #               Child Loop BB2_184 Depth 7
                                        #           Child Loop BB2_80 Depth 5
                                        #             Child Loop BB2_83 Depth 6
                                        #               Child Loop BB2_88 Depth 7
                                        #               Child Loop BB2_95 Depth 7
	movq	%rcx, %rax
	movq	%rdx, 280(%rsp)         # 8-byte Spill
	subq	%rdx, %rax
	cmpq	$7, %rax
	movl	$7, %ecx
	cmovgel	%ecx, %eax
	decl	%eax
	cmpl	$6, %eax
	ja	.LBB2_216
# BB#76:                                #   in Loop: Header=BB2_75 Depth=4
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_77:                               #   in Loop: Header=BB2_75 Depth=4
	movq	464(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r10d
	movq	272(%rsp), %rcx         # 8-byte Reload
	movl	8(%rcx), %eax
	movl	%eax, %r9d
	subl	%r10d, %r9d
	incl	%r9d
	cmpl	%r10d, %eax
	movl	(%rcx), %r11d
	movl	12(%rcx), %eax
	movl	$0, %edx
	cmovsl	%edx, %r9d
	movl	%eax, %esi
	subl	%r11d, %esi
	incl	%esi
	cmpl	%r11d, %eax
	movq	496(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r8d
	movq	312(%rsp), %rcx         # 8-byte Reload
	movl	8(%rcx), %eax
	cmovsl	%edx, %esi
	movl	%eax, %r15d
	subl	%r8d, %r15d
	incl	%r15d
	cmpl	%r8d, %eax
	movl	(%rcx), %ebx
	movl	12(%rcx), %eax
	cmovsl	%edx, %r15d
	movl	%eax, %r13d
	subl	%ebx, %r13d
	incl	%r13d
	cmpl	%ebx, %eax
	movq	320(%rsp), %rdi         # 8-byte Reload
	movl	-4(%rdi), %eax
	movq	488(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx), %r14d
	cmovsl	%edx, %r13d
	movl	%eax, %ecx
	subl	%r14d, %ecx
	incl	%ecx
	cmpl	%r14d, %eax
	movl	(%rdi), %eax
	movl	-12(%rdi), %edi
	cmovsl	%edx, %ecx
	movl	%eax, %ebp
	subl	%edi, %ebp
	incl	%ebp
	cmpl	%edi, %eax
	cmovsl	%edx, %ebp
	cmpl	$0, 308(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_216
# BB#78:                                # %.lr.ph3939
                                        #   in Loop: Header=BB2_75 Depth=4
	cmpb	$0, 47(%rsp)            # 1-byte Folded Reload
	jne	.LBB2_216
# BB#79:                                # %.preheader3680.us.preheader
                                        #   in Loop: Header=BB2_75 Depth=4
	movq	384(%rsp), %rdx         # 8-byte Reload
	movl	32(%rdx), %eax
	movq	%rdx, %r12
	movl	%r9d, %edx
	imull	%esi, %edx
	imull	%eax, %edx
	movl	%edx, 120(%rsp)         # 4-byte Spill
	movl	%ecx, %edx
	imull	%ebp, %edx
	imull	%eax, %edx
	movl	%edx, 184(%rsp)         # 4-byte Spill
	movl	%r10d, 72(%rsp)         # 4-byte Spill
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	movl	%r15d, %r10d
	imull	%r13d, %r10d
	imull	%eax, %r10d
	movq	280(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rax,2), %rax
	movl	%edi, 48(%rsp)          # 4-byte Spill
	movq	472(%rsp), %rdi         # 8-byte Reload
	movl	8(%rdi,%rax,4), %edx
	imull	%ebp, %edx
	addl	4(%rdi,%rax,4), %edx
	imull	%ecx, %edx
	movslq	(%rdi,%rax,4), %rax
	movslq	%edx, %rdx
	addq	%rax, %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%r12, %rax
	movl	28(%rax), %edx
	movl	%edx, %r12d
	imull	%r15d, %r12d
	movl	%r9d, %ebx
	imull	%edx, %ebx
	imull	%ecx, %edx
	movslq	24(%rax), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	%r14d, 96(%rsp)         # 4-byte Spill
	movl	%eax, %r14d
	negl	%r14d
	imull	64(%rsp), %r14d         # 4-byte Folded Reload
	movl	%r11d, 56(%rsp)         # 4-byte Spill
	leal	(%r14,%r12), %r11d
	movl	%r8d, 136(%rsp)         # 4-byte Spill
	leal	(%r14,%rdx), %r8d
	addl	%ebx, %r14d
	movl	304(%rsp), %edi         # 4-byte Reload
	imull	%edi, %r11d
	addl	%r12d, %r11d
	movq	104(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	64(%rsp), %eax          # 4-byte Folded Reload
	subl	%eax, %r11d
	movl	%r11d, 176(%rsp)        # 4-byte Spill
	imull	%edi, %r8d
	addl	%edx, %r8d
	subl	%eax, %r8d
	movl	%r8d, 112(%rsp)         # 4-byte Spill
	imull	%edi, %r14d
	addl	%ebx, %r14d
	subl	%eax, %r14d
	movq	%r14, 216(%rsp)         # 8-byte Spill
	movq	168(%rsp), %rax         # 8-byte Reload
	movl	%eax, %edi
	movq	%r12, 16(%rsp)          # 8-byte Spill
	imull	%r12d, %edi
	movl	%edi, 152(%rsp)         # 4-byte Spill
	subl	%edi, %r10d
	movl	%r10d, 240(%rsp)        # 4-byte Spill
	movl	%eax, %edi
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	imull	%edx, %edi
	movl	%edi, 336(%rsp)         # 4-byte Spill
	subl	%edi, 184(%rsp)         # 4-byte Folded Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%ebx, 32(%rsp)          # 4-byte Spill
	imull	%ebx, %eax
	movl	%eax, 200(%rsp)         # 4-byte Spill
	subl	%eax, 120(%rsp)         # 4-byte Folded Spill
	movq	608(%rsp), %r14         # 8-byte Reload
	testq	%r14, %r14
	setne	%r12b
	movq	456(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r11d
	movl	%r11d, %r8d
	subl	72(%rsp), %r8d          # 4-byte Folded Reload
	movq	480(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx), %r10d
	movl	%r10d, %eax
	subl	56(%rsp), %eax          # 4-byte Folded Reload
	movl	4(%rdx), %ebx
	movl	%ebx, %edi
	movq	272(%rsp), %rdx         # 8-byte Reload
	subl	4(%rdx), %edi
	imull	%esi, %edi
	addl	%eax, %edi
	imull	%r9d, %edi
	addl	%r8d, %edi
	movl	%edi, 56(%rsp)          # 4-byte Spill
	movl	%r11d, %eax
	subl	96(%rsp), %eax          # 4-byte Folded Reload
	movl	%r10d, %esi
	subl	48(%rsp), %esi          # 4-byte Folded Reload
	movl	%ebx, %edx
	movq	320(%rsp), %rdi         # 8-byte Reload
	subl	-8(%rdi), %edx
	imull	%ebp, %edx
	addl	%esi, %edx
	imull	%ecx, %edx
	addl	%eax, %edx
	movl	%edx, 352(%rsp)         # 4-byte Spill
	subl	136(%rsp), %r11d        # 4-byte Folded Reload
	subl	8(%rsp), %r10d          # 4-byte Folded Reload
	movq	312(%rsp), %rax         # 8-byte Reload
	subl	4(%rax), %ebx
	imull	%r13d, %ebx
	addl	%r10d, %ebx
	imull	%r15d, %ebx
	addl	%r11d, %ebx
	movl	%ebx, 88(%rsp)          # 4-byte Spill
	movq	408(%rsp), %rcx         # 8-byte Reload
	movq	64(%rcx), %rax
	movq	448(%rsp), %rdx         # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	280(%rsp), %rdx         # 8-byte Reload
	movslq	(%rax,%rdx,4), %rax
	movq	48(%rcx), %rcx
	leaq	(%rcx,%rax,8), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	528(%rsp), %rdx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	leaq	8(%rcx,%rax,8), %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	104(%rsp), %rdx         # 8-byte Reload
	cmpl	$1, %edx
	sete	%al
	andb	%r12b, %al
	movb	%al, 72(%rsp)           # 1-byte Spill
	movq	%rdx, %rax
	imulq	%r14, %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	%rdx, %rsi
	shlq	$5, %rsi
	movq	%rdx, %r14
	shlq	$4, %r14
	movq	376(%rsp), %rax         # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	(,%rdx,8), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	xorl	%edi, %edi
	movq	%rsi, 224(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_80:                               # %.preheader3680.us
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB2_83 Depth 6
                                        #               Child Loop BB2_88 Depth 7
                                        #               Child Loop BB2_95 Depth 7
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	movq	216(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	112(%rsp), %ecx         # 4-byte Reload
	movl	176(%rsp), %edx         # 4-byte Reload
	jle	.LBB2_98
# BB#81:                                # %.preheader3672.us.us.preheader
                                        #   in Loop: Header=BB2_80 Depth=5
	movl	%edi, 248(%rsp)         # 4-byte Spill
	xorl	%r13d, %r13d
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	352(%rsp), %r11d        # 4-byte Reload
	movl	88(%rsp), %r10d         # 4-byte Reload
	movq	128(%rsp), %r15         # 8-byte Reload
	jmp	.LBB2_83
.LBB2_82:                               #   in Loop: Header=BB2_83 Depth=6
	movq	%rdx, %r9
	movq	%rcx, %rax
	xorl	%ecx, %ecx
	movq	128(%rsp), %r15         # 8-byte Reload
	jmp	.LBB2_91
	.p2align	4, 0x90
.LBB2_83:                               # %.preheader3672.us.us
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        #           Parent Loop BB2_80 Depth=5
                                        # =>          This Loop Header: Depth=6
                                        #               Child Loop BB2_88 Depth 7
                                        #               Child Loop BB2_95 Depth 7
	movslq	%r10d, %rcx
	movslq	%r11d, %r12
	movslq	%r8d, %rdx
	cmpq	$3, 344(%rsp)           # 8-byte Folded Reload
	jbe	.LBB2_90
# BB#84:                                # %min.iters.checked5024
                                        #   in Loop: Header=BB2_83 Depth=6
	cmpb	$0, 72(%rsp)            # 1-byte Folded Reload
	je	.LBB2_90
# BB#85:                                # %vector.memcheck5064
                                        #   in Loop: Header=BB2_83 Depth=6
	movq	16(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r13d, %eax
	addl	88(%rsp), %eax          # 4-byte Folded Reload
	cltq
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	imull	%r13d, %esi
	addl	352(%rsp), %esi         # 4-byte Folded Reload
	movq	%r15, %rdi
	movslq	%esi, %r15
	movl	32(%rsp), %esi          # 4-byte Reload
	imull	%r13d, %esi
	addl	56(%rsp), %esi          # 4-byte Folded Reload
	movslq	%esi, %r9
	leaq	(%rdi,%r9,8), %rsi
	movq	568(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%r9,8), %rdi
	movq	8(%rsp), %rbx           # 8-byte Reload
	leaq	(%rbx,%rax,8), %rbx
	movq	%rbx, 136(%rsp)         # 8-byte Spill
	movq	296(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rax,8), %rbp
	cmpq	%rbp, %rsi
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%r15, 256(%rsp)         # 8-byte Spill
	leaq	(%rbx,%r15), %rbp
	sbbb	%bl, %bl
	cmpq	%rdi, 136(%rsp)         # 8-byte Folded Reload
	sbbb	%r15b, %r15b
	andb	%bl, %r15b
	movq	560(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rbp,8), %rbx
	cmpq	%rbx, %rsi
	movq	376(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rbp,8), %rsi
	sbbb	%bl, %bl
	cmpq	%rdi, %rsi
	sbbb	%sil, %sil
	testb	$1, %r15b
	jne	.LBB2_82
# BB#86:                                # %vector.memcheck5064
                                        #   in Loop: Header=BB2_83 Depth=6
	andb	%sil, %bl
	andb	$1, %bl
	jne	.LBB2_82
# BB#87:                                # %vector.body5020.preheader
                                        #   in Loop: Header=BB2_83 Depth=6
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rcx
	movq	48(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%r12,8), %rsi
	movq	128(%rsp), %r15         # 8-byte Reload
	leaq	(%r15,%rdx,8), %rdx
	movq	192(%rsp), %rdi         # 8-byte Reload
	addq	%rdi, %r9
	movq	256(%rsp), %r12         # 8-byte Reload
	addq	%rdi, %r12
	addq	%rdi, %rax
	movq	608(%rsp), %rbp         # 8-byte Reload
	movl	$16, %ebx
	movq	224(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_88:                               # %vector.body5020
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        #           Parent Loop BB2_80 Depth=5
                                        #             Parent Loop BB2_83 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movupd	-16(%rcx,%rbx), %xmm0
	movupd	(%rcx,%rbx), %xmm1
	movupd	-16(%rsi,%rbx), %xmm2
	movupd	(%rsi,%rbx), %xmm3
	mulpd	%xmm0, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	-16(%rdx,%rbx), %xmm0
	movupd	(%rdx,%rbx), %xmm1
	addpd	%xmm2, %xmm0
	addpd	%xmm3, %xmm1
	movupd	%xmm0, -16(%rdx,%rbx)
	movupd	%xmm1, (%rdx,%rbx)
	addq	%rdi, %rbx
	addq	$-4, %rbp
	jne	.LBB2_88
# BB#89:                                # %middle.block5021
                                        #   in Loop: Header=BB2_83 Depth=6
	movq	608(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, 344(%rsp)         # 8-byte Folded Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	jne	.LBB2_91
	jmp	.LBB2_96
	.p2align	4, 0x90
.LBB2_90:                               #   in Loop: Header=BB2_83 Depth=6
	movq	%rdx, %r9
	movq	%rcx, %rax
	xorl	%ecx, %ecx
.LBB2_91:                               # %scalar.ph5022.preheader
                                        #   in Loop: Header=BB2_83 Depth=6
	movq	64(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	subl	%ecx, %edx
	testb	$1, %dl
	jne	.LBB2_93
# BB#92:                                #   in Loop: Header=BB2_83 Depth=6
	movl	%ecx, %edi
	cmpl	%ecx, 528(%rsp)         # 4-byte Folded Reload
	jne	.LBB2_94
	jmp	.LBB2_96
	.p2align	4, 0x90
.LBB2_93:                               # %scalar.ph5022.prol
                                        #   in Loop: Header=BB2_83 Depth=6
	movq	8(%rsp), %rdx           # 8-byte Reload
	movsd	(%rdx,%rax,8), %xmm0    # xmm0 = mem[0],zero
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r12), %rdx
	movq	376(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rdx,8), %xmm0
	addsd	(%r15,%r9,8), %xmm0
	movsd	%xmm0, (%r15,%r9,8)
	movq	104(%rsp), %rdx         # 8-byte Reload
	addq	%rdx, %rax
	addq	%rdx, %r12
	addq	%rdx, %r9
	leal	1(%rcx), %edi
	cmpl	%ecx, 528(%rsp)         # 4-byte Folded Reload
	je	.LBB2_96
.LBB2_94:                               # %scalar.ph5022.preheader.new
                                        #   in Loop: Header=BB2_83 Depth=6
	leaq	(%r15,%r9,8), %rcx
	movq	96(%rsp), %rbp          # 8-byte Reload
	leaq	(%rcx,%rbp), %rdx
	movq	48(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%r12,8), %rsi
	leaq	(%rsi,%rbp), %r9
	movq	8(%rsp), %rbx           # 8-byte Reload
	leaq	(%rbx,%rax,8), %rax
	leaq	(%rax,%rbp), %rbp
	movq	64(%rsp), %rbx          # 8-byte Reload
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill>
	subl	%edi, %ebx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_95:                               # %scalar.ph5022
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        #           Parent Loop BB2_80 Depth=5
                                        #             Parent Loop BB2_83 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movsd	(%rax,%rdi), %xmm0      # xmm0 = mem[0],zero
	mulsd	(%rsi,%rdi), %xmm0
	addsd	(%rcx,%rdi), %xmm0
	movsd	%xmm0, (%rcx,%rdi)
	movsd	(%rbp,%rdi), %xmm0      # xmm0 = mem[0],zero
	mulsd	(%r9,%rdi), %xmm0
	addsd	(%rdx,%rdi), %xmm0
	movsd	%xmm0, (%rdx,%rdi)
	addq	%r14, %rdi
	addl	$-2, %ebx
	jne	.LBB2_95
.LBB2_96:                               # %._crit_edge3898.us.us
                                        #   in Loop: Header=BB2_83 Depth=6
	addl	16(%rsp), %r10d         # 4-byte Folded Reload
	addl	24(%rsp), %r11d         # 4-byte Folded Reload
	addl	32(%rsp), %r8d          # 4-byte Folded Reload
	incl	%r13d
	cmpl	168(%rsp), %r13d        # 4-byte Folded Reload
	jne	.LBB2_83
# BB#97:                                #   in Loop: Header=BB2_80 Depth=5
	movl	200(%rsp), %eax         # 4-byte Reload
	movl	336(%rsp), %ecx         # 4-byte Reload
	movl	152(%rsp), %edx         # 4-byte Reload
	movl	248(%rsp), %edi         # 4-byte Reload
.LBB2_98:                               # %._crit_edge3906.us
                                        #   in Loop: Header=BB2_80 Depth=5
	addl	88(%rsp), %edx          # 4-byte Folded Reload
	addl	352(%rsp), %ecx         # 4-byte Folded Reload
	addl	56(%rsp), %eax          # 4-byte Folded Reload
	addl	240(%rsp), %edx         # 4-byte Folded Reload
	addl	184(%rsp), %ecx         # 4-byte Folded Reload
	addl	120(%rsp), %eax         # 4-byte Folded Reload
	incl	%edi
	cmpl	212(%rsp), %edi         # 4-byte Folded Reload
	movl	%edx, 88(%rsp)          # 4-byte Spill
	movl	%ecx, 352(%rsp)         # 4-byte Spill
	movl	%eax, 56(%rsp)          # 4-byte Spill
	jne	.LBB2_80
	jmp	.LBB2_216
.LBB2_99:                               #   in Loop: Header=BB2_75 Depth=4
	movq	464(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r9d
	movq	272(%rsp), %rcx         # 8-byte Reload
	movl	8(%rcx), %eax
	movl	%eax, %ebp
	subl	%r9d, %ebp
	incl	%ebp
	cmpl	%r9d, %eax
	movl	(%rcx), %r11d
	movl	12(%rcx), %eax
	movl	$0, %ecx
	cmovsl	%ecx, %ebp
	movl	%eax, %r8d
	subl	%r11d, %r8d
	incl	%r8d
	cmpl	%r11d, %eax
	movq	496(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r10d
	movq	312(%rsp), %rdx         # 8-byte Reload
	movl	8(%rdx), %eax
	cmovsl	%ecx, %r8d
	movl	%eax, %r13d
	subl	%r10d, %r13d
	incl	%r13d
	cmpl	%r10d, %eax
	movl	(%rdx), %r12d
	movl	12(%rdx), %eax
	cmovsl	%ecx, %r13d
	movl	%eax, %r14d
	subl	%r12d, %r14d
	incl	%r14d
	cmpl	%r12d, %eax
	movq	320(%rsp), %rsi         # 8-byte Reload
	movl	-4(%rsi), %edx
	movq	488(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %ebx
	cmovsl	%ecx, %r14d
	movl	%edx, %eax
	subl	%ebx, %eax
	incl	%eax
	cmpl	%ebx, %edx
	movl	(%rsi), %edi
	movl	-12(%rsi), %esi
	cmovsl	%ecx, %eax
	movl	%edi, %edx
	subl	%esi, %edx
	incl	%edx
	cmpl	%esi, %edi
	cmovsl	%ecx, %edx
	cmpl	$0, 308(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_216
# BB#100:                               # %.lr.ph4033
                                        #   in Loop: Header=BB2_75 Depth=4
	cmpb	$0, 47(%rsp)            # 1-byte Folded Reload
	jne	.LBB2_216
# BB#101:                               # %.preheader3678.us.preheader
                                        #   in Loop: Header=BB2_75 Depth=4
	movl	%r11d, 24(%rsp)         # 4-byte Spill
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	movq	384(%rsp), %r11         # 8-byte Reload
	movl	32(%r11), %edi
	movl	%ebp, %ecx
	imull	%r8d, %ecx
	imull	%edi, %ecx
	movl	%ecx, 288(%rsp)         # 4-byte Spill
	movl	%eax, %ecx
	imull	%edx, %ecx
	imull	%edi, %ecx
	movl	%ecx, 328(%rsp)         # 4-byte Spill
	movl	%r13d, %ecx
	imull	%r14d, %ecx
	imull	%edi, %ecx
	movl	%ecx, 392(%rsp)         # 4-byte Spill
	movq	280(%rsp), %rcx         # 8-byte Reload
	leaq	2(%rcx), %rdi
	movq	%rcx, %rbx
	leaq	(%rdi,%rdi,2), %rdi
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movq	472(%rsp), %rsi         # 8-byte Reload
	movl	8(%rsi,%rdi,4), %ecx
	imull	%edx, %ecx
	addl	4(%rsi,%rdi,4), %ecx
	imull	%eax, %ecx
	movslq	(%rsi,%rdi,4), %rdi
	movslq	%ecx, %rcx
	addq	%rdi, %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	leaq	1(%rbx), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movl	8(%rsi,%rcx,4), %edi
	imull	%edx, %edi
	addl	4(%rsi,%rcx,4), %edi
	imull	%eax, %edi
	movslq	(%rsi,%rcx,4), %rcx
	movslq	%edi, %rdi
	addq	%rcx, %rdi
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	leaq	(%rbx,%rbx,2), %rcx
	movl	8(%rsi,%rcx,4), %edi
	imull	%edx, %edi
	addl	4(%rsi,%rcx,4), %edi
	imull	%eax, %edi
	movslq	(%rsi,%rcx,4), %rcx
	movslq	%edi, %rsi
	addq	%rcx, %rsi
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movl	28(%r11), %r15d
	movl	%r15d, %edi
	imull	%r13d, %edi
	movl	%r10d, 32(%rsp)         # 4-byte Spill
	movl	%r9d, 48(%rsp)          # 4-byte Spill
	movl	%ebp, %r9d
	imull	%r15d, %r9d
	imull	%eax, %r15d
	movslq	24(%r11), %r11
	movl	%r11d, %esi
	negl	%esi
	imull	64(%rsp), %esi          # 4-byte Folded Reload
	movl	%r12d, 80(%rsp)         # 4-byte Spill
	leal	(%rsi,%rdi), %r10d
	leal	(%rsi,%r15), %r12d
	addl	%r9d, %esi
	movl	304(%rsp), %ebx         # 4-byte Reload
	imull	%ebx, %r10d
	addl	%edi, %r10d
	movl	%r11d, %ecx
	imull	64(%rsp), %ecx          # 4-byte Folded Reload
	subl	%ecx, %r10d
	movl	%r10d, 416(%rsp)        # 4-byte Spill
	imull	%ebx, %r12d
	addl	%r15d, %r12d
	subl	%ecx, %r12d
	movl	%r12d, 512(%rsp)        # 4-byte Spill
	imull	%ebx, %esi
	addl	%r9d, %esi
	subl	%ecx, %esi
	movq	%rsi, 424(%rsp)         # 8-byte Spill
	movq	168(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, %esi
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	imull	%edi, %esi
	movl	%esi, 504(%rsp)         # 4-byte Spill
	subl	%esi, 392(%rsp)         # 4-byte Folded Spill
	movl	%ecx, %esi
	movq	%r15, 56(%rsp)          # 8-byte Spill
	imull	%r15d, %esi
	movl	%esi, 600(%rsp)         # 4-byte Spill
	subl	%esi, 328(%rsp)         # 4-byte Folded Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r9d, 352(%rsp)         # 4-byte Spill
	imull	%r9d, %ecx
	movl	%ecx, 592(%rsp)         # 4-byte Spill
	subl	%ecx, 288(%rsp)         # 4-byte Folded Spill
	cmpq	$0, 400(%rsp)           # 8-byte Folded Reload
	setne	256(%rsp)               # 1-byte Folded Spill
	movq	456(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx), %r10d
	movl	%r10d, %r15d
	subl	48(%rsp), %r15d         # 4-byte Folded Reload
	movq	480(%rsp), %rsi         # 8-byte Reload
	movl	(%rsi), %r12d
	movl	%r12d, %r9d
	subl	24(%rsp), %r9d          # 4-byte Folded Reload
	movl	4(%rsi), %ebx
	movl	%ebx, %ecx
	movq	272(%rsp), %rsi         # 8-byte Reload
	subl	4(%rsi), %ecx
	imull	%r8d, %ecx
	addl	%r9d, %ecx
	imull	%ebp, %ecx
	addl	%r15d, %ecx
	movl	%ecx, 216(%rsp)         # 4-byte Spill
	movl	%r10d, %ecx
	subl	8(%rsp), %ecx           # 4-byte Folded Reload
	movl	%r12d, %ebp
	subl	16(%rsp), %ebp          # 4-byte Folded Reload
	movl	%ebx, %edi
	movq	320(%rsp), %rsi         # 8-byte Reload
	subl	-8(%rsi), %edi
	imull	%edx, %edi
	addl	%ebp, %edi
	imull	%eax, %edi
	addl	%ecx, %edi
	movl	%edi, 112(%rsp)         # 4-byte Spill
	subl	32(%rsp), %r10d         # 4-byte Folded Reload
	subl	80(%rsp), %r12d         # 4-byte Folded Reload
	movq	312(%rsp), %rax         # 8-byte Reload
	subl	4(%rax), %ebx
	imull	%r14d, %ebx
	addl	%r12d, %ebx
	imull	%r13d, %ebx
	addl	%r10d, %ebx
	movl	%ebx, 176(%rsp)         # 4-byte Spill
	movq	408(%rsp), %rsi         # 8-byte Reload
	movq	64(%rsi), %rax
	movq	448(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	280(%rsp), %rdi         # 8-byte Reload
	movslq	(%rax,%rdi,4), %rcx
	movslq	4(%rax,%rdi,4), %rdx
	movslq	8(%rax,%rdi,4), %rax
	movq	48(%rsi), %rsi
	leaq	(%rsi,%rcx,8), %r14
	leaq	(%rsi,%rdx,8), %rbp
	leaq	(%rsi,%rax,8), %rdi
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movq	528(%rsp), %rdi         # 8-byte Reload
	leaq	(%rsi,%rdi,8), %rsi
	leaq	8(%rsi,%rax,8), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	leaq	8(%rsi,%rdx,8), %rax
	movq	%rax, 336(%rsp)         # 8-byte Spill
	leaq	8(%rsi,%rcx,8), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	cmpl	$1, %r11d
	sete	%al
	andb	256(%rsp), %al          # 1-byte Folded Reload
	movb	%al, 256(%rsp)          # 1-byte Spill
	movq	%r11, %rax
	imulq	400(%rsp), %rax         # 8-byte Folded Reload
	movq	%rax, 232(%rsp)         # 8-byte Spill
	movq	%r11, %rax
	shlq	$4, %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	shlq	$3, %r11
	movl	$0, 264(%rsp)           # 4-byte Folded Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_102:                              # %.preheader3678.us
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB2_105 Depth 6
                                        #               Child Loop BB2_114 Depth 7
                                        #               Child Loop BB2_119 Depth 7
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	movq	424(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	512(%rsp), %ecx         # 4-byte Reload
	movl	416(%rsp), %edx         # 4-byte Reload
	jle	.LBB2_122
# BB#103:                               # %.preheader3670.us.us.preheader
                                        #   in Loop: Header=BB2_102 Depth=5
	xorl	%edx, %edx
	movl	216(%rsp), %eax         # 4-byte Reload
	movl	112(%rsp), %ecx         # 4-byte Reload
	movl	176(%rsp), %r13d        # 4-byte Reload
	jmp	.LBB2_105
.LBB2_104:                              #   in Loop: Header=BB2_105 Depth=6
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	120(%rsp), %r9          # 8-byte Reload
	jmp	.LBB2_117
	.p2align	4, 0x90
.LBB2_105:                              # %.preheader3670.us.us
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        #           Parent Loop BB2_102 Depth=5
                                        # =>          This Loop Header: Depth=6
                                        #               Child Loop BB2_114 Depth 7
                                        #               Child Loop BB2_119 Depth 7
	movl	%r13d, 16(%rsp)         # 4-byte Spill
	movslq	%r13d, %r13
	movslq	%ecx, %r9
	movslq	%eax, %rsi
	cmpq	$1, 344(%rsp)           # 8-byte Folded Reload
	movl	%edx, 32(%rsp)          # 4-byte Spill
	movl	%eax, 80(%rsp)          # 4-byte Spill
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	jbe	.LBB2_116
# BB#106:                               # %min.iters.checked4780
                                        #   in Loop: Header=BB2_105 Depth=6
	cmpb	$0, 256(%rsp)           # 1-byte Folded Reload
	je	.LBB2_116
# BB#107:                               # %vector.memcheck4868
                                        #   in Loop: Header=BB2_105 Depth=6
	movq	%rsi, 184(%rsp)         # 8-byte Spill
	movq	%r9, 120(%rsp)          # 8-byte Spill
	movq	88(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%edx, %eax
	addl	176(%rsp), %eax         # 4-byte Folded Reload
	movslq	%eax, %r10
	movq	56(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	imull	%edx, %ecx
	addl	112(%rsp), %ecx         # 4-byte Folded Reload
	movslq	%ecx, %r9
	movl	352(%rsp), %ecx         # 4-byte Reload
	imull	%edx, %ecx
	addl	216(%rsp), %ecx         # 4-byte Folded Reload
	movslq	%ecx, %rax
	movq	128(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	568(%rsp), %rsi         # 8-byte Reload
	movq	%rax, 296(%rsp)         # 8-byte Spill
	leaq	(%rsi,%rax,8), %rbx
	movq	96(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r10,8), %r8
	movq	152(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r10,8), %rax
	leaq	(%rbp,%r10,8), %rdx
	movq	%rdx, 224(%rsp)         # 8-byte Spill
	movq	336(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r10,8), %r12
	leaq	(%r14,%r10,8), %rdx
	movq	%rdx, 192(%rsp)         # 8-byte Spill
	movq	200(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r10,8), %r14
	cmpq	%rax, %rcx
	movq	72(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r9), %rbp
	sbbb	%sil, %sil
	cmpq	%rbx, %r8
	sbbb	%r15b, %r15b
	andb	%sil, %r15b
	movq	376(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rbp,8), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movq	560(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbp,8), %rbp
	cmpq	%r12, %rcx
	sbbb	%r12b, %r12b
	cmpq	%rbx, 224(%rsp)         # 8-byte Folded Reload
	movq	136(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%r9), %rsi
	sbbb	%dl, %dl
	movb	%dl, 224(%rsp)          # 1-byte Spill
	cmpq	%r14, %rcx
	sbbb	%r14b, %r14b
	cmpq	%rbx, 192(%rsp)         # 8-byte Folded Reload
	leaq	(%rdi,%rsi,8), %rdx
	movq	%rdx, 192(%rsp)         # 8-byte Spill
	leaq	(%rax,%rsi,8), %r8
	sbbb	%dl, %dl
	movb	%dl, 240(%rsp)          # 1-byte Spill
	cmpq	%rbp, %rcx
	sbbb	%bpl, %bpl
	cmpq	%rbx, 248(%rsp)         # 8-byte Folded Reload
	movq	104(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%r9), %rsi
	sbbb	%dl, %dl
	cmpq	%r8, %rcx
	sbbb	%r8b, %r8b
	cmpq	%rbx, 192(%rsp)         # 8-byte Folded Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	sbbb	%al, %al
	cmpq	192(%rsp), %rcx         # 8-byte Folded Reload
	leaq	(%rdi,%rsi,8), %rsi
	sbbb	%cl, %cl
	cmpq	%rbx, %rsi
	sbbb	%bl, %bl
	testb	$1, %r15b
	jne	.LBB2_104
# BB#108:                               # %vector.memcheck4868
                                        #   in Loop: Header=BB2_105 Depth=6
	andb	224(%rsp), %r12b        # 1-byte Folded Reload
	andb	$1, %r12b
	jne	.LBB2_104
# BB#109:                               # %vector.memcheck4868
                                        #   in Loop: Header=BB2_105 Depth=6
	andb	240(%rsp), %r14b        # 1-byte Folded Reload
	andb	$1, %r14b
	jne	.LBB2_104
# BB#110:                               # %vector.memcheck4868
                                        #   in Loop: Header=BB2_105 Depth=6
	andb	%dl, %bpl
	andb	$1, %bpl
	jne	.LBB2_104
# BB#111:                               # %vector.memcheck4868
                                        #   in Loop: Header=BB2_105 Depth=6
	andb	%al, %r8b
	andb	$1, %r8b
	jne	.LBB2_104
# BB#112:                               # %vector.memcheck4868
                                        #   in Loop: Header=BB2_105 Depth=6
	andb	%bl, %cl
	andb	$1, %cl
	jne	.LBB2_104
# BB#113:                               # %vector.body4776.preheader
                                        #   in Loop: Header=BB2_105 Depth=6
	shlq	$3, %r13
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	120(%rsp), %rdx         # 8-byte Reload
	leaq	(%rax,%rdx), %rcx
	movq	376(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %r8
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx), %rcx
	leaq	(%rsi,%rcx,8), %r12
	addq	104(%rsp), %rdx         # 8-byte Folded Reload
	leaq	(%rsi,%rdx,8), %r14
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	184(%rsp), %rax         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rbx
	movq	232(%rsp), %rax         # 8-byte Reload
	addq	%rax, 296(%rsp)         # 8-byte Folded Spill
	addq	%rax, %r9
	addq	%rax, %r10
	movq	400(%rsp), %rcx         # 8-byte Reload
	xorl	%esi, %esi
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	144(%rsp), %rax         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_114:                              # %vector.body4776
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        #           Parent Loop BB2_102 Depth=5
                                        #             Parent Loop BB2_105 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	leaq	(%r13,%rsi), %rbp
	movupd	(%r15,%rbp), %xmm0
	movupd	(%r14,%rsi), %xmm1
	mulpd	%xmm0, %xmm1
	movupd	(%rdx,%rbp), %xmm0
	movupd	(%r12,%rsi), %xmm2
	mulpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm2
	movupd	(%rdi,%rbp), %xmm0
	movupd	(%r8,%rsi), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm2, %xmm1
	movupd	(%rbx,%rsi), %xmm0
	addpd	%xmm1, %xmm0
	movupd	%xmm0, (%rbx,%rsi)
	addq	%rax, %rsi
	addq	$-2, %rcx
	jne	.LBB2_114
# BB#115:                               # %middle.block4777
                                        #   in Loop: Header=BB2_105 Depth=6
	movq	400(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, 344(%rsp)         # 8-byte Folded Reload
	movl	%ecx, %r8d
	movq	160(%rsp), %r15         # 8-byte Reload
	movl	16(%rsp), %r13d         # 4-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	jne	.LBB2_118
	jmp	.LBB2_120
	.p2align	4, 0x90
.LBB2_116:                              #   in Loop: Header=BB2_105 Depth=6
	movq	%rsi, %rax
.LBB2_117:                              # %scalar.ph4778.preheader
                                        #   in Loop: Header=BB2_105 Depth=6
	movq	%r13, %r10
	xorl	%r8d, %r8d
	movq	160(%rsp), %r15         # 8-byte Reload
	movl	16(%rsp), %r13d         # 4-byte Reload
.LBB2_118:                              # %scalar.ph4778.preheader
                                        #   in Loop: Header=BB2_105 Depth=6
	movq	128(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rdx
	movq	72(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r9), %rcx
	movq	520(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %r12
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r9), %rcx
	leaq	(%rsi,%rcx,8), %r14
	addq	104(%rsp), %r9          # 8-byte Folded Reload
	leaq	(%rsi,%r9,8), %r9
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%rax,%r10,8), %rbp
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r10,8), %rsi
	movq	96(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r10,8), %rax
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %edi
	subl	%r8d, %edi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_119:                              # %scalar.ph4778
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        #           Parent Loop BB2_102 Depth=5
                                        #             Parent Loop BB2_105 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movsd	(%rbp,%rbx), %xmm0      # xmm0 = mem[0],zero
	leaq	(%r15,%rbx), %rcx
	mulsd	(%r9,%rcx), %xmm0
	movsd	(%rsi,%rbx), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%r14,%rcx), %xmm1
	addsd	%xmm0, %xmm1
	movsd	(%rax,%rbx), %xmm0      # xmm0 = mem[0],zero
	mulsd	(%r12,%rcx), %xmm0
	addsd	%xmm1, %xmm0
	addsd	(%rdx,%rbx), %xmm0
	movsd	%xmm0, (%rdx,%rbx)
	addq	%r11, %rbx
	decl	%edi
	jne	.LBB2_119
.LBB2_120:                              # %._crit_edge3992.us.us
                                        #   in Loop: Header=BB2_105 Depth=6
	addl	88(%rsp), %r13d         # 4-byte Folded Reload
	movl	48(%rsp), %ecx          # 4-byte Reload
	addl	56(%rsp), %ecx          # 4-byte Folded Reload
	movl	80(%rsp), %eax          # 4-byte Reload
	addl	352(%rsp), %eax         # 4-byte Folded Reload
	movl	32(%rsp), %edx          # 4-byte Reload
	incl	%edx
	cmpl	168(%rsp), %edx         # 4-byte Folded Reload
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	jne	.LBB2_105
# BB#121:                               #   in Loop: Header=BB2_102 Depth=5
	movl	592(%rsp), %eax         # 4-byte Reload
	movl	600(%rsp), %ecx         # 4-byte Reload
	movl	504(%rsp), %edx         # 4-byte Reload
.LBB2_122:                              # %._crit_edge4000.us
                                        #   in Loop: Header=BB2_102 Depth=5
	addl	176(%rsp), %edx         # 4-byte Folded Reload
	addl	112(%rsp), %ecx         # 4-byte Folded Reload
	addl	216(%rsp), %eax         # 4-byte Folded Reload
	addl	392(%rsp), %edx         # 4-byte Folded Reload
	addl	328(%rsp), %ecx         # 4-byte Folded Reload
	addl	288(%rsp), %eax         # 4-byte Folded Reload
	movl	264(%rsp), %esi         # 4-byte Reload
	incl	%esi
	movl	%esi, 264(%rsp)         # 4-byte Spill
	cmpl	212(%rsp), %esi         # 4-byte Folded Reload
	movl	%edx, 176(%rsp)         # 4-byte Spill
	movl	%ecx, 112(%rsp)         # 4-byte Spill
	movl	%eax, 216(%rsp)         # 4-byte Spill
	jne	.LBB2_102
	jmp	.LBB2_216
.LBB2_123:                              #   in Loop: Header=BB2_75 Depth=4
	movq	464(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r9d
	movq	272(%rsp), %rcx         # 8-byte Reload
	movl	8(%rcx), %eax
	movl	%eax, %edx
	subl	%r9d, %edx
	incl	%edx
	cmpl	%r9d, %eax
	movl	(%rcx), %r11d
	movl	12(%rcx), %eax
	movl	$0, %ebp
	cmovsl	%ebp, %edx
	movl	%eax, %ecx
	subl	%r11d, %ecx
	incl	%ecx
	cmpl	%r11d, %eax
	movq	496(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r15d
	movq	312(%rsp), %rax         # 8-byte Reload
	movl	8(%rax), %esi
	cmovsl	%ebp, %ecx
	movl	%esi, %r14d
	subl	%r15d, %r14d
	incl	%r14d
	cmpl	%r15d, %esi
	movl	(%rax), %r10d
	movl	12(%rax), %esi
	cmovsl	%ebp, %r14d
	movl	%esi, %r8d
	subl	%r10d, %r8d
	incl	%r8d
	cmpl	%r10d, %esi
	movq	320(%rsp), %rbx         # 8-byte Reload
	movl	-4(%rbx), %esi
	movq	488(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %eax
	cmovsl	%ebp, %r8d
	movl	%esi, %r12d
	subl	%eax, %r12d
	incl	%r12d
	cmpl	%eax, %esi
	movl	(%rbx), %esi
	movl	-12(%rbx), %edi
	cmovsl	%ebp, %r12d
	movl	%esi, %ebx
	subl	%edi, %ebx
	incl	%ebx
	cmpl	%edi, %esi
	cmovsl	%ebp, %ebx
	cmpl	$0, 308(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_216
# BB#124:                               # %.lr.ph4080
                                        #   in Loop: Header=BB2_75 Depth=4
	cmpb	$0, 47(%rsp)            # 1-byte Folded Reload
	jne	.LBB2_216
# BB#125:                               # %.preheader3677.us.preheader
                                        #   in Loop: Header=BB2_75 Depth=4
	movl	%r11d, 24(%rsp)         # 4-byte Spill
	movl	%edi, 8(%rsp)           # 4-byte Spill
	movq	384(%rsp), %r11         # 8-byte Reload
	movl	32(%r11), %esi
	movl	%edx, %edi
	imull	%ecx, %edi
	imull	%esi, %edi
	movl	%edi, 556(%rsp)         # 4-byte Spill
	movl	%r12d, %edi
	imull	%ebx, %edi
	imull	%esi, %edi
	movl	%edi, 552(%rsp)         # 4-byte Spill
	movl	%r14d, %edi
	imull	%r8d, %edi
	imull	%esi, %edi
	movl	%edi, 548(%rsp)         # 4-byte Spill
	movq	280(%rsp), %rdi         # 8-byte Reload
	leaq	3(%rdi), %rsi
	movq	%rdi, %r13
	leaq	(%rsi,%rsi,2), %rsi
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	472(%rsp), %rax         # 8-byte Reload
	movl	8(%rax,%rsi,4), %ebp
	imull	%ebx, %ebp
	addl	4(%rax,%rsi,4), %ebp
	imull	%r12d, %ebp
	movslq	(%rax,%rsi,4), %rsi
	movslq	%ebp, %rdi
	addq	%rsi, %rdi
	movq	%rdi, 328(%rsp)         # 8-byte Spill
	leaq	2(%r13), %rsi
	leaq	(%rsi,%rsi,2), %rsi
	movl	8(%rax,%rsi,4), %ebp
	imull	%ebx, %ebp
	addl	4(%rax,%rsi,4), %ebp
	imull	%r12d, %ebp
	movslq	(%rax,%rsi,4), %rsi
	movslq	%ebp, %rdi
	addq	%rsi, %rdi
	movq	%rdi, 392(%rsp)         # 8-byte Spill
	leaq	1(%r13), %rsi
	leaq	(%rsi,%rsi,2), %rsi
	movl	8(%rax,%rsi,4), %ebp
	imull	%ebx, %ebp
	addl	4(%rax,%rsi,4), %ebp
	imull	%r12d, %ebp
	movslq	(%rax,%rsi,4), %rsi
	movslq	%ebp, %rdi
	addq	%rsi, %rdi
	movq	%rdi, 424(%rsp)         # 8-byte Spill
	leaq	(%r13,%r13,2), %rsi
	movl	8(%rax,%rsi,4), %ebp
	imull	%ebx, %ebp
	addl	4(%rax,%rsi,4), %ebp
	imull	%r12d, %ebp
	movslq	(%rax,%rsi,4), %rsi
	movslq	%ebp, %rax
	addq	%rsi, %rax
	movq	%rax, 416(%rsp)         # 8-byte Spill
	movl	28(%r11), %r13d
	movl	%r15d, 32(%rsp)         # 4-byte Spill
	movl	%r13d, %edi
	imull	%r14d, %edi
	movl	%r10d, 80(%rsp)         # 4-byte Spill
	movl	%edx, %r10d
	imull	%r13d, %r10d
	imull	%r12d, %r13d
	movslq	24(%r11), %r11
	movl	%r11d, %eax
	negl	%eax
	imull	64(%rsp), %eax          # 4-byte Folded Reload
	movl	%r9d, 48(%rsp)          # 4-byte Spill
	leal	(%rax,%rdi), %r9d
	leal	(%rax,%r13), %r15d
	addl	%r10d, %eax
	movl	304(%rsp), %ebp         # 4-byte Reload
	imull	%ebp, %r9d
	addl	%edi, %r9d
	movl	%r11d, %esi
	imull	64(%rsp), %esi          # 4-byte Folded Reload
	subl	%esi, %r9d
	movl	%r9d, 640(%rsp)         # 4-byte Spill
	imull	%ebp, %r15d
	addl	%r13d, %r15d
	subl	%esi, %r15d
	movl	%r15d, 636(%rsp)        # 4-byte Spill
	imull	%ebp, %eax
	addl	%r10d, %eax
	subl	%esi, %eax
	movq	%rax, 720(%rsp)         # 8-byte Spill
	movq	168(%rsp), %rax         # 8-byte Reload
	movl	%eax, %esi
	movq	%rdi, 256(%rsp)         # 8-byte Spill
	imull	%edi, %esi
	movl	%esi, 632(%rsp)         # 4-byte Spill
	subl	%esi, 548(%rsp)         # 4-byte Folded Spill
	movl	%eax, %esi
	movq	%r13, 352(%rsp)         # 8-byte Spill
	imull	%r13d, %esi
	movl	%esi, 628(%rsp)         # 4-byte Spill
	subl	%esi, 552(%rsp)         # 4-byte Folded Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%r10d, 296(%rsp)        # 4-byte Spill
	imull	%r10d, %eax
	movl	%eax, 624(%rsp)         # 4-byte Spill
	subl	%eax, 556(%rsp)         # 4-byte Folded Spill
	movq	400(%rsp), %r13         # 8-byte Reload
	testq	%r13, %r13
	setne	72(%rsp)                # 1-byte Folded Spill
	movq	456(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r9d
	movl	%r9d, %ebp
	subl	48(%rsp), %ebp          # 4-byte Folded Reload
	movq	480(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r15d
	movl	%r15d, %r10d
	subl	24(%rsp), %r10d         # 4-byte Folded Reload
	movl	4(%rax), %edi
	movl	%edi, %esi
	movq	272(%rsp), %rax         # 8-byte Reload
	subl	4(%rax), %esi
	imull	%ecx, %esi
	addl	%r10d, %esi
	imull	%edx, %esi
	addl	%ebp, %esi
	movl	%esi, 144(%rsp)         # 4-byte Spill
	movl	%r9d, %ecx
	subl	16(%rsp), %ecx          # 4-byte Folded Reload
	movl	%r15d, %edx
	subl	8(%rsp), %edx           # 4-byte Folded Reload
	movl	%edi, %esi
	movq	320(%rsp), %rax         # 8-byte Reload
	subl	-8(%rax), %esi
	imull	%ebx, %esi
	addl	%edx, %esi
	imull	%r12d, %esi
	addl	%ecx, %esi
	movl	%esi, 288(%rsp)         # 4-byte Spill
	subl	32(%rsp), %r9d          # 4-byte Folded Reload
	subl	80(%rsp), %r15d         # 4-byte Folded Reload
	movq	312(%rsp), %rax         # 8-byte Reload
	subl	4(%rax), %edi
	imull	%r8d, %edi
	addl	%r15d, %edi
	imull	%r14d, %edi
	addl	%r9d, %edi
	movl	%edi, 264(%rsp)         # 4-byte Spill
	movq	408(%rsp), %rdi         # 8-byte Reload
	movq	64(%rdi), %rax
	movq	448(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	280(%rsp), %rbp         # 8-byte Reload
	movslq	(%rax,%rbp,4), %rcx
	movslq	4(%rax,%rbp,4), %rdx
	movslq	8(%rax,%rbp,4), %rsi
	movslq	12(%rax,%rbp,4), %rax
	movq	48(%rdi), %rdi
	leaq	(%rdi,%rax,8), %rbp
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	movq	528(%rsp), %rbp         # 8-byte Reload
	leaq	(%rdi,%rbp,8), %rbp
	leaq	8(%rbp,%rax,8), %rax
	movq	%rax, 512(%rsp)         # 8-byte Spill
	leaq	(%rdi,%rsi,8), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	8(%rbp,%rsi,8), %rax
	movq	%rax, 504(%rsp)         # 8-byte Spill
	leaq	(%rdi,%rdx,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	8(%rbp,%rdx,8), %rax
	movq	%rax, 600(%rsp)         # 8-byte Spill
	leaq	(%rdi,%rcx,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	8(%rbp,%rcx,8), %rax
	movq	%rax, 592(%rsp)         # 8-byte Spill
	cmpl	$1, %r11d
	sete	%al
	andb	72(%rsp), %al           # 1-byte Folded Reload
	movb	%al, 224(%rsp)          # 1-byte Spill
	movq	%r11, %rax
	imulq	%r13, %rax
	movq	%rax, 736(%rsp)         # 8-byte Spill
	movq	%r11, %rsi
	shlq	$4, %rsi
	shlq	$3, %r11
	movq	520(%rsp), %rax         # 8-byte Reload
	movq	328(%rsp), %rcx         # 8-byte Reload
	leaq	(%rax,%rcx,8), %r8
	movq	392(%rsp), %rcx         # 8-byte Reload
	leaq	(%rax,%rcx,8), %r13
	movq	424(%rsp), %rcx         # 8-byte Reload
	leaq	(%rax,%rcx,8), %r14
	movq	416(%rsp), %rcx         # 8-byte Reload
	leaq	(%rax,%rcx,8), %r15
	xorl	%edi, %edi
	movq	%rsi, 728(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_126:                              # %.preheader3677.us
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB2_132 Depth 6
                                        #               Child Loop BB2_143 Depth 7
                                        #               Child Loop BB2_148 Depth 7
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	movq	720(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	636(%rsp), %ecx         # 4-byte Reload
	movl	640(%rsp), %edx         # 4-byte Reload
	jle	.LBB2_151
# BB#127:                               # %.preheader3669.us.us.preheader
                                        #   in Loop: Header=BB2_126 Depth=5
	movl	%edi, 644(%rsp)         # 4-byte Spill
	xorl	%ecx, %ecx
	movl	144(%rsp), %eax         # 4-byte Reload
	movl	288(%rsp), %edx         # 4-byte Reload
	movl	264(%rsp), %esi         # 4-byte Reload
	movq	160(%rsp), %r12         # 8-byte Reload
	jmp	.LBB2_132
.LBB2_128:                              #   in Loop: Header=BB2_132 Depth=6
	movq	%r10, %rcx
	jmp	.LBB2_146
.LBB2_129:                              #   in Loop: Header=BB2_132 Depth=6
	movq	%r10, %rcx
	movq	%rbp, %rsi
.LBB2_130:                              # %scalar.ph4624.preheader
                                        #   in Loop: Header=BB2_132 Depth=6
	xorl	%edx, %edx
	movq	160(%rsp), %r12         # 8-byte Reload
	jmp	.LBB2_147
.LBB2_131:                              #   in Loop: Header=BB2_132 Depth=6
	movq	248(%rsp), %rcx         # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB2_130
	.p2align	4, 0x90
.LBB2_132:                              # %.preheader3669.us.us
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        #           Parent Loop BB2_126 Depth=5
                                        # =>          This Loop Header: Depth=6
                                        #               Child Loop BB2_143 Depth 7
                                        #               Child Loop BB2_148 Depth 7
	movslq	%esi, %rdi
	movslq	%edx, %rbp
	movslq	%eax, %rbx
	cmpq	$1, 344(%rsp)           # 8-byte Folded Reload
	movl	%ecx, 80(%rsp)          # 4-byte Spill
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movl	%edx, 72(%rsp)          # 4-byte Spill
	movl	%esi, 136(%rsp)         # 4-byte Spill
	jbe	.LBB2_145
# BB#133:                               # %min.iters.checked4626
                                        #   in Loop: Header=BB2_132 Depth=6
	cmpb	$0, 224(%rsp)           # 1-byte Folded Reload
	je	.LBB2_145
# BB#134:                               # %vector.memcheck
                                        #   in Loop: Header=BB2_132 Depth=6
	movq	%rbx, 248(%rsp)         # 8-byte Spill
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	256(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%ecx, %eax
	addl	264(%rsp), %eax         # 4-byte Folded Reload
	movslq	%eax, %rdi
	movq	352(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%ecx, %eax
	addl	288(%rsp), %eax         # 4-byte Folded Reload
	movslq	%eax, %rsi
	movl	296(%rsp), %eax         # 4-byte Reload
	imull	%ecx, %eax
	addl	144(%rsp), %eax         # 4-byte Folded Reload
	movslq	%eax, %rcx
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rbp
	movq	568(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	leaq	(%rax,%rcx,8), %rax
	movq	104(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %r10
	movq	512(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rdx
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	movq	504(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %r12
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movq	600(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %r9
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	592(%rsp), %rcx         # 8-byte Reload
	movq	%rdi, 152(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	cmpq	%rdx, %rbp
	movq	328(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi), %rdx
	sbbb	%cl, %cl
	cmpq	%rax, %r10
	sbbb	%bl, %bl
	andb	%cl, %bl
	movb	%bl, 16(%rsp)           # 1-byte Spill
	movq	376(%rsp), %r10         # 8-byte Reload
	leaq	(%r10,%rdx,8), %rcx
	movq	%rcx, 216(%rsp)         # 8-byte Spill
	movq	560(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rdx,8), %rcx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	cmpq	%r12, %rbp
	sbbb	%r12b, %r12b
	cmpq	%rax, 240(%rsp)         # 8-byte Folded Reload
	movq	392(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi), %rdx
	movq	%rsi, 192(%rsp)         # 8-byte Spill
	sbbb	%cl, %cl
	movb	%cl, 336(%rsp)          # 1-byte Spill
	cmpq	%r9, %rbp
	sbbb	%cl, %cl
	cmpq	%rax, 120(%rsp)         # 8-byte Folded Reload
	leaq	(%r10,%rdx,8), %rbx
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	leaq	(%rdi,%rdx,8), %rdx
	movq	%rdx, 240(%rsp)         # 8-byte Spill
	sbbb	%dl, %dl
	movb	%dl, 200(%rsp)          # 1-byte Spill
	cmpq	184(%rsp), %rbp         # 8-byte Folded Reload
	sbbb	%r9b, %r9b
	cmpq	%rax, 24(%rsp)          # 8-byte Folded Reload
	movq	424(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rsi), %rbx
	sbbb	%dl, %dl
	movb	%dl, 232(%rsp)          # 1-byte Spill
	cmpq	176(%rsp), %rbp         # 8-byte Folded Reload
	sbbb	%sil, %sil
	movb	%sil, 176(%rsp)         # 1-byte Spill
	cmpq	%rax, 216(%rsp)         # 8-byte Folded Reload
	leaq	(%r10,%rbx,8), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	leaq	(%rdi,%rbx,8), %rdx
	movq	%rdx, 184(%rsp)         # 8-byte Spill
	sbbb	%bl, %bl
	movb	%bl, 216(%rsp)          # 1-byte Spill
	cmpq	240(%rsp), %rbp         # 8-byte Folded Reload
	sbbb	%bl, %bl
	movb	%bl, 240(%rsp)          # 1-byte Spill
	cmpq	%rax, 120(%rsp)         # 8-byte Folded Reload
	movq	416(%rsp), %rsi         # 8-byte Reload
	movq	192(%rsp), %rdx         # 8-byte Reload
	leaq	(%rsi,%rdx), %rsi
	sbbb	%bl, %bl
	movb	%bl, 120(%rsp)          # 1-byte Spill
	cmpq	184(%rsp), %rbp         # 8-byte Folded Reload
	sbbb	%dl, %dl
	cmpq	%rax, 24(%rsp)          # 8-byte Folded Reload
	leaq	(%rdi,%rsi,8), %rbx
	sbbb	%dil, %dil
	cmpq	%rbx, %rbp
	leaq	(%r10,%rsi,8), %rbx
	sbbb	%sil, %sil
	cmpq	%rax, %rbx
	sbbb	%al, %al
	testb	$1, 16(%rsp)            # 1-byte Folded Reload
	jne	.LBB2_131
# BB#135:                               # %vector.memcheck
                                        #   in Loop: Header=BB2_132 Depth=6
	movl	%edi, %ebx
	andb	336(%rsp), %r12b        # 1-byte Folded Reload
	andb	$1, %r12b
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	248(%rsp), %r10         # 8-byte Reload
	jne	.LBB2_129
# BB#136:                               # %vector.memcheck
                                        #   in Loop: Header=BB2_132 Depth=6
	andb	200(%rsp), %cl          # 1-byte Folded Reload
	andb	$1, %cl
	jne	.LBB2_129
# BB#137:                               # %vector.memcheck
                                        #   in Loop: Header=BB2_132 Depth=6
	andb	232(%rsp), %r9b         # 1-byte Folded Reload
	andb	$1, %r9b
	jne	.LBB2_129
# BB#138:                               # %vector.memcheck
                                        #   in Loop: Header=BB2_132 Depth=6
	movb	176(%rsp), %cl          # 1-byte Reload
	andb	216(%rsp), %cl          # 1-byte Folded Reload
	andb	$1, %cl
	jne	.LBB2_129
# BB#139:                               # %vector.memcheck
                                        #   in Loop: Header=BB2_132 Depth=6
	movb	240(%rsp), %cl          # 1-byte Reload
	andb	120(%rsp), %cl          # 1-byte Folded Reload
	andb	$1, %cl
	jne	.LBB2_129
# BB#140:                               # %vector.memcheck
                                        #   in Loop: Header=BB2_132 Depth=6
	andb	%bl, %dl
	andb	$1, %dl
	movq	160(%rsp), %r12         # 8-byte Reload
	jne	.LBB2_128
# BB#141:                               # %vector.memcheck
                                        #   in Loop: Header=BB2_132 Depth=6
	andb	%al, %sil
	andb	$1, %sil
	jne	.LBB2_128
# BB#142:                               # %vector.body4622.preheader
                                        #   in Loop: Header=BB2_132 Depth=6
	shlq	$3, %rdi
	movq	328(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbp), %rax
	movq	376(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rax,8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	392(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbp), %rax
	leaq	(%rsi,%rax,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	424(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rbp), %rdx
	leaq	(%rsi,%rdx,8), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	addq	416(%rsp), %rbp         # 8-byte Folded Reload
	leaq	(%rsi,%rbp,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	128(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r10,8), %r9
	movq	736(%rsp), %rdx         # 8-byte Reload
	addq	%rdx, 112(%rsp)         # 8-byte Folded Spill
	addq	%rdx, 192(%rsp)         # 8-byte Folded Spill
	addq	%rdx, 152(%rsp)         # 8-byte Folded Spill
	movq	104(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi), %rsi
	movq	96(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rdi), %r10
	movq	56(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rdi), %rbx
	addq	88(%rsp), %rdi          # 8-byte Folded Reload
	movq	400(%rsp), %rdx         # 8-byte Reload
	movq	%rdi, %rax
	xorl	%edi, %edi
	movq	728(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_143:                              # %vector.body4622
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        #           Parent Loop BB2_126 Depth=5
                                        #             Parent Loop BB2_132 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movupd	(%rax,%rdi), %xmm0
	movq	32(%rsp), %rbp          # 8-byte Reload
	movupd	(%rbp,%rdi), %xmm1
	mulpd	%xmm0, %xmm1
	movupd	(%rbx,%rdi), %xmm0
	movq	16(%rsp), %rbp          # 8-byte Reload
	movupd	(%rbp,%rdi), %xmm2
	mulpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm2
	movupd	(%r10,%rdi), %xmm0
	movq	24(%rsp), %rbp          # 8-byte Reload
	movupd	(%rbp,%rdi), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm2, %xmm1
	movupd	(%rsi,%rdi), %xmm0
	movq	8(%rsp), %r12           # 8-byte Reload
	movupd	(%r12,%rdi), %xmm2
	mulpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm2
	movupd	(%r9,%rdi), %xmm0
	addpd	%xmm2, %xmm0
	movupd	%xmm0, (%r9,%rdi)
	addq	%rcx, %rdi
	addq	$-2, %rdx
	jne	.LBB2_143
# BB#144:                               # %middle.block4623
                                        #   in Loop: Header=BB2_132 Depth=6
	movq	400(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, 344(%rsp)         # 8-byte Folded Reload
	movl	%eax, %edx
	movq	160(%rsp), %r12         # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	192(%rsp), %rsi         # 8-byte Reload
	movq	152(%rsp), %rdi         # 8-byte Reload
	jne	.LBB2_147
	jmp	.LBB2_149
	.p2align	4, 0x90
.LBB2_145:                              #   in Loop: Header=BB2_132 Depth=6
	movq	%rbx, %rcx
.LBB2_146:                              # %scalar.ph4624.preheader
                                        #   in Loop: Header=BB2_132 Depth=6
	movq	%rbp, %rsi
	xorl	%edx, %edx
.LBB2_147:                              # %scalar.ph4624.preheader
                                        #   in Loop: Header=BB2_132 Depth=6
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	leaq	(%r12,%rsi,8), %r9
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %r10
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rbp
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rbx
	movq	104(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rdi
	movq	64(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%edx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_148:                              # %scalar.ph4624
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        #           Parent Loop BB2_126 Depth=5
                                        #             Parent Loop BB2_132 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movsd	(%r10,%rdx), %xmm0      # xmm0 = mem[0],zero
	leaq	(%r9,%rdx), %rsi
	mulsd	(%r15,%rsi), %xmm0
	movsd	(%rbp,%rdx), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%r14,%rsi), %xmm1
	addsd	%xmm0, %xmm1
	movsd	(%rbx,%rdx), %xmm0      # xmm0 = mem[0],zero
	mulsd	(%r13,%rsi), %xmm0
	addsd	%xmm1, %xmm0
	movsd	(%rdi,%rdx), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%r8,%rsi), %xmm1
	addsd	%xmm0, %xmm1
	addsd	(%rax,%rdx), %xmm1
	movsd	%xmm1, (%rax,%rdx)
	addq	%r11, %rdx
	decl	%ecx
	jne	.LBB2_148
.LBB2_149:                              # %._crit_edge4039.us.us
                                        #   in Loop: Header=BB2_132 Depth=6
	movl	136(%rsp), %esi         # 4-byte Reload
	addl	256(%rsp), %esi         # 4-byte Folded Reload
	movl	72(%rsp), %edx          # 4-byte Reload
	addl	352(%rsp), %edx         # 4-byte Folded Reload
	movl	48(%rsp), %eax          # 4-byte Reload
	addl	296(%rsp), %eax         # 4-byte Folded Reload
	movl	80(%rsp), %ecx          # 4-byte Reload
	incl	%ecx
	cmpl	168(%rsp), %ecx         # 4-byte Folded Reload
	jne	.LBB2_132
# BB#150:                               #   in Loop: Header=BB2_126 Depth=5
	movl	624(%rsp), %eax         # 4-byte Reload
	movl	628(%rsp), %ecx         # 4-byte Reload
	movl	632(%rsp), %edx         # 4-byte Reload
	movl	644(%rsp), %edi         # 4-byte Reload
.LBB2_151:                              # %._crit_edge4047.us
                                        #   in Loop: Header=BB2_126 Depth=5
	addl	264(%rsp), %edx         # 4-byte Folded Reload
	addl	288(%rsp), %ecx         # 4-byte Folded Reload
	addl	144(%rsp), %eax         # 4-byte Folded Reload
	addl	548(%rsp), %edx         # 4-byte Folded Reload
	addl	552(%rsp), %ecx         # 4-byte Folded Reload
	addl	556(%rsp), %eax         # 4-byte Folded Reload
	incl	%edi
	cmpl	212(%rsp), %edi         # 4-byte Folded Reload
	movl	%edx, 264(%rsp)         # 4-byte Spill
	movl	%ecx, 288(%rsp)         # 4-byte Spill
	movl	%eax, 144(%rsp)         # 4-byte Spill
	jne	.LBB2_126
	jmp	.LBB2_216
.LBB2_152:                              #   in Loop: Header=BB2_75 Depth=4
	movq	464(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r8d
	movq	272(%rsp), %rcx         # 8-byte Reload
	movl	8(%rcx), %eax
	movl	%eax, %esi
	subl	%r8d, %esi
	incl	%esi
	cmpl	%r8d, %eax
	movl	(%rcx), %r11d
	movl	12(%rcx), %eax
	movl	$0, %r9d
	cmovsl	%r9d, %esi
	movl	%eax, %r10d
	subl	%r11d, %r10d
	incl	%r10d
	cmpl	%r11d, %eax
	movq	496(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r14d
	movq	312(%rsp), %rcx         # 8-byte Reload
	movl	8(%rcx), %eax
	cmovsl	%r9d, %r10d
	movl	%eax, %edi
	subl	%r14d, %edi
	incl	%edi
	cmpl	%r14d, %eax
	movl	(%rcx), %r15d
	movl	12(%rcx), %ecx
	cmovsl	%r9d, %edi
	movl	%ecx, %ebp
	subl	%r15d, %ebp
	incl	%ebp
	cmpl	%r15d, %ecx
	movq	320(%rsp), %rbx         # 8-byte Reload
	movl	-4(%rbx), %edx
	movq	488(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r12d
	cmovsl	%r9d, %ebp
	movl	%edx, %ecx
	subl	%r12d, %ecx
	incl	%ecx
	cmpl	%r12d, %edx
	movl	(%rbx), %edx
	movl	-12(%rbx), %ebx
	cmovsl	%r9d, %ecx
	movl	%edx, %eax
	subl	%ebx, %eax
	incl	%eax
	cmpl	%ebx, %edx
	cmovsl	%r9d, %eax
	cmpl	$0, 308(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_216
# BB#153:                               # %.lr.ph4105
                                        #   in Loop: Header=BB2_75 Depth=4
	cmpl	$0, 212(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_216
# BB#154:                               # %.preheader3676.preheader
                                        #   in Loop: Header=BB2_75 Depth=4
	movq	280(%rsp), %rdx         # 8-byte Reload
	leaq	3(%rdx), %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movl	%esi, 112(%rsp)         # 4-byte Spill
	movl	%edi, 152(%rsp)         # 4-byte Spill
	movq	456(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx), %esi
	movl	%esi, %edx
	movl	%esi, %r9d
	subl	%r14d, %edx
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movq	480(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx), %r14d
	movl	4(%rdx), %edi
	movl	%r14d, %edx
	subl	%r15d, %edx
	movl	%edi, %r13d
	movq	312(%rsp), %rsi         # 8-byte Reload
	subl	4(%rsi), %r13d
	imull	%ebp, %r13d
	addl	%edx, %r13d
	movl	%r9d, %edx
	subl	%r12d, %edx
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movl	%r14d, %edx
	subl	%ebx, %edx
	movl	%edi, %r15d
	movq	320(%rsp), %rsi         # 8-byte Reload
	subl	-8(%rsi), %r15d
	imull	%eax, %r15d
	addl	%edx, %r15d
	movq	280(%rsp), %rdx         # 8-byte Reload
	leaq	4(%rdx), %rdx
	imull	152(%rsp), %r13d        # 4-byte Folded Reload
	subl	%r8d, %r9d
	movl	%r9d, 16(%rsp)          # 4-byte Spill
	subl	%r11d, %r14d
	movq	272(%rsp), %rsi         # 8-byte Reload
	subl	4(%rsi), %edi
	movl	%edi, %r9d
	imull	%r10d, %r9d
	addl	%r14d, %r9d
	imull	112(%rsp), %r9d         # 4-byte Folded Reload
	movq	384(%rsp), %rbx         # 8-byte Reload
	movl	28(%rbx), %r8d
	movl	32(%rbx), %esi
	movl	112(%rsp), %r14d        # 4-byte Reload
	movl	112(%rsp), %edi         # 4-byte Reload
	imull	%r10d, %edi
	movl	%edi, 112(%rsp)         # 4-byte Spill
	movl	%r8d, %r10d
	imull	152(%rsp), %r10d        # 4-byte Folded Reload
	movl	112(%rsp), %edi         # 4-byte Reload
	imull	%esi, %edi
	movl	%edi, 112(%rsp)         # 4-byte Spill
	movl	152(%rsp), %edi         # 4-byte Reload
	imull	%ebp, %edi
	movl	%edi, 152(%rsp)         # 4-byte Spill
	movl	%ecx, %r12d
	imull	%eax, %r12d
	imull	%esi, %r12d
	movl	152(%rsp), %edi         # 4-byte Reload
	imull	%esi, %edi
	movl	%edi, 152(%rsp)         # 4-byte Spill
	leaq	(%rdx,%rdx,2), %rdx
	movq	472(%rsp), %rdi         # 8-byte Reload
	movl	8(%rdi,%rdx,4), %esi
	imull	%eax, %esi
	addl	4(%rdi,%rdx,4), %esi
	imull	%ecx, %esi
	movslq	(%rdi,%rdx,4), %rdx
	movslq	%esi, %rsi
	addq	%rdx, %rsi
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rdx,2), %rdx
	movl	8(%rdi,%rdx,4), %esi
	imull	%eax, %esi
	addl	4(%rdi,%rdx,4), %esi
	imull	%ecx, %esi
	movslq	(%rdi,%rdx,4), %rdx
	movslq	%esi, %rsi
	addq	%rdx, %rsi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	280(%rsp), %rbp         # 8-byte Reload
	leaq	2(%rbp), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	movl	8(%rdi,%rdx,4), %esi
	imull	%eax, %esi
	addl	4(%rdi,%rdx,4), %esi
	imull	%ecx, %esi
	movslq	(%rdi,%rdx,4), %rdx
	movslq	%esi, %rsi
	addq	%rdx, %rsi
	movq	%rsi, 224(%rsp)         # 8-byte Spill
	leaq	1(%rbp), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	movl	8(%rdi,%rdx,4), %esi
	imull	%eax, %esi
	addl	4(%rdi,%rdx,4), %esi
	imull	%ecx, %esi
	movslq	(%rdi,%rdx,4), %rdx
	movslq	%esi, %rsi
	addq	%rdx, %rsi
	movq	%rsi, 248(%rsp)         # 8-byte Spill
	imull	%ecx, %r15d
	imull	%r8d, %r14d
	imull	%ecx, %r8d
	leaq	(%rbp,%rbp,2), %rdx
	imull	8(%rdi,%rdx,4), %eax
	addl	4(%rdi,%rdx,4), %eax
	imull	%ecx, %eax
	movslq	(%rdi,%rdx,4), %rcx
	cltq
	addq	%rcx, %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movslq	24(%rbx), %r11
	movl	%r11d, %esi
	negl	%esi
	movq	64(%rsp), %rax          # 8-byte Reload
	imull	%eax, %esi
	leal	(%rsi,%r10), %edi
	leal	(%rsi,%r8), %ebx
	addl	%r14d, %esi
	movl	304(%rsp), %ecx         # 4-byte Reload
	imull	%ecx, %edi
	addl	%r10d, %edi
	movl	%r11d, %edx
	imull	%eax, %edx
	subl	%edx, %edi
	movl	%edi, 288(%rsp)         # 4-byte Spill
	imull	%ecx, %ebx
	addl	%r8d, %ebx
	subl	%edx, %ebx
	movl	%ebx, 328(%rsp)         # 4-byte Spill
	imull	%ecx, %esi
	addl	%r14d, %esi
	subl	%edx, %esi
	movq	%rsi, 264(%rsp)         # 8-byte Spill
	addl	16(%rsp), %r9d          # 4-byte Folded Reload
	addl	24(%rsp), %r15d         # 4-byte Folded Reload
	addl	8(%rsp), %r13d          # 4-byte Folded Reload
	movq	408(%rsp), %rax         # 8-byte Reload
	movq	64(%rax), %rdx
	movq	448(%rsp), %rcx         # 8-byte Reload
	movq	(%rdx,%rcx,8), %rdx
	movq	%rbp, %rcx
	movslq	(%rdx,%rcx,4), %rsi
	movslq	4(%rdx,%rcx,4), %rdi
	movslq	8(%rdx,%rcx,4), %rbx
	movslq	12(%rdx,%rcx,4), %rbp
	movslq	16(%rdx,%rcx,4), %rdx
	movq	48(%rax), %rax
	leaq	(%rax,%rsi,8), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movl	%r13d, %esi
	leaq	(%rax,%rdi,8), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%r15, %rdi
	leaq	(%rax,%rbx,8), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movl	%r12d, %ebx
	leaq	(%rax,%rbp,8), %rcx
	movq	%rcx, 352(%rsp)         # 8-byte Spill
	leaq	(%rax,%rdx,8), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movl	152(%rsp), %edx         # 4-byte Reload
	movl	112(%rsp), %ecx         # 4-byte Reload
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	%rax, %r15
	movl	%r15d, %ebp
	movq	%r10, 136(%rsp)         # 8-byte Spill
	imull	%r10d, %ebp
	movl	%ebp, 200(%rsp)         # 4-byte Spill
	subl	%ebp, %edx
	movq	%r8, 48(%rsp)           # 8-byte Spill
	imull	%r8d, %eax
	movq	%rax, 232(%rsp)         # 8-byte Spill
	subl	%eax, %ebx
	movl	%r15d, %eax
	movl	%r14d, 72(%rsp)         # 4-byte Spill
	imull	%r14d, %eax
	movl	%eax, 144(%rsp)         # 4-byte Spill
	subl	%eax, %ecx
	shlq	$3, %r11
	movq	520(%rsp), %rax         # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	leaq	(%rax,%rbp,8), %r14
	movq	80(%rsp), %rbp          # 8-byte Reload
	leaq	(%rax,%rbp,8), %r8
	movq	224(%rsp), %rbp         # 8-byte Reload
	leaq	(%rax,%rbp,8), %r10
	movq	248(%rsp), %rbp         # 8-byte Reload
	leaq	(%rax,%rbp,8), %r15
	movq	120(%rsp), %rbp         # 8-byte Reload
	leaq	(%rax,%rbp,8), %r12
	movl	%r9d, %eax
	xorl	%ebp, %ebp
	movl	%ecx, 112(%rsp)         # 4-byte Spill
	movl	%edx, 152(%rsp)         # 4-byte Spill
	movl	%ebx, 336(%rsp)         # 4-byte Spill
	movq	%r11, 104(%rsp)         # 8-byte Spill
	movq	%r14, 296(%rsp)         # 8-byte Spill
	movq	%r8, 192(%rsp)          # 8-byte Spill
	movq	%r10, 224(%rsp)         # 8-byte Spill
	movq	%r15, 248(%rsp)         # 8-byte Spill
	movq	%r12, 120(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_155:                              # %.preheader3676
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB2_158 Depth 6
                                        #               Child Loop BB2_159 Depth 7
	cmpl	$0, 168(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_164
# BB#156:                               # %.preheader3668.lr.ph
                                        #   in Loop: Header=BB2_155 Depth=5
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_163
# BB#157:                               # %.preheader3668.us.preheader
                                        #   in Loop: Header=BB2_155 Depth=5
	movl	%ebp, 216(%rsp)         # 4-byte Spill
	movq	232(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%rdi), %ecx
	movl	%ecx, 176(%rsp)         # 4-byte Spill
	xorl	%ecx, %ecx
	movl	%esi, 240(%rsp)         # 4-byte Spill
	movl	%eax, 184(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB2_158:                              # %.preheader3668.us
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        #           Parent Loop BB2_155 Depth=5
                                        # =>          This Loop Header: Depth=6
                                        #               Child Loop BB2_159 Depth 7
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movl	%eax, 80(%rsp)          # 4-byte Spill
	cltq
	movq	128(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rdx
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movslq	%edi, %rax
	movl	%esi, 32(%rsp)          # 4-byte Spill
	movslq	%esi, %rdi
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %r14
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %r15
	movq	352(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %r11
	movq	256(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rbx
	movq	160(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %edi
	xorl	%ebp, %ebp
	movq	104(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r8          # 8-byte Reload
	movq	192(%rsp), %r10         # 8-byte Reload
	movq	224(%rsp), %r12         # 8-byte Reload
	movq	248(%rsp), %r13         # 8-byte Reload
	movq	120(%rsp), %rsi         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_159:                              #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        #           Parent Loop BB2_155 Depth=5
                                        #             Parent Loop BB2_158 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movq	8(%rsp), %rcx           # 8-byte Reload
	movsd	(%rcx,%rbp), %xmm0      # xmm0 = mem[0],zero
	leaq	(%rax,%rbp), %rcx
	mulsd	(%rsi,%rcx), %xmm0
	movsd	(%r14,%rbp), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%r13,%rcx), %xmm1
	movsd	(%r15,%rbp), %xmm2      # xmm2 = mem[0],zero
	mulsd	(%r12,%rcx), %xmm2
	movsd	(%r11,%rbp), %xmm3      # xmm3 = mem[0],zero
	mulsd	(%r10,%rcx), %xmm3
	movsd	(%rbx,%rbp), %xmm4      # xmm4 = mem[0],zero
	mulsd	(%r8,%rcx), %xmm4
	addsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm2
	addsd	%xmm2, %xmm3
	addsd	%xmm3, %xmm4
	addsd	(%rdx,%rbp), %xmm4
	movsd	%xmm4, (%rdx,%rbp)
	addq	%r9, %rbp
	decl	%edi
	jne	.LBB2_159
# BB#160:                               # %._crit_edge4086.us
                                        #   in Loop: Header=BB2_158 Depth=6
	movl	16(%rsp), %ecx          # 4-byte Reload
	incl	%ecx
	movl	80(%rsp), %eax          # 4-byte Reload
	addl	72(%rsp), %eax          # 4-byte Folded Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	addl	48(%rsp), %edi          # 4-byte Folded Reload
	movl	32(%rsp), %esi          # 4-byte Reload
	addl	136(%rsp), %esi         # 4-byte Folded Reload
	cmpl	168(%rsp), %ecx         # 4-byte Folded Reload
	jne	.LBB2_158
# BB#161:                               # %._crit_edge4094.loopexit
                                        #   in Loop: Header=BB2_155 Depth=5
	movl	240(%rsp), %esi         # 4-byte Reload
	addl	200(%rsp), %esi         # 4-byte Folded Reload
	movl	184(%rsp), %eax         # 4-byte Reload
	addl	144(%rsp), %eax         # 4-byte Folded Reload
	movl	112(%rsp), %ecx         # 4-byte Reload
	movl	152(%rsp), %edx         # 4-byte Reload
	movl	336(%rsp), %ebx         # 4-byte Reload
	movl	216(%rsp), %ebp         # 4-byte Reload
	movl	176(%rsp), %edi         # 4-byte Reload
	jmp	.LBB2_165
	.p2align	4, 0x90
.LBB2_163:                              # %.preheader3668.preheader
                                        #   in Loop: Header=BB2_155 Depth=5
	addl	328(%rsp), %edi         # 4-byte Folded Reload
	addl	288(%rsp), %esi         # 4-byte Folded Reload
	addl	264(%rsp), %eax         # 4-byte Folded Reload
.LBB2_164:                              # %._crit_edge4094
                                        #   in Loop: Header=BB2_155 Depth=5
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
.LBB2_165:                              # %._crit_edge4094
                                        #   in Loop: Header=BB2_155 Depth=5
	addl	%edx, %esi
	addl	%ebx, %edi
	addl	%ecx, %eax
	incl	%ebp
	cmpl	212(%rsp), %ebp         # 4-byte Folded Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	jne	.LBB2_155
	jmp	.LBB2_216
.LBB2_166:                              #   in Loop: Header=BB2_75 Depth=4
	movq	464(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r11d
	movq	272(%rsp), %rcx         # 8-byte Reload
	movl	8(%rcx), %eax
	movl	%eax, %edx
	subl	%r11d, %edx
	incl	%edx
	cmpl	%r11d, %eax
	movl	(%rcx), %r15d
	movl	12(%rcx), %eax
	movl	$0, %ecx
	cmovsl	%ecx, %edx
	movl	%eax, %ebp
	subl	%r15d, %ebp
	incl	%ebp
	cmpl	%r15d, %eax
	movq	496(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r8d
	movq	312(%rsp), %rsi         # 8-byte Reload
	movl	8(%rsi), %eax
	cmovsl	%ecx, %ebp
	movl	%eax, %r14d
	subl	%r8d, %r14d
	incl	%r14d
	cmpl	%r8d, %eax
	movl	(%rsi), %r10d
	movl	12(%rsi), %eax
	cmovsl	%ecx, %r14d
	movl	%eax, %r13d
	subl	%r10d, %r13d
	incl	%r13d
	cmpl	%r10d, %eax
	movq	320(%rsp), %rdi         # 8-byte Reload
	movl	-4(%rdi), %eax
	movq	488(%rsp), %rsi         # 8-byte Reload
	movl	(%rsi), %esi
	cmovsl	%ecx, %r13d
	movl	%eax, %ebx
	subl	%esi, %ebx
	incl	%ebx
	cmpl	%esi, %eax
	movl	(%rdi), %eax
	movl	-12(%rdi), %edi
	cmovsl	%ecx, %ebx
	movl	%eax, %r9d
	subl	%edi, %r9d
	incl	%r9d
	cmpl	%edi, %eax
	cmovsl	%ecx, %r9d
	cmpl	$0, 308(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_216
# BB#167:                               # %.lr.ph3986
                                        #   in Loop: Header=BB2_75 Depth=4
	cmpb	$0, 47(%rsp)            # 1-byte Folded Reload
	jne	.LBB2_216
# BB#168:                               # %.preheader3679.us.preheader
                                        #   in Loop: Header=BB2_75 Depth=4
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movq	384(%rsp), %r12         # 8-byte Reload
	movl	32(%r12), %eax
	movl	%edx, %ecx
	imull	%ebp, %ecx
	imull	%eax, %ecx
	movl	%ecx, 112(%rsp)         # 4-byte Spill
	movl	%ebx, %ecx
	imull	%r9d, %ecx
	imull	%eax, %ecx
	movl	%ecx, 152(%rsp)         # 4-byte Spill
	movl	%r14d, %ecx
	imull	%r13d, %ecx
	imull	%eax, %ecx
	movl	%ecx, 336(%rsp)         # 4-byte Spill
	movq	280(%rsp), %rcx         # 8-byte Reload
	leaq	1(%rcx), %rax
	movq	%rcx, %rsi
	leaq	(%rax,%rax,2), %rax
	movl	%edi, 32(%rsp)          # 4-byte Spill
	movq	472(%rsp), %rdi         # 8-byte Reload
	movl	8(%rdi,%rax,4), %ecx
	imull	%r9d, %ecx
	addl	4(%rdi,%rax,4), %ecx
	imull	%ebx, %ecx
	movslq	(%rdi,%rax,4), %rax
	movslq	%ecx, %rcx
	addq	%rax, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	leaq	(%rsi,%rsi,2), %rax
	movl	8(%rdi,%rax,4), %ecx
	imull	%r9d, %ecx
	addl	4(%rdi,%rax,4), %ecx
	imull	%ebx, %ecx
	movslq	(%rdi,%rax,4), %rax
	movslq	%ecx, %rcx
	addq	%rax, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	28(%r12), %edi
	movl	%edi, %ecx
	imull	%r14d, %ecx
	movl	%r15d, 104(%rsp)        # 4-byte Spill
	movl	%edx, %r15d
	imull	%edi, %r15d
	imull	%ebx, %edi
	movslq	24(%r12), %r12
	movl	%r12d, %esi
	negl	%esi
	imull	64(%rsp), %esi          # 4-byte Folded Reload
	movl	%r11d, 56(%rsp)         # 4-byte Spill
	leal	(%rsi,%rcx), %r11d
	movl	%r8d, 136(%rsp)         # 4-byte Spill
	leal	(%rsi,%rdi), %r8d
	addl	%r15d, %esi
	movl	%r10d, 96(%rsp)         # 4-byte Spill
	movl	304(%rsp), %r10d        # 4-byte Reload
	imull	%r10d, %r11d
	addl	%ecx, %r11d
	movl	%r12d, %eax
	imull	64(%rsp), %eax          # 4-byte Folded Reload
	subl	%eax, %r11d
	movl	%r11d, 232(%rsp)        # 4-byte Spill
	imull	%r10d, %r8d
	addl	%edi, %r8d
	subl	%eax, %r8d
	movl	%r8d, 144(%rsp)         # 4-byte Spill
	imull	%r10d, %esi
	addl	%r15d, %esi
	subl	%eax, %esi
	movq	%rsi, 200(%rsp)         # 8-byte Spill
	movq	168(%rsp), %rax         # 8-byte Reload
	movl	%eax, %esi
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	imull	%ecx, %esi
	movl	%esi, 264(%rsp)         # 4-byte Spill
	subl	%esi, 336(%rsp)         # 4-byte Folded Spill
	movl	%eax, %ecx
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	imull	%edi, %ecx
	movl	%ecx, 288(%rsp)         # 4-byte Spill
	subl	%ecx, 152(%rsp)         # 4-byte Folded Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%r15d, 72(%rsp)         # 4-byte Spill
	imull	%r15d, %eax
	movl	%eax, 328(%rsp)         # 4-byte Spill
	subl	%eax, 112(%rsp)         # 4-byte Folded Spill
	movq	400(%rsp), %r11         # 8-byte Reload
	testq	%r11, %r11
	setne	88(%rsp)                # 1-byte Folded Spill
	movq	456(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r15d
	movl	%r15d, %r10d
	subl	56(%rsp), %r10d         # 4-byte Folded Reload
	movq	480(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r8d
	movl	%r8d, %edi
	subl	104(%rsp), %edi         # 4-byte Folded Reload
	movl	4(%rax), %esi
	movl	%esi, %ecx
	movq	272(%rsp), %rax         # 8-byte Reload
	subl	4(%rax), %ecx
	imull	%ebp, %ecx
	addl	%edi, %ecx
	imull	%edx, %ecx
	addl	%r10d, %ecx
	movl	%ecx, 192(%rsp)         # 4-byte Spill
	movl	%r15d, %edx
	subl	16(%rsp), %edx          # 4-byte Folded Reload
	movl	%r8d, %eax
	subl	32(%rsp), %eax          # 4-byte Folded Reload
	movl	%esi, %ecx
	movq	320(%rsp), %rdi         # 8-byte Reload
	subl	-8(%rdi), %ecx
	imull	%r9d, %ecx
	addl	%eax, %ecx
	imull	%ebx, %ecx
	addl	%edx, %ecx
	movl	%ecx, 248(%rsp)         # 4-byte Spill
	subl	136(%rsp), %r15d        # 4-byte Folded Reload
	subl	96(%rsp), %r8d          # 4-byte Folded Reload
	movq	312(%rsp), %rdx         # 8-byte Reload
	subl	4(%rdx), %esi
	imull	%r13d, %esi
	addl	%r8d, %esi
	imull	%r14d, %esi
	addl	%r15d, %esi
	movl	%esi, 224(%rsp)         # 4-byte Spill
	movq	408(%rsp), %rdx         # 8-byte Reload
	movq	64(%rdx), %rax
	movq	448(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	280(%rsp), %rsi         # 8-byte Reload
	movslq	(%rax,%rsi,4), %rcx
	movslq	4(%rax,%rsi,4), %rax
	movq	48(%rdx), %rdx
	leaq	(%rdx,%rax,8), %rsi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	528(%rsp), %rsi         # 8-byte Reload
	leaq	(%rdx,%rsi,8), %rsi
	leaq	8(%rsi,%rax,8), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	8(%rsi,%rcx,8), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	cmpl	$1, %r12d
	sete	%al
	andb	88(%rsp), %al           # 1-byte Folded Reload
	movb	%al, 136(%rsp)          # 1-byte Spill
	movq	%r12, %rax
	imulq	%r11, %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	movq	%r12, %rax
	shlq	$4, %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	shlq	$3, %r12
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_169:                              # %.preheader3679.us
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB2_172 Depth 6
                                        #               Child Loop BB2_179 Depth 7
                                        #               Child Loop BB2_184 Depth 7
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	movq	200(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	144(%rsp), %ecx         # 4-byte Reload
	movl	232(%rsp), %edx         # 4-byte Reload
	jle	.LBB2_187
# BB#170:                               # %.preheader3671.us.us.preheader
                                        #   in Loop: Header=BB2_169 Depth=5
	movl	%esi, 176(%rsp)         # 4-byte Spill
	xorl	%r10d, %r10d
	movl	192(%rsp), %r9d         # 4-byte Reload
	movl	248(%rsp), %r8d         # 4-byte Reload
	movl	224(%rsp), %r13d        # 4-byte Reload
	movq	128(%rsp), %r15         # 8-byte Reload
	jmp	.LBB2_172
.LBB2_171:                              #   in Loop: Header=BB2_172 Depth=6
	movq	352(%rsp), %rax         # 8-byte Reload
	movq	88(%rsp), %r14          # 8-byte Reload
	movq	56(%rsp), %rdi          # 8-byte Reload
	xorl	%r11d, %r11d
	movq	128(%rsp), %r15         # 8-byte Reload
	jmp	.LBB2_183
	.p2align	4, 0x90
.LBB2_172:                              # %.preheader3671.us.us
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        #           Parent Loop BB2_169 Depth=5
                                        # =>          This Loop Header: Depth=6
                                        #               Child Loop BB2_179 Depth 7
                                        #               Child Loop BB2_184 Depth 7
	movslq	%r13d, %rdi
	movslq	%r8d, %r14
	movslq	%r9d, %rax
	cmpq	$1, 344(%rsp)           # 8-byte Folded Reload
	jbe	.LBB2_182
# BB#173:                               # %min.iters.checked4915
                                        #   in Loop: Header=BB2_172 Depth=6
	cmpb	$0, 136(%rsp)           # 1-byte Folded Reload
	je	.LBB2_182
# BB#174:                               # %vector.memcheck4979
                                        #   in Loop: Header=BB2_172 Depth=6
	movq	%rax, 352(%rsp)         # 8-byte Spill
	movq	%r14, 88(%rsp)          # 8-byte Spill
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movq	48(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r10d, %eax
	addl	224(%rsp), %eax         # 4-byte Folded Reload
	movslq	%eax, %rdi
	movq	80(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r10d, %eax
	addl	248(%rsp), %eax         # 4-byte Folded Reload
	movslq	%eax, %r14
	movl	72(%rsp), %eax          # 4-byte Reload
	imull	%r10d, %eax
	addl	192(%rsp), %eax         # 4-byte Folded Reload
	movslq	%eax, %rdx
	leaq	(%r15,%rdx,8), %rax
	movq	568(%rsp), %rcx         # 8-byte Reload
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rdx,8), %r15
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rbp
	movq	120(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rbx
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rsi
	movq	184(%rsp), %rcx         # 8-byte Reload
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 256(%rsp)         # 8-byte Spill
	cmpq	%rbx, %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%r14), %rbx
	sbbb	%dl, %dl
	cmpq	%r15, %rbp
	sbbb	%bpl, %bpl
	andb	%dl, %bpl
	movq	376(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx,8), %rdx
	movq	%rdx, 296(%rsp)         # 8-byte Spill
	movq	560(%rsp), %r11         # 8-byte Reload
	leaq	(%r11,%rbx,8), %rbx
	cmpq	256(%rsp), %rax         # 8-byte Folded Reload
	sbbb	%dl, %dl
	cmpq	%r15, %rsi
	movq	24(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%r14), %rsi
	sbbb	%dil, %dil
	movb	%dil, 256(%rsp)         # 1-byte Spill
	cmpq	%rbx, %rax
	sbbb	%bl, %bl
	cmpq	%r15, 296(%rsp)         # 8-byte Folded Reload
	leaq	(%r11,%rsi,8), %rdi
	sbbb	%r11b, %r11b
	cmpq	%rdi, %rax
	leaq	(%rcx,%rsi,8), %rsi
	sbbb	%al, %al
	cmpq	%r15, %rsi
	sbbb	%cl, %cl
	testb	$1, %bpl
	jne	.LBB2_171
# BB#175:                               # %vector.memcheck4979
                                        #   in Loop: Header=BB2_172 Depth=6
	andb	256(%rsp), %dl          # 1-byte Folded Reload
	andb	$1, %dl
	jne	.LBB2_171
# BB#176:                               # %vector.memcheck4979
                                        #   in Loop: Header=BB2_172 Depth=6
	andb	%r11b, %bl
	andb	$1, %bl
	movq	128(%rsp), %r15         # 8-byte Reload
	jne	.LBB2_181
# BB#177:                               # %vector.memcheck4979
                                        #   in Loop: Header=BB2_172 Depth=6
	andb	%cl, %al
	andb	$1, %al
	jne	.LBB2_181
# BB#178:                               # %vector.body4911.preheader
                                        #   in Loop: Header=BB2_172 Depth=6
	movq	%r15, %rdx
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,8), %r15
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx,8), %rbp
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	88(%rsp), %rbx          # 8-byte Reload
	leaq	(%rax,%rbx), %rax
	movq	376(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rsi
	addq	24(%rsp), %rbx          # 8-byte Folded Reload
	leaq	(%rcx,%rbx,8), %rcx
	movq	352(%rsp), %rax         # 8-byte Reload
	leaq	(%rdx,%rax,8), %rdx
	movq	240(%rsp), %rax         # 8-byte Reload
	addq	%rax, 96(%rsp)          # 8-byte Folded Spill
	addq	%rax, %r14
	addq	%rax, 104(%rsp)         # 8-byte Folded Spill
	movq	400(%rsp), %rbx         # 8-byte Reload
	xorl	%eax, %eax
	movq	216(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_179:                              # %vector.body4911
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        #           Parent Loop BB2_169 Depth=5
                                        #             Parent Loop BB2_172 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movupd	(%rbp,%rax), %xmm0
	movupd	(%rcx,%rax), %xmm1
	mulpd	%xmm0, %xmm1
	movupd	(%r15,%rax), %xmm0
	movupd	(%rsi,%rax), %xmm2
	mulpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm2
	movupd	(%rdx,%rax), %xmm0
	addpd	%xmm2, %xmm0
	movupd	%xmm0, (%rdx,%rax)
	addq	%rdi, %rax
	addq	$-2, %rbx
	jne	.LBB2_179
# BB#180:                               # %middle.block4912
                                        #   in Loop: Header=BB2_172 Depth=6
	movq	400(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, 344(%rsp)         # 8-byte Folded Reload
	movl	%eax, %r11d
	movq	128(%rsp), %r15         # 8-byte Reload
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	jne	.LBB2_183
	jmp	.LBB2_185
.LBB2_181:                              #   in Loop: Header=BB2_172 Depth=6
	movq	352(%rsp), %rax         # 8-byte Reload
	movq	88(%rsp), %r14          # 8-byte Reload
	movq	56(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_182:                              #   in Loop: Header=BB2_172 Depth=6
	xorl	%r11d, %r11d
.LBB2_183:                              # %scalar.ph4913.preheader
                                        #   in Loop: Header=BB2_172 Depth=6
	leaq	(%r15,%rax,8), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%r14), %rcx
	movq	376(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rcx
	addq	24(%rsp), %r14          # 8-byte Folded Reload
	leaq	(%rdx,%r14,8), %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdi,8), %rsi
	movq	16(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rdi,8), %rbp
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	%edi, %ebx
	subl	%r11d, %ebx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_184:                              # %scalar.ph4913
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        #           Parent Loop BB2_169 Depth=5
                                        #             Parent Loop BB2_172 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movsd	(%rsi,%rdi), %xmm0      # xmm0 = mem[0],zero
	mulsd	(%rdx,%rdi), %xmm0
	movsd	(%rbp,%rdi), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%rcx,%rdi), %xmm1
	addsd	%xmm0, %xmm1
	addsd	(%rax,%rdi), %xmm1
	movsd	%xmm1, (%rax,%rdi)
	addq	%r12, %rdi
	decl	%ebx
	jne	.LBB2_184
.LBB2_185:                              # %._crit_edge3945.us.us
                                        #   in Loop: Header=BB2_172 Depth=6
	addl	48(%rsp), %r13d         # 4-byte Folded Reload
	addl	80(%rsp), %r8d          # 4-byte Folded Reload
	addl	72(%rsp), %r9d          # 4-byte Folded Reload
	incl	%r10d
	cmpl	168(%rsp), %r10d        # 4-byte Folded Reload
	jne	.LBB2_172
# BB#186:                               #   in Loop: Header=BB2_169 Depth=5
	movl	328(%rsp), %eax         # 4-byte Reload
	movl	288(%rsp), %ecx         # 4-byte Reload
	movl	264(%rsp), %edx         # 4-byte Reload
	movl	176(%rsp), %esi         # 4-byte Reload
.LBB2_187:                              # %._crit_edge3953.us
                                        #   in Loop: Header=BB2_169 Depth=5
	addl	224(%rsp), %edx         # 4-byte Folded Reload
	addl	248(%rsp), %ecx         # 4-byte Folded Reload
	addl	192(%rsp), %eax         # 4-byte Folded Reload
	addl	336(%rsp), %edx         # 4-byte Folded Reload
	addl	152(%rsp), %ecx         # 4-byte Folded Reload
	addl	112(%rsp), %eax         # 4-byte Folded Reload
	incl	%esi
	cmpl	212(%rsp), %esi         # 4-byte Folded Reload
	movl	%edx, 224(%rsp)         # 4-byte Spill
	movl	%ecx, 248(%rsp)         # 4-byte Spill
	movl	%eax, 192(%rsp)         # 4-byte Spill
	jne	.LBB2_169
	jmp	.LBB2_216
.LBB2_188:                              #   in Loop: Header=BB2_75 Depth=4
	movq	464(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r8d
	movq	272(%rsp), %rcx         # 8-byte Reload
	movl	8(%rcx), %eax
	movl	%eax, %edi
	subl	%r8d, %edi
	incl	%edi
	cmpl	%r8d, %eax
	movl	(%rcx), %r10d
	movl	12(%rcx), %ecx
	movl	$0, %r9d
	cmovsl	%r9d, %edi
	movl	%ecx, %eax
	subl	%r10d, %eax
	incl	%eax
	cmpl	%r10d, %ecx
	movq	496(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx), %r15d
	movq	312(%rsp), %rdx         # 8-byte Reload
	movl	8(%rdx), %esi
	cmovsl	%r9d, %eax
	movl	%esi, %ecx
	subl	%r15d, %ecx
	incl	%ecx
	cmpl	%r15d, %esi
	movl	(%rdx), %r14d
	movl	12(%rdx), %edx
	cmovsl	%r9d, %ecx
	movl	%edx, %esi
	subl	%r14d, %esi
	incl	%esi
	cmpl	%r14d, %edx
	movq	320(%rsp), %rbx         # 8-byte Reload
	movl	-4(%rbx), %edx
	movq	488(%rsp), %rbp         # 8-byte Reload
	movl	(%rbp), %r12d
	cmovsl	%r9d, %esi
	movl	%edx, %r11d
	subl	%r12d, %r11d
	incl	%r11d
	cmpl	%r12d, %edx
	movl	(%rbx), %edx
	movl	-12(%rbx), %r13d
	cmovsl	%r9d, %r11d
	movl	%edx, %ebx
	subl	%r13d, %ebx
	incl	%ebx
	cmpl	%r13d, %edx
	cmovsl	%r9d, %ebx
	cmpl	$0, 308(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_216
# BB#189:                               # %.lr.ph4130
                                        #   in Loop: Header=BB2_75 Depth=4
	cmpl	$0, 212(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_216
# BB#190:                               # %.preheader3675.preheader
                                        #   in Loop: Header=BB2_75 Depth=4
	movq	280(%rsp), %rdx         # 8-byte Reload
	leaq	4(%rdx), %rbp
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	456(%rsp), %rdx         # 8-byte Reload
	movl	%edi, 200(%rsp)         # 4-byte Spill
	movl	%ecx, 232(%rsp)         # 4-byte Spill
	movl	(%rdx), %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	subl	%r15d, %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movq	480(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx), %r15d
	movl	4(%rdx), %r9d
	movl	%r15d, %edx
	subl	%r14d, %edx
	movl	%r9d, %edi
	movq	312(%rsp), %rcx         # 8-byte Reload
	subl	4(%rcx), %edi
	imull	%esi, %edi
	addl	%edx, %edi
	movl	%r8d, %ebp
	movl	16(%rsp), %r8d          # 4-byte Reload
	movl	%r8d, %ecx
	subl	%r12d, %ecx
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	movl	%r15d, %ecx
	subl	%r13d, %ecx
	movl	%r9d, %r14d
	movq	320(%rsp), %rdx         # 8-byte Reload
	subl	-8(%rdx), %r14d
	imull	%ebx, %r14d
	addl	%ecx, %r14d
	movq	280(%rsp), %r12         # 8-byte Reload
	leaq	5(%r12), %rcx
	imull	232(%rsp), %edi         # 4-byte Folded Reload
	movl	%edi, 176(%rsp)         # 4-byte Spill
	subl	%ebp, %r8d
	movl	%r8d, %r13d
	subl	%r10d, %r15d
	movq	272(%rsp), %rdx         # 8-byte Reload
	subl	4(%rdx), %r9d
	imull	%eax, %r9d
	addl	%r15d, %r9d
	imull	200(%rsp), %r9d         # 4-byte Folded Reload
	movq	384(%rsp), %rbp         # 8-byte Reload
	movl	28(%rbp), %r8d
	movl	32(%rbp), %edx
	movl	200(%rsp), %r10d        # 4-byte Reload
	movl	200(%rsp), %edi         # 4-byte Reload
	imull	%eax, %edi
	movl	%edi, 200(%rsp)         # 4-byte Spill
	movl	%r8d, %r15d
	imull	232(%rsp), %r15d        # 4-byte Folded Reload
	movl	200(%rsp), %eax         # 4-byte Reload
	imull	%edx, %eax
	movl	%eax, 200(%rsp)         # 4-byte Spill
	movl	232(%rsp), %eax         # 4-byte Reload
	imull	%esi, %eax
	movl	%eax, 232(%rsp)         # 4-byte Spill
	movl	%r11d, %eax
	imull	%ebx, %eax
	imull	%edx, %eax
	movl	%eax, 144(%rsp)         # 4-byte Spill
	movl	232(%rsp), %eax         # 4-byte Reload
	imull	%edx, %eax
	movl	%eax, 232(%rsp)         # 4-byte Spill
	leaq	(%rcx,%rcx,2), %rax
	movq	472(%rsp), %rsi         # 8-byte Reload
	movl	8(%rsi,%rax,4), %ecx
	imull	%ebx, %ecx
	addl	4(%rsi,%rax,4), %ecx
	imull	%r11d, %ecx
	movslq	(%rsi,%rax,4), %rax
	movslq	%ecx, %rcx
	addq	%rax, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rax,2), %rax
	movl	8(%rsi,%rax,4), %ecx
	imull	%ebx, %ecx
	addl	4(%rsi,%rax,4), %ecx
	imull	%r11d, %ecx
	movslq	(%rsi,%rax,4), %rax
	movslq	%ecx, %rcx
	addq	%rax, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	leaq	3(%r12), %rax
	leaq	(%rax,%rax,2), %rax
	movl	8(%rsi,%rax,4), %ecx
	imull	%ebx, %ecx
	addl	4(%rsi,%rax,4), %ecx
	imull	%r11d, %ecx
	movslq	(%rsi,%rax,4), %rax
	movslq	%ecx, %rcx
	addq	%rax, %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	leaq	2(%r12), %rax
	leaq	(%rax,%rax,2), %rax
	movl	8(%rsi,%rax,4), %ecx
	imull	%ebx, %ecx
	addl	4(%rsi,%rax,4), %ecx
	imull	%r11d, %ecx
	movslq	(%rsi,%rax,4), %rdx
	movslq	%ecx, %rax
	addq	%rdx, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	1(%r12), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movl	8(%rsi,%rcx,4), %edx
	imull	%ebx, %edx
	addl	4(%rsi,%rcx,4), %edx
	imull	%r11d, %edx
	movslq	(%rsi,%rcx,4), %rcx
	movslq	%edx, %rax
	addq	%rcx, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	imull	%r11d, %r14d
	movl	%r10d, %edx
	imull	%r8d, %edx
	imull	%r11d, %r8d
	leaq	(%r12,%r12,2), %rcx
	imull	8(%rsi,%rcx,4), %ebx
	addl	4(%rsi,%rcx,4), %ebx
	imull	%r11d, %ebx
	movslq	(%rsi,%rcx,4), %rcx
	movslq	%ebx, %r11
	addq	%rcx, %r11
	movslq	24(%rbp), %r10
	movl	%r10d, %esi
	negl	%esi
	movq	64(%rsp), %rax          # 8-byte Reload
	imull	%eax, %esi
	leal	(%rsi,%r15), %ebx
	leal	(%rsi,%r8), %ebp
	addl	%edx, %esi
	movl	%edx, %edi
	movl	%edi, 104(%rsp)         # 4-byte Spill
	movl	304(%rsp), %edx         # 4-byte Reload
	imull	%edx, %ebx
	addl	%r15d, %ebx
	movl	%r10d, %ecx
	imull	%eax, %ecx
	subl	%ecx, %ebx
	movl	%ebx, 424(%rsp)         # 4-byte Spill
	imull	%edx, %ebp
	addl	%r8d, %ebp
	subl	%ecx, %ebp
	movl	%ebp, 416(%rsp)         # 4-byte Spill
	imull	%edx, %esi
	addl	%edi, %esi
	subl	%ecx, %esi
	movq	%rsi, 392(%rsp)         # 8-byte Spill
	addl	%r13d, %r9d
	addl	24(%rsp), %r14d         # 4-byte Folded Reload
	movl	176(%rsp), %edi         # 4-byte Reload
	addl	8(%rsp), %edi           # 4-byte Folded Reload
	movq	408(%rsp), %rsi         # 8-byte Reload
	movq	64(%rsi), %rcx
	movq	448(%rsp), %rax         # 8-byte Reload
	movq	(%rcx,%rax,8), %r13
	movq	%r12, %rcx
	movslq	(%r13,%rcx,4), %rdx
	movslq	4(%r13,%rcx,4), %rbx
	movslq	8(%r13,%rcx,4), %rbp
	movslq	12(%r13,%rcx,4), %rax
	movslq	16(%r13,%rcx,4), %r12
	movslq	20(%r13,%rcx,4), %r13
	movq	48(%rsi), %rcx
	movl	%r9d, %esi
	leaq	(%rcx,%rdx,8), %rdx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rbx,8), %rdx
	movq	%rdx, 352(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rbp,8), %rdx
	movq	%rdx, 256(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	leaq	(%rcx,%r12,8), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	leaq	(%rcx,%r13,8), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movl	232(%rsp), %r12d        # 4-byte Reload
	movl	200(%rsp), %r9d         # 4-byte Reload
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rbp
	movl	%ebp, %ecx
	movq	%r15, 56(%rsp)          # 8-byte Spill
	imull	%r15d, %ecx
	movl	%ecx, 264(%rsp)         # 4-byte Spill
	subl	%ecx, %r12d
	movq	%r8, 96(%rsp)           # 8-byte Spill
	imull	%r8d, %eax
	movl	144(%rsp), %ecx         # 4-byte Reload
	movq	%r14, %rbx
	movq	%rax, 288(%rsp)         # 8-byte Spill
	subl	%eax, %ecx
	movl	%ebp, %eax
	imull	104(%rsp), %eax         # 4-byte Folded Reload
	movq	%r10, %rbp
	movl	%eax, 328(%rsp)         # 4-byte Spill
	subl	%eax, %r9d
	shlq	$3, %rbp
	movq	520(%rsp), %r8          # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%r8,%rax,8), %rax
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	(%r8,%rdx,8), %r10
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%r8,%rdx,8), %r14
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	(%r8,%rdx,8), %r15
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	(%r8,%rdx,8), %r13
	leaq	(%r8,%r11,8), %r8
	xorl	%edx, %edx
	movl	%r9d, 200(%rsp)         # 4-byte Spill
	movl	%r12d, 232(%rsp)        # 4-byte Spill
	movl	%ecx, 144(%rsp)         # 4-byte Spill
	movq	%r10, 248(%rsp)         # 8-byte Spill
	movq	%r14, 120(%rsp)         # 8-byte Spill
	movq	%r15, 184(%rsp)         # 8-byte Spill
	movq	%r13, 240(%rsp)         # 8-byte Spill
	movq	%r8, 216(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_191:                              # %.preheader3675
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB2_194 Depth 6
                                        #               Child Loop BB2_195 Depth 7
	cmpl	$0, 168(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_200
# BB#192:                               # %.preheader3667.lr.ph
                                        #   in Loop: Header=BB2_191 Depth=5
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_199
# BB#193:                               # %.preheader3667.us.preheader
                                        #   in Loop: Header=BB2_191 Depth=5
	movl	%edx, 152(%rsp)         # 4-byte Spill
	movq	288(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%rbx), %ecx
	movl	%ecx, 336(%rsp)         # 4-byte Spill
	xorl	%ecx, %ecx
	movl	%edi, 176(%rsp)         # 4-byte Spill
	movl	%esi, 112(%rsp)         # 4-byte Spill
	movl	%esi, %edx
	.p2align	4, 0x90
.LBB2_194:                              # %.preheader3667.us
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        #           Parent Loop BB2_191 Depth=5
                                        # =>          This Loop Header: Depth=6
                                        #               Child Loop BB2_195 Depth 7
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	movl	%edx, 136(%rsp)         # 4-byte Spill
	movslq	%edx, %rcx
	movq	128(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %r14
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movslq	%ebx, %rdx
	movl	%edi, 72(%rsp)          # 4-byte Spill
	movslq	%edi, %rcx
	movq	88(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rsi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	352(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rsi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	256(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rsi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	296(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rsi
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	192(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdi
	movq	224(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rcx
	movq	160(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rsi
	movq	64(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	xorl	%r8d, %r8d
	movq	248(%rsp), %r12         # 8-byte Reload
	movq	120(%rsp), %r13         # 8-byte Reload
	movq	184(%rsp), %r15         # 8-byte Reload
	movq	240(%rsp), %r11         # 8-byte Reload
	movq	216(%rsp), %r9          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_195:                              #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        #           Parent Loop BB2_191 Depth=5
                                        #             Parent Loop BB2_194 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movq	8(%rsp), %rbx           # 8-byte Reload
	movsd	(%rbx,%r8), %xmm0       # xmm0 = mem[0],zero
	leaq	(%rsi,%r8), %r10
	mulsd	(%r9,%r10), %xmm0
	movq	24(%rsp), %rbx          # 8-byte Reload
	movsd	(%rbx,%r8), %xmm1       # xmm1 = mem[0],zero
	mulsd	(%r11,%r10), %xmm1
	movq	16(%rsp), %rbx          # 8-byte Reload
	movsd	(%rbx,%r8), %xmm2       # xmm2 = mem[0],zero
	mulsd	(%r15,%r10), %xmm2
	movq	32(%rsp), %rbx          # 8-byte Reload
	movsd	(%rbx,%r8), %xmm3       # xmm3 = mem[0],zero
	mulsd	(%r13,%r10), %xmm3
	movsd	(%rdi,%r8), %xmm4       # xmm4 = mem[0],zero
	mulsd	(%r12,%r10), %xmm4
	movsd	(%rcx,%r8), %xmm5       # xmm5 = mem[0],zero
	mulsd	(%rax,%r10), %xmm5
	addsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm2
	addsd	%xmm2, %xmm3
	addsd	%xmm3, %xmm4
	addsd	%xmm4, %xmm5
	addsd	(%r14,%r8), %xmm5
	movsd	%xmm5, (%r14,%r8)
	addq	%rbp, %r8
	decl	%edx
	jne	.LBB2_195
# BB#196:                               # %._crit_edge4111.us
                                        #   in Loop: Header=BB2_194 Depth=6
	movl	48(%rsp), %ecx          # 4-byte Reload
	incl	%ecx
	movl	136(%rsp), %edx         # 4-byte Reload
	addl	104(%rsp), %edx         # 4-byte Folded Reload
	movq	80(%rsp), %rbx          # 8-byte Reload
	addl	96(%rsp), %ebx          # 4-byte Folded Reload
	movl	72(%rsp), %edi          # 4-byte Reload
	addl	56(%rsp), %edi          # 4-byte Folded Reload
	cmpl	168(%rsp), %ecx         # 4-byte Folded Reload
	jne	.LBB2_194
# BB#197:                               # %._crit_edge4119.loopexit
                                        #   in Loop: Header=BB2_191 Depth=5
	movl	176(%rsp), %edi         # 4-byte Reload
	addl	264(%rsp), %edi         # 4-byte Folded Reload
	movl	112(%rsp), %esi         # 4-byte Reload
	addl	328(%rsp), %esi         # 4-byte Folded Reload
	movl	200(%rsp), %r9d         # 4-byte Reload
	movl	232(%rsp), %r12d        # 4-byte Reload
	movl	144(%rsp), %ecx         # 4-byte Reload
	movl	152(%rsp), %edx         # 4-byte Reload
	movl	336(%rsp), %ebx         # 4-byte Reload
	jmp	.LBB2_201
	.p2align	4, 0x90
.LBB2_199:                              # %.preheader3667.preheader
                                        #   in Loop: Header=BB2_191 Depth=5
	addl	416(%rsp), %ebx         # 4-byte Folded Reload
	addl	424(%rsp), %edi         # 4-byte Folded Reload
	addl	392(%rsp), %esi         # 4-byte Folded Reload
.LBB2_200:                              # %._crit_edge4119
                                        #   in Loop: Header=BB2_191 Depth=5
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill>
.LBB2_201:                              # %._crit_edge4119
                                        #   in Loop: Header=BB2_191 Depth=5
	addl	%r12d, %edi
	addl	%ecx, %ebx
	addl	%r9d, %esi
	incl	%edx
	cmpl	212(%rsp), %edx         # 4-byte Folded Reload
                                        # kill: %EBX<def> %EBX<kill> %RBX<def>
	jne	.LBB2_191
	jmp	.LBB2_216
.LBB2_202:                              #   in Loop: Header=BB2_75 Depth=4
	movq	464(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %edx
	movq	272(%rsp), %rcx         # 8-byte Reload
	movl	8(%rcx), %eax
	movl	%eax, %r12d
	subl	%edx, %r12d
	incl	%r12d
	movl	%edx, 8(%rsp)           # 4-byte Spill
	cmpl	%edx, %eax
	movl	(%rcx), %ebx
	movl	12(%rcx), %eax
	movl	$0, %ecx
	cmovsl	%ecx, %r12d
	movl	%eax, %r8d
	subl	%ebx, %r8d
	incl	%r8d
	cmpl	%ebx, %eax
	movq	496(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r15d
	movq	312(%rsp), %rdx         # 8-byte Reload
	movl	8(%rdx), %eax
	cmovsl	%ecx, %r8d
	movl	%eax, %edi
	subl	%r15d, %edi
	incl	%edi
	cmpl	%r15d, %eax
	movl	(%rdx), %ebp
	movl	12(%rdx), %eax
	cmovsl	%ecx, %edi
	movl	%eax, %r10d
	subl	%ebp, %r10d
	incl	%r10d
	cmpl	%ebp, %eax
	movq	320(%rsp), %rsi         # 8-byte Reload
	movl	-4(%rsi), %eax
	movq	488(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx), %r13d
	cmovsl	%ecx, %r10d
	movl	%eax, %r14d
	subl	%r13d, %r14d
	incl	%r14d
	cmpl	%r13d, %eax
	movl	(%rsi), %eax
	movl	-12(%rsi), %r11d
	cmovsl	%ecx, %r14d
	movl	%eax, %r9d
	subl	%r11d, %r9d
	incl	%r9d
	cmpl	%r11d, %eax
	cmovsl	%ecx, %r9d
	cmpl	$0, 308(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_216
# BB#203:                               # %.lr.ph4155
                                        #   in Loop: Header=BB2_75 Depth=4
	cmpl	$0, 212(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_216
# BB#204:                               # %.preheader3674.preheader
                                        #   in Loop: Header=BB2_75 Depth=4
	movq	408(%rsp), %rcx         # 8-byte Reload
	movq	48(%rcx), %rax
	movq	64(%rcx), %rcx
	movq	448(%rsp), %rdx         # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	movl	%edi, 144(%rsp)         # 4-byte Spill
	movq	280(%rsp), %rdi         # 8-byte Reload
	movslq	(%rcx,%rdi,4), %rdx
	leaq	(%rax,%rdx,8), %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movslq	4(%rcx,%rdi,4), %rdx
	leaq	(%rax,%rdx,8), %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movslq	8(%rcx,%rdi,4), %rdx
	leaq	(%rax,%rdx,8), %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movslq	12(%rcx,%rdi,4), %rdx
	leaq	(%rax,%rdx,8), %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movslq	16(%rcx,%rdi,4), %rdx
	leaq	(%rax,%rdx,8), %rdx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movslq	20(%rcx,%rdi,4), %rdx
	leaq	(%rax,%rdx,8), %rdx
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	movslq	24(%rcx,%rdi,4), %rcx
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	456(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %ecx
	movl	%ecx, %eax
	movl	%ecx, %esi
	subl	%r15d, %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movq	480(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx), %eax
	movl	%eax, %ecx
	subl	%ebp, %ecx
	movl	4(%rdx), %r15d
	movl	%r15d, %ebp
	movq	312(%rsp), %rdx         # 8-byte Reload
	subl	4(%rdx), %ebp
	imull	%r10d, %ebp
	addl	%ecx, %ebp
	movl	%esi, %ecx
	subl	%r13d, %ecx
	movl	%ecx, 104(%rsp)         # 4-byte Spill
	movl	%eax, %ecx
	subl	%r11d, %ecx
	movl	%r15d, %r11d
	movq	320(%rsp), %rdx         # 8-byte Reload
	subl	-8(%rdx), %r11d
	imull	%r9d, %r11d
	addl	%ecx, %r11d
	subl	8(%rsp), %esi           # 4-byte Folded Reload
	movl	%esi, 8(%rsp)           # 4-byte Spill
	subl	%ebx, %eax
	movq	272(%rsp), %rcx         # 8-byte Reload
	subl	4(%rcx), %r15d
	imull	%r8d, %r15d
	addl	%eax, %r15d
	imull	%r12d, %r15d
	movl	%r12d, 352(%rsp)        # 4-byte Spill
	imull	%r8d, %r12d
	imull	144(%rsp), %ebp         # 4-byte Folded Reload
	movq	384(%rsp), %rdx         # 8-byte Reload
	movl	28(%rdx), %ebx
	movl	%ebx, %eax
	imull	144(%rsp), %eax         # 4-byte Folded Reload
	movq	%rax, %r13
	movl	144(%rsp), %eax         # 4-byte Reload
	imull	%r10d, %eax
	movl	%eax, 144(%rsp)         # 4-byte Spill
	movl	32(%rdx), %eax
	imull	%eax, %r12d
	movl	%r14d, %ecx
	imull	%r9d, %ecx
	imull	%eax, %ecx
	movl	%ecx, 264(%rsp)         # 4-byte Spill
	movl	144(%rsp), %ecx         # 4-byte Reload
	imull	%eax, %ecx
	movl	%ecx, 144(%rsp)         # 4-byte Spill
	leaq	6(%rdi), %rax
	leaq	(%rax,%rax,2), %rax
	movq	472(%rsp), %rsi         # 8-byte Reload
	movl	8(%rsi,%rax,4), %ecx
	imull	%r9d, %ecx
	addl	4(%rsi,%rax,4), %ecx
	imull	%r14d, %ecx
	movslq	(%rsi,%rax,4), %rax
	movslq	%ecx, %rcx
	addq	%rax, %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	5(%rdi), %rax
	leaq	(%rax,%rax,2), %rax
	movl	8(%rsi,%rax,4), %ecx
	imull	%r9d, %ecx
	addl	4(%rsi,%rax,4), %ecx
	imull	%r14d, %ecx
	movslq	(%rsi,%rax,4), %rax
	movslq	%ecx, %rcx
	addq	%rax, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	leaq	4(%rdi), %rax
	leaq	(%rax,%rax,2), %rax
	movl	8(%rsi,%rax,4), %ecx
	imull	%r9d, %ecx
	addl	4(%rsi,%rax,4), %ecx
	imull	%r14d, %ecx
	movslq	(%rsi,%rax,4), %rax
	movslq	%ecx, %rcx
	addq	%rax, %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	leaq	3(%rdi), %rax
	leaq	(%rax,%rax,2), %rax
	movl	8(%rsi,%rax,4), %ecx
	imull	%r9d, %ecx
	addl	4(%rsi,%rax,4), %ecx
	imull	%r14d, %ecx
	movslq	(%rsi,%rax,4), %rax
	movslq	%ecx, %rcx
	addq	%rax, %rcx
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	leaq	2(%rdi), %rax
	leaq	(%rax,%rax,2), %rax
	movl	8(%rsi,%rax,4), %ecx
	imull	%r9d, %ecx
	addl	4(%rsi,%rax,4), %ecx
	imull	%r14d, %ecx
	movslq	(%rsi,%rax,4), %rax
	movslq	%ecx, %rcx
	addq	%rax, %rcx
	movq	%rcx, 216(%rsp)         # 8-byte Spill
	leaq	1(%rdi), %rax
	leaq	(%rax,%rax,2), %rax
	movl	8(%rsi,%rax,4), %ecx
	imull	%r9d, %ecx
	addl	4(%rsi,%rax,4), %ecx
	imull	%r14d, %ecx
	movslq	(%rsi,%rax,4), %rax
	movslq	%ecx, %rcx
	addq	%rax, %rcx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	imull	%r14d, %r11d
	movl	352(%rsp), %r8d         # 4-byte Reload
	imull	%ebx, %r8d
	imull	%r14d, %ebx
	leaq	(%rdi,%rdi,2), %rax
	movl	144(%rsp), %edi         # 4-byte Reload
	imull	8(%rsi,%rax,4), %r9d
	addl	4(%rsi,%rax,4), %r9d
	imull	%r14d, %r9d
	movslq	(%rsi,%rax,4), %rax
	movslq	%r9d, %rcx
	movq	%r11, %rsi
	addq	%rax, %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movslq	24(%rdx), %r11
	movl	%r11d, %r14d
	negl	%r14d
	movq	64(%rsp), %r10          # 8-byte Reload
	imull	%r10d, %r14d
	movq	%r13, %rcx
	movq	%rcx, 256(%rsp)         # 8-byte Spill
	leal	(%r14,%rcx), %eax
	leal	(%r14,%rbx), %edx
	addl	%r8d, %r14d
	movl	%r8d, %r13d
	movl	304(%rsp), %r9d         # 4-byte Reload
	imull	%r9d, %eax
	addl	%ecx, %eax
	movl	%r11d, %r8d
	imull	%r10d, %r8d
	movl	%r15d, %ecx
	subl	%r8d, %eax
	movl	%eax, 512(%rsp)         # 4-byte Spill
	imull	%r9d, %edx
	addl	%ebx, %edx
	subl	%r8d, %edx
	movl	%edx, 504(%rsp)         # 4-byte Spill
	imull	%r9d, %r14d
	movl	%ebp, %edx
	addl	%r13d, %r14d
	subl	%r8d, %r14d
	movq	%r14, 416(%rsp)         # 8-byte Spill
	addl	8(%rsp), %ecx           # 4-byte Folded Reload
	addl	104(%rsp), %esi         # 4-byte Folded Reload
	addl	24(%rsp), %edx          # 4-byte Folded Reload
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	%rax, %r8
	imull	256(%rsp), %eax         # 4-byte Folded Reload
	movl	%eax, 328(%rsp)         # 4-byte Spill
	subl	%eax, %edi
	movl	%r8d, %eax
	movq	%rbx, 192(%rsp)         # 8-byte Spill
	imull	%ebx, %eax
	movl	264(%rsp), %ebx         # 4-byte Reload
	movq	%rax, 392(%rsp)         # 8-byte Spill
	subl	%eax, %ebx
	movl	%r8d, %eax
	movl	%r13d, 352(%rsp)        # 4-byte Spill
	imull	%r13d, %eax
	movl	%eax, 424(%rsp)         # 4-byte Spill
	subl	%eax, %r12d
	shlq	$3, %r11
	movq	520(%rsp), %r8          # 8-byte Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	leaq	(%r8,%rax,8), %r9
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%r8,%rax,8), %r14
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%r8,%rax,8), %r15
	movq	240(%rsp), %rax         # 8-byte Reload
	leaq	(%r8,%rax,8), %r13
	movq	216(%rsp), %rax         # 8-byte Reload
	leaq	(%r8,%rax,8), %r10
	movq	176(%rsp), %rax         # 8-byte Reload
	leaq	(%r8,%rax,8), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	(%r8,%rax,8), %rbp
	xorl	%eax, %eax
	movl	%r12d, 288(%rsp)        # 4-byte Spill
	movl	%edi, 144(%rsp)         # 4-byte Spill
	movl	%ebx, 264(%rsp)         # 4-byte Spill
	movq	%r11, 224(%rsp)         # 8-byte Spill
	movq	%r9, 248(%rsp)          # 8-byte Spill
	movq	%r14, 120(%rsp)         # 8-byte Spill
	movq	%r15, 184(%rsp)         # 8-byte Spill
	movq	%r13, 240(%rsp)         # 8-byte Spill
	movq	%r10, 216(%rsp)         # 8-byte Spill
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_205:                              # %.preheader3674
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB2_208 Depth 6
                                        #               Child Loop BB2_209 Depth 7
	cmpl	$0, 168(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_214
# BB#206:                               # %.preheader3666.lr.ph
                                        #   in Loop: Header=BB2_205 Depth=5
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_213
# BB#207:                               # %.preheader3666.us.preheader
                                        #   in Loop: Header=BB2_205 Depth=5
	movl	%eax, 200(%rsp)         # 4-byte Spill
	movq	392(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rsi), %eax
	movl	%eax, 232(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	movl	%edx, 336(%rsp)         # 4-byte Spill
	movl	%ecx, 152(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB2_208:                              # %.preheader3666.us
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        #           Parent Loop BB2_205 Depth=5
                                        # =>          This Loop Header: Depth=6
                                        #               Child Loop BB2_209 Depth 7
	movl	%eax, 96(%rsp)          # 4-byte Spill
	movl	%ecx, 88(%rsp)          # 4-byte Spill
	movslq	%ecx, %rax
	movq	128(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movslq	%esi, %rax
	movl	%edx, 56(%rsp)          # 4-byte Spill
	movslq	%edx, %rdx
	shlq	$3, %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	160(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	xorl	%edx, %edx
	movq	296(%rsp), %r8          # 8-byte Reload
	movq	224(%rsp), %r13         # 8-byte Reload
	movq	248(%rsp), %r15         # 8-byte Reload
	movq	120(%rsp), %r14         # 8-byte Reload
	movq	184(%rsp), %rsi         # 8-byte Reload
	movq	240(%rsp), %rbx         # 8-byte Reload
	movq	216(%rsp), %r9          # 8-byte Reload
	movq	176(%rsp), %r12         # 8-byte Reload
	movq	112(%rsp), %r11         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_209:                              #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_75 Depth=4
                                        #           Parent Loop BB2_205 Depth=5
                                        #             Parent Loop BB2_208 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movq	8(%rsp), %rbp           # 8-byte Reload
	leaq	(%rbp,%rdx), %rbp
	movq	16(%rsp), %r10          # 8-byte Reload
	movsd	(%r10,%rbp), %xmm0      # xmm0 = mem[0],zero
	movq	32(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%rbp), %xmm1      # xmm1 = mem[0],zero
	movq	80(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%rbp), %xmm2      # xmm2 = mem[0],zero
	movq	48(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%rbp), %xmm3      # xmm3 = mem[0],zero
	movq	72(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%rbp), %xmm4      # xmm4 = mem[0],zero
	movq	136(%rsp), %rdi         # 8-byte Reload
	movsd	(%rdi,%rbp), %xmm5      # xmm5 = mem[0],zero
	movsd	(%r8,%rbp), %xmm6       # xmm6 = mem[0],zero
	movq	24(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rdx), %rbp
	mulsd	(%r11,%rbp), %xmm0
	mulsd	(%r12,%rbp), %xmm1
	mulsd	(%r9,%rbp), %xmm2
	mulsd	(%rbx,%rbp), %xmm3
	mulsd	(%rsi,%rbp), %xmm4
	mulsd	(%r14,%rbp), %xmm5
	mulsd	(%r15,%rbp), %xmm6
	addsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm2
	addsd	%xmm2, %xmm3
	addsd	%xmm3, %xmm4
	addsd	%xmm4, %xmm5
	addsd	%xmm5, %xmm6
	addsd	(%rcx,%rdx), %xmm6
	movsd	%xmm6, (%rcx,%rdx)
	addq	%r13, %rdx
	decl	%eax
	jne	.LBB2_209
# BB#210:                               # %._crit_edge4136.us
                                        #   in Loop: Header=BB2_208 Depth=6
	movl	96(%rsp), %eax          # 4-byte Reload
	incl	%eax
	movl	88(%rsp), %ecx          # 4-byte Reload
	addl	352(%rsp), %ecx         # 4-byte Folded Reload
	movq	104(%rsp), %rsi         # 8-byte Reload
	addl	192(%rsp), %esi         # 4-byte Folded Reload
	movl	56(%rsp), %edx          # 4-byte Reload
	addl	256(%rsp), %edx         # 4-byte Folded Reload
	cmpl	168(%rsp), %eax         # 4-byte Folded Reload
	jne	.LBB2_208
# BB#211:                               # %._crit_edge4144.loopexit
                                        #   in Loop: Header=BB2_205 Depth=5
	movl	336(%rsp), %edx         # 4-byte Reload
	addl	328(%rsp), %edx         # 4-byte Folded Reload
	movl	152(%rsp), %ecx         # 4-byte Reload
	addl	424(%rsp), %ecx         # 4-byte Folded Reload
	movl	288(%rsp), %r12d        # 4-byte Reload
	movl	144(%rsp), %edi         # 4-byte Reload
	movl	264(%rsp), %ebx         # 4-byte Reload
	movl	200(%rsp), %eax         # 4-byte Reload
	movl	232(%rsp), %esi         # 4-byte Reload
	jmp	.LBB2_215
	.p2align	4, 0x90
.LBB2_213:                              # %.preheader3666.preheader
                                        #   in Loop: Header=BB2_205 Depth=5
	addl	504(%rsp), %esi         # 4-byte Folded Reload
	addl	512(%rsp), %edx         # 4-byte Folded Reload
	addl	416(%rsp), %ecx         # 4-byte Folded Reload
.LBB2_214:                              # %._crit_edge4144
                                        #   in Loop: Header=BB2_205 Depth=5
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
.LBB2_215:                              # %._crit_edge4144
                                        #   in Loop: Header=BB2_205 Depth=5
	addl	%edi, %edx
	addl	%ebx, %esi
	addl	%r12d, %ecx
	incl	%eax
	cmpl	212(%rsp), %eax         # 4-byte Folded Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	jne	.LBB2_205
	.p2align	4, 0x90
.LBB2_216:                              # %.loopexit3684
                                        #   in Loop: Header=BB2_75 Depth=4
	movq	280(%rsp), %rdx         # 8-byte Reload
	addq	$7, %rdx
	movq	672(%rsp), %rcx         # 8-byte Reload
	cmpq	%rdx, %rcx
	jg	.LBB2_75
.LBB2_217:                              # %._crit_edge4173
                                        #   in Loop: Header=BB2_73 Depth=3
	ucomisd	.LCPI2_0(%rip), %xmm7
	movapd	752(%rsp), %xmm2        # 16-byte Reload
	jne	.LBB2_218
	jnp	.LBB2_243
.LBB2_218:                              #   in Loop: Header=BB2_73 Depth=3
	movq	464(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r9d
	movq	272(%rsp), %rdx         # 8-byte Reload
	movl	8(%rdx), %eax
	movl	%eax, %ecx
	subl	%r9d, %ecx
	incl	%ecx
	cmpl	%r9d, %eax
	movl	(%rdx), %r10d
	movl	12(%rdx), %eax
	movl	$0, %esi
	cmovsl	%esi, %ecx
	movl	%eax, %edx
	subl	%r10d, %edx
	incl	%edx
	cmpl	%r10d, %eax
	cmovsl	%esi, %edx
	movl	436(%rsp), %eax
	movl	440(%rsp), %edi
	movl	444(%rsp), %ebp
	cmpl	%eax, %edi
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%eax, %esi
	movl	%edi, 32(%rsp)          # 4-byte Spill
	cmovgel	%edi, %esi
	cmpl	%esi, %ebp
	movl	%ebp, 96(%rsp)          # 4-byte Spill
	cmovgel	%ebp, %esi
	testl	%esi, %esi
	jle	.LBB2_243
# BB#219:                               # %.lr.ph4214
                                        #   in Loop: Header=BB2_73 Depth=3
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_243
# BB#220:                               # %.lr.ph4214.split.us.preheader
                                        #   in Loop: Header=BB2_73 Depth=3
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_243
# BB#221:                               # %.lr.ph4214.split.us.preheader
                                        #   in Loop: Header=BB2_73 Depth=3
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB2_243
# BB#222:                               # %.preheader3681.us.us.us.preheader
                                        #   in Loop: Header=BB2_73 Depth=3
	movq	384(%rsp), %rbx         # 8-byte Reload
	movslq	24(%rbx), %rsi
	movl	32(%rbx), %ebp
	movl	%ecx, %eax
	imull	%edx, %eax
	imull	%ebp, %eax
	movl	%eax, 88(%rsp)          # 4-byte Spill
	movl	28(%rbx), %eax
	imull	%ecx, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	456(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r8d
	subl	%r9d, %r8d
	movq	664(%rsp), %rax         # 8-byte Reload
	movq	656(%rsp), %rdi         # 8-byte Reload
	movl	4(%rax,%rdi,8), %ebx
	subl	%r10d, %ebx
	movl	8(%rax,%rdi,8), %edi
	movq	272(%rsp), %rax         # 8-byte Reload
	subl	4(%rax), %edi
	imull	%edx, %edi
	addl	%ebx, %edi
	imull	%ecx, %edi
	addl	%r8d, %edi
	movq	536(%rsp), %rbx         # 8-byte Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	-1(%rax), %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	incq	%rax
	imull	%ecx, %ebp
	imull	%edx, %ebp
	movl	%ebp, 56(%rsp)          # 4-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movabsq	$8589934588, %rcx       # imm = 0x1FFFFFFFC
	andq	%rcx, %rax
	leaq	-4(%rax), %rcx
	shrq	$2, %rcx
	movq	%rsi, %rdx
	movq	%rax, 48(%rsp)          # 8-byte Spill
	imulq	%rax, %rdx
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	movq	%rcx, 136(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rsi, %r8
	shlq	$6, %r8
	movq	%rsi, %r9
	shlq	$5, %r9
	leaq	(%rsi,%rsi,2), %rcx
	leaq	(%rbx,%rcx,8), %r10
	movq	%rsi, %r11
	shlq	$4, %r11
	addq	%rbx, %r11
	leaq	(%rbx,%rsi,8), %r15
	xorl	%eax, %eax
	movl	%edi, 352(%rsp)         # 4-byte Spill
	movl	%edi, %ecx
	.p2align	4, 0x90
.LBB2_223:                              # %.preheader3681.us.us.us
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB2_229 Depth 5
                                        #             Child Loop BB2_226 Depth 6
                                        #             Child Loop BB2_237 Depth 6
                                        #             Child Loop BB2_240 Depth 6
	movl	56(%rsp), %edx          # 4-byte Reload
	movl	%eax, 344(%rsp)         # 4-byte Spill
	imull	%eax, %edx
	addl	352(%rsp), %edx         # 4-byte Folded Reload
	movl	%edx, 72(%rsp)          # 4-byte Spill
	movl	%ecx, 104(%rsp)         # 4-byte Spill
	xorl	%r14d, %r14d
	movq	680(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB2_229
.LBB2_224:                              #   in Loop: Header=BB2_229 Depth=5
	xorl	%ebx, %ebx
	cmpq	$0, 136(%rsp)           # 8-byte Folded Reload
	je	.LBB2_227
.LBB2_225:                              # %vector.ph.new
                                        #   in Loop: Header=BB2_229 Depth=5
	movq	48(%rsp), %rdx          # 8-byte Reload
	subq	%rbx, %rdx
	movq	%rsi, %rdi
	imulq	%rbx, %rdi
	addq	%r13, %rdi
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	16(%rax,%rdi,8), %rdi
	addq	$4, %rbx
	imulq	%rsi, %rbx
	addq	%r13, %rbx
	leaq	16(%rax,%rbx,8), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_226:                              # %vector.body
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_223 Depth=4
                                        #           Parent Loop BB2_229 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movupd	-16(%rdi,%rbp), %xmm0
	movupd	(%rdi,%rbp), %xmm1
	mulpd	%xmm2, %xmm0
	mulpd	%xmm2, %xmm1
	movupd	%xmm0, -16(%rdi,%rbp)
	movupd	%xmm1, (%rdi,%rbp)
	movupd	-16(%rbx,%rbp), %xmm0
	movupd	(%rbx,%rbp), %xmm1
	mulpd	%xmm2, %xmm0
	mulpd	%xmm2, %xmm1
	movupd	%xmm0, -16(%rbx,%rbp)
	movupd	%xmm1, (%rbx,%rbp)
	addq	%r8, %rbp
	addq	$-8, %rdx
	jne	.LBB2_226
.LBB2_227:                              # %middle.block
                                        #   in Loop: Header=BB2_229 Depth=5
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, 16(%rsp)          # 8-byte Folded Reload
	movq	680(%rsp), %rbp         # 8-byte Reload
	je	.LBB2_241
# BB#228:                               #   in Loop: Header=BB2_229 Depth=5
	addq	168(%rsp), %rcx         # 8-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r12d
	jmp	.LBB2_235
	.p2align	4, 0x90
.LBB2_229:                              # %.preheader3673.us.us.us.us
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_223 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB2_226 Depth 6
                                        #             Child Loop BB2_237 Depth 6
                                        #             Child Loop BB2_240 Depth 6
	movslq	%ecx, %r13
	xorl	%r12d, %r12d
	cmpq	$3, 16(%rsp)            # 8-byte Folded Reload
	jbe	.LBB2_234
# BB#230:                               # %min.iters.checked
                                        #   in Loop: Header=BB2_229 Depth=5
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB2_234
# BB#231:                               # %vector.scevcheck
                                        #   in Loop: Header=BB2_229 Depth=5
	cmpl	$1, %esi
	jne	.LBB2_234
# BB#232:                               # %vector.ph
                                        #   in Loop: Header=BB2_229 Depth=5
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	imull	%r14d, %ecx
	addl	72(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	jne	.LBB2_224
# BB#233:                               # %vector.body.prol
                                        #   in Loop: Header=BB2_229 Depth=5
	movq	128(%rsp), %rax         # 8-byte Reload
	movupd	(%rax,%rcx,8), %xmm0
	movupd	16(%rax,%rcx,8), %xmm1
	mulpd	%xmm2, %xmm0
	mulpd	%xmm2, %xmm1
	movupd	%xmm0, (%rax,%rcx,8)
	movupd	%xmm1, 16(%rax,%rcx,8)
	movl	$4, %ebx
	cmpq	$0, 136(%rsp)           # 8-byte Folded Reload
	jne	.LBB2_225
	jmp	.LBB2_227
	.p2align	4, 0x90
.LBB2_234:                              #   in Loop: Header=BB2_229 Depth=5
	movq	%r13, %rcx
.LBB2_235:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB2_229 Depth=5
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %edi
	subl	%r12d, %edi
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	%eax, %edx
	subl	%r12d, %edx
	andl	$3, %edi
	je	.LBB2_238
# BB#236:                               # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB2_229 Depth=5
	negl	%edi
	movq	128(%rsp), %rax         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_237:                              # %scalar.ph.prol
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_223 Depth=4
                                        #           Parent Loop BB2_229 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movsd	(%rax,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm7, %xmm0
	movsd	%xmm0, (%rax,%rcx,8)
	addq	%rsi, %rcx
	incl	%r12d
	incl	%edi
	jne	.LBB2_237
.LBB2_238:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB2_229 Depth=5
	cmpl	$3, %edx
	jb	.LBB2_241
# BB#239:                               # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB2_229 Depth=5
	leaq	(%rbp,%rcx,8), %rcx
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %edx
	subl	%r12d, %edx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_240:                              # %scalar.ph
                                        #   Parent Loop BB2_30 Depth=1
                                        #     Parent Loop BB2_71 Depth=2
                                        #       Parent Loop BB2_73 Depth=3
                                        #         Parent Loop BB2_223 Depth=4
                                        #           Parent Loop BB2_229 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	536(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rdi), %rbx
	movsd	(%rcx,%rbx), %xmm0      # xmm0 = mem[0],zero
	mulsd	%xmm7, %xmm0
	movsd	%xmm0, (%rcx,%rbx)
	leaq	(%r15,%rdi), %rbx
	movsd	(%rcx,%rbx), %xmm0      # xmm0 = mem[0],zero
	mulsd	%xmm7, %xmm0
	movsd	%xmm0, (%rcx,%rbx)
	leaq	(%r11,%rdi), %rbx
	movsd	(%rcx,%rbx), %xmm0      # xmm0 = mem[0],zero
	mulsd	%xmm7, %xmm0
	movsd	%xmm0, (%rcx,%rbx)
	leaq	(%r10,%rdi), %rbx
	movsd	(%rcx,%rbx), %xmm0      # xmm0 = mem[0],zero
	mulsd	%xmm7, %xmm0
	movsd	%xmm0, (%rcx,%rbx)
	addq	%r9, %rdi
	addl	$-4, %edx
	jne	.LBB2_240
.LBB2_241:                              # %._crit_edge4191.us.us.us.us
                                        #   in Loop: Header=BB2_229 Depth=5
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	(%r13,%rax), %ecx
	incl	%r14d
	cmpl	32(%rsp), %r14d         # 4-byte Folded Reload
	jne	.LBB2_229
# BB#242:                               # %._crit_edge4195.us-lcssa.us.us.us.us
                                        #   in Loop: Header=BB2_223 Depth=4
	movl	344(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movl	104(%rsp), %ecx         # 4-byte Reload
	addl	88(%rsp), %ecx          # 4-byte Folded Reload
	cmpl	96(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB2_223
	.p2align	4, 0x90
.LBB2_243:                              # %.loopexit3693
                                        #   in Loop: Header=BB2_73 Depth=3
	movq	712(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	movq	704(%rsp), %rdi         # 8-byte Reload
	movslq	8(%rdi), %rax
	cmpq	%rax, %rcx
	jl	.LBB2_73
# BB#244:                               # %._crit_edge4268.loopexit
                                        #   in Loop: Header=BB2_71 Depth=2
	movq	576(%rsp), %rdx         # 8-byte Reload
	movl	8(%rdx), %eax
	movq	408(%rsp), %rbp         # 8-byte Reload
	movq	648(%rsp), %r15         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
.LBB2_245:                              # %._crit_edge4268
                                        #   in Loop: Header=BB2_71 Depth=2
	incq	%rsi
	movslq	%eax, %rcx
	cmpq	%rcx, %rsi
	jl	.LBB2_71
.LBB2_246:                              # %._crit_edge4304
                                        #   in Loop: Header=BB2_30 Depth=1
	movl	620(%rsp), %eax         # 4-byte Reload
	incl	%eax
	cmpl	$2, %eax
	jne	.LBB2_30
.LBB2_247:                              # %.loopexit
	xorl	%eax, %eax
	addq	$792, %rsp              # imm = 0x318
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	hypre_StructMatvecCompute, .Lfunc_end2-hypre_StructMatvecCompute
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_77
	.quad	.LBB2_166
	.quad	.LBB2_99
	.quad	.LBB2_123
	.quad	.LBB2_152
	.quad	.LBB2_188
	.quad	.LBB2_202

	.text
	.globl	hypre_StructMatvecDestroy
	.p2align	4, 0x90
	.type	hypre_StructMatvecDestroy,@function
hypre_StructMatvecDestroy:              # @hypre_StructMatvecDestroy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 16
.Lcfi35:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB3_2
# BB#1:
	movq	(%rbx), %rdi
	callq	hypre_StructMatrixDestroy
	movq	8(%rbx), %rdi
	callq	hypre_StructVectorDestroy
	movq	16(%rbx), %rdi
	callq	hypre_ComputePkgDestroy
	movq	%rbx, %rdi
	callq	hypre_Free
.LBB3_2:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	hypre_StructMatvecDestroy, .Lfunc_end3-hypre_StructMatvecDestroy
	.cfi_endproc

	.globl	hypre_StructMatvec
	.p2align	4, 0x90
	.type	hypre_StructMatvec,@function
hypre_StructMatvec:                     # @hypre_StructMatvec
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 48
	subq	$96, %rsp
.Lcfi41:
	.cfi_def_cfa_offset 144
.Lcfi42:
	.cfi_offset %rbx, -48
.Lcfi43:
	.cfi_offset %r12, -40
.Lcfi44:
	.cfi_offset %r13, -32
.Lcfi45:
	.cfi_offset %r14, -24
.Lcfi46:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %r12
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	$1, %edi
	movl	$24, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbx
	movq	8(%r12), %r13
	movq	24(%r12), %rsi
	leaq	32(%rsp), %rax
	leaq	40(%rsp), %r10
	leaq	72(%rsp), %rdx
	leaq	64(%rsp), %rcx
	leaq	56(%rsp), %r8
	leaq	48(%rsp), %r9
	movq	%r13, %rdi
	pushq	%rax
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	callq	hypre_CreateComputeInfo
	addq	$16, %rsp
.Lcfi49:
	.cfi_adjust_cfa_offset -16
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 84(%rsp)
	movl	$1, 92(%rsp)
	movq	72(%rsp), %rdi
	movq	64(%rsp), %rsi
	movq	56(%rsp), %r8
	movq	48(%rsp), %r9
	subq	$8, %rsp
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	leaq	32(%rsp), %rax
	leaq	92(%rsp), %rdx
	movq	%rdx, %rcx
	pushq	%rax
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	pushq	16(%r15)
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	pushq	%rdx
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	pushq	80(%rsp)
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	callq	hypre_ComputePkgCreate
	addq	$64, %rsp
.Lcfi58:
	.cfi_adjust_cfa_offset -64
	movq	%r12, %rdi
	callq	hypre_StructMatrixRef
	movq	%rax, (%rbx)
	movq	%r15, %rdi
	callq	hypre_StructVectorRef
	movq	%rax, 8(%rbx)
	movq	24(%rsp), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, %rdi
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%r12, %rsi
	movq	%r15, %rdx
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%r14, %rcx
	callq	hypre_StructMatvecCompute
	testq	%rbx, %rbx
	je	.LBB4_2
# BB#1:
	movq	(%rbx), %rdi
	callq	hypre_StructMatrixDestroy
	movq	8(%rbx), %rdi
	callq	hypre_StructVectorDestroy
	movq	16(%rbx), %rdi
	callq	hypre_ComputePkgDestroy
	movq	%rbx, %rdi
	callq	hypre_Free
.LBB4_2:                                # %hypre_StructMatvecDestroy.exit
	xorl	%eax, %eax
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	hypre_StructMatvec, .Lfunc_end4-hypre_StructMatvec
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
