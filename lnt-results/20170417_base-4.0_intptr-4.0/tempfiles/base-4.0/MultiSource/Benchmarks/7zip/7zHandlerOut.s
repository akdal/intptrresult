	.text
	.file	"7zHandlerOut.bc"
	.globl	_ZN8NArchive3N7z8CHandler15GetFileTimeTypeEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler15GetFileTimeTypeEPj,@function
_ZN8NArchive3N7z8CHandler15GetFileTimeTypeEPj: # @_ZN8NArchive3N7z8CHandler15GetFileTimeTypeEPj
	.cfi_startproc
# BB#0:
	movl	$0, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	_ZN8NArchive3N7z8CHandler15GetFileTimeTypeEPj, .Lfunc_end0-_ZN8NArchive3N7z8CHandler15GetFileTimeTypeEPj
	.cfi_endproc

	.globl	_ZThn120_N8NArchive3N7z8CHandler15GetFileTimeTypeEPj
	.p2align	4, 0x90
	.type	_ZThn120_N8NArchive3N7z8CHandler15GetFileTimeTypeEPj,@function
_ZThn120_N8NArchive3N7z8CHandler15GetFileTimeTypeEPj: # @_ZThn120_N8NArchive3N7z8CHandler15GetFileTimeTypeEPj
	.cfi_startproc
# BB#0:
	movl	$0, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	_ZThn120_N8NArchive3N7z8CHandler15GetFileTimeTypeEPj, .Lfunc_end1-_ZThn120_N8NArchive3N7z8CHandler15GetFileTimeTypeEPj
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	76                      # 0x4c
	.long	90                      # 0x5a
	.long	77                      # 0x4d
	.long	65                      # 0x41
	.text
	.globl	_ZN8NArchive3N7z8CHandler20SetCompressionMethodERNS0_22CCompressionMethodModeES3_
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler20SetCompressionMethodERNS0_22CCompressionMethodModeES3_,@function
_ZN8NArchive3N7z8CHandler20SetCompressionMethodERNS0_22CCompressionMethodModeES3_: # @_ZN8NArchive3N7z8CHandler20SetCompressionMethodERNS0_22CCompressionMethodModeES3_
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 256
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r13
	movq	%rdi, %r12
	leaq	16(%r12), %rdx
	movl	8(%r12), %ecx
	callq	_ZN8NArchive3N7z8CHandler20SetCompressionMethodERNS0_22CCompressionMethodModeER13CObjectVectorINS_14COneMethodInfoEEj
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB2_38
# BB#1:
	leaq	32(%r13), %r15
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	860(%r12), %ebp
	movl	44(%r13), %esi
	addl	%ebp, %esi
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%ebp, %ebp
	jle	.LBB2_4
# BB#2:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	movq	864(%r12), %rax
	movups	(%rax,%rbx), %xmm0
	movaps	%xmm0, 176(%rsp)        # 16-byte Spill
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	48(%r13), %rax
	movslq	44(%r13), %rcx
	shlq	$4, %rcx
	movaps	176(%rsp), %xmm0        # 16-byte Reload
	movups	%xmm0, (%rax,%rcx)
	incl	44(%r13)
	addq	$16, %rbx
	decq	%rbp
	jne	.LBB2_3
.LBB2_4:                                # %_ZN13CRecordVectorIN8NArchive3N7z5CBindEEaSERKS3_.exit
	cmpb	$0, 74(%r12)
	je	.LBB2_37
# BB#5:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 56(%rsp)
	movq	$8, 72(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, 48(%rsp)
	movups	%xmm0, 8(%rsp)
	movq	$8, 24(%rsp)
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rsp)
	movups	%xmm0, 32(%rsp)
.Ltmp0:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp1:
# BB#6:
	movq	%rbx, 32(%rsp)
	movl	$4, 44(%rsp)
	movl	$0, 40(%rsp)
	movl	$0, (%rbx)
.Ltmp12:
	movl	$20, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp13:
# BB#7:                                 # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.preheader
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	%rbp, 32(%rsp)
	movl	$5, 44(%rsp)
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [76,90,77,65]
	movups	%xmm0, (%rbp)
	movl	$0, 16(%rbp)
	movl	$4, 40(%rsp)
	leaq	160(%rsp), %r15
	movw	$0, 160(%rsp)
	movw	$0, 162(%rsp)
	movl	$9, 152(%rsp)
.Ltmp14:
	movl	$.L.str.1, %esi
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp15:
# BB#8:
.Ltmp16:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp17:
# BB#9:                                 # %.noexc40
	movl	152(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp18:
	movq	%r15, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp19:
# BB#10:                                # %_ZN5CPropC2ERKS_.exit.i
.Ltmp21:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp22:
# BB#11:
	movq	16(%rsp), %rax
	movslq	12(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rsp)
.Ltmp26:
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp27:
# BB#12:
	leaq	136(%rsp), %r15
	movw	$0, 136(%rsp)
	movw	$0, 138(%rsp)
	movl	$12, 128(%rsp)
.Ltmp29:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp30:
# BB#13:
.Ltmp31:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp32:
# BB#14:                                # %.noexc46
	movl	128(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp33:
	movq	%r15, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp34:
# BB#15:                                # %_ZN5CPropC2ERKS_.exit.i45
.Ltmp36:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp37:
# BB#16:
	movq	16(%rsp), %rax
	movslq	12(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rsp)
.Ltmp41:
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp42:
# BB#17:
	leaq	112(%rsp), %r15
	movw	$0, 112(%rsp)
	movw	$0, 114(%rsp)
	movl	$8, 104(%rsp)
.Ltmp44:
	movl	$273, %esi              # imm = 0x111
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp45:
# BB#18:
.Ltmp46:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp47:
# BB#19:                                # %.noexc54
	movl	104(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp48:
	movq	%r15, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp49:
# BB#20:                                # %_ZN5CPropC2ERKS_.exit.i53
.Ltmp51:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp52:
# BB#21:
	movq	16(%rsp), %rax
	movslq	12(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rsp)
.Ltmp56:
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp57:
# BB#22:
	leaq	88(%rsp), %r15
	movw	$0, 88(%rsp)
	movw	$0, 90(%rsp)
	movl	$1, 80(%rsp)
.Ltmp59:
	movl	$1048576, %esi          # imm = 0x100000
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp60:
# BB#23:
.Ltmp61:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp62:
# BB#24:                                # %.noexc62
	movl	80(%rsp), %eax
	movl	%eax, (%rbp)
	movq	%rbp, %rdi
	addq	$8, %rdi
.Ltmp63:
	movq	%r15, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp64:
# BB#25:                                # %_ZN5CPropC2ERKS_.exit.i61
.Ltmp66:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp67:
# BB#26:
	movq	16(%rsp), %rax
	movslq	12(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rsp)
.Ltmp71:
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp72:
# BB#27:                                # %_ZN5CPropD2Ev.exit68
.Ltmp74:
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp75:
# BB#28:                                # %.noexc69
.Ltmp76:
	movq	%rsp, %rsi
	movq	%rbp, %rdi
	callq	_ZN8NArchive14COneMethodInfoC2ERKS0_
.Ltmp77:
# BB#29:
.Ltmp79:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp80:
# BB#30:
	movq	64(%rsp), %rax
	movslq	60(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 60(%rsp)
.Ltmp81:
	leaq	48(%rsp), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZN8NArchive3N7z8CHandler20SetCompressionMethodERNS0_22CCompressionMethodModeER13CObjectVectorINS_14COneMethodInfoEEj
	movl	%eax, %ebp
.Ltmp82:
# BB#31:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_33
# BB#32:
	callq	_ZdaPv
.LBB2_33:                               # %_ZN11CStringBaseIwED2Ev.exit.i
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rsp)
.Ltmp92:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp93:
# BB#34:                                # %_ZN13CObjectVectorI5CPropED2Ev.exit.i73
.Ltmp98:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp99:
# BB#35:                                # %_ZN8NArchive14COneMethodInfoD2Ev.exit
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, 48(%rsp)
.Ltmp110:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp111:
# BB#36:                                # %_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev.exit
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	testl	%ebp, %ebp
	jne	.LBB2_38
.LBB2_37:
	xorl	%ebp, %ebp
.LBB2_38:
	movl	%ebp, %eax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_39:
.Ltmp112:
	movq	%rax, %rbx
.Ltmp113:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp114:
	jmp	.LBB2_75
.LBB2_40:
.Ltmp115:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_41:
.Ltmp100:
	movq	%rax, %rbx
	jmp	.LBB2_73
.LBB2_42:
.Ltmp94:
	movq	%rax, %rbx
.Ltmp95:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp96:
	jmp	.LBB2_73
.LBB2_43:
.Ltmp97:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_44:
.Ltmp78:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB2_69
.LBB2_45:
.Ltmp73:
	jmp	.LBB2_68
.LBB2_46:
.Ltmp65:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB2_60
.LBB2_47:
.Ltmp58:
	jmp	.LBB2_68
.LBB2_48:
.Ltmp50:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB2_62
.LBB2_49:
.Ltmp43:
	jmp	.LBB2_68
.LBB2_50:
.Ltmp35:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB2_64
.LBB2_51:
.Ltmp28:
	jmp	.LBB2_68
.LBB2_52:
.Ltmp20:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB2_66
.LBB2_53:
.Ltmp2:
	movq	%rax, %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rsp)
.Ltmp3:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp4:
# BB#54:
.Ltmp9:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp10:
	jmp	.LBB2_73
.LBB2_55:
.Ltmp11:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB2_56:
.Ltmp5:
	movq	%rax, %rbx
.Ltmp6:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp7:
# BB#57:                                # %.body.i
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB2_58:
.Ltmp8:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_59:
.Ltmp68:
	movq	%rax, %rbx
.LBB2_60:                               # %.body64
.Ltmp69:
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp70:
	jmp	.LBB2_69
.LBB2_61:
.Ltmp53:
	movq	%rax, %rbx
.LBB2_62:                               # %.body56
.Ltmp54:
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp55:
	jmp	.LBB2_69
.LBB2_63:
.Ltmp38:
	movq	%rax, %rbx
.LBB2_64:                               # %.body48
.Ltmp39:
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp40:
	jmp	.LBB2_69
.LBB2_65:
.Ltmp23:
	movq	%rax, %rbx
.LBB2_66:                               # %.body42
.Ltmp24:
	movq	%r15, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp25:
	jmp	.LBB2_69
.LBB2_67:
.Ltmp83:
.LBB2_68:
	movq	%rax, %rbx
.LBB2_69:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_71
# BB#70:
	callq	_ZdaPv
.LBB2_71:                               # %_ZN11CStringBaseIwED2Ev.exit.i85
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rsp)
.Ltmp84:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp85:
# BB#72:                                # %_ZN13CObjectVectorI5CPropED2Ev.exit.i86
.Ltmp90:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp91:
.LBB2_73:
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, 48(%rsp)
.Ltmp101:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp102:
# BB#74:
.Ltmp107:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp108:
.LBB2_75:                               # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB2_76:
.Ltmp86:
	movq	%rax, %rbx
.Ltmp87:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp88:
	jmp	.LBB2_81
.LBB2_77:
.Ltmp89:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_78:
.Ltmp103:
	movq	%rax, %rbx
.Ltmp104:
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp105:
	jmp	.LBB2_81
.LBB2_79:
.Ltmp106:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_80:
.Ltmp109:
	movq	%rax, %rbx
.LBB2_81:                               # %.body87
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN8NArchive3N7z8CHandler20SetCompressionMethodERNS0_22CCompressionMethodModeES3_, .Lfunc_end2-_ZN8NArchive3N7z8CHandler20SetCompressionMethodERNS0_22CCompressionMethodModeES3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\204\004"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\373\003"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp83-.Lfunc_begin0   #     jumps to .Ltmp83
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp17-.Ltmp14         #   Call between .Ltmp14 and .Ltmp17
	.long	.Ltmp23-.Lfunc_begin0   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin0   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp32-.Ltmp29         #   Call between .Ltmp29 and .Ltmp32
	.long	.Ltmp38-.Lfunc_begin0   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin0   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin0   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin0   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp47-.Ltmp44         #   Call between .Ltmp44 and .Ltmp47
	.long	.Ltmp53-.Lfunc_begin0   #     jumps to .Ltmp53
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin0   #     jumps to .Ltmp50
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin0   #     jumps to .Ltmp53
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin0   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp62-.Ltmp59         #   Call between .Ltmp59 and .Ltmp62
	.long	.Ltmp68-.Lfunc_begin0   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin0   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin0   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin0   #     jumps to .Ltmp73
	.byte	0                       #   On action: cleanup
	.long	.Ltmp74-.Lfunc_begin0   # >> Call Site 20 <<
	.long	.Ltmp75-.Ltmp74         #   Call between .Ltmp74 and .Ltmp75
	.long	.Ltmp83-.Lfunc_begin0   #     jumps to .Ltmp83
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin0   # >> Call Site 21 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin0   #     jumps to .Ltmp78
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin0   # >> Call Site 22 <<
	.long	.Ltmp82-.Ltmp79         #   Call between .Ltmp79 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin0   #     jumps to .Ltmp83
	.byte	0                       #   On action: cleanup
	.long	.Ltmp92-.Lfunc_begin0   # >> Call Site 23 <<
	.long	.Ltmp93-.Ltmp92         #   Call between .Ltmp92 and .Ltmp93
	.long	.Ltmp94-.Lfunc_begin0   #     jumps to .Ltmp94
	.byte	0                       #   On action: cleanup
	.long	.Ltmp98-.Lfunc_begin0   # >> Call Site 24 <<
	.long	.Ltmp99-.Ltmp98         #   Call between .Ltmp98 and .Ltmp99
	.long	.Ltmp100-.Lfunc_begin0  #     jumps to .Ltmp100
	.byte	0                       #   On action: cleanup
	.long	.Ltmp110-.Lfunc_begin0  # >> Call Site 25 <<
	.long	.Ltmp111-.Ltmp110       #   Call between .Ltmp110 and .Ltmp111
	.long	.Ltmp112-.Lfunc_begin0  #     jumps to .Ltmp112
	.byte	0                       #   On action: cleanup
	.long	.Ltmp111-.Lfunc_begin0  # >> Call Site 26 <<
	.long	.Ltmp113-.Ltmp111       #   Call between .Ltmp111 and .Ltmp113
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp113-.Lfunc_begin0  # >> Call Site 27 <<
	.long	.Ltmp114-.Ltmp113       #   Call between .Ltmp113 and .Ltmp114
	.long	.Ltmp115-.Lfunc_begin0  #     jumps to .Ltmp115
	.byte	1                       #   On action: 1
	.long	.Ltmp95-.Lfunc_begin0   # >> Call Site 28 <<
	.long	.Ltmp96-.Ltmp95         #   Call between .Ltmp95 and .Ltmp96
	.long	.Ltmp97-.Lfunc_begin0   #     jumps to .Ltmp97
	.byte	1                       #   On action: 1
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 29 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 30 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 31 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	1                       #   On action: 1
	.long	.Ltmp69-.Lfunc_begin0   # >> Call Site 32 <<
	.long	.Ltmp25-.Ltmp69         #   Call between .Ltmp69 and .Ltmp25
	.long	.Ltmp109-.Lfunc_begin0  #     jumps to .Ltmp109
	.byte	1                       #   On action: 1
	.long	.Ltmp84-.Lfunc_begin0   # >> Call Site 33 <<
	.long	.Ltmp85-.Ltmp84         #   Call between .Ltmp84 and .Ltmp85
	.long	.Ltmp86-.Lfunc_begin0   #     jumps to .Ltmp86
	.byte	1                       #   On action: 1
	.long	.Ltmp90-.Lfunc_begin0   # >> Call Site 34 <<
	.long	.Ltmp91-.Ltmp90         #   Call between .Ltmp90 and .Ltmp91
	.long	.Ltmp109-.Lfunc_begin0  #     jumps to .Ltmp109
	.byte	1                       #   On action: 1
	.long	.Ltmp101-.Lfunc_begin0  # >> Call Site 35 <<
	.long	.Ltmp102-.Ltmp101       #   Call between .Ltmp101 and .Ltmp102
	.long	.Ltmp103-.Lfunc_begin0  #     jumps to .Ltmp103
	.byte	1                       #   On action: 1
	.long	.Ltmp107-.Lfunc_begin0  # >> Call Site 36 <<
	.long	.Ltmp108-.Ltmp107       #   Call between .Ltmp107 and .Ltmp108
	.long	.Ltmp109-.Lfunc_begin0  #     jumps to .Ltmp109
	.byte	1                       #   On action: 1
	.long	.Ltmp108-.Lfunc_begin0  # >> Call Site 37 <<
	.long	.Ltmp87-.Ltmp108        #   Call between .Ltmp108 and .Ltmp87
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp87-.Lfunc_begin0   # >> Call Site 38 <<
	.long	.Ltmp88-.Ltmp87         #   Call between .Ltmp87 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin0   #     jumps to .Ltmp89
	.byte	1                       #   On action: 1
	.long	.Ltmp104-.Lfunc_begin0  # >> Call Site 39 <<
	.long	.Ltmp105-.Ltmp104       #   Call between .Ltmp104 and .Ltmp105
	.long	.Ltmp106-.Lfunc_begin0  #     jumps to .Ltmp106
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.zero	16
	.text
	.globl	_ZN8NArchive3N7z8CHandler20SetCompressionMethodERNS0_22CCompressionMethodModeER13CObjectVectorINS_14COneMethodInfoEEj
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler20SetCompressionMethodERNS0_22CCompressionMethodModeER13CObjectVectorINS_14COneMethodInfoEEj,@function
_ZN8NArchive3N7z8CHandler20SetCompressionMethodERNS0_22CCompressionMethodModeER13CObjectVectorINS_14COneMethodInfoEEj: # @_ZN8NArchive3N7z8CHandler20SetCompressionMethodERNS0_22CCompressionMethodModeER13CObjectVectorINS_14COneMethodInfoEEj
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 192
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%ecx, 60(%rsp)          # 4-byte Spill
	movq	%rdx, %r15
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movl	12(%r15), %eax
	testl	%eax, %eax
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	jne	.LBB3_1
# BB#10:
	movl	84(%rdi), %ebx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rsp)
	movq	$8, 24(%rsp)
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rsp)
	movups	%xmm0, 32(%rsp)
.Ltmp116:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp117:
# BB#11:                                # %_ZN8NArchive14COneMethodInfoC2Ev.exit
	movq	%rbp, 32(%rsp)
	movl	$4, 44(%rsp)
	testl	%ebx, %ebx
	movl	$.L.str.2, %eax
	movl	$.L.str, %ebx
	cmoveq	%rax, %rbx
	movl	$0, 40(%rsp)
	movl	$0, (%rbp)
	movl	$-1, %r12d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB3_12:                               # =>This Inner Loop Header: Depth=1
	incl	%r12d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB3_12
# BB#13:                                # %_Z11MyStringLenIwEiPKT_.exit.i
	cmpl	$3, %r12d
	je	.LBB3_16
# BB#14:
	leal	1(%r12), %r13d
	movslq	%r13d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp128:
	callq	_Znam
	movq	%rax, %r14
.Ltmp129:
# BB#15:                                # %._crit_edge16.i.i
	movq	%rbp, %rdi
	callq	_ZdaPv
	movq	%r14, 32(%rsp)
	movl	$0, (%r14)
	movl	%r13d, 44(%rsp)
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB3_16:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rbp)
	addq	$4, %rbp
	testl	%eax, %eax
	jne	.LBB3_16
# BB#17:
	movl	%r12d, 40(%rsp)
.Ltmp130:
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp131:
# BB#18:                                # %.noexc54
.Ltmp132:
	movq	%rsp, %rsi
	movq	%rbp, %rdi
	callq	_ZN8NArchive14COneMethodInfoC2ERKS0_
.Ltmp133:
# BB#19:
.Ltmp135:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp136:
# BB#20:
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_22
# BB#21:
	callq	_ZdaPv
.LBB3_22:                               # %_ZN11CStringBaseIwED2Ev.exit.i
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rsp)
.Ltmp146:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp147:
# BB#23:                                # %_ZN8NArchive14COneMethodInfoD2Ev.exit
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	movl	12(%r15), %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
.LBB3_1:                                # %.preheader79
	testl	%eax, %eax
	jle	.LBB3_65
# BB#2:                                 # %.lr.ph96
	leaq	8(%rdi), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	88(%rsp), %r13
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_34:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_47 Depth 2
	movq	16(%r15), %rax
	movq	(%rax,%r12,8), %rbx
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	%rbx, %rsi
	movl	60(%rsp), %edx          # 4-byte Reload
	callq	_ZN8NArchive11COutHandler21SetCompressionMethod2ERNS_14COneMethodInfoEj
	leaq	32(%rbx), %rbp
	movq	32(%rbx), %rdi
	movl	$.L.str.2, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB3_36
# BB#35:                                #   in Loop: Header=BB3_34 Depth=1
	movb	$1, %al
	movq	%rax, 64(%rsp)          # 8-byte Spill
.LBB3_36:                               #   in Loop: Header=BB3_34 Depth=1
	leaq	96(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	$8, 112(%rsp)
	movq	$_ZTV13CObjectVectorI5CPropE+16, 88(%rsp)
.Ltmp152:
	movq	%rbp, %rdi
	leaq	80(%rsp), %rsi
	leaq	120(%rsp), %rdx
	leaq	124(%rsp), %rcx
	callq	_Z10FindMethodRK11CStringBaseIwERyRjS4_
.Ltmp153:
# BB#37:                                #   in Loop: Header=BB3_34 Depth=1
	movl	$1, %ebp
	testb	%al, %al
	je	.LBB3_59
# BB#38:                                #   in Loop: Header=BB3_34 Depth=1
.Ltmp154:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp155:
# BB#39:                                # %.noexc67
                                        #   in Loop: Header=BB3_34 Depth=1
.Ltmp156:
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	_ZN13CObjectVectorI5CPropEpLERKS1_
.Ltmp157:
# BB#40:                                # %_ZN13CObjectVectorI5CPropEaSERKS1_.exit
                                        #   in Loop: Header=BB3_34 Depth=1
.Ltmp158:
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp159:
# BB#41:                                # %.noexc69
                                        #   in Loop: Header=BB3_34 Depth=1
	movq	80(%rsp), %rax
	movq	%rax, (%r14)
	movq	%r14, %rbx
	addq	$8, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r14)
	movq	$8, 32(%r14)
	movq	$_ZTV13CObjectVectorI5CPropE+16, 8(%r14)
.Ltmp160:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp161:
# BB#42:                                # %.noexc.i.i.i
                                        #   in Loop: Header=BB3_34 Depth=1
.Ltmp162:
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	_ZN13CObjectVectorI5CPropEpLERKS1_
.Ltmp163:
# BB#43:                                #   in Loop: Header=BB3_34 Depth=1
	movq	120(%rsp), %rax
	movq	%rax, 40(%r14)
.Ltmp168:
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp169:
# BB#44:                                #   in Loop: Header=BB3_34 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movq	%r14, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rdx)
	xorl	%ebp, %ebp
	movq	48(%rsp), %rbx          # 8-byte Reload
	cmpb	$0, 72(%rbx)
	jne	.LBB3_59
# BB#45:                                #   in Loop: Header=BB3_34 Depth=1
	movl	100(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB3_59
# BB#46:                                # %.lr.ph
                                        #   in Loop: Header=BB3_34 Depth=1
	cltq
	movq	104(%rsp), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_47:                               #   Parent Loop BB3_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rdx,8), %rsi
	movl	(%rsi), %edi
	decl	%edi
	cmpl	$1, %edi
	ja	.LBB3_58
# BB#48:                                #   in Loop: Header=BB3_47 Depth=2
	movzwl	8(%rsi), %edi
	cmpl	$19, %edi
	je	.LBB3_49
.LBB3_58:                               #   in Loop: Header=BB3_47 Depth=2
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB3_47
	jmp	.LBB3_59
.LBB3_49:                               # %.critedge
                                        #   in Loop: Header=BB3_34 Depth=1
	movl	16(%rsi), %eax
	movq	%rax, %rcx
	shlq	$7, %rcx
	cmpq	$131072, %rax           # imm = 0x20000
	movl	$16777216, %eax         # imm = 0x1000000
	cmovbq	%rax, %rcx
	movq	%rcx, 64(%rbx)
	movb	$1, 72(%rbx)
	.p2align	4, 0x90
.LBB3_59:                               # %.loopexit
                                        #   in Loop: Header=BB3_34 Depth=1
	movq	$_ZTV13CObjectVectorI5CPropE+16, 88(%rsp)
.Ltmp180:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp181:
# BB#60:                                # %_ZN7CMethodD2Ev.exit76
                                        #   in Loop: Header=BB3_34 Depth=1
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	testl	%ebp, %ebp
	jne	.LBB3_61
# BB#33:                                #   in Loop: Header=BB3_34 Depth=1
	incq	%r12
	movslq	12(%r15), %rax
	cmpq	%rax, %r12
	jl	.LBB3_34
# BB#64:                                # %._crit_edge
	xorl	%eax, %eax
	testb	$1, 64(%rsp)            # 1-byte Folded Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	jne	.LBB3_67
.LBB3_65:                               # %._crit_edge.thread
	xorl	%eax, %eax
	cmpb	$0, 72(%rdi)
	jne	.LBB3_67
# BB#66:
	movb	$1, 72(%rdi)
	movq	$0, 64(%rdi)
	jmp	.LBB3_67
.LBB3_61:
	movl	$-2147024809, %eax      # imm = 0x80070057
.LBB3_67:                               # %.loopexit80
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_24:
.Ltmp148:
	movq	%rax, %r15
.Ltmp149:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp150:
	jmp	.LBB3_5
.LBB3_25:
.Ltmp151:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_70:
.Ltmp134:
	movq	%rax, %r15
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB3_27
.LBB3_3:
.Ltmp118:
	movq	%rax, %r15
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rsp)
.Ltmp119:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp120:
# BB#4:
.Ltmp125:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp126:
	jmp	.LBB3_5
.LBB3_8:
.Ltmp127:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB3_6:
.Ltmp121:
	movq	%rax, %rbx
.Ltmp122:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp123:
# BB#9:                                 # %.body.i
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB3_7:
.Ltmp124:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_26:
.Ltmp137:
	movq	%rax, %r15
.LBB3_27:                               # %.body
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_29
# BB#28:
	callq	_ZdaPv
.LBB3_29:                               # %_ZN11CStringBaseIwED2Ev.exit.i57
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rsp)
.Ltmp138:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp139:
# BB#30:                                # %_ZN13CObjectVectorI5CPropED2Ev.exit.i58
.Ltmp144:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp145:
	jmp	.LBB3_5
.LBB3_31:
.Ltmp140:
	movq	%rax, %rbx
.Ltmp141:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp142:
	jmp	.LBB3_69
.LBB3_32:
.Ltmp143:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_62:
.Ltmp182:
	movq	%rax, %r15
.Ltmp183:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp184:
	jmp	.LBB3_5
.LBB3_63:
.Ltmp185:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_56:
.Ltmp164:
	movq	%rax, %r15
.Ltmp165:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp166:
# BB#57:                                # %.body73
	movq	%r14, %rdi
	callq	_ZdlPv
	jmp	.LBB3_51
.LBB3_55:
.Ltmp167:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_50:
.Ltmp170:
	movq	%rax, %r15
.LBB3_51:                               # %.body71
	movq	$_ZTV13CObjectVectorI5CPropE+16, 88(%rsp)
.Ltmp171:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp172:
# BB#52:                                # %_ZN13CObjectVectorI5CPropED2Ev.exit.i63
.Ltmp177:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp178:
.LBB3_5:                                # %unwind_resume
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB3_53:
.Ltmp173:
	movq	%rax, %rbx
.Ltmp174:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp175:
	jmp	.LBB3_69
.LBB3_54:
.Ltmp176:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_68:
.Ltmp179:
	movq	%rax, %rbx
.LBB3_69:                               # %.body59
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN8NArchive3N7z8CHandler20SetCompressionMethodERNS0_22CCompressionMethodModeER13CObjectVectorINS_14COneMethodInfoEEj, .Lfunc_end3-_ZN8NArchive3N7z8CHandler20SetCompressionMethodERNS0_22CCompressionMethodModeER13CObjectVectorINS_14COneMethodInfoEEj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\301\202\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\270\002"              # Call site table length
	.long	.Ltmp116-.Lfunc_begin1  # >> Call Site 1 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp118-.Lfunc_begin1  #     jumps to .Ltmp118
	.byte	0                       #   On action: cleanup
	.long	.Ltmp128-.Lfunc_begin1  # >> Call Site 2 <<
	.long	.Ltmp131-.Ltmp128       #   Call between .Ltmp128 and .Ltmp131
	.long	.Ltmp137-.Lfunc_begin1  #     jumps to .Ltmp137
	.byte	0                       #   On action: cleanup
	.long	.Ltmp132-.Lfunc_begin1  # >> Call Site 3 <<
	.long	.Ltmp133-.Ltmp132       #   Call between .Ltmp132 and .Ltmp133
	.long	.Ltmp134-.Lfunc_begin1  #     jumps to .Ltmp134
	.byte	0                       #   On action: cleanup
	.long	.Ltmp135-.Lfunc_begin1  # >> Call Site 4 <<
	.long	.Ltmp136-.Ltmp135       #   Call between .Ltmp135 and .Ltmp136
	.long	.Ltmp137-.Lfunc_begin1  #     jumps to .Ltmp137
	.byte	0                       #   On action: cleanup
	.long	.Ltmp146-.Lfunc_begin1  # >> Call Site 5 <<
	.long	.Ltmp147-.Ltmp146       #   Call between .Ltmp146 and .Ltmp147
	.long	.Ltmp148-.Lfunc_begin1  #     jumps to .Ltmp148
	.byte	0                       #   On action: cleanup
	.long	.Ltmp147-.Lfunc_begin1  # >> Call Site 6 <<
	.long	.Ltmp152-.Ltmp147       #   Call between .Ltmp147 and .Ltmp152
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp152-.Lfunc_begin1  # >> Call Site 7 <<
	.long	.Ltmp159-.Ltmp152       #   Call between .Ltmp152 and .Ltmp159
	.long	.Ltmp170-.Lfunc_begin1  #     jumps to .Ltmp170
	.byte	0                       #   On action: cleanup
	.long	.Ltmp160-.Lfunc_begin1  # >> Call Site 8 <<
	.long	.Ltmp163-.Ltmp160       #   Call between .Ltmp160 and .Ltmp163
	.long	.Ltmp164-.Lfunc_begin1  #     jumps to .Ltmp164
	.byte	0                       #   On action: cleanup
	.long	.Ltmp168-.Lfunc_begin1  # >> Call Site 9 <<
	.long	.Ltmp169-.Ltmp168       #   Call between .Ltmp168 and .Ltmp169
	.long	.Ltmp170-.Lfunc_begin1  #     jumps to .Ltmp170
	.byte	0                       #   On action: cleanup
	.long	.Ltmp180-.Lfunc_begin1  # >> Call Site 10 <<
	.long	.Ltmp181-.Ltmp180       #   Call between .Ltmp180 and .Ltmp181
	.long	.Ltmp182-.Lfunc_begin1  #     jumps to .Ltmp182
	.byte	0                       #   On action: cleanup
	.long	.Ltmp181-.Lfunc_begin1  # >> Call Site 11 <<
	.long	.Ltmp149-.Ltmp181       #   Call between .Ltmp181 and .Ltmp149
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp149-.Lfunc_begin1  # >> Call Site 12 <<
	.long	.Ltmp150-.Ltmp149       #   Call between .Ltmp149 and .Ltmp150
	.long	.Ltmp151-.Lfunc_begin1  #     jumps to .Ltmp151
	.byte	1                       #   On action: 1
	.long	.Ltmp119-.Lfunc_begin1  # >> Call Site 13 <<
	.long	.Ltmp120-.Ltmp119       #   Call between .Ltmp119 and .Ltmp120
	.long	.Ltmp121-.Lfunc_begin1  #     jumps to .Ltmp121
	.byte	1                       #   On action: 1
	.long	.Ltmp125-.Lfunc_begin1  # >> Call Site 14 <<
	.long	.Ltmp126-.Ltmp125       #   Call between .Ltmp125 and .Ltmp126
	.long	.Ltmp127-.Lfunc_begin1  #     jumps to .Ltmp127
	.byte	1                       #   On action: 1
	.long	.Ltmp122-.Lfunc_begin1  # >> Call Site 15 <<
	.long	.Ltmp123-.Ltmp122       #   Call between .Ltmp122 and .Ltmp123
	.long	.Ltmp124-.Lfunc_begin1  #     jumps to .Ltmp124
	.byte	1                       #   On action: 1
	.long	.Ltmp138-.Lfunc_begin1  # >> Call Site 16 <<
	.long	.Ltmp139-.Ltmp138       #   Call between .Ltmp138 and .Ltmp139
	.long	.Ltmp140-.Lfunc_begin1  #     jumps to .Ltmp140
	.byte	1                       #   On action: 1
	.long	.Ltmp144-.Lfunc_begin1  # >> Call Site 17 <<
	.long	.Ltmp145-.Ltmp144       #   Call between .Ltmp144 and .Ltmp145
	.long	.Ltmp179-.Lfunc_begin1  #     jumps to .Ltmp179
	.byte	1                       #   On action: 1
	.long	.Ltmp141-.Lfunc_begin1  # >> Call Site 18 <<
	.long	.Ltmp142-.Ltmp141       #   Call between .Ltmp141 and .Ltmp142
	.long	.Ltmp143-.Lfunc_begin1  #     jumps to .Ltmp143
	.byte	1                       #   On action: 1
	.long	.Ltmp183-.Lfunc_begin1  # >> Call Site 19 <<
	.long	.Ltmp184-.Ltmp183       #   Call between .Ltmp183 and .Ltmp184
	.long	.Ltmp185-.Lfunc_begin1  #     jumps to .Ltmp185
	.byte	1                       #   On action: 1
	.long	.Ltmp165-.Lfunc_begin1  # >> Call Site 20 <<
	.long	.Ltmp166-.Ltmp165       #   Call between .Ltmp165 and .Ltmp166
	.long	.Ltmp167-.Lfunc_begin1  #     jumps to .Ltmp167
	.byte	1                       #   On action: 1
	.long	.Ltmp171-.Lfunc_begin1  # >> Call Site 21 <<
	.long	.Ltmp172-.Ltmp171       #   Call between .Ltmp171 and .Ltmp172
	.long	.Ltmp173-.Lfunc_begin1  #     jumps to .Ltmp173
	.byte	1                       #   On action: 1
	.long	.Ltmp177-.Lfunc_begin1  # >> Call Site 22 <<
	.long	.Ltmp178-.Ltmp177       #   Call between .Ltmp177 and .Ltmp178
	.long	.Ltmp179-.Lfunc_begin1  #     jumps to .Ltmp179
	.byte	1                       #   On action: 1
	.long	.Ltmp178-.Lfunc_begin1  # >> Call Site 23 <<
	.long	.Ltmp174-.Ltmp178       #   Call between .Ltmp178 and .Ltmp174
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp174-.Lfunc_begin1  # >> Call Site 24 <<
	.long	.Ltmp175-.Ltmp174       #   Call between .Ltmp174 and .Ltmp175
	.long	.Ltmp176-.Lfunc_begin1  #     jumps to .Ltmp176
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end4:
	.size	__clang_call_terminate, .Lfunc_end4-__clang_call_terminate

	.section	.text._ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev,@function
_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev: # @_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, (%rbx)
.Ltmp186:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp187:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB5_2:
.Ltmp188:
	movq	%rax, %r14
.Ltmp189:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp190:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_4:
.Ltmp191:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev, .Lfunc_end5-_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp186-.Lfunc_begin2  # >> Call Site 1 <<
	.long	.Ltmp187-.Ltmp186       #   Call between .Ltmp186 and .Ltmp187
	.long	.Ltmp188-.Lfunc_begin2  #     jumps to .Ltmp188
	.byte	0                       #   On action: cleanup
	.long	.Ltmp187-.Lfunc_begin2  # >> Call Site 2 <<
	.long	.Ltmp189-.Ltmp187       #   Call between .Ltmp187 and .Ltmp189
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp189-.Lfunc_begin2  # >> Call Site 3 <<
	.long	.Ltmp190-.Ltmp189       #   Call between .Ltmp189 and .Ltmp190
	.long	.Ltmp191-.Lfunc_begin2  #     jumps to .Ltmp191
	.byte	1                       #   On action: 1
	.long	.Ltmp190-.Lfunc_begin2  # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp190    #   Call between .Ltmp190 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.zero	16
	.text
	.globl	_ZN8NArchive3N7z8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback,@function
_ZN8NArchive3N7z8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback: # @_ZN8NArchive3N7z8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$1176, %rsp             # imm = 0x498
.Lcfi37:
	.cfi_def_cfa_offset 1232
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rsi, 464(%rsp)         # 8-byte Spill
	movq	136(%rdi), %rax
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	leaq	144(%rdi), %r12
	testq	%rax, %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	cmoveq	%rax, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 152(%rsp)
	movq	$8, 168(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CUpdateItemEE+16, 144(%rsp)
	movl	%edx, 108(%rsp)         # 4-byte Spill
	testl	%edx, %edx
	je	.LBB6_147
# BB#1:                                 # %.lr.ph
	xorl	%r14d, %r14d
                                        # implicit-def: %R13D
                                        # implicit-def: %AL
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movq	%r12, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB6_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_16 Depth 2
                                        #     Child Loop BB6_85 Depth 2
                                        #     Child Loop BB6_88 Depth 2
                                        #     Child Loop BB6_99 Depth 2
                                        #     Child Loop BB6_138 Depth 2
	testq	%r15, %r15
	movl	$1, %ebx
	je	.LBB6_6
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	(%r15), %rax
.Ltmp192:
.Lcfi44:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	movl	%r14d, %esi
	leaq	188(%rsp), %rdx
	leaq	184(%rsp), %rcx
	leaq	180(%rsp), %r8
	callq	*56(%rax)
.Ltmp193:
# BB#4:                                 #   in Loop: Header=BB6_2 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %r13d
	je	.LBB6_7
# BB#5:                                 #   in Loop: Header=BB6_2 Depth=1
	movl	%eax, %r13d
	testl	%ebx, %ebx
	je	.LBB6_146
	jmp	.LBB6_175
	.p2align	4, 0x90
.LBB6_6:                                #   in Loop: Header=BB6_2 Depth=1
	movl	$-2147467259, %r13d     # imm = 0x80004005
	testl	%ebx, %ebx
	je	.LBB6_146
	jmp	.LBB6_175
	.p2align	4, 0x90
.LBB6_7:                                #   in Loop: Header=BB6_2 Depth=1
	xorps	%xmm0, %xmm0
	leaq	72(%rsp), %rax
	movups	%xmm0, (%rax)
.Ltmp194:
.Lcfi45:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp195:
# BB#8:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	%rbp, 72(%rsp)
	movl	$0, (%rbp)
	movl	$4, 84(%rsp)
	leaq	72(%rsp), %rax
	movw	$0, 26(%rax)
	movl	$0, 22(%rax)
	cmpl	$0, 184(%rsp)
	setne	%cl
	setne	93(%rsp)
	cmpl	$0, 188(%rsp)
	setne	92(%rsp)
	movslq	180(%rsp), %rax
	cmpq	$-1, %rax
	movl	%eax, 32(%rsp)
	movl	%r14d, 36(%rsp)
	movb	$0, 94(%rsp)
	movq	$0, 64(%rsp)
	je	.LBB6_36
# BB#9:                                 #   in Loop: Header=BB6_2 Depth=1
	movl	$1, %ebx
	cmpq	$0, 136(%rsp)           # 8-byte Folded Reload
	je	.LBB6_21
# BB#10:                                #   in Loop: Header=BB6_2 Depth=1
	cmpl	172(%r12), %eax
	jge	.LBB6_21
# BB#11:                                #   in Loop: Header=BB6_2 Depth=1
	movq	%r15, (%rsp)            # 8-byte Spill
	movq	176(%r12), %rcx
	movq	(%rcx,%rax,8), %r15
	leaq	16(%r15), %rdx
	leaq	72(%rsp), %rcx
	cmpq	%rcx, %rdx
	je	.LBB6_18
# BB#12:                                #   in Loop: Header=BB6_2 Depth=1
	movl	$0, 80(%rsp)
	movl	$0, (%rbp)
	movslq	24(%r15), %rbx
	incq	%rbx
	cmpl	$4, %ebx
	je	.LBB6_15
# BB#13:                                #   in Loop: Header=BB6_2 Depth=1
	movq	%rdx, 456(%rsp)         # 8-byte Spill
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp197:
.Lcfi46:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
.Ltmp198:
# BB#14:                                # %._crit_edge16.i.i
                                        #   in Loop: Header=BB6_2 Depth=1
.Lcfi47:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdaPv
	movslq	80(%rsp), %rax
	movq	%r12, 72(%rsp)
	movl	$0, (%r12,%rax,4)
	movl	%ebx, 84(%rsp)
	movq	%r12, %rbp
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	456(%rsp), %rdx         # 8-byte Reload
.LBB6_15:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	(%rdx), %rax
	.p2align	4, 0x90
.LBB6_16:                               #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbp)
	addq	$4, %rbp
	testl	%ecx, %ecx
	jne	.LBB6_16
# BB#17:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	24(%r15), %eax
	movl	%eax, 80(%rsp)
	movl	32(%rsp), %eax
.LBB6_18:                               # %_ZN11CStringBaseIwEaSERKS0_.exit
                                        #   in Loop: Header=BB6_2 Depth=1
	movb	33(%r15), %cl
	movb	%cl, 95(%rsp)
	movq	(%r15), %rcx
	movq	%rcx, 64(%rsp)
	cmpl	%eax, 460(%r12)
	jle	.LBB6_22
# BB#19:                                #   in Loop: Header=BB6_2 Depth=1
	movq	464(%r12), %rcx
	movslq	%eax, %rdx
	cmpb	$0, (%rcx,%rdx)
	setne	%cl
	jmp	.LBB6_23
.LBB6_21:                               #   in Loop: Header=BB6_2 Depth=1
	movl	$-2147024809, %r13d     # imm = 0x80070057
	jmp	.LBB6_143
.LBB6_22:                               #   in Loop: Header=BB6_2 Depth=1
	xorl	%ecx, %ecx
.LBB6_23:                               # %_ZNK8NArchive3N7z16CArchiveDatabase10IsItemAntiEi.exit
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	(%rsp), %r15            # 8-byte Reload
	movb	%cl, 94(%rsp)
	cmpl	%eax, 236(%r12)
	jle	.LBB6_26
# BB#24:                                #   in Loop: Header=BB6_2 Depth=1
	movq	240(%r12), %rdx
	movslq	%eax, %rcx
	cmpb	$0, (%rdx,%rcx)
	je	.LBB6_26
# BB#25:                                #   in Loop: Header=BB6_2 Depth=1
	movq	208(%r12), %rdx
	movq	(%rdx,%rcx,8), %rcx
	movb	$1, %dl
	jmp	.LBB6_27
.LBB6_26:                               #   in Loop: Header=BB6_2 Depth=1
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.LBB6_27:                               #   in Loop: Header=BB6_2 Depth=1
	movq	%rcx, 40(%rsp)
	movb	%dl, 97(%rsp)
	cmpl	%eax, 300(%r12)
	jle	.LBB6_30
# BB#28:                                #   in Loop: Header=BB6_2 Depth=1
	movq	304(%r12), %rdx
	movslq	%eax, %rcx
	cmpb	$0, (%rdx,%rcx)
	je	.LBB6_30
# BB#29:                                #   in Loop: Header=BB6_2 Depth=1
	movq	272(%r12), %rdx
	movq	(%rdx,%rcx,8), %rcx
	movb	$1, %dl
	jmp	.LBB6_31
.LBB6_30:                               #   in Loop: Header=BB6_2 Depth=1
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.LBB6_31:                               #   in Loop: Header=BB6_2 Depth=1
	movq	%rcx, 48(%rsp)
	movb	%dl, 98(%rsp)
	cmpl	%eax, 364(%r12)
	jle	.LBB6_34
# BB#32:                                #   in Loop: Header=BB6_2 Depth=1
	movq	368(%r12), %rcx
	cltq
	cmpb	$0, (%rcx,%rax)
	je	.LBB6_34
# BB#33:                                #   in Loop: Header=BB6_2 Depth=1
	movq	336(%r12), %rcx
	movq	(%rcx,%rax,8), %rax
	movb	$1, %cl
	jmp	.LBB6_35
.LBB6_34:                               #   in Loop: Header=BB6_2 Depth=1
	xorl	%eax, %eax
	xorl	%ecx, %ecx
.LBB6_35:                               #   in Loop: Header=BB6_2 Depth=1
	movq	%rax, 56(%rsp)
	movb	%cl, 99(%rsp)
	movb	93(%rsp), %cl
.LBB6_36:                               #   in Loop: Header=BB6_2 Depth=1
	testb	%cl, %cl
	je	.LBB6_46
# BB#37:                                #   in Loop: Header=BB6_2 Depth=1
	movl	$0, 336(%rsp)
	movq	(%r15), %rax
.Ltmp200:
.Lcfi48:
	.cfi_escape 0x2e, 0x00
	movl	$9, %edx
	movq	%r15, %rdi
	movl	%r14d, %esi
	leaq	336(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %r12d
.Ltmp201:
# BB#38:                                #   in Loop: Header=BB6_2 Depth=1
	testl	%r12d, %r12d
	cmovnel	%r12d, %r13d
	movl	$1, %ebp
	jne	.LBB6_43
# BB#39:                                #   in Loop: Header=BB6_2 Depth=1
	xorl	%eax, %eax
	movzwl	336(%rsp), %ecx
	testw	%cx, %cx
	je	.LBB6_42
# BB#40:                                #   in Loop: Header=BB6_2 Depth=1
	movl	$-2147024809, %r12d     # imm = 0x80070057
	movzwl	%cx, %eax
	cmpl	$19, %eax
	jne	.LBB6_43
# BB#41:                                #   in Loop: Header=BB6_2 Depth=1
	movl	344(%rsp), %eax
	movl	%eax, 88(%rsp)
	movb	$1, %al
.LBB6_42:                               #   in Loop: Header=BB6_2 Depth=1
	movb	%al, 96(%rsp)
	xorl	%ebp, %ebp
	movl	%r13d, %r12d
.LBB6_43:                               #   in Loop: Header=BB6_2 Depth=1
.Ltmp205:
.Lcfi49:
	.cfi_escape 0x2e, 0x00
	leaq	336(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp206:
# BB#44:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit274
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	$1, %ebx
	testl	%ebp, %ebp
	je	.LBB6_54
# BB#45:                                #   in Loop: Header=BB6_2 Depth=1
	movl	%r12d, %r13d
	jmp	.LBB6_142
.LBB6_46:                               #   in Loop: Header=BB6_2 Depth=1
	movq	%r15, %r12
	movl	%r13d, %r15d
.LBB6_47:                               # %.thread371
                                        #   in Loop: Header=BB6_2 Depth=1
	cmpb	$0, 92(%rsp)
	je	.LBB6_131
.LBB6_48:                               #   in Loop: Header=BB6_2 Depth=1
	movl	$0, 288(%rsp)
	movq	%r12, %rdi
	movq	(%rdi), %rax
.Ltmp266:
.Lcfi50:
	.cfi_escape 0x2e, 0x00
	movl	$7, %edx
	movl	%r14d, %esi
	leaq	288(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %r13d
.Ltmp267:
# BB#49:                                #   in Loop: Header=BB6_2 Depth=1
	testl	%r13d, %r13d
	cmovnel	%r13d, %r15d
	movl	$1, %ebp
	jne	.LBB6_52
# BB#50:                                #   in Loop: Header=BB6_2 Depth=1
	movl	$-2147024809, %r13d     # imm = 0x80070057
	movzwl	288(%rsp), %eax
	cmpl	$21, %eax
	jne	.LBB6_52
# BB#51:                                #   in Loop: Header=BB6_2 Depth=1
	movq	296(%rsp), %rax
	movq	%rax, 64(%rsp)
	testq	%rax, %rax
	sete	%al
	setne	%cl
	cmpb	$0, 94(%rsp)
	sete	%dl
	setne	%bl
	andb	%cl, %bl
	movzbl	%bl, %ebp
	orb	%al, %dl
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmovel	%eax, %r15d
	movl	%r15d, %r13d
.LBB6_52:                               #   in Loop: Header=BB6_2 Depth=1
.Ltmp271:
.Lcfi51:
	.cfi_escape 0x2e, 0x00
	leaq	288(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp272:
# BB#53:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit326
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	$1, %ebx
	testl	%ebp, %ebp
	jne	.LBB6_141
	jmp	.LBB6_132
.LBB6_54:                               #   in Loop: Header=BB6_2 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpb	$0, 77(%rax)
	movq	$0, 40(%rsp)
	movb	$0, 97(%rsp)
	je	.LBB6_63
# BB#55:                                #   in Loop: Header=BB6_2 Depth=1
	movl	$0, 192(%rsp)
	movq	(%r15), %rax
.Ltmp208:
.Lcfi52:
	.cfi_escape 0x2e, 0x00
	movl	$10, %edx
	movq	%r15, %rdi
	movl	%r14d, %esi
	leaq	192(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %r13d
.Ltmp209:
# BB#56:                                #   in Loop: Header=BB6_2 Depth=1
	testl	%r13d, %r13d
	jne	.LBB6_61
# BB#57:                                #   in Loop: Header=BB6_2 Depth=1
	movzwl	192(%rsp), %eax
	testw	%ax, %ax
	je	.LBB6_60
# BB#58:                                #   in Loop: Header=BB6_2 Depth=1
	movl	$-2147024809, %r13d     # imm = 0x80070057
	movzwl	%ax, %eax
	cmpl	$64, %eax
	jne	.LBB6_61
# BB#59:                                #   in Loop: Header=BB6_2 Depth=1
	movl	200(%rsp), %eax
	movl	204(%rsp), %ecx
	shlq	$32, %rcx
	orq	%rax, %rcx
	movq	%rcx, 40(%rsp)
	movb	$1, 97(%rsp)
.LBB6_60:                               #   in Loop: Header=BB6_2 Depth=1
	xorl	%r13d, %r13d
.LBB6_61:                               #   in Loop: Header=BB6_2 Depth=1
.Ltmp214:
.Lcfi53:
	.cfi_escape 0x2e, 0x00
	leaq	192(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp215:
# BB#62:                                # %_ZN8NArchive3N7zL7GetTimeEP22IArchiveUpdateCallbackibjRyRb.exit
                                        #   in Loop: Header=BB6_2 Depth=1
	testl	%r13d, %r13d
	jne	.LBB6_142
.LBB6_63:                               # %_ZN8NArchive3N7zL7GetTimeEP22IArchiveUpdateCallbackibjRyRb.exit.thread
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpb	$0, 78(%rax)
	movq	$0, 48(%rsp)
	movb	$0, 98(%rsp)
	je	.LBB6_72
# BB#64:                                #   in Loop: Header=BB6_2 Depth=1
	movl	$0, 208(%rsp)
	movq	(%r15), %rax
.Ltmp216:
.Lcfi54:
	.cfi_escape 0x2e, 0x00
	movl	$11, %edx
	movq	%r15, %rdi
	movl	%r14d, %esi
	leaq	208(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %r13d
.Ltmp217:
# BB#65:                                #   in Loop: Header=BB6_2 Depth=1
	testl	%r13d, %r13d
	jne	.LBB6_70
# BB#66:                                #   in Loop: Header=BB6_2 Depth=1
	movzwl	208(%rsp), %eax
	testw	%ax, %ax
	je	.LBB6_69
# BB#67:                                #   in Loop: Header=BB6_2 Depth=1
	movl	$-2147024809, %r13d     # imm = 0x80070057
	movzwl	%ax, %eax
	cmpl	$64, %eax
	jne	.LBB6_70
# BB#68:                                #   in Loop: Header=BB6_2 Depth=1
	movl	216(%rsp), %eax
	movl	220(%rsp), %ecx
	shlq	$32, %rcx
	orq	%rax, %rcx
	movq	%rcx, 48(%rsp)
	movb	$1, 98(%rsp)
.LBB6_69:                               #   in Loop: Header=BB6_2 Depth=1
	xorl	%r13d, %r13d
.LBB6_70:                               #   in Loop: Header=BB6_2 Depth=1
.Ltmp222:
.Lcfi55:
	.cfi_escape 0x2e, 0x00
	leaq	208(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp223:
# BB#71:                                # %_ZN8NArchive3N7zL7GetTimeEP22IArchiveUpdateCallbackibjRyRb.exit284
                                        #   in Loop: Header=BB6_2 Depth=1
	testl	%r13d, %r13d
	jne	.LBB6_142
.LBB6_72:                               # %_ZN8NArchive3N7zL7GetTimeEP22IArchiveUpdateCallbackibjRyRb.exit284.thread
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	$0, 56(%rsp)
	movb	$0, 99(%rsp)
	movl	$0, 224(%rsp)
	movq	(%r15), %rax
.Ltmp224:
.Lcfi56:
	.cfi_escape 0x2e, 0x00
	movl	$12, %edx
	movq	%r15, %rdi
	movl	%r14d, %esi
	leaq	224(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %r13d
.Ltmp225:
# BB#73:                                #   in Loop: Header=BB6_2 Depth=1
	testl	%r13d, %r13d
	jne	.LBB6_78
# BB#74:                                #   in Loop: Header=BB6_2 Depth=1
	movzwl	224(%rsp), %eax
	testw	%ax, %ax
	je	.LBB6_77
# BB#75:                                #   in Loop: Header=BB6_2 Depth=1
	movl	$-2147024809, %r13d     # imm = 0x80070057
	movzwl	%ax, %eax
	cmpl	$64, %eax
	jne	.LBB6_78
# BB#76:                                #   in Loop: Header=BB6_2 Depth=1
	movl	232(%rsp), %eax
	movl	236(%rsp), %ecx
	shlq	$32, %rcx
	orq	%rax, %rcx
	movq	%rcx, 56(%rsp)
	movb	$1, 99(%rsp)
.LBB6_77:                               #   in Loop: Header=BB6_2 Depth=1
	xorl	%r13d, %r13d
.LBB6_78:                               #   in Loop: Header=BB6_2 Depth=1
.Ltmp230:
.Lcfi57:
	.cfi_escape 0x2e, 0x00
	leaq	224(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp231:
# BB#79:                                #   in Loop: Header=BB6_2 Depth=1
	testl	%r13d, %r13d
	cmovnel	%r13d, %r12d
	jne	.LBB6_142
# BB#80:                                #   in Loop: Header=BB6_2 Depth=1
	movl	$0, 320(%rsp)
	movq	(%r15), %rax
.Ltmp233:
.Lcfi58:
	.cfi_escape 0x2e, 0x00
	movl	$3, %edx
	movq	%r15, %rdi
	movl	%r14d, %esi
	leaq	320(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %r13d
.Ltmp234:
# BB#81:                                #   in Loop: Header=BB6_2 Depth=1
	testl	%r13d, %r13d
	cmovnel	%r13d, %r12d
	movl	$1, %ebp
	jne	.LBB6_105
# BB#82:                                #   in Loop: Header=BB6_2 Depth=1
	movzwl	320(%rsp), %eax
	testw	%ax, %ax
	je	.LBB6_104
# BB#83:                                #   in Loop: Header=BB6_2 Depth=1
	movl	$-2147024809, %r13d     # imm = 0x80070057
	movzwl	%ax, %eax
	cmpl	$8, %eax
	jne	.LBB6_105
# BB#84:                                #   in Loop: Header=BB6_2 Depth=1
	movq	%r15, (%rsp)            # 8-byte Spill
	movq	328(%rsp), %rbx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 1040(%rsp)
	movl	$-1, %ebp
	movq	%rbx, %rax
.LBB6_85:                               #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB6_85
# BB#86:                                # %_Z11MyStringLenIwEiPKT_.exit.i
                                        #   in Loop: Header=BB6_2 Depth=1
	leal	1(%rbp), %r15d
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp236:
.Lcfi59:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
.Ltmp237:
# BB#87:                                # %.noexc296
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	%rax, 1040(%rsp)
	movl	$0, (%rax)
	movl	%r15d, 1052(%rsp)
.LBB6_88:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i294
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB6_88
# BB#89:                                #   in Loop: Header=BB6_2 Depth=1
	movl	%ebp, 1048(%rsp)
.Ltmp239:
.Lcfi60:
	.cfi_escape 0x2e, 0x00
	leaq	560(%rsp), %rdi
	leaq	1040(%rsp), %rsi
	callq	_ZN8NArchive9NItemName13MakeLegalNameERK11CStringBaseIwE
.Ltmp240:
# BB#90:                                #   in Loop: Header=BB6_2 Depth=1
	movl	$0, 80(%rsp)
	movq	72(%rsp), %rbp
	movl	$0, (%rbp)
	movslq	568(%rsp), %r13
	incq	%r13
	movl	84(%rsp), %r15d
	cmpl	%r15d, %r13d
	jne	.LBB6_92
# BB#91:                                #   in Loop: Header=BB6_2 Depth=1
	movq	(%rsp), %r15            # 8-byte Reload
	jmp	.LBB6_98
.LBB6_92:                               #   in Loop: Header=BB6_2 Depth=1
	movq	%r13, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp242:
.Lcfi61:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp243:
# BB#93:                                # %.noexc306
                                        #   in Loop: Header=BB6_2 Depth=1
	testq	%rbp, %rbp
	je	.LBB6_96
# BB#94:                                # %.noexc306
                                        #   in Loop: Header=BB6_2 Depth=1
	testl	%r15d, %r15d
	movl	$0, %eax
	movq	(%rsp), %r15            # 8-byte Reload
	jle	.LBB6_97
# BB#95:                                # %._crit_edge.thread.i.i300
                                        #   in Loop: Header=BB6_2 Depth=1
.Lcfi62:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdaPv
	movslq	80(%rsp), %rax
	jmp	.LBB6_97
.LBB6_96:                               #   in Loop: Header=BB6_2 Depth=1
	xorl	%eax, %eax
	movq	(%rsp), %r15            # 8-byte Reload
.LBB6_97:                               # %._crit_edge16.i.i301
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	%rbx, 72(%rsp)
	movl	$0, (%rbx,%rax,4)
	movl	%r13d, 84(%rsp)
	movq	%rbx, %rbp
.LBB6_98:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i302
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	560(%rsp), %rdi
	xorl	%eax, %eax
.LBB6_99:                               #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB6_99
# BB#100:                               #   in Loop: Header=BB6_2 Depth=1
	movl	568(%rsp), %eax
	movl	%eax, 80(%rsp)
	testq	%rdi, %rdi
	je	.LBB6_102
# BB#101:                               #   in Loop: Header=BB6_2 Depth=1
.Lcfi63:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB6_102:                              # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	1040(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_104
# BB#103:                               #   in Loop: Header=BB6_2 Depth=1
.Lcfi64:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB6_104:                              #   in Loop: Header=BB6_2 Depth=1
	xorl	%ebp, %ebp
	movl	%r12d, %r13d
.LBB6_105:                              #   in Loop: Header=BB6_2 Depth=1
.Ltmp247:
.Lcfi65:
	.cfi_escape 0x2e, 0x00
	leaq	320(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp248:
# BB#106:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit312
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	$1, %ebx
	testl	%ebp, %ebp
	jne	.LBB6_142
# BB#107:                               #   in Loop: Header=BB6_2 Depth=1
	movl	$0, 352(%rsp)
	movq	(%r15), %rax
.Ltmp250:
.Lcfi66:
	.cfi_escape 0x2e, 0x00
	movl	$6, %edx
	movq	%r15, %rdi
	movl	%r14d, %esi
	leaq	352(%rsp), %rbx
	movq	%rbx, %rcx
	callq	*64(%rax)
	movl	%eax, %ebp
.Ltmp251:
# BB#108:                               #   in Loop: Header=BB6_2 Depth=1
	testl	%ebp, %ebp
	cmovnel	%ebp, %r13d
	movl	$1, %r12d
	jne	.LBB6_114
# BB#109:                               #   in Loop: Header=BB6_2 Depth=1
	movzwl	352(%rsp), %eax
	testw	%ax, %ax
	je	.LBB6_112
# BB#110:                               #   in Loop: Header=BB6_2 Depth=1
	movl	$-2147024809, %ebp      # imm = 0x80070057
	movzwl	%ax, %eax
	cmpl	$11, %eax
	jne	.LBB6_114
# BB#111:                               #   in Loop: Header=BB6_2 Depth=1
	cmpw	$0, 360(%rsp)
	setne	95(%rsp)
	movb	$1, %al
	movl	%eax, 28(%rsp)          # 4-byte Spill
	jmp	.LBB6_113
.LBB6_112:                              #   in Loop: Header=BB6_2 Depth=1
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
.LBB6_113:                              #   in Loop: Header=BB6_2 Depth=1
	xorl	%r12d, %r12d
	movl	%r13d, %ebp
.LBB6_114:                              #   in Loop: Header=BB6_2 Depth=1
.Ltmp255:
.Lcfi67:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp256:
# BB#115:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit318
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	$1, %ebx
	testl	%r12d, %r12d
	je	.LBB6_117
# BB#116:                               #   in Loop: Header=BB6_2 Depth=1
	movl	%ebp, %r13d
	jmp	.LBB6_142
.LBB6_117:                              #   in Loop: Header=BB6_2 Depth=1
	movl	$0, 304(%rsp)
	movq	(%r15), %rax
.Ltmp258:
.Lcfi68:
	.cfi_escape 0x2e, 0x00
	movl	$21, %edx
	movq	%r15, %r12
	movq	%r15, %rdi
	movl	%r14d, %esi
	leaq	304(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %r15d
.Ltmp259:
# BB#118:                               #   in Loop: Header=BB6_2 Depth=1
	testl	%r15d, %r15d
	cmovnel	%r15d, %ebp
	movl	$1, %r13d
	jne	.LBB6_123
# BB#119:                               #   in Loop: Header=BB6_2 Depth=1
	xorl	%eax, %eax
	movzwl	304(%rsp), %ecx
	testw	%cx, %cx
	je	.LBB6_122
# BB#120:                               #   in Loop: Header=BB6_2 Depth=1
	movl	$-2147024809, %r15d     # imm = 0x80070057
	movzwl	%cx, %eax
	cmpl	$11, %eax
	jne	.LBB6_123
# BB#121:                               #   in Loop: Header=BB6_2 Depth=1
	cmpw	$0, 312(%rsp)
	setne	%al
.LBB6_122:                              #   in Loop: Header=BB6_2 Depth=1
	movb	%al, 94(%rsp)
	xorl	%r13d, %r13d
	movl	%ebp, %r15d
.LBB6_123:                              #   in Loop: Header=BB6_2 Depth=1
.Ltmp263:
.Lcfi69:
	.cfi_escape 0x2e, 0x00
	leaq	304(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp264:
# BB#124:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit322
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	$1, %ebx
	testl	%r13d, %r13d
	je	.LBB6_126
# BB#125:                               #   in Loop: Header=BB6_2 Depth=1
	movl	%r15d, %r13d
	jmp	.LBB6_141
.LBB6_126:                              #   in Loop: Header=BB6_2 Depth=1
	cmpb	$0, 94(%rsp)
	je	.LBB6_128
# BB#127:                               # %.thread552
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	$0, 64(%rsp)
	movl	$0, 96(%rsp)
	cmpb	$0, 92(%rsp)
	jne	.LBB6_48
	jmp	.LBB6_131
.LBB6_128:                              #   in Loop: Header=BB6_2 Depth=1
	testb	$1, 28(%rsp)            # 1-byte Folded Reload
	jne	.LBB6_47
# BB#129:                               #   in Loop: Header=BB6_2 Depth=1
	movb	96(%rsp), %al
	testb	%al, %al
	je	.LBB6_47
# BB#130:                               #   in Loop: Header=BB6_2 Depth=1
	movb	88(%rsp), %al
	shrb	$4, %al
	andb	$1, %al
	movb	%al, 95(%rsp)
	cmpb	$0, 92(%rsp)
	jne	.LBB6_48
.LBB6_131:                              #   in Loop: Header=BB6_2 Depth=1
	movl	%r15d, %r13d
.LBB6_132:                              #   in Loop: Header=BB6_2 Depth=1
.Ltmp274:
.Lcfi70:
	.cfi_escape 0x2e, 0x00
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp275:
# BB#133:                               # %.noexc327
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	64(%rsp), %rax
	movq	%rax, 32(%rbp)
	movups	32(%rsp), %xmm0
	movups	48(%rsp), %xmm1
	movups	%xmm1, 16(%rbp)
	movups	%xmm0, (%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%rbp)
	movslq	80(%rsp), %r15
	leaq	1(%r15), %rbx
	testl	%ebx, %ebx
	je	.LBB6_136
# BB#134:                               # %._crit_edge16.i.i.i.i
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp276:
.Lcfi71:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
.Ltmp277:
# BB#135:                               # %.noexc.i
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	%rax, 40(%rbp)
	movl	$0, (%rax)
	movl	%ebx, 52(%rbp)
	jmp	.LBB6_137
.LBB6_136:                              #   in Loop: Header=BB6_2 Depth=1
	xorl	%eax, %eax
.LBB6_137:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i.i
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	72(%rsp), %rcx
	.p2align	4, 0x90
.LBB6_138:                              #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB6_138
# BB#139:                               #   in Loop: Header=BB6_2 Depth=1
	movl	%r15d, 48(%rbp)
	leaq	72(%rsp), %rax
	movq	%rax, %rcx
	movl	24(%rcx), %eax
	movl	%eax, 64(%rbp)
	movq	16(%rcx), %rax
	movq	%rax, 56(%rbp)
.Ltmp279:
.Lcfi72:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp280:
# BB#140:                               # %_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEE3AddERKS2_.exit
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	160(%rsp), %rax
	movslq	156(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 156(%rsp)
	xorl	%ebx, %ebx
.LBB6_141:                              # %.thread
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	%r12, %r15
.LBB6_142:                              # %.thread
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	8(%rsp), %r12           # 8-byte Reload
.LBB6_143:                              # %.thread
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_145
# BB#144:                               #   in Loop: Header=BB6_2 Depth=1
.Lcfi73:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	.p2align	4, 0x90
.LBB6_145:                              #   in Loop: Header=BB6_2 Depth=1
	testl	%ebx, %ebx
	jne	.LBB6_175
.LBB6_146:                              #   in Loop: Header=BB6_2 Depth=1
	incl	%r14d
	cmpl	108(%rsp), %r14d        # 4-byte Folded Reload
	jb	.LBB6_2
	jmp	.LBB6_148
.LBB6_147:
                                        # implicit-def: %R13D
.LBB6_148:                              # %._crit_edge
.Ltmp282:
.Lcfi74:
	.cfi_escape 0x2e, 0x00
	leaq	368(%rsp), %rdi
	callq	_ZN8NArchive3N7z22CCompressionMethodModeC2Ev
.Ltmp283:
# BB#149:
.Ltmp285:
.Lcfi75:
	.cfi_escape 0x2e, 0x00
	leaq	472(%rsp), %rdi
	callq	_ZN8NArchive3N7z22CCompressionMethodModeC2Ev
.Ltmp286:
	movq	16(%rsp), %rbx          # 8-byte Reload
# BB#150:
.Ltmp288:
.Lcfi76:
	.cfi_escape 0x2e, 0x00
	leaq	368(%rsp), %rsi
	leaq	472(%rsp), %r14
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	_ZN8NArchive3N7z8CHandler20SetCompressionMethodERNS0_22CCompressionMethodModeES3_
	movl	%eax, %ebp
.Ltmp289:
# BB#151:
	testl	%ebp, %ebp
	cmovnel	%ebp, %r13d
	jne	.LBB6_195
# BB#152:
	movl	8(%rbx), %eax
	movl	%eax, 432(%rsp)
	movl	$1, 536(%rsp)
	movq	$0, 128(%rsp)
	movq	(%r15), %rax
.Ltmp291:
.Lcfi77:
	.cfi_escape 0x2e, 0x00
	leaq	128(%rsp), %rdx
	movl	$IID_ICryptoGetTextPassword2, %esi
	movq	%r15, %rdi
	callq	*(%rax)
.Ltmp292:
# BB#153:
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_176
# BB#154:
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movq	%r15, (%rsp)            # 8-byte Spill
	movq	$0, 120(%rsp)
	movq	(%rdi), %rax
.Ltmp294:
.Lcfi78:
	.cfi_escape 0x2e, 0x00
	leaq	560(%rsp), %rsi
	leaq	120(%rsp), %rdx
	callq	*40(%rax)
.Ltmp295:
# BB#155:
	testl	%eax, %eax
	cmovnel	%eax, %r13d
	movl	$1, %ebx
	jne	.LBB6_169
# BB#156:
	cmpl	$0, 560(%rsp)
	setne	436(%rsp)
	je	.LBB6_168
# BB#157:
	movq	120(%rsp), %rbx
	movl	$0, 448(%rsp)
	movq	440(%rsp), %rbp
	movl	$0, (%rbp)
	xorl	%r12d, %r12d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB6_158:                              # =>This Inner Loop Header: Depth=1
	incl	%r12d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB6_158
# BB#159:                               # %_Z11MyStringLenIwEiPKT_.exit.i333
	movl	452(%rsp), %r15d
	cmpl	%r12d, %r15d
	je	.LBB6_165
# BB#160:
	movslq	%r12d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp296:
.Lcfi79:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
.Ltmp297:
# BB#161:                               # %.noexc343
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	je	.LBB6_164
# BB#162:                               # %.noexc343
	testl	%r15d, %r15d
	jle	.LBB6_164
# BB#163:                               # %._crit_edge.thread.i.i337
.Lcfi80:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	movq	%rax, %rbp
	callq	_ZdaPv
	movq	%rbp, %rax
	movslq	448(%rsp), %rcx
.LBB6_164:                              # %._crit_edge16.i.i338
	movq	%rax, 440(%rsp)
	movl	$0, (%rax,%rcx,4)
	movl	%r12d, 452(%rsp)
	movq	%rax, %rbp
.LBB6_165:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i341.preheader
	decl	%r12d
	.p2align	4, 0x90
.LBB6_166:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i341
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rbp)
	addq	$4, %rbp
	testl	%eax, %eax
	jne	.LBB6_166
# BB#167:                               # %_ZN11CStringBaseIwEaSEPKw.exit
	movl	%r12d, 448(%rsp)
.LBB6_168:
	xorl	%ebx, %ebx
.LBB6_169:
	movq	120(%rsp), %rdi
.Ltmp301:
.Lcfi81:
	.cfi_escape 0x2e, 0x00
	callq	SysFreeString
.Ltmp302:
# BB#170:                               # %_ZN10CMyComBSTRD2Ev.exit345
	testl	%ebx, %ebx
	movq	8(%rsp), %r12           # 8-byte Reload
	jne	.LBB6_192
# BB#171:
	movb	436(%rsp), %cl
	movq	16(%rsp), %rdi          # 8-byte Reload
	movb	74(%rdi), %al
	testb	%cl, %cl
	je	.LBB6_199
# BB#172:
	leaq	76(%rdi), %rdx
	leaq	840(%rdi), %rsi
	cmpb	$0, 75(%rdi)
	cmoveq	%rsi, %rdx
	movb	(%rdx), %r13b
	movb	$1, %bl
	testb	%r13b, %r13b
	je	.LBB6_200
# BB#173:
	movb	%cl, 540(%rsp)
	movl	$0, 552(%rsp)
	movq	544(%rsp), %rbp
	movl	$0, (%rbp)
	movslq	448(%rsp), %rax
	incq	%rax
	movl	556(%rsp), %r15d
	cmpl	%r15d, %eax
	jne	.LBB6_201
# BB#174:
	movq	(%rsp), %r15            # 8-byte Reload
	jmp	.LBB6_207
.LBB6_175:
	movl	%r13d, %ebp
	jmp	.LBB6_197
.LBB6_176:                              # %.thread554
	movb	$0, 436(%rsp)
	movb	74(%rbx), %al
	xorl	%r13d, %r13d
	movq	%rbx, %rdx
	movb	%al, %bl
.LBB6_177:
	leaq	368(%rsp), %rsi
.LBB6_178:
	movb	$1, 258(%rsp)
	movb	$0, 259(%rsp)
	movb	$0, 260(%rsp)
	movb	$1, 261(%rsp)
	movq	%rsi, 240(%rsp)
	xorl	%ecx, %ecx
	orb	%r13b, %al
	cmoveq	%rcx, %r14
	movq	%r14, 248(%rsp)
	movl	84(%rdx), %eax
	testl	%eax, %eax
	je	.LBB6_180
# BB#179:
	movb	80(%rdx), %cl
.LBB6_180:
	movb	%cl, 256(%rsp)
	cmpl	$7, %eax
	seta	257(%rsp)
	cmpl	$2, 108(%rsp)           # 4-byte Folded Reload
	jae	.LBB6_182
# BB#181:
	xorl	%ebx, %ebx
.LBB6_182:
	movb	%bl, 258(%rsp)
	movq	16(%rsp), %rbx          # 8-byte Reload
	movb	77(%rbx), %al
	movb	%al, 259(%rsp)
	movb	78(%rbx), %al
	movb	%al, 260(%rsp)
	movb	79(%rbx), %al
	movb	%al, 261(%rsp)
	movups	56(%rbx), %xmm0
	movups	%xmm0, 264(%rsp)
	movb	73(%rbx), %al
	movb	%al, 280(%rsp)
	movb	48(%rbx), %al
	movb	%al, 281(%rsp)
	movb	88(%rbx), %al
	movb	%al, 282(%rsp)
.Ltmp307:
.Lcfi82:
	.cfi_escape 0x2e, 0x00
	leaq	1040(%rsp), %rdi
	callq	_ZN8NArchive3N7z11COutArchiveC2Ev
.Ltmp308:
# BB#183:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 568(%rsp)
	movq	$8, 584(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 560(%rsp)
	movups	%xmm0, 600(%rsp)
	movq	$1, 616(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 592(%rsp)
	movups	%xmm0, 632(%rsp)
	movq	$4, 648(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 624(%rsp)
	movups	%xmm0, 664(%rsp)
	movq	$8, 680(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, 656(%rsp)
	movups	%xmm0, 696(%rsp)
	movq	$4, 712(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 688(%rsp)
	movups	%xmm0, 728(%rsp)
	movq	$8, 744(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE+16, 720(%rsp)
	movups	%xmm0, 760(%rsp)
	movq	$8, 776(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 752(%rsp)
	movups	%xmm0, 792(%rsp)
	movq	$1, 808(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 784(%rsp)
	movups	%xmm0, 824(%rsp)
	movq	$8, 840(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 816(%rsp)
	movups	%xmm0, 856(%rsp)
	movq	$1, 872(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 848(%rsp)
	movups	%xmm0, 888(%rsp)
	movq	$8, 904(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 880(%rsp)
	movups	%xmm0, 920(%rsp)
	movq	$1, 936(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 912(%rsp)
	movups	%xmm0, 952(%rsp)
	movq	$8, 968(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 944(%rsp)
	movups	%xmm0, 984(%rsp)
	movq	$1, 1000(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 976(%rsp)
	movups	%xmm0, 1016(%rsp)
	movq	$1, 1032(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 1008(%rsp)
	movq	$0, 112(%rsp)
	movq	(%r15), %rax
.Ltmp310:
.Lcfi83:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdx
	movl	$IID_ICryptoGetTextPassword, %esi
	movq	%r15, %rdi
	callq	*(%rax)
.Ltmp311:
# BB#184:
	movq	136(%rbx), %rdi
	movq	112(%rsp), %rax
.Ltmp312:
.Lcfi84:
	.cfi_escape 0x2e, 0x20
	subq	$8, %rsp
.Lcfi85:
	.cfi_adjust_cfa_offset 8
	leaq	248(%rsp), %rbp
	leaq	152(%rsp), %rdx
	leaq	1048(%rsp), %rcx
	leaq	568(%rsp), %r8
	movq	%r12, %rsi
	movq	472(%rsp), %r9          # 8-byte Reload
	pushq	%rax
.Lcfi86:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi87:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi88:
	.cfi_adjust_cfa_offset 8
	callq	_ZN8NArchive3N7z6UpdateEP9IInStreamPKNS0_18CArchiveDatabaseExERK13CObjectVectorINS0_11CUpdateItemEERNS0_11COutArchiveERNS0_16CArchiveDatabaseEP20ISequentialOutStreamP22IArchiveUpdateCallbackRKNS0_14CUpdateOptionsEP22ICryptoGetTextPassword
	addq	$32, %rsp
.Lcfi89:
	.cfi_adjust_cfa_offset -32
	movl	%eax, %r13d
.Ltmp313:
# BB#185:
	testl	%r13d, %r13d
	jne	.LBB6_188
# BB#186:
.Ltmp315:
.Lcfi90:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVector12ClearAndFreeEv
.Ltmp316:
# BB#187:
	leaq	258(%rsp), %rcx
	movq	248(%rsp), %rdx
.Ltmp317:
.Lcfi91:
	.cfi_escape 0x2e, 0x00
	leaq	1040(%rsp), %rdi
	leaq	560(%rsp), %rsi
	callq	_ZN8NArchive3N7z11COutArchive13WriteDatabaseERKNS0_16CArchiveDatabaseEPKNS0_22CCompressionMethodModeERKNS0_14CHeaderOptionsE
	movl	%eax, %r13d
.Ltmp318:
.LBB6_188:
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_190
# BB#189:
	movq	(%rdi), %rax
.Ltmp322:
.Lcfi92:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp323:
.LBB6_190:                              # %_ZN9CMyComPtrI22ICryptoGetTextPasswordED2Ev.exit
.Ltmp327:
.Lcfi93:
	.cfi_escape 0x2e, 0x00
	leaq	560(%rsp), %rdi
	callq	_ZN8NArchive3N7z16CArchiveDatabaseD2Ev
.Ltmp328:
# BB#191:
.Ltmp332:
.Lcfi94:
	.cfi_escape 0x2e, 0x00
	leaq	1040(%rsp), %rdi
	callq	_ZN8NArchive3N7z11COutArchiveD2Ev
.Ltmp333:
.LBB6_192:
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_194
# BB#193:
	movq	(%rdi), %rax
.Ltmp337:
.Lcfi95:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp338:
.LBB6_194:                              # %_ZN9CMyComPtrI23ICryptoGetTextPassword2ED2Ev.exit362
	movl	%r13d, %ebp
.LBB6_195:
.Ltmp342:
.Lcfi96:
	.cfi_escape 0x2e, 0x00
	leaq	472(%rsp), %rdi
	callq	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
.Ltmp343:
# BB#196:
.Ltmp347:
.Lcfi97:
	.cfi_escape 0x2e, 0x00
	leaq	368(%rsp), %rdi
	callq	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
.Ltmp348:
.LBB6_197:
	leaq	144(%rsp), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CUpdateItemEE+16, 144(%rsp)
.Ltmp359:
.Lcfi98:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp360:
# BB#198:
.Ltmp365:
.Lcfi99:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp366:
	jmp	.LBB6_277
.LBB6_199:
	xorl	%r13d, %r13d
	movq	%rdi, %rdx
	movb	%al, %bl
	movq	(%rsp), %r15            # 8-byte Reload
	jmp	.LBB6_177
.LBB6_200:
	xorl	%r13d, %r13d
	movq	(%rsp), %r15            # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB6_177
.LBB6_201:
	movl	$4, %ecx
	movq	%rax, 136(%rsp)         # 8-byte Spill
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp304:
.Lcfi100:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %rcx
.Ltmp305:
# BB#202:                               # %.noexc355
	xorl	%eax, %eax
	testq	%rbp, %rbp
	je	.LBB6_205
# BB#203:                               # %.noexc355
	testl	%r15d, %r15d
	movq	(%rsp), %r15            # 8-byte Reload
	jle	.LBB6_206
# BB#204:                               # %._crit_edge.thread.i.i349
.Lcfi101:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	movq	%rcx, %rbp
	callq	_ZdaPv
	movq	%rbp, %rcx
	movslq	552(%rsp), %rax
	jmp	.LBB6_206
.LBB6_205:
	movq	(%rsp), %r15            # 8-byte Reload
.LBB6_206:                              # %._crit_edge16.i.i350
	movq	%rcx, 544(%rsp)
	movl	$0, (%rcx,%rax,4)
	movq	136(%rsp), %rax         # 8-byte Reload
	movl	%eax, 556(%rsp)
	movq	%rcx, %rbp
.LBB6_207:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i351
	leaq	368(%rsp), %rsi
	movq	440(%rsp), %rax
	.p2align	4, 0x90
.LBB6_208:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbp)
	addq	$4, %rbp
	testl	%ecx, %ecx
	jne	.LBB6_208
# BB#209:                               # %_ZN11CStringBaseIwEaSERKS0_.exit356
	movl	448(%rsp), %eax
	movl	%eax, 552(%rsp)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movb	74(%rdx), %al
	jmp	.LBB6_178
.LBB6_210:
.Ltmp306:
	jmp	.LBB6_220
.LBB6_211:
.Ltmp324:
	movq	%rdx, %r14
	movq	%rax, %r15
	jmp	.LBB6_224
.LBB6_212:
.Ltmp319:
	jmp	.LBB6_222
.LBB6_213:
.Ltmp334:
	jmp	.LBB6_220
.LBB6_214:
.Ltmp329:
	movq	%rdx, %r14
	movq	%rax, %r15
	jmp	.LBB6_225
.LBB6_215:
.Ltmp309:
	jmp	.LBB6_220
.LBB6_216:
.Ltmp339:
	jmp	.LBB6_231
.LBB6_217:
.Ltmp303:
	jmp	.LBB6_220
.LBB6_218:
.Ltmp298:
	movq	%rdx, %r14
	movq	%rax, %r15
	movq	120(%rsp), %rdi
.Ltmp299:
.Lcfi102:
	.cfi_escape 0x2e, 0x00
	callq	SysFreeString
.Ltmp300:
	jmp	.LBB6_226
.LBB6_219:
.Ltmp293:
.LBB6_220:
	movq	%rdx, %r14
	movq	%rax, %r15
	jmp	.LBB6_226
.LBB6_221:
.Ltmp314:
.LBB6_222:
	movq	%rdx, %r14
	movq	%rax, %r15
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_224
# BB#223:
	movq	(%rdi), %rax
.Ltmp320:
.Lcfi103:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp321:
.LBB6_224:                              # %_ZN9CMyComPtrI22ICryptoGetTextPasswordED2Ev.exit360
.Ltmp325:
.Lcfi104:
	.cfi_escape 0x2e, 0x00
	leaq	560(%rsp), %rdi
	callq	_ZN8NArchive3N7z16CArchiveDatabaseD2Ev
.Ltmp326:
.LBB6_225:
.Ltmp330:
.Lcfi105:
	.cfi_escape 0x2e, 0x00
	leaq	1040(%rsp), %rdi
	callq	_ZN8NArchive3N7z11COutArchiveD2Ev
.Ltmp331:
.LBB6_226:
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_232
# BB#227:
	movq	(%rdi), %rax
.Ltmp335:
.Lcfi106:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp336:
	jmp	.LBB6_232
.LBB6_228:
.Ltmp349:
	jmp	.LBB6_272
.LBB6_229:
.Ltmp344:
	jmp	.LBB6_234
.LBB6_230:
.Ltmp290:
.LBB6_231:
	movq	%rdx, %r14
	movq	%rax, %r15
.LBB6_232:
.Ltmp340:
.Lcfi107:
	.cfi_escape 0x2e, 0x00
	leaq	472(%rsp), %rdi
	callq	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
.Ltmp341:
	jmp	.LBB6_235
.LBB6_233:
.Ltmp287:
.LBB6_234:
	movq	%rdx, %r14
	movq	%rax, %r15
.LBB6_235:
.Ltmp345:
.Lcfi108:
	.cfi_escape 0x2e, 0x00
	leaq	368(%rsp), %rdi
	callq	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
.Ltmp346:
	jmp	.LBB6_273
.LBB6_236:
.Ltmp284:
	jmp	.LBB6_272
.LBB6_237:
.Ltmp367:
	movq	%rdx, %r14
	movq	%rax, %r15
	jmp	.LBB6_275
.LBB6_238:
.Ltmp361:
	movq	%rdx, %r14
	movq	%rax, %r15
.Ltmp362:
.Lcfi109:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp363:
	jmp	.LBB6_275
.LBB6_239:
.Ltmp364:
.Lcfi110:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_240:
.Ltmp244:
	movq	%rdx, %r14
	movq	%rax, %r15
	movq	560(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_243
# BB#241:
.Lcfi111:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB6_243
.LBB6_242:
.Ltmp241:
	movq	%rdx, %r14
	movq	%rax, %r15
.LBB6_243:                              # %_ZN11CStringBaseIwED2Ev.exit309
	movq	1040(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_253
# BB#244:
.Lcfi112:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB6_253
.LBB6_245:
.Ltmp265:
	jmp	.LBB6_267
.LBB6_246:
.Ltmp238:
	jmp	.LBB6_252
.LBB6_247:
.Ltmp260:
	movq	%rdx, %r14
	movq	%rax, %r15
.Ltmp261:
.Lcfi113:
	.cfi_escape 0x2e, 0x00
	leaq	304(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp262:
	jmp	.LBB6_269
.LBB6_248:
.Ltmp257:
	jmp	.LBB6_267
.LBB6_249:
.Ltmp252:
	movq	%rdx, %r14
	movq	%rax, %r15
.Ltmp253:
.Lcfi114:
	.cfi_escape 0x2e, 0x00
	leaq	352(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp254:
	jmp	.LBB6_269
.LBB6_250:
.Ltmp249:
	jmp	.LBB6_267
.LBB6_251:
.Ltmp235:
.LBB6_252:
	movq	%rdx, %r14
	movq	%rax, %r15
.LBB6_253:
.Ltmp245:
.Lcfi115:
	.cfi_escape 0x2e, 0x00
	leaq	320(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp246:
	jmp	.LBB6_269
.LBB6_254:
.Ltmp226:
	movq	%rdx, %r14
	movq	%rax, %r15
.Ltmp227:
.Lcfi116:
	.cfi_escape 0x2e, 0x00
	leaq	224(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp228:
	jmp	.LBB6_269
.LBB6_255:
.Ltmp229:
.Lcfi117:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_256:
.Ltmp218:
	movq	%rdx, %r14
	movq	%rax, %r15
.Ltmp219:
.Lcfi118:
	.cfi_escape 0x2e, 0x00
	leaq	208(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp220:
	jmp	.LBB6_269
.LBB6_257:
.Ltmp221:
.Lcfi119:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_258:
.Ltmp199:
	jmp	.LBB6_267
.LBB6_259:
.Ltmp278:
	movq	%rdx, %r14
	movq	%rax, %r15
.Lcfi120:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB6_269
.LBB6_260:
.Ltmp210:
	movq	%rdx, %r14
	movq	%rax, %r15
.Ltmp211:
.Lcfi121:
	.cfi_escape 0x2e, 0x00
	leaq	192(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp212:
	jmp	.LBB6_269
.LBB6_261:
.Ltmp213:
.Lcfi122:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_262:
.Ltmp273:
	jmp	.LBB6_267
.LBB6_263:
.Ltmp268:
	movq	%rdx, %r14
	movq	%rax, %r15
.Ltmp269:
.Lcfi123:
	.cfi_escape 0x2e, 0x00
	leaq	288(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp270:
	jmp	.LBB6_269
.LBB6_264:
.Ltmp232:
	jmp	.LBB6_267
.LBB6_265:
.Ltmp281:
	jmp	.LBB6_267
.LBB6_266:
.Ltmp207:
.LBB6_267:
	movq	%rdx, %r14
	movq	%rax, %r15
	jmp	.LBB6_269
.LBB6_268:
.Ltmp202:
	movq	%rdx, %r14
	movq	%rax, %r15
.Ltmp203:
.Lcfi124:
	.cfi_escape 0x2e, 0x00
	leaq	336(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp204:
.LBB6_269:
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_273
# BB#270:
.Lcfi125:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB6_273
.LBB6_271:
.Ltmp196:
.LBB6_272:
	movq	%rdx, %r14
	movq	%rax, %r15
.LBB6_273:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CUpdateItemEE+16, 144(%rsp)
.Ltmp350:
.Lcfi126:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp351:
# BB#274:
.Ltmp356:
.Lcfi127:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp357:
.LBB6_275:
.Lcfi128:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r14d
	je	.LBB6_278
# BB#276:
.Lcfi129:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
	movl	$-2147024882, %ebp      # imm = 0x8007000E
.LBB6_277:
	movl	%ebp, %eax
	addq	$1176, %rsp             # imm = 0x498
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_278:
.Lcfi130:
	.cfi_escape 0x2e, 0x00
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp368:
.Lcfi131:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp369:
# BB#279:
.LBB6_280:
.Ltmp370:
	movq	%rax, %rbx
.Lcfi132:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
.Lcfi133:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB6_281:
.Ltmp352:
	movq	%rax, %rbx
.Ltmp353:
.Lcfi134:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp354:
	jmp	.LBB6_284
.LBB6_282:
.Ltmp355:
.Lcfi135:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_283:
.Ltmp358:
	movq	%rax, %rbx
.LBB6_284:                              # %.body
.Lcfi136:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN8NArchive3N7z8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback, .Lfunc_end6-_ZN8NArchive3N7z8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\332\205\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\313\005"              # Call site table length
	.long	.Ltmp192-.Lfunc_begin3  # >> Call Site 1 <<
	.long	.Ltmp195-.Ltmp192       #   Call between .Ltmp192 and .Ltmp195
	.long	.Ltmp196-.Lfunc_begin3  #     jumps to .Ltmp196
	.byte	3                       #   On action: 2
	.long	.Ltmp197-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Ltmp198-.Ltmp197       #   Call between .Ltmp197 and .Ltmp198
	.long	.Ltmp199-.Lfunc_begin3  #     jumps to .Ltmp199
	.byte	3                       #   On action: 2
	.long	.Ltmp200-.Lfunc_begin3  # >> Call Site 3 <<
	.long	.Ltmp201-.Ltmp200       #   Call between .Ltmp200 and .Ltmp201
	.long	.Ltmp202-.Lfunc_begin3  #     jumps to .Ltmp202
	.byte	3                       #   On action: 2
	.long	.Ltmp205-.Lfunc_begin3  # >> Call Site 4 <<
	.long	.Ltmp206-.Ltmp205       #   Call between .Ltmp205 and .Ltmp206
	.long	.Ltmp207-.Lfunc_begin3  #     jumps to .Ltmp207
	.byte	3                       #   On action: 2
	.long	.Ltmp266-.Lfunc_begin3  # >> Call Site 5 <<
	.long	.Ltmp267-.Ltmp266       #   Call between .Ltmp266 and .Ltmp267
	.long	.Ltmp268-.Lfunc_begin3  #     jumps to .Ltmp268
	.byte	3                       #   On action: 2
	.long	.Ltmp271-.Lfunc_begin3  # >> Call Site 6 <<
	.long	.Ltmp272-.Ltmp271       #   Call between .Ltmp271 and .Ltmp272
	.long	.Ltmp273-.Lfunc_begin3  #     jumps to .Ltmp273
	.byte	3                       #   On action: 2
	.long	.Ltmp208-.Lfunc_begin3  # >> Call Site 7 <<
	.long	.Ltmp209-.Ltmp208       #   Call between .Ltmp208 and .Ltmp209
	.long	.Ltmp210-.Lfunc_begin3  #     jumps to .Ltmp210
	.byte	3                       #   On action: 2
	.long	.Ltmp214-.Lfunc_begin3  # >> Call Site 8 <<
	.long	.Ltmp215-.Ltmp214       #   Call between .Ltmp214 and .Ltmp215
	.long	.Ltmp232-.Lfunc_begin3  #     jumps to .Ltmp232
	.byte	3                       #   On action: 2
	.long	.Ltmp216-.Lfunc_begin3  # >> Call Site 9 <<
	.long	.Ltmp217-.Ltmp216       #   Call between .Ltmp216 and .Ltmp217
	.long	.Ltmp218-.Lfunc_begin3  #     jumps to .Ltmp218
	.byte	3                       #   On action: 2
	.long	.Ltmp222-.Lfunc_begin3  # >> Call Site 10 <<
	.long	.Ltmp223-.Ltmp222       #   Call between .Ltmp222 and .Ltmp223
	.long	.Ltmp232-.Lfunc_begin3  #     jumps to .Ltmp232
	.byte	3                       #   On action: 2
	.long	.Ltmp224-.Lfunc_begin3  # >> Call Site 11 <<
	.long	.Ltmp225-.Ltmp224       #   Call between .Ltmp224 and .Ltmp225
	.long	.Ltmp226-.Lfunc_begin3  #     jumps to .Ltmp226
	.byte	3                       #   On action: 2
	.long	.Ltmp230-.Lfunc_begin3  # >> Call Site 12 <<
	.long	.Ltmp231-.Ltmp230       #   Call between .Ltmp230 and .Ltmp231
	.long	.Ltmp232-.Lfunc_begin3  #     jumps to .Ltmp232
	.byte	3                       #   On action: 2
	.long	.Ltmp233-.Lfunc_begin3  # >> Call Site 13 <<
	.long	.Ltmp234-.Ltmp233       #   Call between .Ltmp233 and .Ltmp234
	.long	.Ltmp235-.Lfunc_begin3  #     jumps to .Ltmp235
	.byte	3                       #   On action: 2
	.long	.Ltmp236-.Lfunc_begin3  # >> Call Site 14 <<
	.long	.Ltmp237-.Ltmp236       #   Call between .Ltmp236 and .Ltmp237
	.long	.Ltmp238-.Lfunc_begin3  #     jumps to .Ltmp238
	.byte	3                       #   On action: 2
	.long	.Ltmp239-.Lfunc_begin3  # >> Call Site 15 <<
	.long	.Ltmp240-.Ltmp239       #   Call between .Ltmp239 and .Ltmp240
	.long	.Ltmp241-.Lfunc_begin3  #     jumps to .Ltmp241
	.byte	3                       #   On action: 2
	.long	.Ltmp242-.Lfunc_begin3  # >> Call Site 16 <<
	.long	.Ltmp243-.Ltmp242       #   Call between .Ltmp242 and .Ltmp243
	.long	.Ltmp244-.Lfunc_begin3  #     jumps to .Ltmp244
	.byte	3                       #   On action: 2
	.long	.Ltmp247-.Lfunc_begin3  # >> Call Site 17 <<
	.long	.Ltmp248-.Ltmp247       #   Call between .Ltmp247 and .Ltmp248
	.long	.Ltmp249-.Lfunc_begin3  #     jumps to .Ltmp249
	.byte	3                       #   On action: 2
	.long	.Ltmp250-.Lfunc_begin3  # >> Call Site 18 <<
	.long	.Ltmp251-.Ltmp250       #   Call between .Ltmp250 and .Ltmp251
	.long	.Ltmp252-.Lfunc_begin3  #     jumps to .Ltmp252
	.byte	3                       #   On action: 2
	.long	.Ltmp255-.Lfunc_begin3  # >> Call Site 19 <<
	.long	.Ltmp256-.Ltmp255       #   Call between .Ltmp255 and .Ltmp256
	.long	.Ltmp257-.Lfunc_begin3  #     jumps to .Ltmp257
	.byte	3                       #   On action: 2
	.long	.Ltmp258-.Lfunc_begin3  # >> Call Site 20 <<
	.long	.Ltmp259-.Ltmp258       #   Call between .Ltmp258 and .Ltmp259
	.long	.Ltmp260-.Lfunc_begin3  #     jumps to .Ltmp260
	.byte	3                       #   On action: 2
	.long	.Ltmp263-.Lfunc_begin3  # >> Call Site 21 <<
	.long	.Ltmp264-.Ltmp263       #   Call between .Ltmp263 and .Ltmp264
	.long	.Ltmp265-.Lfunc_begin3  #     jumps to .Ltmp265
	.byte	3                       #   On action: 2
	.long	.Ltmp274-.Lfunc_begin3  # >> Call Site 22 <<
	.long	.Ltmp275-.Ltmp274       #   Call between .Ltmp274 and .Ltmp275
	.long	.Ltmp281-.Lfunc_begin3  #     jumps to .Ltmp281
	.byte	3                       #   On action: 2
	.long	.Ltmp276-.Lfunc_begin3  # >> Call Site 23 <<
	.long	.Ltmp277-.Ltmp276       #   Call between .Ltmp276 and .Ltmp277
	.long	.Ltmp278-.Lfunc_begin3  #     jumps to .Ltmp278
	.byte	3                       #   On action: 2
	.long	.Ltmp279-.Lfunc_begin3  # >> Call Site 24 <<
	.long	.Ltmp280-.Ltmp279       #   Call between .Ltmp279 and .Ltmp280
	.long	.Ltmp281-.Lfunc_begin3  #     jumps to .Ltmp281
	.byte	3                       #   On action: 2
	.long	.Ltmp282-.Lfunc_begin3  # >> Call Site 25 <<
	.long	.Ltmp283-.Ltmp282       #   Call between .Ltmp282 and .Ltmp283
	.long	.Ltmp284-.Lfunc_begin3  #     jumps to .Ltmp284
	.byte	3                       #   On action: 2
	.long	.Ltmp285-.Lfunc_begin3  # >> Call Site 26 <<
	.long	.Ltmp286-.Ltmp285       #   Call between .Ltmp285 and .Ltmp286
	.long	.Ltmp287-.Lfunc_begin3  #     jumps to .Ltmp287
	.byte	3                       #   On action: 2
	.long	.Ltmp288-.Lfunc_begin3  # >> Call Site 27 <<
	.long	.Ltmp289-.Ltmp288       #   Call between .Ltmp288 and .Ltmp289
	.long	.Ltmp290-.Lfunc_begin3  #     jumps to .Ltmp290
	.byte	3                       #   On action: 2
	.long	.Ltmp291-.Lfunc_begin3  # >> Call Site 28 <<
	.long	.Ltmp292-.Ltmp291       #   Call between .Ltmp291 and .Ltmp292
	.long	.Ltmp293-.Lfunc_begin3  #     jumps to .Ltmp293
	.byte	3                       #   On action: 2
	.long	.Ltmp294-.Lfunc_begin3  # >> Call Site 29 <<
	.long	.Ltmp297-.Ltmp294       #   Call between .Ltmp294 and .Ltmp297
	.long	.Ltmp298-.Lfunc_begin3  #     jumps to .Ltmp298
	.byte	3                       #   On action: 2
	.long	.Ltmp301-.Lfunc_begin3  # >> Call Site 30 <<
	.long	.Ltmp302-.Ltmp301       #   Call between .Ltmp301 and .Ltmp302
	.long	.Ltmp303-.Lfunc_begin3  #     jumps to .Ltmp303
	.byte	3                       #   On action: 2
	.long	.Ltmp307-.Lfunc_begin3  # >> Call Site 31 <<
	.long	.Ltmp308-.Ltmp307       #   Call between .Ltmp307 and .Ltmp308
	.long	.Ltmp309-.Lfunc_begin3  #     jumps to .Ltmp309
	.byte	3                       #   On action: 2
	.long	.Ltmp310-.Lfunc_begin3  # >> Call Site 32 <<
	.long	.Ltmp313-.Ltmp310       #   Call between .Ltmp310 and .Ltmp313
	.long	.Ltmp314-.Lfunc_begin3  #     jumps to .Ltmp314
	.byte	3                       #   On action: 2
	.long	.Ltmp315-.Lfunc_begin3  # >> Call Site 33 <<
	.long	.Ltmp318-.Ltmp315       #   Call between .Ltmp315 and .Ltmp318
	.long	.Ltmp319-.Lfunc_begin3  #     jumps to .Ltmp319
	.byte	3                       #   On action: 2
	.long	.Ltmp322-.Lfunc_begin3  # >> Call Site 34 <<
	.long	.Ltmp323-.Ltmp322       #   Call between .Ltmp322 and .Ltmp323
	.long	.Ltmp324-.Lfunc_begin3  #     jumps to .Ltmp324
	.byte	3                       #   On action: 2
	.long	.Ltmp327-.Lfunc_begin3  # >> Call Site 35 <<
	.long	.Ltmp328-.Ltmp327       #   Call between .Ltmp327 and .Ltmp328
	.long	.Ltmp329-.Lfunc_begin3  #     jumps to .Ltmp329
	.byte	3                       #   On action: 2
	.long	.Ltmp332-.Lfunc_begin3  # >> Call Site 36 <<
	.long	.Ltmp333-.Ltmp332       #   Call between .Ltmp332 and .Ltmp333
	.long	.Ltmp334-.Lfunc_begin3  #     jumps to .Ltmp334
	.byte	3                       #   On action: 2
	.long	.Ltmp337-.Lfunc_begin3  # >> Call Site 37 <<
	.long	.Ltmp338-.Ltmp337       #   Call between .Ltmp337 and .Ltmp338
	.long	.Ltmp339-.Lfunc_begin3  #     jumps to .Ltmp339
	.byte	3                       #   On action: 2
	.long	.Ltmp342-.Lfunc_begin3  # >> Call Site 38 <<
	.long	.Ltmp343-.Ltmp342       #   Call between .Ltmp342 and .Ltmp343
	.long	.Ltmp344-.Lfunc_begin3  #     jumps to .Ltmp344
	.byte	3                       #   On action: 2
	.long	.Ltmp347-.Lfunc_begin3  # >> Call Site 39 <<
	.long	.Ltmp348-.Ltmp347       #   Call between .Ltmp347 and .Ltmp348
	.long	.Ltmp349-.Lfunc_begin3  #     jumps to .Ltmp349
	.byte	3                       #   On action: 2
	.long	.Ltmp359-.Lfunc_begin3  # >> Call Site 40 <<
	.long	.Ltmp360-.Ltmp359       #   Call between .Ltmp359 and .Ltmp360
	.long	.Ltmp361-.Lfunc_begin3  #     jumps to .Ltmp361
	.byte	3                       #   On action: 2
	.long	.Ltmp365-.Lfunc_begin3  # >> Call Site 41 <<
	.long	.Ltmp366-.Ltmp365       #   Call between .Ltmp365 and .Ltmp366
	.long	.Ltmp367-.Lfunc_begin3  #     jumps to .Ltmp367
	.byte	3                       #   On action: 2
	.long	.Ltmp304-.Lfunc_begin3  # >> Call Site 42 <<
	.long	.Ltmp305-.Ltmp304       #   Call between .Ltmp304 and .Ltmp305
	.long	.Ltmp306-.Lfunc_begin3  #     jumps to .Ltmp306
	.byte	3                       #   On action: 2
	.long	.Ltmp299-.Lfunc_begin3  # >> Call Site 43 <<
	.long	.Ltmp346-.Ltmp299       #   Call between .Ltmp299 and .Ltmp346
	.long	.Ltmp358-.Lfunc_begin3  #     jumps to .Ltmp358
	.byte	1                       #   On action: 1
	.long	.Ltmp362-.Lfunc_begin3  # >> Call Site 44 <<
	.long	.Ltmp363-.Ltmp362       #   Call between .Ltmp362 and .Ltmp363
	.long	.Ltmp364-.Lfunc_begin3  #     jumps to .Ltmp364
	.byte	1                       #   On action: 1
	.long	.Ltmp261-.Lfunc_begin3  # >> Call Site 45 <<
	.long	.Ltmp246-.Ltmp261       #   Call between .Ltmp261 and .Ltmp246
	.long	.Ltmp358-.Lfunc_begin3  #     jumps to .Ltmp358
	.byte	1                       #   On action: 1
	.long	.Ltmp227-.Lfunc_begin3  # >> Call Site 46 <<
	.long	.Ltmp228-.Ltmp227       #   Call between .Ltmp227 and .Ltmp228
	.long	.Ltmp229-.Lfunc_begin3  #     jumps to .Ltmp229
	.byte	1                       #   On action: 1
	.long	.Ltmp219-.Lfunc_begin3  # >> Call Site 47 <<
	.long	.Ltmp220-.Ltmp219       #   Call between .Ltmp219 and .Ltmp220
	.long	.Ltmp221-.Lfunc_begin3  #     jumps to .Ltmp221
	.byte	1                       #   On action: 1
	.long	.Ltmp211-.Lfunc_begin3  # >> Call Site 48 <<
	.long	.Ltmp212-.Ltmp211       #   Call between .Ltmp211 and .Ltmp212
	.long	.Ltmp213-.Lfunc_begin3  #     jumps to .Ltmp213
	.byte	1                       #   On action: 1
	.long	.Ltmp269-.Lfunc_begin3  # >> Call Site 49 <<
	.long	.Ltmp204-.Ltmp269       #   Call between .Ltmp269 and .Ltmp204
	.long	.Ltmp358-.Lfunc_begin3  #     jumps to .Ltmp358
	.byte	1                       #   On action: 1
	.long	.Ltmp350-.Lfunc_begin3  # >> Call Site 50 <<
	.long	.Ltmp351-.Ltmp350       #   Call between .Ltmp350 and .Ltmp351
	.long	.Ltmp352-.Lfunc_begin3  #     jumps to .Ltmp352
	.byte	1                       #   On action: 1
	.long	.Ltmp356-.Lfunc_begin3  # >> Call Site 51 <<
	.long	.Ltmp357-.Ltmp356       #   Call between .Ltmp356 and .Ltmp357
	.long	.Ltmp358-.Lfunc_begin3  #     jumps to .Ltmp358
	.byte	1                       #   On action: 1
	.long	.Ltmp357-.Lfunc_begin3  # >> Call Site 52 <<
	.long	.Ltmp368-.Ltmp357       #   Call between .Ltmp357 and .Ltmp368
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp368-.Lfunc_begin3  # >> Call Site 53 <<
	.long	.Ltmp369-.Ltmp368       #   Call between .Ltmp368 and .Ltmp369
	.long	.Ltmp370-.Lfunc_begin3  #     jumps to .Ltmp370
	.byte	0                       #   On action: cleanup
	.long	.Ltmp369-.Lfunc_begin3  # >> Call Site 54 <<
	.long	.Ltmp353-.Ltmp369       #   Call between .Ltmp369 and .Ltmp353
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp353-.Lfunc_begin3  # >> Call Site 55 <<
	.long	.Ltmp354-.Ltmp353       #   Call between .Ltmp353 and .Ltmp354
	.long	.Ltmp355-.Lfunc_begin3  #     jumps to .Ltmp355
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z22CCompressionMethodModeC2Ev,"axG",@progbits,_ZN8NArchive3N7z22CCompressionMethodModeC2Ev,comdat
	.weak	_ZN8NArchive3N7z22CCompressionMethodModeC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z22CCompressionMethodModeC2Ev,@function
_ZN8NArchive3N7z22CCompressionMethodModeC2Ev: # @_ZN8NArchive3N7z22CCompressionMethodModeC2Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r15
.Lcfi137:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi138:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi139:
	.cfi_def_cfa_offset 32
.Lcfi140:
	.cfi_offset %rbx, -32
.Lcfi141:
	.cfi_offset %r14, -24
.Lcfi142:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movq	$8, 24(%rbx)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
	leaq	32(%rbx), %r15
	movups	%xmm0, 40(%rbx)
	movq	$16, 56(%rbx)
	movq	$_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE+16, 32(%rbx)
	movl	$1, 64(%rbx)
	movb	$0, 68(%rbx)
	movups	%xmm0, 72(%rbx)
.Ltmp371:
	movl	$16, %edi
	callq	_Znam
.Ltmp372:
# BB#1:
	movq	%rax, 72(%rbx)
	movl	$0, (%rax)
	movl	$4, 84(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB7_2:
.Ltmp373:
	movq	%rax, %r14
.Ltmp374:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp375:
# BB#3:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp376:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp377:
# BB#4:
.Ltmp382:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp383:
# BB#5:                                 # %_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB7_6:
.Ltmp378:
	movq	%rax, %r14
.Ltmp379:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp380:
	jmp	.LBB7_9
.LBB7_7:
.Ltmp381:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_8:
.Ltmp384:
	movq	%rax, %r14
.LBB7_9:                                # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN8NArchive3N7z22CCompressionMethodModeC2Ev, .Lfunc_end7-_ZN8NArchive3N7z22CCompressionMethodModeC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp371-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp372-.Ltmp371       #   Call between .Ltmp371 and .Ltmp372
	.long	.Ltmp373-.Lfunc_begin4  #     jumps to .Ltmp373
	.byte	0                       #   On action: cleanup
	.long	.Ltmp374-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp375-.Ltmp374       #   Call between .Ltmp374 and .Ltmp375
	.long	.Ltmp384-.Lfunc_begin4  #     jumps to .Ltmp384
	.byte	1                       #   On action: 1
	.long	.Ltmp376-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp377-.Ltmp376       #   Call between .Ltmp376 and .Ltmp377
	.long	.Ltmp378-.Lfunc_begin4  #     jumps to .Ltmp378
	.byte	1                       #   On action: 1
	.long	.Ltmp382-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp383-.Ltmp382       #   Call between .Ltmp382 and .Ltmp383
	.long	.Ltmp384-.Lfunc_begin4  #     jumps to .Ltmp384
	.byte	1                       #   On action: 1
	.long	.Ltmp383-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Ltmp379-.Ltmp383       #   Call between .Ltmp383 and .Ltmp379
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp379-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Ltmp380-.Ltmp379       #   Call between .Ltmp379 and .Ltmp380
	.long	.Ltmp381-.Lfunc_begin4  #     jumps to .Ltmp381
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z11COutArchiveC2Ev,"axG",@progbits,_ZN8NArchive3N7z11COutArchiveC2Ev,comdat
	.weak	_ZN8NArchive3N7z11COutArchiveC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchiveC2Ev,@function
_ZN8NArchive3N7z11COutArchiveC2Ev:      # @_ZN8NArchive3N7z11COutArchiveC2Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r15
.Lcfi143:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi144:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi145:
	.cfi_def_cfa_offset 32
.Lcfi146:
	.cfi_offset %rbx, -32
.Lcfi147:
	.cfi_offset %r14, -24
.Lcfi148:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	leaq	32(%rbx), %r15
	movq	$0, 32(%rbx)
	movl	$0, 40(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 72(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
.Ltmp385:
	movl	$65536, %esi            # imm = 0x10000
	movq	%r15, %rdi
	callq	_ZN10COutBuffer6CreateEj
.Ltmp386:
# BB#1:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB8_2:
.Ltmp387:
	movq	%rax, %r14
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp388:
	callq	*16(%rax)
.Ltmp389:
.LBB8_4:                                # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp390:
	callq	*16(%rax)
.Ltmp391:
.LBB8_6:                                # %_ZN9CMyComPtrI10IOutStreamED2Ev.exit
.Ltmp392:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer4FreeEv
.Ltmp393:
# BB#7:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_9
# BB#8:
	movq	(%rdi), %rax
.Ltmp398:
	callq	*16(%rax)
.Ltmp399:
.LBB8_9:                                # %_ZN10COutBufferD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_10:
.Ltmp394:
	movq	%rax, %r14
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_14
# BB#11:
	movq	(%rdi), %rax
.Ltmp395:
	callq	*16(%rax)
.Ltmp396:
	jmp	.LBB8_14
.LBB8_12:
.Ltmp397:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_13:
.Ltmp400:
	movq	%rax, %r14
.LBB8_14:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN8NArchive3N7z11COutArchiveC2Ev, .Lfunc_end8-_ZN8NArchive3N7z11COutArchiveC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp385-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp386-.Ltmp385       #   Call between .Ltmp385 and .Ltmp386
	.long	.Ltmp387-.Lfunc_begin5  #     jumps to .Ltmp387
	.byte	0                       #   On action: cleanup
	.long	.Ltmp388-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp391-.Ltmp388       #   Call between .Ltmp388 and .Ltmp391
	.long	.Ltmp400-.Lfunc_begin5  #     jumps to .Ltmp400
	.byte	1                       #   On action: 1
	.long	.Ltmp392-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp393-.Ltmp392       #   Call between .Ltmp392 and .Ltmp393
	.long	.Ltmp394-.Lfunc_begin5  #     jumps to .Ltmp394
	.byte	1                       #   On action: 1
	.long	.Ltmp398-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp399-.Ltmp398       #   Call between .Ltmp398 and .Ltmp399
	.long	.Ltmp400-.Lfunc_begin5  #     jumps to .Ltmp400
	.byte	1                       #   On action: 1
	.long	.Ltmp399-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp395-.Ltmp399       #   Call between .Ltmp399 and .Ltmp395
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp395-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp396-.Ltmp395       #   Call between .Ltmp395 and .Ltmp396
	.long	.Ltmp397-.Lfunc_begin5  #     jumps to .Ltmp397
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z16CArchiveDatabaseD2Ev,"axG",@progbits,_ZN8NArchive3N7z16CArchiveDatabaseD2Ev,comdat
	.weak	_ZN8NArchive3N7z16CArchiveDatabaseD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CArchiveDatabaseD2Ev,@function
_ZN8NArchive3N7z16CArchiveDatabaseD2Ev: # @_ZN8NArchive3N7z16CArchiveDatabaseD2Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r15
.Lcfi149:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi150:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi151:
	.cfi_def_cfa_offset 32
.Lcfi152:
	.cfi_offset %rbx, -32
.Lcfi153:
	.cfi_offset %r14, -24
.Lcfi154:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	448(%r15), %rdi
.Ltmp401:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp402:
# BB#1:
	leaq	384(%r15), %rbx
	leaq	416(%r15), %rdi
.Ltmp412:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp413:
# BB#2:
.Ltmp418:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp419:
# BB#3:                                 # %_ZN8NArchive3N7z16CUInt64DefVectorD2Ev.exit
	leaq	320(%r15), %rbx
	leaq	352(%r15), %rdi
.Ltmp429:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp430:
# BB#4:
.Ltmp435:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp436:
# BB#5:                                 # %_ZN8NArchive3N7z16CUInt64DefVectorD2Ev.exit13
	leaq	256(%r15), %rbx
	leaq	288(%r15), %rdi
.Ltmp446:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp447:
# BB#6:
.Ltmp452:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp453:
# BB#7:                                 # %_ZN8NArchive3N7z16CUInt64DefVectorD2Ev.exit16
	leaq	192(%r15), %rbx
	leaq	224(%r15), %rdi
.Ltmp463:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp464:
# BB#8:
.Ltmp469:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp470:
# BB#9:                                 # %_ZN8NArchive3N7z16CUInt64DefVectorD2Ev.exit19
	leaq	160(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE+16, 160(%r15)
.Ltmp480:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp481:
# BB#10:
.Ltmp486:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp487:
# BB#11:                                # %_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev.exit
	movq	%r15, %rdi
	subq	$-128, %rdi
.Ltmp491:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp492:
# BB#12:
	leaq	96(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, 96(%r15)
.Ltmp502:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp503:
# BB#13:
.Ltmp508:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp509:
# BB#14:                                # %_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev.exit
	leaq	64(%r15), %rdi
.Ltmp513:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp514:
# BB#15:
	leaq	32(%r15), %rdi
.Ltmp518:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp519:
# BB#16:
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB9_62:
.Ltmp520:
	movq	%rax, %r14
	jmp	.LBB9_65
.LBB9_63:
.Ltmp515:
	movq	%rax, %r14
	jmp	.LBB9_64
.LBB9_55:
.Ltmp510:
	movq	%rax, %r14
	jmp	.LBB9_59
.LBB9_27:
.Ltmp504:
	movq	%rax, %r14
.Ltmp505:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp506:
	jmp	.LBB9_59
.LBB9_28:
.Ltmp507:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_56:
.Ltmp493:
	movq	%rax, %r14
	jmp	.LBB9_57
.LBB9_52:
.Ltmp488:
	movq	%rax, %r14
	jmp	.LBB9_39
.LBB9_25:
.Ltmp482:
	movq	%rax, %r14
.Ltmp483:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp484:
	jmp	.LBB9_39
.LBB9_26:
.Ltmp485:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_49:
.Ltmp471:
	movq	%rax, %r14
	jmp	.LBB9_37
.LBB9_23:
.Ltmp465:
	movq	%rax, %r14
.Ltmp466:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp467:
	jmp	.LBB9_37
.LBB9_24:
.Ltmp468:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_46:
.Ltmp454:
	movq	%rax, %r14
	jmp	.LBB9_35
.LBB9_21:
.Ltmp448:
	movq	%rax, %r14
.Ltmp449:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp450:
	jmp	.LBB9_35
.LBB9_22:
.Ltmp451:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_43:
.Ltmp437:
	movq	%rax, %r14
	jmp	.LBB9_33
.LBB9_19:
.Ltmp431:
	movq	%rax, %r14
.Ltmp432:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp433:
	jmp	.LBB9_33
.LBB9_20:
.Ltmp434:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_42:
.Ltmp420:
	movq	%rax, %r14
	jmp	.LBB9_31
.LBB9_17:
.Ltmp414:
	movq	%rax, %r14
.Ltmp415:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp416:
	jmp	.LBB9_31
.LBB9_18:
.Ltmp417:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_29:
.Ltmp403:
	movq	%rax, %r14
	leaq	384(%r15), %rbx
	leaq	416(%r15), %rdi
.Ltmp404:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp405:
# BB#30:
.Ltmp410:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp411:
.LBB9_31:                               # %_ZN8NArchive3N7z16CUInt64DefVectorD2Ev.exit26
	leaq	320(%r15), %rbx
	leaq	352(%r15), %rdi
.Ltmp421:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp422:
# BB#32:
.Ltmp427:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp428:
.LBB9_33:                               # %_ZN8NArchive3N7z16CUInt64DefVectorD2Ev.exit29
	leaq	256(%r15), %rbx
	leaq	288(%r15), %rdi
.Ltmp438:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp439:
# BB#34:
.Ltmp444:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp445:
.LBB9_35:                               # %_ZN8NArchive3N7z16CUInt64DefVectorD2Ev.exit32
	leaq	192(%r15), %rbx
	leaq	224(%r15), %rdi
.Ltmp455:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp456:
# BB#36:
.Ltmp461:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp462:
.LBB9_37:                               # %_ZN8NArchive3N7z16CUInt64DefVectorD2Ev.exit35
	leaq	160(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE+16, 160(%r15)
.Ltmp472:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp473:
# BB#38:
.Ltmp478:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp479:
.LBB9_39:                               # %_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev.exit38
	movq	%r15, %rdi
	subq	$-128, %rdi
.Ltmp489:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp490:
.LBB9_57:
	leaq	96(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, 96(%r15)
.Ltmp494:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp495:
# BB#58:
.Ltmp500:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp501:
.LBB9_59:                               # %_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev.exit41
	leaq	64(%r15), %rdi
.Ltmp511:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp512:
.LBB9_64:
	leaq	32(%r15), %rdi
.Ltmp516:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp517:
.LBB9_65:
.Ltmp521:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp522:
# BB#66:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB9_40:
.Ltmp406:
	movq	%rax, %r14
.Ltmp407:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp408:
	jmp	.LBB9_68
.LBB9_41:
.Ltmp409:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_44:
.Ltmp423:
	movq	%rax, %r14
.Ltmp424:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp425:
	jmp	.LBB9_68
.LBB9_45:
.Ltmp426:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_47:
.Ltmp440:
	movq	%rax, %r14
.Ltmp441:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp442:
	jmp	.LBB9_68
.LBB9_48:
.Ltmp443:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_50:
.Ltmp457:
	movq	%rax, %r14
.Ltmp458:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp459:
	jmp	.LBB9_68
.LBB9_51:
.Ltmp460:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_53:
.Ltmp474:
	movq	%rax, %r14
.Ltmp475:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp476:
	jmp	.LBB9_68
.LBB9_54:
.Ltmp477:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_60:
.Ltmp496:
	movq	%rax, %r14
.Ltmp497:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp498:
	jmp	.LBB9_68
.LBB9_61:
.Ltmp499:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_67:
.Ltmp523:
	movq	%rax, %r14
.LBB9_68:                               # %.body24
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN8NArchive3N7z16CArchiveDatabaseD2Ev, .Lfunc_end9-_ZN8NArchive3N7z16CArchiveDatabaseD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\253\204"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\242\004"              # Call site table length
	.long	.Ltmp401-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp402-.Ltmp401       #   Call between .Ltmp401 and .Ltmp402
	.long	.Ltmp403-.Lfunc_begin6  #     jumps to .Ltmp403
	.byte	0                       #   On action: cleanup
	.long	.Ltmp412-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp413-.Ltmp412       #   Call between .Ltmp412 and .Ltmp413
	.long	.Ltmp414-.Lfunc_begin6  #     jumps to .Ltmp414
	.byte	0                       #   On action: cleanup
	.long	.Ltmp418-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp419-.Ltmp418       #   Call between .Ltmp418 and .Ltmp419
	.long	.Ltmp420-.Lfunc_begin6  #     jumps to .Ltmp420
	.byte	0                       #   On action: cleanup
	.long	.Ltmp429-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp430-.Ltmp429       #   Call between .Ltmp429 and .Ltmp430
	.long	.Ltmp431-.Lfunc_begin6  #     jumps to .Ltmp431
	.byte	0                       #   On action: cleanup
	.long	.Ltmp435-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp436-.Ltmp435       #   Call between .Ltmp435 and .Ltmp436
	.long	.Ltmp437-.Lfunc_begin6  #     jumps to .Ltmp437
	.byte	0                       #   On action: cleanup
	.long	.Ltmp446-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp447-.Ltmp446       #   Call between .Ltmp446 and .Ltmp447
	.long	.Ltmp448-.Lfunc_begin6  #     jumps to .Ltmp448
	.byte	0                       #   On action: cleanup
	.long	.Ltmp452-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp453-.Ltmp452       #   Call between .Ltmp452 and .Ltmp453
	.long	.Ltmp454-.Lfunc_begin6  #     jumps to .Ltmp454
	.byte	0                       #   On action: cleanup
	.long	.Ltmp463-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Ltmp464-.Ltmp463       #   Call between .Ltmp463 and .Ltmp464
	.long	.Ltmp465-.Lfunc_begin6  #     jumps to .Ltmp465
	.byte	0                       #   On action: cleanup
	.long	.Ltmp469-.Lfunc_begin6  # >> Call Site 9 <<
	.long	.Ltmp470-.Ltmp469       #   Call between .Ltmp469 and .Ltmp470
	.long	.Ltmp471-.Lfunc_begin6  #     jumps to .Ltmp471
	.byte	0                       #   On action: cleanup
	.long	.Ltmp480-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp481-.Ltmp480       #   Call between .Ltmp480 and .Ltmp481
	.long	.Ltmp482-.Lfunc_begin6  #     jumps to .Ltmp482
	.byte	0                       #   On action: cleanup
	.long	.Ltmp486-.Lfunc_begin6  # >> Call Site 11 <<
	.long	.Ltmp487-.Ltmp486       #   Call between .Ltmp486 and .Ltmp487
	.long	.Ltmp488-.Lfunc_begin6  #     jumps to .Ltmp488
	.byte	0                       #   On action: cleanup
	.long	.Ltmp491-.Lfunc_begin6  # >> Call Site 12 <<
	.long	.Ltmp492-.Ltmp491       #   Call between .Ltmp491 and .Ltmp492
	.long	.Ltmp493-.Lfunc_begin6  #     jumps to .Ltmp493
	.byte	0                       #   On action: cleanup
	.long	.Ltmp502-.Lfunc_begin6  # >> Call Site 13 <<
	.long	.Ltmp503-.Ltmp502       #   Call between .Ltmp502 and .Ltmp503
	.long	.Ltmp504-.Lfunc_begin6  #     jumps to .Ltmp504
	.byte	0                       #   On action: cleanup
	.long	.Ltmp508-.Lfunc_begin6  # >> Call Site 14 <<
	.long	.Ltmp509-.Ltmp508       #   Call between .Ltmp508 and .Ltmp509
	.long	.Ltmp510-.Lfunc_begin6  #     jumps to .Ltmp510
	.byte	0                       #   On action: cleanup
	.long	.Ltmp513-.Lfunc_begin6  # >> Call Site 15 <<
	.long	.Ltmp514-.Ltmp513       #   Call between .Ltmp513 and .Ltmp514
	.long	.Ltmp515-.Lfunc_begin6  #     jumps to .Ltmp515
	.byte	0                       #   On action: cleanup
	.long	.Ltmp518-.Lfunc_begin6  # >> Call Site 16 <<
	.long	.Ltmp519-.Ltmp518       #   Call between .Ltmp518 and .Ltmp519
	.long	.Ltmp520-.Lfunc_begin6  #     jumps to .Ltmp520
	.byte	0                       #   On action: cleanup
	.long	.Ltmp519-.Lfunc_begin6  # >> Call Site 17 <<
	.long	.Ltmp505-.Ltmp519       #   Call between .Ltmp519 and .Ltmp505
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp505-.Lfunc_begin6  # >> Call Site 18 <<
	.long	.Ltmp506-.Ltmp505       #   Call between .Ltmp505 and .Ltmp506
	.long	.Ltmp507-.Lfunc_begin6  #     jumps to .Ltmp507
	.byte	1                       #   On action: 1
	.long	.Ltmp483-.Lfunc_begin6  # >> Call Site 19 <<
	.long	.Ltmp484-.Ltmp483       #   Call between .Ltmp483 and .Ltmp484
	.long	.Ltmp485-.Lfunc_begin6  #     jumps to .Ltmp485
	.byte	1                       #   On action: 1
	.long	.Ltmp466-.Lfunc_begin6  # >> Call Site 20 <<
	.long	.Ltmp467-.Ltmp466       #   Call between .Ltmp466 and .Ltmp467
	.long	.Ltmp468-.Lfunc_begin6  #     jumps to .Ltmp468
	.byte	1                       #   On action: 1
	.long	.Ltmp449-.Lfunc_begin6  # >> Call Site 21 <<
	.long	.Ltmp450-.Ltmp449       #   Call between .Ltmp449 and .Ltmp450
	.long	.Ltmp451-.Lfunc_begin6  #     jumps to .Ltmp451
	.byte	1                       #   On action: 1
	.long	.Ltmp432-.Lfunc_begin6  # >> Call Site 22 <<
	.long	.Ltmp433-.Ltmp432       #   Call between .Ltmp432 and .Ltmp433
	.long	.Ltmp434-.Lfunc_begin6  #     jumps to .Ltmp434
	.byte	1                       #   On action: 1
	.long	.Ltmp415-.Lfunc_begin6  # >> Call Site 23 <<
	.long	.Ltmp416-.Ltmp415       #   Call between .Ltmp415 and .Ltmp416
	.long	.Ltmp417-.Lfunc_begin6  #     jumps to .Ltmp417
	.byte	1                       #   On action: 1
	.long	.Ltmp404-.Lfunc_begin6  # >> Call Site 24 <<
	.long	.Ltmp405-.Ltmp404       #   Call between .Ltmp404 and .Ltmp405
	.long	.Ltmp406-.Lfunc_begin6  #     jumps to .Ltmp406
	.byte	1                       #   On action: 1
	.long	.Ltmp410-.Lfunc_begin6  # >> Call Site 25 <<
	.long	.Ltmp411-.Ltmp410       #   Call between .Ltmp410 and .Ltmp411
	.long	.Ltmp523-.Lfunc_begin6  #     jumps to .Ltmp523
	.byte	1                       #   On action: 1
	.long	.Ltmp421-.Lfunc_begin6  # >> Call Site 26 <<
	.long	.Ltmp422-.Ltmp421       #   Call between .Ltmp421 and .Ltmp422
	.long	.Ltmp423-.Lfunc_begin6  #     jumps to .Ltmp423
	.byte	1                       #   On action: 1
	.long	.Ltmp427-.Lfunc_begin6  # >> Call Site 27 <<
	.long	.Ltmp428-.Ltmp427       #   Call between .Ltmp427 and .Ltmp428
	.long	.Ltmp523-.Lfunc_begin6  #     jumps to .Ltmp523
	.byte	1                       #   On action: 1
	.long	.Ltmp438-.Lfunc_begin6  # >> Call Site 28 <<
	.long	.Ltmp439-.Ltmp438       #   Call between .Ltmp438 and .Ltmp439
	.long	.Ltmp440-.Lfunc_begin6  #     jumps to .Ltmp440
	.byte	1                       #   On action: 1
	.long	.Ltmp444-.Lfunc_begin6  # >> Call Site 29 <<
	.long	.Ltmp445-.Ltmp444       #   Call between .Ltmp444 and .Ltmp445
	.long	.Ltmp523-.Lfunc_begin6  #     jumps to .Ltmp523
	.byte	1                       #   On action: 1
	.long	.Ltmp455-.Lfunc_begin6  # >> Call Site 30 <<
	.long	.Ltmp456-.Ltmp455       #   Call between .Ltmp455 and .Ltmp456
	.long	.Ltmp457-.Lfunc_begin6  #     jumps to .Ltmp457
	.byte	1                       #   On action: 1
	.long	.Ltmp461-.Lfunc_begin6  # >> Call Site 31 <<
	.long	.Ltmp462-.Ltmp461       #   Call between .Ltmp461 and .Ltmp462
	.long	.Ltmp523-.Lfunc_begin6  #     jumps to .Ltmp523
	.byte	1                       #   On action: 1
	.long	.Ltmp472-.Lfunc_begin6  # >> Call Site 32 <<
	.long	.Ltmp473-.Ltmp472       #   Call between .Ltmp472 and .Ltmp473
	.long	.Ltmp474-.Lfunc_begin6  #     jumps to .Ltmp474
	.byte	1                       #   On action: 1
	.long	.Ltmp478-.Lfunc_begin6  # >> Call Site 33 <<
	.long	.Ltmp490-.Ltmp478       #   Call between .Ltmp478 and .Ltmp490
	.long	.Ltmp523-.Lfunc_begin6  #     jumps to .Ltmp523
	.byte	1                       #   On action: 1
	.long	.Ltmp494-.Lfunc_begin6  # >> Call Site 34 <<
	.long	.Ltmp495-.Ltmp494       #   Call between .Ltmp494 and .Ltmp495
	.long	.Ltmp496-.Lfunc_begin6  #     jumps to .Ltmp496
	.byte	1                       #   On action: 1
	.long	.Ltmp500-.Lfunc_begin6  # >> Call Site 35 <<
	.long	.Ltmp522-.Ltmp500       #   Call between .Ltmp500 and .Ltmp522
	.long	.Ltmp523-.Lfunc_begin6  #     jumps to .Ltmp523
	.byte	1                       #   On action: 1
	.long	.Ltmp522-.Lfunc_begin6  # >> Call Site 36 <<
	.long	.Ltmp407-.Ltmp522       #   Call between .Ltmp522 and .Ltmp407
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp407-.Lfunc_begin6  # >> Call Site 37 <<
	.long	.Ltmp408-.Ltmp407       #   Call between .Ltmp407 and .Ltmp408
	.long	.Ltmp409-.Lfunc_begin6  #     jumps to .Ltmp409
	.byte	1                       #   On action: 1
	.long	.Ltmp424-.Lfunc_begin6  # >> Call Site 38 <<
	.long	.Ltmp425-.Ltmp424       #   Call between .Ltmp424 and .Ltmp425
	.long	.Ltmp426-.Lfunc_begin6  #     jumps to .Ltmp426
	.byte	1                       #   On action: 1
	.long	.Ltmp441-.Lfunc_begin6  # >> Call Site 39 <<
	.long	.Ltmp442-.Ltmp441       #   Call between .Ltmp441 and .Ltmp442
	.long	.Ltmp443-.Lfunc_begin6  #     jumps to .Ltmp443
	.byte	1                       #   On action: 1
	.long	.Ltmp458-.Lfunc_begin6  # >> Call Site 40 <<
	.long	.Ltmp459-.Ltmp458       #   Call between .Ltmp458 and .Ltmp459
	.long	.Ltmp460-.Lfunc_begin6  #     jumps to .Ltmp460
	.byte	1                       #   On action: 1
	.long	.Ltmp475-.Lfunc_begin6  # >> Call Site 41 <<
	.long	.Ltmp476-.Ltmp475       #   Call between .Ltmp475 and .Ltmp476
	.long	.Ltmp477-.Lfunc_begin6  #     jumps to .Ltmp477
	.byte	1                       #   On action: 1
	.long	.Ltmp497-.Lfunc_begin6  # >> Call Site 42 <<
	.long	.Ltmp498-.Ltmp497       #   Call between .Ltmp497 and .Ltmp498
	.long	.Ltmp499-.Lfunc_begin6  #     jumps to .Ltmp499
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z11COutArchiveD2Ev,"axG",@progbits,_ZN8NArchive3N7z11COutArchiveD2Ev,comdat
	.weak	_ZN8NArchive3N7z11COutArchiveD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z11COutArchiveD2Ev,@function
_ZN8NArchive3N7z11COutArchiveD2Ev:      # @_ZN8NArchive3N7z11COutArchiveD2Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi155:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi156:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi157:
	.cfi_def_cfa_offset 32
.Lcfi158:
	.cfi_offset %rbx, -24
.Lcfi159:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp524:
	callq	*16(%rax)
.Ltmp525:
.LBB10_2:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp529:
	callq	*16(%rax)
.Ltmp530:
.LBB10_4:                               # %_ZN9CMyComPtrI10IOutStreamED2Ev.exit
	leaq	32(%rbx), %rdi
.Ltmp541:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp542:
# BB#5:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_11
# BB#6:
	movq	(%rdi), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*16(%rax)               # TAILCALL
.LBB10_11:                              # %_ZN10COutBufferD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB10_14:
.Ltmp531:
	movq	%rax, %r14
	jmp	.LBB10_15
.LBB10_12:
.Ltmp526:
	movq	%rax, %r14
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_15
# BB#13:
	movq	(%rdi), %rax
.Ltmp527:
	callq	*16(%rax)
.Ltmp528:
.LBB10_15:                              # %_ZN9CMyComPtrI10IOutStreamED2Ev.exit5
	leaq	32(%rbx), %rdi
.Ltmp532:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp533:
# BB#16:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_9
# BB#17:
	movq	(%rdi), %rax
.Ltmp538:
	callq	*16(%rax)
.Ltmp539:
	jmp	.LBB10_9
.LBB10_21:
.Ltmp540:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB10_18:
.Ltmp534:
	movq	%rax, %r14
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_22
# BB#19:
	movq	(%rdi), %rax
.Ltmp535:
	callq	*16(%rax)
.Ltmp536:
.LBB10_22:                              # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB10_20:
.Ltmp537:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB10_7:
.Ltmp543:
	movq	%rax, %r14
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_9
# BB#8:
	movq	(%rdi), %rax
.Ltmp544:
	callq	*16(%rax)
.Ltmp545:
.LBB10_9:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB10_10:
.Ltmp546:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end10:
	.size	_ZN8NArchive3N7z11COutArchiveD2Ev, .Lfunc_end10-_ZN8NArchive3N7z11COutArchiveD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp524-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp525-.Ltmp524       #   Call between .Ltmp524 and .Ltmp525
	.long	.Ltmp526-.Lfunc_begin7  #     jumps to .Ltmp526
	.byte	0                       #   On action: cleanup
	.long	.Ltmp529-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp530-.Ltmp529       #   Call between .Ltmp529 and .Ltmp530
	.long	.Ltmp531-.Lfunc_begin7  #     jumps to .Ltmp531
	.byte	0                       #   On action: cleanup
	.long	.Ltmp541-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp542-.Ltmp541       #   Call between .Ltmp541 and .Ltmp542
	.long	.Ltmp543-.Lfunc_begin7  #     jumps to .Ltmp543
	.byte	0                       #   On action: cleanup
	.long	.Ltmp542-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp527-.Ltmp542       #   Call between .Ltmp542 and .Ltmp527
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp527-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Ltmp528-.Ltmp527       #   Call between .Ltmp527 and .Ltmp528
	.long	.Ltmp540-.Lfunc_begin7  #     jumps to .Ltmp540
	.byte	1                       #   On action: 1
	.long	.Ltmp532-.Lfunc_begin7  # >> Call Site 6 <<
	.long	.Ltmp533-.Ltmp532       #   Call between .Ltmp532 and .Ltmp533
	.long	.Ltmp534-.Lfunc_begin7  #     jumps to .Ltmp534
	.byte	1                       #   On action: 1
	.long	.Ltmp538-.Lfunc_begin7  # >> Call Site 7 <<
	.long	.Ltmp539-.Ltmp538       #   Call between .Ltmp538 and .Ltmp539
	.long	.Ltmp540-.Lfunc_begin7  #     jumps to .Ltmp540
	.byte	1                       #   On action: 1
	.long	.Ltmp535-.Lfunc_begin7  # >> Call Site 8 <<
	.long	.Ltmp536-.Ltmp535       #   Call between .Ltmp535 and .Ltmp536
	.long	.Ltmp537-.Lfunc_begin7  #     jumps to .Ltmp537
	.byte	1                       #   On action: 1
	.long	.Ltmp544-.Lfunc_begin7  # >> Call Site 9 <<
	.long	.Ltmp545-.Ltmp544       #   Call between .Ltmp544 and .Ltmp545
	.long	.Ltmp546-.Lfunc_begin7  #     jumps to .Ltmp546
	.byte	1                       #   On action: 1
	.long	.Ltmp545-.Lfunc_begin7  # >> Call Site 10 <<
	.long	.Lfunc_end10-.Ltmp545   #   Call between .Ltmp545 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z22CCompressionMethodModeD2Ev,"axG",@progbits,_ZN8NArchive3N7z22CCompressionMethodModeD2Ev,comdat
	.weak	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev,@function
_ZN8NArchive3N7z22CCompressionMethodModeD2Ev: # @_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi161:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi162:
	.cfi_def_cfa_offset 32
.Lcfi163:
	.cfi_offset %rbx, -24
.Lcfi164:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_2
# BB#1:
	callq	_ZdaPv
.LBB11_2:                               # %_ZN11CStringBaseIwED2Ev.exit
	leaq	32(%rbx), %rdi
.Ltmp547:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp548:
# BB#3:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp559:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp560:
# BB#4:                                 # %_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB11_5:
.Ltmp561:
	movq	%rax, %r14
.Ltmp562:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp563:
	jmp	.LBB11_6
.LBB11_7:
.Ltmp564:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB11_8:
.Ltmp549:
	movq	%rax, %r14
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp550:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp551:
# BB#9:
.Ltmp556:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp557:
.LBB11_6:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB11_12:
.Ltmp558:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB11_10:
.Ltmp552:
	movq	%rax, %r14
.Ltmp553:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp554:
# BB#13:                                # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB11_11:
.Ltmp555:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end11:
	.size	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev, .Lfunc_end11-_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp547-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp548-.Ltmp547       #   Call between .Ltmp547 and .Ltmp548
	.long	.Ltmp549-.Lfunc_begin8  #     jumps to .Ltmp549
	.byte	0                       #   On action: cleanup
	.long	.Ltmp559-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp560-.Ltmp559       #   Call between .Ltmp559 and .Ltmp560
	.long	.Ltmp561-.Lfunc_begin8  #     jumps to .Ltmp561
	.byte	0                       #   On action: cleanup
	.long	.Ltmp560-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp562-.Ltmp560       #   Call between .Ltmp560 and .Ltmp562
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp562-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp563-.Ltmp562       #   Call between .Ltmp562 and .Ltmp563
	.long	.Ltmp564-.Lfunc_begin8  #     jumps to .Ltmp564
	.byte	1                       #   On action: 1
	.long	.Ltmp550-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Ltmp551-.Ltmp550       #   Call between .Ltmp550 and .Ltmp551
	.long	.Ltmp552-.Lfunc_begin8  #     jumps to .Ltmp552
	.byte	1                       #   On action: 1
	.long	.Ltmp556-.Lfunc_begin8  # >> Call Site 6 <<
	.long	.Ltmp557-.Ltmp556       #   Call between .Ltmp556 and .Ltmp557
	.long	.Ltmp558-.Lfunc_begin8  #     jumps to .Ltmp558
	.byte	1                       #   On action: 1
	.long	.Ltmp557-.Lfunc_begin8  # >> Call Site 7 <<
	.long	.Ltmp553-.Ltmp557       #   Call between .Ltmp557 and .Ltmp553
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp553-.Lfunc_begin8  # >> Call Site 8 <<
	.long	.Ltmp554-.Ltmp553       #   Call between .Ltmp553 and .Ltmp554
	.long	.Ltmp555-.Lfunc_begin8  #     jumps to .Ltmp555
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED2Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi165:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi166:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi167:
	.cfi_def_cfa_offset 32
.Lcfi168:
	.cfi_offset %rbx, -24
.Lcfi169:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CUpdateItemEE+16, (%rbx)
.Ltmp565:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp566:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB12_2:
.Ltmp567:
	movq	%rax, %r14
.Ltmp568:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp569:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB12_4:
.Ltmp570:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED2Ev, .Lfunc_end12-_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp565-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp566-.Ltmp565       #   Call between .Ltmp565 and .Ltmp566
	.long	.Ltmp567-.Lfunc_begin9  #     jumps to .Ltmp567
	.byte	0                       #   On action: cleanup
	.long	.Ltmp566-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp568-.Ltmp566       #   Call between .Ltmp566 and .Ltmp568
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp568-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp569-.Ltmp568       #   Call between .Ltmp568 and .Ltmp569
	.long	.Ltmp570-.Lfunc_begin9  #     jumps to .Ltmp570
	.byte	1                       #   On action: 1
	.long	.Ltmp569-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Lfunc_end12-.Ltmp569   #   Call between .Ltmp569 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn120_N8NArchive3N7z8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.p2align	4, 0x90
	.type	_ZThn120_N8NArchive3N7z8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback,@function
_ZThn120_N8NArchive3N7z8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback: # @_ZThn120_N8NArchive3N7z8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.cfi_startproc
# BB#0:
	addq	$-120, %rdi
	jmp	_ZN8NArchive3N7z8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback # TAILCALL
.Lfunc_end13:
	.size	_ZThn120_N8NArchive3N7z8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback, .Lfunc_end13-_ZThn120_N8NArchive3N7z8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI14_0:
	.zero	16
	.text
	.globl	_ZN8NArchive3N7z8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi,@function
_ZN8NArchive3N7z8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi: # @_ZN8NArchive3N7z8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%rbp
.Lcfi170:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi171:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi172:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi173:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi174:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi175:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi176:
	.cfi_def_cfa_offset 160
.Lcfi177:
	.cfi_offset %rbx, -56
.Lcfi178:
	.cfi_offset %r12, -48
.Lcfi179:
	.cfi_offset %r13, -40
.Lcfi180:
	.cfi_offset %r14, -32
.Lcfi181:
	.cfi_offset %r15, -24
.Lcfi182:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %rbx
	leaq	848(%rbx), %rbp
.Ltmp571:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp572:
# BB#1:
	leaq	8(%rbx), %rdi
.Ltmp573:
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	callq	_ZN8NArchive11COutHandler17BeforeSetPropertyEv
.Ltmp574:
# BB#2:                                 # %.preheader
	movl	$2, %ecx
	testl	%r14d, %r14d
	jle	.LBB14_3
# BB#8:                                 # %.lr.ph
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movslq	%r14d, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
                                        # implicit-def: %EBP
	.p2align	4, 0x90
.LBB14_9:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_10 Depth 2
                                        #     Child Loop BB14_13 Depth 2
	movq	(%r12,%r14,8), %r15
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %r13d
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB14_10:                              #   Parent Loop BB14_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%r13d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB14_10
# BB#11:                                # %_Z11MyStringLenIwEiPKT_.exit.i
                                        #   in Loop: Header=BB14_9 Depth=1
	leal	1(%r13), %ebx
	movslq	%ebx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp576:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp577:
# BB#12:                                # %.noexc
                                        #   in Loop: Header=BB14_9 Depth=1
	movq	%rax, (%rsp)
	movl	$0, (%rax)
	movl	%ebx, 12(%rsp)
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB14_13:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   Parent Loop BB14_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r15), %edx
	addq	$4, %r15
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB14_13
# BB#14:                                #   in Loop: Header=BB14_9 Depth=1
	movl	%r13d, 8(%rsp)
.Ltmp579:
	movq	%rax, %rdi
	callq	_Z13MyStringUpperPw
.Ltmp580:
# BB#15:                                # %_ZN11CStringBaseIwE9MakeUpperEv.exit
                                        #   in Loop: Header=BB14_9 Depth=1
	movl	8(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB14_16
# BB#21:                                #   in Loop: Header=BB14_9 Depth=1
	movq	(%rsp), %rsi
	cmpl	$66, (%rsi)
	jne	.LBB14_36
# BB#22:                                #   in Loop: Header=BB14_9 Depth=1
	cmpl	$2, %ecx
	movl	$1, %eax
	cmovll	%ecx, %eax
	testl	%ecx, %ecx
	jle	.LBB14_24
# BB#23:                                #   in Loop: Header=BB14_9 Depth=1
	movslq	%eax, %rbx
	leaq	(%rsi,%rbx,4), %rax
	incl	%ecx
	subl	%ebx, %ecx
	movslq	%ecx, %rdx
	shlq	$2, %rdx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	memmove
	subl	%ebx, 8(%rsp)
.LBB14_24:                              # %_ZN11CStringBaseIwE6DeleteEii.exit
                                        #   in Loop: Header=BB14_9 Depth=1
.Ltmp585:
	movq	%rsp, %rdi
	leaq	24(%rsp), %rsi
	leaq	28(%rsp), %rdx
	callq	_ZN8NArchive3N7zL15GetBindInfoPartER11CStringBaseIwERjS4_
.Ltmp586:
# BB#25:                                # %.noexc56
                                        #   in Loop: Header=BB14_9 Depth=1
	testl	%eax, %eax
	jne	.LBB14_37
# BB#26:                                #   in Loop: Header=BB14_9 Depth=1
	movq	(%rsp), %rdi
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$58, (%rdi)
	jne	.LBB14_37
# BB#27:                                #   in Loop: Header=BB14_9 Depth=1
	movl	8(%rsp), %eax
	cmpl	$2, %eax
	movl	$1, %ecx
	cmovll	%eax, %ecx
	testl	%eax, %eax
	jle	.LBB14_29
# BB#28:                                #   in Loop: Header=BB14_9 Depth=1
	movslq	%ecx, %rbx
	leaq	(%rdi,%rbx,4), %rsi
	incl	%eax
	subl	%ebx, %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	callq	memmove
	subl	%ebx, 8(%rsp)
.LBB14_29:                              # %_ZN11CStringBaseIwE6DeleteEii.exit.i
                                        #   in Loop: Header=BB14_9 Depth=1
.Ltmp587:
	movq	%rsp, %rdi
	leaq	16(%rsp), %rsi
	leaq	20(%rsp), %rdx
	callq	_ZN8NArchive3N7zL15GetBindInfoPartER11CStringBaseIwERjS4_
.Ltmp588:
# BB#30:                                # %.noexc57
                                        #   in Loop: Header=BB14_9 Depth=1
	testl	%eax, %eax
	jne	.LBB14_37
# BB#31:                                #   in Loop: Header=BB14_9 Depth=1
	cmpl	$0, 8(%rsp)
	movl	$-2147024809, %eax      # imm = 0x80070057
	jne	.LBB14_37
# BB#32:                                #   in Loop: Header=BB14_9 Depth=1
	movaps	16(%rsp), %xmm0
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
.Ltmp589:
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp590:
# BB#33:                                # %_ZN13CRecordVectorIN8NArchive3N7z5CBindEE3AddES2_.exit
                                        #   in Loop: Header=BB14_9 Depth=1
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	864(%rdx), %rax
	movslq	860(%rdx), %rcx
	shlq	$4, %rcx
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	movups	%xmm0, (%rax,%rcx)
	incl	860(%rdx)
	movl	$4, %ebx
	jmp	.LBB14_38
	.p2align	4, 0x90
.LBB14_16:                              #   in Loop: Header=BB14_9 Depth=1
	movl	$-2147024809, %ebp      # imm = 0x80070057
	movl	$1, %ebx
	jmp	.LBB14_38
	.p2align	4, 0x90
.LBB14_36:                              #   in Loop: Header=BB14_9 Depth=1
	movq	%r14, %rdx
	shlq	$4, %rdx
	addq	64(%rsp), %rdx          # 8-byte Folded Reload
.Ltmp582:
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	_ZN8NArchive11COutHandler11SetPropertyEPKwRK14tagPROPVARIANT
.Ltmp583:
.LBB14_37:                              #   in Loop: Header=BB14_9 Depth=1
	xorl	%ebx, %ebx
	testl	%eax, %eax
	setne	%bl
	cmovnel	%eax, %ebp
.LBB14_38:                              #   in Loop: Header=BB14_9 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_40
# BB#39:                                #   in Loop: Header=BB14_9 Depth=1
	callq	_ZdaPv
.LBB14_40:                              # %_ZN11CStringBaseIwED2Ev.exit54
                                        #   in Loop: Header=BB14_9 Depth=1
	movl	%ebx, %eax
	orl	$4, %eax
	cmpl	$4, %eax
	jne	.LBB14_41
# BB#42:                                #   in Loop: Header=BB14_9 Depth=1
	incq	%r14
	cmpq	72(%rsp), %r14          # 8-byte Folded Reload
	jl	.LBB14_9
# BB#43:
	movl	$2, %ecx
	jmp	.LBB14_44
.LBB14_3:
                                        # implicit-def: %EBP
	jmp	.LBB14_44
.LBB14_41:
	movl	%ebx, %ecx
.LBB14_44:                              # %._crit_edge
	xorl	%eax, %eax
	cmpl	$2, %ecx
	cmovnel	%ebp, %eax
	jmp	.LBB14_45
.LBB14_4:
.Ltmp575:
	jmp	.LBB14_5
.LBB14_34:
.Ltmp584:
	jmp	.LBB14_19
.LBB14_35:
.Ltmp591:
	jmp	.LBB14_19
.LBB14_18:
.Ltmp581:
.LBB14_19:
	movq	%rdx, %rbx
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_6
# BB#20:
	callq	_ZdaPv
	jmp	.LBB14_6
.LBB14_17:
.Ltmp578:
.LBB14_5:
	movq	%rdx, %rbx
	movq	%rax, %rbp
.LBB14_6:
	movq	%rbp, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbp
	cmpl	$2, %ebx
	je	.LBB14_7
# BB#46:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
.LBB14_45:                              # %._crit_edge
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_7:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbp, (%rax)
.Ltmp592:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp593:
# BB#48:
.LBB14_47:
.Ltmp594:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZN8NArchive3N7z8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi, .Lfunc_end14-_ZN8NArchive3N7z8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\236\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp571-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp574-.Ltmp571       #   Call between .Ltmp571 and .Ltmp574
	.long	.Ltmp575-.Lfunc_begin10 #     jumps to .Ltmp575
	.byte	3                       #   On action: 2
	.long	.Ltmp576-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp577-.Ltmp576       #   Call between .Ltmp576 and .Ltmp577
	.long	.Ltmp578-.Lfunc_begin10 #     jumps to .Ltmp578
	.byte	3                       #   On action: 2
	.long	.Ltmp579-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp580-.Ltmp579       #   Call between .Ltmp579 and .Ltmp580
	.long	.Ltmp581-.Lfunc_begin10 #     jumps to .Ltmp581
	.byte	3                       #   On action: 2
	.long	.Ltmp580-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Ltmp585-.Ltmp580       #   Call between .Ltmp580 and .Ltmp585
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp585-.Lfunc_begin10 # >> Call Site 5 <<
	.long	.Ltmp586-.Ltmp585       #   Call between .Ltmp585 and .Ltmp586
	.long	.Ltmp591-.Lfunc_begin10 #     jumps to .Ltmp591
	.byte	3                       #   On action: 2
	.long	.Ltmp586-.Lfunc_begin10 # >> Call Site 6 <<
	.long	.Ltmp587-.Ltmp586       #   Call between .Ltmp586 and .Ltmp587
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp587-.Lfunc_begin10 # >> Call Site 7 <<
	.long	.Ltmp590-.Ltmp587       #   Call between .Ltmp587 and .Ltmp590
	.long	.Ltmp591-.Lfunc_begin10 #     jumps to .Ltmp591
	.byte	3                       #   On action: 2
	.long	.Ltmp582-.Lfunc_begin10 # >> Call Site 8 <<
	.long	.Ltmp583-.Ltmp582       #   Call between .Ltmp582 and .Ltmp583
	.long	.Ltmp584-.Lfunc_begin10 #     jumps to .Ltmp584
	.byte	3                       #   On action: 2
	.long	.Ltmp583-.Lfunc_begin10 # >> Call Site 9 <<
	.long	.Ltmp592-.Ltmp583       #   Call between .Ltmp583 and .Ltmp592
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp592-.Lfunc_begin10 # >> Call Site 10 <<
	.long	.Ltmp593-.Ltmp592       #   Call between .Ltmp592 and .Ltmp593
	.long	.Ltmp594-.Lfunc_begin10 #     jumps to .Ltmp594
	.byte	0                       #   On action: cleanup
	.long	.Ltmp593-.Lfunc_begin10 # >> Call Site 11 <<
	.long	.Lfunc_end14-.Ltmp593   #   Call between .Ltmp593 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn112_N8NArchive3N7z8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.p2align	4, 0x90
	.type	_ZThn112_N8NArchive3N7z8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi,@function
_ZThn112_N8NArchive3N7z8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi: # @_ZThn112_N8NArchive3N7z8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.cfi_startproc
# BB#0:
	addq	$-112, %rdi
	jmp	_ZN8NArchive3N7z8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi # TAILCALL
.Lfunc_end15:
	.size	_ZThn112_N8NArchive3N7z8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi, .Lfunc_end15-_ZThn112_N8NArchive3N7z8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.cfi_endproc

	.section	.text._ZN13CObjectVectorI5CPropED2Ev,"axG",@progbits,_ZN13CObjectVectorI5CPropED2Ev,comdat
	.weak	_ZN13CObjectVectorI5CPropED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropED2Ev,@function
_ZN13CObjectVectorI5CPropED2Ev:         # @_ZN13CObjectVectorI5CPropED2Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi183:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi184:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi185:
	.cfi_def_cfa_offset 32
.Lcfi186:
	.cfi_offset %rbx, -24
.Lcfi187:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbx)
.Ltmp595:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp596:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB16_2:
.Ltmp597:
	movq	%rax, %r14
.Ltmp598:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp599:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB16_4:
.Ltmp600:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN13CObjectVectorI5CPropED2Ev, .Lfunc_end16-_ZN13CObjectVectorI5CPropED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp595-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp596-.Ltmp595       #   Call between .Ltmp595 and .Ltmp596
	.long	.Ltmp597-.Lfunc_begin11 #     jumps to .Ltmp597
	.byte	0                       #   On action: cleanup
	.long	.Ltmp596-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp598-.Ltmp596       #   Call between .Ltmp596 and .Ltmp598
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp598-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp599-.Ltmp598       #   Call between .Ltmp598 and .Ltmp599
	.long	.Ltmp600-.Lfunc_begin11 #     jumps to .Ltmp600
	.byte	1                       #   On action: 1
	.long	.Ltmp599-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Lfunc_end16-.Ltmp599   #   Call between .Ltmp599 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropED0Ev,"axG",@progbits,_ZN13CObjectVectorI5CPropED0Ev,comdat
	.weak	_ZN13CObjectVectorI5CPropED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropED0Ev,@function
_ZN13CObjectVectorI5CPropED0Ev:         # @_ZN13CObjectVectorI5CPropED0Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi188:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi189:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi190:
	.cfi_def_cfa_offset 32
.Lcfi191:
	.cfi_offset %rbx, -24
.Lcfi192:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbx)
.Ltmp601:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp602:
# BB#1:
.Ltmp607:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp608:
# BB#2:                                 # %_ZN13CObjectVectorI5CPropED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB17_5:
.Ltmp609:
	movq	%rax, %r14
	jmp	.LBB17_6
.LBB17_3:
.Ltmp603:
	movq	%rax, %r14
.Ltmp604:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp605:
.LBB17_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB17_4:
.Ltmp606:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end17:
	.size	_ZN13CObjectVectorI5CPropED0Ev, .Lfunc_end17-_ZN13CObjectVectorI5CPropED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp601-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp602-.Ltmp601       #   Call between .Ltmp601 and .Ltmp602
	.long	.Ltmp603-.Lfunc_begin12 #     jumps to .Ltmp603
	.byte	0                       #   On action: cleanup
	.long	.Ltmp607-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp608-.Ltmp607       #   Call between .Ltmp607 and .Ltmp608
	.long	.Ltmp609-.Lfunc_begin12 #     jumps to .Ltmp609
	.byte	0                       #   On action: cleanup
	.long	.Ltmp604-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp605-.Ltmp604       #   Call between .Ltmp604 and .Ltmp605
	.long	.Ltmp606-.Lfunc_begin12 #     jumps to .Ltmp606
	.byte	1                       #   On action: 1
	.long	.Ltmp605-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Lfunc_end17-.Ltmp605   #   Call between .Ltmp605 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI5CPropE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI5CPropE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropE6DeleteEii,@function
_ZN13CObjectVectorI5CPropE6DeleteEii:   # @_ZN13CObjectVectorI5CPropE6DeleteEii
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi193:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi194:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi195:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi196:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi197:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi198:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi199:
	.cfi_def_cfa_offset 64
.Lcfi200:
	.cfi_offset %rbx, -56
.Lcfi201:
	.cfi_offset %r12, -48
.Lcfi202:
	.cfi_offset %r13, -40
.Lcfi203:
	.cfi_offset %r14, -32
.Lcfi204:
	.cfi_offset %r15, -24
.Lcfi205:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB18_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB18_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB18_5
# BB#3:                                 #   in Loop: Header=BB18_2 Depth=1
	leaq	8(%rbp), %rdi
.Ltmp610:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp611:
# BB#4:                                 # %_ZN5CPropD2Ev.exit
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB18_5:                               #   in Loop: Header=BB18_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB18_2
.LBB18_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB18_7:
.Ltmp612:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end18:
	.size	_ZN13CObjectVectorI5CPropE6DeleteEii, .Lfunc_end18-_ZN13CObjectVectorI5CPropE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp610-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp611-.Ltmp610       #   Call between .Ltmp610 and .Ltmp611
	.long	.Ltmp612-.Lfunc_begin13 #     jumps to .Ltmp612
	.byte	0                       #   On action: cleanup
	.long	.Ltmp611-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Lfunc_end18-.Ltmp611   #   Call between .Ltmp611 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi206:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi207:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi208:
	.cfi_def_cfa_offset 32
.Lcfi209:
	.cfi_offset %rbx, -24
.Lcfi210:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp613:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp614:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB19_2:
.Ltmp615:
	movq	%rax, %r14
.Ltmp616:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp617:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB19_4:
.Ltmp618:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end19:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev, .Lfunc_end19-_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp613-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp614-.Ltmp613       #   Call between .Ltmp613 and .Ltmp614
	.long	.Ltmp615-.Lfunc_begin14 #     jumps to .Ltmp615
	.byte	0                       #   On action: cleanup
	.long	.Ltmp614-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp616-.Ltmp614       #   Call between .Ltmp614 and .Ltmp616
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp616-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp617-.Ltmp616       #   Call between .Ltmp616 and .Ltmp617
	.long	.Ltmp618-.Lfunc_begin14 #     jumps to .Ltmp618
	.byte	1                       #   On action: 1
	.long	.Ltmp617-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Lfunc_end19-.Ltmp617   #   Call between .Ltmp617 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi211:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi212:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi213:
	.cfi_def_cfa_offset 32
.Lcfi214:
	.cfi_offset %rbx, -24
.Lcfi215:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp619:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp620:
# BB#1:
.Ltmp625:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp626:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB20_5:
.Ltmp627:
	movq	%rax, %r14
	jmp	.LBB20_6
.LBB20_3:
.Ltmp621:
	movq	%rax, %r14
.Ltmp622:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp623:
.LBB20_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB20_4:
.Ltmp624:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end20:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev, .Lfunc_end20-_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp619-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp620-.Ltmp619       #   Call between .Ltmp619 and .Ltmp620
	.long	.Ltmp621-.Lfunc_begin15 #     jumps to .Ltmp621
	.byte	0                       #   On action: cleanup
	.long	.Ltmp625-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp626-.Ltmp625       #   Call between .Ltmp625 and .Ltmp626
	.long	.Ltmp627-.Lfunc_begin15 #     jumps to .Ltmp627
	.byte	0                       #   On action: cleanup
	.long	.Ltmp622-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp623-.Ltmp622       #   Call between .Ltmp622 and .Ltmp623
	.long	.Ltmp624-.Lfunc_begin15 #     jumps to .Ltmp624
	.byte	1                       #   On action: 1
	.long	.Ltmp623-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Lfunc_end20-.Ltmp623   #   Call between .Ltmp623 and .Lfunc_end20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi216:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi217:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi218:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi219:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi220:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi221:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi222:
	.cfi_def_cfa_offset 80
.Lcfi223:
	.cfi_offset %rbx, -56
.Lcfi224:
	.cfi_offset %r12, -48
.Lcfi225:
	.cfi_offset %r13, -40
.Lcfi226:
	.cfi_offset %r14, -32
.Lcfi227:
	.cfi_offset %r15, -24
.Lcfi228:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %edi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	subl	%esi, %edi
	cmpl	%ecx, %eax
	cmovlel	%edx, %edi
	movl	%edi, 12(%rsp)          # 4-byte Spill
	testl	%edi, %edi
	jle	.LBB21_7
# BB#1:                                 # %.lr.ph
	movslq	16(%rsp), %r13          # 4-byte Folded Reload
	movslq	12(%rsp), %r14          # 4-byte Folded Reload
	shlq	$3, %r13
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB21_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%r13, %rax
	movq	(%rax,%r15,8), %rbp
	testq	%rbp, %rbp
	je	.LBB21_6
# BB#3:                                 #   in Loop: Header=BB21_2 Depth=1
	leaq	8(%rbp), %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, 8(%rbp)
.Ltmp628:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp629:
# BB#4:                                 # %_ZN13CObjectVectorI5CPropED2Ev.exit.i
                                        #   in Loop: Header=BB21_2 Depth=1
.Ltmp634:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp635:
# BB#5:                                 # %_ZN7CMethodD2Ev.exit
                                        #   in Loop: Header=BB21_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB21_6:                               #   in Loop: Header=BB21_2 Depth=1
	incq	%r15
	cmpq	%r14, %r15
	jl	.LBB21_2
.LBB21_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	12(%rsp), %edx          # 4-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB21_8:
.Ltmp630:
	movq	%rax, %r14
.Ltmp631:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp632:
	jmp	.LBB21_11
.LBB21_9:
.Ltmp633:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB21_10:
.Ltmp636:
	movq	%rax, %r14
.LBB21_11:                              # %.body
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end21:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii, .Lfunc_end21-_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp628-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp629-.Ltmp628       #   Call between .Ltmp628 and .Ltmp629
	.long	.Ltmp630-.Lfunc_begin16 #     jumps to .Ltmp630
	.byte	0                       #   On action: cleanup
	.long	.Ltmp634-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp635-.Ltmp634       #   Call between .Ltmp634 and .Ltmp635
	.long	.Ltmp636-.Lfunc_begin16 #     jumps to .Ltmp636
	.byte	0                       #   On action: cleanup
	.long	.Ltmp635-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Ltmp631-.Ltmp635       #   Call between .Ltmp635 and .Ltmp631
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp631-.Lfunc_begin16 # >> Call Site 4 <<
	.long	.Ltmp632-.Ltmp631       #   Call between .Ltmp631 and .Ltmp632
	.long	.Ltmp633-.Lfunc_begin16 #     jumps to .Ltmp633
	.byte	1                       #   On action: 1
	.long	.Ltmp632-.Lfunc_begin16 # >> Call Site 5 <<
	.long	.Lfunc_end21-.Ltmp632   #   Call between .Ltmp632 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev,"axG",@progbits,_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev,comdat
	.weak	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev,@function
_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev: # @_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi229:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi230:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi231:
	.cfi_def_cfa_offset 32
.Lcfi232:
	.cfi_offset %rbx, -24
.Lcfi233:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp637:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp638:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB22_2:
.Ltmp639:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end22:
	.size	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev, .Lfunc_end22-_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp637-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp638-.Ltmp637       #   Call between .Ltmp637 and .Ltmp638
	.long	.Ltmp639-.Lfunc_begin17 #     jumps to .Ltmp639
	.byte	0                       #   On action: cleanup
	.long	.Ltmp638-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Lfunc_end22-.Ltmp638   #   Call between .Ltmp638 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%r14
.Lcfi234:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi235:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi236:
	.cfi_def_cfa_offset 32
.Lcfi237:
	.cfi_offset %rbx, -24
.Lcfi238:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE+16, (%rbx)
.Ltmp640:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp641:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB23_2:
.Ltmp642:
	movq	%rax, %r14
.Ltmp643:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp644:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB23_4:
.Ltmp645:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end23:
	.size	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev, .Lfunc_end23-_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp640-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp641-.Ltmp640       #   Call between .Ltmp640 and .Ltmp641
	.long	.Ltmp642-.Lfunc_begin18 #     jumps to .Ltmp642
	.byte	0                       #   On action: cleanup
	.long	.Ltmp641-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Ltmp643-.Ltmp641       #   Call between .Ltmp641 and .Ltmp643
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp643-.Lfunc_begin18 # >> Call Site 3 <<
	.long	.Ltmp644-.Ltmp643       #   Call between .Ltmp643 and .Ltmp644
	.long	.Ltmp645-.Lfunc_begin18 #     jumps to .Ltmp645
	.byte	1                       #   On action: 1
	.long	.Ltmp644-.Lfunc_begin18 # >> Call Site 4 <<
	.long	.Lfunc_end23-.Ltmp644   #   Call between .Ltmp644 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:
	pushq	%r14
.Lcfi239:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi240:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi241:
	.cfi_def_cfa_offset 32
.Lcfi242:
	.cfi_offset %rbx, -24
.Lcfi243:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, (%rbx)
.Ltmp646:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp647:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB24_2:
.Ltmp648:
	movq	%rax, %r14
.Ltmp649:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp650:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB24_4:
.Ltmp651:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end24:
	.size	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev, .Lfunc_end24-_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table24:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp646-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp647-.Ltmp646       #   Call between .Ltmp646 and .Ltmp647
	.long	.Ltmp648-.Lfunc_begin19 #     jumps to .Ltmp648
	.byte	0                       #   On action: cleanup
	.long	.Ltmp647-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Ltmp649-.Ltmp647       #   Call between .Ltmp647 and .Ltmp649
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp649-.Lfunc_begin19 # >> Call Site 3 <<
	.long	.Ltmp650-.Ltmp649       #   Call between .Ltmp649 and .Ltmp650
	.long	.Ltmp651-.Lfunc_begin19 #     jumps to .Ltmp651
	.byte	1                       #   On action: 1
	.long	.Ltmp650-.Lfunc_begin19 # >> Call Site 4 <<
	.long	.Lfunc_end24-.Ltmp650   #   Call between .Ltmp650 and .Lfunc_end24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIyED0Ev,"axG",@progbits,_ZN13CRecordVectorIyED0Ev,comdat
	.weak	_ZN13CRecordVectorIyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyED0Ev,@function
_ZN13CRecordVectorIyED0Ev:              # @_ZN13CRecordVectorIyED0Ev
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%r14
.Lcfi244:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi245:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi246:
	.cfi_def_cfa_offset 32
.Lcfi247:
	.cfi_offset %rbx, -24
.Lcfi248:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp652:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp653:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB25_2:
.Ltmp654:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end25:
	.size	_ZN13CRecordVectorIyED0Ev, .Lfunc_end25-_ZN13CRecordVectorIyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp652-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp653-.Ltmp652       #   Call between .Ltmp652 and .Ltmp653
	.long	.Ltmp654-.Lfunc_begin20 #     jumps to .Ltmp654
	.byte	0                       #   On action: cleanup
	.long	.Ltmp653-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Lfunc_end25-.Ltmp653   #   Call between .Ltmp653 and .Lfunc_end25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIbED0Ev,"axG",@progbits,_ZN13CRecordVectorIbED0Ev,comdat
	.weak	_ZN13CRecordVectorIbED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIbED0Ev,@function
_ZN13CRecordVectorIbED0Ev:              # @_ZN13CRecordVectorIbED0Ev
.Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception21
# BB#0:
	pushq	%r14
.Lcfi249:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi250:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi251:
	.cfi_def_cfa_offset 32
.Lcfi252:
	.cfi_offset %rbx, -24
.Lcfi253:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp655:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp656:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB26_2:
.Ltmp657:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end26:
	.size	_ZN13CRecordVectorIbED0Ev, .Lfunc_end26-_ZN13CRecordVectorIbED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception21:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp655-.Lfunc_begin21 # >> Call Site 1 <<
	.long	.Ltmp656-.Ltmp655       #   Call between .Ltmp655 and .Ltmp656
	.long	.Ltmp657-.Lfunc_begin21 #     jumps to .Ltmp657
	.byte	0                       #   On action: cleanup
	.long	.Ltmp656-.Lfunc_begin21 # >> Call Site 2 <<
	.long	.Lfunc_end26-.Ltmp656   #   Call between .Ltmp656 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIjED0Ev,"axG",@progbits,_ZN13CRecordVectorIjED0Ev,comdat
	.weak	_ZN13CRecordVectorIjED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIjED0Ev,@function
_ZN13CRecordVectorIjED0Ev:              # @_ZN13CRecordVectorIjED0Ev
.Lfunc_begin22:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception22
# BB#0:
	pushq	%r14
.Lcfi254:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi255:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi256:
	.cfi_def_cfa_offset 32
.Lcfi257:
	.cfi_offset %rbx, -24
.Lcfi258:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp658:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp659:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB27_2:
.Ltmp660:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end27:
	.size	_ZN13CRecordVectorIjED0Ev, .Lfunc_end27-_ZN13CRecordVectorIjED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table27:
.Lexception22:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp658-.Lfunc_begin22 # >> Call Site 1 <<
	.long	.Ltmp659-.Ltmp658       #   Call between .Ltmp658 and .Ltmp659
	.long	.Ltmp660-.Lfunc_begin22 #     jumps to .Ltmp660
	.byte	0                       #   On action: cleanup
	.long	.Ltmp659-.Lfunc_begin22 # >> Call Site 2 <<
	.long	.Lfunc_end27-.Ltmp659   #   Call between .Ltmp659 and .Lfunc_end27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev
.Lfunc_begin23:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception23
# BB#0:
	pushq	%r14
.Lcfi259:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi260:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi261:
	.cfi_def_cfa_offset 32
.Lcfi262:
	.cfi_offset %rbx, -24
.Lcfi263:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, (%rbx)
.Ltmp661:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp662:
# BB#1:
.Ltmp667:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp668:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB28_5:
.Ltmp669:
	movq	%rax, %r14
	jmp	.LBB28_6
.LBB28_3:
.Ltmp663:
	movq	%rax, %r14
.Ltmp664:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp665:
.LBB28_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB28_4:
.Ltmp666:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end28:
	.size	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev, .Lfunc_end28-_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception23:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp661-.Lfunc_begin23 # >> Call Site 1 <<
	.long	.Ltmp662-.Ltmp661       #   Call between .Ltmp661 and .Ltmp662
	.long	.Ltmp663-.Lfunc_begin23 #     jumps to .Ltmp663
	.byte	0                       #   On action: cleanup
	.long	.Ltmp667-.Lfunc_begin23 # >> Call Site 2 <<
	.long	.Ltmp668-.Ltmp667       #   Call between .Ltmp667 and .Ltmp668
	.long	.Ltmp669-.Lfunc_begin23 #     jumps to .Ltmp669
	.byte	0                       #   On action: cleanup
	.long	.Ltmp664-.Lfunc_begin23 # >> Call Site 3 <<
	.long	.Ltmp665-.Ltmp664       #   Call between .Ltmp664 and .Ltmp665
	.long	.Ltmp666-.Lfunc_begin23 #     jumps to .Ltmp666
	.byte	1                       #   On action: 1
	.long	.Ltmp665-.Lfunc_begin23 # >> Call Site 4 <<
	.long	.Lfunc_end28-.Ltmp665   #   Call between .Ltmp665 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii
.Lfunc_begin24:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception24
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi264:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi265:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi266:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi267:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi268:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi269:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi270:
	.cfi_def_cfa_offset 64
.Lcfi271:
	.cfi_offset %rbx, -56
.Lcfi272:
	.cfi_offset %r12, -48
.Lcfi273:
	.cfi_offset %r13, -40
.Lcfi274:
	.cfi_offset %r14, -32
.Lcfi275:
	.cfi_offset %r15, -24
.Lcfi276:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB29_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB29_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB29_5
# BB#3:                                 #   in Loop: Header=BB29_2 Depth=1
.Ltmp670:
	movq	%rbp, %rdi
	callq	_ZN8NArchive3N7z7CFolderD2Ev
.Ltmp671:
# BB#4:                                 #   in Loop: Header=BB29_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB29_5:                               #   in Loop: Header=BB29_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB29_2
.LBB29_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB29_7:
.Ltmp672:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end29:
	.size	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii, .Lfunc_end29-_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table29:
.Lexception24:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp670-.Lfunc_begin24 # >> Call Site 1 <<
	.long	.Ltmp671-.Ltmp670       #   Call between .Ltmp670 and .Ltmp671
	.long	.Ltmp672-.Lfunc_begin24 #     jumps to .Ltmp672
	.byte	0                       #   On action: cleanup
	.long	.Ltmp671-.Lfunc_begin24 # >> Call Site 2 <<
	.long	.Lfunc_end29-.Ltmp671   #   Call between .Ltmp671 and .Lfunc_end29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive3N7z7CFolderD2Ev,"axG",@progbits,_ZN8NArchive3N7z7CFolderD2Ev,comdat
	.weak	_ZN8NArchive3N7z7CFolderD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z7CFolderD2Ev,@function
_ZN8NArchive3N7z7CFolderD2Ev:           # @_ZN8NArchive3N7z7CFolderD2Ev
.Lfunc_begin25:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception25
# BB#0:
	pushq	%r14
.Lcfi277:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi278:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi279:
	.cfi_def_cfa_offset 32
.Lcfi280:
	.cfi_offset %rbx, -24
.Lcfi281:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	96(%rbx), %rdi
.Ltmp673:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp674:
# BB#1:
	leaq	64(%rbx), %rdi
.Ltmp678:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp679:
# BB#2:
	leaq	32(%rbx), %rdi
.Ltmp683:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp684:
# BB#3:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp695:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp696:
# BB#4:                                 # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB30_5:
.Ltmp697:
	movq	%rax, %r14
.Ltmp698:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp699:
	jmp	.LBB30_6
.LBB30_7:
.Ltmp700:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB30_9:
.Ltmp685:
	movq	%rax, %r14
	jmp	.LBB30_12
.LBB30_10:
.Ltmp680:
	movq	%rax, %r14
	jmp	.LBB30_11
.LBB30_8:
.Ltmp675:
	movq	%rax, %r14
	leaq	64(%rbx), %rdi
.Ltmp676:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp677:
.LBB30_11:
	leaq	32(%rbx), %rdi
.Ltmp681:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp682:
.LBB30_12:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp686:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp687:
# BB#13:
.Ltmp692:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp693:
.LBB30_6:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB30_14:
.Ltmp688:
	movq	%rax, %r14
.Ltmp689:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp690:
	jmp	.LBB30_17
.LBB30_15:
.Ltmp691:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB30_16:
.Ltmp694:
	movq	%rax, %r14
.LBB30_17:                              # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end30:
	.size	_ZN8NArchive3N7z7CFolderD2Ev, .Lfunc_end30-_ZN8NArchive3N7z7CFolderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception25:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp673-.Lfunc_begin25 # >> Call Site 1 <<
	.long	.Ltmp674-.Ltmp673       #   Call between .Ltmp673 and .Ltmp674
	.long	.Ltmp675-.Lfunc_begin25 #     jumps to .Ltmp675
	.byte	0                       #   On action: cleanup
	.long	.Ltmp678-.Lfunc_begin25 # >> Call Site 2 <<
	.long	.Ltmp679-.Ltmp678       #   Call between .Ltmp678 and .Ltmp679
	.long	.Ltmp680-.Lfunc_begin25 #     jumps to .Ltmp680
	.byte	0                       #   On action: cleanup
	.long	.Ltmp683-.Lfunc_begin25 # >> Call Site 3 <<
	.long	.Ltmp684-.Ltmp683       #   Call between .Ltmp683 and .Ltmp684
	.long	.Ltmp685-.Lfunc_begin25 #     jumps to .Ltmp685
	.byte	0                       #   On action: cleanup
	.long	.Ltmp695-.Lfunc_begin25 # >> Call Site 4 <<
	.long	.Ltmp696-.Ltmp695       #   Call between .Ltmp695 and .Ltmp696
	.long	.Ltmp697-.Lfunc_begin25 #     jumps to .Ltmp697
	.byte	0                       #   On action: cleanup
	.long	.Ltmp696-.Lfunc_begin25 # >> Call Site 5 <<
	.long	.Ltmp698-.Ltmp696       #   Call between .Ltmp696 and .Ltmp698
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp698-.Lfunc_begin25 # >> Call Site 6 <<
	.long	.Ltmp699-.Ltmp698       #   Call between .Ltmp698 and .Ltmp699
	.long	.Ltmp700-.Lfunc_begin25 #     jumps to .Ltmp700
	.byte	1                       #   On action: 1
	.long	.Ltmp676-.Lfunc_begin25 # >> Call Site 7 <<
	.long	.Ltmp682-.Ltmp676       #   Call between .Ltmp676 and .Ltmp682
	.long	.Ltmp694-.Lfunc_begin25 #     jumps to .Ltmp694
	.byte	1                       #   On action: 1
	.long	.Ltmp686-.Lfunc_begin25 # >> Call Site 8 <<
	.long	.Ltmp687-.Ltmp686       #   Call between .Ltmp686 and .Ltmp687
	.long	.Ltmp688-.Lfunc_begin25 #     jumps to .Ltmp688
	.byte	1                       #   On action: 1
	.long	.Ltmp692-.Lfunc_begin25 # >> Call Site 9 <<
	.long	.Ltmp693-.Ltmp692       #   Call between .Ltmp692 and .Ltmp693
	.long	.Ltmp694-.Lfunc_begin25 #     jumps to .Ltmp694
	.byte	1                       #   On action: 1
	.long	.Ltmp693-.Lfunc_begin25 # >> Call Site 10 <<
	.long	.Ltmp689-.Ltmp693       #   Call between .Ltmp693 and .Ltmp689
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp689-.Lfunc_begin25 # >> Call Site 11 <<
	.long	.Ltmp690-.Ltmp689       #   Call between .Ltmp689 and .Ltmp690
	.long	.Ltmp691-.Lfunc_begin25 #     jumps to .Ltmp691
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
.Lfunc_begin26:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception26
# BB#0:
	pushq	%r14
.Lcfi282:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi283:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi284:
	.cfi_def_cfa_offset 32
.Lcfi285:
	.cfi_offset %rbx, -24
.Lcfi286:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp701:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp702:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB31_2:
.Ltmp703:
	movq	%rax, %r14
.Ltmp704:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp705:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB31_4:
.Ltmp706:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end31:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev, .Lfunc_end31-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table31:
.Lexception26:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp701-.Lfunc_begin26 # >> Call Site 1 <<
	.long	.Ltmp702-.Ltmp701       #   Call between .Ltmp701 and .Ltmp702
	.long	.Ltmp703-.Lfunc_begin26 #     jumps to .Ltmp703
	.byte	0                       #   On action: cleanup
	.long	.Ltmp702-.Lfunc_begin26 # >> Call Site 2 <<
	.long	.Ltmp704-.Ltmp702       #   Call between .Ltmp702 and .Ltmp704
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp704-.Lfunc_begin26 # >> Call Site 3 <<
	.long	.Ltmp705-.Ltmp704       #   Call between .Ltmp704 and .Ltmp705
	.long	.Ltmp706-.Lfunc_begin26 #     jumps to .Ltmp706
	.byte	1                       #   On action: 1
	.long	.Ltmp705-.Lfunc_begin26 # >> Call Site 4 <<
	.long	.Lfunc_end31-.Ltmp705   #   Call between .Ltmp705 and .Lfunc_end31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
.Lfunc_begin27:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception27
# BB#0:
	pushq	%r14
.Lcfi287:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi288:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi289:
	.cfi_def_cfa_offset 32
.Lcfi290:
	.cfi_offset %rbx, -24
.Lcfi291:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp707:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp708:
# BB#1:
.Ltmp713:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp714:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB32_5:
.Ltmp715:
	movq	%rax, %r14
	jmp	.LBB32_6
.LBB32_3:
.Ltmp709:
	movq	%rax, %r14
.Ltmp710:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp711:
.LBB32_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB32_4:
.Ltmp712:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end32:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev, .Lfunc_end32-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception27:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp707-.Lfunc_begin27 # >> Call Site 1 <<
	.long	.Ltmp708-.Ltmp707       #   Call between .Ltmp707 and .Ltmp708
	.long	.Ltmp709-.Lfunc_begin27 #     jumps to .Ltmp709
	.byte	0                       #   On action: cleanup
	.long	.Ltmp713-.Lfunc_begin27 # >> Call Site 2 <<
	.long	.Ltmp714-.Ltmp713       #   Call between .Ltmp713 and .Ltmp714
	.long	.Ltmp715-.Lfunc_begin27 #     jumps to .Ltmp715
	.byte	0                       #   On action: cleanup
	.long	.Ltmp710-.Lfunc_begin27 # >> Call Site 3 <<
	.long	.Ltmp711-.Ltmp710       #   Call between .Ltmp710 and .Ltmp711
	.long	.Ltmp712-.Lfunc_begin27 #     jumps to .Ltmp712
	.byte	1                       #   On action: 1
	.long	.Ltmp711-.Lfunc_begin27 # >> Call Site 4 <<
	.long	.Lfunc_end32-.Ltmp711   #   Call between .Ltmp711 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi292:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi293:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi294:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi295:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi296:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi297:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi298:
	.cfi_def_cfa_offset 64
.Lcfi299:
	.cfi_offset %rbx, -56
.Lcfi300:
	.cfi_offset %r12, -48
.Lcfi301:
	.cfi_offset %r13, -40
.Lcfi302:
	.cfi_offset %r14, -32
.Lcfi303:
	.cfi_offset %r15, -24
.Lcfi304:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB33_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB33_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB33_6
# BB#3:                                 #   in Loop: Header=BB33_2 Depth=1
	movq	$_ZTV7CBufferIhE+16, 8(%rbp)
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB33_5
# BB#4:                                 #   in Loop: Header=BB33_2 Depth=1
	callq	_ZdaPv
.LBB33_5:                               # %_ZN8NArchive3N7z10CCoderInfoD2Ev.exit
                                        #   in Loop: Header=BB33_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB33_6:                               #   in Loop: Header=BB33_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB33_2
.LBB33_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end33:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii, .Lfunc_end33-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN7CBufferIhED2Ev,"axG",@progbits,_ZN7CBufferIhED2Ev,comdat
	.weak	_ZN7CBufferIhED2Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED2Ev,@function
_ZN7CBufferIhED2Ev:                     # @_ZN7CBufferIhED2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV7CBufferIhE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB34_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB34_1:
	retq
.Lfunc_end34:
	.size	_ZN7CBufferIhED2Ev, .Lfunc_end34-_ZN7CBufferIhED2Ev
	.cfi_endproc

	.section	.text._ZN7CBufferIhED0Ev,"axG",@progbits,_ZN7CBufferIhED0Ev,comdat
	.weak	_ZN7CBufferIhED0Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED0Ev,@function
_ZN7CBufferIhED0Ev:                     # @_ZN7CBufferIhED0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi305:
	.cfi_def_cfa_offset 16
.Lcfi306:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV7CBufferIhE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB35_2
# BB#1:
	callq	_ZdaPv
.LBB35_2:                               # %_ZN7CBufferIhED2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end35:
	.size	_ZN7CBufferIhED0Ev, .Lfunc_end35-_ZN7CBufferIhED0Ev
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED0Ev
.Lfunc_begin28:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception28
# BB#0:
	pushq	%r14
.Lcfi307:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi308:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi309:
	.cfi_def_cfa_offset 32
.Lcfi310:
	.cfi_offset %rbx, -24
.Lcfi311:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE+16, (%rbx)
.Ltmp716:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp717:
# BB#1:
.Ltmp722:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp723:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB36_5:
.Ltmp724:
	movq	%rax, %r14
	jmp	.LBB36_6
.LBB36_3:
.Ltmp718:
	movq	%rax, %r14
.Ltmp719:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp720:
.LBB36_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB36_4:
.Ltmp721:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end36:
	.size	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED0Ev, .Lfunc_end36-_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table36:
.Lexception28:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp716-.Lfunc_begin28 # >> Call Site 1 <<
	.long	.Ltmp717-.Ltmp716       #   Call between .Ltmp716 and .Ltmp717
	.long	.Ltmp718-.Lfunc_begin28 #     jumps to .Ltmp718
	.byte	0                       #   On action: cleanup
	.long	.Ltmp722-.Lfunc_begin28 # >> Call Site 2 <<
	.long	.Ltmp723-.Ltmp722       #   Call between .Ltmp722 and .Ltmp723
	.long	.Ltmp724-.Lfunc_begin28 #     jumps to .Ltmp724
	.byte	0                       #   On action: cleanup
	.long	.Ltmp719-.Lfunc_begin28 # >> Call Site 3 <<
	.long	.Ltmp720-.Ltmp719       #   Call between .Ltmp719 and .Ltmp720
	.long	.Ltmp721-.Lfunc_begin28 #     jumps to .Ltmp721
	.byte	1                       #   On action: 1
	.long	.Ltmp720-.Lfunc_begin28 # >> Call Site 4 <<
	.long	.Lfunc_end36-.Ltmp720   #   Call between .Ltmp720 and .Lfunc_end36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi312:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi313:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi314:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi315:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi316:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi317:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi318:
	.cfi_def_cfa_offset 64
.Lcfi319:
	.cfi_offset %rbx, -56
.Lcfi320:
	.cfi_offset %r12, -48
.Lcfi321:
	.cfi_offset %r13, -40
.Lcfi322:
	.cfi_offset %r14, -32
.Lcfi323:
	.cfi_offset %r15, -24
.Lcfi324:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB37_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB37_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB37_6
# BB#3:                                 #   in Loop: Header=BB37_2 Depth=1
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB37_5
# BB#4:                                 #   in Loop: Header=BB37_2 Depth=1
	callq	_ZdaPv
.LBB37_5:                               # %_ZN8NArchive3N7z9CFileItemD2Ev.exit
                                        #   in Loop: Header=BB37_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB37_6:                               #   in Loop: Header=BB37_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB37_2
.LBB37_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end37:
	.size	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE6DeleteEii, .Lfunc_end37-_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE6DeleteEii
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7zL15GetBindInfoPartER11CStringBaseIwERjS4_,@function
_ZN8NArchive3N7zL15GetBindInfoPartER11CStringBaseIwERjS4_: # @_ZN8NArchive3N7zL15GetBindInfoPartER11CStringBaseIwERjS4_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi325:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi326:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi327:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi328:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi329:
	.cfi_def_cfa_offset 48
.Lcfi330:
	.cfi_offset %rbx, -40
.Lcfi331:
	.cfi_offset %r14, -32
.Lcfi332:
	.cfi_offset %r15, -24
.Lcfi333:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movl	$0, (%r14)
	callq	_Z19ParseStringToUInt32RK11CStringBaseIwERj
	movl	%eax, %ebp
	movl	$-2147024809, %r15d     # imm = 0x80070057
	testl	%ebp, %ebp
	je	.LBB38_10
# BB#1:
	movl	8(%rbx), %eax
	cmpl	%eax, %ebp
	cmovgl	%eax, %ebp
	testl	%ebp, %ebp
	jle	.LBB38_3
# BB#2:
	movq	(%rbx), %rdi
	movslq	%ebp, %rcx
	leaq	(%rdi,%rcx,4), %rsi
	incl	%eax
	subl	%ebp, %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	callq	memmove
	movl	8(%rbx), %eax
	subl	%ebp, %eax
	movl	%eax, 8(%rbx)
.LBB38_3:                               # %_ZN11CStringBaseIwE6DeleteEii.exit21
	movq	(%rbx), %rdi
	cmpl	$83, (%rdi)
	jne	.LBB38_9
# BB#4:
	cmpl	$2, %eax
	movl	$1, %ecx
	cmovll	%eax, %ecx
	testl	%eax, %eax
	jle	.LBB38_6
# BB#5:
	movslq	%ecx, %rbp
	leaq	(%rdi,%rbp,4), %rsi
	incl	%eax
	subl	%ebp, %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	callq	memmove
	subl	%ebp, 8(%rbx)
.LBB38_6:                               # %_ZN11CStringBaseIwE6DeleteEii.exit19
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_Z19ParseStringToUInt32RK11CStringBaseIwERj
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB38_10
# BB#7:                                 # %.critedge
	movl	8(%rbx), %eax
	cmpl	%eax, %ebp
	cmovgl	%eax, %ebp
	testl	%ebp, %ebp
	jle	.LBB38_9
# BB#8:
	movq	(%rbx), %rdi
	movslq	%ebp, %rcx
	leaq	(%rdi,%rcx,4), %rsi
	incl	%eax
	subl	%ebp, %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	callq	memmove
	subl	%ebp, 8(%rbx)
.LBB38_9:                               # %_ZN11CStringBaseIwE6DeleteEii.exit
	xorl	%r15d, %r15d
.LBB38_10:                              # %_ZN11CStringBaseIwE6DeleteEii.exit
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end38:
	.size	_ZN8NArchive3N7zL15GetBindInfoPartER11CStringBaseIwERjS4_, .Lfunc_end38-_ZN8NArchive3N7zL15GetBindInfoPartER11CStringBaseIwERjS4_
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev,@function
_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev: # @_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev
.Lfunc_begin29:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception29
# BB#0:
	pushq	%r14
.Lcfi334:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi335:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi336:
	.cfi_def_cfa_offset 32
.Lcfi337:
	.cfi_offset %rbx, -24
.Lcfi338:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE+16, (%rbx)
.Ltmp725:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp726:
# BB#1:
.Ltmp731:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp732:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB39_5:
.Ltmp733:
	movq	%rax, %r14
	jmp	.LBB39_6
.LBB39_3:
.Ltmp727:
	movq	%rax, %r14
.Ltmp728:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp729:
.LBB39_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB39_4:
.Ltmp730:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end39:
	.size	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev, .Lfunc_end39-_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table39:
.Lexception29:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp725-.Lfunc_begin29 # >> Call Site 1 <<
	.long	.Ltmp726-.Ltmp725       #   Call between .Ltmp725 and .Ltmp726
	.long	.Ltmp727-.Lfunc_begin29 #     jumps to .Ltmp727
	.byte	0                       #   On action: cleanup
	.long	.Ltmp731-.Lfunc_begin29 # >> Call Site 2 <<
	.long	.Ltmp732-.Ltmp731       #   Call between .Ltmp731 and .Ltmp732
	.long	.Ltmp733-.Lfunc_begin29 #     jumps to .Ltmp733
	.byte	0                       #   On action: cleanup
	.long	.Ltmp728-.Lfunc_begin29 # >> Call Site 3 <<
	.long	.Ltmp729-.Ltmp728       #   Call between .Ltmp728 and .Ltmp729
	.long	.Ltmp730-.Lfunc_begin29 #     jumps to .Ltmp730
	.byte	1                       #   On action: 1
	.long	.Ltmp729-.Lfunc_begin29 # >> Call Site 4 <<
	.long	.Lfunc_end39-.Ltmp729   #   Call between .Ltmp729 and .Lfunc_end39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii
.Lfunc_begin30:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception30
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi339:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi340:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi341:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi342:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi343:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi344:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi345:
	.cfi_def_cfa_offset 64
.Lcfi346:
	.cfi_offset %rbx, -56
.Lcfi347:
	.cfi_offset %r12, -48
.Lcfi348:
	.cfi_offset %r13, -40
.Lcfi349:
	.cfi_offset %r14, -32
.Lcfi350:
	.cfi_offset %r15, -24
.Lcfi351:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB40_9
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB40_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB40_8
# BB#3:                                 #   in Loop: Header=BB40_2 Depth=1
	movq	32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB40_5
# BB#4:                                 #   in Loop: Header=BB40_2 Depth=1
	callq	_ZdaPv
.LBB40_5:                               # %_ZN11CStringBaseIwED2Ev.exit.i
                                        #   in Loop: Header=BB40_2 Depth=1
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbp)
.Ltmp734:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp735:
# BB#6:                                 # %_ZN13CObjectVectorI5CPropED2Ev.exit.i
                                        #   in Loop: Header=BB40_2 Depth=1
.Ltmp740:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp741:
# BB#7:                                 # %_ZN8NArchive14COneMethodInfoD2Ev.exit
                                        #   in Loop: Header=BB40_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB40_8:                               #   in Loop: Header=BB40_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB40_2
.LBB40_9:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB40_10:
.Ltmp736:
	movq	%rax, %rbx
.Ltmp737:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp738:
	jmp	.LBB40_13
.LBB40_11:
.Ltmp739:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB40_12:
.Ltmp742:
	movq	%rax, %rbx
.LBB40_13:                              # %.body
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end40:
	.size	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii, .Lfunc_end40-_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception30:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp734-.Lfunc_begin30 # >> Call Site 1 <<
	.long	.Ltmp735-.Ltmp734       #   Call between .Ltmp734 and .Ltmp735
	.long	.Ltmp736-.Lfunc_begin30 #     jumps to .Ltmp736
	.byte	0                       #   On action: cleanup
	.long	.Ltmp740-.Lfunc_begin30 # >> Call Site 2 <<
	.long	.Ltmp741-.Ltmp740       #   Call between .Ltmp740 and .Ltmp741
	.long	.Ltmp742-.Lfunc_begin30 #     jumps to .Ltmp742
	.byte	0                       #   On action: cleanup
	.long	.Ltmp741-.Lfunc_begin30 # >> Call Site 3 <<
	.long	.Ltmp737-.Ltmp741       #   Call between .Ltmp741 and .Ltmp737
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp737-.Lfunc_begin30 # >> Call Site 4 <<
	.long	.Ltmp738-.Ltmp737       #   Call between .Ltmp737 and .Ltmp738
	.long	.Ltmp739-.Lfunc_begin30 #     jumps to .Ltmp739
	.byte	1                       #   On action: 1
	.long	.Ltmp738-.Lfunc_begin30 # >> Call Site 5 <<
	.long	.Lfunc_end40-.Ltmp738   #   Call between .Ltmp738 and .Lfunc_end40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive14COneMethodInfoC2ERKS0_,"axG",@progbits,_ZN8NArchive14COneMethodInfoC2ERKS0_,comdat
	.weak	_ZN8NArchive14COneMethodInfoC2ERKS0_
	.p2align	4, 0x90
	.type	_ZN8NArchive14COneMethodInfoC2ERKS0_,@function
_ZN8NArchive14COneMethodInfoC2ERKS0_:   # @_ZN8NArchive14COneMethodInfoC2ERKS0_
.Lfunc_begin31:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception31
# BB#0:
	pushq	%r15
.Lcfi352:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi353:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi354:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi355:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi356:
	.cfi_def_cfa_offset 48
.Lcfi357:
	.cfi_offset %rbx, -40
.Lcfi358:
	.cfi_offset %r12, -32
.Lcfi359:
	.cfi_offset %r14, -24
.Lcfi360:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movq	$8, 24(%rbx)
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbx)
.Ltmp743:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp744:
# BB#1:                                 # %.noexc.i
.Ltmp745:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN13CObjectVectorI5CPropEpLERKS1_
.Ltmp746:
# BB#2:                                 # %_ZN13CObjectVectorI5CPropEC2ERKS1_.exit
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movslq	40(%r14), %r12
	leaq	1(%r12), %r15
	testl	%r15d, %r15d
	je	.LBB41_3
# BB#7:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp751:
	callq	_Znam
.Ltmp752:
# BB#8:                                 # %.noexc
	movq	%rax, 32(%rbx)
	movl	$0, (%rax)
	movl	%r15d, 44(%rbx)
	jmp	.LBB41_9
.LBB41_3:
	xorl	%eax, %eax
.LBB41_9:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	32(%r14), %rcx
	.p2align	4, 0x90
.LBB41_10:                              # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB41_10
# BB#11:
	movl	%r12d, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB41_12:
.Ltmp753:
	movq	%rax, %r14
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbx)
.Ltmp754:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp755:
# BB#13:
.Ltmp760:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp761:
	jmp	.LBB41_5
.LBB41_16:
.Ltmp762:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB41_14:
.Ltmp756:
	movq	%rax, %r14
.Ltmp757:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp758:
# BB#17:                                # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB41_15:
.Ltmp759:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB41_4:
.Ltmp747:
	movq	%rax, %r14
.Ltmp748:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp749:
.LBB41_5:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB41_6:
.Ltmp750:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end41:
	.size	_ZN8NArchive14COneMethodInfoC2ERKS0_, .Lfunc_end41-_ZN8NArchive14COneMethodInfoC2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table41:
.Lexception31:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp743-.Lfunc_begin31 # >> Call Site 1 <<
	.long	.Ltmp746-.Ltmp743       #   Call between .Ltmp743 and .Ltmp746
	.long	.Ltmp747-.Lfunc_begin31 #     jumps to .Ltmp747
	.byte	0                       #   On action: cleanup
	.long	.Ltmp751-.Lfunc_begin31 # >> Call Site 2 <<
	.long	.Ltmp752-.Ltmp751       #   Call between .Ltmp751 and .Ltmp752
	.long	.Ltmp753-.Lfunc_begin31 #     jumps to .Ltmp753
	.byte	0                       #   On action: cleanup
	.long	.Ltmp754-.Lfunc_begin31 # >> Call Site 3 <<
	.long	.Ltmp755-.Ltmp754       #   Call between .Ltmp754 and .Ltmp755
	.long	.Ltmp756-.Lfunc_begin31 #     jumps to .Ltmp756
	.byte	1                       #   On action: 1
	.long	.Ltmp760-.Lfunc_begin31 # >> Call Site 4 <<
	.long	.Ltmp761-.Ltmp760       #   Call between .Ltmp760 and .Ltmp761
	.long	.Ltmp762-.Lfunc_begin31 #     jumps to .Ltmp762
	.byte	1                       #   On action: 1
	.long	.Ltmp757-.Lfunc_begin31 # >> Call Site 5 <<
	.long	.Ltmp758-.Ltmp757       #   Call between .Ltmp757 and .Ltmp758
	.long	.Ltmp759-.Lfunc_begin31 #     jumps to .Ltmp759
	.byte	1                       #   On action: 1
	.long	.Ltmp748-.Lfunc_begin31 # >> Call Site 6 <<
	.long	.Ltmp749-.Ltmp748       #   Call between .Ltmp748 and .Ltmp749
	.long	.Ltmp750-.Lfunc_begin31 #     jumps to .Ltmp750
	.byte	1                       #   On action: 1
	.long	.Ltmp749-.Lfunc_begin31 # >> Call Site 7 <<
	.long	.Lfunc_end41-.Ltmp749   #   Call between .Ltmp749 and .Lfunc_end41
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropEpLERKS1_,"axG",@progbits,_ZN13CObjectVectorI5CPropEpLERKS1_,comdat
	.weak	_ZN13CObjectVectorI5CPropEpLERKS1_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropEpLERKS1_,@function
_ZN13CObjectVectorI5CPropEpLERKS1_:     # @_ZN13CObjectVectorI5CPropEpLERKS1_
.Lfunc_begin32:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception32
# BB#0:
	pushq	%rbp
.Lcfi361:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi362:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi363:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi364:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi365:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi366:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi367:
	.cfi_def_cfa_offset 64
.Lcfi368:
	.cfi_offset %rbx, -56
.Lcfi369:
	.cfi_offset %r12, -48
.Lcfi370:
	.cfi_offset %r13, -40
.Lcfi371:
	.cfi_offset %r14, -32
.Lcfi372:
	.cfi_offset %r15, -24
.Lcfi373:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	12(%r14), %r13
	movl	12(%r15), %esi
	addl	%r13d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testq	%r13, %r13
	jle	.LBB42_4
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB42_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbp,8), %rbx
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r12
	movl	(%rbx), %eax
	movl	%eax, (%r12)
	leaq	8(%r12), %rdi
	addq	$8, %rbx
.Ltmp763:
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp764:
# BB#3:                                 # %_ZN13CObjectVectorI5CPropE3AddERKS0_.exit
                                        #   in Loop: Header=BB42_2 Depth=1
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	%r12, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	incq	%rbp
	cmpq	%r13, %rbp
	jl	.LBB42_2
.LBB42_4:                               # %._crit_edge
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB42_5:
.Ltmp765:
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end42:
	.size	_ZN13CObjectVectorI5CPropEpLERKS1_, .Lfunc_end42-_ZN13CObjectVectorI5CPropEpLERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table42:
.Lexception32:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin32-.Lfunc_begin32 # >> Call Site 1 <<
	.long	.Ltmp763-.Lfunc_begin32 #   Call between .Lfunc_begin32 and .Ltmp763
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp763-.Lfunc_begin32 # >> Call Site 2 <<
	.long	.Ltmp764-.Ltmp763       #   Call between .Ltmp763 and .Ltmp764
	.long	.Ltmp765-.Lfunc_begin32 #     jumps to .Ltmp765
	.byte	0                       #   On action: cleanup
	.long	.Ltmp764-.Lfunc_begin32 # >> Call Site 3 <<
	.long	.Lfunc_end42-.Ltmp764   #   Call between .Ltmp764 and .Lfunc_end42
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED0Ev
.Lfunc_begin33:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception33
# BB#0:
	pushq	%r14
.Lcfi374:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi375:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi376:
	.cfi_def_cfa_offset 32
.Lcfi377:
	.cfi_offset %rbx, -24
.Lcfi378:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CUpdateItemEE+16, (%rbx)
.Ltmp766:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp767:
# BB#1:
.Ltmp772:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp773:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB43_5:
.Ltmp774:
	movq	%rax, %r14
	jmp	.LBB43_6
.LBB43_3:
.Ltmp768:
	movq	%rax, %r14
.Ltmp769:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp770:
.LBB43_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB43_4:
.Ltmp771:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end43:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED0Ev, .Lfunc_end43-_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table43:
.Lexception33:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp766-.Lfunc_begin33 # >> Call Site 1 <<
	.long	.Ltmp767-.Ltmp766       #   Call between .Ltmp766 and .Ltmp767
	.long	.Ltmp768-.Lfunc_begin33 #     jumps to .Ltmp768
	.byte	0                       #   On action: cleanup
	.long	.Ltmp772-.Lfunc_begin33 # >> Call Site 2 <<
	.long	.Ltmp773-.Ltmp772       #   Call between .Ltmp772 and .Ltmp773
	.long	.Ltmp774-.Lfunc_begin33 #     jumps to .Ltmp774
	.byte	0                       #   On action: cleanup
	.long	.Ltmp769-.Lfunc_begin33 # >> Call Site 3 <<
	.long	.Ltmp770-.Ltmp769       #   Call between .Ltmp769 and .Ltmp770
	.long	.Ltmp771-.Lfunc_begin33 #     jumps to .Ltmp771
	.byte	1                       #   On action: 1
	.long	.Ltmp770-.Lfunc_begin33 # >> Call Site 4 <<
	.long	.Lfunc_end43-.Ltmp770   #   Call between .Ltmp770 and .Lfunc_end43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi379:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi380:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi381:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi382:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi383:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi384:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi385:
	.cfi_def_cfa_offset 64
.Lcfi386:
	.cfi_offset %rbx, -56
.Lcfi387:
	.cfi_offset %r12, -48
.Lcfi388:
	.cfi_offset %r13, -40
.Lcfi389:
	.cfi_offset %r14, -32
.Lcfi390:
	.cfi_offset %r15, -24
.Lcfi391:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB44_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB44_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB44_6
# BB#3:                                 #   in Loop: Header=BB44_2 Depth=1
	movq	40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB44_5
# BB#4:                                 #   in Loop: Header=BB44_2 Depth=1
	callq	_ZdaPv
.LBB44_5:                               # %_ZN8NArchive3N7z11CUpdateItemD2Ev.exit
                                        #   in Loop: Header=BB44_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB44_6:                               #   in Loop: Header=BB44_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB44_2
.LBB44_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end44:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEE6DeleteEii, .Lfunc_end44-_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEE6DeleteEii
	.cfi_endproc

	.type	_ZTV13CObjectVectorI5CPropE,@object # @_ZTV13CObjectVectorI5CPropE
	.section	.rodata._ZTV13CObjectVectorI5CPropE,"aG",@progbits,_ZTV13CObjectVectorI5CPropE,comdat
	.weak	_ZTV13CObjectVectorI5CPropE
	.p2align	3
_ZTV13CObjectVectorI5CPropE:
	.quad	0
	.quad	_ZTI13CObjectVectorI5CPropE
	.quad	_ZN13CObjectVectorI5CPropED2Ev
	.quad	_ZN13CObjectVectorI5CPropED0Ev
	.quad	_ZN13CObjectVectorI5CPropE6DeleteEii
	.size	_ZTV13CObjectVectorI5CPropE, 40

	.type	_ZTS13CObjectVectorI5CPropE,@object # @_ZTS13CObjectVectorI5CPropE
	.section	.rodata._ZTS13CObjectVectorI5CPropE,"aG",@progbits,_ZTS13CObjectVectorI5CPropE,comdat
	.weak	_ZTS13CObjectVectorI5CPropE
	.p2align	4
_ZTS13CObjectVectorI5CPropE:
	.asciz	"13CObjectVectorI5CPropE"
	.size	_ZTS13CObjectVectorI5CPropE, 24

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorI5CPropE,@object # @_ZTI13CObjectVectorI5CPropE
	.section	.rodata._ZTI13CObjectVectorI5CPropE,"aG",@progbits,_ZTI13CObjectVectorI5CPropE,comdat
	.weak	_ZTI13CObjectVectorI5CPropE
	.p2align	4
_ZTI13CObjectVectorI5CPropE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI5CPropE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI5CPropE, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	76                      # 0x4c
	.long	90                      # 0x5a
	.long	77                      # 0x4d
	.long	65                      # 0x41
	.long	0                       # 0x0
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
	.p2align	2
.L.str.1:
	.long	66                      # 0x42
	.long	84                      # 0x54
	.long	50                      # 0x32
	.long	0                       # 0x0
	.size	.L.str.1, 16

	.type	.L.str.2,@object        # @.str.2
	.p2align	2
.L.str.2:
	.long	67                      # 0x43
	.long	111                     # 0x6f
	.long	112                     # 0x70
	.long	121                     # 0x79
	.long	0                       # 0x0
	.size	.L.str.2, 20

	.type	_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z11CMethodFullEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE, 46

	.type	_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE, 24

	.type	_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE,@object # @_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE
	.section	.rodata._ZTV13CRecordVectorIN8NArchive3N7z5CBindEE,"aG",@progbits,_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE,comdat
	.weak	_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE
	.p2align	3
_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE:
	.quad	0
	.quad	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE, 40

	.type	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE,@object # @_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE
	.section	.rodata._ZTS13CRecordVectorIN8NArchive3N7z5CBindEE,"aG",@progbits,_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE,comdat
	.weak	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE
	.p2align	4
_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE:
	.asciz	"13CRecordVectorIN8NArchive3N7z5CBindEE"
	.size	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE, 39

	.type	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE,@object # @_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE
	.section	.rodata._ZTI13CRecordVectorIN8NArchive3N7z5CBindEE,"aG",@progbits,_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE,comdat
	.weak	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE
	.p2align	4
_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE, 24

	.type	_ZTV13CRecordVectorIyE,@object # @_ZTV13CRecordVectorIyE
	.section	.rodata._ZTV13CRecordVectorIyE,"aG",@progbits,_ZTV13CRecordVectorIyE,comdat
	.weak	_ZTV13CRecordVectorIyE
	.p2align	3
_ZTV13CRecordVectorIyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIyE, 40

	.type	_ZTS13CRecordVectorIyE,@object # @_ZTS13CRecordVectorIyE
	.section	.rodata._ZTS13CRecordVectorIyE,"aG",@progbits,_ZTS13CRecordVectorIyE,comdat
	.weak	_ZTS13CRecordVectorIyE
	.p2align	4
_ZTS13CRecordVectorIyE:
	.asciz	"13CRecordVectorIyE"
	.size	_ZTS13CRecordVectorIyE, 19

	.type	_ZTI13CRecordVectorIyE,@object # @_ZTI13CRecordVectorIyE
	.section	.rodata._ZTI13CRecordVectorIyE,"aG",@progbits,_ZTI13CRecordVectorIyE,comdat
	.weak	_ZTI13CRecordVectorIyE
	.p2align	4
_ZTI13CRecordVectorIyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIyE, 24

	.type	_ZTV13CRecordVectorIbE,@object # @_ZTV13CRecordVectorIbE
	.section	.rodata._ZTV13CRecordVectorIbE,"aG",@progbits,_ZTV13CRecordVectorIbE,comdat
	.weak	_ZTV13CRecordVectorIbE
	.p2align	3
_ZTV13CRecordVectorIbE:
	.quad	0
	.quad	_ZTI13CRecordVectorIbE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIbED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIbE, 40

	.type	_ZTS13CRecordVectorIbE,@object # @_ZTS13CRecordVectorIbE
	.section	.rodata._ZTS13CRecordVectorIbE,"aG",@progbits,_ZTS13CRecordVectorIbE,comdat
	.weak	_ZTS13CRecordVectorIbE
	.p2align	4
_ZTS13CRecordVectorIbE:
	.asciz	"13CRecordVectorIbE"
	.size	_ZTS13CRecordVectorIbE, 19

	.type	_ZTI13CRecordVectorIbE,@object # @_ZTI13CRecordVectorIbE
	.section	.rodata._ZTI13CRecordVectorIbE,"aG",@progbits,_ZTI13CRecordVectorIbE,comdat
	.weak	_ZTI13CRecordVectorIbE
	.p2align	4
_ZTI13CRecordVectorIbE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIbE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIbE, 24

	.type	_ZTV13CRecordVectorIjE,@object # @_ZTV13CRecordVectorIjE
	.section	.rodata._ZTV13CRecordVectorIjE,"aG",@progbits,_ZTV13CRecordVectorIjE,comdat
	.weak	_ZTV13CRecordVectorIjE
	.p2align	3
_ZTV13CRecordVectorIjE:
	.quad	0
	.quad	_ZTI13CRecordVectorIjE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIjED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIjE, 40

	.type	_ZTS13CRecordVectorIjE,@object # @_ZTS13CRecordVectorIjE
	.section	.rodata._ZTS13CRecordVectorIjE,"aG",@progbits,_ZTS13CRecordVectorIjE,comdat
	.weak	_ZTS13CRecordVectorIjE
	.p2align	4
_ZTS13CRecordVectorIjE:
	.asciz	"13CRecordVectorIjE"
	.size	_ZTS13CRecordVectorIjE, 19

	.type	_ZTI13CRecordVectorIjE,@object # @_ZTI13CRecordVectorIjE
	.section	.rodata._ZTI13CRecordVectorIjE,"aG",@progbits,_ZTI13CRecordVectorIjE,comdat
	.weak	_ZTI13CRecordVectorIjE
	.p2align	4
_ZTI13CRecordVectorIjE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIjE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIjE, 24

	.type	_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z7CFolderEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE, 41

	.type	_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE, 24

	.type	_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z10CCoderInfoEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE, 45

	.type	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE, 24

	.type	_ZTV7CBufferIhE,@object # @_ZTV7CBufferIhE
	.section	.rodata._ZTV7CBufferIhE,"aG",@progbits,_ZTV7CBufferIhE,comdat
	.weak	_ZTV7CBufferIhE
	.p2align	3
_ZTV7CBufferIhE:
	.quad	0
	.quad	_ZTI7CBufferIhE
	.quad	_ZN7CBufferIhED2Ev
	.quad	_ZN7CBufferIhED0Ev
	.size	_ZTV7CBufferIhE, 32

	.type	_ZTS7CBufferIhE,@object # @_ZTS7CBufferIhE
	.section	.rodata._ZTS7CBufferIhE,"aG",@progbits,_ZTS7CBufferIhE,comdat
	.weak	_ZTS7CBufferIhE
_ZTS7CBufferIhE:
	.asciz	"7CBufferIhE"
	.size	_ZTS7CBufferIhE, 12

	.type	_ZTI7CBufferIhE,@object # @_ZTI7CBufferIhE
	.section	.rodata._ZTI7CBufferIhE,"aG",@progbits,_ZTI7CBufferIhE,comdat
	.weak	_ZTI7CBufferIhE
	.p2align	3
_ZTI7CBufferIhE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS7CBufferIhE
	.size	_ZTI7CBufferIhE, 16

	.type	_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z9CFileItemEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z9CFileItemEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z9CFileItemEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z9CFileItemEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z9CFileItemEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z9CFileItemEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z9CFileItemEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z9CFileItemEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z9CFileItemEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z9CFileItemEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z9CFileItemEE, 43

	.type	_ZTI13CObjectVectorIN8NArchive3N7z9CFileItemEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z9CFileItemEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z9CFileItemEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z9CFileItemEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z9CFileItemEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z9CFileItemEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z9CFileItemEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z9CFileItemEE, 24

	.type	_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE,@object # @_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE
	.quad	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive14COneMethodInfoEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive14COneMethodInfoEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE,@object # @_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE:
	.asciz	"13CObjectVectorIN8NArchive14COneMethodInfoEE"
	.size	_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE, 45

	.type	_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE,@object # @_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive14COneMethodInfoEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive14COneMethodInfoEE, 24

	.type	_ZTV13CObjectVectorIN8NArchive3N7z11CUpdateItemEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z11CUpdateItemEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z11CUpdateItemEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z11CUpdateItemEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z11CUpdateItemEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z11CUpdateItemEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z11CUpdateItemEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z11CUpdateItemEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z11CUpdateItemEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z11CUpdateItemEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z11CUpdateItemEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z11CUpdateItemEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z11CUpdateItemEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z11CUpdateItemEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z11CUpdateItemEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z11CUpdateItemEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z11CUpdateItemEE, 46

	.type	_ZTI13CObjectVectorIN8NArchive3N7z11CUpdateItemEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z11CUpdateItemEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z11CUpdateItemEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z11CUpdateItemEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z11CUpdateItemEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z11CUpdateItemEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z11CUpdateItemEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z11CUpdateItemEE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
