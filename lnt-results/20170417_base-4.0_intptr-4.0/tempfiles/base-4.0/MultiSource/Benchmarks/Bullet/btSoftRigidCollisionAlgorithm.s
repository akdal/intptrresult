	.text
	.file	"btSoftRigidCollisionAlgorithm.bc"
	.globl	_ZN29btSoftRigidCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b
	.p2align	4, 0x90
	.type	_ZN29btSoftRigidCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b,@function
_ZN29btSoftRigidCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b: # @_ZN29btSoftRigidCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movq	%rdi, %rbx
	movq	%rdx, %rsi
	callq	_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo
	movq	$_ZTV29btSoftRigidCollisionAlgorithm+16, (%rbx)
	movb	%bpl, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN29btSoftRigidCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b, .Lfunc_end0-_ZN29btSoftRigidCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b
	.cfi_endproc

	.globl	_ZN29btSoftRigidCollisionAlgorithmD2Ev
	.p2align	4, 0x90
	.type	_ZN29btSoftRigidCollisionAlgorithmD2Ev,@function
_ZN29btSoftRigidCollisionAlgorithmD2Ev: # @_ZN29btSoftRigidCollisionAlgorithmD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	_ZN29btSoftRigidCollisionAlgorithmD2Ev, .Lfunc_end1-_ZN29btSoftRigidCollisionAlgorithmD2Ev
	.cfi_endproc

	.globl	_ZN29btSoftRigidCollisionAlgorithmD0Ev
	.p2align	4, 0x90
	.type	_ZN29btSoftRigidCollisionAlgorithmD0Ev,@function
_ZN29btSoftRigidCollisionAlgorithmD0Ev: # @_ZN29btSoftRigidCollisionAlgorithmD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end2:
	.size	_ZN29btSoftRigidCollisionAlgorithmD0Ev, .Lfunc_end2-_ZN29btSoftRigidCollisionAlgorithmD0Ev
	.cfi_endproc

	.globl	_ZN29btSoftRigidCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN29btSoftRigidCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN29btSoftRigidCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN29btSoftRigidCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	cmpb	$0, 32(%rdi)
	movq	%rsi, %rdi
	cmovneq	%rdx, %rdi
	cmovneq	%rsi, %rdx
	movslq	284(%rdi), %rax
	testq	%rax, %rax
	jle	.LBB3_4
# BB#1:                                 # %.lr.ph.i
	movq	296(%rdi), %rsi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	cmpq	%rdx, (%rsi,%rcx,8)
	je	.LBB3_5
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB3_2
	jmp	.LBB3_4
.LBB3_5:                                # %_ZNK20btAlignedObjectArrayIP17btCollisionObjectE16findLinearSearchERKS1_.exit
	cmpl	%ecx, %eax
	jne	.LBB3_6
.LBB3_4:                                # %_ZNK20btAlignedObjectArrayIP17btCollisionObjectE16findLinearSearchERKS1_.exit.thread
	movq	%rdx, %rsi
	jmp	_ZN10btSoftBody23defaultCollisionHandlerEP17btCollisionObject # TAILCALL
.LBB3_6:
	retq
.Lfunc_end3:
	.size	_ZN29btSoftRigidCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end3-_ZN29btSoftRigidCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN29btSoftRigidCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN29btSoftRigidCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN29btSoftRigidCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN29btSoftRigidCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	movss	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end4:
	.size	_ZN29btSoftRigidCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end4-_ZN29btSoftRigidCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.text._ZN29btSoftRigidCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,"axG",@progbits,_ZN29btSoftRigidCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,comdat
	.weak	_ZN29btSoftRigidCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.p2align	4, 0x90
	.type	_ZN29btSoftRigidCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,@function
_ZN29btSoftRigidCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE: # @_ZN29btSoftRigidCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end5:
	.size	_ZN29btSoftRigidCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE, .Lfunc_end5-_ZN29btSoftRigidCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_endproc

	.type	_ZTV29btSoftRigidCollisionAlgorithm,@object # @_ZTV29btSoftRigidCollisionAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTV29btSoftRigidCollisionAlgorithm
	.p2align	3
_ZTV29btSoftRigidCollisionAlgorithm:
	.quad	0
	.quad	_ZTI29btSoftRigidCollisionAlgorithm
	.quad	_ZN29btSoftRigidCollisionAlgorithmD2Ev
	.quad	_ZN29btSoftRigidCollisionAlgorithmD0Ev
	.quad	_ZN29btSoftRigidCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN29btSoftRigidCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN29btSoftRigidCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.size	_ZTV29btSoftRigidCollisionAlgorithm, 56

	.type	_ZTS29btSoftRigidCollisionAlgorithm,@object # @_ZTS29btSoftRigidCollisionAlgorithm
	.globl	_ZTS29btSoftRigidCollisionAlgorithm
	.p2align	4
_ZTS29btSoftRigidCollisionAlgorithm:
	.asciz	"29btSoftRigidCollisionAlgorithm"
	.size	_ZTS29btSoftRigidCollisionAlgorithm, 32

	.type	_ZTS20btCollisionAlgorithm,@object # @_ZTS20btCollisionAlgorithm
	.section	.rodata._ZTS20btCollisionAlgorithm,"aG",@progbits,_ZTS20btCollisionAlgorithm,comdat
	.weak	_ZTS20btCollisionAlgorithm
	.p2align	4
_ZTS20btCollisionAlgorithm:
	.asciz	"20btCollisionAlgorithm"
	.size	_ZTS20btCollisionAlgorithm, 23

	.type	_ZTI20btCollisionAlgorithm,@object # @_ZTI20btCollisionAlgorithm
	.section	.rodata._ZTI20btCollisionAlgorithm,"aG",@progbits,_ZTI20btCollisionAlgorithm,comdat
	.weak	_ZTI20btCollisionAlgorithm
	.p2align	3
_ZTI20btCollisionAlgorithm:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS20btCollisionAlgorithm
	.size	_ZTI20btCollisionAlgorithm, 16

	.type	_ZTI29btSoftRigidCollisionAlgorithm,@object # @_ZTI29btSoftRigidCollisionAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTI29btSoftRigidCollisionAlgorithm
	.p2align	4
_ZTI29btSoftRigidCollisionAlgorithm:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS29btSoftRigidCollisionAlgorithm
	.quad	_ZTI20btCollisionAlgorithm
	.size	_ZTI29btSoftRigidCollisionAlgorithm, 24


	.globl	_ZN29btSoftRigidCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b
	.type	_ZN29btSoftRigidCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b,@function
_ZN29btSoftRigidCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b = _ZN29btSoftRigidCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b
	.globl	_ZN29btSoftRigidCollisionAlgorithmD1Ev
	.type	_ZN29btSoftRigidCollisionAlgorithmD1Ev,@function
_ZN29btSoftRigidCollisionAlgorithmD1Ev = _ZN29btSoftRigidCollisionAlgorithmD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
