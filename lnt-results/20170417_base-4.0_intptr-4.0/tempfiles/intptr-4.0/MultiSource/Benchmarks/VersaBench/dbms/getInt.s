	.text
	.file	"getInt.bc"
	.globl	getInt
	.p2align	4, 0x90
	.type	getInt,@function
getInt:                                 # @getInt
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	callq	getString
	movq	$-2147483647, %r14      # imm = 0x80000001
	testq	%rax, %rax
	je	.LBB0_4
# BB#1:
	movq	%rsp, %rsi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	strtol
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.LBB0_7
# BB#2:                                 # %thread-pre-split
	cmpq	$-2147483647, %rax      # imm = 0x80000001
	jge	.LBB0_10
# BB#3:
	movl	$2, %eax
	jmp	.LBB0_5
.LBB0_4:
	movl	$1, %eax
.LBB0_5:                                # %.sink.split
	movq	%r14, (%rbx)
	jmp	.LBB0_6
.LBB0_7:
	movq	(%rsp), %rax
	cmpb	$0, (%rax)
	je	.LBB0_14
# BB#8:
	callq	__errno_location
	cmpl	$34, (%rax)
	jne	.LBB0_14
# BB#9:
	movl	$3, %eax
	jmp	.LBB0_5
.LBB0_10:
	movl	$2147483648, %ecx       # imm = 0x80000000
	cmpq	%rcx, %rax
	jl	.LBB0_14
# BB#11:
	movl	$2, %eax
	movl	$2147483647, %r14d      # imm = 0x7FFFFFFF
	jmp	.LBB0_5
.LBB0_14:
	xorl	%eax, %eax
.LBB0_6:                                # %.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	getInt, .Lfunc_end0-getInt
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
