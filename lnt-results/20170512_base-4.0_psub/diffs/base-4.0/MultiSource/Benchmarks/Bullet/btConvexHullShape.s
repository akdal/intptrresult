	.text
	.file	"btConvexHullShape.bc"
	.globl	_ZN17btConvexHullShapeC2EPKfii
	.p2align	4, 0x90
	.type	_ZN17btConvexHullShapeC2EPKfii,@function
_ZN17btConvexHullShapeC2EPKfii:         # @_ZN17btConvexHullShapeC2EPKfii
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movl	%edx, %r15d
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	_ZN34btPolyhedralConvexAabbCachingShapeC2Ev
	movq	$_ZTV17btConvexHullShape+16, (%r14)
	movb	$1, 128(%r14)
	movq	$0, 120(%r14)
	movl	$0, 108(%r14)
	movl	$0, 112(%r14)
	movl	$4, 8(%r14)
	testl	%r15d, %r15d
	jle	.LBB0_1
# BB#4:
	movslq	%r15d, %r13
	movq	%r13, %rdi
	shlq	$4, %rdi
.Ltmp0:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
.Ltmp1:
# BB#5:                                 # %_ZN20btAlignedObjectArrayI9btVector3E8allocateEi.exit.i.i
	movslq	108(%r14), %rax
	testq	%rax, %rax
	jle	.LBB0_13
# BB#6:                                 # %.lr.ph.i.i.i
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB0_7
# BB#8:                                 # %.prol.preheader46
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_9:                                # =>This Inner Loop Header: Depth=1
	movq	120(%r14), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	%xmm0, (%rbp,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB0_9
	jmp	.LBB0_10
.LBB0_1:                                # %.loopexit.thread
	movl	%r15d, 108(%r14)
	jmp	.LBB0_2
.LBB0_7:
	xorl	%ecx, %ecx
.LBB0_10:                               # %.prol.loopexit47
	cmpq	$3, %r8
	jb	.LBB0_13
# BB#11:                                # %.lr.ph.i.i.i.new
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB0_12:                               # =>This Inner Loop Header: Depth=1
	movq	120(%r14), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%rbp,%rcx)
	movq	120(%r14), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%rbp,%rcx)
	movq	120(%r14), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%rbp,%rcx)
	movq	120(%r14), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%rbp,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB0_12
.LBB0_13:                               # %_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_.exit.i.i
	movq	120(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_17
# BB#14:
	cmpb	$0, 128(%r14)
	je	.LBB0_16
# BB#15:
.Ltmp2:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp3:
.LBB0_16:                               # %.noexc25
	movq	$0, 120(%r14)
.LBB0_17:                               # %.lr.ph.i
	movb	$1, 128(%r14)
	movq	%rbp, 120(%r14)
	movl	%r15d, 112(%r14)
	movups	8(%rsp), %xmm0
	movups	%xmm0, (%rbp)
	cmpl	$1, %r15d
	je	.LBB0_26
# BB#18:                                # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i._ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i_crit_edge.preheader
	leal	3(%r15), %edx
	leaq	-2(%r13), %rcx
	andq	$3, %rdx
	je	.LBB0_19
# BB#20:                                # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i._ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i_crit_edge.prol.preheader
	xorl	%eax, %eax
	movl	$16, %esi
	.p2align	4, 0x90
.LBB0_21:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i._ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	120(%r14), %rdi
	movups	8(%rsp), %xmm0
	movups	%xmm0, (%rdi,%rsi)
	incq	%rax
	addq	$16, %rsi
	cmpq	%rax, %rdx
	jne	.LBB0_21
# BB#22:                                # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i._ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i_crit_edge.prol.loopexit.unr-lcssa
	incq	%rax
	cmpq	$3, %rcx
	jae	.LBB0_24
	jmp	.LBB0_26
.LBB0_19:
	movl	$1, %eax
	cmpq	$3, %rcx
	jb	.LBB0_26
.LBB0_24:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i._ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i_crit_edge.preheader.new
	subq	%rax, %r13
	shlq	$4, %rax
	addq	$48, %rax
	.p2align	4, 0x90
.LBB0_25:                               # %_ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i._ZN20btAlignedObjectArrayI9btVector3E7reserveEi.exit.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	120(%r14), %rcx
	movups	8(%rsp), %xmm0
	movups	%xmm0, -48(%rcx,%rax)
	movq	120(%r14), %rcx
	movups	8(%rsp), %xmm0
	movups	%xmm0, -32(%rcx,%rax)
	movq	120(%r14), %rcx
	movups	8(%rsp), %xmm0
	movups	%xmm0, -16(%rcx,%rax)
	movq	120(%r14), %rcx
	movups	8(%rsp), %xmm0
	movups	%xmm0, (%rcx,%rax)
	addq	$64, %rax
	addq	$-4, %r13
	jne	.LBB0_25
.LBB0_26:                               # %.loopexit
	movl	%r15d, 108(%r14)
	testl	%r15d, %r15d
	jle	.LBB0_2
# BB#27:                                # %.lr.ph
	movslq	%r12d, %r8
	movl	%r15d, %ecx
	testb	$1, %cl
	jne	.LBB0_29
# BB#28:
	xorl	%edx, %edx
	cmpl	$1, %r15d
	jne	.LBB0_31
	jmp	.LBB0_2
.LBB0_29:
	movl	(%rbx), %edx
	movl	4(%rbx), %esi
	movl	8(%rbx), %edi
	movq	120(%r14), %rbp
	movl	%edx, (%rbp)
	movl	%esi, 4(%rbp)
	movl	%edi, 8(%rbp)
	movl	$0, 12(%rbp)
	addq	%r8, %rbx
	movl	$1, %edx
	cmpl	$1, %r15d
	je	.LBB0_2
.LBB0_31:                               # %.lr.ph.new
	subq	%rdx, %rcx
	shlq	$4, %rdx
	.p2align	4, 0x90
.LBB0_32:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %esi
	movl	4(%rbx), %edi
	movl	8(%rbx), %ebp
	movq	120(%r14), %rax
	movl	%esi, (%rax,%rdx)
	movl	%edi, 4(%rax,%rdx)
	movl	%ebp, 8(%rax,%rdx)
	movl	$0, 12(%rax,%rdx)
	movl	(%rbx,%r8), %eax
	movl	4(%rbx,%r8), %esi
	movl	8(%rbx,%r8), %edi
	leaq	(%rbx,%r8), %rbx
	movq	120(%r14), %rbp
	movl	%eax, 16(%rbp,%rdx)
	movl	%esi, 20(%rbp,%rdx)
	movl	%edi, 24(%rbp,%rdx)
	movl	$0, 28(%rbp,%rdx)
	addq	%r8, %rbx
	addq	$32, %rdx
	addq	$-2, %rcx
	jne	.LBB0_32
.LBB0_2:                                # %._crit_edge
.Ltmp5:
	movq	%r14, %rdi
	callq	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv
.Ltmp6:
# BB#3:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_41:
.Ltmp4:
	jmp	.LBB0_34
.LBB0_33:
.Ltmp7:
.LBB0_34:
	movq	%rax, %rbx
	movq	120(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_38
# BB#35:
	cmpb	$0, 128(%r14)
	je	.LBB0_37
# BB#36:
.Ltmp8:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp9:
.LBB0_37:                               # %.noexc
	movq	$0, 120(%r14)
.LBB0_38:
	movb	$1, 128(%r14)
	movq	$0, 120(%r14)
	movq	$0, 108(%r14)
.Ltmp10:
	movq	%r14, %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp11:
# BB#39:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_40:
.Ltmp12:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN17btConvexHullShapeC2EPKfii, .Lfunc_end0-_ZN17btConvexHullShapeC2EPKfii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp11-.Ltmp8          #   Call between .Ltmp8 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	1                       #   On action: 1
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Lfunc_end0-.Ltmp11     #   Call between .Ltmp11 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN17btConvexHullShape15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN17btConvexHullShape15setLocalScalingERK9btVector3,@function
_ZN17btConvexHullShape15setLocalScalingERK9btVector3: # @_ZN17btConvexHullShape15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	movups	(%rsi), %xmm0
	movups	%xmm0, 24(%rdi)
	jmp	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv # TAILCALL
.Lfunc_end2:
	.size	_ZN17btConvexHullShape15setLocalScalingERK9btVector3, .Lfunc_end2-_ZN17btConvexHullShape15setLocalScalingERK9btVector3
	.cfi_endproc

	.globl	_ZN17btConvexHullShape8addPointERK9btVector3
	.p2align	4, 0x90
	.type	_ZN17btConvexHullShape8addPointERK9btVector3,@function
_ZN17btConvexHullShape8addPointERK9btVector3: # @_ZN17btConvexHullShape8addPointERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	108(%rbx), %eax
	cmpl	112(%rbx), %eax
	jne	.LBB3_18
# BB#1:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%ecx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB3_18
# BB#2:
	testl	%ebp, %ebp
	je	.LBB3_3
# BB#4:
	movslq	%ebp, %rdi
	shlq	$4, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	108(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB3_6
	jmp	.LBB3_13
.LBB3_3:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.LBB3_13
.LBB3_6:                                # %.lr.ph.i.i.i
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB3_7
# BB#8:                                 # %.prol.preheader
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_9:                                # =>This Inner Loop Header: Depth=1
	movq	120(%rbx), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	%xmm0, (%r15,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB3_9
	jmp	.LBB3_10
.LBB3_7:
	xorl	%ecx, %ecx
.LBB3_10:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB3_13
# BB#11:                                # %.lr.ph.i.i.i.new
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB3_12:                               # =>This Inner Loop Header: Depth=1
	movq	120(%rbx), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%r15,%rcx)
	movq	120(%rbx), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%r15,%rcx)
	movq	120(%rbx), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%r15,%rcx)
	movq	120(%rbx), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%r15,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB3_12
.LBB3_13:                               # %_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_.exit.i.i
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_17
# BB#14:
	cmpb	$0, 128(%rbx)
	je	.LBB3_16
# BB#15:
	callq	_Z21btAlignedFreeInternalPv
.LBB3_16:
	movq	$0, 120(%rbx)
.LBB3_17:                               # %_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv.exit.i.i
	movb	$1, 128(%rbx)
	movq	%r15, 120(%rbx)
	movl	%ebp, 112(%rbx)
	movl	108(%rbx), %eax
.LBB3_18:                               # %_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_.exit
	movq	120(%rbx), %rcx
	cltq
	shlq	$4, %rax
	movups	(%r14), %xmm0
	movups	%xmm0, (%rcx,%rax)
	incl	108(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv # TAILCALL
.Lfunc_end3:
	.size	_ZN17btConvexHullShape8addPointERK9btVector3, .Lfunc_end3-_ZN17btConvexHullShape8addPointERK9btVector3
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1065353216              # float 1
.LCPI4_1:
	.long	953267991               # float 9.99999974E-5
.LCPI4_2:
	.long	3713928043              # float -9.99999984E+17
	.text
	.globl	_ZNK17btConvexHullShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK17btConvexHullShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK17btConvexHullShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK17btConvexHullShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movss	(%rsi), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm10, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm11, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	.LCPI4_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB4_2
# BB#1:
	xorps	%xmm10, %xmm10
	movss	.LCPI4_0(%rip), %xmm9   # xmm9 = mem[0],zero,zero,zero
	xorps	%xmm11, %xmm11
	jmp	.LBB4_5
.LBB4_2:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB4_4
# BB#3:                                 # %call.sqrt
	movss	%xmm9, 12(%rsp)         # 4-byte Spill
	movss	%xmm10, 8(%rsp)         # 4-byte Spill
	movss	%xmm11, 4(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm11         # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm10         # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB4_4:                                # %.split
	movss	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm9
	mulss	%xmm0, %xmm10
	mulss	%xmm0, %xmm11
.LBB4_5:                                # %.preheader
	movslq	108(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB4_6
# BB#8:                                 # %.lr.ph
	movq	120(%rbx), %rcx
	movsd	24(%rbx), %xmm8         # xmm8 = mem[0],zero
	movss	32(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addq	$8, %rcx
	xorps	%xmm1, %xmm1
	movss	.LCPI4_2(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	xorl	%edx, %edx
	xorps	%xmm5, %xmm5
	.p2align	4, 0x90
.LBB4_9:                                # =>This Inner Loop Header: Depth=1
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	mulps	%xmm8, %xmm0
	movss	(%rcx), %xmm7           # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm7
	movaps	%xmm0, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movaps	%xmm9, %xmm6
	mulss	%xmm0, %xmm6
	mulss	%xmm10, %xmm2
	addss	%xmm6, %xmm2
	movaps	%xmm11, %xmm6
	mulss	%xmm7, %xmm6
	addss	%xmm2, %xmm6
	ucomiss	%xmm4, %xmm6
	ja	.LBB4_10
# BB#11:                                #   in Loop: Header=BB4_9 Depth=1
	movaps	%xmm5, %xmm0
	jmp	.LBB4_12
	.p2align	4, 0x90
.LBB4_10:                               #   in Loop: Header=BB4_9 Depth=1
	xorps	%xmm1, %xmm1
	movss	%xmm7, %xmm1            # xmm1 = xmm7[0],xmm1[1,2,3]
.LBB4_12:                               #   in Loop: Header=BB4_9 Depth=1
	maxss	%xmm4, %xmm6
	incq	%rdx
	addq	$16, %rcx
	cmpq	%rax, %rdx
	movaps	%xmm6, %xmm4
	movaps	%xmm0, %xmm5
	jl	.LBB4_9
	jmp	.LBB4_7
.LBB4_6:
	xorps	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
.LBB4_7:                                # %._crit_edge
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZNK17btConvexHullShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end4-_ZNK17btConvexHullShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.globl	_ZNK17btConvexHullShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK17btConvexHullShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK17btConvexHullShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK17btConvexHullShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_startproc
# BB#0:
	testl	%ecx, %ecx
	jle	.LBB5_8
# BB#1:                                 # %.lr.ph42.preheader
	movl	%ecx, %r8d
	leaq	-1(%r8), %r9
	movq	%r8, %r10
	andq	$7, %r10
	je	.LBB5_2
# BB#3:                                 # %.lr.ph42.prol.preheader
	leaq	12(%rdx), %rax
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB5_4:                                # %.lr.ph42.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$-581039253, (%rax)     # imm = 0xDD5E0B6B
	incq	%r11
	addq	$16, %rax
	cmpq	%r11, %r10
	jne	.LBB5_4
	jmp	.LBB5_5
.LBB5_2:
	xorl	%r11d, %r11d
.LBB5_5:                                # %.lr.ph42.prol.loopexit
	cmpq	$7, %r9
	jb	.LBB5_8
# BB#6:                                 # %.lr.ph42.preheader.new
	subq	%r11, %r8
	shlq	$4, %r11
	leaq	124(%rdx,%r11), %rax
	.p2align	4, 0x90
.LBB5_7:                                # %.lr.ph42
                                        # =>This Inner Loop Header: Depth=1
	movl	$-581039253, -112(%rax) # imm = 0xDD5E0B6B
	movl	$-581039253, -96(%rax)  # imm = 0xDD5E0B6B
	movl	$-581039253, -80(%rax)  # imm = 0xDD5E0B6B
	movl	$-581039253, -64(%rax)  # imm = 0xDD5E0B6B
	movl	$-581039253, -48(%rax)  # imm = 0xDD5E0B6B
	movl	$-581039253, -32(%rax)  # imm = 0xDD5E0B6B
	movl	$-581039253, -16(%rax)  # imm = 0xDD5E0B6B
	movl	$-581039253, (%rax)     # imm = 0xDD5E0B6B
	subq	$-128, %rax
	addq	$-8, %r8
	jne	.LBB5_7
.LBB5_8:                                # %.preheader
	movl	108(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB5_20
# BB#9:                                 # %.lr.ph38
	testl	%ecx, %ecx
	jle	.LBB5_10
# BB#14:                                # %.lr.ph38.split.us.preheader
	movl	%ecx, %r8d
	addq	$8, %rsi
	addq	$12, %rdx
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB5_15:                               # %.lr.ph38.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_16 Depth 2
	movq	120(%rdi), %rax
	movq	%r9, %rcx
	shlq	$4, %rcx
	movsd	(%rax,%rcx), %xmm1      # xmm1 = mem[0],zero
	movsd	24(%rdi), %xmm0         # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	8(%rax,%rcx), %xmm2     # xmm2 = mem[0],zero,zero,zero
	mulss	32(%rdi), %xmm2
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movq	%r8, %r10
	movq	%rdx, %rcx
	movq	%rsi, %rax
	.p2align	4, 0x90
.LBB5_16:                               #   Parent Loop BB5_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-8(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	-4(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	addss	%xmm4, %xmm5
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	addss	%xmm5, %xmm4
	ucomiss	(%rcx), %xmm4
	jbe	.LBB5_18
# BB#17:                                #   in Loop: Header=BB5_16 Depth=2
	movlps	%xmm0, -12(%rcx)
	movlps	%xmm3, -4(%rcx)
	movss	%xmm4, (%rcx)
.LBB5_18:                               #   in Loop: Header=BB5_16 Depth=2
	addq	$16, %rax
	addq	$16, %rcx
	decq	%r10
	jne	.LBB5_16
# BB#19:                                # %._crit_edge.us
                                        #   in Loop: Header=BB5_15 Depth=1
	incq	%r9
	movslq	108(%rdi), %rax
	cmpq	%rax, %r9
	jl	.LBB5_15
	jmp	.LBB5_20
.LBB5_10:                               # %.lr.ph38.split.preheader
	leal	-1(%rax), %edx
	movl	%eax, %esi
	xorl	%ecx, %ecx
	andl	$7, %esi
	je	.LBB5_12
	.p2align	4, 0x90
.LBB5_11:                               # %.lr.ph38.split.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	%ecx, %esi
	jne	.LBB5_11
.LBB5_12:                               # %.lr.ph38.split.prol.loopexit
	cmpl	$7, %edx
	jb	.LBB5_20
	.p2align	4, 0x90
.LBB5_13:                               # %.lr.ph38.split
                                        # =>This Inner Loop Header: Depth=1
	addl	$8, %ecx
	cmpl	%eax, %ecx
	jl	.LBB5_13
.LBB5_20:                               # %._crit_edge39
	retq
.Lfunc_end5:
	.size	_ZNK17btConvexHullShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end5-_ZNK17btConvexHullShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_0:
	.long	679477248               # float 1.42108547E-14
.LCPI6_2:
	.long	3212836864              # float -1
.LCPI6_3:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_1:
	.long	3212836864              # float -1
	.long	3212836864              # float -1
	.zero	4
	.zero	4
	.text
	.globl	_ZNK17btConvexHullShape24localGetSupportingVertexERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK17btConvexHullShape24localGetSupportingVertexERK9btVector3,@function
_ZNK17btConvexHullShape24localGetSupportingVertexERK9btVector3: # @_ZNK17btConvexHullShape24localGetSupportingVertexERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 24
	subq	$72, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 96
.Lcfi28:
	.cfi_offset %rbx, -24
.Lcfi29:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%r14), %rax
	callq	*104(%rax)
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB6_2
	jnp	.LBB6_1
.LBB6_2:
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movss	8(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	mulss	%xmm1, %xmm1
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	movss	.LCPI6_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	xorl	%eax, %eax
	ucomiss	%xmm1, %xmm2
	seta	%al
	movd	%eax, %xmm1
	pshufd	$80, %xmm1, %xmm2       # xmm2 = xmm1[0,0,1,1]
	pslld	$31, %xmm2
	psrad	$31, %xmm2
	movdqa	%xmm2, %xmm1
	pandn	%xmm0, %xmm1
	pand	.LCPI6_1(%rip), %xmm2
	por	%xmm1, %xmm2
	pshufd	$229, %xmm2, %xmm0      # xmm0 = xmm2[1,1,2,3]
	movdqa	%xmm2, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	jbe	.LBB6_4
# BB#3:
	movss	.LCPI6_2(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
.LBB6_4:
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB6_6
# BB#5:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movdqa	%xmm2, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movdqa	(%rsp), %xmm2           # 16-byte Reload
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB6_6:                                # %.split
	movss	.LCPI6_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	mulss	%xmm3, %xmm1
	movaps	%xmm1, (%rsp)           # 16-byte Spill
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	movaps	(%rsp), %xmm2           # 16-byte Reload
	mulss	%xmm0, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	16(%rsp), %xmm0         # 16-byte Folded Reload
	movaps	48(%rsp), %xmm1         # 16-byte Reload
	addps	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	addss	%xmm2, %xmm1
	jmp	.LBB6_7
.LBB6_1:
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movaps	32(%rsp), %xmm1         # 16-byte Reload
.LBB6_7:
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	_ZNK17btConvexHullShape24localGetSupportingVertexERK9btVector3, .Lfunc_end6-_ZNK17btConvexHullShape24localGetSupportingVertexERK9btVector3
	.cfi_endproc

	.globl	_ZNK17btConvexHullShape14getNumVerticesEv
	.p2align	4, 0x90
	.type	_ZNK17btConvexHullShape14getNumVerticesEv,@function
_ZNK17btConvexHullShape14getNumVerticesEv: # @_ZNK17btConvexHullShape14getNumVerticesEv
	.cfi_startproc
# BB#0:
	movl	108(%rdi), %eax
	retq
.Lfunc_end7:
	.size	_ZNK17btConvexHullShape14getNumVerticesEv, .Lfunc_end7-_ZNK17btConvexHullShape14getNumVerticesEv
	.cfi_endproc

	.globl	_ZNK17btConvexHullShape11getNumEdgesEv
	.p2align	4, 0x90
	.type	_ZNK17btConvexHullShape11getNumEdgesEv,@function
_ZNK17btConvexHullShape11getNumEdgesEv: # @_ZNK17btConvexHullShape11getNumEdgesEv
	.cfi_startproc
# BB#0:
	movl	108(%rdi), %eax
	retq
.Lfunc_end8:
	.size	_ZNK17btConvexHullShape11getNumEdgesEv, .Lfunc_end8-_ZNK17btConvexHullShape11getNumEdgesEv
	.cfi_endproc

	.globl	_ZNK17btConvexHullShape7getEdgeEiR9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK17btConvexHullShape7getEdgeEiR9btVector3S1_,@function
_ZNK17btConvexHullShape7getEdgeEiR9btVector3S1_: # @_ZNK17btConvexHullShape7getEdgeEiR9btVector3S1_
	.cfi_startproc
# BB#0:
	movq	%rdx, %r8
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	108(%rdi), %r10d
	movl	%esi, %eax
	cltd
	idivl	%r10d
	movl	%edx, %r9d
	leal	1(%rsi), %eax
	cltd
	idivl	%r10d
	movq	120(%rdi), %rax
	movslq	%r9d, %rsi
	shlq	$4, %rsi
	movsd	(%rax,%rsi), %xmm0      # xmm0 = mem[0],zero
	movsd	24(%rdi), %xmm1         # xmm1 = mem[0],zero
	mulps	%xmm0, %xmm1
	movss	8(%rax,%rsi), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	32(%rdi), %xmm0
	xorps	%xmm2, %xmm2
	xorps	%xmm3, %xmm3
	movss	%xmm0, %xmm3            # xmm3 = xmm0[0],xmm3[1,2,3]
	movlps	%xmm1, (%r8)
	movlps	%xmm3, 8(%r8)
	movq	120(%rdi), %rax
	movslq	%edx, %rdx
	shlq	$4, %rdx
	movsd	(%rax,%rdx), %xmm0      # xmm0 = mem[0],zero
	movsd	24(%rdi), %xmm1         # xmm1 = mem[0],zero
	mulps	%xmm0, %xmm1
	movss	8(%rax,%rdx), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	32(%rdi), %xmm0
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, (%rcx)
	movlps	%xmm2, 8(%rcx)
	retq
.Lfunc_end9:
	.size	_ZNK17btConvexHullShape7getEdgeEiR9btVector3S1_, .Lfunc_end9-_ZNK17btConvexHullShape7getEdgeEiR9btVector3S1_
	.cfi_endproc

	.globl	_ZNK17btConvexHullShape9getVertexEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK17btConvexHullShape9getVertexEiR9btVector3,@function
_ZNK17btConvexHullShape9getVertexEiR9btVector3: # @_ZNK17btConvexHullShape9getVertexEiR9btVector3
	.cfi_startproc
# BB#0:
	movq	120(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	movsd	(%rax,%rcx), %xmm0      # xmm0 = mem[0],zero
	movsd	24(%rdi), %xmm1         # xmm1 = mem[0],zero
	mulps	%xmm0, %xmm1
	movss	8(%rax,%rcx), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	32(%rdi), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, (%rdx)
	movlps	%xmm2, 8(%rdx)
	retq
.Lfunc_end10:
	.size	_ZNK17btConvexHullShape9getVertexEiR9btVector3, .Lfunc_end10-_ZNK17btConvexHullShape9getVertexEiR9btVector3
	.cfi_endproc

	.globl	_ZNK17btConvexHullShape12getNumPlanesEv
	.p2align	4, 0x90
	.type	_ZNK17btConvexHullShape12getNumPlanesEv,@function
_ZNK17btConvexHullShape12getNumPlanesEv: # @_ZNK17btConvexHullShape12getNumPlanesEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end11:
	.size	_ZNK17btConvexHullShape12getNumPlanesEv, .Lfunc_end11-_ZNK17btConvexHullShape12getNumPlanesEv
	.cfi_endproc

	.globl	_ZNK17btConvexHullShape8getPlaneER9btVector3S1_i
	.p2align	4, 0x90
	.type	_ZNK17btConvexHullShape8getPlaneER9btVector3S1_i,@function
_ZNK17btConvexHullShape8getPlaneER9btVector3S1_i: # @_ZNK17btConvexHullShape8getPlaneER9btVector3S1_i
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end12:
	.size	_ZNK17btConvexHullShape8getPlaneER9btVector3S1_i, .Lfunc_end12-_ZNK17btConvexHullShape8getPlaneER9btVector3S1_i
	.cfi_endproc

	.globl	_ZNK17btConvexHullShape8isInsideERK9btVector3f
	.p2align	4, 0x90
	.type	_ZNK17btConvexHullShape8isInsideERK9btVector3f,@function
_ZNK17btConvexHullShape8isInsideERK9btVector3f: # @_ZNK17btConvexHullShape8isInsideERK9btVector3f
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end13:
	.size	_ZNK17btConvexHullShape8isInsideERK9btVector3f, .Lfunc_end13-_ZNK17btConvexHullShape8isInsideERK9btVector3f
	.cfi_endproc

	.section	.text._ZN17btConvexHullShapeD2Ev,"axG",@progbits,_ZN17btConvexHullShapeD2Ev,comdat
	.weak	_ZN17btConvexHullShapeD2Ev
	.p2align	4, 0x90
	.type	_ZN17btConvexHullShapeD2Ev,@function
_ZN17btConvexHullShapeD2Ev:             # @_ZN17btConvexHullShapeD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -24
.Lcfi34:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV17btConvexHullShape+16, (%rbx)
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_4
# BB#1:
	cmpb	$0, 128(%rbx)
	je	.LBB14_3
# BB#2:
.Ltmp13:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp14:
.LBB14_3:                               # %.noexc
	movq	$0, 120(%rbx)
.LBB14_4:
	movb	$1, 128(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 108(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN13btConvexShapeD2Ev  # TAILCALL
.LBB14_5:
.Ltmp15:
	movq	%rax, %r14
.Ltmp16:
	movq	%rbx, %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp17:
# BB#6:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB14_7:
.Ltmp18:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end14:
	.size	_ZN17btConvexHullShapeD2Ev, .Lfunc_end14-_ZN17btConvexHullShapeD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin1   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp16-.Ltmp14         #   Call between .Ltmp14 and .Ltmp16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin1   #     jumps to .Ltmp18
	.byte	1                       #   On action: 1
	.long	.Ltmp17-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end14-.Ltmp17    #   Call between .Ltmp17 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN17btConvexHullShapeD0Ev,"axG",@progbits,_ZN17btConvexHullShapeD0Ev,comdat
	.weak	_ZN17btConvexHullShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN17btConvexHullShapeD0Ev,@function
_ZN17btConvexHullShapeD0Ev:             # @_ZN17btConvexHullShapeD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV17btConvexHullShape+16, (%rbx)
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_4
# BB#1:
	cmpb	$0, 128(%rbx)
	je	.LBB15_3
# BB#2:
.Ltmp19:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp20:
.LBB15_3:                               # %.noexc.i
	movq	$0, 120(%rbx)
.LBB15_4:
	movb	$1, 128(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 108(%rbx)
.Ltmp25:
	movq	%rbx, %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp26:
# BB#5:                                 # %_ZN17btConvexHullShapeD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB15_6:
.Ltmp21:
	movq	%rax, %r14
.Ltmp22:
	movq	%rbx, %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp23:
	jmp	.LBB15_9
.LBB15_7:
.Ltmp24:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_8:
.Ltmp27:
	movq	%rax, %r14
.LBB15_9:                               # %.body
.Ltmp28:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp29:
# BB#10:                                # %_ZN17btConvexHullShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB15_11:
.Ltmp30:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZN17btConvexHullShapeD0Ev, .Lfunc_end15-_ZN17btConvexHullShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp19-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin2   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin2   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp22-.Ltmp26         #   Call between .Ltmp26 and .Ltmp22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin2   #     jumps to .Ltmp24
	.byte	1                       #   On action: 1
	.long	.Ltmp28-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin2   #     jumps to .Ltmp30
	.byte	1                       #   On action: 1
	.long	.Ltmp29-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Lfunc_end15-.Ltmp29    #   Call between .Ltmp29 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK21btConvexInternalShape15getLocalScalingEv,"axG",@progbits,_ZNK21btConvexInternalShape15getLocalScalingEv,comdat
	.weak	_ZNK21btConvexInternalShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape15getLocalScalingEv,@function
_ZNK21btConvexInternalShape15getLocalScalingEv: # @_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	24(%rdi), %rax
	retq
.Lfunc_end16:
	.size	_ZNK21btConvexInternalShape15getLocalScalingEv, .Lfunc_end16-_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK17btConvexHullShape7getNameEv,"axG",@progbits,_ZNK17btConvexHullShape7getNameEv,comdat
	.weak	_ZNK17btConvexHullShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK17btConvexHullShape7getNameEv,@function
_ZNK17btConvexHullShape7getNameEv:      # @_ZNK17btConvexHullShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end17:
	.size	_ZNK17btConvexHullShape7getNameEv, .Lfunc_end17-_ZNK17btConvexHullShape7getNameEv
	.cfi_endproc

	.section	.text._ZN21btConvexInternalShape9setMarginEf,"axG",@progbits,_ZN21btConvexInternalShape9setMarginEf,comdat
	.weak	_ZN21btConvexInternalShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN21btConvexInternalShape9setMarginEf,@function
_ZN21btConvexInternalShape9setMarginEf: # @_ZN21btConvexInternalShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 56(%rdi)
	retq
.Lfunc_end18:
	.size	_ZN21btConvexInternalShape9setMarginEf, .Lfunc_end18-_ZN21btConvexInternalShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape9getMarginEv,"axG",@progbits,_ZNK21btConvexInternalShape9getMarginEv,comdat
	.weak	_ZNK21btConvexInternalShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape9getMarginEv,@function
_ZNK21btConvexInternalShape9getMarginEv: # @_ZNK21btConvexInternalShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	56(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end19:
	.size	_ZNK21btConvexInternalShape9getMarginEv, .Lfunc_end19-_ZNK21btConvexInternalShape9getMarginEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,"axG",@progbits,_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,comdat
	.weak	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end20:
	.size	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end20-_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,"axG",@progbits,_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,comdat
	.weak	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end21:
	.size	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end21-_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.type	_ZTV17btConvexHullShape,@object # @_ZTV17btConvexHullShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV17btConvexHullShape
	.p2align	3
_ZTV17btConvexHullShape:
	.quad	0
	.quad	_ZTI17btConvexHullShape
	.quad	_ZN17btConvexHullShapeD2Ev
	.quad	_ZN17btConvexHullShapeD0Ev
	.quad	_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN17btConvexHullShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK17btConvexHullShape7getNameEv
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK17btConvexHullShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK17btConvexHullShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK17btConvexHullShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.quad	_ZNK17btConvexHullShape14getNumVerticesEv
	.quad	_ZNK17btConvexHullShape11getNumEdgesEv
	.quad	_ZNK17btConvexHullShape7getEdgeEiR9btVector3S1_
	.quad	_ZNK17btConvexHullShape9getVertexEiR9btVector3
	.quad	_ZNK17btConvexHullShape12getNumPlanesEv
	.quad	_ZNK17btConvexHullShape8getPlaneER9btVector3S1_i
	.quad	_ZNK17btConvexHullShape8isInsideERK9btVector3f
	.size	_ZTV17btConvexHullShape, 216

	.type	_ZTS17btConvexHullShape,@object # @_ZTS17btConvexHullShape
	.globl	_ZTS17btConvexHullShape
	.p2align	4
_ZTS17btConvexHullShape:
	.asciz	"17btConvexHullShape"
	.size	_ZTS17btConvexHullShape, 20

	.type	_ZTI17btConvexHullShape,@object # @_ZTI17btConvexHullShape
	.globl	_ZTI17btConvexHullShape
	.p2align	4
_ZTI17btConvexHullShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS17btConvexHullShape
	.quad	_ZTI34btPolyhedralConvexAabbCachingShape
	.size	_ZTI17btConvexHullShape, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Convex"
	.size	.L.str, 7


	.globl	_ZN17btConvexHullShapeC1EPKfii
	.type	_ZN17btConvexHullShapeC1EPKfii,@function
_ZN17btConvexHullShapeC1EPKfii = _ZN17btConvexHullShapeC2EPKfii
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
