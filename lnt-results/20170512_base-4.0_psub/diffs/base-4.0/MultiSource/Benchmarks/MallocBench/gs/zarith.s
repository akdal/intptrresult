	.text
	.file	"zarith.bc"
	.globl	zadd
	.p2align	4, 0x90
	.type	zadd,@function
zadd:                                   # @zadd
	.cfi_startproc
# BB#0:
	movb	8(%rdi), %cl
	shrb	$2, %cl
	movl	$-20, %eax
	cmpb	$5, %cl
	je	.LBB0_7
# BB#1:
	cmpb	$11, %cl
	jne	.LBB0_15
# BB#2:
	movb	-8(%rdi), %cl
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB0_6
# BB#3:
	cmpb	$11, %cl
	jne	.LBB0_15
# BB#4:
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB0_5
.LBB0_7:
	movb	-8(%rdi), %cl
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB0_10
# BB#8:
	cmpb	$11, %cl
	jne	.LBB0_15
# BB#9:
	cvtsi2ssq	(%rdi), %xmm0
.LBB0_5:
	addss	-16(%rdi), %xmm0
	movss	%xmm0, -16(%rdi)
	jmp	.LBB0_14
.LBB0_6:
	cvtsi2ssq	-16(%rdi), %xmm0
	addss	(%rdi), %xmm0
	jmp	.LBB0_13
.LBB0_10:
	movq	-16(%rdi), %rdx
	movq	(%rdi), %rax
	leaq	(%rdx,%rax), %rcx
	movq	%rcx, -16(%rdi)
	xorq	%rax, %rdx
	js	.LBB0_14
# BB#11:
	movq	%rcx, %rdx
	xorq	%rax, %rdx
	jns	.LBB0_14
# BB#12:
	cvtsi2ssq	%rcx, %xmm0
	cvtsi2ssq	%rax, %xmm1
	subss	%xmm1, %xmm0
.LBB0_13:
	movss	%xmm0, -16(%rdi)
	movw	$44, -8(%rdi)
.LBB0_14:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB0_15:
	retq
.Lfunc_end0:
	.size	zadd, .Lfunc_end0-zadd
	.cfi_endproc

	.globl	zdiv
	.p2align	4, 0x90
	.type	zdiv,@function
zdiv:                                   # @zdiv
	.cfi_startproc
# BB#0:
	movb	8(%rdi), %cl
	shrb	$2, %cl
	movl	$-20, %eax
	cmpb	$5, %cl
	je	.LBB1_7
# BB#1:
	cmpb	$11, %cl
	jne	.LBB1_15
# BB#2:
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movl	$-23, %eax
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB1_3
	jnp	.LBB1_15
.LBB1_3:
	movb	-8(%rdi), %al
	shrb	$2, %al
	cmpb	$5, %al
	je	.LBB1_6
# BB#4:
	cmpb	$11, %al
	movl	$-20, %eax
	je	.LBB1_5
	jmp	.LBB1_15
.LBB1_7:
	movq	(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB1_8
# BB#9:
	movb	-8(%rdi), %dl
	shrb	$2, %dl
	cmpb	$5, %dl
	je	.LBB1_12
# BB#10:
	cmpb	$11, %dl
	jne	.LBB1_15
# BB#11:
	cvtsi2ssq	%rcx, %xmm0
.LBB1_5:
	movss	-16(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movss	%xmm1, -16(%rdi)
	jmp	.LBB1_14
.LBB1_8:
	movl	$-23, %eax
	retq
.LBB1_12:
	cvtsi2ssq	-16(%rdi), %xmm0
	cvtsi2ssq	%rcx, %xmm1
	divss	%xmm1, %xmm0
	movss	%xmm0, -16(%rdi)
	jmp	.LBB1_13
.LBB1_6:
	xorps	%xmm1, %xmm1
	cvtsi2ssq	-16(%rdi), %xmm1
	divss	%xmm0, %xmm1
	movss	%xmm1, -16(%rdi)
.LBB1_13:
	movw	$44, -8(%rdi)
.LBB1_14:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB1_15:
	retq
.Lfunc_end1:
	.size	zdiv, .Lfunc_end1-zdiv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	3472883712              # float -2.14748365E+9
	.text
	.globl	zmul
	.p2align	4, 0x90
	.type	zmul,@function
zmul:                                   # @zmul
	.cfi_startproc
# BB#0:
	movb	8(%rdi), %cl
	shrb	$2, %cl
	movl	$-20, %eax
	cmpb	$5, %cl
	je	.LBB2_7
# BB#1:
	cmpb	$11, %cl
	jne	.LBB2_18
# BB#2:
	movb	-8(%rdi), %cl
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB2_6
# BB#3:
	cmpb	$11, %cl
	jne	.LBB2_18
# BB#4:
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB2_5
.LBB2_7:
	movb	-8(%rdi), %cl
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB2_10
# BB#8:
	cmpb	$11, %cl
	jne	.LBB2_18
# BB#9:
	cvtsi2ssq	(%rdi), %xmm0
.LBB2_5:
	mulss	-16(%rdi), %xmm0
	movss	%xmm0, -16(%rdi)
	jmp	.LBB2_17
.LBB2_6:
	cvtsi2ssq	-16(%rdi), %xmm0
	mulss	(%rdi), %xmm0
.LBB2_15:
	movss	%xmm0, -16(%rdi)
	movw	$44, -8(%rdi)
	jmp	.LBB2_17
.LBB2_10:
	movq	-16(%rdi), %r8
	movq	(%rdi), %r9
	movq	%r8, %rcx
	negq	%rcx
	cmovlq	%r8, %rcx
	movq	%r9, %rsi
	negq	%rsi
	cmovlq	%r9, %rsi
	cmpq	$32767, %rcx            # imm = 0x7FFF
	setg	%al
	cmpq	$32767, %rsi            # imm = 0x7FFF
	setg	%dl
	testq	%r8, %r8
	je	.LBB2_16
# BB#11:
	orb	%dl, %al
	je	.LBB2_16
# BB#12:
	movl	$2147483647, %eax       # imm = 0x7FFFFFFF
	xorl	%edx, %edx
	idivq	%rcx
	cmpq	%rax, %rsi
	jle	.LBB2_16
# BB#13:
	cvtsi2ssq	%r8, %xmm1
	cvtsi2ssq	%r9, %xmm0
	mulss	%xmm1, %xmm0
	movq	%r9, %rax
	imulq	%r8, %rax
	cmpq	$-2147483648, %rax      # imm = 0x80000000
	jne	.LBB2_15
# BB#14:
	ucomiss	.LCPI2_0(%rip), %xmm0
	jne	.LBB2_15
	jp	.LBB2_15
.LBB2_16:
	imulq	%r8, %r9
	movq	%r9, -16(%rdi)
.LBB2_17:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB2_18:
	retq
.Lfunc_end2:
	.size	zmul, .Lfunc_end2-zmul
	.cfi_endproc

	.globl	zsub
	.p2align	4, 0x90
	.type	zsub,@function
zsub:                                   # @zsub
	.cfi_startproc
# BB#0:
	movb	8(%rdi), %cl
	shrb	$2, %cl
	movl	$-20, %eax
	cmpb	$5, %cl
	je	.LBB3_6
# BB#1:
	cmpb	$11, %cl
	jne	.LBB3_13
# BB#2:
	movb	-8(%rdi), %cl
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB3_5
# BB#3:
	cmpb	$11, %cl
	jne	.LBB3_13
# BB#4:
	movss	-16(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	subss	(%rdi), %xmm0
	movss	%xmm0, -16(%rdi)
	jmp	.LBB3_12
.LBB3_6:
	movb	-8(%rdi), %cl
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB3_9
# BB#7:
	cmpb	$11, %cl
	jne	.LBB3_13
# BB#8:
	cvtsi2ssq	(%rdi), %xmm0
	movss	-16(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, -16(%rdi)
	jmp	.LBB3_12
.LBB3_5:
	cvtsi2ssq	-16(%rdi), %xmm0
	subss	(%rdi), %xmm0
	jmp	.LBB3_11
.LBB3_9:
	movq	-16(%rdi), %rax
	movq	(%rdi), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	movq	%rdx, -16(%rdi)
	xorq	%rax, %rdx
	movq	%rcx, %rsi
	xorq	%rax, %rsi
	testq	%rsi, %rdx
	jns	.LBB3_12
# BB#10:
	cvtsi2ssq	%rax, %xmm0
	cvtsi2ssq	%rcx, %xmm1
	subss	%xmm1, %xmm0
.LBB3_11:
	movss	%xmm0, -16(%rdi)
	movw	$44, -8(%rdi)
.LBB3_12:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB3_13:
	retq
.Lfunc_end3:
	.size	zsub, .Lfunc_end3-zsub
	.cfi_endproc

	.globl	zidiv
	.p2align	4, 0x90
	.type	zidiv,@function
zidiv:                                  # @zidiv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 48
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	-16(%rbx), %r14
	movups	-16(%rbx), %xmm0
	movaps	%xmm0, (%rsp)
	movb	8(%rbx), %cl
	shrb	$2, %cl
	movl	$-20, %eax
	cmpb	$5, %cl
	je	.LBB4_7
# BB#1:
	cmpb	$11, %cl
	jne	.LBB4_16
# BB#2:
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movl	$-23, %eax
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB4_3
	jnp	.LBB4_16
.LBB4_3:
	movb	-8(%rbx), %al
	shrb	$2, %al
	cmpb	$5, %al
	je	.LBB4_6
# BB#4:
	cmpb	$11, %al
	movl	$-20, %eax
	je	.LBB4_5
	jmp	.LBB4_16
.LBB4_7:
	movq	(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB4_8
# BB#9:
	movb	-8(%rbx), %dl
	shrb	$2, %dl
	cmpb	$5, %dl
	je	.LBB4_12
# BB#10:
	cmpb	$11, %dl
	jne	.LBB4_16
# BB#11:
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rcx, %xmm0
.LBB4_5:
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movss	%xmm1, (%r14)
	jmp	.LBB4_14
.LBB4_8:
	movl	$-23, %eax
	jmp	.LBB4_16
.LBB4_12:
	xorps	%xmm0, %xmm0
	cvtsi2ssq	-16(%rbx), %xmm0
	cvtsi2ssq	%rcx, %xmm1
	divss	%xmm1, %xmm0
	movss	%xmm0, -16(%rbx)
	jmp	.LBB4_13
.LBB4_6:
	xorps	%xmm1, %xmm1
	cvtsi2ssq	-16(%rbx), %xmm1
	divss	%xmm0, %xmm1
	movss	%xmm1, -16(%rbx)
.LBB4_13:
	movw	$44, -8(%rbx)
.LBB4_14:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	zcvi
	testl	%eax, %eax
	jns	.LBB4_16
# BB#15:
	movaps	(%rsp), %xmm0
	movups	%xmm0, (%r14)
	movq	%rbx, osp(%rip)
.LBB4_16:                               # %zdiv.exit.thread
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	zidiv, .Lfunc_end4-zidiv
	.cfi_endproc

	.globl	zmod
	.p2align	4, 0x90
	.type	zmod,@function
zmod:                                   # @zmod
	.cfi_startproc
# BB#0:
	movzwl	-8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$20, %ecx
	jne	.LBB5_5
# BB#1:
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	cmpl	$20, %ecx
	jne	.LBB5_5
# BB#2:
	movq	(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB5_3
# BB#4:
	movq	-16(%rdi), %rax
	cqto
	idivq	%rcx
	movq	%rdx, -16(%rdi)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB5_5:
	retq
.LBB5_3:
	movl	$-23, %eax
	retq
.Lfunc_end5:
	.size	zmod, .Lfunc_end5-zmod
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	zneg
	.p2align	4, 0x90
	.type	zneg,@function
zneg:                                   # @zneg
	.cfi_startproc
# BB#0:
	movb	8(%rdi), %cl
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB6_3
# BB#1:
	movl	$-20, %eax
	cmpb	$11, %cl
	jne	.LBB6_7
# BB#2:
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	xorps	.LCPI6_0(%rip), %xmm0
	movss	%xmm0, (%rdi)
	jmp	.LBB6_6
.LBB6_3:
	movq	(%rdi), %rax
	cmpq	$-2147483648, %rax      # imm = 0x80000000
	jne	.LBB6_5
# BB#4:
	movl	$1325400064, (%rdi)     # imm = 0x4F000000
	movw	$44, 8(%rdi)
	jmp	.LBB6_6
.LBB6_5:
	negq	%rax
	movq	%rax, (%rdi)
.LBB6_6:
	xorl	%eax, %eax
.LBB6_7:
	retq
.Lfunc_end6:
	.size	zneg, .Lfunc_end6-zneg
	.cfi_endproc

	.globl	zceiling
	.p2align	4, 0x90
	.type	zceiling,@function
zceiling:                               # @zceiling
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	8(%rbx), %cl
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB7_3
# BB#1:
	movl	$-20, %eax
	cmpb	$11, %cl
	jne	.LBB7_4
# BB#2:
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	callq	ceilf
	movss	%xmm0, (%rbx)
.LBB7_3:
	xorl	%eax, %eax
.LBB7_4:
	popq	%rbx
	retq
.Lfunc_end7:
	.size	zceiling, .Lfunc_end7-zceiling
	.cfi_endproc

	.globl	zfloor
	.p2align	4, 0x90
	.type	zfloor,@function
zfloor:                                 # @zfloor
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
.Lcfi8:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	8(%rbx), %cl
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB8_3
# BB#1:
	movl	$-20, %eax
	cmpb	$11, %cl
	jne	.LBB8_4
# BB#2:
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	callq	floorf
	movss	%xmm0, (%rbx)
.LBB8_3:
	xorl	%eax, %eax
.LBB8_4:
	popq	%rbx
	retq
.Lfunc_end8:
	.size	zfloor, .Lfunc_end8-zfloor
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI9_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	zround
	.p2align	4, 0x90
	.type	zround,@function
zround:                                 # @zround
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	8(%rbx), %cl
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB9_3
# BB#1:
	movl	$-20, %eax
	cmpb	$11, %cl
	jne	.LBB9_4
# BB#2:
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	addsd	.LCPI9_0(%rip), %xmm0
	callq	floor
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rbx)
.LBB9_3:
	xorl	%eax, %eax
.LBB9_4:
	popq	%rbx
	retq
.Lfunc_end9:
	.size	zround, .Lfunc_end9-zround
	.cfi_endproc

	.globl	ztruncate
	.p2align	4, 0x90
	.type	ztruncate,@function
ztruncate:                              # @ztruncate
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
.Lcfi12:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	8(%rbx), %cl
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB10_6
# BB#1:
	movl	$-20, %eax
	cmpb	$11, %cl
	jne	.LBB10_7
# BB#2:
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jbe	.LBB10_4
# BB#3:
	callq	ceilf
	jmp	.LBB10_5
.LBB10_4:
	callq	floorf
.LBB10_5:
	movss	%xmm0, (%rbx)
.LBB10_6:
	xorl	%eax, %eax
.LBB10_7:
	popq	%rbx
	retq
.Lfunc_end10:
	.size	ztruncate, .Lfunc_end10-ztruncate
	.cfi_endproc

	.globl	zarith_op_init
	.p2align	4, 0x90
	.type	zarith_op_init,@function
zarith_op_init:                         # @zarith_op_init
	.cfi_startproc
# BB#0:
	movl	$zarith_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end11:
	.size	zarith_op_init, .Lfunc_end11-zarith_op_init
	.cfi_endproc

	.type	zarith_op_init.my_defs,@object # @zarith_op_init.my_defs
	.data
	.p2align	4
zarith_op_init.my_defs:
	.quad	.L.str
	.quad	zadd
	.quad	.L.str.1
	.quad	zceiling
	.quad	.L.str.2
	.quad	zdiv
	.quad	.L.str.3
	.quad	zidiv
	.quad	.L.str.4
	.quad	zfloor
	.quad	.L.str.5
	.quad	zmod
	.quad	.L.str.6
	.quad	zmul
	.quad	.L.str.7
	.quad	zneg
	.quad	.L.str.8
	.quad	zround
	.quad	.L.str.9
	.quad	zsub
	.quad	.L.str.10
	.quad	ztruncate
	.zero	16
	.size	zarith_op_init.my_defs, 192

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"2add"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"1ceiling"
	.size	.L.str.1, 9

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"2div"
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"2idiv"
	.size	.L.str.3, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"1floor"
	.size	.L.str.4, 7

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"2mod"
	.size	.L.str.5, 5

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"2mul"
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"1neg"
	.size	.L.str.7, 5

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"1round"
	.size	.L.str.8, 7

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"2sub"
	.size	.L.str.9, 5

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"1truncate"
	.size	.L.str.10, 10


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
