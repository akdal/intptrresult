	.text
	.file	"Solver.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	4607419450359352697     # double 1.0526315789473684
	.quad	4607186926907752514     # double 1.0010010010010011
.LCPI0_1:
	.quad	4609434218613702656     # double 1.5
	.quad	4599676419421066581     # double 0.33333333333333331
	.text
	.globl	_ZN6SolverC2Ev
	.p2align	4, 0x90
	.type	_ZN6SolverC2Ev,@function
_ZN6SolverC2Ev:                         # @_ZN6SolverC2Ev
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movaps	.LCPI0_0(%rip), %xmm1   # xmm1 = [1.052632e+00,1.001001e+00]
	movups	%xmm1, 32(%rdi)
	movabsq	$4581421828931458171, %rax # imm = 0x3F947AE147AE147B
	movq	%rax, 48(%rdi)
	movl	$100, 56(%rdi)
	movaps	.LCPI0_1(%rip), %xmm1   # xmm1 = [1.500000e+00,3.333333e-01]
	movups	%xmm1, 64(%rdi)
	movabsq	$4607632778762754458, %rax # imm = 0x3FF199999999999A
	movq	%rax, 80(%rdi)
	movb	$1, 88(%rdi)
	movq	$1, 92(%rdi)
	movups	%xmm0, 152(%rdi)
	movups	%xmm0, 136(%rdi)
	movups	%xmm0, 120(%rdi)
	movups	%xmm0, 104(%rdi)
	movq	$0, 168(%rdi)
	movb	$1, 176(%rdi)
	movups	%xmm0, 200(%rdi)
	movups	%xmm0, 184(%rdi)
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, 216(%rdi)
	leaq	224(%rdi), %rcx
	movups	%xmm0, 224(%rdi)
	movq	%rax, 240(%rdi)
	movups	%xmm0, 360(%rdi)
	movups	%xmm0, 344(%rdi)
	movups	%xmm0, 328(%rdi)
	movups	%xmm0, 312(%rdi)
	movups	%xmm0, 296(%rdi)
	movups	%xmm0, 280(%rdi)
	movups	%xmm0, 264(%rdi)
	movups	%xmm0, 248(%rdi)
	movl	$0, 376(%rdi)
	movl	$-1, 380(%rdi)
	movups	%xmm0, 384(%rdi)
	movq	$0, 400(%rdi)
	movq	%rcx, 408(%rdi)
	movups	%xmm0, 432(%rdi)
	movups	%xmm0, 416(%rdi)
	movabsq	$4725922819630694400, %rax # imm = 0x4195D9C3F4000000
	movq	%rax, 448(%rdi)
	movq	$0, 456(%rdi)
	movb	$1, 464(%rdi)
	movups	%xmm0, 520(%rdi)
	movups	%xmm0, 504(%rdi)
	movups	%xmm0, 488(%rdi)
	movups	%xmm0, 472(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN6SolverC2Ev, .Lfunc_end0-_ZN6SolverC2Ev
	.cfi_endproc

	.globl	_ZN6SolverD2Ev
	.p2align	4, 0x90
	.type	_ZN6SolverD2Ev,@function
_ZN6SolverD2Ev:                         # @_ZN6SolverD2Ev
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	cmpl	$0, 208(%r14)
	jle	.LBB1_3
# BB#1:                                 # %.lr.ph115
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movq	200(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	free
	incq	%rbx
	movslq	208(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB1_2
.LBB1_3:                                # %.preheader
	cmpl	$0, 192(%r14)
	jle	.LBB1_6
# BB#4:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_5:                                # =>This Inner Loop Header: Depth=1
	movq	184(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	free
	incq	%rbx
	movslq	192(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB1_5
.LBB1_6:                                # %._crit_edge
	movq	520(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_8
# BB#7:                                 # %.preheader.i.i31
	movl	$0, 528(%r14)
	callq	free
	movq	$0, 520(%r14)
	movl	$0, 532(%r14)
.LBB1_8:                                # %_ZN3vecI3LitED2Ev.exit32
	movq	504(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_10
# BB#9:                                 # %.preheader.i.i34
	movl	$0, 512(%r14)
	callq	free
	movq	$0, 504(%r14)
	movl	$0, 516(%r14)
.LBB1_10:                               # %_ZN3vecI3LitED2Ev.exit35
	movq	488(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_12
# BB#11:                                # %.preheader.i.i36
	movl	$0, 496(%r14)
	callq	free
	movq	$0, 488(%r14)
	movl	$0, 500(%r14)
.LBB1_12:                               # %_ZN3vecI3LitED2Ev.exit37
	movq	472(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_14
# BB#13:                                # %.preheader.i.i38
	movl	$0, 480(%r14)
	callq	free
	movq	$0, 472(%r14)
	movl	$0, 484(%r14)
.LBB1_14:                               # %_ZN3vecIcED2Ev.exit
	movq	432(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_16
# BB#15:                                # %.preheader.i.i.i
	movl	$0, 440(%r14)
	callq	free
	movq	$0, 432(%r14)
	movl	$0, 444(%r14)
.LBB1_16:                               # %_ZN3vecIiED2Ev.exit.i
	movq	416(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_18
# BB#17:                                # %.preheader.i.i2.i
	movl	$0, 424(%r14)
	callq	free
	movq	$0, 416(%r14)
	movl	$0, 428(%r14)
.LBB1_18:                               # %_ZN4HeapIN6Solver10VarOrderLtEED2Ev.exit
	movq	392(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_20
# BB#19:                                # %.preheader.i.i39
	movl	$0, 400(%r14)
	callq	free
	movq	$0, 392(%r14)
	movl	$0, 404(%r14)
.LBB1_20:                               # %_ZN3vecI3LitED2Ev.exit40
	movq	360(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_22
# BB#21:                                # %.preheader.i.i41
	movl	$0, 368(%r14)
	callq	free
	movq	$0, 360(%r14)
	movl	$0, 372(%r14)
.LBB1_22:                               # %_ZN3vecIiED2Ev.exit
	movq	344(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_24
# BB#23:                                # %.preheader.i.i42
	movl	$0, 352(%r14)
	callq	free
	movq	$0, 344(%r14)
	movl	$0, 356(%r14)
.LBB1_24:                               # %_ZN3vecIP6ClauseED2Ev.exit43
	movq	328(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_26
# BB#25:                                # %.preheader.i.i44
	movl	$0, 336(%r14)
	callq	free
	movq	$0, 328(%r14)
	movl	$0, 340(%r14)
.LBB1_26:                               # %_ZN3vecIiED2Ev.exit45
	movq	312(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_28
# BB#27:                                # %.preheader.i.i46
	movl	$0, 320(%r14)
	callq	free
	movq	$0, 312(%r14)
	movl	$0, 324(%r14)
.LBB1_28:                               # %_ZN3vecI3LitED2Ev.exit47
	movq	296(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_30
# BB#29:                                # %.preheader.i.i48
	movl	$0, 304(%r14)
	callq	free
	movq	$0, 296(%r14)
	movl	$0, 308(%r14)
.LBB1_30:                               # %_ZN3vecIcED2Ev.exit49
	movq	280(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_32
# BB#31:                                # %.preheader.i.i50
	movl	$0, 288(%r14)
	callq	free
	movq	$0, 280(%r14)
	movl	$0, 292(%r14)
.LBB1_32:                               # %_ZN3vecIcED2Ev.exit51
	movq	264(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_34
# BB#33:                                # %.preheader.i.i52
	movl	$0, 272(%r14)
	callq	free
	movq	$0, 264(%r14)
	movl	$0, 276(%r14)
.LBB1_34:                               # %_ZN3vecIcED2Ev.exit53
	movq	248(%r14), %r12
	testq	%r12, %r12
	je	.LBB1_42
# BB#35:                                # %.preheader.i.i54
	movl	256(%r14), %eax
	testl	%eax, %eax
	jle	.LBB1_41
# BB#36:                                # %.lr.ph.i.i.preheader
	movl	$1, %r15d
	xorl	%ebx, %ebx
	jmp	.LBB1_37
	.p2align	4, 0x90
.LBB1_53:                               # %_ZN3vecIP6ClauseED2Ev.exit..lr.ph_crit_edge.i.i
                                        #   in Loop: Header=BB1_37 Depth=1
	movq	248(%r14), %r12
	incq	%r15
	addq	$16, %rbx
.LBB1_37:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_39
# BB#38:                                # %.preheader.i.i.i.i
                                        #   in Loop: Header=BB1_37 Depth=1
	movl	$0, 8(%r12,%rbx)
	callq	free
	movq	$0, (%r12,%rbx)
	movl	$0, 12(%r12,%rbx)
	movl	256(%r14), %eax
.LBB1_39:                               # %_ZN3vecIP6ClauseED2Ev.exit.i.i
                                        #   in Loop: Header=BB1_37 Depth=1
	movslq	%eax, %rcx
	cmpq	%rcx, %r15
	jl	.LBB1_53
# BB#40:                                # %._crit_edge.i.loopexit.i
	movq	248(%r14), %r12
.LBB1_41:                               # %._crit_edge.i.i
	movl	$0, 256(%r14)
	movq	%r12, %rdi
	callq	free
	movq	$0, 248(%r14)
	movl	$0, 260(%r14)
.LBB1_42:                               # %_ZN3vecIS_IP6ClauseEED2Ev.exit
	movq	224(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_44
# BB#43:                                # %.preheader.i.i55
	movl	$0, 232(%r14)
	callq	free
	movq	$0, 224(%r14)
	movl	$0, 236(%r14)
.LBB1_44:                               # %_ZN3vecIdED2Ev.exit
	movq	200(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_46
# BB#45:                                # %.preheader.i.i56
	movl	$0, 208(%r14)
	callq	free
	movq	$0, 200(%r14)
	movl	$0, 212(%r14)
.LBB1_46:                               # %_ZN3vecIP6ClauseED2Ev.exit57
	movq	184(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_48
# BB#47:                                # %.preheader.i.i58
	movl	$0, 192(%r14)
	callq	free
	movq	$0, 184(%r14)
	movl	$0, 196(%r14)
.LBB1_48:                               # %_ZN3vecIP6ClauseED2Ev.exit59
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_50
# BB#49:                                # %.preheader.i.i60
	movl	$0, 24(%r14)
	callq	free
	movq	$0, 16(%r14)
	movl	$0, 28(%r14)
.LBB1_50:                               # %_ZN3vecI3LitED2Ev.exit61
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_52
# BB#51:                                # %.preheader.i.i62
	movl	$0, 8(%r14)
	callq	free
	movq	$0, (%r14)
	movl	$0, 12(%r14)
.LBB1_52:                               # %_ZN3vecI5lboolED2Ev.exit63
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_ZN6SolverD2Ev, .Lfunc_end1-_ZN6SolverD2Ev
	.cfi_endproc

	.globl	_ZN6Solver6newVarEbb
	.p2align	4, 0x90
	.type	_ZN6Solver6newVarEbb,@function
_ZN6Solver6newVarEbb:                   # @_ZN6Solver6newVarEbb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movslq	272(%rbx), %r14
	movl	256(%rbx), %ecx
	cmpl	260(%rbx), %ecx
	jne	.LBB2_1
# BB#2:
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 260(%rbx)
	movq	248(%rbx), %rdi
	movslq	%ecx, %rsi
	shlq	$4, %rsi
	callq	realloc
	movq	%rax, 248(%rbx)
	movl	256(%rbx), %ecx
	jmp	.LBB2_3
.LBB2_1:                                # %._crit_edge.i
	movq	248(%rbx), %rax
.LBB2_3:                                # %_ZN3vecIS_IP6ClauseEE4pushEv.exit
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax,%rcx)
	movl	256(%rbx), %ecx
	incl	%ecx
	movl	%ecx, 256(%rbx)
	cmpl	260(%rbx), %ecx
	jne	.LBB2_4
# BB#5:
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 260(%rbx)
	movq	248(%rbx), %rdi
	movslq	%ecx, %rsi
	shlq	$4, %rsi
	callq	realloc
	movq	%rax, 248(%rbx)
	movl	256(%rbx), %ecx
	jmp	.LBB2_6
.LBB2_4:                                # %._crit_edge.i8
	movq	248(%rbx), %rax
.LBB2_6:                                # %_ZN3vecIS_IP6ClauseEE4pushEv.exit10
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax,%rcx)
	incl	256(%rbx)
	movl	352(%rbx), %ecx
	cmpl	356(%rbx), %ecx
	jne	.LBB2_7
# BB#8:
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 356(%rbx)
	movq	344(%rbx), %rdi
	movslq	%ecx, %rsi
	shlq	$3, %rsi
	callq	realloc
	movq	%rax, 344(%rbx)
	movl	352(%rbx), %ecx
	jmp	.LBB2_9
.LBB2_7:                                # %._crit_edge.i13
	movq	344(%rbx), %rax
.LBB2_9:                                # %_ZN3vecIP6ClauseE4pushERKS1_.exit
	leal	1(%rcx), %edx
	movl	%edx, 352(%rbx)
	movslq	%ecx, %rcx
	movq	$0, (%rax,%rcx,8)
	movl	272(%rbx), %ecx
	cmpl	276(%rbx), %ecx
	jne	.LBB2_10
# BB#11:
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 276(%rbx)
	movq	264(%rbx), %rdi
	movslq	%ecx, %rsi
	callq	realloc
	movq	%rax, 264(%rbx)
	movl	272(%rbx), %ecx
	jmp	.LBB2_12
.LBB2_10:                               # %._crit_edge.i20
	movq	264(%rbx), %rax
.LBB2_12:                               # %_ZN3vecIcE4pushERKc.exit22
	leal	1(%rcx), %edx
	movl	%edx, 272(%rbx)
	movslq	%ecx, %rcx
	movb	$0, (%rax,%rcx)
	movl	368(%rbx), %ecx
	cmpl	372(%rbx), %ecx
	jne	.LBB2_13
# BB#14:
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 372(%rbx)
	movq	360(%rbx), %rdi
	movslq	%ecx, %rsi
	shlq	$2, %rsi
	callq	realloc
	movq	%rax, 360(%rbx)
	movl	368(%rbx), %ecx
	jmp	.LBB2_15
.LBB2_13:                               # %._crit_edge.i25
	movq	360(%rbx), %rax
.LBB2_15:                               # %_ZN3vecIiE4pushERKi.exit
	leal	1(%rcx), %edx
	movl	%edx, 368(%rbx)
	movslq	%ecx, %rcx
	movl	$-1, (%rax,%rcx,4)
	movl	232(%rbx), %ecx
	cmpl	236(%rbx), %ecx
	jne	.LBB2_16
# BB#17:
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 236(%rbx)
	movq	224(%rbx), %rdi
	movslq	%ecx, %rsi
	shlq	$3, %rsi
	callq	realloc
	movq	%rax, 224(%rbx)
	movl	232(%rbx), %ecx
	jmp	.LBB2_18
.LBB2_16:                               # %._crit_edge.i29
	movq	224(%rbx), %rax
.LBB2_18:                               # %_ZN3vecIdE4pushERKd.exit
	leal	1(%rcx), %edx
	movl	%edx, 232(%rbx)
	movslq	%ecx, %rcx
	movq	$0, (%rax,%rcx,8)
	movl	480(%rbx), %ecx
	cmpl	484(%rbx), %ecx
	jne	.LBB2_19
# BB#20:
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 484(%rbx)
	movq	472(%rbx), %rdi
	movslq	%ecx, %rsi
	callq	realloc
	movq	%rax, 472(%rbx)
	movl	480(%rbx), %ecx
	jmp	.LBB2_21
.LBB2_19:                               # %._crit_edge.i33
	movq	472(%rbx), %rax
.LBB2_21:                               # %_ZN3vecIcE4pushERKc.exit35
	leal	1(%rcx), %edx
	movl	%edx, 480(%rbx)
	movslq	%ecx, %rcx
	movb	$0, (%rax,%rcx)
	movl	288(%rbx), %ecx
	cmpl	292(%rbx), %ecx
	jne	.LBB2_22
# BB#23:
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 292(%rbx)
	movq	280(%rbx), %rdi
	movslq	%ecx, %rsi
	callq	realloc
	movq	%rax, 280(%rbx)
	movl	288(%rbx), %ecx
	jmp	.LBB2_24
.LBB2_22:                               # %._crit_edge.i38
	movq	280(%rbx), %rax
.LBB2_24:                               # %_ZN3vecIcE4pushERKc.exit40
	leal	1(%rcx), %edx
	movl	%edx, 288(%rbx)
	movslq	%ecx, %rcx
	movb	%bpl, (%rax,%rcx)
	movl	304(%rbx), %ecx
	cmpl	308(%rbx), %ecx
	jne	.LBB2_25
# BB#26:
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 308(%rbx)
	movq	296(%rbx), %rdi
	movslq	%ecx, %rsi
	callq	realloc
	movq	%rax, 296(%rbx)
	movl	304(%rbx), %ecx
	jmp	.LBB2_27
.LBB2_25:                               # %._crit_edge.i16
	movq	296(%rbx), %rax
.LBB2_27:                               # %_ZN3vecIcE4pushERKc.exit
	leal	1(%rcx), %edx
	movl	%edx, 304(%rbx)
	movslq	%ecx, %rcx
	movb	%r15b, (%rax,%rcx)
	cmpl	%r14d, 440(%rbx)
	jle	.LBB2_29
# BB#28:                                # %_ZNK4HeapIN6Solver10VarOrderLtEE6inHeapEi.exit.i
	movq	432(%rbx), %rax
	cmpl	$0, (%rax,%r14,4)
	jns	.LBB2_31
.LBB2_29:                               # %_ZNK4HeapIN6Solver10VarOrderLtEE6inHeapEi.exit.thread.i
	movq	296(%rbx), %rax
	cmpb	$0, (%rax,%r14)
	je	.LBB2_31
# BB#30:
	addq	$408, %rbx              # imm = 0x198
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	_ZN4HeapIN6Solver10VarOrderLtEE6insertEi
.LBB2_31:                               # %_ZN6Solver14insertVarOrderEi.exit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN6Solver6newVarEbb, .Lfunc_end2-_ZN6Solver6newVarEbb
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI3_1:
	.long	31                      # 0x1f
	.long	31                      # 0x1f
	.long	31                      # 0x1f
	.long	31                      # 0x1f
.LCPI3_2:
	.long	1065353216              # 0x3f800000
	.long	1065353216              # 0x3f800000
	.long	1065353216              # 0x3f800000
	.long	1065353216              # 0x3f800000
	.text
	.globl	_ZN6Solver9addClauseER3vecI3LitE
	.p2align	4, 0x90
	.type	_ZN6Solver9addClauseER3vecI3LitE,@function
_ZN6Solver9addClauseER3vecI3LitE:       # @_ZN6Solver9addClauseER3vecI3LitE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 64
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r15
	xorl	%ebp, %ebp
	cmpb	$0, 176(%r15)
	je	.LBB3_13
# BB#1:
	movq	(%r13), %rdi
	movl	8(%r13), %esi
	callq	_Z4sortI3Lit16LessThan_defaultIS0_EEvPT_iT0_
	movl	8(%r13), %r12d
	testl	%r12d, %r12d
	jle	.LBB3_14
# BB#2:                                 # %.lr.ph
	movq	(%r13), %rsi
	movq	264(%r15), %r8
	movl	$-2, %ebx
	xorl	%ecx, %ecx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rcx,4), %edi
	movl	%edi, %eax
	sarl	%eax
	cltq
	movzbl	(%r8,%rax), %edx
	testb	$1, %dil
	je	.LBB3_5
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	negb	%dl
.LBB3_5:                                #   in Loop: Header=BB3_3 Depth=1
	movl	%ebx, %eax
	xorl	$1, %eax
	cmpl	%eax, %edi
	movb	$1, %al
	je	.LBB3_37
# BB#6:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpb	$1, %dl
	je	.LBB3_37
# BB#7:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpl	%ebx, %edi
	je	.LBB3_11
# BB#8:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpb	$-1, %dl
	je	.LBB3_11
# BB#9:                                 #   in Loop: Header=BB3_3 Depth=1
	movslq	%ebp, %rax
	incl	%ebp
	movl	%edi, (%rsi,%rax,4)
	movl	8(%r13), %r12d
	jmp	.LBB3_12
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_3 Depth=1
	movl	%ebx, %edi
.LBB3_12:                               #   in Loop: Header=BB3_3 Depth=1
	incq	%rcx
	movslq	%r12d, %rax
	cmpq	%rax, %rcx
	movl	%edi, %ebx
	jl	.LBB3_3
	jmp	.LBB3_15
.LBB3_13:
	xorl	%eax, %eax
	jmp	.LBB3_37
.LBB3_14:
	xorl	%ecx, %ecx
.LBB3_15:                               # %.critedge37
	subl	%ebp, %ecx
	jle	.LBB3_17
# BB#16:                                # %.lr.ph.i
	subl	%ecx, %r12d
	movl	%r12d, 8(%r13)
.LBB3_17:                               # %_ZN3vecI3LitE6shrinkEi.exit
	cmpl	$1, %r12d
	je	.LBB3_20
# BB#18:                                # %_ZN3vecI3LitE6shrinkEi.exit
	testl	%r12d, %r12d
	jne	.LBB3_22
# BB#19:
	movb	$0, 176(%r15)
	xorl	%eax, %eax
	jmp	.LBB3_37
.LBB3_20:
	movq	(%r13), %rax
	movl	(%rax), %ebx
	movl	%ebx, %eax
	addb	%al, %al
	notb	%al
	andb	$2, %al
	decb	%al
	movl	%ebx, %ecx
	sarl	%ecx
	movq	264(%r15), %rdx
	movslq	%ecx, %rcx
	movb	%al, (%rdx,%rcx)
	movl	336(%r15), %eax
	movq	360(%r15), %rdx
	movl	%eax, (%rdx,%rcx,4)
	movq	344(%r15), %rax
	movq	$0, (%rax,%rcx,8)
	movl	320(%r15), %ecx
	cmpl	324(%r15), %ecx
	jne	.LBB3_29
# BB#21:
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 324(%r15)
	movq	312(%r15), %rdi
	movslq	%ecx, %rsi
	shlq	$2, %rsi
	callq	realloc
	movq	%rax, 312(%r15)
	movl	320(%r15), %ecx
	jmp	.LBB3_30
.LBB3_22:
	movslq	%r12d, %rbp
	leaq	8(,%rbp,4), %rdi
	callq	malloc
	movq	%rax, %r14
	leal	(,%r12,8), %ebx
	movl	%ebx, (%r14)
	testl	%r12d, %r12d
	jle	.LBB3_24
# BB#23:                                # %.lr.ph.i.i
	movq	(%r13), %rsi
	leaq	8(%r14), %rdi
	testq	%rbp, %rbp
	movl	$1, %edx
	cmovgq	%rbp, %rdx
	shlq	$2, %rdx
	callq	memcpy
.LBB3_24:                               # %._crit_edge.i.i
	xorl	%edx, %edx
	cmpl	$8, %ebx
	jb	.LBB3_33
# BB#25:                                # %.lr.ph.i.i.i
	andl	$536870911, %r12d       # imm = 0x1FFFFFFF
	cmpl	$1, %r12d
	movl	$1, %ecx
	cmovaq	%r12, %rcx
	cmpq	$8, %rcx
	jb	.LBB3_31
# BB#26:                                # %min.iters.checked
	movq	%rcx, %rax
	andq	$536870904, %rax        # imm = 0x1FFFFFF8
	je	.LBB3_31
# BB#27:                                # %vector.body.preheader
	leaq	-8(%rax), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	btl	$3, %esi
	jb	.LBB3_38
# BB#28:                                # %vector.body.prol
	movdqu	8(%r14), %xmm0
	movdqu	24(%r14), %xmm1
	psrld	$1, %xmm0
	movdqa	.LCPI3_0(%rip), %xmm2   # xmm2 = [1,1,1,1]
	psrld	$1, %xmm1
	movdqa	.LCPI3_1(%rip), %xmm3   # xmm3 = [31,31,31,31]
	pand	%xmm3, %xmm0
	pand	%xmm3, %xmm1
	pslld	$23, %xmm0
	movdqa	.LCPI3_2(%rip), %xmm3   # xmm3 = [1065353216,1065353216,1065353216,1065353216]
	paddd	%xmm3, %xmm0
	cvttps2dq	%xmm0, %xmm0
	pshufd	$245, %xmm0, %xmm4      # xmm4 = xmm0[1,1,3,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pmuludq	%xmm2, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	pslld	$23, %xmm1
	paddd	%xmm3, %xmm1
	cvttps2dq	%xmm1, %xmm1
	pshufd	$245, %xmm1, %xmm3      # xmm3 = xmm1[1,1,3,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pmuludq	%xmm2, %xmm3
	pshufd	$232, %xmm3, %xmm2      # xmm2 = xmm3[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movl	$8, %esi
	testq	%rdx, %rdx
	jne	.LBB3_39
	jmp	.LBB3_41
.LBB3_29:                               # %._crit_edge.i.i38
	movq	312(%r15), %rax
.LBB3_30:                               # %_ZN6Solver16uncheckedEnqueueE3LitP6Clause.exit
	leal	1(%rcx), %edx
	movl	%edx, 320(%r15)
	movslq	%ecx, %rcx
	movl	%ebx, (%rax,%rcx,4)
	movq	%r15, %rdi
	callq	_ZN6Solver9propagateEv
	testq	%rax, %rax
	sete	%al
	sete	176(%r15)
	jmp	.LBB3_37
.LBB3_31:
	xorl	%eax, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_32:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	8(%r14,%rax,4), %ecx
	shrb	%cl
	movl	$1, %esi
	shll	%cl, %esi
	orl	%esi, %edx
	incq	%rax
	cmpq	%r12, %rax
	jl	.LBB3_32
.LBB3_33:                               # %_Z10Clause_newI3vecI3LitEEP6ClauseRKT_b.exit
	movl	%edx, 4(%r14)
	movl	192(%r15), %ecx
	cmpl	196(%r15), %ecx
	jne	.LBB3_35
# BB#34:
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 196(%r15)
	movq	184(%r15), %rdi
	movslq	%ecx, %rsi
	shlq	$3, %rsi
	callq	realloc
	movq	%rax, 184(%r15)
	movl	192(%r15), %ecx
	jmp	.LBB3_36
.LBB3_35:                               # %._crit_edge.i
	movq	184(%r15), %rax
.LBB3_36:                               # %_ZN3vecIP6ClauseE4pushERKS1_.exit
	leal	1(%rcx), %edx
	movl	%edx, 192(%r15)
	movslq	%ecx, %rcx
	movq	%r14, (%rax,%rcx,8)
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	_ZN6Solver12attachClauseER6Clause
	movb	$1, %al
.LBB3_37:                               # %.critedge
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_38:
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	pxor	%xmm1, %xmm1
	testq	%rdx, %rdx
	je	.LBB3_41
.LBB3_39:                               # %vector.body.preheader.new
	movq	%rax, %rdx
	subq	%rsi, %rdx
	leaq	56(%r14,%rsi,4), %rsi
	movdqa	.LCPI3_0(%rip), %xmm10  # xmm10 = [1,1,1,1]
	movdqa	.LCPI3_1(%rip), %xmm8   # xmm8 = [31,31,31,31]
	movdqa	.LCPI3_2(%rip), %xmm9   # xmm9 = [1065353216,1065353216,1065353216,1065353216]
	.p2align	4, 0x90
.LBB3_40:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-48(%rsi), %xmm3
	movdqu	-32(%rsi), %xmm4
	movdqu	-16(%rsi), %xmm7
	movdqu	(%rsi), %xmm5
	psrld	$1, %xmm3
	psrld	$1, %xmm4
	pand	%xmm8, %xmm3
	pand	%xmm8, %xmm4
	pslld	$23, %xmm3
	paddd	%xmm9, %xmm3
	cvttps2dq	%xmm3, %xmm3
	pshufd	$245, %xmm3, %xmm2      # xmm2 = xmm3[1,1,3,3]
	pmuludq	%xmm10, %xmm3
	pshufd	$232, %xmm3, %xmm6      # xmm6 = xmm3[0,2,2,3]
	pmuludq	%xmm10, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0],xmm6[1],xmm2[1]
	pslld	$23, %xmm4
	paddd	%xmm9, %xmm4
	cvttps2dq	%xmm4, %xmm2
	pshufd	$245, %xmm2, %xmm4      # xmm4 = xmm2[1,1,3,3]
	pmuludq	%xmm10, %xmm2
	pshufd	$232, %xmm2, %xmm3      # xmm3 = xmm2[0,2,2,3]
	pmuludq	%xmm10, %xmm4
	pshufd	$232, %xmm4, %xmm2      # xmm2 = xmm4[0,2,2,3]
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	por	%xmm0, %xmm6
	por	%xmm1, %xmm3
	psrld	$1, %xmm7
	psrld	$1, %xmm5
	pand	%xmm8, %xmm7
	pand	%xmm8, %xmm5
	pslld	$23, %xmm7
	paddd	%xmm9, %xmm7
	cvttps2dq	%xmm7, %xmm0
	pshufd	$245, %xmm0, %xmm1      # xmm1 = xmm0[1,1,3,3]
	pmuludq	%xmm10, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pmuludq	%xmm10, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	pslld	$23, %xmm5
	paddd	%xmm9, %xmm5
	cvttps2dq	%xmm5, %xmm1
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm10, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pmuludq	%xmm10, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	por	%xmm6, %xmm0
	por	%xmm3, %xmm1
	addq	$64, %rsi
	addq	$-16, %rdx
	jne	.LBB3_40
.LBB3_41:                               # %middle.block
	por	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	por	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	por	%xmm1, %xmm0
	movd	%xmm0, %edx
	cmpq	%rax, %rcx
	jne	.LBB3_32
	jmp	.LBB3_33
.Lfunc_end3:
	.size	_ZN6Solver9addClauseER3vecI3LitE, .Lfunc_end3-_ZN6Solver9addClauseER3vecI3LitE
	.cfi_endproc

	.globl	_ZN6Solver16uncheckedEnqueueE3LitP6Clause
	.p2align	4, 0x90
	.type	_ZN6Solver16uncheckedEnqueueE3LitP6Clause,@function
_ZN6Solver16uncheckedEnqueueE3LitP6Clause: # @_ZN6Solver16uncheckedEnqueueE3LitP6Clause
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	%ebp, %eax
	addb	%al, %al
	notb	%al
	andb	$2, %al
	decb	%al
	movl	%ebp, %ecx
	sarl	%ecx
	movq	264(%rbx), %rsi
	movslq	%ecx, %rcx
	movb	%al, (%rsi,%rcx)
	movl	336(%rbx), %eax
	movq	360(%rbx), %rsi
	movl	%eax, (%rsi,%rcx,4)
	movq	344(%rbx), %rax
	movq	%rdx, (%rax,%rcx,8)
	movl	320(%rbx), %ecx
	cmpl	324(%rbx), %ecx
	jne	.LBB4_1
# BB#2:
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 324(%rbx)
	movq	312(%rbx), %rdi
	movslq	%ecx, %rsi
	shlq	$2, %rsi
	callq	realloc
	movq	%rax, 312(%rbx)
	movl	320(%rbx), %ecx
	jmp	.LBB4_3
.LBB4_1:                                # %._crit_edge.i
	movq	312(%rbx), %rax
.LBB4_3:                                # %_ZN3vecI3LitE4pushERKS0_.exit
	leal	1(%rcx), %edx
	movl	%edx, 320(%rbx)
	movslq	%ecx, %rcx
	movl	%ebp, (%rax,%rcx,4)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN6Solver16uncheckedEnqueueE3LitP6Clause, .Lfunc_end4-_ZN6Solver16uncheckedEnqueueE3LitP6Clause
	.cfi_endproc

	.globl	_ZN6Solver9propagateEv
	.p2align	4, 0x90
	.type	_ZN6Solver9propagateEv,@function
_ZN6Solver9propagateEv:                 # @_ZN6Solver9propagateEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 112
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	376(%r14), %eax
	xorl	%ecx, %ecx
	cmpl	320(%r14), %eax
	movl	$0, %ebx
	jge	.LBB5_7
# BB#1:                                 # %.lr.ph151
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_12 Depth 2
                                        #       Child Loop BB5_13 Depth 3
                                        #         Child Loop BB5_14 Depth 4
                                        #           Child Loop BB5_24 Depth 5
                                        #       Child Loop BB5_43 Depth 3
                                        #       Child Loop BB5_45 Depth 3
                                        #       Child Loop BB5_50 Depth 3
                                        #       Child Loop BB5_52 Depth 3
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leal	1(%rax), %ecx
	movl	%ecx, 376(%r14)
	movq	248(%r14), %rdx
	movq	312(%r14), %rcx
	cltq
	movslq	(%rcx,%rax,4), %r11
	movq	%r11, %rax
	shlq	$4, %rax
	movq	(%rdx,%rax), %r13
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movslq	8(%rdx,%rax), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#11:                                # %.lr.ph119.lr.ph.lr.ph
                                        #   in Loop: Header=BB5_2 Depth=1
	leaq	(%r13,%rax,8), %r10
	xorl	$1, %r11d
	leaq	-1(%r13,%rax,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r13, %r15
	movq	%r10, 16(%rsp)          # 8-byte Spill
	movq	%r11, 8(%rsp)           # 8-byte Spill
.LBB5_12:                               # %.lr.ph119.lr.ph
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_13 Depth 3
                                        #         Child Loop BB5_14 Depth 4
                                        #           Child Loop BB5_24 Depth 5
                                        #       Child Loop BB5_43 Depth 3
                                        #       Child Loop BB5_45 Depth 3
                                        #       Child Loop BB5_50 Depth 3
                                        #       Child Loop BB5_52 Depth 3
	movq	%rbx, (%rsp)            # 8-byte Spill
	jmp	.LBB5_13
.LBB5_21:                               #   in Loop: Header=BB5_13 Depth=3
	movq	%rbx, (%r15)
	addq	$8, %r15
	jmp	.LBB5_22
	.p2align	4, 0x90
.LBB5_13:                               # %.lr.ph119
                                        #   Parent Loop BB5_2 Depth=1
                                        #     Parent Loop BB5_12 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB5_14 Depth 4
                                        #           Child Loop BB5_24 Depth 5
	movq	%r13, %r9
.LBB5_14:                               #   Parent Loop BB5_2 Depth=1
                                        #     Parent Loop BB5_12 Depth=2
                                        #       Parent Loop BB5_13 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB5_24 Depth 5
	movq	(%r9), %rbx
	movl	8(%rbx), %r12d
	cmpl	%r11d, %r12d
	jne	.LBB5_16
# BB#15:                                #   in Loop: Header=BB5_14 Depth=4
	movl	12(%rbx), %r12d
	movl	%r12d, 8(%rbx)
	movl	%r11d, 12(%rbx)
.LBB5_16:                               #   in Loop: Header=BB5_14 Depth=4
	movl	%r12d, %eax
	sarl	%eax
	movq	264(%r14), %rsi
	movslq	%eax, %r8
	movb	(%rsi,%r8), %al
	movl	%r12d, %edx
	andl	$1, %edx
	je	.LBB5_18
# BB#17:                                #   in Loop: Header=BB5_14 Depth=4
	negb	%al
.LBB5_18:                               #   in Loop: Header=BB5_14 Depth=4
	leaq	8(%r9), %r13
	cmpb	$1, %al
	je	.LBB5_21
# BB#19:                                # %.preheader
                                        #   in Loop: Header=BB5_14 Depth=4
	movl	(%rbx), %edi
	cmpl	$24, %edi
	jb	.LBB5_32
# BB#20:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB5_14 Depth=4
	shrl	$3, %edi
	movl	$2, %eax
	.p2align	4, 0x90
.LBB5_24:                               # %.lr.ph
                                        #   Parent Loop BB5_2 Depth=1
                                        #     Parent Loop BB5_12 Depth=2
                                        #       Parent Loop BB5_13 Depth=3
                                        #         Parent Loop BB5_14 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	8(%rbx,%rax,4), %ebp
	movl	%ebp, %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	movzbl	(%rsi,%rcx), %ecx
	testb	$1, %bpl
	je	.LBB5_26
# BB#25:                                #   in Loop: Header=BB5_24 Depth=5
	negb	%cl
.LBB5_26:                               # %.lr.ph
                                        #   in Loop: Header=BB5_24 Depth=5
	cmpb	$-1, %cl
	jne	.LBB5_27
# BB#23:                                #   in Loop: Header=BB5_24 Depth=5
	incq	%rax
	cmpq	%rdi, %rax
	jl	.LBB5_24
	jmp	.LBB5_32
	.p2align	4, 0x90
.LBB5_27:                               # %.critedge
                                        #   in Loop: Header=BB5_14 Depth=4
	movq	%r15, %r12
	movl	%ebp, 12(%rbx)
	movl	%r11d, 8(%rbx,%rax,4)
	movslq	12(%rbx), %rax
	movq	248(%r14), %rdx
	xorq	$1, %rax
	shlq	$4, %rax
	leaq	(%rdx,%rax), %rbp
	leaq	8(%rdx,%rax), %r15
	movl	8(%rdx,%rax), %ecx
	cmpl	12(%rdx,%rax), %ecx
	jne	.LBB5_28
# BB#29:                                #   in Loop: Header=BB5_14 Depth=4
	leaq	12(%rdx,%rax), %rax
	leal	1(%rcx,%rcx,2), %ecx
	sarl	%ecx
	leal	-2(%rcx), %edx
	sarl	$31, %edx
	movl	%edx, %esi
	andl	$2, %esi
	notl	%edx
	andl	%ecx, %edx
	addl	%esi, %edx
	movl	%edx, (%rax)
	movq	(%rbp), %rdi
	movslq	%edx, %rsi
	shlq	$3, %rsi
	callq	realloc
	movq	8(%rsp), %r11           # 8-byte Reload
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	%rax, (%rbp)
	movl	(%r15), %ecx
	jmp	.LBB5_30
	.p2align	4, 0x90
.LBB5_28:                               # %._crit_edge.i
                                        #   in Loop: Header=BB5_14 Depth=4
	movq	(%rbp), %rax
.LBB5_30:                               # %_ZN3vecIP6ClauseE4pushERKS1_.exit
                                        #   in Loop: Header=BB5_14 Depth=4
	leal	1(%rcx), %edx
	movl	%edx, (%r15)
	movslq	%ecx, %rcx
	movq	%rbx, (%rax,%rcx,8)
	cmpq	%r10, %r13
	movq	%r13, %r9
	movq	%r12, %r15
	jne	.LBB5_14
	jmp	.LBB5_31
	.p2align	4, 0x90
.LBB5_32:                               # %.preheader._crit_edge
                                        #   in Loop: Header=BB5_13 Depth=3
	movq	%rbx, (%r15)
	movq	264(%r14), %rax
	movb	(%rax,%r8), %cl
	testl	%edx, %edx
	je	.LBB5_34
# BB#33:                                #   in Loop: Header=BB5_13 Depth=3
	negb	%cl
.LBB5_34:                               # %.preheader._crit_edge
                                        #   in Loop: Header=BB5_13 Depth=3
	movq	%r15, %rdx
	leaq	8(%rdx), %r15
	cmpb	$-1, %cl
	je	.LBB5_35
# BB#53:                                #   in Loop: Header=BB5_13 Depth=3
	movl	%r12d, %ecx
	addb	%cl, %cl
	notb	%cl
	andb	$2, %cl
	decb	%cl
	movb	%cl, (%rax,%r8)
	movl	336(%r14), %eax
	movq	360(%r14), %rcx
	movl	%eax, (%rcx,%r8,4)
	movq	344(%r14), %rax
	movq	%rbx, (%rax,%r8,8)
	movl	320(%r14), %ecx
	cmpl	324(%r14), %ecx
	jne	.LBB5_54
# BB#55:                                #   in Loop: Header=BB5_13 Depth=3
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 324(%r14)
	movq	312(%r14), %rdi
	movslq	%ecx, %rsi
	shlq	$2, %rsi
	callq	realloc
	movq	8(%rsp), %r11           # 8-byte Reload
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	%rax, 312(%r14)
	movl	320(%r14), %ecx
	jmp	.LBB5_56
	.p2align	4, 0x90
.LBB5_54:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB5_13 Depth=3
	movq	312(%r14), %rax
.LBB5_56:                               # %_ZN6Solver16uncheckedEnqueueE3LitP6Clause.exit
                                        #   in Loop: Header=BB5_13 Depth=3
	leal	1(%rcx), %edx
	movl	%edx, 320(%r14)
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx,4)
.LBB5_22:                               # %.outer.backedge
                                        #   in Loop: Header=BB5_13 Depth=3
	cmpq	%r10, %r13
	jne	.LBB5_13
	jmp	.LBB5_31
	.p2align	4, 0x90
.LBB5_35:                               #   in Loop: Header=BB5_12 Depth=2
	movl	320(%r14), %eax
	movl	%eax, 376(%r14)
	cmpq	%r10, %r13
	jae	.LBB5_9
# BB#36:                                # %.lr.ph139.preheader
                                        #   in Loop: Header=BB5_12 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	subq	%r13, %rax
	movq	%rax, %rcx
	shrq	$3, %rcx
	leaq	16(%rdx), %r12
	leaq	1(%rcx), %rsi
	cmpq	$4, %rsi
	jb	.LBB5_48
# BB#37:                                # %min.iters.checked
                                        #   in Loop: Header=BB5_12 Depth=2
	movq	%rsi, %rdi
	movabsq	$4611686018427387900, %rbp # imm = 0x3FFFFFFFFFFFFFFC
	andq	%rbp, %rdi
	je	.LBB5_48
# BB#38:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_12 Depth=2
	andq	$-8, %rax
	leaq	16(%r9,%rax), %rbp
	cmpq	%rbp, %r15
	jae	.LBB5_40
# BB#39:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_12 Depth=2
	leaq	16(%rdx,%rax), %rax
	cmpq	%rax, %r13
	jb	.LBB5_48
.LBB5_40:                               # %vector.body.preheader
                                        #   in Loop: Header=BB5_12 Depth=2
	leaq	-4(%rdi), %r8
	movl	%r8d, %eax
	shrl	$2, %eax
	incl	%eax
	andq	$3, %rax
	je	.LBB5_41
# BB#42:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB5_12 Depth=2
	negq	%rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_43:                               # %vector.body.prol
                                        #   Parent Loop BB5_2 Depth=1
                                        #     Parent Loop BB5_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	8(%r9,%rbp,8), %xmm0
	movups	24(%r9,%rbp,8), %xmm1
	movups	%xmm0, 8(%rdx,%rbp,8)
	movups	%xmm1, 24(%rdx,%rbp,8)
	addq	$4, %rbp
	incq	%rax
	jne	.LBB5_43
	jmp	.LBB5_44
.LBB5_41:                               #   in Loop: Header=BB5_12 Depth=2
	xorl	%ebp, %ebp
.LBB5_44:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB5_12 Depth=2
	cmpq	$12, %r8
	jb	.LBB5_46
	.p2align	4, 0x90
.LBB5_45:                               # %vector.body
                                        #   Parent Loop BB5_2 Depth=1
                                        #     Parent Loop BB5_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	8(%r9,%rbp,8), %xmm0
	movups	24(%r9,%rbp,8), %xmm1
	movups	%xmm0, 8(%rdx,%rbp,8)
	movups	%xmm1, 24(%rdx,%rbp,8)
	movups	40(%r9,%rbp,8), %xmm0
	movups	56(%r9,%rbp,8), %xmm1
	movups	%xmm0, 40(%rdx,%rbp,8)
	movups	%xmm1, 56(%rdx,%rbp,8)
	movups	72(%r9,%rbp,8), %xmm0
	movups	88(%r9,%rbp,8), %xmm1
	movups	%xmm0, 72(%rdx,%rbp,8)
	movups	%xmm1, 88(%rdx,%rbp,8)
	movups	104(%r9,%rbp,8), %xmm0
	movups	120(%r9,%rbp,8), %xmm1
	movups	%xmm0, 104(%rdx,%rbp,8)
	movups	%xmm1, 120(%rdx,%rbp,8)
	addq	$16, %rbp
	cmpq	%rbp, %rdi
	jne	.LBB5_45
.LBB5_46:                               # %middle.block
                                        #   in Loop: Header=BB5_12 Depth=2
	cmpq	%rdi, %rsi
	je	.LBB5_8
# BB#47:                                #   in Loop: Header=BB5_12 Depth=2
	leaq	(%r13,%rdi,8), %r13
	leaq	(%r15,%rdi,8), %r15
.LBB5_48:                               # %.lr.ph139.preheader290
                                        #   in Loop: Header=BB5_12 Depth=2
	leaq	8(%r13), %rsi
	cmpq	%rsi, %r10
	cmovaq	%r10, %rsi
	subq	%r13, %rsi
	decq	%rsi
	movl	%esi, %eax
	shrl	$3, %eax
	incl	%eax
	andq	$7, %rax
	je	.LBB5_51
# BB#49:                                # %.lr.ph139.prol.preheader
                                        #   in Loop: Header=BB5_12 Depth=2
	negq	%rax
	.p2align	4, 0x90
.LBB5_50:                               # %.lr.ph139.prol
                                        #   Parent Loop BB5_2 Depth=1
                                        #     Parent Loop BB5_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13), %rdi
	addq	$8, %r13
	movq	%rdi, (%r15)
	addq	$8, %r15
	incq	%rax
	jne	.LBB5_50
.LBB5_51:                               # %.lr.ph139.prol.loopexit
                                        #   in Loop: Header=BB5_12 Depth=2
	cmpq	$56, %rsi
	jb	.LBB5_8
	.p2align	4, 0x90
.LBB5_52:                               # %.lr.ph139
                                        #   Parent Loop BB5_2 Depth=1
                                        #     Parent Loop BB5_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13), %rax
	movq	%rax, (%r15)
	movq	8(%r13), %rax
	movq	%rax, 8(%r15)
	movq	16(%r13), %rax
	movq	%rax, 16(%r15)
	movq	24(%r13), %rax
	movq	%rax, 24(%r15)
	movq	32(%r13), %rax
	movq	%rax, 32(%r15)
	movq	40(%r13), %rax
	movq	%rax, 40(%r15)
	movq	48(%r13), %rax
	movq	%rax, 48(%r15)
	movq	56(%r13), %rax
	movq	%rax, 56(%r15)
	addq	$64, %r13
	addq	$64, %r15
	cmpq	%r10, %r13
	jb	.LBB5_52
.LBB5_8:                                # %.loopexit.loopexit
                                        #   in Loop: Header=BB5_12 Depth=2
	leaq	16(%r9,%rcx,8), %r13
	leaq	(%r12,%rcx,8), %r15
.LBB5_9:                                # %.loopexit
                                        #   in Loop: Header=BB5_12 Depth=2
	cmpq	%r10, %r13
	jne	.LBB5_12
# BB#10:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%r15, %r13
	jmp	.LBB5_4
	.p2align	4, 0x90
.LBB5_31:                               #   in Loop: Header=BB5_2 Depth=1
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%r15, %r13
	jmp	.LBB5_4
	.p2align	4, 0x90
.LBB5_3:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%r13, %r10
.LBB5_4:                                # %.outer._crit_edge
                                        #   in Loop: Header=BB5_2 Depth=1
	subq	%r13, %r10
	shrq	$3, %r10
	testl	%r10d, %r10d
	jle	.LBB5_5
# BB#57:                                # %.lr.ph.i
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	8(%rax,%rcx), %rax
	subl	%r10d, (%rax)
.LBB5_5:                                # %_ZN3vecIP6ClauseE6shrinkEi.exit.backedge
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	incl	%ecx
	movl	376(%r14), %eax
	cmpl	320(%r14), %eax
	jl	.LBB5_2
# BB#6:                                 # %_ZN3vecIP6ClauseE6shrinkEi.exit._crit_edge.loopexit
	movslq	%ecx, %rcx
.LBB5_7:                                # %_ZN3vecIP6ClauseE6shrinkEi.exit._crit_edge
	addq	%rcx, 128(%r14)
	subq	%rcx, 384(%r14)
	movq	%rbx, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN6Solver9propagateEv, .Lfunc_end5-_ZN6Solver9propagateEv
	.cfi_endproc

	.globl	_ZN6Solver12attachClauseER6Clause
	.p2align	4, 0x90
	.type	_ZN6Solver12attachClauseER6Clause,@function
_ZN6Solver12attachClauseER6Clause:      # @_ZN6Solver12attachClauseER6Clause
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi53:
	.cfi_def_cfa_offset 48
.Lcfi54:
	.cfi_offset %rbx, -40
.Lcfi55:
	.cfi_offset %r12, -32
.Lcfi56:
	.cfi_offset %r14, -24
.Lcfi57:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movslq	8(%r14), %rax
	movq	248(%rbx), %rdx
	xorq	$1, %rax
	shlq	$4, %rax
	leaq	(%rdx,%rax), %r12
	leaq	8(%rdx,%rax), %r15
	movl	8(%rdx,%rax), %ecx
	cmpl	12(%rdx,%rax), %ecx
	jne	.LBB6_1
# BB#2:
	leaq	12(%rdx,%rax), %rax
	leal	1(%rcx,%rcx,2), %ecx
	sarl	%ecx
	leal	-2(%rcx), %edx
	sarl	$31, %edx
	movl	%edx, %esi
	andl	$2, %esi
	notl	%edx
	andl	%ecx, %edx
	addl	%esi, %edx
	movl	%edx, (%rax)
	movq	(%r12), %rdi
	movslq	%edx, %rsi
	shlq	$3, %rsi
	callq	realloc
	movq	%rax, (%r12)
	movl	(%r15), %ecx
	jmp	.LBB6_3
.LBB6_1:                                # %._crit_edge.i14
	movq	(%r12), %rax
.LBB6_3:                                # %_ZN3vecIP6ClauseE4pushERKS1_.exit16
	leal	1(%rcx), %edx
	movl	%edx, (%r15)
	movslq	%ecx, %rcx
	movq	%r14, (%rax,%rcx,8)
	movslq	12(%r14), %rax
	movq	248(%rbx), %rdx
	xorq	$1, %rax
	shlq	$4, %rax
	leaq	(%rdx,%rax), %r12
	leaq	8(%rdx,%rax), %r15
	movl	8(%rdx,%rax), %ecx
	cmpl	12(%rdx,%rax), %ecx
	jne	.LBB6_4
# BB#5:
	leaq	12(%rdx,%rax), %rax
	leal	1(%rcx,%rcx,2), %ecx
	sarl	%ecx
	leal	-2(%rcx), %edx
	sarl	$31, %edx
	movl	%edx, %esi
	andl	$2, %esi
	notl	%edx
	andl	%ecx, %edx
	addl	%esi, %edx
	movl	%edx, (%rax)
	movq	(%r12), %rdi
	movslq	%edx, %rsi
	shlq	$3, %rsi
	callq	realloc
	movq	%rax, (%r12)
	movl	(%r15), %ecx
	jmp	.LBB6_6
.LBB6_4:                                # %._crit_edge.i
	movq	(%r12), %rax
.LBB6_6:                                # %_ZN3vecIP6ClauseE4pushERKS1_.exit
	leal	1(%rcx), %edx
	movl	%edx, (%r15)
	movslq	%ecx, %rcx
	movq	%r14, (%rax,%rcx,8)
	movl	(%r14), %eax
	movl	%eax, %ecx
	shrl	$3, %ecx
	leaq	144(%rbx), %rdx
	addq	$152, %rbx
	testb	$1, %al
	cmoveq	%rdx, %rbx
	addq	%rcx, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	_ZN6Solver12attachClauseER6Clause, .Lfunc_end6-_ZN6Solver12attachClauseER6Clause
	.cfi_endproc

	.globl	_ZN6Solver12detachClauseER6Clause
	.p2align	4, 0x90
	.type	_ZN6Solver12detachClauseER6Clause,@function
_ZN6Solver12detachClauseER6Clause:      # @_ZN6Solver12detachClauseER6Clause
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 24
.Lcfi60:
	.cfi_offset %rbx, -24
.Lcfi61:
	.cfi_offset %r14, -16
	movslq	8(%rsi), %rdx
	movq	248(%rdi), %r9
	xorq	$1, %rdx
	shlq	$4, %rdx
	leaq	(%r9,%rdx), %r11
	movslq	8(%r9,%rdx), %r10
	testq	%r10, %r10
	jle	.LBB7_1
# BB#2:                                 # %.lr.ph4.i13
	movq	(%r11), %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_3:                                # =>This Inner Loop Header: Depth=1
	cmpq	%rsi, (%rcx,%rax,8)
	je	.LBB7_4
# BB#22:                                #   in Loop: Header=BB7_3 Depth=1
	incq	%rax
	cmpq	%r10, %rax
	jl	.LBB7_3
	jmp	.LBB7_4
.LBB7_1:
	xorl	%eax, %eax
.LBB7_4:                                # %.critedge.preheader.i17
	leaq	8(%r9,%rdx), %r8
	decl	%r10d
	cmpl	%r10d, %eax
	jge	.LBB7_11
# BB#5:                                 # %.lr.ph.i18
	movslq	%eax, %rdx
	movslq	%r10d, %r9
	movl	%r10d, %ecx
	subl	%eax, %ecx
	leaq	-1(%r9), %r14
	subq	%rdx, %r14
	andq	$3, %rcx
	je	.LBB7_8
# BB#6:                                 # %.critedge.i22.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB7_7:                                # %.critedge.i22.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r11), %rbx
	movq	8(%rbx,%rdx,8), %rax
	movq	%rax, (%rbx,%rdx,8)
	incq	%rdx
	incq	%rcx
	jne	.LBB7_7
.LBB7_8:                                # %.critedge.i22.prol.loopexit
	cmpq	$3, %r14
	jb	.LBB7_10
	.p2align	4, 0x90
.LBB7_9:                                # %.critedge.i22
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r11), %rax
	movq	8(%rax,%rdx,8), %rcx
	movq	%rcx, (%rax,%rdx,8)
	movq	(%r11), %rax
	movq	16(%rax,%rdx,8), %rcx
	movq	%rcx, 8(%rax,%rdx,8)
	movq	(%r11), %rax
	movq	24(%rax,%rdx,8), %rcx
	movq	%rcx, 16(%rax,%rdx,8)
	movq	(%r11), %rax
	movq	32(%rax,%rdx,8), %rcx
	movq	%rcx, 24(%rax,%rdx,8)
	leaq	4(%rdx), %rdx
	cmpq	%rdx, %r9
	jne	.LBB7_9
.LBB7_10:                               # %_ZL6removeI3vecIP6ClauseES2_EvRT_RKT0_.exit23.loopexit
	movq	248(%rdi), %r9
.LBB7_11:                               # %_ZL6removeI3vecIP6ClauseES2_EvRT_RKT0_.exit23
	movl	%r10d, (%r8)
	movslq	12(%rsi), %rdx
	xorq	$1, %rdx
	shlq	$4, %rdx
	leaq	(%r9,%rdx), %r11
	movslq	8(%r9,%rdx), %r10
	testq	%r10, %r10
	jle	.LBB7_12
# BB#13:                                # %.lr.ph4.i
	movq	(%r11), %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_14:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rsi, (%rcx,%rax,8)
	je	.LBB7_15
# BB#23:                                #   in Loop: Header=BB7_14 Depth=1
	incq	%rax
	cmpq	%r10, %rax
	jl	.LBB7_14
	jmp	.LBB7_15
.LBB7_12:
	xorl	%eax, %eax
.LBB7_15:                               # %.critedge.preheader.i
	leaq	8(%r9,%rdx), %r8
	decl	%r10d
	cmpl	%r10d, %eax
	jge	.LBB7_21
# BB#16:                                # %.lr.ph.i
	movslq	%eax, %rdx
	movslq	%r10d, %r9
	movl	%r10d, %ecx
	subl	%eax, %ecx
	leaq	-1(%r9), %r14
	subq	%rdx, %r14
	andq	$3, %rcx
	je	.LBB7_19
# BB#17:                                # %.critedge.i.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB7_18:                               # %.critedge.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r11), %rbx
	movq	8(%rbx,%rdx,8), %rax
	movq	%rax, (%rbx,%rdx,8)
	incq	%rdx
	incq	%rcx
	jne	.LBB7_18
.LBB7_19:                               # %.critedge.i.prol.loopexit
	cmpq	$3, %r14
	jb	.LBB7_21
	.p2align	4, 0x90
.LBB7_20:                               # %.critedge.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r11), %rax
	movq	8(%rax,%rdx,8), %rcx
	movq	%rcx, (%rax,%rdx,8)
	movq	(%r11), %rax
	movq	16(%rax,%rdx,8), %rcx
	movq	%rcx, 8(%rax,%rdx,8)
	movq	(%r11), %rax
	movq	24(%rax,%rdx,8), %rcx
	movq	%rcx, 16(%rax,%rdx,8)
	movq	(%r11), %rax
	movq	32(%rax,%rdx,8), %rcx
	movq	%rcx, 24(%rax,%rdx,8)
	leaq	4(%rdx), %rdx
	cmpq	%rdx, %r9
	jne	.LBB7_20
.LBB7_21:                               # %_ZL6removeI3vecIP6ClauseES2_EvRT_RKT0_.exit
	movl	%r10d, (%r8)
	movl	(%rsi), %eax
	movl	%eax, %ecx
	shrl	$3, %ecx
	leaq	144(%rdi), %rdx
	addq	$152, %rdi
	testb	$1, %al
	cmoveq	%rdx, %rdi
	subq	%rcx, (%rdi)
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	_ZN6Solver12detachClauseER6Clause, .Lfunc_end7-_ZN6Solver12detachClauseER6Clause
	.cfi_endproc

	.globl	_ZN6Solver12removeClauseER6Clause
	.p2align	4, 0x90
	.type	_ZN6Solver12removeClauseER6Clause,@function
_ZN6Solver12removeClauseER6Clause:      # @_ZN6Solver12removeClauseER6Clause
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 16
.Lcfi63:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	callq	_ZN6Solver12detachClauseER6Clause
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end8:
	.size	_ZN6Solver12removeClauseER6Clause, .Lfunc_end8-_ZN6Solver12removeClauseER6Clause
	.cfi_endproc

	.globl	_ZNK6Solver9satisfiedERK6Clause
	.p2align	4, 0x90
	.type	_ZNK6Solver9satisfiedERK6Clause,@function
_ZNK6Solver9satisfiedERK6Clause:        # @_ZNK6Solver9satisfiedERK6Clause
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	cmpl	$8, %eax
	jb	.LBB9_1
# BB#4:                                 # %.lr.ph
	movq	264(%rdi), %r8
	shrl	$3, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_5:                                # =>This Inner Loop Header: Depth=1
	movl	8(%rsi,%rdx,4), %edi
	movl	%edi, %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	movzbl	(%r8,%rcx), %ecx
	testb	$1, %dil
	je	.LBB9_7
# BB#6:                                 #   in Loop: Header=BB9_5 Depth=1
	negb	%cl
.LBB9_7:                                #   in Loop: Header=BB9_5 Depth=1
	cmpb	$1, %cl
	je	.LBB9_8
# BB#2:                                 #   in Loop: Header=BB9_5 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB9_5
# BB#3:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB9_1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB9_8:
	movb	$1, %al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end9:
	.size	_ZNK6Solver9satisfiedERK6Clause, .Lfunc_end9-_ZNK6Solver9satisfiedERK6Clause
	.cfi_endproc

	.globl	_ZN6Solver11cancelUntilEi
	.p2align	4, 0x90
	.type	_ZN6Solver11cancelUntilEi,@function
_ZN6Solver11cancelUntilEi:              # @_ZN6Solver11cancelUntilEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 48
.Lcfi69:
	.cfi_offset %rbx, -48
.Lcfi70:
	.cfi_offset %r12, -40
.Lcfi71:
	.cfi_offset %r14, -32
.Lcfi72:
	.cfi_offset %r15, -24
.Lcfi73:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	cmpl	%r14d, 336(%rbx)
	jle	.LBB10_11
# BB#1:
	movl	320(%rbx), %edx
	movq	328(%rbx), %rax
	movslq	%r14d, %r12
	movl	(%rax,%r12,4), %ecx
	cmpl	%ecx, %edx
	jle	.LBB10_7
# BB#2:                                 # %.lr.ph
	movslq	%edx, %rbp
	leaq	408(%rbx), %r15
	.p2align	4, 0x90
.LBB10_3:                               # =>This Inner Loop Header: Depth=1
	movq	264(%rbx), %rcx
	movq	312(%rbx), %rax
	movl	-4(%rax,%rbp,4), %esi
	decq	%rbp
	sarl	%esi
	movslq	%esi, %rax
	movb	$0, (%rcx,%rax)
	cmpl	%eax, 440(%rbx)
	jle	.LBB10_12
# BB#4:                                 # %_ZNK4HeapIN6Solver10VarOrderLtEE6inHeapEi.exit.i
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	432(%rbx), %rcx
	cmpl	$0, (%rcx,%rax,4)
	jns	.LBB10_5
.LBB10_12:                              # %_ZNK4HeapIN6Solver10VarOrderLtEE6inHeapEi.exit.thread.i
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	296(%rbx), %rcx
	cmpb	$0, (%rcx,%rax)
	je	.LBB10_5
# BB#13:                                #   in Loop: Header=BB10_3 Depth=1
	movq	%r15, %rdi
	callq	_ZN4HeapIN6Solver10VarOrderLtEE6insertEi
.LBB10_5:                               # %_ZN6Solver14insertVarOrderEi.exit.backedge
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	328(%rbx), %rax
	movslq	(%rax,%r12,4), %rcx
	cmpq	%rcx, %rbp
	jg	.LBB10_3
# BB#6:                                 # %_ZN6Solver14insertVarOrderEi.exit._crit_edge.loopexit
	movl	320(%rbx), %edx
.LBB10_7:                               # %_ZN6Solver14insertVarOrderEi.exit._crit_edge
	movl	%ecx, 376(%rbx)
	movl	(%rax,%r12,4), %eax
	cmpl	%eax, %edx
	jle	.LBB10_9
# BB#8:                                 # %.lr.ph.i15
	movl	%eax, 320(%rbx)
.LBB10_9:                               # %_ZN3vecI3LitE6shrinkEi.exit
	cmpl	%r14d, 336(%rbx)
	jle	.LBB10_11
# BB#10:                                # %.lr.ph.i
	movl	%r14d, 336(%rbx)
.LBB10_11:                              # %_ZN3vecIiE6shrinkEi.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZN6Solver11cancelUntilEi, .Lfunc_end10-_ZN6Solver11cancelUntilEi
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI11_0:
	.quad	4698719940088168448     # double 1389796
.LCPI11_1:
	.quad	4746794007244308480     # double 2147483647
	.text
	.globl	_ZN6Solver13pickBranchLitEid
	.p2align	4, 0x90
	.type	_ZN6Solver13pickBranchLitEid,@function
_ZN6Solver13pickBranchLitEid:           # @_ZN6Solver13pickBranchLitEid
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 32
.Lcfi77:
	.cfi_offset %rbx, -32
.Lcfi78:
	.cfi_offset %r14, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movsd	448(%rbx), %xmm1        # xmm1 = mem[0],zero
	mulsd	.LCPI11_0(%rip), %xmm1
	movsd	.LCPI11_1(%rip), %xmm3  # xmm3 = mem[0],zero
	movapd	%xmm1, %xmm2
	divsd	%xmm3, %xmm2
	cvttsd2si	%xmm2, %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	mulsd	%xmm3, %xmm2
	subsd	%xmm2, %xmm1
	movsd	%xmm1, 448(%rbx)
	movapd	%xmm1, %xmm2
	divsd	%xmm3, %xmm2
	movl	$-1, %eax
	ucomisd	%xmm2, %xmm0
	jbe	.LBB11_5
# BB#1:
	movl	424(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB11_5
# BB#2:
	mulsd	.LCPI11_0(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	.LCPI11_1(%rip), %xmm2  # xmm2 = mem[0],zero
	divsd	%xmm2, %xmm0
	cvttsd2si	%xmm0, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 448(%rbx)
	divsd	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	movq	264(%rbx), %rdx
	movq	416(%rbx), %rcx
	cltq
	movl	(%rcx,%rax,4), %eax
	movslq	%eax, %rcx
	cmpb	$0, (%rdx,%rcx)
	jne	.LBB11_5
# BB#3:
	movq	296(%rbx), %rdx
	cmpb	$0, (%rdx,%rcx)
	je	.LBB11_5
# BB#4:
	incq	120(%rbx)
.LBB11_5:                               # %.preheader
	leaq	408(%rbx), %rbp
	cmpl	$-1, %eax
	jne	.LBB11_7
	jmp	.LBB11_9
	.p2align	4, 0x90
.LBB11_23:                              #   in Loop: Header=BB11_9 Depth=1
	movq	%rbp, %rdi
	callq	_ZN4HeapIN6Solver10VarOrderLtEE9removeMinEv
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$-1, %eax
	je	.LBB11_9
.LBB11_7:
	movq	264(%rbx), %rdx
	movslq	%eax, %rcx
	cmpb	$0, (%rdx,%rcx)
	jne	.LBB11_9
# BB#8:
	movq	296(%rbx), %rdx
	cmpb	$0, (%rdx,%rcx)
	jne	.LBB11_11
.LBB11_9:                               # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 424(%rbx)
	jne	.LBB11_23
# BB#10:
	movl	$-1, %eax
.LBB11_11:
	cmpl	$3, %r14d
	je	.LBB11_17
# BB#12:
	cmpl	$2, %r14d
	je	.LBB11_16
# BB#13:
	cmpl	$1, %r14d
	jne	.LBB11_14
# BB#15:
	movb	$1, %cl
	cmpl	$-1, %eax
	jne	.LBB11_21
	jmp	.LBB11_20
.LBB11_17:
	movsd	448(%rbx), %xmm0        # xmm0 = mem[0],zero
	mulsd	.LCPI11_0(%rip), %xmm0
	movapd	%xmm0, %xmm1
	movsd	.LCPI11_1(%rip), %xmm2  # xmm2 = mem[0],zero
	divsd	%xmm2, %xmm1
	cvttsd2si	%xmm1, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	mulsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, 448(%rbx)
	divsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm0
	cvttsd2si	%xmm0, %ecx
	testl	%ecx, %ecx
	jmp	.LBB11_18
.LBB11_16:
	movq	280(%rbx), %rcx
	movslq	%eax, %rdx
	cmpb	$0, (%rcx,%rdx)
.LBB11_18:
	setne	%cl
	cmpl	$-1, %eax
	jne	.LBB11_21
	jmp	.LBB11_20
.LBB11_14:
	xorl	%ecx, %ecx
	cmpl	$-1, %eax
	je	.LBB11_20
.LBB11_21:
	movzbl	%cl, %ecx
	leal	(%rcx,%rax,2), %eax
	jmp	.LBB11_22
.LBB11_20:
	movl	$-2, %eax
.LBB11_22:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZN6Solver13pickBranchLitEid, .Lfunc_end11-_ZN6Solver13pickBranchLitEid
	.cfi_endproc

	.section	.text._ZN4HeapIN6Solver10VarOrderLtEE9removeMinEv,"axG",@progbits,_ZN4HeapIN6Solver10VarOrderLtEE9removeMinEv,comdat
	.weak	_ZN4HeapIN6Solver10VarOrderLtEE9removeMinEv
	.p2align	4, 0x90
	.type	_ZN4HeapIN6Solver10VarOrderLtEE9removeMinEv,@function
_ZN4HeapIN6Solver10VarOrderLtEE9removeMinEv: # @_ZN4HeapIN6Solver10VarOrderLtEE9removeMinEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 24
.Lcfi82:
	.cfi_offset %rbx, -24
.Lcfi83:
	.cfi_offset %r14, -16
	movq	8(%rdi), %r10
	movslq	(%r10), %rax
	movslq	16(%rdi), %rcx
	movslq	-4(%r10,%rcx,4), %rcx
	movl	%ecx, (%r10)
	movq	24(%rdi), %r9
	movl	$0, (%r9,%rcx,4)
	movl	$-1, (%r9,%rax,4)
	movl	16(%rdi), %esi
	decl	%esi
	movl	%esi, 16(%rdi)
	cmpl	$2, %esi
	jl	.LBB12_10
# BB#1:                                 # %.lr.ph.i
	movl	(%r10), %ecx
	movslq	%ecx, %r8
	xorl	%ebx, %ebx
	movl	$1, %edx
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB12_2:                               # =>This Inner Loop Header: Depth=1
	movl	%ebx, %ecx
	addl	$2, %ecx
	cmpl	%esi, %ecx
	jge	.LBB12_3
# BB#4:                                 #   in Loop: Header=BB12_2 Depth=1
	movslq	%ecx, %rsi
	movslq	(%r10,%rsi,4), %r14
	movslq	%edx, %rsi
	movslq	(%r10,%rsi,4), %rbx
	movq	(%rdi), %rsi
	movq	(%rsi), %rsi
	movsd	(%rsi,%r14,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rsi,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB12_5
	jmp	.LBB12_6
	.p2align	4, 0x90
.LBB12_3:                               # %._crit_edge27.i
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	(%rdi), %rcx
	movq	(%rcx), %rsi
	movslq	%edx, %rcx
	movslq	(%r10,%rcx,4), %rbx
	movsd	(%rsi,%rbx,8), %xmm1    # xmm1 = mem[0],zero
.LBB12_5:                               #   in Loop: Header=BB12_2 Depth=1
	movaps	%xmm1, %xmm0
	movl	%ebx, %r14d
	movl	%edx, %ecx
.LBB12_6:                               #   in Loop: Header=BB12_2 Depth=1
	ucomisd	(%rsi,%r8,8), %xmm0
	jbe	.LBB12_7
# BB#8:                                 # %.thread.i
                                        #   in Loop: Header=BB12_2 Depth=1
	movslq	%r14d, %rdx
	movslq	%r11d, %rsi
	movl	%edx, (%r10,%rsi,4)
	movl	%esi, (%r9,%rdx,4)
	leal	(%rcx,%rcx), %ebx
	leal	1(%rcx,%rcx), %edx
	movl	16(%rdi), %esi
	cmpl	%esi, %edx
	movl	%ecx, %r11d
	jl	.LBB12_2
	jmp	.LBB12_9
.LBB12_7:
	movl	%r11d, %ecx
.LBB12_9:                               # %_ZN4HeapIN6Solver10VarOrderLtEE13percolateDownEi.exit
	movslq	%ecx, %rcx
	movl	%r8d, (%r10,%rcx,4)
	movl	%ecx, (%r9,%r8,4)
.LBB12_10:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	_ZN4HeapIN6Solver10VarOrderLtEE9removeMinEv, .Lfunc_end12-_ZN4HeapIN6Solver10VarOrderLtEE9removeMinEv
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI13_0:
	.quad	4906019910204099648     # double 1.0E+20
.LCPI13_1:
	.quad	4307583784117748259     # double 9.9999999999999995E-21
.LCPI13_2:
	.quad	6103021453049119613     # double 1.0E+100
.LCPI13_4:
	.quad	3110860544497550640     # double 1.0E-100
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_3:
	.quad	3110860544497550640     # double 1.0E-100
	.quad	3110860544497550640     # double 1.0E-100
.LCPI13_5:
	.long	4294967294              # 0xfffffffe
	.long	4294967294              # 0xfffffffe
	.long	4294967294              # 0xfffffffe
	.long	4294967294              # 0xfffffffe
	.text
	.globl	_ZN6Solver7analyzeEP6ClauseR3vecI3LitERi
	.p2align	4, 0x90
	.type	_ZN6Solver7analyzeEP6ClauseR3vecI3LitERi,@function
_ZN6Solver7analyzeEP6ClauseR3vecI3LitERi: # @_ZN6Solver7analyzeEP6ClauseR3vecI3LitERi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi87:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi88:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi90:
	.cfi_def_cfa_offset 96
.Lcfi91:
	.cfi_offset %rbx, -56
.Lcfi92:
	.cfi_offset %r12, -48
.Lcfi93:
	.cfi_offset %r13, -40
.Lcfi94:
	.cfi_offset %r14, -32
.Lcfi95:
	.cfi_offset %r15, -24
.Lcfi96:
	.cfi_offset %rbp, -16
	movq	%rdx, %r8
	movq	%rsi, %r12
	movq	%rdi, %r11
	movl	8(%r8), %edx
	cmpl	12(%r8), %edx
	movq	%r11, 16(%rsp)          # 8-byte Spill
	jne	.LBB13_2
# BB#1:
	leal	1(%rdx,%rdx,2), %eax
	sarl	%eax
	leal	-2(%rax), %esi
	sarl	$31, %esi
	movl	%esi, %edx
	andl	$2, %edx
	notl	%esi
	andl	%eax, %esi
	addl	%edx, %esi
	movl	%esi, 12(%r8)
	movq	(%r8), %rdi
	movslq	%esi, %rsi
	shlq	$2, %rsi
	movq	%rcx, %rbp
	movq	%r8, %rbx
	callq	realloc
	movq	%rbx, %r8
	movq	%rbp, %rcx
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	%rax, (%r8)
	movl	8(%r8), %edx
	jmp	.LBB13_3
.LBB13_2:                               # %._crit_edge.i
	movq	(%r8), %rax
.LBB13_3:                               # %_ZN3vecI3LitE4pushEv.exit
	movslq	%edx, %rdx
	movl	$-2, (%rax,%rdx,4)
	incl	%edx
	movl	%edx, 8(%r8)
	movl	320(%r11), %ebp
	decl	%ebp
	movl	$0, (%rcx)
	xorl	%ebx, %ebx
	movl	$-2, %eax
	movsd	.LCPI13_2(%rip), %xmm2  # xmm2 = mem[0],zero
	movsd	.LCPI13_4(%rip), %xmm3  # xmm3 = mem[0],zero
	movapd	.LCPI13_3(%rip), %xmm4  # xmm4 = [1.000000e-100,1.000000e-100]
	movsd	.LCPI13_1(%rip), %xmm5  # xmm5 = mem[0],zero
	movq	%rcx, %r9
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%r8, 8(%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB13_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_11 Depth 2
                                        #     Child Loop BB13_45 Depth 2
                                        #       Child Loop BB13_43 Depth 3
                                        #       Child Loop BB13_24 Depth 3
                                        #       Child Loop BB13_29 Depth 3
                                        #     Child Loop BB13_49 Depth 2
	movl	(%r12), %ecx
	testb	$1, %cl
	je	.LBB13_13
# BB#5:                                 #   in Loop: Header=BB13_4 Depth=1
	movsd	216(%r11), %xmm0        # xmm0 = mem[0],zero
	movss	4(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, 4(%r12)
	cvtss2sd	%xmm1, %xmm1
	ucomisd	.LCPI13_0(%rip), %xmm1
	jbe	.LBB13_13
# BB#6:                                 # %.preheader.i
                                        #   in Loop: Header=BB13_4 Depth=1
	movslq	208(%r11), %rdx
	testq	%rdx, %rdx
	jle	.LBB13_12
# BB#7:                                 # %.lr.ph.i149
                                        #   in Loop: Header=BB13_4 Depth=1
	movq	200(%r11), %rsi
	testb	$1, %dl
	jne	.LBB13_9
# BB#8:                                 #   in Loop: Header=BB13_4 Depth=1
	xorl	%edi, %edi
	cmpl	$1, %edx
	jne	.LBB13_10
	jmp	.LBB13_12
.LBB13_9:                               #   in Loop: Header=BB13_4 Depth=1
	movq	(%rsi), %rdi
	movss	4(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm5, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, 4(%rdi)
	movl	$1, %edi
	cmpl	$1, %edx
	je	.LBB13_12
.LBB13_10:                              # %.lr.ph.i149.new
                                        #   in Loop: Header=BB13_4 Depth=1
	subq	%rdi, %rdx
	leaq	8(%rsi,%rdi,8), %rsi
	.p2align	4, 0x90
.LBB13_11:                              #   Parent Loop BB13_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rsi), %rdi
	movss	4(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm5, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, 4(%rdi)
	movq	(%rsi), %rdi
	movss	4(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm5, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, 4(%rdi)
	addq	$16, %rsi
	addq	$-2, %rdx
	jne	.LBB13_11
.LBB13_12:                              # %._crit_edge.i150
                                        #   in Loop: Header=BB13_4 Depth=1
	mulsd	%xmm5, %xmm0
	movsd	%xmm0, 216(%r11)
.LBB13_13:                              # %_ZN6Solver15claBumpActivityER6Clause.exit
                                        #   in Loop: Header=BB13_4 Depth=1
	xorl	%edx, %edx
	cmpl	$-2, %eax
	setne	%dl
	shrl	$3, %ecx
	cmpl	%ecx, %edx
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	jae	.LBB13_47
# BB#14:                                # %.lr.ph231.preheader
                                        #   in Loop: Header=BB13_4 Depth=1
	xorl	%r14d, %r14d
	cmpl	$-2, %eax
	setne	%r14b
	movq	%r9, %rbx
	jmp	.LBB13_45
	.p2align	4, 0x90
.LBB13_15:                              #   in Loop: Header=BB13_45 Depth=2
	movq	360(%r11), %rax
	cmpl	$0, (%rax,%r13,4)
	jle	.LBB13_46
# BB#16:                                #   in Loop: Header=BB13_45 Depth=2
	movsd	240(%r11), %xmm0        # xmm0 = mem[0],zero
	movq	224(%r11), %rsi
	addsd	(%rsi,%r13,8), %xmm0
	movsd	%xmm0, (%rsi,%r13,8)
	ucomisd	%xmm2, %xmm0
	jbe	.LBB13_26
# BB#17:                                # %.preheader.i179
                                        #   in Loop: Header=BB13_45 Depth=2
	movslq	272(%r11), %rdx
	testq	%rdx, %rdx
	jle	.LBB13_25
# BB#18:                                # %.lr.ph.i180
                                        #   in Loop: Header=BB13_45 Depth=2
	cmpl	$3, %edx
	jbe	.LBB13_22
# BB#19:                                # %min.iters.checked
                                        #   in Loop: Header=BB13_45 Depth=2
	movq	%rdx, %r8
	andq	$-4, %r8
	je	.LBB13_22
# BB#20:                                # %vector.body.preheader
                                        #   in Loop: Header=BB13_45 Depth=2
	leaq	-4(%r8), %rax
	movq	%rax, %rdi
	shrq	$2, %rdi
	btl	$2, %eax
	jb	.LBB13_41
# BB#21:                                # %vector.body.prol
                                        #   in Loop: Header=BB13_45 Depth=2
	movupd	(%rsi), %xmm0
	movupd	16(%rsi), %xmm1
	mulpd	%xmm4, %xmm0
	mulpd	%xmm4, %xmm1
	movupd	%xmm0, (%rsi)
	movupd	%xmm1, 16(%rsi)
	movl	$4, %ebx
	testq	%rdi, %rdi
	jne	.LBB13_42
	jmp	.LBB13_44
.LBB13_22:                              #   in Loop: Header=BB13_45 Depth=2
	xorl	%r8d, %r8d
.LBB13_23:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB13_45 Depth=2
	subq	%r8, %rdx
	leaq	(%rsi,%r8,8), %rsi
	.p2align	4, 0x90
.LBB13_24:                              # %scalar.ph
                                        #   Parent Loop BB13_4 Depth=1
                                        #     Parent Loop BB13_45 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	mulsd	%xmm3, %xmm0
	movsd	%xmm0, (%rsi)
	addq	$8, %rsi
	decq	%rdx
	jne	.LBB13_24
.LBB13_25:                              # %._crit_edge.i181
                                        #   in Loop: Header=BB13_45 Depth=2
	movsd	240(%r11), %xmm0        # xmm0 = mem[0],zero
	mulsd	%xmm3, %xmm0
	movsd	%xmm0, 240(%r11)
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB13_26:                              #   in Loop: Header=BB13_45 Depth=2
	cmpl	%ecx, 440(%r11)
	jle	.LBB13_34
# BB#27:                                # %_ZNK4HeapIN6Solver10VarOrderLtEE6inHeapEi.exit.i
                                        #   in Loop: Header=BB13_45 Depth=2
	movq	432(%r11), %r10
	movslq	(%r10,%r13,4), %rdi
	testq	%rdi, %rdi
	js	.LBB13_34
# BB#28:                                #   in Loop: Header=BB13_45 Depth=2
	movq	416(%r11), %rdx
	movl	(%rdx,%rdi,4), %eax
	xorl	%r8d, %r8d
	testl	%edi, %edi
	movslq	%eax, %rsi
	je	.LBB13_33
	.p2align	4, 0x90
.LBB13_29:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB13_4 Depth=1
                                        #     Parent Loop BB13_45 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	-1(%rdi), %ebx
	sarl	%ebx
	movslq	%ebx, %rax
	movl	(%rdx,%rax,4), %eax
	movq	408(%r11), %rcx
	movq	(%rcx), %rcx
	movsd	(%rcx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	cltq
	ucomisd	(%rcx,%rax,8), %xmm0
	jbe	.LBB13_31
# BB#30:                                #   in Loop: Header=BB13_29 Depth=3
	movslq	%edi, %rcx
	movl	%eax, (%rdx,%rcx,4)
	movl	%edi, (%r10,%rax,4)
	testl	%ebx, %ebx
	movl	%ebx, %edi
	jne	.LBB13_29
	jmp	.LBB13_32
.LBB13_31:                              #   in Loop: Header=BB13_45 Depth=2
	movl	%edi, %r8d
.LBB13_32:                              #   in Loop: Header=BB13_45 Depth=2
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB13_33:                              # %_ZN4HeapIN6Solver10VarOrderLtEE8decreaseEi.exit.i
                                        #   in Loop: Header=BB13_45 Depth=2
	movslq	%r8d, %rax
	movl	%esi, (%rdx,%rax,4)
	movl	%eax, (%r10,%rsi,4)
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB13_34:                              # %_ZN6Solver15varBumpActivityEi.exit
                                        #   in Loop: Header=BB13_45 Depth=2
	movb	$1, (%r9,%r13)
	movq	360(%r11), %rcx
	movl	(%rcx,%r13,4), %eax
	cmpl	336(%r11), %eax
	jge	.LBB13_37
# BB#35:                                #   in Loop: Header=BB13_45 Depth=2
	movl	8(%r8), %edx
	cmpl	12(%r8), %edx
	jne	.LBB13_38
# BB#36:                                #   in Loop: Header=BB13_45 Depth=2
	leal	1(%rdx,%rdx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 12(%r8)
	movq	(%r8), %rdi
	movslq	%ecx, %rsi
	shlq	$2, %rsi
	callq	realloc
	movsd	.LCPI13_1(%rip), %xmm5  # xmm5 = mem[0],zero
	movapd	.LCPI13_3(%rip), %xmm4  # xmm4 = [1.000000e-100,1.000000e-100]
	movsd	.LCPI13_4(%rip), %xmm3  # xmm3 = mem[0],zero
	movsd	.LCPI13_2(%rip), %xmm2  # xmm2 = mem[0],zero
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	%rax, (%r8)
	movl	8(%r8), %edx
	movq	360(%r11), %rcx
	jmp	.LBB13_39
.LBB13_37:                              #   in Loop: Header=BB13_45 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jmp	.LBB13_46
.LBB13_38:                              # %._crit_edge.i178
                                        #   in Loop: Header=BB13_45 Depth=2
	movq	(%r8), %rax
.LBB13_39:                              # %_ZN3vecI3LitE4pushERKS0_.exit
                                        #   in Loop: Header=BB13_45 Depth=2
	leal	1(%rdx), %esi
	movl	%esi, 8(%r8)
	movslq	%edx, %rdx
	movl	%r15d, (%rax,%rdx,4)
	movl	(%rcx,%r13,4), %eax
	cmpl	(%rbx), %eax
	jle	.LBB13_46
# BB#40:                                #   in Loop: Header=BB13_45 Depth=2
	movl	%eax, (%rbx)
	jmp	.LBB13_46
.LBB13_41:                              #   in Loop: Header=BB13_45 Depth=2
	xorl	%ebx, %ebx
	testq	%rdi, %rdi
	je	.LBB13_44
.LBB13_42:                              # %vector.body.preheader.new
                                        #   in Loop: Header=BB13_45 Depth=2
	movq	%r8, %rdi
	subq	%rbx, %rdi
	leaq	48(%rsi,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB13_43:                              # %vector.body
                                        #   Parent Loop BB13_4 Depth=1
                                        #     Parent Loop BB13_45 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	-48(%rbx), %xmm0
	movupd	-32(%rbx), %xmm1
	mulpd	%xmm4, %xmm0
	mulpd	%xmm4, %xmm1
	movupd	%xmm0, -48(%rbx)
	movupd	%xmm1, -32(%rbx)
	movupd	-16(%rbx), %xmm0
	movupd	(%rbx), %xmm1
	mulpd	%xmm4, %xmm0
	mulpd	%xmm4, %xmm1
	movupd	%xmm0, -16(%rbx)
	movupd	%xmm1, (%rbx)
	addq	$64, %rbx
	addq	$-8, %rdi
	jne	.LBB13_43
.LBB13_44:                              # %middle.block
                                        #   in Loop: Header=BB13_45 Depth=2
	cmpq	%r8, %rdx
	movq	24(%rsp), %rbx          # 8-byte Reload
	jne	.LBB13_23
	jmp	.LBB13_25
	.p2align	4, 0x90
.LBB13_45:                              # %.lr.ph231
                                        #   Parent Loop BB13_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_43 Depth 3
                                        #       Child Loop BB13_24 Depth 3
                                        #       Child Loop BB13_29 Depth 3
	movl	8(%r12,%r14,4), %r15d
	movl	%r15d, %ecx
	sarl	%ecx
	movq	472(%r11), %r9
	movslq	%ecx, %r13
	cmpb	$0, (%r9,%r13)
	je	.LBB13_15
.LBB13_46:                              #   in Loop: Header=BB13_45 Depth=2
	incq	%r14
	movl	(%r12), %eax
	shrl	$3, %eax
	cmpq	%rax, %r14
	jl	.LBB13_45
	jmp	.LBB13_48
	.p2align	4, 0x90
.LBB13_47:                              #   in Loop: Header=BB13_4 Depth=1
	movq	%r9, %rbx
.LBB13_48:                              # %.preheader198
                                        #   in Loop: Header=BB13_4 Depth=1
	movq	%rbx, %r9
	movq	472(%r11), %rcx
	movslq	%ebp, %rdx
	shlq	$2, %rdx
	addq	312(%r11), %rdx
	.p2align	4, 0x90
.LBB13_49:                              #   Parent Loop BB13_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %eax
	movl	%eax, %esi
	sarl	%esi
	movslq	%esi, %rsi
	addq	$-4, %rdx
	decl	%ebp
	cmpb	$0, (%rcx,%rsi)
	je	.LBB13_49
# BB#50:                                #   in Loop: Header=BB13_4 Depth=1
	movq	344(%r11), %rdx
	movq	(%rdx,%rsi,8), %r12
	movb	$0, (%rcx,%rsi)
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	$1, %ecx
	leal	-1(%rcx), %ecx
	movl	%ecx, %ebx
	jg	.LBB13_4
# BB#51:
	xorl	$1, %eax
	movq	(%r8), %rcx
	movl	%eax, (%rcx)
	cmpb	$0, 88(%r11)
	je	.LBB13_55
# BB#52:                                # %.preheader197
	movslq	8(%r8), %r13
	xorl	%r15d, %r15d
	cmpq	$2, %r13
	jl	.LBB13_59
# BB#53:                                # %.lr.ph226
	movq	(%r8), %rax
	movq	360(%r11), %rdx
	testb	$1, %r13b
	jne	.LBB13_57
# BB#54:
	movl	4(%rax), %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	movb	(%rdx,%rcx,4), %cl
	movl	$1, %r15d
	shll	%cl, %r15d
	movl	$2, %esi
	cmpl	$2, %r13d
	jne	.LBB13_58
	jmp	.LBB13_59
.LBB13_55:
	movq	504(%r11), %rax
	testq	%rax, %rax
	je	.LBB13_65
# BB#56:                                # %.preheader.i.i
	movl	$0, 512(%r11)
	xorl	%edx, %edx
	jmp	.LBB13_66
.LBB13_57:
	xorl	%r15d, %r15d
	movl	$1, %esi
	cmpl	$2, %r13d
	je	.LBB13_59
	.p2align	4, 0x90
.LBB13_58:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rsi,4), %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	movzbl	(%rdx,%rcx,4), %ecx
	movl	$1, %edi
	shll	%cl, %edi
	orl	%r15d, %edi
	movl	4(%rax,%rsi,4), %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	movzbl	(%rdx,%rcx,4), %ecx
	movl	$1, %r15d
	shll	%cl, %r15d
	orl	%edi, %r15d
	addq	$2, %rsi
	cmpq	%r13, %rsi
	jl	.LBB13_58
.LBB13_59:                              # %._crit_edge227
	movq	504(%r11), %rax
	testq	%rax, %rax
	je	.LBB13_61
# BB#60:                                # %.preheader.i.i156
	movl	$0, 512(%r11)
	movl	8(%r8), %r13d
	xorl	%ecx, %ecx
	cmpl	%r13d, %ecx
	jl	.LBB13_62
	jmp	.LBB13_95
.LBB13_61:                              # %._ZN3vecI3LitE5clearEb.exit_crit_edge.i155
	movl	512(%r11), %ecx
	cmpl	%r13d, %ecx
	jge	.LBB13_95
.LBB13_62:
	movl	516(%r11), %edx
	cmpl	%r13d, %edx
	jge	.LBB13_72
# BB#63:
	testl	%edx, %edx
	je	.LBB13_70
	.p2align	4, 0x90
.LBB13_64:                              # %.preheader.i.i.i158
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rdx,%rdx,2), %edx
	sarl	%edx
	cmpl	%r13d, %edx
	jl	.LBB13_64
	jmp	.LBB13_71
.LBB13_65:                              # %._ZN3vecI3LitE5clearEb.exit_crit_edge.i
	movl	512(%r11), %edx
.LBB13_66:                              # %_ZN3vecI3LitE5clearEb.exit.i
	movl	8(%r8), %r13d
	cmpl	%r13d, %edx
	jge	.LBB13_116
# BB#67:
	movl	516(%r11), %ecx
	cmpl	%r13d, %ecx
	jge	.LBB13_80
# BB#68:
	testl	%ecx, %ecx
	je	.LBB13_78
	.p2align	4, 0x90
.LBB13_69:                              # %.preheader.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rcx,%rcx,2), %ecx
	sarl	%ecx
	cmpl	%r13d, %ecx
	jl	.LBB13_69
	jmp	.LBB13_79
.LBB13_70:
	cmpl	$1, %r13d
	movl	$2, %edx
	cmovgl	%r13d, %edx
.LBB13_71:                              # %.loopexit.i.i.i161
	movl	%edx, 516(%r11)
	movslq	%edx, %rsi
	shlq	$2, %rsi
	movq	%rax, %rdi
	movq	%r11, %rbp
	callq	realloc
	movq	%rax, 504(%rbp)
	movl	512(%rbp), %ecx
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB13_72:                              # %_ZN3vecI3LitE4growEi.exit.i.i162
	cmpl	%r13d, %ecx
	jge	.LBB13_94
# BB#73:                                # %.lr.ph.i.i164
	movslq	%ecx, %r11
	movslq	%r13d, %rcx
	movq	%rcx, %rsi
	subq	%r11, %rsi
	cmpq	$7, %rsi
	jbe	.LBB13_92
# BB#74:                                # %min.iters.checked286
	movq	%rsi, %r9
	andq	$-8, %r9
	movq	%rsi, %r10
	andq	$-8, %r10
	je	.LBB13_92
# BB#75:                                # %vector.body282.preheader
	leaq	-8(%r10), %r8
	movl	%r8d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB13_86
# BB#76:                                # %vector.body282.prol.preheader
	leaq	16(%rax,%r11,4), %rdx
	negq	%rbp
	xorl	%edi, %edi
	movapd	.LCPI13_5(%rip), %xmm0  # xmm0 = [4294967294,4294967294,4294967294,4294967294]
	.p2align	4, 0x90
.LBB13_77:                              # %vector.body282.prol
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, -16(%rdx,%rdi,4)
	movupd	%xmm0, (%rdx,%rdi,4)
	addq	$8, %rdi
	incq	%rbp
	jne	.LBB13_77
	jmp	.LBB13_87
.LBB13_78:
	cmpl	$1, %r13d
	movl	$2, %ecx
	cmovgl	%r13d, %ecx
.LBB13_79:                              # %.loopexit.i.i.i
	movl	%ecx, 516(%r11)
	movslq	%ecx, %rsi
	shlq	$2, %rsi
	movq	%rax, %rdi
	callq	realloc
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	%rax, 504(%r11)
	movl	512(%r11), %edx
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB13_80:                              # %_ZN3vecI3LitE4growEi.exit.i.i
	cmpl	%r13d, %edx
	jge	.LBB13_115
# BB#81:                                # %.lr.ph.i.i
	movslq	%r13d, %rcx
	movslq	%edx, %rdx
	movq	%rcx, %rsi
	subq	%rdx, %rsi
	cmpq	$7, %rsi
	jbe	.LBB13_113
# BB#82:                                # %min.iters.checked303
	movq	%rsi, %r9
	andq	$-8, %r9
	movq	%rsi, %r10
	andq	$-8, %r10
	je	.LBB13_113
# BB#83:                                # %vector.body299.preheader
	leaq	-8(%r10), %r8
	movl	%r8d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB13_107
# BB#84:                                # %vector.body299.prol.preheader
	leaq	16(%rax,%rdx,4), %rbp
	negq	%rdi
	xorl	%ebx, %ebx
	movapd	.LCPI13_5(%rip), %xmm0  # xmm0 = [4294967294,4294967294,4294967294,4294967294]
	.p2align	4, 0x90
.LBB13_85:                              # %vector.body299.prol
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, -16(%rbp,%rbx,4)
	movupd	%xmm0, (%rbp,%rbx,4)
	addq	$8, %rbx
	incq	%rdi
	jne	.LBB13_85
	jmp	.LBB13_108
.LBB13_86:
	xorl	%edi, %edi
.LBB13_87:                              # %vector.body282.prol.loopexit
	cmpq	$24, %r8
	jb	.LBB13_90
# BB#88:                                # %vector.body282.preheader.new
	movq	%r10, %rbp
	subq	%rdi, %rbp
	addq	%r11, %rdi
	leaq	112(%rax,%rdi,4), %rdi
	movapd	.LCPI13_5(%rip), %xmm0  # xmm0 = [4294967294,4294967294,4294967294,4294967294]
	.p2align	4, 0x90
.LBB13_89:                              # %vector.body282
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, -112(%rdi)
	movupd	%xmm0, -96(%rdi)
	movupd	%xmm0, -80(%rdi)
	movupd	%xmm0, -64(%rdi)
	movupd	%xmm0, -48(%rdi)
	movupd	%xmm0, -32(%rdi)
	movupd	%xmm0, -16(%rdi)
	movupd	%xmm0, (%rdi)
	subq	$-128, %rdi
	addq	$-32, %rbp
	jne	.LBB13_89
.LBB13_90:                              # %middle.block283
	cmpq	%r10, %rsi
	movq	8(%rsp), %r8            # 8-byte Reload
	je	.LBB13_94
# BB#91:
	addq	%r9, %r11
.LBB13_92:                              # %scalar.ph284.preheader
	subq	%r11, %rcx
	leaq	(%rax,%r11,4), %rdx
	.p2align	4, 0x90
.LBB13_93:                              # %scalar.ph284
                                        # =>This Inner Loop Header: Depth=1
	movl	$-2, (%rdx)
	addq	$4, %rdx
	decq	%rcx
	jne	.LBB13_93
.LBB13_94:                              # %._crit_edge.i.i166
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	%r13d, 512(%r11)
	movl	8(%r8), %r13d
.LBB13_95:                              # %_ZN3vecI3LitE6growToEi.exit.preheader.i167
	movl	$1, %r14d
	testl	%r13d, %r13d
	jle	.LBB13_130
# BB#96:                                # %.lr.ph.i168
	movq	(%r8), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB13_97:                              # %_ZN3vecI3LitE6growToEi.exit.i174
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rdx,4), %esi
	movl	%esi, (%rax,%rdx,4)
	incq	%rdx
	movslq	8(%r8), %r13
	cmpq	%r13, %rdx
	jl	.LBB13_97
# BB#98:                                # %_ZNK3vecI3LitE6copyToERS1_.exit175.preheader
	cmpl	$2, %r13d
	movl	$1, %ebp
	jl	.LBB13_131
# BB#99:                                # %.lr.ph221.preheader
	movl	$1, %r14d
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB13_100:                             # %.lr.ph221
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r8), %rax
	movl	(%rax,%rbp,4), %esi
	movl	%esi, %ecx
	sarl	%ecx
	movq	344(%r11), %rdx
	movslq	%ecx, %rcx
	cmpq	$0, (%rdx,%rcx,8)
	je	.LBB13_104
# BB#101:                               #   in Loop: Header=BB13_100 Depth=1
	movq	%r11, %rdi
	movl	%r15d, %edx
	callq	_ZN6Solver12litRedundantE3Litj
	testb	%al, %al
	je	.LBB13_103
# BB#102:                               #   in Loop: Header=BB13_100 Depth=1
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	jmp	.LBB13_105
	.p2align	4, 0x90
.LBB13_103:                             # %._crit_edge258
                                        #   in Loop: Header=BB13_100 Depth=1
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	(%r8), %rax
	movl	(%rax,%rbp,4), %esi
	movq	16(%rsp), %r11          # 8-byte Reload
.LBB13_104:                             #   in Loop: Header=BB13_100 Depth=1
	movslq	%r14d, %rcx
	incl	%r14d
	movl	%esi, (%rax,%rcx,4)
.LBB13_105:                             # %_ZNK3vecI3LitE6copyToERS1_.exit175
                                        #   in Loop: Header=BB13_100 Depth=1
	incq	%rbp
	movslq	8(%r8), %r13
	cmpq	%r13, %rbp
	jl	.LBB13_100
	jmp	.LBB13_131
.LBB13_107:
	xorl	%ebx, %ebx
.LBB13_108:                             # %vector.body299.prol.loopexit
	cmpq	$24, %r8
	jb	.LBB13_111
# BB#109:                               # %vector.body299.preheader.new
	movq	%r10, %rdi
	subq	%rbx, %rdi
	addq	%rdx, %rbx
	leaq	112(%rax,%rbx,4), %rbx
	movapd	.LCPI13_5(%rip), %xmm0  # xmm0 = [4294967294,4294967294,4294967294,4294967294]
	.p2align	4, 0x90
.LBB13_110:                             # %vector.body299
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, -112(%rbx)
	movupd	%xmm0, -96(%rbx)
	movupd	%xmm0, -80(%rbx)
	movupd	%xmm0, -64(%rbx)
	movupd	%xmm0, -48(%rbx)
	movupd	%xmm0, -32(%rbx)
	movupd	%xmm0, -16(%rbx)
	movupd	%xmm0, (%rbx)
	subq	$-128, %rbx
	addq	$-32, %rdi
	jne	.LBB13_110
.LBB13_111:                             # %middle.block300
	cmpq	%r10, %rsi
	movq	8(%rsp), %r8            # 8-byte Reload
	je	.LBB13_115
# BB#112:
	addq	%r9, %rdx
.LBB13_113:                             # %scalar.ph301.preheader
	subq	%rdx, %rcx
	leaq	(%rax,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB13_114:                             # %scalar.ph301
                                        # =>This Inner Loop Header: Depth=1
	movl	$-2, (%rdx)
	addq	$4, %rdx
	decq	%rcx
	jne	.LBB13_114
.LBB13_115:                             # %._crit_edge.i.i
	movl	%r13d, 512(%r11)
	movl	8(%r8), %r13d
.LBB13_116:                             # %_ZN3vecI3LitE6growToEi.exit.preheader.i
	movl	$1, %r14d
	testl	%r13d, %r13d
	jle	.LBB13_130
# BB#117:                               # %.lr.ph.i148
	movq	(%r8), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB13_118:                             # %_ZN3vecI3LitE6growToEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rdx,4), %esi
	movl	%esi, (%rax,%rdx,4)
	incq	%rdx
	movslq	8(%r8), %r13
	cmpq	%r13, %rdx
	jl	.LBB13_118
# BB#119:                               # %_ZNK3vecI3LitE6copyToERS1_.exit.preheader
	cmpl	$2, %r13d
	jl	.LBB13_130
# BB#120:                               # %.lr.ph216.preheader
	movl	$1, %r14d
	movl	$1, %ebp
	jmp	.LBB13_122
.LBB13_121:                             #   in Loop: Header=BB13_122 Depth=1
	movslq	%r14d, %rax
	incl	%r14d
	movl	%r9d, (%r8,%rax,4)
	movq	8(%rsp), %r8            # 8-byte Reload
	movl	8(%r8), %r13d
	jmp	.LBB13_128
	.p2align	4, 0x90
.LBB13_122:                             # %.lr.ph216
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_124 Depth 2
	movq	(%r8), %r8
	movl	(%r8,%rbp,4), %r9d
	movl	%r9d, %eax
	sarl	%eax
	movq	344(%r11), %rdx
	cltq
	movq	(%rdx,%rax,8), %rdx
	movl	(%rdx), %esi
	cmpl	$16, %esi
	jb	.LBB13_127
# BB#123:                               # %.lr.ph213
                                        #   in Loop: Header=BB13_122 Depth=1
	movq	472(%r11), %rdi
	shrl	$3, %esi
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB13_124:                             #   Parent Loop BB13_122 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%rdx,%rbx,4), %eax
	sarl	%eax
	cltq
	cmpb	$0, (%rdi,%rax)
	jne	.LBB13_126
# BB#125:                               #   in Loop: Header=BB13_124 Depth=2
	movq	360(%r11), %rcx
	cmpl	$0, (%rcx,%rax,4)
	jg	.LBB13_121
.LBB13_126:                             #   in Loop: Header=BB13_124 Depth=2
	incq	%rbx
	cmpq	%rsi, %rbx
	jl	.LBB13_124
.LBB13_127:                             #   in Loop: Header=BB13_122 Depth=1
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB13_128:                             # %_ZNK3vecI3LitE6copyToERS1_.exit
                                        #   in Loop: Header=BB13_122 Depth=1
	incq	%rbp
	movslq	%r13d, %rax
	cmpq	%rax, %rbp
	jl	.LBB13_122
	jmp	.LBB13_131
.LBB13_130:
	movl	$1, %ebp
.LBB13_131:                             # %.loopexit195
	movslq	%r13d, %rax
	addq	%rax, 160(%r11)
	subl	%r14d, %ebp
	jle	.LBB13_133
# BB#132:                               # %.lr.ph.i
	subl	%ebp, %r13d
	movl	%r13d, 8(%r8)
.LBB13_133:                             # %_ZN3vecI3LitE6shrinkEi.exit
	movq	24(%rsp), %rdi          # 8-byte Reload
	movslq	%r13d, %r8
	addq	%r8, 168(%r11)
	xorl	%eax, %eax
	cmpl	$1, %r8d
	je	.LBB13_142
# BB#134:                               # %.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rax
	cmpl	$3, %r13d
	jl	.LBB13_137
# BB#135:                               # %.lr.ph208
	movq	360(%r11), %rdx
	testb	$1, %r8b
	jne	.LBB13_138
# BB#136:
	movl	$1, %edi
	movl	$2, %esi
	cmpl	$3, %r13d
	jne	.LBB13_139
	jmp	.LBB13_140
.LBB13_137:
	movl	$1, %ecx
	jmp	.LBB13_141
.LBB13_138:
	movl	4(%rax), %esi
	movl	8(%rax), %edi
	sarl	%edi
	movslq	%edi, %rdi
	movl	(%rdx,%rdi,4), %ebp
	sarl	%esi
	movslq	%esi, %rsi
	xorl	%edi, %edi
	cmpl	(%rdx,%rsi,4), %ebp
	setg	%dil
	incl	%edi
	movl	$3, %esi
	cmpl	$3, %r13d
	je	.LBB13_140
	.p2align	4, 0x90
.LBB13_139:                             # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rsi,4), %ebp
	sarl	%ebp
	movslq	%ebp, %rbp
	movl	(%rdx,%rbp,4), %ebp
	movslq	%edi, %rdi
	movl	(%rax,%rdi,4), %ebx
	sarl	%ebx
	movslq	%ebx, %rbx
	cmpl	(%rdx,%rbx,4), %ebp
	cmovgl	%esi, %edi
	movl	4(%rax,%rsi,4), %ebp
	sarl	%ebp
	movslq	%ebp, %rbp
	movl	(%rdx,%rbp,4), %ebp
	movslq	%edi, %rdi
	movl	(%rax,%rdi,4), %ebx
	sarl	%ebx
	movslq	%ebx, %rbx
	leal	1(%rsi), %ecx
	cmpl	(%rdx,%rbx,4), %ebp
	cmovgl	%ecx, %edi
	addq	$2, %rsi
	cmpq	%r8, %rsi
	jl	.LBB13_139
.LBB13_140:                             # %._crit_edge209.loopexit
	movslq	%edi, %rcx
	movq	24(%rsp), %rdi          # 8-byte Reload
.LBB13_141:                             # %._crit_edge209
	movl	(%rax,%rcx,4), %edx
	movl	4(%rax), %esi
	movl	%esi, (%rax,%rcx,4)
	movl	%edx, 4(%rax)
	sarl	%edx
	movq	360(%r11), %rax
	movslq	%edx, %rcx
	movl	(%rax,%rcx,4), %eax
.LBB13_142:
	movl	%eax, (%rdi)
	cmpl	$0, 512(%r11)
	jle	.LBB13_145
# BB#143:                               # %.lr.ph
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB13_144:                             # =>This Inner Loop Header: Depth=1
	movq	472(%r11), %rcx
	movq	504(%r11), %rdx
	movl	(%rdx,%rax,4), %edx
	sarl	%edx
	movslq	%edx, %rdx
	movb	$0, (%rcx,%rdx)
	incq	%rax
	movslq	512(%r11), %rcx
	cmpq	%rcx, %rax
	jl	.LBB13_144
.LBB13_145:                             # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_ZN6Solver7analyzeEP6ClauseR3vecI3LitERi, .Lfunc_end13-_ZN6Solver7analyzeEP6ClauseR3vecI3LitERi
	.cfi_endproc

	.globl	_ZN6Solver12litRedundantE3Litj
	.p2align	4, 0x90
	.type	_ZN6Solver12litRedundantE3Litj,@function
_ZN6Solver12litRedundantE3Litj:         # @_ZN6Solver12litRedundantE3Litj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi100:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi101:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi103:
	.cfi_def_cfa_offset 64
.Lcfi104:
	.cfi_offset %rbx, -56
.Lcfi105:
	.cfi_offset %r12, -48
.Lcfi106:
	.cfi_offset %r13, -40
.Lcfi107:
	.cfi_offset %r14, -32
.Lcfi108:
	.cfi_offset %r15, -24
.Lcfi109:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	488(%rbx), %rax
	testq	%rax, %rax
	je	.LBB14_1
# BB#2:                                 # %.preheader.i
	movl	$0, 496(%rbx)
	xorl	%ecx, %ecx
	cmpl	500(%rbx), %ecx
	je	.LBB14_4
	jmp	.LBB14_5
.LBB14_1:                               # %._ZN3vecI3LitE5clearEb.exit_crit_edge
	movl	496(%rbx), %ecx
	cmpl	500(%rbx), %ecx
	jne	.LBB14_5
.LBB14_4:
	leal	1(%rcx,%rcx,2), %ecx
	sarl	%ecx
	leal	-2(%rcx), %edx
	sarl	$31, %edx
	movl	%edx, %esi
	andl	$2, %esi
	notl	%edx
	andl	%ecx, %edx
	addl	%esi, %edx
	movl	%edx, 500(%rbx)
	movslq	%edx, %rsi
	shlq	$2, %rsi
	movq	%rax, %rdi
	callq	realloc
	movq	%rax, 488(%rbx)
	movl	496(%rbx), %ecx
.LBB14_5:                               # %_ZN3vecI3LitE4pushERKS0_.exit39
	leal	1(%rcx), %edx
	movl	%edx, 496(%rbx)
	movslq	%ecx, %rcx
	movl	%r14d, (%rax,%rcx,4)
	movl	496(%rbx), %ecx
	movb	$1, %dl
	testl	%ecx, %ecx
	jle	.LBB14_10
# BB#6:
	movl	512(%rbx), %edx
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movslq	%edx, %r13
	jmp	.LBB14_12
	.p2align	4, 0x90
.LBB14_7:                               # %_ZN3vecI3LitE6shrinkEi.exit.thread.loopexit.loopexit
                                        #   in Loop: Header=BB14_12 Depth=1
	movl	496(%rbx), %ecx
.LBB14_8:                               # %_ZN3vecI3LitE6shrinkEi.exit.thread.loopexit
                                        #   in Loop: Header=BB14_12 Depth=1
	testl	%ecx, %ecx
	jle	.LBB14_9
# BB#11:                                # %_ZN3vecI3LitE6shrinkEi.exit.thread.loopexit._crit_edge
                                        #   in Loop: Header=BB14_12 Depth=1
	movq	488(%rbx), %rax
.LBB14_12:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_14 Depth 2
	movslq	%ecx, %rdx
	decl	%ecx
	movl	-4(%rax,%rdx,4), %eax
	sarl	%eax
	movq	344(%rbx), %rdx
	cltq
	movq	(%rdx,%rax,8), %rbp
	movl	%ecx, 496(%rbx)
	movl	(%rbp), %eax
	cmpl	$16, %eax
	jb	.LBB14_8
# BB#13:                                # %.lr.ph60.preheader
                                        #   in Loop: Header=BB14_12 Depth=1
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB14_14:                              # %.lr.ph60
                                        #   Parent Loop BB14_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%rbp,%r15,4), %r14d
	movl	%r14d, %edx
	sarl	%edx
	movq	472(%rbx), %rcx
	movslq	%edx, %rdx
	cmpb	$0, (%rcx,%rdx)
	jne	.LBB14_31
# BB#15:                                #   in Loop: Header=BB14_14 Depth=2
	movq	360(%rbx), %rsi
	movl	(%rsi,%rdx,4), %esi
	testl	%esi, %esi
	jle	.LBB14_31
# BB#16:                                #   in Loop: Header=BB14_14 Depth=2
	movq	344(%rbx), %rax
	cmpq	$0, (%rax,%rdx,8)
	je	.LBB14_17
# BB#23:                                #   in Loop: Header=BB14_14 Depth=2
	btl	%esi, %r12d
	jae	.LBB14_17
# BB#24:                                #   in Loop: Header=BB14_14 Depth=2
	movb	$1, (%rcx,%rdx)
	movl	496(%rbx), %ecx
	cmpl	500(%rbx), %ecx
	jne	.LBB14_25
# BB#26:                                #   in Loop: Header=BB14_14 Depth=2
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 500(%rbx)
	movq	488(%rbx), %rdi
	movslq	%ecx, %rsi
	shlq	$2, %rsi
	callq	realloc
	movq	%rax, 488(%rbx)
	movl	496(%rbx), %ecx
	jmp	.LBB14_27
.LBB14_25:                              # %._crit_edge.i42
                                        #   in Loop: Header=BB14_14 Depth=2
	movq	488(%rbx), %rax
.LBB14_27:                              # %_ZN3vecI3LitE4pushERKS0_.exit44
                                        #   in Loop: Header=BB14_14 Depth=2
	leal	1(%rcx), %edx
	movl	%edx, 496(%rbx)
	movslq	%ecx, %rcx
	movl	%r14d, (%rax,%rcx,4)
	movl	512(%rbx), %ecx
	cmpl	516(%rbx), %ecx
	jne	.LBB14_28
# BB#29:                                #   in Loop: Header=BB14_14 Depth=2
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 516(%rbx)
	movq	504(%rbx), %rdi
	movslq	%ecx, %rsi
	shlq	$2, %rsi
	callq	realloc
	movq	%rax, 504(%rbx)
	movl	512(%rbx), %ecx
	jmp	.LBB14_30
.LBB14_28:                              # %._crit_edge.i
                                        #   in Loop: Header=BB14_14 Depth=2
	movq	504(%rbx), %rax
.LBB14_30:                              # %_ZN3vecI3LitE4pushERKS0_.exit
                                        #   in Loop: Header=BB14_14 Depth=2
	leal	1(%rcx), %edx
	movl	%edx, 512(%rbx)
	movslq	%ecx, %rcx
	movl	%r14d, (%rax,%rcx,4)
	movl	(%rbp), %eax
.LBB14_31:                              #   in Loop: Header=BB14_14 Depth=2
	incq	%r15
	movl	%eax, %ecx
	shrl	$3, %ecx
	cmpq	%rcx, %r15
	jl	.LBB14_14
	jmp	.LBB14_7
.LBB14_17:                              # %.preheader
	movl	512(%rbx), %eax
	movl	4(%rsp), %edx           # 4-byte Reload
	cmpl	%eax, %edx
	jge	.LBB14_20
# BB#18:                                # %.lr.ph
	movq	504(%rbx), %rax
	movl	(%rax,%r13,4), %eax
	sarl	%eax
	cltq
	movb	$0, (%rcx,%rax)
	jmp	.LBB14_19
	.p2align	4, 0x90
.LBB14_32:                              # %._crit_edge68
                                        #   in Loop: Header=BB14_19 Depth=1
	movq	472(%rbx), %rax
	movq	504(%rbx), %rcx
	movl	(%rcx,%r13,4), %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	movb	$0, (%rax,%rcx)
.LBB14_19:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incq	%r13
	movslq	512(%rbx), %rax
	cmpq	%rax, %r13
	jl	.LBB14_32
.LBB14_20:                              # %._crit_edge
	cmpl	%edx, %eax
	jle	.LBB14_22
# BB#21:                                # %.lr.ph.i
	movl	%edx, 512(%rbx)
.LBB14_22:                              # %_ZN3vecI3LitE6shrinkEi.exit
	xorl	%edx, %edx
.LBB14_10:                              # %_ZN3vecI3LitE6shrinkEi.exit
	movl	%edx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_9:
	movb	$1, %dl
	jmp	.LBB14_10
.Lfunc_end14:
	.size	_ZN6Solver12litRedundantE3Litj, .Lfunc_end14-_ZN6Solver12litRedundantE3Litj
	.cfi_endproc

	.globl	_ZN6Solver12analyzeFinalE3LitR3vecIS0_E
	.p2align	4, 0x90
	.type	_ZN6Solver12analyzeFinalE3LitR3vecIS0_E,@function
_ZN6Solver12analyzeFinalE3LitR3vecIS0_E: # @_ZN6Solver12analyzeFinalE3LitR3vecIS0_E
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi113:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi114:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi116:
	.cfi_def_cfa_offset 64
.Lcfi117:
	.cfi_offset %rbx, -56
.Lcfi118:
	.cfi_offset %r12, -48
.Lcfi119:
	.cfi_offset %r13, -40
.Lcfi120:
	.cfi_offset %r14, -32
.Lcfi121:
	.cfi_offset %r15, -24
.Lcfi122:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.LBB15_1
# BB#2:                                 # %.preheader.i
	movl	$0, 8(%r14)
	xorl	%ecx, %ecx
	cmpl	12(%r14), %ecx
	je	.LBB15_4
	jmp	.LBB15_5
.LBB15_1:                               # %._ZN3vecI3LitE5clearEb.exit_crit_edge
	movl	8(%r14), %ecx
	cmpl	12(%r14), %ecx
	jne	.LBB15_5
.LBB15_4:
	leal	1(%rcx,%rcx,2), %ecx
	sarl	%ecx
	leal	-2(%rcx), %edx
	sarl	$31, %edx
	movl	%edx, %esi
	andl	$2, %esi
	notl	%edx
	andl	%ecx, %edx
	addl	%esi, %edx
	movl	%edx, 12(%r14)
	movslq	%edx, %rsi
	shlq	$2, %rsi
	movq	%rax, %rdi
	callq	realloc
	movq	%rax, (%r14)
	movl	8(%r14), %ecx
.LBB15_5:                               # %_ZN3vecI3LitE4pushERKS0_.exit35
	leal	1(%rcx), %edx
	movl	%edx, 8(%r14)
	movslq	%ecx, %rcx
	movl	%ebp, (%rax,%rcx,4)
	cmpl	$0, 336(%rbx)
	je	.LBB15_8
# BB#6:
	sarl	%ebp
	movq	472(%rbx), %rax
	movslq	%ebp, %r15
	movb	$1, (%rax,%r15)
	movslq	320(%rbx), %r13
	movq	328(%rbx), %rax
	movl	(%rax), %eax
	cmpl	%eax, %r13d
	jle	.LBB15_7
	.p2align	4, 0x90
.LBB15_9:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_14 Depth 2
	movq	312(%rbx), %rcx
	movq	472(%rbx), %rdx
	movl	-4(%rcx,%r13,4), %ebp
	decq	%r13
	movl	%ebp, %ecx
	sarl	%ecx
	movslq	%ecx, %r12
	cmpb	$0, (%rdx,%r12)
	je	.LBB15_10
# BB#11:                                #   in Loop: Header=BB15_9 Depth=1
	movq	344(%rbx), %rax
	movq	(%rax,%r12,8), %rax
	testq	%rax, %rax
	je	.LBB15_17
# BB#12:                                # %.preheader
                                        #   in Loop: Header=BB15_9 Depth=1
	movl	(%rax), %ecx
	cmpl	$16, %ecx
	jb	.LBB15_21
# BB#13:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB15_9 Depth=1
	movl	$1, %edx
	.p2align	4, 0x90
.LBB15_14:                              # %.lr.ph
                                        #   Parent Loop BB15_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%rax,%rdx,4), %esi
	sarl	%esi
	movq	360(%rbx), %rdi
	movslq	%esi, %rsi
	cmpl	$0, (%rdi,%rsi,4)
	jle	.LBB15_16
# BB#15:                                #   in Loop: Header=BB15_14 Depth=2
	movq	472(%rbx), %rcx
	movb	$1, (%rcx,%rsi)
	movl	(%rax), %ecx
.LBB15_16:                              #   in Loop: Header=BB15_14 Depth=2
	incq	%rdx
	movl	%ecx, %esi
	shrl	$3, %esi
	cmpq	%rsi, %rdx
	jl	.LBB15_14
	jmp	.LBB15_21
	.p2align	4, 0x90
.LBB15_17:                              #   in Loop: Header=BB15_9 Depth=1
	xorl	$1, %ebp
	movl	8(%r14), %ecx
	cmpl	12(%r14), %ecx
	jne	.LBB15_18
# BB#19:                                #   in Loop: Header=BB15_9 Depth=1
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 12(%r14)
	movq	(%r14), %rdi
	movslq	%ecx, %rsi
	shlq	$2, %rsi
	callq	realloc
	movq	%rax, (%r14)
	movl	8(%r14), %ecx
	jmp	.LBB15_20
.LBB15_18:                              # %._crit_edge.i
                                        #   in Loop: Header=BB15_9 Depth=1
	movq	(%r14), %rax
.LBB15_20:                              # %_ZN3vecI3LitE4pushERKS0_.exit
                                        #   in Loop: Header=BB15_9 Depth=1
	leal	1(%rcx), %edx
	movl	%edx, 8(%r14)
	movslq	%ecx, %rcx
	movl	%ebp, (%rax,%rcx,4)
.LBB15_21:                              # %.loopexit
                                        #   in Loop: Header=BB15_9 Depth=1
	movq	472(%rbx), %rax
	movb	$0, (%rax,%r12)
	movq	328(%rbx), %rax
	movl	(%rax), %eax
.LBB15_10:                              # %.backedge
                                        #   in Loop: Header=BB15_9 Depth=1
	movslq	%eax, %rcx
	cmpq	%rcx, %r13
	jg	.LBB15_9
.LBB15_7:                               # %._crit_edge
	movq	472(%rbx), %rax
	movb	$0, (%rax,%r15)
.LBB15_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	_ZN6Solver12analyzeFinalE3LitR3vecIS0_E, .Lfunc_end15-_ZN6Solver12analyzeFinalE3LitR3vecIS0_E
	.cfi_endproc

	.globl	_ZN6Solver8reduceDBEv
	.p2align	4, 0x90
	.type	_ZN6Solver8reduceDBEv,@function
_ZN6Solver8reduceDBEv:                  # @_ZN6Solver8reduceDBEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi124:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi125:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi126:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi127:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi128:
	.cfi_def_cfa_offset 64
.Lcfi129:
	.cfi_offset %rbx, -48
.Lcfi130:
	.cfi_offset %r12, -40
.Lcfi131:
	.cfi_offset %r14, -32
.Lcfi132:
	.cfi_offset %r15, -24
.Lcfi133:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movsd	216(%r15), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	208(%r15), %r14d
	movq	200(%r15), %rdi
	movl	%r14d, %esi
	callq	_Z4sortIP6Clause11reduceDB_ltEvPT_iT0_
	movl	208(%r15), %eax
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	cmpl	$2, %eax
	jl	.LBB16_1
# BB#10:                                # %.lr.ph35
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB16_11:                              # =>This Inner Loop Header: Depth=1
	movq	200(%r15), %rcx
	movq	(%rcx,%rbx,8), %rbp
	cmpl	$24, (%rbp)
	jb	.LBB16_17
# BB#12:                                #   in Loop: Header=BB16_11 Depth=1
	movl	8(%rbp), %esi
	movl	%esi, %edx
	sarl	%edx
	movq	344(%r15), %rdi
	movslq	%edx, %rdx
	cmpq	%rbp, (%rdi,%rdx,8)
	jne	.LBB16_16
# BB#13:                                # %_ZNK6Solver6lockedERK6Clause.exit27
                                        #   in Loop: Header=BB16_11 Depth=1
	movq	264(%r15), %rdi
	movzbl	(%rdi,%rdx), %edx
	testb	$1, %sil
	je	.LBB16_15
# BB#14:                                #   in Loop: Header=BB16_11 Depth=1
	negb	%dl
.LBB16_15:                              # %_ZNK6Solver6lockedERK6Clause.exit27
                                        #   in Loop: Header=BB16_11 Depth=1
	cmpb	$1, %dl
	jne	.LBB16_16
	.p2align	4, 0x90
.LBB16_17:                              #   in Loop: Header=BB16_11 Depth=1
	movslq	%r12d, %rdx
	incl	%r12d
	movq	%rbp, (%rcx,%rdx,8)
	jmp	.LBB16_18
	.p2align	4, 0x90
.LBB16_16:                              # %_ZNK6Solver6lockedERK6Clause.exit27.thread
                                        #   in Loop: Header=BB16_11 Depth=1
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	_ZN6Solver12detachClauseER6Clause
	movq	%rbp, %rdi
	callq	free
	movl	208(%r15), %eax
.LBB16_18:                              #   in Loop: Header=BB16_11 Depth=1
	incq	%rbx
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB16_11
.LBB16_1:                               # %.preheader
	cmpl	%eax, %ebx
	jge	.LBB16_21
# BB#2:                                 # %.lr.ph
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r14d, %xmm0
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movslq	%ebx, %rbx
	.p2align	4, 0x90
.LBB16_3:                               # =>This Inner Loop Header: Depth=1
	movq	200(%r15), %rcx
	movq	(%rcx,%rbx,8), %rbp
	cmpl	$24, (%rbp)
	jb	.LBB16_19
# BB#4:                                 #   in Loop: Header=BB16_3 Depth=1
	movl	8(%rbp), %esi
	movl	%esi, %edx
	sarl	%edx
	movq	344(%r15), %rdi
	movslq	%edx, %rdx
	cmpq	%rbp, (%rdi,%rdx,8)
	jne	.LBB16_8
# BB#5:                                 # %_ZNK6Solver6lockedERK6Clause.exit
                                        #   in Loop: Header=BB16_3 Depth=1
	movq	264(%r15), %rdi
	movzbl	(%rdi,%rdx), %edx
	testb	$1, %sil
	je	.LBB16_7
# BB#6:                                 #   in Loop: Header=BB16_3 Depth=1
	negb	%dl
.LBB16_7:                               # %_ZNK6Solver6lockedERK6Clause.exit
                                        #   in Loop: Header=BB16_3 Depth=1
	cmpb	$1, %dl
	je	.LBB16_19
.LBB16_8:                               # %_ZNK6Solver6lockedERK6Clause.exit.thread
                                        #   in Loop: Header=BB16_3 Depth=1
	movss	4(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB16_19
# BB#9:                                 #   in Loop: Header=BB16_3 Depth=1
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	_ZN6Solver12detachClauseER6Clause
	movq	%rbp, %rdi
	callq	free
	movl	208(%r15), %eax
	jmp	.LBB16_20
	.p2align	4, 0x90
.LBB16_19:                              #   in Loop: Header=BB16_3 Depth=1
	movslq	%r12d, %rdx
	incl	%r12d
	movq	%rbp, (%rcx,%rdx,8)
.LBB16_20:                              #   in Loop: Header=BB16_3 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB16_3
.LBB16_21:                              # %._crit_edge
	subl	%r12d, %ebx
	jle	.LBB16_23
# BB#22:                                # %.lr.ph.i
	subl	%ebx, %eax
	movl	%eax, 208(%r15)
.LBB16_23:                              # %_ZN3vecIP6ClauseE6shrinkEi.exit
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	_ZN6Solver8reduceDBEv, .Lfunc_end16-_ZN6Solver8reduceDBEv
	.cfi_endproc

	.globl	_ZN6Solver15removeSatisfiedER3vecIP6ClauseE
	.p2align	4, 0x90
	.type	_ZN6Solver15removeSatisfiedER3vecIP6ClauseE,@function
_ZN6Solver15removeSatisfiedER3vecIP6ClauseE: # @_ZN6Solver15removeSatisfiedER3vecIP6ClauseE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi134:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi135:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi136:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi137:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi138:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi139:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi140:
	.cfi_def_cfa_offset 64
.Lcfi141:
	.cfi_offset %rbx, -56
.Lcfi142:
	.cfi_offset %r12, -48
.Lcfi143:
	.cfi_offset %r13, -40
.Lcfi144:
	.cfi_offset %r14, -32
.Lcfi145:
	.cfi_offset %r15, -24
.Lcfi146:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	8(%r15), %r8d
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	testl	%r8d, %r8d
	jle	.LBB17_10
	.p2align	4, 0x90
.LBB17_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_4 Depth 2
	movq	(%r15), %rcx
	movq	(%rcx,%r12,8), %rbx
	movl	(%rbx), %edx
	cmpl	$8, %edx
	jb	.LBB17_8
# BB#2:                                 # %.lr.ph.i14
                                        #   in Loop: Header=BB17_1 Depth=1
	movq	264(%r14), %rsi
	shrl	$3, %edx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB17_4:                               #   Parent Loop BB17_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%rbx,%rdi,4), %ebp
	movl	%ebp, %eax
	sarl	%eax
	cltq
	movzbl	(%rsi,%rax), %eax
	testb	$1, %bpl
	je	.LBB17_6
# BB#5:                                 #   in Loop: Header=BB17_4 Depth=2
	negb	%al
.LBB17_6:                               #   in Loop: Header=BB17_4 Depth=2
	cmpb	$1, %al
	je	.LBB17_7
# BB#3:                                 #   in Loop: Header=BB17_4 Depth=2
	incq	%rdi
	cmpq	%rdx, %rdi
	jl	.LBB17_4
.LBB17_8:                               # %.loopexit
                                        #   in Loop: Header=BB17_1 Depth=1
	movslq	%r13d, %rax
	incl	%r13d
	movq	%rbx, (%rcx,%rax,8)
	jmp	.LBB17_9
	.p2align	4, 0x90
.LBB17_7:                               #   in Loop: Header=BB17_1 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN6Solver12detachClauseER6Clause
	movq	%rbx, %rdi
	callq	free
	movl	8(%r15), %r8d
.LBB17_9:                               #   in Loop: Header=BB17_1 Depth=1
	incq	%r12
	movslq	%r8d, %rax
	cmpq	%rax, %r12
	jl	.LBB17_1
.LBB17_10:                              # %._crit_edge
	subl	%r13d, %r12d
	jle	.LBB17_12
# BB#11:                                # %.lr.ph.i
	subl	%r12d, %r8d
	movl	%r8d, 8(%r15)
.LBB17_12:                              # %_ZN3vecIP6ClauseE6shrinkEi.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	_ZN6Solver15removeSatisfiedER3vecIP6ClauseE, .Lfunc_end17-_ZN6Solver15removeSatisfiedER3vecIP6ClauseE
	.cfi_endproc

	.globl	_ZN6Solver8simplifyEv
	.p2align	4, 0x90
	.type	_ZN6Solver8simplifyEv,@function
_ZN6Solver8simplifyEv:                  # @_ZN6Solver8simplifyEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi147:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi148:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi149:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi150:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi151:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi152:
	.cfi_def_cfa_offset 64
.Lcfi153:
	.cfi_offset %rbx, -48
.Lcfi154:
	.cfi_offset %r12, -40
.Lcfi155:
	.cfi_offset %r14, -32
.Lcfi156:
	.cfi_offset %r15, -24
.Lcfi157:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	cmpb	$0, 176(%r15)
	je	.LBB18_2
# BB#1:
	movq	%r15, %rdi
	callq	_ZN6Solver9propagateEv
	testq	%rax, %rax
	je	.LBB18_3
.LBB18_2:
	movb	$0, 176(%r15)
	xorl	%r14d, %r14d
.LBB18_31:
	movl	%r14d, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_3:
	movl	320(%r15), %eax
	movb	$1, %r14b
	cmpl	380(%r15), %eax
	je	.LBB18_31
# BB#4:
	cmpq	$0, 384(%r15)
	jg	.LBB18_31
# BB#5:
	movl	208(%r15), %r8d
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	testl	%r8d, %r8d
	jle	.LBB18_15
	.p2align	4, 0x90
.LBB18_6:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_9 Depth 2
	movq	200(%r15), %r9
	movq	(%r9,%rbx,8), %rbp
	movl	(%rbp), %edx
	cmpl	$8, %edx
	jb	.LBB18_13
# BB#7:                                 # %.lr.ph.i14.i
                                        #   in Loop: Header=BB18_6 Depth=1
	movq	264(%r15), %rsi
	shrl	$3, %edx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB18_9:                               #   Parent Loop BB18_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%rbp,%rdi,4), %ecx
	movl	%ecx, %eax
	sarl	%eax
	cltq
	movzbl	(%rsi,%rax), %eax
	testb	$1, %cl
	je	.LBB18_11
# BB#10:                                #   in Loop: Header=BB18_9 Depth=2
	negb	%al
.LBB18_11:                              #   in Loop: Header=BB18_9 Depth=2
	cmpb	$1, %al
	je	.LBB18_12
# BB#8:                                 #   in Loop: Header=BB18_9 Depth=2
	incq	%rdi
	cmpq	%rdx, %rdi
	jl	.LBB18_9
.LBB18_13:                              # %.loopexit.i
                                        #   in Loop: Header=BB18_6 Depth=1
	movslq	%r12d, %rax
	incl	%r12d
	movq	%rbp, (%r9,%rax,8)
	jmp	.LBB18_14
	.p2align	4, 0x90
.LBB18_12:                              #   in Loop: Header=BB18_6 Depth=1
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	_ZN6Solver12detachClauseER6Clause
	movq	%rbp, %rdi
	callq	free
	movl	208(%r15), %r8d
.LBB18_14:                              #   in Loop: Header=BB18_6 Depth=1
	incq	%rbx
	movslq	%r8d, %rax
	cmpq	%rax, %rbx
	jl	.LBB18_6
.LBB18_15:                              # %._crit_edge.i
	subl	%r12d, %ebx
	jle	.LBB18_17
# BB#16:                                # %.lr.ph.i.i
	subl	%ebx, %r8d
	movl	%r8d, 208(%r15)
.LBB18_17:                              # %_ZN6Solver15removeSatisfiedER3vecIP6ClauseE.exit
	cmpb	$0, 464(%r15)
	je	.LBB18_30
# BB#18:
	movl	192(%r15), %r8d
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	testl	%r8d, %r8d
	jle	.LBB18_28
	.p2align	4, 0x90
.LBB18_19:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_22 Depth 2
	movq	184(%r15), %r9
	movq	(%r9,%rbx,8), %rbp
	movl	(%rbp), %edx
	cmpl	$8, %edx
	jb	.LBB18_26
# BB#20:                                # %.lr.ph.i14.i5
                                        #   in Loop: Header=BB18_19 Depth=1
	movq	264(%r15), %rsi
	shrl	$3, %edx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB18_22:                              #   Parent Loop BB18_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%rbp,%rdi,4), %ecx
	movl	%ecx, %eax
	sarl	%eax
	cltq
	movzbl	(%rsi,%rax), %eax
	testb	$1, %cl
	je	.LBB18_24
# BB#23:                                #   in Loop: Header=BB18_22 Depth=2
	negb	%al
.LBB18_24:                              #   in Loop: Header=BB18_22 Depth=2
	cmpb	$1, %al
	je	.LBB18_25
# BB#21:                                #   in Loop: Header=BB18_22 Depth=2
	incq	%rdi
	cmpq	%rdx, %rdi
	jl	.LBB18_22
.LBB18_26:                              # %.loopexit.i12
                                        #   in Loop: Header=BB18_19 Depth=1
	movslq	%r12d, %rax
	incl	%r12d
	movq	%rbp, (%r9,%rax,8)
	jmp	.LBB18_27
.LBB18_25:                              #   in Loop: Header=BB18_19 Depth=1
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	_ZN6Solver12detachClauseER6Clause
	movq	%rbp, %rdi
	callq	free
	movl	192(%r15), %r8d
.LBB18_27:                              #   in Loop: Header=BB18_19 Depth=1
	incq	%rbx
	movslq	%r8d, %rax
	cmpq	%rax, %rbx
	jl	.LBB18_19
.LBB18_28:                              # %._crit_edge.i19
	subl	%r12d, %ebx
	jle	.LBB18_30
# BB#29:                                # %.lr.ph.i.i20
	subl	%ebx, %r8d
	movl	%r8d, 192(%r15)
.LBB18_30:                              # %_ZN6Solver15removeSatisfiedER3vecIP6ClauseE.exit21
	leaq	408(%r15), %rdi
	movq	%r15, 8(%rsp)
	leaq	8(%rsp), %rsi
	callq	_ZN4HeapIN6Solver10VarOrderLtEE6filterINS0_9VarFilterEEEvRKT_
	movl	320(%r15), %eax
	movl	%eax, 380(%r15)
	movq	152(%r15), %rax
	addq	144(%r15), %rax
	movq	%rax, 384(%r15)
	jmp	.LBB18_31
.Lfunc_end18:
	.size	_ZN6Solver8simplifyEv, .Lfunc_end18-_ZN6Solver8simplifyEv
	.cfi_endproc

	.section	.text._ZN4HeapIN6Solver10VarOrderLtEE6filterINS0_9VarFilterEEEvRKT_,"axG",@progbits,_ZN4HeapIN6Solver10VarOrderLtEE6filterINS0_9VarFilterEEEvRKT_,comdat
	.weak	_ZN4HeapIN6Solver10VarOrderLtEE6filterINS0_9VarFilterEEEvRKT_
	.p2align	4, 0x90
	.type	_ZN4HeapIN6Solver10VarOrderLtEE6filterINS0_9VarFilterEEEvRKT_,@function
_ZN4HeapIN6Solver10VarOrderLtEE6filterINS0_9VarFilterEEEvRKT_: # @_ZN4HeapIN6Solver10VarOrderLtEE6filterINS0_9VarFilterEEEvRKT_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi158:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi160:
	.cfi_def_cfa_offset 32
.Lcfi161:
	.cfi_offset %rbx, -32
.Lcfi162:
	.cfi_offset %r14, -24
.Lcfi163:
	.cfi_offset %rbp, -16
	movl	16(%rdi), %edx
	testl	%edx, %edx
	jle	.LBB19_1
# BB#2:                                 # %.lr.ph33
	movq	8(%rdi), %r8
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB19_3:                               # =>This Inner Loop Header: Depth=1
	movl	%ebx, %r9d
	movl	(%r8,%rax,4), %ecx
	movq	(%rsi), %rbp
	movq	264(%rbp), %rdx
	movslq	%ecx, %rbx
	cmpb	$0, (%rdx,%rbx)
	je	.LBB19_5
.LBB19_4:                               #   in Loop: Header=BB19_3 Depth=1
	movl	%r9d, %ebx
	movl	$-1, %r9d
	jmp	.LBB19_7
	.p2align	4, 0x90
.LBB19_5:                               # %_ZNK6Solver9VarFilterclEi.exit
                                        #   in Loop: Header=BB19_3 Depth=1
	movq	296(%rbp), %rdx
	cmpb	$0, (%rdx,%rbx)
	je	.LBB19_4
# BB#6:                                 #   in Loop: Header=BB19_3 Depth=1
	movslq	%r9d, %rbx
	movl	%ecx, (%r8,%rbx,4)
	incl	%ebx
	movl	(%r8,%rax,4), %ecx
.LBB19_7:                               # %.sink.split
                                        #   in Loop: Header=BB19_3 Depth=1
	movq	24(%rdi), %rdx
	movslq	%ecx, %rcx
	movl	%r9d, (%rdx,%rcx,4)
	incq	%rax
	movslq	16(%rdi), %rdx
	cmpq	%rdx, %rax
	jl	.LBB19_3
	jmp	.LBB19_8
.LBB19_1:
	xorl	%eax, %eax
	xorl	%ebx, %ebx
.LBB19_8:                               # %._crit_edge34
	subl	%ebx, %eax
	jle	.LBB19_10
# BB#9:                                 # %.lr.ph.i25
	subl	%eax, %edx
	movl	%edx, 16(%rdi)
.LBB19_10:                              # %_ZN3vecIiE6shrinkEi.exit
	cmpl	$2, %edx
	jl	.LBB19_24
# BB#11:                                # %.lr.ph
	movl	%edx, %r9d
	shrl	%r9d
	movq	8(%rdi), %r14
	movq	24(%rdi), %r8
	jmp	.LBB19_12
	.p2align	4, 0x90
.LBB19_13:                              #   in Loop: Header=BB19_12 Depth=1
	movl	%r10d, %ebx
	jmp	.LBB19_22
	.p2align	4, 0x90
.LBB19_20:                              #   in Loop: Header=BB19_12 Depth=1
	movl	%ebp, %ebx
.LBB19_22:                              # %_ZN4HeapIN6Solver10VarOrderLtEE13percolateDownEi.exit
                                        #   in Loop: Header=BB19_12 Depth=1
	movslq	%ebx, %rax
	movl	%r11d, (%r14,%rax,4)
	movl	%eax, (%r8,%r11,4)
	cmpq	$2, %r9
	jl	.LBB19_24
# BB#23:                                # %_ZN4HeapIN6Solver10VarOrderLtEE13percolateDownEi.exit._crit_edge
                                        #   in Loop: Header=BB19_12 Depth=1
	movl	16(%rdi), %edx
	movq	%r10, %r9
.LBB19_12:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_15 Depth 2
	leaq	-1(%r9), %r10
	movl	-4(%r14,%r9,4), %eax
	leal	1(%r10,%r10), %esi
	movslq	%eax, %r11
	cmpl	%edx, %esi
	jge	.LBB19_13
# BB#14:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB19_12 Depth=1
	leal	(%r10,%r10), %eax
	movl	%r10d, %ebp
	.p2align	4, 0x90
.LBB19_15:                              # %.lr.ph.i
                                        #   Parent Loop BB19_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebx
	addl	$2, %ebx
	cmpl	%edx, %ebx
	jge	.LBB19_16
# BB#17:                                #   in Loop: Header=BB19_15 Depth=2
	movslq	%ebx, %rax
	movslq	(%r14,%rax,4), %rcx
	movslq	%esi, %rax
	movslq	(%r14,%rax,4), %rax
	movq	(%rdi), %rdx
	movq	(%rdx), %rdx
	movsd	(%rdx,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rdx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB19_18
	jmp	.LBB19_19
	.p2align	4, 0x90
.LBB19_16:                              # %._crit_edge27.i
                                        #   in Loop: Header=BB19_15 Depth=2
	movq	(%rdi), %rax
	movq	(%rax), %rdx
	movslq	%esi, %rax
	movslq	(%r14,%rax,4), %rax
	movsd	(%rdx,%rax,8), %xmm1    # xmm1 = mem[0],zero
.LBB19_18:                              #   in Loop: Header=BB19_15 Depth=2
	movaps	%xmm1, %xmm0
	movl	%eax, %ecx
	movl	%esi, %ebx
.LBB19_19:                              #   in Loop: Header=BB19_15 Depth=2
	ucomisd	(%rdx,%r11,8), %xmm0
	jbe	.LBB19_20
# BB#21:                                # %.thread.i
                                        #   in Loop: Header=BB19_15 Depth=2
	movslq	%ecx, %rax
	movslq	%ebp, %rcx
	movl	%eax, (%r14,%rcx,4)
	movq	24(%rdi), %rdx
	movl	%ecx, (%rdx,%rax,4)
	leal	(%rbx,%rbx), %eax
	leal	1(%rbx,%rbx), %esi
	movl	16(%rdi), %edx
	cmpl	%edx, %esi
	movl	%ebx, %ebp
	jl	.LBB19_15
	jmp	.LBB19_22
.LBB19_24:                              # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end19:
	.size	_ZN4HeapIN6Solver10VarOrderLtEE6filterINS0_9VarFilterEEEvRKT_, .Lfunc_end19-_ZN4HeapIN6Solver10VarOrderLtEE6filterINS0_9VarFilterEEEvRKT_
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI20_0:
	.quad	4906019910204099648     # double 1.0E+20
.LCPI20_1:
	.quad	4307583784117748259     # double 9.9999999999999995E-21
.LCPI20_2:
	.quad	4607182418800017408     # double 1
	.text
	.globl	_ZN6Solver6searchEii
	.p2align	4, 0x90
	.type	_ZN6Solver6searchEii,@function
_ZN6Solver6searchEii:                   # @_ZN6Solver6searchEii
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi164:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi165:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi166:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi167:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi168:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi169:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi170:
	.cfi_def_cfa_offset 128
.Lcfi171:
	.cfi_offset %rbx, -56
.Lcfi172:
	.cfi_offset %r12, -48
.Lcfi173:
	.cfi_offset %r13, -40
.Lcfi174:
	.cfi_offset %r14, -32
.Lcfi175:
	.cfi_offset %r15, -24
.Lcfi176:
	.cfi_offset %rbp, -16
	movl	%edx, 48(%rsp)          # 4-byte Spill
	movq	%rdi, %rbx
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 16(%rsp)
	incq	104(%rbx)
	leaq	320(%rbx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	16(%rbx), %r15
	xorl	%ecx, %ecx
	movl	%esi, 12(%rsp)          # 4-byte Spill
	jmp	.LBB20_2
	.p2align	4, 0x90
.LBB20_1:                               # %_ZN6Solver16uncheckedEnqueueE3LitP6Clause.exit64
                                        #   in Loop: Header=BB20_2 Depth=1
	movl	12(%rsp), %esi          # 4-byte Reload
	movq	56(%rsp), %rdi          # 8-byte Reload
	incl	%edi
	leal	1(%rcx), %edx
	movl	%edx, 320(%rbx)
	movslq	%ecx, %rcx
	movl	%r13d, (%rax,%rcx,4)
	movq	%rdi, %rcx
	movsd	32(%rbx), %xmm0         # xmm0 = mem[0],zero
	mulsd	240(%rbx), %xmm0
	movsd	%xmm0, 240(%rbx)
	movsd	40(%rbx), %xmm0         # xmm0 = mem[0],zero
	mulsd	216(%rbx), %xmm0
	movsd	%xmm0, 216(%rbx)
.LBB20_2:                               # %_ZN6Solver11cancelUntilEi.exit.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_16 Depth 2
                                        #       Child Loop BB20_26 Depth 3
                                        #     Child Loop BB20_56 Depth 2
	testl	%esi, %esi
	sets	%al
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	cmpl	%esi, %ecx
	setl	%r13b
	orb	%al, %r13b
	jmp	.LBB20_16
	.p2align	4, 0x90
.LBB20_5:                               #   in Loop: Header=BB20_16 Depth=2
	movl	%r14d, %r12d
.LBB20_6:                               #   in Loop: Header=BB20_16 Depth=2
	cmpl	$-2, %r12d
	jne	.LBB20_9
# BB#7:                                 #   in Loop: Header=BB20_16 Depth=2
	incq	112(%rbx)
	movl	92(%rbx), %esi
	movsd	48(%rbx), %xmm0         # xmm0 = mem[0],zero
.Ltmp22:
	movq	%rbx, %rdi
	callq	_ZN6Solver13pickBranchLitEid
	movl	%eax, %r12d
.Ltmp23:
# BB#8:                                 #   in Loop: Header=BB20_16 Depth=2
	cmpl	$-2, %r12d
	je	.LBB20_84
.LBB20_9:                               #   in Loop: Header=BB20_16 Depth=2
	movl	320(%rbx), %ebp
	movl	336(%rbx), %ecx
	cmpl	340(%rbx), %ecx
	jne	.LBB20_11
# BB#10:                                #   in Loop: Header=BB20_16 Depth=2
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 340(%rbx)
	movq	328(%rbx), %rdi
	movslq	%ecx, %rsi
	shlq	$2, %rsi
	callq	realloc
	movq	%rax, 328(%rbx)
	movl	336(%rbx), %ecx
	jmp	.LBB20_12
	.p2align	4, 0x90
.LBB20_11:                              # %._crit_edge.i.i58
                                        #   in Loop: Header=BB20_16 Depth=2
	movq	328(%rbx), %rax
.LBB20_12:                              #   in Loop: Header=BB20_16 Depth=2
	leal	1(%rcx), %edx
	movl	%edx, 336(%rbx)
	movslq	%ecx, %rcx
	movl	%ebp, (%rax,%rcx,4)
	movl	%r12d, %eax
	addb	%al, %al
	notb	%al
	andb	$2, %al
	decb	%al
	movl	%r12d, %ecx
	sarl	%ecx
	movq	264(%rbx), %rdx
	movslq	%ecx, %rcx
	movb	%al, (%rdx,%rcx)
	movl	336(%rbx), %eax
	movq	360(%rbx), %rdx
	movl	%eax, (%rdx,%rcx,4)
	movq	344(%rbx), %rax
	movq	$0, (%rax,%rcx,8)
	movl	320(%rbx), %ecx
	cmpl	324(%rbx), %ecx
	jne	.LBB20_14
# BB#13:                                #   in Loop: Header=BB20_16 Depth=2
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 324(%rbx)
	movq	312(%rbx), %rdi
	movslq	%ecx, %rsi
	shlq	$2, %rsi
	callq	realloc
	movq	%rax, 312(%rbx)
	movl	320(%rbx), %ecx
	jmp	.LBB20_15
	.p2align	4, 0x90
.LBB20_14:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB20_16 Depth=2
	movq	312(%rbx), %rax
.LBB20_15:                              # %.thread
                                        #   in Loop: Header=BB20_16 Depth=2
	leal	1(%rcx), %edx
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	%edx, (%rsi)
	movslq	%ecx, %rcx
	movl	%r12d, (%rax,%rcx,4)
	jmp	.LBB20_16
	.p2align	4, 0x90
.LBB20_3:                               #   in Loop: Header=BB20_16 Depth=2
	cmpb	$5, %al
	je	.LBB20_6
# BB#4:                                 #   in Loop: Header=BB20_16 Depth=2
	testl	%ebp, %ebp
	jne	.LBB20_82
.LBB20_16:                              # %_ZN6Solver11cancelUntilEi.exit
                                        #   Parent Loop BB20_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB20_26 Depth 3
.Ltmp0:
	movq	%rbx, %rdi
	callq	_ZN6Solver9propagateEv
.Ltmp1:
# BB#17:                                #   in Loop: Header=BB20_16 Depth=2
	testq	%rax, %rax
	jne	.LBB20_37
# BB#18:                                #   in Loop: Header=BB20_16 Depth=2
	testb	%r13b, %r13b
	je	.LBB20_62
# BB#19:                                #   in Loop: Header=BB20_16 Depth=2
	cmpl	$0, 336(%rbx)
	jne	.LBB20_22
# BB#20:                                #   in Loop: Header=BB20_16 Depth=2
.Ltmp14:
	movq	%rbx, %rdi
	callq	_ZN6Solver8simplifyEv
.Ltmp15:
# BB#21:                                #   in Loop: Header=BB20_16 Depth=2
	testb	%al, %al
	je	.LBB20_82
.LBB20_22:                              #   in Loop: Header=BB20_16 Depth=2
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	js	.LBB20_25
# BB#23:                                #   in Loop: Header=BB20_16 Depth=2
	movl	208(%rbx), %eax
	subl	320(%rbx), %eax
	cmpl	48(%rsp), %eax          # 4-byte Folded Reload
	jl	.LBB20_25
# BB#24:                                #   in Loop: Header=BB20_16 Depth=2
.Ltmp16:
	movq	%rbx, %rdi
	callq	_ZN6Solver8reduceDBEv
.Ltmp17:
.LBB20_25:                              # %.preheader.preheader
                                        #   in Loop: Header=BB20_16 Depth=2
	movl	$-2, %r12d
	.p2align	4, 0x90
.LBB20_26:                              # %.preheader
                                        #   Parent Loop BB20_2 Depth=1
                                        #     Parent Loop BB20_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r12d, %r14d
	movl	336(%rbx), %ecx
	cmpl	400(%rbx), %ecx
	jge	.LBB20_5
# BB#27:                                #   in Loop: Header=BB20_26 Depth=3
	movslq	%ecx, %rax
	movq	264(%rbx), %rdx
	movq	392(%rbx), %rsi
	movl	(%rsi,%rax,4), %r12d
	movl	%r12d, %eax
	sarl	%eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	$1, %r12b
	je	.LBB20_29
# BB#28:                                #   in Loop: Header=BB20_26 Depth=3
	negb	%al
.LBB20_29:                              #   in Loop: Header=BB20_26 Depth=3
	cmpb	$-1, %al
	je	.LBB20_33
# BB#30:                                #   in Loop: Header=BB20_26 Depth=3
	movl	$5, %ebp
	cmpb	$1, %al
	jne	.LBB20_36
# BB#31:                                #   in Loop: Header=BB20_26 Depth=3
	movl	320(%rbx), %ebp
	cmpl	340(%rbx), %ecx
	jne	.LBB20_34
# BB#32:                                #   in Loop: Header=BB20_26 Depth=3
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 340(%rbx)
	movq	328(%rbx), %rdi
	movslq	%ecx, %rsi
	shlq	$2, %rsi
	callq	realloc
	movq	%rax, 328(%rbx)
	movl	336(%rbx), %ecx
	jmp	.LBB20_35
	.p2align	4, 0x90
.LBB20_33:                              #   in Loop: Header=BB20_26 Depth=3
	xorl	$1, %r12d
	movl	$1, %ebp
.Ltmp19:
	movq	%rbx, %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	callq	_ZN6Solver12analyzeFinalE3LitR3vecIS0_E
.Ltmp20:
	movl	%r14d, %r12d
	jmp	.LBB20_36
.LBB20_34:                              # %._crit_edge.i.i75
                                        #   in Loop: Header=BB20_26 Depth=3
	movq	328(%rbx), %rax
.LBB20_35:                              # %_ZN6Solver16newDecisionLevelEv.exit77
                                        #   in Loop: Header=BB20_26 Depth=3
	leal	1(%rcx), %edx
	movl	%edx, 336(%rbx)
	movslq	%ecx, %rcx
	movl	%ebp, (%rax,%rcx,4)
	xorl	%ebp, %ebp
	movl	%r14d, %r12d
.LBB20_36:                              #   in Loop: Header=BB20_26 Depth=3
	movl	%ebp, %eax
	andb	$7, %al
	je	.LBB20_26
	jmp	.LBB20_3
	.p2align	4, 0x90
.LBB20_37:                              #   in Loop: Header=BB20_2 Depth=1
	incq	136(%rbx)
	cmpl	$0, 336(%rbx)
	je	.LBB20_82
# BB#38:                                #   in Loop: Header=BB20_2 Depth=1
	cmpq	$0, 16(%rsp)
	je	.LBB20_40
# BB#39:                                # %.preheader.i
                                        #   in Loop: Header=BB20_2 Depth=1
	movl	$0, 24(%rsp)
.LBB20_40:                              # %_ZN3vecI3LitE5clearEb.exit
                                        #   in Loop: Header=BB20_2 Depth=1
.Ltmp3:
	movq	%rbx, %rdi
	movq	%rax, %rsi
	leaq	16(%rsp), %rdx
	leaq	68(%rsp), %rcx
	callq	_ZN6Solver7analyzeEP6ClauseR3vecI3LitERi
.Ltmp4:
# BB#41:                                #   in Loop: Header=BB20_2 Depth=1
	movl	68(%rsp), %esi
.Ltmp5:
	movq	%rbx, %rdi
	callq	_ZN6Solver11cancelUntilEi
.Ltmp6:
# BB#42:                                #   in Loop: Header=BB20_2 Depth=1
	movslq	24(%rsp), %r13
	cmpq	$1, %r13
	jne	.LBB20_44
# BB#43:                                #   in Loop: Header=BB20_2 Depth=1
	movq	16(%rsp), %rax
	movl	(%rax), %r13d
	movl	%r13d, %eax
	addb	%al, %al
	notb	%al
	andb	$2, %al
	decb	%al
	movl	%r13d, %ecx
	sarl	%ecx
	movq	264(%rbx), %rdx
	movslq	%ecx, %rcx
	movb	%al, (%rdx,%rcx)
	movl	336(%rbx), %eax
	movq	360(%rbx), %rdx
	movl	%eax, (%rdx,%rcx,4)
	movq	344(%rbx), %rax
	movq	$0, (%rax,%rcx,8)
	jmp	.LBB20_59
.LBB20_44:                              #   in Loop: Header=BB20_2 Depth=1
	leaq	8(,%r13,4), %rdi
	callq	malloc
	movq	%rax, %r12
	leal	1(,%r13,8), %eax
	movl	%eax, (%r12)
	testl	%r13d, %r13d
	jle	.LBB20_46
# BB#45:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	16(%rsp), %rsi
	movq	%r12, %rdi
	addq	$8, %rdi
	testq	%r13, %r13
	movl	$1, %eax
	cmovleq	%rax, %r13
	shlq	$2, %r13
	movq	%r13, %rdx
	callq	memcpy
.LBB20_46:                              #   in Loop: Header=BB20_2 Depth=1
	movl	$0, 4(%r12)
	movl	208(%rbx), %ecx
	cmpl	212(%rbx), %ecx
	jne	.LBB20_48
# BB#47:                                #   in Loop: Header=BB20_2 Depth=1
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 212(%rbx)
	movq	200(%rbx), %rdi
	movslq	%ecx, %rsi
	shlq	$3, %rsi
	callq	realloc
	movq	%rax, 200(%rbx)
	movl	208(%rbx), %ecx
	jmp	.LBB20_49
.LBB20_48:                              # %._crit_edge.i
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	200(%rbx), %rax
.LBB20_49:                              #   in Loop: Header=BB20_2 Depth=1
	leal	1(%rcx), %edx
	movl	%edx, 208(%rbx)
	movslq	%ecx, %rcx
	movq	%r12, (%rax,%rcx,8)
.Ltmp8:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	_ZN6Solver12attachClauseER6Clause
.Ltmp9:
# BB#50:                                #   in Loop: Header=BB20_2 Depth=1
	movsd	216(%rbx), %xmm0        # xmm0 = mem[0],zero
	movss	4(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, 4(%r12)
	cvtss2sd	%xmm1, %xmm1
	ucomisd	.LCPI20_0(%rip), %xmm1
	movsd	.LCPI20_1(%rip), %xmm2  # xmm2 = mem[0],zero
	jbe	.LBB20_58
# BB#51:                                # %.preheader.i66
                                        #   in Loop: Header=BB20_2 Depth=1
	movslq	208(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB20_57
# BB#52:                                # %.lr.ph.i
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	200(%rbx), %rcx
	testb	$1, %al
	jne	.LBB20_54
# BB#53:                                #   in Loop: Header=BB20_2 Depth=1
	xorl	%edx, %edx
	cmpl	$1, %eax
	jne	.LBB20_55
	jmp	.LBB20_57
.LBB20_54:                              #   in Loop: Header=BB20_2 Depth=1
	movq	(%rcx), %rdx
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm2, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, 4(%rdx)
	movl	$1, %edx
	cmpl	$1, %eax
	je	.LBB20_57
.LBB20_55:                              # %.lr.ph.i.new
                                        #   in Loop: Header=BB20_2 Depth=1
	subq	%rdx, %rax
	leaq	8(%rcx,%rdx,8), %rcx
	.p2align	4, 0x90
.LBB20_56:                              #   Parent Loop BB20_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rcx), %rdx
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm2, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, 4(%rdx)
	movq	(%rcx), %rdx
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm2, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, 4(%rdx)
	addq	$16, %rcx
	addq	$-2, %rax
	jne	.LBB20_56
.LBB20_57:                              # %._crit_edge.i67
                                        #   in Loop: Header=BB20_2 Depth=1
	mulsd	%xmm2, %xmm0
	movsd	%xmm0, 216(%rbx)
.LBB20_58:                              # %_ZN6Solver15claBumpActivityER6Clause.exit
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	16(%rsp), %rax
	movl	(%rax), %r13d
	movl	%r13d, %eax
	addb	%al, %al
	notb	%al
	andb	$2, %al
	decb	%al
	movl	%r13d, %ecx
	sarl	%ecx
	movq	264(%rbx), %rdx
	movslq	%ecx, %rcx
	movb	%al, (%rdx,%rcx)
	movl	336(%rbx), %eax
	movq	360(%rbx), %rdx
	movl	%eax, (%rdx,%rcx,4)
	movq	344(%rbx), %rax
	movq	%r12, (%rax,%rcx,8)
.LBB20_59:                              #   in Loop: Header=BB20_2 Depth=1
	movl	320(%rbx), %ecx
	cmpl	324(%rbx), %ecx
	jne	.LBB20_61
# BB#60:                                #   in Loop: Header=BB20_2 Depth=1
	leal	1(%rcx,%rcx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 324(%rbx)
	movq	312(%rbx), %rdi
	movslq	%ecx, %rsi
	shlq	$2, %rsi
	callq	realloc
	movq	%rax, 312(%rbx)
	movl	320(%rbx), %ecx
	jmp	.LBB20_1
.LBB20_61:                              # %._crit_edge.i.i62
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	312(%rbx), %rax
	jmp	.LBB20_1
.LBB20_62:
	movl	272(%rbx), %ecx
	movl	336(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	testl	%eax, %eax
	js	.LBB20_83
# BB#63:                                # %.lr.ph.i79
	movsd	.LCPI20_2(%rip), %xmm1  # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 56(%rsp)         # 8-byte Spill
	xorpd	%xmm1, %xmm1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB20_64:                              # =>This Inner Loop Header: Depth=1
	testq	%rbp, %rbp
	movl	$0, %r14d
	je	.LBB20_66
# BB#65:                                #   in Loop: Header=BB20_64 Depth=1
	movq	328(%rbx), %rcx
	movl	-4(%rcx,%rbp,4), %r14d
.LBB20_66:                              #   in Loop: Header=BB20_64 Depth=1
	movsd	%xmm1, 48(%rsp)         # 8-byte Spill
	movl	%eax, %eax
	cmpq	%rax, %rbp
	movq	40(%rsp), %rax          # 8-byte Reload
	je	.LBB20_68
# BB#67:                                #   in Loop: Header=BB20_64 Depth=1
	leaq	(,%rbp,4), %rax
	addq	328(%rbx), %rax
.LBB20_68:                              #   in Loop: Header=BB20_64 Depth=1
	movl	(%rax), %r15d
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	movsd	56(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	pow
	subl	%r14d, %r15d
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r15d, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	48(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	movslq	336(%rbx), %rax
	cmpq	%rax, %rbp
	leaq	1(%rbp), %rbp
	jl	.LBB20_64
# BB#69:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	272(%rbx), %xmm0
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 456(%rbx)
	testl	%eax, %eax
	jle	.LBB20_86
# BB#70:
	movl	320(%rbx), %edx
	movq	328(%rbx), %rax
	movl	(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.LBB20_78
# BB#71:                                # %.lr.ph.i84
	movslq	%edx, %rbp
	leaq	408(%rbx), %r14
	.p2align	4, 0x90
.LBB20_72:                              # =>This Inner Loop Header: Depth=1
	movq	264(%rbx), %rcx
	movq	312(%rbx), %rax
	movl	-4(%rax,%rbp,4), %esi
	sarl	%esi
	movslq	%esi, %rax
	movb	$0, (%rcx,%rax)
	cmpl	%eax, 440(%rbx)
	jle	.LBB20_74
# BB#73:                                # %_ZNK4HeapIN6Solver10VarOrderLtEE6inHeapEi.exit.i.i
                                        #   in Loop: Header=BB20_72 Depth=1
	movq	432(%rbx), %rcx
	cmpl	$0, (%rcx,%rax,4)
	jns	.LBB20_76
.LBB20_74:                              # %_ZNK4HeapIN6Solver10VarOrderLtEE6inHeapEi.exit.thread.i.i
                                        #   in Loop: Header=BB20_72 Depth=1
	movq	296(%rbx), %rcx
	cmpb	$0, (%rcx,%rax)
	je	.LBB20_76
# BB#75:                                #   in Loop: Header=BB20_72 Depth=1
.Ltmp11:
	movq	%r14, %rdi
	callq	_ZN4HeapIN6Solver10VarOrderLtEE6insertEi
.Ltmp12:
.LBB20_76:                              # %_ZN6Solver14insertVarOrderEi.exit.backedge.i
                                        #   in Loop: Header=BB20_72 Depth=1
	decq	%rbp
	movq	328(%rbx), %rax
	movslq	(%rax), %rcx
	cmpq	%rcx, %rbp
	jg	.LBB20_72
# BB#77:                                # %_ZN6Solver14insertVarOrderEi.exit._crit_edge.loopexit.i
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx), %edx
.LBB20_78:                              # %_ZN6Solver14insertVarOrderEi.exit._crit_edge.i
	movl	%ecx, 376(%rbx)
	movl	(%rax), %eax
	cmpl	%eax, %edx
	jle	.LBB20_80
# BB#79:                                # %.lr.ph.i15.i
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
.LBB20_80:                              # %_ZN3vecI3LitE6shrinkEi.exit.i
	cmpl	$0, 336(%rbx)
	jle	.LBB20_86
# BB#81:                                # %.lr.ph.i.i86
	movl	$0, 336(%rbx)
	jmp	.LBB20_86
.LBB20_82:
	movb	$-1, %bl
	jmp	.LBB20_87
.LBB20_83:                              # %.thread146
	xorpd	%xmm1, %xmm1
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 456(%rbx)
.LBB20_86:
	xorl	%ebx, %ebx
	jmp	.LBB20_87
.LBB20_84:
	movb	$1, %bl
.LBB20_87:                              # %.thread101
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_89
# BB#88:                                # %.preheader.i.i54
	movl	$0, 24(%rsp)
	callq	free
	movq	$0, 16(%rsp)
	movl	$0, 28(%rsp)
.LBB20_89:                              # %_ZN3vecI3LitED2Ev.exit55
	movl	%ebx, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB20_90:
.Ltmp10:
	jmp	.LBB20_97
.LBB20_91:                              # %.loopexit.split-lp.loopexit.split-lp
.Ltmp7:
	jmp	.LBB20_97
.LBB20_92:                              # %.loopexit
.Ltmp13:
	jmp	.LBB20_97
.LBB20_93:
.Ltmp24:
	jmp	.LBB20_97
.LBB20_94:                              # %.loopexit.split-lp.loopexit
.Ltmp18:
	jmp	.LBB20_97
.LBB20_95:
.Ltmp2:
	jmp	.LBB20_97
.LBB20_96:
.Ltmp21:
.LBB20_97:                              # %.loopexit.split-lp
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB20_99
# BB#98:                                # %.preheader.i.i
	movl	$0, 24(%rsp)
	callq	free
	movq	$0, 16(%rsp)
	movl	$0, 28(%rsp)
.LBB20_99:                              # %_ZN3vecI3LitED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end20:
	.size	_ZN6Solver6searchEii, .Lfunc_end20-_ZN6Solver6searchEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\367\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Ltmp22-.Lfunc_begin0   # >> Call Site 1 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin0   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp17-.Ltmp14         #   Call between .Ltmp14 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin0   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin0   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp6-.Ltmp3           #   Call between .Ltmp3 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp8-.Ltmp6           #   Call between .Ltmp6 and .Ltmp8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 7 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Lfunc_end20-.Ltmp12    #   Call between .Ltmp12 and .Lfunc_end20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI21_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	_ZNK6Solver16progressEstimateEv
	.p2align	4, 0x90
	.type	_ZNK6Solver16progressEstimateEv,@function
_ZNK6Solver16progressEstimateEv:        # @_ZNK6Solver16progressEstimateEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi177:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi178:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi179:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi180:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi181:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi182:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi183:
	.cfi_def_cfa_offset 80
.Lcfi184:
	.cfi_offset %rbx, -56
.Lcfi185:
	.cfi_offset %r12, -48
.Lcfi186:
	.cfi_offset %r13, -40
.Lcfi187:
	.cfi_offset %r14, -32
.Lcfi188:
	.cfi_offset %r15, -24
.Lcfi189:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	272(%r15), %ecx
	movl	336(%r15), %eax
	testl	%eax, %eax
	js	.LBB21_1
# BB#4:                                 # %.lr.ph
	cvtsi2sdl	%ecx, %xmm0
	movsd	.LCPI21_0(%rip), %xmm1  # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	leaq	320(%r15), %r14
	xorpd	%xmm0, %xmm0
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB21_5:                               # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	movl	$0, %r13d
	je	.LBB21_7
# BB#6:                                 #   in Loop: Header=BB21_5 Depth=1
	movq	328(%r15), %rcx
	movl	-4(%rcx,%rbx,4), %r13d
.LBB21_7:                               #   in Loop: Header=BB21_5 Depth=1
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movl	%eax, %eax
	cmpq	%rax, %rbx
	movq	%r14, %rax
	je	.LBB21_9
# BB#8:                                 #   in Loop: Header=BB21_5 Depth=1
	movq	328(%r15), %rax
	addq	%r12, %rax
.LBB21_9:                               #   in Loop: Header=BB21_5 Depth=1
	movl	(%rax), %ebp
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	pow
	subl	%r13d, %ebp
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	movslq	336(%r15), %rax
	addq	$4, %r12
	cmpq	%rax, %rbx
	leaq	1(%rbx), %rbx
	jl	.LBB21_5
# BB#2:                                 # %._crit_edge.loopexit
	movl	272(%r15), %ecx
	jmp	.LBB21_3
.LBB21_1:
	xorpd	%xmm0, %xmm0
.LBB21_3:                               # %._crit_edge
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	divsd	%xmm1, %xmm0
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	_ZNK6Solver16progressEstimateEv, .Lfunc_end21-_ZNK6Solver16progressEstimateEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI22_0:
	.long	4294967294              # 0xfffffffe
	.long	4294967294              # 0xfffffffe
	.long	4294967294              # 0xfffffffe
	.long	4294967294              # 0xfffffffe
	.text
	.globl	_ZN6Solver5solveERK3vecI3LitE
	.p2align	4, 0x90
	.type	_ZN6Solver5solveERK3vecI3LitE,@function
_ZN6Solver5solveERK3vecI3LitE:          # @_ZN6Solver5solveERK3vecI3LitE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi190:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi191:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi192:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi193:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi194:
	.cfi_def_cfa_offset 64
.Lcfi195:
	.cfi_offset %rbx, -40
.Lcfi196:
	.cfi_offset %r14, -32
.Lcfi197:
	.cfi_offset %r15, -24
.Lcfi198:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpq	$0, (%rbx)
	je	.LBB22_2
# BB#1:                                 # %.preheader.i
	movl	$0, 8(%rbx)
.LBB22_2:                               # %_ZN3vecI5lboolE5clearEb.exit
	cmpq	$0, 16(%rbx)
	je	.LBB22_4
# BB#3:                                 # %.preheader.i22
	movl	$0, 24(%rbx)
.LBB22_4:                               # %_ZN3vecI3LitE5clearEb.exit
	cmpb	$0, 176(%rbx)
	je	.LBB22_5
# BB#6:
	movq	392(%rbx), %rax
	testq	%rax, %rax
	je	.LBB22_7
# BB#8:                                 # %.preheader.i.i26
	movl	$0, 400(%rbx)
	xorl	%edx, %edx
	jmp	.LBB22_9
.LBB22_5:
	xorl	%r14d, %r14d
	jmp	.LBB22_61
.LBB22_7:                               # %._ZN3vecI3LitE5clearEb.exit_crit_edge.i
	movl	400(%rbx), %edx
.LBB22_9:                               # %_ZN3vecI3LitE5clearEb.exit.i
	movl	8(%r14), %ebp
	cmpl	%ebp, %edx
	jge	.LBB22_20
# BB#10:
	movl	404(%rbx), %ecx
	cmpl	%ebp, %ecx
	jge	.LBB22_15
# BB#11:
	testl	%ecx, %ecx
	je	.LBB22_12
	.p2align	4, 0x90
.LBB22_13:                              # %.preheader.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rcx,%rcx,2), %ecx
	sarl	%ecx
	cmpl	%ebp, %ecx
	jl	.LBB22_13
	jmp	.LBB22_14
.LBB22_12:
	cmpl	$1, %ebp
	movl	$2, %ecx
	cmovgl	%ebp, %ecx
.LBB22_14:                              # %.loopexit.i.i.i
	movl	%ecx, 404(%rbx)
	movslq	%ecx, %rsi
	shlq	$2, %rsi
	movq	%rax, %rdi
	callq	realloc
	movq	%rax, 392(%rbx)
	movl	400(%rbx), %edx
.LBB22_15:                              # %_ZN3vecI3LitE4growEi.exit.i.i
	cmpl	%ebp, %edx
	jge	.LBB22_19
# BB#16:                                # %.lr.ph.i.i27
	movslq	%ebp, %rcx
	movslq	%edx, %r11
	movq	%rcx, %r15
	subq	%r11, %r15
	cmpq	$7, %r15
	jbe	.LBB22_17
# BB#30:                                # %min.iters.checked
	movq	%r15, %r8
	andq	$-8, %r8
	movq	%r15, %r10
	andq	$-8, %r10
	je	.LBB22_17
# BB#31:                                # %vector.body.preheader
	leaq	-8(%r10), %r9
	movl	%r9d, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB22_32
# BB#33:                                # %vector.body.prol.preheader
	leaq	16(%rax,%r11,4), %rsi
	negq	%rdx
	xorl	%edi, %edi
	movaps	.LCPI22_0(%rip), %xmm0  # xmm0 = [4294967294,4294967294,4294967294,4294967294]
	.p2align	4, 0x90
.LBB22_34:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -16(%rsi,%rdi,4)
	movups	%xmm0, (%rsi,%rdi,4)
	addq	$8, %rdi
	incq	%rdx
	jne	.LBB22_34
	jmp	.LBB22_35
.LBB22_32:
	xorl	%edi, %edi
.LBB22_35:                              # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB22_38
# BB#36:                                # %vector.body.preheader.new
	movq	%r10, %rdx
	subq	%rdi, %rdx
	addq	%r11, %rdi
	leaq	112(%rax,%rdi,4), %rdi
	movaps	.LCPI22_0(%rip), %xmm0  # xmm0 = [4294967294,4294967294,4294967294,4294967294]
	.p2align	4, 0x90
.LBB22_37:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -112(%rdi)
	movups	%xmm0, -96(%rdi)
	movups	%xmm0, -80(%rdi)
	movups	%xmm0, -64(%rdi)
	movups	%xmm0, -48(%rdi)
	movups	%xmm0, -32(%rdi)
	movups	%xmm0, -16(%rdi)
	movups	%xmm0, (%rdi)
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB22_37
.LBB22_38:                              # %middle.block
	cmpq	%r10, %r15
	je	.LBB22_19
# BB#39:
	addq	%r8, %r11
.LBB22_17:                              # %scalar.ph.preheader
	subq	%r11, %rcx
	leaq	(%rax,%r11,4), %rdx
	.p2align	4, 0x90
.LBB22_18:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$-2, (%rdx)
	addq	$4, %rdx
	decq	%rcx
	jne	.LBB22_18
.LBB22_19:                              # %._crit_edge.i.i
	movl	%ebp, 400(%rbx)
	movl	8(%r14), %ebp
.LBB22_20:                              # %_ZN3vecI3LitE6growToEi.exit.preheader.i
	testl	%ebp, %ebp
	jle	.LBB22_23
# BB#21:                                # %.lr.ph.i28
	movq	(%r14), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB22_22:                              # %_ZN3vecI3LitE6growToEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rdx,4), %esi
	movl	%esi, (%rax,%rdx,4)
	incq	%rdx
	movslq	8(%r14), %rsi
	cmpq	%rsi, %rdx
	jl	.LBB22_22
.LBB22_23:                              # %_ZNK3vecI3LitE6copyToERS1_.exit
	xorps	%xmm0, %xmm0
	cvtsi2sdl	56(%rbx), %xmm0
	cvtsi2sdl	192(%rbx), %xmm1
	mulsd	72(%rbx), %xmm1
	.p2align	4, 0x90
.LBB22_24:                              # =>This Inner Loop Header: Depth=1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	cvttsd2si	%xmm0, %esi
	cvttsd2si	%xmm1, %edx
	movq	%rbx, %rdi
	callq	_ZN6Solver6searchEii
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	64(%rbx), %xmm0
	mulsd	80(%rbx), %xmm1
	testb	%al, %al
	je	.LBB22_24
# BB#25:
	cmpb	$1, %al
	sete	%r14b
	jne	.LBB22_48
# BB#26:
	movl	8(%rbx), %ecx
	movl	272(%rbx), %ebp
	cmpl	%ebp, %ecx
	jge	.LBB22_45
# BB#27:
	movl	12(%rbx), %eax
	cmpl	%ebp, %eax
	jge	.LBB22_42
# BB#28:
	testl	%eax, %eax
	je	.LBB22_29
	.p2align	4, 0x90
.LBB22_40:                              # %.preheader.i.i
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rax,%rax,2), %eax
	sarl	%eax
	cmpl	%ebp, %eax
	jl	.LBB22_40
	jmp	.LBB22_41
.LBB22_48:
	cmpl	$0, 24(%rbx)
	jne	.LBB22_50
# BB#49:
	movb	$0, 176(%rbx)
	cmpl	$0, 336(%rbx)
	jg	.LBB22_51
	jmp	.LBB22_61
.LBB22_29:
	cmpl	$1, %ebp
	movl	$2, %eax
	cmovgl	%ebp, %eax
.LBB22_41:                              # %.loopexit.i.i
	movl	%eax, 12(%rbx)
	movq	(%rbx), %rdi
	movslq	%eax, %rsi
	callq	realloc
	movq	%rax, (%rbx)
	movl	8(%rbx), %ecx
.LBB22_42:                              # %_ZN3vecI5lboolE4growEi.exit.i
	cmpl	%ebp, %ecx
	jge	.LBB22_44
# BB#43:                                # %.lr.ph.i24
	movslq	%ecx, %rdi
	addq	(%rbx), %rdi
	leal	-1(%rbp), %edx
	subl	%ecx, %edx
	incq	%rdx
	xorl	%esi, %esi
	callq	memset
.LBB22_44:                              # %._crit_edge.i
	movl	%ebp, 8(%rbx)
	movl	272(%rbx), %ebp
.LBB22_45:                              # %_ZN3vecI5lboolE6growToEi.exit.preheader
	testl	%ebp, %ebp
	jle	.LBB22_50
# BB#46:                                # %.lr.ph
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB22_47:                              # %_ZN3vecI5lboolE6growToEi.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	264(%rbx), %rdx
	movzbl	(%rdx,%rax), %edx
	movb	%dl, (%rcx,%rax)
	incq	%rax
	movslq	272(%rbx), %rcx
	cmpq	%rcx, %rax
	jl	.LBB22_47
.LBB22_50:                              # %.loopexit
	cmpl	$0, 336(%rbx)
	jle	.LBB22_61
.LBB22_51:
	movl	320(%rbx), %edx
	movq	328(%rbx), %rax
	movl	(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.LBB22_57
# BB#52:                                # %.lr.ph.i
	movslq	%edx, %rbp
	leaq	408(%rbx), %r15
	.p2align	4, 0x90
.LBB22_53:                              # =>This Inner Loop Header: Depth=1
	movq	264(%rbx), %rcx
	movq	312(%rbx), %rax
	movl	-4(%rax,%rbp,4), %esi
	decq	%rbp
	sarl	%esi
	movslq	%esi, %rax
	movb	$0, (%rcx,%rax)
	cmpl	%eax, 440(%rbx)
	jle	.LBB22_62
# BB#54:                                # %_ZNK4HeapIN6Solver10VarOrderLtEE6inHeapEi.exit.i.i
                                        #   in Loop: Header=BB22_53 Depth=1
	movq	432(%rbx), %rcx
	cmpl	$0, (%rcx,%rax,4)
	jns	.LBB22_55
.LBB22_62:                              # %_ZNK4HeapIN6Solver10VarOrderLtEE6inHeapEi.exit.thread.i.i
                                        #   in Loop: Header=BB22_53 Depth=1
	movq	296(%rbx), %rcx
	cmpb	$0, (%rcx,%rax)
	je	.LBB22_55
# BB#63:                                #   in Loop: Header=BB22_53 Depth=1
	movq	%r15, %rdi
	callq	_ZN4HeapIN6Solver10VarOrderLtEE6insertEi
.LBB22_55:                              # %_ZN6Solver14insertVarOrderEi.exit.backedge.i
                                        #   in Loop: Header=BB22_53 Depth=1
	movq	328(%rbx), %rax
	movslq	(%rax), %rcx
	cmpq	%rcx, %rbp
	jg	.LBB22_53
# BB#56:                                # %_ZN6Solver14insertVarOrderEi.exit._crit_edge.loopexit.i
	movl	320(%rbx), %edx
.LBB22_57:                              # %_ZN6Solver14insertVarOrderEi.exit._crit_edge.i
	movl	%ecx, 376(%rbx)
	movl	(%rax), %eax
	cmpl	%eax, %edx
	jle	.LBB22_59
# BB#58:                                # %.lr.ph.i15.i
	movl	%eax, 320(%rbx)
.LBB22_59:                              # %_ZN3vecI3LitE6shrinkEi.exit.i
	cmpl	$0, 336(%rbx)
	jle	.LBB22_61
# BB#60:                                # %.lr.ph.i.i
	movl	$0, 336(%rbx)
.LBB22_61:                              # %_ZN6Solver11cancelUntilEi.exit
	movl	%r14d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	_ZN6Solver5solveERK3vecI3LitE, .Lfunc_end22-_ZN6Solver5solveERK3vecI3LitE
	.cfi_endproc

	.globl	_ZN6Solver11verifyModelEv
	.p2align	4, 0x90
	.type	_ZN6Solver11verifyModelEv,@function
_ZN6Solver11verifyModelEv:              # @_ZN6Solver11verifyModelEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi199:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi200:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi201:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi202:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi203:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi204:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi205:
	.cfi_def_cfa_offset 64
.Lcfi206:
	.cfi_offset %rbx, -56
.Lcfi207:
	.cfi_offset %r12, -48
.Lcfi208:
	.cfi_offset %r13, -40
.Lcfi209:
	.cfi_offset %r14, -32
.Lcfi210:
	.cfi_offset %r15, -24
.Lcfi211:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movl	192(%r13), %r8d
	testl	%r8d, %r8d
	jle	.LBB23_15
# BB#1:                                 # %.lr.ph20
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB23_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_5 Depth 2
                                        #     Child Loop BB23_10 Depth 2
	movq	184(%r13), %rcx
	movq	(%rcx,%r12,8), %rcx
	movl	(%rcx), %edx
	cmpl	$8, %edx
	jb	.LBB23_8
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB23_2 Depth=1
	movq	(%r13), %rsi
	shrl	$3, %edx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB23_5:                               #   Parent Loop BB23_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%rcx,%rdi,4), %ebp
	movl	%ebp, %eax
	sarl	%eax
	cltq
	movzbl	(%rsi,%rax), %ebx
	testb	$1, %bpl
	je	.LBB23_7
# BB#6:                                 #   in Loop: Header=BB23_5 Depth=2
	negb	%bl
.LBB23_7:                               #   in Loop: Header=BB23_5 Depth=2
	cmpb	$1, %bl
	je	.LBB23_14
# BB#4:                                 #   in Loop: Header=BB23_5 Depth=2
	incq	%rdi
	cmpq	%rdx, %rdi
	jl	.LBB23_5
.LBB23_8:                               # %.critedge
                                        #   in Loop: Header=BB23_2 Depth=1
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$20, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	184(%r13), %rax
	movq	(%rax,%r12,8), %rbx
	cmpl	$8, (%rbx)
	jb	.LBB23_13
# BB#9:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB23_2 Depth=1
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB23_10:                              # %.lr.ph.i
                                        #   Parent Loop BB23_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%rbx,%r14,4), %r15d
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	%r15d, %ecx
	sarl	%ecx
	movq	264(%r13), %rax
	movslq	%ecx, %rdx
	movzbl	(%rax,%rdx), %eax
	andl	$1, %r15d
	je	.LBB23_12
# BB#11:                                #   in Loop: Header=BB23_10 Depth=2
	negb	%al
.LBB23_12:                              # %.lr.ph.i
                                        #   in Loop: Header=BB23_10 Depth=2
	testb	%r15b, %r15b
	movl	$.L.str.11, %edx
	movl	$.L.str.10, %esi
	cmovneq	%rsi, %rdx
	incl	%ecx
	cmpb	$-1, %al
	movl	$88, %r8d
	movl	$48, %esi
	cmovel	%esi, %r8d
	cmpb	$1, %al
	movl	$49, %eax
	cmovel	%eax, %r8d
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	incq	%r14
	movl	(%rbx), %eax
	shrl	$3, %eax
	cmpq	%rax, %r14
	jl	.LBB23_10
.LBB23_13:                              # %_ZN6Solver11printClauseI6ClauseEEvRKT_.exit
                                        #   in Loop: Header=BB23_2 Depth=1
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	192(%r13), %r8d
.LBB23_14:                              # %.loopexit
                                        #   in Loop: Header=BB23_2 Depth=1
	incq	%r12
	movslq	%r8d, %rax
	cmpq	%rax, %r12
	jl	.LBB23_2
.LBB23_15:                              # %._crit_edge
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	192(%r13), %edx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fflush                  # TAILCALL
.Lfunc_end23:
	.size	_ZN6Solver11verifyModelEv, .Lfunc_end23-_ZN6Solver11verifyModelEv
	.cfi_endproc

	.globl	_ZN6Solver17checkLiteralCountEv
	.p2align	4, 0x90
	.type	_ZN6Solver17checkLiteralCountEv,@function
_ZN6Solver17checkLiteralCountEv:        # @_ZN6Solver17checkLiteralCountEv
	.cfi_startproc
# BB#0:
	movslq	192(%rdi), %r8
	testq	%r8, %r8
	jle	.LBB24_1
# BB#2:                                 # %.lr.ph
	movq	184(%rdi), %r10
	testb	$1, %r8b
	jne	.LBB24_4
# BB#3:
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	cmpl	$1, %r8d
	jne	.LBB24_6
	jmp	.LBB24_8
.LBB24_1:
	xorl	%ecx, %ecx
	jmp	.LBB24_8
.LBB24_4:
	movq	(%r10), %rax
	movl	(%rax), %eax
	movl	%eax, %esi
	shrl	$3, %esi
	xorl	%ecx, %ecx
	testb	$6, %al
	cmovel	%esi, %ecx
	movl	$1, %esi
	cmpl	$1, %r8d
	je	.LBB24_8
.LBB24_6:
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB24_7:                               # =>This Inner Loop Header: Depth=1
	movq	(%r10,%rsi,8), %rax
	movl	(%rax), %edx
	movl	%edx, %eax
	shrl	$3, %eax
	testb	$6, %dl
	cmovnel	%r9d, %eax
	addl	%ecx, %eax
	movq	8(%r10,%rsi,8), %rcx
	movl	(%rcx), %edx
	movl	%edx, %ecx
	shrl	$3, %ecx
	testb	$6, %dl
	cmovnel	%r9d, %ecx
	addl	%eax, %ecx
	addq	$2, %rsi
	cmpq	%r8, %rsi
	jl	.LBB24_7
.LBB24_8:                               # %._crit_edge
	movl	144(%rdi), %edx
	cmpl	%ecx, %edx
	jne	.LBB24_10
# BB#9:
	retq
.LBB24_10:
	movq	stderr(%rip), %rdi
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	jmp	fprintf                 # TAILCALL
.Lfunc_end24:
	.size	_ZN6Solver17checkLiteralCountEv, .Lfunc_end24-_ZN6Solver17checkLiteralCountEv
	.cfi_endproc

	.section	.text._ZN4HeapIN6Solver10VarOrderLtEE6insertEi,"axG",@progbits,_ZN4HeapIN6Solver10VarOrderLtEE6insertEi,comdat
	.weak	_ZN4HeapIN6Solver10VarOrderLtEE6insertEi
	.p2align	4, 0x90
	.type	_ZN4HeapIN6Solver10VarOrderLtEE6insertEi,@function
_ZN4HeapIN6Solver10VarOrderLtEE6insertEi: # @_ZN4HeapIN6Solver10VarOrderLtEE6insertEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi212:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi213:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi214:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi215:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi216:
	.cfi_def_cfa_offset 48
.Lcfi217:
	.cfi_offset %rbx, -40
.Lcfi218:
	.cfi_offset %r14, -32
.Lcfi219:
	.cfi_offset %r15, -24
.Lcfi220:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	movl	32(%r15), %ecx
	cmpl	%r14d, %ecx
	jg	.LBB25_9
# BB#1:
	leal	1(%r14), %ebp
	movl	36(%r15), %eax
	cmpl	%r14d, %eax
	jg	.LBB25_6
# BB#2:
	testl	%eax, %eax
	je	.LBB25_3
	.p2align	4, 0x90
.LBB25_4:                               # %.preheader.i.i
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rax,%rax,2), %eax
	sarl	%eax
	cmpl	%r14d, %eax
	jle	.LBB25_4
	jmp	.LBB25_5
.LBB25_3:
	cmpl	$1, %ebp
	movl	$2, %eax
	cmovgl	%ebp, %eax
.LBB25_5:                               # %.loopexit.i.i
	movl	%eax, 36(%r15)
	movq	24(%r15), %rdi
	movslq	%eax, %rsi
	shlq	$2, %rsi
	callq	realloc
	movq	%rax, 24(%r15)
	movl	32(%r15), %ecx
.LBB25_6:                               # %_ZN3vecIiE4growEi.exit.i
	cmpl	%r14d, %ecx
	jg	.LBB25_8
# BB#7:                                 # %.lr.ph.i
	movq	24(%r15), %rax
	movslq	%ecx, %rcx
	movslq	%ebp, %rdx
	leaq	(%rax,%rcx,4), %rdi
	subq	%rcx, %rdx
	shlq	$2, %rdx
	movl	$255, %esi
	callq	memset
.LBB25_8:                               # %._crit_edge.i
	movl	%ebp, 32(%r15)
.LBB25_9:                               # %_ZN3vecIiE6growToEiRKi.exit
	movl	16(%r15), %eax
	movq	24(%r15), %rcx
	movslq	%r14d, %rbp
	movl	%eax, (%rcx,%rbp,4)
	movl	16(%r15), %edx
	cmpl	20(%r15), %edx
	jne	.LBB25_10
# BB#11:
	leal	1(%rdx,%rdx,2), %eax
	sarl	%eax
	leal	-2(%rax), %ecx
	sarl	$31, %ecx
	movl	%ecx, %edx
	andl	$2, %edx
	notl	%ecx
	andl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, 20(%r15)
	movq	8(%r15), %rdi
	movslq	%ecx, %rsi
	shlq	$2, %rsi
	callq	realloc
	movq	%rax, 8(%r15)
	movl	16(%r15), %edx
	movq	24(%r15), %rcx
	jmp	.LBB25_12
.LBB25_10:                              # %._crit_edge.i4
	movq	8(%r15), %rax
.LBB25_12:                              # %_ZN3vecIiE4pushERKi.exit
	leal	1(%rdx), %esi
	movl	%esi, 16(%r15)
	movslq	%edx, %rdx
	movl	%r14d, (%rax,%rdx,4)
	movslq	(%rcx,%rbp,4), %rdi
	testq	%rdi, %rdi
	movl	(%rax,%rdi,4), %r8d
	movslq	%r8d, %rdx
	je	.LBB25_15
	.p2align	4, 0x90
.LBB25_13:                              # =>This Inner Loop Header: Depth=1
	leal	-1(%rdi), %ebp
	sarl	%ebp
	movslq	%ebp, %rsi
	movl	(%rax,%rsi,4), %esi
	movq	(%r15), %rbx
	movq	(%rbx), %rbx
	movsd	(%rbx,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movslq	%esi, %rsi
	ucomisd	(%rbx,%rsi,8), %xmm0
	jbe	.LBB25_16
# BB#14:                                #   in Loop: Header=BB25_13 Depth=1
	movslq	%edi, %rbx
	movl	%esi, (%rax,%rbx,4)
	movl	%edi, (%rcx,%rsi,4)
	testl	%ebp, %ebp
	movl	%ebp, %edi
	jne	.LBB25_13
.LBB25_15:
	xorl	%edi, %edi
.LBB25_16:                              # %_ZN4HeapIN6Solver10VarOrderLtEE11percolateUpEi.exit
	movslq	%edi, %rsi
	movl	%r8d, (%rax,%rsi,4)
	movl	%esi, (%rcx,%rdx,4)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	_ZN4HeapIN6Solver10VarOrderLtEE6insertEi, .Lfunc_end25-_ZN4HeapIN6Solver10VarOrderLtEE6insertEi
	.cfi_endproc

	.section	.text._Z4sortI3Lit16LessThan_defaultIS0_EEvPT_iT0_,"axG",@progbits,_Z4sortI3Lit16LessThan_defaultIS0_EEvPT_iT0_,comdat
	.weak	_Z4sortI3Lit16LessThan_defaultIS0_EEvPT_iT0_
	.p2align	4, 0x90
	.type	_Z4sortI3Lit16LessThan_defaultIS0_EEvPT_iT0_,@function
_Z4sortI3Lit16LessThan_defaultIS0_EEvPT_iT0_: # @_Z4sortI3Lit16LessThan_defaultIS0_EEvPT_iT0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi221:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi222:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi223:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi224:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi225:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi226:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi227:
	.cfi_def_cfa_offset 64
.Lcfi228:
	.cfi_offset %rbx, -56
.Lcfi229:
	.cfi_offset %r12, -48
.Lcfi230:
	.cfi_offset %r13, -40
.Lcfi231:
	.cfi_offset %r14, -32
.Lcfi232:
	.cfi_offset %r15, -24
.Lcfi233:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	cmpl	$16, %esi
	jge	.LBB26_16
# BB#1:
	movq	%rdi, %r12
	cmpl	$2, %esi
	jge	.LBB26_5
	jmp	.LBB26_15
	.p2align	4, 0x90
.LBB26_16:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_17 Depth 2
                                        #       Child Loop BB26_18 Depth 3
                                        #       Child Loop BB26_20 Depth 3
	movl	%esi, %r14d
	movl	%r14d, %eax
	shrl	%eax
	movl	(%rdi,%rax,4), %r11d
	leaq	4(%rdi), %r8
	movq	$-1, %r15
	movl	%r14d, %edx
	jmp	.LBB26_17
	.p2align	4, 0x90
.LBB26_22:                              #   in Loop: Header=BB26_17 Depth=2
	movl	%eax, (%r10,%rbp,4)
	movl	%esi, (%rcx)
	movq	%r9, %r15
.LBB26_17:                              #   Parent Loop BB26_16 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB26_18 Depth 3
                                        #       Child Loop BB26_20 Depth 3
	movslq	%r15d, %r9
	leaq	(%rdi,%r9,4), %r12
	leaq	(%r8,%r9,4), %r10
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB26_18:                              #   Parent Loop BB26_16 Depth=1
                                        #     Parent Loop BB26_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r13, %rbp
	movl	(%r10,%rbp,4), %esi
	leaq	1(%rbp), %r13
	addq	$4, %r12
	cmpl	%r11d, %esi
	jl	.LBB26_18
# BB#19:                                # %.preheader
                                        #   in Loop: Header=BB26_17 Depth=2
	movslq	%edx, %rax
	addq	%r13, %r9
	leaq	(%rdi,%rax,4), %rcx
	.p2align	4, 0x90
.LBB26_20:                              #   Parent Loop BB26_16 Depth=1
                                        #     Parent Loop BB26_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rcx), %eax
	addq	$-4, %rcx
	decl	%edx
	cmpl	%eax, %r11d
	jl	.LBB26_20
# BB#21:                                #   in Loop: Header=BB26_17 Depth=2
	leal	(%r15,%r13), %ebx
	cmpl	%edx, %ebx
	jl	.LBB26_22
# BB#2:                                 # %tailrecurse
                                        #   in Loop: Header=BB26_16 Depth=1
	leal	(%r15,%r13), %esi
	callq	_Z4sortI3Lit16LessThan_defaultIS0_EEvPT_iT0_
	movl	%r14d, %esi
	subl	%r15d, %esi
	subl	%r13d, %esi
	cmpl	$16, %esi
	movq	%r12, %rdi
	jge	.LBB26_16
# BB#3:                                 # %tailrecurse._crit_edge.loopexit
	subl	%r15d, %r14d
	subl	%r13d, %r14d
	movl	%r14d, %esi
	cmpl	$2, %esi
	jl	.LBB26_15
.LBB26_5:                               # %.lr.ph32.preheader.i
	leal	-1(%rsi), %r10d
	movslq	%esi, %r11
	movl	%esi, %edx
	leaq	3(%rdx), %r8
	leaq	-2(%rdx), %r9
	addb	$3, %sil
	movl	$1, %r13d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB26_6:                               # %.lr.ph32.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_11 Depth 2
                                        #     Child Loop BB26_13 Depth 2
	movq	%r14, %rcx
	leaq	1(%rcx), %r14
	cmpq	%r11, %r14
	jge	.LBB26_7
# BB#8:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB26_6 Depth=1
	movq	%r9, %r15
	subq	%rcx, %r15
	movl	%r8d, %eax
	subl	%ecx, %eax
	testb	$3, %al
	je	.LBB26_9
# BB#10:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB26_6 Depth=1
	movl	%esi, %eax
	andb	$3, %al
	movzbl	%al, %ebx
	negq	%rbx
	movq	%r13, %rdi
	movl	%ecx, %eax
	.p2align	4, 0x90
.LBB26_11:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB26_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r12,%rdi,4), %ebp
	cltq
	cmpl	(%r12,%rax,4), %ebp
	cmovll	%edi, %eax
	incq	%rdi
	incq	%rbx
	jne	.LBB26_11
	jmp	.LBB26_12
	.p2align	4, 0x90
.LBB26_7:                               #   in Loop: Header=BB26_6 Depth=1
	movl	%ecx, %eax
	jmp	.LBB26_14
.LBB26_9:                               #   in Loop: Header=BB26_6 Depth=1
	movq	%r13, %rdi
	movl	%ecx, %eax
.LBB26_12:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB26_6 Depth=1
	cmpq	$3, %r15
	jb	.LBB26_14
	.p2align	4, 0x90
.LBB26_13:                              # %.lr.ph.i
                                        #   Parent Loop BB26_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r12,%rdi,4), %ebx
	cltq
	cmpl	(%r12,%rax,4), %ebx
	cmovll	%edi, %eax
	movl	4(%r12,%rdi,4), %ebx
	cltq
	leal	1(%rdi), %ebp
	cmpl	(%r12,%rax,4), %ebx
	cmovll	%ebp, %eax
	movl	8(%r12,%rdi,4), %ebx
	cltq
	leal	2(%rdi), %ebp
	cmpl	(%r12,%rax,4), %ebx
	cmovll	%ebp, %eax
	movl	12(%r12,%rdi,4), %ebx
	cltq
	leal	3(%rdi), %ebp
	cmpl	(%r12,%rax,4), %ebx
	cmovll	%ebp, %eax
	addq	$4, %rdi
	cmpq	%rdx, %rdi
	jne	.LBB26_13
.LBB26_14:                              # %._crit_edge.i
                                        #   in Loop: Header=BB26_6 Depth=1
	movl	(%r12,%rcx,4), %edi
	cltq
	movl	(%r12,%rax,4), %ebx
	movl	%ebx, (%r12,%rcx,4)
	movl	%edi, (%r12,%rax,4)
	incq	%r13
	addb	$3, %sil
	cmpq	%r10, %r14
	jne	.LBB26_6
.LBB26_15:                              # %_Z13selectionSortI3Lit16LessThan_defaultIS0_EEvPT_iT0_.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	_Z4sortI3Lit16LessThan_defaultIS0_EEvPT_iT0_, .Lfunc_end26-_Z4sortI3Lit16LessThan_defaultIS0_EEvPT_iT0_
	.cfi_endproc

	.section	.text._Z4sortIP6Clause11reduceDB_ltEvPT_iT0_,"axG",@progbits,_Z4sortIP6Clause11reduceDB_ltEvPT_iT0_,comdat
	.weak	_Z4sortIP6Clause11reduceDB_ltEvPT_iT0_
	.p2align	4, 0x90
	.type	_Z4sortIP6Clause11reduceDB_ltEvPT_iT0_,@function
_Z4sortIP6Clause11reduceDB_ltEvPT_iT0_: # @_Z4sortIP6Clause11reduceDB_ltEvPT_iT0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi234:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi235:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi236:
	.cfi_def_cfa_offset 32
.Lcfi237:
	.cfi_offset %rbx, -32
.Lcfi238:
	.cfi_offset %r14, -24
.Lcfi239:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	cmpl	$16, %r14d
	jge	.LBB27_15
# BB#1:
	movq	%rdi, %rbx
	cmpl	$2, %r14d
	jge	.LBB27_3
	jmp	.LBB27_13
	.p2align	4, 0x90
.LBB27_15:                              # %.lr.ph66
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_30 Depth 2
                                        #       Child Loop BB27_33 Depth 3
                                        #       Child Loop BB27_37 Depth 3
                                        #     Child Loop BB27_17 Depth 2
                                        #       Child Loop BB27_20 Depth 3
                                        #       Child Loop BB27_28 Depth 3
                                        #       Child Loop BB27_24 Depth 3
	movl	%r14d, %eax
	shrl	%eax
	movq	(%rdi,%rax,8), %r8
	movl	(%r8), %r9d
	cmpl	$24, %r9d
	jb	.LBB27_29
# BB#16:                                # %.split.us.preheader
                                        #   in Loop: Header=BB27_15 Depth=1
	andl	$-8, %r9d
	movss	4(%r8), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movl	$-1, %ebp
	movl	%r14d, %edx
	jmp	.LBB27_17
	.p2align	4, 0x90
.LBB27_27:                              #   in Loop: Header=BB27_17 Depth=2
	movq	(%rbx), %rcx
	movq	%rax, (%rbx)
	movq	%rcx, (%rsi)
.LBB27_17:                              # %.split.us
                                        #   Parent Loop BB27_15 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB27_20 Depth 3
                                        #       Child Loop BB27_28 Depth 3
                                        #       Child Loop BB27_24 Depth 3
	movslq	%ebp, %rax
	incl	%ebp
	leaq	8(%rdi,%rax,8), %rbx
	movq	8(%rdi,%rax,8), %rsi
	cmpl	$23, (%rsi)
	jbe	.LBB27_22
# BB#18:                                # %.lr.ph.us
                                        #   in Loop: Header=BB27_17 Depth=2
	cmpl	$16, %r9d
	jne	.LBB27_19
	.p2align	4, 0x90
.LBB27_28:                              # %_ZN11reduceDB_ltclEP6ClauseS1_.exit.thread.backedge.us.us
                                        #   Parent Loop BB27_15 Depth=1
                                        #     Parent Loop BB27_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbx), %rax
	addq	$8, %rbx
	incl	%ebp
	cmpl	$23, (%rax)
	ja	.LBB27_28
	jmp	.LBB27_22
	.p2align	4, 0x90
.LBB27_19:                              # %_ZN11reduceDB_ltclEP6ClauseS1_.exit.us54.preheader
                                        #   in Loop: Header=BB27_17 Depth=2
	movss	4(%r8), %xmm1           # xmm1 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB27_20:                              # %_ZN11reduceDB_ltclEP6ClauseS1_.exit.us54
                                        #   Parent Loop BB27_15 Depth=1
                                        #     Parent Loop BB27_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	ucomiss	4(%rsi), %xmm1
	jbe	.LBB27_22
# BB#21:                                # %_ZN11reduceDB_ltclEP6ClauseS1_.exit.thread.backedge.us55
                                        #   in Loop: Header=BB27_20 Depth=3
	incl	%ebp
	movq	8(%rbx), %rsi
	addq	$8, %rbx
	cmpl	$23, (%rsi)
	ja	.LBB27_20
	.p2align	4, 0x90
.LBB27_22:                              # %_ZN11reduceDB_ltclEP6ClauseS1_.exit.thread39.preheader.us
                                        #   in Loop: Header=BB27_17 Depth=2
	movslq	%edx, %rax
	decl	%edx
	leaq	-8(%rdi,%rax,8), %rsi
	jmp	.LBB27_24
	.p2align	4, 0x90
.LBB27_23:                              # %_ZN11reduceDB_ltclEP6ClauseS1_.exit.thread39.backedge.us
                                        #   in Loop: Header=BB27_24 Depth=3
	decl	%edx
	addq	$-8, %rsi
.LBB27_24:                              #   Parent Loop BB27_15 Depth=1
                                        #     Parent Loop BB27_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rsi), %rax
	movl	(%rax), %ecx
	andl	$-8, %ecx
	cmpl	$16, %ecx
	je	.LBB27_23
# BB#25:                                # %_ZN11reduceDB_ltclEP6ClauseS1_.exit38.us
                                        #   in Loop: Header=BB27_24 Depth=3
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	ja	.LBB27_23
# BB#26:                                # %_ZN11reduceDB_ltclEP6ClauseS1_.exit38.thread40.us
                                        #   in Loop: Header=BB27_17 Depth=2
	cmpl	%edx, %ebp
	jl	.LBB27_27
	jmp	.LBB27_14
	.p2align	4, 0x90
.LBB27_29:                              # %.split.preheader
                                        #   in Loop: Header=BB27_15 Depth=1
	movslq	%r14d, %rdx
	andl	$-8, %r9d
	movl	$-1, %ebp
	jmp	.LBB27_30
	.p2align	4, 0x90
.LBB27_36:                              #   in Loop: Header=BB27_30 Depth=2
	movq	(%rbx), %rcx
	movq	-8(%rdi,%rdx,8), %rsi
	movq	%rsi, (%rbx)
	movq	%rcx, -8(%rdi,%rdx,8)
	movq	%rax, %rdx
.LBB27_30:                              # %.split
                                        #   Parent Loop BB27_15 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB27_33 Depth 3
                                        #       Child Loop BB27_37 Depth 3
	movslq	%ebp, %rax
	incl	%ebp
	leaq	8(%rdi,%rax,8), %rbx
	movq	8(%rdi,%rax,8), %rsi
	cmpl	$24, (%rsi)
	jb	.LBB27_35
# BB#31:                                # %.lr.ph
                                        #   in Loop: Header=BB27_30 Depth=2
	cmpl	$16, %r9d
	jne	.LBB27_32
	.p2align	4, 0x90
.LBB27_37:                              # %.lr.ph.split.us
                                        #   Parent Loop BB27_15 Depth=1
                                        #     Parent Loop BB27_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbx), %rax
	addq	$8, %rbx
	incl	%ebp
	cmpl	$23, (%rax)
	ja	.LBB27_37
	jmp	.LBB27_35
	.p2align	4, 0x90
.LBB27_32:                              # %.lr.ph.split.preheader
                                        #   in Loop: Header=BB27_30 Depth=2
	movss	4(%r8), %xmm0           # xmm0 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB27_33:                              # %.lr.ph.split
                                        #   Parent Loop BB27_15 Depth=1
                                        #     Parent Loop BB27_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	ucomiss	4(%rsi), %xmm0
	jbe	.LBB27_35
# BB#34:                                # %_ZN11reduceDB_ltclEP6ClauseS1_.exit.thread.backedge
                                        #   in Loop: Header=BB27_33 Depth=3
	incl	%ebp
	movq	8(%rbx), %rsi
	addq	$8, %rbx
	cmpl	$23, (%rsi)
	ja	.LBB27_33
	.p2align	4, 0x90
.LBB27_35:                              # %_ZN11reduceDB_ltclEP6ClauseS1_.exit.thread39.preheader
                                        #   in Loop: Header=BB27_30 Depth=2
	leaq	-1(%rdx), %rax
	movslq	%ebp, %rcx
	cmpq	%rax, %rcx
	jl	.LBB27_36
.LBB27_14:                              # %tailrecurse
                                        #   in Loop: Header=BB27_15 Depth=1
	movl	%ebp, %esi
	callq	_Z4sortIP6Clause11reduceDB_ltEvPT_iT0_
	subl	%ebp, %r14d
	cmpl	$16, %r14d
	movq	%rbx, %rdi
	jge	.LBB27_15
# BB#2:                                 # %tailrecurse._crit_edge
	cmpl	$2, %r14d
	jl	.LBB27_13
.LBB27_3:                               # %.lr.ph35.preheader.i
	leal	-1(%r14), %r8d
	movslq	%r14d, %r9
	movl	%r14d, %edx
	movl	$1, %r11d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB27_4:                               # %.lr.ph35.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_7 Depth 2
	leaq	1(%r14), %r10
	cmpq	%r9, %r10
	jge	.LBB27_5
# BB#6:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB27_4 Depth=1
	movq	%r11, %rsi
	movl	%r14d, %eax
	.p2align	4, 0x90
.LBB27_7:                               # %.lr.ph.i
                                        #   Parent Loop BB27_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rsi,8), %rbp
	cmpl	$24, (%rbp)
	jb	.LBB27_11
# BB#8:                                 #   in Loop: Header=BB27_7 Depth=2
	movslq	%eax, %rcx
	movq	(%rbx,%rcx,8), %rcx
	movl	(%rcx), %edi
	andl	$-8, %edi
	cmpl	$16, %edi
	je	.LBB27_10
# BB#9:                                 # %_ZN11reduceDB_ltclEP6ClauseS1_.exit.i
                                        #   in Loop: Header=BB27_7 Depth=2
	movss	4(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%rbp), %xmm0
	jbe	.LBB27_11
.LBB27_10:                              # %_ZN11reduceDB_ltclEP6ClauseS1_.exit.thread.i
                                        #   in Loop: Header=BB27_7 Depth=2
	movl	%esi, %eax
.LBB27_11:                              # %_ZN11reduceDB_ltclEP6ClauseS1_.exit.thread29.i
                                        #   in Loop: Header=BB27_7 Depth=2
	incq	%rsi
	cmpq	%rsi, %rdx
	jne	.LBB27_7
	jmp	.LBB27_12
	.p2align	4, 0x90
.LBB27_5:                               #   in Loop: Header=BB27_4 Depth=1
	movl	%r14d, %eax
.LBB27_12:                              # %._crit_edge.i
                                        #   in Loop: Header=BB27_4 Depth=1
	movq	(%rbx,%r14,8), %rcx
	cltq
	movq	(%rbx,%rax,8), %rsi
	movq	%rsi, (%rbx,%r14,8)
	movq	%rcx, (%rbx,%rax,8)
	incq	%r11
	cmpq	%r8, %r10
	movq	%r10, %r14
	jne	.LBB27_4
.LBB27_13:                              # %_Z13selectionSortIP6Clause11reduceDB_ltEvPT_iT0_.exit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end27:
	.size	_Z4sortIP6Clause11reduceDB_ltEvPT_iT0_, .Lfunc_end27-_Z4sortIP6Clause11reduceDB_ltEvPT_iT0_
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"unsatisfied clause: "
	.size	.L.str, 21

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Verified %d original clauses.\n"
	.size	.L.str.6, 31

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"literal count: %d, real value = %d\n"
	.size	.L.str.7, 36

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%s%d:%c"
	.size	.L.str.9, 8

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"-"
	.size	.L.str.10, 2

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.zero	1
	.size	.L.str.11, 1


	.globl	_ZN6SolverC1Ev
	.type	_ZN6SolverC1Ev,@function
_ZN6SolverC1Ev = _ZN6SolverC2Ev
	.globl	_ZN6SolverD1Ev
	.type	_ZN6SolverD1Ev,@function
_ZN6SolverD1Ev = _ZN6SolverD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
