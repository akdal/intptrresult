	.text
	.file	"InBuffer.bc"
	.globl	_ZN9CInBufferC2Ev
	.p2align	4, 0x90
	.type	_ZN9CInBufferC2Ev,@function
_ZN9CInBufferC2Ev:                      # @_ZN9CInBufferC2Ev
	.cfi_startproc
# BB#0:
	movl	$0, 40(%rdi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, (%rdi)
	retq
.Lfunc_end0:
	.size	_ZN9CInBufferC2Ev, .Lfunc_end0-_ZN9CInBufferC2Ev
	.cfi_endproc

	.globl	_ZN9CInBuffer6CreateEj
	.p2align	4, 0x90
	.type	_ZN9CInBuffer6CreateEj,@function
_ZN9CInBuffer6CreateEj:                 # @_ZN9CInBuffer6CreateEj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	testl	%esi, %esi
	movl	$1, %ebp
	cmovnel	%esi, %ebp
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	movb	$1, %al
	cmpl	%ebp, 40(%rbx)
	je	.LBB1_3
.LBB1_2:                                # %._crit_edge
	callq	MidFree
	movq	$0, 16(%rbx)
	movl	%ebp, 40(%rbx)
	movl	%ebp, %edi
	callq	MidAlloc
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	setne	%al
.LBB1_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN9CInBuffer6CreateEj, .Lfunc_end1-_ZN9CInBuffer6CreateEj
	.cfi_endproc

	.globl	_ZN9CInBuffer4FreeEv
	.p2align	4, 0x90
	.type	_ZN9CInBuffer4FreeEv,@function
_ZN9CInBuffer4FreeEv:                   # @_ZN9CInBuffer4FreeEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	callq	MidFree
	movq	$0, 16(%rbx)
	popq	%rbx
	retq
.Lfunc_end2:
	.size	_ZN9CInBuffer4FreeEv, .Lfunc_end2-_ZN9CInBuffer4FreeEv
	.cfi_endproc

	.globl	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN9CInBuffer9SetStreamEP19ISequentialInStream,@function
_ZN9CInBuffer9SetStreamEP19ISequentialInStream: # @_ZN9CInBuffer9SetStreamEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB3_2
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB3_2:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB3_4:                                # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit
	movq	%rbx, 24(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	_ZN9CInBuffer9SetStreamEP19ISequentialInStream, .Lfunc_end3-_ZN9CInBuffer9SetStreamEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZN9CInBuffer4InitEv
	.p2align	4, 0x90
	.type	_ZN9CInBuffer4InitEv,@function
_ZN9CInBuffer4InitEv:                   # @_ZN9CInBuffer4InitEv
	.cfi_startproc
# BB#0:
	movq	$0, 32(%rdi)
	movq	16(%rdi), %rax
	movq	%rax, (%rdi)
	movq	%rax, 8(%rdi)
	movb	$0, 44(%rdi)
	retq
.Lfunc_end4:
	.size	_ZN9CInBuffer4InitEv, .Lfunc_end4-_ZN9CInBuffer4InitEv
	.cfi_endproc

	.globl	_ZN9CInBuffer9ReadBlockEv
	.p2align	4, 0x90
	.type	_ZN9CInBuffer9ReadBlockEv,@function
_ZN9CInBuffer9ReadBlockEv:              # @_ZN9CInBuffer9ReadBlockEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -24
.Lcfi16:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	cmpb	$0, 44(%rbx)
	je	.LBB5_2
# BB#1:
	xorl	%eax, %eax
	jmp	.LBB5_4
.LBB5_2:
	movq	(%rbx), %rax
	movq	16(%rbx), %rsi
	subq	%rsi, %rax
	addq	%rax, 32(%rbx)
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movl	40(%rbx), %edx
	leaq	4(%rsp), %rcx
	callq	*40(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB5_5
# BB#3:
	movq	16(%rbx), %rcx
	movq	%rcx, (%rbx)
	movl	4(%rsp), %eax
	addq	%rax, %rcx
	testq	%rax, %rax
	setne	%al
	movq	%rcx, 8(%rbx)
	sete	44(%rbx)
.LBB5_4:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB5_5:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	%ebp, (%rax)
	movl	$_ZTI18CInBufferException, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end5:
	.size	_ZN9CInBuffer9ReadBlockEv, .Lfunc_end5-_ZN9CInBuffer9ReadBlockEv
	.cfi_endproc

	.globl	_ZN9CInBuffer10ReadBlock2Ev
	.p2align	4, 0x90
	.type	_ZN9CInBuffer10ReadBlock2Ev,@function
_ZN9CInBuffer10ReadBlock2Ev:            # @_ZN9CInBuffer10ReadBlock2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	cmpb	$0, 44(%rbx)
	je	.LBB6_1
.LBB6_3:
	addq	$32, %rbx
	incq	(%rbx)
	movb	$-1, %al
	jmp	.LBB6_5
.LBB6_1:
	movq	(%rbx), %rax
	movq	16(%rbx), %rsi
	subq	%rsi, %rax
	addq	%rax, 32(%rbx)
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movl	40(%rbx), %edx
	leaq	4(%rsp), %rcx
	callq	*40(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB6_6
# BB#2:                                 # %_ZN9CInBuffer9ReadBlockEv.exit
	movq	16(%rbx), %rax
	movq	%rax, (%rbx)
	movl	4(%rsp), %ecx
	leaq	(%rax,%rcx), %rdx
	testq	%rcx, %rcx
	movq	%rdx, 8(%rbx)
	sete	44(%rbx)
	je	.LBB6_3
# BB#4:
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movb	(%rax), %al
.LBB6_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB6_6:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	%ebp, (%rax)
	movl	$_ZTI18CInBufferException, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end6:
	.size	_ZN9CInBuffer10ReadBlock2Ev, .Lfunc_end6-_ZN9CInBuffer10ReadBlock2Ev
	.cfi_endproc

	.type	_ZTS18CInBufferException,@object # @_ZTS18CInBufferException
	.section	.rodata._ZTS18CInBufferException,"aG",@progbits,_ZTS18CInBufferException,comdat
	.weak	_ZTS18CInBufferException
	.p2align	4
_ZTS18CInBufferException:
	.asciz	"18CInBufferException"
	.size	_ZTS18CInBufferException, 21

	.type	_ZTS16CSystemException,@object # @_ZTS16CSystemException
	.section	.rodata._ZTS16CSystemException,"aG",@progbits,_ZTS16CSystemException,comdat
	.weak	_ZTS16CSystemException
	.p2align	4
_ZTS16CSystemException:
	.asciz	"16CSystemException"
	.size	_ZTS16CSystemException, 19

	.type	_ZTI16CSystemException,@object # @_ZTI16CSystemException
	.section	.rodata._ZTI16CSystemException,"aG",@progbits,_ZTI16CSystemException,comdat
	.weak	_ZTI16CSystemException
	.p2align	3
_ZTI16CSystemException:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16CSystemException
	.size	_ZTI16CSystemException, 16

	.type	_ZTI18CInBufferException,@object # @_ZTI18CInBufferException
	.section	.rodata._ZTI18CInBufferException,"aG",@progbits,_ZTI18CInBufferException,comdat
	.weak	_ZTI18CInBufferException
	.p2align	4
_ZTI18CInBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18CInBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI18CInBufferException, 24


	.globl	_ZN9CInBufferC1Ev
	.type	_ZN9CInBufferC1Ev,@function
_ZN9CInBufferC1Ev = _ZN9CInBufferC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
