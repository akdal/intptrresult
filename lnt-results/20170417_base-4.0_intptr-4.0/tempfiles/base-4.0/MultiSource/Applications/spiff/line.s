	.text
	.file	"line.bc"
	.globl	L_init_file
	.p2align	4, 0x90
	.type	L_init_file,@function
L_init_file:                            # @L_init_file
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movl	%edi, %ebx
	movl	$.L.str, %esi
	movq	%r13, %rdi
	callq	fopen
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.LBB0_2
# BB#1:
	movl	$Z_err_buf, %edi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r13, %rdx
	callq	sprintf
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_fatal
.LBB0_2:
	testl	%ebx, %ebx
	movl	$_L_brlm, %eax
	movl	$_L_arlm, %r15d
	cmovneq	%rax, %r15
	movl	$0, (%r15)
	movl	$L_init_file.buf, %edi
	movl	$1025, %esi             # imm = 0x401
	movq	%r12, %rdx
	callq	fgets
	movl	$1, %ebp
	testq	%rax, %rax
	je	.LBB0_30
# BB#3:                                 # %.lr.ph
	movabsq	$-4294967296, %r14      # imm = 0xFFFFFFFF00000000
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	testl	%ebx, %ebx
	je	.LBB0_4
	.p2align	4, 0x90
.LBB0_16:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movl	$L_init_file.buf, %edi
	callq	strlen
	movq	%rax, %rbx
	movl	%ebx, %ebp
	testl	%ebp, %ebp
	jle	.LBB0_17
# BB#18:                                #   in Loop: Header=BB0_16 Depth=1
	cmpl	$1025, %ebp             # imm = 0x401
	jl	.LBB0_21
# BB#19:                                #   in Loop: Header=BB0_16 Depth=1
	movl	(%r15), %edx
	incl	%edx
	movl	$Z_err_buf, %edi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r13, %rcx
	movl	%ebp, %r8d
	callq	sprintf
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_17:                               #   in Loop: Header=BB0_16 Depth=1
	movl	(%r15), %edx
	incl	%edx
	movl	$Z_err_buf, %edi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r13, %rcx
	callq	sprintf
.LBB0_20:                               #   in Loop: Header=BB0_16 Depth=1
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_fatal
.LBB0_21:                               #   in Loop: Header=BB0_16 Depth=1
	shlq	$32, %rbx
	leaq	(%rbx,%r14), %rax
	sarq	$32, %rax
	cmpb	$10, L_init_file.buf(%rax)
	je	.LBB0_26
# BB#22:                                #   in Loop: Header=BB0_16 Depth=1
	movl	(%r15), %edx
	incl	%edx
	movl	$Z_err_buf, %edi
	cmpl	$1024, %ebp             # imm = 0x400
	jne	.LBB0_24
# BB#23:                                #   in Loop: Header=BB0_16 Depth=1
	movl	$.L.str.4, %esi
	movl	$1024, %r8d             # imm = 0x400
	xorl	%eax, %eax
	movq	%r13, %rcx
	callq	sprintf
	jmp	.LBB0_25
	.p2align	4, 0x90
.LBB0_24:                               #   in Loop: Header=BB0_16 Depth=1
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%r13, %rcx
	callq	sprintf
.LBB0_25:                               #   in Loop: Header=BB0_16 Depth=1
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_complain
	movq	%rbx, %rax
	sarq	$32, %rax
	movb	$10, L_init_file.buf(%rax)
	movabsq	$4294967296, %rax       # imm = 0x100000000
	addq	%rax, %rbx
	sarq	$32, %rbx
	movb	$0, L_init_file.buf(%rbx)
.LBB0_26:                               # %_L_setrline.exit
                                        #   in Loop: Header=BB0_16 Depth=1
	movslq	(%r15), %rax
	leaq	_L_bl(,%rax,8), %rdi
	movl	$L_init_file.buf, %esi
	xorl	%eax, %eax
	callq	S_savestr
	cmpl	$9999, (%r15)           # imm = 0x270F
	jge	.LBB0_27
# BB#28:                                #   in Loop: Header=BB0_16 Depth=1
	movl	_L_brlm(%rip), %eax
	incl	%eax
	movl	%eax, (%r15)
	movl	$L_init_file.buf, %edi
	movl	$1025, %esi             # imm = 0x401
	movq	%r12, %rdx
	callq	fgets
	testq	%rax, %rax
	jne	.LBB0_16
	jmp	.LBB0_29
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	$L_init_file.buf, %edi
	callq	strlen
	movq	%rax, %rbx
	movl	%ebx, %ebp
	testl	%ebp, %ebp
	jle	.LBB0_7
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=1
	cmpl	$1025, %ebp             # imm = 0x401
	jl	.LBB0_9
# BB#6:                                 #   in Loop: Header=BB0_4 Depth=1
	movl	(%r15), %edx
	incl	%edx
	movl	$Z_err_buf, %edi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r13, %rcx
	movl	%ebp, %r8d
	callq	sprintf
	jmp	.LBB0_8
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_4 Depth=1
	movl	(%r15), %edx
	incl	%edx
	movl	$Z_err_buf, %edi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r13, %rcx
	callq	sprintf
.LBB0_8:                                #   in Loop: Header=BB0_4 Depth=1
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_fatal
.LBB0_9:                                #   in Loop: Header=BB0_4 Depth=1
	shlq	$32, %rbx
	leaq	(%rbx,%r14), %rax
	sarq	$32, %rax
	cmpb	$10, L_init_file.buf(%rax)
	je	.LBB0_14
# BB#10:                                #   in Loop: Header=BB0_4 Depth=1
	movl	(%r15), %edx
	incl	%edx
	movl	$Z_err_buf, %edi
	cmpl	$1024, %ebp             # imm = 0x400
	jne	.LBB0_11
# BB#12:                                #   in Loop: Header=BB0_4 Depth=1
	movl	$.L.str.4, %esi
	movl	$1024, %r8d             # imm = 0x400
	xorl	%eax, %eax
	movq	%r13, %rcx
	callq	sprintf
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_11:                               #   in Loop: Header=BB0_4 Depth=1
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%r13, %rcx
	callq	sprintf
.LBB0_13:                               #   in Loop: Header=BB0_4 Depth=1
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_complain
	movq	%rbx, %rax
	sarq	$32, %rax
	movb	$10, L_init_file.buf(%rax)
	movabsq	$4294967296, %rax       # imm = 0x100000000
	addq	%rax, %rbx
	sarq	$32, %rbx
	movb	$0, L_init_file.buf(%rbx)
.LBB0_14:                               # %_L_setrline.exit.us
                                        #   in Loop: Header=BB0_4 Depth=1
	movslq	(%r15), %rax
	leaq	_L_al(,%rax,8), %rdi
	movl	$L_init_file.buf, %esi
	xorl	%eax, %eax
	callq	S_savestr
	cmpl	$9998, (%r15)           # imm = 0x270E
	jg	.LBB0_27
# BB#15:                                #   in Loop: Header=BB0_4 Depth=1
	movl	_L_arlm(%rip), %eax
	incl	%eax
	movl	%eax, (%r15)
	movl	$L_init_file.buf, %edi
	movl	$1025, %esi             # imm = 0x401
	movq	%r12, %rdx
	callq	fgets
	testq	%rax, %rax
	jne	.LBB0_4
.LBB0_29:
	movl	4(%rsp), %ebx           # 4-byte Reload
	movl	$1, %ebp
	jmp	.LBB0_30
.LBB0_27:                               # %.us-lcssa.us
	xorl	%ebp, %ebp
	movl	$Z_err_buf, %edi
	movl	$.L.str.6, %esi
	movl	$10000, %ecx            # imm = 0x2710
	xorl	%eax, %eax
	movq	%r13, %rdx
	callq	sprintf
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_complain
	movl	4(%rsp), %ebx           # 4-byte Reload
.LBB0_30:                               # %.loopexit
	movq	%r12, %rdi
	callq	fclose
	testl	%ebx, %ebx
	movl	$_L_bclm, %eax
	movl	$_L_aclm, %ecx
	cmovneq	%rax, %rcx
	movl	$0, (%rcx)
	movl	$_L_btlm, %eax
	movl	$_L_atlm, %ecx
	cmovneq	%rax, %rcx
	movl	$0, (%rcx)
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	L_init_file, .Lfunc_end0-L_init_file
	.cfi_endproc

	.type	L_init_file.buf,@object # @L_init_file.buf
	.local	L_init_file.buf
	.comm	L_init_file.buf,1026,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"r"
	.size	.L.str, 2

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Cannot open file %s.\n"
	.size	.L.str.1, 22

	.type	_L_brlm,@object         # @_L_brlm
	.comm	_L_brlm,4,4
	.type	_L_arlm,@object         # @_L_arlm
	.comm	_L_arlm,4,4
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"fatal error -- got 0 length line %d in file %s\n"
	.size	.L.str.2, 48

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"got fatally long line %d in file %s length is %d, must be a bug\n"
	.size	.L.str.3, 65

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"line %d too long in file %s, newline added after %d characters\n"
	.size	.L.str.4, 64

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"didn't find a newline at end of line %d in file %s, added one\n"
	.size	.L.str.5, 63

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"warning -- ran out of space reading %s, truncated to %d lines\n"
	.size	.L.str.6, 63

	.type	_L_bclm,@object         # @_L_bclm
	.comm	_L_bclm,4,4
	.type	_L_aclm,@object         # @_L_aclm
	.comm	_L_aclm,4,4
	.type	_L_btlm,@object         # @_L_btlm
	.comm	_L_btlm,4,4
	.type	_L_atlm,@object         # @_L_atlm
	.comm	_L_atlm,4,4
	.type	_L_al,@object           # @_L_al
	.comm	_L_al,80000,16
	.type	_L_bl,@object           # @_L_bl
	.comm	_L_bl,80000,16
	.type	_L_ai,@object           # @_L_ai
	.comm	_L_ai,40000,16
	.type	_L_bi,@object           # @_L_bi
	.comm	_L_bi,40000,16
	.type	_L_ac,@object           # @_L_ac
	.comm	_L_ac,40000,16
	.type	_L_bc,@object           # @_L_bc
	.comm	_L_bc,40000,16
	.type	_L_aclindex,@object     # @_L_aclindex
	.comm	_L_aclindex,40000,16
	.type	_L_bclindex,@object     # @_L_bclindex
	.comm	_L_bclindex,40000,16
	.type	_L_atlindex,@object     # @_L_atlindex
	.comm	_L_atlindex,40000,16
	.type	_L_btlindex,@object     # @_L_btlindex
	.comm	_L_btlindex,40000,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
