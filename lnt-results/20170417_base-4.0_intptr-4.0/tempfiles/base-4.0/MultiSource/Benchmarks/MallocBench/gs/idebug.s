	.text
	.file	"idebug.bc"
	.globl	debug_print_ref
	.p2align	4, 0x90
	.type	debug_print_ref,@function
debug_print_ref:                        # @debug_print_ref
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	10(%rbx), %ebp
	movzwl	8(%rbx), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movzwl	8(%rbx), %esi
	andl	$252, %esi
	shrl	$2, %esi
	cmpb	$15, %sil
	ja	.LBB0_21
# BB#1:
	jmpq	*.LJTI0_0(,%rsi,8)
.LBB0_2:
	movq	(%rbx), %rdx
	movl	$.L.str.1, %edi
	jmp	.LBB0_3
.LBB0_4:
	movzwl	(%rbx), %esi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	jmp	.LBB0_22
.LBB0_7:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	dict_length
	movl	%eax, %ebp
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	dict_maxlength
	movl	%eax, %edx
	movq	(%rbx), %rcx
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	printf                  # TAILCALL
.LBB0_8:
	movq	(%rbx), %rsi
	movl	$.L.str.5, %edi
	jmp	.LBB0_6
.LBB0_21:
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
.LBB0_22:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	printf                  # TAILCALL
.LBB0_9:
	movq	(%rbx), %rsi
	movl	$.L.str.6, %edi
	jmp	.LBB0_6
.LBB0_10:
	movl	$.L.str.7, %edi
	jmp	.LBB0_11
.LBB0_12:
	movq	(%rbx), %rsi
	movzwl	8(%rsi), %edx
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rbx), %rax
	movzwl	10(%rax), %ebx
	testq	%rbx, %rbx
	je	.LBB0_15
# BB#13:                                # %.lr.ph.preheader.i
	movq	16(%rax), %rbp
	.p2align	4, 0x90
.LBB0_14:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	incq	%rbp
	decq	%rbx
	jne	.LBB0_14
.LBB0_15:                               # %debug_print_string.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB0_16:
	movl	$.L.str.9, %edi
.LBB0_11:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	printf                  # TAILCALL
.LBB0_17:
	movq	(%rbx), %rdx
	movl	$.L.str.10, %edi
	jmp	.LBB0_3
.LBB0_18:
	movq	(%rbx), %rdx
	movl	$.L.str.11, %edi
	jmp	.LBB0_3
.LBB0_19:
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.12, %edi
	movb	$1, %al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	printf                  # TAILCALL
.LBB0_20:
	movq	(%rbx), %rdx
	movl	$.L.str.13, %edi
.LBB0_3:
	xorl	%eax, %eax
	movl	%ebp, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	printf                  # TAILCALL
.LBB0_5:
	movq	(%rbx), %rsi
	movl	$.L.str.3, %edi
.LBB0_6:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	printf                  # TAILCALL
.Lfunc_end0:
	.size	debug_print_ref, .Lfunc_end0-debug_print_ref
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_2
	.quad	.LBB0_4
	.quad	.LBB0_7
	.quad	.LBB0_8
	.quad	.LBB0_21
	.quad	.LBB0_9
	.quad	.LBB0_10
	.quad	.LBB0_12
	.quad	.LBB0_16
	.quad	.LBB0_17
	.quad	.LBB0_18
	.quad	.LBB0_19
	.quad	.LBB0_21
	.quad	.LBB0_20
	.quad	.LBB0_21
	.quad	.LBB0_5

	.text
	.globl	debug_print_string
	.p2align	4, 0x90
	.type	debug_print_string,@function
debug_print_string:                     # @debug_print_string
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testw	%si, %si
	je	.LBB1_3
# BB#1:                                 # %.lr.ph.preheader
	movzwl	%si, %r14d
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	incq	%rbx
	decq	%r14
	jne	.LBB1_2
.LBB1_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	debug_print_string, .Lfunc_end1-debug_print_string
	.cfi_endproc

	.globl	debug_dump_refs
	.p2align	4, 0x90
	.type	debug_dump_refs,@function
debug_dump_refs:                        # @debug_dump_refs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 64
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	cmpq	%r14, %r13
	jae	.LBB2_12
# BB#1:                                 # %.lr.ph.preheader
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	movq	%rdx, %rsi
	callq	printf
	movl	$45, %r12d
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_3 Depth 2
	movzwl	8(%r13), %ebx
	movl	%ebx, %edx
	shrl	$2, %edx
	andl	$63, %edx
	cmpl	$15, %edx
	movl	%edx, %ebp
	movl	$9, %eax
	cmoval	%eax, %ebp
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	printf
	movq	debug_dump_refs.ts(,%rbp,8), %rsi
	movl	$.L.str.33, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.30, %r15d
	jmp	.LBB2_3
	.p2align	4, 0x90
.LBB2_6:                                #   in Loop: Header=BB2_3 Depth=2
	incq	%r15
	shrl	%ebx
.LBB2_3:                                #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	(%r15), %edi
	cmpl	$46, %edi
	je	.LBB2_6
# BB#4:                                 #   in Loop: Header=BB2_3 Depth=2
	testb	%dil, %dil
	je	.LBB2_7
# BB#5:                                 #   in Loop: Header=BB2_3 Depth=2
	testb	$1, %bl
	cmovel	%r12d, %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	jmp	.LBB2_6
	.p2align	4, 0x90
.LBB2_7:                                #   in Loop: Header=BB2_2 Depth=1
	movzwl	10(%r13), %esi
	movq	(%r13), %rdx
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	callq	printf
	andb	$63, %bpl
	cmpb	$5, %bpl
	je	.LBB2_10
# BB#8:                                 #   in Loop: Header=BB2_2 Depth=1
	cmpb	$11, %bpl
	jne	.LBB2_11
# BB#9:                                 #   in Loop: Header=BB2_2 Depth=1
	movss	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.35, %edi
	movb	$1, %al
	callq	printf
	jmp	.LBB2_11
	.p2align	4, 0x90
.LBB2_10:                               #   in Loop: Header=BB2_2 Depth=1
	movq	(%r13), %rsi
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	callq	printf
.LBB2_11:                               #   in Loop: Header=BB2_2 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	addq	$16, %r13
	cmpq	%r14, %r13
	jb	.LBB2_2
.LBB2_12:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	debug_dump_refs, .Lfunc_end2-debug_dump_refs
	.cfi_endproc

	.globl	debug_dump_bytes
	.p2align	4, 0x90
	.type	debug_dump_bytes,@function
debug_dump_bytes:                       # @debug_dump_bytes
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 48
.Lcfi28:
	.cfi_offset %rbx, -48
.Lcfi29:
	.cfi_offset %r12, -40
.Lcfi30:
	.cfi_offset %r13, -32
.Lcfi31:
	.cfi_offset %r14, -24
.Lcfi32:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpq	%r14, %rbx
	jae	.LBB3_2
# BB#1:
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	movq	%rdx, %rsi
	callq	printf
.LBB3_2:                                # %.preheader
	cmpq	%r14, %rbx
	je	.LBB3_9
# BB#3:                                 # %.lr.ph19.preheader
	movq	%r14, %r15
	notq	%r15
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph19
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_6 Depth 2
	leaq	16(%rbx), %r12
	cmpq	%r14, %r12
	cmovaeq	%r14, %r12
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	cmpq	%r12, %rbx
	je	.LBB3_8
# BB#5:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB3_4 Depth=1
	movq	$-17, %r13
	subq	%rbx, %r13
	cmpq	%r13, %r15
	cmovaq	%r15, %r13
	negq	%r13
	incq	%rbx
	.p2align	4, 0x90
.LBB3_6:                                # %.lr.ph
                                        #   Parent Loop BB3_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rbx), %esi
	movl	$.L.str.39, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	cmpq	%rbx, %r13
	jne	.LBB3_6
# BB#7:                                 #   in Loop: Header=BB3_4 Depth=1
	movq	%r12, %rbx
.LBB3_8:                                # %._crit_edge
                                        #   in Loop: Header=BB3_4 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	cmpq	%r14, %rbx
	jne	.LBB3_4
.LBB3_9:                                # %._crit_edge20
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	debug_dump_bytes, .Lfunc_end3-debug_dump_bytes
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"(%x)"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"array(%u)0x%lx"
	.size	.L.str.1, 15

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"boolean %x"
	.size	.L.str.2, 11

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"device 0x%lx"
	.size	.L.str.3, 13

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"dict(%u/%u)0x%lx"
	.size	.L.str.4, 17

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"file 0x%lx"
	.size	.L.str.5, 11

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"int %ld"
	.size	.L.str.6, 8

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"mark"
	.size	.L.str.7, 5

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"name(0x%lx#%x)"
	.size	.L.str.8, 15

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"null"
	.size	.L.str.9, 5

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"op(%u)0x%lx"
	.size	.L.str.10, 12

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"packedarray(%u)0x%lx"
	.size	.L.str.11, 21

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"real %f"
	.size	.L.str.12, 8

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"string(%u)0x%lx"
	.size	.L.str.13, 16

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"type 0x%x"
	.size	.L.str.14, 10

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"%s:\n"
	.size	.L.str.15, 5

	.type	debug_dump_refs.ts,@object # @debug_dump_refs.ts
	.section	.rodata,"a",@progbits
	.p2align	4
debug_dump_refs.ts:
	.quad	.L.str.16
	.quad	.L.str.17
	.quad	.L.str.18
	.quad	.L.str.19
	.quad	.L.str.20
	.quad	.L.str.21
	.quad	.L.str.7
	.quad	.L.str.22
	.quad	.L.str.9
	.quad	.L.str.23
	.quad	.L.str.24
	.quad	.L.str.25
	.quad	.L.str.26
	.quad	.L.str.27
	.quad	.L.str.28
	.quad	.L.str.29
	.size	debug_dump_refs.ts, 128

	.type	.L.str.16,@object       # @.str.16
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.16:
	.asciz	"arry"
	.size	.L.str.16, 5

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"bool"
	.size	.L.str.17, 5

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"dict"
	.size	.L.str.18, 5

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"file"
	.size	.L.str.19, 5

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"font"
	.size	.L.str.20, 5

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"int "
	.size	.L.str.21, 5

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"name"
	.size	.L.str.22, 5

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"oper"
	.size	.L.str.23, 5

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"pary"
	.size	.L.str.24, 5

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"real"
	.size	.L.str.25, 5

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"save"
	.size	.L.str.26, 5

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"str "
	.size	.L.str.27, 5

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"colr"
	.size	.L.str.28, 5

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"devc"
	.size	.L.str.29, 5

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"xe......wr?????s"
	.size	.L.str.30, 17

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"0x%lx: 0x%02x "
	.size	.L.str.31, 15

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"%s "
	.size	.L.str.33, 4

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	" 0x%04x 0x%08lx"
	.size	.L.str.34, 16

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"  =  %g"
	.size	.L.str.35, 8

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"  =  %ld"
	.size	.L.str.36, 9

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"%lx:"
	.size	.L.str.38, 5

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	" %02x"
	.size	.L.str.39, 6


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
