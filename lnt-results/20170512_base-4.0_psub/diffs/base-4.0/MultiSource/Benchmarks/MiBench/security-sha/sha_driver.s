	.text
	.file	"sha_driver.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$104, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 144
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	cmpl	$2, %edi
	jge	.LBB0_1
# BB#8:
	movq	stdin(%rip), %rsi
	leaq	8(%rsp), %rbx
	movq	%rbx, %rdi
	callq	sha_stream
	movq	%rbx, %rdi
	callq	sha_print
	jmp	.LBB0_7
.LBB0_1:                                # %.preheader
	cmpl	$1, %edi
	je	.LBB0_7
# BB#2:                                 # %.lr.ph.preheader
	movl	$1, %ebp
	subl	%edi, %ebp
	addq	$8, %r15
	leaq	8(%rsp), %r14
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rdi
	movl	$.L.str, %esi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_4
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	sha_stream
	movq	%r14, %rdi
	callq	sha_print
	movq	%rbx, %rdi
	callq	fclose
	jmp	.LBB0_6
	.p2align	4, 0x90
.LBB0_4:                                #   in Loop: Header=BB0_3 Depth=1
	movq	(%r15), %rsi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
.LBB0_6:                                # %.backedge
                                        #   in Loop: Header=BB0_3 Depth=1
	addq	$8, %r15
	incl	%ebp
	jne	.LBB0_3
.LBB0_7:                                # %.loopexit
	xorl	%eax, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"rb"
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"error opening %s for reading\n"
	.size	.L.str.1, 30


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
