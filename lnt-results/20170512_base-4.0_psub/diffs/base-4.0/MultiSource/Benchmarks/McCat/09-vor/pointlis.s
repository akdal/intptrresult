	.text
	.file	"pointlis.bc"
	.globl	create_point
	.p2align	4, 0x90
	.type	create_point,@function
create_point:                           # @create_point
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$32, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB0_2
# BB#1:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movq	%rbx, 4(%rax)
	movl	CHno(%rip), %ecx
	movl	%ecx, (%rax)
	popq	%rbx
	retq
.LBB0_2:
	movl	$.Lstr, %edi
	callq	puts
	xorl	%edi, %edi
	callq	exit
.Lfunc_end0:
	.size	create_point, .Lfunc_end0-create_point
	.cfi_endproc

	.globl	point_list_insert
	.p2align	4, 0x90
	.type	point_list_insert,@function
point_list_insert:                      # @point_list_insert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -32
.Lcfi6:
	.cfi_offset %r14, -24
.Lcfi7:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	CHno(%rip), %ebp
	incl	%ebp
	movl	%ebp, CHno(%rip)
	movl	$32, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB1_5
# BB#1:                                 # %create_point.exit
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movq	%r14, 4(%rax)
	movl	%ebp, (%rax)
	movq	(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_2
# BB#3:
	movq	%rcx, 16(%rax)
	movq	24(%rcx), %rcx
	movq	%rax, 16(%rcx)
	movq	(%rbx), %rcx
	movq	24(%rcx), %rdx
	addq	$24, %rcx
	movq	%rdx, 24(%rax)
	jmp	.LBB1_4
.LBB1_2:
	movq	%rax, %rcx
	addq	$16, %rcx
	movq	%rax, 24(%rax)
.LBB1_4:
	movq	%rax, (%rcx)
	movq	%rax, (%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB1_5:
	movl	$.Lstr, %edi
	callq	puts
	xorl	%edi, %edi
	callq	exit
.Lfunc_end1:
	.size	point_list_insert, .Lfunc_end1-point_list_insert
	.cfi_endproc

	.globl	before
	.p2align	4, 0x90
	.type	before,@function
before:                                 # @before
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	retq
.Lfunc_end2:
	.size	before, .Lfunc_end2-before
	.cfi_endproc

	.globl	next
	.p2align	4, 0x90
	.type	next,@function
next:                                   # @next
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	retq
.Lfunc_end3:
	.size	next, .Lfunc_end3-next
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	angle
	.p2align	4, 0x90
	.type	angle,@function
angle:                                  # @angle
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 48
.Lcfi13:
	.cfi_offset %rbx, -40
.Lcfi14:
	.cfi_offset %r12, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	4(%r15), %rax
	movq	4(%rdi), %rsi
	movq	%rax, %rdi
	callq	vector
	movq	%rax, %r12
	movq	%r12, %rbx
	shrq	$32, %rbx
	movq	4(%r15), %rdi
	movq	4(%r14), %rsi
	callq	vector
	movq	%rax, %rcx
	shrq	$32, %rcx
	movl	%eax, %edx
	imull	%r12d, %edx
	movl	%ecx, %esi
	imull	%ebx, %esi
	addl	%edx, %esi
	cvtsi2sdl	%esi, %xmm0
	imull	%r12d, %r12d
	imull	%ebx, %ebx
	addl	%r12d, %ebx
	cvtsi2sdl	%ebx, %xmm1
	imull	%eax, %eax
	imull	%ecx, %ecx
	addl	%eax, %ecx
	cvtsi2sdl	%ecx, %xmm2
	mulsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm2
	divsd	%xmm2, %xmm0
	testl	%esi, %esi
	jns	.LBB4_2
# BB#1:
	xorpd	.LCPI4_0(%rip), %xmm0
.LBB4_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	angle, .Lfunc_end4-angle
	.cfi_endproc

	.globl	number_points
	.p2align	4, 0x90
	.type	number_points,@function
number_points:                          # @number_points
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	.p2align	4, 0x90
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rax), %rax
	cmpq	%rdi, %rax
	jne	.LBB5_1
# BB#2:
	retq
.Lfunc_end5:
	.size	number_points, .Lfunc_end5-number_points
	.cfi_endproc

	.globl	remove_points
	.p2align	4, 0x90
	.type	remove_points,@function
remove_points:                          # @remove_points
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 48
.Lcfi22:
	.cfi_offset %rbx, -48
.Lcfi23:
	.cfi_offset %r12, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	16(%r14), %r15
	jmp	.LBB6_1
	.p2align	4, 0x90
.LBB6_15:                               #   in Loop: Header=BB6_1 Depth=1
	movq	%r11, 24(%r15)
	movq	24(%rdi), %rax
	movq	%r15, 16(%rax)
	movq	16(%rdi), %r15
	callq	free
	decl	CHno(%rip)
.LBB6_1:                                # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	cmpq	%r14, %rdi
	je	.LBB6_2
# BB#3:                                 #   in Loop: Header=BB6_1 Depth=1
	movq	16(%rdi), %r15
	cmpq	%rdi, %r15
	setne	%r10b
	jmp	.LBB6_4
	.p2align	4, 0x90
.LBB6_2:                                # %.backedge._crit_edge
                                        #   in Loop: Header=BB6_1 Depth=1
	movq	16(%r14), %r15
	xorl	%r10d, %r10d
.LBB6_4:                                #   in Loop: Header=BB6_1 Depth=1
	movq	24(%rdi), %r11
	movl	4(%r11), %esi
	movl	8(%r11), %r9d
	movl	4(%rdi), %eax
	movl	8(%rdi), %r12d
	movl	4(%r15), %ecx
	movl	8(%r15), %r8d
	movl	%r12d, %ebp
	subl	%r8d, %ebp
	movl	%eax, %ebx
	subl	%esi, %ebx
	imull	%ebp, %ebx
	movl	%ecx, %edx
	subl	%eax, %edx
	movl	%r9d, %ebp
	subl	%r12d, %ebp
	imull	%edx, %ebp
	testb	%r10b, %r10b
	je	.LBB6_16
# BB#5:                                 #   in Loop: Header=BB6_1 Depth=1
	cmpl	%ebp, %ebx
	jne	.LBB6_1
# BB#6:                                 #   in Loop: Header=BB6_1 Depth=1
	cmpl	%esi, %eax
	jle	.LBB6_8
# BB#7:                                 #   in Loop: Header=BB6_1 Depth=1
	cmpl	%eax, %ecx
	jg	.LBB6_15
.LBB6_8:                                #   in Loop: Header=BB6_1 Depth=1
	cmpl	%esi, %eax
	jge	.LBB6_10
# BB#9:                                 #   in Loop: Header=BB6_1 Depth=1
	cmpl	%eax, %ecx
	jl	.LBB6_15
.LBB6_10:                               #   in Loop: Header=BB6_1 Depth=1
	cmpl	%esi, %eax
	sete	%al
	cmpl	%ecx, %esi
	sete	%dl
	andb	%al, %dl
	cmpl	%r8d, %r12d
	jge	.LBB6_13
# BB#11:                                #   in Loop: Header=BB6_1 Depth=1
	cmpl	%r12d, %r9d
	jge	.LBB6_13
# BB#12:                                #   in Loop: Header=BB6_1 Depth=1
	testb	%dl, %dl
	jne	.LBB6_15
.LBB6_13:                               #   in Loop: Header=BB6_1 Depth=1
	cmpl	%r12d, %r9d
	setg	%al
	cmpl	%r8d, %r12d
	jle	.LBB6_1
# BB#14:                                #   in Loop: Header=BB6_1 Depth=1
	andb	%dl, %al
	jne	.LBB6_15
	jmp	.LBB6_1
.LBB6_16:
	cmpl	%ebp, %ebx
	jne	.LBB6_27
# BB#17:
	cmpl	%esi, %eax
	jle	.LBB6_19
# BB#18:
	cmpl	%eax, %ecx
	jg	.LBB6_26
.LBB6_19:
	cmpl	%esi, %eax
	jge	.LBB6_21
# BB#20:
	cmpl	%eax, %ecx
	jl	.LBB6_26
.LBB6_21:
	cmpl	%esi, %eax
	sete	%al
	cmpl	%ecx, %esi
	sete	%dl
	andb	%al, %dl
	cmpl	%r8d, %r12d
	jge	.LBB6_24
# BB#22:
	cmpl	%r12d, %r9d
	jge	.LBB6_24
# BB#23:
	testb	%dl, %dl
	jne	.LBB6_26
.LBB6_24:
	cmpl	%r12d, %r9d
	setg	%al
	cmpl	%r8d, %r12d
	jle	.LBB6_27
# BB#25:
	andb	%dl, %al
	je	.LBB6_27
.LBB6_26:
	movq	%r11, 24(%r15)
	movq	24(%rdi), %rax
	movq	%r15, 16(%rax)
	movq	16(%rdi), %rbx
	callq	free
	decl	CHno(%rip)
	movq	%rbx, %rdi
.LBB6_27:
	movq	%rdi, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	remove_points, .Lfunc_end6-remove_points
	.cfi_endproc

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Can't create point"
	.size	.Lstr, 19


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
