	.text
	.file	"jcmainct.bc"
	.globl	jinit_c_main_controller
	.p2align	4, 0x90
	.type	jinit_c_main_controller,@function
jinit_c_main_controller:                # @jinit_c_main_controller
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r15
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$112, %edx
	callq	*(%rax)
	movq	%rax, %r14
	movq	%r14, 432(%r15)
	movq	$start_pass_main, (%r14)
	cmpl	$0, 248(%r15)
	je	.LBB0_1
.LBB0_5:                                # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_1:
	testl	%ebp, %ebp
	je	.LBB0_2
# BB#6:
	movq	(%r15), %rax
	movl	$4, 40(%rax)
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*(%rax)                 # TAILCALL
.LBB0_2:
	cmpl	$0, 68(%r15)
	jle	.LBB0_5
# BB#3:                                 # %.lr.ph
	movq	80(%r15), %rbp
	addq	$28, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_4:                                # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %rax
	movl	-16(%rbp), %ecx
	movl	(%rbp), %edx
	shll	$3, %edx
	shll	$3, %ecx
	movl	$1, %esi
	movq	%r15, %rdi
	callq	*16(%rax)
	movq	%rax, 32(%r14,%rbx,8)
	incq	%rbx
	movslq	68(%r15), %rax
	addq	$96, %rbp
	cmpq	%rax, %rbx
	jl	.LBB0_4
	jmp	.LBB0_5
.Lfunc_end0:
	.size	jinit_c_main_controller, .Lfunc_end0-jinit_c_main_controller
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_pass_main,@function
start_pass_main:                        # @start_pass_main
	.cfi_startproc
# BB#0:
	cmpl	$0, 248(%rdi)
	je	.LBB1_1
# BB#3:
	retq
.LBB1_1:
	movq	432(%rdi), %rax
	movl	$0, 16(%rax)
	movl	$0, 20(%rax)
	movl	$0, 24(%rax)
	movl	%esi, 28(%rax)
	testl	%esi, %esi
	je	.LBB1_2
# BB#4:
	movq	(%rdi), %rax
	movl	$4, 40(%rax)
	jmpq	*(%rax)                 # TAILCALL
.LBB1_2:
	movq	$process_data_simple_main, 8(%rax)
	retq
.Lfunc_end1:
	.size	start_pass_main, .Lfunc_end1-start_pass_main
	.cfi_endproc

	.p2align	4, 0x90
	.type	process_data_simple_main,@function
process_data_simple_main:               # @process_data_simple_main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 80
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movq	%rdx, %r13
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movq	432(%rbp), %r14
	movl	16(%r14), %eax
	cmpl	312(%rbp), %eax
	jae	.LBB2_11
# BB#1:                                 # %.lr.ph
	leaq	20(%r14), %r12
	leaq	32(%r14), %rbx
	movl	20(%r14), %eax
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	cmpl	$7, %eax
	ja	.LBB2_4
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	440(%rbp), %rax
	movl	$8, (%rsp)
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	movl	20(%rsp), %ecx          # 4-byte Reload
	movq	%rbx, %r8
	movq	%r12, %r9
	callq	*8(%rax)
	movl	(%r12), %eax
.LBB2_4:                                #   in Loop: Header=BB2_2 Depth=1
	cmpl	$8, %eax
	jne	.LBB2_11
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	448(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*8(%rax)
	movl	24(%r14), %ecx
	testl	%eax, %eax
	je	.LBB2_6
# BB#8:                                 #   in Loop: Header=BB2_2 Depth=1
	testl	%ecx, %ecx
	je	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_2 Depth=1
	incl	(%r13)
	movl	$0, 24(%r14)
.LBB2_10:                               #   in Loop: Header=BB2_2 Depth=1
	movl	$0, 20(%r14)
	movl	16(%r14), %ecx
	incl	%ecx
	movl	%ecx, 16(%r14)
	xorl	%eax, %eax
	cmpl	312(%rbp), %ecx
	jb	.LBB2_2
	jmp	.LBB2_11
.LBB2_6:
	testl	%ecx, %ecx
	jne	.LBB2_11
# BB#7:
	decl	(%r13)
	movl	$1, 24(%r14)
.LBB2_11:                               # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	process_data_simple_main, .Lfunc_end2-process_data_simple_main
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
