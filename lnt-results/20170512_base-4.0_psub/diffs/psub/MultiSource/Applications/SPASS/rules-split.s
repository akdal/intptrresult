	.text
	.file	"rules-split.bc"
	.globl	split_Backtrack
	.p2align	4, 0x90
	.type	split_Backtrack,@function
split_Backtrack:                        # @split_Backtrack
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r12
	leaq	120(%r12), %rax
	movl	128(%r12), %r13d
	movl	132(%r12), %ecx
	cmpl	%ecx, %r13d
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jle	.LBB0_1
# BB#2:                                 # %.lr.ph110.i
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	xorl	%r14d, %r14d
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB0_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_13 Depth 2
                                        #     Child Loop BB0_19 Depth 2
                                        #     Child Loop BB0_22 Depth 2
                                        #       Child Loop BB0_25 Depth 3
                                        #     Child Loop BB0_34 Depth 2
                                        #     Child Loop BB0_39 Depth 2
	movq	(%rbx), %rbx
	testl	%r13d, %r13d
	je	.LBB0_43
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	movq	8(%rbx), %rbp
	movl	4(%rbp), %eax
	testl	%eax, %eax
	jne	.LBB0_43
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	xorl	%ecx, %ecx
	cmpl	$64, %r13d
	movl	%r13d, %eax
	jb	.LBB0_7
# BB#6:                                 # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	leal	-64(%r13), %eax
	movl	%eax, %ecx
	shrl	$6, %ecx
	movl	%ecx, %edx
	shll	$6, %edx
	incl	%ecx
	subl	%edx, %eax
.LBB0_7:                                # %clause_ComputeSplitFieldAddress.exit.i.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	cmpl	24(%rdx), %ecx
	jae	.LBB0_9
# BB#8:                                 # %clause_DependsOnSplitLevel.exit.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	16(%rdx), %rdx
	movl	%ecx, %ecx
	movq	(%rdx,%rcx,8), %rcx
	btq	%rax, %rcx
	jb	.LBB0_43
.LBB0_9:                                # %clause_DependsOnSplitLevel.exit.thread.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	$0, 32(%rsp)
	movq	8(%rbp), %rdi
	callq	clause_DeleteClauseList
	movq	$0, 8(%rbp)
	movq	16(%rbp), %r15
	testq	%r15, %r15
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	je	.LBB0_10
# BB#11:                                #   in Loop: Header=BB0_3 Depth=1
	testq	%r14, %r14
	je	.LBB0_15
# BB#12:                                # %.preheader.i91.i.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB0_13:                               # %.preheader.i91.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_13
# BB#14:                                #   in Loop: Header=BB0_3 Depth=1
	movq	%r14, (%rax)
	jmp	.LBB0_15
.LBB0_10:                               #   in Loop: Header=BB0_3 Depth=1
	movq	%r14, %r15
.LBB0_15:                               # %list_Nconc.exit93.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	8(%rsp), %r14d          # 4-byte Reload
	movq	$0, 16(%rbp)
	movq	24(%rbp), %rbx
	testq	%rbx, %rbx
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	je	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movq	$0, 24(%rbp)
	movq	%rax, %r15
.LBB0_17:                               #   in Loop: Header=BB0_3 Depth=1
	movq	%r12, %rdi
	movq	%r15, %rsi
	movl	%r13d, %edx
	leaq	32(%rsp), %rcx
	callq	split_DeleteClausesDependingOnLevelFromList
	movq	%rax, %r15
	movq	120(%r12), %rbp
	testq	%rbp, %rbp
	je	.LBB0_21
# BB#18:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	24(%rsp), %rax
	.p2align	4, 0x90
.LBB0_19:                               # %.lr.ph.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rbx
	cmpl	%r14d, (%rbx)
	jle	.LBB0_20
# BB#107:                               #   in Loop: Header=BB0_19 Depth=2
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	movq	%r12, %rdi
	movl	%r13d, %edx
	leaq	32(%rsp), %rcx
	callq	split_DeleteClausesDependingOnLevelFromList
	movq	%rax, 16(%rbx)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB0_19
.LBB0_20:                               # %.critedge.preheader.loopexit.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, 24(%rsp)
.LBB0_21:                               # %.critedge.preheader.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	32(%rsp), %rsi
	testq	%rsi, %rsi
	leaq	24(%rsp), %rbx
	je	.LBB0_30
	.p2align	4, 0x90
.LBB0_22:                               # %.lr.ph103.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_25 Depth 3
	movq	$0, 24(%rsp)
	movq	%r12, %rdi
	movl	%r13d, %edx
	movq	%rbx, %rcx
	callq	split_DeleteClausesDependingOnLevelFromList
	testq	%rax, %rax
	je	.LBB0_28
# BB#23:                                #   in Loop: Header=BB0_22 Depth=2
	testq	%r15, %r15
	je	.LBB0_27
# BB#24:                                # %.preheader.i85.i.preheader
                                        #   in Loop: Header=BB0_22 Depth=2
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB0_25:                               # %.preheader.i85.i
                                        #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_25
# BB#26:                                #   in Loop: Header=BB0_22 Depth=2
	movq	%r15, (%rcx)
.LBB0_27:                               # %list_Nconc.exit87.i
                                        #   in Loop: Header=BB0_22 Depth=2
	movq	%rax, %r15
.LBB0_28:                               # %list_Nconc.exit87.i
                                        #   in Loop: Header=BB0_22 Depth=2
	movq	24(%rsp), %rsi
	testq	%rsi, %rsi
	jne	.LBB0_22
# BB#29:                                # %.critedge._crit_edge.loopexit.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	$0, 32(%rsp)
.LBB0_30:                               # %.critedge._crit_edge.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	56(%r12), %rsi
	movq	%r12, %rdi
	movl	%r13d, %edx
	callq	split_DeleteClausesDependingOnLevelFromSet
	testq	%r15, %r15
	je	.LBB0_31
# BB#32:                                #   in Loop: Header=BB0_3 Depth=1
	testq	%rax, %rax
	movq	%r15, %rcx
	je	.LBB0_33
	.p2align	4, 0x90
.LBB0_34:                               # %.preheader.i79.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_34
# BB#35:                                #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, (%rdx)
	jmp	.LBB0_36
.LBB0_31:                               #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, %r15
.LBB0_36:                               # %list_Nconc.exit81.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	40(%r12), %rsi
	movq	%r12, %rdi
	movl	%r13d, %edx
	callq	split_DeleteClausesDependingOnLevelFromSet
	movq	%rax, %r14
	testq	%r15, %r15
	jne	.LBB0_37
	jmp	.LBB0_42
.LBB0_33:                               # %list_Nconc.exit81.thread.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	40(%r12), %rsi
	movq	%r12, %rdi
	movl	%r13d, %edx
	callq	split_DeleteClausesDependingOnLevelFromSet
	movq	%rax, %r14
	movq	40(%rsp), %rbx          # 8-byte Reload
.LBB0_37:                               #   in Loop: Header=BB0_3 Depth=1
	testq	%r14, %r14
	je	.LBB0_41
# BB#38:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB0_39:                               # %.preheader.i.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_39
# BB#40:                                #   in Loop: Header=BB0_3 Depth=1
	movq	%r14, (%rax)
.LBB0_41:                               # %list_Nconc.exit.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%r15, %r14
.LBB0_42:                               # %list_Nconc.exit.i
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	$1, 4(%rax)
.LBB0_43:                               # %clause_DependsOnSplitLevel.exit.thread94.i
                                        #   in Loop: Header=BB0_3 Depth=1
	decl	%r13d
	cmpl	8(%rsp), %r13d          # 4-byte Folded Reload
	jg	.LBB0_3
	jmp	.LBB0_44
.LBB0_1:
	xorl	%r14d, %r14d
.LBB0_44:                               # %split_RemoveUnnecessarySplits.exit
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	12(%rax), %r15d
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	$0, (%rax)
	movq	56(%rsp), %r13          # 8-byte Reload
	movq	(%r13), %rax
	testq	%rax, %rax
	je	.LBB0_45
# BB#46:                                # %.lr.ph151
	movl	128(%r12), %ecx
	.p2align	4, 0x90
.LBB0_47:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_54 Depth 2
	cmpl	%r15d, %ecx
	jle	.LBB0_57
# BB#48:                                #   in Loop: Header=BB0_47 Depth=1
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	movq	%rcx, (%r13)
	movq	24(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB0_50
# BB#49:                                #   in Loop: Header=BB0_47 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	$0, 24(%rbx)
	movq	%rax, %r14
.LBB0_50:                               #   in Loop: Header=BB0_47 Depth=1
	movq	16(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB0_51
# BB#52:                                #   in Loop: Header=BB0_47 Depth=1
	testq	%r14, %r14
	je	.LBB0_56
# BB#53:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB0_47 Depth=1
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB0_54:                               # %.preheader.i
                                        #   Parent Loop BB0_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_54
# BB#55:                                #   in Loop: Header=BB0_47 Depth=1
	movq	%r14, (%rax)
	jmp	.LBB0_56
	.p2align	4, 0x90
.LBB0_51:                               #   in Loop: Header=BB0_47 Depth=1
	movq	%r14, %rbp
.LBB0_56:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB0_47 Depth=1
	movq	8(%rbx), %rdi
	callq	clause_DeleteClauseList
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rbx, (%rax)
	movl	128(%r12), %ecx
	decl	%ecx
	movl	%ecx, 128(%r12)
	movq	120(%r12), %rax
	testq	%rax, %rax
	movq	%rbp, %r14
	jne	.LBB0_47
	jmp	.LBB0_67
	.p2align	4, 0x90
.LBB0_57:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_64 Depth 2
	movq	8(%rax), %rbx
	cmpq	$0, 8(%rbx)
	jne	.LBB0_69
# BB#58:                                #   in Loop: Header=BB0_57 Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	movq	%rcx, (%r13)
	movq	24(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB0_60
# BB#59:                                #   in Loop: Header=BB0_57 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, %r14
.LBB0_60:                               #   in Loop: Header=BB0_57 Depth=1
	movq	16(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB0_61
# BB#62:                                #   in Loop: Header=BB0_57 Depth=1
	testq	%r14, %r14
	je	.LBB0_66
# BB#63:                                # %.preheader.i126.preheader
                                        #   in Loop: Header=BB0_57 Depth=1
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB0_64:                               # %.preheader.i126
                                        #   Parent Loop BB0_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_64
# BB#65:                                #   in Loop: Header=BB0_57 Depth=1
	movq	%r14, (%rax)
	jmp	.LBB0_66
	.p2align	4, 0x90
.LBB0_61:                               #   in Loop: Header=BB0_57 Depth=1
	movq	%r14, %rbp
.LBB0_66:                               # %list_Nconc.exit128
                                        #   in Loop: Header=BB0_57 Depth=1
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rbx, (%rax)
	decl	128(%r12)
	movq	120(%r12), %rax
	testq	%rax, %rax
	movq	%rbp, %r14
	jne	.LBB0_57
	jmp	.LBB0_67
.LBB0_45:
	movq	%r14, %rbp
.LBB0_67:                               # %.critedge.preheader.thread
	movq	%r12, %rdi
	callq	prfs_MoveInvalidClausesDocProof
	testq	%rbp, %rbp
	je	.LBB0_68
# BB#102:                               # %.lr.ph.preheader
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB0_103:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	callq	prfs_InsertDocProofClause
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_103
# BB#104:                               # %.lr.ph.i106.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_105:                              # %.lr.ph.i106
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB0_105
	jmp	.LBB0_106
.LBB0_68:
	xorl	%r15d, %r15d
	jmp	.LBB0_106
.LBB0_69:                               # %.critedge77
	movl	128(%r12), %ebp
	movq	8(%rax), %r13
	movq	16(%r13), %r15
	testq	%r15, %r15
	je	.LBB0_70
# BB#71:
	testq	%r14, %r14
	je	.LBB0_77
# BB#72:
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB0_73:                               # %.preheader.i120
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.LBB0_73
# BB#74:
	movq	%r14, (%rcx)
	jmp	.LBB0_75
.LBB0_70:
	movq	%r14, %r15
.LBB0_75:                               # %list_Nconc.exit122
	movq	$0, 16(%r13)
	testq	%r15, %r15
	jne	.LBB0_78
# BB#76:
	xorl	%r15d, %r15d
	jmp	.LBB0_82
.LBB0_77:                               # %list_Nconc.exit122.thread
	movq	$0, 16(%r13)
.LBB0_78:                               # %.lr.ph.i115.preheader
	decl	%ebp
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB0_79:                               # %.lr.ph.i115
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	cmpl	%ebp, 12(%rsi)
	jbe	.LBB0_81
# BB#80:                                #   in Loop: Header=BB0_79 Depth=1
	movq	%r12, %rdi
	callq	prfs_InsertDocProofClause
	movq	$0, 8(%rbx)
.LBB0_81:                               #   in Loop: Header=BB0_79 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_79
.LBB0_82:                               # %split_DeleteInvalidClausesFromList.exit
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	list_PointerDeleteElement
	movq	8(%r13), %r15
	movq	$0, 8(%r13)
	testq	%r15, %r15
	je	.LBB0_83
# BB#84:                                # %.lr.ph141
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB0_85:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	cmpl	$0, (%rax)
	jne	.LBB0_87
# BB#86:                                #   in Loop: Header=BB0_85 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	movq	8(%rbx), %rax
.LBB0_87:                               #   in Loop: Header=BB0_85 Depth=1
	movl	clause_CLAUSECOUNTER(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, clause_CLAUSECOUNTER(%rip)
	movl	%ecx, (%rax)
	movq	8(%rbx), %rbp
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	(%rax), %r13
	movq	32(%rbp), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 32(%rbp)
	movq	8(%rbx), %rbp
	movq	40(%rbp), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	$0, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 40(%rbp)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_85
# BB#88:                                # %._crit_edge142
	movq	8(%rsp), %rdx           # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB0_92
# BB#89:                                # %.preheader.i111.preheader
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB0_90:                               # %.preheader.i111
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_90
# BB#91:
	movq	%rdx, (%rax)
	jmp	.LBB0_92
.LBB0_83:
	movq	%rax, %r15
.LBB0_92:                               # %list_Nconc.exit113
	decl	128(%r12)
	movq	%r12, %rdi
	callq	prfs_MoveInvalidClausesDocProof
	movl	128(%r12), %ebx
	movq	120(%r12), %r13
	testq	%r13, %r13
	je	.LBB0_101
	.p2align	4, 0x90
.LBB0_93:                               # %.lr.ph38.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_95 Depth 2
	movq	8(%r13), %rbp
	movq	16(%rbp), %r14
	testq	%r14, %r14
	je	.LBB0_99
# BB#94:                                # %.lr.ph.i107.preheader
                                        #   in Loop: Header=BB0_93 Depth=1
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB0_95:                               # %.lr.ph.i107
                                        #   Parent Loop BB0_93 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rsi
	cmpl	%ebx, 12(%rsi)
	jbe	.LBB0_97
# BB#96:                                #   in Loop: Header=BB0_95 Depth=2
	movq	%r12, %rdi
	callq	prfs_InsertDocProofClause
	movq	$0, 8(%rbp)
.LBB0_97:                               #   in Loop: Header=BB0_95 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB0_95
# BB#98:                                # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB0_93 Depth=1
	movq	8(%r13), %rbp
.LBB0_99:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_93 Depth=1
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	list_PointerDeleteElement
	movq	%rax, 16(%rbp)
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB0_93
# BB#100:                               # %split_DeleteInvalidClausesFromStack.exit.loopexit
	movl	128(%r12), %ebx
.LBB0_101:                              # %split_DeleteInvalidClausesFromStack.exit
	incl	%ebx
	movl	%ebx, 128(%r12)
.LBB0_106:                              # %list_Delete.exit
	movl	128(%r12), %eax
	movl	%eax, 132(%r12)
	movq	%r15, %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	split_Backtrack, .Lfunc_end0-split_Backtrack
	.cfi_endproc

	.globl	split_DeleteClauseAtLevel
	.p2align	4, 0x90
	.type	split_DeleteClauseAtLevel,@function
split_DeleteClauseAtLevel:              # @split_DeleteClauseAtLevel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testb	$1, 48(%r14)
	jne	.LBB1_1
# BB#2:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	prfs_ExtractUsable
	jmp	.LBB1_3
.LBB1_1:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	prfs_ExtractWorkedOff
.LBB1_3:
	addq	$120, %rbx
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	movq	8(%rbx), %rbp
	cmpl	%r15d, (%rbp)
	jne	.LBB1_4
# BB#5:                                 # %split_KeepClauseAtLevel.exit
	movq	16(%rbp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, 16(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	split_DeleteClauseAtLevel, .Lfunc_end1-split_DeleteClauseAtLevel
	.cfi_endproc

	.globl	split_KeepClauseAtLevel
	.p2align	4, 0x90
	.type	split_KeepClauseAtLevel,@function
split_KeepClauseAtLevel:                # @split_KeepClauseAtLevel
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -32
.Lcfi26:
	.cfi_offset %r14, -24
.Lcfi27:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	addq	$120, %rdi
	.p2align	4, 0x90
.LBB2_1:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rdi
	movq	8(%rdi), %rbx
	cmpl	%edx, (%rbx)
	jne	.LBB2_1
# BB#2:                                 # %prfs_GetSplitOfLevel.exit
	movq	16(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, 16(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	split_KeepClauseAtLevel, .Lfunc_end2-split_KeepClauseAtLevel
	.cfi_endproc

	.globl	split_ExtractEmptyClauses
	.p2align	4, 0x90
	.type	split_ExtractEmptyClauses,@function
split_ExtractEmptyClauses:              # @split_ExtractEmptyClauses
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 48
.Lcfi33:
	.cfi_offset %rbx, -48
.Lcfi34:
	.cfi_offset %r12, -40
.Lcfi35:
	.cfi_offset %r13, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB3_8
# BB#1:                                 # %.lr.ph
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.LBB3_7
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	cmpl	$0, 68(%r12)
	jne	.LBB3_7
# BB#4:                                 #   in Loop: Header=BB3_2 Depth=1
	cmpl	$0, 72(%r12)
	jne	.LBB3_7
# BB#5:                                 # %clause_IsEmptyClause.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpl	$0, 64(%r12)
	jne	.LBB3_7
# BB#6:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	(%r14), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, (%r14)
	movq	$0, 8(%rbx)
	.p2align	4, 0x90
.LBB3_7:                                # %clause_IsEmptyClause.exit.thread
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_2
.LBB3_8:                                # %._crit_edge
	xorl	%esi, %esi
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	list_PointerDeleteElement # TAILCALL
.Lfunc_end3:
	.size	split_ExtractEmptyClauses, .Lfunc_end3-split_ExtractEmptyClauses
	.cfi_endproc

	.globl	split_SmallestSplitLevelClause
	.p2align	4, 0x90
	.type	split_SmallestSplitLevelClause,@function
split_SmallestSplitLevelClause:         # @split_SmallestSplitLevelClause
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rcx
.LBB4_1:                                # %.sink.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_2 Depth 2
	movq	%rcx, %rax
	.p2align	4, 0x90
.LBB4_2:                                #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=2
	movl	12(%rax), %edx
	movq	8(%rdi), %rcx
	cmpl	12(%rcx), %edx
	jbe	.LBB4_2
	jmp	.LBB4_1
.LBB4_4:
	retq
.Lfunc_end4:
	.size	split_SmallestSplitLevelClause, .Lfunc_end4-split_SmallestSplitLevelClause
	.cfi_endproc

	.p2align	4, 0x90
	.type	split_DeleteClausesDependingOnLevelFromList,@function
split_DeleteClausesDependingOnLevelFromList: # @split_DeleteClausesDependingOnLevelFromList
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 80
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, %r15
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	testq	%rsi, %rsi
	je	.LBB5_39
# BB#1:                                 # %.lr.ph
	leaq	120(%r15), %r12
	testl	%edx, %edx
	je	.LBB5_2
# BB#12:                                # %.lr.ph.split
	cmpl	$63, %edx
	jbe	.LBB5_13
# BB#26:                                # %.lr.ph.split.split.us.preheader
	leal	-64(%rdx), %ebp
	shrl	$6, %ebp
	movl	%ebp, %eax
	shll	$6, %eax
	incl	%ebp
	negl	%eax
	leal	-64(%rdx,%rax), %ecx
	movl	%ebp, %ebx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r13           # 8-byte Reload
	.p2align	4, 0x90
.LBB5_27:                               # %.lr.ph.split.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_30 Depth 2
                                        #     Child Loop BB5_34 Depth 2
	movq	8(%r13), %rsi
	cmpl	24(%rsi), %ebp
	jae	.LBB5_38
# BB#28:                                # %clause_DependsOnSplitLevel.exit.us35
                                        #   in Loop: Header=BB5_27 Depth=1
	movq	16(%rsi), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	testq	(%rax,%rbx,8), %rcx
	je	.LBB5_38
# BB#29:                                # %clause_DependsOnSplitLevel.exit.thread25.us36
                                        #   in Loop: Header=BB5_27 Depth=1
	movl	12(%rsi), %ecx
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB5_30:                               # %.lr.ph.i.us39
                                        #   Parent Loop BB5_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rdx
	movq	8(%rdx), %rax
	cmpl	%ecx, (%rax)
	jne	.LBB5_30
# BB#31:                                # %prfs_GetSplitOfLevel.exit.us45
                                        #   in Loop: Header=BB5_27 Depth=1
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB5_37
# BB#32:                                #   in Loop: Header=BB5_27 Depth=1
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	je	.LBB5_36
# BB#33:                                # %.preheader.i.us50.preheader
                                        #   in Loop: Header=BB5_27 Depth=1
	movq	%rbx, %r8
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB5_34:                               # %.preheader.i.us50
                                        #   Parent Loop BB5_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB5_34
# BB#35:                                #   in Loop: Header=BB5_27 Depth=1
	movq	%rdx, (%rbx)
	movq	%r8, %rbx
.LBB5_36:                               # %list_Nconc.exit.us55
                                        #   in Loop: Header=BB5_27 Depth=1
	movq	%rcx, (%r14)
	movq	$0, 16(%rax)
.LBB5_37:                               #   in Loop: Header=BB5_27 Depth=1
	movq	%r15, %rdi
	callq	prfs_InsertDocProofClause
	movq	$0, 8(%r13)
.LBB5_38:                               # %clause_DependsOnSplitLevel.exit.thread.us56
                                        #   in Loop: Header=BB5_27 Depth=1
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB5_27
	jmp	.LBB5_39
.LBB5_2:                                # %.lr.ph.split.us.preheader
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_4 Depth 2
                                        #     Child Loop BB5_8 Depth 2
	movq	8(%rbp), %rsi
	movl	12(%rsi), %ecx
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB5_4:                                # %.lr.ph.i.us
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rdx
	movq	8(%rdx), %rax
	cmpl	%ecx, (%rax)
	jne	.LBB5_4
# BB#5:                                 # %prfs_GetSplitOfLevel.exit.us
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB5_11
# BB#6:                                 #   in Loop: Header=BB5_3 Depth=1
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	je	.LBB5_10
# BB#7:                                 # %.preheader.i.us.preheader
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB5_8:                                # %.preheader.i.us
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB5_8
# BB#9:                                 #   in Loop: Header=BB5_3 Depth=1
	movq	%rdx, (%rbx)
.LBB5_10:                               # %list_Nconc.exit.us
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	%rcx, (%r14)
	movq	$0, 16(%rax)
.LBB5_11:                               # %clause_DependsOnSplitLevel.exit.thread.us
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	%r15, %rdi
	callq	prfs_InsertDocProofClause
	movq	$0, 8(%rbp)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB5_3
	jmp	.LBB5_39
.LBB5_13:                               # %.lr.ph.split.split.preheader
	movl	$1, %r13d
	movl	%edx, %ecx
	shlq	%cl, %r13
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB5_14:                               # %.lr.ph.split.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_17 Depth 2
                                        #     Child Loop BB5_21 Depth 2
	movq	8(%rbp), %rsi
	cmpl	$0, 24(%rsi)
	je	.LBB5_25
# BB#15:                                # %clause_DependsOnSplitLevel.exit
                                        #   in Loop: Header=BB5_14 Depth=1
	movq	16(%rsi), %rax
	testq	(%rax), %r13
	je	.LBB5_25
# BB#16:                                # %clause_DependsOnSplitLevel.exit.thread25
                                        #   in Loop: Header=BB5_14 Depth=1
	movl	12(%rsi), %ecx
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB5_17:                               # %.lr.ph.i
                                        #   Parent Loop BB5_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rdx
	movq	8(%rdx), %rax
	cmpl	%ecx, (%rax)
	jne	.LBB5_17
# BB#18:                                # %prfs_GetSplitOfLevel.exit
                                        #   in Loop: Header=BB5_14 Depth=1
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB5_24
# BB#19:                                #   in Loop: Header=BB5_14 Depth=1
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	je	.LBB5_23
# BB#20:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB5_14 Depth=1
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB5_21:                               # %.preheader.i
                                        #   Parent Loop BB5_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB5_21
# BB#22:                                #   in Loop: Header=BB5_14 Depth=1
	movq	%rdx, (%rbx)
.LBB5_23:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB5_14 Depth=1
	movq	%rcx, (%r14)
	movq	$0, 16(%rax)
.LBB5_24:                               #   in Loop: Header=BB5_14 Depth=1
	movq	%r15, %rdi
	callq	prfs_InsertDocProofClause
	movq	$0, 8(%rbp)
.LBB5_25:                               # %clause_DependsOnSplitLevel.exit.thread
                                        #   in Loop: Header=BB5_14 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB5_14
.LBB5_39:                               # %._crit_edge
	xorl	%esi, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	list_PointerDeleteElement # TAILCALL
.Lfunc_end5:
	.size	split_DeleteClausesDependingOnLevelFromList, .Lfunc_end5-split_DeleteClausesDependingOnLevelFromList
	.cfi_endproc

	.p2align	4, 0x90
	.type	split_DeleteClausesDependingOnLevelFromSet,@function
split_DeleteClausesDependingOnLevelFromSet: # @split_DeleteClausesDependingOnLevelFromSet
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi57:
	.cfi_def_cfa_offset 96
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rsi, %r12
	testq	%r12, %r12
	je	.LBB6_1
# BB#2:                                 # %.lr.ph44
	leaq	120(%rdi), %r13
	testl	%edx, %edx
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	je	.LBB6_3
# BB#13:                                # %.lr.ph44.split
	cmpl	$63, %edx
	movq	%r13, 8(%rsp)           # 8-byte Spill
	jbe	.LBB6_14
# BB#21:                                # %.lr.ph44.split.split.us.preheader
	leal	-64(%rdx), %ebp
	shrl	$6, %ebp
	movl	%ebp, %eax
	shll	$6, %eax
	incl	%ebp
	negl	%eax
	leal	-64(%rdx,%rax), %ecx
	movl	%ebp, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_22:                               # %.lr.ph44.split.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_25 Depth 2
                                        #     Child Loop BB6_30 Depth 2
	movq	8(%r12), %r14
	cmpl	24(%r14), %ebp
	jae	.LBB6_33
# BB#23:                                # %clause_DependsOnSplitLevel.exit.us55
                                        #   in Loop: Header=BB6_22 Depth=1
	movq	16(%r14), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	testq	(%rax,%rcx,8), %rdx
	je	.LBB6_33
# BB#24:                                # %clause_DependsOnSplitLevel.exit.thread35.us56
                                        #   in Loop: Header=BB6_22 Depth=1
	movl	12(%r14), %ecx
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB6_25:                               # %.lr.ph.i.us61
                                        #   Parent Loop BB6_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rdx
	movq	8(%rdx), %rax
	cmpl	%ecx, (%rax)
	jne	.LBB6_25
# BB#26:                                # %prfs_GetSplitOfLevel.exit.us67
                                        #   in Loop: Header=BB6_22 Depth=1
	movq	16(%rax), %r13
	testq	%r13, %r13
	je	.LBB6_27
# BB#28:                                #   in Loop: Header=BB6_22 Depth=1
	testq	%r15, %r15
	je	.LBB6_32
# BB#29:                                # %.preheader.i.us72.preheader
                                        #   in Loop: Header=BB6_22 Depth=1
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB6_30:                               # %.preheader.i.us72
                                        #   Parent Loop BB6_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB6_30
# BB#31:                                #   in Loop: Header=BB6_22 Depth=1
	movq	%r15, (%rcx)
	jmp	.LBB6_32
.LBB6_27:                               #   in Loop: Header=BB6_22 Depth=1
	movq	%r15, %r13
.LBB6_32:                               # %list_Nconc.exit.us77
                                        #   in Loop: Header=BB6_22 Depth=1
	movq	$0, 16(%rax)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, %rbx
	movq	%r13, %r15
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB6_33:                               # %clause_DependsOnSplitLevel.exit.thread.us79
                                        #   in Loop: Header=BB6_22 Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB6_22
	jmp	.LBB6_34
.LBB6_1:
	xorl	%r15d, %r15d
	jmp	.LBB6_46
.LBB6_3:                                # %.lr.ph44.split.us.preheader
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_4:                                # %.lr.ph44.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_5 Depth 2
                                        #     Child Loop BB6_10 Depth 2
	movq	8(%r12), %r14
	movl	12(%r14), %edx
	movq	%r13, %rsi
	.p2align	4, 0x90
.LBB6_5:                                # %.lr.ph.i.us
                                        #   Parent Loop BB6_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rsi
	movq	8(%rsi), %rcx
	cmpl	%edx, (%rcx)
	jne	.LBB6_5
# BB#6:                                 # %prfs_GetSplitOfLevel.exit.us
                                        #   in Loop: Header=BB6_4 Depth=1
	movq	16(%rcx), %r15
	testq	%r15, %r15
	je	.LBB6_7
# BB#8:                                 #   in Loop: Header=BB6_4 Depth=1
	testq	%rax, %rax
	je	.LBB6_12
# BB#9:                                 # %.preheader.i.us.preheader
                                        #   in Loop: Header=BB6_4 Depth=1
	movq	%r15, %rsi
	.p2align	4, 0x90
.LBB6_10:                               # %.preheader.i.us
                                        #   Parent Loop BB6_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB6_10
# BB#11:                                #   in Loop: Header=BB6_4 Depth=1
	movq	%rax, (%rdx)
	jmp	.LBB6_12
	.p2align	4, 0x90
.LBB6_7:                                #   in Loop: Header=BB6_4 Depth=1
	movq	%rax, %r15
.LBB6_12:                               # %list_Nconc.exit.us
                                        #   in Loop: Header=BB6_4 Depth=1
	movq	$0, 16(%rcx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r14, 8(%rbx)
	movq	%rbp, (%rbx)
	movq	(%r12), %r12
	testq	%r12, %r12
	movq	%r15, %rax
	movq	%rbx, %rbp
	jne	.LBB6_4
	jmp	.LBB6_34
.LBB6_14:                               # %.lr.ph44.split.split.preheader
	movl	$1, %r14d
	movl	%edx, %ecx
	shlq	%cl, %r14
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_15:                               # %.lr.ph44.split.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_18 Depth 2
                                        #     Child Loop BB6_40 Depth 2
	movq	8(%r12), %rbp
	cmpl	$0, 24(%rbp)
	je	.LBB6_43
# BB#16:                                # %clause_DependsOnSplitLevel.exit
                                        #   in Loop: Header=BB6_15 Depth=1
	movq	16(%rbp), %rax
	testq	(%rax), %r14
	je	.LBB6_43
# BB#17:                                # %clause_DependsOnSplitLevel.exit.thread35
                                        #   in Loop: Header=BB6_15 Depth=1
	movl	12(%rbp), %ecx
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB6_18:                               # %.lr.ph.i
                                        #   Parent Loop BB6_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rdx
	movq	8(%rdx), %rax
	cmpl	%ecx, (%rax)
	jne	.LBB6_18
# BB#19:                                # %prfs_GetSplitOfLevel.exit
                                        #   in Loop: Header=BB6_15 Depth=1
	movq	16(%rax), %r13
	testq	%r13, %r13
	je	.LBB6_20
# BB#38:                                #   in Loop: Header=BB6_15 Depth=1
	testq	%r15, %r15
	je	.LBB6_42
# BB#39:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB6_15 Depth=1
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB6_40:                               # %.preheader.i
                                        #   Parent Loop BB6_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB6_40
# BB#41:                                #   in Loop: Header=BB6_15 Depth=1
	movq	%r15, (%rcx)
	jmp	.LBB6_42
.LBB6_20:                               #   in Loop: Header=BB6_15 Depth=1
	movq	%r15, %r13
.LBB6_42:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB6_15 Depth=1
	movq	$0, 16(%rax)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, %rbx
	movq	%r13, %r15
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB6_43:                               # %clause_DependsOnSplitLevel.exit.thread
                                        #   in Loop: Header=BB6_15 Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB6_15
.LBB6_34:                               # %.preheader
	testq	%rbx, %rbx
	je	.LBB6_46
# BB#35:
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_36:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	testb	$1, 48(%rsi)
	jne	.LBB6_37
# BB#44:                                #   in Loop: Header=BB6_36 Depth=1
	movq	%rbp, %rdi
	callq	prfs_MoveUsableDocProof
	jmp	.LBB6_45
	.p2align	4, 0x90
.LBB6_37:                               #   in Loop: Header=BB6_36 Depth=1
	movq	%rbp, %rdi
	callq	prfs_MoveWorkedOffDocProof
.LBB6_45:                               #   in Loop: Header=BB6_36 Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB6_36
.LBB6_46:                               # %._crit_edge
	movq	%r15, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	split_DeleteClausesDependingOnLevelFromSet, .Lfunc_end6-split_DeleteClausesDependingOnLevelFromSet
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
