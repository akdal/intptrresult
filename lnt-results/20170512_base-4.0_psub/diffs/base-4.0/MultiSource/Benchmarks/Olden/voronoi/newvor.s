	.text
	.file	"newvor.bc"
	.globl	connect_left
	.p2align	4, 0x90
	.type	connect_left,@function
connect_left:                           # @connect_left
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rax
	xorq	$64, %rax
	movq	(%rax), %rax
	leal	96(%rdi), %ecx
	andl	$127, %ecx
	andq	$-128, %rdi
	movq	8(%rcx,%rdi), %rbx
	leal	32(%rbx), %r15d
	andl	$127, %r15d
	andq	$-128, %rbx
	movq	(%r14), %rsi
	movq	%rax, %rdi
	callq	makeedge
	movq	8(%rax), %rcx
	leal	32(%rcx), %edx
	andl	$127, %edx
	andq	$-128, %rcx
	movq	8(%r15,%rbx), %rsi
	leal	32(%rsi), %edi
	andl	$127, %edi
	andq	$-128, %rsi
	movq	8(%rdi,%rsi), %r8
	movq	8(%rdx,%rcx), %r9
	movq	%r8, 8(%rdx,%rcx)
	movq	%r9, 8(%rdi,%rsi)
	movq	8(%rax), %rcx
	movq	8(%r15,%rbx), %rdx
	movq	%rcx, 8(%r15,%rbx)
	movq	%rdx, 8(%rax)
	movq	%rax, %rcx
	xorq	$64, %rcx
	movq	8(%rcx), %rdx
	leal	32(%rdx), %esi
	andl	$127, %esi
	andq	$-128, %rdx
	movq	8(%r14), %rdi
	leal	32(%rdi), %ebx
	andl	$127, %ebx
	andq	$-128, %rdi
	movq	8(%rbx,%rdi), %r8
	movq	8(%rsi,%rdx), %r9
	movq	%r8, 8(%rsi,%rdx)
	movq	%r9, 8(%rbx,%rdi)
	movq	8(%rcx), %rdx
	movq	8(%r14), %rsi
	movq	%rdx, 8(%r14)
	movq	%rsi, 8(%rcx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	connect_left, .Lfunc_end0-connect_left
	.cfi_endproc

	.globl	connect_right
	.p2align	4, 0x90
	.type	connect_right,@function
connect_right:                          # @connect_right
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	xorq	$64, %rbx
	movq	(%rbx), %rdi
	movq	(%rsi), %rax
	leal	32(%rsi), %ecx
	andl	$127, %ecx
	andq	$-128, %rsi
	movq	8(%rcx,%rsi), %r14
	leal	32(%r14), %r15d
	andl	$127, %r15d
	andq	$-128, %r14
	movq	%rax, %rsi
	callq	makeedge
	movq	8(%rax), %rcx
	leal	32(%rcx), %edx
	andl	$127, %edx
	andq	$-128, %rcx
	movq	8(%rbx), %rsi
	leal	32(%rsi), %edi
	andl	$127, %edi
	andq	$-128, %rsi
	movq	8(%rdi,%rsi), %r8
	movq	8(%rdx,%rcx), %r9
	movq	%r8, 8(%rdx,%rcx)
	movq	%r9, 8(%rdi,%rsi)
	movq	8(%rax), %rcx
	movq	8(%rbx), %rdx
	movq	%rcx, 8(%rbx)
	movq	%rdx, 8(%rax)
	movq	%rax, %rcx
	xorq	$64, %rcx
	movq	8(%rcx), %rdx
	leal	32(%rdx), %esi
	andl	$127, %esi
	andq	$-128, %rdx
	movq	8(%r15,%r14), %rdi
	leal	32(%rdi), %ebx
	andl	$127, %ebx
	andq	$-128, %rdi
	movq	8(%rbx,%rdi), %r8
	movq	8(%rsi,%rdx), %r9
	movq	%r8, 8(%rsi,%rdx)
	movq	%r9, 8(%rbx,%rdi)
	movq	8(%rcx), %rdx
	movq	8(%r15,%r14), %rsi
	movq	%rdx, 8(%r15,%r14)
	movq	%rsi, 8(%rcx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	connect_right, .Lfunc_end1-connect_right
	.cfi_endproc

	.globl	deleteedge
	.p2align	4, 0x90
	.type	deleteedge,@function
deleteedge:                             # @deleteedge
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	leal	32(%rdi), %ecx
	andl	$127, %ecx
	movq	%rdi, %r8
	andq	$-128, %r8
	movq	8(%rcx,%r8), %rcx
	leal	32(%rcx), %r9d
	andl	$127, %r9d
	andq	$-128, %rcx
	movq	8(%rdi), %rsi
	leal	32(%rsi), %r10d
	andl	$127, %r10d
	andq	$-128, %rsi
	movq	8(%r9,%rcx), %rdx
	leal	32(%rdx), %eax
	andl	$127, %eax
	andq	$-128, %rdx
	movq	8(%rax,%rdx), %r11
	movq	8(%r10,%rsi), %rbx
	movq	%r11, 8(%r10,%rsi)
	movq	%rbx, 8(%rax,%rdx)
	movq	8(%rdi), %rax
	movq	8(%r9,%rcx), %rdx
	movq	%rax, 8(%r9,%rcx)
	movq	%rdx, 8(%rdi)
	xorq	$64, %rdi
	leal	32(%rdi), %eax
	andl	$127, %eax
	movq	8(%rax,%r8), %rcx
	leal	32(%rcx), %r9d
	andl	$127, %r9d
	andq	$-128, %rcx
	movq	8(%rdi), %rax
	leal	32(%rax), %esi
	andl	$127, %esi
	andq	$-128, %rax
	movq	8(%r9,%rcx), %rbx
	leal	32(%rbx), %edx
	andl	$127, %edx
	andq	$-128, %rbx
	movq	8(%rdx,%rbx), %r10
	movq	8(%rsi,%rax), %r11
	movq	%r10, 8(%rsi,%rax)
	movq	%r11, 8(%rdx,%rbx)
	movq	8(%rdi), %rax
	movq	8(%r9,%rcx), %rdx
	movq	%rax, 8(%r9,%rcx)
	movq	%rdx, 8(%rdi)
	movq	avail_edge(%rip), %rax
	movq	%rax, 8(%r8)
	movq	%r8, avail_edge(%rip)
	popq	%rbx
	retq
.Lfunc_end2:
	.size	deleteedge, .Lfunc_end2-deleteedge
	.cfi_endproc

	.globl	free_edge
	.p2align	4, 0x90
	.type	free_edge,@function
free_edge:                              # @free_edge
	.cfi_startproc
# BB#0:
	andq	$-128, %rdi
	movq	avail_edge(%rip), %rax
	movq	%rax, 8(%rdi)
	movq	%rdi, avail_edge(%rip)
	retq
.Lfunc_end3:
	.size	free_edge, .Lfunc_end3-free_edge
	.cfi_endproc

	.globl	build_delaunay_triangulation
	.p2align	4, 0x90
	.type	build_delaunay_triangulation,@function
build_delaunay_triangulation:           # @build_delaunay_triangulation
	.cfi_startproc
# BB#0:
	jmp	build_delaunay          # TAILCALL
.Lfunc_end4:
	.size	build_delaunay_triangulation, .Lfunc_end4-build_delaunay_triangulation
	.cfi_endproc

	.globl	build_delaunay
	.p2align	4, 0x90
	.type	build_delaunay,@function
build_delaunay:                         # @build_delaunay
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 64
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB5_8
# BB#1:
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_9
# BB#2:                                 # %.preheader91.preheader
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB5_3:                                # %.preheader91
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB5_3
# BB#4:                                 # %get_low.exit
	movq	%r14, %rsi
	callq	build_delaunay
	movq	%rax, %rbp
	movq	%rdx, %r12
	movq	24(%r15), %rdi
	movq	%r15, %rsi
	callq	build_delaunay
	movq	%rax, %rdi
	movq	%rdx, %rsi
	movq	%rbp, %rdx
	movq	%r12, %rcx
	callq	do_merge
	movq	%rax, %rbp
	cmpq	%rbx, (%rbp)
	je	.LBB5_6
	.p2align	4, 0x90
.LBB5_17:                               # %.lr.ph94
                                        # =>This Inner Loop Header: Depth=1
	xorq	$64, %rbp
	movq	8(%rbp), %rbp
	cmpq	%rbx, (%rbp)
	jne	.LBB5_17
	jmp	.LBB5_6
	.p2align	4, 0x90
.LBB5_7:                                # %.lr.ph
                                        #   in Loop: Header=BB5_6 Depth=1
	movq	8(%rdx), %rdx
	xorq	$64, %rdx
.LBB5_6:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r14, (%rdx)
	jne	.LBB5_7
.LBB5_16:                               # %.loopexit
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_9:
	movq	24(%r15), %r12
	testq	%r12, %r12
	je	.LBB5_10
# BB#11:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	makeedge
	movq	%rax, %rbp
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	makeedge
	movq	%rbp, %rcx
	xorq	$64, %rcx
	movq	8(%rcx), %rdx
	leal	32(%rdx), %esi
	andl	$127, %esi
	andq	$-128, %rdx
	movq	8(%rax), %rdi
	leal	32(%rdi), %ebx
	andl	$127, %ebx
	andq	$-128, %rdi
	movq	8(%rbx,%rdi), %r8
	movq	8(%rsi,%rdx), %r9
	movq	%r8, 8(%rsi,%rdx)
	movq	%r9, 8(%rbx,%rdi)
	movq	8(%rcx), %rdx
	movq	8(%rax), %rsi
	movq	%rdx, 8(%rax)
	movq	%rsi, 8(%rcx)
	movq	%rax, %rcx
	xorq	$64, %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	(%rcx), %rdi
	leal	96(%rax), %ecx
	andl	$127, %ecx
	andq	$-128, %rax
	movq	8(%rcx,%rax), %rbx
	leal	32(%rbx), %r13d
	andl	$127, %r13d
	andq	$-128, %rbx
	movq	(%rbp), %rsi
	callq	makeedge
	movq	%rax, %rdx
	movq	8(%rdx), %rax
	leal	32(%rax), %ecx
	andl	$127, %ecx
	andq	$-128, %rax
	movq	8(%r13,%rbx), %rsi
	leal	32(%rsi), %edi
	andl	$127, %edi
	andq	$-128, %rsi
	movq	8(%rdi,%rsi), %r8
	movq	8(%rcx,%rax), %r9
	movq	%r8, 8(%rcx,%rax)
	movq	%r9, 8(%rdi,%rsi)
	movq	8(%rdx), %rax
	movq	8(%r13,%rbx), %rcx
	movq	%rax, 8(%r13,%rbx)
	movq	%rcx, 8(%rdx)
	movq	%rdx, %r8
	xorq	$64, %r8
	movq	8(%r8), %rcx
	leal	32(%rcx), %esi
	andl	$127, %esi
	andq	$-128, %rcx
	movq	8(%rbp), %rdi
	leal	32(%rdi), %ebx
	andl	$127, %ebx
	andq	$-128, %rdi
	movq	8(%rbx,%rdi), %rax
	movq	8(%rsi,%rcx), %r9
	movq	%rax, 8(%rsi,%rcx)
	movq	%r9, 8(%rbx,%rdi)
	movq	8(%r8), %rax
	movq	8(%rbp), %rcx
	movq	%rax, 8(%rbp)
	movq	%rcx, 8(%r8)
	movupd	(%r12), %xmm0
	movupd	(%r14), %xmm1
	movsd	(%r15), %xmm3           # xmm3 = mem[0],zero
	movsd	8(%r15), %xmm2          # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm5
	unpcklpd	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0]
	movaps	%xmm3, %xmm4
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	subpd	%xmm4, %xmm5
	movapd	%xmm1, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	movapd	%xmm0, %xmm6
	movsd	%xmm4, %xmm6            # xmm6 = xmm4[0],xmm6[1]
	movaps	%xmm2, %xmm7
	movlhps	%xmm7, %xmm7            # xmm7 = xmm7[0,0]
	subpd	%xmm7, %xmm6
	mulpd	%xmm5, %xmm6
	movapd	%xmm6, %xmm5
	movhlps	%xmm5, %xmm5            # xmm5 = xmm5[1,1]
	subsd	%xmm5, %xmm6
	xorps	%xmm5, %xmm5
	ucomisd	%xmm5, %xmm6
	jbe	.LBB5_13
# BB#12:
	movq	%r8, %rbp
	jmp	.LBB5_16
.LBB5_10:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	makeedge
	movq	%rax, %rbp
	movq	%rbp, %rdx
	xorq	$64, %rdx
	jmp	.LBB5_16
.LBB5_13:
	unpcklpd	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0]
	unpcklpd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0]
	subpd	%xmm4, %xmm2
	subpd	%xmm1, %xmm0
	mulpd	%xmm2, %xmm0
	movapd	%xmm0, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	subsd	%xmm1, %xmm0
	ucomisd	%xmm5, %xmm0
	ja	.LBB5_15
# BB#14:
	leal	32(%rdx), %eax
	andl	$127, %eax
	movq	%rdx, %r9
	andq	$-128, %r9
	movq	8(%rax,%r9), %rsi
	leal	32(%rsi), %r10d
	andl	$127, %r10d
	andq	$-128, %rsi
	movq	8(%rdx), %rax
	leal	32(%rax), %ebx
	andl	$127, %ebx
	andq	$-128, %rax
	movq	8(%r10,%rsi), %rcx
	leal	32(%rcx), %edi
	andl	$127, %edi
	andq	$-128, %rcx
	movq	8(%rdi,%rcx), %r11
	movq	8(%rbx,%rax), %r14
	movq	%r11, 8(%rbx,%rax)
	movq	%r14, 8(%rdi,%rcx)
	movq	8(%rdx), %rax
	movq	8(%r10,%rsi), %rcx
	movq	%rax, 8(%r10,%rsi)
	movq	%rcx, 8(%rdx)
	leal	32(%r8), %eax
	andl	$127, %eax
	movq	8(%rax,%r9), %rdx
	leal	32(%rdx), %esi
	andl	$127, %esi
	andq	$-128, %rdx
	movq	8(%r8), %rax
	leal	32(%rax), %ecx
	andl	$127, %ecx
	andq	$-128, %rax
	movq	8(%rsi,%rdx), %rdi
	leal	32(%rdi), %ebx
	andl	$127, %ebx
	andq	$-128, %rdi
	movq	8(%rbx,%rdi), %r10
	movq	8(%rcx,%rax), %r11
	movq	%r10, 8(%rcx,%rax)
	movq	%r11, 8(%rbx,%rdi)
	movq	8(%r8), %rax
	movq	8(%rsi,%rdx), %rcx
	movq	%rax, 8(%rsi,%rdx)
	movq	%rcx, 8(%r8)
	movq	avail_edge(%rip), %rax
	movq	%rax, 8(%r9)
	movq	%r9, avail_edge(%rip)
.LBB5_15:                               # %.loopexit
	movq	(%rsp), %rdx            # 8-byte Reload
	jmp	.LBB5_16
.LBB5_8:                                # %.critedge
	movl	$.Lstr, %edi
	callq	puts
	movl	$-1, %edi
	callq	exit
.Lfunc_end5:
	.size	build_delaunay, .Lfunc_end5-build_delaunay
	.cfi_endproc

	.globl	get_low
	.p2align	4, 0x90
	.type	get_low,@function
get_low:                                # @get_low
	.cfi_startproc
# BB#0:
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rax
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	jne	.LBB6_1
# BB#2:
	retq
.Lfunc_end6:
	.size	get_low, .Lfunc_end6-get_low
	.cfi_endproc

	.globl	do_merge
	.p2align	4, 0x90
	.type	do_merge,@function
do_merge:                               # @do_merge
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 128
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	xorpd	%xmm0, %xmm0
	jmp	.LBB7_1
	.p2align	4, 0x90
.LBB7_5:                                # %.thread
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	8(%rax), %rdx
.LBB7_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_3 Depth 2
	movq	(%rdx), %rdi
	movups	(%rdi), %xmm1
	movq	%rbx, %rax
	xorq	$64, %rax
	movq	(%rax), %rax
	movq	(%rbx), %rsi
	movupd	(%rsi), %xmm4
	movapd	%xmm4, %xmm5
	movhpd	(%rax), %xmm5           # xmm5 = xmm5[0],mem[0]
	movaps	%xmm1, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	subpd	%xmm2, %xmm5
	movapd	%xmm4, %xmm6
	movlpd	8(%rax), %xmm6          # xmm6 = mem[0],xmm6[1]
	movaps	%xmm1, %xmm3
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	jmp	.LBB7_3
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph
                                        #   in Loop: Header=BB7_3 Depth=2
	leal	96(%rbx), %eax
	andl	$127, %eax
	andq	$-128, %rbx
	movq	8(%rax,%rbx), %rax
	leal	32(%rax), %ecx
	andl	$127, %ecx
	andq	$-128, %rax
	leaq	(%rcx,%rax), %rbx
	movq	%rbx, %rsi
	xorq	$64, %rsi
	movq	(%rsi), %rbp
	movq	(%rcx,%rax), %rsi
	movupd	(%rsi), %xmm4
	movapd	%xmm4, %xmm5
	movhpd	(%rbp), %xmm5           # xmm5 = xmm5[0],mem[0]
	subpd	%xmm2, %xmm5
	movapd	%xmm4, %xmm6
	movlpd	8(%rbp), %xmm6          # xmm6 = mem[0],xmm6[1]
.LBB7_3:                                # %.lr.ph
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	subpd	%xmm3, %xmm6
	mulpd	%xmm5, %xmm6
	movapd	%xmm6, %xmm5
	movhlps	%xmm5, %xmm5            # xmm5 = xmm5[1,1]
	subsd	%xmm5, %xmm6
	ucomisd	%xmm0, %xmm6
	ja	.LBB7_2
# BB#4:                                 # %._crit_edge
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	%rdx, %rax
	xorq	$64, %rax
	movq	(%rax), %rcx
	subpd	%xmm4, %xmm1
	movsd	8(%rcx), %xmm2          # xmm2 = mem[0],zero
	movhpd	(%rcx), %xmm2           # xmm2 = xmm2[0],mem[0]
	shufpd	$1, %xmm4, %xmm4        # xmm4 = xmm4[1,0]
	subpd	%xmm4, %xmm2
	mulpd	%xmm1, %xmm2
	movapd	%xmm2, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	subsd	%xmm2, %xmm1
	ucomisd	%xmm0, %xmm1
	ja	.LBB7_5
# BB#6:
	addl	$96, %eax
	andl	$127, %eax
	andq	$-128, %rdx
	movq	8(%rax,%rdx), %rbp
	leal	32(%rbp), %r14d
	andl	$127, %r14d
	andq	$-128, %rbp
	callq	makeedge
	movq	%rax, %r10
	movq	8(%r10), %rax
	leal	32(%rax), %ecx
	andl	$127, %ecx
	andq	$-128, %rax
	movq	8(%r14,%rbp), %rdx
	leal	32(%rdx), %esi
	andl	$127, %esi
	andq	$-128, %rdx
	movq	8(%rsi,%rdx), %r9
	movq	8(%rcx,%rax), %r8
	movq	%r9, 8(%rcx,%rax)
	movq	%r8, 8(%rsi,%rdx)
	movq	8(%r10), %rax
	movq	8(%r14,%rbp), %rcx
	movq	%rax, 8(%r14,%rbp)
	movq	%rcx, 8(%r10)
	movq	%r10, %rdi
	xorq	$64, %rdi
	movq	8(%rdi), %rax
	leal	32(%rax), %ecx
	andl	$127, %ecx
	andq	$-128, %rax
	movq	8(%rbx), %rdx
	leal	32(%rdx), %esi
	andl	$127, %esi
	andq	$-128, %rdx
	movq	8(%rsi,%rdx), %r8
	movq	8(%rcx,%rax), %rbp
	movq	%r8, 8(%rcx,%rax)
	movq	%rbp, 8(%rsi,%rdx)
	movq	8(%rdi), %rax
	movq	8(%rbx), %r13
	movq	%rax, 8(%rbx)
	movq	%r13, 8(%rdi)
	leal	32(%r10), %eax
	andl	$127, %eax
	movq	%r10, %rcx
	andq	$-128, %rcx
	movq	8(%rax,%rcx), %r12
	leal	32(%r12), %eax
	andl	$127, %eax
	andq	$-128, %r12
	orq	%rax, %r12
	movq	(%r10), %rsi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movq	(%rdi), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorps	%xmm14, %xmm14
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%r10, 64(%rsp)          # 8-byte Spill
	movq	%r10, %rax
	jmp	.LBB7_7
	.p2align	4, 0x90
.LBB7_22:                               #   in Loop: Header=BB7_7 Depth=1
	leal	96(%r12), %eax
	andl	$127, %eax
	andq	$-128, %r12
	movq	8(%rax,%r12), %rbx
	leal	32(%rbx), %r15d
	andl	$127, %r15d
	andq	$-128, %rbx
	movq	%rdx, %rdi
	movq	%rcx, %rsi
	callq	makeedge
	xorps	%xmm14, %xmm14
	movq	8(%rax), %rcx
	leal	32(%rcx), %edx
	andl	$127, %edx
	andq	$-128, %rcx
	movq	8(%r15,%rbx), %rsi
	leal	32(%rsi), %edi
	andl	$127, %edi
	andq	$-128, %rsi
	movq	8(%rdi,%rsi), %rbp
	movq	8(%rdx,%rcx), %r8
	movq	%rbp, 8(%rdx,%rcx)
	movq	%r8, 8(%rdi,%rsi)
	movq	8(%rax), %rcx
	movq	8(%r15,%rbx), %rdx
	movq	%rcx, 8(%r15,%rbx)
	movq	%rdx, 8(%rax)
	movq	%rax, %rcx
	xorq	$64, %rcx
	movq	8(%rcx), %rdx
	leal	32(%rdx), %esi
	andl	$127, %esi
	andq	$-128, %rdx
	movq	8(%r14), %rdi
	leal	32(%rdi), %ebp
	andl	$127, %ebp
	andq	$-128, %rdi
	movq	8(%rbp,%rdi), %rbx
	movq	8(%rsi,%rdx), %r8
	movq	%rbx, 8(%rsi,%rdx)
	movq	%r8, 8(%rbp,%rdi)
	movq	8(%rcx), %rdx
	movq	8(%r14), %rsi
	movq	%rdx, 8(%r14)
	movq	%rsi, 8(%rcx)
	addl	$96, %ecx
	andl	$127, %ecx
	movq	%rax, %rdx
	andq	$-128, %rdx
	movq	8(%rcx,%rdx), %r12
	leal	32(%r12), %ecx
	andl	$127, %ecx
	andq	$-128, %r12
	orq	%rcx, %r12
	movq	(%rax), %rsi
.LBB7_7:                                # %.thread161
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_9 Depth 2
                                        #     Child Loop BB7_14 Depth 2
	movq	8(%r13), %r8
	movq	%rax, %r14
	xorq	$64, %r14
	movq	(%r14), %rcx
	movq	%r8, %rdi
	xorq	$64, %rdi
	movq	(%rdi), %rdi
	movupd	(%rsi), %xmm8
	movups	(%rcx), %xmm9
	movapd	%xmm8, %xmm2
	movhpd	(%rdi), %xmm2           # xmm2 = xmm2[0],mem[0]
	movaps	%xmm9, %xmm3
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	subpd	%xmm3, %xmm2
	movapd	%xmm8, %xmm3
	movlpd	8(%rdi), %xmm3          # xmm3 = mem[0],xmm3[1]
	movaps	%xmm9, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	subpd	%xmm4, %xmm3
	mulpd	%xmm2, %xmm3
	movapd	%xmm3, %xmm2
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	subsd	%xmm2, %xmm3
	ucomisd	%xmm14, %xmm3
	jbe	.LBB7_12
# BB#8:                                 # %.preheader162
                                        #   in Loop: Header=BB7_7 Depth=1
	movapd	%xmm8, %xmm10
	movhlps	%xmm10, %xmm10          # xmm10 = xmm10[1,1]
	movsd	16(%rsi), %xmm9         # xmm9 = mem[0],zero
	jmp	.LBB7_9
	.p2align	4, 0x90
.LBB7_10:                               #   in Loop: Header=BB7_9 Depth=2
	leal	32(%r13), %edi
	andl	$127, %edi
	movq	%r13, %r9
	andq	$-128, %r9
	movq	8(%rdi,%r9), %rdi
	leal	32(%rdi), %ebp
	andl	$127, %ebp
	andq	$-128, %rdi
	movq	8(%r13), %rbx
	leal	32(%rbx), %edx
	andl	$127, %edx
	andq	$-128, %rbx
	movq	8(%rbp,%rdi), %rsi
	leal	32(%rsi), %ecx
	andl	$127, %ecx
	andq	$-128, %rsi
	movq	8(%rcx,%rsi), %r11
	movq	8(%rdx,%rbx), %r15
	movq	%r11, 8(%rdx,%rbx)
	movq	%r15, 8(%rcx,%rsi)
	movq	8(%r13), %rcx
	movq	8(%rbp,%rdi), %rdx
	movq	%rcx, 8(%rbp,%rdi)
	movq	%rdx, 8(%r13)
	leal	32(%r10), %ecx
	andl	$127, %ecx
	movq	8(%rcx,%r9), %rdi
	leal	32(%rdi), %ebp
	andl	$127, %ebp
	andq	$-128, %rdi
	movq	8(%r10), %rcx
	leal	32(%rcx), %edx
	andl	$127, %edx
	andq	$-128, %rcx
	movq	8(%rbp,%rdi), %rsi
	leal	32(%rsi), %ebx
	andl	$127, %ebx
	andq	$-128, %rsi
	movq	8(%rbx,%rsi), %r11
	movq	8(%rdx,%rcx), %r15
	movq	%r11, 8(%rdx,%rcx)
	movq	%r15, 8(%rbx,%rsi)
	movq	8(%r10), %rcx
	movq	8(%rbp,%rdi), %rdx
	movq	%rcx, 8(%rbp,%rdi)
	movq	%rdx, 8(%r10)
	movq	avail_edge(%rip), %rcx
	movq	%rcx, 8(%r9)
	movq	%r9, avail_edge(%rip)
	movq	%r8, %r13
	movq	8(%r8), %r8
.LBB7_9:                                #   Parent Loop BB7_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r8, %rsi
	xorq	$64, %rsi
	movq	%r13, %r10
	xorq	$64, %r10
	movq	(%r13), %rdi
	movq	(%rsi), %rsi
	movq	(%r10), %rbp
	movsd	(%rbp), %xmm13          # xmm13 = mem[0],zero
	movsd	8(%rbp), %xmm11         # xmm11 = mem[0],zero
	subsd	%xmm8, %xmm13
	subsd	%xmm10, %xmm11
	movsd	16(%rbp), %xmm1         # xmm1 = mem[0],zero
	movsd	(%rsi), %xmm3           # xmm3 = mem[0],zero
	movsd	8(%rsi), %xmm5          # xmm5 = mem[0],zero
	subsd	%xmm8, %xmm3
	subsd	%xmm10, %xmm5
	movsd	16(%rsi), %xmm2         # xmm2 = mem[0],zero
	movsd	(%rdi), %xmm7           # xmm7 = mem[0],zero
	movsd	8(%rdi), %xmm0          # xmm0 = mem[0],zero
	subsd	%xmm8, %xmm7
	subsd	%xmm10, %xmm0
	movsd	16(%rdi), %xmm12        # xmm12 = mem[0],zero
	subsd	%xmm9, %xmm1
	movapd	%xmm3, %xmm6
	mulsd	%xmm0, %xmm6
	movapd	%xmm5, %xmm4
	mulsd	%xmm7, %xmm4
	subsd	%xmm4, %xmm6
	mulsd	%xmm1, %xmm6
	subsd	%xmm9, %xmm2
	mulsd	%xmm11, %xmm7
	mulsd	%xmm13, %xmm0
	subsd	%xmm0, %xmm7
	mulsd	%xmm2, %xmm7
	addsd	%xmm6, %xmm7
	subsd	%xmm9, %xmm12
	mulsd	%xmm13, %xmm5
	mulsd	%xmm11, %xmm3
	subsd	%xmm3, %xmm5
	mulsd	%xmm12, %xmm5
	addsd	%xmm7, %xmm5
	ucomisd	%xmm14, %xmm5
	ja	.LBB7_10
# BB#11:                                # %.loopexit163.loopexit
                                        #   in Loop: Header=BB7_7 Depth=1
	movq	(%rax), %rsi
	movq	(%r14), %rcx
	movupd	(%rsi), %xmm8
	movups	(%rcx), %xmm9
.LBB7_12:                               # %.loopexit163
                                        #   in Loop: Header=BB7_7 Depth=1
	leal	32(%r12), %edx
	andl	$127, %edx
	movq	%r12, %rdi
	andq	$-128, %rdi
	movq	8(%rdx,%rdi), %rdx
	leal	32(%rdx), %edi
	andl	$127, %edi
	andq	$-128, %rdx
	orq	%rdi, %rdx
	movq	%rdx, %rdi
	xorq	$64, %rdi
	movq	(%rdi), %rdi
	movaps	%xmm9, %xmm10
	movhlps	%xmm10, %xmm10          # xmm10 = xmm10[1,1]
	movapd	%xmm8, %xmm1
	movhpd	(%rdi), %xmm1           # xmm1 = xmm1[0],mem[0]
	movaps	%xmm9, %xmm3
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	subpd	%xmm3, %xmm1
	movapd	%xmm8, %xmm3
	movlpd	8(%rdi), %xmm3          # xmm3 = mem[0],xmm3[1]
	subpd	%xmm10, %xmm3
	mulpd	%xmm1, %xmm3
	movapd	%xmm3, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	subsd	%xmm1, %xmm3
	ucomisd	%xmm14, %xmm3
	jbe	.LBB7_17
# BB#13:                                # %.preheader
                                        #   in Loop: Header=BB7_7 Depth=1
	movsd	16(%rcx), %xmm8         # xmm8 = mem[0],zero
	movq	%r12, %r8
	jmp	.LBB7_14
	.p2align	4, 0x90
.LBB7_15:                               #   in Loop: Header=BB7_14 Depth=2
	leal	32(%r12), %esi
	andl	$127, %esi
	movq	%r12, %r9
	andq	$-128, %r9
	movq	8(%rsi,%r9), %rsi
	leal	32(%rsi), %edi
	andl	$127, %edi
	andq	$-128, %rsi
	movq	8(%r12), %rbp
	leal	32(%rbp), %ebx
	andl	$127, %ebx
	andq	$-128, %rbp
	movq	8(%rdi,%rsi), %rcx
	leal	32(%rcx), %edx
	andl	$127, %edx
	andq	$-128, %rcx
	movq	8(%rdx,%rcx), %r10
	movq	8(%rbx,%rbp), %r11
	movq	%r10, 8(%rbx,%rbp)
	movq	%r11, 8(%rdx,%rcx)
	movq	8(%r12), %rcx
	movq	8(%rdi,%rsi), %rdx
	movq	%rcx, 8(%rdi,%rsi)
	movq	%rdx, 8(%r12)
	xorq	$64, %r12
	leal	32(%r12), %ecx
	andl	$127, %ecx
	movq	8(%rcx,%r9), %rsi
	leal	32(%rsi), %edi
	andl	$127, %edi
	andq	$-128, %rsi
	movq	8(%r12), %rcx
	leal	32(%rcx), %edx
	andl	$127, %edx
	andq	$-128, %rcx
	movq	8(%rdi,%rsi), %rbp
	leal	32(%rbp), %ebx
	andl	$127, %ebx
	andq	$-128, %rbp
	movq	8(%rbx,%rbp), %r10
	movq	8(%rdx,%rcx), %r11
	movq	%r10, 8(%rdx,%rcx)
	movq	%r11, 8(%rbx,%rbp)
	movq	8(%r12), %rcx
	movq	8(%rdi,%rsi), %rdx
	movq	%rcx, 8(%rdi,%rsi)
	movq	%rdx, 8(%r12)
	movq	avail_edge(%rip), %rcx
	movq	%rcx, 8(%r9)
	movq	%r9, avail_edge(%rip)
	leal	32(%r8), %ecx
	andl	$127, %ecx
	movq	%r8, %rdx
	andq	$-128, %rdx
	movq	8(%rcx,%rdx), %rdx
	leal	32(%rdx), %ecx
	andl	$127, %ecx
	andq	$-128, %rdx
	orq	%rcx, %rdx
	movq	%r8, %r12
.LBB7_14:                               #   Parent Loop BB7_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r8, %rcx
	xorq	$64, %rcx
	movq	(%rcx), %rsi
	movq	%rdx, %r8
	xorq	$64, %rdx
	movq	(%r12), %rdi
	movq	(%rdx), %rdx
	movsd	(%rdx), %xmm13          # xmm13 = mem[0],zero
	movsd	8(%rdx), %xmm11         # xmm11 = mem[0],zero
	subsd	%xmm9, %xmm13
	subsd	%xmm10, %xmm11
	movsd	16(%rdx), %xmm0         # xmm0 = mem[0],zero
	movsd	(%rsi), %xmm3           # xmm3 = mem[0],zero
	movsd	8(%rsi), %xmm5          # xmm5 = mem[0],zero
	subsd	%xmm9, %xmm3
	subsd	%xmm10, %xmm5
	movsd	16(%rsi), %xmm2         # xmm2 = mem[0],zero
	movsd	(%rdi), %xmm7           # xmm7 = mem[0],zero
	movsd	8(%rdi), %xmm1          # xmm1 = mem[0],zero
	subsd	%xmm9, %xmm7
	subsd	%xmm10, %xmm1
	movsd	16(%rdi), %xmm12        # xmm12 = mem[0],zero
	subsd	%xmm8, %xmm0
	movapd	%xmm3, %xmm6
	mulsd	%xmm1, %xmm6
	movapd	%xmm5, %xmm4
	mulsd	%xmm7, %xmm4
	subsd	%xmm4, %xmm6
	mulsd	%xmm0, %xmm6
	subsd	%xmm8, %xmm2
	mulsd	%xmm11, %xmm7
	mulsd	%xmm13, %xmm1
	subsd	%xmm1, %xmm7
	mulsd	%xmm2, %xmm7
	addsd	%xmm6, %xmm7
	subsd	%xmm8, %xmm12
	mulsd	%xmm13, %xmm5
	mulsd	%xmm11, %xmm3
	subsd	%xmm3, %xmm5
	mulsd	%xmm12, %xmm5
	addsd	%xmm7, %xmm5
	ucomisd	%xmm14, %xmm5
	ja	.LBB7_15
# BB#16:                                # %.loopexit.loopexit
                                        #   in Loop: Header=BB7_7 Depth=1
	movq	(%rax), %rsi
	movq	(%r14), %rcx
	movupd	(%rsi), %xmm8
	movups	(%rcx), %xmm9
.LBB7_17:                               # %.loopexit
                                        #   in Loop: Header=BB7_7 Depth=1
	movq	%r13, %r15
	xorq	$64, %r15
	movq	(%r15), %rdi
	movsd	(%rdi), %xmm3           # xmm3 = mem[0],zero
	movsd	8(%rdi), %xmm11         # xmm11 = mem[0],zero
	movaps	%xmm9, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	movapd	%xmm11, %xmm6
	subsd	%xmm0, %xmm6
	subpd	%xmm9, %xmm8
	mulsd	%xmm8, %xmm6
	movapd	%xmm3, %xmm0
	subsd	%xmm9, %xmm0
	movapd	%xmm8, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm6
	movq	%r12, %rdx
	xorq	$64, %rdx
	ucomisd	%xmm14, %xmm6
	movq	(%rdx), %rdx
	movsd	(%rdx), %xmm5           # xmm5 = mem[0],zero
	movsd	8(%rdx), %xmm4          # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm7
	unpcklpd	%xmm5, %xmm7    # xmm7 = xmm7[0],xmm5[0]
	shufpd	$1, %xmm9, %xmm9        # xmm9 = xmm9[1,0]
	subpd	%xmm9, %xmm7
	mulpd	%xmm8, %xmm7
	movapd	%xmm7, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	subsd	%xmm0, %xmm7
	ja	.LBB7_19
# BB#18:                                # %.loopexit
                                        #   in Loop: Header=BB7_7 Depth=1
	ucomisd	%xmm14, %xmm7
	jbe	.LBB7_24
.LBB7_19:                               #   in Loop: Header=BB7_7 Depth=1
	ucomisd	%xmm14, %xmm6
	jbe	.LBB7_22
# BB#20:                                #   in Loop: Header=BB7_7 Depth=1
	ucomisd	%xmm14, %xmm7
	jbe	.LBB7_23
# BB#21:                                #   in Loop: Header=BB7_7 Depth=1
	movq	(%r13), %rbp
	movq	(%r12), %rbx
	movsd	16(%rdx), %xmm8         # xmm8 = mem[0],zero
	subsd	%xmm5, %xmm3
	subsd	%xmm4, %xmm11
	movsd	16(%rdi), %xmm10        # xmm10 = mem[0],zero
	movsd	(%rbp), %xmm6           # xmm6 = mem[0],zero
	movsd	8(%rbp), %xmm2          # xmm2 = mem[0],zero
	subsd	%xmm5, %xmm6
	subsd	%xmm4, %xmm2
	movsd	16(%rbp), %xmm9         # xmm9 = mem[0],zero
	movsd	(%rbx), %xmm7           # xmm7 = mem[0],zero
	movsd	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	subsd	%xmm5, %xmm7
	subsd	%xmm4, %xmm1
	movsd	16(%rbx), %xmm4         # xmm4 = mem[0],zero
	subsd	%xmm8, %xmm10
	movapd	%xmm6, %xmm5
	mulsd	%xmm1, %xmm5
	movapd	%xmm2, %xmm0
	mulsd	%xmm7, %xmm0
	subsd	%xmm0, %xmm5
	mulsd	%xmm10, %xmm5
	subsd	%xmm8, %xmm9
	mulsd	%xmm11, %xmm7
	mulsd	%xmm3, %xmm1
	subsd	%xmm1, %xmm7
	mulsd	%xmm9, %xmm7
	addsd	%xmm5, %xmm7
	subsd	%xmm8, %xmm4
	mulsd	%xmm3, %xmm2
	mulsd	%xmm11, %xmm6
	subsd	%xmm6, %xmm2
	mulsd	%xmm4, %xmm2
	addsd	%xmm7, %xmm2
	ucomisd	%xmm14, %xmm2
	ja	.LBB7_22
.LBB7_23:                               #   in Loop: Header=BB7_7 Depth=1
	leal	32(%rax), %ecx
	andl	$127, %ecx
	andq	$-128, %rax
	movq	8(%rcx,%rax), %r13
	leal	32(%r13), %r14d
	andl	$127, %r14d
	andq	$-128, %r13
	callq	makeedge
	xorps	%xmm14, %xmm14
	movq	%rax, %r8
	movq	8(%r8), %rax
	leal	32(%rax), %edx
	andl	$127, %edx
	andq	$-128, %rax
	movq	8(%r15), %rsi
	leal	32(%rsi), %edi
	andl	$127, %edi
	andq	$-128, %rsi
	movq	8(%rdi,%rsi), %rbx
	movq	8(%rdx,%rax), %rbp
	movq	%rbx, 8(%rdx,%rax)
	movq	%rbp, 8(%rdi,%rsi)
	movq	8(%r8), %rax
	movq	8(%r15), %rdx
	movq	%rax, 8(%r15)
	movq	%rdx, 8(%r8)
	movq	%r8, %rax
	xorq	$64, %rax
	movq	8(%rax), %rdx
	leal	32(%rdx), %esi
	andl	$127, %esi
	andq	$-128, %rdx
	movq	8(%r14,%r13), %rdi
	leal	32(%rdi), %ebp
	andl	$127, %ebp
	andq	$-128, %rdi
	movq	8(%rbp,%rdi), %rbx
	movq	8(%rsi,%rdx), %rcx
	movq	%rbx, 8(%rsi,%rdx)
	movq	%rcx, 8(%rbp,%rdi)
	movq	8(%rax), %rcx
	movq	8(%r14,%r13), %rdx
	movq	%rcx, 8(%r14,%r13)
	movq	%rdx, 8(%rax)
	movq	8(%r8), %r13
	movq	(%rax), %rsi
	jmp	.LBB7_7
.LBB7_24:
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, 40(%rsp)          # 8-byte Folded Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	cmoveq	64(%rsp), %rdx          # 8-byte Folded Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, 48(%rsp)          # 8-byte Folded Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	cmoveq	56(%rsp), %rax          # 8-byte Folded Reload
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	do_merge, .Lfunc_end7-do_merge
	.cfi_endproc

	.globl	ccw
	.p2align	4, 0x90
	.type	ccw,@function
ccw:                                    # @ccw
	.cfi_startproc
# BB#0:
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movhpd	(%rsi), %xmm0           # xmm0 = xmm0[0],mem[0]
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	movsd	8(%rdx), %xmm2          # xmm2 = mem[0],zero
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	subpd	%xmm1, %xmm0
	movsd	8(%rsi), %xmm1          # xmm1 = mem[0],zero
	movhpd	8(%rdi), %xmm1          # xmm1 = xmm1[0],mem[0]
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	subpd	%xmm2, %xmm1
	mulpd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	subsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	xorl	%eax, %eax
	ucomisd	%xmm0, %xmm1
	seta	%al
	retq
.Lfunc_end8:
	.size	ccw, .Lfunc_end8-ccw
	.cfi_endproc

	.globl	delete_all_edges
	.p2align	4, 0x90
	.type	delete_all_edges,@function
delete_all_edges:                       # @delete_all_edges
	.cfi_startproc
# BB#0:
	movq	$0, next_edge(%rip)
	movq	$0, avail_edge(%rip)
	retq
.Lfunc_end9:
	.size	delete_all_edges, .Lfunc_end9-delete_all_edges
	.cfi_endproc

	.globl	myalign
	.p2align	4, 0x90
	.type	myalign,@function
myalign:                                # @myalign
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 16
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	callq	memalign
	testq	%rax, %rax
	je	.LBB10_2
# BB#1:
	popq	%rcx
	retq
.LBB10_2:
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$-1, %edi
	callq	exit
.Lfunc_end10:
	.size	myalign, .Lfunc_end10-myalign
	.cfi_endproc

	.globl	alloc_edge
	.p2align	4, 0x90
	.type	alloc_edge,@function
alloc_edge:                             # @alloc_edge
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 16
	movq	avail_edge(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB11_1
# BB#4:
	movq	8(%rcx), %rax
	movq	%rax, avail_edge(%rip)
	jmp	.LBB11_5
.LBB11_1:
	movl	$128, %edi
	movl	$128, %esi
	callq	memalign
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB11_6
# BB#2:                                 # %myalign.exit
	testb	$127, %cl
	jne	.LBB11_3
.LBB11_5:
	movq	$0, 16(%rcx)
	movq	$0, 48(%rcx)
	movq	$0, 80(%rcx)
	movq	$0, 112(%rcx)
	movq	%rcx, %rax
	popq	%rcx
	retq
.LBB11_6:
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$-1, %edi
	callq	exit
.LBB11_3:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end11:
	.size	alloc_edge, .Lfunc_end11-alloc_edge
	.cfi_endproc

	.globl	incircle
	.p2align	4, 0x90
	.type	incircle,@function
incircle:                               # @incircle
	.cfi_startproc
# BB#0:
	movsd	(%rcx), %xmm6           # xmm6 = mem[0],zero
	movsd	8(%rcx), %xmm7          # xmm7 = mem[0],zero
	movsd	16(%rcx), %xmm8         # xmm8 = mem[0],zero
	movsd	(%rdi), %xmm10          # xmm10 = mem[0],zero
	movsd	8(%rdi), %xmm9          # xmm9 = mem[0],zero
	subsd	%xmm6, %xmm10
	subsd	%xmm7, %xmm9
	movsd	16(%rdi), %xmm3         # xmm3 = mem[0],zero
	movsd	(%rsi), %xmm4           # xmm4 = mem[0],zero
	movsd	8(%rsi), %xmm1          # xmm1 = mem[0],zero
	subsd	%xmm6, %xmm4
	subsd	%xmm7, %xmm1
	movsd	16(%rsi), %xmm11        # xmm11 = mem[0],zero
	movsd	(%rdx), %xmm5           # xmm5 = mem[0],zero
	movsd	8(%rdx), %xmm2          # xmm2 = mem[0],zero
	subsd	%xmm6, %xmm5
	subsd	%xmm7, %xmm2
	movsd	16(%rdx), %xmm6         # xmm6 = mem[0],zero
	subsd	%xmm8, %xmm3
	movapd	%xmm4, %xmm7
	mulsd	%xmm2, %xmm7
	movapd	%xmm1, %xmm0
	mulsd	%xmm5, %xmm0
	subsd	%xmm0, %xmm7
	mulsd	%xmm3, %xmm7
	subsd	%xmm8, %xmm11
	mulsd	%xmm9, %xmm5
	mulsd	%xmm10, %xmm2
	subsd	%xmm2, %xmm5
	mulsd	%xmm11, %xmm5
	addsd	%xmm7, %xmm5
	subsd	%xmm8, %xmm6
	mulsd	%xmm10, %xmm1
	mulsd	%xmm9, %xmm4
	subsd	%xmm4, %xmm1
	mulsd	%xmm6, %xmm1
	addsd	%xmm5, %xmm1
	xorpd	%xmm0, %xmm0
	xorl	%eax, %eax
	ucomisd	%xmm0, %xmm1
	seta	%al
	retq
.Lfunc_end12:
	.size	incircle, .Lfunc_end12-incircle
	.cfi_endproc

	.globl	makeedge
	.p2align	4, 0x90
	.type	makeedge,@function
makeedge:                               # @makeedge
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 32
.Lcfi45:
	.cfi_offset %rbx, -24
.Lcfi46:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	avail_edge(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB13_1
# BB#4:
	movq	8(%rcx), %rax
	movq	%rax, avail_edge(%rip)
	jmp	.LBB13_5
.LBB13_1:
	movl	$128, %edi
	movl	$128, %esi
	callq	memalign
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB13_6
# BB#2:                                 # %myalign.exit.i
	testb	$127, %cl
	jne	.LBB13_3
.LBB13_5:                               # %alloc_edge.exit
	movq	$0, 16(%rcx)
	leaq	32(%rcx), %rax
	movq	$0, 48(%rcx)
	leaq	64(%rcx), %rdx
	movq	$0, 80(%rcx)
	leaq	96(%rcx), %rsi
	movq	$0, 112(%rcx)
	movq	%rcx, 8(%rcx)
	movq	%rbx, (%rcx)
	movq	%rsi, 40(%rcx)
	movq	%rdx, 72(%rcx)
	movq	%r14, 64(%rcx)
	movq	%rax, 104(%rcx)
	movq	%rcx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB13_6:
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$-1, %edi
	callq	exit
.LBB13_3:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end13:
	.size	makeedge, .Lfunc_end13-makeedge
	.cfi_endproc

	.globl	splice
	.p2align	4, 0x90
	.type	splice,@function
splice:                                 # @splice
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	leal	32(%rax), %r8d
	andl	$127, %r8d
	andq	$-128, %rax
	movq	8(%rsi), %rdx
	leal	32(%rdx), %ecx
	andl	$127, %ecx
	andq	$-128, %rdx
	movq	8(%rcx,%rdx), %r9
	movq	8(%r8,%rax), %r10
	movq	%r9, 8(%r8,%rax)
	movq	%r10, 8(%rcx,%rdx)
	movq	8(%rdi), %rax
	movq	8(%rsi), %rcx
	movq	%rax, 8(%rsi)
	movq	%rcx, 8(%rdi)
	retq
.Lfunc_end14:
	.size	splice, .Lfunc_end14-splice
	.cfi_endproc

	.globl	swapedge
	.p2align	4, 0x90
	.type	swapedge,@function
swapedge:                               # @swapedge
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 56
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	leal	32(%rdi), %eax
	andl	$127, %eax
	movq	%rdi, %rdx
	andq	$-128, %rdx
	movq	8(%rax,%rdx), %r10
	leal	32(%r10), %r8d
	andl	$127, %r8d
	movq	%r10, %r11
	andq	$-128, %r11
	movq	%rdi, %r12
	xorq	$64, %r12
	leal	32(%r12), %eax
	andl	$127, %eax
	movq	8(%rax,%rdx), %r9
	leal	32(%r9), %r14d
	andl	$127, %r14d
	movq	%r9, %r15
	andq	$-128, %r15
	movq	8(%rdi), %rdx
	leal	32(%rdx), %esi
	andl	$127, %esi
	andq	$-128, %rdx
	movq	8(%r8,%r11), %rbx
	leal	32(%rbx), %eax
	andl	$127, %eax
	andq	$-128, %rbx
	movq	8(%rax,%rbx), %rcx
	movq	8(%rsi,%rdx), %r13
	movq	%rcx, 8(%rsi,%rdx)
	movq	%r13, 8(%rax,%rbx)
	movq	8(%rdi), %rax
	movq	8(%r8,%r11), %rcx
	movq	%rax, 8(%r8,%r11)
	movq	%rcx, 8(%rdi)
	movq	8(%r12), %rax
	leal	32(%rax), %ecx
	andl	$127, %ecx
	andq	$-128, %rax
	movq	8(%r14,%r15), %rdx
	leal	32(%rdx), %esi
	andl	$127, %esi
	andq	$-128, %rdx
	movq	8(%rsi,%rdx), %rbx
	movq	8(%rcx,%rax), %r13
	movq	%rbx, 8(%rcx,%rax)
	movq	%r13, 8(%rsi,%rdx)
	movq	8(%r12), %rax
	movq	8(%r14,%r15), %rcx
	movq	%rax, 8(%r14,%r15)
	movq	%rcx, 8(%r12)
	movq	8(%r10), %rdx
	leal	32(%rdx), %r10d
	andl	$127, %r10d
	andq	$-128, %rdx
	movq	8(%rdi), %rax
	leal	32(%rax), %ecx
	andl	$127, %ecx
	andq	$-128, %rax
	movq	8(%r10,%rdx), %rbx
	leal	32(%rbx), %esi
	andl	$127, %esi
	andq	$-128, %rbx
	movq	8(%rsi,%rbx), %r13
	movq	8(%rcx,%rax), %rbp
	movq	%r13, 8(%rcx,%rax)
	movq	%rbp, 8(%rsi,%rbx)
	movq	8(%rdi), %rax
	movq	8(%r10,%rdx), %rcx
	movq	%rax, 8(%r10,%rdx)
	movq	%rcx, 8(%rdi)
	movq	8(%r9), %rdx
	leal	32(%rdx), %esi
	andl	$127, %esi
	andq	$-128, %rdx
	movq	8(%r12), %rax
	leal	32(%rax), %ecx
	andl	$127, %ecx
	andq	$-128, %rax
	movq	8(%rsi,%rdx), %rbp
	leal	32(%rbp), %ebx
	andl	$127, %ebx
	andq	$-128, %rbp
	movq	8(%rbx,%rbp), %r9
	movq	8(%rcx,%rax), %r10
	movq	%r9, 8(%rcx,%rax)
	movq	%r10, 8(%rbx,%rbp)
	movq	8(%r12), %rax
	movq	8(%rsi,%rdx), %rcx
	movq	%rax, 8(%rsi,%rdx)
	leaq	(%r8,%r11), %rax
	leaq	(%r14,%r15), %rdx
	movq	%rcx, 8(%r12)
	xorq	$64, %rax
	movq	(%rax), %rax
	xorq	$64, %rdx
	movq	(%rdx), %rcx
	movq	%rax, (%rdi)
	movq	%rcx, (%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	swapedge, .Lfunc_end15-swapedge
	.cfi_endproc

	.globl	valid
	.p2align	4, 0x90
	.type	valid,@function
valid:                                  # @valid
	.cfi_startproc
# BB#0:
	movq	(%rsi), %rax
	xorq	$64, %rsi
	movq	(%rsi), %rcx
	xorq	$64, %rdi
	movq	(%rdi), %rdx
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movhpd	(%rdx), %xmm0           # xmm0 = xmm0[0],mem[0]
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	movsd	8(%rcx), %xmm2          # xmm2 = mem[0],zero
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	subpd	%xmm1, %xmm0
	movsd	8(%rdx), %xmm1          # xmm1 = mem[0],zero
	movhpd	8(%rax), %xmm1          # xmm1 = xmm1[0],mem[0]
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	subpd	%xmm2, %xmm1
	mulpd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	subsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	xorl	%eax, %eax
	ucomisd	%xmm0, %xmm1
	seta	%al
	retq
.Lfunc_end16:
	.size	valid, .Lfunc_end16-valid
	.cfi_endproc

	.globl	dump_quad
	.p2align	4, 0x90
	.type	dump_quad,@function
dump_quad:                              # @dump_quad
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 16
.Lcfi60:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	andq	$-128, %rbx
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	movq	8(%rbx), %rdx
	movq	(%rdx), %rcx
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	leaq	32(%rbx), %rsi
	movq	40(%rbx), %rdx
	movq	(%rdx), %rcx
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	leaq	64(%rbx), %rsi
	movq	72(%rbx), %rdx
	movq	(%rdx), %rcx
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	movq	104(%rbx), %rdx
	leaq	96(%rbx), %rsi
	movq	(%rdx), %rcx
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	popq	%rbx
	jmp	printf                  # TAILCALL
.Lfunc_end17:
	.size	dump_quad, .Lfunc_end17-dump_quad
	.cfi_endproc

	.globl	in_order
	.p2align	4, 0x90
	.type	in_order,@function
in_order:                               # @in_order
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 16
.Lcfi62:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB18_3
	.p2align	4, 0x90
.LBB18_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	movl	$.L.str.6, %edi
	movb	$2, %al
	callq	printf
	movq	24(%rbx), %rdi
	callq	in_order
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB18_1
.LBB18_3:                               # %tailrecurse._crit_edge
	movl	$.Lstr.2, %edi
	popq	%rbx
	jmp	puts                    # TAILCALL
.Lfunc_end18:
	.size	in_order, .Lfunc_end18-in_order
	.cfi_endproc

	.globl	mult
	.p2align	4, 0x90
	.type	mult,@function
mult:                                   # @mult
	.cfi_startproc
# BB#0:
	movslq	%edi, %rcx
	imulq	$1759218605, %rcx, %rdx # imm = 0x68DB8BAD
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$44, %rdx
	addl	%eax, %edx
	imull	$10000, %edx, %eax      # imm = 0x2710
	subl	%eax, %ecx
	movslq	%esi, %rax
	imulq	$1759218605, %rax, %rsi # imm = 0x68DB8BAD
	movq	%rsi, %rdi
	shrq	$63, %rdi
	sarq	$44, %rsi
	addl	%edi, %esi
	imull	$10000, %esi, %edi      # imm = 0x2710
	subl	%edi, %eax
	imull	%ecx, %esi
	imull	%eax, %edx
	addl	%esi, %edx
	movslq	%edx, %rdx
	imulq	$1759218605, %rdx, %rsi # imm = 0x68DB8BAD
	movq	%rsi, %rdi
	shrq	$63, %rdi
	sarq	$44, %rsi
	addl	%edi, %esi
	imull	$10000, %esi, %esi      # imm = 0x2710
	subl	%esi, %edx
	imull	$10000, %edx, %edx      # imm = 0x2710
	imull	%ecx, %eax
	addl	%edx, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end19:
	.size	mult, .Lfunc_end19-mult
	.cfi_endproc

	.globl	skiprand
	.p2align	4, 0x90
	.type	skiprand,@function
skiprand:                               # @skiprand
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	je	.LBB20_2
	.p2align	4, 0x90
.LBB20_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	%edi, %rax
	imulq	$1759218605, %rax, %rcx # imm = 0x68DB8BAD
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$44, %rcx
	addl	%edx, %ecx
	imull	$10000, %ecx, %edx      # imm = 0x2710
	subl	%edx, %eax
	imull	$3141, %eax, %edx       # imm = 0xC45
	imull	$5821, %ecx, %ecx       # imm = 0x16BD
	addl	%edx, %ecx
	movslq	%ecx, %rcx
	imulq	$1759218605, %rcx, %rdx # imm = 0x68DB8BAD
	movq	%rdx, %rdi
	shrq	$63, %rdi
	sarq	$44, %rdx
	addl	%edi, %edx
	imull	$10000, %edx, %edx      # imm = 0x2710
	subl	%edx, %ecx
	imull	$10000, %ecx, %ecx      # imm = 0x2710
	imull	$5821, %eax, %eax       # imm = 0x16BD
	leal	1(%rax,%rcx), %edi
	decl	%esi
	jne	.LBB20_1
.LBB20_2:                               # %._crit_edge
	movl	%edi, %eax
	retq
.Lfunc_end20:
	.size	skiprand, .Lfunc_end20-skiprand
	.cfi_endproc

	.globl	myrandom
	.p2align	4, 0x90
	.type	myrandom,@function
myrandom:                               # @myrandom
	.cfi_startproc
# BB#0:
	movslq	%edi, %rax
	imulq	$1759218605, %rax, %rcx # imm = 0x68DB8BAD
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$44, %rcx
	addl	%edx, %ecx
	imull	$10000, %ecx, %edx      # imm = 0x2710
	subl	%edx, %eax
	imull	$3141, %eax, %edx       # imm = 0xC45
	imull	$5821, %ecx, %ecx       # imm = 0x16BD
	addl	%edx, %ecx
	movslq	%ecx, %rcx
	imulq	$1759218605, %rcx, %rdx # imm = 0x68DB8BAD
	movq	%rdx, %rsi
	shrq	$63, %rsi
	sarq	$44, %rdx
	addl	%esi, %edx
	imull	$10000, %edx, %edx      # imm = 0x2710
	subl	%edx, %ecx
	imull	$10000, %ecx, %ecx      # imm = 0x2710
	imull	$5821, %eax, %eax       # imm = 0x16BD
	leal	1(%rax,%rcx), %eax
	retq
.Lfunc_end21:
	.size	myrandom, .Lfunc_end21-myrandom
	.cfi_endproc

	.globl	print_extra
	.p2align	4, 0x90
	.type	print_extra,@function
print_extra:                            # @print_extra
	.cfi_startproc
# BB#0:
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rdi), %xmm1          # xmm1 = mem[0],zero
	movl	$.L.str.6, %edi
	movb	$2, %al
	jmp	printf                  # TAILCALL
.Lfunc_end22:
	.size	print_extra, .Lfunc_end22-print_extra
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI23_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
.Lcfi68:
	.cfi_def_cfa_offset 96
.Lcfi69:
	.cfi_offset %rbx, -48
.Lcfi70:
	.cfi_offset %r12, -40
.Lcfi71:
	.cfi_offset %r14, -32
.Lcfi72:
	.cfi_offset %r15, -24
.Lcfi73:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	$0, to_color(%rip)
	movl	$0, to_3d_out(%rip)
	movl	$0, to_off(%rip)
	movl	$0, to_lincoln(%rip)
	movl	$1, delaunay(%rip)
	movl	$1, voronoi(%rip)
	movl	$0, ahost(%rip)
	movl	$0, interactive(%rip)
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	dealwithargs
	movl	%eax, %ebx
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movl	NumNodes(%rip), %r8d
	decl	%r8d
	movq	%rsp, %rdi
	movsd	.LCPI23_0(%rip), %xmm0  # xmm0 = mem[0],zero
	movl	$1, %esi
	movl	$1023, %ecx             # imm = 0x3FF
	movl	$1, %r9d
	movl	%ebx, %edx
	callq	get_points
	movq	(%rsp), %r14
	movsd	8(%rsp), %xmm0          # xmm0 = mem[0],zero
	movl	16(%rsp), %ecx
	leal	-1(%rbx), %esi
	movl	NumNodes(%rip), %r9d
	leaq	24(%rsp), %rdi
	xorl	%r8d, %r8d
	movl	%esi, %edx
	callq	get_points
	movq	24(%rsp), %r15
	movl	$.Lstr.3, %edi
	callq	puts
	leal	1(%rbx), %eax
	movl	%eax, num_vertices(%rip)
	leal	4(,%rbx,4), %eax
	leal	(%rax,%rax,2), %ebp
	movl	%ebp, num_edgeparts(%rip)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r12
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 8(%r12)
	leal	2(%rbx,%rbx), %eax
	leal	(%rax,%rax,2), %eax
	movl	%eax, 16(%r12)
	cmpl	$0, flag(%rip)
	je	.LBB23_3
# BB#1:
	movq	%r15, %rdi
	callq	in_order
	cmpl	$0, flag(%rip)
	je	.LBB23_3
# BB#2:
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%r14), %xmm1          # xmm1 = mem[0],zero
	movl	$.L.str.6, %edi
	movb	$2, %al
	callq	printf
.LBB23_3:                               # %.thread
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	build_delaunay
	cmpl	$0, flag(%rip)
	je	.LBB23_5
# BB#4:
	movq	%rax, %rdi
	movl	%ebx, %esi
	movq	%r12, %rdx
	callq	output_voronoi_diagram
.LBB23_5:
	xorl	%eax, %eax
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	main, .Lfunc_end23-main
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI24_0:
	.quad	4746794007244308480     # double 2147483647
	.text
	.globl	get_points
	.p2align	4, 0x90
	.type	get_points,@function
get_points:                             # @get_points
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi77:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi78:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi80:
	.cfi_def_cfa_offset 144
.Lcfi81:
	.cfi_offset %rbx, -56
.Lcfi82:
	.cfi_offset %r12, -48
.Lcfi83:
	.cfi_offset %r13, -40
.Lcfi84:
	.cfi_offset %r14, -32
.Lcfi85:
	.cfi_offset %r15, -24
.Lcfi86:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movl	%edx, %r15d
	movl	%esi, %r12d
	testl	%r12d, %r12d
	jle	.LBB24_1
# BB#2:
	shrl	%r12d
	movl	%r9d, %ebx
	shrl	$31, %ebx
	addl	%r9d, %ebx
	sarl	%ebx
	movq	%r8, 24(%rsp)           # 8-byte Spill
	leal	(%rbx,%r8), %r8d
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	64(%rsp), %rdi
	movl	%r12d, %esi
	movl	%r15d, %edx
	movl	%ebx, %r9d
	callq	get_points
	movq	64(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movsd	72(%rsp), %xmm0         # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movslq	80(%rsp), %r14
	subl	%r12d, %r15d
	movl	$40, %edi
	callq	malloc
	movq	%rax, %r13
	imulq	$1759218605, %r14, %rax # imm = 0x68DB8BAD
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	sarl	$12, %eax
	addl	%ecx, %eax
	imull	$10000, %eax, %ecx      # imm = 0x2710
	subl	%ecx, %r14d
	imull	$3141, %r14d, %ecx      # imm = 0xC45
	imull	$5821, %eax, %eax       # imm = 0x16BD
	addl	%ecx, %eax
	cltq
	imulq	$1759218605, %rax, %rcx # imm = 0x68DB8BAD
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$32, %rcx
	sarl	$12, %ecx
	addl	%edx, %ecx
	imull	$10000, %ecx, %ecx      # imm = 0x2710
	subl	%ecx, %eax
	imull	$10000, %eax, %eax      # imm = 0x2710
	imull	$5821, %r14d, %ecx      # imm = 0x16BD
	leal	1(%rcx,%rax), %ebp
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	divsd	.LCPI24_0(%rip), %xmm0
	callq	log
	cvtsi2sdl	%r15d, %xmm1
	divsd	%xmm1, %xmm0
	callq	exp
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, (%r13)
	movslq	%ebp, %rax
	imulq	$1759218605, %rax, %rcx # imm = 0x68DB8BAD
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$32, %rcx
	sarl	$12, %ecx
	addl	%edx, %ecx
	imull	$10000, %ecx, %edx      # imm = 0x2710
	subl	%edx, %eax
	imull	$3141, %eax, %edx       # imm = 0xC45
	imull	$5821, %ecx, %ecx       # imm = 0x16BD
	addl	%edx, %ecx
	movslq	%ecx, %rcx
	imulq	$1759218605, %rcx, %rdx # imm = 0x68DB8BAD
	movq	%rdx, %rsi
	shrq	$63, %rsi
	shrq	$32, %rdx
	sarl	$12, %edx
	addl	%esi, %edx
	imull	$10000, %edx, %edx      # imm = 0x2710
	subl	%edx, %ecx
	imull	$10000, %ecx, %ecx      # imm = 0x2710
	imull	$5821, %eax, %eax       # imm = 0x16BD
	leal	1(%rax,%rcx), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	divsd	.LCPI24_0(%rip), %xmm1
	movsd	%xmm1, 8(%r13)
	movapd	%xmm0, %xmm2
	mulsd	%xmm2, %xmm2
	mulsd	%xmm1, %xmm1
	addsd	%xmm2, %xmm1
	movsd	%xmm1, 16(%r13)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 32(%r13)
	decl	%r15d
	leaq	40(%rsp), %rdi
	movl	%r12d, %esi
	movl	%r15d, %edx
	movq	24(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%ebx, %r9d
	callq	get_points
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %rax
	movq	48(%rsp), %rdx
	movl	56(%rsp), %ecx
	movl	60(%rsp), %esi
	movq	%rax, 24(%r13)
	movq	%r13, (%rdi)
	movq	%rdx, 8(%rdi)
	movl	%esi, 20(%rdi)
	jmp	.LBB24_3
.LBB24_1:
	movq	$0, (%rdi)
	movsd	%xmm0, 8(%rdi)
.LBB24_3:
	movl	%ecx, 16(%rdi)
	movq	%rdi, %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	get_points, .Lfunc_end24-get_points
	.cfi_endproc

	.globl	allocate_stack
	.p2align	4, 0x90
	.type	allocate_stack,@function
allocate_stack:                         # @allocate_stack
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 32
.Lcfi90:
	.cfi_offset %rbx, -32
.Lcfi91:
	.cfi_offset %r14, -24
.Lcfi92:
	.cfi_offset %rbp, -16
	movl	%edi, %ebx
	leal	(,%rbx,4), %eax
	leal	(%rax,%rax,2), %ebp
	movl	%ebp, num_edgeparts(%rip)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r14
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 8(%r14)
	addl	%ebx, %ebx
	leal	(%rbx,%rbx,2), %eax
	movl	%eax, 16(%r14)
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end25:
	.size	allocate_stack, .Lfunc_end25-allocate_stack
	.cfi_endproc

	.globl	free_all
	.p2align	4, 0x90
	.type	free_all,@function
free_all:                               # @free_all
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 16
.Lcfi94:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	8(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end26:
	.size	free_all, .Lfunc_end26-free_all
	.cfi_endproc

	.globl	pop_edge
	.p2align	4, 0x90
	.type	pop_edge,@function
pop_edge:                               # @pop_edge
	.cfi_startproc
# BB#0:
	movslq	(%rdi), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, (%rdi)
	movq	8(%rdi), %rcx
	movq	(%rcx,%rax,8), %rax
	retq
.Lfunc_end27:
	.size	pop_edge, .Lfunc_end27-pop_edge
	.cfi_endproc

	.globl	push_edge
	.p2align	4, 0x90
	.type	push_edge,@function
push_edge:                              # @push_edge
	.cfi_startproc
# BB#0:
	movslq	(%rdi), %rax
	cmpl	16(%rdi), %eax
	jne	.LBB28_1
# BB#2:
	movl	$.Lstr.4, %edi
	jmp	puts                    # TAILCALL
.LBB28_1:
	leaq	1(%rax), %rcx
	movl	%ecx, (%rdi)
	movq	8(%rdi), %rcx
	movq	%rsi, 8(%rcx,%rax,8)
	retq
.Lfunc_end28:
	.size	push_edge, .Lfunc_end28-push_edge
	.cfi_endproc

	.globl	push_ring
	.p2align	4, 0x90
	.type	push_ring,@function
push_ring:                              # @push_ring
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 32
.Lcfi98:
	.cfi_offset %rbx, -32
.Lcfi99:
	.cfi_offset %r14, -24
.Lcfi100:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	8(%r14), %rbx
	cmpq	%r14, %rbx
	jne	.LBB29_2
	jmp	.LBB29_4
	.p2align	4, 0x90
.LBB29_3:                               # %push_edge.exit.backedge
                                        #   in Loop: Header=BB29_2 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r14, %rbx
	je	.LBB29_4
.LBB29_2:                               # =>This Inner Loop Header: Depth=1
	cmpq	$0, 16(%rbx)
	jne	.LBB29_3
# BB#5:                                 #   in Loop: Header=BB29_2 Depth=1
	movq	$1, 16(%rbx)
	movslq	(%r15), %rax
	cmpl	16(%r15), %eax
	jne	.LBB29_7
# BB#6:                                 #   in Loop: Header=BB29_2 Depth=1
	movl	$.Lstr.4, %edi
	callq	puts
	movq	8(%rbx), %rbx
	cmpq	%r14, %rbx
	jne	.LBB29_2
	jmp	.LBB29_4
.LBB29_7:                               #   in Loop: Header=BB29_2 Depth=1
	leaq	1(%rax), %rcx
	movl	%ecx, (%r15)
	movq	8(%r15), %rcx
	movq	%rbx, 8(%rcx,%rax,8)
	movq	8(%rbx), %rbx
	cmpq	%r14, %rbx
	jne	.LBB29_2
.LBB29_4:                               # %push_edge.exit._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end29:
	.size	push_ring, .Lfunc_end29-push_ring
	.cfi_endproc

	.globl	push_nonzero_ring
	.p2align	4, 0x90
	.type	push_nonzero_ring,@function
push_nonzero_ring:                      # @push_nonzero_ring
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 32
.Lcfi104:
	.cfi_offset %rbx, -32
.Lcfi105:
	.cfi_offset %r14, -24
.Lcfi106:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	8(%r14), %rbx
	cmpq	%r14, %rbx
	jne	.LBB30_2
	jmp	.LBB30_4
	.p2align	4, 0x90
.LBB30_3:                               # %push_edge.exit.backedge
                                        #   in Loop: Header=BB30_2 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r14, %rbx
	je	.LBB30_4
.LBB30_2:                               # =>This Inner Loop Header: Depth=1
	cmpq	$0, 16(%rbx)
	je	.LBB30_3
# BB#5:                                 #   in Loop: Header=BB30_2 Depth=1
	movq	$0, 16(%rbx)
	movslq	(%r15), %rax
	cmpl	16(%r15), %eax
	jne	.LBB30_7
# BB#6:                                 #   in Loop: Header=BB30_2 Depth=1
	movl	$.Lstr.4, %edi
	callq	puts
	movq	8(%rbx), %rbx
	cmpq	%r14, %rbx
	jne	.LBB30_2
	jmp	.LBB30_4
	.p2align	4, 0x90
.LBB30_7:                               #   in Loop: Header=BB30_2 Depth=1
	leaq	1(%rax), %rcx
	movl	%ecx, (%r15)
	movq	8(%r15), %rcx
	movq	%rbx, 8(%rcx,%rax,8)
	movq	8(%rbx), %rbx
	cmpq	%r14, %rbx
	jne	.LBB30_2
.LBB30_4:                               # %push_edge.exit._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end30:
	.size	push_nonzero_ring, .Lfunc_end30-push_nonzero_ring
	.cfi_endproc

	.globl	zero_seen
	.p2align	4, 0x90
	.type	zero_seen,@function
zero_seen:                              # @zero_seen
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi107:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 32
.Lcfi110:
	.cfi_offset %rbx, -32
.Lcfi111:
	.cfi_offset %r14, -24
.Lcfi112:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$0, (%r15)
	movq	8(%r14), %rbx
	cmpq	%r14, %rbx
	je	.LBB31_1
	.p2align	4, 0x90
.LBB31_2:                               # =>This Inner Loop Header: Depth=1
	cmpq	$0, 16(%rbx)
	je	.LBB31_3
# BB#10:                                #   in Loop: Header=BB31_2 Depth=1
	movq	$0, 16(%rbx)
	movslq	(%r15), %rax
	cmpl	16(%r15), %eax
	jne	.LBB31_12
# BB#11:                                #   in Loop: Header=BB31_2 Depth=1
	movl	$.Lstr.4, %edi
	callq	puts
	jmp	.LBB31_3
	.p2align	4, 0x90
.LBB31_12:                              #   in Loop: Header=BB31_2 Depth=1
	leaq	1(%rax), %rcx
	movl	%ecx, (%r15)
	movq	8(%r15), %rcx
	movq	%rbx, 8(%rcx,%rax,8)
.LBB31_3:                               # %push_edge.exit.backedge.i
                                        #   in Loop: Header=BB31_2 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r14, %rbx
	jne	.LBB31_2
	jmp	.LBB31_4
.LBB31_1:
	xorl	%eax, %eax
	testl	%eax, %eax
	jne	.LBB31_6
	jmp	.LBB31_16
.LBB31_4:                               # %push_nonzero_ring.exitthread-pre-split
	movl	(%r15), %eax
	testl	%eax, %eax
	je	.LBB31_16
.LBB31_6:
	leal	-1(%rax), %ecx
	movl	%ecx, (%r15)
	movq	8(%r15), %rcx
	cltq
	movq	(%rcx,%rax,8), %r14
	xorq	$64, %r14
	movq	8(%r14), %rbx
	jmp	.LBB31_9
	.p2align	4, 0x90
.LBB31_8:                               # %push_edge.exit.backedge.i13
                                        #   in Loop: Header=BB31_9 Depth=1
	movq	8(%rbx), %rbx
.LBB31_9:                               # %push_edge.exit.backedge.i13
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r14, %rbx
	je	.LBB31_4
# BB#7:                                 #   in Loop: Header=BB31_9 Depth=1
	cmpq	$0, 16(%rbx)
	je	.LBB31_8
# BB#13:                                #   in Loop: Header=BB31_9 Depth=1
	movq	$0, 16(%rbx)
	movslq	(%r15), %rax
	cmpl	16(%r15), %eax
	jne	.LBB31_15
# BB#14:                                #   in Loop: Header=BB31_9 Depth=1
	movl	$.Lstr.4, %edi
	callq	puts
	jmp	.LBB31_8
	.p2align	4, 0x90
.LBB31_15:                              #   in Loop: Header=BB31_9 Depth=1
	leaq	1(%rax), %rcx
	movl	%ecx, (%r15)
	movq	8(%r15), %rcx
	movq	%rbx, 8(%rcx,%rax,8)
	jmp	.LBB31_8
.LBB31_16:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end31:
	.size	zero_seen, .Lfunc_end31-zero_seen
	.cfi_endproc

	.type	next_edge,@object       # @next_edge
	.comm	next_edge,8,8
	.type	avail_edge,@object      # @avail_edge
	.comm	avail_edge,8,8
	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"Aborting in alloc_edge, ans = 0x%p\n"
	.size	.L.str.2, 36

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Entered DUMP_QUAD: ptr=0x%p\n"
	.size	.L.str.3, 29

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"DUMP_QUAD: ptr=0x%p onext=0x%p,v=0x%p\n"
	.size	.L.str.4, 39

	.type	loop,@object            # @loop
	.bss
	.globl	loop
	.p2align	2
loop:
	.long	0                       # 0x0
	.size	loop, 4

	.type	randum,@object          # @randum
	.data
	.globl	randum
	.p2align	2
randum:
	.long	1                       # 0x1
	.size	randum, 4

	.type	filein,@object          # @filein
	.bss
	.globl	filein
	.p2align	2
filein:
	.long	0                       # 0x0
	.size	filein, 4

	.type	fileout,@object         # @fileout
	.data
	.globl	fileout
	.p2align	2
fileout:
	.long	1                       # 0x1
	.size	fileout, 4

	.type	statistics,@object      # @statistics
	.bss
	.globl	statistics
	.p2align	2
statistics:
	.long	0                       # 0x0
	.size	statistics, 4

	.type	.L.str.6,@object        # @.str.6
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.6:
	.asciz	"X=%f, Y=%f\n"
	.size	.L.str.6, 12

	.type	to_color,@object        # @to_color
	.comm	to_color,4,4
	.type	to_3d_out,@object       # @to_3d_out
	.comm	to_3d_out,4,4
	.type	to_off,@object          # @to_off
	.comm	to_off,4,4
	.type	to_lincoln,@object      # @to_lincoln
	.comm	to_lincoln,4,4
	.type	delaunay,@object        # @delaunay
	.comm	delaunay,4,4
	.type	voronoi,@object         # @voronoi
	.comm	voronoi,4,4
	.type	ahost,@object           # @ahost
	.comm	ahost,4,4
	.type	interactive,@object     # @interactive
	.comm	interactive,4,4
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"argc = %d\n"
	.size	.L.str.7, 11

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"getting %d points\n"
	.size	.L.str.8, 19

	.type	NumNodes,@object        # @NumNodes
	.comm	NumNodes,4,4
	.type	num_vertices,@object    # @num_vertices
	.comm	num_vertices,4,4
	.type	flag,@object            # @flag
	.comm	flag,4,4
	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Doing voronoi on %d nodes\n"
	.size	.L.str.10, 27

	.type	num_edgeparts,@object   # @num_edgeparts
	.comm	num_edgeparts,4,4
	.type	vp,@object              # @vp
	.comm	vp,8,8
	.type	va,@object              # @va
	.comm	va,8,8
	.type	next,@object            # @next
	.comm	next,8,8
	.type	org,@object             # @org
	.comm	org,8,8
	.type	stack_size,@object      # @stack_size
	.comm	stack_size,4,4
	.type	see,@object             # @see
	.comm	see,8,8
	.type	NDim,@object            # @NDim
	.comm	NDim,4,4
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"ERROR: Only 1 point!"
	.size	.Lstr, 21

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"myalign() failed"
	.size	.Lstr.1, 17

	.type	.Lstr.2,@object         # @str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.2:
	.asciz	"NULL"
	.size	.Lstr.2, 5

	.type	.Lstr.3,@object         # @str.3
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.3:
	.asciz	"Done getting points"
	.size	.Lstr.3, 20

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"cannot push onto stack: stack is too large"
	.size	.Lstr.4, 43


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
