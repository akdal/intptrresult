	.text
	.file	"maze.bc"
	.globl	InitAllocMaps
	.p2align	4, 0x90
	.type	InitAllocMaps,@function
InitAllocMaps:                          # @InitAllocMaps
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	channelColumns(%rip), %r13
	incq	%r13
	movq	channelTracks(%rip), %rbx
	addq	$3, %rbx
	imulq	%r13, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r14
	movq	%r14, horzPlane(%rip)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r15
	movq	%r15, vertPlane(%rip)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r12
	movq	%r12, viaPlane(%rip)
	movq	%r13, %rdi
	callq	malloc
	movq	%rax, mazeRoute(%rip)
	testq	%rax, %rax
	je	.LBB0_5
# BB#1:
	testq	%r14, %r14
	je	.LBB0_5
# BB#2:
	testq	%r15, %r15
	je	.LBB0_5
# BB#3:
	testq	%r12, %r12
	je	.LBB0_5
# BB#4:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB0_5:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$41, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	InitAllocMaps, .Lfunc_end0-InitAllocMaps
	.cfi_endproc

	.globl	FreeAllocMaps
	.p2align	4, 0x90
	.type	FreeAllocMaps,@function
FreeAllocMaps:                          # @FreeAllocMaps
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 16
	movq	horzPlane(%rip), %rdi
	callq	free
	movq	vertPlane(%rip), %rdi
	callq	free
	movq	viaPlane(%rip), %rdi
	callq	free
	movq	mazeRoute(%rip), %rdi
	popq	%rax
	jmp	free                    # TAILCALL
.Lfunc_end1:
	.size	FreeAllocMaps, .Lfunc_end1-FreeAllocMaps
	.cfi_endproc

	.globl	DrawSegment
	.p2align	4, 0x90
	.type	DrawSegment,@function
DrawSegment:                            # @DrawSegment
	.cfi_startproc
# BB#0:
	cmpq	%rcx, %rsi
	jne	.LBB2_5
# BB#1:
	cmpq	%r8, %rdx
	movq	%r8, %rcx
	cmovbq	%rdx, %rcx
	cmovbq	%r8, %rdx
	movq	channelColumns(%rip), %rax
	imulq	%rcx, %rax
	addq	%rsi, %rax
	orb	$8, (%rdi,%rax)
	jmp	.LBB2_3
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        #   in Loop: Header=BB2_3 Depth=1
	imulq	%rcx, %rax
	addq	%rsi, %rax
	orb	$12, (%rdi,%rax)
.LBB2_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incq	%rcx
	movq	channelColumns(%rip), %rax
	cmpq	%rdx, %rcx
	jb	.LBB2_2
# BB#4:                                 # %._crit_edge
	imulq	%rdx, %rax
	addq	%rsi, %rax
	movb	$4, %cl
	orb	%cl, (%rdi,%rax)
	retq
.LBB2_5:
	movq	channelColumns(%rip), %rax
	imulq	%rdx, %rax
	cmpq	%rcx, %rsi
	movq	%rcx, %r8
	cmovbq	%rsi, %r8
	cmovbq	%rcx, %rsi
	addq	%r8, %rax
	orb	$2, (%rdi,%rax)
	jmp	.LBB2_7
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph69
                                        #   in Loop: Header=BB2_7 Depth=1
	addq	%rdi, %rax
	movb	$3, (%r8,%rax)
.LBB2_7:                                # %.lr.ph69
                                        # =>This Inner Loop Header: Depth=1
	incq	%r8
	movq	channelColumns(%rip), %rax
	imulq	%rdx, %rax
	cmpq	%rsi, %r8
	jb	.LBB2_6
# BB#8:                                 # %._crit_edge70
	addq	%rsi, %rax
	movb	$1, %cl
	orb	%cl, (%rdi,%rax)
	retq
.Lfunc_end2:
	.size	DrawSegment, .Lfunc_end2-DrawSegment
	.cfi_endproc

	.globl	DrawVia
	.p2align	4, 0x90
	.type	DrawVia,@function
DrawVia:                                # @DrawVia
	.cfi_startproc
# BB#0:
	movq	viaPlane(%rip), %rax
	imulq	channelColumns(%rip), %rsi
	addq	%rdi, %rsi
	movb	$1, (%rax,%rsi)
	retq
.Lfunc_end3:
	.size	DrawVia, .Lfunc_end3-DrawVia
	.cfi_endproc

	.globl	HasVia
	.p2align	4, 0x90
	.type	HasVia,@function
HasVia:                                 # @HasVia
	.cfi_startproc
# BB#0:
	movq	viaPlane(%rip), %rax
	imulq	channelColumns(%rip), %rsi
	addq	%rdi, %rsi
	movsbl	(%rax,%rsi), %eax
	retq
.Lfunc_end4:
	.size	HasVia, .Lfunc_end4-HasVia
	.cfi_endproc

	.globl	SegmentFree
	.p2align	4, 0x90
	.type	SegmentFree,@function
SegmentFree:                            # @SegmentFree
	.cfi_startproc
# BB#0:
	cmpq	%rcx, %rsi
	jne	.LBB5_5
# BB#1:
	cmpq	%r8, %rdx
	movq	%r8, %rax
	cmovbq	%rdx, %rax
	movq	channelColumns(%rip), %r9
	cmovbq	%r8, %rdx
	movq	%r9, %rcx
	imulq	%rax, %rcx
	addq	%rsi, %rcx
	addq	%rcx, %rdi
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rdi)
	jne	.LBB5_3
# BB#4:                                 #   in Loop: Header=BB5_2 Depth=1
	incq	%rax
	addq	%r9, %rdi
	cmpq	%rdx, %rax
	jbe	.LBB5_2
	jmp	.LBB5_9
.LBB5_5:
	imulq	channelColumns(%rip), %rdx
	cmpq	%rcx, %rsi
	movq	%rcx, %rax
	cmovbq	%rsi, %rax
	cmovbq	%rcx, %rsi
	addq	%rdx, %rdi
	.p2align	4, 0x90
.LBB5_6:                                # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rdi,%rax)
	jne	.LBB5_7
# BB#8:                                 #   in Loop: Header=BB5_6 Depth=1
	incq	%rax
	cmpq	%rsi, %rax
	jbe	.LBB5_6
.LBB5_9:
	movl	$1, %eax
	retq
.LBB5_3:
	xorl	%eax, %eax
	retq
.LBB5_7:
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	SegmentFree, .Lfunc_end5-SegmentFree
	.cfi_endproc

	.globl	PrintChannel
	.p2align	4, 0x90
	.type	PrintChannel,@function
PrintChannel:                           # @PrintChannel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 48
.Lcfi16:
	.cfi_offset %rbx, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpq	$0, channelColumns(%rip)
	je	.LBB6_3
# BB#1:                                 # %.lr.ph133.preheader
	movl	$1, %ebx
	movabsq	$2951479051793528259, %rbp # imm = 0x28F5C28F5C28F5C3
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph133
                                        # =>This Inner Loop Header: Depth=1
	movq	TOP(%rip), %rax
	movq	(%rax,%rbx,8), %rax
	shrq	$2, %rax
	mulq	%rbp
	shrq	$2, %rdx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%rdx, %rsi
	callq	printf
	incq	%rbx
	cmpq	channelColumns(%rip), %rbx
	jbe	.LBB6_2
.LBB6_3:                                # %._crit_edge134
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpq	$0, channelColumns(%rip)
	je	.LBB6_6
# BB#4:                                 # %.lr.ph129.preheader
	movl	$1, %ebx
	movabsq	$2951479051793528259, %r14 # imm = 0x28F5C28F5C28F5C3
	movabsq	$-3689348814741910323, %r15 # imm = 0xCCCCCCCCCCCCCCCD
	.p2align	4, 0x90
.LBB6_5:                                # %.lr.ph129
                                        # =>This Inner Loop Header: Depth=1
	movq	TOP(%rip), %rax
	movq	(%rax,%rbx,8), %rcx
	movq	%rcx, %rax
	shrq	$2, %rax
	mulq	%r14
	shrq	$2, %rdx
	imulq	$100, %rdx, %rax
	subq	%rax, %rcx
	movq	%rcx, %rax
	mulq	%r15
	shrq	$3, %rdx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%rdx, %rsi
	callq	printf
	incq	%rbx
	cmpq	channelColumns(%rip), %rbx
	jbe	.LBB6_5
.LBB6_6:                                # %._crit_edge130
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpq	$0, channelColumns(%rip)
	je	.LBB6_9
# BB#7:                                 # %.lr.ph125.preheader
	movl	$1, %ebx
	movabsq	$-3689348814741910323, %r14 # imm = 0xCCCCCCCCCCCCCCCD
	movabsq	$4611686018427387902, %rbp # imm = 0x3FFFFFFFFFFFFFFE
	.p2align	4, 0x90
.LBB6_8:                                # %.lr.ph125
                                        # =>This Inner Loop Header: Depth=1
	movq	TOP(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
	movq	%rsi, %rax
	mulq	%r14
	shrq	$2, %rdx
	andq	%rbp, %rdx
	leaq	(%rdx,%rdx,4), %rax
	subq	%rax, %rsi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	cmpq	channelColumns(%rip), %rbx
	jbe	.LBB6_8
.LBB6_9:                                # %._crit_edge126
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpq	$0, channelColumns(%rip)
	je	.LBB6_12
# BB#10:                                # %.lr.ph121.preheader
	movl	$1, %ebx
	movl	$124, %ebp
	.p2align	4, 0x90
.LBB6_11:                               # %.lr.ph121
                                        # =>This Inner Loop Header: Depth=1
	movq	vertPlane(%rip), %rax
	cmpb	$0, (%rax,%rbx)
	movl	$32, %esi
	cmovnel	%ebp, %esi
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	cmpq	channelColumns(%rip), %rbx
	jbe	.LBB6_11
.LBB6_12:                               # %._crit_edge122
	movl	$10, %edi
	callq	putchar
	movq	channelTracks(%rip), %rbx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpq	$0, %rbx
	je	.LBB6_18
# BB#13:                                # %.preheader92.preheader
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB6_14:                               # %.preheader92
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_16 Depth 2
                                        #     Child Loop BB6_35 Depth 2
                                        #     Child Loop BB6_61 Depth 2
	movq	channelColumns(%rip), %rax
	testq	%rax, %rax
	je	.LBB6_33
# BB#15:                                # %.lr.ph106.preheader
                                        #   in Loop: Header=BB6_14 Depth=1
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB6_16:                               # %.lr.ph106
                                        #   Parent Loop BB6_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imulq	%r14, %rax
	addq	vertPlane(%rip), %rax
	testb	$4, (%rbx,%rax)
	jne	.LBB6_17
# BB#31:                                #   in Loop: Header=BB6_16 Depth=2
	movl	$.L.str.6, %edi
	jmp	.LBB6_32
	.p2align	4, 0x90
.LBB6_17:                               #   in Loop: Header=BB6_16 Depth=2
	movl	$.L.str.5, %edi
.LBB6_32:                               #   in Loop: Header=BB6_16 Depth=2
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	movq	channelColumns(%rip), %rax
	cmpq	%rax, %rbx
	jbe	.LBB6_16
.LBB6_33:                               # %._crit_edge107
                                        #   in Loop: Header=BB6_14 Depth=1
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	printf
	movq	channelColumns(%rip), %rax
	testq	%rax, %rax
	je	.LBB6_59
# BB#34:                                # %.lr.ph111.preheader
                                        #   in Loop: Header=BB6_14 Depth=1
	movl	$1, %ebx
	jmp	.LBB6_35
.LBB6_48:                               #   in Loop: Header=BB6_35 Depth=2
	testb	%al, %al
	je	.LBB6_50
# BB#49:                                #   in Loop: Header=BB6_35 Depth=2
	movl	$124, %edi
	jmp	.LBB6_51
.LBB6_47:                               #   in Loop: Header=BB6_35 Depth=2
	movl	$45, %edi
	jmp	.LBB6_51
.LBB6_50:                               #   in Loop: Header=BB6_35 Depth=2
	movl	$32, %edi
	jmp	.LBB6_51
	.p2align	4, 0x90
.LBB6_35:                               # %.lr.ph111
                                        #   Parent Loop BB6_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imulq	%r14, %rax
	movq	horzPlane(%rip), %rcx
	addq	%rax, %rcx
	addq	vertPlane(%rip), %rax
	movzbl	(%rbx,%rax), %eax
	andb	$1, %al
	testb	$1, (%rbx,%rcx)
	jne	.LBB6_36
# BB#39:                                #   in Loop: Header=BB6_35 Depth=2
	testb	%al, %al
	jne	.LBB6_40
# BB#41:                                #   in Loop: Header=BB6_35 Depth=2
	movl	$32, %edi
	jmp	.LBB6_42
	.p2align	4, 0x90
.LBB6_36:                               #   in Loop: Header=BB6_35 Depth=2
	testb	%al, %al
	jne	.LBB6_37
# BB#38:                                #   in Loop: Header=BB6_35 Depth=2
	movl	$45, %edi
	jmp	.LBB6_42
	.p2align	4, 0x90
.LBB6_40:                               #   in Loop: Header=BB6_35 Depth=2
	movl	$94, %edi
	jmp	.LBB6_42
	.p2align	4, 0x90
.LBB6_37:                               #   in Loop: Header=BB6_35 Depth=2
	movl	$61, %edi
.LBB6_42:                               #   in Loop: Header=BB6_35 Depth=2
	callq	putchar
	movq	channelColumns(%rip), %rax
	imulq	%r14, %rax
	movq	viaPlane(%rip), %rcx
	addq	%rax, %rcx
	cmpb	$0, (%rbx,%rcx)
	je	.LBB6_44
# BB#43:                                #   in Loop: Header=BB6_35 Depth=2
	movl	$88, %edi
.LBB6_51:                               #   in Loop: Header=BB6_35 Depth=2
	callq	putchar
	movq	channelColumns(%rip), %rax
	imulq	%r14, %rax
	movq	horzPlane(%rip), %rcx
	addq	%rax, %rcx
	addq	vertPlane(%rip), %rax
	movzbl	(%rbx,%rax), %eax
	andb	$2, %al
	testb	$2, (%rbx,%rcx)
	jne	.LBB6_52
# BB#55:                                #   in Loop: Header=BB6_35 Depth=2
	testb	%al, %al
	jne	.LBB6_56
# BB#57:                                #   in Loop: Header=BB6_35 Depth=2
	movl	$32, %edi
	jmp	.LBB6_58
	.p2align	4, 0x90
.LBB6_52:                               #   in Loop: Header=BB6_35 Depth=2
	testb	%al, %al
	jne	.LBB6_53
# BB#54:                                #   in Loop: Header=BB6_35 Depth=2
	movl	$45, %edi
	jmp	.LBB6_58
	.p2align	4, 0x90
.LBB6_44:                               #   in Loop: Header=BB6_35 Depth=2
	movq	horzPlane(%rip), %rcx
	addq	%rax, %rcx
	addq	vertPlane(%rip), %rax
	cmpb	$0, (%rbx,%rcx)
	movzbl	(%rbx,%rax), %eax
	je	.LBB6_48
# BB#45:                                #   in Loop: Header=BB6_35 Depth=2
	testb	%al, %al
	je	.LBB6_47
# BB#46:                                #   in Loop: Header=BB6_35 Depth=2
	movl	$43, %edi
	jmp	.LBB6_51
	.p2align	4, 0x90
.LBB6_56:                               #   in Loop: Header=BB6_35 Depth=2
	movl	$94, %edi
	jmp	.LBB6_58
	.p2align	4, 0x90
.LBB6_53:                               #   in Loop: Header=BB6_35 Depth=2
	movl	$61, %edi
.LBB6_58:                               #   in Loop: Header=BB6_35 Depth=2
	callq	putchar
	incq	%rbx
	movq	channelColumns(%rip), %rax
	cmpq	%rax, %rbx
	jbe	.LBB6_35
.LBB6_59:                               # %._crit_edge112
                                        #   in Loop: Header=BB6_14 Depth=1
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movq	channelColumns(%rip), %rax
	testq	%rax, %rax
	je	.LBB6_65
# BB#60:                                # %.lr.ph115.preheader
                                        #   in Loop: Header=BB6_14 Depth=1
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB6_61:                               # %.lr.ph115
                                        #   Parent Loop BB6_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imulq	%r14, %rax
	addq	vertPlane(%rip), %rax
	testb	$8, (%rbx,%rax)
	jne	.LBB6_62
# BB#63:                                #   in Loop: Header=BB6_61 Depth=2
	movl	$.L.str.6, %edi
	jmp	.LBB6_64
	.p2align	4, 0x90
.LBB6_62:                               #   in Loop: Header=BB6_61 Depth=2
	movl	$.L.str.5, %edi
.LBB6_64:                               #   in Loop: Header=BB6_61 Depth=2
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	movq	channelColumns(%rip), %rax
	cmpq	%rax, %rbx
	jbe	.LBB6_61
.LBB6_65:                               # %._crit_edge116
                                        #   in Loop: Header=BB6_14 Depth=1
	movl	$10, %edi
	callq	putchar
	incq	%r14
	movq	channelTracks(%rip), %rbx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpq	%rbx, %r14
	jbe	.LBB6_14
.LBB6_18:                               # %.preheader
	movq	channelColumns(%rip), %rax
	testq	%rax, %rax
	je	.LBB6_21
# BB#19:                                # %.lr.ph103.preheader
	movl	$1, %ebx
	movl	$124, %ebp
	.p2align	4, 0x90
.LBB6_20:                               # %.lr.ph103
                                        # =>This Inner Loop Header: Depth=1
	movq	channelTracks(%rip), %rcx
	incq	%rcx
	imulq	%rax, %rcx
	addq	vertPlane(%rip), %rcx
	cmpb	$0, (%rbx,%rcx)
	movl	$32, %esi
	cmovnel	%ebp, %esi
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	movq	channelColumns(%rip), %rax
	cmpq	%rax, %rbx
	jbe	.LBB6_20
.LBB6_21:                               # %._crit_edge104
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpq	$0, channelColumns(%rip)
	je	.LBB6_24
# BB#22:                                # %.lr.ph100.preheader
	movl	$1, %ebx
	movabsq	$2951479051793528259, %rbp # imm = 0x28F5C28F5C28F5C3
	.p2align	4, 0x90
.LBB6_23:                               # %.lr.ph100
                                        # =>This Inner Loop Header: Depth=1
	movq	BOT(%rip), %rax
	movq	(%rax,%rbx,8), %rax
	shrq	$2, %rax
	mulq	%rbp
	shrq	$2, %rdx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%rdx, %rsi
	callq	printf
	incq	%rbx
	cmpq	channelColumns(%rip), %rbx
	jbe	.LBB6_23
.LBB6_24:                               # %._crit_edge101
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpq	$0, channelColumns(%rip)
	je	.LBB6_27
# BB#25:                                # %.lr.ph96.preheader
	movl	$1, %ebx
	movabsq	$2951479051793528259, %r14 # imm = 0x28F5C28F5C28F5C3
	movabsq	$-3689348814741910323, %r15 # imm = 0xCCCCCCCCCCCCCCCD
	.p2align	4, 0x90
.LBB6_26:                               # %.lr.ph96
                                        # =>This Inner Loop Header: Depth=1
	movq	BOT(%rip), %rax
	movq	(%rax,%rbx,8), %rcx
	movq	%rcx, %rax
	shrq	$2, %rax
	mulq	%r14
	shrq	$2, %rdx
	imulq	$100, %rdx, %rax
	subq	%rax, %rcx
	movq	%rcx, %rax
	mulq	%r15
	shrq	$3, %rdx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%rdx, %rsi
	callq	printf
	incq	%rbx
	cmpq	channelColumns(%rip), %rbx
	jbe	.LBB6_26
.LBB6_27:                               # %._crit_edge97
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpq	$0, channelColumns(%rip)
	je	.LBB6_30
# BB#28:                                # %.lr.ph.preheader
	movl	$1, %ebx
	movabsq	$-3689348814741910323, %r14 # imm = 0xCCCCCCCCCCCCCCCD
	movabsq	$4611686018427387902, %rbp # imm = 0x3FFFFFFFFFFFFFFE
	.p2align	4, 0x90
.LBB6_29:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	BOT(%rip), %rax
	movq	(%rax,%rbx,8), %rsi
	movq	%rsi, %rax
	mulq	%r14
	shrq	$2, %rdx
	andq	%rbp, %rdx
	leaq	(%rdx,%rdx,4), %rax
	subq	%rax, %rsi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	cmpq	channelColumns(%rip), %rbx
	jbe	.LBB6_29
.LBB6_30:                               # %._crit_edge
	movl	$10, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	putchar                 # TAILCALL
.Lfunc_end6:
	.size	PrintChannel, .Lfunc_end6-PrintChannel
	.cfi_endproc

	.globl	DrawNets
	.p2align	4, 0x90
	.type	DrawNets,@function
DrawNets:                               # @DrawNets
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 64
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	horzPlane(%rip), %rbx
	movq	channelColumns(%rip), %rdx
	movq	channelTracks(%rip), %rax
	addq	$2, %rax
	shlq	$32, %rdx
	movabsq	$4294967296, %rbp       # imm = 0x100000000
	addq	%rbp, %rdx
	imulq	%rax, %rdx
	sarq	$32, %rdx
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	memset
	movq	vertPlane(%rip), %r13
	movq	channelColumns(%rip), %rdx
	movq	channelTracks(%rip), %rax
	addq	$2, %rax
	shlq	$32, %rdx
	addq	%rbp, %rdx
	imulq	%rax, %rdx
	sarq	$32, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	memset
	movq	viaPlane(%rip), %r15
	movq	channelColumns(%rip), %rdx
	movq	channelTracks(%rip), %rax
	addq	$2, %rax
	shlq	$32, %rdx
	addq	%rbp, %rdx
	imulq	%rax, %rdx
	sarq	$32, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	memset
	movq	mazeRoute(%rip), %r12
	movq	channelColumns(%rip), %rdx
	shlq	$32, %rdx
	addq	%rbp, %rdx
	sarq	$32, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	memset
	movq	channelNets(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB7_8
# BB#1:                                 # %.lr.ph122.preheader
	movl	$1, %eax
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph122
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_5 Depth 2
	movq	FIRST(%rip), %rcx
	movq	(%rcx,%rax,8), %rcx
	movq	LAST(%rip), %rsi
	movq	(%rsi,%rax,8), %rdi
	cmpq	%rdi, %rcx
	je	.LBB7_7
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	movq	netsAssign(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
	movq	channelColumns(%rip), %rbp
	imulq	%rdx, %rbp
	cmpq	%rdi, %rcx
	movq	%rdi, %rsi
	cmovbq	%rcx, %rsi
	cmovbq	%rdi, %rcx
	addq	%rsi, %rbp
	orb	$2, (%rbx,%rbp)
	jmp	.LBB7_5
	.p2align	4, 0x90
.LBB7_4:                                # %.lr.ph69.i
                                        #   in Loop: Header=BB7_5 Depth=2
	addq	%rbx, %rdi
	movb	$3, (%rsi,%rdi)
.LBB7_5:                                # %.lr.ph69.i
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rsi
	movq	channelColumns(%rip), %rdi
	imulq	%rdx, %rdi
	cmpq	%rcx, %rsi
	jb	.LBB7_4
# BB#6:                                 # %DrawSegment.exit
                                        #   in Loop: Header=BB7_2 Depth=1
	addq	%rcx, %rdi
	orb	$1, (%rbx,%rdi)
	movq	channelNets(%rip), %rdx
.LBB7_7:                                #   in Loop: Header=BB7_2 Depth=1
	incq	%rax
	cmpq	%rdx, %rax
	jbe	.LBB7_2
.LBB7_8:                                # %.preheader
	movq	channelColumns(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB7_57
# BB#9:                                 # %.lr.ph.preheader
	xorl	%r14d, %r14d
	movl	$1, %eax
	.p2align	4, 0x90
.LBB7_10:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_29 Depth 2
                                        #     Child Loop BB7_24 Depth 2
                                        #     Child Loop BB7_50 Depth 2
                                        #     Child Loop BB7_53 Depth 2
                                        #     Child Loop BB7_42 Depth 2
                                        #     Child Loop BB7_35 Depth 2
	movq	BOT(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
	movq	TOP(%rip), %rsi
	movq	(%rsi,%rax,8), %rsi
	testq	%rdx, %rdx
	je	.LBB7_11
# BB#26:                                #   in Loop: Header=BB7_10 Depth=1
	testq	%rsi, %rsi
	je	.LBB7_27
# BB#14:                                # %.thread135
                                        #   in Loop: Header=BB7_10 Depth=1
	cmpq	%rdx, %rsi
	jne	.LBB7_44
.LBB7_15:                               #   in Loop: Header=BB7_10 Depth=1
	movq	FIRST(%rip), %rcx
	movq	(%rcx,%rdx,8), %rdi
	movq	LAST(%rip), %rbp
	movq	channelTracks(%rip), %rsi
	leaq	1(%rsi), %rcx
	movq	(%rbp,%rdx,8), %rdx
	orb	$8, (%r13,%rax)
	cmpq	%rdx, %rdi
	movq	channelColumns(%rip), %rdx
	jne	.LBB7_37
# BB#16:                                #   in Loop: Header=BB7_10 Depth=1
	cmpq	$2, %rcx
	jb	.LBB7_36
# BB#17:                                # %.lr.ph.i88.preheader
                                        #   in Loop: Header=BB7_10 Depth=1
	testb	$1, %sil
	jne	.LBB7_33
# BB#18:                                #   in Loop: Header=BB7_10 Depth=1
	movl	$1, %edi
	cmpq	$1, %rsi
	jne	.LBB7_35
	jmp	.LBB7_36
	.p2align	4, 0x90
.LBB7_11:                               #   in Loop: Header=BB7_10 Depth=1
	testq	%rsi, %rsi
	je	.LBB7_56
# BB#12:                                #   in Loop: Header=BB7_10 Depth=1
	movq	TOP(%rip), %rcx
	movq	(%rcx,%rax,8), %rcx
	testq	%rcx, %rcx
	je	.LBB7_13
# BB#19:                                #   in Loop: Header=BB7_10 Depth=1
	movq	netsAssign(%rip), %rdx
	movq	(%rdx,%rcx,8), %rcx
	orb	$8, (%r13,%rax)
	movq	channelColumns(%rip), %rdx
	cmpq	$2, %rcx
	jb	.LBB7_25
# BB#20:                                # %.lr.ph.i105.preheader
                                        #   in Loop: Header=BB7_10 Depth=1
	testb	$1, %cl
	jne	.LBB7_21
# BB#22:                                # %.lr.ph.i105.prol
                                        #   in Loop: Header=BB7_10 Depth=1
	addq	%rax, %rdx
	orb	$12, (%r13,%rdx)
	movq	channelColumns(%rip), %rdx
	movl	$2, %esi
	cmpq	$2, %rcx
	jne	.LBB7_24
	jmp	.LBB7_25
	.p2align	4, 0x90
.LBB7_27:                               #   in Loop: Header=BB7_10 Depth=1
	movq	netsAssign(%rip), %rsi
	movq	(%rsi,%rdx,8), %rdi
	movq	channelTracks(%rip), %rdx
	incq	%rdx
	cmpq	%rdx, %rdi
	movq	%rdx, %rsi
	cmovbq	%rdi, %rsi
	cmovaeq	%rdi, %rdx
	imulq	%rsi, %rcx
	addq	%rax, %rcx
	orb	$8, (%r13,%rcx)
	jmp	.LBB7_29
	.p2align	4, 0x90
.LBB7_28:                               # %.lr.ph.i97
                                        #   in Loop: Header=BB7_29 Depth=2
	imulq	%rsi, %rcx
	addq	%rax, %rcx
	orb	$12, (%r13,%rcx)
.LBB7_29:                               # %.lr.ph.i97
                                        #   Parent Loop BB7_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rsi
	movq	channelColumns(%rip), %rcx
	cmpq	%rdx, %rsi
	jb	.LBB7_28
# BB#30:                                # %DrawSegment.exit102
                                        #   in Loop: Header=BB7_10 Depth=1
	imulq	%rdx, %rcx
	addq	%rax, %rcx
	orb	$4, (%r13,%rcx)
	jmp	.LBB7_31
.LBB7_37:                               #   in Loop: Header=BB7_10 Depth=1
	cmpq	$2, %rcx
	jb	.LBB7_43
# BB#38:                                # %.lr.ph.i80.preheader
                                        #   in Loop: Header=BB7_10 Depth=1
	testb	$1, %sil
	jne	.LBB7_40
# BB#39:                                #   in Loop: Header=BB7_10 Depth=1
	movl	$1, %edi
	cmpq	$1, %rsi
	jne	.LBB7_42
	jmp	.LBB7_43
.LBB7_13:                               #   in Loop: Header=BB7_10 Depth=1
	xorl	%esi, %esi
	cmpq	%rdx, %rsi
	je	.LBB7_15
	.p2align	4, 0x90
.LBB7_44:                               #   in Loop: Header=BB7_10 Depth=1
	movq	netsAssign(%rip), %rdi
	movq	(%rdi,%rsi,8), %rcx
	cmpq	(%rdi,%rdx,8), %rcx
	jae	.LBB7_55
# BB#45:                                #   in Loop: Header=BB7_10 Depth=1
	orb	$8, (%r13,%rax)
	movq	channelColumns(%rip), %rdx
	cmpq	$2, %rcx
	jb	.LBB7_51
# BB#46:                                # %.lr.ph.i72.preheader
                                        #   in Loop: Header=BB7_10 Depth=1
	testb	$1, %cl
	jne	.LBB7_47
# BB#48:                                # %.lr.ph.i72.prol
                                        #   in Loop: Header=BB7_10 Depth=1
	addq	%rax, %rdx
	orb	$12, (%r13,%rdx)
	movq	channelColumns(%rip), %rdx
	movl	$2, %esi
	cmpq	$2, %rcx
	jne	.LBB7_50
	jmp	.LBB7_51
.LBB7_55:                               #   in Loop: Header=BB7_10 Depth=1
	movb	$1, (%r12,%rax)
	incl	%r14d
	jmp	.LBB7_56
.LBB7_21:                               #   in Loop: Header=BB7_10 Depth=1
	movl	$1, %esi
	cmpq	$2, %rcx
	je	.LBB7_25
	.p2align	4, 0x90
.LBB7_24:                               # %.lr.ph.i105
                                        #   Parent Loop BB7_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imulq	%rsi, %rdx
	addq	%rax, %rdx
	orb	$12, (%r13,%rdx)
	leaq	1(%rsi), %rdx
	imulq	channelColumns(%rip), %rdx
	addq	%rax, %rdx
	orb	$12, (%r13,%rdx)
	addq	$2, %rsi
	movq	channelColumns(%rip), %rdx
	cmpq	%rsi, %rcx
	jne	.LBB7_24
.LBB7_25:                               # %DrawSegment.exit110
                                        #   in Loop: Header=BB7_10 Depth=1
	imulq	%rcx, %rdx
	addq	%rax, %rdx
	orb	$4, (%r13,%rdx)
	movq	netsAssign(%rip), %rcx
	movq	TOP(%rip), %rdx
	jmp	.LBB7_32
.LBB7_33:                               # %.lr.ph.i88.prol
                                        #   in Loop: Header=BB7_10 Depth=1
	addq	%rax, %rdx
	orb	$12, (%r13,%rdx)
	movq	channelColumns(%rip), %rdx
	movl	$2, %edi
	cmpq	$1, %rsi
	je	.LBB7_36
	.p2align	4, 0x90
.LBB7_35:                               # %.lr.ph.i88
                                        #   Parent Loop BB7_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imulq	%rdi, %rdx
	addq	%rax, %rdx
	orb	$12, (%r13,%rdx)
	leaq	1(%rdi), %rdx
	imulq	channelColumns(%rip), %rdx
	addq	%rax, %rdx
	orb	$12, (%r13,%rdx)
	addq	$2, %rdi
	movq	channelColumns(%rip), %rdx
	cmpq	%rdi, %rcx
	jne	.LBB7_35
.LBB7_36:                               # %DrawSegment.exit93
                                        #   in Loop: Header=BB7_10 Depth=1
	imulq	%rcx, %rdx
	addq	%rax, %rdx
	orb	$4, (%r13,%rdx)
	jmp	.LBB7_56
.LBB7_40:                               # %.lr.ph.i80.prol
                                        #   in Loop: Header=BB7_10 Depth=1
	addq	%rax, %rdx
	orb	$12, (%r13,%rdx)
	movq	channelColumns(%rip), %rdx
	movl	$2, %edi
	cmpq	$1, %rsi
	je	.LBB7_43
	.p2align	4, 0x90
.LBB7_42:                               # %.lr.ph.i80
                                        #   Parent Loop BB7_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imulq	%rdi, %rdx
	addq	%rax, %rdx
	orb	$12, (%r13,%rdx)
	leaq	1(%rdi), %rdx
	imulq	channelColumns(%rip), %rdx
	addq	%rax, %rdx
	orb	$12, (%r13,%rdx)
	addq	$2, %rdi
	movq	channelColumns(%rip), %rdx
	cmpq	%rdi, %rcx
	jne	.LBB7_42
.LBB7_43:                               # %DrawSegment.exit85
                                        #   in Loop: Header=BB7_10 Depth=1
	imulq	%rcx, %rdx
	addq	%rax, %rdx
	orb	$4, (%r13,%rdx)
	jmp	.LBB7_31
.LBB7_47:                               #   in Loop: Header=BB7_10 Depth=1
	movl	$1, %esi
	cmpq	$2, %rcx
	je	.LBB7_51
	.p2align	4, 0x90
.LBB7_50:                               # %.lr.ph.i72
                                        #   Parent Loop BB7_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imulq	%rsi, %rdx
	addq	%rax, %rdx
	orb	$12, (%r13,%rdx)
	leaq	1(%rsi), %rdx
	imulq	channelColumns(%rip), %rdx
	addq	%rax, %rdx
	orb	$12, (%r13,%rdx)
	addq	$2, %rsi
	movq	channelColumns(%rip), %rdx
	cmpq	%rsi, %rcx
	jne	.LBB7_50
.LBB7_51:                               # %DrawSegment.exit77
                                        #   in Loop: Header=BB7_10 Depth=1
	imulq	%rcx, %rdx
	addq	%rax, %rdx
	orb	$4, (%r13,%rdx)
	movq	netsAssign(%rip), %rcx
	movq	TOP(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
	movq	channelColumns(%rip), %rsi
	imulq	(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movb	$1, (%r15,%rsi)
	movq	netsAssign(%rip), %rcx
	movq	BOT(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
	movq	(%rcx,%rdx,8), %rsi
	movq	channelTracks(%rip), %rcx
	incq	%rcx
	cmpq	%rcx, %rsi
	movq	%rcx, %rdx
	cmovbq	%rsi, %rdx
	cmovaeq	%rsi, %rcx
	movq	channelColumns(%rip), %rsi
	imulq	%rdx, %rsi
	addq	%rax, %rsi
	orb	$8, (%r13,%rsi)
	jmp	.LBB7_53
	.p2align	4, 0x90
.LBB7_52:                               # %.lr.ph.i64
                                        #   in Loop: Header=BB7_53 Depth=2
	imulq	%rdx, %rsi
	addq	%rax, %rsi
	orb	$12, (%r13,%rsi)
.LBB7_53:                               # %.lr.ph.i64
                                        #   Parent Loop BB7_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rdx
	movq	channelColumns(%rip), %rsi
	cmpq	%rcx, %rdx
	jb	.LBB7_52
# BB#54:                                # %DrawSegment.exit69
                                        #   in Loop: Header=BB7_10 Depth=1
	imulq	%rcx, %rsi
	addq	%rax, %rsi
	orb	$4, (%r13,%rsi)
.LBB7_31:                               #   in Loop: Header=BB7_10 Depth=1
	movq	netsAssign(%rip), %rcx
	movq	BOT(%rip), %rdx
.LBB7_32:                               #   in Loop: Header=BB7_10 Depth=1
	movq	(%rdx,%rax,8), %rdx
	movq	channelColumns(%rip), %rsi
	imulq	(%rcx,%rdx,8), %rsi
	addq	%rax, %rsi
	movb	$1, (%r15,%rsi)
.LBB7_56:                               #   in Loop: Header=BB7_10 Depth=1
	incq	%rax
	movq	channelColumns(%rip), %rcx
	cmpq	%rcx, %rax
	jbe	.LBB7_10
.LBB7_57:                               # %._crit_edge
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	DrawNets, .Lfunc_end7-DrawNets
	.cfi_endproc

	.globl	Maze1
	.p2align	4, 0x90
	.type	Maze1,@function
Maze1:                                  # @Maze1
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 48
.Lcfi38:
	.cfi_offset %rbx, -40
.Lcfi39:
	.cfi_offset %r12, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %r15, -16
	movq	channelColumns(%rip), %rax
	xorl	%r14d, %r14d
	testq	%rax, %rax
	je	.LBB8_19
# BB#1:                                 # %.lr.ph.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB8_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	mazeRoute(%rip), %rcx
	cmpb	$0, (%rcx,%rbx)
	je	.LBB8_18
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	netsAssign(%rip), %rcx
	movq	TOP(%rip), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movq	(%rcx,%rdx,8), %r15
	movq	BOT(%rip), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movq	(%rcx,%rdx,8), %r12
	cmpq	$2, %rbx
	jb	.LBB8_7
# BB#4:                                 #   in Loop: Header=BB8_2 Depth=1
	cmpq	$2, %r12
	jb	.LBB8_7
# BB#5:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	channelTracks(%rip), %rsi
	incq	%rsi
	movl	$-1, (%rsp)
	xorl	%ecx, %ecx
	movl	$-1, %r9d
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r15, %r8
	callq	Maze1Mech
	testl	%eax, %eax
	jne	.LBB8_16
# BB#6:                                 # %._crit_edge53
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	channelColumns(%rip), %rax
.LBB8_7:                                #   in Loop: Header=BB8_2 Depth=1
	cmpq	$2, %r12
	jb	.LBB8_10
# BB#8:                                 #   in Loop: Header=BB8_2 Depth=1
	cmpq	%rax, %rbx
	jae	.LBB8_10
# BB#9:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	channelTracks(%rip), %rsi
	incq	%rsi
	movl	$-1, (%rsp)
	xorl	%ecx, %ecx
	movl	$1, %r9d
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r15, %r8
	callq	Maze1Mech
	testl	%eax, %eax
	jne	.LBB8_16
.LBB8_10:                               #   in Loop: Header=BB8_2 Depth=1
	cmpq	$2, %rbx
	jb	.LBB8_13
# BB#11:                                #   in Loop: Header=BB8_2 Depth=1
	movq	channelTracks(%rip), %rcx
	cmpq	%rcx, %r15
	jae	.LBB8_13
# BB#12:                                #   in Loop: Header=BB8_2 Depth=1
	incq	%rcx
	movl	$1, (%rsp)
	xorl	%esi, %esi
	movl	$-1, %r9d
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r12, %r8
	callq	Maze1Mech
	testl	%eax, %eax
	jne	.LBB8_16
	.p2align	4, 0x90
.LBB8_13:                               #   in Loop: Header=BB8_2 Depth=1
	cmpq	channelColumns(%rip), %rbx
	jae	.LBB8_17
# BB#14:                                #   in Loop: Header=BB8_2 Depth=1
	movq	channelTracks(%rip), %rcx
	cmpq	%rcx, %r15
	jae	.LBB8_17
# BB#15:                                #   in Loop: Header=BB8_2 Depth=1
	incq	%rcx
	movl	$1, (%rsp)
	xorl	%esi, %esi
	movl	$1, %r9d
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r12, %r8
	callq	Maze1Mech
	testl	%eax, %eax
	je	.LBB8_17
	.p2align	4, 0x90
.LBB8_16:                               #   in Loop: Header=BB8_2 Depth=1
	movq	mazeRoute(%rip), %rax
	movb	$0, (%rax,%rbx)
	movq	TOP(%rip), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	CleanNet
	movq	BOT(%rip), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	CleanNet
	jmp	.LBB8_18
	.p2align	4, 0x90
.LBB8_17:                               #   in Loop: Header=BB8_2 Depth=1
	incl	%r14d
.LBB8_18:                               #   in Loop: Header=BB8_2 Depth=1
	incq	%rbx
	movq	channelColumns(%rip), %rax
	cmpq	%rax, %rbx
	jbe	.LBB8_2
.LBB8_19:                               # %._crit_edge
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	Maze1, .Lfunc_end8-Maze1
	.cfi_endproc

	.p2align	4, 0x90
	.type	Maze1Mech,@function
Maze1Mech:                              # @Maze1Mech
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
.Lcfi48:
	.cfi_offset %rbx, -56
.Lcfi49:
	.cfi_offset %r12, -48
.Lcfi50:
	.cfi_offset %r13, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	movq	%rdx, %r11
	movl	56(%rsp), %edx
	movq	vertPlane(%rip), %rax
	cmpq	%r11, %rsi
	movq	%r11, %r14
	cmovbq	%rsi, %r14
	movq	channelColumns(%rip), %r10
	cmovbq	%r11, %rsi
	movq	%r10, %r9
	imulq	%r14, %r9
	addq	%rdi, %r9
	movq	%rax, %r15
	leaq	(%rax,%r9), %rbp
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB9_1:                                # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	cmpb	$0, (%rbp)
	jne	.LBB9_51
# BB#2:                                 #   in Loop: Header=BB9_1 Depth=1
	incq	%rbx
	addq	%r10, %rbp
	cmpq	%rsi, %rbx
	jbe	.LBB9_1
# BB#3:                                 # %SegmentFree.exit
	movq	%r8, -8(%rsp)           # 8-byte Spill
	movslq	%edx, %r8
	addq	%r11, %r8
	cmpq	%rcx, %r8
	movq	%r8, %r13
	cmovaq	%rcx, %r13
	cmovaq	%r8, %rcx
	movq	%r10, %rbp
	imulq	%r13, %rbp
	addq	%rdi, %rbp
	addq	%r15, %rbp
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB9_4:                                # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rbp)
	jne	.LBB9_51
# BB#5:                                 #   in Loop: Header=BB9_4 Depth=1
	incq	%rdx
	addq	%r10, %rbp
	cmpq	%rcx, %rdx
	jbe	.LBB9_4
# BB#6:                                 # %SegmentFree.exit112
	movl	%r12d, -28(%rsp)        # 4-byte Spill
	movslq	%r12d, %rdx
	movq	%r10, %rbp
	imulq	%r8, %rbp
	leaq	(%rdx,%rdi), %r12
	testl	%edx, %edx
	movq	%r12, -24(%rsp)         # 8-byte Spill
	movq	%rdi, -16(%rsp)         # 8-byte Spill
	je	.LBB9_7
# BB#10:
	cmpq	%rdi, %r12
	movq	%r12, %rbx
	cmovaq	%rdi, %rbx
	movq	%rdi, %rdx
	cmovaq	%r12, %rdx
	addq	%r15, %rbp
	.p2align	4, 0x90
.LBB9_11:                               # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rbp,%rbx)
	jne	.LBB9_51
# BB#12:                                #   in Loop: Header=BB9_11 Depth=1
	incq	%rbx
	cmpq	%rdx, %rbx
	jbe	.LBB9_11
	jmp	.LBB9_13
.LBB9_7:
	addq	%rdi, %rbp
	addq	%r15, %rbp
	movq	%r8, %rdx
	.p2align	4, 0x90
.LBB9_8:                                # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rbp)
	jne	.LBB9_51
# BB#9:                                 #   in Loop: Header=BB9_8 Depth=1
	incq	%rdx
	addq	%r10, %rbp
	cmpq	%r8, %rdx
	jbe	.LBB9_8
.LBB9_13:                               # %SegmentFree.exit108
	movq	-8(%rsp), %rdx          # 8-byte Reload
	cmpq	%rdx, %r8
	movq	%rdx, %r12
	cmovbq	%r8, %r12
	movq	%r8, %rbx
	cmovbq	%rdx, %rbx
	movq	%r10, %rbp
	imulq	%r12, %rbp
	addq	-24(%rsp), %rbp         # 8-byte Folded Reload
	addq	%r15, %rbp
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB9_14:                               # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rbp)
	jne	.LBB9_51
# BB#15:                                #   in Loop: Header=BB9_14 Depth=1
	incq	%rdx
	addq	%r10, %rbp
	cmpq	%rbx, %rdx
	jbe	.LBB9_14
# BB#16:                                # %SegmentFree.exit104
	movq	TOP(%rip), %rdx
	movq	-24(%rsp), %r10         # 8-byte Reload
	movq	(%rdx,%r10,8), %rbp
	testq	%rbp, %rbp
	je	.LBB9_20
# BB#17:
	movq	BOT(%rip), %rdx
	movq	(%rdx,%r10,8), %rdx
	testq	%rdx, %rdx
	je	.LBB9_20
# BB#18:
	cmpq	%rdx, %rbp
	je	.LBB9_20
# BB#19:                                # %HasVCV.exit
	movq	netsAssign(%rip), %r10
	movq	(%r10,%rbp,8), %rbp
	cmpq	(%r10,%rdx,8), %rbp
	movq	-24(%rsp), %r10         # 8-byte Reload
	ja	.LBB9_51
.LBB9_20:                               # %HasVCV.exit.thread
	orb	$8, (%r15,%r9)
	incq	%r14
	movq	channelColumns(%rip), %rax
	cmpq	%rsi, %r14
	jae	.LBB9_21
# BB#22:                                # %.lr.ph.i95.preheader
	movq	-16(%rsp), %rbp         # 8-byte Reload
	.p2align	4, 0x90
.LBB9_23:                               # %.lr.ph.i95
                                        # =>This Inner Loop Header: Depth=1
	imulq	%r14, %rax
	addq	%rbp, %rax
	orb	$12, (%r15,%rax)
	incq	%r14
	movq	channelColumns(%rip), %rax
	cmpq	%rsi, %r14
	jb	.LBB9_23
	jmp	.LBB9_24
.LBB9_21:
	movq	-16(%rsp), %rbp         # 8-byte Reload
.LBB9_24:                               # %DrawSegment.exit100
	imulq	%rsi, %rax
	addq	%rbp, %rax
	orb	$4, (%r15,%rax)
	movq	viaPlane(%rip), %rax
	imulq	channelColumns(%rip), %r11
	addq	%rbp, %r11
	movb	$1, (%rax,%r11)
	movq	channelColumns(%rip), %rdx
	imulq	%r13, %rdx
	addq	%rbp, %rdx
	orb	$8, (%r15,%rdx)
	jmp	.LBB9_26
	.p2align	4, 0x90
.LBB9_25:                               # %.lr.ph.i86
                                        #   in Loop: Header=BB9_26 Depth=1
	imulq	%r13, %rdx
	addq	%rbp, %rdx
	orb	$12, (%r15,%rdx)
.LBB9_26:                               # %.lr.ph.i86
                                        # =>This Inner Loop Header: Depth=1
	incq	%r13
	movq	channelColumns(%rip), %rdx
	cmpq	%rcx, %r13
	jb	.LBB9_25
# BB#27:                                # %DrawSegment.exit91
	imulq	%rcx, %rdx
	addq	%rbp, %rdx
	orb	$4, (%r15,%rdx)
	movq	channelColumns(%rip), %rcx
	imulq	%r8, %rcx
	cmpl	$0, -28(%rsp)           # 4-byte Folded Reload
	je	.LBB9_28
# BB#33:
	cmpq	%rbp, %r10
	movq	%r10, %rsi
	cmovaq	%rbp, %rsi
	movq	%rbp, %rdx
	cmovaq	%r10, %rdx
	addq	%rsi, %rcx
	orb	$2, (%r15,%rcx)
	jmp	.LBB9_35
	.p2align	4, 0x90
.LBB9_34:                               # %.lr.ph69.i77
                                        #   in Loop: Header=BB9_35 Depth=1
	addq	%r15, %rcx
	movb	$3, (%rsi,%rcx)
.LBB9_35:                               # %.lr.ph69.i77
                                        # =>This Inner Loop Header: Depth=1
	incq	%rsi
	movq	channelColumns(%rip), %rcx
	imulq	%r8, %rcx
	cmpq	%rdx, %rsi
	jb	.LBB9_34
# BB#36:                                # %._crit_edge70.i79
	addq	%rdx, %rcx
	movb	$1, %dl
	jmp	.LBB9_37
.LBB9_28:
	addq	%rbp, %rcx
	orb	$8, (%r15,%rcx)
	cmpq	$-1, %r8
	je	.LBB9_30
# BB#29:
	movq	channelColumns(%rip), %rcx
	jmp	.LBB9_32
.LBB9_30:                               # %.lr.ph.i71.preheader.new
	orb	$12, (%r15,%rbp)
	movl	$2, %edx
	movq	channelColumns(%rip), %rcx
	.p2align	4, 0x90
.LBB9_31:                               # %.lr.ph.i71
                                        # =>This Inner Loop Header: Depth=1
	leaq	-1(%rdx), %rsi
	imulq	%rcx, %rsi
	addq	%rbp, %rsi
	orb	$12, (%r15,%rsi)
	movq	channelColumns(%rip), %rcx
	imulq	%rdx, %rcx
	addq	%rbp, %rcx
	orb	$12, (%r15,%rcx)
	movq	channelColumns(%rip), %rcx
	addq	$2, %rdx
	jne	.LBB9_31
.LBB9_32:                               # %._crit_edge.i73
	imulq	%r8, %rcx
	addq	%rbp, %rcx
	movb	$4, %dl
.LBB9_37:                               # %DrawSegment.exit82
	orb	%dl, (%r15,%rcx)
	movq	channelColumns(%rip), %rcx
	imulq	%r12, %rcx
	addq	%r10, %rcx
	orb	$8, (%r15,%rcx)
	jmp	.LBB9_39
	.p2align	4, 0x90
.LBB9_38:                               # %.lr.ph.i62
                                        #   in Loop: Header=BB9_39 Depth=1
	imulq	%r12, %rcx
	addq	%r10, %rcx
	orb	$12, (%r15,%rcx)
.LBB9_39:                               # %.lr.ph.i62
                                        # =>This Inner Loop Header: Depth=1
	incq	%r12
	movq	channelColumns(%rip), %rcx
	cmpq	%rbx, %r12
	jb	.LBB9_38
# BB#40:                                # %DrawSegment.exit67
	imulq	%rbx, %rcx
	addq	%r10, %rcx
	orb	$4, (%r15,%rcx)
	movq	channelColumns(%rip), %rcx
	movq	-8(%rsp), %r8           # 8-byte Reload
	imulq	%r8, %rcx
	addq	%r10, %rcx
	movb	$1, (%rax,%rcx)
	movq	horzPlane(%rip), %rax
	movq	channelColumns(%rip), %rcx
	imulq	%r8, %rcx
	cmpl	$0, -28(%rsp)           # 4-byte Folded Reload
	je	.LBB9_41
# BB#46:
	cmpq	%rbp, %r10
	movq	%rbp, %rdx
	cmovbq	%r10, %rdx
	cmovbq	%rbp, %r10
	addq	%rdx, %rcx
	orb	$2, (%rax,%rcx)
	jmp	.LBB9_48
	.p2align	4, 0x90
.LBB9_47:                               # %.lr.ph69.i
                                        #   in Loop: Header=BB9_48 Depth=1
	addq	%rax, %rcx
	movb	$3, (%rdx,%rcx)
.LBB9_48:                               # %.lr.ph69.i
                                        # =>This Inner Loop Header: Depth=1
	incq	%rdx
	movq	channelColumns(%rip), %rcx
	imulq	%r8, %rcx
	cmpq	%r10, %rdx
	jb	.LBB9_47
# BB#49:                                # %._crit_edge70.i
	addq	%r10, %rcx
	movb	$1, %dl
	jmp	.LBB9_50
.LBB9_41:
	addq	%r10, %rcx
	orb	$8, (%rax,%rcx)
	cmpq	$-1, %r8
	je	.LBB9_43
# BB#42:
	movq	channelColumns(%rip), %rcx
	jmp	.LBB9_45
.LBB9_43:                               # %.lr.ph.i.preheader.new
	orb	$12, (%rax,%r10)
	movl	$2, %edx
	movq	channelColumns(%rip), %rcx
	.p2align	4, 0x90
.LBB9_44:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	-1(%rdx), %rsi
	imulq	%rcx, %rsi
	addq	%r10, %rsi
	orb	$12, (%rax,%rsi)
	movq	channelColumns(%rip), %rcx
	imulq	%rdx, %rcx
	addq	%r10, %rcx
	orb	$12, (%rax,%rcx)
	movq	channelColumns(%rip), %rcx
	addq	$2, %rdx
	jne	.LBB9_44
.LBB9_45:                               # %._crit_edge.i
	imulq	%r8, %rcx
	addq	%r10, %rcx
	movb	$4, %dl
.LBB9_50:                               # %DrawSegment.exit
	orb	%dl, (%rax,%rcx)
	movl	$1, %eax
.LBB9_51:                               # %SegmentFree.exit.thread
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	Maze1Mech, .Lfunc_end9-Maze1Mech
	.cfi_endproc

	.p2align	4, 0x90
	.type	CleanNet,@function
CleanNet:                               # @CleanNet
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 32
.Lcfi57:
	.cfi_offset %rbx, -32
.Lcfi58:
	.cfi_offset %r14, -24
.Lcfi59:
	.cfi_offset %r15, -16
	movq	FIRST(%rip), %rax
	movq	(%rax,%rdi,8), %r9
	movq	LAST(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	cmpq	%rax, %r9
	ja	.LBB10_6
# BB#1:                                 # %.lr.ph84
	movq	TOP(%rip), %rcx
	movq	mazeRoute(%rip), %rdx
	movq	BOT(%rip), %rsi
	movq	%r9, %rbx
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rdi, (%rcx,%rbx,8)
	je	.LBB10_4
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	cmpq	%rdi, (%rsi,%rbx,8)
	jne	.LBB10_5
.LBB10_4:                               #   in Loop: Header=BB10_2 Depth=1
	cmpb	$0, (%rdx,%rbx)
	jne	.LBB10_29
.LBB10_5:                               #   in Loop: Header=BB10_2 Depth=1
	incq	%rbx
	cmpq	%rax, %rbx
	jbe	.LBB10_2
.LBB10_6:                               # %._crit_edge85
	movq	netsAssign(%rip), %rcx
	movq	(%rcx,%rdi,8), %r11
	movq	horzPlane(%rip), %r8
	movq	channelColumns(%rip), %rdi
	imulq	%r11, %rdi
	leaq	(%r8,%rdi), %rcx
	xorl	%ebx, %ebx
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB10_7:                               # =>This Inner Loop Header: Depth=1
	incb	%bl
	testb	$1, (%rcx,%rdx)
	leaq	-1(%rdx), %rdx
	jne	.LBB10_7
# BB#8:                                 # %.preheader87.preheader
	decq	%rax
	leaq	1(%rdx), %r15
	.p2align	4, 0x90
.LBB10_9:                               # %.preheader87
                                        # =>This Inner Loop Header: Depth=1
	testb	$2, 1(%rcx,%rax)
	leaq	1(%rax), %rax
	jne	.LBB10_9
# BB#10:                                # %.preheader67
	cmpq	%rax, %r15
	jbe	.LBB10_12
# BB#11:
	xorl	%esi, %esi
	movl	$9999999, %r10d         # imm = 0x98967F
	cmpq	%r15, %r10
	ja	.LBB10_17
	jmp	.LBB10_25
.LBB10_12:                              # %.lr.ph78
	movq	viaPlane(%rip), %r14
	addq	%rdi, %r14
	movl	$9999999, %r10d         # imm = 0x98967F
	xorl	%esi, %esi
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB10_13:                              # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%r14,%rcx)
	je	.LBB10_15
# BB#14:                                #   in Loop: Header=BB10_13 Depth=1
	cmpq	%r10, %rcx
	cmovbq	%rcx, %r10
	cmpq	%rsi, %rcx
	cmovaq	%rcx, %rsi
.LBB10_15:                              #   in Loop: Header=BB10_13 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jbe	.LBB10_13
# BB#16:                                # %._crit_edge79
	cmpq	%r15, %r10
	jbe	.LBB10_25
.LBB10_17:                              # %.preheader.preheader
	movq	%r10, %r14
	subq	%rdx, %r14
	leal	-1(%r14), %ecx
	addq	$-2, %r14
	testb	$3, %cl
	je	.LBB10_21
# BB#18:                                # %.preheader.prol.preheader
	movl	%r10d, %ecx
	subl	%r9d, %ecx
	addb	%cl, %bl
	addb	$3, %bl
	movzbl	%bl, %r9d
	andl	$3, %r9d
	xorl	%ebx, %ebx
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB10_19:                              # %.preheader.prol
                                        # =>This Inner Loop Header: Depth=1
	addq	%rcx, %rdi
	addq	%r8, %rdi
	movb	$0, (%rdx,%rdi)
	movq	channelColumns(%rip), %rdi
	imulq	%r11, %rdi
	incq	%rbx
	incq	%rcx
	cmpq	%rbx, %r9
	jne	.LBB10_19
# BB#20:                                # %.preheader.prol.loopexit.unr-lcssa
	leaq	1(%rdx,%rbx), %r15
.LBB10_21:                              # %.preheader.prol.loopexit
	cmpq	$3, %r14
	jb	.LBB10_24
# BB#22:                                # %.preheader.preheader.new
	movq	%r8, %rdx
	movq	%r10, %rbx
	.p2align	4, 0x90
.LBB10_23:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	addq	%r15, %rdi
	movb	$0, (%rdx,%rdi)
	movq	channelColumns(%rip), %rcx
	imulq	%r11, %rcx
	addq	%r15, %rcx
	movb	$0, 1(%rdx,%rcx)
	movq	channelColumns(%rip), %rcx
	imulq	%r11, %rcx
	addq	%r15, %rcx
	movb	$0, 2(%rdx,%rcx)
	movq	channelColumns(%rip), %rcx
	imulq	%r11, %rcx
	addq	%r15, %rcx
	movb	$0, 3(%rdx,%rcx)
	movq	channelColumns(%rip), %rdi
	imulq	%r11, %rdi
	addq	$-4, %rbx
	addq	$4, %rdx
	cmpq	%rbx, %r15
	jne	.LBB10_23
.LBB10_24:                              # %._crit_edge
	addq	%r10, %rdi
	andb	$-2, (%r8,%rdi)
.LBB10_25:
	cmpq	%rax, %rsi
	jae	.LBB10_29
# BB#26:
	movq	channelColumns(%rip), %rcx
	imulq	%r11, %rcx
	addq	%rsi, %rcx
	andb	$-3, (%r8,%rcx)
	jmp	.LBB10_28
	.p2align	4, 0x90
.LBB10_27:                              # %.lr.ph
                                        #   in Loop: Header=BB10_28 Depth=1
	movq	channelColumns(%rip), %rcx
	imulq	%r11, %rcx
	addq	%r8, %rcx
	movb	$0, (%rsi,%rcx)
.LBB10_28:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incq	%rsi
	cmpq	%rax, %rsi
	jbe	.LBB10_27
.LBB10_29:                              # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	CleanNet, .Lfunc_end10-CleanNet
	.cfi_endproc

	.globl	ExtendOK
	.p2align	4, 0x90
	.type	ExtendOK,@function
ExtendOK:                               # @ExtendOK
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 16
.Lcfi61:
	.cfi_offset %rbx, -16
	cmpq	%r9, %rcx
	cmovbq	%rcx, %r9
	cmpq	%r8, %rdx
	movq	%r8, %rax
	cmovbq	%rdx, %rax
	cmovbq	%r8, %rdx
	movq	FIRST(%rip), %rcx
	movq	(%rcx,%rdi,8), %r11
	cmpq	%r11, %rax
	movq	LAST(%rip), %rcx
	movq	(%rcx,%rdi,8), %r8
	jae	.LBB11_1
# BB#7:
	decq	%r11
	movq	channelColumns(%rip), %rbx
	movq	%rbx, %r10
	imulq	%r9, %r10
	cmpq	%r8, %rdx
	jbe	.LBB11_25
# BB#8:
	cmpq	%r11, %rax
	jne	.LBB11_13
# BB#9:
	addq	%r10, %rax
	addq	%rsi, %rax
	movq	%r9, %rcx
	.p2align	4, 0x90
.LBB11_10:                              # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rax)
	jne	.LBB11_11
# BB#12:                                #   in Loop: Header=BB11_10 Depth=1
	incq	%rcx
	addq	%rbx, %rax
	cmpq	%r9, %rcx
	jbe	.LBB11_10
	jmp	.LBB11_17
.LBB11_1:
	movl	$1, %eax
	cmpq	%r8, %rdx
	jbe	.LBB11_43
# BB#2:
	movq	LAST(%rip), %rcx
	movq	(%rcx,%rdi,8), %rcx
	cmpq	%rcx, %rdx
	jbe	.LBB11_41
# BB#3:
	incq	%rcx
	movq	channelColumns(%rip), %rdi
	movq	%rdi, %r8
	imulq	%r9, %r8
	cmpq	%rdx, %rcx
	jne	.LBB11_37
# BB#4:
	addq	%rdx, %r8
	addq	%r8, %rsi
	movq	%r9, %rcx
	.p2align	4, 0x90
.LBB11_5:                               # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rsi)
	jne	.LBB11_6
# BB#36:                                #   in Loop: Header=BB11_5 Depth=1
	incq	%rcx
	addq	%rdi, %rsi
	cmpq	%r9, %rcx
	jbe	.LBB11_5
	jmp	.LBB11_43
.LBB11_25:
	cmpq	%r11, %rax
	jne	.LBB11_31
# BB#26:
	addq	%rax, %r10
	addq	%r10, %rsi
	movq	%r9, %rax
	.p2align	4, 0x90
.LBB11_27:                              # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rsi)
	jne	.LBB11_28
# BB#29:                                #   in Loop: Header=BB11_27 Depth=1
	incq	%rax
	addq	%rbx, %rsi
	cmpq	%r9, %rax
	jbe	.LBB11_27
# BB#30:
	movl	$1, %eax
	popq	%rbx
	retq
.LBB11_13:
	movq	%r11, %rcx
	cmovbq	%rax, %rcx
	cmovbq	%r11, %rax
	leaq	(%rsi,%r10), %rdi
	.p2align	4, 0x90
.LBB11_14:                              # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rdi,%rcx)
	jne	.LBB11_15
# BB#16:                                #   in Loop: Header=BB11_14 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jbe	.LBB11_14
.LBB11_17:                              # %SegmentFree.exit
	incq	%r8
	cmpq	%rdx, %r8
	jne	.LBB11_21
# BB#18:
	addq	%rdx, %r10
	addq	%r10, %rsi
	movq	%r9, %rcx
	.p2align	4, 0x90
.LBB11_19:                              # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rsi)
	jne	.LBB11_20
# BB#42:                                #   in Loop: Header=BB11_19 Depth=1
	incq	%rcx
	addq	%rbx, %rsi
	movl	$1, %eax
	cmpq	%r9, %rcx
	jbe	.LBB11_19
	jmp	.LBB11_43
.LBB11_31:
	movq	%r11, %rcx
	cmovbq	%rax, %rcx
	cmovbq	%r11, %rax
	addq	%r10, %rsi
	.p2align	4, 0x90
.LBB11_32:                              # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rsi,%rcx)
	jne	.LBB11_33
# BB#34:                                #   in Loop: Header=BB11_32 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jbe	.LBB11_32
# BB#35:
	movl	$1, %eax
	popq	%rbx
	retq
.LBB11_37:
	movq	%rdx, %rdi
	cmovbq	%rcx, %rdi
	cmovbq	%rdx, %rcx
	addq	%r8, %rsi
	.p2align	4, 0x90
.LBB11_38:                              # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rsi,%rdi)
	jne	.LBB11_39
# BB#40:                                #   in Loop: Header=BB11_38 Depth=1
	incq	%rdi
	cmpq	%rcx, %rdi
	jbe	.LBB11_38
	jmp	.LBB11_43
.LBB11_11:
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB11_28:
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB11_15:
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB11_33:
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB11_6:
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB11_39:
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB11_21:
	movq	%rdx, %rcx
	cmovbq	%r8, %rcx
	cmovbq	%rdx, %r8
	addq	%r10, %rsi
	.p2align	4, 0x90
.LBB11_22:                              # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rsi,%rcx)
	jne	.LBB11_23
# BB#24:                                #   in Loop: Header=BB11_22 Depth=1
	incq	%rcx
	movl	$1, %eax
	cmpq	%r8, %rcx
	jbe	.LBB11_22
.LBB11_43:                              # %SegmentFree.exit66
	popq	%rbx
	retq
.LBB11_20:
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB11_23:
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB11_41:
	callq	abort
.Lfunc_end11:
	.size	ExtendOK, .Lfunc_end11-ExtendOK
	.cfi_endproc

	.globl	Maze2
	.p2align	4, 0x90
	.type	Maze2,@function
Maze2:                                  # @Maze2
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 48
.Lcfi67:
	.cfi_offset %rbx, -48
.Lcfi68:
	.cfi_offset %r12, -40
.Lcfi69:
	.cfi_offset %r13, -32
.Lcfi70:
	.cfi_offset %r14, -24
.Lcfi71:
	.cfi_offset %r15, -16
	movq	channelColumns(%rip), %rax
	xorl	%r14d, %r14d
	testq	%rax, %rax
	je	.LBB12_19
# BB#1:                                 # %.lr.ph.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB12_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	mazeRoute(%rip), %rcx
	cmpb	$0, (%rcx,%rbx)
	je	.LBB12_18
# BB#3:                                 #   in Loop: Header=BB12_2 Depth=1
	movq	netsAssign(%rip), %rcx
	movq	TOP(%rip), %rdx
	movq	(%rdx,%rbx,8), %rdi
	movq	(%rcx,%rdi,8), %r15
	movq	BOT(%rip), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movq	(%rcx,%rdx,8), %r12
	cmpq	$2, %rbx
	jb	.LBB12_7
# BB#4:                                 #   in Loop: Header=BB12_2 Depth=1
	cmpq	$2, %r12
	jb	.LBB12_7
# BB#5:                                 #   in Loop: Header=BB12_2 Depth=1
	movq	channelTracks(%rip), %rdx
	incq	%rdx
	leaq	-1(%rbx), %r10
	leaq	-1(%r12), %rax
	subq	$8, %rsp
.Lcfi72:
	.cfi_adjust_cfa_offset 8
	movl	$0, %r8d
	movq	%rbx, %rsi
	movq	%r12, %rcx
	movq	%r15, %r9
	pushq	%rax
.Lcfi73:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi74:
	.cfi_adjust_cfa_offset 8
	pushq	$-1
.Lcfi75:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi76:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi77:
	.cfi_adjust_cfa_offset 8
	callq	Maze2Mech
	addq	$48, %rsp
.Lcfi78:
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB12_16
# BB#6:                                 # %._crit_edge65
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	channelColumns(%rip), %rax
.LBB12_7:                               #   in Loop: Header=BB12_2 Depth=1
	cmpq	$2, %r12
	jb	.LBB12_10
# BB#8:                                 #   in Loop: Header=BB12_2 Depth=1
	cmpq	%rax, %rbx
	jae	.LBB12_10
# BB#9:                                 #   in Loop: Header=BB12_2 Depth=1
	movq	TOP(%rip), %rcx
	movq	(%rcx,%rbx,8), %rdi
	movq	channelTracks(%rip), %rdx
	incq	%rdx
	leaq	1(%rbx), %r10
	leaq	-1(%r12), %r11
	subq	$8, %rsp
.Lcfi79:
	.cfi_adjust_cfa_offset 8
	movl	$0, %r8d
	movq	%rbx, %rsi
	movq	%r12, %rcx
	movq	%r15, %r9
	pushq	%r11
.Lcfi80:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi81:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi82:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi83:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi84:
	.cfi_adjust_cfa_offset 8
	callq	Maze2Mech
	addq	$48, %rsp
.Lcfi85:
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB12_16
.LBB12_10:                              #   in Loop: Header=BB12_2 Depth=1
	cmpq	$2, %rbx
	jb	.LBB12_13
# BB#11:                                #   in Loop: Header=BB12_2 Depth=1
	movq	channelTracks(%rip), %rax
	cmpq	%rax, %r15
	jae	.LBB12_13
# BB#12:                                #   in Loop: Header=BB12_2 Depth=1
	movq	BOT(%rip), %rcx
	movq	(%rcx,%rbx,8), %rdi
	leaq	1(%rax), %r8
	leaq	-1(%rbx), %r10
	leaq	1(%r15), %r11
	subq	$8, %rsp
.Lcfi86:
	.cfi_adjust_cfa_offset 8
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r15, %rcx
	movq	%r12, %r9
	pushq	%rax
.Lcfi87:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi88:
	.cfi_adjust_cfa_offset 8
	pushq	$-1
.Lcfi89:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi90:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi91:
	.cfi_adjust_cfa_offset 8
	callq	Maze2Mech
	addq	$48, %rsp
.Lcfi92:
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB12_16
	.p2align	4, 0x90
.LBB12_13:                              #   in Loop: Header=BB12_2 Depth=1
	movq	channelColumns(%rip), %rax
	cmpq	%rax, %rbx
	jae	.LBB12_17
# BB#14:                                #   in Loop: Header=BB12_2 Depth=1
	movq	channelTracks(%rip), %r10
	cmpq	%r10, %r15
	jae	.LBB12_17
# BB#15:                                #   in Loop: Header=BB12_2 Depth=1
	movq	BOT(%rip), %rcx
	movq	(%rcx,%rbx,8), %rdi
	leaq	1(%r10), %r8
	leaq	1(%rbx), %r11
	subq	$8, %rsp
.Lcfi93:
	.cfi_adjust_cfa_offset 8
	xorl	%edx, %edx
	leaq	1(%r15), %r13
	movq	%rbx, %rsi
	movq	%r15, %rcx
	movq	%r12, %r9
	pushq	%r10
.Lcfi94:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi95:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi96:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi97:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi98:
	.cfi_adjust_cfa_offset 8
	callq	Maze2Mech
	addq	$48, %rsp
.Lcfi99:
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	je	.LBB12_17
	.p2align	4, 0x90
.LBB12_16:                              #   in Loop: Header=BB12_2 Depth=1
	movq	mazeRoute(%rip), %rax
	movb	$0, (%rax,%rbx)
	movq	TOP(%rip), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	CleanNet
	movq	BOT(%rip), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	CleanNet
	jmp	.LBB12_18
	.p2align	4, 0x90
.LBB12_17:                              #   in Loop: Header=BB12_2 Depth=1
	incl	%r14d
.LBB12_18:                              #   in Loop: Header=BB12_2 Depth=1
	incq	%rbx
	movq	channelColumns(%rip), %rax
	cmpq	%rax, %rbx
	jbe	.LBB12_2
.LBB12_19:                              # %._crit_edge
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	Maze2, .Lfunc_end12-Maze2
	.cfi_endproc

	.p2align	4, 0x90
	.type	Maze2Mech,@function
Maze2Mech:                              # @Maze2Mech
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi100:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi101:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi102:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi103:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi104:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi106:
	.cfi_def_cfa_offset 192
.Lcfi107:
	.cfi_offset %rbx, -56
.Lcfi108:
	.cfi_offset %r12, -48
.Lcfi109:
	.cfi_offset %r13, -40
.Lcfi110:
	.cfi_offset %r14, -32
.Lcfi111:
	.cfi_offset %r15, -24
.Lcfi112:
	.cfi_offset %rbp, -16
	movq	%r8, %r11
	movq	%rdx, %r13
	movq	%rsi, %r8
	movq	216(%rsp), %r15
	movq	224(%rsp), %rbx
	incq	%rbx
	xorl	%eax, %eax
	cmpq	%r15, %rbx
	je	.LBB13_62
# BB#1:                                 # %.preheader.lr.ph
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movslq	208(%rsp), %rax
	movq	200(%rsp), %rdx
	movq	%rax, 80(%rsp)          # 8-byte Spill
	addq	%rax, %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	cmpq	%rcx, %r13
	movq	%rcx, %rax
	cmovbq	%r13, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	cmovbq	%rcx, %r13
	leaq	-1(%r9), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%r15, %r14
	notq	%r14
	movq	%r11, %rax
	notq	%rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	%r9, %rax
	negq	%rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB13_2:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_4 Depth 2
                                        #       Child Loop BB13_9 Depth 3
                                        #       Child Loop BB13_6 Depth 3
                                        #       Child Loop BB13_12 Depth 3
                                        #       Child Loop BB13_15 Depth 3
                                        #       Child Loop BB13_18 Depth 3
	movq	112(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, %r14
	cmovaq	%r14, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	120(%rsp), %r12         # 8-byte Reload
	cmpq	%r12, %r14
	cmovaq	%r14, %r12
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpq	192(%rsp), %rax
	je	.LBB13_60
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB13_2 Depth=1
	notq	48(%rsp)                # 8-byte Folded Spill
	notq	%r12
	cmpq	%r11, %r15
	movq	%r15, %rax
	cmovaq	%r11, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r11, %r10
	cmovaq	%r15, %r10
	movq	96(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, %r15
	movq	%rax, %rcx
	cmovbq	%r15, %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%r15, %rcx
	cmovbq	%rax, %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movq	192(%rsp), %rdx
.LBB13_4:                               #   Parent Loop BB13_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_9 Depth 3
                                        #       Child Loop BB13_6 Depth 3
                                        #       Child Loop BB13_12 Depth 3
                                        #       Child Loop BB13_15 Depth 3
                                        #       Child Loop BB13_18 Depth 3
	movq	horzPlane(%rip), %rsi
	movq	channelColumns(%rip), %rax
	cmpq	%r8, %rdx
	jne	.LBB13_8
# BB#5:                                 #   in Loop: Header=BB13_4 Depth=2
	leaq	(%rsi,%r8), %rdi
	movq	%r15, %rcx
	imulq	%rax, %rcx
	addq	%rdi, %rcx
	movq	%r15, %rdi
	.p2align	4, 0x90
.LBB13_6:                               #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$0, (%rcx)
	jne	.LBB13_60
# BB#7:                                 #   in Loop: Header=BB13_6 Depth=3
	incq	%rdi
	addq	%rax, %rcx
	cmpq	%r15, %rdi
	jbe	.LBB13_6
	jmp	.LBB13_11
	.p2align	4, 0x90
.LBB13_8:                               #   in Loop: Header=BB13_4 Depth=2
	movq	%rdx, %rcx
	cmovaq	%r8, %rcx
	movq	%r8, %rbp
	cmovaq	%rdx, %rbp
	movq	%r15, %rdi
	imulq	%rax, %rdi
	addq	%rsi, %rdi
	.p2align	4, 0x90
.LBB13_9:                               #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$0, (%rdi,%rcx)
	jne	.LBB13_60
# BB#10:                                #   in Loop: Header=BB13_9 Depth=3
	incq	%rcx
	cmpq	%rbp, %rcx
	jbe	.LBB13_9
.LBB13_11:                              # %SegmentFree.exit
                                        #   in Loop: Header=BB13_4 Depth=2
	movq	vertPlane(%rip), %rcx
	movq	%rax, %rdi
	movq	32(%rsp), %rbx          # 8-byte Reload
	imulq	%rbx, %rdi
	leaq	(%rcx,%r8), %rbp
	addq	%rbp, %rdi
	.p2align	4, 0x90
.LBB13_12:                              #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$0, (%rdi)
	jne	.LBB13_59
# BB#13:                                #   in Loop: Header=BB13_12 Depth=3
	incq	%rbx
	addq	%rax, %rdi
	cmpq	%r13, %rbx
	jbe	.LBB13_12
# BB#14:                                # %SegmentFree.exit46
                                        #   in Loop: Header=BB13_4 Depth=2
	movq	%r12, %rdi
	imulq	%rax, %rdi
	addq	%rdi, %rbp
	movq	16(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB13_15:                              #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$0, (%rbp)
	jne	.LBB13_59
# BB#16:                                #   in Loop: Header=BB13_15 Depth=3
	incq	%rdi
	addq	%rax, %rbp
	cmpq	%r10, %rdi
	jbe	.LBB13_15
# BB#17:                                # %SegmentFree.exit54
                                        #   in Loop: Header=BB13_4 Depth=2
	movq	48(%rsp), %rdi          # 8-byte Reload
	imulq	%rax, %rdi
	addq	%rdx, %rdi
	addq	%rdi, %rcx
	movq	104(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB13_18:                              #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$0, (%rcx)
	jne	.LBB13_59
# BB#19:                                #   in Loop: Header=BB13_18 Depth=3
	incq	%rdi
	addq	%rax, %rcx
	cmpq	128(%rsp), %rdi         # 8-byte Folded Reload
	jbe	.LBB13_18
# BB#20:                                # %SegmentFree.exit50
                                        #   in Loop: Header=BB13_4 Depth=2
	movq	TOP(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.LBB13_24
# BB#21:                                #   in Loop: Header=BB13_4 Depth=2
	movq	BOT(%rip), %rcx
	movq	(%rcx,%rdx,8), %rcx
	testq	%rcx, %rcx
	je	.LBB13_24
# BB#22:                                #   in Loop: Header=BB13_4 Depth=2
	cmpq	%rcx, %rax
	je	.LBB13_24
# BB#23:                                # %HasVCV.exit
                                        #   in Loop: Header=BB13_4 Depth=2
	movq	netsAssign(%rip), %rdi
	movq	(%rdi,%rax,8), %rax
	cmpq	(%rdi,%rcx,8), %rax
	ja	.LBB13_59
.LBB13_24:                              # %HasVCV.exit.thread
                                        #   in Loop: Header=BB13_4 Depth=2
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%r9, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	%r9, %rbx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	%r11, %rbp
	movq	%r10, 8(%rsp)           # 8-byte Spill
	callq	ExtendOK
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	%rbp, %r11
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	%rbx, %r9
	testl	%eax, %eax
	jne	.LBB13_25
	.p2align	4, 0x90
.LBB13_59:                              # %SegmentFree.exit.thread
                                        #   in Loop: Header=BB13_4 Depth=2
	addq	80(%rsp), %rdx          # 8-byte Folded Reload
	cmpq	40(%rsp), %rdx          # 8-byte Folded Reload
	movq	88(%rsp), %rbx          # 8-byte Reload
	jne	.LBB13_4
	.p2align	4, 0x90
.LBB13_60:                              # %.critedge
                                        #   in Loop: Header=BB13_2 Depth=1
	incq	%r15
	decq	%r14
	cmpq	%rbx, %r15
	jne	.LBB13_2
# BB#61:
	xorl	%eax, %eax
.LBB13_62:                              # %.loopexit
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_25:
	movq	vertPlane(%rip), %rax
	movq	channelColumns(%rip), %rcx
	movq	32(%rsp), %rsi          # 8-byte Reload
	imulq	%rsi, %rcx
	addq	%r8, %rcx
	orb	$8, (%rax,%rcx)
	incq	%rsi
	movq	channelColumns(%rip), %rcx
	cmpq	%r13, %rsi
	jae	.LBB13_26
# BB#27:                                # %.lr.ph.i37.preheader
	movq	16(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB13_28:                              # %.lr.ph.i37
                                        # =>This Inner Loop Header: Depth=1
	imulq	%rsi, %rcx
	addq	%r8, %rcx
	orb	$12, (%rax,%rcx)
	incq	%rsi
	movq	channelColumns(%rip), %rcx
	cmpq	%r13, %rsi
	jb	.LBB13_28
	jmp	.LBB13_29
.LBB13_26:
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB13_29:                              # %DrawSegment.exit42
	imulq	%r13, %rcx
	addq	%r8, %rcx
	orb	$4, (%rax,%rcx)
	movq	viaPlane(%rip), %r10
	movq	56(%rsp), %rcx          # 8-byte Reload
	imulq	channelColumns(%rip), %rcx
	addq	%r8, %rcx
	movb	$1, (%r10,%rcx)
	movq	channelColumns(%rip), %rcx
	imulq	%rbx, %rcx
	addq	%r8, %rcx
	orb	$8, (%rax,%rcx)
	leaq	1(%rbx), %rcx
	movq	channelColumns(%rip), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	cmpq	%rdi, %rcx
	jae	.LBB13_35
# BB#30:                                # %.lr.ph.i28.preheader
	leal	1(%rdi), %ebp
	subl	%ebx, %ebp
	leaq	-2(%rdi), %rdi
	testb	$1, %bpl
	je	.LBB13_32
# BB#31:                                # %.lr.ph.i28.prol
	imulq	%rsi, %rcx
	addq	%r8, %rcx
	orb	$12, (%rax,%rcx)
	leaq	2(%rbx), %rcx
	movq	channelColumns(%rip), %rsi
.LBB13_32:                              # %.lr.ph.i28.prol.loopexit
	cmpq	%rbx, %rdi
	je	.LBB13_35
# BB#33:                                # %.lr.ph.i28.preheader.new
	cmpq	%r15, %r11
	cmovbeq	%r15, %r11
.LBB13_34:                              # %.lr.ph.i28
                                        # =>This Inner Loop Header: Depth=1
	imulq	%rcx, %rsi
	addq	%r8, %rsi
	orb	$12, (%rax,%rsi)
	leaq	1(%rcx), %rsi
	imulq	channelColumns(%rip), %rsi
	addq	%r8, %rsi
	orb	$12, (%rax,%rsi)
	addq	$2, %rcx
	movq	channelColumns(%rip), %rsi
	cmpq	%rcx, %r11
	jne	.LBB13_34
.LBB13_35:                              # %DrawSegment.exit33
	imulq	8(%rsp), %rsi           # 8-byte Folded Reload
	addq	%r8, %rsi
	orb	$4, (%rax,%rsi)
	movq	channelColumns(%rip), %rcx
	imulq	%r15, %rcx
	addq	%r8, %rcx
	movb	$1, (%r10,%rcx)
	movq	horzPlane(%rip), %rcx
	movq	channelColumns(%rip), %rsi
	imulq	%r15, %rsi
	cmpq	%r8, %rdx
	jne	.LBB13_41
# BB#36:
	addq	%r8, %rsi
	orb	$8, (%rcx,%rsi)
	cmpq	$-1, %r15
	je	.LBB13_38
# BB#37:
	movq	channelColumns(%rip), %rsi
	jmp	.LBB13_40
.LBB13_41:
	movq	%rdx, %rbp
	cmovaq	%r8, %rbp
	movq	%r8, %rdi
	cmovaq	%rdx, %rdi
	addq	%rbp, %rsi
	orb	$2, (%rcx,%rsi)
	jmp	.LBB13_43
.LBB13_42:                              # %.lr.ph69.i19
                                        #   in Loop: Header=BB13_43 Depth=1
	addq	%rcx, %rsi
	movb	$3, (%rbp,%rsi)
.LBB13_43:                              # %.lr.ph69.i19
                                        # =>This Inner Loop Header: Depth=1
	incq	%rbp
	movq	channelColumns(%rip), %rsi
	imulq	%r15, %rsi
	cmpq	%rdi, %rbp
	jb	.LBB13_42
# BB#44:                                # %._crit_edge70.i21
	addq	%rdi, %rsi
	movb	$1, %bl
	jmp	.LBB13_45
.LBB13_38:                              # %.lr.ph.i13.preheader.new
	orb	$12, (%rcx,%r8)
	movl	$2, %edi
	movq	channelColumns(%rip), %rsi
.LBB13_39:                              # %.lr.ph.i13
                                        # =>This Inner Loop Header: Depth=1
	leaq	-1(%rdi), %rbp
	imulq	%rsi, %rbp
	addq	%r8, %rbp
	orb	$12, (%rcx,%rbp)
	movq	channelColumns(%rip), %rsi
	imulq	%rdi, %rsi
	addq	%r8, %rsi
	orb	$12, (%rcx,%rsi)
	movq	channelColumns(%rip), %rsi
	addq	$2, %rdi
	jne	.LBB13_39
.LBB13_40:                              # %._crit_edge.i15
	imulq	%r15, %rsi
	addq	%r8, %rsi
	movb	$4, %bl
.LBB13_45:                              # %DrawSegment.exit24
	orb	%bl, (%rcx,%rsi)
	movq	channelColumns(%rip), %rsi
	imulq	%r15, %rsi
	addq	%rdx, %rsi
	movb	$1, (%r10,%rsi)
	cmpq	%r9, %r15
	movq	%r9, %rsi
	cmovbq	%r15, %rsi
	cmovbq	%r9, %r15
	movq	channelColumns(%rip), %rdi
	imulq	%rsi, %rdi
	addq	%rdx, %rdi
	orb	$8, (%rax,%rdi)
	jmp	.LBB13_47
	.p2align	4, 0x90
.LBB13_46:                              # %.lr.ph.i4
                                        #   in Loop: Header=BB13_47 Depth=1
	imulq	%rsi, %rdi
	addq	%rdx, %rdi
	orb	$12, (%rax,%rdi)
.LBB13_47:                              # %.lr.ph.i4
                                        # =>This Inner Loop Header: Depth=1
	incq	%rsi
	movq	channelColumns(%rip), %rdi
	cmpq	%r15, %rsi
	jb	.LBB13_46
# BB#48:                                # %DrawSegment.exit9
	imulq	%r15, %rdi
	addq	%rdx, %rdi
	orb	$4, (%rax,%rdi)
	movq	channelColumns(%rip), %rax
	imulq	%r9, %rax
	addq	%rdx, %rax
	movb	$1, (%r10,%rax)
	movq	channelColumns(%rip), %rax
	imulq	%r9, %rax
	cmpq	%r8, %rdx
	jne	.LBB13_54
# BB#49:
	addq	%r8, %rax
	orb	$8, (%rcx,%rax)
	cmpq	$-1, %r9
	je	.LBB13_51
# BB#50:
	movq	channelColumns(%rip), %rax
	jmp	.LBB13_53
.LBB13_54:
	movq	%r8, %rsi
	cmovbq	%rdx, %rsi
	cmovbq	%r8, %rdx
	addq	%rsi, %rax
	orb	$2, (%rcx,%rax)
	jmp	.LBB13_56
.LBB13_55:                              # %.lr.ph69.i
                                        #   in Loop: Header=BB13_56 Depth=1
	addq	%rcx, %rax
	movb	$3, (%rsi,%rax)
.LBB13_56:                              # %.lr.ph69.i
                                        # =>This Inner Loop Header: Depth=1
	incq	%rsi
	movq	channelColumns(%rip), %rax
	imulq	%r9, %rax
	cmpq	%rdx, %rsi
	jb	.LBB13_55
# BB#57:                                # %._crit_edge70.i
	addq	%rdx, %rax
	movb	$1, %dl
	jmp	.LBB13_58
.LBB13_51:                              # %.lr.ph.i.preheader.new
	orb	$12, (%rcx,%r8)
	movl	$2, %edx
	movq	channelColumns(%rip), %rax
.LBB13_52:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	-1(%rdx), %rsi
	imulq	%rax, %rsi
	addq	%r8, %rsi
	orb	$12, (%rcx,%rsi)
	movq	channelColumns(%rip), %rax
	imulq	%rdx, %rax
	addq	%r8, %rax
	orb	$12, (%rcx,%rax)
	movq	channelColumns(%rip), %rax
	addq	$2, %rdx
	jne	.LBB13_52
.LBB13_53:                              # %._crit_edge.i
	imulq	%r9, %rax
	addq	%r8, %rax
	movb	$4, %dl
.LBB13_58:                              # %DrawSegment.exit
	orb	%dl, (%rcx,%rax)
	movl	$1, %eax
	jmp	.LBB13_62
.Lfunc_end13:
	.size	Maze2Mech, .Lfunc_end13-Maze2Mech
	.cfi_endproc

	.globl	FindFreeHorzSeg
	.p2align	4, 0x90
	.type	FindFreeHorzSeg,@function
FindFreeHorzSeg:                        # @FindFreeHorzSeg
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB14_4
# BB#1:                                 # %.lr.ph19
	movq	channelColumns(%rip), %r8
	imulq	%rsi, %r8
	addq	horzPlane(%rip), %r8
	movq	%rdi, %rax
	.p2align	4, 0x90
.LBB14_2:                               # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%r8,%rax)
	jne	.LBB14_5
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	decq	%rax
	jne	.LBB14_2
.LBB14_4:
	xorl	%eax, %eax
.LBB14_5:                               # %._crit_edge20
	incq	%rax
	movq	%rax, (%rdx)
	movq	channelColumns(%rip), %rax
	cmpq	%rdi, %rax
	jb	.LBB14_9
# BB#6:                                 # %.lr.ph
	imulq	%rax, %rsi
	addq	horzPlane(%rip), %rsi
	.p2align	4, 0x90
.LBB14_7:                               # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rsi,%rdi)
	jne	.LBB14_9
# BB#8:                                 #   in Loop: Header=BB14_7 Depth=1
	incq	%rdi
	cmpq	%rax, %rdi
	jbe	.LBB14_7
.LBB14_9:                               # %._crit_edge
	decq	%rdi
	movq	%rdi, (%rcx)
	retq
.Lfunc_end14:
	.size	FindFreeHorzSeg, .Lfunc_end14-FindFreeHorzSeg
	.cfi_endproc

	.globl	Maze3
	.p2align	4, 0x90
	.type	Maze3,@function
Maze3:                                  # @Maze3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi113:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi114:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi116:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi117:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi119:
	.cfi_def_cfa_offset 352
.Lcfi120:
	.cfi_offset %rbx, -56
.Lcfi121:
	.cfi_offset %r12, -48
.Lcfi122:
	.cfi_offset %r13, -40
.Lcfi123:
	.cfi_offset %r14, -32
.Lcfi124:
	.cfi_offset %r15, -24
.Lcfi125:
	.cfi_offset %rbp, -16
	cmpq	$0, channelColumns(%rip)
	je	.LBB15_1
# BB#2:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	$-2, %r14
	movl	$1, %r8d
	.p2align	4, 0x90
.LBB15_3:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_6 Depth 2
                                        #       Child Loop BB15_9 Depth 3
                                        #       Child Loop BB15_14 Depth 3
                                        #       Child Loop BB15_21 Depth 3
                                        #         Child Loop BB15_24 Depth 4
                                        #         Child Loop BB15_29 Depth 4
                                        #         Child Loop BB15_34 Depth 4
                                        #           Child Loop BB15_36 Depth 5
                                        #             Child Loop BB15_41 Depth 6
                                        #             Child Loop BB15_44 Depth 6
                                        #             Child Loop BB15_47 Depth 6
                                        #             Child Loop BB15_55 Depth 6
                                        #             Child Loop BB15_58 Depth 6
                                        #             Child Loop BB15_61 Depth 6
                                        #     Child Loop BB15_75 Depth 2
                                        #     Child Loop BB15_79 Depth 2
                                        #     Child Loop BB15_81 Depth 2
                                        #     Child Loop BB15_85 Depth 2
                                        #     Child Loop BB15_89 Depth 2
                                        #     Child Loop BB15_95 Depth 2
                                        #     Child Loop BB15_99 Depth 2
                                        #     Child Loop BB15_101 Depth 2
                                        #     Child Loop BB15_104 Depth 2
                                        #     Child Loop BB15_107 Depth 2
	movq	mazeRoute(%rip), %rax
	cmpb	$0, (%rax,%r8)
	je	.LBB15_109
# BB#4:                                 #   in Loop: Header=BB15_3 Depth=1
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	netsAssign(%rip), %rax
	movq	TOP(%rip), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	%rcx, 264(%rsp)         # 8-byte Spill
	movq	(%rax,%rcx,8), %rdi
	movq	BOT(%rip), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	movq	(%rax,%rcx,8), %rsi
	leaq	1(%rsi), %rax
	leaq	-1(%rdi), %rcx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rax
	jae	.LBB15_18
# BB#5:                                 # %.lr.ph177.i
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	channelTracks(%rip), %rcx
	leaq	1(%rdi), %rdx
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	leaq	-1(%rsi), %rdx
	movq	%rdx, 192(%rsp)         # 8-byte Spill
	movq	$-2, %rbp
	subq	%rsi, %rbp
	movq	$-2, %rdx
	subq	%rdi, %rdx
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	movq	$-3, %rdx
	subq	%rsi, %rdx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movq	$-2, %rdx
	subq	%rcx, %rdx
	movq	%rdx, 224(%rsp)         # 8-byte Spill
	incq	%rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rsi, %rcx
	negq	%rcx
	movq	%rcx, 216(%rsp)         # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB15_6:                               #   Parent Loop BB15_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB15_9 Depth 3
                                        #       Child Loop BB15_14 Depth 3
                                        #       Child Loop BB15_21 Depth 3
                                        #         Child Loop BB15_24 Depth 4
                                        #         Child Loop BB15_29 Depth 4
                                        #         Child Loop BB15_34 Depth 4
                                        #           Child Loop BB15_36 Depth 5
                                        #             Child Loop BB15_41 Depth 6
                                        #             Child Loop BB15_44 Depth 6
                                        #             Child Loop BB15_47 Depth 6
                                        #             Child Loop BB15_55 Depth 6
                                        #             Child Loop BB15_58 Depth 6
                                        #             Child Loop BB15_61 Depth 6
	movq	%r14, %r9
	movq	%rax, %r14
	movq	168(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, %rbp
	cmovaq	%rbp, %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	testq	%r8, %r8
	movq	%rbp, 184(%rsp)         # 8-byte Spill
	je	.LBB15_7
# BB#8:                                 # %.lr.ph19.i.i
                                        #   in Loop: Header=BB15_6 Depth=2
	movq	channelColumns(%rip), %rcx
	movq	%rcx, %rdx
	imulq	%r14, %rdx
	addq	horzPlane(%rip), %rdx
	movq	%r8, %rax
	.p2align	4, 0x90
.LBB15_9:                               #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$0, (%rdx,%rax)
	jne	.LBB15_12
# BB#10:                                #   in Loop: Header=BB15_9 Depth=3
	decq	%rax
	jne	.LBB15_9
	jmp	.LBB15_11
	.p2align	4, 0x90
.LBB15_7:                               # %.._crit_edge20.i_crit_edge.i
                                        #   in Loop: Header=BB15_6 Depth=2
	movq	channelColumns(%rip), %rcx
.LBB15_11:                              #   in Loop: Header=BB15_6 Depth=2
	xorl	%eax, %eax
.LBB15_12:                              # %._crit_edge20.i.i
                                        #   in Loop: Header=BB15_6 Depth=2
	leaq	1(%rax), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	cmpq	%r8, %rcx
	movq	%r8, %rsi
	jb	.LBB15_16
# BB#13:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB15_6 Depth=2
	movq	%rcx, %rdx
	imulq	%r14, %rdx
	addq	horzPlane(%rip), %rdx
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB15_14:                              #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$0, (%rdx,%rsi)
	jne	.LBB15_16
# BB#15:                                #   in Loop: Header=BB15_14 Depth=3
	incq	%rsi
	cmpq	%rcx, %rsi
	jbe	.LBB15_14
.LBB15_16:                              # %FindFreeHorzSeg.exit.i
                                        #   in Loop: Header=BB15_6 Depth=2
	decq	%rsi
	cmpq	56(%rsp), %rsi          # 8-byte Folded Reload
	jbe	.LBB15_17
# BB#19:                                #   in Loop: Header=BB15_6 Depth=2
	addq	$2, %r9
	cmpq	%rdi, %r9
	jae	.LBB15_17
# BB#20:                                # %.lr.ph172.i
                                        #   in Loop: Header=BB15_6 Depth=2
	notq	144(%rsp)               # 8-byte Folded Spill
	movq	160(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, %r14
	movq	%rcx, %rdx
	cmovbq	%r14, %rdx
	movq	%rdx, 280(%rsp)         # 8-byte Spill
	movq	%r14, %rdx
	cmovbq	%rcx, %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	$-2, %rcx
	subq	%rax, %rcx
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	%rsi, 272(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB15_21:                              #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB15_24 Depth 4
                                        #         Child Loop BB15_29 Depth 4
                                        #         Child Loop BB15_34 Depth 4
                                        #           Child Loop BB15_36 Depth 5
                                        #             Child Loop BB15_41 Depth 6
                                        #             Child Loop BB15_44 Depth 6
                                        #             Child Loop BB15_47 Depth 6
                                        #             Child Loop BB15_55 Depth 6
                                        #             Child Loop BB15_58 Depth 6
                                        #             Child Loop BB15_61 Depth 6
	movq	216(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, %rcx
	cmovaq	%rcx, %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	224(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, %rcx
	movq	%rcx, 232(%rsp)         # 8-byte Spill
	cmovaq	%rcx, %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	testq	%r8, %r8
	je	.LBB15_22
# BB#23:                                # %.lr.ph19.i84.i
                                        #   in Loop: Header=BB15_21 Depth=3
	movq	channelColumns(%rip), %rcx
	movq	%rcx, %rdx
	imulq	%r9, %rdx
	addq	horzPlane(%rip), %rdx
	movq	%r8, %rax
	.p2align	4, 0x90
.LBB15_24:                              #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        #       Parent Loop BB15_21 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpb	$0, (%rdx,%rax)
	jne	.LBB15_27
# BB#25:                                #   in Loop: Header=BB15_24 Depth=4
	decq	%rax
	jne	.LBB15_24
	jmp	.LBB15_26
	.p2align	4, 0x90
.LBB15_22:                              # %.._crit_edge20.i87_crit_edge.i
                                        #   in Loop: Header=BB15_21 Depth=3
	movq	channelColumns(%rip), %rcx
.LBB15_26:                              #   in Loop: Header=BB15_21 Depth=3
	xorl	%eax, %eax
.LBB15_27:                              # %._crit_edge20.i87.i
                                        #   in Loop: Header=BB15_21 Depth=3
	cmpq	%r8, %rcx
	movq	%r8, %rbp
	jb	.LBB15_31
# BB#28:                                # %.lr.ph.i88.i
                                        #   in Loop: Header=BB15_21 Depth=3
	movq	%rcx, %rdx
	imulq	%r9, %rdx
	addq	horzPlane(%rip), %rdx
	movq	%r8, %rbp
	.p2align	4, 0x90
.LBB15_29:                              #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        #       Parent Loop BB15_21 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpb	$0, (%rdx,%rbp)
	jne	.LBB15_31
# BB#30:                                #   in Loop: Header=BB15_29 Depth=4
	incq	%rbp
	cmpq	%rcx, %rbp
	jbe	.LBB15_29
.LBB15_31:                              # %FindFreeHorzSeg.exit92.i
                                        #   in Loop: Header=BB15_21 Depth=3
	cmpq	%rsi, 56(%rsp)          # 8-byte Folded Reload
	ja	.LBB15_113
# BB#32:                                # %FindFreeHorzSeg.exit92.i
                                        #   in Loop: Header=BB15_21 Depth=3
	leaq	1(%rax), %rcx
	decq	%rbp
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rbp
	jbe	.LBB15_113
# BB#33:                                # %.preheader.lr.ph.i
                                        #   in Loop: Header=BB15_21 Depth=3
	notq	120(%rsp)               # 8-byte Folded Spill
	notq	128(%rsp)               # 8-byte Folded Spill
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, %r9
	movq	%r9, %rdx
	cmovaq	%rcx, %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%rcx, %r10
	cmovaq	%r9, %r10
	movq	192(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, %r9
	movq	%rcx, %rdx
	cmovbq	%r9, %rdx
	movq	%rdx, 240(%rsp)         # 8-byte Spill
	movq	%r9, %rdx
	cmovbq	%rcx, %rdx
	movq	%rdx, 288(%rsp)         # 8-byte Spill
	movq	$-2, %rcx
	subq	%rax, %rcx
	movq	%rcx, 256(%rsp)         # 8-byte Spill
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	%r9, 88(%rsp)           # 8-byte Spill
	movq	%r10, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB15_34:                              # %.preheader.i
                                        #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        #       Parent Loop BB15_21 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB15_36 Depth 5
                                        #             Child Loop BB15_41 Depth 6
                                        #             Child Loop BB15_44 Depth 6
                                        #             Child Loop BB15_47 Depth 6
                                        #             Child Loop BB15_55 Depth 6
                                        #             Child Loop BB15_58 Depth 6
                                        #             Child Loop BB15_61 Depth 6
	movq	%rax, 152(%rsp)         # 8-byte Spill
	cmpq	%rbp, 136(%rsp)         # 8-byte Folded Reload
	ja	.LBB15_112
# BB#35:                                # %.lr.ph.i
                                        #   in Loop: Header=BB15_34 Depth=4
	cmpq	%r8, %rcx
	movq	%rcx, %rax
	cmovaq	%r8, %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%r8, %r12
	cmovaq	%rcx, %r12
	movq	256(%rsp), %r13         # 8-byte Reload
	movq	136(%rsp), %rbx         # 8-byte Reload
	.p2align	4, 0x90
.LBB15_36:                              #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        #       Parent Loop BB15_21 Depth=3
                                        #         Parent Loop BB15_34 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB15_41 Depth 6
                                        #             Child Loop BB15_44 Depth 6
                                        #             Child Loop BB15_47 Depth 6
                                        #             Child Loop BB15_55 Depth 6
                                        #             Child Loop BB15_58 Depth 6
                                        #             Child Loop BB15_61 Depth 6
	movq	%rcx, %r15
	cmpq	%rbx, %rcx
	je	.LBB15_111
# BB#37:                                #   in Loop: Header=BB15_36 Depth=5
	cmpq	%r9, %r14
	je	.LBB15_111
# BB#38:                                #   in Loop: Header=BB15_36 Depth=5
	cmpq	%r8, %r15
	je	.LBB15_111
# BB#39:                                #   in Loop: Header=BB15_36 Depth=5
	cmpq	%r8, %rbx
	je	.LBB15_111
# BB#40:                                #   in Loop: Header=BB15_36 Depth=5
	movq	vertPlane(%rip), %rcx
	movq	channelColumns(%rip), %rax
	leaq	(%rcx,%r8), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB15_41:                              #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        #       Parent Loop BB15_21 Depth=3
                                        #         Parent Loop BB15_34 Depth=4
                                        #           Parent Loop BB15_36 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	cmpb	$0, (%rdx)
	jne	.LBB15_111
# BB#42:                                #   in Loop: Header=BB15_41 Depth=6
	incq	%rsi
	addq	%rax, %rdx
	cmpq	%r14, %rsi
	jbe	.LBB15_41
# BB#43:                                #   in Loop: Header=BB15_36 Depth=5
	movq	horzPlane(%rip), %rsi
	movq	%r14, %rdx
	imulq	%rax, %rdx
	addq	%rsi, %rdx
	movq	96(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB15_44:                              #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        #       Parent Loop BB15_21 Depth=3
                                        #         Parent Loop BB15_34 Depth=4
                                        #           Parent Loop BB15_36 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	cmpb	$0, (%rdx,%rdi)
	jne	.LBB15_111
# BB#45:                                #   in Loop: Header=BB15_44 Depth=6
	incq	%rdi
	cmpq	%r12, %rdi
	jbe	.LBB15_44
# BB#46:                                # %SegmentFree.exit116.i
                                        #   in Loop: Header=BB15_36 Depth=5
	addq	%r15, %rcx
	movq	144(%rsp), %rdx         # 8-byte Reload
	imulq	%rax, %rdx
	addq	%rcx, %rdx
	movq	280(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB15_47:                              #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        #       Parent Loop BB15_21 Depth=3
                                        #         Parent Loop BB15_34 Depth=4
                                        #           Parent Loop BB15_36 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	cmpb	$0, (%rdx)
	jne	.LBB15_111
# BB#48:                                #   in Loop: Header=BB15_47 Depth=6
	incq	%rcx
	addq	%rax, %rdx
	cmpq	(%rsp), %rcx            # 8-byte Folded Reload
	jbe	.LBB15_47
# BB#49:                                # %SegmentFree.exit110.i
                                        #   in Loop: Header=BB15_36 Depth=5
	movq	TOP(%rip), %rax
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.LBB15_53
# BB#50:                                #   in Loop: Header=BB15_36 Depth=5
	movq	BOT(%rip), %rcx
	movq	(%rcx,%r15,8), %rcx
	testq	%rcx, %rcx
	je	.LBB15_53
# BB#51:                                #   in Loop: Header=BB15_36 Depth=5
	cmpq	%rcx, %rax
	je	.LBB15_53
# BB#52:                                # %HasVCV.exit106.i
                                        #   in Loop: Header=BB15_36 Depth=5
	movq	netsAssign(%rip), %rdx
	movq	(%rdx,%rax,8), %rax
	cmpq	(%rdx,%rcx,8), %rax
	ja	.LBB15_111
.LBB15_53:                              # %HasVCV.exit106.thread.i
                                        #   in Loop: Header=BB15_36 Depth=5
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	%r15, %rdx
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %r9
	callq	ExtendOK
	movq	72(%rsp), %r10          # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	testl	%eax, %eax
	je	.LBB15_111
# BB#54:                                #   in Loop: Header=BB15_36 Depth=5
	movq	vertPlane(%rip), %rax
	movq	channelColumns(%rip), %rcx
	leaq	(%rax,%r8), %rsi
	movq	128(%rsp), %rdx         # 8-byte Reload
	imulq	%rcx, %rdx
	addq	%rsi, %rdx
	movq	64(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB15_55:                              #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        #       Parent Loop BB15_21 Depth=3
                                        #         Parent Loop BB15_34 Depth=4
                                        #           Parent Loop BB15_36 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	cmpb	$0, (%rdx)
	jne	.LBB15_111
# BB#56:                                #   in Loop: Header=BB15_55 Depth=6
	incq	%rsi
	addq	%rcx, %rdx
	cmpq	%r10, %rsi
	jbe	.LBB15_55
# BB#57:                                #   in Loop: Header=BB15_36 Depth=5
	movq	horzPlane(%rip), %rsi
	cmpq	%r8, %rbx
	movq	%rbx, %rdi
	cmovaq	%r8, %rdi
	movq	%r8, %r11
	cmovaq	%rbx, %r11
	movq	%r9, %rdx
	imulq	%rcx, %rdx
	addq	%rsi, %rdx
	movq	%rdi, 248(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB15_58:                              #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        #       Parent Loop BB15_21 Depth=3
                                        #         Parent Loop BB15_34 Depth=4
                                        #           Parent Loop BB15_36 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	cmpb	$0, (%rdx,%rdi)
	jne	.LBB15_111
# BB#59:                                #   in Loop: Header=BB15_58 Depth=6
	incq	%rdi
	cmpq	%r11, %rdi
	jbe	.LBB15_58
# BB#60:                                # %SegmentFree.exit96.i
                                        #   in Loop: Header=BB15_36 Depth=5
	movq	120(%rsp), %rdx         # 8-byte Reload
	imulq	%rcx, %rdx
	addq	%rbx, %rdx
	addq	%rdx, %rax
	movq	240(%rsp), %rdx         # 8-byte Reload
	.p2align	4, 0x90
.LBB15_61:                              #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        #       Parent Loop BB15_21 Depth=3
                                        #         Parent Loop BB15_34 Depth=4
                                        #           Parent Loop BB15_36 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	cmpb	$0, (%rax)
	jne	.LBB15_111
# BB#62:                                #   in Loop: Header=BB15_61 Depth=6
	incq	%rdx
	addq	%rcx, %rax
	cmpq	288(%rsp), %rdx         # 8-byte Folded Reload
	jbe	.LBB15_61
# BB#63:                                # %SegmentFree.exit.i
                                        #   in Loop: Header=BB15_36 Depth=5
	movq	TOP(%rip), %rax
	movq	(%rax,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB15_67
# BB#64:                                #   in Loop: Header=BB15_36 Depth=5
	movq	BOT(%rip), %rcx
	movq	(%rcx,%rbx,8), %rcx
	testq	%rcx, %rcx
	je	.LBB15_67
# BB#65:                                #   in Loop: Header=BB15_36 Depth=5
	cmpq	%rcx, %rax
	je	.LBB15_67
# BB#66:                                # %HasVCV.exit.i
                                        #   in Loop: Header=BB15_36 Depth=5
	movq	netsAssign(%rip), %rdx
	movq	(%rdx,%rax,8), %rax
	cmpq	(%rdx,%rcx,8), %rax
	ja	.LBB15_111
.LBB15_67:                              # %HasVCV.exit.thread.i
                                        #   in Loop: Header=BB15_36 Depth=5
	movq	208(%rsp), %rdi         # 8-byte Reload
	movq	%rbx, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %r9
	movq	%r11, 112(%rsp)         # 8-byte Spill
	callq	ExtendOK
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	testl	%eax, %eax
	jne	.LBB15_68
	.p2align	4, 0x90
.LBB15_111:                             # %SegmentFree.exit104.thread.i
                                        #   in Loop: Header=BB15_36 Depth=5
	incq	%rbx
	decq	%r13
	cmpq	%rbp, %rbx
	movq	%r15, %rcx
	jbe	.LBB15_36
.LBB15_112:                             # %._crit_edge.i
                                        #   in Loop: Header=BB15_34 Depth=4
	incq	%rcx
	movq	152(%rsp), %rax         # 8-byte Reload
	decq	%rax
	movq	272(%rsp), %rsi         # 8-byte Reload
	cmpq	%rsi, %rcx
	jbe	.LBB15_34
.LBB15_113:                             # %.loopexit.i
                                        #   in Loop: Header=BB15_21 Depth=3
	incq	%r9
	movq	232(%rsp), %rcx         # 8-byte Reload
	decq	%rcx
	movq	80(%rsp), %rdi          # 8-byte Reload
	cmpq	%rdi, %r9
	jb	.LBB15_21
	.p2align	4, 0x90
.LBB15_17:                              # %.backedge.i
                                        #   in Loop: Header=BB15_6 Depth=2
	leaq	1(%r14), %rax
	movq	184(%rsp), %rbp         # 8-byte Reload
	decq	%rbp
	decq	104(%rsp)               # 8-byte Folded Spill
	incq	32(%rsp)                # 8-byte Folded Spill
	cmpq	176(%rsp), %rax         # 8-byte Folded Reload
	jb	.LBB15_6
	.p2align	4, 0x90
.LBB15_18:                              # %.loopexit28
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	incl	%eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB15_109
	.p2align	4, 0x90
.LBB15_68:                              #   in Loop: Header=BB15_3 Depth=1
	movq	vertPlane(%rip), %rax
	orb	$8, (%rax,%r8)
	movq	channelColumns(%rip), %rcx
	cmpq	$2, %r14
	jb	.LBB15_69
# BB#70:                                # %.lr.ph.i78.i.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	testb	$1, %r14b
	movq	48(%rsp), %r11          # 8-byte Reload
	movq	96(%rsp), %r10          # 8-byte Reload
	jne	.LBB15_71
# BB#72:                                # %.lr.ph.i78.i.prol
                                        #   in Loop: Header=BB15_3 Depth=1
	addq	%r8, %rcx
	orb	$12, (%rax,%rcx)
	movq	channelColumns(%rip), %rcx
	movl	$2, %edx
	jmp	.LBB15_73
.LBB15_69:                              #   in Loop: Header=BB15_3 Depth=1
	movq	48(%rsp), %r11          # 8-byte Reload
	movq	96(%rsp), %r10          # 8-byte Reload
	jmp	.LBB15_76
.LBB15_71:                              #   in Loop: Header=BB15_3 Depth=1
	movl	$1, %edx
.LBB15_73:                              # %.lr.ph.i78.i.prol.loopexit
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	32(%rsp), %rsi          # 8-byte Reload
	cmpq	$2, %r14
	je	.LBB15_76
# BB#74:                                # %.lr.ph.i78.i.preheader.new
                                        #   in Loop: Header=BB15_3 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB15_75:                              # %.lr.ph.i78.i
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imulq	%rdx, %rcx
	addq	%r8, %rcx
	orb	$12, (%rax,%rcx)
	leaq	1(%rdx), %rcx
	imulq	channelColumns(%rip), %rcx
	addq	%r8, %rcx
	orb	$12, (%rax,%rcx)
	addq	$2, %rdx
	movq	channelColumns(%rip), %rcx
	cmpq	%rdx, %rsi
	jne	.LBB15_75
.LBB15_76:                              # %.loopexit222.i
                                        #   in Loop: Header=BB15_3 Depth=1
	imulq	%r14, %rcx
	addq	%r8, %rcx
	orb	$4, (%rax,%rcx)
	movq	viaPlane(%rip), %rdx
	movq	channelColumns(%rip), %rcx
	imulq	%r14, %rcx
	addq	%r8, %rcx
	movq	%rdx, (%rsp)            # 8-byte Spill
	movb	$1, (%rdx,%rcx)
	movq	horzPlane(%rip), %rcx
	movq	channelColumns(%rip), %rdx
	imulq	%r14, %rdx
	addq	%r10, %rdx
	orb	$2, (%rcx,%rdx)
	leaq	1(%r10), %rsi
	movq	channelColumns(%rip), %rdi
	imulq	%r14, %rdi
	cmpq	%r12, %rsi
	jae	.LBB15_82
# BB#77:                                # %.lr.ph69.i70.i.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	leal	3(%r12), %edx
	subl	%r10d, %edx
	leaq	-2(%r12), %r9
	subq	%r10, %r9
	testb	$3, %dl
	je	.LBB15_80
# BB#78:                                # %.lr.ph69.i70.i.prol.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	152(%rsp), %r10         # 8-byte Reload
	cmpq	%rdx, %r10
	cmovbel	%edx, %r10d
	movq	%r15, %rbp
	cmpq	%r8, %rbp
	movl	%r8d, %edx
	cmoval	%ebp, %edx
	leal	4(%r10,%rdx), %edx
	andl	$3, %edx
	negq	%rdx
	.p2align	4, 0x90
.LBB15_79:                              # %.lr.ph69.i70.i.prol
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%rcx, %rdi
	movb	$3, (%rsi,%rdi)
	incq	%rsi
	movq	channelColumns(%rip), %rdi
	imulq	%r14, %rdi
	incq	%rdx
	jne	.LBB15_79
.LBB15_80:                              # %.lr.ph69.i70.i.prol.loopexit
                                        #   in Loop: Header=BB15_3 Depth=1
	cmpq	$3, %r9
	movq	88(%rsp), %r9           # 8-byte Reload
	jb	.LBB15_82
	.p2align	4, 0x90
.LBB15_81:                              # %.lr.ph69.i70.i
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%rsi, %rdi
	movb	$3, (%rcx,%rdi)
	movq	channelColumns(%rip), %rdx
	imulq	%r14, %rdx
	addq	%rsi, %rdx
	movb	$3, 1(%rcx,%rdx)
	movq	channelColumns(%rip), %rdx
	imulq	%r14, %rdx
	addq	%rsi, %rdx
	movb	$3, 2(%rcx,%rdx)
	movq	channelColumns(%rip), %rdx
	imulq	%r14, %rdx
	addq	%rsi, %rdx
	movb	$3, 3(%rcx,%rdx)
	addq	$4, %rsi
	movq	channelColumns(%rip), %rdi
	imulq	%r14, %rdi
	cmpq	%r12, %rsi
	jne	.LBB15_81
.LBB15_82:                              # %DrawSegment.exit75.i
                                        #   in Loop: Header=BB15_3 Depth=1
	addq	%r12, %rdi
	orb	$1, (%rcx,%rdi)
	movq	channelColumns(%rip), %rdx
	imulq	%r14, %rdx
	movq	%r15, %rbp
	addq	%rbp, %rdx
	movq	(%rsp), %rsi            # 8-byte Reload
	movb	$1, (%rsi,%rdx)
	movq	80(%rsp), %rdi          # 8-byte Reload
	cmpq	%rdi, %r14
	movq	%rdi, %rdx
	cmovbq	%r14, %rdx
	cmovbq	%rdi, %r14
	movq	channelColumns(%rip), %rsi
	imulq	%rdx, %rsi
	addq	%rbp, %rsi
	orb	$8, (%rax,%rsi)
	incq	%rdx
	movq	channelColumns(%rip), %rsi
	cmpq	%r14, %rdx
	jae	.LBB15_83
# BB#84:                                # %.lr.ph.i55.i.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	64(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB15_85:                              # %.lr.ph.i55.i
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imulq	%rdx, %rsi
	addq	%r15, %rsi
	orb	$12, (%rax,%rsi)
	incq	%rdx
	movq	channelColumns(%rip), %rsi
	cmpq	%r14, %rdx
	jb	.LBB15_85
	jmp	.LBB15_86
.LBB15_83:                              #   in Loop: Header=BB15_3 Depth=1
	movq	64(%rsp), %r12          # 8-byte Reload
.LBB15_86:                              # %.loopexit221.i
                                        #   in Loop: Header=BB15_3 Depth=1
	imulq	%r14, %rsi
	addq	%r15, %rsi
	orb	$4, (%rax,%rsi)
	movq	channelColumns(%rip), %rdx
	imulq	%rdi, %rdx
	addq	%r15, %rdx
	movq	(%rsp), %rsi            # 8-byte Reload
	movb	$1, (%rsi,%rdx)
	movq	channelColumns(%rip), %rsi
	imulq	%rdi, %rsi
	cmpq	%r8, %r15
	movq	%r8, %rdx
	cmovbq	%r15, %rdx
	cmovbq	%r8, %r15
	addq	%rdx, %rsi
	orb	$2, (%rcx,%rsi)
	incq	%rdx
	movq	channelColumns(%rip), %rsi
	imulq	%rdi, %rsi
	movq	%r15, %rbp
	cmpq	%r15, %rdx
	jae	.LBB15_87
# BB#88:                                # %.lr.ph69.i46.i.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	16(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB15_89:                              # %.lr.ph69.i46.i
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%rcx, %rsi
	movb	$3, (%rdx,%rsi)
	incq	%rdx
	movq	channelColumns(%rip), %rsi
	imulq	%rdi, %rsi
	cmpq	%rbp, %rdx
	jb	.LBB15_89
	jmp	.LBB15_90
.LBB15_87:                              #   in Loop: Header=BB15_3 Depth=1
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB15_90:                              # %DrawSegment.exit51.i
                                        #   in Loop: Header=BB15_3 Depth=1
	addq	%rbp, %rsi
	orb	$1, (%rcx,%rsi)
	movq	channelColumns(%rip), %rdx
	imulq	%r12, %rdx
	addq	%r8, %rdx
	orb	$8, (%rax,%rdx)
	leaq	1(%r12), %rsi
	movq	channelColumns(%rip), %rdi
	movq	72(%rsp), %rdx          # 8-byte Reload
	cmpq	%rdx, %rsi
	jae	.LBB15_96
# BB#91:                                # %.lr.ph.i31.i.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	leal	1(%rdx), %ebp
	subl	%r12d, %ebp
	leaq	-2(%rdx), %rdx
	testb	$1, %bpl
	je	.LBB15_93
# BB#92:                                # %.lr.ph.i31.i.prol
                                        #   in Loop: Header=BB15_3 Depth=1
	imulq	%rdi, %rsi
	addq	%r8, %rsi
	orb	$12, (%rax,%rsi)
	leaq	2(%r12), %rsi
	movq	channelColumns(%rip), %rdi
.LBB15_93:                              # %.lr.ph.i31.i.prol.loopexit
                                        #   in Loop: Header=BB15_3 Depth=1
	movq	40(%rsp), %rbp          # 8-byte Reload
	cmpq	%r12, %rdx
	je	.LBB15_96
# BB#94:                                # %.lr.ph.i31.i.preheader.new
                                        #   in Loop: Header=BB15_3 Depth=1
	cmpq	%rbp, %r9
	cmovaq	%r9, %rbp
	.p2align	4, 0x90
.LBB15_95:                              # %.lr.ph.i31.i
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imulq	%rsi, %rdi
	addq	%r8, %rdi
	orb	$12, (%rax,%rdi)
	leaq	1(%rsi), %rdx
	imulq	channelColumns(%rip), %rdx
	addq	%r8, %rdx
	orb	$12, (%rax,%rdx)
	addq	$2, %rsi
	movq	channelColumns(%rip), %rdi
	cmpq	%rsi, %rbp
	jne	.LBB15_95
.LBB15_96:                              # %.loopexit220.i
                                        #   in Loop: Header=BB15_3 Depth=1
	imulq	72(%rsp), %rdi          # 8-byte Folded Reload
	addq	%r8, %rdi
	orb	$4, (%rax,%rdi)
	movq	channelColumns(%rip), %rdx
	imulq	%r9, %rdx
	addq	%r8, %rdx
	movq	(%rsp), %rsi            # 8-byte Reload
	movb	$1, (%rsi,%rdx)
	movq	channelColumns(%rip), %rdx
	imulq	%r9, %rdx
	movq	248(%rsp), %r12         # 8-byte Reload
	addq	%r12, %rdx
	orb	$2, (%rcx,%rdx)
	leaq	1(%r12), %rsi
	movq	channelColumns(%rip), %rdi
	imulq	%r9, %rdi
	movq	112(%rsp), %rbp         # 8-byte Reload
	cmpq	%rbp, %rsi
	jae	.LBB15_102
# BB#97:                                # %.lr.ph69.i22.i.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	leal	3(%rbp), %edx
	subl	%r12d, %edx
	leaq	-2(%rbp), %r15
	subq	%r12, %r15
	testb	$3, %dl
	je	.LBB15_100
# BB#98:                                # %.lr.ph69.i22.i.prol.preheader
                                        #   in Loop: Header=BB15_3 Depth=1
	cmpq	%r14, %r13
	cmovbel	%r14d, %r13d
	cmpq	%r8, %rbx
	movl	%r8d, %ebp
	cmoval	%ebx, %ebp
	leal	4(%r13,%rbp), %ebp
	andl	$3, %ebp
	negq	%rbp
	.p2align	4, 0x90
.LBB15_99:                              # %.lr.ph69.i22.i.prol
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%rcx, %rdi
	movb	$3, (%rsi,%rdi)
	incq	%rsi
	movq	channelColumns(%rip), %rdi
	imulq	%r9, %rdi
	incq	%rbp
	jne	.LBB15_99
.LBB15_100:                             # %.lr.ph69.i22.i.prol.loopexit
                                        #   in Loop: Header=BB15_3 Depth=1
	cmpq	$3, %r15
	movq	112(%rsp), %rbp         # 8-byte Reload
	jb	.LBB15_102
	.p2align	4, 0x90
.LBB15_101:                             # %.lr.ph69.i22.i
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%rsi, %rdi
	movb	$3, (%rcx,%rdi)
	movq	channelColumns(%rip), %rdx
	imulq	%r9, %rdx
	addq	%rsi, %rdx
	movb	$3, 1(%rcx,%rdx)
	movq	channelColumns(%rip), %rdx
	imulq	%r9, %rdx
	addq	%rsi, %rdx
	movb	$3, 2(%rcx,%rdx)
	movq	channelColumns(%rip), %rdx
	imulq	%r9, %rdx
	addq	%rsi, %rdx
	movb	$3, 3(%rcx,%rdx)
	addq	$4, %rsi
	movq	channelColumns(%rip), %rdi
	imulq	%r9, %rdi
	cmpq	%rbp, %rsi
	jne	.LBB15_101
.LBB15_102:                             # %DrawSegment.exit27.i
                                        #   in Loop: Header=BB15_3 Depth=1
	addq	%rbp, %rdi
	orb	$1, (%rcx,%rdi)
	movq	channelColumns(%rip), %rdx
	imulq	%r9, %rdx
	addq	%rbx, %rdx
	movq	(%rsp), %rsi            # 8-byte Reload
	movb	$1, (%rsi,%rdx)
	cmpq	%r11, %r9
	movq	%r11, %rdx
	cmovbq	%r9, %rdx
	cmovbq	%r11, %r9
	movq	channelColumns(%rip), %rsi
	imulq	%rdx, %rsi
	addq	%rbx, %rsi
	orb	$8, (%rax,%rsi)
	jmp	.LBB15_104
	.p2align	4, 0x90
.LBB15_103:                             # %.lr.ph.i7.i
                                        #   in Loop: Header=BB15_104 Depth=2
	imulq	%rdx, %rsi
	addq	%rbx, %rsi
	orb	$12, (%rax,%rsi)
.LBB15_104:                             # %.lr.ph.i7.i
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rdx
	movq	channelColumns(%rip), %rsi
	cmpq	%r9, %rdx
	jb	.LBB15_103
# BB#105:                               # %.loopexit219.i
                                        #   in Loop: Header=BB15_3 Depth=1
	imulq	%r9, %rsi
	addq	%rbx, %rsi
	orb	$4, (%rax,%rsi)
	movq	channelColumns(%rip), %rax
	imulq	%r11, %rax
	addq	%rbx, %rax
	movq	(%rsp), %rdx            # 8-byte Reload
	movb	$1, (%rdx,%rax)
	movq	channelColumns(%rip), %rdx
	imulq	%r11, %rdx
	cmpq	%r8, %rbx
	movq	%r8, %rax
	cmovbq	%rbx, %rax
	cmovbq	%r8, %rbx
	addq	%rax, %rdx
	orb	$2, (%rcx,%rdx)
	jmp	.LBB15_107
	.p2align	4, 0x90
.LBB15_106:                             # %.lr.ph69.i.i
                                        #   in Loop: Header=BB15_107 Depth=2
	addq	%rcx, %rdx
	movb	$3, (%rax,%rdx)
.LBB15_107:                             # %.lr.ph69.i.i
                                        #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rax
	movq	channelColumns(%rip), %rdx
	imulq	%r11, %rdx
	cmpq	%rbx, %rax
	jb	.LBB15_106
# BB#108:                               # %.loopexit
                                        #   in Loop: Header=BB15_3 Depth=1
	addq	%rbx, %rdx
	orb	$1, (%rcx,%rdx)
	movq	mazeRoute(%rip), %rax
	movb	$0, (%rax,%r8)
	movq	TOP(%rip), %rax
	movq	(%rax,%r8,8), %rdi
	callq	CleanNet
	movq	BOT(%rip), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rdi
	callq	CleanNet
	movq	24(%rsp), %r8           # 8-byte Reload
.LBB15_109:                             #   in Loop: Header=BB15_3 Depth=1
	incq	%r8
	decq	%r14
	cmpq	channelColumns(%rip), %r8
	jbe	.LBB15_3
	jmp	.LBB15_110
.LBB15_1:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB15_110:                             # %._crit_edge
	movq	8(%rsp), %rax           # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	Maze3, .Lfunc_end15-Maze3
	.cfi_endproc

	.type	horzPlane,@object       # @horzPlane
	.local	horzPlane
	.comm	horzPlane,8,8
	.type	vertPlane,@object       # @vertPlane
	.local	vertPlane
	.comm	vertPlane,8,8
	.type	viaPlane,@object        # @viaPlane
	.local	viaPlane
	.comm	viaPlane,8,8
	.type	mazeRoute,@object       # @mazeRoute
	.local	mazeRoute
	.comm	mazeRoute,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"unable to allocate plane allocation maps\n"
	.size	.L.str, 42

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"           "
	.size	.L.str.1, 12

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" %d "
	.size	.L.str.2, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%%%c%%"
	.size	.L.str.4, 7

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	" | "
	.size	.L.str.5, 4

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"   "
	.size	.L.str.6, 4

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Track %3d: "
	.size	.L.str.7, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
