	.text
	.file	"analyze.bc"
	.globl	ana_AnalyzeProblem
	.p2align	4, 0x90
	.type	ana_AnalyzeProblem,@function
ana_AnalyzeProblem:                     # @ana_AnalyzeProblem
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$0, ana_EQUATIONS(%rip)
	movb	$0, ana_PEQUATIONS(%rip)
	movb	$0, ana_NEQUATIONS(%rip)
	movb	$0, ana_FUNCTIONS(%rip)
	movb	$0, ana_FINDOMAIN(%rip)
	movb	$0, ana_NONTRIVDOMAIN(%rip)
	movl	$0, ana_MONADIC(%rip)
	movl	$0, ana_NONMONADIC(%rip)
	movl	$0, ana_PROP(%rip)
	movl	$0, ana_GROUND(%rip)
	movl	$0, ana_SORTRES(%rip)
	movl	$0, ana_USORTRES(%rip)
	movb	$0, ana_NONUNIT(%rip)
	movb	$1, ana_CONGROUND(%rip)
	movl	$0, ana_AXIOMCLAUSES(%rip)
	movl	$0, ana_CONCLAUSES(%rip)
	movl	$0, ana_NONHORNCLAUSES(%rip)
	movq	ana_FINITEMONADICPREDICATES(%rip), %rax
	testq	%rax, %rax
	je	.LBB0_2
	.p2align	4, 0x90
.LBB0_1:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB0_1
.LBB0_2:                                # %list_Delete.exit
	movq	$0, ana_FINITEMONADICPREDICATES(%rip)
	testq	%r15, %r15
	je	.LBB0_41
# BB#3:                                 # %.lr.ph
	movq	%r15, %rdi
	callq	clause_FiniteMonadicPredicates
	movq	%rax, ana_FINITEMONADICPREDICATES(%rip)
	movl	$ana_CONCLAUSES, %r12d
	.p2align	4, 0x90
.LBB0_4:                                # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %rbx
	movq	112(%r14), %rsi
	movq	%rbx, %rdi
	callq	clause_ComputeWeight
	movl	%eax, 4(%rbx)
	movl	48(%rbx), %eax
	testb	$8, %al
	movl	$ana_AXIOMCLAUSES, %eax
	cmovneq	%r12, %rax
	incl	(%rax)
	cmpl	$2, 72(%rbx)
	jl	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=1
	incl	ana_NONHORNCLAUSES(%rip)
.LBB0_6:                                #   in Loop: Header=BB0_4 Depth=1
	cmpb	$1, ana_CONGROUND(%rip)
	jne	.LBB0_10
# BB#7:                                 #   in Loop: Header=BB0_4 Depth=1
	testb	$8, 48(%rbx)
	je	.LBB0_10
# BB#8:                                 #   in Loop: Header=BB0_4 Depth=1
	cmpl	$0, 52(%rbx)
	je	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_4 Depth=1
	movb	$0, ana_CONGROUND(%rip)
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_4 Depth=1
	movzbl	ana_PEQUATIONS(%rip), %eax
	testb	%al, %al
	jne	.LBB0_13
# BB#11:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rbx, %rdi
	callq	clause_ContainsPositiveEquations
	testl	%eax, %eax
	je	.LBB0_13
# BB#12:                                #   in Loop: Header=BB0_4 Depth=1
	movb	$1, ana_PEQUATIONS(%rip)
.LBB0_13:                               #   in Loop: Header=BB0_4 Depth=1
	movzbl	ana_NEQUATIONS(%rip), %eax
	testb	%al, %al
	jne	.LBB0_16
# BB#14:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rbx, %rdi
	callq	clause_ContainsNegativeEquations
	testl	%eax, %eax
	je	.LBB0_16
# BB#15:                                #   in Loop: Header=BB0_4 Depth=1
	movb	$1, ana_NEQUATIONS(%rip)
.LBB0_16:                               #   in Loop: Header=BB0_4 Depth=1
	cmpl	$0, ana_MONADIC(%rip)
	je	.LBB0_20
# BB#17:                                #   in Loop: Header=BB0_4 Depth=1
	movl	ana_NONMONADIC(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_20
# BB#18:                                #   in Loop: Header=BB0_4 Depth=1
	movl	ana_PROP(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_4 Depth=1
	movl	ana_GROUND(%rip), %eax
	testl	%eax, %eax
	jne	.LBB0_21
.LBB0_20:                               #   in Loop: Header=BB0_4 Depth=1
	movl	$ana_PROP, %esi
	movl	$ana_GROUND, %edx
	movl	$ana_MONADIC, %ecx
	movl	$ana_NONMONADIC, %r8d
	movq	%rbx, %rdi
	callq	clause_ContainsFolAtom
.LBB0_21:                               #   in Loop: Header=BB0_4 Depth=1
	movzbl	ana_FUNCTIONS(%rip), %eax
	testb	%al, %al
	jne	.LBB0_24
# BB#22:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rbx, %rdi
	callq	clause_ContainsFunctions
	testl	%eax, %eax
	je	.LBB0_24
# BB#23:                                #   in Loop: Header=BB0_4 Depth=1
	movb	$1, ana_FUNCTIONS(%rip)
.LBB0_24:                               #   in Loop: Header=BB0_4 Depth=1
	movzbl	ana_FINDOMAIN(%rip), %eax
	testb	%al, %al
	jne	.LBB0_27
# BB#25:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rbx, %rdi
	callq	clause_ImpliesFiniteDomain
	testl	%eax, %eax
	je	.LBB0_27
# BB#26:                                #   in Loop: Header=BB0_4 Depth=1
	movb	$1, ana_FINDOMAIN(%rip)
.LBB0_27:                               #   in Loop: Header=BB0_4 Depth=1
	movzbl	ana_NONTRIVDOMAIN(%rip), %eax
	testb	%al, %al
	jne	.LBB0_30
# BB#28:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rbx, %rdi
	callq	clause_ImpliesNonTrivialDomain
	testl	%eax, %eax
	je	.LBB0_30
# BB#29:                                #   in Loop: Header=BB0_4 Depth=1
	movl	(%rbx), %eax
	movl	%eax, 156(%r14)
	movb	$1, ana_NONTRIVDOMAIN(%rip)
.LBB0_30:                               #   in Loop: Header=BB0_4 Depth=1
	movzbl	ana_NONUNIT(%rip), %eax
	testb	%al, %al
	jne	.LBB0_33
# BB#31:                                #   in Loop: Header=BB0_4 Depth=1
	movl	68(%rbx), %eax
	addl	64(%rbx), %eax
	addl	72(%rbx), %eax
	cmpl	$2, %eax
	jl	.LBB0_33
# BB#32:                                #   in Loop: Header=BB0_4 Depth=1
	movb	$1, ana_NONUNIT(%rip)
.LBB0_33:                               #   in Loop: Header=BB0_4 Depth=1
	cmpl	$0, ana_SORTRES(%rip)
	je	.LBB0_35
# BB#34:                                #   in Loop: Header=BB0_4 Depth=1
	movl	ana_USORTRES(%rip), %eax
	testl	%eax, %eax
	jne	.LBB0_36
.LBB0_35:                               #   in Loop: Header=BB0_4 Depth=1
	movl	$ana_SORTRES, %esi
	movl	$ana_USORTRES, %edx
	movq	%rbx, %rdi
	callq	clause_ContainsSortRestriction
.LBB0_36:                               #   in Loop: Header=BB0_4 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB0_4
# BB#37:                                # %._crit_edge
	movb	ana_NEQUATIONS(%rip), %al
	orb	ana_PEQUATIONS(%rip), %al
	movzbl	%al, %ecx
	movl	%ecx, %eax
	andl	$1, %eax
	movl	%eax, ana_EQUATIONS(%rip)
	xorl	%eax, %eax
	testb	$1, %cl
	jne	.LBB0_40
# BB#38:                                # %._crit_edge
	movl	ana_NONMONADIC(%rip), %ecx
	orl	ana_MONADIC(%rip), %ecx
	jne	.LBB0_40
# BB#39:
	cmpl	$0, ana_PROP(%rip)
	setne	%al
.LBB0_40:
	movzbl	%al, %eax
	movl	%eax, ana_PUREPROPOSITIONAL(%rip)
.LBB0_41:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	ana_AnalyzeProblem, .Lfunc_end0-ana_AnalyzeProblem
	.cfi_endproc

	.globl	ana_AnalyzeSortStructure
	.p2align	4, 0x90
	.type	ana_AnalyzeSortStructure,@function
ana_AnalyzeSortStructure:               # @ana_AnalyzeSortStructure
	.cfi_startproc
# BB#0:
	cmpb	$1, ana_PEQUATIONS(%rip)
	jne	.LBB1_3
# BB#1:
	movl	ana_SORTRES(%rip), %eax
	testl	%eax, %eax
	je	.LBB1_3
# BB#2:
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 16
	callq	sort_AnalyzeSortStructure
	xorl	%ecx, %ecx
	cmpl	$3, %eax
	sete	%cl
	movl	%ecx, ana_SORTMANYEQUATIONS(%rip)
	orl	$1, %eax
	xorl	%ecx, %ecx
	cmpl	$3, %eax
	sete	%cl
	movl	%ecx, ana_SORTDECEQUATIONS(%rip)
	addq	$8, %rsp
.LBB1_3:
	retq
.Lfunc_end1:
	.size	ana_AnalyzeSortStructure, .Lfunc_end1-ana_AnalyzeSortStructure
	.cfi_endproc

	.globl	ana_Print
	.p2align	4, 0x90
	.type	ana_Print,@function
ana_Print:                              # @ana_Print
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -40
.Lcfi16:
	.cfi_offset %r12, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	cmpl	$0, ana_NONHORNCLAUSES(%rip)
	movl	$.L.str, %eax
	movl	$.L.str.1, %r15d
	cmoveq	%rax, %r15
	movl	ana_MONADIC(%rip), %ecx
	testl	%ecx, %ecx
	movl	ana_NONMONADIC(%rip), %eax
	je	.LBB2_7
# BB#1:
	testl	%eax, %eax
	jne	.LBB2_7
# BB#2:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	printf
	movb	ana_PEQUATIONS(%rip), %al
	movq	stdout(%rip), %rcx
	testb	%al, %al
	jne	.LBB2_4
# BB#3:
	testb	$1, ana_NEQUATIONS(%rip)
	jne	.LBB2_4
# BB#6:
	movl	$.L.str.4, %edi
	movl	$18, %esi
	jmp	.LBB2_5
.LBB2_7:
	movb	ana_PEQUATIONS(%rip), %dl
	orl	%ecx, %eax
	testb	%dl, %dl
	jne	.LBB2_9
# BB#8:
	testb	$1, ana_NEQUATIONS(%rip)
	jne	.LBB2_9
# BB#14:
	testl	%eax, %eax
	je	.LBB2_17
# BB#15:
	movl	$.L.str.8, %edi
	jmp	.LBB2_16
.LBB2_4:
	movl	$.L.str.3, %edi
	movl	$15, %esi
.LBB2_5:
	movl	$1, %edx
	callq	fwrite
	cmpl	$0, ana_PUREPROPOSITIONAL(%rip)
	jne	.LBB2_18
	jmp	.LBB2_19
.LBB2_9:
	orl	ana_PROP(%rip), %eax
	je	.LBB2_11
# BB#10:
	movl	$.L.str.5, %edi
	jmp	.LBB2_16
.LBB2_11:
	cmpb	$1, ana_NONUNIT(%rip)
	jne	.LBB2_13
# BB#12:
	movl	$.L.str.6, %edi
.LBB2_16:
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	printf
.LBB2_17:
	cmpl	$0, ana_PUREPROPOSITIONAL(%rip)
	je	.LBB2_19
.LBB2_18:
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	printf
	cmpb	$1, ana_NONTRIVDOMAIN(%rip)
	je	.LBB2_26
	jmp	.LBB2_27
.LBB2_19:
	cmpb	$1, ana_FUNCTIONS(%rip)
	jne	.LBB2_21
# BB#20:
	movb	ana_FINDOMAIN(%rip), %al
	xorb	$1, %al
	testb	$1, %al
	jne	.LBB2_25
.LBB2_21:
	movq	stdout(%rip), %rcx
	movl	$.L.str.10, %edi
	movl	$60, %esi
	movl	$1, %edx
	callq	fwrite
	cmpb	$1, ana_FINDOMAIN(%rip)
	jne	.LBB2_23
# BB#22:
	movq	stdout(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
.LBB2_23:
	movb	ana_FUNCTIONS(%rip), %al
	testb	%al, %al
	jne	.LBB2_25
# BB#24:
	movq	stdout(%rip), %rcx
	movl	$.L.str.12, %edi
	movl	$32, %esi
	movl	$1, %edx
	callq	fwrite
.LBB2_25:
	cmpb	$1, ana_NONTRIVDOMAIN(%rip)
	jne	.LBB2_27
.LBB2_26:
	movq	stdout(%rip), %rcx
	movl	$.L.str.13, %edi
	movl	$65, %esi
	movl	$1, %edx
	callq	fwrite
.LBB2_27:
	cmpl	$0, ana_SORTRES(%rip)
	je	.LBB2_34
# BB#28:
	movq	stdout(%rip), %rcx
	movl	$.L.str.14, %edi
	movl	$51, %esi
	movl	$1, %edx
	callq	fwrite
	cmpb	$1, ana_PEQUATIONS(%rip)
	jne	.LBB2_34
# BB#29:
	cmpl	$0, ana_SORTMANYEQUATIONS(%rip)
	je	.LBB2_31
# BB#30:
	movq	stdout(%rip), %rcx
	movl	$.L.str.15, %edi
	movl	$32, %esi
	jmp	.LBB2_33
.LBB2_31:
	cmpl	$0, ana_SORTDECEQUATIONS(%rip)
	je	.LBB2_34
# BB#32:
	movq	stdout(%rip), %rcx
	movl	$.L.str.16, %edi
	movl	$36, %esi
.LBB2_33:
	movl	$1, %edx
	callq	fwrite
.LBB2_34:
	cmpl	$0, ana_CONCLAUSES(%rip)
	sete	%al
	cmpl	$0, ana_PUREPROPOSITIONAL(%rip)
	jne	.LBB2_37
# BB#35:
	movb	ana_CONGROUND(%rip), %cl
	xorb	$1, %cl
	orb	%cl, %al
	testb	$1, %al
	jne	.LBB2_37
# BB#36:
	movq	stdout(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$27, %esi
	movl	$1, %edx
	callq	fwrite
.LBB2_37:
	cmpq	$0, ana_FINITEMONADICPREDICATES(%rip)
	je	.LBB2_43
# BB#38:
	movq	stdout(%rip), %rcx
	movl	$.L.str.18, %edi
	movl	$59, %esi
	movl	$1, %edx
	callq	fwrite
	movq	ana_FINITEMONADICPREDICATES(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_40
	jmp	.LBB2_42
	.p2align	4, 0x90
.LBB2_41:                               # %.backedge
                                        #   in Loop: Header=BB2_40 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.19, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB2_42
.LBB2_40:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%rbx), %edi
	callq	symbol_Print
	cmpq	$0, (%rbx)
	jne	.LBB2_41
.LBB2_42:                               # %._crit_edge
	movq	stdout(%rip), %rsi
	movl	$46, %edi
	callq	_IO_putc
.LBB2_43:
	movl	ana_AXIOMCLAUSES(%rip), %esi
	movl	ana_CONCLAUSES(%rip), %edx
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	printf
	movq	stdout(%rip), %rcx
	movl	$.L.str.37, %edi
	movl	$14, %esi
	movl	$1, %edx
	callq	fwrite
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_44:                               # =>This Inner Loop Header: Depth=1
	movl	%ebx, %edi
	callq	flag_Type
	testl	%eax, %eax
	jne	.LBB2_47
# BB#45:                                #   in Loop: Header=BB2_44 Depth=1
	cmpl	$0, (%r12,%rbx,4)
	je	.LBB2_47
# BB#46:                                #   in Loop: Header=BB2_44 Depth=1
	movl	%ebx, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	(%r12,%rbx,4), %edx
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
.LBB2_47:                               #   in Loop: Header=BB2_44 Depth=1
	incq	%rbx
	cmpq	$96, %rbx
	jne	.LBB2_44
# BB#48:                                # %flag_PrintInferenceRules.exit
	movq	stdout(%rip), %rcx
	movl	$.L.str.39, %edi
	movl	$14, %esi
	movl	$1, %edx
	callq	fwrite
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_49:                               # =>This Inner Loop Header: Depth=1
	movl	%ebx, %edi
	callq	flag_Type
	cmpl	$2, %eax
	jne	.LBB2_52
# BB#50:                                #   in Loop: Header=BB2_49 Depth=1
	cmpl	$0, (%r12,%rbx,4)
	je	.LBB2_52
# BB#51:                                #   in Loop: Header=BB2_49 Depth=1
	movl	%ebx, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	(%r12,%rbx,4), %edx
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
.LBB2_52:                               #   in Loop: Header=BB2_49 Depth=1
	incq	%rbx
	cmpq	$96, %rbx
	jne	.LBB2_49
# BB#53:                                # %flag_PrintReductionRules.exit
	movq	stdout(%rip), %rcx
	movl	$.L.str.21, %edi
	movl	$14, %esi
	movl	$1, %edx
	callq	fwrite
	cmpl	$0, 164(%r12)
	movq	stdout(%rip), %rcx
	je	.LBB2_55
# BB#54:
	movl	$.L.str.22, %edi
	movl	$18, %esi
	jmp	.LBB2_56
.LBB2_55:
	movl	$.L.str.23, %edi
	movl	$21, %esi
.LBB2_56:
	movl	$1, %edx
	callq	fwrite
	movl	152(%r12), %eax
	testl	%eax, %eax
	je	.LBB2_57
# BB#58:
	movq	stdout(%rip), %rcx
	cmpl	$1, %eax
	jne	.LBB2_60
# BB#59:
	movl	$.L.str.25, %edi
	movl	$19, %esi
	jmp	.LBB2_61
.LBB2_57:
	movq	stdout(%rip), %rcx
	movl	$.L.str.24, %edi
	movl	$14, %esi
	jmp	.LBB2_61
.LBB2_60:
	movl	$.L.str.26, %edi
	movl	$18, %esi
.LBB2_61:
	movl	$1, %edx
	callq	fwrite
	movl	20(%r12), %esi
	testl	%esi, %esi
	je	.LBB2_65
# BB#62:
	cmpl	$-1, %esi
	jne	.LBB2_66
# BB#63:
	movq	stdout(%rip), %rcx
	movl	$.L.str.27, %edi
	movl	$16, %esi
	jmp	.LBB2_64
.LBB2_65:
	movq	stdout(%rip), %rcx
	movl	$.L.str.28, %edi
	movl	$14, %esi
.LBB2_64:
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB2_67
.LBB2_66:
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	printf
.LBB2_67:
	cmpl	$0, 176(%r12)
	movq	stdout(%rip), %rcx
	je	.LBB2_69
# BB#68:
	movl	$.L.str.30, %edi
	jmp	.LBB2_70
.LBB2_69:
	movl	$.L.str.31, %edi
.LBB2_70:
	movl	$16, %esi
	movl	$1, %edx
	callq	fwrite
	movl	168(%r12), %esi
	movl	180(%r12), %edx
	movl	184(%r12), %ecx
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	printf
	movq	stdout(%rip), %rcx
	movl	$.L.str.33, %edi
	movl	$14, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r14, %rdi
	callq	fol_PrintPrecedence
	movq	stdout(%rip), %rcx
	movl	$.L.str.34, %edi
	movl	$14, %esi
	movl	$1, %edx
	callq	fwrite
	cmpl	$0, 208(%r12)
	movl	$.L.str.35, %eax
	movl	$.L.str.36, %edi
	cmoveq	%rax, %rdi
	movq	stdout(%rip), %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	fputs                   # TAILCALL
.LBB2_13:
	movq	stdout(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$34, %esi
	jmp	.LBB2_5
.Lfunc_end2:
	.size	ana_Print, .Lfunc_end2-ana_Print
	.cfi_endproc

	.globl	ana_AutoConfiguration
	.p2align	4, 0x90
	.type	ana_AutoConfiguration,@function
ana_AutoConfiguration:                  # @ana_AutoConfiguration
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 144
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	%rdi, %rbp
	callq	symbol_GetAllFunctions
	movq	%rax, 64(%rsp)          # 8-byte Spill
	callq	fol_GetNonFOLPredicates
	movq	%rax, %rbx
	xorl	%r15d, %r15d
	testq	%rbx, %rbx
	je	.LBB3_24
# BB#1:                                 # %.lr.ph124.i
	callq	graph_Create
	movq	%rax, %r14
	movl	symbol_TYPESTATBITS(%rip), %r12d
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	subl	8(%rbx), %esi
	movl	%r12d, %ecx
	sarl	%cl, %esi
	movq	%r14, %rdi
	callq	graph_AddNode
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB3_2
# BB#3:                                 # %.preheader.i
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	testq	%rbp, %rbp
	je	.LBB3_21
# BB#4:
	movq	32(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph120.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_7 Depth 2
                                        #       Child Loop BB3_12 Depth 3
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	8(%rax), %r14
	movl	64(%r14), %eax
	movl	68(%r14), %edx
	movl	%edx, %ecx
	addl	%eax, %ecx
	jle	.LBB3_20
# BB#6:                                 # %.lr.ph117.i
                                        #   in Loop: Header=BB3_5 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_7:                                #   Parent Loop BB3_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_12 Depth 3
	movq	56(%r14), %rdi
	movq	(%rdi,%rbp,8), %rcx
	movq	24(%rcx), %rcx
	movl	(%rcx), %r15d
	movl	fol_NOT(%rip), %ebx
	cmpl	%r15d, %ebx
	jne	.LBB3_9
# BB#8:                                 #   in Loop: Header=BB3_7 Depth=2
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %r15d
.LBB3_9:                                # %clause_GetLiteralAtom.exit95.i
                                        #   in Loop: Header=BB3_7 Depth=2
	cmpl	fol_EQUALITY(%rip), %r15d
	je	.LBB3_19
# BB#10:                                #   in Loop: Header=BB3_7 Depth=2
	movl	72(%r14), %esi
	testl	%esi, %esi
	jle	.LBB3_19
# BB#11:                                # %.lr.ph112.i
                                        #   in Loop: Header=BB3_7 Depth=2
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	leal	(%rax,%rdx), %r8d
	movl	%r15d, %ebp
	negl	%ebp
	movl	%r12d, %ecx
	sarl	%cl, %ebp
	movl	%ebp, 28(%rsp)          # 4-byte Spill
	movslq	%r8d, %r12
	jmp	.LBB3_12
	.p2align	4, 0x90
.LBB3_144:                              # %._crit_edge130.i
                                        #   in Loop: Header=BB3_12 Depth=3
	movq	56(%r14), %rdi
	movl	fol_NOT(%rip), %ebx
.LBB3_12:                               #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incq	%r12
	movq	-8(%rdi,%r12,8), %rcx
	movq	24(%rcx), %rcx
	movl	(%rcx), %ebp
	cmpl	%ebp, %ebx
	jne	.LBB3_14
# BB#13:                                #   in Loop: Header=BB3_12 Depth=3
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %ebp
.LBB3_14:                               # %clause_GetLiteralAtom.exit.i
                                        #   in Loop: Header=BB3_12 Depth=3
	cmpl	%ebp, %r15d
	je	.LBB3_17
# BB#15:                                # %clause_GetLiteralAtom.exit.i
                                        #   in Loop: Header=BB3_12 Depth=3
	cmpl	fol_EQUALITY(%rip), %ebp
	je	.LBB3_17
# BB#16:                                #   in Loop: Header=BB3_12 Depth=3
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movl	28(%rsp), %esi          # 4-byte Reload
	callq	graph_GetNode
	movq	%rax, %r13
	negl	%ebp
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %ebp
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	graph_GetNode
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	graph_AddEdge
	movl	64(%r14), %eax
	movl	68(%r14), %edx
	movl	72(%r14), %esi
.LBB3_17:                               #   in Loop: Header=BB3_12 Depth=3
	leal	(%rdx,%rsi), %ecx
	addl	%eax, %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %r12
	jl	.LBB3_144
# BB#18:                                #   in Loop: Header=BB3_7 Depth=2
	movl	symbol_TYPESTATBITS(%rip), %r12d
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB3_19:                               # %.loopexit98.i
                                        #   in Loop: Header=BB3_7 Depth=2
	incq	%rbp
	leal	(%rax,%rdx), %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rbp
	jl	.LBB3_7
.LBB3_20:                               # %._crit_edge118.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB3_5
.LBB3_21:                               # %._crit_edge121.i
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	graph_StronglyConnectedComponents
	movl	%eax, %r14d
	xorl	%r15d, %r15d
	jmp	.LBB3_22
	.p2align	4, 0x90
.LBB3_39:                               #   in Loop: Header=BB3_22 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rbp
	jmp	.LBB3_43
	.p2align	4, 0x90
.LBB3_42:                               #   in Loop: Header=BB3_43 Depth=2
	movq	(%rbp), %rbp
.LBB3_43:                               #   Parent Loop BB3_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rbp, %rbp
	je	.LBB3_22
# BB#40:                                # %.lr.ph.i
                                        #   in Loop: Header=BB3_43 Depth=2
	movq	8(%rbp), %rax
	cmpl	%r14d, 8(%rax)
	jne	.LBB3_42
# BB#41:                                #   in Loop: Header=BB3_43 Depth=2
	movq	symbol_SIGNATURE(%rip), %rcx
	movslq	(%rax), %rax
	movq	(%rcx,%rax,8), %rax
	movslq	24(%rax), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
	jmp	.LBB3_42
	.p2align	4, 0x90
.LBB3_22:                               # %._crit_edge121.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_43 Depth 2
	decl	%r14d
	jns	.LBB3_39
# BB#23:                                # %._crit_edge.i
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	graph_Delete
	movq	32(%rsp), %rbp          # 8-byte Reload
.LBB3_24:                               # %ana_CalculatePredicatePrecedence.exit
	movq	64(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	movq	%r15, 72(%rsp)          # 8-byte Spill
	je	.LBB3_25
# BB#44:
	movb	ana_PEQUATIONS(%rip), %al
	testb	%al, %al
	je	.LBB3_45
# BB#52:                                # %.lr.ph270.i
	callq	graph_Create
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	%rax, %r12
	movl	symbol_TYPESTATBITS(%rip), %r14d
	.p2align	4, 0x90
.LBB3_53:                               # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	subl	8(%rcx), %esi
	movq	%rcx, %rbx
	movl	%r14d, %ecx
	sarl	%cl, %esi
	movq	%r12, %rdi
	callq	graph_AddNode
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.LBB3_53
# BB#54:                                # %._crit_edge271.i
	movl	$ana_NodeGreater, %esi
	movq	%r12, %rdi
	callq	graph_SortNodes
	testq	%rbp, %rbp
	movq	%r12, 8(%rsp)           # 8-byte Spill
	je	.LBB3_55
# BB#56:                                # %.lr.ph265.i.preheader
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_57:                               # %.lr.ph265.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_59 Depth 2
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	8(%rbp), %r15
	movl	68(%r15), %r14d
	movl	72(%r15), %eax
	addl	64(%r15), %r14d
	leal	-1(%rax,%r14), %eax
	cmpl	%eax, %r14d
	jbe	.LBB3_59
	jmp	.LBB3_75
	.p2align	4, 0x90
.LBB3_74:                               #   in Loop: Header=BB3_59 Depth=2
	incl	%r14d
	movl	64(%r15), %eax
	movl	72(%r15), %ecx
	addl	68(%r15), %eax
	leal	-1(%rcx,%rax), %eax
	cmpl	%eax, %r14d
	ja	.LBB3_75
.LBB3_59:                               #   Parent Loop BB3_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r15), %rax
	movslq	%r14d, %rbx
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	(%rdi), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	movl	%eax, %edx
	jne	.LBB3_61
# BB#60:                                #   in Loop: Header=BB3_59 Depth=2
	movq	16(%rdi), %rdx
	movq	8(%rdx), %rdx
	movl	(%rdx), %edx
.LBB3_61:                               # %clause_LiteralIsEquality.exit.i
                                        #   in Loop: Header=BB3_59 Depth=2
	cmpl	%edx, fol_EQUALITY(%rip)
	jne	.LBB3_74
# BB#62:                                #   in Loop: Header=BB3_59 Depth=2
	cmpl	%eax, %ecx
	jne	.LBB3_64
# BB#63:                                #   in Loop: Header=BB3_59 Depth=2
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB3_64:                               # %clause_GetLiteralAtom.exit208.i
                                        #   in Loop: Header=BB3_59 Depth=2
	leaq	52(%rsp), %rsi
	leaq	48(%rsp), %rdx
	callq	fol_DistributiveEquation
	testl	%eax, %eax
	je	.LBB3_66
# BB#65:                                #   in Loop: Header=BB3_59 Depth=2
	movslq	52(%rsp), %r12
	movslq	48(%rsp), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r12, 8(%rbp)
	movq	%r13, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB3_66:                               #   in Loop: Header=BB3_59 Depth=2
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rcx
	movl	fol_NOT(%rip), %edx
	movq	16(%rcx), %rax
	movq	8(%rax), %rbp
	cmpl	(%rcx), %edx
	jne	.LBB3_68
# BB#67:                                #   in Loop: Header=BB3_59 Depth=2
	movq	16(%rbp), %rax
	movq	8(%rax), %rbp
.LBB3_68:                               # %clause_GetLiteralAtom.exit.i91
                                        #   in Loop: Header=BB3_59 Depth=2
	movl	(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.LBB3_74
# BB#69:                                #   in Loop: Header=BB3_59 Depth=2
	movq	(%rax), %rax
	movq	8(%rax), %r12
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.LBB3_74
# BB#70:                                #   in Loop: Header=BB3_59 Depth=2
	cmpl	%eax, %ecx
	je	.LBB3_74
# BB#71:                                #   in Loop: Header=BB3_59 Depth=2
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	term_HasSubterm
	testl	%eax, %eax
	jne	.LBB3_74
# BB#72:                                #   in Loop: Header=BB3_59 Depth=2
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	term_HasSubterm
	testl	%eax, %eax
	jne	.LBB3_74
# BB#73:                                #   in Loop: Header=BB3_59 Depth=2
	movl	(%rbp), %esi
	negl	%esi
	movl	symbol_TYPESTATBITS(%rip), %r13d
	movl	%r13d, %ecx
	sarl	%cl, %esi
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdi
	callq	graph_GetNode
	movq	%rax, %rbx
	movl	(%r12), %esi
	negl	%esi
	movl	%r13d, %ecx
	sarl	%cl, %esi
	movq	%rbp, %rdi
	callq	graph_GetNode
	movq	%rax, %rbp
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	graph_AddEdge
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	graph_AddEdge
	movl	16(%rbx), %eax
	incl	%eax
	movq	%rax, 16(%rbx)
	movl	16(%rbp), %eax
	incl	%eax
	movq	%rax, 16(%rbp)
	jmp	.LBB3_74
	.p2align	4, 0x90
.LBB3_75:                               # %._crit_edge260.i
                                        #   in Loop: Header=BB3_57 Depth=1
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB3_57
	jmp	.LBB3_76
.LBB3_45:
	movl	$symbol_PositiveArity, %esi
	callq	list_NumberSort
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB3_47
	jmp	.LBB3_25
.LBB3_55:
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB3_76:                               # %._crit_edge266.i
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	%r14, %rdi
	callq	graph_DeleteDuplicateEdges
	movq	8(%r14), %rbp
	testq	%rbp, %rbp
	jne	.LBB3_78
	jmp	.LBB3_92
	.p2align	4, 0x90
.LBB3_91:                               # %._crit_edge247.i
                                        #   in Loop: Header=BB3_78 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB3_92
.LBB3_78:                               # %.lr.ph251.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_80 Depth 2
                                        #     Child Loop BB3_90 Depth 2
	movq	8(%rbp), %rbx
	movq	24(%rbx), %r15
	testq	%r15, %r15
	je	.LBB3_91
# BB#79:                                # %.lr.ph243.i
                                        #   in Loop: Header=BB3_78 Depth=1
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_80:                               #   Parent Loop BB3_78 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r15), %rbp
	movl	16(%rbx), %r14d
	movl	16(%rbp), %r13d
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	(%rbx), %rcx
	movq	(%rax,%rcx,8), %rcx
	xorl	%edx, %edx
	subl	24(%rcx), %edx
	movl	symbol_TYPESTATBITS(%rip), %ecx
	sarl	%cl, %edx
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rdx
	movl	16(%rdx), %edi
	movslq	(%rbp), %rdx
	movq	(%rax,%rdx,8), %rsi
	xorl	%edx, %edx
	subl	24(%rsi), %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edx
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %rax
	movl	16(%rax), %eax
	cmpl	%r13d, %r14d
	movl	%eax, 28(%rsp)          # 4-byte Spill
	ja	.LBB3_83
# BB#81:                                #   in Loop: Header=BB3_80 Depth=2
	jne	.LBB3_84
# BB#82:                                #   in Loop: Header=BB3_80 Depth=2
	cmpl	%eax, %edi
	jl	.LBB3_84
.LBB3_83:                               #   in Loop: Header=BB3_80 Depth=2
	movl	%edi, %ebx
	movl	$16, %edi
	callq	memory_Malloc
	movl	%ebx, %edi
	movq	%rbp, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, %r12
.LBB3_84:                               #   in Loop: Header=BB3_80 Depth=2
	cmpl	%r13d, %r14d
	movq	40(%rsp), %rbx          # 8-byte Reload
	jb	.LBB3_87
# BB#85:                                #   in Loop: Header=BB3_80 Depth=2
	jne	.LBB3_88
# BB#86:                                #   in Loop: Header=BB3_80 Depth=2
	cmpl	28(%rsp), %edi          # 4-byte Folded Reload
	jg	.LBB3_88
.LBB3_87:                               #   in Loop: Header=BB3_80 Depth=2
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	graph_DeleteEdge
.LBB3_88:                               #   in Loop: Header=BB3_80 Depth=2
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB3_80
# BB#89:                                # %.preheader225.i
                                        #   in Loop: Header=BB3_78 Depth=1
	testq	%r12, %r12
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB3_91
	.p2align	4, 0x90
.LBB3_90:                               # %.lr.ph246.i
                                        #   Parent Loop BB3_78 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	callq	graph_DeleteEdge
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB3_90
	jmp	.LBB3_91
.LBB3_92:                               # %._crit_edge252.i
	movq	16(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	movq	%r14, %r15
	je	.LBB3_103
# BB#93:                                # %.lr.ph33.i.preheader.i
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	movq	56(%rsp), %rdi          # 8-byte Reload
	je	.LBB3_100
# BB#94:
	movq	16(%rsp), %rax          # 8-byte Reload
.LBB3_96:                               # %.lr.ph.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_97 Depth 2
	movq	8(%rax), %rcx
	movq	%rdx, %rax
	movq	8(%rcx), %rdx
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB3_97:                               #   Parent Loop BB3_96 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rsi), %rbx
	cmpl	(%rbx), %edx
	jne	.LBB3_98
# BB#99:                                #   in Loop: Header=BB3_97 Depth=2
	movl	(%rcx), %ebp
	cmpl	8(%rbx), %ebp
	je	.LBB3_101
.LBB3_98:                               # %.backedge.i.i
                                        #   in Loop: Header=BB3_97 Depth=2
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB3_97
# BB#95:                                # %.lr.ph33.i.loopexit.i
                                        #   in Loop: Header=BB3_96 Depth=1
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_96
.LBB3_100:                              # %.lr.ph33.i._crit_edge.i
	movl	$52, %esi
	movl	$1, %edx
	callq	flag_SetFlagValue
.LBB3_101:                              # %ana_BidirectionalDistributivity.exit.i.preheader
	movq	8(%rsp), %r15           # 8-byte Reload
	movl	symbol_TYPESTATBITS(%rip), %r12d
	movq	16(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_102:                              # %ana_BidirectionalDistributivity.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %r14
	xorl	%esi, %esi
	subl	8(%r14), %esi
	movl	%r12d, %ecx
	sarl	%cl, %esi
	movq	%r15, %rdi
	movq	%rax, %r13
	callq	graph_GetNode
	movq	%rax, %rbx
	xorl	%esi, %esi
	subl	(%r14), %esi
	movl	%r12d, %ecx
	sarl	%cl, %esi
	movq	%r15, %rdi
	callq	graph_GetNode
	movq	%rax, %rbp
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	graph_DeleteEdge
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	graph_DeleteEdge
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	graph_AddEdge
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r14, (%rax)
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	jne	.LBB3_102
.LBB3_103:                              # %ana_BidirectionalDistributivity.exit._crit_edge.i
	movq	%r15, %rdi
	callq	graph_StronglyConnectedComponents
	movl	%eax, %r14d
	xorl	%r15d, %r15d
	jmp	.LBB3_104
	.p2align	4, 0x90
.LBB3_106:                              # %.preheader.i96
                                        #   in Loop: Header=BB3_104 Depth=1
	decl	%r14d
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	8(%rax), %rbx
	jmp	.LBB3_110
	.p2align	4, 0x90
.LBB3_109:                              #   in Loop: Header=BB3_110 Depth=2
	movq	(%rbx), %rbx
.LBB3_110:                              #   Parent Loop BB3_104 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rbx, %rbx
	je	.LBB3_104
# BB#107:                               # %.lr.ph.i97
                                        #   in Loop: Header=BB3_110 Depth=2
	movq	8(%rbx), %rax
	cmpl	%r14d, 8(%rax)
	jne	.LBB3_109
# BB#108:                               #   in Loop: Header=BB3_110 Depth=2
	movq	symbol_SIGNATURE(%rip), %rcx
	movslq	(%rax), %rax
	movq	(%rcx,%rax,8), %rax
	movslq	24(%rax), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
	jmp	.LBB3_109
	.p2align	4, 0x90
.LBB3_104:                              # %ana_BidirectionalDistributivity.exit._crit_edge.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_110 Depth 2
	testl	%r14d, %r14d
	jne	.LBB3_106
# BB#105:                               # %._crit_edge.i98
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	graph_Delete
	testq	%r15, %r15
	je	.LBB3_25
.LBB3_47:                               # %.lr.ph116
	movl	symbol_TYPEMASK(%rip), %r12d
	xorl	%r14d, %r14d
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB3_48:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rbp
	testl	%ebp, %ebp
	jns	.LBB3_51
# BB#49:                                # %symbol_IsConstant.exit
                                        #   in Loop: Header=BB3_48 Depth=1
	movl	%ebp, %eax
	negl	%eax
	testl	%eax, %r12d
	jne	.LBB3_51
# BB#50:                                #   in Loop: Header=BB3_48 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, %r14
	.p2align	4, 0x90
.LBB3_51:                               # %symbol_IsConstant.exit.thread
                                        #   in Loop: Header=BB3_48 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_48
	jmp	.LBB3_26
.LBB3_25:                               # %ana_CalculateFunctionPrecedence.exit.thread
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
.LBB3_26:                               # %._crit_edge117
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	list_NPointerDifference
	movq	%rax, %rbp
	movq	%r14, %rdi
	callq	list_NReverse
	movq	%rax, %r15
	testq	%rbp, %rbp
	movq	80(%rsp), %r12          # 8-byte Reload
	je	.LBB3_29
# BB#27:                                # %.lr.ph113
	movl	symbol_TYPESTATBITS(%rip), %r14d
	.p2align	4, 0x90
.LBB3_28:                               # =>This Inner Loop Header: Depth=1
	xorl	%ebx, %ebx
	subl	8(%rbp), %ebx
	callq	symbol_GetIncreasedOrderingCounter
	movl	%r14d, %ecx
	sarl	%cl, %ebx
	movslq	%ebx, %rcx
	movl	%eax, (%r12,%rcx,4)
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB3_28
.LBB3_29:                               # %.preheader107
	movq	72(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB3_32
# BB#30:                                # %.lr.ph110
	movl	symbol_TYPESTATBITS(%rip), %r14d
	.p2align	4, 0x90
.LBB3_31:                               # =>This Inner Loop Header: Depth=1
	xorl	%ebx, %ebx
	subl	8(%rbp), %ebx
	callq	symbol_GetIncreasedOrderingCounter
	movl	%r14d, %ecx
	sarl	%cl, %ebx
	movslq	%ebx, %rcx
	movl	%eax, (%r12,%rcx,4)
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB3_31
.LBB3_32:                               # %.preheader
	testq	%r15, %r15
	je	.LBB3_35
# BB#33:                                # %.lr.ph
	movl	symbol_TYPESTATBITS(%rip), %r14d
	.p2align	4, 0x90
.LBB3_34:                               # =>This Inner Loop Header: Depth=1
	xorl	%ebx, %ebx
	subl	8(%r15), %ebx
	callq	symbol_GetIncreasedOrderingCounter
	movl	%r14d, %ecx
	sarl	%cl, %ebx
	movslq	%ebx, %rcx
	movl	%eax, (%r12,%rcx,4)
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB3_34
.LBB3_35:                               # %._crit_edge
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	flag_ClearInferenceRules
	movq	%rbx, %rdi
	callq	flag_ClearReductionRules
	movl	$85, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	$88, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	$91, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	$92, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	$83, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	$84, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	$86, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	$44, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	$42, %esi
	movl	$5, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	cmpb	$1, ana_NEQUATIONS(%rip)
	jne	.LBB3_113
# BB#36:
	movl	$61, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	cmpb	$1, ana_NONUNIT(%rip)
	jne	.LBB3_113
# BB#37:
	cmpb	$1, ana_NONTRIVDOMAIN(%rip)
	jne	.LBB3_111
# BB#38:
	movl	$93, %esi
	movl	$2, %edx
	jmp	.LBB3_112
.LBB3_111:
	movl	$93, %esi
	movl	$1, %edx
.LBB3_112:
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
.LBB3_113:
	cmpb	$1, ana_PEQUATIONS(%rip)
	jne	.LBB3_119
# BB#114:
	movl	$65, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	$68, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	cmpl	$0, ana_NONHORNCLAUSES(%rip)
	je	.LBB3_116
# BB#115:
	movl	$63, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
.LBB3_116:
	cmpb	$1, ana_NONUNIT(%rip)
	jne	.LBB3_118
# BB#117:
	movl	$94, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
.LBB3_118:
	movl	$79, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	$80, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	$81, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	$82, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
.LBB3_119:
	cmpl	$0, ana_SORTRES(%rip)
	je	.LBB3_123
# BB#120:
	movl	$40, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	$59, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	$60, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	$90, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	cmpl	$0, ana_SORTMANYEQUATIONS(%rip)
	jne	.LBB3_122
# BB#121:
	movb	ana_PEQUATIONS(%rip), %al
	xorb	$1, %al
	testb	$1, %al
	je	.LBB3_125
.LBB3_122:
	movl	$89, %esi
	movl	$1, %edx
	jmp	.LBB3_124
.LBB3_123:
	movl	$40, %esi
	xorl	%edx, %edx
.LBB3_124:
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
.LBB3_125:
	movl	ana_NONMONADIC(%rip), %eax
	orl	ana_MONADIC(%rip), %eax
	je	.LBB3_130
# BB#126:
	movl	$69, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	cmpl	$0, ana_NONHORNCLAUSES(%rip)
	je	.LBB3_128
# BB#127:
	movl	$74, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
.LBB3_128:
	cmpb	$1, ana_NONUNIT(%rip)
	jne	.LBB3_130
# BB#129:
	movl	$94, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
.LBB3_130:
	movb	ana_FUNCTIONS(%rip), %al
	testb	%al, %al
	je	.LBB3_131
# BB#132:
	cmpb	$1, ana_NONUNIT(%rip)
	jne	.LBB3_134
# BB#133:
	movl	$38, %esi
	movl	$1, %edx
	jmp	.LBB3_135
.LBB3_131:
	movl	$38, %esi
	movl	$2, %edx
	jmp	.LBB3_135
.LBB3_134:
	movl	$38, %esi
	xorl	%edx, %edx
.LBB3_135:
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	ana_CONCLAUSES(%rip), %eax
	cmpl	ana_AXIOMCLAUSES(%rip), %eax
	jb	.LBB3_138
# BB#136:
	cmpl	$0, ana_PUREPROPOSITIONAL(%rip)
	jne	.LBB3_139
# BB#137:
	movb	ana_CONGROUND(%rip), %al
	xorb	$1, %al
	testb	$1, %al
	jne	.LBB3_139
.LBB3_138:
	movl	$41, %esi
	movl	$1, %edx
	jmp	.LBB3_140
.LBB3_139:
	movl	$41, %esi
	xorl	%edx, %edx
.LBB3_140:
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	cmpl	$0, ana_NONHORNCLAUSES(%rip)
	je	.LBB3_142
# BB#141:
	movl	$5, %esi
	movl	$-1, %edx
	jmp	.LBB3_143
.LBB3_142:
	movl	$5, %esi
	xorl	%edx, %edx
.LBB3_143:
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	ana_AutoConfiguration, .Lfunc_end3-ana_AutoConfiguration
	.cfi_endproc

	.p2align	4, 0x90
	.type	flag_SetFlagValue,@function
flag_SetFlagValue:                      # @flag_SetFlagValue
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	%ebp, %edi
	callq	flag_Minimum
	cmpl	%ebx, %eax
	jge	.LBB4_1
# BB#3:
	movl	%ebp, %edi
	callq	flag_Maximum
	cmpl	%ebx, %eax
	jle	.LBB4_4
# BB#5:                                 # %flag_CheckFlagValueInRange.exit
	movl	%ebp, %eax
	movl	%ebx, (%r14,%rax,4)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB4_1:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	%ebp, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.40, %edi
	jmp	.LBB4_2
.LBB4_4:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	%ebp, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.41, %edi
.LBB4_2:
	xorl	%eax, %eax
	movl	%ebx, %esi
	movq	%rcx, %rdx
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end4:
	.size	flag_SetFlagValue, .Lfunc_end4-flag_SetFlagValue
	.cfi_endproc

	.globl	ana_ExploitSortAnalysis
	.p2align	4, 0x90
	.type	ana_ExploitSortAnalysis,@function
ana_ExploitSortAnalysis:                # @ana_ExploitSortAnalysis
	.cfi_startproc
# BB#0:
	cmpl	$0, ana_SORTRES(%rip)
	je	.LBB5_3
# BB#1:
	cmpl	$0, ana_SORTMANYEQUATIONS(%rip)
	jne	.LBB5_4
# BB#2:
	movb	ana_PEQUATIONS(%rip), %al
	xorb	$1, %al
	testb	$1, %al
	jne	.LBB5_4
.LBB5_3:
	retq
.LBB5_4:
	movl	$89, %esi
	movl	$1, %edx
	jmp	flag_SetFlagValue       # TAILCALL
.Lfunc_end5:
	.size	ana_ExploitSortAnalysis, .Lfunc_end5-ana_ExploitSortAnalysis
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_Error,@function
misc_Error:                             # @misc_Error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	misc_Error, .Lfunc_end6-misc_Error
	.cfi_endproc

	.p2align	4, 0x90
	.type	symbol_PositiveArity,@function
symbol_PositiveArity:                   # @symbol_PositiveArity
	.cfi_startproc
# BB#0:
	negl	%edi
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %edi
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rax
	movl	16(%rax), %ecx
	cmpl	$-2, %ecx
	movl	$-1, %eax
	cmovgl	%ecx, %eax
	retq
.Lfunc_end7:
	.size	symbol_PositiveArity, .Lfunc_end7-symbol_PositiveArity
	.cfi_endproc

	.p2align	4, 0x90
	.type	ana_NodeGreater,@function
ana_NodeGreater:                        # @ana_NodeGreater
	.cfi_startproc
# BB#0:
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	(%rdi), %rcx
	movq	(%rax,%rcx,8), %rcx
	xorl	%edx, %edx
	xorl	%edi, %edi
	subl	24(%rcx), %edi
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %edi
	movslq	%edi, %rdi
	movq	(%rax,%rdi,8), %rdi
	movl	16(%rdi), %edi
	movslq	(%rsi), %rsi
	movq	(%rax,%rsi,8), %rsi
	subl	24(%rsi), %edx
	sarl	%cl, %edx
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %rcx
	xorl	%eax, %eax
	cmpl	16(%rcx), %edi
	setg	%al
	retq
.Lfunc_end8:
	.size	ana_NodeGreater, .Lfunc_end8-ana_NodeGreater
	.cfi_endproc

	.type	ana_EQUATIONS,@object   # @ana_EQUATIONS
	.comm	ana_EQUATIONS,4,4
	.type	ana_PEQUATIONS,@object  # @ana_PEQUATIONS
	.local	ana_PEQUATIONS
	.comm	ana_PEQUATIONS,1,4
	.type	ana_NEQUATIONS,@object  # @ana_NEQUATIONS
	.local	ana_NEQUATIONS
	.comm	ana_NEQUATIONS,1,4
	.type	ana_FUNCTIONS,@object   # @ana_FUNCTIONS
	.local	ana_FUNCTIONS
	.comm	ana_FUNCTIONS,1,4
	.type	ana_FINDOMAIN,@object   # @ana_FINDOMAIN
	.local	ana_FINDOMAIN
	.comm	ana_FINDOMAIN,1,4
	.type	ana_NONTRIVDOMAIN,@object # @ana_NONTRIVDOMAIN
	.local	ana_NONTRIVDOMAIN
	.comm	ana_NONTRIVDOMAIN,1,4
	.type	ana_MONADIC,@object     # @ana_MONADIC
	.local	ana_MONADIC
	.comm	ana_MONADIC,4,4
	.type	ana_NONMONADIC,@object  # @ana_NONMONADIC
	.local	ana_NONMONADIC
	.comm	ana_NONMONADIC,4,4
	.type	ana_PROP,@object        # @ana_PROP
	.local	ana_PROP
	.comm	ana_PROP,4,4
	.type	ana_GROUND,@object      # @ana_GROUND
	.local	ana_GROUND
	.comm	ana_GROUND,4,4
	.type	ana_SORTRES,@object     # @ana_SORTRES
	.comm	ana_SORTRES,4,4
	.type	ana_USORTRES,@object    # @ana_USORTRES
	.comm	ana_USORTRES,4,4
	.type	ana_NONUNIT,@object     # @ana_NONUNIT
	.local	ana_NONUNIT
	.comm	ana_NONUNIT,1,4
	.type	ana_CONGROUND,@object   # @ana_CONGROUND
	.local	ana_CONGROUND
	.comm	ana_CONGROUND,1,4
	.type	ana_AXIOMCLAUSES,@object # @ana_AXIOMCLAUSES
	.local	ana_AXIOMCLAUSES
	.comm	ana_AXIOMCLAUSES,4,4
	.type	ana_CONCLAUSES,@object  # @ana_CONCLAUSES
	.local	ana_CONCLAUSES
	.comm	ana_CONCLAUSES,4,4
	.type	ana_NONHORNCLAUSES,@object # @ana_NONHORNCLAUSES
	.local	ana_NONHORNCLAUSES
	.comm	ana_NONHORNCLAUSES,4,4
	.type	ana_FINITEMONADICPREDICATES,@object # @ana_FINITEMONADICPREDICATES
	.comm	ana_FINITEMONADICPREDICATES,8,8
	.type	ana_PUREPROPOSITIONAL,@object # @ana_PUREPROPOSITIONAL
	.local	ana_PUREPROPOSITIONAL
	.comm	ana_PUREPROPOSITIONAL,4,4
	.type	ana_SORTMANYEQUATIONS,@object # @ana_SORTMANYEQUATIONS
	.local	ana_SORTMANYEQUATIONS
	.comm	ana_SORTMANYEQUATIONS,4,4
	.type	ana_SORTDECEQUATIONS,@object # @ana_SORTDECEQUATIONS
	.comm	ana_SORTDECEQUATIONS,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Horn"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Non-Horn"
	.size	.L.str.1, 9

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n This is a monadic %s problem"
	.size	.L.str.2, 31

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" with equality."
	.size	.L.str.3, 16

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	" without equality."
	.size	.L.str.4, 19

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\n This is a first-order %s problem containing equality."
	.size	.L.str.5, 56

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\n This is a pure equality %s problem."
	.size	.L.str.6, 38

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\n This is a unit equality problem."
	.size	.L.str.7, 35

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"\n This is a first-order %s problem without equality."
	.size	.L.str.8, 53

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"\n This is a propositional %s problem."
	.size	.L.str.9, 38

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"\n This is a problem that has, if any, a finite domain model."
	.size	.L.str.10, 61

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"\n There is a finite domain clause."
	.size	.L.str.11, 35

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"\n There are no function symbols."
	.size	.L.str.12, 33

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"\n This is a problem that has, if any, a non-trivial domain model."
	.size	.L.str.13, 66

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"\n This is a problem that contains sort information."
	.size	.L.str.14, 52

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"\n All equations are many sorted."
	.size	.L.str.15, 33

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"\n All equations are sort-decreasing."
	.size	.L.str.16, 37

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"\n The conjecture is ground."
	.size	.L.str.17, 28

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"\n The following monadic predicates have finite extensions: "
	.size	.L.str.18, 60

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	", "
	.size	.L.str.19, 3

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"\n Axiom clauses: %d Conjecture clauses: %d"
	.size	.L.str.20, 43

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"\n Extras    : "
	.size	.L.str.21, 15

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Input Saturation, "
	.size	.L.str.22, 19

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"No Input Saturation, "
	.size	.L.str.23, 22

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"No Selection, "
	.size	.L.str.24, 15

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"Dynamic Selection, "
	.size	.L.str.25, 20

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"Always Selection, "
	.size	.L.str.26, 19

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"Full Splitting, "
	.size	.L.str.27, 17

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"No Splitting, "
	.size	.L.str.28, 15

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"Maximum of %d Splits, "
	.size	.L.str.29, 23

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"Full Reduction, "
	.size	.L.str.30, 17

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Lazy Reduction, "
	.size	.L.str.31, 17

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	" Ratio: %d, FuncWeight: %d, VarWeight: %d"
	.size	.L.str.32, 42

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"\n Precedence: "
	.size	.L.str.33, 15

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"\n Ordering  : "
	.size	.L.str.34, 15

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"KBO"
	.size	.L.str.35, 4

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"RPOS"
	.size	.L.str.36, 5

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"\n Inferences: "
	.size	.L.str.37, 15

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"%s=%d "
	.size	.L.str.38, 7

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"\n Reductions: "
	.size	.L.str.39, 15

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"\n Error: Flag value %d is too small for flag %s.\n"
	.size	.L.str.40, 50

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"\n Error: Flag value %d is too large for flag %s.\n"
	.size	.L.str.41, 50


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
