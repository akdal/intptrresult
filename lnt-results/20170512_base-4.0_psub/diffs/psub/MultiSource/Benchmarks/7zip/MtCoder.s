	.text
	.file	"MtCoder.bc"
	.globl	LoopThread_Construct
	.p2align	4, 0x90
	.type	LoopThread_Construct,@function
LoopThread_Construct:                   # @LoopThread_Construct
	.cfi_startproc
# BB#0:
	movl	$0, 8(%rdi)
	movl	$0, 16(%rdi)
	movl	$0, 120(%rdi)
	retq
.Lfunc_end0:
	.size	LoopThread_Construct, .Lfunc_end0-LoopThread_Construct
	.cfi_endproc

	.globl	LoopThread_Close
	.p2align	4, 0x90
	.type	LoopThread_Close,@function
LoopThread_Close:                       # @LoopThread_Close
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	Thread_Close
	leaq	16(%rbx), %rdi
	callq	Event_Close
	addq	$120, %rbx
	movq	%rbx, %rdi
	popq	%rbx
	jmp	Event_Close             # TAILCALL
.Lfunc_end1:
	.size	LoopThread_Close, .Lfunc_end1-LoopThread_Close
	.cfi_endproc

	.globl	LoopThread_Create
	.p2align	4, 0x90
	.type	LoopThread_Create,@function
LoopThread_Create:                      # @LoopThread_Create
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$0, 224(%rbx)
	leaq	16(%rbx), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	testl	%eax, %eax
	jne	.LBB2_2
# BB#1:
	leaq	120(%rbx), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	testl	%eax, %eax
	je	.LBB2_3
.LBB2_2:
	popq	%rbx
	retq
.LBB2_3:
	movl	$LoopThreadFunc, %esi
	movq	%rbx, %rdi
	movq	%rbx, %rdx
	popq	%rbx
	jmp	Thread_Create           # TAILCALL
.Lfunc_end2:
	.size	LoopThread_Create, .Lfunc_end2-LoopThread_Create
	.cfi_endproc

	.p2align	4, 0x90
	.type	LoopThreadFunc,@function
LoopThreadFunc:                         # @LoopThreadFunc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 48
.Lcfi9:
	.cfi_offset %rbx, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	16(%rbx), %r14
	leaq	120(%rbx), %r15
	movl	$12, %ebp
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	Event_Wait
	testl	%eax, %eax
	jne	.LBB3_5
# BB#2:                                 #   in Loop: Header=BB3_1 Depth=1
	cmpl	$0, 224(%rbx)
	jne	.LBB3_3
# BB#4:                                 #   in Loop: Header=BB3_1 Depth=1
	movq	240(%rbx), %rdi
	callq	*232(%rbx)
	movl	%eax, 248(%rbx)
	movq	%r15, %rdi
	callq	Event_Set
	testl	%eax, %eax
	je	.LBB3_1
	jmp	.LBB3_5
.LBB3_3:
	xorl	%ebp, %ebp
.LBB3_5:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	LoopThreadFunc, .Lfunc_end3-LoopThreadFunc
	.cfi_endproc

	.globl	LoopThread_StopAndWait
	.p2align	4, 0x90
	.type	LoopThread_StopAndWait,@function
LoopThread_StopAndWait:                 # @LoopThread_StopAndWait
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, 224(%rbx)
	leaq	16(%rbx), %rdi
	callq	Event_Set
	testl	%eax, %eax
	je	.LBB4_2
# BB#1:
	movl	$12, %eax
	popq	%rbx
	retq
.LBB4_2:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	Thread_Wait             # TAILCALL
.Lfunc_end4:
	.size	LoopThread_StopAndWait, .Lfunc_end4-LoopThread_StopAndWait
	.cfi_endproc

	.globl	LoopThread_StartSubThread
	.p2align	4, 0x90
	.type	LoopThread_StartSubThread,@function
LoopThread_StartSubThread:              # @LoopThread_StartSubThread
	.cfi_startproc
# BB#0:
	addq	$16, %rdi
	jmp	Event_Set               # TAILCALL
.Lfunc_end5:
	.size	LoopThread_StartSubThread, .Lfunc_end5-LoopThread_StartSubThread
	.cfi_endproc

	.globl	LoopThread_WaitSubThread
	.p2align	4, 0x90
	.type	LoopThread_WaitSubThread,@function
LoopThread_WaitSubThread:               # @LoopThread_WaitSubThread
	.cfi_startproc
# BB#0:
	addq	$120, %rdi
	jmp	Event_Wait              # TAILCALL
.Lfunc_end6:
	.size	LoopThread_WaitSubThread, .Lfunc_end6-LoopThread_WaitSubThread
	.cfi_endproc

	.globl	MtProgress_Set
	.p2align	4, 0x90
	.type	MtProgress_Set,@function
MtProgress_Set:                         # @MtProgress_Set
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 48
.Lcfi20:
	.cfi_offset %rbx, -48
.Lcfi21:
	.cfi_offset %r12, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %rbp
	movl	%esi, %r12d
	movq	%rdi, %rbx
	leaq	32(%rbx), %r14
	movq	%r14, %rdi
	callq	pthread_mutex_lock
	cmpq	$-1, %rbp
	je	.LBB7_2
# BB#1:
	movl	%r12d, %eax
	movq	%rbp, %rcx
	subq	72(%rbx,%rax,8), %rcx
	addq	%rcx, (%rbx)
	movq	%rbp, 72(%rbx,%rax,8)
.LBB7_2:
	cmpq	$-1, %r15
	je	.LBB7_4
# BB#3:
	movl	%r12d, %eax
	movq	%r15, %rcx
	subq	328(%rbx,%rax,8), %rcx
	addq	%rcx, 8(%rbx)
	movq	%r15, 328(%rbx,%rax,8)
.LBB7_4:
	movl	24(%rbx), %ebp
	testl	%ebp, %ebp
	jne	.LBB7_9
# BB#5:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_6
# BB#7:
	movq	(%rbx), %rsi
	movq	8(%rbx), %rdx
	callq	*(%rdi)
	testl	%eax, %eax
	movl	$10, %ebp
	cmovel	%eax, %ebp
	jmp	.LBB7_8
.LBB7_6:
	xorl	%ebp, %ebp
.LBB7_8:                                # %Progress.exit
	movl	%ebp, 24(%rbx)
.LBB7_9:
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	movl	%ebp, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	MtProgress_Set, .Lfunc_end7-MtProgress_Set
	.cfi_endproc

	.globl	CMtThread_Construct
	.p2align	4, 0x90
	.type	CMtThread_Construct,@function
CMtThread_Construct:                    # @CMtThread_Construct
	.cfi_startproc
# BB#0:
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 24(%rdi)
	movl	$0, 312(%rdi)
	movl	$0, 416(%rdi)
	movl	$0, 56(%rdi)
	movl	$0, 64(%rdi)
	movl	$0, 168(%rdi)
	retq
.Lfunc_end8:
	.size	CMtThread_Construct, .Lfunc_end8-CMtThread_Construct
	.cfi_endproc

	.globl	MtCoder_Construct
	.p2align	4, 0x90
	.type	MtCoder_Construct,@function
MtCoder_Construct:                      # @MtCoder_Construct
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 16
.Lcfi26:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$0, 48(%rbx)
	leaq	696(%rbx), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB9_1:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, 40(%rax)
	movq	%rbx, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 24(%rax)
	movl	$0, 312(%rax)
	movl	$0, 416(%rax)
	movl	$0, 56(%rax)
	movl	$0, 64(%rax)
	movl	$0, 168(%rax)
	incq	%rcx
	addq	$520, %rax              # imm = 0x208
	cmpq	$32, %rcx
	jne	.LBB9_1
# BB#2:
	leaq	64(%rbx), %rdi
	callq	CriticalSection_Init
	addq	$144, %rbx
	movq	%rbx, %rdi
	popq	%rbx
	jmp	CriticalSection_Init    # TAILCALL
.Lfunc_end9:
	.size	MtCoder_Construct, .Lfunc_end9-MtCoder_Construct
	.cfi_endproc

	.globl	MtCoder_Destruct
	.p2align	4, 0x90
	.type	MtCoder_Destruct,@function
MtCoder_Destruct:                       # @MtCoder_Destruct
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 48
.Lcfi32:
	.cfi_offset %rbx, -48
.Lcfi33:
	.cfi_offset %r12, -40
.Lcfi34:
	.cfi_offset %r13, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB10_1:                               # =>This Inner Loop Header: Depth=1
	leaq	1008(%r12,%rbx), %rdi
	callq	Event_Close
	leaq	1112(%r12,%rbx), %rdi
	callq	Event_Close
	cmpl	$0, 752(%r12,%rbx)
	je	.LBB10_5
# BB#2:                                 #   in Loop: Header=BB10_1 Depth=1
	movl	$1, 968(%r12,%rbx)
	leaq	760(%r12,%rbx), %r14
	movq	%r14, %rdi
	callq	Event_Set
	leaq	744(%r12,%rbx), %r15
	testl	%eax, %eax
	jne	.LBB10_4
# BB#3:                                 #   in Loop: Header=BB10_1 Depth=1
	movq	%r15, %rdi
	callq	Thread_Wait
.LBB10_4:                               # %LoopThread_StopAndWait.exit.i
                                        #   in Loop: Header=BB10_1 Depth=1
	movq	%r15, %rdi
	callq	Thread_Close
	movq	%r14, %rdi
	callq	Event_Close
	leaq	864(%r12,%rbx), %rdi
	callq	Event_Close
.LBB10_5:                               #   in Loop: Header=BB10_1 Depth=1
	movq	696(%r12,%rbx), %rax
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB10_6
# BB#7:                                 #   in Loop: Header=BB10_1 Depth=1
	movq	704(%r12,%rbx), %rsi
	callq	*8(%rdi)
	movq	696(%r12,%rbx), %rax
	movq	48(%rax), %rdi
	movq	$0, 704(%r12,%rbx)
	testq	%rdi, %rdi
	je	.LBB10_8
# BB#9:                                 #   in Loop: Header=BB10_1 Depth=1
	leaq	720(%r12,%rbx), %r14
	movq	720(%r12,%rbx), %rsi
	callq	*8(%rdi)
	jmp	.LBB10_10
	.p2align	4, 0x90
.LBB10_6:                               # %.thread.i
                                        #   in Loop: Header=BB10_1 Depth=1
	movq	$0, 704(%r12,%rbx)
.LBB10_8:                               # %._crit_edge.i
                                        #   in Loop: Header=BB10_1 Depth=1
	imulq	$520, %r13, %rax        # imm = 0x208
	leaq	720(%r12,%rax), %r14
.LBB10_10:                              # %CMtThread_Destruct.exit
                                        #   in Loop: Header=BB10_1 Depth=1
	movq	$0, (%r14)
	incq	%r13
	addq	$520, %rbx              # imm = 0x208
	cmpq	$16640, %rbx            # imm = 0x4100
	jne	.LBB10_1
# BB#11:
	leaq	64(%r12), %rdi
	callq	pthread_mutex_destroy
	addq	$144, %r12
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	pthread_mutex_destroy   # TAILCALL
.Lfunc_end10:
	.size	MtCoder_Destruct, .Lfunc_end10-MtCoder_Destruct
	.cfi_endproc

	.globl	MtCoder_Code
	.p2align	4, 0x90
	.type	MtCoder_Code,@function
MtCoder_Code:                           # @MtCoder_Code
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi43:
	.cfi_def_cfa_offset 80
.Lcfi44:
	.cfi_offset %rbx, -56
.Lcfi45:
	.cfi_offset %r12, -48
.Lcfi46:
	.cfi_offset %r13, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	16(%r14), %r12d
	movl	$0, 104(%r14)
	movq	40(%r14), %rbx
	leaq	184(%r14), %rdi
	xorps	%xmm0, %xmm0
	movups	%xmm0, 112(%r14)
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$512, %edx              # imm = 0x200
	callq	memset
	testq	%r12, %r12
	movq	%rbx, 128(%r14)
	movl	$0, 136(%r14)
	movl	$0, %r15d
	je	.LBB11_30
# BB#1:                                 # %.lr.ph93.preheader
	leaq	696(%r14), %rbx
	xorl	%r13d, %r13d
	movq	%r14, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB11_2:                               # %.lr.ph93
                                        # =>This Inner Loop Header: Depth=1
	imulq	$520, %r13, %rcx        # imm = 0x208
	leaq	696(%r14,%rcx), %r15
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	je	.LBB11_5
# BB#3:                                 #   in Loop: Header=BB11_2 Depth=1
	movq	(%rbx), %rax
	movq	32(%rbx), %rcx
	cmpq	(%rax), %rcx
	je	.LBB11_7
# BB#4:                                 #   in Loop: Header=BB11_2 Depth=1
	leaq	32(%rbx), %r14
	movq	%r15, (%rsp)            # 8-byte Spill
	jmp	.LBB11_6
	.p2align	4, 0x90
.LBB11_5:                               # %._crit_edge.i
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	%r15, (%rsp)            # 8-byte Spill
	movq	(%rbx), %rax
	leaq	728(%r14,%rcx), %r14
	movq	%rbx, %r15
.LBB11_6:                               #   in Loop: Header=BB11_2 Depth=1
	movq	48(%rax), %rdi
	callq	*8(%rdi)
	movq	(%r15), %rax
	movq	(%rax), %rsi
	movq	%rsi, (%r14)
	movq	48(%rax), %rdi
	callq	*(%rdi)
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	(%rsp), %r15            # 8-byte Reload
	je	.LBB11_27
.LBB11_7:                               #   in Loop: Header=BB11_2 Depth=1
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB11_10
# BB#8:                                 #   in Loop: Header=BB11_2 Depth=1
	movq	(%rbx), %rax
	movq	16(%rbx), %rcx
	cmpq	8(%rax), %rcx
	je	.LBB11_12
# BB#9:                                 #   in Loop: Header=BB11_2 Depth=1
	leaq	16(%rbx), %r14
	jmp	.LBB11_11
	.p2align	4, 0x90
.LBB11_10:                              # %._crit_edge30.i
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	(%rbx), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	712(%r14,%rcx), %r14
	movq	%rbx, %r15
.LBB11_11:                              #   in Loop: Header=BB11_2 Depth=1
	movq	48(%rax), %rdi
	callq	*8(%rdi)
	movq	(%r15), %rax
	movq	8(%rax), %rsi
	movq	%rsi, (%r14)
	movq	48(%rax), %rdi
	callq	*(%rdi)
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	movq	8(%rsp), %r14           # 8-byte Reload
	je	.LBB11_27
.LBB11_12:                              #   in Loop: Header=BB11_2 Depth=1
	movq	$0, 304(%rbx)
	leaq	312(%rbx), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	movl	$12, %r15d
	testl	%eax, %eax
	jne	.LBB11_38
# BB#13:                                # %CMtThread_Prepare.exit
                                        #   in Loop: Header=BB11_2 Depth=1
	leaq	416(%rbx), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	testl	%eax, %eax
	jne	.LBB11_38
# BB#14:                                #   in Loop: Header=BB11_2 Depth=1
	incq	%r13
	addq	$520, %rbx              # imm = 0x208
	cmpq	%r12, %r13
	jb	.LBB11_2
# BB#15:                                # %.preheader
	testl	%r12d, %r12d
	je	.LBB11_28
# BB#16:                                # %.lr.ph89.preheader
	leaq	696(%r14), %rbx
	xorl	%ebp, %ebp
	movl	$12, %r15d
	.p2align	4, 0x90
.LBB11_17:                              # %.lr.ph89
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 56(%rbx)
	jne	.LBB11_21
# BB#18:                                #   in Loop: Header=BB11_17 Depth=1
	movq	$ThreadFunc, 280(%rbx)
	movq	%rbx, 288(%rbx)
	movl	$0, 272(%rbx)
	leaq	64(%rbx), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	testl	%eax, %eax
	jne	.LBB11_33
# BB#19:                                #   in Loop: Header=BB11_17 Depth=1
	leaq	168(%rbx), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	testl	%eax, %eax
	jne	.LBB11_33
# BB#20:                                # %LoopThread_Create.exit
                                        #   in Loop: Header=BB11_17 Depth=1
	leaq	48(%rbx), %rdi
	movl	$LoopThreadFunc, %esi
	movq	%rdi, %rdx
	callq	Thread_Create
	testl	%eax, %eax
	jne	.LBB11_33
.LBB11_21:                              #   in Loop: Header=BB11_17 Depth=1
	incq	%rbp
	addq	$520, %rbx              # imm = 0x208
	cmpq	%r12, %rbp
	jb	.LBB11_17
# BB#22:                                # %.thread77.preheader
	testl	%r12d, %r12d
	je	.LBB11_28
# BB#23:                                # %.lr.ph87.preheader
	leaq	760(%r14), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_24:                              # %.lr.ph87
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	Event_Set
	testl	%eax, %eax
	jne	.LBB11_39
# BB#25:                                # %.thread77
                                        #   in Loop: Header=BB11_24 Depth=1
	incq	%rbp
	addq	$520, %rbx              # imm = 0x208
	cmpq	%r12, %rbp
	jb	.LBB11_24
	jmp	.LBB11_29
.LBB11_27:
	movl	$2, %r15d
	jmp	.LBB11_38
.LBB11_28:
	xorl	%ebp, %ebp
.LBB11_29:                              # %.loopexit.loopexit
	xorl	%r15d, %r15d
.LBB11_30:                              # %.loopexit
	leaq	1112(%r14), %rdi
	callq	Event_Set
	leaq	1008(%r14), %rdi
	callq	Event_Set
	testl	%ebp, %ebp
	je	.LBB11_33
# BB#31:                                # %.lr.ph85.preheader
	movl	%ebp, %ebp
	leaq	864(%r14), %rbx
	.p2align	4, 0x90
.LBB11_32:                              # %.lr.ph85
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	Event_Wait
	addq	$520, %rbx              # imm = 0x208
	decq	%rbp
	jne	.LBB11_32
.LBB11_33:                              # %LoopThread_Create.exit.thread
	testl	%r12d, %r12d
	je	.LBB11_36
# BB#34:                                # %.lr.ph.preheader
	leaq	1008(%r14), %rbx
	.p2align	4, 0x90
.LBB11_35:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	Event_Close
	leaq	104(%rbx), %rdi
	callq	Event_Close
	addq	$520, %rbx              # imm = 0x208
	decq	%r12
	jne	.LBB11_35
.LBB11_36:                              # %._crit_edge
	testl	%r15d, %r15d
	jne	.LBB11_38
# BB#37:
	movl	104(%r14), %r15d
.LBB11_38:                              # %CMtThread_Prepare.exit.thread
	movl	%r15d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_39:
	movl	$1, 1000(%r14)
	movl	$12, %r15d
	jmp	.LBB11_30
.Lfunc_end11:
	.size	MtCoder_Code, .Lfunc_end11-MtCoder_Code
	.cfi_endproc

	.p2align	4, 0x90
	.type	ThreadFunc,@function
ThreadFunc:                             # @ThreadFunc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi56:
	.cfi_def_cfa_offset 144
.Lcfi57:
	.cfi_offset %rbx, -56
.Lcfi58:
	.cfi_offset %r12, -48
.Lcfi59:
	.cfi_offset %r13, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	312(%rbx), %rbp
	leaq	416(%rbx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%rbp, 56(%rsp)          # 8-byte Spill
.LBB12_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_5 Depth 2
	movq	(%rbx), %r13
	movl	40(%rbx), %r12d
	movl	16(%r13), %r15d
	movq	%rbp, %rdi
	callq	Event_Wait
	testl	%eax, %eax
	jne	.LBB12_20
# BB#2:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	(%rbx), %rdx
	movl	40(%rbx), %eax
	movl	16(%rdx), %ecx
	decl	%ecx
	movq	%rbx, %r14
	leal	1(%rax), %r13d
	cmpl	%ecx, %eax
	movl	$0, %eax
	cmoveq	%rax, %r13
	cmpl	$0, 304(%r14)
	jne	.LBB12_3
# BB#4:                                 #   in Loop: Header=BB12_1 Depth=1
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	(%rdx), %rbx
	movq	16(%r14), %rax
	movq	%rax, 24(%rsp)
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	24(%rdx), %r15
	movq	24(%r14), %r14
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB12_5:                               # %.thread.i.i
                                        #   Parent Loop BB12_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rbx, %rbx
	je	.LBB12_9
# BB#6:                                 #   in Loop: Header=BB12_5 Depth=2
	movq	%rbx, 48(%rsp)
	movq	%r15, %rdi
	movq	%r14, %rsi
	leaq	48(%rsp), %rdx
	callq	*(%r15)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB12_7
# BB#8:                                 #   in Loop: Header=BB12_5 Depth=2
	movq	48(%rsp), %rax
	addq	%rax, %r12
	subq	%rax, %rbx
	addq	%rax, %r14
	testq	%rax, %rax
	jne	.LBB12_5
.LBB12_9:                               #   in Loop: Header=BB12_1 Depth=1
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	(%r14), %rax
	movq	(%rax), %rax
	xorl	%ebx, %ebx
	movq	%rax, 72(%rsp)          # 8-byte Spill
	cmpq	%rax, %r12
	setne	%bl
	imulq	$520, %r13, %r15        # imm = 0x208
	movq	80(%rsp), %r13          # 8-byte Reload
	movl	%ebx, 1000(%r13,%r15)
	leaq	1008(%r13,%r15), %rdi
	callq	Event_Set
	movl	$12, %ebp
	testl	%eax, %eax
	jne	.LBB12_10
# BB#11:                                #   in Loop: Header=BB12_1 Depth=1
	movq	(%r14), %rax
	movq	8(%r14), %rdx
	movq	56(%rax), %rdi
	movl	40(%r14), %esi
	movq	24(%r14), %r8
	movl	%ebx, (%rsp)
	leaq	24(%rsp), %rcx
	movq	%r12, %r9
	callq	*(%rdi)
	testl	%eax, %eax
	movq	%r14, %rbx
	jne	.LBB12_12
# BB#13:                                #   in Loop: Header=BB12_1 Depth=1
	movq	(%rbx), %rax
	movl	40(%rbx), %ecx
	movq	$0, 184(%rax,%rcx,8)
	movq	$0, 440(%rax,%rcx,8)
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	Event_Wait
	testl	%eax, %eax
	jne	.LBB12_19
# BB#14:                                #   in Loop: Header=BB12_1 Depth=1
	cmpl	$0, 308(%rbx)
	jne	.LBB12_15
# BB#16:                                #   in Loop: Header=BB12_1 Depth=1
	movq	(%rbx), %rax
	movq	8(%rbx), %rsi
	movq	32(%rax), %rdi
	movq	24(%rsp), %rdx
	callq	*(%rdi)
	cmpq	24(%rsp), %rax
	jne	.LBB12_17
# BB#18:                                #   in Loop: Header=BB12_1 Depth=1
	leaq	1112(%r13,%r15), %rdi
	callq	Event_Set
	testl	%eax, %eax
	jne	.LBB12_19
# BB#26:                                #   in Loop: Header=BB12_1 Depth=1
	cmpq	72(%rsp), %r12          # 8-byte Folded Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
	je	.LBB12_1
# BB#27:
	xorl	%r15d, %r15d
	jmp	.LBB12_28
.LBB12_7:                               # %FullRead.exit.thread54.i
	movl	12(%rsp), %edx          # 4-byte Reload
	decl	%edx
	movq	32(%rsp), %rcx          # 8-byte Reload
	leal	1(%rcx), %eax
	xorl	%r12d, %r12d
	cmpl	%edx, %ecx
	cmovneq	%rax, %r12
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB12_21
.LBB12_20:                              # %MtThread_Process.exit.thread.loopexit
	decl	%r15d
	leal	1(%r12), %eax
	cmpl	%r15d, %r12d
	movl	$0, %r12d
	cmovneq	%rax, %r12
	movl	$12, %ebp
	jmp	.LBB12_21
.LBB12_3:
	decl	%r15d
	leal	1(%r12), %eax
	movl	%r15d, %ecx
	xorl	%r15d, %r15d
	cmpl	%ecx, %r12d
	movl	$0, %r12d
	cmovneq	%rax, %r12
	imulq	$520, %r13, %rax        # imm = 0x208
	movl	$1, 1000(%rdx,%rax)
	leaq	1008(%rdx,%rax), %rdi
	callq	Event_Set
	movl	$12, %ebp
	testl	%eax, %eax
	movq	%r14, %rbx
	movq	16(%rsp), %r13          # 8-byte Reload
	jne	.LBB12_21
	jmp	.LBB12_28
.LBB12_10:
	movq	%r14, %rbx
	jmp	.LBB12_19
.LBB12_12:
	movl	%eax, %ebp
	jmp	.LBB12_19
.LBB12_15:
	movl	$11, %ebp
	jmp	.LBB12_19
.LBB12_17:
	movl	$9, %ebp
.LBB12_19:                              # %MtThread_Process.exit.thread33.loopexit
	movl	12(%rsp), %edx          # 4-byte Reload
	decl	%edx
	movq	32(%rsp), %rcx          # 8-byte Reload
	leal	1(%rcx), %eax
	xorl	%r12d, %r12d
	cmpl	%edx, %ecx
	cmovneq	%rax, %r12
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB12_21:                              # %MtThread_Process.exit.thread
	movq	(%rbx), %r15
	leaq	64(%r15), %r14
	movq	%r14, %rdi
	callq	pthread_mutex_lock
	cmpl	$0, 104(%r15)
	jne	.LBB12_23
# BB#22:
	movl	%ebp, 104(%r15)
.LBB12_23:                              # %MtCoder_SetError.exit
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	movq	(%rbx), %rbx
	leaq	144(%rbx), %r14
	movq	%r14, %rdi
	callq	pthread_mutex_lock
	cmpl	$0, 136(%rbx)
	jne	.LBB12_25
# BB#24:
	movl	%ebp, 136(%rbx)
.LBB12_25:                              # %.thread
	movq	%r14, %rdi
	callq	pthread_mutex_unlock
	imulq	$520, %r12, %rbx        # imm = 0x208
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 1000(%r13,%rbx)
	leaq	1008(%r13,%rbx), %rdi
	callq	Event_Set
	leaq	1112(%r13,%rbx), %rdi
	callq	Event_Set
	movl	%ebp, %r15d
.LBB12_28:                              # %.thread35
	movl	%r15d, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	ThreadFunc, .Lfunc_end12-ThreadFunc
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
