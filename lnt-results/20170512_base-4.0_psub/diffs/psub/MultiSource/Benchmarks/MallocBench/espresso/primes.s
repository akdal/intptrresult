	.text
	.file	"primes.bc"
	.globl	primes_consensus
	.p2align	4, 0x90
	.type	primes_consensus,@function
primes_consensus:                       # @primes_consensus
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_29
# BB#1:
	movq	(%rbp), %r15
	cmpq	$0, 24(%rbp)
	je	.LBB0_30
# BB#2:                                 # %.preheader.preheader
	leaq	24(%rbp), %r13
	testq	%rdi, %rdi
	je	.LBB0_6
# BB#3:
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph42
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	full_row
	testl	%eax, %eax
	jne	.LBB0_31
# BB#5:                                 # %..preheader_crit_edge
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	(%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.LBB0_4
.LBB0_6:                                # %.preheader._crit_edge
	movl	(%r15), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	movl	$2, %ecx
	cmpl	$33, %eax
	jb	.LBB0_8
# BB#7:
	decl	%eax
	shrl	$5, %eax
	addl	$2, %eax
	movl	%eax, %ecx
.LBB0_8:
	movl	%ecx, %edi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r15, %rsi
	callq	set_copy
	movq	%rax, %rbx
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	16(%rbp), %rax
	testq	%rax, %rax
	je	.LBB0_24
# BB#9:                                 # %.lr.ph37.preheader
	leaq	-4(%rbx), %r11
	leaq	4(%rbx), %r10
	leaq	-12(%rbx), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	$1, %r14d
	movl	$2, %r9d
	.p2align	4, 0x90
.LBB0_10:                               # %.lr.ph37
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_18 Depth 2
                                        #     Child Loop BB0_22 Depth 2
	movl	(%rbx), %ecx
	movl	%ecx, %esi
	andl	$1023, %esi             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movq	%rsi, %rcx
	cmoveq	%r14, %rcx
	cmpq	$8, %rcx
	jb	.LBB0_21
# BB#11:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	%rcx, %r12
	andq	$1016, %r12             # imm = 0x3F8
	je	.LBB0_21
# BB#12:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_10 Depth=1
	leaq	1(%rsi), %rdx
	testl	%esi, %esi
	cmovneq	%r9, %rdx
	leaq	(%r11,%rdx,4), %rdi
	leaq	4(%rax,%rsi,4), %rbp
	cmpq	%rbp, %rdi
	jae	.LBB0_14
# BB#13:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_10 Depth=1
	leaq	(%r10,%rsi,4), %rdi
	leaq	-4(%rax,%rdx,4), %rdx
	cmpq	%rdi, %rdx
	jb	.LBB0_21
.LBB0_14:                               # %vector.body.preheader
                                        #   in Loop: Header=BB0_10 Depth=1
	leaq	-8(%r12), %rdi
	movq	%rdi, %rdx
	shrq	$3, %rdx
	btl	$3, %edi
	jb	.LBB0_16
# BB#15:                                # %vector.body.prol
                                        #   in Loop: Header=BB0_10 Depth=1
	movups	-12(%rbx,%rsi,4), %xmm0
	movups	-28(%rbx,%rsi,4), %xmm1
	movups	-12(%rax,%rsi,4), %xmm2
	movups	-28(%rax,%rsi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rsi,4)
	movups	%xmm3, -28(%rbx,%rsi,4)
	movl	$8, %edi
	testq	%rdx, %rdx
	jne	.LBB0_17
	jmp	.LBB0_19
.LBB0_16:                               #   in Loop: Header=BB0_10 Depth=1
	xorl	%edi, %edi
	testq	%rdx, %rdx
	je	.LBB0_19
.LBB0_17:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	%rcx, %rdx
	andq	$-8, %rdx
	negq	%rdx
	negq	%rdi
	movq	8(%rsp), %rbp           # 8-byte Reload
	leaq	(%rbp,%rsi,4), %rbp
	leaq	-12(%rax,%rsi,4), %r8
	.p2align	4, 0x90
.LBB0_18:                               # %vector.body
                                        #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbp,%rdi,4), %xmm0
	movups	-16(%rbp,%rdi,4), %xmm1
	movups	(%r8,%rdi,4), %xmm2
	movups	-16(%r8,%rdi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rbp,%rdi,4)
	movups	%xmm3, -16(%rbp,%rdi,4)
	movups	-32(%rbp,%rdi,4), %xmm0
	movups	-48(%rbp,%rdi,4), %xmm1
	movups	-32(%r8,%rdi,4), %xmm2
	movups	-48(%r8,%rdi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbp,%rdi,4)
	movups	%xmm3, -48(%rbp,%rdi,4)
	addq	$-16, %rdi
	cmpq	%rdi, %rdx
	jne	.LBB0_18
.LBB0_19:                               # %middle.block
                                        #   in Loop: Header=BB0_10 Depth=1
	cmpq	%r12, %rcx
	je	.LBB0_23
# BB#20:                                #   in Loop: Header=BB0_10 Depth=1
	subq	%r12, %rsi
	.p2align	4, 0x90
.LBB0_21:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_10 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB0_22:                               # %scalar.ph
                                        #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rax,%rsi,4), %ecx
	orl	%ecx, -4(%rbx,%rsi,4)
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB0_22
.LBB0_23:                               # %.loopexit
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	(%r13), %rax
	addq	$8, %r13
	testq	%rax, %rax
	jne	.LBB0_10
.LBB0_24:                               # %._crit_edge38
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	setp_equal
	testl	%eax, %eax
	je	.LBB0_36
# BB#25:
	testq	%rbx, %rbx
	je	.LBB0_27
# BB#26:
	movq	%rbx, %rdi
	callq	free
.LBB0_27:
	xorl	%eax, %eax
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	massive_count
	movl	cdata+32(%rip), %eax
	cmpl	$1, %eax
	jne	.LBB0_38
# BB#28:
	movl	cube(%rip), %esi
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rcx
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_addset
	movq	%rax, %r12
	jmp	.LBB0_113
.LBB0_29:
	movl	cube(%rip), %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	sf_new
	jmp	.LBB0_33
.LBB0_30:
	movl	cube(%rip), %esi
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rbx
	movq	16(%rbp), %rdx
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r15, %rsi
	callq	set_or
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rsi
	jmp	.LBB0_32
.LBB0_31:
	movl	cube(%rip), %esi
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rcx
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rcx, %rdi
.LBB0_32:
	callq	sf_addset
.LBB0_33:
	movq	%rax, %r12
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_35
# BB#34:
	callq	free
.LBB0_35:
	movq	%rbp, %rdi
	jmp	.LBB0_116
.LBB0_36:
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jge	.LBB0_40
# BB#37:
	movl	$8, %edi
	jmp	.LBB0_41
.LBB0_38:
	cmpl	%eax, cdata+36(%rip)
	jne	.LBB0_59
# BB#39:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	cubeunlist
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_contain
	movq	%rax, %r12
	jmp	.LBB0_113
.LBB0_40:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB0_41:
	movq	32(%rsp), %r14          # 8-byte Reload
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %rbp
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	set_diff
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	set_or
	testq	%rbp, %rbp
	je	.LBB0_43
# BB#42:
	movq	%rbp, %rdi
	callq	free
.LBB0_43:
	movq	%r14, %rdi
	callq	primes_consensus
	movq	%rax, %r12
	movslq	(%r12), %rax
	movslq	12(%r12), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	jle	.LBB0_61
# BB#44:                                # %.lr.ph.preheader
	movq	24(%r12), %rax
	leaq	(%rax,%rcx,4), %r15
	leaq	-4(%rbx), %r10
	leaq	4(%rbx), %r11
	movq	%rbx, %r9
	addq	$-12, %r9
	movl	$1, %r14d
	movl	$2, %r8d
	.p2align	4, 0x90
.LBB0_45:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_53 Depth 2
                                        #     Child Loop BB0_57 Depth 2
	movl	(%rax), %ecx
	movl	%ecx, %edi
	andl	$1023, %edi             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movq	%rdi, %rcx
	cmoveq	%r14, %rcx
	cmpq	$8, %rcx
	jb	.LBB0_56
# BB#46:                                # %min.iters.checked167
                                        #   in Loop: Header=BB0_45 Depth=1
	movq	%rcx, %r13
	andq	$1016, %r13             # imm = 0x3F8
	je	.LBB0_56
# BB#47:                                # %vector.memcheck189
                                        #   in Loop: Header=BB0_45 Depth=1
	leaq	1(%rdi), %rdx
	testl	%edi, %edi
	cmovneq	%r8, %rdx
	leaq	-4(%rax,%rdx,4), %rsi
	leaq	(%r11,%rdi,4), %rbp
	cmpq	%rbp, %rsi
	jae	.LBB0_49
# BB#48:                                # %vector.memcheck189
                                        #   in Loop: Header=BB0_45 Depth=1
	leaq	4(%rax,%rdi,4), %rsi
	leaq	(%r10,%rdx,4), %rdx
	cmpq	%rsi, %rdx
	jb	.LBB0_56
.LBB0_49:                               # %vector.body162.preheader
                                        #   in Loop: Header=BB0_45 Depth=1
	leaq	-8(%r13), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	btl	$3, %esi
	jb	.LBB0_51
# BB#50:                                # %vector.body162.prol
                                        #   in Loop: Header=BB0_45 Depth=1
	movups	-12(%rax,%rdi,4), %xmm0
	movups	-28(%rax,%rdi,4), %xmm1
	movups	-12(%rbx,%rdi,4), %xmm2
	movups	-28(%rbx,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rax,%rdi,4)
	movups	%xmm3, -28(%rax,%rdi,4)
	movl	$8, %ebp
	testq	%rdx, %rdx
	jne	.LBB0_52
	jmp	.LBB0_54
.LBB0_51:                               #   in Loop: Header=BB0_45 Depth=1
	xorl	%ebp, %ebp
	testq	%rdx, %rdx
	je	.LBB0_54
.LBB0_52:                               # %vector.body162.preheader.new
                                        #   in Loop: Header=BB0_45 Depth=1
	movq	%rcx, %rdx
	andq	$-8, %rdx
	negq	%rdx
	negq	%rbp
	leaq	-12(%rax,%rdi,4), %rsi
	leaq	(%r9,%rdi,4), %r8
	.p2align	4, 0x90
.LBB0_53:                               # %vector.body162
                                        #   Parent Loop BB0_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rsi,%rbp,4), %xmm0
	movups	-16(%rsi,%rbp,4), %xmm1
	movups	(%r8,%rbp,4), %xmm2
	movups	-16(%r8,%rbp,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rsi,%rbp,4)
	movups	%xmm3, -16(%rsi,%rbp,4)
	movups	-32(%rsi,%rbp,4), %xmm0
	movups	-48(%rsi,%rbp,4), %xmm1
	movups	-32(%r8,%rbp,4), %xmm2
	movups	-48(%r8,%rbp,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rsi,%rbp,4)
	movups	%xmm3, -48(%rsi,%rbp,4)
	addq	$-16, %rbp
	cmpq	%rbp, %rdx
	jne	.LBB0_53
.LBB0_54:                               # %middle.block163
                                        #   in Loop: Header=BB0_45 Depth=1
	cmpq	%r13, %rcx
	movl	$2, %r8d
	je	.LBB0_58
# BB#55:                                #   in Loop: Header=BB0_45 Depth=1
	subq	%r13, %rdi
	.p2align	4, 0x90
.LBB0_56:                               # %scalar.ph164.preheader
                                        #   in Loop: Header=BB0_45 Depth=1
	incq	%rdi
	.p2align	4, 0x90
.LBB0_57:                               # %scalar.ph164
                                        #   Parent Loop BB0_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rbx,%rdi,4), %ecx
	andl	%ecx, -4(%rax,%rdi,4)
	decq	%rdi
	cmpq	$1, %rdi
	jg	.LBB0_57
.LBB0_58:                               # %.loopexit211
                                        #   in Loop: Header=BB0_45 Depth=1
	movslq	(%r12), %rcx
	leaq	(%rax,%rcx,4), %rax
	cmpq	%r15, %rax
	jb	.LBB0_45
	jmp	.LBB0_115
.LBB0_59:                               # %primes_consensus_special_cases.exit
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jge	.LBB0_62
# BB#60:
	movl	$8, %edi
	jmp	.LBB0_63
.LBB0_61:                               # %._crit_edge
	testq	%rbx, %rbx
	jne	.LBB0_115
	jmp	.LBB0_117
.LBB0_62:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB0_63:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jge	.LBB0_65
# BB#64:
	movl	$8, %edi
	jmp	.LBB0_66
.LBB0_65:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB0_66:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %r15
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rsi
	movq	%r15, %rdx
	callq	binate_split_select
	movl	%eax, %r14d
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movl	%r14d, %edx
	callq	scofactor
	movq	%rax, %rdi
	callq	primes_consensus
	movq	%rax, %r13
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rbx
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movq	%r15, %rsi
	movl	%r14d, %edx
	callq	scofactor
	movq	%rax, %rdi
	callq	primes_consensus
	movq	%rax, %r14
	movslq	(%r13), %rcx
	movslq	12(%r13), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB0_82
# BB#67:                                # %.lr.ph.i.i.preheader
	movq	24(%r13), %rbp
	leaq	(%rbp,%rax,4), %r15
	leaq	-4(%rbx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	4(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	-12(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB0_68:                               # %.lr.ph.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_76 Depth 2
                                        #     Child Loop BB0_80 Depth 2
	movl	(%rbp), %ecx
	movl	%ecx, %eax
	andl	$1023, %eax             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movq	%rax, %rcx
	cmoveq	%r12, %rcx
	cmpq	$8, %rcx
	jb	.LBB0_79
# BB#69:                                # %min.iters.checked69
                                        #   in Loop: Header=BB0_68 Depth=1
	movq	%rcx, %r8
	andq	$1016, %r8              # imm = 0x3F8
	je	.LBB0_79
# BB#70:                                # %vector.memcheck91
                                        #   in Loop: Header=BB0_68 Depth=1
	leaq	1(%rax), %rdx
	testl	%eax, %eax
	movl	$2, %esi
	cmovneq	%rsi, %rdx
	leaq	-4(%rbp,%rdx,4), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	leaq	(%rdi,%rax,4), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB0_72
# BB#71:                                # %vector.memcheck91
                                        #   in Loop: Header=BB0_68 Depth=1
	leaq	4(%rbp,%rax,4), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rdx,4), %rdx
	cmpq	%rsi, %rdx
	jb	.LBB0_79
.LBB0_72:                               # %vector.body64.preheader
                                        #   in Loop: Header=BB0_68 Depth=1
	leaq	-8(%r8), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	btl	$3, %esi
	jb	.LBB0_74
# BB#73:                                # %vector.body64.prol
                                        #   in Loop: Header=BB0_68 Depth=1
	movups	-12(%rbp,%rax,4), %xmm0
	movups	-28(%rbp,%rax,4), %xmm1
	movups	-12(%rbx,%rax,4), %xmm2
	movups	-28(%rbx,%rax,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbp,%rax,4)
	movups	%xmm3, -28(%rbp,%rax,4)
	movl	$8, %esi
	testq	%rdx, %rdx
	jne	.LBB0_75
	jmp	.LBB0_77
.LBB0_74:                               #   in Loop: Header=BB0_68 Depth=1
	xorl	%esi, %esi
	testq	%rdx, %rdx
	je	.LBB0_77
.LBB0_75:                               # %vector.body64.preheader.new
                                        #   in Loop: Header=BB0_68 Depth=1
	movq	%rcx, %rdi
	andq	$-8, %rdi
	negq	%rdi
	negq	%rsi
	leaq	-12(%rbp,%rax,4), %rbx
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,4), %rdx
	.p2align	4, 0x90
.LBB0_76:                               # %vector.body64
                                        #   Parent Loop BB0_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbx,%rsi,4), %xmm0
	movups	-16(%rbx,%rsi,4), %xmm1
	movups	(%rdx,%rsi,4), %xmm2
	movups	-16(%rdx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rbx,%rsi,4)
	movups	%xmm3, -16(%rbx,%rsi,4)
	movups	-32(%rbx,%rsi,4), %xmm0
	movups	-48(%rbx,%rsi,4), %xmm1
	movups	-32(%rdx,%rsi,4), %xmm2
	movups	-48(%rdx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbx,%rsi,4)
	movups	%xmm3, -48(%rbx,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB0_76
.LBB0_77:                               # %middle.block65
                                        #   in Loop: Header=BB0_68 Depth=1
	cmpq	%r8, %rcx
	movq	40(%rsp), %rbx          # 8-byte Reload
	je	.LBB0_81
# BB#78:                                #   in Loop: Header=BB0_68 Depth=1
	subq	%r8, %rax
	.p2align	4, 0x90
.LBB0_79:                               # %scalar.ph66.preheader
                                        #   in Loop: Header=BB0_68 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB0_80:                               # %scalar.ph66
                                        #   Parent Loop BB0_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rbx,%rax,4), %ecx
	andl	%ecx, -4(%rbp,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB0_80
.LBB0_81:                               # %.loopexit213
                                        #   in Loop: Header=BB0_68 Depth=1
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	cdist
	movl	(%rbp), %ecx
	movl	%ecx, %edx
	orl	$8192, %edx             # imm = 0x2000
	andl	$-8193, %ecx            # imm = 0xDFFF
	testl	%eax, %eax
	cmovlel	%edx, %ecx
	movl	%ecx, (%rbp)
	movslq	(%r13), %rax
	leaq	(%rbp,%rax,4), %rbp
	cmpq	%r15, %rbp
	jb	.LBB0_68
.LBB0_82:                               # %and_with_cofactor.exit.i
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	sf_inactive
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movslq	(%r14), %rcx
	movslq	12(%r14), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	movq	48(%rsp), %rbp          # 8-byte Reload
	jle	.LBB0_98
# BB#83:                                # %.lr.ph.i64.i.preheader
	movq	24(%r14), %rbx
	leaq	(%rbx,%rax,4), %r15
	leaq	-4(%rbp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	4(%rbp), %r12
	leaq	-12(%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$1, %r13d
	.p2align	4, 0x90
.LBB0_84:                               # %.lr.ph.i64.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_92 Depth 2
                                        #     Child Loop BB0_96 Depth 2
	movl	(%rbx), %ecx
	movl	%ecx, %eax
	andl	$1023, %eax             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movq	%rax, %rcx
	cmoveq	%r13, %rcx
	cmpq	$8, %rcx
	jb	.LBB0_95
# BB#85:                                # %min.iters.checked118
                                        #   in Loop: Header=BB0_84 Depth=1
	movq	%rcx, %r8
	andq	$1016, %r8              # imm = 0x3F8
	je	.LBB0_95
# BB#86:                                # %vector.memcheck140
                                        #   in Loop: Header=BB0_84 Depth=1
	leaq	1(%rax), %rdx
	testl	%eax, %eax
	movl	$2, %esi
	cmovneq	%rsi, %rdx
	leaq	-4(%rbx,%rdx,4), %rsi
	leaq	(%r12,%rax,4), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB0_88
# BB#87:                                # %vector.memcheck140
                                        #   in Loop: Header=BB0_84 Depth=1
	leaq	4(%rbx,%rax,4), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	leaq	(%rdi,%rdx,4), %rdx
	cmpq	%rsi, %rdx
	jb	.LBB0_95
.LBB0_88:                               # %vector.body113.preheader
                                        #   in Loop: Header=BB0_84 Depth=1
	leaq	-8(%r8), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	btl	$3, %esi
	jb	.LBB0_90
# BB#89:                                # %vector.body113.prol
                                        #   in Loop: Header=BB0_84 Depth=1
	movups	-12(%rbx,%rax,4), %xmm0
	movups	-28(%rbx,%rax,4), %xmm1
	movups	-12(%rbp,%rax,4), %xmm2
	movups	-28(%rbp,%rax,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rax,4)
	movups	%xmm3, -28(%rbx,%rax,4)
	movl	$8, %esi
	testq	%rdx, %rdx
	jne	.LBB0_91
	jmp	.LBB0_93
.LBB0_90:                               #   in Loop: Header=BB0_84 Depth=1
	xorl	%esi, %esi
	testq	%rdx, %rdx
	je	.LBB0_93
.LBB0_91:                               # %vector.body113.preheader.new
                                        #   in Loop: Header=BB0_84 Depth=1
	movq	%rcx, %rdi
	andq	$-8, %rdi
	negq	%rdi
	negq	%rsi
	leaq	-12(%rbx,%rax,4), %rbp
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,4), %rdx
	.p2align	4, 0x90
.LBB0_92:                               # %vector.body113
                                        #   Parent Loop BB0_84 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbp,%rsi,4), %xmm0
	movups	-16(%rbp,%rsi,4), %xmm1
	movups	(%rdx,%rsi,4), %xmm2
	movups	-16(%rdx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rbp,%rsi,4)
	movups	%xmm3, -16(%rbp,%rsi,4)
	movups	-32(%rbp,%rsi,4), %xmm0
	movups	-48(%rbp,%rsi,4), %xmm1
	movups	-32(%rdx,%rsi,4), %xmm2
	movups	-48(%rdx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbp,%rsi,4)
	movups	%xmm3, -48(%rbp,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB0_92
.LBB0_93:                               # %middle.block114
                                        #   in Loop: Header=BB0_84 Depth=1
	cmpq	%r8, %rcx
	movq	48(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_97
# BB#94:                                #   in Loop: Header=BB0_84 Depth=1
	subq	%r8, %rax
	.p2align	4, 0x90
.LBB0_95:                               # %scalar.ph115.preheader
                                        #   in Loop: Header=BB0_84 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB0_96:                               # %scalar.ph115
                                        #   Parent Loop BB0_84 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rbp,%rax,4), %ecx
	andl	%ecx, -4(%rbx,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB0_96
.LBB0_97:                               # %.loopexit212
                                        #   in Loop: Header=BB0_84 Depth=1
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	cdist
	movl	(%rbx), %ecx
	movl	%ecx, %edx
	orl	$8192, %edx             # imm = 0x2000
	andl	$-8193, %ecx            # imm = 0xDFFF
	testl	%eax, %eax
	cmovlel	%edx, %ecx
	movl	%ecx, (%rbx)
	movslq	(%r14), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r15, %rbx
	jb	.LBB0_84
.LBB0_98:                               # %and_with_cofactor.exit68.i
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_inactive
	movq	%rax, %rbx
	movq	24(%rsp), %r13          # 8-byte Reload
	movl	4(%r13), %esi
	movl	$500, %edi              # imm = 0x1F4
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r14
	movq	24(%r14), %r15
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	sf_join
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_contain
	movl	(%r13), %ecx
	movl	12(%r13), %edx
	imull	%ecx, %edx
	testl	%edx, %edx
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jle	.LBB0_109
# BB#99:                                # %.lr.ph79.i
	movq	24(%r13), %r12
	movslq	%edx, %rax
	leaq	(%r12,%rax,4), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	(%rbx), %eax
	.p2align	4, 0x90
.LBB0_100:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_102 Depth 2
	movslq	12(%rbx), %rsi
	movslq	%eax, %rdx
	imulq	%rsi, %rdx
	testl	%edx, %edx
	jle	.LBB0_108
# BB#101:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_100 Depth=1
	movq	24(%rbx), %r13
	leaq	(%r13,%rdx,4), %rbp
	.p2align	4, 0x90
.LBB0_102:                              # %.lr.ph.i
                                        #   Parent Loop BB0_100 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	cdist01
	cmpl	$1, %eax
	jne	.LBB0_106
# BB#103:                               #   in Loop: Header=BB0_102 Depth=2
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	consensus
	movl	12(%r14), %eax
	incl	%eax
	movl	%eax, 12(%r14)
	cmpl	8(%r14), %eax
	jge	.LBB0_105
# BB#104:                               #   in Loop: Header=BB0_102 Depth=2
	movslq	(%r14), %rax
	leaq	(%r15,%rax,4), %r15
	jmp	.LBB0_106
	.p2align	4, 0x90
.LBB0_105:                              #   in Loop: Header=BB0_102 Depth=2
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_contain
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rcx, %rsi
	callq	sf_union
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %esi
	movl	$500, %edi              # imm = 0x1F4
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r14
	movq	24(%r14), %r15
.LBB0_106:                              #   in Loop: Header=BB0_102 Depth=2
	movslq	(%rbx), %rax
	leaq	(%r13,%rax,4), %r13
	cmpq	%rbp, %r13
	jb	.LBB0_102
# BB#107:                               # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB0_100 Depth=1
	movq	24(%rsp), %r13          # 8-byte Reload
	movl	(%r13), %ecx
	movq	48(%rsp), %rbp          # 8-byte Reload
.LBB0_108:                              # %._crit_edge.i
                                        #   in Loop: Header=BB0_100 Depth=1
	movslq	%ecx, %rdx
	leaq	(%r12,%rdx,4), %r12
	cmpq	16(%rsp), %r12          # 8-byte Folded Reload
	jb	.LBB0_100
.LBB0_109:                              # %primes_consensus_merge.exit
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_contain
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rcx, %rsi
	callq	sf_union
	movq	%rax, %r12
	movq	40(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB0_111
# BB#110:
	callq	free
.LBB0_111:
	testq	%rbp, %rbp
	movq	32(%rsp), %rbx          # 8-byte Reload
	je	.LBB0_113
# BB#112:
	movq	%rbp, %rdi
	callq	free
.LBB0_113:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_115
# BB#114:
	callq	free
.LBB0_115:
	movq	%rbx, %rdi
.LBB0_116:                              # %primes_consensus_special_cases.exit.thread
	callq	free
.LBB0_117:                              # %primes_consensus_special_cases.exit.thread
	movq	%r12, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	primes_consensus, .Lfunc_end0-primes_consensus
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
