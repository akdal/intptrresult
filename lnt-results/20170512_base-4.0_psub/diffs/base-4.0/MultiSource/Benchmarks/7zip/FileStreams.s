	.text
	.file	"FileStreams.bc"
	.globl	_ZN13CInFileStream4OpenEPKw
	.p2align	4, 0x90
	.type	_ZN13CInFileStream4OpenEPKw,@function
_ZN13CInFileStream4OpenEPKw:            # @_ZN13CInFileStream4OpenEPKw
	.cfi_startproc
# BB#0:
	movzbl	20(%rdi), %edx
	addq	$24, %rdi
	jmp	_ZN8NWindows5NFile3NIO7CInFile4OpenEPKwb # TAILCALL
.Lfunc_end0:
	.size	_ZN13CInFileStream4OpenEPKw, .Lfunc_end0-_ZN13CInFileStream4OpenEPKw
	.cfi_endproc

	.globl	_ZN13CInFileStream10OpenSharedEPKwb
	.p2align	4, 0x90
	.type	_ZN13CInFileStream10OpenSharedEPKwb,@function
_ZN13CInFileStream10OpenSharedEPKwb:    # @_ZN13CInFileStream10OpenSharedEPKwb
	.cfi_startproc
# BB#0:
	movzbl	20(%rdi), %edx
	addq	$24, %rdi
	jmp	_ZN8NWindows5NFile3NIO7CInFile4OpenEPKwb # TAILCALL
.Lfunc_end1:
	.size	_ZN13CInFileStream10OpenSharedEPKwb, .Lfunc_end1-_ZN13CInFileStream10OpenSharedEPKwb
	.cfi_endproc

	.globl	_ZN13CInFileStream4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN13CInFileStream4ReadEPvjPj,@function
_ZN13CInFileStream4ReadEPvjPj:          # @_ZN13CInFileStream4ReadEPvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rcx, %rbx
	addq	$24, %rdi
	leaq	12(%rsp), %rcx
	callq	_ZN8NWindows5NFile3NIO7CInFile8ReadPartEPvjRj
	testq	%rbx, %rbx
	je	.LBB2_2
# BB#1:
	movl	12(%rsp), %ecx
	movl	%ecx, (%rbx)
.LBB2_2:
	xorl	%ecx, %ecx
	testb	%al, %al
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovnel	%ecx, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end2:
	.size	_ZN13CInFileStream4ReadEPvjPj, .Lfunc_end2-_ZN13CInFileStream4ReadEPvjPj
	.cfi_endproc

	.globl	_ZN16CStdInFileStream4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN16CStdInFileStream4ReadEPvjPj,@function
_ZN16CStdInFileStream4ReadEPvjPj:       # @_ZN16CStdInFileStream4ReadEPvjPj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 48
.Lcfi8:
	.cfi_offset %rbx, -40
.Lcfi9:
	.cfi_offset %r12, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rsi, %r15
	testq	%r14, %r14
	je	.LBB3_2
# BB#1:
	movl	$0, (%r14)
.LBB3_2:                                # %.preheader
	movl	%edx, %r12d
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	xorl	%edi, %edi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	read
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jns	.LBB3_5
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	callq	__errno_location
	cmpl	$4, (%rax)
	je	.LBB3_3
.LBB3_5:                                # %.critedge
	testq	%r14, %r14
	sete	%cl
	xorl	%edx, %edx
	cmpq	$-1, %rbx
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovnel	%edx, %eax
	je	.LBB3_8
# BB#6:                                 # %.critedge
	testb	%cl, %cl
	jne	.LBB3_8
# BB#7:
	movl	%ebx, (%r14)
	xorl	%eax, %eax
.LBB3_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_ZN16CStdInFileStream4ReadEPvjPj, .Lfunc_end3-_ZN16CStdInFileStream4ReadEPvjPj
	.cfi_endproc

	.globl	_ZN13CInFileStream4SeekExjPy
	.p2align	4, 0x90
	.type	_ZN13CInFileStream4SeekExjPy,@function
_ZN13CInFileStream4SeekExjPy:           # @_ZN13CInFileStream4SeekExjPy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	%rcx, %rbx
	movl	$-2147287039, %eax      # imm = 0x80030001
	cmpl	$2, %edx
	ja	.LBB4_4
# BB#1:
	addq	$24, %rdi
	leaq	8(%rsp), %rcx
	callq	_ZN8NWindows5NFile3NIO9CFileBase4SeekExjRy
	testq	%rbx, %rbx
	je	.LBB4_3
# BB#2:
	movq	8(%rsp), %rcx
	movq	%rcx, (%rbx)
.LBB4_3:
	xorl	%ecx, %ecx
	testb	%al, %al
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovnel	%ecx, %eax
.LBB4_4:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZN13CInFileStream4SeekExjPy, .Lfunc_end4-_ZN13CInFileStream4SeekExjPy
	.cfi_endproc

	.globl	_ZN13CInFileStream7GetSizeEPy
	.p2align	4, 0x90
	.type	_ZN13CInFileStream7GetSizeEPy,@function
_ZN13CInFileStream7GetSizeEPy:          # @_ZN13CInFileStream7GetSizeEPy
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 16
	addq	$24, %rdi
	callq	_ZNK8NWindows5NFile3NIO9CFileBase9GetLengthERy
	xorl	%ecx, %ecx
	testb	%al, %al
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovnel	%ecx, %eax
	popq	%rcx
	retq
.Lfunc_end5:
	.size	_ZN13CInFileStream7GetSizeEPy, .Lfunc_end5-_ZN13CInFileStream7GetSizeEPy
	.cfi_endproc

	.globl	_ZThn8_N13CInFileStream7GetSizeEPy
	.p2align	4, 0x90
	.type	_ZThn8_N13CInFileStream7GetSizeEPy,@function
_ZThn8_N13CInFileStream7GetSizeEPy:     # @_ZThn8_N13CInFileStream7GetSizeEPy
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 16
	addq	$16, %rdi
	callq	_ZNK8NWindows5NFile3NIO9CFileBase9GetLengthERy
	xorl	%ecx, %ecx
	testb	%al, %al
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovnel	%ecx, %eax
	popq	%rcx
	retq
.Lfunc_end6:
	.size	_ZThn8_N13CInFileStream7GetSizeEPy, .Lfunc_end6-_ZThn8_N13CInFileStream7GetSizeEPy
	.cfi_endproc

	.globl	_ZN14COutFileStream5CloseEv
	.p2align	4, 0x90
	.type	_ZN14COutFileStream5CloseEv,@function
_ZN14COutFileStream5CloseEv:            # @_ZN14COutFileStream5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 16
	addq	$16, %rdi
	callq	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
	xorl	%ecx, %ecx
	testb	%al, %al
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovnel	%ecx, %eax
	popq	%rcx
	retq
.Lfunc_end7:
	.size	_ZN14COutFileStream5CloseEv, .Lfunc_end7-_ZN14COutFileStream5CloseEv
	.cfi_endproc

	.globl	_ZN14COutFileStream5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN14COutFileStream5WriteEPKvjPj,@function
_ZN14COutFileStream5WriteEPKvjPj:       # @_ZN14COutFileStream5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movq	%rdi, %rbx
	leaq	16(%rbx), %rdi
	leaq	4(%rsp), %rcx
	callq	_ZN8NWindows5NFile3NIO8COutFile9WritePartEPKvjRj
	movl	4(%rsp), %ecx
	addq	%rcx, 1104(%rbx)
	testq	%r14, %r14
	je	.LBB8_2
# BB#1:
	movl	%ecx, (%r14)
.LBB8_2:
	xorl	%ecx, %ecx
	testb	%al, %al
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovnel	%ecx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	_ZN14COutFileStream5WriteEPKvjPj, .Lfunc_end8-_ZN14COutFileStream5WriteEPKvjPj
	.cfi_endproc

	.globl	_ZN14COutFileStream4SeekExjPy
	.p2align	4, 0x90
	.type	_ZN14COutFileStream4SeekExjPy,@function
_ZN14COutFileStream4SeekExjPy:          # @_ZN14COutFileStream4SeekExjPy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -16
	movq	%rcx, %rbx
	movl	$-2147287039, %eax      # imm = 0x80030001
	cmpl	$2, %edx
	ja	.LBB9_4
# BB#1:
	addq	$16, %rdi
	leaq	8(%rsp), %rcx
	callq	_ZN8NWindows5NFile3NIO9CFileBase4SeekExjRy
	testq	%rbx, %rbx
	je	.LBB9_3
# BB#2:
	movq	8(%rsp), %rcx
	movq	%rcx, (%rbx)
.LBB9_3:
	xorl	%ecx, %ecx
	testb	%al, %al
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovnel	%ecx, %eax
.LBB9_4:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end9:
	.size	_ZN14COutFileStream4SeekExjPy, .Lfunc_end9-_ZN14COutFileStream4SeekExjPy
	.cfi_endproc

	.globl	_ZN14COutFileStream7SetSizeEy
	.p2align	4, 0x90
	.type	_ZN14COutFileStream7SetSizeEy,@function
_ZN14COutFileStream7SetSizeEy:          # @_ZN14COutFileStream7SetSizeEy
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi28:
	.cfi_def_cfa_offset 48
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	addq	$16, %rbx
	leaq	8(%rsp), %rcx
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	_ZN8NWindows5NFile3NIO9CFileBase4SeekExjRy
	movl	%eax, %ecx
	movl	$-2147467259, %eax      # imm = 0x80004005
	testb	%cl, %cl
	je	.LBB10_3
# BB#1:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN8NWindows5NFile3NIO8COutFile9SetLengthEy
	movl	%eax, %ecx
	movl	$-2147467259, %eax      # imm = 0x80004005
	testb	%cl, %cl
	je	.LBB10_3
# BB#2:
	movq	8(%rsp), %rsi
	leaq	16(%rsp), %rdx
	movq	%rbx, %rdi
	callq	_ZN8NWindows5NFile3NIO9CFileBase4SeekEyRy
	xorl	%ecx, %ecx
	testb	%al, %al
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovnel	%ecx, %eax
.LBB10_3:
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	_ZN14COutFileStream7SetSizeEy, .Lfunc_end10-_ZN14COutFileStream7SetSizeEy
	.cfi_endproc

	.globl	_ZN17CStdOutFileStream5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN17CStdOutFileStream5WriteEPKvjPj,@function
_ZN17CStdOutFileStream5WriteEPKvjPj:    # @_ZN17CStdOutFileStream5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi35:
	.cfi_def_cfa_offset 48
.Lcfi36:
	.cfi_offset %rbx, -40
.Lcfi37:
	.cfi_offset %r12, -32
.Lcfi38:
	.cfi_offset %r14, -24
.Lcfi39:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rsi, %r15
	testq	%r14, %r14
	je	.LBB11_2
# BB#1:
	movl	$0, (%r14)
.LBB11_2:                               # %.preheader
	movl	%edx, %r12d
	.p2align	4, 0x90
.LBB11_3:                               # =>This Inner Loop Header: Depth=1
	movl	$1, %edi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	write
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jns	.LBB11_5
# BB#4:                                 #   in Loop: Header=BB11_3 Depth=1
	callq	__errno_location
	cmpl	$4, (%rax)
	je	.LBB11_3
.LBB11_5:                               # %.critedge
	testq	%r14, %r14
	sete	%cl
	xorl	%edx, %edx
	cmpq	$-1, %rbx
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovnel	%edx, %eax
	je	.LBB11_8
# BB#6:                                 # %.critedge
	testb	%cl, %cl
	jne	.LBB11_8
# BB#7:
	movl	%ebx, (%r14)
	xorl	%eax, %eax
.LBB11_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	_ZN17CStdOutFileStream5WriteEPKvjPj, .Lfunc_end11-_ZN17CStdOutFileStream5WriteEPKvjPj
	.cfi_endproc

	.section	.text._ZN13CInFileStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv,@function
_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv: # @_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -32
.Lcfi44:
	.cfi_offset %r14, -24
.Lcfi45:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB12_1
# BB#2:
	movl	$IID_IInStream, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB12_3
.LBB12_1:
	movq	%rbx, (%r14)
.LBB12_6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB12_7:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB12_3:
	movl	$IID_IStreamGetSize, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB12_4
# BB#5:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	jmp	.LBB12_6
.LBB12_4:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB12_7
.Lfunc_end12:
	.size	_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end12-_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN13CInFileStream6AddRefEv,"axG",@progbits,_ZN13CInFileStream6AddRefEv,comdat
	.weak	_ZN13CInFileStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN13CInFileStream6AddRefEv,@function
_ZN13CInFileStream6AddRefEv:            # @_ZN13CInFileStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end13:
	.size	_ZN13CInFileStream6AddRefEv, .Lfunc_end13-_ZN13CInFileStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN13CInFileStream7ReleaseEv,"axG",@progbits,_ZN13CInFileStream7ReleaseEv,comdat
	.weak	_ZN13CInFileStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN13CInFileStream7ReleaseEv,@function
_ZN13CInFileStream7ReleaseEv:           # @_ZN13CInFileStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB14_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB14_2:
	popq	%rcx
	retq
.Lfunc_end14:
	.size	_ZN13CInFileStream7ReleaseEv, .Lfunc_end14-_ZN13CInFileStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN13CInFileStreamD2Ev,"axG",@progbits,_ZN13CInFileStreamD2Ev,comdat
	.weak	_ZN13CInFileStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN13CInFileStreamD2Ev,@function
_ZN13CInFileStreamD2Ev:                 # @_ZN13CInFileStreamD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi47:
	.cfi_def_cfa_offset 16
	movl	$_ZTV13CInFileStream+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTV13CInFileStream+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rdi)
	addq	$24, %rdi
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
	popq	%rax
	retq
.Lfunc_end15:
	.size	_ZN13CInFileStreamD2Ev, .Lfunc_end15-_ZN13CInFileStreamD2Ev
	.cfi_endproc

	.section	.text._ZN13CInFileStreamD0Ev,"axG",@progbits,_ZN13CInFileStreamD0Ev,comdat
	.weak	_ZN13CInFileStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN13CInFileStreamD0Ev,@function
_ZN13CInFileStreamD0Ev:                 # @_ZN13CInFileStreamD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 32
.Lcfi51:
	.cfi_offset %rbx, -24
.Lcfi52:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTV13CInFileStream+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTV13CInFileStream+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	leaq	24(%rbx), %rdi
.Ltmp0:
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp1:
# BB#1:                                 # %_ZN13CInFileStreamD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB16_2:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end16:
	.size	_ZN13CInFileStreamD0Ev, .Lfunc_end16-_ZN13CInFileStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end16-.Ltmp1     #   Call between .Ltmp1 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N13CInFileStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N13CInFileStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N13CInFileStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N13CInFileStream14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N13CInFileStream14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N13CInFileStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi53:
	.cfi_def_cfa_offset 16
	addq	$-8, %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB17_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB17_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB17_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB17_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB17_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB17_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB17_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB17_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB17_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB17_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB17_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB17_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB17_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB17_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB17_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB17_16
.LBB17_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_IInStream(%rip), %cl
	jne	.LBB17_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_IInStream+1(%rip), %al
	jne	.LBB17_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_IInStream+2(%rip), %al
	jne	.LBB17_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_IInStream+3(%rip), %al
	jne	.LBB17_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_IInStream+4(%rip), %al
	jne	.LBB17_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_IInStream+5(%rip), %al
	jne	.LBB17_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_IInStream+6(%rip), %al
	jne	.LBB17_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_IInStream+7(%rip), %al
	jne	.LBB17_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_IInStream+8(%rip), %al
	jne	.LBB17_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_IInStream+9(%rip), %al
	jne	.LBB17_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_IInStream+10(%rip), %al
	jne	.LBB17_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_IInStream+11(%rip), %al
	jne	.LBB17_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_IInStream+12(%rip), %al
	jne	.LBB17_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_IInStream+13(%rip), %al
	jne	.LBB17_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_IInStream+14(%rip), %al
	jne	.LBB17_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit6
	movb	15(%rsi), %al
	cmpb	IID_IInStream+15(%rip), %al
	jne	.LBB17_33
.LBB17_16:
	movq	%rdi, (%rdx)
	jmp	.LBB17_50
.LBB17_33:                              # %_ZeqRK4GUIDS1_.exit6.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IStreamGetSize(%rip), %cl
	jne	.LBB17_51
# BB#34:
	movb	1(%rsi), %cl
	cmpb	IID_IStreamGetSize+1(%rip), %cl
	jne	.LBB17_51
# BB#35:
	movb	2(%rsi), %cl
	cmpb	IID_IStreamGetSize+2(%rip), %cl
	jne	.LBB17_51
# BB#36:
	movb	3(%rsi), %cl
	cmpb	IID_IStreamGetSize+3(%rip), %cl
	jne	.LBB17_51
# BB#37:
	movb	4(%rsi), %cl
	cmpb	IID_IStreamGetSize+4(%rip), %cl
	jne	.LBB17_51
# BB#38:
	movb	5(%rsi), %cl
	cmpb	IID_IStreamGetSize+5(%rip), %cl
	jne	.LBB17_51
# BB#39:
	movb	6(%rsi), %cl
	cmpb	IID_IStreamGetSize+6(%rip), %cl
	jne	.LBB17_51
# BB#40:
	movb	7(%rsi), %cl
	cmpb	IID_IStreamGetSize+7(%rip), %cl
	jne	.LBB17_51
# BB#41:
	movb	8(%rsi), %cl
	cmpb	IID_IStreamGetSize+8(%rip), %cl
	jne	.LBB17_51
# BB#42:
	movb	9(%rsi), %cl
	cmpb	IID_IStreamGetSize+9(%rip), %cl
	jne	.LBB17_51
# BB#43:
	movb	10(%rsi), %cl
	cmpb	IID_IStreamGetSize+10(%rip), %cl
	jne	.LBB17_51
# BB#44:
	movb	11(%rsi), %cl
	cmpb	IID_IStreamGetSize+11(%rip), %cl
	jne	.LBB17_51
# BB#45:
	movb	12(%rsi), %cl
	cmpb	IID_IStreamGetSize+12(%rip), %cl
	jne	.LBB17_51
# BB#46:
	movb	13(%rsi), %cl
	cmpb	IID_IStreamGetSize+13(%rip), %cl
	jne	.LBB17_51
# BB#47:
	movb	14(%rsi), %cl
	cmpb	IID_IStreamGetSize+14(%rip), %cl
	jne	.LBB17_51
# BB#48:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_IStreamGetSize+15(%rip), %cl
	jne	.LBB17_51
# BB#49:
	leaq	8(%rdi), %rax
	movq	%rax, (%rdx)
.LBB17_50:                              # %_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv.exit
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB17_51:                              # %_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end17:
	.size	_ZThn8_N13CInFileStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end17-_ZThn8_N13CInFileStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N13CInFileStream6AddRefEv,"axG",@progbits,_ZThn8_N13CInFileStream6AddRefEv,comdat
	.weak	_ZThn8_N13CInFileStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N13CInFileStream6AddRefEv,@function
_ZThn8_N13CInFileStream6AddRefEv:       # @_ZThn8_N13CInFileStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end18:
	.size	_ZThn8_N13CInFileStream6AddRefEv, .Lfunc_end18-_ZThn8_N13CInFileStream6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N13CInFileStream7ReleaseEv,"axG",@progbits,_ZThn8_N13CInFileStream7ReleaseEv,comdat
	.weak	_ZThn8_N13CInFileStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N13CInFileStream7ReleaseEv,@function
_ZThn8_N13CInFileStream7ReleaseEv:      # @_ZThn8_N13CInFileStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB19_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB19_2:                               # %_ZN13CInFileStream7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end19:
	.size	_ZThn8_N13CInFileStream7ReleaseEv, .Lfunc_end19-_ZThn8_N13CInFileStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N13CInFileStreamD1Ev,"axG",@progbits,_ZThn8_N13CInFileStreamD1Ev,comdat
	.weak	_ZThn8_N13CInFileStreamD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N13CInFileStreamD1Ev,@function
_ZThn8_N13CInFileStreamD1Ev:            # @_ZThn8_N13CInFileStreamD1Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTV13CInFileStream+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTV13CInFileStream+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rdi)
	addq	$16, %rdi
	jmp	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev # TAILCALL
.Lfunc_end20:
	.size	_ZThn8_N13CInFileStreamD1Ev, .Lfunc_end20-_ZThn8_N13CInFileStreamD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N13CInFileStreamD0Ev,"axG",@progbits,_ZThn8_N13CInFileStreamD0Ev,comdat
	.weak	_ZThn8_N13CInFileStreamD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N13CInFileStreamD0Ev,@function
_ZThn8_N13CInFileStreamD0Ev:            # @_ZThn8_N13CInFileStreamD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi57:
	.cfi_def_cfa_offset 32
.Lcfi58:
	.cfi_offset %rbx, -24
.Lcfi59:
	.cfi_offset %r14, -16
	movl	$_ZTV13CInFileStream+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTV13CInFileStream+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rdi)
	leaq	-8(%rdi), %rbx
	addq	$16, %rdi
.Ltmp3:
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp4:
# BB#1:                                 # %_ZN13CInFileStreamD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB21_2:
.Ltmp5:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end21:
	.size	_ZThn8_N13CInFileStreamD0Ev, .Lfunc_end21-_ZThn8_N13CInFileStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Lfunc_end21-.Ltmp4     #   Call between .Ltmp4 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN16CStdInFileStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN16CStdInFileStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN16CStdInFileStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN16CStdInFileStream14QueryInterfaceERK4GUIDPPv,@function
_ZN16CStdInFileStream14QueryInterfaceERK4GUIDPPv: # @_ZN16CStdInFileStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB22_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB22_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB22_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB22_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB22_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB22_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB22_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB22_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB22_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB22_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB22_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB22_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB22_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB22_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB22_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB22_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB22_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end22:
	.size	_ZN16CStdInFileStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end22-_ZN16CStdInFileStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN16CStdInFileStream6AddRefEv,"axG",@progbits,_ZN16CStdInFileStream6AddRefEv,comdat
	.weak	_ZN16CStdInFileStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN16CStdInFileStream6AddRefEv,@function
_ZN16CStdInFileStream6AddRefEv:         # @_ZN16CStdInFileStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end23:
	.size	_ZN16CStdInFileStream6AddRefEv, .Lfunc_end23-_ZN16CStdInFileStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN16CStdInFileStream7ReleaseEv,"axG",@progbits,_ZN16CStdInFileStream7ReleaseEv,comdat
	.weak	_ZN16CStdInFileStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN16CStdInFileStream7ReleaseEv,@function
_ZN16CStdInFileStream7ReleaseEv:        # @_ZN16CStdInFileStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi61:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB24_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB24_2:
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZN16CStdInFileStream7ReleaseEv, .Lfunc_end24-_ZN16CStdInFileStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN16CStdInFileStreamD0Ev,"axG",@progbits,_ZN16CStdInFileStreamD0Ev,comdat
	.weak	_ZN16CStdInFileStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN16CStdInFileStreamD0Ev,@function
_ZN16CStdInFileStreamD0Ev:              # @_ZN16CStdInFileStreamD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end25:
	.size	_ZN16CStdInFileStreamD0Ev, .Lfunc_end25-_ZN16CStdInFileStreamD0Ev
	.cfi_endproc

	.section	.text._ZN14COutFileStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN14COutFileStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN14COutFileStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN14COutFileStream14QueryInterfaceERK4GUIDPPv,@function
_ZN14COutFileStream14QueryInterfaceERK4GUIDPPv: # @_ZN14COutFileStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi62:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB26_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB26_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB26_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB26_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB26_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB26_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB26_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB26_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB26_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB26_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB26_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB26_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB26_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB26_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB26_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB26_32
.LBB26_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IOutStream(%rip), %cl
	jne	.LBB26_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_IOutStream+1(%rip), %cl
	jne	.LBB26_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_IOutStream+2(%rip), %cl
	jne	.LBB26_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_IOutStream+3(%rip), %cl
	jne	.LBB26_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_IOutStream+4(%rip), %cl
	jne	.LBB26_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_IOutStream+5(%rip), %cl
	jne	.LBB26_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_IOutStream+6(%rip), %cl
	jne	.LBB26_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_IOutStream+7(%rip), %cl
	jne	.LBB26_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_IOutStream+8(%rip), %cl
	jne	.LBB26_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_IOutStream+9(%rip), %cl
	jne	.LBB26_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_IOutStream+10(%rip), %cl
	jne	.LBB26_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_IOutStream+11(%rip), %cl
	jne	.LBB26_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_IOutStream+12(%rip), %cl
	jne	.LBB26_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_IOutStream+13(%rip), %cl
	jne	.LBB26_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_IOutStream+14(%rip), %cl
	jne	.LBB26_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit6
	movb	15(%rsi), %cl
	cmpb	IID_IOutStream+15(%rip), %cl
	jne	.LBB26_33
.LBB26_32:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB26_33:                              # %_ZeqRK4GUIDS1_.exit6.thread
	popq	%rcx
	retq
.Lfunc_end26:
	.size	_ZN14COutFileStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end26-_ZN14COutFileStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN14COutFileStream6AddRefEv,"axG",@progbits,_ZN14COutFileStream6AddRefEv,comdat
	.weak	_ZN14COutFileStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN14COutFileStream6AddRefEv,@function
_ZN14COutFileStream6AddRefEv:           # @_ZN14COutFileStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end27:
	.size	_ZN14COutFileStream6AddRefEv, .Lfunc_end27-_ZN14COutFileStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN14COutFileStream7ReleaseEv,"axG",@progbits,_ZN14COutFileStream7ReleaseEv,comdat
	.weak	_ZN14COutFileStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN14COutFileStream7ReleaseEv,@function
_ZN14COutFileStream7ReleaseEv:          # @_ZN14COutFileStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi63:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB28_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB28_2:
	popq	%rcx
	retq
.Lfunc_end28:
	.size	_ZN14COutFileStream7ReleaseEv, .Lfunc_end28-_ZN14COutFileStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN14COutFileStreamD2Ev,"axG",@progbits,_ZN14COutFileStreamD2Ev,comdat
	.weak	_ZN14COutFileStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN14COutFileStreamD2Ev,@function
_ZN14COutFileStreamD2Ev:                # @_ZN14COutFileStreamD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV14COutFileStream+16, (%rdi)
	addq	$16, %rdi
	jmp	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev # TAILCALL
.Lfunc_end29:
	.size	_ZN14COutFileStreamD2Ev, .Lfunc_end29-_ZN14COutFileStreamD2Ev
	.cfi_endproc

	.section	.text._ZN14COutFileStreamD0Ev,"axG",@progbits,_ZN14COutFileStreamD0Ev,comdat
	.weak	_ZN14COutFileStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN14COutFileStreamD0Ev,@function
_ZN14COutFileStreamD0Ev:                # @_ZN14COutFileStreamD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi66:
	.cfi_def_cfa_offset 32
.Lcfi67:
	.cfi_offset %rbx, -24
.Lcfi68:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV14COutFileStream+16, (%rbx)
	leaq	16(%rbx), %rdi
.Ltmp6:
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp7:
# BB#1:                                 # %_ZN14COutFileStreamD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB30_2:
.Ltmp8:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end30:
	.size	_ZN14COutFileStreamD0Ev, .Lfunc_end30-_ZN14COutFileStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin2    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin2    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin2    # >> Call Site 2 <<
	.long	.Lfunc_end30-.Ltmp7     #   Call between .Ltmp7 and .Lfunc_end30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN17CStdOutFileStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN17CStdOutFileStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN17CStdOutFileStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN17CStdOutFileStream14QueryInterfaceERK4GUIDPPv,@function
_ZN17CStdOutFileStream14QueryInterfaceERK4GUIDPPv: # @_ZN17CStdOutFileStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi69:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB31_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB31_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB31_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB31_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB31_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB31_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB31_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB31_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB31_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB31_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB31_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB31_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB31_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB31_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB31_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB31_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB31_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end31:
	.size	_ZN17CStdOutFileStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end31-_ZN17CStdOutFileStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN17CStdOutFileStream6AddRefEv,"axG",@progbits,_ZN17CStdOutFileStream6AddRefEv,comdat
	.weak	_ZN17CStdOutFileStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN17CStdOutFileStream6AddRefEv,@function
_ZN17CStdOutFileStream6AddRefEv:        # @_ZN17CStdOutFileStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end32:
	.size	_ZN17CStdOutFileStream6AddRefEv, .Lfunc_end32-_ZN17CStdOutFileStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN17CStdOutFileStream7ReleaseEv,"axG",@progbits,_ZN17CStdOutFileStream7ReleaseEv,comdat
	.weak	_ZN17CStdOutFileStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN17CStdOutFileStream7ReleaseEv,@function
_ZN17CStdOutFileStream7ReleaseEv:       # @_ZN17CStdOutFileStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi70:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB33_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB33_2:
	popq	%rcx
	retq
.Lfunc_end33:
	.size	_ZN17CStdOutFileStream7ReleaseEv, .Lfunc_end33-_ZN17CStdOutFileStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN17CStdOutFileStreamD0Ev,"axG",@progbits,_ZN17CStdOutFileStreamD0Ev,comdat
	.weak	_ZN17CStdOutFileStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN17CStdOutFileStreamD0Ev,@function
_ZN17CStdOutFileStreamD0Ev:             # @_ZN17CStdOutFileStreamD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end34:
	.size	_ZN17CStdOutFileStreamD0Ev, .Lfunc_end34-_ZN17CStdOutFileStreamD0Ev
	.cfi_endproc

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB35_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB35_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB35_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB35_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB35_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB35_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB35_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB35_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB35_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB35_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB35_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB35_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB35_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB35_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB35_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB35_16:
	xorl	%eax, %eax
	retq
.Lfunc_end35:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end35-_ZeqRK4GUIDS1_
	.cfi_endproc

	.section	.text._ZN8IUnknownD2Ev,"axG",@progbits,_ZN8IUnknownD2Ev,comdat
	.weak	_ZN8IUnknownD2Ev
	.p2align	4, 0x90
	.type	_ZN8IUnknownD2Ev,@function
_ZN8IUnknownD2Ev:                       # @_ZN8IUnknownD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end36:
	.size	_ZN8IUnknownD2Ev, .Lfunc_end36-_ZN8IUnknownD2Ev
	.cfi_endproc

	.type	_ZTV13CInFileStream,@object # @_ZTV13CInFileStream
	.section	.rodata,"a",@progbits
	.globl	_ZTV13CInFileStream
	.p2align	3
_ZTV13CInFileStream:
	.quad	0
	.quad	_ZTI13CInFileStream
	.quad	_ZN13CInFileStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN13CInFileStream6AddRefEv
	.quad	_ZN13CInFileStream7ReleaseEv
	.quad	_ZN13CInFileStreamD2Ev
	.quad	_ZN13CInFileStreamD0Ev
	.quad	_ZN13CInFileStream4ReadEPvjPj
	.quad	_ZN13CInFileStream4SeekExjPy
	.quad	_ZN13CInFileStream7GetSizeEPy
	.quad	-8
	.quad	_ZTI13CInFileStream
	.quad	_ZThn8_N13CInFileStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N13CInFileStream6AddRefEv
	.quad	_ZThn8_N13CInFileStream7ReleaseEv
	.quad	_ZThn8_N13CInFileStreamD1Ev
	.quad	_ZThn8_N13CInFileStreamD0Ev
	.quad	_ZThn8_N13CInFileStream7GetSizeEPy
	.size	_ZTV13CInFileStream, 144

	.type	_ZTS13CInFileStream,@object # @_ZTS13CInFileStream
	.globl	_ZTS13CInFileStream
_ZTS13CInFileStream:
	.asciz	"13CInFileStream"
	.size	_ZTS13CInFileStream, 16

	.type	_ZTS9IInStream,@object  # @_ZTS9IInStream
	.section	.rodata._ZTS9IInStream,"aG",@progbits,_ZTS9IInStream,comdat
	.weak	_ZTS9IInStream
_ZTS9IInStream:
	.asciz	"9IInStream"
	.size	_ZTS9IInStream, 11

	.type	_ZTS19ISequentialInStream,@object # @_ZTS19ISequentialInStream
	.section	.rodata._ZTS19ISequentialInStream,"aG",@progbits,_ZTS19ISequentialInStream,comdat
	.weak	_ZTS19ISequentialInStream
	.p2align	4
_ZTS19ISequentialInStream:
	.asciz	"19ISequentialInStream"
	.size	_ZTS19ISequentialInStream, 22

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI19ISequentialInStream,@object # @_ZTI19ISequentialInStream
	.section	.rodata._ZTI19ISequentialInStream,"aG",@progbits,_ZTI19ISequentialInStream,comdat
	.weak	_ZTI19ISequentialInStream
	.p2align	4
_ZTI19ISequentialInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ISequentialInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19ISequentialInStream, 24

	.type	_ZTI9IInStream,@object  # @_ZTI9IInStream
	.section	.rodata._ZTI9IInStream,"aG",@progbits,_ZTI9IInStream,comdat
	.weak	_ZTI9IInStream
	.p2align	4
_ZTI9IInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS9IInStream
	.quad	_ZTI19ISequentialInStream
	.size	_ZTI9IInStream, 24

	.type	_ZTS14IStreamGetSize,@object # @_ZTS14IStreamGetSize
	.section	.rodata._ZTS14IStreamGetSize,"aG",@progbits,_ZTS14IStreamGetSize,comdat
	.weak	_ZTS14IStreamGetSize
	.p2align	4
_ZTS14IStreamGetSize:
	.asciz	"14IStreamGetSize"
	.size	_ZTS14IStreamGetSize, 17

	.type	_ZTI14IStreamGetSize,@object # @_ZTI14IStreamGetSize
	.section	.rodata._ZTI14IStreamGetSize,"aG",@progbits,_ZTI14IStreamGetSize,comdat
	.weak	_ZTI14IStreamGetSize
	.p2align	4
_ZTI14IStreamGetSize:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14IStreamGetSize
	.quad	_ZTI8IUnknown
	.size	_ZTI14IStreamGetSize, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI13CInFileStream,@object # @_ZTI13CInFileStream
	.section	.rodata,"a",@progbits
	.globl	_ZTI13CInFileStream
	.p2align	4
_ZTI13CInFileStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS13CInFileStream
	.long	1                       # 0x1
	.long	3                       # 0x3
	.quad	_ZTI9IInStream
	.quad	2                       # 0x2
	.quad	_ZTI14IStreamGetSize
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.size	_ZTI13CInFileStream, 72

	.type	_ZTV16CStdInFileStream,@object # @_ZTV16CStdInFileStream
	.globl	_ZTV16CStdInFileStream
	.p2align	3
_ZTV16CStdInFileStream:
	.quad	0
	.quad	_ZTI16CStdInFileStream
	.quad	_ZN16CStdInFileStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16CStdInFileStream6AddRefEv
	.quad	_ZN16CStdInFileStream7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN16CStdInFileStreamD0Ev
	.quad	_ZN16CStdInFileStream4ReadEPvjPj
	.size	_ZTV16CStdInFileStream, 64

	.type	_ZTS16CStdInFileStream,@object # @_ZTS16CStdInFileStream
	.globl	_ZTS16CStdInFileStream
	.p2align	4
_ZTS16CStdInFileStream:
	.asciz	"16CStdInFileStream"
	.size	_ZTS16CStdInFileStream, 19

	.type	_ZTI16CStdInFileStream,@object # @_ZTI16CStdInFileStream
	.globl	_ZTI16CStdInFileStream
	.p2align	4
_ZTI16CStdInFileStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS16CStdInFileStream
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI19ISequentialInStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI16CStdInFileStream, 56

	.type	_ZTV14COutFileStream,@object # @_ZTV14COutFileStream
	.globl	_ZTV14COutFileStream
	.p2align	3
_ZTV14COutFileStream:
	.quad	0
	.quad	_ZTI14COutFileStream
	.quad	_ZN14COutFileStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN14COutFileStream6AddRefEv
	.quad	_ZN14COutFileStream7ReleaseEv
	.quad	_ZN14COutFileStreamD2Ev
	.quad	_ZN14COutFileStreamD0Ev
	.quad	_ZN14COutFileStream5WriteEPKvjPj
	.quad	_ZN14COutFileStream4SeekExjPy
	.quad	_ZN14COutFileStream7SetSizeEy
	.size	_ZTV14COutFileStream, 80

	.type	_ZTS14COutFileStream,@object # @_ZTS14COutFileStream
	.globl	_ZTS14COutFileStream
	.p2align	4
_ZTS14COutFileStream:
	.asciz	"14COutFileStream"
	.size	_ZTS14COutFileStream, 17

	.type	_ZTS10IOutStream,@object # @_ZTS10IOutStream
	.section	.rodata._ZTS10IOutStream,"aG",@progbits,_ZTS10IOutStream,comdat
	.weak	_ZTS10IOutStream
_ZTS10IOutStream:
	.asciz	"10IOutStream"
	.size	_ZTS10IOutStream, 13

	.type	_ZTS20ISequentialOutStream,@object # @_ZTS20ISequentialOutStream
	.section	.rodata._ZTS20ISequentialOutStream,"aG",@progbits,_ZTS20ISequentialOutStream,comdat
	.weak	_ZTS20ISequentialOutStream
	.p2align	4
_ZTS20ISequentialOutStream:
	.asciz	"20ISequentialOutStream"
	.size	_ZTS20ISequentialOutStream, 23

	.type	_ZTI20ISequentialOutStream,@object # @_ZTI20ISequentialOutStream
	.section	.rodata._ZTI20ISequentialOutStream,"aG",@progbits,_ZTI20ISequentialOutStream,comdat
	.weak	_ZTI20ISequentialOutStream
	.p2align	4
_ZTI20ISequentialOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ISequentialOutStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ISequentialOutStream, 24

	.type	_ZTI10IOutStream,@object # @_ZTI10IOutStream
	.section	.rodata._ZTI10IOutStream,"aG",@progbits,_ZTI10IOutStream,comdat
	.weak	_ZTI10IOutStream
	.p2align	4
_ZTI10IOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10IOutStream
	.quad	_ZTI20ISequentialOutStream
	.size	_ZTI10IOutStream, 24

	.type	_ZTI14COutFileStream,@object # @_ZTI14COutFileStream
	.section	.rodata,"a",@progbits
	.globl	_ZTI14COutFileStream
	.p2align	4
_ZTI14COutFileStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS14COutFileStream
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI10IOutStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI14COutFileStream, 56

	.type	_ZTV17CStdOutFileStream,@object # @_ZTV17CStdOutFileStream
	.globl	_ZTV17CStdOutFileStream
	.p2align	3
_ZTV17CStdOutFileStream:
	.quad	0
	.quad	_ZTI17CStdOutFileStream
	.quad	_ZN17CStdOutFileStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN17CStdOutFileStream6AddRefEv
	.quad	_ZN17CStdOutFileStream7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN17CStdOutFileStreamD0Ev
	.quad	_ZN17CStdOutFileStream5WriteEPKvjPj
	.size	_ZTV17CStdOutFileStream, 64

	.type	_ZTS17CStdOutFileStream,@object # @_ZTS17CStdOutFileStream
	.globl	_ZTS17CStdOutFileStream
	.p2align	4
_ZTS17CStdOutFileStream:
	.asciz	"17CStdOutFileStream"
	.size	_ZTS17CStdOutFileStream, 20

	.type	_ZTI17CStdOutFileStream,@object # @_ZTI17CStdOutFileStream
	.globl	_ZTI17CStdOutFileStream
	.p2align	4
_ZTI17CStdOutFileStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS17CStdOutFileStream
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI20ISequentialOutStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI17CStdOutFileStream, 56


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
