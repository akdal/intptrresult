	.text
	.file	"wrgif.bc"
	.globl	jinit_write_gif
	.p2align	4, 0x90
	.type	jinit_write_gif,@function
jinit_write_gif:                        # @jinit_write_gif
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$384, %edx              # imm = 0x180
	callq	*(%rax)
	movq	%rax, %r14
	movq	%rbx, 48(%r14)
	movq	$start_output_gif, (%r14)
	movq	$put_pixel_rows, 8(%r14)
	movq	$finish_output_gif, 16(%r14)
	movl	56(%rbx), %eax
	leal	-1(%rax), %ecx
	cmpl	$2, %ecx
	jb	.LBB0_2
# BB#1:
	movq	(%rbx), %rax
	movl	$1014, 40(%rax)         # imm = 0x3F6
	movq	%rbx, %rdi
	callq	*(%rax)
	movl	56(%rbx), %eax
.LBB0_2:
	cmpl	$1, %eax
	jne	.LBB0_4
# BB#3:
	cmpl	$9, 288(%rbx)
	jl	.LBB0_6
.LBB0_4:
	movl	$1, 100(%rbx)
	cmpl	$257, 112(%rbx)         # imm = 0x101
	jl	.LBB0_6
# BB#5:
	movl	$256, 112(%rbx)         # imm = 0x100
.LBB0_6:
	movq	%rbx, %rdi
	callq	jpeg_calc_output_dimensions
	cmpl	$1, 140(%rbx)
	je	.LBB0_8
# BB#7:
	movq	(%rbx), %rax
	movl	$1012, 40(%rax)         # imm = 0x3F4
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB0_8:
	movq	8(%rbx), %rax
	movl	128(%rbx), %edx
	movl	$1, %esi
	movl	$1, %ecx
	movq	%rbx, %rdi
	callq	*16(%rax)
	movq	%rax, 32(%r14)
	movl	$1, 40(%r14)
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$10006, %edx            # imm = 0x2716
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, 104(%r14)
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$40024, %edx            # imm = 0x9C58
	movq	%rbx, %rdi
	callq	*8(%rax)
	movq	%rax, 112(%r14)
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	jinit_write_gif, .Lfunc_end0-jinit_write_gif
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_output_gif,@function
start_output_gif:                       # @start_output_gif
	.cfi_startproc
# BB#0:
	movq	%rsi, %rax
	cmpl	$0, 100(%rdi)
	je	.LBB1_2
# BB#1:
	movl	148(%rdi), %esi
	movq	152(%rdi), %rdx
	movq	%rax, %rdi
	jmp	emit_header             # TAILCALL
.LBB1_2:
	movl	$256, %esi              # imm = 0x100
	xorl	%edx, %edx
	movq	%rax, %rdi
	jmp	emit_header             # TAILCALL
.Lfunc_end1:
	.size	start_output_gif, .Lfunc_end1-start_output_gif
	.cfi_endproc

	.p2align	4, 0x90
	.type	put_pixel_rows,@function
put_pixel_rows:                         # @put_pixel_rows
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 64
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	128(%rdi), %r15d
	testl	%r15d, %r15d
	je	.LBB2_36
# BB#1:                                 # %.lr.ph
	movq	32(%rbx), %rax
	movq	(%rax), %r14
	leaq	124(%rbx), %rbp
	.p2align	4, 0x90
.LBB2_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_8 Depth 2
                                        #     Child Loop BB2_12 Depth 2
                                        #     Child Loop BB2_25 Depth 2
	movzbl	(%r14), %eax
	cmpl	$0, 88(%rbx)
	je	.LBB2_4
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	movw	%ax, 84(%rbx)
	movl	$0, 88(%rbx)
	jmp	.LBB2_35
	.p2align	4, 0x90
.LBB2_4:                                #   in Loop: Header=BB2_2 Depth=1
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movzwl	%ax, %r13d
	movl	%r13d, %ecx
	shll	$4, %ecx
	movswq	84(%rbx), %rax
	leal	(%rax,%rcx), %edx
	cmpl	$5002, %edx             # imm = 0x138A
	leal	-5003(%rax,%rcx), %r12d
	cmovlel	%edx, %r12d
	movq	%rax, %rcx
	shlq	$8, %rcx
	orq	%rcx, %r13
	movq	104(%rbx), %rcx
	movslq	%r12d, %rdi
	movzwl	(%rcx,%rdi,2), %esi
	testw	%si, %si
	je	.LBB2_10
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	112(%rbx), %rdx
	cmpq	%r13, (%rdx,%rdi,8)
	jne	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_2 Depth=1
	movw	%si, 84(%rbx)
	jmp	.LBB2_35
.LBB2_7:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%rbp, %r8
	movl	$5003, %esi             # imm = 0x138B
	subl	%r12d, %esi
	testl	%r12d, %r12d
	movl	$1, %edi
	cmovel	%edi, %esi
	.p2align	4, 0x90
.LBB2_8:                                #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r12d, %edi
	subl	%esi, %edi
	addl	$5003, %edi             # imm = 0x138B
	subl	%esi, %r12d
	cmovsl	%edi, %r12d
	movslq	%r12d, %rbp
	movzwl	(%rcx,%rbp,2), %edi
	testw	%di, %di
	je	.LBB2_9
# BB#21:                                #   in Loop: Header=BB2_8 Depth=2
	cmpq	%r13, (%rdx,%rbp,8)
	jne	.LBB2_8
# BB#22:                                #   in Loop: Header=BB2_2 Depth=1
	movw	%di, 84(%rbx)
	movq	%r8, %rbp
	jmp	.LBB2_35
.LBB2_9:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%r8, %rbp
.LBB2_10:                               # %.loopexit.i
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	80(%rbx), %ecx
	shlq	%cl, %rax
	orq	72(%rbx), %rax
	movq	%rax, 72(%rbx)
	addl	56(%rbx), %ecx
	movl	%ecx, 80(%rbx)
	cmpl	$8, %ecx
	jl	.LBB2_17
# BB#11:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	120(%rbx), %ecx
	.p2align	4, 0x90
.LBB2_12:                               #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ecx, %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 120(%rbx)
	movb	%al, 125(%rbx,%rcx)
	movslq	120(%rbx), %rcx
	cmpq	$255, %rcx
	jl	.LBB2_16
# BB#13:                                #   in Loop: Header=BB2_12 Depth=2
	movq	%rcx, %rdx
	incq	%rdx
	movl	%edx, 120(%rbx)
	movb	%cl, 124(%rbx)
	movq	24(%rbx), %rcx
	movl	$1, %esi
	movq	%rbp, %rdi
	callq	fwrite
	movslq	120(%rbx), %rcx
	cmpq	%rcx, %rax
	je	.LBB2_15
# BB#14:                                #   in Loop: Header=BB2_12 Depth=2
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$36, 40(%rax)
	callq	*(%rax)
.LBB2_15:                               # %flush_packet.exit.i
                                        #   in Loop: Header=BB2_12 Depth=2
	movl	$0, 120(%rbx)
	xorl	%ecx, %ecx
.LBB2_16:                               #   in Loop: Header=BB2_12 Depth=2
	movq	72(%rbx), %rax
	sarq	$8, %rax
	movq	%rax, 72(%rbx)
	movl	80(%rbx), %edx
	addl	$-8, %edx
	movl	%edx, 80(%rbx)
	cmpl	$7, %edx
	jg	.LBB2_12
.LBB2_17:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB2_2 Depth=1
	movw	96(%rbx), %ax
	cmpw	60(%rbx), %ax
	jle	.LBB2_19
# BB#18:                                #   in Loop: Header=BB2_2 Depth=1
	movl	56(%rbx), %ecx
	incl	%ecx
	movl	%ecx, 56(%rbx)
	movl	$1, %edx
	shll	%cl, %edx
	addl	$65535, %edx            # imm = 0xFFFF
	cmpl	$12, %ecx
	movw	$4096, %cx              # imm = 0x1000
	cmovew	%cx, %dx
	movw	%dx, 60(%rbx)
.LBB2_19:                               # %output.exit.i
                                        #   in Loop: Header=BB2_2 Depth=1
	movswl	%ax, %ecx
	cmpl	$4095, %ecx             # imm = 0xFFF
	jg	.LBB2_23
# BB#20:                                #   in Loop: Header=BB2_2 Depth=1
	leal	1(%rax), %ecx
	movw	%cx, 96(%rbx)
	movq	104(%rbx), %rcx
	movslq	%r12d, %rdx
	movw	%ax, (%rcx,%rdx,2)
	movq	112(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
	jmp	.LBB2_34
.LBB2_23:                               #   in Loop: Header=BB2_2 Depth=1
	movq	104(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10006, %edx            # imm = 0x2716
	callq	memset
	movswq	92(%rbx), %rax
	leal	2(%rax), %edx
	movw	%dx, 96(%rbx)
	movl	80(%rbx), %ecx
	shlq	%cl, %rax
	orq	72(%rbx), %rax
	movq	%rax, 72(%rbx)
	addl	56(%rbx), %ecx
	movl	%ecx, 80(%rbx)
	cmpl	$8, %ecx
	jl	.LBB2_31
# BB#24:                                # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	120(%rbx), %ecx
	.p2align	4, 0x90
.LBB2_25:                               #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ecx, %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 120(%rbx)
	movb	%al, 125(%rbx,%rcx)
	movslq	120(%rbx), %rcx
	cmpq	$255, %rcx
	jl	.LBB2_29
# BB#26:                                #   in Loop: Header=BB2_25 Depth=2
	movq	%rcx, %rdx
	incq	%rdx
	movl	%edx, 120(%rbx)
	movb	%cl, 124(%rbx)
	movq	24(%rbx), %rcx
	movl	$1, %esi
	movq	%rbp, %rdi
	callq	fwrite
	movslq	120(%rbx), %rcx
	cmpq	%rcx, %rax
	je	.LBB2_28
# BB#27:                                #   in Loop: Header=BB2_25 Depth=2
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$36, 40(%rax)
	callq	*(%rax)
.LBB2_28:                               # %flush_packet.exit.i.i
                                        #   in Loop: Header=BB2_25 Depth=2
	movl	$0, 120(%rbx)
	xorl	%ecx, %ecx
.LBB2_29:                               #   in Loop: Header=BB2_25 Depth=2
	movq	72(%rbx), %rax
	sarq	$8, %rax
	movq	%rax, 72(%rbx)
	movl	80(%rbx), %edx
	addl	$-8, %edx
	movl	%edx, 80(%rbx)
	cmpl	$7, %edx
	jg	.LBB2_25
# BB#30:                                # %._crit_edge.i.loopexit.i.i
                                        #   in Loop: Header=BB2_2 Depth=1
	movw	96(%rbx), %dx
.LBB2_31:                               # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=1
	cmpw	60(%rbx), %dx
	jle	.LBB2_33
# BB#32:                                #   in Loop: Header=BB2_2 Depth=1
	movl	56(%rbx), %ecx
	incl	%ecx
	movl	%ecx, 56(%rbx)
	movl	$1, %eax
	shll	%cl, %eax
	addl	$65535, %eax            # imm = 0xFFFF
	cmpl	$12, %ecx
	movw	$4096, %cx              # imm = 0x1000
	cmovew	%cx, %ax
	movw	%ax, 60(%rbx)
.LBB2_33:                               # %clear_block.exit.i
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	64(%rbx), %ecx
	movl	%ecx, 56(%rbx)
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	addl	$65535, %eax            # imm = 0xFFFF
	movw	%ax, 60(%rbx)
.LBB2_34:                               #   in Loop: Header=BB2_2 Depth=1
	movl	4(%rsp), %eax           # 4-byte Reload
	movw	%ax, 84(%rbx)
	.p2align	4, 0x90
.LBB2_35:                               # %compress_byte.exit
                                        #   in Loop: Header=BB2_2 Depth=1
	incq	%r14
	decl	%r15d
	jne	.LBB2_2
.LBB2_36:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	put_pixel_rows, .Lfunc_end2-put_pixel_rows
	.cfi_endproc

	.p2align	4, 0x90
	.type	finish_output_gif,@function
finish_output_gif:                      # @finish_output_gif
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 48
.Lcfi23:
	.cfi_offset %rbx, -48
.Lcfi24:
	.cfi_offset %r12, -40
.Lcfi25:
	.cfi_offset %r13, -32
.Lcfi26:
	.cfi_offset %r14, -24
.Lcfi27:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	cmpl	$0, 88(%rbx)
	je	.LBB3_2
# BB#1:                                 # %.output.exit_crit_edge.i
	leaq	80(%rbx), %r12
	movl	80(%rbx), %eax
	leaq	72(%rbx), %r13
	movq	72(%rbx), %rdx
	jmp	.LBB3_11
.LBB3_2:
	movswq	84(%rbx), %rdx
	leaq	80(%rbx), %r12
	movl	80(%rbx), %eax
	movl	%eax, %ecx
	shlq	%cl, %rdx
	leaq	72(%rbx), %r13
	orq	72(%rbx), %rdx
	movq	%rdx, 72(%rbx)
	addl	56(%rbx), %eax
	movl	%eax, 80(%rbx)
	cmpl	$8, %eax
	jl	.LBB3_9
# BB#3:                                 # %.lr.ph.i.i
	leaq	124(%rbx), %r15
	movl	120(%rbx), %ecx
	.p2align	4, 0x90
.LBB3_4:                                # =>This Inner Loop Header: Depth=1
	movslq	%ecx, %rax
	leaq	1(%rax), %rcx
	movl	%ecx, 120(%rbx)
	movb	%dl, 125(%rbx,%rax)
	movslq	120(%rbx), %rcx
	cmpq	$255, %rcx
	jl	.LBB3_8
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=1
	movq	%rcx, %rdx
	incq	%rdx
	movl	%edx, 120(%rbx)
	movb	%cl, 124(%rbx)
	movq	24(%rbx), %rcx
	movl	$1, %esi
	movq	%r15, %rdi
	callq	fwrite
	movslq	120(%rbx), %rcx
	cmpq	%rcx, %rax
	je	.LBB3_7
# BB#6:                                 #   in Loop: Header=BB3_4 Depth=1
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$36, 40(%rax)
	callq	*(%rax)
.LBB3_7:                                # %flush_packet.exit.i
                                        #   in Loop: Header=BB3_4 Depth=1
	movl	$0, 120(%rbx)
	xorl	%ecx, %ecx
.LBB3_8:                                #   in Loop: Header=BB3_4 Depth=1
	movq	(%r13), %rdx
	sarq	$8, %rdx
	movq	%rdx, (%r13)
	movl	(%r12), %eax
	addl	$-8, %eax
	movl	%eax, (%r12)
	cmpl	$7, %eax
	jg	.LBB3_4
.LBB3_9:                                # %._crit_edge.i.i
	movzwl	96(%rbx), %ecx
	cmpw	60(%rbx), %cx
	jle	.LBB3_11
# BB#10:
	movl	56(%rbx), %ecx
	incl	%ecx
	movl	%ecx, 56(%rbx)
	movl	$1, %esi
	shll	%cl, %esi
	addl	$65535, %esi            # imm = 0xFFFF
	cmpl	$12, %ecx
	movw	$4096, %cx              # imm = 0x1000
	cmovnew	%si, %cx
	movw	%cx, 60(%rbx)
.LBB3_11:                               # %output.exit.i
	movswq	94(%rbx), %rsi
	movl	%eax, %ecx
	shlq	%cl, %rsi
	orq	%rdx, %rsi
	movq	%rsi, 72(%rbx)
	addl	56(%rbx), %eax
	movl	%eax, 80(%rbx)
	cmpl	$8, %eax
	jl	.LBB3_18
# BB#12:                                # %.lr.ph.i12.i
	leaq	124(%rbx), %r15
	movl	120(%rbx), %ecx
	.p2align	4, 0x90
.LBB3_13:                               # =>This Inner Loop Header: Depth=1
	movslq	%ecx, %rax
	leaq	1(%rax), %rcx
	movl	%ecx, 120(%rbx)
	movb	%sil, 125(%rbx,%rax)
	movslq	120(%rbx), %rcx
	cmpq	$255, %rcx
	jl	.LBB3_17
# BB#14:                                #   in Loop: Header=BB3_13 Depth=1
	movq	%rcx, %rdx
	incq	%rdx
	movl	%edx, 120(%rbx)
	movb	%cl, 124(%rbx)
	movq	24(%rbx), %rcx
	movl	$1, %esi
	movq	%r15, %rdi
	callq	fwrite
	movslq	120(%rbx), %rcx
	cmpq	%rcx, %rax
	je	.LBB3_16
# BB#15:                                #   in Loop: Header=BB3_13 Depth=1
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$36, 40(%rax)
	callq	*(%rax)
.LBB3_16:                               # %flush_packet.exit16.i
                                        #   in Loop: Header=BB3_13 Depth=1
	movl	$0, 120(%rbx)
	xorl	%ecx, %ecx
.LBB3_17:                               #   in Loop: Header=BB3_13 Depth=1
	movq	(%r13), %rsi
	sarq	$8, %rsi
	movq	%rsi, (%r13)
	movl	(%r12), %eax
	addl	$-8, %eax
	movl	%eax, (%r12)
	cmpl	$7, %eax
	jg	.LBB3_13
.LBB3_18:                               # %._crit_edge.i13.i
	movzwl	96(%rbx), %ecx
	cmpw	60(%rbx), %cx
	jle	.LBB3_20
# BB#19:
	movl	56(%rbx), %ecx
	incl	%ecx
	movl	%ecx, 56(%rbx)
	movl	$1, %edx
	shll	%cl, %edx
	addl	$65535, %edx            # imm = 0xFFFF
	cmpl	$12, %ecx
	movw	$4096, %cx              # imm = 0x1000
	cmovnew	%dx, %cx
	movw	%cx, 60(%rbx)
.LBB3_20:                               # %output.exit15.i
	leaq	120(%rbx), %r15
	testl	%eax, %eax
	jle	.LBB3_21
# BB#24:
	movslq	120(%rbx), %rax
	leaq	1(%rax), %rcx
	movl	%ecx, 120(%rbx)
	movb	%sil, 125(%rbx,%rax)
	movslq	120(%rbx), %rax
	cmpq	$255, %rax
	jl	.LBB3_22
# BB#25:
	movq	%rax, %rdx
	incq	%rdx
	movl	%edx, 120(%rbx)
	leaq	124(%rbx), %rdi
	movb	%al, 124(%rbx)
	jmp	.LBB3_26
.LBB3_21:                               # %output.exit15._crit_edge.i
	movl	120(%rbx), %eax
.LBB3_22:
	testl	%eax, %eax
	jle	.LBB3_29
# BB#23:
	leal	1(%rax), %ecx
	movl	%ecx, 120(%rbx)
	leaq	124(%rbx), %rdi
	movb	%al, 124(%rbx)
	movslq	%ecx, %rdx
.LBB3_26:
	movq	24(%rbx), %rcx
	movl	$1, %esi
	callq	fwrite
	movslq	120(%rbx), %rcx
	cmpq	%rcx, %rax
	je	.LBB3_28
# BB#27:
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$36, 40(%rax)
	callq	*(%rax)
.LBB3_28:                               # %flush_packet.exit18.sink.split.i
	movl	$0, (%r15)
.LBB3_29:                               # %compress_term.exit
	movq	24(%rbx), %rsi
	xorl	%edi, %edi
	callq	_IO_putc
	movq	24(%rbx), %rsi
	movl	$59, %edi
	callq	_IO_putc
	movq	24(%rbx), %rdi
	callq	fflush
	movq	24(%rbx), %rdi
	callq	ferror
	testl	%eax, %eax
	je	.LBB3_30
# BB#31:
	movq	(%r14), %rax
	movl	$36, 40(%rax)
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmpq	*(%rax)                 # TAILCALL
.LBB3_30:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	finish_output_gif, .Lfunc_end3-finish_output_gif
	.cfi_endproc

	.p2align	4, 0x90
	.type	emit_header,@function
emit_header:                            # @emit_header
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 96
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%esi, %r13d
	movq	%rdi, %r12
	movq	48(%r12), %rdi
	movl	288(%rdi), %ebx
	cmpl	$257, %r13d             # imm = 0x101
	jl	.LBB4_2
# BB#1:
	movq	(%rdi), %rax
	movl	$1039, 40(%rax)         # imm = 0x40F
	movl	%r13d, 44(%rax)
	callq	*(%rax)
.LBB4_2:                                # %.preheader.preheader
	addl	$-8, %ebx
	movl	%ebx, 20(%rsp)          # 4-byte Spill
	movl	$1, %ebp
	movl	$-16, %r15d
	.p2align	4, 0x90
.LBB4_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %r14d
	movl	%ebp, %ecx
	shll	%cl, %r14d
	incl	%ebp
	addl	$16, %r15d
	cmpl	%r13d, %r14d
	jl	.LBB4_3
# BB#4:
	leal	-1(%rbp), %eax
	cmpl	$1, %eax
	movl	$2, %ecx
	cmovlel	%ecx, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	24(%r12), %rsi
	movl	$71, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	movl	$73, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	movl	$70, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	movl	$56, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	movl	$55, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	movl	$97, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	movq	48(%r12), %rax
	movl	128(%rax), %ebx
	movzbl	%bl, %edi
	callq	_IO_putc
	movzbl	%bh, %edi  # NOREX
	movq	24(%r12), %rsi
	callq	_IO_putc
	movq	24(%r12), %rsi
	movq	48(%r12), %rax
	movl	132(%rax), %ebx
	movzbl	%bl, %edi
	callq	_IO_putc
	movzbl	%bh, %edi  # NOREX
	movq	24(%r12), %rsi
	callq	_IO_putc
	leal	-2(%rbp), %edi
	orl	%r15d, %edi
	orl	$128, %edi
	movq	24(%r12), %rsi
	callq	_IO_putc
	movq	24(%r12), %rsi
	xorl	%edi, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	xorl	%edi, %edi
	callq	_IO_putc
	cmpl	$32, %ebp
	je	.LBB4_19
# BB#5:                                 # %.lr.ph
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB4_6
# BB#11:                                # %.lr.ph.split.preheader
	movslq	%r14d, %r15
	movslq	%r13d, %rbp
	xorl	%ebx, %ebx
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r13           # 8-byte Reload
	movl	20(%rsp), %r14d         # 4-byte Reload
	.p2align	4, 0x90
.LBB4_12:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rbp, %rbx
	jge	.LBB4_17
# BB#13:                                #   in Loop: Header=BB4_12 Depth=1
	movq	24(%r12), %rsi
	movq	48(%r12), %rax
	movq	(%r13), %rcx
	movzbl	(%rcx,%rbx), %ebp
	movl	%r14d, %ecx
	shrl	%cl, %ebp
	cmpl	$2, 56(%rax)
	jne	.LBB4_15
# BB#14:                                #   in Loop: Header=BB4_12 Depth=1
	movl	%ebp, %edi
	callq	_IO_putc
	movq	8(%r13), %rax
	movzbl	(%rax,%rbx), %edi
	movl	%r14d, %ecx
	shrl	%cl, %edi
	movq	24(%r12), %rsi
	callq	_IO_putc
	movq	16(%r13), %rax
	movzbl	(%rax,%rbx), %edi
	movl	%r14d, %ecx
	shrl	%cl, %edi
	movq	24(%r12), %rsi
	jmp	.LBB4_16
	.p2align	4, 0x90
.LBB4_17:                               #   in Loop: Header=BB4_12 Depth=1
	movq	24(%r12), %rsi
	xorl	%edi, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	xorl	%edi, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	xorl	%edi, %edi
	callq	_IO_putc
	jmp	.LBB4_18
	.p2align	4, 0x90
.LBB4_15:                               #   in Loop: Header=BB4_12 Depth=1
	movl	%ebp, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	movl	%ebp, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	movl	%ebp, %edi
.LBB4_16:                               #   in Loop: Header=BB4_12 Depth=1
	callq	_IO_putc
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB4_18:                               #   in Loop: Header=BB4_12 Depth=1
	incq	%rbx
	cmpq	%r15, %rbx
	jl	.LBB4_12
	jmp	.LBB4_19
.LBB4_6:                                # %.lr.ph.split.us.preheader
	leal	-1(%r13), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	shrl	$31, %eax
	leal	-1(%r13,%rax), %ebp
	sarl	%ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_7:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r13d, %ebx
	jge	.LBB4_8
# BB#9:                                 #   in Loop: Header=BB4_7 Depth=1
	movl	%ebp, %eax
	cltd
	idivl	8(%rsp)                 # 4-byte Folded Reload
	movl	%eax, %r15d
	movq	24(%r12), %rsi
	movl	%r15d, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	movl	%r15d, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	movl	%r15d, %edi
	jmp	.LBB4_10
	.p2align	4, 0x90
.LBB4_8:                                #   in Loop: Header=BB4_7 Depth=1
	movq	24(%r12), %rsi
	xorl	%edi, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	xorl	%edi, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	xorl	%edi, %edi
.LBB4_10:                               #   in Loop: Header=BB4_7 Depth=1
	callq	_IO_putc
	incl	%ebx
	addl	$255, %ebp
	cmpl	%r14d, %ebx
	jl	.LBB4_7
.LBB4_19:                               # %._crit_edge
	movq	24(%r12), %rsi
	movl	$44, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	xorl	%edi, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	xorl	%edi, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	xorl	%edi, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	xorl	%edi, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	movq	48(%r12), %rax
	movl	128(%rax), %ebx
	movzbl	%bl, %edi
	callq	_IO_putc
	movzbl	%bh, %edi  # NOREX
	movq	24(%r12), %rsi
	callq	_IO_putc
	movq	24(%r12), %rsi
	movq	48(%r12), %rax
	movl	132(%rax), %ebx
	movzbl	%bl, %edi
	callq	_IO_putc
	movzbl	%bh, %edi  # NOREX
	movq	24(%r12), %rsi
	callq	_IO_putc
	movq	24(%r12), %rsi
	xorl	%edi, %edi
	callq	_IO_putc
	movq	24(%r12), %rsi
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %edi
	callq	_IO_putc
	leal	1(%rbx), %eax
	movl	%eax, 64(%r12)
	movl	%eax, 56(%r12)
	movl	%ebx, %ecx
	movl	$2, %eax
	shll	%cl, %eax
	addl	$65535, %eax            # imm = 0xFFFF
	movw	%ax, 60(%r12)
	movl	$1, %eax
	shll	%cl, %eax
	movw	%ax, 92(%r12)
	movl	%eax, %ecx
	orl	$1, %ecx
	movw	%cx, 94(%r12)
	addl	$2, %eax
	movw	%ax, 96(%r12)
	movl	$1, 88(%r12)
	movl	$0, 120(%r12)
	movq	$0, 72(%r12)
	movl	$0, 80(%r12)
	movq	104(%r12), %rdi
	xorl	%esi, %esi
	movl	$10006, %edx            # imm = 0x2716
	callq	memset
	movswq	92(%r12), %rax
	movl	80(%r12), %ecx
	shlq	%cl, %rax
	orq	72(%r12), %rax
	movq	%rax, 72(%r12)
	addl	56(%r12), %ecx
	movl	%ecx, 80(%r12)
	cmpl	$8, %ecx
	jl	.LBB4_26
# BB#20:                                # %.lr.ph.i.preheader.i
	leaq	124(%r12), %rbp
	movl	120(%r12), %ecx
	.p2align	4, 0x90
.LBB4_21:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ecx, %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 120(%r12)
	movb	%al, 125(%r12,%rcx)
	movslq	120(%r12), %rcx
	cmpq	$255, %rcx
	jl	.LBB4_25
# BB#22:                                #   in Loop: Header=BB4_21 Depth=1
	movq	%rcx, %rdx
	incq	%rdx
	movl	%edx, 120(%r12)
	movb	%cl, 124(%r12)
	movq	24(%r12), %rcx
	movl	$1, %esi
	movq	%rbp, %rdi
	callq	fwrite
	movslq	120(%r12), %rcx
	cmpq	%rcx, %rax
	je	.LBB4_24
# BB#23:                                #   in Loop: Header=BB4_21 Depth=1
	movq	48(%r12), %rdi
	movq	(%rdi), %rax
	movl	$36, 40(%rax)
	callq	*(%rax)
.LBB4_24:                               # %flush_packet.exit.i
                                        #   in Loop: Header=BB4_21 Depth=1
	movl	$0, 120(%r12)
	xorl	%ecx, %ecx
.LBB4_25:                               #   in Loop: Header=BB4_21 Depth=1
	movq	72(%r12), %rax
	sarq	$8, %rax
	movq	%rax, 72(%r12)
	movl	80(%r12), %edx
	addl	$-8, %edx
	movl	%edx, 80(%r12)
	cmpl	$7, %edx
	jg	.LBB4_21
.LBB4_26:                               # %._crit_edge.i.i
	movzwl	96(%r12), %eax
	cmpw	60(%r12), %ax
	jle	.LBB4_28
# BB#27:
	movl	56(%r12), %ecx
	incl	%ecx
	movl	%ecx, 56(%r12)
	movl	$1, %eax
	shll	%cl, %eax
	addl	$65535, %eax            # imm = 0xFFFF
	cmpl	$12, %ecx
	movw	$4096, %cx              # imm = 0x1000
	cmovnew	%ax, %cx
	movw	%cx, 60(%r12)
.LBB4_28:                               # %compress_init.exit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	emit_header, .Lfunc_end4-emit_header
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
