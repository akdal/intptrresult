	.text
	.file	"btEmptyCollisionAlgorithm.bc"
	.globl	_ZN16btEmptyAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo
	.p2align	4, 0x90
	.type	_ZN16btEmptyAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo,@function
_ZN16btEmptyAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo: # @_ZN16btEmptyAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo
	movq	$_ZTV16btEmptyAlgorithm+16, (%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN16btEmptyAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo, .Lfunc_end0-_ZN16btEmptyAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo
	.cfi_endproc

	.globl	_ZN16btEmptyAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN16btEmptyAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN16btEmptyAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN16btEmptyAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	_ZN16btEmptyAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end1-_ZN16btEmptyAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN16btEmptyAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN16btEmptyAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN16btEmptyAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN16btEmptyAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	movss	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end2:
	.size	_ZN16btEmptyAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end2-_ZN16btEmptyAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.text._ZN20btCollisionAlgorithmD2Ev,"axG",@progbits,_ZN20btCollisionAlgorithmD2Ev,comdat
	.weak	_ZN20btCollisionAlgorithmD2Ev
	.p2align	4, 0x90
	.type	_ZN20btCollisionAlgorithmD2Ev,@function
_ZN20btCollisionAlgorithmD2Ev:          # @_ZN20btCollisionAlgorithmD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end3:
	.size	_ZN20btCollisionAlgorithmD2Ev, .Lfunc_end3-_ZN20btCollisionAlgorithmD2Ev
	.cfi_endproc

	.section	.text._ZN16btEmptyAlgorithmD0Ev,"axG",@progbits,_ZN16btEmptyAlgorithmD0Ev,comdat
	.weak	_ZN16btEmptyAlgorithmD0Ev
	.p2align	4, 0x90
	.type	_ZN16btEmptyAlgorithmD0Ev,@function
_ZN16btEmptyAlgorithmD0Ev:              # @_ZN16btEmptyAlgorithmD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end4:
	.size	_ZN16btEmptyAlgorithmD0Ev, .Lfunc_end4-_ZN16btEmptyAlgorithmD0Ev
	.cfi_endproc

	.section	.text._ZN16btEmptyAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,"axG",@progbits,_ZN16btEmptyAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,comdat
	.weak	_ZN16btEmptyAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.p2align	4, 0x90
	.type	_ZN16btEmptyAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,@function
_ZN16btEmptyAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE: # @_ZN16btEmptyAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end5:
	.size	_ZN16btEmptyAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE, .Lfunc_end5-_ZN16btEmptyAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_endproc

	.type	_ZTV16btEmptyAlgorithm,@object # @_ZTV16btEmptyAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTV16btEmptyAlgorithm
	.p2align	3
_ZTV16btEmptyAlgorithm:
	.quad	0
	.quad	_ZTI16btEmptyAlgorithm
	.quad	_ZN20btCollisionAlgorithmD2Ev
	.quad	_ZN16btEmptyAlgorithmD0Ev
	.quad	_ZN16btEmptyAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN16btEmptyAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN16btEmptyAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.size	_ZTV16btEmptyAlgorithm, 56

	.type	_ZTS16btEmptyAlgorithm,@object # @_ZTS16btEmptyAlgorithm
	.globl	_ZTS16btEmptyAlgorithm
	.p2align	4
_ZTS16btEmptyAlgorithm:
	.asciz	"16btEmptyAlgorithm"
	.size	_ZTS16btEmptyAlgorithm, 19

	.type	_ZTS20btCollisionAlgorithm,@object # @_ZTS20btCollisionAlgorithm
	.section	.rodata._ZTS20btCollisionAlgorithm,"aG",@progbits,_ZTS20btCollisionAlgorithm,comdat
	.weak	_ZTS20btCollisionAlgorithm
	.p2align	4
_ZTS20btCollisionAlgorithm:
	.asciz	"20btCollisionAlgorithm"
	.size	_ZTS20btCollisionAlgorithm, 23

	.type	_ZTI20btCollisionAlgorithm,@object # @_ZTI20btCollisionAlgorithm
	.section	.rodata._ZTI20btCollisionAlgorithm,"aG",@progbits,_ZTI20btCollisionAlgorithm,comdat
	.weak	_ZTI20btCollisionAlgorithm
	.p2align	3
_ZTI20btCollisionAlgorithm:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS20btCollisionAlgorithm
	.size	_ZTI20btCollisionAlgorithm, 16

	.type	_ZTI16btEmptyAlgorithm,@object # @_ZTI16btEmptyAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTI16btEmptyAlgorithm
	.p2align	4
_ZTI16btEmptyAlgorithm:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS16btEmptyAlgorithm
	.quad	_ZTI20btCollisionAlgorithm
	.size	_ZTI16btEmptyAlgorithm, 24


	.globl	_ZN16btEmptyAlgorithmC1ERK36btCollisionAlgorithmConstructionInfo
	.type	_ZN16btEmptyAlgorithmC1ERK36btCollisionAlgorithmConstructionInfo,@function
_ZN16btEmptyAlgorithmC1ERK36btCollisionAlgorithmConstructionInfo = _ZN16btEmptyAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
