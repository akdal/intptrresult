	.text
	.file	"ldebug.bc"
	.globl	lua_sethook
	.p2align	4, 0x90
	.type	lua_sethook,@function
lua_sethook:                            # @lua_sethook
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	sete	%r9b
	testl	%edx, %edx
	sete	%al
	xorl	%r8d, %r8d
	orb	%r9b, %al
	movl	$0, %eax
	cmoveq	%rsi, %rax
	movq	%rax, 112(%rdi)
	movl	%ecx, 104(%rdi)
	movl	%ecx, 108(%rdi)
	jne	.LBB0_2
# BB#1:
	movb	%dl, %r8b
.LBB0_2:
	movb	%r8b, 100(%rdi)
	movl	$1, %eax
	retq
.Lfunc_end0:
	.size	lua_sethook, .Lfunc_end0-lua_sethook
	.cfi_endproc

	.globl	lua_gethook
	.p2align	4, 0x90
	.type	lua_gethook,@function
lua_gethook:                            # @lua_gethook
	.cfi_startproc
# BB#0:
	movq	112(%rdi), %rax
	retq
.Lfunc_end1:
	.size	lua_gethook, .Lfunc_end1-lua_gethook
	.cfi_endproc

	.globl	lua_gethookmask
	.p2align	4, 0x90
	.type	lua_gethookmask,@function
lua_gethookmask:                        # @lua_gethookmask
	.cfi_startproc
# BB#0:
	movzbl	100(%rdi), %eax
	retq
.Lfunc_end2:
	.size	lua_gethookmask, .Lfunc_end2-lua_gethookmask
	.cfi_endproc

	.globl	lua_gethookcount
	.p2align	4, 0x90
	.type	lua_gethookcount,@function
lua_gethookcount:                       # @lua_gethookcount
	.cfi_startproc
# BB#0:
	movl	104(%rdi), %eax
	retq
.Lfunc_end3:
	.size	lua_gethookcount, .Lfunc_end3-lua_gethookcount
	.cfi_endproc

	.globl	lua_getstack
	.p2align	4, 0x90
	.type	lua_getstack,@function
lua_getstack:                           # @lua_getstack
	.cfi_startproc
# BB#0:
	movq	40(%rdi), %rcx
	testl	%esi, %esi
	jle	.LBB4_7
# BB#1:                                 # %.lr.ph
	movq	80(%rdi), %r8
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	cmpq	%r8, %rcx
	jbe	.LBB4_3
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=1
	decl	%esi
	movq	8(%rcx), %rax
	movq	(%rax), %rax
	cmpb	$0, 10(%rax)
	jne	.LBB4_6
# BB#5:                                 #   in Loop: Header=BB4_2 Depth=1
	subl	36(%rcx), %esi
.LBB4_6:                                #   in Loop: Header=BB4_2 Depth=1
	addq	$-40, %rcx
	testl	%esi, %esi
	jg	.LBB4_2
.LBB4_7:                                # %.critedge
	xorl	%eax, %eax
	testl	%esi, %esi
	jne	.LBB4_10
# BB#8:
	xorl	%eax, %eax
	subq	80(%rdi), %rcx
	jbe	.LBB4_11
# BB#9:
	shrq	$3, %rcx
	imull	$-858993459, %ecx, %eax # imm = 0xCCCCCCCD
.LBB4_10:                               # %.sink.split
	movl	%eax, 116(%rdx)
	movl	$1, %eax
.LBB4_11:                               # %.thread
	retq
.LBB4_3:
	xorl	%eax, %eax
	retq
.Lfunc_end4:
	.size	lua_getstack, .Lfunc_end4-lua_getstack
	.cfi_endproc

	.globl	lua_getlocal
	.p2align	4, 0x90
	.type	lua_getlocal,@function
lua_getlocal:                           # @lua_getlocal
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rdi, %rbx
	movq	80(%rbx), %rax
	movslq	116(%rsi), %rcx
	leaq	(%rcx,%rcx,4), %rsi
	leaq	(%rax,%rsi,8), %rbp
	movq	8(%rax,%rsi,8), %rcx
	cmpl	$6, 8(%rcx)
	jne	.LBB5_8
# BB#1:
	movq	(%rcx), %r8
	cmpb	$0, 10(%r8)
	je	.LBB5_2
.LBB5_8:                                # %getluaproto.exit.thread.i
	leaq	48(%rbp), %rax
	cmpq	%rbp, 40(%rbx)
	leaq	16(%rbx), %rcx
	cmoveq	%rcx, %rax
	xorl	%r15d, %r15d
	testl	%r14d, %r14d
	jle	.LBB5_12
# BB#9:                                 # %getluaproto.exit.thread.i
	movq	(%rax), %rcx
	movq	(%rbp), %rax
	subq	%rax, %rcx
	sarq	$4, %rcx
	movslq	%r14d, %rdx
	cmpq	%rdx, %rcx
	jl	.LBB5_12
# BB#10:                                # %getluaproto.exit.thread.i.select.unfold_crit_edge
	movl	$.L.str.6, %r15d
.LBB5_11:                               # %select.unfold
	movslq	%r14d, %rcx
	shlq	$4, %rcx
	leaq	-16(%rax,%rcx), %rsi
	movq	%rbx, %rdi
	callq	luaA_pushobject
.LBB5_12:                               # %findlocal.exit
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_2:                                # %getluaproto.exit.i
	movq	32(%r8), %rdi
	testq	%rdi, %rdi
	je	.LBB5_8
# BB#3:
	cmpq	%rbp, 40(%rbx)
	je	.LBB5_5
# BB#4:                                 # %._crit_edge.i.i
	movq	24(%rax,%rsi,8), %rdx
	jmp	.LBB5_6
.LBB5_5:
	movq	48(%rbx), %rdx
	movq	%rdx, 24(%rax,%rsi,8)
	movq	(%rcx), %r8
.LBB5_6:                                # %currentpc.exit.i
	movq	32(%r8), %rax
	subq	24(%rax), %rdx
	shrq	$2, %rdx
	decl	%edx
	movl	%r14d, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	luaF_getlocalname
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB5_8
# BB#7:                                 # %currentpc.exit.i.select.unfold_crit_edge
	movq	(%rbp), %rax
	jmp	.LBB5_11
.Lfunc_end5:
	.size	lua_getlocal, .Lfunc_end5-lua_getlocal
	.cfi_endproc

	.globl	lua_setlocal
	.p2align	4, 0x90
	.type	lua_setlocal,@function
lua_setlocal:                           # @lua_setlocal
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -32
.Lcfi13:
	.cfi_offset %r14, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rdi, %rbx
	movq	80(%rbx), %rax
	movslq	116(%rsi), %rcx
	leaq	(%rcx,%rcx,4), %rsi
	leaq	(%rax,%rsi,8), %rbp
	movq	8(%rax,%rsi,8), %rcx
	cmpl	$6, 8(%rcx)
	jne	.LBB6_8
# BB#1:
	movq	(%rcx), %r8
	cmpb	$0, 10(%r8)
	je	.LBB6_2
.LBB6_8:                                # %getluaproto.exit.thread.i
	leaq	48(%rbp), %rcx
	cmpq	%rbp, 40(%rbx)
	leaq	16(%rbx), %rbx
	cmoveq	%rbx, %rcx
	xorl	%eax, %eax
	testl	%r14d, %r14d
	jle	.LBB6_12
# BB#9:                                 # %getluaproto.exit.thread.i
	movq	(%rcx), %rdx
	movq	(%rbp), %rcx
	subq	%rcx, %rdx
	sarq	$4, %rdx
	movslq	%r14d, %rsi
	cmpq	%rsi, %rdx
	jl	.LBB6_12
# BB#10:                                # %getluaproto.exit.thread.i.select.unfold_crit_edge
	movl	$.L.str.6, %eax
.LBB6_11:                               # %select.unfold
	movq	(%rbx), %rdx
	movslq	%r14d, %rsi
	shlq	$4, %rsi
	movq	-16(%rdx), %rdi
	movq	%rdi, -16(%rcx,%rsi)
	movl	-8(%rdx), %edx
	movl	%edx, -8(%rcx,%rsi)
.LBB6_12:                               # %findlocal.exit
	addq	$-16, (%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB6_2:                                # %getluaproto.exit.i
	movq	32(%r8), %rdi
	testq	%rdi, %rdi
	je	.LBB6_8
# BB#3:
	cmpq	%rbp, 40(%rbx)
	je	.LBB6_5
# BB#4:                                 # %._crit_edge.i.i
	movq	24(%rax,%rsi,8), %rdx
	jmp	.LBB6_6
.LBB6_5:
	movq	48(%rbx), %rdx
	movq	%rdx, 24(%rax,%rsi,8)
	movq	(%rcx), %r8
.LBB6_6:                                # %currentpc.exit.i
	movq	32(%r8), %rax
	subq	24(%rax), %rdx
	shrq	$2, %rdx
	decl	%edx
	movl	%r14d, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	luaF_getlocalname
	testq	%rax, %rax
	je	.LBB6_8
# BB#7:                                 # %currentpc.exit.i.select.unfold_crit_edge
	movq	(%rbp), %rcx
	addq	$16, %rbx
	jmp	.LBB6_11
.Lfunc_end6:
	.size	lua_setlocal, .Lfunc_end6-lua_setlocal
	.cfi_endproc

	.globl	lua_getinfo
	.p2align	4, 0x90
	.type	lua_getinfo,@function
lua_getinfo:                            # @lua_getinfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 112
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	movq	%rdi, %r15
	cmpb	$62, (%rbx)
	jne	.LBB7_2
# BB#1:
	movq	16(%r15), %rax
	incq	%rbx
	movq	-16(%rax), %r12
	addq	$-16, %rax
	movq	%rax, 16(%r15)
	xorl	%r13d, %r13d
	testq	%r12, %r12
	jne	.LBB7_5
	jmp	.LBB7_7
.LBB7_2:
	movslq	116(%rbp), %rax
	testq	%rax, %rax
	je	.LBB7_7
# BB#3:
	movq	80(%r15), %rcx
	leaq	(%rax,%rax,4), %rax
	leaq	(%rcx,%rax,8), %r13
	movq	8(%rcx,%rax,8), %rax
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.LBB7_7
.LBB7_5:                                # %.preheader.i
	movb	(%rbx), %al
	testb	%al, %al
	je	.LBB7_6
# BB#8:                                 # %.lr.ph.i
	leaq	56(%rbp), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leaq	8(%rbp), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	leaq	-40(%r13), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	leaq	1(%rbx), %r14
	movl	$1, %ebx
	movl	$.L.str.7, %ecx
	movd	%rcx, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqa	%xmm0, 32(%rsp)         # 16-byte Spill
	jmp	.LBB7_9
.LBB7_7:                                # %.thread
	movl	$.L.str.7, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, 8(%rbp)
	movl	$-1, 40(%rbp)
	movl	$-1, 48(%rbp)
	movl	$-1, 52(%rbp)
	movl	$.L.str.9, %eax
	movd	%rax, %xmm0
	movl	$.L.str.8, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 24(%rbp)
	leaq	56(%rbp), %rdi
	movl	$.L.str.9, %esi
	movl	$60, %edx
	callq	luaO_chunkid
	movl	$0, 44(%rbp)
	movl	$1, %r13d
	movb	$1, %bpl
	xorl	%r12d, %r12d
	jmp	.LBB7_44
.LBB7_6:
	movl	$1, %r13d
	xorl	%ebp, %ebp
	jmp	.LBB7_44
.LBB7_20:                               #   in Loop: Header=BB7_9 Depth=1
	movq	48(%r15), %rcx
	movq	%rcx, 24(%r13)
	movq	(%rsi), %rdx
.LBB7_21:                               # %currentpc.exit.i.i
                                        #   in Loop: Header=BB7_9 Depth=1
	movq	32(%rdx), %rdx
	subq	24(%rdx), %rcx
	movq	%rcx, %rsi
	shrq	$2, %rsi
	testl	%esi, %esi
	jle	.LBB7_25
# BB#22:                                #   in Loop: Header=BB7_9 Depth=1
	movq	40(%rdx), %rax
	testq	%rax, %rax
	je	.LBB7_23
# BB#24:                                #   in Loop: Header=BB7_9 Depth=1
	shlq	$30, %rcx
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	addq	%rdx, %rcx
	sarq	$30, %rcx
	movl	(%rax,%rcx), %eax
	jmp	.LBB7_25
.LBB7_35:                               #   in Loop: Header=BB7_9 Depth=1
	movq	48(%r15), %rdx
	movq	%rdx, -16(%r13)
	movq	(%rsi), %rcx
.LBB7_36:                               # %currentpc.exit.i32.i
                                        #   in Loop: Header=BB7_9 Depth=1
	movq	32(%rcx), %rcx
	subq	24(%rcx), %rdx
	shlq	$30, %rdx
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	addq	%rcx, %rdx
	sarq	$30, %rdx
	movl	(%rax,%rdx), %eax
	movl	%eax, %ecx
	andb	$63, %cl
	cmpb	$33, %cl
	ja	.LBB7_38
# BB#37:                                # %currentpc.exit.i32.i
                                        #   in Loop: Header=BB7_9 Depth=1
	movl	%eax, %ecx
	andl	$63, %ecx
	movabsq	$9395240960, %rdx       # imm = 0x230000000
	btq	%rcx, %rdx
	jae	.LBB7_38
# BB#39:                                # %getfuncname.exit.i
                                        #   in Loop: Header=BB7_9 Depth=1
	shrl	$6, %eax
	movzbl	%al, %edx
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsp), %rcx            # 8-byte Reload
	callq	getobjname
	movq	%rax, 16(%rbp)
	testq	%rax, %rax
	jne	.LBB7_42
	jmp	.LBB7_40
.LBB7_23:                               #   in Loop: Header=BB7_9 Depth=1
	xorl	%eax, %eax
	movl	%eax, 40(%rbp)
	jmp	.LBB7_42
	.p2align	4, 0x90
.LBB7_9:                                # =>This Inner Loop Header: Depth=1
	movsbl	%al, %eax
	addl	$-76, %eax
	cmpl	$41, %eax
	ja	.LBB7_41
# BB#10:                                #   in Loop: Header=BB7_9 Depth=1
	jmpq	*.LJTI7_0(,%rax,8)
.LBB7_11:                               #   in Loop: Header=BB7_9 Depth=1
	cmpb	$0, 10(%r12)
	je	.LBB7_13
# BB#12:                                #   in Loop: Header=BB7_9 Depth=1
	movq	$.L.str.10, 32(%rbp)
	movl	$-1, %eax
	movl	$.L.str.10, %esi
	movl	$-1, %ecx
	movl	$.L.str.11, %edx
	jmp	.LBB7_14
.LBB7_41:                               #   in Loop: Header=BB7_9 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB7_42
.LBB7_15:                               #   in Loop: Header=BB7_9 Depth=1
	movl	$-1, %eax
	testq	%r13, %r13
	je	.LBB7_25
# BB#16:                                #   in Loop: Header=BB7_9 Depth=1
	movq	8(%r13), %rsi
	cmpl	$6, 8(%rsi)
	jne	.LBB7_25
# BB#17:                                #   in Loop: Header=BB7_9 Depth=1
	movq	(%rsi), %rdx
	cmpb	$0, 10(%rdx)
	je	.LBB7_18
.LBB7_25:                               # %currentline.exit.i
                                        #   in Loop: Header=BB7_9 Depth=1
	movl	%eax, 40(%rbp)
	jmp	.LBB7_42
.LBB7_27:                               #   in Loop: Header=BB7_9 Depth=1
	testq	%r13, %r13
	je	.LBB7_38
# BB#28:                                #   in Loop: Header=BB7_9 Depth=1
	movq	8(%r13), %rax
	cmpl	$6, 8(%rax)
	jne	.LBB7_31
# BB#29:                                #   in Loop: Header=BB7_9 Depth=1
	movq	(%rax), %rax
	cmpb	$0, 10(%rax)
	je	.LBB7_30
.LBB7_31:                               #   in Loop: Header=BB7_9 Depth=1
	movq	-32(%r13), %rsi
	cmpl	$6, 8(%rsi)
	jne	.LBB7_38
# BB#32:                                #   in Loop: Header=BB7_9 Depth=1
	movq	(%rsi), %rcx
	cmpb	$0, 10(%rcx)
	jne	.LBB7_38
# BB#33:                                #   in Loop: Header=BB7_9 Depth=1
	movq	32(%rcx), %rax
	movq	24(%rax), %rax
	movq	8(%rsp), %rdx           # 8-byte Reload
	cmpq	%rdx, 40(%r15)
	je	.LBB7_35
# BB#34:                                # %._crit_edge.i.i29.i
                                        #   in Loop: Header=BB7_9 Depth=1
	movq	-16(%r13), %rdx
	jmp	.LBB7_36
.LBB7_26:                               #   in Loop: Header=BB7_9 Depth=1
	movzbl	11(%r12), %eax
	movl	%eax, 44(%rbp)
	jmp	.LBB7_42
.LBB7_13:                               #   in Loop: Header=BB7_9 Depth=1
	movq	32(%r12), %rcx
	movq	64(%rcx), %rsi
	addq	$24, %rsi
	movq	%rsi, 32(%rbp)
	movl	96(%rcx), %eax
	movl	100(%rcx), %ecx
	testl	%eax, %eax
	movl	$.L.str.13, %edx
	movl	$.L.str.12, %edi
	cmoveq	%rdi, %rdx
.LBB7_14:                               # %funcinfo.exit.i
                                        #   in Loop: Header=BB7_9 Depth=1
	movl	%eax, 48(%rbp)
	movl	%ecx, 52(%rbp)
	movq	%rdx, 24(%rbp)
	movl	$60, %edx
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	luaO_chunkid
	jmp	.LBB7_42
.LBB7_18:                               #   in Loop: Header=BB7_9 Depth=1
	cmpq	%r13, 40(%r15)
	je	.LBB7_20
# BB#19:                                # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB7_9 Depth=1
	movq	24(%r13), %rcx
	jmp	.LBB7_21
.LBB7_30:                               #   in Loop: Header=BB7_9 Depth=1
	cmpl	$0, 36(%r13)
	jle	.LBB7_31
.LBB7_38:                               # %getfuncname.exit.thread.i
                                        #   in Loop: Header=BB7_9 Depth=1
	movq	$0, 16(%rbp)
.LBB7_40:                               #   in Loop: Header=BB7_9 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movdqa	32(%rsp), %xmm0         # 16-byte Reload
	movdqu	%xmm0, (%rax)
	.p2align	4, 0x90
.LBB7_42:                               #   in Loop: Header=BB7_9 Depth=1
	movzbl	(%r14), %eax
	incq	%r14
	testb	%al, %al
	jne	.LBB7_9
# BB#43:
	xorl	%ebp, %ebp
	movl	%ebx, %r13d
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB7_44:                               # %auxgetinfo.exit
	movl	$102, %esi
	movq	%rbx, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB7_51
# BB#45:
	movq	16(%r15), %rcx
	testb	%bpl, %bpl
	je	.LBB7_47
# BB#46:                                # %._crit_edge
	xorl	%edx, %edx
	movq	%rcx, %rax
	jmp	.LBB7_48
.LBB7_47:
	movq	%r12, (%rcx)
	movq	16(%r15), %rax
	movl	$6, %edx
.LBB7_48:
	movl	%edx, 8(%rcx)
	movq	56(%r15), %rcx
	subq	%rax, %rcx
	cmpq	$16, %rcx
	jg	.LBB7_50
# BB#49:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	luaD_growstack
	movq	16(%r15), %rax
.LBB7_50:
	addq	$16, %rax
	movq	%rax, 16(%r15)
.LBB7_51:
	movl	$76, %esi
	movq	%rbx, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB7_62
# BB#52:
	testb	%bpl, %bpl
	jne	.LBB7_54
# BB#53:
	cmpb	$0, 10(%r12)
	je	.LBB7_55
.LBB7_54:
	leaq	16(%r15), %r14
	movq	16(%r15), %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
.LBB7_59:
	movl	%edx, 8(%rcx)
	movq	56(%r15), %rcx
	subq	%rax, %rcx
	cmpq	$16, %rcx
	jg	.LBB7_61
# BB#60:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	luaD_growstack
	movq	16(%r15), %rax
.LBB7_61:                               # %collectvalidlines.exit
	addq	$16, %rax
	movq	%rax, (%r14)
.LBB7_62:
	movl	%r13d, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_55:
	movl	%r13d, (%rsp)           # 4-byte Spill
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	luaH_new
	movq	%rax, %r13
	movq	32(%r12), %rax
	cmpl	$0, 84(%rax)
	jle	.LBB7_58
# BB#56:                                # %.lr.ph.i41.preheader
	movq	40(%rax), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_57:                               # %.lr.ph.i41
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rbp,4), %edx
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	luaH_setnum
	movl	$1, (%rax)
	movl	$1, 8(%rax)
	incq	%rbp
	movq	32(%r12), %rax
	movslq	84(%rax), %rax
	cmpq	%rax, %rbp
	jl	.LBB7_57
.LBB7_58:                               # %._crit_edge.i
	leaq	16(%r15), %r14
	movq	16(%r15), %rcx
	movq	%r13, (%rcx)
	movq	16(%r15), %rax
	movl	$5, %edx
	movl	(%rsp), %r13d           # 4-byte Reload
	jmp	.LBB7_59
.Lfunc_end7:
	.size	lua_getinfo, .Lfunc_end7-lua_getinfo
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_42
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_11
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_42
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_15
	.quad	.LBB7_41
	.quad	.LBB7_27
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_41
	.quad	.LBB7_26

	.text
	.hidden	luaG_checkopenop
	.globl	luaG_checkopenop
	.p2align	4, 0x90
	.type	luaG_checkopenop,@function
luaG_checkopenop:                       # @luaG_checkopenop
	.cfi_startproc
# BB#0:
	movl	%edi, %ecx
	andb	$63, %cl
	xorl	%eax, %eax
	cmpb	$34, %cl
	ja	.LBB8_3
# BB#1:
	movl	%edi, %ecx
	andl	$63, %ecx
	movabsq	$19058917376, %rdx      # imm = 0x470000000
	btq	%rcx, %rdx
	jae	.LBB8_3
# BB#2:
	cmpl	$8388608, %edi          # imm = 0x800000
	sbbl	%eax, %eax
	andl	$1, %eax
.LBB8_3:
	retq
.Lfunc_end8:
	.size	luaG_checkopenop, .Lfunc_end8-luaG_checkopenop
	.cfi_endproc

	.hidden	luaG_checkcode
	.globl	luaG_checkcode
	.p2align	4, 0x90
	.type	luaG_checkcode,@function
luaG_checkcode:                         # @luaG_checkcode
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 16
	movl	80(%rdi), %esi
	movl	$255, %edx
	callq	symbexec
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	movl	%ecx, %eax
	popq	%rcx
	retq
.Lfunc_end9:
	.size	luaG_checkcode, .Lfunc_end9-luaG_checkcode
	.cfi_endproc

	.p2align	4, 0x90
	.type	symbexec,@function
symbexec:                               # @symbexec
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movzbl	115(%rdi), %r15d
	xorl	%eax, %eax
	cmpl	$250, %r15d
	ja	.LBB10_91
# BB#1:
	movzbl	114(%rdi), %r8d
	movl	%r8d, %ecx
	andl	$5, %ecx
	cmpl	$4, %ecx
	je	.LBB10_91
# BB#2:
	movzbl	113(%rdi), %ecx
	movl	%r8d, %ebp
	andl	$1, %ebp
	addl	%ecx, %ebp
	cmpl	%r15d, %ebp
	ja	.LBB10_91
# BB#3:
	movl	%edx, %r9d
	movzbl	112(%rdi), %ecx
	cmpl	%ecx, 72(%rdi)
	jle	.LBB10_4
.LBB10_91:                              # %precheck.exit.thread
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_4:
	movslq	80(%rdi), %rdx
	movl	84(%rdi), %ebp
	cmpl	%edx, %ebp
	sete	%r10b
	testl	%ebp, %ebp
	sete	%bl
	testl	%edx, %edx
	jle	.LBB10_91
# BB#5:
	orb	%r10b, %bl
	je	.LBB10_91
# BB#6:                                 # %precheck.exit
	movl	%ecx, -20(%rsp)         # 4-byte Spill
	movq	24(%rdi), %rcx
	movq	%rdx, -40(%rsp)         # 8-byte Spill
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	movl	-4(%rcx,%rdx,4), %ebp
	movl	%ebp, %edx
	andl	$63, %edx
	cmpl	$30, %edx
	movl	%r9d, %ecx
	jne	.LBB10_91
# BB#7:
	testl	%esi, %esi
	movl	%ebp, %eax
	jle	.LBB10_91
# BB#8:                                 # %.lr.ph261
	movq	-40(%rsp), %rax         # 8-byte Reload
	leaq	-1(%rax), %rdx
	andb	$6, %r8b
	movq	-48(%rsp), %rax         # 8-byte Reload
	leaq	4(%rax), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	movq	%rdx, %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movl	%edx, %r11d
	movl	%ecx, -52(%rsp)         # 4-byte Spill
.LBB10_9:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_23 Depth 2
                                        #     Child Loop BB10_72 Depth 2
	movl	%r11d, %r14d
	movslq	%r13d, %rdx
	movq	-48(%rsp), %rax         # 8-byte Reload
	movq	%rdx, -32(%rsp)         # 8-byte Spill
	movl	(%rax,%rdx,4), %ebp
	movl	%ebp, %eax
	andl	$63, %eax
	cmpl	$37, %eax
	ja	.LBB10_90
# BB#10:                                #   in Loop: Header=BB10_9 Depth=1
	movl	%ebp, %edx
	shrl	$6, %edx
	movzbl	%dl, %r12d
	cmpl	%r15d, %r12d
	jae	.LBB10_90
# BB#11:                                #   in Loop: Header=BB10_9 Depth=1
	movl	%eax, %eax
	movzbl	luaP_opmodes(%rax), %r9d
	movl	%r9d, %eax
	andb	$3, %al
	xorl	%r10d, %r10d
	cmpb	$2, %al
	je	.LBB10_18
# BB#12:                                #   in Loop: Header=BB10_9 Depth=1
	cmpb	$1, %al
	je	.LBB10_26
# BB#13:                                #   in Loop: Header=BB10_9 Depth=1
	testb	%al, %al
	movl	$0, %eax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	jne	.LBB10_40
# BB#14:                                #   in Loop: Header=BB10_9 Depth=1
	movl	%ebp, %ebx
	shrl	$23, %ebx
	movl	%r9d, %eax
	shrb	$4, %al
	andb	$3, %al
	cmpb	$3, %al
	je	.LBB10_28
# BB#15:                                #   in Loop: Header=BB10_9 Depth=1
	cmpb	$2, %al
	je	.LBB10_88
# BB#16:                                #   in Loop: Header=BB10_9 Depth=1
	testb	%al, %al
	jne	.LBB10_32
# BB#17:                                #   in Loop: Header=BB10_9 Depth=1
	testl	%ebx, %ebx
	je	.LBB10_32
	jmp	.LBB10_90
.LBB10_18:                              #   in Loop: Header=BB10_9 Depth=1
	movl	%ebp, %edx
	shrl	$14, %edx
	addl	$-131071, %edx          # imm = 0xFFFE0001
	movl	%r9d, %eax
	andl	$48, %eax
	cmpl	$32, %eax
	movq	%rdx, -64(%rsp)         # 8-byte Spill
	jne	.LBB10_40
# BB#19:                                #   in Loop: Header=BB10_9 Depth=1
	leal	(%r13,%rdx), %ebx
	xorl	%eax, %eax
	movl	%ebx, %edx
	incl	%edx
	js	.LBB10_91
# BB#20:                                #   in Loop: Header=BB10_9 Depth=1
	cmpl	-40(%rsp), %edx         # 4-byte Folded Reload
	jge	.LBB10_91
# BB#21:                                #   in Loop: Header=BB10_9 Depth=1
	testl	%edx, %edx
	jle	.LBB10_30
# BB#22:                                # %.lr.ph
                                        #   in Loop: Header=BB10_9 Depth=1
	movslq	%edx, %r10
	xorl	%eax, %eax
	movl	$8372287, %r11d         # imm = 0x7FC03F
.LBB10_23:                              #   Parent Loop BB10_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebx, %rdx
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rdx,4), %edx
	andl	%r11d, %edx
	cmpl	$34, %edx
	jne	.LBB10_25
# BB#24:                                # %.critedge
                                        #   in Loop: Header=BB10_23 Depth=2
	incq	%rax
	decl	%ebx
	cmpq	%r10, %rax
	jl	.LBB10_23
.LBB10_25:                              # %._crit_edge
                                        #   in Loop: Header=BB10_9 Depth=1
	xorl	%r10d, %r10d
	testb	$1, %al
	movl	$0, %eax
	movl	-52(%rsp), %ecx         # 4-byte Reload
	je	.LBB10_40
	jmp	.LBB10_91
.LBB10_26:                              #   in Loop: Header=BB10_9 Depth=1
	movl	%ebp, %edx
	shrl	$14, %edx
	movl	%r9d, %eax
	andl	$48, %eax
	cmpl	$48, %eax
	movq	%rdx, -64(%rsp)         # 8-byte Spill
	jne	.LBB10_40
# BB#27:                                #   in Loop: Header=BB10_9 Depth=1
	xorl	%r10d, %r10d
	cmpl	76(%rdi), %edx
	movl	$0, %eax
	jl	.LBB10_40
	jmp	.LBB10_91
.LBB10_28:                              #   in Loop: Header=BB10_9 Depth=1
	testb	$1, %bh
	jne	.LBB10_31
# BB#29:                                #   in Loop: Header=BB10_9 Depth=1
	cmpl	%ebx, %r15d
	ja	.LBB10_32
	jmp	.LBB10_90
.LBB10_88:                              #   in Loop: Header=BB10_9 Depth=1
	cmpl	%ebx, %r15d
	ja	.LBB10_32
	jmp	.LBB10_90
.LBB10_30:                              #   in Loop: Header=BB10_9 Depth=1
	xorl	%r10d, %r10d
	jmp	.LBB10_40
.LBB10_31:                              #   in Loop: Header=BB10_9 Depth=1
	movzbl	%bl, %eax
	cmpl	76(%rdi), %eax
	jge	.LBB10_90
.LBB10_32:                              # %checkArgMode.exit228
                                        #   in Loop: Header=BB10_9 Depth=1
	movl	%ebp, %eax
	shrl	$14, %eax
	movl	%eax, %r10d
	andl	$511, %r10d             # imm = 0x1FF
	movl	%r9d, %edx
	shrb	$2, %dl
	andb	$3, %dl
	cmpb	$3, %dl
	movq	%rbx, -64(%rsp)         # 8-byte Spill
	je	.LBB10_36
# BB#33:                                # %checkArgMode.exit228
                                        #   in Loop: Header=BB10_9 Depth=1
	cmpb	$2, %dl
	je	.LBB10_38
# BB#34:                                # %checkArgMode.exit228
                                        #   in Loop: Header=BB10_9 Depth=1
	testb	%dl, %dl
	jne	.LBB10_40
# BB#35:                                #   in Loop: Header=BB10_9 Depth=1
	xorl	%eax, %eax
	testl	%r10d, %r10d
	movl	$0, %r10d
	je	.LBB10_40
	jmp	.LBB10_91
.LBB10_36:                              #   in Loop: Header=BB10_9 Depth=1
	testb	$1, %ah
	jne	.LBB10_39
.LBB10_38:                              #   in Loop: Header=BB10_9 Depth=1
	cmpl	%r10d, %r15d
	ja	.LBB10_40
	jmp	.LBB10_90
.LBB10_39:                              #   in Loop: Header=BB10_9 Depth=1
	movzbl	%al, %eax
	cmpl	76(%rdi), %eax
	jge	.LBB10_90
.LBB10_40:                              # %checkArgMode.exit
                                        #   in Loop: Header=BB10_9 Depth=1
	testb	$64, %r9b
	movl	%r14d, %r11d
	cmovnel	%r13d, %r11d
	cmpl	%ecx, %r12d
	cmovnel	%r14d, %r11d
	testb	%r9b, %r9b
	jns	.LBB10_43
# BB#41:                                #   in Loop: Header=BB10_9 Depth=1
	leal	2(%r13), %edx
	xorl	%eax, %eax
	cmpl	-40(%rsp), %edx         # 4-byte Folded Reload
	jge	.LBB10_91
# BB#42:                                #   in Loop: Header=BB10_9 Depth=1
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movq	-32(%rsp), %rdx         # 8-byte Reload
	movl	4(%rcx,%rdx,4), %edx
	movl	-52(%rsp), %ecx         # 4-byte Reload
	andl	$63, %edx
	cmpl	$22, %edx
	jne	.LBB10_91
.LBB10_43:                              #   in Loop: Header=BB10_9 Depth=1
	andb	$63, %bpl
	addb	$-2, %bpl
	cmpb	$35, %bpl
	ja	.LBB10_85
# BB#44:                                #   in Loop: Header=BB10_9 Depth=1
	movzbl	%bpl, %eax
	jmpq	*.LJTI10_0(,%rax,8)
.LBB10_45:                              #   in Loop: Header=BB10_9 Depth=1
	movq	-64(%rsp), %rax         # 8-byte Reload
	cmpl	-20(%rsp), %eax         # 4-byte Folded Reload
	jl	.LBB10_85
	jmp	.LBB10_90
.LBB10_87:                              #   in Loop: Header=BB10_9 Depth=1
	movq	16(%rdi), %rax
	movslq	-64(%rsp), %rdx         # 4-byte Folded Reload
	shlq	$4, %rdx
	cmpl	$4, 8(%rax,%rdx)
	je	.LBB10_85
	jmp	.LBB10_90
.LBB10_46:                              #   in Loop: Header=BB10_9 Depth=1
	xorl	%eax, %eax
	movq	-64(%rsp), %rdx         # 8-byte Reload
	testl	%edx, %edx
	je	.LBB10_48
# BB#47:                                #   in Loop: Header=BB10_9 Depth=1
	addl	%r12d, %edx
	cmpl	%r15d, %edx
	jg	.LBB10_91
.LBB10_48:                              #   in Loop: Header=BB10_9 Depth=1
	testl	%r10d, %r10d
	je	.LBB10_81
# BB#49:                                #   in Loop: Header=BB10_9 Depth=1
	decl	%r10d
	je	.LBB10_84
# BB#50:                                #   in Loop: Header=BB10_9 Depth=1
	addl	%r12d, %r10d
	cmpl	%r15d, %r10d
	jle	.LBB10_84
	jmp	.LBB10_91
.LBB10_51:                              #   in Loop: Header=BB10_9 Depth=1
	addl	$3, %r12d
	cmpl	%r15d, %r12d
	jae	.LBB10_90
.LBB10_52:                              #   in Loop: Header=BB10_9 Depth=1
	movq	-64(%rsp), %rbp         # 8-byte Reload
	leal	1(%r13,%rbp), %eax
	cmpl	%eax, %r13d
	movl	$0, %edx
	cmovgel	%edx, %ebp
	cmpl	$255, %ecx
	cmovel	%edx, %ebp
	cmpl	%esi, %eax
	cmovgl	%edx, %ebp
	addl	%r13d, %ebp
	movl	%ebp, %r13d
	jmp	.LBB10_85
.LBB10_53:                              #   in Loop: Header=BB10_9 Depth=1
	cmpl	$1, %r10d
	jne	.LBB10_85
# BB#54:                                #   in Loop: Header=BB10_9 Depth=1
	leal	2(%r13), %edx
	xorl	%eax, %eax
	cmpl	-40(%rsp), %edx         # 4-byte Folded Reload
	jge	.LBB10_91
# BB#55:                                #   in Loop: Header=BB10_9 Depth=1
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movq	-32(%rsp), %rdx         # 8-byte Reload
	movl	4(%rcx,%rdx,4), %edx
	movl	$8372287, %ecx          # imm = 0x7FC03F
	andl	%ecx, %edx
	cmpl	$34, %edx
	movl	-52(%rsp), %ecx         # 4-byte Reload
	jne	.LBB10_85
	jmp	.LBB10_91
.LBB10_56:                              #   in Loop: Header=BB10_9 Depth=1
	cmpl	%ecx, -64(%rsp)         # 4-byte Folded Reload
	movl	%r13d, %eax
	cmovll	%r11d, %eax
	cmpl	%ecx, %r12d
	cmovlel	%eax, %r11d
	jmp	.LBB10_85
.LBB10_57:                              #   in Loop: Header=BB10_9 Depth=1
	incl	%r12d
	cmpl	%r15d, %r12d
	jae	.LBB10_90
# BB#58:                                #   in Loop: Header=BB10_9 Depth=1
	cmpl	%ecx, %r12d
	cmovel	%r13d, %r11d
	jmp	.LBB10_85
.LBB10_89:                              #   in Loop: Header=BB10_9 Depth=1
	cmpl	%r10d, -64(%rsp)        # 4-byte Folded Reload
	jl	.LBB10_85
	jmp	.LBB10_90
.LBB10_59:                              #   in Loop: Header=BB10_9 Depth=1
	movq	-64(%rsp), %rax         # 8-byte Reload
	cmpl	$2, %eax
	jl	.LBB10_85
# BB#60:                                #   in Loop: Header=BB10_9 Depth=1
	leal	-1(%r12,%rax), %eax
	cmpl	%r15d, %eax
	jle	.LBB10_85
	jmp	.LBB10_90
.LBB10_61:                              #   in Loop: Header=BB10_9 Depth=1
	xorl	%eax, %eax
	testl	%r10d, %r10d
	je	.LBB10_91
# BB#62:                                #   in Loop: Header=BB10_9 Depth=1
	leal	2(%r10,%r12), %edx
	cmpl	%r15d, %edx
	jae	.LBB10_91
# BB#63:                                #   in Loop: Header=BB10_9 Depth=1
	addl	$2, %r12d
	jmp	.LBB10_84
.LBB10_64:                              #   in Loop: Header=BB10_9 Depth=1
	xorl	%eax, %eax
	movq	-64(%rsp), %rdx         # 8-byte Reload
	testl	%edx, %edx
	jle	.LBB10_66
# BB#65:                                #   in Loop: Header=BB10_9 Depth=1
	addl	%r12d, %edx
	cmpl	%r15d, %edx
	jge	.LBB10_91
.LBB10_66:                              #   in Loop: Header=BB10_9 Depth=1
	testl	%r10d, %r10d
	jne	.LBB10_85
# BB#67:                                #   in Loop: Header=BB10_9 Depth=1
	incl	%r13d
	cmpl	-16(%rsp), %r13d        # 4-byte Folded Reload
	jl	.LBB10_85
	jmp	.LBB10_91
.LBB10_68:                              #   in Loop: Header=BB10_9 Depth=1
	xorl	%eax, %eax
	movq	-64(%rsp), %rbp         # 8-byte Reload
	cmpl	88(%rdi), %ebp
	jge	.LBB10_91
# BB#69:                                #   in Loop: Header=BB10_9 Depth=1
	movq	32(%rdi), %rdx
	movslq	%ebp, %rbp
	movq	(%rdx,%rbp,8), %rdx
	movzbl	112(%rdx), %ebp
	leal	(%rbp,%r13), %r9d
	cmpl	-40(%rsp), %r9d         # 4-byte Folded Reload
	jge	.LBB10_91
# BB#70:                                # %.preheader
                                        #   in Loop: Header=BB10_9 Depth=1
	testb	%bpl, %bpl
	je	.LBB10_74
# BB#71:                                # %.lr.ph255
                                        #   in Loop: Header=BB10_9 Depth=1
	movq	-8(%rsp), %rdx          # 8-byte Reload
	movq	-32(%rsp), %rbx         # 8-byte Reload
	leaq	(%rdx,%rbx,4), %r10
	xorl	%edx, %edx
.LBB10_72:                              #   Parent Loop BB10_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r10,%rdx,4), %ebx
	andl	$59, %ebx
	orl	$4, %ebx
	cmpl	$4, %ebx
	jne	.LBB10_91
# BB#73:                                #   in Loop: Header=BB10_72 Depth=2
	incq	%rdx
	cmpq	%rbp, %rdx
	jl	.LBB10_72
.LBB10_74:                              # %._crit_edge256
                                        #   in Loop: Header=BB10_9 Depth=1
	cmpl	$255, %ecx
	cmovel	%r13d, %r9d
	movl	%r9d, %r13d
	jmp	.LBB10_85
.LBB10_75:                              #   in Loop: Header=BB10_9 Depth=1
	xorl	%eax, %eax
	cmpb	$2, %r8b
	jne	.LBB10_91
# BB#76:                                #   in Loop: Header=BB10_9 Depth=1
	movq	-64(%rsp), %rdx         # 8-byte Reload
	testl	%edx, %edx
	jne	.LBB10_80
# BB#77:                                #   in Loop: Header=BB10_9 Depth=1
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movq	-32(%rsp), %rdx         # 8-byte Reload
	movl	4(%rcx,%rdx,4), %ebp
	movl	%ebp, %ebx
	andb	$63, %bl
	cmpb	$34, %bl
	ja	.LBB10_91
# BB#78:                                #   in Loop: Header=BB10_9 Depth=1
	movl	%ebp, %edx
	andl	$63, %edx
	movabsq	$19058917376, %rcx      # imm = 0x470000000
	btq	%rdx, %rcx
	jae	.LBB10_91
# BB#79:                                # %luaG_checkopenop.exit
                                        #   in Loop: Header=BB10_9 Depth=1
	cmpl	$8388607, %ebp          # imm = 0x7FFFFF
	movl	-52(%rsp), %ecx         # 4-byte Reload
	movq	-64(%rsp), %rdx         # 8-byte Reload
	ja	.LBB10_91
.LBB10_80:                              #   in Loop: Header=BB10_9 Depth=1
	leal	-1(%r12,%rdx), %edx
	cmpl	%r15d, %edx
	jle	.LBB10_85
	jmp	.LBB10_91
.LBB10_81:                              #   in Loop: Header=BB10_9 Depth=1
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movq	-32(%rsp), %rdx         # 8-byte Reload
	movl	4(%rcx,%rdx,4), %ebp
	movl	%ebp, %ebx
	andb	$63, %bl
	cmpb	$34, %bl
	ja	.LBB10_91
# BB#82:                                #   in Loop: Header=BB10_9 Depth=1
	movl	%ebp, %edx
	andl	$63, %edx
	movabsq	$19058917376, %rcx      # imm = 0x470000000
	btq	%rdx, %rcx
	movl	-52(%rsp), %ecx         # 4-byte Reload
	jae	.LBB10_91
# BB#83:                                # %luaG_checkopenop.exit225
                                        #   in Loop: Header=BB10_9 Depth=1
	cmpl	$8388608, %ebp          # imm = 0x800000
	jae	.LBB10_91
.LBB10_84:                              #   in Loop: Header=BB10_9 Depth=1
	cmpl	%ecx, %r12d
	cmovlel	%r13d, %r11d
.LBB10_85:                              # %.thread238
                                        #   in Loop: Header=BB10_9 Depth=1
	incl	%r13d
	cmpl	%esi, %r13d
	jl	.LBB10_9
# BB#86:                                # %._crit_edge262.loopexit
	movslq	%r11d, %rax
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rax,4), %eax
	jmp	.LBB10_91
.LBB10_90:
	xorl	%eax, %eax
	jmp	.LBB10_91
.Lfunc_end10:
	.size	symbexec, .Lfunc_end10-symbexec
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI10_0:
	.quad	.LBB10_53
	.quad	.LBB10_56
	.quad	.LBB10_45
	.quad	.LBB10_87
	.quad	.LBB10_85
	.quad	.LBB10_87
	.quad	.LBB10_45
	.quad	.LBB10_85
	.quad	.LBB10_85
	.quad	.LBB10_57
	.quad	.LBB10_85
	.quad	.LBB10_85
	.quad	.LBB10_85
	.quad	.LBB10_85
	.quad	.LBB10_85
	.quad	.LBB10_85
	.quad	.LBB10_85
	.quad	.LBB10_85
	.quad	.LBB10_85
	.quad	.LBB10_89
	.quad	.LBB10_52
	.quad	.LBB10_85
	.quad	.LBB10_85
	.quad	.LBB10_85
	.quad	.LBB10_85
	.quad	.LBB10_85
	.quad	.LBB10_46
	.quad	.LBB10_46
	.quad	.LBB10_59
	.quad	.LBB10_51
	.quad	.LBB10_51
	.quad	.LBB10_61
	.quad	.LBB10_64
	.quad	.LBB10_85
	.quad	.LBB10_68
	.quad	.LBB10_75

	.text
	.hidden	luaG_typeerror
	.globl	luaG_typeerror
	.p2align	4, 0x90
	.type	luaG_typeerror,@function
luaG_typeerror:                         # @luaG_typeerror
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 48
.Lcfi45:
	.cfi_offset %rbx, -32
.Lcfi46:
	.cfi_offset %r14, -24
.Lcfi47:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movq	$0, 8(%rsp)
	movslq	8(%rax), %rcx
	movq	luaT_typenames(,%rcx,8), %r15
	movq	40(%rbx), %rsi
	movq	(%rsi), %rcx
	movq	16(%rsi), %rdx
	cmpq	%rdx, %rcx
	jb	.LBB11_4
	jmp	.LBB11_2
	.p2align	4, 0x90
.LBB11_3:                               #   in Loop: Header=BB11_4 Depth=1
	addq	$16, %rcx
	cmpq	%rdx, %rcx
	jae	.LBB11_2
.LBB11_4:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rcx, %rax
	jne	.LBB11_3
# BB#5:
	subq	24(%rbx), %rax
	shrq	$4, %rax
	leaq	8(%rsp), %rcx
	movq	%rbx, %rdi
	movl	%eax, %edx
	callq	getobjname
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB11_2
# BB#6:
	movq	8(%rsp), %r8
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	movq	%r15, %r9
	callq	luaG_runerror
	jmp	.LBB11_7
.LBB11_2:                               # %.thread
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	luaG_runerror
.LBB11_7:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	luaG_typeerror, .Lfunc_end11-luaG_typeerror
	.cfi_endproc

	.p2align	4, 0x90
	.type	getobjname,@function
getobjname:                             # @getobjname
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 64
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %ebx
	movq	%rsi, %r12
	movq	%rdi, %r13
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB12_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %rcx
	cmpl	$6, 8(%rcx)
	jne	.LBB12_14
# BB#2:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	(%rcx), %rax
	cmpb	$0, 10(%rax)
	jne	.LBB12_14
# BB#3:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	32(%rax), %rbp
	movl	$-1, %r15d
	cmpb	$0, 10(%rax)
	jne	.LBB12_8
# BB#4:                                 #   in Loop: Header=BB12_1 Depth=1
	cmpq	%r12, 40(%r13)
	je	.LBB12_6
# BB#5:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB12_1 Depth=1
	movq	24(%r12), %r15
	jmp	.LBB12_7
.LBB12_6:                               #   in Loop: Header=BB12_1 Depth=1
	movq	48(%r13), %r15
	movq	%r15, 24(%r12)
	movq	(%rcx), %rax
.LBB12_7:                               #   in Loop: Header=BB12_1 Depth=1
	movq	32(%rax), %rax
	subq	24(%rax), %r15
	shrq	$2, %r15
	decl	%r15d
.LBB12_8:                               # %currentpc.exit
                                        #   in Loop: Header=BB12_1 Depth=1
	leal	1(%rbx), %esi
	movq	%rbp, %rdi
	movl	%r15d, %edx
	callq	luaF_getlocalname
	movq	%rax, (%r14)
	testq	%rax, %rax
	jne	.LBB12_12
# BB#9:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	%rbp, %rdi
	movl	%r15d, %esi
	movl	%ebx, %edx
	callq	symbexec
	movl	%eax, %ebx
	andb	$63, %al
	cmpb	$11, %al
	ja	.LBB12_14
# BB#10:                                #   in Loop: Header=BB12_1 Depth=1
	movl	%ebx, %eax
	andl	$63, %eax
	jmpq	*.LJTI12_0(,%rax,8)
.LBB12_11:                              #   in Loop: Header=BB12_1 Depth=1
	movl	%ebx, %eax
	shrl	$6, %eax
	movzbl	%al, %eax
	shrl	$23, %ebx
	cmpl	%eax, %ebx
	jb	.LBB12_1
	jmp	.LBB12_14
.LBB12_12:
	movl	$.L.str.14, %eax
.LBB12_13:                              # %.thread
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB12_14:                              # %.thread
	movq	(%rsp), %rax            # 8-byte Reload
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_15:
	movq	56(%rbp), %rax
	testq	%rax, %rax
	je	.LBB12_26
# BB#16:
	shrl	$23, %ebx
	movq	(%rax,%rbx,8), %rax
	addq	$24, %rax
	jmp	.LBB12_27
.LBB12_17:
	shrl	$14, %ebx
	testb	$1, %bh
	je	.LBB12_28
# BB#22:
	movq	16(%rbp), %rax
	movzbl	%bl, %ecx
	shlq	$4, %rcx
	cmpl	$4, 8(%rax,%rcx)
	jne	.LBB12_28
# BB#23:
	movq	(%rax,%rcx), %rax
	addq	$24, %rax
	jmp	.LBB12_29
.LBB12_19:
	shrl	$14, %ebx
	movq	16(%rbp), %rax
	shlq	$4, %rbx
	movq	(%rax,%rbx), %rax
	addq	$24, %rax
	movq	%rax, (%r14)
	movl	$.L.str.15, %eax
	jmp	.LBB12_13
.LBB12_20:
	shrl	$14, %ebx
	testb	$1, %bh
	je	.LBB12_30
# BB#24:
	movq	16(%rbp), %rax
	movzbl	%bl, %ecx
	shlq	$4, %rcx
	cmpl	$4, 8(%rax,%rcx)
	jne	.LBB12_30
# BB#25:
	movq	(%rax,%rcx), %rax
	addq	$24, %rax
	jmp	.LBB12_31
.LBB12_28:
	movl	$.L.str.17, %eax
.LBB12_29:                              # %kname.exit
	movq	%rax, (%r14)
	movl	$.L.str.19, %eax
	jmp	.LBB12_13
.LBB12_30:
	movl	$.L.str.17, %eax
.LBB12_31:                              # %kname.exit62
	movq	%rax, (%r14)
	movl	$.L.str.16, %eax
	jmp	.LBB12_13
.LBB12_26:
	movl	$.L.str.17, %eax
.LBB12_27:
	movq	%rax, (%r14)
	movl	$.L.str.18, %eax
	jmp	.LBB12_13
.Lfunc_end12:
	.size	getobjname, .Lfunc_end12-getobjname
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI12_0:
	.quad	.LBB12_11
	.quad	.LBB12_14
	.quad	.LBB12_14
	.quad	.LBB12_14
	.quad	.LBB12_15
	.quad	.LBB12_19
	.quad	.LBB12_20
	.quad	.LBB12_14
	.quad	.LBB12_14
	.quad	.LBB12_14
	.quad	.LBB12_14
	.quad	.LBB12_17

	.text
	.hidden	luaG_runerror
	.globl	luaG_runerror
	.p2align	4, 0x90
	.type	luaG_runerror,@function
luaG_runerror:                          # @luaG_runerror
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 40
	subq	$280, %rsp              # imm = 0x118
.Lcfi65:
	.cfi_def_cfa_offset 320
.Lcfi66:
	.cfi_offset %rbx, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	testb	%al, %al
	je	.LBB13_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB13_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	320(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$16, (%rsp)
	movq	%rsp, %rdx
	movq	%rbx, %rdi
	callq	luaO_pushvfstring
	movq	%rax, %r14
	movq	40(%rbx), %rax
	movq	8(%rax), %rdx
	cmpl	$6, 8(%rdx)
	jne	.LBB13_10
# BB#3:
	movq	(%rdx), %rcx
	cmpb	$0, 10(%rcx)
	jne	.LBB13_10
# BB#4:                                 # %currentpc.exit.i.i
	movq	48(%rbx), %rcx
	movq	%rcx, 24(%rax)
	movq	(%rdx), %rax
	movq	32(%rax), %rdx
	subq	24(%rdx), %rcx
	movq	%rcx, %rsi
	shrq	$2, %rsi
	testl	%esi, %esi
	jle	.LBB13_5
# BB#6:
	movq	40(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB13_7
# BB#8:
	shlq	$30, %rcx
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	addq	%rcx, %rsi
	sarq	$30, %rsi
	movl	(%rdx,%rsi), %r15d
	jmp	.LBB13_9
.LBB13_5:
	movl	$-1, %r15d
	jmp	.LBB13_9
.LBB13_7:
	xorl	%r15d, %r15d
.LBB13_9:                               # %currentline.exit.i
	movq	32(%rax), %rax
	movq	64(%rax), %rsi
	addq	$24, %rsi
	leaq	208(%rsp), %rbp
	movl	$60, %edx
	movq	%rbp, %rdi
	callq	luaO_chunkid
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	movl	%r15d, %ecx
	movq	%r14, %r8
	callq	luaO_pushfstring
.LBB13_10:                              # %addinfo.exit
	movq	%rbx, %rdi
	callq	luaG_errormsg
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	luaG_runerror, .Lfunc_end13-luaG_runerror
	.cfi_endproc

	.hidden	luaG_concaterror
	.globl	luaG_concaterror
	.p2align	4, 0x90
	.type	luaG_concaterror,@function
luaG_concaterror:                       # @luaG_concaterror
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi72:
	.cfi_def_cfa_offset 32
.Lcfi73:
	.cfi_offset %rbx, -24
.Lcfi74:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	8(%rsi), %eax
	addl	$-3, %eax
	cmpl	$2, %eax
	cmovaeq	%rsi, %rdx
	movq	$0, (%rsp)
	movslq	8(%rdx), %rax
	movq	luaT_typenames(,%rax,8), %r14
	movq	40(%rbx), %rsi
	movq	(%rsi), %rax
	movq	16(%rsi), %rcx
	cmpq	%rcx, %rax
	jb	.LBB14_4
	jmp	.LBB14_2
	.p2align	4, 0x90
.LBB14_3:                               #   in Loop: Header=BB14_4 Depth=1
	addq	$16, %rax
	cmpq	%rcx, %rax
	jae	.LBB14_2
.LBB14_4:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rax, %rdx
	jne	.LBB14_3
# BB#5:
	subq	24(%rbx), %rdx
	shrq	$4, %rdx
	movq	%rsp, %rcx
	movq	%rbx, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	getobjname
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB14_2
# BB#6:
	movq	(%rsp), %r8
	movl	$.L.str, %esi
	movl	$.L.str.2, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %r9
	callq	luaG_runerror
	jmp	.LBB14_7
.LBB14_2:                               # %.thread.i
	movl	$.L.str.1, %esi
	movl	$.L.str.2, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rcx
	callq	luaG_runerror
.LBB14_7:                               # %luaG_typeerror.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end14:
	.size	luaG_concaterror, .Lfunc_end14-luaG_concaterror
	.cfi_endproc

	.hidden	luaG_aritherror
	.globl	luaG_aritherror
	.p2align	4, 0x90
	.type	luaG_aritherror,@function
luaG_aritherror:                        # @luaG_aritherror
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi78:
	.cfi_def_cfa_offset 64
.Lcfi79:
	.cfi_offset %rbx, -32
.Lcfi80:
	.cfi_offset %r14, -24
.Lcfi81:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	leaq	16(%rsp), %rsi
	movq	%r14, %rdi
	callq	luaV_tonumber
	testq	%rax, %rax
	cmoveq	%r14, %rbx
	movq	$0, 8(%rsp)
	movslq	8(%rbx), %rax
	movq	luaT_typenames(,%rax,8), %r14
	movq	40(%r15), %rsi
	movq	(%rsi), %rax
	movq	16(%rsi), %rcx
	cmpq	%rcx, %rax
	jb	.LBB15_4
	jmp	.LBB15_2
	.p2align	4, 0x90
.LBB15_3:                               #   in Loop: Header=BB15_4 Depth=1
	addq	$16, %rax
	cmpq	%rcx, %rax
	jae	.LBB15_2
.LBB15_4:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rax, %rbx
	jne	.LBB15_3
# BB#5:
	subq	24(%r15), %rbx
	shrq	$4, %rbx
	leaq	8(%rsp), %rcx
	movq	%r15, %rdi
	movl	%ebx, %edx
	callq	getobjname
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB15_2
# BB#6:
	movq	8(%rsp), %r8
	movl	$.L.str, %esi
	movl	$.L.str.3, %edx
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r14, %r9
	callq	luaG_runerror
	jmp	.LBB15_7
.LBB15_2:                               # %.thread.i
	movl	$.L.str.1, %esi
	movl	$.L.str.3, %edx
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r14, %rcx
	callq	luaG_runerror
.LBB15_7:                               # %luaG_typeerror.exit
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	luaG_aritherror, .Lfunc_end15-luaG_aritherror
	.cfi_endproc

	.hidden	luaG_ordererror
	.globl	luaG_ordererror
	.p2align	4, 0x90
	.type	luaG_ordererror,@function
luaG_ordererror:                        # @luaG_ordererror
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi82:
	.cfi_def_cfa_offset 16
	movq	%rdx, %rax
	movslq	8(%rsi), %rcx
	movq	luaT_typenames(,%rcx,8), %rdx
	movslq	8(%rax), %rax
	movq	luaT_typenames(,%rax,8), %rcx
	movb	2(%rdx), %al
	cmpb	2(%rcx), %al
	jne	.LBB16_2
# BB#1:
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	callq	luaG_runerror
	jmp	.LBB16_3
.LBB16_2:
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	callq	luaG_runerror
.LBB16_3:
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end16:
	.size	luaG_ordererror, .Lfunc_end16-luaG_ordererror
	.cfi_endproc

	.hidden	luaG_errormsg
	.globl	luaG_errormsg
	.p2align	4, 0x90
	.type	luaG_errormsg,@function
luaG_errormsg:                          # @luaG_errormsg
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 32
.Lcfi86:
	.cfi_offset %rbx, -32
.Lcfi87:
	.cfi_offset %r14, -24
.Lcfi88:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	176(%rbx), %r14
	testq	%r14, %r14
	je	.LBB17_6
# BB#1:
	movq	64(%rbx), %r15
	cmpl	$6, 8(%r15,%r14)
	je	.LBB17_3
# BB#2:
	movl	$5, %esi
	movq	%rbx, %rdi
	callq	luaD_throw
.LBB17_3:
	movq	16(%rbx), %rax
	movq	-16(%rax), %rcx
	movq	%rcx, (%rax)
	movl	-8(%rax), %ecx
	movl	%ecx, 8(%rax)
	movq	16(%rbx), %rax
	movq	(%r15,%r14), %rcx
	movq	%rcx, -16(%rax)
	movl	8(%r15,%r14), %ecx
	movl	%ecx, -8(%rax)
	movq	16(%rbx), %rsi
	movq	56(%rbx), %rax
	subq	%rsi, %rax
	cmpq	$16, %rax
	jg	.LBB17_5
# BB#4:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	luaD_growstack
	movq	16(%rbx), %rsi
.LBB17_5:
	leaq	16(%rsi), %rax
	movq	%rax, 16(%rbx)
	addq	$-16, %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	luaD_call
.LBB17_6:
	movl	$2, %esi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	luaD_throw              # TAILCALL
.Lfunc_end17:
	.size	luaG_errormsg, .Lfunc_end17-luaG_errormsg
	.cfi_endproc

	.hidden	luaT_typenames
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"attempt to %s %s '%s' (a %s value)"
	.size	.L.str, 35

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"attempt to %s a %s value"
	.size	.L.str.1, 25

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"concatenate"
	.size	.L.str.2, 12

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"perform arithmetic on"
	.size	.L.str.3, 22

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"attempt to compare two %s values"
	.size	.L.str.4, 33

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"attempt to compare %s with %s"
	.size	.L.str.5, 30

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"(*temporary)"
	.size	.L.str.6, 13

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.zero	1
	.size	.L.str.7, 1

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"tail"
	.size	.L.str.8, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"=(tail call)"
	.size	.L.str.9, 13

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"=[C]"
	.size	.L.str.10, 5

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"C"
	.size	.L.str.11, 2

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"main"
	.size	.L.str.12, 5

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Lua"
	.size	.L.str.13, 4

	.hidden	luaP_opmodes
	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"local"
	.size	.L.str.14, 6

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"global"
	.size	.L.str.15, 7

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"field"
	.size	.L.str.16, 6

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"?"
	.size	.L.str.17, 2

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"upvalue"
	.size	.L.str.18, 8

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"method"
	.size	.L.str.19, 7

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"%s:%d: %s"
	.size	.L.str.20, 10

	.hidden	luaA_pushobject
	.hidden	luaD_growstack
	.hidden	luaV_tonumber
	.hidden	luaD_throw
	.hidden	luaD_call
	.hidden	luaO_pushvfstring
	.hidden	luaF_getlocalname
	.hidden	luaO_chunkid
	.hidden	luaH_new
	.hidden	luaH_setnum
	.hidden	luaO_pushfstring

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
