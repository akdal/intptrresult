	.text
	.file	"gim_tri_collision.bc"
	.globl	_ZNK12GIM_TRIANGLE26collide_triangle_hard_testERKS_R25GIM_TRIANGLE_CONTACT_DATA
	.p2align	4, 0x90
	.type	_ZNK12GIM_TRIANGLE26collide_triangle_hard_testERKS_R25GIM_TRIANGLE_CONTACT_DATA,@function
_ZNK12GIM_TRIANGLE26collide_triangle_hard_testERKS_R25GIM_TRIANGLE_CONTACT_DATA: # @_ZNK12GIM_TRIANGLE26collide_triangle_hard_testERKS_R25GIM_TRIANGLE_CONTACT_DATA
	.cfi_startproc
# BB#0:
	subq	$1016, %rsp             # imm = 0x3F8
.Lcfi0:
	.cfi_def_cfa_offset 1024
	movq	%rdx, %r10
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	leaq	4(%rdi), %rax
	leaq	20(%rdi), %rdx
	leaq	36(%rdi), %rcx
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	leaq	4(%rsi), %r8
	leaq	20(%rsi), %r9
	leaq	36(%rsi), %r11
	movq	%rsp, %rdi
	movq	%rax, %rsi
	pushq	%r10
.Lcfi1:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi2:
	.cfi_adjust_cfa_offset 8
	callq	_ZN30GIM_TRIANGLE_CALCULATION_CACHE18triangle_collisionERK9btVector3S2_S2_fS2_S2_S2_fR25GIM_TRIANGLE_CONTACT_DATA
	addq	$1032, %rsp             # imm = 0x408
.Lcfi3:
	.cfi_adjust_cfa_offset -16
	retq
.Lfunc_end0:
	.size	_ZNK12GIM_TRIANGLE26collide_triangle_hard_testERKS_R25GIM_TRIANGLE_CONTACT_DATA, .Lfunc_end0-_ZNK12GIM_TRIANGLE26collide_triangle_hard_testERKS_R25GIM_TRIANGLE_CONTACT_DATA
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	869711765               # float 1.00000001E-7
.LCPI1_1:
	.long	3204448256              # float -0.5
.LCPI1_2:
	.long	1069547520              # float 1.5
.LCPI1_3:
	.long	2139095039              # float 3.40282347E+38
.LCPI1_4:
	.long	1077936128              # float 3
.LCPI1_6:
	.long	3296329728              # float -1000
.LCPI1_7:
	.long	872415232               # float 1.1920929E-7
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_5:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZN30GIM_TRIANGLE_CALCULATION_CACHE18triangle_collisionERK9btVector3S2_S2_fS2_S2_S2_fR25GIM_TRIANGLE_CONTACT_DATA,"axG",@progbits,_ZN30GIM_TRIANGLE_CALCULATION_CACHE18triangle_collisionERK9btVector3S2_S2_fS2_S2_S2_fR25GIM_TRIANGLE_CONTACT_DATA,comdat
	.weak	_ZN30GIM_TRIANGLE_CALCULATION_CACHE18triangle_collisionERK9btVector3S2_S2_fS2_S2_S2_fR25GIM_TRIANGLE_CONTACT_DATA
	.p2align	4, 0x90
	.type	_ZN30GIM_TRIANGLE_CALCULATION_CACHE18triangle_collisionERK9btVector3S2_S2_fS2_S2_S2_fR25GIM_TRIANGLE_CONTACT_DATA,@function
_ZN30GIM_TRIANGLE_CALCULATION_CACHE18triangle_collisionERK9btVector3S2_S2_fS2_S2_S2_fR25GIM_TRIANGLE_CONTACT_DATA: # @_ZN30GIM_TRIANGLE_CALCULATION_CACHE18triangle_collisionERK9btVector3S2_S2_fS2_S2_S2_fR25GIM_TRIANGLE_CONTACT_DATA
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 40
	subq	$232, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 272
.Lcfi9:
	.cfi_offset %rbx, -40
.Lcfi10:
	.cfi_offset %r12, -32
.Lcfi11:
	.cfi_offset %r14, -24
.Lcfi12:
	.cfi_offset %r15, -16
	movaps	%xmm0, %xmm2
	movq	%rdi, %rbx
	movq	272(%rsp), %rax
	addss	%xmm1, %xmm2
	movss	%xmm2, (%rbx)
	movups	(%rsi), %xmm0
	movups	%xmm0, 4(%rbx)
	movups	(%rdx), %xmm0
	movups	%xmm0, 20(%rbx)
	movups	(%rcx), %xmm0
	movups	%xmm0, 36(%rbx)
	movups	(%r8), %xmm0
	movups	%xmm0, 52(%rbx)
	movups	(%r9), %xmm0
	movups	%xmm0, 68(%rbx)
	movups	(%rax), %xmm0
	movups	%xmm0, 84(%rbx)
	movss	68(%rbx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movss	52(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	56(%rbx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm4
	subss	%xmm5, %xmm4
	movss	72(%rbx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm3
	subss	%xmm12, %xmm3
	movss	76(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	60(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm9
	subss	%xmm8, %xmm9
	movss	84(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm6
	movss	88(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm12, %xmm1
	movss	92(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm7
	movaps	%xmm9, %xmm0
	mulss	%xmm6, %xmm9
	mulss	%xmm3, %xmm6
	mulss	%xmm7, %xmm3
	mulss	%xmm1, %xmm0
	subss	%xmm0, %xmm3
	movss	%xmm3, 116(%rbx)
	mulss	%xmm4, %xmm7
	subss	%xmm7, %xmm9
	movss	%xmm9, 120(%rbx)
	mulss	%xmm4, %xmm1
	subss	%xmm6, %xmm1
	movss	%xmm1, 124(%rbx)
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm9, %xmm6
	mulss	%xmm6, %xmm6
	addss	%xmm0, %xmm6
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm4
	addss	%xmm6, %xmm4
	movss	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm4, %xmm0
	movss	%xmm10, 8(%rsp)         # 4-byte Spill
	movss	%xmm2, 12(%rsp)         # 4-byte Spill
	movss	%xmm11, 4(%rsp)         # 4-byte Spill
	movss	%xmm13, (%rsp)          # 4-byte Spill
	jae	.LBB1_3
# BB#1:
	movd	%xmm4, %eax
	mulss	.LCPI1_1(%rip), %xmm4
	shrl	%eax
	movl	$1597463007, %ecx       # imm = 0x5F3759DF
	subl	%eax, %ecx
	movd	%ecx, %xmm0
	mulss	%xmm0, %xmm4
	mulss	%xmm0, %xmm4
	addss	.LCPI1_2(%rip), %xmm4
	mulss	%xmm0, %xmm4
	movss	.LCPI1_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm4, %xmm0
	jbe	.LBB1_3
# BB#2:
	mulss	%xmm4, %xmm3
	movss	%xmm3, 116(%rbx)
	mulss	%xmm4, %xmm9
	movss	%xmm9, 120(%rbx)
	mulss	%xmm4, %xmm1
	movss	%xmm1, 124(%rbx)
.LBB1_3:                                # %.thread
	movss	4(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movaps	%xmm0, 112(%rsp)        # 16-byte Spill
	movaps	%xmm5, 32(%rsp)         # 16-byte Spill
	unpcklps	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1]
	mulps	%xmm4, %xmm0
	movaps	%xmm9, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	movaps	%xmm2, 144(%rsp)        # 16-byte Spill
	movaps	%xmm12, 48(%rsp)        # 16-byte Spill
	unpcklps	%xmm12, %xmm2   # xmm2 = xmm2[0],xmm12[0],xmm2[1],xmm12[1]
	mulps	%xmm6, %xmm2
	addps	%xmm0, %xmm2
	movss	12(%rbx), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	movaps	%xmm14, 128(%rsp)       # 16-byte Spill
	movaps	%xmm8, 64(%rsp)         # 16-byte Spill
	unpcklps	%xmm8, %xmm14   # xmm14 = xmm14[0],xmm8[0],xmm14[1],xmm8[1]
	mulps	%xmm7, %xmm14
	addps	%xmm2, %xmm14
	movaps	%xmm14, %xmm15
	shufps	$229, %xmm15, %xmm15    # xmm15 = xmm15[1,1,2,3]
	movss	%xmm15, 128(%rbx)
	subss	%xmm15, %xmm14
	movss	%xmm14, 196(%rbx)
	movss	20(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	28(%rbx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movss	36(%rbx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	unpcklps	%xmm12, %xmm0   # xmm0 = xmm0[0],xmm12[0],xmm0[1],xmm12[1]
	mulps	%xmm4, %xmm0
	movss	40(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm2, 96(%rsp)         # 16-byte Spill
	unpcklps	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1]
	mulps	%xmm6, %xmm2
	addps	%xmm0, %xmm2
	movss	44(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm11, 16(%rsp)        # 16-byte Spill
	unpcklps	%xmm10, %xmm11  # xmm11 = xmm11[0],xmm10[0],xmm11[1],xmm10[1]
	mulps	%xmm7, %xmm11
	addps	%xmm2, %xmm11
	subps	%xmm15, %xmm11
	movss	%xmm11, 200(%rbx)
	movaps	%xmm14, %xmm6
	mulss	%xmm11, %xmm6
	movaps	%xmm11, %xmm13
	shufps	$229, %xmm13, %xmm13    # xmm13 = xmm13[1,1,2,3]
	movss	%xmm13, 204(%rbx)
	movss	%xmm6, 212(%rbx)
	movaps	%xmm13, %xmm0
	mulss	%xmm14, %xmm0
	movss	%xmm0, 216(%rbx)
	xorps	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm0
	leaq	116(%rbx), %r14
	leaq	196(%rbx), %rax
	jbe	.LBB1_11
# BB#4:                                 # %.thread
	ucomiss	%xmm4, %xmm6
	jbe	.LBB1_11
# BB#5:
	xorps	%xmm0, %xmm0
	xorl	%ecx, %ecx
	ucomiss	%xmm14, %xmm0
	jbe	.LBB1_9
# BB#6:
	ucomiss	%xmm11, %xmm13
	seta	%cl
	leaq	200(%rbx,%rcx,4), %rdx
	movss	200(%rbx,%rcx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm14, %xmm0
	cmovaq	%rdx, %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	xorps	.LCPI1_5(%rip), %xmm0
	movss	%xmm0, 180(%rbx)
	ucomiss	12(%rsp), %xmm0         # 4-byte Folded Reload
	jbe	.LBB1_8
# BB#7:
	xorl	%eax, %eax
	jmp	.LBB1_65
.LBB1_11:
	movaps	%xmm14, %xmm0
	addss	%xmm11, %xmm0
	addss	%xmm13, %xmm0
	divss	.LCPI1_4(%rip), %xmm0
	leaq	180(%rbx), %rcx
	movss	%xmm0, 180(%rbx)
	ucomiss	%xmm0, %xmm4
	jbe	.LBB1_13
# BB#12:
	movaps	32(%rsp), %xmm2         # 16-byte Reload
	movss	(%rsp), %xmm4           # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm2
	movaps	%xmm2, %xmm0
	subss	%xmm4, %xmm0
	movss	%xmm0, 68(%rbx)
	subss	%xmm0, %xmm2
	movss	%xmm2, 52(%rbx)
	movaps	48(%rsp), %xmm2         # 16-byte Reload
	movss	4(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm2
	movaps	%xmm2, %xmm0
	subss	%xmm4, %xmm0
	movss	%xmm0, 72(%rbx)
	subss	%xmm0, %xmm2
	movss	%xmm2, 56(%rbx)
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	movss	8(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm2
	movaps	%xmm2, %xmm0
	subss	%xmm4, %xmm0
	movss	%xmm0, 76(%rbx)
	subss	%xmm0, %xmm2
	movss	%xmm2, 60(%rbx)
	unpcklps	%xmm15, %xmm9   # xmm9 = xmm9[0],xmm15[0],xmm9[1],xmm15[1]
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	unpcklps	%xmm9, %xmm3    # xmm3 = xmm3[0],xmm9[0],xmm3[1],xmm9[1]
	xorps	.LCPI1_5(%rip), %xmm3
	movups	%xmm3, 116(%rbx)
	xorl	%edx, %edx
	ucomiss	%xmm11, %xmm13
	seta	%dl
	leaq	200(%rbx,%rdx,4), %rsi
	movss	200(%rbx,%rdx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm14, %xmm0
	cmovaq	%rsi, %rax
	movl	(%rax), %eax
	movl	%eax, 180(%rbx)
	xorl	$-2147483648, %eax      # imm = 0x80000000
	movd	%eax, %xmm0
	jmp	.LBB1_14
.LBB1_9:
	ucomiss	%xmm13, %xmm11
	seta	%cl
	leaq	200(%rbx,%rcx,4), %rdx
	ucomiss	200(%rbx,%rcx,4), %xmm14
	cmovaq	%rdx, %rax
	movl	(%rax), %eax
	movl	%eax, 180(%rbx)
	movd	%eax, %xmm0
	ucomiss	12(%rsp), %xmm0         # 4-byte Folded Reload
	movaps	144(%rsp), %xmm13       # 16-byte Reload
	movaps	128(%rsp), %xmm14       # 16-byte Reload
	movaps	112(%rsp), %xmm15       # 16-byte Reload
	movaps	16(%rsp), %xmm4         # 16-byte Reload
	movaps	96(%rsp), %xmm5         # 16-byte Reload
	movaps	80(%rsp), %xmm6         # 16-byte Reload
	jbe	.LBB1_16
# BB#10:
	xorl	%eax, %eax
	jmp	.LBB1_65
.LBB1_13:
	xorl	%eax, %eax
	ucomiss	%xmm13, %xmm11
	seta	%al
	movss	200(%rbx,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	minss	%xmm14, %xmm0
.LBB1_14:                               # %.sink.split
	xorl	%eax, %eax
	jmp	.LBB1_15
.LBB1_8:
	movaps	32(%rsp), %xmm2         # 16-byte Reload
	movss	(%rsp), %xmm4           # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm2
	movaps	%xmm2, %xmm0
	subss	%xmm4, %xmm0
	movss	%xmm0, 68(%rbx)
	subss	%xmm0, %xmm2
	movss	%xmm2, 52(%rbx)
	movaps	48(%rsp), %xmm2         # 16-byte Reload
	movss	4(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm2
	movaps	%xmm2, %xmm0
	subss	%xmm4, %xmm0
	movss	%xmm0, 72(%rbx)
	subss	%xmm0, %xmm2
	movss	%xmm2, 56(%rbx)
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	movss	8(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm2
	movaps	%xmm2, %xmm0
	subss	%xmm4, %xmm0
	movss	%xmm0, 76(%rbx)
	subss	%xmm0, %xmm2
	movss	%xmm2, 60(%rbx)
	movaps	.LCPI1_5(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm3
	movss	%xmm3, 116(%rbx)
	xorps	%xmm0, %xmm9
	movss	%xmm9, 120(%rbx)
	xorps	%xmm0, %xmm1
	movss	%xmm1, 124(%rbx)
	xorps	%xmm15, %xmm0
	movl	$3, %eax
	movq	%r14, %rcx
.LBB1_15:                               # %.sink.split
	movss	%xmm0, (%rcx,%rax,4)
	movss	20(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm15         # xmm15 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	28(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movss	36(%rbx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movss	40(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	44(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
.LBB1_16:
	movaps	%xmm6, %xmm2
	subss	%xmm15, %xmm2
	movaps	%xmm5, %xmm1
	subss	%xmm13, %xmm1
	movaps	%xmm4, %xmm11
	subss	%xmm14, %xmm11
	subss	%xmm15, %xmm12
	subss	%xmm13, %xmm8
	subss	%xmm14, %xmm10
	movaps	%xmm11, %xmm3
	mulss	%xmm12, %xmm11
	mulss	%xmm1, %xmm12
	mulss	%xmm10, %xmm1
	mulss	%xmm8, %xmm3
	subss	%xmm3, %xmm1
	movss	%xmm1, 100(%rbx)
	mulss	%xmm2, %xmm10
	subss	%xmm10, %xmm11
	movss	%xmm11, 104(%rbx)
	mulss	%xmm2, %xmm8
	subss	%xmm12, %xmm8
	movss	%xmm8, 108(%rbx)
	movaps	%xmm1, %xmm2
	mulss	%xmm2, %xmm2
	movaps	%xmm11, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm8, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm3, %xmm2
	movss	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	movaps	%xmm4, 16(%rsp)         # 16-byte Spill
	movaps	%xmm6, %xmm10
	movaps	%xmm5, %xmm12
	jae	.LBB1_19
# BB#17:
	movd	%xmm2, %eax
	mulss	.LCPI1_1(%rip), %xmm2
	shrl	%eax
	movl	$1597463007, %ecx       # imm = 0x5F3759DF
	subl	%eax, %ecx
	movd	%ecx, %xmm3
	mulss	%xmm3, %xmm2
	mulss	%xmm3, %xmm2
	addss	.LCPI1_2(%rip), %xmm2
	mulss	%xmm3, %xmm2
	movss	.LCPI1_3(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm3
	jbe	.LBB1_19
# BB#18:
	mulss	%xmm2, %xmm1
	movss	%xmm1, 100(%rbx)
	mulss	%xmm2, %xmm11
	movss	%xmm11, 104(%rbx)
	mulss	%xmm2, %xmm8
	movss	%xmm8, 108(%rbx)
.LBB1_19:                               # %.thread181
	leaq	100(%rbx), %r15
	movaps	%xmm1, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	movss	52(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	56(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm15, %xmm2   # xmm2 = xmm2[0],xmm15[0],xmm2[1],xmm15[1]
	mulps	%xmm6, %xmm2
	movaps	%xmm11, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	unpcklps	%xmm13, %xmm5   # xmm5 = xmm5[0],xmm13[0],xmm5[1],xmm13[1]
	mulps	%xmm7, %xmm5
	addps	%xmm2, %xmm5
	movaps	%xmm8, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	60(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm14, %xmm3   # xmm3 = xmm3[0],xmm14[0],xmm3[1],xmm14[1]
	mulps	%xmm4, %xmm3
	addps	%xmm5, %xmm3
	movaps	%xmm3, %xmm9
	shufps	$229, %xmm9, %xmm9      # xmm9 = xmm9[1,1,2,3]
	movss	%xmm9, 112(%rbx)
	subss	%xmm9, %xmm3
	leaq	220(%rbx), %rcx
	movss	%xmm3, 220(%rbx)
	movss	84(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	72(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	mulps	%xmm6, %xmm2
	movss	88(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1]
	mulps	%xmm7, %xmm0
	addps	%xmm2, %xmm0
	movss	92(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	76(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0],xmm6[1],xmm2[1]
	mulps	%xmm4, %xmm6
	addps	%xmm0, %xmm6
	subps	%xmm9, %xmm6
	movss	%xmm6, 224(%rbx)
	movaps	%xmm3, %xmm5
	mulss	%xmm6, %xmm5
	movaps	%xmm6, %xmm7
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	movss	%xmm7, 228(%rbx)
	movss	%xmm5, 236(%rbx)
	movaps	%xmm7, %xmm0
	mulss	%xmm3, %xmm0
	movss	%xmm0, 240(%rbx)
	xorps	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm0
	jbe	.LBB1_27
# BB#20:                                # %.thread181
	ucomiss	%xmm4, %xmm5
	jbe	.LBB1_27
# BB#21:
	xorps	%xmm0, %xmm0
	xorl	%eax, %eax
	ucomiss	%xmm3, %xmm0
	jbe	.LBB1_25
# BB#22:
	ucomiss	%xmm6, %xmm7
	seta	%al
	leaq	224(%rbx,%rax,4), %rdx
	movss	224(%rbx,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm0
	cmovaq	%rdx, %rcx
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	xorps	.LCPI1_5(%rip), %xmm0
	movss	%xmm0, 184(%rbx)
	ucomiss	(%rbx), %xmm0
	jbe	.LBB1_24
# BB#23:
	xorl	%eax, %eax
	jmp	.LBB1_65
.LBB1_27:
	movaps	%xmm3, %xmm0
	addss	%xmm6, %xmm0
	addss	%xmm7, %xmm0
	divss	.LCPI1_4(%rip), %xmm0
	leaq	180(%rbx), %rdx
	leaq	184(%rbx), %rax
	movss	%xmm0, 184(%rbx)
	ucomiss	%xmm0, %xmm4
	jbe	.LBB1_29
# BB#28:
	addss	%xmm10, %xmm15
	movaps	%xmm15, %xmm0
	subss	%xmm10, %xmm0
	movss	%xmm0, 20(%rbx)
	subss	%xmm0, %xmm15
	movss	%xmm15, 4(%rbx)
	addss	%xmm12, %xmm13
	movaps	%xmm13, %xmm0
	subss	%xmm12, %xmm0
	movss	%xmm0, 24(%rbx)
	subss	%xmm0, %xmm13
	movss	%xmm13, 8(%rbx)
	movaps	16(%rsp), %xmm10        # 16-byte Reload
	addss	%xmm10, %xmm14
	movaps	%xmm14, %xmm0
	subss	%xmm10, %xmm0
	movss	%xmm0, 28(%rbx)
	subss	%xmm0, %xmm14
	movss	%xmm14, 12(%rbx)
	unpcklps	%xmm9, %xmm11   # xmm11 = xmm11[0],xmm9[0],xmm11[1],xmm9[1]
	unpcklps	%xmm8, %xmm1    # xmm1 = xmm1[0],xmm8[0],xmm1[1],xmm8[1]
	unpcklps	%xmm11, %xmm1   # xmm1 = xmm1[0],xmm11[0],xmm1[1],xmm11[1]
	xorps	.LCPI1_5(%rip), %xmm1
	movups	%xmm1, 100(%rbx)
	xorl	%esi, %esi
	ucomiss	%xmm6, %xmm7
	seta	%sil
	leaq	224(%rbx,%rsi,4), %rdi
	movss	224(%rbx,%rsi,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm0
	cmovaq	%rdi, %rcx
	movl	(%rcx), %ecx
	movl	%ecx, 184(%rbx)
	xorl	$-2147483648, %ecx      # imm = 0x80000000
	movd	%ecx, %xmm5
	jmp	.LBB1_30
.LBB1_25:
	ucomiss	%xmm7, %xmm6
	seta	%al
	leaq	224(%rbx,%rax,4), %rdx
	ucomiss	224(%rbx,%rax,4), %xmm3
	cmovaq	%rdx, %rcx
	movl	(%rcx), %eax
	movl	%eax, 184(%rbx)
	movd	%eax, %xmm0
	ucomiss	(%rbx), %xmm0
	jbe	.LBB1_32
# BB#26:
	xorl	%eax, %eax
	jmp	.LBB1_65
.LBB1_29:
	xorl	%ecx, %ecx
	ucomiss	%xmm7, %xmm6
	seta	%cl
	movss	224(%rbx,%rcx,4), %xmm5 # xmm5 = mem[0],zero,zero,zero
	minss	%xmm3, %xmm5
.LBB1_30:                               # %.sink.split51
	movl	$1, %ecx
	jmp	.LBB1_31
.LBB1_24:
	leaq	184(%rbx), %rax
	addss	%xmm10, %xmm15
	movaps	%xmm15, %xmm0
	subss	%xmm10, %xmm0
	movss	%xmm0, 20(%rbx)
	subss	%xmm0, %xmm15
	movss	%xmm15, 4(%rbx)
	addss	%xmm12, %xmm13
	movaps	%xmm13, %xmm0
	subss	%xmm12, %xmm0
	movss	%xmm0, 24(%rbx)
	subss	%xmm0, %xmm13
	movss	%xmm13, 8(%rbx)
	movaps	16(%rsp), %xmm10        # 16-byte Reload
	addss	%xmm10, %xmm14
	movaps	%xmm14, %xmm0
	subss	%xmm10, %xmm0
	movss	%xmm0, 28(%rbx)
	subss	%xmm0, %xmm14
	movss	%xmm14, 12(%rbx)
	movaps	.LCPI1_5(%rip), %xmm5   # xmm5 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm5, %xmm1
	movss	%xmm1, 100(%rbx)
	xorps	%xmm5, %xmm11
	movss	%xmm11, 104(%rbx)
	xorps	%xmm5, %xmm8
	movss	%xmm8, 108(%rbx)
	xorps	%xmm9, %xmm5
	movl	$3, %ecx
	movq	%r15, %rdx
.LBB1_31:                               # %.sink.split51
	movss	%xmm5, (%rdx,%rcx,4)
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
.LBB1_32:
	movq	280(%rsp), %r12
	leaq	4(%rbx), %rcx
	leaq	52(%rbx), %rax
	leaq	756(%rbx), %r8
	ucomiss	180(%rbx), %xmm0
	jbe	.LBB1_33
# BB#50:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rcx, %rdx
	movq	%rax, %rcx
	callq	_ZN30GIM_TRIANGLE_CALCULATION_CACHE13clip_triangleERK9btVector4PK9btVector3S5_PS3_
	movl	%eax, %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.LBB1_65
# BB#51:                                # %.lr.ph23.i.i
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movups	(%r15), %xmm1
	movups	%xmm1, 8(%r12)
	leaq	4(%r12), %r9
	movl	$3296329728, %edx       # imm = 0xC47A0000
	movq	%rdx, (%r12)
	movl	%ecx, %ecx
	leaq	764(%rbx), %rsi
	movss	.LCPI1_6(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	movss	.LCPI1_7(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	xorl	%edi, %edi
	jmp	.LBB1_52
	.p2align	4, 0x90
.LBB1_66:                               #   in Loop: Header=BB1_52 Depth=1
	addss	%xmm2, %xmm4
	ucomiss	%xmm3, %xmm4
	jb	.LBB1_68
# BB#67:                                #   in Loop: Header=BB1_52 Depth=1
	movl	%eax, %edx
	movl	%edi, 160(%rsp,%rdx,4)
	incl	%eax
	movl	%eax, (%r9)
	jmp	.LBB1_68
	.p2align	4, 0x90
.LBB1_52:                               # =>This Inner Loop Header: Depth=1
	movss	100(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	104(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	-8(%rsi), %xmm4
	mulss	-4(%rsi), %xmm5
	addss	%xmm4, %xmm5
	movss	108(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	(%rsi), %xmm6
	addss	%xmm5, %xmm6
	subss	112(%rbx), %xmm6
	movaps	%xmm0, %xmm4
	subss	%xmm6, %xmm4
	ucomiss	%xmm1, %xmm4
	jae	.LBB1_53
.LBB1_68:                               #   in Loop: Header=BB1_52 Depth=1
	movaps	%xmm3, %xmm4
	jmp	.LBB1_69
	.p2align	4, 0x90
.LBB1_53:                               #   in Loop: Header=BB1_52 Depth=1
	ucomiss	%xmm3, %xmm4
	jbe	.LBB1_66
# BB#54:                                #   in Loop: Header=BB1_52 Depth=1
	movss	%xmm4, (%r12)
	movl	%edi, 160(%rsp)
	movl	$1, 4(%r12)
	movl	$1, %eax
.LBB1_69:                               #   in Loop: Header=BB1_52 Depth=1
	incq	%rdi
	addq	$16, %rsi
	cmpq	%rdi, %rcx
	movaps	%xmm4, %xmm3
	jne	.LBB1_52
# BB#55:                                # %.preheader.i.i
	testl	%eax, %eax
	je	.LBB1_63
# BB#56:                                # %.lr.ph.i.preheader.i
	movl	%eax, %eax
	leaq	-1(%rax), %r8
	movq	%rax, %rdi
	andq	$3, %rdi
	je	.LBB1_57
# BB#58:                                # %.lr.ph.i.i.prol.preheader
	leaq	24(%r12), %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_59:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	160(%rsp,%rsi,4), %edx
	shlq	$4, %rdx
	movups	756(%rbx,%rdx), %xmm0
	movups	%xmm0, (%rcx)
	incq	%rsi
	addq	$16, %rcx
	cmpq	%rsi, %rdi
	jne	.LBB1_59
	jmp	.LBB1_60
.LBB1_33:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	_ZN30GIM_TRIANGLE_CALCULATION_CACHE13clip_triangleERK9btVector4PK9btVector3S5_PS3_
	movl	%eax, %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.LBB1_65
# BB#34:                                # %.lr.ph23.i.i166
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movups	(%r14), %xmm1
	movups	%xmm1, 8(%r12)
	leaq	4(%r12), %r9
	movl	$3296329728, %esi       # imm = 0xC47A0000
	movq	%rsi, (%r12)
	movl	%ecx, %ecx
	leaq	764(%rbx), %rsi
	movss	.LCPI1_6(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	movss	.LCPI1_7(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	xorl	%edi, %edi
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_46:                               #   in Loop: Header=BB1_35 Depth=1
	addss	%xmm2, %xmm4
	ucomiss	%xmm3, %xmm4
	jb	.LBB1_48
# BB#47:                                #   in Loop: Header=BB1_35 Depth=1
	movl	%eax, %edx
	movl	%edi, 160(%rsp,%rdx,4)
	incl	%eax
	movl	%eax, (%r9)
	jmp	.LBB1_48
	.p2align	4, 0x90
.LBB1_35:                               # =>This Inner Loop Header: Depth=1
	movss	116(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	120(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	-8(%rsi), %xmm4
	mulss	-4(%rsi), %xmm5
	addss	%xmm4, %xmm5
	movss	124(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	(%rsi), %xmm6
	addss	%xmm5, %xmm6
	subss	128(%rbx), %xmm6
	movaps	%xmm0, %xmm4
	subss	%xmm6, %xmm4
	ucomiss	%xmm1, %xmm4
	jae	.LBB1_36
.LBB1_48:                               #   in Loop: Header=BB1_35 Depth=1
	movaps	%xmm3, %xmm4
	jmp	.LBB1_49
	.p2align	4, 0x90
.LBB1_36:                               #   in Loop: Header=BB1_35 Depth=1
	ucomiss	%xmm3, %xmm4
	jbe	.LBB1_46
# BB#37:                                #   in Loop: Header=BB1_35 Depth=1
	movss	%xmm4, (%r12)
	movl	%edi, 160(%rsp)
	movl	$1, 4(%r12)
	movl	$1, %eax
.LBB1_49:                               #   in Loop: Header=BB1_35 Depth=1
	incq	%rdi
	addq	$16, %rsi
	cmpq	%rdi, %rcx
	movaps	%xmm4, %xmm3
	jne	.LBB1_35
# BB#38:                                # %.preheader.i.i167
	testl	%eax, %eax
	je	.LBB1_64
# BB#39:                                # %.lr.ph.i.preheader.i169
	movl	%eax, %eax
	leaq	-1(%rax), %r8
	movq	%rax, %rdi
	andq	$3, %rdi
	je	.LBB1_40
# BB#41:                                # %.lr.ph.i.i176.prol.preheader
	leaq	24(%r12), %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_42:                               # %.lr.ph.i.i176.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	160(%rsp,%rsi,4), %edx
	shlq	$4, %rdx
	movups	756(%rbx,%rdx), %xmm0
	movups	%xmm0, (%rcx)
	incq	%rsi
	addq	$16, %rcx
	cmpq	%rsi, %rdi
	jne	.LBB1_42
	jmp	.LBB1_43
.LBB1_57:
	xorl	%esi, %esi
.LBB1_60:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB1_63
# BB#61:                                # %.lr.ph.i.preheader.i.new
	subq	%rsi, %rax
	leaq	172(%rsp,%rsi,4), %rcx
	shlq	$4, %rsi
	leaq	72(%r12,%rsi), %rsi
	.p2align	4, 0x90
.LBB1_62:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rcx), %edx
	shlq	$4, %rdx
	movups	756(%rbx,%rdx), %xmm0
	movups	%xmm0, -48(%rsi)
	movl	-8(%rcx), %edx
	shlq	$4, %rdx
	movups	756(%rbx,%rdx), %xmm0
	movups	%xmm0, -32(%rsi)
	movl	-4(%rcx), %edx
	shlq	$4, %rdx
	movups	756(%rbx,%rdx), %xmm0
	movups	%xmm0, -16(%rsi)
	movl	(%rcx), %edx
	shlq	$4, %rdx
	movups	756(%rbx,%rdx), %xmm0
	movups	%xmm0, (%rsi)
	addq	$64, %rsi
	addq	$16, %rcx
	addq	$-4, %rax
	jne	.LBB1_62
.LBB1_63:                               # %_ZN25GIM_TRIANGLE_CONTACT_DATA12merge_pointsERK9btVector4fPK9btVector3j.exit
	movss	8(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	.LCPI1_5(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm1, %xmm0
	movss	%xmm0, 8(%r12)
	movss	12(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm0
	movss	%xmm0, 12(%r12)
	movss	16(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm0
	movss	%xmm0, 16(%r12)
	jmp	.LBB1_64
.LBB1_40:
	xorl	%esi, %esi
.LBB1_43:                               # %.lr.ph.i.i176.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB1_64
# BB#44:                                # %.lr.ph.i.preheader.i169.new
	subq	%rsi, %rax
	leaq	172(%rsp,%rsi,4), %rcx
	shlq	$4, %rsi
	leaq	72(%r12,%rsi), %rsi
	.p2align	4, 0x90
.LBB1_45:                               # %.lr.ph.i.i176
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rcx), %edx
	shlq	$4, %rdx
	movups	756(%rbx,%rdx), %xmm0
	movups	%xmm0, -48(%rsi)
	movl	-8(%rcx), %edx
	shlq	$4, %rdx
	movups	756(%rbx,%rdx), %xmm0
	movups	%xmm0, -32(%rsi)
	movl	-4(%rcx), %edx
	shlq	$4, %rdx
	movups	756(%rbx,%rdx), %xmm0
	movups	%xmm0, -16(%rsi)
	movl	(%rcx), %edx
	shlq	$4, %rdx
	movups	756(%rbx,%rdx), %xmm0
	movups	%xmm0, (%rsi)
	addq	$64, %rsi
	addq	$16, %rcx
	addq	$-4, %rax
	jne	.LBB1_45
.LBB1_64:
	cmpl	$0, (%r9)
	setne	%al
.LBB1_65:
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_ZN30GIM_TRIANGLE_CALCULATION_CACHE18triangle_collisionERK9btVector3S2_S2_fS2_S2_S2_fR25GIM_TRIANGLE_CONTACT_DATA, .Lfunc_end1-_ZN30GIM_TRIANGLE_CALCULATION_CACHE18triangle_collisionERK9btVector3S2_S2_fS2_S2_S2_fR25GIM_TRIANGLE_CONTACT_DATA
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	869711765               # float 1.00000001E-7
.LCPI2_1:
	.long	3204448256              # float -0.5
.LCPI2_2:
	.long	1069547520              # float 1.5
.LCPI2_3:
	.long	2139095039              # float 3.40282347E+38
	.section	.text._ZN30GIM_TRIANGLE_CALCULATION_CACHE13clip_triangleERK9btVector4PK9btVector3S5_PS3_,"axG",@progbits,_ZN30GIM_TRIANGLE_CALCULATION_CACHE13clip_triangleERK9btVector4PK9btVector3S5_PS3_,comdat
	.weak	_ZN30GIM_TRIANGLE_CALCULATION_CACHE13clip_triangleERK9btVector4PK9btVector3S5_PS3_
	.p2align	4, 0x90
	.type	_ZN30GIM_TRIANGLE_CALCULATION_CACHE13clip_triangleERK9btVector4PK9btVector3S5_PS3_,@function
_ZN30GIM_TRIANGLE_CALCULATION_CACHE13clip_triangleERK9btVector4PK9btVector3S5_PS3_: # @_ZN30GIM_TRIANGLE_CALCULATION_CACHE13clip_triangleERK9btVector4PK9btVector3S5_PS3_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movq	%rdi, %r15
	movss	16(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm1
	movss	20(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	%xmm9, %xmm6
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm0
	movss	4(%r12), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm5
	mulss	%xmm2, %xmm5
	movaps	%xmm0, %xmm4
	mulss	%xmm7, %xmm4
	subss	%xmm4, %xmm5
	movss	%xmm5, 8(%rsp)
	movss	(%r12), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	mulss	%xmm1, %xmm2
	subss	%xmm2, %xmm0
	movss	%xmm0, 12(%rsp)
	mulss	%xmm7, %xmm1
	mulss	%xmm4, %xmm6
	subss	%xmm6, %xmm1
	movss	%xmm1, 16(%rsp)
	movaps	%xmm5, %xmm2
	mulss	%xmm2, %xmm2
	movaps	%xmm0, %xmm4
	mulss	%xmm4, %xmm4
	addss	%xmm2, %xmm4
	movaps	%xmm1, %xmm6
	mulss	%xmm6, %xmm6
	addss	%xmm4, %xmm6
	movss	.LCPI2_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm2
	jae	.LBB2_3
# BB#1:
	movd	%xmm6, %eax
	mulss	.LCPI2_1(%rip), %xmm6
	shrl	%eax
	movl	$1597463007, %edx       # imm = 0x5F3759DF
	subl	%eax, %edx
	movd	%edx, %xmm2
	mulss	%xmm2, %xmm6
	mulss	%xmm2, %xmm6
	addss	.LCPI2_2(%rip), %xmm6
	mulss	%xmm2, %xmm6
	movss	.LCPI2_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm2
	jbe	.LBB2_3
# BB#2:
	mulss	%xmm6, %xmm5
	movss	%xmm5, 8(%rsp)
	mulss	%xmm6, %xmm0
	movss	%xmm0, 12(%rsp)
	mulss	%xmm6, %xmm1
	movss	%xmm1, 16(%rsp)
.LBB2_3:                                # %.thread
	mulss	%xmm5, %xmm8
	mulss	%xmm0, %xmm9
	addss	%xmm8, %xmm9
	mulss	%xmm1, %xmm3
	addss	%xmm9, %xmm3
	movss	%xmm3, 20(%rsp)
	leaq	244(%r15), %rbp
	leaq	8(%rsp), %rdi
	leaq	16(%rcx), %rdx
	leaq	32(%rcx), %rax
	movq	%rcx, %rsi
	movq	%rax, %rcx
	movq	%rbp, %r8
	callq	_Z27PLANE_CLIP_TRIANGLE_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_RKT_S8_S8_PS6_T1_
	xorl	%r13d, %r13d
	testl	%eax, %eax
	je	.LBB2_12
# BB#4:
	movss	32(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	16(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm1
	movss	36(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	%xmm9, %xmm6
	movss	40(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm0
	movss	4(%r12), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm5
	mulss	%xmm2, %xmm5
	movaps	%xmm0, %xmm4
	mulss	%xmm7, %xmm4
	subss	%xmm4, %xmm5
	movss	%xmm5, 8(%rsp)
	movss	(%r12), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	mulss	%xmm1, %xmm2
	subss	%xmm2, %xmm0
	movss	%xmm0, 12(%rsp)
	mulss	%xmm7, %xmm1
	mulss	%xmm4, %xmm6
	subss	%xmm6, %xmm1
	movss	%xmm1, 16(%rsp)
	movaps	%xmm5, %xmm2
	mulss	%xmm2, %xmm2
	movaps	%xmm0, %xmm4
	mulss	%xmm4, %xmm4
	addss	%xmm2, %xmm4
	movaps	%xmm1, %xmm6
	mulss	%xmm6, %xmm6
	addss	%xmm4, %xmm6
	movss	.LCPI2_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm2
	jae	.LBB2_7
# BB#5:
	movd	%xmm6, %ecx
	mulss	.LCPI2_1(%rip), %xmm6
	shrl	%ecx
	movl	$1597463007, %edx       # imm = 0x5F3759DF
	subl	%ecx, %edx
	movd	%edx, %xmm2
	mulss	%xmm2, %xmm6
	mulss	%xmm2, %xmm6
	addss	.LCPI2_2(%rip), %xmm6
	mulss	%xmm2, %xmm6
	movss	.LCPI2_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm2
	jbe	.LBB2_7
# BB#6:
	mulss	%xmm6, %xmm5
	movss	%xmm5, 8(%rsp)
	mulss	%xmm6, %xmm0
	movss	%xmm0, 12(%rsp)
	mulss	%xmm6, %xmm1
	movss	%xmm1, 16(%rsp)
.LBB2_7:                                # %.thread137
	mulss	%xmm5, %xmm8
	mulss	%xmm0, %xmm9
	addss	%xmm8, %xmm9
	mulss	%xmm1, %xmm3
	addss	%xmm9, %xmm3
	movss	%xmm3, 20(%rsp)
	addq	$500, %r15              # imm = 0x1F4
	leaq	8(%rsp), %rdi
	movq	%rbp, %rsi
	movl	%eax, %edx
	movq	%r15, %rcx
	callq	_Z26PLANE_CLIP_POLYGON_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_PKT_jPS6_T1_
	testl	%eax, %eax
	je	.LBB2_12
# BB#8:
	movss	32(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm0
	movss	36(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	subss	%xmm9, %xmm6
	movss	8(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
	movss	4(%r12), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm5
	mulss	%xmm1, %xmm5
	movaps	%xmm2, %xmm4
	mulss	%xmm7, %xmm4
	subss	%xmm4, %xmm5
	movss	%xmm5, 8(%rsp)
	movss	(%r12), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	mulss	%xmm0, %xmm1
	subss	%xmm1, %xmm2
	movss	%xmm2, 12(%rsp)
	mulss	%xmm7, %xmm0
	mulss	%xmm4, %xmm6
	subss	%xmm6, %xmm0
	movss	%xmm0, 16(%rsp)
	movaps	%xmm5, %xmm1
	mulss	%xmm1, %xmm1
	movaps	%xmm2, %xmm4
	mulss	%xmm4, %xmm4
	addss	%xmm1, %xmm4
	movaps	%xmm0, %xmm6
	mulss	%xmm6, %xmm6
	addss	%xmm4, %xmm6
	movss	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm1
	jae	.LBB2_11
# BB#9:
	movd	%xmm6, %ecx
	mulss	.LCPI2_1(%rip), %xmm6
	shrl	%ecx
	movl	$1597463007, %edx       # imm = 0x5F3759DF
	subl	%ecx, %edx
	movd	%edx, %xmm1
	mulss	%xmm1, %xmm6
	mulss	%xmm1, %xmm6
	addss	.LCPI2_2(%rip), %xmm6
	mulss	%xmm1, %xmm6
	movss	.LCPI2_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm1
	jbe	.LBB2_11
# BB#10:
	mulss	%xmm6, %xmm5
	movss	%xmm5, 8(%rsp)
	mulss	%xmm6, %xmm2
	movss	%xmm2, 12(%rsp)
	mulss	%xmm6, %xmm0
	movss	%xmm0, 16(%rsp)
.LBB2_11:                               # %.thread138
	mulss	%xmm5, %xmm8
	mulss	%xmm2, %xmm9
	addss	%xmm8, %xmm9
	mulss	%xmm0, %xmm3
	addss	%xmm9, %xmm3
	movss	%xmm3, 20(%rsp)
	leaq	8(%rsp), %rdi
	movq	%r15, %rsi
	movl	%eax, %edx
	movq	%r14, %rcx
	callq	_Z26PLANE_CLIP_POLYGON_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_PKT_jPS6_T1_
	movl	%eax, %r13d
.LBB2_12:
	movl	%r13d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN30GIM_TRIANGLE_CALCULATION_CACHE13clip_triangleERK9btVector4PK9btVector3S5_PS3_, .Lfunc_end2-_ZN30GIM_TRIANGLE_CALCULATION_CACHE13clip_triangleERK9btVector4PK9btVector3S5_PS3_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	872415232               # float 1.1920929E-7
.LCPI3_2:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._Z27PLANE_CLIP_TRIANGLE_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_RKT_S8_S8_PS6_T1_,"axG",@progbits,_Z27PLANE_CLIP_TRIANGLE_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_RKT_S8_S8_PS6_T1_,comdat
	.weak	_Z27PLANE_CLIP_TRIANGLE_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_RKT_S8_S8_PS6_T1_
	.p2align	4, 0x90
	.type	_Z27PLANE_CLIP_TRIANGLE_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_RKT_S8_S8_PS6_T1_,@function
_Z27PLANE_CLIP_TRIANGLE_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_RKT_S8_S8_PS6_T1_: # @_Z27PLANE_CLIP_TRIANGLE_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_RKT_S8_S8_PS6_T1_
	.cfi_startproc
# BB#0:
	movss	(%rdi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm0
	mulss	%xmm3, %xmm0
	movss	4(%rsi), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm0, %xmm5
	movss	8(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	addss	%xmm5, %xmm0
	movss	12(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm0
	xorl	%eax, %eax
	ucomiss	.LCPI3_0(%rip), %xmm0
	ja	.LBB3_2
# BB#1:
	movss	%xmm3, (%r8)
	movl	4(%rsi), %eax
	movl	%eax, 4(%r8)
	movl	8(%rsi), %eax
	movl	%eax, 8(%r8)
	movss	(%rdi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movl	$1, %eax
.LBB3_2:
	movss	.LCPI3_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm0
	seta	%r9b
	movss	(%rdx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm4
	mulss	4(%rdx), %xmm2
	addss	%xmm4, %xmm2
	mulss	8(%rdx), %xmm1
	addss	%xmm2, %xmm1
	subss	%xmm5, %xmm1
	ucomiss	%xmm3, %xmm1
	seta	%r10b
	cmpb	%r10b, %r9b
	je	.LBB3_4
# BB#3:
	movaps	.LCPI3_1(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm2
	movaps	%xmm1, %xmm4
	subss	%xmm0, %xmm4
	divss	%xmm4, %xmm2
	movss	.LCPI3_2(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm4
	movss	(%rsi), %xmm5           # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	mulss	%xmm2, %xmm6
	addss	%xmm5, %xmm6
	movq	%rax, %r9
	shlq	$4, %r9
	movss	%xmm6, (%r8,%r9)
	movss	4(%rsi), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	movss	4(%rdx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm6
	addss	%xmm5, %xmm6
	movss	%xmm6, 4(%r8,%r9)
	mulss	8(%rsi), %xmm4
	mulss	8(%rdx), %xmm2
	addss	%xmm4, %xmm2
	movss	%xmm2, 8(%r8,%r9)
	incl	%eax
.LBB3_4:
	ucomiss	.LCPI3_0(%rip), %xmm1
	ja	.LBB3_6
# BB#5:
	movl	(%rdx), %r9d
	movl	%eax, %r10d
	shlq	$4, %r10
	movl	%r9d, (%r8,%r10)
	movl	4(%rdx), %r9d
	movl	%r9d, 4(%r8,%r10)
	movl	8(%rdx), %r9d
	movl	%r9d, 8(%r8,%r10)
	incl	%eax
.LBB3_6:                                # %_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj.exit37
	ucomiss	%xmm3, %xmm1
	seta	%r9b
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	(%rdi), %xmm5           # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	movss	4(%rdi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	4(%rcx), %xmm6
	addss	%xmm5, %xmm6
	mulss	8(%rcx), %xmm2
	addss	%xmm6, %xmm2
	subss	12(%rdi), %xmm2
	ucomiss	%xmm3, %xmm2
	seta	%dil
	cmpb	%dil, %r9b
	je	.LBB3_8
# BB#7:
	movaps	%xmm2, %xmm3
	subss	%xmm1, %xmm3
	xorps	.LCPI3_1(%rip), %xmm1
	divss	%xmm3, %xmm1
	movss	.LCPI3_2(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm3
	movss	(%rdx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	mulss	%xmm1, %xmm4
	addss	%xmm5, %xmm4
	movl	%eax, %edi
	shlq	$4, %rdi
	movss	%xmm4, (%r8,%rdi)
	movss	4(%rdx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	movss	4(%rcx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	addss	%xmm4, %xmm5
	movss	%xmm5, 4(%r8,%rdi)
	mulss	8(%rdx), %xmm3
	mulss	8(%rcx), %xmm1
	addss	%xmm3, %xmm1
	movss	%xmm1, 8(%r8,%rdi)
	incl	%eax
.LBB3_8:
	ucomiss	.LCPI3_0(%rip), %xmm2
	jbe	.LBB3_9
# BB#10:                                # %_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj.exit36
	ucomiss	.LCPI3_0(%rip), %xmm0
	jbe	.LBB3_11
	jmp	.LBB3_13
.LBB3_9:
	movl	%eax, %edx
	shlq	$4, %rdx
	incl	%eax
	ucomiss	.LCPI3_0(%rip), %xmm0
	movl	(%rcx), %edi
	movl	%edi, (%r8,%rdx)
	movl	4(%rcx), %edi
	movl	%edi, 4(%r8,%rdx)
	movl	8(%rcx), %edi
	movl	%edi, 8(%r8,%rdx)
	jbe	.LBB3_12
.LBB3_11:
	movl	%eax, %edx
	shlq	$4, %rdx
	incl	%eax
	ucomiss	.LCPI3_0(%rip), %xmm0
	subss	%xmm2, %xmm0
	xorps	.LCPI3_1(%rip), %xmm2
	divss	%xmm0, %xmm2
	movss	.LCPI3_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm0
	movss	(%rcx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	addss	%xmm1, %xmm3
	movss	%xmm3, (%r8,%rdx)
	movss	4(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	4(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	addss	%xmm1, %xmm3
	movss	%xmm3, 4(%r8,%rdx)
	mulss	8(%rcx), %xmm0
	mulss	8(%rsi), %xmm2
	addss	%xmm0, %xmm2
	movss	%xmm2, 8(%r8,%rdx)
	ja	.LBB3_13
.LBB3_12:                               # %.thread53
	movl	(%rsi), %ecx
	movl	%eax, %edx
	shlq	$4, %rdx
	movl	%ecx, (%r8,%rdx)
	movl	4(%rsi), %ecx
	movl	%ecx, 4(%r8,%rdx)
	movl	8(%rsi), %ecx
	movl	%ecx, 8(%r8,%rdx)
	incl	%eax
.LBB3_13:                               # %_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj.exit
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end3:
	.size	_Z27PLANE_CLIP_TRIANGLE_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_RKT_S8_S8_PS6_T1_, .Lfunc_end3-_Z27PLANE_CLIP_TRIANGLE_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_RKT_S8_S8_PS6_T1_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	872415232               # float 1.1920929E-7
.LCPI4_2:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._Z26PLANE_CLIP_POLYGON_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_PKT_jPS6_T1_,"axG",@progbits,_Z26PLANE_CLIP_POLYGON_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_PKT_jPS6_T1_,comdat
	.weak	_Z26PLANE_CLIP_POLYGON_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_PKT_jPS6_T1_
	.p2align	4, 0x90
	.type	_Z26PLANE_CLIP_POLYGON_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_PKT_jPS6_T1_,@function
_Z26PLANE_CLIP_POLYGON_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_PKT_jPS6_T1_: # @_Z26PLANE_CLIP_POLYGON_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_PKT_jPS6_T1_
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	(%rdi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	4(%rdi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	4(%rsi), %xmm3
	addss	%xmm2, %xmm3
	mulss	8(%rsi), %xmm0
	addss	%xmm3, %xmm0
	subss	12(%rdi), %xmm0
	xorl	%eax, %eax
	ucomiss	.LCPI4_0(%rip), %xmm0
	ja	.LBB4_2
# BB#1:
	movss	%xmm1, (%rcx)
	movl	4(%rsi), %eax
	movl	%eax, 4(%rcx)
	movl	8(%rsi), %eax
	movl	%eax, 8(%rcx)
	movl	$1, %eax
.LBB4_2:                                # %.preheader
	cmpl	$2, %edx
	jb	.LBB4_3
# BB#9:                                 # %.lr.ph.preheader
	movl	%edx, %r8d
	leaq	24(%rsi), %r9
	decq	%r8
	movss	.LCPI4_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	.LCPI4_1(%rip), %xmm8   # xmm8 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movss	.LCPI4_2(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm5
	.p2align	4, 0x90
.LBB4_10:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	-8(%r9), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	(%rdi), %xmm7           # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm7
	movsd	4(%rdi), %xmm3          # xmm3 = mem[0],zero
	movsd	-4(%r9), %xmm1          # xmm1 = mem[0],zero
	mulps	%xmm3, %xmm1
	addss	%xmm1, %xmm7
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	addss	%xmm7, %xmm1
	subss	12(%rdi), %xmm1
	ucomiss	%xmm2, %xmm5
	seta	%r10b
	ucomiss	%xmm2, %xmm1
	seta	%r11b
	cmpb	%r11b, %r10b
	je	.LBB4_12
# BB#11:                                #   in Loop: Header=BB4_10 Depth=1
	movaps	%xmm1, %xmm3
	subss	%xmm5, %xmm3
	xorps	%xmm8, %xmm5
	divss	%xmm3, %xmm5
	movaps	%xmm4, %xmm3
	subss	%xmm5, %xmm3
	movss	-24(%r9), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm7
	mulss	%xmm5, %xmm6
	addss	%xmm7, %xmm6
	movl	%eax, %r10d
	shlq	$4, %r10
	movss	%xmm6, (%rcx,%r10)
	movss	-20(%r9), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	movss	-4(%r9), %xmm7          # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm7
	addss	%xmm6, %xmm7
	movss	%xmm7, 4(%rcx,%r10)
	mulss	-16(%r9), %xmm3
	mulss	(%r9), %xmm5
	addss	%xmm3, %xmm5
	movss	%xmm5, 8(%rcx,%r10)
	incl	%eax
.LBB4_12:                               #   in Loop: Header=BB4_10 Depth=1
	ucomiss	%xmm2, %xmm1
	ja	.LBB4_14
# BB#13:                                #   in Loop: Header=BB4_10 Depth=1
	movl	-8(%r9), %r10d
	movl	%eax, %r11d
	shlq	$4, %r11
	movl	%r10d, (%rcx,%r11)
	movl	-4(%r9), %r10d
	movl	%r10d, 4(%rcx,%r11)
	movl	(%r9), %r10d
	movl	%r10d, 8(%rcx,%r11)
	incl	%eax
.LBB4_14:                               # %_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj.exit
                                        #   in Loop: Header=BB4_10 Depth=1
	addq	$16, %r9
	decq	%r8
	movaps	%xmm1, %xmm5
	jne	.LBB4_10
	jmp	.LBB4_4
.LBB4_3:
	movaps	%xmm0, %xmm1
.LBB4_4:                                # %._crit_edge
	movss	.LCPI4_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	seta	%dil
	ucomiss	%xmm2, %xmm1
	seta	%r8b
	cmpb	%r8b, %dil
	je	.LBB4_6
# BB#5:
	decl	%edx
	movaps	%xmm0, %xmm2
	subss	%xmm1, %xmm2
	xorps	.LCPI4_1(%rip), %xmm1
	divss	%xmm2, %xmm1
	movss	.LCPI4_2(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	shlq	$4, %rdx
	movss	(%rsi,%rdx), %xmm3      # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm3, %xmm4
	movl	%eax, %edi
	shlq	$4, %rdi
	movss	%xmm4, (%rcx,%rdi)
	movss	4(%rsi,%rdx), %xmm3     # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	movss	4(%rsi), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm3, %xmm4
	movss	%xmm4, 4(%rcx,%rdi)
	mulss	8(%rsi,%rdx), %xmm2
	mulss	8(%rsi), %xmm1
	addss	%xmm2, %xmm1
	movss	%xmm1, 8(%rcx,%rdi)
	incl	%eax
.LBB4_6:
	ucomiss	.LCPI4_0(%rip), %xmm0
	ja	.LBB4_8
# BB#7:
	movl	(%rsi), %edx
	movl	%eax, %edi
	shlq	$4, %rdi
	movl	%edx, (%rcx,%rdi)
	movl	4(%rsi), %edx
	movl	%edx, 4(%rcx,%rdi)
	movl	8(%rsi), %edx
	movl	%edx, 8(%rcx,%rdi)
	incl	%eax
.LBB4_8:                                # %_Z26PLANE_CLIP_POLYGON_COLLECTI9btVector3EvRKT_S3_ffPS1_Rj.exit37
	retq
.Lfunc_end4:
	.size	_Z26PLANE_CLIP_POLYGON_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_PKT_jPS6_T1_, .Lfunc_end4-_Z26PLANE_CLIP_POLYGON_GENERICI9btVector39btVector422DISTANCE_PLANE_3D_FUNCEjRKT0_PKT_jPS6_T1_
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
