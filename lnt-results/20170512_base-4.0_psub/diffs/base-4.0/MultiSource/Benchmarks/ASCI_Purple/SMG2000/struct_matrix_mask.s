	.text
	.file	"struct_matrix_mask.bc"
	.globl	hypre_StructMatrixCreateMask
	.p2align	4, 0x90
	.type	hypre_StructMatrixCreateMask,@function
hypre_StructMatrixCreateMask:           # @hypre_StructMatrixCreateMask
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %r12d
	movq	%rdi, %r13
	movq	24(%r13), %rax
	movq	(%rax), %rbp
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	8(%rax), %r14d
	movl	$1, %edi
	movl	$136, %esi
	callq	hypre_CAlloc
	movq	%rax, %r15
	movl	(%r13), %eax
	movl	%eax, (%r15)
	movq	8(%r13), %rdi
	leaq	8(%r15), %rsi
	callq	hypre_StructGridRef
	movq	16(%r13), %rdi
	callq	hypre_StructStencilRef
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	%rax, 16(%r15)
	movl	$12, %esi
	movl	%r12d, %edi
	callq	hypre_CAlloc
	testl	%r12d, %r12d
	jle	.LBB0_3
# BB#1:                                 # %.lr.ph117.preheader
	movl	%r12d, %ecx
	movq	%rax, %rdx
	addq	$8, %rdx
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph117
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rsi), %rdi
	leaq	(%rdi,%rdi,2), %rdi
	movl	(%rbp,%rdi,4), %edi
	movl	%edi, -8(%rdx)
	movslq	(%rsi), %rdi
	leaq	(%rdi,%rdi,2), %rdi
	movl	4(%rbp,%rdi,4), %edi
	movl	%edi, -4(%rdx)
	movslq	(%rsi), %rdi
	leaq	(%rdi,%rdi,2), %rdi
	movl	8(%rbp,%rdi,4), %edi
	movl	%edi, (%rdx)
	addq	$4, %rsi
	addq	$12, %rdx
	decq	%rcx
	jne	.LBB0_2
.LBB0_3:                                # %._crit_edge118
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	16(%rcx), %edi
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movl	%r12d, %esi
	movq	%rax, %rdx
	callq	hypre_StructStencilCreate
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rax, 24(%rbp)
	movl	32(%r13), %eax
	movl	%eax, 32(%rbp)
	movq	40(%r13), %rdi
	callq	hypre_BoxArrayDuplicate
	movq	%rax, 40(%rbp)
	movq	48(%r13), %rax
	movq	%rax, 48(%rbp)
	movl	$0, 56(%rbp)
	movl	60(%r13), %eax
	movl	%eax, 60(%rbp)
	movq	40(%r13), %rbp
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	64(%r13), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	8(%rbp), %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rbp, %rcx
	movq	%rax, %rbp
	cmpl	$0, 8(%rcx)
	jle	.LBB0_16
# BB#4:                                 # %.lr.ph113
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	(,%rax,4), %edx
	testl	%eax, %eax
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movq	%rbp, %r13
	jle	.LBB0_5
# BB#7:                                 # %.lr.ph113.split.us.preheader
	movl	%eax, %r12d
	leaq	-1(%r12), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%r12, 48(%rsp)          # 8-byte Spill
                                        # kill: %R12D<def> %R12D<kill> %R12<kill> %R12<def>
	andl	$3, %r12d
	leaq	12(%rbx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph113.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_11 Depth 2
                                        #     Child Loop BB0_14 Depth 2
	movl	%edx, %edi
	callq	hypre_MAlloc
	movq	%r13, %rbp
	testq	%r12, %r12
	movq	%rax, (%rbp,%r15,8)
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r15,8), %rcx
	je	.LBB0_9
# BB#10:                                # %.prol.preheader
                                        #   in Loop: Header=BB0_8 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_11:                               #   Parent Loop BB0_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rbx,%rsi,4), %rdx
	movl	(%rcx,%rdx,4), %edx
	movl	%edx, (%rax,%rsi,4)
	incq	%rsi
	cmpq	%rsi, %r12
	jne	.LBB0_11
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_9:                                #   in Loop: Header=BB0_8 Depth=1
	xorl	%esi, %esi
.LBB0_12:                               # %.prol.loopexit
                                        #   in Loop: Header=BB0_8 Depth=1
	cmpq	$3, 56(%rsp)            # 8-byte Folded Reload
	jb	.LBB0_15
# BB#13:                                # %.lr.ph113.split.us.new
                                        #   in Loop: Header=BB0_8 Depth=1
	movq	48(%rsp), %rdx          # 8-byte Reload
	subq	%rsi, %rdx
	leaq	12(%rax,%rsi,4), %rax
	movq	40(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB0_14:                               #   Parent Loop BB0_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	-12(%rsi), %rdi
	movl	(%rcx,%rdi,4), %edi
	movl	%edi, -12(%rax)
	movslq	-8(%rsi), %rdi
	movl	(%rcx,%rdi,4), %edi
	movl	%edi, -8(%rax)
	movslq	-4(%rsi), %rdi
	movl	(%rcx,%rdi,4), %edi
	movl	%edi, -4(%rax)
	movslq	(%rsi), %rdi
	movl	(%rcx,%rdi,4), %edi
	movl	%edi, (%rax)
	addq	$16, %rax
	addq	$16, %rsi
	addq	$-4, %rdx
	jne	.LBB0_14
.LBB0_15:                               # %._crit_edge.us
                                        #   in Loop: Header=BB0_8 Depth=1
	incq	%r15
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	8(%rax), %rax
	cmpq	%rax, %r15
	movl	4(%rsp), %edx           # 4-byte Reload
	jl	.LBB0_8
	jmp	.LBB0_16
.LBB0_5:                                # %.lr.ph113.split.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph113.split
                                        # =>This Inner Loop Header: Depth=1
	movl	4(%rsp), %edi           # 4-byte Reload
	callq	hypre_MAlloc
	movq	%r13, %rbp
	movq	%rax, (%rbp,%rbx,8)
	incq	%rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	8(%rax), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_6
.LBB0_16:                               # %._crit_edge114
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbp, 64(%rbx)
	movq	32(%rsp), %r15          # 8-byte Reload
	movl	72(%r15), %eax
	movl	%eax, 72(%rbx)
	leal	(,%r14,4), %edi
	callq	hypre_MAlloc
	movq	%rax, 80(%rbx)
	testl	%r14d, %r14d
	movq	24(%rsp), %r8           # 8-byte Reload
	jle	.LBB0_31
# BB#17:                                # %.lr.ph
	movq	80(%r15), %rcx
	cmpl	$7, %r14d
	jbe	.LBB0_18
# BB#25:                                # %min.iters.checked
	movl	%r14d, %esi
	andl	$7, %esi
	movq	%r14, %rdx
	subq	%rsi, %rdx
	je	.LBB0_18
# BB#26:                                # %vector.memcheck
	leaq	(%rcx,%r14,4), %rdi
	cmpq	%rdi, %rax
	jae	.LBB0_28
# BB#27:                                # %vector.memcheck
	leaq	(%rax,%r14,4), %rdi
	cmpq	%rdi, %rcx
	jae	.LBB0_28
.LBB0_18:
	xorl	%edx, %edx
.LBB0_19:                               # %scalar.ph.preheader
	movl	%r14d, %edi
	subl	%edx, %edi
	leaq	-1(%r14), %rsi
	subq	%rdx, %rsi
	andq	$7, %rdi
	je	.LBB0_22
# BB#20:                                # %scalar.ph.prol.preheader
	negq	%rdi
	.p2align	4, 0x90
.LBB0_21:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rdx,4), %ebp
	movl	%ebp, (%rax,%rdx,4)
	incq	%rdx
	incq	%rdi
	jne	.LBB0_21
.LBB0_22:                               # %scalar.ph.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB0_31
# BB#23:                                # %scalar.ph.preheader.new
	subq	%rdx, %r14
	leaq	28(%rax,%rdx,4), %rax
	leaq	28(%rcx,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB0_24:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %edx
	movl	%edx, -28(%rax)
	movl	-24(%rcx), %edx
	movl	%edx, -24(%rax)
	movl	-20(%rcx), %edx
	movl	%edx, -20(%rax)
	movl	-16(%rcx), %edx
	movl	%edx, -16(%rax)
	movl	-12(%rcx), %edx
	movl	%edx, -12(%rax)
	movl	-8(%rcx), %edx
	movl	%edx, -8(%rax)
	movl	-4(%rcx), %edx
	movl	%edx, -4(%rax)
	movl	(%rcx), %edx
	movl	%edx, (%rax)
	addq	$32, %rax
	addq	$32, %rcx
	addq	$-8, %r14
	jne	.LBB0_24
.LBB0_31:                               # %.preheader
	movl	88(%r15), %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%ecx, 88(%rax)
	movl	92(%r15), %ecx
	movl	%ecx, 92(%rax)
	movl	96(%r15), %ecx
	movl	%ecx, 96(%rax)
	movl	100(%r15), %ecx
	movl	%ecx, 100(%rax)
	movl	104(%r15), %ecx
	movl	%ecx, 104(%rax)
	movl	108(%r15), %ecx
	movl	%ecx, 108(%rax)
	movq	8(%rax), %rcx
	imull	52(%rcx), %r8d
	movl	%r8d, 112(%rax)
	movq	$0, 120(%rax)
	movl	$1, 128(%rax)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_28:                               # %vector.body.preheader
	leaq	16(%rcx), %rdi
	leaq	16(%rax), %rbp
	movq	%rdx, %rbx
	.p2align	4, 0x90
.LBB0_29:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$-8, %rbx
	jne	.LBB0_29
# BB#30:                                # %middle.block
	testl	%esi, %esi
	jne	.LBB0_19
	jmp	.LBB0_31
.Lfunc_end0:
	.size	hypre_StructMatrixCreateMask, .Lfunc_end0-hypre_StructMatrixCreateMask
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
