	.text
	.file	"order.bc"
	.globl	ord_Compare
	.p2align	4, 0x90
	.type	ord_Compare,@function
ord_Compare:                            # @ord_Compare
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rcx, %rbx
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r12
	movl	(%r12), %edi
	movq	%rbx, %rdx
	callq	ord_CheckDomPred
	testl	%eax, %eax
	je	.LBB0_1
# BB#6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB0_1:
	movq	%rbx, ord_PRECEDENCE(%rip)
	movl	208(%r15), %eax
	cmpl	$1, %eax
	je	.LBB0_4
# BB#2:
	testl	%eax, %eax
	jne	.LBB0_5
# BB#3:
	movq	%r12, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	kbo_Compare             # TAILCALL
.LBB0_4:
	movq	%r12, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	rpos_Compare            # TAILCALL
.LBB0_5:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$116, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end0:
	.size	ord_Compare, .Lfunc_end0-ord_Compare
	.cfi_endproc

	.p2align	4, 0x90
	.type	ord_CheckDomPred,@function
ord_CheckDomPred:                       # @ord_CheckDomPred
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	testl	%edi, %edi
	jns	.LBB1_3
# BB#1:                                 # %term_IsAtom.exit
	movl	%edi, %r10d
	negl	%r10d
	movl	symbol_TYPEMASK(%rip), %r8d
	movl	%r8d, %eax
	andl	%r10d, %eax
	cmpl	$2, %eax
	jne	.LBB1_3
# BB#2:
	movl	symbol_TYPESTATBITS(%rip), %ecx
	movl	%r10d, %eax
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %r9
	cltq
	movq	(%r9,%rax,8), %rax
	testb	$64, 20(%rax)
	jne	.LBB1_8
.LBB1_3:                                # %term_IsAtom.exit.thread
	movl	(%rsi), %eax
	testl	%eax, %eax
	jns	.LBB1_17
# BB#4:                                 # %term_IsAtom.exit28
	negl	%eax
	movl	symbol_TYPEMASK(%rip), %r8d
	movl	%r8d, %ecx
	andl	%eax, %ecx
	cmpl	$2, %ecx
	jne	.LBB1_17
# BB#5:
	movl	symbol_TYPESTATBITS(%rip), %ecx
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %r9
	cltq
	movq	(%r9,%rax,8), %rax
	testb	$64, 20(%rax)
	jne	.LBB1_6
.LBB1_17:                               # %term_IsAtom.exit28.thread
	xorl	%eax, %eax
.LBB1_18:                               # %term_IsAtom.exit27.thread
	popq	%rbx
	retq
.LBB1_6:
	movl	$1, %eax
	testl	%edi, %edi
	jns	.LBB1_18
# BB#7:                                 # %.term_IsAtom.exit27_crit_edge
	movl	%edi, %r10d
	negl	%r10d
.LBB1_8:                                # %term_IsAtom.exit27
	movl	%r8d, %eax
	andl	%r10d, %eax
	cmpl	$2, %eax
	jne	.LBB1_9
# BB#10:
	movl	(%rsi), %ebx
	movl	$3, %eax
	testl	%ebx, %ebx
	jns	.LBB1_18
# BB#11:                                # %term_IsAtom.exit26
	movl	%ebx, %r11d
	negl	%r11d
	andl	%r11d, %r8d
	cmpl	$2, %r8d
	jne	.LBB1_18
# BB#12:
	sarl	%cl, %r10d
	movslq	%r10d, %r8
	movq	(%r9,%r8,8), %rsi
	testb	$64, 20(%rsi)
	jne	.LBB1_14
# BB#13:
	movl	$1, %eax
	popq	%rbx
	retq
.LBB1_9:
	movl	$1, %eax
	popq	%rbx
	retq
.LBB1_14:
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r11d
	movslq	%r11d, %rcx
	movq	(%r9,%rcx,8), %rsi
	testb	$64, 20(%rsi)
	je	.LBB1_18
# BB#15:
	movl	(%rdx,%r8,4), %esi
	cmpl	(%rdx,%rcx,4), %esi
	jl	.LBB1_18
# BB#16:
	cmpl	%edi, %ebx
	movl	$1, %eax
	je	.LBB1_17
	jmp	.LBB1_18
.Lfunc_end1:
	.size	ord_CheckDomPred, .Lfunc_end1-ord_CheckDomPred
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_DumpCore,@function
misc_DumpCore:                          # @misc_DumpCore
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rcx
	movl	$.L.str.13, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	callq	abort
.Lfunc_end2:
	.size	misc_DumpCore, .Lfunc_end2-misc_DumpCore
	.cfi_endproc

	.globl	ord_CompareEqual
	.p2align	4, 0x90
	.type	ord_CompareEqual,@function
ord_CompareEqual:                       # @ord_CompareEqual
	.cfi_startproc
# BB#0:
	movl	208(%rdx), %eax
	cmpl	$1, %eax
	je	.LBB3_3
# BB#1:
	testl	%eax, %eax
	jne	.LBB3_4
# BB#2:
	jmp	term_Equal              # TAILCALL
.LBB3_3:
	jmp	rpos_Equal              # TAILCALL
.LBB3_4:
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 16
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$137, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end3:
	.size	ord_CompareEqual, .Lfunc_end3-ord_CompareEqual
	.cfi_endproc

	.globl	ord_ContCompare
	.p2align	4, 0x90
	.type	ord_ContCompare,@function
ord_ContCompare:                        # @ord_ContCompare
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r9, %rbp
	movq	%r8, %r13
	movq	%rcx, %r15
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movl	(%rbx), %edi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	ord_CheckDomPred
	testl	%eax, %eax
	je	.LBB4_1
# BB#6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_1:
	movq	%rbp, ord_PRECEDENCE(%rip)
	movl	208(%r13), %eax
	cmpl	$1, %eax
	je	.LBB4_4
# BB#2:
	testl	%eax, %eax
	jne	.LBB4_5
# BB#3:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	kbo_ContCompare         # TAILCALL
.LBB4_4:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	rpos_ContCompare        # TAILCALL
.LBB4_5:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$167, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end4:
	.size	ord_ContCompare, .Lfunc_end4-ord_ContCompare
	.cfi_endproc

	.globl	ord_ContGreater
	.p2align	4, 0x90
	.type	ord_ContGreater,@function
ord_ContGreater:                        # @ord_ContGreater
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%r9, %rbp
	movq	%r8, %r13
	movq	%rcx, %r15
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movl	(%rbx), %edi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	ord_CheckDomPred
	movl	%eax, %ecx
	testl	%ecx, %ecx
	jne	.LBB5_5
# BB#1:
	movq	%rbp, ord_PRECEDENCE(%rip)
	movl	208(%r13), %eax
	cmpl	$1, %eax
	je	.LBB5_4
# BB#2:
	testl	%eax, %eax
	jne	.LBB5_3
# BB#6:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	kbo_ContGreater         # TAILCALL
.LBB5_4:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	rpos_ContGreaterEqual
	movl	%eax, %ecx
.LBB5_5:
	xorl	%eax, %eax
	cmpl	$3, %ecx
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_3:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$200, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end5:
	.size	ord_ContGreater, .Lfunc_end5-ord_ContGreater
	.cfi_endproc

	.globl	ord_Not
	.p2align	4, 0x90
	.type	ord_Not,@function
ord_Not:                                # @ord_Not
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	orl	$2, %eax
	xorl	%ecx, %ecx
	cmpl	$3, %edi
	setne	%cl
	cmpl	$2, %eax
	leal	1(%rcx,%rcx), %eax
	cmovel	%edi, %eax
	retq
.Lfunc_end6:
	.size	ord_Not, .Lfunc_end6-ord_Not
	.cfi_endproc

	.globl	ord_Print
	.p2align	4, 0x90
	.type	ord_Print,@function
ord_Print:                              # @ord_Print
	.cfi_startproc
# BB#0:
	cmpl	$3, %edi
	ja	.LBB7_7
# BB#1:
	movl	%edi, %eax
	jmpq	*.LJTI7_0(,%rax,8)
.LBB7_2:
	movq	stdout(%rip), %rcx
	movl	$.L.str.8, %edi
	jmp	.LBB7_3
.LBB7_7:
	movq	stdout(%rip), %rcx
	movl	$.L.str.12, %edi
	movl	$11, %esi
	movl	$1, %edx
	jmp	fwrite                  # TAILCALL
.LBB7_6:
	movq	stdout(%rip), %rcx
	movl	$.L.str.11, %edi
	jmp	.LBB7_3
.LBB7_4:
	movq	stdout(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$7, %esi
	movl	$1, %edx
	jmp	fwrite                  # TAILCALL
.LBB7_5:
	movq	stdout(%rip), %rcx
	movl	$.L.str.10, %edi
.LBB7_3:
	movl	$14, %esi
	movl	$1, %edx
	jmp	fwrite                  # TAILCALL
.Lfunc_end7:
	.size	ord_Print, .Lfunc_end7-ord_Print
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_2
	.quad	.LBB7_6
	.quad	.LBB7_4
	.quad	.LBB7_5

	.text
	.globl	ord_LiteralCompare
	.p2align	4, 0x90
	.type	ord_LiteralCompare,@function
ord_LiteralCompare:                     # @ord_LiteralCompare
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 112
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movl	%r8d, %r13d
	movl	%ecx, %r15d
	movq	%rdx, %r12
	movl	%esi, %ebx
	movq	%rdi, %r14
	movq	112(%rsp), %rcx
	movl	fol_NOT(%rip), %edx
	movl	$1, %edi
	cmpl	%edx, (%r14)
	jne	.LBB8_2
# BB#1:
	movq	16(%r14), %rax
	movq	8(%rax), %r14
	xorl	%ebp, %ebp
	jmp	.LBB8_3
.LBB8_2:
	movl	$1, %ebp
.LBB8_3:
	movl	(%r12), %eax
	cmpl	%edx, %eax
	jne	.LBB8_5
# BB#4:
	movq	16(%r12), %rax
	movq	8(%rax), %r12
	movl	(%r12), %eax
	xorl	%edi, %edi
.LBB8_5:
	movl	fol_EQUALITY(%rip), %esi
	cmpl	(%r14), %esi
	jne	.LBB8_13
# BB#6:
	movq	16(%r14), %rdx
	movq	8(%rdx), %r10
	cmpl	%eax, %esi
	jne	.LBB8_18
# BB#7:
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	movl	%edi, (%rsp)            # 4-byte Spill
	movq	(%rdx), %rax
	movq	8(%rax), %rsi
	movq	16(%r12), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %r8
	movq	8(%rdx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	testl	%ebx, %ebx
	movl	$0, %eax
	je	.LBB8_27
.LBB8_8:
	cmpl	$1, %eax
	movq	%r10, %rbx
	cmoveq	%rsi, %rbx
	cmoveq	%r10, %rsi
	movq	%rsi, %r12
	testl	%r15d, %r15d
	je	.LBB8_37
.LBB8_9:
	cmpl	$1, %r14d
	movq	%r8, %rsi
	movq	16(%rsp), %rbp          # 8-byte Reload
	cmoveq	%rbp, %rsi
	cmoveq	%r8, %rbp
	movq	%rbx, %rdi
	movq	%r9, %rbx
	movq	%r9, %rdx
	movq	%rcx, %r14
	callq	ord_Compare
	movl	%eax, %r13d
	cmpl	$2, %r13d
	movl	4(%rsp), %eax           # 4-byte Reload
	jne	.LBB8_129
# BB#10:
	testl	%eax, %eax
	je	.LBB8_48
# BB#11:
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jne	.LBB8_49
# BB#12:
	movl	$1, %r13d
	jmp	.LBB8_129
.LBB8_13:
	cmpl	%eax, %esi
	jne	.LBB8_22
# BB#14:
	movq	16(%r12), %rax
	movq	8(%rax), %rsi
	movq	%r14, %rdi
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%r9, %rdx
	movq	%rcx, %rbp
	callq	ord_Compare
	movl	%eax, %ebx
	testl	%r15d, %r15d
	jne	.LBB8_46
# BB#15:
	cmpl	$1, %ebx
	movl	%ebx, %r13d
	je	.LBB8_129
# BB#16:
	movq	16(%r12), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movq	%r14, %rdi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rbp, %rcx
	callq	ord_Compare
	movl	$1, %r13d
	cmpl	$1, %eax
	je	.LBB8_129
# BB#17:
	xorl	%r13d, %r13d
	cmpl	%eax, %ebx
	jmp	.LBB8_26
.LBB8_18:
	movq	%r10, %rdi
	movq	%r12, %rsi
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%r9, %rdx
	movq	%rcx, %r15
	callq	ord_Compare
	movl	%eax, %ebp
	testl	%ebx, %ebx
	jne	.LBB8_47
# BB#19:
	cmpl	$3, %ebp
	movl	%ebp, %r13d
	je	.LBB8_129
# BB#20:
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movq	%r12, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r15, %rcx
	callq	ord_Compare
	movl	$3, %r13d
	cmpl	$3, %eax
	je	.LBB8_129
# BB#21:
	xorl	%r13d, %r13d
	cmpl	%eax, %ebp
	cmovel	%ebp, %r13d
	jmp	.LBB8_129
.LBB8_22:
	movl	%edi, %ebx
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%r9, %rdx
	callq	ord_Compare
	movl	%ebp, %ecx
	movl	%eax, %r13d
	cmpl	$2, %r13d
	jne	.LBB8_129
# BB#23:
	testl	%ecx, %ecx
	je	.LBB8_25
# BB#24:
	movl	$1, %r13d
	testl	%ebx, %ebx
	je	.LBB8_129
.LBB8_25:
	orl	$2, %ebx
	testl	%ecx, %ecx
	movl	$2, %r13d
.LBB8_26:                               # %.thread
	cmovel	%ebx, %r13d
	jmp	.LBB8_129
.LBB8_27:
	testl	%r13d, %r13d
	je	.LBB8_29
# BB#28:
	movq	%r10, %rdi
	movq	%rsi, %rbx
	movq	%r9, %rdx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%r9, %rbp
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movq	%r10, 48(%rsp)          # 8-byte Spill
	callq	ord_Compare
	movq	48(%rsp), %r10          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	%rbx, %rsi
	movq	%rbp, %r9
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%eax, %edx
	orl	$2, %edx
	cmpl	$3, %edx
	je	.LBB8_8
.LBB8_29:
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	xorl	%edx, %edx
	testl	%r15d, %r15d
	je	.LBB8_50
.LBB8_30:
	movq	%r9, %r14
	movq	%rcx, %r15
	testl	%edx, %edx
	movq	%r8, %rbp
	movq	16(%rsp), %rbx          # 8-byte Reload
	cmovneq	%rbx, %rbp
	cmovneq	%r8, %rbx
	movq	%r10, %rdi
	movq	%rbp, %rsi
	movq	%r10, %r12
	callq	term_Equal
	testl	%eax, %eax
	je	.LBB8_53
# BB#31:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	ord_Compare
	cmpl	$2, %eax
	jne	.LBB8_73
# BB#32:
	movl	4(%rsp), %ecx           # 4-byte Reload
	testl	%ecx, %ecx
	je	.LBB8_34
# BB#33:
	movl	$1, %r13d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB8_129
.LBB8_34:
	testl	%ecx, %ecx
	jne	.LBB8_99
# BB#35:
	movl	$3, %r13d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jne	.LBB8_129
# BB#36:
	movl	%eax, %r13d
	jmp	.LBB8_129
.LBB8_37:
	testl	%r13d, %r13d
	je	.LBB8_39
# BB#38:
	movq	%r8, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r9, %rdx
	movq	%rcx, %r14
	movq	%r9, %rbp
	movq	%r8, %r15
	callq	ord_Compare
	movq	%r15, %r8
	movq	%rbp, %r9
	movq	%r14, %rcx
	movl	%eax, %r14d
	orl	$2, %eax
	cmpl	$3, %eax
	je	.LBB8_9
.LBB8_39:
	movq	%r9, %r14
	movq	%rcx, %r15
	movq	%rbx, %rdi
	movq	%r8, %rsi
	movq	%r8, %rbp
	callq	term_Equal
	testl	%eax, %eax
	je	.LBB8_60
# BB#40:
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	ord_Compare
	cmpl	$2, %eax
	jne	.LBB8_73
# BB#41:
	movl	4(%rsp), %ecx           # 4-byte Reload
	testl	%ecx, %ecx
	je	.LBB8_43
# BB#42:
	movl	$1, %r13d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB8_129
.LBB8_43:
	testl	%ecx, %ecx
	jne	.LBB8_99
# BB#44:
	movl	$3, %r13d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jne	.LBB8_129
# BB#45:
	movl	%eax, %r13d
	jmp	.LBB8_129
.LBB8_46:
	movl	%ebx, %r13d
	jmp	.LBB8_129
.LBB8_47:
	movl	%ebp, %r13d
	jmp	.LBB8_129
.LBB8_48:
	movl	$3, %r13d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jne	.LBB8_129
.LBB8_49:
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	movq	%r14, %rcx
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	ord_Compare             # TAILCALL
.LBB8_50:
	testl	%r13d, %r13d
	je	.LBB8_67
# BB#51:
	movq	16(%r12), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rdi
	movq	8(%rdx), %rsi
	movq	%r9, %rdx
	movq	%rcx, %r15
	movq	%r9, %rbp
	movq	%r8, %r14
	movq	%r10, %rbx
	callq	ord_Compare
	movq	%rbx, %r10
	movq	%r14, %r8
	movq	%rbp, %r9
	movq	%r15, %rcx
	xorl	%edx, %edx
	cmpl	$3, %eax
	sete	%al
	jne	.LBB8_67
# BB#52:
	movb	%al, %dl
	jmp	.LBB8_30
.LBB8_53:
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	term_Equal
	testl	%eax, %eax
	je	.LBB8_74
# BB#54:
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	ord_Compare
	cmpl	$2, %eax
	jne	.LBB8_88
# BB#55:
	movl	4(%rsp), %ecx           # 4-byte Reload
	testl	%ecx, %ecx
	je	.LBB8_57
# BB#56:
	movl	$1, %r13d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB8_129
.LBB8_57:
	testl	%ecx, %ecx
	jne	.LBB8_107
# BB#58:
	movl	$3, %r13d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jne	.LBB8_129
# BB#59:
	movl	%eax, %r13d
	jmp	.LBB8_129
.LBB8_60:
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rsi
	callq	term_Equal
	testl	%eax, %eax
	je	.LBB8_76
# BB#61:
	movq	%r12, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	ord_Compare
	cmpl	$2, %eax
	jne	.LBB8_88
# BB#62:
	movl	4(%rsp), %ecx           # 4-byte Reload
	testl	%ecx, %ecx
	je	.LBB8_64
# BB#63:
	movl	$1, %r13d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB8_129
.LBB8_64:
	testl	%ecx, %ecx
	jne	.LBB8_107
# BB#65:
	movl	$3, %r13d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jne	.LBB8_129
# BB#66:
	movl	%eax, %r13d
	jmp	.LBB8_129
.LBB8_67:
	movq	%r9, %r15
	movq	%rcx, %r12
	movq	%r10, %rdi
	movq	%r8, %rsi
	movq	%r8, %r13
	movq	%r10, %r14
	callq	term_Equal
	testl	%eax, %eax
	je	.LBB8_81
# BB#68:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	ord_Compare
	cmpl	$2, %eax
	jne	.LBB8_88
# BB#69:
	movl	4(%rsp), %ecx           # 4-byte Reload
	testl	%ecx, %ecx
	je	.LBB8_71
# BB#70:
	movl	$1, %r13d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB8_129
.LBB8_71:
	testl	%ecx, %ecx
	jne	.LBB8_107
# BB#72:
	movl	$3, %r13d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jne	.LBB8_129
.LBB8_73:
	movl	%eax, %r13d
	jmp	.LBB8_129
.LBB8_74:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	movq	%r14, %rbx
	movq	%rbx, %rdx
	movq	%r15, %r13
	movq	%r13, %rcx
	callq	ord_Compare
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%rbx, %rdx
	movl	%eax, %ebx
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%r13, %rcx
	callq	ord_Compare
	movl	%eax, %ebp
	cmpl	%ebp, %ebx
	jne	.LBB8_89
# BB#75:
	movl	%ebx, %r13d
	jmp	.LBB8_129
.LBB8_76:
	movq	%rbx, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%r15, %r13
	movq	%r13, %rcx
	callq	ord_Compare
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%r13, %rcx
	callq	ord_Compare
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, %r13d
	cmpl	%eax, %ecx
	je	.LBB8_129
# BB#77:
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	%eax, %ebp
	movq	40(%rsp), %rcx          # 8-byte Reload
	callq	ord_Compare
	movl	%ebp, %ecx
	movl	%r13d, %ebx
	cmpl	%eax, %ebx
	movl	%ebx, %ebp
	je	.LBB8_129
# BB#78:
	cmpl	$1, %ecx
	jne	.LBB8_80
# BB#79:
	movl	$1, %r13d
	cmpl	%eax, %ecx
	je	.LBB8_129
.LBB8_80:
	movq	%r12, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	ord_Compare
	cmpl	%eax, %ebp
	sete	%al
	cmpl	$1, %ebp
	sete	%cl
	andb	%al, %cl
	movzbl	%cl, %r13d
	jmp	.LBB8_129
.LBB8_81:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r13, %rsi
	callq	term_Equal
	testl	%eax, %eax
	movq	16(%rsp), %rbx          # 8-byte Reload
	je	.LBB8_93
# BB#82:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	ord_Compare
	cmpl	$2, %eax
	jne	.LBB8_99
# BB#83:
	movl	4(%rsp), %ecx           # 4-byte Reload
	testl	%ecx, %ecx
	je	.LBB8_85
# BB#84:
	movl	$1, %r13d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB8_129
.LBB8_85:
	testl	%ecx, %ecx
	jne	.LBB8_114
# BB#86:
	movl	$3, %r13d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jne	.LBB8_129
.LBB8_88:
	movl	%eax, %r13d
	jmp	.LBB8_129
.LBB8_89:
	movq	%r12, %rdi
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	callq	ord_Compare
	cmpl	%eax, %ebx
	movl	%ebx, %r13d
	je	.LBB8_129
# BB#90:
	cmpl	$3, %ebp
	jne	.LBB8_92
# BB#91:
	movl	$3, %r13d
	cmpl	%eax, %ebp
	je	.LBB8_129
.LBB8_92:
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	ord_Compare
	xorl	%ecx, %ecx
	cmpl	%eax, %ebx
	sete	%cl
	xorl	%eax, %eax
	cmpl	$3, %ebx
	leal	(%rcx,%rcx,2), %r13d
	cmovnel	%eax, %r13d
	jmp	.LBB8_129
.LBB8_93:
	movq	%r14, %rbp
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	term_Equal
	testl	%eax, %eax
	je	.LBB8_100
# BB#94:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	ord_Compare
	cmpl	$2, %eax
	jne	.LBB8_107
# BB#95:
	movl	4(%rsp), %ecx           # 4-byte Reload
	testl	%ecx, %ecx
	je	.LBB8_97
# BB#96:
	movl	$1, %r13d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB8_129
.LBB8_97:
	testl	%ecx, %ecx
	jne	.LBB8_121
# BB#98:
	movl	$3, %r13d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jne	.LBB8_129
.LBB8_99:
	movl	%eax, %r13d
	jmp	.LBB8_129
.LBB8_100:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbx, %rsi
	callq	term_Equal
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	ord_Compare
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	je	.LBB8_108
# BB#101:
	cmpl	$2, %eax
	jne	.LBB8_114
# BB#102:
	movl	4(%rsp), %ecx           # 4-byte Reload
	testl	%ecx, %ecx
	je	.LBB8_104
# BB#103:
	movl	$1, %r13d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB8_129
.LBB8_104:
	testl	%ecx, %ecx
	jne	.LBB8_125
# BB#105:
	movl	$3, %r13d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jne	.LBB8_129
.LBB8_107:
	movl	%eax, %r13d
.LBB8_129:                              # %.thread
	movl	%r13d, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_114:
	movl	%eax, %r13d
	jmp	.LBB8_129
.LBB8_108:
	testl	%eax, %eax
	je	.LBB8_115
.LBB8_109:
	cmpl	$1, %eax
	je	.LBB8_122
# BB#110:
	cmpl	$3, %eax
	movl	%eax, %r13d
	jne	.LBB8_129
# BB#111:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	ord_Compare
	movl	%eax, %r14d
	movl	$3, %r13d
	cmpl	$3, %r14d
	je	.LBB8_129
# BB#112:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	ord_Compare
	cmpl	$3, %eax
	je	.LBB8_129
# BB#113:
	testl	%r14d, %r14d
	setne	%cl
	testl	%eax, %eax
	setne	%al
	andb	%cl, %al
	movzbl	%al, %r13d
	jmp	.LBB8_129
.LBB8_115:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	ord_Compare
	cmpl	$3, %eax
	je	.LBB8_126
# BB#116:
	cmpl	$1, %eax
	je	.LBB8_127
# BB#117:
	testl	%eax, %eax
	jne	.LBB8_109
# BB#118:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	ord_Compare
	cmpl	$3, %eax
	jne	.LBB8_120
# BB#119:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	ord_Compare
	movl	$3, %r13d
	cmpl	$3, %eax
	je	.LBB8_129
.LBB8_120:
	xorl	%r13d, %r13d
	jmp	.LBB8_129
.LBB8_121:
	movl	%eax, %r13d
	jmp	.LBB8_129
.LBB8_122:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	ord_Compare
	movl	%eax, %r14d
	movl	$1, %r13d
	cmpl	$1, %r14d
	je	.LBB8_129
# BB#123:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	ord_Compare
	cmpl	$1, %eax
	je	.LBB8_129
# BB#124:
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	testl	%r14d, %r14d
	leal	(%rcx,%rcx,2), %r13d
	cmovel	%r14d, %r13d
	jmp	.LBB8_129
.LBB8_125:
	movl	%eax, %r13d
	jmp	.LBB8_129
.LBB8_126:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	ord_Compare
	xorl	%ecx, %ecx
	cmpl	$3, %eax
	sete	%cl
	leal	(%rcx,%rcx,2), %r13d
	jmp	.LBB8_129
.LBB8_127:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	ord_Compare
	movl	$1, %r13d
	cmpl	$1, %eax
	je	.LBB8_129
# BB#128:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	ord_Compare
	xorl	%r13d, %r13d
	cmpl	$1, %eax
	sete	%r13b
	jmp	.LBB8_129
.Lfunc_end8:
	.size	ord_LiteralCompare, .Lfunc_end8-ord_LiteralCompare
	.cfi_endproc

	.type	ord_PRECEDENCE,@object  # @ord_PRECEDENCE
	.comm	ord_PRECEDENCE,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n\tError in file %s at line %d\n"
	.size	.L.str, 31

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/SPASS/order.c"
	.size	.L.str.1, 77

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n In ord_Compare:"
	.size	.L.str.2, 18

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n Illegal ordering type."
	.size	.L.str.3, 25

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n Please report this error via email to spass@mpi-sb.mpg.de including\n the SPASS version, input problem, options, operating system.\n"
	.size	.L.str.4, 133

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\n In ord_Compare: Illegal ordering type."
	.size	.L.str.5, 41

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\n In ord_ContCompare:"
	.size	.L.str.6, 22

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\n In ord_ContGreater:"
	.size	.L.str.7, 22

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	" uncomparable "
	.size	.L.str.8, 15

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	" equal "
	.size	.L.str.9, 8

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	" greater than "
	.size	.L.str.10, 15

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	" smaller than "
	.size	.L.str.11, 15

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	" Nonsense! "
	.size	.L.str.12, 12

	.type	ord_VARCOUNT,@object    # @ord_VARCOUNT
	.comm	ord_VARCOUNT,16000,16
	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"\n\n"
	.size	.L.str.13, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
