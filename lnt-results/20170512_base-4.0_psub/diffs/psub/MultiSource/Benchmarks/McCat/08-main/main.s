	.text
	.file	"main.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4618760256179416810     # double 6.2831853071800001
.LCPI0_1:
	.quad	-4609115380302729494    # double -3.1415926535900001
.LCPI0_2:
	.quad	4614256658803846128     # double 3.1415936535900002
	.text
	.globl	MakeSphere
	.p2align	4, 0x90
	.type	MakeSphere,@function
MakeSphere:                             # @MakeSphere
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$168, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 192
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	divsd	%xmm0, %xmm2
	movsd	%xmm2, 48(%rsp)         # 8-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 56(%rsp)         # 8-byte Spill
	movsd	.LCPI0_1(%rip), %xmm0   # xmm0 = mem[0],zero
	leaq	64(%rsp), %r14
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_2 Depth 2
	movsd	48(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	32(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB0_2:                                #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	movapd	%xmm1, %xmm0
	callq	cos
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 64(%rsp)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 72(%rsp)
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 80(%rsp)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	cos
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 88(%rsp)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 96(%rsp)
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 104(%rsp)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	56(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	cos
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 112(%rsp)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 120(%rsp)
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 128(%rsp)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	cos
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 136(%rsp)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 144(%rsp)
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 152(%rsp)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	InsertPoly4
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	.LCPI0_2(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	ja	.LBB0_2
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	movsd	40(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	ja	.LBB0_1
# BB#4:
	movq	%rbx, %rax
	addq	$168, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	MakeSphere, .Lfunc_end0-MakeSphere
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4621819117588971520     # double 10
.LCPI1_2:
	.quad	4607182418800017408     # double 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_1:
	.quad	4635822497680326656     # double 87
	.quad	4635822497680326656     # double 87
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 48
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movl	$.L.str, %edi
	callq	Oalloc
	movq	%rax, %rbx
	movl	$.L.str.1, %edi
	callq	Oalloc
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movl	$25, %esi
	movl	$25, %edx
	movq	%rax, %rdi
	callq	MakeSphere
	movq	%rax, %r14
	movapd	.LCPI1_1(%rip), %xmm0   # xmm0 = [8.700000e+01,8.700000e+01]
	movupd	%xmm0, 136(%r14)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	InsertChild
	xorpd	%xmm0, %xmm0
	movl	$360, %ebp              # imm = 0x168
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	%xmm0, 168(%rbx)
	movq	%rbx, %rdi
	callq	CalcObject
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI1_2(%rip), %xmm0
	decl	%ebp
	jne	.LBB1_1
# BB#2:
	movq	%r14, %rdi
	callq	PrintObject
	xorl	%edi, %edi
	callq	Draw_All
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc

	.type	pyramid,@object         # @pyramid
	.data
	.globl	pyramid
	.p2align	4
pyramid:
	.quad	4607182418800017408     # double 1
	.quad	-4619161993808822927    # double -0.67000000000000004
	.quad	4604210043045952881     # double 0.67000000000000004
	.quad	-4616189618054758400    # double -1
	.quad	-4619161993808822927    # double -0.67000000000000004
	.quad	4604210043045952881     # double 0.67000000000000004
	.quad	0                       # double 0
	.quad	-4619161993808822927    # double -0.67000000000000004
	.quad	-4616189618054758400    # double -1
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	-4619161993808822927    # double -0.67000000000000004
	.quad	4604210043045952881     # double 0.67000000000000004
	.quad	0                       # double 0
	.quad	-4619161993808822927    # double -0.67000000000000004
	.quad	-4616189618054758400    # double -1
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	-4619161993808822927    # double -0.67000000000000004
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	-4619161993808822927    # double -0.67000000000000004
	.quad	4604210043045952881     # double 0.67000000000000004
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.quad	-4619161993808822927    # double -0.67000000000000004
	.quad	4604210043045952881     # double 0.67000000000000004
	.quad	-4616189618054758400    # double -1
	.quad	-4619161993808822927    # double -0.67000000000000004
	.quad	4604210043045952881     # double 0.67000000000000004
	.size	pyramid, 288

	.type	SPyramid,@object        # @SPyramid
	.globl	SPyramid
	.p2align	4
SPyramid:
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.quad	-4616189618054758400    # double -1
	.size	SPyramid, 576

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"TestObject"
	.size	.L.str, 11

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"SphereObject"
	.size	.L.str.1, 13


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
