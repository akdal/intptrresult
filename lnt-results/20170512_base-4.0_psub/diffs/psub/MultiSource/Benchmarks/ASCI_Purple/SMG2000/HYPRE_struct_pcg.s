	.text
	.file	"HYPRE_struct_pcg.bc"
	.globl	HYPRE_StructPCGCreate
	.p2align	4, 0x90
	.type	HYPRE_StructPCGCreate,@function
HYPRE_StructPCGCreate:                  # @HYPRE_StructPCGCreate
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movl	$hypre_CAlloc, %edi
	movl	$hypre_StructKrylovFree, %esi
	movl	$hypre_StructKrylovCreateVector, %edx
	movl	$hypre_StructKrylovDestroyVector, %ecx
	movl	$hypre_StructKrylovMatvecCreate, %r8d
	movl	$hypre_StructKrylovMatvec, %r9d
	pushq	$hypre_StructKrylovIdentity
.Lcfi2:
	.cfi_adjust_cfa_offset 8
	pushq	$hypre_StructKrylovIdentitySetup
.Lcfi3:
	.cfi_adjust_cfa_offset 8
	pushq	$hypre_StructKrylovAxpy
.Lcfi4:
	.cfi_adjust_cfa_offset 8
	pushq	$hypre_StructKrylovScaleVector
.Lcfi5:
	.cfi_adjust_cfa_offset 8
	pushq	$hypre_StructKrylovClearVector
.Lcfi6:
	.cfi_adjust_cfa_offset 8
	pushq	$hypre_StructKrylovCopyVector
.Lcfi7:
	.cfi_adjust_cfa_offset 8
	pushq	$hypre_StructKrylovInnerProd
.Lcfi8:
	.cfi_adjust_cfa_offset 8
	pushq	$hypre_StructKrylovMatvecDestroy
.Lcfi9:
	.cfi_adjust_cfa_offset 8
	callq	hypre_PCGFunctionsCreate
	addq	$64, %rsp
.Lcfi10:
	.cfi_adjust_cfa_offset -64
	movq	%rax, %rdi
	callq	hypre_PCGCreate
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	HYPRE_StructPCGCreate, .Lfunc_end0-HYPRE_StructPCGCreate
	.cfi_endproc

	.globl	HYPRE_StructPCGDestroy
	.p2align	4, 0x90
	.type	HYPRE_StructPCGDestroy,@function
HYPRE_StructPCGDestroy:                 # @HYPRE_StructPCGDestroy
	.cfi_startproc
# BB#0:
	jmp	hypre_PCGDestroy        # TAILCALL
.Lfunc_end1:
	.size	HYPRE_StructPCGDestroy, .Lfunc_end1-HYPRE_StructPCGDestroy
	.cfi_endproc

	.globl	HYPRE_StructPCGSetup
	.p2align	4, 0x90
	.type	HYPRE_StructPCGSetup,@function
HYPRE_StructPCGSetup:                   # @HYPRE_StructPCGSetup
	.cfi_startproc
# BB#0:
	jmp	HYPRE_PCGSetup          # TAILCALL
.Lfunc_end2:
	.size	HYPRE_StructPCGSetup, .Lfunc_end2-HYPRE_StructPCGSetup
	.cfi_endproc

	.globl	HYPRE_StructPCGSolve
	.p2align	4, 0x90
	.type	HYPRE_StructPCGSolve,@function
HYPRE_StructPCGSolve:                   # @HYPRE_StructPCGSolve
	.cfi_startproc
# BB#0:
	jmp	HYPRE_PCGSolve          # TAILCALL
.Lfunc_end3:
	.size	HYPRE_StructPCGSolve, .Lfunc_end3-HYPRE_StructPCGSolve
	.cfi_endproc

	.globl	HYPRE_StructPCGSetTol
	.p2align	4, 0x90
	.type	HYPRE_StructPCGSetTol,@function
HYPRE_StructPCGSetTol:                  # @HYPRE_StructPCGSetTol
	.cfi_startproc
# BB#0:
	jmp	HYPRE_PCGSetTol         # TAILCALL
.Lfunc_end4:
	.size	HYPRE_StructPCGSetTol, .Lfunc_end4-HYPRE_StructPCGSetTol
	.cfi_endproc

	.globl	HYPRE_StructPCGSetMaxIter
	.p2align	4, 0x90
	.type	HYPRE_StructPCGSetMaxIter,@function
HYPRE_StructPCGSetMaxIter:              # @HYPRE_StructPCGSetMaxIter
	.cfi_startproc
# BB#0:
	jmp	HYPRE_PCGSetMaxIter     # TAILCALL
.Lfunc_end5:
	.size	HYPRE_StructPCGSetMaxIter, .Lfunc_end5-HYPRE_StructPCGSetMaxIter
	.cfi_endproc

	.globl	HYPRE_StructPCGSetTwoNorm
	.p2align	4, 0x90
	.type	HYPRE_StructPCGSetTwoNorm,@function
HYPRE_StructPCGSetTwoNorm:              # @HYPRE_StructPCGSetTwoNorm
	.cfi_startproc
# BB#0:
	jmp	HYPRE_PCGSetTwoNorm     # TAILCALL
.Lfunc_end6:
	.size	HYPRE_StructPCGSetTwoNorm, .Lfunc_end6-HYPRE_StructPCGSetTwoNorm
	.cfi_endproc

	.globl	HYPRE_StructPCGSetRelChange
	.p2align	4, 0x90
	.type	HYPRE_StructPCGSetRelChange,@function
HYPRE_StructPCGSetRelChange:            # @HYPRE_StructPCGSetRelChange
	.cfi_startproc
# BB#0:
	jmp	HYPRE_PCGSetRelChange   # TAILCALL
.Lfunc_end7:
	.size	HYPRE_StructPCGSetRelChange, .Lfunc_end7-HYPRE_StructPCGSetRelChange
	.cfi_endproc

	.globl	HYPRE_StructPCGSetPrecond
	.p2align	4, 0x90
	.type	HYPRE_StructPCGSetPrecond,@function
HYPRE_StructPCGSetPrecond:              # @HYPRE_StructPCGSetPrecond
	.cfi_startproc
# BB#0:
	jmp	HYPRE_PCGSetPrecond     # TAILCALL
.Lfunc_end8:
	.size	HYPRE_StructPCGSetPrecond, .Lfunc_end8-HYPRE_StructPCGSetPrecond
	.cfi_endproc

	.globl	HYPRE_StructPCGSetLogging
	.p2align	4, 0x90
	.type	HYPRE_StructPCGSetLogging,@function
HYPRE_StructPCGSetLogging:              # @HYPRE_StructPCGSetLogging
	.cfi_startproc
# BB#0:
	jmp	HYPRE_PCGSetLogging     # TAILCALL
.Lfunc_end9:
	.size	HYPRE_StructPCGSetLogging, .Lfunc_end9-HYPRE_StructPCGSetLogging
	.cfi_endproc

	.globl	HYPRE_StructPCGGetNumIterations
	.p2align	4, 0x90
	.type	HYPRE_StructPCGGetNumIterations,@function
HYPRE_StructPCGGetNumIterations:        # @HYPRE_StructPCGGetNumIterations
	.cfi_startproc
# BB#0:
	jmp	HYPRE_PCGGetNumIterations # TAILCALL
.Lfunc_end10:
	.size	HYPRE_StructPCGGetNumIterations, .Lfunc_end10-HYPRE_StructPCGGetNumIterations
	.cfi_endproc

	.globl	HYPRE_StructPCGGetFinalRelativeResidualNorm
	.p2align	4, 0x90
	.type	HYPRE_StructPCGGetFinalRelativeResidualNorm,@function
HYPRE_StructPCGGetFinalRelativeResidualNorm: # @HYPRE_StructPCGGetFinalRelativeResidualNorm
	.cfi_startproc
# BB#0:
	jmp	HYPRE_PCGGetFinalRelativeResidualNorm # TAILCALL
.Lfunc_end11:
	.size	HYPRE_StructPCGGetFinalRelativeResidualNorm, .Lfunc_end11-HYPRE_StructPCGGetFinalRelativeResidualNorm
	.cfi_endproc

	.globl	HYPRE_StructDiagScaleSetup
	.p2align	4, 0x90
	.type	HYPRE_StructDiagScaleSetup,@function
HYPRE_StructDiagScaleSetup:             # @HYPRE_StructDiagScaleSetup
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end12:
	.size	HYPRE_StructDiagScaleSetup, .Lfunc_end12-HYPRE_StructDiagScaleSetup
	.cfi_endproc

	.globl	HYPRE_StructDiagScale
	.p2align	4, 0x90
	.type	HYPRE_StructDiagScale,@function
HYPRE_StructDiagScale:                  # @HYPRE_StructDiagScale
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	subq	$248, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 304
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	movq	%rdx, 200(%rsp)         # 8-byte Spill
	movq	%rsi, 192(%rsp)         # 8-byte Spill
	movq	8(%rsi), %rax
	movq	8(%rax), %rcx
	cmpl	$0, 8(%rcx)
	jle	.LBB13_20
# BB#1:                                 # %.lr.ph407
	xorl	%r12d, %r12d
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB13_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_6 Depth 2
                                        #       Child Loop BB13_8 Depth 3
                                        #         Child Loop BB13_29 Depth 4
                                        #         Child Loop BB13_15 Depth 4
	movq	(%rcx), %rcx
	leaq	(,%r12,8), %rax
	leaq	(%rax,%rax,2), %r14
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	leaq	(%rcx,%r14), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	192(%rsp), %rdi         # 8-byte Reload
	movq	40(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	208(%rsp), %rbx         # 8-byte Reload
	movq	16(%rbx), %rax
	movq	(%rax), %r15
	movq	200(%rsp), %rbp         # 8-byte Reload
	movq	16(%rbp), %rax
	movq	(%rax), %r13
	movq	$0, 236(%rsp)
	movl	$0, 244(%rsp)
	movl	%r12d, %esi
	leaq	236(%rsp), %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	24(%rbx), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	40(%rbx), %rax
	movslq	(%rax,%r12,4), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	24(%rbp), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	40(%rbp), %rax
	movq	%r12, 216(%rsp)         # 8-byte Spill
	movslq	(%rax,%r12,4), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rbx
	leaq	172(%rsp), %rsi
	callq	hypre_BoxGetSize
	movq	%r15, %r11
	movq	24(%rsp), %r13          # 8-byte Reload
	movl	(%rbx,%r14), %edx
	movl	4(%rbx,%r14), %r9d
	movl	12(%rbx,%r14), %eax
	movl	%eax, %ecx
	subl	%edx, %ecx
	incl	%ecx
	cmpl	%edx, %eax
	movl	16(%rbx,%r14), %eax
	movl	$0, %esi
	cmovsl	%esi, %ecx
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movl	%eax, %r15d
	subl	%r9d, %r15d
	incl	%r15d
	cmpl	%r9d, %eax
	movl	(%r11,%r14), %r10d
	movl	4(%r11,%r14), %ebp
	movl	12(%r11,%r14), %eax
	cmovsl	%esi, %r15d
	movl	%eax, %edi
	subl	%r10d, %edi
	incl	%edi
	cmpl	%r10d, %eax
	movl	16(%r11,%r14), %ecx
	cmovsl	%esi, %edi
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movl	%ecx, %eax
	subl	%ebp, %eax
	incl	%eax
	movl	%ebp, (%rsp)            # 4-byte Spill
	cmpl	%ebp, %ecx
	movl	(%r13,%r14), %ebp
	movl	4(%r13,%r14), %r12d
	movl	12(%r13,%r14), %ecx
	cmovsl	%esi, %eax
	movl	%ecx, %edi
	subl	%ebp, %edi
	incl	%edi
	movl	%ebp, 72(%rsp)          # 4-byte Spill
	cmpl	%ebp, %ecx
	movl	16(%r13,%r14), %ecx
	cmovsl	%esi, %edi
	movl	%edi, 16(%rsp)          # 4-byte Spill
	movl	%ecx, %r8d
	subl	%r12d, %r8d
	incl	%r8d
	movl	%r12d, 96(%rsp)         # 4-byte Spill
	cmpl	%r12d, %ecx
	cmovsl	%esi, %r8d
	movl	172(%rsp), %ecx
	movl	176(%rsp), %esi
	cmpl	%ecx, %esi
	movq	%rcx, 32(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	%rsi, 120(%rsp)         # 8-byte Spill
	cmovgel	%esi, %ecx
	movl	180(%rsp), %esi
	cmpl	%ecx, %esi
	movl	%esi, 44(%rsp)          # 4-byte Spill
	cmovgel	%esi, %ecx
	testl	%ecx, %ecx
	jle	.LBB13_19
# BB#3:                                 # %.lr.ph403
                                        #   in Loop: Header=BB13_2 Depth=1
	cmpl	$0, 44(%rsp)            # 4-byte Folded Reload
	jle	.LBB13_19
# BB#4:                                 # %.lr.ph403
                                        #   in Loop: Header=BB13_2 Depth=1
	cmpl	$0, 120(%rsp)           # 4-byte Folded Reload
	jle	.LBB13_19
# BB#5:                                 # %.preheader359.us.preheader
                                        #   in Loop: Header=BB13_2 Depth=1
	movq	112(%rsp), %rsi         # 8-byte Reload
	movl	8(%rsi,%r14), %ecx
	movl	%r10d, 64(%rsp)         # 4-byte Spill
	movl	%ecx, %r10d
	subl	8(%rbx,%r14), %r10d
	movl	%edx, %ebp
	movl	%r9d, 4(%rsp)           # 4-byte Spill
	movl	%ecx, %r9d
	subl	8(%r11,%r14), %r9d
	subl	8(%r13,%r14), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	(%rsi,%r14), %ecx
	movl	4(%rsi,%r14), %edi
	movl	16(%rsp), %r12d         # 4-byte Reload
	movq	32(%rsp), %r11          # 8-byte Reload
	subl	%r11d, %r12d
	movl	12(%rsp), %ebx          # 4-byte Reload
	subl	%r11d, %ebx
	movl	20(%rsp), %edx          # 4-byte Reload
	movl	%edx, %r14d
	subl	%r11d, %r14d
	movq	120(%rsp), %r13         # 8-byte Reload
	leal	-1(%r13), %esi
	imull	%esi, %r12d
	imull	%esi, %ebx
	imull	%esi, %r14d
	movl	%ecx, %esi
	subl	%ebp, %esi
	movl	%edi, %ebp
	subl	4(%rsp), %ebp           # 4-byte Folded Reload
	imull	%r15d, %r10d
	addl	%ebp, %r10d
	imull	%edx, %r10d
	addl	%esi, %r10d
	movl	%r10d, 4(%rsp)          # 4-byte Spill
	movl	%ecx, %esi
	subl	64(%rsp), %esi          # 4-byte Folded Reload
	movl	%edi, %edx
	subl	(%rsp), %edx            # 4-byte Folded Reload
	imull	%eax, %r9d
	addl	%edx, %r9d
	imull	12(%rsp), %r9d          # 4-byte Folded Reload
	addl	%esi, %r9d
	movl	%r9d, (%rsp)            # 4-byte Spill
	subl	72(%rsp), %ecx          # 4-byte Folded Reload
	subl	96(%rsp), %edi          # 4-byte Folded Reload
	movl	8(%rsp), %ebp           # 4-byte Reload
	imull	%r8d, %ebp
	addl	%edi, %ebp
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	104(%rsp), %rdi         # 8-byte Reload
	leaq	(%r9,%rdi,8), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movl	16(%rsp), %esi          # 4-byte Reload
	imull	%esi, %ebp
	addl	%ecx, %ebp
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	leal	-1(%r11), %ecx
	addq	%rcx, %rdi
	leaq	8(%r9,%rdi,8), %rdx
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rdx,8), %r10
	subl	%r13d, %r8d
	imull	%esi, %r8d
	movl	%r8d, 152(%rsp)         # 4-byte Spill
	subl	%r13d, %eax
	movl	12(%rsp), %edi          # 4-byte Reload
	imull	%edi, %eax
	movl	%eax, 148(%rsp)         # 4-byte Spill
	subl	%r13d, %r15d
	movl	20(%rsp), %eax          # 4-byte Reload
	imull	%eax, %r15d
	movl	%r15d, 144(%rsp)        # 4-byte Spill
	addl	%esi, %r12d
	subl	%r11d, %r12d
	movl	%r12d, 164(%rsp)        # 4-byte Spill
	addl	%edi, %ebx
	subl	%r11d, %ebx
	movl	%ebx, 160(%rsp)         # 4-byte Spill
	addl	%eax, %r14d
	movl	%eax, %ebx
	subl	%r11d, %r14d
	movl	%r14d, 156(%rsp)        # 4-byte Spill
	movq	%rdx, %rax
	addq	%rcx, %rax
	leaq	8(%rbp,%rax,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	%esi, %eax
	imull	%r13d, %eax
	movl	%eax, 140(%rsp)         # 4-byte Spill
	movl	%edi, %eax
	imull	%r13d, %eax
	movl	%eax, 136(%rsp)         # 4-byte Spill
	movl	%ebx, %eax
	imull	%r13d, %eax
	movl	%eax, 132(%rsp)         # 4-byte Spill
	leaq	1(%rcx), %r14
	movq	%r14, 56(%rsp)          # 8-byte Spill
	movabsq	$8589934590, %rax       # imm = 0x1FFFFFFFE
	andq	%rax, %r14
	leaq	-2(%r14), %rax
	shrq	%rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %rdx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	leaq	8(%rax,%rcx,8), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	leaq	16(%rax), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB13_6:                               # %.preheader359.us
                                        #   Parent Loop BB13_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_8 Depth 3
                                        #         Child Loop BB13_29 Depth 4
                                        #         Child Loop BB13_15 Depth 4
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	movl	160(%rsp), %eax         # 4-byte Reload
	movl	156(%rsp), %edx         # 4-byte Reload
	movl	164(%rsp), %ecx         # 4-byte Reload
	jle	.LBB13_18
# BB#7:                                 # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB13_6 Depth=2
	movl	%esi, 168(%rsp)         # 4-byte Spill
	xorl	%r9d, %r9d
	movl	(%rsp), %ebp            # 4-byte Reload
	movl	4(%rsp), %ebx           # 4-byte Reload
	movl	8(%rsp), %r11d          # 4-byte Reload
	jmp	.LBB13_8
.LBB13_25:                              #   in Loop: Header=BB13_8 Depth=3
	xorl	%edx, %edx
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	je	.LBB13_30
.LBB13_28:                              # %vector.body.preheader.new
                                        #   in Loop: Header=BB13_8 Depth=3
	movq	224(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12,8), %rsi
	leaq	(%r10,%r13,8), %rcx
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r15,8), %rdi
	.p2align	4, 0x90
.LBB13_29:                              # %vector.body
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_6 Depth=2
                                        #       Parent Loop BB13_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movupd	(%rcx,%rdx,8), %xmm0
	movupd	-16(%rsi,%rdx,8), %xmm1
	divpd	%xmm1, %xmm0
	movupd	%xmm0, (%rdi,%rdx,8)
	movupd	16(%rcx,%rdx,8), %xmm0
	movupd	(%rsi,%rdx,8), %xmm1
	divpd	%xmm1, %xmm0
	movupd	%xmm0, 16(%rdi,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %r14
	jne	.LBB13_29
.LBB13_30:                              # %middle.block
                                        #   in Loop: Header=BB13_8 Depth=3
	cmpq	%r14, 56(%rsp)          # 8-byte Folded Reload
	je	.LBB13_16
# BB#31:                                #   in Loop: Header=BB13_8 Depth=3
	addq	%r14, %r15
	addq	%r14, %r13
	addq	%r14, %r12
	movl	%r14d, %r8d
	jmp	.LBB13_10
	.p2align	4, 0x90
.LBB13_8:                               # %.preheader.us.us
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB13_29 Depth 4
                                        #         Child Loop BB13_15 Depth 4
	movslq	%r11d, %r12
	movslq	%ebx, %r13
	movslq	%ebp, %r15
	cmpq	$1, 56(%rsp)            # 8-byte Folded Reload
	jbe	.LBB13_9
# BB#21:                                # %min.iters.checked
                                        #   in Loop: Header=BB13_8 Depth=3
	testq	%r14, %r14
	je	.LBB13_9
# BB#22:                                # %vector.memcheck
                                        #   in Loop: Header=BB13_8 Depth=3
	movl	12(%rsp), %ecx          # 4-byte Reload
	imull	%r9d, %ecx
	addl	(%rsp), %ecx            # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx,8), %r8
	movl	20(%rsp), %esi          # 4-byte Reload
	imull	%r9d, %esi
	addl	4(%rsp), %esi           # 4-byte Folded Reload
	movslq	%esi, %rsi
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rdi
	movl	16(%rsp), %eax          # 4-byte Reload
	imull	%r9d, %eax
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	cmpq	%rdi, %r8
	movq	96(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdi
	leaq	(%r10,%rsi,8), %rcx
	cltq
	sbbb	%sil, %sil
	cmpq	%rdi, %rcx
	sbbb	%dl, %dl
	andb	%sil, %dl
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	cmpq	%rcx, %r8
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	sbbb	%cl, %cl
	cmpq	%rdi, %rax
	sbbb	%al, %al
	testb	$1, %dl
	jne	.LBB13_9
# BB#23:                                # %vector.memcheck
                                        #   in Loop: Header=BB13_8 Depth=3
	andb	%al, %cl
	andb	$1, %cl
	jne	.LBB13_9
# BB#24:                                # %vector.body.preheader
                                        #   in Loop: Header=BB13_8 Depth=3
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	jne	.LBB13_25
# BB#26:                                # %vector.body.prol
                                        #   in Loop: Header=BB13_8 Depth=3
	movupd	(%r10,%r13,8), %xmm0
	movq	48(%rsp), %rax          # 8-byte Reload
	movupd	(%rax,%r12,8), %xmm1
	divpd	%xmm1, %xmm0
	movq	24(%rsp), %rax          # 8-byte Reload
	movupd	%xmm0, (%rax,%r15,8)
	movl	$2, %edx
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	jne	.LBB13_28
	jmp	.LBB13_30
	.p2align	4, 0x90
.LBB13_9:                               #   in Loop: Header=BB13_8 Depth=3
	xorl	%r8d, %r8d
.LBB13_10:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB13_8 Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	subl	%r8d, %ecx
	testb	$1, %cl
	jne	.LBB13_12
# BB#11:                                #   in Loop: Header=BB13_8 Depth=3
	movl	%r8d, %ecx
	cmpl	%r8d, 112(%rsp)         # 4-byte Folded Reload
	jne	.LBB13_14
	jmp	.LBB13_16
	.p2align	4, 0x90
.LBB13_12:                              # %scalar.ph.prol
                                        #   in Loop: Header=BB13_8 Depth=3
	movsd	(%r10,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movq	48(%rsp), %rax          # 8-byte Reload
	divsd	(%rax,%r12,8), %xmm0
	movq	24(%rsp), %rax          # 8-byte Reload
	movsd	%xmm0, (%rax,%r15,8)
	incq	%r12
	incq	%r15
	incq	%r13
	leal	1(%r8), %ecx
	cmpl	%r8d, 112(%rsp)         # 4-byte Folded Reload
	je	.LBB13_16
.LBB13_14:                              # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB13_8 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax,%r15,8), %rax
	leaq	8(%r10,%r13,8), %rdi
	movq	104(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r12,8), %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	subl	%ecx, %esi
	.p2align	4, 0x90
.LBB13_15:                              # %scalar.ph
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_6 Depth=2
                                        #       Parent Loop BB13_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	-8(%rdi), %xmm0         # xmm0 = mem[0],zero
	divsd	-8(%rdx), %xmm0
	movsd	%xmm0, -8(%rax)
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	divsd	(%rdx), %xmm0
	movsd	%xmm0, (%rax)
	addq	$16, %rax
	addq	$16, %rdi
	addq	$16, %rdx
	addl	$-2, %esi
	jne	.LBB13_15
.LBB13_16:                              # %._crit_edge.us.us
                                        #   in Loop: Header=BB13_8 Depth=3
	addl	16(%rsp), %r11d         # 4-byte Folded Reload
	addl	12(%rsp), %ebp          # 4-byte Folded Reload
	addl	20(%rsp), %ebx          # 4-byte Folded Reload
	incl	%r9d
	cmpl	120(%rsp), %r9d         # 4-byte Folded Reload
	jne	.LBB13_8
# BB#17:                                #   in Loop: Header=BB13_6 Depth=2
	movl	136(%rsp), %eax         # 4-byte Reload
	movl	132(%rsp), %edx         # 4-byte Reload
	movl	140(%rsp), %ecx         # 4-byte Reload
	movl	168(%rsp), %esi         # 4-byte Reload
.LBB13_18:                              # %._crit_edge370.us
                                        #   in Loop: Header=BB13_6 Depth=2
	addl	8(%rsp), %ecx           # 4-byte Folded Reload
	addl	4(%rsp), %edx           # 4-byte Folded Reload
	addl	(%rsp), %eax            # 4-byte Folded Reload
	addl	152(%rsp), %ecx         # 4-byte Folded Reload
	addl	148(%rsp), %eax         # 4-byte Folded Reload
	addl	144(%rsp), %edx         # 4-byte Folded Reload
	incl	%esi
	cmpl	44(%rsp), %esi          # 4-byte Folded Reload
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movl	%eax, (%rsp)            # 4-byte Spill
	jne	.LBB13_6
.LBB13_19:                              # %._crit_edge404
                                        #   in Loop: Header=BB13_2 Depth=1
	movq	216(%rsp), %r12         # 8-byte Reload
	incq	%r12
	movq	184(%rsp), %rcx         # 8-byte Reload
	movslq	8(%rcx), %rax
	cmpq	%rax, %r12
	jl	.LBB13_2
.LBB13_20:                              # %._crit_edge408
	xorl	%eax, %eax
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	HYPRE_StructDiagScale, .Lfunc_end13-HYPRE_StructDiagScale
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
