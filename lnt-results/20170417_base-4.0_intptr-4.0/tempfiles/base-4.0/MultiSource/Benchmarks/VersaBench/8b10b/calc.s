	.text
	.file	"calc.bc"
	.globl	calcSetup
	.p2align	4, 0x90
	.type	calcSetup,@function
calcSetup:                              # @calcSetup
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	calcSetup, .Lfunc_end0-calcSetup
	.cfi_endproc

	.globl	calc
	.p2align	4, 0x90
	.type	calc,@function
calc:                                   # @calc
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movl	%edi, %eax
	shrl	$3, %eax
	andl	$31, %eax
	andl	$7, %edi
	leal	(%rdi,%rsi,8), %edx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	shll	$5, %esi
	orl	%eax, %esi
	movl	lookupTable5B(,%rsi,4), %ecx
	movl	lookupTable3B(,%rdx,4), %eax
	testl	$65536, %ecx            # imm = 0x10000
	jne	.LBB1_1
# BB#2:
	movl	disparity0(%rip), %r8d
	movl	%ecx, %edx
	shrl	$18, %edx
	andl	$1, %edx
	movl	%ecx, %esi
	andl	$994, %esi              # imm = 0x3E2
	movl	%esi, %edi
	xorl	$994, %edi              # imm = 0x3E2
	cmpl	%edx, %r8d
	cmovel	%esi, %edi
	shrl	$19, %ecx
	andl	$1, %ecx
	movl	%ecx, %r9d
	xorl	%r8d, %r9d
	movl	%edi, %ecx
	jmp	.LBB1_3
.LBB1_1:
	andl	$994, %ecx              # imm = 0x3E2
	movl	disparity0(%rip), %r9d
.LBB1_3:
	movl	%r9d, disparity1(%rip)
	testl	$65536, %eax            # imm = 0x10000
	jne	.LBB1_4
# BB#5:
	movl	%eax, %esi
	shrl	$18, %esi
	andl	$1, %esi
	movl	%eax, %edx
	andl	$29, %edx
	movl	%edx, %edi
	xorl	$29, %edi
	cmpl	%esi, %r9d
	cmovel	%edx, %edi
	shrl	$19, %eax
	andl	$1, %eax
	xorl	%eax, %r9d
	movl	%edi, %eax
	jmp	.LBB1_6
.LBB1_4:
	andl	$29, %eax
.LBB1_6:
	orl	%ecx, %eax
	movl	%r9d, disparity0(%rip)
	retq
.Lfunc_end1:
	.size	calc, .Lfunc_end1-calc
	.cfi_endproc

	.globl	bigTableCalc
	.p2align	4, 0x90
	.type	bigTableCalc,@function
bigTableCalc:                           # @bigTableCalc
	.cfi_startproc
# BB#0:
	movl	disparity0(%rip), %eax
	shll	$9, %eax
	orl	%edi, %eax
	movl	bigTable(,%rax,4), %eax
	movl	%eax, %ecx
	shrl	$16, %ecx
	movl	%ecx, disparity0(%rip)
	andl	$1023, %eax             # imm = 0x3FF
	retq
.Lfunc_end2:
	.size	bigTableCalc, .Lfunc_end2-bigTableCalc
	.cfi_endproc

	.globl	resetDisparity
	.p2align	4, 0x90
	.type	resetDisparity,@function
resetDisparity:                         # @resetDisparity
	.cfi_startproc
# BB#0:
	movl	$0, disparity0(%rip)
	retq
.Lfunc_end3:
	.size	resetDisparity, .Lfunc_end3-resetDisparity
	.cfi_endproc

	.globl	bigTableSetup
	.p2align	4, 0x90
	.type	bigTableSetup,@function
bigTableSetup:                          # @bigTableSetup
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	movl	%eax, %r8d
	sarl	$9, %r8d
	movl	%eax, %edx
	shrl	$8, %edx
	andl	$1, %edx
	movl	%eax, %esi
	shrl	$3, %esi
	andl	$31, %esi
	movl	%eax, %edi
	andl	$7, %edi
	leal	(%rdi,%rdx,8), %edi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	shll	$5, %edx
	orl	%esi, %edx
	movl	lookupTable5B(,%rdx,4), %esi
	movl	lookupTable3B(,%rdi,4), %edx
	testl	$65536, %esi            # imm = 0x10000
	jne	.LBB4_2
# BB#3:                                 #   in Loop: Header=BB4_1 Depth=1
	movl	%esi, %edi
	shrl	$18, %edi
	andl	$1, %edi
	movl	%esi, %r9d
	andl	$994, %r9d              # imm = 0x3E2
	movl	%r9d, %ecx
	xorl	$994, %ecx              # imm = 0x3E2
	cmpl	%edi, %r8d
	cmovel	%r9d, %ecx
	shrl	$19, %esi
	andl	$1, %esi
	xorl	%esi, %r8d
	movl	%ecx, %esi
	testl	$65536, %edx            # imm = 0x10000
	je	.LBB4_6
	jmp	.LBB4_5
	.p2align	4, 0x90
.LBB4_2:                                #   in Loop: Header=BB4_1 Depth=1
	andl	$994, %esi              # imm = 0x3E2
	testl	$65536, %edx            # imm = 0x10000
	jne	.LBB4_5
.LBB4_6:                                #   in Loop: Header=BB4_1 Depth=1
	movl	%edx, %ecx
	shrl	$18, %ecx
	andl	$1, %ecx
	movl	%edx, %edi
	andl	$29, %edi
	movl	%edi, %r9d
	xorl	$29, %r9d
	cmpl	%ecx, %r8d
	cmovel	%edi, %r9d
	shrl	$19, %edx
	andl	$1, %edx
	movl	%edx, %edi
	xorl	%r8d, %edi
	movl	%r9d, %edx
	jmp	.LBB4_7
	.p2align	4, 0x90
.LBB4_5:                                #   in Loop: Header=BB4_1 Depth=1
	andl	$29, %edx
	movl	%r8d, %edi
.LBB4_7:                                # %calc.exit
                                        #   in Loop: Header=BB4_1 Depth=1
	orl	%esi, %edx
	shll	$16, %edi
	orl	%edx, %edi
	movl	%edi, bigTable(,%rax,4)
	incq	%rax
	cmpq	$1024, %rax             # imm = 0x400
	jne	.LBB4_1
# BB#8:
	movl	%r8d, disparity1(%rip)
	movl	$0, disparity0(%rip)
	retq
.Lfunc_end4:
	.size	bigTableSetup, .Lfunc_end4-bigTableSetup
	.cfi_endproc

	.type	lookupTable5B,@object   # @lookupTable5B
	.data
	.globl	lookupTable5B
	.p2align	4
lookupTable5B:
	.long	786816                  # 0xc0180
	.long	655778                  # 0xa01a2
	.long	786528                  # 0xc0060
	.long	786624                  # 0xc00c0
	.long	786592                  # 0xc00a0
	.long	65698                   # 0x100a2
	.long	65730                   # 0x100c2
	.long	65760                   # 0x100e0
	.long	786720                  # 0xc0120
	.long	65826                   # 0x10122
	.long	65858                   # 0x10142
	.long	65888                   # 0x10160
	.long	65922                   # 0x10182
	.long	65952                   # 0x101a0
	.long	65984                   # 0x101c0
	.long	655840                  # 0xa01e0
	.long	786976                  # 0xc0220
	.long	66082                   # 0x10222
	.long	66114                   # 0x10242
	.long	66144                   # 0x10260
	.long	66178                   # 0x10282
	.long	66208                   # 0x102a0
	.long	66240                   # 0x102c0
	.long	656096                  # 0xa02e0
	.long	66306                   # 0x10302
	.long	66336                   # 0x10320
	.long	66368                   # 0x10340
	.long	656224                  # 0xa0360
	.long	131968                  # 0x20380
	.long	656288                  # 0xa03a0
	.long	787072                  # 0xc0280
	.long	656034                  # 0xa02a2
	.long	786816                  # 0xc0180
	.long	655778                  # 0xa01a2
	.long	786528                  # 0xc0060
	.long	786624                  # 0xc00c0
	.long	786592                  # 0xc00a0
	.long	65698                   # 0x100a2
	.long	65730                   # 0x100c2
	.long	655586                  # 0xa00e2
	.long	786720                  # 0xc0120
	.long	65826                   # 0x10122
	.long	65858                   # 0x10142
	.long	65888                   # 0x10160
	.long	65922                   # 0x10182
	.long	65952                   # 0x101a0
	.long	65984                   # 0x101c0
	.long	655840                  # 0xa01e0
	.long	786976                  # 0xc0220
	.long	66082                   # 0x10222
	.long	66114                   # 0x10242
	.long	66144                   # 0x10260
	.long	66178                   # 0x10282
	.long	66208                   # 0x102a0
	.long	66240                   # 0x102c0
	.long	656096                  # 0xa02e0
	.long	66306                   # 0x10302
	.long	66336                   # 0x10320
	.long	66368                   # 0x10340
	.long	656224                  # 0xa0360
	.long	131968                  # 0x20380
	.long	656288                  # 0xa03a0
	.long	787072                  # 0xc0280
	.long	656034                  # 0xa02a2
	.size	lookupTable5B, 256

	.type	lookupTable3B,@object   # @lookupTable3B
	.globl	lookupTable3B
	.p2align	4
lookupTable3B:
	.long	786440                  # 0xc0008
	.long	786436                  # 0xc0004
	.long	65545                   # 0x10009
	.long	65548                   # 0x1000c
	.long	65553                   # 0x10011
	.long	65556                   # 0x10014
	.long	131096                  # 0x20018
	.long	655388                  # 0xa001c
	.long	786440                  # 0xc0008
	.long	786436                  # 0xc0004
	.long	262153                  # 0x40009
	.long	262156                  # 0x4000c
	.long	262161                  # 0x40011
	.long	262164                  # 0x40014
	.long	131096                  # 0x20018
	.long	655373                  # 0xa000d
	.size	lookupTable3B, 64

	.type	disparity0,@object      # @disparity0
	.bss
	.globl	disparity0
	.p2align	2
disparity0:
	.long	0                       # 0x0
	.size	disparity0, 4

	.type	disparity1,@object      # @disparity1
	.data
	.globl	disparity1
	.p2align	2
disparity1:
	.long	1                       # 0x1
	.size	disparity1, 4

	.type	bigTable,@object        # @bigTable
	.comm	bigTable,4096,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
