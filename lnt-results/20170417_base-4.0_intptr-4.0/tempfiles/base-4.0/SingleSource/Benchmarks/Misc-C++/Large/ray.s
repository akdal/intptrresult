	.text
	.file	"ray.bc"
	.globl	_ZplRK3VecS1_
	.p2align	4, 0x90
	.type	_ZplRK3VecS1_,@function
_ZplRK3VecS1_:                          # @_ZplRK3VecS1_
	.cfi_startproc
# BB#0:
	movupd	(%rsi), %xmm0
	movupd	(%rdx), %xmm1
	addpd	%xmm0, %xmm1
	movsd	16(%rsi), %xmm0         # xmm0 = mem[0],zero
	addsd	16(%rdx), %xmm0
	movupd	%xmm1, (%rdi)
	movsd	%xmm0, 16(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end0:
	.size	_ZplRK3VecS1_, .Lfunc_end0-_ZplRK3VecS1_
	.cfi_endproc

	.globl	_ZmiRK3VecS1_
	.p2align	4, 0x90
	.type	_ZmiRK3VecS1_,@function
_ZmiRK3VecS1_:                          # @_ZmiRK3VecS1_
	.cfi_startproc
# BB#0:
	movupd	(%rsi), %xmm0
	movupd	(%rdx), %xmm1
	subpd	%xmm1, %xmm0
	movsd	16(%rsi), %xmm1         # xmm1 = mem[0],zero
	subsd	16(%rdx), %xmm1
	movupd	%xmm0, (%rdi)
	movsd	%xmm1, 16(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end1:
	.size	_ZmiRK3VecS1_, .Lfunc_end1-_ZmiRK3VecS1_
	.cfi_endproc

	.globl	_ZmldRK3Vec
	.p2align	4, 0x90
	.type	_ZmldRK3Vec,@function
_ZmldRK3Vec:                            # @_ZmldRK3Vec
	.cfi_startproc
# BB#0:
	movupd	(%rsi), %xmm1
	movaps	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm1, %xmm2
	mulsd	16(%rsi), %xmm0
	movupd	%xmm2, (%rdi)
	movsd	%xmm0, 16(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end2:
	.size	_ZmldRK3Vec, .Lfunc_end2-_ZmldRK3Vec
	.cfi_endproc

	.globl	_Z3dotRK3VecS1_
	.p2align	4, 0x90
	.type	_Z3dotRK3VecS1_,@function
_Z3dotRK3VecS1_:                        # @_Z3dotRK3VecS1_
	.cfi_startproc
# BB#0:
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rdi), %xmm1          # xmm1 = mem[0],zero
	mulsd	(%rsi), %xmm0
	mulsd	8(%rsi), %xmm1
	addsd	%xmm0, %xmm1
	movsd	16(%rdi), %xmm0         # xmm0 = mem[0],zero
	mulsd	16(%rsi), %xmm0
	addsd	%xmm1, %xmm0
	retq
.Lfunc_end3:
	.size	_Z3dotRK3VecS1_, .Lfunc_end3-_Z3dotRK3VecS1_
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	_Z7unitiseRK3Vec
	.p2align	4, 0x90
	.type	_Z7unitiseRK3Vec,@function
_Z7unitiseRK3Vec:                       # @_Z7unitiseRK3Vec
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB4_2
# BB#1:                                 # %call.sqrt
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB4_2:                                # %.split
	movsd	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	movupd	(%rbx), %xmm1
	movapd	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm1, %xmm2
	mulsd	16(%rbx), %xmm0
	movupd	%xmm2, (%r14)
	movsd	%xmm0, 16(%r14)
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	_Z7unitiseRK3Vec, .Lfunc_end4-_Z7unitiseRK3Vec
	.cfi_endproc

	.globl	_Z9intersectRK3RayRK5Scene
	.p2align	4, 0x90
	.type	_Z9intersectRK3RayRK5Scene,@function
_Z9intersectRK3RayRK5Scene:             # @_Z9intersectRK3RayRK5Scene
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 48
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rsi, %r8
	movq	%rdi, %rbx
	movq	(%rdx), %rcx
	movq	16(%rcx), %rax
	movq	infinity(%rip), %rcx
	movq	%rcx, (%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rsp)
	movq	$0, 24(%rsp)
	movq	%rsp, %rcx
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	%r8, %rcx
	callq	*%rax
	movq	%rbx, %rax
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end5:
	.size	_Z9intersectRK3RayRK5Scene, .Lfunc_end5-_Z9intersectRK3RayRK5Scene
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	_Z9ray_traceRK3VecRK3RayRK5Scene
	.p2align	4, 0x90
	.type	_Z9ray_traceRK3VecRK3RayRK5Scene,@function
_Z9ray_traceRK3VecRK3RayRK5Scene:       # @_Z9ray_traceRK3VecRK3RayRK5Scene
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 32
	subq	$160, %rsp
.Lcfi11:
	.cfi_def_cfa_offset 192
.Lcfi12:
	.cfi_offset %rbx, -32
.Lcfi13:
	.cfi_offset %r14, -24
.Lcfi14:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	(%r14), %rax
	movq	16(%rax), %rax
	movq	infinity(%rip), %rcx
	movq	%rcx, (%rsp)
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 8(%rsp)
	movq	$0, 24(%rsp)
	leaq	64(%rsp), %rdi
	movq	%rsp, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rcx
	callq	*%rax
	movsd	64(%rsp), %xmm2         # xmm2 = mem[0],zero
	movsd	infinity(%rip), %xmm1   # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm1, %xmm2
	jne	.LBB6_1
	jnp	.LBB6_3
.LBB6_1:
	movupd	72(%rsp), %xmm4
	movupd	(%r15), %xmm3
	movapd	%xmm4, %xmm0
	mulsd	%xmm3, %xmm0
	movapd	%xmm4, %xmm5
	movhlps	%xmm5, %xmm5            # xmm5 = xmm5[1,1]
	movapd	%xmm3, %xmm7
	movhlps	%xmm7, %xmm7            # xmm7 = xmm7[1,1]
	mulsd	%xmm5, %xmm7
	addsd	%xmm0, %xmm7
	movsd	88(%rsp), %xmm5         # xmm5 = mem[0],zero
	movsd	16(%r15), %xmm9         # xmm9 = mem[0],zero
	movapd	%xmm5, %xmm6
	mulsd	%xmm9, %xmm6
	addsd	%xmm7, %xmm6
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm6
	jae	.LBB6_3
# BB#2:
	movapd	%xmm6, 48(%rsp)         # 16-byte Spill
	movupd	(%rbx), %xmm8
	movupd	24(%rbx), %xmm7
	movaps	%xmm2, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	%xmm7, %xmm0
	mulsd	40(%rbx), %xmm2
	addpd	%xmm8, %xmm0
	addsd	16(%rbx), %xmm2
	movsd	delta(%rip), %xmm7      # xmm7 = mem[0],zero
	mulsd	%xmm7, %xmm5
	movlhps	%xmm7, %xmm7            # xmm7 = xmm7[0,0]
	mulpd	%xmm7, %xmm4
	addpd	%xmm0, %xmm4
	addsd	%xmm2, %xmm5
	movapd	.LCPI6_0(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm0, %xmm3
	xorpd	%xmm0, %xmm9
	movapd	%xmm4, (%rsp)
	movsd	%xmm5, 16(%rsp)
	movupd	%xmm3, 24(%rsp)
	movlpd	%xmm9, 40(%rsp)
	movq	(%r14), %rax
	movq	16(%rax), %rax
	movsd	%xmm1, 96(%rsp)
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 104(%rsp)
	movq	$0, 120(%rsp)
	leaq	128(%rsp), %rdi
	leaq	96(%rsp), %rdx
	movq	%rsp, %rcx
	movq	%r14, %rsi
	callq	*%rax
	movsd	128(%rsp), %xmm0        # xmm0 = mem[0],zero
	movapd	48(%rsp), %xmm1         # 16-byte Reload
	xorpd	.LCPI6_0(%rip), %xmm1
	cmpltsd	infinity(%rip), %xmm0
	andnpd	%xmm1, %xmm0
.LBB6_3:
	addq	$160, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	_Z9ray_traceRK3VecRK3RayRK5Scene, .Lfunc_end6-_Z9ray_traceRK3VecRK3RayRK5Scene
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI7_0:
	.quad	4613937818241073152     # double 3
.LCPI7_1:
	.quad	4614982882171571370     # double 3.4641016151377544
.LCPI7_2:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	_Z6createiRK3Vecd
	.p2align	4, 0x90
	.type	_Z6createiRK3Vecd,@function
_Z6createiRK3Vecd:                      # @_Z6createiRK3Vecd
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 208
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rsi, %r14
	movl	%edi, %r15d
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %r12
	movq	$_ZTV6Sphere+16, (%r12)
	movq	16(%r14), %rax
	movq	%rax, 24(%r12)
	movups	(%r14), %xmm0
	movups	%xmm0, 8(%r12)
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 32(%r12)
	cmpl	$1, %r15d
	je	.LBB7_24
# BB#1:
	leaq	8(%rsp), %rbp
	movq	%rbp, 8(%rsp)
	movq	%rbp, 16(%rsp)
	movq	$0, 24(%rsp)
.Ltmp0:
	movl	$24, %edi
	callq	_Znwm
.Ltmp1:
# BB#2:                                 # %.preheader
	movq	%r12, 16(%rax)
	leaq	8(%rsp), %rsi
	movq	%rax, %rdi
	callq	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	incq	24(%rsp)
	movsd	.LCPI7_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 104(%rsp)        # 8-byte Spill
	movapd	%xmm1, %xmm4
	divsd	.LCPI7_1(%rip), %xmm4
	decl	%r15d
	mulsd	.LCPI7_2(%rip), %xmm0
	movsd	(%r14), %xmm3           # xmm3 = mem[0],zero
	movsd	16(%r14), %xmm1         # xmm1 = mem[0],zero
	subsd	%xmm4, %xmm3
	movsd	8(%r14), %xmm2          # xmm2 = mem[0],zero
	addsd	%xmm4, %xmm2
	movapd	%xmm4, 80(%rsp)         # 16-byte Spill
	subsd	%xmm4, %xmm1
	movsd	%xmm3, 32(%rsp)
	movsd	%xmm2, 40(%rsp)
	movsd	%xmm1, 48(%rsp)
.Ltmp3:
	leaq	32(%rsp), %rsi
	movl	%r15d, %edi
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	callq	_Z6createiRK3Vecd
	movq	%rax, %rbx
.Ltmp4:
# BB#3:
.Ltmp5:
	movl	$24, %edi
	callq	_Znwm
.Ltmp6:
# BB#4:
	movq	%rbx, 16(%rax)
	leaq	8(%rsp), %rsi
	movq	%rax, %rdi
	callq	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	incq	24(%rsp)
	movupd	(%r14), %xmm0
	movaps	80(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movaps	%xmm1, 112(%rsp)        # 16-byte Spill
	addpd	%xmm1, %xmm0
	movsd	16(%r14), %xmm1         # xmm1 = mem[0],zero
	subsd	%xmm2, %xmm1
	movapd	%xmm0, 32(%rsp)
	movsd	%xmm1, 48(%rsp)
.Ltmp7:
	leaq	32(%rsp), %rsi
	movl	%r15d, %edi
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	_Z6createiRK3Vecd
	movq	%rax, %rbx
.Ltmp8:
# BB#5:
.Ltmp9:
	movl	$24, %edi
	callq	_Znwm
.Ltmp10:
# BB#6:                                 # %.preheader.181
	movq	%rbx, 16(%rax)
	leaq	8(%rsp), %rsi
	movq	%rax, %rdi
	callq	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	incq	24(%rsp)
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	subsd	80(%rsp), %xmm0         # 16-byte Folded Reload
	movupd	8(%r14), %xmm1
	addpd	112(%rsp), %xmm1        # 16-byte Folded Reload
	movsd	%xmm0, 32(%rsp)
	movupd	%xmm1, 40(%rsp)
.Ltmp11:
	leaq	32(%rsp), %rsi
	movl	%r15d, %edi
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	_Z6createiRK3Vecd
	movq	%rax, %rbx
.Ltmp12:
# BB#7:
.Ltmp13:
	movl	$24, %edi
	callq	_Znwm
.Ltmp14:
# BB#8:
	movq	%rbx, 16(%rax)
	leaq	8(%rsp), %rsi
	movq	%rax, %rdi
	callq	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	incq	24(%rsp)
	movupd	(%r14), %xmm0
	movapd	112(%rsp), %xmm1        # 16-byte Reload
	addpd	%xmm0, %xmm1
	movapd	80(%rsp), %xmm0         # 16-byte Reload
	addsd	16(%r14), %xmm0
	movapd	%xmm1, 32(%rsp)
	movsd	%xmm0, 48(%rsp)
.Ltmp15:
	leaq	32(%rsp), %rsi
	movl	%r15d, %edi
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	_Z6createiRK3Vecd
	movq	%rax, %rbx
.Ltmp16:
# BB#9:
.Ltmp17:
	movl	$24, %edi
	callq	_Znwm
.Ltmp18:
# BB#10:
	movq	%rbx, 16(%rax)
	leaq	8(%rsp), %rbx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	incq	24(%rsp)
.Ltmp20:
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp21:
# BB#11:
	movq	%r13, %r12
	movq	16(%r14), %rax
	movq	%rax, 144(%rsp)
	movupd	(%r14), %xmm0
	movapd	%xmm0, 128(%rsp)
	leaq	56(%rsp), %rcx
	movq	%rcx, 56(%rsp)
	movq	%rcx, 64(%rsp)
	movq	$0, 72(%rsp)
	movq	8(%rsp), %r15
	cmpq	%rbx, %r15
	movq	%rcx, %r14
	je	.LBB7_16
# BB#12:                                # %.lr.ph.i.i44
	leaq	56(%rsp), %r14
	.p2align	4, 0x90
.LBB7_13:                               # =>This Inner Loop Header: Depth=1
.Ltmp23:
	movl	$24, %edi
	callq	_Znwm
.Ltmp24:
# BB#14:                                # %.noexc.i
                                        #   in Loop: Header=BB7_13 Depth=1
	movq	16(%r15), %rcx
	movq	%rcx, 16(%rax)
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	incq	72(%rsp)
	movq	(%r15), %r15
	cmpq	%rbx, %r15
	jne	.LBB7_13
# BB#15:                                # %_ZNSt7__cxx114listIP5SceneSaIS2_EEC2ERKS4_.exit.loopexit
	movq	56(%rsp), %r14
	leaq	56(%rsp), %rcx
.LBB7_16:                               # %_ZNSt7__cxx114listIP5SceneSaIS2_EEC2ERKS4_.exit
	movl	$_ZTV6Sphere+16, %eax
	movd	%rax, %xmm0
	movl	$_ZTV5Group+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r12)
	movq	144(%rsp), %rax
	movq	%rax, 32(%r12)
	movaps	128(%rsp), %xmm0
	movups	%xmm0, 16(%r12)
	movsd	104(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 40(%r12)
	addq	$48, %r13
	movq	%r13, 48(%r12)
	movq	%r13, 56(%r12)
	movq	$0, 64(%r12)
	cmpq	%rcx, %r14
	je	.LBB7_22
# BB#17:                                # %.lr.ph.i.i.i45
	leaq	56(%rsp), %r15
	.p2align	4, 0x90
.LBB7_18:                               # =>This Inner Loop Header: Depth=1
.Ltmp26:
	movl	$24, %edi
	callq	_Znwm
.Ltmp27:
# BB#19:                                # %.noexc.i.i
                                        #   in Loop: Header=BB7_18 Depth=1
	movq	16(%r14), %rcx
	movq	%rcx, 16(%rax)
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	incq	64(%r12)
	movq	(%r14), %r14
	cmpq	%r15, %r14
	jne	.LBB7_18
# BB#20:                                # %_ZN5GroupC2E6SphereNSt7__cxx114listIP5SceneSaIS4_EEE.exit
	movq	56(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB7_22
	.p2align	4, 0x90
.LBB7_21:                               # %.lr.ph.i.i49
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	_ZdlPv
	cmpq	%r15, %rbx
	movq	%rbx, %rdi
	jne	.LBB7_21
.LBB7_22:                               # %_ZNSt7__cxx1110_List_baseIP5SceneSaIS2_EED2Ev.exit50
	movq	8(%rsp), %rdi
	leaq	8(%rsp), %rbp
	cmpq	%rbp, %rdi
	je	.LBB7_24
	.p2align	4, 0x90
.LBB7_23:                               # %.lr.ph.i.i41
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	_ZdlPv
	cmpq	%rbp, %rbx
	movq	%rbx, %rdi
	jne	.LBB7_23
.LBB7_24:
	movq	%r12, %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_33:
.Ltmp22:
	jmp	.LBB7_34
.LBB7_38:
.Ltmp2:
	jmp	.LBB7_34
.LBB7_39:
.Ltmp19:
.LBB7_34:
	movq	%rax, %r15
	jmp	.LBB7_35
.LBB7_27:
.Ltmp28:
	movq	%rax, %r14
	movq	(%r13), %rdi
	cmpq	%r13, %rdi
	je	.LBB7_29
	.p2align	4, 0x90
.LBB7_28:                               # %.lr.ph.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	_ZdlPv
	cmpq	%r13, %rbx
	movq	%rbx, %rdi
	jne	.LBB7_28
.LBB7_29:                               # %.body46
	movq	56(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB7_31
	.p2align	4, 0x90
.LBB7_30:                               # %.lr.ph.i.i38
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	_ZdlPv
	cmpq	%r15, %rbx
	movq	%rbx, %rdi
	jne	.LBB7_30
.LBB7_31:
	movq	%r14, %r15
	jmp	.LBB7_32
.LBB7_25:
.Ltmp25:
	movq	%rax, %r15
	movq	56(%rsp), %rdi
	cmpq	%r14, %rdi
	je	.LBB7_32
	.p2align	4, 0x90
.LBB7_26:                               # %.lr.ph.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	_ZdlPv
	cmpq	%r14, %rbx
	movq	%rbx, %rdi
	jne	.LBB7_26
.LBB7_32:                               # %_ZNSt7__cxx1110_List_baseIP5SceneSaIS2_EED2Ev.exit39
	movq	%r12, %rdi
	callq	_ZdlPv
.LBB7_35:
	movq	8(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.LBB7_37
	.p2align	4, 0x90
.LBB7_36:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	_ZdlPv
	cmpq	%rbp, %rbx
	movq	%rbx, %rdi
	jne	.LBB7_36
.LBB7_37:                               # %_ZNSt7__cxx1110_List_baseIP5SceneSaIS2_EED2Ev.exit
	movq	%r15, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_Z6createiRK3Vecd, .Lfunc_end7-_Z6createiRK3Vecd
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp18-.Ltmp3          #   Call between .Ltmp3 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin0   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Lfunc_end7-.Ltmp27     #   Call between .Ltmp27 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.quad	-4624885866418920406    # double -0.2672612419124244
	.quad	-4617974991532575169    # double -0.80178372573727319
.LCPI8_11:
	.zero	16
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI8_1:
	.quad	-4616189618054758400    # double -1
.LCPI8_2:
	.quad	4607182418800017408     # double 1
.LCPI8_3:
	.quad	-4580160821035794432    # double -256
.LCPI8_4:
	.quad	4598175219545276416     # double 0.25
.LCPI8_5:
	.quad	4602678819172646912     # double 0.5
.LCPI8_6:
	.quad	4604930618986332160     # double 0.75
.LCPI8_7:
	.quad	4688247212092686336     # double 262144
.LCPI8_8:
	.quad	4647714815446351872     # double 512
.LCPI8_9:
	.quad	4643176031446892544     # double 255
.LCPI8_10:
	.quad	4589168020290535424     # double 0.0625
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi34:
	.cfi_def_cfa_offset 320
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movl	%edi, %eax
	movl	$6, %edi
	cmpl	$2, %eax
	jne	.LBB8_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rdi
.LBB8_2:
	movaps	.LCPI8_0(%rip), %xmm0   # xmm0 = [-2.672612e-01,-8.017837e-01]
	movaps	%xmm0, 240(%rsp)
	movabsq	$4602989770063225898, %rax # imm = 0x3FE11ACEE560242A
	movq	%rax, 256(%rsp)
	movq	.LCPI8_1(%rip), %xmm0   # xmm0 = mem[0],zero
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqa	%xmm0, 16(%rsp)
	movq	$0, 32(%rsp)
	leaq	16(%rsp), %r14
	movsd	.LCPI8_2(%rip), %xmm0   # xmm0 = mem[0],zero
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r14, %rsi
	callq	_Z6createiRK3Vecd
	movq	%rax, %rbx
	movl	$_ZSt4cout, %edi
	movl	$.L.str, %esi
	movl	$3, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$_ZSt4cout, %edi
	movl	$512, %esi              # imm = 0x200
	callq	_ZNSolsEi
	movq	%rax, %r15
	movl	$.L.str.3, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$512, %esi              # imm = 0x200
	movq	%r15, %rdi
	callq	_ZNSolsEi
	movl	$.L.str.4, %esi
	movl	$5, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movsd	.LCPI8_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$511, %eax              # imm = 0x1FF
	movabsq	$-4607182418800017408, %r13 # imm = 0xC010000000000000
	leaq	240(%rsp), %r15
	.p2align	4, 0x90
.LBB8_3:                                # %.preheader85
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_4 Depth 2
                                        #       Child Loop BB8_5 Depth 3
	movq	%rax, 96(%rsp)          # 8-byte Spill
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movapd	%xmm1, %xmm3
	movsd	.LCPI8_3(%rip), %xmm0   # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm3
	movapd	%xmm3, 208(%rsp)        # 16-byte Spill
	mulsd	%xmm3, %xmm3
	movapd	%xmm3, 192(%rsp)        # 16-byte Spill
	movapd	%xmm1, %xmm3
	addsd	.LCPI8_4(%rip), %xmm3
	addsd	%xmm0, %xmm3
	movapd	%xmm3, 176(%rsp)        # 16-byte Spill
	mulsd	%xmm3, %xmm3
	movapd	%xmm3, 160(%rsp)        # 16-byte Spill
	movapd	%xmm1, %xmm3
	addsd	.LCPI8_5(%rip), %xmm3
	addsd	%xmm0, %xmm3
	movapd	%xmm3, 144(%rsp)        # 16-byte Spill
	mulsd	%xmm3, %xmm3
	movapd	%xmm3, 128(%rsp)        # 16-byte Spill
	addsd	.LCPI8_6(%rip), %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm1, 224(%rsp)        # 16-byte Spill
	mulsd	%xmm1, %xmm1
	movapd	%xmm1, 112(%rsp)        # 16-byte Spill
	xorl	%ebp, %ebp
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB8_4:                                # %.preheader84
                                        #   Parent Loop BB8_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_5 Depth 3
	movl	$4, %r12d
	xorpd	%xmm1, %xmm1
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB8_5:                                # %.preheader
                                        #   Parent Loop BB8_3 Depth=1
                                        #     Parent Loop BB8_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	%xmm1, 104(%rsp)        # 8-byte Spill
	movapd	%xmm1, %xmm5
	mulsd	.LCPI8_4(%rip), %xmm5
	addsd	8(%rsp), %xmm5          # 8-byte Folded Reload
	addsd	.LCPI8_3(%rip), %xmm5
	movapd	%xmm5, %xmm0
	mulsd	%xmm0, %xmm0
	movapd	%xmm0, 80(%rsp)         # 16-byte Spill
	addsd	192(%rsp), %xmm0        # 16-byte Folded Reload
	movsd	.LCPI8_7(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	movsd	.LCPI8_8(%rip), %xmm3   # xmm3 = mem[0],zero
	xorps	%xmm4, %xmm4
	movapd	%xmm5, 64(%rsp)         # 16-byte Spill
	jnp	.LBB8_7
# BB#6:                                 # %call.sqrt
                                        #   in Loop: Header=BB8_5 Depth=3
	callq	sqrt
	movapd	64(%rsp), %xmm5         # 16-byte Reload
	xorps	%xmm4, %xmm4
	movsd	.LCPI8_8(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI8_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB8_7:                                # %.preheader.split
                                        #   in Loop: Header=BB8_5 Depth=3
	movapd	%xmm2, %xmm0
	divsd	%xmm1, %xmm0
	movapd	%xmm5, %xmm1
	unpcklpd	208(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0]
	movapd	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm1, %xmm2
	mulsd	%xmm3, %xmm0
	movaps	%xmm4, 16(%rsp)
	movq	%r13, 32(%rsp)
	movupd	%xmm2, 40(%rsp)
	movsd	%xmm0, 56(%rsp)
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	_Z9ray_traceRK3VecRK3RayRK5Scene
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movapd	80(%rsp), %xmm0         # 16-byte Reload
	addsd	160(%rsp), %xmm0        # 16-byte Folded Reload
	addsd	.LCPI8_7(%rip), %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB8_9
# BB#8:                                 # %call.sqrt95
                                        #   in Loop: Header=BB8_5 Depth=3
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB8_9:                                # %.preheader.split.split
                                        #   in Loop: Header=BB8_5 Depth=3
	movsd	.LCPI8_2(%rip), %xmm0   # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	movapd	64(%rsp), %xmm1         # 16-byte Reload
	unpcklpd	176(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0]
	movapd	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm1, %xmm2
	mulsd	.LCPI8_8(%rip), %xmm0
	xorpd	%xmm1, %xmm1
	movapd	%xmm1, 16(%rsp)
	movq	%r13, 32(%rsp)
	movupd	%xmm2, 40(%rsp)
	movsd	%xmm0, 56(%rsp)
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	_Z9ray_traceRK3VecRK3RayRK5Scene
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movapd	80(%rsp), %xmm0         # 16-byte Reload
	addsd	128(%rsp), %xmm0        # 16-byte Folded Reload
	addsd	.LCPI8_7(%rip), %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB8_11
# BB#10:                                # %call.sqrt96
                                        #   in Loop: Header=BB8_5 Depth=3
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB8_11:                               # %.preheader.split.split.split
                                        #   in Loop: Header=BB8_5 Depth=3
	movsd	.LCPI8_2(%rip), %xmm0   # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	movapd	64(%rsp), %xmm1         # 16-byte Reload
	unpcklpd	144(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0]
	movapd	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm1, %xmm2
	mulsd	.LCPI8_8(%rip), %xmm0
	xorpd	%xmm1, %xmm1
	movapd	%xmm1, 16(%rsp)
	movq	%r13, 32(%rsp)
	movupd	%xmm2, 40(%rsp)
	movsd	%xmm0, 56(%rsp)
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	_Z9ray_traceRK3VecRK3RayRK5Scene
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movapd	80(%rsp), %xmm1         # 16-byte Reload
	addsd	112(%rsp), %xmm1        # 16-byte Folded Reload
	addsd	.LCPI8_7(%rip), %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB8_13
# BB#12:                                # %call.sqrt97
                                        #   in Loop: Header=BB8_5 Depth=3
	movapd	%xmm1, %xmm0
	callq	sqrt
.LBB8_13:                               # %.preheader.split.split.split.split
                                        #   in Loop: Header=BB8_5 Depth=3
	movsd	.LCPI8_2(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movapd	64(%rsp), %xmm2         # 16-byte Reload
	unpcklpd	224(%rsp), %xmm2 # 16-byte Folded Reload
                                        # xmm2 = xmm2[0],mem[0]
	movapd	%xmm1, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	%xmm2, %xmm0
	mulsd	.LCPI8_8(%rip), %xmm1
	xorpd	%xmm2, %xmm2
	movapd	%xmm2, 16(%rsp)
	movq	%r13, 32(%rsp)
	movupd	%xmm0, 40(%rsp)
	movsd	%xmm1, 56(%rsp)
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	_Z9ray_traceRK3VecRK3RayRK5Scene
	movsd	(%rsp), %xmm3           # 8-byte Reload
                                        # xmm3 = mem[0],zero
	addsd	%xmm0, %xmm3
	movsd	104(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	.LCPI8_2(%rip), %xmm0   # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm1
	decl	%r12d
	movapd	%xmm0, %xmm2
	movapd	%xmm3, %xmm0
	jne	.LBB8_5
# BB#14:                                #   in Loop: Header=BB8_4 Depth=2
	mulsd	.LCPI8_9(%rip), %xmm0
	mulsd	.LCPI8_10(%rip), %xmm0
	addsd	.LCPI8_5(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	movb	%al, 16(%rsp)
	movl	$_ZSt4cout, %edi
	movl	$1, %edx
	movq	%r14, %rsi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movsd	.LCPI8_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm2, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	incl	%ebp
	cmpl	$512, %ebp              # imm = 0x200
	jne	.LBB8_4
# BB#15:                                #   in Loop: Header=BB8_3 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	leal	-1(%rax), %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	jg	.LBB8_3
# BB#16:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	main, .Lfunc_end8-main
	.cfi_endproc

	.section	.text._ZN6SphereD0Ev,"axG",@progbits,_ZN6SphereD0Ev,comdat
	.weak	_ZN6SphereD0Ev
	.p2align	4, 0x90
	.type	_ZN6SphereD0Ev,@function
_ZN6SphereD0Ev:                         # @_ZN6SphereD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end9:
	.size	_ZN6SphereD0Ev, .Lfunc_end9-_ZN6SphereD0Ev
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI10_0:
	.quad	4607182418800017408     # double 1
	.section	.text._ZNK6Sphere9intersectERKSt4pairId3VecERK3Ray,"axG",@progbits,_ZNK6Sphere9intersectERKSt4pairId3VecERK3Ray,comdat
	.weak	_ZNK6Sphere9intersectERKSt4pairId3VecERK3Ray
	.p2align	4, 0x90
	.type	_ZNK6Sphere9intersectERKSt4pairId3VecERK3Ray,@function
_ZNK6Sphere9intersectERKSt4pairId3VecERK3Ray: # @_ZNK6Sphere9intersectERKSt4pairId3VecERK3Ray
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 40
	subq	$56, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 96
.Lcfi46:
	.cfi_offset %rbx, -40
.Lcfi47:
	.cfi_offset %r12, -32
.Lcfi48:
	.cfi_offset %r14, -24
.Lcfi49:
	.cfi_offset %r15, -16
	movq	%rcx, %rbx
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rdi, %r14
	movsd	8(%r15), %xmm0          # xmm0 = mem[0],zero
	movsd	16(%r15), %xmm1         # xmm1 = mem[0],zero
	subsd	(%rbx), %xmm0
	subsd	8(%rbx), %xmm1
	movsd	24(%r15), %xmm2         # xmm2 = mem[0],zero
	subsd	16(%rbx), %xmm2
	movsd	24(%rbx), %xmm3         # xmm3 = mem[0],zero
	mulsd	%xmm0, %xmm3
	movsd	32(%rbx), %xmm4         # xmm4 = mem[0],zero
	mulsd	%xmm1, %xmm4
	addsd	%xmm3, %xmm4
	movsd	40(%rbx), %xmm5         # xmm5 = mem[0],zero
	mulsd	%xmm2, %xmm5
	addsd	%xmm4, %xmm5
	movapd	%xmm5, %xmm3
	mulsd	%xmm3, %xmm3
	mulsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm2
	addsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm3
	movsd	32(%r15), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	addsd	%xmm3, %xmm1
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm1, %xmm0
	ja	.LBB10_1
# BB#2:
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB10_4
# BB#3:                                 # %call.sqrt
	movapd	%xmm1, %xmm0
	movapd	%xmm5, (%rsp)           # 16-byte Spill
	callq	sqrt
	movapd	(%rsp), %xmm5           # 16-byte Reload
.LBB10_4:                               # %.split
	movapd	%xmm5, %xmm1
	addsd	%xmm0, %xmm1
	xorpd	%xmm3, %xmm3
	ucomisd	%xmm1, %xmm3
	jbe	.LBB10_6
.LBB10_1:
	movsd	infinity(%rip), %xmm3   # xmm3 = mem[0],zero
	ucomisd	(%r12), %xmm3
	jb	.LBB10_7
	jmp	.LBB10_10
.LBB10_6:
	subsd	%xmm0, %xmm5
	cmpltsd	%xmm5, %xmm3
	andpd	%xmm3, %xmm5
	andnpd	%xmm1, %xmm3
	orpd	%xmm5, %xmm3
	ucomisd	(%r12), %xmm3
	jae	.LBB10_10
.LBB10_7:
	movupd	(%rbx), %xmm0
	movupd	24(%rbx), %xmm1
	movapd	%xmm3, %xmm4
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	mulpd	%xmm1, %xmm4
	movsd	40(%rbx), %xmm5         # xmm5 = mem[0],zero
	mulsd	%xmm3, %xmm5
	addpd	%xmm0, %xmm4
	addsd	16(%rbx), %xmm5
	movupd	8(%r15), %xmm0
	subpd	%xmm0, %xmm4
	subsd	24(%r15), %xmm5
	movapd	%xmm4, %xmm0
	mulsd	%xmm0, %xmm0
	movapd	%xmm4, %xmm2
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	mulsd	%xmm2, %xmm2
	addsd	%xmm0, %xmm2
	movapd	%xmm5, %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB10_9
# BB#8:                                 # %call.sqrt33
	movapd	%xmm1, %xmm0
	movapd	%xmm3, (%rsp)           # 16-byte Spill
	movapd	%xmm4, 32(%rsp)         # 16-byte Spill
	movsd	%xmm5, 24(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	24(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movapd	32(%rsp), %xmm4         # 16-byte Reload
	movapd	(%rsp), %xmm3           # 16-byte Reload
.LBB10_9:
	movsd	.LCPI10_0(%rip), %xmm1  # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	mulsd	%xmm1, %xmm5
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm1, %xmm4
	movsd	%xmm3, (%r14)
	movupd	%xmm4, 8(%r14)
	movsd	%xmm5, 24(%r14)
	jmp	.LBB10_11
.LBB10_10:
	movupd	(%r12), %xmm0
	movups	16(%r12), %xmm1
	movups	%xmm1, 16(%r14)
	movupd	%xmm0, (%r14)
.LBB10_11:
	movq	%r14, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	_ZNK6Sphere9intersectERKSt4pairId3VecERK3Ray, .Lfunc_end10-_ZNK6Sphere9intersectERKSt4pairId3VecERK3Ray
	.cfi_endproc

	.section	.text._ZN5SceneD2Ev,"axG",@progbits,_ZN5SceneD2Ev,comdat
	.weak	_ZN5SceneD2Ev
	.p2align	4, 0x90
	.type	_ZN5SceneD2Ev,@function
_ZN5SceneD2Ev:                          # @_ZN5SceneD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end11:
	.size	_ZN5SceneD2Ev, .Lfunc_end11-_ZN5SceneD2Ev
	.cfi_endproc

	.section	.text._ZN5GroupD2Ev,"axG",@progbits,_ZN5GroupD2Ev,comdat
	.weak	_ZN5GroupD2Ev
	.p2align	4, 0x90
	.type	_ZN5GroupD2Ev,@function
_ZN5GroupD2Ev:                          # @_ZN5GroupD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 32
.Lcfi53:
	.cfi_offset %rbx, -32
.Lcfi54:
	.cfi_offset %r14, -24
.Lcfi55:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	$_ZTV5Group+16, (%r15)
	movq	48(%r15), %r14
	addq	$48, %r15
	cmpq	%r15, %r14
	je	.LBB12_6
	.p2align	4, 0x90
.LBB12_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB12_3
# BB#2:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	(%rdi), %rax
.Ltmp29:
	callq	*8(%rax)
.Ltmp30:
.LBB12_3:                               #   in Loop: Header=BB12_1 Depth=1
	movq	(%r14), %r14
	cmpq	%r15, %r14
	jne	.LBB12_1
# BB#4:                                 # %._crit_edge
	movq	(%r15), %rdi
	cmpq	%r15, %rdi
	je	.LBB12_6
	.p2align	4, 0x90
.LBB12_5:                               # %.lr.ph.i.i7
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %r14
	callq	_ZdlPv
	cmpq	%r15, %r14
	movq	%r14, %rdi
	jne	.LBB12_5
.LBB12_6:                               # %_ZNSt7__cxx1110_List_baseIP5SceneSaIS2_EED2Ev.exit8
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB12_7:
.Ltmp31:
	movq	%rax, %r14
	movq	(%r15), %rdi
	cmpq	%r15, %rdi
	je	.LBB12_9
	.p2align	4, 0x90
.LBB12_8:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	_ZdlPv
	cmpq	%r15, %rbx
	movq	%rbx, %rdi
	jne	.LBB12_8
.LBB12_9:                               # %_ZNSt7__cxx1110_List_baseIP5SceneSaIS2_EED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN5GroupD2Ev, .Lfunc_end12-_ZN5GroupD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp29-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin1   #     jumps to .Ltmp31
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end12-.Ltmp30    #   Call between .Ltmp30 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN5GroupD0Ev,"axG",@progbits,_ZN5GroupD0Ev,comdat
	.weak	_ZN5GroupD0Ev
	.p2align	4, 0x90
	.type	_ZN5GroupD0Ev,@function
_ZN5GroupD0Ev:                          # @_ZN5GroupD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 48
.Lcfi61:
	.cfi_offset %rbx, -40
.Lcfi62:
	.cfi_offset %r12, -32
.Lcfi63:
	.cfi_offset %r14, -24
.Lcfi64:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	$_ZTV5Group+16, (%r15)
	leaq	48(%r15), %r12
	movq	48(%r15), %r14
	cmpq	%r12, %r14
	je	.LBB13_6
	.p2align	4, 0x90
.LBB13_1:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB13_3
# BB#2:                                 #   in Loop: Header=BB13_1 Depth=1
	movq	(%rdi), %rax
.Ltmp32:
	callq	*8(%rax)
.Ltmp33:
.LBB13_3:                               #   in Loop: Header=BB13_1 Depth=1
	movq	(%r14), %r14
	cmpq	%r12, %r14
	jne	.LBB13_1
# BB#4:                                 # %._crit_edge.i
	movq	(%r12), %rdi
	cmpq	%r12, %rdi
	je	.LBB13_6
	.p2align	4, 0x90
.LBB13_5:                               # %.lr.ph.i.i7.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %r14
	callq	_ZdlPv
	cmpq	%r12, %r14
	movq	%r14, %rdi
	jne	.LBB13_5
.LBB13_6:                               # %_ZN5GroupD2Ev.exit
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZdlPv                  # TAILCALL
.LBB13_7:
.Ltmp34:
	movq	%rax, %r14
	movq	(%r12), %rdi
	cmpq	%r12, %rdi
	je	.LBB13_9
	.p2align	4, 0x90
.LBB13_8:                               # %.lr.ph.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	_ZdlPv
	cmpq	%r12, %rbx
	movq	%rbx, %rdi
	jne	.LBB13_8
.LBB13_9:                               # %.body
	movq	%r15, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end13:
	.size	_ZN5GroupD0Ev, .Lfunc_end13-_ZN5GroupD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp32-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin2   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end13-.Ltmp33    #   Call between .Ltmp33 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNK5Group9intersectERKSt4pairId3VecERK3Ray,"axG",@progbits,_ZNK5Group9intersectERKSt4pairId3VecERK3Ray,comdat
	.weak	_ZNK5Group9intersectERKSt4pairId3VecERK3Ray
	.p2align	4, 0x90
	.type	_ZNK5Group9intersectERKSt4pairId3VecERK3Ray,@function
_ZNK5Group9intersectERKSt4pairId3VecERK3Ray: # @_ZNK5Group9intersectERKSt4pairId3VecERK3Ray
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi71:
	.cfi_def_cfa_offset 144
.Lcfi72:
	.cfi_offset %rbx, -56
.Lcfi73:
	.cfi_offset %r12, -48
.Lcfi74:
	.cfi_offset %r13, -40
.Lcfi75:
	.cfi_offset %r14, -32
.Lcfi76:
	.cfi_offset %r15, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %r14
	movups	(%r12), %xmm0
	movups	16(%r12), %xmm1
	movaps	%xmm1, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movsd	16(%r13), %xmm0         # xmm0 = mem[0],zero
	movsd	24(%r13), %xmm1         # xmm1 = mem[0],zero
	subsd	(%r15), %xmm0
	subsd	8(%r15), %xmm1
	movsd	32(%r13), %xmm2         # xmm2 = mem[0],zero
	subsd	16(%r15), %xmm2
	movsd	24(%r15), %xmm3         # xmm3 = mem[0],zero
	mulsd	%xmm0, %xmm3
	movsd	32(%r15), %xmm4         # xmm4 = mem[0],zero
	mulsd	%xmm1, %xmm4
	addsd	%xmm3, %xmm4
	movsd	40(%r15), %xmm5         # xmm5 = mem[0],zero
	mulsd	%xmm2, %xmm5
	addsd	%xmm4, %xmm5
	movapd	%xmm5, %xmm3
	mulsd	%xmm3, %xmm3
	mulsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm2
	addsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm3
	movsd	40(%r13), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	addsd	%xmm3, %xmm1
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm1, %xmm0
	ja	.LBB14_1
# BB#2:
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB14_4
# BB#3:                                 # %call.sqrt
	movapd	%xmm1, %xmm0
	movapd	%xmm5, 32(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	32(%rsp), %xmm5         # 16-byte Reload
.LBB14_4:                               # %.split
	movapd	%xmm5, %xmm2
	addsd	%xmm0, %xmm2
	xorpd	%xmm1, %xmm1
	ucomisd	%xmm2, %xmm1
	jbe	.LBB14_6
.LBB14_1:
	movsd	infinity(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	(%r12), %xmm1
	jb	.LBB14_7
	jmp	.LBB14_11
.LBB14_6:
	subsd	%xmm0, %xmm5
	cmpltsd	%xmm5, %xmm1
	andpd	%xmm1, %xmm5
	andnpd	%xmm2, %xmm1
	orpd	%xmm5, %xmm1
	ucomisd	(%r12), %xmm1
	jae	.LBB14_11
.LBB14_7:
	movq	48(%r13), %rbp
	addq	$48, %r13
	cmpq	%r13, %rbp
	je	.LBB14_10
# BB#8:
	leaq	56(%rsp), %r12
	movq	%rsp, %rbx
	.p2align	4, 0x90
.LBB14_9:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rsi
	movq	(%rsi), %rax
	movq	%r12, %rdi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	callq	*16(%rax)
	movupd	56(%rsp), %xmm0
	movupd	72(%rsp), %xmm1
	movapd	%xmm1, 16(%rsp)
	movapd	%xmm0, (%rsp)
	movq	(%rbp), %rbp
	cmpq	%r13, %rbp
	jne	.LBB14_9
.LBB14_10:                              # %._crit_edge
	movaps	(%rsp), %xmm0
	movaps	16(%rsp), %xmm1
	jmp	.LBB14_12
.LBB14_11:
	movups	(%r12), %xmm0
	movups	16(%r12), %xmm1
.LBB14_12:
	movups	%xmm1, 16(%r14)
	movups	%xmm0, (%r14)
	movq	%r14, %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	_ZNK5Group9intersectERKSt4pairId3VecERK3Ray, .Lfunc_end14-_ZNK5Group9intersectERKSt4pairId3VecERK3Ray
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_ray.ii,@function
_GLOBAL__sub_I_ray.ii:                  # @_GLOBAL__sub_I_ray.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movabsq	$4490088828488384512, %rax # imm = 0x3E50000000000000
	movq	%rax, delta(%rip)
	movabsq	$9218868437227405312, %rax # imm = 0x7FF0000000000000
	movq	%rax, infinity(%rip)
	popq	%rax
	retq
.Lfunc_end15:
	.size	_GLOBAL__sub_I_ray.ii, .Lfunc_end15-_GLOBAL__sub_I_ray.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	real,@object            # @real
	.bss
	.globl	real
real:
	.zero	1
	.size	real, 1

	.type	delta,@object           # @delta
	.globl	delta
	.p2align	3
delta:
	.quad	0                       # double 0
	.size	delta, 8

	.type	infinity,@object        # @infinity
	.globl	infinity
	.p2align	3
infinity:
	.quad	0                       # double 0
	.size	infinity, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"P5\n"
	.size	.L.str, 4

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" "
	.size	.L.str.3, 2

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n255\n"
	.size	.L.str.4, 6

	.type	_ZTV6Sphere,@object     # @_ZTV6Sphere
	.section	.rodata._ZTV6Sphere,"aG",@progbits,_ZTV6Sphere,comdat
	.weak	_ZTV6Sphere
	.p2align	3
_ZTV6Sphere:
	.quad	0
	.quad	_ZTI6Sphere
	.quad	_ZN5SceneD2Ev
	.quad	_ZN6SphereD0Ev
	.quad	_ZNK6Sphere9intersectERKSt4pairId3VecERK3Ray
	.size	_ZTV6Sphere, 40

	.type	_ZTS6Sphere,@object     # @_ZTS6Sphere
	.section	.rodata._ZTS6Sphere,"aG",@progbits,_ZTS6Sphere,comdat
	.weak	_ZTS6Sphere
_ZTS6Sphere:
	.asciz	"6Sphere"
	.size	_ZTS6Sphere, 8

	.type	_ZTS5Scene,@object      # @_ZTS5Scene
	.section	.rodata._ZTS5Scene,"aG",@progbits,_ZTS5Scene,comdat
	.weak	_ZTS5Scene
_ZTS5Scene:
	.asciz	"5Scene"
	.size	_ZTS5Scene, 7

	.type	_ZTI5Scene,@object      # @_ZTI5Scene
	.section	.rodata._ZTI5Scene,"aG",@progbits,_ZTI5Scene,comdat
	.weak	_ZTI5Scene
	.p2align	3
_ZTI5Scene:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS5Scene
	.size	_ZTI5Scene, 16

	.type	_ZTI6Sphere,@object     # @_ZTI6Sphere
	.section	.rodata._ZTI6Sphere,"aG",@progbits,_ZTI6Sphere,comdat
	.weak	_ZTI6Sphere
	.p2align	4
_ZTI6Sphere:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS6Sphere
	.quad	_ZTI5Scene
	.size	_ZTI6Sphere, 24

	.type	_ZTV5Group,@object      # @_ZTV5Group
	.section	.rodata._ZTV5Group,"aG",@progbits,_ZTV5Group,comdat
	.weak	_ZTV5Group
	.p2align	3
_ZTV5Group:
	.quad	0
	.quad	_ZTI5Group
	.quad	_ZN5GroupD2Ev
	.quad	_ZN5GroupD0Ev
	.quad	_ZNK5Group9intersectERKSt4pairId3VecERK3Ray
	.size	_ZTV5Group, 40

	.type	_ZTS5Group,@object      # @_ZTS5Group
	.section	.rodata._ZTS5Group,"aG",@progbits,_ZTS5Group,comdat
	.weak	_ZTS5Group
_ZTS5Group:
	.asciz	"5Group"
	.size	_ZTS5Group, 7

	.type	_ZTI5Group,@object      # @_ZTI5Group
	.section	.rodata._ZTI5Group,"aG",@progbits,_ZTI5Group,comdat
	.weak	_ZTI5Group
	.p2align	4
_ZTI5Group:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS5Group
	.quad	_ZTI5Scene
	.size	_ZTI5Group, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_ray.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
