	.text
	.file	"arg.bc"
	.globl	process_arg
	.p2align	4, 0x90
	.type	process_arg,@function
process_arg:                            # @process_arg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rdi, %r15
	movq	24(%r15), %rbx
	movslq	%esi, %rax
	imulq	$56, %rax, %rbp
	movq	24(%rbx,%rbp), %rax
	testq	%rax, %rax
	je	.LBB0_1
# BB#2:
	movsbl	(%rax), %eax
	cmpl	$83, %eax
	jg	.LBB0_9
# BB#3:
	cmpb	$43, %al
	je	.LBB0_14
# BB#4:
	cmpb	$70, %al
	je	.LBB0_11
	jmp	.LBB0_5
.LBB0_1:
	xorl	%r13d, %r13d
	jmp	.LBB0_23
.LBB0_9:
	cmpb	$84, %al
	je	.LBB0_13
# BB#10:
	cmpb	$102, %al
	jne	.LBB0_5
.LBB0_11:
	xorl	%ecx, %ecx
	cmpb	$70, %al
	sete	%cl
	movq	32(%rbx,%rbp), %rax
	jmp	.LBB0_12
.LBB0_5:
	movq	(%r14), %rcx
	movq	(%rcx), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rcx)
	movq	(%r14), %rcx
	cmpb	$0, 1(%rdx)
	jne	.LBB0_7
# BB#6:
	addq	$8, %rcx
	movq	%rcx, (%r14)
.LBB0_7:
	movq	(%rcx), %r13
	testq	%r13, %r13
	je	.LBB0_8
# BB#15:
	addl	$-68, %eax
	cmpl	$15, %eax
	ja	.LBB0_20
# BB#16:
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_17:
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	strtod
	movq	32(%rbx,%rbp), %rax
	movsd	%xmm0, (%rax)
	jmp	.LBB0_22
.LBB0_14:
	movq	32(%rbx,%rbp), %rax
	incl	(%rax)
	xorl	%r13d, %r13d
	jmp	.LBB0_23
.LBB0_13:
	movq	32(%rbx,%rbp), %rax
	xorl	%ecx, %ecx
	cmpl	$0, (%rax)
	sete	%cl
.LBB0_12:
	movl	%ecx, (%rax)
	xorl	%r13d, %r13d
	jmp	.LBB0_23
.LBB0_18:
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	32(%rbx,%rbp), %rcx
	movq	%rax, (%rcx)
	jmp	.LBB0_22
.LBB0_19:
	leaq	24(%rbx,%rbp), %rax
	movq	32(%rbx,%rbp), %r12
	movq	(%rax), %rdi
	incq	%rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movslq	%eax, %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	strncpy
	jmp	.LBB0_22
.LBB0_21:
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	32(%rbx,%rbp), %rcx
	movl	%eax, (%rcx)
.LBB0_22:
	movq	(%r14), %r12
	movq	(%r12), %r14
	movq	%r14, %rdi
	callq	strlen
	leaq	-1(%r14,%rax), %rax
	movq	%rax, (%r12)
.LBB0_23:
	movq	48(%rbx,%rbp), %rax
	testq	%rax, %rax
	je	.LBB0_24
# BB#25:
	movq	%r15, %rdi
	movq	%r13, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.LBB0_24:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_8:
	movq	%r15, %rdi
	callq	usage
.LBB0_20:
	movq	stderr(%rip), %rdi
	movq	16(%r15), %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	process_arg, .Lfunc_end0-process_arg
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_17
	.quad	.LBB0_20
	.quad	.LBB0_20
	.quad	.LBB0_20
	.quad	.LBB0_20
	.quad	.LBB0_21
	.quad	.LBB0_20
	.quad	.LBB0_20
	.quad	.LBB0_18
	.quad	.LBB0_20
	.quad	.LBB0_20
	.quad	.LBB0_20
	.quad	.LBB0_20
	.quad	.LBB0_20
	.quad	.LBB0_20
	.quad	.LBB0_19

	.text
	.globl	usage
	.p2align	4, 0x90
	.type	usage,@function
usage:                                  # @usage
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rbp
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB1_24
# BB#1:                                 # %.lr.ph.preheader
	addq	$8, %rbp
	jmp	.LBB1_2
.LBB1_3:                                #   in Loop: Header=BB1_2 Depth=1
	movq	stderr(%rip), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movsbl	(%rbp), %r13d
	cmpl	$32, %r13d
	movl	$45, %r15d
	cmovel	%r13d, %r15d
	movl	$44, %r12d
	cmovel	%r13d, %r12d
	movq	%rbx, %rdi
	callq	strlen
	leaq	61(%rax), %rcx
	cmpq	$81, %rcx
	leaq	.L.str.14+61(%rax), %r14
	movl	$.L.str.3, %eax
	cmovaeq	%rax, %r14
	movq	16(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB1_5
# BB#4:                                 #   in Loop: Header=BB1_2 Depth=1
	movl	$8, %r10d
	jmp	.LBB1_6
.LBB1_5:                                #   in Loop: Header=BB1_2 Depth=1
	movsbl	(%rax), %esi
	movl	$.L.str.24, %edi
	movl	$9, %edx
	callq	memchr
	movq	%rax, %r10
	movl	$.L.str.24, %eax
	subq	%rax, %r10
.LBB1_6:                                #   in Loop: Header=BB1_2 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	$.L.str.2, %esi
	movl	$0, %eax
	movl	%r15d, %edx
	movl	%r13d, %ecx
	movl	%r12d, %r8d
	movq	%rbx, %r9
	pushq	arg_types_desc(,%r10,8)
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$16, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset -16
	movq	16(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB1_7
.LBB1_9:                                # %.thread
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
.LBB1_10:                               #   in Loop: Header=BB1_2 Depth=1
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB1_22
.LBB1_7:                                #   in Loop: Header=BB1_2 Depth=1
	movsbl	(%rax), %eax
	leal	-43(%rax), %ecx
	cmpl	$59, %ecx
	ja	.LBB1_8
# BB#25:                                #   in Loop: Header=BB1_2 Depth=1
	jmpq	*.LJTI1_0(,%rcx,8)
.LBB1_19:                               #   in Loop: Header=BB1_2 Depth=1
	movq	stderr(%rip), %rdi
	movq	24(%rbp), %rax
	cmpl	$0, (%rax)
	movl	$.L.str.12, %edx
	movl	$.L.str.11, %eax
	cmovneq	%rax, %rdx
	movl	$.L.str.6, %esi
	jmp	.LBB1_20
.LBB1_18:                               #   in Loop: Header=BB1_2 Depth=1
	movq	stderr(%rip), %rdi
	movq	24(%rbp), %rax
	movl	(%rax), %edx
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB1_22
.LBB1_8:                                #   in Loop: Header=BB1_2 Depth=1
	testl	%eax, %eax
	je	.LBB1_9
	jmp	.LBB1_22
.LBB1_17:                               #   in Loop: Header=BB1_2 Depth=1
	movq	stderr(%rip), %rdi
	movq	24(%rbp), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movl	$.L.str.9, %esi
	movb	$1, %al
	callq	fprintf
	jmp	.LBB1_22
.LBB1_11:                               #   in Loop: Header=BB1_2 Depth=1
	movq	stderr(%rip), %rdi
	movq	24(%rbp), %rax
	movq	(%rax), %rdx
	movl	$.L.str.5, %esi
	jmp	.LBB1_20
.LBB1_12:                               #   in Loop: Header=BB1_2 Depth=1
	movq	24(%rbp), %rbx
	cmpb	$0, (%rbx)
	jne	.LBB1_13
# BB#16:                                #   in Loop: Header=BB1_2 Depth=1
	movq	stderr(%rip), %rcx
	movl	$.L.str.8, %edi
	jmp	.LBB1_10
.LBB1_13:                               #   in Loop: Header=BB1_2 Depth=1
	movq	%rbx, %rdi
	callq	strlen
	cmpq	$9, %rax
	ja	.LBB1_15
# BB#14:                                #   in Loop: Header=BB1_2 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	jmp	.LBB1_21
.LBB1_15:                               #   in Loop: Header=BB1_2 Depth=1
	movb	$0, 7(%rbx)
	movq	stderr(%rip), %rdi
	movq	24(%rbp), %rdx
	movl	$.L.str.7, %esi
.LBB1_20:                               #   in Loop: Header=BB1_2 Depth=1
	xorl	%eax, %eax
.LBB1_21:                               #   in Loop: Header=BB1_2 Depth=1
	callq	fprintf
.LBB1_22:                               #   in Loop: Header=BB1_2 Depth=1
	movq	stderr(%rip), %rdi
	movq	8(%rbp), %rdx
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB1_23
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$0, 8(%rbp)
	jne	.LBB1_3
.LBB1_23:                               #   in Loop: Header=BB1_2 Depth=1
	movq	48(%rbp), %rbx
	addq	$56, %rbp
	testq	%rbx, %rbx
	jne	.LBB1_2
.LBB1_24:                               # %._crit_edge
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	usage, .Lfunc_end1-usage
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_18
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_17
	.quad	.LBB1_22
	.quad	.LBB1_19
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_18
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_11
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_12
	.quad	.LBB1_19
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_22
	.quad	.LBB1_19

	.text
	.globl	process_args
	.p2align	4, 0x90
	.type	process_args,@function
process_args:                           # @process_args
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 112
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	%rbx, 16(%rsp)
	movq	24(%r15), %r13
	cmpq	$0, (%r13)
	je	.LBB2_13
# BB#1:                                 # %.lr.ph88.preheader
	leaq	56(%r13), %rbp
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph88
                                        # =>This Inner Loop Header: Depth=1
	movq	-16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_12
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	callq	getenv
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB2_12
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	-32(%rbp), %rdi
	movsbl	(%rdi), %eax
	addl	$-68, %eax
	cmpl	$15, %eax
	ja	.LBB2_10
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_7:                                #   in Loop: Header=BB2_2 Depth=1
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	strtod
	movq	-24(%rbp), %rax
	movsd	%xmm0, (%rax)
	jmp	.LBB2_10
.LBB2_6:                                #   in Loop: Header=BB2_2 Depth=1
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	callq	strtol
	movq	-24(%rbp), %rcx
	movl	%eax, (%rcx)
	jmp	.LBB2_10
.LBB2_8:                                #   in Loop: Header=BB2_2 Depth=1
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	callq	strtoll
	movq	-24(%rbp), %rcx
	movq	%rax, (%rcx)
	jmp	.LBB2_10
.LBB2_9:                                #   in Loop: Header=BB2_2 Depth=1
	movq	-24(%rbp), %r14
	incq	%rdi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	strtol
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	callq	strncpy
	.p2align	4, 0x90
.LBB2_10:                               #   in Loop: Header=BB2_2 Depth=1
	movq	-8(%rbp), %rax
	testq	%rax, %rax
	je	.LBB2_12
# BB#11:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	*%rax
.LBB2_12:                               #   in Loop: Header=BB2_2 Depth=1
	cmpq	$0, (%rbp)
	leaq	56(%rbp), %rbp
	jne	.LBB2_2
.LBB2_13:                               # %._crit_edge89
	movq	(%rbx), %rax
	movq	%rax, 16(%r15)
	leaq	8(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, 16(%rsp)
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.LBB2_41
# BB#14:                                # %.lr.ph84
	leaq	56(%r13), %rbp
	leaq	16(%rsp), %rbx
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_15:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_18 Depth 2
                                        #       Child Loop BB2_20 Depth 3
                                        #     Child Loop BB2_25 Depth 2
                                        #     Child Loop BB2_32 Depth 2
	cmpb	$45, (%rax)
	jne	.LBB2_39
# BB#16:                                #   in Loop: Header=BB2_15 Depth=1
	cmpb	$45, 1(%rax)
	jne	.LBB2_17
# BB#22:                                # %.preheader72
                                        #   in Loop: Header=BB2_15 Depth=1
	movq	(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB2_35
# BB#23:                                # %.lr.ph81
                                        #   in Loop: Header=BB2_15 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rbp
	addq	$2, %rbp
	movl	$61, %esi
	movq	%rbp, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB2_24
# BB#31:                                # %.lr.ph81.split.us.preheader
                                        #   in Loop: Header=BB2_15 Depth=1
	movq	%rax, %r12
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%ebp, %eax
	movslq	%eax, %r15
	movq	24(%rsp), %rbp          # 8-byte Reload
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_32:                               # %.lr.ph81.split.us
                                        #   Parent Loop BB2_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	strlen
	cmpq	%rax, %r15
	jne	.LBB2_34
# BB#33:                                #   in Loop: Header=BB2_32 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %r14
	leaq	2(%r14), %rsi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB2_28
.LBB2_34:                               #   in Loop: Header=BB2_32 Depth=2
	incq	%r13
	movq	(%rbp), %rbx
	addq	$56, %rbp
	testq	%rbx, %rbx
	jne	.LBB2_32
	jmp	.LBB2_35
	.p2align	4, 0x90
.LBB2_39:                               #   in Loop: Header=BB2_15 Depth=1
	movq	(%r15), %rdi
	movslq	8(%r15), %rax
	leaq	16(,%rax,8), %rsi
	callq	realloc
	movq	%rax, (%r15)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx), %rcx
	movslq	8(%r15), %rdx
	leaq	1(%rdx), %rsi
	movl	%esi, 8(%r15)
	movq	%rcx, (%rax,%rdx,8)
	movq	$0, 8(%rax,%rdx,8)
	jmp	.LBB2_40
	.p2align	4, 0x90
.LBB2_17:                               # %.preheader73
                                        #   in Loop: Header=BB2_15 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rdx), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, (%rdx)
	cmpb	$0, 1(%rax)
	je	.LBB2_40
	.p2align	4, 0x90
.LBB2_18:                               # %.preheader
                                        #   Parent Loop BB2_15 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_20 Depth 3
	cmpq	$0, (%r13)
	je	.LBB2_38
# BB#19:                                # %.lr.ph
                                        #   in Loop: Header=BB2_18 Depth=2
	movq	16(%rsp), %rax
	movq	(%rax), %rax
	movb	(%rax), %al
	xorl	%esi, %esi
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB2_20:                               #   Parent Loop BB2_15 Depth=1
                                        #     Parent Loop BB2_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	%al, -48(%rcx)
	je	.LBB2_21
# BB#37:                                #   in Loop: Header=BB2_20 Depth=3
	incl	%esi
	cmpq	$0, (%rcx)
	leaq	56(%rcx), %rcx
	jne	.LBB2_20
	jmp	.LBB2_38
	.p2align	4, 0x90
.LBB2_21:                               #   in Loop: Header=BB2_18 Depth=2
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	process_arg
	movq	16(%rsp), %rax
	movq	(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rax)
	cmpb	$0, 1(%rcx)
	jne	.LBB2_18
	jmp	.LBB2_40
.LBB2_24:                               # %.lr.ph81.split.preheader
                                        #   in Loop: Header=BB2_15 Depth=1
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	callq	strlen
	movslq	%eax, %r15
	movq	24(%rsp), %r12          # 8-byte Reload
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_25:                               # %.lr.ph81.split
                                        #   Parent Loop BB2_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	strlen
	cmpq	%rax, %r15
	jne	.LBB2_36
# BB#26:                                #   in Loop: Header=BB2_25 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %r14
	leaq	2(%r14), %rsi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB2_27
.LBB2_36:                               #   in Loop: Header=BB2_25 Depth=2
	incq	%r13
	movq	(%r12), %rbx
	addq	$56, %r12
	testq	%rbx, %rbx
	jne	.LBB2_25
	jmp	.LBB2_35
.LBB2_27:                               #   in Loop: Header=BB2_15 Depth=1
	movq	40(%rsp), %r12          # 8-byte Reload
.LBB2_28:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB2_15 Depth=1
	testq	%r12, %r12
	movq	32(%rsp), %r15          # 8-byte Reload
	jne	.LBB2_30
# BB#29:                                #   in Loop: Header=BB2_15 Depth=1
	movq	%r14, %rdi
	callq	strlen
	leaq	-1(%r14,%rax), %r12
.LBB2_30:                               #   in Loop: Header=BB2_15 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%r12, (%rax)
	movq	%r15, %rdi
	movl	%r13d, %esi
	leaq	16(%rsp), %rbx
	movq	%rbx, %rdx
	callq	process_arg
	movq	48(%rsp), %r13          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB2_40:                               # %.backedge
                                        #   in Loop: Header=BB2_15 Depth=1
	movq	16(%rsp), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rcx, 16(%rsp)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.LBB2_15
.LBB2_41:                               # %._crit_edge85
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_38:                               # %.preheader._crit_edge
	movq	%r15, %rdi
	callq	usage
.LBB2_35:                               # %.preheader72._crit_edge
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	usage
.Lfunc_end2:
	.size	process_args, .Lfunc_end2-process_args
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_7
	.quad	.LBB2_10
	.quad	.LBB2_10
	.quad	.LBB2_10
	.quad	.LBB2_10
	.quad	.LBB2_6
	.quad	.LBB2_10
	.quad	.LBB2_10
	.quad	.LBB2_8
	.quad	.LBB2_10
	.quad	.LBB2_10
	.quad	.LBB2_10
	.quad	.LBB2_10
	.quad	.LBB2_10
	.quad	.LBB2_10
	.quad	.LBB2_9

	.text
	.globl	free_args
	.p2align	4, 0x90
	.type	free_args,@function
free_args:                              # @free_args
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB3_1
# BB#2:
	jmp	free                    # TAILCALL
.LBB3_1:
	retq
.Lfunc_end3:
	.size	free_args, .Lfunc_end3-free_args
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s:bad argument description\n"
	.size	.L.str, 29

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Usage: %s [flags|args]\n"
	.size	.L.str.1, 24

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"  %c%c%c --%s%s%s"
	.size	.L.str.2, 18

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.zero	1
	.size	.L.str.3, 1

	.type	arg_types_desc,@object  # @arg_types_desc
	.section	.rodata,"a",@progbits
	.p2align	4
arg_types_desc:
	.quad	.L.str.15
	.quad	.L.str.16
	.quad	.L.str.17
	.quad	.L.str.18
	.quad	.L.str.19
	.quad	.L.str.20
	.quad	.L.str.21
	.quad	.L.str.22
	.quad	.L.str.23
	.size	arg_types_desc, 72

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"          "
	.size	.L.str.4, 11

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	" %-9lld"
	.size	.L.str.5, 8

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	" %-9s"
	.size	.L.str.6, 6

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	" %-7s.."
	.size	.L.str.7, 8

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	" (null)   "
	.size	.L.str.8, 11

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	" %-9.3e"
	.size	.L.str.9, 8

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	" %-9d"
	.size	.L.str.10, 6

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"true "
	.size	.L.str.11, 6

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"false"
	.size	.L.str.12, 6

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	" %s\n"
	.size	.L.str.13, 5

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"                                                                               "
	.size	.L.str.14, 80

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"int     "
	.size	.L.str.15, 9

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"string  "
	.size	.L.str.16, 9

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"double  "
	.size	.L.str.17, 9

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"set off "
	.size	.L.str.18, 9

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"set on  "
	.size	.L.str.19, 9

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"incr    "
	.size	.L.str.20, 9

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"toggle  "
	.size	.L.str.21, 9

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"int64   "
	.size	.L.str.22, 9

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"        "
	.size	.L.str.23, 9

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"ISDfF+TL"
	.size	.L.str.24, 9


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
