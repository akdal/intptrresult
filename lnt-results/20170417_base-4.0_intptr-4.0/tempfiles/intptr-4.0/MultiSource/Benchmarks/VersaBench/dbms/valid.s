	.text
	.file	"valid.bc"
	.globl	validIndexKey
	.p2align	4, 0x90
	.type	validIndexKey,@function
validIndexKey:                          # @validIndexKey
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	16(%rdi), %xmm0
	jae	.LBB0_1
# BB#2:
	movss	4(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	20(%rdi), %xmm0
	jae	.LBB0_3
# BB#4:
	movss	8(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	24(%rdi), %xmm0
	jae	.LBB0_5
# BB#6:
	movss	12(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movb	$1, %bl
	ucomiss	28(%rdi), %xmm0
	jb	.LBB0_9
# BB#7:
	xorl	%ebx, %ebx
	movl	$.L.str.3, %edi
	jmp	.LBB0_8
.LBB0_1:
	xorl	%ebx, %ebx
	movl	$.L.str, %edi
	jmp	.LBB0_8
.LBB0_3:
	xorl	%ebx, %ebx
	movl	$.L.str.1, %edi
	jmp	.LBB0_8
.LBB0_5:
	xorl	%ebx, %ebx
	movl	$.L.str.2, %edi
.LBB0_8:
	xorl	%esi, %esi
	callq	errorMessage
	movl	$validIndexKey.name, %edi
	movl	$1, %esi
	callq	errorMessage
.LBB0_9:
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	validIndexKey, .Lfunc_end0-validIndexKey
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	-4039728866278637763    # double -3.4028234699999998E+38
.LCPI1_1:
	.quad	5183643170576138045     # double 3.4028234699999998E+38
	.text
	.globl	validAttributes
	.p2align	4, 0x90
	.type	validAttributes,@function
validAttributes:                        # @validAttributes
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movb	$1, %bl
	testq	%rdi, %rdi
	je	.LBB1_12
# BB#1:                                 # %.lr.ph.preheader
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI1_1(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rax
	cmpq	$51, %rax
	jae	.LBB1_3
# BB#5:                                 #   in Loop: Header=BB1_2 Depth=1
	cmpq	$7, %rax
	jg	.LBB1_9
# BB#6:                                 #   in Loop: Header=BB1_2 Depth=1
	movss	8(%rdi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	ucomisd	%xmm2, %xmm0
	ja	.LBB1_8
# BB#7:                                 #   in Loop: Header=BB1_2 Depth=1
	ucomisd	%xmm1, %xmm2
	jbe	.LBB1_11
	jmp	.LBB1_8
	.p2align	4, 0x90
.LBB1_9:                                #   in Loop: Header=BB1_2 Depth=1
	cmpq	$0, 8(%rdi)
	je	.LBB1_10
.LBB1_11:                               #   in Loop: Header=BB1_2 Depth=1
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_2
	jmp	.LBB1_12
.LBB1_3:
	xorl	%ebx, %ebx
	movl	$.L.str.4, %edi
	jmp	.LBB1_4
.LBB1_8:
	xorl	%ebx, %ebx
	movl	$.L.str.5, %edi
	jmp	.LBB1_4
.LBB1_10:
	xorl	%ebx, %ebx
	movl	$.L.str.6, %edi
.LBB1_4:                                # %.loopexit
	xorl	%esi, %esi
	callq	errorMessage
	movl	$validAttributes.name, %edi
	movl	$1, %esi
	callq	errorMessage
.LBB1_12:                               # %.loopexit
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	validAttributes, .Lfunc_end1-validAttributes
	.cfi_endproc

	.type	validIndexKey.name,@object # @validIndexKey.name
	.data
validIndexKey.name:
	.asciz	"validIndexKey"
	.size	validIndexKey.name, 14

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Lower T > Upper T"
	.size	.L.str, 18

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Lower X > Upper X"
	.size	.L.str.1, 18

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Lower Y > Upper Y"
	.size	.L.str.2, 18

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Lower Z > Upper Z"
	.size	.L.str.3, 18

	.type	validAttributes.name,@object # @validAttributes.name
	.data
	.p2align	4
validAttributes.name:
	.asciz	"validAttributes"
	.size	validAttributes.name, 16

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"invalid attribute code"
	.size	.L.str.4, 23

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"key value out-of-range"
	.size	.L.str.5, 23

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"non-key value set to NULL"
	.size	.L.str.6, 26


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
