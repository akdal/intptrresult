	.text
	.file	"uuencode.bc"
	.globl	encode_char
	.p2align	4, 0x90
	.type	encode_char,@function
encode_char:                            # @encode_char
	.cfi_startproc
# BB#0:
	andb	$63, %dil
	addb	$32, %dil
	movzbl	%dil, %eax
	retq
.Lfunc_end0:
	.size	encode_char, .Lfunc_end0-encode_char
	.cfi_endproc

	.globl	encode_line
	.p2align	4, 0x90
	.type	encode_line,@function
encode_line:                            # @encode_line
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%edx, %eax
	andb	$63, %al
	addb	$32, %al
	movb	%al, (%rcx)
	testl	%edx, %edx
	jle	.LBB1_1
# BB#2:                                 # %.lr.ph.preheader
	movslq	%esi, %rax
	addq	%rax, %rdi
	movl	$1, %r8d
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$3, %edx
	jl	.LBB1_5
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=1
	movzbl	(%rdi), %eax
	shrb	$2, %al
	addb	$32, %al
	movb	%al, (%rcx,%r8)
	movzbl	(%rdi), %eax
	shll	$4, %eax
	movsbl	1(%rdi), %esi
	shrl	$4, %esi
	orl	%eax, %esi
	andb	$63, %sil
	addb	$32, %sil
	movb	%sil, 1(%rcx,%r8)
	movzbl	1(%rdi), %eax
	shll	$2, %eax
	movsbl	2(%rdi), %esi
	shrl	$6, %esi
	orl	%eax, %esi
	andb	$63, %sil
	addb	$32, %sil
	movb	%sil, 2(%rcx,%r8)
	movzbl	2(%rdi), %eax
	andb	$63, %al
	addb	$32, %al
	jmp	.LBB1_10
	.p2align	4, 0x90
.LBB1_5:                                #   in Loop: Header=BB1_3 Depth=1
	cmpl	$2, %edx
	je	.LBB1_8
# BB#6:                                 #   in Loop: Header=BB1_3 Depth=1
	cmpl	$1, %edx
	jne	.LBB1_11
# BB#7:                                 #   in Loop: Header=BB1_3 Depth=1
	movzbl	(%rdi), %eax
	shrb	$2, %al
	addb	$32, %al
	movb	%al, (%rcx,%r8)
	movzbl	(%rdi), %eax
	shlb	$4, %al
	andb	$48, %al
	addb	$32, %al
	movb	%al, 1(%rcx,%r8)
	movb	$61, 2(%rcx,%r8)
	jmp	.LBB1_9
	.p2align	4, 0x90
.LBB1_8:                                #   in Loop: Header=BB1_3 Depth=1
	movzbl	(%rdi), %eax
	shrb	$2, %al
	addb	$32, %al
	movb	%al, (%rcx,%r8)
	movzbl	(%rdi), %eax
	shll	$4, %eax
	movsbl	1(%rdi), %esi
	shrl	$4, %esi
	orl	%eax, %esi
	andb	$63, %sil
	addb	$32, %sil
	movb	%sil, 1(%rcx,%r8)
	movzbl	1(%rdi), %eax
	shlb	$2, %al
	andb	$60, %al
	addb	$32, %al
	movb	%al, 2(%rcx,%r8)
.LBB1_9:                                #   in Loop: Header=BB1_3 Depth=1
	movb	$61, %al
.LBB1_10:                               #   in Loop: Header=BB1_3 Depth=1
	leal	3(%r8), %esi
	movslq	%esi, %rsi
	movb	%al, (%rcx,%rsi)
	addq	$3, %rdi
	addq	$4, %r8
	cmpl	$3, %edx
	leal	-3(%rdx), %eax
	movl	%eax, %edx
	jg	.LBB1_3
	jmp	.LBB1_11
.LBB1_1:
	movl	$1, %r8d
.LBB1_11:                               # %._crit_edge
	movslq	%r8d, %rax
	movw	$10, (%rcx,%rax)
	retq
.Lfunc_end1:
	.size	encode_line, .Lfunc_end1-encode_line
	.cfi_endproc

	.globl	encode
	.p2align	4, 0x90
	.type	encode,@function
encode:                                 # @encode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$64, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 112
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %r12d
	movq	%rdi, %r14
	testl	%r12d, %r12d
	jle	.LBB2_6
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	movq	%rsp, %r15
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%r12d, %edx
	subl	%ebp, %edx
	cmpl	$45, %edx
	jl	.LBB2_4
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	movl	$45, %edx
	movq	%r14, %rdi
	movl	%ebp, %esi
	movq	%r15, %rcx
	callq	encode_line
	addl	$45, %ebp
	jmp	.LBB2_5
	.p2align	4, 0x90
.LBB2_4:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%r14, %rdi
	movl	%ebp, %esi
	movq	%r15, %rcx
	callq	encode_line
	movl	%r12d, %ebp
.LBB2_5:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	strcat
	movq	%rbx, %rdi
	callq	strlen
	addq	%rax, %rbx
	cmpl	%r12d, %ebp
	jl	.LBB2_2
.LBB2_6:                                # %._crit_edge
	movq	%rbx, %rdi
	callq	strlen
	movw	$100, 4(%rbx,%rax)
	movl	$1852115488, (%rbx,%rax) # imm = 0x6E650A20
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	encode, .Lfunc_end2-encode
	.cfi_endproc

	.globl	do_encode
	.p2align	4, 0x90
	.type	do_encode,@function
do_encode:                              # @do_encode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 128
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movq	%rsi, %r14
	movq	%rdi, %r15
	movabsq	$3762230197301503330, %rax # imm = 0x3436206E69676562
	movq	%rax, (%r14)
	movb	$0, 10(%r14)
	movw	$8240, 8(%r14)          # imm = 0x2030
	movq	%r14, %rdi
	movq	%rcx, %rsi
	callq	strcat
	movq	%r14, %rdi
	callq	strlen
	movb	$0, 2(%r14,%rax)
	movw	$2592, (%r14,%rax)      # imm = 0xA20
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
	addq	%r14, %rbx
	testl	%r13d, %r13d
	jle	.LBB3_6
# BB#1:                                 # %.lr.ph.i.preheader
	xorl	%ebp, %ebp
	movq	%rsp, %r12
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%r13d, %edx
	subl	%ebp, %edx
	cmpl	$45, %edx
	jl	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	$45, %edx
	movq	%r15, %rdi
	movl	%ebp, %esi
	movq	%r12, %rcx
	callq	encode_line
	addl	$45, %ebp
	jmp	.LBB3_5
	.p2align	4, 0x90
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	movq	%r15, %rdi
	movl	%ebp, %esi
	movq	%r12, %rcx
	callq	encode_line
	movl	%r13d, %ebp
.LBB3_5:                                #   in Loop: Header=BB3_2 Depth=1
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	strcat
	movq	%rbx, %rdi
	callq	strlen
	addq	%rax, %rbx
	cmpl	%r13d, %ebp
	jl	.LBB3_2
.LBB3_6:                                # %encode.exit
	movq	%rbx, %rdi
	callq	strlen
	movw	$100, 4(%rbx,%rax)
	movl	$1852115488, (%rbx,%rax) # imm = 0x6E650A20
	movq	%r14, %rdi
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	strlen                  # TAILCALL
.Lfunc_end3:
	.size	do_encode, .Lfunc_end3-do_encode
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 80
.Lcfi30:
	.cfi_offset %rbx, -48
.Lcfi31:
	.cfi_offset %r12, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movl	%edi, %ebp
	movl	$1000000, %edi          # imm = 0xF4240
	callq	malloc
	movq	%rax, %r14
	movl	$2000000, %edi          # imm = 0x1E8480
	callq	malloc
	movq	%rax, %r12
	cmpl	$2, %ebp
	jl	.LBB4_3
# BB#1:
	movq	8(%r15), %rdi
	movl	$.L.str.3, %esi
	callq	fopen
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB4_8
# BB#2:
	decl	%ebp
	cmpl	$1, %ebp
	je	.LBB4_5
	jmp	.LBB4_9
.LBB4_3:
	movq	stdin(%rip), %rcx
	cmpl	$1, %ebp
	jne	.LBB4_9
.LBB4_5:
	movl	$1, %esi
	movl	$1000000, %edx          # imm = 0xF4240
	movq	%r14, %rdi
	callq	fread
	movq	%rax, %r15
	leaq	16(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	movl	$1000, %ebp             # imm = 0x3E8
	.p2align	4, 0x90
.LBB4_6:                                # =>This Inner Loop Header: Depth=1
	movl	$.L.str.5, %ecx
	movq	%r14, %rdi
	movq	%r12, %rsi
	movl	%r15d, %edx
	callq	do_encode
	movl	%eax, %ebx
	decl	%ebp
	jne	.LBB4_6
# BB#7:
	movq	%rsp, %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	xorl	%edi, %edi
	callq	exit
.LBB4_9:
	movl	$.Lstr, %edi
	callq	puts
	movl	$2, %edi
	callq	exit
.LBB4_8:
	movq	8(%r15), %rdi
	callq	perror
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	main, .Lfunc_end4-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" \nend"
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"begin 640 "
	.size	.L.str.1, 11

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" \n"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"r"
	.size	.L.str.3, 2

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"uuencode.c"
	.size	.L.str.5, 11

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%d\n"
	.size	.L.str.6, 4

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Usage: uuencode [infile]"
	.size	.Lstr, 25


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
