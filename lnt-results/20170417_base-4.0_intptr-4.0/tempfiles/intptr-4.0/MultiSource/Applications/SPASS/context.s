	.text
	.file	"context.bc"
	.globl	cont_Init
	.p2align	4, 0x90
	.type	cont_Init,@function
cont_Init:                              # @cont_Init
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	$0, cont_LASTBINDING(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movl	$0, cont_NOOFCONTEXTS(%rip)
	movq	$0, cont_LISTOFCONTEXTS(%rip)
	movl	$0, cont_BINDINGS(%rip)
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rax, cont_INSTANCECONTEXT(%rip)
	movl	$96032, %edi            # imm = 0x17720
	callq	memory_Malloc
	movq	%rax, %r14
	addq	$32, %rax
	xorl	%ecx, %ecx
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_5:                                #   in Loop: Header=BB0_1 Depth=1
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	$0, 24(%rax)
	movq	$0, 8(%rax)
	movl	%edx, (%rax)
	movq	cont_CURRENTBINDING(%rip), %rdx
	movl	$0, 4(%rdx)
	movq	cont_CURRENTBINDING(%rip), %rdx
	movq	$0, 16(%rdx)
	addq	$2, %rcx
	addq	$64, %rax
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	leaq	-32(%rax), %rdx
	movq	%rdx, cont_CURRENTBINDING(%rip)
	movq	$0, -8(%rax)
	movq	$0, -24(%rax)
	movl	%ecx, -32(%rax)
	movq	cont_CURRENTBINDING(%rip), %rdx
	movl	$0, 4(%rdx)
	movq	cont_CURRENTBINDING(%rip), %rdx
	movq	$0, 16(%rdx)
	movq	%rcx, %rdx
	orq	$1, %rdx
	cmpq	$3001, %rdx             # imm = 0xBB9
	jne	.LBB0_5
# BB#2:                                 # %cont_Create.exit4
	movq	cont_LISTOFCONTEXTS(%rip), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, cont_LISTOFCONTEXTS(%rip)
	incl	cont_NOOFCONTEXTS(%rip)
	movq	%r14, cont_LEFTCONTEXT(%rip)
	movl	$96032, %edi            # imm = 0x17720
	callq	memory_Malloc
	movq	%rax, %r14
	addq	$32, %rax
	xorl	%ecx, %ecx
	jmp	.LBB0_3
	.p2align	4, 0x90
.LBB0_4:                                #   in Loop: Header=BB0_3 Depth=1
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	$0, 24(%rax)
	movq	$0, 8(%rax)
	movl	%edx, (%rax)
	movq	cont_CURRENTBINDING(%rip), %rdx
	movl	$0, 4(%rdx)
	movq	cont_CURRENTBINDING(%rip), %rdx
	movq	$0, 16(%rdx)
	addq	$2, %rcx
	addq	$64, %rax
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	leaq	-32(%rax), %rdx
	movq	%rdx, cont_CURRENTBINDING(%rip)
	movq	$0, -8(%rax)
	movq	$0, -24(%rax)
	movl	%ecx, -32(%rax)
	movq	cont_CURRENTBINDING(%rip), %rdx
	movl	$0, 4(%rdx)
	movq	cont_CURRENTBINDING(%rip), %rdx
	movq	$0, 16(%rdx)
	movq	%rcx, %rdx
	orq	$1, %rdx
	cmpq	$3001, %rdx             # imm = 0xBB9
	jne	.LBB0_4
# BB#6:                                 # %cont_Create.exit
	movq	cont_LISTOFCONTEXTS(%rip), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbx, (%rax)
	incl	cont_NOOFCONTEXTS(%rip)
	movq	%rax, cont_LISTOFCONTEXTS(%rip)
	movq	%r14, cont_RIGHTCONTEXT(%rip)
	movl	$0, cont_STACK+4(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	cont_Init, .Lfunc_end0-cont_Init
	.cfi_endproc

	.globl	cont_Check
	.p2align	4, 0x90
	.type	cont_Check,@function
cont_Check:                             # @cont_Check
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	cont_Check, .Lfunc_end1-cont_Check
	.cfi_endproc

	.globl	cont_Free
	.p2align	4, 0x90
	.type	cont_Free,@function
cont_Free:                              # @cont_Free
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -48
.Lcfi11:
	.cfi_offset %r12, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	cmpl	$0, cont_NOOFCONTEXTS(%rip)
	jle	.LBB2_7
# BB#1:                                 # %.lr.ph
	movl	memory_ALIGN(%rip), %r14d
	leal	96032(%r14), %ebp
	movl	$96032, %r15d           # imm = 0x17720
	movl	$memory_BIGBLOCKS, %r12d
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movq	cont_LISTOFCONTEXTS(%rip), %rdi
	movq	8(%rdi), %rbx
	movq	%rbx, %rsi
	callq	list_PointerDeleteOneElement
	movq	%rax, cont_LISTOFCONTEXTS(%rip)
	decl	cont_NOOFCONTEXTS(%rip)
	movl	$96032, %eax            # imm = 0x17720
	xorl	%edx, %edx
	divl	%r14d
	movl	%ebp, %eax
	subl	%edx, %eax
	testl	%edx, %edx
	cmovel	%r15d, %eax
	movl	memory_OFFSET(%rip), %ecx
	movq	%rbx, %rdx
	subq	%rcx, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %rdi
	testq	%rsi, %rsi
	leaq	8(%rsi), %rsi
	cmoveq	%r12, %rsi
	movq	%rdi, (%rsi)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB2_4
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	negq	%rcx
	movq	-16(%rbx,%rcx), %rcx
	movq	%rcx, (%rdx)
.LBB2_4:                                #   in Loop: Header=BB2_2 Depth=1
	addl	memory_MARKSIZE(%rip), %eax
	movq	memory_FREEDBYTES(%rip), %rcx
	leaq	16(%rcx,%rax), %rcx
	movq	%rcx, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rcx
	testq	%rcx, %rcx
	js	.LBB2_6
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
	leaq	16(%rax,%rcx), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB2_6:                                # %cont_Delete.exit
                                        #   in Loop: Header=BB2_2 Depth=1
	addq	$-16, %rbx
	movq	%rbx, %rdi
	callq	free
	cmpl	$0, cont_NOOFCONTEXTS(%rip)
	jg	.LBB2_2
.LBB2_7:                                # %._crit_edge
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_INSTANCECONTEXT(%rip), %rax
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rax, (%rcx)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	cont_Free, .Lfunc_end2-cont_Free
	.cfi_endproc

	.globl	cont_TermEqual
	.p2align	4, 0x90
	.type	cont_TermEqual,@function
cont_TermEqual:                         # @cont_TermEqual
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 48
.Lcfi20:
	.cfi_offset %rbx, -48
.Lcfi21:
	.cfi_offset %r12, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rdi, %r12
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.LBB3_5
# BB#1:                                 # %.lr.ph.i.preheader
	movq	cont_INSTANCECONTEXT(%rip), %rax
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rax, %r12
	je	.LBB3_5
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movslq	%edx, %rdx
	shlq	$5, %rdx
	movq	8(%r12,%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_5
# BB#4:                                 # %.thread.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	16(%r12,%rdx), %r12
	movl	(%rdi), %edx
	testl	%edx, %edx
	movq	%rdi, %rsi
	jg	.LBB3_2
.LBB3_5:                                # %cont_Deref.exit
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.LBB3_11
# BB#6:                                 # %.lr.ph.i29.preheader
	movq	cont_INSTANCECONTEXT(%rip), %rdx
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph.i29
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rdx, %r14
	je	.LBB3_10
# BB#8:                                 #   in Loop: Header=BB3_7 Depth=1
	cltq
	shlq	$5, %rax
	movq	8(%r14,%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB3_10
# BB#9:                                 # %.thread.i34
                                        #   in Loop: Header=BB3_7 Depth=1
	movq	16(%r14,%rax), %r14
	movl	(%rdi), %eax
	testl	%eax, %eax
	movq	%rdi, %rcx
	jg	.LBB3_7
.LBB3_10:                               # %cont_Deref.exit36.loopexit
	movl	(%rcx), %eax
.LBB3_11:                               # %cont_Deref.exit36
	xorl	%r15d, %r15d
	cmpl	%eax, (%rsi)
	jne	.LBB3_20
# BB#12:
	movq	16(%rsi), %rbx
	testq	%rbx, %rbx
	je	.LBB3_13
# BB#14:
	movq	16(%rcx), %rbp
	testq	%rbp, %rbp
	je	.LBB3_20
	.p2align	4, 0x90
.LBB3_15:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	8(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r14, %rdx
	callq	cont_TermEqual
	testl	%eax, %eax
	je	.LBB3_20
# BB#16:                                #   in Loop: Header=BB3_15 Depth=1
	movq	(%rbx), %rbx
	movq	(%rbp), %rbp
	testq	%rbx, %rbx
	je	.LBB3_18
# BB#17:                                #   in Loop: Header=BB3_15 Depth=1
	testq	%rbp, %rbp
	jne	.LBB3_15
.LBB3_18:                               # %.critedge
	testq	%rbx, %rbx
	jne	.LBB3_20
# BB#19:
	xorl	%r15d, %r15d
	testq	%rbp, %rbp
	sete	%r15b
	jmp	.LBB3_20
.LBB3_13:
	movl	$1, %r15d
.LBB3_20:                               # %.loopexit
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	cont_TermEqual, .Lfunc_end3-cont_TermEqual
	.cfi_endproc

	.globl	cont_TermEqualModuloBindings
	.p2align	4, 0x90
	.type	cont_TermEqualModuloBindings,@function
cont_TermEqualModuloBindings:           # @cont_TermEqualModuloBindings
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 48
.Lcfi30:
	.cfi_offset %rbx, -48
.Lcfi31:
	.cfi_offset %r12, -40
.Lcfi32:
	.cfi_offset %r13, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %r15, -16
	movq	%rcx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	(%rdx), %esi
	testl	%esi, %esi
	jle	.LBB4_6
# BB#1:                                 # %.lr.ph111
	movq	cont_INSTANCECONTEXT(%rip), %rcx
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	leal	-2001(%rsi), %eax
	cmpl	$1000, %eax             # imm = 0x3E8
	movq	%r14, %rdi
	jb	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	cmpq	%rcx, %rbx
	movq	%rbx, %rdi
	je	.LBB4_8
.LBB4_4:                                #   in Loop: Header=BB4_2 Depth=1
	movslq	%esi, %rsi
	shlq	$5, %rsi
	movq	8(%rdi,%rsi), %rax
	testq	%rax, %rax
	je	.LBB4_7
# BB#5:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	16(%rdi,%rsi), %rbx
	movl	(%rax), %esi
	testl	%esi, %esi
	movq	%rax, %rdx
	jg	.LBB4_2
	jmp	.LBB4_9
.LBB4_6:
	movq	%rdx, %rax
	jmp	.LBB4_9
.LBB4_7:
	movq	%rdx, %rax
	movq	%rdi, %rbx
	jmp	.LBB4_9
.LBB4_8:
	movq	%rdx, %rax
	movq	%rcx, %rbx
.LBB4_9:                                # %.thread.preheader
	movl	(%r8), %ecx
	testl	%ecx, %ecx
	jle	.LBB4_18
# BB#10:                                # %.lr.ph98
	movq	cont_INSTANCECONTEXT(%rip), %rdx
	.p2align	4, 0x90
.LBB4_11:                               # =>This Inner Loop Header: Depth=1
	leal	-2001(%rcx), %esi
	cmpl	$1000, %esi             # imm = 0x3E8
	movq	%r14, %rdi
	jb	.LBB4_13
# BB#12:                                #   in Loop: Header=BB4_11 Depth=1
	cmpq	%rdx, %r15
	movq	%r15, %rdi
	je	.LBB4_16
.LBB4_13:                               #   in Loop: Header=BB4_11 Depth=1
	movslq	%ecx, %rcx
	shlq	$5, %rcx
	movq	8(%rdi,%rcx), %rsi
	testq	%rsi, %rsi
	je	.LBB4_15
# BB#14:                                # %.thread
                                        #   in Loop: Header=BB4_11 Depth=1
	movq	16(%rdi,%rcx), %r15
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	movq	%rsi, %r8
	jg	.LBB4_11
	jmp	.LBB4_17
.LBB4_15:
	movq	%r8, %rsi
	movq	%rdi, %r15
	jmp	.LBB4_17
.LBB4_16:
	movq	%r8, %rsi
	movq	%rdx, %r15
.LBB4_17:                               # %.thread83.loopexit
	movl	(%rsi), %ecx
	movq	%rsi, %r8
.LBB4_18:                               # %.thread83
	cmpl	%ecx, (%rax)
	jne	.LBB4_21
# BB#19:
	testl	%ecx, %ecx
	jle	.LBB4_23
# BB#20:
	cmpq	%r15, %rbx
	sete	%al
	jmp	.LBB4_22
.LBB4_23:
	movq	16(%rax), %r12
	testq	%r12, %r12
	je	.LBB4_29
# BB#24:
	movq	16(%r8), %r13
	testq	%r13, %r13
	je	.LBB4_30
	.p2align	4, 0x90
.LBB4_25:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %rdx
	movq	8(%r13), %r8
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rcx
	callq	cont_TermEqualModuloBindings
	testl	%eax, %eax
	je	.LBB4_21
# BB#26:                                #   in Loop: Header=BB4_25 Depth=1
	movq	(%r12), %r12
	movq	(%r13), %r13
	testq	%r12, %r12
	je	.LBB4_28
# BB#27:                                #   in Loop: Header=BB4_25 Depth=1
	testq	%r13, %r13
	jne	.LBB4_25
.LBB4_28:                               # %.critedge.loopexit
	testq	%r13, %r13
	sete	%cl
	jmp	.LBB4_31
.LBB4_21:
	xorl	%eax, %eax
.LBB4_22:                               # %.loopexit
	movzbl	%al, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB4_29:
	movb	$1, %al
	jmp	.LBB4_22
.LBB4_30:
	movb	$1, %cl
.LBB4_31:                               # %.critedge
	testq	%r12, %r12
	sete	%al
	andb	%cl, %al
	jmp	.LBB4_22
.Lfunc_end4:
	.size	cont_TermEqualModuloBindings, .Lfunc_end4-cont_TermEqualModuloBindings
	.cfi_endproc

	.globl	cont_CopyAndApplyBindings
	.p2align	4, 0x90
	.type	cont_CopyAndApplyBindings,@function
cont_CopyAndApplyBindings:              # @cont_CopyAndApplyBindings
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 48
.Lcfi40:
	.cfi_offset %rbx, -40
.Lcfi41:
	.cfi_offset %r12, -32
.Lcfi42:
	.cfi_offset %r14, -24
.Lcfi43:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.LBB5_4
	.p2align	4, 0x90
.LBB5_1:                                # %.lr.ph45
                                        # =>This Inner Loop Header: Depth=1
	cltq
	shlq	$5, %rax
	movq	8(%r12,%rax), %r15
	testq	%r15, %r15
	je	.LBB5_4
# BB#2:                                 # %.thread
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	16(%r12,%rax), %r12
	movl	(%r15), %eax
	testl	%eax, %eax
	movq	%r15, %rsi
	jg	.LBB5_1
	jmp	.LBB5_5
.LBB5_4:
	movq	%rsi, %r15
.LBB5_5:                                # %._crit_edge
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_10
# BB#6:
	callq	list_Copy
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB5_10
# BB#7:                                 # %.lr.ph.preheader
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB5_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	callq	cont_CopyAndApplyBindings
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB5_8
	jmp	.LBB5_11
.LBB5_10:
	xorl	%r14d, %r14d
.LBB5_11:                               # %.loopexit
	movl	(%r15), %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	term_Create             # TAILCALL
.Lfunc_end5:
	.size	cont_CopyAndApplyBindings, .Lfunc_end5-cont_CopyAndApplyBindings
	.cfi_endproc

	.globl	cont_CopyAndApplyBindingsCom
	.p2align	4, 0x90
	.type	cont_CopyAndApplyBindingsCom,@function
cont_CopyAndApplyBindingsCom:           # @cont_CopyAndApplyBindingsCom
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi48:
	.cfi_def_cfa_offset 48
.Lcfi49:
	.cfi_offset %rbx, -40
.Lcfi50:
	.cfi_offset %r12, -32
.Lcfi51:
	.cfi_offset %r14, -24
.Lcfi52:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rsi, %r12
	movslq	(%r12), %rax
	testq	%rax, %rax
	jle	.LBB6_3
# BB#2:                                 #   in Loop: Header=BB6_1 Depth=1
	shlq	$5, %rax
	movq	8(%r15,%rax), %rsi
	testq	%rsi, %rsi
	jne	.LBB6_1
.LBB6_3:                                # %.critedge
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB6_8
# BB#4:
	callq	list_Copy
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB6_8
# BB#5:                                 # %.lr.ph.preheader
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB6_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	callq	cont_CopyAndApplyBindingsCom
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB6_6
	jmp	.LBB6_9
.LBB6_8:
	xorl	%r14d, %r14d
.LBB6_9:                                # %.loopexit
	movl	(%r12), %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	term_Create             # TAILCALL
.Lfunc_end6:
	.size	cont_CopyAndApplyBindingsCom, .Lfunc_end6-cont_CopyAndApplyBindingsCom
	.cfi_endproc

	.globl	cont_ApplyBindingsModuloMatching
	.p2align	4, 0x90
	.type	cont_ApplyBindingsModuloMatching,@function
cont_ApplyBindingsModuloMatching:       # @cont_ApplyBindingsModuloMatching
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi57:
	.cfi_def_cfa_offset 48
.Lcfi58:
	.cfi_offset %rbx, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movslq	(%r14), %rax
	testq	%rax, %rax
	jle	.LBB7_3
# BB#1:
	shlq	$5, %rax
	movq	8(%rbx,%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB7_6
# BB#2:
	movq	16(%rbp), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, %rbx
	movl	(%rbp), %eax
	movl	%eax, (%r14)
	movq	16(%r14), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	%rbx, 16(%r14)
	jmp	.LBB7_6
.LBB7_3:
	movq	16(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB7_6
	.p2align	4, 0x90
.LBB7_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rsi
	movq	%rbx, %rdi
	movl	%r15d, %edx
	callq	cont_ApplyBindingsModuloMatching
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB7_4
.LBB7_6:                                # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	cont_ApplyBindingsModuloMatching, .Lfunc_end7-cont_ApplyBindingsModuloMatching
	.cfi_endproc

	.globl	cont_ApplyBindingsModuloMatchingReverse
	.p2align	4, 0x90
	.type	cont_ApplyBindingsModuloMatchingReverse,@function
cont_ApplyBindingsModuloMatchingReverse: # @cont_ApplyBindingsModuloMatchingReverse
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 32
.Lcfi65:
	.cfi_offset %rbx, -32
.Lcfi66:
	.cfi_offset %r14, -24
.Lcfi67:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	(%r14), %rax
	testq	%rax, %rax
	jle	.LBB8_3
# BB#1:
	shlq	$5, %rax
	movq	8(%r15,%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB8_6
# BB#2:
	movq	%r15, %rdi
	callq	cont_CopyAndApplyIndexVariableBindings
	movq	%rax, %rbx
	movl	(%rbx), %eax
	movl	%eax, (%r14)
	movq	16(%r14), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	16(%rbx), %rax
	movq	%rax, 16(%r14)
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rbx, (%rax)
	jmp	.LBB8_6
.LBB8_3:
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB8_6
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	callq	cont_ApplyBindingsModuloMatchingReverse
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_4
.LBB8_6:                                # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	cont_ApplyBindingsModuloMatchingReverse, .Lfunc_end8-cont_ApplyBindingsModuloMatchingReverse
	.cfi_endproc

	.p2align	4, 0x90
	.type	cont_CopyAndApplyIndexVariableBindings,@function
cont_CopyAndApplyIndexVariableBindings: # @cont_CopyAndApplyIndexVariableBindings
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi72:
	.cfi_def_cfa_offset 48
.Lcfi73:
	.cfi_offset %rbx, -40
.Lcfi74:
	.cfi_offset %r12, -32
.Lcfi75:
	.cfi_offset %r14, -24
.Lcfi76:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	jmp	.LBB9_3
	.p2align	4, 0x90
.LBB9_1:                                # %.sink.split.split.us.preheader
                                        #   in Loop: Header=BB9_3 Depth=1
	movslq	%r12d, %rax
	shlq	$5, %rax
	movq	8(%r14,%rax), %rax
	.p2align	4, 0x90
.LBB9_2:                                # %.sink.split.split.us
                                        #   Parent Loop BB9_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rsi
	testq	%rsi, %rsi
	movl	$0, %eax
	je	.LBB9_2
.LBB9_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_2 Depth 2
	movl	(%rsi), %r12d
	leal	-2001(%r12), %eax
	cmpl	$999, %eax              # imm = 0x3E7
	jbe	.LBB9_1
# BB#4:                                 # %.us-lcssa
	movq	16(%rsi), %rdi
	testq	%rdi, %rdi
	je	.LBB9_9
# BB#5:
	callq	list_Copy
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB9_9
# BB#6:                                 # %.lr.ph.preheader
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB9_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	cont_CopyAndApplyIndexVariableBindings
	movq	%rax, 8(%rbx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB9_7
	jmp	.LBB9_10
.LBB9_9:
	xorl	%r15d, %r15d
.LBB9_10:                               # %.loopexit
	movl	%r12d, %edi
	movq	%r15, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	term_Create             # TAILCALL
.Lfunc_end9:
	.size	cont_CopyAndApplyIndexVariableBindings, .Lfunc_end9-cont_CopyAndApplyIndexVariableBindings
	.cfi_endproc

	.globl	cont_BindingsAreRenamingModuloMatching
	.p2align	4, 0x90
	.type	cont_BindingsAreRenamingModuloMatching,@function
cont_BindingsAreRenamingModuloMatching: # @cont_BindingsAreRenamingModuloMatching
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 24
.Lcfi79:
	.cfi_offset %rbx, -24
.Lcfi80:
	.cfi_offset %r14, -16
	movl	cont_BINDINGS(%rip), %eax
	movl	cont_STACKPOINTER(%rip), %ecx
	leal	1(%rcx), %r8d
	movl	%r8d, cont_STACKPOINTER(%rip)
	movslq	%ecx, %r9
	movl	%eax, cont_STACK(,%r9,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rsi
	testq	%rsi, %rsi
	je	.LBB10_17
# BB#1:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	movl	$-2001, %r10d           # imm = 0xF82F
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB10_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %ecx
	addl	%r10d, %ecx
	cmpl	$1000, %ecx             # imm = 0x3E8
	jb	.LBB10_13
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	8(%rsi), %rcx
	movslq	(%rcx), %rdx
	testq	%rdx, %rdx
	jle	.LBB10_9
# BB#4:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	%rdx, %rcx
	shlq	$5, %rcx
	cmpl	$0, 4(%rdi,%rcx)
	jne	.LBB10_5
# BB#12:                                # %.critedge18
                                        #   in Loop: Header=BB10_2 Depth=1
	leaq	4(%rdi,%rcx), %r11
	leaq	(%rdi,%rcx), %r14
	movq	%r14, cont_CURRENTBINDING(%rip)
	movups	%xmm0, 8(%rdi,%rcx)
	movq	cont_LASTBINDING(%rip), %rbx
	movq	%rbx, 24(%rdi,%rcx)
	movq	%r14, cont_LASTBINDING(%rip)
	incl	%eax
	movl	%eax, cont_BINDINGS(%rip)
	movl	%edx, (%r11)
.LBB10_13:                              #   in Loop: Header=BB10_2 Depth=1
	movq	24(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB10_2
# BB#14:                                # %._crit_edge
	testl	%eax, %eax
	jle	.LBB10_17
# BB#15:                                # %.lr.ph.i.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB10_16:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB10_16
.LBB10_17:                              # %._crit_edge.i
	movl	$1, %eax
	testl	%r8d, %r8d
	jne	.LBB10_19
	jmp	.LBB10_20
.LBB10_9:
	testl	%eax, %eax
	jle	.LBB10_8
# BB#10:                                # %.lr.ph.i24.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB10_11:                              # %.lr.ph.i24
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB10_11
	jmp	.LBB10_8
.LBB10_5:                               # %.critedge
	testl	%eax, %eax
	jle	.LBB10_8
# BB#6:                                 # %.lr.ph.i30.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB10_7:                               # %.lr.ph.i30
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB10_7
.LBB10_8:                               # %._crit_edge.i31
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.LBB10_20
.LBB10_19:
	movl	%r9d, cont_STACKPOINTER(%rip)
	movl	cont_STACK(,%r9,4), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
.LBB10_20:                              # %cont_BackTrack.exit32
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	cont_BindingsAreRenamingModuloMatching, .Lfunc_end10-cont_BindingsAreRenamingModuloMatching
	.cfi_endproc

	.globl	cont_TermMaxVar
	.p2align	4, 0x90
	.type	cont_TermMaxVar,@function
cont_TermMaxVar:                        # @cont_TermMaxVar
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 32
.Lcfi84:
	.cfi_offset %rbx, -32
.Lcfi85:
	.cfi_offset %r14, -24
.Lcfi86:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	(%rsi), %r15d
	testl	%r15d, %r15d
	jle	.LBB11_6
# BB#1:                                 # %.lr.ph.i.preheader
	movq	cont_INSTANCECONTEXT(%rip), %rax
	.p2align	4, 0x90
.LBB11_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rax, %r14
	je	.LBB11_5
# BB#3:                                 #   in Loop: Header=BB11_2 Depth=1
	movslq	%r15d, %rdx
	shlq	$5, %rdx
	movq	8(%r14,%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB11_5
# BB#4:                                 # %.thread.i
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	16(%r14,%rdx), %r14
	movl	(%rcx), %r15d
	testl	%r15d, %r15d
	movq	%rcx, %rsi
	jg	.LBB11_2
.LBB11_5:                               # %cont_Deref.exit.loopexit
	movl	(%rsi), %r15d
.LBB11_6:                               # %cont_Deref.exit
	leal	-1(%r15), %eax
	cmpl	$1999, %eax             # imm = 0x7CF
	jbe	.LBB11_10
# BB#7:
	movq	16(%rsi), %rbx
	xorl	%r15d, %r15d
	testq	%rbx, %rbx
	je	.LBB11_10
	.p2align	4, 0x90
.LBB11_8:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	cont_TermMaxVar
	cmpl	%r15d, %eax
	cmovgel	%eax, %r15d
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB11_8
.LBB11_10:                              # %._crit_edge
	movl	%r15d, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	cont_TermMaxVar, .Lfunc_end11-cont_TermMaxVar
	.cfi_endproc

	.globl	cont_TermSize
	.p2align	4, 0x90
	.type	cont_TermSize,@function
cont_TermSize:                          # @cont_TermSize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 32
.Lcfi90:
	.cfi_offset %rbx, -32
.Lcfi91:
	.cfi_offset %r14, -24
.Lcfi92:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jle	.LBB12_5
# BB#1:                                 # %.lr.ph.i.preheader
	movq	cont_INSTANCECONTEXT(%rip), %rax
	.p2align	4, 0x90
.LBB12_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rax, %r14
	je	.LBB12_5
# BB#3:                                 #   in Loop: Header=BB12_2 Depth=1
	movslq	%ecx, %rcx
	shlq	$5, %rcx
	movq	8(%r14,%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB12_5
# BB#4:                                 # %.thread.i
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	16(%r14,%rcx), %r14
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	movq	%rdx, %rsi
	jg	.LBB12_2
.LBB12_5:                               # %cont_Deref.exit
	movq	16(%rsi), %rbx
	movl	$1, %ebp
	testq	%rbx, %rbx
	je	.LBB12_8
	.p2align	4, 0x90
.LBB12_6:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	cont_TermSize
	addl	%eax, %ebp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB12_6
.LBB12_8:                               # %._crit_edge
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end12:
	.size	cont_TermSize, .Lfunc_end12-cont_TermSize
	.cfi_endproc

	.globl	cont_TermContainsSymbol
	.p2align	4, 0x90
	.type	cont_TermContainsSymbol,@function
cont_TermContainsSymbol:                # @cont_TermContainsSymbol
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi97:
	.cfi_def_cfa_offset 48
.Lcfi98:
	.cfi_offset %rbx, -40
.Lcfi99:
	.cfi_offset %r14, -32
.Lcfi100:
	.cfi_offset %r15, -24
.Lcfi101:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rdi, %rbx
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.LBB13_6
# BB#1:                                 # %.lr.ph.i.preheader
	movq	cont_INSTANCECONTEXT(%rip), %rcx
	.p2align	4, 0x90
.LBB13_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rcx, %rbx
	je	.LBB13_5
# BB#3:                                 #   in Loop: Header=BB13_2 Depth=1
	cltq
	shlq	$5, %rax
	movq	8(%rbx,%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB13_5
# BB#4:                                 # %.thread.i
                                        #   in Loop: Header=BB13_2 Depth=1
	movq	16(%rbx,%rax), %rbx
	movl	(%rdx), %eax
	testl	%eax, %eax
	movq	%rdx, %rsi
	jg	.LBB13_2
.LBB13_5:                               # %cont_Deref.exit.loopexit
	movl	(%rsi), %eax
.LBB13_6:                               # %cont_Deref.exit
	movl	$1, %r14d
	cmpl	%r15d, %eax
	je	.LBB13_12
# BB#7:
	movq	16(%rsi), %rbp
	testq	%rbp, %rbp
	je	.LBB13_11
	.p2align	4, 0x90
.LBB13_8:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rsi
	movq	%rbx, %rdi
	movl	%r15d, %edx
	callq	cont_TermContainsSymbol
	testl	%eax, %eax
	jne	.LBB13_12
# BB#9:                                 #   in Loop: Header=BB13_8 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB13_8
.LBB13_11:
	xorl	%r14d, %r14d
.LBB13_12:                              # %.loopexit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	cont_TermContainsSymbol, .Lfunc_end13-cont_TermContainsSymbol
	.cfi_endproc

	.globl	cont_TermPrintPrefix
	.p2align	4, 0x90
	.type	cont_TermPrintPrefix,@function
cont_TermPrintPrefix:                   # @cont_TermPrintPrefix
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 32
.Lcfi105:
	.cfi_offset %rbx, -32
.Lcfi106:
	.cfi_offset %r14, -24
.Lcfi107:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	(%r14), %edi
	testl	%edi, %edi
	jle	.LBB14_6
# BB#1:                                 # %.lr.ph.i.preheader
	movq	cont_INSTANCECONTEXT(%rip), %rax
	.p2align	4, 0x90
.LBB14_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rax, %r15
	je	.LBB14_5
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	movslq	%edi, %rdx
	shlq	$5, %rdx
	movq	8(%r15,%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB14_5
# BB#4:                                 # %.thread.i
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	16(%r15,%rdx), %r15
	movl	(%rcx), %edi
	testl	%edi, %edi
	movq	%rcx, %r14
	jg	.LBB14_2
.LBB14_5:                               # %cont_Deref.exit.loopexit
	movl	(%r14), %edi
.LBB14_6:                               # %cont_Deref.exit
	callq	symbol_Print
	cmpq	$0, 16(%r14)
	je	.LBB14_12
# BB#7:
	movq	stdout(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB14_9
	jmp	.LBB14_11
	.p2align	4, 0x90
.LBB14_10:                              # %.backedge
                                        #   in Loop: Header=BB14_9 Depth=1
	movq	stdout(%rip), %rsi
	movl	$44, %edi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB14_11
.LBB14_9:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	callq	cont_TermPrintPrefix
	cmpq	$0, (%rbx)
	jne	.LBB14_10
.LBB14_11:                              # %._crit_edge
	movq	stdout(%rip), %rsi
	movl	$41, %edi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_IO_putc                # TAILCALL
.LBB14_12:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	cont_TermPrintPrefix, .Lfunc_end14-cont_TermPrintPrefix
	.cfi_endproc

	.type	cont_LASTBINDING,@object # @cont_LASTBINDING
	.comm	cont_LASTBINDING,8,8
	.type	cont_NOOFCONTEXTS,@object # @cont_NOOFCONTEXTS
	.comm	cont_NOOFCONTEXTS,4,4
	.type	cont_LISTOFCONTEXTS,@object # @cont_LISTOFCONTEXTS
	.comm	cont_LISTOFCONTEXTS,8,8
	.type	cont_BINDINGS,@object   # @cont_BINDINGS
	.comm	cont_BINDINGS,4,4
	.type	cont_INSTANCECONTEXT,@object # @cont_INSTANCECONTEXT
	.comm	cont_INSTANCECONTEXT,8,8
	.type	cont_LEFTCONTEXT,@object # @cont_LEFTCONTEXT
	.comm	cont_LEFTCONTEXT,8,8
	.type	cont_RIGHTCONTEXT,@object # @cont_RIGHTCONTEXT
	.comm	cont_RIGHTCONTEXT,8,8
	.type	cont_INDEXVARSCANNER,@object # @cont_INDEXVARSCANNER
	.comm	cont_INDEXVARSCANNER,4,4
	.type	cont_CURRENTBINDING,@object # @cont_CURRENTBINDING
	.comm	cont_CURRENTBINDING,8,8
	.type	cont_STACK,@object      # @cont_STACK
	.comm	cont_STACK,4000,16
	.type	cont_STACKPOINTER,@object # @cont_STACKPOINTER
	.comm	cont_STACKPOINTER,4,4
	.type	cont_CHECKSTACK,@object # @cont_CHECKSTACK
	.comm	cont_CHECKSTACK,8000,16
	.type	cont_CHECKSTACKPOINTER,@object # @cont_CHECKSTACKPOINTER
	.comm	cont_CHECKSTACKPOINTER,4,4
	.type	cont_STATELASTBINDING,@object # @cont_STATELASTBINDING
	.comm	cont_STATELASTBINDING,8,8
	.type	cont_STATEBINDINGS,@object # @cont_STATEBINDINGS
	.comm	cont_STATEBINDINGS,4,4
	.type	cont_STATESTACK,@object # @cont_STATESTACK
	.comm	cont_STATESTACK,4,4
	.type	cont_STATETOPSTACK,@object # @cont_STATETOPSTACK
	.comm	cont_STATETOPSTACK,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
