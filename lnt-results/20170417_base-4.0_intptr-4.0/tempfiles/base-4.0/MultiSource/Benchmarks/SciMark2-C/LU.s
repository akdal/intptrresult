	.text
	.file	"LU.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4613937818241073152     # double 3
	.text
	.globl	LU_num_flops
	.p2align	4, 0x90
	.type	LU_num_flops,@function
LU_num_flops:                           # @LU_num_flops
	.cfi_startproc
# BB#0:
	cvtsi2sdl	%edi, %xmm1
	movapd	%xmm1, %xmm0
	addsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	%xmm1, %xmm0
	divsd	.LCPI0_0(%rip), %xmm0
	retq
.Lfunc_end0:
	.size	LU_num_flops, .Lfunc_end0-LU_num_flops
	.cfi_endproc

	.globl	LU_copy_matrix
	.p2align	4, 0x90
	.type	LU_copy_matrix,@function
LU_copy_matrix:                         # @LU_copy_matrix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	testl	%edi, %edi
	jle	.LBB1_20
# BB#1:
	testl	%esi, %esi
	jle	.LBB1_20
# BB#2:                                 # %.preheader.us.preheader
	movl	%esi, %r14d
	movl	%edi, %r11d
	leaq	-1(%r14), %r8
	movl	%esi, %r9d
	andl	$3, %r9d
	movq	%r14, %r10
	subq	%r9, %r10
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_11 Depth 2
                                        #     Child Loop BB1_15 Depth 2
                                        #     Child Loop BB1_18 Depth 2
	cmpl	$4, %esi
	movq	(%rcx,%r15,8), %r12
	movq	(%rdx,%r15,8), %rbp
	jae	.LBB1_5
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=1
	xorl	%edi, %edi
	jmp	.LBB1_13
	.p2align	4, 0x90
.LBB1_5:                                # %min.iters.checked
                                        #   in Loop: Header=BB1_3 Depth=1
	testq	%r10, %r10
	je	.LBB1_9
# BB#6:                                 # %vector.memcheck
                                        #   in Loop: Header=BB1_3 Depth=1
	leaq	(%r12,%r14,8), %rax
	cmpq	%rax, %rbp
	jae	.LBB1_10
# BB#7:                                 # %vector.memcheck
                                        #   in Loop: Header=BB1_3 Depth=1
	leaq	(%rbp,%r14,8), %rax
	cmpq	%rax, %r12
	jae	.LBB1_10
.LBB1_9:                                #   in Loop: Header=BB1_3 Depth=1
	xorl	%edi, %edi
	jmp	.LBB1_13
.LBB1_10:                               # %vector.body.preheader
                                        #   in Loop: Header=BB1_3 Depth=1
	leaq	16(%r12), %rdi
	leaq	16(%rbp), %rbx
	movq	%r10, %rax
	.p2align	4, 0x90
.LBB1_11:                               # %vector.body
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	addq	$32, %rdi
	addq	$32, %rbx
	addq	$-4, %rax
	jne	.LBB1_11
# BB#12:                                # %middle.block
                                        #   in Loop: Header=BB1_3 Depth=1
	testl	%r9d, %r9d
	movq	%r10, %rdi
	je	.LBB1_19
	.p2align	4, 0x90
.LBB1_13:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB1_3 Depth=1
	movl	%r14d, %ebx
	subl	%edi, %ebx
	movq	%r8, %r13
	subq	%rdi, %r13
	andq	$7, %rbx
	je	.LBB1_16
# BB#14:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB1_3 Depth=1
	negq	%rbx
	.p2align	4, 0x90
.LBB1_15:                               # %scalar.ph.prol
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%rdi,8), %rax
	movq	%rax, (%rbp,%rdi,8)
	incq	%rdi
	incq	%rbx
	jne	.LBB1_15
.LBB1_16:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB1_3 Depth=1
	cmpq	$7, %r13
	jb	.LBB1_19
# BB#17:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	%r14, %rbx
	subq	%rdi, %rbx
	leaq	56(%rbp,%rdi,8), %rax
	leaq	56(%r12,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB1_18:                               # %scalar.ph
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-56(%rdi), %rbp
	movq	%rbp, -56(%rax)
	movq	-48(%rdi), %rbp
	movq	%rbp, -48(%rax)
	movq	-40(%rdi), %rbp
	movq	%rbp, -40(%rax)
	movq	-32(%rdi), %rbp
	movq	%rbp, -32(%rax)
	movq	-24(%rdi), %rbp
	movq	%rbp, -24(%rax)
	movq	-16(%rdi), %rbp
	movq	%rbp, -16(%rax)
	movq	-8(%rdi), %rbp
	movq	%rbp, -8(%rax)
	movq	(%rdi), %rbp
	movq	%rbp, (%rax)
	addq	$64, %rax
	addq	$64, %rdi
	addq	$-8, %rbx
	jne	.LBB1_18
.LBB1_19:                               # %._crit_edge.us
                                        #   in Loop: Header=BB1_3 Depth=1
	incq	%r15
	cmpq	%r11, %r15
	jne	.LBB1_3
.LBB1_20:                               # %._crit_edge17
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	LU_copy_matrix, .Lfunc_end1-LU_copy_matrix
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_1:
	.quad	4607182418800017408     # double 1
	.text
	.globl	LU_factor
	.p2align	4, 0x90
	.type	LU_factor,@function
LU_factor:                              # @LU_factor
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi18:
	.cfi_def_cfa_offset 96
.Lcfi19:
	.cfi_offset %rbx, -56
.Lcfi20:
	.cfi_offset %r12, -48
.Lcfi21:
	.cfi_offset %r13, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movq	%rcx, 8(%rsp)           # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	cmpl	%esi, %edi
	movl	%esi, %eax
	cmovlel	%edi, %eax
	testl	%eax, %eax
	jle	.LBB2_1
# BB#3:                                 # %.lr.ph126
	leal	-1(%rdi), %ecx
	leal	-1(%rax), %ebp
	movslq	%ebp, %rbp
	movq	%rbp, -40(%rsp)         # 8-byte Spill
	movslq	%ecx, %rcx
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	movslq	%edi, %rcx
	movq	%rcx, -104(%rsp)        # 8-byte Spill
	movslq	%esi, %rcx
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	cltq
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movl	%edi, %r15d
	movl	%esi, %ecx
	movq	%rcx, %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	leaq	-1(%rcx), %rax
	leaq	-1(%r15), %rcx
	movq	%rcx, -88(%rsp)         # 8-byte Spill
	leaq	-2(%r15), %rcx
	movq	%rcx, -96(%rsp)         # 8-byte Spill
	addb	$3, %dil
	leaq	24(%rdx), %rcx
	movq	%rcx, -56(%rsp)         # 8-byte Spill
	movl	$1, %r13d
	xorl	%r10d, %r10d
	movl	$7, %ecx
	movq	%rcx, -112(%rsp)        # 8-byte Spill
	movapd	.LCPI2_0(%rip), %xmm9   # xmm9 = [nan,nan]
	movapd	.LCPI2_0(%rip), %xmm1   # xmm1 = [nan,nan]
	xorps	%xmm2, %xmm2
	movsd	.LCPI2_1(%rip), %xmm8   # xmm8 = mem[0],zero
	movq	%rax, -64(%rsp)         # 8-byte Spill
	movq	%rax, -80(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_10 Depth 2
                                        #     Child Loop BB2_19 Depth 2
                                        #     Child Loop BB2_23 Depth 2
                                        #     Child Loop BB2_29 Depth 2
                                        #       Child Loop BB2_38 Depth 3
                                        #       Child Loop BB2_42 Depth 3
                                        #       Child Loop BB2_45 Depth 3
	leaq	1(%r10), %rbx
	movq	(%rdx,%r10,8), %rax
	cmpq	-104(%rsp), %rbx        # 8-byte Folded Reload
	jge	.LBB2_5
# BB#6:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	-88(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	subl	%r10d, %ecx
	movsd	(%rax,%r10,8), %xmm4    # xmm4 = mem[0],zero
	andpd	%xmm9, %xmm4
	testb	$1, %cl
	jne	.LBB2_8
# BB#7:                                 #   in Loop: Header=BB2_4 Depth=1
	movq	%r13, %rbp
	movl	%r10d, %ecx
	cmpq	%r10, -96(%rsp)         # 8-byte Folded Reload
	jne	.LBB2_10
	jmp	.LBB2_11
	.p2align	4, 0x90
.LBB2_5:                                #   in Loop: Header=BB2_4 Depth=1
	movl	%r10d, %ecx
	jmp	.LBB2_11
	.p2align	4, 0x90
.LBB2_8:                                # %.lr.ph.prol
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	(%rdx,%r13,8), %rcx
	movsd	(%rcx,%r10,8), %xmm5    # xmm5 = mem[0],zero
	andpd	%xmm9, %xmm5
	ucomisd	%xmm4, %xmm5
	maxsd	%xmm4, %xmm5
	movl	%r10d, %ecx
	cmoval	%r13d, %ecx
	leaq	1(%r13), %rbp
	movapd	%xmm5, %xmm4
	cmpq	%r10, -96(%rsp)         # 8-byte Folded Reload
	je	.LBB2_11
	.p2align	4, 0x90
.LBB2_10:                               # %.lr.ph
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rbp,8), %rsi
	movsd	(%rsi,%r10,8), %xmm5    # xmm5 = mem[0],zero
	andpd	%xmm1, %xmm5
	ucomisd	%xmm4, %xmm5
	maxsd	%xmm4, %xmm5
	cmoval	%ebp, %ecx
	movq	8(%rdx,%rbp,8), %rsi
	movsd	(%rsi,%r10,8), %xmm4    # xmm4 = mem[0],zero
	andpd	%xmm1, %xmm4
	leal	1(%rbp), %esi
	ucomisd	%xmm5, %xmm4
	maxsd	%xmm5, %xmm4
	cmoval	%esi, %ecx
	addq	$2, %rbp
	cmpq	%r15, %rbp
	jne	.LBB2_10
.LBB2_11:                               # %._crit_edge
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%ecx, (%rsi,%r10,4)
	movslq	%ecx, %rsi
	movq	(%rdx,%rsi,8), %rbp
	movsd	(%rbp,%r10,8), %xmm4    # xmm4 = mem[0],zero
	ucomisd	%xmm2, %xmm4
	jne	.LBB2_13
	jnp	.LBB2_12
.LBB2_13:                               #   in Loop: Header=BB2_4 Depth=1
	movq	%rdi, -72(%rsp)         # 8-byte Spill
	movl	%ecx, %ecx
	cmpq	%r10, %rcx
	je	.LBB2_15
# BB#14:                                #   in Loop: Header=BB2_4 Depth=1
	movq	%rbp, (%rdx,%r10,8)
	movq	%rax, (%rdx,%rsi,8)
.LBB2_15:                               #   in Loop: Header=BB2_4 Depth=1
	cmpq	-24(%rsp), %r10         # 8-byte Folded Reload
	movq	%rbx, -128(%rsp)        # 8-byte Spill
	jge	.LBB2_24
# BB#16:                                #   in Loop: Header=BB2_4 Depth=1
	cmpq	-104(%rsp), %rbx        # 8-byte Folded Reload
	jge	.LBB2_26
# BB#17:                                # %.lr.ph117.preheader
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	-88(%rsp), %rcx         # 8-byte Reload
	subq	%r10, %rcx
	movq	-96(%rsp), %rax         # 8-byte Reload
	subq	%r10, %rax
	movq	(%rdx,%r10,8), %rsi
	movapd	%xmm8, %xmm4
	divsd	(%rsi,%r10,8), %xmm4
	testb	$3, %cl
	movq	%r13, %rcx
	je	.LBB2_21
# BB#18:                                # %.lr.ph117.prol.preheader
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	-72(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	andb	$3, %cl
	movzbl	%cl, %ecx
	negq	%rcx
	movq	%rdx, %rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_19:                               # %.lr.ph117.prol
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%r13,8), %rbp
	movsd	(%rbp,%r10,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm4, %xmm5
	movsd	%xmm5, (%rbp,%r10,8)
	decq	%rsi
	addq	$8, %rdi
	cmpq	%rsi, %rcx
	jne	.LBB2_19
# BB#20:                                # %.lr.ph117.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	%r13, %rcx
	subq	%rsi, %rcx
.LBB2_21:                               # %.lr.ph117.prol.loopexit
                                        #   in Loop: Header=BB2_4 Depth=1
	cmpq	$3, %rax
	jb	.LBB2_24
# BB#22:                                # %.lr.ph117.preheader.new
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	%r15, %rax
	subq	%rcx, %rax
	movq	-56(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB2_23:                               # %.lr.ph117
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rcx), %rsi
	movsd	(%rsi,%r10,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm4, %xmm5
	movsd	%xmm5, (%rsi,%r10,8)
	movq	-16(%rcx), %rsi
	movsd	(%rsi,%r10,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm4, %xmm5
	movsd	%xmm5, (%rsi,%r10,8)
	movq	-8(%rcx), %rsi
	movsd	(%rsi,%r10,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm4, %xmm5
	movsd	%xmm5, (%rsi,%r10,8)
	movq	(%rcx), %rsi
	movsd	(%rsi,%r10,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm4, %xmm5
	movsd	%xmm5, (%rsi,%r10,8)
	addq	$32, %rcx
	addq	$-4, %rax
	jne	.LBB2_23
	.p2align	4, 0x90
.LBB2_24:                               # %.loopexit111
                                        #   in Loop: Header=BB2_4 Depth=1
	cmpq	-40(%rsp), %r10         # 8-byte Folded Reload
	jge	.LBB2_26
# BB#25:                                # %.loopexit111
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	-128(%rsp), %rax        # 8-byte Reload
	cmpq	-104(%rsp), %rax        # 8-byte Folded Reload
	jge	.LBB2_26
# BB#27:                                # %.lr.ph123
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	-128(%rsp), %rax        # 8-byte Reload
	cmpq	-48(%rsp), %rax         # 8-byte Folded Reload
	jge	.LBB2_26
# BB#28:                                # %.lr.ph123.split.us.preheader
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	-64(%rsp), %rax         # 8-byte Reload
	subq	%r10, %rax
	leaq	-4(%rax), %rsi
	movq	-80(%rsp), %r8          # 8-byte Reload
	andq	$-4, %r8
	shrq	$2, %rsi
	movq	(%rdx,%r10,8), %r14
	movq	-128(%rsp), %rcx        # 8-byte Reload
	leaq	(%r14,%rcx,8), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	-120(%rsp), %rcx        # 8-byte Reload
	leaq	(%r14,%rcx,8), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	andq	$-4, %r9
	leaq	(%r13,%r9), %rcx
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	movq	%rsi, (%rsp)            # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	andl	$1, %esi
	movq	%rsi, -16(%rsp)         # 8-byte Spill
	movq	-112(%rsp), %rcx        # 8-byte Reload
	leaq	(%r14,%rcx,8), %rcx
	leaq	24(%r14), %rsi
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB2_29:                               # %.lr.ph123.split.us
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_38 Depth 3
                                        #       Child Loop BB2_42 Depth 3
                                        #       Child Loop BB2_45 Depth 3
	cmpq	$4, %rax
	movq	(%rdx,%rbp,8), %r12
	movsd	(%r12,%r10,8), %xmm4    # xmm4 = mem[0],zero
	movq	%r13, %r11
	jb	.LBB2_40
# BB#30:                                # %min.iters.checked
                                        #   in Loop: Header=BB2_29 Depth=2
	testq	%r9, %r9
	movq	%r13, %r11
	je	.LBB2_40
# BB#31:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_29 Depth=2
	movq	-128(%rsp), %rsi        # 8-byte Reload
	leaq	(%r12,%rsi,8), %rsi
	cmpq	24(%rsp), %rsi          # 8-byte Folded Reload
	jae	.LBB2_33
# BB#32:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_29 Depth=2
	movq	-120(%rsp), %rsi        # 8-byte Reload
	leaq	(%r12,%rsi,8), %rsi
	cmpq	%rsi, 16(%rsp)          # 8-byte Folded Reload
	movq	%r13, %r11
	jb	.LBB2_40
.LBB2_33:                               # %vector.ph
                                        #   in Loop: Header=BB2_29 Depth=2
	movaps	%xmm4, %xmm5
	movlhps	%xmm5, %xmm5            # xmm5 = xmm5[0,0]
	cmpq	$0, -16(%rsp)           # 8-byte Folded Reload
	jne	.LBB2_34
# BB#35:                                # %vector.body.prol
                                        #   in Loop: Header=BB2_29 Depth=2
	movupd	(%r14,%r13,8), %xmm6
	movupd	16(%r14,%r13,8), %xmm7
	mulpd	%xmm5, %xmm6
	mulpd	%xmm5, %xmm7
	movupd	(%r12,%r13,8), %xmm3
	movupd	16(%r12,%r13,8), %xmm0
	subpd	%xmm6, %xmm3
	subpd	%xmm7, %xmm0
	movupd	%xmm3, (%r12,%r13,8)
	movupd	%xmm0, 16(%r12,%r13,8)
	movl	$4, %esi
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	jne	.LBB2_37
	jmp	.LBB2_39
.LBB2_34:                               #   in Loop: Header=BB2_29 Depth=2
	xorl	%esi, %esi
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB2_39
.LBB2_37:                               # %vector.ph.new
                                        #   in Loop: Header=BB2_29 Depth=2
	movq	-112(%rsp), %rdi        # 8-byte Reload
	leaq	(%r12,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB2_38:                               # %vector.body
                                        #   Parent Loop BB2_4 Depth=1
                                        #     Parent Loop BB2_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	-48(%rcx,%rsi,8), %xmm0
	movupd	-32(%rcx,%rsi,8), %xmm3
	mulpd	%xmm5, %xmm0
	mulpd	%xmm5, %xmm3
	movupd	-48(%rdi,%rsi,8), %xmm6
	movupd	-32(%rdi,%rsi,8), %xmm7
	subpd	%xmm0, %xmm6
	subpd	%xmm3, %xmm7
	movupd	%xmm6, -48(%rdi,%rsi,8)
	movupd	%xmm7, -32(%rdi,%rsi,8)
	movupd	-16(%rcx,%rsi,8), %xmm0
	movupd	(%rcx,%rsi,8), %xmm3
	mulpd	%xmm5, %xmm0
	mulpd	%xmm5, %xmm3
	movupd	-16(%rdi,%rsi,8), %xmm6
	movupd	(%rdi,%rsi,8), %xmm7
	subpd	%xmm0, %xmm6
	subpd	%xmm3, %xmm7
	movupd	%xmm6, -16(%rdi,%rsi,8)
	movupd	%xmm7, (%rdi,%rsi,8)
	addq	$8, %rsi
	cmpq	%rsi, %r8
	jne	.LBB2_38
.LBB2_39:                               # %middle.block
                                        #   in Loop: Header=BB2_29 Depth=2
	cmpq	%r9, %rax
	movq	-8(%rsp), %r11          # 8-byte Reload
	je	.LBB2_46
	.p2align	4, 0x90
.LBB2_40:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB2_29 Depth=2
	movq	-120(%rsp), %rdi        # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	subl	%r11d, %edi
	movq	-64(%rsp), %rsi         # 8-byte Reload
	subq	%r11, %rsi
	andq	$3, %rdi
	je	.LBB2_43
# BB#41:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB2_29 Depth=2
	negq	%rdi
	.p2align	4, 0x90
.LBB2_42:                               # %scalar.ph.prol
                                        #   Parent Loop BB2_4 Depth=1
                                        #     Parent Loop BB2_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%r14,%r11,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	movsd	(%r12,%r11,8), %xmm3    # xmm3 = mem[0],zero
	subsd	%xmm0, %xmm3
	movsd	%xmm3, (%r12,%r11,8)
	incq	%r11
	incq	%rdi
	jne	.LBB2_42
.LBB2_43:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB2_29 Depth=2
	cmpq	$3, %rsi
	jb	.LBB2_46
# BB#44:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB2_29 Depth=2
	movq	-120(%rsp), %rdi        # 8-byte Reload
	subq	%r11, %rdi
	leaq	24(%r12,%r11,8), %rsi
	movq	32(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%r11,8), %rbx
	.p2align	4, 0x90
.LBB2_45:                               # %scalar.ph
                                        #   Parent Loop BB2_4 Depth=1
                                        #     Parent Loop BB2_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-24(%rbx), %xmm0        # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	movsd	-24(%rsi), %xmm3        # xmm3 = mem[0],zero
	subsd	%xmm0, %xmm3
	movsd	%xmm3, -24(%rsi)
	movsd	-16(%rbx), %xmm0        # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	movsd	-16(%rsi), %xmm3        # xmm3 = mem[0],zero
	subsd	%xmm0, %xmm3
	movsd	%xmm3, -16(%rsi)
	movsd	-8(%rbx), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	movsd	-8(%rsi), %xmm3         # xmm3 = mem[0],zero
	subsd	%xmm0, %xmm3
	movsd	%xmm3, -8(%rsi)
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	movsd	(%rsi), %xmm3           # xmm3 = mem[0],zero
	subsd	%xmm0, %xmm3
	movsd	%xmm3, (%rsi)
	addq	$32, %rsi
	addq	$32, %rbx
	addq	$-4, %rdi
	jne	.LBB2_45
.LBB2_46:                               # %._crit_edge121.us
                                        #   in Loop: Header=BB2_29 Depth=2
	incq	%rbp
	cmpq	%r15, %rbp
	jne	.LBB2_29
	.p2align	4, 0x90
.LBB2_26:                               # %.backedge
                                        #   in Loop: Header=BB2_4 Depth=1
	incq	%r13
	movq	-72(%rsp), %rdi         # 8-byte Reload
	addb	$3, %dil
	decq	-80(%rsp)               # 8-byte Folded Spill
	incq	-112(%rsp)              # 8-byte Folded Spill
	movq	-128(%rsp), %r10        # 8-byte Reload
	cmpq	-32(%rsp), %r10         # 8-byte Folded Reload
	jl	.LBB2_4
.LBB2_1:
	xorl	%eax, %eax
	jmp	.LBB2_2
.LBB2_12:
	movl	$1, %eax
.LBB2_2:                                # %.critedge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	LU_factor, .Lfunc_end2-LU_factor
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
