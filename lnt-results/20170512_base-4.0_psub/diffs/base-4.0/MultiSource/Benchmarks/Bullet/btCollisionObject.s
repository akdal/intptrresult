	.text
	.file	"btCollisionObject.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1                       # 0x1
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	1                       # 0x1
	.text
	.globl	_ZN17btCollisionObjectC2Ev
	.p2align	4, 0x90
	.type	_ZN17btCollisionObjectC2Ev,@function
_ZN17btCollisionObjectC2Ev:             # @_ZN17btCollisionObjectC2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV17btCollisionObject+16, (%rdi)
	movl	$1065353216, 168(%rdi)  # imm = 0x3F800000
	movl	$1065353216, 172(%rdi)  # imm = 0x3F800000
	movl	$1065353216, 176(%rdi)  # imm = 0x3F800000
	movl	$0, 180(%rdi)
	movb	$0, 184(%rdi)
	movl	$1566444395, 188(%rdi)  # imm = 0x5D5E0B6B
	xorps	%xmm0, %xmm0
	movups	%xmm0, 192(%rdi)
	movq	$0, 208(%rdi)
	movaps	.LCPI0_0(%rip), %xmm1   # xmm1 = [1,4294967295,4294967295,1]
	movups	%xmm1, 216(%rdi)
	movl	$0, 232(%rdi)
	movl	$1056964608, 236(%rdi)  # imm = 0x3F000000
	movl	$0, 240(%rdi)
	movq	$0, 248(%rdi)
	movabsq	$4575657221408423937, %rax # imm = 0x3F80000000000001
	movq	%rax, 256(%rdi)
	movb	$0, 272(%rdi)
	movq	$0, 264(%rdi)
	movl	$1065353216, 8(%rdi)    # imm = 0x3F800000
	movups	%xmm0, 12(%rdi)
	movl	$1065353216, 28(%rdi)   # imm = 0x3F800000
	movups	%xmm0, 32(%rdi)
	movl	$1065353216, 48(%rdi)   # imm = 0x3F800000
	movups	%xmm0, 52(%rdi)
	movl	$0, 68(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN17btCollisionObjectC2Ev, .Lfunc_end0-_ZN17btCollisionObjectC2Ev
	.cfi_endproc

	.globl	_ZN17btCollisionObjectD2Ev
	.p2align	4, 0x90
	.type	_ZN17btCollisionObjectD2Ev,@function
_ZN17btCollisionObjectD2Ev:             # @_ZN17btCollisionObjectD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	_ZN17btCollisionObjectD2Ev, .Lfunc_end1-_ZN17btCollisionObjectD2Ev
	.cfi_endproc

	.globl	_ZN17btCollisionObjectD0Ev
	.p2align	4, 0x90
	.type	_ZN17btCollisionObjectD0Ev,@function
_ZN17btCollisionObjectD0Ev:             # @_ZN17btCollisionObjectD0Ev
	.cfi_startproc
# BB#0:
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.Lfunc_end2:
	.size	_ZN17btCollisionObjectD0Ev, .Lfunc_end2-_ZN17btCollisionObjectD0Ev
	.cfi_endproc

	.globl	_ZN17btCollisionObject18setActivationStateEi
	.p2align	4, 0x90
	.type	_ZN17btCollisionObject18setActivationStateEi,@function
_ZN17btCollisionObject18setActivationStateEi: # @_ZN17btCollisionObject18setActivationStateEi
	.cfi_startproc
# BB#0:
	movl	228(%rdi), %eax
	andl	$-2, %eax
	cmpl	$4, %eax
	je	.LBB3_2
# BB#1:
	movl	%esi, 228(%rdi)
.LBB3_2:
	retq
.Lfunc_end3:
	.size	_ZN17btCollisionObject18setActivationStateEi, .Lfunc_end3-_ZN17btCollisionObject18setActivationStateEi
	.cfi_endproc

	.globl	_ZN17btCollisionObject20forceActivationStateEi
	.p2align	4, 0x90
	.type	_ZN17btCollisionObject20forceActivationStateEi,@function
_ZN17btCollisionObject20forceActivationStateEi: # @_ZN17btCollisionObject20forceActivationStateEi
	.cfi_startproc
# BB#0:
	movl	%esi, 228(%rdi)
	retq
.Lfunc_end4:
	.size	_ZN17btCollisionObject20forceActivationStateEi, .Lfunc_end4-_ZN17btCollisionObject20forceActivationStateEi
	.cfi_endproc

	.globl	_ZN17btCollisionObject8activateEb
	.p2align	4, 0x90
	.type	_ZN17btCollisionObject8activateEb,@function
_ZN17btCollisionObject8activateEb:      # @_ZN17btCollisionObject8activateEb
	.cfi_startproc
# BB#0:
	testb	%sil, %sil
	jne	.LBB5_2
# BB#1:
	testb	$3, 216(%rdi)
	jne	.LBB5_5
.LBB5_2:
	movl	228(%rdi), %eax
	andl	$-2, %eax
	cmpl	$4, %eax
	je	.LBB5_4
# BB#3:
	movl	$1, 228(%rdi)
.LBB5_4:                                # %_ZN17btCollisionObject18setActivationStateEi.exit
	movl	$0, 232(%rdi)
.LBB5_5:
	retq
.Lfunc_end5:
	.size	_ZN17btCollisionObject8activateEb, .Lfunc_end5-_ZN17btCollisionObject8activateEb
	.cfi_endproc

	.section	.text._ZN17btCollisionObject24checkCollideWithOverrideEPS_,"axG",@progbits,_ZN17btCollisionObject24checkCollideWithOverrideEPS_,comdat
	.weak	_ZN17btCollisionObject24checkCollideWithOverrideEPS_
	.p2align	4, 0x90
	.type	_ZN17btCollisionObject24checkCollideWithOverrideEPS_,@function
_ZN17btCollisionObject24checkCollideWithOverrideEPS_: # @_ZN17btCollisionObject24checkCollideWithOverrideEPS_
	.cfi_startproc
# BB#0:
	movb	$1, %al
	retq
.Lfunc_end6:
	.size	_ZN17btCollisionObject24checkCollideWithOverrideEPS_, .Lfunc_end6-_ZN17btCollisionObject24checkCollideWithOverrideEPS_
	.cfi_endproc

	.section	.text._ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape,"axG",@progbits,_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape,comdat
	.weak	_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape
	.p2align	4, 0x90
	.type	_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape,@function
_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape: # @_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape
	.cfi_startproc
# BB#0:
	movq	%rsi, 200(%rdi)
	movq	%rsi, 208(%rdi)
	retq
.Lfunc_end7:
	.size	_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape, .Lfunc_end7-_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape
	.cfi_endproc

	.type	_ZTV17btCollisionObject,@object # @_ZTV17btCollisionObject
	.section	.rodata,"a",@progbits
	.globl	_ZTV17btCollisionObject
	.p2align	3
_ZTV17btCollisionObject:
	.quad	0
	.quad	_ZTI17btCollisionObject
	.quad	_ZN17btCollisionObject24checkCollideWithOverrideEPS_
	.quad	_ZN17btCollisionObjectD2Ev
	.quad	_ZN17btCollisionObjectD0Ev
	.quad	_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape
	.size	_ZTV17btCollisionObject, 48

	.type	_ZTS17btCollisionObject,@object # @_ZTS17btCollisionObject
	.globl	_ZTS17btCollisionObject
	.p2align	4
_ZTS17btCollisionObject:
	.asciz	"17btCollisionObject"
	.size	_ZTS17btCollisionObject, 20

	.type	_ZTI17btCollisionObject,@object # @_ZTI17btCollisionObject
	.globl	_ZTI17btCollisionObject
	.p2align	3
_ZTI17btCollisionObject:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS17btCollisionObject
	.size	_ZTI17btCollisionObject, 16


	.globl	_ZN17btCollisionObjectC1Ev
	.type	_ZN17btCollisionObjectC1Ev,@function
_ZN17btCollisionObjectC1Ev = _ZN17btCollisionObjectC2Ev
	.globl	_ZN17btCollisionObjectD1Ev
	.type	_ZN17btCollisionObjectD1Ev,@function
_ZN17btCollisionObjectD1Ev = _ZN17btCollisionObjectD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
