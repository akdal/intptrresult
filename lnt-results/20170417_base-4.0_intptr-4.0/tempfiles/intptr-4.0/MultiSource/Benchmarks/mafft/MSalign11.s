	.text
	.file	"MSalign11.bc"
	.globl	backdp
	.p2align	4, 0x90
	.type	backdp,@function
backdp:                                 # @backdp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 288
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movl	%ecx, %r15d
	movq	304(%rsp), %r9
	cvtsi2ssl	penalty(%rip), %xmm2
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movslq	%r8d, %rcx
	movq	%rcx, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	-1(%rcx), %r13
	testl	%r15d, %r15d
	je	.LBB0_6
# BB#1:                                 # %.lr.ph.i
	movq	344(%rsp), %rax
	movq	(%rax), %rax
	movq	352(%rsp), %rcx
	movq	(%rcx), %rcx
	movsbq	(%rcx,%r13), %r8
	testb	$1, %r15b
	jne	.LBB0_3
# BB#2:
	movq	%r9, %rbx
	movl	%r15d, %ebp
	cmpl	$1, %r15d
	jne	.LBB0_4
	jmp	.LBB0_6
.LBB0_3:
	leal	-1(%r15), %ebp
	movsbq	(%rax), %rbx
	incq	%rax
	movq	%r8, %rcx
	shlq	$9, %rcx
	cvtsi2ssl	amino_dis(%rcx,%rbx,4), %xmm1
	leaq	4(%r9), %rbx
	movss	%xmm1, (%r9)
	cmpl	$1, %r15d
	je	.LBB0_6
.LBB0_4:                                # %.lr.ph.i.new
	shlq	$9, %r8
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	movsbq	(%rax), %rcx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	amino_dis(%r8,%rcx,4), %xmm1
	movss	%xmm1, (%rbx)
	addl	$-2, %ebp
	movsbq	1(%rax), %rcx
	leaq	2(%rax), %rax
	xorps	%xmm1, %xmm1
	cvtsi2ssl	amino_dis(%r8,%rcx,4), %xmm1
	movss	%xmm1, 4(%rbx)
	leaq	8(%rbx), %rbx
	jne	.LBB0_5
.LBB0_6:                                # %match_calc.exit
	movaps	%xmm2, 160(%rsp)        # 16-byte Spill
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movl	328(%rsp), %r14d
	movslq	%r15d, %r12
	decq	%r12
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB0_12
# BB#7:                                 # %.lr.ph.i219
	movq	352(%rsp), %rax
	movq	(%rax), %rax
	movq	344(%rsp), %rcx
	movq	(%rcx), %rcx
	movsbq	(%rcx,%r12), %rcx
	movq	24(%rsp), %rdi          # 8-byte Reload
	testb	$1, %dil
	jne	.LBB0_9
# BB#8:
	movq	288(%rsp), %rbp
	movl	%edi, %ebx
	testl	%r13d, %r13d
	jne	.LBB0_10
	jmp	.LBB0_12
.LBB0_9:
	leal	-1(%rdi), %ebx
	movsbq	(%rax), %rbp
	incq	%rax
	movq	%rcx, %rdi
	shlq	$9, %rdi
	xorps	%xmm1, %xmm1
	cvtsi2ssl	amino_dis(%rdi,%rbp,4), %xmm1
	movq	288(%rsp), %rdi
	leaq	4(%rdi), %rbp
	movss	%xmm1, (%rdi)
	testl	%r13d, %r13d
	je	.LBB0_12
.LBB0_10:                               # %.lr.ph.i219.new
	shlq	$9, %rcx
	.p2align	4, 0x90
.LBB0_11:                               # =>This Inner Loop Header: Depth=1
	movsbq	(%rax), %rdi
	xorps	%xmm1, %xmm1
	cvtsi2ssl	amino_dis(%rcx,%rdi,4), %xmm1
	movss	%xmm1, (%rbp)
	addl	$-2, %ebx
	movsbq	1(%rax), %rdi
	leaq	2(%rax), %rax
	xorps	%xmm1, %xmm1
	cvtsi2ssl	amino_dis(%rcx,%rdi,4), %xmm1
	movss	%xmm1, 4(%rbp)
	leaq	8(%rbp), %rbp
	jne	.LBB0_11
.LBB0_12:                               # %match_calc.exit223
	movss	%xmm0, 20(%rsp)         # 4-byte Spill
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	movl	%r15d, %ecx
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movl	336(%rsp), %ebp
	movl	%ebp, %edx
	movq	24(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %ecx
	callq	fprintf
	movq	360(%rsp), %rdi
	movq	368(%rsp), %rsi
	movq	344(%rsp), %rdx
	movq	352(%rsp), %rcx
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movl	%r14d, %r8d
	movl	%ebp, %r9d
	pushq	%rbx
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	extendmseq
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	cmpl	$2, %r15d
	movq	80(%rsp), %r10          # 8-byte Reload
	movaps	160(%rsp), %xmm5        # 16-byte Reload
	jl	.LBB0_18
# BB#13:                                # %.lr.ph260
	movslq	%r13d, %rax
	movl	%r12d, %ecx
	testb	$1, %cl
	jne	.LBB0_15
# BB#14:
	xorl	%esi, %esi
	cmpl	$1, %r12d
	jne	.LBB0_16
	jmp	.LBB0_18
.LBB0_15:
	movq	304(%rsp), %rdx
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm0
	movss	%xmm0, (%rdx)
	movq	(%r10), %rdx
	movss	(%rdx,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm0
	movss	%xmm0, (%rdx,%rax,4)
	movl	$1, %esi
	cmpl	$1, %r12d
	je	.LBB0_18
.LBB0_16:                               # %.lr.ph260.new
	subq	%rsi, %rcx
	movq	304(%rsp), %rdx
	leaq	4(%rdx,%rsi,4), %rdx
	leaq	8(%r10,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB0_17:                               # =>This Inner Loop Header: Depth=1
	movss	-4(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm0
	movss	%xmm0, -4(%rdx)
	movq	-8(%rsi), %rdi
	movss	(%rdi,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm0
	movss	%xmm0, (%rdi,%rax,4)
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm0
	movss	%xmm0, (%rdx)
	movq	(%rsi), %rdi
	movss	(%rdi,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm0
	movss	%xmm0, (%rdi,%rax,4)
	addq	$8, %rdx
	addq	$16, %rsi
	addq	$-2, %rcx
	jne	.LBB0_17
.LBB0_18:                               # %.preheader230
	movq	%r15, 64(%rsp)          # 8-byte Spill
	movq	320(%rsp), %r15
	movq	312(%rsp), %r14
	cmpl	$2, 24(%rsp)            # 4-byte Folded Reload
	movq	96(%rsp), %r8           # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	movss	20(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movl	336(%rsp), %edi
	jl	.LBB0_48
# BB#19:                                # %.lr.ph258
	movslq	%r12d, %rax
	movq	(%r10,%rax,8), %r11
	movl	%r13d, %eax
	cmpl	$7, %r13d
	ja	.LBB0_21
# BB#20:
	xorl	%ebx, %ebx
	movq	288(%rsp), %rcx
	movq	24(%rsp), %r12          # 8-byte Reload
	jmp	.LBB0_29
.LBB0_21:                               # %min.iters.checked
	movl	%r13d, %esi
	andl	$7, %esi
	movq	%rax, %rbx
	subq	%rsi, %rbx
	movq	288(%rsp), %rcx
	movq	24(%rsp), %r12          # 8-byte Reload
	je	.LBB0_25
# BB#22:                                # %vector.memcheck
	leaq	(%r11,%rax,4), %rdx
	cmpq	%rcx, %rdx
	jbe	.LBB0_26
# BB#23:                                # %vector.memcheck
	leaq	(%rcx,%rax,4), %rdx
	cmpq	%rdx, %r11
	jae	.LBB0_26
.LBB0_25:
	xorl	%ebx, %ebx
	jmp	.LBB0_29
.LBB0_26:                               # %vector.ph
	movaps	%xmm5, %xmm0
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%rcx), %rdi
	leaq	16(%r11), %rbp
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB0_27:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdi), %xmm1
	movups	(%rdi), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	%xmm1, -16(%rdi)
	movups	%xmm2, (%rdi)
	movups	-16(%rbp), %xmm1
	movups	(%rbp), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	%xmm1, -16(%rbp)
	movups	%xmm2, (%rbp)
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$-8, %rdx
	jne	.LBB0_27
# BB#28:                                # %middle.block
	testl	%esi, %esi
	movl	336(%rsp), %edi
	je	.LBB0_34
.LBB0_29:                               # %scalar.ph.preheader
	movl	%eax, %esi
	subl	%ebx, %esi
	leaq	-1(%rax), %rdx
	testb	$1, %sil
	movq	%rbx, %rsi
	je	.LBB0_31
# BB#30:                                # %scalar.ph.prol
	movss	(%rcx,%rbx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm0
	movss	%xmm0, (%rcx,%rbx,4)
	movss	(%r11,%rbx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm0
	movss	%xmm0, (%r11,%rbx,4)
	leaq	1(%rbx), %rsi
.LBB0_31:                               # %scalar.ph.prol.loopexit
	cmpq	%rbx, %rdx
	je	.LBB0_34
# BB#32:                                # %scalar.ph.preheader.new
	subq	%rsi, %rax
	leaq	4(%rcx,%rsi,4), %rdx
	leaq	4(%r11,%rsi,4), %rcx
	.p2align	4, 0x90
.LBB0_33:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	-4(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm0
	movss	%xmm0, -4(%rdx)
	movss	-4(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm0
	movss	%xmm0, -4(%rcx)
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm0
	movss	%xmm0, (%rdx)
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm0
	movss	%xmm0, (%rcx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-2, %rax
	jne	.LBB0_33
.LBB0_34:                               # %.preheader229
	cmpl	$2, %r12d
	jl	.LBB0_48
# BB#35:                                # %.lr.ph256.preheader
	cmpq	$7, %r13
	movq	%r13, %rcx
	jbe	.LBB0_46
# BB#36:                                # %min.iters.checked298
	movq	%r13, %rax
	andq	$-8, %rax
	movq	%r13, %rcx
	je	.LBB0_46
# BB#37:                                # %vector.memcheck313
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	288(%rsp), %rdx
	leaq	(%rdx,%rcx,4), %rcx
	cmpq	%r14, %rcx
	jbe	.LBB0_39
# BB#38:                                # %vector.memcheck313
	leaq	(%r14,%r13,4), %rcx
	movq	288(%rsp), %rdx
	leaq	4(%rdx), %rdx
	cmpq	%rcx, %rdx
	movq	%r13, %rcx
	jb	.LBB0_46
.LBB0_39:                               # %vector.body294.preheader
	leaq	-8(%rax), %rcx
	movq	%rcx, %rdx
	shrq	$3, %rdx
	btl	$3, %ecx
	jb	.LBB0_41
# BB#40:                                # %vector.body294.prol
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	288(%rsp), %rsi
	movups	-16(%rsi,%rcx,4), %xmm0
	movups	-32(%rsi,%rcx,4), %xmm1
	movups	%xmm0, -20(%r14,%rcx,4)
	movups	%xmm1, -36(%r14,%rcx,4)
	xorps	%xmm0, %xmm0
	movups	%xmm0, -16(%r15,%rcx,4)
	movups	%xmm0, -32(%r15,%rcx,4)
	movl	$8, %ecx
	testq	%rdx, %rdx
	jne	.LBB0_42
	jmp	.LBB0_44
.LBB0_41:
	xorl	%ecx, %ecx
	movq	288(%rsp), %rsi
	testq	%rdx, %rdx
	je	.LBB0_44
.LBB0_42:                               # %vector.body294.preheader.new
	movq	%rax, %rdx
	negq	%rdx
	negq	%rcx
	movq	32(%rsp), %rbp          # 8-byte Reload
	leaq	-16(%rsi,%rbp,4), %rsi
	leaq	-20(%r14,%rbp,4), %rdi
	leaq	-16(%r15,%rbp,4), %rbp
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_43:                               # %vector.body294
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rsi,%rcx,4), %xmm1
	movups	-16(%rsi,%rcx,4), %xmm2
	movups	%xmm1, (%rdi,%rcx,4)
	movups	%xmm2, -16(%rdi,%rcx,4)
	movups	%xmm0, (%rbp,%rcx,4)
	movups	%xmm0, -16(%rbp,%rcx,4)
	movups	-32(%rsi,%rcx,4), %xmm1
	movups	-48(%rsi,%rcx,4), %xmm2
	movups	%xmm1, -32(%rdi,%rcx,4)
	movups	%xmm2, -48(%rdi,%rcx,4)
	movups	%xmm0, -32(%rbp,%rcx,4)
	movups	%xmm0, -48(%rbp,%rcx,4)
	addq	$-16, %rcx
	cmpq	%rcx, %rdx
	jne	.LBB0_43
.LBB0_44:                               # %middle.block295
	cmpq	%rax, %r13
	movl	336(%rsp), %edi
	je	.LBB0_48
# BB#45:
	movq	%r13, %rcx
	subq	%rax, %rcx
.LBB0_46:                               # %.lr.ph256.preheader329
	incq	%rcx
	movq	288(%rsp), %rdx
	.p2align	4, 0x90
.LBB0_47:                               # %.lr.ph256
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rdx,%rcx,4), %eax
	movl	%eax, -8(%r14,%rcx,4)
	movl	$0, -4(%r15,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB0_47
.LBB0_48:                               # %.preheader
	movq	24(%rsp), %r12          # 8-byte Reload
	testl	%r12d, %r12d
	jle	.LBB0_50
# BB#49:                                # %.lr.ph253.preheader
	movl	%r13d, %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	memset
	movl	336(%rsp), %edi
	movaps	160(%rsp), %xmm5        # 16-byte Reload
	movq	80(%rsp), %r10          # 8-byte Reload
	movss	20(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	96(%rsp), %r8           # 8-byte Reload
.LBB0_50:                               # %._crit_edge254
	movq	64(%rsp), %rdx          # 8-byte Reload
	addl	$-2, %edx
	movq	304(%rsp), %rbx
	js	.LBB0_82
# BB#51:                                # %.lr.ph248
	movq	296(%rsp), %rax
	leal	-2(%r12), %esi
	testl	%r12d, %r12d
	movq	%r13, 152(%rsp)         # 8-byte Spill
	movslq	%r13d, %rbp
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	-8(%r14,%rcx,4), %r13
	leaq	(%r15,%rcx,4), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movslq	%esi, %r15
	movslq	%edx, %r14
	movq	288(%rsp), %r11
	movl	%esi, 76(%rsp)          # 4-byte Spill
	movq	%rbp, 128(%rsp)         # 8-byte Spill
	je	.LBB0_69
# BB#52:                                # %.lr.ph248.split.preheader
	movl	%r12d, %ecx
	andl	$1, %ecx
	movl	%ecx, 148(%rsp)         # 4-byte Spill
	leal	-1(%r12), %ecx
	movl	%ecx, 144(%rsp)         # 4-byte Spill
	leaq	(,%r15,4), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	leaq	(%r8,%r15,4), %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movl	%r15d, %ecx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_53:                               # %.lr.ph248.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_56 Depth 2
                                        #     Child Loop BB0_59 Depth 2
	movq	%rax, %rsi
	cmpl	$0, 148(%rsp)           # 4-byte Folded Reload
	movl	4(%rbx,%r14,4), %eax
	movl	%eax, (%r11,%rbp,4)
	movq	352(%rsp), %rax
	movq	(%rax), %rax
	movq	344(%rsp), %rcx
	movq	(%rcx), %rcx
	movsbq	(%rcx,%r14), %rbp
	movq	%rsi, 176(%rsp)         # 8-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	jne	.LBB0_55
# BB#54:                                #   in Loop: Header=BB0_53 Depth=1
	movq	%rsi, %rdx
	movl	%r12d, %esi
	cmpl	$1, %r12d
	jne	.LBB0_56
	jmp	.LBB0_57
	.p2align	4, 0x90
.LBB0_55:                               #   in Loop: Header=BB0_53 Depth=1
	movsbq	(%rax), %rdx
	incq	%rax
	movq	%rsi, %rcx
	movq	%rbp, %rsi
	shlq	$9, %rsi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rsi,%rdx,4), %xmm0
	leaq	4(%rcx), %rdx
	movss	%xmm0, (%rcx)
	movl	144(%rsp), %esi         # 4-byte Reload
	cmpl	$1, %r12d
	je	.LBB0_57
	.p2align	4, 0x90
.LBB0_56:                               #   Parent Loop BB0_53 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	(%rax), %rdi
	movq	%rbp, %rcx
	shlq	$9, %rcx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rcx,%rdi,4), %xmm0
	movss	%xmm0, (%rdx)
	addl	$-2, %esi
	movsbq	1(%rax), %rdi
	leaq	2(%rax), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rcx,%rdi,4), %xmm0
	movss	%xmm0, 4(%rdx)
	leaq	8(%rdx), %rdx
	jne	.LBB0_56
.LBB0_57:                               # %match_calc.exit228
                                        #   in Loop: Header=BB0_53 Depth=1
	cmpl	$0, 76(%rsp)            # 4-byte Folded Reload
	movl	(%rbx,%r14,4), %eax
	movq	128(%rsp), %rbp         # 8-byte Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx,%rbp,4)
	js	.LBB0_67
# BB#58:                                # %.lr.ph
                                        #   in Loop: Header=BB0_53 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rbx
	movss	(%r11,%rbp,4), %xmm6    # xmm6 = mem[0],zero,zero,zero
	leaq	-1(%r14), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	%r11, 120(%rsp)         # 8-byte Spill
	leaq	(%r11,%rax,4), %r12
	xorl	%ebp, %ebp
	movq	8(%rsp), %r11           # 8-byte Reload
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	104(%rsp), %rsi         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_59:                               #   Parent Loop BB0_53 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r15,%rbp), %rax
	movss	(%r12,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	addss	%xmm6, %xmm1
	maxss	%xmm0, %xmm1
	movaps	%xmm6, %xmm2
	cmpless	%xmm0, %xmm2
	movaps	%xmm2, %xmm3
	andps	%xmm0, %xmm3
	andnps	%xmm6, %xmm2
	movaps	%xmm2, %xmm6
	orps	%xmm3, %xmm6
	movss	(%r13,%rbp,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	addss	%xmm2, %xmm3
	maxss	%xmm1, %xmm3
	ucomiss	%xmm2, %xmm0
	jb	.LBB0_61
# BB#60:                                #   in Loop: Header=BB0_59 Depth=2
	movss	%xmm0, (%r13,%rbp,4)
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, -8(%rsi,%rbp,4)
.LBB0_61:                               #   in Loop: Header=BB0_59 Depth=2
	movq	136(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%r9,%r14,4), %xmm0
	movq	%rax, %rcx
	cmovaq	%r14, %rcx
	movq	%r8, %rdx
	cmovaq	%r9, %rdx
	movslq	%ecx, %rcx
	movss	(%rdx,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm0
	movq	(%r10,%r14,8), %rcx
	addq	64(%rsp), %rcx          # 8-byte Folded Reload
	movss	%xmm0, (%rcx,%rbp,4)
	movslq	%edi, %rcx
	cmpq	%rcx, %rax
	jge	.LBB0_65
# BB#62:                                #   in Loop: Header=BB0_59 Depth=2
	movslq	%r11d, %rax
	cmpq	%rax, %r14
	jge	.LBB0_65
# BB#63:                                #   in Loop: Header=BB0_59 Depth=2
	ucomiss	%xmm4, %xmm0
	jne	.LBB0_65
	jp	.LBB0_65
# BB#64:                                #   in Loop: Header=BB0_59 Depth=2
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbp), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	stderr(%rip), %rcx
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movl	$.L.str.2, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%r11, 8(%rsp)           # 8-byte Spill
	movaps	%xmm6, 208(%rsp)        # 16-byte Spill
	movaps	%xmm3, 192(%rsp)        # 16-byte Spill
	callq	fwrite
	movq	360(%rsp), %rax
	movq	%rax, %rdi
	movq	368(%rsp), %rax
	movq	%rax, %rsi
	movq	344(%rsp), %rdx
	movq	352(%rsp), %rcx
	movl	%r14d, %r8d
	movq	48(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	callq	extendmseq
	movaps	208(%rsp), %xmm3        # 16-byte Reload
	movaps	224(%rsp), %xmm6        # 16-byte Reload
	movq	120(%rsp), %rsi         # 8-byte Reload
	movaps	176(%rsp), %xmm5        # 16-byte Reload
	movq	96(%rsp), %r10          # 8-byte Reload
	movss	36(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movq	104(%rsp), %r9          # 8-byte Reload
	movq	112(%rsp), %r8          # 8-byte Reload
	addq	$16, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset -16
	movq	360(%rsp), %rdx
	movq	(%rdx), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdx)
	movb	$117, -1(%rax)
	movq	368(%rsp), %rdx
	movq	(%rdx), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdx)
	movb	$117, -1(%rax)
	movq	48(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	movl	%r14d, %r11d
	.p2align	4, 0x90
.LBB0_65:                               #   in Loop: Header=BB0_59 Depth=2
	addss	-8(%rbx,%rbp,4), %xmm3
	movss	%xmm3, -8(%rbx,%rbp,4)
	leaq	-1(%r15,%rbp), %rax
	decq	%rbp
	incq	%rax
	testq	%rax, %rax
	jg	.LBB0_59
# BB#66:                                #   in Loop: Header=BB0_53 Depth=1
	movq	%r11, 8(%rsp)           # 8-byte Spill
	movq	304(%rsp), %rbx
	movq	120(%rsp), %r11         # 8-byte Reload
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	128(%rsp), %rbp         # 8-byte Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	jmp	.LBB0_68
	.p2align	4, 0x90
.LBB0_67:                               # %match_calc.exit228.._crit_edge_crit_edge
                                        #   in Loop: Header=BB0_53 Depth=1
	leaq	-1(%r14), %rax
	movq	40(%rsp), %rdi          # 8-byte Reload
.LBB0_68:                               # %._crit_edge
                                        #   in Loop: Header=BB0_53 Depth=1
	testq	%r14, %r14
	movq	%rax, %r14
	movq	%r11, %rax
	movq	%rcx, %r11
	jg	.LBB0_53
	jmp	.LBB0_82
.LBB0_69:                               # %.lr.ph248.split.us.preheader
	movl	%esi, %ecx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	leaq	(,%r15,4), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	leaq	(%r8,%r15,4), %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movq	104(%rsp), %rsi         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_70:                               # %.lr.ph248.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_72 Depth 2
	movq	%rax, %rcx
	cmpl	$0, 76(%rsp)            # 4-byte Folded Reload
	movl	4(%rbx,%r14,4), %eax
	movl	%eax, (%r11,%rbp,4)
	movl	(%rbx,%r14,4), %eax
	movl	%eax, (%rcx,%rbp,4)
	js	.LBB0_80
# BB#71:                                # %.lr.ph.us
                                        #   in Loop: Header=BB0_70 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rax,4), %rbx
	movss	(%r11,%rbp,4), %xmm6    # xmm6 = mem[0],zero,zero,zero
	leaq	-1(%r14), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	%r11, 120(%rsp)         # 8-byte Spill
	leaq	(%r11,%rax,4), %r12
	xorl	%ebp, %ebp
	movq	8(%rsp), %r11           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_72:                               #   Parent Loop BB0_70 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r15,%rbp), %rax
	movss	(%r12,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	addss	%xmm6, %xmm1
	maxss	%xmm0, %xmm1
	movaps	%xmm6, %xmm2
	cmpless	%xmm0, %xmm2
	movaps	%xmm2, %xmm3
	andps	%xmm0, %xmm3
	andnps	%xmm6, %xmm2
	movaps	%xmm2, %xmm6
	orps	%xmm3, %xmm6
	movss	(%r13,%rbp,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	addss	%xmm2, %xmm3
	maxss	%xmm1, %xmm3
	ucomiss	%xmm2, %xmm0
	jb	.LBB0_74
# BB#73:                                #   in Loop: Header=BB0_72 Depth=2
	movss	%xmm0, (%r13,%rbp,4)
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, -8(%rsi,%rbp,4)
.LBB0_74:                               #   in Loop: Header=BB0_72 Depth=2
	movq	136(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%r9,%r14,4), %xmm0
	movq	%rax, %rcx
	cmovaq	%r14, %rcx
	movq	%r8, %rdx
	cmovaq	%r9, %rdx
	movslq	%ecx, %rcx
	movss	(%rdx,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm0
	movq	(%r10,%r14,8), %rcx
	addq	64(%rsp), %rcx          # 8-byte Folded Reload
	movss	%xmm0, (%rcx,%rbp,4)
	movslq	%edi, %rcx
	cmpq	%rcx, %rax
	jge	.LBB0_78
# BB#75:                                #   in Loop: Header=BB0_72 Depth=2
	movslq	%r11d, %rax
	cmpq	%rax, %r14
	jge	.LBB0_78
# BB#76:                                #   in Loop: Header=BB0_72 Depth=2
	ucomiss	%xmm4, %xmm0
	jne	.LBB0_78
	jp	.LBB0_78
# BB#77:                                #   in Loop: Header=BB0_72 Depth=2
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbp), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	stderr(%rip), %rcx
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movl	$.L.str.2, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%r11, 8(%rsp)           # 8-byte Spill
	movaps	%xmm6, 176(%rsp)        # 16-byte Spill
	movaps	%xmm3, 208(%rsp)        # 16-byte Spill
	callq	fwrite
	movq	360(%rsp), %rax
	movq	%rax, %rdi
	movq	368(%rsp), %rax
	movq	%rax, %rsi
	movq	344(%rsp), %rdx
	movq	352(%rsp), %rcx
	movl	%r14d, %r8d
	movq	48(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	callq	extendmseq
	movaps	224(%rsp), %xmm3        # 16-byte Reload
	movaps	192(%rsp), %xmm6        # 16-byte Reload
	movq	120(%rsp), %rsi         # 8-byte Reload
	movaps	176(%rsp), %xmm5        # 16-byte Reload
	movq	96(%rsp), %r10          # 8-byte Reload
	movss	36(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movq	104(%rsp), %r9          # 8-byte Reload
	movq	112(%rsp), %r8          # 8-byte Reload
	addq	$16, %rsp
.Lcfi21:
	.cfi_adjust_cfa_offset -16
	movq	360(%rsp), %rdx
	movq	(%rdx), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdx)
	movb	$117, -1(%rax)
	movq	368(%rsp), %rdx
	movq	(%rdx), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rdx)
	movb	$117, -1(%rax)
	movq	48(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	movl	%r14d, %r11d
	.p2align	4, 0x90
.LBB0_78:                               #   in Loop: Header=BB0_72 Depth=2
	addss	-8(%rbx,%rbp,4), %xmm3
	movss	%xmm3, -8(%rbx,%rbp,4)
	leaq	-1(%r15,%rbp), %rax
	decq	%rbp
	incq	%rax
	testq	%rax, %rax
	jg	.LBB0_72
# BB#79:                                #   in Loop: Header=BB0_70 Depth=1
	movq	%r11, 8(%rsp)           # 8-byte Spill
	movq	304(%rsp), %rbx
	movq	120(%rsp), %r11         # 8-byte Reload
	movq	128(%rsp), %rbp         # 8-byte Reload
	movq	192(%rsp), %rcx         # 8-byte Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	jmp	.LBB0_81
	.p2align	4, 0x90
.LBB0_80:                               # %.lr.ph248.split.us.._crit_edge.us_crit_edge
                                        #   in Loop: Header=BB0_70 Depth=1
	leaq	-1(%r14), %rax
.LBB0_81:                               # %._crit_edge.us
                                        #   in Loop: Header=BB0_70 Depth=1
	testq	%r14, %r14
	movq	%rax, %r14
	movq	%r11, %rax
	movq	%rcx, %r11
	jg	.LBB0_70
.LBB0_82:                               # %._crit_edge249
	movl	$-1, %r8d
	movl	$-1, %r9d
	movq	%rdi, %rax
	movq	360(%rsp), %rdi
	movq	368(%rsp), %rsi
	movq	344(%rsp), %rdx
	movq	352(%rsp), %rcx
	pushq	%rax
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	callq	extendmseq
	addq	$248, %rsp
.Lcfi24:
	.cfi_adjust_cfa_offset -16
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	backdp, .Lfunc_end0-backdp
	.cfi_endproc

	.p2align	4, 0x90
	.type	extendmseq,@function
extendmseq:                             # @extendmseq
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 80
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movl	%r9d, 12(%rsp)          # 4-byte Spill
	movl	%r8d, %r13d
	movq	%rcx, %r14
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	88(%rsp), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	80(%rsp), %ebp
	movq	stderr(%rip), %rdi
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	movl	%ebp, %ecx
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movl	12(%rsp), %edx          # 4-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	subl	%r13d, %ebp
	leal	-1(%rbp), %edx
	movq	stderr(%rip), %rdi
	movl	$.L.str.15, %esi
	xorl	%eax, %eax
	callq	fprintf
	cmpl	$2, %ebp
	jl	.LBB1_5
# BB#1:                                 # %.lr.ph41.preheader
	movslq	%ebp, %rax
	movslq	8(%rsp), %rcx           # 4-byte Folded Reload
	testb	$1, %bpl
	jne	.LBB1_3
# BB#2:                                 # %.lr.ph41.prol
	movq	(%r12), %rdx
	leaq	-1(%rax,%rcx), %rsi
	decq	%rax
	movb	(%rdx,%rsi), %dl
	movq	(%rbx), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%rbx)
	movb	%dl, -1(%rsi)
	movq	(%r15), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r15)
	movb	$45, -1(%rdx)
.LBB1_3:                                # %.lr.ph41.prol.loopexit
	cmpl	$2, %ebp
	je	.LBB1_5
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph41
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdx
	addq	%rcx, %rdx
	movzbl	-1(%rax,%rdx), %edx
	movq	(%rbx), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%rbx)
	movb	%dl, -1(%rsi)
	movq	(%r15), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r15)
	movb	$45, -1(%rdx)
	movq	(%r12), %rdx
	addq	%rcx, %rdx
	movzbl	-2(%rax,%rdx), %edx
	addq	$-2, %rax
	movq	(%rbx), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%rbx)
	movb	%dl, -1(%rsi)
	movq	(%r15), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r15)
	movb	$45, -1(%rdx)
	cmpq	$1, %rax
	jg	.LBB1_4
.LBB1_5:                                # %._crit_edge42
	movl	12(%rsp), %r13d         # 4-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	subl	%r13d, %ebp
	leal	-1(%rbp), %edx
	movq	stderr(%rip), %rdi
	movl	$.L.str.15, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	%rbp, %rdx
	cmpl	$2, %edx
	jl	.LBB1_10
# BB#6:                                 # %.lr.ph.preheader
	movslq	%edx, %rax
	movslq	%r13d, %rcx
	testb	$1, %dl
	jne	.LBB1_8
# BB#7:                                 # %.lr.ph.prol
	movq	(%rbx), %rdi
	leaq	-1(%rdi), %rsi
	movq	%rsi, (%rbx)
	movb	$45, -1(%rdi)
	movq	(%r14), %rdi
	leaq	-1(%rax,%rcx), %rsi
	decq	%rax
	movb	(%rdi,%rsi), %r8b
	movq	(%r15), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%r15)
	movb	%r8b, -1(%rsi)
.LBB1_8:                                # %.lr.ph.prol.loopexit
	cmpl	$2, %edx
	je	.LBB1_10
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%rbx)
	movb	$45, -1(%rdx)
	movq	(%r14), %rdx
	addq	%rcx, %rdx
	movzbl	-1(%rax,%rdx), %edx
	movq	(%r15), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%r15)
	movb	%dl, -1(%rsi)
	movq	(%rbx), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%rbx)
	movb	$45, -1(%rdx)
	movq	(%r14), %rdx
	addq	%rcx, %rdx
	movzbl	-2(%rax,%rdx), %edx
	addq	$-2, %rax
	movq	(%r15), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%r15)
	movb	%dl, -1(%rsi)
	cmpq	$1, %rax
	jg	.LBB1_9
.LBB1_10:                               # %._crit_edge
	movl	%r13d, %eax
	orl	8(%rsp), %eax           # 4-byte Folded Reload
	js	.LBB1_11
# BB#12:
	movq	(%r12), %rax
	movslq	8(%rsp), %rcx           # 4-byte Folded Reload
	movb	(%rax,%rcx), %al
	movq	(%rbx), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%rbx)
	movb	%al, -1(%rdx)
	movq	(%r14), %rax
	movslq	%r13d, %rbp
	movb	(%rax,%rbp), %al
	movq	(%r15), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r15)
	movb	%al, -1(%rdx)
	movq	stderr(%rip), %rdi
	movq	(%r12), %rax
	movsbl	(%rax,%rcx), %edx
	movq	(%rbx), %rcx
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	(%r14), %rax
	movsbl	(%rax,%rbp), %edx
	movq	(%r15), %rcx
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fprintf                 # TAILCALL
.LBB1_11:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	extendmseq, .Lfunc_end1-extendmseq
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4608533498688228557     # double 1.3
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_1:
	.long	3296328090              # float -999.900024
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_2:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI2_3:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI2_4:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.text
	.globl	MSalign11
	.p2align	4, 0x90
	.type	MSalign11,@function
MSalign11:                              # @MSalign11
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 160
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %r12
	cvtsi2ssl	penalty(%rip), %xmm0
	movss	%xmm0, 36(%rsp)         # 4-byte Spill
	movl	MSalign11.orlgth1(%rip), %r14d
	testl	%r14d, %r14d
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	jne	.LBB2_2
# BB#1:
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, MSalign11.mseq1(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rax, MSalign11.mseq2(%rip)
	movl	MSalign11.orlgth1(%rip), %r14d
.LBB2_2:
	movq	(%rdi), %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	(%r12), %rdi
	callq	strlen
	movq	%rax, %r11
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	cmpl	%r14d, %ebp
	movl	MSalign11.orlgth2(%rip), %r15d
	movq	%r12, 64(%rsp)          # 8-byte Spill
	movq	%r11, 24(%rsp)          # 8-byte Spill
	movl	%ebx, 84(%rsp)          # 4-byte Spill
	jg	.LBB2_5
# BB#3:
	cmpl	%r15d, %r11d
	jg	.LBB2_5
# BB#4:
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	jmp	.LBB2_9
.LBB2_5:
	testl	%r14d, %r14d
	jle	.LBB2_8
# BB#6:
	testl	%r15d, %r15d
	jle	.LBB2_8
# BB#7:
	movq	MSalign11.w1(%rip), %rdi
	callq	FreeFloatVec
	movq	MSalign11.w2(%rip), %rdi
	callq	FreeFloatVec
	movq	MSalign11.match(%rip), %rdi
	callq	FreeFloatVec
	movq	MSalign11.initverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	MSalign11.lastverticalw(%rip), %rdi
	callq	FreeFloatVec
	xorl	%edi, %edi
	callq	FreeFloatVec
	xorl	%edi, %edi
	callq	FreeFloatVec
	movq	MSalign11.m(%rip), %rdi
	callq	FreeFloatVec
	movq	MSalign11.mp(%rip), %rdi
	callq	FreeIntVec
	movq	MSalign11.mseq(%rip), %rdi
	callq	FreeCharMtx
	movq	MSalign11.cpmx1(%rip), %rdi
	callq	FreeFloatMtx
	movq	MSalign11.cpmx2(%rip), %rdi
	callq	FreeFloatMtx
	movq	MSalign11.floatwork(%rip), %rdi
	callq	FreeFloatMtx
	movq	MSalign11.intwork(%rip), %rdi
	callq	FreeIntMtx
	movq	24(%rsp), %r11          # 8-byte Reload
	movl	MSalign11.orlgth1(%rip), %r14d
	movl	MSalign11.orlgth2(%rip), %r15d
.LBB2_8:
	movq	72(%rsp), %rax          # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r14d, %eax
	cmovgel	%eax, %r14d
	leal	100(%r14), %ebx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r11d, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r15d, %eax
	cmovgel	%eax, %r15d
	leal	100(%r15), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	leal	102(%r15), %r13d
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, MSalign11.w1(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, MSalign11.w2(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, MSalign11.match(%rip)
	leal	102(%r14), %r12d
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, MSalign11.initverticalw(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, MSalign11.lastverticalw(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, MSalign11.m(%rip)
	movl	%r13d, %edi
	callq	AllocateIntVec
	movq	%rax, MSalign11.mp(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, %rbp
	movl	njob(%rip), %edi
	leal	200(%r15,%r14), %esi
	callq	AllocateCharMtx
	movq	%rax, MSalign11.mseq(%rip)
	movl	$26, %edi
	movl	%r12d, %esi
	callq	AllocateFloatMtx
	movq	%rax, MSalign11.cpmx1(%rip)
	movl	$26, %edi
	movl	%r13d, %esi
	movq	%rbp, %r13
	callq	AllocateFloatMtx
	movq	%rax, MSalign11.cpmx2(%rip)
	movl	40(%rsp), %esi          # 4-byte Reload
	cmpl	%esi, %ebx
	cmovgel	%ebx, %esi
	addl	$2, %esi
	movl	$26, %edi
	movl	%esi, %ebx
	callq	AllocateFloatMtx
	movq	%rax, MSalign11.floatwork(%rip)
	movl	$26, %edi
	movl	%ebx, %esi
	callq	AllocateIntMtx
	movq	24(%rsp), %r11          # 8-byte Reload
	movq	%rax, MSalign11.intwork(%rip)
	movl	%r14d, MSalign11.orlgth1(%rip)
	movl	%r15d, MSalign11.orlgth2(%rip)
	movq	64(%rsp), %r12          # 8-byte Reload
.LBB2_9:
	movq	8(%rsp), %r8            # 8-byte Reload
	movss	36(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movq	MSalign11.mseq(%rip), %rax
	movq	(%rax), %rcx
	movq	MSalign11.mseq1(%rip), %rdx
	movq	%rcx, (%rdx)
	movq	8(%rax), %rax
	movq	MSalign11.mseq2(%rip), %rcx
	movq	%rax, (%rcx)
	movl	commonAlloc1(%rip), %ebp
	cmpl	%ebp, %r14d
	movl	commonAlloc2(%rip), %ebx
	jg	.LBB2_11
# BB#10:
	cmpl	%ebx, %r15d
	jle	.LBB2_15
.LBB2_11:                               # %._crit_edge451
	testl	%ebp, %ebp
	je	.LBB2_14
# BB#12:                                # %._crit_edge451
	testl	%ebx, %ebx
	je	.LBB2_14
# BB#13:
	movq	commonIP(%rip), %rdi
	callq	FreeIntMtx
	movq	MSalign11.WMMTX(%rip), %rdi
	callq	FreeFloatMtx
	movl	MSalign11.orlgth1(%rip), %r14d
	movl	commonAlloc1(%rip), %ebp
	movl	MSalign11.orlgth2(%rip), %r15d
	movl	commonAlloc2(%rip), %ebx
.LBB2_14:
	cmpl	%ebp, %r14d
	cmovgel	%r14d, %ebp
	cmpl	%ebx, %r15d
	cmovgel	%r15d, %ebx
	leal	10(%rbp), %r14d
	leal	10(%rbx), %r15d
	movl	%r14d, %edi
	movl	%r15d, %esi
	callq	AllocateIntMtx
	movq	%rax, commonIP(%rip)
	movl	%r14d, %edi
	movl	%r15d, %esi
	callq	AllocateFloatMtx
	movq	%rax, MSalign11.WMMTX(%rip)
	movl	%ebp, commonAlloc1(%rip)
	movl	%ebx, commonAlloc2(%rip)
	movq	8(%rsp), %r8            # 8-byte Reload
	movss	36(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movq	24(%rsp), %r11          # 8-byte Reload
.LBB2_15:
	movq	commonIP(%rip), %rax
	movq	%rax, MSalign11.ijp(%rip)
	movq	MSalign11.w1(%rip), %r14
	movq	MSalign11.w2(%rip), %r9
	movq	72(%rsp), %r15          # 8-byte Reload
	testl	%r15d, %r15d
	je	.LBB2_21
# BB#16:                                # %.lr.ph.i
	movq	(%r8), %rcx
	movq	(%r12), %rax
	movq	MSalign11.initverticalw(%rip), %rdx
	movsbq	(%rax), %rbp
	testb	$1, %r15b
	jne	.LBB2_18
# BB#17:
	movl	%r15d, %edi
	cmpl	$1, %r15d
	jne	.LBB2_19
	jmp	.LBB2_21
.LBB2_18:
	leal	-1(%r15), %edi
	movsbq	(%rcx), %rax
	incq	%rcx
	movq	%rbp, %rsi
	shlq	$9, %rsi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rsi,%rax,4), %xmm0
	movss	%xmm0, (%rdx)
	addq	$4, %rdx
	cmpl	$1, %r15d
	je	.LBB2_21
.LBB2_19:                               # %.lr.ph.i.new
	shlq	$9, %rbp
	.p2align	4, 0x90
.LBB2_20:                               # =>This Inner Loop Header: Depth=1
	movsbq	(%rcx), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rbp,%rax,4), %xmm0
	movss	%xmm0, (%rdx)
	addl	$-2, %edi
	movsbq	1(%rcx), %rax
	leaq	2(%rcx), %rcx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rbp,%rax,4), %xmm0
	movss	%xmm0, 4(%rdx)
	leaq	8(%rdx), %rdx
	jne	.LBB2_20
.LBB2_21:                               # %match_calc.exit
	testl	%r11d, %r11d
	je	.LBB2_27
# BB#22:                                # %.lr.ph.i344
	movq	(%r12), %rcx
	movq	(%r8), %rax
	movsbq	(%rax), %rdx
	testb	$1, %r11b
	jne	.LBB2_24
# BB#23:
	movq	%r14, %rdi
	movl	%r11d, %esi
	cmpl	$1, %r11d
	jne	.LBB2_25
	jmp	.LBB2_27
.LBB2_24:
	leal	-1(%r11), %esi
	movsbq	(%rcx), %rax
	incq	%rcx
	movq	%rdx, %rdi
	shlq	$9, %rdi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rdi,%rax,4), %xmm0
	leaq	4(%r14), %rdi
	movss	%xmm0, (%r14)
	cmpl	$1, %r11d
	je	.LBB2_27
.LBB2_25:                               # %.lr.ph.i344.new
	shlq	$9, %rdx
	.p2align	4, 0x90
.LBB2_26:                               # =>This Inner Loop Header: Depth=1
	movsbq	(%rcx), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rdx,%rax,4), %xmm0
	movss	%xmm0, (%rdi)
	addl	$-2, %esi
	movsbq	1(%rcx), %rax
	leaq	2(%rcx), %rcx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rdx,%rax,4), %xmm0
	movss	%xmm0, 4(%rdi)
	leaq	8(%rdi), %rdi
	jne	.LBB2_26
.LBB2_27:                               # %match_calc.exit349
	movq	MSalign11.initverticalw(%rip), %r10
	movl	(%r10), %eax
	movq	MSalign11.WMMTX(%rip), %rdx
	movq	(%rdx), %rcx
	movl	%eax, (%rcx)
	movl	(%r10), %eax
	movl	%eax, (%r13)
	testl	%r15d, %r15d
	jle	.LBB2_39
# BB#28:                                # %.lr.ph409
	leaq	1(%r15), %rax
	movl	%eax, %esi
	testb	$1, %al
	jne	.LBB2_32
# BB#29:
	movss	4(%r10), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	movss	%xmm0, 4(%r10)
	movq	8(%rdx), %rax
	movss	%xmm0, (%rax)
	movss	4(%r10), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%r13), %xmm0
	jbe	.LBB2_31
# BB#30:
	movss	%xmm0, (%r13)
.LBB2_31:                               # %.prol.loopexit519
	movl	$2, %eax
	cmpq	$2, %rsi
	jne	.LBB2_33
	jmp	.LBB2_39
.LBB2_32:
	movl	$1, %eax
	cmpq	$2, %rsi
	je	.LBB2_39
.LBB2_33:                               # %.lr.ph409.new
	subq	%rax, %rsi
	leaq	8(%rdx,%rax,8), %rdi
	leaq	4(%r10,%rax,4), %rbp
	.p2align	4, 0x90
.LBB2_34:                               # =>This Inner Loop Header: Depth=1
	movss	-4(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	movss	%xmm0, -4(%rbp)
	movq	-8(%rdi), %rax
	movss	%xmm0, (%rax)
	movss	-4(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%r13), %xmm0
	jbe	.LBB2_36
# BB#35:                                #   in Loop: Header=BB2_34 Depth=1
	movss	%xmm0, (%r13)
.LBB2_36:                               #   in Loop: Header=BB2_34 Depth=1
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rbp)
	movq	(%rdi), %rax
	movss	%xmm0, (%rax)
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%r13), %xmm0
	jbe	.LBB2_38
# BB#37:                                #   in Loop: Header=BB2_34 Depth=1
	movss	%xmm0, (%r13)
.LBB2_38:                               #   in Loop: Header=BB2_34 Depth=1
	addq	$16, %rdi
	addq	$8, %rbp
	addq	$-2, %rsi
	jne	.LBB2_34
.LBB2_39:                               # %._crit_edge410
	movl	(%r14), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	testl	%r11d, %r11d
	jle	.LBB2_62
# BB#40:                                # %.lr.ph407
	movq	(%rdx), %rbx
	leaq	1(%r11), %rdx
	movl	%edx, %ebp
	testb	$1, %dl
	jne	.LBB2_44
# BB#41:
	movss	4(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	movss	%xmm0, 4(%r14)
	movss	%xmm0, 4(%rbx)
	movss	4(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movq	16(%rsp), %rax          # 8-byte Reload
	ucomiss	(%rax), %xmm0
	jbe	.LBB2_43
# BB#42:
	movss	%xmm0, (%rax)
.LBB2_43:                               # %.prol.loopexit515
	movl	$2, %eax
	cmpq	$2, %rbp
	jne	.LBB2_45
	jmp	.LBB2_51
.LBB2_44:
	movl	$1, %eax
	cmpq	$2, %rbp
	je	.LBB2_51
.LBB2_45:                               # %.lr.ph407.new
	movq	%rbp, %rsi
	subq	%rax, %rsi
	leaq	4(%r14,%rax,4), %rdi
	leaq	4(%rbx,%rax,4), %rbx
	.p2align	4, 0x90
.LBB2_46:                               # =>This Inner Loop Header: Depth=1
	movss	-4(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	movss	%xmm0, -4(%rdi)
	movss	%xmm0, -4(%rbx)
	movss	-4(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movq	16(%rsp), %rax          # 8-byte Reload
	ucomiss	(%rax), %xmm0
	jbe	.LBB2_48
# BB#47:                                #   in Loop: Header=BB2_46 Depth=1
	movss	%xmm0, (%rax)
.LBB2_48:                               #   in Loop: Header=BB2_46 Depth=1
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rdi)
	movss	%xmm0, (%rbx)
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%rax), %xmm0
	jbe	.LBB2_50
# BB#49:                                #   in Loop: Header=BB2_46 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movss	%xmm0, (%rax)
.LBB2_50:                               #   in Loop: Header=BB2_46 Depth=1
	addq	$8, %rdi
	addq	$8, %rbx
	addq	$-2, %rsi
	jne	.LBB2_46
.LBB2_51:                               # %.preheader361
	testl	%r11d, %r11d
	jle	.LBB2_62
# BB#52:                                # %.lr.ph403
	movq	MSalign11.m(%rip), %rsi
	movq	MSalign11.mp(%rip), %rdi
	leaq	-1(%rbp), %r11
	cmpq	$7, %r11
	jbe	.LBB2_56
# BB#53:                                # %min.iters.checked
	movq	24(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill> %R8<def>
	andl	$7, %r8d
	subq	%r8, %r11
	je	.LBB2_56
# BB#54:                                # %vector.memcheck
	leaq	4(%rsi), %rax
	leaq	-4(%r14,%rbp,4), %rcx
	cmpq	%rcx, %rax
	jae	.LBB2_155
# BB#55:                                # %vector.memcheck
	leaq	(%rsi,%rbp,4), %rax
	cmpq	%rax, %r14
	jae	.LBB2_155
.LBB2_56:
	movl	$1, %ebp
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
.LBB2_57:                               # %scalar.ph.preheader
	movl	%edx, %ebx
	subl	%ebp, %edx
	leaq	-1(%rbx), %rax
	subq	%rbp, %rax
	andq	$3, %rdx
	je	.LBB2_60
# BB#58:                                # %scalar.ph.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB2_59:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r14,%rbp,4), %ecx
	movl	%ecx, (%rsi,%rbp,4)
	movl	$0, (%rdi,%rbp,4)
	incq	%rbp
	incq	%rdx
	jne	.LBB2_59
.LBB2_60:                               # %scalar.ph.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB2_62
	.p2align	4, 0x90
.LBB2_61:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r14,%rbp,4), %eax
	movl	%eax, (%rsi,%rbp,4)
	movl	$0, (%rdi,%rbp,4)
	movl	(%r14,%rbp,4), %eax
	movl	%eax, 4(%rsi,%rbp,4)
	movl	$0, 4(%rdi,%rbp,4)
	movl	4(%r14,%rbp,4), %eax
	movl	%eax, 8(%rsi,%rbp,4)
	movl	$0, 8(%rdi,%rbp,4)
	movl	8(%r14,%rbp,4), %eax
	movl	%eax, 12(%rsi,%rbp,4)
	movl	$0, 12(%rdi,%rbp,4)
	addq	$4, %rbp
	cmpq	%rbp, %rbx
	jne	.LBB2_61
.LBB2_62:                               # %._crit_edge404
	movslq	%r11d, %rax
	testl	%r15d, %r15d
	movl	-4(%r14,%rax,4), %ecx
	leaq	-1(%rax), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	MSalign11.lastverticalw(%rip), %rax
	movl	%ecx, (%rax)
	movss	.LCPI2_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movq	%r13, 40(%rsp)          # 8-byte Spill
	jle	.LBB2_91
# BB#63:                                # %.lr.ph399
	movslq	%r11d, %r12
	movq	%r15, %rax
	leal	1(%r12), %r15d
	leal	1(%rax), %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%r11d, %eax
	andl	$1, %eax
	movl	%eax, 92(%rsp)          # 4-byte Spill
	leal	-1(%r11), %eax
	movl	%eax, 88(%rsp)          # 4-byte Spill
	xorps	%xmm4, %xmm4
	movl	$1, %r13d
	jmp	.LBB2_66
	.p2align	4, 0x90
.LBB2_64:                               # %._crit_edge392
                                        #   in Loop: Header=BB2_66 Depth=1
	movq	96(%rsp), %rdx          # 8-byte Reload
	movl	(%rbp,%rdx,4), %eax
	movq	MSalign11.lastverticalw(%rip), %rcx
	movl	%eax, (%rcx,%r13,4)
	incq	%r13
	cmpq	56(%rsp), %r13          # 8-byte Folded Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movss	.LCPI2_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	je	.LBB2_87
# BB#65:                                # %._crit_edge392._crit_edge
                                        #   in Loop: Header=BB2_66 Depth=1
	movq	MSalign11.initverticalw(%rip), %r10
	movq	%r14, %r9
	movq	%rbp, %r14
.LBB2_66:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_70 Depth 2
                                        #     Child Loop BB2_73 Depth 2
	movq	%r9, %rbp
	testl	%r11d, %r11d
	movl	-4(%r10,%r13,4), %eax
	movl	%eax, (%r14)
	je	.LBB2_71
# BB#67:                                # %.lr.ph.i350
                                        #   in Loop: Header=BB2_66 Depth=1
	cmpl	$0, 92(%rsp)            # 4-byte Folded Reload
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	(%r8), %rcx
	movsbq	(%rcx,%r13), %rbx
	jne	.LBB2_69
# BB#68:                                #   in Loop: Header=BB2_66 Depth=1
	movq	%rbp, %rsi
	movl	%r11d, %edi
	cmpl	$1, %r11d
	jne	.LBB2_70
	jmp	.LBB2_71
	.p2align	4, 0x90
.LBB2_69:                               #   in Loop: Header=BB2_66 Depth=1
	movsbq	(%rax), %rcx
	incq	%rax
	movq	%rbx, %rsi
	shlq	$9, %rsi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rsi,%rcx,4), %xmm0
	leaq	4(%rbp), %rsi
	movss	%xmm0, (%rbp)
	movl	88(%rsp), %edi          # 4-byte Reload
	cmpl	$1, %r11d
	je	.LBB2_71
	.p2align	4, 0x90
.LBB2_70:                               #   Parent Loop BB2_66 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	(%rax), %rcx
	movq	%rbx, %rdx
	shlq	$9, %rdx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rdx,%rcx,4), %xmm0
	movss	%xmm0, (%rsi)
	addl	$-2, %edi
	movsbq	1(%rax), %rcx
	leaq	2(%rax), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rdx,%rcx,4), %xmm0
	movss	%xmm0, 4(%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB2_70
.LBB2_71:                               # %match_calc.exit355
                                        #   in Loop: Header=BB2_66 Depth=1
	movl	(%r10,%r13,4), %eax
	movl	%eax, (%rbp)
	movl	(%r14), %ecx
	movl	%ecx, MSalign11.mi(%rip)
	movl	$0, MSalign11.mpi(%rip)
	movd	%eax, %xmm1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rbx
	movl	%eax, (%rbx,%r13,4)
	movq	stderr(%rip), %rdi
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movl	$.L.str.3, %esi
	movb	$2, %al
	movss	%xmm4, 52(%rsp)         # 4-byte Spill
	callq	fprintf
	movss	52(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movq	%rbx, %r10
	movq	24(%rsp), %r11          # 8-byte Reload
	testl	%r11d, %r11d
	movss	36(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	jle	.LBB2_64
# BB#72:                                # %.lr.ph391
                                        #   in Loop: Header=BB2_66 Depth=1
	leaq	-1(%r13), %r8
	movq	MSalign11.ijp(%rip), %rax
	movq	(%rax,%r13,8), %rcx
	movq	MSalign11.m(%rip), %rdx
	movq	MSalign11.mp(%rip), %r9
	movq	MSalign11.WMMTX(%rip), %rax
	movq	(%rax,%r13,8), %rdi
	movss	MSalign11.mi(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movl	$-1, %ebx
	movl	$1, %eax
	.p2align	4, 0x90
.LBB2_73:                               #   Parent Loop BB2_66 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-4(%r14,%rax,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movl	$0, (%rcx,%rax,4)
	movaps	%xmm3, %xmm2
	addss	%xmm0, %xmm2
	ucomiss	%xmm1, %xmm2
	movaps	%xmm1, %xmm4
	jbe	.LBB2_75
# BB#74:                                #   in Loop: Header=BB2_73 Depth=2
	movl	MSalign11.mpi(%rip), %esi
	addl	%ebx, %esi
	movl	%esi, (%rcx,%rax,4)
	movaps	%xmm2, %xmm4
.LBB2_75:                               #   in Loop: Header=BB2_73 Depth=2
	ucomiss	%xmm0, %xmm1
	jb	.LBB2_77
# BB#76:                                #   in Loop: Header=BB2_73 Depth=2
	movss	%xmm1, MSalign11.mi(%rip)
	leal	-1(%rax), %esi
	movl	%esi, MSalign11.mpi(%rip)
	movaps	%xmm1, %xmm0
.LBB2_77:                               #   in Loop: Header=BB2_73 Depth=2
	movss	(%rdx,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	addss	%xmm1, %xmm2
	ucomiss	%xmm4, %xmm2
	jbe	.LBB2_79
# BB#78:                                #   in Loop: Header=BB2_73 Depth=2
	movl	%r13d, %esi
	subl	(%r9,%rax,4), %esi
	movl	%esi, (%rcx,%rax,4)
	movaps	%xmm2, %xmm4
.LBB2_79:                               #   in Loop: Header=BB2_73 Depth=2
	movd	-4(%r14,%rax,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jb	.LBB2_81
# BB#80:                                #   in Loop: Header=BB2_73 Depth=2
	movd	%xmm2, (%rdx,%rax,4)
	movl	%r8d, (%r9,%rax,4)
.LBB2_81:                               #   in Loop: Header=BB2_73 Depth=2
	movss	(%rbp,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm1
	movss	%xmm1, (%rbp,%rax,4)
	movss	%xmm1, (%rdi,%rax,4)
	cmpq	%r12, %rax
	jge	.LBB2_86
# BB#82:                                #   in Loop: Header=BB2_73 Depth=2
	movss	(%rbp,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	(%r10,%r13,4), %xmm1
	jbe	.LBB2_84
# BB#83:                                #   in Loop: Header=BB2_73 Depth=2
	movss	%xmm1, (%r10,%r13,4)
	movss	(%rbp,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
.LBB2_84:                               #   in Loop: Header=BB2_73 Depth=2
	movq	40(%rsp), %rsi          # 8-byte Reload
	ucomiss	(%rsi,%rax,4), %xmm1
	jbe	.LBB2_86
# BB#85:                                #   in Loop: Header=BB2_73 Depth=2
	movq	40(%rsp), %rsi          # 8-byte Reload
	movss	%xmm1, (%rsi,%rax,4)
.LBB2_86:                               # %.critedge
                                        #   in Loop: Header=BB2_73 Depth=2
	incq	%rax
	decl	%ebx
	cmpq	%rax, %r15
	jne	.LBB2_73
	jmp	.LBB2_64
.LBB2_87:                               # %.preheader360
	movq	72(%rsp), %r15          # 8-byte Reload
	testl	%r15d, %r15d
	setg	%r8b
	jle	.LBB2_92
# BB#88:                                # %.lr.ph377.preheader
	movl	%r15d, %eax
	leaq	-1(%rax), %rdi
	movq	%r15, %rbx
	andq	$3, %rbx
	je	.LBB2_93
# BB#89:                                # %.lr.ph377.prol.preheader
	movss	.LCPI2_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_90:                               # %.lr.ph377.prol
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rcx,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	maxss	%xmm0, %xmm1
	cmoval	%edx, %r12d
	cmoval	%esi, %r13d
	incq	%rsi
	cmpq	%rsi, %rbx
	movaps	%xmm1, %xmm0
	jne	.LBB2_90
	jmp	.LBB2_94
.LBB2_91:
	xorl	%r8d, %r8d
	xorps	%xmm4, %xmm4
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	movq	16(%rsp), %r10          # 8-byte Reload
	jmp	.LBB2_98
.LBB2_92:
	xorl	%r8d, %r8d
	movq	%rbp, %r14
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	jmp	.LBB2_98
.LBB2_93:
	xorl	%esi, %esi
	movss	.LCPI2_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
.LBB2_94:                               # %.lr.ph377.prol.loopexit
	cmpq	$3, %rdi
	jb	.LBB2_97
	.p2align	4, 0x90
.LBB2_96:                               # %.lr.ph377
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rcx,%rsi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	leal	1(%rsi), %edi
	leal	2(%rsi), %ebx
	ucomiss	%xmm1, %xmm0
	maxss	%xmm1, %xmm0
	cmoval	%esi, %r13d
	movss	4(%rcx,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	cmoval	%edx, %r12d
	ucomiss	%xmm0, %xmm1
	maxss	%xmm0, %xmm1
	cmoval	%edi, %r13d
	movss	8(%rcx,%rsi,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	cmoval	%edx, %r12d
	ucomiss	%xmm1, %xmm0
	maxss	%xmm1, %xmm0
	movl	%r13d, %edi
	cmoval	%ebx, %edi
	movss	12(%rcx,%rsi,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	cmoval	%edx, %r12d
	leal	3(%rsi), %r13d
	ucomiss	%xmm0, %xmm1
	maxss	%xmm0, %xmm1
	cmoval	%edx, %r12d
	cmovbel	%edi, %r13d
	addq	$4, %rsi
	cmpq	%rax, %rsi
	jne	.LBB2_96
.LBB2_97:
	movq	%rbp, %r14
.LBB2_98:                               # %.preheader359
	testl	%r11d, %r11d
	movss	%xmm4, 52(%rsp)         # 4-byte Spill
	jle	.LBB2_104
# BB#99:                                # %.lr.ph369
	leal	-1(%r15), %ecx
	movl	%r11d, %edx
	leaq	-1(%rdx), %rdi
	xorl	%esi, %esi
	andq	$3, %r11
	je	.LBB2_102
# BB#100:                               # %.prol.preheader
	movaps	%xmm1, %xmm0
	.p2align	4, 0x90
.LBB2_101:                              # =>This Inner Loop Header: Depth=1
	movss	(%r14,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	maxss	%xmm0, %xmm1
	cmoval	%esi, %r12d
	cmoval	%ecx, %r13d
	incq	%rsi
	cmpq	%rsi, %r11
	movaps	%xmm1, %xmm0
	jne	.LBB2_101
.LBB2_102:                              # %.prol.loopexit
	cmpq	$3, %rdi
	jb	.LBB2_104
	.p2align	4, 0x90
.LBB2_103:                              # =>This Inner Loop Header: Depth=1
	movss	(%r14,%rsi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	leal	1(%rsi), %edi
	leal	2(%rsi), %ebp
	leal	3(%rsi), %ebx
	ucomiss	%xmm1, %xmm0
	maxss	%xmm1, %xmm0
	cmoval	%esi, %r12d
	movss	4(%r14,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	cmoval	%ecx, %r13d
	ucomiss	%xmm0, %xmm1
	maxss	%xmm0, %xmm1
	cmoval	%edi, %r12d
	movss	8(%r14,%rsi,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	cmoval	%ecx, %r13d
	ucomiss	%xmm1, %xmm0
	maxss	%xmm1, %xmm0
	cmoval	%ebp, %r12d
	movss	12(%r14,%rsi,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	cmoval	%ecx, %r13d
	ucomiss	%xmm0, %xmm1
	maxss	%xmm0, %xmm1
	cmoval	%ebx, %r12d
	cmoval	%ecx, %r13d
	addq	$4, %rsi
	cmpq	%rdx, %rsi
	jne	.LBB2_103
.LBB2_104:                              # %.preheader358
	movss	%xmm1, 56(%rsp)         # 4-byte Spill
	testb	%r8b, %r8b
	je	.LBB2_107
# BB#105:                               # %.lr.ph364.preheader
	movl	%r15d, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_106:                              # %.lr.ph364
                                        # =>This Inner Loop Header: Depth=1
	movq	stderr(%rip), %rdi
	movss	(%r10,%rbx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.4, %esi
	movb	$1, %al
	movl	%ebx, %edx
	callq	fprintf
	movq	16(%rsp), %r10          # 8-byte Reload
	incq	%rbx
	cmpq	%rbx, %rbp
	jne	.LBB2_106
.LBB2_107:                              # %.preheader
	movq	stderr(%rip), %rdi
	movq	24(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB2_110
# BB#108:                               # %.lr.ph.preheader
	movl	%eax, %ebx
	xorl	%r14d, %r14d
	movq	40(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_109:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rbp,%r14,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.5, %esi
	movb	$1, %al
	movl	%r14d, %edx
	callq	fprintf
	incq	%r14
	movq	stderr(%rip), %rdi
	cmpq	%r14, %rbx
	jne	.LBB2_109
.LBB2_110:                              # %._crit_edge
	movss	56(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.6, %esi
	movb	$1, %al
	movl	%r13d, %edx
	movl	%r12d, %ecx
	callq	fprintf
	movss	56(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	leal	-1(%r15), %eax
	cmpl	%eax, %r13d
	movss	36(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	je	.LBB2_112
# BB#111:                               # %._crit_edge
	movaps	%xmm0, %xmm1
.LBB2_112:                              # %._crit_edge
	cmpl	96(%rsp), %r12d         # 4-byte Folded Reload
	je	.LBB2_114
# BB#113:                               # %._crit_edge
	movaps	%xmm0, %xmm1
.LBB2_114:                              # %._crit_edge
	movss	%xmm1, 56(%rsp)         # 4-byte Spill
	movq	stderr(%rip), %rdi
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	movl	$.L.str.7, %esi
	movb	$1, %al
	callq	fprintf
	movq	24(%rsp), %r8           # 8-byte Reload
	leal	(%r8,%r15), %eax
	movq	MSalign11.mseq1(%rip), %rbx
	movq	(%rbx), %rcx
	movslq	%eax, %rbp
	leaq	(%rcx,%rbp), %rax
	movq	%rax, (%rbx)
	movb	$0, (%rcx,%rbp)
	movq	MSalign11.mseq2(%rip), %rax
	movq	(%rax), %rcx
	leaq	(%rcx,%rbp), %rdx
	movq	%rdx, (%rax)
	movb	$0, (%rcx,%rbp)
	movq	MSalign11.WMMTX(%rip), %rdi
	subq	$8, %rsp
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	movd	64(%rsp), %xmm0         # 4-byte Folded Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	movl	%r15d, %ecx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	pushq	%rax
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	movq	88(%rsp), %rax          # 8-byte Reload
	pushq	%rax
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	movq	40(%rsp), %r14          # 8-byte Reload
	pushq	%r14
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	movq	%rax, %r13
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	pushq	MSalign11.mp(%rip)
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	pushq	MSalign11.m(%rip)
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	pushq	MSalign11.initverticalw(%rip)
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	pushq	MSalign11.w2(%rip)
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	pushq	MSalign11.w1(%rip)
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	callq	backdp
	addq	$96, %rsp
.Lcfi63:
	.cfi_adjust_cfa_offset -96
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stderr(%rip), %rdi
	movq	MSalign11.mseq1(%rip), %rax
	movq	(%rax), %rdx
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	MSalign11.mseq2(%rip), %rax
	movq	(%rax), %rdx
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	MSalign11.mseq(%rip), %rax
	movq	(%rax), %rcx
	movq	MSalign11.mseq1(%rip), %rdx
	movq	%rcx, (%rdx)
	movq	8(%rax), %rax
	movq	MSalign11.mseq2(%rip), %rcx
	movq	%rax, (%rcx)
	movq	(%rdx), %rax
	leaq	(%rax,%rbp), %rcx
	movq	%rcx, (%rdx)
	movb	$0, (%rax,%rbp)
	movq	MSalign11.mseq2(%rip), %r15
	movq	(%r15), %rax
	leaq	(%rax,%rbp), %rcx
	movq	%rcx, (%r15)
	movb	$0, (%rax,%rbp)
	movq	MSalign11.mseq1(%rip), %rbp
	movq	MSalign11.ijp(%rip), %rbx
	movq	(%r14), %rdi
	callq	strlen
	movq	%rax, %r14
	movq	(%r13), %rdi
	callq	strlen
	testl	%r14d, %r14d
	js	.LBB2_120
# BB#115:                               # %.lr.ph19.preheader.i
	movq	%r14, %rdi
	incq	%rdi
	movl	%edi, %ecx
	leaq	-1(%rcx), %r8
	xorl	%esi, %esi
	andq	$7, %rdi
	je	.LBB2_117
	.p2align	4, 0x90
.LBB2_116:                              # %.lr.ph19.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rsi,8), %rdx
	incq	%rsi
	movl	%esi, (%rdx)
	cmpq	%rsi, %rdi
	jne	.LBB2_116
.LBB2_117:                              # %.lr.ph19.i.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB2_120
# BB#118:                               # %.lr.ph19.preheader.i.new
	negq	%rcx
	leaq	4(%rsi,%rcx), %r8
	leaq	56(%rbx,%rsi,8), %rdx
	leaq	4(%rsi), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_119:                              # %.lr.ph19.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rdx,%rdi,8), %r10
	leal	(%rsi,%rdi), %r9d
	leal	-3(%rsi,%rdi), %ecx
	movl	%ecx, (%r10)
	movq	-48(%rdx,%rdi,8), %r10
	leal	-2(%rsi,%rdi), %ecx
	movl	%ecx, (%r10)
	movq	-40(%rdx,%rdi,8), %r10
	leal	-1(%rsi,%rdi), %ecx
	movl	%ecx, (%r10)
	movq	-32(%rdx,%rdi,8), %rcx
	movl	%r9d, (%rcx)
	movq	-24(%rdx,%rdi,8), %r9
	leal	1(%rsi,%rdi), %ecx
	movl	%ecx, (%r9)
	movq	-16(%rdx,%rdi,8), %r9
	leal	2(%rsi,%rdi), %ecx
	movl	%ecx, (%r9)
	movq	-8(%rdx,%rdi,8), %r9
	leal	3(%rsi,%rdi), %ecx
	movl	%ecx, (%r9)
	movq	(%rdx,%rdi,8), %r9
	leal	4(%rsi,%rdi), %ecx
	movl	%ecx, (%r9)
	leaq	8(%r8,%rdi), %rcx
	addq	$8, %rdi
	cmpq	$4, %rcx
	jne	.LBB2_119
.LBB2_120:                              # %.preheader.i
	testl	%eax, %eax
	movq	8(%rsp), %r11           # 8-byte Reload
	js	.LBB2_129
# BB#121:                               # %.lr.ph15.i
	movq	(%rbx), %r8
	movq	%rax, %rdi
	incq	%rdi
	movl	%edi, %ecx
	cmpq	$7, %rcx
	jbe	.LBB2_126
# BB#122:                               # %min.iters.checked481
	andl	$7, %edi
	movq	%rcx, %r9
	subq	%rdi, %r9
	je	.LBB2_126
# BB#123:                               # %vector.body477.preheader
	leaq	16(%r8), %rdx
	movdqa	.LCPI2_2(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI2_3(%rip), %xmm1   # xmm1 = [4,4,4,4]
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI2_4(%rip), %xmm3   # xmm3 = [8,8,8,8]
	movq	%r9, %rsi
	.p2align	4, 0x90
.LBB2_124:                              # %vector.body477
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	pxor	%xmm2, %xmm5
	pxor	%xmm2, %xmm4
	movdqu	%xmm5, -16(%rdx)
	movdqu	%xmm4, (%rdx)
	paddd	%xmm3, %xmm0
	addq	$32, %rdx
	addq	$-8, %rsi
	jne	.LBB2_124
# BB#125:                               # %middle.block478
	testq	%rdi, %rdi
	jne	.LBB2_127
	jmp	.LBB2_129
.LBB2_126:
	xorl	%r9d, %r9d
.LBB2_127:                              # %scalar.ph479.preheader
	movl	%r9d, %edx
	notl	%edx
	leaq	(%r8,%r9,4), %rsi
	subq	%r9, %rcx
	.p2align	4, 0x90
.LBB2_128:                              # %scalar.ph479
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rsi)
	decl	%edx
	addq	$4, %rsi
	decq	%rcx
	jne	.LBB2_128
.LBB2_129:                              # %._crit_edge16.i
	leal	(%rax,%r14), %edx
	movq	(%rbp), %rcx
	movl	%edx, 40(%rsp)          # 4-byte Spill
	movslq	%edx, %rdx
	leaq	(%rcx,%rdx), %rsi
	movq	%rsi, (%rbp)
	movb	$0, (%rcx,%rdx)
	movq	(%r15), %rcx
	leaq	(%rcx,%rdx), %rsi
	movq	%rsi, (%r15)
	movb	$0, (%rcx,%rdx)
	testl	%edx, %edx
	js	.LBB2_152
# BB#130:                               # %.lr.ph11.i.preheader
	xorl	%r8d, %r8d
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_131:                              # %.lr.ph11.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_141 Depth 2
                                        #     Child Loop BB2_147 Depth 2
	movslq	%r14d, %rcx
	movq	(%rbx,%rcx,8), %rcx
	movslq	%eax, %rsi
	movl	(%rcx,%rsi,4), %r12d
	testl	%r12d, %r12d
	js	.LBB2_134
# BB#132:                               #   in Loop: Header=BB2_131 Depth=1
	je	.LBB2_135
# BB#133:                               #   in Loop: Header=BB2_131 Depth=1
	movl	%r14d, %r10d
	subl	%r12d, %r10d
	jmp	.LBB2_136
	.p2align	4, 0x90
.LBB2_134:                              #   in Loop: Header=BB2_131 Depth=1
	leal	-1(%r14), %r10d
	jmp	.LBB2_137
	.p2align	4, 0x90
.LBB2_135:                              #   in Loop: Header=BB2_131 Depth=1
	leal	-1(%r14), %r10d
.LBB2_136:                              #   in Loop: Header=BB2_131 Depth=1
	movl	$-1, %r12d
.LBB2_137:                              #   in Loop: Header=BB2_131 Depth=1
	movl	%r14d, %ecx
	subl	%r10d, %ecx
	decl	%ecx
	je	.LBB2_143
# BB#138:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB2_131 Depth=1
	movslq	%ecx, %rcx
	movslq	%r10d, %rsi
	leal	-2(%r14), %r9d
	testb	$1, %cl
	je	.LBB2_140
# BB#139:                               # %.lr.ph.i343.prol
                                        #   in Loop: Header=BB2_131 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rdx), %r11
	leaq	(%rcx,%rsi), %rdi
	movb	(%r11,%rdi), %r11b
	movq	(%rbp), %r13
	leaq	-1(%r13), %rdi
	movq	%rdi, (%rbp)
	movb	%r11b, -1(%r13)
	movq	64(%rsp), %r13          # 8-byte Reload
	movq	(%r15), %r11
	leaq	-1(%r11), %rdi
	movq	%rdi, (%r15)
	movb	$45, -1(%r11)
	movq	8(%rsp), %r11           # 8-byte Reload
	decq	%rcx
.LBB2_140:                              # %.lr.ph.i343.prol.loopexit
                                        #   in Loop: Header=BB2_131 Depth=1
	cmpl	%r10d, %r9d
	je	.LBB2_142
	.p2align	4, 0x90
.LBB2_141:                              # %.lr.ph.i343
                                        #   Parent Loop BB2_131 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r11), %rdi
	addq	%rsi, %rdi
	movzbl	(%rcx,%rdi), %edx
	movq	(%rbp), %rdi
	leaq	-1(%rdi), %rbx
	movq	%rbx, (%rbp)
	movb	%dl, -1(%rdi)
	movq	(%r15), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, (%r15)
	movb	$45, -1(%rdx)
	movq	(%r11), %rdx
	addq	%rsi, %rdx
	movzbl	-1(%rcx,%rdx), %edx
	movq	(%rbp), %rdi
	leaq	-1(%rdi), %rbx
	movq	%rbx, (%rbp)
	movb	%dl, -1(%rdi)
	movq	(%r15), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, (%r15)
	movb	$45, -1(%rdx)
	addq	$-2, %rcx
	testl	%ecx, %ecx
	jne	.LBB2_141
.LBB2_142:                              # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB2_131 Depth=1
	leal	-1(%r8,%r14), %r8d
	subl	%r10d, %r8d
.LBB2_143:                              # %._crit_edge.i
                                        #   in Loop: Header=BB2_131 Depth=1
	leal	(%r12,%rax), %r9d
	cmpl	$-1, %r12d
	je	.LBB2_149
# BB#144:                               # %.lr.ph4.preheader.i
                                        #   in Loop: Header=BB2_131 Depth=1
	movl	%r12d, %r11d
	notl	%r11d
	movslq	%r11d, %rsi
	movslq	%r9d, %rcx
	testb	$1, %sil
	je	.LBB2_146
# BB#145:                               # %.lr.ph4.i.prol
                                        #   in Loop: Header=BB2_131 Depth=1
	movq	(%rbp), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, (%rbp)
	movb	$45, -1(%rdx)
	movq	(%r13), %rdx
	leaq	(%rsi,%rcx), %rdi
	movb	(%rdx,%rdi), %dl
	movq	(%r15), %rdi
	leaq	-1(%rdi), %rbx
	movq	%rbx, (%r15)
	movb	%dl, -1(%rdi)
	decq	%rsi
.LBB2_146:                              # %.lr.ph4.i.prol.loopexit
                                        #   in Loop: Header=BB2_131 Depth=1
	cmpl	$-2, %r12d
	je	.LBB2_148
	.p2align	4, 0x90
.LBB2_147:                              # %.lr.ph4.i
                                        #   Parent Loop BB2_131 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, (%rbp)
	movb	$45, -1(%rdx)
	movq	(%r13), %rdx
	addq	%rcx, %rdx
	movzbl	(%rsi,%rdx), %edx
	movq	(%r15), %rdi
	leaq	-1(%rdi), %rbx
	movq	%rbx, (%r15)
	movb	%dl, -1(%rdi)
	movq	(%rbp), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, (%rbp)
	movb	$45, -1(%rdx)
	movq	(%r13), %rdx
	addq	%rcx, %rdx
	movzbl	-1(%rsi,%rdx), %edx
	movq	(%r15), %rdi
	leaq	-1(%rdi), %rbx
	movq	%rbx, (%r15)
	movb	%dl, -1(%rdi)
	addq	$-2, %rsi
	testl	%esi, %esi
	jne	.LBB2_147
.LBB2_148:                              # %._crit_edge5.loopexit.i
                                        #   in Loop: Header=BB2_131 Depth=1
	addl	%r11d, %r8d
	movq	8(%rsp), %r11           # 8-byte Reload
.LBB2_149:                              # %._crit_edge5.i
                                        #   in Loop: Header=BB2_131 Depth=1
	movq	16(%rsp), %rbx          # 8-byte Reload
	testl	%r14d, %r14d
	jle	.LBB2_152
# BB#150:                               # %._crit_edge5.i
                                        #   in Loop: Header=BB2_131 Depth=1
	testl	%eax, %eax
	jle	.LBB2_152
# BB#151:                               #   in Loop: Header=BB2_131 Depth=1
	movq	(%r11), %rax
	movslq	%r10d, %rcx
	movb	(%rax,%rcx), %al
	movq	(%rbp), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%rbp)
	movb	%al, -1(%rcx)
	movq	(%r13), %rax
	movslq	%r9d, %rcx
	movb	(%rax,%rcx), %al
	movq	(%r15), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%r15)
	movb	%al, -1(%rcx)
	addl	$2, %r8d
	cmpl	40(%rsp), %r8d          # 4-byte Folded Reload
	movl	%r10d, %r14d
	movl	%r9d, %eax
	jle	.LBB2_131
.LBB2_152:                              # %Atracking.exit
	movq	(%rbp), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rcx
	movl	84(%rsp), %edx          # 4-byte Reload
	cmpl	%edx, %ecx
	jg	.LBB2_158
# BB#153:                               # %Atracking.exit
	cmpl	$5000001, %ecx          # imm = 0x4C4B41
	jge	.LBB2_158
.LBB2_154:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	(%r13), %rdi
	movq	MSalign11.mseq2(%rip), %rax
	movq	(%rax), %rsi
	callq	strcpy
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stderr(%rip), %rdi
	movq	MSalign11.mseq1(%rip), %rax
	movq	(%rax), %rdx
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	MSalign11.mseq2(%rip), %rax
	movq	(%rax), %rdx
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	callq	fprintf
	movss	52(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_155:                              # %vector.body.preheader
	leaq	16(%r14), %rax
	leaq	20(%rsi), %rcx
	leaq	20(%rdi), %rbx
	xorpd	%xmm0, %xmm0
	leaq	1(%r11), %rbp
	.p2align	4, 0x90
.LBB2_156:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rax), %xmm1
	movdqu	(%rax), %xmm2
	movups	%xmm1, -16(%rcx)
	movdqu	%xmm2, (%rcx)
	movupd	%xmm0, -16(%rbx)
	movupd	%xmm0, (%rbx)
	addq	$32, %rax
	addq	$32, %rcx
	addq	$32, %rbx
	addq	$-8, %r11
	jne	.LBB2_156
# BB#157:                               # %middle.block
	testq	%r8, %r8
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
	jne	.LBB2_57
	jmp	.LBB2_62
.LBB2_158:
	movq	stderr(%rip), %rdi
	movl	$.L.str.10, %esi
	movl	$5000000, %r8d          # imm = 0x4C4B40
	xorl	%eax, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$.L.str.11, %edi
	callq	ErrorExit
	movq	MSalign11.mseq1(%rip), %rax
	movq	(%rax), %rbx
	jmp	.LBB2_154
.Lfunc_end2:
	.size	MSalign11, .Lfunc_end2-MSalign11
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"prevhiti = %d, lgth1 = %d\n"
	.size	.L.str, 27

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"prevhitj = %d, lgth2 = %d\n"
	.size	.L.str.1, 27

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"hit!\n"
	.size	.L.str.2, 6

	.type	MSalign11.mi,@object    # @MSalign11.mi
	.local	MSalign11.mi
	.comm	MSalign11.mi,4,4
	.type	MSalign11.m,@object     # @MSalign11.m
	.local	MSalign11.m
	.comm	MSalign11.m,8,8
	.type	MSalign11.ijp,@object   # @MSalign11.ijp
	.local	MSalign11.ijp
	.comm	MSalign11.ijp,8,8
	.type	MSalign11.mpi,@object   # @MSalign11.mpi
	.local	MSalign11.mpi
	.comm	MSalign11.mpi,4,4
	.type	MSalign11.mp,@object    # @MSalign11.mp
	.local	MSalign11.mp
	.comm	MSalign11.mp,8,8
	.type	MSalign11.w1,@object    # @MSalign11.w1
	.local	MSalign11.w1
	.comm	MSalign11.w1,8,8
	.type	MSalign11.w2,@object    # @MSalign11.w2
	.local	MSalign11.w2
	.comm	MSalign11.w2,8,8
	.type	MSalign11.match,@object # @MSalign11.match
	.local	MSalign11.match
	.comm	MSalign11.match,8,8
	.type	MSalign11.initverticalw,@object # @MSalign11.initverticalw
	.local	MSalign11.initverticalw
	.comm	MSalign11.initverticalw,8,8
	.type	MSalign11.lastverticalw,@object # @MSalign11.lastverticalw
	.local	MSalign11.lastverticalw
	.comm	MSalign11.lastverticalw,8,8
	.type	MSalign11.mseq1,@object # @MSalign11.mseq1
	.local	MSalign11.mseq1
	.comm	MSalign11.mseq1,8,8
	.type	MSalign11.mseq2,@object # @MSalign11.mseq2
	.local	MSalign11.mseq2
	.comm	MSalign11.mseq2,8,8
	.type	MSalign11.mseq,@object  # @MSalign11.mseq
	.local	MSalign11.mseq
	.comm	MSalign11.mseq,8,8
	.type	MSalign11.cpmx1,@object # @MSalign11.cpmx1
	.local	MSalign11.cpmx1
	.comm	MSalign11.cpmx1,8,8
	.type	MSalign11.cpmx2,@object # @MSalign11.cpmx2
	.local	MSalign11.cpmx2
	.comm	MSalign11.cpmx2,8,8
	.type	MSalign11.intwork,@object # @MSalign11.intwork
	.local	MSalign11.intwork
	.comm	MSalign11.intwork,8,8
	.type	MSalign11.WMMTX,@object # @MSalign11.WMMTX
	.local	MSalign11.WMMTX
	.comm	MSalign11.WMMTX,8,8
	.type	MSalign11.floatwork,@object # @MSalign11.floatwork
	.local	MSalign11.floatwork
	.comm	MSalign11.floatwork,8,8
	.type	MSalign11.orlgth1,@object # @MSalign11.orlgth1
	.local	MSalign11.orlgth1
	.comm	MSalign11.orlgth1,4,4
	.type	MSalign11.orlgth2,@object # @MSalign11.orlgth2
	.local	MSalign11.orlgth2
	.comm	MSalign11.orlgth2,4,4
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"currentw[0]  = %f, *maxinwpt = %f\n"
	.size	.L.str.3, 35

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"maxinw[%d] = %f\n"
	.size	.L.str.4, 17

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"maxinh[%d] = %f\n"
	.size	.L.str.5, 17

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"wmmax = %f (%d,%d)\n"
	.size	.L.str.6, 20

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"wmmax = %f\n"
	.size	.L.str.7, 12

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	">MSres\n%s\n"
	.size	.L.str.9, 11

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"alloclen=%d, resultlen=%d, N=%d\n"
	.size	.L.str.10, 33

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"LENGTH OVER!\n"
	.size	.L.str.11, 14

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	">\n%s\n"
	.size	.L.str.12, 6

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"i=%d, prevhiti=%d\n"
	.size	.L.str.13, 19

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"j=%d, prevhitj=%d\n"
	.size	.L.str.14, 19

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"l=%d\n"
	.size	.L.str.15, 6

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"added %c to mseq1, mseq1 = %s \n"
	.size	.L.str.16, 32

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"added %c to mseq2, mseq2 = %s \n"
	.size	.L.str.17, 32


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
