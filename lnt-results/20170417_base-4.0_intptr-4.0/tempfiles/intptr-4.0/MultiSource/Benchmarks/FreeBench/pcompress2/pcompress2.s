	.text
	.file	"pcompress2.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$1008, %rsp             # imm = 0x3F0
.Lcfi3:
	.cfi_def_cfa_offset 1040
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	movl	$.L.str.3, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	compress
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	compress
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	compress
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	compress
	movq	8(%rbx), %rsi
	movq	%rsp, %r14
	movq	%r14, %rdi
	callq	strcpy
	movq	%r14, %rdi
	callq	strlen
	movb	$0, 6(%rsp,%rax)
	movw	$29296, 4(%rsp,%rax)    # imm = 0x7270
	movl	$1836016430, (%rsp,%rax) # imm = 0x6D6F632E
	movq	%r14, 8(%rbx)
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	uncompress
	movq	%r14, %rdi
	callq	remove
	xorl	%eax, %eax
	addq	$1008, %rsp             # imm = 0x3F0
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Compile date: %s\n"
	.size	.L.str, 18

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"today"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Compiler switches: %s\n"
	.size	.L.str.2, 23

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.zero	1
	.size	.L.str.3, 1

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	".compr"
	.size	.L.str.4, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
