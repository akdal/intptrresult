	.text
	.file	"libclamav_nsis_bzlib.bc"
	.globl	nsis_BZ2_bzDecompressInit
	.p2align	4, 0x90
	.type	nsis_BZ2_bzDecompressInit,@function
nsis_BZ2_bzDecompressInit:              # @nsis_BZ2_bzDecompressInit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebx
	movq	%rdi, %rbp
	cmpl	$4, %ebx
	movl	$-2, %eax
	ja	.LBB0_10
# BB#1:
	testq	%rbp, %rbp
	je	.LBB0_10
# BB#2:
	cmpl	$1, %r14d
	ja	.LBB0_10
# BB#3:
	movq	56(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB0_5
# BB#4:
	movq	$default_bzalloc, 56(%rbp)
	movl	$default_bzalloc, %eax
.LBB0_5:
	cmpq	$0, 64(%rbp)
	jne	.LBB0_7
# BB#6:
	movq	$default_bzfree, 64(%rbp)
.LBB0_7:
	movq	72(%rbp), %rdi
	movl	$64144, %esi            # imm = 0xFA90
	movl	$1, %edx
	callq	*%rax
	testq	%rax, %rax
	je	.LBB0_8
# BB#9:
	movq	%rbp, (%rax)
	movq	%rax, 48(%rbp)
	movl	$10, 8(%rax)
	movl	$0, 36(%rax)
	movl	$0, 32(%rax)
	movl	$0, 3188(%rax)
	movl	$0, 12(%rbp)
	movl	$0, 16(%rbp)
	movl	$0, 36(%rbp)
	movl	$0, 40(%rbp)
	movb	%r14b, 44(%rax)
	movl	$0, 48(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 3152(%rax)
	movq	$0, 3168(%rax)
	movl	%ebx, 52(%rax)
	xorl	%eax, %eax
	jmp	.LBB0_10
.LBB0_8:
	movl	$-3, %eax
.LBB0_10:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	nsis_BZ2_bzDecompressInit, .Lfunc_end0-nsis_BZ2_bzDecompressInit
	.cfi_endproc

	.p2align	4, 0x90
	.type	default_bzalloc,@function
default_bzalloc:                        # @default_bzalloc
	.cfi_startproc
# BB#0:
	imull	%edx, %esi
	movslq	%esi, %rdi
	jmp	cli_malloc              # TAILCALL
.Lfunc_end1:
	.size	default_bzalloc, .Lfunc_end1-default_bzalloc
	.cfi_endproc

	.p2align	4, 0x90
	.type	default_bzfree,@function
default_bzfree:                         # @default_bzfree
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB2_1
# BB#2:
	movq	%rsi, %rdi
	jmp	free                    # TAILCALL
.LBB2_1:
	retq
.Lfunc_end2:
	.size	default_bzfree, .Lfunc_end2-default_bzfree
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	9                       # 0x9
	.byte	10                      # 0xa
	.byte	11                      # 0xb
	.byte	12                      # 0xc
	.byte	13                      # 0xd
	.byte	14                      # 0xe
	.byte	15                      # 0xf
.LCPI3_1:
	.byte	16                      # 0x10
	.byte	17                      # 0x11
	.byte	18                      # 0x12
	.byte	19                      # 0x13
	.byte	20                      # 0x14
	.byte	21                      # 0x15
	.byte	22                      # 0x16
	.byte	23                      # 0x17
	.byte	24                      # 0x18
	.byte	25                      # 0x19
	.byte	26                      # 0x1a
	.byte	27                      # 0x1b
	.byte	28                      # 0x1c
	.byte	29                      # 0x1d
	.byte	30                      # 0x1e
	.byte	31                      # 0x1f
.LCPI3_2:
	.long	32                      # 0x20
	.long	32                      # 0x20
	.long	32                      # 0x20
	.long	32                      # 0x20
.LCPI3_3:
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.text
	.globl	nsis_BZ2_bzDecompress
	.p2align	4, 0x90
	.type	nsis_BZ2_bzDecompress,@function
nsis_BZ2_bzDecompress:                  # @nsis_BZ2_bzDecompress
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	subq	$536, %rsp              # imm = 0x218
.Lcfi12:
	.cfi_def_cfa_offset 592
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	$-2, %eax
	testq	%rdi, %rdi
	je	.LBB3_3
# BB#1:
	movq	48(%rdi), %r14
	testq	%r14, %r14
	je	.LBB3_3
# BB#2:
	cmpq	%rdi, (%r14)
	je	.LBB3_4
.LBB3_3:                                # %unRLE_obuf_to_output_SMALL.exit.thread
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_4:                                # %.preheader
	leaq	64036(%r14), %r13
	leaq	3196(%r14), %rax
	movq	%rax, 480(%rsp)         # 8-byte Spill
	leaq	68(%r14), %rax
	movq	%rax, 472(%rsp)         # 8-byte Spill
	movl	8(%r14), %ecx
	leaq	43892(%r14), %rax
	movq	%rax, 464(%rsp)         # 8-byte Spill
	leaq	43888(%r14), %rax
	movq	%rax, 456(%rsp)         # 8-byte Spill
	leaq	51648(%r14), %rax
	movq	%rax, 448(%rsp)         # 8-byte Spill
	pxor	%xmm9, %xmm9
	movdqa	.LCPI3_0(%rip), %xmm2   # xmm2 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
	movdqa	.LCPI3_1(%rip), %xmm3   # xmm3 = [16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
	movq	%r13, 256(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB3_5:                                # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_54 Depth 2
                                        #       Child Loop BB3_61 Depth 3
                                        #       Child Loop BB3_65 Depth 3
                                        #       Child Loop BB3_69 Depth 3
                                        #       Child Loop BB3_73 Depth 3
                                        #       Child Loop BB3_75 Depth 3
                                        #     Child Loop BB3_39 Depth 2
                                        #     Child Loop BB3_87 Depth 2
                                        #     Child Loop BB3_100 Depth 2
                                        #     Child Loop BB3_109 Depth 2
                                        #     Child Loop BB3_118 Depth 2
                                        #     Child Loop BB3_203 Depth 2
                                        #     Child Loop BB3_226 Depth 2
                                        #     Child Loop BB3_216 Depth 2
                                        #     Child Loop BB3_234 Depth 2
                                        #     Child Loop BB3_133 Depth 2
                                        #     Child Loop BB3_160 Depth 2
                                        #     Child Loop BB3_166 Depth 2
                                        #     Child Loop BB3_169 Depth 2
                                        #       Child Loop BB3_172 Depth 3
                                        #       Child Loop BB3_175 Depth 3
                                        #     Child Loop BB3_249 Depth 2
                                        #       Child Loop BB3_253 Depth 3
                                        #       Child Loop BB3_257 Depth 3
                                        #       Child Loop BB3_259 Depth 3
                                        #         Child Loop BB3_265 Depth 4
                                        #       Child Loop BB3_276 Depth 3
                                        #       Child Loop BB3_280 Depth 3
                                        #       Child Loop BB3_288 Depth 3
                                        #       Child Loop BB3_291 Depth 3
                                        #     Child Loop BB3_303 Depth 2
                                        #     Child Loop BB3_308 Depth 2
                                        #     Child Loop BB3_419 Depth 2
                                        #     Child Loop BB3_446 Depth 2
                                        #     Child Loop BB3_449 Depth 2
                                        #     Child Loop BB3_403 Depth 2
                                        #     Child Loop BB3_408 Depth 2
                                        #     Child Loop BB3_412 Depth 2
                                        #     Child Loop BB3_459 Depth 2
                                        #     Child Loop BB3_192 Depth 2
                                        #     Child Loop BB3_389 Depth 2
                                        #     Child Loop BB3_424 Depth 2
                                        #     Child Loop BB3_471 Depth 2
                                        #     Child Loop BB3_183 Depth 2
                                        #     Child Loop BB3_360 Depth 2
                                        #     Child Loop BB3_362 Depth 2
                                        #     Child Loop BB3_365 Depth 2
                                        #     Child Loop BB3_428 Depth 2
                                        #     Child Loop BB3_431 Depth 2
                                        #     Child Loop BB3_437 Depth 2
                                        #     Child Loop BB3_442 Depth 2
                                        #     Child Loop BB3_484 Depth 2
                                        #     Child Loop BB3_347 Depth 2
                                        #     Child Loop BB3_295 Depth 2
                                        #     Child Loop BB3_323 Depth 2
                                        #     Child Loop BB3_331 Depth 2
                                        #     Child Loop BB3_141 Depth 2
                                        #       Child Loop BB3_144 Depth 3
	cmpl	$2, %ecx
	je	.LBB3_12
# BB#6:                                 # %.outer
                                        #   in Loop: Header=BB3_5 Depth=1
	cmpl	$1, %ecx
	je	.LBB3_493
# BB#7:                                 # %.outer.split.split
                                        #   in Loop: Header=BB3_5 Depth=1
	cmpl	$9, %ecx
	jle	.LBB3_492
# BB#8:                                 # %.us-lcssa136
                                        #   in Loop: Header=BB3_5 Depth=1
	cmpl	$10, %ecx
	jne	.LBB3_15
# BB#9:                                 # %.thread.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rbp
	movdqu	%xmm9, 92(%r13)
	movdqu	%xmm9, 80(%r13)
	movdqu	%xmm9, 64(%r13)
	movdqu	%xmm9, 48(%r13)
	movdqu	%xmm9, 32(%r13)
	movdqu	%xmm9, 16(%r13)
	movdqu	%xmm9, (%r13)
	movl	$9, 40(%r14)
	cmpb	$0, 44(%r14)
	movq	56(%rbp), %rax
	movq	72(%rbp), %rdi
	je	.LBB3_46
# BB#10:                                #   in Loop: Header=BB3_5 Depth=1
	movl	$1800000, %esi          # imm = 0x1B7740
	movl	$1, %edx
	callq	*%rax
	movq	%rax, 3160(%r14)
	movq	72(%rbp), %rdi
	imull	$100000, 40(%r14), %esi # imm = 0x186A0
	sarl	%esi
	movl	$1, %edx
	callq	*56(%rbp)
	movq	%rax, 3168(%r14)
	xorl	%r8d, %r8d
	testq	%rax, %rax
	movl	$-3, %eax
	je	.LBB3_48
# BB#11:                                #   in Loop: Header=BB3_5 Depth=1
	movq	3160(%r14), %rcx
	testq	%rcx, %rcx
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$0, %r9d
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movl	$0, %ebp
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	movl	$0, 32(%rsp)            # 4-byte Folded Spill
	movl	$0, %r10d
	movl	$0, 40(%rsp)            # 4-byte Folded Spill
	movl	$0, %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	$0, %r11d
	movl	$0, %r15d
	movl	$0, %ecx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movl	$0, %ecx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movl	$0, %ecx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movl	$0, %r12d
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movl	$0, %ecx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movl	$0, 60(%rsp)            # 4-byte Folded Spill
	movl	$0, %ecx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
	movl	$0, %ecx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movl	$0, %ecx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movl	$0, %ecx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	pxor	%xmm9, %xmm9
	movdqa	.LCPI3_0(%rip), %xmm2   # xmm2 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
	movdqa	.LCPI3_1(%rip), %xmm3   # xmm3 = [16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
	jne	.LBB3_84
	jmp	.LBB3_490
	.p2align	4, 0x90
.LBB3_12:                               # %.outer.split.split.us
                                        #   in Loop: Header=BB3_5 Depth=1
	cmpb	$0, 44(%r14)
	je	.LBB3_16
# BB#13:                                #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rsi
	cmpl	$0, 32(%rsi)
	je	.LBB3_79
# BB#14:                                # %.lr.ph.lr.ph.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	16(%r14), %edx
	.p2align	4, 0x90
.LBB3_54:                               #   Parent Loop BB3_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_61 Depth 3
                                        #       Child Loop BB3_65 Depth 3
                                        #       Child Loop BB3_69 Depth 3
                                        #       Child Loop BB3_73 Depth 3
                                        #       Child Loop BB3_75 Depth 3
	testl	%edx, %edx
	je	.LBB3_58
# BB#55:                                #   in Loop: Header=BB3_54 Depth=2
	movb	12(%r14), %al
	movq	24(%rsi), %rcx
	movb	%al, (%rcx)
	movl	16(%r14), %edx
	decl	%edx
	movl	%edx, 16(%r14)
	movq	(%r14), %rsi
	incq	24(%rsi)
	movl	32(%rsi), %eax
	decl	%eax
	movl	%eax, 32(%rsi)
	incl	36(%rsi)
	jne	.LBB3_57
# BB#56:                                #   in Loop: Header=BB3_54 Depth=2
	incl	40(%rsi)
.LBB3_57:                               # %.backedge.i
                                        #   in Loop: Header=BB3_54 Depth=2
	testl	%eax, %eax
	jne	.LBB3_54
	jmp	.LBB3_79
.LBB3_58:                               #   in Loop: Header=BB3_54 Depth=2
	movl	1092(%r14), %ebx
	movl	64080(%r14), %r12d
	leal	1(%r12), %eax
	cmpl	%eax, %ebx
	je	.LBB3_79
# BB#59:                                #   in Loop: Header=BB3_54 Depth=2
	jg	.LBB3_495
# BB#60:                                #   in Loop: Header=BB3_54 Depth=2
	movl	$1, 16(%r14)
	movl	64(%r14), %r11d
	movb	%r11b, 12(%r14)
	movl	60(%r14), %ecx
	xorl	%ebp, %ebp
	movl	$256, %eax              # imm = 0x100
	.p2align	4, 0x90
.LBB3_61:                               #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_54 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rbp,%rax), %edx
	sarl	%edx
	movslq	%edx, %rdx
	cmpl	%ecx, 1096(%r14,%rdx,4)
	cmovgl	%edx, %eax
	cmovlel	%edx, %ebp
	movl	%eax, %edx
	subl	%ebp, %edx
	cmpl	$1, %edx
	jne	.LBB3_61
# BB#62:                                # %indexIntoF.exit.i
                                        #   in Loop: Header=BB3_54 Depth=2
	movq	3160(%r14), %r10
	movq	3168(%r14), %r9
	movzwl	(%r10,%rcx,2), %eax
	movl	%ecx, %edx
	shrl	%edx
	movzbl	(%r9,%rdx), %edi
	shlb	$2, %cl
	andb	$4, %cl
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrl	%cl, %edi
	andl	$15, %edi
	shll	$16, %edi
	orl	%eax, %edi
	movl	%edi, 60(%r14)
	leal	1(%rbx), %r15d
	movl	%r15d, 1092(%r14)
	movl	$1, %edx
	cmpl	%r12d, %ebx
	je	.LBB3_78
# BB#63:                                #   in Loop: Header=BB3_54 Depth=2
	movzbl	%bpl, %ecx
	cmpl	%r11d, %ecx
	jne	.LBB3_77
# BB#64:                                #   in Loop: Header=BB3_54 Depth=2
	movl	$2, 16(%r14)
	xorl	%ebp, %ebp
	movl	$256, %ecx              # imm = 0x100
	.p2align	4, 0x90
.LBB3_65:                               #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_54 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rbp,%rcx), %edx
	sarl	%edx
	movslq	%edx, %rdx
	cmpl	%edi, 1096(%r14,%rdx,4)
	cmovgl	%edx, %ecx
	cmovlel	%edx, %ebp
	movl	%ecx, %edx
	subl	%ebp, %edx
	cmpl	$1, %edx
	jne	.LBB3_65
# BB#66:                                # %indexIntoF.exit103.i
                                        #   in Loop: Header=BB3_54 Depth=2
	movl	%edi, %ecx
	movzwl	(%r10,%rcx,2), %r8d
	shrl	%edi
	movzbl	(%r9,%rdi), %edi
	shlb	$2, %al
	andb	$4, %al
	movl	%eax, %ecx
	shrl	%cl, %edi
	andl	$15, %edi
	shll	$16, %edi
	orl	%r8d, %edi
	movl	%edi, 60(%r14)
	leal	2(%rbx), %eax
	movl	%eax, 1092(%r14)
	movl	$2, %edx
	cmpl	%r12d, %r15d
	je	.LBB3_78
# BB#67:                                #   in Loop: Header=BB3_54 Depth=2
	movzbl	%bpl, %ecx
	cmpl	%r11d, %ecx
	jne	.LBB3_77
# BB#68:                                #   in Loop: Header=BB3_54 Depth=2
	movl	$3, 16(%r14)
	xorl	%ebp, %ebp
	movl	$256, %ecx              # imm = 0x100
	.p2align	4, 0x90
.LBB3_69:                               #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_54 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rbp,%rcx), %edx
	sarl	%edx
	movslq	%edx, %rdx
	cmpl	%edi, 1096(%r14,%rdx,4)
	cmovgl	%edx, %ecx
	cmovlel	%edx, %ebp
	movl	%ecx, %edx
	subl	%ebp, %edx
	cmpl	$1, %edx
	jne	.LBB3_69
# BB#70:                                # %indexIntoF.exit98.i
                                        #   in Loop: Header=BB3_54 Depth=2
	movl	%edi, %ecx
	movzwl	(%r10,%rcx,2), %r15d
	shrl	%edi
	movzbl	(%r9,%rdi), %edi
	shlb	$2, %r8b
	andb	$4, %r8b
	movl	%r8d, %ecx
	shrl	%cl, %edi
	andl	$15, %edi
	shll	$16, %edi
	orl	%r15d, %edi
	movl	%edi, 60(%r14)
	leal	3(%rbx), %ecx
	movl	%ecx, 1092(%r14)
	movl	$3, %edx
	cmpl	%r12d, %eax
	je	.LBB3_78
# BB#71:                                #   in Loop: Header=BB3_54 Depth=2
	movzbl	%bpl, %eax
	cmpl	%r11d, %eax
	jne	.LBB3_53
# BB#72:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB3_54 Depth=2
	xorl	%edx, %edx
	movl	$256, %eax              # imm = 0x100
.LBB3_73:                               # %.preheader.i
                                        #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_54 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rdx,%rax), %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	cmpl	%edi, 1096(%r14,%rcx,4)
	cmovgl	%ecx, %eax
	cmovlel	%ecx, %edx
	movl	%eax, %ecx
	subl	%edx, %ecx
	cmpl	$1, %ecx
	jne	.LBB3_73
# BB#74:                                # %indexIntoF.exit93.i
                                        #   in Loop: Header=BB3_54 Depth=2
	movl	%edi, %eax
	movzwl	(%r10,%rax,2), %r8d
	shrl	%edi
	movzbl	(%r9,%rdi), %eax
	shlb	$2, %r15b
	andb	$4, %r15b
	movl	%r15d, %ecx
	shrl	%cl, %eax
	andl	$15, %eax
	shll	$16, %eax
	orl	%r8d, %eax
	movl	%eax, 60(%r14)
	leal	4(%rbx), %ecx
	movl	%ecx, 1092(%r14)
	movzbl	%dl, %edx
	addl	$4, %edx
	movl	%edx, 16(%r14)
	xorl	%ecx, %ecx
	movl	$256, %edi              # imm = 0x100
.LBB3_75:                               #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_54 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rcx,%rdi), %ebp
	sarl	%ebp
	movslq	%ebp, %rbp
	cmpl	%eax, 1096(%r14,%rbp,4)
	cmovgl	%ebp, %edi
	cmovlel	%ebp, %ecx
	movl	%edi, %ebp
	subl	%ecx, %ebp
	cmpl	$1, %ebp
	jne	.LBB3_75
# BB#76:                                # %indexIntoF.exit88.i
                                        #   in Loop: Header=BB3_54 Depth=2
	movl	%ecx, 64(%r14)
	movl	%eax, %ecx
	movzwl	(%r10,%rcx,2), %edi
	shrl	%eax
	movzbl	(%r9,%rax), %eax
	shlb	$2, %r8b
	andb	$4, %r8b
	movl	%r8d, %ecx
	shrl	%cl, %eax
	andl	$15, %eax
	shll	$16, %eax
	orl	%edi, %eax
	movl	%eax, 60(%r14)
	addl	$5, %ebx
	movl	%ebx, 1092(%r14)
	cmpl	$0, 32(%rsi)
	jne	.LBB3_54
	jmp	.LBB3_79
.LBB3_77:                               #   in Loop: Header=BB3_54 Depth=2
	movl	%ecx, 64(%r14)
.LBB3_78:                               # %.backedge105.i
                                        #   in Loop: Header=BB3_54 Depth=2
	cmpl	$0, 32(%rsi)
	jne	.LBB3_54
	jmp	.LBB3_79
.LBB3_53:                               #   in Loop: Header=BB3_54 Depth=2
	movl	%eax, 64(%r14)
	cmpl	$0, 32(%rsi)
	jne	.LBB3_54
	jmp	.LBB3_79
.LBB3_15:                               # %.us-lcssa136._crit_edge
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	64080(%r14), %r15d
	jmp	.LBB3_82
.LBB3_16:                               #   in Loop: Header=BB3_5 Depth=1
	movl	3184(%r14), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movb	12(%r14), %dl
	movl	16(%r14), %r15d
	movl	1092(%r14), %r12d
	movl	64(%r14), %r13d
	movq	3152(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	60(%r14), %ecx
	movq	(%r14), %rax
	movq	24(%rax), %rsi
	movl	32(%rax), %r11d
	movl	64080(%r14), %ebx
	incl	%ebx
	movl	%r11d, 32(%rsp)         # 4-byte Spill
	testl	%r15d, %r15d
	jg	.LBB3_37
	jmp	.LBB3_17
.LBB3_46:                               #   in Loop: Header=BB3_5 Depth=1
	movl	$3600000, %esi          # imm = 0x36EE80
	movl	$1, %edx
	callq	*%rax
	movq	%rax, 3152(%r14)
	xorl	%r12d, %r12d
	testq	%rax, %rax
	je	.LBB3_49
# BB#47:                                #   in Loop: Header=BB3_5 Depth=1
	xorl	%r8d, %r8d
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	xorl	%r9d, %r9d
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	xorl	%ebp, %ebp
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	movl	$0, 32(%rsp)            # 4-byte Folded Spill
	xorl	%r10d, %r10d
	movl	$0, 40(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%r11d, %r11d
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	$0, 60(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	pxor	%xmm9, %xmm9
	movdqa	.LCPI3_0(%rip), %xmm2   # xmm2 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
	movdqa	.LCPI3_1(%rip), %xmm3   # xmm3 = [16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
	jmp	.LBB3_84
.LBB3_48:                               #   in Loop: Header=BB3_5 Depth=1
	xorl	%r12d, %r12d
	movl	$0, 60(%rsp)            # 4-byte Folded Spill
	xorl	%ecx, %ecx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
	xorl	%ecx, %ecx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	xorl	%ecx, %ecx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%r11d, %r11d
	xorl	%ecx, %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	$0, 40(%rsp)            # 4-byte Folded Spill
	xorl	%r10d, %r10d
	movl	$0, 32(%rsp)            # 4-byte Folded Spill
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	xorl	%ebp, %ebp
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	xorl	%r9d, %r9d
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	jmp	.LBB3_50
.LBB3_49:                               #   in Loop: Header=BB3_5 Depth=1
	movl	$-3, %eax
	movl	$0, 60(%rsp)            # 4-byte Folded Spill
	xorl	%ecx, %ecx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
	xorl	%ecx, %ecx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	xorl	%ecx, %ecx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%r11d, %r11d
	xorl	%ecx, %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	$0, 40(%rsp)            # 4-byte Folded Spill
	xorl	%r10d, %r10d
	movl	$0, 32(%rsp)            # 4-byte Folded Spill
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	xorl	%ebp, %ebp
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	xorl	%r9d, %r9d
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	xorl	%r8d, %r8d
.LBB3_50:                               # %BZ2_decompress.exit
                                        #   in Loop: Header=BB3_5 Depth=1
	pxor	%xmm9, %xmm9
	movdqa	.LCPI3_0(%rip), %xmm2   # xmm2 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
	movdqa	.LCPI3_1(%rip), %xmm3   # xmm3 = [16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
	jmp	.LBB3_490
.LBB3_35:                               #   in Loop: Header=BB3_5 Depth=1
	movzbl	%dl, %r15d
	addl	$4, %r15d
	movl	%ecx, %eax
	movl	(%r9,%rax,4), %ecx
	movzbl	%cl, %r13d
	shrl	$8, %ecx
	addl	$5, %edi
	movl	%edi, %r12d
	movl	%ebp, %edx
	testl	%r15d, %r15d
	jg	.LBB3_37
.LBB3_17:                               #   in Loop: Header=BB3_5 Depth=1
	movl	%r12d, %edi
	movl	%r13d, %ebp
	movq	16(%rsp), %r9           # 8-byte Reload
	jmp	.LBB3_20
.LBB3_18:                               # %.loopexit154.i.loopexit
                                        #   in Loop: Header=BB3_5 Depth=1
	subl	%eax, %r11d
	addq	%rax, %rsi
	testl	%r11d, %r11d
	je	.LBB3_51
	.p2align	4, 0x90
.LBB3_19:                               #   in Loop: Header=BB3_5 Depth=1
	movb	%dl, (%rsi)
	incq	%rsi
	decl	%r11d
	movl	%r12d, %edi
	movl	%r13d, %ebp
.LBB3_20:                               #   in Loop: Header=BB3_5 Depth=1
	movl	$-4, %eax
	cmpl	%ebx, %edi
	jg	.LBB3_3
# BB#21:                                #   in Loop: Header=BB3_5 Depth=1
	cmpl	%ebx, %edi
	je	.LBB3_52
# BB#22:                                #   in Loop: Header=BB3_5 Depth=1
	imull	$100000, 40(%r14), %r8d # imm = 0x186A0
	cmpl	%r8d, %ecx
	jae	.LBB3_3
# BB#23:                                #   in Loop: Header=BB3_5 Depth=1
	movl	%ecx, %ecx
	movl	(%r9,%rcx,4), %ecx
	movzbl	%cl, %r13d
	shrl	$8, %ecx
	leal	1(%rdi), %r12d
	cmpl	%ebp, %r13d
	jne	.LBB3_26
# BB#24:                                #   in Loop: Header=BB3_5 Depth=1
	cmpl	%ebx, %r12d
	jne	.LBB3_27
# BB#25:                                #   in Loop: Header=BB3_5 Depth=1
	movl	%ebx, %r12d
	movl	%ebp, %r13d
.LBB3_26:                               #   in Loop: Header=BB3_5 Depth=1
	movl	%ebp, %edx
	testl	%r11d, %r11d
	jne	.LBB3_19
	jmp	.LBB3_51
.LBB3_27:                               #   in Loop: Header=BB3_5 Depth=1
	cmpl	%r8d, %ecx
	jae	.LBB3_3
# BB#28:                                #   in Loop: Header=BB3_5 Depth=1
	movl	%ecx, %ecx
	movl	(%r9,%rcx,4), %r9d
	movl	%r9d, %ecx
	shrl	$8, %ecx
	leal	2(%rdi), %r10d
	movl	$2, %r15d
	cmpl	%ebx, %r10d
	movl	%ebx, %r12d
	movl	%ebp, %r13d
	movl	%ebp, %edx
	je	.LBB3_36
# BB#29:                                #   in Loop: Header=BB3_5 Depth=1
	movzbl	%r9b, %r13d
	cmpl	%ebp, %r13d
	movl	%r10d, %r12d
	movl	%ebp, %edx
	jne	.LBB3_36
# BB#30:                                #   in Loop: Header=BB3_5 Depth=1
	cmpl	%r8d, %ecx
	jae	.LBB3_3
# BB#31:                                #   in Loop: Header=BB3_5 Depth=1
	movl	%ecx, %ecx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx,%rcx,4), %r9d
	movl	%r9d, %ecx
	shrl	$8, %ecx
	leal	3(%rdi), %r10d
	movl	$3, %r15d
	cmpl	%ebx, %r10d
	movl	%ebx, %r12d
	movl	%ebp, %r13d
	movl	%ebp, %edx
	je	.LBB3_36
# BB#32:                                #   in Loop: Header=BB3_5 Depth=1
	movzbl	%r9b, %r13d
	cmpl	%ebp, %r13d
	movl	%r10d, %r12d
	movl	%ebp, %edx
	jne	.LBB3_36
# BB#33:                                #   in Loop: Header=BB3_5 Depth=1
	cmpl	%r8d, %ecx
	jae	.LBB3_3
# BB#34:                                #   in Loop: Header=BB3_5 Depth=1
	movl	%ecx, %ecx
	movq	16(%rsp), %r9           # 8-byte Reload
	movl	(%r9,%rcx,4), %edx
	movl	%edx, %ecx
	shrl	$8, %ecx
	cmpl	%r8d, %ecx
	jb	.LBB3_35
	jmp	.LBB3_3
	.p2align	4, 0x90
.LBB3_36:                               # %.backedge.i36
                                        #   in Loop: Header=BB3_5 Depth=1
	testl	%r15d, %r15d
	jle	.LBB3_17
.LBB3_37:                               # %.preheader.i37
                                        #   in Loop: Header=BB3_5 Depth=1
	testl	%r11d, %r11d
	movq	16(%rsp), %r9           # 8-byte Reload
	je	.LBB3_42
# BB#38:                                # %.lr.ph.i38.preheader
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	%r11d, %edi
	movl	%r15d, %ebp
	decq	%rbp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_39:                               # %.lr.ph.i38
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%eax, %ebp
	je	.LBB3_18
# BB#40:                                #   in Loop: Header=BB3_39 Depth=2
	movb	%dl, (%rsi,%rax)
	incq	%rax
	cmpl	%eax, %edi
	jne	.LBB3_39
# BB#41:                                # %.loopexit.i41.loopexit
                                        #   in Loop: Header=BB3_5 Depth=1
	addq	%rax, %rsi
	subl	%eax, %r15d
.LBB3_42:                               # %.loopexit.i41
                                        #   in Loop: Header=BB3_5 Depth=1
	xorl	%r11d, %r11d
.LBB3_43:                               # %.loopexit.i41
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rax
	movl	32(%rsp), %edi          # 4-byte Reload
	subl	%r11d, %edi
	addl	36(%rax), %edi
	movl	%edi, 36(%rax)
	jae	.LBB3_45
# BB#44:                                #   in Loop: Header=BB3_5 Depth=1
	incl	40(%rax)
.LBB3_45:                               #   in Loop: Header=BB3_5 Depth=1
	movl	40(%rsp), %edi          # 4-byte Reload
	movl	%edi, 3184(%r14)
	movb	%dl, 12(%r14)
	movl	%r15d, 16(%r14)
	movl	%r12d, 1092(%r14)
	movl	%r13d, 64(%r14)
	movq	%r9, 3152(%r14)
	movl	%ecx, 60(%r14)
	movq	%rsi, 24(%rax)
	movl	%r11d, 32(%rax)
	movq	256(%rsp), %r13         # 8-byte Reload
.LBB3_79:                               # %unRLE_obuf_to_output_SMALL.exit
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	64080(%r14), %r15d
	leal	1(%r15), %ecx
	xorl	%eax, %eax
	cmpl	%ecx, 1092(%r14)
	jne	.LBB3_3
# BB#80:                                #   in Loop: Header=BB3_5 Depth=1
	cmpl	$0, 16(%r14)
	jne	.LBB3_3
# BB#81:                                # %.thread43
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	$14, 8(%r14)
	movl	$14, %ecx
.LBB3_82:                               #   in Loop: Header=BB3_5 Depth=1
	movl	64036(%r14), %r8d
	movl	64040(%r14), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	64044(%r14), %r9d
	movl	64048(%r14), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	64052(%r14), %ebp
	movl	64056(%r14), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	64060(%r14), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	64064(%r14), %r10d
	movl	64068(%r14), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	64072(%r14), %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	64076(%r14), %r11d
	movl	64084(%r14), %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	64088(%r14), %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	64092(%r14), %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	64096(%r14), %r12d
	addl	$-14, %ecx
	cmpl	$27, %ecx
	movl	64100(%r14), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	64104(%r14), %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	64108(%r14), %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movl	64112(%r14), %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movl	64116(%r14), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movq	64120(%r14), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	64128(%r14), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	64136(%r14), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	$-4, %eax
	ja	.LBB3_490
# BB#83:                                #   in Loop: Header=BB3_5 Depth=1
	jmpq	*.LJTI3_0(,%rcx,8)
.LBB3_84:                               #   in Loop: Header=BB3_5 Depth=1
	movl	$14, 8(%r14)
	movl	36(%r14), %ecx
	cmpl	$8, %ecx
	jl	.LBB3_86
# BB#85:                                # %.._crit_edge1702_crit_edge.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	32(%r14), %edx
	jmp	.LBB3_91
.LBB3_86:                               # %.lr.ph1701.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rax
	movl	8(%rax), %esi
	decl	%esi
	.p2align	4, 0x90
.LBB3_87:                               #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %esi
	je	.LBB3_94
# BB#88:                                #   in Loop: Header=BB3_87 Depth=2
	movl	32(%r14), %edi
	shll	$8, %edi
	movq	(%rax), %rbx
	movzbl	(%rbx), %edx
	orl	%edi, %edx
	movl	%edx, 32(%r14)
	addl	$8, %ecx
	movl	%ecx, 36(%r14)
	incq	%rbx
	movq	%rbx, (%rax)
	movl	%esi, 8(%rax)
	incl	12(%rax)
	jne	.LBB3_90
# BB#89:                                #   in Loop: Header=BB3_87 Depth=2
	incl	16(%rax)
.LBB3_90:                               # %.backedge1462.i
                                        #   in Loop: Header=BB3_87 Depth=2
	decl	%esi
	cmpl	$7, %ecx
	jle	.LBB3_87
.LBB3_91:                               # %._crit_edge1702.i
                                        #   in Loop: Header=BB3_5 Depth=1
	addl	$-8, %ecx
	shrl	%cl, %edx
	movl	%ecx, 36(%r14)
	cmpb	$23, %dl
	je	.LBB3_95
# BB#92:                                # %._crit_edge1702.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	$-4, %eax
	cmpb	$49, %dl
	jne	.LBB3_490
# BB#93:                                #   in Loop: Header=BB3_5 Depth=1
	movl	$0, 56(%r14)
	jmp	.LBB3_97
.LBB3_94:                               #   in Loop: Header=BB3_5 Depth=1
	xorl	%eax, %eax
	jmp	.LBB3_490
.LBB3_95:                               #   in Loop: Header=BB3_5 Depth=1
	movl	$1, 8(%r14)
	movl	$4, %eax
	jmp	.LBB3_490
.LBB3_96:                               # %._crit_edge1876.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	36(%r14), %ecx
.LBB3_97:                               #   in Loop: Header=BB3_5 Depth=1
	movl	$25, 8(%r14)
	cmpl	$8, %ecx
	jl	.LBB3_99
# BB#98:                                # %.._crit_edge1697_crit_edge.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	32(%r14), %eax
	jmp	.LBB3_104
.LBB3_99:                               # %.lr.ph1696.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rdx
	movl	8(%rdx), %esi
	decl	%esi
	.p2align	4, 0x90
.LBB3_100:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %esi
	je	.LBB3_162
# BB#101:                               #   in Loop: Header=BB3_100 Depth=2
	movl	32(%r14), %edi
	shll	$8, %edi
	movq	(%rdx), %rbx
	movzbl	(%rbx), %eax
	orl	%edi, %eax
	movl	%eax, 32(%r14)
	addl	$8, %ecx
	movl	%ecx, 36(%r14)
	incq	%rbx
	movq	%rbx, (%rdx)
	movl	%esi, 8(%rdx)
	incl	12(%rdx)
	jne	.LBB3_103
# BB#102:                               #   in Loop: Header=BB3_100 Depth=2
	incl	16(%rdx)
.LBB3_103:                              # %.backedge1460.i
                                        #   in Loop: Header=BB3_100 Depth=2
	decl	%esi
	cmpl	$7, %ecx
	jle	.LBB3_100
.LBB3_104:                              # %._crit_edge1697.i
                                        #   in Loop: Header=BB3_5 Depth=1
	addl	$-8, %ecx
	shrl	%cl, %eax
	movl	%ecx, 36(%r14)
	movl	56(%r14), %edx
	shll	$8, %edx
	movzbl	%al, %eax
	orl	%edx, %eax
	movl	%eax, 56(%r14)
	jmp	.LBB3_106
.LBB3_105:                              # %._crit_edge1882.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	36(%r14), %ecx
.LBB3_106:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$26, 8(%r14)
	cmpl	$8, %ecx
	jl	.LBB3_108
# BB#107:                               # %.._crit_edge1692_crit_edge.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	32(%r14), %eax
	jmp	.LBB3_113
.LBB3_108:                              # %.lr.ph1691.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rdx
	movl	8(%rdx), %esi
	decl	%esi
	.p2align	4, 0x90
.LBB3_109:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %esi
	je	.LBB3_151
# BB#110:                               #   in Loop: Header=BB3_109 Depth=2
	movl	32(%r14), %edi
	shll	$8, %edi
	movq	(%rdx), %rbx
	movzbl	(%rbx), %eax
	orl	%edi, %eax
	movl	%eax, 32(%r14)
	addl	$8, %ecx
	movl	%ecx, 36(%r14)
	incq	%rbx
	movq	%rbx, (%rdx)
	movl	%esi, 8(%rdx)
	incl	12(%rdx)
	jne	.LBB3_112
# BB#111:                               #   in Loop: Header=BB3_109 Depth=2
	incl	16(%rdx)
.LBB3_112:                              # %.backedge1458.i
                                        #   in Loop: Header=BB3_109 Depth=2
	decl	%esi
	cmpl	$7, %ecx
	jle	.LBB3_109
.LBB3_113:                              # %._crit_edge1692.i
                                        #   in Loop: Header=BB3_5 Depth=1
	addl	$-8, %ecx
	shrl	%cl, %eax
	movl	%ecx, 36(%r14)
	movl	56(%r14), %edx
	shll	$8, %edx
	movzbl	%al, %eax
	orl	%edx, %eax
	movl	%eax, 56(%r14)
	jmp	.LBB3_115
.LBB3_114:                              # %._crit_edge1888.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	36(%r14), %ecx
.LBB3_115:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$27, 8(%r14)
	cmpl	$8, %ecx
	jl	.LBB3_117
# BB#116:                               # %.._crit_edge1687_crit_edge.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	32(%r14), %eax
	jmp	.LBB3_122
.LBB3_117:                              # %.lr.ph1686.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rdx
	movl	8(%rdx), %esi
	decl	%esi
	.p2align	4, 0x90
.LBB3_118:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %esi
	je	.LBB3_131
# BB#119:                               #   in Loop: Header=BB3_118 Depth=2
	movl	32(%r14), %edi
	shll	$8, %edi
	movq	(%rdx), %rbx
	movzbl	(%rbx), %eax
	orl	%edi, %eax
	movl	%eax, 32(%r14)
	addl	$8, %ecx
	movl	%ecx, 36(%r14)
	incq	%rbx
	movq	%rbx, (%rdx)
	movl	%esi, 8(%rdx)
	incl	12(%rdx)
	jne	.LBB3_121
# BB#120:                               #   in Loop: Header=BB3_118 Depth=2
	incl	16(%rdx)
.LBB3_121:                              # %.backedge1456.i
                                        #   in Loop: Header=BB3_118 Depth=2
	decl	%esi
	cmpl	$7, %ecx
	jle	.LBB3_118
.LBB3_122:                              # %._crit_edge1687.i
                                        #   in Loop: Header=BB3_5 Depth=1
	addl	$-8, %ecx
	shrl	%cl, %eax
	movl	%ecx, 36(%r14)
	movl	56(%r14), %ecx
	shll	$8, %ecx
	movzbl	%al, %edx
	orl	%ecx, %edx
	movl	%edx, 56(%r14)
	movl	$-4, %eax
	testl	%edx, %edx
	js	.LBB3_490
# BB#123:                               #   in Loop: Header=BB3_5 Depth=1
	imull	$100000, 40(%r14), %esi # imm = 0x186A0
	orl	$10, %esi
	xorl	%ecx, %ecx
	cmpl	%esi, %edx
	jg	.LBB3_490
# BB#198:                               #   in Loop: Header=BB3_5 Depth=1
	cmpl	$16, %ecx
	jge	.LBB3_208
	jmp	.LBB3_199
.LBB3_124:                              # %._crit_edge1897.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	36(%r14), %ecx
.LBB3_125:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$31, 8(%r14)
	cmpl	$15, %ecx
	jl	.LBB3_132
# BB#126:                               # %.._crit_edge1676_crit_edge.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	32(%r14), %esi
	jmp	.LBB3_137
.LBB3_127:                              # %._crit_edge1943.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	36(%r14), %eax
	jmp	.LBB3_328
.LBB3_128:                              # %._crit_edge1931.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	36(%r14), %edx
	jmp	.LBB3_344
.LBB3_129:                              # %._crit_edge1921.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	36(%r14), %edx
	jmp	.LBB3_180
.LBB3_130:                              # %._crit_edge1912.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	36(%r14), %edx
	jmp	.LBB3_189
.LBB3_131:                              #   in Loop: Header=BB3_5 Depth=1
	xorl	%eax, %eax
	jmp	.LBB3_490
.LBB3_132:                              # %.lr.ph1675.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rax
	movl	8(%rax), %edx
	decl	%edx
	.p2align	4, 0x90
.LBB3_133:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %edx
	je	.LBB3_164
# BB#134:                               #   in Loop: Header=BB3_133 Depth=2
	movl	32(%r14), %esi
	shll	$8, %esi
	movq	(%rax), %rdi
	movzbl	(%rdi), %ebx
	orl	%esi, %ebx
	movl	%ebx, %esi
	movl	%ebx, 32(%r14)
	addl	$8, %ecx
	movl	%ecx, 36(%r14)
	incq	%rdi
	movq	%rdi, (%rax)
	movl	%edx, 8(%rax)
	incl	12(%rax)
	jne	.LBB3_136
# BB#135:                               #   in Loop: Header=BB3_133 Depth=2
	incl	16(%rax)
.LBB3_136:                              # %.backedge1450.i
                                        #   in Loop: Header=BB3_133 Depth=2
	decl	%edx
	cmpl	$14, %ecx
	jle	.LBB3_133
.LBB3_137:                              # %._crit_edge1676.i
                                        #   in Loop: Header=BB3_5 Depth=1
	addl	$-15, %ecx
	movl	%esi, %eax
	shrl	%cl, %eax
	andl	$32767, %eax            # imm = 0x7FFF
	movl	%ecx, 36(%r14)
	je	.LBB3_163
# BB#138:                               #   in Loop: Header=BB3_5 Depth=1
	xorl	%r8d, %r8d
.LBB3_139:                              #   in Loop: Header=BB3_5 Depth=1
	cmpl	%eax, %r8d
	movl	%eax, 132(%rsp)         # 4-byte Spill
	jge	.LBB3_152
# BB#140:                               #   in Loop: Header=BB3_5 Depth=1
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB3_141:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_144 Depth 3
	movl	$32, 8(%r14)
	movl	36(%r14), %eax
	testl	%eax, %eax
	jle	.LBB3_143
# BB#142:                               # %.._crit_edge1556_crit_edge.i
                                        #   in Loop: Header=BB3_141 Depth=2
	movl	32(%r14), %esi
	jmp	.LBB3_148
.LBB3_143:                              # %.lr.ph1555.i
                                        #   in Loop: Header=BB3_141 Depth=2
	movq	(%r14), %rcx
	movl	8(%rcx), %edx
	decl	%edx
	.p2align	4, 0x90
.LBB3_144:                              #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_141 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	$-1, %edx
	je	.LBB3_151
# BB#145:                               #   in Loop: Header=BB3_144 Depth=3
	movl	32(%r14), %edi
	shll	$8, %edi
	movq	(%rcx), %rbx
	movzbl	(%rbx), %esi
	orl	%edi, %esi
	movl	%esi, 32(%r14)
	addl	$8, %eax
	movl	%eax, 36(%r14)
	incq	%rbx
	movq	%rbx, (%rcx)
	movl	%edx, 8(%rcx)
	incl	12(%rcx)
	jne	.LBB3_147
# BB#146:                               #   in Loop: Header=BB3_144 Depth=3
	incl	16(%rcx)
.LBB3_147:                              # %.backedge1410.i
                                        #   in Loop: Header=BB3_144 Depth=3
	decl	%edx
	leal	-8(%rax), %edi
	cmpl	$-8, %edi
	jle	.LBB3_144
.LBB3_148:                              # %._crit_edge1556.i
                                        #   in Loop: Header=BB3_141 Depth=2
	decl	%eax
	movl	%eax, 36(%r14)
	btl	%eax, %esi
	jae	.LBB3_179
# BB#149:                               #   in Loop: Header=BB3_141 Depth=2
	movl	8(%rsp), %ecx           # 4-byte Reload
	incl	%ecx
	movl	$-4, %eax
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	cmpl	%ebp, %ecx
	jl	.LBB3_141
	jmp	.LBB3_490
.LBB3_151:                              #   in Loop: Header=BB3_5 Depth=1
	xorl	%eax, %eax
	jmp	.LBB3_490
.LBB3_152:                              #   in Loop: Header=BB3_5 Depth=1
	testl	%ebp, %ebp
	jle	.LBB3_167
# BB#153:                               # %.lr.ph1673.i.preheader
                                        #   in Loop: Header=BB3_5 Depth=1
	cmpl	$31, %ebp
	jbe	.LBB3_165
# BB#154:                               # %min.iters.checked460
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	%ebp, %eax
	andl	$-32, %eax
	je	.LBB3_165
# BB#155:                               # %vector.scevcheck
                                        #   in Loop: Header=BB3_5 Depth=1
	leal	-1(%rbp), %edx
	xorl	%ecx, %ecx
	cmpb	$-1, %dl
	je	.LBB3_166
# BB#156:                               # %vector.scevcheck
                                        #   in Loop: Header=BB3_5 Depth=1
	cmpl	$255, %edx
	ja	.LBB3_166
# BB#157:                               # %vector.body456.preheader
                                        #   in Loop: Header=BB3_5 Depth=1
	leal	-32(%rax), %esi
	movl	%esi, %edx
	shrl	$5, %edx
	xorl	%ecx, %ecx
	btl	$5, %esi
	jb	.LBB3_159
# BB#158:                               # %vector.body456.prol
                                        #   in Loop: Header=BB3_5 Depth=1
	movdqa	%xmm2, 64(%rsp)
	movdqa	%xmm3, 80(%rsp)
	movl	$32, %ecx
.LBB3_159:                              # %vector.body456.prol.loopexit
                                        #   in Loop: Header=BB3_5 Depth=1
	testl	%edx, %edx
	je	.LBB3_161
.LBB3_160:                              # %vector.body456
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	%ecx, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqa	%xmm0, %xmm1
	paddb	%xmm2, %xmm1
	paddb	%xmm3, %xmm0
	movl	%ecx, %edx
	andl	$224, %edx
	movdqa	%xmm1, 64(%rsp,%rdx)
	movdqa	%xmm0, 80(%rsp,%rdx)
	leal	32(%rcx), %edx
	movd	%edx, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqa	%xmm0, %xmm1
	paddb	%xmm2, %xmm1
	paddb	%xmm3, %xmm0
	andl	$224, %edx
	movdqa	%xmm1, 64(%rsp,%rdx)
	movdqa	%xmm0, 80(%rsp,%rdx)
	addl	$64, %ecx
	cmpl	%eax, %ecx
	jne	.LBB3_160
.LBB3_161:                              # %middle.block457
                                        #   in Loop: Header=BB3_5 Depth=1
	cmpl	%eax, %ebp
	movb	%al, %cl
	jne	.LBB3_166
	jmp	.LBB3_167
.LBB3_51:                               #   in Loop: Header=BB3_5 Depth=1
	xorl	%r11d, %r11d
	movl	$1, %r15d
	jmp	.LBB3_43
.LBB3_162:                              #   in Loop: Header=BB3_5 Depth=1
	xorl	%eax, %eax
	jmp	.LBB3_490
.LBB3_52:                               #   in Loop: Header=BB3_5 Depth=1
	xorl	%r15d, %r15d
	movl	%ebx, %r12d
	movl	%ebp, %r13d
	jmp	.LBB3_43
.LBB3_163:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	movl	$-4, %eax
	jmp	.LBB3_490
.LBB3_164:                              #   in Loop: Header=BB3_5 Depth=1
	xorl	%eax, %eax
	jmp	.LBB3_490
.LBB3_165:                              #   in Loop: Header=BB3_5 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_166:                              # %.lr.ph1673.i
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%cl, %ecx
	movb	%cl, 64(%rsp,%rcx)
	incb	%cl
	movzbl	%cl, %eax
	cmpl	%ebp, %eax
	jl	.LBB3_166
.LBB3_167:                              # %.preheader1448.i
                                        #   in Loop: Header=BB3_5 Depth=1
	xorl	%r9d, %r9d
	movl	132(%rsp), %eax         # 4-byte Reload
	testl	%eax, %eax
	movl	$0, %r8d
	jle	.LBB3_178
# BB#168:                               # %.lr.ph1668.preheader.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	%r13, %r9
	movl	%r11d, %r13d
	movq	%rbp, %r11
	movl	%eax, %ebp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_169:                              # %.lr.ph1668.i
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_172 Depth 3
                                        #       Child Loop BB3_175 Depth 3
	movzbl	25886(%r14,%rcx), %esi
	testq	%rsi, %rsi
	movb	64(%rsp,%rsi), %r8b
	je	.LBB3_176
# BB#170:                               # %.lr.ph1665.i.preheader
                                        #   in Loop: Header=BB3_169 Depth=2
	movl	%esi, %eax
	decb	%al
	movl	%esi, %edx
	andb	$7, %dl
	je	.LBB3_173
# BB#171:                               # %.lr.ph1665.i.prol.preheader
                                        #   in Loop: Header=BB3_169 Depth=2
	negb	%dl
	.p2align	4, 0x90
.LBB3_172:                              # %.lr.ph1665.i.prol
                                        #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_169 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	63(%rsp,%rsi), %ebx
	movb	%bl, 64(%rsp,%rsi)
	decq	%rsi
	incb	%dl
	jne	.LBB3_172
.LBB3_173:                              # %.lr.ph1665.i.prol.loopexit
                                        #   in Loop: Header=BB3_169 Depth=2
	cmpb	$7, %al
	jb	.LBB3_176
# BB#174:                               # %.lr.ph1665.i.preheader.new
                                        #   in Loop: Header=BB3_169 Depth=2
	leaq	64(%rsp,%rsi), %rdi
	addl	$-7, %esi
	movb	$-7, %al
	subb	%sil, %al
	.p2align	4, 0x90
.LBB3_175:                              # %.lr.ph1665.i
                                        #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_169 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	-1(%rdi), %edx
	movb	%dl, (%rdi)
	movzbl	-2(%rdi), %edx
	movb	%dl, -1(%rdi)
	movzbl	-3(%rdi), %edx
	movb	%dl, -2(%rdi)
	movzbl	-4(%rdi), %edx
	movb	%dl, -3(%rdi)
	movzbl	-5(%rdi), %edx
	movb	%dl, -4(%rdi)
	movzbl	-6(%rdi), %edx
	movb	%dl, -5(%rdi)
	movzbl	-7(%rdi), %edx
	movb	%dl, -6(%rdi)
	movzbl	-8(%rdi), %edx
	movb	%dl, -7(%rdi)
	leaq	-8(%rdi), %rdi
	addb	$8, %al
	jne	.LBB3_175
.LBB3_176:                              # %._crit_edge1666.i
                                        #   in Loop: Header=BB3_169 Depth=2
	movb	%r8b, 64(%rsp)
	movb	%r8b, 7884(%r14,%rcx)
	incq	%rcx
	cmpq	%rbp, %rcx
	jne	.LBB3_169
# BB#177:                               #   in Loop: Header=BB3_5 Depth=1
	movl	132(%rsp), %eax         # 4-byte Reload
	movl	%eax, %r8d
	movq	%r11, %rbp
	movl	%r13d, %r11d
	movq	%r9, %r13
	xorl	%r9d, %r9d
.LBB3_178:                              # %._crit_edge1669.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
	movq	120(%rsp), %rcx         # 8-byte Reload
	jmp	.LBB3_243
.LBB3_179:                              #   in Loop: Header=BB3_5 Depth=1
	movslq	%r8d, %r8
	movl	8(%rsp), %eax           # 4-byte Reload
	movb	%al, 25886(%r14,%r8)
	incl	%r8d
	movl	16(%rsp), %eax          # 4-byte Reload
	jmp	.LBB3_139
.LBB3_199:                              #   in Loop: Header=BB3_5 Depth=1
	movl	%ecx, %r8d
.LBB3_200:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$28, 8(%r14)
	movl	36(%r14), %eax
	testl	%eax, %eax
	jle	.LBB3_202
# BB#201:                               # %.._crit_edge_crit_edge.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	32(%r14), %esi
	jmp	.LBB3_207
.LBB3_202:                              # %.lr.ph.i27
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rcx
	movl	8(%rcx), %edx
	decl	%edx
	.p2align	4, 0x90
.LBB3_203:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %edx
	je	.LBB3_209
# BB#204:                               #   in Loop: Header=BB3_203 Depth=2
	movl	32(%r14), %edi
	shll	$8, %edi
	movq	(%rcx), %rbx
	movzbl	(%rbx), %esi
	orl	%edi, %esi
	movl	%esi, 32(%r14)
	addl	$8, %eax
	movl	%eax, 36(%r14)
	incq	%rbx
	movq	%rbx, (%rcx)
	movl	%edx, 8(%rcx)
	incl	12(%rcx)
	jne	.LBB3_206
# BB#205:                               #   in Loop: Header=BB3_203 Depth=2
	incl	16(%rcx)
.LBB3_206:                              # %.backedge.i28
                                        #   in Loop: Header=BB3_203 Depth=2
	decl	%edx
	leal	-8(%rax), %edi
	cmpl	$-8, %edi
	jle	.LBB3_203
.LBB3_207:                              # %._crit_edge.i
                                        #   in Loop: Header=BB3_5 Depth=1
	decl	%eax
	movl	%eax, 36(%r14)
	btl	%eax, %esi
	setb	%al
	movslq	%r8d, %rcx
	movb	%al, 3452(%r14,%rcx)
	incl	%ecx
	cmpl	$16, %ecx
	jl	.LBB3_199
.LBB3_208:                              # %.preheader1453.preheader.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	480(%rsp), %rax         # 8-byte Reload
	movdqu	%xmm9, 240(%rax)
	movdqu	%xmm9, 224(%rax)
	movdqu	%xmm9, 208(%rax)
	movdqu	%xmm9, 192(%rax)
	movdqu	%xmm9, 176(%rax)
	movdqu	%xmm9, 160(%rax)
	movdqu	%xmm9, 144(%rax)
	movdqu	%xmm9, 128(%rax)
	movdqu	%xmm9, 112(%rax)
	movdqu	%xmm9, 96(%rax)
	movdqu	%xmm9, 80(%rax)
	movdqu	%xmm9, 64(%rax)
	movdqu	%xmm9, 48(%rax)
	movdqu	%xmm9, 32(%rax)
	movdqu	%xmm9, 16(%rax)
	movdqu	%xmm9, (%rax)
	xorl	%r8d, %r8d
	cmpl	$15, %r8d
	jg	.LBB3_215
	jmp	.LBB3_210
.LBB3_209:                              #   in Loop: Header=BB3_5 Depth=1
	xorl	%eax, %eax
	jmp	.LBB3_490
.LBB3_344:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$37, 8(%r14)
	testl	%edx, %edx
	jle	.LBB3_346
# BB#345:                               # %.._crit_edge1576_crit_edge.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	32(%r14), %eax
	jmp	.LBB3_351
.LBB3_346:                              # %.lr.ph1575.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rcx
	movl	8(%rcx), %esi
	decl	%esi
	.p2align	4, 0x90
.LBB3_347:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %esi
	je	.LBB3_352
# BB#348:                               #   in Loop: Header=BB3_347 Depth=2
	movl	32(%r14), %edi
	shll	$8, %edi
	movq	(%rcx), %rbx
	movzbl	(%rbx), %eax
	orl	%edi, %eax
	movl	%eax, 32(%r14)
	addl	$8, %edx
	movl	%edx, 36(%r14)
	incq	%rbx
	movq	%rbx, (%rcx)
	movl	%esi, 8(%rcx)
	incl	12(%rcx)
	jne	.LBB3_350
# BB#349:                               #   in Loop: Header=BB3_347 Depth=2
	incl	16(%rcx)
.LBB3_350:                              # %.backedge1418.i
                                        #   in Loop: Header=BB3_347 Depth=2
	decl	%esi
	leal	-8(%rdx), %edi
	cmpl	$-8, %edi
	jle	.LBB3_347
.LBB3_351:                              # %._crit_edge1576.i
                                        #   in Loop: Header=BB3_5 Depth=1
	decl	%edx
	movl	%edx, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	movl	%edx, 36(%r14)
	movq	88(%rsp), %rcx          # 8-byte Reload
	leal	(%rax,%rcx,2), %ecx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movl	4(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB3_341
.LBB3_352:                              #   in Loop: Header=BB3_5 Depth=1
	xorl	%eax, %eax
	jmp	.LBB3_490
.LBB3_243:                              #   in Loop: Header=BB3_5 Depth=1
	cmpl	%ebp, %r9d
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	jge	.LBB3_247
# BB#244:                               #   in Loop: Header=BB3_5 Depth=1
	movl	132(%rsp), %eax         # 4-byte Reload
	movl	%eax, 16(%rsp)          # 4-byte Spill
.LBB3_245:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$33, 8(%r14)
	movl	36(%r14), %ecx
	cmpl	$5, %ecx
	jl	.LBB3_294
# BB#246:                               # %.._crit_edge1571_crit_edge.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	32(%r14), %eax
	jmp	.LBB3_299
.LBB3_247:                              # %.preheader1447.i
                                        #   in Loop: Header=BB3_5 Depth=1
	testl	%ebp, %ebp
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	jle	.LBB3_301
# BB#248:                               # %.preheader1446.lr.ph.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	%eax, %ecx
	movl	%ebp, %edx
	movq	%rdx, 488(%rsp)         # 8-byte Spill
	movq	%rcx, %rbp
	leaq	-1(%rbp), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%ebp, %ecx
	andl	$1, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	%eax, %ecx
	andl	$7, %ecx
	movq	%rbp, %rdx
	movq	%rcx, 440(%rsp)         # 8-byte Spill
	subq	%rcx, %rdx
	movq	%rdx, 288(%rsp)         # 8-byte Spill
	movq	448(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 296(%rsp)         # 8-byte Spill
	movq	%r14, %r13
	movq	456(%rsp), %rcx         # 8-byte Reload
	movq	464(%rsp), %rsi         # 8-byte Reload
	xorl	%ebx, %ebx
	movdqa	.LCPI3_3(%rip), %xmm2   # xmm2 = [2,2,2,2]
	movq	%r12, 280(%rsp)         # 8-byte Spill
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_249:                              # %.preheader1446.i
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_253 Depth 3
                                        #       Child Loop BB3_257 Depth 3
                                        #       Child Loop BB3_259 Depth 3
                                        #         Child Loop BB3_265 Depth 4
                                        #       Child Loop BB3_276 Depth 3
                                        #       Child Loop BB3_280 Depth 3
                                        #       Child Loop BB3_288 Depth 3
                                        #       Child Loop BB3_291 Depth 3
	testl	%eax, %eax
	movq	%rbx, 168(%rsp)         # 8-byte Spill
	movq	%rsi, 184(%rsp)         # 8-byte Spill
	jle	.LBB3_273
# BB#250:                               # %.lr.ph1655.i.preheader
                                        #   in Loop: Header=BB3_249 Depth=2
	movl	$32, %r15d
	cmpl	$8, %eax
	jb	.LBB3_255
# BB#251:                               # %min.iters.checked424
                                        #   in Loop: Header=BB3_249 Depth=2
	movq	288(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	je	.LBB3_255
# BB#252:                               # %vector.body420.preheader
                                        #   in Loop: Header=BB3_249 Depth=2
	pxor	%xmm2, %xmm2
	movq	%rax, %rdx
	movdqa	.LCPI3_2(%rip), %xmm0   # xmm0 = [32,32,32,32]
	movdqa	%xmm0, %xmm8
	movdqa	%xmm0, %xmm1
	pxor	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB3_253:                              # %vector.body420
                                        #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_249 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movd	-4(%rsi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movd	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm9, %xmm5    # xmm5 = xmm5[0],xmm9[0],xmm5[1],xmm9[1],xmm5[2],xmm9[2],xmm5[3],xmm9[3],xmm5[4],xmm9[4],xmm5[5],xmm9[5],xmm5[6],xmm9[6],xmm5[7],xmm9[7]
	punpcklwd	%xmm9, %xmm5    # xmm5 = xmm5[0],xmm9[0],xmm5[1],xmm9[1],xmm5[2],xmm9[2],xmm5[3],xmm9[3]
	punpcklbw	%xmm9, %xmm4    # xmm4 = xmm4[0],xmm9[0],xmm4[1],xmm9[1],xmm4[2],xmm9[2],xmm4[3],xmm9[3],xmm4[4],xmm9[4],xmm4[5],xmm9[5],xmm4[6],xmm9[6],xmm4[7],xmm9[7]
	punpcklwd	%xmm9, %xmm4    # xmm4 = xmm4[0],xmm9[0],xmm4[1],xmm9[1],xmm4[2],xmm9[2],xmm4[3],xmm9[3]
	movdqa	%xmm5, %xmm6
	pcmpgtd	%xmm2, %xmm6
	movdqa	%xmm4, %xmm7
	pcmpgtd	%xmm3, %xmm7
	movdqa	%xmm5, %xmm0
	pand	%xmm6, %xmm0
	pandn	%xmm2, %xmm6
	movdqa	%xmm6, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm4, %xmm0
	pand	%xmm7, %xmm0
	pandn	%xmm3, %xmm7
	movdqa	%xmm7, %xmm3
	por	%xmm0, %xmm3
	movdqa	%xmm8, %xmm0
	pcmpgtd	%xmm5, %xmm0
	movdqa	%xmm1, %xmm6
	pcmpgtd	%xmm4, %xmm6
	pand	%xmm0, %xmm5
	pandn	%xmm8, %xmm0
	movdqa	%xmm0, %xmm8
	por	%xmm5, %xmm8
	pand	%xmm6, %xmm4
	pandn	%xmm1, %xmm6
	movdqa	%xmm6, %xmm1
	por	%xmm4, %xmm1
	addq	$8, %rsi
	addq	$-8, %rdx
	jne	.LBB3_253
# BB#254:                               # %middle.block421
                                        #   in Loop: Header=BB3_249 Depth=2
	movdqa	%xmm2, %xmm0
	pcmpgtd	%xmm3, %xmm0
	pand	%xmm0, %xmm2
	pandn	%xmm3, %xmm0
	por	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm2       # xmm2 = xmm0[2,3,0,1]
	movdqa	%xmm0, %xmm3
	pcmpgtd	%xmm2, %xmm3
	pand	%xmm3, %xmm0
	pandn	%xmm2, %xmm3
	por	%xmm0, %xmm3
	pshufd	$229, %xmm3, %xmm0      # xmm0 = xmm3[1,1,2,3]
	movd	%xmm3, %eax
	pcmpgtd	%xmm0, %xmm3
	movdqa	%xmm3, 496(%rsp)
	testb	$1, 496(%rsp)
	movd	%xmm0, %r12d
	cmovnel	%eax, %r12d
	movdqa	%xmm1, %xmm0
	pcmpgtd	%xmm8, %xmm0
	pand	%xmm0, %xmm8
	pandn	%xmm1, %xmm0
	por	%xmm8, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	movdqa	%xmm1, %xmm2
	pcmpgtd	%xmm0, %xmm2
	pand	%xmm2, %xmm0
	pandn	%xmm1, %xmm2
	por	%xmm0, %xmm2
	pshufd	$229, %xmm2, %xmm0      # xmm0 = xmm2[1,1,2,3]
	movd	%xmm0, %eax
	pcmpgtd	%xmm2, %xmm0
	movdqa	%xmm0, 512(%rsp)
	movd	%xmm2, %r15d
	testb	$1, 512(%rsp)
	cmovel	%eax, %r15d
	cmpl	$0, 440(%rsp)           # 4-byte Folded Reload
	movq	288(%rsp), %rax         # 8-byte Reload
	movdqa	.LCPI3_3(%rip), %xmm2   # xmm2 = [2,2,2,2]
	jne	.LBB3_256
	jmp	.LBB3_258
	.p2align	4, 0x90
.LBB3_255:                              #   in Loop: Header=BB3_249 Depth=2
	xorl	%eax, %eax
	xorl	%r12d, %r12d
.LBB3_256:                              # %.lr.ph1655.i.preheader479
                                        #   in Loop: Header=BB3_249 Depth=2
	movq	%rbp, %rdx
	subq	%rax, %rdx
	addq	%rcx, %rax
	.p2align	4, 0x90
.LBB3_257:                              # %.lr.ph1655.i
                                        #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_249 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %esi
	cmpl	%r12d, %esi
	cmovgel	%esi, %r12d
	cmpl	%r15d, %esi
	cmovlel	%esi, %r15d
	incq	%rax
	decq	%rdx
	jne	.LBB3_257
.LBB3_258:                              # %.preheader73.us.i.i.preheader
                                        #   in Loop: Header=BB3_249 Depth=2
	imulq	$1032, %rbx, %r10       # imm = 0x408
	leaq	51628(%r14,%r10), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	imulq	$258, %rbx, %rax        # imm = 0x102
	movq	%rax, 152(%rsp)         # 8-byte Spill
	leaq	43888(%r14,%rax), %rdx
	xorl	%esi, %esi
	movl	%r15d, %ebp
	.p2align	4, 0x90
.LBB3_259:                              # %.preheader73.us.i.i
                                        #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_249 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_265 Depth 4
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	jne	.LBB3_261
# BB#260:                               #   in Loop: Header=BB3_259 Depth=3
	xorl	%r9d, %r9d
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	jne	.LBB3_264
	jmp	.LBB3_270
	.p2align	4, 0x90
.LBB3_261:                              #   in Loop: Header=BB3_259 Depth=3
	movzbl	(%rdx), %eax
	cmpl	%ebp, %eax
	jne	.LBB3_263
# BB#262:                               #   in Loop: Header=BB3_259 Depth=3
	movslq	%esi, %rsi
	leaq	(%r14,%r10), %rax
	movl	$0, 57820(%rax,%rsi,4)
	incl	%esi
.LBB3_263:                              # %.prol.loopexit
                                        #   in Loop: Header=BB3_259 Depth=3
	movl	$1, %r9d
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB3_270
.LBB3_264:                              # %.preheader73.us.i.i.new
                                        #   in Loop: Header=BB3_259 Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	subq	%r9, %rax
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB3_265:                              #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_249 Depth=2
                                        #       Parent Loop BB3_259 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leaq	(%r9,%r11), %rdi
	movzbl	(%rcx,%rdi), %ebx
	cmpl	%ebp, %ebx
	jne	.LBB3_267
# BB#266:                               #   in Loop: Header=BB3_265 Depth=4
	movslq	%esi, %rsi
	leaq	(%r14,%r10), %rbx
	leal	(%r9,%r11), %r8d
	movl	%r8d, 57820(%rbx,%rsi,4)
	incl	%esi
.LBB3_267:                              #   in Loop: Header=BB3_265 Depth=4
	movzbl	1(%rcx,%rdi), %edi
	cmpl	%ebp, %edi
	jne	.LBB3_269
# BB#268:                               #   in Loop: Header=BB3_265 Depth=4
	movslq	%esi, %rsi
	leaq	(%r14,%r10), %rdi
	leal	1(%r9,%r11), %ebx
	movl	%ebx, 57820(%rdi,%rsi,4)
	incl	%esi
.LBB3_269:                              #   in Loop: Header=BB3_265 Depth=4
	addq	$2, %r11
	cmpq	%r11, %rax
	jne	.LBB3_265
.LBB3_270:                              # %._crit_edge88.us.i.i
                                        #   in Loop: Header=BB3_259 Depth=3
	cmpl	%r12d, %ebp
	leal	1(%rbp), %eax
	movl	%eax, %ebp
	jl	.LBB3_259
# BB#271:                               # %.lr.ph83.i.i.preheader
                                        #   in Loop: Header=BB3_249 Depth=2
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	movq	176(%rsp), %rax         # 8-byte Reload
	movdqu	%xmm9, 76(%rax)
	movdqu	%xmm9, 64(%rax)
	movdqu	%xmm9, 48(%rax)
	movdqu	%xmm9, 32(%rax)
	movdqu	%xmm9, 16(%rax)
	movdqu	%xmm9, (%rax)
	jne	.LBB3_274
# BB#272:                               #   in Loop: Header=BB3_249 Depth=2
	xorl	%eax, %eax
	jmp	.LBB3_275
	.p2align	4, 0x90
.LBB3_273:                              # %.preheader72.preheader..preheader70.preheader_crit_edge.i.i
                                        #   in Loop: Header=BB3_249 Depth=2
	imulq	$1032, %rbx, %rax       # imm = 0x408
	movdqu	%xmm9, 51704(%r14,%rax)
	movdqu	%xmm9, 51692(%r14,%rax)
	movdqu	%xmm9, 51676(%r14,%rax)
	movdqu	%xmm9, 51660(%r14,%rax)
	movdqu	%xmm9, 51644(%r14,%rax)
	movdqu	%xmm9, 51628(%r14,%rax)
	leaq	51644(%r14,%rax), %rdx
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	leaq	51660(%r14,%rax), %rdx
	movq	%rdx, 176(%rsp)         # 8-byte Spill
	leaq	51676(%r14,%rax), %rdx
	leaq	51692(%r14,%rax), %r11
	leaq	51704(%r14,%rax), %rsi
	movq	%rsi, 312(%rsp)         # 8-byte Spill
	leaq	51632(%r14,%rax), %rsi
	movq	%rsi, 320(%rsp)         # 8-byte Spill
	leaq	51636(%r14,%rax), %rsi
	movq	%rsi, 304(%rsp)         # 8-byte Spill
	leaq	51640(%r14,%rax), %rsi
	movq	%rsi, 328(%rsp)         # 8-byte Spill
	leaq	51648(%r14,%rax), %rsi
	movq	%rsi, 336(%rsp)         # 8-byte Spill
	leaq	51652(%r14,%rax), %rsi
	movq	%rsi, 344(%rsp)         # 8-byte Spill
	leaq	51656(%r14,%rax), %rsi
	movq	%rsi, 352(%rsp)         # 8-byte Spill
	leaq	51664(%r14,%rax), %rsi
	movq	%rsi, 360(%rsp)         # 8-byte Spill
	leaq	51668(%r14,%rax), %rsi
	movq	%rsi, 368(%rsp)         # 8-byte Spill
	leaq	51672(%r14,%rax), %rsi
	movq	%rsi, 376(%rsp)         # 8-byte Spill
	leaq	51680(%r14,%rax), %rsi
	movq	%rsi, 384(%rsp)         # 8-byte Spill
	leaq	51684(%r14,%rax), %rsi
	movq	%rsi, 392(%rsp)         # 8-byte Spill
	leaq	51688(%r14,%rax), %rsi
	movq	%rsi, 400(%rsp)         # 8-byte Spill
	leaq	51696(%r14,%rax), %rsi
	movq	%rsi, 408(%rsp)         # 8-byte Spill
	leaq	51700(%r14,%rax), %rsi
	movq	%rsi, 416(%rsp)         # 8-byte Spill
	leaq	51708(%r14,%rax), %rsi
	movq	%rsi, 424(%rsp)         # 8-byte Spill
	leaq	51712(%r14,%rax), %rsi
	movq	%rsi, 432(%rsp)         # 8-byte Spill
	leaq	51716(%r14,%rax), %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	xorl	%r12d, %r12d
	movl	$32, %r15d
	movb	$1, %al
	movl	%eax, 272(%rsp)         # 4-byte Spill
	movq	%rbx, %rdi
	movq	%rdx, %rbx
	xorl	%r10d, %r10d
	movl	$0, 252(%rsp)           # 4-byte Folded Spill
	movl	$0, 248(%rsp)           # 4-byte Folded Spill
	movl	$0, 244(%rsp)           # 4-byte Folded Spill
	movl	$0, 240(%rsp)           # 4-byte Folded Spill
	movl	$0, 236(%rsp)           # 4-byte Folded Spill
	movl	$0, 232(%rsp)           # 4-byte Folded Spill
	movl	$0, 228(%rsp)           # 4-byte Folded Spill
	movl	$0, 224(%rsp)           # 4-byte Folded Spill
	movl	$0, 220(%rsp)           # 4-byte Folded Spill
	movl	$0, 216(%rsp)           # 4-byte Folded Spill
	movl	$0, 212(%rsp)           # 4-byte Folded Spill
	movl	$0, 208(%rsp)           # 4-byte Folded Spill
	movl	$0, 204(%rsp)           # 4-byte Folded Spill
	movl	$0, 200(%rsp)           # 4-byte Folded Spill
	movl	$0, 196(%rsp)           # 4-byte Folded Spill
	movl	$0, 192(%rsp)           # 4-byte Folded Spill
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%eax, %eax
	xorl	%r9d, %r9d
	xorl	%ebp, %ebp
	jmp	.LBB3_278
.LBB3_274:                              # %.lr.ph83.i.i.prol
                                        #   in Loop: Header=BB3_249 Depth=2
	movq	152(%rsp), %rax         # 8-byte Reload
	movzbl	43888(%r14,%rax), %eax
	leaq	(%r14,%r10), %rdx
	incl	51632(%rdx,%rax,4)
	movl	$1, %eax
.LBB3_275:                              # %.lr.ph83.i.i.prol.loopexit
                                        #   in Loop: Header=BB3_249 Depth=2
	movq	32(%rsp), %rdi          # 8-byte Reload
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB3_277
	.p2align	4, 0x90
.LBB3_276:                              # %.lr.ph83.i.i
                                        #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_249 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rcx,%rax), %edx
	leaq	(%r14,%r10), %rsi
	incl	51632(%rsi,%rdx,4)
	movzbl	1(%rcx,%rax), %edx
	incl	51632(%rsi,%rdx,4)
	addq	$2, %rax
	cmpq	%rax, %rdi
	jne	.LBB3_276
.LBB3_277:                              # %.preheader70.preheader.loopexit.i.i
                                        #   in Loop: Header=BB3_249 Depth=2
	leaq	51632(%r14,%r10), %rax
	movq	%rax, 320(%rsp)         # 8-byte Spill
	movl	51632(%r14,%r10), %ebp
	movl	51636(%r14,%r10), %r9d
	leaq	51636(%r14,%r10), %rax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	leaq	51640(%r14,%r10), %rax
	movq	%rax, 328(%rsp)         # 8-byte Spill
	movl	51640(%r14,%r10), %eax
	leaq	51644(%r14,%r10), %rdx
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	movl	51644(%r14,%r10), %esi
	leaq	51648(%r14,%r10), %rdx
	movq	%rdx, 336(%rsp)         # 8-byte Spill
	movl	51648(%r14,%r10), %edx
	leaq	51652(%r14,%r10), %rdi
	movq	%rdi, 344(%rsp)         # 8-byte Spill
	movl	51652(%r14,%r10), %edi
	movl	%edi, 192(%rsp)         # 4-byte Spill
	leaq	51656(%r14,%r10), %rdi
	movq	%rdi, 352(%rsp)         # 8-byte Spill
	movl	51656(%r14,%r10), %edi
	movl	%edi, 196(%rsp)         # 4-byte Spill
	leaq	51660(%r14,%r10), %rdi
	movq	%rdi, 176(%rsp)         # 8-byte Spill
	movl	51660(%r14,%r10), %edi
	movl	%edi, 200(%rsp)         # 4-byte Spill
	leaq	51664(%r14,%r10), %rdi
	movq	%rdi, 360(%rsp)         # 8-byte Spill
	movl	51664(%r14,%r10), %edi
	movl	%edi, 204(%rsp)         # 4-byte Spill
	leaq	51668(%r14,%r10), %rdi
	movq	%rdi, 368(%rsp)         # 8-byte Spill
	movl	51668(%r14,%r10), %edi
	movl	%edi, 208(%rsp)         # 4-byte Spill
	leaq	51672(%r14,%r10), %rdi
	movq	%rdi, 376(%rsp)         # 8-byte Spill
	movl	51672(%r14,%r10), %edi
	movl	%edi, 212(%rsp)         # 4-byte Spill
	leaq	51676(%r14,%r10), %rbx
	movl	51676(%r14,%r10), %edi
	movl	%edi, 216(%rsp)         # 4-byte Spill
	leaq	51680(%r14,%r10), %rdi
	movq	%rdi, 384(%rsp)         # 8-byte Spill
	movl	51680(%r14,%r10), %edi
	movl	%edi, 220(%rsp)         # 4-byte Spill
	leaq	51684(%r14,%r10), %rdi
	movq	%rdi, 392(%rsp)         # 8-byte Spill
	movl	51684(%r14,%r10), %edi
	movl	%edi, 224(%rsp)         # 4-byte Spill
	leaq	51688(%r14,%r10), %rdi
	movq	%rdi, 400(%rsp)         # 8-byte Spill
	movl	51688(%r14,%r10), %edi
	movl	%edi, 228(%rsp)         # 4-byte Spill
	leaq	51692(%r14,%r10), %r11
	movl	51692(%r14,%r10), %edi
	movl	%edi, 232(%rsp)         # 4-byte Spill
	leaq	51696(%r14,%r10), %rdi
	movq	%rdi, 408(%rsp)         # 8-byte Spill
	movl	51696(%r14,%r10), %edi
	movl	%edi, 236(%rsp)         # 4-byte Spill
	leaq	51700(%r14,%r10), %rdi
	movq	%rdi, 416(%rsp)         # 8-byte Spill
	movl	51700(%r14,%r10), %edi
	movl	%edi, 240(%rsp)         # 4-byte Spill
	leaq	51704(%r14,%r10), %rdi
	movq	%rdi, 312(%rsp)         # 8-byte Spill
	movl	51704(%r14,%r10), %edi
	movl	%edi, 244(%rsp)         # 4-byte Spill
	leaq	51708(%r14,%r10), %rdi
	movq	%rdi, 424(%rsp)         # 8-byte Spill
	movl	51708(%r14,%r10), %edi
	movl	%edi, 248(%rsp)         # 4-byte Spill
	leaq	51712(%r14,%r10), %rdi
	movq	%rdi, 432(%rsp)         # 8-byte Spill
	movl	51712(%r14,%r10), %edi
	movl	%edi, 252(%rsp)         # 4-byte Spill
	leaq	51716(%r14,%r10), %rdi
	movq	%rdi, 264(%rsp)         # 8-byte Spill
	movl	51716(%r14,%r10), %r10d
	movl	$0, 272(%rsp)           # 4-byte Folded Spill
	movq	168(%rsp), %rdi         # 8-byte Reload
.LBB3_278:                              # %.preheader70.preheader.i.i
                                        #   in Loop: Header=BB3_249 Depth=2
	movq	320(%rsp), %r8          # 8-byte Reload
	movl	%ebp, (%r8)
	addl	%ebp, %r9d
	movq	304(%rsp), %rbp         # 8-byte Reload
	movl	%r9d, (%rbp)
	addl	%eax, %r9d
	movq	328(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax)
	addl	%esi, %r9d
	movq	152(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax)
	addl	%edx, %r9d
	movq	336(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax)
	addl	192(%rsp), %r9d         # 4-byte Folded Reload
	movq	344(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax)
	addl	196(%rsp), %r9d         # 4-byte Folded Reload
	movq	352(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax)
	addl	200(%rsp), %r9d         # 4-byte Folded Reload
	movq	176(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax)
	addl	204(%rsp), %r9d         # 4-byte Folded Reload
	movq	360(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax)
	addl	208(%rsp), %r9d         # 4-byte Folded Reload
	movq	368(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax)
	addl	212(%rsp), %r9d         # 4-byte Folded Reload
	movq	376(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax)
	addl	216(%rsp), %r9d         # 4-byte Folded Reload
	movl	%r9d, (%rbx)
	addl	220(%rsp), %r9d         # 4-byte Folded Reload
	movq	384(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax)
	addl	224(%rsp), %r9d         # 4-byte Folded Reload
	movq	392(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax)
	addl	228(%rsp), %r9d         # 4-byte Folded Reload
	movq	400(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax)
	addl	232(%rsp), %r9d         # 4-byte Folded Reload
	movl	%r9d, (%r11)
	addl	236(%rsp), %r9d         # 4-byte Folded Reload
	movq	408(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax)
	addl	240(%rsp), %r9d         # 4-byte Folded Reload
	movq	416(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax)
	addl	244(%rsp), %r9d         # 4-byte Folded Reload
	movq	312(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax)
	addl	248(%rsp), %r9d         # 4-byte Folded Reload
	movq	424(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax)
	addl	252(%rsp), %r9d         # 4-byte Folded Reload
	movq	432(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax)
	addl	%r10d, %r9d
	movq	264(%rsp), %rax         # 8-byte Reload
	movl	%r9d, (%rax)
	imulq	$1032, %rdi, %r11       # imm = 0x408
	cmpb	$0, 272(%rsp)           # 1-byte Folded Reload
	movdqu	%xmm9, 45512(%r14,%r11)
	movdqu	%xmm9, 45500(%r14,%r11)
	movdqu	%xmm9, 45484(%r14,%r11)
	movdqu	%xmm9, 45468(%r14,%r11)
	movdqu	%xmm9, 45452(%r14,%r11)
	movdqu	%xmm9, 45436(%r14,%r11)
	jne	.LBB3_281
# BB#279:                               # %.lr.ph79.preheader.i.i
                                        #   in Loop: Header=BB3_249 Depth=2
	movslq	%r15d, %r9
	movslq	%r12d, %r8
	leaq	(%r14,%r11), %rsi
	movl	51628(%rsi,%r9,4), %ebp
	leaq	(%r13,%r9,4), %rsi
	decq	%r9
	xorl	%edi, %edi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_280:                              # %.lr.ph79.i.i
                                        #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_249 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	51632(%rsi,%rdi,4), %edx
	movl	%edx, %eax
	subl	%ebp, %eax
	leal	-1(%rax,%rbx), %ebp
	leal	(%rax,%rbx), %eax
	movl	%ebp, 45436(%rsi,%rdi,4)
	addl	%eax, %eax
	leaq	1(%r9,%rdi), %rbp
	incq	%rdi
	cmpq	%r8, %rbp
	movl	%edx, %ebp
	movl	%eax, %ebx
	jl	.LBB3_280
.LBB3_281:                              # %.preheader.i.i
                                        #   in Loop: Header=BB3_249 Depth=2
	cmpl	%r12d, %r15d
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	jge	.LBB3_292
# BB#282:                               # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB3_249 Depth=2
	movslq	%r15d, %rdx
	movslq	%r12d, %rsi
	movq	%rsi, %r9
	subq	%rdx, %r9
	cmpq	$3, %r9
	jbe	.LBB3_291
# BB#283:                               # %min.iters.checked408
                                        #   in Loop: Header=BB3_249 Depth=2
	movq	%r9, %r10
	andq	$-4, %r10
	movq	%r9, %r12
	andq	$-4, %r12
	je	.LBB3_291
# BB#284:                               # %vector.body405.preheader
                                        #   in Loop: Header=BB3_249 Depth=2
	leaq	-4(%r12), %rdi
	movq	%rdi, %rax
	shrq	$2, %rax
	btl	$2, %edi
	jb	.LBB3_286
# BB#285:                               # %vector.body405.prol
                                        #   in Loop: Header=BB3_249 Depth=2
	addq	%r14, %r11
	movdqu	45436(%r11,%rdx,4), %xmm0
	paddd	%xmm0, %xmm0
	paddd	%xmm2, %xmm0
	movdqu	51632(%r11,%rdx,4), %xmm1
	psubd	%xmm1, %xmm0
	movdqu	%xmm0, 51632(%r11,%rdx,4)
	movl	$4, %edi
	testq	%rax, %rax
	jne	.LBB3_287
	jmp	.LBB3_289
.LBB3_286:                              #   in Loop: Header=BB3_249 Depth=2
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.LBB3_289
.LBB3_287:                              # %vector.body405.preheader.new
                                        #   in Loop: Header=BB3_249 Depth=2
	movq	%r12, %rax
	subq	%rdi, %rax
	addq	%rdx, %rdi
	movq	296(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdi,4), %rbp
	.p2align	4, 0x90
.LBB3_288:                              # %vector.body405
                                        #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_249 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	-6212(%rbp), %xmm0
	paddd	%xmm0, %xmm0
	paddd	%xmm2, %xmm0
	movdqu	-16(%rbp), %xmm1
	psubd	%xmm1, %xmm0
	movdqu	%xmm0, -16(%rbp)
	movdqu	-6196(%rbp), %xmm0
	paddd	%xmm0, %xmm0
	paddd	%xmm2, %xmm0
	movdqu	(%rbp), %xmm1
	psubd	%xmm1, %xmm0
	movdqu	%xmm0, (%rbp)
	addq	$32, %rbp
	addq	$-8, %rax
	jne	.LBB3_288
.LBB3_289:                              # %middle.block406
                                        #   in Loop: Header=BB3_249 Depth=2
	cmpq	%r12, %r9
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB3_292
# BB#290:                               #   in Loop: Header=BB3_249 Depth=2
	addq	%r10, %rdx
	.p2align	4, 0x90
.LBB3_291:                              # %.lr.ph.i.i
                                        #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_249 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	45436(%r13,%rdx,4), %eax
	leal	2(%rax,%rax), %eax
	subl	51632(%r13,%rdx,4), %eax
	movl	%eax, 51632(%r13,%rdx,4)
	incq	%rdx
	cmpq	%rdx, %rsi
	jne	.LBB3_291
.LBB3_292:                              # %CreateDecodeTables.exit.i
                                        #   in Loop: Header=BB3_249 Depth=2
	movq	168(%rsp), %rbx         # 8-byte Reload
	movl	%r15d, 64012(%r14,%rbx,4)
	incq	%rbx
	movq	184(%rsp), %rsi         # 8-byte Reload
	addq	$258, %rsi              # imm = 0x102
	addq	$258, %rcx              # imm = 0x102
	addq	$1032, %r13             # imm = 0x408
	addq	$1032, 296(%rsp)        # 8-byte Folded Spill
                                        # imm = 0x408
	cmpq	488(%rsp), %rbx         # 8-byte Folded Reload
	movq	280(%rsp), %r12         # 8-byte Reload
	movl	12(%rsp), %eax          # 4-byte Reload
	jne	.LBB3_249
# BB#293:                               #   in Loop: Header=BB3_5 Depth=1
	movl	%edi, %r13d
	jmp	.LBB3_302
.LBB3_294:                              # %.lr.ph1570.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rdx
	movl	8(%rdx), %esi
	decl	%esi
	.p2align	4, 0x90
.LBB3_295:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %esi
	je	.LBB3_300
# BB#296:                               #   in Loop: Header=BB3_295 Depth=2
	movl	32(%r14), %edi
	shll	$8, %edi
	movq	(%rdx), %rbx
	movzbl	(%rbx), %eax
	orl	%edi, %eax
	movl	%eax, 32(%r14)
	addl	$8, %ecx
	movl	%ecx, 36(%r14)
	incq	%rbx
	movq	%rbx, (%rdx)
	movl	%esi, 8(%rdx)
	incl	12(%rdx)
	jne	.LBB3_298
# BB#297:                               #   in Loop: Header=BB3_295 Depth=2
	incl	16(%rdx)
.LBB3_298:                              # %.backedge1416.i
                                        #   in Loop: Header=BB3_295 Depth=2
	decl	%esi
	cmpl	$4, %ecx
	jle	.LBB3_295
.LBB3_299:                              # %._crit_edge1571.i
                                        #   in Loop: Header=BB3_5 Depth=1
	addl	$-5, %ecx
	shrl	%cl, %eax
	andl	$31, %eax
	movl	%ecx, 36(%r14)
	xorl	%r8d, %r8d
	movl	%eax, %ecx
	jmp	.LBB3_316
.LBB3_300:                              #   in Loop: Header=BB3_5 Depth=1
	xorl	%eax, %eax
	jmp	.LBB3_490
.LBB3_301:                              #   in Loop: Header=BB3_5 Depth=1
	xorl	%r13d, %r13d
.LBB3_302:                              # %._crit_edge1661.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	3192(%r14), %ebx
	movl	40(%r14), %ebp
	xorl	%esi, %esi
	movl	$1024, %edx             # imm = 0x400
	movq	472(%rsp), %rdi         # 8-byte Reload
	callq	memset
	incl	%ebx
	movl	%ebx, 32(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_303:                              # %.preheader1444.i
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	240(%rax), %edx
	leal	255(%rax), %esi
	movb	%sil, 7819(%r14,%rax)
	leal	254(%rax), %esi
	movb	%sil, 7818(%r14,%rax)
	leal	253(%rax), %esi
	movb	%sil, 7817(%r14,%rax)
	leal	252(%rax), %esi
	movb	%sil, 7816(%r14,%rax)
	leal	251(%rax), %esi
	movb	%sil, 7815(%r14,%rax)
	leal	250(%rax), %esi
	movb	%sil, 7814(%r14,%rax)
	leal	249(%rax), %esi
	movb	%sil, 7813(%r14,%rax)
	leal	248(%rax), %esi
	movb	%sil, 7812(%r14,%rax)
	leal	247(%rax), %esi
	movb	%sil, 7811(%r14,%rax)
	leal	246(%rax), %esi
	movb	%sil, 7810(%r14,%rax)
	leal	245(%rax), %esi
	movb	%sil, 7809(%r14,%rax)
	leal	244(%rax), %esi
	movb	%sil, 7808(%r14,%rax)
	leal	243(%rax), %esi
	movb	%sil, 7807(%r14,%rax)
	leal	242(%rax), %esi
	movb	%sil, 7806(%r14,%rax)
	leal	241(%rax), %esi
	movb	%sil, 7805(%r14,%rax)
	movb	%dl, 7804(%r14,%rax)
	leal	4080(%rax), %edx
	movl	%edx, 7880(%r14,%rcx,4)
	leaq	-1(%rcx), %rdx
	addq	$15, %rcx
	addq	$-16, %rax
	testq	%rcx, %rcx
	movq	%rdx, %rcx
	jg	.LBB3_303
# BB#304:                               #   in Loop: Header=BB3_5 Depth=1
	imull	$100000, %ebp, %r11d    # imm = 0x186A0
	movl	132(%rsp), %esi         # 4-byte Reload
	testl	%esi, %esi
	pxor	%xmm9, %xmm9
	movdqa	.LCPI3_0(%rip), %xmm2   # xmm2 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
	movdqa	.LCPI3_1(%rip), %xmm3   # xmm3 = [16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
	jle	.LBB3_315
# BB#305:                               #   in Loop: Header=BB3_5 Depth=1
	movzbl	7884(%r14), %eax
	movl	64012(%r14,%rax,4), %ecx
	movq	%rax, 160(%rsp)         # 8-byte Spill
	imulq	$1032, %rax, %rax       # imm = 0x408
	leaq	45436(%r14,%rax), %rdx
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	leaq	57820(%r14,%rax), %rdx
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	leaq	51628(%r14,%rax), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	$256, %r8d              # imm = 0x100
	movl	$49, 40(%rsp)           # 4-byte Folded Spill
	xorl	%r15d, %r15d
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	xorl	%r10d, %r10d
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movq	144(%rsp), %rbp         # 8-byte Reload
	movl	%r13d, %r9d
	movq	256(%rsp), %r13         # 8-byte Reload
.LBB3_306:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$36, 8(%r14)
	movl	36(%r14), %edx
	cmpl	4(%rsp), %edx           # 4-byte Folded Reload
	jge	.LBB3_312
# BB#307:                               # %.lr.ph1643.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rcx
	movl	8(%rcx), %esi
	decl	%esi
	.p2align	4, 0x90
.LBB3_308:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %esi
	je	.LBB3_314
# BB#309:                               #   in Loop: Header=BB3_308 Depth=2
	movl	32(%r14), %edi
	shll	$8, %edi
	movq	(%rcx), %rbx
	movzbl	(%rbx), %eax
	orl	%edi, %eax
	movl	%eax, 32(%r14)
	addl	$8, %edx
	movl	%edx, 36(%r14)
	incq	%rbx
	movq	%rbx, (%rcx)
	movl	%esi, 8(%rcx)
	incl	12(%rcx)
	jne	.LBB3_311
# BB#310:                               #   in Loop: Header=BB3_308 Depth=2
	incl	16(%rcx)
.LBB3_311:                              # %.backedge1443.i
                                        #   in Loop: Header=BB3_308 Depth=2
	decl	%esi
	cmpl	4(%rsp), %edx           # 4-byte Folded Reload
	jl	.LBB3_308
	jmp	.LBB3_313
.LBB3_312:                              # %.._crit_edge1644_crit_edge.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	32(%r14), %eax
.LBB3_313:                              # %._crit_edge1644.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	4(%rsp), %ebx           # 4-byte Reload
	subl	%ebx, %edx
	movl	%edx, %ecx
	shrl	%cl, %eax
	movl	$1, %esi
	movl	%ebx, %ecx
	shll	%cl, %esi
	movq	%rsi, %rcx
	decl	%ecx
	andl	%eax, %ecx
	movl	%edx, 36(%r14)
.LBB3_341:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$-4, %eax
	cmpl	$20, %ebx
	jg	.LBB3_353
# BB#342:                               #   in Loop: Header=BB3_5 Depth=1
	movq	%rcx, %rdi
	movslq	%ebx, %rcx
	movq	112(%rsp), %rsi         # 8-byte Reload
	cmpl	(%rsi,%rcx,4), %edi
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	jle	.LBB3_354
# BB#343:                               #   in Loop: Header=BB3_5 Depth=1
	incl	%ebx
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	jmp	.LBB3_344
.LBB3_314:                              #   in Loop: Header=BB3_5 Depth=1
	xorl	%eax, %eax
	jmp	.LBB3_490
.LBB3_315:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$-4, %eax
	movl	$256, %r8d              # imm = 0x100
	xorl	%r15d, %r15d
	movl	$0, 40(%rsp)            # 4-byte Folded Spill
	xorl	%r10d, %r10d
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movq	144(%rsp), %rbp         # 8-byte Reload
	movl	%r13d, %r9d
	movq	256(%rsp), %r13         # 8-byte Reload
	jmp	.LBB3_490
.LBB3_180:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$39, 8(%r14)
	testl	%edx, %edx
	jle	.LBB3_182
# BB#181:                               # %.._crit_edge1594_crit_edge.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	32(%r14), %eax
	jmp	.LBB3_187
.LBB3_182:                              # %.lr.ph1593.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rcx
	movl	8(%rcx), %esi
	decl	%esi
	.p2align	4, 0x90
.LBB3_183:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %esi
	je	.LBB3_188
# BB#184:                               #   in Loop: Header=BB3_183 Depth=2
	movl	32(%r14), %edi
	shll	$8, %edi
	movq	(%rcx), %rbx
	movzbl	(%rbx), %eax
	orl	%edi, %eax
	movl	%eax, 32(%r14)
	addl	$8, %edx
	movl	%edx, 36(%r14)
	incq	%rbx
	movq	%rbx, (%rcx)
	movl	%esi, 8(%rcx)
	incl	12(%rcx)
	jne	.LBB3_186
# BB#185:                               #   in Loop: Header=BB3_183 Depth=2
	incl	16(%rcx)
.LBB3_186:                              # %.backedge1425.i
                                        #   in Loop: Header=BB3_183 Depth=2
	decl	%esi
	leal	-8(%rdx), %edi
	cmpl	$-8, %edi
	jle	.LBB3_183
.LBB3_187:                              # %._crit_edge1594.i
                                        #   in Loop: Header=BB3_5 Depth=1
	decl	%edx
	movl	%edx, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	movl	%edx, 36(%r14)
	movq	88(%rsp), %rcx          # 8-byte Reload
	leal	(%rax,%rcx,2), %ecx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movl	4(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB3_395
.LBB3_188:                              #   in Loop: Header=BB3_5 Depth=1
	xorl	%eax, %eax
	jmp	.LBB3_490
.LBB3_189:                              #   in Loop: Header=BB3_5 Depth=1
	movq	%rbp, %rbx
	movl	$41, 8(%r14)
	testl	%edx, %edx
	jle	.LBB3_191
# BB#190:                               # %.._crit_edge1614_crit_edge.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	32(%r14), %eax
	jmp	.LBB3_196
.LBB3_191:                              # %.lr.ph1613.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rcx
	movl	8(%rcx), %esi
	decl	%esi
	.p2align	4, 0x90
.LBB3_192:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %esi
	je	.LBB3_197
# BB#193:                               #   in Loop: Header=BB3_192 Depth=2
	movl	32(%r14), %edi
	shll	$8, %edi
	movq	(%rcx), %rbp
	movzbl	(%rbp), %eax
	orl	%edi, %eax
	movl	%eax, 32(%r14)
	addl	$8, %edx
	movl	%edx, 36(%r14)
	incq	%rbp
	movq	%rbp, (%rcx)
	movl	%esi, 8(%rcx)
	incl	12(%rcx)
	jne	.LBB3_195
# BB#194:                               #   in Loop: Header=BB3_192 Depth=2
	incl	16(%rcx)
.LBB3_195:                              # %.backedge1435.i
                                        #   in Loop: Header=BB3_192 Depth=2
	decl	%esi
	leal	-8(%rdx), %edi
	cmpl	$-8, %edi
	jle	.LBB3_192
.LBB3_196:                              # %._crit_edge1614.i
                                        #   in Loop: Header=BB3_5 Depth=1
	decl	%edx
	movl	%edx, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	movl	%edx, 36(%r14)
	movq	88(%rsp), %rcx          # 8-byte Reload
	leal	(%rax,%rcx,2), %ecx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movq	%rbx, %rbp
	movl	4(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB3_465
.LBB3_197:                              #   in Loop: Header=BB3_5 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rbp
	jmp	.LBB3_490
.LBB3_210:                              #   in Loop: Header=BB3_5 Depth=1
	movslq	%r8d, %rcx
	xorl	%eax, %eax
	cmpb	$0, 3452(%r14,%rcx)
	je	.LBB3_214
.LBB3_211:                              #   in Loop: Header=BB3_5 Depth=1
	cmpl	$15, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	jg	.LBB3_214
.LBB3_212:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$29, 8(%r14)
	movl	36(%r14), %eax
	testl	%eax, %eax
	jle	.LBB3_225
# BB#213:                               # %.._crit_edge1551_crit_edge.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	32(%r14), %esi
	jmp	.LBB3_230
.LBB3_214:                              #   in Loop: Header=BB3_5 Depth=1
	incl	%r8d
	cmpl	$15, %r8d
	jle	.LBB3_210
.LBB3_215:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$0, 3192(%r14)
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_216:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, 3196(%r14,%rcx)
	je	.LBB3_218
# BB#217:                               #   in Loop: Header=BB3_216 Depth=2
	cltq
	movb	%cl, 3468(%r14,%rax)
	movl	3192(%r14), %eax
	incl	%eax
	movl	%eax, 3192(%r14)
.LBB3_218:                              #   in Loop: Header=BB3_216 Depth=2
	cmpb	$0, 3197(%r14,%rcx)
	je	.LBB3_220
# BB#219:                               #   in Loop: Header=BB3_216 Depth=2
	leal	1(%rcx), %edx
	cltq
	movb	%dl, 3468(%r14,%rax)
	movl	3192(%r14), %eax
	incl	%eax
	movl	%eax, 3192(%r14)
.LBB3_220:                              #   in Loop: Header=BB3_216 Depth=2
	addq	$2, %rcx
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB3_216
# BB#221:                               # %makeMaps_d.exit.i
                                        #   in Loop: Header=BB3_5 Depth=1
	testl	%eax, %eax
	je	.LBB3_241
# BB#222:                               #   in Loop: Header=BB3_5 Depth=1
	addl	$2, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 12(%rsp)          # 4-byte Spill
.LBB3_223:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$30, 8(%r14)
	movl	36(%r14), %ecx
	cmpl	$3, %ecx
	jl	.LBB3_233
# BB#224:                               # %.._crit_edge1681_crit_edge.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	32(%r14), %edx
	jmp	.LBB3_238
.LBB3_225:                              # %.lr.ph1550.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rcx
	movl	8(%rcx), %edx
	decl	%edx
	.p2align	4, 0x90
.LBB3_226:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %edx
	je	.LBB3_240
# BB#227:                               #   in Loop: Header=BB3_226 Depth=2
	movl	32(%r14), %edi
	shll	$8, %edi
	movq	(%rcx), %rbx
	movzbl	(%rbx), %esi
	orl	%edi, %esi
	movl	%esi, 32(%r14)
	addl	$8, %eax
	movl	%eax, 36(%r14)
	incq	%rbx
	movq	%rbx, (%rcx)
	movl	%edx, 8(%rcx)
	incl	12(%rcx)
	jne	.LBB3_229
# BB#228:                               #   in Loop: Header=BB3_226 Depth=2
	incl	16(%rcx)
.LBB3_229:                              # %.backedge1408.i
                                        #   in Loop: Header=BB3_226 Depth=2
	decl	%edx
	leal	-8(%rax), %edi
	cmpl	$-8, %edi
	jle	.LBB3_226
.LBB3_230:                              # %._crit_edge1551.i
                                        #   in Loop: Header=BB3_5 Depth=1
	decl	%eax
	movl	%eax, 36(%r14)
	btl	%eax, %esi
	movl	8(%rsp), %ecx           # 4-byte Reload
	jae	.LBB3_232
# BB#231:                               #   in Loop: Header=BB3_5 Depth=1
	movl	%r8d, %eax
	shll	$4, %eax
	addl	%ecx, %eax
	cltq
	movb	$1, 3196(%r14,%rax)
.LBB3_232:                              #   in Loop: Header=BB3_5 Depth=1
	incl	%ecx
	movl	%ecx, %eax
	jmp	.LBB3_211
.LBB3_233:                              # %.lr.ph1680.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rax
	movl	8(%rax), %esi
	decl	%esi
	.p2align	4, 0x90
.LBB3_234:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %esi
	je	.LBB3_242
# BB#235:                               #   in Loop: Header=BB3_234 Depth=2
	movl	32(%r14), %edi
	shll	$8, %edi
	movq	(%rax), %rbx
	movzbl	(%rbx), %edx
	orl	%edi, %edx
	movl	%edx, 32(%r14)
	addl	$8, %ecx
	movl	%ecx, 36(%r14)
	incq	%rbx
	movq	%rbx, (%rax)
	movl	%esi, 8(%rax)
	incl	12(%rax)
	jne	.LBB3_237
# BB#236:                               #   in Loop: Header=BB3_234 Depth=2
	incl	16(%rax)
.LBB3_237:                              # %.backedge1452.i
                                        #   in Loop: Header=BB3_234 Depth=2
	decl	%esi
	cmpl	$2, %ecx
	jle	.LBB3_234
.LBB3_238:                              # %._crit_edge1681.i
                                        #   in Loop: Header=BB3_5 Depth=1
	addl	$-3, %ecx
	shrl	%cl, %edx
	movl	%edx, %ebp
	andl	$7, %ebp
	movl	%ecx, 36(%r14)
	xorl	%eax, %eax
	testb	%al, %al
	jne	.LBB3_125
# BB#239:                               # %._crit_edge1681.i
                                        #   in Loop: Header=BB3_5 Depth=1
	andb	$7, %dl
	movl	$-4, %eax
	movzbl	%dl, %edx
	movl	$131, %esi
	btl	%edx, %esi
	jb	.LBB3_490
	jmp	.LBB3_125
.LBB3_240:                              #   in Loop: Header=BB3_5 Depth=1
	xorl	%eax, %eax
	jmp	.LBB3_490
.LBB3_241:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$-4, %eax
	jmp	.LBB3_490
.LBB3_242:                              #   in Loop: Header=BB3_5 Depth=1
	xorl	%eax, %eax
	jmp	.LBB3_490
.LBB3_316:                              #   in Loop: Header=BB3_5 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, %r8d
	jge	.LBB3_318
# BB#317:                               #   in Loop: Header=BB3_5 Depth=1
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jmp	.LBB3_319
.LBB3_318:                              #   in Loop: Header=BB3_5 Depth=1
	incl	%r9d
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	%edx, 132(%rsp)         # 4-byte Spill
	jmp	.LBB3_243
.LBB3_319:                              #   in Loop: Header=BB3_5 Depth=1
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	leal	-1(%rcx), %ecx
	movl	$-4, %eax
	cmpl	$19, %ecx
	ja	.LBB3_490
.LBB3_320:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$34, 8(%r14)
	movl	36(%r14), %eax
	testl	%eax, %eax
	jle	.LBB3_322
# BB#321:                               # %.._crit_edge1566_crit_edge.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	32(%r14), %esi
	jmp	.LBB3_327
.LBB3_322:                              # %.lr.ph1565.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rcx
	movl	8(%rcx), %edx
	decl	%edx
	.p2align	4, 0x90
.LBB3_323:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %edx
	je	.LBB3_340
# BB#324:                               #   in Loop: Header=BB3_323 Depth=2
	movl	32(%r14), %edi
	shll	$8, %edi
	movq	(%rcx), %rbx
	movzbl	(%rbx), %esi
	orl	%edi, %esi
	movl	%esi, 32(%r14)
	addl	$8, %eax
	movl	%eax, 36(%r14)
	incq	%rbx
	movq	%rbx, (%rcx)
	movl	%edx, 8(%rcx)
	incl	12(%rcx)
	jne	.LBB3_326
# BB#325:                               #   in Loop: Header=BB3_323 Depth=2
	incl	16(%rcx)
.LBB3_326:                              # %.backedge1414.i
                                        #   in Loop: Header=BB3_323 Depth=2
	decl	%edx
	leal	-8(%rax), %edi
	cmpl	$-8, %edi
	jle	.LBB3_323
.LBB3_327:                              # %._crit_edge1566.i
                                        #   in Loop: Header=BB3_5 Depth=1
	decl	%eax
	movl	%eax, 36(%r14)
	btl	%eax, %esi
	jae	.LBB3_337
.LBB3_328:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$35, 8(%r14)
	testl	%eax, %eax
	jle	.LBB3_330
# BB#329:                               # %.._crit_edge1561_crit_edge.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	32(%r14), %esi
	jmp	.LBB3_335
.LBB3_330:                              # %.lr.ph1560.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rcx
	movl	8(%rcx), %edx
	decl	%edx
	.p2align	4, 0x90
.LBB3_331:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %edx
	je	.LBB3_340
# BB#332:                               #   in Loop: Header=BB3_331 Depth=2
	movl	32(%r14), %edi
	shll	$8, %edi
	movq	(%rcx), %rbx
	movzbl	(%rbx), %esi
	orl	%edi, %esi
	movl	%esi, 32(%r14)
	addl	$8, %eax
	movl	%eax, 36(%r14)
	incq	%rbx
	movq	%rbx, (%rcx)
	movl	%edx, 8(%rcx)
	incl	12(%rcx)
	jne	.LBB3_334
# BB#333:                               #   in Loop: Header=BB3_331 Depth=2
	incl	16(%rcx)
.LBB3_334:                              # %.backedge1412.i
                                        #   in Loop: Header=BB3_331 Depth=2
	decl	%edx
	leal	-8(%rax), %edi
	cmpl	$-8, %edi
	jle	.LBB3_331
.LBB3_335:                              # %._crit_edge1561.i
                                        #   in Loop: Header=BB3_5 Depth=1
	decl	%eax
	movl	%eax, 36(%r14)
	btl	%eax, %esi
	jae	.LBB3_338
# BB#336:                               #   in Loop: Header=BB3_5 Depth=1
	movq	120(%rsp), %rcx         # 8-byte Reload
	decl	%ecx
	jmp	.LBB3_319
.LBB3_340:                              #   in Loop: Header=BB3_5 Depth=1
	xorl	%eax, %eax
	jmp	.LBB3_490
.LBB3_337:                              #   in Loop: Header=BB3_5 Depth=1
	movslq	%r9d, %rax
	movslq	%r8d, %r8
	imulq	$258, %rax, %rax        # imm = 0x102
	addq	%r14, %rax
	movq	120(%rsp), %rcx         # 8-byte Reload
	movb	%cl, 43888(%r8,%rax)
	incl	%r8d
	jmp	.LBB3_316
.LBB3_338:                              #   in Loop: Header=BB3_5 Depth=1
	movq	120(%rsp), %rcx         # 8-byte Reload
	incl	%ecx
	jmp	.LBB3_319
.LBB3_353:                              #   in Loop: Header=BB3_5 Depth=1
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	jmp	.LBB3_490
.LBB3_354:                              #   in Loop: Header=BB3_5 Depth=1
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	movq	104(%rsp), %rdx         # 8-byte Reload
	movslq	(%rdx,%rcx,4), %rdx
	movslq	%edi, %rcx
	subq	%rdx, %rcx
	cmpl	$257, %ecx              # imm = 0x101
	ja	.LBB3_490
# BB#355:                               #   in Loop: Header=BB3_5 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%rcx,4), %ecx
.LBB3_356:                              # %.loopexit1427.i
                                        #   in Loop: Header=BB3_5 Depth=1
	cmpl	32(%rsp), %ecx          # 4-byte Folded Reload
	movl	%r11d, 168(%rsp)        # 4-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	jne	.LBB3_376
# BB#357:                               #   in Loop: Header=BB3_5 Depth=1
	movl	56(%r14), %ecx
	movl	$-4, %eax
	testl	%ecx, %ecx
	js	.LBB3_489
# BB#358:                               #   in Loop: Header=BB3_5 Depth=1
	cmpl	%r15d, %ecx
	jge	.LBB3_489
# BB#359:                               # %min.iters.checked395
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	$0, 1096(%r14)
	movq	$-256, %rcx
	.p2align	4, 0x90
.LBB3_360:                              # %vector.body392
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	1092(%r14,%rcx,4), %xmm0
	movups	%xmm0, 2124(%r14,%rcx,4)
	movups	1108(%r14,%rcx,4), %xmm0
	movups	%xmm0, 2140(%r14,%rcx,4)
	movups	1124(%r14,%rcx,4), %xmm0
	movups	%xmm0, 2156(%r14,%rcx,4)
	movdqu	1140(%r14,%rcx,4), %xmm0
	movdqu	%xmm0, 2172(%r14,%rcx,4)
	addq	$16, %rcx
	jne	.LBB3_360
# BB#361:                               # %.preheader1423.preheader.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	1096(%r14), %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_362:                              # %.preheader1423.i
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	1100(%r14,%rdx,4), %ecx
	movl	%ecx, 1100(%r14,%rdx,4)
	addl	1104(%r14,%rdx,4), %ecx
	movl	%ecx, 1104(%r14,%rdx,4)
	addl	1108(%r14,%rdx,4), %ecx
	movl	%ecx, 1108(%r14,%rdx,4)
	addl	1112(%r14,%rdx,4), %ecx
	movl	%ecx, 1112(%r14,%rdx,4)
	addq	$4, %rdx
	cmpq	$256, %rdx              # imm = 0x100
	jne	.LBB3_362
# BB#363:                               # %.preheader1421.i.preheader
                                        #   in Loop: Header=BB3_5 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB3_365
.LBB3_364:                              #   in Loop: Header=BB3_365 Depth=2
	addq	$4, %rcx
.LBB3_365:                              # %.preheader1421.i
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	1096(%r14,%rcx,4), %edx
	testl	%edx, %edx
	js	.LBB3_375
# BB#366:                               # %.preheader1421.i
                                        #   in Loop: Header=BB3_365 Depth=2
	cmpl	%r15d, %edx
	jg	.LBB3_375
# BB#367:                               #   in Loop: Header=BB3_365 Depth=2
	leaq	1(%rcx), %rdx
	cmpq	$257, %rdx              # imm = 0x101
	jge	.LBB3_426
# BB#368:                               # %.preheader1421.i.1
                                        #   in Loop: Header=BB3_365 Depth=2
	movq	%rcx, %r8
	orq	$1, %r8
	movl	1100(%r14,%rcx,4), %edx
	testl	%edx, %edx
	js	.LBB3_489
# BB#369:                               # %.preheader1421.i.1
                                        #   in Loop: Header=BB3_365 Depth=2
	cmpl	%r15d, %edx
	jg	.LBB3_489
# BB#370:                               # %.preheader1421.i.2581
                                        #   in Loop: Header=BB3_365 Depth=2
	movq	%rcx, %r8
	orq	$2, %r8
	movl	1104(%r14,%rcx,4), %edx
	testl	%edx, %edx
	js	.LBB3_489
# BB#371:                               # %.preheader1421.i.2581
                                        #   in Loop: Header=BB3_365 Depth=2
	cmpl	%r15d, %edx
	jg	.LBB3_489
# BB#372:                               # %.preheader1421.i.3582
                                        #   in Loop: Header=BB3_365 Depth=2
	movl	1108(%r14,%rcx,4), %edx
	testl	%edx, %edx
	js	.LBB3_374
# BB#373:                               # %.preheader1421.i.3582
                                        #   in Loop: Header=BB3_365 Depth=2
	cmpl	%r15d, %edx
	jle	.LBB3_364
.LBB3_374:                              # %.preheader1421.i.3582..loopexit.loopexit1711.i_crit_edge
                                        #   in Loop: Header=BB3_5 Depth=1
	addq	$3, %rcx
.LBB3_375:                              # %.loopexit.loopexit1711.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	%rcx, %r8
	jmp	.LBB3_489
.LBB3_376:                              #   in Loop: Header=BB3_5 Depth=1
	movl	%ecx, %eax
	orl	$1, %eax
	cmpl	$1, %eax
	jne	.LBB3_399
# BB#377:                               #   in Loop: Header=BB3_5 Depth=1
	movl	$-1, %eax
	movl	$1, %ecx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
.LBB3_378:                              #   in Loop: Header=BB3_5 Depth=1
	movl	40(%rsp), %ecx          # 4-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	cmpl	$1, %edx
	je	.LBB3_381
# BB#379:                               #   in Loop: Header=BB3_5 Depth=1
	testl	%edx, %edx
	jne	.LBB3_382
# BB#380:                               #   in Loop: Header=BB3_5 Depth=1
	movq	136(%rsp), %rdx         # 8-byte Reload
	addl	%edx, %eax
	jmp	.LBB3_383
.LBB3_381:                              #   in Loop: Header=BB3_5 Depth=1
	movq	136(%rsp), %rdx         # 8-byte Reload
	leal	(%rax,%rdx,2), %eax
	jmp	.LBB3_383
.LBB3_382:                              #   in Loop: Header=BB3_5 Depth=1
	movq	136(%rsp), %rdx         # 8-byte Reload
.LBB3_383:                              #   in Loop: Header=BB3_5 Depth=1
	addl	%edx, %edx
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	testl	%ecx, %ecx
	movq	%rax, 80(%rsp)          # 8-byte Spill
	jne	.LBB3_386
# BB#384:                               #   in Loop: Header=BB3_5 Depth=1
	movslq	%r10d, %r10
	incq	%r10
	cmpl	16(%rsp), %r10d         # 4-byte Folded Reload
	jge	.LBB3_417
# BB#385:                               #   in Loop: Header=BB3_5 Depth=1
	movzbl	7884(%r14,%r10), %eax
	movl	64012(%r14,%rax,4), %ecx
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	movq	%rax, 160(%rsp)         # 8-byte Spill
	imulq	$1032, %rax, %rax       # imm = 0x408
	leaq	45436(%r14,%rax), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	leaq	57820(%r14,%rax), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	51628(%r14,%rax), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	$50, %ecx
.LBB3_386:                              #   in Loop: Header=BB3_5 Depth=1
	decl	%ecx
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	movl	28(%rsp), %eax          # 4-byte Reload
	movl	%eax, 4(%rsp)           # 4-byte Spill
.LBB3_387:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$38, 8(%r14)
	movl	36(%r14), %edx
	cmpl	4(%rsp), %edx           # 4-byte Folded Reload
	jge	.LBB3_393
# BB#388:                               # %.lr.ph1608.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rcx
	movl	8(%rcx), %esi
	decl	%esi
	.p2align	4, 0x90
.LBB3_389:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %esi
	je	.LBB3_398
# BB#390:                               #   in Loop: Header=BB3_389 Depth=2
	movl	32(%r14), %edi
	shll	$8, %edi
	movq	(%rcx), %rbx
	movzbl	(%rbx), %eax
	orl	%edi, %eax
	movl	%eax, 32(%r14)
	addl	$8, %edx
	movl	%edx, 36(%r14)
	incq	%rbx
	movq	%rbx, (%rcx)
	movl	%esi, 8(%rcx)
	incl	12(%rcx)
	jne	.LBB3_392
# BB#391:                               #   in Loop: Header=BB3_389 Depth=2
	incl	16(%rcx)
.LBB3_392:                              # %.backedge1433.i
                                        #   in Loop: Header=BB3_389 Depth=2
	decl	%esi
	cmpl	4(%rsp), %edx           # 4-byte Folded Reload
	jl	.LBB3_389
	jmp	.LBB3_394
.LBB3_393:                              # %.._crit_edge1609_crit_edge.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	32(%r14), %eax
.LBB3_394:                              # %._crit_edge1609.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	4(%rsp), %ebx           # 4-byte Reload
	subl	%ebx, %edx
	movl	%edx, %ecx
	shrl	%cl, %eax
	movl	$1, %esi
	movl	%ebx, %ecx
	shll	%cl, %esi
	movq	%rsi, %rcx
	decl	%ecx
	andl	%eax, %ecx
	movl	%edx, 36(%r14)
.LBB3_395:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$-4, %eax
	cmpl	$20, %ebx
	jg	.LBB3_353
# BB#396:                               #   in Loop: Header=BB3_5 Depth=1
	movq	%rcx, %rdi
	movslq	%ebx, %rcx
	movq	112(%rsp), %rsi         # 8-byte Reload
	cmpl	(%rsi,%rcx,4), %edi
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	jle	.LBB3_414
# BB#397:                               #   in Loop: Header=BB3_5 Depth=1
	incl	%ebx
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	jmp	.LBB3_180
.LBB3_398:                              #   in Loop: Header=BB3_5 Depth=1
	xorl	%eax, %eax
	jmp	.LBB3_490
.LBB3_399:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$-4, %eax
	cmpl	%r11d, %r15d
	jge	.LBB3_490
# BB#400:                               #   in Loop: Header=BB3_5 Depth=1
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	leal	-1(%rcx), %ecx
	cmpl	$15, %ecx
	ja	.LBB3_418
# BB#401:                               #   in Loop: Header=BB3_5 Depth=1
	movslq	7820(%r14), %rsi
	movq	%rsi, %rdx
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	leal	(%rsi,%rcx), %edx
	movb	3724(%r14,%rdx), %r11b
	cmpl	$4, %ecx
	jb	.LBB3_405
# BB#402:                               # %.lr.ph1627.i.preheader
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	152(%rsp), %rcx         # 8-byte Reload
	leal	-1(%rcx), %esi
	movq	48(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
.LBB3_403:                              # %.lr.ph1627.i
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rsi,%rdx), %edi
	leal	-1(%rsi,%rdx), %ebx
	movslq	%ebx, %rbx
	movzbl	3724(%r14,%rbx), %ebp
	movslq	%edi, %rdi
	movb	%bpl, 3724(%r14,%rdi)
	leal	-2(%rsi,%rdx), %edi
	movslq	%edi, %rdi
	movzbl	3724(%r14,%rdi), %ecx
	movb	%cl, 3724(%r14,%rbx)
	leal	-3(%rsi,%rdx), %ecx
	movslq	%ecx, %rcx
	movzbl	3724(%r14,%rcx), %ebx
	movb	%bl, 3724(%r14,%rdi)
	leal	-4(%rsi,%rdx), %edi
	movslq	%edi, %rdi
	movzbl	3724(%r14,%rdi), %ebx
	movb	%bl, 3724(%r14,%rcx)
	leal	-4(%rdx), %ecx
	addl	$-5, %edx
	cmpl	$3, %edx
	movl	%ecx, %edx
	ja	.LBB3_403
# BB#404:                               # %.preheader1438.loopexit.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	leal	3(%rcx), %ecx
	andl	$3, %ecx
.LBB3_405:                              # %.preheader1438.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movb	%r11b, 264(%rsp)        # 1-byte Spill
	movq	%r10, 272(%rsp)         # 8-byte Spill
	movl	%r9d, 184(%rsp)         # 4-byte Spill
	movq	%r12, 280(%rsp)         # 8-byte Spill
	testl	%ecx, %ecx
	je	.LBB3_413
# BB#406:                               # %.lr.ph1623.preheader.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	%ecx, %ebp
	leal	-1(%rcx), %r12d
	movl	%ecx, %edi
	andl	$3, %edi
	je	.LBB3_410
# BB#407:                               # %.lr.ph1623.i.prol.preheader
                                        #   in Loop: Header=BB3_5 Depth=1
	negq	%rbp
	addl	152(%rsp), %ecx         # 4-byte Folded Reload
	negl	%edi
.LBB3_408:                              # %.lr.ph1623.i.prol
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %ebx
	leal	-1(%rcx), %ecx
	movzbl	3724(%r14,%rcx), %edx
	movb	%dl, 3724(%r14,%rbx)
	incq	%rbp
	incl	%edi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	jne	.LBB3_408
# BB#409:                               # %.lr.ph1623.i.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB3_5 Depth=1
	negq	%rbp
.LBB3_410:                              # %.lr.ph1623.i.prol.loopexit
                                        #   in Loop: Header=BB3_5 Depth=1
	cmpl	$3, %r12d
	jb	.LBB3_413
# BB#411:                               # %.lr.ph1623.preheader.i.new
                                        #   in Loop: Header=BB3_5 Depth=1
	leal	-3(%rbp), %ecx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movq	152(%rsp), %rcx         # 8-byte Reload
	leal	-3(%rbp,%rcx), %r13d
	leal	-4(%rbp,%rcx), %r11d
	leal	-2(%rbp,%rcx), %r12d
	leal	-1(%rbp,%rcx), %ebx
	addl	%ecx, %ebp
	xorl	%edi, %edi
.LBB3_412:                              # %.lr.ph1623.i
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rbp,%rdi), %esi
	leal	(%rbx,%rdi), %r9d
	movzbl	3724(%r14,%r9), %r10d
	movb	%r10b, 3724(%r14,%rsi)
	leal	(%r12,%rdi), %esi
	movzbl	3724(%r14,%rsi), %ecx
	movb	%cl, 3724(%r14,%r9)
	leal	(%r13,%rdi), %ecx
	movzbl	3724(%r14,%rcx), %edx
	movb	%dl, 3724(%r14,%rsi)
	leal	(%r11,%rdi), %edx
	movzbl	3724(%r14,%rdx), %edx
	movb	%dl, 3724(%r14,%rcx)
	movq	176(%rsp), %rcx         # 8-byte Reload
	leal	-4(%rcx,%rdi), %ecx
	leal	-4(%rdi), %edx
	cmpl	$-3, %ecx
	movl	%edx, %edi
	jne	.LBB3_412
.LBB3_413:                              # %._crit_edge1624.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movb	264(%rsp), %r11b        # 1-byte Reload
	movq	152(%rsp), %rcx         # 8-byte Reload
	movb	%r11b, 3724(%r14,%rcx)
	movq	256(%rsp), %r13         # 8-byte Reload
	movq	280(%rsp), %r12         # 8-byte Reload
	movq	144(%rsp), %rbp         # 8-byte Reload
	movl	184(%rsp), %r9d         # 4-byte Reload
	movq	272(%rsp), %r10         # 8-byte Reload
	jmp	.LBB3_450
.LBB3_414:                              #   in Loop: Header=BB3_5 Depth=1
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	movq	104(%rsp), %rdx         # 8-byte Reload
	movslq	(%rdx,%rcx,4), %rdx
	movslq	%edi, %rcx
	subq	%rdx, %rcx
	cmpl	$257, %ecx              # imm = 0x101
	ja	.LBB3_490
# BB#415:                               #   in Loop: Header=BB3_5 Depth=1
	movq	96(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx,%rcx,4), %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	orl	$1, %ecx
	cmpl	$1, %ecx
	jne	.LBB3_421
# BB#416:                               #   in Loop: Header=BB3_5 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	jmp	.LBB3_378
.LBB3_417:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$-4, %eax
	movl	$0, 40(%rsp)            # 4-byte Folded Spill
	jmp	.LBB3_490
.LBB3_418:                              #   in Loop: Header=BB3_5 Depth=1
	movl	%ecx, %edx
	shrl	$4, %edx
	movl	7820(%r14,%rdx,4), %esi
	movl	%ecx, %edi
	andl	$15, %edi
	addl	%esi, %edi
	movl	%ecx, %ebp
	andl	$15, %ebp
	movslq	%edi, %rdi
	movb	3724(%r14,%rdi), %r11b
	je	.LBB3_444
.LBB3_419:                              # %.lr.ph1639.i
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	3723(%r14,%rdi), %ebx
	movb	%bl, 3724(%r14,%rdi)
	decq	%rdi
	movslq	7820(%r14,%rdx,4), %rsi
	cmpq	%rsi, %rdi
	jg	.LBB3_419
# BB#420:                               # %._crit_edge1640.i
                                        #   in Loop: Header=BB3_5 Depth=1
	incl	%esi
	movl	%esi, 7820(%r14,%rdx,4)
	testl	%edx, %edx
	jne	.LBB3_445
	jmp	.LBB3_447
.LBB3_421:                              #   in Loop: Header=BB3_5 Depth=1
	movq	80(%rsp), %rsi          # 8-byte Reload
	leal	1(%rsi), %ecx
	movslq	7820(%r14), %rdx
	movzbl	3724(%r14,%rdx), %edx
	movzbl	3468(%r14,%rdx), %edx
	addl	%ecx, 68(%r14,%rdx,4)
	cmpb	$0, 44(%r14)
	je	.LBB3_469
# BB#422:                               # %.preheader1429.i
                                        #   in Loop: Header=BB3_5 Depth=1
	testl	%esi, %esi
	js	.LBB3_475
# BB#423:                               # %.lr.ph1604.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movslq	%r15d, %r15
	movslq	%r11d, %rsi
.LBB3_424:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rsi, %r15
	jge	.LBB3_476
# BB#425:                               #   in Loop: Header=BB3_424 Depth=2
	movq	3160(%r14), %rdi
	movw	%dx, (%rdi,%r15,2)
	incq	%r15
	leal	-1(%rcx), %edi
	cmpl	$1, %ecx
	movl	%edi, %ecx
	jg	.LBB3_424
	jmp	.LBB3_473
.LBB3_426:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$0, 16(%r14)
	movb	$0, 12(%r14)
	movl	$2, 8(%r14)
	cmpb	$0, 44(%r14)
	je	.LBB3_477
# BB#427:                               # %vector.body.preheader
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	movq	$-256, %rax
.LBB3_428:                              # %vector.body
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	2120(%r14,%rax,4), %xmm0
	movups	%xmm0, 3148(%r14,%rax,4)
	movups	2136(%r14,%rax,4), %xmm0
	movups	%xmm0, 3164(%r14,%rax,4)
	movups	2152(%r14,%rax,4), %xmm0
	movups	%xmm0, 3180(%r14,%rax,4)
	movups	2168(%r14,%rax,4), %xmm0
	movups	%xmm0, 3196(%r14,%rax,4)
	movups	2184(%r14,%rax,4), %xmm0
	movups	%xmm0, 3212(%r14,%rax,4)
	movups	2200(%r14,%rax,4), %xmm0
	movups	%xmm0, 3228(%r14,%rax,4)
	movups	2216(%r14,%rax,4), %xmm0
	movups	%xmm0, 3244(%r14,%rax,4)
	movdqu	2232(%r14,%rax,4), %xmm0
	movdqu	%xmm0, 3260(%r14,%rax,4)
	addq	$32, %rax
	jne	.LBB3_428
# BB#429:                               # %.preheader1420.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	2120(%r14), %eax
	movl	%eax, 3148(%r14)
	testl	%r15d, %r15d
	jle	.LBB3_435
# BB#430:                               # %.lr.ph1586.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	%r15d, %r8d
	xorl	%ecx, %ecx
.LBB3_431:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	3160(%r14), %rax
	movzbl	(%rax,%rcx,2), %edx
	movl	2124(%r14,%rdx,4), %esi
	movw	%si, (%rax,%rcx,2)
	movq	3168(%r14), %rdi
	movl	%ecx, %eax
	sarl	%eax
	movslq	%eax, %rbp
	movzbl	(%rdi,%rbp), %eax
	testb	$1, %cl
	jne	.LBB3_433
# BB#432:                               #   in Loop: Header=BB3_431 Depth=2
	andl	$240, %eax
	sarl	$16, %esi
	orl	%eax, %esi
	jmp	.LBB3_434
.LBB3_433:                              #   in Loop: Header=BB3_431 Depth=2
	andl	$15, %eax
	sarl	$16, %esi
	shll	$4, %esi
	orl	%eax, %esi
.LBB3_434:                              #   in Loop: Header=BB3_431 Depth=2
	movb	%sil, (%rdi,%rbp)
	incl	2124(%r14,%rdx,4)
	incq	%rcx
	cmpq	%rcx, %r8
	jne	.LBB3_431
.LBB3_435:                              # %._crit_edge1587.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movslq	56(%r14), %rdx
	movq	3160(%r14), %rbp
	movq	3168(%r14), %rax
	movzwl	(%rbp,%rdx,2), %esi
	movl	%edx, %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %edi
	movl	%edx, %ecx
	shlb	$2, %cl
	andb	$4, %cl
	shrl	%cl, %edi
	movl	%edi, %ecx
	andl	$15, %ecx
	shll	$16, %ecx
	orl	%esi, %ecx
	jmp	.LBB3_437
.LBB3_436:                              # %._crit_edge1928.i
                                        #   in Loop: Header=BB3_437 Depth=2
	movq	3160(%r14), %rbp
	movq	3168(%r14), %rax
	movl	%r8d, %edx
	movl	8(%rsp), %ecx           # 4-byte Reload
.LBB3_437:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %r8d
	movslq	%r8d, %rbx
	movzwl	(%rbp,%rbx,2), %esi
	movl	%ebx, %ecx
	sarl	%ecx
	movslq	%ecx, %rdi
	movzbl	(%rax,%rdi), %r11d
	leal	(,%rbx,4), %ecx
	andl	$4, %ecx
	shrl	%cl, %r11d
	andl	$15, %r11d
	shll	$16, %r11d
	movl	%r11d, 8(%rsp)          # 4-byte Spill
	movw	%dx, (%rbp,%rbx,2)
	movzbl	(%rax,%rdi), %ebp
	testb	$1, %bl
	jne	.LBB3_439
# BB#438:                               #   in Loop: Header=BB3_437 Depth=2
	andl	$240, %ebp
	sarl	$16, %edx
	orl	%ebp, %edx
	jmp	.LBB3_440
.LBB3_439:                              #   in Loop: Header=BB3_437 Depth=2
	andl	$15, %ebp
	sarl	$16, %edx
	shll	$4, %edx
	orl	%ebp, %edx
.LBB3_440:                              #   in Loop: Header=BB3_437 Depth=2
	movl	168(%rsp), %r11d        # 4-byte Reload
	addl	%esi, 8(%rsp)           # 4-byte Folded Spill
	movb	%dl, (%rax,%rdi)
	cmpl	56(%r14), %r8d
	jne	.LBB3_436
# BB#441:                               #   in Loop: Header=BB3_5 Depth=1
	movl	%r8d, 60(%r14)
	movl	$0, 1092(%r14)
	xorl	%eax, %eax
	movl	$256, %edx              # imm = 0x100
.LBB3_442:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rax,%rdx), %esi
	sarl	%esi
	movslq	%esi, %rsi
	cmpl	%r8d, 1096(%r14,%rsi,4)
	cmovgl	%esi, %edx
	cmovlel	%esi, %eax
	movl	%edx, %esi
	subl	%eax, %esi
	cmpl	$1, %esi
	jne	.LBB3_442
# BB#443:                               # %indexIntoF.exit.i34
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	%eax, 64(%r14)
	movl	%r8d, %eax
	movq	3160(%r14), %rdx
	movq	3168(%r14), %rsi
	movzwl	(%rdx,%rax,2), %edx
	movl	%r8d, %eax
	shrl	%eax
	movzbl	(%rsi,%rax), %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	andl	$15, %eax
	shll	$16, %eax
	orl	%edx, %eax
	movq	144(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB3_488
.LBB3_444:                              # %._crit_edge1640.thread.i
                                        #   in Loop: Header=BB3_5 Depth=1
	incl	%esi
	movl	%esi, 7820(%r14,%rdx,4)
.LBB3_445:                              # %.lr.ph1635.i.preheader
                                        #   in Loop: Header=BB3_5 Depth=1
	movslq	%esi, %rsi
	leaq	-1(%rsi), %rdi
	movl	%edi, 7820(%r14,%rdx,4)
	movslq	7816(%r14,%rdx,4), %rdi
	movb	3739(%r14,%rdi), %bl
	movb	%bl, 3723(%r14,%rsi)
	cmpl	$32, %ecx
	jb	.LBB3_447
.LBB3_446:                              # %.lr.ph1635..lr.ph1635_crit_edge.i
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	7816(%r14,%rdx,4), %rcx
	leaq	-1(%rcx), %rsi
	movl	%esi, 7816(%r14,%rdx,4)
	movslq	7812(%r14,%rdx,4), %rsi
	movzbl	3739(%r14,%rsi), %ebx
	movb	%bl, 3723(%r14,%rcx)
	decq	%rdx
	cmpq	$1, %rdx
	jg	.LBB3_446
.LBB3_447:                              # %._crit_edge1636.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movslq	7820(%r14), %rcx
	leaq	-1(%rcx), %rdx
	movl	%edx, 7820(%r14)
	movb	%r11b, 3723(%r14,%rcx)
	cmpl	$0, 7820(%r14)
	movq	144(%rsp), %rbp         # 8-byte Reload
	jne	.LBB3_450
# BB#448:                               # %.preheader1439.i.preheader
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	$7819, %ecx             # imm = 0x1E8B
	movl	$1970, %edx             # imm = 0x7B2
.LBB3_449:                              # %.preheader1439.i
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%r14,%rdx,4), %rsi
	movzbl	3739(%r14,%rsi), %ebx
	movb	%bl, (%r14,%rcx)
	movslq	(%r14,%rdx,4), %rsi
	movzbl	3738(%r14,%rsi), %ebx
	movb	%bl, -1(%r14,%rcx)
	movslq	(%r14,%rdx,4), %rsi
	movzbl	3737(%r14,%rsi), %ebx
	movb	%bl, -2(%r14,%rcx)
	movslq	(%r14,%rdx,4), %rsi
	movzbl	3736(%r14,%rsi), %ebx
	movb	%bl, -3(%r14,%rcx)
	movslq	(%r14,%rdx,4), %rsi
	movzbl	3735(%r14,%rsi), %ebx
	movb	%bl, -4(%r14,%rcx)
	movslq	(%r14,%rdx,4), %rsi
	movzbl	3734(%r14,%rsi), %ebx
	movb	%bl, -5(%r14,%rcx)
	movslq	(%r14,%rdx,4), %rsi
	movzbl	3733(%r14,%rsi), %ebx
	movb	%bl, -6(%r14,%rcx)
	movslq	(%r14,%rdx,4), %rsi
	movzbl	3732(%r14,%rsi), %ebx
	movb	%bl, -7(%r14,%rcx)
	movslq	(%r14,%rdx,4), %rsi
	movzbl	3731(%r14,%rsi), %ebx
	movb	%bl, -8(%r14,%rcx)
	movslq	(%r14,%rdx,4), %rsi
	movzbl	3730(%r14,%rsi), %ebx
	movb	%bl, -9(%r14,%rcx)
	movslq	(%r14,%rdx,4), %rsi
	movzbl	3729(%r14,%rsi), %ebx
	movb	%bl, -10(%r14,%rcx)
	movslq	(%r14,%rdx,4), %rsi
	movzbl	3728(%r14,%rsi), %ebx
	movb	%bl, -11(%r14,%rcx)
	movslq	(%r14,%rdx,4), %rsi
	movzbl	3727(%r14,%rsi), %ebx
	movb	%bl, -12(%r14,%rcx)
	movslq	(%r14,%rdx,4), %rsi
	movzbl	3726(%r14,%rsi), %ebx
	movb	%bl, -13(%r14,%rcx)
	movslq	(%r14,%rdx,4), %rsi
	movzbl	3725(%r14,%rsi), %ebx
	movb	%bl, -14(%r14,%rcx)
	movslq	(%r14,%rdx,4), %rsi
	movzbl	3724(%r14,%rsi), %ebx
	movb	%bl, -15(%r14,%rcx)
	leal	-3739(%rcx), %esi
	movl	%esi, (%r14,%rdx,4)
	leaq	-1(%rdx), %rsi
	addq	$-1955, %rdx            # imm = 0xF85D
	addq	$-16, %rcx
	testq	%rdx, %rdx
	movq	%rsi, %rdx
	jg	.LBB3_449
.LBB3_450:                              # %.loopexit1441.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movzbl	%r11b, %ecx
	movzbl	3468(%r14,%rcx), %edx
	incl	68(%r14,%rdx,4)
	cmpb	$0, 44(%r14)
	movzbl	3468(%r14,%rcx), %ecx
	je	.LBB3_452
# BB#451:                               #   in Loop: Header=BB3_5 Depth=1
	movq	3160(%r14), %rdx
	movslq	%r15d, %rsi
	movw	%cx, (%rdx,%rsi,2)
	jmp	.LBB3_453
.LBB3_452:                              #   in Loop: Header=BB3_5 Depth=1
	movzwl	%cx, %ecx
	movq	3152(%r14), %rdx
	movslq	%r15d, %rsi
	movl	%ecx, (%rdx,%rsi,4)
.LBB3_453:                              #   in Loop: Header=BB3_5 Depth=1
	movl	168(%rsp), %r11d        # 4-byte Reload
	incl	%r15d
	movl	40(%rsp), %ecx          # 4-byte Reload
	testl	%ecx, %ecx
	jne	.LBB3_456
# BB#454:                               #   in Loop: Header=BB3_5 Depth=1
	movslq	%r10d, %r10
	incq	%r10
	cmpl	16(%rsp), %r10d         # 4-byte Folded Reload
	jge	.LBB3_474
# BB#455:                               #   in Loop: Header=BB3_5 Depth=1
	movzbl	7884(%r14,%r10), %eax
	movl	64012(%r14,%rax,4), %ecx
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	movq	%rax, 160(%rsp)         # 8-byte Spill
	imulq	$1032, %rax, %rax       # imm = 0x408
	leaq	45436(%r14,%rax), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	leaq	57820(%r14,%rax), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	51628(%r14,%rax), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	$50, %ecx
.LBB3_456:                              #   in Loop: Header=BB3_5 Depth=1
	decl	%ecx
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	movl	28(%rsp), %eax          # 4-byte Reload
	movl	%eax, 4(%rsp)           # 4-byte Spill
.LBB3_457:                              #   in Loop: Header=BB3_5 Depth=1
	movl	%r9d, 184(%rsp)         # 4-byte Spill
	movq	%r13, %r9
	movl	%r11d, %r13d
	movl	$40, 8(%r14)
	movl	36(%r14), %edx
	movl	4(%rsp), %edi           # 4-byte Reload
	cmpl	%edi, %edx
	movq	%rbp, %r11
	jge	.LBB3_463
# BB#458:                               # %.lr.ph1618.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	(%r14), %rcx
	movl	8(%rcx), %esi
	decl	%esi
	.p2align	4, 0x90
.LBB3_459:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$-1, %esi
	je	.LBB3_468
# BB#460:                               #   in Loop: Header=BB3_459 Depth=2
	movl	32(%r14), %edi
	shll	$8, %edi
	movq	(%rcx), %rbp
	movzbl	(%rbp), %eax
	orl	%edi, %eax
	movl	%eax, 32(%r14)
	addl	$8, %edx
	movl	%edx, 36(%r14)
	incq	%rbp
	movq	%rbp, (%rcx)
	movl	%esi, 8(%rcx)
	incl	12(%rcx)
	jne	.LBB3_462
# BB#461:                               #   in Loop: Header=BB3_459 Depth=2
	incl	16(%rcx)
.LBB3_462:                              # %.backedge1437.i
                                        #   in Loop: Header=BB3_459 Depth=2
	decl	%esi
	movl	4(%rsp), %edi           # 4-byte Reload
	cmpl	%edi, %edx
	jl	.LBB3_459
	jmp	.LBB3_464
.LBB3_463:                              # %.._crit_edge1619_crit_edge.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	32(%r14), %eax
.LBB3_464:                              # %._crit_edge1619.i
                                        #   in Loop: Header=BB3_5 Depth=1
	subl	%edi, %edx
	movl	%edx, %ecx
	shrl	%cl, %eax
	movl	$1, %esi
	movl	%edi, %ecx
	shll	%cl, %esi
	movq	%rsi, %rcx
	decl	%ecx
	andl	%eax, %ecx
	movl	%edx, 36(%r14)
	movl	%edi, %ebx
	movq	%r11, %rbp
	movl	%r13d, %r11d
	movq	%r9, %r13
	movl	184(%rsp), %r9d         # 4-byte Reload
.LBB3_465:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$-4, %eax
	cmpl	$20, %ebx
	jg	.LBB3_353
# BB#466:                               #   in Loop: Header=BB3_5 Depth=1
	movq	%rcx, %rdi
	movslq	%ebx, %rcx
	movq	112(%rsp), %rsi         # 8-byte Reload
	cmpl	(%rsi,%rcx,4), %edi
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	jle	.LBB3_354
# BB#467:                               #   in Loop: Header=BB3_5 Depth=1
	incl	%ebx
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	jmp	.LBB3_189
.LBB3_468:                              #   in Loop: Header=BB3_5 Depth=1
	xorl	%eax, %eax
	movq	%r11, %rbp
	movl	%r13d, %r11d
	movq	%r9, %r13
	movl	184(%rsp), %r9d         # 4-byte Reload
	jmp	.LBB3_490
.LBB3_469:                              # %.preheader1426.i
                                        #   in Loop: Header=BB3_5 Depth=1
	testl	%esi, %esi
	js	.LBB3_475
# BB#470:                               # %.lr.ph1599.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movzwl	%dx, %edx
	movslq	%r15d, %r15
	movslq	%r11d, %rsi
.LBB3_471:                              #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rsi, %r15
	jge	.LBB3_476
# BB#472:                               #   in Loop: Header=BB3_471 Depth=2
	movq	3152(%r14), %rdi
	movl	%edx, (%rdi,%r15,4)
	incq	%r15
	leal	-1(%rcx), %edi
	cmpl	$1, %ecx
	movl	%edi, %ecx
	jg	.LBB3_471
.LBB3_473:                              #   in Loop: Header=BB3_5 Depth=1
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movq	48(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB3_356
.LBB3_474:                              #   in Loop: Header=BB3_5 Depth=1
	movl	$0, 40(%rsp)            # 4-byte Folded Spill
	jmp	.LBB3_490
.LBB3_475:                              #   in Loop: Header=BB3_5 Depth=1
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	48(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB3_356
.LBB3_476:                              #   in Loop: Header=BB3_5 Depth=1
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	jmp	.LBB3_490
.LBB3_477:                              # %.preheader.i29
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	3152(%r14), %rcx
	testl	%r15d, %r15d
	jle	.LBB3_480
# BB#478:                               # %.lr.ph1580.preheader.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	%r15d, %eax
	testb	$1, %al
	jne	.LBB3_481
# BB#479:                               #   in Loop: Header=BB3_5 Depth=1
	xorl	%edx, %edx
	jmp	.LBB3_482
.LBB3_480:                              #   in Loop: Header=BB3_5 Depth=1
	xorl	%r8d, %r8d
	jmp	.LBB3_486
.LBB3_481:                              # %.lr.ph1580.i.prol
                                        #   in Loop: Header=BB3_5 Depth=1
	movzbl	(%rcx), %ecx
	incl	1096(%r14,%rcx,4)
	movq	3152(%r14), %rcx
	movl	$1, %edx
.LBB3_482:                              # %.lr.ph1580.i.prol.loopexit
                                        #   in Loop: Header=BB3_5 Depth=1
	cmpl	$1, %r15d
	movl	%r15d, %r8d
	je	.LBB3_486
# BB#483:                               # %.lr.ph1580.preheader.i.new
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	%rbp, %r11
	movl	%edx, %esi
	shll	$8, %esi
	leal	256(%rsi), %edi
.LBB3_484:                              # %.lr.ph1580.i
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx,%rdx,4), %ebp
	movslq	1096(%r14,%rbp,4), %rbx
	orl	%esi, (%rcx,%rbx,4)
	incl	1096(%r14,%rbp,4)
	movq	3152(%r14), %rcx
	movzbl	4(%rcx,%rdx,4), %ebp
	movslq	1096(%r14,%rbp,4), %rbx
	orl	%edi, (%rcx,%rbx,4)
	incl	1096(%r14,%rbp,4)
	addq	$2, %rdx
	movq	3152(%r14), %rcx
	addl	$512, %edi              # imm = 0x200
	addl	$512, %esi              # imm = 0x200
	cmpq	%rdx, %rax
	jne	.LBB3_484
# BB#485:                               #   in Loop: Header=BB3_5 Depth=1
	movl	%r15d, %r8d
	movq	%r11, %rbp
	movl	168(%rsp), %r11d        # 4-byte Reload
.LBB3_486:                              # %._crit_edge1581.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movslq	56(%r14), %rax
	movl	(%rcx,%rax,4), %edx
	shrl	$8, %edx
	movl	%edx, 60(%r14)
	movl	$0, 1092(%r14)
	imull	$100000, 40(%r14), %esi # imm = 0x186A0
	movl	$1, %eax
	cmpl	%esi, %edx
	jae	.LBB3_491
# BB#487:                               #   in Loop: Header=BB3_5 Depth=1
	movl	%edx, %eax
	movl	(%rcx,%rax,4), %eax
	movzbl	%al, %ecx
	movl	%ecx, 64(%r14)
	shrl	$8, %eax
.LBB3_488:                              #   in Loop: Header=BB3_5 Depth=1
	movl	%eax, 60(%r14)
	movl	$1, 1092(%r14)
	xorl	%eax, %eax
.LBB3_489:                              # %.loopexit.loopexit1711.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB3_490:                              # %BZ2_decompress.exit
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	%r8d, 64036(%r14)
	movl	8(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, 64040(%r14)
	movl	%r9d, 64044(%r14)
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 64048(%r14)
	movl	%ebp, 64052(%r14)
	movl	16(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 64056(%r14)
	movl	32(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 64060(%r14)
	movl	%r10d, 64064(%r14)
	movl	40(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 64068(%r14)
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 64072(%r14)
	movl	%r11d, 64076(%r14)
	movl	%r15d, 64080(%r14)
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 64084(%r14)
	movq	136(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, 64088(%r14)
	movq	120(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, 64092(%r14)
	movl	%r12d, 64096(%r14)
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, 64100(%r14)
	movq	88(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 64104(%r14)
	movl	60(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 64108(%r14)
	movq	160(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, 64112(%r14)
	movl	28(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 64116(%r14)
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 64120(%r14)
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 64128(%r14)
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 64136(%r14)
	cmpl	$4, %eax
	je	.LBB3_494
.LBB3_491:                              # %BZ2_decompress.exit.thread
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	$2, %ecx
	cmpl	$2, 8(%r14)
	je	.LBB3_5
	jmp	.LBB3_3
	.p2align	4, 0x90
.LBB3_492:                              # %.outer.split.split.split
                                        # =>This Inner Loop Header: Depth=1
	jmp	.LBB3_492
.LBB3_493:
	movl	$-1, %eax
	jmp	.LBB3_3
.LBB3_494:
	movl	$4, %eax
	jmp	.LBB3_3
.LBB3_495:
	movl	$-4, %eax
	jmp	.LBB3_3
.Lfunc_end3:
	.size	nsis_BZ2_bzDecompress, .Lfunc_end3-nsis_BZ2_bzDecompress
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_84
	.quad	.LBB3_490
	.quad	.LBB3_490
	.quad	.LBB3_490
	.quad	.LBB3_490
	.quad	.LBB3_490
	.quad	.LBB3_490
	.quad	.LBB3_490
	.quad	.LBB3_490
	.quad	.LBB3_490
	.quad	.LBB3_490
	.quad	.LBB3_96
	.quad	.LBB3_105
	.quad	.LBB3_114
	.quad	.LBB3_200
	.quad	.LBB3_212
	.quad	.LBB3_223
	.quad	.LBB3_124
	.quad	.LBB3_141
	.quad	.LBB3_245
	.quad	.LBB3_320
	.quad	.LBB3_127
	.quad	.LBB3_306
	.quad	.LBB3_128
	.quad	.LBB3_387
	.quad	.LBB3_129
	.quad	.LBB3_457
	.quad	.LBB3_130

	.text
	.globl	nsis_BZ2_bzDecompressEnd
	.p2align	4, 0x90
	.type	nsis_BZ2_bzDecompressEnd,@function
nsis_BZ2_bzDecompressEnd:               # @nsis_BZ2_bzDecompressEnd
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -24
.Lcfi23:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$-2, %eax
	testq	%rbx, %rbx
	je	.LBB4_10
# BB#1:
	movq	48(%rbx), %r14
	testq	%r14, %r14
	je	.LBB4_10
# BB#2:
	cmpq	%rbx, (%r14)
	jne	.LBB4_10
# BB#3:
	movq	3152(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB4_5
# BB#4:
	movq	72(%rbx), %rdi
	callq	*64(%rbx)
.LBB4_5:
	movq	3160(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB4_7
# BB#6:
	movq	72(%rbx), %rdi
	callq	*64(%rbx)
.LBB4_7:
	movq	3168(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB4_9
# BB#8:
	movq	72(%rbx), %rdi
	callq	*64(%rbx)
.LBB4_9:
	movq	48(%rbx), %rsi
	movq	72(%rbx), %rdi
	callq	*64(%rbx)
	movq	$0, 48(%rbx)
	xorl	%eax, %eax
.LBB4_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	nsis_BZ2_bzDecompressEnd, .Lfunc_end4-nsis_BZ2_bzDecompressEnd
	.cfi_endproc

	.type	BZ2_rNums,@object       # @BZ2_rNums
	.section	.rodata,"a",@progbits
	.globl	BZ2_rNums
	.p2align	4
BZ2_rNums:
	.long	619                     # 0x26b
	.long	720                     # 0x2d0
	.long	127                     # 0x7f
	.long	481                     # 0x1e1
	.long	931                     # 0x3a3
	.long	816                     # 0x330
	.long	813                     # 0x32d
	.long	233                     # 0xe9
	.long	566                     # 0x236
	.long	247                     # 0xf7
	.long	985                     # 0x3d9
	.long	724                     # 0x2d4
	.long	205                     # 0xcd
	.long	454                     # 0x1c6
	.long	863                     # 0x35f
	.long	491                     # 0x1eb
	.long	741                     # 0x2e5
	.long	242                     # 0xf2
	.long	949                     # 0x3b5
	.long	214                     # 0xd6
	.long	733                     # 0x2dd
	.long	859                     # 0x35b
	.long	335                     # 0x14f
	.long	708                     # 0x2c4
	.long	621                     # 0x26d
	.long	574                     # 0x23e
	.long	73                      # 0x49
	.long	654                     # 0x28e
	.long	730                     # 0x2da
	.long	472                     # 0x1d8
	.long	419                     # 0x1a3
	.long	436                     # 0x1b4
	.long	278                     # 0x116
	.long	496                     # 0x1f0
	.long	867                     # 0x363
	.long	210                     # 0xd2
	.long	399                     # 0x18f
	.long	680                     # 0x2a8
	.long	480                     # 0x1e0
	.long	51                      # 0x33
	.long	878                     # 0x36e
	.long	465                     # 0x1d1
	.long	811                     # 0x32b
	.long	169                     # 0xa9
	.long	869                     # 0x365
	.long	675                     # 0x2a3
	.long	611                     # 0x263
	.long	697                     # 0x2b9
	.long	867                     # 0x363
	.long	561                     # 0x231
	.long	862                     # 0x35e
	.long	687                     # 0x2af
	.long	507                     # 0x1fb
	.long	283                     # 0x11b
	.long	482                     # 0x1e2
	.long	129                     # 0x81
	.long	807                     # 0x327
	.long	591                     # 0x24f
	.long	733                     # 0x2dd
	.long	623                     # 0x26f
	.long	150                     # 0x96
	.long	238                     # 0xee
	.long	59                      # 0x3b
	.long	379                     # 0x17b
	.long	684                     # 0x2ac
	.long	877                     # 0x36d
	.long	625                     # 0x271
	.long	169                     # 0xa9
	.long	643                     # 0x283
	.long	105                     # 0x69
	.long	170                     # 0xaa
	.long	607                     # 0x25f
	.long	520                     # 0x208
	.long	932                     # 0x3a4
	.long	727                     # 0x2d7
	.long	476                     # 0x1dc
	.long	693                     # 0x2b5
	.long	425                     # 0x1a9
	.long	174                     # 0xae
	.long	647                     # 0x287
	.long	73                      # 0x49
	.long	122                     # 0x7a
	.long	335                     # 0x14f
	.long	530                     # 0x212
	.long	442                     # 0x1ba
	.long	853                     # 0x355
	.long	695                     # 0x2b7
	.long	249                     # 0xf9
	.long	445                     # 0x1bd
	.long	515                     # 0x203
	.long	909                     # 0x38d
	.long	545                     # 0x221
	.long	703                     # 0x2bf
	.long	919                     # 0x397
	.long	874                     # 0x36a
	.long	474                     # 0x1da
	.long	882                     # 0x372
	.long	500                     # 0x1f4
	.long	594                     # 0x252
	.long	612                     # 0x264
	.long	641                     # 0x281
	.long	801                     # 0x321
	.long	220                     # 0xdc
	.long	162                     # 0xa2
	.long	819                     # 0x333
	.long	984                     # 0x3d8
	.long	589                     # 0x24d
	.long	513                     # 0x201
	.long	495                     # 0x1ef
	.long	799                     # 0x31f
	.long	161                     # 0xa1
	.long	604                     # 0x25c
	.long	958                     # 0x3be
	.long	533                     # 0x215
	.long	221                     # 0xdd
	.long	400                     # 0x190
	.long	386                     # 0x182
	.long	867                     # 0x363
	.long	600                     # 0x258
	.long	782                     # 0x30e
	.long	382                     # 0x17e
	.long	596                     # 0x254
	.long	414                     # 0x19e
	.long	171                     # 0xab
	.long	516                     # 0x204
	.long	375                     # 0x177
	.long	682                     # 0x2aa
	.long	485                     # 0x1e5
	.long	911                     # 0x38f
	.long	276                     # 0x114
	.long	98                      # 0x62
	.long	553                     # 0x229
	.long	163                     # 0xa3
	.long	354                     # 0x162
	.long	666                     # 0x29a
	.long	933                     # 0x3a5
	.long	424                     # 0x1a8
	.long	341                     # 0x155
	.long	533                     # 0x215
	.long	870                     # 0x366
	.long	227                     # 0xe3
	.long	730                     # 0x2da
	.long	475                     # 0x1db
	.long	186                     # 0xba
	.long	263                     # 0x107
	.long	647                     # 0x287
	.long	537                     # 0x219
	.long	686                     # 0x2ae
	.long	600                     # 0x258
	.long	224                     # 0xe0
	.long	469                     # 0x1d5
	.long	68                      # 0x44
	.long	770                     # 0x302
	.long	919                     # 0x397
	.long	190                     # 0xbe
	.long	373                     # 0x175
	.long	294                     # 0x126
	.long	822                     # 0x336
	.long	808                     # 0x328
	.long	206                     # 0xce
	.long	184                     # 0xb8
	.long	943                     # 0x3af
	.long	795                     # 0x31b
	.long	384                     # 0x180
	.long	383                     # 0x17f
	.long	461                     # 0x1cd
	.long	404                     # 0x194
	.long	758                     # 0x2f6
	.long	839                     # 0x347
	.long	887                     # 0x377
	.long	715                     # 0x2cb
	.long	67                      # 0x43
	.long	618                     # 0x26a
	.long	276                     # 0x114
	.long	204                     # 0xcc
	.long	918                     # 0x396
	.long	873                     # 0x369
	.long	777                     # 0x309
	.long	604                     # 0x25c
	.long	560                     # 0x230
	.long	951                     # 0x3b7
	.long	160                     # 0xa0
	.long	578                     # 0x242
	.long	722                     # 0x2d2
	.long	79                      # 0x4f
	.long	804                     # 0x324
	.long	96                      # 0x60
	.long	409                     # 0x199
	.long	713                     # 0x2c9
	.long	940                     # 0x3ac
	.long	652                     # 0x28c
	.long	934                     # 0x3a6
	.long	970                     # 0x3ca
	.long	447                     # 0x1bf
	.long	318                     # 0x13e
	.long	353                     # 0x161
	.long	859                     # 0x35b
	.long	672                     # 0x2a0
	.long	112                     # 0x70
	.long	785                     # 0x311
	.long	645                     # 0x285
	.long	863                     # 0x35f
	.long	803                     # 0x323
	.long	350                     # 0x15e
	.long	139                     # 0x8b
	.long	93                      # 0x5d
	.long	354                     # 0x162
	.long	99                      # 0x63
	.long	820                     # 0x334
	.long	908                     # 0x38c
	.long	609                     # 0x261
	.long	772                     # 0x304
	.long	154                     # 0x9a
	.long	274                     # 0x112
	.long	580                     # 0x244
	.long	184                     # 0xb8
	.long	79                      # 0x4f
	.long	626                     # 0x272
	.long	630                     # 0x276
	.long	742                     # 0x2e6
	.long	653                     # 0x28d
	.long	282                     # 0x11a
	.long	762                     # 0x2fa
	.long	623                     # 0x26f
	.long	680                     # 0x2a8
	.long	81                      # 0x51
	.long	927                     # 0x39f
	.long	626                     # 0x272
	.long	789                     # 0x315
	.long	125                     # 0x7d
	.long	411                     # 0x19b
	.long	521                     # 0x209
	.long	938                     # 0x3aa
	.long	300                     # 0x12c
	.long	821                     # 0x335
	.long	78                      # 0x4e
	.long	343                     # 0x157
	.long	175                     # 0xaf
	.long	128                     # 0x80
	.long	250                     # 0xfa
	.long	170                     # 0xaa
	.long	774                     # 0x306
	.long	972                     # 0x3cc
	.long	275                     # 0x113
	.long	999                     # 0x3e7
	.long	639                     # 0x27f
	.long	495                     # 0x1ef
	.long	78                      # 0x4e
	.long	352                     # 0x160
	.long	126                     # 0x7e
	.long	857                     # 0x359
	.long	956                     # 0x3bc
	.long	358                     # 0x166
	.long	619                     # 0x26b
	.long	580                     # 0x244
	.long	124                     # 0x7c
	.long	737                     # 0x2e1
	.long	594                     # 0x252
	.long	701                     # 0x2bd
	.long	612                     # 0x264
	.long	669                     # 0x29d
	.long	112                     # 0x70
	.long	134                     # 0x86
	.long	694                     # 0x2b6
	.long	363                     # 0x16b
	.long	992                     # 0x3e0
	.long	809                     # 0x329
	.long	743                     # 0x2e7
	.long	168                     # 0xa8
	.long	974                     # 0x3ce
	.long	944                     # 0x3b0
	.long	375                     # 0x177
	.long	748                     # 0x2ec
	.long	52                      # 0x34
	.long	600                     # 0x258
	.long	747                     # 0x2eb
	.long	642                     # 0x282
	.long	182                     # 0xb6
	.long	862                     # 0x35e
	.long	81                      # 0x51
	.long	344                     # 0x158
	.long	805                     # 0x325
	.long	988                     # 0x3dc
	.long	739                     # 0x2e3
	.long	511                     # 0x1ff
	.long	655                     # 0x28f
	.long	814                     # 0x32e
	.long	334                     # 0x14e
	.long	249                     # 0xf9
	.long	515                     # 0x203
	.long	897                     # 0x381
	.long	955                     # 0x3bb
	.long	664                     # 0x298
	.long	981                     # 0x3d5
	.long	649                     # 0x289
	.long	113                     # 0x71
	.long	974                     # 0x3ce
	.long	459                     # 0x1cb
	.long	893                     # 0x37d
	.long	228                     # 0xe4
	.long	433                     # 0x1b1
	.long	837                     # 0x345
	.long	553                     # 0x229
	.long	268                     # 0x10c
	.long	926                     # 0x39e
	.long	240                     # 0xf0
	.long	102                     # 0x66
	.long	654                     # 0x28e
	.long	459                     # 0x1cb
	.long	51                      # 0x33
	.long	686                     # 0x2ae
	.long	754                     # 0x2f2
	.long	806                     # 0x326
	.long	760                     # 0x2f8
	.long	493                     # 0x1ed
	.long	403                     # 0x193
	.long	415                     # 0x19f
	.long	394                     # 0x18a
	.long	687                     # 0x2af
	.long	700                     # 0x2bc
	.long	946                     # 0x3b2
	.long	670                     # 0x29e
	.long	656                     # 0x290
	.long	610                     # 0x262
	.long	738                     # 0x2e2
	.long	392                     # 0x188
	.long	760                     # 0x2f8
	.long	799                     # 0x31f
	.long	887                     # 0x377
	.long	653                     # 0x28d
	.long	978                     # 0x3d2
	.long	321                     # 0x141
	.long	576                     # 0x240
	.long	617                     # 0x269
	.long	626                     # 0x272
	.long	502                     # 0x1f6
	.long	894                     # 0x37e
	.long	679                     # 0x2a7
	.long	243                     # 0xf3
	.long	440                     # 0x1b8
	.long	680                     # 0x2a8
	.long	879                     # 0x36f
	.long	194                     # 0xc2
	.long	572                     # 0x23c
	.long	640                     # 0x280
	.long	724                     # 0x2d4
	.long	926                     # 0x39e
	.long	56                      # 0x38
	.long	204                     # 0xcc
	.long	700                     # 0x2bc
	.long	707                     # 0x2c3
	.long	151                     # 0x97
	.long	457                     # 0x1c9
	.long	449                     # 0x1c1
	.long	797                     # 0x31d
	.long	195                     # 0xc3
	.long	791                     # 0x317
	.long	558                     # 0x22e
	.long	945                     # 0x3b1
	.long	679                     # 0x2a7
	.long	297                     # 0x129
	.long	59                      # 0x3b
	.long	87                      # 0x57
	.long	824                     # 0x338
	.long	713                     # 0x2c9
	.long	663                     # 0x297
	.long	412                     # 0x19c
	.long	693                     # 0x2b5
	.long	342                     # 0x156
	.long	606                     # 0x25e
	.long	134                     # 0x86
	.long	108                     # 0x6c
	.long	571                     # 0x23b
	.long	364                     # 0x16c
	.long	631                     # 0x277
	.long	212                     # 0xd4
	.long	174                     # 0xae
	.long	643                     # 0x283
	.long	304                     # 0x130
	.long	329                     # 0x149
	.long	343                     # 0x157
	.long	97                      # 0x61
	.long	430                     # 0x1ae
	.long	751                     # 0x2ef
	.long	497                     # 0x1f1
	.long	314                     # 0x13a
	.long	983                     # 0x3d7
	.long	374                     # 0x176
	.long	822                     # 0x336
	.long	928                     # 0x3a0
	.long	140                     # 0x8c
	.long	206                     # 0xce
	.long	73                      # 0x49
	.long	263                     # 0x107
	.long	980                     # 0x3d4
	.long	736                     # 0x2e0
	.long	876                     # 0x36c
	.long	478                     # 0x1de
	.long	430                     # 0x1ae
	.long	305                     # 0x131
	.long	170                     # 0xaa
	.long	514                     # 0x202
	.long	364                     # 0x16c
	.long	692                     # 0x2b4
	.long	829                     # 0x33d
	.long	82                      # 0x52
	.long	855                     # 0x357
	.long	953                     # 0x3b9
	.long	676                     # 0x2a4
	.long	246                     # 0xf6
	.long	369                     # 0x171
	.long	970                     # 0x3ca
	.long	294                     # 0x126
	.long	750                     # 0x2ee
	.long	807                     # 0x327
	.long	827                     # 0x33b
	.long	150                     # 0x96
	.long	790                     # 0x316
	.long	288                     # 0x120
	.long	923                     # 0x39b
	.long	804                     # 0x324
	.long	378                     # 0x17a
	.long	215                     # 0xd7
	.long	828                     # 0x33c
	.long	592                     # 0x250
	.long	281                     # 0x119
	.long	565                     # 0x235
	.long	555                     # 0x22b
	.long	710                     # 0x2c6
	.long	82                      # 0x52
	.long	896                     # 0x380
	.long	831                     # 0x33f
	.long	547                     # 0x223
	.long	261                     # 0x105
	.long	524                     # 0x20c
	.long	462                     # 0x1ce
	.long	293                     # 0x125
	.long	465                     # 0x1d1
	.long	502                     # 0x1f6
	.long	56                      # 0x38
	.long	661                     # 0x295
	.long	821                     # 0x335
	.long	976                     # 0x3d0
	.long	991                     # 0x3df
	.long	658                     # 0x292
	.long	869                     # 0x365
	.long	905                     # 0x389
	.long	758                     # 0x2f6
	.long	745                     # 0x2e9
	.long	193                     # 0xc1
	.long	768                     # 0x300
	.long	550                     # 0x226
	.long	608                     # 0x260
	.long	933                     # 0x3a5
	.long	378                     # 0x17a
	.long	286                     # 0x11e
	.long	215                     # 0xd7
	.long	979                     # 0x3d3
	.long	792                     # 0x318
	.long	961                     # 0x3c1
	.long	61                      # 0x3d
	.long	688                     # 0x2b0
	.long	793                     # 0x319
	.long	644                     # 0x284
	.long	986                     # 0x3da
	.long	403                     # 0x193
	.long	106                     # 0x6a
	.long	366                     # 0x16e
	.long	905                     # 0x389
	.long	644                     # 0x284
	.long	372                     # 0x174
	.long	567                     # 0x237
	.long	466                     # 0x1d2
	.long	434                     # 0x1b2
	.long	645                     # 0x285
	.long	210                     # 0xd2
	.long	389                     # 0x185
	.long	550                     # 0x226
	.long	919                     # 0x397
	.long	135                     # 0x87
	.long	780                     # 0x30c
	.long	773                     # 0x305
	.long	635                     # 0x27b
	.long	389                     # 0x185
	.long	707                     # 0x2c3
	.long	100                     # 0x64
	.long	626                     # 0x272
	.long	958                     # 0x3be
	.long	165                     # 0xa5
	.long	504                     # 0x1f8
	.long	920                     # 0x398
	.long	176                     # 0xb0
	.long	193                     # 0xc1
	.long	713                     # 0x2c9
	.long	857                     # 0x359
	.long	265                     # 0x109
	.long	203                     # 0xcb
	.long	50                      # 0x32
	.long	668                     # 0x29c
	.long	108                     # 0x6c
	.long	645                     # 0x285
	.long	990                     # 0x3de
	.long	626                     # 0x272
	.long	197                     # 0xc5
	.long	510                     # 0x1fe
	.long	357                     # 0x165
	.long	358                     # 0x166
	.long	850                     # 0x352
	.long	858                     # 0x35a
	.long	364                     # 0x16c
	.long	936                     # 0x3a8
	.long	638                     # 0x27e
	.size	BZ2_rNums, 2048


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
