	.text
	.file	"fldry.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$.L.str, %edi
	movl	$8, %esi
	xorl	%eax, %eax
	callq	printf
	callq	Proc0
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.quad	4619567317775286272     # double 7
	.quad	4619567317775286272     # double 7
.LCPI1_1:
	.quad	4620693217682128896     # double 8
	.quad	4620693217682128896     # double 8
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_2:
	.quad	4607182418800017408     # double 1
	.text
	.globl	Proc0
	.p2align	4, 0x90
	.type	Proc0,@function
Proc0:                                  # @Proc0
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 16
.Lcfi2:
	.cfi_offset %rbx, -16
	callq	clock
	callq	clock
	movl	$56, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, PtrGlbNext(%rip)
	movl	$56, %edi
	callq	malloc
	movq	%rax, PtrGlb(%rip)
	movq	%rbx, (%rax)
	movl	$0, 8(%rax)
	movl	$10001, 12(%rax)        # imm = 0x2711
	movabsq	$4630826316843712512, %rcx # imm = 0x4044000000000000
	movq	%rcx, 16(%rax)
	movups	.L.str.1+15(%rip), %xmm0
	movups	%xmm0, 39(%rax)
	movups	.L.str.1(%rip), %xmm0
	movups	%xmm0, 24(%rax)
	movabsq	$4621819117588971520, %rax # imm = 0x4024000000000000
	movq	%rax, Array2Glob+3320(%rip)
	callq	clock
	xorl	%eax, %eax
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [7.000000e+00,7.000000e+00]
	movabsq	$4620693217682128896, %rdi # imm = 0x4020000000000000
	movaps	.LCPI1_1(%rip), %xmm1   # xmm1 = [8.000000e+00,8.000000e+00]
	movsd	.LCPI1_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movabsq	$4619567317775286272, %rsi # imm = 0x401C000000000000
	movabsq	$4617315517961601024, %rdx # imm = 0x4014000000000000
	.p2align	4, 0x90
.LBB1_1:                                # %Proc8.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_3 Depth 2
	movb	$65, Char1Glob(%rip)
	movb	$66, Char2Glob(%rip)
	movaps	%xmm0, Array1Glob+64(%rip)
	movq	%rdi, Array1Glob+304(%rip)
	movaps	%xmm1, Array2Glob+3328(%rip)
	movsd	Array2Glob+3320(%rip), %xmm3 # xmm3 = mem[0],zero
	addsd	%xmm2, %xmm3
	movsd	%xmm3, Array2Glob+3320(%rip)
	movq	%rsi, Array2Glob+11488(%rip)
	movq	PtrGlb(%rip), %rcx
	movq	%rdx, 16(%rcx)
	movq	(%rcx), %rcx
	movq	%rdx, 16(%rcx)
	movq	%rcx, (%rcx)
	movb	Char2Glob(%rip), %bl
	cmpb	$65, %bl
	jl	.LBB1_4
# BB#2:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_1 Depth=1
	movb	$65, %cl
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incb	%cl
	cmpb	%bl, %cl
	jle	.LBB1_3
.LBB1_4:                                # %Proc2.exit
                                        #   in Loop: Header=BB1_1 Depth=1
	incl	%eax
	cmpl	$100000000, %eax        # imm = 0x5F5E100
	jne	.LBB1_1
# BB#5:
	movl	$1, BoolGlob(%rip)
	movq	%rdx, IntGlob(%rip)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	Proc0, .Lfunc_end1-Proc0
	.cfi_endproc

	.globl	Proc1
	.p2align	4, 0x90
	.type	Proc1,@function
Proc1:                                  # @Proc1
	.cfi_startproc
# BB#0:
	movabsq	$4617315517961601024, %rax # imm = 0x4014000000000000
	movq	%rax, 16(%rdi)
	movq	(%rdi), %rcx
	movq	%rax, 16(%rcx)
	movq	%rcx, (%rcx)
	retq
.Lfunc_end2:
	.size	Proc1, .Lfunc_end2-Proc1
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4621819117588971520     # double 10
.LCPI3_1:
	.quad	-4616189618054758400    # double -1
	.text
	.globl	Proc2
	.p2align	4, 0x90
	.type	Proc2,@function
Proc2:                                  # @Proc2
	.cfi_startproc
# BB#0:
	cmpb	$65, Char1Glob(%rip)
	jne	.LBB3_2
# BB#1:
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	addsd	.LCPI3_0(%rip), %xmm0
	addsd	.LCPI3_1(%rip), %xmm0
	subsd	IntGlob(%rip), %xmm0
	movsd	%xmm0, (%rdi)
.LBB3_2:
	retq
.Lfunc_end3:
	.size	Proc2, .Lfunc_end3-Proc2
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4636737291354636288     # double 100
	.text
	.globl	Proc3
	.p2align	4, 0x90
	.type	Proc3,@function
Proc3:                                  # @Proc3
	.cfi_startproc
# BB#0:
	movq	PtrGlb(%rip), %rax
	testq	%rax, %rax
	je	.LBB4_2
# BB#1:
	movq	(%rax), %rax
	movq	%rax, (%rdi)
	movsd	IntGlob(%rip), %xmm0    # xmm0 = mem[0],zero
	movq	PtrGlb(%rip), %rsi
	jmp	.LBB4_3
.LBB4_2:
	movabsq	$4636737291354636288, %rax # imm = 0x4059000000000000
	movq	%rax, IntGlob(%rip)
	movsd	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero
	xorl	%esi, %esi
.LBB4_3:
	addq	$16, %rsi
	movl	$10, %edi
	movb	$1, %al
	jmp	Proc7                   # TAILCALL
.Lfunc_end4:
	.size	Proc3, .Lfunc_end4-Proc3
	.cfi_endproc

	.globl	Proc4
	.p2align	4, 0x90
	.type	Proc4,@function
Proc4:                                  # @Proc4
	.cfi_startproc
# BB#0:
	movb	$66, Char2Glob(%rip)
	retq
.Lfunc_end5:
	.size	Proc4, .Lfunc_end5-Proc4
	.cfi_endproc

	.globl	Proc5
	.p2align	4, 0x90
	.type	Proc5,@function
Proc5:                                  # @Proc5
	.cfi_startproc
# BB#0:
	movb	$65, Char1Glob(%rip)
	movl	$0, BoolGlob(%rip)
	retq
.Lfunc_end6:
	.size	Proc5, .Lfunc_end6-Proc5
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI7_0:
	.quad	4636737291354636288     # double 100
	.text
	.globl	Proc6
	.p2align	4, 0x90
	.type	Proc6,@function
Proc6:                                  # @Proc6
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	$10001, %edi            # imm = 0x2711
	setne	%al
	addl	$10001, %eax            # imm = 0x2711
	movl	%eax, (%rsi)
	cmpl	$10000, %edi            # imm = 0x2710
	jg	.LBB7_4
# BB#1:
	testl	%edi, %edi
	je	.LBB7_6
# BB#2:
	cmpl	$10000, %edi            # imm = 0x2710
	jne	.LBB7_10
# BB#3:
	movsd	IntGlob(%rip), %xmm0    # xmm0 = mem[0],zero
	xorl	%ecx, %ecx
	ucomisd	.LCPI7_0(%rip), %xmm0
	movl	$10002, %eax            # imm = 0x2712
	cmoval	%ecx, %eax
	jmp	.LBB7_9
.LBB7_4:
	cmpl	$10001, %edi            # imm = 0x2711
	je	.LBB7_5
# BB#7:
	cmpl	$10003, %edi            # imm = 0x2713
	jne	.LBB7_10
# BB#8:
	movl	$10001, %eax            # imm = 0x2711
	jmp	.LBB7_9
.LBB7_6:
	xorl	%eax, %eax
	jmp	.LBB7_9
.LBB7_5:
	movl	$10000, %eax            # imm = 0x2710
.LBB7_9:                                # %.sink.split
	movl	%eax, (%rsi)
.LBB7_10:
	retq
.Lfunc_end7:
	.size	Proc6, .Lfunc_end7-Proc6
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI8_0:
	.quad	4611686018427387904     # double 2
	.text
	.globl	Proc7
	.p2align	4, 0x90
	.type	Proc7,@function
Proc7:                                  # @Proc7
	.cfi_startproc
# BB#0:
	addsd	.LCPI8_0(%rip), %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%rdi)
	retq
.Lfunc_end8:
	.size	Proc7, .Lfunc_end8-Proc7
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI9_0:
	.quad	4617315517961601024     # double 5
.LCPI9_1:
	.quad	4607182418800017408     # double 1
	.text
	.globl	Proc8
	.p2align	4, 0x90
	.type	Proc8,@function
Proc8:                                  # @Proc8
	.cfi_startproc
# BB#0:
	addsd	.LCPI9_0(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	cltq
	movsd	%xmm1, (%rdi,%rax,8)
	movsd	%xmm1, 8(%rdi,%rax,8)
	movsd	%xmm0, 240(%rdi,%rax,8)
	movsd	.LCPI9_1(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	ucomisd	%xmm0, %xmm1
	jb	.LBB9_3
# BB#1:                                 # %.lr.ph.preheader
	imulq	$408, %rax, %rcx        # imm = 0x198
	addq	%rsi, %rcx
	movsd	.LCPI9_1(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm3
	.p2align	4, 0x90
.LBB9_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cvttsd2si	%xmm3, %edx
	movslq	%edx, %rdx
	movsd	%xmm0, (%rcx,%rdx,8)
	addsd	%xmm2, %xmm3
	ucomisd	%xmm3, %xmm1
	jae	.LBB9_2
.LBB9_3:                                # %._crit_edge
	imulq	$408, %rax, %rcx        # imm = 0x198
	addq	%rsi, %rcx
	movsd	-8(%rcx,%rax,8), %xmm0  # xmm0 = mem[0],zero
	addsd	.LCPI9_1(%rip), %xmm0
	movsd	%xmm0, -8(%rcx,%rax,8)
	movq	(%rdi,%rax,8), %rcx
	leal	20(%rax), %edx
	movslq	%edx, %rdx
	imulq	$408, %rdx, %rdx        # imm = 0x198
	addq	%rsi, %rdx
	movq	%rcx, (%rdx,%rax,8)
	movabsq	$4617315517961601024, %rax # imm = 0x4014000000000000
	movq	%rax, IntGlob(%rip)
	retq
.Lfunc_end9:
	.size	Proc8, .Lfunc_end9-Proc8
	.cfi_endproc

	.globl	Func1
	.p2align	4, 0x90
	.type	Func1,@function
Func1:                                  # @Func1
	.cfi_startproc
# BB#0:
	xorl	%ecx, %ecx
	xorb	%dil, %sil
	movl	$10000, %eax            # imm = 0x2710
	cmovnel	%ecx, %eax
	retq
.Lfunc_end10:
	.size	Func1, .Lfunc_end10-Func1
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI11_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	Func2
	.p2align	4, 0x90
	.type	Func2,@function
Func2:                                  # @Func2
	.cfi_startproc
# BB#0:
	movsd	.LCPI11_0(%rip), %xmm0  # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	.p2align	4, 0x90
.LBB11_1:                               # =>This Inner Loop Header: Depth=1
	cvttsd2si	%xmm1, %eax
	cltq
	movzbl	1(%rsi,%rax), %ecx
	cmpb	(%rdi,%rax), %cl
	je	.LBB11_3
# BB#2:                                 #   in Loop: Header=BB11_1 Depth=1
	addsd	%xmm0, %xmm1
.LBB11_3:                               #   in Loop: Header=BB11_1 Depth=1
	ucomisd	%xmm1, %xmm0
	jae	.LBB11_1
# BB#4:
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 16
	callq	strcmp
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setg	%cl
	movl	%ecx, %eax
	popq	%rcx
	retq
.Lfunc_end11:
	.size	Func2, .Lfunc_end11-Func2
	.cfi_endproc

	.globl	Func3
	.p2align	4, 0x90
	.type	Func3,@function
Func3:                                  # @Func3
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	$10001, %edi            # imm = 0x2711
	sete	%al
	retq
.Lfunc_end12:
	.size	Func3, .Lfunc_end12-Func3
	.cfi_endproc

	.type	Version,@object         # @Version
	.data
	.globl	Version
Version:
	.asciz	"1.1"
	.size	Version, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"calculate floating dhrystones using doubles size %d\n"
	.size	.L.str, 53

	.type	PtrGlbNext,@object      # @PtrGlbNext
	.comm	PtrGlbNext,8,8
	.type	PtrGlb,@object          # @PtrGlb
	.comm	PtrGlb,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"DHRYSTONE PROGRAM, SOME STRING"
	.size	.L.str.1, 31

	.type	Array2Glob,@object      # @Array2Glob
	.comm	Array2Glob,20808,16
	.type	BoolGlob,@object        # @BoolGlob
	.comm	BoolGlob,4,4
	.type	Array1Glob,@object      # @Array1Glob
	.comm	Array1Glob,408,16
	.type	Char2Glob,@object       # @Char2Glob
	.comm	Char2Glob,1,1
	.type	Char1Glob,@object       # @Char1Glob
	.comm	Char1Glob,1,1
	.type	IntGlob,@object         # @IntGlob
	.comm	IntGlob,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
