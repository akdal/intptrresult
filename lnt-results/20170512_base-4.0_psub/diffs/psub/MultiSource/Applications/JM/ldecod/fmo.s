	.text
	.file	"fmo.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.text
	.globl	FmoInit
	.p2align	4, 0x90
	.type	FmoInit,@function
FmoInit:                                # @FmoInit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r13
	movl	2068(%rbp), %r12d
	movl	2072(%rbp), %eax
	incl	%eax
	incl	%r12d
	imull	%eax, %r12d
	cmpl	$6, 992(%r13)
	jne	.LBB0_3
# BB#1:
	movl	1100(%r13), %eax
	incl	%eax
	cmpl	%r12d, %eax
	je	.LBB0_3
# BB#2:
	movl	$.L.str, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB0_3:
	movq	MapUnitToSliceGroupMap(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_5
# BB#4:
	callq	free
.LBB0_5:
	movl	%r12d, %ebx
	leaq	(,%rbx,4), %r15
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, %r10
	movq	%r10, MapUnitToSliceGroupMap(%rip)
	testq	%r10, %r10
	je	.LBB0_116
# BB#6:
	movl	988(%r13), %r8d
	testl	%r8d, %r8d
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	je	.LBB0_19
# BB#7:
	movl	992(%r13), %esi
	cmpq	$6, %rsi
	ja	.LBB0_119
# BB#8:
	jmpq	*.LJTI0_0(,%rsi,8)
.LBB0_9:                                # %.preheader.i.preheader
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_10:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_12 Depth 2
                                        #       Child Loop BB0_13 Depth 3
	cmpl	%r12d, %edx
	jae	.LBB0_20
# BB#11:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB0_10 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_12:                               # %.preheader.i.i
                                        #   Parent Loop BB0_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_13 Depth 3
	movl	%eax, %esi
	movl	%edx, %edx
	leaq	(%r10,%rdx,4), %rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_13:                               #   Parent Loop BB0_10 Depth=1
                                        #     Parent Loop BB0_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%rdx,%rcx), %rdi
	cmpq	%rbx, %rdi
	jae	.LBB0_15
# BB#14:                                #   in Loop: Header=BB0_13 Depth=3
	movl	%eax, (%rbp,%rcx,4)
	movl	996(%r13,%rsi,4), %edi
	incq	%rcx
	cmpl	%edi, %ecx
	jbe	.LBB0_13
	jmp	.LBB0_16
	.p2align	4, 0x90
.LBB0_15:                               # %..critedge1_crit_edge.i.i
                                        #   in Loop: Header=BB0_12 Depth=2
	movl	996(%r13,%rsi,4), %edi
.LBB0_16:                               # %.critedge1.i.i
                                        #   in Loop: Header=BB0_12 Depth=2
	incl	%eax
	leal	1(%rdx,%rdi), %edx
	cmpl	%r8d, %eax
	ja	.LBB0_18
# BB#17:                                # %.critedge1.i.i
                                        #   in Loop: Header=BB0_12 Depth=2
	cmpl	%r12d, %edx
	jb	.LBB0_12
.LBB0_18:                               # %.critedge.i.i
                                        #   in Loop: Header=BB0_10 Depth=1
	cmpl	%r12d, %edx
	jb	.LBB0_10
	jmp	.LBB0_20
.LBB0_19:
	xorl	%esi, %esi
	movq	%r10, %rdi
	movq	%r15, %rdx
	callq	memset
.LBB0_20:                               # %FmoGenerateMapUnitToSliceGroupMap.exit
	movq	MbToSliceGroupMap(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_22
# BB#21:
	callq	free
.LBB0_22:
	movq	img(%rip), %rbx
	movl	5836(%rbx), %edi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rcx
	movq	%rcx, MbToSliceGroupMap(%rip)
	testq	%rcx, %rcx
	movq	32(%rsp), %rax          # 8-byte Reload
	je	.LBB0_117
# BB#23:
	cmpl	$0, 2076(%rax)
	jne	.LBB0_25
# BB#24:
	cmpl	$0, 5680(%rbx)
	je	.LBB0_29
.LBB0_25:                               # %.preheader3.i
	cmpl	$0, 5836(%rbx)
	je	.LBB0_28
# BB#26:                                # %.lr.ph9.i
	xorl	%eax, %eax
	movq	MapUnitToSliceGroupMap(%rip), %rdx
	.p2align	4, 0x90
.LBB0_27:                               # =>This Inner Loop Header: Depth=1
	movl	%eax, %esi
	movl	(%rdx,%rsi,4), %edi
	movl	%edi, (%rcx,%rsi,4)
	incl	%eax
	cmpl	5836(%rbx), %eax
	jb	.LBB0_27
.LBB0_28:                               # %FmoGenerateMbToSliceGroupMap.exit
	movl	988(%r13), %eax
	incl	%eax
	movl	%eax, NumberOfSliceGroups(%rip)
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_29:
	cmpl	$0, 2080(%rax)
	movl	5836(%rbx), %eax
	je	.LBB0_61
# BB#30:                                # %.preheader1.i
	testl	%eax, %eax
	je	.LBB0_28
# BB#31:                                # %.lr.ph7.i
	xorl	%eax, %eax
	movq	MapUnitToSliceGroupMap(%rip), %rdx
	.p2align	4, 0x90
.LBB0_32:                               # =>This Inner Loop Header: Depth=1
	movl	%eax, %esi
	shrl	%esi
	movl	(%rdx,%rsi,4), %esi
	movl	%eax, %edi
	movl	%esi, (%rcx,%rdi,4)
	incl	%eax
	cmpl	5836(%rbx), %eax
	jb	.LBB0_32
	jmp	.LBB0_28
.LBB0_33:
	testl	%r12d, %r12d
	je	.LBB0_20
# BB#34:                                # %.lr.ph.i34.i
	movq	img(%rip), %rbp
	incl	%r8d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_35:                               # %._crit_edge.i
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edx, %edx
	movl	%esi, %eax
	divl	5820(%rbp)
	imull	%r8d, %eax
	shrl	%eax
	addl	%edx, %eax
	xorl	%edx, %edx
	divl	%r8d
	movl	%edx, (%r10,%rsi,4)
	incq	%rsi
	cmpq	%rsi, %rbx
	jne	.LBB0_35
	jmp	.LBB0_20
.LBB0_36:
	testl	%r12d, %r12d
	je	.LBB0_67
# BB#37:                                # %.lr.ph13.i.i.preheader
	cmpl	$7, %r12d
	jbe	.LBB0_64
# BB#38:                                # %min.iters.checked84
	andl	$7, %r12d
	movq	%rbx, %rax
	subq	%r12, %rax
	je	.LBB0_64
# BB#39:                                # %vector.ph88
	movd	%r8d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r10), %rcx
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB0_40:                               # %vector.body80
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rcx)
	movdqu	%xmm0, (%rcx)
	addq	$32, %rcx
	addq	$-8, %rdx
	jne	.LBB0_40
# BB#41:                                # %middle.block81
	testl	%r12d, %r12d
	jne	.LBB0_65
	jmp	.LBB0_67
.LBB0_42:
	movl	1096(%r13), %eax
	incl	%eax
	movq	img(%rip), %rdx
	imull	5648(%rdx), %eax
	cmpl	%r12d, %eax
	cmovgl	%r12d, %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	testl	%r12d, %r12d
	je	.LBB0_87
# BB#43:                                # %.lr.ph15.i.i.preheader
	cmpl	$8, %r12d
	jb	.LBB0_84
# BB#76:                                # %min.iters.checked67
	movl	%r12d, %esi
	andl	$7, %esi
	movq	%rbx, %rax
	subq	%rsi, %rax
	je	.LBB0_84
# BB#77:                                # %vector.body63.preheader
	leaq	16(%r10), %rcx
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [2,2,2,2]
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB0_78:                               # %vector.body63
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rcx)
	movdqu	%xmm0, (%rcx)
	addq	$32, %rcx
	addq	$-8, %rdi
	jne	.LBB0_78
# BB#79:                                # %middle.block64
	testl	%esi, %esi
	jne	.LBB0_85
	jmp	.LBB0_87
.LBB0_45:
	movl	1096(%r13), %edx
	incl	%edx
	movq	img(%rip), %rax
	imull	5648(%rax), %edx
	cmpl	%r12d, %edx
	cmovgl	%r12d, %edx
	testl	%r12d, %r12d
	je	.LBB0_20
# BB#46:                                # %.lr.ph.i11.i
	movl	1092(%r13), %eax
	subl	%edx, %r12d
	testl	%eax, %eax
	cmovel	%edx, %r12d
	movl	$1, %edx
	subl	%eax, %edx
	leaq	-1(%rbx), %rdi
	movq	%rbx, %rbp
	xorl	%esi, %esi
	andq	$3, %rbp
	je	.LBB0_48
	.p2align	4, 0x90
.LBB0_47:                               # =>This Inner Loop Header: Depth=1
	cmpq	%r12, %rsi
	movl	%edx, %ecx
	cmovbl	%eax, %ecx
	movl	%ecx, (%r10,%rsi,4)
	incq	%rsi
	cmpq	%rsi, %rbp
	jne	.LBB0_47
.LBB0_48:                               # %.prol.loopexit
	cmpq	$3, %rdi
	jb	.LBB0_20
	.p2align	4, 0x90
.LBB0_49:                               # =>This Inner Loop Header: Depth=1
	cmpq	%r12, %rsi
	movl	%edx, %ecx
	cmovbl	%eax, %ecx
	movl	%ecx, (%r10,%rsi,4)
	leaq	1(%rsi), %rcx
	cmpq	%r12, %rcx
	movl	%edx, %ecx
	cmovbl	%eax, %ecx
	movl	%ecx, 4(%r10,%rsi,4)
	leaq	2(%rsi), %rcx
	cmpq	%r12, %rcx
	movl	%edx, %ecx
	cmovbl	%eax, %ecx
	movl	%ecx, 8(%r10,%rsi,4)
	leaq	3(%rsi), %rcx
	cmpq	%r12, %rcx
	movl	%edx, %ecx
	cmovbl	%eax, %ecx
	movl	%ecx, 12(%r10,%rsi,4)
	addq	$4, %rsi
	cmpq	%rbx, %rsi
	jne	.LBB0_49
	jmp	.LBB0_20
.LBB0_50:
	movl	1092(%r13), %r8d
	movl	1096(%r13), %eax
	incl	%eax
	movq	img(%rip), %rdx
	imull	5648(%rdx), %eax
	cmpl	%r12d, %eax
	cmovgl	%r12d, %eax
	subl	%eax, %r12d
	testl	%r8d, %r8d
	cmovel	%eax, %r12d
	movl	5820(%rdx), %esi
	testl	%esi, %esi
	je	.LBB0_20
# BB#51:                                # %.preheader.lr.ph.i.i
	movl	$1, %r9d
	subl	%r8d, %r9d
	movl	5824(%rdx), %eax
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_52:                               # %.preheader.i7.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_54 Depth 2
	testl	%eax, %eax
	je	.LBB0_56
# BB#53:                                # %.lr.ph.i8.i.preheader
                                        #   in Loop: Header=BB0_52 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_54:                               # %.lr.ph.i8.i
                                        #   Parent Loop BB0_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rbp,%rdi), %eax
	cmpl	%r12d, %eax
	movl	%r9d, %eax
	cmovbl	%r8d, %eax
	imull	%edi, %esi
	addl	%ebx, %esi
	movl	%eax, (%r10,%rsi,4)
	incl	%edi
	movl	5820(%rdx), %esi
	movl	5824(%rdx), %eax
	cmpl	%eax, %edi
	jb	.LBB0_54
# BB#55:                                # %._crit_edge.i.i.loopexit
                                        #   in Loop: Header=BB0_52 Depth=1
	addl	%edi, %ebp
	jmp	.LBB0_57
	.p2align	4, 0x90
.LBB0_56:                               #   in Loop: Header=BB0_52 Depth=1
	xorl	%eax, %eax
.LBB0_57:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB0_52 Depth=1
	incl	%ebx
	cmpl	%esi, %ebx
	jb	.LBB0_52
	jmp	.LBB0_20
.LBB0_58:
	testl	%r12d, %r12d
	je	.LBB0_20
# BB#59:                                # %.lr.ph.i.i
	movq	1104(%r13), %rax
	cmpl	$8, %r12d
	jae	.LBB0_80
# BB#60:
	xorl	%edx, %edx
	jmp	.LBB0_110
.LBB0_61:                               # %.preheader.i6
	testl	%eax, %eax
	je	.LBB0_28
# BB#62:                                # %.lr.ph.i
	xorl	%esi, %esi
	movq	MapUnitToSliceGroupMap(%rip), %r8
	.p2align	4, 0x90
.LBB0_63:                               # =>This Inner Loop Header: Depth=1
	movl	5820(%rbx), %ebp
	leal	(%rbp,%rbp), %edi
	xorl	%edx, %edx
	movl	%esi, %eax
	divl	%edi
	movl	%eax, %edi
	imull	%ebp, %edi
	xorl	%edx, %edx
	movl	%esi, %eax
	divl	%ebp
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	addl	%edi, %edx
	movl	(%r8,%rdx,4), %eax
	movl	%esi, %edx
	movl	%eax, (%rcx,%rdx,4)
	incl	%esi
	cmpl	5836(%rbx), %esi
	jb	.LBB0_63
	jmp	.LBB0_28
.LBB0_64:
	xorl	%eax, %eax
.LBB0_65:                               # %.lr.ph13.i.i.preheader104
	subq	%rax, %rbx
	leaq	(%r10,%rax,4), %rax
	.p2align	4, 0x90
.LBB0_66:                               # %.lr.ph13.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%r8d, (%rax)
	addq	$4, %rax
	decq	%rbx
	jne	.LBB0_66
.LBB0_67:                               # %.preheader1.i.i
	decl	%r8d
	js	.LBB0_20
# BB#68:                                # %.lr.ph9.i.i
	movq	img(%rip), %rbp
	movslq	%r8d, %rbx
	.p2align	4, 0x90
.LBB0_69:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_72 Depth 2
                                        #       Child Loop BB0_73 Depth 3
                                        #     Child Loop BB0_71 Depth 2
	movl	1028(%r13,%rbx,4), %eax
	movl	5820(%rbp), %ecx
	xorl	%edx, %edx
	divl	%ecx
	movl	%eax, %esi
	movl	%edx, %r8d
	movl	1060(%r13,%rbx,4), %eax
	xorl	%edx, %edx
	divl	%ecx
	cmpl	%eax, %esi
	ja	.LBB0_75
# BB#70:                                # %.preheader.lr.ph.i30.i
                                        #   in Loop: Header=BB0_69 Depth=1
	cmpl	%edx, %r8d
	jbe	.LBB0_72
	.p2align	4, 0x90
.LBB0_71:                               # %.preheader.us.i.i
                                        #   Parent Loop BB0_69 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%esi
	cmpl	%eax, %esi
	jbe	.LBB0_71
	jmp	.LBB0_75
	.p2align	4, 0x90
.LBB0_72:                               # %.preheader.i31.i
                                        #   Parent Loop BB0_69 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_73 Depth 3
	movl	%r8d, %ecx
	.p2align	4, 0x90
.LBB0_73:                               #   Parent Loop BB0_69 Depth=1
                                        #     Parent Loop BB0_72 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	5820(%rbp), %edi
	imull	%esi, %edi
	addl	%ecx, %edi
	movl	%ebx, (%r10,%rdi,4)
	incl	%ecx
	cmpl	%edx, %ecx
	jbe	.LBB0_73
# BB#74:                                # %._crit_edge.i32.i
                                        #   in Loop: Header=BB0_72 Depth=2
	incl	%esi
	cmpl	%eax, %esi
	jbe	.LBB0_72
.LBB0_75:                               # %.loopexit.i.i
                                        #   in Loop: Header=BB0_69 Depth=1
	decq	%rbx
	testl	%ebx, %ebx
	jns	.LBB0_69
	jmp	.LBB0_20
.LBB0_84:
	xorl	%eax, %eax
.LBB0_85:                               # %.lr.ph15.i.i.preheader106
	subq	%rax, %rbx
	leaq	(%r10,%rax,4), %rax
	.p2align	4, 0x90
.LBB0_86:                               # %.lr.ph15.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$2, (%rax)
	addq	$4, %rax
	decq	%rbx
	jne	.LBB0_86
.LBB0_87:                               # %._crit_edge16.i.i
	testl	%r12d, %r12d
	je	.LBB0_20
# BB#88:                                # %.lr.ph.i21.i
	movl	1092(%r13), %r15d
	movl	5820(%rdx), %r9d
	movl	5824(%rdx), %esi
	movl	%r9d, %edi
	subl	%r15d, %edi
	shrl	%edi
	subl	%r15d, %esi
	shrl	%esi
	leal	-1(%r15), %r8d
	leal	(%r15,%r15), %eax
	leal	-1(%r15,%r15), %ecx
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	movl	$1, %ecx
	subl	%eax, %ecx
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	xorl	%ebx, %ebx
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	%edi, %eax
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movl	%edi, %ebp
	jmp	.LBB0_90
	.p2align	4, 0x90
.LBB0_89:                               # %._crit_edge
                                        #   in Loop: Header=BB0_90 Depth=1
	movl	5820(%rdx), %r9d
.LBB0_90:                               # =>This Inner Loop Header: Depth=1
	imull	%esi, %r9d
	addl	%edi, %r9d
	xorl	%r14d, %r14d
	cmpl	$2, (%r10,%r9,4)
	sete	%cl
	jne	.LBB0_92
# BB#91:                                #   in Loop: Header=BB0_90 Depth=1
	xorl	%r11d, %r11d
	cmpl	28(%rsp), %ebx          # 4-byte Folded Reload
	setae	%r11b
	movl	%r11d, (%r10,%r9,4)
.LBB0_92:                               #   in Loop: Header=BB0_90 Depth=1
	movb	%cl, %r14b
	cmpl	$-1, %r8d
	jne	.LBB0_95
# BB#93:                                #   in Loop: Header=BB0_90 Depth=1
	cmpl	%ebp, %edi
	jne	.LBB0_95
# BB#94:                                #   in Loop: Header=BB0_90 Depth=1
	xorl	%r8d, %r8d
	decl	%edi
	cmovsl	%r8d, %edi
	movl	%edi, %ebp
	movl	24(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %r15d
	jmp	.LBB0_105
	.p2align	4, 0x90
.LBB0_95:                               #   in Loop: Header=BB0_90 Depth=1
	cmpl	$1, %r8d
	jne	.LBB0_98
# BB#96:                                #   in Loop: Header=BB0_90 Depth=1
	cmpl	%eax, %edi
	jne	.LBB0_98
# BB#97:                                #   in Loop: Header=BB0_90 Depth=1
	incl	%edi
	movl	5820(%rdx), %eax
	decl	%eax
	cmpl	%eax, %edi
	cmovlel	%edi, %eax
	xorl	%r8d, %r8d
	movl	%eax, %edi
	movl	20(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %r15d
	jmp	.LBB0_105
	.p2align	4, 0x90
.LBB0_98:                               #   in Loop: Header=BB0_90 Depth=1
	cmpl	$-1, %r15d
	jne	.LBB0_101
# BB#99:                                #   in Loop: Header=BB0_90 Depth=1
	cmpl	16(%rsp), %esi          # 4-byte Folded Reload
	jne	.LBB0_101
# BB#100:                               #   in Loop: Header=BB0_90 Depth=1
	xorl	%r15d, %r15d
	decl	%esi
	cmovsl	%r15d, %esi
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movl	20(%rsp), %r8d          # 4-byte Reload
	jmp	.LBB0_105
.LBB0_101:                              #   in Loop: Header=BB0_90 Depth=1
	cmpl	$1, %r15d
	jne	.LBB0_104
# BB#102:                               #   in Loop: Header=BB0_90 Depth=1
	cmpl	12(%rsp), %esi          # 4-byte Folded Reload
	jne	.LBB0_104
# BB#103:                               #   in Loop: Header=BB0_90 Depth=1
	incl	%esi
	movl	5824(%rdx), %ecx
	decl	%ecx
	cmpl	%ecx, %esi
	cmovlel	%esi, %ecx
	xorl	%r15d, %r15d
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	%ecx, %esi
	movl	24(%rsp), %r8d          # 4-byte Reload
	jmp	.LBB0_105
.LBB0_104:                              #   in Loop: Header=BB0_90 Depth=1
	addl	%r8d, %edi
	addl	%r15d, %esi
	.p2align	4, 0x90
.LBB0_105:                              #   in Loop: Header=BB0_90 Depth=1
	addl	%r14d, %ebx
	cmpl	%r12d, %ebx
	jb	.LBB0_89
	jmp	.LBB0_20
.LBB0_80:                               # %min.iters.checked
	andl	$7, %r12d
	movq	%rbx, %rdx
	subq	%r12, %rdx
	je	.LBB0_106
# BB#81:                                # %vector.memcheck
	leaq	(%rax,%rbx,4), %rcx
	cmpq	%rcx, %r10
	jae	.LBB0_107
# BB#82:                                # %vector.memcheck
	leaq	(%r10,%rbx,4), %rcx
	cmpq	%rcx, %rax
	jae	.LBB0_107
# BB#83:
	xorl	%edx, %edx
	jmp	.LBB0_110
.LBB0_106:
	xorl	%edx, %edx
	jmp	.LBB0_110
.LBB0_107:                              # %vector.body.preheader
	leaq	16(%rax), %rsi
	leaq	16(%r10), %rdi
	movq	%rdx, %rbp
	.p2align	4, 0x90
.LBB0_108:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movdqu	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-8, %rbp
	jne	.LBB0_108
# BB#109:                               # %middle.block
	testl	%r12d, %r12d
	je	.LBB0_20
.LBB0_110:                              # %scalar.ph.preheader
	movl	%ebx, %edi
	subl	%edx, %edi
	leaq	-1(%rbx), %rsi
	subq	%rdx, %rsi
	andq	$7, %rdi
	je	.LBB0_113
# BB#111:                               # %scalar.ph.prol.preheader
	negq	%rdi
	.p2align	4, 0x90
.LBB0_112:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rdx,4), %ecx
	movl	%ecx, (%r10,%rdx,4)
	incq	%rdx
	incq	%rdi
	jne	.LBB0_112
.LBB0_113:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB0_20
# BB#114:                               # %scalar.ph.preheader.new
	subq	%rdx, %rbx
	leaq	28(%r10,%rdx,4), %rcx
	leaq	28(%rax,%rdx,4), %rax
	.p2align	4, 0x90
.LBB0_115:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rax), %edx
	movl	%edx, -28(%rcx)
	movl	-24(%rax), %edx
	movl	%edx, -24(%rcx)
	movl	-20(%rax), %edx
	movl	%edx, -20(%rcx)
	movl	-16(%rax), %edx
	movl	%edx, -16(%rcx)
	movl	-12(%rax), %edx
	movl	%edx, -12(%rcx)
	movl	-8(%rax), %edx
	movl	%edx, -8(%rcx)
	movl	-4(%rax), %edx
	movl	%edx, -4(%rcx)
	movl	(%rax), %edx
	movl	%edx, (%rcx)
	addq	$32, %rcx
	addq	$32, %rax
	addq	$-8, %rbx
	jne	.LBB0_115
	jmp	.LBB0_20
.LBB0_116:
	movl	1100(%r13), %eax
	leal	4(,%rax,4), %esi
	movl	$.L.str.1, %edi
	jmp	.LBB0_118
.LBB0_117:
	movl	5836(%rbx), %esi
	shll	$2, %esi
	movl	$.L.str.3, %edi
.LBB0_118:
	xorl	%eax, %eax
	jmp	.LBB0_120
.LBB0_119:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
.LBB0_120:
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end0:
	.size	FmoInit, .Lfunc_end0-FmoInit
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_9
	.quad	.LBB0_33
	.quad	.LBB0_36
	.quad	.LBB0_42
	.quad	.LBB0_45
	.quad	.LBB0_50
	.quad	.LBB0_58

	.text
	.globl	FmoFinit
	.p2align	4, 0x90
	.type	FmoFinit,@function
FmoFinit:                               # @FmoFinit
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movq	MbToSliceGroupMap(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	callq	free
	movq	$0, MbToSliceGroupMap(%rip)
.LBB1_2:
	movq	MapUnitToSliceGroupMap(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#3:
	callq	free
	movq	$0, MapUnitToSliceGroupMap(%rip)
.LBB1_4:
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end1:
	.size	FmoFinit, .Lfunc_end1-FmoFinit
	.cfi_endproc

	.globl	FmoGetNumberOfSliceGroup
	.p2align	4, 0x90
	.type	FmoGetNumberOfSliceGroup,@function
FmoGetNumberOfSliceGroup:               # @FmoGetNumberOfSliceGroup
	.cfi_startproc
# BB#0:
	movl	NumberOfSliceGroups(%rip), %eax
	retq
.Lfunc_end2:
	.size	FmoGetNumberOfSliceGroup, .Lfunc_end2-FmoGetNumberOfSliceGroup
	.cfi_endproc

	.globl	FmoGetLastMBOfPicture
	.p2align	4, 0x90
	.type	FmoGetLastMBOfPicture,@function
FmoGetLastMBOfPicture:                  # @FmoGetLastMBOfPicture
	.cfi_startproc
# BB#0:
	movl	NumberOfSliceGroups(%rip), %ecx
	decl	%ecx
	movq	img(%rip), %rax
	movl	5836(%rax), %eax
	movq	MbToSliceGroupMap(%rip), %rdx
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	decl	%eax
	js	.LBB3_2
# BB#3:                                 #   in Loop: Header=BB3_1 Depth=1
	movslq	%eax, %rsi
	cmpl	%ecx, (%rdx,%rsi,4)
	jne	.LBB3_1
# BB#4:                                 # %FmoGetLastMBInSliceGroup.exit
	retq
.LBB3_2:
	movl	$-1, %eax
	retq
.Lfunc_end3:
	.size	FmoGetLastMBOfPicture, .Lfunc_end3-FmoGetLastMBOfPicture
	.cfi_endproc

	.globl	FmoGetLastMBInSliceGroup
	.p2align	4, 0x90
	.type	FmoGetLastMBInSliceGroup,@function
FmoGetLastMBInSliceGroup:               # @FmoGetLastMBInSliceGroup
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	movl	5836(%rax), %eax
	movq	MbToSliceGroupMap(%rip), %rcx
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	decl	%eax
	js	.LBB4_2
# BB#3:                                 #   in Loop: Header=BB4_1 Depth=1
	movslq	%eax, %rdx
	cmpl	%edi, (%rcx,%rdx,4)
	jne	.LBB4_1
# BB#4:
	retq
.LBB4_2:
	movl	$-1, %eax
	retq
.Lfunc_end4:
	.size	FmoGetLastMBInSliceGroup, .Lfunc_end4-FmoGetLastMBInSliceGroup
	.cfi_endproc

	.globl	FmoGetSliceGroupId
	.p2align	4, 0x90
	.type	FmoGetSliceGroupId,@function
FmoGetSliceGroupId:                     # @FmoGetSliceGroupId
	.cfi_startproc
# BB#0:
	movq	MbToSliceGroupMap(%rip), %rax
	movslq	%edi, %rcx
	movl	(%rax,%rcx,4), %eax
	retq
.Lfunc_end5:
	.size	FmoGetSliceGroupId, .Lfunc_end5-FmoGetSliceGroupId
	.cfi_endproc

	.globl	FmoGetNextMBNr
	.p2align	4, 0x90
	.type	FmoGetNextMBNr,@function
FmoGetNextMBNr:                         # @FmoGetNextMBNr
	.cfi_startproc
# BB#0:
	movq	MbToSliceGroupMap(%rip), %rax
	movslq	%edi, %rdi
	movl	(%rax,%rdi,4), %edx
	movq	img(%rip), %rcx
	movslq	5836(%rcx), %rcx
	incq	%rdi
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rsi
	cmpq	%rcx, %rsi
	jge	.LBB6_3
# BB#2:                                 #   in Loop: Header=BB6_1 Depth=1
	leaq	1(%rsi), %rdi
	cmpl	%edx, (%rax,%rsi,4)
	jne	.LBB6_1
.LBB6_3:                                # %.critedge
	cmpl	%ecx, %esi
	movl	$-1, %eax
	cmovll	%esi, %eax
	retq
.Lfunc_end6:
	.size	FmoGetNextMBNr, .Lfunc_end6-FmoGetNextMBNr
	.cfi_endproc

	.type	MbToSliceGroupMap,@object # @MbToSliceGroupMap
	.bss
	.globl	MbToSliceGroupMap
	.p2align	3
MbToSliceGroupMap:
	.quad	0
	.size	MbToSliceGroupMap, 8

	.type	MapUnitToSliceGroupMap,@object # @MapUnitToSliceGroupMap
	.globl	MapUnitToSliceGroupMap
	.p2align	3
MapUnitToSliceGroupMap:
	.quad	0
	.size	MapUnitToSliceGroupMap, 8

	.type	NumberOfSliceGroups,@object # @NumberOfSliceGroups
	.local	NumberOfSliceGroups
	.comm	NumberOfSliceGroups,4,4
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"wrong pps->num_slice_group_map_units_minus1 for used SPS and FMO type 6"
	.size	.L.str, 72

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"cannot allocated %d bytes for MapUnitToSliceGroupMap, exit\n"
	.size	.L.str.1, 60

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Illegal slice_group_map_type %d , exit \n"
	.size	.L.str.2, 41

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"cannot allocate %d bytes for MbToSliceGroupMap, exit\n"
	.size	.L.str.3, 54


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
