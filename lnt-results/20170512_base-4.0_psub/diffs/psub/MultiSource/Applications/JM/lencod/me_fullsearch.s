	.text
	.file	"me_fullsearch.bc"
	.globl	FullPelBlockMotionSearch
	.p2align	4, 0x90
	.type	FullPelBlockMotionSearch,@function
FullPelBlockMotionSearch:               # @FullPelBlockMotionSearch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%r8, 32(%rsp)           # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movq	168(%rsp), %r8
	movq	160(%rsp), %r10
	movq	img(%rip), %rax
	movq	14224(%rax), %rcx
	movslq	12(%rax), %rdi
	imulq	$536, %rdi, %rdi        # imm = 0x218
	movslq	432(%rcx,%rdi), %rcx
	movslq	%edx, %rdi
	addq	%rcx, %rdi
	movq	listX(,%rdi,8), %rcx
	movslq	%esi, %r11
	movq	(%rcx,%r11,8), %rbx
	movq	input(%rip), %rcx
	movslq	%r9d, %rdx
	movl	76(%rcx,%rdx,8), %ebp
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movl	72(%rcx,%rdx,8), %ebp
	movl	%ebp, 16(%rsp)          # 4-byte Spill
	movswl	(%r10), %r12d
	cmpl	$1, %edx
	movswl	(%r8), %r15d
	jne	.LBB0_3
# BB#1:
	cmpl	$0, 4168(%rcx)
	je	.LBB0_4
.LBB0_3:
	xorl	%r8d, %r8d
	jmp	.LBB0_5
.LBB0_4:
	testw	%si, %si
	sete	%dl
	cmpl	$1, 20(%rax)
	setne	%r8b
	andb	%dl, %r8b
.LBB0_5:
	movl	176(%rsp), %edx
	movq	active_pps(%rip), %rsi
	cmpl	$0, 192(%rsi)
	je	.LBB0_8
# BB#6:
	movl	20(%rax), %ebp
	testl	%ebp, %ebp
	je	.LBB0_10
# BB#7:
	cmpl	$3, %ebp
	je	.LBB0_10
.LBB0_8:
	cmpl	$0, 196(%rsi)
	je	.LBB0_12
# BB#9:
	cmpl	$1, 20(%rax)
	jne	.LBB0_12
.LBB0_10:
	cmpl	$0, 2936(%rcx)
	setne	%r10b
	jmp	.LBB0_13
.LBB0_12:
	xorl	%r10d, %r10d
.LBB0_13:
	movq	6448(%rbx), %rax
	movq	%rax, ref_pic_sub(%rip)
	movl	6392(%rbx), %eax
	movw	%ax, img_width(%rip)
	movl	6396(%rbx), %r9d
	movw	%r9w, img_height(%rip)
	movl	6408(%rbx), %ecx
	movl	%ecx, width_pad(%rip)
	movl	6412(%rbx), %ecx
	movl	%ecx, height_pad(%rip)
	testb	%r10b, %r10b
	je	.LBB0_15
# BB#14:
	movq	wp_weight(%rip), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	(%rcx,%r11,8), %rcx
	movl	(%rcx), %ecx
	movl	%ecx, weight_luma(%rip)
	movq	wp_offset(%rip), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	(%rcx,%r11,8), %rcx
	movl	(%rcx), %ecx
	movl	%ecx, offset_luma(%rip)
.LBB0_15:
	leal	1(%rdx,%rdx), %esi
	addl	24(%rsp), %r12d         # 4-byte Folded Reload
	cmpl	$0, ChromaMEEnable(%rip)
	movl	%r12d, %r14d
	je	.LBB0_18
# BB#16:
	movq	6464(%rbx), %rcx
	movq	(%rcx), %rbp
	movq	%rbp, ref_pic_sub+8(%rip)
	movl	%r14d, %r12d
	movq	8(%rcx), %rcx
	movq	%rcx, ref_pic_sub+16(%rip)
	movl	6416(%rbx), %ecx
	movl	%ecx, width_pad_cr(%rip)
	movl	6420(%rbx), %ecx
	movl	%ecx, height_pad_cr(%rip)
	testb	%r10b, %r10b
	je	.LBB0_18
# BB#17:
	movq	wp_weight(%rip), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	(%rcx,%r11,8), %rcx
	movl	4(%rcx), %ebx
	movl	%ebx, weight_cr(%rip)
	movl	8(%rcx), %ecx
	movl	%ecx, weight_cr+4(%rip)
	movq	wp_offset(%rip), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	(%rcx,%r11,8), %rcx
	movl	4(%rcx), %edi
	movl	%edi, offset_cr(%rip)
	movl	8(%rcx), %ecx
	movl	%ecx, offset_cr+4(%rip)
.LBB0_18:
	imull	%esi, %esi
	movswl	144(%rsp), %ebx
	movswl	152(%rsp), %edi
	movl	%r15d, %r11d
	addl	32(%rsp), %r11d         # 4-byte Folded Reload
	movl	$1, %ecx
	cmpl	%edx, %r12d
	jle	.LBB0_22
# BB#19:
	cmpl	%edx, %r11d
	jle	.LBB0_22
# BB#20:
	cwtl
	notl	%edx
	movl	%edx, %ebp
	subl	16(%rsp), %ebp          # 4-byte Folded Reload
	addl	%eax, %ebp
	cmpl	%ebp, %r14d
	movl	%r14d, %r12d
	jge	.LBB0_22
# BB#21:
	movswl	%r9w, %eax
	subl	12(%rsp), %edx          # 4-byte Folded Reload
	addl	%eax, %edx
	xorl	%ecx, %ecx
	cmpl	%edx, %r11d
	setge	%cl
.LBB0_22:
	movl	192(%rsp), %ebp
	movl	184(%rsp), %r13d
	movl	%ecx, ref_access_method(%rip)
	movq	24(%rsp), %rdx          # 8-byte Reload
	leal	(,%rdx,4), %eax
	negl	%eax
	subl	%ebx, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rcx          # 8-byte Reload
	leal	(,%rcx,4), %eax
	negl	%eax
	subl	%edi, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movzbl	%r10b, %eax
	leaq	(%rax,%rax,2), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%esi, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	testb	%r8b, %r8b
	movl	%r11d, 20(%rsp)         # 4-byte Spill
	je	.LBB0_27
# BB#23:                                # %.split.us.preheader
	sarl	$12, %ebp
	movl	%ebp, 44(%rsp)          # 4-byte Spill
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	movq	%rcx, %r10
	movq	%rdx, %rbp
	.p2align	4, 0x90
.LBB0_24:                               # %.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	spiral_search_x(%rip), %rax
	movswl	(%rax,%r14,2), %eax
	addl	%r12d, %eax
	movq	spiral_search_y(%rip), %rcx
	movswl	(%rcx,%r14,2), %ecx
	addl	%r11d, %ecx
	movq	mvbits(%rip), %rdx
	movq	80(%rsp), %rsi          # 8-byte Reload
	leal	(%rsi,%rax,4), %esi
	movslq	%esi, %rsi
	movq	72(%rsp), %rdi          # 8-byte Reload
	leal	(%rdi,%rcx,4), %edi
	movslq	%edi, %rdi
	movl	(%rdx,%rdi,4), %ebx
	addl	(%rdx,%rsi,4), %ebx
	leal	(,%rax,4), %r8d
	leal	(,%rcx,4), %r9d
	imull	192(%rsp), %ebx
	sarl	$16, %ebx
	cmpl	%r10d, %r9d
	movl	$0, %eax
	cmovel	44(%rsp), %eax          # 4-byte Folded Reload
	cmpl	%ebp, %r8d
	movl	$0, %ecx
	cmovnel	%ecx, %eax
	subl	%eax, %ebx
	movl	%r13d, %ecx
	subl	%ebx, %ecx
	jle	.LBB0_26
# BB#25:                                #   in Loop: Header=BB0_24 Depth=1
	addl	$80, %r8d
	addl	$80, %r9d
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	callq	*computeUniPred(,%rax,8)
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	32(%rsp), %r10          # 8-byte Reload
	movl	20(%rsp), %r11d         # 4-byte Reload
	addl	%ebx, %eax
	cmpl	%r13d, %eax
	cmovll	%r14d, %r15d
	cmovlel	%eax, %r13d
.LBB0_26:                               #   in Loop: Header=BB0_24 Depth=1
	incq	%r14
	cmpq	64(%rsp), %r14          # 8-byte Folded Reload
	jl	.LBB0_24
	jmp	.LBB0_31
.LBB0_27:                               # %.split.preheader
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_28:                               # %.split
                                        # =>This Inner Loop Header: Depth=1
	movq	spiral_search_x(%rip), %rax
	movswl	(%rax,%rbx,2), %r8d
	addl	%r12d, %r8d
	movq	spiral_search_y(%rip), %rax
	movswl	(%rax,%rbx,2), %r9d
	addl	%r11d, %r9d
	movq	mvbits(%rip), %rax
	movq	80(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%r8,4), %ecx
	movslq	%ecx, %rcx
	movq	72(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%r9,4), %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %ebp
	addl	(%rax,%rcx,4), %ebp
	imull	192(%rsp), %ebp
	sarl	$16, %ebp
	movl	%r13d, %ecx
	subl	%ebp, %ecx
	jle	.LBB0_30
# BB#29:                                #   in Loop: Header=BB0_28 Depth=1
	shll	$2, %r8d
	shll	$2, %r9d
	addl	$80, %r8d
	addl	$80, %r9d
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	movq	48(%rsp), %rax          # 8-byte Reload
	callq	*computeUniPred(,%rax,8)
	movl	20(%rsp), %r11d         # 4-byte Reload
	addl	%ebp, %eax
	cmpl	%r13d, %eax
	cmovll	%ebx, %r15d
	cmovlel	%eax, %r13d
.LBB0_30:                               #   in Loop: Header=BB0_28 Depth=1
	movl	%r14d, %r12d
	incq	%rbx
	cmpq	64(%rsp), %rbx          # 8-byte Folded Reload
	jl	.LBB0_28
.LBB0_31:                               # %.us-lcssa.us
	testl	%r15d, %r15d
	je	.LBB0_33
# BB#32:
	movq	spiral_search_x(%rip), %rax
	movslq	%r15d, %rcx
	movzwl	(%rax,%rcx,2), %eax
	movq	160(%rsp), %rdx
	addw	%ax, (%rdx)
	movq	spiral_search_y(%rip), %rax
	movzwl	(%rax,%rcx,2), %eax
	movq	168(%rsp), %rcx
	addw	%ax, (%rcx)
.LBB0_33:
	movl	%r13d, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	FullPelBlockMotionSearch, .Lfunc_end0-FullPelBlockMotionSearch
	.cfi_endproc

	.globl	FullPelBlockMotionBiPred
	.p2align	4, 0x90
	.type	FullPelBlockMotionBiPred,@function
FullPelBlockMotionBiPred:               # @FullPelBlockMotionBiPred
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 112
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movq	152(%rsp), %rax
	movq	144(%rsp), %rcx
	movq	168(%rsp), %r10
	movq	160(%rsp), %r11
	movq	img(%rip), %rdi
	movq	14224(%rdi), %rbp
	movslq	12(%rdi), %rdi
	imulq	$536, %rdi, %rdi        # imm = 0x218
	movslq	432(%rbp,%rdi), %r14
	movq	input(%rip), %rdi
	movslq	%r9d, %rbx
	movl	76(%rdi,%rbx,8), %ebp
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movl	72(%rdi,%rbx,8), %edi
	movl	%edi, 16(%rsp)          # 4-byte Spill
	movzwl	(%rcx), %r13d
	movzwl	(%rax), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movzwl	(%r11), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movzwl	(%r10), %ecx
	movq	active_pps(%rip), %rax
	movl	196(%rax), %r9d
	testl	%r9d, %r9d
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	je	.LBB1_3
# BB#1:
	movq	wp_offset(%rip), %rdi
	testl	%edx, %edx
	je	.LBB1_4
# BB#2:
	movq	8(%rdi,%r14,8), %rcx
	movswq	%si, %rax
	shlq	$2, %rax
	movq	(%rcx), %rcx
	addq	%rax, %rcx
	movq	(%rdi,%r14,8), %rdi
	addq	(%rdi), %rax
	jmp	.LBB1_5
.LBB1_3:
	xorl	%ebp, %ebp
	movl	$1, %ecx
	jmp	.LBB1_6
.LBB1_4:
	movq	(%rdi,%r14,8), %rax
	movswq	%si, %rbx
	movq	(%rax,%rbx,8), %rcx
	movq	8(%rdi,%r14,8), %rax
	movq	(%rax,%rbx,8), %rax
.LBB1_5:
	movswl	(%rcx), %ecx
	movswl	(%rax), %ebp
	incl	%ecx
.LBB1_6:
	leal	(%r14,%rdx), %eax
	cltq
	movq	listX(,%rax,8), %rax
	movswq	%si, %rsi
	movq	(%rax,%rsi,8), %rbx
	movl	%edx, %eax
	xorl	$1, %eax
	addl	%r14d, %eax
	cltq
	movq	listX(,%rax,8), %rax
	movq	(%rax), %r11
	movq	6448(%rbx), %rdi
	movq	%rdi, ref_pic1_sub(%rip)
	movq	6448(%r11), %rdi
	movq	%rdi, ref_pic2_sub(%rip)
	movl	6392(%rbx), %r10d
	movw	%r10w, img_width(%rip)
	movl	6396(%rbx), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movw	%ax, img_height(%rip)
	movl	6408(%rbx), %edi
	movl	%edi, width_pad(%rip)
	movl	6412(%rbx), %edi
	movl	%edi, height_pad(%rip)
	testl	%r9d, %r9d
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	je	.LBB1_9
# BB#7:
	movl	%r9d, 8(%rsp)           # 4-byte Spill
	movl	%r10d, %r9d
	movl	32(%rsp), %r12d         # 4-byte Reload
	movq	wbp_weight(%rip), %rdi
	testl	%edx, %edx
	je	.LBB1_10
# BB#8:
	movq	8(%rdi,%r14,8), %rax
	movq	(%rax), %rax
	leaq	(,%rsi,8), %r10
	movq	(%rax,%rsi,8), %rax
	movzwl	(%rax), %eax
	movw	%ax, weight1(%rip)
	movq	(%rdi,%r14,8), %rax
	addq	(%rax), %r10
	jmp	.LBB1_11
.LBB1_9:
	movb	luma_log_weight_denom(%rip), %cl
	movl	$1, %eax
	shll	%cl, %eax
	movw	%ax, weight1(%rip)
	movw	%ax, weight2(%rip)
	movl	$computeBiPred1, %edi
	xorl	%ecx, %ecx
	movl	32(%rsp), %r12d         # 4-byte Reload
	jmp	.LBB1_12
.LBB1_10:
	movq	(%rdi,%r14,8), %rax
	movq	(%rax,%rsi,8), %rax
	movq	(%rax), %rax
	movzwl	(%rax), %eax
	movw	%ax, weight1(%rip)
	movq	8(%rdi,%r14,8), %rax
	movq	(%rax,%rsi,8), %r10
.LBB1_11:
	movq	(%r10), %rax
	movzwl	(%rax), %eax
	movw	%ax, weight2(%rip)
	addl	%ebp, %ecx
	shrl	%ecx
	movl	$computeBiPred2, %edi
	movl	%r9d, %r10d
	movl	8(%rsp), %r9d           # 4-byte Reload
.LBB1_12:
	addl	%r15d, %r13d
	movw	%cx, offsetBi(%rip)
	movq	(%rdi), %rax
	movq	%rax, computeBiPred(%rip)
	cmpl	$0, ChromaMEEnable(%rip)
	je	.LBB1_23
# BB#13:
	movq	6464(%rbx), %rax
	movq	(%rax), %rcx
	movq	%rcx, ref_pic1_sub+8(%rip)
	movq	8(%rax), %rax
	movq	%rax, ref_pic1_sub+16(%rip)
	movq	6464(%r11), %rax
	movq	(%rax), %rcx
	movq	%rcx, ref_pic2_sub+8(%rip)
	movq	8(%rax), %rax
	movq	%rax, ref_pic2_sub+16(%rip)
	movl	6416(%rbx), %eax
	movl	%eax, width_pad_cr(%rip)
	movl	6420(%rbx), %eax
	movl	%eax, height_pad_cr(%rip)
	testl	%r9d, %r9d
	je	.LBB1_16
# BB#14:
	movq	wbp_weight(%rip), %rax
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB1_17
# BB#15:
	movq	8(%rax,%r14,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movzwl	4(%rcx), %edi
	movw	%di, weight1_cr(%rip)
	movzwl	8(%rcx), %ecx
	movw	%cx, weight1_cr+2(%rip)
	movq	(%rax,%r14,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%rsi,8), %rax
	movzwl	4(%rax), %ecx
	movw	%cx, weight2_cr(%rip)
	movzwl	8(%rax), %eax
	movw	%ax, weight2_cr+2(%rip)
	movq	wp_offset(%rip), %rax
	movq	8(%rax,%r14,8), %rdi
	movq	(%rax,%r14,8), %rcx
	jmp	.LBB1_18
.LBB1_16:
	movb	chroma_log_weight_denom(%rip), %cl
	movl	$1, %eax
	shll	%cl, %eax
	movw	%ax, weight1_cr(%rip)
	movw	%ax, weight1_cr+2(%rip)
	movw	%ax, weight2_cr(%rip)
	movw	%ax, weight2_cr+2(%rip)
	movw	$0, offsetBi_cr(%rip)
	xorl	%eax, %eax
	jmp	.LBB1_22
.LBB1_17:
	movq	(%rax,%r14,8), %rdi
	leaq	(,%rsi,8), %rcx
	movq	(%rdi,%rsi,8), %rdi
	movq	(%rdi), %rdi
	movzwl	4(%rdi), %ebx
	movw	%bx, weight1_cr(%rip)
	movzwl	8(%rdi), %edi
	movw	%di, weight1_cr+2(%rip)
	movq	8(%rax,%r14,8), %rax
	movq	(%rax,%rsi,8), %rax
	movq	(%rax), %rax
	movzwl	4(%rax), %edi
	movw	%di, weight2_cr(%rip)
	movzwl	8(%rax), %eax
	movw	%ax, weight2_cr+2(%rip)
	movq	wp_offset(%rip), %rax
	movq	(%rax,%r14,8), %rdi
	addq	%rcx, %rdi
	addq	8(%rax,%r14,8), %rcx
.LBB1_18:
	movq	(%rdi), %rdi
	movl	4(%rdi), %edi
	movq	(%rcx), %rcx
	movl	4(%rcx), %ecx
	leal	1(%rdi,%rcx), %ecx
	shrl	%ecx
	movw	%cx, offsetBi_cr(%rip)
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB1_20
# BB#19:
	movq	8(%rax,%r14,8), %rcx
	movq	(%rax,%r14,8), %rsi
	jmp	.LBB1_21
.LBB1_20:
	shlq	$3, %rsi
	movq	(%rax,%r14,8), %rcx
	addq	%rsi, %rcx
	addq	8(%rax,%r14,8), %rsi
.LBB1_21:
	movq	(%rcx), %rax
	movl	8(%rax), %eax
	movq	(%rsi), %rcx
	movl	8(%rcx), %ecx
	leal	1(%rax,%rcx), %eax
	shrl	%eax
.LBB1_22:
	movw	%ax, offsetBi_cr+2(%rip)
.LBB1_23:
	movl	176(%rsp), %r14d
	leal	1(%r14,%r14), %r11d
	movl	4(%rsp), %ecx           # 4-byte Reload
	addl	%r8d, %ecx
	addl	%r15d, %r12d
	movswl	%r13w, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	cmpl	%r14d, %eax
	movl	$1, %edi
	jle	.LBB1_27
# BB#24:
	movswl	%r10w, %esi
	movl	%r14d, %eax
	notl	%eax
	movl	%eax, %edi
	subl	16(%rsp), %edi          # 4-byte Folded Reload
	addl	%esi, %edi
	cmpl	%edi, 4(%rsp)           # 4-byte Folded Reload
	movl	$1, %edi
	jge	.LBB1_27
# BB#25:
	movswl	%cx, %esi
	cmpl	%r14d, %esi
	movl	$1, %edi
	jle	.LBB1_27
# BB#26:
	movswl	(%rsp), %edi            # 2-byte Folded Reload
	subl	12(%rsp), %eax          # 4-byte Folded Reload
	addl	%edi, %eax
	xorl	%edi, %edi
	cmpl	%eax, %esi
	setge	%dil
.LBB1_27:
	movl	%r10d, %edx
	imull	%r11d, %r11d
	shll	$2, %r15d
	movswl	112(%rsp), %esi
	movswl	120(%rsp), %eax
	movswl	128(%rsp), %r10d
	movswl	136(%rsp), %r9d
	movl	20(%rsp), %ebx          # 4-byte Reload
	addl	%r8d, %ebx
	shll	$2, %r8d
	movl	%edi, bipred2_access_method(%rip)
	movl	%r12d, %ebp
	shll	$16, %r12d
	movswl	%bp, %ebp
	cmpl	%r14d, %ebp
	jle	.LBB1_32
# BB#28:
	movl	%ebx, %r13d
	movswl	%dx, %ebx
	movl	%r14d, %edx
	notl	%edx
	movl	%edx, %edi
	subl	16(%rsp), %edi          # 4-byte Folded Reload
	addl	%ebx, %edi
	movl	%r13d, %ebx
	cmpl	%edi, %ebp
	jge	.LBB1_32
# BB#29:
	movswl	%bx, %ebp
	cmpl	%r14d, %ebp
	movl	$1, %edi
	jle	.LBB1_33
# BB#30:
	movswl	(%rsp), %edi            # 2-byte Folded Reload
	subl	12(%rsp), %edx          # 4-byte Folded Reload
	addl	%edi, %edx
	xorl	%edi, %edi
	cmpl	%edx, %ebp
	setge	%dil
	jmp	.LBB1_33
.LBB1_32:
	movl	$1, %edi
.LBB1_33:
	movl	184(%rsp), %r13d
	movl	%edi, bipred1_access_method(%rip)
	movswl	%cx, %ecx
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	sarl	$14, %r12d
	addl	%r15d, %esi
	movl	%r12d, %edx
	subl	%esi, %edx
	movslq	%edx, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movswl	%bx, %edx
	leal	(,%rdx,4), %esi
	addl	%r8d, %eax
	subl	%eax, %esi
	movslq	%esi, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%r10d, %ebx
	addl	%r15d, %ebx
	addl	%r8d, %r9d
	movl	%r9d, %r8d
	addl	$80, %r12d
	movl	%r12d, (%rsp)           # 4-byte Spill
	leal	80(,%rdx,4), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	%r11d, %r11d
	xorl	%r14d, %r14d
	xorl	%r10d, %r10d
	movl	%r8d, 44(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB1_34:                               # =>This Inner Loop Header: Depth=1
	movq	spiral_search_x(%rip), %rax
	movswl	(%rax,%r14,2), %eax
	addl	4(%rsp), %eax           # 4-byte Folded Reload
	shll	$2, %eax
	movq	spiral_search_y(%rip), %rcx
	movswl	(%rcx,%r14,2), %ebp
	addl	20(%rsp), %ebp          # 4-byte Folded Reload
	shll	$2, %ebp
	movq	mvbits(%rip), %rcx
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	(%rcx,%rdx,4), %edx
	movq	32(%rsp), %rsi          # 8-byte Reload
	addl	(%rcx,%rsi,4), %edx
	movl	192(%rsp), %esi
	movl	%esi, %r9d
	imull	%r9d, %edx
	sarl	$16, %edx
	movl	%eax, %esi
	subl	%ebx, %esi
	movslq	%esi, %rsi
	movl	%ebp, %edi
	subl	%r8d, %edi
	movslq	%edi, %rdi
	movl	(%rcx,%rdi,4), %r15d
	addl	(%rcx,%rsi,4), %r15d
	imull	%r9d, %r15d
	sarl	$16, %r15d
	addl	%edx, %r15d
	movl	%r13d, %ecx
	subl	%r15d, %ecx
	jle	.LBB1_36
# BB#35:                                #   in Loop: Header=BB1_34 Depth=1
	addl	$80, %eax
	addl	$80, %ebp
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	(%rsp), %r8d            # 4-byte Reload
	movl	8(%rsp), %r9d           # 4-byte Reload
	pushq	%rbp
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	movl	%r10d, %ebp
	movl	%r13d, %r12d
	movq	%r11, %r13
	callq	*computeBiPred(%rip)
	movq	%r13, %r11
	movl	%r12d, %r13d
	movl	%ebp, %r10d
	movl	60(%rsp), %r8d          # 4-byte Reload
	addq	$16, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset -16
	addl	%r15d, %eax
	cmpl	%r13d, %eax
	cmovll	%r14d, %r10d
	cmovlel	%eax, %r13d
.LBB1_36:                               #   in Loop: Header=BB1_34 Depth=1
	incq	%r14
	cmpq	%r11, %r14
	jl	.LBB1_34
# BB#37:
	testl	%r10d, %r10d
	je	.LBB1_39
# BB#38:
	movq	spiral_search_x(%rip), %rax
	movslq	%r10d, %rcx
	movzwl	(%rax,%rcx,2), %eax
	movq	144(%rsp), %rdx
	addw	%ax, (%rdx)
	movq	spiral_search_y(%rip), %rax
	movzwl	(%rax,%rcx,2), %eax
	movq	152(%rsp), %rcx
	addw	%ax, (%rcx)
.LBB1_39:
	movl	%r13d, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	FullPelBlockMotionBiPred, .Lfunc_end1-FullPelBlockMotionBiPred
	.cfi_endproc

	.globl	SubPelBlockMotionSearch
	.p2align	4, 0x90
	.type	SubPelBlockMotionSearch,@function
SubPelBlockMotionSearch:                # @SubPelBlockMotionSearch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 144
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movl	176(%rsp), %edi
	movq	input(%rip), %rbp
	movq	img(%rip), %r11
	cmpl	$0, 4168(%rbp)
	je	.LBB2_2
# BB#1:
	xorl	%r10d, %r10d
	jmp	.LBB2_9
.LBB2_2:
	cmpl	$1, 20(%r11)
	jne	.LBB2_4
# BB#3:
	xorl	%r10d, %r10d
	jmp	.LBB2_9
.LBB2_4:
	xorl	%r10d, %r10d
	testw	%si, %si
	jne	.LBB2_9
# BB#5:
	cmpl	$1, %r9d
	jne	.LBB2_9
# BB#6:
	movq	160(%rsp), %rax
	cmpw	$0, (%rax)
	je	.LBB2_8
# BB#7:
	xorl	%r10d, %r10d
	jmp	.LBB2_9
.LBB2_8:
	movq	168(%rsp), %rax
	cmpw	$0, (%rax)
	sete	%r10b
.LBB2_9:                                # %._crit_edge219
	movq	200(%rsp), %r15
	movslq	%r9d, %rax
	movl	72(%rbp,%rax,8), %ebx
	movl	%ebx, 20(%rsp)          # 4-byte Spill
	movl	76(%rbp,%rax,8), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movslq	start_me_refinement_hp(%rip), %r14
	testl	%edi, %edi
	movl	$1, %r9d
	cmovgl	%edi, %r9d
	testq	%r14, %r14
	cmovnel	%edi, %r9d
	movq	14224(%r11), %rax
	movslq	12(%r11), %rdi
	imulq	$536, %rdi, %rdi        # imm = 0x218
	movslq	432(%rax,%rdi), %rdi
	movq	active_pps(%rip), %rbx
	cmpl	$0, 192(%rbx)
	je	.LBB2_12
# BB#10:
	movl	20(%r11), %eax
	testl	%eax, %eax
	je	.LBB2_15
# BB#11:
	cmpl	$3, %eax
	je	.LBB2_15
.LBB2_12:
	cmpl	$0, 196(%rbx)
	je	.LBB2_13
# BB#14:
	cmpl	$1, 20(%r11)
	jne	.LBB2_13
.LBB2_15:
	cmpl	$0, 2936(%rbp)
	setne	%bpl
	jmp	.LBB2_16
.LBB2_13:
	xorl	%ebp, %ebp
.LBB2_16:
	movq	160(%rsp), %r11
	movslq	%edx, %rax
	addq	%rax, %rdi
	movq	listX(,%rdi,8), %rdx
	movswq	%si, %rax
	movq	(%rdx,%rax,8), %rdx
	movl	6392(%rdx), %ebx
	movl	6396(%rdx), %r12d
	movl	4(%r15), %esi
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movq	6448(%rdx), %rsi
	movq	%rsi, ref_pic_sub(%rip)
	movl	6408(%rdx), %esi
	movl	%esi, width_pad(%rip)
	movl	6412(%rdx), %esi
	movl	%esi, height_pad(%rip)
	testb	%bpl, %bpl
	je	.LBB2_18
# BB#17:
	movq	wp_weight(%rip), %rsi
	movq	(%rsi,%rdi,8), %rsi
	movq	(%rsi,%rax,8), %rsi
	movl	(%rsi), %esi
	movl	%esi, weight_luma(%rip)
	movq	wp_offset(%rip), %rsi
	movq	(%rsi,%rdi,8), %rsi
	movq	(%rsi,%rax,8), %rsi
	movl	(%rsi), %esi
	movl	%esi, offset_luma(%rip)
.LBB2_18:
	leal	80(,%rcx,4), %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	cmpl	$0, ChromaMEEnable(%rip)
	je	.LBB2_21
# BB#19:
	movq	6464(%rdx), %rcx
	movq	(%rcx), %rsi
	movq	%rsi, ref_pic_sub+8(%rip)
	movq	8(%rcx), %rcx
	movq	%rcx, ref_pic_sub+16(%rip)
	movl	6416(%rdx), %ecx
	movl	%ecx, width_pad_cr(%rip)
	movl	6420(%rdx), %ecx
	movl	%ecx, height_pad_cr(%rip)
	testb	%bpl, %bpl
	je	.LBB2_21
# BB#20:
	movq	wp_weight(%rip), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	(%rcx,%rax,8), %rcx
	movl	4(%rcx), %edx
	movl	%edx, weight_cr(%rip)
	movl	8(%rcx), %ecx
	movl	%ecx, weight_cr+4(%rip)
	movq	wp_offset(%rip), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	(%rcx,%rax,8), %rax
	movl	4(%rax), %ecx
	movl	%ecx, offset_cr(%rip)
	movl	8(%rax), %eax
	movl	%eax, offset_cr+4(%rip)
.LBB2_21:
	movzbl	%bpl, %ebp
	subl	20(%rsp), %ebx          # 4-byte Folded Reload
	subl	16(%rsp), %r12d         # 4-byte Folded Reload
	leal	80(,%r8,4), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movswl	(%r11), %ecx
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	(%rcx,%rax), %edx
	movl	$1, %eax
	cmpl	$2, %edx
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	jl	.LBB2_22
# BB#23:
	leal	(,%rbx,4), %esi
	addl	$159, %esi
	movq	168(%rsp), %rdi
	cmpl	%esi, %edx
	jge	.LBB2_26
# BB#24:
	movswl	(%rdi), %edx
	addl	8(%rsp), %edx           # 4-byte Folded Reload
	cmpl	$2, %edx
	jl	.LBB2_26
# BB#25:
	leal	(,%r12,4), %esi
	addl	$159, %esi
	xorl	%eax, %eax
	cmpl	%esi, %edx
	setge	%al
	jmp	.LBB2_26
.LBB2_22:
	movq	168(%rsp), %rdi
.LBB2_26:
	movl	192(%rsp), %r13d
	movzwl	152(%rsp), %r8d
	movzwl	144(%rsp), %ebx
	movl	%eax, ref_access_method(%rip)
	movl	%r9d, %esi
	cmpl	%esi, %r14d
	movq	%r12, 64(%rsp)          # 8-byte Spill
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	jge	.LBB2_35
# BB#27:                                # %.lr.ph206
	leal	1(%rbp,%rbp,2), %eax
	movswl	%bx, %edx
	movl	%edx, 40(%rsp)          # 4-byte Spill
	movswl	%r8w, %edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testb	%r10b, %r10b
	je	.LBB2_28
# BB#45:                                # %.lr.ph206.split.us.preheader
	movl	4(%rsp), %eax           # 4-byte Reload
	sarl	$12, %eax
	negl	%eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movq	%r14, %rbp
	addq	%rbp, %rbp
	decl	%esi
	xorl	%r15d, %r15d
	jmp	.LBB2_46
	.p2align	4, 0x90
.LBB2_49:                               # %..lr.ph206.split.us_crit_edge
                                        #   in Loop: Header=BB2_46 Depth=1
	movw	(%r11), %cx
	incl	%r14d
	addq	$2, %rbp
.LBB2_46:                               # %.lr.ph206.split.us
                                        # =>This Inner Loop Header: Depth=1
	movswl	%cx, %eax
	movq	spiral_hpel_search_x(%rip), %rcx
	movswl	(%rcx,%rbp), %r8d
	addl	%eax, %r8d
	movswl	(%rdi), %eax
	movq	spiral_hpel_search_y(%rip), %rcx
	movswl	(%rcx,%rbp), %r9d
	addl	%eax, %r9d
	movq	mvbits(%rip), %rax
	movl	%r8d, %ecx
	subl	40(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movl	%r9d, %edx
	subl	12(%rsp), %edx          # 4-byte Folded Reload
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %r12d
	addl	(%rax,%rcx,4), %r12d
	imull	4(%rsp), %r12d          # 4-byte Folded Reload
	sarl	$16, %r12d
	movl	%r13d, %ecx
	subl	%r12d, %ecx
	jle	.LBB2_48
# BB#47:                                #   in Loop: Header=BB2_46 Depth=1
	addl	24(%rsp), %r8d          # 4-byte Folded Reload
	addl	8(%rsp), %r9d           # 4-byte Folded Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%esi, %ebx
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	20(%rsp), %edx          # 4-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	callq	*computeUniPred(,%rax,8)
	movl	%ebx, %esi
	movq	160(%rsp), %r11
	movq	168(%rsp), %rdi
	testl	%r14d, %r14d
	movl	$0, %ecx
	cmovel	60(%rsp), %ecx          # 4-byte Folded Reload
	addl	%ecx, %r12d
	addl	%eax, %r12d
	cmpl	%r13d, %r12d
	cmovll	%r14d, %r15d
	cmovlel	%r12d, %r13d
.LBB2_48:                               #   in Loop: Header=BB2_46 Depth=1
	cmpl	%r14d, %esi
	jne	.LBB2_49
	jmp	.LBB2_32
.LBB2_28:                               # %.lr.ph206.split.preheader
	movq	%r14, %rbp
	addq	%rbp, %rbp
	decl	%esi
	xorl	%r15d, %r15d
	jmp	.LBB2_29
	.p2align	4, 0x90
.LBB2_50:                               # %..lr.ph206.split_crit_edge
                                        #   in Loop: Header=BB2_29 Depth=1
	movw	(%r11), %cx
	addq	$2, %rbp
	incl	%r14d
.LBB2_29:                               # %.lr.ph206.split
                                        # =>This Inner Loop Header: Depth=1
	movswl	%cx, %eax
	movq	spiral_hpel_search_x(%rip), %rcx
	movswl	(%rcx,%rbp), %r8d
	addl	%eax, %r8d
	movswl	(%rdi), %eax
	movq	spiral_hpel_search_y(%rip), %rcx
	movswl	(%rcx,%rbp), %r9d
	addl	%eax, %r9d
	movq	mvbits(%rip), %rax
	movl	%r8d, %ecx
	subl	40(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movl	%r9d, %edx
	subl	12(%rsp), %edx          # 4-byte Folded Reload
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %ebx
	addl	(%rax,%rcx,4), %ebx
	imull	4(%rsp), %ebx           # 4-byte Folded Reload
	sarl	$16, %ebx
	movl	%r13d, %ecx
	subl	%ebx, %ecx
	jle	.LBB2_31
# BB#30:                                #   in Loop: Header=BB2_29 Depth=1
	addl	24(%rsp), %r8d          # 4-byte Folded Reload
	addl	8(%rsp), %r9d           # 4-byte Folded Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%esi, %r12d
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	20(%rsp), %edx          # 4-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	callq	*computeUniPred(,%rax,8)
	movl	%r12d, %esi
	movq	160(%rsp), %r11
	movq	168(%rsp), %rdi
	addl	%eax, %ebx
	cmpl	%r13d, %ebx
	cmovll	%r14d, %r15d
	cmovlel	%ebx, %r13d
.LBB2_31:                               #   in Loop: Header=BB2_29 Depth=1
	cmpl	%r14d, %esi
	jne	.LBB2_50
.LBB2_32:
	movzwl	152(%rsp), %ebx
	movl	%ebx, %r8d
	movzwl	144(%rsp), %ebx
	testl	%r15d, %r15d
	je	.LBB2_34
# BB#33:
	movq	spiral_hpel_search_x(%rip), %rax
	movslq	%r15d, %rcx
	movzwl	(%rax,%rcx,2), %eax
	addw	%ax, (%r11)
	movq	spiral_hpel_search_y(%rip), %rax
	movzwl	(%rax,%rcx,2), %eax
	addw	%ax, (%rdi)
.LBB2_34:                               # %._crit_edge207.thread
	movq	200(%rsp), %r15
.LBB2_35:                               # %._crit_edge207.thread
	movl	184(%rsp), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movslq	start_me_refinement_qp(%rip), %r12
	testq	%r12, %r12
	movl	$2147483647, %r10d      # imm = 0x7FFFFFFF
	cmovnel	%r13d, %r10d
	movswl	(%r11), %eax
	movl	$1, %ecx
	movl	%eax, %edx
	addl	24(%rsp), %edx          # 4-byte Folded Reload
	jle	.LBB2_39
# BB#36:                                # %._crit_edge207.thread
	movq	80(%rsp), %rsi          # 8-byte Reload
	leal	160(,%rsi,4), %esi
	cmpl	%esi, %edx
	jge	.LBB2_39
# BB#37:
	movswl	(%rdi), %edx
	addl	8(%rsp), %edx           # 4-byte Folded Reload
	jle	.LBB2_39
# BB#38:
	movq	64(%rsp), %rcx          # 8-byte Reload
	leal	160(,%rcx,4), %esi
	xorl	%ecx, %ecx
	cmpl	%esi, %edx
	setge	%cl
.LBB2_39:
	movl	%ecx, ref_access_method(%rip)
	cmpl	184(%rsp), %r12d
	jge	.LBB2_53
# BB#40:                                # %.lr.ph
	movq	72(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rcx,2), %ecx
	movl	8(%r15), %r15d
	addl	$2, %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movswl	%bx, %ebp
	movswl	%r8w, %r14d
	movq	%r12, %rbx
	addq	%rbx, %rbx
	decl	4(%rsp)                 # 4-byte Folded Spill
	xorl	%esi, %esi
	jmp	.LBB2_41
	.p2align	4, 0x90
.LBB2_44:                               # %._crit_edge222
                                        #   in Loop: Header=BB2_41 Depth=1
	movw	(%r11), %ax
	addq	$2, %rbx
	incl	%r12d
.LBB2_41:                               # =>This Inner Loop Header: Depth=1
	cwtl
	movq	spiral_search_x(%rip), %rcx
	movswl	(%rcx,%rbx), %r8d
	addl	%eax, %r8d
	movswl	(%rdi), %eax
	movq	spiral_search_y(%rip), %rcx
	movswl	(%rcx,%rbx), %r9d
	addl	%eax, %r9d
	movq	mvbits(%rip), %rax
	movl	%r8d, %ecx
	subl	%ebp, %ecx
	movslq	%ecx, %rcx
	movl	%r9d, %edx
	subl	%r14d, %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %r13d
	addl	(%rax,%rcx,4), %r13d
	imull	%r15d, %r13d
	sarl	$16, %r13d
	movl	%r10d, %ecx
	subl	%r13d, %ecx
	jle	.LBB2_43
# BB#42:                                #   in Loop: Header=BB2_41 Depth=1
	addl	24(%rsp), %r8d          # 4-byte Folded Reload
	addl	8(%rsp), %r9d           # 4-byte Folded Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movl	%r10d, %r12d
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	20(%rsp), %edx          # 4-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	callq	*computeUniPred(,%rax,8)
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	%r12d, %r10d
	movq	40(%rsp), %r12          # 8-byte Reload
	movq	160(%rsp), %r11
	movq	168(%rsp), %rdi
	addl	%r13d, %eax
	cmpl	%r10d, %eax
	cmovll	%r12d, %esi
	cmovlel	%eax, %r10d
.LBB2_43:                               #   in Loop: Header=BB2_41 Depth=1
	cmpl	%r12d, 4(%rsp)          # 4-byte Folded Reload
	jne	.LBB2_44
# BB#51:                                # %._crit_edge
	testl	%esi, %esi
	je	.LBB2_53
# BB#52:
	movq	spiral_search_x(%rip), %rax
	movslq	%esi, %rcx
	movzwl	(%rax,%rcx,2), %eax
	addw	%ax, (%r11)
	movq	spiral_search_y(%rip), %rax
	movzwl	(%rax,%rcx,2), %eax
	addw	%ax, (%rdi)
.LBB2_53:                               # %._crit_edge.thread
	movl	%r10d, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	SubPelBlockMotionSearch, .Lfunc_end2-SubPelBlockMotionSearch
	.cfi_endproc

	.globl	SubPelBlockSearchBiPred
	.p2align	4, 0x90
	.type	SubPelBlockSearchBiPred,@function
SubPelBlockSearchBiPred:                # @SubPelBlockSearchBiPred
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi48:
	.cfi_def_cfa_offset 160
.Lcfi49:
	.cfi_offset %rbx, -56
.Lcfi50:
	.cfi_offset %r12, -48
.Lcfi51:
	.cfi_offset %r13, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movl	208(%rsp), %eax
	movq	192(%rsp), %r10
	movq	active_pps(%rip), %rdi
	movl	196(%rdi), %ebx
	movl	%ebx, 28(%rsp)          # 4-byte Spill
	movq	img(%rip), %rbp
	movq	14224(%rbp), %rdi
	movslq	12(%rbp), %rbp
	imulq	$536, %rbp, %rbp        # imm = 0x218
	movslq	432(%rdi,%rbp), %r13
	testl	%ebx, %ebx
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	je	.LBB3_1
# BB#2:
	movq	wp_offset(%rip), %rbx
	testl	%edx, %edx
	je	.LBB3_3
# BB#4:
	movq	8(%rbx,%r13,8), %rcx
	movq	(%rbx,%r13,8), %rdi
	jmp	.LBB3_5
.LBB3_1:
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	jmp	.LBB3_6
.LBB3_3:
	movswq	%si, %rdi
	shlq	$3, %rdi
	movq	(%rbx,%r13,8), %rcx
	addq	%rdi, %rcx
	addq	8(%rbx,%r13,8), %rdi
.LBB3_5:
	movq	(%rcx), %rcx
	movl	(%rcx), %r14d
	movq	(%rdi), %rcx
	movl	(%rcx), %r15d
.LBB3_6:
	movq	input(%rip), %rcx
	movslq	%r9d, %rdi
	movl	72(%rcx,%rdi,8), %ebp
	movl	76(%rcx,%rdi,8), %ecx
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	movslq	start_me_refinement_hp(%rip), %rcx
	testl	%eax, %eax
	movl	$1, %edi
	cmovgl	%eax, %edi
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	testq	%rcx, %rcx
	cmovnel	%eax, %edi
	movl	%edi, 24(%rsp)          # 4-byte Spill
	movswl	(%r10), %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	200(%rsp), %rax
	movswl	(%rax), %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leal	(%r13,%rdx), %eax
	cltq
	movq	listX(,%rax,8), %rax
	movswq	%si, %r8
	movq	(%rax,%r8,8), %rbx
	movl	%edx, %eax
	xorl	$1, %eax
	addl	%r13d, %eax
	cltq
	movq	listX(,%rax,8), %rax
	movq	(%rax), %r12
	movl	6392(%rbx), %ecx
	movl	6396(%rbx), %esi
	movq	232(%rsp), %rax
	movl	4(%rax), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movq	6448(%rbx), %rdi
	movq	%rdi, ref_pic1_sub(%rip)
	movq	6448(%r12), %rdi
	movq	%rdi, ref_pic2_sub(%rip)
	movw	%cx, img_width(%rip)
	movq	%rsi, %r11
	movw	%si, img_height(%rip)
	movl	6408(%rbx), %edi
	movl	%edi, width_pad(%rip)
	movl	6412(%rbx), %edi
	movl	%edi, height_pad(%rip)
	movl	$1, %edi
	movl	%ecx, %esi
	movl	%ebp, 60(%rsp)          # 4-byte Spill
	subl	%ebp, %esi
	movl	28(%rsp), %r10d         # 4-byte Reload
	testl	%r10d, %r10d
	je	.LBB3_11
# BB#7:
	movq	wbp_weight(%rip), %rcx
	testl	%edx, %edx
	je	.LBB3_8
# BB#9:
	movq	8(%rcx,%r13,8), %rdi
	movq	(%rdi), %rax
	leaq	(,%r8,8), %rdi
	movq	(%rax,%r8,8), %rax
	movzwl	(%rax), %eax
	movw	%ax, weight1(%rip)
	movq	(%rcx,%r13,8), %rax
	addq	(%rax), %rdi
	jmp	.LBB3_10
.LBB3_11:
	movb	luma_log_weight_denom(%rip), %cl
	shll	%cl, %edi
	movw	%di, weight1(%rip)
	movw	%di, weight2(%rip)
	movl	$computeBiPred1+8, %edi
	xorl	%ecx, %ecx
	movq	%r11, %rbp
	movq	40(%rsp), %r9           # 8-byte Reload
	jmp	.LBB3_12
.LBB3_8:
	movq	(%rcx,%r13,8), %rdi
	movq	(%rdi,%r8,8), %rdi
	movq	(%rdi), %rdi
	movzwl	(%rdi), %edi
	movw	%di, weight1(%rip)
	movq	8(%rcx,%r13,8), %rcx
	movq	(%rcx,%r8,8), %rdi
.LBB3_10:
	movq	%r11, %rbp
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	(%rdi), %rax
	movzwl	(%rax), %eax
	movw	%ax, weight2(%rip)
	movswl	%r14w, %eax
	movswl	%r15w, %ecx
	leal	1(%rax,%rcx), %ecx
	shrl	%ecx
	movl	$computeBiPred2+8, %edi
.LBB3_12:
	subl	36(%rsp), %ebp          # 4-byte Folded Reload
	movq	64(%rsp), %r15          # 8-byte Reload
	leal	80(,%r15,4), %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leal	(,%rsi,4), %r14d
	movw	%cx, offsetBi(%rip)
	movq	(%rdi), %rax
	movq	%rax, computeBiPred(%rip)
	cmpl	$0, ChromaMEEnable(%rip)
	je	.LBB3_23
# BB#13:
	movq	6464(%rbx), %rax
	movq	(%rax), %rcx
	movq	%rcx, ref_pic1_sub+8(%rip)
	movq	8(%rax), %rax
	movq	%rax, ref_pic1_sub+16(%rip)
	movq	6464(%r12), %rax
	movq	(%rax), %rcx
	movq	%rcx, ref_pic2_sub+8(%rip)
	movq	8(%rax), %rax
	movq	%rax, ref_pic2_sub+16(%rip)
	movl	6416(%rbx), %eax
	movl	%eax, width_pad_cr(%rip)
	movl	6420(%rbx), %eax
	movl	%eax, height_pad_cr(%rip)
	testl	%r10d, %r10d
	je	.LBB3_21
# BB#14:
	movq	wbp_weight(%rip), %rax
	testl	%edx, %edx
	je	.LBB3_15
# BB#16:
	movq	8(%rax,%r13,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movzwl	4(%rcx), %edi
	movw	%di, weight1_cr(%rip)
	movzwl	8(%rcx), %ecx
	movw	%cx, weight1_cr+2(%rip)
	movq	(%rax,%r13,8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r8,8), %rax
	movzwl	4(%rax), %ecx
	movw	%cx, weight2_cr(%rip)
	movzwl	8(%rax), %eax
	movw	%ax, weight2_cr+2(%rip)
	movq	wp_offset(%rip), %rax
	movq	8(%rax,%r13,8), %rdi
	movq	(%rax,%r13,8), %rcx
	jmp	.LBB3_17
.LBB3_21:
	movb	chroma_log_weight_denom(%rip), %cl
	movl	$1, %eax
	shll	%cl, %eax
	movw	%ax, weight1_cr(%rip)
	movw	%ax, weight1_cr+2(%rip)
	movw	%ax, weight2_cr(%rip)
	movw	%ax, weight2_cr+2(%rip)
	movw	$0, offsetBi_cr(%rip)
	xorl	%eax, %eax
	jmp	.LBB3_22
.LBB3_15:
	movq	(%rax,%r13,8), %rdi
	leaq	(,%r8,8), %rcx
	movq	(%rdi,%r8,8), %rdi
	movq	(%rdi), %rdi
	movzwl	4(%rdi), %ebx
	movw	%bx, weight1_cr(%rip)
	movzwl	8(%rdi), %edi
	movw	%di, weight1_cr+2(%rip)
	movq	8(%rax,%r13,8), %rax
	movq	(%rax,%r8,8), %rax
	movq	(%rax), %rax
	movzwl	4(%rax), %edi
	movw	%di, weight2_cr(%rip)
	movzwl	8(%rax), %eax
	movw	%ax, weight2_cr+2(%rip)
	movq	wp_offset(%rip), %rax
	movq	(%rax,%r13,8), %rdi
	addq	%rcx, %rdi
	addq	8(%rax,%r13,8), %rcx
.LBB3_17:
	movq	(%rdi), %rdi
	movl	4(%rdi), %edi
	movq	(%rcx), %rcx
	movl	4(%rcx), %ecx
	leal	1(%rdi,%rcx), %ecx
	shrl	%ecx
	movw	%cx, offsetBi_cr(%rip)
	testl	%edx, %edx
	je	.LBB3_18
# BB#19:
	movq	8(%rax,%r13,8), %rcx
	movq	(%rax,%r13,8), %r8
	jmp	.LBB3_20
.LBB3_18:
	shlq	$3, %r8
	movq	(%rax,%r13,8), %rcx
	addq	%r8, %rcx
	addq	8(%rax,%r13,8), %r8
.LBB3_20:
	movq	(%rcx), %rax
	movl	8(%rax), %eax
	movq	(%r8), %rcx
	movl	8(%rcx), %ecx
	leal	1(%rax,%rcx), %eax
	shrl	%eax
.LBB3_22:
	movw	%ax, offsetBi_cr+2(%rip)
.LBB3_23:
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	184(%rsp), %r8
	leal	80(,%r9,4), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	leal	(,%rbp,4), %r12d
	movq	176(%rsp), %rax
	movswl	(%rax), %ecx
	movq	48(%rsp), %rax          # 8-byte Reload
	leal	(%rcx,%rax), %edi
	addl	$159, %r14d
	movl	$1, %r11d
	cmpl	$2, %edi
	movl	$1, %esi
	jl	.LBB3_27
# BB#24:
	cmpl	%r14d, %edi
	movl	$1, %esi
	jge	.LBB3_27
# BB#25:
	movswl	(%r8), %edi
	addl	12(%rsp), %edi          # 4-byte Folded Reload
	cmpl	$2, %edi
	movl	$1, %esi
	jl	.LBB3_27
# BB#26:
	leal	159(%r12), %ebx
	xorl	%esi, %esi
	cmpl	%ebx, %edi
	setge	%sil
.LBB3_27:
	movl	%esi, bipred2_access_method(%rip)
	movq	192(%rsp), %rbx
	movswl	(%rbx), %esi
	addl	48(%rsp), %esi          # 4-byte Folded Reload
	cmpl	$2, %esi
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	jl	.LBB3_28
# BB#29:
	cmpl	%r14d, %esi
	jge	.LBB3_28
# BB#30:
	movq	200(%rsp), %rsi
	movswl	(%rsi), %esi
	addl	12(%rsp), %esi          # 4-byte Folded Reload
	cmpl	$2, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	24(%rsp), %ebp          # 4-byte Reload
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	72(%rsp), %rax          # 8-byte Reload
	jl	.LBB3_32
# BB#31:
	addl	$159, %r12d
	xorl	%r11d, %r11d
	cmpl	%r12d, %esi
	setge	%r11b
	jmp	.LBB3_32
.LBB3_28:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	24(%rsp), %ebp          # 4-byte Reload
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	72(%rsp), %rax          # 8-byte Reload
.LBB3_32:
	movl	224(%rsp), %esi
	movl	%esi, (%rsp)            # 4-byte Spill
	movzwl	168(%rsp), %r14d
	movzwl	160(%rsp), %r12d
	leal	80(%rdx,%r15,4), %edx
	movl	%edx, 64(%rsp)          # 4-byte Spill
	leal	80(%rax,%r9,4), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	%r11d, bipred1_access_method(%rip)
	cmpl	%ebp, %edi
	jge	.LBB3_33
# BB#34:                                # %.lr.ph314
	movswl	%r12w, %r13d
	movswl	%r14w, %r14d
	movq	%rdi, %r12
	addq	%r12, %r12
	decl	%ebp
	xorl	%r10d, %r10d
	jmp	.LBB3_35
	.p2align	4, 0x90
.LBB3_38:                               # %._crit_edge322
                                        #   in Loop: Header=BB3_35 Depth=1
	movq	176(%rsp), %rax
	movw	(%rax), %cx
	addq	$2, %r12
	incl	%edi
.LBB3_35:                               # =>This Inner Loop Header: Depth=1
	movswl	%cx, %ecx
	movq	spiral_hpel_search_x(%rip), %rax
	movswl	(%rax,%r12), %eax
	addl	%ecx, %eax
	movswl	(%r8), %ecx
	movq	spiral_hpel_search_y(%rip), %rdx
	movswl	(%rdx,%r12), %ebx
	addl	%ecx, %ebx
	movq	mvbits(%rip), %rcx
	movl	%eax, %edx
	subl	%r13d, %edx
	movslq	%edx, %rdx
	movl	%ebx, %esi
	subl	%r14d, %esi
	movslq	%esi, %rsi
	movl	(%rcx,%rsi,4), %r15d
	addl	(%rcx,%rdx,4), %r15d
	imull	32(%rsp), %r15d         # 4-byte Folded Reload
	sarl	$16, %r15d
	movl	(%rsp), %ecx            # 4-byte Reload
	subl	%r15d, %ecx
	jle	.LBB3_37
# BB#36:                                #   in Loop: Header=BB3_35 Depth=1
	addl	48(%rsp), %eax          # 4-byte Folded Reload
	addl	12(%rsp), %ebx          # 4-byte Folded Reload
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	96(%rsp), %rdi          # 8-byte Reload
	movl	36(%rsp), %esi          # 4-byte Reload
	movl	60(%rsp), %edx          # 4-byte Reload
	movl	64(%rsp), %r8d          # 4-byte Reload
	movl	40(%rsp), %r9d          # 4-byte Reload
	pushq	%rbx
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	movl	%r10d, %ebx
	callq	*computeBiPred(%rip)
	movl	%ebx, %r10d
	movq	200(%rsp), %r8
	movq	32(%rsp), %rdi          # 8-byte Reload
	addq	$16, %rsp
.Lcfi57:
	.cfi_adjust_cfa_offset -16
	addl	%r15d, %eax
	movl	(%rsp), %ecx            # 4-byte Reload
	cmpl	%ecx, %eax
	cmovll	%edi, %r10d
	cmovlel	%eax, %ecx
	movl	%ecx, (%rsp)            # 4-byte Spill
.LBB3_37:                               #   in Loop: Header=BB3_35 Depth=1
	cmpl	%edi, %ebp
	jne	.LBB3_38
# BB#39:                                # %._crit_edge315
	testl	%r10d, %r10d
	je	.LBB3_41
# BB#40:
	movq	spiral_hpel_search_x(%rip), %rax
	movslq	%r10d, %rcx
	movzwl	(%rax,%rcx,2), %eax
	movq	176(%rsp), %rdx
	addw	%ax, (%rdx)
	movq	spiral_hpel_search_y(%rip), %rax
	movzwl	(%rax,%rcx,2), %eax
	addw	%ax, (%r8)
.LBB3_41:                               # %._crit_edge315.thread
	movq	192(%rsp), %rbx
	movl	28(%rsp), %r10d         # 4-byte Reload
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	movzwl	168(%rsp), %r14d
	movzwl	160(%rsp), %r12d
	jmp	.LBB3_42
.LBB3_33:
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
.LBB3_42:                               # %._crit_edge315.thread
	leal	160(,%rcx,4), %esi
	leal	160(,%rax,4), %edx
	testl	%r10d, %r10d
	movl	$computeBiPred2+16, %eax
	movl	$computeBiPred1+16, %ecx
	cmovneq	%rax, %rcx
	movq	(%rcx), %rax
	movq	%rax, computeBiPred(%rip)
	movq	176(%rsp), %rax
	movswl	(%rax), %eax
	movl	$1, %ecx
	movl	%eax, %ebp
	addl	48(%rsp), %ebp          # 4-byte Folded Reload
	movl	$1, %edi
	jle	.LBB3_46
# BB#43:                                # %._crit_edge315.thread
	cmpl	%esi, %ebp
	movl	$1, %edi
	jge	.LBB3_46
# BB#44:
	movswl	(%r8), %ebp
	addl	12(%rsp), %ebp          # 4-byte Folded Reload
	movl	$1, %edi
	jle	.LBB3_46
# BB#45:
	xorl	%edi, %edi
	cmpl	%edx, %ebp
	setge	%dil
.LBB3_46:
	movl	216(%rsp), %r9d
	movl	%edi, bipred2_access_method(%rip)
	movswl	(%rbx), %edi
	addl	48(%rsp), %edi          # 4-byte Folded Reload
	jle	.LBB3_50
# BB#47:
	cmpl	%esi, %edi
	jge	.LBB3_50
# BB#48:
	movq	200(%rsp), %rsi
	movswl	(%rsi), %esi
	addl	12(%rsp), %esi          # 4-byte Folded Reload
	jle	.LBB3_50
# BB#49:
	xorl	%ecx, %ecx
	cmpl	%edx, %esi
	setge	%cl
.LBB3_50:
	movl	%ecx, bipred1_access_method(%rip)
	movl	start_me_refinement_qp(%rip), %ebp
	testl	%ebp, %ebp
	movl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	movl	(%rsp), %edx            # 4-byte Reload
	cmovel	%ecx, %edx
	movl	%edx, (%rsp)            # 4-byte Spill
	cmpl	%r9d, %ebp
	jge	.LBB3_58
# BB#51:                                # %.lr.ph
	movslq	%ebp, %r15
	movq	232(%rsp), %rcx
	movl	8(%rcx), %edi
	movswl	%r12w, %r10d
	movswl	%r14w, %r14d
	addq	%r15, %r15
	decl	%r9d
	xorl	%r13d, %r13d
	movl	%edi, 32(%rsp)          # 4-byte Spill
	jmp	.LBB3_52
	.p2align	4, 0x90
.LBB3_55:                               # %._crit_edge323
                                        #   in Loop: Header=BB3_52 Depth=1
	movq	176(%rsp), %rax
	movw	(%rax), %ax
	addq	$2, %r15
	incl	%ebp
.LBB3_52:                               # =>This Inner Loop Header: Depth=1
	movswl	%ax, %ecx
	movq	spiral_search_x(%rip), %rax
	movswl	(%rax,%r15), %eax
	addl	%ecx, %eax
	movswl	(%r8), %ecx
	movq	spiral_search_y(%rip), %rdx
	movswl	(%rdx,%r15), %ebx
	addl	%ecx, %ebx
	movq	mvbits(%rip), %rcx
	movl	%eax, %edx
	subl	%r10d, %edx
	movslq	%edx, %rdx
	movl	%ebx, %esi
	subl	%r14d, %esi
	movslq	%esi, %rsi
	movl	(%rcx,%rsi,4), %r12d
	addl	(%rcx,%rdx,4), %r12d
	imull	%edi, %r12d
	sarl	$16, %r12d
	movl	(%rsp), %ecx            # 4-byte Reload
	subl	%r12d, %ecx
	jle	.LBB3_54
# BB#53:                                #   in Loop: Header=BB3_52 Depth=1
	addl	48(%rsp), %eax          # 4-byte Folded Reload
	addl	12(%rsp), %ebx          # 4-byte Folded Reload
	movq	96(%rsp), %rdi          # 8-byte Reload
	movl	36(%rsp), %esi          # 4-byte Reload
	movl	60(%rsp), %edx          # 4-byte Reload
	movl	64(%rsp), %r8d          # 4-byte Reload
	movl	%ebp, 16(%rsp)          # 4-byte Spill
	movl	%r14d, %ebp
	movl	%r9d, %r14d
	movl	40(%rsp), %r9d          # 4-byte Reload
	pushq	%rbx
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	movl	%r10d, %ebx
	callq	*computeBiPred(%rip)
	movl	%ebx, %r10d
	movl	48(%rsp), %edi          # 4-byte Reload
	movl	16(%rsp), %ecx          # 4-byte Reload
	movl	%r14d, %r9d
	movl	%ebp, %r14d
	movl	32(%rsp), %ebp          # 4-byte Reload
	movq	200(%rsp), %r8
	addq	$16, %rsp
.Lcfi60:
	.cfi_adjust_cfa_offset -16
	addl	%r12d, %eax
	cmpl	%ecx, %eax
	cmovll	%ebp, %r13d
	cmovlel	%eax, %ecx
	movl	%ecx, (%rsp)            # 4-byte Spill
.LBB3_54:                               #   in Loop: Header=BB3_52 Depth=1
	cmpl	%ebp, %r9d
	jne	.LBB3_55
# BB#56:                                # %._crit_edge
	testl	%r13d, %r13d
	je	.LBB3_58
# BB#57:
	movq	spiral_search_x(%rip), %rax
	movslq	%r13d, %rcx
	movzwl	(%rax,%rcx,2), %eax
	movq	176(%rsp), %rdx
	addw	%ax, (%rdx)
	movq	spiral_search_y(%rip), %rax
	movzwl	(%rax,%rcx,2), %eax
	addw	%ax, (%r8)
.LBB3_58:                               # %._crit_edge.thread
	movl	(%rsp), %eax            # 4-byte Reload
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	SubPelBlockSearchBiPred, .Lfunc_end3-SubPelBlockSearchBiPred
	.cfi_endproc

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	width_pad,@object       # @width_pad
	.comm	width_pad,4,4
	.type	height_pad,@object      # @height_pad
	.comm	height_pad,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	width_pad_cr,@object    # @width_pad_cr
	.comm	width_pad_cr,4,4
	.type	height_pad_cr,@object   # @height_pad_cr
	.comm	height_pad_cr,4,4
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
