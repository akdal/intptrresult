	.text
	.file	"btSoftRigidDynamicsWorld.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	_ZN24btSoftRigidDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
	.p2align	4, 0x90
	.type	_ZN24btSoftRigidDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration,@function
_ZN24btSoftRigidDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration: # @_ZN24btSoftRigidDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	callq	_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
	movq	$_ZTV24btSoftRigidDynamicsWorld+16, (%r12)
	movb	$1, 400(%r12)
	movq	$0, 392(%r12)
	movl	$0, 380(%r12)
	movl	$0, 384(%r12)
	leaq	480(%r12), %rdi
	movb	$1, 504(%r12)
	movq	$0, 496(%r12)
	movl	$0, 484(%r12)
	movl	$0, 488(%r12)
	movl	$4302, 408(%r12)        # imm = 0x10CE
	movb	$1, 412(%r12)
	movb	$0, 413(%r12)
	movb	$0, 414(%r12)
	movq	%r14, 448(%r12)
	movq	%r15, 456(%r12)
.Ltmp0:
	movl	$2383, %esi             # imm = 0x94F
	callq	_ZN11btSparseSdfILi3EE10InitializeEi
.Ltmp1:
# BB#1:
	movl	484(%r12), %r14d
	testl	%r14d, %r14d
	jle	.LBB0_6
# BB#2:                                 # %.lr.ph21.i
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
	movq	496(%r12), %rax
	movq	(%rax,%r15,8), %rdi
	movq	$0, (%rax,%r15,8)
	testq	%rdi, %rdi
	je	.LBB0_5
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph.i
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	288(%rdi), %rbx
	callq	_ZdlPv
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB0_4
.LBB0_5:                                # %._crit_edge.i
                                        #   in Loop: Header=BB0_3 Depth=1
	incq	%r15
	cmpq	%r14, %r15
	jne	.LBB0_3
.LBB0_6:                                # %.loopexit
	movl	$1048576000, 512(%r12)  # imm = 0x3E800000
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,0,1,1]
	movups	%xmm0, 516(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB0_7:
.Ltmp2:
	movq	%rax, %r14
	movq	496(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB0_11
# BB#8:
	cmpb	$0, 504(%r12)
	je	.LBB0_10
# BB#9:
.Ltmp3:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
.LBB0_10:                               # %.noexc10
	movq	$0, 496(%r12)
.LBB0_11:
	movb	$1, 504(%r12)
	movq	$0, 496(%r12)
	movq	$0, 484(%r12)
	movq	392(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB0_15
# BB#12:
	cmpb	$0, 400(%r12)
	je	.LBB0_14
# BB#13:
.Ltmp5:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp6:
.LBB0_14:                               # %.noexc
	movq	$0, 392(%r12)
.LBB0_15:
	movb	$1, 400(%r12)
	movq	$0, 392(%r12)
	movq	$0, 380(%r12)
.Ltmp7:
	movq	%r12, %rdi
	callq	_ZN23btDiscreteDynamicsWorldD2Ev
.Ltmp8:
# BB#16:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_17:
.Ltmp9:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN24btSoftRigidDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration, .Lfunc_end0-_ZN24btSoftRigidDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp8-.Ltmp3           #   Call between .Ltmp3 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	1                       #   On action: 1
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp8      #   Call between .Ltmp8 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.section	.text._ZN11btSparseSdfILi3EE10InitializeEi,"axG",@progbits,_ZN11btSparseSdfILi3EE10InitializeEi,comdat
	.weak	_ZN11btSparseSdfILi3EE10InitializeEi
	.p2align	4, 0x90
	.type	_ZN11btSparseSdfILi3EE10InitializeEi,@function
_ZN11btSparseSdfILi3EE10InitializeEi:   # @_ZN11btSparseSdfILi3EE10InitializeEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 64
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	movl	4(%r15), %r13d
	cmpl	%r14d, %r13d
	jge	.LBB1_21
# BB#1:
	movslq	%r13d, %rbx
	cmpl	%r14d, 8(%r15)
	jge	.LBB1_2
# BB#3:
	testl	%r14d, %r14d
	je	.LBB1_4
# BB#5:
	movslq	%r14d, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r12
	movl	4(%r15), %eax
	jmp	.LBB1_6
.LBB1_2:                                # %..lr.ph.i_crit_edge
	leaq	16(%r15), %rbp
	jmp	.LBB1_16
.LBB1_4:
	xorl	%r12d, %r12d
	movl	%r13d, %eax
.LBB1_6:                                # %_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE8allocateEi.exit.i.i
	leaq	16(%r15), %rbp
	testl	%eax, %eax
	jle	.LBB1_11
# BB#7:                                 # %.lr.ph.i.i.i
	cltq
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB1_9
	.p2align	4, 0x90
.LBB1_8:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movq	(%rdi,%rcx,8), %rdi
	movq	%rdi, (%r12,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB1_8
.LBB1_9:                                # %.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB1_11
	.p2align	4, 0x90
.LBB1_10:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, (%r12,%rcx,8)
	movq	(%rbp), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movq	%rdx, 8(%r12,%rcx,8)
	movq	(%rbp), %rdx
	movq	16(%rdx,%rcx,8), %rdx
	movq	%rdx, 16(%r12,%rcx,8)
	movq	(%rbp), %rdx
	movq	24(%rdx,%rcx,8), %rdx
	movq	%rdx, 24(%r12,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB1_10
.LBB1_11:                               # %_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4copyEiiPS3_.exit.i.i
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB1_15
# BB#12:
	cmpb	$0, 24(%r15)
	je	.LBB1_14
# BB#13:
	callq	_Z21btAlignedFreeInternalPv
.LBB1_14:
	movq	$0, (%rbp)
.LBB1_15:                               # %_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7reserveEi.exit.preheader.i
	movb	$1, 24(%r15)
	movq	%r12, 16(%r15)
	movl	%r14d, 8(%r15)
.LBB1_16:                               # %.lr.ph.i
	movslq	%r14d, %rax
	movl	%r14d, %edx
	subl	%r13d, %edx
	leaq	-1(%rax), %rcx
	subq	%rbx, %rcx
	andq	$7, %rdx
	je	.LBB1_19
# BB#17:                                # %_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7reserveEi.exit.i.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB1_18:                               # %_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rsi
	movq	$0, (%rsi,%rbx,8)
	incq	%rbx
	incq	%rdx
	jne	.LBB1_18
.LBB1_19:                               # %_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7reserveEi.exit.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB1_21
	.p2align	4, 0x90
.LBB1_20:                               # %_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rcx
	movq	$0, (%rcx,%rbx,8)
	movq	(%rbp), %rcx
	movq	$0, 8(%rcx,%rbx,8)
	movq	(%rbp), %rcx
	movq	$0, 16(%rcx,%rbx,8)
	movq	(%rbp), %rcx
	movq	$0, 24(%rcx,%rbx,8)
	movq	(%rbp), %rcx
	movq	$0, 32(%rcx,%rbx,8)
	movq	(%rbp), %rcx
	movq	$0, 40(%rcx,%rbx,8)
	movq	(%rbp), %rcx
	movq	$0, 48(%rcx,%rbx,8)
	movq	(%rbp), %rcx
	movq	$0, 56(%rcx,%rbx,8)
	addq	$8, %rbx
	cmpq	%rbx, %rax
	jne	.LBB1_20
.LBB1_21:                               # %_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE6resizeEiRKS3_.exit
	movl	%r14d, 4(%r15)
	testl	%r14d, %r14d
	jle	.LBB1_26
# BB#22:                                # %.lr.ph21.i
	movl	%r14d, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_23:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_24 Depth 2
	movq	16(%r15), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	$0, (%rax,%rbp,8)
	testq	%rdi, %rdi
	je	.LBB1_25
	.p2align	4, 0x90
.LBB1_24:                               # %.lr.ph.i3
                                        #   Parent Loop BB1_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	288(%rdi), %rbx
	callq	_ZdlPv
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB1_24
.LBB1_25:                               # %._crit_edge.i
                                        #   in Loop: Header=BB1_23 Depth=1
	incq	%rbp
	cmpq	%r14, %rbp
	jne	.LBB1_23
.LBB1_26:                               # %_ZN11btSparseSdfILi3EE5ResetEv.exit
	movl	$1048576000, 32(%r15)   # imm = 0x3E800000
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [0,0,1,1]
	movups	%xmm0, 36(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN11btSparseSdfILi3EE10InitializeEi, .Lfunc_end1-_ZN11btSparseSdfILi3EE10InitializeEi
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN24btSoftRigidDynamicsWorldD2Ev
	.p2align	4, 0x90
	.type	_ZN24btSoftRigidDynamicsWorldD2Ev,@function
_ZN24btSoftRigidDynamicsWorldD2Ev:      # @_ZN24btSoftRigidDynamicsWorldD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -24
.Lcfi26:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV24btSoftRigidDynamicsWorld+16, (%rbx)
	movq	496(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#1:
	cmpb	$0, 504(%rbx)
	je	.LBB3_3
# BB#2:
.Ltmp10:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp11:
.LBB3_3:                                # %.noexc
	movq	$0, 496(%rbx)
.LBB3_4:
	movb	$1, 504(%rbx)
	movq	$0, 496(%rbx)
	movq	$0, 484(%rbx)
	movq	392(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_8
# BB#5:
	cmpb	$0, 400(%rbx)
	je	.LBB3_7
# BB#6:
.Ltmp15:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp16:
.LBB3_7:                                # %.noexc3
	movq	$0, 392(%rbx)
.LBB3_8:
	movb	$1, 400(%rbx)
	movq	$0, 392(%rbx)
	movq	$0, 380(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN23btDiscreteDynamicsWorldD2Ev # TAILCALL
.LBB3_14:
.Ltmp17:
	movq	%rax, %r14
	jmp	.LBB3_15
.LBB3_9:
.Ltmp12:
	movq	%rax, %r14
	movq	392(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_13
# BB#10:
	cmpb	$0, 400(%rbx)
	je	.LBB3_12
# BB#11:
.Ltmp13:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp14:
.LBB3_12:                               # %.noexc5
	movq	$0, 392(%rbx)
.LBB3_13:                               # %_ZN20btAlignedObjectArrayIP10btSoftBodyED2Ev.exit6
	movb	$1, 400(%rbx)
	movq	$0, 392(%rbx)
	movq	$0, 380(%rbx)
.LBB3_15:
.Ltmp18:
	movq	%rbx, %rdi
	callq	_ZN23btDiscreteDynamicsWorldD2Ev
.Ltmp19:
# BB#16:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_17:
.Ltmp20:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN24btSoftRigidDynamicsWorldD2Ev, .Lfunc_end3-_ZN24btSoftRigidDynamicsWorldD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin1   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin1   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp13-.Ltmp16         #   Call between .Ltmp16 and .Ltmp13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp19-.Ltmp13         #   Call between .Ltmp13 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin1   #     jumps to .Ltmp20
	.byte	1                       #   On action: 1
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end3-.Ltmp19     #   Call between .Ltmp19 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN24btSoftRigidDynamicsWorldD0Ev
	.p2align	4, 0x90
	.type	_ZN24btSoftRigidDynamicsWorldD0Ev,@function
_ZN24btSoftRigidDynamicsWorldD0Ev:      # @_ZN24btSoftRigidDynamicsWorldD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -24
.Lcfi31:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp21:
	callq	_ZN24btSoftRigidDynamicsWorldD2Ev
.Ltmp22:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB4_2:
.Ltmp23:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN24btSoftRigidDynamicsWorldD0Ev, .Lfunc_end4-_ZN24btSoftRigidDynamicsWorldD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin2   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end4-.Ltmp22     #   Call between .Ltmp22 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN24btSoftRigidDynamicsWorld25predictUnconstraintMotionEf
	.p2align	4, 0x90
	.type	_ZN24btSoftRigidDynamicsWorld25predictUnconstraintMotionEf,@function
_ZN24btSoftRigidDynamicsWorld25predictUnconstraintMotionEf: # @_ZN24btSoftRigidDynamicsWorld25predictUnconstraintMotionEf
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -24
.Lcfi36:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	callq	_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf
	cmpl	$0, 380(%r14)
	jle	.LBB5_3
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movq	392(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN10btSoftBody13predictMotionEf
	incq	%rbx
	movslq	380(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB5_2
.LBB5_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	_ZN24btSoftRigidDynamicsWorld25predictUnconstraintMotionEf, .Lfunc_end5-_ZN24btSoftRigidDynamicsWorld25predictUnconstraintMotionEf
	.cfi_endproc

	.globl	_ZN24btSoftRigidDynamicsWorld28internalSingleStepSimulationEf
	.p2align	4, 0x90
	.type	_ZN24btSoftRigidDynamicsWorld28internalSingleStepSimulationEf,@function
_ZN24btSoftRigidDynamicsWorld28internalSingleStepSimulationEf: # @_ZN24btSoftRigidDynamicsWorld28internalSingleStepSimulationEf
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 32
.Lcfi40:
	.cfi_offset %rbx, -24
.Lcfi41:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	callq	_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf
	movq	%r14, %rdi
	callq	_ZN24btSoftRigidDynamicsWorld26solveSoftBodiesConstraintsEv
	cmpl	$0, 380(%r14)
	jle	.LBB6_3
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movq	392(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	%rdi, %rsi
	callq	_ZN10btSoftBody23defaultCollisionHandlerEPS_
	incq	%rbx
	movslq	380(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB6_2
.LBB6_3:                                # %._crit_edge
	movl	$.L.str, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	cmpl	$0, 380(%r14)
	jle	.LBB6_7
# BB#4:                                 # %.lr.ph.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_5:                                # =>This Inner Loop Header: Depth=1
	movq	392(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
.Ltmp24:
	callq	_ZN10btSoftBody15integrateMotionEv
.Ltmp25:
# BB#6:                                 #   in Loop: Header=BB6_5 Depth=1
	incq	%rbx
	movslq	380(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB6_5
.LBB6_7:                                # %_ZN24btSoftRigidDynamicsWorld16updateSoftBodiesEv.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN15CProfileManager12Stop_ProfileEv # TAILCALL
.LBB6_8:
.Ltmp26:
	movq	%rax, %rbx
.Ltmp27:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp28:
# BB#9:                                 # %_ZN14CProfileSampleD2Ev.exit.i
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB6_10:
.Ltmp29:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN24btSoftRigidDynamicsWorld28internalSingleStepSimulationEf, .Lfunc_end6-_ZN24btSoftRigidDynamicsWorld28internalSingleStepSimulationEf
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp24-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin3   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp27-.Ltmp25         #   Call between .Ltmp25 and .Ltmp27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin3   #     jumps to .Ltmp29
	.byte	1                       #   On action: 1
	.long	.Ltmp28-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Lfunc_end6-.Ltmp28     #   Call between .Ltmp28 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN24btSoftRigidDynamicsWorld26solveSoftBodiesConstraintsEv
	.p2align	4, 0x90
	.type	_ZN24btSoftRigidDynamicsWorld26solveSoftBodiesConstraintsEv,@function
_ZN24btSoftRigidDynamicsWorld26solveSoftBodiesConstraintsEv: # @_ZN24btSoftRigidDynamicsWorld26solveSoftBodiesConstraintsEv
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 32
.Lcfi45:
	.cfi_offset %rbx, -24
.Lcfi46:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$.L.str.1, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	cmpl	$0, 380(%r14)
	je	.LBB7_6
# BB#1:
	leaq	376(%r14), %rdi
.Ltmp30:
	callq	_ZN10btSoftBody13solveClustersERK20btAlignedObjectArrayIPS_E
.Ltmp31:
# BB#2:                                 # %.preheader
	cmpl	$0, 380(%r14)
	jle	.LBB7_6
# BB#3:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_4:                                # =>This Inner Loop Header: Depth=1
	movq	392(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
.Ltmp33:
	callq	_ZN10btSoftBody16solveConstraintsEv
.Ltmp34:
# BB#5:                                 #   in Loop: Header=BB7_4 Depth=1
	incq	%rbx
	movslq	380(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB7_4
.LBB7_6:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN15CProfileManager12Stop_ProfileEv # TAILCALL
.LBB7_7:
.Ltmp32:
	jmp	.LBB7_9
.LBB7_8:
.Ltmp35:
.LBB7_9:
	movq	%rax, %rbx
.Ltmp36:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp37:
# BB#10:                                # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB7_11:
.Ltmp38:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN24btSoftRigidDynamicsWorld26solveSoftBodiesConstraintsEv, .Lfunc_end7-_ZN24btSoftRigidDynamicsWorld26solveSoftBodiesConstraintsEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp30-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin4   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin4   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp36-.Ltmp34         #   Call between .Ltmp34 and .Ltmp36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin4   #     jumps to .Ltmp38
	.byte	1                       #   On action: 1
	.long	.Ltmp37-.Lfunc_begin4   # >> Call Site 6 <<
	.long	.Lfunc_end7-.Ltmp37     #   Call between .Ltmp37 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN24btSoftRigidDynamicsWorld16updateSoftBodiesEv
	.p2align	4, 0x90
	.type	_ZN24btSoftRigidDynamicsWorld16updateSoftBodiesEv,@function
_ZN24btSoftRigidDynamicsWorld16updateSoftBodiesEv: # @_ZN24btSoftRigidDynamicsWorld16updateSoftBodiesEv
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi49:
	.cfi_def_cfa_offset 32
.Lcfi50:
	.cfi_offset %rbx, -24
.Lcfi51:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$.L.str, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	cmpl	$0, 380(%r14)
	jle	.LBB8_4
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	movq	392(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
.Ltmp39:
	callq	_ZN10btSoftBody15integrateMotionEv
.Ltmp40:
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	incq	%rbx
	movslq	380(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB8_2
.LBB8_4:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN15CProfileManager12Stop_ProfileEv # TAILCALL
.LBB8_5:
.Ltmp41:
	movq	%rax, %rbx
.Ltmp42:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp43:
# BB#6:                                 # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB8_7:
.Ltmp44:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN24btSoftRigidDynamicsWorld16updateSoftBodiesEv, .Lfunc_end8-_ZN24btSoftRigidDynamicsWorld16updateSoftBodiesEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp39-.Lfunc_begin5   #   Call between .Lfunc_begin5 and .Ltmp39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin5   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp42-.Ltmp40         #   Call between .Ltmp40 and .Ltmp42
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin5   #     jumps to .Ltmp44
	.byte	1                       #   On action: 1
	.long	.Ltmp43-.Lfunc_begin5   # >> Call Site 5 <<
	.long	.Lfunc_end8-.Ltmp43     #   Call between .Ltmp43 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN24btSoftRigidDynamicsWorld11addSoftBodyEP10btSoftBodyss
	.p2align	4, 0x90
	.type	_ZN24btSoftRigidDynamicsWorld11addSoftBodyEP10btSoftBodyss,@function
_ZN24btSoftRigidDynamicsWorld11addSoftBodyEP10btSoftBodyss: # @_ZN24btSoftRigidDynamicsWorld11addSoftBodyEP10btSoftBodyss
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi58:
	.cfi_def_cfa_offset 64
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %r15d
	movq	%rsi, %r12
	movq	%rdi, %rbp
	movl	380(%rbp), %eax
	cmpl	384(%rbp), %eax
	jne	.LBB9_15
# BB#1:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %r13d
	cmovnel	%ecx, %r13d
	cmpl	%r13d, %eax
	jge	.LBB9_15
# BB#2:
	testl	%r13d, %r13d
	je	.LBB9_3
# BB#4:
	movslq	%r13d, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	380(%rbp), %eax
	testl	%eax, %eax
	jg	.LBB9_6
	jmp	.LBB9_10
.LBB9_3:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB9_10
.LBB9_6:                                # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB9_8
	.p2align	4, 0x90
.LBB9_7:                                # =>This Inner Loop Header: Depth=1
	movq	392(%rbp), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB9_7
.LBB9_8:                                # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB9_10
	.p2align	4, 0x90
.LBB9_9:                                # =>This Inner Loop Header: Depth=1
	movq	392(%rbp), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	movq	392(%rbp), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%rbx,%rdx,8)
	movq	392(%rbp), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%rbx,%rdx,8)
	movq	392(%rbp), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%rbx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB9_9
.LBB9_10:                               # %_ZNK20btAlignedObjectArrayIP10btSoftBodyE4copyEiiPS1_.exit.i.i
	movq	392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_14
# BB#11:
	cmpb	$0, 400(%rbp)
	je	.LBB9_13
# BB#12:
	callq	_Z21btAlignedFreeInternalPv
	movl	380(%rbp), %eax
.LBB9_13:
	movq	$0, 392(%rbp)
.LBB9_14:                               # %_ZN20btAlignedObjectArrayIP10btSoftBodyE10deallocateEv.exit.i.i
	movb	$1, 400(%rbp)
	movq	%rbx, 392(%rbp)
	movl	%r13d, 384(%rbp)
.LBB9_15:                               # %_ZN20btAlignedObjectArrayIP10btSoftBodyE9push_backERKS1_.exit
	movq	392(%rbp), %rcx
	cltq
	movq	%r12, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, 380(%rbp)
	movswl	%r15w, %edx
	movswl	%r14w, %ecx
	movq	%rbp, %rdi
	movq	%r12, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectss # TAILCALL
.Lfunc_end9:
	.size	_ZN24btSoftRigidDynamicsWorld11addSoftBodyEP10btSoftBodyss, .Lfunc_end9-_ZN24btSoftRigidDynamicsWorld11addSoftBodyEP10btSoftBodyss
	.cfi_endproc

	.globl	_ZN24btSoftRigidDynamicsWorld14removeSoftBodyEP10btSoftBody
	.p2align	4, 0x90
	.type	_ZN24btSoftRigidDynamicsWorld14removeSoftBodyEP10btSoftBody,@function
_ZN24btSoftRigidDynamicsWorld14removeSoftBodyEP10btSoftBody: # @_ZN24btSoftRigidDynamicsWorld14removeSoftBodyEP10btSoftBody
	.cfi_startproc
# BB#0:
	movslq	380(%rdi), %r10
	testq	%r10, %r10
	jle	.LBB10_6
# BB#1:                                 # %.lr.ph.i.i
	movq	392(%rdi), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rsi, (%rcx,%rdx,8)
	je	.LBB10_4
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	incq	%rdx
	cmpq	%r10, %rdx
	jl	.LBB10_2
	jmp	.LBB10_6
.LBB10_4:                               # %_ZNK20btAlignedObjectArrayIP10btSoftBodyE16findLinearSearchERKS1_.exit.i
	cmpl	%r10d, %edx
	jge	.LBB10_6
# BB#5:
	leal	-1(%r10), %r8d
	movq	(%rcx,%rdx,8), %r9
	movq	-8(%rcx,%r10,8), %rax
	movq	%rax, (%rcx,%rdx,8)
	movq	392(%rdi), %rax
	movq	%r9, -8(%rax,%r10,8)
	movl	%r8d, 380(%rdi)
.LBB10_6:                               # %_ZN20btAlignedObjectArrayIP10btSoftBodyE6removeERKS1_.exit
	jmp	_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject # TAILCALL
.Lfunc_end10:
	.size	_ZN24btSoftRigidDynamicsWorld14removeSoftBodyEP10btSoftBody, .Lfunc_end10-_ZN24btSoftRigidDynamicsWorld14removeSoftBodyEP10btSoftBody
	.cfi_endproc

	.globl	_ZN24btSoftRigidDynamicsWorld21removeCollisionObjectEP17btCollisionObject
	.p2align	4, 0x90
	.type	_ZN24btSoftRigidDynamicsWorld21removeCollisionObjectEP17btCollisionObject,@function
_ZN24btSoftRigidDynamicsWorld21removeCollisionObjectEP17btCollisionObject: # @_ZN24btSoftRigidDynamicsWorld21removeCollisionObjectEP17btCollisionObject
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB11_9
# BB#1:
	cmpl	$4, 256(%rsi)
	jne	.LBB11_9
# BB#2:
	movslq	380(%rdi), %r10
	testq	%r10, %r10
	jle	.LBB11_8
# BB#3:                                 # %.lr.ph.i.i.i
	movq	392(%rdi), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB11_4:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rsi, (%rcx,%rdx,8)
	je	.LBB11_6
# BB#5:                                 #   in Loop: Header=BB11_4 Depth=1
	incq	%rdx
	cmpq	%r10, %rdx
	jl	.LBB11_4
	jmp	.LBB11_8
.LBB11_9:
	jmp	_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject # TAILCALL
.LBB11_6:                               # %_ZNK20btAlignedObjectArrayIP10btSoftBodyE16findLinearSearchERKS1_.exit.i.i
	cmpl	%r10d, %edx
	jge	.LBB11_8
# BB#7:
	leal	-1(%r10), %r8d
	movq	(%rcx,%rdx,8), %r9
	movq	-8(%rcx,%r10,8), %rax
	movq	%rax, (%rcx,%rdx,8)
	movq	392(%rdi), %rax
	movq	%r9, -8(%rax,%r10,8)
	movl	%r8d, 380(%rdi)
.LBB11_8:                               # %_ZN24btSoftRigidDynamicsWorld14removeSoftBodyEP10btSoftBody.exit
	jmp	_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject # TAILCALL
.Lfunc_end11:
	.size	_ZN24btSoftRigidDynamicsWorld21removeCollisionObjectEP17btCollisionObject, .Lfunc_end11-_ZN24btSoftRigidDynamicsWorld21removeCollisionObjectEP17btCollisionObject
	.cfi_endproc

	.globl	_ZN24btSoftRigidDynamicsWorld14debugDrawWorldEv
	.p2align	4, 0x90
	.type	_ZN24btSoftRigidDynamicsWorld14debugDrawWorldEv,@function
_ZN24btSoftRigidDynamicsWorld14debugDrawWorldEv: # @_ZN24btSoftRigidDynamicsWorld14debugDrawWorldEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 32
.Lcfi68:
	.cfi_offset %rbx, -32
.Lcfi69:
	.cfi_offset %r14, -24
.Lcfi70:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	callq	_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	testq	%rax, %rax
	je	.LBB12_12
# BB#1:                                 # %.preheader
	cmpl	$0, 380(%r15)
	jle	.LBB12_12
# BB#2:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_3:                               # =>This Inner Loop Header: Depth=1
	movq	120(%r15), %rsi
	movq	392(%r15), %rax
	movq	(%rax,%rbx,8), %r14
	movq	%r14, %rdi
	callq	_ZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDraw
	movq	120(%r15), %rsi
	movl	408(%r15), %edx
	movq	%r14, %rdi
	callq	_ZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawi
	movq	120(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB12_11
# BB#4:                                 #   in Loop: Header=BB12_3 Depth=1
	movq	(%rdi), %rax
	callq	*96(%rax)
	testb	$2, %al
	je	.LBB12_11
# BB#5:                                 #   in Loop: Header=BB12_3 Depth=1
	cmpb	$0, 412(%r15)
	je	.LBB12_7
# BB#6:                                 #   in Loop: Header=BB12_3 Depth=1
	movq	120(%r15), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r14, %rdi
	callq	_ZN17btSoftBodyHelpers12DrawNodeTreeEP10btSoftBodyP12btIDebugDrawii
.LBB12_7:                               #   in Loop: Header=BB12_3 Depth=1
	cmpb	$0, 413(%r15)
	je	.LBB12_9
# BB#8:                                 #   in Loop: Header=BB12_3 Depth=1
	movq	120(%r15), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r14, %rdi
	callq	_ZN17btSoftBodyHelpers12DrawFaceTreeEP10btSoftBodyP12btIDebugDrawii
.LBB12_9:                               #   in Loop: Header=BB12_3 Depth=1
	cmpb	$0, 414(%r15)
	je	.LBB12_11
# BB#10:                                #   in Loop: Header=BB12_3 Depth=1
	movq	120(%r15), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r14, %rdi
	callq	_ZN17btSoftBodyHelpers15DrawClusterTreeEP10btSoftBodyP12btIDebugDrawii
	.p2align	4, 0x90
.LBB12_11:                              #   in Loop: Header=BB12_3 Depth=1
	incq	%rbx
	movslq	380(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB12_3
.LBB12_12:                              # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	_ZN24btSoftRigidDynamicsWorld14debugDrawWorldEv, .Lfunc_end12-_ZN24btSoftRigidDynamicsWorld14debugDrawWorldEv
	.cfi_endproc

	.section	.text._ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw,"axG",@progbits,_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw,comdat
	.weak	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw,@function
_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw: # @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.cfi_startproc
# BB#0:
	movq	%rsi, 120(%rdi)
	retq
.Lfunc_end13:
	.size	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw, .Lfunc_end13-_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.cfi_endproc

	.section	.text._ZN16btCollisionWorld14getDebugDrawerEv,"axG",@progbits,_ZN16btCollisionWorld14getDebugDrawerEv,comdat
	.weak	_ZN16btCollisionWorld14getDebugDrawerEv
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld14getDebugDrawerEv,@function
_ZN16btCollisionWorld14getDebugDrawerEv: # @_ZN16btCollisionWorld14getDebugDrawerEv
	.cfi_startproc
# BB#0:
	movq	120(%rdi), %rax
	retq
.Lfunc_end14:
	.size	_ZN16btCollisionWorld14getDebugDrawerEv, .Lfunc_end14-_ZN16btCollisionWorld14getDebugDrawerEv
	.cfi_endproc

	.section	.text._ZNK23btDiscreteDynamicsWorld12getWorldTypeEv,"axG",@progbits,_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv,comdat
	.weak	_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv
	.p2align	4, 0x90
	.type	_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv,@function
_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv: # @_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv
	.cfi_startproc
# BB#0:
	movl	$2, %eax
	retq
.Lfunc_end15:
	.size	_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv, .Lfunc_end15-_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv
	.cfi_endproc

	.section	.text._ZN23btDiscreteDynamicsWorld11setNumTasksEi,"axG",@progbits,_ZN23btDiscreteDynamicsWorld11setNumTasksEi,comdat
	.weak	_ZN23btDiscreteDynamicsWorld11setNumTasksEi
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld11setNumTasksEi,@function
_ZN23btDiscreteDynamicsWorld11setNumTasksEi: # @_ZN23btDiscreteDynamicsWorld11setNumTasksEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end16:
	.size	_ZN23btDiscreteDynamicsWorld11setNumTasksEi, .Lfunc_end16-_ZN23btDiscreteDynamicsWorld11setNumTasksEi
	.cfi_endproc

	.section	.text._ZN23btDiscreteDynamicsWorld14updateVehiclesEf,"axG",@progbits,_ZN23btDiscreteDynamicsWorld14updateVehiclesEf,comdat
	.weak	_ZN23btDiscreteDynamicsWorld14updateVehiclesEf
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld14updateVehiclesEf,@function
_ZN23btDiscreteDynamicsWorld14updateVehiclesEf: # @_ZN23btDiscreteDynamicsWorld14updateVehiclesEf
	.cfi_startproc
# BB#0:
	jmp	_ZN23btDiscreteDynamicsWorld13updateActionsEf # TAILCALL
.Lfunc_end17:
	.size	_ZN23btDiscreteDynamicsWorld14updateVehiclesEf, .Lfunc_end17-_ZN23btDiscreteDynamicsWorld14updateVehiclesEf
	.cfi_endproc

	.type	_ZTV24btSoftRigidDynamicsWorld,@object # @_ZTV24btSoftRigidDynamicsWorld
	.section	.rodata,"a",@progbits
	.globl	_ZTV24btSoftRigidDynamicsWorld
	.p2align	3
_ZTV24btSoftRigidDynamicsWorld:
	.quad	0
	.quad	_ZTI24btSoftRigidDynamicsWorld
	.quad	_ZN24btSoftRigidDynamicsWorldD2Ev
	.quad	_ZN24btSoftRigidDynamicsWorldD0Ev
	.quad	_ZN16btCollisionWorld11updateAabbsEv
	.quad	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.quad	_ZN16btCollisionWorld14getDebugDrawerEv
	.quad	_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectss
	.quad	_ZN24btSoftRigidDynamicsWorld21removeCollisionObjectEP17btCollisionObject
	.quad	_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv
	.quad	_ZN23btDiscreteDynamicsWorld14stepSimulationEfif
	.quad	_ZN24btSoftRigidDynamicsWorld14debugDrawWorldEv
	.quad	_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb
	.quad	_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint
	.quad	_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface
	.quad	_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface
	.quad	_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3
	.quad	_ZNK23btDiscreteDynamicsWorld10getGravityEv
	.quad	_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv
	.quad	_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody
	.quad	_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody
	.quad	_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver
	.quad	_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv
	.quad	_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv
	.quad	_ZN23btDiscreteDynamicsWorld13getConstraintEi
	.quad	_ZNK23btDiscreteDynamicsWorld13getConstraintEi
	.quad	_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv
	.quad	_ZN23btDiscreteDynamicsWorld11clearForcesEv
	.quad	_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface
	.quad	_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface
	.quad	_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface
	.quad	_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface
	.quad	_ZN24btSoftRigidDynamicsWorld25predictUnconstraintMotionEf
	.quad	_ZN23btDiscreteDynamicsWorld19integrateTransformsEf
	.quad	_ZN23btDiscreteDynamicsWorld26calculateSimulationIslandsEv
	.quad	_ZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfo
	.quad	_ZN24btSoftRigidDynamicsWorld28internalSingleStepSimulationEf
	.quad	_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf
	.quad	_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyss
	.quad	_ZN23btDiscreteDynamicsWorld12applyGravityEv
	.quad	_ZN23btDiscreteDynamicsWorld11setNumTasksEi
	.quad	_ZN23btDiscreteDynamicsWorld14updateVehiclesEf
	.size	_ZTV24btSoftRigidDynamicsWorld, 336

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"updateSoftBodies"
	.size	.L.str, 17

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"solveSoftConstraints"
	.size	.L.str.1, 21

	.type	_ZTS24btSoftRigidDynamicsWorld,@object # @_ZTS24btSoftRigidDynamicsWorld
	.section	.rodata,"a",@progbits
	.globl	_ZTS24btSoftRigidDynamicsWorld
	.p2align	4
_ZTS24btSoftRigidDynamicsWorld:
	.asciz	"24btSoftRigidDynamicsWorld"
	.size	_ZTS24btSoftRigidDynamicsWorld, 27

	.type	_ZTI24btSoftRigidDynamicsWorld,@object # @_ZTI24btSoftRigidDynamicsWorld
	.globl	_ZTI24btSoftRigidDynamicsWorld
	.p2align	4
_ZTI24btSoftRigidDynamicsWorld:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS24btSoftRigidDynamicsWorld
	.quad	_ZTI23btDiscreteDynamicsWorld
	.size	_ZTI24btSoftRigidDynamicsWorld, 24


	.globl	_ZN24btSoftRigidDynamicsWorldC1EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
	.type	_ZN24btSoftRigidDynamicsWorldC1EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration,@function
_ZN24btSoftRigidDynamicsWorldC1EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration = _ZN24btSoftRigidDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
	.globl	_ZN24btSoftRigidDynamicsWorldD1Ev
	.type	_ZN24btSoftRigidDynamicsWorldD1Ev,@function
_ZN24btSoftRigidDynamicsWorldD1Ev = _ZN24btSoftRigidDynamicsWorldD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
