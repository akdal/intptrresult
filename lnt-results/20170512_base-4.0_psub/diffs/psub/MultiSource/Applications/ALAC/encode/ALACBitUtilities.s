	.text
	.file	"ALACBitUtilities.bc"
	.globl	BitBufferInit
	.p2align	4, 0x90
	.type	BitBufferInit,@function
BitBufferInit:                          # @BitBufferInit
	.cfi_startproc
# BB#0:
	movq	%rsi, (%rdi)
	movl	%edx, %eax
	addq	%rsi, %rax
	movq	%rax, 8(%rdi)
	movl	$0, 16(%rdi)
	movl	%edx, 20(%rdi)
	retq
.Lfunc_end0:
	.size	BitBufferInit, .Lfunc_end0-BitBufferInit
	.cfi_endproc

	.globl	BitBufferRead
	.p2align	4, 0x90
	.type	BitBufferRead,@function
BitBufferRead:                          # @BitBufferRead
	.cfi_startproc
# BB#0:
	movq	(%rdi), %r8
	movzbl	(%r8), %eax
	shll	$16, %eax
	movzbl	1(%r8), %ecx
	shll	$8, %ecx
	orl	%eax, %ecx
	movzbl	2(%r8), %eax
	orl	%ecx, %eax
	movl	16(%rdi), %edx
	movl	%edx, %ecx
	shll	%cl, %eax
	andl	$16777215, %eax         # imm = 0xFFFFFF
	addl	%esi, %edx
	movl	$24, %ecx
	subl	%esi, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	movl	%edx, %ecx
	shrl	$3, %ecx
	addq	%r8, %rcx
	movq	%rcx, (%rdi)
	andl	$7, %edx
	movl	%edx, 16(%rdi)
	retq
.Lfunc_end1:
	.size	BitBufferRead, .Lfunc_end1-BitBufferRead
	.cfi_endproc

	.globl	BitBufferReadSmall
	.p2align	4, 0x90
	.type	BitBufferReadSmall,@function
BitBufferReadSmall:                     # @BitBufferReadSmall
	.cfi_startproc
# BB#0:
	movq	(%rdi), %r8
	movzbl	(%r8), %ecx
	shll	$8, %ecx
	movzbl	1(%r8), %eax
	orl	%ecx, %eax
	movl	16(%rdi), %edx
	movl	%edx, %ecx
	shll	%cl, %eax
	addl	%esi, %edx
	movzwl	%ax, %eax
	movl	$16, %ecx
	subl	%esi, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	movl	%edx, %ecx
	shrl	$3, %ecx
	addq	%r8, %rcx
	movq	%rcx, (%rdi)
	andl	$7, %edx
	movl	%edx, 16(%rdi)
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end2:
	.size	BitBufferReadSmall, .Lfunc_end2-BitBufferReadSmall
	.cfi_endproc

	.globl	BitBufferReadOne
	.p2align	4, 0x90
	.type	BitBufferReadOne,@function
BitBufferReadOne:                       # @BitBufferReadOne
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rdx
	movzbl	(%rdx), %eax
	movl	16(%rdi), %esi
	movl	$7, %ecx
	subl	%esi, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	andb	$1, %al
	incl	%esi
	movl	%esi, %ecx
	shrl	$3, %ecx
	addq	%rdx, %rcx
	movq	%rcx, (%rdi)
	andl	$7, %esi
	movl	%esi, 16(%rdi)
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end3:
	.size	BitBufferReadOne, .Lfunc_end3-BitBufferReadOne
	.cfi_endproc

	.globl	BitBufferPeek
	.p2align	4, 0x90
	.type	BitBufferPeek,@function
BitBufferPeek:                          # @BitBufferPeek
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movzbl	(%rax), %ecx
	shll	$16, %ecx
	movzbl	1(%rax), %edx
	shll	$8, %edx
	orl	%ecx, %edx
	movzbl	2(%rax), %eax
	orl	%edx, %eax
	movb	16(%rdi), %cl
	shll	%cl, %eax
	andl	$16777215, %eax         # imm = 0xFFFFFF
	movl	$24, %ecx
	subl	%esi, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	retq
.Lfunc_end4:
	.size	BitBufferPeek, .Lfunc_end4-BitBufferPeek
	.cfi_endproc

	.globl	BitBufferPeekOne
	.p2align	4, 0x90
	.type	BitBufferPeekOne,@function
BitBufferPeekOne:                       # @BitBufferPeekOne
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movzbl	(%rax), %eax
	movl	$7, %ecx
	subl	16(%rdi), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	andl	$1, %eax
	retq
.Lfunc_end5:
	.size	BitBufferPeekOne, .Lfunc_end5-BitBufferPeekOne
	.cfi_endproc

	.globl	BitBufferUnpackBERSize
	.p2align	4, 0x90
	.type	BitBufferUnpackBERSize,@function
BitBufferUnpackBERSize:                 # @BitBufferUnpackBERSize
	.cfi_startproc
# BB#0:
	movq	(%rdi), %r8
	movl	16(%rdi), %ecx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%r8), %esi
	shll	$8, %esi
	movzbl	1(%r8), %edx
	orl	%esi, %edx
	shll	%cl, %edx
	addl	$8, %ecx
	shrl	$8, %edx
	movl	%ecx, %esi
	shrl	$3, %esi
	addq	%rsi, %r8
	movq	%r8, (%rdi)
	andl	$7, %ecx
	movl	%ecx, 16(%rdi)
	movl	%eax, %esi
	shll	$7, %esi
	movl	%edx, %eax
	andl	$127, %eax
	orl	%esi, %eax
	testb	%dl, %dl
	js	.LBB6_1
# BB#2:
	retq
.Lfunc_end6:
	.size	BitBufferUnpackBERSize, .Lfunc_end6-BitBufferUnpackBERSize
	.cfi_endproc

	.globl	BitBufferGetPosition
	.p2align	4, 0x90
	.type	BitBufferGetPosition,@function
BitBufferGetPosition:                   # @BitBufferGetPosition
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	movl	8(%rdi), %ecx
	subl	20(%rdi), %ecx
	subl	%ecx, %eax
	shll	$3, %eax
	addl	16(%rdi), %eax
	retq
.Lfunc_end7:
	.size	BitBufferGetPosition, .Lfunc_end7-BitBufferGetPosition
	.cfi_endproc

	.globl	BitBufferByteAlign
	.p2align	4, 0x90
	.type	BitBufferByteAlign,@function
BitBufferByteAlign:                     # @BitBufferByteAlign
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %ecx
	testl	%ecx, %ecx
	je	.LBB8_10
# BB#1:
	movl	$8, %r8d
	subl	%ecx, %r8d
	testl	%esi, %esi
	je	.LBB8_8
# BB#2:
	testl	%r8d, %r8d
	je	.LBB8_10
# BB#3:                                 # %.lr.ph.i
	movl	%r8d, %edx
	.p2align	4, 0x90
.LBB8_4:                                # =>This Inner Loop Header: Depth=1
	cmpl	%r8d, %edx
	movl	%r8d, %esi
	cmovbl	%edx, %esi
	subl	%esi, %r8d
	movl	$8, %ecx
	subl	%esi, %ecx
	movl	$255, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	movzbl	%al, %eax
	movl	%edx, %ecx
	subl	%esi, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movq	(%rdi), %rcx
	movzbl	(%rcx), %r9d
	notl	%eax
	andl	%r9d, %eax
	subl	%esi, %edx
	movb	%al, (%rcx)
	jne	.LBB8_6
# BB#5:                                 #   in Loop: Header=BB8_4 Depth=1
	incq	(%rdi)
	movl	$8, %edx
.LBB8_6:                                # %.backedge.i
                                        #   in Loop: Header=BB8_4 Depth=1
	testl	%r8d, %r8d
	jne	.LBB8_4
# BB#7:                                 # %._crit_edge.i
	movl	$8, %eax
	subl	%edx, %eax
	movl	%eax, 16(%rdi)
	retq
.LBB8_8:
	testl	%r8d, %r8d
	je	.LBB8_10
# BB#9:
	incq	(%rdi)
	movl	$0, 16(%rdi)
.LBB8_10:                               # %BitBufferWrite.exit
	retq
.Lfunc_end8:
	.size	BitBufferByteAlign, .Lfunc_end8-BitBufferByteAlign
	.cfi_endproc

	.globl	BitBufferWrite
	.p2align	4, 0x90
	.type	BitBufferWrite,@function
BitBufferWrite:                         # @BitBufferWrite
	.cfi_startproc
# BB#0:
	movl	%edx, %r8d
	testq	%rdi, %rdi
	je	.LBB9_7
# BB#1:
	testl	%r8d, %r8d
	je	.LBB9_7
# BB#2:                                 # %.lr.ph
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	$8, %r9d
	subl	16(%rdi), %r9d
	.p2align	4, 0x90
.LBB9_3:                                # =>This Inner Loop Header: Depth=1
	cmpl	%r8d, %r9d
	movl	%r8d, %r10d
	cmovbl	%r9d, %r10d
	subl	%r10d, %r8d
	movl	%esi, %edx
	movl	%r8d, %ecx
	shrl	%cl, %edx
	movl	$8, %ecx
	subl	%r10d, %ecx
	movl	$255, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	movzbl	%al, %eax
	movl	%r9d, %ecx
	subl	%r10d, %ecx
	andl	%eax, %edx
	shll	%cl, %eax
	movq	(%rdi), %r11
	movzbl	(%r11), %ebx
	notl	%eax
	andl	%ebx, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%eax, %edx
	subl	%r10d, %r9d
	movb	%dl, (%r11)
	jne	.LBB9_5
# BB#4:                                 #   in Loop: Header=BB9_3 Depth=1
	incq	(%rdi)
	movl	$8, %r9d
.LBB9_5:                                # %.backedge
                                        #   in Loop: Header=BB9_3 Depth=1
	testl	%r8d, %r8d
	jne	.LBB9_3
# BB#6:                                 # %._crit_edge
	movl	$8, %eax
	subl	%r9d, %eax
	movl	%eax, 16(%rdi)
	popq	%rbx
.LBB9_7:
	retq
.Lfunc_end9:
	.size	BitBufferWrite, .Lfunc_end9-BitBufferWrite
	.cfi_endproc

	.globl	BitBufferAdvance
	.p2align	4, 0x90
	.type	BitBufferAdvance,@function
BitBufferAdvance:                       # @BitBufferAdvance
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	je	.LBB10_2
# BB#1:
	addl	16(%rdi), %esi
	movl	%esi, %eax
	shrl	$3, %eax
	addq	%rax, (%rdi)
	andl	$7, %esi
	movl	%esi, 16(%rdi)
.LBB10_2:
	retq
.Lfunc_end10:
	.size	BitBufferAdvance, .Lfunc_end10-BitBufferAdvance
	.cfi_endproc

	.globl	BitBufferRewind
	.p2align	4, 0x90
	.type	BitBufferRewind,@function
BitBufferRewind:                        # @BitBufferRewind
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	je	.LBB11_7
# BB#1:
	movl	16(%rdi), %eax
	movl	%eax, %ecx
	subl	%esi, %ecx
	jae	.LBB11_2
# BB#3:
	subl	%eax, %esi
	movl	$0, 16(%rdi)
	movl	%esi, %ecx
	shrl	$3, %ecx
	movq	(%rdi), %rax
	subq	%rcx, %rax
	andl	$7, %esi
	movq	%rax, (%rdi)
	je	.LBB11_5
# BB#4:
	movl	$8, %ecx
	subl	%esi, %ecx
	movl	%ecx, 16(%rdi)
	decq	%rax
	movq	%rax, (%rdi)
.LBB11_5:
	movq	8(%rdi), %rcx
	movl	20(%rdi), %edx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jae	.LBB11_7
# BB#6:
	movq	%rcx, (%rdi)
	movl	$0, 16(%rdi)
.LBB11_7:
	retq
.LBB11_2:
	movl	%ecx, 16(%rdi)
	retq
.Lfunc_end11:
	.size	BitBufferRewind, .Lfunc_end11-BitBufferRewind
	.cfi_endproc

	.globl	BitBufferReset
	.p2align	4, 0x90
	.type	BitBufferReset,@function
BitBufferReset:                         # @BitBufferReset
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movl	20(%rdi), %ecx
	subq	%rcx, %rax
	movq	%rax, (%rdi)
	movl	$0, 16(%rdi)
	retq
.Lfunc_end12:
	.size	BitBufferReset, .Lfunc_end12-BitBufferReset
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
