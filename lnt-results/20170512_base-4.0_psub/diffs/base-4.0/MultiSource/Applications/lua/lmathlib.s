	.text
	.file	"lmathlib.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4614256656552045848     # double 3.1415926535897931
.LCPI0_1:
	.quad	9218868437227405312     # double +Inf
	.text
	.globl	luaopen_math
	.p2align	4, 0x90
	.type	luaopen_math,@function
luaopen_math:                           # @luaopen_math
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$.L.str, %esi
	movl	$mathlib, %edx
	callq	luaL_register
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$-2, %esi
	movl	$.L.str.1, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movsd	.LCPI0_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$-2, %esi
	movl	$.L.str.2, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movl	$-1, %esi
	movl	$.L.str.3, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-2, %esi
	movl	$.L.str.4, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	luaopen_math, .Lfunc_end0-luaopen_math
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.p2align	4, 0x90
	.type	math_abs,@function
math_abs:                               # @math_abs
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	andps	.LCPI1_0(%rip), %xmm0
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	math_abs, .Lfunc_end1-math_abs
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_acos,@function
math_acos:                              # @math_acos
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 16
.Lcfi5:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	callq	acos
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	math_acos, .Lfunc_end2-math_acos
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_asin,@function
math_asin:                              # @math_asin
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	callq	asin
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	math_asin, .Lfunc_end3-math_asin
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_atan2,@function
math_atan2:                             # @math_atan2
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	luaL_checknumber
	movapd	%xmm0, %xmm1
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	atan2
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end4:
	.size	math_atan2, .Lfunc_end4-math_atan2
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_atan,@function
math_atan:                              # @math_atan
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
.Lcfi12:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	callq	atan
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	math_atan, .Lfunc_end5-math_atan
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_ceil,@function
math_ceil:                              # @math_ceil
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	callq	ceil
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end6:
	.size	math_ceil, .Lfunc_end6-math_ceil
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_cosh,@function
math_cosh:                              # @math_cosh
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 16
.Lcfi16:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	callq	cosh
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end7:
	.size	math_cosh, .Lfunc_end7-math_cosh
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_cos,@function
math_cos:                               # @math_cos
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 16
.Lcfi18:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	callq	cos
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end8:
	.size	math_cos, .Lfunc_end8-math_cos
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI9_0:
	.quad	4580687790476533049     # double 0.017453292519943295
	.text
	.p2align	4, 0x90
	.type	math_deg,@function
math_deg:                               # @math_deg
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 16
.Lcfi20:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	divsd	.LCPI9_0(%rip), %xmm0
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end9:
	.size	math_deg, .Lfunc_end9-math_deg
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_exp,@function
math_exp:                               # @math_exp
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 16
.Lcfi22:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	callq	exp
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end10:
	.size	math_exp, .Lfunc_end10-math_exp
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_floor,@function
math_floor:                             # @math_floor
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 16
.Lcfi24:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	callq	floor
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end11:
	.size	math_floor, .Lfunc_end11-math_floor
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_fmod,@function
math_fmod:                              # @math_fmod
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	luaL_checknumber
	movapd	%xmm0, %xmm1
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	fmod
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end12:
	.size	math_fmod, .Lfunc_end12-math_fmod
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_frexp,@function
math_frexp:                             # @math_frexp
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	leaq	12(%rsp), %rdi
	callq	frexp
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movslq	12(%rsp), %rsi
	movq	%rbx, %rdi
	callq	lua_pushinteger
	movl	$2, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end13:
	.size	math_frexp, .Lfunc_end13-math_frexp
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_ldexp,@function
math_ldexp:                             # @math_ldexp
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	luaL_checkinteger
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movl	%eax, %edi
	callq	ldexp
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end14:
	.size	math_ldexp, .Lfunc_end14-math_ldexp
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_log10,@function
math_log10:                             # @math_log10
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 16
.Lcfi35:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	callq	log10
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end15:
	.size	math_log10, .Lfunc_end15-math_log10
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_log,@function
math_log:                               # @math_log
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 16
.Lcfi37:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	callq	log
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end16:
	.size	math_log, .Lfunc_end16-math_log
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_max,@function
math_max:                               # @math_max
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi41:
	.cfi_def_cfa_offset 48
.Lcfi42:
	.cfi_offset %rbx, -32
.Lcfi43:
	.cfi_offset %r14, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	lua_gettop
	movl	%eax, %ebp
	movl	$1, %ebx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	luaL_checknumber
	movapd	%xmm0, %xmm1
	cmpl	$2, %ebp
	jge	.LBB17_2
# BB#1:
	movapd	%xmm1, %xmm0
	jmp	.LBB17_3
	.p2align	4, 0x90
.LBB17_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	incl	%ebx
	movq	%r14, %rdi
	movl	%ebx, %esi
	callq	luaL_checknumber
	maxsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	cmpl	%ebx, %ebp
	movapd	%xmm0, %xmm1
	jne	.LBB17_2
.LBB17_3:                               # %._crit_edge
	movq	%r14, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end17:
	.size	math_max, .Lfunc_end17-math_max
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_min,@function
math_min:                               # @math_min
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi48:
	.cfi_def_cfa_offset 48
.Lcfi49:
	.cfi_offset %rbx, -32
.Lcfi50:
	.cfi_offset %r14, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	lua_gettop
	movl	%eax, %ebp
	movl	$1, %ebx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	luaL_checknumber
	movapd	%xmm0, %xmm1
	cmpl	$2, %ebp
	jge	.LBB18_2
# BB#1:
	movapd	%xmm1, %xmm0
	jmp	.LBB18_3
	.p2align	4, 0x90
.LBB18_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	incl	%ebx
	movq	%r14, %rdi
	movl	%ebx, %esi
	callq	luaL_checknumber
	minsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	cmpl	%ebx, %ebp
	movapd	%xmm0, %xmm1
	jne	.LBB18_2
.LBB18_3:                               # %._crit_edge
	movq	%r14, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end18:
	.size	math_min, .Lfunc_end18-math_min
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_modf,@function
math_modf:                              # @math_modf
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi53:
	.cfi_def_cfa_offset 32
.Lcfi54:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	leaq	8(%rsp), %rdi
	callq	modf
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	8(%rsp), %xmm0          # xmm0 = mem[0],zero
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movq	%rbx, %rdi
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	lua_pushnumber
	movl	$2, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end19:
	.size	math_modf, .Lfunc_end19-math_modf
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_pow,@function
math_pow:                               # @math_pow
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi56:
	.cfi_def_cfa_offset 32
.Lcfi57:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	luaL_checknumber
	movapd	%xmm0, %xmm1
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	pow
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end20:
	.size	math_pow, .Lfunc_end20-math_pow
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI21_0:
	.quad	4580687790476533049     # double 0.017453292519943295
	.text
	.p2align	4, 0x90
	.type	math_rad,@function
math_rad:                               # @math_rad
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 16
.Lcfi59:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	mulsd	.LCPI21_0(%rip), %xmm0
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end21:
	.size	math_rad, .Lfunc_end21-math_rad
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI22_0:
	.quad	4746794007244308480     # double 2147483647
.LCPI22_1:
	.quad	4607182418800017408     # double 1
	.text
	.p2align	4, 0x90
	.type	math_random,@function
math_random:                            # @math_random
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 48
.Lcfi65:
	.cfi_offset %rbx, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	callq	rand
	cltq
	movq	%rax, %rcx
	shlq	$30, %rcx
	addq	%rax, %rcx
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$61, %rcx
	addl	%edx, %ecx
	movl	%ecx, %edx
	shll	$31, %edx
	subl	%ecx, %edx
	subl	%edx, %eax
	cvtsi2sdl	%eax, %xmm0
	divsd	.LCPI22_0(%rip), %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rbx, %rdi
	callq	lua_gettop
	cmpl	$2, %eax
	je	.LBB22_4
# BB#1:
	cmpl	$1, %eax
	je	.LBB22_7
# BB#2:
	testl	%eax, %eax
	jne	.LBB22_12
# BB#3:
	movq	%rbx, %rdi
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	jmp	.LBB22_11
.LBB22_4:
	movl	$1, %ebp
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	luaL_checkinteger
	movq	%rax, %r14
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	luaL_checkinteger
	movq	%rax, %r15
	cmpl	%r14d, %r15d
	jge	.LBB22_6
# BB#5:
	movl	$2, %esi
	movl	$.L.str.32, %edx
	movq	%rbx, %rdi
	callq	luaL_argerror
.LBB22_6:
	subl	%r14d, %ebp
	addl	%r15d, %ebp
	cvtsi2sdl	%ebp, %xmm1
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	callq	floor
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r14d, %xmm1
	addsd	%xmm1, %xmm0
	jmp	.LBB22_10
.LBB22_7:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	luaL_checkinteger
	movq	%rax, %r14
	testl	%r14d, %r14d
	jg	.LBB22_9
# BB#8:
	movl	$1, %esi
	movl	$.L.str.32, %edx
	movq	%rbx, %rdi
	callq	luaL_argerror
.LBB22_9:
	cvtsi2sdl	%r14d, %xmm1
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	callq	floor
	addsd	.LCPI22_1(%rip), %xmm0
.LBB22_10:
	movq	%rbx, %rdi
.LBB22_11:
	callq	lua_pushnumber
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB22_12:
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	luaL_error              # TAILCALL
.Lfunc_end22:
	.size	math_random, .Lfunc_end22-math_random
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_randomseed,@function
math_randomseed:                        # @math_randomseed
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi69:
	.cfi_def_cfa_offset 16
	movl	$1, %esi
	callq	luaL_checkinteger
	movl	%eax, %edi
	callq	srand
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end23:
	.size	math_randomseed, .Lfunc_end23-math_randomseed
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_sinh,@function
math_sinh:                              # @math_sinh
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 16
.Lcfi71:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	callq	sinh
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end24:
	.size	math_sinh, .Lfunc_end24-math_sinh
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_sin,@function
math_sin:                               # @math_sin
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 16
.Lcfi73:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	callq	sin
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end25:
	.size	math_sin, .Lfunc_end25-math_sin
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_sqrt,@function
math_sqrt:                              # @math_sqrt
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 16
.Lcfi75:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	movapd	%xmm0, %xmm1
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB26_2
# BB#1:                                 # %call.sqrt
	movapd	%xmm1, %xmm0
	callq	sqrt
.LBB26_2:                               # %.split
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end26:
	.size	math_sqrt, .Lfunc_end26-math_sqrt
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_tanh,@function
math_tanh:                              # @math_tanh
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 16
.Lcfi77:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	callq	tanh
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end27:
	.size	math_tanh, .Lfunc_end27-math_tanh
	.cfi_endproc

	.p2align	4, 0x90
	.type	math_tan,@function
math_tan:                               # @math_tan
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 16
.Lcfi79:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	callq	tan
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end28:
	.size	math_tan, .Lfunc_end28-math_tan
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"math"
	.size	.L.str, 5

	.type	mathlib,@object         # @mathlib
	.section	.rodata,"a",@progbits
	.p2align	4
mathlib:
	.quad	.L.str.5
	.quad	math_abs
	.quad	.L.str.6
	.quad	math_acos
	.quad	.L.str.7
	.quad	math_asin
	.quad	.L.str.8
	.quad	math_atan2
	.quad	.L.str.9
	.quad	math_atan
	.quad	.L.str.10
	.quad	math_ceil
	.quad	.L.str.11
	.quad	math_cosh
	.quad	.L.str.12
	.quad	math_cos
	.quad	.L.str.13
	.quad	math_deg
	.quad	.L.str.14
	.quad	math_exp
	.quad	.L.str.15
	.quad	math_floor
	.quad	.L.str.3
	.quad	math_fmod
	.quad	.L.str.16
	.quad	math_frexp
	.quad	.L.str.17
	.quad	math_ldexp
	.quad	.L.str.18
	.quad	math_log10
	.quad	.L.str.19
	.quad	math_log
	.quad	.L.str.20
	.quad	math_max
	.quad	.L.str.21
	.quad	math_min
	.quad	.L.str.22
	.quad	math_modf
	.quad	.L.str.23
	.quad	math_pow
	.quad	.L.str.24
	.quad	math_rad
	.quad	.L.str.25
	.quad	math_random
	.quad	.L.str.26
	.quad	math_randomseed
	.quad	.L.str.27
	.quad	math_sinh
	.quad	.L.str.28
	.quad	math_sin
	.quad	.L.str.29
	.quad	math_sqrt
	.quad	.L.str.30
	.quad	math_tanh
	.quad	.L.str.31
	.quad	math_tan
	.zero	16
	.size	mathlib, 464

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"pi"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"huge"
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"fmod"
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"mod"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"abs"
	.size	.L.str.5, 4

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"acos"
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"asin"
	.size	.L.str.7, 5

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"atan2"
	.size	.L.str.8, 6

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"atan"
	.size	.L.str.9, 5

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"ceil"
	.size	.L.str.10, 5

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"cosh"
	.size	.L.str.11, 5

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"cos"
	.size	.L.str.12, 4

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"deg"
	.size	.L.str.13, 4

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"exp"
	.size	.L.str.14, 4

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"floor"
	.size	.L.str.15, 6

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"frexp"
	.size	.L.str.16, 6

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"ldexp"
	.size	.L.str.17, 6

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"log10"
	.size	.L.str.18, 6

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"log"
	.size	.L.str.19, 4

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"max"
	.size	.L.str.20, 4

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"min"
	.size	.L.str.21, 4

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"modf"
	.size	.L.str.22, 5

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"pow"
	.size	.L.str.23, 4

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"rad"
	.size	.L.str.24, 4

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"random"
	.size	.L.str.25, 7

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"randomseed"
	.size	.L.str.26, 11

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"sinh"
	.size	.L.str.27, 5

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"sin"
	.size	.L.str.28, 4

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"sqrt"
	.size	.L.str.29, 5

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"tanh"
	.size	.L.str.30, 5

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"tan"
	.size	.L.str.31, 4

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"interval is empty"
	.size	.L.str.32, 18

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"wrong number of arguments"
	.size	.L.str.33, 26


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
