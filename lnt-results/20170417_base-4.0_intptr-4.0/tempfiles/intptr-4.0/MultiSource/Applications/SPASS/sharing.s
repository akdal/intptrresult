	.text
	.file	"sharing.bc"
	.globl	sharing_IndexCreate
	.p2align	4, 0x90
	.type	sharing_IndexCreate,@function
sharing_IndexCreate:                    # @sharing_IndexCreate
	.cfi_startproc
# BB#0:                                 # %.preheader.preheader
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	$56024, %edi            # imm = 0xDAD8
	callq	memory_Malloc
	movq	%rax, %rbx
	callq	st_IndexCreate
	movq	%rax, (%rbx)
	leaq	8(%rbx), %rdi
	xorl	%esi, %esi
	movl	$56008, %edx            # imm = 0xDAC8
	callq	memset
	callq	term_GetStampID
	movl	%eax, 56016(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	sharing_IndexCreate, .Lfunc_end0-sharing_IndexCreate
	.cfi_endproc

	.globl	sharing_IndexDelete
	.p2align	4, 0x90
	.type	sharing_IndexDelete,@function
sharing_IndexDelete:                    # @sharing_IndexDelete
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	callq	st_IndexDelete
	movl	memory_ALIGN(%rip), %ecx
	movl	$56024, %esi            # imm = 0xDAD8
	movl	$56024, %eax            # imm = 0xDAD8
	xorl	%edx, %edx
	divl	%ecx
	addl	$56024, %ecx            # imm = 0xDAD8
	subl	%edx, %ecx
	testl	%edx, %edx
	cmovel	%esi, %ecx
	movl	memory_OFFSET(%rip), %eax
	movq	%rbx, %rdx
	subq	%rax, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %r8
	testq	%rsi, %rsi
	leaq	8(%rsi), %rsi
	movl	$memory_BIGBLOCKS, %edi
	cmovneq	%rsi, %rdi
	movq	%r8, (%rdi)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB1_2
# BB#1:
	negq	%rax
	movq	-16(%rbx,%rax), %rax
	movq	%rax, (%rdx)
.LBB1_2:
	addl	memory_MARKSIZE(%rip), %ecx
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rcx), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB1_4
# BB#3:
	leaq	16(%rcx,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB1_4:                                # %memory_Free.exit
	addq	$-16, %rbx
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end1:
	.size	sharing_IndexDelete, .Lfunc_end1-sharing_IndexDelete
	.cfi_endproc

	.globl	sharing_PushOnStack
	.p2align	4, 0x90
	.type	sharing_PushOnStack,@function
sharing_PushOnStack:                    # @sharing_PushOnStack
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 16
.Lcfi5:
	.cfi_offset %rbx, -16
	movl	stack_POINTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, stack_POINTER(%rip)
	movq	%rdi, stack_STACK(,%rax,8)
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB2_3
	.p2align	4, 0x90
.LBB2_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	sharing_PushOnStack
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_1
.LBB2_3:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end2:
	.size	sharing_PushOnStack, .Lfunc_end2-sharing_PushOnStack
	.cfi_endproc

	.globl	sharing_PushReverseOnStack
	.p2align	4, 0x90
	.type	sharing_PushReverseOnStack,@function
sharing_PushReverseOnStack:             # @sharing_PushReverseOnStack
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpl	$0, (%r14)
	jg	.LBB3_5
# BB#1:
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB3_4
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	sharing_PushReverseOnStack
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_2
.LBB3_4:                                # %._crit_edge
	movl	stack_POINTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, stack_POINTER(%rip)
	movq	%r14, stack_STACK(,%rax,8)
.LBB3_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	sharing_PushReverseOnStack, .Lfunc_end3-sharing_PushReverseOnStack
	.cfi_endproc

	.globl	sharing_PushReverseOnStackExcept
	.p2align	4, 0x90
	.type	sharing_PushReverseOnStackExcept,@function
sharing_PushReverseOnStackExcept:       # @sharing_PushReverseOnStackExcept
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	cmpl	$0, (%r14)
	jg	.LBB4_9
# BB#1:
	testq	%r15, %r15
	je	.LBB4_5
# BB#2:                                 # %.lr.ph.i.i.preheader
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	term_Equal
	testl	%eax, %eax
	jne	.LBB4_9
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB4_3
.LBB4_5:                                # %.loopexit
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB4_8
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	callq	sharing_PushReverseOnStackExcept
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB4_6
.LBB4_8:                                # %._crit_edge
	movl	stack_POINTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, stack_POINTER(%rip)
	movq	%r14, stack_STACK(,%rax,8)
.LBB4_9:                                # %term_ListContainsTerm.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	sharing_PushReverseOnStackExcept, .Lfunc_end4-sharing_PushReverseOnStackExcept
	.cfi_endproc

	.globl	sharing_PushOnStackNoStamps
	.p2align	4, 0x90
	.type	sharing_PushOnStackNoStamps,@function
sharing_PushOnStackNoStamps:            # @sharing_PushOnStackNoStamps
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 16
.Lcfi18:
	.cfi_offset %rbx, -16
	cmpl	$0, (%rdi)
	jg	.LBB5_5
# BB#1:
	movl	term_STAMP(%rip), %eax
	cmpl	24(%rdi), %eax
	je	.LBB5_5
# BB#2:
	movl	stack_POINTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, stack_POINTER(%rip)
	movq	%rdi, stack_STACK(,%rax,8)
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB5_5
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	sharing_PushOnStackNoStamps
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB5_3
.LBB5_5:                                # %.loopexit
	popq	%rbx
	retq
.Lfunc_end5:
	.size	sharing_PushOnStackNoStamps, .Lfunc_end5-sharing_PushOnStackNoStamps
	.cfi_endproc

	.globl	sharing_PushListOnStack
	.p2align	4, 0x90
	.type	sharing_PushListOnStack,@function
sharing_PushListOnStack:                # @sharing_PushListOnStack
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 16
.Lcfi20:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB6_3
	.p2align	4, 0x90
.LBB6_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	sharing_PushOnStack
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB6_1
.LBB6_3:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end6:
	.size	sharing_PushListOnStack, .Lfunc_end6-sharing_PushListOnStack
	.cfi_endproc

	.globl	sharing_PushListReverseOnStack
	.p2align	4, 0x90
	.type	sharing_PushListReverseOnStack,@function
sharing_PushListReverseOnStack:         # @sharing_PushListReverseOnStack
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 16
.Lcfi22:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB7_3
	.p2align	4, 0x90
.LBB7_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	sharing_PushReverseOnStack
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB7_1
.LBB7_3:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end7:
	.size	sharing_PushListReverseOnStack, .Lfunc_end7-sharing_PushListReverseOnStack
	.cfi_endproc

	.globl	sharing_PushListReverseOnStackExcept
	.p2align	4, 0x90
	.type	sharing_PushListReverseOnStackExcept,@function
sharing_PushListReverseOnStackExcept:   # @sharing_PushListReverseOnStackExcept
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB8_3
	.p2align	4, 0x90
.LBB8_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	callq	sharing_PushReverseOnStackExcept
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_1
.LBB8_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	sharing_PushListReverseOnStackExcept, .Lfunc_end8-sharing_PushListReverseOnStackExcept
	.cfi_endproc

	.globl	sharing_PushListOnStackNoStamps
	.p2align	4, 0x90
	.type	sharing_PushListOnStackNoStamps,@function
sharing_PushListOnStackNoStamps:        # @sharing_PushListOnStackNoStamps
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
.Lcfi29:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB9_3
	.p2align	4, 0x90
.LBB9_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	sharing_PushOnStackNoStamps
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB9_1
.LBB9_3:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end9:
	.size	sharing_PushListOnStackNoStamps, .Lfunc_end9-sharing_PushListOnStackNoStamps
	.cfi_endproc

	.globl	sharing_Insert
	.p2align	4, 0x90
	.type	sharing_Insert,@function
sharing_Insert:                         # @sharing_Insert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 80
.Lcfi37:
	.cfi_offset %rbx, -56
.Lcfi38:
	.cfi_offset %r12, -48
.Lcfi39:
	.cfi_offset %r13, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movq	%rdi, (%rsp)            # 8-byte Spill
	movl	stack_POINTER(%rip), %r15d
	movq	%rsi, %rdi
	callq	sharing_PushOnStack
	movl	stack_POINTER(%rip), %eax
	cmpl	%r15d, %eax
	jne	.LBB10_3
# BB#1:                                 # %.._crit_edge143_crit_edge.i
	decl	%r15d
	movq	stack_STACK(,%r15,8), %rax
	movq	8(%rax), %r12
	jmp	.LBB10_2
.LBB10_3:                               # %.lr.ph142.i
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	%r13, 8(%rsp)           # 8-byte Spill
	jmp	.LBB10_4
	.p2align	4, 0x90
.LBB10_8:                               #   in Loop: Header=BB10_4 Depth=1
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.LBB10_9
# BB#13:                                # %term_IsConstant.exit.thread.i
                                        #   in Loop: Header=BB10_4 Depth=1
	movq	8(%rax), %rcx
	movq	8(%rcx), %rcx
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB10_15
	jmp	.LBB10_19
.LBB10_17:                              #   in Loop: Header=BB10_15 Depth=2
	xorl	%eax, %eax
.LBB10_18:                              # %.critedge1.thread.i
                                        #   in Loop: Header=BB10_15 Depth=2
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB10_19
.LBB10_15:                              # %.lr.ph
                                        #   Parent Loop BB10_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_24 Depth 3
	movq	8(%rcx), %r12
	cmpl	%ebp, (%r12)
	jne	.LBB10_18
# BB#16:                                #   in Loop: Header=BB10_15 Depth=2
	testq	%rax, %rax
	je	.LBB10_17
# BB#23:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB10_15 Depth=2
	leaq	16(%r12), %rdx
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB10_24:                              # %.lr.ph.i
                                        #   Parent Loop BB10_4 Depth=1
                                        #     Parent Loop BB10_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdx), %rdx
	movq	8(%rsi), %rdi
	movq	8(%rdi), %rdi
	cmpq	8(%rdx), %rdi
	jne	.LBB10_18
# BB#25:                                #   in Loop: Header=BB10_24 Depth=3
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB10_24
	jmp	.LBB10_12
.LBB10_9:                               #   in Loop: Header=BB10_4 Depth=1
	movl	%ebp, %ebx
	negl	%ebx
	movl	symbol_TYPESTATBITS(%rip), %r12d
	movl	%r12d, %ecx
	sarl	%cl, %ebx
	cmpq	$0, 24016(%r13,%rbx,8)
	jne	.LBB10_11
# BB#10:                                #   in Loop: Header=BB10_4 Depth=1
	xorl	%esi, %esi
	movl	%ebp, %edi
	callq	term_Create
	movq	%rax, 24016(%r13,%rbx,8)
	movq	(%r13), %rdi
	xorl	%eax, %eax
	subl	(%r14), %eax
	movl	%r12d, %ecx
	sarl	%cl, %eax
	movq	24016(%r13,%rax,8), %rsi
	movq	cont_LEFTCONTEXT(%rip), %rcx
	movq	%rsi, %rdx
	callq	st_EntryCreate
	movl	(%r14), %ebp
.LBB10_11:                              #   in Loop: Header=BB10_4 Depth=1
	negl	%ebp
	movl	%r12d, %ecx
	sarl	%cl, %ebp
	movq	24016(%r13,%rbp,8), %r12
	jmp	.LBB10_12
.LBB10_19:                              # %.critedge.i
                                        #   in Loop: Header=BB10_4 Depth=1
	xorl	%esi, %esi
	movl	%ebp, %edi
	callq	term_Create
	movq	%rax, %r12
	movq	16(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB10_22
	.p2align	4, 0x90
.LBB10_20:                              #   Parent Loop BB10_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rax
	movq	8(%rax), %r15
	movq	16(%r12), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, 16(%r12)
	movq	8(%r15), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, 8(%r15)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB10_20
.LBB10_22:                              # %._crit_edge.i
                                        #   in Loop: Header=BB10_4 Depth=1
	movq	16(%r12), %rdi
	callq	list_NReverse
	movq	%rax, 16(%r12)
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	(%r13), %rdi
	movq	cont_LEFTCONTEXT(%rip), %rcx
	movq	%r12, %rsi
	movq	%r12, %rdx
	callq	st_EntryCreate
	movq	16(%rsp), %r15          # 8-byte Reload
	jmp	.LBB10_12
	.p2align	4, 0x90
.LBB10_4:                               # %.backedge._crit_edge.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_15 Depth 2
                                        #       Child Loop BB10_24 Depth 3
                                        #     Child Loop BB10_20 Depth 2
	decl	%eax
	movl	%eax, stack_POINTER(%rip)
	movq	stack_STACK(,%rax,8), %r14
	movl	(%r14), %ebp
	testl	%ebp, %ebp
	jle	.LBB10_8
# BB#5:                                 #   in Loop: Header=BB10_4 Depth=1
	cmpq	$0, 8(%r13,%rbp,8)
	jne	.LBB10_7
# BB#6:                                 #   in Loop: Header=BB10_4 Depth=1
	xorl	%esi, %esi
	movl	%ebp, %edi
	callq	term_Create
	movq	%rax, 8(%r13,%rbp,8)
	movq	(%r13), %rdi
	movl	(%r14), %eax
	movq	8(%r13,%rax,8), %rsi
	movq	cont_LEFTCONTEXT(%rip), %rcx
	movq	%rsi, %rdx
	callq	st_EntryCreate
	movl	(%r14), %ebp
.LBB10_7:                               #   in Loop: Header=BB10_4 Depth=1
	movl	%ebp, %eax
	movq	8(%r13,%rax,8), %r12
.LBB10_12:                              # %.backedge.i
                                        #   in Loop: Header=BB10_4 Depth=1
	movq	%r12, 8(%r14)
	movl	stack_POINTER(%rip), %eax
	cmpl	%r15d, %eax
	jne	.LBB10_4
.LBB10_2:                               # %sharing_InsertIntoSharing.exit
	movq	8(%r12), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, 8(%r12)
	movq	%r12, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	sharing_Insert, .Lfunc_end10-sharing_Insert
	.cfi_endproc

	.globl	sharing_Delete
	.p2align	4, 0x90
	.type	sharing_Delete,@function
sharing_Delete:                         # @sharing_Delete
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 32
.Lcfi46:
	.cfi_offset %rbx, -24
.Lcfi47:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %rax
	movq	8(%rbx), %rdi
	movq	%rax, %rsi
	callq	list_PointerDeleteElement
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.LBB11_2
# BB#1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB11_2:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	sharing_DeleteFromSharing # TAILCALL
.Lfunc_end11:
	.size	sharing_Delete, .Lfunc_end11-sharing_Delete
	.cfi_endproc

	.p2align	4, 0x90
	.type	sharing_DeleteFromSharing,@function
sharing_DeleteFromSharing:              # @sharing_DeleteFromSharing
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 48
.Lcfi53:
	.cfi_offset %rbx, -40
.Lcfi54:
	.cfi_offset %r12, -32
.Lcfi55:
	.cfi_offset %r14, -24
.Lcfi56:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	(%r14), %rdi
	movq	cont_LEFTCONTEXT(%rip), %rcx
	movq	%r15, %rsi
	movq	%r15, %rdx
	callq	st_EntryDelete
	movq	16(%r15), %r12
	testq	%r12, %r12
	je	.LBB12_4
	.p2align	4, 0x90
.LBB12_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rax
	movq	(%rax), %r12
	movq	8(%rax), %rbx
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	callq	list_PointerDeleteOneElement
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	jne	.LBB12_3
# BB#2:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	sharing_DeleteFromSharing
.LBB12_3:                               # %.backedge
                                        #   in Loop: Header=BB12_1 Depth=1
	testq	%r12, %r12
	jne	.LBB12_1
	jmp	.LBB12_8
.LBB12_4:                               # %term_IsConstant.exit
	movl	(%r15), %eax
	testl	%eax, %eax
	jle	.LBB12_5
# BB#6:                                 # %term_IsConstant.exit.thread
	leaq	8(%r14,%rax,8), %rax
	jmp	.LBB12_7
.LBB12_5:
	negl	%eax
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %eax
	leaq	24016(%r14,%rax,8), %rax
.LBB12_7:                               # %.sink.split
	movq	$0, (%rax)
.LBB12_8:                               # %.loopexit
	movq	8(%r15), %rax
	testq	%rax, %rax
	je	.LBB12_10
	.p2align	4, 0x90
.LBB12_9:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB12_9
.LBB12_10:                              # %list_Delete.exit
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%r15, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	sharing_DeleteFromSharing, .Lfunc_end12-sharing_DeleteFromSharing
	.cfi_endproc

	.globl	sharing_GetDataList
	.p2align	4, 0x90
	.type	sharing_GetDataList,@function
sharing_GetDataList:                    # @sharing_GetDataList
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 32
.Lcfi60:
	.cfi_offset %rbx, -32
.Lcfi61:
	.cfi_offset %r14, -24
.Lcfi62:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	56016(%r15), %edi
	callq	term_StampOverflow
	testl	%eax, %eax
	je	.LBB13_9
# BB#1:                                 # %.preheader.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB13_2:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB13_4
# BB#3:                                 #   in Loop: Header=BB13_2 Depth=1
	callq	sharing_ResetTermStamp
.LBB13_4:                               #   in Loop: Header=BB13_2 Depth=1
	incq	%rbx
	cmpq	$3002, %rbx             # imm = 0xBBA
	jne	.LBB13_2
# BB#5:                                 # %.preheader.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB13_6:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	24016(%r15,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB13_8
# BB#7:                                 #   in Loop: Header=BB13_6 Depth=1
	callq	sharing_ResetTermStamp
.LBB13_8:                               #   in Loop: Header=BB13_6 Depth=1
	incq	%rbx
	cmpq	$4000, %rbx             # imm = 0xFA0
	jne	.LBB13_6
.LBB13_9:                               # %sharing_ResetAllTermStamps.exit
	incl	term_STAMP(%rip)
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	sharing_InternGetDataList # TAILCALL
.Lfunc_end13:
	.size	sharing_GetDataList, .Lfunc_end13-sharing_GetDataList
	.cfi_endproc

	.globl	sharing_ResetAllTermStamps
	.p2align	4, 0x90
	.type	sharing_ResetAllTermStamps,@function
sharing_ResetAllTermStamps:             # @sharing_ResetAllTermStamps
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 32
.Lcfi66:
	.cfi_offset %rbx, -24
.Lcfi67:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB14_1:                               # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB14_3
# BB#2:                                 #   in Loop: Header=BB14_1 Depth=1
	callq	sharing_ResetTermStamp
.LBB14_3:                               #   in Loop: Header=BB14_1 Depth=1
	incq	%rbx
	cmpq	$3002, %rbx             # imm = 0xBBA
	jne	.LBB14_1
# BB#4:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB14_5:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	24016(%r14,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB14_7
# BB#6:                                 #   in Loop: Header=BB14_5 Depth=1
	callq	sharing_ResetTermStamp
.LBB14_7:                               #   in Loop: Header=BB14_5 Depth=1
	incq	%rbx
	cmpq	$4000, %rbx             # imm = 0xFA0
	jne	.LBB14_5
# BB#8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end14:
	.size	sharing_ResetAllTermStamps, .Lfunc_end14-sharing_ResetAllTermStamps
	.cfi_endproc

	.p2align	4, 0x90
	.type	sharing_InternGetDataList,@function
sharing_InternGetDataList:              # @sharing_InternGetDataList
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	testl	%eax, %eax
	jns	.LBB15_2
# BB#1:                                 # %term_IsAtom.exit
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	jne	.LBB15_2
# BB#14:
	movq	8(%rdi), %rdi
	jmp	list_Copy               # TAILCALL
.LBB15_2:                               # %term_IsAtom.exit.thread
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 32
.Lcfi71:
	.cfi_offset %rbx, -32
.Lcfi72:
	.cfi_offset %r14, -24
.Lcfi73:
	.cfi_offset %r15, -16
	movq	8(%rdi), %rbx
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	jne	.LBB15_4
	jmp	.LBB15_13
	.p2align	4, 0x90
.LBB15_12:                              #   in Loop: Header=BB15_4 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB15_13
.LBB15_4:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_9 Depth 2
	movq	8(%rbx), %r15
	movl	term_STAMP(%rip), %eax
	cmpl	24(%r15), %eax
	je	.LBB15_12
# BB#5:                                 #   in Loop: Header=BB15_4 Depth=1
	movq	%r15, %rdi
	callq	sharing_InternGetDataList
	testq	%rax, %rax
	je	.LBB15_6
# BB#7:                                 #   in Loop: Header=BB15_4 Depth=1
	testq	%r14, %r14
	je	.LBB15_11
# BB#8:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB15_4 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB15_9:                               # %.preheader.i
                                        #   Parent Loop BB15_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB15_9
# BB#10:                                #   in Loop: Header=BB15_4 Depth=1
	movq	%r14, (%rcx)
	jmp	.LBB15_11
.LBB15_6:                               #   in Loop: Header=BB15_4 Depth=1
	movq	%r14, %rax
.LBB15_11:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB15_4 Depth=1
	movl	term_STAMP(%rip), %ecx
	movl	%ecx, 24(%r15)
	movq	%rax, %r14
	jmp	.LBB15_12
.LBB15_13:                              # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	sharing_InternGetDataList, .Lfunc_end15-sharing_InternGetDataList
	.cfi_endproc

	.globl	sharing_StartDataIterator
	.p2align	4, 0x90
	.type	sharing_StartDataIterator,@function
sharing_StartDataIterator:              # @sharing_StartDataIterator
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 32
.Lcfi77:
	.cfi_offset %rbx, -32
.Lcfi78:
	.cfi_offset %r14, -24
.Lcfi79:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	56016(%r14), %edi
	callq	term_StampOverflow
	testl	%eax, %eax
	je	.LBB16_9
# BB#1:                                 # %.preheader.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB16_2:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB16_4
# BB#3:                                 #   in Loop: Header=BB16_2 Depth=1
	callq	sharing_ResetTermStamp
.LBB16_4:                               #   in Loop: Header=BB16_2 Depth=1
	incq	%rbx
	cmpq	$3002, %rbx             # imm = 0xBBA
	jne	.LBB16_2
# BB#5:                                 # %.preheader.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_6:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	24016(%r14,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB16_8
# BB#7:                                 #   in Loop: Header=BB16_6 Depth=1
	callq	sharing_ResetTermStamp
.LBB16_8:                               #   in Loop: Header=BB16_6 Depth=1
	incq	%rbx
	cmpq	$4000, %rbx             # imm = 0xFA0
	jne	.LBB16_6
.LBB16_9:                               # %sharing_ResetAllTermStamps.exit
	incl	term_STAMP(%rip)
	movq	$sharing_STACK, sharing_STACKPOINTER(%rip)
	movl	symbol_TYPEMASK(%rip), %eax
	movl	$sharing_STACK+8, %ecx
	jmp	.LBB16_10
	.p2align	4, 0x90
.LBB16_12:                              # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB16_10 Depth=1
	movq	8(%r15), %rdx
	movq	%rcx, sharing_STACKPOINTER(%rip)
	movq	%rdx, -8(%rcx)
	movq	8(%r15), %rdx
	movq	8(%rdx), %r15
	addq	$8, %rcx
.LBB16_10:                              # =>This Inner Loop Header: Depth=1
	movl	(%r15), %edx
	testl	%edx, %edx
	jns	.LBB16_12
# BB#11:                                # %term_IsAtom.exit
                                        #   in Loop: Header=BB16_10 Depth=1
	negl	%edx
	andl	%eax, %edx
	cmpl	$2, %edx
	jne	.LBB16_12
# BB#13:
	movq	8(%r15), %rax
	movq	%rax, sharing_DATALIST(%rip)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end16:
	.size	sharing_StartDataIterator, .Lfunc_end16-sharing_StartDataIterator
	.cfi_endproc

	.globl	sharing_GetNextData
	.p2align	4, 0x90
	.type	sharing_GetNextData,@function
sharing_GetNextData:                    # @sharing_GetNextData
	.cfi_startproc
# BB#0:
	movq	sharing_DATALIST(%rip), %rax
	testq	%rax, %rax
	je	.LBB17_3
.LBB17_1:                               # %.sink.split
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, sharing_DATALIST(%rip)
.LBB17_2:                               # %.critedge.thread
	retq
.LBB17_3:                               # %.critedge3.preheader
	movq	sharing_STACKPOINTER(%rip), %rsi
	movl	$sharing_STACK, %eax
	cmpq	%rax, %rsi
	jbe	.LBB17_20
# BB#4:                                 # %.preheader.lr.ph
	movl	symbol_TYPEMASK(%rip), %r8d
	movl	$sharing_STACK, %r9d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB17_6:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_7 Depth 2
                                        #       Child Loop BB17_8 Depth 3
                                        #     Child Loop BB17_12 Depth 2
                                        #       Child Loop BB17_16 Depth 3
	movl	term_STAMP(%rip), %ecx
.LBB17_7:                               #   Parent Loop BB17_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_8 Depth 3
	movq	%rsi, %rdi
	leaq	-8(%rdi), %rsi
	movq	%rsi, sharing_STACKPOINTER(%rip)
	movq	-8(%rdi), %rdi
	movq	8(%rdi), %rdx
	movl	%ecx, 24(%rdx)
	.p2align	4, 0x90
.LBB17_8:                               #   Parent Loop BB17_6 Depth=1
                                        #     Parent Loop BB17_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB17_10
# BB#9:                                 #   in Loop: Header=BB17_8 Depth=3
	movq	8(%rdi), %rdx
	cmpl	24(%rdx), %ecx
	je	.LBB17_8
	jmp	.LBB17_12
	.p2align	4, 0x90
.LBB17_10:                              # %.critedge1
                                        #   in Loop: Header=BB17_7 Depth=2
	cmpq	%r9, %rsi
	ja	.LBB17_7
	jmp	.LBB17_19
	.p2align	4, 0x90
.LBB17_11:                              #   in Loop: Header=BB17_12 Depth=2
	movq	%rcx, %rsi
.LBB17_12:                              # %.lr.ph49
                                        #   Parent Loop BB17_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_16 Depth 3
	movq	8(%rdi), %rcx
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jns	.LBB17_14
# BB#13:                                # %term_IsAtom.exit
                                        #   in Loop: Header=BB17_12 Depth=2
	negl	%ecx
	andl	%r8d, %ecx
	cmpl	$2, %ecx
	je	.LBB17_21
.LBB17_14:                              # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB17_12 Depth=2
	leaq	8(%rsi), %rcx
	movq	%rcx, sharing_STACKPOINTER(%rip)
	movq	%rdi, (%rsi)
	movq	8(%rdi), %rdx
	movq	8(%rdx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_18
# BB#15:                                # %.lr.ph
                                        #   in Loop: Header=BB17_12 Depth=2
	movl	term_STAMP(%rip), %esi
	.p2align	4, 0x90
.LBB17_16:                              #   Parent Loop BB17_6 Depth=1
                                        #     Parent Loop BB17_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rdi), %rdx
	cmpl	24(%rdx), %esi
	jne	.LBB17_11
# BB#17:                                #   in Loop: Header=BB17_16 Depth=3
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB17_16
.LBB17_18:                              #   in Loop: Header=BB17_6 Depth=1
	movq	%rcx, %rsi
.LBB17_19:                              # %.critedge3.loopexit
                                        #   in Loop: Header=BB17_6 Depth=1
	cmpq	%r9, %rsi
	ja	.LBB17_6
	jmp	.LBB17_2
.LBB17_20:
	xorl	%eax, %eax
	retq
.LBB17_21:                              # %.critedge
	leaq	8(%rsi), %rax
	movq	%rax, sharing_STACKPOINTER(%rip)
	movq	%rdi, (%rsi)
	movq	8(%rdi), %rax
	movq	8(%rax), %rax
	movq	%rax, sharing_DATALIST(%rip)
	jmp	.LBB17_1
.Lfunc_end17:
	.size	sharing_GetNextData, .Lfunc_end17-sharing_GetNextData
	.cfi_endproc

	.globl	sharing_StopDataIterator
	.p2align	4, 0x90
	.type	sharing_StopDataIterator,@function
sharing_StopDataIterator:               # @sharing_StopDataIterator
	.cfi_startproc
# BB#0:
	movq	$0, sharing_DATALIST(%rip)
	retq
.Lfunc_end18:
	.size	sharing_StopDataIterator, .Lfunc_end18-sharing_StopDataIterator
	.cfi_endproc

	.globl	sharing_NAtomDataList
	.p2align	4, 0x90
	.type	sharing_NAtomDataList,@function
sharing_NAtomDataList:                  # @sharing_NAtomDataList
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	retq
.Lfunc_end19:
	.size	sharing_NAtomDataList, .Lfunc_end19-sharing_NAtomDataList
	.cfi_endproc

	.globl	sharing_GetAllSuperTerms
	.p2align	4, 0x90
	.type	sharing_GetAllSuperTerms,@function
sharing_GetAllSuperTerms:               # @sharing_GetAllSuperTerms
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 32
.Lcfi83:
	.cfi_offset %rbx, -32
.Lcfi84:
	.cfi_offset %r14, -24
.Lcfi85:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	56016(%r15), %edi
	callq	term_StampOverflow
	testl	%eax, %eax
	je	.LBB20_9
# BB#1:                                 # %.preheader30.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB20_2:                               # %.preheader30
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB20_4
# BB#3:                                 #   in Loop: Header=BB20_2 Depth=1
	callq	sharing_ResetTermStamp
.LBB20_4:                               #   in Loop: Header=BB20_2 Depth=1
	incq	%rbx
	cmpq	$3002, %rbx             # imm = 0xBBA
	jne	.LBB20_2
# BB#5:                                 # %.preheader.i29.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB20_6:                               # %.preheader.i29
                                        # =>This Inner Loop Header: Depth=1
	movq	24016(%r15,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB20_8
# BB#7:                                 #   in Loop: Header=BB20_6 Depth=1
	callq	sharing_ResetTermStamp
.LBB20_8:                               #   in Loop: Header=BB20_6 Depth=1
	incq	%rbx
	cmpq	$4000, %rbx             # imm = 0xFA0
	jne	.LBB20_6
.LBB20_9:                               # %sharing_ResetAllTermStamps.exit
	incl	term_STAMP(%rip)
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB20_10:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_14 Depth 2
	movq	8(%r15,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB20_17
# BB#11:                                #   in Loop: Header=BB20_10 Depth=1
	callq	sharing_InternGetDataList
	testq	%rax, %rax
	je	.LBB20_17
# BB#12:                                #   in Loop: Header=BB20_10 Depth=1
	testq	%r14, %r14
	je	.LBB20_16
# BB#13:                                # %.preheader.i26.preheader
                                        #   in Loop: Header=BB20_10 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB20_14:                              # %.preheader.i26
                                        #   Parent Loop BB20_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB20_14
# BB#15:                                #   in Loop: Header=BB20_10 Depth=1
	movq	%r14, (%rcx)
.LBB20_16:                              # %list_Nconc.exit28
                                        #   in Loop: Header=BB20_10 Depth=1
	movq	%rax, %r14
.LBB20_17:                              # %list_Nconc.exit28
                                        #   in Loop: Header=BB20_10 Depth=1
	incq	%rbx
	cmpq	$3001, %rbx             # imm = 0xBB9
	jne	.LBB20_10
# BB#18:                                # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB20_19:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_23 Depth 2
	movq	24016(%r15,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB20_26
# BB#20:                                #   in Loop: Header=BB20_19 Depth=1
	callq	sharing_InternGetDataList
	testq	%rax, %rax
	je	.LBB20_26
# BB#21:                                #   in Loop: Header=BB20_19 Depth=1
	testq	%r14, %r14
	je	.LBB20_25
# BB#22:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB20_19 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB20_23:                              # %.preheader.i
                                        #   Parent Loop BB20_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB20_23
# BB#24:                                #   in Loop: Header=BB20_19 Depth=1
	movq	%r14, (%rcx)
.LBB20_25:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB20_19 Depth=1
	movq	%rax, %r14
.LBB20_26:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB20_19 Depth=1
	incq	%rbx
	cmpq	$4000, %rbx             # imm = 0xFA0
	jne	.LBB20_19
# BB#27:
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end20:
	.size	sharing_GetAllSuperTerms, .Lfunc_end20-sharing_GetAllSuperTerms
	.cfi_endproc

	.p2align	4, 0x90
	.type	sharing_ResetTermStamp,@function
sharing_ResetTermStamp:                 # @sharing_ResetTermStamp
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi88:
	.cfi_def_cfa_offset 32
.Lcfi89:
	.cfi_offset %rbx, -24
.Lcfi90:
	.cfi_offset %r14, -16
	movl	(%rdi), %eax
	testl	%eax, %eax
	jns	.LBB21_2
# BB#1:                                 # %term_IsAtom.exit
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	je	.LBB21_7
.LBB21_2:                               # %term_IsAtom.exit.thread
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	jne	.LBB21_4
	jmp	.LBB21_7
	.p2align	4, 0x90
.LBB21_6:                               #   in Loop: Header=BB21_4 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB21_7
.LBB21_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r14
	cmpl	$0, 24(%r14)
	je	.LBB21_6
# BB#5:                                 #   in Loop: Header=BB21_4 Depth=1
	movq	%r14, %rdi
	callq	sharing_ResetTermStamp
	movl	$0, 24(%r14)
	jmp	.LBB21_6
.LBB21_7:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end21:
	.size	sharing_ResetTermStamp, .Lfunc_end21-sharing_ResetTermStamp
	.cfi_endproc

	.globl	sharing_GetNumberOfOccurances
	.p2align	4, 0x90
	.type	sharing_GetNumberOfOccurances,@function
sharing_GetNumberOfOccurances:          # @sharing_GetNumberOfOccurances
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	testl	%eax, %eax
	jns	.LBB22_2
# BB#1:                                 # %term_IsAtom.exit
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	jne	.LBB22_2
# BB#6:
	movq	8(%rdi), %rdi
	jmp	list_Length             # TAILCALL
.LBB22_2:                               # %term_IsAtom.exit.thread
	pushq	%rbp
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi93:
	.cfi_def_cfa_offset 32
.Lcfi94:
	.cfi_offset %rbx, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movq	8(%rdi), %rbx
	xorl	%ebp, %ebp
	testq	%rbx, %rbx
	je	.LBB22_5
	.p2align	4, 0x90
.LBB22_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	sharing_GetNumberOfOccurances
	addl	%eax, %ebp
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB22_3
.LBB22_5:                               # %.loopexit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end22:
	.size	sharing_GetNumberOfOccurances, .Lfunc_end22-sharing_GetNumberOfOccurances
	.cfi_endproc

	.globl	sharing_GetNumberOfInstances
	.p2align	4, 0x90
	.type	sharing_GetNumberOfInstances,@function
sharing_GetNumberOfInstances:           # @sharing_GetNumberOfInstances
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 16
.Lcfi97:
	.cfi_offset %rbx, -16
	movq	%rdi, %rax
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	(%rsi), %rsi
	movq	%rax, %rdx
	callq	st_ExistInstance
	xorl	%ebx, %ebx
	testq	%rax, %rax
	je	.LBB23_3
	.p2align	4, 0x90
.LBB23_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rdi
	callq	sharing_GetNumberOfOccurances
	addl	%eax, %ebx
	callq	st_NextCandidate
	testq	%rax, %rax
	jne	.LBB23_1
.LBB23_3:                               # %._crit_edge
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end23:
	.size	sharing_GetNumberOfInstances, .Lfunc_end23-sharing_GetNumberOfInstances
	.cfi_endproc

	.globl	sharing_PrintVartable
	.p2align	4, 0x90
	.type	sharing_PrintVartable,@function
sharing_PrintVartable:                  # @sharing_PrintVartable
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi100:
	.cfi_def_cfa_offset 32
.Lcfi101:
	.cfi_offset %rbx, -24
.Lcfi102:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB24_1:                               # =>This Inner Loop Header: Depth=1
	cmpq	$0, 8(%r14,%rbx,8)
	je	.LBB24_3
# BB#2:                                 #   in Loop: Header=BB24_1 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movq	8(%r14,%rbx,8), %rdi
	callq	term_Print
.LBB24_3:                               #   in Loop: Header=BB24_1 Depth=1
	incq	%rbx
	cmpq	$3001, %rbx             # imm = 0xBB9
	jne	.LBB24_1
# BB#4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end24:
	.size	sharing_PrintVartable, .Lfunc_end24-sharing_PrintVartable
	.cfi_endproc

	.globl	sharing_PrintConsttable
	.p2align	4, 0x90
	.type	sharing_PrintConsttable,@function
sharing_PrintConsttable:                # @sharing_PrintConsttable
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi105:
	.cfi_def_cfa_offset 32
.Lcfi106:
	.cfi_offset %rbx, -24
.Lcfi107:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB25_1:                               # =>This Inner Loop Header: Depth=1
	cmpq	$0, 24016(%r14,%rbx,8)
	je	.LBB25_3
# BB#2:                                 #   in Loop: Header=BB25_1 Depth=1
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movq	24016(%r14,%rbx,8), %rdi
	callq	term_Print
.LBB25_3:                               #   in Loop: Header=BB25_1 Depth=1
	incq	%rbx
	cmpq	$4000, %rbx             # imm = 0xFA0
	jne	.LBB25_1
# BB#4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end25:
	.size	sharing_PrintConsttable, .Lfunc_end25-sharing_PrintConsttable
	.cfi_endproc

	.globl	sharing_PrintSharingConstterms1
	.p2align	4, 0x90
	.type	sharing_PrintSharingConstterms1,@function
sharing_PrintSharingConstterms1:        # @sharing_PrintSharingConstterms1
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 32
.Lcfi111:
	.cfi_offset %rbx, -32
.Lcfi112:
	.cfi_offset %r14, -24
.Lcfi113:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB26_1:                               # =>This Inner Loop Header: Depth=1
	movq	24016(%r14,%rbx,8), %r15
	testq	%r15, %r15
	je	.LBB26_3
# BB#2:                                 #   in Loop: Header=BB26_1 Depth=1
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movq	%r15, %rdi
	callq	term_Print
	movl	$.L.str.2, %edi
	callq	puts
	movq	8(%r15), %rdi
	callq	term_TermListPrint
.LBB26_3:                               #   in Loop: Header=BB26_1 Depth=1
	incq	%rbx
	cmpq	$4000, %rbx             # imm = 0xFA0
	jne	.LBB26_1
# BB#4:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end26:
	.size	sharing_PrintSharingConstterms1, .Lfunc_end26-sharing_PrintSharingConstterms1
	.cfi_endproc

	.globl	sharing_PrintSharingVarterms1
	.p2align	4, 0x90
	.type	sharing_PrintSharingVarterms1,@function
sharing_PrintSharingVarterms1:          # @sharing_PrintSharingVarterms1
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi114:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 32
.Lcfi117:
	.cfi_offset %rbx, -32
.Lcfi118:
	.cfi_offset %r14, -24
.Lcfi119:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB27_1:                               # =>This Inner Loop Header: Depth=1
	movq	8(%r14,%rbx,8), %r15
	testq	%r15, %r15
	je	.LBB27_3
# BB#2:                                 #   in Loop: Header=BB27_1 Depth=1
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movq	%r15, %rdi
	callq	term_Print
	movl	$.L.str.2, %edi
	callq	puts
	movq	8(%r15), %rdi
	callq	term_TermListPrint
.LBB27_3:                               #   in Loop: Header=BB27_1 Depth=1
	incq	%rbx
	cmpq	$3001, %rbx             # imm = 0xBB9
	jne	.LBB27_1
# BB#4:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end27:
	.size	sharing_PrintSharingVarterms1, .Lfunc_end27-sharing_PrintSharingVarterms1
	.cfi_endproc

	.globl	sharing_PrintSharing
	.p2align	4, 0x90
	.type	sharing_PrintSharing,@function
sharing_PrintSharing:                   # @sharing_PrintSharing
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi120:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi121:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi122:
	.cfi_def_cfa_offset 32
.Lcfi123:
	.cfi_offset %rbx, -24
.Lcfi124:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB28_1:                               # =>This Inner Loop Header: Depth=1
	movq	24016(%r14,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB28_3
# BB#2:                                 #   in Loop: Header=BB28_1 Depth=1
	callq	sharing_PrintWithSuperterms
	movl	$.L.str.4, %edi
	callq	puts
.LBB28_3:                               #   in Loop: Header=BB28_1 Depth=1
	incq	%rbx
	cmpq	$4000, %rbx             # imm = 0xFA0
	jne	.LBB28_1
# BB#4:
	movl	$.L.str.5, %edi
	callq	puts
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB28_5:                               # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB28_7
# BB#6:                                 #   in Loop: Header=BB28_5 Depth=1
	callq	sharing_PrintWithSuperterms
	movl	$.L.str.4, %edi
	callq	puts
.LBB28_7:                               #   in Loop: Header=BB28_5 Depth=1
	incq	%rbx
	cmpq	$3002, %rbx             # imm = 0xBBA
	jne	.LBB28_5
# BB#8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end28:
	.size	sharing_PrintSharing, .Lfunc_end28-sharing_PrintSharing
	.cfi_endproc

	.p2align	4, 0x90
	.type	sharing_PrintWithSuperterms,@function
sharing_PrintWithSuperterms:            # @sharing_PrintWithSuperterms
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi125:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi127:
	.cfi_def_cfa_offset 32
.Lcfi128:
	.cfi_offset %rbx, -24
.Lcfi129:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	(%rbx), %ebp
	testl	%ebp, %ebp
	js	.LBB29_2
# BB#1:                                 # %term_IsAtom.exit.thread
	movq	%rbx, %rdi
	callq	term_Print
	jmp	.LBB29_3
.LBB29_2:                               # %term_IsAtom.exit
	negl	%ebp
	andl	symbol_TYPEMASK(%rip), %ebp
	movq	%rbx, %rdi
	callq	term_Print
	cmpl	$2, %ebp
	jne	.LBB29_3
# BB#7:
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	_IO_putc                # TAILCALL
.LBB29_3:
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB29_6
# BB#4:                                 # %.lr.ph
	movq	stdout(%rip), %rsi
	movl	$91, %edi
	callq	_IO_putc
	movq	%rbx, %rdi
	callq	term_TermListPrint
	movl	$.L.str.6, %edi
	callq	puts
	.p2align	4, 0x90
.LBB29_5:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	sharing_PrintWithSuperterms
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB29_5
.LBB29_6:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end29:
	.size	sharing_PrintWithSuperterms, .Lfunc_end29-sharing_PrintWithSuperterms
	.cfi_endproc

	.globl	sharing_PrintSameLevelTerms
	.p2align	4, 0x90
	.type	sharing_PrintSameLevelTerms,@function
sharing_PrintSameLevelTerms:            # @sharing_PrintSameLevelTerms
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi130:
	.cfi_def_cfa_offset 16
.Lcfi131:
	.cfi_offset %rbx, -16
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	jne	.LBB30_2
	jmp	.LBB30_5
	.p2align	4, 0x90
.LBB30_4:                               #   in Loop: Header=BB30_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB30_5
.LBB30_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB30_4
# BB#3:                                 #   in Loop: Header=BB30_2 Depth=1
	callq	term_TermListPrint
	jmp	.LBB30_4
.LBB30_5:                               # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end30:
	.size	sharing_PrintSameLevelTerms, .Lfunc_end30-sharing_PrintSameLevelTerms
	.cfi_endproc

	.globl	sharing_PrintStack
	.p2align	4, 0x90
	.type	sharing_PrintStack,@function
sharing_PrintStack:                     # @sharing_PrintStack
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi134:
	.cfi_def_cfa_offset 32
.Lcfi135:
	.cfi_offset %rbx, -24
.Lcfi136:
	.cfi_offset %r14, -16
	movq	sharing_STACKPOINTER(%rip), %rbx
	movl	$sharing_STACK, %eax
	cmpq	%rax, %rbx
	jbe	.LBB31_3
# BB#1:                                 # %.lr.ph.preheader
	movl	$sharing_STACK, %r14d
	.p2align	4, 0x90
.LBB31_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rbx), %rax
	addq	$-8, %rbx
	movq	8(%rax), %rdi
	callq	term_Print
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	cmpq	%r14, %rbx
	ja	.LBB31_2
.LBB31_3:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end31:
	.size	sharing_PrintStack, .Lfunc_end31-sharing_PrintStack
	.cfi_endproc

	.type	sharing_STACK,@object   # @sharing_STACK
	.local	sharing_STACK
	.comm	sharing_STACK,4000,16
	.type	sharing_STACKPOINTER,@object # @sharing_STACKPOINTER
	.data
	.p2align	3
sharing_STACKPOINTER:
	.quad	sharing_STACK
	.size	sharing_STACKPOINTER, 8

	.type	sharing_DATALIST,@object # @sharing_DATALIST
	.local	sharing_DATALIST
	.comm	sharing_DATALIST,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n X%d   :  "
	.size	.L.str, 12

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\n c%d   :  "
	.size	.L.str.1, 12

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"   has the direct superterms : "
	.size	.L.str.2, 32

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n x%d   :  "
	.size	.L.str.3, 12

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n"
	.size	.L.str.4, 2

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"------------------------"
	.size	.L.str.5, 25

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"]"
	.size	.L.str.6, 2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
