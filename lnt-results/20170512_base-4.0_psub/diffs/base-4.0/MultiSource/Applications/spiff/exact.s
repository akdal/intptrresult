	.text
	.file	"exact.bc"
	.globl	Q_do_exact
	.p2align	4, 0x90
	.type	Q_do_exact,@function
Q_do_exact:                             # @Q_do_exact
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movl	%esi, %ebx
	movl	%edi, %ebp
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movslq	%edx, %rax
	leaq	8(,%rax,8), %rdi
	xorl	%eax, %eax
	callq	_Z_myalloc
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpl	%ebx, %ebp
	movl	%ebx, %eax
	cmovlel	%ebp, %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	je	.LBB0_2
# BB#1:
	movl	$Z_err_buf, %edi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	movl	%ebx, %ecx
	callq	sprintf
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_complain
.LBB0_2:                                # %.preheader91.preheader
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	movl	$0, 36(%rsp)            # 4-byte Folded Spill
	xorl	%r13d, %r13d
	movl	12(%rsp), %ebx          # 4-byte Reload
	movl	28(%rsp), %ebp          # 4-byte Reload
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader91
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
                                        #     Child Loop BB0_7 Depth 2
                                        #     Child Loop BB0_13 Depth 2
                                        #     Child Loop BB0_16 Depth 2
	movl	%r13d, %r14d
	cmpl	%ebp, %r13d
	jge	.LBB0_5
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%eax, %eax
	movl	%r14d, %edi
	movl	%r14d, %esi
	movl	%r12d, %edx
	callq	X_com
	testl	%eax, %eax
	jne	.LBB0_5
# BB#9:                                 #   in Loop: Header=BB0_4 Depth=2
	incl	%r14d
	cmpl	%ebp, %r14d
	jl	.LBB0_4
.LBB0_5:                                # %.critedge90.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpl	%ebp, %r14d
	jge	.LBB0_10
# BB#6:                                 #   in Loop: Header=BB0_3 Depth=1
	movl	%r14d, %r13d
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph97
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%eax, %eax
	movl	%r13d, %edi
	movl	%r13d, %esi
	movl	%r12d, %edx
	callq	X_com
	testl	%eax, %eax
	je	.LBB0_8
# BB#21:                                #   in Loop: Header=BB0_7 Depth=2
	movl	36(%rsp), %eax          # 4-byte Reload
	addl	$2, %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	cmpl	%ebx, %eax
	jle	.LBB0_23
# BB#22:                                #   in Loop: Header=BB0_7 Depth=2
	xorl	%eax, %eax
	movl	%ebx, %edi
	callq	Z_exceed
.LBB0_23:                               # %.critedge90
                                        #   in Loop: Header=BB0_7 Depth=2
	incl	%r13d
	cmpl	%ebp, %r13d
	jl	.LBB0_7
# BB#24:                                #   in Loop: Header=BB0_3 Depth=1
	xorl	%eax, %eax
	cmpl	%r13d, %r14d
	jl	.LBB0_12
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_3 Depth=1
	xorl	%eax, %eax
	movl	%r14d, %r13d
	cmpl	%r13d, %r14d
	jl	.LBB0_12
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_8:                                #   in Loop: Header=BB0_3 Depth=1
	movb	$1, %al
	cmpl	%r13d, %r14d
	jge	.LBB0_19
.LBB0_12:                               # %.critedge.preheader116
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movslq	16(%rsp), %rax          # 4-byte Folded Reload
	leal	-1(%r13), %ecx
	subl	%r14d, %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rbx
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB0_13:                               # %.critedge
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$24, %edi
	xorl	%eax, %eax
	callq	_Z_myalloc
	movq	%rax, (%rbx)
	movq	%r15, (%rax)
	movq	(%rbx), %r15
	movl	$2, 8(%r15)
	incl	%ebp
	movl	%ebp, 12(%r15)
	movl	$0, 16(%r15)
	addq	$8, %rbx
	cmpl	%ebp, %r13d
	jne	.LBB0_13
# BB#14:                                # %.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	leal	1(%rax,%rcx), %eax
	cmpl	%r13d, %r14d
	movl	28(%rsp), %ebp          # 4-byte Reload
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jge	.LBB0_18
# BB#15:                                # %.lr.ph113.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%r12d, %ebx
	cltq
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %r12
	.p2align	4, 0x90
.LBB0_16:                               # %.lr.ph113
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$24, %edi
	xorl	%eax, %eax
	callq	_Z_myalloc
	movq	%rax, (%r12)
	movq	%r15, (%rax)
	movq	(%r12), %r15
	movl	$1, 8(%r15)
	movl	%r13d, 12(%r15)
	incl	%r14d
	movl	%r14d, 16(%r15)
	addq	$8, %r12
	cmpl	%r14d, %r13d
	jne	.LBB0_16
# BB#17:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	leal	1(%rax,%rcx), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%ebx, %r12d
.LBB0_18:                               # %._crit_edge
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	12(%rsp), %ebx          # 4-byte Reload
	movl	32(%rsp), %eax          # 4-byte Reload
.LBB0_19:                               # %._crit_edge
                                        #   in Loop: Header=BB0_3 Depth=1
	testb	%al, %al
	jne	.LBB0_3
# BB#20:
	movq	%r15, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	Q_do_exact, .Lfunc_end0-Q_do_exact
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"unequal number of tokens, %d and %d respectively\n"
	.size	.L.str, 50


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
