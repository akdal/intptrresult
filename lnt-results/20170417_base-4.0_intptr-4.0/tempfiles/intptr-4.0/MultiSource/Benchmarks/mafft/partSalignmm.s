	.text
	.file	"partSalignmm.bc"
	.globl	part_imp_match_out_sc
	.p2align	4, 0x90
	.type	part_imp_match_out_sc,@function
part_imp_match_out_sc:                  # @part_imp_match_out_sc
	.cfi_startproc
# BB#0:
	movq	impmtx(%rip), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	%esi, %rcx
	movss	(%rax,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end0:
	.size	part_imp_match_out_sc, .Lfunc_end0-part_imp_match_out_sc
	.cfi_endproc

	.globl	part_imp_match_init_strict
	.p2align	4, 0x90
	.type	part_imp_match_init_strict,@function
part_imp_match_init_strict:             # @part_imp_match_init_strict
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movl	%r8d, %ebp
	movl	%ecx, %r12d
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	%esi, %r14d
	movl	impalloclen(%rip), %eax
	leal	2(%r12), %ecx
	cmpl	%ecx, %eax
	jle	.LBB1_2
# BB#1:
	leal	2(%rbp), %ecx
	cmpl	%ecx, %eax
	jg	.LBB1_5
.LBB1_2:
	movq	impmtx(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#3:
	callq	FreeFloatMtx
.LBB1_4:
	cmpl	%ebp, %r12d
	movl	%ebp, %edi
	cmovgel	%r12d, %edi
	leal	2(%rdi), %eax
	movl	%eax, impalloclen(%rip)
	addl	$102, %edi
	movl	%edi, %esi
	callq	AllocateFloatMtx
	movq	%rax, impmtx(%rip)
.LBB1_5:                                # %.preheader174
	testl	%r12d, %r12d
	jle	.LBB1_12
# BB#6:                                 # %.preheader173.lr.ph
	testl	%ebp, %ebp
	jle	.LBB1_12
# BB#7:                                 # %.preheader173.us.preheader
	movl	%r14d, 24(%rsp)         # 4-byte Spill
	movq	impmtx(%rip), %r13
	decl	%ebp
	leaq	4(,%rbp,4), %rbp
	movl	%r12d, %r15d
	leaq	-1(%r15), %r12
	movq	%r15, %r14
	xorl	%ebx, %ebx
	andq	$7, %r14
	je	.LBB1_9
	.p2align	4, 0x90
.LBB1_8:                                # %.preheader173.us.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB1_8
.LBB1_9:                                # %.preheader173.us.prol.loopexit
	cmpq	$7, %r12
	movl	24(%rsp), %r14d         # 4-byte Reload
	jb	.LBB1_12
# BB#10:                                # %.preheader173.us.preheader.new
	subq	%rbx, %r15
	leaq	56(%r13,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB1_11:                               # %.preheader173.us
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-48(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-40(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-32(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-24(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-16(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	addq	$64, %rbx
	addq	$-8, %r15
	jne	.LBB1_11
.LBB1_12:                               # %._crit_edge186
	testl	%r14d, %r14d
	jle	.LBB1_50
# BB#13:                                # %.preheader172.lr.ph
	movsd	fastathreshold(%rip), %xmm0 # xmm0 = mem[0],zero
	movq	impmtx(%rip), %rdi
	movl	12(%rsp), %eax          # 4-byte Reload
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%r14d, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_14:                               # %.preheader172
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_16 Depth 2
                                        #       Child Loop BB1_18 Depth 3
                                        #         Child Loop BB1_19 Depth 4
                                        #         Child Loop BB1_23 Depth 4
                                        #         Child Loop BB1_27 Depth 4
                                        #         Child Loop BB1_30 Depth 4
                                        #         Child Loop BB1_34 Depth 4
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_49
# BB#15:                                # %.lr.ph178
                                        #   in Loop: Header=BB1_14 Depth=1
	movq	120(%rsp), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movsd	(%rax,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movq	136(%rsp), %rax
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_16:                               #   Parent Loop BB1_14 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_18 Depth 3
                                        #         Child Loop BB1_19 Depth 4
                                        #         Child Loop BB1_23 Depth 4
                                        #         Child Loop BB1_27 Depth 4
                                        #         Child Loop BB1_30 Depth 4
                                        #         Child Loop BB1_34 Depth 4
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r12,8), %r14
	testq	%r14, %r14
	je	.LBB1_48
# BB#17:                                # %.lr.ph
                                        #   in Loop: Header=BB1_16 Depth=2
	movq	128(%rsp), %rax
	movsd	(%rax,%r12,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %r13
	.p2align	4, 0x90
.LBB1_18:                               #   Parent Loop BB1_14 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_19 Depth 4
                                        #         Child Loop BB1_23 Depth 4
                                        #         Child Loop BB1_27 Depth 4
                                        #         Child Loop BB1_30 Depth 4
                                        #         Child Loop BB1_34 Depth 4
	movl	$-1, %eax
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB1_19:                               #   Parent Loop BB1_14 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        #       Parent Loop BB1_18 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB1_20
# BB#21:                                #   in Loop: Header=BB1_19 Depth=4
	incq	%rbp
	xorl	%edx, %edx
	cmpb	$45, %cl
	setne	%dl
	addl	%edx, %eax
	cmpl	24(%r14), %eax
	movl	%eax, %ecx
	jne	.LBB1_19
	jmp	.LBB1_22
	.p2align	4, 0x90
.LBB1_20:                               # %._crit_edge203
                                        #   in Loop: Header=BB1_18 Depth=3
	movl	24(%r14), %ecx
.LBB1_22:                               # %.loopexit
                                        #   in Loop: Header=BB1_18 Depth=3
	movl	%ebp, %edx
	subl	%r13d, %edx
	decl	%edx
	movl	28(%r14), %esi
	cmpl	%esi, %ecx
	movl	%edx, %r8d
	je	.LBB1_26
	.p2align	4, 0x90
.LBB1_23:                               # %.preheader171
                                        #   Parent Loop BB1_14 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        #       Parent Loop BB1_18 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB1_25
# BB#24:                                #   in Loop: Header=BB1_23 Depth=4
	incq	%rbp
	xorl	%ebx, %ebx
	cmpb	$45, %cl
	setne	%bl
	addl	%ebx, %eax
	cmpl	%esi, %eax
	jne	.LBB1_23
.LBB1_25:                               #   in Loop: Header=BB1_18 Depth=3
	subl	%r13d, %ebp
	decl	%ebp
	movl	%ebp, %r8d
.LBB1_26:                               #   in Loop: Header=BB1_18 Depth=3
	movq	112(%rsp), %rax
	movq	(%rax,%r12,8), %r9
	movl	$-1, %eax
	movq	%r9, %rbp
	.p2align	4, 0x90
.LBB1_27:                               #   Parent Loop BB1_14 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        #       Parent Loop BB1_18 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB1_29
# BB#28:                                #   in Loop: Header=BB1_27 Depth=4
	incq	%rbp
	xorl	%esi, %esi
	cmpb	$45, %cl
	setne	%sil
	addl	%esi, %eax
	cmpl	32(%r14), %eax
	jne	.LBB1_27
.LBB1_29:                               #   in Loop: Header=BB1_18 Depth=3
	movl	%ebp, %r15d
	subl	%r9d, %r15d
	decl	%r15d
	movl	36(%r14), %esi
	cmpl	%esi, 32(%r14)
	movl	%r15d, %r10d
	je	.LBB1_33
	.p2align	4, 0x90
.LBB1_30:                               # %.preheader
                                        #   Parent Loop BB1_14 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        #       Parent Loop BB1_18 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB1_32
# BB#31:                                #   in Loop: Header=BB1_30 Depth=4
	incq	%rbp
	xorl	%ebx, %ebx
	cmpb	$45, %cl
	setne	%bl
	addl	%ebx, %eax
	cmpl	%esi, %eax
	jne	.LBB1_30
.LBB1_32:                               #   in Loop: Header=BB1_18 Depth=3
	subl	%r9d, %ebp
	decl	%ebp
	movl	%ebp, %r10d
.LBB1_33:                               #   in Loop: Header=BB1_18 Depth=3
	movslq	%edx, %r11
	addq	%r13, %r11
	movslq	%r15d, %rax
	addq	%rax, %r9
	jmp	.LBB1_34
	.p2align	4, 0x90
.LBB1_41:                               #   in Loop: Header=BB1_34 Depth=4
	cmpb	$45, %cl
	jne	.LBB1_45
# BB#42:                                #   in Loop: Header=BB1_34 Depth=4
	incl	%edx
	incq	%r11
	cmpb	$45, %al
	je	.LBB1_43
	jmp	.LBB1_45
	.p2align	4, 0x90
.LBB1_34:                               #   Parent Loop BB1_14 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        #       Parent Loop BB1_18 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%r11), %ecx
	testb	%cl, %cl
	je	.LBB1_47
# BB#35:                                #   in Loop: Header=BB1_34 Depth=4
	movzbl	(%r9), %eax
	testb	%al, %al
	je	.LBB1_47
# BB#36:                                #   in Loop: Header=BB1_34 Depth=4
	cmpb	$45, %cl
	je	.LBB1_39
# BB#37:                                #   in Loop: Header=BB1_34 Depth=4
	cmpb	$45, %al
	je	.LBB1_39
# BB#38:                                #   in Loop: Header=BB1_34 Depth=4
	movss	64(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	mulsd	%xmm2, %xmm3
	movslq	%edx, %rdx
	movq	(%rdi,%rdx,8), %rax
	movslq	%r15d, %r15
	movss	(%rax,%r15,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm3, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rax,%r15,4)
	incl	%edx
	incl	%r15d
	incq	%r11
	jmp	.LBB1_44
	.p2align	4, 0x90
.LBB1_39:                               #   in Loop: Header=BB1_34 Depth=4
	cmpb	$45, %al
	setne	%bl
	cmpb	$45, %cl
	je	.LBB1_41
# BB#40:                                #   in Loop: Header=BB1_34 Depth=4
	testb	%bl, %bl
	jne	.LBB1_41
.LBB1_43:                               #   in Loop: Header=BB1_34 Depth=4
	incl	%r15d
.LBB1_44:                               # %.thread
                                        #   in Loop: Header=BB1_34 Depth=4
	incq	%r9
.LBB1_45:                               # %.thread
                                        #   in Loop: Header=BB1_34 Depth=4
	cmpl	%r10d, %r15d
	jg	.LBB1_47
# BB#46:                                # %.thread
                                        #   in Loop: Header=BB1_34 Depth=4
	cmpl	%r8d, %edx
	jle	.LBB1_34
	.p2align	4, 0x90
.LBB1_47:                               # %.critedge
                                        #   in Loop: Header=BB1_18 Depth=3
	movq	8(%r14), %r14
	testq	%r14, %r14
	jne	.LBB1_18
.LBB1_48:                               # %._crit_edge
                                        #   in Loop: Header=BB1_16 Depth=2
	incq	%r12
	cmpq	48(%rsp), %r12          # 8-byte Folded Reload
	jne	.LBB1_16
.LBB1_49:                               # %._crit_edge179
                                        #   in Loop: Header=BB1_14 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpq	32(%rsp), %rcx          # 8-byte Folded Reload
	jne	.LBB1_14
.LBB1_50:                               # %._crit_edge181
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	part_imp_match_init_strict, .Lfunc_end1-part_imp_match_init_strict
	.cfi_endproc

	.globl	part_imp_rna
	.p2align	4, 0x90
	.type	part_imp_rna,@function
part_imp_rna:                           # @part_imp_rna
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	48(%rsp)
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	impmtx(%rip)
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	callq	foldrna
	addq	$48, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset -48
	popq	%rax
	retq
.Lfunc_end2:
	.size	part_imp_rna, .Lfunc_end2-part_imp_rna
	.cfi_endproc

	.globl	part_imp_match_init
	.p2align	4, 0x90
	.type	part_imp_match_init,@function
part_imp_match_init:                    # @part_imp_match_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 128
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movl	%r8d, %r14d
	movl	%ecx, %r12d
	movl	%esi, %r15d
	movl	part_imp_match_init.impalloclen(%rip), %eax
	cmpl	%r12d, %eax
	movl	%edx, 12(%rsp)          # 4-byte Spill
	jl	.LBB3_2
# BB#1:
	cmpl	%r14d, %eax
	jge	.LBB3_9
.LBB3_2:
	movq	impmtx(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#3:
	callq	FreeFloatMtx
.LBB3_4:
	movq	part_imp_match_init.nocount1(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_6
# BB#5:
	callq	free
.LBB3_6:
	movq	part_imp_match_init.nocount2(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_8
# BB#7:
	callq	free
.LBB3_8:
	cmpl	%r14d, %r12d
	movl	%r14d, %edi
	cmovgel	%r12d, %edi
	addl	$2, %edi
	movl	%edi, part_imp_match_init.impalloclen(%rip)
	movl	%edi, %esi
	callq	AllocateFloatMtx
	movq	%rax, impmtx(%rip)
	movl	part_imp_match_init.impalloclen(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, part_imp_match_init.nocount1(%rip)
	movl	part_imp_match_init.impalloclen(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, part_imp_match_init.nocount2(%rip)
	addl	$-2, part_imp_match_init.impalloclen(%rip)
	movl	12(%rsp), %edx          # 4-byte Reload
.LBB3_9:                                # %.preheader236
	testl	%r12d, %r12d
	jle	.LBB3_19
# BB#10:                                # %.preheader235.lr.ph
	testl	%r15d, %r15d
	movq	part_imp_match_init.nocount1(%rip), %rax
	jle	.LBB3_11
# BB#23:                                # %.preheader235.us.preheader
	movslq	%r15d, %rcx
	movl	%r12d, %r8d
	xorl	%esi, %esi
	testb	$1, %r8b
	je	.LBB3_27
	.p2align	4, 0x90
.LBB3_24:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rsi,8), %rdi
	cmpb	$45, (%rdi)
	je	.LBB3_26
# BB#25:                                #   in Loop: Header=BB3_24 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB3_24
.LBB3_26:                               # %._crit_edge267.us.prol
	cmpl	%r15d, %esi
	setne	(%rax)
	movl	$1, %esi
.LBB3_27:                               # %.preheader235.us.prol.loopexit
	cmpl	$1, %r12d
	je	.LBB3_19
	.p2align	4, 0x90
.LBB3_28:                               # %.preheader235.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_29 Depth 2
                                        #     Child Loop BB3_32 Depth 2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_29:                               #   Parent Loop BB3_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rdi,8), %rbp
	cmpb	$45, (%rbp,%rsi)
	je	.LBB3_31
# BB#30:                                #   in Loop: Header=BB3_29 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB3_29
.LBB3_31:                               # %._crit_edge267.us
                                        #   in Loop: Header=BB3_28 Depth=1
	cmpl	%r15d, %edi
	setne	(%rax,%rsi)
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_32:                               #   Parent Loop BB3_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rdi,8), %rbp
	cmpb	$45, 1(%rbp,%rsi)
	je	.LBB3_34
# BB#33:                                #   in Loop: Header=BB3_32 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB3_32
.LBB3_34:                               # %._crit_edge267.us.1
                                        #   in Loop: Header=BB3_28 Depth=1
	cmpl	%r15d, %edi
	setne	1(%rax,%rsi)
	addq	$2, %rsi
	cmpq	%r8, %rsi
	jne	.LBB3_28
	jmp	.LBB3_19
.LBB3_11:                               # %.preheader235.preheader
	setne	%cl
	movl	%r12d, %edi
	cmpl	$31, %r12d
	jbe	.LBB3_12
# BB#15:                                # %min.iters.checked
	movl	%r12d, %r8d
	andl	$31, %r8d
	movq	%rdi, %r9
	subq	%r8, %r9
	je	.LBB3_12
# BB#16:                                # %vector.ph
	movzbl	%cl, %esi
	movd	%esi, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	16(%rax), %rbp
	movq	%r9, %rsi
	.p2align	4, 0x90
.LBB3_17:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbp)
	movdqu	%xmm0, (%rbp)
	addq	$32, %rbp
	addq	$-32, %rsi
	jne	.LBB3_17
# BB#18:                                # %middle.block
	testl	%r8d, %r8d
	jne	.LBB3_13
	jmp	.LBB3_19
.LBB3_12:
	xorl	%r9d, %r9d
.LBB3_13:                               # %.preheader235.preheader370
	addq	%r9, %rax
	subq	%r9, %rdi
	.p2align	4, 0x90
.LBB3_14:                               # %.preheader235
                                        # =>This Inner Loop Header: Depth=1
	movb	%cl, (%rax)
	incq	%rax
	decq	%rdi
	jne	.LBB3_14
.LBB3_19:                               # %.preheader234
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	128(%rsp), %rbx
	testl	%r14d, %r14d
	jle	.LBB3_41
# BB#20:                                # %.preheader233.lr.ph
	testl	%edx, %edx
	movq	part_imp_match_init.nocount2(%rip), %rax
	jle	.LBB3_21
# BB#45:                                # %.preheader233.us.preheader
	movslq	%edx, %rcx
	movl	%r14d, %r8d
	xorl	%esi, %esi
	testb	$1, %r8b
	je	.LBB3_49
	.p2align	4, 0x90
.LBB3_46:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rsi,8), %rdi
	cmpb	$45, (%rdi)
	je	.LBB3_48
# BB#47:                                #   in Loop: Header=BB3_46 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB3_46
.LBB3_48:                               # %._crit_edge261.us.prol
	cmpl	%edx, %esi
	setne	(%rax)
	movl	$1, %esi
.LBB3_49:                               # %.preheader233.us.prol.loopexit
	cmpl	$1, %r14d
	je	.LBB3_41
	.p2align	4, 0x90
.LBB3_50:                               # %.preheader233.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_51 Depth 2
                                        #     Child Loop BB3_54 Depth 2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_51:                               #   Parent Loop BB3_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rdi,8), %rbp
	cmpb	$45, (%rbp,%rsi)
	je	.LBB3_53
# BB#52:                                #   in Loop: Header=BB3_51 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB3_51
.LBB3_53:                               # %._crit_edge261.us
                                        #   in Loop: Header=BB3_50 Depth=1
	cmpl	%edx, %edi
	setne	(%rax,%rsi)
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_54:                               #   Parent Loop BB3_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rdi,8), %rbp
	cmpb	$45, 1(%rbp,%rsi)
	je	.LBB3_56
# BB#55:                                #   in Loop: Header=BB3_54 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB3_54
.LBB3_56:                               # %._crit_edge261.us.1
                                        #   in Loop: Header=BB3_50 Depth=1
	cmpl	%edx, %edi
	setne	1(%rax,%rsi)
	addq	$2, %rsi
	cmpq	%r8, %rsi
	jne	.LBB3_50
	jmp	.LBB3_41
.LBB3_21:                               # %.preheader233.preheader
	setne	%cl
	movl	%r14d, %edx
	cmpl	$31, %r14d
	jbe	.LBB3_22
# BB#35:                                # %min.iters.checked351
	movl	%r14d, %r8d
	andl	$31, %r8d
	movq	%rdx, %rdi
	subq	%r8, %rdi
	je	.LBB3_22
# BB#36:                                # %vector.ph355
	movzbl	%cl, %esi
	movd	%esi, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	16(%rax), %rbp
	movq	%rdi, %rsi
	.p2align	4, 0x90
.LBB3_37:                               # %vector.body347
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbp)
	movdqu	%xmm0, (%rbp)
	addq	$32, %rbp
	addq	$-32, %rsi
	jne	.LBB3_37
# BB#38:                                # %middle.block348
	testl	%r8d, %r8d
	jne	.LBB3_39
	jmp	.LBB3_41
.LBB3_22:
	xorl	%edi, %edi
.LBB3_39:                               # %.preheader233.preheader368
	addq	%rdi, %rax
	subq	%rdi, %rdx
	.p2align	4, 0x90
.LBB3_40:                               # %.preheader233
                                        # =>This Inner Loop Header: Depth=1
	movb	%cl, (%rax)
	incq	%rax
	decq	%rdx
	jne	.LBB3_40
.LBB3_41:                               # %.preheader232
	testl	%r12d, %r12d
	jle	.LBB3_62
# BB#42:                                # %.preheader231.lr.ph
	testl	%r14d, %r14d
	jle	.LBB3_62
# BB#43:                                # %.preheader231.us.preheader
	movq	impmtx(%rip), %r13
	decl	%r14d
	leaq	4(,%r14,4), %rbx
	movl	%r12d, %r14d
	leaq	-1(%r14), %rax
	movq	%r14, %r12
	andq	$7, %r12
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB3_44
# BB#57:                                # %.preheader231.us.prol.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_58:                               # %.preheader231.us.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	incq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB3_58
	jmp	.LBB3_59
.LBB3_44:
	xorl	%ebp, %ebp
.LBB3_59:                               # %.preheader231.us.prol.loopexit
	cmpq	$7, 16(%rsp)            # 8-byte Folded Reload
	jb	.LBB3_62
# BB#60:                                # %.preheader231.us.preheader.new
	subq	%rbp, %r14
	leaq	56(%r13,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB3_61:                               # %.preheader231.us
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-48(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-40(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-32(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-24(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-16(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	-8(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	addq	$64, %rbp
	addq	$-8, %r14
	jne	.LBB3_61
.LBB3_62:                               # %.preheader230
	testl	%r15d, %r15d
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	128(%rsp), %rbp
	movq	%rbp, %r13
	jg	.LBB3_63
.LBB3_111:                              # %._crit_edge254
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_63:                               # %.lr.ph253
	movl	12(%rsp), %eax          # 4-byte Reload
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%r15d, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	jmp	.LBB3_64
.LBB3_65:                               # %.lr.ph250
                                        #   in Loop: Header=BB3_64 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB3_66
.LBB3_97:                               # %.preheader
                                        #   in Loop: Header=BB3_66 Depth=2
	cmpl	%ebx, %ebp
	movq	%rbx, %rax
	jge	.LBB3_98
	.p2align	4, 0x90
.LBB3_112:                              # %.preheader.split.us
                                        #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	stderr(%rip), %rdi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	callq	fprintf
	movq	152(%rsp), %r11
	decl	%r13d
	cmpl	$-1, %r13d
	jne	.LBB3_112
	jmp	.LBB3_109
.LBB3_98:                               # %.preheader.split.preheader
                                        #   in Loop: Header=BB3_66 Depth=2
	movslq	%eax, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movslq	%ebp, %rbx
	movq	32(%rsp), %rcx          # 8-byte Reload
	leal	(%rax,%rcx), %edx
	subl	%ebp, %edx
	movslq	24(%rsp), %rbp          # 4-byte Folded Reload
	movslq	%ecx, %r15
	.p2align	4, 0x90
.LBB3_99:                               # %.preheader.split
                                        #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_101 Depth 4
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movq	stderr(%rip), %rdi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	callq	fprintf
	movl	16(%rsp), %edx          # 4-byte Reload
	movq	152(%rsp), %r11
	movq	24(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%r13d, %eax
	cmpl	32(%rsp), %eax          # 4-byte Folded Reload
	jg	.LBB3_108
# BB#100:                               # %.lr.ph247
                                        #   in Loop: Header=BB3_99 Depth=3
	movslq	%edx, %rax
	movq	part_imp_match_init.nocount1(%rip), %rcx
	movq	part_imp_match_init.nocount2(%rip), %r9
	movq	impmtx(%rip), %r8
	movq	48(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_101:                              #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        #       Parent Loop BB3_99 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testq	%rax, %rax
	js	.LBB3_106
# BB#102:                               #   in Loop: Header=BB3_101 Depth=4
	cmpq	%rax, %rbp
	jg	.LBB3_106
# BB#103:                               #   in Loop: Header=BB3_101 Depth=4
	cmpb	$0, (%rcx,%rdi)
	jne	.LBB3_106
# BB#104:                               #   in Loop: Header=BB3_101 Depth=4
	cmpb	$0, (%r9,%rax)
	jne	.LBB3_106
# BB#105:                               #   in Loop: Header=BB3_101 Depth=4
	movq	(%r11,%r12,8), %rsi
	movq	(%rsi,%r14,8), %rsi
	movsd	72(%rsi), %xmm0         # xmm0 = mem[0],zero
	movq	136(%rsp), %rsi
	mulsd	(%rsi,%r12,8), %xmm0
	movq	144(%rsp), %rsi
	mulsd	(%rsi,%r14,8), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r8,%rdi,8), %rsi
	movd	%xmm0, (%rsi,%rax,4)
	.p2align	4, 0x90
.LBB3_106:                              #   in Loop: Header=BB3_101 Depth=4
	cmpq	%r15, %rax
	jge	.LBB3_108
# BB#107:                               #   in Loop: Header=BB3_101 Depth=4
	incq	%rax
	cmpq	%rbx, %rdi
	leaq	1(%rdi), %rdi
	jl	.LBB3_101
.LBB3_108:                              # %.critedge2
                                        #   in Loop: Header=BB3_99 Depth=3
	incl	%edx
	testl	%r13d, %r13d
	leal	-1(%r13), %eax
	movl	%eax, %r13d
	jne	.LBB3_99
	jmp	.LBB3_109
	.p2align	4, 0x90
.LBB3_66:                               #   Parent Loop BB3_64 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_67 Depth 3
                                        #       Child Loop BB3_70 Depth 3
                                        #       Child Loop BB3_73 Depth 3
                                        #       Child Loop BB3_76 Depth 3
                                        #       Child Loop BB3_81 Depth 3
                                        #       Child Loop BB3_88 Depth 3
                                        #         Child Loop BB3_90 Depth 4
                                        #       Child Loop BB3_99 Depth 3
                                        #         Child Loop BB3_101 Depth 4
                                        #       Child Loop BB3_112 Depth 3
	movq	stderr(%rip), %rdi
	movq	(%r11,%r12,8), %rax
	movq	(%rax,%r14,8), %rax
	movl	24(%rax), %edx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	152(%rsp), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax,%r14,8), %rax
	movl	28(%rax), %edx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	(%r13,%r14,8), %rcx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	fprintf
	movq	152(%rsp), %rdi
	movq	(%rbx,%r12,8), %rdx
	movl	$-1, %ecx
	movq	%rdx, %rax
	.p2align	4, 0x90
.LBB3_67:                               #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %ebx
	testb	%bl, %bl
	je	.LBB3_69
# BB#68:                                #   in Loop: Header=BB3_67 Depth=3
	incq	%rax
	xorl	%esi, %esi
	cmpb	$45, %bl
	setne	%sil
	addl	%esi, %ecx
	movq	(%rdi,%r12,8), %rsi
	movq	(%rsi,%r14,8), %rsi
	cmpl	24(%rsi), %ecx
	jne	.LBB3_67
.LBB3_69:                               #   in Loop: Header=BB3_66 Depth=2
	movl	$4294967295, %ebp       # imm = 0xFFFFFFFF
	subq	%rdx, %rbp
	leaq	(%rbp,%rax), %r15
	.p2align	4, 0x90
.LBB3_70:                               #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %edx
	testb	%dl, %dl
	je	.LBB3_72
# BB#71:                                #   in Loop: Header=BB3_70 Depth=3
	incq	%rax
	xorl	%esi, %esi
	cmpb	$45, %dl
	setne	%sil
	addl	%esi, %ecx
	movq	(%rdi,%r12,8), %rdx
	movq	(%rdx,%r14,8), %rdx
	cmpl	28(%rdx), %ecx
	jne	.LBB3_70
.LBB3_72:                               #   in Loop: Header=BB3_66 Depth=2
	addq	%rax, %rbp
	movq	(%r13,%r14,8), %rdx
	movl	$-1, %ecx
	movq	%rdx, %rax
	.p2align	4, 0x90
.LBB3_73:                               #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %ebx
	testb	%bl, %bl
	je	.LBB3_75
# BB#74:                                #   in Loop: Header=BB3_73 Depth=3
	incq	%rax
	xorl	%esi, %esi
	cmpb	$45, %bl
	setne	%sil
	addl	%esi, %ecx
	movq	(%rdi,%r12,8), %rsi
	movq	(%rsi,%r14,8), %rsi
	cmpl	32(%rsi), %ecx
	jne	.LBB3_73
.LBB3_75:                               #   in Loop: Header=BB3_66 Depth=2
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	$4294967295, %ebx       # imm = 0xFFFFFFFF
	subq	%rdx, %rbx
	leaq	(%rbx,%rax), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_76:                               #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rax), %edx
	testb	%dl, %dl
	je	.LBB3_78
# BB#77:                                #   in Loop: Header=BB3_76 Depth=3
	incq	%rax
	xorl	%esi, %esi
	cmpb	$45, %dl
	setne	%sil
	addl	%esi, %ecx
	movq	(%rdi,%r12,8), %rdx
	movq	(%rdx,%r14,8), %rdx
	cmpl	36(%rdx), %ecx
	jne	.LBB3_76
.LBB3_78:                               #   in Loop: Header=BB3_66 Depth=2
	addq	%rax, %rbx
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	152(%rsp), %r9
	movq	24(%rsp), %r13          # 8-byte Reload
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	subl	%ebx, %r13d
	movq	%r15, %r10
	movq	16(%rsp), %r11          # 8-byte Reload
	jg	.LBB3_86
# BB#79:                                #   in Loop: Header=BB3_66 Depth=2
	cmpl	%r10d, %r11d
	jl	.LBB3_86
# BB#80:                                # %.lr.ph
                                        #   in Loop: Header=BB3_66 Depth=2
	movq	part_imp_match_init.nocount1(%rip), %rax
	movq	part_imp_match_init.nocount2(%rip), %rcx
	movq	impmtx(%rip), %r8
	movslq	%r10d, %rsi
	movslq	%r11d, %rdi
	movslq	24(%rsp), %rbx          # 4-byte Folded Reload
	movslq	32(%rsp), %rdx          # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB3_81:                               #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$0, (%rax,%rsi)
	jne	.LBB3_84
# BB#82:                                #   in Loop: Header=BB3_81 Depth=3
	cmpb	$0, (%rcx,%rbx)
	jne	.LBB3_84
# BB#83:                                #   in Loop: Header=BB3_81 Depth=3
	movq	(%r9,%r12,8), %rbp
	movq	(%rbp,%r14,8), %rbp
	movsd	72(%rbp), %xmm0         # xmm0 = mem[0],zero
	movq	136(%rsp), %rbp
	mulsd	(%rbp,%r12,8), %xmm0
	movq	144(%rsp), %rbp
	mulsd	(%rbp,%r14,8), %xmm0
	movq	(%r8,%rsi,8), %rbp
	movss	(%rbp,%rbx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movd	%xmm0, (%rbp,%rbx,4)
	.p2align	4, 0x90
.LBB3_84:                               #   in Loop: Header=BB3_81 Depth=3
	cmpq	%rdx, %rbx
	jge	.LBB3_86
# BB#85:                                #   in Loop: Header=BB3_81 Depth=3
	incq	%rbx
	cmpq	%rdi, %rsi
	leaq	1(%rsi), %rsi
	jl	.LBB3_81
.LBB3_86:                               # %.critedge
                                        #   in Loop: Header=BB3_66 Depth=2
	movl	%r11d, %eax
	subl	%r10d, %eax
	addl	%eax, %r13d
	movq	stderr(%rip), %rdi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	movq	%r10, %rbx
	movq	%r11, %rbp
	callq	fprintf
	testl	%r13d, %r13d
	jg	.LBB3_97
# BB#87:                                # %.preheader228
                                        #   in Loop: Header=BB3_66 Depth=2
	movq	part_imp_match_init.nocount1(%rip), %rax
	movq	part_imp_match_init.nocount2(%rip), %r10
	movq	impmtx(%rip), %r9
	movq	24(%rsp), %rcx          # 8-byte Reload
	leal	(%rbp,%rcx), %esi
	subl	32(%rsp), %esi          # 4-byte Folded Reload
	movslq	%ebx, %rdi
	movslq	%ebp, %rbx
	movslq	%ecx, %r8
	movq	152(%rsp), %r11
	.p2align	4, 0x90
.LBB3_88:                               #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_90 Depth 4
	leal	(%r13,%r15), %edx
	cmpl	16(%rsp), %edx          # 4-byte Folded Reload
	jg	.LBB3_96
# BB#89:                                # %.lr.ph242.preheader
                                        #   in Loop: Header=BB3_88 Depth=3
	movslq	%esi, %rdx
	movq	%r8, %rbp
	.p2align	4, 0x90
.LBB3_90:                               # %.lr.ph242
                                        #   Parent Loop BB3_64 Depth=1
                                        #     Parent Loop BB3_66 Depth=2
                                        #       Parent Loop BB3_88 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testq	%rdx, %rdx
	js	.LBB3_95
# BB#91:                                # %.lr.ph242
                                        #   in Loop: Header=BB3_90 Depth=4
	cmpq	%rdi, %rdx
	jl	.LBB3_95
# BB#92:                                #   in Loop: Header=BB3_90 Depth=4
	cmpb	$0, (%rax,%rdx)
	jne	.LBB3_95
# BB#93:                                #   in Loop: Header=BB3_90 Depth=4
	cmpb	$0, (%r10,%rbp)
	jne	.LBB3_95
# BB#94:                                #   in Loop: Header=BB3_90 Depth=4
	movq	(%r11,%r12,8), %rcx
	movq	(%rcx,%r14,8), %rcx
	movsd	72(%rcx), %xmm0         # xmm0 = mem[0],zero
	movq	136(%rsp), %rcx
	mulsd	(%rcx,%r12,8), %xmm0
	movq	144(%rsp), %rcx
	mulsd	(%rcx,%r14,8), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r9,%rdx,8), %rcx
	movd	%xmm0, (%rcx,%rbp,4)
	.p2align	4, 0x90
.LBB3_95:                               #   in Loop: Header=BB3_90 Depth=4
	incq	%rbp
	cmpq	%rbx, %rdx
	leaq	1(%rdx), %rdx
	jl	.LBB3_90
.LBB3_96:                               # %._crit_edge
                                        #   in Loop: Header=BB3_88 Depth=3
	incl	%esi
	testl	%r13d, %r13d
	leal	1(%r13), %ecx
	movl	%ecx, %r13d
	jne	.LBB3_88
.LBB3_109:                              # %.loopexit
                                        #   in Loop: Header=BB3_66 Depth=2
	incq	%r14
	cmpq	64(%rsp), %r14          # 8-byte Folded Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	128(%rsp), %rbp
	movq	%rbp, %r13
	jne	.LBB3_66
	jmp	.LBB3_110
	.p2align	4, 0x90
.LBB3_64:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_66 Depth 2
                                        #       Child Loop BB3_67 Depth 3
                                        #       Child Loop BB3_70 Depth 3
                                        #       Child Loop BB3_73 Depth 3
                                        #       Child Loop BB3_76 Depth 3
                                        #       Child Loop BB3_81 Depth 3
                                        #       Child Loop BB3_88 Depth 3
                                        #         Child Loop BB3_90 Depth 4
                                        #       Child Loop BB3_99 Depth 3
                                        #         Child Loop BB3_101 Depth 4
                                        #       Child Loop BB3_112 Depth 3
	movq	stderr(%rip), %rdi
	movq	(%rbx,%r12,8), %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%r12d, %edx
	callq	fprintf
	movq	152(%rsp), %r11
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jg	.LBB3_65
.LBB3_110:                              # %._crit_edge251
                                        #   in Loop: Header=BB3_64 Depth=1
	incq	%r12
	cmpq	56(%rsp), %r12          # 8-byte Folded Reload
	jne	.LBB3_64
	jmp	.LBB3_111
.Lfunc_end3:
	.size	part_imp_match_init, .Lfunc_end3-part_imp_match_init
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4608533498688228557     # double 1.3
.LCPI4_3:
	.quad	4607182418800017408     # double 1
.LCPI4_4:
	.quad	4602678819172646912     # double 0.5
.LCPI4_5:
	.quad	-4620693217682128896    # double -0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_1:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
.LCPI4_2:
	.quad	4602678819172646912     # double 0.5
	.quad	4602678819172646912     # double 0.5
.LCPI4_6:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI4_7:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI4_8:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.text
	.globl	partA__align
	.p2align	4, 0x90
	.type	partA__align,@function
partA__align:                           # @partA__align
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi40:
	.cfi_def_cfa_offset 368
.Lcfi41:
	.cfi_offset %rbx, -56
.Lcfi42:
	.cfi_offset %r12, -48
.Lcfi43:
	.cfi_offset %r13, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movl	%r9d, %r15d
	movl	%r8d, %r14d
	movq	%rcx, %r12
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	cvtsi2ssl	penalty(%rip), %xmm0
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
	movl	partA__align.orlgth1(%rip), %r13d
	testl	%r13d, %r13d
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	jne	.LBB4_2
# BB#1:
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, partA__align.mseq1(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	%rax, partA__align.mseq2(%rip)
	movl	partA__align.orlgth1(%rip), %r13d
.LBB4_2:
	movq	(%rdi), %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%rbx, 136(%rsp)         # 8-byte Spill
	movq	(%rbx), %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	cmpl	%r13d, %ebp
	movl	partA__align.orlgth2(%rip), %r10d
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	movl	%r14d, 28(%rsp)         # 4-byte Spill
	movq	%r12, 40(%rsp)          # 8-byte Spill
	jg	.LBB4_4
# BB#3:
	cmpl	%r10d, %ebx
	jle	.LBB4_8
.LBB4_4:
	testl	%r13d, %r13d
	jle	.LBB4_7
# BB#5:
	testl	%r10d, %r10d
	jle	.LBB4_7
# BB#6:
	movq	partA__align.w1(%rip), %rdi
	callq	FreeFloatVec
	movq	partA__align.w2(%rip), %rdi
	callq	FreeFloatVec
	movq	partA__align.match(%rip), %rdi
	callq	FreeFloatVec
	movq	partA__align.initverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	partA__align.lastverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	partA__align.m(%rip), %rdi
	callq	FreeFloatVec
	movq	partA__align.mp(%rip), %rdi
	callq	FreeIntVec
	movq	partA__align.mseq(%rip), %rdi
	callq	FreeCharMtx
	movq	partA__align.ogcp1(%rip), %rdi
	callq	FreeFloatVec
	movq	partA__align.ogcp2(%rip), %rdi
	callq	FreeFloatVec
	movq	partA__align.fgcp1(%rip), %rdi
	callq	FreeFloatVec
	movq	partA__align.fgcp2(%rip), %rdi
	callq	FreeFloatVec
	movq	partA__align.cpmx1(%rip), %rdi
	callq	FreeFloatMtx
	movq	partA__align.cpmx2(%rip), %rdi
	callq	FreeFloatMtx
	movq	partA__align.floatwork(%rip), %rdi
	callq	FreeFloatMtx
	movq	partA__align.intwork(%rip), %rdi
	callq	FreeIntMtx
	movl	partA__align.orlgth1(%rip), %r13d
	movl	partA__align.orlgth2(%rip), %r10d
.LBB4_7:
	movq	144(%rsp), %rax         # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI4_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r13d, %eax
	cmovgel	%eax, %r13d
	leal	100(%r13), %r14d
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r10d, %eax
	cmovgel	%eax, %r10d
	leal	100(%r10), %r12d
	leal	102(%r10), %ebx
	movl	%ebx, %edi
	movq	%r10, %rbp
	callq	AllocateFloatVec
	movq	%rax, partA__align.w1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, partA__align.w2(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, partA__align.match(%rip)
	leal	102(%r13), %r15d
	movl	%r15d, %edi
	callq	AllocateFloatVec
	movq	%rax, partA__align.initverticalw(%rip)
	movl	%r15d, %edi
	callq	AllocateFloatVec
	movq	%rax, partA__align.lastverticalw(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, partA__align.m(%rip)
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, partA__align.mp(%rip)
	movl	njob(%rip), %edi
	leal	200(%rbp,%r13), %esi
	callq	AllocateCharMtx
	movq	%rax, partA__align.mseq(%rip)
	movl	%r15d, %edi
	callq	AllocateFloatVec
	movq	%rax, partA__align.ogcp1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, partA__align.ogcp2(%rip)
	movl	%r15d, %edi
	callq	AllocateFloatVec
	movq	%rax, partA__align.fgcp1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, partA__align.fgcp2(%rip)
	movl	$26, %edi
	movl	%r15d, %esi
	callq	AllocateFloatMtx
	movq	%rax, partA__align.cpmx1(%rip)
	movl	$26, %edi
	movl	%ebx, %esi
	callq	AllocateFloatMtx
	movq	%rax, partA__align.cpmx2(%rip)
	cmpl	%r12d, %r14d
	cmovgel	%r14d, %r12d
	addl	$2, %r12d
	movl	$26, %esi
	movl	%r12d, %edi
	callq	AllocateFloatMtx
	movq	%rax, partA__align.floatwork(%rip)
	movl	$26, %esi
	movl	%r12d, %edi
	callq	AllocateIntMtx
	movq	%rbp, %r10
	movq	%rax, partA__align.intwork(%rip)
	movl	%r13d, partA__align.orlgth1(%rip)
	movl	%r10d, partA__align.orlgth2(%rip)
	movl	28(%rsp), %r14d         # 4-byte Reload
	movq	40(%rsp), %r12          # 8-byte Reload
	movl	12(%rsp), %r15d         # 4-byte Reload
.LBB4_8:                                # %.preheader419
	testl	%r14d, %r14d
	jle	.LBB4_23
# BB#9:                                 # %.lr.ph473
	movq	partA__align.mseq(%rip), %rcx
	movq	partA__align.mseq1(%rip), %rdx
	movl	%r14d, %ebp
	cmpl	$3, %r14d
	jbe	.LBB4_10
# BB#17:                                # %min.iters.checked
	movl	%r14d, %r9d
	andl	$3, %r9d
	movq	%rbp, %rsi
	subq	%r9, %rsi
	je	.LBB4_10
# BB#18:                                # %vector.memcheck
	leaq	(%rcx,%rbp,8), %rax
	cmpq	%rax, %rdx
	jae	.LBB4_20
# BB#19:                                # %vector.memcheck
	leaq	(%rdx,%rbp,8), %rax
	cmpq	%rax, %rcx
	jae	.LBB4_20
.LBB4_10:
	xorl	%esi, %esi
.LBB4_11:                               # %scalar.ph.preheader
	movl	%ebp, %eax
	subl	%esi, %eax
	leaq	-1(%rbp), %rdi
	subq	%rsi, %rdi
	andq	$7, %rax
	je	.LBB4_14
# BB#12:                                # %scalar.ph.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB4_13:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rsi,8), %rbx
	movq	%rbx, (%rdx,%rsi,8)
	incq	%rsi
	incq	%rax
	jne	.LBB4_13
.LBB4_14:                               # %scalar.ph.prol.loopexit
	cmpq	$7, %rdi
	jb	.LBB4_23
# BB#15:                                # %scalar.ph.preheader.new
	subq	%rsi, %rbp
	leaq	56(%rdx,%rsi,8), %rdx
	leaq	56(%rcx,%rsi,8), %rcx
	.p2align	4, 0x90
.LBB4_16:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rax
	movq	%rax, -56(%rdx)
	movq	-48(%rcx), %rax
	movq	%rax, -48(%rdx)
	movq	-40(%rcx), %rax
	movq	%rax, -40(%rdx)
	movq	-32(%rcx), %rax
	movq	%rax, -32(%rdx)
	movq	-24(%rcx), %rax
	movq	%rax, -24(%rdx)
	movq	-16(%rcx), %rax
	movq	%rax, -16(%rdx)
	movq	-8(%rcx), %rax
	movq	%rax, -8(%rdx)
	movq	(%rcx), %rax
	movq	%rax, (%rdx)
	addq	$64, %rdx
	addq	$64, %rcx
	addq	$-8, %rbp
	jne	.LBB4_16
.LBB4_23:                               # %.preheader418
	testl	%r15d, %r15d
	jle	.LBB4_38
# BB#24:                                # %.lr.ph470
	movq	partA__align.mseq(%rip), %r11
	movq	partA__align.mseq2(%rip), %rsi
	movslq	%r14d, %r9
	movl	%r15d, %ebx
	cmpl	$3, %r15d
	jbe	.LBB4_25
# BB#32:                                # %min.iters.checked578
	movl	%r15d, %ebp
	andl	$3, %ebp
	movq	%rbx, %rdi
	subq	%rbp, %rdi
	je	.LBB4_25
# BB#33:                                # %vector.memcheck593
	leaq	(%r9,%rbx), %rax
	leaq	(%r11,%rax,8), %rax
	cmpq	%rax, %rsi
	jae	.LBB4_35
# BB#34:                                # %vector.memcheck593
	leaq	(%rsi,%rbx,8), %rax
	leaq	(%r11,%r9,8), %rcx
	cmpq	%rax, %rcx
	jae	.LBB4_35
.LBB4_25:
	xorl	%edi, %edi
.LBB4_26:                               # %scalar.ph576.preheader
	movl	%ebx, %eax
	subl	%edi, %eax
	leaq	-1(%rbx), %rbp
	subq	%rdi, %rbp
	andq	$3, %rax
	je	.LBB4_29
# BB#27:                                # %scalar.ph576.prol.preheader
	leaq	(%r11,%r9,8), %rcx
	negq	%rax
	.p2align	4, 0x90
.LBB4_28:                               # %scalar.ph576.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdi,8), %rdx
	movq	%rdx, (%rsi,%rdi,8)
	incq	%rdi
	incq	%rax
	jne	.LBB4_28
.LBB4_29:                               # %scalar.ph576.prol.loopexit
	cmpq	$3, %rbp
	jb	.LBB4_38
# BB#30:                                # %scalar.ph576.preheader.new
	subq	%rdi, %rbx
	leaq	24(%rsi,%rdi,8), %rsi
	addq	%rdi, %r9
	leaq	24(%r11,%r9,8), %rax
	.p2align	4, 0x90
.LBB4_31:                               # %scalar.ph576
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%rax), %rcx
	movq	%rcx, -24(%rsi)
	movq	-16(%rax), %rcx
	movq	%rcx, -16(%rsi)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rsi)
	movq	(%rax), %rcx
	movq	%rcx, (%rsi)
	addq	$32, %rsi
	addq	$32, %rax
	addq	$-4, %rbx
	jne	.LBB4_31
.LBB4_38:                               # %._crit_edge471
	movl	commonAlloc1(%rip), %ebx
	cmpl	%ebx, %r13d
	movl	commonAlloc2(%rip), %ebp
	jg	.LBB4_41
# BB#39:                                # %._crit_edge471
	cmpl	%ebp, %r10d
	jg	.LBB4_41
# BB#40:                                # %._crit_edge545
	movq	commonIP(%rip), %rax
	jmp	.LBB4_45
.LBB4_41:                               # %._crit_edge471._crit_edge
	testl	%ebx, %ebx
	je	.LBB4_44
# BB#42:                                # %._crit_edge471._crit_edge
	testl	%ebp, %ebp
	je	.LBB4_44
# BB#43:
	movq	commonIP(%rip), %rdi
	callq	FreeIntMtx
	movl	partA__align.orlgth1(%rip), %r13d
	movl	commonAlloc1(%rip), %ebx
	movl	partA__align.orlgth2(%rip), %r10d
	movl	commonAlloc2(%rip), %ebp
.LBB4_44:
	cmpl	%ebx, %r13d
	cmovgel	%r13d, %ebx
	cmpl	%ebp, %r10d
	cmovgel	%r10d, %ebp
	leal	10(%rbx), %edi
	leal	10(%rbp), %esi
	callq	AllocateIntMtx
	movq	%rax, commonIP(%rip)
	movl	%ebx, commonAlloc1(%rip)
	movl	%ebp, commonAlloc2(%rip)
.LBB4_45:
	movq	80(%rsp), %r13          # 8-byte Reload
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	%rax, partA__align.ijp(%rip)
	movq	partA__align.cpmx1(%rip), %rsi
	movq	%r13, %rdi
	movq	%r15, %rdx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r14d, %r8d
	callq	cpmx_calc_new
	movq	partA__align.cpmx2(%rip), %rsi
	movq	136(%rsp), %rdi         # 8-byte Reload
	movq	%r12, %rdx
	movq	%r12, %rbp
	movq	96(%rsp), %r12          # 8-byte Reload
	movl	%r12d, %ecx
	movl	12(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %r8d
	callq	cpmx_calc_new
	movq	440(%rsp), %r9
	testq	%r9, %r9
	movq	partA__align.ogcp1(%rip), %rdi
	movl	%r14d, %esi
	je	.LBB4_47
# BB#46:
	movq	%rbp, %rbx
	movq	%r13, %rbp
	movq	%rbp, %rdx
	movq	%r15, %rcx
	movq	144(%rsp), %r8          # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	new_OpeningGapCount
	movq	partA__align.ogcp2(%rip), %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	movq	136(%rsp), %r13         # 8-byte Reload
	movq	%r13, %rdx
	movq	%rbx, %rcx
	movl	%r12d, %r8d
	movq	448(%rsp), %r9
	movq	144(%rsp), %rbx         # 8-byte Reload
	callq	new_OpeningGapCount
	movq	partA__align.fgcp1(%rip), %rdi
	movl	%r14d, %esi
	movq	%rbp, %rdx
	movq	%r15, %rcx
	movl	%ebx, %r8d
	movq	456(%rsp), %r9
	callq	new_FinalGapCount
	movq	partA__align.fgcp2(%rip), %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	movq	%r13, %rdx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rbx, %r13
	movl	%r12d, %r8d
	movq	464(%rsp), %r9
	callq	new_FinalGapCount
	movss	48(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	testl	%r13d, %r13d
	jg	.LBB4_49
	jmp	.LBB4_59
.LBB4_47:
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	144(%rsp), %r15         # 8-byte Reload
	movl	%r15d, %r8d
	callq	st_OpeningGapCount
	movq	partA__align.ogcp2(%rip), %rdi
	movl	%ebx, %esi
	movq	136(%rsp), %r13         # 8-byte Reload
	movq	%r13, %rdx
	movq	%rbp, %rcx
	movl	%r12d, %r8d
	callq	st_OpeningGapCount
	movq	partA__align.fgcp1(%rip), %rdi
	movl	%r14d, %esi
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%r15d, %r8d
	callq	st_FinalGapCount
	movq	partA__align.fgcp2(%rip), %rdi
	movl	%ebx, %esi
	movq	%r13, %rdx
	movq	%rbp, %rcx
	movl	%r12d, %r8d
	callq	st_FinalGapCount
	movss	48(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movq	%r15, %r13
	testl	%r13d, %r13d
	jle	.LBB4_59
.LBB4_49:                               # %.lr.ph468
	movq	partA__align.ogcp1(%rip), %r8
	cvtss2sd	%xmm7, %xmm0
	movq	partA__align.fgcp1(%rip), %rbx
	movl	%r13d, %eax
	cmpq	$3, %rax
	jbe	.LBB4_50
# BB#53:                                # %min.iters.checked609
	movl	%r13d, %edi
	andl	$3, %edi
	movq	%rax, %rbp
	subq	%rdi, %rbp
	je	.LBB4_50
# BB#54:                                # %vector.memcheck622
	leaq	(%rbx,%rax,4), %rcx
	cmpq	%rcx, %r8
	jae	.LBB4_56
# BB#55:                                # %vector.memcheck622
	leaq	(%r8,%rax,4), %rcx
	cmpq	%rcx, %rbx
	jae	.LBB4_56
.LBB4_50:
	xorl	%ebp, %ebp
.LBB4_51:                               # %scalar.ph607.preheader
	leaq	(%r8,%rbp,4), %rcx
	leaq	(%rbx,%rbp,4), %rdx
	subq	%rbp, %rax
	movsd	.LCPI4_3(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI4_4(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB4_52:                               # %scalar.ph607
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rcx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rcx)
	movss	(%rdx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rdx)
	addq	$4, %rcx
	addq	$4, %rdx
	decq	%rax
	jne	.LBB4_52
.LBB4_59:                               # %.preheader416
	testl	%r12d, %r12d
	jle	.LBB4_70
# BB#60:                                # %.lr.ph465
	movq	partA__align.ogcp2(%rip), %r8
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm7, %xmm0
	movq	partA__align.fgcp2(%rip), %rbx
	movl	%r12d, %eax
	cmpq	$3, %rax
	jbe	.LBB4_61
# BB#64:                                # %min.iters.checked639
	movl	%r12d, %edi
	andl	$3, %edi
	movq	%rax, %rbp
	subq	%rdi, %rbp
	je	.LBB4_61
# BB#65:                                # %vector.memcheck652
	leaq	(%rbx,%rax,4), %rcx
	cmpq	%rcx, %r8
	jae	.LBB4_67
# BB#66:                                # %vector.memcheck652
	leaq	(%r8,%rax,4), %rcx
	cmpq	%rcx, %rbx
	jae	.LBB4_67
.LBB4_61:
	xorl	%ebp, %ebp
.LBB4_62:                               # %scalar.ph637.preheader
	leaq	(%r8,%rbp,4), %rcx
	leaq	(%rbx,%rbp,4), %rdx
	subq	%rbp, %rax
	movsd	.LCPI4_3(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI4_4(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB4_63:                               # %scalar.ph637
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rcx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rcx)
	movss	(%rdx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm4, %xmm3
	movss	%xmm3, (%rdx)
	addq	$4, %rcx
	addq	$4, %rdx
	decq	%rax
	jne	.LBB4_63
.LBB4_70:                               # %._crit_edge466
	movl	408(%rsp), %r14d
	movq	partA__align.w1(%rip), %r15
	movq	partA__align.w2(%rip), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	partA__align.initverticalw(%rip), %rdi
	movq	partA__align.cpmx2(%rip), %rsi
	movq	partA__align.cpmx1(%rip), %rdx
	movq	partA__align.floatwork(%rip), %r9
	movl	$0, %ecx
	movl	%r13d, %r8d
	pushq	$1
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	partA__align.intwork(%rip)
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	addq	$16, %rsp
.Lcfi49:
	.cfi_adjust_cfa_offset -16
	cmpq	$0, 376(%rsp)
	je	.LBB4_85
# BB#71:
	testl	%r13d, %r13d
	jle	.LBB4_78
# BB#72:                                # %.lr.ph.i
	movq	partA__align.initverticalw(%rip), %rsi
	movq	432(%rsp), %rax
	movslq	(%rax), %rcx
	movslq	%r14d, %rax
	addq	%rcx, %rax
	movq	impmtx(%rip), %rcx
	movl	%r13d, %edx
	testb	$1, %r13b
	jne	.LBB4_74
# BB#73:
	xorl	%edi, %edi
	cmpq	$1, %rdx
	jne	.LBB4_76
	jmp	.LBB4_78
.LBB4_85:                               # %.critedge
	movq	partA__align.cpmx1(%rip), %rsi
	movq	partA__align.cpmx2(%rip), %rdx
	movq	partA__align.floatwork(%rip), %r9
	movl	$0, %ecx
	movq	%r15, %rdi
	movl	%r12d, %r8d
	pushq	$1
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	pushq	partA__align.intwork(%rip)
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	addq	$16, %rsp
.Lcfi52:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB4_86
.LBB4_74:
	movq	424(%rsp), %rdi
	movslq	(%rdi), %rdi
	movslq	392(%rsp), %rbp
	addq	%rdi, %rbp
	movq	(%rcx,%rbp,8), %rdi
	movss	(%rdi,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	movl	$1, %edi
	cmpq	$1, %rdx
	je	.LBB4_78
.LBB4_76:                               # %.lr.ph.i.new
	subq	%rdi, %rdx
	leaq	4(%rsi,%rdi,4), %rsi
	movq	424(%rsp), %rbp
	leaq	4(%rbp,%rdi,4), %rdi
	movslq	392(%rsp), %rbp
	.p2align	4, 0x90
.LBB4_77:                               # =>This Inner Loop Header: Depth=1
	movslq	-4(%rdi), %rbx
	addq	%rbp, %rbx
	movq	(%rcx,%rbx,8), %rbx
	movss	(%rbx,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movslq	(%rdi), %rbx
	addq	%rbp, %rbx
	movq	(%rcx,%rbx,8), %rbx
	movss	(%rbx,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rdx
	jne	.LBB4_77
.LBB4_78:                               # %part_imp_match_out_vead_tate_gapmap.exit
	movq	partA__align.cpmx1(%rip), %rsi
	movq	partA__align.cpmx2(%rip), %rdx
	movq	partA__align.floatwork(%rip), %r9
	movl	$0, %ecx
	movq	%r15, %rdi
	movl	%r12d, %r8d
	pushq	$1
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	pushq	partA__align.intwork(%rip)
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	callq	match_calc
	addq	$16, %rsp
.Lcfi55:
	.cfi_adjust_cfa_offset -16
	testl	%r12d, %r12d
	jle	.LBB4_86
# BB#79:                                # %.lr.ph.i361
	movq	424(%rsp), %rax
	movslq	(%rax), %rax
	movslq	392(%rsp), %rcx
	addq	%rax, %rcx
	movq	impmtx(%rip), %rax
	movq	(%rax,%rcx,8), %rax
	movl	%r12d, %ecx
	testb	$1, %r12b
	jne	.LBB4_81
# BB#80:
	xorl	%esi, %esi
	cmpq	$1, %rcx
	jne	.LBB4_83
	jmp	.LBB4_86
.LBB4_81:
	movq	432(%rsp), %rdx
	movslq	(%rdx), %rdx
	movslq	%r14d, %rsi
	addq	%rdx, %rsi
	movss	(%rax,%rsi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%r15), %xmm0
	movss	%xmm0, (%r15)
	movl	$1, %esi
	cmpq	$1, %rcx
	je	.LBB4_86
.LBB4_83:                               # %.lr.ph.i361.new
	subq	%rsi, %rcx
	leaq	4(%r15,%rsi,4), %rdx
	movq	432(%rsp), %rdi
	leaq	4(%rdi,%rsi,4), %rsi
	movslq	%r14d, %rdi
	.p2align	4, 0x90
.LBB4_84:                               # =>This Inner Loop Header: Depth=1
	movslq	-4(%rsi), %rbp
	addq	%rdi, %rbp
	movss	(%rax,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rdx), %xmm0
	movss	%xmm0, -4(%rdx)
	movslq	(%rsi), %rbp
	addq	%rdi, %rbp
	movss	(%rax,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB4_84
.LBB4_86:                               # %part_imp_match_out_vead_gapmap.exit
	movl	outgap(%rip), %r11d
	cmpl	$1, %r11d
	jne	.LBB4_87
# BB#97:                                # %.preheader412
	movl	%r11d, 120(%rsp)        # 4-byte Spill
	testl	%r13d, %r13d
	jle	.LBB4_112
# BB#98:                                # %.lr.ph459
	movq	partA__align.ogcp1(%rip), %rax
	movq	partA__align.fgcp1(%rip), %r8
	movq	partA__align.initverticalw(%rip), %r9
	leaq	1(%r13), %rbx
	movl	%ebx, %ecx
	leaq	-1(%rcx), %r11
	cmpq	$7, %r11
	jbe	.LBB4_99
# BB#105:                               # %min.iters.checked669
	movl	%r13d, %r10d
	andl	$7, %r10d
	movq	%r11, %rdi
	subq	%r10, %rdi
	je	.LBB4_99
# BB#106:                               # %vector.memcheck688
	movq	%r15, 32(%rsp)          # 8-byte Spill
	leaq	4(%r9), %rdx
	leaq	(%r9,%rcx,4), %rbp
	leaq	-4(%r8,%rcx,4), %rsi
	cmpq	%rax, %rdx
	sbbb	%r14b, %r14b
	cmpq	%rbp, %rax
	sbbb	%r15b, %r15b
	andb	%r14b, %r15b
	cmpq	%rsi, %rdx
	sbbb	%sil, %sil
	cmpq	%rbp, %r8
	sbbb	%bpl, %bpl
	movl	$1, %edx
	testb	$1, %r15b
	jne	.LBB4_107
# BB#108:                               # %vector.memcheck688
	andb	%bpl, %sil
	andb	$1, %sil
	movq	32(%rsp), %r15          # 8-byte Reload
	jne	.LBB4_100
# BB#109:                               # %vector.body665.preheader
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r8), %rbp
	leaq	20(%r9), %rsi
	leaq	1(%rdi), %rdx
	.p2align	4, 0x90
.LBB4_110:                              # %vector.body665
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbp), %xmm1
	movups	(%rbp), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rsi), %xmm3
	movups	(%rsi), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rsi)
	movups	%xmm4, (%rsi)
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB4_110
# BB#111:                               # %middle.block666
	testq	%r10, %r10
	jne	.LBB4_100
	jmp	.LBB4_112
.LBB4_87:                               # %.preheader415
	testl	%r12d, %r12d
	jle	.LBB4_94
# BB#88:                                # %.lr.ph463
	movslq	offset(%rip), %rax
	leaq	1(%r12), %rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB4_89
# BB#90:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI4_5(%rip), %xmm0
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%r15)
	movl	$2, %edx
	cmpq	$2, %rcx
	jne	.LBB4_92
	jmp	.LBB4_94
.LBB4_99:
	movl	$1, %edx
.LBB4_100:                              # %scalar.ph667.preheader
	subl	%edx, %ebx
	testb	$1, %bl
	movq	%rdx, %rsi
	je	.LBB4_102
# BB#101:                               # %scalar.ph667.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%r8,%rdx,4), %xmm0
	addss	(%r9,%rdx,4), %xmm0
	movss	%xmm0, (%r9,%rdx,4)
	leaq	1(%rdx), %rsi
.LBB4_102:                              # %scalar.ph667.prol.loopexit
	cmpq	%rdx, %r11
	je	.LBB4_112
# BB#103:                               # %scalar.ph667.preheader.new
	subq	%rsi, %rcx
	leaq	(%r8,%rsi,4), %rdx
	leaq	4(%r9,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB4_104:                              # %scalar.ph667
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rdx), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB4_104
.LBB4_112:                              # %.preheader411
	testl	%r12d, %r12d
	jle	.LBB4_113
# BB#114:                               # %.lr.ph457
	movq	partA__align.ogcp2(%rip), %rax
	movq	partA__align.fgcp2(%rip), %r8
	leaq	1(%r12), %rdi
	movl	%edi, %ecx
	leaq	-1(%rcx), %r10
	cmpq	$7, %r10
	movl	120(%rsp), %r11d        # 4-byte Reload
	jbe	.LBB4_115
# BB#121:                               # %min.iters.checked706
	movl	%r12d, %r9d
	andl	$7, %r9d
	movq	%r10, %rsi
	subq	%r9, %rsi
	je	.LBB4_115
# BB#122:                               # %vector.memcheck727
	leaq	4(%r15), %rbx
	leaq	(%r15,%rcx,4), %rbp
	leaq	-4(%r8,%rcx,4), %r11
	cmpq	%rax, %rbx
	sbbb	%r14b, %r14b
	cmpq	%rbp, %rax
	sbbb	%dl, %dl
	andb	%r14b, %dl
	cmpq	%r11, %rbx
	sbbb	%r14b, %r14b
	cmpq	%rbp, %r8
	sbbb	%r11b, %r11b
	movl	$1, %ebx
	testb	$1, %dl
	jne	.LBB4_123
# BB#124:                               # %vector.memcheck727
	andb	%r11b, %r14b
	andb	$1, %r14b
	movl	120(%rsp), %r11d        # 4-byte Reload
	jne	.LBB4_116
# BB#125:                               # %vector.body702.preheader
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r8), %rbp
	leaq	20(%r15), %rdx
	leaq	1(%rsi), %rbx
	.p2align	4, 0x90
.LBB4_126:                              # %vector.body702
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbp), %xmm1
	movups	(%rbp), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	-16(%rdx), %xmm3
	movups	(%rdx), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm2, %xmm4
	movups	%xmm3, -16(%rdx)
	movups	%xmm4, (%rdx)
	addq	$32, %rbp
	addq	$32, %rdx
	addq	$-8, %rsi
	jne	.LBB4_126
# BB#127:                               # %middle.block703
	testq	%r9, %r9
	jne	.LBB4_116
	jmp	.LBB4_128
.LBB4_115:
	movl	$1, %ebx
.LBB4_116:                              # %scalar.ph704.preheader
	subl	%ebx, %edi
	testb	$1, %dil
	movq	%rbx, %rsi
	je	.LBB4_118
# BB#117:                               # %scalar.ph704.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%r8,%rbx,4), %xmm0
	addss	(%r15,%rbx,4), %xmm0
	movss	%xmm0, (%r15,%rbx,4)
	leaq	1(%rbx), %rsi
.LBB4_118:                              # %scalar.ph704.prol.loopexit
	cmpq	%rbx, %r10
	je	.LBB4_128
# BB#119:                               # %scalar.ph704.preheader.new
	subq	%rsi, %rcx
	leaq	(%r8,%rsi,4), %rdx
	leaq	4(%r15,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB4_120:                              # %scalar.ph704
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rdx), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB4_120
	jmp	.LBB4_128
.LBB4_113:
	movb	$1, 64(%rsp)            # 1-byte Folded Spill
	movl	120(%rsp), %r11d        # 4-byte Reload
	jmp	.LBB4_148
.LBB4_89:
	movl	$1, %edx
	cmpq	$2, %rcx
	je	.LBB4_94
.LBB4_92:                               # %.lr.ph463.new
	subq	%rdx, %rcx
	leaq	4(%r15,%rdx,4), %rsi
	leaq	1(%rdx), %rdi
	imull	%eax, %edx
	imull	%eax, %edi
	addl	%eax, %eax
	xorl	%ebp, %ebp
	movsd	.LCPI4_4(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB4_93:                               # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addl	%eax, %ebp
	addq	$-2, %rcx
	jne	.LBB4_93
.LBB4_94:                               # %.preheader413
	testl	%r13d, %r13d
	jle	.LBB4_128
# BB#95:                                # %.lr.ph461
	movq	partA__align.initverticalw(%rip), %rsi
	movslq	offset(%rip), %rax
	leaq	1(%r13), %rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB4_96
# BB#130:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI4_5(%rip), %xmm0
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%rsi)
	movl	$2, %edx
	cmpq	$2, %rcx
	jne	.LBB4_132
	jmp	.LBB4_128
.LBB4_96:
	movl	$1, %edx
	cmpq	$2, %rcx
	je	.LBB4_128
.LBB4_132:                              # %.lr.ph461.new
	subq	%rdx, %rcx
	leaq	4(%rsi,%rdx,4), %rsi
	leaq	1(%rdx), %rdi
	imull	%eax, %edx
	imull	%eax, %edi
	addl	%eax, %eax
	xorl	%ebp, %ebp
	movsd	.LCPI4_4(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB4_133:                              # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addl	%eax, %ebp
	addq	$-2, %rcx
	jne	.LBB4_133
.LBB4_128:                              # %.preheader410
	testl	%r12d, %r12d
	setle	64(%rsp)                # 1-byte Folded Spill
	jle	.LBB4_129
# BB#134:                               # %.lr.ph454
	movq	partA__align.ogcp1(%rip), %rdx
	leaq	4(%rdx), %rax
	movq	partA__align.m(%rip), %r9
	movq	partA__align.mp(%rip), %r8
	leaq	1(%r12), %r14
	movl	%r14d, %ecx
	leaq	-1(%rcx), %r11
	cmpq	$7, %r11
	jbe	.LBB4_135
# BB#141:                               # %min.iters.checked747
	movl	%r12d, %r10d
	andl	$7, %r10d
	movq	%r11, %rdi
	subq	%r10, %rdi
	je	.LBB4_135
# BB#142:                               # %vector.memcheck768
	leaq	4(%r9), %rbx
	leaq	(%r9,%rcx,4), %rsi
	leaq	-4(%r15,%rcx,4), %rbp
	cmpq	%rbp, %rbx
	sbbb	%bpl, %bpl
	cmpq	%rsi, %r15
	sbbb	%bl, %bl
	andb	%bpl, %bl
	cmpq	%rdx, %r9
	sbbb	%dl, %dl
	cmpq	%rsi, %rax
	sbbb	%bpl, %bpl
	movl	$1, %esi
	testb	$1, %bl
	jne	.LBB4_136
# BB#143:                               # %vector.memcheck768
	andb	%bpl, %dl
	andb	$1, %dl
	jne	.LBB4_136
# BB#144:                               # %vector.body743.preheader
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r15), %rbp
	leaq	20(%r9), %rdx
	leaq	20(%r8), %rbx
	xorps	%xmm1, %xmm1
	leaq	1(%rdi), %rsi
	.p2align	4, 0x90
.LBB4_145:                              # %vector.body743
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbp), %xmm2
	movups	(%rbp), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm0, %xmm3
	movups	%xmm2, -16(%rdx)
	movups	%xmm3, (%rdx)
	movups	%xmm1, -16(%rbx)
	movups	%xmm1, (%rbx)
	addq	$32, %rbp
	addq	$32, %rdx
	addq	$32, %rbx
	addq	$-8, %rdi
	jne	.LBB4_145
# BB#146:                               # %middle.block744
	testq	%r10, %r10
	jne	.LBB4_136
	jmp	.LBB4_147
.LBB4_135:
	movl	$1, %esi
.LBB4_136:                              # %scalar.ph745.preheader
	subl	%esi, %r14d
	testb	$1, %r14b
	movq	%rsi, %rdi
	je	.LBB4_138
# BB#137:                               # %scalar.ph745.prol
	movss	-4(%r15,%rsi,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, (%r9,%rsi,4)
	movl	$0, (%r8,%rsi,4)
	leaq	1(%rsi), %rdi
.LBB4_138:                              # %scalar.ph745.prol.loopexit
	cmpq	%rsi, %r11
	je	.LBB4_147
# BB#139:                               # %scalar.ph745.preheader.new
	subq	%rdi, %rcx
	leaq	(%r15,%rdi,4), %rdx
	leaq	4(%r9,%rdi,4), %rsi
	leaq	4(%r8,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB4_140:                              # %scalar.ph745
                                        # =>This Inner Loop Header: Depth=1
	movss	-4(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, -4(%rsi)
	movl	$0, -4(%rdi)
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, (%rsi)
	movl	$0, (%rdi)
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rcx
	jne	.LBB4_140
.LBB4_147:                              # %._crit_edge455.loopexit
	movl	outgap(%rip), %r11d
	jmp	.LBB4_148
.LBB4_129:
	movb	$1, 64(%rsp)            # 1-byte Folded Spill
.LBB4_148:                              # %._crit_edge455
	movq	%r12, %rax
	shlq	$32, %rax
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	addq	%rax, %rsi
	movq	%rsi, %rax
	sarq	$30, %rax
	movl	(%r15,%rax), %eax
	movq	partA__align.lastverticalw(%rip), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movl	%eax, (%rcx)
	cmpl	$1, %r11d
	movl	%r13d, %eax
	sbbl	$-1, %eax
	xorpd	%xmm5, %xmm5
	cmpl	$2, %eax
	movl	%r11d, 120(%rsp)        # 4-byte Spill
	jl	.LBB4_149
# BB#150:                               # %.lr.ph447
	sarq	$32, %rsi
	cmpq	$0, 376(%rsp)
	setne	%r8b
	testl	%r12d, %r12d
	setg	%dl
	movq	partA__align.initverticalw(%rip), %rbp
	movq	partA__align.cpmx1(%rip), %rdi
	movq	partA__align.floatwork(%rip), %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movq	partA__align.intwork(%rip), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	andb	%r8b, %dl
	movb	%dl, 168(%rsp)          # 1-byte Spill
	movq	impmtx(%rip), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movl	%r12d, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	partA__align.ogcp2(%rip), %r10
	movq	partA__align.ijp(%rip), %rcx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movq	partA__align.m(%rip), %r8
	movq	partA__align.mp(%rip), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	partA__align.fgcp2(%rip), %r9
	movq	partA__align.fgcp1(%rip), %rcx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%r12d, %eax
	andl	$1, %eax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	xorpd	%xmm5, %xmm5
	movq	partA__align.ogcp1(%rip), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	432(%rsp), %rax
	leaq	4(%rax), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	leal	-1(%r12), %r11d
	movl	$1, %edx
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_151:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_152 Depth 2
                                        #       Child Loop BB4_153 Depth 3
                                        #     Child Loop BB4_156 Depth 2
                                        #       Child Loop BB4_158 Depth 3
                                        #     Child Loop BB4_166 Depth 2
                                        #     Child Loop BB4_169 Depth 2
	movq	%rcx, %r14
	leaq	-1(%rdx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rbp, %r13
	movl	-4(%rbp,%rdx,4), %eax
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movl	%eax, (%r15)
	movl	$n_dis_consweight_multi, %ecx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_152:                              #   Parent Loop BB4_151 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_153 Depth 3
	movl	$0, 208(%rsp,%rsi,4)
	xorps	%xmm0, %xmm0
	movl	$1, %eax
	movq	%rcx, %rbx
	.p2align	4, 0x90
.LBB4_153:                              #   Parent Loop BB4_151 Depth=1
                                        #     Parent Loop BB4_152 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-8(%rdi,%rax,8), %rbp
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rbp,%rdx,4), %xmm1
	addss	%xmm0, %xmm1
	movss	104(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movq	(%rdi,%rax,8), %rbp
	mulss	(%rbp,%rdx,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$208, %rbx
	addq	$2, %rax
	cmpq	$27, %rax
	jne	.LBB4_153
# BB#154:                               #   in Loop: Header=BB4_152 Depth=2
	movss	%xmm0, 208(%rsp,%rsi,4)
	incq	%rsi
	addq	$4, %rcx
	cmpq	$26, %rsi
	jne	.LBB4_152
# BB#155:                               # %.preheader.i
                                        #   in Loop: Header=BB4_151 Depth=1
	testl	%r12d, %r12d
	movl	%r12d, %r15d
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	128(%rsp), %r12         # 8-byte Reload
	movq	%r14, %rbx
	je	.LBB4_160
	.p2align	4, 0x90
.LBB4_156:                              # %.lr.ph84.i
                                        #   Parent Loop BB4_151 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_158 Depth 3
	decl	%r15d
	movl	$0, (%rbx)
	movq	(%rcx), %rax
	movl	(%rax), %ebp
	testl	%ebp, %ebp
	js	.LBB4_159
# BB#157:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB4_156 Depth=2
	movq	(%r12), %rsi
	addq	$4, %rax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB4_158:                              # %.lr.ph.i369
                                        #   Parent Loop BB4_151 Depth=1
                                        #     Parent Loop BB4_156 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%ebp, %rbp
	movss	208(%rsp,%rbp,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rsi), %xmm1
	addq	$4, %rsi
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rbx)
	movl	(%rax), %ebp
	addq	$4, %rax
	testl	%ebp, %ebp
	jns	.LBB4_158
.LBB4_159:                              # %._crit_edge.i370
                                        #   in Loop: Header=BB4_156 Depth=2
	addq	$8, %rcx
	addq	$8, %r12
	addq	$4, %rbx
	testl	%r15d, %r15d
	jne	.LBB4_156
.LBB4_160:                              # %match_calc.exit
                                        #   in Loop: Header=BB4_151 Depth=1
	cmpb	$0, 168(%rsp)           # 1-byte Folded Reload
	movl	408(%rsp), %r12d
	je	.LBB4_167
# BB#161:                               # %.lr.ph.i399
                                        #   in Loop: Header=BB4_151 Depth=1
	movq	424(%rsp), %rax
	movslq	(%rax,%rdx,4), %rax
	movslq	392(%rsp), %rcx
	addq	%rax, %rcx
	cmpq	$0, 152(%rsp)           # 8-byte Folded Reload
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rcx,8), %r15
	jne	.LBB4_163
# BB#162:                               #   in Loop: Header=BB4_151 Depth=1
	xorl	%ebx, %ebx
	cmpq	$1, 16(%rsp)            # 8-byte Folded Reload
	jne	.LBB4_165
	jmp	.LBB4_167
	.p2align	4, 0x90
.LBB4_163:                              #   in Loop: Header=BB4_151 Depth=1
	movq	432(%rsp), %rax
	movslq	(%rax), %rax
	movslq	%r12d, %rsi
	addq	%rax, %rsi
	movss	(%r15,%rsi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%r14), %xmm0
	movss	%xmm0, (%r14)
	movl	$1, %ebx
	cmpq	$1, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB4_167
.LBB4_165:                              # %.lr.ph.i399.new
                                        #   in Loop: Header=BB4_151 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	subq	%rbx, %rax
	leaq	4(%r14,%rbx,4), %rsi
	movq	184(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB4_166:                              #   Parent Loop BB4_151 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	-4(%rbx), %rbp
	movslq	%r12d, %rcx
	addq	%rcx, %rbp
	movss	(%r15,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movslq	(%rbx), %rbp
	addq	%rcx, %rbp
	movss	(%r15,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addq	$8, %rsi
	addq	$8, %rbx
	addq	$-2, %rax
	jne	.LBB4_166
.LBB4_167:                              # %part_imp_match_out_vead_gapmap.exit404
                                        #   in Loop: Header=BB4_151 Depth=1
	movq	%r13, %rbp
	movl	(%rbp,%rdx,4), %eax
	movl	%eax, (%r14)
	movq	48(%rsp), %rcx          # 8-byte Reload
	movss	(%rcx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%r10), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm0
	movss	%xmm0, partA__align.mi(%rip)
	xorl	%r13d, %r13d
	cmpb	$0, 64(%rsp)            # 1-byte Folded Reload
	jne	.LBB4_179
# BB#168:                               # %.lr.ph440.preheader
                                        #   in Loop: Header=BB4_151 Depth=1
	movq	176(%rsp), %rax         # 8-byte Reload
	movss	-4(%rax,%rdx,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movq	192(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%rdx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdx,8), %rsi
	xorl	%r12d, %r12d
	movl	$-1, %r15d
	xorl	%r13d, %r13d
	jmp	.LBB4_169
	.p2align	4, 0x90
.LBB4_178:                              # %..lr.ph440_crit_edge
                                        #   in Loop: Header=BB4_169 Depth=2
	movss	4(%rcx,%r12,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	incq	%r12
	decl	%r15d
.LBB4_169:                              # %.lr.ph440
                                        #   Parent Loop BB4_151 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$0, 4(%rsi,%r12,4)
	movss	(%r9,%r12,4), %xmm4     # xmm4 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	ucomiss	%xmm3, %xmm4
	movaps	%xmm3, %xmm5
	jbe	.LBB4_171
# BB#170:                               #   in Loop: Header=BB4_169 Depth=2
	leal	(%r13,%r15), %eax
	movl	%eax, 4(%rsi,%r12,4)
	movaps	%xmm4, %xmm5
.LBB4_171:                              #   in Loop: Header=BB4_169 Depth=2
	addss	4(%r10,%r12,4), %xmm3
	ucomiss	%xmm0, %xmm3
	jb	.LBB4_173
# BB#172:                               #   in Loop: Header=BB4_169 Depth=2
	movss	%xmm3, partA__align.mi(%rip)
	movaps	%xmm3, %xmm0
	movl	%r12d, %r13d
.LBB4_173:                              #   in Loop: Header=BB4_169 Depth=2
	movss	4(%r8,%r12,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	addss	%xmm3, %xmm4
	ucomiss	%xmm5, %xmm4
	jbe	.LBB4_175
# BB#174:                               #   in Loop: Header=BB4_169 Depth=2
	movl	%edx, %eax
	movq	32(%rsp), %rbx          # 8-byte Reload
	subl	4(%rbx,%r12,4), %eax
	movl	%eax, 4(%rsi,%r12,4)
	movaps	%xmm4, %xmm5
.LBB4_175:                              #   in Loop: Header=BB4_169 Depth=2
	movss	(%rcx,%r12,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm4
	ucomiss	%xmm3, %xmm4
	jb	.LBB4_177
# BB#176:                               #   in Loop: Header=BB4_169 Depth=2
	movss	%xmm4, 4(%r8,%r12,4)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, 4(%rax,%r12,4)
.LBB4_177:                              #   in Loop: Header=BB4_169 Depth=2
	movss	4(%r14,%r12,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm3
	movss	%xmm3, 4(%r14,%r12,4)
	cmpl	%r12d, %r11d
	jne	.LBB4_178
.LBB4_179:                              # %._crit_edge441
                                        #   in Loop: Header=BB4_151 Depth=1
	movq	112(%rsp), %rsi         # 8-byte Reload
	movl	(%r14,%rsi,4), %eax
	movq	88(%rsp), %rbx          # 8-byte Reload
	movl	%eax, (%rbx,%rdx,4)
	incq	%rdx
	cmpq	56(%rsp), %rdx          # 8-byte Folded Reload
	movq	%r14, %r15
	movq	96(%rsp), %r12          # 8-byte Reload
	jne	.LBB4_151
# BB#180:                               # %._crit_edge448
	movl	%r13d, partA__align.mpi(%rip)
	movq	80(%rsp), %r8           # 8-byte Reload
	movq	144(%rsp), %r13         # 8-byte Reload
	movl	120(%rsp), %r11d        # 4-byte Reload
	jmp	.LBB4_181
.LBB4_149:
	movq	%r15, %r14
	movq	80(%rsp), %r8           # 8-byte Reload
.LBB4_181:
	testl	%r11d, %r11d
	movq	88(%rsp), %rsi          # 8-byte Reload
	je	.LBB4_182
.LBB4_196:                              # %.loopexit
	movq	%rsi, %rbx
	movss	%xmm5, 204(%rsp)        # 4-byte Spill
	movq	partA__align.mseq1(%rip), %r12
	movq	partA__align.mseq2(%rip), %r15
	movq	partA__align.ijp(%rip), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movq	(%r8), %rdi
	callq	strlen
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpq	$0, 376(%rsp)
	je	.LBB4_337
# BB#197:
	cmpl	$1, 120(%rsp)           # 4-byte Folded Reload
	movq	136(%rsp), %r11         # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	je	.LBB4_198
# BB#209:
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	56(%rsp), %rsi          # 8-byte Reload
	testl	%esi, %esi
	jle	.LBB4_215
# BB#210:                               # %.lr.ph75.i
	movslq	%esi, %rax
	movslq	16(%rsp), %rcx          # 4-byte Folded Reload
	movl	%eax, %edx
	addq	$4, %rbx
	movq	%rbx, %rbp
	decq	%rdx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movaps	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm0
	jb	.LBB4_213
	jmp	.LBB4_212
	.p2align	4, 0x90
.LBB4_483:                              # %._crit_edge.i380
                                        #   in Loop: Header=BB4_213 Depth=1
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rbp
	decq	%rdx
	decl	%esi
	ucomiss	%xmm1, %xmm0
	jb	.LBB4_213
.LBB4_212:
	movq	(%r9,%rax,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movaps	%xmm0, %xmm1
.LBB4_213:                              # =>This Inner Loop Header: Depth=1
	testq	%rdx, %rdx
	jne	.LBB4_483
# BB#214:
	movaps	%xmm1, %xmm0
.LBB4_215:                              # %.preheader16.i
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_198
# BB#216:                               # %.lr.ph71.i
	movslq	56(%rsp), %rax          # 4-byte Folded Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movslq	%esi, %rcx
	movl	%ecx, %edx
	testb	$1, %sil
	jne	.LBB4_218
# BB#217:
	xorl	%esi, %esi
	cmpq	$1, %rdx
	jne	.LBB4_222
	jmp	.LBB4_198
.LBB4_182:                              # %.preheader409
	movq	%rsi, %r9
	cmpb	$0, 64(%rsp)            # 1-byte Folded Reload
	jne	.LBB4_189
# BB#183:                               # %.lr.ph426
	movslq	offset(%rip), %rax
	movq	%r12, %rdx
	incq	%rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB4_184
# BB#185:
	leal	-1(%r12), %edx
	imull	%eax, %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	mulsd	.LCPI4_5(%rip), %xmm0
	movss	4(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%r14)
	movl	$2, %ebp
	cmpq	$2, %rcx
	jne	.LBB4_187
	jmp	.LBB4_189
.LBB4_337:
	cmpl	$1, 120(%rsp)           # 4-byte Folded Reload
	movq	136(%rsp), %r9          # 8-byte Reload
	movq	176(%rsp), %r10         # 8-byte Reload
	movq	56(%rsp), %r11          # 8-byte Reload
	je	.LBB4_338
# BB#349:
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	testl	%r11d, %r11d
	jle	.LBB4_355
# BB#350:                               # %.lr.ph74.i
	movslq	%r11d, %rax
	movslq	16(%rsp), %rcx          # 4-byte Folded Reload
	movl	%eax, %edx
	addq	$4, %rbx
	movq	%rbx, %rbp
	decq	%rdx
	movl	%r11d, %esi
	movaps	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm0
	jb	.LBB4_353
	jmp	.LBB4_352
	.p2align	4, 0x90
.LBB4_484:                              # %._crit_edge.i
                                        #   in Loop: Header=BB4_353 Depth=1
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rbp
	decq	%rdx
	decl	%esi
	ucomiss	%xmm1, %xmm0
	jb	.LBB4_353
.LBB4_352:
	movq	(%r10,%rax,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movaps	%xmm0, %xmm1
.LBB4_353:                              # =>This Inner Loop Header: Depth=1
	testq	%rdx, %rdx
	jne	.LBB4_484
# BB#354:
	movaps	%xmm1, %xmm0
.LBB4_355:                              # %.preheader15.i
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_338
# BB#356:                               # %.lr.ph70.i
	movslq	%r11d, %rax
	movq	16(%rsp), %rsi          # 8-byte Reload
	movslq	%esi, %rcx
	movl	%ecx, %edx
	testb	$1, %sil
	jne	.LBB4_358
# BB#357:
	xorl	%esi, %esi
	cmpq	$1, %rdx
	jne	.LBB4_362
	jmp	.LBB4_338
.LBB4_20:                               # %vector.body.preheader
	leaq	16(%rcx), %rax
	leaq	16(%rdx), %rbx
	movq	%rsi, %rdi
	.p2align	4, 0x90
.LBB4_21:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rax), %xmm0
	movupd	(%rax), %xmm1
	movupd	%xmm0, -16(%rbx)
	movupd	%xmm1, (%rbx)
	addq	$32, %rax
	addq	$32, %rbx
	addq	$-4, %rdi
	jne	.LBB4_21
# BB#22:                                # %middle.block
	testl	%r9d, %r9d
	jne	.LBB4_11
	jmp	.LBB4_23
.LBB4_35:                               # %vector.body574.preheader
	leaq	16(%r11,%r9,8), %rax
	leaq	16(%rsi), %rcx
	movq	%rdi, %rdx
	.p2align	4, 0x90
.LBB4_36:                               # %vector.body574
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rax), %xmm0
	movupd	(%rax), %xmm1
	movupd	%xmm0, -16(%rcx)
	movupd	%xmm1, (%rcx)
	addq	$32, %rax
	addq	$32, %rcx
	addq	$-4, %rdx
	jne	.LBB4_36
# BB#37:                                # %middle.block575
	testl	%ebp, %ebp
	jne	.LBB4_26
	jmp	.LBB4_38
.LBB4_56:                               # %vector.ph623
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movapd	.LCPI4_1(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI4_2(%rip), %xmm3   # xmm3 = [5.000000e-01,5.000000e-01]
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB4_57:                               # %vector.body605
                                        # =>This Inner Loop Header: Depth=1
	cvtps2pd	8(%rcx), %xmm4
	cvtps2pd	(%rcx), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rcx)
	cvtps2pd	8(%rdx), %xmm4
	cvtps2pd	(%rdx), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rdx)
	addq	$16, %rcx
	addq	$16, %rdx
	addq	$-4, %rsi
	jne	.LBB4_57
# BB#58:                                # %middle.block606
	testq	%rdi, %rdi
	jne	.LBB4_51
	jmp	.LBB4_59
.LBB4_67:                               # %vector.ph653
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movapd	.LCPI4_1(%rip), %xmm2   # xmm2 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI4_2(%rip), %xmm3   # xmm3 = [5.000000e-01,5.000000e-01]
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB4_68:                               # %vector.body635
                                        # =>This Inner Loop Header: Depth=1
	cvtps2pd	8(%rcx), %xmm4
	cvtps2pd	(%rcx), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rcx)
	cvtps2pd	8(%rdx), %xmm4
	cvtps2pd	(%rdx), %xmm5
	movapd	%xmm2, %xmm6
	subpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm5
	subpd	%xmm4, %xmm5
	mulpd	%xmm3, %xmm5
	mulpd	%xmm3, %xmm6
	mulpd	%xmm1, %xmm6
	mulpd	%xmm1, %xmm5
	cvtpd2ps	%xmm5, %xmm4
	cvtpd2ps	%xmm6, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	movupd	%xmm5, (%rdx)
	addq	$16, %rcx
	addq	$16, %rdx
	addq	$-4, %rsi
	jne	.LBB4_68
# BB#69:                                # %middle.block636
	testq	%rdi, %rdi
	jne	.LBB4_62
	jmp	.LBB4_70
.LBB4_218:
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB4_220
# BB#219:
	movl	$1, %esi
	cmpq	$1, %rdx
	jne	.LBB4_222
	jmp	.LBB4_198
.LBB4_184:
	movl	$1, %ebp
	cmpq	$2, %rcx
	je	.LBB4_189
.LBB4_187:                              # %.lr.ph426.new
	subq	%rbp, %rcx
	movl	%r12d, %edx
	subl	%ebp, %edx
	imull	%eax, %edx
	leaq	4(%r14,%rbp,4), %rsi
	leal	-1(%r12), %edi
	subl	%ebp, %edi
	imull	%eax, %edi
	addl	%eax, %eax
	xorl	%ebp, %ebp
	movsd	.LCPI4_4(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB4_188:                              # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	subl	%eax, %ebp
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB4_188
.LBB4_189:                              # %.preheader408
	testl	%r13d, %r13d
	movq	%r9, %rsi
	jle	.LBB4_196
# BB#190:                               # %.lr.ph424
	xorps	%xmm0, %xmm0
	cvtsi2sdl	offset(%rip), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r13d, %xmm1
	incq	%r13
	movl	%r13d, %eax
	testb	$1, %r13b
	jne	.LBB4_191
# BB#192:
	movsd	.LCPI4_5(%rip), %xmm2   # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movq	%rsi, %rcx
	movss	4(%rcx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm2, %xmm3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
	movss	%xmm2, 4(%rcx)
	movl	$2, %ecx
	cmpq	$2, %rax
	je	.LBB4_196
	jmp	.LBB4_194
.LBB4_191:
	movl	$1, %ecx
	cmpq	$2, %rax
	je	.LBB4_196
.LBB4_194:
	movsd	.LCPI4_4(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB4_195:                              # =>This Inner Loop Header: Depth=1
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%ecx, %xmm3
	mulsd	%xmm2, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	movss	(%rsi,%rcx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movss	%xmm3, (%rsi,%rcx,4)
	leal	1(%rcx), %edx
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%edx, %xmm3
	mulsd	%xmm2, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	movss	4(%rsi,%rcx,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movss	%xmm3, 4(%rsi,%rcx,4)
	addq	$2, %rcx
	cmpq	%rax, %rcx
	jne	.LBB4_195
	jmp	.LBB4_196
.LBB4_358:
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB4_360
# BB#359:
	movl	$1, %esi
	cmpq	$1, %rdx
	jne	.LBB4_362
	jmp	.LBB4_338
.LBB4_220:
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	negl	%esi
	movq	(%r9,%rax,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movl	$1, %esi
	movaps	%xmm1, %xmm0
	cmpq	$1, %rdx
	je	.LBB4_198
.LBB4_222:                              # %.lr.ph71.i.new
	movq	16(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	negl	%edi
	.p2align	4, 0x90
.LBB4_223:                              # =>This Inner Loop Header: Depth=1
	movss	(%r14,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB4_225
# BB#224:                               #   in Loop: Header=BB4_223 Depth=1
	leal	(%rdi,%rsi), %ebp
	movq	(%r9,%rax,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movaps	%xmm1, %xmm0
.LBB4_225:                              #   in Loop: Header=BB4_223 Depth=1
	movss	4(%r14,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB4_227
# BB#226:                               #   in Loop: Header=BB4_223 Depth=1
	leal	1(%rdi,%rsi), %ebp
	movq	(%r9,%rax,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movaps	%xmm1, %xmm0
.LBB4_227:                              #   in Loop: Header=BB4_223 Depth=1
	addq	$2, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB4_223
.LBB4_198:                              # %.preheader15.i371
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	movl	12(%rsp), %r8d          # 4-byte Reload
	js	.LBB4_204
# BB#199:                               # %.lr.ph68.preheader.i
	movq	56(%rsp), %rsi          # 8-byte Reload
	incq	%rsi
	movl	%esi, %eax
	leaq	-1(%rax), %rcx
	xorl	%edx, %edx
	andq	$7, %rsi
	je	.LBB4_201
	.p2align	4, 0x90
.LBB4_200:                              # %.lr.ph68.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rdx,8), %rdi
	incq	%rdx
	movl	%edx, (%rdi)
	cmpq	%rdx, %rsi
	jne	.LBB4_200
.LBB4_201:                              # %.lr.ph68.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB4_204
# BB#202:                               # %.lr.ph68.preheader.i.new
	negq	%rax
	leaq	4(%rdx,%rax), %rax
	leaq	56(%r9,%rdx,8), %rcx
	leaq	4(%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_203:                              # %.lr.ph68.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx,%rsi,8), %rdi
	leal	(%rdx,%rsi), %ebp
	leal	-3(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-48(%rcx,%rsi,8), %rdi
	leal	-2(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-40(%rcx,%rsi,8), %rdi
	leal	-1(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-32(%rcx,%rsi,8), %rdi
	movl	%ebp, (%rdi)
	movq	-24(%rcx,%rsi,8), %rdi
	leal	1(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-16(%rcx,%rsi,8), %rdi
	leal	2(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-8(%rcx,%rsi,8), %rdi
	leal	3(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	(%rcx,%rsi,8), %rdi
	leal	4(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	leaq	8(%rax,%rsi), %rdi
	addq	$8, %rsi
	cmpq	$4, %rdi
	jne	.LBB4_203
.LBB4_204:                              # %.preheader14.i382
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	js	.LBB4_232
# BB#205:                               # %.lr.ph66.i
	movq	(%r9), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	incq	%rsi
	movl	%esi, %eax
	cmpq	$7, %rax
	jbe	.LBB4_206
# BB#228:                               # %min.iters.checked804
	andl	$7, %esi
	movq	%rax, %rbp
	subq	%rsi, %rbp
	je	.LBB4_206
# BB#229:                               # %vector.body800.preheader
	leaq	16(%rcx), %rdi
	movdqa	.LCPI4_6(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI4_7(%rip), %xmm1   # xmm1 = [4,4,4,4]
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI4_8(%rip), %xmm3   # xmm3 = [8,8,8,8]
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB4_230:                              # %vector.body800
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	pxor	%xmm2, %xmm5
	pxor	%xmm2, %xmm4
	movdqu	%xmm5, -16(%rdi)
	movdqu	%xmm4, (%rdi)
	paddd	%xmm3, %xmm0
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB4_230
# BB#231:                               # %middle.block801
	testq	%rsi, %rsi
	jne	.LBB4_207
	jmp	.LBB4_232
.LBB4_206:
	xorl	%ebp, %ebp
.LBB4_207:                              # %scalar.ph802.preheader
	movl	%ebp, %edx
	notl	%edx
	leaq	(%rcx,%rbp,4), %rcx
	subq	%rbp, %rax
	.p2align	4, 0x90
.LBB4_208:                              # %scalar.ph802
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rcx)
	decl	%edx
	addq	$4, %rcx
	decq	%rax
	jne	.LBB4_208
.LBB4_232:                              # %.preheader13.i383
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_238
# BB#233:                               # %.lr.ph64.i
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rax), %eax
	cltq
	movl	28(%rsp), %ecx          # 4-byte Reload
	leaq	-1(%rcx), %rdx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	andq	$3, %rdi
	je	.LBB4_235
	.p2align	4, 0x90
.LBB4_234:                              # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rsi,8), %rbp
	leaq	(%rbp,%rax), %rbx
	movq	%rbx, (%r12,%rsi,8)
	movb	$0, (%rbp,%rax)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB4_234
.LBB4_235:                              # %.prol.loopexit878
	cmpq	$3, %rdx
	jb	.LBB4_238
# BB#236:                               # %.lr.ph64.i.new
	subq	%rsi, %rcx
	leaq	24(%r12,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB4_237:                              # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -24(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-16(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -16(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-8(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -8(%rdx)
	movb	$0, (%rsi,%rax)
	movq	(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, (%rdx)
	movb	$0, (%rsi,%rax)
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB4_237
.LBB4_238:                              # %.preheader12.i385
	testl	%r8d, %r8d
	jle	.LBB4_244
# BB#239:                               # %.lr.ph61.i
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rax), %eax
	cltq
	movl	%r8d, %ecx
	leaq	-1(%rcx), %rdx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	andq	$3, %rdi
	je	.LBB4_241
	.p2align	4, 0x90
.LBB4_240:                              # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rsi,8), %rbp
	leaq	(%rbp,%rax), %rbx
	movq	%rbx, (%r15,%rsi,8)
	movb	$0, (%rbp,%rax)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB4_240
.LBB4_241:                              # %.prol.loopexit873
	cmpq	$3, %rdx
	jb	.LBB4_244
# BB#242:                               # %.lr.ph61.i.new
	subq	%rsi, %rcx
	leaq	24(%r15,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB4_243:                              # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -24(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-16(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -16(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-8(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -8(%rdx)
	movb	$0, (%rsi,%rax)
	movq	(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, (%rdx)
	movb	$0, (%rsi,%rax)
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB4_243
.LBB4_244:                              # %._crit_edge62.i
	movq	384(%rsp), %rax
	movl	$0, (%rax)
	movq	16(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addl	56(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 184(%rsp)         # 4-byte Spill
	movq	80(%rsp), %r8           # 8-byte Reload
	js	.LBB4_473
# BB#245:                               # %.lr.ph57.i
	movq	impmtx(%rip), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	28(%rsp), %r14d         # 4-byte Reload
	movl	12(%rsp), %r10d         # 4-byte Reload
	leaq	-1(%r10), %r13
	leaq	-1(%r14), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r14, 48(%rsp)          # 8-byte Spill
                                        # kill: %R14D<def> %R14D<kill> %R14<kill> %R14<def>
	andl	$3, %r14d
	movq	%r10, 96(%rsp)          # 8-byte Spill
                                        # kill: %R10D<def> %R10D<kill> %R10<kill> %R10<def>
	andl	$3, %r10d
	leaq	24(%r15), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	24(%r12), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	24(%r8), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	leaq	24(%r11), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	xorl	%r9d, %r9d
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 40(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB4_246:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_276 Depth 2
                                        #       Child Loop BB4_277 Depth 3
                                        #       Child Loop BB4_280 Depth 3
                                        #     Child Loop BB4_268 Depth 2
                                        #       Child Loop BB4_269 Depth 3
                                        #       Child Loop BB4_272 Depth 3
                                        #     Child Loop BB4_255 Depth 2
                                        #       Child Loop BB4_256 Depth 3
                                        #       Child Loop BB4_259 Depth 3
                                        #       Child Loop BB4_263 Depth 3
                                        #       Child Loop BB4_266 Depth 3
                                        #     Child Loop BB4_308 Depth 2
                                        #       Child Loop BB4_309 Depth 3
                                        #       Child Loop BB4_312 Depth 3
                                        #     Child Loop BB4_298 Depth 2
                                        #       Child Loop BB4_299 Depth 3
                                        #       Child Loop BB4_302 Depth 3
                                        #     Child Loop BB4_287 Depth 2
                                        #       Child Loop BB4_288 Depth 3
                                        #       Child Loop BB4_291 Depth 3
                                        #       Child Loop BB4_293 Depth 3
                                        #       Child Loop BB4_296 Depth 3
                                        #     Child Loop BB4_324 Depth 2
                                        #     Child Loop BB4_327 Depth 2
                                        #     Child Loop BB4_332 Depth 2
                                        #     Child Loop BB4_335 Depth 2
	movl	40(%rsp), %eax          # 4-byte Reload
	movl	%eax, %esi
	movl	%ecx, %edx
	movslq	%edx, %rcx
	movq	176(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movq	(%rax,%rcx,8), %rax
	movslq	%esi, %rcx
	movl	(%rax,%rcx,4), %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	testl	%eax, %eax
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	js	.LBB4_247
# BB#248:                               #   in Loop: Header=BB4_246 Depth=1
	je	.LBB4_250
# BB#249:                               #   in Loop: Header=BB4_246 Depth=1
	movl	%edx, %ecx
	subl	72(%rsp), %ecx          # 4-byte Folded Reload
	jmp	.LBB4_251
	.p2align	4, 0x90
.LBB4_247:                              #   in Loop: Header=BB4_246 Depth=1
	leal	-1(%rdx), %ecx
	jmp	.LBB4_252
	.p2align	4, 0x90
.LBB4_250:                              #   in Loop: Header=BB4_246 Depth=1
	leal	-1(%rdx), %ecx
.LBB4_251:                              #   in Loop: Header=BB4_246 Depth=1
	movl	$-1, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
.LBB4_252:                              #   in Loop: Header=BB4_246 Depth=1
	movl	%edx, %eax
	subl	%ecx, %eax
	decl	%eax
	movl	%ecx, 104(%rsp)         # 4-byte Spill
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	%rsi, 160(%rsp)         # 8-byte Spill
	je	.LBB4_283
# BB#253:                               # %.preheader9.lr.ph.i
                                        #   in Loop: Header=BB4_246 Depth=1
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_275
# BB#254:                               # %.preheader9.us.preheader.i
                                        #   in Loop: Header=BB4_246 Depth=1
	movq	%r9, 152(%rsp)          # 8-byte Spill
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movslq	%eax, %r9
	movslq	%ecx, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	jle	.LBB4_268
	.p2align	4, 0x90
.LBB4_255:                              # %.preheader9.us.i.us
                                        #   Parent Loop BB4_246 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_256 Depth 3
                                        #       Child Loop BB4_259 Depth 3
                                        #       Child Loop BB4_263 Depth 3
                                        #       Child Loop BB4_266 Depth 3
	movq	%r13, %rcx
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%r9,%rax), %r13
	xorl	%edx, %edx
	testq	%r14, %r14
	je	.LBB4_257
	.p2align	4, 0x90
.LBB4_256:                              #   Parent Loop BB4_246 Depth=1
                                        #     Parent Loop BB4_255 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r8,%rdx,8), %rsi
	movzbl	(%rsi,%r13), %ebx
	movq	(%r12,%rdx,8), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, (%r12,%rdx,8)
	movb	%bl, -1(%rsi)
	incq	%rdx
	cmpq	%rdx, %r14
	jne	.LBB4_256
.LBB4_257:                              # %.prol.loopexit835
                                        #   in Loop: Header=BB4_255 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_260
# BB#258:                               # %.preheader9.us.i.us.new
                                        #   in Loop: Header=BB4_255 Depth=2
	movq	48(%rsp), %rsi          # 8-byte Reload
	subq	%rdx, %rsi
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,8), %rbp
	movq	168(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB4_259:                              #   Parent Loop BB4_246 Depth=1
                                        #     Parent Loop BB4_255 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdx), %rbx
	movzbl	(%rbx,%r13), %ebx
	movq	-24(%rbp), %r11
	leaq	-1(%r11), %r8
	movq	%r8, -24(%rbp)
	movb	%bl, -1(%r11)
	movq	-16(%rdx), %rbx
	movzbl	(%rbx,%r13), %ebx
	movq	-16(%rbp), %rax
	leaq	-1(%rax), %rdi
	movq	%rdi, -16(%rbp)
	movb	%bl, -1(%rax)
	movq	-8(%rdx), %rax
	movzbl	(%rax,%r13), %eax
	movq	-8(%rbp), %rdi
	leaq	-1(%rdi), %rbx
	movq	%rbx, -8(%rbp)
	movb	%al, -1(%rdi)
	movq	(%rdx), %rax
	movzbl	(%rax,%r13), %eax
	movq	(%rbp), %rdi
	leaq	-1(%rdi), %rbx
	movq	%rbx, (%rbp)
	movb	%al, -1(%rdi)
	addq	$32, %rbp
	addq	$32, %rdx
	addq	$-4, %rsi
	jne	.LBB4_259
.LBB4_260:                              # %.lr.ph19.us.i.us.preheader
                                        #   in Loop: Header=BB4_255 Depth=2
	xorl	%esi, %esi
	testq	%r10, %r10
	je	.LBB4_261
# BB#262:                               # %.lr.ph19.us.i.us.prol.preheader
                                        #   in Loop: Header=BB4_255 Depth=2
	movq	136(%rsp), %r11         # 8-byte Reload
	movq	%rcx, %r13
	.p2align	4, 0x90
.LBB4_263:                              # %.lr.ph19.us.i.us.prol
                                        #   Parent Loop BB4_246 Depth=1
                                        #     Parent Loop BB4_255 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15,%rsi,8), %rax
	leaq	-1(%rax), %rdx
	movq	%rdx, (%r15,%rsi,8)
	movb	$45, -1(%rax)
	incq	%rsi
	cmpq	%rsi, %r10
	jne	.LBB4_263
	jmp	.LBB4_264
	.p2align	4, 0x90
.LBB4_261:                              #   in Loop: Header=BB4_255 Depth=2
	movq	136(%rsp), %r11         # 8-byte Reload
	movq	%rcx, %r13
.LBB4_264:                              # %.lr.ph19.us.i.us.prol.loopexit
                                        #   in Loop: Header=BB4_255 Depth=2
	cmpq	$3, %r13
	jb	.LBB4_267
# BB#265:                               # %.lr.ph19.us.i.us.preheader.new
                                        #   in Loop: Header=BB4_255 Depth=2
	movq	96(%rsp), %rdx          # 8-byte Reload
	subq	%rsi, %rdx
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB4_266:                              # %.lr.ph19.us.i.us
                                        #   Parent Loop BB4_246 Depth=1
                                        #     Parent Loop BB4_255 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rsi), %rax
	leaq	-1(%rax), %rdi
	movq	%rdi, -24(%rsi)
	movb	$45, -1(%rax)
	movq	-16(%rsi), %rax
	leaq	-1(%rax), %rdi
	movq	%rdi, -16(%rsi)
	movb	$45, -1(%rax)
	movq	-8(%rsi), %rax
	leaq	-1(%rax), %rdi
	movq	%rdi, -8(%rsi)
	movb	$45, -1(%rax)
	movq	(%rsi), %rax
	leaq	-1(%rax), %rdi
	movq	%rdi, (%rsi)
	movb	$45, -1(%rax)
	addq	$32, %rsi
	addq	$-4, %rdx
	jne	.LBB4_266
.LBB4_267:                              # %._crit_edge.us.i389.loopexit.us
                                        #   in Loop: Header=BB4_255 Depth=2
	decq	%r9
	testl	%r9d, %r9d
	movq	80(%rsp), %r8           # 8-byte Reload
	jne	.LBB4_255
	jmp	.LBB4_274
	.p2align	4, 0x90
.LBB4_268:                              # %.preheader9.us.i
                                        #   Parent Loop BB4_246 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_269 Depth 3
                                        #       Child Loop BB4_272 Depth 3
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%r9,%rax), %rsi
	xorl	%ebx, %ebx
	testq	%r14, %r14
	je	.LBB4_270
	.p2align	4, 0x90
.LBB4_269:                              #   Parent Loop BB4_246 Depth=1
                                        #     Parent Loop BB4_268 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r8,%rbx,8), %rax
	movzbl	(%rax,%rsi), %eax
	movq	(%r12,%rbx,8), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, (%r12,%rbx,8)
	movb	%al, -1(%rdx)
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB4_269
.LBB4_270:                              # %.prol.loopexit830
                                        #   in Loop: Header=BB4_268 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_273
# BB#271:                               # %.preheader9.us.i.new
                                        #   in Loop: Header=BB4_268 Depth=2
	movq	48(%rsp), %rbp          # 8-byte Reload
	subq	%rbx, %rbp
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbx,8), %rdx
	movq	168(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB4_272:                              #   Parent Loop BB4_246 Depth=1
                                        #     Parent Loop BB4_268 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rbx), %rax
	movzbl	(%rax,%rsi), %eax
	movq	-24(%rdx), %rdi
	leaq	-1(%rdi), %rcx
	movq	%rcx, -24(%rdx)
	movb	%al, -1(%rdi)
	movq	-16(%rbx), %rax
	movzbl	(%rax,%rsi), %eax
	movq	-16(%rdx), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -16(%rdx)
	movb	%al, -1(%rcx)
	movq	-8(%rbx), %rax
	movzbl	(%rax,%rsi), %eax
	movq	-8(%rdx), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -8(%rdx)
	movb	%al, -1(%rcx)
	movq	(%rbx), %rax
	movzbl	(%rax,%rsi), %eax
	movq	(%rdx), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, (%rdx)
	movb	%al, -1(%rcx)
	addq	$32, %rdx
	addq	$32, %rbx
	addq	$-4, %rbp
	jne	.LBB4_272
.LBB4_273:                              # %..preheader8_crit_edge.us.i
                                        #   in Loop: Header=BB4_268 Depth=2
	decq	%r9
	testl	%r9d, %r9d
	jne	.LBB4_268
.LBB4_274:                              # %._crit_edge21.loopexit.i
                                        #   in Loop: Header=BB4_246 Depth=1
	movq	152(%rsp), %r9          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	leal	-1(%r9,%rdx), %r9d
	subl	104(%rsp), %r9d         # 4-byte Folded Reload
	movq	160(%rsp), %rsi         # 8-byte Reload
	jmp	.LBB4_283
	.p2align	4, 0x90
.LBB4_275:                              # %.preheader9.lr.ph.split.i
                                        #   in Loop: Header=BB4_246 Depth=1
	movq	%rsi, %rbx
	leal	-1(%r9,%rdx), %r9d
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_282
	.p2align	4, 0x90
.LBB4_276:                              # %.preheader9.us22.i
                                        #   Parent Loop BB4_246 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_277 Depth 3
                                        #       Child Loop BB4_280 Depth 3
	xorl	%esi, %esi
	testq	%r10, %r10
	je	.LBB4_278
	.p2align	4, 0x90
.LBB4_277:                              #   Parent Loop BB4_246 Depth=1
                                        #     Parent Loop BB4_276 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15,%rsi,8), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%r15,%rsi,8)
	movb	$45, -1(%rcx)
	incq	%rsi
	cmpq	%rsi, %r10
	jne	.LBB4_277
.LBB4_278:                              # %.prol.loopexit
                                        #   in Loop: Header=BB4_276 Depth=2
	cmpq	$3, %r13
	jb	.LBB4_281
# BB#279:                               # %.preheader9.us22.i.new
                                        #   in Loop: Header=BB4_276 Depth=2
	movq	96(%rsp), %rdx          # 8-byte Reload
	subq	%rsi, %rdx
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB4_280:                              #   Parent Loop BB4_246 Depth=1
                                        #     Parent Loop BB4_276 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rsi), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -24(%rsi)
	movb	$45, -1(%rcx)
	movq	-16(%rsi), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -16(%rsi)
	movb	$45, -1(%rcx)
	movq	-8(%rsi), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -8(%rsi)
	movb	$45, -1(%rcx)
	movq	(%rsi), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, (%rsi)
	movb	$45, -1(%rcx)
	addq	$32, %rsi
	addq	$-4, %rdx
	jne	.LBB4_280
.LBB4_281:                              # %._crit_edge.us30.i
                                        #   in Loop: Header=BB4_276 Depth=2
	decl	%eax
	movq	112(%rsp), %rdx         # 8-byte Reload
	jne	.LBB4_276
.LBB4_282:                              # %._crit_edge21.loopexit79.i
                                        #   in Loop: Header=BB4_246 Depth=1
	subl	104(%rsp), %r9d         # 4-byte Folded Reload
	movq	%rbx, %rsi
.LBB4_283:                              # %._crit_edge21.i
                                        #   in Loop: Header=BB4_246 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rsi), %ecx
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	cmpl	$-1, %eax
	je	.LBB4_315
# BB#284:                               # %.preheader7.lr.ph.i
                                        #   in Loop: Header=BB4_246 Depth=1
	notl	%eax
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	movq	%rax, 72(%rsp)          # 8-byte Spill
	jle	.LBB4_305
# BB#285:                               # %.preheader7.us.preheader.i
                                        #   in Loop: Header=BB4_246 Depth=1
	movq	%r9, 152(%rsp)          # 8-byte Spill
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movslq	%eax, %r9
	jle	.LBB4_298
# BB#286:                               # %.preheader7.us.i.us.preheader
                                        #   in Loop: Header=BB4_246 Depth=1
	movslq	40(%rsp), %r8           # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB4_287:                              # %.preheader7.us.i.us
                                        #   Parent Loop BB4_246 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_288 Depth 3
                                        #       Child Loop BB4_291 Depth 3
                                        #       Child Loop BB4_293 Depth 3
                                        #       Child Loop BB4_296 Depth 3
	xorl	%esi, %esi
	testq	%r14, %r14
	je	.LBB4_289
	.p2align	4, 0x90
.LBB4_288:                              #   Parent Loop BB4_246 Depth=1
                                        #     Parent Loop BB4_287 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r12,%rsi,8), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, (%r12,%rsi,8)
	movb	$45, -1(%rdx)
	incq	%rsi
	cmpq	%rsi, %r14
	jne	.LBB4_288
.LBB4_289:                              # %.prol.loopexit853
                                        #   in Loop: Header=BB4_287 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_292
# BB#290:                               # %.preheader7.us.i.us.new
                                        #   in Loop: Header=BB4_287 Depth=2
	movq	48(%rsp), %rdx          # 8-byte Reload
	subq	%rsi, %rdx
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB4_291:                              #   Parent Loop BB4_246 Depth=1
                                        #     Parent Loop BB4_287 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -24(%rsi)
	movb	$45, -1(%rdi)
	movq	-16(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -16(%rsi)
	movb	$45, -1(%rdi)
	movq	-8(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -8(%rsi)
	movb	$45, -1(%rdi)
	movq	(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, (%rsi)
	movb	$45, -1(%rdi)
	addq	$32, %rsi
	addq	$-4, %rdx
	jne	.LBB4_291
.LBB4_292:                              # %.lr.ph34.us.i.us
                                        #   in Loop: Header=BB4_287 Depth=2
	leaq	(%r9,%r8), %rsi
	xorl	%ebx, %ebx
	testq	%r10, %r10
	je	.LBB4_294
	.p2align	4, 0x90
.LBB4_293:                              #   Parent Loop BB4_246 Depth=1
                                        #     Parent Loop BB4_287 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r11,%rbx,8), %rdx
	movzbl	(%rdx,%rsi), %edx
	movq	(%r15,%rbx,8), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, (%r15,%rbx,8)
	movb	%dl, -1(%rdi)
	incq	%rbx
	cmpq	%rbx, %r10
	jne	.LBB4_293
.LBB4_294:                              # %.prol.loopexit858
                                        #   in Loop: Header=BB4_287 Depth=2
	cmpq	$3, %r13
	jb	.LBB4_297
# BB#295:                               # %.lr.ph34.us.i.us.new
                                        #   in Loop: Header=BB4_287 Depth=2
	movq	96(%rsp), %rbp          # 8-byte Reload
	subq	%rbx, %rbp
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbx,8), %rdx
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB4_296:                              #   Parent Loop BB4_246 Depth=1
                                        #     Parent Loop BB4_287 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rbx), %rdi
	movzbl	(%rdi,%rsi), %ecx
	movq	-24(%rdx), %rdi
	leaq	-1(%rdi), %rax
	movq	%rax, -24(%rdx)
	movb	%cl, -1(%rdi)
	movq	-16(%rbx), %rax
	movzbl	(%rax,%rsi), %eax
	movq	-16(%rdx), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -16(%rdx)
	movb	%al, -1(%rcx)
	movq	-8(%rbx), %rax
	movzbl	(%rax,%rsi), %eax
	movq	-8(%rdx), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -8(%rdx)
	movb	%al, -1(%rcx)
	movq	(%rbx), %rax
	movzbl	(%rax,%rsi), %eax
	movq	(%rdx), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, (%rdx)
	movb	%al, -1(%rcx)
	addq	$32, %rdx
	addq	$32, %rbx
	addq	$-4, %rbp
	jne	.LBB4_296
.LBB4_297:                              # %._crit_edge35.us.i.loopexit.us
                                        #   in Loop: Header=BB4_287 Depth=2
	decq	%r9
	testl	%r9d, %r9d
	jne	.LBB4_287
	jmp	.LBB4_304
	.p2align	4, 0x90
.LBB4_298:                              # %.preheader7.us.i
                                        #   Parent Loop BB4_246 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_299 Depth 3
                                        #       Child Loop BB4_302 Depth 3
	xorl	%esi, %esi
	testq	%r14, %r14
	je	.LBB4_300
	.p2align	4, 0x90
.LBB4_299:                              #   Parent Loop BB4_246 Depth=1
                                        #     Parent Loop BB4_298 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r12,%rsi,8), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%r12,%rsi,8)
	movb	$45, -1(%rax)
	incq	%rsi
	cmpq	%rsi, %r14
	jne	.LBB4_299
.LBB4_300:                              # %.prol.loopexit848
                                        #   in Loop: Header=BB4_298 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_303
# BB#301:                               # %.preheader7.us.i.new
                                        #   in Loop: Header=BB4_298 Depth=2
	movq	48(%rsp), %rdx          # 8-byte Reload
	subq	%rsi, %rdx
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB4_302:                              #   Parent Loop BB4_246 Depth=1
                                        #     Parent Loop BB4_298 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rsi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, -24(%rsi)
	movb	$45, -1(%rax)
	movq	-16(%rsi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, -16(%rsi)
	movb	$45, -1(%rax)
	movq	-8(%rsi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, -8(%rsi)
	movb	$45, -1(%rax)
	movq	(%rsi), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rsi)
	movb	$45, -1(%rax)
	addq	$32, %rsi
	addq	$-4, %rdx
	jne	.LBB4_302
.LBB4_303:                              # %..preheader_crit_edge.us.i394
                                        #   in Loop: Header=BB4_298 Depth=2
	decq	%r9
	testl	%r9d, %r9d
	jne	.LBB4_298
.LBB4_304:                              # %._crit_edge37.loopexit.i
                                        #   in Loop: Header=BB4_246 Depth=1
	movq	152(%rsp), %r9          # 8-byte Reload
	addl	72(%rsp), %r9d          # 4-byte Folded Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movq	160(%rsp), %rsi         # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	cmpl	56(%rsp), %edx          # 4-byte Folded Reload
	jne	.LBB4_316
	jmp	.LBB4_318
	.p2align	4, 0x90
.LBB4_305:                              # %.preheader7.lr.ph.split.i
                                        #   in Loop: Header=BB4_246 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_306
# BB#307:                               # %.preheader7.us39.preheader.i
                                        #   in Loop: Header=BB4_246 Depth=1
	movq	%r9, 152(%rsp)          # 8-byte Spill
	movslq	72(%rsp), %r9           # 4-byte Folded Reload
	movslq	40(%rsp), %r8           # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB4_308:                              # %.preheader7.us39.i
                                        #   Parent Loop BB4_246 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_309 Depth 3
                                        #       Child Loop BB4_312 Depth 3
	leaq	(%r9,%r8), %rsi
	xorl	%ebx, %ebx
	testq	%r10, %r10
	je	.LBB4_310
	.p2align	4, 0x90
.LBB4_309:                              #   Parent Loop BB4_246 Depth=1
                                        #     Parent Loop BB4_308 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r11,%rbx,8), %rdx
	movzbl	(%rdx,%rsi), %edx
	movq	(%r15,%rbx,8), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, (%r15,%rbx,8)
	movb	%dl, -1(%rdi)
	incq	%rbx
	cmpq	%rbx, %r10
	jne	.LBB4_309
.LBB4_310:                              # %.prol.loopexit843
                                        #   in Loop: Header=BB4_308 Depth=2
	cmpq	$3, %r13
	jb	.LBB4_313
# BB#311:                               # %.preheader7.us39.i.new
                                        #   in Loop: Header=BB4_308 Depth=2
	movq	96(%rsp), %rbp          # 8-byte Reload
	subq	%rbx, %rbp
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbx,8), %rdx
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB4_312:                              #   Parent Loop BB4_246 Depth=1
                                        #     Parent Loop BB4_308 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rbx), %rdi
	movzbl	(%rdi,%rsi), %ecx
	movq	-24(%rdx), %rdi
	leaq	-1(%rdi), %rax
	movq	%rax, -24(%rdx)
	movb	%cl, -1(%rdi)
	movq	-16(%rbx), %rax
	movzbl	(%rax,%rsi), %eax
	movq	-16(%rdx), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -16(%rdx)
	movb	%al, -1(%rcx)
	movq	-8(%rbx), %rax
	movzbl	(%rax,%rsi), %eax
	movq	-8(%rdx), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -8(%rdx)
	movb	%al, -1(%rcx)
	movq	(%rbx), %rax
	movzbl	(%rax,%rsi), %eax
	movq	(%rdx), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, (%rdx)
	movb	%al, -1(%rcx)
	addq	$32, %rdx
	addq	$32, %rbx
	addq	$-4, %rbp
	jne	.LBB4_312
.LBB4_313:                              # %._crit_edge35.us47.i
                                        #   in Loop: Header=BB4_308 Depth=2
	decq	%r9
	testl	%r9d, %r9d
	movq	112(%rsp), %rdx         # 8-byte Reload
	jne	.LBB4_308
# BB#314:                               # %._crit_edge37.loopexit77.i
                                        #   in Loop: Header=BB4_246 Depth=1
	movq	152(%rsp), %r9          # 8-byte Reload
	addl	72(%rsp), %r9d          # 4-byte Folded Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movq	160(%rsp), %rsi         # 8-byte Reload
	cmpl	56(%rsp), %edx          # 4-byte Folded Reload
	jne	.LBB4_316
	jmp	.LBB4_318
.LBB4_306:                              # %.preheader7.preheader.i
                                        #   in Loop: Header=BB4_246 Depth=1
	addl	72(%rsp), %r9d          # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB4_315:                              # %._crit_edge37.i
                                        #   in Loop: Header=BB4_246 Depth=1
	cmpl	56(%rsp), %edx          # 4-byte Folded Reload
	je	.LBB4_318
.LBB4_316:                              # %._crit_edge37.i
                                        #   in Loop: Header=BB4_246 Depth=1
	cmpl	16(%rsp), %esi          # 4-byte Folded Reload
	je	.LBB4_318
# BB#317:                               #   in Loop: Header=BB4_246 Depth=1
	movq	424(%rsp), %rax
	movq	144(%rsp), %rcx         # 8-byte Reload
	movslq	(%rax,%rcx,4), %rax
	movslq	392(%rsp), %rcx
	addq	%rax, %rcx
	movq	432(%rsp), %rax
	movq	192(%rsp), %rdx         # 8-byte Reload
	movslq	(%rax,%rdx,4), %rax
	movslq	408(%rsp), %rdx
	addq	%rax, %rdx
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movss	(%rax,%rdx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	112(%rsp), %rdx         # 8-byte Reload
	movq	384(%rsp), %rax
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
.LBB4_318:                              #   in Loop: Header=BB4_246 Depth=1
	testl	%edx, %edx
	jle	.LBB4_473
# BB#319:                               #   in Loop: Header=BB4_246 Depth=1
	testl	%esi, %esi
	jle	.LBB4_473
# BB#320:                               # %.preheader11.i396
                                        #   in Loop: Header=BB4_246 Depth=1
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_328
# BB#321:                               # %.lr.ph49.i
                                        #   in Loop: Header=BB4_246 Depth=1
	testq	%r14, %r14
	movslq	104(%rsp), %rax         # 4-byte Folded Reload
	je	.LBB4_322
# BB#323:                               # %.prol.preheader862
                                        #   in Loop: Header=BB4_246 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_324:                              #   Parent Loop BB4_246 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r8,%rdx,8), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%r12,%rdx,8), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%r12,%rdx,8)
	movb	%cl, -1(%rsi)
	incq	%rdx
	cmpq	%rdx, %r14
	jne	.LBB4_324
	jmp	.LBB4_325
	.p2align	4, 0x90
.LBB4_322:                              #   in Loop: Header=BB4_246 Depth=1
	xorl	%edx, %edx
.LBB4_325:                              # %.prol.loopexit863
                                        #   in Loop: Header=BB4_246 Depth=1
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_328
# BB#326:                               # %.lr.ph49.i.new
                                        #   in Loop: Header=BB4_246 Depth=1
	movq	48(%rsp), %rsi          # 8-byte Reload
	subq	%rdx, %rsi
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rbx
	movq	168(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB4_327:                              #   Parent Loop BB4_246 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rdx), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-24(%rbx), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -24(%rbx)
	movb	%cl, -1(%rdi)
	movq	-16(%rdx), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-16(%rbx), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -16(%rbx)
	movb	%cl, -1(%rdi)
	movq	-8(%rdx), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-8(%rbx), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -8(%rbx)
	movb	%cl, -1(%rdi)
	movq	(%rdx), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%rbx), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, (%rbx)
	movb	%cl, -1(%rdi)
	addq	$32, %rbx
	addq	$32, %rdx
	addq	$-4, %rsi
	jne	.LBB4_327
.LBB4_328:                              # %.preheader10.i397
                                        #   in Loop: Header=BB4_246 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_336
# BB#329:                               # %.lr.ph51.i
                                        #   in Loop: Header=BB4_246 Depth=1
	testq	%r10, %r10
	movslq	40(%rsp), %rax          # 4-byte Folded Reload
	je	.LBB4_330
# BB#331:                               # %.prol.preheader867
                                        #   in Loop: Header=BB4_246 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_332:                              #   Parent Loop BB4_246 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r11,%rdx,8), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%r15,%rdx,8), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%r15,%rdx,8)
	movb	%cl, -1(%rsi)
	incq	%rdx
	cmpq	%rdx, %r10
	jne	.LBB4_332
	jmp	.LBB4_333
	.p2align	4, 0x90
.LBB4_330:                              #   in Loop: Header=BB4_246 Depth=1
	xorl	%edx, %edx
.LBB4_333:                              # %.prol.loopexit868
                                        #   in Loop: Header=BB4_246 Depth=1
	cmpq	$3, %r13
	jb	.LBB4_336
# BB#334:                               # %.lr.ph51.i.new
                                        #   in Loop: Header=BB4_246 Depth=1
	movq	96(%rsp), %rsi          # 8-byte Reload
	subq	%rdx, %rsi
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rbx
	movq	128(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB4_335:                              #   Parent Loop BB4_246 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rdx), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-24(%rbx), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -24(%rbx)
	movb	%cl, -1(%rdi)
	movq	-16(%rdx), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-16(%rbx), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -16(%rbx)
	movb	%cl, -1(%rdi)
	movq	-8(%rdx), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-8(%rbx), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -8(%rbx)
	movb	%cl, -1(%rdi)
	movq	(%rdx), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%rbx), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, (%rbx)
	movb	%cl, -1(%rdi)
	addq	$32, %rbx
	addq	$32, %rdx
	addq	$-4, %rsi
	jne	.LBB4_335
.LBB4_336:                              # %._crit_edge52.i
                                        #   in Loop: Header=BB4_246 Depth=1
	addl	$2, %r9d
	cmpl	184(%rsp), %r9d         # 4-byte Folded Reload
	movl	104(%rsp), %ecx         # 4-byte Reload
	jle	.LBB4_246
	jmp	.LBB4_473
.LBB4_360:
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	negl	%esi
	movq	(%r10,%rax,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movl	$1, %esi
	movaps	%xmm1, %xmm0
	cmpq	$1, %rdx
	je	.LBB4_338
.LBB4_362:                              # %.lr.ph70.i.new
	movq	16(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	negl	%edi
	.p2align	4, 0x90
.LBB4_363:                              # =>This Inner Loop Header: Depth=1
	movss	(%r14,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB4_365
# BB#364:                               #   in Loop: Header=BB4_363 Depth=1
	leal	(%rdi,%rsi), %ebp
	movq	(%r10,%rax,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movaps	%xmm1, %xmm0
.LBB4_365:                              #   in Loop: Header=BB4_363 Depth=1
	movss	4(%r14,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB4_367
# BB#366:                               #   in Loop: Header=BB4_363 Depth=1
	leal	1(%rdi,%rsi), %ebp
	movq	(%r10,%rax,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movaps	%xmm1, %xmm0
.LBB4_367:                              #   in Loop: Header=BB4_363 Depth=1
	addq	$2, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB4_363
.LBB4_338:                              # %.preheader14.i
	testl	%r11d, %r11d
	movl	12(%rsp), %r8d          # 4-byte Reload
	js	.LBB4_344
# BB#339:                               # %.lr.ph67.preheader.i
	movq	%r11, %rsi
	incq	%rsi
	movl	%esi, %eax
	leaq	-1(%rax), %rcx
	xorl	%edx, %edx
	andq	$7, %rsi
	je	.LBB4_341
	.p2align	4, 0x90
.LBB4_340:                              # %.lr.ph67.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r10,%rdx,8), %rdi
	incq	%rdx
	movl	%edx, (%rdi)
	cmpq	%rdx, %rsi
	jne	.LBB4_340
.LBB4_341:                              # %.lr.ph67.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB4_344
# BB#342:                               # %.lr.ph67.preheader.i.new
	negq	%rax
	leaq	4(%rdx,%rax), %rax
	leaq	56(%r10,%rdx,8), %rcx
	leaq	4(%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_343:                              # %.lr.ph67.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx,%rsi,8), %rdi
	leal	(%rdx,%rsi), %ebp
	leal	-3(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-48(%rcx,%rsi,8), %rdi
	leal	-2(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-40(%rcx,%rsi,8), %rdi
	leal	-1(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-32(%rcx,%rsi,8), %rdi
	movl	%ebp, (%rdi)
	movq	-24(%rcx,%rsi,8), %rdi
	leal	1(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-16(%rcx,%rsi,8), %rdi
	leal	2(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-8(%rcx,%rsi,8), %rdi
	leal	3(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	(%rcx,%rsi,8), %rdi
	leal	4(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	leaq	8(%rax,%rsi), %rdi
	addq	$8, %rsi
	cmpq	$4, %rdi
	jne	.LBB4_343
.LBB4_344:                              # %.preheader13.i
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	js	.LBB4_372
# BB#345:                               # %.lr.ph65.i
	movq	(%r10), %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	incq	%rsi
	movl	%esi, %eax
	cmpq	$7, %rax
	jbe	.LBB4_346
# BB#368:                               # %min.iters.checked786
	andl	$7, %esi
	movq	%rax, %rbp
	subq	%rsi, %rbp
	je	.LBB4_346
# BB#369:                               # %vector.body782.preheader
	leaq	16(%rcx), %rdi
	movdqa	.LCPI4_6(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI4_7(%rip), %xmm1   # xmm1 = [4,4,4,4]
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI4_8(%rip), %xmm3   # xmm3 = [8,8,8,8]
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB4_370:                              # %vector.body782
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	pxor	%xmm2, %xmm5
	pxor	%xmm2, %xmm4
	movdqu	%xmm5, -16(%rdi)
	movdqu	%xmm4, (%rdi)
	paddd	%xmm3, %xmm0
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB4_370
# BB#371:                               # %middle.block783
	testq	%rsi, %rsi
	jne	.LBB4_347
	jmp	.LBB4_372
.LBB4_346:
	xorl	%ebp, %ebp
.LBB4_347:                              # %scalar.ph784.preheader
	movl	%ebp, %edx
	notl	%edx
	leaq	(%rcx,%rbp,4), %rcx
	subq	%rbp, %rax
	.p2align	4, 0x90
.LBB4_348:                              # %scalar.ph784
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rcx)
	decl	%edx
	addq	$4, %rcx
	decq	%rax
	jne	.LBB4_348
.LBB4_372:                              # %.preheader12.i
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_378
# BB#373:                               # %.lr.ph63.i
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r11), %eax
	cltq
	movl	28(%rsp), %ecx          # 4-byte Reload
	leaq	-1(%rcx), %rdx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	andq	$3, %rdi
	je	.LBB4_375
	.p2align	4, 0x90
.LBB4_374:                              # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rsi,8), %rbp
	leaq	(%rbp,%rax), %rbx
	movq	%rbx, (%r12,%rsi,8)
	movb	$0, (%rbp,%rax)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB4_374
.LBB4_375:                              # %.prol.loopexit943
	cmpq	$3, %rdx
	jb	.LBB4_378
# BB#376:                               # %.lr.ph63.i.new
	subq	%rsi, %rcx
	leaq	24(%r12,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB4_377:                              # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -24(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-16(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -16(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-8(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -8(%rdx)
	movb	$0, (%rsi,%rax)
	movq	(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, (%rdx)
	movb	$0, (%rsi,%rax)
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB4_377
.LBB4_378:                              # %.preheader11.i
	testl	%r8d, %r8d
	jle	.LBB4_384
# BB#379:                               # %.lr.ph60.i
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r11), %eax
	cltq
	movl	%r8d, %ecx
	leaq	-1(%rcx), %rdx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	andq	$3, %rdi
	je	.LBB4_381
	.p2align	4, 0x90
.LBB4_380:                              # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rsi,8), %rbp
	leaq	(%rbp,%rax), %rbx
	movq	%rbx, (%r15,%rsi,8)
	movb	$0, (%rbp,%rax)
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB4_380
.LBB4_381:                              # %.prol.loopexit938
	cmpq	$3, %rdx
	jb	.LBB4_384
# BB#382:                               # %.lr.ph60.i.new
	subq	%rsi, %rcx
	leaq	24(%r15,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB4_383:                              # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -24(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-16(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -16(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-8(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -8(%rdx)
	movb	$0, (%rsi,%rax)
	movq	(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, (%rdx)
	movb	$0, (%rsi,%rax)
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB4_383
.LBB4_384:                              # %._crit_edge61.i
	movq	16(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addl	%r11d, %eax
	movl	%eax, 152(%rsp)         # 4-byte Spill
	movq	80(%rsp), %r8           # 8-byte Reload
	js	.LBB4_473
# BB#385:                               # %.lr.ph56.preheader.i
	movl	28(%rsp), %r10d         # 4-byte Reload
	movl	12(%rsp), %r14d         # 4-byte Reload
	leaq	-1(%r14), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	-1(%r10), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%r10, 48(%rsp)          # 8-byte Spill
                                        # kill: %R10D<def> %R10D<kill> %R10<kill> %R10<def>
	andl	$3, %r10d
	movq	%r14, 96(%rsp)          # 8-byte Spill
                                        # kill: %R14D<def> %R14D<kill> %R14<kill> %R14<def>
	andl	$3, %r14d
	leaq	24(%r15), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	24(%r12), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	24(%r8), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leaq	24(%r9), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_386:                              # %.lr.ph56.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_414 Depth 2
                                        #       Child Loop BB4_415 Depth 3
                                        #       Child Loop BB4_418 Depth 3
                                        #     Child Loop BB4_406 Depth 2
                                        #       Child Loop BB4_407 Depth 3
                                        #       Child Loop BB4_410 Depth 3
                                        #     Child Loop BB4_395 Depth 2
                                        #       Child Loop BB4_396 Depth 3
                                        #       Child Loop BB4_399 Depth 3
                                        #       Child Loop BB4_401 Depth 3
                                        #       Child Loop BB4_404 Depth 3
                                        #     Child Loop BB4_446 Depth 2
                                        #       Child Loop BB4_447 Depth 3
                                        #       Child Loop BB4_450 Depth 3
                                        #     Child Loop BB4_424 Depth 2
                                        #       Child Loop BB4_425 Depth 3
                                        #       Child Loop BB4_428 Depth 3
                                        #     Child Loop BB4_431 Depth 2
                                        #       Child Loop BB4_432 Depth 3
                                        #       Child Loop BB4_435 Depth 3
                                        #       Child Loop BB4_437 Depth 3
                                        #       Child Loop BB4_440 Depth 3
                                        #     Child Loop BB4_460 Depth 2
                                        #     Child Loop BB4_463 Depth 2
                                        #     Child Loop BB4_468 Depth 2
                                        #     Child Loop BB4_471 Depth 2
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r11d
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ebx
	movslq	%ebx, %rax
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movslq	%r11d, %rsi
	movl	(%rax,%rsi,4), %edx
	testl	%edx, %edx
	js	.LBB4_387
# BB#388:                               #   in Loop: Header=BB4_386 Depth=1
	je	.LBB4_390
# BB#389:                               #   in Loop: Header=BB4_386 Depth=1
	movl	%ebx, %ebp
	subl	%edx, %ebp
	jmp	.LBB4_391
	.p2align	4, 0x90
.LBB4_387:                              #   in Loop: Header=BB4_386 Depth=1
	leal	-1(%rbx), %ebp
	jmp	.LBB4_392
	.p2align	4, 0x90
.LBB4_390:                              #   in Loop: Header=BB4_386 Depth=1
	leal	-1(%rbx), %ebp
.LBB4_391:                              #   in Loop: Header=BB4_386 Depth=1
	movl	$-1, %edx
.LBB4_392:                              #   in Loop: Header=BB4_386 Depth=1
	movl	%ebx, %eax
	subl	%ebp, %eax
	decl	%eax
	movq	%rbp, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%r11, 72(%rsp)          # 8-byte Spill
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	je	.LBB4_421
# BB#393:                               # %.preheader8.lr.ph.i
                                        #   in Loop: Header=BB4_386 Depth=1
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	jle	.LBB4_413
# BB#394:                               # %.preheader8.us.preheader.i
                                        #   in Loop: Header=BB4_386 Depth=1
	movq	%r13, 160(%rsp)         # 8-byte Spill
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movslq	%eax, %r11
	movslq	%ebp, %r13
	jle	.LBB4_406
	.p2align	4, 0x90
.LBB4_395:                              # %.preheader8.us.i.us
                                        #   Parent Loop BB4_386 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_396 Depth 3
                                        #       Child Loop BB4_399 Depth 3
                                        #       Child Loop BB4_401 Depth 3
                                        #       Child Loop BB4_404 Depth 3
	leaq	(%r11,%r13), %rax
	xorl	%edi, %edi
	testq	%r10, %r10
	je	.LBB4_397
	.p2align	4, 0x90
.LBB4_396:                              #   Parent Loop BB4_386 Depth=1
                                        #     Parent Loop BB4_395 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r8,%rdi,8), %rsi
	movzbl	(%rsi,%rax), %ebx
	movq	(%r12,%rdi,8), %rsi
	leaq	-1(%rsi), %rbp
	movq	%rbp, (%r12,%rdi,8)
	movb	%bl, -1(%rsi)
	incq	%rdi
	cmpq	%rdi, %r10
	jne	.LBB4_396
.LBB4_397:                              # %.prol.loopexit900
                                        #   in Loop: Header=BB4_395 Depth=2
	cmpq	$3, 40(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_400
# BB#398:                               # %.preheader8.us.i.us.new
                                        #   in Loop: Header=BB4_395 Depth=2
	movq	48(%rsp), %rsi          # 8-byte Reload
	subq	%rdi, %rsi
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rbp
	movq	112(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB4_399:                              #   Parent Loop BB4_386 Depth=1
                                        #     Parent Loop BB4_395 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdi), %rbx
	movzbl	(%rbx,%rax), %ebx
	movq	-24(%rbp), %r9
	leaq	-1(%r9), %r8
	movq	%r8, -24(%rbp)
	movb	%bl, -1(%r9)
	movq	-16(%rdi), %rbx
	movzbl	(%rbx,%rax), %ebx
	movq	-16(%rbp), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -16(%rbp)
	movb	%bl, -1(%rcx)
	movq	-8(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-8(%rbp), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, -8(%rbp)
	movb	%cl, -1(%rdx)
	movq	(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%rbp), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, (%rbp)
	movb	%cl, -1(%rdx)
	addq	$32, %rbp
	addq	$32, %rdi
	addq	$-4, %rsi
	jne	.LBB4_399
.LBB4_400:                              # %.lr.ph18.us.i.us.preheader
                                        #   in Loop: Header=BB4_395 Depth=2
	xorl	%esi, %esi
	testq	%r14, %r14
	je	.LBB4_402
	.p2align	4, 0x90
.LBB4_401:                              # %.lr.ph18.us.i.us.prol
                                        #   Parent Loop BB4_386 Depth=1
                                        #     Parent Loop BB4_395 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15,%rsi,8), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%r15,%rsi,8)
	movb	$45, -1(%rax)
	incq	%rsi
	cmpq	%rsi, %r14
	jne	.LBB4_401
.LBB4_402:                              # %.lr.ph18.us.i.us.prol.loopexit
                                        #   in Loop: Header=BB4_395 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_405
# BB#403:                               # %.lr.ph18.us.i.us.preheader.new
                                        #   in Loop: Header=BB4_395 Depth=2
	movq	96(%rsp), %rax          # 8-byte Reload
	subq	%rsi, %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB4_404:                              # %.lr.ph18.us.i.us
                                        #   Parent Loop BB4_386 Depth=1
                                        #     Parent Loop BB4_395 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -24(%rsi)
	movb	$45, -1(%rcx)
	movq	-16(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -16(%rsi)
	movb	$45, -1(%rcx)
	movq	-8(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -8(%rsi)
	movb	$45, -1(%rcx)
	movq	(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%rsi)
	movb	$45, -1(%rcx)
	addq	$32, %rsi
	addq	$-4, %rax
	jne	.LBB4_404
.LBB4_405:                              # %._crit_edge.us.i.loopexit.us
                                        #   in Loop: Header=BB4_395 Depth=2
	decq	%r11
	testl	%r11d, %r11d
	movq	80(%rsp), %r8           # 8-byte Reload
	jne	.LBB4_395
	jmp	.LBB4_412
	.p2align	4, 0x90
.LBB4_406:                              # %.preheader8.us.i
                                        #   Parent Loop BB4_386 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_407 Depth 3
                                        #       Child Loop BB4_410 Depth 3
	leaq	(%r11,%r13), %rax
	xorl	%edi, %edi
	testq	%r10, %r10
	je	.LBB4_408
	.p2align	4, 0x90
.LBB4_407:                              #   Parent Loop BB4_386 Depth=1
                                        #     Parent Loop BB4_406 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r8,%rdi,8), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%r12,%rdi,8), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r12,%rdi,8)
	movb	%cl, -1(%rdx)
	incq	%rdi
	cmpq	%rdi, %r10
	jne	.LBB4_407
.LBB4_408:                              # %.prol.loopexit895
                                        #   in Loop: Header=BB4_406 Depth=2
	cmpq	$3, 40(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_411
# BB#409:                               # %.preheader8.us.i.new
                                        #   in Loop: Header=BB4_406 Depth=2
	movq	48(%rsp), %rsi          # 8-byte Reload
	subq	%rdi, %rsi
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rbp
	movq	112(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB4_410:                              #   Parent Loop BB4_386 Depth=1
                                        #     Parent Loop BB4_406 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-24(%rbp), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, -24(%rbp)
	movb	%cl, -1(%rdx)
	movq	-16(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-16(%rbp), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, -16(%rbp)
	movb	%cl, -1(%rdx)
	movq	-8(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-8(%rbp), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, -8(%rbp)
	movb	%cl, -1(%rdx)
	movq	(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%rbp), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, (%rbp)
	movb	%cl, -1(%rdx)
	addq	$32, %rbp
	addq	$32, %rdi
	addq	$-4, %rsi
	jne	.LBB4_410
.LBB4_411:                              # %..preheader7_crit_edge.us.i
                                        #   in Loop: Header=BB4_406 Depth=2
	decq	%r11
	testl	%r11d, %r11d
	jne	.LBB4_406
.LBB4_412:                              # %._crit_edge20.loopexit.i
                                        #   in Loop: Header=BB4_386 Depth=1
	movq	160(%rsp), %r13         # 8-byte Reload
	movq	104(%rsp), %rbx         # 8-byte Reload
	leal	-1(%r13,%rbx), %r13d
	subl	56(%rsp), %r13d         # 4-byte Folded Reload
	movq	136(%rsp), %r9          # 8-byte Reload
	movq	72(%rsp), %r11          # 8-byte Reload
	movq	128(%rsp), %rdx         # 8-byte Reload
	jmp	.LBB4_421
	.p2align	4, 0x90
.LBB4_413:                              # %.preheader8.lr.ph.split.i
                                        #   in Loop: Header=BB4_386 Depth=1
	leal	-1(%r13,%rbx), %r13d
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_420
	.p2align	4, 0x90
.LBB4_414:                              # %.preheader8.us21.i
                                        #   Parent Loop BB4_386 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_415 Depth 3
                                        #       Child Loop BB4_418 Depth 3
	xorl	%edi, %edi
	testq	%r14, %r14
	je	.LBB4_416
	.p2align	4, 0x90
.LBB4_415:                              #   Parent Loop BB4_386 Depth=1
                                        #     Parent Loop BB4_414 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15,%rdi,8), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%r15,%rdi,8)
	movb	$45, -1(%rcx)
	incq	%rdi
	cmpq	%rdi, %r14
	jne	.LBB4_415
.LBB4_416:                              # %.prol.loopexit890
                                        #   in Loop: Header=BB4_414 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_419
# BB#417:                               # %.preheader8.us21.i.new
                                        #   in Loop: Header=BB4_414 Depth=2
	movq	96(%rsp), %rsi          # 8-byte Reload
	subq	%rdi, %rsi
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB4_418:                              #   Parent Loop BB4_386 Depth=1
                                        #     Parent Loop BB4_414 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -24(%rdi)
	movb	$45, -1(%rcx)
	movq	-16(%rdi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -16(%rdi)
	movb	$45, -1(%rcx)
	movq	-8(%rdi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -8(%rdi)
	movb	$45, -1(%rcx)
	movq	(%rdi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%rdi)
	movb	$45, -1(%rcx)
	addq	$32, %rdi
	addq	$-4, %rsi
	jne	.LBB4_418
.LBB4_419:                              # %._crit_edge.us29.i
                                        #   in Loop: Header=BB4_414 Depth=2
	decl	%eax
	movq	128(%rsp), %rdx         # 8-byte Reload
	jne	.LBB4_414
.LBB4_420:                              # %._crit_edge20.loopexit78.i
                                        #   in Loop: Header=BB4_386 Depth=1
	subl	%ebp, %r13d
.LBB4_421:                              # %._crit_edge20.i
                                        #   in Loop: Header=BB4_386 Depth=1
	leal	(%rdx,%r11), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpl	$-1, %edx
	je	.LBB4_454
# BB#422:                               # %.preheader6.lr.ph.i
                                        #   in Loop: Header=BB4_386 Depth=1
	notl	%edx
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_443
# BB#423:                               # %.preheader6.us.preheader.i
                                        #   in Loop: Header=BB4_386 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movslq	%edx, %r8
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movq	%r13, %r11
	jle	.LBB4_424
# BB#430:                               # %.preheader6.us.i.us.preheader
                                        #   in Loop: Header=BB4_386 Depth=1
	movslq	16(%rsp), %r13          # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB4_431:                              # %.preheader6.us.i.us
                                        #   Parent Loop BB4_386 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_432 Depth 3
                                        #       Child Loop BB4_435 Depth 3
                                        #       Child Loop BB4_437 Depth 3
                                        #       Child Loop BB4_440 Depth 3
	xorl	%esi, %esi
	testq	%r10, %r10
	je	.LBB4_433
	.p2align	4, 0x90
.LBB4_432:                              #   Parent Loop BB4_386 Depth=1
                                        #     Parent Loop BB4_431 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r12,%rsi,8), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%r12,%rsi,8)
	movb	$45, -1(%rax)
	incq	%rsi
	cmpq	%rsi, %r10
	jne	.LBB4_432
.LBB4_433:                              # %.prol.loopexit918
                                        #   in Loop: Header=BB4_431 Depth=2
	cmpq	$3, 40(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_436
# BB#434:                               # %.preheader6.us.i.us.new
                                        #   in Loop: Header=BB4_431 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	subq	%rsi, %rax
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB4_435:                              #   Parent Loop BB4_386 Depth=1
                                        #     Parent Loop BB4_431 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -24(%rsi)
	movb	$45, -1(%rcx)
	movq	-16(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -16(%rsi)
	movb	$45, -1(%rcx)
	movq	-8(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -8(%rsi)
	movb	$45, -1(%rcx)
	movq	(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%rsi)
	movb	$45, -1(%rcx)
	addq	$32, %rsi
	addq	$-4, %rax
	jne	.LBB4_435
.LBB4_436:                              # %.lr.ph33.us.i.us
                                        #   in Loop: Header=BB4_431 Depth=2
	leaq	(%r8,%r13), %rsi
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.LBB4_438
	.p2align	4, 0x90
.LBB4_437:                              #   Parent Loop BB4_386 Depth=1
                                        #     Parent Loop BB4_431 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r9,%rax,8), %rcx
	movzbl	(%rcx,%rsi), %ecx
	movq	(%r15,%rax,8), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, (%r15,%rax,8)
	movb	%cl, -1(%rdx)
	incq	%rax
	cmpq	%rax, %r14
	jne	.LBB4_437
.LBB4_438:                              # %.prol.loopexit923
                                        #   in Loop: Header=BB4_431 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_441
# BB#439:                               # %.lr.ph33.us.i.us.new
                                        #   in Loop: Header=BB4_431 Depth=2
	movq	96(%rsp), %rbp          # 8-byte Reload
	subq	%rax, %rbp
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rdi
	movq	168(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	.p2align	4, 0x90
.LBB4_440:                              #   Parent Loop BB4_386 Depth=1
                                        #     Parent Loop BB4_431 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rax), %rcx
	movzbl	(%rcx,%rsi), %ecx
	movq	-24(%rdi), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, -24(%rdi)
	movb	%cl, -1(%rdx)
	movq	-16(%rax), %rcx
	movzbl	(%rcx,%rsi), %ecx
	movq	-16(%rdi), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, -16(%rdi)
	movb	%cl, -1(%rdx)
	movq	-8(%rax), %rcx
	movzbl	(%rcx,%rsi), %ecx
	movq	-8(%rdi), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, -8(%rdi)
	movb	%cl, -1(%rdx)
	movq	(%rax), %rcx
	movzbl	(%rcx,%rsi), %ecx
	movq	(%rdi), %rdx
	leaq	-1(%rdx), %rbx
	movq	%rbx, (%rdi)
	movb	%cl, -1(%rdx)
	addq	$32, %rdi
	addq	$32, %rax
	addq	$-4, %rbp
	jne	.LBB4_440
.LBB4_441:                              # %._crit_edge34.us.i.loopexit.us
                                        #   in Loop: Header=BB4_431 Depth=2
	decq	%r8
	testl	%r8d, %r8d
	jne	.LBB4_431
	jmp	.LBB4_442
	.p2align	4, 0x90
.LBB4_424:                              # %.preheader6.us.i
                                        #   Parent Loop BB4_386 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_425 Depth 3
                                        #       Child Loop BB4_428 Depth 3
	xorl	%esi, %esi
	testq	%r10, %r10
	je	.LBB4_426
	.p2align	4, 0x90
.LBB4_425:                              #   Parent Loop BB4_386 Depth=1
                                        #     Parent Loop BB4_424 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r12,%rsi,8), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%r12,%rsi,8)
	movb	$45, -1(%rax)
	incq	%rsi
	cmpq	%rsi, %r10
	jne	.LBB4_425
.LBB4_426:                              # %.prol.loopexit913
                                        #   in Loop: Header=BB4_424 Depth=2
	cmpq	$3, 40(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_429
# BB#427:                               # %.preheader6.us.i.new
                                        #   in Loop: Header=BB4_424 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	subq	%rsi, %rax
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB4_428:                              #   Parent Loop BB4_386 Depth=1
                                        #     Parent Loop BB4_424 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -24(%rsi)
	movb	$45, -1(%rcx)
	movq	-16(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -16(%rsi)
	movb	$45, -1(%rcx)
	movq	-8(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -8(%rsi)
	movb	$45, -1(%rcx)
	movq	(%rsi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%rsi)
	movb	$45, -1(%rcx)
	addq	$32, %rsi
	addq	$-4, %rax
	jne	.LBB4_428
.LBB4_429:                              # %..preheader_crit_edge.us.i
                                        #   in Loop: Header=BB4_424 Depth=2
	decq	%r8
	testl	%r8d, %r8d
	jne	.LBB4_424
.LBB4_442:                              # %._crit_edge36.loopexit.i
                                        #   in Loop: Header=BB4_386 Depth=1
	movq	%r11, %r13
	addl	128(%rsp), %r13d        # 4-byte Folded Reload
	jmp	.LBB4_453
	.p2align	4, 0x90
.LBB4_443:                              # %.preheader6.lr.ph.split.i
                                        #   in Loop: Header=BB4_386 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_444
# BB#445:                               # %.preheader6.us38.preheader.i
                                        #   in Loop: Header=BB4_386 Depth=1
	movq	%r13, 160(%rsp)         # 8-byte Spill
	movq	%rdx, %r11
	movslq	%edx, %r8
	movslq	16(%rsp), %r13          # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB4_446:                              # %.preheader6.us38.i
                                        #   Parent Loop BB4_386 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_447 Depth 3
                                        #       Child Loop BB4_450 Depth 3
	leaq	(%r8,%r13), %rsi
	xorl	%ebx, %ebx
	testq	%r14, %r14
	je	.LBB4_448
	.p2align	4, 0x90
.LBB4_447:                              #   Parent Loop BB4_386 Depth=1
                                        #     Parent Loop BB4_446 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r9,%rbx,8), %rcx
	movzbl	(%rcx,%rsi), %ecx
	movq	(%r15,%rbx,8), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, (%r15,%rbx,8)
	movb	%cl, -1(%rdx)
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB4_447
.LBB4_448:                              # %.prol.loopexit908
                                        #   in Loop: Header=BB4_446 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_451
# BB#449:                               # %.preheader6.us38.i.new
                                        #   in Loop: Header=BB4_446 Depth=2
	movq	96(%rsp), %rbp          # 8-byte Reload
	subq	%rbx, %rbp
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbx,8), %rdi
	movq	168(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB4_450:                              #   Parent Loop BB4_386 Depth=1
                                        #     Parent Loop BB4_446 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rbx), %rcx
	movzbl	(%rcx,%rsi), %ecx
	movq	-24(%rdi), %rdx
	leaq	-1(%rdx), %rax
	movq	%rax, -24(%rdi)
	movb	%cl, -1(%rdx)
	movq	-16(%rbx), %rax
	movzbl	(%rax,%rsi), %eax
	movq	-16(%rdi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -16(%rdi)
	movb	%al, -1(%rcx)
	movq	-8(%rbx), %rax
	movzbl	(%rax,%rsi), %eax
	movq	-8(%rdi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, -8(%rdi)
	movb	%al, -1(%rcx)
	movq	(%rbx), %rax
	movzbl	(%rax,%rsi), %eax
	movq	(%rdi), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%rdi)
	movb	%al, -1(%rcx)
	addq	$32, %rdi
	addq	$32, %rbx
	addq	$-4, %rbp
	jne	.LBB4_450
.LBB4_451:                              # %._crit_edge34.us46.i
                                        #   in Loop: Header=BB4_446 Depth=2
	decq	%r8
	testl	%r8d, %r8d
	jne	.LBB4_446
# BB#452:                               # %._crit_edge36.loopexit76.i
                                        #   in Loop: Header=BB4_386 Depth=1
	movq	160(%rsp), %r13         # 8-byte Reload
	addl	%r11d, %r13d
.LBB4_453:                              # %._crit_edge36.i
                                        #   in Loop: Header=BB4_386 Depth=1
	movq	80(%rsp), %r8           # 8-byte Reload
	movq	72(%rsp), %r11          # 8-byte Reload
	movq	104(%rsp), %rbx         # 8-byte Reload
.LBB4_454:                              # %._crit_edge36.i
                                        #   in Loop: Header=BB4_386 Depth=1
	testl	%ebx, %ebx
	jle	.LBB4_473
.LBB4_455:                              # %._crit_edge36.i
                                        #   in Loop: Header=BB4_386 Depth=1
	testl	%r11d, %r11d
	jle	.LBB4_473
# BB#456:                               # %.preheader10.i
                                        #   in Loop: Header=BB4_386 Depth=1
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	jle	.LBB4_464
# BB#457:                               # %.lr.ph48.i
                                        #   in Loop: Header=BB4_386 Depth=1
	testq	%r10, %r10
	cltq
	je	.LBB4_458
# BB#459:                               # %.prol.preheader927
                                        #   in Loop: Header=BB4_386 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_460:                              #   Parent Loop BB4_386 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r8,%rdi,8), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%r12,%rdi,8), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r12,%rdi,8)
	movb	%cl, -1(%rdx)
	incq	%rdi
	cmpq	%rdi, %r10
	jne	.LBB4_460
	jmp	.LBB4_461
	.p2align	4, 0x90
.LBB4_458:                              #   in Loop: Header=BB4_386 Depth=1
	xorl	%edi, %edi
.LBB4_461:                              # %.prol.loopexit928
                                        #   in Loop: Header=BB4_386 Depth=1
	cmpq	$3, 40(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_464
# BB#462:                               # %.lr.ph48.i.new
                                        #   in Loop: Header=BB4_386 Depth=1
	movq	48(%rsp), %rsi          # 8-byte Reload
	subq	%rdi, %rsi
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rbx
	movq	112(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB4_463:                              #   Parent Loop BB4_386 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-24(%rbx), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, -24(%rbx)
	movb	%cl, -1(%rdx)
	movq	-16(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-16(%rbx), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, -16(%rbx)
	movb	%cl, -1(%rdx)
	movq	-8(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-8(%rbx), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, -8(%rbx)
	movb	%cl, -1(%rdx)
	movq	(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%rbx), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, (%rbx)
	movb	%cl, -1(%rdx)
	addq	$32, %rbx
	addq	$32, %rdi
	addq	$-4, %rsi
	jne	.LBB4_463
.LBB4_464:                              # %.preheader9.i
                                        #   in Loop: Header=BB4_386 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_472
# BB#465:                               # %.lr.ph50.i
                                        #   in Loop: Header=BB4_386 Depth=1
	testq	%r14, %r14
	movslq	16(%rsp), %rax          # 4-byte Folded Reload
	je	.LBB4_466
# BB#467:                               # %.prol.preheader932
                                        #   in Loop: Header=BB4_386 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_468:                              #   Parent Loop BB4_386 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r9,%rdi,8), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%r15,%rdi,8), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r15,%rdi,8)
	movb	%cl, -1(%rdx)
	incq	%rdi
	cmpq	%rdi, %r14
	jne	.LBB4_468
	jmp	.LBB4_469
	.p2align	4, 0x90
.LBB4_466:                              #   in Loop: Header=BB4_386 Depth=1
	xorl	%edi, %edi
.LBB4_469:                              # %.prol.loopexit933
                                        #   in Loop: Header=BB4_386 Depth=1
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB4_472
# BB#470:                               # %.lr.ph50.i.new
                                        #   in Loop: Header=BB4_386 Depth=1
	movq	96(%rsp), %rsi          # 8-byte Reload
	subq	%rdi, %rsi
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rbx
	movq	168(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB4_471:                              #   Parent Loop BB4_386 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-24(%rbx), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, -24(%rbx)
	movb	%cl, -1(%rdx)
	movq	-16(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-16(%rbx), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, -16(%rbx)
	movb	%cl, -1(%rdx)
	movq	-8(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	-8(%rbx), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, -8(%rbx)
	movb	%cl, -1(%rdx)
	movq	(%rdi), %rcx
	movzbl	(%rcx,%rax), %ecx
	movq	(%rbx), %rdx
	leaq	-1(%rdx), %rbp
	movq	%rbp, (%rbx)
	movb	%cl, -1(%rdx)
	addq	$32, %rbx
	addq	$32, %rdi
	addq	$-4, %rsi
	jne	.LBB4_471
.LBB4_472:                              # %._crit_edge51.i
                                        #   in Loop: Header=BB4_386 Depth=1
	addl	$2, %r13d
	cmpl	152(%rsp), %r13d        # 4-byte Folded Reload
	jle	.LBB4_386
	jmp	.LBB4_473
.LBB4_444:                              # %.preheader6.preheader.i
                                        #   in Loop: Header=BB4_386 Depth=1
	addl	%edx, %r13d
	testl	%ebx, %ebx
	jg	.LBB4_455
.LBB4_473:                              # %Atracking_localhom.exit
	movq	(%r12), %rdi
	callq	strlen
	movq	%rax, %rcx
	movl	368(%rsp), %edx
	cmpl	%edx, %ecx
	jg	.LBB4_475
# BB#474:                               # %Atracking_localhom.exit
	cmpl	$5000001, %ecx          # imm = 0x4C4B41
	jge	.LBB4_475
.LBB4_476:                              # %.preheader406
	movl	28(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	movl	12(%rsp), %r14d         # 4-byte Reload
	movq	80(%rsp), %r15          # 8-byte Reload
	jle	.LBB4_479
# BB#477:                               # %.lr.ph422.preheader
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_478:                              # %.lr.ph422
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbp,8), %rdi
	movq	partA__align.mseq1(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB4_478
.LBB4_479:                              # %.preheader
	testl	%r14d, %r14d
	movl	%r14d, %eax
	movq	136(%rsp), %r14         # 8-byte Reload
	jle	.LBB4_482
# BB#480:                               # %.lr.ph.preheader
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_481:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbp,8), %rdi
	movq	partA__align.mseq2(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB4_481
.LBB4_482:                              # %._crit_edge
	movss	204(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_475:
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	movl	$5000000, %r8d          # imm = 0x4C4B40
	xorl	%eax, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$.L.str.7, %edi
	callq	ErrorExit
	jmp	.LBB4_476
.LBB4_107:
	movq	32(%rsp), %r15          # 8-byte Reload
	jmp	.LBB4_100
.LBB4_123:
	movl	120(%rsp), %r11d        # 4-byte Reload
	jmp	.LBB4_116
.Lfunc_end4:
	.size	partA__align, .Lfunc_end4-partA__align
	.cfi_endproc

	.p2align	4, 0x90
	.type	match_calc,@function
match_calc:                             # @match_calc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 40
.Lcfi60:
	.cfi_offset %rbx, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movq	40(%rsp), %r11
	cmpl	$0, 48(%rsp)
	je	.LBB5_10
# BB#1:
	testl	%r8d, %r8d
	jle	.LBB5_10
# BB#2:                                 # %.preheader77.preheader
	movl	%r8d, %r10d
	xorl	%r14d, %r14d
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB5_3:                                # %.preheader77
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_4 Depth 2
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB5_4:                                #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rax,8), %rbx
	movss	(%rbx,%r14,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB5_5
	jnp	.LBB5_6
.LBB5_5:                                #   in Loop: Header=BB5_4 Depth=2
	movq	(%r9,%r14,8), %rbx
	movslq	%r15d, %r15
	movss	%xmm1, (%rbx,%r15,4)
	movq	(%r11,%r14,8), %rbx
	movl	%eax, (%rbx,%r15,4)
	incl	%r15d
.LBB5_6:                                #   in Loop: Header=BB5_4 Depth=2
	movq	8(%rdx,%rax,8), %rbx
	movss	(%rbx,%r14,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB5_7
	jnp	.LBB5_8
.LBB5_7:                                #   in Loop: Header=BB5_4 Depth=2
	movq	(%r9,%r14,8), %rbx
	movslq	%r15d, %r15
	movss	%xmm1, (%rbx,%r15,4)
	movq	(%r11,%r14,8), %rbx
	leal	1(%rax), %ebp
	movl	%ebp, (%rbx,%r15,4)
	incl	%r15d
.LBB5_8:                                #   in Loop: Header=BB5_4 Depth=2
	addq	$2, %rax
	cmpq	$26, %rax
	jne	.LBB5_4
# BB#9:                                 #   in Loop: Header=BB5_3 Depth=1
	movq	(%r11,%r14,8), %rax
	movslq	%r15d, %rbx
	movl	$-1, (%rax,%rbx,4)
	incq	%r14
	cmpq	%r10, %r14
	jne	.LBB5_3
.LBB5_10:                               # %.preheader76
	movl	$n_dis_consweight_multi, %r10d
	movslq	%ecx, %rcx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_12 Depth 2
	movl	$0, -104(%rsp,%r14,4)
	xorps	%xmm0, %xmm0
	movl	$1, %ebx
	movq	%r10, %rax
	.p2align	4, 0x90
.LBB5_12:                               #   Parent Loop BB5_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rsi,%rbx,8), %rdx
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdx,%rcx,4), %xmm1
	addss	%xmm0, %xmm1
	movss	104(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movq	(%rsi,%rbx,8), %rdx
	mulss	(%rdx,%rcx,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$208, %rax
	addq	$2, %rbx
	cmpq	$27, %rbx
	jne	.LBB5_12
# BB#13:                                #   in Loop: Header=BB5_11 Depth=1
	movss	%xmm0, -104(%rsp,%r14,4)
	incq	%r14
	addq	$4, %r10
	cmpq	$26, %r14
	jne	.LBB5_11
	jmp	.LBB5_14
	.p2align	4, 0x90
.LBB5_18:                               # %._crit_edge
                                        #   in Loop: Header=BB5_14 Depth=1
	addq	$8, %r11
	addq	$8, %r9
	addq	$4, %rdi
.LBB5_14:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_17 Depth 2
	testl	%r8d, %r8d
	je	.LBB5_19
# BB#15:                                # %.lr.ph84
                                        #   in Loop: Header=BB5_14 Depth=1
	decl	%r8d
	movl	$0, (%rdi)
	movq	(%r11), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	js	.LBB5_18
# BB#16:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB5_14 Depth=1
	movq	(%r9), %rcx
	addq	$4, %rax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB5_17:                               # %.lr.ph
                                        #   Parent Loop BB5_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rdx
	movss	-104(%rsp,%rdx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm1
	addq	$4, %rcx
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rdi)
	movl	(%rax), %edx
	addq	$4, %rax
	testl	%edx, %edx
	jns	.LBB5_17
	jmp	.LBB5_18
.LBB5_19:                               # %._crit_edge85
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	match_calc, .Lfunc_end5-match_calc
	.cfi_endproc

	.type	impmtx,@object          # @impmtx
	.local	impmtx
	.comm	impmtx,8,8
	.type	impalloclen,@object     # @impalloclen
	.local	impalloclen
	.comm	impalloclen,4,4
	.type	part_imp_match_init.impalloclen,@object # @part_imp_match_init.impalloclen
	.local	part_imp_match_init.impalloclen
	.comm	part_imp_match_init.impalloclen,4,4
	.type	part_imp_match_init.nocount1,@object # @part_imp_match_init.nocount1
	.local	part_imp_match_init.nocount1
	.comm	part_imp_match_init.nocount1,8,8
	.type	part_imp_match_init.nocount2,@object # @part_imp_match_init.nocount2
	.local	part_imp_match_init.nocount2
	.comm	part_imp_match_init.nocount2,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"i = %d, seq1 = %s\n"
	.size	.L.str, 19

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"start1 = %d\n"
	.size	.L.str.1, 13

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"end1   = %d\n"
	.size	.L.str.2, 13

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"j = %d, seq2 = %s\n"
	.size	.L.str.3, 19

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"step 0\n"
	.size	.L.str.4, 8

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"dif = %d\n"
	.size	.L.str.5, 10

	.type	partA__align.mi,@object # @partA__align.mi
	.local	partA__align.mi
	.comm	partA__align.mi,4,4
	.type	partA__align.m,@object  # @partA__align.m
	.local	partA__align.m
	.comm	partA__align.m,8,8
	.type	partA__align.ijp,@object # @partA__align.ijp
	.local	partA__align.ijp
	.comm	partA__align.ijp,8,8
	.type	partA__align.mpi,@object # @partA__align.mpi
	.local	partA__align.mpi
	.comm	partA__align.mpi,4,4
	.type	partA__align.mp,@object # @partA__align.mp
	.local	partA__align.mp
	.comm	partA__align.mp,8,8
	.type	partA__align.w1,@object # @partA__align.w1
	.local	partA__align.w1
	.comm	partA__align.w1,8,8
	.type	partA__align.w2,@object # @partA__align.w2
	.local	partA__align.w2
	.comm	partA__align.w2,8,8
	.type	partA__align.match,@object # @partA__align.match
	.local	partA__align.match
	.comm	partA__align.match,8,8
	.type	partA__align.initverticalw,@object # @partA__align.initverticalw
	.local	partA__align.initverticalw
	.comm	partA__align.initverticalw,8,8
	.type	partA__align.lastverticalw,@object # @partA__align.lastverticalw
	.local	partA__align.lastverticalw
	.comm	partA__align.lastverticalw,8,8
	.type	partA__align.mseq1,@object # @partA__align.mseq1
	.local	partA__align.mseq1
	.comm	partA__align.mseq1,8,8
	.type	partA__align.mseq2,@object # @partA__align.mseq2
	.local	partA__align.mseq2
	.comm	partA__align.mseq2,8,8
	.type	partA__align.mseq,@object # @partA__align.mseq
	.local	partA__align.mseq
	.comm	partA__align.mseq,8,8
	.type	partA__align.ogcp1,@object # @partA__align.ogcp1
	.local	partA__align.ogcp1
	.comm	partA__align.ogcp1,8,8
	.type	partA__align.ogcp2,@object # @partA__align.ogcp2
	.local	partA__align.ogcp2
	.comm	partA__align.ogcp2,8,8
	.type	partA__align.fgcp1,@object # @partA__align.fgcp1
	.local	partA__align.fgcp1
	.comm	partA__align.fgcp1,8,8
	.type	partA__align.fgcp2,@object # @partA__align.fgcp2
	.local	partA__align.fgcp2
	.comm	partA__align.fgcp2,8,8
	.type	partA__align.cpmx1,@object # @partA__align.cpmx1
	.local	partA__align.cpmx1
	.comm	partA__align.cpmx1,8,8
	.type	partA__align.cpmx2,@object # @partA__align.cpmx2
	.local	partA__align.cpmx2
	.comm	partA__align.cpmx2,8,8
	.type	partA__align.intwork,@object # @partA__align.intwork
	.local	partA__align.intwork
	.comm	partA__align.intwork,8,8
	.type	partA__align.floatwork,@object # @partA__align.floatwork
	.local	partA__align.floatwork
	.comm	partA__align.floatwork,8,8
	.type	partA__align.orlgth1,@object # @partA__align.orlgth1
	.local	partA__align.orlgth1
	.comm	partA__align.orlgth1,4,4
	.type	partA__align.orlgth2,@object # @partA__align.orlgth2
	.local	partA__align.orlgth2
	.comm	partA__align.orlgth2,4,4
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"alloclen=%d, resultlen=%d, N=%d\n"
	.size	.L.str.6, 33

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"LENGTH OVER!\n"
	.size	.L.str.7, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
