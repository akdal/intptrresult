	.text
	.file	"DefaultName.bc"
	.globl	_Z15GetDefaultName2RK11CStringBaseIwES2_S2_
	.p2align	4, 0x90
	.type	_Z15GetDefaultName2RK11CStringBaseIwES2_S2_,@function
_Z15GetDefaultName2RK11CStringBaseIwES2_S2_: # @_Z15GetDefaultName2RK11CStringBaseIwES2_S2_
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 64
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r13, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movq	%rdi, %r15
	movl	8(%rbx), %ecx
	movl	8(%r12), %eax
	leal	1(%rcx), %edx
	movl	%eax, %r13d
	subl	%edx, %r13d
	jle	.LBB0_7
# BB#1:
	movq	(%r12), %rdx
	movslq	%r13d, %rsi
	cmpl	$46, (%rdx,%rsi,4)
	jne	.LBB0_7
# BB#2:
	leal	1(%r13), %edx
	movq	%rsp, %rdi
	movq	%r12, %rsi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	_ZNK11CStringBaseIwE3MidEii
	movq	(%rbx), %rdi
	movq	(%rsp), %rsi
.Ltmp0:
	callq	_Z21MyStringCompareNoCasePKwS0_
	movl	%eax, %ebx
.Ltmp1:
# BB#3:                                 # %_ZNK11CStringBaseIwE13CompareNoCaseERKS0_.exit.i
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_5
# BB#4:
	callq	_ZdaPv
.LBB0_5:                                # %_ZN11CStringBaseIwED2Ev.exit38.i
	testl	%ebx, %ebx
	je	.LBB0_27
# BB#6:                                 # %_ZN11CStringBaseIwED2Ev.exit38.thread-pre-split_crit_edge.i
	movl	8(%r12), %eax
.LBB0_7:                                # %thread-pre-split.i
	testl	%eax, %eax
	je	.LBB0_11
# BB#8:
	movq	(%r12), %rdx
	cltq
	shlq	$2, %rax
	.p2align	4, 0x90
.LBB0_9:                                # =>This Inner Loop Header: Depth=1
	cmpl	$46, -4(%rdx,%rax)
	je	.LBB0_13
# BB#10:                                #   in Loop: Header=BB0_9 Depth=1
	addq	$-4, %rax
	jne	.LBB0_9
	jmp	.LBB0_11
.LBB0_13:                               # %_ZNK11CStringBaseIwE11ReverseFindEw.exit.i
	leaq	-4(%rdx,%rax), %rcx
	subq	%rdx, %rcx
	shrq	$2, %rcx
	testl	%ecx, %ecx
	jle	.LBB0_11
# BB#14:
	movq	%rsp, %rdi
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	movq	%r12, %rsi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	_ZNK11CStringBaseIwE3MidEii
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
	movslq	8(%rsp), %r13
	leaq	1(%r13), %r12
	testl	%r12d, %r12d
	je	.LBB0_17
# BB#15:                                # %._crit_edge16.i.i.i42.i
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp3:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp4:
# BB#16:                                # %.noexc51.i
	movq	%rbx, (%r15)
	movl	$0, (%rbx)
	movl	%r12d, 12(%r15)
.LBB0_17:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i43.i
	movq	(%rsp), %rax
	.p2align	4, 0x90
.LBB0_18:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB0_18
# BB#19:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i46.i
	movl	%r13d, 8(%r15)
	movl	8(%r14), %esi
.Ltmp6:
	movq	%r15, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp7:
# BB#20:                                # %.noexc.i47.i
	movslq	8(%r15), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%r15), %rcx
	movq	(%r14), %rsi
	.p2align	4, 0x90
.LBB0_21:                               # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %edx
	addq	$4, %rsi
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB0_21
# BB#22:
	movl	8(%r14), %ecx
	addl	%eax, %ecx
	movl	%ecx, 8(%r15)
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_25
# BB#23:
	callq	_ZdaPv
	jmp	.LBB0_25
.LBB0_11:                               # %_ZNK11CStringBaseIwE11ReverseFindEw.exit.thread.i
	cmpl	$0, 8(%r14)
	je	.LBB0_24
# BB#12:
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	_ZplIwE11CStringBaseIT_ERKS2_S4_
	jmp	.LBB0_25
.LBB0_24:
	movl	$.L.str, %edx
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	_ZplIwE11CStringBaseIT_ERKS2_PKS1_
.LBB0_25:                               # %_ZL15GetDefaultName3RK11CStringBaseIwES2_S2_.exit
.Ltmp15:
	movq	%r15, %rdi
	callq	_ZN11CStringBaseIwE9TrimRightEv
.Ltmp16:
# BB#26:
	movq	%r15, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB0_27:
	movq	%rsp, %rdi
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	%r13d, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
	movslq	8(%rsp), %r13
	leaq	1(%r13), %r12
	testl	%r12d, %r12d
	je	.LBB0_30
# BB#28:                                # %._crit_edge16.i.i.i.i
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp9:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp10:
# BB#29:                                # %.noexc.i
	movq	%rbx, (%r15)
	movl	$0, (%rbx)
	movl	%r12d, 12(%r15)
.LBB0_30:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i.i
	movq	(%rsp), %rax
	.p2align	4, 0x90
.LBB0_31:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB0_31
# BB#32:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i.i
	movl	%r13d, 8(%r15)
	movl	8(%r14), %esi
.Ltmp12:
	movq	%r15, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp13:
# BB#33:                                # %.noexc.i.i
	movslq	8(%r15), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%r15), %rcx
	movq	(%r14), %rsi
	.p2align	4, 0x90
.LBB0_34:                               # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %edx
	addq	$4, %rsi
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB0_34
# BB#35:                                # %.critedge.i
	movl	8(%r14), %ecx
	addl	%eax, %ecx
	movl	%ecx, 8(%r15)
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_25
# BB#36:
	callq	_ZdaPv
	jmp	.LBB0_25
.LBB0_37:
.Ltmp11:
	jmp	.LBB0_44
.LBB0_38:
.Ltmp14:
	jmp	.LBB0_39
.LBB0_41:
.Ltmp5:
	jmp	.LBB0_44
.LBB0_42:
.Ltmp8:
.LBB0_39:
	movq	%rax, %rbx
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB0_45
# BB#40:
	callq	_ZdaPv
	jmp	.LBB0_45
.LBB0_43:
.Ltmp2:
.LBB0_44:                               # %.body.i
	movq	%rax, %rbx
.LBB0_45:                               # %.body.i
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_47
	jmp	.LBB0_48
.LBB0_46:
.Ltmp17:
	movq	%rax, %rbx
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB0_48
.LBB0_47:
	callq	_ZdaPv
.LBB0_48:                               # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z15GetDefaultName2RK11CStringBaseIwES2_S2_, .Lfunc_end0-_Z15GetDefaultName2RK11CStringBaseIwES2_S2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\222\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp15-.Ltmp7          #   Call between .Ltmp7 and .Ltmp15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp9-.Ltmp16          #   Call between .Ltmp16 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 9 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Lfunc_end0-.Ltmp13     #   Call between .Ltmp13 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIwE9TrimRightEv,"axG",@progbits,_ZN11CStringBaseIwE9TrimRightEv,comdat
	.weak	_ZN11CStringBaseIwE9TrimRightEv
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE9TrimRightEv,@function
_ZN11CStringBaseIwE9TrimRightEv:        # @_ZN11CStringBaseIwE9TrimRightEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	_ZN11CStringBaseIwE21GetTrimDefaultCharSetEv
	movq	(%r14), %r8
	movl	(%r8), %ebx
	testl	%ebx, %ebx
	je	.LBB1_11
# BB#1:                                 # %.lr.ph.i
	movq	8(%rsp), %r10
	movl	(%r10), %edi
	xorl	%r9d, %r9d
	movq	%r8, %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_5 Depth 2
	cmpl	%ebx, %edi
	movq	%r10, %rdx
	movl	%edi, %esi
	je	.LBB1_3
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph.i.i.i
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%esi, %esi
	je	.LBB1_6
# BB#4:                                 #   in Loop: Header=BB1_5 Depth=2
	movl	4(%rdx), %esi
	addq	$4, %rdx
	cmpl	%ebx, %esi
	jne	.LBB1_5
.LBB1_3:                                # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB1_2 Depth=1
	subq	%r10, %rdx
	shrq	$2, %rdx
	jmp	.LBB1_7
	.p2align	4, 0x90
.LBB1_6:                                #   in Loop: Header=BB1_2 Depth=1
	movl	$-1, %edx
.LBB1_7:                                # %_ZNK11CStringBaseIwE4FindEw.exit.i
                                        #   in Loop: Header=BB1_2 Depth=1
	testq	%rcx, %rcx
	cmoveq	%rax, %rcx
	testl	%edx, %edx
	cmovsq	%r9, %rcx
	movl	4(%rax), %ebx
	addq	$4, %rax
	testl	%ebx, %ebx
	jne	.LBB1_2
# BB#8:                                 # %._crit_edge.i
	testq	%rcx, %rcx
	je	.LBB1_11
# BB#9:
	subq	%r8, %rcx
	movq	%rcx, %rsi
	shrq	$2, %rsi
	movslq	8(%r14), %rax
	movl	%eax, %edx
	subl	%esi, %edx
	jle	.LBB1_11
# BB#10:
	shlq	$30, %rcx
	sarq	$30, %rcx
	movl	(%r8,%rax,4), %eax
	movl	%eax, (%r8,%rcx)
	subl	%edx, 8(%r14)
.LBB1_11:                               # %_ZN11CStringBaseIwE20TrimRightWithCharSetERKS0_.exit
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_13
# BB#12:
	callq	_ZdaPv
.LBB1_13:                               # %_ZN11CStringBaseIwED2Ev.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	_ZN11CStringBaseIwE9TrimRightEv, .Lfunc_end1-_ZN11CStringBaseIwE9TrimRightEv
	.cfi_endproc

	.section	.text._ZplIwE11CStringBaseIT_ERKS2_S4_,"axG",@progbits,_ZplIwE11CStringBaseIT_ERKS2_S4_,comdat
	.weak	_ZplIwE11CStringBaseIT_ERKS2_S4_
	.p2align	4, 0x90
	.type	_ZplIwE11CStringBaseIT_ERKS2_S4_,@function
_ZplIwE11CStringBaseIT_ERKS2_S4_:       # @_ZplIwE11CStringBaseIT_ERKS2_S4_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 48
.Lcfi21:
	.cfi_offset %rbx, -48
.Lcfi22:
	.cfi_offset %r12, -40
.Lcfi23:
	.cfi_offset %r13, -32
.Lcfi24:
	.cfi_offset %r14, -24
.Lcfi25:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	8(%r15), %r13
	leaq	1(%r13), %r12
	testl	%r12d, %r12d
	je	.LBB2_1
# BB#2:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%r12d, 12(%rbx)
	jmp	.LBB2_3
.LBB2_1:
	xorl	%eax, %eax
.LBB2_3:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB2_4:                                # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB2_4
# BB#5:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit
	movl	%r13d, 8(%rbx)
	movl	8(%r14), %esi
.Ltmp18:
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp19:
# BB#6:                                 # %.noexc
	movslq	8(%rbx), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%rbx), %rcx
	movq	(%r14), %rsi
	.p2align	4, 0x90
.LBB2_7:                                # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %edx
	addq	$4, %rsi
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB2_7
# BB#8:
	movl	8(%r14), %ecx
	addl	%eax, %ecx
	movl	%ecx, 8(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB2_9:
.Ltmp20:
	movq	%rax, %r14
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_11
# BB#10:
	callq	_ZdaPv
.LBB2_11:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZplIwE11CStringBaseIT_ERKS2_S4_, .Lfunc_end2-_ZplIwE11CStringBaseIT_ERKS2_S4_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp18-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin1   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Lfunc_end2-.Ltmp19     #   Call between .Ltmp19 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZplIwE11CStringBaseIT_ERKS2_PKS1_,"axG",@progbits,_ZplIwE11CStringBaseIT_ERKS2_PKS1_,comdat
	.weak	_ZplIwE11CStringBaseIT_ERKS2_PKS1_
	.p2align	4, 0x90
	.type	_ZplIwE11CStringBaseIT_ERKS2_PKS1_,@function
_ZplIwE11CStringBaseIT_ERKS2_PKS1_:     # @_ZplIwE11CStringBaseIT_ERKS2_PKS1_
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -48
.Lcfi32:
	.cfi_offset %r12, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movslq	8(%r15), %r12
	leaq	1(%r12), %rbp
	testl	%ebp, %ebp
	je	.LBB3_1
# BB#2:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%ebp, 12(%r14)
	jmp	.LBB3_3
.LBB3_1:
	xorl	%eax, %eax
.LBB3_3:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB3_4:                                # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB3_4
# BB#5:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit
	movl	%r12d, 8(%r14)
	movl	$-1, %ebp
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB3_6:                                # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB3_6
# BB#7:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
.Ltmp21:
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp22:
# BB#8:                                 # %.noexc
	movslq	8(%r14), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%r14), %rcx
	.p2align	4, 0x90
.LBB3_9:                                # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %edx
	addq	$4, %rbx
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB3_9
# BB#10:
	addl	%eax, %ebp
	movl	%ebp, 8(%r14)
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_11:
.Ltmp23:
	movq	%rax, %rbx
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB3_13
# BB#12:
	callq	_ZdaPv
.LBB3_13:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZplIwE11CStringBaseIT_ERKS2_PKS1_, .Lfunc_end3-_ZplIwE11CStringBaseIT_ERKS2_PKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp21-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin2   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Lfunc_end3-.Ltmp22     #   Call between .Ltmp22 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 64
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB4_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB4_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB4_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB4_5
.LBB4_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB4_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp24:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp25:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB4_35
# BB#12:
	movq	%rbx, %r13
.LBB4_13:                               # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB4_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB4_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB4_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB4_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB4_15
.LBB4_14:
	xorl	%esi, %esi
.LBB4_15:                               # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB4_16:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB4_16
.LBB4_29:
	movq	%r13, %rbx
.LBB4_30:                               # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp26:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp27:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB4_32:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB4_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB4_8
.LBB4_3:
	xorl	%eax, %eax
.LBB4_5:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB4_6:                                # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB4_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB4_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_35:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB4_30
.LBB4_21:                               # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB4_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_24:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB4_24
	jmp	.LBB4_25
.LBB4_22:
	xorl	%ecx, %ecx
.LBB4_25:                               # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB4_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB4_27:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB4_27
.LBB4_28:                               # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB4_15
	jmp	.LBB4_29
.LBB4_34:                               # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp28:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end4-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp24-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp27-.Ltmp24         #   Call between .Ltmp24 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin3   #     jumps to .Ltmp28
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Lfunc_end4-.Ltmp27     #   Call between .Ltmp27 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIwE10GrowLengthEi,"axG",@progbits,_ZN11CStringBaseIwE10GrowLengthEi,comdat
	.weak	_ZN11CStringBaseIwE10GrowLengthEi
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE10GrowLengthEi,@function
_ZN11CStringBaseIwE10GrowLengthEi:      # @_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 48
.Lcfi54:
	.cfi_offset %rbx, -48
.Lcfi55:
	.cfi_offset %r12, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	8(%r14), %ebp
	movl	12(%r14), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	%esi, %eax
	jg	.LBB5_28
# BB#1:
	decl	%eax
	cmpl	$8, %ebx
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebx
	jl	.LBB5_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
.LBB5_3:                                # %select.end
	movl	%esi, %edx
	subl	%eax, %edx
	addl	%ecx, %eax
	cmpl	%esi, %eax
	cmovgel	%ecx, %edx
	leal	1(%rbx,%rdx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB5_28
# BB#4:
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB5_27
# BB#5:                                 # %.preheader.i
	movq	(%r14), %rdi
	testl	%ebp, %ebp
	jle	.LBB5_25
# BB#6:                                 # %.lr.ph.i
	movslq	%ebp, %rax
	cmpl	$7, %ebp
	jbe	.LBB5_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB5_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r12
	jae	.LBB5_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB5_17
.LBB5_7:
	xorl	%ecx, %ecx
.LBB5_8:                                # %scalar.ph.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB5_11
# BB#9:                                 # %scalar.ph.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB5_10:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %esi
	movl	%esi, (%r12,%rcx,4)
	incq	%rcx
	incq	%rbp
	jne	.LBB5_10
.LBB5_11:                               # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB5_26
# BB#12:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%r12,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB5_13:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB5_13
	jmp	.LBB5_26
.LBB5_25:                               # %._crit_edge.i
	testq	%rdi, %rdi
	je	.LBB5_27
.LBB5_26:                               # %._crit_edge.thread.i
	callq	_ZdaPv
	movl	8(%r14), %ebp
.LBB5_27:                               # %._crit_edge16.i
	movq	%r12, (%r14)
	movslq	%ebp, %rax
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
.LBB5_28:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_17:                               # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB5_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r12,%rbx,4)
	movups	%xmm1, 16(%r12,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB5_20
	jmp	.LBB5_21
.LBB5_18:
	xorl	%ebx, %ebx
.LBB5_21:                               # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB5_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx,4), %rsi
	leaq	112(%rdi,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB5_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB5_23
.LBB5_24:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB5_8
	jmp	.LBB5_26
.Lfunc_end5:
	.size	_ZN11CStringBaseIwE10GrowLengthEi, .Lfunc_end5-_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_endproc

	.section	.text._ZN11CStringBaseIwE21GetTrimDefaultCharSetEv,"axG",@progbits,_ZN11CStringBaseIwE21GetTrimDefaultCharSetEv,comdat
	.weak	_ZN11CStringBaseIwE21GetTrimDefaultCharSetEv
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE21GetTrimDefaultCharSetEv,@function
_ZN11CStringBaseIwE21GetTrimDefaultCharSetEv: # @_ZN11CStringBaseIwE21GetTrimDefaultCharSetEv
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi61:
	.cfi_def_cfa_offset 32
.Lcfi62:
	.cfi_offset %rbx, -24
.Lcfi63:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	$4, 12(%rbx)
.Ltmp29:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp30:
# BB#1:
	movq	(%rbx), %rax
	movslq	8(%rbx), %rcx
	movl	$32, (%rax,%rcx,4)
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%rbx)
	movl	$0, 4(%rax,%rcx,4)
.Ltmp31:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp32:
# BB#2:
	movq	(%rbx), %rax
	movslq	8(%rbx), %rcx
	movl	$10, (%rax,%rcx,4)
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%rbx)
	movl	$0, 4(%rax,%rcx,4)
.Ltmp33:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp34:
# BB#3:
	movq	(%rbx), %rax
	movslq	8(%rbx), %rcx
	movl	$9, (%rax,%rcx,4)
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%rbx)
	movl	$0, 4(%rax,%rcx,4)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB6_4:
.Ltmp35:
	movq	%rax, %r14
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_6
# BB#5:
	callq	_ZdaPv
.LBB6_6:                                # %_ZN11CStringBaseIwED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN11CStringBaseIwE21GetTrimDefaultCharSetEv, .Lfunc_end6-_ZN11CStringBaseIwE21GetTrimDefaultCharSetEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp29-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp34-.Ltmp29         #   Call between .Ltmp29 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin4   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Lfunc_end6-.Ltmp34     #   Call between .Ltmp34 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	126                     # 0x7e
	.long	0                       # 0x0
	.size	.L.str, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
