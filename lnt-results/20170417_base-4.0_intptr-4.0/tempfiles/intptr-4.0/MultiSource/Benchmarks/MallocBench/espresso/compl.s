	.text
	.file	"compl.bc"
	.globl	complement
	.p2align	4, 0x90
	.type	complement,@function
complement:                             # @complement
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	testb	$1, debug(%rip)
	je	.LBB0_2
# BB#1:
	movl	complement.compl_level(%rip), %edx
	leal	1(%rdx), %eax
	movl	%eax, complement.compl_level(%rip)
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	debug_print
.LBB0_2:
	movq	16(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB0_10
# BB#3:
	movq	(%r14), %r15
	cmpq	$0, 24(%r14)
	je	.LBB0_11
# BB#4:                                 # %.preheader.preheader
	leaq	24(%r14), %r12
	testq	%rcx, %rcx
	je	.LBB0_8
# BB#5:
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph49
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r15, %rsi
	callq	full_row
	testl	%eax, %eax
	jne	.LBB0_33
# BB#7:                                 # %..preheader_crit_edge
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	(%rbx), %rcx
	addq	$8, %rbx
	testq	%rcx, %rcx
	jne	.LBB0_6
.LBB0_8:                                # %.preheader._crit_edge
	movl	(%r15), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	cmpl	$33, %eax
	jae	.LBB0_12
# BB#9:
	movl	$8, %edi
	jmp	.LBB0_13
.LBB0_10:
	movl	cube(%rip), %esi
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rcx
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_addset
	jmp	.LBB0_34
.LBB0_11:
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r15, %rsi
	movq	%rcx, %rdx
	callq	set_or
	movq	%rax, %rdi
	callq	compl_cube
	jmp	.LBB0_34
.LBB0_12:
	decl	%eax
	shrl	$5, %eax
	leal	8(,%rax,4), %edi
.LBB0_13:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r15, %rsi
	callq	set_copy
	movq	%rax, %rbx
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.LBB0_29
# BB#14:                                # %.lr.ph.preheader
	leaq	-4(%rbx), %r11
	leaq	4(%rbx), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leaq	-12(%rbx), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	$1, %r13d
	movl	$2, %r8d
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_23 Depth 2
                                        #     Child Loop BB0_27 Depth 2
	movl	(%rbx), %ecx
	movl	%ecx, %esi
	andl	$1023, %esi             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movq	%rsi, %rdx
	cmoveq	%r13, %rdx
	cmpq	$8, %rdx
	jb	.LBB0_26
# BB#16:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_15 Depth=1
	movq	%rdx, %r10
	andq	$1016, %r10             # imm = 0x3F8
	je	.LBB0_26
# BB#17:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_15 Depth=1
	leaq	1(%rsi), %rdi
	testl	%esi, %esi
	cmovneq	%r8, %rdi
	leaq	(%r11,%rdi,4), %rbp
	leaq	4(%rax,%rsi,4), %rcx
	cmpq	%rcx, %rbp
	jae	.LBB0_19
# BB#18:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_15 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rsi,4), %rcx
	leaq	-4(%rax,%rdi,4), %rdi
	cmpq	%rcx, %rdi
	jb	.LBB0_26
.LBB0_19:                               # %vector.body.preheader
                                        #   in Loop: Header=BB0_15 Depth=1
	leaq	-8(%r10), %rcx
	movq	%rcx, %rbp
	shrq	$3, %rbp
	btl	$3, %ecx
	jb	.LBB0_21
# BB#20:                                # %vector.body.prol
                                        #   in Loop: Header=BB0_15 Depth=1
	movups	-12(%rbx,%rsi,4), %xmm0
	movups	-28(%rbx,%rsi,4), %xmm1
	movups	-12(%rax,%rsi,4), %xmm2
	movups	-28(%rax,%rsi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rsi,4)
	movups	%xmm3, -28(%rbx,%rsi,4)
	movl	$8, %edi
	testq	%rbp, %rbp
	jne	.LBB0_22
	jmp	.LBB0_24
.LBB0_21:                               #   in Loop: Header=BB0_15 Depth=1
	xorl	%edi, %edi
	testq	%rbp, %rbp
	je	.LBB0_24
.LBB0_22:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_15 Depth=1
	movq	%rdx, %rbp
	andq	$-8, %rbp
	negq	%rbp
	negq	%rdi
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%rsi,4), %r8
	leaq	-12(%rax,%rsi,4), %r9
	.p2align	4, 0x90
.LBB0_23:                               # %vector.body
                                        #   Parent Loop BB0_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r8,%rdi,4), %xmm0
	movups	-16(%r8,%rdi,4), %xmm1
	movups	(%r9,%rdi,4), %xmm2
	movups	-16(%r9,%rdi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%r8,%rdi,4)
	movups	%xmm3, -16(%r8,%rdi,4)
	movups	-32(%r8,%rdi,4), %xmm0
	movups	-48(%r8,%rdi,4), %xmm1
	movups	-32(%r9,%rdi,4), %xmm2
	movups	-48(%r9,%rdi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%r8,%rdi,4)
	movups	%xmm3, -48(%r8,%rdi,4)
	addq	$-16, %rdi
	cmpq	%rdi, %rbp
	jne	.LBB0_23
.LBB0_24:                               # %middle.block
                                        #   in Loop: Header=BB0_15 Depth=1
	cmpq	%r10, %rdx
	movl	$2, %r8d
	je	.LBB0_28
# BB#25:                                #   in Loop: Header=BB0_15 Depth=1
	subq	%r10, %rsi
	.p2align	4, 0x90
.LBB0_26:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_15 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB0_27:                               # %scalar.ph
                                        #   Parent Loop BB0_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rax,%rsi,4), %ecx
	orl	%ecx, -4(%rbx,%rsi,4)
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB0_27
.LBB0_28:                               # %.loopexit
                                        #   in Loop: Header=BB0_15 Depth=1
	movq	(%r12), %rax
	addq	$8, %r12
	testq	%rax, %rax
	jne	.LBB0_15
.LBB0_29:                               # %._crit_edge
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	setp_equal
	testl	%eax, %eax
	je	.LBB0_40
# BB#30:
	testq	%rbx, %rbx
	je	.LBB0_32
# BB#31:
	movq	%rbx, %rdi
	callq	free
.LBB0_32:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	massive_count
	movl	cdata+32(%rip), %eax
	cmpl	$1, %eax
	jne	.LBB0_43
.LBB0_33:
	movl	cube(%rip), %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	sf_new
.LBB0_34:
	movq	%rax, %rbx
.LBB0_35:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_37
# BB#36:
	callq	free
.LBB0_37:
	movq	%r14, %rdi
	callq	free
	testb	$1, debug(%rip)
	je	.LBB0_39
.LBB0_38:
	movl	complement.compl_level(%rip), %edx
	decl	%edx
	movl	%edx, complement.compl_level(%rip)
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	debug1_print
.LBB0_39:
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_40:
	movq	%rbx, %rdi
	callq	compl_cube
	movq	%rax, %r12
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbx, %rdx
	callq	set_diff
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r15, %rsi
	movq	%rcx, %rdx
	callq	set_or
	testq	%rbx, %rbx
	je	.LBB0_42
# BB#41:
	movq	%rbx, %rdi
	callq	free
.LBB0_42:
	movq	%r14, %rdi
	callq	complement
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r12, %rsi
	callq	sf_append
	movq	%rax, %rbx
	testb	$1, debug(%rip)
	jne	.LBB0_38
	jmp	.LBB0_39
.LBB0_43:
	cmpl	%eax, cdata+36(%rip)
	jne	.LBB0_47
# BB#44:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	map_cover_to_unate
	movq	%rax, %rbx
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_46
# BB#45:
	callq	free
.LBB0_46:
	movq	%r14, %rdi
	callq	free
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	unate_compl
	movq	%rax, %r14
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	map_unate_to_cover
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_free
	testb	$1, debug(%rip)
	jne	.LBB0_38
	jmp	.LBB0_39
.LBB0_47:                               # %compl_special_cases.exit
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	jge	.LBB0_49
# BB#48:
	movl	$8, %edi
	jmp	.LBB0_50
.LBB0_49:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB0_50:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, %r15
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	jge	.LBB0_52
# BB#51:
	movl	$8, %edi
	jmp	.LBB0_53
.LBB0_52:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB0_53:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, %r12
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	binate_split_select
	movl	%eax, %ebx
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rsi
	movl	%ebx, %edx
	callq	scofactor
	movq	%rax, %rdi
	callq	complement
	movq	%rax, %r13
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movl	%ebx, %edx
	callq	scofactor
	movq	%rax, %rdi
	callq	complement
	movl	12(%rax), %ecx
	movl	12(%r13), %edx
	movl	%edx, %esi
	imull	%ecx, %esi
	movslq	%esi, %rsi
	addl	%ecx, %edx
	movslq	%edx, %rcx
	movq	8(%r14), %rdx
	subq	%r14, %rdx
	sarq	$3, %rdx
	addq	$-3, %rdx
	imulq	%rcx, %rdx
	xorl	%ecx, %ecx
	cmpq	%rdx, %rsi
	setg	%cl
	movl	%ecx, (%rsp)
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%r15, %rcx
	movq	%r12, %r8
	movl	%ebx, %r9d
	callq	compl_merge
	movq	%rax, %rbx
	testq	%r15, %r15
	je	.LBB0_55
# BB#54:
	movq	%r15, %rdi
	callq	free
.LBB0_55:
	testq	%r12, %r12
	je	.LBB0_35
# BB#56:
	movq	%r12, %rdi
	callq	free
	jmp	.LBB0_35
.Lfunc_end0:
	.size	complement, .Lfunc_end0-complement
	.cfi_endproc

	.globl	simp_comp
	.p2align	4, 0x90
	.type	simp_comp,@function
simp_comp:                              # @simp_comp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 144
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	testb	$1, debug(%rip)
	je	.LBB1_2
# BB#1:
	movl	simp_comp.simplify_level(%rip), %edx
	leal	1(%rdx), %eax
	movl	%eax, simp_comp.simplify_level(%rip)
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	debug_print
.LBB1_2:
	movq	16(%rbp), %rcx
	testq	%rcx, %rcx
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	je	.LBB1_31
# BB#3:
	movq	(%rbp), %r13
	cmpq	$0, 24(%rbp)
	je	.LBB1_32
# BB#4:                                 # %.preheader.preheader
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	leaq	24(%rbp), %rbp
	testq	%rcx, %rcx
	je	.LBB1_8
# BB#5:
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph58
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r13, %rsi
	callq	full_row
	testl	%eax, %eax
	jne	.LBB1_41
# BB#7:                                 # %..preheader_crit_edge
                                        #   in Loop: Header=BB1_6 Depth=1
	movq	(%rbx), %rcx
	addq	$8, %rbx
	testq	%rcx, %rcx
	jne	.LBB1_6
.LBB1_8:                                # %.preheader._crit_edge
	movl	(%r13), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	movl	$2, %ecx
	cmpl	$33, %eax
	jb	.LBB1_10
# BB#9:
	decl	%eax
	shrl	$5, %eax
	addl	$2, %eax
	movl	%eax, %ecx
.LBB1_10:
	movl	%ecx, %edi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r13, %rsi
	callq	set_copy
	movq	%rax, %rbx
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	testq	%rax, %rax
	movq	%r14, 40(%rsp)          # 8-byte Spill
	je	.LBB1_26
# BB#11:                                # %.lr.ph53.preheader
	leaq	-4(%rbx), %r11
	leaq	4(%rbx), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	leaq	-12(%rbx), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	$1, %r10d
	movl	$2, %r8d
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph53
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_20 Depth 2
                                        #     Child Loop BB1_24 Depth 2
	movl	(%rbx), %ecx
	movl	%ecx, %esi
	andl	$1023, %esi             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movq	%rsi, %r15
	cmoveq	%r10, %r15
	cmpq	$8, %r15
	jb	.LBB1_23
# BB#13:                                # %min.iters.checked
                                        #   in Loop: Header=BB1_12 Depth=1
	movq	%r15, %r12
	andq	$1016, %r12             # imm = 0x3F8
	je	.LBB1_23
# BB#14:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_12 Depth=1
	leaq	1(%rsi), %rdx
	testl	%esi, %esi
	cmovneq	%r8, %rdx
	leaq	(%r11,%rdx,4), %rdi
	leaq	4(%rax,%rsi,4), %rcx
	cmpq	%rcx, %rdi
	jae	.LBB1_16
# BB#15:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_12 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rsi,4), %rcx
	leaq	-4(%rax,%rdx,4), %rdx
	cmpq	%rcx, %rdx
	jb	.LBB1_23
.LBB1_16:                               # %vector.body.preheader
                                        #   in Loop: Header=BB1_12 Depth=1
	leaq	-8(%r12), %rcx
	movq	%rcx, %rdx
	shrq	$3, %rdx
	btl	$3, %ecx
	jb	.LBB1_18
# BB#17:                                # %vector.body.prol
                                        #   in Loop: Header=BB1_12 Depth=1
	movups	-12(%rbx,%rsi,4), %xmm0
	movups	-28(%rbx,%rsi,4), %xmm1
	movups	-12(%rax,%rsi,4), %xmm2
	movups	-28(%rax,%rsi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rsi,4)
	movups	%xmm3, -28(%rbx,%rsi,4)
	movl	$8, %r14d
	testq	%rdx, %rdx
	jne	.LBB1_19
	jmp	.LBB1_21
.LBB1_18:                               #   in Loop: Header=BB1_12 Depth=1
	xorl	%r14d, %r14d
	testq	%rdx, %rdx
	je	.LBB1_21
.LBB1_19:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB1_12 Depth=1
	movq	%r15, %rdx
	andq	$-8, %rdx
	negq	%rdx
	negq	%r14
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rsi,4), %r8
	leaq	-12(%rax,%rsi,4), %r9
	.p2align	4, 0x90
.LBB1_20:                               # %vector.body
                                        #   Parent Loop BB1_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r8,%r14,4), %xmm0
	movups	-16(%r8,%r14,4), %xmm1
	movups	(%r9,%r14,4), %xmm2
	movups	-16(%r9,%r14,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%r8,%r14,4)
	movups	%xmm3, -16(%r8,%r14,4)
	movups	-32(%r8,%r14,4), %xmm0
	movups	-48(%r8,%r14,4), %xmm1
	movups	-32(%r9,%r14,4), %xmm2
	movups	-48(%r9,%r14,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%r8,%r14,4)
	movups	%xmm3, -48(%r8,%r14,4)
	addq	$-16, %r14
	cmpq	%r14, %rdx
	jne	.LBB1_20
.LBB1_21:                               # %middle.block
                                        #   in Loop: Header=BB1_12 Depth=1
	cmpq	%r12, %r15
	movq	40(%rsp), %r14          # 8-byte Reload
	movl	$2, %r8d
	je	.LBB1_25
# BB#22:                                #   in Loop: Header=BB1_12 Depth=1
	subq	%r12, %rsi
	.p2align	4, 0x90
.LBB1_23:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB1_12 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB1_24:                               # %scalar.ph
                                        #   Parent Loop BB1_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rax,%rsi,4), %ecx
	orl	%ecx, -4(%rbx,%rsi,4)
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB1_24
.LBB1_25:                               # %.loopexit
                                        #   in Loop: Header=BB1_12 Depth=1
	movq	(%rbp), %rax
	addq	$8, %rbp
	testq	%rax, %rax
	jne	.LBB1_12
.LBB1_26:                               # %._crit_edge54
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	setp_equal
	testl	%eax, %eax
	je	.LBB1_43
# BB#27:
	testq	%rbx, %rbx
	je	.LBB1_29
# BB#28:
	movq	%rbx, %rdi
	callq	free
.LBB1_29:
	xorl	%eax, %eax
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	massive_count
	movl	cdata+32(%rip), %eax
	cmpl	$1, %eax
	jne	.LBB1_45
# BB#30:
	movl	cube(%rip), %esi
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rcx
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_addset
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	movl	cube(%rip), %esi
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	jmp	.LBB1_33
.LBB1_31:
	movl	cube(%rip), %esi
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, (%rbx)
	movl	cube(%rip), %esi
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rcx
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_addset
	jmp	.LBB1_33
.LBB1_32:
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r13, %rsi
	movq	%rcx, %rdx
	callq	set_or
	movl	cube(%rip), %esi
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r13, %rsi
	callq	sf_addset
	movq	%rax, (%rbx)
	movq	%r13, %rdi
	callq	compl_cube
.LBB1_33:
	movq	%rax, (%r14)
.LBB1_34:
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_36
# BB#35:
	callq	free
.LBB1_36:
	movq	%rbp, %rdi
.LBB1_37:                               # %simp_comp_special_cases.exit.thread
	callq	free
.LBB1_38:                               # %simp_comp_special_cases.exit.thread
	testb	$1, debug(%rip)
	je	.LBB1_40
# BB#39:
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movl	simp_comp.simplify_level(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	callq	debug1_print
	movq	(%r14), %rdi
	movl	simp_comp.simplify_level(%rip), %edx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	callq	debug1_print
	decl	simp_comp.simplify_level(%rip)
.LBB1_40:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_41:
	movl	cube(%rip), %esi
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rcx
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_addset
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	movl	cube(%rip), %esi
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, (%r14)
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_67
# BB#42:
	callq	free
	jmp	.LBB1_67
.LBB1_43:
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jge	.LBB1_47
# BB#44:
	movl	$8, %edi
	jmp	.LBB1_48
.LBB1_45:
	cmpl	%eax, cdata+36(%rip)
	jne	.LBB1_68
# BB#46:
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	cubeunlist
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_contain
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	map_cover_to_unate
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	unate_compl
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	map_unate_to_cover
	movq	%rax, (%r14)
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_free
	jmp	.LBB1_34
.LBB1_47:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB1_48:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %rbp
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	set_diff
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	set_or
	testq	%rbp, %rbp
	je	.LBB1_50
# BB#49:
	movq	%rbp, %rdi
	callq	free
.LBB1_50:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	simp_comp
	movq	(%rbp), %r15
	movslq	(%r15), %rcx
	movslq	12(%r15), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB1_66
# BB#51:                                # %.lr.ph.preheader
	movq	24(%r15), %rcx
	leaq	(%rcx,%rax,4), %r14
	leaq	-4(%rbx), %r10
	leaq	4(%rbx), %r11
	leaq	-12(%rbx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$1, %r12d
	movl	$2, %r8d
	.p2align	4, 0x90
.LBB1_52:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_60 Depth 2
                                        #     Child Loop BB1_64 Depth 2
	movl	(%rcx), %eax
	movl	%eax, %esi
	andl	$1023, %esi             # imm = 0x3FF
	testw	$1023, %ax              # imm = 0x3FF
	movq	%rsi, %r9
	cmoveq	%r12, %r9
	cmpq	$8, %r9
	jb	.LBB1_63
# BB#53:                                # %min.iters.checked85
                                        #   in Loop: Header=BB1_52 Depth=1
	movq	%r9, %r13
	andq	$1016, %r13             # imm = 0x3F8
	je	.LBB1_63
# BB#54:                                # %vector.memcheck107
                                        #   in Loop: Header=BB1_52 Depth=1
	leaq	1(%rsi), %rdx
	testl	%esi, %esi
	cmovneq	%r8, %rdx
	leaq	-4(%rcx,%rdx,4), %rdi
	leaq	(%r11,%rsi,4), %rbp
	cmpq	%rbp, %rdi
	jae	.LBB1_56
# BB#55:                                # %vector.memcheck107
                                        #   in Loop: Header=BB1_52 Depth=1
	leaq	4(%rcx,%rsi,4), %rdi
	leaq	(%r10,%rdx,4), %rdx
	cmpq	%rdi, %rdx
	jb	.LBB1_63
.LBB1_56:                               # %vector.body80.preheader
                                        #   in Loop: Header=BB1_52 Depth=1
	leaq	-8(%r13), %rdx
	movq	%rdx, %rdi
	shrq	$3, %rdi
	btl	$3, %edx
	jb	.LBB1_58
# BB#57:                                # %vector.body80.prol
                                        #   in Loop: Header=BB1_52 Depth=1
	movups	-12(%rcx,%rsi,4), %xmm0
	movups	-28(%rcx,%rsi,4), %xmm1
	movups	-12(%rbx,%rsi,4), %xmm2
	movups	-28(%rbx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rcx,%rsi,4)
	movups	%xmm3, -28(%rcx,%rsi,4)
	movl	$8, %edx
	testq	%rdi, %rdi
	jne	.LBB1_59
	jmp	.LBB1_61
.LBB1_58:                               #   in Loop: Header=BB1_52 Depth=1
	xorl	%edx, %edx
	testq	%rdi, %rdi
	je	.LBB1_61
.LBB1_59:                               # %vector.body80.preheader.new
                                        #   in Loop: Header=BB1_52 Depth=1
	movq	%r9, %rdi
	andq	$-8, %rdi
	negq	%rdi
	negq	%rdx
	leaq	-12(%rcx,%rsi,4), %rbp
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,4), %r8
	.p2align	4, 0x90
.LBB1_60:                               # %vector.body80
                                        #   Parent Loop BB1_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbp,%rdx,4), %xmm0
	movups	-16(%rbp,%rdx,4), %xmm1
	movups	(%r8,%rdx,4), %xmm2
	movups	-16(%r8,%rdx,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rbp,%rdx,4)
	movups	%xmm3, -16(%rbp,%rdx,4)
	movups	-32(%rbp,%rdx,4), %xmm0
	movups	-48(%rbp,%rdx,4), %xmm1
	movups	-32(%r8,%rdx,4), %xmm2
	movups	-48(%r8,%rdx,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rbp,%rdx,4)
	movups	%xmm3, -48(%rbp,%rdx,4)
	addq	$-16, %rdx
	cmpq	%rdx, %rdi
	jne	.LBB1_60
.LBB1_61:                               # %middle.block81
                                        #   in Loop: Header=BB1_52 Depth=1
	cmpq	%r13, %r9
	movl	$2, %r8d
	je	.LBB1_65
# BB#62:                                #   in Loop: Header=BB1_52 Depth=1
	subq	%r13, %rsi
	.p2align	4, 0x90
.LBB1_63:                               # %scalar.ph82.preheader
                                        #   in Loop: Header=BB1_52 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB1_64:                               # %scalar.ph82
                                        #   Parent Loop BB1_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rbx,%rsi,4), %eax
	andl	%eax, -4(%rcx,%rsi,4)
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB1_64
.LBB1_65:                               # %.loopexit129
                                        #   in Loop: Header=BB1_52 Depth=1
	movslq	(%r15), %rax
	leaq	(%rcx,%rax,4), %rcx
	cmpq	%r14, %rcx
	jb	.LBB1_52
.LBB1_66:                               # %._crit_edge
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	(%r14), %rbp
	movq	%rbx, %rdi
	callq	compl_cube
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rsi
	callq	sf_append
	movq	%rax, (%r14)
	testq	%rbx, %rbx
	je	.LBB1_38
.LBB1_67:
	movq	%rbx, %rdi
	jmp	.LBB1_37
.LBB1_68:                               # %simp_comp_special_cases.exit
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	jge	.LBB1_70
# BB#69:
	movl	$8, %edi
	jmp	.LBB1_71
.LBB1_70:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB1_71:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, %r13
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	jge	.LBB1_73
# BB#72:
	movl	$8, %edi
	jmp	.LBB1_74
.LBB1_73:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB1_74:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, %r15
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	binate_split_select
	movl	%eax, %ebp
	xorl	%eax, %eax
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	movl	%ebp, %edx
	callq	scofactor
	leaq	80(%rsp), %rsi
	leaq	64(%rsp), %rdx
	movq	%rax, %rdi
	callq	simp_comp
	xorl	%eax, %eax
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r15, %rsi
	movl	%ebp, %edx
	callq	scofactor
	leaq	72(%rsp), %rsi
	leaq	56(%rsp), %rdx
	movq	%rax, %rdi
	callq	simp_comp
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdx
	movl	$0, (%rsp)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rcx
	movq	%r15, %r8
	movl	%ebp, %r9d
	callq	compl_merge
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%rax, (%rbx)
	movq	64(%rsp), %rsi
	movq	56(%rsp), %rdx
	movl	$0, (%rsp)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rcx
	movq	%r15, %r8
	movl	%ebp, %r9d
	movq	16(%rsp), %rbp          # 8-byte Reload
	callq	compl_merge
	movq	%rax, (%r14)
	movq	(%rbx), %rdi
	movslq	12(%rdi), %rax
	movq	8(%rbp), %rcx
	subq	%rbp, %rcx
	sarq	$3, %rcx
	addq	$-3, %rcx
	cmpq	%rcx, %rax
	jle	.LBB1_76
# BB#75:
	xorl	%eax, %eax
	callq	sf_free
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	cubeunlist
	movq	%rax, (%rbx)
.LBB1_76:
	testq	%r13, %r13
	je	.LBB1_78
# BB#77:
	movq	%r13, %rdi
	callq	free
.LBB1_78:
	testq	%r15, %r15
	je	.LBB1_34
# BB#79:
	movq	%r15, %rdi
	callq	free
	jmp	.LBB1_34
.Lfunc_end1:
	.size	simp_comp, .Lfunc_end1-simp_comp
	.cfi_endproc

	.globl	simplify
	.p2align	4, 0x90
	.type	simplify,@function
simplify:                               # @simplify
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 80
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	testb	$1, debug(%rip)
	je	.LBB2_2
# BB#1:
	movl	simplify.simplify_level(%rip), %edx
	leal	1(%rdx), %eax
	movl	%eax, simplify.simplify_level(%rip)
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	debug_print
.LBB2_2:
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_31
# BB#3:
	movq	(%rbp), %r15
	cmpq	$0, 24(%rbp)
	je	.LBB2_32
# BB#4:                                 # %.preheader.preheader
	leaq	24(%rbp), %r14
	testq	%rdi, %rdi
	je	.LBB2_8
# BB#5:
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph52
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	full_row
	testl	%eax, %eax
	jne	.LBB2_33
# BB#7:                                 # %..preheader_crit_edge
                                        #   in Loop: Header=BB2_6 Depth=1
	movq	(%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.LBB2_6
.LBB2_8:                                # %.preheader._crit_edge
	movl	(%r15), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	movl	$2, %ecx
	cmpl	$33, %eax
	jb	.LBB2_10
# BB#9:
	decl	%eax
	shrl	$5, %eax
	addl	$2, %eax
	movl	%eax, %ecx
.LBB2_10:
	movl	%ecx, %edi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r15, %rsi
	callq	set_copy
	movq	%rax, %rbx
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	16(%rbp), %rax
	testq	%rax, %rax
	je	.LBB2_26
# BB#11:                                # %.lr.ph47.preheader
	leaq	-4(%rbx), %r11
	leaq	4(%rbx), %r10
	leaq	-12(%rbx), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	$1, %r12d
	movl	$2, %ebp
	.p2align	4, 0x90
.LBB2_12:                               # %.lr.ph47
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_20 Depth 2
                                        #     Child Loop BB2_24 Depth 2
	movl	(%rbx), %ecx
	movl	%ecx, %esi
	andl	$1023, %esi             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movq	%rsi, %rcx
	cmoveq	%r12, %rcx
	cmpq	$8, %rcx
	jb	.LBB2_23
# BB#13:                                # %min.iters.checked
                                        #   in Loop: Header=BB2_12 Depth=1
	movq	%rcx, %r13
	andq	$1016, %r13             # imm = 0x3F8
	je	.LBB2_23
# BB#14:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_12 Depth=1
	leaq	1(%rsi), %rdx
	testl	%esi, %esi
	cmovneq	%rbp, %rdx
	leaq	(%r11,%rdx,4), %r8
	leaq	4(%rax,%rsi,4), %rdi
	cmpq	%rdi, %r8
	jae	.LBB2_16
# BB#15:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_12 Depth=1
	leaq	(%r10,%rsi,4), %rdi
	leaq	-4(%rax,%rdx,4), %rdx
	cmpq	%rdi, %rdx
	jb	.LBB2_23
.LBB2_16:                               # %vector.body.preheader
                                        #   in Loop: Header=BB2_12 Depth=1
	leaq	-8(%r13), %rdi
	movq	%rdi, %rdx
	shrq	$3, %rdx
	btl	$3, %edi
	jb	.LBB2_18
# BB#17:                                # %vector.body.prol
                                        #   in Loop: Header=BB2_12 Depth=1
	movups	-12(%rbx,%rsi,4), %xmm0
	movups	-28(%rbx,%rsi,4), %xmm1
	movups	-12(%rax,%rsi,4), %xmm2
	movups	-28(%rax,%rsi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rsi,4)
	movups	%xmm3, -28(%rbx,%rsi,4)
	movl	$8, %edi
	testq	%rdx, %rdx
	jne	.LBB2_19
	jmp	.LBB2_21
.LBB2_18:                               #   in Loop: Header=BB2_12 Depth=1
	xorl	%edi, %edi
	testq	%rdx, %rdx
	je	.LBB2_21
.LBB2_19:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB2_12 Depth=1
	movq	%rcx, %rdx
	andq	$-8, %rdx
	negq	%rdx
	negq	%rdi
	movq	16(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rsi,4), %r8
	movl	$2, %ebp
	leaq	-12(%rax,%rsi,4), %r9
	.p2align	4, 0x90
.LBB2_20:                               # %vector.body
                                        #   Parent Loop BB2_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r8,%rdi,4), %xmm0
	movups	-16(%r8,%rdi,4), %xmm1
	movups	(%r9,%rdi,4), %xmm2
	movups	-16(%r9,%rdi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%r8,%rdi,4)
	movups	%xmm3, -16(%r8,%rdi,4)
	movups	-32(%r8,%rdi,4), %xmm0
	movups	-48(%r8,%rdi,4), %xmm1
	movups	-32(%r9,%rdi,4), %xmm2
	movups	-48(%r9,%rdi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%r8,%rdi,4)
	movups	%xmm3, -48(%r8,%rdi,4)
	addq	$-16, %rdi
	cmpq	%rdi, %rdx
	jne	.LBB2_20
.LBB2_21:                               # %middle.block
                                        #   in Loop: Header=BB2_12 Depth=1
	cmpq	%r13, %rcx
	je	.LBB2_25
# BB#22:                                #   in Loop: Header=BB2_12 Depth=1
	subq	%r13, %rsi
	.p2align	4, 0x90
.LBB2_23:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB2_12 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB2_24:                               # %scalar.ph
                                        #   Parent Loop BB2_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rax,%rsi,4), %ecx
	orl	%ecx, -4(%rbx,%rsi,4)
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB2_24
.LBB2_25:                               # %.loopexit
                                        #   in Loop: Header=BB2_12 Depth=1
	movq	(%r14), %rax
	addq	$8, %r14
	testq	%rax, %rax
	jne	.LBB2_12
.LBB2_26:                               # %._crit_edge48
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	setp_equal
	testl	%eax, %eax
	je	.LBB2_38
# BB#27:
	testq	%rbx, %rbx
	je	.LBB2_29
# BB#28:
	movq	%rbx, %rdi
	callq	free
.LBB2_29:
	xorl	%eax, %eax
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rdi
	callq	massive_count
	movl	cdata+32(%rip), %eax
	cmpl	$1, %eax
	jne	.LBB2_40
# BB#30:
	movl	cube(%rip), %esi
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rcx
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_addset
	movq	%rax, %r15
	jmp	.LBB2_75
.LBB2_31:
	movl	cube(%rip), %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	sf_new
	jmp	.LBB2_35
.LBB2_32:
	movl	cube(%rip), %esi
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rbx
	movq	16(%rbp), %rdx
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r15, %rsi
	callq	set_or
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rsi
	jmp	.LBB2_34
.LBB2_33:
	movl	cube(%rip), %esi
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rcx
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rcx, %rdi
.LBB2_34:
	callq	sf_addset
.LBB2_35:
	movq	%rax, %r15
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_37
# BB#36:
	callq	free
.LBB2_37:
	movq	%rbp, %rdi
	jmp	.LBB2_78
.LBB2_38:
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jge	.LBB2_42
# BB#39:
	movl	$8, %edi
	jmp	.LBB2_43
.LBB2_40:
	cmpl	%eax, cdata+36(%rip)
	jne	.LBB2_61
# BB#41:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	cubeunlist
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_contain
	movq	%rax, %r15
	jmp	.LBB2_75
.LBB2_42:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB2_43:
	movq	8(%rsp), %r14           # 8-byte Reload
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %rbp
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	set_diff
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	set_or
	testq	%rbp, %rbp
	je	.LBB2_45
# BB#44:
	movq	%rbp, %rdi
	callq	free
.LBB2_45:
	movq	%r14, %rdi
	callq	simplify
	movq	%rax, %r15
	movslq	(%r15), %rax
	movslq	12(%r15), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	jle	.LBB2_63
# BB#46:                                # %.lr.ph.preheader
	movq	24(%r15), %rax
	leaq	(%rax,%rcx,4), %r12
	leaq	-4(%rbx), %r10
	leaq	4(%rbx), %r11
	movq	%rbx, %r9
	addq	$-12, %r9
	movl	$1, %r14d
	movl	$2, %r8d
	.p2align	4, 0x90
.LBB2_47:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_55 Depth 2
                                        #     Child Loop BB2_59 Depth 2
	movl	(%rax), %ecx
	movl	%ecx, %edi
	andl	$1023, %edi             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movq	%rdi, %rcx
	cmoveq	%r14, %rcx
	cmpq	$8, %rcx
	jb	.LBB2_58
# BB#48:                                # %min.iters.checked79
                                        #   in Loop: Header=BB2_47 Depth=1
	movq	%rcx, %r13
	andq	$1016, %r13             # imm = 0x3F8
	je	.LBB2_58
# BB#49:                                # %vector.memcheck101
                                        #   in Loop: Header=BB2_47 Depth=1
	leaq	1(%rdi), %rdx
	testl	%edi, %edi
	cmovneq	%r8, %rdx
	leaq	-4(%rax,%rdx,4), %rsi
	leaq	(%r11,%rdi,4), %rbp
	cmpq	%rbp, %rsi
	jae	.LBB2_51
# BB#50:                                # %vector.memcheck101
                                        #   in Loop: Header=BB2_47 Depth=1
	leaq	4(%rax,%rdi,4), %rsi
	leaq	(%r10,%rdx,4), %rdx
	cmpq	%rsi, %rdx
	jb	.LBB2_58
.LBB2_51:                               # %vector.body74.preheader
                                        #   in Loop: Header=BB2_47 Depth=1
	leaq	-8(%r13), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	btl	$3, %esi
	jb	.LBB2_53
# BB#52:                                # %vector.body74.prol
                                        #   in Loop: Header=BB2_47 Depth=1
	movups	-12(%rax,%rdi,4), %xmm0
	movups	-28(%rax,%rdi,4), %xmm1
	movups	-12(%rbx,%rdi,4), %xmm2
	movups	-28(%rbx,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rax,%rdi,4)
	movups	%xmm3, -28(%rax,%rdi,4)
	movl	$8, %ebp
	testq	%rdx, %rdx
	jne	.LBB2_54
	jmp	.LBB2_56
.LBB2_53:                               #   in Loop: Header=BB2_47 Depth=1
	xorl	%ebp, %ebp
	testq	%rdx, %rdx
	je	.LBB2_56
.LBB2_54:                               # %vector.body74.preheader.new
                                        #   in Loop: Header=BB2_47 Depth=1
	movq	%rcx, %rdx
	andq	$-8, %rdx
	negq	%rdx
	negq	%rbp
	leaq	-12(%rax,%rdi,4), %rsi
	leaq	(%r9,%rdi,4), %r8
	.p2align	4, 0x90
.LBB2_55:                               # %vector.body74
                                        #   Parent Loop BB2_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rsi,%rbp,4), %xmm0
	movups	-16(%rsi,%rbp,4), %xmm1
	movups	(%r8,%rbp,4), %xmm2
	movups	-16(%r8,%rbp,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rsi,%rbp,4)
	movups	%xmm3, -16(%rsi,%rbp,4)
	movups	-32(%rsi,%rbp,4), %xmm0
	movups	-48(%rsi,%rbp,4), %xmm1
	movups	-32(%r8,%rbp,4), %xmm2
	movups	-48(%r8,%rbp,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rsi,%rbp,4)
	movups	%xmm3, -48(%rsi,%rbp,4)
	addq	$-16, %rbp
	cmpq	%rbp, %rdx
	jne	.LBB2_55
.LBB2_56:                               # %middle.block75
                                        #   in Loop: Header=BB2_47 Depth=1
	cmpq	%r13, %rcx
	movl	$2, %r8d
	je	.LBB2_60
# BB#57:                                #   in Loop: Header=BB2_47 Depth=1
	subq	%r13, %rdi
	.p2align	4, 0x90
.LBB2_58:                               # %scalar.ph76.preheader
                                        #   in Loop: Header=BB2_47 Depth=1
	incq	%rdi
	.p2align	4, 0x90
.LBB2_59:                               # %scalar.ph76
                                        #   Parent Loop BB2_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rbx,%rdi,4), %ecx
	andl	%ecx, -4(%rax,%rdi,4)
	decq	%rdi
	cmpq	$1, %rdi
	jg	.LBB2_59
.LBB2_60:                               # %.loopexit123
                                        #   in Loop: Header=BB2_47 Depth=1
	movslq	(%r15), %rcx
	leaq	(%rax,%rcx,4), %rax
	cmpq	%r12, %rax
	jb	.LBB2_47
	jmp	.LBB2_77
.LBB2_61:                               # %simplify_special_cases.exit
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	jge	.LBB2_64
# BB#62:
	movl	$8, %edi
	jmp	.LBB2_65
.LBB2_63:                               # %._crit_edge
	testq	%rbx, %rbx
	jne	.LBB2_77
	jmp	.LBB2_79
.LBB2_64:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB2_65:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, %r12
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	jge	.LBB2_67
# BB#66:
	movl	$8, %edi
	jmp	.LBB2_68
.LBB2_67:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB2_68:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, %r13
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	binate_split_select
	movl	%eax, %ebp
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	%ebp, %edx
	callq	scofactor
	movq	%rax, %rdi
	callq	simplify
	movq	%rax, %r14
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	%ebp, %edx
	callq	scofactor
	movq	%rax, %rdi
	callq	simplify
	movl	$0, (%rsp)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	movq	%r12, %rcx
	movq	%r13, %r8
	movl	%ebp, %r9d
	callq	compl_merge
	movq	%rax, %r15
	movslq	12(%r15), %rax
	movq	8(%rbx), %rcx
	subq	%rbx, %rcx
	sarq	$3, %rcx
	addq	$-3, %rcx
	cmpq	%rcx, %rax
	jle	.LBB2_70
# BB#69:
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	cubeunlist
	movq	%rax, %r15
.LBB2_70:
	testq	%r12, %r12
	je	.LBB2_72
# BB#71:
	movq	%r12, %rdi
	callq	free
.LBB2_72:
	testq	%r13, %r13
	je	.LBB2_74
# BB#73:
	movq	%r13, %rdi
	callq	free
.LBB2_74:
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB2_75:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_77
# BB#76:
	callq	free
.LBB2_77:
	movq	%rbx, %rdi
.LBB2_78:                               # %simplify_special_cases.exit.thread
	callq	free
.LBB2_79:                               # %simplify_special_cases.exit.thread
	testb	$1, debug(%rip)
	je	.LBB2_81
# BB#80:
	movl	simplify.simplify_level(%rip), %edx
	decl	%edx
	movl	%edx, simplify.simplify_level(%rip)
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	debug1_print
.LBB2_81:
	movq	%r15, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	simplify, .Lfunc_end2-simplify
	.cfi_endproc

	.p2align	4, 0x90
	.type	compl_cube,@function
compl_cube:                             # @compl_cube
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 128
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	cube+80(%rip), %rax
	movq	56(%rax), %r15
	movq	cube+88(%rip), %r12
	movl	cube+4(%rip), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r14
	movl	(%r12), %eax
	movl	%eax, %ecx
	notl	%ecx
	movl	$-1024, %edx            # imm = 0xFC00
	andl	(%r15), %edx
	orq	$-1024, %rcx            # imm = 0xFC00
	andl	$1023, %eax             # imm = 0x3FF
	movq	$-2, %rsi
	cmoveq	%rcx, %rsi
	orl	%eax, %edx
	movl	%edx, (%r15)
	leaq	2(%rsi,%rax), %r9
	cmpq	$8, %r9
	jb	.LBB3_11
# BB#1:                                 # %min.iters.checked
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB3_11
# BB#2:                                 # %vector.memcheck
	testl	%eax, %eax
	movl	$1, %ecx
	cmoveq	%rax, %rcx
	leaq	(%r15,%rcx,4), %rdi
	leaq	4(%r15,%rax,4), %rsi
	leaq	(%r12,%rcx,4), %rdx
	leaq	4(%r12,%rax,4), %rbp
	leaq	(%rbx,%rcx,4), %r10
	leaq	4(%rbx,%rax,4), %r11
	cmpq	%rbp, %rdi
	sbbb	%cl, %cl
	cmpq	%rsi, %rdx
	sbbb	%dl, %dl
	andb	%cl, %dl
	cmpq	%r11, %rdi
	sbbb	%cl, %cl
	cmpq	%rsi, %r10
	sbbb	%sil, %sil
	testb	$1, %dl
	jne	.LBB3_11
# BB#3:                                 # %vector.memcheck
	andb	%sil, %cl
	andb	$1, %cl
	jne	.LBB3_11
# BB#4:                                 # %vector.body.preheader
	leaq	-8(%r8), %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB3_6
# BB#5:                                 # %vector.body.prol
	movups	-12(%r12,%rax,4), %xmm0
	movups	-28(%r12,%rax,4), %xmm1
	movups	-12(%rbx,%rax,4), %xmm2
	movups	-28(%rbx,%rax,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -12(%r15,%rax,4)
	movups	%xmm3, -28(%r15,%rax,4)
	movl	$8, %esi
	testq	%rcx, %rcx
	jne	.LBB3_7
	jmp	.LBB3_9
.LBB3_6:
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.LBB3_9
.LBB3_7:                                # %vector.body.preheader.new
	movq	%r8, %rdi
	negq	%rdi
	negq	%rsi
	leaq	-12(%r12,%rax,4), %rbp
	leaq	-12(%rbx,%rax,4), %rdx
	leaq	-12(%r15,%rax,4), %rcx
	.p2align	4, 0x90
.LBB3_8:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbp,%rsi,4), %xmm0
	movups	-16(%rbp,%rsi,4), %xmm1
	movups	(%rdx,%rsi,4), %xmm2
	movups	-16(%rdx,%rsi,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, (%rcx,%rsi,4)
	movups	%xmm3, -16(%rcx,%rsi,4)
	movups	-32(%rbp,%rsi,4), %xmm0
	movups	-48(%rbp,%rsi,4), %xmm1
	movups	-32(%rdx,%rsi,4), %xmm2
	movups	-48(%rdx,%rsi,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -32(%rcx,%rsi,4)
	movups	%xmm3, -48(%rcx,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB3_8
.LBB3_9:                                # %middle.block
	cmpq	%r8, %r9
	je	.LBB3_13
# BB#10:
	subq	%r8, %rax
.LBB3_11:                               # %scalar.ph.preheader
	incq	%rax
	.p2align	4, 0x90
.LBB3_12:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rbx,%rax,4), %ecx
	notl	%ecx
	andl	-4(%r12,%rax,4), %ecx
	movl	%ecx, -4(%r15,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB3_12
.LBB3_13:                               # %.preheader
	cmpl	$0, cube+4(%rip)
	jle	.LBB3_32
# BB#14:                                # %.lr.ph
	leaq	-4(%r15), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	4(%r15), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	-4(%r12), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	4(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	-12(%r15), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	-12(%r12), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$-1024, %ebp            # imm = 0xFC00
	xorl	%r13d, %r13d
	movq	%r14, (%rsp)            # 8-byte Spill
	jmp	.LBB3_15
.LBB3_27:                               #   in Loop: Header=BB3_15 Depth=1
	movq	%rdx, %rdi
	movq	(%rsp), %r14            # 8-byte Reload
	movl	$-1024, %ebp            # imm = 0xFC00
	jmp	.LBB3_29
	.p2align	4, 0x90
.LBB3_15:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_23 Depth 2
                                        #     Child Loop BB3_30 Depth 2
	movq	cube+72(%rip), %rax
	movq	(%rax,%r13,8), %rbx
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	setp_disjoint
	testl	%eax, %eax
	jne	.LBB3_31
# BB#16:                                #   in Loop: Header=BB3_15 Depth=1
	movq	24(%r14), %r10
	movl	12(%r14), %eax
	leal	1(%rax), %ecx
	imull	(%r14), %eax
	movl	%ecx, 12(%r14)
	movslq	%eax, %r11
	movl	(%r15), %edx
	movl	(%r10,%r11,4), %eax
	andl	%ebp, %eax
	andl	$1023, %edx             # imm = 0x3FF
	movq	%rdx, %r8
	movl	$1, %ecx
	cmoveq	%rcx, %r8
	movl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, (%r10,%r11,4)
	cmpq	$8, %r8
	jb	.LBB3_28
# BB#18:                                # %min.iters.checked85
                                        #   in Loop: Header=BB3_15 Depth=1
	movq	%r8, %rax
	andq	$1016, %rax             # imm = 0x3F8
	je	.LBB3_28
# BB#19:                                # %vector.memcheck127
                                        #   in Loop: Header=BB3_15 Depth=1
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rdx, %rax
	notq	%rax
	testl	%edx, %edx
	movq	$-2, %rcx
	cmovneq	%rcx, %rax
	movq	%r11, %rcx
	subq	%rax, %rcx
	leaq	-4(%r10,%rcx,4), %rsi
	leaq	(%r11,%rdx), %r9
	shlq	$2, %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	subq	%rax, %rcx
	movq	%rbx, %rdi
	subq	%rax, %rdi
	movq	32(%rsp), %rbp          # 8-byte Reload
	subq	%rax, %rbp
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,4), %rax
	addq	$-4, %rdi
	cmpq	%rax, %rsi
	movq	%r9, 56(%rsp)           # 8-byte Spill
	leaq	4(%r10,%r9,4), %r14
	sbbb	%al, %al
	cmpq	%r14, %rcx
	sbbb	%r9b, %r9b
	andb	%al, %r9b
	leaq	4(%rbx,%rdx,4), %rax
	cmpq	%rax, %rsi
	sbbb	%cl, %cl
	cmpq	%r14, %rdi
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,4), %rax
	sbbb	%dil, %dil
	cmpq	%rax, %rsi
	sbbb	%al, %al
	cmpq	%r14, %rbp
	sbbb	%sil, %sil
	testb	$1, %r9b
	jne	.LBB3_27
# BB#20:                                # %vector.memcheck127
                                        #   in Loop: Header=BB3_15 Depth=1
	andb	%dil, %cl
	andb	$1, %cl
	movq	(%rsp), %r14            # 8-byte Reload
	movl	$-1024, %ebp            # imm = 0xFC00
	jne	.LBB3_28
# BB#21:                                # %vector.memcheck127
                                        #   in Loop: Header=BB3_15 Depth=1
	andb	%sil, %al
	andb	$1, %al
	jne	.LBB3_28
# BB#22:                                # %vector.body80.preheader
                                        #   in Loop: Header=BB3_15 Depth=1
	movq	%rdx, %rdi
	movq	64(%rsp), %r9           # 8-byte Reload
	subq	%r9, %rdi
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,4), %rsi
	leaq	-12(%rbx,%rdx,4), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%rdx,4), %rdx
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	-12(%r10,%rcx,4), %rcx
	movq	%r8, %rbp
	andq	$-8, %rbp
	negq	%rbp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_23:                               # %vector.body80
                                        #   Parent Loop BB3_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rsi,%r14,4), %xmm0
	movups	-16(%rsi,%r14,4), %xmm1
	movups	(%rax,%r14,4), %xmm2
	movups	-16(%rax,%r14,4), %xmm3
	andps	%xmm2, %xmm0
	andps	%xmm3, %xmm1
	movups	(%rdx,%r14,4), %xmm4
	movups	-16(%rdx,%r14,4), %xmm5
	andnps	%xmm4, %xmm2
	andnps	%xmm5, %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rcx,%r14,4)
	movups	%xmm3, -16(%rcx,%r14,4)
	addq	$-8, %r14
	cmpq	%r14, %rbp
	jne	.LBB3_23
# BB#24:                                # %middle.block81
                                        #   in Loop: Header=BB3_15 Depth=1
	cmpq	%r9, %r8
	movq	(%rsp), %r14            # 8-byte Reload
	movl	$-1024, %ebp            # imm = 0xFC00
	jne	.LBB3_29
	jmp	.LBB3_31
	.p2align	4, 0x90
.LBB3_28:                               #   in Loop: Header=BB3_15 Depth=1
	movq	%rdx, %rdi
.LBB3_29:                               # %scalar.ph82.preheader
                                        #   in Loop: Header=BB3_15 Depth=1
	leaq	(%r10,%r11,4), %rax
	incq	%rdi
	.p2align	4, 0x90
.LBB3_30:                               # %scalar.ph82
                                        #   Parent Loop BB3_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rbx,%rdi,4), %ecx
	movl	-4(%r15,%rdi,4), %edx
	andl	%ecx, %edx
	notl	%ecx
	andl	-4(%r12,%rdi,4), %ecx
	orl	%edx, %ecx
	movl	%ecx, -4(%rax,%rdi,4)
	decq	%rdi
	cmpq	$1, %rdi
	jg	.LBB3_30
.LBB3_31:                               # %.loopexit
                                        #   in Loop: Header=BB3_15 Depth=1
	incq	%r13
	movslq	cube+4(%rip), %rax
	cmpq	%rax, %r13
	jl	.LBB3_15
.LBB3_32:                               # %._crit_edge
	movq	%r14, %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	compl_cube, .Lfunc_end3-compl_cube
	.cfi_endproc

	.p2align	4, 0x90
	.type	compl_merge,@function
compl_merge:                            # @compl_merge
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi58:
	.cfi_def_cfa_offset 144
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movl	%r9d, 40(%rsp)          # 4-byte Spill
	movq	%r8, %r15
	movq	%rcx, %r12
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	testb	$1, debug(%rip)
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	je	.LBB4_2
# BB#1:
	movl	12(%rbp), %esi
	movl	12(%rbx), %edx
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	pc1
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	pc2
	movq	%rax, %rcx
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%rcx, %rdx
	callq	printf
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	cprint
	movl	$.Lstr, %edi
	callq	puts
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	cprint
.LBB4_2:                                # %._crit_edge200
	movslq	(%rbp), %rax
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movslq	12(%rbp), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	jle	.LBB4_20
# BB#3:                                 # %.lr.ph184.preheader
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rax
	leaq	(%rax,%rcx,4), %r13
	leaq	-4(%r12), %r10
	leaq	4(%r12), %r11
	leaq	-12(%r12), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	$1, %r14d
	movl	$2, %edx
	.p2align	4, 0x90
.LBB4_4:                                # %.lr.ph184
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_14 Depth 2
                                        #     Child Loop BB4_18 Depth 2
	movl	(%rax), %ecx
	movl	%ecx, %edi
	andl	$1023, %edi             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movq	%rdi, %rbp
	cmoveq	%r14, %rbp
	cmpq	$8, %rbp
	jb	.LBB4_17
# BB#5:                                 # %min.iters.checked
                                        #   in Loop: Header=BB4_4 Depth=1
	movq	%rbp, %r9
	andq	$1016, %r9              # imm = 0x3F8
	je	.LBB4_17
# BB#6:                                 # %vector.memcheck
                                        #   in Loop: Header=BB4_4 Depth=1
	leaq	1(%rdi), %rcx
	testl	%edi, %edi
	cmovneq	%rdx, %rcx
	leaq	-4(%rax,%rcx,4), %rsi
	leaq	(%r11,%rdi,4), %rbx
	cmpq	%rbx, %rsi
	jae	.LBB4_9
# BB#7:                                 # %vector.memcheck
                                        #   in Loop: Header=BB4_4 Depth=1
	leaq	4(%rax,%rdi,4), %rsi
	leaq	(%r10,%rcx,4), %rcx
	cmpq	%rsi, %rcx
	jae	.LBB4_9
# BB#8:                                 #   in Loop: Header=BB4_4 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB4_17
.LBB4_9:                                # %vector.body.preheader
                                        #   in Loop: Header=BB4_4 Depth=1
	leaq	-8(%r9), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	btl	$3, %esi
	jb	.LBB4_10
# BB#11:                                # %vector.body.prol
                                        #   in Loop: Header=BB4_4 Depth=1
	movups	-12(%rax,%rdi,4), %xmm0
	movups	-28(%rax,%rdi,4), %xmm1
	movups	-12(%r12,%rdi,4), %xmm2
	movups	-28(%r12,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rax,%rdi,4)
	movups	%xmm3, -28(%rax,%rdi,4)
	movl	$8, %esi
	testq	%rcx, %rcx
	jne	.LBB4_13
	jmp	.LBB4_15
.LBB4_10:                               #   in Loop: Header=BB4_4 Depth=1
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.LBB4_15
.LBB4_13:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB4_4 Depth=1
	movq	%rbp, %rbx
	andq	$-8, %rbx
	negq	%rbx
	negq	%rsi
	leaq	-12(%rax,%rdi,4), %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	(%rdx,%rdi,4), %r8
	movl	$2, %edx
	.p2align	4, 0x90
.LBB4_14:                               # %vector.body
                                        #   Parent Loop BB4_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rcx,%rsi,4), %xmm0
	movups	-16(%rcx,%rsi,4), %xmm1
	movups	(%r8,%rsi,4), %xmm2
	movups	-16(%r8,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rcx,%rsi,4)
	movups	%xmm3, -16(%rcx,%rsi,4)
	movups	-32(%rcx,%rsi,4), %xmm0
	movups	-48(%rcx,%rsi,4), %xmm1
	movups	-32(%r8,%rsi,4), %xmm2
	movups	-48(%r8,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rcx,%rsi,4)
	movups	%xmm3, -48(%rcx,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %rbx
	jne	.LBB4_14
.LBB4_15:                               # %middle.block
                                        #   in Loop: Header=BB4_4 Depth=1
	cmpq	%r9, %rbp
	movq	24(%rsp), %rbx          # 8-byte Reload
	je	.LBB4_19
# BB#16:                                #   in Loop: Header=BB4_4 Depth=1
	subq	%r9, %rdi
	.p2align	4, 0x90
.LBB4_17:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB4_4 Depth=1
	incq	%rdi
	.p2align	4, 0x90
.LBB4_18:                               # %scalar.ph
                                        #   Parent Loop BB4_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%r12,%rdi,4), %ecx
	andl	%ecx, -4(%rax,%rdi,4)
	decq	%rdi
	cmpq	$1, %rdi
	jg	.LBB4_18
.LBB4_19:                               # %.loopexit511
                                        #   in Loop: Header=BB4_4 Depth=1
	orb	$32, 1(%rax)
	movq	64(%rsp), %rcx          # 8-byte Reload
	movslq	(%rcx), %rcx
	leaq	(%rax,%rcx,4), %rax
	cmpq	%r13, %rax
	jb	.LBB4_4
.LBB4_20:                               # %._crit_edge185
	movslq	(%rbx), %rax
	movslq	12(%rbx), %rcx
	imulq	%rax, %rcx
	testl	%ecx, %ecx
	jle	.LBB4_38
# BB#21:                                # %.lr.ph180.preheader
	movq	24(%rbx), %rax
	leaq	(%rax,%rcx,4), %r13
	leaq	-4(%r15), %r10
	leaq	4(%r15), %r11
	leaq	-12(%r15), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	$1, %r14d
	movl	$2, %edx
	.p2align	4, 0x90
.LBB4_22:                               # %.lr.ph180
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_32 Depth 2
                                        #     Child Loop BB4_36 Depth 2
	movl	(%rax), %ecx
	movl	%ecx, %edi
	andl	$1023, %edi             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movq	%rdi, %rbp
	cmoveq	%r14, %rbp
	cmpq	$8, %rbp
	jb	.LBB4_35
# BB#23:                                # %min.iters.checked240
                                        #   in Loop: Header=BB4_22 Depth=1
	movq	%rbp, %r9
	andq	$1016, %r9              # imm = 0x3F8
	je	.LBB4_35
# BB#24:                                # %vector.memcheck262
                                        #   in Loop: Header=BB4_22 Depth=1
	leaq	1(%rdi), %rcx
	testl	%edi, %edi
	cmovneq	%rdx, %rcx
	leaq	-4(%rax,%rcx,4), %rsi
	leaq	(%r11,%rdi,4), %rbx
	cmpq	%rbx, %rsi
	jae	.LBB4_27
# BB#25:                                # %vector.memcheck262
                                        #   in Loop: Header=BB4_22 Depth=1
	leaq	4(%rax,%rdi,4), %rsi
	leaq	(%r10,%rcx,4), %rcx
	cmpq	%rsi, %rcx
	jae	.LBB4_27
# BB#26:                                #   in Loop: Header=BB4_22 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB4_35
.LBB4_27:                               # %vector.body235.preheader
                                        #   in Loop: Header=BB4_22 Depth=1
	leaq	-8(%r9), %rsi
	movq	%rsi, %rcx
	shrq	$3, %rcx
	btl	$3, %esi
	jb	.LBB4_28
# BB#29:                                # %vector.body235.prol
                                        #   in Loop: Header=BB4_22 Depth=1
	movups	-12(%rax,%rdi,4), %xmm0
	movups	-28(%rax,%rdi,4), %xmm1
	movups	-12(%r15,%rdi,4), %xmm2
	movups	-28(%r15,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rax,%rdi,4)
	movups	%xmm3, -28(%rax,%rdi,4)
	movl	$8, %esi
	testq	%rcx, %rcx
	jne	.LBB4_31
	jmp	.LBB4_33
.LBB4_28:                               #   in Loop: Header=BB4_22 Depth=1
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.LBB4_33
.LBB4_31:                               # %vector.body235.preheader.new
                                        #   in Loop: Header=BB4_22 Depth=1
	movq	%rbp, %rbx
	andq	$-8, %rbx
	negq	%rbx
	negq	%rsi
	leaq	-12(%rax,%rdi,4), %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	(%rdx,%rdi,4), %r8
	movl	$2, %edx
	.p2align	4, 0x90
.LBB4_32:                               # %vector.body235
                                        #   Parent Loop BB4_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rcx,%rsi,4), %xmm0
	movups	-16(%rcx,%rsi,4), %xmm1
	movups	(%r8,%rsi,4), %xmm2
	movups	-16(%r8,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rcx,%rsi,4)
	movups	%xmm3, -16(%rcx,%rsi,4)
	movups	-32(%rcx,%rsi,4), %xmm0
	movups	-48(%rcx,%rsi,4), %xmm1
	movups	-32(%r8,%rsi,4), %xmm2
	movups	-48(%r8,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rcx,%rsi,4)
	movups	%xmm3, -48(%rcx,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %rbx
	jne	.LBB4_32
.LBB4_33:                               # %middle.block236
                                        #   in Loop: Header=BB4_22 Depth=1
	cmpq	%r9, %rbp
	movq	24(%rsp), %rbx          # 8-byte Reload
	je	.LBB4_37
# BB#34:                                #   in Loop: Header=BB4_22 Depth=1
	subq	%r9, %rdi
	.p2align	4, 0x90
.LBB4_35:                               # %scalar.ph237.preheader
                                        #   in Loop: Header=BB4_22 Depth=1
	incq	%rdi
	.p2align	4, 0x90
.LBB4_36:                               # %scalar.ph237
                                        #   Parent Loop BB4_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%r15,%rdi,4), %ecx
	andl	%ecx, -4(%rax,%rdi,4)
	decq	%rdi
	cmpq	$1, %rdi
	jg	.LBB4_36
.LBB4_37:                               # %.loopexit510
                                        #   in Loop: Header=BB4_22 Depth=1
	orb	$32, 1(%rax)
	movslq	(%rbx), %rcx
	leaq	(%rax,%rcx,4), %rax
	cmpq	%r13, %rax
	jb	.LBB4_22
.LBB4_38:                               # %._crit_edge181
	movq	cube+80(%rip), %rax
	movq	(%rax), %rdi
	movq	cube+72(%rip), %rax
	movslq	40(%rsp), %rcx          # 4-byte Folded Reload
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %rsi
	xorl	%eax, %eax
	callq	set_copy
	xorl	%eax, %eax
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	sf_list
	movq	%rax, %r14
	movslq	12(%rbp), %rsi
	movl	$8, %edx
	movl	$d1_order, %ecx
	movq	%r14, %rdi
	callq	qsort
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_list
	movslq	12(%rbx), %rsi
	movl	$8, %edx
	movl	$d1_order, %ecx
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	callq	qsort
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.LBB4_62
# BB#39:                                # %._crit_edge181
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r14
	testq	%r14, %r14
	je	.LBB4_62
# BB#40:                                # %.outer29..outer29.split_crit_edge.us.preheader.i.preheader
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_41:                               # %.outer29..outer29.split_crit_edge.us.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_42 Depth 2
                                        #       Child Loop BB4_53 Depth 3
                                        #       Child Loop BB4_57 Depth 3
	leaq	-4(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	4(%r13), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	-12(%r13), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_42:                               #   Parent Loop BB4_41 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_53 Depth 3
                                        #       Child Loop BB4_57 Depth 3
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	d1_order
	cmpl	$-1, %eax
	je	.LBB4_60
# BB#43:                                #   in Loop: Header=BB4_42 Depth=2
	testl	%eax, %eax
	je	.LBB4_44
# BB#58:                                #   in Loop: Header=BB4_42 Depth=2
	cmpl	$1, %eax
	jne	.LBB4_42
	jmp	.LBB4_59
.LBB4_44:                               #   in Loop: Header=BB4_42 Depth=2
	andb	$-33, 1(%r14)
	movl	(%r13), %ecx
	movl	%ecx, %eax
	andl	$1023, %eax             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movq	%rax, %r9
	movl	$1, %ecx
	cmoveq	%rcx, %r9
	cmpq	$8, %r9
	jb	.LBB4_56
# BB#45:                                # %min.iters.checked289
                                        #   in Loop: Header=BB4_42 Depth=2
	movq	%r9, %r8
	andq	$1016, %r8              # imm = 0x3F8
	je	.LBB4_56
# BB#46:                                # %vector.memcheck311
                                        #   in Loop: Header=BB4_42 Depth=2
	leaq	1(%rax), %rcx
	testl	%eax, %eax
	movl	$2, %edx
	cmovneq	%rdx, %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	(%rdx,%rcx,4), %rdx
	leaq	4(%r14,%rax,4), %rsi
	cmpq	%rsi, %rdx
	jae	.LBB4_48
# BB#47:                                # %vector.memcheck311
                                        #   in Loop: Header=BB4_42 Depth=2
	movq	56(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,4), %rdx
	leaq	-4(%r14,%rcx,4), %rcx
	cmpq	%rdx, %rcx
	jb	.LBB4_56
.LBB4_48:                               # %vector.body284.preheader
                                        #   in Loop: Header=BB4_42 Depth=2
	leaq	-8(%r8), %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB4_49
# BB#50:                                # %vector.body284.prol
                                        #   in Loop: Header=BB4_42 Depth=2
	movups	-12(%r13,%rax,4), %xmm0
	movups	-28(%r13,%rax,4), %xmm1
	movups	-12(%r14,%rax,4), %xmm2
	movups	-28(%r14,%rax,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%r13,%rax,4)
	movups	%xmm3, -28(%r13,%rax,4)
	movl	$8, %esi
	testq	%rcx, %rcx
	jne	.LBB4_52
	jmp	.LBB4_54
.LBB4_49:                               #   in Loop: Header=BB4_42 Depth=2
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.LBB4_54
.LBB4_52:                               # %vector.body284.preheader.new
                                        #   in Loop: Header=BB4_42 Depth=2
	movq	%r9, %rdi
	andq	$-8, %rdi
	negq	%rdi
	negq	%rsi
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rdx
	leaq	-12(%r14,%rax,4), %rcx
	.p2align	4, 0x90
.LBB4_53:                               # %vector.body284
                                        #   Parent Loop BB4_41 Depth=1
                                        #     Parent Loop BB4_42 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	(%rdx,%rsi,4), %xmm0
	movups	-16(%rdx,%rsi,4), %xmm1
	movups	(%rcx,%rsi,4), %xmm2
	movups	-16(%rcx,%rsi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rdx,%rsi,4)
	movups	%xmm3, -16(%rdx,%rsi,4)
	movups	-32(%rdx,%rsi,4), %xmm0
	movups	-48(%rdx,%rsi,4), %xmm1
	movups	-32(%rcx,%rsi,4), %xmm2
	movups	-48(%rcx,%rsi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%rdx,%rsi,4)
	movups	%xmm3, -48(%rdx,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB4_53
.LBB4_54:                               # %middle.block285
                                        #   in Loop: Header=BB4_42 Depth=2
	cmpq	%r8, %r9
	je	.LBB4_59
# BB#55:                                #   in Loop: Header=BB4_42 Depth=2
	subq	%r8, %rax
.LBB4_56:                               # %scalar.ph286.preheader
                                        #   in Loop: Header=BB4_42 Depth=2
	incq	%rax
	.p2align	4, 0x90
.LBB4_57:                               # %scalar.ph286
                                        #   Parent Loop BB4_41 Depth=1
                                        #     Parent Loop BB4_42 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%r14,%rax,4), %ecx
	orl	%ecx, -4(%r13,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB4_57
.LBB4_59:                               # %.outer29.backedge.us.i
                                        #   in Loop: Header=BB4_42 Depth=2
	movq	8(%rbp), %r14
	addq	$8, %rbp
	testq	%r14, %r14
	jne	.LBB4_42
	jmp	.LBB4_62
	.p2align	4, 0x90
.LBB4_60:                               # %.outer.i
                                        #   in Loop: Header=BB4_41 Depth=1
	testq	%r14, %r14
	je	.LBB4_62
# BB#61:                                # %.outer.i
                                        #   in Loop: Header=BB4_41 Depth=1
	movq	8(%rbx), %r13
	addq	$8, %rbx
	testq	%r13, %r13
	jne	.LBB4_41
.LBB4_62:                               # %compl_d1merge.exit
	movl	144(%rsp), %eax
	testl	%eax, %eax
	je	.LBB4_68
# BB#63:                                # %compl_d1merge.exit
	cmpl	$2, %eax
	je	.LBB4_66
# BB#64:                                # %compl_d1merge.exit
	cmpl	$1, %eax
	jne	.LBB4_76
# BB#65:
	xorl	%eax, %eax
	movq	80(%rsp), %rdi          # 8-byte Reload
	callq	cubeunlist
	movq	%rax, %rbx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movl	40(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %ecx
	callq	compl_lift_onset
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r12, %rdx
	movl	%ebp, %ecx
	callq	compl_lift_onset
	jmp	.LBB4_67
.LBB4_68:
	movq	cube+80(%rip), %rax
	movq	32(%rax), %rbp
	movq	40(%rax), %r13
	movq	cube+72(%rip), %rax
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rdx
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	callq	set_and
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r14
	testq	%r14, %r14
	je	.LBB4_72
# BB#69:                                # %.lr.ph.i.preheader
	leaq	-4(%r13), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	4(%r13), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	-12(%r13), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_70:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_89 Depth 2
                                        #       Child Loop BB4_91 Depth 3
                                        #     Child Loop BB4_104 Depth 2
                                        #     Child Loop BB4_108 Depth 2
	addq	$8, %rbx
	testb	$32, 1(%r14)
	je	.LBB4_71
# BB#88:                                #   in Loop: Header=BB4_70 Depth=1
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	callq	set_merge
	movq	16(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_89:                               #   Parent Loop BB4_70 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_91 Depth 3
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB4_71
# BB#90:                                #   in Loop: Header=BB4_89 Depth=2
	addq	$8, %rax
	movl	(%rbp), %edx
	andl	$1023, %edx             # imm = 0x3FF
	.p2align	4, 0x90
.LBB4_91:                               #   Parent Loop BB4_70 Depth=1
                                        #     Parent Loop BB4_89 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rcx,%rdx,4), %esi
	notl	%esi
	testl	(%rbp,%rdx,4), %esi
	jne	.LBB4_93
# BB#92:                                #   in Loop: Header=BB4_91 Depth=3
	leaq	-1(%rdx), %rsi
	cmpq	$1, %rdx
	movq	%rsi, %rdx
	jg	.LBB4_91
	jmp	.LBB4_94
	.p2align	4, 0x90
.LBB4_93:                               # %._crit_edge757
                                        #   in Loop: Header=BB4_89 Depth=2
	movl	%edx, %esi
.LBB4_94:                               #   in Loop: Header=BB4_89 Depth=2
	testl	%esi, %esi
	jne	.LBB4_89
# BB#95:                                #   in Loop: Header=BB4_70 Depth=1
	movl	(%r14), %ecx
	movl	%ecx, %eax
	andl	$1023, %eax             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movq	%rax, %r9
	movl	$1, %ecx
	cmoveq	%rcx, %r9
	cmpq	$8, %r9
	jb	.LBB4_107
# BB#96:                                # %min.iters.checked338
                                        #   in Loop: Header=BB4_70 Depth=1
	movq	%r9, %r8
	andq	$1016, %r8              # imm = 0x3F8
	je	.LBB4_107
# BB#97:                                # %vector.memcheck360
                                        #   in Loop: Header=BB4_70 Depth=1
	leaq	1(%rax), %rcx
	testl	%eax, %eax
	movl	$2, %edx
	cmovneq	%rdx, %rcx
	leaq	-4(%r14,%rcx,4), %rdx
	movq	56(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rax,4), %rsi
	cmpq	%rsi, %rdx
	jae	.LBB4_99
# BB#98:                                # %vector.memcheck360
                                        #   in Loop: Header=BB4_70 Depth=1
	leaq	4(%r14,%rax,4), %rdx
	movq	48(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,4), %rcx
	cmpq	%rdx, %rcx
	jb	.LBB4_107
.LBB4_99:                               # %vector.body333.preheader
                                        #   in Loop: Header=BB4_70 Depth=1
	leaq	-8(%r8), %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB4_100
# BB#101:                               # %vector.body333.prol
                                        #   in Loop: Header=BB4_70 Depth=1
	movups	-12(%r14,%rax,4), %xmm0
	movups	-28(%r14,%rax,4), %xmm1
	movups	-12(%r13,%rax,4), %xmm2
	movups	-28(%r13,%rax,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%r14,%rax,4)
	movups	%xmm3, -28(%r14,%rax,4)
	movl	$8, %esi
	testq	%rcx, %rcx
	jne	.LBB4_103
	jmp	.LBB4_105
.LBB4_100:                              #   in Loop: Header=BB4_70 Depth=1
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.LBB4_105
.LBB4_103:                              # %vector.body333.preheader.new
                                        #   in Loop: Header=BB4_70 Depth=1
	movq	%r9, %rdi
	andq	$-8, %rdi
	negq	%rdi
	negq	%rsi
	leaq	-12(%r14,%rax,4), %rdx
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rcx
	.p2align	4, 0x90
.LBB4_104:                              # %vector.body333
                                        #   Parent Loop BB4_70 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdx,%rsi,4), %xmm0
	movups	-16(%rdx,%rsi,4), %xmm1
	movups	(%rcx,%rsi,4), %xmm2
	movups	-16(%rcx,%rsi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rdx,%rsi,4)
	movups	%xmm3, -16(%rdx,%rsi,4)
	movups	-32(%rdx,%rsi,4), %xmm0
	movups	-48(%rdx,%rsi,4), %xmm1
	movups	-32(%rcx,%rsi,4), %xmm2
	movups	-48(%rcx,%rsi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%rdx,%rsi,4)
	movups	%xmm3, -48(%rdx,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB4_104
.LBB4_105:                              # %middle.block334
                                        #   in Loop: Header=BB4_70 Depth=1
	cmpq	%r8, %r9
	je	.LBB4_71
# BB#106:                               #   in Loop: Header=BB4_70 Depth=1
	subq	%r8, %rax
	.p2align	4, 0x90
.LBB4_107:                              # %scalar.ph335.preheader
                                        #   in Loop: Header=BB4_70 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB4_108:                              # %scalar.ph335
                                        #   Parent Loop BB4_70 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%r13,%rax,4), %ecx
	orl	%ecx, -4(%r14,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB4_108
.LBB4_71:                               # %.backedge.i
                                        #   in Loop: Header=BB4_70 Depth=1
	movq	(%rbx), %r14
	testq	%r14, %r14
	jne	.LBB4_70
.LBB4_72:                               # %compl_lift.exit
	movq	cube+80(%rip), %rax
	movq	32(%rax), %rbx
	movq	40(%rax), %r14
	movq	cube+72(%rip), %rax
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %r13
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	set_and
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB4_76
# BB#73:                                # %.lr.ph.i155.preheader
	leaq	-4(%r14), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	4(%r14), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	-12(%r14), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	16(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_74:                               # %.lr.ph.i155
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_110 Depth 2
                                        #       Child Loop BB4_112 Depth 3
                                        #     Child Loop BB4_125 Depth 2
                                        #     Child Loop BB4_129 Depth 2
	addq	$8, %r15
	testb	$32, 1(%rbp)
	je	.LBB4_75
# BB#109:                               #   in Loop: Header=BB4_74 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	movq	%r13, %rcx
	callq	set_merge
	movq	32(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_110:                              #   Parent Loop BB4_74 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_112 Depth 3
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB4_75
# BB#111:                               #   in Loop: Header=BB4_110 Depth=2
	addq	$8, %rax
	movl	(%rbx), %edx
	andl	$1023, %edx             # imm = 0x3FF
	.p2align	4, 0x90
.LBB4_112:                              #   Parent Loop BB4_74 Depth=1
                                        #     Parent Loop BB4_110 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rcx,%rdx,4), %esi
	notl	%esi
	testl	(%rbx,%rdx,4), %esi
	jne	.LBB4_114
# BB#113:                               #   in Loop: Header=BB4_112 Depth=3
	leaq	-1(%rdx), %rsi
	cmpq	$1, %rdx
	movq	%rsi, %rdx
	jg	.LBB4_112
	jmp	.LBB4_115
	.p2align	4, 0x90
.LBB4_114:                              # %._crit_edge706
                                        #   in Loop: Header=BB4_110 Depth=2
	movl	%edx, %esi
.LBB4_115:                              #   in Loop: Header=BB4_110 Depth=2
	testl	%esi, %esi
	jne	.LBB4_110
# BB#116:                               #   in Loop: Header=BB4_74 Depth=1
	movl	(%rbp), %ecx
	movl	%ecx, %eax
	andl	$1023, %eax             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movq	%rax, %r9
	movl	$1, %ecx
	cmoveq	%rcx, %r9
	cmpq	$8, %r9
	jb	.LBB4_128
# BB#117:                               # %min.iters.checked387
                                        #   in Loop: Header=BB4_74 Depth=1
	movq	%r9, %r8
	andq	$1016, %r8              # imm = 0x3F8
	je	.LBB4_128
# BB#118:                               # %vector.memcheck409
                                        #   in Loop: Header=BB4_74 Depth=1
	leaq	1(%rax), %rcx
	testl	%eax, %eax
	movl	$2, %edx
	cmovneq	%rdx, %rcx
	leaq	-4(%rbp,%rcx,4), %rdx
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	(%rsi,%rax,4), %rsi
	cmpq	%rsi, %rdx
	jae	.LBB4_120
# BB#119:                               # %vector.memcheck409
                                        #   in Loop: Header=BB4_74 Depth=1
	leaq	4(%rbp,%rax,4), %rdx
	movq	56(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,4), %rcx
	cmpq	%rdx, %rcx
	jb	.LBB4_128
.LBB4_120:                              # %vector.body382.preheader
                                        #   in Loop: Header=BB4_74 Depth=1
	leaq	-8(%r8), %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB4_121
# BB#122:                               # %vector.body382.prol
                                        #   in Loop: Header=BB4_74 Depth=1
	movups	-12(%rbp,%rax,4), %xmm0
	movups	-28(%rbp,%rax,4), %xmm1
	movups	-12(%r14,%rax,4), %xmm2
	movups	-28(%r14,%rax,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbp,%rax,4)
	movups	%xmm3, -28(%rbp,%rax,4)
	movl	$8, %esi
	testq	%rcx, %rcx
	jne	.LBB4_124
	jmp	.LBB4_126
.LBB4_121:                              #   in Loop: Header=BB4_74 Depth=1
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.LBB4_126
.LBB4_124:                              # %vector.body382.preheader.new
                                        #   in Loop: Header=BB4_74 Depth=1
	movq	%r9, %rdi
	andq	$-8, %rdi
	negq	%rdi
	negq	%rsi
	leaq	-12(%rbp,%rax,4), %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rcx
	.p2align	4, 0x90
.LBB4_125:                              # %vector.body382
                                        #   Parent Loop BB4_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdx,%rsi,4), %xmm0
	movups	-16(%rdx,%rsi,4), %xmm1
	movups	(%rcx,%rsi,4), %xmm2
	movups	-16(%rcx,%rsi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rdx,%rsi,4)
	movups	%xmm3, -16(%rdx,%rsi,4)
	movups	-32(%rdx,%rsi,4), %xmm0
	movups	-48(%rdx,%rsi,4), %xmm1
	movups	-32(%rcx,%rsi,4), %xmm2
	movups	-48(%rcx,%rsi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%rdx,%rsi,4)
	movups	%xmm3, -48(%rdx,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB4_125
.LBB4_126:                              # %middle.block383
                                        #   in Loop: Header=BB4_74 Depth=1
	cmpq	%r8, %r9
	je	.LBB4_75
# BB#127:                               #   in Loop: Header=BB4_74 Depth=1
	subq	%r8, %rax
	.p2align	4, 0x90
.LBB4_128:                              # %scalar.ph384.preheader
                                        #   in Loop: Header=BB4_74 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB4_129:                              # %scalar.ph384
                                        #   Parent Loop BB4_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%r14,%rax,4), %ecx
	orl	%ecx, -4(%rbp,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB4_129
.LBB4_75:                               # %.backedge.i156
                                        #   in Loop: Header=BB4_74 Depth=1
	movq	(%r15), %rbp
	testq	%rbp, %rbp
	jne	.LBB4_74
	jmp	.LBB4_76
.LBB4_66:
	xorl	%eax, %eax
	movq	80(%rsp), %rdi          # 8-byte Reload
	callq	cubeunlist
	movq	%rax, %rbx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	movl	40(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %edx
	callq	compl_lift_onset_complex
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	movl	%ebp, %edx
	callq	compl_lift_onset_complex
.LBB4_67:                               # %compl_lift.exit164
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_free
.LBB4_76:                               # %compl_lift.exit164
	movq	32(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB4_78
# BB#77:
	callq	free
.LBB4_78:
	movq	16(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	64(%rsp), %r15          # 8-byte Reload
	je	.LBB4_80
# BB#79:
	callq	free
.LBB4_80:
	movl	12(%rbx), %edi
	addl	12(%r15), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r14
	movq	24(%r14), %rax
	movslq	(%r15), %rcx
	movslq	12(%r15), %rdx
	imulq	%rcx, %rdx
	testl	%edx, %edx
	jle	.LBB4_141
# BB#81:                                # %.lr.ph176
	movq	24(%r15), %rcx
	leaq	(%rcx,%rdx,4), %r8
	.p2align	4, 0x90
.LBB4_82:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_131 Depth 2
                                        #     Child Loop BB4_135 Depth 2
                                        #     Child Loop BB4_139 Depth 2
	movl	(%rcx), %esi
	andl	$1023, %esi             # imm = 0x3FF
	leaq	1(%rsi), %r11
	cmpq	$8, %r11
	jb	.LBB4_138
# BB#83:                                # %min.iters.checked435
                                        #   in Loop: Header=BB4_82 Depth=1
	movq	%r11, %r10
	andq	$2040, %r10             # imm = 0x7F8
	je	.LBB4_138
# BB#84:                                # %vector.memcheck452
                                        #   in Loop: Header=BB4_82 Depth=1
	leaq	4(%rcx,%rsi,4), %rdx
	cmpq	%rdx, %rax
	jae	.LBB4_86
# BB#85:                                # %vector.memcheck452
                                        #   in Loop: Header=BB4_82 Depth=1
	leaq	4(%rax,%rsi,4), %rdx
	cmpq	%rdx, %rcx
	jb	.LBB4_138
.LBB4_86:                               # %vector.body431.preheader
                                        #   in Loop: Header=BB4_82 Depth=1
	leaq	-8(%r10), %r9
	movl	%r9d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB4_87
# BB#130:                               # %vector.body431.prol.preheader
                                        #   in Loop: Header=BB4_82 Depth=1
	leaq	-12(%rax,%rsi,4), %rbx
	leaq	-12(%rcx,%rsi,4), %rbp
	negq	%rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_131:                              # %vector.body431.prol
                                        #   Parent Loop BB4_82 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbp,%rdx,4), %xmm0
	movups	-16(%rbp,%rdx,4), %xmm1
	movups	%xmm0, (%rbx,%rdx,4)
	movups	%xmm1, -16(%rbx,%rdx,4)
	addq	$-8, %rdx
	incq	%rdi
	jne	.LBB4_131
# BB#132:                               # %vector.body431.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB4_82 Depth=1
	negq	%rdx
	cmpq	$24, %r9
	jae	.LBB4_134
	jmp	.LBB4_136
.LBB4_87:                               #   in Loop: Header=BB4_82 Depth=1
	xorl	%edx, %edx
	cmpq	$24, %r9
	jb	.LBB4_136
.LBB4_134:                              # %vector.body431.preheader.new
                                        #   in Loop: Header=BB4_82 Depth=1
	movq	%r11, %rdi
	andq	$-8, %rdi
	negq	%rdi
	negq	%rdx
	leaq	-12(%rax,%rsi,4), %rbx
	leaq	-12(%rcx,%rsi,4), %rbp
	.p2align	4, 0x90
.LBB4_135:                              # %vector.body431
                                        #   Parent Loop BB4_82 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbp,%rdx,4), %xmm0
	movups	-16(%rbp,%rdx,4), %xmm1
	movups	%xmm0, (%rbx,%rdx,4)
	movups	%xmm1, -16(%rbx,%rdx,4)
	movups	-32(%rbp,%rdx,4), %xmm0
	movups	-48(%rbp,%rdx,4), %xmm1
	movups	%xmm0, -32(%rbx,%rdx,4)
	movups	%xmm1, -48(%rbx,%rdx,4)
	movups	-64(%rbp,%rdx,4), %xmm0
	movups	-80(%rbp,%rdx,4), %xmm1
	movups	%xmm0, -64(%rbx,%rdx,4)
	movups	%xmm1, -80(%rbx,%rdx,4)
	movups	-96(%rbp,%rdx,4), %xmm0
	movups	-112(%rbp,%rdx,4), %xmm1
	movups	%xmm0, -96(%rbx,%rdx,4)
	movups	%xmm1, -112(%rbx,%rdx,4)
	addq	$-32, %rdx
	cmpq	%rdx, %rdi
	jne	.LBB4_135
.LBB4_136:                              # %middle.block432
                                        #   in Loop: Header=BB4_82 Depth=1
	cmpq	%r10, %r11
	movq	24(%rsp), %rbx          # 8-byte Reload
	je	.LBB4_140
# BB#137:                               #   in Loop: Header=BB4_82 Depth=1
	subq	%r10, %rsi
	.p2align	4, 0x90
.LBB4_138:                              # %scalar.ph433.preheader
                                        #   in Loop: Header=BB4_82 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB4_139:                              # %scalar.ph433
                                        #   Parent Loop BB4_82 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rcx,%rsi,4), %edx
	movl	%edx, -4(%rax,%rsi,4)
	decq	%rsi
	jg	.LBB4_139
.LBB4_140:                              # %.loopexit509
                                        #   in Loop: Header=BB4_82 Depth=1
	incl	12(%r14)
	movslq	(%r14), %rdx
	leaq	(%rax,%rdx,4), %rax
	movslq	(%r15), %rdx
	leaq	(%rcx,%rdx,4), %rcx
	cmpq	%r8, %rcx
	jb	.LBB4_82
.LBB4_141:                              # %._crit_edge177
	movl	(%rbx), %esi
	movl	12(%rbx), %edx
	imull	%esi, %edx
	testl	%edx, %edx
	jle	.LBB4_162
# BB#142:                               # %.lr.ph
	movq	24(%rbx), %rcx
	movslq	%edx, %rdx
	leaq	(%rcx,%rdx,4), %r8
	.p2align	4, 0x90
.LBB4_143:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_151 Depth 2
                                        #     Child Loop BB4_155 Depth 2
                                        #     Child Loop BB4_159 Depth 2
	movl	(%rcx), %edx
	testb	$32, %dh
	je	.LBB4_161
# BB#144:                               #   in Loop: Header=BB4_143 Depth=1
	andl	$1023, %edx             # imm = 0x3FF
	leaq	1(%rdx), %r11
	cmpq	$8, %r11
	jb	.LBB4_158
# BB#145:                               # %min.iters.checked474
                                        #   in Loop: Header=BB4_143 Depth=1
	movq	%r11, %r10
	andq	$2040, %r10             # imm = 0x7F8
	je	.LBB4_158
# BB#146:                               # %vector.memcheck491
                                        #   in Loop: Header=BB4_143 Depth=1
	leaq	4(%rcx,%rdx,4), %rsi
	cmpq	%rsi, %rax
	jae	.LBB4_148
# BB#147:                               # %vector.memcheck491
                                        #   in Loop: Header=BB4_143 Depth=1
	leaq	4(%rax,%rdx,4), %rsi
	cmpq	%rsi, %rcx
	jb	.LBB4_158
.LBB4_148:                              # %vector.body470.preheader
                                        #   in Loop: Header=BB4_143 Depth=1
	leaq	-8(%r10), %r9
	movl	%r9d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB4_149
# BB#150:                               # %vector.body470.prol.preheader
                                        #   in Loop: Header=BB4_143 Depth=1
	leaq	-12(%rax,%rdx,4), %rbx
	leaq	-12(%rcx,%rdx,4), %rbp
	negq	%rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_151:                              # %vector.body470.prol
                                        #   Parent Loop BB4_143 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbp,%rsi,4), %xmm0
	movups	-16(%rbp,%rsi,4), %xmm1
	movups	%xmm0, (%rbx,%rsi,4)
	movups	%xmm1, -16(%rbx,%rsi,4)
	addq	$-8, %rsi
	incq	%rdi
	jne	.LBB4_151
# BB#152:                               # %vector.body470.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB4_143 Depth=1
	negq	%rsi
	cmpq	$24, %r9
	jae	.LBB4_154
	jmp	.LBB4_156
.LBB4_149:                              #   in Loop: Header=BB4_143 Depth=1
	xorl	%esi, %esi
	cmpq	$24, %r9
	jb	.LBB4_156
.LBB4_154:                              # %vector.body470.preheader.new
                                        #   in Loop: Header=BB4_143 Depth=1
	movq	%r11, %rdi
	andq	$-8, %rdi
	negq	%rdi
	negq	%rsi
	leaq	-12(%rax,%rdx,4), %rbx
	leaq	-12(%rcx,%rdx,4), %rbp
	.p2align	4, 0x90
.LBB4_155:                              # %vector.body470
                                        #   Parent Loop BB4_143 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbp,%rsi,4), %xmm0
	movups	-16(%rbp,%rsi,4), %xmm1
	movups	%xmm0, (%rbx,%rsi,4)
	movups	%xmm1, -16(%rbx,%rsi,4)
	movups	-32(%rbp,%rsi,4), %xmm0
	movups	-48(%rbp,%rsi,4), %xmm1
	movups	%xmm0, -32(%rbx,%rsi,4)
	movups	%xmm1, -48(%rbx,%rsi,4)
	movups	-64(%rbp,%rsi,4), %xmm0
	movups	-80(%rbp,%rsi,4), %xmm1
	movups	%xmm0, -64(%rbx,%rsi,4)
	movups	%xmm1, -80(%rbx,%rsi,4)
	movups	-96(%rbp,%rsi,4), %xmm0
	movups	-112(%rbp,%rsi,4), %xmm1
	movups	%xmm0, -96(%rbx,%rsi,4)
	movups	%xmm1, -112(%rbx,%rsi,4)
	addq	$-32, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB4_155
.LBB4_156:                              # %middle.block471
                                        #   in Loop: Header=BB4_143 Depth=1
	cmpq	%r10, %r11
	movq	24(%rsp), %rbx          # 8-byte Reload
	je	.LBB4_160
# BB#157:                               #   in Loop: Header=BB4_143 Depth=1
	subq	%r10, %rdx
	.p2align	4, 0x90
.LBB4_158:                              # %scalar.ph472.preheader
                                        #   in Loop: Header=BB4_143 Depth=1
	incq	%rdx
	.p2align	4, 0x90
.LBB4_159:                              # %scalar.ph472
                                        #   Parent Loop BB4_143 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rcx,%rdx,4), %esi
	movl	%esi, -4(%rax,%rdx,4)
	decq	%rdx
	jg	.LBB4_159
.LBB4_160:                              # %.loopexit
                                        #   in Loop: Header=BB4_143 Depth=1
	incl	12(%r14)
	movslq	(%r14), %rdx
	leaq	(%rax,%rdx,4), %rax
	movl	(%rbx), %esi
.LBB4_161:                              #   in Loop: Header=BB4_143 Depth=1
	movslq	%esi, %rdx
	leaq	(%rcx,%rdx,4), %rcx
	cmpq	%r8, %rcx
	jb	.LBB4_143
.LBB4_162:                              # %._crit_edge
	testb	$1, debug(%rip)
	je	.LBB4_165
# BB#163:
	movl	12(%r14), %esi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, verbose_debug(%rip)
	je	.LBB4_165
# BB#164:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	cprint
.LBB4_165:
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_free
	movq	%r14, %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	compl_merge, .Lfunc_end4-compl_merge
	.cfi_endproc

	.p2align	4, 0x90
	.type	compl_lift_onset,@function
compl_lift_onset:                       # @compl_lift_onset
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi71:
	.cfi_def_cfa_offset 144
.Lcfi72:
	.cfi_offset %rbx, -56
.Lcfi73:
	.cfi_offset %r12, -48
.Lcfi74:
	.cfi_offset %r13, -40
.Lcfi75:
	.cfi_offset %r14, -32
.Lcfi76:
	.cfi_offset %r15, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	(%rdi), %rbp
	testq	%rbp, %rbp
	je	.LBB5_53
# BB#1:                                 # %.lr.ph62
	movq	cube+80(%rip), %rax
	movq	32(%rax), %r13
	movq	cube+72(%rip), %rax
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %r15
	leaq	4(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	-4(%r13), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	-4(%r12), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	4(%r12), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	-4(%r15), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	4(%r15), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	-12(%r12), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	-12(%r15), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	-12(%r13), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB5_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_12 Depth 2
                                        #     Child Loop BB5_16 Depth 2
                                        #     Child Loop BB5_26 Depth 2
                                        #     Child Loop BB5_30 Depth 2
                                        #     Child Loop BB5_33 Depth 2
                                        #     Child Loop BB5_42 Depth 2
                                        #     Child Loop BB5_46 Depth 2
                                        #     Child Loop BB5_50 Depth 2
	addq	$8, %rdi
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	testb	$32, 1(%rbp)
	je	.LBB5_52
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	movl	(%r12), %eax
	movl	(%r13), %ecx
	movl	$-1024, %edx            # imm = 0xFC00
	andl	%edx, %ecx
	andl	$1023, %eax             # imm = 0x3FF
	movq	%rax, %r9
	movl	$1, %edx
	cmoveq	%rdx, %r9
	movl	%eax, %edx
	orl	%ecx, %edx
	movl	%edx, (%r13)
	cmpq	$8, %r9
	jb	.LBB5_15
# BB#4:                                 # %min.iters.checked131
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%r9, %r8
	andq	$1016, %r8              # imm = 0x3F8
	je	.LBB5_15
# BB#5:                                 # %vector.memcheck162
                                        #   in Loop: Header=BB5_2 Depth=1
	leaq	1(%rax), %rcx
	testl	%eax, %eax
	movl	$2, %edx
	cmovneq	%rdx, %rcx
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,4), %rdx
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	(%rsi,%rax,4), %rsi
	movq	72(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rcx,4), %r14
	movq	64(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rax,4), %rbx
	movq	56(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rcx,4), %r10
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %r11
	cmpq	%rbx, %rdx
	sbbb	%cl, %cl
	cmpq	%rsi, %r14
	sbbb	%bl, %bl
	andb	%cl, %bl
	cmpq	%r11, %rdx
	sbbb	%cl, %cl
	cmpq	%rsi, %r10
	sbbb	%dl, %dl
	testb	$1, %bl
	jne	.LBB5_15
# BB#6:                                 # %vector.memcheck162
                                        #   in Loop: Header=BB5_2 Depth=1
	andb	%dl, %cl
	andb	$1, %cl
	jne	.LBB5_15
# BB#7:                                 # %vector.body126.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	leaq	-8(%r8), %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB5_8
# BB#9:                                 # %vector.body126.prol
                                        #   in Loop: Header=BB5_2 Depth=1
	movups	-12(%r12,%rax,4), %xmm0
	movups	-28(%r12,%rax,4), %xmm1
	movups	-12(%r15,%rax,4), %xmm2
	movups	-28(%r15,%rax,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%r13,%rax,4)
	movups	%xmm3, -28(%r13,%rax,4)
	movl	$8, %esi
	testq	%rcx, %rcx
	jne	.LBB5_11
	jmp	.LBB5_13
.LBB5_8:                                #   in Loop: Header=BB5_2 Depth=1
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.LBB5_13
.LBB5_11:                               # %vector.body126.preheader.new
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%r9, %rdi
	andq	$-8, %rdi
	negq	%rdi
	negq	%rsi
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rbx
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rdx
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	(%rcx,%rax,4), %rcx
	.p2align	4, 0x90
.LBB5_12:                               # %vector.body126
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbx,%rsi,4), %xmm0
	movups	-16(%rbx,%rsi,4), %xmm1
	movups	(%rdx,%rsi,4), %xmm2
	movups	-16(%rdx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rcx,%rsi,4)
	movups	%xmm3, -16(%rcx,%rsi,4)
	movups	-32(%rbx,%rsi,4), %xmm0
	movups	-48(%rbx,%rsi,4), %xmm1
	movups	-32(%rdx,%rsi,4), %xmm2
	movups	-48(%rdx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rcx,%rsi,4)
	movups	%xmm3, -48(%rcx,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB5_12
.LBB5_13:                               # %middle.block127
                                        #   in Loop: Header=BB5_2 Depth=1
	cmpq	%r8, %r9
	je	.LBB5_17
# BB#14:                                #   in Loop: Header=BB5_2 Depth=1
	subq	%r8, %rax
	.p2align	4, 0x90
.LBB5_15:                               # %scalar.ph128.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB5_16:                               # %scalar.ph128
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%r15,%rax,4), %ecx
	andl	-4(%r12,%rax,4), %ecx
	movl	%ecx, -4(%r13,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB5_16
.LBB5_17:                               # %.loopexit185
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	(%rbp), %eax
	movl	(%r13), %edx
	movl	$-1024, %ecx            # imm = 0xFC00
	andl	%ecx, %edx
	andl	$1023, %eax             # imm = 0x3FF
	movl	$1, %ecx
	cmovneq	%rax, %rcx
	movl	%eax, %esi
	orl	%edx, %esi
	movl	%esi, (%r13)
	cmpq	$8, %rcx
	jb	.LBB5_29
# BB#18:                                # %min.iters.checked82
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%rcx, %r8
	andq	$1016, %r8              # imm = 0x3F8
	je	.LBB5_29
# BB#19:                                # %vector.memcheck104
                                        #   in Loop: Header=BB5_2 Depth=1
	leaq	1(%rax), %rdx
	testl	%eax, %eax
	movl	$2, %esi
	cmovneq	%rsi, %rdx
	movq	24(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,4), %rsi
	leaq	4(%rbp,%rax,4), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB5_21
# BB#20:                                # %vector.memcheck104
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	(%rsi,%rax,4), %rsi
	leaq	-4(%rbp,%rdx,4), %rdx
	cmpq	%rsi, %rdx
	jb	.LBB5_29
.LBB5_21:                               # %vector.body78.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	leaq	-8(%r8), %rsi
	movq	%rsi, %rdx
	shrq	$3, %rdx
	btl	$3, %esi
	jb	.LBB5_22
# BB#23:                                # %vector.body78.prol
                                        #   in Loop: Header=BB5_2 Depth=1
	movups	-12(%rbp,%rax,4), %xmm0
	movups	-28(%rbp,%rax,4), %xmm1
	movups	-12(%r13,%rax,4), %xmm2
	movups	-28(%r13,%rax,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%r13,%rax,4)
	movups	%xmm3, -28(%r13,%rax,4)
	movl	$8, %esi
	testq	%rdx, %rdx
	jne	.LBB5_25
	jmp	.LBB5_27
.LBB5_22:                               #   in Loop: Header=BB5_2 Depth=1
	xorl	%esi, %esi
	testq	%rdx, %rdx
	je	.LBB5_27
.LBB5_25:                               # %vector.body78.preheader.new
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%rcx, %rdi
	andq	$-8, %rdi
	negq	%rdi
	negq	%rsi
	leaq	-12(%rbp,%rax,4), %rbx
	movq	(%rsp), %rdx            # 8-byte Reload
	leaq	(%rdx,%rax,4), %rdx
	.p2align	4, 0x90
.LBB5_26:                               # %vector.body78
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbx,%rsi,4), %xmm0
	movups	-16(%rbx,%rsi,4), %xmm1
	movups	(%rdx,%rsi,4), %xmm2
	movups	-16(%rdx,%rsi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rdx,%rsi,4)
	movups	%xmm3, -16(%rdx,%rsi,4)
	movups	-32(%rbx,%rsi,4), %xmm0
	movups	-48(%rbx,%rsi,4), %xmm1
	movups	-32(%rdx,%rsi,4), %xmm2
	movups	-48(%rdx,%rsi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%rdx,%rsi,4)
	movups	%xmm3, -48(%rdx,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB5_26
.LBB5_27:                               # %middle.block79
                                        #   in Loop: Header=BB5_2 Depth=1
	cmpq	%r8, %rcx
	je	.LBB5_31
# BB#28:                                #   in Loop: Header=BB5_2 Depth=1
	subq	%r8, %rax
	.p2align	4, 0x90
.LBB5_29:                               # %scalar.ph80.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB5_30:                               # %scalar.ph80
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rbp,%rax,4), %ecx
	orl	%ecx, -4(%r13,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB5_30
.LBB5_31:                               # %.loopexit184
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	(%rax), %rcx
	movslq	12(%rax), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB5_35
# BB#32:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	24(%rcx), %rbx
	leaq	(%rbx,%rax,4), %r14
	.p2align	4, 0x90
.LBB5_33:                               # %.lr.ph
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	cdist0
	testl	%eax, %eax
	jne	.LBB5_52
# BB#34:                                #   in Loop: Header=BB5_33 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	(%rax), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r14, %rbx
	jb	.LBB5_33
.LBB5_35:                               # %._crit_edge
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	(%r13), %eax
	andl	$1023, %eax             # imm = 0x3FF
	leaq	1(%rax), %rcx
	cmpq	$8, %rcx
	jb	.LBB5_49
# BB#36:                                # %min.iters.checked
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%rcx, %r8
	andq	$2040, %r8              # imm = 0x7F8
	je	.LBB5_49
# BB#37:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	(%rdx,%rax,4), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB5_39
# BB#38:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_2 Depth=1
	leaq	4(%rbp,%rax,4), %rdx
	cmpq	%rdx, %r13
	jb	.LBB5_49
.LBB5_39:                               # %vector.body.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	leaq	-8(%r8), %r9
	movl	%r9d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB5_40
# BB#41:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	leaq	-12(%rbp,%rax,4), %rdx
	movq	(%rsp), %rsi            # 8-byte Reload
	leaq	(%rsi,%rax,4), %rbx
	negq	%rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_42:                               # %vector.body.prol
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbx,%rsi,4), %xmm0
	movups	-16(%rbx,%rsi,4), %xmm1
	movups	%xmm0, (%rdx,%rsi,4)
	movups	%xmm1, -16(%rdx,%rsi,4)
	addq	$-8, %rsi
	incq	%rdi
	jne	.LBB5_42
# BB#43:                                # %vector.body.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB5_2 Depth=1
	negq	%rsi
	cmpq	$24, %r9
	jae	.LBB5_45
	jmp	.LBB5_47
.LBB5_40:                               #   in Loop: Header=BB5_2 Depth=1
	xorl	%esi, %esi
	cmpq	$24, %r9
	jb	.LBB5_47
.LBB5_45:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%rcx, %rdi
	andq	$-8, %rdi
	negq	%rdi
	negq	%rsi
	leaq	-12(%rbp,%rax,4), %rbx
	movq	(%rsp), %rdx            # 8-byte Reload
	leaq	(%rdx,%rax,4), %rdx
	.p2align	4, 0x90
.LBB5_46:                               # %vector.body
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdx,%rsi,4), %xmm0
	movups	-16(%rdx,%rsi,4), %xmm1
	movups	%xmm0, (%rbx,%rsi,4)
	movups	%xmm1, -16(%rbx,%rsi,4)
	movups	-32(%rdx,%rsi,4), %xmm0
	movups	-48(%rdx,%rsi,4), %xmm1
	movups	%xmm0, -32(%rbx,%rsi,4)
	movups	%xmm1, -48(%rbx,%rsi,4)
	movups	-64(%rdx,%rsi,4), %xmm0
	movups	-80(%rdx,%rsi,4), %xmm1
	movups	%xmm0, -64(%rbx,%rsi,4)
	movups	%xmm1, -80(%rbx,%rsi,4)
	movups	-96(%rdx,%rsi,4), %xmm0
	movups	-112(%rdx,%rsi,4), %xmm1
	movups	%xmm0, -96(%rbx,%rsi,4)
	movups	%xmm1, -112(%rbx,%rsi,4)
	addq	$-32, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB5_46
.LBB5_47:                               # %middle.block
                                        #   in Loop: Header=BB5_2 Depth=1
	cmpq	%r8, %rcx
	je	.LBB5_51
# BB#48:                                #   in Loop: Header=BB5_2 Depth=1
	subq	%r8, %rax
	.p2align	4, 0x90
.LBB5_49:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB5_50:                               # %scalar.ph
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%r13,%rax,4), %ecx
	movl	%ecx, -4(%rbp,%rax,4)
	decq	%rax
	jg	.LBB5_50
.LBB5_51:                               # %.loopexit
                                        #   in Loop: Header=BB5_2 Depth=1
	orb	$32, 1(%rbp)
.LBB5_52:                               # %.backedge
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbp
	testq	%rbp, %rbp
	jne	.LBB5_2
.LBB5_53:                               # %._crit_edge63
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	compl_lift_onset, .Lfunc_end5-compl_lift_onset
	.cfi_endproc

	.p2align	4, 0x90
	.type	compl_lift_onset_complex,@function
compl_lift_onset_complex:               # @compl_lift_onset_complex
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi81:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi82:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi84:
	.cfi_def_cfa_offset 64
.Lcfi85:
	.cfi_offset %rbx, -56
.Lcfi86:
	.cfi_offset %r12, -48
.Lcfi87:
	.cfi_offset %r13, -40
.Lcfi88:
	.cfi_offset %r14, -32
.Lcfi89:
	.cfi_offset %r15, -24
.Lcfi90:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %r12
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	jge	.LBB6_2
# BB#1:
	movl	$8, %edi
	jmp	.LBB6_3
.LBB6_2:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB6_3:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movslq	%ebp, %rbx
	movl	$-2, %ebp
	jmp	.LBB6_4
	.p2align	4, 0x90
.LBB6_16:                               # %._crit_edge
                                        #   in Loop: Header=BB6_4 Depth=1
	movq	cube+72(%rip), %rax
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	(%rax,%rbx,8), %rsi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r15, %rdx
	callq	set_diff
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	set_or
	testq	%r15, %r15
	movl	$0, %eax
	movl	$-2, %ebp
	je	.LBB6_4
# BB#17:                                #   in Loop: Header=BB6_4 Depth=1
	movq	%r15, %rdi
	callq	free
	xorl	%eax, %eax
.LBB6_4:                                # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_5 Depth 2
                                        #     Child Loop BB6_11 Depth 2
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB6_5:                                #   Parent Loop BB6_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.LBB6_18
# BB#6:                                 #   in Loop: Header=BB6_5 Depth=2
	addq	$8, %r12
	testb	$32, 1(%r13)
	je	.LBB6_5
# BB#7:                                 #   in Loop: Header=BB6_4 Depth=1
	movq	%rbx, (%rsp)            # 8-byte Spill
	movl	cube(%rip), %ecx
	movl	$1, %eax
	cmpl	$33, %ecx
	jl	.LBB6_9
# BB#8:                                 #   in Loop: Header=BB6_4 Depth=1
	decl	%ecx
	sarl	$5, %ecx
	incl	%ecx
	movl	%ecx, %eax
.LBB6_9:                                #   in Loop: Header=BB6_4 Depth=1
	movl	%eax, (%r15)
	movslq	%eax, %rcx
	movl	%ecx, %edx
	notl	%edx
	cmpl	$-3, %edx
	cmovlel	%ebp, %edx
	leal	1(%rax,%rdx), %eax
	subq	%rax, %rcx
	leaq	(%r15,%rcx,4), %rdi
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movslq	(%r14), %rcx
	movslq	12(%r14), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB6_16
# BB#10:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB6_4 Depth=1
	movq	24(%r14), %rbx
	leaq	(%rbx,%rax,4), %rbp
	.p2align	4, 0x90
.LBB6_11:                               # %.lr.ph
                                        #   Parent Loop BB6_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	cdist01
	cmpl	$1, %eax
	jg	.LBB6_15
# BB#12:                                #   in Loop: Header=BB6_11 Depth=2
	testl	%eax, %eax
	je	.LBB6_13
# BB#14:                                #   in Loop: Header=BB6_11 Depth=2
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	force_lower
	jmp	.LBB6_15
.LBB6_13:                               #   in Loop: Header=BB6_11 Depth=2
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	fatal
	.p2align	4, 0x90
.LBB6_15:                               #   in Loop: Header=BB6_11 Depth=2
	movslq	(%r14), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%rbp, %rbx
	jb	.LBB6_11
	jmp	.LBB6_16
.LBB6_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	compl_lift_onset_complex, .Lfunc_end6-compl_lift_onset_complex
	.cfi_endproc

	.type	complement.compl_level,@object # @complement.compl_level
	.local	complement.compl_level
	.comm	complement.compl_level,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"COMPLEMENT"
	.size	.L.str, 11

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"exit COMPLEMENT"
	.size	.L.str.1, 16

	.type	simp_comp.simplify_level,@object # @simp_comp.simplify_level
	.local	simp_comp.simplify_level
	.comm	simp_comp.simplify_level,4,4
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"SIMPCOMP"
	.size	.L.str.2, 9

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"exit SIMPCOMP (new)"
	.size	.L.str.3, 20

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"exit SIMPCOMP (compl)"
	.size	.L.str.4, 22

	.type	simplify.simplify_level,@object # @simplify.simplify_level
	.local	simplify.simplify_level
	.comm	simplify.simplify_level,4,4
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"SIMPLIFY"
	.size	.L.str.5, 9

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"exit SIMPLIFY"
	.size	.L.str.6, 14

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"compl_merge: left %d, right %d\n"
	.size	.L.str.7, 32

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%s (cl)\n%s (cr)\nLeft is\n"
	.size	.L.str.8, 25

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Result %d\n"
	.size	.L.str.10, 11

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"compl: ON-set and OFF-set are not orthogonal"
	.size	.L.str.11, 45

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"Right is"
	.size	.Lstr, 9


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
