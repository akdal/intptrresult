	.text
	.file	"libclamav_regex_list.bc"
	.globl	regex_list_match
	.p2align	4, 0x90
	.type	regex_list_match,@function
regex_list_match:                       # @regex_list_match
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rdx, %r12
	movq	%rdi, %rbp
	cmpl	$0, 32(%rbp)
	je	.LBB0_1
# BB#2:
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movl	144(%rsp), %r15d
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	%rsi, %rdi
	callq	strlen
	movq	%rax, %r14
	movq	%r12, %rdi
	callq	strlen
	movl	%ebx, 16(%rsp)          # 4-byte Spill
	testl	%ebx, %ebx
	movl	%r15d, %ebx
	sete	%cl
	testl	%ebx, %ebx
	setne	%r15b
	orb	%cl, %r15b
	cmpb	$1, %r15b
	movq	%r14, %rcx
	jne	.LBB0_4
# BB#3:
	cmpl	$1, %ebx
	movq	%r14, %rcx
	sbbq	$-1, %rcx
	leaq	1(%rcx,%rax), %rcx
.LBB0_4:
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leaq	1(%rcx), %r13
	movq	%r13, %rdi
	callq	cli_malloc
	testq	%rax, %rax
	je	.LBB0_5
# BB#6:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, %rdi
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	callq	strncpy
	testl	%ebx, %ebx
	movb	$58, %al
	jne	.LBB0_8
# BB#7:
	xorl	%eax, %eax
.LBB0_8:
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_10
# BB#9:
	movb	$58, %al
.LBB0_10:
	movq	8(%rsp), %rsi           # 8-byte Reload
	movb	%al, (%rsi,%r14)
	testb	%r15b, %r15b
	movl	16(%rsp), %ebx          # 4-byte Reload
	je	.LBB0_14
# BB#11:
	leaq	(%rsi,%r14), %rdi
	incq	%rdi
	movq	%r12, %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	strncpy
	movq	8(%rsp), %rsi           # 8-byte Reload
	cmpl	$0, 144(%rsp)
	movq	24(%rsp), %rax          # 8-byte Reload
	je	.LBB0_13
# BB#12:
	movb	$47, -1(%rsi,%rax)
.LBB0_13:
	movb	$0, (%rsi,%rax)
.LBB0_14:
	movq	%r14, 48(%rsp)          # 8-byte Spill
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testl	%ebx, %ebx
	je	.LBB0_26
# BB#15:
	leaq	72(%rsp), %rdi
	xorl	%esi, %esi
	movl	$8, %edx
	callq	cli_ac_initdata
	movl	%eax, %r14d
	testl	%r14d, %r14d
	jne	.LBB0_88
# BB#16:                                # %.preheader
	cmpq	$0, 24(%rbp)
	je	.LBB0_26
# BB#17:                                # %.lr.ph
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_18:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rcx
	addq	%r15, %rcx
	movl	$0, %r9d
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdx
	leaq	72(%rsp), %r14
	movq	%r14, %r8
	pushq	$0
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	$-1
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	cli_ac_scanbuff
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
	movq	%r14, %rdi
	movl	%eax, %r14d
	callq	cli_ac_freedata
	testl	%r14d, %r14d
	je	.LBB0_25
# BB#19:                                #   in Loop: Header=BB0_18 Depth=1
	movq	(%r12), %rdi
	movl	$58, %esi
	callq	strchr
	testq	%rax, %rax
	je	.LBB0_20
# BB#21:                                #   in Loop: Header=BB0_18 Depth=1
	incq	%rax
	movq	%rax, %rdi
	callq	strlen
	movq	%rax, %rbp
	jmp	.LBB0_22
	.p2align	4, 0x90
.LBB0_20:                               #   in Loop: Header=BB0_18 Depth=1
	xorl	%ebp, %ebp
.LBB0_22:                               #   in Loop: Header=BB0_18 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r13, %rdx
	callq	get_char_at_pos_with_skip
	cmpb	$63, %al
	movq	48(%rsp), %r12          # 8-byte Reload
	ja	.LBB0_23
# BB#31:                                #   in Loop: Header=BB0_18 Depth=1
	movzbl	%al, %ecx
	movabsq	$-9223231295071453183, %rdx # imm = 0x8000800100000001
	btq	%rcx, %rdx
	jae	.LBB0_23
# BB#32:                                #   in Loop: Header=BB0_18 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	movq	8(%rsp), %rsi           # 8-byte Reload
	je	.LBB0_33
# BB#34:                                #   in Loop: Header=BB0_18 Depth=1
	jbe	.LBB0_24
# BB#35:                                #   in Loop: Header=BB0_18 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	get_char_at_pos_with_skip
	cmpb	$46, %al
	je	.LBB0_36
# BB#37:                                #   in Loop: Header=BB0_18 Depth=1
	cmpb	$32, %al
	movq	8(%rsp), %rsi           # 8-byte Reload
	jne	.LBB0_24
	jmp	.LBB0_38
.LBB0_23:                               #   in Loop: Header=BB0_18 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB0_24:                               #   in Loop: Header=BB0_18 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rdx
	movsbl	%al, %ecx
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_25:                               #   in Loop: Header=BB0_18 Depth=1
	incq	%rbx
	addq	$80, %r15
	movq	56(%rsp), %rbp          # 8-byte Reload
	cmpq	24(%rbp), %rbx
	jb	.LBB0_18
.LBB0_26:                               # %.thread104
	leaq	8(%rbp), %rax
	addq	$16, %rbp
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	cmoveq	%rax, %rbp
	movq	(%rbp), %rbp
	cmpq	$0, 24(%rbp)
	je	.LBB0_84
# BB#27:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	$0, (%rax)
	movq	8(%rsp), %r14           # 8-byte Reload
	decq	%r14
	jmp	.LBB0_28
	.p2align	4, 0x90
.LBB0_65:                               #   in Loop: Header=BB0_64 Depth=2
	decq	%r14
	incq	%r13
	testq	%r15, %r15
	jne	.LBB0_64
	jmp	.LBB0_84
.LBB0_83:                               # %.critedge123.i
                                        #   in Loop: Header=BB0_28 Depth=1
	testq	%r15, %r15
	movq	%r15, %rbp
	jne	.LBB0_28
	jmp	.LBB0_84
.LBB0_74:                               #   in Loop: Header=BB0_28 Depth=1
	movq	%rdx, %r13
	movq	56(%rsp), %r14          # 8-byte Reload
.LBB0_75:                               # %.preheader133.i
                                        #   in Loop: Header=BB0_28 Depth=1
	movq	(%r10), %rcx
	movb	17(%rcx), %al
	testb	%al, %al
	cmoveq	%rcx, %r15
	testq	%r15, %r15
	je	.LBB0_84
# BB#76:                                # %.lr.ph191.preheader.i
                                        #   in Loop: Header=BB0_28 Depth=1
	addq	$2, %r14
	addq	$-2, %r13
	testb	%al, %al
	cmoveq	%r14, %r11
	cmoveq	%r13, %r12
	decq	%r11
	incq	%r12
	movq	%r12, %r13
	movq	%r11, %r14
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB0_77:                               # %.lr.ph191.i
                                        #   Parent Loop BB0_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, 17(%rbp)
	movq	(%rbp), %rbp
	je	.LBB0_79
# BB#78:                                #   in Loop: Header=BB0_77 Depth=2
	decq	%r14
	incq	%r13
	testq	%rbp, %rbp
	jne	.LBB0_77
	jmp	.LBB0_84
.LBB0_79:                               # %.critedge.i
                                        #   in Loop: Header=BB0_28 Depth=1
	testq	%rbp, %rbp
	jne	.LBB0_28
	jmp	.LBB0_84
	.p2align	4, 0x90
.LBB0_69:                               #   in Loop: Header=BB0_28 Depth=1
	movq	%r12, %r13
	movq	%r11, %r14
.LBB0_28:                               # %.critedge122.thread.outer.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_49 Depth 2
                                        #       Child Loop BB0_68 Depth 3
                                        #     Child Loop BB0_29 Depth 2
                                        #       Child Loop BB0_43 Depth 3
                                        #     Child Loop BB0_77 Depth 2
                                        #     Child Loop BB0_64 Depth 2
	movq	%rbp, %r15
	movq	24(%r15), %rbx
	movl	12(%r15), %esi
	cmpl	$5, %esi
	movq	%rbx, %r9
	movq	%r13, %r8
	movq	%r14, %rdx
	jne	.LBB0_49
.LBB0_29:                               # %.critedge122.thread.i.us
                                        #   Parent Loop BB0_28 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_43 Depth 3
	movq	%rbx, %r10
	leaq	1(%r14), %r11
	movq	%r13, %r12
	decq	%r12
	je	.LBB0_30
# BB#41:                                #   in Loop: Header=BB0_29 Depth=2
	movsbl	16(%r15), %r8d
	testl	%r8d, %r8d
	jle	.LBB0_47
# BB#42:                                # %.lr.ph.lr.ph.i.us
                                        #   in Loop: Header=BB0_29 Depth=2
	leal	-1(%r8), %ecx
	movsbl	(%r11), %r9d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_43:                               #   Parent Loop BB0_28 Depth=1
                                        #     Parent Loop BB0_29 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ecx, %edi
	subl	%ebx, %edi
	movl	%edi, %eax
	shrl	$31, %eax
	addl	%edi, %eax
	sarl	%eax
	addl	%ebx, %eax
	movslq	%eax, %rbp
	movq	(%r10,%rbp,8), %rbp
	movzbl	8(%rbp), %edx
	cmpl	%r9d, %edx
	je	.LBB0_69
# BB#44:                                #   in Loop: Header=BB0_43 Depth=3
	jl	.LBB0_46
# BB#45:                                #   in Loop: Header=BB0_43 Depth=3
	decl	%eax
	cmpl	$2, %edi
	movl	%eax, %ecx
	jge	.LBB0_43
	jmp	.LBB0_47
.LBB0_46:                               # %.outer.i.us
                                        #   in Loop: Header=BB0_43 Depth=3
	leal	1(%rax), %ebx
	cmpl	%eax, %ecx
	jg	.LBB0_43
	.p2align	4, 0x90
.LBB0_47:                               # %.outer._crit_edge.i.us
                                        #   in Loop: Header=BB0_29 Depth=2
	testb	%r8b, %r8b
	jne	.LBB0_75
# BB#48:                                #   in Loop: Header=BB0_29 Depth=2
	testq	%r10, %r10
	movl	$0, %ebx
	movq	%r12, %r13
	movq	%r11, %r14
	je	.LBB0_29
	jmp	.LBB0_81
	.p2align	4, 0x90
.LBB0_49:                               # %.critedge122.thread.i
                                        #   Parent Loop BB0_28 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_68 Depth 3
	cmpl	$6, %esi
	ja	.LBB0_57
# BB#50:                                # %.critedge122.thread.i
                                        #   in Loop: Header=BB0_49 Depth=2
	movl	$1, %ebp
	movl	%esi, %eax
	movq	%r9, %r10
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_52:                               #   in Loop: Header=BB0_49 Depth=2
	movzbl	(%rdx), %eax
	movzwl	char_class(%rax,%rax), %eax
	movzbl	8(%r15), %ebp
	andl	%eax, %ebp
	jmp	.LBB0_59
.LBB0_53:                               #   in Loop: Header=BB0_49 Depth=2
	movq	(%r9), %rax
	leaq	8(%r9), %r10
	movzbl	(%rdx), %ecx
	movq	%rcx, %rdi
	shrq	$3, %rdi
	movzbl	(%rax,%rdi), %eax
	andb	$7, %cl
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %ebp
	andl	%eax, %ebp
	jmp	.LBB0_60
.LBB0_51:                               #   in Loop: Header=BB0_49 Depth=2
	xorl	%ebp, %ebp
	cmpb	$10, (%rdx)
	setne	%bpl
	jmp	.LBB0_59
.LBB0_58:                               #   in Loop: Header=BB0_49 Depth=2
	decq	%rdx
	incq	%r8
.LBB0_59:                               # %.thread127.i
                                        #   in Loop: Header=BB0_49 Depth=2
	movq	%r9, %r10
.LBB0_60:                               # %.thread127.i
                                        #   in Loop: Header=BB0_49 Depth=2
	leaq	1(%rdx), %r11
	movq	%r8, %r12
	decq	%r12
	je	.LBB0_62
# BB#61:                                # %.thread127.i
                                        #   in Loop: Header=BB0_49 Depth=2
	testl	%ebp, %ebp
	je	.LBB0_62
# BB#66:                                #   in Loop: Header=BB0_49 Depth=2
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%r8, %rdx
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movl	%esi, 24(%rsp)          # 4-byte Spill
	movsbl	16(%r15), %ecx
	testl	%ecx, %ecx
	jle	.LBB0_73
# BB#67:                                # %.lr.ph.lr.ph.i
                                        #   in Loop: Header=BB0_49 Depth=2
	leal	-1(%rcx), %r9d
	movsbl	(%r11), %eax
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_68:                               #   Parent Loop BB0_28 Depth=1
                                        #     Parent Loop BB0_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r9d, %r8d
	subl	%edi, %r8d
	movl	%r8d, %esi
	shrl	$31, %esi
	addl	%r8d, %esi
	sarl	%esi
	addl	%edi, %esi
	movslq	%esi, %rbp
	movq	(%r10,%rbp,8), %rbp
	movzbl	8(%rbp), %ebx
	cmpl	%eax, %ebx
	je	.LBB0_69
# BB#70:                                #   in Loop: Header=BB0_68 Depth=3
	jl	.LBB0_71
# BB#72:                                #   in Loop: Header=BB0_68 Depth=3
	decl	%esi
	cmpl	$1, %r8d
	movl	%esi, %r9d
	jg	.LBB0_68
	jmp	.LBB0_73
.LBB0_71:                               # %.outer.i
                                        #   in Loop: Header=BB0_68 Depth=3
	leal	1(%rsi), %edi
	cmpl	%esi, %r9d
	jg	.LBB0_68
	.p2align	4, 0x90
.LBB0_73:                               # %.outer._crit_edge.i
                                        #   in Loop: Header=BB0_49 Depth=2
	testb	%cl, %cl
	jne	.LBB0_74
# BB#80:                                #   in Loop: Header=BB0_49 Depth=2
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	movl	$0, %r9d
	movq	%r12, %r8
	movq	%r11, %rdx
	movl	24(%rsp), %esi          # 4-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	je	.LBB0_49
.LBB0_81:                               # %.us-lcssa170.us
                                        #   in Loop: Header=BB0_28 Depth=1
	movq	(%r10), %rbp
	cmpl	$6, %esi
	movq	%r12, %r13
	movq	%r11, %r14
	jne	.LBB0_28
# BB#82:                                #   in Loop: Header=BB0_28 Depth=1
	movq	%r15, (%rbp)
	jmp	.LBB0_69
.LBB0_54:                               # %.us-lcssa169
                                        #   in Loop: Header=BB0_28 Depth=1
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_85
# BB#55:                                #   in Loop: Header=BB0_28 Depth=1
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r14, %rsi
	callq	cli_regexec
	testl	%eax, %eax
	je	.LBB0_85
# BB#56:                                # %.preheader.thread.i
                                        #   in Loop: Header=BB0_28 Depth=1
	decq	%r13
	incq	%r14
	movq	%r14, %r11
	movq	%r13, %r12
	jmp	.LBB0_63
.LBB0_30:                               #   in Loop: Header=BB0_28 Depth=1
	xorl	%r12d, %r12d
.LBB0_62:                               # %.preheader.i
                                        #   in Loop: Header=BB0_28 Depth=1
	testq	%r15, %r15
	je	.LBB0_84
.LBB0_63:                               # %.lr.ph195.i.preheader
                                        #   in Loop: Header=BB0_28 Depth=1
	decq	%r11
	incq	%r12
	movq	%r12, %r13
	movq	%r11, %r14
	.p2align	4, 0x90
.LBB0_64:                               # %.lr.ph195.i
                                        #   Parent Loop BB0_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, 17(%r15)
	movq	(%r15), %r15
	jne	.LBB0_65
	jmp	.LBB0_83
.LBB0_1:
	xorl	%r14d, %r14d
	jmp	.LBB0_88
.LBB0_5:
	movl	$-114, %r14d
	jmp	.LBB0_88
.LBB0_84:                               # %.loopexit
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	free
	xorl	%r14d, %r14d
	movl	$.L.str.5, %edi
.LBB0_87:
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_88:
	movl	%r14d, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_33:
	movq	%rcx, %rbp
.LBB0_38:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	leaq	1(%rbp), %rax
	cmpq	%rax, %r12
	jb	.LBB0_40
# BB#39:
	decq	%r12
	subq	%rbp, %r12
	movb	$46, (%rbx,%r12)
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
.LBB0_40:                               # %.thread
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	free
	jmp	.LBB0_86
.LBB0_85:                               # %.thread.i
	movq	(%rbx), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	free
	movl	$1, %r14d
.LBB0_86:
	movl	$.L.str.6, %edi
	jmp	.LBB0_87
.LBB0_36:
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB0_38
.LBB0_57:                               # %.us-lcssa
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	regex_list_match, .Lfunc_end0-regex_list_match
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_60
	.quad	.LBB0_52
	.quad	.LBB0_53
	.quad	.LBB0_51
	.quad	.LBB0_54
	.quad	.LBB0_60
	.quad	.LBB0_58

	.text
	.p2align	4, 0x90
	.type	get_char_at_pos_with_skip,@function
get_char_at_pos_with_skip:              # @get_char_at_pos_with_skip
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 64
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	testq	%r12, %r12
	je	.LBB1_11
# BB#1:
	movq	16(%r12), %r13
	movq	24(%r12), %rdx
	movq	32(%r12), %rcx
	xorl	%ebx, %ebx
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	movq	%r13, %r8
	movq	%r15, %r9
	callq	cli_dbgmsg
	addq	24(%r12), %r14
	movb	(%r13), %bpl
	testb	%bpl, %bpl
	je	.LBB1_5
# BB#2:                                 # %.lr.ph
	callq	__ctype_b_loc
	movq	(%rax), %rax
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movsbq	%bpl, %rcx
	testb	$8, (%rax,%rcx,2)
	jne	.LBB1_13
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=1
	movzbl	1(%r13,%rbx), %ebp
	incq	%rbx
	testb	%bpl, %bpl
	jne	.LBB1_3
.LBB1_5:                                # %.critedge.preheader.thread
	testq	%r14, %r14
	setne	%bpl
.LBB1_6:                                # %.preheader
	addq	$-2, %rbx
	.p2align	4, 0x90
.LBB1_7:                                # =>This Inner Loop Header: Depth=1
	cmpb	$32, 2(%r13,%rbx)
	leaq	1(%rbx), %rbx
	je	.LBB1_7
# BB#8:
	leaq	1(%r13,%rbx), %rsi
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testb	%bpl, %bpl
	je	.LBB1_10
# BB#9:
	cmpb	$0, 1(%r13,%rbx)
	je	.LBB1_18
.LBB1_10:
	xorl	%eax, %eax
	cmpq	$-1, %rbx
	cmovneq	%rbx, %rax
	movsbq	(%r13,%rax), %rax
	jmp	.LBB1_19
.LBB1_11:
	movq	%r15, %rdi
	callq	strlen
	cmpq	%r14, %rax
	jae	.LBB1_17
.LBB1_18:
	xorl	%eax, %eax
	jmp	.LBB1_19
.LBB1_13:                               # %.critedge.preheader
	testq	%r14, %r14
	setne	%bpl
	je	.LBB1_6
	.p2align	4, 0x90
.LBB1_14:                               # =>This Inner Loop Header: Depth=1
	cmpb	$32, (%r13,%rbx)
	leaq	1(%rbx), %rbx
	je	.LBB1_14
# BB#15:                                # %.critedge
                                        #   in Loop: Header=BB1_14 Depth=1
	decq	%r14
	setne	%bpl
	je	.LBB1_6
# BB#16:                                # %.critedge
                                        #   in Loop: Header=BB1_14 Depth=1
	movzbl	(%r13,%rbx), %eax
	testb	%al, %al
	jne	.LBB1_14
	jmp	.LBB1_6
.LBB1_17:
	leaq	-1(%r14), %rax
	testq	%r14, %r14
	cmoveq	%r14, %rax
	movsbq	(%r15,%rax), %rax
.LBB1_19:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	get_char_at_pos_with_skip, .Lfunc_end1-get_char_at_pos_with_skip
	.cfi_endproc

	.globl	init_regex_list
	.p2align	4, 0x90
	.type	init_regex_list,@function
init_regex_list:                        # @init_regex_list
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$0, 32(%rbx)
	movq	$0, (%rbx)
	movq	$0, 24(%rbx)
	movl	$32, %edi
	callq	cli_malloc
	testq	%rax, %rax
	je	.LBB2_5
# BB#1:                                 # %tree_root_alloc.exit.thread
	movb	$0, 16(%rax)
	movq	$0, 24(%rax)
	movl	$5, 12(%rax)
	movb	$0, 8(%rax)
	movq	$0, (%rax)
	movb	$1, 17(%rax)
	movq	%rax, 8(%rbx)
	movl	$32, %edi
	callq	cli_malloc
	testq	%rax, %rax
	je	.LBB2_6
# BB#2:                                 # %tree_root_alloc.exit20.thread
	movb	$0, 16(%rax)
	movq	$0, 24(%rax)
	movl	$5, 12(%rax)
	movb	$0, 8(%rax)
	movq	$0, (%rax)
	movb	$1, 17(%rax)
	movq	%rax, 16(%rbx)
	movl	$1024, %eax             # imm = 0x400
	movd	%rax, %xmm0
	movdqu	%xmm0, 56(%rbx)
	movl	$8192, %edi             # imm = 0x2000
	callq	cli_malloc
	movq	%rax, 48(%rbx)
	testq	%rax, %rax
	je	.LBB2_7
# BB#3:
	movl	$1024, %eax             # imm = 0x400
	movd	%rax, %xmm0
	movdqu	%xmm0, 80(%rbx)
	movl	$8192, %edi             # imm = 0x2000
	callq	cli_malloc
	movq	%rax, 72(%rbx)
	xorl	%ecx, %ecx
	testq	%rax, %rax
	movl	$-114, %ebp
	cmovnel	%ecx, %ebp
	je	.LBB2_11
# BB#4:
	movl	$1, 40(%rbx)
	movq	$1, 32(%rbx)
	xorl	%ebp, %ebp
	jmp	.LBB2_10
.LBB2_5:                                # %tree_root_alloc.exit
	movq	$0, 8(%rbx)
	jmp	.LBB2_9
.LBB2_6:                                # %tree_root_alloc.exit20
	movq	$0, 16(%rbx)
	jmp	.LBB2_8
.LBB2_7:
	movq	16(%rbx), %rdi
	callq	free
.LBB2_8:
	movq	8(%rbx), %rdi
	callq	free
.LBB2_9:
	movl	$-114, %ebp
.LBB2_10:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB2_11:
	leaq	48(%rbx), %r14
	movq	16(%rbx), %rdi
	callq	free
	movq	8(%rbx), %rdi
	callq	free
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_13
# BB#12:
	callq	free
.LBB2_13:                               # %stack_destroy.exit
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%r14)
	jmp	.LBB2_10
.Lfunc_end2:
	.size	init_regex_list, .Lfunc_end2-init_regex_list
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.zero	16
	.text
	.globl	load_regex_matcher
	.p2align	4, 0x90
	.type	load_regex_matcher,@function
load_regex_matcher:                     # @load_regex_matcher
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
	subq	$8312, %rsp             # imm = 0x2078
.Lcfi43:
	.cfi_def_cfa_offset 8368
.Lcfi44:
	.cfi_offset %rbx, -56
.Lcfi45:
	.cfi_offset %r12, -48
.Lcfi46:
	.cfi_offset %r13, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, %r14
	movl	$-116, %eax
	cmpl	$-1, 32(%r14)
	je	.LBB3_239
# BB#1:
	testq	%rbp, %rbp
	je	.LBB3_2
# BB#3:
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpl	$0, 32(%r14)
	jne	.LBB3_5
# BB#4:
	movq	%r14, %rdi
	callq	init_regex_list
	movl	%eax, %ebx
	cmpl	$0, 32(%r14)
	je	.LBB3_7
.LBB3_5:                                # %functionality_level_check.exit.thread117.preheader
	leaq	8(%r14), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	16(%r14), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	48(%r14), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	64(%r14), %r15
	leaq	112(%rsp), %rbx
	movl	$0, 44(%rsp)            # 4-byte Folded Spill
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	jmp	.LBB3_6
	.p2align	4, 0x90
.LBB3_25:                               #   in Loop: Header=BB3_6 Depth=1
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	leaq	112(%rsp), %rbx
	movq	%rbx, %rsi
	movl	%r13d, %edx
	callq	cli_dbgmsg
.LBB3_6:                                # %functionality_level_check.exit.thread117.outer.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_197 Depth 2
                                        #       Child Loop BB3_13 Depth 3
                                        #       Child Loop BB3_20 Depth 3
                                        #       Child Loop BB3_46 Depth 3
                                        #       Child Loop BB3_73 Depth 3
                                        #         Child Loop BB3_121 Depth 4
                                        #         Child Loop BB3_86 Depth 4
                                        #           Child Loop BB3_87 Depth 5
                                        #     Child Loop BB3_227 Depth 2
                                        #     Child Loop BB3_215 Depth 2
                                        #     Child Loop BB3_218 Depth 2
	movl	$8192, %esi             # imm = 0x2000
	movq	%rbx, %rdi
	jmp	.LBB3_197
.LBB3_188:                              #   in Loop: Header=BB3_197 Depth=2
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, %rbp
	movl	$-114, %r15d
	testq	%rbp, %rbp
	je	.LBB3_174
# BB#189:                               #   in Loop: Header=BB3_197 Depth=2
	leaq	113(%rsp), %rdi
	callq	cli_strdup
	movq	%rax, (%rbp)
	cmpb	$6, %bl
	jne	.LBB3_195
# BB#190:                               #   in Loop: Header=BB3_197 Depth=2
	movl	$32, %edi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_174
# BB#191:                               #   in Loop: Header=BB3_197 Depth=2
	movq	72(%rsp), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	cli_regcomp
	movq	%rbx, 8(%rbp)
	testl	%eax, %eax
	jne	.LBB3_192
# BB#193:                               #   in Loop: Header=BB3_197 Depth=2
	movl	$32, %edi
	callq	cli_malloc
	testq	%rax, %rax
	je	.LBB3_174
# BB#194:                               #   in Loop: Header=BB3_197 Depth=2
	movl	$4, 12(%rax)
	movq	%r13, (%rax)
	movb	$0, 16(%rax)
	movq	%rbp, 24(%rax)
	movb	$1, 17(%rax)
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	tree_node_insert_nonbin
	jmp	.LBB3_196
.LBB3_195:                              #   in Loop: Header=BB3_197 Depth=2
	movq	$0, 8(%rbp)
	movb	$0, 16(%r13)
	movq	%rbp, 24(%r13)
	movl	$4, (%r12)
	jmp	.LBB3_196
.LBB3_57:                               #   in Loop: Header=BB3_197 Depth=2
	xorl	%eax, %eax
	jmp	.LBB3_65
	.p2align	4, 0x90
.LBB3_196:                              # %add_pattern.exit
                                        #   in Loop: Header=BB3_197 Depth=2
	movl	$8192, %esi             # imm = 0x2000
	leaq	112(%rsp), %rbx
	movq	%rbx, %rdi
	movq	56(%rsp), %rbp          # 8-byte Reload
.LBB3_197:                              # %add_pattern.exit
                                        #   Parent Loop BB3_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_13 Depth 3
                                        #       Child Loop BB3_20 Depth 3
                                        #       Child Loop BB3_46 Depth 3
                                        #       Child Loop BB3_73 Depth 3
                                        #         Child Loop BB3_121 Depth 4
                                        #         Child Loop BB3_86 Depth 4
                                        #           Child Loop BB3_87 Depth 5
	movq	%rbp, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB3_17
# BB#8:                                 #   in Loop: Header=BB3_197 Depth=2
	movq	%rbx, %rdi
	callq	cli_chomp
	cmpb	$0, 112(%rsp)
	je	.LBB3_6
# BB#9:                                 #   in Loop: Header=BB3_197 Depth=2
	movl	$58, %esi
	movq	%rbx, %rdi
	callq	strrchr
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB3_28
# BB#10:                                #   in Loop: Header=BB3_197 Depth=2
	leaq	1(%r15), %r13
	movl	$45, %esi
	movq	%r13, %rdi
	callq	strchr
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB3_28
# BB#11:                                #   in Loop: Header=BB3_197 Depth=2
	cmpq	%r14, %r13
	jae	.LBB3_15
# BB#12:                                # %.lr.ph43.i
                                        #   in Loop: Header=BB3_197 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB3_13:                               #   Parent Loop BB3_6 Depth=1
                                        #     Parent Loop BB3_197 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbq	(%rcx), %rdx
	testb	$8, 1(%rax,%rdx,2)
	je	.LBB3_28
# BB#14:                                #   in Loop: Header=BB3_13 Depth=3
	incq	%rcx
	cmpq	%r14, %rcx
	jb	.LBB3_13
.LBB3_15:                               # %.preheader.i
                                        #   in Loop: Header=BB3_197 Depth=2
	leaq	1(%r14), %rbp
	movq	%rbp, %rdi
	callq	strlen
	testq	%rax, %rax
	je	.LBB3_21
# BB#16:                                # %.lr.ph.i
                                        #   in Loop: Header=BB3_197 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_20:                               #   Parent Loop BB3_6 Depth=1
                                        #     Parent Loop BB3_197 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbq	(%rbp,%rbx), %rax
	testb	$8, 1(%r12,%rax,2)
	je	.LBB3_28
# BB#19:                                #   in Loop: Header=BB3_20 Depth=3
	incq	%rbx
	movq	%rbp, %rdi
	callq	strlen
	cmpq	%rax, %rbx
	jb	.LBB3_20
.LBB3_21:                               # %._crit_edge.i
                                        #   in Loop: Header=BB3_197 Depth=2
	movb	$0, (%r14)
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, %r13
	movslq	%r13d, %r14
	movq	%rbp, %rdi
	callq	strlen
	testq	%rax, %rax
	je	.LBB3_22
# BB#23:                                #   in Loop: Header=BB3_197 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbp, %rdi
	callq	strtol
	movslq	%eax, %r12
	jmp	.LBB3_24
.LBB3_22:                               #   in Loop: Header=BB3_197 Depth=2
	movl	$2147483647, %r12d      # imm = 0x7FFFFFFF
.LBB3_24:                               #   in Loop: Header=BB3_197 Depth=2
	movq	56(%rsp), %rbp          # 8-byte Reload
	callq	cl_retflevel
	movl	%eax, %eax
	cmpq	%rax, %r14
	ja	.LBB3_25
# BB#26:                                #   in Loop: Header=BB3_197 Depth=2
	callq	cl_retflevel
	movl	%eax, %eax
	cmpq	%rax, %r12
	leaq	112(%rsp), %rbx
	jb	.LBB3_6
# BB#27:                                #   in Loop: Header=BB3_197 Depth=2
	movb	$0, (%r15)
	.p2align	4, 0x90
.LBB3_28:                               # %.loopexit120
                                        #   in Loop: Header=BB3_197 Depth=2
	movl	44(%rsp), %ebp          # 4-byte Reload
	incl	%ebp
	movl	$58, %esi
	leaq	112(%rsp), %rbx
	movq	%rbx, %rdi
	callq	strchr
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB3_29
# BB#30:                                #   in Loop: Header=BB3_197 Depth=2
	movl	%ebp, 44(%rsp)          # 4-byte Spill
	leaq	1(%r12), %rbp
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	je	.LBB3_34
# BB#31:                                #   in Loop: Header=BB3_197 Depth=2
	movq	%rbp, %rdi
	callq	strlen
	cmpq	$8191, %rax             # imm = 0x1FFF
	movq	24(%rsp), %r15          # 8-byte Reload
	ja	.LBB3_33
# BB#32:                                # %.thread107
                                        #   in Loop: Header=BB3_197 Depth=2
	movw	$47, (%rbp,%rax)
	movb	112(%rsp), %al
	cmpb	$88, %al
	je	.LBB3_41
	jmp	.LBB3_36
.LBB3_34:                               #   in Loop: Header=BB3_197 Depth=2
	movb	112(%rsp), %al
	cmpb	$82, %al
	movq	24(%rsp), %r15          # 8-byte Reload
	je	.LBB3_42
# BB#35:                                #   in Loop: Header=BB3_197 Depth=2
	cmpb	$88, %al
	jne	.LBB3_36
.LBB3_41:                               #   in Loop: Header=BB3_197 Depth=2
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB3_38
.LBB3_42:                               #   in Loop: Header=BB3_197 Depth=2
	xorl	%ecx, %ecx
	jmp	.LBB3_43
.LBB3_36:                               #   in Loop: Header=BB3_197 Depth=2
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB3_38
# BB#37:                                #   in Loop: Header=BB3_197 Depth=2
	movb	$1, %cl
	cmpb	$89, %al
	jne	.LBB3_38
.LBB3_43:                               #   in Loop: Header=BB3_197 Depth=2
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	$80, %edi
	callq	cli_malloc
	movq	%rax, (%rsp)            # 8-byte Spill
	testq	%rax, %rax
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	je	.LBB3_44
# BB#45:                                # %.preheader.i.i
                                        #   in Loop: Header=BB3_197 Depth=2
	movq	%rbp, %rbx
	xorl	%ebp, %ebp
	movl	$10, %r15d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_46:                               #   Parent Loop BB3_6 Depth=1
                                        #     Parent Loop BB3_197 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rbx, %r14
	movq	%r14, %rdi
	leaq	72(%rsp), %rsi
	callq	getNextToken
	movq	%rax, %rbx
	movzbl	88(%rsp), %r13d
	movl	%r13d, %eax
	addb	$-2, %al
	cmpb	$4, %al
	ja	.LBB3_50
# BB#47:                                #   in Loop: Header=BB3_46 Depth=3
	movzbl	%al, %eax
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_51:                               #   in Loop: Header=BB3_46 Depth=3
	testq	%r12, %r12
	movq	(%rsp), %rdi            # 8-byte Reload
	je	.LBB3_54
# BB#52:                                #   in Loop: Header=BB3_46 Depth=3
	movq	-8(%rdi,%r12,8), %rax
	cmpb	$124, (%rax)
	jne	.LBB3_54
# BB#53:                                #   in Loop: Header=BB3_46 Depth=3
	movq	%r14, -8(%rdi,%r12,8)
	jmp	.LBB3_61
	.p2align	4, 0x90
.LBB3_54:                               #   in Loop: Header=BB3_46 Depth=3
	movq	%r14, (%rdi,%r12,8)
	incq	%r12
	cmpq	%r15, %r12
	jne	.LBB3_61
# BB#55:                                #   in Loop: Header=BB3_46 Depth=3
	leaq	160(,%r15,8), %rsi
	callq	cli_realloc2
	testq	%rax, %rax
	je	.LBB3_57
# BB#56:                                #   in Loop: Header=BB3_46 Depth=3
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	20(%r15), %rax
	movq	%r15, %r12
	cmpb	$0, (%rbx)
	jne	.LBB3_63
	jmp	.LBB3_64
	.p2align	4, 0x90
.LBB3_48:                               #   in Loop: Header=BB3_46 Depth=3
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_50
# BB#49:                                #   in Loop: Header=BB3_46 Depth=3
	callq	free
.LBB3_50:                               # %thread-pre-split.thread.i.i
                                        #   in Loop: Header=BB3_46 Depth=3
	xorl	%eax, %eax
	cmpb	$3, %r13b
	sete	%al
	subq	%rax, %r12
	jmp	.LBB3_61
	.p2align	4, 0x90
.LBB3_58:                               #   in Loop: Header=BB3_46 Depth=3
	testq	%r12, %r12
	je	.LBB3_59
# BB#60:                                #   in Loop: Header=BB3_46 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %r14
	.p2align	4, 0x90
.LBB3_61:                               #   in Loop: Header=BB3_46 Depth=3
	movq	%r15, %rax
	cmpb	$0, (%rbx)
	je	.LBB3_64
.LBB3_63:                               #   in Loop: Header=BB3_46 Depth=3
	cmpb	$6, 88(%rsp)
	movq	%r14, %rbp
	movq	%rax, %r15
	jne	.LBB3_46
	jmp	.LBB3_64
.LBB3_59:                               #   in Loop: Header=BB3_46 Depth=3
	movq	%rbp, %r14
	movq	%r15, %rax
	xorl	%r12d, %r12d
	cmpb	$0, (%rbx)
	jne	.LBB3_63
.LBB3_64:                               # %.critedge.i.i
                                        #   in Loop: Header=BB3_197 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
	leaq	1(%r14), %rax
	cmpb	$0, (%rbx)
	cmovneq	%r14, %rax
.LBB3_65:                               # %find_regex_start.exit.i
                                        #   in Loop: Header=BB3_197 Depth=2
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	jmp	.LBB3_66
.LBB3_44:                               #   in Loop: Header=BB3_197 Depth=2
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB3_66:                               # %find_regex_start.exit.i
                                        #   in Loop: Header=BB3_197 Depth=2
	movl	8(%rsp), %eax           # 4-byte Reload
	testb	%al, %al
	movq	104(%rsp), %rax         # 8-byte Reload
	cmovneq	96(%rsp), %rax          # 8-byte Folded Reload
	movq	(%rax), %r13
	movq	$0, 64(%r14)
	movq	$0, 88(%r14)
	cmpq	$0, 56(%r14)
	je	.LBB3_68
# BB#67:                                # %._crit_edge.i.i
                                        #   in Loop: Header=BB3_197 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	xorl	%ecx, %ecx
	movq	64(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB3_70
.LBB3_68:                               #   in Loop: Header=BB3_197 Depth=2
	movq	$4096, 56(%r14)         # imm = 0x1000
	movq	48(%r14), %rdi
	movl	$32768, %esi            # imm = 0x8000
	callq	cli_realloc2
	movq	%rax, 48(%r14)
	testq	%rax, %rax
	movq	64(%rsp), %rbp          # 8-byte Reload
	je	.LBB3_71
# BB#69:                                # %._crit_edge1.i.i
                                        #   in Loop: Header=BB3_197 Depth=2
	movq	(%r15), %rcx
.LBB3_70:                               #   in Loop: Header=BB3_197 Depth=2
	leaq	1(%rcx), %rdx
	movq	%rdx, (%r15)
	movq	%r13, (%rax,%rcx,8)
.LBB3_71:                               # %stack_push.exit.preheader.i
                                        #   in Loop: Header=BB3_197 Depth=2
	cmpl	$4, 12(%r13)
	je	.LBB3_196
# BB#72:                                # %.lr.ph.i99.preheader
                                        #   in Loop: Header=BB3_197 Depth=2
	leaq	12(%r13), %r12
	.p2align	4, 0x90
.LBB3_73:                               # %.lr.ph.i99
                                        #   Parent Loop BB3_6 Depth=1
                                        #     Parent Loop BB3_197 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_121 Depth 4
                                        #         Child Loop BB3_86 Depth 4
                                        #           Child Loop BB3_87 Depth 5
	cmpq	(%rsp), %rbp            # 8-byte Folded Reload
	jae	.LBB3_75
# BB#74:                                #   in Loop: Header=BB3_73 Depth=3
	movq	%rbp, %rdi
	leaq	72(%rsp), %rsi
	callq	getNextToken
	movq	%rax, %rbp
	movb	88(%rsp), %bl
	jmp	.LBB3_78
	.p2align	4, 0x90
.LBB3_75:                               #   in Loop: Header=BB3_73 Depth=3
	cmpb	$0, (%rbp)
	je	.LBB3_77
# BB#76:                                #   in Loop: Header=BB3_73 Depth=3
	movb	$6, 88(%rsp)
	movq	%rbp, 72(%rsp)
	movb	$6, %bl
	jmp	.LBB3_78
.LBB3_77:                               #   in Loop: Header=BB3_73 Depth=3
	movb	$7, 88(%rsp)
	movb	$7, %bl
	.p2align	4, 0x90
.LBB3_78:                               #   in Loop: Header=BB3_73 Depth=3
	movsbl	%bl, %eax
	cmpl	$7, %eax
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	ja	.LBB3_79
# BB#240:                               #   in Loop: Header=BB3_73 Depth=3
	jmpq	*.LJTI3_1(,%rax,8)
.LBB3_81:                               #   in Loop: Header=BB3_73 Depth=3
	movb	72(%rsp), %cl
	movl	(%r12), %r8d
	movq	24(%r13), %rdi
	cmpl	$2, %r8d
	movq	%rdi, %r10
	jne	.LBB3_83
# BB#82:                                #   in Loop: Header=BB3_73 Depth=3
	leaq	8(%rdi), %r10
	movq	8(%rdi), %rax
	testq	%rax, %rax
	cmoveq	%rax, %r10
.LBB3_83:                               # %tree_node_get_children.exit.i.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movsbl	16(%r13), %r9d
	testl	%r9d, %r9d
	movb	%cl, 8(%rsp)            # 1-byte Spill
	jle	.LBB3_84
# BB#85:                                # %.lr.ph.lr.ph.i.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movsbl	%cl, %r11d
	xorl	%ebx, %ebx
	movl	%r9d, %ecx
.LBB3_86:                               # %.lr.ph.i.i
                                        #   Parent Loop BB3_6 Depth=1
                                        #     Parent Loop BB3_197 Depth=2
                                        #       Parent Loop BB3_73 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB3_87 Depth 5
	movl	%ecx, %edx
	decl	%edx
	.p2align	4, 0x90
.LBB3_87:                               #   Parent Loop BB3_6 Depth=1
                                        #     Parent Loop BB3_197 Depth=2
                                        #       Parent Loop BB3_73 Depth=3
                                        #         Parent Loop BB3_86 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	%edx, %eax
	subl	%ebx, %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	addl	%ebx, %ecx
	movslq	%ecx, %rbp
	movq	(%r10,%rbp,8), %rbp
	movzbl	8(%rbp), %esi
	cmpl	%r11d, %esi
	je	.LBB3_91
# BB#88:                                #   in Loop: Header=BB3_87 Depth=5
	jge	.LBB3_90
# BB#89:                                # %.thread.i.i
                                        #   in Loop: Header=BB3_87 Depth=5
	leal	1(%rcx), %ebx
	cmpl	%ecx, %edx
	jg	.LBB3_87
	jmp	.LBB3_92
	.p2align	4, 0x90
.LBB3_90:                               # %.thread.outer.i.i
                                        #   in Loop: Header=BB3_86 Depth=4
	cmpl	$1, %eax
	jg	.LBB3_86
	jmp	.LBB3_92
.LBB3_177:                              #   in Loop: Header=BB3_73 Depth=3
	movq	24(%r13), %rax
	cmpl	$2, (%r12)
	jne	.LBB3_179
# BB#178:                               #   in Loop: Header=BB3_73 Depth=3
	movq	8(%rax), %rcx
	addq	$8, %rax
	testq	%rcx, %rcx
	cmoveq	%rcx, %rax
.LBB3_179:                              # %tree_node_get_children.exit.i157.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movb	16(%r13), %cl
	testq	%rax, %rax
	je	.LBB3_182
# BB#180:                               # %tree_node_get_children.exit.i157.i
                                        #   in Loop: Header=BB3_73 Depth=3
	testb	%cl, %cl
	jne	.LBB3_182
# BB#181:                               #   in Loop: Header=BB3_73 Depth=3
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	cmoveq	%r13, %rbx
	jmp	.LBB3_184
.LBB3_107:                              #   in Loop: Header=BB3_73 Depth=3
	movq	88(%r14), %rcx
	cmpq	80(%r14), %rcx
	jne	.LBB3_108
# BB#109:                               #   in Loop: Header=BB3_73 Depth=3
	leaq	4096(%rcx), %rax
	movq	%rax, 80(%r14)
	movq	72(%r14), %rdi
	leaq	32768(,%rcx,8), %rsi
	callq	cli_realloc2
	movq	%rax, 72(%r14)
	testq	%rax, %rax
	je	.LBB3_112
# BB#110:                               # %._crit_edge1.i120.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movq	88(%r14), %rcx
	jmp	.LBB3_111
.LBB3_117:                              #   in Loop: Header=BB3_73 Depth=3
	movl	$32, %edi
	callq	cli_malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_118
# BB#119:                               #   in Loop: Header=BB3_73 Depth=3
	movb	$0, 16(%rbp)
	movq	$0, (%rbp)
	movq	%rbp, %rax
	addq	$17, %rax
	movb	$1, 17(%rbp)
	movq	$0, 24(%rbp)
	jmp	.LBB3_120
.LBB3_138:                              #   in Loop: Header=BB3_73 Depth=3
	movq	24(%r13), %rax
	cmpl	$2, (%r12)
	jne	.LBB3_140
# BB#139:                               #   in Loop: Header=BB3_73 Depth=3
	movq	8(%rax), %rcx
	addq	$8, %rax
	testq	%rcx, %rcx
	cmoveq	%rcx, %rax
.LBB3_140:                              # %tree_node_get_children.exit.i146.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movb	16(%r13), %cl
	testq	%rax, %rax
	je	.LBB3_143
# BB#141:                               # %tree_node_get_children.exit.i146.i
                                        #   in Loop: Header=BB3_73 Depth=3
	testb	%cl, %cl
	jne	.LBB3_143
# BB#142:                               #   in Loop: Header=BB3_73 Depth=3
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	cmoveq	%r13, %rbx
	jmp	.LBB3_145
.LBB3_125:                              #   in Loop: Header=BB3_73 Depth=3
	movq	88(%r14), %rcx
	cmpq	80(%r14), %rcx
	jne	.LBB3_126
# BB#127:                               #   in Loop: Header=BB3_73 Depth=3
	leaq	4096(%rcx), %rax
	movq	%rax, 80(%r14)
	movq	72(%r14), %rdi
	leaq	32768(,%rcx,8), %rsi
	callq	cli_realloc2
	movq	%rax, 72(%r14)
	testq	%rax, %rax
	je	.LBB3_130
# BB#128:                               # %._crit_edge1.i133.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movq	88(%r14), %rcx
	jmp	.LBB3_129
.LBB3_182:                              # %thread-pre-split.i159.i
                                        #   in Loop: Header=BB3_73 Depth=3
	cmpb	$2, %cl
	movq	%r13, %rbx
	jl	.LBB3_184
# BB#183:                               #   in Loop: Header=BB3_73 Depth=3
	movq	(%rax), %rax
	movq	(%rax), %rbx
.LBB3_184:                              # %tree_get_next.exit162.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movl	$32, %edi
	callq	cli_malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_186
# BB#185:                               #   in Loop: Header=BB3_73 Depth=3
	movb	$0, 16(%rbp)
	movq	%rbx, (%rbp)
	movb	$1, 17(%rbp)
	movq	$0, 24(%rbp)
.LBB3_186:                              # %tree_node_alloc.exit163.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movl	$3, 12(%rbp)
	jmp	.LBB3_187
.LBB3_108:                              # %._crit_edge.i118.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movq	72(%r14), %rax
.LBB3_111:                              #   in Loop: Header=BB3_73 Depth=3
	leaq	1(%rcx), %rdx
	movq	%rdx, 88(%r14)
	movq	$0, (%rax,%rcx,8)
.LBB3_112:                              # %stack_push.exit121.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movq	64(%r14), %rcx
	cmpq	56(%r14), %rcx
	jne	.LBB3_113
# BB#114:                               #   in Loop: Header=BB3_73 Depth=3
	leaq	4096(%rcx), %rax
	movq	%rax, 56(%r14)
	movq	48(%r14), %rdi
	leaq	32768(,%rcx,8), %rsi
	callq	cli_realloc2
	movq	%rax, 48(%r14)
	testq	%rax, %rax
	je	.LBB3_79
# BB#115:                               # %._crit_edge1.i126.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movq	(%r15), %rcx
	jmp	.LBB3_116
.LBB3_143:                              # %thread-pre-split.i.i
                                        #   in Loop: Header=BB3_73 Depth=3
	cmpb	$2, %cl
	movq	%r13, %rbx
	jl	.LBB3_145
# BB#144:                               #   in Loop: Header=BB3_73 Depth=3
	movq	(%rax), %rax
	movq	(%rax), %rbx
.LBB3_145:                              # %tree_get_next.exit.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movl	$32, %edi
	callq	cli_malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_147
# BB#146:                               #   in Loop: Header=BB3_73 Depth=3
	movb	$0, 16(%rbp)
	movq	%rbx, (%rbp)
	movb	$1, 17(%rbp)
	movq	$0, 24(%rbp)
.LBB3_147:                              # %tree_node_alloc.exit149.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movq	72(%rsp), %rbx
	movl	$char_class_bitmap, %esi
	movl	$32, %edx
	movq	%rbx, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB3_148
# BB#150:                               #   in Loop: Header=BB3_73 Depth=3
	movl	$char_class_bitmap+32, %esi
	movl	$32, %edx
	movq	%rbx, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB3_151
# BB#152:                               #   in Loop: Header=BB3_73 Depth=3
	movl	$char_class_bitmap+64, %esi
	movl	$32, %edx
	movq	%rbx, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB3_153
# BB#154:                               #   in Loop: Header=BB3_73 Depth=3
	movl	$char_class_bitmap+96, %esi
	movl	$32, %edx
	movq	%rbx, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB3_155
# BB#156:                               #   in Loop: Header=BB3_73 Depth=3
	movl	$char_class_bitmap+128, %esi
	movl	$32, %edx
	movq	%rbx, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB3_157
# BB#158:                               #   in Loop: Header=BB3_73 Depth=3
	movl	$char_class_bitmap+160, %esi
	movl	$32, %edx
	movq	%rbx, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB3_159
# BB#160:                               #   in Loop: Header=BB3_73 Depth=3
	movl	$char_class_bitmap+192, %esi
	movl	$32, %edx
	movq	%rbx, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB3_161
# BB#162:                               #   in Loop: Header=BB3_73 Depth=3
	movl	$char_class_bitmap+224, %esi
	movl	$32, %edx
	movq	%rbx, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB3_163
# BB#164:                               #   in Loop: Header=BB3_73 Depth=3
	movl	$char_class_bitmap+256, %esi
	movl	$32, %edx
	movq	%rbx, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB3_165
# BB#166:                               #   in Loop: Header=BB3_73 Depth=3
	movl	$char_class_bitmap+288, %esi
	movl	$32, %edx
	movq	%rbx, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB3_167
# BB#168:                               #   in Loop: Header=BB3_73 Depth=3
	movl	$char_class_bitmap+320, %esi
	movl	$32, %edx
	movq	%rbx, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB3_169
# BB#170:                               #   in Loop: Header=BB3_73 Depth=3
	movl	$char_class_bitmap+352, %esi
	movl	$32, %edx
	movq	%rbx, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB3_171
# BB#172:                               #   in Loop: Header=BB3_73 Depth=3
	movl	$2, 12(%rbp)
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, 24(%rbp)
	testq	%rax, %rax
	je	.LBB3_173
# BB#176:                               #   in Loop: Header=BB3_73 Depth=3
	movq	%rbx, (%rax)
	movq	24(%rbp), %rax
	movq	$0, 8(%rax)
	jmp	.LBB3_187
.LBB3_126:                              # %._crit_edge.i131.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movq	72(%r14), %rax
.LBB3_129:                              #   in Loop: Header=BB3_73 Depth=3
	leaq	1(%rcx), %rdx
	movq	%rdx, 88(%r14)
	movq	%r13, (%rax,%rcx,8)
.LBB3_130:                              # %stack_push.exit134.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.LBB3_131
# BB#132:                               #   in Loop: Header=BB3_73 Depth=3
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rdx
	leaq	-1(%rax), %rcx
	movq	%rcx, (%r15)
	movq	-8(%rdx,%rax,8), %rbp
	cmpq	56(%r14), %rcx
	je	.LBB3_135
	jmp	.LBB3_134
.LBB3_113:                              # %._crit_edge.i124.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
.LBB3_116:                              #   in Loop: Header=BB3_73 Depth=3
	leaq	1(%rcx), %rdx
	movq	%rdx, (%r15)
	movq	%r13, (%rax,%rcx,8)
.LBB3_79:                               #   in Loop: Header=BB3_73 Depth=3
	movq	%r13, %rbp
	jmp	.LBB3_80
.LBB3_84:                               #   in Loop: Header=BB3_73 Depth=3
	xorl	%ebx, %ebx
	jmp	.LBB3_92
.LBB3_118:                              #   in Loop: Header=BB3_73 Depth=3
	movl	$17, %eax
.LBB3_120:                              # %tree_node_alloc.exit.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movl	$6, 12(%rbp)
	movb	$0, 8(%rbp)
	movb	$1, (%rax)
	movq	%r13, %rdi
	.p2align	4, 0x90
.LBB3_121:                              # %tree_node_alloc.exit.i
                                        #   Parent Loop BB3_6 Depth=1
                                        #     Parent Loop BB3_197 Depth=2
                                        #       Parent Loop BB3_73 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rbp, %rsi
	callq	tree_node_insert_nonbin
	movq	88(%r14), %rax
	testq	%rax, %rax
	je	.LBB3_123
# BB#122:                               # %stack_pop.exit.i
                                        #   in Loop: Header=BB3_121 Depth=4
	movq	72(%r14), %rcx
	leaq	-1(%rax), %rdx
	movq	%rdx, 88(%r14)
	movq	-8(%rcx,%rax,8), %rdi
	testq	%rdi, %rdi
	jne	.LBB3_121
.LBB3_123:                              # %stack_pop.exit.thread.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.LBB3_80
# BB#124:                               #   in Loop: Header=BB3_73 Depth=3
	decq	%rax
	movq	%rax, (%r15)
	jmp	.LBB3_80
.LBB3_148:                              #   in Loop: Header=BB3_73 Depth=3
	xorl	%eax, %eax
	jmp	.LBB3_149
.LBB3_131:                              #   in Loop: Header=BB3_73 Depth=3
	xorl	%ecx, %ecx
	xorl	%ebp, %ebp
	cmpq	56(%r14), %rcx
	jne	.LBB3_134
.LBB3_135:                              #   in Loop: Header=BB3_73 Depth=3
	leaq	4096(%rcx), %rax
	movq	%rax, 56(%r14)
	movq	48(%r14), %rdi
	leaq	32768(,%rcx,8), %rsi
	callq	cli_realloc2
	movq	%rax, 48(%r14)
	testq	%rax, %rax
	je	.LBB3_80
# BB#136:                               # %._crit_edge1.i140.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movq	(%r15), %rcx
	jmp	.LBB3_137
.LBB3_134:                              # %._crit_edge.i138.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
.LBB3_137:                              #   in Loop: Header=BB3_73 Depth=3
	leaq	1(%rcx), %rdx
	movq	%rdx, (%r15)
	movq	%rbp, (%rax,%rcx,8)
	jmp	.LBB3_80
.LBB3_91:                               # %tree_node_char_binsearch.exit.i
                                        #   in Loop: Header=BB3_73 Depth=3
	testq	%rbp, %rbp
	jne	.LBB3_80
.LBB3_92:                               # %tree_node_char_binsearch.exit.thread.i
                                        #   in Loop: Header=BB3_73 Depth=3
	cmpl	$2, %r8d
	movq	%rdi, %rax
	jne	.LBB3_94
# BB#93:                                #   in Loop: Header=BB3_73 Depth=3
	leaq	8(%rdi), %rax
	movq	8(%rdi), %rcx
	testq	%rcx, %rcx
	cmoveq	%rcx, %rax
.LBB3_94:                               # %tree_node_get_children.exit.i.i.i
                                        #   in Loop: Header=BB3_73 Depth=3
	testb	%r9b, %r9b
	jne	.LBB3_97
# BB#95:                                # %tree_node_get_children.exit.i.i.i
                                        #   in Loop: Header=BB3_73 Depth=3
	testq	%rax, %rax
	je	.LBB3_97
# BB#96:                                #   in Loop: Header=BB3_73 Depth=3
	movq	(%rax), %r15
	testq	%r15, %r15
	cmoveq	%r13, %r15
	jmp	.LBB3_99
.LBB3_97:                               # %thread-pre-split.i.i.i
                                        #   in Loop: Header=BB3_73 Depth=3
	cmpb	$2, %r9b
	movq	%r13, %r15
	jl	.LBB3_99
# BB#98:                                #   in Loop: Header=BB3_73 Depth=3
	movq	(%rax), %rax
	movq	(%rax), %r15
.LBB3_99:                               # %tree_get_next.exit.i.i
                                        #   in Loop: Header=BB3_73 Depth=3
	xorl	%esi, %esi
	cmpl	$2, %r8d
	sete	%sil
	incb	%r9b
	movb	%r9b, 16(%r13)
	movsbq	%r9b, %rax
	addq	%rax, %rsi
	shlq	$3, %rsi
	callq	cli_realloc2
	movq	%rax, 24(%r13)
	testq	%rax, %rax
	je	.LBB3_100
# BB#101:                               #   in Loop: Header=BB3_73 Depth=3
	leaq	8(%rax), %r14
	cmpl	$2, (%r12)
	cmovneq	%rax, %r14
	movl	$32, %edi
	callq	cli_malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_103
# BB#102:                               # %tree_node_alloc.exit.thread.i.i
                                        #   in Loop: Header=BB3_73 Depth=3
	cmpq	%r13, %r15
	movb	$0, 16(%rbp)
	movq	%r15, (%rbp)
	sete	17(%rbp)
	movq	$0, 24(%rbp)
	movl	$0, 12(%rbp)
	movb	8(%rsp), %al            # 1-byte Reload
	movb	%al, 8(%rbp)
.LBB3_103:                              # %tree_node_alloc.exit.i.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movsbq	16(%r13), %rax
	movslq	%ebx, %rcx
	subq	%rcx, %rax
	cmpl	$1, %eax
	jle	.LBB3_104
# BB#105:                               #   in Loop: Header=BB3_73 Depth=3
	leaq	8(%r14,%rcx,8), %rdi
	leaq	(%r14,%rcx,8), %rbx
	leaq	-8(,%rax,8), %rdx
	movq	%rbx, %rsi
	callq	memmove
	jmp	.LBB3_106
.LBB3_100:                              #   in Loop: Header=BB3_73 Depth=3
	xorl	%ebp, %ebp
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	jmp	.LBB3_80
.LBB3_104:                              # %tree_node_alloc.exit._crit_edge.i.i
                                        #   in Loop: Header=BB3_73 Depth=3
	leaq	(%r14,%rcx,8), %rbx
.LBB3_106:                              #   in Loop: Header=BB3_73 Depth=3
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	%rbp, (%rbx)
	jmp	.LBB3_80
.LBB3_151:                              #   in Loop: Header=BB3_73 Depth=3
	movb	$1, %al
	jmp	.LBB3_149
.LBB3_153:                              #   in Loop: Header=BB3_73 Depth=3
	movb	$2, %al
	jmp	.LBB3_149
.LBB3_155:                              #   in Loop: Header=BB3_73 Depth=3
	movb	$3, %al
	jmp	.LBB3_149
.LBB3_157:                              #   in Loop: Header=BB3_73 Depth=3
	movb	$4, %al
	jmp	.LBB3_149
.LBB3_159:                              #   in Loop: Header=BB3_73 Depth=3
	movb	$5, %al
	jmp	.LBB3_149
.LBB3_161:                              #   in Loop: Header=BB3_73 Depth=3
	movb	$6, %al
	jmp	.LBB3_149
.LBB3_163:                              #   in Loop: Header=BB3_73 Depth=3
	movb	$7, %al
	jmp	.LBB3_149
.LBB3_165:                              #   in Loop: Header=BB3_73 Depth=3
	movb	$8, %al
	jmp	.LBB3_149
.LBB3_167:                              #   in Loop: Header=BB3_73 Depth=3
	movb	$9, %al
	jmp	.LBB3_149
.LBB3_169:                              #   in Loop: Header=BB3_73 Depth=3
	movb	$10, %al
	jmp	.LBB3_149
.LBB3_171:                              #   in Loop: Header=BB3_73 Depth=3
	movb	$11, %al
.LBB3_149:                              # %select.unfold.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movl	$1, 12(%rbp)
	movb	%al, 8(%rbp)
.LBB3_187:                              # %stack_push.exit.backedge.i
                                        #   in Loop: Header=BB3_73 Depth=3
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	tree_node_insert_nonbin
.LBB3_80:                               # %stack_push.exit.backedge.i
                                        #   in Loop: Header=BB3_73 Depth=3
	leaq	12(%rbp), %r12
	cmpl	$4, 12(%rbp)
	movq	%rbp, %r13
	movq	64(%rsp), %rbp          # 8-byte Reload
	jne	.LBB3_73
	jmp	.LBB3_196
.LBB3_38:                               #   in Loop: Header=BB3_6 Depth=1
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	setne	%dl
	sete	%cl
	cmpb	$72, %al
	sete	%sil
	cmpb	$77, %al
	sete	%al
	testb	%al, %dl
	jne	.LBB3_198
# BB#39:                                #   in Loop: Header=BB3_6 Depth=1
	andb	%sil, %cl
	je	.LBB3_40
.LBB3_198:                              #   in Loop: Header=BB3_6 Depth=1
	cmpl	$0, 40(%r14)
	je	.LBB3_204
# BB#199:                               #   in Loop: Header=BB3_6 Depth=1
	movq	%rbp, %r13
	movq	(%r14), %rbp
	movq	24(%r14), %rax
	incq	%rax
	movq	%rax, 24(%r14)
	shlq	$4, %rax
	leaq	(%rax,%rax,4), %rsi
	movq	%rbp, %rdi
	callq	cli_realloc
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.LBB3_200
# BB#201:                               #   in Loop: Header=BB3_6 Depth=1
	movq	24(%r14), %rcx
	leaq	(%rcx,%rcx,4), %rcx
	shlq	$4, %rcx
	leaq	-80(%rax,%rcx), %rbp
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, -16(%rax,%rcx)
	movdqu	%xmm0, -32(%rax,%rcx)
	movdqu	%xmm0, -48(%rax,%rcx)
	movdqu	%xmm0, -64(%rax,%rcx)
	movdqu	%xmm0, -80(%rax,%rcx)
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	cli_ac_maxdepth(%rip), %edx
	movzbl	cli_ac_mindepth(%rip), %esi
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rbp, %rdi
	callq	cli_ac_init
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB3_202
# BB#203:                               #   in Loop: Header=BB3_6 Depth=1
	movl	$0, 40(%r14)
	movq	%r13, %rbp
	jmp	.LBB3_205
.LBB3_204:                              #   in Loop: Header=BB3_6 Depth=1
	movq	(%r14), %rax
	movq	24(%r14), %rcx
	leaq	(%rcx,%rcx,4), %rcx
	shlq	$4, %rcx
	leaq	-80(%rax,%rcx), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB3_205:                              #   in Loop: Header=BB3_6 Depth=1
	movl	$1, %edi
	movl	$96, %esi
	callq	cli_calloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB3_206
# BB#207:                               #   in Loop: Header=BB3_6 Depth=1
	movq	%rbp, %rdi
	callq	strlen
	movq	%r13, %rcx
	movq	%rax, %r13
	movw	$0, 74(%rcx)
	movl	$0, 24(%rcx)
	movl	$0, 28(%rcx)
	movb	$0, 72(%rcx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 40(%rcx)
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movw	%r13w, 16(%rcx)
	movzwl	%r13w, %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	movzwl	(%rcx), %ecx
	cmpl	%ecx, %eax
	jbe	.LBB3_209
# BB#208:                               #   in Loop: Header=BB3_6 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movw	%r13w, (%rax)
.LBB3_209:                              #   in Loop: Header=BB3_6 Depth=1
	leaq	(%r13,%r13), %rdi
	callq	cli_malloc
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, (%rcx)
	testq	%rax, %rax
	je	.LBB3_231
# BB#210:                               # %.preheader.i102
                                        #   in Loop: Header=BB3_6 Depth=1
	testq	%r13, %r13
	je	.LBB3_229
# BB#211:                               # %.lr.ph.i103.preheader
                                        #   in Loop: Header=BB3_6 Depth=1
	cmpq	$15, %r13
	jbe	.LBB3_212
# BB#219:                               # %min.iters.checked
                                        #   in Loop: Header=BB3_6 Depth=1
	movq	%r13, %rcx
	andq	$-16, %rcx
	je	.LBB3_212
# BB#220:                               # %vector.memcheck
                                        #   in Loop: Header=BB3_6 Depth=1
	leaq	(%rbp,%r13), %rdx
	cmpq	%rdx, %rax
	jae	.LBB3_222
# BB#221:                               # %vector.memcheck
                                        #   in Loop: Header=BB3_6 Depth=1
	leaq	(%rax,%r13,2), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB3_222
.LBB3_212:                              #   in Loop: Header=BB3_6 Depth=1
	xorl	%ecx, %ecx
.LBB3_213:                              # %.lr.ph.i103.preheader353
                                        #   in Loop: Header=BB3_6 Depth=1
	movl	%r13d, %esi
	subl	%ecx, %esi
	leaq	-1(%r13), %rdx
	subq	%rcx, %rdx
	andq	$3, %rsi
	je	.LBB3_216
# BB#214:                               # %.lr.ph.i103.prol.preheader
                                        #   in Loop: Header=BB3_6 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB3_215:                              # %.lr.ph.i103.prol
                                        #   Parent Loop BB3_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	1(%r12,%rcx), %edi
	movw	%di, (%rax,%rcx,2)
	incq	%rcx
	incq	%rsi
	jne	.LBB3_215
.LBB3_216:                              # %.lr.ph.i103.prol.loopexit
                                        #   in Loop: Header=BB3_6 Depth=1
	cmpq	$3, %rdx
	jb	.LBB3_229
# BB#217:                               # %.lr.ph.i103.preheader353.new
                                        #   in Loop: Header=BB3_6 Depth=1
	subq	%rcx, %r13
	leaq	6(%rax,%rcx,2), %rax
	leaq	4(%r12,%rcx), %rcx
	.p2align	4, 0x90
.LBB3_218:                              # %.lr.ph.i103
                                        #   Parent Loop BB3_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	-3(%rcx), %edx
	movw	%dx, -6(%rax)
	movsbl	-2(%rcx), %edx
	movw	%dx, -4(%rax)
	movsbl	-1(%rcx), %edx
	movw	%dx, -2(%rax)
	movsbl	(%rcx), %edx
	movw	%dx, (%rax)
	addq	$8, %rax
	addq	$4, %rcx
	addq	$-4, %r13
	jne	.LBB3_218
.LBB3_229:                              # %._crit_edge.i104
                                        #   in Loop: Header=BB3_6 Depth=1
	leaq	113(%rsp), %rdi
	callq	cli_strdup
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%rax, 32(%r13)
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r13, %rsi
	callq	cli_ac_addpatt
	movl	%eax, %r12d
	testl	%r12d, %r12d
	movq	56(%rsp), %rbp          # 8-byte Reload
	je	.LBB3_6
	jmp	.LBB3_230
.LBB3_222:                              # %vector.body.preheader
                                        #   in Loop: Header=BB3_6 Depth=1
	leaq	-16(%rcx), %rsi
	movq	%rsi, %rdx
	shrq	$4, %rdx
	btl	$4, %esi
	jb	.LBB3_223
# BB#224:                               # %vector.body.prol
                                        #   in Loop: Header=BB3_6 Depth=1
	movq	1(%r12), %xmm0          # xmm0 = mem[0],zero
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	psraw	$8, %xmm0
	movq	9(%r12), %xmm1          # xmm1 = mem[0],zero
	punpcklbw	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	psraw	$8, %xmm1
	movdqu	%xmm0, (%rax)
	movdqu	%xmm1, 16(%rax)
	movl	$16, %edi
	testq	%rdx, %rdx
	jne	.LBB3_226
	jmp	.LBB3_228
.LBB3_223:                              #   in Loop: Header=BB3_6 Depth=1
	xorl	%edi, %edi
	testq	%rdx, %rdx
	je	.LBB3_228
.LBB3_226:                              # %vector.body.preheader.new
                                        #   in Loop: Header=BB3_6 Depth=1
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	25(%r12,%rdi), %rsi
	leaq	48(%rax,%rdi,2), %rdi
.LBB3_227:                              # %vector.body
                                        #   Parent Loop BB3_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rsi), %xmm0        # xmm0 = mem[0],zero
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	psraw	$8, %xmm0
	movq	-16(%rsi), %xmm1        # xmm1 = mem[0],zero
	punpcklbw	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	psraw	$8, %xmm1
	movdqu	%xmm0, -48(%rdi)
	movdqu	%xmm1, -32(%rdi)
	movq	-8(%rsi), %xmm0         # xmm0 = mem[0],zero
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	psraw	$8, %xmm0
	movq	(%rsi), %xmm1           # xmm1 = mem[0],zero
	punpcklbw	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	psraw	$8, %xmm1
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm1, (%rdi)
	addq	$32, %rsi
	addq	$64, %rdi
	addq	$-32, %rdx
	jne	.LBB3_227
.LBB3_228:                              # %middle.block
                                        #   in Loop: Header=BB3_6 Depth=1
	cmpq	%rcx, %r13
	jne	.LBB3_213
	jmp	.LBB3_229
.LBB3_2:
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-123, %eax
	jmp	.LBB3_239
.LBB3_17:                               # %functionality_level_check.exit.thread117.outer._crit_edge
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	$1, 36(%rbx)
	cmpl	$0, 32(%rbx)
	je	.LBB3_18
# BB#232:
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.LBB3_234
# BB#233:
	movq	24(%rbx), %rcx
	leaq	(%rcx,%rcx,4), %rcx
	shlq	$4, %rcx
	leaq	-80(%rax,%rcx), %rdi
	callq	cli_ac_buildtrie
	testl	%eax, %eax
	jne	.LBB3_239
.LBB3_234:
	movl	$1, 40(%rbx)
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_236
# BB#235:
	callq	free
.LBB3_236:                              # %stack_destroy.exit.i
	xorps	%xmm0, %xmm0
	movq	48(%rsp), %rax          # 8-byte Reload
	movups	%xmm0, (%rax)
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB3_238
# BB#237:
	callq	free
	xorps	%xmm0, %xmm0
.LBB3_238:                              # %regex_list_cleanup.exit
	movups	%xmm0, (%rbp)
	movq	$0, 16(%rbp)
	movq	$1024, 56(%rbx)         # imm = 0x400
	movl	$8192, %edi             # imm = 0x2000
	callq	cli_malloc
	movq	%rax, 48(%rbx)
	movl	$1024, %eax             # imm = 0x400
	movd	%rax, %xmm0
	movdqu	%xmm0, 80(%rbx)
	movl	$8192, %edi             # imm = 0x2000
	callq	cli_malloc
	movq	%rax, 72(%rbx)
	xorl	%eax, %eax
	jmp	.LBB3_239
.LBB3_18:
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-1, %eax
	jmp	.LBB3_239
.LBB3_29:
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_errmsg
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	regex_list_done
	movl	$-1, 32(%rbx)
	movl	$-116, %eax
	jmp	.LBB3_239
.LBB3_7:
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	%r14, %rdi
	callq	regex_list_done
	movl	$-1, 32(%r14)
	movl	%ebx, %eax
	jmp	.LBB3_239
.LBB3_33:
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	movl	44(%rsp), %esi          # 4-byte Reload
	callq	cli_errmsg
	movq	%r14, %rdi
	callq	regex_list_done
	movl	$-1, 32(%r14)
	movl	$-116, %eax
	jmp	.LBB3_239
.LBB3_192:
	movl	%eax, %r15d
.LBB3_174:                              # %.loopexit
	cmpl	$-114, %r15d
.LBB3_175:                              # %functionality_level_check.exit.thread
	sete	%al
	addb	%al, %al
	movzbl	%al, %eax
	orl	$-116, %eax
.LBB3_239:                              # %functionality_level_check.exit.thread
	addq	$8312, %rsp             # imm = 0x2078
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_40:
	movl	$-116, %eax
	jmp	.LBB3_239
.LBB3_206:
	movl	$-114, %eax
	jmp	.LBB3_239
.LBB3_231:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	free
	movl	$-114, %eax
	jmp	.LBB3_239
.LBB3_230:
	movq	32(%r13), %rdi
	callq	free
	movq	(%r13), %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	cmpl	$-114, %r12d
	jmp	.LBB3_175
.LBB3_200:
	movq	%rbp, (%r14)
	movl	$-114, %eax
	jmp	.LBB3_239
.LBB3_202:
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	%ebp, %eax
	jmp	.LBB3_239
.LBB3_173:
	movl	$-114, %r15d
	jmp	.LBB3_174
.Lfunc_end3:
	.size	load_regex_matcher, .Lfunc_end3-load_regex_matcher
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_51
	.quad	.LBB3_50
	.quad	.LBB3_48
	.quad	.LBB3_51
	.quad	.LBB3_58
.LJTI3_1:
	.quad	.LBB3_81
	.quad	.LBB3_177
	.quad	.LBB3_107
	.quad	.LBB3_117
	.quad	.LBB3_138
	.quad	.LBB3_125
	.quad	.LBB3_188
	.quad	.LBB3_188

	.text
	.globl	regex_list_cleanup
	.p2align	4, 0x90
	.type	regex_list_cleanup,@function
regex_list_cleanup:                     # @regex_list_cleanup
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 32
.Lcfi53:
	.cfi_offset %rbx, -24
.Lcfi54:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	48(%rbx), %r14
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_2
# BB#1:
	callq	free
.LBB4_2:                                # %stack_destroy.exit
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_4
# BB#3:
	callq	free
	xorps	%xmm0, %xmm0
.LBB4_4:                                # %stack_destroy.exit4
	movups	%xmm0, 64(%rbx)
	movq	$0, 80(%rbx)
	movq	$1024, 56(%rbx)         # imm = 0x400
	movl	$8192, %edi             # imm = 0x2000
	callq	cli_malloc
	movq	%rax, 48(%rbx)
	movl	$1024, %eax             # imm = 0x400
	movd	%rax, %xmm0
	movdqu	%xmm0, 80(%rbx)
	movl	$8192, %edi             # imm = 0x2000
	callq	cli_malloc
	movq	%rax, 72(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	regex_list_cleanup, .Lfunc_end4-regex_list_cleanup
	.cfi_endproc

	.globl	regex_list_done
	.p2align	4, 0x90
	.type	regex_list_done,@function
regex_list_done:                        # @regex_list_done
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi59:
	.cfi_def_cfa_offset 48
.Lcfi60:
	.cfi_offset %rbx, -40
.Lcfi61:
	.cfi_offset %r12, -32
.Lcfi62:
	.cfi_offset %r14, -24
.Lcfi63:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	48(%r15), %r14
	movq	48(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_2
# BB#1:
	callq	free
.LBB5_2:                                # %stack_destroy.exit.i
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movq	72(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_4
# BB#3:
	callq	free
	xorps	%xmm0, %xmm0
.LBB5_4:                                # %regex_list_cleanup.exit
	movups	%xmm0, 64(%r15)
	movq	$0, 80(%r15)
	movq	$1024, 56(%r15)         # imm = 0x400
	movl	$8192, %edi             # imm = 0x2000
	callq	cli_malloc
	movq	%rax, 48(%r15)
	movl	$1024, %eax             # imm = 0x400
	movd	%rax, %xmm0
	movdqu	%xmm0, 80(%r15)
	movl	$8192, %edi             # imm = 0x2000
	callq	cli_malloc
	movq	%rax, 72(%r15)
	cmpl	$0, 36(%r15)
	je	.LBB5_15
# BB#5:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_10
# BB#6:                                 # %.preheader
	cmpq	$0, 24(%r15)
	je	.LBB5_9
# BB#7:                                 # %.lr.ph.preheader
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	addq	%r12, %rdi
	callq	cli_ac_free
	incq	%rbx
	movq	(%r15), %rdi
	addq	$80, %r12
	cmpq	24(%r15), %rbx
	jb	.LBB5_8
.LBB5_9:                                # %._crit_edge
	callq	free
	movq	$0, (%r15)
.LBB5_10:                               # %._crit_edge20
	movq	$0, 24(%r15)
	movl	$0, 40(%r15)
	movq	$0, 64(%r15)
	movq	8(%r15), %rsi
	movq	%r15, %rdi
	callq	destroy_tree_internal
	movq	16(%r15), %rsi
	movq	%r15, %rdi
	callq	destroy_tree_internal
	movq	64(%r15), %rax
	testq	%rax, %rax
	je	.LBB5_14
# BB#11:                                # %.lr.ph.i
	leaq	64(%r15), %rbx
	.p2align	4, 0x90
.LBB5_12:                               # %stack_pop.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	48(%r15), %rdx
	leaq	-1(%rax), %rcx
	movq	%rcx, 64(%r15)
	movq	-8(%rdx,%rax,8), %rdi
	testq	%rdi, %rdi
	je	.LBB5_13
# BB#22:                                #   in Loop: Header=BB5_12 Depth=1
	callq	free
	movq	(%rbx), %rcx
.LBB5_13:                               # %stack_pop.exit.thread.backedge.i
                                        #   in Loop: Header=BB5_12 Depth=1
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB5_12
.LBB5_14:                               # %destroy_tree.exit
	movl	$0, 36(%r15)
.LBB5_15:
	cmpl	$0, 32(%r15)
	je	.LBB5_17
# BB#16:
	movl	$0, 32(%r15)
.LBB5_17:
	addq	$72, %r15
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB5_19
# BB#18:
	callq	free
.LBB5_19:                               # %stack_destroy.exit
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%r14)
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_21
# BB#20:
	callq	free
	pxor	%xmm0, %xmm0
.LBB5_21:                               # %stack_destroy.exit17
	movdqu	%xmm0, (%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	regex_list_done, .Lfunc_end5-regex_list_done
	.cfi_endproc

	.globl	is_regex_ok
	.p2align	4, 0x90
	.type	is_regex_ok,@function
is_regex_ok:                            # @is_regex_ok
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	$-1, 32(%rdi)
	setne	%al
	retq
.Lfunc_end6:
	.size	is_regex_ok, .Lfunc_end6-is_regex_ok
	.cfi_endproc

	.p2align	4, 0x90
	.type	getNextToken,@function
getNextToken:                           # @getNextToken
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi70:
	.cfi_def_cfa_offset 80
.Lcfi71:
	.cfi_offset %rbx, -56
.Lcfi72:
	.cfi_offset %r12, -48
.Lcfi73:
	.cfi_offset %r13, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movb	(%r15), %al
	addb	$-40, %al
	cmpb	$85, %al
	ja	.LBB7_48
# BB#1:
	movzbl	%al, %eax
	jmpq	*.LJTI7_0(,%rax,8)
.LBB7_7:
	movb	$6, 16(%r14)
	jmp	.LBB7_51
.LBB7_46:
	movb	$2, 16(%r14)
	jmp	.LBB7_51
.LBB7_47:
	movb	$3, 16(%r14)
	jmp	.LBB7_51
.LBB7_48:
	movb	$0, 16(%r14)
	movb	(%r15), %al
.LBB7_49:
	movb	%al, (%r14)
	jmp	.LBB7_50
.LBB7_45:
	movb	$1, 16(%r14)
	jmp	.LBB7_51
.LBB7_8:
	movl	$32, %edi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB7_9
# BB#10:
	leaq	1(%r15), %r12
	cmpb	$94, (%r12)
	jne	.LBB7_12
# BB#11:
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 16(%rbx)
	movdqu	%xmm0, (%rbx)
	leaq	2(%r15), %r12
	jmp	.LBB7_13
.LBB7_2:
	movb	$0, 16(%r14)
	movzbl	1(%r15), %ebx
	incq	%r15
	movb	%bl, (%r14)
	callq	__ctype_b_loc
	movq	(%rax), %rax
	testb	$2, 1(%rax,%rbx,2)
	je	.LBB7_50
# BB#3:
	movb	$0, 14(%rsp)
	movw	$92, 12(%rsp)
	movb	%bl, 13(%rsp)
	leaq	11(%rsp), %rdi
	leaq	12(%rsp), %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	callq	snprintf
	cmpl	$1, %eax
	jne	.LBB7_4
# BB#5:
	movb	11(%rsp), %al
	jmp	.LBB7_49
.LBB7_6:
	movb	$5, 16(%r14)
	jmp	.LBB7_51
.LBB7_9:
	xorl	%r15d, %r15d
	jmp	.LBB7_52
.LBB7_12:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 16(%rbx)
	movdqu	%xmm0, (%rbx)
.LBB7_13:                               # %.preheader116.preheader
	movb	(%r12), %cl
	xorl	%r13d, %r13d
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB7_14:                               # %.preheader116
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_38 Depth 2
                                        #     Child Loop BB7_26 Depth 2
	testl	%r13d, %r13d
	je	.LBB7_29
# BB#15:                                # %.preheader116
                                        #   in Loop: Header=BB7_14 Depth=1
	cmpb	$45, %cl
	jne	.LBB7_29
# BB#16:                                #   in Loop: Header=BB7_14 Depth=1
	movb	1(%r12), %al
	cmpb	$91, %al
	jne	.LBB7_21
# BB#17:                                #   in Loop: Header=BB7_14 Depth=1
	movb	$91, %al
	cmpb	$46, 2(%r12)
	jne	.LBB7_21
# BB#18:                                #   in Loop: Header=BB7_14 Depth=1
	cmpb	$45, 3(%r12)
	jne	.LBB7_27
# BB#19:                                #   in Loop: Header=BB7_14 Depth=1
	cmpb	$46, 4(%r12)
	jne	.LBB7_27
# BB#20:                                #   in Loop: Header=BB7_14 Depth=1
	movb	$45, %al
	cmpb	$93, 5(%r12)
	jne	.LBB7_27
.LBB7_21:                               #   in Loop: Header=BB7_14 Depth=1
	incq	%r12
	movzbl	%r8b, %esi
	leal	1(%rsi), %edx
	movzbl	%al, %eax
	xorl	%r13d, %r13d
	cmpl	%eax, %edx
	ja	.LBB7_42
# BB#22:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB7_14 Depth=1
	movl	%eax, %ecx
	subl	%esi, %ecx
	leal	-1(%rax), %edi
	testb	$1, %cl
	je	.LBB7_24
# BB#23:                                # %.lr.ph.prol
                                        #   in Loop: Header=BB7_14 Depth=1
	movl	%edx, %ecx
	andb	$7, %cl
	movl	$1, %ebp
	shll	%cl, %ebp
	shrl	$3, %edx
	movzbl	(%rbx,%rdx), %ecx
	xorl	%ebp, %ecx
	movb	%cl, (%rbx,%rdx)
	leal	2(%rsi), %edx
.LBB7_24:                               # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB7_14 Depth=1
	cmpl	%esi, %edi
	je	.LBB7_42
# BB#25:                                # %.lr.ph.preheader.new
                                        #   in Loop: Header=BB7_14 Depth=1
	incl	%eax
	.p2align	4, 0x90
.LBB7_26:                               # %.lr.ph
                                        #   Parent Loop BB7_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %ecx
	andb	$7, %cl
	movl	$1, %esi
	shll	%cl, %esi
	movl	%edx, %ecx
	shrl	$3, %ecx
	movzbl	(%rbx,%rcx), %edi
	xorl	%esi, %edi
	movb	%dil, (%rbx,%rcx)
	leal	1(%rdx), %esi
	movl	%esi, %ecx
	andb	$7, %cl
	movl	$1, %edi
	shll	%cl, %edi
	shrl	$3, %esi
	movzbl	(%rbx,%rsi), %ecx
	xorl	%edi, %ecx
	movb	%cl, (%rbx,%rsi)
	addl	$2, %edx
	cmpl	%edx, %eax
	jne	.LBB7_26
	jmp	.LBB7_42
	.p2align	4, 0x90
.LBB7_29:                               #   in Loop: Header=BB7_14 Depth=1
	leaq	1(%r12), %rax
	cmpb	$91, %cl
	jne	.LBB7_41
# BB#30:                                #   in Loop: Header=BB7_14 Depth=1
	cmpb	$58, (%rax)
	jne	.LBB7_41
# BB#31:                                #   in Loop: Header=BB7_14 Depth=1
	movq	%r8, 16(%rsp)           # 8-byte Spill
	addq	$2, %r12
	movl	$.L.str.21, %esi
	movq	%r12, %rdi
	callq	strstr
	testq	%rax, %rax
	je	.LBB7_32
# BB#33:                                #   in Loop: Header=BB7_14 Depth=1
	subl	%r12d, %eax
	movslq	%eax, %rbp
	movl	$.L.str.24, %esi
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB7_34
# BB#35:                                #   in Loop: Header=BB7_14 Depth=1
	movl	$.L.str.25, %esi
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB7_36
# BB#53:                                #   in Loop: Header=BB7_14 Depth=1
	movl	$.L.str.26, %esi
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB7_54
# BB#55:                                #   in Loop: Header=BB7_14 Depth=1
	movl	$.L.str.27, %esi
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB7_56
# BB#57:                                #   in Loop: Header=BB7_14 Depth=1
	movl	$.L.str.28, %esi
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB7_58
# BB#59:                                #   in Loop: Header=BB7_14 Depth=1
	movl	$.L.str.29, %esi
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB7_60
# BB#61:                                #   in Loop: Header=BB7_14 Depth=1
	movl	$.L.str.30, %esi
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB7_62
# BB#63:                                #   in Loop: Header=BB7_14 Depth=1
	movl	$.L.str.31, %esi
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB7_64
# BB#65:                                #   in Loop: Header=BB7_14 Depth=1
	movl	$.L.str.32, %esi
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB7_66
# BB#67:                                #   in Loop: Header=BB7_14 Depth=1
	movl	$.L.str.33, %esi
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB7_68
# BB#69:                                #   in Loop: Header=BB7_14 Depth=1
	movl	$.L.str.34, %esi
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB7_70
# BB#71:                                #   in Loop: Header=BB7_14 Depth=1
	movl	$.L.str.35, %esi
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	strncmp
	movl	$11, %ecx
	testl	%eax, %eax
	je	.LBB7_37
	jmp	.LBB7_72
	.p2align	4, 0x90
.LBB7_41:                               # %._crit_edge
                                        #   in Loop: Header=BB7_14 Depth=1
	movzbl	%cl, %edx
	andb	$7, %cl
	movl	$1, %r13d
	movl	$1, %esi
	shll	%cl, %esi
	shrq	$3, %rdx
	movzbl	(%rbx,%rdx), %ecx
	xorl	%esi, %ecx
	movb	%cl, (%rbx,%rdx)
	movb	(%rax), %r8b
	movq	%rax, %r12
	jmp	.LBB7_42
.LBB7_34:                               #   in Loop: Header=BB7_14 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB7_37
.LBB7_36:                               #   in Loop: Header=BB7_14 Depth=1
	movl	$1, %ecx
	jmp	.LBB7_37
.LBB7_54:                               #   in Loop: Header=BB7_14 Depth=1
	movl	$2, %ecx
	jmp	.LBB7_37
.LBB7_56:                               #   in Loop: Header=BB7_14 Depth=1
	movl	$3, %ecx
	jmp	.LBB7_37
.LBB7_58:                               #   in Loop: Header=BB7_14 Depth=1
	movl	$4, %ecx
	jmp	.LBB7_37
.LBB7_60:                               #   in Loop: Header=BB7_14 Depth=1
	movl	$5, %ecx
	jmp	.LBB7_37
.LBB7_62:                               #   in Loop: Header=BB7_14 Depth=1
	movl	$6, %ecx
	jmp	.LBB7_37
.LBB7_64:                               #   in Loop: Header=BB7_14 Depth=1
	movl	$7, %ecx
	jmp	.LBB7_37
.LBB7_66:                               #   in Loop: Header=BB7_14 Depth=1
	movl	$8, %ecx
	jmp	.LBB7_37
.LBB7_68:                               #   in Loop: Header=BB7_14 Depth=1
	movl	$9, %ecx
	jmp	.LBB7_37
.LBB7_70:                               #   in Loop: Header=BB7_14 Depth=1
	movl	$10, %ecx
.LBB7_37:                               # %.preheader
                                        #   in Loop: Header=BB7_14 Depth=1
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	xorl	%edx, %edx
	movq	16(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB7_38:                               #   Parent Loop BB7_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	char_class(%rdx,%rdx), %ecx
	testl	%eax, %ecx
	je	.LBB7_40
# BB#39:                                #   in Loop: Header=BB7_38 Depth=2
	movl	%edx, %ecx
	andb	$7, %cl
	movl	$1, %esi
	shll	%cl, %esi
	movq	%rdx, %rcx
	shrq	$3, %rcx
	movzbl	(%rbx,%rcx), %edi
	xorl	%esi, %edi
	movb	%dil, (%rbx,%rcx)
.LBB7_40:                               #   in Loop: Header=BB7_38 Depth=2
	incq	%rdx
	cmpq	$256, %rdx              # imm = 0x100
	jne	.LBB7_38
	.p2align	4, 0x90
.LBB7_42:                               # %.thread
                                        #   in Loop: Header=BB7_14 Depth=1
	movb	(%r12), %cl
	cmpb	$93, %cl
	jne	.LBB7_14
# BB#43:                                # %.loopexit.loopexit
	movq	%r12, %r15
	jmp	.LBB7_44
.LBB7_4:
	movb	$6, 16(%r14)
	movq	%r15, (%r14)
.LBB7_50:
	movq	$1, 8(%r14)
	jmp	.LBB7_51
.LBB7_27:
	movl	$.L.str.20, %edi
	jmp	.LBB7_28
.LBB7_32:
	movl	$.L.str.22, %edi
.LBB7_28:                               # %.loopexit
	xorl	%eax, %eax
	callq	cli_warnmsg
	movb	$6, 16(%r14)
.LBB7_44:                               # %.loopexit
	movb	$4, 16(%r14)
	movq	%rbx, (%r14)
.LBB7_51:
	incq	%r15
.LBB7_52:                               # %.thread114
	movq	%r15, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_72:                               # %.thread112
	movl	$.L.str.23, %edi
	jmp	.LBB7_28
.Lfunc_end7:
	.size	getNextToken, .Lfunc_end7-getNextToken
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_46
	.quad	.LBB7_47
	.quad	.LBB7_7
	.quad	.LBB7_7
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_45
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_7
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_8
	.quad	.LBB7_2
	.quad	.LBB7_51
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_48
	.quad	.LBB7_7
	.quad	.LBB7_6
	.quad	.LBB7_7

	.text
	.p2align	4, 0x90
	.type	tree_node_insert_nonbin,@function
tree_node_insert_nonbin:                # @tree_node_insert_nonbin
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 32
.Lcfi80:
	.cfi_offset %rbx, -32
.Lcfi81:
	.cfi_offset %r14, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	12(%rbx), %ebp
	movq	24(%rbx), %rdi
	cmpl	$2, %ebp
	movq	%rdi, %rax
	jne	.LBB8_2
# BB#1:
	leaq	8(%rdi), %rax
	movq	8(%rdi), %rcx
	testq	%rcx, %rcx
	cmoveq	%rcx, %rax
.LBB8_2:                                # %tree_node_get_children.exit
	movsbq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB8_10
# BB#3:
	movq	(%rax), %rdx
	movq	(%rdx), %rsi
	cmpq	%rbx, %rsi
	je	.LBB8_4
	.p2align	4, 0x90
.LBB8_8:                                # %.preheader53
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rax
	movq	(%rax), %rsi
	cmpq	%rbx, %rsi
	jne	.LBB8_8
# BB#9:
	movb	$1, 17(%r14)
	movb	$0, 17(%rax)
	movq	%r14, (%rax)
	jmp	.LBB8_17
.LBB8_10:
	testq	%rdi, %rdi
	je	.LBB8_15
# BB#11:
	xorl	%eax, %eax
	cmpl	$2, %ebp
	sete	%al
	movq	(%rdi,%rax,8), %rax
	testq	%rax, %rax
	je	.LBB8_15
	.p2align	4, 0x90
.LBB8_12:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.LBB8_14
# BB#13:                                #   in Loop: Header=BB8_12 Depth=1
	cmpb	$0, 17(%rcx)
	je	.LBB8_12
.LBB8_14:                               # %.critedge
	movb	$0, 17(%rcx)
	movq	%rax, (%r14)
	movq	%r14, (%rcx)
	movb	$1, 17(%r14)
	jmp	.LBB8_17
.LBB8_4:
	movb	$1, 17(%r14)
	testb	%cl, %cl
	jle	.LBB8_17
# BB#5:                                 # %.lr.ph
	movq	%r14, (%rdx)
	movq	(%rax), %rdx
	movb	$0, 17(%rdx)
	cmpb	$1, %cl
	je	.LBB8_17
# BB#6:                                 # %._crit_edge.preheader
	movl	$1, %edx
	.p2align	4, 0x90
.LBB8_7:                                # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rdx,8), %rsi
	movq	%r14, (%rsi)
	movq	(%rax,%rdx,8), %rsi
	movb	$0, 17(%rsi)
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB8_7
	jmp	.LBB8_17
.LBB8_15:
	movl	$16, %esi
	callq	cli_realloc2
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.LBB8_17
# BB#16:
	xorl	%ecx, %ecx
	cmpl	$2, %ebp
	sete	%cl
	movq	%r14, (%rax,%rcx,8)
.LBB8_17:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end8:
	.size	tree_node_insert_nonbin, .Lfunc_end8-tree_node_insert_nonbin
	.cfi_endproc

	.p2align	4, 0x90
	.type	destroy_tree_internal,@function
destroy_tree_internal:                  # @destroy_tree_internal
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 48
.Lcfi88:
	.cfi_offset %rbx, -48
.Lcfi89:
	.cfi_offset %r12, -40
.Lcfi90:
	.cfi_offset %r13, -32
.Lcfi91:
	.cfi_offset %r14, -24
.Lcfi92:
	.cfi_offset %r15, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	movl	12(%r13), %eax
	movq	24(%r13), %r12
	cmpl	$4, %eax
	je	.LBB9_3
# BB#1:
	cmpl	$2, %eax
	jne	.LBB9_32
# BB#2:                                 # %tree_node_get_children.exit.thread
	movq	8(%r12), %rax
	addq	$8, %r12
	testq	%rax, %rax
	cmoveq	%rax, %r12
	jmp	.LBB9_32
.LBB9_3:
	movq	(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB9_4
# BB#5:
	cmpb	$0, 17(%r13)
	movq	%r12, %r15
	jne	.LBB9_7
# BB#6:
	movq	%r14, %rdi
	callq	destroy_tree_internal
	movq	24(%r13), %r15
	jmp	.LBB9_7
.LBB9_4:
	movq	%r12, %r15
.LBB9_7:
	movq	64(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB9_8
# BB#12:                                # %.lr.ph.i57
	movq	48(%r14), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_13:                               # =>This Inner Loop Header: Depth=1
	cmpq	%r15, (%rax,%rdx,8)
	je	.LBB9_14
# BB#11:                                #   in Loop: Header=BB9_13 Depth=1
	incq	%rdx
	cmpq	%rcx, %rdx
	jb	.LBB9_13
	jmp	.LBB9_9
.LBB9_8:
	xorl	%ecx, %ecx
.LBB9_9:                                # %._crit_edge.i60
	cmpq	56(%r14), %rcx
	jne	.LBB9_10
# BB#15:
	leaq	4096(%rcx), %rax
	movq	%rax, 56(%r14)
	movq	48(%r14), %rdi
	leaq	32768(,%rcx,8), %rsi
	callq	cli_realloc2
	movq	%rax, 48(%r14)
	testq	%rax, %rax
	je	.LBB9_16
# BB#17:                                # %._crit_edge1.i.i65
	movq	64(%r14), %rcx
	jmp	.LBB9_18
.LBB9_10:                               # %._crit_edge.i.i63
	movq	48(%r14), %rax
.LBB9_18:
	leaq	1(%rcx), %rdx
	movq	%rdx, 64(%r14)
	movq	%r15, (%rax,%rcx,8)
	movq	%rdx, %rcx
	testq	%rcx, %rcx
	jne	.LBB9_14
	jmp	.LBB9_20
.LBB9_16:                               # %.stack_push_once.exit66_crit_edge
	movq	64(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB9_20
.LBB9_14:                               # %.lr.ph.i67
	movq	48(%r14), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_24:                               # =>This Inner Loop Header: Depth=1
	cmpq	%r13, (%rax,%rdx,8)
	je	.LBB9_28
# BB#23:                                #   in Loop: Header=BB9_24 Depth=1
	incq	%rdx
	cmpq	%rcx, %rdx
	jb	.LBB9_24
	jmp	.LBB9_21
.LBB9_20:
	xorl	%ecx, %ecx
.LBB9_21:                               # %._crit_edge.i70
	cmpq	56(%r14), %rcx
	jne	.LBB9_22
# BB#25:
	leaq	4096(%rcx), %rax
	movq	%rax, 56(%r14)
	movq	48(%r14), %rdi
	leaq	32768(,%rcx,8), %rsi
	callq	cli_realloc2
	movq	%rax, 48(%r14)
	testq	%rax, %rax
	je	.LBB9_28
# BB#26:                                # %._crit_edge1.i.i75
	movq	64(%r14), %rcx
	jmp	.LBB9_27
.LBB9_22:                               # %._crit_edge.i.i73
	movq	48(%r14), %rax
.LBB9_27:
	leaq	1(%rcx), %rdx
	movq	%rdx, 64(%r14)
	movq	%r13, (%rax,%rcx,8)
.LBB9_28:                               # %stack_push_once.exit76
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB9_30
# BB#29:
	callq	cli_regfree
	movq	8(%r12), %rdi
	callq	free
	movq	$0, 8(%r12)
.LBB9_30:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB9_32
# BB#31:
	callq	free
	movq	$0, (%r12)
.LBB9_32:
	movb	16(%r13), %al
	testb	%al, %al
	je	.LBB9_43
# BB#33:
	movq	(%r12), %rsi
	cmpl	$4, 12(%rsi)
	jne	.LBB9_35
# BB#34:
	xorl	%r15d, %r15d
	testb	%al, %al
	jg	.LBB9_37
	jmp	.LBB9_40
.LBB9_43:
	testq	%r12, %r12
	je	.LBB9_47
# BB#44:
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	je	.LBB9_47
# BB#45:
	movq	%r14, %rdi
	jmp	.LBB9_46
.LBB9_35:
	movq	(%rsi), %r15
	testb	%al, %al
	jle	.LBB9_40
.LBB9_37:                               # %.lr.ph.preheader
	movq	%r14, %rdi
	callq	destroy_tree_internal
	cmpb	$2, 16(%r13)
	jl	.LBB9_40
# BB#38:                                # %.lr.ph..lr.ph_crit_edge.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB9_39:                               # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbx,8), %rsi
	movq	%r14, %rdi
	callq	destroy_tree_internal
	incq	%rbx
	movsbq	16(%r13), %rax
	cmpq	%rax, %rbx
	jl	.LBB9_39
.LBB9_40:                               # %._crit_edge
	testq	%r15, %r15
	je	.LBB9_47
# BB#41:                                # %._crit_edge
	cmpq	%r13, %r15
	je	.LBB9_47
# BB#42:
	movq	%r14, %rdi
	movq	%r15, %rsi
.LBB9_46:
	callq	destroy_tree_internal
.LBB9_47:
	cmpl	$4, 12(%r13)
	je	.LBB9_51
# BB#48:
	movq	(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB9_51
# BB#49:
	cmpb	$0, 17(%r13)
	jne	.LBB9_51
# BB#50:
	movq	%r14, %rdi
	callq	destroy_tree_internal
.LBB9_51:
	movq	24(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB9_62
# BB#52:
	movq	64(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB9_53
# BB#56:                                # %.lr.ph.i77
	movq	48(%r14), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_57:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rbx, (%rax,%rdx,8)
	je	.LBB9_62
# BB#58:                                #   in Loop: Header=BB9_57 Depth=1
	incq	%rdx
	cmpq	%rcx, %rdx
	jb	.LBB9_57
	jmp	.LBB9_54
.LBB9_53:
	xorl	%ecx, %ecx
.LBB9_54:                               # %._crit_edge.i80
	cmpq	56(%r14), %rcx
	jne	.LBB9_55
# BB#59:
	leaq	4096(%rcx), %rax
	movq	%rax, 56(%r14)
	movq	48(%r14), %rdi
	leaq	32768(,%rcx,8), %rsi
	callq	cli_realloc2
	movq	%rax, 48(%r14)
	testq	%rax, %rax
	je	.LBB9_62
# BB#60:                                # %._crit_edge1.i.i85
	movq	64(%r14), %rcx
	jmp	.LBB9_61
.LBB9_55:                               # %._crit_edge.i.i83
	movq	48(%r14), %rax
.LBB9_61:
	leaq	1(%rcx), %rdx
	movq	%rdx, 64(%r14)
	movq	%rbx, (%rax,%rcx,8)
.LBB9_62:                               # %stack_push_once.exit86
	cmpl	$2, 12(%r13)
	jne	.LBB9_65
# BB#63:
	movq	24(%r13), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB9_65
# BB#64:
	callq	free
	movq	24(%r13), %rax
	movq	$0, (%rax)
.LBB9_65:
	movq	64(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB9_66
# BB#69:                                # %.lr.ph.i
	movq	48(%r14), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_70:                               # =>This Inner Loop Header: Depth=1
	cmpq	%r13, (%rax,%rdx,8)
	je	.LBB9_75
# BB#71:                                #   in Loop: Header=BB9_70 Depth=1
	incq	%rdx
	cmpq	%rcx, %rdx
	jb	.LBB9_70
	jmp	.LBB9_67
.LBB9_66:
	xorl	%ecx, %ecx
.LBB9_67:                               # %._crit_edge.i
	cmpq	56(%r14), %rcx
	jne	.LBB9_68
# BB#72:
	leaq	4096(%rcx), %rax
	movq	%rax, 56(%r14)
	movq	48(%r14), %rdi
	leaq	32768(,%rcx,8), %rsi
	callq	cli_realloc2
	movq	%rax, 48(%r14)
	testq	%rax, %rax
	je	.LBB9_75
# BB#73:                                # %._crit_edge1.i.i
	movq	64(%r14), %rcx
	jmp	.LBB9_74
.LBB9_68:                               # %._crit_edge.i.i
	movq	48(%r14), %rax
.LBB9_74:
	leaq	1(%rcx), %rdx
	movq	%rdx, 64(%r14)
	movq	%r13, (%rax,%rcx,8)
.LBB9_75:                               # %stack_push_once.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	destroy_tree_internal, .Lfunc_end9-destroy_tree_internal
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Looking up in regex_list: %s\n"
	.size	.L.str, 30

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Got a match: %s with %s\n"
	.size	.L.str.1, 25

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Before inserting .: %s\n"
	.size	.L.str.2, 24

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"After inserting .: %s\n"
	.size	.L.str.3, 23

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Ignoring false match: %s with %s,%c\n"
	.size	.L.str.4, 37

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Lookup result: not in regex list\n"
	.size	.L.str.5, 34

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Lookup result: in regex list\n"
	.size	.L.str.6, 30

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Unable to load regex list (null file)\n"
	.size	.L.str.7, 39

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Loading regex_list\n"
	.size	.L.str.8, 20

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Regex list failed to initialize!\n"
	.size	.L.str.9, 34

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Malformed regex list line %d\n"
	.size	.L.str.10, 30

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Overlong regex line %d\n"
	.size	.L.str.11, 24

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"regex_list: Initialising AC pattern matcher\n"
	.size	.L.str.12, 45

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"regex_list: Can't initialise AC pattern matcher\n"
	.size	.L.str.13, 49

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"calc_pos_with_skip: skip:%lu, %lu - %lu \"%s\",\"%s\"\n"
	.size	.L.str.15, 51

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"calc_pos_with_skip:%s\n"
	.size	.L.str.16, 23

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"regex list line %s not loaded (required f-level: %u)\n"
	.size	.L.str.17, 54

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Regex list not loaded!\n"
	.size	.L.str.18, 24

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Building regex list\n"
	.size	.L.str.19, 21

	.type	.LgetNextToken.fmt,@object # @getNextToken.fmt
	.section	.rodata,"a",@progbits
.LgetNextToken.fmt:
	.asciz	"\\\000"
	.size	.LgetNextToken.fmt, 3

	.type	.L.str.20,@object       # @.str.20
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.20:
	.asciz	"confused about collating sequences in regex,bailing out"
	.size	.L.str.20, 56

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	":]"
	.size	.L.str.21, 3

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"confused about std char class syntax regex,bailing out"
	.size	.L.str.22, 55

	.type	char_class,@object      # @char_class
	.section	.rodata,"a",@progbits
	.p2align	4
char_class:
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	608                     # 0x260
	.short	544                     # 0x220
	.short	544                     # 0x220
	.short	544                     # 0x220
	.short	544                     # 0x220
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	512                     # 0x200
	.short	1120                    # 0x460
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	3091                    # 0xc13
	.short	3091                    # 0xc13
	.short	3091                    # 0xc13
	.short	3091                    # 0xc13
	.short	3091                    # 0xc13
	.short	3091                    # 0xc13
	.short	3091                    # 0xc13
	.short	3091                    # 0xc13
	.short	3091                    # 0xc13
	.short	3091                    # 0xc13
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	3353                    # 0xd19
	.short	3353                    # 0xd19
	.short	3353                    # 0xd19
	.short	3353                    # 0xd19
	.short	3353                    # 0xd19
	.short	3353                    # 0xd19
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1305                    # 0x519
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	3225                    # 0xc99
	.short	3225                    # 0xc99
	.short	3225                    # 0xc99
	.short	3225                    # 0xc99
	.short	3225                    # 0xc99
	.short	3225                    # 0xc99
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1177                    # 0x499
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	1044                    # 0x414
	.short	512                     # 0x200
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.size	char_class, 512

	.type	.L.str.23,@object       # @.str.23
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.23:
	.asciz	"confused about regex bracket expression, bailing out"
	.size	.L.str.23, 53

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"[:alnum:]"
	.size	.L.str.24, 10

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"[:digit:]"
	.size	.L.str.25, 10

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"[:punct:]"
	.size	.L.str.26, 10

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"[:alpha:]"
	.size	.L.str.27, 10

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"[:graph:]"
	.size	.L.str.28, 10

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"[:space:]"
	.size	.L.str.29, 10

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"[:blank:]"
	.size	.L.str.30, 10

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"[:lower:]"
	.size	.L.str.31, 10

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"[:upper:]"
	.size	.L.str.32, 10

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"[:cntrl:]"
	.size	.L.str.33, 10

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"[:print:]"
	.size	.L.str.34, 10

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"[:xdigit:]"
	.size	.L.str.35, 11

	.type	char_class_bitmap,@object # @char_class_bitmap
	.section	.rodata,"a",@progbits
	.p2align	4
char_class_bitmap:
	.asciz	"\000\000\000\000\000\000\377\003\376\377\377\007\376\377\377\007\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\000\000\000\000\000\000\377\003\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\000\000\000\000\376\377\000\374\001\000\000\370\001\000\000x\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\000\000\000\000\000\000\000\000\376\377\377\007\376\377\377\007\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\000\000\000\000\376\377\377\377\377\377\377\377\377\377\377\177\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\000>\000\000\001\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\000\002\000\000\001\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\000\000\000\000\000\000\000\000\000\000\000\000\376\377\377\007\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\000\000\000\000\000\000\000\000\376\377\377\007\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\377\377\377\377\000\000\000\000\000\000\000\000\000\000\000\200\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\000\000\000\000\377\377\377\377\377\377\377\377\377\377\377\177\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"\000\000\000\000\000\000\377\003~\000\000\000~\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.size	char_class_bitmap, 384

	.type	.L.str.36,@object       # @.str.36
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.36:
	.asciz	"Encountered invalid operator in tree:%d\n"
	.size	.L.str.36, 41


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
