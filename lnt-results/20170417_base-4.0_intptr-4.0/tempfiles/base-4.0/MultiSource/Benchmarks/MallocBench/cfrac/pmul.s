	.text
	.file	"pmul.bc"
	.globl	pmul
	.p2align	4, 0x90
	.type	pmul,@function
pmul:                                   # @pmul
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbp
	testq	%rbp, %rbp
	je	.LBB0_2
# BB#1:
	incw	(%rbp)
.LBB0_2:
	testq	%r14, %r14
	je	.LBB0_4
# BB#3:
	incw	(%r14)
.LBB0_4:
	movq	%rbp, %rdi
	callq	pcmpz
	testl	%eax, %eax
	je	.LBB0_6
# BB#5:
	movq	%r14, %rdi
	callq	pcmpz
	testl	%eax, %eax
	je	.LBB0_6
# BB#8:
	movzwl	4(%rbp), %eax
	cmpw	4(%r14), %ax
	movq	%r14, %r15
	cmovbq	%rbp, %r15
	cmovbq	%r14, %rbp
	movzwl	4(%rbp), %eax
	movzwl	4(%r15), %edi
	addl	%eax, %edi
	xorl	%eax, %eax
	callq	palloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_28
# BB#9:
	movb	6(%rbp), %al
	cmpb	6(%r15), %al
	setne	6(%rbx)
	leaq	8(%rbx), %rax
	movzwl	4(%rbp), %ecx
	leaq	8(%rbx,%rcx,2), %rcx
	.p2align	4, 0x90
.LBB0_10:                               # =>This Inner Loop Header: Depth=1
	movw	$0, -2(%rcx)
	addq	$-2, %rcx
	cmpq	%rax, %rcx
	ja	.LBB0_10
# BB#11:
	leaq	8(%r15), %r10
	leaq	8(%rbp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movzwl	4(%rbp), %eax
	leaq	8(%rbp,%rax,2), %rdx
	leaq	10(%rbp), %rax
	cmpq	%rax, %rdx
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmovaq	%rdx, %rax
	movq	$-9, %r13
	movq	%rbp, (%rsp)            # 8-byte Spill
	subq	%rbp, %r13
	addq	%rax, %r13
	shrq	%r13
	leaq	10(%rbx,%r13,2), %r11
	movl	%r13d, %r14d
	andl	$1, %r14d
	movq	%r10, %rax
	movq	%rbx, %r9
	.p2align	4, 0x90
.LBB0_12:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_16 Depth 2
	movq	%rax, %rsi
	subq	%r10, %rsi
	leaq	8(%rbx,%rsi), %rcx
	movzwl	(%rax), %ebp
	xorl	%edi, %edi
	testq	%r14, %r14
	jne	.LBB0_13
# BB#14:                                #   in Loop: Header=BB0_12 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movzwl	(%rdi), %ebx
	imull	%ebp, %ebx
	movzwl	(%rcx), %edi
	addl	%ebx, %edi
	movw	%di, (%rcx)
	shrl	$16, %edi
	addq	$2, %rcx
	movq	16(%rsp), %r8           # 8-byte Reload
	jmp	.LBB0_15
	.p2align	4, 0x90
.LBB0_13:                               #   in Loop: Header=BB0_12 Depth=1
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB0_15:                               # %.prol.loopexit
                                        #   in Loop: Header=BB0_12 Depth=1
	sarq	%rsi
	testq	%r13, %r13
	je	.LBB0_17
	.p2align	4, 0x90
.LBB0_16:                               #   Parent Loop BB0_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%r8), %ebx
	imull	%ebp, %ebx
	movzwl	(%rcx), %r12d
	addl	%edi, %r12d
	addl	%ebx, %r12d
	movw	%r12w, (%rcx)
	shrl	$16, %r12d
	movzwl	2(%r8), %edi
	imull	%ebp, %edi
	movzwl	2(%rcx), %ebx
	addl	%r12d, %ebx
	addl	%edi, %ebx
	movw	%bx, 2(%rcx)
	movl	%ebx, %edi
	shrl	$16, %edi
	addq	$4, %r8
	addq	$4, %rcx
	cmpq	%rdx, %r8
	jb	.LBB0_16
.LBB0_17:                               #   in Loop: Header=BB0_12 Depth=1
	movw	%di, (%r11,%rsi,2)
	addq	$2, %rax
	movzwl	4(%r15), %ecx
	leaq	8(%r15,%rcx,2), %rcx
	cmpq	%rcx, %rax
	movq	%r9, %rbx
	jb	.LBB0_12
# BB#18:
	testl	%edi, %edi
	jne	.LBB0_20
# BB#19:
	decw	4(%rbx)
.LBB0_20:
	movq	%r15, %r14
	movq	(%rsp), %rbp            # 8-byte Reload
	testq	%rbp, %rbp
	jne	.LBB0_22
	jmp	.LBB0_24
.LBB0_6:
	movl	$1, %edi
	xorl	%eax, %eax
	callq	palloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_28
# BB#7:
	movb	$0, 6(%rbx)
	movw	$0, 8(%rbx)
	testq	%rbp, %rbp
	je	.LBB0_24
.LBB0_22:
	decw	(%rbp)
	jne	.LBB0_24
# BB#23:
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	pfree
.LBB0_24:
	testq	%r14, %r14
	je	.LBB0_27
# BB#25:
	decw	(%r14)
	jne	.LBB0_27
# BB#26:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	pfree
.LBB0_27:
	movq	%rbx, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	presult                 # TAILCALL
.LBB0_28:
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	pmul, .Lfunc_end0-pmul
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
