	.text
	.file	"vector.bc"
	.globl	V2_cprod
	.p2align	4, 0x90
	.type	V2_cprod,@function
V2_cprod:                               # @V2_cprod
	.cfi_startproc
# BB#0:
	movsd	40(%rsp), %xmm0         # xmm0 = mem[0],zero
	movhpd	32(%rsp), %xmm0         # xmm0 = xmm0[0],mem[0]
	mulpd	8(%rsp), %xmm0
	movapd	%xmm0, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	subsd	%xmm1, %xmm0
	retq
.Lfunc_end0:
	.size	V2_cprod, .Lfunc_end0-V2_cprod
	.cfi_endproc

	.globl	V2_dot
	.p2align	4, 0x90
	.type	V2_dot,@function
V2_dot:                                 # @V2_dot
	.cfi_startproc
# BB#0:
	movupd	32(%rsp), %xmm1
	mulpd	8(%rsp), %xmm1
	movapd	%xmm1, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	addsd	%xmm1, %xmm0
	retq
.Lfunc_end1:
	.size	V2_dot, .Lfunc_end1-V2_dot
	.cfi_endproc

	.globl	V2_times
	.p2align	4, 0x90
	.type	V2_times,@function
V2_times:                               # @V2_times
	.cfi_startproc
# BB#0:
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	8(%rsp), %xmm0
	movupd	%xmm0, (%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end2:
	.size	V2_times, .Lfunc_end2-V2_times
	.cfi_endproc

	.globl	V2_sum
	.p2align	4, 0x90
	.type	V2_sum,@function
V2_sum:                                 # @V2_sum
	.cfi_startproc
# BB#0:
	movupd	32(%rsp), %xmm0
	addpd	8(%rsp), %xmm0
	movupd	%xmm0, (%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end3:
	.size	V2_sum, .Lfunc_end3-V2_sum
	.cfi_endproc

	.globl	V2_sub
	.p2align	4, 0x90
	.type	V2_sub,@function
V2_sub:                                 # @V2_sub
	.cfi_startproc
# BB#0:
	movapd	8(%rsp), %xmm0
	movupd	32(%rsp), %xmm1
	subpd	%xmm1, %xmm0
	movupd	%xmm0, (%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end4:
	.size	V2_sub, .Lfunc_end4-V2_sub
	.cfi_endproc

	.globl	V2_magn
	.p2align	4, 0x90
	.type	V2_magn,@function
V2_magn:                                # @V2_magn
	.cfi_startproc
# BB#0:
	movsd	8(%rsp), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rsp), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jp	.LBB5_2
# BB#1:                                 # %.split
	movapd	%xmm1, %xmm0
	retq
.LBB5_2:                                # %call.sqrt
	jmp	sqrt                    # TAILCALL
.Lfunc_end5:
	.size	V2_magn, .Lfunc_end5-V2_magn
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	V2_cross
	.p2align	4, 0x90
	.type	V2_cross,@function
V2_cross:                               # @V2_cross
	.cfi_startproc
# BB#0:
	movq	16(%rsp), %rax
	movsd	8(%rsp), %xmm0          # xmm0 = mem[0],zero
	xorps	.LCPI6_0(%rip), %xmm0
	movq	%rax, (%rdi)
	movlps	%xmm0, 8(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end6:
	.size	V2_cross, .Lfunc_end6-V2_cross
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
