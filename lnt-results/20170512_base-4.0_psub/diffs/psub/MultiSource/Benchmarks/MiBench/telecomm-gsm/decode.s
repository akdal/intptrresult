	.text
	.file	"decode.bc"
	.globl	Gsm_Decoder
	.p2align	4, 0x90
	.type	Gsm_Decoder,@function
Gsm_Decoder:                            # @Gsm_Decoder
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$456, %rsp              # imm = 0x1C8
.Lcfi6:
	.cfi_def_cfa_offset 512
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%rcx, %r12
	movq	%rdx, %rbx
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%rdi, %r14
	movq	520(%rsp), %r15
	movq	512(%rsp), %rcx
	leaq	240(%r14), %rbp
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movswl	(%r8), %edx
	movswl	(%r9), %esi
	leaq	368(%rsp), %r13
	movq	%r13, %r8
	callq	Gsm_RPE_Decoding
	movswl	(%r12), %edx
	movswl	(%rbx), %esi
	movq	%r14, %rdi
	movq	%r13, %rcx
	movq	%rbp, %r8
	callq	Gsm_Long_Term_Synthesis_Filtering
	movups	304(%r14), %xmm0
	movaps	%xmm0, 112(%rsp)
	movups	240(%r14), %xmm0
	movups	256(%r14), %xmm1
	movups	272(%r14), %xmm2
	movups	288(%r14), %xmm3
	movaps	%xmm3, 96(%rsp)
	movaps	%xmm2, 80(%rsp)
	movaps	%xmm1, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movq	512(%rsp), %rax
	leaq	26(%rax), %rcx
	movq	8(%rsp), %r13           # 8-byte Reload
	movswl	2(%r13), %edx
	movq	24(%rsp), %rbp          # 8-byte Reload
	movswl	2(%rbp), %esi
	movq	%r14, %rdi
	leaq	368(%rsp), %r8
	callq	Gsm_RPE_Decoding
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movswl	2(%r12), %edx
	movswl	2(%rbx), %esi
	movq	%r14, %rdi
	leaq	368(%rsp), %rcx
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	Gsm_Long_Term_Synthesis_Filtering
	movups	304(%r14), %xmm0
	movaps	%xmm0, 192(%rsp)
	movups	240(%r14), %xmm0
	movups	256(%r14), %xmm1
	movups	272(%r14), %xmm2
	movups	288(%r14), %xmm3
	movaps	%xmm3, 176(%rsp)
	movaps	%xmm2, 160(%rsp)
	movaps	%xmm1, 144(%rsp)
	movaps	%xmm0, 128(%rsp)
	movq	512(%rsp), %rax
	leaq	52(%rax), %rcx
	movswl	4(%r13), %edx
	movswl	4(%rbp), %esi
	movq	%r14, %rdi
	leaq	368(%rsp), %r13
	movq	%r13, %r8
	callq	Gsm_RPE_Decoding
	movswl	4(%r12), %edx
	movswl	4(%rbx), %esi
	movq	%r14, %rdi
	movq	%r13, %rcx
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	%r12, %r8
	callq	Gsm_Long_Term_Synthesis_Filtering
	movups	304(%r14), %xmm0
	movaps	%xmm0, 272(%rsp)
	movups	240(%r14), %xmm0
	movups	256(%r14), %xmm1
	movups	272(%r14), %xmm2
	movups	288(%r14), %xmm3
	movaps	%xmm3, 256(%rsp)
	movaps	%xmm2, 240(%rsp)
	movaps	%xmm1, 224(%rsp)
	movaps	%xmm0, 208(%rsp)
	movq	512(%rsp), %rcx
	addq	$78, %rcx
	movq	8(%rsp), %rax           # 8-byte Reload
	movswl	6(%rax), %edx
	movswl	6(%rbp), %esi
	movq	%r14, %rdi
	movq	%r13, %r8
	callq	Gsm_RPE_Decoding
	movq	32(%rsp), %rax          # 8-byte Reload
	movswl	6(%rax), %edx
	movswl	6(%rbx), %esi
	movq	%r14, %rdi
	movq	%r13, %rcx
	movq	%r12, %r8
	callq	Gsm_Long_Term_Synthesis_Filtering
	movups	304(%r14), %xmm0
	movaps	%xmm0, 352(%rsp)
	movups	240(%r14), %xmm0
	movups	256(%r14), %xmm1
	movups	272(%r14), %xmm2
	movups	288(%r14), %xmm3
	movaps	%xmm3, 336(%rsp)
	movaps	%xmm2, 320(%rsp)
	movaps	%xmm1, 304(%rsp)
	movaps	%xmm0, 288(%rsp)
	leaq	48(%rsp), %rdx
	movq	%r14, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rcx
	callq	Gsm_Short_Term_Synthesis_Filter
	movw	650(%r14), %bp
	xorl	%eax, %eax
	movabsq	$242064356802560, %r8   # imm = 0xDC2800000000
	movabsq	$140737488355328, %r9   # imm = 0x800000000000
	movl	$32767, %esi            # imm = 0x7FFF
	movw	$32760, %di             # imm = 0x7FF8
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movswq	%bp, %rbp
	movswq	(%r15,%rax,2), %rbx
	imulq	%r8, %rbp
	addq	%r9, %rbp
	sarq	$48, %rbp
	leaq	(%rbp,%rbx), %rcx
	leaq	32768(%rbp,%rbx), %rbx
	testq	%rcx, %rcx
	movq	$-32768, %rbp           # imm = 0x8000
	cmovgq	%rsi, %rbp
	cmpq	$65535, %rbx            # imm = 0xFFFF
	cmovbeq	%rcx, %rbp
	movq	%rbp, %rbx
	shlq	$48, %rbx
	movq	%rbx, %rcx
	sarq	$47, %rcx
	leaq	32768(%rcx), %rdx
	testq	%rbx, %rbx
	movw	$-32768, %bx            # imm = 0x8000
	cmovgw	%di, %bx
	cmpq	$65535, %rdx            # imm = 0xFFFF
	cmovaw	%bx, %cx
	andl	$65528, %ecx            # imm = 0xFFF8
	movw	%cx, (%r15,%rax,2)
	incq	%rax
	cmpl	$160, %eax
	jne	.LBB0_1
# BB#2:                                 # %Postprocessing.exit
	movw	%bp, 650(%r14)
	addq	$456, %rsp              # imm = 0x1C8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	Gsm_Decoder, .Lfunc_end0-Gsm_Decoder
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
