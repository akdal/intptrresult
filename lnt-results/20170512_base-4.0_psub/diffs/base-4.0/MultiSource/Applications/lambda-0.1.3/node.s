	.text
	.file	"node.bc"
	.globl	_Z9newstringPKc
	.p2align	4, 0x90
	.type	_Z9newstringPKc,@function
_Z9newstringPKc:                        # @_Z9newstringPKc
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB0_1
# BB#2:
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, %r14
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	jmp	.LBB0_3
.LBB0_1:
	xorl	%r14d, %r14d
.LBB0_3:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	_Z9newstringPKc, .Lfunc_end0-_Z9newstringPKc
	.cfi_endproc

	.globl	_ZN4node5resetEv
	.p2align	4, 0x90
	.type	_ZN4node5resetEv,@function
_ZN4node5resetEv:                       # @_ZN4node5resetEv
	.cfi_startproc
# BB#0:
	movl	$1, _ZL13name_sequence(%rip)
	movl	$0, _ZL27lambda_reduce_recurse_level(%rip)
	movl	$0, _ZL24app_reduce_recurse_level(%rip)
	movl	$0, _ZL24var_reduce_recurse_level(%rip)
	retq
.Lfunc_end1:
	.size	_ZN4node5resetEv, .Lfunc_end1-_ZN4node5resetEv
	.cfi_endproc

	.globl	_ZNK4node5printEPK9alst_nodei
	.p2align	4, 0x90
	.type	_ZNK4node5printEPK9alst_nodei,@function
_ZNK4node5printEPK9alst_nodei:          # @_ZNK4node5printEPK9alst_nodei
	.cfi_startproc
# BB#0:
	movq	%rdi, %rcx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	jmp	printf                  # TAILCALL
.Lfunc_end2:
	.size	_ZNK4node5printEPK9alst_nodei, .Lfunc_end2-_ZNK4node5printEPK9alst_nodei
	.cfi_endproc

	.globl	_ZN8arg_nodeC2EPKcPK8exp_nodes
	.p2align	4, 0x90
	.type	_ZN8arg_nodeC2EPKcPK8exp_nodes,@function
_ZN8arg_nodeC2EPKcPK8exp_nodes:         # @_ZN8arg_nodeC2EPKcPK8exp_nodes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -48
.Lcfi11:
	.cfi_offset %r12, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movq	%rdx, %r14
	movq	%rsi, %rbp
	movq	%rdi, %r12
	movq	$0, 8(%r12)
	movq	$_ZTV8arg_node+16, (%r12)
	testq	%rbp, %rbp
	je	.LBB3_1
# BB#2:                                 # %.noexc
	movq	%rbp, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, %rbx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	strcpy
	jmp	.LBB3_3
.LBB3_1:
	xorl	%ebx, %ebx
.LBB3_3:                                # %_Z9newstringPKc.exit
	movq	%rbx, 16(%r12)
	testw	%r15w, %r15w
	je	.LBB3_5
# BB#4:
	movq	%r14, 24(%r12)
.LBB3_8:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_5:
	testq	%r14, %r14
	je	.LBB3_6
# BB#7:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.LBB3_8
# BB#9:
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	%rax, %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rcx                   # TAILCALL
.LBB3_6:                                # %.thread
	movq	$0, 24(%r12)
	jmp	.LBB3_8
.Lfunc_end3:
	.size	_ZN8arg_nodeC2EPKcPK8exp_nodes, .Lfunc_end3-_ZN8arg_nodeC2EPKcPK8exp_nodes
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end4:
	.size	__clang_call_terminate, .Lfunc_end4-__clang_call_terminate

	.text
	.globl	_ZN8arg_nodeC2ERKS_
	.p2align	4, 0x90
	.type	_ZN8arg_nodeC2ERKS_,@function
_ZN8arg_nodeC2ERKS_:                    # @_ZN8arg_nodeC2ERKS_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV8arg_node+16, (%rbx)
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB5_2
# BB#1:
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 16(%rbx)
	movq	16(%r14), %rsi
	movq	%rax, %rdi
	callq	strcpy
	jmp	.LBB5_3
.LBB5_2:
	movq	$0, 16(%rbx)
.LBB5_3:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB5_4
# BB#5:
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.LBB5_7
# BB#6:
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	*96(%rcx)
	jmp	.LBB5_7
.LBB5_4:                                # %.thread
	movq	$0, 24(%rbx)
.LBB5_7:
	movq	8(%r14), %rax
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	_ZN8arg_nodeC2ERKS_, .Lfunc_end5-_ZN8arg_nodeC2ERKS_
	.cfi_endproc

	.globl	_ZNK8arg_node5cloneEv
	.p2align	4, 0x90
	.type	_ZNK8arg_node5cloneEv,@function
_ZNK8arg_node5cloneEv:                  # @_ZNK8arg_node5cloneEv
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -32
.Lcfi24:
	.cfi_offset %r14, -24
.Lcfi25:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV8arg_node+16, (%rbx)
	movq	16(%r14), %r15
	testq	%r15, %r15
	je	.LBB6_3
# BB#1:
	movq	%r15, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp0:
	callq	_Znam
.Ltmp1:
# BB#2:                                 # %.noexc
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	strcpy
	jmp	.LBB6_4
.LBB6_3:
	movq	$0, 16(%rbx)
.LBB6_4:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB6_5
# BB#6:
	movq	(%rdi), %rax
.Ltmp2:
	callq	*(%rax)
.Ltmp3:
# BB#7:                                 # %.noexc2
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.LBB6_9
# BB#8:
	movq	(%rax), %rcx
.Ltmp4:
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	*96(%rcx)
.Ltmp5:
	jmp	.LBB6_9
.LBB6_5:                                # %.thread.i
	movq	$0, 24(%rbx)
.LBB6_9:
	movq	8(%r14), %rax
	movq	%rax, 8(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB6_10:
.Ltmp6:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZNK8arg_node5cloneEv, .Lfunc_end6-_ZNK8arg_node5cloneEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp5-.Ltmp0           #   Call between .Ltmp0 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end6-.Ltmp5      #   Call between .Ltmp5 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8arg_nodeD2Ev
	.p2align	4, 0x90
	.type	_ZN8arg_nodeD2Ev,@function
_ZN8arg_nodeD2Ev:                       # @_ZN8arg_nodeD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 16
.Lcfi27:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV8arg_node+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_2
# BB#1:
	callq	_ZdaPv
.LBB7_2:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_3
# BB#4:
	movq	(%rdi), %rax
	popq	%rbx
	jmpq	*16(%rax)               # TAILCALL
.LBB7_3:
	popq	%rbx
	retq
.Lfunc_end7:
	.size	_ZN8arg_nodeD2Ev, .Lfunc_end7-_ZN8arg_nodeD2Ev
	.cfi_endproc

	.globl	_ZN8arg_nodeD0Ev
	.p2align	4, 0x90
	.type	_ZN8arg_nodeD0Ev,@function
_ZN8arg_nodeD0Ev:                       # @_ZN8arg_nodeD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV8arg_node+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_2
# BB#1:
	callq	_ZdaPv
.LBB8_2:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp7:
	callq	*16(%rax)
.Ltmp8:
.LBB8_4:                                # %_ZN8arg_nodeD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB8_5:
.Ltmp9:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN8arg_nodeD0Ev, .Lfunc_end8-_ZN8arg_nodeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin1    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Lfunc_end8-.Ltmp8      #   Call between .Ltmp8 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8arg_nodeaSERKS_
	.p2align	4, 0x90
	.type	_ZN8arg_nodeaSERKS_,@function
_ZN8arg_nodeaSERKS_:                    # @_ZN8arg_nodeaSERKS_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi35:
	.cfi_def_cfa_offset 32
.Lcfi36:
	.cfi_offset %rbx, -24
.Lcfi37:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpq	%r14, %rbx
	je	.LBB9_9
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB9_3
# BB#2:
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 16(%rbx)
	movq	16(%r14), %rsi
	movq	%rax, %rdi
	callq	strcpy
	jmp	.LBB9_4
.LBB9_3:
	movq	$0, 16(%rbx)
.LBB9_4:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB9_5
# BB#6:
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.LBB9_8
# BB#7:
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	*96(%rcx)
	jmp	.LBB9_8
.LBB9_5:                                # %.thread
	movq	$0, 24(%rbx)
.LBB9_8:
	movq	8(%r14), %rax
	movq	%rax, 8(%rbx)
.LBB9_9:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	_ZN8arg_nodeaSERKS_, .Lfunc_end9-_ZN8arg_nodeaSERKS_
	.cfi_endproc

	.globl	_ZNK8arg_node5printEPK9alst_nodei
	.p2align	4, 0x90
	.type	_ZNK8arg_node5printEPK9alst_nodei,@function
_ZNK8arg_node5printEPK9alst_nodei:      # @_ZNK8arg_node5printEPK9alst_nodei
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 32
.Lcfi41:
	.cfi_offset %rbx, -32
.Lcfi42:
	.cfi_offset %r14, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movq	16(%rbp), %rax
	testq	%rax, %rax
	movl	$.L.str.2, %esi
	cmovneq	%rax, %rsi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpq	$0, 24(%rbp)
	je	.LBB10_1
# BB#2:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	printf
	movq	24(%rbp), %rdi
	movq	(%rdi), %rax
	movq	112(%rax), %rax
	movq	%rbx, %rsi
	movl	%r14d, %edx
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.LBB10_1:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZNK8arg_node5printEPK9alst_nodei, .Lfunc_end10-_ZNK8arg_node5printEPK9alst_nodei
	.cfi_endproc

	.globl	_ZN8arg_node12import_valueEPP8exp_node
	.p2align	4, 0x90
	.type	_ZN8arg_node12import_valueEPP8exp_node,@function
_ZN8arg_node12import_valueEPP8exp_node: # @_ZN8arg_node12import_valueEPP8exp_node
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -24
.Lcfi48:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB11_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB11_2:
	leaq	24(%r14), %rax
	testq	%rbx, %rbx
	movq	%rax, %rcx
	je	.LBB11_4
# BB#3:
	movq	(%rbx), %rcx
	movq	%rcx, (%rax)
	movq	%rbx, %rcx
.LBB11_4:
	movq	$0, (%rcx)
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB11_5
# BB#6:
	movq	(%rdi), %rax
	movq	96(%rax), %rax
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*%rax                   # TAILCALL
.LBB11_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end11:
	.size	_ZN8arg_node12import_valueEPP8exp_node, .Lfunc_end11-_ZN8arg_node12import_valueEPP8exp_node
	.cfi_endproc

	.globl	_ZNK8arg_nodeeqERKS_
	.p2align	4, 0x90
	.type	_ZNK8arg_nodeeqERKS_,@function
_ZNK8arg_nodeeqERKS_:                   # @_ZNK8arg_nodeeqERKS_
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rdi
	movq	16(%rsi), %rsi
	movq	%rdi, %rax
	orq	%rsi, %rax
	sete	%al
	testq	%rdi, %rdi
	je	.LBB12_3
# BB#1:
	testq	%rsi, %rsi
	je	.LBB12_3
# BB#2:
	pushq	%rax
.Lcfi49:
	.cfi_def_cfa_offset 16
	callq	strcmp
	testl	%eax, %eax
	sete	%al
	addq	$8, %rsp
.LBB12_3:
	movzbl	%al, %eax
	retq
.Lfunc_end12:
	.size	_ZNK8arg_nodeeqERKS_, .Lfunc_end12-_ZNK8arg_nodeeqERKS_
	.cfi_endproc

	.globl	_ZNK8arg_nodeeqEPKc
	.p2align	4, 0x90
	.type	_ZNK8arg_nodeeqEPKc,@function
_ZNK8arg_nodeeqEPKc:                    # @_ZNK8arg_nodeeqEPKc
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rdi
	movb	$1, %al
	movq	%rsi, %rcx
	orq	%rdi, %rcx
	je	.LBB13_4
# BB#1:
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.LBB13_4
# BB#2:
	testq	%rdi, %rdi
	je	.LBB13_4
# BB#3:
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 16
	callq	strcmp
	testl	%eax, %eax
	sete	%al
	addq	$8, %rsp
.LBB13_4:
	movzbl	%al, %eax
	retq
.Lfunc_end13:
	.size	_ZNK8arg_nodeeqEPKc, .Lfunc_end13-_ZNK8arg_nodeeqEPKc
	.cfi_endproc

	.globl	_ZNK8exp_node5matchEPK9alst_node
	.p2align	4, 0x90
	.type	_ZNK8exp_node5matchEPK9alst_node,@function
_ZNK8exp_node5matchEPK9alst_node:       # @_ZNK8exp_node5matchEPK9alst_node
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 32
.Lcfi54:
	.cfi_offset %rbx, -32
.Lcfi55:
	.cfi_offset %r14, -24
.Lcfi56:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	jne	.LBB14_2
	jmp	.LBB14_6
	.p2align	4, 0x90
.LBB14_5:                               #   in Loop: Header=BB14_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*80(%rax)
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB14_6
.LBB14_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	testq	%rax, %rax
	je	.LBB14_5
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*40(%rcx)
	testq	%rax, %rax
	je	.LBB14_5
# BB#4:                                 #   in Loop: Header=BB14_2 Depth=1
	movq	(%r14), %rax
	movq	160(%rax), %r15
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*40(%rcx)
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	*%r15
	testl	%eax, %eax
	je	.LBB14_5
	jmp	.LBB14_7
.LBB14_6:
	xorl	%ebx, %ebx
.LBB14_7:                               # %._crit_edge
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	_ZNK8exp_node5matchEPK9alst_node, .Lfunc_end14-_ZNK8exp_node5matchEPK9alst_node
	.cfi_endproc

	.globl	_ZNK8exp_node14symbolic_printEPK9alst_nodei
	.p2align	4, 0x90
	.type	_ZNK8exp_node14symbolic_printEPK9alst_nodei,@function
_ZNK8exp_node14symbolic_printEPK9alst_nodei: # @_ZNK8exp_node14symbolic_printEPK9alst_nodei
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi61:
	.cfi_def_cfa_offset 48
.Lcfi62:
	.cfi_offset %rbx, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testb	$8, %bpl
	je	.LBB15_7
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB15_7
# BB#2:
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	*200(%rax)
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB15_5
# BB#3:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	*112(%rax)
	cmpq	%r15, %rbx
	je	.LBB15_6
# BB#4:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*16(%rax)
	jmp	.LBB15_6
.LBB15_7:
	movq	(%rbx), %rax
	movq	112(%rax), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%ebp, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.LBB15_5:
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	*112(%rax)
.LBB15_6:
	movq	(%r15), %rax
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*16(%rax)               # TAILCALL
.Lfunc_end15:
	.size	_ZNK8exp_node14symbolic_printEPK9alst_nodei, .Lfunc_end15-_ZNK8exp_node14symbolic_printEPK9alst_nodei
	.cfi_endproc

	.globl	_ZN8var_nodeC2EPKc
	.p2align	4, 0x90
	.type	_ZN8var_nodeC2EPKc,@function
_ZN8var_nodeC2EPKc:                     # @_ZN8var_nodeC2EPKc
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi68:
	.cfi_def_cfa_offset 32
.Lcfi69:
	.cfi_offset %rbx, -24
.Lcfi70:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV8var_node+16, (%rbx)
	testq	%r14, %r14
	je	.LBB16_1
# BB#2:
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	strcpy                  # TAILCALL
.LBB16_1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end16:
	.size	_ZN8var_nodeC2EPKc, .Lfunc_end16-_ZN8var_nodeC2EPKc
	.cfi_endproc

	.globl	_ZN8var_nodeC2ERKS_
	.p2align	4, 0x90
	.type	_ZN8var_nodeC2ERKS_,@function
_ZN8var_nodeC2ERKS_:                    # @_ZN8var_nodeC2ERKS_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi73:
	.cfi_def_cfa_offset 32
.Lcfi74:
	.cfi_offset %rbx, -24
.Lcfi75:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV8var_node+16, (%rbx)
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB17_2
# BB#1:
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 16(%rbx)
	movq	16(%r14), %rsi
	movq	%rax, %rdi
	callq	strcpy
	jmp	.LBB17_3
.LBB17_2:
	movq	$0, 16(%rbx)
.LBB17_3:
	movq	$0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end17:
	.size	_ZN8var_nodeC2ERKS_, .Lfunc_end17-_ZN8var_nodeC2ERKS_
	.cfi_endproc

	.globl	_ZNK8var_node8has_freeEPK8arg_nodePK9alst_node
	.p2align	4, 0x90
	.type	_ZNK8var_node8has_freeEPK8arg_nodePK9alst_node,@function
_ZNK8var_node8has_freeEPK8arg_nodePK9alst_node: # @_ZNK8var_node8has_freeEPK8arg_nodePK9alst_node
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 16
.Lcfi77:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	testq	%rbx, %rbx
	je	.LBB18_4
# BB#1:
	movq	(%rdi), %rax
	movq	%rdx, %rsi
	callq	*128(%rax)
	testq	%rax, %rax
	je	.LBB18_4
# BB#2:
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*48(%rcx)
	cmpq	%rbx, %rax
	je	.LBB18_3
.LBB18_4:                               # %.critedge
	xorl	%eax, %eax
.LBB18_5:
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	popq	%rbx
	retq
.LBB18_3:
	movw	$1, %ax
	jmp	.LBB18_5
.Lfunc_end18:
	.size	_ZNK8var_node8has_freeEPK8arg_nodePK9alst_node, .Lfunc_end18-_ZNK8var_node8has_freeEPK8arg_nodePK9alst_node
	.cfi_endproc

	.globl	_ZNK8var_node4bindEPK9alst_node
	.p2align	4, 0x90
	.type	_ZNK8var_node4bindEPK9alst_node,@function
_ZNK8var_node4bindEPK9alst_node:        # @_ZNK8var_node4bindEPK9alst_node
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 32
.Lcfi81:
	.cfi_offset %rbx, -32
.Lcfi82:
	.cfi_offset %r14, -24
.Lcfi83:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%r14), %rax
	callq	*32(%rax)
	testq	%rax, %rax
	jne	.LBB19_1
	jmp	.LBB19_6
	.p2align	4, 0x90
.LBB19_5:                               #   in Loop: Header=BB19_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*80(%rax)
	movq	%rax, %rbx
.LBB19_1:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	je	.LBB19_6
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB19_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	testq	%rax, %rax
	je	.LBB19_5
# BB#3:                                 #   in Loop: Header=BB19_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	testq	%rax, %rax
	je	.LBB19_5
# BB#4:                                 #   in Loop: Header=BB19_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	movq	%rax, %r15
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*32(%rax)
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB19_5
	jmp	.LBB19_7
.LBB19_6:
	xorl	%ebx, %ebx
.LBB19_7:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end19:
	.size	_ZNK8var_node4bindEPK9alst_node, .Lfunc_end19-_ZNK8var_node4bindEPK9alst_node
	.cfi_endproc

	.globl	_ZNK8var_node5printEPK9alst_nodei
	.p2align	4, 0x90
	.type	_ZNK8var_node5printEPK9alst_nodei,@function
_ZNK8var_node5printEPK9alst_nodei:      # @_ZNK8var_node5printEPK9alst_nodei
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 16
.Lcfi85:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*32(%rax)
	testq	%rax, %rax
	je	.LBB20_1
# BB#2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	%rax, %rsi
	jmp	.LBB20_3
.LBB20_1:
	movl	$.L.str.4, %esi
.LBB20_3:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	popq	%rbx
	jmp	printf                  # TAILCALL
.Lfunc_end20:
	.size	_ZNK8var_node5printEPK9alst_nodei, .Lfunc_end20-_ZNK8var_node5printEPK9alst_nodei
	.cfi_endproc

	.globl	_ZN8var_nodeaSERKS_
	.p2align	4, 0x90
	.type	_ZN8var_nodeaSERKS_,@function
_ZN8var_nodeaSERKS_:                    # @_ZN8var_nodeaSERKS_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi88:
	.cfi_def_cfa_offset 32
.Lcfi89:
	.cfi_offset %rbx, -24
.Lcfi90:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB21_2
# BB#1:
	callq	_ZdaPv
.LBB21_2:
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB21_4
# BB#3:
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 16(%rbx)
	movq	16(%r14), %rsi
	movq	%rax, %rdi
	callq	strcpy
	cmpq	%r14, %rbx
	jne	.LBB21_6
	jmp	.LBB21_7
.LBB21_4:
	movq	$0, 16(%rbx)
	cmpq	%r14, %rbx
	je	.LBB21_7
.LBB21_6:
	movq	8(%r14), %rax
	movq	%rax, 8(%rbx)
.LBB21_7:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end21:
	.size	_ZN8var_nodeaSERKS_, .Lfunc_end21-_ZN8var_nodeaSERKS_
	.cfi_endproc

	.globl	_ZN8var_node8set_nameEPKc
	.p2align	4, 0x90
	.type	_ZN8var_node8set_nameEPKc,@function
_ZN8var_node8set_nameEPKc:              # @_ZN8var_node8set_nameEPKc
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi93:
	.cfi_def_cfa_offset 32
.Lcfi94:
	.cfi_offset %rbx, -24
.Lcfi95:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	16(%r14), %rdi
	cmpq	%rbx, %rdi
	je	.LBB22_5
# BB#1:
	testq	%rdi, %rdi
	je	.LBB22_3
# BB#2:
	callq	_ZdaPv
.LBB22_3:
	testq	%rbx, %rbx
	je	.LBB22_4
# BB#6:
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 16(%r14)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	strcpy                  # TAILCALL
.LBB22_4:                               # %.thread
	movq	$0, 16(%r14)
.LBB22_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end22:
	.size	_ZN8var_node8set_nameEPKc, .Lfunc_end22-_ZN8var_node8set_nameEPKc
	.cfi_endproc

	.globl	_ZN8var_node12reduce_valueEPK9alst_nodeiPiPS2_
	.p2align	4, 0x90
	.type	_ZN8var_node12reduce_valueEPK9alst_nodeiPiPS2_,@function
_ZN8var_node12reduce_valueEPK9alst_nodeiPiPS2_: # @_ZN8var_node12reduce_valueEPK9alst_nodeiPiPS2_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi100:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi102:
	.cfi_def_cfa_offset 80
.Lcfi103:
	.cfi_offset %rbx, -56
.Lcfi104:
	.cfi_offset %r12, -48
.Lcfi105:
	.cfi_offset %r13, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %r12
	shrl	$7, %edx
	andl	$2, %edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movq	%r12, %rbp
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
.LBB23_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_2 Depth 2
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB23_2:                               #   Parent Loop BB23_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	*128(%rax)
	movq	%rax, %r13
	testq	%rbx, %rbx
	je	.LBB23_14
# BB#3:                                 #   in Loop: Header=BB23_2 Depth=2
	testq	%r13, %r13
	je	.LBB23_10
# BB#4:                                 #   in Loop: Header=BB23_2 Depth=2
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*48(%rax)
	testq	%rax, %rax
	je	.LBB23_10
# BB#5:                                 #   in Loop: Header=BB23_2 Depth=2
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*40(%rcx)
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB23_11
# BB#6:                                 #   in Loop: Header=BB23_2 Depth=2
	cmpq	%rbp, %rbx
	je	.LBB23_15
# BB#7:                                 #   in Loop: Header=BB23_2 Depth=2
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*24(%rax)
	cmpq	%r12, %rbp
	movl	$1, %r15d
	je	.LBB23_12
# BB#8:                                 #   in Loop: Header=BB23_2 Depth=2
	cmpl	$3, %eax
	movq	%rbp, %rbx
	je	.LBB23_2
	jmp	.LBB23_12
	.p2align	4, 0x90
.LBB23_10:                              #   in Loop: Header=BB23_1 Depth=1
	movq	%rbx, %rbp
	testl	%r15d, %r15d
	jne	.LBB23_13
	jmp	.LBB23_18
.LBB23_11:                              #   in Loop: Header=BB23_1 Depth=1
	movq	%rbx, %rbp
.LBB23_12:                              # %.critedge
                                        #   in Loop: Header=BB23_1 Depth=1
	testl	%r15d, %r15d
	je	.LBB23_18
.LBB23_13:                              #   in Loop: Header=BB23_1 Depth=1
	incl	%r14d
	movq	definition_env(%rip), %r13
	cmpl	12(%rsp), %r14d         # 4-byte Folded Reload
	movq	%rbp, %rbx
	jl	.LBB23_1
	jmp	.LBB23_15
.LBB23_14:
	xorl	%ebx, %ebx
.LBB23_15:                              # %.critedge3
	testq	%rbx, %rbx
	cmovneq	%rbx, %r12
	movq	16(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB23_17
# BB#16:
	movq	definition_env(%rip), %rax
	movq	%rax, (%rcx)
.LBB23_17:
	movq	%r12, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB23_18:
	movq	%rbp, %rbx
	jmp	.LBB23_15
.Lfunc_end23:
	.size	_ZN8var_node12reduce_valueEPK9alst_nodeiPiPS2_, .Lfunc_end23-_ZN8var_node12reduce_valueEPK9alst_nodeiPiPS2_
	.cfi_endproc

	.globl	_ZN8var_node13reduce_numberEv
	.p2align	4, 0x90
	.type	_ZN8var_node13reduce_numberEv,@function
_ZN8var_node13reduce_numberEv:          # @_ZN8var_node13reduce_numberEv
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi112:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi113:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi115:
	.cfi_def_cfa_offset 64
.Lcfi116:
	.cfi_offset %rbx, -56
.Lcfi117:
	.cfi_offset %r12, -48
.Lcfi118:
	.cfi_offset %r13, -40
.Lcfi119:
	.cfi_offset %r14, -32
.Lcfi120:
	.cfi_offset %r15, -24
.Lcfi121:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	(%r12), %rax
	callq	*32(%rax)
	testq	%rax, %rax
	je	.LBB24_1
# BB#3:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*32(%rax)
	movb	(%rax), %cl
	testb	%cl, %cl
	je	.LBB24_7
# BB#4:                                 # %.lr.ph91.preheader
	incq	%rax
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB24_6:                               # %.lr.ph91
                                        # =>This Inner Loop Header: Depth=1
	addb	$-48, %cl
	cmpb	$9, %cl
	ja	.LBB24_2
# BB#5:                                 #   in Loop: Header=BB24_6 Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	testb	%cl, %cl
	jne	.LBB24_6
.LBB24_7:                               # %._crit_edge92.thread
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %r14
	movq	$0, 8(%r14)
	movq	$_ZTV8arg_node+16, (%r14)
.Ltmp10:
	movl	$2, %edi
	callq	_Znam
.Ltmp11:
# BB#8:
	movw	$109, (%rax)
	movq	%rax, 16(%r14)
	movq	$0, 24(%r14)
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %r15
	movq	$0, 8(%r15)
	movq	$_ZTV8arg_node+16, (%r15)
.Ltmp13:
	movl	$2, %edi
	callq	_Znam
.Ltmp14:
# BB#9:
	movw	$110, (%rax)
	movq	%rax, 16(%r15)
	movq	$0, 24(%r15)
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r13
	movq	$0, 8(%r13)
	movq	$_ZTV8var_node+16, (%r13)
.Ltmp16:
	movl	$2, %edi
	callq	_Znam
.Ltmp17:
# BB#10:
	movq	%rax, 16(%r13)
	movw	$110, (%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*32(%rax)
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rax, %rdi
	callq	strtol
	movq	%rax, %r12
	testl	%r12d, %r12d
	jle	.LBB24_11
# BB#12:                                # %.lr.ph.preheader
	incl	%r12d
	.p2align	4, 0x90
.LBB24_13:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movq	$0, 8(%rbp)
	movq	$_ZTV8var_node+16, (%rbp)
.Ltmp19:
	movl	$2, %edi
	callq	_Znam
.Ltmp20:
# BB#14:                                # %.noexc.i
                                        #   in Loop: Header=BB24_13 Depth=1
	movq	%rax, 16(%rbp)
	movw	$109, (%rax)
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV8app_node+16, (%rbx)
	movq	%rbp, 16(%rbx)
	movq	%rbx, 8(%rbp)
	movq	(%r13), %rax
	movq	%r13, 24(%rbx)
.Ltmp22:
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.Ltmp23:
# BB#15:                                # %_ZN8app_nodeC2EP8exp_nodeS1_s.exit
                                        #   in Loop: Header=BB24_13 Depth=1
	decl	%r12d
	cmpl	$1, %r12d
	movq	%rbx, %r13
	jg	.LBB24_13
	jmp	.LBB24_16
.LBB24_1:
	xorl	%ebx, %ebx
	jmp	.LBB24_2
.LBB24_11:
	movq	%r13, %rbx
.LBB24_16:                              # %._crit_edge
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %r12
	movq	$0, 8(%r12)
	movq	$_ZTV8lam_node+16, (%r12)
	movq	$0, 24(%r12)
	movq	%r15, 16(%r12)
	movq	(%r15), %rax
.Ltmp25:
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	*96(%rax)
.Ltmp26:
# BB#17:                                # %.noexc68
	movq	24(%r12), %rdi
	cmpq	%rbx, %rdi
	je	.LBB24_21
# BB#18:
	testq	%rdi, %rdi
	je	.LBB24_20
# BB#19:
	movq	(%rdi), %rax
.Ltmp27:
	callq	*16(%rax)
.Ltmp28:
.LBB24_20:                              # %.noexc.i67
	movq	%rbx, 24(%r12)
	movq	(%rbx), %rax
.Ltmp29:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	*96(%rax)
.Ltmp30:
.LBB24_21:                              # %_ZN8lam_nodeC2EP8arg_nodeP8exp_nodes.exit
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV8lam_node+16, (%rbx)
	movq	$0, 24(%rbx)
	movq	%r14, 16(%rbx)
	movq	(%r14), %rax
.Ltmp32:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.Ltmp33:
# BB#22:                                # %.noexc73
	movq	24(%rbx), %rdi
	cmpq	%r12, %rdi
	je	.LBB24_2
# BB#23:
	testq	%rdi, %rdi
	je	.LBB24_25
# BB#24:
	movq	(%rdi), %rax
.Ltmp34:
	callq	*16(%rax)
.Ltmp35:
.LBB24_25:                              # %.noexc.i72
	movq	%r12, 24(%rbx)
	movq	(%r12), %rax
.Ltmp36:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.Ltmp37:
.LBB24_2:                               # %._crit_edge92.thread106
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB24_28:
.Ltmp18:
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%r13, %rdi
	jmp	.LBB24_34
.LBB24_27:
.Ltmp15:
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%r15, %rdi
	jmp	.LBB24_34
.LBB24_26:
.Ltmp12:
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%r14, %rdi
	jmp	.LBB24_34
.LBB24_32:
.Ltmp38:
	jmp	.LBB24_33
.LBB24_31:
.Ltmp31:
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%r12, %rdi
	jmp	.LBB24_34
.LBB24_30:
.Ltmp24:
.LBB24_33:
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rbx, %rdi
	jmp	.LBB24_34
.LBB24_29:
.Ltmp21:
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rbp, %rdi
.LBB24_34:
	callq	_ZdlPv
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_Unwind_Resume
.Lfunc_end24:
	.size	_ZN8var_node13reduce_numberEv, .Lfunc_end24-_ZN8var_node13reduce_numberEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table24:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\306\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\303\001"              # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp10-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin2   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp13-.Ltmp11         #   Call between .Ltmp11 and .Ltmp13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin2   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp16-.Ltmp14         #   Call between .Ltmp14 and .Ltmp16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin2   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp19-.Ltmp17         #   Call between .Ltmp17 and .Ltmp19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin2   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Ltmp22-.Ltmp20         #   Call between .Ltmp20 and .Ltmp22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 10 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin2   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin2   # >> Call Site 11 <<
	.long	.Ltmp25-.Ltmp23         #   Call between .Ltmp23 and .Ltmp25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin2   # >> Call Site 12 <<
	.long	.Ltmp30-.Ltmp25         #   Call between .Ltmp25 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin2   #     jumps to .Ltmp31
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin2   # >> Call Site 13 <<
	.long	.Ltmp32-.Ltmp30         #   Call between .Ltmp30 and .Ltmp32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin2   # >> Call Site 14 <<
	.long	.Ltmp37-.Ltmp32         #   Call between .Ltmp32 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin2   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin2   # >> Call Site 15 <<
	.long	.Lfunc_end24-.Ltmp37    #   Call between .Ltmp37 and .Lfunc_end24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8var_node6reduceEPK9alst_nodeiPi
	.p2align	4, 0x90
	.type	_ZN8var_node6reduceEPK9alst_nodeiPi,@function
_ZN8var_node6reduceEPK9alst_nodeiPi:    # @_ZN8var_node6reduceEPK9alst_nodeiPi
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi124:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi125:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi126:
	.cfi_def_cfa_offset 48
.Lcfi127:
	.cfi_offset %rbx, -40
.Lcfi128:
	.cfi_offset %r12, -32
.Lcfi129:
	.cfi_offset %r14, -24
.Lcfi130:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movl	%edx, %ebx
	movq	%rdi, %r15
	movl	_ZL24var_reduce_recurse_level(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, _ZL24var_reduce_recurse_level(%rip)
	cmpl	$4000, %eax             # imm = 0xFA0
	jl	.LBB25_4
# BB#1:
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	movq	(%r15), %rax
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	*112(%rax)
	movl	$.Lstr, %edi
	callq	puts
	decl	_ZL24var_reduce_recurse_level(%rip)
	testq	%r14, %r14
	je	.LBB25_3
# BB#2:
	orb	$1, (%r14)
.LBB25_3:
	movq	%r15, %rbx
	jmp	.LBB25_19
.LBB25_4:
	movq	$0, (%rsp)
	movq	(%r15), %rax
	movq	%rsp, %r8
	movq	%r15, %rdi
	movl	%ebx, %edx
	movq	%r14, %rcx
	callq	*216(%rax)
	movq	%rax, %r12
	cmpq	%r15, %r12
	je	.LBB25_13
# BB#5:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	movq	%rax, %r12
	cmpq	$0, (%rsp)
	je	.LBB25_13
# BB#6:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*24(%rax)
	cmpl	$5, %eax
	jne	.LBB25_7
# BB#9:
	testb	$1, %bh
	jne	.LBB25_10
	jmp	.LBB25_13
.LBB25_7:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*24(%rax)
	testb	$1, %bh
	je	.LBB25_13
# BB#8:
	cmpl	$4, %eax
	jne	.LBB25_13
.LBB25_10:
	movq	(%r12), %rax
	movq	(%rsp), %rsi
	movq	%r12, %rdi
	movl	%ebx, %edx
	movq	%r14, %rcx
	callq	*136(%rax)
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB25_13
# BB#11:
	cmpq	%r12, %rbx
	je	.LBB25_13
# BB#12:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*16(%rax)
	movq	%rbx, %r12
.LBB25_13:
	movq	%r15, %rdi
	callq	_ZN8var_node13reduce_numberEv
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB25_14
# BB#15:
	cmpq	%r15, %r12
	je	.LBB25_18
# BB#16:
	testq	%r12, %r12
	je	.LBB25_18
# BB#17:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*16(%rax)
	jmp	.LBB25_18
.LBB25_14:
	movq	%r12, %rbx
.LBB25_18:
	decl	_ZL24var_reduce_recurse_level(%rip)
.LBB25_19:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end25:
	.size	_ZN8var_node6reduceEPK9alst_nodeiPi, .Lfunc_end25-_ZN8var_node6reduceEPK9alst_nodeiPi
	.cfi_endproc

	.globl	_ZN8var_node6renameEP8arg_nodePKcP9alst_node
	.p2align	4, 0x90
	.type	_ZN8var_node6renameEP8arg_nodePKcP9alst_node,@function
_ZN8var_node6renameEP8arg_nodePKcP9alst_node: # @_ZN8var_node6renameEP8arg_nodePKcP9alst_node
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 32
.Lcfi134:
	.cfi_offset %rbx, -32
.Lcfi135:
	.cfi_offset %r14, -24
.Lcfi136:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	movq	%rcx, %rsi
	callq	*128(%rax)
	testq	%rax, %rax
	je	.LBB26_8
# BB#1:
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*48(%rcx)
	testq	%rax, %rax
	je	.LBB26_8
# BB#2:
	cmpq	%r15, %rax
	jne	.LBB26_8
# BB#3:
	movq	16(%rbx), %rdi
	cmpq	%r14, %rdi
	je	.LBB26_8
# BB#4:
	testq	%rdi, %rdi
	je	.LBB26_6
# BB#5:
	callq	_ZdaPv
.LBB26_6:
	testq	%r14, %r14
	je	.LBB26_7
# BB#9:
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	strcpy                  # TAILCALL
.LBB26_7:                               # %.thread.i
	movq	$0, 16(%rbx)
.LBB26_8:                               # %_ZN8var_node8set_nameEPKc.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end26:
	.size	_ZN8var_node6renameEP8arg_nodePKcP9alst_node, .Lfunc_end26-_ZN8var_node6renameEP8arg_nodePKcP9alst_node
	.cfi_endproc

	.globl	_ZNK8var_nodeeqERK8exp_node
	.p2align	4, 0x90
	.type	_ZNK8var_nodeeqERK8exp_node,@function
_ZNK8var_nodeeqERK8exp_node:            # @_ZNK8var_nodeeqERK8exp_node
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi137:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi138:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi139:
	.cfi_def_cfa_offset 32
.Lcfi140:
	.cfi_offset %rbx, -24
.Lcfi141:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*24(%rax)
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	$3, %ecx
	jne	.LBB27_10
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*32(%rax)
	testq	%rax, %rax
	jne	.LBB27_3
# BB#2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	testq	%rax, %rax
	je	.LBB27_8
.LBB27_3:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*32(%rax)
	testq	%rax, %rax
	je	.LBB27_7
# BB#4:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	testq	%rax, %rax
	je	.LBB27_7
# BB#5:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*32(%rax)
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	strcmp
	testl	%eax, %eax
	sete	%al
	jmp	.LBB27_9
.LBB27_7:
	xorl	%eax, %eax
	jmp	.LBB27_9
.LBB27_8:
	movb	$1, %al
.LBB27_9:
	movzbl	%al, %eax
.LBB27_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end27:
	.size	_ZNK8var_nodeeqERK8exp_node, .Lfunc_end27-_ZNK8var_nodeeqERK8exp_node
	.cfi_endproc

	.globl	_ZNK8var_node7extractEPKci
	.p2align	4, 0x90
	.type	_ZNK8var_node7extractEPKci,@function
_ZNK8var_node7extractEPKci:             # @_ZNK8var_node7extractEPKci
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r15
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi144:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi145:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi146:
	.cfi_def_cfa_offset 48
.Lcfi147:
	.cfi_offset %rbx, -40
.Lcfi148:
	.cfi_offset %r12, -32
.Lcfi149:
	.cfi_offset %r14, -24
.Lcfi150:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*32(%rax)
	testq	%r14, %r14
	je	.LBB28_7
# BB#1:
	testq	%rax, %rax
	je	.LBB28_7
# BB#2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB28_3
.LBB28_7:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r15
	testq	%r14, %r14
	je	.LBB28_22
# BB#8:
	movq	$0, 8(%r15)
	movq	$_ZTV8var_node+16, (%r15)
.Ltmp44:
	movl	$2, %edi
	callq	_Znam
.Ltmp45:
# BB#9:
	movq	%rax, 16(%r15)
	movw	$75, (%rax)
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r14
	movq	(%rbx), %rax
.Ltmp47:
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	%rax, %rbx
.Ltmp48:
# BB#10:
	movq	$0, 8(%r14)
	movq	$_ZTV8var_node+16, (%r14)
	testq	%rbx, %rbx
	je	.LBB28_13
# BB#11:
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp49:
	callq	_Znam
.Ltmp50:
# BB#12:                                # %.noexc
	movq	%rax, 16(%r14)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcpy
.LBB28_13:                              # %_ZN8var_nodeC2EPKc.exit18
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV8app_node+16, (%rbx)
	movq	$0, 24(%rbx)
	movq	%r15, 16(%rbx)
	movq	(%r15), %rax
.Ltmp52:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.Ltmp53:
# BB#14:                                # %.noexc19
	movq	24(%rbx), %rdi
	cmpq	%r14, %rdi
	je	.LBB28_15
# BB#16:
	testq	%rdi, %rdi
	je	.LBB28_18
# BB#17:
	movq	(%rdi), %rax
.Ltmp54:
	callq	*16(%rax)
.Ltmp55:
.LBB28_18:                              # %.noexc.i
	movq	%r14, 24(%rbx)
	movq	(%r14), %rax
.Ltmp56:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.Ltmp57:
# BB#19:
	movq	%rbx, %r15
	jmp	.LBB28_26
.LBB28_22:
	movq	(%rbx), %rax
.Ltmp39:
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	%rax, %rbx
.Ltmp40:
# BB#23:
	movq	$0, 8(%r15)
	movq	$_ZTV8var_node+16, (%r15)
	testq	%rbx, %rbx
	je	.LBB28_26
# BB#24:
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp41:
	callq	_Znam
.Ltmp42:
# BB#25:                                # %.noexc22
	movq	%rax, 16(%r15)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	jmp	.LBB28_26
.LBB28_15:
	movq	%rbx, %r15
	jmp	.LBB28_26
.LBB28_3:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r15
	movq	$0, 8(%r15)
	movq	$_ZTV8var_node+16, (%r15)
.Ltmp59:
	movl	$2, %edi
	callq	_Znam
.Ltmp60:
# BB#4:                                 # %_ZN8var_nodeC2EPKc.exit
	movq	%rax, 16(%r15)
	movw	$73, (%rax)
.LBB28_26:                              # %_ZN8app_nodeC2EP8exp_nodeS1_s.exit
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB28_5:
.Ltmp61:
	jmp	.LBB28_6
.LBB28_29:
.Ltmp43:
	jmp	.LBB28_6
.LBB28_20:
.Ltmp46:
.LBB28_6:
	movq	%rax, %r12
	movq	%r15, %rdi
	jmp	.LBB28_28
.LBB28_27:
.Ltmp51:
	movq	%rax, %r12
	movq	%r14, %rdi
	jmp	.LBB28_28
.LBB28_21:
.Ltmp58:
	movq	%rax, %r12
	movq	%rbx, %rdi
.LBB28_28:
	callq	_ZdlPv
	movq	%r12, %rdi
	callq	_Unwind_Resume
.Lfunc_end28:
	.size	_ZNK8var_node7extractEPKci, .Lfunc_end28-_ZNK8var_node7extractEPKci
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\205\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp44-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp44
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin3   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp47-.Ltmp45         #   Call between .Ltmp45 and .Ltmp47
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp50-.Ltmp47         #   Call between .Ltmp47 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin3   #     jumps to .Ltmp51
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp52-.Ltmp50         #   Call between .Ltmp50 and .Ltmp52
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp57-.Ltmp52         #   Call between .Ltmp52 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin3   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp42-.Ltmp39         #   Call between .Ltmp39 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin3   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp59-.Ltmp42         #   Call between .Ltmp42 and .Ltmp59
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Ltmp60-.Ltmp59         #   Call between .Ltmp59 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin3   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin3   # >> Call Site 10 <<
	.long	.Lfunc_end28-.Ltmp60    #   Call between .Ltmp60 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8lam_nodeC2EP8arg_nodeP8exp_nodes
	.p2align	4, 0x90
	.type	_ZN8lam_nodeC2EP8arg_nodeP8exp_nodes,@function
_ZN8lam_nodeC2EP8arg_nodeP8exp_nodes:   # @_ZN8lam_nodeC2EP8arg_nodeP8exp_nodes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi151:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi152:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi153:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi154:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi155:
	.cfi_def_cfa_offset 48
.Lcfi156:
	.cfi_offset %rbx, -40
.Lcfi157:
	.cfi_offset %r14, -32
.Lcfi158:
	.cfi_offset %r15, -24
.Lcfi159:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV8lam_node+16, (%rbx)
	testq	%rsi, %rsi
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	je	.LBB29_1
# BB#2:                                 # %.noexc10
	leaq	16(%rbx), %rbp
	testw	%r15w, %r15w
	je	.LBB29_4
# BB#3:                                 # %.thread
	movq	%rsi, (%rbp)
	jmp	.LBB29_5
.LBB29_1:
	xorl	%edi, %edi
	cmpq	%r14, %rdi
	jne	.LBB29_8
	jmp	.LBB29_15
.LBB29_4:
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	callq	*(%rax)
	movq	%rax, %rsi
	movq	%rsi, (%rbp)
	testq	%rsi, %rsi
	je	.LBB29_6
.LBB29_5:
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.LBB29_6:                               # %_ZN8lam_node7set_argEP8arg_nodes.exitthread-pre-split
	movq	24(%rbx), %rdi
	cmpq	%r14, %rdi
	je	.LBB29_15
.LBB29_8:
	testq	%rdi, %rdi
	je	.LBB29_10
# BB#9:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB29_10:                              # %.noexc
	testw	%r15w, %r15w
	jne	.LBB29_14
# BB#11:
	testq	%r14, %r14
	je	.LBB29_12
# BB#13:                                # %.noexc6
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	movq	%rax, %r14
.LBB29_14:
	movq	%r14, 24(%rbx)
	testq	%r14, %r14
	je	.LBB29_15
# BB#16:
	movq	(%r14), %rax
	movq	96(%rax), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.LBB29_12:                              # %.thread.i
	movq	$0, 24(%rbx)
.LBB29_15:                              # %_ZN8lam_node8set_bodyEP8exp_nodes.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end29:
	.size	_ZN8lam_nodeC2EP8arg_nodeP8exp_nodes, .Lfunc_end29-_ZN8lam_nodeC2EP8arg_nodeP8exp_nodes
	.cfi_endproc

	.globl	_ZN8lam_node7set_argEP8arg_nodes
	.p2align	4, 0x90
	.type	_ZN8lam_node7set_argEP8arg_nodes,@function
_ZN8lam_node7set_argEP8arg_nodes:       # @_ZN8lam_node7set_argEP8arg_nodes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi161:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 32
.Lcfi163:
	.cfi_offset %rbx, -32
.Lcfi164:
	.cfi_offset %r14, -24
.Lcfi165:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	16(%r14), %rdi
	cmpq	%rbx, %rdi
	je	.LBB30_8
# BB#1:
	testq	%rdi, %rdi
	je	.LBB30_3
# BB#2:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB30_3:
	testw	%bp, %bp
	jne	.LBB30_7
# BB#4:
	testq	%rbx, %rbx
	je	.LBB30_5
# BB#6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, %rbx
.LBB30_7:
	movq	%rbx, 16(%r14)
	testq	%rbx, %rbx
	je	.LBB30_8
# BB#9:
	movq	(%rbx), %rax
	movq	96(%rax), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.LBB30_5:                               # %.thread
	movq	$0, 16(%r14)
.LBB30_8:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end30:
	.size	_ZN8lam_node7set_argEP8arg_nodes, .Lfunc_end30-_ZN8lam_node7set_argEP8arg_nodes
	.cfi_endproc

	.globl	_ZN8lam_node8set_bodyEP8exp_nodes
	.p2align	4, 0x90
	.type	_ZN8lam_node8set_bodyEP8exp_nodes,@function
_ZN8lam_node8set_bodyEP8exp_nodes:      # @_ZN8lam_node8set_bodyEP8exp_nodes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi166:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi167:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi168:
	.cfi_def_cfa_offset 32
.Lcfi169:
	.cfi_offset %rbx, -32
.Lcfi170:
	.cfi_offset %r14, -24
.Lcfi171:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	24(%r14), %rdi
	cmpq	%rbx, %rdi
	je	.LBB31_8
# BB#1:
	testq	%rdi, %rdi
	je	.LBB31_3
# BB#2:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB31_3:
	testw	%bp, %bp
	jne	.LBB31_7
# BB#4:
	testq	%rbx, %rbx
	je	.LBB31_5
# BB#6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, %rbx
.LBB31_7:
	movq	%rbx, 24(%r14)
	testq	%rbx, %rbx
	je	.LBB31_8
# BB#9:
	movq	(%rbx), %rax
	movq	96(%rax), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.LBB31_5:                               # %.thread
	movq	$0, 24(%r14)
.LBB31_8:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end31:
	.size	_ZN8lam_node8set_bodyEP8exp_nodes, .Lfunc_end31-_ZN8lam_node8set_bodyEP8exp_nodes
	.cfi_endproc

	.globl	_ZN8lam_nodeC2ERKS_
	.p2align	4, 0x90
	.type	_ZN8lam_nodeC2ERKS_,@function
_ZN8lam_nodeC2ERKS_:                    # @_ZN8lam_nodeC2ERKS_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi172:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi173:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi174:
	.cfi_def_cfa_offset 32
.Lcfi175:
	.cfi_offset %rbx, -24
.Lcfi176:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV8lam_node+16, (%rbx)
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB32_1
# BB#2:
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.LBB32_4
# BB#3:
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	*96(%rcx)
	jmp	.LBB32_4
.LBB32_1:                               # %.thread
	movq	$0, 16(%rbx)
.LBB32_4:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB32_5
# BB#6:
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.LBB32_8
# BB#7:
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	*96(%rcx)
	jmp	.LBB32_8
.LBB32_5:                               # %.thread9
	movq	$0, 24(%rbx)
.LBB32_8:
	movq	$0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end32:
	.size	_ZN8lam_nodeC2ERKS_, .Lfunc_end32-_ZN8lam_nodeC2ERKS_
	.cfi_endproc

	.globl	_ZN8lam_nodeD2Ev
	.p2align	4, 0x90
	.type	_ZN8lam_nodeD2Ev,@function
_ZN8lam_nodeD2Ev:                       # @_ZN8lam_nodeD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi177:
	.cfi_def_cfa_offset 16
.Lcfi178:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV8lam_node+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB33_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB33_2:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB33_3
# BB#4:
	movq	(%rdi), %rax
	popq	%rbx
	jmpq	*16(%rax)               # TAILCALL
.LBB33_3:
	popq	%rbx
	retq
.Lfunc_end33:
	.size	_ZN8lam_nodeD2Ev, .Lfunc_end33-_ZN8lam_nodeD2Ev
	.cfi_endproc

	.globl	_ZN8lam_nodeD0Ev
	.p2align	4, 0x90
	.type	_ZN8lam_nodeD0Ev,@function
_ZN8lam_nodeD0Ev:                       # @_ZN8lam_nodeD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi179:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi180:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi181:
	.cfi_def_cfa_offset 32
.Lcfi182:
	.cfi_offset %rbx, -24
.Lcfi183:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV8lam_node+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB34_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp62:
	callq	*16(%rax)
.Ltmp63:
.LBB34_2:                               # %.noexc
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB34_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp64:
	callq	*16(%rax)
.Ltmp65:
.LBB34_4:                               # %_ZN8lam_nodeD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB34_5:
.Ltmp66:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end34:
	.size	_ZN8lam_nodeD0Ev, .Lfunc_end34-_ZN8lam_nodeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table34:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp62-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp65-.Ltmp62         #   Call between .Ltmp62 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin4   #     jumps to .Ltmp66
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end34-.Ltmp65    #   Call between .Ltmp65 and .Lfunc_end34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZNK8lam_node8has_freeEPK8arg_nodePK9alst_node
	.p2align	4, 0x90
	.type	_ZNK8lam_node8has_freeEPK8arg_nodePK9alst_node,@function
_ZNK8lam_node8has_freeEPK8arg_nodePK9alst_node: # @_ZNK8lam_node8has_freeEPK8arg_nodePK9alst_node
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi184:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi185:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi186:
	.cfi_def_cfa_offset 64
.Lcfi187:
	.cfi_offset %rbx, -24
.Lcfi188:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	16(%rbx), %rax
	movq	$0, 16(%rsp)
	movq	$_ZTV11stack_frame+16, 8(%rsp)
	movq	%rax, 24(%rsp)
	movq	%rdx, 32(%rsp)
	testq	%rax, %rax
	je	.LBB35_6
# BB#1:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	movq	16(%r14), %rsi
	je	.LBB35_5
# BB#2:                                 # %.thread.i
	testq	%rsi, %rsi
	je	.LBB35_6
# BB#3:
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB35_6
	jmp	.LBB35_9
.LBB35_5:                               # %_ZNK8arg_nodeeqERKS_.exit
	testq	%rsi, %rsi
	je	.LBB35_9
.LBB35_6:                               # %_ZNK8arg_nodeeqERKS_.exit.thread
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB35_9
# BB#7:
	movq	(%rdi), %rax
.Ltmp67:
	leaq	8(%rsp), %rdx
	movq	%r14, %rsi
	callq	*120(%rax)
                                        # kill: %AX<def> %AX<kill> %EAX<def>
.Ltmp68:
	jmp	.LBB35_10
.LBB35_9:
	xorl	%eax, %eax
.LBB35_10:
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB35_11:
.Ltmp69:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end35:
	.size	_ZNK8lam_node8has_freeEPK8arg_nodePK9alst_node, .Lfunc_end35-_ZNK8lam_node8has_freeEPK8arg_nodePK9alst_node
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table35:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp67-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp68-.Ltmp67         #   Call between .Ltmp67 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin5   #     jumps to .Ltmp69
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Lfunc_end35-.Ltmp68    #   Call between .Ltmp68 and .Lfunc_end35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11stack_frameD2Ev,"axG",@progbits,_ZN11stack_frameD2Ev,comdat
	.weak	_ZN11stack_frameD2Ev
	.p2align	4, 0x90
	.type	_ZN11stack_frameD2Ev,@function
_ZN11stack_frameD2Ev:                   # @_ZN11stack_frameD2Ev
	.cfi_startproc
# BB#0:                                 # %_ZN11arglst_nodeD2Ev.exit
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rdi)
	movq	$_ZTV11arglst_node+16, (%rdi)
	retq
.Lfunc_end36:
	.size	_ZN11stack_frameD2Ev, .Lfunc_end36-_ZN11stack_frameD2Ev
	.cfi_endproc

	.text
	.globl	_ZNK8lam_node5printEPK9alst_nodei
	.p2align	4, 0x90
	.type	_ZNK8lam_node5printEPK9alst_nodei,@function
_ZNK8lam_node5printEPK9alst_nodei:      # @_ZNK8lam_node5printEPK9alst_nodei
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi189:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi190:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi191:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi192:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi193:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi194:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi195:
	.cfi_def_cfa_offset 64
.Lcfi196:
	.cfi_offset %rbx, -56
.Lcfi197:
	.cfi_offset %r12, -48
.Lcfi198:
	.cfi_offset %r13, -40
.Lcfi199:
	.cfi_offset %r14, -32
.Lcfi200:
	.cfi_offset %r15, -24
.Lcfi201:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testb	$8, %bpl
	jne	.LBB37_1
.LBB37_31:                              # %.thread
	movl	$94, %edi
	callq	putchar
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	testq	%rax, %rax
	je	.LBB37_34
# BB#32:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	testq	%rax, %rax
	je	.LBB37_34
# BB#33:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	movq	%rax, %rcx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	jmp	.LBB37_35
.LBB37_1:
	testq	%r14, %r14
	je	.LBB37_3
# BB#2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*192(%rax)
	testq	%rax, %rax
	je	.LBB37_3
# BB#40:
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*48(%rcx)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	movq	%rax, %rcx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	printf                  # TAILCALL
.LBB37_34:
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	printf
.LBB37_35:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	testq	%rax, %rax
	je	.LBB37_38
# BB#36:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*40(%rcx)
	testq	%rax, %rax
	je	.LBB37_38
# BB#37:
	movl	$91, %edi
	callq	putchar
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*40(%rcx)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	*112(%rcx)
	movl	$93, %edi
	callq	putchar
.LBB37_38:
	movl	$46, %edi
	callq	putchar
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	testq	%rax, %rax
	je	.LBB37_39
# BB#42:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	movq	(%rax), %rcx
	movq	112(%rcx), %rcx
	movq	%rax, %rdi
	movq	%r14, %rsi
	movl	%ebp, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rcx                   # TAILCALL
.LBB37_3:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*24(%rcx)
	cmpl	$4, %eax
	jne	.LBB37_26
# BB#4:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	testq	%rax, %rax
	je	.LBB37_26
# BB#5:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	testq	%rax, %rax
	je	.LBB37_26
# BB#6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	testq	%rax, %rax
	je	.LBB37_26
# BB#7:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*48(%rcx)
	testq	%rax, %rax
	je	.LBB37_26
# BB#8:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*48(%rcx)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	testq	%rax, %rax
	je	.LBB37_26
# BB#9:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*56(%rcx)
	testq	%rax, %rax
	je	.LBB37_26
# BB#10:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*48(%rcx)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*56(%rcx)
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB37_11
# BB#12:                                # %.lr.ph.preheader
	movq	%r15, (%rsp)            # 8-byte Spill
	movl	$-1, %r15d
.LBB37_13:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*24(%rax)
	cmpl	$5, %eax
	jne	.LBB37_21
# BB#14:                                #   in Loop: Header=BB37_13 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*64(%rax)
	testq	%rax, %rax
	je	.LBB37_21
# BB#15:                                #   in Loop: Header=BB37_13 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*24(%rcx)
	cmpl	$3, %eax
	jne	.LBB37_21
# BB#16:                                #   in Loop: Header=BB37_13 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	testq	%rax, %rax
	je	.LBB37_21
# BB#17:                                #   in Loop: Header=BB37_13 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB37_21
# BB#18:                                #   in Loop: Header=BB37_13 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*72(%rax)
	movq	%rax, %r12
	incl	%r15d
	testq	%r12, %r12
	jne	.LBB37_13
# BB#19:                                # %._crit_edge
	cmpl	$-1, %r15d
	jl	.LBB37_31
# BB#20:
	incl	%r15d
	jmp	.LBB37_25
.LBB37_26:                              # %._crit_edge58
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	testq	%rax, %rax
	je	.LBB37_31
# BB#27:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	testq	%rax, %rax
	je	.LBB37_31
# BB#28:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	testq	%rax, %rax
	je	.LBB37_31
# BB#29:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*24(%rcx)
	cmpl	$3, %eax
	jne	.LBB37_31
# BB#30:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB37_31
# BB#41:
	movl	$73, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	putchar                 # TAILCALL
.LBB37_39:
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	printf                  # TAILCALL
.LBB37_21:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*24(%rax)
	cmpl	$3, %eax
	jne	.LBB37_31
# BB#22:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*32(%rax)
	testq	%rax, %rax
	je	.LBB37_31
# BB#23:
	incl	%r15d
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*32(%rax)
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rax, %rsi
	callq	strcmp
	testl	%r15d, %r15d
	js	.LBB37_31
# BB#24:
	testl	%eax, %eax
	je	.LBB37_25
	jmp	.LBB37_31
.LBB37_11:
	xorl	%r15d, %r15d
.LBB37_25:                              # %._crit_edge.thread
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	printf                  # TAILCALL
.Lfunc_end37:
	.size	_ZNK8lam_node5printEPK9alst_nodei, .Lfunc_end37-_ZNK8lam_node5printEPK9alst_nodei
	.cfi_endproc

	.globl	_ZN8lam_nodeaSERKS_
	.p2align	4, 0x90
	.type	_ZN8lam_nodeaSERKS_,@function
_ZN8lam_nodeaSERKS_:                    # @_ZN8lam_nodeaSERKS_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi202:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi203:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi204:
	.cfi_def_cfa_offset 32
.Lcfi205:
	.cfi_offset %rbx, -24
.Lcfi206:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpq	%r14, %rbx
	je	.LBB38_10
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB38_2
# BB#3:
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.LBB38_5
# BB#4:
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	*96(%rcx)
	jmp	.LBB38_5
.LBB38_2:                               # %.thread
	movq	$0, 16(%rbx)
.LBB38_5:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB38_6
# BB#7:
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.LBB38_9
# BB#8:
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	*96(%rcx)
	jmp	.LBB38_9
.LBB38_6:                               # %.thread9
	movq	$0, 24(%rbx)
.LBB38_9:
	movq	$0, 8(%rbx)
.LBB38_10:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end38:
	.size	_ZN8lam_nodeaSERKS_, .Lfunc_end38-_ZN8lam_nodeaSERKS_
	.cfi_endproc

	.globl	_ZN8lam_node6reduceEPK9alst_nodeiPi
	.p2align	4, 0x90
	.type	_ZN8lam_node6reduceEPK9alst_nodeiPi,@function
_ZN8lam_node6reduceEPK9alst_nodeiPi:    # @_ZN8lam_node6reduceEPK9alst_nodeiPi
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi207:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi208:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi209:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi210:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi211:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi212:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi213:
	.cfi_def_cfa_offset 112
.Lcfi214:
	.cfi_offset %rbx, -56
.Lcfi215:
	.cfi_offset %r12, -48
.Lcfi216:
	.cfi_offset %r13, -40
.Lcfi217:
	.cfi_offset %r14, -32
.Lcfi218:
	.cfi_offset %r15, -24
.Lcfi219:
	.cfi_offset %rbp, -16
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%edx, %r15d
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	16(%r12), %rax
	movq	$0, 32(%rsp)
	movq	$_ZTV11stack_frame+16, 24(%rsp)
	movq	%rax, 40(%rsp)
	movq	%r14, 48(%rsp)
	testb	$32, %r15b
	je	.LBB39_33
# BB#1:                                 # %.preheader
	cmpl	$0, _ZL27lambda_reduce_recurse_level(%rip)
	jle	.LBB39_4
# BB#2:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB39_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$46, %edi
	callq	putchar
	incl	%ebx
	cmpl	_ZL27lambda_reduce_recurse_level(%rip), %ebx
	jl	.LBB39_3
.LBB39_4:                               # %._crit_edge
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	printf
	movl	%r15d, %r13d
	andl	$8, %r13d
	jne	.LBB39_6
# BB#5:
	movq	(%r12), %rax
.Ltmp84:
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	%r15d, %edx
	callq	*112(%rax)
.Ltmp85:
	jmp	.LBB39_16
.LBB39_6:
	movq	(%r12), %rax
.Ltmp70:
	movq	%r12, %rdi
	callq	*(%rax)
	movq	%rax, %rbx
.Ltmp71:
# BB#7:                                 # %.noexc
	testq	%rbx, %rbx
	je	.LBB39_13
# BB#8:
	movq	(%rbx), %rax
.Ltmp72:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*200(%rax)
	movq	%rax, %rbp
.Ltmp73:
# BB#9:                                 # %.noexc122
	testq	%rbp, %rbp
	je	.LBB39_14
# BB#10:
	movq	(%rbp), %rax
.Ltmp74:
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movl	%r15d, %edx
	callq	*112(%rax)
.Ltmp75:
# BB#11:                                # %.noexc123
	cmpq	%rbx, %rbp
	je	.LBB39_15
# BB#12:
	movq	(%rbp), %rax
.Ltmp76:
	movq	%rbp, %rdi
	callq	*16(%rax)
.Ltmp77:
	jmp	.LBB39_15
.LBB39_13:
	movq	(%r12), %rax
.Ltmp82:
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	%r15d, %edx
	callq	*112(%rax)
.Ltmp83:
	jmp	.LBB39_16
.LBB39_14:
	movq	(%rbx), %rax
.Ltmp78:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%r15d, %edx
	callq	*112(%rax)
.Ltmp79:
.LBB39_15:                              # %.noexc124
	movq	(%rbx), %rax
.Ltmp80:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp81:
.LBB39_16:                              # %_ZNK8exp_node14symbolic_printEPK9alst_nodei.exit
	movl	$10, %edi
	callq	putchar
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB39_17:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
.Ltmp86:
	movq	%rbp, %rdi
	callq	*88(%rax)
.Ltmp87:
# BB#18:                                #   in Loop: Header=BB39_17 Depth=1
	testq	%rax, %rax
	je	.LBB39_20
# BB#19:                                #   in Loop: Header=BB39_17 Depth=1
	movq	(%rbp), %rax
.Ltmp88:
	movq	%rbp, %rdi
	callq	*88(%rax)
	movq	%rax, %rbp
.Ltmp89:
	jmp	.LBB39_17
.LBB39_20:
	testl	%r13d, %r13d
	jne	.LBB39_22
# BB#21:
	movq	(%rbp), %rax
.Ltmp105:
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movl	%r15d, %edx
	callq	*112(%rax)
.Ltmp106:
	jmp	.LBB39_32
.LBB39_22:
	movq	(%rbp), %rax
.Ltmp91:
	movq	%rbp, %rdi
	callq	*(%rax)
	movq	%rax, %rbx
.Ltmp92:
# BB#23:                                # %.noexc129
	testq	%rbx, %rbx
	je	.LBB39_29
# BB#24:
	movq	(%rbx), %rax
.Ltmp93:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*200(%rax)
	movq	%rax, %rbp
.Ltmp94:
# BB#25:                                # %.noexc130
	testq	%rbp, %rbp
	je	.LBB39_30
# BB#26:
	movq	(%rbp), %rax
.Ltmp95:
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movl	%r15d, %edx
	callq	*112(%rax)
.Ltmp96:
# BB#27:                                # %.noexc131
	cmpq	%rbx, %rbp
	je	.LBB39_31
# BB#28:
	movq	(%rbp), %rax
.Ltmp97:
	movq	%rbp, %rdi
	callq	*16(%rax)
.Ltmp98:
	jmp	.LBB39_31
.LBB39_29:
	movq	(%rbp), %rax
.Ltmp103:
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movl	%r15d, %edx
	callq	*112(%rax)
.Ltmp104:
	jmp	.LBB39_32
.LBB39_30:
	movq	(%rbx), %rax
.Ltmp99:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%r15d, %edx
	callq	*112(%rax)
.Ltmp100:
.LBB39_31:                              # %.noexc132
	movq	(%rbx), %rax
.Ltmp101:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp102:
.LBB39_32:                              # %_ZNK8exp_node14symbolic_printEPK9alst_nodei.exit137
	movl	$10, %edi
	callq	putchar
.LBB39_33:
	movl	_ZL27lambda_reduce_recurse_level(%rip), %eax
	leal	1(%rax), %esi
	movl	%esi, _ZL27lambda_reduce_recurse_level(%rip)
	cmpl	$4000, %eax             # imm = 0xFA0
	jl	.LBB39_36
# BB#34:
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	printf
	decl	_ZL27lambda_reduce_recurse_level(%rip)
	movq	16(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB39_60
# BB#35:
	orb	$1, (%rax)
	movq	%r12, %rbp
	jmp	.LBB39_116
.LBB39_36:
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp108:
	movq	%r12, %rsi
	callq	*96(%rax)
.Ltmp109:
# BB#37:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB39_55
# BB#38:
	movq	(%rdi), %rax
.Ltmp110:
	callq	*32(%rax)
.Ltmp111:
# BB#39:
	testq	%rax, %rax
	je	.LBB39_42
# BB#40:
	movq	16(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp112:
	callq	*32(%rax)
.Ltmp113:
# BB#41:
	movl	%r15d, %ecx
	andl	$-6, %ecx
	cmpb	$38, (%rax)
	cmovel	%ecx, %r15d
.LBB39_42:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB39_55
# BB#43:
	movq	(%rdi), %rax
.Ltmp114:
	callq	*40(%rax)
.Ltmp115:
# BB#44:
	testq	%rax, %rax
	je	.LBB39_55
# BB#45:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB39_62
# BB#46:
	movq	(%rdi), %rax
	movq	16(%r12), %rsi
.Ltmp116:
	leaq	24(%rsp), %rdx
	callq	*152(%rax)
.Ltmp117:
# BB#47:
	movq	16(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp118:
	callq	*32(%rax)
.Ltmp119:
# BB#48:
	testq	%rax, %rax
	je	.LBB39_52
# BB#49:
	movq	16(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp120:
	callq	*32(%rax)
.Ltmp121:
# BB#50:
	cmpb	$36, (%rax)
	jne	.LBB39_52
# BB#51:
	andl	$-6, %r15d
	orl	$4, %r15d
.LBB39_52:
	testb	$1, %r15b
	movq	16(%rsp), %rcx          # 8-byte Reload
	jne	.LBB39_63
# BB#53:
	testq	%rcx, %rcx
	je	.LBB39_83
# BB#54:
	movl	(%rcx), %eax
	jmp	.LBB39_84
.LBB39_55:                              # %.thread
	cmpq	$0, 24(%r12)
	movq	16(%rsp), %rcx          # 8-byte Reload
	je	.LBB39_62
# BB#56:
	movq	(%r12), %rax
.Ltmp140:
	leaq	24(%rsp), %rsi
	movq	%r12, %rdi
	movl	%r15d, %edx
	callq	*216(%rax)
	movq	%rax, %rbp
.Ltmp141:
# BB#57:
	cmpq	%rbp, %r12
	jne	.LBB39_115
# BB#58:
	movq	16(%rsp), %r14          # 8-byte Reload
	testq	%r14, %r14
	je	.LBB39_64
# BB#59:
	movl	(%r14), %eax
	jmp	.LBB39_65
.LBB39_60:
	movq	%r12, %rbp
	jmp	.LBB39_116
.LBB39_62:
	movq	%r12, %rbp
	jmp	.LBB39_115
.LBB39_63:
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp122:
	leaq	24(%rsp), %rsi
	movl	%r15d, %edx
	callq	*136(%rax)
	movq	%rax, %rbp
.Ltmp123:
	jmp	.LBB39_109
.LBB39_64:
	xorl	%eax, %eax
.LBB39_65:
	movl	%eax, 12(%rsp)
	testb	$4, %r15b
	jne	.LBB39_72
# BB#66:
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp150:
	leaq	24(%rsp), %rsi
	movl	%r15d, %edx
	movq	%r14, %rcx
	callq	*136(%rax)
	movq	%rax, %rbx
.Ltmp151:
# BB#67:
	testq	%rbx, %rbx
	je	.LBB39_78
# BB#68:
	movq	24(%r12), %rdi
	cmpq	%rbx, %rdi
	je	.LBB39_78
# BB#69:
	testq	%rdi, %rdi
	je	.LBB39_71
# BB#70:
	movq	(%rdi), %rax
.Ltmp152:
	callq	*16(%rax)
.Ltmp153:
.LBB39_71:
	movq	%rbx, 24(%r12)
	movq	(%rbx), %rax
.Ltmp154:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	*96(%rax)
.Ltmp155:
	jmp	.LBB39_78
.LBB39_72:
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp143:
	leaq	24(%rsp), %rsi
	leaq	12(%rsp), %rcx
	movl	%r15d, %edx
	callq	*104(%rax)
	movq	%rax, %rbx
.Ltmp144:
# BB#73:
	testq	%rbx, %rbx
	je	.LBB39_78
# BB#74:
	movq	24(%r12), %rdi
	cmpq	%rbx, %rdi
	je	.LBB39_78
# BB#75:
	testq	%rdi, %rdi
	je	.LBB39_77
# BB#76:
	movq	(%rdi), %rax
.Ltmp145:
	callq	*16(%rax)
.Ltmp146:
.LBB39_77:
	movq	%rbx, 24(%r12)
	movq	(%rbx), %rax
.Ltmp147:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	*96(%rax)
.Ltmp148:
.LBB39_78:                              # %_ZN8lam_node8set_bodyEP8exp_nodes.exit142
	testb	$1, %r15b
	je	.LBB39_80
# BB#79:
	testb	$2, 12(%rsp)
	movq	%r12, %rbp
	jne	.LBB39_81
.LBB39_80:
	movq	(%r12), %rax
.Ltmp157:
	leaq	24(%rsp), %rsi
	leaq	12(%rsp), %rcx
	movq	%r12, %rdi
	movl	%r15d, %edx
	callq	*216(%rax)
	movq	%rax, %rbp
.Ltmp158:
.LBB39_81:
	testq	%r14, %r14
	je	.LBB39_115
# BB#82:
	movl	12(%rsp), %eax
	movl	%eax, (%r14)
	jmp	.LBB39_115
.LBB39_83:
	xorl	%eax, %eax
.LBB39_84:
	movl	%eax, 12(%rsp)
	movq	24(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp124:
	leaq	24(%rsp), %rsi
	movl	%r15d, %edx
	callq	*136(%rax)
	movq	%rax, %rbx
.Ltmp125:
# BB#85:
	movq	24(%r12), %rdi
	cmpq	%rdi, %rbx
	je	.LBB39_89
# BB#86:
	testq	%rdi, %rdi
	je	.LBB39_89
# BB#87:
	movq	(%rdi), %rax
.Ltmp126:
	callq	*16(%rax)
.Ltmp127:
# BB#88:                                # %.noexc138
	movq	$0, 24(%r12)
.LBB39_89:                              # %_ZN8lam_node8set_bodyEP8exp_nodes.exit
	movq	16(%r12), %rbp
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB39_92
# BB#90:
	movq	(%rdi), %rax
.Ltmp128:
	callq	*16(%rax)
.Ltmp129:
# BB#91:                                # %.noexc139
	movq	$0, 24(%rbp)
.LBB39_92:                              # %_ZN8arg_node12import_valueEP8exp_node.exit.preheader
	leaq	12(%rsp), %r14
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB39_93:                              # %_ZN8arg_node12import_valueEP8exp_node.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
.Ltmp131:
	movq	%rbx, %rdi
	leaq	24(%rsp), %rsi
	movl	%r15d, %edx
	movq	%r14, %rcx
	callq	*104(%rax)
	movq	%rax, %rbp
.Ltmp132:
# BB#94:                                #   in Loop: Header=BB39_93 Depth=1
	testq	%rbp, %rbp
	je	.LBB39_101
# BB#95:                                #   in Loop: Header=BB39_93 Depth=1
	cmpq	%rbp, %rbx
	je	.LBB39_101
# BB#96:                                #   in Loop: Header=BB39_93 Depth=1
	cmpq	24(%r12), %rbx
	jne	.LBB39_98
# BB#97:                                #   in Loop: Header=BB39_93 Depth=1
	movq	$0, 24(%r12)
.LBB39_98:                              #   in Loop: Header=BB39_93 Depth=1
	testq	%rbx, %rbx
	je	.LBB39_102
# BB#99:                                #   in Loop: Header=BB39_93 Depth=1
	movq	(%rbx), %rax
.Ltmp133:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp134:
	jmp	.LBB39_102
	.p2align	4, 0x90
.LBB39_101:                             #   in Loop: Header=BB39_93 Depth=1
	movq	%rbx, %rbp
.LBB39_102:                             #   in Loop: Header=BB39_93 Depth=1
	movl	12(%rsp), %eax
	movl	%eax, %ecx
	andl	$3, %ecx
	cmpl	$2, %ecx
	jne	.LBB39_105
# BB#103:                               #   in Loop: Header=BB39_93 Depth=1
	andl	$-3, %eax
	movl	%eax, 12(%rsp)
	incl	%r13d
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB39_93
# BB#104:
	xorl	%ebp, %ebp
.LBB39_105:                             # %.thread147
	movq	16(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB39_107
# BB#106:
	movl	%eax, (%rcx)
.LBB39_107:
	cmpl	$2, %r13d
	jl	.LBB39_109
# BB#108:
	movl	_ZL27lambda_reduce_recurse_level(%rip), %edx
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	printf
.LBB39_109:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB39_112
# BB#110:
	cmpq	%rbp, %rdi
	je	.LBB39_112
# BB#111:
	movq	(%rdi), %rax
.Ltmp136:
	callq	*16(%rax)
.Ltmp137:
.LBB39_112:
	movq	$0, 24(%r12)
	testq	%rbp, %rbp
	je	.LBB39_114
# BB#113:
	movq	(%rbp), %rax
	movq	8(%r12), %rsi
.Ltmp138:
	movq	%rbp, %rdi
	callq	*96(%rax)
.Ltmp139:
	jmp	.LBB39_115
.LBB39_114:
	xorl	%ebp, %ebp
.LBB39_115:                             # %.thread151
	decl	_ZL27lambda_reduce_recurse_level(%rip)
.LBB39_116:
	movq	%rbp, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB39_117:
.Ltmp149:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB39_118:
.Ltmp156:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB39_119:
.Ltmp130:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB39_120:
.Ltmp159:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB39_121:
.Ltmp135:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB39_122:                             # %.loopexit.split-lp
.Ltmp107:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB39_123:
.Ltmp142:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB39_124:                             # %.loopexit
.Ltmp90:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end39:
	.size	_ZN8lam_node6reduceEPK9alst_nodeiPi, .Lfunc_end39-_ZN8lam_node6reduceEPK9alst_nodeiPi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table39:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\323\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\320\001"              # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp84-.Lfunc_begin6   #   Call between .Lfunc_begin6 and .Ltmp84
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp84-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp81-.Ltmp84         #   Call between .Ltmp84 and .Ltmp81
	.long	.Ltmp142-.Lfunc_begin6  #     jumps to .Ltmp142
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Ltmp86-.Ltmp81         #   Call between .Ltmp81 and .Ltmp86
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp86-.Lfunc_begin6   # >> Call Site 4 <<
	.long	.Ltmp89-.Ltmp86         #   Call between .Ltmp86 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin6   #     jumps to .Ltmp90
	.byte	0                       #   On action: cleanup
	.long	.Ltmp105-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp102-.Ltmp105       #   Call between .Ltmp105 and .Ltmp102
	.long	.Ltmp107-.Lfunc_begin6  #     jumps to .Ltmp107
	.byte	0                       #   On action: cleanup
	.long	.Ltmp102-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp108-.Ltmp102       #   Call between .Ltmp102 and .Ltmp108
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp108-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp123-.Ltmp108       #   Call between .Ltmp108 and .Ltmp123
	.long	.Ltmp142-.Lfunc_begin6  #     jumps to .Ltmp142
	.byte	0                       #   On action: cleanup
	.long	.Ltmp150-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Ltmp151-.Ltmp150       #   Call between .Ltmp150 and .Ltmp151
	.long	.Ltmp159-.Lfunc_begin6  #     jumps to .Ltmp159
	.byte	0                       #   On action: cleanup
	.long	.Ltmp152-.Lfunc_begin6  # >> Call Site 9 <<
	.long	.Ltmp155-.Ltmp152       #   Call between .Ltmp152 and .Ltmp155
	.long	.Ltmp156-.Lfunc_begin6  #     jumps to .Ltmp156
	.byte	0                       #   On action: cleanup
	.long	.Ltmp143-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp144-.Ltmp143       #   Call between .Ltmp143 and .Ltmp144
	.long	.Ltmp159-.Lfunc_begin6  #     jumps to .Ltmp159
	.byte	0                       #   On action: cleanup
	.long	.Ltmp145-.Lfunc_begin6  # >> Call Site 11 <<
	.long	.Ltmp148-.Ltmp145       #   Call between .Ltmp145 and .Ltmp148
	.long	.Ltmp149-.Lfunc_begin6  #     jumps to .Ltmp149
	.byte	0                       #   On action: cleanup
	.long	.Ltmp157-.Lfunc_begin6  # >> Call Site 12 <<
	.long	.Ltmp158-.Ltmp157       #   Call between .Ltmp157 and .Ltmp158
	.long	.Ltmp159-.Lfunc_begin6  #     jumps to .Ltmp159
	.byte	0                       #   On action: cleanup
	.long	.Ltmp124-.Lfunc_begin6  # >> Call Site 13 <<
	.long	.Ltmp129-.Ltmp124       #   Call between .Ltmp124 and .Ltmp129
	.long	.Ltmp130-.Lfunc_begin6  #     jumps to .Ltmp130
	.byte	0                       #   On action: cleanup
	.long	.Ltmp131-.Lfunc_begin6  # >> Call Site 14 <<
	.long	.Ltmp134-.Ltmp131       #   Call between .Ltmp131 and .Ltmp134
	.long	.Ltmp135-.Lfunc_begin6  #     jumps to .Ltmp135
	.byte	0                       #   On action: cleanup
	.long	.Ltmp136-.Lfunc_begin6  # >> Call Site 15 <<
	.long	.Ltmp139-.Ltmp136       #   Call between .Ltmp136 and .Ltmp139
	.long	.Ltmp142-.Lfunc_begin6  #     jumps to .Ltmp142
	.byte	0                       #   On action: cleanup
	.long	.Ltmp139-.Lfunc_begin6  # >> Call Site 16 <<
	.long	.Lfunc_end39-.Ltmp139   #   Call between .Ltmp139 and .Lfunc_end39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8lam_node10eta_reduceEPK9alst_nodeiPi
	.p2align	4, 0x90
	.type	_ZN8lam_node10eta_reduceEPK9alst_nodeiPi,@function
_ZN8lam_node10eta_reduceEPK9alst_nodeiPi: # @_ZN8lam_node10eta_reduceEPK9alst_nodeiPi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi220:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi221:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi222:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi223:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi224:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi225:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi226:
	.cfi_def_cfa_offset 64
.Lcfi227:
	.cfi_offset %rbx, -56
.Lcfi228:
	.cfi_offset %r12, -48
.Lcfi229:
	.cfi_offset %r13, -40
.Lcfi230:
	.cfi_offset %r14, -32
.Lcfi231:
	.cfi_offset %r15, -24
.Lcfi232:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %r13d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB40_30
# BB#1:
	movl	%r13d, %r15d
	andl	$1, %r15d
	je	.LBB40_4
# BB#2:
	testq	%r14, %r14
	je	.LBB40_4
# BB#3:
	testb	$2, (%r14)
	jne	.LBB40_30
.LBB40_4:
	movq	(%rdi), %rax
	callq	*24(%rax)
	testb	$2, %r13b
	jne	.LBB40_30
# BB#5:
	cmpl	$5, %eax
	jne	.LBB40_30
# BB#6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*64(%rcx)
	testq	%rax, %rax
	je	.LBB40_30
# BB#7:
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*64(%rax)
	movq	(%rax), %r8
	movq	%rax, %rdi
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rbp, %rsi
	movl	%r13d, %edx
	movq	%r14, %rcx
	callq	*136(%r8)
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB40_12
# BB#8:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	movq	%rax, %r12
	movq	16(%r12), %rdi
	cmpq	%rbp, %rdi
	je	.LBB40_12
# BB#9:
	testq	%rdi, %rdi
	je	.LBB40_11
# BB#10:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB40_11:
	movq	%rbp, 16(%r12)
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	*96(%rax)
.LBB40_12:                              # %_ZN8app_node8set_leftEP8exp_nodes.exit
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*72(%rax)
	testq	%rax, %rax
	je	.LBB40_30
# BB#13:
	movq	16(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB40_30
# BB#14:
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*72(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	movq	16(%rbp), %rdi
	movq	%rax, %rcx
	orq	%rdi, %rcx
	movq	(%rsp), %rbp            # 8-byte Reload
	je	.LBB40_18
# BB#15:
	testq	%rax, %rax
	je	.LBB40_30
# BB#16:
	testq	%rdi, %rdi
	je	.LBB40_30
# BB#17:                                # %_ZNK8arg_nodeeqEPKc.exit
	movq	%rax, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB40_30
.LBB40_18:                              # %_ZNK8arg_nodeeqEPKc.exit.thread36
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	16(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rbp, %rdx
	callq	*120(%rcx)
	testw	%ax, %ax
	jne	.LBB40_30
# BB#19:
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*176(%rax)
	movq	%rax, %r12
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB40_21
# BB#20:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 24(%rbx)
.LBB40_21:                              # %_ZN8lam_node8set_bodyEP8exp_nodes.exit
	testq	%r14, %r14
	je	.LBB40_23
# BB#22:
	orb	$6, (%r14)
.LBB40_23:
	testl	%r15d, %r15d
	je	.LBB40_25
.LBB40_29:
	movq	%r12, %rbx
	jmp	.LBB40_30
.LBB40_25:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%r13d, %edx
	movq	%r14, %rcx
	callq	*104(%rax)
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB40_29
# BB#26:
	cmpq	%rbx, %r12
	je	.LBB40_29
# BB#27:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*16(%rax)
.LBB40_30:                              # %_ZNK8arg_nodeeqEPKc.exit.thread
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end40:
	.size	_ZN8lam_node10eta_reduceEPK9alst_nodeiPi, .Lfunc_end40-_ZN8lam_node10eta_reduceEPK9alst_nodeiPi
	.cfi_endproc

	.globl	_ZN8app_node8set_leftEP8exp_nodes
	.p2align	4, 0x90
	.type	_ZN8app_node8set_leftEP8exp_nodes,@function
_ZN8app_node8set_leftEP8exp_nodes:      # @_ZN8app_node8set_leftEP8exp_nodes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi233:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi234:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi235:
	.cfi_def_cfa_offset 32
.Lcfi236:
	.cfi_offset %rbx, -32
.Lcfi237:
	.cfi_offset %r14, -24
.Lcfi238:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	16(%r14), %rdi
	cmpq	%rbx, %rdi
	je	.LBB41_8
# BB#1:
	testq	%rdi, %rdi
	je	.LBB41_3
# BB#2:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB41_3:
	testw	%bp, %bp
	jne	.LBB41_7
# BB#4:
	testq	%rbx, %rbx
	je	.LBB41_5
# BB#6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, %rbx
.LBB41_7:
	movq	%rbx, 16(%r14)
	testq	%rbx, %rbx
	je	.LBB41_8
# BB#9:
	movq	(%rbx), %rax
	movq	96(%rax), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.LBB41_5:                               # %.thread
	movq	$0, 16(%r14)
.LBB41_8:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end41:
	.size	_ZN8app_node8set_leftEP8exp_nodes, .Lfunc_end41-_ZN8app_node8set_leftEP8exp_nodes
	.cfi_endproc

	.globl	_ZN8lam_node11reduce_varsEPK9alst_nodeiPi
	.p2align	4, 0x90
	.type	_ZN8lam_node11reduce_varsEPK9alst_nodeiPi,@function
_ZN8lam_node11reduce_varsEPK9alst_nodeiPi: # @_ZN8lam_node11reduce_varsEPK9alst_nodeiPi
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi239:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi240:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi241:
	.cfi_def_cfa_offset 64
.Lcfi242:
	.cfi_offset %rbx, -24
.Lcfi243:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	16(%r14), %rax
	movq	$0, 16(%rsp)
	movq	$_ZTV11stack_frame+16, 8(%rsp)
	movq	%rax, 24(%rsp)
	movq	%rsi, 32(%rsp)
	movq	24(%r14), %rdi
	movq	(%rdi), %rax
.Ltmp160:
	leaq	8(%rsp), %rsi
	callq	*136(%rax)
	movq	%rax, %rbx
.Ltmp161:
# BB#1:
	testq	%rbx, %rbx
	je	.LBB42_6
# BB#2:
	movq	24(%r14), %rdi
	cmpq	%rbx, %rdi
	je	.LBB42_6
# BB#3:
	testq	%rdi, %rdi
	je	.LBB42_5
# BB#4:
	movq	(%rdi), %rax
.Ltmp163:
	callq	*16(%rax)
.Ltmp164:
.LBB42_5:
	movq	%rbx, 24(%r14)
	movq	(%rbx), %rax
.Ltmp165:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*96(%rax)
.Ltmp166:
.LBB42_6:                               # %_ZN8lam_node8set_bodyEP8exp_nodes.exit
	movq	%r14, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB42_9:
.Ltmp167:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB42_7:
.Ltmp162:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end42:
	.size	_ZN8lam_node11reduce_varsEPK9alst_nodeiPi, .Lfunc_end42-_ZN8lam_node11reduce_varsEPK9alst_nodeiPi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table42:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp160-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp161-.Ltmp160       #   Call between .Ltmp160 and .Ltmp161
	.long	.Ltmp162-.Lfunc_begin7  #     jumps to .Ltmp162
	.byte	0                       #   On action: cleanup
	.long	.Ltmp163-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp166-.Ltmp163       #   Call between .Ltmp163 and .Ltmp166
	.long	.Ltmp167-.Lfunc_begin7  #     jumps to .Ltmp167
	.byte	0                       #   On action: cleanup
	.long	.Ltmp166-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Lfunc_end42-.Ltmp166   #   Call between .Ltmp166 and .Lfunc_end42
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8lam_node6renameEP8arg_nodePKcP9alst_node
	.p2align	4, 0x90
	.type	_ZN8lam_node6renameEP8arg_nodePKcP9alst_node,@function
_ZN8lam_node6renameEP8arg_nodePKcP9alst_node: # @_ZN8lam_node6renameEP8arg_nodePKcP9alst_node
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	subq	$40, %rsp
.Lcfi244:
	.cfi_def_cfa_offset 48
	movq	16(%rdi), %rax
	movq	$0, 16(%rsp)
	movq	$_ZTV11stack_frame+16, 8(%rsp)
	movq	%rax, 24(%rsp)
	movq	%rcx, 32(%rsp)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB43_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp168:
	leaq	8(%rsp), %rcx
	callq	*144(%rax)
.Ltmp169:
.LBB43_2:
	addq	$40, %rsp
	retq
.LBB43_3:
.Ltmp170:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end43:
	.size	_ZN8lam_node6renameEP8arg_nodePKcP9alst_node, .Lfunc_end43-_ZN8lam_node6renameEP8arg_nodePKcP9alst_node
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table43:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp168-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp169-.Ltmp168       #   Call between .Ltmp168 and .Ltmp169
	.long	.Ltmp170-.Lfunc_begin8  #     jumps to .Ltmp170
	.byte	0                       #   On action: cleanup
	.long	.Ltmp169-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Lfunc_end43-.Ltmp169   #   Call between .Ltmp169 and .Lfunc_end43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8lam_node18resolve_name_clashEP8arg_nodeP9alst_node
	.p2align	4, 0x90
	.type	_ZN8lam_node18resolve_name_clashEP8arg_nodeP9alst_node,@function
_ZN8lam_node18resolve_name_clashEP8arg_nodeP9alst_node: # @_ZN8lam_node18resolve_name_clashEP8arg_nodeP9alst_node
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%rbp
.Lcfi245:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi246:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi247:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi248:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi249:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi250:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi251:
	.cfi_def_cfa_offset 224
.Lcfi252:
	.cfi_offset %rbx, -56
.Lcfi253:
	.cfi_offset %r12, -48
.Lcfi254:
	.cfi_offset %r13, -40
.Lcfi255:
	.cfi_offset %r14, -32
.Lcfi256:
	.cfi_offset %r15, -24
.Lcfi257:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	movq	16(%r13), %rax
	movq	$0, 8(%rsp)
	movq	$_ZTV11stack_frame+16, (%rsp)
	movq	%rax, 16(%rsp)
	movq	%rdx, 24(%rsp)
	movq	(%r14), %rax
.Ltmp171:
	movq	%r14, %rdi
	callq	*40(%rax)
.Ltmp172:
# BB#1:
	testq	%rax, %rax
	je	.LBB44_25
# BB#2:
	movq	(%r13), %rax
.Ltmp173:
	movq	%r13, %rdi
	callq	*48(%rax)
	movq	%rax, %r15
.Ltmp174:
# BB#3:
	movq	(%r13), %rax
.Ltmp176:
	movq	%r13, %rdi
	callq	*56(%rax)
	movq	%rax, %rbx
.Ltmp177:
# BB#4:
	cmpq	%r14, %r15
	je	.LBB44_21
# BB#5:
	testq	%r15, %r15
	je	.LBB44_21
# BB#6:
	testq	%rbx, %rbx
	je	.LBB44_21
# BB#7:
	movq	(%r14), %rax
.Ltmp179:
	movq	%r14, %rdi
	callq	*40(%rax)
.Ltmp180:
# BB#8:
	movq	(%rax), %rcx
.Ltmp181:
	movq	%rsp, %rdx
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	*120(%rcx)
	movl	%eax, %ebp
.Ltmp182:
# BB#9:
	movq	(%rbx), %rax
.Ltmp183:
	movq	%rsp, %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*120(%rax)
.Ltmp184:
# BB#10:
	testw	%bp, %bp
	je	.LBB44_21
# BB#11:
	testw	%ax, %ax
	je	.LBB44_21
# BB#12:
	movq	(%r15), %rax
.Ltmp186:
	movq	%r15, %rdi
	callq	*32(%rax)
.Ltmp187:
# BB#13:
	testq	%rax, %rax
	movl	$.L.str.29, %edx
	cmovneq	%rax, %rdx
	movl	_ZL13name_sequence(%rip), %ecx
	leaq	32(%rsp), %rbx
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	incl	_ZL13name_sequence(%rip)
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp188:
	callq	_Znam
	movq	%rax, %r12
.Ltmp189:
# BB#14:
	leaq	32(%rsp), %rsi
	movq	%r12, %rdi
	callq	strcpy
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
.Ltmp191:
	movq	%rsp, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	*144(%rax)
.Ltmp192:
# BB#15:
	movq	16(%r15), %rdi
	cmpq	%r12, %rdi
	je	.LBB44_20
# BB#16:
	testq	%rdi, %rdi
	je	.LBB44_18
# BB#17:
	callq	_ZdaPv
.LBB44_18:
	movq	%r12, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp193:
	callq	_Znam
.Ltmp194:
# BB#19:                                # %.noexc
	movq	%rax, 16(%r15)
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	strcpy
.LBB44_20:                              # %_ZN8arg_node8set_nameEPKc.exit
	movq	%r12, %rdi
	callq	_ZdaPv
.LBB44_21:
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB44_25
# BB#22:
	movq	(%rdi), %rax
.Ltmp196:
	movq	%rsp, %rdx
	movq	%r14, %rsi
	callq	*120(%rax)
.Ltmp197:
# BB#23:
	testw	%ax, %ax
	je	.LBB44_25
# BB#24:
	movq	24(%r13), %rdi
	movq	(%rdi), %rax
.Ltmp198:
	movq	%rsp, %rdx
	movq	%r14, %rsi
	callq	*152(%rax)
.Ltmp199:
.LBB44_25:
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB44_30:
.Ltmp195:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB44_29:
.Ltmp190:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB44_28:
.Ltmp185:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB44_26:
.Ltmp178:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB44_27:
.Ltmp200:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB44_31:
.Ltmp175:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end44:
	.size	_ZN8lam_node18resolve_name_clashEP8arg_nodeP9alst_node, .Lfunc_end44-_ZN8lam_node18resolve_name_clashEP8arg_nodeP9alst_node
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table44:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\352\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp171-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp174-.Ltmp171       #   Call between .Ltmp171 and .Ltmp174
	.long	.Ltmp175-.Lfunc_begin9  #     jumps to .Ltmp175
	.byte	0                       #   On action: cleanup
	.long	.Ltmp176-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp177-.Ltmp176       #   Call between .Ltmp176 and .Ltmp177
	.long	.Ltmp178-.Lfunc_begin9  #     jumps to .Ltmp178
	.byte	0                       #   On action: cleanup
	.long	.Ltmp179-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp182-.Ltmp179       #   Call between .Ltmp179 and .Ltmp182
	.long	.Ltmp200-.Lfunc_begin9  #     jumps to .Ltmp200
	.byte	0                       #   On action: cleanup
	.long	.Ltmp183-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Ltmp184-.Ltmp183       #   Call between .Ltmp183 and .Ltmp184
	.long	.Ltmp185-.Lfunc_begin9  #     jumps to .Ltmp185
	.byte	0                       #   On action: cleanup
	.long	.Ltmp186-.Lfunc_begin9  # >> Call Site 5 <<
	.long	.Ltmp189-.Ltmp186       #   Call between .Ltmp186 and .Ltmp189
	.long	.Ltmp190-.Lfunc_begin9  #     jumps to .Ltmp190
	.byte	0                       #   On action: cleanup
	.long	.Ltmp191-.Lfunc_begin9  # >> Call Site 6 <<
	.long	.Ltmp194-.Ltmp191       #   Call between .Ltmp191 and .Ltmp194
	.long	.Ltmp195-.Lfunc_begin9  #     jumps to .Ltmp195
	.byte	0                       #   On action: cleanup
	.long	.Ltmp196-.Lfunc_begin9  # >> Call Site 7 <<
	.long	.Ltmp199-.Ltmp196       #   Call between .Ltmp196 and .Ltmp199
	.long	.Ltmp200-.Lfunc_begin9  #     jumps to .Ltmp200
	.byte	0                       #   On action: cleanup
	.long	.Ltmp199-.Lfunc_begin9  # >> Call Site 8 <<
	.long	.Lfunc_end44-.Ltmp199   #   Call between .Ltmp199 and .Lfunc_end44
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZNK8lam_nodeeqERK8exp_node
	.p2align	4, 0x90
	.type	_ZNK8lam_nodeeqERK8exp_node,@function
_ZNK8lam_nodeeqERK8exp_node:            # @_ZNK8lam_nodeeqERK8exp_node
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi258:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi259:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi260:
	.cfi_def_cfa_offset 32
.Lcfi261:
	.cfi_offset %rbx, -32
.Lcfi262:
	.cfi_offset %r14, -24
.Lcfi263:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*24(%rax)
	xorl	%ebp, %ebp
	cmpl	$4, %eax
	jne	.LBB45_14
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*48(%rax)
	testq	%rax, %rax
	je	.LBB45_14
# BB#2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	testq	%rax, %rax
	je	.LBB45_14
# BB#3:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*56(%rax)
	testq	%rax, %rax
	je	.LBB45_14
# BB#4:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	testq	%rax, %rax
	je	.LBB45_14
# BB#5:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*48(%rax)
	movq	%rax, %rbp
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	movq	16(%rax), %rsi
	je	.LBB45_9
# BB#6:                                 # %.thread.i
	testq	%rsi, %rsi
	je	.LBB45_12
# BB#7:
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB45_12
	jmp	.LBB45_11
.LBB45_9:                               # %_ZNK8arg_nodeeqERKS_.exit
	testq	%rsi, %rsi
	je	.LBB45_11
.LBB45_12:
	xorl	%eax, %eax
	jmp	.LBB45_13
.LBB45_11:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*56(%rax)
	movq	%rax, %rbp
	movq	(%rbp), %rax
	movq	160(%rax), %r14
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	*%r14
	testl	%eax, %eax
	setne	%al
.LBB45_13:                              # %_ZNK8arg_nodeeqERKS_.exit.thread
	movzbl	%al, %ebp
.LBB45_14:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end45:
	.size	_ZNK8lam_nodeeqERK8exp_node, .Lfunc_end45-_ZNK8lam_nodeeqERK8exp_node
	.cfi_endproc

	.globl	_ZN8lam_node12extract_defsEPK9alst_node
	.p2align	4, 0x90
	.type	_ZN8lam_node12extract_defsEPK9alst_node,@function
_ZN8lam_node12extract_defsEPK9alst_node: # @_ZN8lam_node12extract_defsEPK9alst_node
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r15
.Lcfi264:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi265:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi266:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi267:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi268:
	.cfi_def_cfa_offset 48
.Lcfi269:
	.cfi_offset %rbx, -40
.Lcfi270:
	.cfi_offset %r12, -32
.Lcfi271:
	.cfi_offset %r14, -24
.Lcfi272:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	(%r12), %rax
	callq	*192(%rax)
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB46_1
# BB#14:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r12
	movq	(%rbx), %rax
.Ltmp201:
	movq	%rbx, %rdi
	callq	*48(%rax)
.Ltmp202:
# BB#15:
	movq	(%rax), %rcx
.Ltmp203:
	movq	%rax, %rdi
	callq	*32(%rcx)
	movq	%rax, %r14
.Ltmp204:
# BB#16:
	movq	$0, 8(%r12)
	movq	$_ZTV8var_node+16, (%r12)
	testq	%r14, %r14
	je	.LBB46_19
# BB#17:
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp205:
	callq	_Znam
.Ltmp206:
.LBB46_18:                              # %.noexc22
	movq	%rax, 16(%r12)
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	strcpy
	jmp	.LBB46_19
.LBB46_1:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*56(%rax)
	testq	%rax, %rax
	je	.LBB46_19
# BB#2:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*56(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	*200(%rcx)
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB46_7
# BB#3:
	movq	24(%r12), %rdi
	cmpq	%r15, %rdi
	je	.LBB46_7
# BB#4:
	testq	%rdi, %rdi
	je	.LBB46_6
# BB#5:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB46_6:
	movq	%r15, 24(%r12)
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	*96(%rax)
.LBB46_7:                               # %_ZN8lam_node8set_bodyEP8exp_nodes.exit
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	*192(%rax)
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB46_19
# BB#8:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r12
	movq	(%rbx), %rax
.Ltmp208:
	movq	%rbx, %rdi
	callq	*48(%rax)
.Ltmp209:
# BB#9:
	movq	(%rax), %rcx
.Ltmp210:
	movq	%rax, %rdi
	callq	*32(%rcx)
	movq	%rax, %r14
.Ltmp211:
# BB#10:
	movq	$0, 8(%r12)
	movq	$_ZTV8var_node+16, (%r12)
	testq	%r14, %r14
	je	.LBB46_19
# BB#11:
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp212:
	callq	_Znam
.Ltmp213:
	jmp	.LBB46_18
.LBB46_19:
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB46_12:
.Ltmp214:
	jmp	.LBB46_13
.LBB46_20:
.Ltmp207:
.LBB46_13:
	movq	%rax, %r14
	movq	%r12, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end46:
	.size	_ZN8lam_node12extract_defsEPK9alst_node, .Lfunc_end46-_ZN8lam_node12extract_defsEPK9alst_node
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table46:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin10-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp201-.Lfunc_begin10 #   Call between .Lfunc_begin10 and .Ltmp201
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp201-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp206-.Ltmp201       #   Call between .Ltmp201 and .Ltmp206
	.long	.Ltmp207-.Lfunc_begin10 #     jumps to .Ltmp207
	.byte	0                       #   On action: cleanup
	.long	.Ltmp206-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp208-.Ltmp206       #   Call between .Ltmp206 and .Ltmp208
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp208-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Ltmp213-.Ltmp208       #   Call between .Ltmp208 and .Ltmp213
	.long	.Ltmp214-.Lfunc_begin10 #     jumps to .Ltmp214
	.byte	0                       #   On action: cleanup
	.long	.Ltmp213-.Lfunc_begin10 # >> Call Site 5 <<
	.long	.Lfunc_end46-.Ltmp213   #   Call between .Ltmp213 and .Lfunc_end46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZNK8lam_node7extractEPKci
	.p2align	4, 0x90
	.type	_ZNK8lam_node7extractEPKci,@function
_ZNK8lam_node7extractEPKci:             # @_ZNK8lam_node7extractEPKci
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi273:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi274:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi275:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi276:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi277:
	.cfi_def_cfa_offset 48
.Lcfi278:
	.cfi_offset %rbx, -48
.Lcfi279:
	.cfi_offset %r12, -40
.Lcfi280:
	.cfi_offset %r14, -32
.Lcfi281:
	.cfi_offset %r15, -24
.Lcfi282:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movq	(%rbp), %rax
	callq	*56(%rax)
	testq	%rax, %rax
	je	.LBB47_1
# BB#3:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*48(%rax)
	testq	%rax, %rax
	je	.LBB47_1
# BB#4:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*56(%rax)
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	208(%rax), %r12
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*48(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movl	%r14d, %edx
	callq	*%r12
	movq	%rax, %rbp
	testq	%r15, %r15
	je	.LBB47_2
# BB#5:
	testq	%rbp, %rbp
	je	.LBB47_2
# BB#6:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movl	%r14d, %edx
	callq	*208(%rax)
	movq	%rax, %rbx
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*16(%rax)
	movq	%rbx, %rax
	jmp	.LBB47_7
.LBB47_1:
	xorl	%ebp, %ebp
.LBB47_2:
	movq	%rbp, %rax
.LBB47_7:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end47:
	.size	_ZNK8lam_node7extractEPKci, .Lfunc_end47-_ZNK8lam_node7extractEPKci
	.cfi_endproc

	.globl	_ZN8app_nodeC2EP8exp_nodeS1_s
	.p2align	4, 0x90
	.type	_ZN8app_nodeC2EP8exp_nodeS1_s,@function
_ZN8app_nodeC2EP8exp_nodeS1_s:          # @_ZN8app_nodeC2EP8exp_nodeS1_s
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi283:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi284:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi285:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi286:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi287:
	.cfi_def_cfa_offset 48
.Lcfi288:
	.cfi_offset %rbx, -40
.Lcfi289:
	.cfi_offset %r14, -32
.Lcfi290:
	.cfi_offset %r15, -24
.Lcfi291:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV8app_node+16, (%rbx)
	testq	%rsi, %rsi
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	je	.LBB48_1
# BB#2:                                 # %.noexc10
	leaq	16(%rbx), %rbp
	testw	%r15w, %r15w
	je	.LBB48_4
# BB#3:                                 # %.thread
	movq	%rsi, (%rbp)
	jmp	.LBB48_5
.LBB48_1:
	xorl	%edi, %edi
	cmpq	%r14, %rdi
	jne	.LBB48_8
	jmp	.LBB48_15
.LBB48_4:
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	callq	*(%rax)
	movq	%rax, %rsi
	movq	%rsi, (%rbp)
	testq	%rsi, %rsi
	je	.LBB48_6
.LBB48_5:
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.LBB48_6:                               # %_ZN8app_node8set_leftEP8exp_nodes.exitthread-pre-split
	movq	24(%rbx), %rdi
	cmpq	%r14, %rdi
	je	.LBB48_15
.LBB48_8:
	testq	%rdi, %rdi
	je	.LBB48_10
# BB#9:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB48_10:                              # %.noexc
	testw	%r15w, %r15w
	jne	.LBB48_14
# BB#11:
	testq	%r14, %r14
	je	.LBB48_12
# BB#13:                                # %.noexc6
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	movq	%rax, %r14
.LBB48_14:
	movq	%r14, 24(%rbx)
	testq	%r14, %r14
	je	.LBB48_15
# BB#16:
	movq	(%r14), %rax
	movq	96(%rax), %rax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.LBB48_12:                              # %.thread.i
	movq	$0, 24(%rbx)
.LBB48_15:                              # %_ZN8app_node9set_rightEP8exp_nodes.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end48:
	.size	_ZN8app_nodeC2EP8exp_nodeS1_s, .Lfunc_end48-_ZN8app_nodeC2EP8exp_nodeS1_s
	.cfi_endproc

	.globl	_ZN8app_node9set_rightEP8exp_nodes
	.p2align	4, 0x90
	.type	_ZN8app_node9set_rightEP8exp_nodes,@function
_ZN8app_node9set_rightEP8exp_nodes:     # @_ZN8app_node9set_rightEP8exp_nodes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi292:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi293:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi294:
	.cfi_def_cfa_offset 32
.Lcfi295:
	.cfi_offset %rbx, -32
.Lcfi296:
	.cfi_offset %r14, -24
.Lcfi297:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	24(%r14), %rdi
	cmpq	%rbx, %rdi
	je	.LBB49_8
# BB#1:
	testq	%rdi, %rdi
	je	.LBB49_3
# BB#2:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB49_3:
	testw	%bp, %bp
	jne	.LBB49_7
# BB#4:
	testq	%rbx, %rbx
	je	.LBB49_5
# BB#6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, %rbx
.LBB49_7:
	movq	%rbx, 24(%r14)
	testq	%rbx, %rbx
	je	.LBB49_8
# BB#9:
	movq	(%rbx), %rax
	movq	96(%rax), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.LBB49_5:                               # %.thread
	movq	$0, 24(%r14)
.LBB49_8:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end49:
	.size	_ZN8app_node9set_rightEP8exp_nodes, .Lfunc_end49-_ZN8app_node9set_rightEP8exp_nodes
	.cfi_endproc

	.globl	_ZN8app_nodeC2ERKS_
	.p2align	4, 0x90
	.type	_ZN8app_nodeC2ERKS_,@function
_ZN8app_nodeC2ERKS_:                    # @_ZN8app_nodeC2ERKS_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi298:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi299:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi300:
	.cfi_def_cfa_offset 32
.Lcfi301:
	.cfi_offset %rbx, -24
.Lcfi302:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV8app_node+16, (%rbx)
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB50_1
# BB#2:
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.LBB50_4
# BB#3:
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	*96(%rcx)
	jmp	.LBB50_4
.LBB50_1:                               # %.thread
	movq	$0, 16(%rbx)
.LBB50_4:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB50_5
# BB#6:
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.LBB50_8
# BB#7:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
	jmp	.LBB50_8
.LBB50_5:                               # %.thread10
	movq	$0, 24(%rbx)
.LBB50_8:
	movq	8(%r14), %rax
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end50:
	.size	_ZN8app_nodeC2ERKS_, .Lfunc_end50-_ZN8app_nodeC2ERKS_
	.cfi_endproc

	.globl	_ZN8app_nodeD2Ev
	.p2align	4, 0x90
	.type	_ZN8app_nodeD2Ev,@function
_ZN8app_nodeD2Ev:                       # @_ZN8app_nodeD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi303:
	.cfi_def_cfa_offset 16
.Lcfi304:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV8app_node+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB51_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB51_2:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB51_3
# BB#4:
	movq	(%rdi), %rax
	popq	%rbx
	jmpq	*16(%rax)               # TAILCALL
.LBB51_3:
	popq	%rbx
	retq
.Lfunc_end51:
	.size	_ZN8app_nodeD2Ev, .Lfunc_end51-_ZN8app_nodeD2Ev
	.cfi_endproc

	.globl	_ZN8app_nodeD0Ev
	.p2align	4, 0x90
	.type	_ZN8app_nodeD0Ev,@function
_ZN8app_nodeD0Ev:                       # @_ZN8app_nodeD0Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi305:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi306:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi307:
	.cfi_def_cfa_offset 32
.Lcfi308:
	.cfi_offset %rbx, -24
.Lcfi309:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV8app_node+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB52_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp215:
	callq	*16(%rax)
.Ltmp216:
.LBB52_2:                               # %.noexc
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB52_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp217:
	callq	*16(%rax)
.Ltmp218:
.LBB52_4:                               # %_ZN8app_nodeD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB52_5:
.Ltmp219:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end52:
	.size	_ZN8app_nodeD0Ev, .Lfunc_end52-_ZN8app_nodeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table52:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp215-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp218-.Ltmp215       #   Call between .Ltmp215 and .Ltmp218
	.long	.Ltmp219-.Lfunc_begin11 #     jumps to .Ltmp219
	.byte	0                       #   On action: cleanup
	.long	.Ltmp218-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Lfunc_end52-.Ltmp218   #   Call between .Ltmp218 and .Lfunc_end52
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZNK8app_node8has_freeEPK8arg_nodePK9alst_node
	.p2align	4, 0x90
	.type	_ZNK8app_node8has_freeEPK8arg_nodePK9alst_node,@function
_ZNK8app_node8has_freeEPK8arg_nodePK9alst_node: # @_ZNK8app_node8has_freeEPK8arg_nodePK9alst_node
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi310:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi311:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi312:
	.cfi_def_cfa_offset 32
.Lcfi313:
	.cfi_offset %rbx, -32
.Lcfi314:
	.cfi_offset %r14, -24
.Lcfi315:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB53_1
# BB#2:
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	*120(%rax)
	movl	%eax, %ecx
	xorl	%eax, %eax
	testw	%cx, %cx
	setne	%al
	jmp	.LBB53_3
.LBB53_1:
	xorl	%eax, %eax
.LBB53_3:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB53_7
# BB#4:
	movb	$1, %cl
	testw	%ax, %ax
	jne	.LBB53_6
# BB#5:
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	*120(%rax)
	testw	%ax, %ax
	setne	%cl
.LBB53_6:
	movzbl	%cl, %eax
.LBB53_7:
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end53:
	.size	_ZNK8app_node8has_freeEPK8arg_nodePK9alst_node, .Lfunc_end53-_ZNK8app_node8has_freeEPK8arg_nodePK9alst_node
	.cfi_endproc

	.globl	_ZNK8app_node5printEPK9alst_nodei
	.p2align	4, 0x90
	.type	_ZNK8app_node5printEPK9alst_nodei,@function
_ZNK8app_node5printEPK9alst_nodei:      # @_ZNK8app_node5printEPK9alst_nodei
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi316:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi317:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi318:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi319:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi320:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi321:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi322:
	.cfi_def_cfa_offset 64
.Lcfi323:
	.cfi_offset %rbx, -56
.Lcfi324:
	.cfi_offset %r12, -48
.Lcfi325:
	.cfi_offset %r13, -40
.Lcfi326:
	.cfi_offset %r14, -32
.Lcfi327:
	.cfi_offset %r15, -24
.Lcfi328:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*64(%rax)
	testb	$16, %r14b
	jne	.LBB54_7
# BB#1:
	testq	%rax, %rax
	je	.LBB54_12
# BB#2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*24(%rcx)
	cmpl	$3, %eax
	jne	.LBB54_4
# BB#3:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*40(%rcx)
	testq	%rax, %rax
	je	.LBB54_5
.LBB54_4:
	movl	$40, %edi
	callq	putchar
.LBB54_5:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	testq	%rax, %rax
	je	.LBB54_20
# BB#6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%r12, %rsi
	movl	%r14d, %edx
	callq	*112(%rcx)
	jmp	.LBB54_21
.LBB54_7:
	testq	%rax, %rax
	movq	%r12, (%rsp)            # 8-byte Spill
	je	.LBB54_13
# BB#8:
	xorl	%ebp, %ebp
	movl	%r14d, %r13d
	andl	$8, %r13d
	je	.LBB54_10
# BB#9:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	*192(%rcx)
	xorl	%ebp, %ebp
	testq	%rax, %rax
	setne	%bpl
.LBB54_10:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*24(%rcx)
	cmpl	$3, %eax
	jne	.LBB54_14
.LBB54_11:
	xorl	%r12d, %r12d
	jmp	.LBB54_16
.LBB54_12:
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB54_24
.LBB54_13:
	xorl	%r15d, %r15d
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$1, %r13d
	jmp	.LBB54_44
.LBB54_14:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*24(%rcx)
	testl	%ebp, %ebp
	jne	.LBB54_11
# BB#15:
	movl	$1, %r12d
	cmpl	$5, %eax
	je	.LBB54_11
.LBB54_16:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*24(%rcx)
	xorl	%r15d, %r15d
	cmpl	$5, %eax
	jne	.LBB54_36
# BB#17:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*72(%rcx)
	testq	%rax, %rax
	je	.LBB54_36
# BB#18:
	testl	%r13d, %r13d
	je	.LBB54_34
# BB#19:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*72(%rcx)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	*192(%rcx)
	testq	%rax, %rax
	setne	%bpl
	jmp	.LBB54_35
.LBB54_20:
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	callq	printf
.LBB54_21:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*24(%rcx)
	cmpl	$3, %eax
	jne	.LBB54_23
# BB#22:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*40(%rcx)
	testq	%rax, %rax
	je	.LBB54_24
.LBB54_23:
	movl	$41, %edi
	callq	putchar
.LBB54_24:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*72(%rax)
	testq	%rax, %rax
	je	.LBB54_47
# BB#25:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*72(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*24(%rcx)
	cmpl	$3, %eax
	jne	.LBB54_27
# BB#26:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*72(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*40(%rcx)
	testq	%rax, %rax
	je	.LBB54_33
.LBB54_27:
	movl	$40, %edi
.LBB54_28:
	callq	putchar
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*72(%rax)
	testq	%rax, %rax
	je	.LBB54_30
# BB#29:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*72(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%r12, %rsi
	movl	%r14d, %edx
	callq	*112(%rcx)
	jmp	.LBB54_31
.LBB54_30:
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	callq	printf
.LBB54_31:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*72(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*24(%rcx)
	cmpl	$3, %eax
	jne	.LBB54_58
# BB#32:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*72(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*40(%rcx)
	testq	%rax, %rax
	jne	.LBB54_58
	jmp	.LBB54_57
.LBB54_33:
	movl	$32, %edi
	jmp	.LBB54_28
.LBB54_34:
	xorl	%ebp, %ebp
.LBB54_35:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*72(%rcx)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*24(%rcx)
	cmpl	$3, %eax
	setne	%al
	notb	%bpl
	andb	%al, %bpl
	movzbl	%bpl, %r15d
.LBB54_36:
	testl	%r12d, %r12d
	je	.LBB54_38
# BB#37:
	movl	$40, %edi
	callq	putchar
.LBB54_38:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	testq	%rax, %rax
	je	.LBB54_40
# BB#39:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%r14d, %edx
	callq	*112(%rcx)
	testl	%r12d, %r12d
	jne	.LBB54_41
	jmp	.LBB54_42
.LBB54_40:
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	callq	printf
	testl	%r12d, %r12d
	je	.LBB54_42
.LBB54_41:
	movl	$41, %edi
	callq	putchar
	movl	$1, %r13d
	jmp	.LBB54_43
.LBB54_42:
	xorl	%r13d, %r13d
.LBB54_43:
	movq	(%rsp), %r12            # 8-byte Reload
.LBB54_44:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*72(%rax)
	testq	%rax, %rax
	je	.LBB54_47
# BB#45:
	testb	$8, %r14b
	jne	.LBB54_48
# BB#46:
	xorl	%ebp, %ebp
	jmp	.LBB54_49
.LBB54_47:
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	printf                  # TAILCALL
.LBB54_48:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*72(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	*192(%rcx)
	testq	%rax, %rax
	setne	%bpl
.LBB54_49:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*72(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*24(%rcx)
	cmpl	$3, %eax
	sete	%r12b
	orb	%bpl, %r12b
	je	.LBB54_52
# BB#50:
	orl	%r13d, %r15d
	jne	.LBB54_54
# BB#51:
	movl	$32, %edi
	jmp	.LBB54_53
.LBB54_52:
	movl	$40, %edi
.LBB54_53:
	callq	putchar
.LBB54_54:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*72(%rax)
	testq	%rax, %rax
	je	.LBB54_56
# BB#55:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*72(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	%r14d, %edx
	callq	*112(%rcx)
	testb	$1, %r12b
	je	.LBB54_58
	jmp	.LBB54_57
.LBB54_56:
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	callq	printf
	testb	$1, %r12b
	je	.LBB54_58
.LBB54_57:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB54_58:
	movl	$41, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	putchar                 # TAILCALL
.Lfunc_end54:
	.size	_ZNK8app_node5printEPK9alst_nodei, .Lfunc_end54-_ZNK8app_node5printEPK9alst_nodei
	.cfi_endproc

	.globl	_ZN8app_nodeaSERKS_
	.p2align	4, 0x90
	.type	_ZN8app_nodeaSERKS_,@function
_ZN8app_nodeaSERKS_:                    # @_ZN8app_nodeaSERKS_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi329:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi330:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi331:
	.cfi_def_cfa_offset 32
.Lcfi332:
	.cfi_offset %rbx, -24
.Lcfi333:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpq	%r14, %rbx
	je	.LBB55_10
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB55_2
# BB#3:
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.LBB55_5
# BB#4:
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	*96(%rcx)
	jmp	.LBB55_5
.LBB55_2:                               # %.thread
	movq	$0, 16(%rbx)
.LBB55_5:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB55_6
# BB#7:
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.LBB55_9
# BB#8:
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	*96(%rcx)
	jmp	.LBB55_9
.LBB55_6:                               # %.thread10
	movq	$0, 24(%rbx)
.LBB55_9:
	movq	8(%r14), %rax
	movq	%rax, 8(%rbx)
.LBB55_10:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end55:
	.size	_ZN8app_nodeaSERKS_, .Lfunc_end55-_ZN8app_nodeaSERKS_
	.cfi_endproc

	.globl	_ZN8app_node6renameEP8arg_nodePKcP9alst_node
	.p2align	4, 0x90
	.type	_ZN8app_node6renameEP8arg_nodePKcP9alst_node,@function
_ZN8app_node6renameEP8arg_nodePKcP9alst_node: # @_ZN8app_node6renameEP8arg_nodePKcP9alst_node
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi334:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi335:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi336:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi337:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi338:
	.cfi_def_cfa_offset 48
.Lcfi339:
	.cfi_offset %rbx, -40
.Lcfi340:
	.cfi_offset %r12, -32
.Lcfi341:
	.cfi_offset %r14, -24
.Lcfi342:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB56_2
# BB#1:
	movq	(%rdi), %rax
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	*144(%rax)
.LBB56_2:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB56_3
# BB#4:
	movq	(%rdi), %rax
	movq	144(%rax), %rax
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.LBB56_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end56:
	.size	_ZN8app_node6renameEP8arg_nodePKcP9alst_node, .Lfunc_end56-_ZN8app_node6renameEP8arg_nodePKcP9alst_node
	.cfi_endproc

	.globl	_ZN8app_node18resolve_name_clashEP8arg_nodeP9alst_node
	.p2align	4, 0x90
	.type	_ZN8app_node18resolve_name_clashEP8arg_nodeP9alst_node,@function
_ZN8app_node18resolve_name_clashEP8arg_nodeP9alst_node: # @_ZN8app_node18resolve_name_clashEP8arg_nodeP9alst_node
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi343:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi344:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi345:
	.cfi_def_cfa_offset 32
.Lcfi346:
	.cfi_offset %rbx, -32
.Lcfi347:
	.cfi_offset %r14, -24
.Lcfi348:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB57_2
# BB#1:
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	*152(%rax)
.LBB57_2:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB57_3
# BB#4:
	movq	(%rdi), %rax
	movq	152(%rax), %rax
	movq	%r15, %rsi
	movq	%r14, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.LBB57_3:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end57:
	.size	_ZN8app_node18resolve_name_clashEP8arg_nodeP9alst_node, .Lfunc_end57-_ZN8app_node18resolve_name_clashEP8arg_nodeP9alst_node
	.cfi_endproc

	.globl	_ZN8app_node6reduceEPK9alst_nodeiPi
	.p2align	4, 0x90
	.type	_ZN8app_node6reduceEPK9alst_nodeiPi,@function
_ZN8app_node6reduceEPK9alst_nodeiPi:    # @_ZN8app_node6reduceEPK9alst_nodeiPi
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%rbp
.Lcfi349:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi350:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi351:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi352:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi353:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi354:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi355:
	.cfi_def_cfa_offset 96
.Lcfi356:
	.cfi_offset %rbx, -56
.Lcfi357:
	.cfi_offset %r12, -48
.Lcfi358:
	.cfi_offset %r13, -40
.Lcfi359:
	.cfi_offset %r14, -32
.Lcfi360:
	.cfi_offset %r15, -24
.Lcfi361:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	_ZL24app_reduce_recurse_level(%rip), %eax
	leal	1(%rax), %esi
	movl	%esi, _ZL24app_reduce_recurse_level(%rip)
	cmpl	$4000, %eax             # imm = 0xFA0
	jl	.LBB58_3
# BB#1:
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	decl	_ZL24app_reduce_recurse_level(%rip)
	testq	%r14, %r14
	je	.LBB58_88
# BB#2:
	orb	$1, (%r14)
	jmp	.LBB58_88
.LBB58_3:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB58_38
# BB#4:
	movl	%r12d, %r13d
	andl	$-5, %r13d
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	callq	*96(%rax)
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movl	%r13d, %edx
	movq	%r14, %rcx
	callq	*104(%rax)
	movq	%rax, %rbp
	movq	16(%rbx), %rdi
	cmpq	%rbp, %rdi
	je	.LBB58_9
# BB#5:
	testq	%rdi, %rdi
	je	.LBB58_7
# BB#6:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB58_7:
	movq	%rbp, 16(%rbx)
	testq	%rbp, %rbp
	je	.LBB58_9
# BB#8:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.LBB58_9:                               # %_ZN8app_node8set_leftEP8exp_nodes.exit
	testq	%r14, %r14
	je	.LBB58_13
# BB#10:
	movl	(%r14), %eax
	testb	$1, %al
	jne	.LBB58_87
# BB#11:
	testb	$1, %r12b
	je	.LBB58_13
# BB#12:
	andl	$2, %eax
	jne	.LBB58_87
.LBB58_13:                              # %.thread
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB58_38
# BB#14:
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpl	$5, %eax
	jne	.LBB58_31
# BB#15:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	callq	*96(%rax)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB58_21
# BB#16:
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movl	%r13d, %edx
	movq	%r14, %rcx
	callq	*104(%rax)
	movq	%rax, %rbp
	movq	16(%rbx), %rdi
	cmpq	%rbp, %rdi
	je	.LBB58_21
# BB#17:
	testq	%rdi, %rdi
	je	.LBB58_19
# BB#18:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB58_19:
	movq	%rbp, 16(%rbx)
	testq	%rbp, %rbp
	je	.LBB58_21
# BB#20:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.LBB58_21:                              # %_ZN8app_node8set_leftEP8exp_nodes.exit134
	testq	%r14, %r14
	je	.LBB58_25
# BB#22:
	movl	(%r14), %eax
	testb	$1, %al
	jne	.LBB58_87
# BB#23:
	testb	$1, %r12b
	je	.LBB58_25
# BB#24:
	andl	$2, %eax
	jne	.LBB58_87
.LBB58_25:                              # %.thread142
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB58_31
# BB#26:
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	callq	*96(%rax)
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movl	%r12d, %edx
	movq	%r14, %rcx
	callq	*136(%rax)
	movq	%rax, %rbp
	movq	24(%rbx), %rdi
	cmpq	%rbp, %rdi
	je	.LBB58_31
# BB#27:
	testq	%rdi, %rdi
	je	.LBB58_29
# BB#28:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB58_29:
	movq	%rbp, 24(%rbx)
	testq	%rbp, %rbp
	je	.LBB58_31
# BB#30:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.LBB58_31:                              # %_ZN8app_node9set_rightEP8exp_nodes.exit
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB58_38
# BB#32:
	movq	(%rdi), %rax
	callq	*24(%rax)
	cmpl	$4, %eax
	jne	.LBB58_38
# BB#33:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*48(%rax)
	testq	%rax, %rax
	movl	%r12d, %ebp
	je	.LBB58_54
# BB#34:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*48(%rax)
	testq	%rax, %rax
	movl	%r12d, %ebp
	je	.LBB58_54
# BB#35:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*48(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	testq	%rax, %rax
	movl	%r12d, %ebp
	je	.LBB58_54
# BB#36:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*48(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	cmpb	$64, (%rax)
	jne	.LBB58_53
# BB#37:
	movl	%r12d, %ebp
	orl	$2, %ebp
	jmp	.LBB58_54
.LBB58_38:                              # %_ZN8app_node9set_rightEP8exp_nodes.exit.thread
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB58_51
# BB#39:
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	callq	*96(%rax)
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movl	%r12d, %edx
	movq	%r14, %rcx
	callq	*136(%rax)
	movq	%rax, %rbp
	movq	24(%rbx), %rdi
	cmpq	%rbp, %rdi
	je	.LBB58_46
# BB#40:
	testq	%rdi, %rdi
	je	.LBB58_42
# BB#41:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB58_42:
	movq	%rbp, 24(%rbx)
	testq	%rbp, %rbp
	je	.LBB58_45
# BB#43:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
	movq	24(%rbx), %rdi
	jmp	.LBB58_46
.LBB58_45:
	movq	%rbp, %rdi
.LBB58_46:                              # %_ZN8app_node9set_rightEP8exp_nodes.exit137
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movl	%r12d, %edx
	movq	%r14, %rcx
	callq	*104(%rax)
	movq	%rax, %rbp
	movq	24(%rbx), %rdi
	cmpq	%rbp, %rdi
	je	.LBB58_51
# BB#47:
	testq	%rdi, %rdi
	je	.LBB58_49
# BB#48:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB58_49:
	movq	%rbp, 24(%rbx)
	testq	%rbp, %rbp
	je	.LBB58_51
# BB#50:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.LBB58_51:                              # %_ZN8app_node9set_rightEP8exp_nodes.exit138
	movq	%rbx, %rbp
.LBB58_52:                              # %_ZN8app_node9set_rightEP8exp_nodes.exit138
	decl	_ZL24app_reduce_recurse_level(%rip)
	jmp	.LBB58_89
.LBB58_53:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*48(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	movl	%r12d, %ebp
	andl	$-3, %ebp
	cmpb	$35, (%rax)
	cmovnel	%r12d, %ebp
.LBB58_54:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB58_76
# BB#55:
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	callq	*96(%rax)
	testb	$2, %bpl
	je	.LBB58_69
# BB#56:
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*48(%rax)
	movq	%rax, %rbp
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*56(%rax)
	movq	$0, 16(%rsp)
	movq	$_ZTV11stack_frame+16, 8(%rsp)
	movq	%rbp, 24(%rsp)
	movq	%r15, 32(%rsp)
	testq	%rbp, %rbp
	je	.LBB58_69
# BB#57:
	testq	%rax, %rax
	je	.LBB58_69
# BB#58:
	movq	(%rax), %rcx
.Ltmp220:
	leaq	8(%rsp), %rdx
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	*120(%rcx)
.Ltmp221:
# BB#59:
	testw	%ax, %ax
	je	.LBB58_69
# BB#60:
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movl	%r13d, %edx
	movq	%r14, %rcx
	callq	*104(%rax)
	movq	%rax, %rbp
	movq	24(%rbx), %rdi
	cmpq	%rbp, %rdi
	je	.LBB58_65
# BB#61:
	testq	%rdi, %rdi
	je	.LBB58_63
# BB#62:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB58_63:
	movq	%rbp, 24(%rbx)
	testq	%rbp, %rbp
	je	.LBB58_65
# BB#64:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.LBB58_65:                              # %_ZN8app_node9set_rightEP8exp_nodes.exit135
	testq	%r14, %r14
	je	.LBB58_76
# BB#66:
	movl	(%r14), %eax
	testb	$1, %al
	jne	.LBB58_87
# BB#67:
	testb	$1, %r12b
	je	.LBB58_76
# BB#68:
	andl	$2, %eax
	jne	.LBB58_87
	jmp	.LBB58_76
.LBB58_69:                              # %.thread145
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movl	%r12d, %edx
	movq	%r14, %rcx
	callq	*136(%rax)
	movq	%rax, %rbp
	movq	24(%rbx), %rdi
	cmpq	%rbp, %rdi
	je	.LBB58_74
# BB#70:
	testq	%rdi, %rdi
	je	.LBB58_72
# BB#71:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB58_72:
	movq	%rbp, 24(%rbx)
	testq	%rbp, %rbp
	je	.LBB58_74
# BB#73:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.LBB58_74:                              # %_ZN8app_node9set_rightEP8exp_nodes.exit136
	testq	%r14, %r14
	je	.LBB58_76
# BB#75:
	testb	$1, (%r14)
	jne	.LBB58_87
.LBB58_76:                              # %.thread154
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*48(%rax)
	movq	%rax, %rbp
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB58_78
# BB#77:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB58_78:
	movq	24(%rbx), %rax
	movq	%rax, 24(%rbp)
	movq	$0, 24(%rbx)
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB58_80
# BB#79:
	movq	(%rdi), %rax
	movq	%rbp, %rsi
	callq	*96(%rax)
.LBB58_80:                              # %_ZN8arg_node12import_valueEPP8exp_node.exit
	movq	$0, 24(%rbx)
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	callq	*96(%rax)
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movl	%r12d, %edx
	movq	%r14, %rcx
	callq	*104(%rax)
	movq	%rax, %rbp
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB58_83
# BB#81:                                # %_ZN8arg_node12import_valueEPP8exp_node.exit
	cmpq	%rbp, %rdi
	je	.LBB58_83
# BB#82:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB58_83:
	movq	$0, 16(%rbx)
	testq	%rbp, %rbp
	je	.LBB58_85
# BB#84:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.LBB58_85:
	testq	%r14, %r14
	je	.LBB58_52
# BB#86:
	orb	$2, (%r14)
	jmp	.LBB58_52
.LBB58_87:                              # %.critedge
	decl	_ZL24app_reduce_recurse_level(%rip)
.LBB58_88:                              # %.thread157
	movq	%rbx, %rbp
.LBB58_89:                              # %.thread157
	movq	%rbp, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB58_90:
.Ltmp222:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end58:
	.size	_ZN8app_node6reduceEPK9alst_nodeiPi, .Lfunc_end58-_ZN8app_node6reduceEPK9alst_nodeiPi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table58:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin12-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp220-.Lfunc_begin12 #   Call between .Lfunc_begin12 and .Ltmp220
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp220-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp221-.Ltmp220       #   Call between .Ltmp220 and .Ltmp221
	.long	.Ltmp222-.Lfunc_begin12 #     jumps to .Ltmp222
	.byte	0                       #   On action: cleanup
	.long	.Ltmp221-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Lfunc_end58-.Ltmp221   #   Call between .Ltmp221 and .Lfunc_end58
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8app_node11reduce_varsEPK9alst_nodeiPi
	.p2align	4, 0x90
	.type	_ZN8app_node11reduce_varsEPK9alst_nodeiPi,@function
_ZN8app_node11reduce_varsEPK9alst_nodeiPi: # @_ZN8app_node11reduce_varsEPK9alst_nodeiPi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi362:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi363:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi364:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi365:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi366:
	.cfi_def_cfa_offset 48
.Lcfi367:
	.cfi_offset %rbx, -48
.Lcfi368:
	.cfi_offset %r12, -40
.Lcfi369:
	.cfi_offset %r14, -32
.Lcfi370:
	.cfi_offset %r15, -24
.Lcfi371:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %r15d
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB59_10
# BB#1:
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	callq	*96(%rax)
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	movl	%r15d, %edx
	movq	%r14, %rcx
	callq	*136(%rax)
	movq	%rax, %rbp
	movq	16(%rbx), %rdi
	cmpq	%rbp, %rdi
	je	.LBB59_6
# BB#2:
	testq	%rdi, %rdi
	je	.LBB59_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB59_4:
	movq	%rbp, 16(%rbx)
	testq	%rbp, %rbp
	je	.LBB59_6
# BB#5:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.LBB59_6:                               # %_ZN8app_node8set_leftEP8exp_nodes.exit
	testq	%r14, %r14
	je	.LBB59_10
# BB#7:
	testb	$1, (%r14)
	jne	.LBB59_8
.LBB59_10:                              # %.thread
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB59_17
# BB#11:
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	callq	*96(%rax)
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	movl	%r15d, %edx
	movq	%r14, %rcx
	callq	*136(%rax)
	movq	%rax, %rbp
	movq	24(%rbx), %rdi
	cmpq	%rbp, %rdi
	je	.LBB59_17
# BB#12:
	testq	%rdi, %rdi
	je	.LBB59_14
# BB#13:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB59_14:
	movq	%rbp, 24(%rbx)
	testq	%rbp, %rbp
	je	.LBB59_17
# BB#15:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	jmp	.LBB59_16
.LBB59_8:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB59_17
# BB#9:
	movq	(%rdi), %rax
.LBB59_16:                              # %_ZN8app_node9set_rightEP8exp_nodes.exit
	movq	%rbx, %rsi
	callq	*96(%rax)
.LBB59_17:                              # %_ZN8app_node9set_rightEP8exp_nodes.exit
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end59:
	.size	_ZN8app_node11reduce_varsEPK9alst_nodeiPi, .Lfunc_end59-_ZN8app_node11reduce_varsEPK9alst_nodeiPi
	.cfi_endproc

	.globl	_ZNK8app_nodeeqERK8exp_node
	.p2align	4, 0x90
	.type	_ZNK8app_nodeeqERK8exp_node,@function
_ZNK8app_nodeeqERK8exp_node:            # @_ZNK8app_nodeeqERK8exp_node
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi372:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi373:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi374:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi375:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi376:
	.cfi_def_cfa_offset 48
.Lcfi377:
	.cfi_offset %rbx, -40
.Lcfi378:
	.cfi_offset %r14, -32
.Lcfi379:
	.cfi_offset %r15, -24
.Lcfi380:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*24(%rax)
	xorl	%ebp, %ebp
	cmpl	$5, %eax
	jne	.LBB60_9
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*64(%rax)
	testq	%rax, %rax
	je	.LBB60_9
# BB#2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	testq	%rax, %rax
	je	.LBB60_9
# BB#3:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*72(%rax)
	testq	%rax, %rax
	je	.LBB60_9
# BB#4:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*72(%rax)
	testq	%rax, %rax
	je	.LBB60_9
# BB#5:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*64(%rax)
	movq	%rax, %rbp
	movq	(%rbp), %rax
	movq	160(%rax), %r15
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*64(%rax)
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	*%r15
	testl	%eax, %eax
	je	.LBB60_6
# BB#7:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	%rax, %rbp
	movq	(%rbp), %rax
	movq	160(%rax), %r14
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*72(%rax)
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	*%r14
	testl	%eax, %eax
	setne	%al
	jmp	.LBB60_8
.LBB60_6:
	xorl	%eax, %eax
.LBB60_8:
	movzbl	%al, %ebp
.LBB60_9:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end60:
	.size	_ZNK8app_nodeeqERK8exp_node, .Lfunc_end60-_ZNK8app_nodeeqERK8exp_node
	.cfi_endproc

	.globl	_ZN8app_node12extract_defsEPK9alst_node
	.p2align	4, 0x90
	.type	_ZN8app_node12extract_defsEPK9alst_node,@function
_ZN8app_node12extract_defsEPK9alst_node: # @_ZN8app_node12extract_defsEPK9alst_node
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r15
.Lcfi381:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi382:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi383:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi384:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi385:
	.cfi_def_cfa_offset 48
.Lcfi386:
	.cfi_offset %rbx, -40
.Lcfi387:
	.cfi_offset %r12, -32
.Lcfi388:
	.cfi_offset %r14, -24
.Lcfi389:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	(%r12), %rax
	callq	*192(%rax)
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB61_1
# BB#20:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r12
	movq	(%rbx), %rax
.Ltmp223:
	movq	%rbx, %rdi
	callq	*48(%rax)
.Ltmp224:
# BB#21:
	movq	(%rax), %rcx
.Ltmp225:
	movq	%rax, %rdi
	callq	*32(%rcx)
	movq	%rax, %r14
.Ltmp226:
# BB#22:
	movq	$0, 8(%r12)
	movq	$_ZTV8var_node+16, (%r12)
	testq	%r14, %r14
	je	.LBB61_25
# BB#23:
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp227:
	callq	_Znam
.Ltmp228:
	jmp	.LBB61_24
.LBB61_1:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*64(%rax)
	testq	%rax, %rax
	je	.LBB61_7
# BB#2:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	*200(%rcx)
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB61_7
# BB#3:
	movq	16(%r12), %rdi
	cmpq	%r15, %rdi
	je	.LBB61_7
# BB#4:
	testq	%rdi, %rdi
	je	.LBB61_6
# BB#5:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB61_6:
	movq	%r15, 16(%r12)
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	*96(%rax)
.LBB61_7:                               # %_ZN8app_node8set_leftEP8exp_nodes.exit
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*72(%rax)
	testq	%rax, %rax
	je	.LBB61_13
# BB#8:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*72(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	*200(%rcx)
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB61_13
# BB#9:
	movq	24(%r12), %rdi
	cmpq	%r15, %rdi
	je	.LBB61_13
# BB#10:
	testq	%rdi, %rdi
	je	.LBB61_12
# BB#11:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB61_12:
	movq	%r15, 24(%r12)
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	*96(%rax)
.LBB61_13:                              # %_ZN8app_node9set_rightEP8exp_nodes.exit
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	*192(%rax)
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB61_25
# BB#14:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r12
	movq	(%rbx), %rax
.Ltmp230:
	movq	%rbx, %rdi
	callq	*48(%rax)
.Ltmp231:
# BB#15:
	movq	(%rax), %rcx
.Ltmp232:
	movq	%rax, %rdi
	callq	*32(%rcx)
	movq	%rax, %r14
.Ltmp233:
# BB#16:
	movq	$0, 8(%r12)
	movq	$_ZTV8var_node+16, (%r12)
	testq	%r14, %r14
	je	.LBB61_25
# BB#17:
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp234:
	callq	_Znam
.Ltmp235:
.LBB61_24:                              # %.noexc26
	movq	%rax, 16(%r12)
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	strcpy
.LBB61_25:
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB61_18:
.Ltmp236:
	jmp	.LBB61_19
.LBB61_26:
.Ltmp229:
.LBB61_19:
	movq	%rax, %r14
	movq	%r12, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end61:
	.size	_ZN8app_node12extract_defsEPK9alst_node, .Lfunc_end61-_ZN8app_node12extract_defsEPK9alst_node
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table61:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin13-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp223-.Lfunc_begin13 #   Call between .Lfunc_begin13 and .Ltmp223
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp223-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp228-.Ltmp223       #   Call between .Ltmp223 and .Ltmp228
	.long	.Ltmp229-.Lfunc_begin13 #     jumps to .Ltmp229
	.byte	0                       #   On action: cleanup
	.long	.Ltmp228-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp230-.Ltmp228       #   Call between .Ltmp228 and .Ltmp230
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp230-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Ltmp235-.Ltmp230       #   Call between .Ltmp230 and .Ltmp235
	.long	.Ltmp236-.Lfunc_begin13 #     jumps to .Ltmp236
	.byte	0                       #   On action: cleanup
	.long	.Ltmp235-.Lfunc_begin13 # >> Call Site 5 <<
	.long	.Lfunc_end61-.Ltmp235   #   Call between .Ltmp235 and .Lfunc_end61
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZNK8app_node7extractEPKci
	.p2align	4, 0x90
	.type	_ZNK8app_node7extractEPKci,@function
_ZNK8app_node7extractEPKci:             # @_ZNK8app_node7extractEPKci
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%rbp
.Lcfi390:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi391:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi392:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi393:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi394:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi395:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi396:
	.cfi_def_cfa_offset 96
.Lcfi397:
	.cfi_offset %rbx, -56
.Lcfi398:
	.cfi_offset %r12, -48
.Lcfi399:
	.cfi_offset %r13, -40
.Lcfi400:
	.cfi_offset %r14, -32
.Lcfi401:
	.cfi_offset %r15, -24
.Lcfi402:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	(%r14), %rax
	callq	*64(%rax)
	testq	%rax, %rax
	je	.LBB62_20
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*72(%rax)
	xorl	%r12d, %r12d
	testq	%r15, %r15
	je	.LBB62_21
# BB#2:
	testq	%rax, %rax
	movl	$0, %r13d
	je	.LBB62_36
# BB#3:                                 # %.noexc.i
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV8arg_node+16, (%rbx)
	movq	%r15, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp237:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp238:
# BB#4:
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movq	%rbp, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 16(%rsp)
	movq	$_ZTV11arglst_node+16, 8(%rsp)
	movq	%rbx, 24(%rsp)
	movq	$0, 32(%rsp)
	movq	(%r14), %rax
.Ltmp240:
	leaq	8(%rsp), %rdx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*120(%rax)
.Ltmp241:
# BB#5:
	testw	%ax, %ax
	je	.LBB62_22
# BB#6:
	movq	(%r14), %rax
.Ltmp242:
	movq	%r14, %rdi
	callq	*64(%rax)
.Ltmp243:
# BB#7:
	movq	(%rax), %rcx
.Ltmp244:
	leaq	8(%rsp), %rdx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	*120(%rcx)
.Ltmp245:
# BB#8:
	testw	%ax, %ax
	jne	.LBB62_19
# BB#9:
	movq	(%r14), %rax
.Ltmp246:
	movq	%r14, %rdi
	callq	*72(%rax)
.Ltmp247:
# BB#10:
	movq	(%rax), %rcx
.Ltmp248:
	movq	%rax, %rdi
	callq	*24(%rcx)
.Ltmp249:
# BB#11:
	cmpl	$3, %eax
	jne	.LBB62_19
# BB#12:
	movq	(%r14), %rax
.Ltmp250:
	movq	%r14, %rdi
	callq	*72(%rax)
.Ltmp251:
# BB#13:
	movq	(%rax), %rcx
.Ltmp252:
	movq	%rax, %rdi
	callq	*32(%rcx)
.Ltmp253:
# BB#14:
	xorl	%r13d, %r13d
	movl	4(%rsp), %ebp           # 4-byte Reload
	testb	$64, %bpl
	je	.LBB62_85
# BB#15:
	testq	%rax, %rax
	movl	$0, %r12d
	movl	$0, %ebx
	je	.LBB62_31
# BB#16:
	movq	(%r14), %rax
.Ltmp254:
	movq	%r14, %rdi
	callq	*72(%rax)
.Ltmp255:
# BB#17:
	movq	(%rax), %rcx
.Ltmp256:
	movq	%rax, %rdi
	callq	*32(%rcx)
.Ltmp257:
# BB#18:
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB62_86
.LBB62_19:
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	jmp	.LBB62_30
.LBB62_20:
	xorl	%r12d, %r12d
.LBB62_21:
	xorl	%r13d, %r13d
.LBB62_36:                              # %.thread
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*64(%rax)
	testq	%rax, %rax
	je	.LBB62_38
# BB#37:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%r15, %rsi
	movl	%ebp, %edx
	callq	*208(%rcx)
	movq	%rax, %r13
.LBB62_38:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*72(%rax)
	testq	%rax, %rax
	je	.LBB62_40
# BB#39:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*72(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	movq	%r15, %rsi
	movl	%ebp, %edx
	callq	*208(%rcx)
	movq	%rax, %r12
.LBB62_40:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbp
	testq	%r15, %r15
	je	.LBB62_46
# BB#41:
.Ltmp311:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp312:
# BB#42:
	movq	$0, 8(%rbx)
	movq	$_ZTV8var_node+16, (%rbx)
.Ltmp313:
	movl	$2, %edi
	callq	_Znam
.Ltmp314:
# BB#43:                                # %.noexc90
	movq	%rax, 16(%rbx)
	movw	$83, (%rax)
	movq	$0, 8(%rbp)
	movq	$_ZTV8app_node+16, (%rbp)
	movq	$0, 24(%rbp)
	movq	%rbx, 16(%rbp)
	movq	%rbp, 8(%rbx)
	testq	%r13, %r13
	je	.LBB62_49
# BB#44:
	movq	%r13, 24(%rbp)
	movq	(%r13), %rax
.Ltmp316:
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	*96(%rax)
.Ltmp317:
# BB#45:                                # %._ZN8app_nodeC2EP8exp_nodeS1_s.exit93_crit_edge
	movq	(%rbp), %r14
	jmp	.LBB62_50
.LBB62_46:
	movq	$0, 8(%rbp)
	movq	$_ZTV8app_node+16, (%rbp)
	testq	%r13, %r13
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbp)
	je	.LBB62_56
# BB#47:                                # %.noexc10.i
	movq	%r13, 16(%rbp)
	movq	(%r13), %rax
.Ltmp326:
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	*96(%rax)
.Ltmp327:
# BB#48:                                # %.noexc102
	movq	24(%rbp), %rdi
	cmpq	%r12, %rdi
	jne	.LBB62_57
	jmp	.LBB62_61
.LBB62_49:
	movl	$_ZTV8app_node+16, %r14d
.LBB62_50:                              # %_ZN8app_nodeC2EP8exp_nodeS1_s.exit93
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV8app_node+16, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbp, 16(%rbx)
.Ltmp319:
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*96(%r14)
.Ltmp320:
# BB#51:                                # %.noexc96
	movq	24(%rbx), %rdi
	cmpq	%r12, %rdi
	je	.LBB62_62
# BB#52:
	testq	%rdi, %rdi
	je	.LBB62_54
# BB#53:
	movq	(%rdi), %rax
.Ltmp321:
	callq	*16(%rax)
.Ltmp322:
.LBB62_54:                              # %.noexc.i95
	movq	%r12, 24(%rbx)
	testq	%r12, %r12
	je	.LBB62_62
# BB#55:
	movq	(%r12), %rax
.Ltmp323:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.Ltmp324:
	jmp	.LBB62_62
.LBB62_22:
	movl	4(%rsp), %edx           # 4-byte Reload
	testb	%dl, %dl
	js	.LBB62_63
# BB#23:
	movq	(%r14), %rax
.Ltmp262:
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	*208(%rax)
	movq	%rax, %rbp
.Ltmp263:
# BB#24:
.Ltmp264:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp265:
# BB#25:
.Ltmp266:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r12
.Ltmp267:
# BB#26:
	movq	$0, 8(%r12)
	movq	$_ZTV8var_node+16, (%r12)
.Ltmp268:
	movl	$2, %edi
	callq	_Znam
.Ltmp269:
# BB#27:                                # %.noexc79
	movq	%rax, 16(%r12)
	movw	$75, (%rax)
	movq	$0, 8(%rbx)
	movq	$_ZTV8app_node+16, (%rbx)
	movq	$0, 24(%rbx)
	movq	%r12, 16(%rbx)
	movq	%rbx, 8(%r12)
	testq	%rbp, %rbp
	je	.LBB62_29
# BB#28:
	movq	%rbp, 24(%rbx)
	movq	(%rbp), %rax
.Ltmp271:
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.Ltmp272:
.LBB62_29:
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
.LBB62_30:
	movl	4(%rsp), %ebp           # 4-byte Reload
.LBB62_31:
	movq	$_ZTV11arglst_node+16, 8(%rsp)
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB62_33
# BB#32:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB62_33:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB62_35
# BB#34:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB62_35:
	testq	%rbx, %rbx
	jne	.LBB62_62
	jmp	.LBB62_36
.LBB62_56:
	xorl	%edi, %edi
	cmpq	%r12, %rdi
	je	.LBB62_61
.LBB62_57:
	testq	%rdi, %rdi
	je	.LBB62_59
# BB#58:
	movq	(%rdi), %rax
.Ltmp328:
	callq	*16(%rax)
.Ltmp329:
.LBB62_59:                              # %.noexc.i101
	movq	%r12, 24(%rbp)
	testq	%r12, %r12
	je	.LBB62_61
# BB#60:
	movq	(%r12), %rax
.Ltmp330:
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	*96(%rax)
.Ltmp331:
.LBB62_61:                              # %_ZN8app_nodeC2EP8exp_nodeS1_s.exit105
	movq	%rbp, %rbx
.LBB62_62:
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB62_63:
	movq	(%r14), %rax
.Ltmp274:
	movq	%r14, %rdi
	callq	*64(%rax)
.Ltmp275:
# BB#64:
	testq	%rax, %rax
	je	.LBB62_67
# BB#65:
	movq	(%r14), %rax
.Ltmp276:
	movq	%r14, %rdi
	callq	*64(%rax)
.Ltmp277:
# BB#66:
	movq	(%rax), %rcx
.Ltmp278:
	movq	%rax, %rdi
	movq	%r15, %rsi
	movl	4(%rsp), %edx           # 4-byte Reload
	callq	*208(%rcx)
	movq	%rax, %r13
.Ltmp279:
	jmp	.LBB62_68
.LBB62_67:
	xorl	%r13d, %r13d
.LBB62_68:
	movq	(%r14), %rax
.Ltmp280:
	movq	%r14, %rdi
	callq	*72(%rax)
.Ltmp281:
# BB#69:
	testq	%rax, %rax
	je	.LBB62_72
# BB#70:
	movq	(%r14), %rax
.Ltmp282:
	movq	%r14, %rdi
	callq	*72(%rax)
.Ltmp283:
# BB#71:
	movq	(%rax), %rcx
.Ltmp284:
	movq	%rax, %rdi
	movq	%r15, %rsi
	movl	4(%rsp), %edx           # 4-byte Reload
	callq	*208(%rcx)
	movq	%rax, %r12
.Ltmp285:
	jmp	.LBB62_73
.LBB62_72:
	xorl	%r12d, %r12d
.LBB62_73:
.Ltmp286:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp287:
# BB#74:
.Ltmp288:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp289:
# BB#75:
	movq	$0, 8(%rbx)
	movq	$_ZTV8var_node+16, (%rbx)
.Ltmp290:
	movl	$2, %edi
	callq	_Znam
.Ltmp291:
# BB#76:                                # %.noexc66
	movq	%rax, 16(%rbx)
	movw	$83, (%rax)
	movq	$0, 8(%rbp)
	movq	$_ZTV8app_node+16, (%rbp)
	movq	$0, 24(%rbp)
	movq	%rbx, 16(%rbp)
	movq	%rbp, 8(%rbx)
	testq	%r13, %r13
	je	.LBB62_78
# BB#77:
	movq	%r13, 24(%rbp)
	movq	(%r13), %rax
.Ltmp293:
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	*96(%rax)
.Ltmp294:
.LBB62_78:                              # %_ZN8app_nodeC2EP8exp_nodeS1_s.exit
.Ltmp296:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp297:
# BB#79:
	movq	$0, 8(%rbx)
	movq	$_ZTV8app_node+16, (%rbx)
	movq	$0, 24(%rbx)
	movq	%rbp, 16(%rbx)
	movq	(%rbp), %rax
.Ltmp299:
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.Ltmp300:
# BB#80:                                # %.noexc71
	movq	24(%rbx), %rdi
	cmpq	%r12, %rdi
	je	.LBB62_30
# BB#81:
	testq	%rdi, %rdi
	je	.LBB62_83
# BB#82:
	movq	(%rdi), %rax
.Ltmp301:
	callq	*16(%rax)
.Ltmp302:
.LBB62_83:                              # %.noexc.i70
	movq	%r12, 24(%rbx)
	testq	%r12, %r12
	je	.LBB62_30
# BB#84:
	movq	(%r12), %rax
.Ltmp303:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.Ltmp304:
	jmp	.LBB62_30
.LBB62_85:
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	jmp	.LBB62_31
.LBB62_86:
	movq	(%r14), %rax
.Ltmp258:
	movq	%r14, %rdi
	callq	*64(%rax)
.Ltmp259:
# BB#87:
	movq	(%rax), %rcx
.Ltmp260:
	movq	%rax, %rdi
	callq	*(%rcx)
	movq	%rax, %rbx
.Ltmp261:
	jmp	.LBB62_29
.LBB62_88:
.Ltmp292:
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB62_90
.LBB62_89:
.Ltmp295:
	movq	%rax, %r15
.LBB62_90:
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB62_106
.LBB62_91:
.Ltmp270:
	movq	%rax, %r15
	movq	%r12, %rdi
	callq	_ZdlPv
	jmp	.LBB62_95
.LBB62_92:
.Ltmp305:
	jmp	.LBB62_94
.LBB62_93:
.Ltmp273:
.LBB62_94:
	movq	%rax, %r15
.LBB62_95:
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB62_106
.LBB62_96:
.Ltmp239:
	jmp	.LBB62_103
.LBB62_97:
.Ltmp332:
	jmp	.LBB62_100
.LBB62_98:
.Ltmp315:
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB62_101
.LBB62_99:
.Ltmp318:
.LBB62_100:
	movq	%rax, %r15
.LBB62_101:
	movq	%rbp, %rdi
	jmp	.LBB62_104
.LBB62_102:
.Ltmp325:
.LBB62_103:
	movq	%rax, %r15
	movq	%rbx, %rdi
.LBB62_104:
	callq	_ZdlPv
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB62_105:
.Ltmp298:
	movq	%rax, %r15
.LBB62_106:
	movq	$_ZTV11arglst_node+16, 8(%rsp)
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB62_108
# BB#107:
	movq	(%rdi), %rax
.Ltmp306:
	callq	*16(%rax)
.Ltmp307:
.LBB62_108:                             # %.noexc83
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB62_110
# BB#109:
	movq	(%rdi), %rax
.Ltmp308:
	callq	*16(%rax)
.Ltmp309:
.LBB62_110:
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB62_111:
.Ltmp310:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end62:
	.size	_ZNK8app_node7extractEPKci, .Lfunc_end62-_ZNK8app_node7extractEPKci
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table62:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\333\202"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\322\002"              # Call site table length
	.long	.Lfunc_begin14-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp237-.Lfunc_begin14 #   Call between .Lfunc_begin14 and .Ltmp237
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp237-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp238-.Ltmp237       #   Call between .Ltmp237 and .Ltmp238
	.long	.Ltmp239-.Lfunc_begin14 #     jumps to .Ltmp239
	.byte	0                       #   On action: cleanup
	.long	.Ltmp240-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp257-.Ltmp240       #   Call between .Ltmp240 and .Ltmp257
	.long	.Ltmp298-.Lfunc_begin14 #     jumps to .Ltmp298
	.byte	0                       #   On action: cleanup
	.long	.Ltmp257-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Ltmp311-.Ltmp257       #   Call between .Ltmp257 and .Ltmp311
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp311-.Lfunc_begin14 # >> Call Site 5 <<
	.long	.Ltmp312-.Ltmp311       #   Call between .Ltmp311 and .Ltmp312
	.long	.Ltmp318-.Lfunc_begin14 #     jumps to .Ltmp318
	.byte	0                       #   On action: cleanup
	.long	.Ltmp313-.Lfunc_begin14 # >> Call Site 6 <<
	.long	.Ltmp314-.Ltmp313       #   Call between .Ltmp313 and .Ltmp314
	.long	.Ltmp315-.Lfunc_begin14 #     jumps to .Ltmp315
	.byte	0                       #   On action: cleanup
	.long	.Ltmp316-.Lfunc_begin14 # >> Call Site 7 <<
	.long	.Ltmp317-.Ltmp316       #   Call between .Ltmp316 and .Ltmp317
	.long	.Ltmp318-.Lfunc_begin14 #     jumps to .Ltmp318
	.byte	0                       #   On action: cleanup
	.long	.Ltmp326-.Lfunc_begin14 # >> Call Site 8 <<
	.long	.Ltmp327-.Ltmp326       #   Call between .Ltmp326 and .Ltmp327
	.long	.Ltmp332-.Lfunc_begin14 #     jumps to .Ltmp332
	.byte	0                       #   On action: cleanup
	.long	.Ltmp327-.Lfunc_begin14 # >> Call Site 9 <<
	.long	.Ltmp319-.Ltmp327       #   Call between .Ltmp327 and .Ltmp319
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp319-.Lfunc_begin14 # >> Call Site 10 <<
	.long	.Ltmp324-.Ltmp319       #   Call between .Ltmp319 and .Ltmp324
	.long	.Ltmp325-.Lfunc_begin14 #     jumps to .Ltmp325
	.byte	0                       #   On action: cleanup
	.long	.Ltmp262-.Lfunc_begin14 # >> Call Site 11 <<
	.long	.Ltmp265-.Ltmp262       #   Call between .Ltmp262 and .Ltmp265
	.long	.Ltmp298-.Lfunc_begin14 #     jumps to .Ltmp298
	.byte	0                       #   On action: cleanup
	.long	.Ltmp266-.Lfunc_begin14 # >> Call Site 12 <<
	.long	.Ltmp267-.Ltmp266       #   Call between .Ltmp266 and .Ltmp267
	.long	.Ltmp273-.Lfunc_begin14 #     jumps to .Ltmp273
	.byte	0                       #   On action: cleanup
	.long	.Ltmp268-.Lfunc_begin14 # >> Call Site 13 <<
	.long	.Ltmp269-.Ltmp268       #   Call between .Ltmp268 and .Ltmp269
	.long	.Ltmp270-.Lfunc_begin14 #     jumps to .Ltmp270
	.byte	0                       #   On action: cleanup
	.long	.Ltmp271-.Lfunc_begin14 # >> Call Site 14 <<
	.long	.Ltmp272-.Ltmp271       #   Call between .Ltmp271 and .Ltmp272
	.long	.Ltmp273-.Lfunc_begin14 #     jumps to .Ltmp273
	.byte	0                       #   On action: cleanup
	.long	.Ltmp272-.Lfunc_begin14 # >> Call Site 15 <<
	.long	.Ltmp328-.Ltmp272       #   Call between .Ltmp272 and .Ltmp328
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp328-.Lfunc_begin14 # >> Call Site 16 <<
	.long	.Ltmp331-.Ltmp328       #   Call between .Ltmp328 and .Ltmp331
	.long	.Ltmp332-.Lfunc_begin14 #     jumps to .Ltmp332
	.byte	0                       #   On action: cleanup
	.long	.Ltmp274-.Lfunc_begin14 # >> Call Site 17 <<
	.long	.Ltmp287-.Ltmp274       #   Call between .Ltmp274 and .Ltmp287
	.long	.Ltmp298-.Lfunc_begin14 #     jumps to .Ltmp298
	.byte	0                       #   On action: cleanup
	.long	.Ltmp288-.Lfunc_begin14 # >> Call Site 18 <<
	.long	.Ltmp289-.Ltmp288       #   Call between .Ltmp288 and .Ltmp289
	.long	.Ltmp295-.Lfunc_begin14 #     jumps to .Ltmp295
	.byte	0                       #   On action: cleanup
	.long	.Ltmp290-.Lfunc_begin14 # >> Call Site 19 <<
	.long	.Ltmp291-.Ltmp290       #   Call between .Ltmp290 and .Ltmp291
	.long	.Ltmp292-.Lfunc_begin14 #     jumps to .Ltmp292
	.byte	0                       #   On action: cleanup
	.long	.Ltmp293-.Lfunc_begin14 # >> Call Site 20 <<
	.long	.Ltmp294-.Ltmp293       #   Call between .Ltmp293 and .Ltmp294
	.long	.Ltmp295-.Lfunc_begin14 #     jumps to .Ltmp295
	.byte	0                       #   On action: cleanup
	.long	.Ltmp296-.Lfunc_begin14 # >> Call Site 21 <<
	.long	.Ltmp297-.Ltmp296       #   Call between .Ltmp296 and .Ltmp297
	.long	.Ltmp298-.Lfunc_begin14 #     jumps to .Ltmp298
	.byte	0                       #   On action: cleanup
	.long	.Ltmp299-.Lfunc_begin14 # >> Call Site 22 <<
	.long	.Ltmp304-.Ltmp299       #   Call between .Ltmp299 and .Ltmp304
	.long	.Ltmp305-.Lfunc_begin14 #     jumps to .Ltmp305
	.byte	0                       #   On action: cleanup
	.long	.Ltmp258-.Lfunc_begin14 # >> Call Site 23 <<
	.long	.Ltmp261-.Ltmp258       #   Call between .Ltmp258 and .Ltmp261
	.long	.Ltmp298-.Lfunc_begin14 #     jumps to .Ltmp298
	.byte	0                       #   On action: cleanup
	.long	.Ltmp261-.Lfunc_begin14 # >> Call Site 24 <<
	.long	.Ltmp306-.Ltmp261       #   Call between .Ltmp261 and .Ltmp306
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp306-.Lfunc_begin14 # >> Call Site 25 <<
	.long	.Ltmp309-.Ltmp306       #   Call between .Ltmp306 and .Ltmp309
	.long	.Ltmp310-.Lfunc_begin14 #     jumps to .Ltmp310
	.byte	1                       #   On action: 1
	.long	.Ltmp309-.Lfunc_begin14 # >> Call Site 26 <<
	.long	.Lfunc_end62-.Ltmp309   #   Call between .Ltmp309 and .Lfunc_end62
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN11arglst_nodeC2EP8arg_nodePS_s
	.p2align	4, 0x90
	.type	_ZN11arglst_nodeC2EP8arg_nodePS_s,@function
_ZN11arglst_nodeC2EP8arg_nodePS_s:      # @_ZN11arglst_nodeC2EP8arg_nodePS_s
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi403:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi404:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi405:
	.cfi_def_cfa_offset 32
.Lcfi406:
	.cfi_offset %rbx, -24
.Lcfi407:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV11arglst_node+16, (%rbx)
	testw	%cx, %cx
	je	.LBB63_1
# BB#7:                                 # %.critedge
	movq	%rsi, 16(%rbx)
	jmp	.LBB63_8
.LBB63_1:
	testq	%rsi, %rsi
	je	.LBB63_2
# BB#3:
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	callq	*(%rax)
	jmp	.LBB63_4
.LBB63_2:
	xorl	%eax, %eax
.LBB63_4:
	movq	%rax, 16(%rbx)
	testq	%r14, %r14
	je	.LBB63_5
# BB#6:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	movq	%rax, %r14
	jmp	.LBB63_8
.LBB63_5:
	xorl	%r14d, %r14d
.LBB63_8:
	movq	%r14, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end63:
	.size	_ZN11arglst_nodeC2EP8arg_nodePS_s, .Lfunc_end63-_ZN11arglst_nodeC2EP8arg_nodePS_s
	.cfi_endproc

	.globl	_ZN11arglst_nodeC2ERKS_
	.p2align	4, 0x90
	.type	_ZN11arglst_nodeC2ERKS_,@function
_ZN11arglst_nodeC2ERKS_:                # @_ZN11arglst_nodeC2ERKS_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi408:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi409:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi410:
	.cfi_def_cfa_offset 32
.Lcfi411:
	.cfi_offset %rbx, -24
.Lcfi412:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV11arglst_node+16, (%rbx)
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB64_1
# BB#2:
	movq	(%rdi), %rax
	callq	*(%rax)
	jmp	.LBB64_3
.LBB64_1:
	xorl	%eax, %eax
.LBB64_3:
	movq	%rax, 16(%rbx)
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB64_5
# BB#4:
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	%rax, 24(%rbx)
	jmp	.LBB64_6
.LBB64_5:
	movq	$0, 16(%rbx)
.LBB64_6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end64:
	.size	_ZN11arglst_nodeC2ERKS_, .Lfunc_end64-_ZN11arglst_nodeC2ERKS_
	.cfi_endproc

	.globl	_ZN11arglst_nodeD2Ev
	.p2align	4, 0x90
	.type	_ZN11arglst_nodeD2Ev,@function
_ZN11arglst_nodeD2Ev:                   # @_ZN11arglst_nodeD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi413:
	.cfi_def_cfa_offset 16
.Lcfi414:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV11arglst_node+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB65_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB65_2:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB65_3
# BB#4:
	movq	(%rdi), %rax
	popq	%rbx
	jmpq	*16(%rax)               # TAILCALL
.LBB65_3:
	popq	%rbx
	retq
.Lfunc_end65:
	.size	_ZN11arglst_nodeD2Ev, .Lfunc_end65-_ZN11arglst_nodeD2Ev
	.cfi_endproc

	.globl	_ZN11arglst_nodeD0Ev
	.p2align	4, 0x90
	.type	_ZN11arglst_nodeD0Ev,@function
_ZN11arglst_nodeD0Ev:                   # @_ZN11arglst_nodeD0Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi415:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi416:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi417:
	.cfi_def_cfa_offset 32
.Lcfi418:
	.cfi_offset %rbx, -24
.Lcfi419:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV11arglst_node+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB66_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp333:
	callq	*16(%rax)
.Ltmp334:
.LBB66_2:                               # %.noexc
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB66_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp335:
	callq	*16(%rax)
.Ltmp336:
.LBB66_4:                               # %_ZN11arglst_nodeD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB66_5:
.Ltmp337:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end66:
	.size	_ZN11arglst_nodeD0Ev, .Lfunc_end66-_ZN11arglst_nodeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table66:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp333-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp336-.Ltmp333       #   Call between .Ltmp333 and .Ltmp336
	.long	.Ltmp337-.Lfunc_begin15 #     jumps to .Ltmp337
	.byte	0                       #   On action: cleanup
	.long	.Ltmp336-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Lfunc_end66-.Ltmp336   #   Call between .Ltmp336 and .Lfunc_end66
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN11arglst_node3addEP8arg_nodes
	.p2align	4, 0x90
	.type	_ZN11arglst_node3addEP8arg_nodes,@function
_ZN11arglst_node3addEP8arg_nodes:       # @_ZN11arglst_node3addEP8arg_nodes
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%rbp
.Lcfi420:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi421:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi422:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi423:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi424:
	.cfi_def_cfa_offset 48
.Lcfi425:
	.cfi_offset %rbx, -48
.Lcfi426:
	.cfi_offset %r12, -40
.Lcfi427:
	.cfi_offset %r14, -32
.Lcfi428:
	.cfi_offset %r15, -24
.Lcfi429:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %r12
	movq	%rdi, %r14
	testq	%r12, %r12
	je	.LBB67_1
# BB#2:                                 # %.preheader
	testq	%r14, %r14
	je	.LBB67_15
# BB#3:                                 # %.lr.ph
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB67_4:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*48(%rax)
	testq	%rax, %rax
	je	.LBB67_14
# BB#5:                                 #   in Loop: Header=BB67_4 Depth=1
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*48(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	testq	%rax, %rax
	je	.LBB67_14
# BB#6:                                 #   in Loop: Header=BB67_4 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*32(%rax)
	movq	%rax, %rbx
	movq	16(%rbp), %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB67_7
.LBB67_14:                              #   in Loop: Header=BB67_4 Depth=1
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*80(%rax)
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB67_4
.LBB67_15:                              # %_ZN11arglst_node7set_argEP8arg_nodes.exit
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movq	$0, 8(%rbp)
	movq	$_ZTV11arglst_node+16, (%rbp)
	testw	%r15w, %r15w
	jne	.LBB67_17
# BB#16:
	movq	(%r12), %rax
.Ltmp338:
	movq	%r12, %rdi
	callq	*(%rax)
	movq	%rax, %r12
.Ltmp339:
.LBB67_17:                              # %.critedge.i
	movq	%r12, 16(%rbp)
	movq	%r14, 24(%rbp)
	movq	%rbp, %r14
	jmp	.LBB67_18
.LBB67_1:
	xorl	%r14d, %r14d
.LBB67_18:
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB67_7:
	movq	16(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.LBB67_18
# BB#8:
	testq	%rdi, %rdi
	je	.LBB67_10
# BB#9:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB67_10:
	testw	%r15w, %r15w
	je	.LBB67_12
# BB#11:                                # %.thread
	movq	%r12, 16(%rbp)
	jmp	.LBB67_13
.LBB67_12:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	movq	%rax, %r12
	movq	%r12, 16(%rbp)
	testq	%r12, %r12
	je	.LBB67_18
.LBB67_13:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	*96(%rax)
	jmp	.LBB67_18
.LBB67_19:
.Ltmp340:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end67:
	.size	_ZN11arglst_node3addEP8arg_nodes, .Lfunc_end67-_ZN11arglst_node3addEP8arg_nodes
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table67:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin16-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp338-.Lfunc_begin16 #   Call between .Lfunc_begin16 and .Ltmp338
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp338-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp339-.Ltmp338       #   Call between .Ltmp338 and .Ltmp339
	.long	.Ltmp340-.Lfunc_begin16 #     jumps to .Ltmp340
	.byte	0                       #   On action: cleanup
	.long	.Ltmp339-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Lfunc_end67-.Ltmp339   #   Call between .Ltmp339 and .Lfunc_end67
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN11arglst_node7set_argEP8arg_nodes
	.p2align	4, 0x90
	.type	_ZN11arglst_node7set_argEP8arg_nodes,@function
_ZN11arglst_node7set_argEP8arg_nodes:   # @_ZN11arglst_node7set_argEP8arg_nodes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi430:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi431:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi432:
	.cfi_def_cfa_offset 32
.Lcfi433:
	.cfi_offset %rbx, -32
.Lcfi434:
	.cfi_offset %r14, -24
.Lcfi435:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	16(%r14), %rdi
	cmpq	%rbx, %rdi
	je	.LBB68_8
# BB#1:
	testq	%rdi, %rdi
	je	.LBB68_3
# BB#2:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB68_3:
	testw	%bp, %bp
	jne	.LBB68_7
# BB#4:
	testq	%rbx, %rbx
	je	.LBB68_5
# BB#6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, %rbx
.LBB68_7:
	movq	%rbx, 16(%r14)
	testq	%rbx, %rbx
	je	.LBB68_8
# BB#9:
	movq	(%rbx), %rax
	movq	96(%rax), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.LBB68_5:                               # %.thread
	movq	$0, 16(%r14)
.LBB68_8:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end68:
	.size	_ZN11arglst_node7set_argEP8arg_nodes, .Lfunc_end68-_ZN11arglst_node7set_argEP8arg_nodes
	.cfi_endproc

	.globl	_ZN11arglst_node4findEP8arg_node
	.p2align	4, 0x90
	.type	_ZN11arglst_node4findEP8arg_node,@function
_ZN11arglst_node4findEP8arg_node:       # @_ZN11arglst_node4findEP8arg_node
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi436:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi437:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi438:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi439:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi440:
	.cfi_def_cfa_offset 48
.Lcfi441:
	.cfi_offset %rbx, -40
.Lcfi442:
	.cfi_offset %r12, -32
.Lcfi443:
	.cfi_offset %r14, -24
.Lcfi444:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	xorl	%r14d, %r14d
	testq	%r15, %r15
	je	.LBB69_8
# BB#1:
	testq	%rbx, %rbx
	je	.LBB69_8
# BB#2:                                 # %.lr.ph
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB69_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	testq	%rax, %rax
	je	.LBB69_6
# BB#4:                                 #   in Loop: Header=BB69_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*32(%rcx)
	testq	%rax, %rax
	je	.LBB69_6
# BB#5:                                 #   in Loop: Header=BB69_3 Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movq	%rax, %r12
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB69_7
.LBB69_6:                               #   in Loop: Header=BB69_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*80(%rax)
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB69_3
	jmp	.LBB69_8
.LBB69_7:
	movq	16(%rbx), %r14
.LBB69_8:                               # %.thread
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end69:
	.size	_ZN11arglst_node4findEP8arg_node, .Lfunc_end69-_ZN11arglst_node4findEP8arg_node
	.cfi_endproc

	.globl	_ZN11arglst_node4listEv
	.p2align	4, 0x90
	.type	_ZN11arglst_node4listEv,@function
_ZN11arglst_node4listEv:                # @_ZN11arglst_node4listEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi445:
	.cfi_def_cfa_offset 16
.Lcfi446:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB70_2
	jmp	.LBB70_5
	.p2align	4, 0x90
.LBB70_4:                               #   in Loop: Header=BB70_2 Depth=1
	movl	$10, %edi
	callq	putchar
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*80(%rax)
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB70_5
.LBB70_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	testq	%rax, %rax
	je	.LBB70_4
# BB#3:                                 #   in Loop: Header=BB70_2 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	movq	(%rax), %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	*112(%rcx)
	jmp	.LBB70_4
.LBB70_5:                               # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end70:
	.size	_ZN11arglst_node4listEv, .Lfunc_end70-_ZN11arglst_node4listEv
	.cfi_endproc

	.section	.text._ZN4nodeD0Ev,"axG",@progbits,_ZN4nodeD0Ev,comdat
	.weak	_ZN4nodeD0Ev
	.p2align	4, 0x90
	.type	_ZN4nodeD0Ev,@function
_ZN4nodeD0Ev:                           # @_ZN4nodeD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end71:
	.size	_ZN4nodeD0Ev, .Lfunc_end71-_ZN4nodeD0Ev
	.cfi_endproc

	.section	.text._ZNK4node2opEv,"axG",@progbits,_ZNK4node2opEv,comdat
	.weak	_ZNK4node2opEv
	.p2align	4, 0x90
	.type	_ZNK4node2opEv,@function
_ZNK4node2opEv:                         # @_ZNK4node2opEv
	.cfi_startproc
# BB#0:
	movl	$1, %eax
	retq
.Lfunc_end72:
	.size	_ZNK4node2opEv, .Lfunc_end72-_ZNK4node2opEv
	.cfi_endproc

	.section	.text._ZNK4node4nameEv,"axG",@progbits,_ZNK4node4nameEv,comdat
	.weak	_ZNK4node4nameEv
	.p2align	4, 0x90
	.type	_ZNK4node4nameEv,@function
_ZNK4node4nameEv:                       # @_ZNK4node4nameEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end73:
	.size	_ZNK4node4nameEv, .Lfunc_end73-_ZNK4node4nameEv
	.cfi_endproc

	.section	.text._ZNK4node5valueEv,"axG",@progbits,_ZNK4node5valueEv,comdat
	.weak	_ZNK4node5valueEv
	.p2align	4, 0x90
	.type	_ZNK4node5valueEv,@function
_ZNK4node5valueEv:                      # @_ZNK4node5valueEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end74:
	.size	_ZNK4node5valueEv, .Lfunc_end74-_ZNK4node5valueEv
	.cfi_endproc

	.section	.text._ZNK4node3argEv,"axG",@progbits,_ZNK4node3argEv,comdat
	.weak	_ZNK4node3argEv
	.p2align	4, 0x90
	.type	_ZNK4node3argEv,@function
_ZNK4node3argEv:                        # @_ZNK4node3argEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end75:
	.size	_ZNK4node3argEv, .Lfunc_end75-_ZNK4node3argEv
	.cfi_endproc

	.section	.text._ZNK4node4bodyEv,"axG",@progbits,_ZNK4node4bodyEv,comdat
	.weak	_ZNK4node4bodyEv
	.p2align	4, 0x90
	.type	_ZNK4node4bodyEv,@function
_ZNK4node4bodyEv:                       # @_ZNK4node4bodyEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end76:
	.size	_ZNK4node4bodyEv, .Lfunc_end76-_ZNK4node4bodyEv
	.cfi_endproc

	.section	.text._ZNK4node4leftEv,"axG",@progbits,_ZNK4node4leftEv,comdat
	.weak	_ZNK4node4leftEv
	.p2align	4, 0x90
	.type	_ZNK4node4leftEv,@function
_ZNK4node4leftEv:                       # @_ZNK4node4leftEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end77:
	.size	_ZNK4node4leftEv, .Lfunc_end77-_ZNK4node4leftEv
	.cfi_endproc

	.section	.text._ZNK4node5rightEv,"axG",@progbits,_ZNK4node5rightEv,comdat
	.weak	_ZNK4node5rightEv
	.p2align	4, 0x90
	.type	_ZNK4node5rightEv,@function
_ZNK4node5rightEv:                      # @_ZNK4node5rightEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end78:
	.size	_ZNK4node5rightEv, .Lfunc_end78-_ZNK4node5rightEv
	.cfi_endproc

	.section	.text._ZNK4node4nextEv,"axG",@progbits,_ZNK4node4nextEv,comdat
	.weak	_ZNK4node4nextEv
	.p2align	4, 0x90
	.type	_ZNK4node4nextEv,@function
_ZNK4node4nextEv:                       # @_ZNK4node4nextEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end79:
	.size	_ZNK4node4nextEv, .Lfunc_end79-_ZNK4node4nextEv
	.cfi_endproc

	.section	.text._ZNK4node6parentEv,"axG",@progbits,_ZNK4node6parentEv,comdat
	.weak	_ZNK4node6parentEv
	.p2align	4, 0x90
	.type	_ZNK4node6parentEv,@function
_ZNK4node6parentEv:                     # @_ZNK4node6parentEv
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	retq
.Lfunc_end80:
	.size	_ZNK4node6parentEv, .Lfunc_end80-_ZNK4node6parentEv
	.cfi_endproc

	.section	.text._ZN4node10set_parentEPS_,"axG",@progbits,_ZN4node10set_parentEPS_,comdat
	.weak	_ZN4node10set_parentEPS_
	.p2align	4, 0x90
	.type	_ZN4node10set_parentEPS_,@function
_ZN4node10set_parentEPS_:               # @_ZN4node10set_parentEPS_
	.cfi_startproc
# BB#0:
	movq	%rsi, 8(%rdi)
	retq
.Lfunc_end81:
	.size	_ZN4node10set_parentEPS_, .Lfunc_end81-_ZN4node10set_parentEPS_
	.cfi_endproc

	.section	.text._ZN4node6reduceEPK9alst_nodeiPi,"axG",@progbits,_ZN4node6reduceEPK9alst_nodeiPi,comdat
	.weak	_ZN4node6reduceEPK9alst_nodeiPi
	.p2align	4, 0x90
	.type	_ZN4node6reduceEPK9alst_nodeiPi,@function
_ZN4node6reduceEPK9alst_nodeiPi:        # @_ZN4node6reduceEPK9alst_nodeiPi
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end82:
	.size	_ZN4node6reduceEPK9alst_nodeiPi, .Lfunc_end82-_ZN4node6reduceEPK9alst_nodeiPi
	.cfi_endproc

	.section	.text._ZNK8arg_node2opEv,"axG",@progbits,_ZNK8arg_node2opEv,comdat
	.weak	_ZNK8arg_node2opEv
	.p2align	4, 0x90
	.type	_ZNK8arg_node2opEv,@function
_ZNK8arg_node2opEv:                     # @_ZNK8arg_node2opEv
	.cfi_startproc
# BB#0:
	movl	$2, %eax
	retq
.Lfunc_end83:
	.size	_ZNK8arg_node2opEv, .Lfunc_end83-_ZNK8arg_node2opEv
	.cfi_endproc

	.section	.text._ZNK8arg_node4nameEv,"axG",@progbits,_ZNK8arg_node4nameEv,comdat
	.weak	_ZNK8arg_node4nameEv
	.p2align	4, 0x90
	.type	_ZNK8arg_node4nameEv,@function
_ZNK8arg_node4nameEv:                   # @_ZNK8arg_node4nameEv
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	retq
.Lfunc_end84:
	.size	_ZNK8arg_node4nameEv, .Lfunc_end84-_ZNK8arg_node4nameEv
	.cfi_endproc

	.section	.text._ZNK8arg_node5valueEv,"axG",@progbits,_ZNK8arg_node5valueEv,comdat
	.weak	_ZNK8arg_node5valueEv
	.p2align	4, 0x90
	.type	_ZNK8arg_node5valueEv,@function
_ZNK8arg_node5valueEv:                  # @_ZNK8arg_node5valueEv
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	retq
.Lfunc_end85:
	.size	_ZNK8arg_node5valueEv, .Lfunc_end85-_ZNK8arg_node5valueEv
	.cfi_endproc

	.section	.text._ZN8exp_nodeD0Ev,"axG",@progbits,_ZN8exp_nodeD0Ev,comdat
	.weak	_ZN8exp_nodeD0Ev
	.p2align	4, 0x90
	.type	_ZN8exp_nodeD0Ev,@function
_ZN8exp_nodeD0Ev:                       # @_ZN8exp_nodeD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end86:
	.size	_ZN8exp_nodeD0Ev, .Lfunc_end86-_ZN8exp_nodeD0Ev
	.cfi_endproc

	.section	.text._ZNK8exp_node8has_freeEPK8arg_nodePK9alst_node,"axG",@progbits,_ZNK8exp_node8has_freeEPK8arg_nodePK9alst_node,comdat
	.weak	_ZNK8exp_node8has_freeEPK8arg_nodePK9alst_node
	.p2align	4, 0x90
	.type	_ZNK8exp_node8has_freeEPK8arg_nodePK9alst_node,@function
_ZNK8exp_node8has_freeEPK8arg_nodePK9alst_node: # @_ZNK8exp_node8has_freeEPK8arg_nodePK9alst_node
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end87:
	.size	_ZNK8exp_node8has_freeEPK8arg_nodePK9alst_node, .Lfunc_end87-_ZNK8exp_node8has_freeEPK8arg_nodePK9alst_node
	.cfi_endproc

	.section	.text._ZNK8exp_node4bindEPK9alst_node,"axG",@progbits,_ZNK8exp_node4bindEPK9alst_node,comdat
	.weak	_ZNK8exp_node4bindEPK9alst_node
	.p2align	4, 0x90
	.type	_ZNK8exp_node4bindEPK9alst_node,@function
_ZNK8exp_node4bindEPK9alst_node:        # @_ZNK8exp_node4bindEPK9alst_node
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end88:
	.size	_ZNK8exp_node4bindEPK9alst_node, .Lfunc_end88-_ZNK8exp_node4bindEPK9alst_node
	.cfi_endproc

	.section	.text._ZN8exp_node11reduce_varsEPK9alst_nodeiPi,"axG",@progbits,_ZN8exp_node11reduce_varsEPK9alst_nodeiPi,comdat
	.weak	_ZN8exp_node11reduce_varsEPK9alst_nodeiPi
	.p2align	4, 0x90
	.type	_ZN8exp_node11reduce_varsEPK9alst_nodeiPi,@function
_ZN8exp_node11reduce_varsEPK9alst_nodeiPi: # @_ZN8exp_node11reduce_varsEPK9alst_nodeiPi
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end89:
	.size	_ZN8exp_node11reduce_varsEPK9alst_nodeiPi, .Lfunc_end89-_ZN8exp_node11reduce_varsEPK9alst_nodeiPi
	.cfi_endproc

	.section	.text._ZN8exp_node6renameEP8arg_nodePKcP9alst_node,"axG",@progbits,_ZN8exp_node6renameEP8arg_nodePKcP9alst_node,comdat
	.weak	_ZN8exp_node6renameEP8arg_nodePKcP9alst_node
	.p2align	4, 0x90
	.type	_ZN8exp_node6renameEP8arg_nodePKcP9alst_node,@function
_ZN8exp_node6renameEP8arg_nodePKcP9alst_node: # @_ZN8exp_node6renameEP8arg_nodePKcP9alst_node
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end90:
	.size	_ZN8exp_node6renameEP8arg_nodePKcP9alst_node, .Lfunc_end90-_ZN8exp_node6renameEP8arg_nodePKcP9alst_node
	.cfi_endproc

	.section	.text._ZN8exp_node18resolve_name_clashEP8arg_nodeP9alst_node,"axG",@progbits,_ZN8exp_node18resolve_name_clashEP8arg_nodeP9alst_node,comdat
	.weak	_ZN8exp_node18resolve_name_clashEP8arg_nodeP9alst_node
	.p2align	4, 0x90
	.type	_ZN8exp_node18resolve_name_clashEP8arg_nodeP9alst_node,@function
_ZN8exp_node18resolve_name_clashEP8arg_nodeP9alst_node: # @_ZN8exp_node18resolve_name_clashEP8arg_nodeP9alst_node
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end91:
	.size	_ZN8exp_node18resolve_name_clashEP8arg_nodeP9alst_node, .Lfunc_end91-_ZN8exp_node18resolve_name_clashEP8arg_nodeP9alst_node
	.cfi_endproc

	.section	.text._ZNK8exp_nodeeqERKS_,"axG",@progbits,_ZNK8exp_nodeeqERKS_,comdat
	.weak	_ZNK8exp_nodeeqERKS_
	.p2align	4, 0x90
	.type	_ZNK8exp_nodeeqERKS_,@function
_ZNK8exp_nodeeqERKS_:                   # @_ZNK8exp_nodeeqERKS_
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end92:
	.size	_ZNK8exp_nodeeqERKS_, .Lfunc_end92-_ZNK8exp_nodeeqERKS_
	.cfi_endproc

	.section	.text._ZN8exp_node11export_bodyEv,"axG",@progbits,_ZN8exp_node11export_bodyEv,comdat
	.weak	_ZN8exp_node11export_bodyEv
	.p2align	4, 0x90
	.type	_ZN8exp_node11export_bodyEv,@function
_ZN8exp_node11export_bodyEv:            # @_ZN8exp_node11export_bodyEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end93:
	.size	_ZN8exp_node11export_bodyEv, .Lfunc_end93-_ZN8exp_node11export_bodyEv
	.cfi_endproc

	.section	.text._ZN8exp_node11export_leftEv,"axG",@progbits,_ZN8exp_node11export_leftEv,comdat
	.weak	_ZN8exp_node11export_leftEv
	.p2align	4, 0x90
	.type	_ZN8exp_node11export_leftEv,@function
_ZN8exp_node11export_leftEv:            # @_ZN8exp_node11export_leftEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end94:
	.size	_ZN8exp_node11export_leftEv, .Lfunc_end94-_ZN8exp_node11export_leftEv
	.cfi_endproc

	.section	.text._ZN8exp_node12export_rightEv,"axG",@progbits,_ZN8exp_node12export_rightEv,comdat
	.weak	_ZN8exp_node12export_rightEv
	.p2align	4, 0x90
	.type	_ZN8exp_node12export_rightEv,@function
_ZN8exp_node12export_rightEv:           # @_ZN8exp_node12export_rightEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end95:
	.size	_ZN8exp_node12export_rightEv, .Lfunc_end95-_ZN8exp_node12export_rightEv
	.cfi_endproc

	.section	.text._ZN8exp_node12extract_defsEPK9alst_node,"axG",@progbits,_ZN8exp_node12extract_defsEPK9alst_node,comdat
	.weak	_ZN8exp_node12extract_defsEPK9alst_node
	.p2align	4, 0x90
	.type	_ZN8exp_node12extract_defsEPK9alst_node,@function
_ZN8exp_node12extract_defsEPK9alst_node: # @_ZN8exp_node12extract_defsEPK9alst_node
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end96:
	.size	_ZN8exp_node12extract_defsEPK9alst_node, .Lfunc_end96-_ZN8exp_node12extract_defsEPK9alst_node
	.cfi_endproc

	.section	.text._ZNK8exp_node7extractEPKci,"axG",@progbits,_ZNK8exp_node7extractEPKci,comdat
	.weak	_ZNK8exp_node7extractEPKci
	.p2align	4, 0x90
	.type	_ZNK8exp_node7extractEPKci,@function
_ZNK8exp_node7extractEPKci:             # @_ZNK8exp_node7extractEPKci
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end97:
	.size	_ZNK8exp_node7extractEPKci, .Lfunc_end97-_ZNK8exp_node7extractEPKci
	.cfi_endproc

	.section	.text._ZNK8var_node5cloneEv,"axG",@progbits,_ZNK8var_node5cloneEv,comdat
	.weak	_ZNK8var_node5cloneEv
	.p2align	4, 0x90
	.type	_ZNK8var_node5cloneEv,@function
_ZNK8var_node5cloneEv:                  # @_ZNK8var_node5cloneEv
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi447:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi448:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi449:
	.cfi_def_cfa_offset 32
.Lcfi450:
	.cfi_offset %rbx, -24
.Lcfi451:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV8var_node+16, (%rbx)
	movq	16(%r14), %r14
	testq	%r14, %r14
	je	.LBB98_3
# BB#1:
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp341:
	callq	_Znam
.Ltmp342:
# BB#2:                                 # %.noexc
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	strcpy
	jmp	.LBB98_4
.LBB98_3:
	movq	$0, 16(%rbx)
.LBB98_4:
	movq	$0, 8(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB98_5:
.Ltmp343:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end98:
	.size	_ZNK8var_node5cloneEv, .Lfunc_end98-_ZNK8var_node5cloneEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table98:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin17-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp341-.Lfunc_begin17 #   Call between .Lfunc_begin17 and .Ltmp341
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp341-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Ltmp342-.Ltmp341       #   Call between .Ltmp341 and .Ltmp342
	.long	.Ltmp343-.Lfunc_begin17 #     jumps to .Ltmp343
	.byte	0                       #   On action: cleanup
	.long	.Ltmp342-.Lfunc_begin17 # >> Call Site 3 <<
	.long	.Lfunc_end98-.Ltmp342   #   Call between .Ltmp342 and .Lfunc_end98
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8var_nodeD2Ev,"axG",@progbits,_ZN8var_nodeD2Ev,comdat
	.weak	_ZN8var_nodeD2Ev
	.p2align	4, 0x90
	.type	_ZN8var_nodeD2Ev,@function
_ZN8var_nodeD2Ev:                       # @_ZN8var_nodeD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV8var_node+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB99_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB99_1:
	retq
.Lfunc_end99:
	.size	_ZN8var_nodeD2Ev, .Lfunc_end99-_ZN8var_nodeD2Ev
	.cfi_endproc

	.section	.text._ZN8var_nodeD0Ev,"axG",@progbits,_ZN8var_nodeD0Ev,comdat
	.weak	_ZN8var_nodeD0Ev
	.p2align	4, 0x90
	.type	_ZN8var_nodeD0Ev,@function
_ZN8var_nodeD0Ev:                       # @_ZN8var_nodeD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi452:
	.cfi_def_cfa_offset 16
.Lcfi453:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV8var_node+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB100_2
# BB#1:
	callq	_ZdaPv
.LBB100_2:                              # %_ZN8var_nodeD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end100:
	.size	_ZN8var_nodeD0Ev, .Lfunc_end100-_ZN8var_nodeD0Ev
	.cfi_endproc

	.section	.text._ZNK8var_node2opEv,"axG",@progbits,_ZNK8var_node2opEv,comdat
	.weak	_ZNK8var_node2opEv
	.p2align	4, 0x90
	.type	_ZNK8var_node2opEv,@function
_ZNK8var_node2opEv:                     # @_ZNK8var_node2opEv
	.cfi_startproc
# BB#0:
	movl	$3, %eax
	retq
.Lfunc_end101:
	.size	_ZNK8var_node2opEv, .Lfunc_end101-_ZNK8var_node2opEv
	.cfi_endproc

	.section	.text._ZNK8var_node4nameEv,"axG",@progbits,_ZNK8var_node4nameEv,comdat
	.weak	_ZNK8var_node4nameEv
	.p2align	4, 0x90
	.type	_ZNK8var_node4nameEv,@function
_ZNK8var_node4nameEv:                   # @_ZNK8var_node4nameEv
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	retq
.Lfunc_end102:
	.size	_ZNK8var_node4nameEv, .Lfunc_end102-_ZNK8var_node4nameEv
	.cfi_endproc

	.section	.text._ZN8var_node11reduce_varsEPK9alst_nodeiPi,"axG",@progbits,_ZN8var_node11reduce_varsEPK9alst_nodeiPi,comdat
	.weak	_ZN8var_node11reduce_varsEPK9alst_nodeiPi
	.p2align	4, 0x90
	.type	_ZN8var_node11reduce_varsEPK9alst_nodeiPi,@function
_ZN8var_node11reduce_varsEPK9alst_nodeiPi: # @_ZN8var_node11reduce_varsEPK9alst_nodeiPi
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	104(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end103:
	.size	_ZN8var_node11reduce_varsEPK9alst_nodeiPi, .Lfunc_end103-_ZN8var_node11reduce_varsEPK9alst_nodeiPi
	.cfi_endproc

	.section	.text._ZN8var_node18resolve_name_clashEP8arg_nodeP9alst_node,"axG",@progbits,_ZN8var_node18resolve_name_clashEP8arg_nodeP9alst_node,comdat
	.weak	_ZN8var_node18resolve_name_clashEP8arg_nodeP9alst_node
	.p2align	4, 0x90
	.type	_ZN8var_node18resolve_name_clashEP8arg_nodeP9alst_node,@function
_ZN8var_node18resolve_name_clashEP8arg_nodeP9alst_node: # @_ZN8var_node18resolve_name_clashEP8arg_nodeP9alst_node
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end104:
	.size	_ZN8var_node18resolve_name_clashEP8arg_nodeP9alst_node, .Lfunc_end104-_ZN8var_node18resolve_name_clashEP8arg_nodeP9alst_node
	.cfi_endproc

	.section	.text._ZNK8lam_node5cloneEv,"axG",@progbits,_ZNK8lam_node5cloneEv,comdat
	.weak	_ZNK8lam_node5cloneEv
	.p2align	4, 0x90
	.type	_ZNK8lam_node5cloneEv,@function
_ZNK8lam_node5cloneEv:                  # @_ZNK8lam_node5cloneEv
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%r14
.Lcfi454:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi455:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi456:
	.cfi_def_cfa_offset 32
.Lcfi457:
	.cfi_offset %rbx, -24
.Lcfi458:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV8lam_node+16, (%rbx)
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB105_1
# BB#2:
	movq	(%rdi), %rax
.Ltmp344:
	callq	*(%rax)
.Ltmp345:
# BB#3:                                 # %.noexc
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.LBB105_5
# BB#4:
	movq	(%rax), %rcx
.Ltmp346:
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	*96(%rcx)
.Ltmp347:
	jmp	.LBB105_5
.LBB105_1:                              # %.thread.i
	movq	$0, 16(%rbx)
.LBB105_5:                              # %.noexc2
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB105_6
# BB#7:
	movq	(%rdi), %rax
.Ltmp348:
	callq	*(%rax)
.Ltmp349:
# BB#8:                                 # %.noexc3
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.LBB105_10
# BB#9:
	movq	(%rax), %rcx
.Ltmp350:
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	*96(%rcx)
.Ltmp351:
	jmp	.LBB105_10
.LBB105_6:                              # %.thread9.i
	movq	$0, 24(%rbx)
.LBB105_10:
	movq	$0, 8(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB105_11:
.Ltmp352:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end105:
	.size	_ZNK8lam_node5cloneEv, .Lfunc_end105-_ZNK8lam_node5cloneEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table105:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin18-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp344-.Lfunc_begin18 #   Call between .Lfunc_begin18 and .Ltmp344
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp344-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Ltmp351-.Ltmp344       #   Call between .Ltmp344 and .Ltmp351
	.long	.Ltmp352-.Lfunc_begin18 #     jumps to .Ltmp352
	.byte	0                       #   On action: cleanup
	.long	.Ltmp351-.Lfunc_begin18 # >> Call Site 3 <<
	.long	.Lfunc_end105-.Ltmp351  #   Call between .Ltmp351 and .Lfunc_end105
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNK8lam_node2opEv,"axG",@progbits,_ZNK8lam_node2opEv,comdat
	.weak	_ZNK8lam_node2opEv
	.p2align	4, 0x90
	.type	_ZNK8lam_node2opEv,@function
_ZNK8lam_node2opEv:                     # @_ZNK8lam_node2opEv
	.cfi_startproc
# BB#0:
	movl	$4, %eax
	retq
.Lfunc_end106:
	.size	_ZNK8lam_node2opEv, .Lfunc_end106-_ZNK8lam_node2opEv
	.cfi_endproc

	.section	.text._ZNK8lam_node3argEv,"axG",@progbits,_ZNK8lam_node3argEv,comdat
	.weak	_ZNK8lam_node3argEv
	.p2align	4, 0x90
	.type	_ZNK8lam_node3argEv,@function
_ZNK8lam_node3argEv:                    # @_ZNK8lam_node3argEv
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	retq
.Lfunc_end107:
	.size	_ZNK8lam_node3argEv, .Lfunc_end107-_ZNK8lam_node3argEv
	.cfi_endproc

	.section	.text._ZNK8lam_node4bodyEv,"axG",@progbits,_ZNK8lam_node4bodyEv,comdat
	.weak	_ZNK8lam_node4bodyEv
	.p2align	4, 0x90
	.type	_ZNK8lam_node4bodyEv,@function
_ZNK8lam_node4bodyEv:                   # @_ZNK8lam_node4bodyEv
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	retq
.Lfunc_end108:
	.size	_ZNK8lam_node4bodyEv, .Lfunc_end108-_ZNK8lam_node4bodyEv
	.cfi_endproc

	.section	.text._ZNK8lam_node4bindEPK9alst_node,"axG",@progbits,_ZNK8lam_node4bindEPK9alst_node,comdat
	.weak	_ZNK8lam_node4bindEPK9alst_node
	.p2align	4, 0x90
	.type	_ZNK8lam_node4bindEPK9alst_node,@function
_ZNK8lam_node4bindEPK9alst_node:        # @_ZNK8lam_node4bindEPK9alst_node
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end109:
	.size	_ZNK8lam_node4bindEPK9alst_node, .Lfunc_end109-_ZNK8lam_node4bindEPK9alst_node
	.cfi_endproc

	.section	.text._ZN8lam_node11export_bodyEv,"axG",@progbits,_ZN8lam_node11export_bodyEv,comdat
	.weak	_ZN8lam_node11export_bodyEv
	.p2align	4, 0x90
	.type	_ZN8lam_node11export_bodyEv,@function
_ZN8lam_node11export_bodyEv:            # @_ZN8lam_node11export_bodyEv
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	movq	$0, 24(%rdi)
	retq
.Lfunc_end110:
	.size	_ZN8lam_node11export_bodyEv, .Lfunc_end110-_ZN8lam_node11export_bodyEv
	.cfi_endproc

	.section	.text._ZNK8app_node5cloneEv,"axG",@progbits,_ZNK8app_node5cloneEv,comdat
	.weak	_ZNK8app_node5cloneEv
	.p2align	4, 0x90
	.type	_ZNK8app_node5cloneEv,@function
_ZNK8app_node5cloneEv:                  # @_ZNK8app_node5cloneEv
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:
	pushq	%r14
.Lcfi459:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi460:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi461:
	.cfi_def_cfa_offset 32
.Lcfi462:
	.cfi_offset %rbx, -24
.Lcfi463:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV8app_node+16, (%rbx)
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB111_1
# BB#2:
	movq	(%rdi), %rax
.Ltmp353:
	callq	*(%rax)
.Ltmp354:
# BB#3:                                 # %.noexc
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.LBB111_5
# BB#4:
	movq	(%rax), %rcx
.Ltmp355:
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	*96(%rcx)
.Ltmp356:
	jmp	.LBB111_5
.LBB111_1:                              # %.thread.i
	movq	$0, 16(%rbx)
.LBB111_5:                              # %.noexc2
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB111_6
# BB#7:
	movq	(%rdi), %rax
.Ltmp357:
	callq	*(%rax)
.Ltmp358:
# BB#8:                                 # %.noexc3
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.LBB111_10
# BB#9:
	movq	(%rbx), %rax
.Ltmp359:
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.Ltmp360:
	jmp	.LBB111_10
.LBB111_6:                              # %.thread10.i
	movq	$0, 24(%rbx)
.LBB111_10:
	movq	8(%r14), %rax
	movq	%rax, 8(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB111_11:
.Ltmp361:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end111:
	.size	_ZNK8app_node5cloneEv, .Lfunc_end111-_ZNK8app_node5cloneEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table111:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin19-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp353-.Lfunc_begin19 #   Call between .Lfunc_begin19 and .Ltmp353
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp353-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Ltmp360-.Ltmp353       #   Call between .Ltmp353 and .Ltmp360
	.long	.Ltmp361-.Lfunc_begin19 #     jumps to .Ltmp361
	.byte	0                       #   On action: cleanup
	.long	.Ltmp360-.Lfunc_begin19 # >> Call Site 3 <<
	.long	.Lfunc_end111-.Ltmp360  #   Call between .Ltmp360 and .Lfunc_end111
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNK8app_node2opEv,"axG",@progbits,_ZNK8app_node2opEv,comdat
	.weak	_ZNK8app_node2opEv
	.p2align	4, 0x90
	.type	_ZNK8app_node2opEv,@function
_ZNK8app_node2opEv:                     # @_ZNK8app_node2opEv
	.cfi_startproc
# BB#0:
	movl	$5, %eax
	retq
.Lfunc_end112:
	.size	_ZNK8app_node2opEv, .Lfunc_end112-_ZNK8app_node2opEv
	.cfi_endproc

	.section	.text._ZNK8app_node4leftEv,"axG",@progbits,_ZNK8app_node4leftEv,comdat
	.weak	_ZNK8app_node4leftEv
	.p2align	4, 0x90
	.type	_ZNK8app_node4leftEv,@function
_ZNK8app_node4leftEv:                   # @_ZNK8app_node4leftEv
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	retq
.Lfunc_end113:
	.size	_ZNK8app_node4leftEv, .Lfunc_end113-_ZNK8app_node4leftEv
	.cfi_endproc

	.section	.text._ZNK8app_node5rightEv,"axG",@progbits,_ZNK8app_node5rightEv,comdat
	.weak	_ZNK8app_node5rightEv
	.p2align	4, 0x90
	.type	_ZNK8app_node5rightEv,@function
_ZNK8app_node5rightEv:                  # @_ZNK8app_node5rightEv
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	retq
.Lfunc_end114:
	.size	_ZNK8app_node5rightEv, .Lfunc_end114-_ZNK8app_node5rightEv
	.cfi_endproc

	.section	.text._ZNK8app_node4bindEPK9alst_node,"axG",@progbits,_ZNK8app_node4bindEPK9alst_node,comdat
	.weak	_ZNK8app_node4bindEPK9alst_node
	.p2align	4, 0x90
	.type	_ZNK8app_node4bindEPK9alst_node,@function
_ZNK8app_node4bindEPK9alst_node:        # @_ZNK8app_node4bindEPK9alst_node
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end115:
	.size	_ZNK8app_node4bindEPK9alst_node, .Lfunc_end115-_ZNK8app_node4bindEPK9alst_node
	.cfi_endproc

	.section	.text._ZN8app_node11export_leftEv,"axG",@progbits,_ZN8app_node11export_leftEv,comdat
	.weak	_ZN8app_node11export_leftEv
	.p2align	4, 0x90
	.type	_ZN8app_node11export_leftEv,@function
_ZN8app_node11export_leftEv:            # @_ZN8app_node11export_leftEv
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	movq	$0, 16(%rdi)
	retq
.Lfunc_end116:
	.size	_ZN8app_node11export_leftEv, .Lfunc_end116-_ZN8app_node11export_leftEv
	.cfi_endproc

	.section	.text._ZN8app_node12export_rightEv,"axG",@progbits,_ZN8app_node12export_rightEv,comdat
	.weak	_ZN8app_node12export_rightEv
	.p2align	4, 0x90
	.type	_ZN8app_node12export_rightEv,@function
_ZN8app_node12export_rightEv:           # @_ZN8app_node12export_rightEv
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	movq	$0, 24(%rdi)
	retq
.Lfunc_end117:
	.size	_ZN8app_node12export_rightEv, .Lfunc_end117-_ZN8app_node12export_rightEv
	.cfi_endproc

	.section	.text._ZNK11arglst_node5cloneEv,"axG",@progbits,_ZNK11arglst_node5cloneEv,comdat
	.weak	_ZNK11arglst_node5cloneEv
	.p2align	4, 0x90
	.type	_ZNK11arglst_node5cloneEv,@function
_ZNK11arglst_node5cloneEv:              # @_ZNK11arglst_node5cloneEv
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%r14
.Lcfi464:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi465:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi466:
	.cfi_def_cfa_offset 32
.Lcfi467:
	.cfi_offset %rbx, -24
.Lcfi468:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	$0, 8(%rbx)
	movq	$_ZTV11arglst_node+16, (%rbx)
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB118_1
# BB#2:
	movq	(%rdi), %rax
.Ltmp362:
	callq	*(%rax)
.Ltmp363:
	jmp	.LBB118_3
.LBB118_1:
	xorl	%eax, %eax
.LBB118_3:
	movq	%rax, 16(%rbx)
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB118_6
# BB#4:
	movq	(%rdi), %rax
.Ltmp364:
	callq	*(%rax)
.Ltmp365:
# BB#5:                                 # %.noexc2
	movq	%rax, 24(%rbx)
	jmp	.LBB118_7
.LBB118_6:
	movq	$0, 16(%rbx)
.LBB118_7:                              # %_ZN11arglst_nodeC2ERKS_.exit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB118_8:
.Ltmp366:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end118:
	.size	_ZNK11arglst_node5cloneEv, .Lfunc_end118-_ZNK11arglst_node5cloneEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table118:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin20-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp362-.Lfunc_begin20 #   Call between .Lfunc_begin20 and .Ltmp362
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp362-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Ltmp365-.Ltmp362       #   Call between .Ltmp362 and .Ltmp365
	.long	.Ltmp366-.Lfunc_begin20 #     jumps to .Ltmp366
	.byte	0                       #   On action: cleanup
	.long	.Ltmp365-.Lfunc_begin20 # >> Call Site 3 <<
	.long	.Lfunc_end118-.Ltmp365  #   Call between .Ltmp365 and .Lfunc_end118
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNK11arglst_node2opEv,"axG",@progbits,_ZNK11arglst_node2opEv,comdat
	.weak	_ZNK11arglst_node2opEv
	.p2align	4, 0x90
	.type	_ZNK11arglst_node2opEv,@function
_ZNK11arglst_node2opEv:                 # @_ZNK11arglst_node2opEv
	.cfi_startproc
# BB#0:
	movl	$6, %eax
	retq
.Lfunc_end119:
	.size	_ZNK11arglst_node2opEv, .Lfunc_end119-_ZNK11arglst_node2opEv
	.cfi_endproc

	.section	.text._ZNK11arglst_node3argEv,"axG",@progbits,_ZNK11arglst_node3argEv,comdat
	.weak	_ZNK11arglst_node3argEv
	.p2align	4, 0x90
	.type	_ZNK11arglst_node3argEv,@function
_ZNK11arglst_node3argEv:                # @_ZNK11arglst_node3argEv
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	retq
.Lfunc_end120:
	.size	_ZNK11arglst_node3argEv, .Lfunc_end120-_ZNK11arglst_node3argEv
	.cfi_endproc

	.section	.text._ZNK11arglst_node4nextEv,"axG",@progbits,_ZNK11arglst_node4nextEv,comdat
	.weak	_ZNK11arglst_node4nextEv
	.p2align	4, 0x90
	.type	_ZNK11arglst_node4nextEv,@function
_ZNK11arglst_node4nextEv:               # @_ZNK11arglst_node4nextEv
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	retq
.Lfunc_end121:
	.size	_ZNK11arglst_node4nextEv, .Lfunc_end121-_ZNK11arglst_node4nextEv
	.cfi_endproc

	.section	.text._ZN11stack_frameD0Ev,"axG",@progbits,_ZN11stack_frameD0Ev,comdat
	.weak	_ZN11stack_frameD0Ev
	.p2align	4, 0x90
	.type	_ZN11stack_frameD0Ev,@function
_ZN11stack_frameD0Ev:                   # @_ZN11stack_frameD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end122:
	.size	_ZN11stack_frameD0Ev, .Lfunc_end122-_ZN11stack_frameD0Ev
	.cfi_endproc

	.section	.text._ZNK11stack_frame2opEv,"axG",@progbits,_ZNK11stack_frame2opEv,comdat
	.weak	_ZNK11stack_frame2opEv
	.p2align	4, 0x90
	.type	_ZNK11stack_frame2opEv,@function
_ZNK11stack_frame2opEv:                 # @_ZNK11stack_frame2opEv
	.cfi_startproc
# BB#0:
	movl	$7, %eax
	retq
.Lfunc_end123:
	.size	_ZNK11stack_frame2opEv, .Lfunc_end123-_ZNK11stack_frame2opEv
	.cfi_endproc

	.section	.text._ZN4nodeD2Ev,"axG",@progbits,_ZN4nodeD2Ev,comdat
	.weak	_ZN4nodeD2Ev
	.p2align	4, 0x90
	.type	_ZN4nodeD2Ev,@function
_ZN4nodeD2Ev:                           # @_ZN4nodeD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end124:
	.size	_ZN4nodeD2Ev, .Lfunc_end124-_ZN4nodeD2Ev
	.cfi_endproc

	.type	definition_env,@object  # @definition_env
	.bss
	.globl	definition_env
	.p2align	3
definition_env:
	.quad	0
	.size	definition_env, 8

	.type	_ZL13name_sequence,@object # @_ZL13name_sequence
	.data
	.p2align	2
_ZL13name_sequence:
	.long	1                       # 0x1
	.size	_ZL13name_sequence, 4

	.type	_ZL27lambda_reduce_recurse_level,@object # @_ZL27lambda_reduce_recurse_level
	.local	_ZL27lambda_reduce_recurse_level
	.comm	_ZL27lambda_reduce_recurse_level,4,4
	.type	_ZL24app_reduce_recurse_level,@object # @_ZL24app_reduce_recurse_level
	.local	_ZL24app_reduce_recurse_level
	.comm	_ZL24app_reduce_recurse_level,4,4
	.type	_ZL24var_reduce_recurse_level,@object # @_ZL24var_reduce_recurse_level
	.local	_ZL24var_reduce_recurse_level
	.comm	_ZL24var_reduce_recurse_level,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"(%p:node)"
	.size	.L.str, 10

	.type	_ZTV8arg_node,@object   # @_ZTV8arg_node
	.section	.rodata,"a",@progbits
	.globl	_ZTV8arg_node
	.p2align	3
_ZTV8arg_node:
	.quad	0
	.quad	_ZTI8arg_node
	.quad	_ZNK8arg_node5cloneEv
	.quad	_ZN8arg_nodeD2Ev
	.quad	_ZN8arg_nodeD0Ev
	.quad	_ZNK8arg_node2opEv
	.quad	_ZNK8arg_node4nameEv
	.quad	_ZNK8arg_node5valueEv
	.quad	_ZNK4node3argEv
	.quad	_ZNK4node4bodyEv
	.quad	_ZNK4node4leftEv
	.quad	_ZNK4node5rightEv
	.quad	_ZNK4node4nextEv
	.quad	_ZNK4node6parentEv
	.quad	_ZN4node10set_parentEPS_
	.quad	_ZN4node6reduceEPK9alst_nodeiPi
	.quad	_ZNK8arg_node5printEPK9alst_nodei
	.quad	_ZN8arg_nodeaSERKS_
	.size	_ZTV8arg_node, 144

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"%s"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"(null)"
	.size	.L.str.2, 7

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" = "
	.size	.L.str.3, 4

	.type	_ZTV8var_node,@object   # @_ZTV8var_node
	.section	.rodata,"a",@progbits
	.globl	_ZTV8var_node
	.p2align	3
_ZTV8var_node:
	.quad	0
	.quad	_ZTI8var_node
	.quad	_ZNK8var_node5cloneEv
	.quad	_ZN8var_nodeD2Ev
	.quad	_ZN8var_nodeD0Ev
	.quad	_ZNK8var_node2opEv
	.quad	_ZNK8var_node4nameEv
	.quad	_ZNK4node5valueEv
	.quad	_ZNK4node3argEv
	.quad	_ZNK4node4bodyEv
	.quad	_ZNK4node4leftEv
	.quad	_ZNK4node5rightEv
	.quad	_ZNK4node4nextEv
	.quad	_ZNK4node6parentEv
	.quad	_ZN4node10set_parentEPS_
	.quad	_ZN8var_node6reduceEPK9alst_nodeiPi
	.quad	_ZNK8var_node5printEPK9alst_nodei
	.quad	_ZNK8var_node8has_freeEPK8arg_nodePK9alst_node
	.quad	_ZNK8var_node4bindEPK9alst_node
	.quad	_ZN8var_node11reduce_varsEPK9alst_nodeiPi
	.quad	_ZN8var_node6renameEP8arg_nodePKcP9alst_node
	.quad	_ZN8var_node18resolve_name_clashEP8arg_nodeP9alst_node
	.quad	_ZNK8var_nodeeqERK8exp_node
	.quad	_ZN8exp_node11export_bodyEv
	.quad	_ZN8exp_node11export_leftEv
	.quad	_ZN8exp_node12export_rightEv
	.quad	_ZNK8exp_node5matchEPK9alst_node
	.quad	_ZN8exp_node12extract_defsEPK9alst_node
	.quad	_ZNK8var_node7extractEPKci
	.quad	_ZN8var_node12reduce_valueEPK9alst_nodeiPiPS2_
	.size	_ZTV8var_node, 240

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"(null-var)"
	.size	.L.str.4, 11

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\nvar_reduce_recurse_level %d ["
	.size	.L.str.7, 31

	.type	_ZTV8lam_node,@object   # @_ZTV8lam_node
	.section	.rodata,"a",@progbits
	.globl	_ZTV8lam_node
	.p2align	3
_ZTV8lam_node:
	.quad	0
	.quad	_ZTI8lam_node
	.quad	_ZNK8lam_node5cloneEv
	.quad	_ZN8lam_nodeD2Ev
	.quad	_ZN8lam_nodeD0Ev
	.quad	_ZNK8lam_node2opEv
	.quad	_ZNK4node4nameEv
	.quad	_ZNK4node5valueEv
	.quad	_ZNK8lam_node3argEv
	.quad	_ZNK8lam_node4bodyEv
	.quad	_ZNK4node4leftEv
	.quad	_ZNK4node5rightEv
	.quad	_ZNK4node4nextEv
	.quad	_ZNK4node6parentEv
	.quad	_ZN4node10set_parentEPS_
	.quad	_ZN8lam_node6reduceEPK9alst_nodeiPi
	.quad	_ZNK8lam_node5printEPK9alst_nodei
	.quad	_ZNK8lam_node8has_freeEPK8arg_nodePK9alst_node
	.quad	_ZNK8lam_node4bindEPK9alst_node
	.quad	_ZN8lam_node11reduce_varsEPK9alst_nodeiPi
	.quad	_ZN8lam_node6renameEP8arg_nodePKcP9alst_node
	.quad	_ZN8lam_node18resolve_name_clashEP8arg_nodeP9alst_node
	.quad	_ZNK8lam_nodeeqERK8exp_node
	.quad	_ZN8lam_node11export_bodyEv
	.quad	_ZN8exp_node11export_leftEv
	.quad	_ZN8exp_node12export_rightEv
	.quad	_ZNK8exp_node5matchEPK9alst_node
	.quad	_ZN8lam_node12extract_defsEPK9alst_node
	.quad	_ZNK8lam_node7extractEPKci
	.quad	_ZN8lam_node10eta_reduceEPK9alst_nodeiPi
	.size	_ZTV8lam_node, 240

	.type	.L.str.11,@object       # @.str.11
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.11:
	.asciz	"%d"
	.size	.L.str.11, 3

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"(null-arg)"
	.size	.L.str.13, 11

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"(null-body)"
	.size	.L.str.17, 12

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"L:  "
	.size	.L.str.18, 5

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"\nlambda_reduce_recurse_level %d\n"
	.size	.L.str.20, 33

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"[%d.%d]"
	.size	.L.str.21, 8

	.type	_ZTV8app_node,@object   # @_ZTV8app_node
	.section	.rodata,"a",@progbits
	.globl	_ZTV8app_node
	.p2align	3
_ZTV8app_node:
	.quad	0
	.quad	_ZTI8app_node
	.quad	_ZNK8app_node5cloneEv
	.quad	_ZN8app_nodeD2Ev
	.quad	_ZN8app_nodeD0Ev
	.quad	_ZNK8app_node2opEv
	.quad	_ZNK4node4nameEv
	.quad	_ZNK4node5valueEv
	.quad	_ZNK4node3argEv
	.quad	_ZNK4node4bodyEv
	.quad	_ZNK8app_node4leftEv
	.quad	_ZNK8app_node5rightEv
	.quad	_ZNK4node4nextEv
	.quad	_ZNK4node6parentEv
	.quad	_ZN4node10set_parentEPS_
	.quad	_ZN8app_node6reduceEPK9alst_nodeiPi
	.quad	_ZNK8app_node5printEPK9alst_nodei
	.quad	_ZNK8app_node8has_freeEPK8arg_nodePK9alst_node
	.quad	_ZNK8app_node4bindEPK9alst_node
	.quad	_ZN8app_node11reduce_varsEPK9alst_nodeiPi
	.quad	_ZN8app_node6renameEP8arg_nodePKcP9alst_node
	.quad	_ZN8app_node18resolve_name_clashEP8arg_nodeP9alst_node
	.quad	_ZNK8app_nodeeqERK8exp_node
	.quad	_ZN8exp_node11export_bodyEv
	.quad	_ZN8app_node11export_leftEv
	.quad	_ZN8app_node12export_rightEv
	.quad	_ZNK8exp_node5matchEPK9alst_node
	.quad	_ZN8app_node12extract_defsEPK9alst_node
	.quad	_ZNK8app_node7extractEPKci
	.size	_ZTV8app_node, 232

	.type	.L.str.23,@object       # @.str.23
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.23:
	.asciz	"(null-right)"
	.size	.L.str.23, 13

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"()"
	.size	.L.str.25, 3

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"\napp_reduce_recurse_level %d\n"
	.size	.L.str.27, 30

	.type	_ZTV11arglst_node,@object # @_ZTV11arglst_node
	.section	.rodata,"a",@progbits
	.globl	_ZTV11arglst_node
	.p2align	3
_ZTV11arglst_node:
	.quad	0
	.quad	_ZTI11arglst_node
	.quad	_ZNK11arglst_node5cloneEv
	.quad	_ZN11arglst_nodeD2Ev
	.quad	_ZN11arglst_nodeD0Ev
	.quad	_ZNK11arglst_node2opEv
	.quad	_ZNK4node4nameEv
	.quad	_ZNK4node5valueEv
	.quad	_ZNK11arglst_node3argEv
	.quad	_ZNK4node4bodyEv
	.quad	_ZNK4node4leftEv
	.quad	_ZNK4node5rightEv
	.quad	_ZNK11arglst_node4nextEv
	.quad	_ZNK4node6parentEv
	.quad	_ZN4node10set_parentEPS_
	.quad	_ZN4node6reduceEPK9alst_nodeiPi
	.quad	_ZNK4node5printEPK9alst_nodei
	.quad	_ZN11arglst_node3addEP8arg_nodes
	.quad	_ZN11arglst_node4findEP8arg_node
	.quad	_ZN11arglst_node4listEv
	.size	_ZTV11arglst_node, 160

	.type	_ZTV4node,@object       # @_ZTV4node
	.globl	_ZTV4node
	.p2align	3
_ZTV4node:
	.quad	0
	.quad	_ZTI4node
	.quad	__cxa_pure_virtual
	.quad	_ZN4nodeD2Ev
	.quad	_ZN4nodeD0Ev
	.quad	_ZNK4node2opEv
	.quad	_ZNK4node4nameEv
	.quad	_ZNK4node5valueEv
	.quad	_ZNK4node3argEv
	.quad	_ZNK4node4bodyEv
	.quad	_ZNK4node4leftEv
	.quad	_ZNK4node5rightEv
	.quad	_ZNK4node4nextEv
	.quad	_ZNK4node6parentEv
	.quad	_ZN4node10set_parentEPS_
	.quad	_ZN4node6reduceEPK9alst_nodeiPi
	.quad	_ZNK4node5printEPK9alst_nodei
	.size	_ZTV4node, 136

	.type	_ZTS4node,@object       # @_ZTS4node
	.globl	_ZTS4node
_ZTS4node:
	.asciz	"4node"
	.size	_ZTS4node, 6

	.type	_ZTI4node,@object       # @_ZTI4node
	.globl	_ZTI4node
	.p2align	3
_ZTI4node:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS4node
	.size	_ZTI4node, 16

	.type	_ZTS8arg_node,@object   # @_ZTS8arg_node
	.globl	_ZTS8arg_node
_ZTS8arg_node:
	.asciz	"8arg_node"
	.size	_ZTS8arg_node, 10

	.type	_ZTI8arg_node,@object   # @_ZTI8arg_node
	.globl	_ZTI8arg_node
	.p2align	4
_ZTI8arg_node:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS8arg_node
	.quad	_ZTI4node
	.size	_ZTI8arg_node, 24

	.type	_ZTV8exp_node,@object   # @_ZTV8exp_node
	.globl	_ZTV8exp_node
	.p2align	3
_ZTV8exp_node:
	.quad	0
	.quad	_ZTI8exp_node
	.quad	__cxa_pure_virtual
	.quad	_ZN4nodeD2Ev
	.quad	_ZN8exp_nodeD0Ev
	.quad	_ZNK4node2opEv
	.quad	_ZNK4node4nameEv
	.quad	_ZNK4node5valueEv
	.quad	_ZNK4node3argEv
	.quad	_ZNK4node4bodyEv
	.quad	_ZNK4node4leftEv
	.quad	_ZNK4node5rightEv
	.quad	_ZNK4node4nextEv
	.quad	_ZNK4node6parentEv
	.quad	_ZN4node10set_parentEPS_
	.quad	_ZN4node6reduceEPK9alst_nodeiPi
	.quad	_ZNK4node5printEPK9alst_nodei
	.quad	_ZNK8exp_node8has_freeEPK8arg_nodePK9alst_node
	.quad	_ZNK8exp_node4bindEPK9alst_node
	.quad	_ZN8exp_node11reduce_varsEPK9alst_nodeiPi
	.quad	_ZN8exp_node6renameEP8arg_nodePKcP9alst_node
	.quad	_ZN8exp_node18resolve_name_clashEP8arg_nodeP9alst_node
	.quad	_ZNK8exp_nodeeqERKS_
	.quad	_ZN8exp_node11export_bodyEv
	.quad	_ZN8exp_node11export_leftEv
	.quad	_ZN8exp_node12export_rightEv
	.quad	_ZNK8exp_node5matchEPK9alst_node
	.quad	_ZN8exp_node12extract_defsEPK9alst_node
	.quad	_ZNK8exp_node7extractEPKci
	.size	_ZTV8exp_node, 232

	.type	_ZTS8exp_node,@object   # @_ZTS8exp_node
	.globl	_ZTS8exp_node
_ZTS8exp_node:
	.asciz	"8exp_node"
	.size	_ZTS8exp_node, 10

	.type	_ZTI8exp_node,@object   # @_ZTI8exp_node
	.globl	_ZTI8exp_node
	.p2align	4
_ZTI8exp_node:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS8exp_node
	.quad	_ZTI4node
	.size	_ZTI8exp_node, 24

	.type	_ZTS8var_node,@object   # @_ZTS8var_node
	.globl	_ZTS8var_node
_ZTS8var_node:
	.asciz	"8var_node"
	.size	_ZTS8var_node, 10

	.type	_ZTI8var_node,@object   # @_ZTI8var_node
	.globl	_ZTI8var_node
	.p2align	4
_ZTI8var_node:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS8var_node
	.quad	_ZTI8exp_node
	.size	_ZTI8var_node, 24

	.type	_ZTS8lam_node,@object   # @_ZTS8lam_node
	.globl	_ZTS8lam_node
_ZTS8lam_node:
	.asciz	"8lam_node"
	.size	_ZTS8lam_node, 10

	.type	_ZTI8lam_node,@object   # @_ZTI8lam_node
	.globl	_ZTI8lam_node
	.p2align	4
_ZTI8lam_node:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS8lam_node
	.quad	_ZTI8exp_node
	.size	_ZTI8lam_node, 24

	.type	_ZTS8app_node,@object   # @_ZTS8app_node
	.globl	_ZTS8app_node
_ZTS8app_node:
	.asciz	"8app_node"
	.size	_ZTS8app_node, 10

	.type	_ZTI8app_node,@object   # @_ZTI8app_node
	.globl	_ZTI8app_node
	.p2align	4
_ZTI8app_node:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS8app_node
	.quad	_ZTI8exp_node
	.size	_ZTI8app_node, 24

	.type	_ZTS11arglst_node,@object # @_ZTS11arglst_node
	.globl	_ZTS11arglst_node
_ZTS11arglst_node:
	.asciz	"11arglst_node"
	.size	_ZTS11arglst_node, 14

	.type	_ZTS9alst_node,@object  # @_ZTS9alst_node
	.section	.rodata._ZTS9alst_node,"aG",@progbits,_ZTS9alst_node,comdat
	.weak	_ZTS9alst_node
_ZTS9alst_node:
	.asciz	"9alst_node"
	.size	_ZTS9alst_node, 11

	.type	_ZTI9alst_node,@object  # @_ZTI9alst_node
	.section	.rodata._ZTI9alst_node,"aG",@progbits,_ZTI9alst_node,comdat
	.weak	_ZTI9alst_node
	.p2align	4
_ZTI9alst_node:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS9alst_node
	.quad	_ZTI4node
	.size	_ZTI9alst_node, 24

	.type	_ZTI11arglst_node,@object # @_ZTI11arglst_node
	.section	.rodata,"a",@progbits
	.globl	_ZTI11arglst_node
	.p2align	4
_ZTI11arglst_node:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS11arglst_node
	.quad	_ZTI9alst_node
	.size	_ZTI11arglst_node, 24

	.type	_ZTV11stack_frame,@object # @_ZTV11stack_frame
	.section	.rodata._ZTV11stack_frame,"aG",@progbits,_ZTV11stack_frame,comdat
	.weak	_ZTV11stack_frame
	.p2align	3
_ZTV11stack_frame:
	.quad	0
	.quad	_ZTI11stack_frame
	.quad	_ZNK11arglst_node5cloneEv
	.quad	_ZN11stack_frameD2Ev
	.quad	_ZN11stack_frameD0Ev
	.quad	_ZNK11stack_frame2opEv
	.quad	_ZNK4node4nameEv
	.quad	_ZNK4node5valueEv
	.quad	_ZNK11arglst_node3argEv
	.quad	_ZNK4node4bodyEv
	.quad	_ZNK4node4leftEv
	.quad	_ZNK4node5rightEv
	.quad	_ZNK11arglst_node4nextEv
	.quad	_ZNK4node6parentEv
	.quad	_ZN4node10set_parentEPS_
	.quad	_ZN4node6reduceEPK9alst_nodeiPi
	.quad	_ZNK4node5printEPK9alst_nodei
	.quad	_ZN11arglst_node3addEP8arg_nodes
	.quad	_ZN11arglst_node4findEP8arg_node
	.quad	_ZN11arglst_node4listEv
	.size	_ZTV11stack_frame, 160

	.type	_ZTS11stack_frame,@object # @_ZTS11stack_frame
	.section	.rodata._ZTS11stack_frame,"aG",@progbits,_ZTS11stack_frame,comdat
	.weak	_ZTS11stack_frame
_ZTS11stack_frame:
	.asciz	"11stack_frame"
	.size	_ZTS11stack_frame, 14

	.type	_ZTI11stack_frame,@object # @_ZTI11stack_frame
	.section	.rodata._ZTI11stack_frame,"aG",@progbits,_ZTI11stack_frame,comdat
	.weak	_ZTI11stack_frame
	.p2align	4
_ZTI11stack_frame:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS11stack_frame
	.quad	_ZTI11arglst_node
	.size	_ZTI11stack_frame, 24

	.type	.L.str.29,@object       # @.str.29
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.29:
	.asciz	"~"
	.size	.L.str.29, 2

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"%s#%d"
	.size	.L.str.30, 6

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"]"
	.size	.Lstr, 2


	.globl	_ZN8arg_nodeC1EPKcPK8exp_nodes
	.type	_ZN8arg_nodeC1EPKcPK8exp_nodes,@function
_ZN8arg_nodeC1EPKcPK8exp_nodes = _ZN8arg_nodeC2EPKcPK8exp_nodes
	.globl	_ZN8arg_nodeC1ERKS_
	.type	_ZN8arg_nodeC1ERKS_,@function
_ZN8arg_nodeC1ERKS_ = _ZN8arg_nodeC2ERKS_
	.globl	_ZN8arg_nodeD1Ev
	.type	_ZN8arg_nodeD1Ev,@function
_ZN8arg_nodeD1Ev = _ZN8arg_nodeD2Ev
	.globl	_ZN8var_nodeC1EPKc
	.type	_ZN8var_nodeC1EPKc,@function
_ZN8var_nodeC1EPKc = _ZN8var_nodeC2EPKc
	.globl	_ZN8var_nodeC1ERKS_
	.type	_ZN8var_nodeC1ERKS_,@function
_ZN8var_nodeC1ERKS_ = _ZN8var_nodeC2ERKS_
	.globl	_ZN8lam_nodeC1EP8arg_nodeP8exp_nodes
	.type	_ZN8lam_nodeC1EP8arg_nodeP8exp_nodes,@function
_ZN8lam_nodeC1EP8arg_nodeP8exp_nodes = _ZN8lam_nodeC2EP8arg_nodeP8exp_nodes
	.globl	_ZN8lam_nodeC1ERKS_
	.type	_ZN8lam_nodeC1ERKS_,@function
_ZN8lam_nodeC1ERKS_ = _ZN8lam_nodeC2ERKS_
	.globl	_ZN8lam_nodeD1Ev
	.type	_ZN8lam_nodeD1Ev,@function
_ZN8lam_nodeD1Ev = _ZN8lam_nodeD2Ev
	.globl	_ZN8app_nodeC1EP8exp_nodeS1_s
	.type	_ZN8app_nodeC1EP8exp_nodeS1_s,@function
_ZN8app_nodeC1EP8exp_nodeS1_s = _ZN8app_nodeC2EP8exp_nodeS1_s
	.globl	_ZN8app_nodeC1ERKS_
	.type	_ZN8app_nodeC1ERKS_,@function
_ZN8app_nodeC1ERKS_ = _ZN8app_nodeC2ERKS_
	.globl	_ZN8app_nodeD1Ev
	.type	_ZN8app_nodeD1Ev,@function
_ZN8app_nodeD1Ev = _ZN8app_nodeD2Ev
	.globl	_ZN11arglst_nodeC1EP8arg_nodePS_s
	.type	_ZN11arglst_nodeC1EP8arg_nodePS_s,@function
_ZN11arglst_nodeC1EP8arg_nodePS_s = _ZN11arglst_nodeC2EP8arg_nodePS_s
	.globl	_ZN11arglst_nodeC1ERKS_
	.type	_ZN11arglst_nodeC1ERKS_,@function
_ZN11arglst_nodeC1ERKS_ = _ZN11arglst_nodeC2ERKS_
	.globl	_ZN11arglst_nodeD1Ev
	.type	_ZN11arglst_nodeD1Ev,@function
_ZN11arglst_nodeD1Ev = _ZN11arglst_nodeD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
