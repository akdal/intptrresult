	.text
	.file	"struct_matrix.bc"
	.globl	hypre_StructMatrixExtractPointerByIndex
	.p2align	4, 0x90
	.type	hypre_StructMatrixExtractPointerByIndex,@function
hypre_StructMatrixExtractPointerByIndex: # @hypre_StructMatrixExtractPointerByIndex
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	%rdx, %rsi
	callq	hypre_StructStencilElementRank
	testl	%eax, %eax
	js	.LBB0_1
# BB#2:
	movq	64(%rbx), %rcx
	movslq	%ebp, %rdx
	movq	(%rcx,%rdx,8), %rcx
	cltq
	movslq	(%rcx,%rax,4), %rax
	shlq	$3, %rax
	addq	48(%rbx), %rax
	jmp	.LBB0_3
.LBB0_1:
	xorl	%eax, %eax
.LBB0_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_StructMatrixExtractPointerByIndex, .Lfunc_end0-hypre_StructMatrixExtractPointerByIndex
	.cfi_endproc

	.globl	hypre_StructMatrixCreate
	.p2align	4, 0x90
	.type	hypre_StructMatrixCreate,@function
hypre_StructMatrixCreate:               # @hypre_StructMatrixCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movl	%edi, %ebp
	movl	$1, %edi
	movl	$136, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbx
	movl	%ebp, (%rbx)
	leaq	8(%rbx), %rsi
	movq	%r15, %rdi
	callq	hypre_StructGridRef
	movq	%r14, %rdi
	callq	hypre_StructStencilRef
	movq	%rax, 16(%rbx)
	movl	$1, 56(%rbx)
	movl	$1, 128(%rbx)
	movl	$0, 72(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 88(%rbx)
	movq	$0, 104(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hypre_StructMatrixCreate, .Lfunc_end1-hypre_StructMatrixCreate
	.cfi_endproc

	.globl	hypre_StructMatrixRef
	.p2align	4, 0x90
	.type	hypre_StructMatrixRef,@function
hypre_StructMatrixRef:                  # @hypre_StructMatrixRef
	.cfi_startproc
# BB#0:
	incl	128(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end2:
	.size	hypre_StructMatrixRef, .Lfunc_end2-hypre_StructMatrixRef
	.cfi_endproc

	.globl	hypre_StructMatrixDestroy
	.p2align	4, 0x90
	.type	hypre_StructMatrixDestroy,@function
hypre_StructMatrixDestroy:              # @hypre_StructMatrixDestroy
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB3_8
# BB#1:
	decl	128(%r14)
	jne	.LBB3_8
# BB#2:
	cmpl	$0, 56(%r14)
	je	.LBB3_4
# BB#3:
	movq	48(%r14), %rdi
	callq	hypre_Free
	movq	$0, 48(%r14)
.LBB3_4:
	movq	120(%r14), %rdi
	callq	hypre_CommPkgDestroy
	movq	40(%r14), %rax
	movq	64(%r14), %rdi
	cmpl	$0, 8(%rax)
	jle	.LBB3_7
# BB#5:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rdi
	callq	hypre_Free
	movq	64(%r14), %rax
	movq	$0, (%rax,%rbx,8)
	incq	%rbx
	movq	40(%r14), %rax
	movq	64(%r14), %rdi
	movslq	8(%rax), %rax
	cmpq	%rax, %rbx
	jl	.LBB3_6
.LBB3_7:                                # %._crit_edge
	callq	hypre_Free
	movq	$0, 64(%r14)
	movq	40(%r14), %rdi
	callq	hypre_BoxArrayDestroy
	movq	80(%r14), %rdi
	callq	hypre_Free
	movq	$0, 80(%r14)
	movq	16(%r14), %rdi
	callq	hypre_StructStencilDestroy
	movq	24(%r14), %rdi
	callq	hypre_StructStencilDestroy
	movq	8(%r14), %rdi
	callq	hypre_StructGridDestroy
	movq	%r14, %rdi
	callq	hypre_Free
.LBB3_8:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	hypre_StructMatrixDestroy, .Lfunc_end3-hypre_StructMatrixDestroy
	.cfi_endproc

	.globl	hypre_StructMatrixInitializeShell
	.p2align	4, 0x90
	.type	hypre_StructMatrixInitializeShell,@function
hypre_StructMatrixInitializeShell:      # @hypre_StructMatrixInitializeShell
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 144
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	8(%r15), %r12
	movq	24(%r15), %rcx
	testq	%rcx, %rcx
	je	.LBB4_2
# BB#1:                                 # %._crit_edge252
	movq	80(%r15), %rax
	jmp	.LBB4_7
.LBB4_2:
	movq	16(%r15), %rdi
	leaq	24(%rsp), %rsi
	leaq	16(%rsp), %rdx
	callq	hypre_StructStencilSymmetrize
	movq	24(%rsp), %rcx
	movl	8(%rcx), %ebx
	cmpl	$0, 72(%r15)
	je	.LBB4_3
# BB#5:
	leal	1(%rbx), %eax
	shrl	$31, %eax
	leal	1(%rbx,%rax), %ebx
	sarl	%ebx
	jmp	.LBB4_6
.LBB4_3:                                # %.preheader196
	testl	%ebx, %ebx
	jle	.LBB4_6
# BB#4:                                 # %.lr.ph219
	movq	16(%rsp), %rdi
	leal	-1(%rbx), %eax
	leaq	4(,%rax,4), %rdx
	movl	$255, %esi
	callq	memset
	movq	24(%rsp), %rcx
.LBB4_6:                                # %.loopexit197
	movq	%rcx, 24(%r15)
	movq	16(%rsp), %rax
	movq	%rax, 80(%r15)
	movl	%ebx, 32(%r15)
.LBB4_7:
	movq	%rcx, 24(%rsp)
	movq	(%rcx), %r13
	movl	8(%rcx), %edi
	movq	%rax, 16(%rsp)
	testl	%edi, %edi
	jle	.LBB4_8
# BB#9:                                 # %.lr.ph217.preheader
	leaq	8(%r13), %rsi
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movq	%rdi, %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_10:                               # %.lr.ph217
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rax)
	js	.LBB4_12
# BB#11:                                # %.preheader195.preheader
                                        #   in Loop: Header=BB4_10 Depth=1
	xorl	%ebp, %ebp
	subl	-8(%rsi), %ebp
	xorl	%ebx, %ebx
	subl	-4(%rsi), %ebx
	movd	%ebx, %xmm1
	movd	%ebp, %xmm2
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movq	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movdqa	%xmm2, %xmm1
	pcmpgtd	%xmm0, %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm0, %xmm1
	por	%xmm2, %xmm1
	movl	(%rsi), %ebx
	movl	%ebx, %ebp
	negl	%ebp
	cmpl	%ebp, %edx
	cmovll	%ebp, %edx
	cmpl	%ebx, %r8d
	cmovll	%ebx, %r8d
	movdqa	%xmm1, %xmm0
.LBB4_12:                               # %.loopexit
                                        #   in Loop: Header=BB4_10 Depth=1
	addq	$4, %rax
	addq	$12, %rsi
	decq	%rcx
	jne	.LBB4_10
	jmp	.LBB4_13
.LBB4_8:
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%edx, %edx
.LBB4_13:                               # %.preheader194.preheader
	movdqu	88(%r15), %xmm1
	paddd	%xmm0, %xmm1
	movdqu	%xmm1, 88(%r15)
	addl	%edx, 104(%r15)
	addl	%r8d, 108(%r15)
	movq	40(%r15), %r14
	testq	%r14, %r14
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	jne	.LBB4_18
# BB#14:
	movq	8(%r12), %r14
	movl	8(%r14), %edi
	callq	hypre_BoxArrayCreate
	cmpl	$0, 8(%r14)
	jle	.LBB4_17
# BB#15:                                # %.lr.ph211
	movq	(%r14), %rbp
	movq	(%rax), %rcx
	addq	$20, %rbp
	addq	$20, %rcx
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB4_16:                               # =>This Inner Loop Header: Depth=1
	movl	-20(%rbp), %esi
	movl	%esi, -20(%rcx)
	movl	-16(%rbp), %edi
	movl	%edi, -16(%rcx)
	movl	-12(%rbp), %r9d
	movl	%r9d, -12(%rcx)
	movl	-8(%rbp), %ebx
	movl	%ebx, -8(%rcx)
	movl	-4(%rbp), %edx
	movl	%edx, -4(%rcx)
	movl	(%rbp), %r10d
	movl	%r10d, (%rcx)
	subl	88(%r15), %esi
	movl	%esi, -20(%rcx)
	addl	92(%r15), %ebx
	movl	%ebx, -8(%rcx)
	subl	96(%r15), %edi
	movl	%edi, -16(%rcx)
	addl	100(%r15), %edx
	movl	%edx, -4(%rcx)
	subl	104(%r15), %r9d
	movl	%r9d, -12(%rcx)
	addl	108(%r15), %r10d
	movl	%r10d, (%rcx)
	incq	%r8
	movslq	8(%r14), %rdx
	addq	$24, %rbp
	addq	$24, %rcx
	cmpq	%rdx, %r8
	jl	.LBB4_16
.LBB4_17:                               # %._crit_edge212
	movq	%rax, %r14
	movq	%r14, 40(%r15)
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB4_18:
	cmpq	$0, 64(%r15)
	jne	.LBB4_43
# BB#19:
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movl	8(%r14), %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbp
	cmpl	$0, 8(%r14)
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	jle	.LBB4_20
# BB#21:                                # %.lr.ph205
	movq	8(%rsp), %rdi           # 8-byte Reload
	testl	%edi, %edi
	jle	.LBB4_22
# BB#24:                                # %.lr.ph205.split.us.preheader
	movl	%edi, %eax
	andl	$1, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	addq	$8, %r13
	movq	%r13, 72(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	xorl	%r13d, %r13d
	movq	%r14, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_25:                               # %.lr.ph205.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_32 Depth 2
                                        #     Child Loop BB4_38 Depth 2
	movq	(%r14), %r15
	leaq	(%rbp,%rbp,2), %r14
	movl	20(%r15,%r14,8), %eax
	movl	8(%r15,%r14,8), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	incl	%edx
	cmpl	%ecx, %eax
	movl	16(%r15,%r14,8), %eax
	movl	4(%r15,%r14,8), %ecx
	cmovsl	%r12d, %edx
	movl	%eax, %esi
	subl	%ecx, %esi
	incl	%esi
	cmpl	%ecx, %eax
	movl	12(%r15,%r14,8), %eax
	movl	(%r15,%r14,8), %ecx
	cmovsl	%r12d, %esi
	movl	%eax, %ebx
	subl	%ecx, %ebx
	incl	%ebx
	cmpl	%ecx, %eax
	cmovsl	%r12d, %ebx
	imull	%esi, %ebx
	imull	%edx, %ebx
	movl	$4, %esi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	hypre_CAlloc
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	movq	%rax, (%rcx,%rbp,8)
	movq	16(%rsp), %r9
	jne	.LBB4_27
# BB#26:                                #   in Loop: Header=BB4_25 Depth=1
	xorl	%edi, %edi
	jmp	.LBB4_30
	.p2align	4, 0x90
.LBB4_27:                               #   in Loop: Header=BB4_25 Depth=1
	cmpl	$0, (%r9)
	jns	.LBB4_29
# BB#28:                                #   in Loop: Header=BB4_25 Depth=1
	movl	%r13d, (%rax)
	addl	%ebx, %r13d
.LBB4_29:                               # %.prol.loopexit
                                        #   in Loop: Header=BB4_25 Depth=1
	movl	$1, %edi
.LBB4_30:                               # %.prol.loopexit
                                        #   in Loop: Header=BB4_25 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	$1, %ecx
	je	.LBB4_37
# BB#31:                                # %.lr.ph205.split.us.new
                                        #   in Loop: Header=BB4_25 Depth=1
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	4(%r9,%rdi,4), %rsi
	leaq	4(%rax,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB4_32:                               #   Parent Loop BB4_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, -4(%rsi)
	jns	.LBB4_34
# BB#33:                                #   in Loop: Header=BB4_32 Depth=2
	movl	%r13d, -4(%rdi)
	addl	%ebx, %r13d
.LBB4_34:                               #   in Loop: Header=BB4_32 Depth=2
	cmpl	$0, (%rsi)
	jns	.LBB4_36
# BB#35:                                #   in Loop: Header=BB4_32 Depth=2
	movl	%r13d, (%rdi)
	addl	%ebx, %r13d
.LBB4_36:                               #   in Loop: Header=BB4_32 Depth=2
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rdx
	jne	.LBB4_32
.LBB4_37:                               # %.lr.ph201.us.preheader
                                        #   in Loop: Header=BB4_25 Depth=1
	leaq	12(%r15,%r14,8), %r8
	movq	%rcx, %rsi
	movq	%rax, %rdi
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	56(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_38:                               # %.lr.ph201.us
                                        #   Parent Loop BB4_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%r9), %r10
	testq	%r10, %r10
	js	.LBB4_40
# BB#39:                                #   in Loop: Header=BB4_38 Depth=2
	movl	(%r8), %ebp
	movl	-12(%r8), %ecx
	movl	%ebp, %edx
	subl	%ecx, %edx
	incl	%edx
	cmpl	%ecx, %ebp
	movl	4(%r8), %r11d
	movl	-8(%r8), %ebp
	cmovsl	%r12d, %edx
	movl	%r11d, %ecx
	subl	%ebp, %ecx
	incl	%ecx
	cmpl	%ebp, %r11d
	movl	-8(%rbx), %ebp
	cmovsl	%r12d, %ecx
	imull	(%rbx), %ecx
	addl	-4(%rbx), %ecx
	imull	%edx, %ecx
	addl	(%rax,%r10,4), %ebp
	addl	%ecx, %ebp
	movl	%ebp, (%rdi)
.LBB4_40:                               #   in Loop: Header=BB4_38 Depth=2
	addq	$4, %r9
	addq	$12, %rbx
	addq	$4, %rdi
	decq	%rsi
	jne	.LBB4_38
# BB#41:                                # %._crit_edge.us
                                        #   in Loop: Header=BB4_25 Depth=1
	movq	80(%rsp), %rbp          # 8-byte Reload
	incq	%rbp
	movslq	8(%r14), %rax
	cmpq	%rax, %rbp
	movq	8(%rsp), %rdi           # 8-byte Reload
	jl	.LBB4_25
	jmp	.LBB4_42
.LBB4_20:
	xorl	%r13d, %r13d
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB4_42
.LBB4_22:                               # %._crit_edge.preheader
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_23:                               # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movl	$4, %esi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	hypre_CAlloc
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rax, (%rbp,%rbx,8)
	incq	%rbx
	movslq	8(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB4_23
.LBB4_42:                               # %._crit_edge206
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, 64(%r15)
	movl	%r13d, 60(%r15)
	movq	40(%rsp), %r12          # 8-byte Reload
.LBB4_43:
	movl	52(%r12), %eax
	imull	%edi, %eax
	movl	%eax, 112(%r15)
	xorl	%eax, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	hypre_StructMatrixInitializeShell, .Lfunc_end4-hypre_StructMatrixInitializeShell
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.text
	.globl	hypre_StructMatrixInitializeData
	.p2align	4, 0x90
	.type	hypre_StructMatrixInitializeData,@function
hypre_StructMatrixInitializeData:       # @hypre_StructMatrixInitializeData
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi38:
	.cfi_def_cfa_offset 144
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%rsi, 48(%rdi)
	movl	$0, 56(%rdi)
	movq	$0, 76(%rsp)
	movl	$0, 84(%rsp)
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	40(%rdi), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmpl	$0, 8(%rax)
	jle	.LBB5_16
# BB#1:                                 # %.lr.ph263
	movabsq	$4607182418800017408, %rbp # imm = 0x3FF0000000000000
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB5_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_9 Depth 2
                                        #       Child Loop BB5_10 Depth 3
                                        #         Child Loop BB5_21 Depth 4
                                        #         Child Loop BB5_24 Depth 4
                                        #         Child Loop BB5_12 Depth 4
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rdi
	leaq	76(%rsp), %rsi
	callq	hypre_StructStencilElementRank
	testl	%eax, %eax
	js	.LBB5_15
# BB#3:                                 # %hypre_StructMatrixExtractPointerByIndex.exit
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	48(%rcx), %r14
	movq	64(%rcx), %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	cltq
	movslq	(%rcx,%rax,4), %r12
	leaq	(,%r12,8), %rax
	addq	%r14, %rax
	je	.LBB5_15
# BB#4:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r15
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%rax,%rax,2), %rbx
	leaq	(%r15,%rbx,8), %rdi
	leaq	44(%rsp), %rsi
	callq	hypre_BoxGetSize
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	movl	12(%r15,%rbx,8), %eax
	movl	(%r15,%rbx,8), %ecx
	movl	%eax, %r13d
	subl	%ecx, %r13d
	incl	%r13d
	cmpl	%ecx, %eax
	movl	$0, %eax
	cmovsl	%eax, %r13d
	movl	44(%rsp), %r8d
	movl	48(%rsp), %r10d
	movl	52(%rsp), %ecx
	cmpl	%r8d, %r10d
	movl	%r8d, %eax
	cmovgel	%r10d, %eax
	cmpl	%eax, %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	cmovgel	%ecx, %eax
	testl	%eax, %eax
	jle	.LBB5_15
# BB#5:                                 # %.preheader202.lr.ph
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	16(%r15,%rbx,8), %eax
	subl	4(%r15,%rbx,8), %eax
	leal	1(%rax), %ecx
	testl	%eax, %eax
	movl	$0, %eax
	cmovsl	%eax, %ecx
	movl	%ecx, (%rsp)            # 4-byte Spill
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB5_15
# BB#6:                                 # %.preheader202.us.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	testl	%r10d, %r10d
	jle	.LBB5_15
# BB#7:                                 # %.preheader202.us.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	testl	%r8d, %r8d
	jle	.LBB5_15
# BB#8:                                 # %.preheader201.us.us.us.preheader
                                        #   in Loop: Header=BB5_2 Depth=1
	leal	-1(%r8), %edi
	incq	%rdi
	movq	%rdi, %rbx
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	andq	%rax, %rbx
	leaq	-4(%rbx), %r11
	movq	%r11, 64(%rsp)          # 8-byte Spill
                                        # kill: %R11D<def> %R11D<kill> %R11<kill> %R11<def>
	shrl	$2, %r11d
	incl	%r11d
	andl	$3, %r11d
	leaq	16(%r14,%r12,8), %rsi
	movl	(%rsp), %eax            # 4-byte Reload
	imull	%r13d, %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movq	%r11, %rax
	negq	%rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_9:                                # %.preheader201.us.us.us
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_10 Depth 3
                                        #         Child Loop BB5_21 Depth 4
                                        #         Child Loop BB5_24 Depth 4
                                        #         Child Loop BB5_12 Depth 4
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movl	%r12d, 20(%rsp)         # 4-byte Spill
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB5_10:                               # %.preheader.us.us.us.us
                                        #   Parent Loop BB5_2 Depth=1
                                        #     Parent Loop BB5_9 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB5_21 Depth 4
                                        #         Child Loop BB5_24 Depth 4
                                        #         Child Loop BB5_12 Depth 4
	movslq	%r12d, %r15
	xorl	%ecx, %ecx
	cmpq	$3, %rdi
	jbe	.LBB5_11
# BB#17:                                # %min.iters.checked
                                        #   in Loop: Header=BB5_10 Depth=3
	testq	%rbx, %rbx
	je	.LBB5_11
# BB#18:                                # %vector.body.preheader
                                        #   in Loop: Header=BB5_10 Depth=3
	testq	%r11, %r11
	je	.LBB5_19
# BB#20:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB5_10 Depth=3
	leaq	(%rsi,%r15,8), %rcx
	movq	56(%rsp), %rax          # 8-byte Reload
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_21:                               # %vector.body.prol
                                        #   Parent Loop BB5_2 Depth=1
                                        #     Parent Loop BB5_9 Depth=2
                                        #       Parent Loop BB5_10 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	%xmm0, -16(%rcx,%r14,8)
	movups	%xmm0, (%rcx,%r14,8)
	addq	$4, %r14
	incq	%rax
	jne	.LBB5_21
	jmp	.LBB5_22
.LBB5_19:                               #   in Loop: Header=BB5_10 Depth=3
	xorl	%r14d, %r14d
.LBB5_22:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB5_10 Depth=3
	cmpq	$12, 64(%rsp)           # 8-byte Folded Reload
	jb	.LBB5_25
# BB#23:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB5_10 Depth=3
	leaq	-16(%rsi,%r15,8), %rcx
	.p2align	4, 0x90
.LBB5_24:                               # %vector.body
                                        #   Parent Loop BB5_2 Depth=1
                                        #     Parent Loop BB5_9 Depth=2
                                        #       Parent Loop BB5_10 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	%xmm0, (%rcx,%r14,8)
	movups	%xmm0, 16(%rcx,%r14,8)
	movups	%xmm0, 32(%rcx,%r14,8)
	movups	%xmm0, 48(%rcx,%r14,8)
	movups	%xmm0, 64(%rcx,%r14,8)
	movups	%xmm0, 80(%rcx,%r14,8)
	movups	%xmm0, 96(%rcx,%r14,8)
	movups	%xmm0, 112(%rcx,%r14,8)
	addq	$16, %r14
	cmpq	%r14, %rbx
	jne	.LBB5_24
.LBB5_25:                               # %middle.block
                                        #   in Loop: Header=BB5_10 Depth=3
	cmpq	%rbx, %rdi
	je	.LBB5_13
# BB#26:                                #   in Loop: Header=BB5_10 Depth=3
	addq	%rbx, %r15
	movl	%ebx, %ecx
	.p2align	4, 0x90
.LBB5_11:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB5_10 Depth=3
	leaq	-16(%rsi,%r15,8), %rax
	movl	%r8d, %edx
	subl	%ecx, %edx
	.p2align	4, 0x90
.LBB5_12:                               # %scalar.ph
                                        #   Parent Loop BB5_2 Depth=1
                                        #     Parent Loop BB5_9 Depth=2
                                        #       Parent Loop BB5_10 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rbp, (%rax)
	addq	$8, %rax
	decl	%edx
	jne	.LBB5_12
.LBB5_13:                               # %._crit_edge.us.us.us.us
                                        #   in Loop: Header=BB5_10 Depth=3
	addl	%r13d, %r12d
	incl	%r9d
	cmpl	%r10d, %r9d
	jne	.LBB5_10
# BB#14:                                # %._crit_edge207.us-lcssa.us.us.us.us
                                        #   in Loop: Header=BB5_9 Depth=2
	movl	16(%rsp), %ecx          # 4-byte Reload
	incl	%ecx
	movl	20(%rsp), %r12d         # 4-byte Reload
	addl	(%rsp), %r12d           # 4-byte Folded Reload
	cmpl	4(%rsp), %ecx           # 4-byte Folded Reload
	jne	.LBB5_9
	.p2align	4, 0x90
.LBB5_15:                               # %hypre_StructMatrixExtractPointerByIndex.exit.thread
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rdx
	incq	%rdx
	movq	24(%rsp), %rax          # 8-byte Reload
	movslq	8(%rax), %rax
	movq	%rdx, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	cmpq	%rax, %rdx
	jl	.LBB5_2
.LBB5_16:                               # %._crit_edge264
	xorl	%eax, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	hypre_StructMatrixInitializeData, .Lfunc_end5-hypre_StructMatrixInitializeData
	.cfi_endproc

	.globl	hypre_StructMatrixInitialize
	.p2align	4, 0x90
	.type	hypre_StructMatrixInitialize,@function
hypre_StructMatrixInitialize:           # @hypre_StructMatrixInitialize
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 16
.Lcfi46:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	hypre_StructMatrixInitializeShell
	movl	60(%rbx), %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	hypre_StructMatrixInitializeData
	movl	$1, 56(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end6:
	.size	hypre_StructMatrixInitialize, .Lfunc_end6-hypre_StructMatrixInitialize
	.cfi_endproc

	.globl	hypre_StructMatrixSetValues
	.p2align	4, 0x90
	.type	hypre_StructMatrixSetValues,@function
hypre_StructMatrixSetValues:            # @hypre_StructMatrixSetValues
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 56
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movl	%r9d, -44(%rsp)         # 4-byte Spill
	movq	%r8, -56(%rsp)          # 8-byte Spill
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	movl	%edx, -76(%rsp)         # 4-byte Spill
	movq	%rdi, -72(%rsp)         # 8-byte Spill
	movq	8(%rdi), %rax
	movq	8(%rax), %rax
	movslq	8(%rax), %r10
	testq	%r10, %r10
	jle	.LBB7_24
# BB#1:                                 # %.lr.ph113
	movq	(%rax), %r8
	movl	(%rsi), %r11d
	movl	-76(%rsp), %eax         # 4-byte Reload
	leaq	-1(%rax), %rcx
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	movq	%rax, -24(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movq	-64(%rsp), %rax         # 8-byte Reload
	leaq	4(%rax), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movq	-56(%rsp), %rax         # 8-byte Reload
	leaq	8(%rax), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	xorl	%ebp, %ebp
	jmp	.LBB7_2
.LBB7_8:                                #   in Loop: Header=BB7_2 Depth=1
	cmpl	$0, -44(%rsp)           # 4-byte Folded Reload
	je	.LBB7_16
# BB#9:                                 # %.preheader107
                                        #   in Loop: Header=BB7_2 Depth=1
	cmpl	$0, -76(%rsp)           # 4-byte Folded Reload
	jle	.LBB7_23
# BB#10:                                # %.lr.ph
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	-72(%rsp), %rcx         # 8-byte Reload
	movq	40(%rcx), %rax
	movq	48(%rcx), %r13
	movq	(%rax), %rax
	movl	(%rax,%r14,8), %ecx
	movl	%ecx, -48(%rsp)         # 4-byte Spill
	movl	12(%rax,%r14,8), %edx
	movl	%edx, %ebx
	subl	%ecx, %ebx
	incl	%ebx
	cmpl	%ecx, %edx
	movl	4(%rax,%r14,8), %edx
	movl	16(%rax,%r14,8), %ecx
	movl	$0, %edi
	cmovsl	%edi, %ebx
	xorl	%edi, %edi
	movl	%ecx, %r9d
	subl	%edx, %r9d
	incl	%r9d
	cmpl	%edx, %ecx
	cmovsl	%edi, %r9d
	movl	%r11d, %ecx
	subl	-48(%rsp), %ecx         # 4-byte Folded Reload
	subl	%edx, %r15d
	subl	8(%rax,%r14,8), %r12d
	imull	%r9d, %r12d
	addl	%r15d, %r12d
	imull	%ebx, %r12d
	addl	%ecx, %r12d
	cmpq	$0, -16(%rsp)           # 8-byte Folded Reload
	movq	-72(%rsp), %rax         # 8-byte Reload
	movq	64(%rax), %rax
	movq	(%rax,%rbp,8), %r14
	movslq	%r12d, %r15
	jne	.LBB7_12
# BB#11:                                #   in Loop: Header=BB7_2 Depth=1
	xorl	%ecx, %ecx
	cmpq	$0, -8(%rsp)            # 8-byte Folded Reload
	jne	.LBB7_14
	jmp	.LBB7_23
.LBB7_16:                               # %.preheader
                                        #   in Loop: Header=BB7_2 Depth=1
	cmpl	$0, -76(%rsp)           # 4-byte Folded Reload
	jle	.LBB7_23
# BB#17:                                # %.lr.ph111
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	-72(%rsp), %rcx         # 8-byte Reload
	movq	40(%rcx), %rax
	movq	48(%rcx), %r13
	movq	(%rax), %rax
	movl	(%rax,%r14,8), %r9d
	movl	12(%rax,%r14,8), %edx
	movl	%edx, %edi
	subl	%r9d, %edi
	incl	%edi
	cmpl	%r9d, %edx
	movl	4(%rax,%r14,8), %edx
	movl	16(%rax,%r14,8), %ecx
	movl	$0, %ebx
	cmovsl	%ebx, %edi
	movl	%ecx, %ebx
	subl	%edx, %ebx
	incl	%ebx
	cmpl	%edx, %ecx
	movl	$0, %ecx
	cmovsl	%ecx, %ebx
	movl	%r11d, %ecx
	subl	%r9d, %ecx
	subl	%edx, %r15d
	subl	8(%rax,%r14,8), %r12d
	imull	%ebx, %r12d
	addl	%r15d, %r12d
	imull	%edi, %r12d
	addl	%ecx, %r12d
	cmpq	$0, -16(%rsp)           # 8-byte Folded Reload
	movq	-72(%rsp), %rax         # 8-byte Reload
	movq	64(%rax), %rax
	movq	(%rax,%rbp,8), %r14
	movslq	%r12d, %r9
	jne	.LBB7_19
# BB#18:                                #   in Loop: Header=BB7_2 Depth=1
	xorl	%edx, %edx
	cmpq	$0, -8(%rsp)            # 8-byte Folded Reload
	jne	.LBB7_21
	jmp	.LBB7_23
.LBB7_12:                               #   in Loop: Header=BB7_2 Depth=1
	movq	-64(%rsp), %rax         # 8-byte Reload
	movslq	(%rax), %rax
	movslq	(%r14,%rax,4), %rax
	leaq	(%r13,%rax,8), %rax
	movq	-56(%rsp), %rcx         # 8-byte Reload
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addsd	(%rax,%r15,8), %xmm0
	movsd	%xmm0, (%rax,%r15,8)
	movl	$1, %ecx
	cmpq	$0, -8(%rsp)            # 8-byte Folded Reload
	je	.LBB7_23
.LBB7_14:                               # %.lr.ph.new
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	-24(%rsp), %rbx         # 8-byte Reload
	subq	%rcx, %rbx
	movq	-32(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,4), %rax
	movq	-40(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB7_15:                               #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	-4(%rax), %rdx
	movslq	(%r14,%rdx,4), %rdx
	leaq	(%r13,%rdx,8), %rdx
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	addsd	(%rdx,%r15,8), %xmm0
	movsd	%xmm0, (%rdx,%r15,8)
	movslq	(%rax), %rdx
	movslq	(%r14,%rdx,4), %rdx
	leaq	(%r13,%rdx,8), %rdx
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addsd	(%rdx,%r15,8), %xmm0
	movsd	%xmm0, (%rdx,%r15,8)
	addq	$8, %rax
	addq	$16, %rcx
	addq	$-2, %rbx
	jne	.LBB7_15
	jmp	.LBB7_23
.LBB7_19:                               #   in Loop: Header=BB7_2 Depth=1
	movq	-64(%rsp), %rax         # 8-byte Reload
	movslq	(%rax), %rax
	movslq	(%r14,%rax,4), %rax
	leaq	(%r13,%rax,8), %rax
	movq	-56(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx), %rcx
	movq	%rcx, (%rax,%r9,8)
	movl	$1, %edx
	cmpq	$0, -8(%rsp)            # 8-byte Folded Reload
	je	.LBB7_23
.LBB7_21:                               # %.lr.ph111.new
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	-24(%rsp), %rax         # 8-byte Reload
	subq	%rdx, %rax
	movq	-32(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,4), %rcx
	movq	-40(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB7_22:                               #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	-4(%rcx), %rdi
	movslq	(%r14,%rdi,4), %rdi
	leaq	(%r13,%rdi,8), %rdi
	movq	-8(%rdx), %rbx
	movq	%rbx, (%rdi,%r9,8)
	movslq	(%rcx), %rdi
	movslq	(%r14,%rdi,4), %rdi
	leaq	(%r13,%rdi,8), %rdi
	movq	(%rdx), %rbx
	movq	%rbx, (%rdi,%r9,8)
	addq	$8, %rcx
	addq	$16, %rdx
	addq	$-2, %rax
	jne	.LBB7_22
	jmp	.LBB7_23
	.p2align	4, 0x90
.LBB7_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_15 Depth 2
                                        #     Child Loop BB7_22 Depth 2
	leaq	(%rbp,%rbp,2), %r14
	cmpl	(%r8,%r14,8), %r11d
	jl	.LBB7_23
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	cmpl	12(%r8,%r14,8), %r11d
	jg	.LBB7_23
# BB#4:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	4(%rsi), %r15d
	cmpl	4(%r8,%r14,8), %r15d
	jl	.LBB7_23
# BB#5:                                 #   in Loop: Header=BB7_2 Depth=1
	cmpl	16(%r8,%r14,8), %r15d
	jg	.LBB7_23
# BB#6:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	8(%rsi), %r12d
	cmpl	8(%r8,%r14,8), %r12d
	jl	.LBB7_23
# BB#7:                                 #   in Loop: Header=BB7_2 Depth=1
	cmpl	20(%r8,%r14,8), %r12d
	jle	.LBB7_8
	.p2align	4, 0x90
.LBB7_23:                               # %.loopexit
                                        #   in Loop: Header=BB7_2 Depth=1
	incq	%rbp
	cmpq	%r10, %rbp
	jl	.LBB7_2
.LBB7_24:                               # %._crit_edge
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	hypre_StructMatrixSetValues, .Lfunc_end7-hypre_StructMatrixSetValues
	.cfi_endproc

	.globl	hypre_StructMatrixSetBoxValues
	.p2align	4, 0x90
	.type	hypre_StructMatrixSetBoxValues,@function
hypre_StructMatrixSetBoxValues:         # @hypre_StructMatrixSetBoxValues
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
	subq	$344, %rsp              # imm = 0x158
.Lcfi65:
	.cfi_def_cfa_offset 400
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movl	%r9d, 164(%rsp)         # 4-byte Spill
	movq	%r8, %r14
	movq	%rcx, 296(%rsp)         # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, 200(%rsp)         # 8-byte Spill
	movq	8(%rdi), %rax
	movq	8(%rax), %rbx
	movl	8(%rbx), %edi
	callq	hypre_BoxArrayCreate
	movq	%rax, %r13
	callq	hypre_BoxCreate
	movq	%rax, %r12
	cmpl	$0, 8(%rbx)
	jle	.LBB8_4
# BB#1:                                 # %.lr.ph681
	xorl	%ebp, %ebp
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	%r13, %r15
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	addq	%rbp, %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rdx
	callq	hypre_IntersectBoxes
	movl	(%r12), %eax
	movq	(%r15), %rcx
	movl	%eax, (%rcx,%rbp)
	movl	4(%r12), %eax
	movl	%eax, 4(%rcx,%rbp)
	movl	8(%r12), %eax
	movl	%eax, 8(%rcx,%rbp)
	movl	12(%r12), %eax
	movl	%eax, 12(%rcx,%rbp)
	movl	16(%r12), %eax
	movl	%eax, 16(%rcx,%rbp)
	movl	20(%r12), %eax
	movl	%eax, 20(%rcx,%rbp)
	incq	%r13
	movslq	8(%rbx), %rax
	addq	$24, %rbp
	cmpq	%rax, %r13
	jl	.LBB8_2
# BB#3:                                 # %._crit_edge682.thread
	movq	%r12, %rdi
	callq	hypre_BoxDestroy
	movq	%r15, %rbx
	movq	16(%rsp), %r15          # 8-byte Reload
	jmp	.LBB8_5
.LBB8_4:                                # %._crit_edge682
	movq	%r12, %rdi
	callq	hypre_BoxDestroy
	testq	%r13, %r13
	movq	%r13, %rbx
	je	.LBB8_56
.LBB8_5:
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	40(%rax), %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	callq	hypre_BoxDuplicate
	movl	(%rax), %edx
	movq	72(%rsp), %rcx          # 8-byte Reload
	imull	%ecx, %edx
	movl	%edx, (%rax)
	movl	12(%rax), %edx
	imull	%ecx, %edx
	leal	-1(%rcx,%rdx), %ecx
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movl	%ecx, 12(%rax)
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB8_55
# BB#6:                                 # %.lr.ph677
	movq	72(%rsp), %rcx          # 8-byte Reload
	movslq	%ecx, %r13
	movl	%ecx, %ecx
	movq	%rcx, 288(%rsp)         # 8-byte Spill
	movq	%r14, %rcx
	notq	%rcx
	movq	%rcx, 336(%rsp)         # 8-byte Spill
	movq	%r13, %rcx
	shlq	$5, %rcx
	movq	%rcx, 248(%rsp)         # 8-byte Spill
	leaq	(,%r13,8), %rcx
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rcx,2), %rcx
	movq	%rcx, 232(%rsp)         # 8-byte Spill
	movq	%r13, %rcx
	shlq	$4, %rcx
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	leaq	(%r14,%r13,8), %rbp
	xorl	%esi, %esi
	movq	%rbx, 272(%rsp)         # 8-byte Spill
	movq	%rbp, 216(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB8_7:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_10 Depth 2
                                        #       Child Loop BB8_15 Depth 3
                                        #         Child Loop BB8_17 Depth 4
                                        #           Child Loop BB8_30 Depth 5
                                        #           Child Loop BB8_24 Depth 5
                                        #       Child Loop BB8_39 Depth 3
                                        #         Child Loop BB8_41 Depth 4
                                        #           Child Loop BB8_44 Depth 5
                                        #           Child Loop BB8_48 Depth 5
	movq	(%rbx), %rdx
	leaq	(%rsi,%rsi,2), %rcx
	leaq	(,%rcx,8), %rdi
	addq	%rdx, %rdi
	movq	%rdi, 208(%rsp)         # 8-byte Spill
	je	.LBB8_54
# BB#8:                                 #   in Loop: Header=BB8_7 Depth=1
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	jle	.LBB8_54
# BB#9:                                 # %.lr.ph666
                                        #   in Loop: Header=BB8_7 Depth=1
	movq	280(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	leaq	4(%rdx,%rcx,8), %rdi
	movq	%rdi, 312(%rsp)         # 8-byte Spill
	movl	4(%rdx,%rcx,8), %edi
	movl	%edi, 172(%rsp)         # 4-byte Spill
	movl	8(%rdx,%rcx,8), %edx
	movl	%edx, 168(%rsp)         # 4-byte Spill
	movq	208(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx), %edx
	imull	72(%rsp), %edx          # 4-byte Folded Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	xorl	%edi, %edi
	movq	%rsi, 320(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB8_10:                               #   Parent Loop BB8_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_15 Depth 3
                                        #         Child Loop BB8_17 Depth 4
                                        #           Child Loop BB8_30 Depth 5
                                        #           Child Loop BB8_24 Depth 5
                                        #       Child Loop BB8_39 Depth 3
                                        #         Child Loop BB8_41 Depth 4
                                        #           Child Loop BB8_44 Depth 5
                                        #           Child Loop BB8_48 Depth 5
	movl	%edx, 96(%rsp)          # 4-byte Spill
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	48(%rax), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	64(%rax), %rax
	movq	(%rax,%rsi,8), %rax
	movq	296(%rsp), %rcx         # 8-byte Reload
	movq	%rdi, 328(%rsp)         # 8-byte Spill
	movslq	(%rcx,%rdi,4), %rcx
	movslq	(%rax,%rcx,4), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	208(%rsp), %r12         # 8-byte Reload
	movq	%r12, %rdi
	leaq	260(%rsp), %rsi
	callq	hypre_BoxGetSize
	movq	192(%rsp), %rbp         # 8-byte Reload
	movl	(%rbp), %r11d
	movl	4(%rbp), %edx
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movl	12(%rbp), %ecx
	movl	%ecx, %r8d
	subl	%r11d, %r8d
	incl	%r8d
	cmpl	%r11d, %ecx
	movl	16(%rbp), %ecx
	movl	$0, %eax
	cmovsl	%eax, %r8d
	movl	%ecx, %esi
	subl	%edx, %esi
	incl	%esi
	cmpl	%edx, %ecx
	movq	304(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx), %r9d
	movl	12(%rdx), %ecx
	cmovsl	%eax, %esi
	xorl	%eax, %eax
	movl	%ecx, %r15d
	subl	%r9d, %r15d
	incl	%r15d
	cmpl	%r9d, %ecx
	movl	4(%rdx), %ecx
	movl	16(%rdx), %ebx
	cmovsl	%eax, %r15d
	movl	%ebx, %edi
	subl	%ecx, %edi
	incl	%edi
	cmpl	%ecx, %ebx
	movl	(%r12), %eax
	movq	312(%rsp), %rbx         # 8-byte Reload
	movl	(%rbx), %r12d
	movl	4(%rbx), %ebx
	movl	$0, %r10d
	cmovsl	%r10d, %edi
	subl	%r9d, %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	subl	%ecx, %r12d
	subl	8(%rdx), %ebx
	imull	%edi, %ebx
	addl	%r12d, %ebx
	movq	80(%rsp), %rax          # 8-byte Reload
	imull	%r15d, %ebx
	movl	%ebx, %edx
	movl	96(%rsp), %r12d         # 4-byte Reload
	subl	%r11d, %r12d
	movl	172(%rsp), %ecx         # 4-byte Reload
	subl	16(%rsp), %ecx          # 4-byte Folded Reload
	movl	168(%rsp), %ebx         # 4-byte Reload
	subl	8(%rbp), %ebx
	imull	%esi, %ebx
	addl	%ecx, %ebx
	imull	%r8d, %ebx
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	movl	%r8d, %ebx
	imull	%r15d, %edi
	movl	%edi, 28(%rsp)          # 4-byte Spill
	imull	%ebx, %esi
	movl	%esi, 32(%rsp)          # 4-byte Spill
	movl	260(%rsp), %ecx
	movl	264(%rsp), %esi
	movl	268(%rsp), %edi
	cmpl	%ecx, %esi
	movq	%rcx, 48(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	cmovgel	%esi, %ecx
	cmpl	%ecx, %edi
	movl	%edi, 36(%rsp)          # 4-byte Spill
	cmovgel	%edi, %ecx
	cmpl	$0, 164(%rsp)           # 4-byte Folded Reload
	movq	%rax, %r8
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rsi
	movq	%rsi, 152(%rsp)         # 8-byte Spill
	movl	%r15d, 100(%rsp)        # 4-byte Spill
	je	.LBB8_35
# BB#11:                                #   in Loop: Header=BB8_10 Depth=2
	testl	%ecx, %ecx
	movq	216(%rsp), %rbp         # 8-byte Reload
	movl	%edx, %r11d
	jle	.LBB8_52
# BB#12:                                # %.lr.ph621
                                        #   in Loop: Header=BB8_10 Depth=2
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	jle	.LBB8_52
# BB#13:                                # %.lr.ph621
                                        #   in Loop: Header=BB8_10 Depth=2
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB8_52
# BB#14:                                # %.preheader585.us.preheader
                                        #   in Loop: Header=BB8_10 Depth=2
	movl	%r15d, %eax
	movq	48(%rsp), %r9           # 8-byte Reload
	subl	%r9d, %eax
	movl	%r9d, %ecx
	imull	72(%rsp), %ecx          # 4-byte Folded Reload
	movl	%ebx, %edi
	subl	%ecx, %edi
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %esi
	imull	%r15d, %esi
	movl	%esi, 180(%rsp)         # 4-byte Spill
	subl	%esi, 28(%rsp)          # 4-byte Folded Spill
	movl	%ebx, %esi
	imull	%edx, %esi
	movl	%esi, 176(%rsp)         # 4-byte Spill
	subl	%esi, 32(%rsp)          # 4-byte Folded Spill
	leal	-1(%rdx), %edx
	imull	%edx, %eax
	addl	%r15d, %eax
	subl	%r9d, %eax
	movl	%eax, 188(%rsp)         # 4-byte Spill
	imull	%edx, %edi
	addl	%ebx, %edi
	subl	%ecx, %edi
	movl	%edi, 184(%rsp)         # 4-byte Spill
	leal	-1(%r9), %ecx
	addl	%r12d, 12(%rsp)         # 4-byte Folded Spill
	addl	56(%rsp), %r11d         # 4-byte Folded Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx), %rax
	leaq	8(%r8,%rax,8), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	%r13, %rax
	imulq	%rcx, %rax
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	incq	%rcx
	leaq	(%r14,%rax,8), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	andq	%rax, %rcx
	movq	%r13, %rax
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	imulq	%rcx, %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	16(%r8,%rdx,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	movl	%ebx, 144(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB8_15:                               # %.preheader585.us
                                        #   Parent Loop BB8_7 Depth=1
                                        #     Parent Loop BB8_10 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB8_17 Depth 4
                                        #           Child Loop BB8_30 Depth 5
                                        #           Child Loop BB8_24 Depth 5
	movq	48(%rsp), %r9           # 8-byte Reload
	testl	%r9d, %r9d
	movl	184(%rsp), %eax         # 4-byte Reload
	movl	188(%rsp), %ecx         # 4-byte Reload
	jle	.LBB8_34
# BB#16:                                # %.preheader583.us.us.preheader
                                        #   in Loop: Header=BB8_15 Depth=3
	movl	%edx, 68(%rsp)          # 4-byte Spill
	xorl	%r12d, %r12d
	movl	12(%rsp), %r8d          # 4-byte Reload
	movl	%r11d, %eax
	movl	%r11d, 80(%rsp)         # 4-byte Spill
	jmp	.LBB8_17
.LBB8_29:                               # %vector.body.preheader
                                        #   in Loop: Header=BB8_17 Depth=4
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r10,8), %rcx
	shlq	$3, %rdi
	addq	104(%rsp), %rsi         # 8-byte Folded Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	addq	%r9, %r10
	movq	%r10, 120(%rsp)         # 8-byte Spill
	movq	232(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi), %r10
	movq	224(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi), %r11
	movq	240(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi), %r15
	movq	%r14, %rbx
	movq	248(%rsp), %rdx         # 8-byte Reload
	.p2align	4, 0x90
.LBB8_30:                               # %vector.body
                                        #   Parent Loop BB8_7 Depth=1
                                        #     Parent Loop BB8_10 Depth=2
                                        #       Parent Loop BB8_15 Depth=3
                                        #         Parent Loop BB8_17 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movsd	(%rdi,%rbx), %xmm0      # xmm0 = mem[0],zero
	movhpd	(%r15,%rbx), %xmm0      # xmm0 = xmm0[0],mem[0]
	movsd	(%r11,%rbx), %xmm1      # xmm1 = mem[0],zero
	movhpd	(%r10,%rbx), %xmm1      # xmm1 = xmm1[0],mem[0]
	movupd	-16(%rcx), %xmm2
	movupd	(%rcx), %xmm3
	addpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm3
	movupd	%xmm2, -16(%rcx)
	movupd	%xmm3, (%rcx)
	addq	$32, %rcx
	addq	%rdx, %rbx
	addq	$-4, %r9
	jne	.LBB8_30
# BB#31:                                # %middle.block
                                        #   in Loop: Header=BB8_17 Depth=4
	movq	88(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, 56(%rsp)          # 8-byte Folded Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	movq	216(%rsp), %rbp         # 8-byte Reload
	movl	100(%rsp), %r15d        # 4-byte Reload
	movl	80(%rsp), %r11d         # 4-byte Reload
	movq	48(%rsp), %r9           # 8-byte Reload
	movq	120(%rsp), %r10         # 8-byte Reload
	jne	.LBB8_19
	jmp	.LBB8_32
	.p2align	4, 0x90
.LBB8_17:                               # %.preheader583.us.us
                                        #   Parent Loop BB8_7 Depth=1
                                        #     Parent Loop BB8_10 Depth=2
                                        #       Parent Loop BB8_15 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB8_30 Depth 5
                                        #           Child Loop BB8_24 Depth 5
	movslq	%eax, %r10
	movslq	%r8d, %rdi
	movl	%ebx, %ecx
	imull	%r12d, %ecx
	addl	12(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rsi
	leaq	(,%rsi,8), %rcx
	movq	336(%rsp), %rbx         # 8-byte Reload
	subq	%rcx, %rbx
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi,8), %rdx
	movq	%rdx, %rcx
	notq	%rcx
	cmpq	%rcx, %rbx
	cmovaq	%rbx, %rcx
	leaq	(%r14,%rsi,8), %rbx
	cmpq	%rbx, %rdx
	cmovaq	%rdx, %rbx
	cmpq	$3, 56(%rsp)            # 8-byte Folded Reload
	jbe	.LBB8_18
# BB#25:                                # %min.iters.checked
                                        #   in Loop: Header=BB8_17 Depth=4
	cmpq	$0, 88(%rsp)            # 8-byte Folded Reload
	je	.LBB8_18
# BB#26:                                # %vector.memcheck
                                        #   in Loop: Header=BB8_17 Depth=4
	movl	%r15d, %edx
	imull	%r12d, %edx
	addl	%r11d, %edx
	movslq	%edx, %r9
	movq	152(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r9,8), %rdx
	addq	$8, %rbx
	cmpq	%rbx, %rdx
	jae	.LBB8_29
# BB#27:                                # %vector.memcheck
                                        #   in Loop: Header=BB8_17 Depth=4
	movq	112(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r9,8), %rdx
	notq	%rcx
	cmpq	%rdx, %rcx
	jae	.LBB8_29
# BB#28:                                #   in Loop: Header=BB8_17 Depth=4
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	movq	48(%rsp), %r9           # 8-byte Reload
	jmp	.LBB8_19
	.p2align	4, 0x90
.LBB8_18:                               #   in Loop: Header=BB8_17 Depth=4
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
.LBB8_19:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB8_17 Depth=4
	movl	%r9d, %edx
	subl	%ecx, %edx
	testb	$1, %dl
	jne	.LBB8_21
# BB#20:                                #   in Loop: Header=BB8_17 Depth=4
	movl	%ecx, %ebx
	cmpl	%ecx, 128(%rsp)         # 4-byte Folded Reload
	jne	.LBB8_23
	jmp	.LBB8_32
	.p2align	4, 0x90
.LBB8_21:                               # %scalar.ph.prol
                                        #   in Loop: Header=BB8_17 Depth=4
	movsd	(%r14,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movq	152(%rsp), %rdx         # 8-byte Reload
	addsd	(%rdx,%r10,8), %xmm0
	movsd	%xmm0, (%rdx,%r10,8)
	incq	%r10
	addq	%r13, %rsi
	leal	1(%rcx), %ebx
	cmpl	%ecx, 128(%rsp)         # 4-byte Folded Reload
	je	.LBB8_32
.LBB8_23:                               # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB8_17 Depth=4
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	-8(%rcx,%r10,8), %rcx
	movl	%r9d, %edi
	subl	%ebx, %edi
	.p2align	4, 0x90
.LBB8_24:                               # %scalar.ph
                                        #   Parent Loop BB8_7 Depth=1
                                        #     Parent Loop BB8_10 Depth=2
                                        #       Parent Loop BB8_15 Depth=3
                                        #         Parent Loop BB8_17 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movsd	(%r14,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	addsd	-8(%rcx), %xmm0
	movsd	%xmm0, -8(%rcx)
	movsd	(%rbp,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	addq	%r13, %rsi
	addsd	(%rcx), %xmm0
	movsd	%xmm0, (%rcx)
	addq	%r13, %rsi
	addq	$16, %rcx
	addl	$-2, %edi
	jne	.LBB8_24
.LBB8_32:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB8_17 Depth=4
	addl	%r15d, %eax
	movl	144(%rsp), %ebx         # 4-byte Reload
	addl	%ebx, %r8d
	incl	%r12d
	cmpl	16(%rsp), %r12d         # 4-byte Folded Reload
	jne	.LBB8_17
# BB#33:                                #   in Loop: Header=BB8_15 Depth=3
	movl	176(%rsp), %eax         # 4-byte Reload
	movl	180(%rsp), %ecx         # 4-byte Reload
	movl	68(%rsp), %edx          # 4-byte Reload
.LBB8_34:                               # %._crit_edge595.us
                                        #   in Loop: Header=BB8_15 Depth=3
	addl	%r11d, %ecx
	addl	12(%rsp), %eax          # 4-byte Folded Reload
	addl	28(%rsp), %ecx          # 4-byte Folded Reload
	addl	32(%rsp), %eax          # 4-byte Folded Reload
	incl	%edx
	cmpl	36(%rsp), %edx          # 4-byte Folded Reload
	movl	%ecx, %r11d
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jne	.LBB8_15
	jmp	.LBB8_52
	.p2align	4, 0x90
.LBB8_35:                               #   in Loop: Header=BB8_10 Depth=2
	testl	%ecx, %ecx
	jle	.LBB8_52
# BB#36:                                # %.lr.ph658
                                        #   in Loop: Header=BB8_10 Depth=2
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	jle	.LBB8_52
# BB#37:                                # %.lr.ph658
                                        #   in Loop: Header=BB8_10 Depth=2
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB8_52
# BB#38:                                # %.preheader584.us.preheader
                                        #   in Loop: Header=BB8_10 Depth=2
	movl	%r15d, %eax
	movq	48(%rsp), %r11          # 8-byte Reload
	subl	%r11d, %eax
	movl	%r11d, %ecx
	imull	72(%rsp), %ecx          # 4-byte Folded Reload
	movl	%ebx, %edi
	subl	%ecx, %edi
	movq	16(%rsp), %r9           # 8-byte Reload
	movl	%r9d, %esi
	imull	%r15d, %esi
	movl	%esi, 104(%rsp)         # 4-byte Spill
	subl	%esi, 28(%rsp)          # 4-byte Folded Spill
	leal	-1(%r9), %esi
	imull	%esi, %eax
	movl	%edi, 136(%rsp)         # 4-byte Spill
	imull	%edi, %esi
	addl	%ebx, %esi
	imull	%r9d, %ebx
	movl	%ebx, 68(%rsp)          # 4-byte Spill
	subl	%ebx, 32(%rsp)          # 4-byte Folded Spill
	addl	%r15d, %eax
	subl	%r11d, %eax
	movl	%eax, 120(%rsp)         # 4-byte Spill
	subl	%ecx, %esi
	movl	%esi, 112(%rsp)         # 4-byte Spill
	leal	-1(%r11), %eax
	addl	%r12d, 12(%rsp)         # 4-byte Folded Spill
	movq	%rax, 144(%rsp)         # 8-byte Spill
	incq	%rax
	imulq	%r13, %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	addl	56(%rsp), %edx          # 4-byte Folded Reload
	movl	%r11d, %edi
	andl	$3, %edi
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	24(%r8,%rax,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB8_39:                               # %.preheader584.us
                                        #   Parent Loop BB8_7 Depth=1
                                        #     Parent Loop BB8_10 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB8_41 Depth 4
                                        #           Child Loop BB8_44 Depth 5
                                        #           Child Loop BB8_48 Depth 5
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	movl	112(%rsp), %eax         # 4-byte Reload
	movl	120(%rsp), %ecx         # 4-byte Reload
	jle	.LBB8_51
# BB#40:                                # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB8_39 Depth=3
	movl	%esi, 40(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	movl	%edx, 80(%rsp)          # 4-byte Spill
	movl	%edx, %r9d
	movl	12(%rsp), %r10d         # 4-byte Reload
	.p2align	4, 0x90
.LBB8_41:                               # %.preheader.us.us
                                        #   Parent Loop BB8_7 Depth=1
                                        #     Parent Loop BB8_10 Depth=2
                                        #       Parent Loop BB8_39 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB8_44 Depth 5
                                        #           Child Loop BB8_48 Depth 5
	movl	%eax, 56(%rsp)          # 4-byte Spill
	testl	%edi, %edi
	movslq	%r9d, %r9
	movslq	%r10d, %r11
	je	.LBB8_42
# BB#43:                                # %.prol.preheader
                                        #   in Loop: Header=BB8_41 Depth=4
	movq	152(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r9,8), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_44:                               #   Parent Loop BB8_7 Depth=1
                                        #     Parent Loop BB8_10 Depth=2
                                        #       Parent Loop BB8_39 Depth=3
                                        #         Parent Loop BB8_41 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	(%r14,%r11,8), %rdx
	movq	%rdx, (%rax,%rcx,8)
	addq	%r13, %r11
	incq	%rcx
	cmpl	%ecx, %edi
	jne	.LBB8_44
# BB#45:                                # %.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB8_41 Depth=4
	leaq	(%r9,%rcx), %rax
	jmp	.LBB8_46
	.p2align	4, 0x90
.LBB8_42:                               #   in Loop: Header=BB8_41 Depth=4
	movq	%r9, %rax
	xorl	%ecx, %ecx
.LBB8_46:                               # %.prol.loopexit
                                        #   in Loop: Header=BB8_41 Depth=4
	cmpl	$3, 144(%rsp)           # 4-byte Folded Reload
	movq	248(%rsp), %r8          # 8-byte Reload
	jb	.LBB8_49
# BB#47:                                # %.preheader.us.us.new
                                        #   in Loop: Header=BB8_41 Depth=4
	movq	232(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r11,8), %r12
	movq	224(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r11,8), %r15
	movq	240(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r11,8), %rbx
	leaq	(,%r11,8), %rdx
	movq	88(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rax,8), %rsi
	movq	48(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%ecx, %eax
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB8_48:                               #   Parent Loop BB8_7 Depth=1
                                        #     Parent Loop BB8_10 Depth=2
                                        #       Parent Loop BB8_39 Depth=3
                                        #         Parent Loop BB8_41 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	(%rdx,%rcx), %rbp
	movq	%rbp, -24(%rsi)
	movq	(%rbx,%rcx), %rbp
	movq	%rbp, -16(%rsi)
	movq	(%r15,%rcx), %rbp
	movq	%rbp, -8(%rsi)
	movq	(%r12,%rcx), %rbp
	movq	%rbp, (%rsi)
	addq	%r8, %rcx
	addq	$32, %rsi
	addl	$-4, %eax
	jne	.LBB8_48
.LBB8_49:                               # %._crit_edge626.us.us
                                        #   in Loop: Header=BB8_41 Depth=4
	addl	128(%rsp), %r10d        # 4-byte Folded Reload
	addl	100(%rsp), %r9d         # 4-byte Folded Reload
	addl	136(%rsp), %r10d        # 4-byte Folded Reload
	movl	56(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	16(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB8_41
# BB#50:                                #   in Loop: Header=BB8_39 Depth=3
	movl	68(%rsp), %eax          # 4-byte Reload
	movl	104(%rsp), %ecx         # 4-byte Reload
	movl	80(%rsp), %edx          # 4-byte Reload
	movl	40(%rsp), %esi          # 4-byte Reload
.LBB8_51:                               # %._crit_edge632.us
                                        #   in Loop: Header=BB8_39 Depth=3
	addl	%edx, %ecx
	addl	12(%rsp), %eax          # 4-byte Folded Reload
	addl	28(%rsp), %ecx          # 4-byte Folded Reload
	addl	32(%rsp), %eax          # 4-byte Folded Reload
	incl	%esi
	cmpl	36(%rsp), %esi          # 4-byte Folded Reload
	movl	%ecx, %edx
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jne	.LBB8_39
	.p2align	4, 0x90
.LBB8_52:                               # %.loopexit
                                        #   in Loop: Header=BB8_10 Depth=2
	movl	96(%rsp), %edx          # 4-byte Reload
	incl	%edx
	movq	328(%rsp), %rdi         # 8-byte Reload
	incq	%rdi
	cmpq	288(%rsp), %rdi         # 8-byte Folded Reload
	movq	320(%rsp), %rsi         # 8-byte Reload
	jne	.LBB8_10
# BB#53:                                # %.loopexit587.loopexit
                                        #   in Loop: Header=BB8_7 Depth=1
	movq	272(%rsp), %rbx         # 8-byte Reload
	movl	8(%rbx), %eax
.LBB8_54:                               # %.loopexit587
                                        #   in Loop: Header=BB8_7 Depth=1
	incq	%rsi
	movslq	%eax, %rcx
	cmpq	%rcx, %rsi
	jl	.LBB8_7
.LBB8_55:                               # %._crit_edge678
	movq	192(%rsp), %rdi         # 8-byte Reload
	callq	hypre_BoxDestroy
.LBB8_56:
	movq	%rbx, %rdi
	callq	hypre_BoxArrayDestroy
	xorl	%eax, %eax
	addq	$344, %rsp              # imm = 0x158
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	hypre_StructMatrixSetBoxValues, .Lfunc_end8-hypre_StructMatrixSetBoxValues
	.cfi_endproc

	.globl	hypre_StructMatrixAssemble
	.p2align	4, 0x90
	.type	hypre_StructMatrixAssemble,@function
hypre_StructMatrixAssemble:             # @hypre_StructMatrixAssemble
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 16
	subq	$64, %rsp
.Lcfi73:
	.cfi_def_cfa_offset 80
.Lcfi74:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB9_2
# BB#1:
	leaq	88(%rbx), %rsi
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 52(%rsp)
	movl	$1, 60(%rsp)
	movq	8(%rbx), %rdi
	leaq	40(%rsp), %rdx
	leaq	32(%rsp), %rcx
	leaq	24(%rsp), %r8
	leaq	16(%rsp), %r9
	callq	hypre_CreateCommInfoFromNumGhost
	movq	40(%rsp), %rdi
	movq	32(%rsp), %rsi
	movq	40(%rbx), %r8
	movl	32(%rbx), %r10d
	movl	(%rbx), %r11d
	movq	8(%rbx), %rax
	addq	$56, %rax
	subq	$8, %rsp
.Lcfi75:
	.cfi_adjust_cfa_offset 8
	leaq	60(%rsp), %rdx
	movq	%rdx, %rcx
	movq	%r8, %r9
	pushq	%rax
.Lcfi76:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi77:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi78:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)
.Lcfi79:
	.cfi_adjust_cfa_offset 8
	pushq	64(%rsp)
.Lcfi80:
	.cfi_adjust_cfa_offset 8
	callq	hypre_CommPkgCreate
	addq	$48, %rsp
.Lcfi81:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %rdi
	movq	%rdi, 120(%rbx)
.LBB9_2:
	movq	48(%rbx), %rsi
	leaq	8(%rsp), %rcx
	movq	%rsi, %rdx
	callq	hypre_InitializeCommunication
	movq	8(%rsp), %rdi
	callq	hypre_FinalizeCommunication
	xorl	%eax, %eax
	addq	$64, %rsp
	popq	%rbx
	retq
.Lfunc_end9:
	.size	hypre_StructMatrixAssemble, .Lfunc_end9-hypre_StructMatrixAssemble
	.cfi_endproc

	.globl	hypre_StructMatrixSetNumGhost
	.p2align	4, 0x90
	.type	hypre_StructMatrixSetNumGhost,@function
hypre_StructMatrixSetNumGhost:          # @hypre_StructMatrixSetNumGhost
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	movl	%eax, 88(%rdi)
	movl	4(%rsi), %eax
	movl	%eax, 92(%rdi)
	movl	8(%rsi), %eax
	movl	%eax, 96(%rdi)
	movl	12(%rsi), %eax
	movl	%eax, 100(%rdi)
	movl	16(%rsi), %eax
	movl	%eax, 104(%rdi)
	movl	20(%rsi), %eax
	movl	%eax, 108(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	hypre_StructMatrixSetNumGhost, .Lfunc_end10-hypre_StructMatrixSetNumGhost
	.cfi_endproc

	.globl	hypre_StructMatrixPrint
	.p2align	4, 0x90
	.type	hypre_StructMatrixPrint,@function
hypre_StructMatrixPrint:                # @hypre_StructMatrixPrint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi88:
	.cfi_def_cfa_offset 352
.Lcfi89:
	.cfi_offset %rbx, -56
.Lcfi90:
	.cfi_offset %r12, -48
.Lcfi91:
	.cfi_offset %r13, -40
.Lcfi92:
	.cfi_offset %r14, -32
.Lcfi93:
	.cfi_offset %r15, -24
.Lcfi94:
	.cfi_offset %rbp, -16
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	(%r14), %edi
	leaq	12(%rsp), %rsi
	callq	hypre_MPI_Comm_rank
	movl	12(%rsp), %ecx
	leaq	32(%rsp), %rbp
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	sprintf
	movl	$.L.str.1, %esi
	movq	%rbp, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB11_9
# BB#1:
	movl	$.L.str.3, %edi
	movl	$13, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	72(%r14), %edx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.5, %edi
	movl	$7, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	8(%r14), %rsi
	movq	%rbx, %rdi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	callq	hypre_StructGridPrint
	movl	$.L.str.6, %edi
	movl	$10, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	24(%r14), %rbp
	movq	(%rbp), %r15
	movl	32(%r14), %edx
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	80(%r14), %r12
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%edx, 4(%rsp)           # 4-byte Spill
	callq	fprintf
	movl	8(%rbp), %eax
	testl	%eax, %eax
	jle	.LBB11_6
# BB#2:                                 # %.lr.ph.preheader
	addq	$8, %r15
	xorl	%r13d, %r13d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB11_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%r12,%r13,4)
	jns	.LBB11_5
# BB#4:                                 #   in Loop: Header=BB11_3 Depth=1
	leal	1(%rdx), %r14d
	movl	-8(%r15), %ecx
	movl	-4(%r15), %r8d
	movl	(%r15), %r9d
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	movl	8(%rbp), %eax
	movl	%r14d, %edx
.LBB11_5:                               #   in Loop: Header=BB11_3 Depth=1
	incq	%r13
	movslq	%eax, %rcx
	addq	$12, %r15
	cmpq	%rcx, %r13
	jl	.LBB11_3
.LBB11_6:                               # %._crit_edge
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	40(%rbp), %r14
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	%r14, %r15
	jne	.LBB11_8
# BB#7:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %r15
.LBB11_8:
	movl	$.L.str.9, %edi
	movl	$7, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	48(%rbp), %r8
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	movl	4(%rsp), %ecx           # 4-byte Reload
	callq	hypre_PrintBoxArrayData
	movq	%rbx, %rdi
	callq	fflush
	movq	%rbx, %rdi
	callq	fclose
	xorl	%eax, %eax
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_9:
	leaq	32(%rsp), %rsi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$1, %edi
	callq	exit
.Lfunc_end11:
	.size	hypre_StructMatrixPrint, .Lfunc_end11-hypre_StructMatrixPrint
	.cfi_endproc

	.globl	hypre_StructMatrixMigrate
	.p2align	4, 0x90
	.type	hypre_StructMatrixMigrate,@function
hypre_StructMatrixMigrate:              # @hypre_StructMatrixMigrate
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 24
	subq	$56, %rsp
.Lcfi97:
	.cfi_def_cfa_offset 80
.Lcfi98:
	.cfi_offset %rbx, -24
.Lcfi99:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 44(%rsp)
	movl	$1, 52(%rsp)
	movq	8(%rbx), %rdi
	movq	8(%r14), %rsi
	leaq	32(%rsp), %rdx
	leaq	24(%rsp), %rcx
	leaq	16(%rsp), %r8
	leaq	8(%rsp), %r9
	callq	hypre_CreateCommInfoFromGrids
	movq	32(%rsp), %rdi
	movq	24(%rsp), %rsi
	movq	40(%rbx), %r8
	movq	40(%r14), %r9
	movl	32(%rbx), %r10d
	movl	(%rbx), %r11d
	movq	8(%rbx), %rax
	addq	$56, %rax
	subq	$8, %rsp
.Lcfi100:
	.cfi_adjust_cfa_offset 8
	leaq	52(%rsp), %rdx
	movq	%rdx, %rcx
	pushq	%rax
.Lcfi101:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi102:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi103:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)
.Lcfi104:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)
.Lcfi105:
	.cfi_adjust_cfa_offset 8
	callq	hypre_CommPkgCreate
	addq	$48, %rsp
.Lcfi106:
	.cfi_adjust_cfa_offset -48
	movq	48(%rbx), %rsi
	movq	48(%r14), %rdx
	movq	%rsp, %rcx
	movq	%rax, %rdi
	callq	hypre_InitializeCommunication
	movq	(%rsp), %rdi
	callq	hypre_FinalizeCommunication
	xorl	%eax, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	hypre_StructMatrixMigrate, .Lfunc_end12-hypre_StructMatrixMigrate
	.cfi_endproc

	.globl	hypre_StructMatrixRead
	.p2align	4, 0x90
	.type	hypre_StructMatrixRead,@function
hypre_StructMatrixRead:                 # @hypre_StructMatrixRead
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi107:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi108:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi109:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi110:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi111:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi112:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi113:
	.cfi_def_cfa_offset 368
.Lcfi114:
	.cfi_offset %rbx, -56
.Lcfi115:
	.cfi_offset %r12, -48
.Lcfi116:
	.cfi_offset %r13, -40
.Lcfi117:
	.cfi_offset %r14, -32
.Lcfi118:
	.cfi_offset %r15, -24
.Lcfi119:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movl	%edi, %ebp
	leaq	36(%rsp), %rsi
	callq	hypre_MPI_Comm_rank
	movl	36(%rsp), %ecx
	leaq	48(%rsp), %r15
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	sprintf
	movl	$.L.str.10, %esi
	movq	%r15, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB13_6
# BB#1:
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	leaq	40(%rsp), %rdx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	leaq	16(%rsp), %rdx
	movl	%ebp, 32(%rsp)          # 4-byte Spill
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	hypre_StructGridRead
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movq	16(%rsp), %rax
	movl	4(%rax), %ebp
	leaq	12(%rsp), %rdx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movl	12(%rsp), %edi
	movl	$12, %esi
	callq	hypre_CAlloc
	movq	%rax, %r13
	movl	12(%rsp), %esi
	testl	%esi, %esi
	jle	.LBB13_5
# BB#2:                                 # %.lr.ph.preheader
	movl	%ebp, 28(%rsp)          # 4-byte Spill
	leaq	44(%rsp), %rbp
	movq	%r13, %r15
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB13_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	4(%r15), %r8
	leaq	8(%r15), %r9
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	movq	%r15, %rcx
	callq	fscanf
	incq	%r12
	movslq	12(%rsp), %rsi
	addq	$12, %r15
	cmpq	%rsi, %r12
	jl	.LBB13_3
# BB#4:
	movl	28(%rsp), %ebp          # 4-byte Reload
.LBB13_5:                               # %._crit_edge
	movl	%ebp, %edi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r13, %rdx
	callq	hypre_StructStencilCreate
	movq	%rax, %r15
	movq	16(%rsp), %r12
	movl	$1, %edi
	movl	$136, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbp
	movl	32(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rbp)
	leaq	8(%rbp), %rsi
	movq	%r12, %rdi
	callq	hypre_StructGridRef
	movq	%r15, %rdi
	callq	hypre_StructStencilRef
	movq	%rax, 16(%rbp)
	movl	$1, 56(%rbp)
	movl	$1, 128(%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 88(%rbp)
	movq	$0, 104(%rbp)
	movl	40(%rsp), %eax
	movl	%eax, 72(%rbp)
	movl	(%r14), %eax
	movl	%eax, 88(%rbp)
	movl	4(%r14), %eax
	movl	%eax, 92(%rbp)
	movl	8(%r14), %eax
	movl	%eax, 96(%rbp)
	movl	12(%r14), %eax
	movl	%eax, 100(%rbp)
	movl	16(%r14), %eax
	movl	%eax, 104(%rbp)
	movl	20(%r14), %eax
	movl	%eax, 108(%rbp)
	movq	%rbp, %rdi
	callq	hypre_StructMatrixInitializeShell
	movl	60(%rbp), %edi
	movl	$8, %esi
	callq	hypre_CAlloc
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	hypre_StructMatrixInitializeData
	movl	$1, 56(%rbp)
	movq	16(%rsp), %rax
	movq	8(%rax), %r14
	movq	40(%rbp), %r15
	movl	32(%rbp), %r12d
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movq	48(%rbp), %r8
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	movl	%r12d, %ecx
	callq	hypre_ReadBoxArrayData
	movq	%rbp, %rdi
	callq	hypre_StructMatrixAssemble
	movq	%rbx, %rdi
	callq	fclose
	movq	%rbp, %rax
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_6:
	leaq	48(%rsp), %rsi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$1, %edi
	callq	exit
.Lfunc_end13:
	.size	hypre_StructMatrixRead, .Lfunc_end13-hypre_StructMatrixRead
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s.%05d"
	.size	.L.str, 8

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"w"
	.size	.L.str.1, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Error: can't open output file %s\n"
	.size	.L.str.2, 34

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"StructMatrix\n"
	.size	.L.str.3, 14

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\nSymmetric: %d\n"
	.size	.L.str.4, 16

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\nGrid:\n"
	.size	.L.str.5, 8

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\nStencil:\n"
	.size	.L.str.6, 11

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%d\n"
	.size	.L.str.7, 4

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%d: %d %d %d\n"
	.size	.L.str.8, 14

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"\nData:\n"
	.size	.L.str.9, 8

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"r"
	.size	.L.str.10, 2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
