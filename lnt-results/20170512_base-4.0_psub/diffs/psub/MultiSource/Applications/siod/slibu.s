	.text
	.file	"slibu.bc"
	.globl	lsystem
	.p2align	4, 0x90
	.type	lsystem,@function
lsystem:                                # @lsystem
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	string_append
	movq	%rax, %rdi
	callq	get_c_string
	movq	%rax, %rdi
	callq	system
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	no_interrupt
	cvtsi2sdl	%ebp, %xmm0
	callq	flocons
	movq	%rax, %rbx
	testl	%ebp, %ebp
	js	.LBB0_2
# BB#1:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB0_2:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movq	%rbx, %rdi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	cons                    # TAILCALL
.Lfunc_end0:
	.size	lsystem, .Lfunc_end0-lsystem
	.cfi_endproc

	.globl	lgetuid
	.p2align	4, 0x90
	.type	lgetuid,@function
lgetuid:                                # @lgetuid
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 16
	callq	getuid
	movl	%eax, %eax
	cvtsi2sdq	%rax, %xmm0
	popq	%rax
	jmp	flocons                 # TAILCALL
.Lfunc_end1:
	.size	lgetuid, .Lfunc_end1-lgetuid
	.cfi_endproc

	.globl	lgetgid
	.p2align	4, 0x90
	.type	lgetgid,@function
lgetgid:                                # @lgetgid
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 16
	callq	getgid
	movl	%eax, %eax
	cvtsi2sdq	%rax, %xmm0
	popq	%rax
	jmp	flocons                 # TAILCALL
.Lfunc_end2:
	.size	lgetgid, .Lfunc_end2-lgetgid
	.cfi_endproc

	.globl	lgetcwd
	.p2align	4, 0x90
	.type	lgetcwd,@function
lgetcwd:                                # @lgetcwd
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 16
	subq	$4112, %rsp             # imm = 0x1010
.Lcfi9:
	.cfi_def_cfa_offset 4128
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	%rsp, %rdi
	movl	$4097, %esi             # imm = 0x1001
	callq	getcwd
	testq	%rax, %rax
	je	.LBB3_2
# BB#1:
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcons
	jmp	.LBB3_3
.LBB3_2:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.1, %edi
	movq	%rax, %rsi
	callq	err
.LBB3_3:
	addq	$4112, %rsp             # imm = 0x1010
	popq	%rbx
	retq
.Lfunc_end3:
	.size	lgetcwd, .Lfunc_end3-lgetcwd
	.cfi_endproc

	.globl	ldecode_pwent
	.p2align	4, 0x90
	.type	ldecode_pwent,@function
ldecode_pwent:                          # @ldecode_pwent
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 80
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	strcons
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rbx), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	strcons
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	16(%rbx), %eax
	cvtsi2sdq	%rax, %xmm0
	callq	flocons
	movq	%rax, %r13
	movl	20(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	callq	flocons
	movq	%rax, %r12
	movq	32(%rbx), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	strcons
	movq	%rax, %r15
	movq	24(%rbx), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	strcons
	movq	%rax, %r14
	movq	40(%rbx), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcons
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.2, %edi
	movl	$.L.str.3, %edx
	movl	$.L.str.4, %r8d
	movl	$0, %eax
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r13, %r9
	pushq	$0
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.8
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.7
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.6
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.5
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	callq	symalist
	addq	$104, %rsp
.Lcfi34:
	.cfi_adjust_cfa_offset -80
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	ldecode_pwent, .Lfunc_end4-ldecode_pwent
	.cfi_endproc

	.globl	symalist
	.p2align	4, 0x90
	.type	symalist,@function
symalist:                               # @symalist
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 32
	subq	$208, %rsp
.Lcfi38:
	.cfi_def_cfa_offset 240
.Lcfi39:
	.cfi_offset %rbx, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %r15, -16
	testb	%al, %al
	je	.LBB5_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB5_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rsi, 40(%rsp)
	testq	%rdi, %rdi
	je	.LBB5_3
# BB#5:
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	240(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$8, (%rsp)
	movslq	(%rsp), %rcx
	cmpq	$40, %rcx
	ja	.LBB5_7
# BB#6:
	movq	%rcx, %rax
	addq	16(%rsp), %rax
	leal	8(%rcx), %ecx
	movl	%ecx, (%rsp)
	jmp	.LBB5_8
.LBB5_3:
	xorl	%r14d, %r14d
	jmp	.LBB5_4
.LBB5_7:
	movq	8(%rsp), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, 8(%rsp)
.LBB5_8:
	movq	(%rax), %rbx
	callq	cintern
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	cons
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	cons
	movq	%rax, %r14
	movq	%r14, %r15
	jmp	.LBB5_9
	.p2align	4, 0x90
.LBB5_16:                               #   in Loop: Header=BB5_9 Depth=1
	movq	(%rcx), %rbx
	callq	cintern
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	cons
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	cons
	movq	%rax, 16(%r15)
	movq	%rax, %r15
.LBB5_9:                                # =>This Inner Loop Header: Depth=1
	movslq	(%rsp), %rax
	cmpq	$40, %rax
	ja	.LBB5_11
# BB#10:                                #   in Loop: Header=BB5_9 Depth=1
	movq	%rax, %rcx
	addq	16(%rsp), %rcx
	leal	8(%rax), %eax
	movl	%eax, (%rsp)
	jmp	.LBB5_12
	.p2align	4, 0x90
.LBB5_11:                               #   in Loop: Header=BB5_9 Depth=1
	movq	8(%rsp), %rcx
	leaq	8(%rcx), %rdx
	movq	%rdx, 8(%rsp)
.LBB5_12:                               #   in Loop: Header=BB5_9 Depth=1
	movq	(%rcx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_4
# BB#13:                                #   in Loop: Header=BB5_9 Depth=1
	cmpl	$40, %eax
	ja	.LBB5_15
# BB#14:                                #   in Loop: Header=BB5_9 Depth=1
	movslq	%eax, %rcx
	addq	16(%rsp), %rcx
	addl	$8, %eax
	movl	%eax, (%rsp)
	jmp	.LBB5_16
	.p2align	4, 0x90
.LBB5_15:                               #   in Loop: Header=BB5_9 Depth=1
	movq	8(%rsp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 8(%rsp)
	jmp	.LBB5_16
.LBB5_4:
	movq	%r14, %rax
	addq	$208, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	symalist, .Lfunc_end5-symalist
	.cfi_endproc

	.globl	lencode_pwent
	.p2align	4, 0x90
	.type	lencode_pwent,@function
lencode_pwent:                          # @lencode_pwent
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 32
.Lcfi45:
	.cfi_offset %rbx, -32
.Lcfi46:
	.cfi_offset %r14, -24
.Lcfi47:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$.L.str.2, %edi
	callq	rintern
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	assq
	testq	%rax, %rax
	je	.LBB6_1
# BB#2:
	movq	%rax, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	get_c_string
	jmp	.LBB6_3
.LBB6_1:
	movl	$.L.str.114, %eax
.LBB6_3:                                # %strfield.exit
	movq	%rax, (%r14)
	movl	$.L.str.3, %edi
	callq	rintern
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	assq
	testq	%rax, %rax
	je	.LBB6_4
# BB#5:
	movq	%rax, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	get_c_string
	jmp	.LBB6_6
.LBB6_4:
	movl	$.L.str.114, %eax
.LBB6_6:                                # %strfield.exit15
	movq	%rax, 8(%r14)
	movl	$.L.str.4, %edi
	callq	rintern
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	assq
	xorl	%ebx, %ebx
	testq	%rax, %rax
	movl	$0, %ecx
	je	.LBB6_8
# BB#7:
	movq	%rax, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	get_c_long
	movq	%rax, %rcx
.LBB6_8:                                # %longfield.exit
	movl	%ecx, 16(%r14)
	movl	$.L.str.5, %edi
	callq	rintern
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	assq
	testq	%rax, %rax
	je	.LBB6_10
# BB#9:
	movq	%rax, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	get_c_long
	movq	%rax, %rbx
.LBB6_10:                               # %longfield.exit18
	movl	%ebx, 20(%r14)
	movl	$.L.str.6, %edi
	callq	rintern
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	assq
	testq	%rax, %rax
	je	.LBB6_11
# BB#12:
	movq	%rax, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	get_c_string
	jmp	.LBB6_13
.LBB6_11:
	movl	$.L.str.114, %eax
.LBB6_13:                               # %strfield.exit20
	movq	%rax, 32(%r14)
	movl	$.L.str.7, %edi
	callq	rintern
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	assq
	testq	%rax, %rax
	je	.LBB6_14
# BB#15:
	movq	%rax, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	get_c_string
	jmp	.LBB6_16
.LBB6_14:
	movl	$.L.str.114, %eax
.LBB6_16:                               # %strfield.exit22
	movq	%rax, 24(%r14)
	movl	$.L.str.8, %edi
	callq	rintern
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	assq
	testq	%rax, %rax
	je	.LBB6_17
# BB#18:
	movq	%rax, %rdi
	callq	cdr
	movq	%rax, %rdi
	callq	get_c_string
	jmp	.LBB6_19
.LBB6_17:
	movl	$.L.str.114, %eax
.LBB6_19:                               # %strfield.exit24
	movq	%rax, 40(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	lencode_pwent, .Lfunc_end6-lencode_pwent
	.cfi_endproc

	.globl	lgetpwuid
	.p2align	4, 0x90
	.type	lgetpwuid,@function
lgetpwuid:                              # @lgetpwuid
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 32
.Lcfi51:
	.cfi_offset %rbx, -24
.Lcfi52:
	.cfi_offset %r14, -16
	callq	get_c_long
	movq	%rax, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	movl	%ebx, %edi
	callq	getpwuid
	testq	%rax, %rax
	je	.LBB7_1
# BB#2:
	movq	%rax, %rdi
	callq	ldecode_pwent
	movq	%rax, %rbx
	jmp	.LBB7_3
.LBB7_1:
	xorl	%ebx, %ebx
.LBB7_3:
	movslq	%r14d, %rdi
	callq	no_interrupt
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	lgetpwuid, .Lfunc_end7-lgetpwuid
	.cfi_endproc

	.globl	lgetpwnam
	.p2align	4, 0x90
	.type	lgetpwnam,@function
lgetpwnam:                              # @lgetpwnam
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 32
.Lcfi56:
	.cfi_offset %rbx, -24
.Lcfi57:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%rax, %rdi
	callq	getpwnam
	testq	%rax, %rax
	je	.LBB8_1
# BB#2:
	movq	%rax, %rdi
	callq	ldecode_pwent
	movq	%rax, %rbx
	jmp	.LBB8_3
.LBB8_1:
	xorl	%ebx, %ebx
.LBB8_3:
	movslq	%r14d, %rdi
	callq	no_interrupt
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	lgetpwnam, .Lfunc_end8-lgetpwnam
	.cfi_endproc

	.globl	lgetpwent
	.p2align	4, 0x90
	.type	lgetpwent,@function
lgetpwent:                              # @lgetpwent
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 32
.Lcfi61:
	.cfi_offset %rbx, -24
.Lcfi62:
	.cfi_offset %r14, -16
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	callq	getpwent
	testq	%rax, %rax
	je	.LBB9_1
# BB#2:
	movq	%rax, %rdi
	callq	ldecode_pwent
	movq	%rax, %rbx
	jmp	.LBB9_3
.LBB9_1:
	xorl	%ebx, %ebx
.LBB9_3:
	movslq	%r14d, %rdi
	callq	no_interrupt
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	lgetpwent, .Lfunc_end9-lgetpwent
	.cfi_endproc

	.globl	lsetpwent
	.p2align	4, 0x90
	.type	lsetpwent,@function
lsetpwent:                              # @lsetpwent
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 16
.Lcfi64:
	.cfi_offset %rbx, -16
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %rbx
	callq	setpwent
	movslq	%ebx, %rdi
	callq	no_interrupt
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end10:
	.size	lsetpwent, .Lfunc_end10-lsetpwent
	.cfi_endproc

	.globl	lendpwent
	.p2align	4, 0x90
	.type	lendpwent,@function
lendpwent:                              # @lendpwent
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 16
.Lcfi66:
	.cfi_offset %rbx, -16
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %rbx
	callq	endpwent
	movslq	%ebx, %rdi
	callq	no_interrupt
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end11:
	.size	lendpwent, .Lfunc_end11-lendpwent
	.cfi_endproc

	.globl	lsetuid
	.p2align	4, 0x90
	.type	lsetuid,@function
lsetuid:                                # @lsetuid
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi67:
	.cfi_def_cfa_offset 16
	callq	get_c_long
	movl	%eax, %edi
	callq	setuid
	testl	%eax, %eax
	je	.LBB12_1
# BB#2:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.9, %edi
	movq	%rax, %rsi
	popq	%rax
	jmp	err                     # TAILCALL
.LBB12_1:
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end12:
	.size	lsetuid, .Lfunc_end12-lsetuid
	.cfi_endproc

	.globl	lseteuid
	.p2align	4, 0x90
	.type	lseteuid,@function
lseteuid:                               # @lseteuid
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi68:
	.cfi_def_cfa_offset 16
	callq	get_c_long
	movl	%eax, %edi
	callq	seteuid
	testl	%eax, %eax
	je	.LBB13_1
# BB#2:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.10, %edi
	movq	%rax, %rsi
	popq	%rax
	jmp	err                     # TAILCALL
.LBB13_1:
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end13:
	.size	lseteuid, .Lfunc_end13-lseteuid
	.cfi_endproc

	.globl	lgeteuid
	.p2align	4, 0x90
	.type	lgeteuid,@function
lgeteuid:                               # @lgeteuid
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi69:
	.cfi_def_cfa_offset 16
	callq	geteuid
	movl	%eax, %eax
	cvtsi2sdq	%rax, %xmm0
	popq	%rax
	jmp	flocons                 # TAILCALL
.Lfunc_end14:
	.size	lgeteuid, .Lfunc_end14-lgeteuid
	.cfi_endproc

	.globl	laccess_problem
	.p2align	4, 0x90
	.type	laccess_problem,@function
laccess_problem:                        # @laccess_problem
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 48
.Lcfi75:
	.cfi_offset %rbx, -48
.Lcfi76:
	.cfi_offset %r12, -40
.Lcfi77:
	.cfi_offset %r14, -32
.Lcfi78:
	.cfi_offset %r15, -24
.Lcfi79:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	callq	get_c_string
	movq	%rax, %r14
	movq	%r12, %rdi
	callq	get_c_string
	movq	%rax, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r15
	movb	(%rbx), %al
	testb	%al, %al
	je	.LBB15_1
# BB#2:                                 # %.lr.ph.preheader
	incq	%rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB15_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	%al, %eax
	addl	$-102, %eax
	cmpl	$18, %eax
	ja	.LBB15_8
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB15_3 Depth=1
	jmpq	*.LJTI15_0(,%rax,8)
.LBB15_5:                               #   in Loop: Header=BB15_3 Depth=1
	orl	$4, %ebp
	jmp	.LBB15_9
	.p2align	4, 0x90
.LBB15_8:                               #   in Loop: Header=BB15_3 Depth=1
	movl	$.L.str.11, %edi
	movq	%r12, %rsi
	callq	err
	jmp	.LBB15_9
	.p2align	4, 0x90
.LBB15_6:                               #   in Loop: Header=BB15_3 Depth=1
	orl	$2, %ebp
	jmp	.LBB15_9
	.p2align	4, 0x90
.LBB15_7:                               #   in Loop: Header=BB15_3 Depth=1
	orl	$1, %ebp
	.p2align	4, 0x90
.LBB15_9:                               #   in Loop: Header=BB15_3 Depth=1
	movzbl	(%rbx), %eax
	incq	%rbx
	testb	%al, %al
	jne	.LBB15_3
	jmp	.LBB15_10
.LBB15_1:
	xorl	%ebp, %ebp
.LBB15_10:                              # %._crit_edge
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	access
	movl	%eax, %ebx
	movslq	%r15d, %rdi
	callq	no_interrupt
	testl	%ebx, %ebx
	js	.LBB15_12
# BB#11:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_12:
	movl	$-1, %edi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	llast_c_errmsg          # TAILCALL
.Lfunc_end15:
	.size	laccess_problem, .Lfunc_end15-laccess_problem
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI15_0:
	.quad	.LBB15_9
	.quad	.LBB15_8
	.quad	.LBB15_8
	.quad	.LBB15_8
	.quad	.LBB15_8
	.quad	.LBB15_8
	.quad	.LBB15_8
	.quad	.LBB15_8
	.quad	.LBB15_8
	.quad	.LBB15_8
	.quad	.LBB15_8
	.quad	.LBB15_8
	.quad	.LBB15_5
	.quad	.LBB15_8
	.quad	.LBB15_8
	.quad	.LBB15_8
	.quad	.LBB15_8
	.quad	.LBB15_6
	.quad	.LBB15_7

	.text
	.globl	lsymlink
	.p2align	4, 0x90
	.type	lsymlink,@function
lsymlink:                               # @lsymlink
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 32
.Lcfi83:
	.cfi_offset %rbx, -32
.Lcfi84:
	.cfi_offset %r14, -24
.Lcfi85:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	get_c_string
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	symlink
	testl	%eax, %eax
	je	.LBB16_1
# BB#2:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.12, %edi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	err                     # TAILCALL
.LBB16_1:
	movq	%r14, %rdi
	callq	no_interrupt
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end16:
	.size	lsymlink, .Lfunc_end16-lsymlink
	.cfi_endproc

	.globl	llink
	.p2align	4, 0x90
	.type	llink,@function
llink:                                  # @llink
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 32
.Lcfi89:
	.cfi_offset %rbx, -32
.Lcfi90:
	.cfi_offset %r14, -24
.Lcfi91:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	get_c_string
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	link
	testl	%eax, %eax
	je	.LBB17_1
# BB#2:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.13, %edi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	err                     # TAILCALL
.LBB17_1:
	movq	%r14, %rdi
	callq	no_interrupt
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	llink, .Lfunc_end17-llink
	.cfi_endproc

	.globl	lunlink
	.p2align	4, 0x90
	.type	lunlink,@function
lunlink:                                # @lunlink
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi94:
	.cfi_def_cfa_offset 32
.Lcfi95:
	.cfi_offset %rbx, -24
.Lcfi96:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%rax, %rdi
	callq	unlink
	testl	%eax, %eax
	je	.LBB18_1
# BB#2:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.14, %edi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	err                     # TAILCALL
.LBB18_1:
	movq	%r14, %rdi
	callq	no_interrupt
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end18:
	.size	lunlink, .Lfunc_end18-lunlink
	.cfi_endproc

	.globl	lrmdir
	.p2align	4, 0x90
	.type	lrmdir,@function
lrmdir:                                 # @lrmdir
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi99:
	.cfi_def_cfa_offset 32
.Lcfi100:
	.cfi_offset %rbx, -24
.Lcfi101:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%rax, %rdi
	callq	rmdir
	testl	%eax, %eax
	je	.LBB19_1
# BB#2:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.15, %edi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	err                     # TAILCALL
.LBB19_1:
	movq	%r14, %rdi
	callq	no_interrupt
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end19:
	.size	lrmdir, .Lfunc_end19-lrmdir
	.cfi_endproc

	.globl	lmkdir
	.p2align	4, 0x90
	.type	lmkdir,@function
lmkdir:                                 # @lmkdir
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 32
.Lcfi105:
	.cfi_offset %rbx, -32
.Lcfi106:
	.cfi_offset %r14, -24
.Lcfi107:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	get_c_long
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB20_1
# BB#2:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.16, %edi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	err                     # TAILCALL
.LBB20_1:
	movq	%r14, %rdi
	callq	no_interrupt
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end20:
	.size	lmkdir, .Lfunc_end20-lmkdir
	.cfi_endproc

	.globl	lreadlink
	.p2align	4, 0x90
	.type	lreadlink,@function
lreadlink:                              # @lreadlink
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 24
	subq	$4104, %rsp             # imm = 0x1008
.Lcfi110:
	.cfi_def_cfa_offset 4128
.Lcfi111:
	.cfi_offset %rbx, -24
.Lcfi112:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%rsp, %rsi
	movl	$4097, %edx             # imm = 0x1001
	movq	%rax, %rdi
	callq	readlink
	movq	%rax, %rbx
	testl	%ebx, %ebx
	js	.LBB21_1
# BB#2:
	movq	%r14, %rdi
	callq	no_interrupt
	movslq	%ebx, %rdi
	movq	%rsp, %rsi
	callq	strcons
	jmp	.LBB21_3
.LBB21_1:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.17, %edi
	movq	%rax, %rsi
	callq	err
.LBB21_3:
	addq	$4104, %rsp             # imm = 0x1008
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end21:
	.size	lreadlink, .Lfunc_end21-lreadlink
	.cfi_endproc

	.globl	lrename
	.p2align	4, 0x90
	.type	lrename,@function
lrename:                                # @lrename
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi113:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi114:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 32
.Lcfi116:
	.cfi_offset %rbx, -32
.Lcfi117:
	.cfi_offset %r14, -24
.Lcfi118:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	get_c_string
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	rename
	testl	%eax, %eax
	je	.LBB22_1
# BB#2:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.18, %edi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	err                     # TAILCALL
.LBB22_1:
	movq	%r14, %rdi
	callq	no_interrupt
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end22:
	.size	lrename, .Lfunc_end22-lrename
	.cfi_endproc

	.globl	lrandom
	.p2align	4, 0x90
	.type	lrandom,@function
lrandom:                                # @lrandom
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi119:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi121:
	.cfi_def_cfa_offset 32
.Lcfi122:
	.cfi_offset %rbx, -24
.Lcfi123:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	callq	random
	testq	%r14, %r14
	movslq	%eax, %rbx
	je	.LBB23_2
# BB#1:
	movq	%r14, %rdi
	callq	get_c_long
	movq	%rax, %rcx
	movq	%rbx, %rax
	cqto
	idivq	%rcx
	movq	%rdx, %rbx
.LBB23_2:
	cvtsi2sdq	%rbx, %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	flocons                 # TAILCALL
.Lfunc_end23:
	.size	lrandom, .Lfunc_end23-lrandom
	.cfi_endproc

	.globl	lsrandom
	.p2align	4, 0x90
	.type	lsrandom,@function
lsrandom:                               # @lsrandom
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi124:
	.cfi_def_cfa_offset 16
	callq	get_c_long
	movl	%eax, %edi
	callq	srandom
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end24:
	.size	lsrandom, .Lfunc_end24-lsrandom
	.cfi_endproc

	.globl	lfork
	.p2align	4, 0x90
	.type	lfork,@function
lfork:                                  # @lfork
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi125:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi127:
	.cfi_def_cfa_offset 32
.Lcfi128:
	.cfi_offset %rbx, -24
.Lcfi129:
	.cfi_offset %rbp, -16
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %rbx
	callq	fork
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB25_3
# BB#1:
	cmpl	$-1, %ebp
	jne	.LBB25_2
# BB#4:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.19, %edi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	err                     # TAILCALL
.LBB25_3:
	movslq	%ebx, %rdi
	callq	no_interrupt
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB25_2:
	movslq	%ebx, %rdi
	callq	no_interrupt
	cvtsi2sdl	%ebp, %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	flocons                 # TAILCALL
.Lfunc_end25:
	.size	lfork, .Lfunc_end25-lfork
	.cfi_endproc

	.globl	list2char
	.p2align	4, 0x90
	.type	list2char,@function
list2char:                              # @list2char
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi133:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi134:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi136:
	.cfi_def_cfa_offset 80
.Lcfi137:
	.cfi_offset %rbx, -56
.Lcfi138:
	.cfi_offset %r12, -48
.Lcfi139:
	.cfi_offset %r13, -40
.Lcfi140:
	.cfi_offset %r14, -32
.Lcfi141:
	.cfi_offset %r15, -24
.Lcfi142:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	movq	%r12, %rdi
	callq	llength
	movq	%rax, %rdi
	callq	get_c_long
	movq	%rax, %rbx
	leaq	8(,%rbx,8), %rsi
	leaq	8(%rsp), %rdi
	callq	mallocl
	movq	(%r14), %rsi
	movq	%rax, %rdi
	callq	cons
	movq	%rax, (%r14)
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	testq	%rbx, %rbx
	jle	.LBB26_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	movq	16(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB26_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	car
	movq	%rax, %rdi
	callq	get_c_string
	movq	%rax, %r13
	movq	8(%rsp), %rbx
	addq	%rbp, %rbx
	movq	%r13, %rdi
	callq	strlen
	leaq	1(%rax), %rsi
	movq	%rbx, %rdi
	callq	mallocl
	movq	(%r14), %rsi
	movq	%rax, %rdi
	callq	cons
	movq	%rax, (%r14)
	movq	8(%rsp), %rax
	movq	(%rax,%rbp), %rdi
	movq	%r13, %rsi
	callq	strcpy
	movq	%r12, %rdi
	callq	cdr
	movq	%rax, %r12
	addq	$8, %rbp
	decq	%r15
	jne	.LBB26_2
.LBB26_3:                               # %._crit_edge
	movq	8(%rsp), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	$0, (%rax,%rcx,8)
	movq	8(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	list2char, .Lfunc_end26-list2char
	.cfi_endproc

	.globl	lexec
	.p2align	4, 0x90
	.type	lexec,@function
lexec:                                  # @lexec
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi143:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi144:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi145:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi146:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi148:
	.cfi_def_cfa_offset 64
.Lcfi149:
	.cfi_offset %rbx, -48
.Lcfi150:
	.cfi_offset %r12, -40
.Lcfi151:
	.cfi_offset %r13, -32
.Lcfi152:
	.cfi_offset %r14, -24
.Lcfi153:
	.cfi_offset %r15, -16
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	$0, 8(%rsp)
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	leaq	8(%rsp), %rdi
	movq	%rbx, %rsi
	callq	list2char
	movq	%rax, %r15
	testq	%r13, %r13
	je	.LBB27_1
# BB#3:
	leaq	8(%rsp), %rdi
	movq	%r13, %rsi
	callq	list2char
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	get_c_string
	movq	%rax, %rdi
	testq	%rbx, %rbx
	je	.LBB27_2
# BB#4:
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	execve
	jmp	.LBB27_5
.LBB27_1:                               # %.thread
	movq	%r12, %rdi
	callq	get_c_string
	movq	%rax, %rdi
.LBB27_2:
	movq	%r15, %rsi
	callq	execv
.LBB27_5:
	movslq	%r14d, %rdi
	callq	no_interrupt
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.20, %edi
	movq	%rax, %rsi
	callq	err
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end27:
	.size	lexec, .Lfunc_end27-lexec
	.cfi_endproc

	.globl	lnice
	.p2align	4, 0x90
	.type	lnice,@function
lnice:                                  # @lnice
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi154:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi155:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi156:
	.cfi_def_cfa_offset 32
.Lcfi157:
	.cfi_offset %rbx, -24
.Lcfi158:
	.cfi_offset %rbp, -16
	callq	get_c_long
	movq	%rax, %rbp
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %rbx
	movl	%ebp, %edi
	callq	nice
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB28_2
# BB#1:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.21, %edi
	movq	%rax, %rsi
	callq	err
.LBB28_2:
	movslq	%ebx, %rdi
	callq	no_interrupt
	cvtsi2sdl	%ebp, %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	flocons                 # TAILCALL
.Lfunc_end28:
	.size	lnice, .Lfunc_end28-lnice
	.cfi_endproc

	.globl	assemble_options
	.p2align	4, 0x90
	.type	assemble_options,@function
assemble_options:                       # @assemble_options
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi161:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi162:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi163:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi164:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi165:
	.cfi_def_cfa_offset 288
.Lcfi166:
	.cfi_offset %rbx, -56
.Lcfi167:
	.cfi_offset %r12, -48
.Lcfi168:
	.cfi_offset %r13, -40
.Lcfi169:
	.cfi_offset %r14, -32
.Lcfi170:
	.cfi_offset %r15, -24
.Lcfi171:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	testb	%al, %al
	je	.LBB29_2
# BB#1:
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm1, 112(%rsp)
	movaps	%xmm2, 128(%rsp)
	movaps	%xmm3, 144(%rsp)
	movaps	%xmm4, 160(%rsp)
	movaps	%xmm5, 176(%rsp)
	movaps	%xmm6, 192(%rsp)
	movaps	%xmm7, 208(%rsp)
.LBB29_2:
	movq	%r9, 88(%rsp)
	movq	%r8, 80(%rsp)
	movq	%rcx, 72(%rsp)
	movq	%rdx, 64(%rsp)
	movq	%rsi, 56(%rsp)
	testq	%rbx, %rbx
	je	.LBB29_3
# BB#4:
	movl	$-1, %r13d
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB29_6
# BB#5:
	movq	%rbx, %rdi
	callq	llength
	movq	%rax, %rdi
	callq	get_c_long
	movq	%rax, %r13
.LBB29_6:
	leaq	48(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	288(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$8, (%rsp)
	testl	%r13d, %r13d
	setg	%r12b
	jle	.LBB29_7
# BB#23:                                # %.outer42.us.preheader
	movl	%r12d, 28(%rsp)         # 4-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movl	%r13d, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.LBB29_21
.LBB29_3:
	xorl	%r14d, %r14d
	jmp	.LBB29_31
.LBB29_20:                              # %.loopexit.us
                                        #   in Loop: Header=BB29_21 Depth=1
	orl	%ebp, %r14d
	movq	%r12, %rdi
	callq	llength
	movq	%rax, %rdi
	callq	get_c_long
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%eax, %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	%eax, %r15d
	movq	%r12, %r13
	jmp	.LBB29_21
	.p2align	4, 0x90
.LBB29_10:                              #   in Loop: Header=BB29_21 Depth=1
	movq	%rax, %rcx
	addq	16(%rsp), %rcx
	leal	8(%rax), %eax
	movl	%eax, (%rsp)
.LBB29_11:                              #   in Loop: Header=BB29_21 Depth=1
	movq	(%rcx), %rdi
	testq	%rdi, %rdi
	je	.LBB29_12
# BB#13:                                #   in Loop: Header=BB29_21 Depth=1
	cmpl	$41, %eax
	jae	.LBB29_14
# BB#15:                                #   in Loop: Header=BB29_21 Depth=1
	movslq	%eax, %rcx
	addq	16(%rsp), %rcx
	addl	$8, %eax
	movl	%eax, (%rsp)
	jmp	.LBB29_16
	.p2align	4, 0x90
.LBB29_14:                              #   in Loop: Header=BB29_21 Depth=1
	movq	8(%rsp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 8(%rsp)
.LBB29_16:                              #   in Loop: Header=BB29_21 Depth=1
	movl	(%rcx), %ebp
	callq	cintern
	cmpq	%rbx, %rax
	je	.LBB29_17
# BB#18:                                #   in Loop: Header=BB29_21 Depth=1
	movzwl	2(%rbx), %ecx
	cmpl	$1, %ecx
	je	.LBB29_19
.LBB29_21:                              # =>This Inner Loop Header: Depth=1
	movslq	(%rsp), %rax
	cmpq	$41, %rax
	jb	.LBB29_10
# BB#22:                                #   in Loop: Header=BB29_21 Depth=1
	movq	8(%rsp), %rcx
	leaq	8(%rcx), %rdx
	movq	%rdx, 8(%rsp)
	jmp	.LBB29_11
.LBB29_17:                              #   in Loop: Header=BB29_21 Depth=1
	movq	%r13, %r12
	jmp	.LBB29_20
.LBB29_19:                              #   in Loop: Header=BB29_21 Depth=1
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	memq
	movq	%rax, %r12
	testq	%r12, %r12
	movl	$0, %r13d
	je	.LBB29_21
	jmp	.LBB29_20
.LBB29_12:
	movl	28(%rsp), %r12d         # 4-byte Reload
	movq	40(%rsp), %r13          # 8-byte Reload
	cmpl	$-1, %r13d
	jne	.LBB29_28
	jmp	.LBB29_30
.LBB29_7:                               # %.outer46.preheader.preheader
	xorl	%r14d, %r14d
	jmp	.LBB29_8
.LBB29_38:                              # %.outer.loopexit.us-lcssa
                                        #   in Loop: Header=BB29_8 Depth=1
	orl	%ebp, %r14d
	movl	$-2, %r13d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB29_8:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rsp), %rax
	cmpq	$40, %rax
	ja	.LBB29_24
# BB#9:                                 #   in Loop: Header=BB29_8 Depth=1
	movq	%rax, %rcx
	addq	16(%rsp), %rcx
	leal	8(%rax), %eax
	movl	%eax, (%rsp)
	jmp	.LBB29_25
	.p2align	4, 0x90
.LBB29_24:                              #   in Loop: Header=BB29_8 Depth=1
	movq	8(%rsp), %rcx
	leaq	8(%rcx), %rdx
	movq	%rdx, 8(%rsp)
.LBB29_25:                              #   in Loop: Header=BB29_8 Depth=1
	movq	(%rcx), %rdi
	testq	%rdi, %rdi
	je	.LBB29_26
# BB#32:                                #   in Loop: Header=BB29_8 Depth=1
	cmpl	$40, %eax
	ja	.LBB29_34
# BB#33:                                #   in Loop: Header=BB29_8 Depth=1
	movslq	%eax, %rcx
	addq	16(%rsp), %rcx
	addl	$8, %eax
	movl	%eax, (%rsp)
	jmp	.LBB29_35
	.p2align	4, 0x90
.LBB29_34:                              #   in Loop: Header=BB29_8 Depth=1
	movq	8(%rsp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 8(%rsp)
.LBB29_35:                              #   in Loop: Header=BB29_8 Depth=1
	movl	(%rcx), %ebp
	callq	cintern
	cmpq	%rbx, %rax
	je	.LBB29_38
# BB#36:                                #   in Loop: Header=BB29_8 Depth=1
	movzwl	2(%rbx), %ecx
	cmpl	$1, %ecx
	jne	.LBB29_8
# BB#37:                                #   in Loop: Header=BB29_8 Depth=1
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	memq
	testq	%rax, %rax
	je	.LBB29_8
	jmp	.LBB29_38
.LBB29_26:
	xorl	%r15d, %r15d
	cmpl	$-1, %r13d
	je	.LBB29_30
.LBB29_28:
	testb	$1, %r12b
	je	.LBB29_31
# BB#29:
	movl	$1, %eax
	movl	%r13d, %ecx
	shll	%cl, %eax
	decl	%eax
	cmpl	%eax, %r15d
	je	.LBB29_31
.LBB29_30:
	movl	$.L.str.22, %edi
	movq	%rbx, %rsi
	callq	err
.LBB29_31:
	movl	%r14d, %eax
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end29:
	.size	assemble_options, .Lfunc_end29-assemble_options
	.cfi_endproc

	.globl	lwait
	.p2align	4, 0x90
	.type	lwait,@function
lwait:                                  # @lwait
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi172:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi173:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi174:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi175:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi176:
	.cfi_def_cfa_offset 48
.Lcfi177:
	.cfi_offset %rbx, -40
.Lcfi178:
	.cfi_offset %r12, -32
.Lcfi179:
	.cfi_offset %r14, -24
.Lcfi180:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movl	$0, 4(%rsp)
	testq	%rdi, %rdi
	je	.LBB30_1
# BB#2:
	callq	get_c_long
	movq	%rax, %r14
	jmp	.LBB30_3
.LBB30_1:
	movl	$-1, %r14d
.LBB30_3:
	xorl	%r15d, %r15d
	movl	$.L.str.23, %esi
	movl	$8, %edx
	movl	$.L.str.24, %ecx
	movl	$16777216, %r8d         # imm = 0x1000000
	movl	$.L.str.25, %r9d
	movl	$0, %eax
	movq	%rbx, %rdi
	pushq	$0
.Lcfi181:
	.cfi_adjust_cfa_offset 8
	pushq	$2
.Lcfi182:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.26
.Lcfi183:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi184:
	.cfi_adjust_cfa_offset 8
	callq	assemble_options
	addq	$32, %rsp
.Lcfi185:
	.cfi_adjust_cfa_offset -32
	movl	%eax, %ebx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r12
	leaq	4(%rsp), %rsi
	movl	%r14d, %edi
	movl	%ebx, %edx
	callq	waitpid
	movl	%eax, %ebx
	movslq	%r12d, %rdi
	callq	no_interrupt
	testl	%ebx, %ebx
	je	.LBB30_8
# BB#4:
	cmpl	$-1, %ebx
	jne	.LBB30_6
# BB#5:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.27, %edi
	movq	%rax, %rsi
	callq	err
	jmp	.LBB30_7
.LBB30_6:
	cvtsi2sdl	%ebx, %xmm0
	callq	flocons
	movq	%rax, %rbx
	movl	4(%rsp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	callq	flocons
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	cons
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	cons
.LBB30_7:
	movq	%rax, %r15
.LBB30_8:
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end30:
	.size	lwait, .Lfunc_end30-lwait
	.cfi_endproc

	.globl	lkill
	.p2align	4, 0x90
	.type	lkill,@function
lkill:                                  # @lkill
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi186:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi187:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi188:
	.cfi_def_cfa_offset 32
.Lcfi189:
	.cfi_offset %rbx, -32
.Lcfi190:
	.cfi_offset %r14, -24
.Lcfi191:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	movq	%r15, %rdi
	callq	get_c_long
	movq	%rax, %r15
	testq	%rbx, %rbx
	je	.LBB31_1
# BB#2:
	movq	%rbx, %rdi
	callq	get_c_long
	movq	%rax, %rsi
	jmp	.LBB31_3
.LBB31_1:
	movl	$9, %esi
.LBB31_3:
	movl	%r15d, %edi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	kill
	testl	%eax, %eax
	je	.LBB31_5
# BB#4:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.28, %edi
	movq	%rax, %rsi
	callq	err
	jmp	.LBB31_6
.LBB31_5:
	movq	%r14, %rdi
	callq	no_interrupt
.LBB31_6:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end31:
	.size	lkill, .Lfunc_end31-lkill
	.cfi_endproc

	.globl	lgetpid
	.p2align	4, 0x90
	.type	lgetpid,@function
lgetpid:                                # @lgetpid
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi192:
	.cfi_def_cfa_offset 16
	callq	getpid
	cvtsi2sdl	%eax, %xmm0
	popq	%rax
	jmp	flocons                 # TAILCALL
.Lfunc_end32:
	.size	lgetpid, .Lfunc_end32-lgetpid
	.cfi_endproc

	.globl	lgetpgrp
	.p2align	4, 0x90
	.type	lgetpgrp,@function
lgetpgrp:                               # @lgetpgrp
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi193:
	.cfi_def_cfa_offset 16
	callq	getpgrp
	cvtsi2sdl	%eax, %xmm0
	popq	%rax
	jmp	flocons                 # TAILCALL
.Lfunc_end33:
	.size	lgetpgrp, .Lfunc_end33-lgetpgrp
	.cfi_endproc

	.globl	lsetpgid
	.p2align	4, 0x90
	.type	lsetpgid,@function
lsetpgid:                               # @lsetpgid
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi194:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi195:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi196:
	.cfi_def_cfa_offset 32
.Lcfi197:
	.cfi_offset %rbx, -24
.Lcfi198:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	callq	get_c_long
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	get_c_long
	movl	%r14d, %edi
	movl	%eax, %esi
	callq	setpgid
	testl	%eax, %eax
	je	.LBB34_1
# BB#2:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.29, %edi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	err                     # TAILCALL
.LBB34_1:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end34:
	.size	lsetpgid, .Lfunc_end34-lsetpgid
	.cfi_endproc

	.globl	lgetgrgid
	.p2align	4, 0x90
	.type	lgetgrgid,@function
lgetgrgid:                              # @lgetgrgid
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi199:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi200:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi201:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi202:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi203:
	.cfi_def_cfa_offset 48
.Lcfi204:
	.cfi_offset %rbx, -48
.Lcfi205:
	.cfi_offset %r12, -40
.Lcfi206:
	.cfi_offset %r13, -32
.Lcfi207:
	.cfi_offset %r14, -24
.Lcfi208:
	.cfi_offset %r15, -16
	callq	get_c_long
	movq	%rax, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	movl	%ebx, %edi
	callq	getgrgid
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB35_1
# BB#2:
	movq	(%r15), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcons
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	cons
	movq	%rax, %r12
	movq	24(%r15), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB35_5
# BB#3:                                 # %.lr.ph.preheader
	movl	$8, %r13d
	.p2align	4, 0x90
.LBB35_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcons
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	cons
	movq	%rax, %r12
	movq	24(%r15), %rax
	movq	(%rax,%r13), %rbx
	addq	$8, %r13
	testq	%rbx, %rbx
	jne	.LBB35_4
.LBB35_5:                               # %._crit_edge
	movq	%r12, %rdi
	callq	nreverse
	movq	%rax, %rbx
	jmp	.LBB35_6
.LBB35_1:
	xorl	%ebx, %ebx
.LBB35_6:
	movq	%r14, %rdi
	callq	no_interrupt
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end35:
	.size	lgetgrgid, .Lfunc_end35-lgetgrgid
	.cfi_endproc

	.globl	lgetppid
	.p2align	4, 0x90
	.type	lgetppid,@function
lgetppid:                               # @lgetppid
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi209:
	.cfi_def_cfa_offset 16
	callq	getppid
	cvtsi2sdl	%eax, %xmm0
	popq	%rax
	jmp	flocons                 # TAILCALL
.Lfunc_end36:
	.size	lgetppid, .Lfunc_end36-lgetppid
	.cfi_endproc

	.globl	lmemref_byte
	.p2align	4, 0x90
	.type	lmemref_byte,@function
lmemref_byte:                           # @lmemref_byte
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi210:
	.cfi_def_cfa_offset 16
	callq	get_c_long
	movzbl	(%rax), %eax
	cvtsi2sdl	%eax, %xmm0
	popq	%rax
	jmp	flocons                 # TAILCALL
.Lfunc_end37:
	.size	lmemref_byte, .Lfunc_end37-lmemref_byte
	.cfi_endproc

	.globl	lexit
	.p2align	4, 0x90
	.type	lexit,@function
lexit:                                  # @lexit
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi211:
	.cfi_def_cfa_offset 16
.Lcfi212:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rbx, %rdi
	callq	get_c_long
	movl	%eax, %edi
	callq	exit
.Lfunc_end38:
	.size	lexit, .Lfunc_end38-lexit
	.cfi_endproc

	.globl	ltrunc
	.p2align	4, 0x90
	.type	ltrunc,@function
ltrunc:                                 # @ltrunc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi213:
	.cfi_def_cfa_offset 16
.Lcfi214:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB39_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB39_3
.LBB39_2:                               # %.critedge
	movl	$.L.str.30, %edi
	movq	%rbx, %rsi
	callq	err
.LBB39_3:
	cvttsd2si	8(%rbx), %rax
	cvtsi2sdq	%rax, %xmm0
	popq	%rbx
	jmp	flocons                 # TAILCALL
.Lfunc_end39:
	.size	ltrunc, .Lfunc_end39-ltrunc
	.cfi_endproc

	.globl	lputenv
	.p2align	4, 0x90
	.type	lputenv,@function
lputenv:                                # @lputenv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi215:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi216:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi217:
	.cfi_def_cfa_offset 32
.Lcfi218:
	.cfi_offset %rbx, -24
.Lcfi219:
	.cfi_offset %r14, -16
	callq	get_c_string
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	must_malloc
	movq	%rax, %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	strcpy
	movq	%rbx, %rdi
	callq	putenv
	testl	%eax, %eax
	je	.LBB40_1
# BB#2:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.31, %edi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	err                     # TAILCALL
.LBB40_1:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end40:
	.size	lputenv, .Lfunc_end40-lputenv
	.cfi_endproc

	.globl	handle_sigalrm
	.p2align	4, 0x90
	.type	handle_sigalrm,@function
handle_sigalrm:                         # @handle_sigalrm
	.cfi_startproc
# BB#0:
	cmpq	$1, nointerrupt(%rip)
	jne	.LBB41_4
# BB#1:
	cmpl	$0, handle_sigalrm_flag(%rip)
	je	.LBB41_3
# BB#2:
	movq	$1, interrupt_differed(%rip)
.LBB41_3:
	retq
.LBB41_4:
	movl	$.L.str.32, %edi
	xorl	%esi, %esi
	jmp	err                     # TAILCALL
.Lfunc_end41:
	.size	handle_sigalrm, .Lfunc_end41-handle_sigalrm
	.cfi_endproc

	.globl	lalarm
	.p2align	4, 0x90
	.type	lalarm,@function
lalarm:                                 # @lalarm
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi220:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi221:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi222:
	.cfi_def_cfa_offset 32
.Lcfi223:
	.cfi_offset %rbx, -32
.Lcfi224:
	.cfi_offset %r14, -24
.Lcfi225:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r15
	movl	$14, %edi
	movl	$handle_sigalrm, %esi
	callq	signal
	xorl	%eax, %eax
	testq	%rbx, %rbx
	setne	%al
	movl	%eax, handle_sigalrm_flag(%rip)
	movq	%r14, %rdi
	callq	get_c_long
	movl	%eax, %edi
	callq	alarm
	movl	%eax, %ebx
	movq	%r15, %rdi
	callq	no_interrupt
	cvtsi2sdl	%ebx, %xmm0
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	flocons                 # TAILCALL
.Lfunc_end42:
	.size	lalarm, .Lfunc_end42-lalarm
	.cfi_endproc

	.globl	l_opendir
	.p2align	4, 0x90
	.type	l_opendir,@function
l_opendir:                              # @l_opendir
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi226:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi227:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi228:
	.cfi_def_cfa_offset 32
.Lcfi229:
	.cfi_offset %rbx, -32
.Lcfi230:
	.cfi_offset %r14, -24
.Lcfi231:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	get_c_string
	movq	%rax, %rdi
	callq	opendir
	testq	%rax, %rax
	je	.LBB43_2
# BB#1:
	movzwl	tc_opendir(%rip), %ecx
	movw	%cx, 2(%rbx)
	movq	%rax, 8(%rbx)
	movq	%r14, %rdi
	callq	no_interrupt
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB43_2:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.33, %edi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	err                     # TAILCALL
.Lfunc_end43:
	.size	l_opendir, .Lfunc_end43-l_opendir
	.cfi_endproc

	.globl	get_opendir
	.p2align	4, 0x90
	.type	get_opendir,@function
get_opendir:                            # @get_opendir
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi232:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi233:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi234:
	.cfi_def_cfa_offset 32
.Lcfi235:
	.cfi_offset %rbx, -24
.Lcfi236:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB44_1
# BB#2:
	movswq	2(%rbx), %rax
	cmpq	tc_opendir(%rip), %rax
	jne	.LBB44_4
	jmp	.LBB44_5
.LBB44_1:
	xorl	%eax, %eax
	cmpq	tc_opendir(%rip), %rax
	je	.LBB44_5
.LBB44_4:
	movl	$.L.str.34, %edi
	movq	%rbx, %rsi
	callq	err
.LBB44_5:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB44_9
# BB#6:
	testq	%r14, %r14
	je	.LBB44_8
# BB#7:
	movl	$.L.str.35, %edi
	movq	%rbx, %rsi
	callq	err
.LBB44_8:
	xorl	%eax, %eax
.LBB44_9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end44:
	.size	get_opendir, .Lfunc_end44-get_opendir
	.cfi_endproc

	.globl	l_closedir
	.p2align	4, 0x90
	.type	l_closedir,@function
l_closedir:                             # @l_closedir
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi237:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi238:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi239:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi240:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi241:
	.cfi_def_cfa_offset 48
.Lcfi242:
	.cfi_offset %rbx, -40
.Lcfi243:
	.cfi_offset %r14, -32
.Lcfi244:
	.cfi_offset %r15, -24
.Lcfi245:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	testq	%r15, %r15
	je	.LBB45_1
# BB#2:
	movswq	2(%r15), %rax
	cmpq	tc_opendir(%rip), %rax
	jne	.LBB45_4
	jmp	.LBB45_5
.LBB45_1:
	xorl	%eax, %eax
	cmpq	tc_opendir(%rip), %rax
	je	.LBB45_5
.LBB45_4:
	movl	$.L.str.34, %edi
	movq	%r15, %rsi
	callq	err
.LBB45_5:
	movq	8(%r15), %rbx
	testq	%rbx, %rbx
	jne	.LBB45_7
# BB#6:
	movl	$.L.str.35, %edi
	movq	%r15, %rsi
	callq	err
	xorl	%ebx, %ebx
.LBB45_7:                               # %get_opendir.exit
	callq	__errno_location
	movl	(%rax), %ebp
	movq	$0, 8(%r15)
	movq	%rbx, %rdi
	callq	closedir
	testl	%eax, %eax
	je	.LBB45_8
# BB#9:
	movl	%ebp, %edi
	callq	llast_c_errmsg
	movl	$.L.str.36, %edi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	err                     # TAILCALL
.LBB45_8:
	movq	%r14, %rdi
	callq	no_interrupt
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end45:
	.size	l_closedir, .Lfunc_end45-l_closedir
	.cfi_endproc

	.globl	opendir_gc_free
	.p2align	4, 0x90
	.type	opendir_gc_free,@function
opendir_gc_free:                        # @opendir_gc_free
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi246:
	.cfi_def_cfa_offset 16
.Lcfi247:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB46_1
# BB#2:
	movswq	2(%rbx), %rax
	cmpq	tc_opendir(%rip), %rax
	jne	.LBB46_4
	jmp	.LBB46_5
.LBB46_1:
	xorl	%eax, %eax
	cmpq	tc_opendir(%rip), %rax
	je	.LBB46_5
.LBB46_4:
	movl	$.L.str.34, %edi
	movq	%rbx, %rsi
	callq	err
.LBB46_5:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB46_6
# BB#7:                                 # %get_opendir.exit
	popq	%rbx
	jmp	closedir                # TAILCALL
.LBB46_6:                               # %get_opendir.exit.thread
	popq	%rbx
	retq
.Lfunc_end46:
	.size	opendir_gc_free, .Lfunc_end46-opendir_gc_free
	.cfi_endproc

	.globl	l_readdir
	.p2align	4, 0x90
	.type	l_readdir,@function
l_readdir:                              # @l_readdir
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi248:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi249:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi250:
	.cfi_def_cfa_offset 32
.Lcfi251:
	.cfi_offset %rbx, -32
.Lcfi252:
	.cfi_offset %r14, -24
.Lcfi253:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB47_1
# BB#2:
	movswq	2(%rbx), %rax
	cmpq	tc_opendir(%rip), %rax
	jne	.LBB47_4
	jmp	.LBB47_5
.LBB47_1:
	xorl	%eax, %eax
	cmpq	tc_opendir(%rip), %rax
	je	.LBB47_5
.LBB47_4:
	movl	$.L.str.34, %edi
	movq	%rbx, %rsi
	callq	err
.LBB47_5:
	movq	8(%rbx), %r14
	testq	%r14, %r14
	jne	.LBB47_7
# BB#6:
	movl	$.L.str.35, %edi
	movq	%rbx, %rsi
	callq	err
	xorl	%r14d, %r14d
.LBB47_7:                               # %get_opendir.exit
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	readdir
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	no_interrupt
	testq	%rbx, %rbx
	je	.LBB47_8
# BB#9:
	movq	%rbx, %r14
	addq	$19, %r14
	movzwl	16(%rbx), %esi
	movq	%r14, %rdi
	callq	safe_strlen
	movq	%rax, %rdi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	strcons                 # TAILCALL
.LBB47_8:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end47:
	.size	l_readdir, .Lfunc_end47-l_readdir
	.cfi_endproc

	.globl	opendir_prin1
	.p2align	4, 0x90
	.type	opendir_prin1,@function
opendir_prin1:                          # @opendir_prin1
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi254:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi255:
	.cfi_def_cfa_offset 24
	subq	$264, %rsp              # imm = 0x108
.Lcfi256:
	.cfi_def_cfa_offset 288
.Lcfi257:
	.cfi_offset %rbx, -24
.Lcfi258:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB48_1
# BB#2:
	movswq	2(%rbx), %rax
	cmpq	tc_opendir(%rip), %rax
	jne	.LBB48_4
	jmp	.LBB48_5
.LBB48_1:
	xorl	%eax, %eax
	cmpq	tc_opendir(%rip), %rax
	je	.LBB48_5
.LBB48_4:
	movl	$.L.str.34, %edi
	movq	%rbx, %rsi
	callq	err
.LBB48_5:                               # %get_opendir.exit
	movq	8(%rbx), %rdx
	movq	%rsp, %rbx
	movl	$.L.str.37, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	gput_st
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end48:
	.size	opendir_prin1, .Lfunc_end48-opendir_prin1
	.cfi_endproc

	.globl	file_times
	.p2align	4, 0x90
	.type	file_times,@function
file_times:                             # @file_times
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi259:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi260:
	.cfi_def_cfa_offset 24
	subq	$152, %rsp
.Lcfi261:
	.cfi_def_cfa_offset 176
.Lcfi262:
	.cfi_offset %rbx, -24
.Lcfi263:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	get_c_string
	leaq	8(%rsp), %rdx
	movl	$1, %edi
	movq	%rax, %rsi
	callq	__xstat
	movl	%eax, %ebx
	movslq	%r14d, %rdi
	callq	no_interrupt
	testl	%ebx, %ebx
	je	.LBB49_2
# BB#1:
	xorl	%eax, %eax
	jmp	.LBB49_3
.LBB49_2:
	movq	112(%rsp), %rax
	cvtsi2sdq	%rax, %xmm0
	callq	flocons
	movq	%rax, %rbx
	movq	96(%rsp), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	callq	flocons
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	cons
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	cons
.LBB49_3:
	addq	$152, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end49:
	.size	file_times, .Lfunc_end49-file_times
	.cfi_endproc

	.globl	decode_st_moden
	.p2align	4, 0x90
	.type	decode_st_moden,@function
decode_st_moden:                        # @decode_st_moden
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi264:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi265:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi266:
	.cfi_def_cfa_offset 32
.Lcfi267:
	.cfi_offset %rbx, -24
.Lcfi268:
	.cfi_offset %r14, -16
	movl	%edi, %ebx
	testb	$8, %bh
	jne	.LBB50_2
# BB#1:
	xorl	%r14d, %r14d
	testb	$4, %bh
	jne	.LBB50_4
	jmp	.LBB50_5
.LBB50_2:
	movl	$.L.str.38, %edi
	callq	cintern
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	cons
	movq	%rax, %r14
	testb	$4, %bh
	je	.LBB50_5
.LBB50_4:
	movl	$.L.str.39, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	cons
	movq	%rax, %r14
.LBB50_5:
	testb	$1, %bh
	je	.LBB50_7
# BB#6:
	movl	$.L.str.40, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	cons
	movq	%rax, %r14
.LBB50_7:
	testb	%bl, %bl
	jns	.LBB50_9
# BB#8:
	movl	$.L.str.41, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	cons
	movq	%rax, %r14
.LBB50_9:
	testb	$64, %bl
	je	.LBB50_11
# BB#10:
	movl	$.L.str.42, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	cons
	movq	%rax, %r14
.LBB50_11:
	testb	$32, %bl
	je	.LBB50_13
# BB#12:
	movl	$.L.str.43, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	cons
	movq	%rax, %r14
.LBB50_13:
	testb	$16, %bl
	je	.LBB50_15
# BB#14:
	movl	$.L.str.44, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	cons
	movq	%rax, %r14
.LBB50_15:
	testb	$8, %bl
	je	.LBB50_17
# BB#16:
	movl	$.L.str.45, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	cons
	movq	%rax, %r14
.LBB50_17:
	testb	$4, %bl
	je	.LBB50_19
# BB#18:
	movl	$.L.str.46, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	cons
	movq	%rax, %r14
.LBB50_19:
	testb	$2, %bl
	je	.LBB50_21
# BB#20:
	movl	$.L.str.47, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	cons
	movq	%rax, %r14
.LBB50_21:
	testb	$1, %bl
	je	.LBB50_23
# BB#22:
	movl	$.L.str.48, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	cons
	movq	%rax, %r14
.LBB50_23:
	andl	$61440, %ebx            # imm = 0xF000
	addl	$-4096, %ebx            # imm = 0xF000
	cmpl	$49151, %ebx            # imm = 0xBFFF
	ja	.LBB50_25
# BB#24:                                # %switch.hole_check
	shrl	$12, %ebx
	movl	$2731, %eax             # imm = 0xAAB
	btl	%ebx, %eax
	jb	.LBB50_26
.LBB50_25:                              # %.thread45
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB50_26:                              # %switch.lookup
	movl	%ebx, %eax
	movq	.Lswitch.table(,%rax,8), %rdi
	callq	cintern
	movq	%rax, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	cons                    # TAILCALL
.Lfunc_end50:
	.size	decode_st_moden, .Lfunc_end50-decode_st_moden
	.cfi_endproc

	.globl	encode_st_mode
	.p2align	4, 0x90
	.type	encode_st_mode,@function
encode_st_mode:                         # @encode_st_mode
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi269:
	.cfi_def_cfa_offset 16
	movl	$.L.str.38, %esi
	movl	$2048, %edx             # imm = 0x800
	movl	$.L.str.39, %ecx
	movl	$1024, %r8d             # imm = 0x400
	movl	$.L.str.40, %r9d
	movl	$0, %eax
	pushq	$0
.Lcfi270:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi271:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.48
.Lcfi272:
	.cfi_adjust_cfa_offset 8
	pushq	$2
.Lcfi273:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.47
.Lcfi274:
	.cfi_adjust_cfa_offset 8
	pushq	$4
.Lcfi275:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.46
.Lcfi276:
	.cfi_adjust_cfa_offset 8
	pushq	$8
.Lcfi277:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.45
.Lcfi278:
	.cfi_adjust_cfa_offset 8
	pushq	$16
.Lcfi279:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.44
.Lcfi280:
	.cfi_adjust_cfa_offset 8
	pushq	$32
.Lcfi281:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.43
.Lcfi282:
	.cfi_adjust_cfa_offset 8
	pushq	$64
.Lcfi283:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.42
.Lcfi284:
	.cfi_adjust_cfa_offset 8
	pushq	$128
.Lcfi285:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.41
.Lcfi286:
	.cfi_adjust_cfa_offset 8
	pushq	$256                    # imm = 0x100
.Lcfi287:
	.cfi_adjust_cfa_offset 8
	callq	assemble_options
	addq	$144, %rsp
.Lcfi288:
	.cfi_adjust_cfa_offset -144
	cvtsi2sdl	%eax, %xmm0
	popq	%rax
	jmp	flocons                 # TAILCALL
.Lfunc_end51:
	.size	encode_st_mode, .Lfunc_end51-encode_st_mode
	.cfi_endproc

	.globl	decode_st_mode
	.p2align	4, 0x90
	.type	decode_st_mode,@function
decode_st_mode:                         # @decode_st_mode
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi289:
	.cfi_def_cfa_offset 16
	callq	get_c_long
	movl	%eax, %edi
	popq	%rax
	jmp	decode_st_moden         # TAILCALL
.Lfunc_end52:
	.size	decode_st_mode, .Lfunc_end52-decode_st_mode
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI53_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI53_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.text
	.globl	decode_stat
	.p2align	4, 0x90
	.type	decode_stat,@function
decode_stat:                            # @decode_stat
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi290:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi291:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi292:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi293:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi294:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi295:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi296:
	.cfi_def_cfa_offset 112
.Lcfi297:
	.cfi_offset %rbx, -56
.Lcfi298:
	.cfi_offset %r12, -48
.Lcfi299:
	.cfi_offset %r13, -40
.Lcfi300:
	.cfi_offset %r14, -32
.Lcfi301:
	.cfi_offset %r15, -24
.Lcfi302:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	(%rbx), %xmm1           # xmm1 = mem[0],zero
	punpckldq	.LCPI53_0(%rip), %xmm1 # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	subpd	.LCPI53_1(%rip), %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	callq	flocons
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	punpckldq	.LCPI53_0(%rip), %xmm1 # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	subpd	.LCPI53_1(%rip), %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	callq	flocons
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	24(%rbx), %edi
	callq	decode_st_moden
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	16(%rbx), %xmm1         # xmm1 = mem[0],zero
	punpckldq	.LCPI53_0(%rip), %xmm1 # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	subpd	.LCPI53_1(%rip), %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	callq	flocons
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	28(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	callq	flocons
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	32(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	callq	flocons
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	40(%rbx), %xmm1         # xmm1 = mem[0],zero
	punpckldq	.LCPI53_0(%rip), %xmm1 # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	subpd	.LCPI53_1(%rip), %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	callq	flocons
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	48(%rbx), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	callq	flocons
	movq	%rax, %r12
	movq	72(%rbx), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	callq	flocons
	movq	%rax, %r13
	movq	88(%rbx), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	callq	flocons
	movq	%rax, %rbp
	movq	104(%rbx), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	callq	flocons
	movq	%rax, %r14
	movq	56(%rbx), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	callq	flocons
	movq	%rax, %r15
	movq	64(%rbx), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	callq	flocons
	movq	%rax, %rbx
	subq	$8, %rsp
.Lcfi303:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.56, %edi
	movl	$.L.str.57, %edx
	movl	$.L.str.58, %r8d
	movl	$0, %eax
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	pushq	$0
.Lcfi304:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi305:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.66
.Lcfi306:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi307:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.65
.Lcfi308:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi309:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.64
.Lcfi310:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi311:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.63
.Lcfi312:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi313:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.62
.Lcfi314:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi315:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.61
.Lcfi316:
	.cfi_adjust_cfa_offset 8
	pushq	112(%rsp)               # 8-byte Folded Reload
.Lcfi317:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.60
.Lcfi318:
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)               # 8-byte Folded Reload
.Lcfi319:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.5
.Lcfi320:
	.cfi_adjust_cfa_offset 8
	pushq	176(%rsp)               # 8-byte Folded Reload
.Lcfi321:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.4
.Lcfi322:
	.cfi_adjust_cfa_offset 8
	pushq	208(%rsp)               # 8-byte Folded Reload
.Lcfi323:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.59
.Lcfi324:
	.cfi_adjust_cfa_offset 8
	callq	symalist
	addq	$232, %rsp
.Lcfi325:
	.cfi_adjust_cfa_offset -176
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end53:
	.size	decode_stat, .Lfunc_end53-decode_stat
	.cfi_endproc

	.globl	g_stat
	.p2align	4, 0x90
	.type	g_stat,@function
g_stat:                                 # @g_stat
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi326:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi327:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi328:
	.cfi_def_cfa_offset 32
	subq	$144, %rsp
.Lcfi329:
	.cfi_def_cfa_offset 176
.Lcfi330:
	.cfi_offset %rbx, -32
.Lcfi331:
	.cfi_offset %r14, -24
.Lcfi332:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r15
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%rsp, %rsi
	movq	%rax, %rdi
	callq	*%r14
	movl	%eax, %ebx
	movslq	%r15d, %rdi
	callq	no_interrupt
	testl	%ebx, %ebx
	je	.LBB54_2
# BB#1:
	xorl	%eax, %eax
	jmp	.LBB54_3
.LBB54_2:
	movq	%rsp, %rdi
	callq	decode_stat
.LBB54_3:
	addq	$144, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end54:
	.size	g_stat, .Lfunc_end54-g_stat
	.cfi_endproc

	.globl	l_stat
	.p2align	4, 0x90
	.type	l_stat,@function
l_stat:                                 # @l_stat
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi333:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi334:
	.cfi_def_cfa_offset 24
	subq	$152, %rsp
.Lcfi335:
	.cfi_def_cfa_offset 176
.Lcfi336:
	.cfi_offset %rbx, -24
.Lcfi337:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%rsp, %rdx
	movl	$1, %edi
	movq	%rax, %rsi
	callq	__xstat
	movl	%eax, %ebx
	movslq	%r14d, %rdi
	callq	no_interrupt
	testl	%ebx, %ebx
	je	.LBB55_2
# BB#1:
	xorl	%eax, %eax
	jmp	.LBB55_3
.LBB55_2:
	movq	%rsp, %rdi
	callq	decode_stat
.LBB55_3:                               # %g_stat.exit
	addq	$152, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end55:
	.size	l_stat, .Lfunc_end55-l_stat
	.cfi_endproc

	.globl	l_fstat
	.p2align	4, 0x90
	.type	l_fstat,@function
l_fstat:                                # @l_fstat
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi338:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi339:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi340:
	.cfi_def_cfa_offset 32
	subq	$144, %rsp
.Lcfi341:
	.cfi_def_cfa_offset 176
.Lcfi342:
	.cfi_offset %rbx, -32
.Lcfi343:
	.cfi_offset %r14, -24
.Lcfi344:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r15
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	get_c_file
	movq	%rax, %rdi
	callq	fileno
	movq	%rsp, %rdx
	movl	$1, %edi
	movl	%eax, %esi
	callq	__fxstat
	movl	%eax, %ebx
	movslq	%r15d, %rdi
	callq	no_interrupt
	testl	%ebx, %ebx
	jne	.LBB56_2
# BB#1:
	movq	%rsp, %rdi
	callq	decode_stat
	movq	%rax, %r14
.LBB56_2:
	movq	%r14, %rax
	addq	$144, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end56:
	.size	l_fstat, .Lfunc_end56-l_fstat
	.cfi_endproc

	.globl	l_lstat
	.p2align	4, 0x90
	.type	l_lstat,@function
l_lstat:                                # @l_lstat
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi345:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi346:
	.cfi_def_cfa_offset 24
	subq	$152, %rsp
.Lcfi347:
	.cfi_def_cfa_offset 176
.Lcfi348:
	.cfi_offset %rbx, -24
.Lcfi349:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%rsp, %rdx
	movl	$1, %edi
	movq	%rax, %rsi
	callq	__lxstat
	movl	%eax, %ebx
	movslq	%r14d, %rdi
	callq	no_interrupt
	testl	%ebx, %ebx
	je	.LBB57_2
# BB#1:
	xorl	%eax, %eax
	jmp	.LBB57_3
.LBB57_2:
	movq	%rsp, %rdi
	callq	decode_stat
.LBB57_3:                               # %g_stat.exit
	addq	$152, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end57:
	.size	l_lstat, .Lfunc_end57-l_lstat
	.cfi_endproc

	.globl	l_chmod
	.p2align	4, 0x90
	.type	l_chmod,@function
l_chmod:                                # @l_chmod
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi350:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi351:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi352:
	.cfi_def_cfa_offset 32
.Lcfi353:
	.cfi_offset %rbx, -24
.Lcfi354:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	callq	get_c_string
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	get_c_long
	movq	%r14, %rdi
	movl	%eax, %esi
	callq	chmod
	testl	%eax, %eax
	je	.LBB58_1
# BB#2:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.67, %edi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	err                     # TAILCALL
.LBB58_1:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end58:
	.size	l_chmod, .Lfunc_end58-l_chmod
	.cfi_endproc

	.globl	lutime
	.p2align	4, 0x90
	.type	lutime,@function
lutime:                                 # @lutime
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi355:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi356:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi357:
	.cfi_def_cfa_offset 48
.Lcfi358:
	.cfi_offset %rbx, -24
.Lcfi359:
	.cfi_offset %r14, -16
	movq	%rdx, %rbx
	movq	%rdi, %r14
	movq	%rsi, %rdi
	callq	get_c_long
	movq	%rax, 16(%rsp)
	testq	%rbx, %rbx
	je	.LBB59_2
# BB#1:
	movq	%rbx, %rdi
	callq	get_c_long
	jmp	.LBB59_3
.LBB59_2:
	xorl	%edi, %edi
	callq	time
.LBB59_3:
	movq	%rax, 8(%rsp)
	movq	%r14, %rdi
	callq	get_c_string
	leaq	8(%rsp), %rsi
	movq	%rax, %rdi
	callq	utime
	testl	%eax, %eax
	je	.LBB59_4
# BB#5:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.68, %edi
	movq	%rax, %rsi
	callq	err
	jmp	.LBB59_6
.LBB59_4:
	xorl	%eax, %eax
.LBB59_6:
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end59:
	.size	lutime, .Lfunc_end59-lutime
	.cfi_endproc

	.globl	lfchmod
	.p2align	4, 0x90
	.type	lfchmod,@function
lfchmod:                                # @lfchmod
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi360:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi361:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi362:
	.cfi_def_cfa_offset 32
.Lcfi363:
	.cfi_offset %rbx, -24
.Lcfi364:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	xorl	%esi, %esi
	callq	get_c_file
	movq	%rax, %rdi
	callq	fileno
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	get_c_long
	movl	%ebp, %edi
	movl	%eax, %esi
	callq	fchmod
	testl	%eax, %eax
	je	.LBB60_1
# BB#2:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.69, %edi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	err                     # TAILCALL
.LBB60_1:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end60:
	.size	lfchmod, .Lfunc_end60-lfchmod
	.cfi_endproc

	.globl	encode_open_flags
	.p2align	4, 0x90
	.type	encode_open_flags,@function
encode_open_flags:                      # @encode_open_flags
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi365:
	.cfi_def_cfa_offset 16
	movl	$.L.str.70, %esi
	movl	$2048, %edx             # imm = 0x800
	movl	$.L.str.71, %ecx
	movl	$1024, %r8d             # imm = 0x400
	movl	$.L.str.72, %r9d
	movl	$0, %eax
	pushq	$0
.Lcfi366:
	.cfi_adjust_cfa_offset 8
	pushq	$128
.Lcfi367:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.77
.Lcfi368:
	.cfi_adjust_cfa_offset 8
	pushq	$512                    # imm = 0x200
.Lcfi369:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.76
.Lcfi370:
	.cfi_adjust_cfa_offset 8
	pushq	$64
.Lcfi371:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.75
.Lcfi372:
	.cfi_adjust_cfa_offset 8
	pushq	$2
.Lcfi373:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.74
.Lcfi374:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi375:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.73
.Lcfi376:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi377:
	.cfi_adjust_cfa_offset 8
	callq	assemble_options
	addq	$96, %rsp
.Lcfi378:
	.cfi_adjust_cfa_offset -96
	cvtsi2sdl	%eax, %xmm0
	popq	%rax
	jmp	flocons                 # TAILCALL
.Lfunc_end61:
	.size	encode_open_flags, .Lfunc_end61-encode_open_flags
	.cfi_endproc

	.globl	get_fd
	.p2align	4, 0x90
	.type	get_fd,@function
get_fd:                                 # @get_fd
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB62_3
# BB#1:
	movzwl	2(%rdi), %eax
	cmpl	$17, %eax
	jne	.LBB62_3
# BB#2:
	pushq	%rax
.Lcfi379:
	.cfi_def_cfa_offset 16
	xorl	%esi, %esi
	callq	get_c_file
	movq	%rax, %rdi
	popq	%rax
	jmp	fileno                  # TAILCALL
.LBB62_3:
	jmp	get_c_long              # TAILCALL
.Lfunc_end62:
	.size	get_fd, .Lfunc_end62-get_fd
	.cfi_endproc

	.globl	gsetlk
	.p2align	4, 0x90
	.type	gsetlk,@function
gsetlk:                                 # @gsetlk
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi380:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi381:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi382:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi383:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi384:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi385:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi386:
	.cfi_def_cfa_offset 96
.Lcfi387:
	.cfi_offset %rbx, -56
.Lcfi388:
	.cfi_offset %r12, -48
.Lcfi389:
	.cfi_offset %r13, -40
.Lcfi390:
	.cfi_offset %r14, -32
.Lcfi391:
	.cfi_offset %r15, -24
.Lcfi392:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%r8, %r13
	movq	%rcx, %rbx
	movq	%rdx, %rbp
	movq	%rsi, %rax
	movl	%edi, %r14d
	testq	%rax, %rax
	je	.LBB63_3
# BB#1:
	movzwl	2(%rax), %ecx
	cmpl	$17, %ecx
	jne	.LBB63_3
# BB#2:
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	get_c_file
	movq	%rax, %rdi
	callq	fileno
	movl	%eax, %r15d
	jmp	.LBB63_4
.LBB63_3:                               # %.critedge.i
	movq	%rax, %rdi
	callq	get_c_long
	movq	%rax, %r15
.LBB63_4:                               # %get_fd.exit
	movq	%rbp, %rdi
	callq	get_c_long
	movw	%ax, 8(%rsp)
	testq	%rbx, %rbx
	je	.LBB63_5
# BB#6:
	movq	%rbx, %rdi
	callq	get_c_long
	jmp	.LBB63_7
.LBB63_5:
	xorl	%eax, %eax
.LBB63_7:
	movw	%ax, 10(%rsp)
	testq	%r13, %r13
	je	.LBB63_8
# BB#9:
	movq	%r13, %rdi
	callq	get_c_long
	jmp	.LBB63_10
.LBB63_8:
	xorl	%eax, %eax
.LBB63_10:
	movq	%rax, 16(%rsp)
	testq	%r12, %r12
	je	.LBB63_11
# BB#12:
	movq	%r12, %rdi
	callq	get_c_long
	jmp	.LBB63_13
.LBB63_11:
	xorl	%eax, %eax
.LBB63_13:
	movq	%rax, 24(%rsp)
	movl	$0, 32(%rsp)
	leaq	8(%rsp), %rdx
	xorl	%eax, %eax
	movl	%r15d, %edi
	movl	%r14d, %esi
	callq	fcntl
	cmpl	$-1, %eax
	je	.LBB63_14
# BB#15:
	cmpl	$5, %r14d
	jne	.LBB63_16
# BB#17:
	movzwl	8(%rsp), %eax
	cmpl	$2, %eax
	jne	.LBB63_19
# BB#18:
	xorl	%eax, %eax
	jmp	.LBB63_20
.LBB63_14:
	movl	$-1, %edi
	callq	llast_c_errmsg
	jmp	.LBB63_20
.LBB63_16:
	xorl	%eax, %eax
	jmp	.LBB63_20
.LBB63_19:
	cwtl
	cvtsi2sdl	%eax, %xmm0
	callq	flocons
	movq	%rax, %rbx
	movl	32(%rsp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	callq	flocons
	movq	%rax, %rcx
	movl	$2, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%rcx, %rdx
	callq	listn
.LBB63_20:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end63:
	.size	gsetlk, .Lfunc_end63-gsetlk
	.cfi_endproc

	.globl	lF_SETLK
	.p2align	4, 0x90
	.type	lF_SETLK,@function
lF_SETLK:                               # @lF_SETLK
	.cfi_startproc
# BB#0:
	movq	%r8, %r9
	movq	%rcx, %rax
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movl	$6, %edi
	movq	%rax, %r8
	jmp	gsetlk                  # TAILCALL
.Lfunc_end64:
	.size	lF_SETLK, .Lfunc_end64-lF_SETLK
	.cfi_endproc

	.globl	lF_SETLKW
	.p2align	4, 0x90
	.type	lF_SETLKW,@function
lF_SETLKW:                              # @lF_SETLKW
	.cfi_startproc
# BB#0:
	movq	%r8, %r9
	movq	%rcx, %rax
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movl	$7, %edi
	movq	%rax, %r8
	jmp	gsetlk                  # TAILCALL
.Lfunc_end65:
	.size	lF_SETLKW, .Lfunc_end65-lF_SETLKW
	.cfi_endproc

	.globl	lF_GETLK
	.p2align	4, 0x90
	.type	lF_GETLK,@function
lF_GETLK:                               # @lF_GETLK
	.cfi_startproc
# BB#0:
	movq	%r8, %r9
	movq	%rcx, %rax
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movl	$5, %edi
	movq	%rax, %r8
	jmp	gsetlk                  # TAILCALL
.Lfunc_end66:
	.size	lF_GETLK, .Lfunc_end66-lF_GETLK
	.cfi_endproc

	.globl	delete_file
	.p2align	4, 0x90
	.type	delete_file,@function
delete_file:                            # @delete_file
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi393:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi394:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi395:
	.cfi_def_cfa_offset 32
.Lcfi396:
	.cfi_offset %rbx, -24
.Lcfi397:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%rax, %rdi
	callq	unlink
	movl	%eax, %ebx
	movslq	%r14d, %rdi
	callq	no_interrupt
	testl	%ebx, %ebx
	je	.LBB67_1
# BB#2:
	movl	$-1, %edi
	callq	last_c_errmsg
	movq	$-1, %rdi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	strcons                 # TAILCALL
.LBB67_1:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end67:
	.size	delete_file, .Lfunc_end67-delete_file
	.cfi_endproc

	.globl	utime2str
	.p2align	4, 0x90
	.type	utime2str,@function
utime2str:                              # @utime2str
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi398:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi399:
	.cfi_def_cfa_offset 24
	subq	$120, %rsp
.Lcfi400:
	.cfi_def_cfa_offset 144
.Lcfi401:
	.cfi_offset %rbx, -24
.Lcfi402:
	.cfi_offset %r14, -16
	callq	get_c_long
	movq	%rax, 8(%rsp)
	leaq	8(%rsp), %rdi
	callq	localtime
	testq	%rax, %rax
	je	.LBB68_1
# BB#2:
	movl	$1900, %edx             # imm = 0x76C
	addl	20(%rax), %edx
	movl	16(%rax), %ecx
	incl	%ecx
	movl	12(%rax), %r8d
	movl	8(%rax), %r9d
	movl	(%rax), %ebx
	movl	4(%rax), %r10d
	subq	$8, %rsp
.Lcfi403:
	.cfi_adjust_cfa_offset 8
	leaq	24(%rsp), %r14
	movl	$.L.str.78, %esi
	movl	$0, %eax
	movq	%r14, %rdi
	pushq	$0
.Lcfi404:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi405:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi406:
	.cfi_adjust_cfa_offset 8
	callq	sprintf
	addq	$32, %rsp
.Lcfi407:
	.cfi_adjust_cfa_offset -32
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	strcons
	jmp	.LBB68_3
.LBB68_1:
	xorl	%eax, %eax
.LBB68_3:
	addq	$120, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end68:
	.size	utime2str, .Lfunc_end68-utime2str
	.cfi_endproc

	.globl	lgetenv
	.p2align	4, 0x90
	.type	lgetenv,@function
lgetenv:                                # @lgetenv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi408:
	.cfi_def_cfa_offset 16
.Lcfi409:
	.cfi_offset %rbx, -16
	callq	get_c_string
	movq	%rax, %rdi
	callq	getenv
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB69_1
# BB#2:
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%rbx, %rsi
	popq	%rbx
	jmp	strcons                 # TAILCALL
.LBB69_1:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end69:
	.size	lgetenv, .Lfunc_end69-lgetenv
	.cfi_endproc

	.globl	unix_time
	.p2align	4, 0x90
	.type	unix_time,@function
unix_time:                              # @unix_time
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi410:
	.cfi_def_cfa_offset 16
	xorl	%edi, %edi
	callq	time
	cvtsi2sdq	%rax, %xmm0
	popq	%rax
	jmp	flocons                 # TAILCALL
.Lfunc_end70:
	.size	unix_time, .Lfunc_end70-unix_time
	.cfi_endproc

	.globl	unix_ctime
	.p2align	4, 0x90
	.type	unix_ctime,@function
unix_ctime:                             # @unix_ctime
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi411:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi412:
	.cfi_def_cfa_offset 32
.Lcfi413:
	.cfi_offset %rbx, -16
	testq	%rdi, %rdi
	je	.LBB71_2
# BB#1:
	callq	get_c_long
	movq	%rax, 8(%rsp)
	jmp	.LBB71_3
.LBB71_2:
	leaq	8(%rsp), %rdi
	callq	time
.LBB71_3:
	leaq	8(%rsp), %rdi
	callq	ctime
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB71_4
# BB#5:
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB71_7
# BB#6:
	movb	$0, (%rax)
.LBB71_7:
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcons
	jmp	.LBB71_8
.LBB71_4:
	xorl	%eax, %eax
.LBB71_8:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end71:
	.size	unix_ctime, .Lfunc_end71-unix_ctime
	.cfi_endproc

	.globl	http_date
	.p2align	4, 0x90
	.type	http_date,@function
http_date:                              # @http_date
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi414:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi415:
	.cfi_def_cfa_offset 24
	subq	$280, %rsp              # imm = 0x118
.Lcfi416:
	.cfi_def_cfa_offset 304
.Lcfi417:
	.cfi_offset %rbx, -24
.Lcfi418:
	.cfi_offset %r14, -16
	testq	%rdi, %rdi
	je	.LBB72_2
# BB#1:
	callq	get_c_long
	movq	%rax, 8(%rsp)
	jmp	.LBB72_3
.LBB72_2:
	leaq	8(%rsp), %rdi
	callq	time
.LBB72_3:
	leaq	8(%rsp), %rdi
	callq	gmtime
	testq	%rax, %rax
	je	.LBB72_4
# BB#5:
	movslq	24(%rax), %rcx
	leaq	.L.str.80(,%rcx,4), %rdx
	movl	12(%rax), %ecx
	movslq	16(%rax), %rsi
	leaq	.L.str.81(,%rsi,4), %r8
	movl	$1900, %r9d             # imm = 0x76C
	addl	20(%rax), %r9d
	movl	8(%rax), %r10d
	movl	(%rax), %ebx
	movl	4(%rax), %r11d
	subq	$8, %rsp
.Lcfi419:
	.cfi_adjust_cfa_offset 8
	leaq	24(%rsp), %r14
	movl	$.L.str.79, %esi
	movl	$0, %eax
	movq	%r14, %rdi
	pushq	%rbx
.Lcfi420:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi421:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi422:
	.cfi_adjust_cfa_offset 8
	callq	sprintf
	addq	$32, %rsp
.Lcfi423:
	.cfi_adjust_cfa_offset -32
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	strcons
	jmp	.LBB72_6
.LBB72_4:
	xorl	%eax, %eax
.LBB72_6:
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end72:
	.size	http_date, .Lfunc_end72-http_date
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI73_0:
	.quad	4696837146684686336     # double 1.0E+6
	.text
	.globl	lsleep
	.p2align	4, 0x90
	.type	lsleep,@function
lsleep:                                 # @lsleep
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi424:
	.cfi_def_cfa_offset 16
	callq	get_c_double
	mulsd	.LCPI73_0(%rip), %xmm0
	cvttsd2si	%xmm0, %rdi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	usleep
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end73:
	.size	lsleep, .Lfunc_end73-lsleep
	.cfi_endproc

	.globl	url_encode
	.p2align	4, 0x90
	.type	url_encode,@function
url_encode:                             # @url_encode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi425:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi426:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi427:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi428:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi429:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi430:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi431:
	.cfi_def_cfa_offset 64
.Lcfi432:
	.cfi_offset %rbx, -56
.Lcfi433:
	.cfi_offset %r12, -48
.Lcfi434:
	.cfi_offset %r13, -40
.Lcfi435:
	.cfi_offset %r14, -32
.Lcfi436:
	.cfi_offset %r15, -24
.Lcfi437:
	.cfi_offset %rbp, -16
	movq	%rdi, (%rsp)            # 8-byte Spill
	callq	get_c_string
	movq	%rax, %r14
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	movq	%r14, %r15
	jmp	.LBB74_1
	.p2align	4, 0x90
.LBB74_19:                              #   in Loop: Header=BB74_1 Depth=1
	incl	%ebp
	incq	%r15
.LBB74_1:                               # =>This Inner Loop Header: Depth=1
	movsbl	(%r15), %ebx
	cmpl	$32, %ebx
	je	.LBB74_19
# BB#2:                                 #   in Loop: Header=BB74_1 Depth=1
	testb	%bl, %bl
	je	.LBB74_7
# BB#3:                                 #   in Loop: Header=BB74_1 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	testb	$8, (%rax,%rcx,2)
	jne	.LBB74_6
# BB#4:                                 #   in Loop: Header=BB74_1 Depth=1
	movl	$.L.str.82, %edi
	movl	$6, %edx
	movl	%ebx, %esi
	callq	memchr
	testq	%rax, %rax
	je	.LBB74_5
.LBB74_6:                               #   in Loop: Header=BB74_1 Depth=1
	incl	%r13d
	incq	%r15
	jmp	.LBB74_1
.LBB74_5:                               #   in Loop: Header=BB74_1 Depth=1
	incl	%r12d
	incq	%r15
	jmp	.LBB74_1
.LBB74_7:
	movl	%ebp, %eax
	orl	%r12d, %eax
	je	.LBB74_18
# BB#8:
	leal	(%r12,%r12,2), %eax
	addl	%r13d, %eax
	addl	%ebp, %eax
	movslq	%eax, %rdi
	xorl	%esi, %esi
	callq	strcons
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rax, %rdi
	callq	get_c_string
	movq	%rax, %r15
	jmp	.LBB74_9
	.p2align	4, 0x90
.LBB74_15:                              #   in Loop: Header=BB74_9 Depth=1
	incq	%r15
	incq	%r14
.LBB74_9:                               # =>This Inner Loop Header: Depth=1
	movsbl	(%r14), %ebp
	cmpl	$32, %ebp
	je	.LBB74_14
# BB#10:                                #   in Loop: Header=BB74_9 Depth=1
	testb	%bpl, %bpl
	je	.LBB74_17
# BB#11:                                #   in Loop: Header=BB74_9 Depth=1
	movslq	%ebp, %rbx
	callq	__ctype_b_loc
	movq	(%rax), %rax
	testb	$8, (%rax,%rbx,2)
	jne	.LBB74_16
# BB#12:                                #   in Loop: Header=BB74_9 Depth=1
	movl	$.L.str.82, %edi
	movl	$6, %edx
	movl	%ebp, %esi
	callq	memchr
	testq	%rax, %rax
	je	.LBB74_13
.LBB74_16:                              #   in Loop: Header=BB74_9 Depth=1
	movb	%bpl, (%r15)
	jmp	.LBB74_15
	.p2align	4, 0x90
.LBB74_14:                              #   in Loop: Header=BB74_9 Depth=1
	movb	$43, (%r15)
	jmp	.LBB74_15
.LBB74_13:                              #   in Loop: Header=BB74_9 Depth=1
	movzbl	%bpl, %edx
	movl	$.L.str.83, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sprintf
	addq	$3, %r15
	incq	%r14
	jmp	.LBB74_9
.LBB74_17:
	movb	$0, (%r15)
.LBB74_18:
	movq	(%rsp), %rax            # 8-byte Reload
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end74:
	.size	url_encode, .Lfunc_end74-url_encode
	.cfi_endproc

	.globl	url_decode
	.p2align	4, 0x90
	.type	url_decode,@function
url_decode:                             # @url_decode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi438:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi439:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi440:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi441:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi442:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi443:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi444:
	.cfi_def_cfa_offset 64
.Lcfi445:
	.cfi_offset %rbx, -56
.Lcfi446:
	.cfi_offset %r12, -48
.Lcfi447:
	.cfi_offset %r13, -40
.Lcfi448:
	.cfi_offset %r14, -32
.Lcfi449:
	.cfi_offset %r15, -24
.Lcfi450:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	get_c_string
	movq	%rax, %r15
	movq	%r15, %rbp
	addq	$2, %rbp
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	jmp	.LBB75_2
	.p2align	4, 0x90
.LBB75_1:                               #   in Loop: Header=BB75_2 Depth=1
	incl	%r13d
	incq	%rbp
.LBB75_2:                               # =>This Inner Loop Header: Depth=1
	movzbl	-2(%rbp), %eax
	cmpb	$37, %al
	je	.LBB75_6
# BB#3:                                 #   in Loop: Header=BB75_2 Depth=1
	cmpb	$43, %al
	je	.LBB75_1
# BB#4:                                 #   in Loop: Header=BB75_2 Depth=1
	testb	%al, %al
	je	.LBB75_9
# BB#5:                                 #   in Loop: Header=BB75_2 Depth=1
	incl	%r12d
	incq	%rbp
	jmp	.LBB75_2
	.p2align	4, 0x90
.LBB75_6:                               #   in Loop: Header=BB75_2 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movsbq	-1(%rbp), %rcx
	testb	$16, 1(%rax,%rcx,2)
	je	.LBB75_25
# BB#7:                                 #   in Loop: Header=BB75_2 Depth=1
	movsbq	(%rbp), %rcx
	testb	$16, 1(%rax,%rcx,2)
	je	.LBB75_25
# BB#8:                                 #   in Loop: Header=BB75_2 Depth=1
	incl	%ebx
	incq	%rbp
	jmp	.LBB75_2
.LBB75_9:
	movl	%r13d, %eax
	orl	%ebx, %eax
	je	.LBB75_26
# BB#10:
	addl	%r12d, %ebx
	addl	%r13d, %ebx
	movslq	%ebx, %rdi
	xorl	%esi, %esi
	callq	strcons
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	get_c_string
	movq	%rax, %r13
	jmp	.LBB75_12
	.p2align	4, 0x90
.LBB75_11:                              #   in Loop: Header=BB75_12 Depth=1
	movb	%bl, (%r13)
	incq	%r13
	incq	%r15
.LBB75_12:                              # =>This Inner Loop Header: Depth=1
	movb	(%r15), %bl
	cmpb	$37, %bl
	je	.LBB75_15
# BB#13:                                #   in Loop: Header=BB75_12 Depth=1
	cmpb	$43, %bl
	je	.LBB75_17
# BB#14:                                #   in Loop: Header=BB75_12 Depth=1
	testb	%bl, %bl
	jne	.LBB75_11
	jmp	.LBB75_23
	.p2align	4, 0x90
.LBB75_15:                              #   in Loop: Header=BB75_12 Depth=1
	movb	$0, (%r13)
	callq	__ctype_b_loc
	movq	%rax, %r12
	movq	(%r12), %rax
	movsbq	1(%r15), %rbp
	testb	$8, 1(%rax,%rbp,2)
	jne	.LBB75_18
# BB#16:                                #   in Loop: Header=BB75_12 Depth=1
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movl	(%rax,%rbp,4), %ebp
	addl	$-55, %ebp
	jmp	.LBB75_19
	.p2align	4, 0x90
.LBB75_17:                              #   in Loop: Header=BB75_12 Depth=1
	movb	$32, %bl
	jmp	.LBB75_11
.LBB75_18:                              #   in Loop: Header=BB75_12 Depth=1
	addl	$-48, %ebp
.LBB75_19:                              #   in Loop: Header=BB75_12 Depth=1
	movb	%bpl, (%r13)
	shll	$4, %ebp
	movq	(%r12), %rax
	movsbq	2(%r15), %rbx
	testb	$8, 1(%rax,%rbx,2)
	jne	.LBB75_21
# BB#20:                                #   in Loop: Header=BB75_12 Depth=1
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movl	(%rax,%rbx,4), %ebx
	addl	$-55, %ebx
	jmp	.LBB75_22
.LBB75_21:                              #   in Loop: Header=BB75_12 Depth=1
	addl	$-48, %ebx
.LBB75_22:                              #   in Loop: Header=BB75_12 Depth=1
	addq	$2, %r15
	addl	%ebp, %ebx
	jmp	.LBB75_11
.LBB75_25:
	xorl	%r14d, %r14d
	jmp	.LBB75_26
.LBB75_23:
	movb	$0, (%r13)
.LBB75_26:                              # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end75:
	.size	url_decode, .Lfunc_end75-url_decode
	.cfi_endproc

	.globl	html_encode
	.p2align	4, 0x90
	.type	html_encode,@function
html_encode:                            # @html_encode
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi451:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi452:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi453:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi454:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi455:
	.cfi_def_cfa_offset 48
.Lcfi456:
	.cfi_offset %rbx, -40
.Lcfi457:
	.cfi_offset %r12, -32
.Lcfi458:
	.cfi_offset %r14, -24
.Lcfi459:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB76_1
# BB#2:
	movswl	2(%r14), %eax
	cmpl	$13, %eax
	je	.LBB76_4
# BB#3:
	cmpl	$3, %eax
	jne	.LBB76_33
.LBB76_4:
	movq	%r14, %rdi
	callq	get_c_string
	movq	%rax, %r12
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %r15
	testq	%r15, %r15
	jle	.LBB76_5
# BB#10:                                # %.lr.ph60.preheader
	testb	$1, %r15b
	jne	.LBB76_12
# BB#11:
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	cmpq	$1, %r15
	jne	.LBB76_18
	jmp	.LBB76_6
.LBB76_1:
	xorl	%r14d, %r14d
	jmp	.LBB76_33
.LBB76_5:
	xorl	%edi, %edi
	cmpq	%rdi, %r15
	jne	.LBB76_7
	jmp	.LBB76_33
.LBB76_12:                              # %.lr.ph60.prol
	movsbl	(%r12), %eax
	addl	$-34, %eax
	cmpl	$28, %eax
	ja	.LBB76_16
# BB#13:                                # %.lr.ph60.prol
	movl	$1, %ecx
	movl	$5, %edi
	jmpq	*.LJTI76_0(,%rax,8)
.LBB76_15:
	movl	$1, %ecx
	movl	$4, %edi
	cmpq	$1, %r15
	jne	.LBB76_18
	jmp	.LBB76_6
.LBB76_14:
	movl	$1, %ecx
	movl	$6, %edi
	cmpq	$1, %r15
	jne	.LBB76_18
	jmp	.LBB76_6
.LBB76_16:
	movl	$1, %edi
	movl	$1, %ecx
.LBB76_17:                              # %.lr.ph60.prol.loopexit
	cmpq	$1, %r15
	je	.LBB76_6
.LBB76_18:                              # %.lr.ph60.preheader.new
	movq	%r15, %rax
	subq	%rcx, %rax
	leaq	1(%r12,%rcx), %rcx
	.p2align	4, 0x90
.LBB76_19:                              # %.lr.ph60
                                        # =>This Inner Loop Header: Depth=1
	movsbl	-1(%rcx), %esi
	addl	$-34, %esi
	cmpl	$28, %esi
	ja	.LBB76_23
# BB#20:                                # %.lr.ph60
                                        #   in Loop: Header=BB76_19 Depth=1
	movl	$5, %edx
	jmpq	*.LJTI76_1(,%rsi,8)
.LBB76_21:                              #   in Loop: Header=BB76_19 Depth=1
	movl	$4, %edx
	jmp	.LBB76_24
	.p2align	4, 0x90
.LBB76_22:                              #   in Loop: Header=BB76_19 Depth=1
	movl	$6, %edx
	jmp	.LBB76_24
.LBB76_23:                              #   in Loop: Header=BB76_19 Depth=1
	movl	$1, %edx
	.p2align	4, 0x90
.LBB76_24:                              # %.lr.ph60.162
                                        #   in Loop: Header=BB76_19 Depth=1
	addq	%rdi, %rdx
	movsbl	(%rcx), %esi
	addl	$-34, %esi
	cmpl	$28, %esi
	ja	.LBB76_36
# BB#25:                                # %.lr.ph60.162
                                        #   in Loop: Header=BB76_19 Depth=1
	movl	$5, %edi
	jmpq	*.LJTI76_2(,%rsi,8)
.LBB76_35:                              #   in Loop: Header=BB76_19 Depth=1
	movl	$4, %edi
	jmp	.LBB76_37
	.p2align	4, 0x90
.LBB76_34:                              #   in Loop: Header=BB76_19 Depth=1
	movl	$6, %edi
	jmp	.LBB76_37
.LBB76_36:                              #   in Loop: Header=BB76_19 Depth=1
	movl	$1, %edi
	.p2align	4, 0x90
.LBB76_37:                              #   in Loop: Header=BB76_19 Depth=1
	addq	%rdx, %rdi
	addq	$2, %rcx
	addq	$-2, %rax
	jne	.LBB76_19
.LBB76_6:                               # %._crit_edge
	cmpq	%rdi, %r15
	je	.LBB76_33
.LBB76_7:
	xorl	%esi, %esi
	callq	strcons
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	get_c_string
	movq	%rax, %rbx
	testq	%r15, %r15
	jle	.LBB76_33
	.p2align	4, 0x90
.LBB76_8:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%r12), %eax
	leal	-34(%rax), %ecx
	cmpl	$28, %ecx
	ja	.LBB76_31
# BB#9:                                 # %.lr.ph
                                        #   in Loop: Header=BB76_8 Depth=1
	jmpq	*.LJTI76_3(,%rcx,8)
.LBB76_30:                              #   in Loop: Header=BB76_8 Depth=1
	movb	$0, 6(%rbx)
	movw	$15220, 4(%rbx)         # imm = 0x3B74
	movl	$1869967654, (%rbx)     # imm = 0x6F757126
	jmp	.LBB76_27
	.p2align	4, 0x90
.LBB76_31:                              #   in Loop: Header=BB76_8 Depth=1
	movb	%al, (%rbx)
	incq	%rbx
	jmp	.LBB76_32
	.p2align	4, 0x90
.LBB76_29:                              #   in Loop: Header=BB76_8 Depth=1
	movw	$59, 4(%rbx)
	movl	$1886216486, (%rbx)     # imm = 0x706D6126
	jmp	.LBB76_27
	.p2align	4, 0x90
.LBB76_28:                              #   in Loop: Header=BB76_8 Depth=1
	movb	$0, 4(%rbx)
	movl	$997485606, (%rbx)      # imm = 0x3B746C26
	jmp	.LBB76_27
	.p2align	4, 0x90
.LBB76_26:                              #   in Loop: Header=BB76_8 Depth=1
	movb	$0, 4(%rbx)
	movl	$997484326, (%rbx)      # imm = 0x3B746726
.LBB76_27:                              #   in Loop: Header=BB76_8 Depth=1
	movq	%rbx, %rdi
	callq	strlen
	addq	%rax, %rbx
.LBB76_32:                              #   in Loop: Header=BB76_8 Depth=1
	incq	%r12
	decq	%r15
	jne	.LBB76_8
.LBB76_33:                              # %.thread
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end76:
	.size	html_encode, .Lfunc_end76-html_encode
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI76_0:
	.quad	.LBB76_14
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_17
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_16
	.quad	.LBB76_15
	.quad	.LBB76_16
	.quad	.LBB76_15
.LJTI76_1:
	.quad	.LBB76_22
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_24
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_23
	.quad	.LBB76_21
	.quad	.LBB76_23
	.quad	.LBB76_21
.LJTI76_2:
	.quad	.LBB76_34
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_37
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_36
	.quad	.LBB76_35
	.quad	.LBB76_36
	.quad	.LBB76_35
.LJTI76_3:
	.quad	.LBB76_30
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_29
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_31
	.quad	.LBB76_28
	.quad	.LBB76_31
	.quad	.LBB76_26

	.text
	.globl	html_decode
	.p2align	4, 0x90
	.type	html_decode,@function
html_decode:                            # @html_decode
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end77:
	.size	html_decode, .Lfunc_end77-html_decode
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI78_0:
	.quad	4656722014701092864     # double 2048
	.text
	.globl	lgets
	.p2align	4, 0x90
	.type	lgets,@function
lgets:                                  # @lgets
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi460:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi461:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi462:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi463:
	.cfi_def_cfa_offset 40
	subq	$2056, %rsp             # imm = 0x808
.Lcfi464:
	.cfi_def_cfa_offset 2096
.Lcfi465:
	.cfi_offset %rbx, -40
.Lcfi466:
	.cfi_offset %r12, -32
.Lcfi467:
	.cfi_offset %r14, -24
.Lcfi468:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	stdin(%rip), %rsi
	callq	get_c_file
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB78_1
# BB#2:
	movq	%rbx, %rdi
	callq	get_c_long
	movq	%rax, %r15
	testq	%r15, %r15
	js	.LBB78_3
# BB#4:
	cmpq	$2049, %r15             # imm = 0x801
	jb	.LBB78_7
# BB#5:
	movsd	.LCPI78_0(%rip), %xmm0  # xmm0 = mem[0],zero
	callq	flocons
	movq	%rax, %rcx
	movl	$2, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%rcx, %rdx
	callq	listn
	movl	$.L.str.89, %edi
	movq	%rax, %rsi
	jmp	.LBB78_6
.LBB78_1:
	movl	$2048, %r15d            # imm = 0x800
	jmp	.LBB78_7
.LBB78_3:
	movl	$.L.str.88, %edi
	movq	%rbx, %rsi
.LBB78_6:
	callq	err
.LBB78_7:
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r12
	movq	%rsp, %rdi
	movl	%r15d, %esi
	movq	%r14, %rdx
	callq	fgets
	movq	%rax, %rbx
	movslq	%r12d, %rdi
	callq	no_interrupt
	testq	%rbx, %rbx
	je	.LBB78_8
# BB#9:
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcons
	jmp	.LBB78_10
.LBB78_8:
	xorl	%eax, %eax
.LBB78_10:
	addq	$2056, %rsp             # imm = 0x808
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end78:
	.size	lgets, .Lfunc_end78-lgets
	.cfi_endproc

	.globl	readline
	.p2align	4, 0x90
	.type	readline,@function
readline:                               # @readline
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi469:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi470:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi471:
	.cfi_def_cfa_offset 32
	subq	$2048, %rsp             # imm = 0x800
.Lcfi472:
	.cfi_def_cfa_offset 2080
.Lcfi473:
	.cfi_offset %rbx, -32
.Lcfi474:
	.cfi_offset %r14, -24
.Lcfi475:
	.cfi_offset %r15, -16
	movq	stdin(%rip), %rsi
	callq	get_c_file
	movq	%rax, %r14
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r15
	movq	%rsp, %rdi
	movl	$2048, %esi             # imm = 0x800
	movq	%r14, %rdx
	callq	fgets
	movq	%rax, %rbx
	movslq	%r15d, %rdi
	callq	no_interrupt
	testq	%rbx, %rbx
	je	.LBB79_5
# BB#1:                                 # %lgets.exit
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcons
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB79_5
# BB#2:
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%rax, %r14
	movl	$10, %esi
	movq	%r14, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB79_6
# BB#3:
	movb	$0, (%rax)
	subq	%r14, %rax
	movq	%rax, 8(%rbx)
	jmp	.LBB79_6
.LBB79_5:
	xorl	%ebx, %ebx
.LBB79_6:
	movq	%rbx, %rax
	addq	$2048, %rsp             # imm = 0x800
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end79:
	.size	readline, .Lfunc_end79-readline
	.cfi_endproc

	.globl	l_chown
	.p2align	4, 0x90
	.type	l_chown,@function
l_chown:                                # @l_chown
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi476:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi477:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi478:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi479:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi480:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi481:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi482:
	.cfi_def_cfa_offset 64
.Lcfi483:
	.cfi_offset %rbx, -56
.Lcfi484:
	.cfi_offset %r12, -48
.Lcfi485:
	.cfi_offset %r13, -40
.Lcfi486:
	.cfi_offset %r14, -32
.Lcfi487:
	.cfi_offset %r15, -24
.Lcfi488:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %r15
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	movq	%r15, %rdi
	callq	get_c_string
	movq	%rax, %rbp
	movq	%r13, %rdi
	callq	get_c_long
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	get_c_long
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movl	%eax, %edx
	callq	chown
	testl	%eax, %eax
	je	.LBB80_2
# BB#1:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	cons
	movl	$.L.str.90, %edi
	movq	%rax, %rsi
	callq	err
.LBB80_2:
	movq	%r14, %rdi
	callq	no_interrupt
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end80:
	.size	l_chown, .Lfunc_end80-l_chown
	.cfi_endproc

	.globl	popen_l
	.p2align	4, 0x90
	.type	popen_l,@function
popen_l:                                # @popen_l
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi489:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi490:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi491:
	.cfi_def_cfa_offset 32
.Lcfi492:
	.cfi_offset %rbx, -24
.Lcfi493:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	callq	get_c_string
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB81_1
# BB#2:
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%rax, %rdx
	jmp	.LBB81_3
.LBB81_1:
	movl	$.L.str.91, %edx
.LBB81_3:
	movl	$popen, %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	fopen_cg                # TAILCALL
.Lfunc_end81:
	.size	popen_l, .Lfunc_end81-popen_l
	.cfi_endproc

	.globl	pclose_l
	.p2align	4, 0x90
	.type	pclose_l,@function
pclose_l:                               # @pclose_l
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi494:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi495:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi496:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi497:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi498:
	.cfi_def_cfa_offset 48
.Lcfi499:
	.cfi_offset %rbx, -40
.Lcfi500:
	.cfi_offset %r14, -32
.Lcfi501:
	.cfi_offset %r15, -24
.Lcfi502:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	xorl	%esi, %esi
	callq	get_c_file
	movq	%rax, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	pclose
	movl	%eax, %ebx
	callq	__errno_location
	movl	(%rax), %r14d
	movq	$0, 8(%rbp)
	movq	16(%rbp), %rdi
	callq	free
	movq	$0, 16(%rbp)
	movq	%r15, %rdi
	callq	no_interrupt
	testl	%ebx, %ebx
	jns	.LBB82_2
# BB#1:
	movl	%r14d, %edi
	callq	llast_c_errmsg
	movl	$.L.str.92, %edi
	movq	%rax, %rsi
	callq	err
.LBB82_2:
	cvtsi2sdl	%ebx, %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	flocons                 # TAILCALL
.Lfunc_end82:
	.size	pclose_l, .Lfunc_end82-pclose_l
	.cfi_endproc

	.globl	so_init_name
	.p2align	4, 0x90
	.type	so_init_name,@function
so_init_name:                           # @so_init_name
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi503:
	.cfi_def_cfa_offset 16
.Lcfi504:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	jne	.LBB83_2
# BB#1:
	movl	$.L.str.93, %edi
	callq	cintern
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	lstrbreakup
	movq	%rax, %rdi
	callq	last
	movq	%rax, %rdi
	callq	car
	movq	%rax, %rbx
	movl	$.L.str.94, %edi
	callq	cintern
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	lstrbreakup
	movq	%rax, %rdi
	callq	butlast
	movq	%rax, %rbx
	movl	$.L.str.94, %edi
	callq	cintern
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	lstrunbreakup
	movq	%rax, %rbx
	movl	$.L.str.95, %edi
	callq	cintern
	movq	%rax, %rcx
	movl	$2, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	movq	%rbx, %rdx
	callq	listn
	movq	%rax, %rdi
	callq	string_append
	movq	%rax, %rsi
.LBB83_2:
	movq	%rsi, %rdi
	popq	%rbx
	jmp	intern                  # TAILCALL
.Lfunc_end83:
	.size	so_init_name, .Lfunc_end83-so_init_name
	.cfi_endproc

	.globl	so_ext
	.p2align	4, 0x90
	.type	so_ext,@function
so_ext:                                 # @so_ext
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi505:
	.cfi_def_cfa_offset 16
.Lcfi506:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$3, %edi
	movl	$.L.str.96, %esi
	callq	strcons
	movq	%rax, %rcx
	testq	%rbx, %rbx
	je	.LBB84_1
# BB#2:
	movl	$2, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%rcx, %rdx
	callq	listn
	movq	%rax, %rdi
	popq	%rbx
	jmp	string_append           # TAILCALL
.LBB84_1:
	movq	%rcx, %rax
	popq	%rbx
	retq
.Lfunc_end84:
	.size	so_ext, .Lfunc_end84-so_ext
	.cfi_endproc

	.globl	load_so
	.p2align	4, 0x90
	.type	load_so,@function
load_so:                                # @load_so
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi507:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi508:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi509:
	.cfi_def_cfa_offset 32
.Lcfi510:
	.cfi_offset %rbx, -32
.Lcfi511:
	.cfi_offset %r14, -24
.Lcfi512:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	callq	so_init_name
	movq	%rax, %r15
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	movl	$3, %edi
	callq	siod_verbose_check
	testl	%eax, %eax
	je	.LBB85_2
# BB#1:
	movl	$.L.str.97, %edi
	callq	put_st
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%rax, %rdi
	callq	put_st
	movl	$.L.str.98, %edi
	callq	put_st
.LBB85_2:
	movl	$.L.str.99, %edi
	movq	%r15, %rsi
	callq	err
	movq	%r14, %rdi
	callq	no_interrupt
	movl	$3, %edi
	callq	siod_verbose_check
	testl	%eax, %eax
	je	.LBB85_4
# BB#3:
	movl	$.L.str.100, %edi
	callq	put_st
.LBB85_4:
	movq	%r15, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end85:
	.size	load_so, .Lfunc_end85-load_so
	.cfi_endproc

	.globl	require_so
	.p2align	4, 0x90
	.type	require_so,@function
require_so:                             # @require_so
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi513:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi514:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi515:
	.cfi_def_cfa_offset 32
.Lcfi516:
	.cfi_offset %rbx, -24
.Lcfi517:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	xorl	%esi, %esi
	callq	so_init_name
	movq	%rax, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	symbol_boundp
	testq	%rax, %rax
	je	.LBB86_3
# BB#1:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	symbol_value
	testq	%rax, %rax
	je	.LBB86_3
# BB#2:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB86_3:
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	load_so
	callq	a_true_value
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	setvar                  # TAILCALL
.Lfunc_end86:
	.size	require_so, .Lfunc_end86-require_so
	.cfi_endproc

	.globl	siod_lib_l
	.p2align	4, 0x90
	.type	siod_lib_l,@function
siod_lib_l:                             # @siod_lib_l
	.cfi_startproc
# BB#0:
	movq	siod_lib(%rip), %rdi
	jmp	rintern                 # TAILCALL
.Lfunc_end87:
	.size	siod_lib_l, .Lfunc_end87-siod_lib_l
	.cfi_endproc

	.globl	ccall_catch_1
	.p2align	4, 0x90
	.type	ccall_catch_1,@function
ccall_catch_1:                          # @ccall_catch_1
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi518:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rax
	movq	%rsi, %rdi
	callq	*%rax
	movq	catch_framep(%rip), %rcx
	movq	216(%rcx), %rcx
	movq	%rcx, catch_framep(%rip)
	popq	%rcx
	retq
.Lfunc_end88:
	.size	ccall_catch_1, .Lfunc_end88-ccall_catch_1
	.cfi_endproc

	.globl	ccall_catch
	.p2align	4, 0x90
	.type	ccall_catch,@function
ccall_catch:                            # @ccall_catch
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi519:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi520:
	.cfi_def_cfa_offset 24
	subq	$232, %rsp
.Lcfi521:
	.cfi_def_cfa_offset 256
.Lcfi522:
	.cfi_offset %rbx, -24
.Lcfi523:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, 8(%rsp)
	movq	catch_framep(%rip), %rax
	movq	%rax, 224(%rsp)
	leaq	24(%rsp), %rdi
	callq	_setjmp
	leaq	8(%rsp), %rcx
	movq	%rcx, catch_framep(%rip)
	cmpl	$2, %eax
	jne	.LBB89_2
# BB#1:
	movq	224(%rsp), %rax
	movq	%rax, catch_framep(%rip)
	movq	16(%rsp), %rax
	jmp	.LBB89_3
.LBB89_2:
	movq	%r14, %rdi
	callq	*%rbx
	movq	catch_framep(%rip), %rcx
	movq	216(%rcx), %rcx
	movq	%rcx, catch_framep(%rip)
.LBB89_3:
	addq	$232, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end89:
	.size	ccall_catch, .Lfunc_end89-ccall_catch
	.cfi_endproc

	.globl	decode_tm
	.p2align	4, 0x90
	.type	decode_tm,@function
decode_tm:                              # @decode_tm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi524:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi525:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi526:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi527:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi528:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi529:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi530:
	.cfi_def_cfa_offset 80
.Lcfi531:
	.cfi_offset %rbx, -56
.Lcfi532:
	.cfi_offset %r12, -48
.Lcfi533:
	.cfi_offset %r13, -40
.Lcfi534:
	.cfi_offset %r14, -32
.Lcfi535:
	.cfi_offset %r15, -24
.Lcfi536:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movl	(%rbp), %eax
	cvtsi2sdl	%eax, %xmm0
	callq	flocons
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	4(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	callq	flocons
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	8(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	callq	flocons
	movq	%rax, %rbx
	movl	12(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	callq	flocons
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	16(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	callq	flocons
	movq	%rax, %r13
	movl	20(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	callq	flocons
	movq	%rax, %r14
	movl	24(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	callq	flocons
	movq	%rax, %r15
	movl	28(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	callq	flocons
	movq	%rax, %r12
	movl	32(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	callq	flocons
	movq	%rax, %rbp
	subq	$8, %rsp
.Lcfi537:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.101, %edi
	movl	$.L.str.102, %edx
	movl	$.L.str.103, %r8d
	movl	$0, %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rbx, %r9
	pushq	$0
.Lcfi538:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi539:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.109
.Lcfi540:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi541:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.108
.Lcfi542:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi543:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.107
.Lcfi544:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi545:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.106
.Lcfi546:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi547:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.105
.Lcfi548:
	.cfi_adjust_cfa_offset 8
	pushq	112(%rsp)               # 8-byte Folded Reload
.Lcfi549:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.104
.Lcfi550:
	.cfi_adjust_cfa_offset 8
	callq	symalist
	addq	$136, %rsp
.Lcfi551:
	.cfi_adjust_cfa_offset -112
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end90:
	.size	decode_tm, .Lfunc_end90-decode_tm
	.cfi_endproc

	.globl	encode_tm
	.p2align	4, 0x90
	.type	encode_tm,@function
encode_tm:                              # @encode_tm
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi552:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi553:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi554:
	.cfi_def_cfa_offset 32
.Lcfi555:
	.cfi_offset %rbx, -32
.Lcfi556:
	.cfi_offset %r14, -24
.Lcfi557:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$.L.str.101, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	assq
	movq	%rax, %rdi
	callq	cdr
	xorl	%ebx, %ebx
	testq	%rax, %rax
	movl	$0, %ecx
	je	.LBB91_2
# BB#1:
	movq	%rax, %rdi
	callq	get_c_long
	movq	%rax, %rcx
.LBB91_2:
	movl	%ecx, (%r14)
	movl	$.L.str.102, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	assq
	movq	%rax, %rdi
	callq	cdr
	testq	%rax, %rax
	je	.LBB91_4
# BB#3:
	movq	%rax, %rdi
	callq	get_c_long
	movq	%rax, %rbx
.LBB91_4:
	movl	%ebx, 4(%r14)
	movl	$.L.str.103, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	assq
	movq	%rax, %rdi
	callq	cdr
	xorl	%ebx, %ebx
	testq	%rax, %rax
	movl	$0, %ecx
	je	.LBB91_6
# BB#5:
	movq	%rax, %rdi
	callq	get_c_long
	movq	%rax, %rcx
.LBB91_6:
	movl	%ecx, 8(%r14)
	movl	$.L.str.104, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	assq
	movq	%rax, %rdi
	callq	cdr
	testq	%rax, %rax
	je	.LBB91_8
# BB#7:
	movq	%rax, %rdi
	callq	get_c_long
	movq	%rax, %rbx
.LBB91_8:
	movl	%ebx, 12(%r14)
	movl	$.L.str.105, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	assq
	movq	%rax, %rdi
	callq	cdr
	xorl	%ebx, %ebx
	testq	%rax, %rax
	movl	$0, %ecx
	je	.LBB91_10
# BB#9:
	movq	%rax, %rdi
	callq	get_c_long
	movq	%rax, %rcx
.LBB91_10:
	movl	%ecx, 16(%r14)
	movl	$.L.str.106, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	assq
	movq	%rax, %rdi
	callq	cdr
	testq	%rax, %rax
	je	.LBB91_12
# BB#11:
	movq	%rax, %rdi
	callq	get_c_long
	movq	%rax, %rbx
.LBB91_12:
	movl	%ebx, 20(%r14)
	movl	$.L.str.107, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	assq
	movq	%rax, %rdi
	callq	cdr
	xorl	%ebx, %ebx
	testq	%rax, %rax
	movl	$0, %ecx
	je	.LBB91_14
# BB#13:
	movq	%rax, %rdi
	callq	get_c_long
	movq	%rax, %rcx
.LBB91_14:
	movl	%ecx, 24(%r14)
	movl	$.L.str.108, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	assq
	movq	%rax, %rdi
	callq	cdr
	testq	%rax, %rax
	je	.LBB91_16
# BB#15:
	movq	%rax, %rdi
	callq	get_c_long
	movq	%rax, %rbx
.LBB91_16:
	movl	%ebx, 28(%r14)
	movl	$.L.str.109, %edi
	callq	cintern
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	assq
	movq	%rax, %rdi
	callq	cdr
	testq	%rax, %rax
	je	.LBB91_17
# BB#18:
	movq	%rax, %rdi
	callq	get_c_long
	jmp	.LBB91_19
.LBB91_17:
	movl	$-1, %eax
.LBB91_19:
	movl	%eax, 32(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end91:
	.size	encode_tm, .Lfunc_end91-encode_tm
	.cfi_endproc

	.globl	llocaltime
	.p2align	4, 0x90
	.type	llocaltime,@function
llocaltime:                             # @llocaltime
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi558:
	.cfi_def_cfa_offset 16
	testq	%rdi, %rdi
	je	.LBB92_2
# BB#1:
	callq	get_c_long
	movq	%rax, (%rsp)
	jmp	.LBB92_3
.LBB92_2:
	movq	%rsp, %rdi
	callq	time
.LBB92_3:
	movq	%rsp, %rdi
	callq	localtime
	testq	%rax, %rax
	je	.LBB92_5
# BB#4:
	movq	%rax, %rdi
	callq	decode_tm
	popq	%rcx
	retq
.LBB92_5:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.110, %edi
	movq	%rax, %rsi
	callq	err
	popq	%rcx
	retq
.Lfunc_end92:
	.size	llocaltime, .Lfunc_end92-llocaltime
	.cfi_endproc

	.globl	lgmtime
	.p2align	4, 0x90
	.type	lgmtime,@function
lgmtime:                                # @lgmtime
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi559:
	.cfi_def_cfa_offset 16
	testq	%rdi, %rdi
	je	.LBB93_2
# BB#1:
	callq	get_c_long
	movq	%rax, (%rsp)
	jmp	.LBB93_3
.LBB93_2:
	movq	%rsp, %rdi
	callq	time
.LBB93_3:
	movq	%rsp, %rdi
	callq	gmtime
	testq	%rax, %rax
	je	.LBB93_5
# BB#4:
	movq	%rax, %rdi
	callq	decode_tm
	popq	%rcx
	retq
.LBB93_5:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.111, %edi
	movq	%rax, %rsi
	callq	err
	popq	%rcx
	retq
.Lfunc_end93:
	.size	lgmtime, .Lfunc_end93-lgmtime
	.cfi_endproc

	.globl	ltzset
	.p2align	4, 0x90
	.type	ltzset,@function
ltzset:                                 # @ltzset
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi560:
	.cfi_def_cfa_offset 16
	callq	tzset
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end94:
	.size	ltzset, .Lfunc_end94-ltzset
	.cfi_endproc

	.globl	lmktime
	.p2align	4, 0x90
	.type	lmktime,@function
lmktime:                                # @lmktime
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi561:
	.cfi_def_cfa_offset 16
	subq	$64, %rsp
.Lcfi562:
	.cfi_def_cfa_offset 80
.Lcfi563:
	.cfi_offset %rbx, -16
	leaq	8(%rsp), %rbx
	movq	%rbx, %rsi
	callq	encode_tm
	movq	%rbx, %rdi
	callq	mktime
	cvtsi2sdq	%rax, %xmm0
	callq	flocons
	addq	$64, %rsp
	popq	%rbx
	retq
.Lfunc_end95:
	.size	lmktime, .Lfunc_end95-lmktime
	.cfi_endproc

	.globl	lstrptime
	.p2align	4, 0x90
	.type	lstrptime,@function
lstrptime:                              # @lstrptime
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi564:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi565:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi566:
	.cfi_def_cfa_offset 32
	subq	$64, %rsp
.Lcfi567:
	.cfi_def_cfa_offset 96
.Lcfi568:
	.cfi_offset %rbx, -32
.Lcfi569:
	.cfi_offset %r14, -24
.Lcfi570:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	8(%rsp), %r15
	movq	%rdx, %rdi
	movq	%r15, %rsi
	callq	encode_tm
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	get_c_string
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%r15, %rdx
	callq	strptime
	testq	%rax, %rax
	je	.LBB96_1
# BB#2:
	leaq	8(%rsp), %rdi
	callq	decode_tm
	jmp	.LBB96_3
.LBB96_1:
	xorl	%eax, %eax
.LBB96_3:
	addq	$64, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end96:
	.size	lstrptime, .Lfunc_end96-lstrptime
	.cfi_endproc

	.globl	lstrftime
	.p2align	4, 0x90
	.type	lstrftime,@function
lstrftime:                              # @lstrftime
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi571:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi572:
	.cfi_def_cfa_offset 24
	subq	$1096, %rsp             # imm = 0x448
.Lcfi573:
	.cfi_def_cfa_offset 1120
.Lcfi574:
	.cfi_offset %rbx, -24
.Lcfi575:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%rsi, %rsi
	je	.LBB97_2
# BB#1:
	leaq	8(%rsp), %rbx
	movq	%rsi, %rdi
	movq	%rbx, %rsi
	callq	encode_tm
	jmp	.LBB97_3
.LBB97_2:
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	callq	time
	movq	%rbx, %rdi
	callq	gmtime
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB97_6
.LBB97_3:
	movq	%r14, %rdi
	callq	get_c_string
	leaq	64(%rsp), %rdi
	movl	$1024, %esi             # imm = 0x400
	movq	%rax, %rdx
	movq	%rbx, %rcx
	callq	strftime
	testq	%rax, %rax
	je	.LBB97_6
# BB#4:
	leaq	64(%rsp), %rsi
	movq	%rax, %rdi
	callq	strcons
	jmp	.LBB97_7
.LBB97_6:
	xorl	%eax, %eax
.LBB97_7:
	addq	$1096, %rsp             # imm = 0x448
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end97:
	.size	lstrftime, .Lfunc_end97-lstrftime
	.cfi_endproc

	.globl	lchdir
	.p2align	4, 0x90
	.type	lchdir,@function
lchdir:                                 # @lchdir
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi576:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi577:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi578:
	.cfi_def_cfa_offset 32
.Lcfi579:
	.cfi_offset %rbx, -24
.Lcfi580:
	.cfi_offset %rbp, -16
	testq	%rdi, %rdi
	je	.LBB98_5
# BB#1:
	movzwl	2(%rdi), %eax
	cmpl	$17, %eax
	jne	.LBB98_5
# BB#2:
	xorl	%esi, %esi
	callq	get_c_file
	movq	%rax, %rdi
	callq	fileno
	movl	%eax, %ebp
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %rbx
	movl	%ebp, %edi
	callq	fchdir
	testl	%eax, %eax
	je	.LBB98_7
# BB#3:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.112, %edi
	jmp	.LBB98_4
.LBB98_5:                               # %.critedge
	callq	get_c_string
	movq	%rax, %rbp
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	chdir
	testl	%eax, %eax
	je	.LBB98_7
# BB#6:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.113, %edi
.LBB98_4:
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	err                     # TAILCALL
.LBB98_7:
	movq	%rbx, %rdi
	callq	no_interrupt
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end98:
	.size	lchdir, .Lfunc_end98-lchdir
	.cfi_endproc

	.globl	lgetpass
	.p2align	4, 0x90
	.type	lgetpass,@function
lgetpass:                               # @lgetpass
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi581:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi582:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi583:
	.cfi_def_cfa_offset 32
.Lcfi584:
	.cfi_offset %rbx, -24
.Lcfi585:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB99_1
# BB#2:
	movq	%rbx, %rdi
	callq	get_c_string
	movq	%rax, %rdi
	jmp	.LBB99_3
.LBB99_1:
	movl	$.L.str.114, %edi
.LBB99_3:
	callq	getpass
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	no_interrupt
	testq	%rbx, %rbx
	je	.LBB99_4
# BB#5:
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%rbx, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	strcons                 # TAILCALL
.LBB99_4:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end99:
	.size	lgetpass, .Lfunc_end99-lgetpass
	.cfi_endproc

	.globl	lpipe
	.p2align	4, 0x90
	.type	lpipe,@function
lpipe:                                  # @lpipe
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi586:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi587:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi588:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi589:
	.cfi_def_cfa_offset 48
.Lcfi590:
	.cfi_offset %rbx, -32
.Lcfi591:
	.cfi_offset %r14, -24
.Lcfi592:
	.cfi_offset %r15, -16
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %r14
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	cons
	movq	%rax, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r15
	leaq	8(%rsp), %rdi
	callq	pipe
	testl	%eax, %eax
	je	.LBB100_1
# BB#2:
	movl	$-1, %edi
	callq	llast_c_errmsg
	movl	$.L.str.116, %edi
	movq	%rax, %rsi
	callq	err
	jmp	.LBB100_3
.LBB100_1:
	movw	$17, 2(%r14)
	movl	8(%rsp), %edi
	movl	$.L.str.91, %esi
	callq	fdopen
	movq	%rax, 8(%r14)
	movw	$17, 2(%rbx)
	movl	12(%rsp), %edi
	movl	$.L.str.115, %esi
	callq	fdopen
	movq	%rax, 8(%rbx)
	movq	%r15, %rdi
	callq	no_interrupt
	movl	$2, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	listn
.LBB100_3:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end100:
	.size	lpipe, .Lfunc_end100-lpipe
	.cfi_endproc

	.globl	err_large_index
	.p2align	4, 0x90
	.type	err_large_index,@function
err_large_index:                        # @err_large_index
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movl	$.L.str.117, %edi
	movq	%rax, %rsi
	jmp	err                     # TAILCALL
.Lfunc_end101:
	.size	err_large_index, .Lfunc_end101-err_large_index
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI102_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI102_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.text
	.globl	datref
	.p2align	4, 0x90
	.type	datref,@function
datref:                                 # @datref
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi593:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi594:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi595:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi596:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi597:
	.cfi_def_cfa_offset 48
.Lcfi598:
	.cfi_offset %rbx, -40
.Lcfi599:
	.cfi_offset %r12, -32
.Lcfi600:
	.cfi_offset %r14, -24
.Lcfi601:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rsp, %rsi
	callq	get_c_string_dim
	movq	%rax, %r14
	movq	%r15, %rdi
	callq	get_c_long
	movq	%rax, %r12
	testq	%r12, %r12
	jns	.LBB102_2
# BB#1:
	movl	$.L.str.118, %edi
	movq	%r15, %rsi
	callq	err
.LBB102_2:
	movq	%rbx, %rdi
	callq	get_c_long
	decq	%rax
	cmpq	$9, %rax
	ja	.LBB102_7
# BB#3:
	jmpq	*.LJTI102_0(,%rax,8)
.LBB102_4:
	leaq	4(,%r12,4), %rax
	cmpq	(%rsp), %rax
	jle	.LBB102_6
# BB#5:
	movl	$.L.str.117, %edi
	movq	%r15, %rsi
	callq	err
.LBB102_6:
	movss	(%r14,%r12,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	jmp	.LBB102_37
.LBB102_7:
	movl	$.L.str.119, %edi
	movq	%rbx, %rsi
	callq	err
	jmp	.LBB102_38
.LBB102_8:
	leaq	8(,%r12,8), %rax
	cmpq	(%rsp), %rax
	jle	.LBB102_10
# BB#9:
	movl	$.L.str.117, %edi
	movq	%r15, %rsi
	callq	err
.LBB102_10:
	movsd	(%r14,%r12,8), %xmm0    # xmm0 = mem[0],zero
	jmp	.LBB102_37
.LBB102_11:
	cmpq	(%rsp), %r12
	jl	.LBB102_13
# BB#12:
	movl	$.L.str.117, %edi
	movq	%r15, %rsi
	callq	err
.LBB102_13:
	movsbl	(%r14,%r12), %eax
	jmp	.LBB102_26
.LBB102_14:
	cmpq	(%rsp), %r12
	jl	.LBB102_16
# BB#15:
	movl	$.L.str.117, %edi
	movq	%r15, %rsi
	callq	err
.LBB102_16:
	movzbl	(%r14,%r12), %eax
	jmp	.LBB102_26
.LBB102_17:
	leaq	2(%r12,%r12), %rax
	cmpq	(%rsp), %rax
	jle	.LBB102_19
# BB#18:
	movl	$.L.str.117, %edi
	movq	%r15, %rsi
	callq	err
.LBB102_19:
	movswl	(%r14,%r12,2), %eax
	jmp	.LBB102_26
.LBB102_20:
	leaq	2(%r12,%r12), %rax
	cmpq	(%rsp), %rax
	jle	.LBB102_22
# BB#21:
	movl	$.L.str.117, %edi
	movq	%r15, %rsi
	callq	err
.LBB102_22:
	movzwl	(%r14,%r12,2), %eax
	jmp	.LBB102_26
.LBB102_23:
	leaq	4(,%r12,4), %rax
	cmpq	(%rsp), %rax
	jle	.LBB102_25
# BB#24:
	movl	$.L.str.117, %edi
	movq	%r15, %rsi
	callq	err
.LBB102_25:
	movl	(%r14,%r12,4), %eax
.LBB102_26:
	cvtsi2sdl	%eax, %xmm0
	jmp	.LBB102_37
.LBB102_27:
	leaq	4(,%r12,4), %rax
	cmpq	(%rsp), %rax
	jle	.LBB102_29
# BB#28:
	movl	$.L.str.117, %edi
	movq	%r15, %rsi
	callq	err
.LBB102_29:
	movl	(%r14,%r12,4), %eax
	jmp	.LBB102_36
.LBB102_30:
	leaq	8(,%r12,8), %rax
	cmpq	(%rsp), %rax
	jle	.LBB102_32
# BB#31:
	movl	$.L.str.117, %edi
	movq	%r15, %rsi
	callq	err
.LBB102_32:
	movq	(%r14,%r12,8), %xmm1    # xmm1 = mem[0],zero
	punpckldq	.LCPI102_0(%rip), %xmm1 # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	subpd	.LCPI102_1(%rip), %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	jmp	.LBB102_37
.LBB102_33:
	leaq	8(,%r12,8), %rax
	cmpq	(%rsp), %rax
	jle	.LBB102_35
# BB#34:
	movl	$.L.str.117, %edi
	movq	%r15, %rsi
	callq	err
.LBB102_35:
	movq	(%r14,%r12,8), %rax
.LBB102_36:
	cvtsi2sdq	%rax, %xmm0
.LBB102_37:
	callq	flocons
.LBB102_38:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end102:
	.size	datref, .Lfunc_end102-datref
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI102_0:
	.quad	.LBB102_4
	.quad	.LBB102_8
	.quad	.LBB102_11
	.quad	.LBB102_14
	.quad	.LBB102_17
	.quad	.LBB102_20
	.quad	.LBB102_23
	.quad	.LBB102_27
	.quad	.LBB102_33
	.quad	.LBB102_30

	.text
	.globl	sdatref
	.p2align	4, 0x90
	.type	sdatref,@function
sdatref:                                # @sdatref
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi602:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi603:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi604:
	.cfi_def_cfa_offset 32
.Lcfi605:
	.cfi_offset %rbx, -32
.Lcfi606:
	.cfi_offset %r14, -24
.Lcfi607:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	car
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	cdr
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	datref                  # TAILCALL
.Lfunc_end103:
	.size	sdatref, .Lfunc_end103-sdatref
	.cfi_endproc

	.globl	mkdatref
	.p2align	4, 0x90
	.type	mkdatref,@function
mkdatref:                               # @mkdatref
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi608:
	.cfi_def_cfa_offset 16
.Lcfi609:
	.cfi_offset %rbx, -16
	callq	cons
	movq	%rax, %rbx
	movl	$.L.str.120, %edi
	callq	cintern
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	leval
	movq	%rbx, %rdi
	movq	%rax, %rsi
	popq	%rbx
	jmp	closure                 # TAILCALL
.Lfunc_end104:
	.size	mkdatref, .Lfunc_end104-mkdatref
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI105_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI105_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.text
	.globl	datlength
	.p2align	4, 0x90
	.type	datlength,@function
datlength:                              # @datlength
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi610:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi611:
	.cfi_def_cfa_offset 32
.Lcfi612:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	leaq	8(%rsp), %rsi
	callq	get_c_string_dim
	movq	%rbx, %rdi
	callq	get_c_long
	decq	%rax
	cmpq	$9, %rax
	ja	.LBB105_9
# BB#1:
	jmpq	*.LJTI105_0(,%rax,8)
.LBB105_2:
	movq	8(%rsp), %rax
	shrq	$2, %rax
	jmp	.LBB105_6
.LBB105_3:
	movq	8(%rsp), %rax
	shrq	$3, %rax
	jmp	.LBB105_6
.LBB105_4:
	movq	8(%rsp), %xmm1          # xmm1 = mem[0],zero
	punpckldq	.LCPI105_0(%rip), %xmm1 # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	subpd	.LCPI105_1(%rip), %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
	jmp	.LBB105_7
.LBB105_5:
	movq	8(%rsp), %rax
	shrq	%rax
.LBB105_6:
	cvtsi2sdq	%rax, %xmm0
.LBB105_7:
	callq	flocons
.LBB105_8:
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB105_9:
	movl	$.L.str.119, %edi
	movq	%rbx, %rsi
	callq	err
	jmp	.LBB105_8
.Lfunc_end105:
	.size	datlength, .Lfunc_end105-datlength
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI105_0:
	.quad	.LBB105_2
	.quad	.LBB105_3
	.quad	.LBB105_4
	.quad	.LBB105_4
	.quad	.LBB105_5
	.quad	.LBB105_5
	.quad	.LBB105_2
	.quad	.LBB105_2
	.quad	.LBB105_3
	.quad	.LBB105_3

	.text
	.globl	siod_main
	.p2align	4, 0x90
	.type	siod_main,@function
siod_main:                              # @siod_main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi613:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi614:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi615:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi616:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi617:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi618:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi619:
	.cfi_def_cfa_offset 128
.Lcfi620:
	.cfi_offset %rbx, -56
.Lcfi621:
	.cfi_offset %r12, -48
.Lcfi622:
	.cfi_offset %r13, -40
.Lcfi623:
	.cfi_offset %r14, -32
.Lcfi624:
	.cfi_offset %r15, -24
.Lcfi625:
	.cfi_offset %rbp, -16
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	$.L.str.114, 32(%rsp)
	xorl	%ebx, %ebx
	movl	%edi, 12(%rsp)          # 4-byte Spill
	cmpl	$2, %edi
	movl	$0, %r12d
	movl	$0, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jl	.LBB106_17
# BB#1:                                 # %.lr.ph115
	movl	12(%rsp), %ecx          # 4-byte Reload
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$1, %r13d
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB106_2:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB106_4 Depth 2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r13,8), %r15
	cmpb	$45, (%r15)
	jne	.LBB106_15
# BB#3:                                 # %.lr.ph107.preheader
                                        #   in Loop: Header=BB106_2 Depth=1
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB106_4:                              # %.lr.ph107
                                        #   Parent Loop BB106_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$.L.str.121, %esi
	movq	%r15, %rdi
	callq	strstr
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB106_6
# BB#5:                                 #   in Loop: Header=BB106_4 Depth=2
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbp
	addq	%r15, %rbp
.LBB106_6:                              #   in Loop: Header=BB106_4 Depth=2
	movq	%rbp, %rbx
	subq	%r15, %rbx
	leaq	1(%rbx), %rdi
	callq	malloc
	movq	%rax, %r14
	movq	%r14, 40(%rsp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movb	$0, (%r14,%rbx)
	movl	$.L.str.122, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB106_7
.LBB106_10:                             #   in Loop: Header=BB106_4 Depth=2
	movq	40(%rsp), %rbx
	movl	$.L.str.124, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB106_11
# BB#12:                                #   in Loop: Header=BB106_4 Depth=2
	movl	$2, %edi
	movl	$1, %edx
	leaq	32(%rsp), %rsi
	callq	process_cla
	jmp	.LBB106_13
	.p2align	4, 0x90
.LBB106_7:                              #   in Loop: Header=BB106_4 Depth=2
	addq	$2, %r14
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r14, %rdi
	callq	strtol
	testq	%rax, %rax
	jle	.LBB106_10
# BB#8:                                 #   in Loop: Header=BB106_4 Depth=2
	cmpb	$48, (%r14)
	je	.LBB106_10
# BB#9:                                 #   in Loop: Header=BB106_4 Depth=2
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$1, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB106_10
	.p2align	4, 0x90
.LBB106_11:                             #   in Loop: Header=BB106_4 Depth=2
	addq	$2, %rbx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, %r12
.LBB106_13:                             #   in Loop: Header=BB106_4 Depth=2
	cmpb	$0, (%rbp)
	leaq	1(%rbp), %r15
	cmoveq	%rbp, %r15
	cmpb	$0, (%r15)
	jne	.LBB106_4
# BB#14:                                #   in Loop: Header=BB106_2 Depth=1
	movl	8(%rsp), %ebx           # 4-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB106_16
	.p2align	4, 0x90
.LBB106_15:                             #   in Loop: Header=BB106_2 Depth=1
	incl	%ebx
.LBB106_16:                             # %.loopexit
                                        #   in Loop: Header=BB106_2 Depth=1
	incq	%r13
	cmpq	%rcx, %r13
	jne	.LBB106_2
.LBB106_17:                             # %._crit_edge116
	callq	print_welcome
	callq	print_hs_1
	callq	init_storage
	movl	12(%rsp), %r13d         # 4-byte Reload
	testl	%r13d, %r13d
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	jle	.LBB106_18
# BB#19:                                # %.lr.ph101.preheader
	movl	%r13d, %ebx
	xorl	%r14d, %r14d
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB106_20:                             # %.lr.ph101
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %r15
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	strcons
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	cons
	movq	%rax, %r14
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB106_20
	jmp	.LBB106_21
.LBB106_18:
	xorl	%r14d, %r14d
.LBB106_21:                             # %._crit_edge102
	movl	$.L.str.125, %edi
	callq	cintern
	movq	%rax, %rbp
	movq	%r14, %rdi
	callq	nreverse
	xorl	%r14d, %r14d
	xorl	%edx, %edx
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	setvar
	movq	56(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB106_26
# BB#22:                                # %.lr.ph94.split.preheader
	movq	(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB106_23
# BB#24:                                # %.lr.ph94.split.preheader131
	addq	$8, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB106_25:                             # %.lr.ph94.split
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	strcons
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	cons
	movq	%rax, %r14
	movq	(%rbx), %rbp
	addq	$8, %rbx
	testq	%rbp, %rbp
	jne	.LBB106_25
	jmp	.LBB106_26
.LBB106_23:
	xorl	%r14d, %r14d
.LBB106_26:                             # %.critedge
	movl	$.L.str.126, %edi
	callq	cintern
	movq	%rax, %rbp
	movq	%r14, %rdi
	callq	nreverse
	xorl	%r14d, %r14d
	xorl	%edx, %edx
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	setvar
	callq	init_subrs
	callq	init_trace
	callq	init_slibu
	movl	$.L.str.127, %edi
	movl	$cgi_main, %esi
	callq	init_subr_1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB106_38
# BB#27:                                # %.preheader
	cmpl	$1, %r12d
	movl	$3, %eax
	cmovlel	%r13d, %eax
	cmpl	$3, %r13d
	cmovlel	%r13d, %eax
	cmpl	$2, %eax
	jl	.LBB106_33
# BB#28:                                # %.lr.ph.preheader
	movslq	%eax, %rbp
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB106_29:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	cmpb	$45, (%rdi)
	je	.LBB106_32
# BB#30:                                #   in Loop: Header=BB106_29 Depth=1
	callq	htqs_arg
	testl	%eax, %eax
	jne	.LBB106_31
.LBB106_32:                             #   in Loop: Header=BB106_29 Depth=1
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB106_29
	jmp	.LBB106_33
.LBB106_38:
	movl	$1, %edi
	movl	$1, %esi
	xorl	%edx, %edx
	callq	repl_driver
	movq	%rax, %r14
	jmp	.LBB106_35
.LBB106_31:
	movl	%eax, %r14d
.LBB106_33:                             # %._crit_edge
	testl	%r12d, %r12d
	je	.LBB106_35
# BB#34:
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movl	$.L.str.129, %eax
	movl	$.L.str.128, %edi
	cmovneq	%rax, %rdi
	cmpl	$2, %r12d
	cmovleq	%rax, %rdi
	callq	htqs_arg
	movl	%eax, %r14d
.LBB106_35:
	movl	$2, %edi
	callq	siod_verbose_check
	testl	%eax, %eax
	je	.LBB106_37
# BB#36:
	movl	$.Lstr, %edi
	callq	puts
.LBB106_37:
	movl	%r14d, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end106:
	.size	siod_main, .Lfunc_end106-siod_main
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI107_0:
	.quad	4607182418800017408     # double 1
.LCPI107_1:
	.quad	4611686018427387904     # double 2
.LCPI107_2:
	.quad	4621256167635550208     # double 9
.LCPI107_3:
	.quad	4617315517961601024     # double 5
.LCPI107_4:
	.quad	4613937818241073152     # double 3
.LCPI107_5:
	.quad	4619567317775286272     # double 7
.LCPI107_6:
	.quad	4621819117588971520     # double 10
.LCPI107_7:
	.quad	4618441417868443648     # double 6
.LCPI107_8:
	.quad	4616189618054758400     # double 4
.LCPI107_9:
	.quad	4620693217682128896     # double 8
	.text
	.globl	init_slibu
	.p2align	4, 0x90
	.type	init_slibu,@function
init_slibu:                             # @init_slibu
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi626:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi627:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi628:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi629:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi630:
	.cfi_def_cfa_offset 48
.Lcfi631:
	.cfi_offset %rbx, -40
.Lcfi632:
	.cfi_offset %r12, -32
.Lcfi633:
	.cfi_offset %r14, -24
.Lcfi634:
	.cfi_offset %r15, -16
	callq	allocate_user_tc
	movq	%rax, tc_opendir(%rip)
	movq	%rsp, %r9
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$opendir_gc_free, %r8d
	movq	%rax, %rdi
	callq	set_gc_hooks
	movq	tc_opendir(%rip), %rdi
	movl	$opendir_prin1, %esi
	callq	set_print_hooks
	movl	$.L.str.67, %edi
	movl	$l_chmod, %esi
	callq	init_subr_2
	movl	$sym_channels, %edi
	movl	$.L.str.136, %esi
	callq	gc_protect_sym
	movq	sym_channels(%rip), %rdi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	setvar
	movl	$.L.str.137, %edi
	movl	$lsystem, %esi
	callq	init_lsubr
	movl	$.L.str.138, %edi
	movl	$lgetgid, %esi
	callq	init_subr_0
	movl	$.L.str.139, %edi
	movl	$lgetuid, %esi
	callq	init_subr_0
	movl	$.L.str.1, %edi
	movl	$lgetcwd, %esi
	callq	init_subr_0
	movl	$.L.str.140, %edi
	movl	$lgetpwuid, %esi
	callq	init_subr_1
	movl	$.L.str.141, %edi
	movl	$lgetpwnam, %esi
	callq	init_subr_1
	movl	$.L.str.142, %edi
	movl	$lgetpwent, %esi
	callq	init_subr_0
	movl	$.L.str.143, %edi
	movl	$lsetpwent, %esi
	callq	init_subr_0
	movl	$.L.str.144, %edi
	movl	$lendpwent, %esi
	callq	init_subr_0
	movl	$.L.str.9, %edi
	movl	$lsetuid, %esi
	callq	init_subr_1
	movl	$.L.str.10, %edi
	movl	$lseteuid, %esi
	callq	init_subr_1
	movl	$.L.str.145, %edi
	movl	$lgeteuid, %esi
	callq	init_subr_0
	movl	$.L.str.146, %edi
	movl	$laccess_problem, %esi
	callq	init_subr_2
	movl	$.L.str.68, %edi
	movl	$lutime, %esi
	callq	init_subr_3
	movl	$.L.str.69, %edi
	movl	$lfchmod, %esi
	callq	init_subr_2
	movl	$.L.str.147, %edi
	movl	$lrandom, %esi
	callq	init_subr_1
	movl	$.L.str.148, %edi
	movl	$lsrandom, %esi
	callq	init_subr_1
	movl	$.L.str.149, %edi
	movl	$car, %esi
	callq	init_subr_1
	movl	$.L.str.150, %edi
	movl	$cdr, %esi
	callq	init_subr_1
	movl	$.L.str.19, %edi
	movl	$lfork, %esi
	callq	init_subr_0
	movl	$.L.str.20, %edi
	movl	$lexec, %esi
	callq	init_subr_3
	movl	$.L.str.21, %edi
	movl	$lnice, %esi
	callq	init_subr_1
	movl	$.L.str.27, %edi
	movl	$lwait, %esi
	callq	init_subr_2
	movl	$.L.str.151, %edi
	movl	$lgetpgrp, %esi
	callq	init_subr_0
	movl	$.L.str.152, %edi
	movl	$lgetgrgid, %esi
	callq	init_subr_1
	movl	$.L.str.29, %edi
	movl	$lsetpgid, %esi
	callq	init_subr_2
	movl	$.L.str.28, %edi
	movl	$lkill, %esi
	callq	init_subr_2
	movl	$.L.str.153, %edi
	movl	$lmemref_byte, %esi
	callq	init_subr_1
	movl	$.L.str.154, %edi
	movl	$lgetpid, %esi
	callq	init_subr_0
	movl	$.L.str.155, %edi
	movl	$lgetppid, %esi
	callq	init_subr_0
	movl	$.L.str.156, %edi
	movl	$lexit, %esi
	callq	init_subr_1
	movl	$.L.str.157, %edi
	movl	$ltrunc, %esi
	callq	init_subr_1
	movl	$.L.str.31, %edi
	movl	$lputenv, %esi
	callq	init_subr_1
	movl	$.L.str.33, %edi
	movl	$l_opendir, %esi
	callq	init_subr_1
	movl	$.L.str.36, %edi
	movl	$l_closedir, %esi
	callq	init_subr_1
	movl	$.L.str.158, %edi
	movl	$l_readdir, %esi
	callq	init_subr_1
	movl	$.L.str.159, %edi
	movl	$delete_file, %esi
	callq	init_subr_1
	movl	$.L.str.160, %edi
	movl	$file_times, %esi
	callq	init_subr_1
	movl	$.L.str.161, %edi
	movl	$utime2str, %esi
	callq	init_subr_1
	movl	$.L.str.162, %edi
	movl	$unix_time, %esi
	callq	init_subr_0
	movl	$.L.str.163, %edi
	movl	$unix_ctime, %esi
	callq	init_subr_1
	movl	$.L.str.164, %edi
	movl	$lgetenv, %esi
	callq	init_subr_1
	movl	$.L.str.165, %edi
	movl	$lsleep, %esi
	callq	init_subr_1
	movl	$.L.str.166, %edi
	movl	$url_encode, %esi
	callq	init_subr_1
	movl	$.L.str.167, %edi
	movl	$url_decode, %esi
	callq	init_subr_1
	movl	$.L.str.168, %edi
	movl	$lgets, %esi
	callq	init_subr_2
	movl	$.L.str.169, %edi
	movl	$readline, %esi
	callq	init_subr_1
	movl	$.L.str.170, %edi
	movl	$html_encode, %esi
	callq	init_subr_1
	movl	$.L.str.171, %edi
	movl	$html_decode, %esi
	callq	init_subr_1
	movl	$.L.str.172, %edi
	movl	$decode_st_mode, %esi
	callq	init_subr_1
	movl	$.L.str.173, %edi
	movl	$encode_st_mode, %esi
	callq	init_subr_1
	movl	$.L.str.174, %edi
	movl	$l_stat, %esi
	callq	init_subr_1
	movl	$.L.str.175, %edi
	movl	$l_fstat, %esi
	callq	init_subr_1
	movl	$.L.str.176, %edi
	movl	$encode_open_flags, %esi
	callq	init_subr_1
	movl	$.L.str.177, %edi
	movl	$l_lstat, %esi
	callq	init_subr_1
	movl	$.L.str.12, %edi
	movl	$lsymlink, %esi
	callq	init_subr_2
	movl	$.L.str.13, %edi
	movl	$llink, %esi
	callq	init_subr_2
	movl	$.L.str.14, %edi
	movl	$lunlink, %esi
	callq	init_subr_1
	movl	$.L.str.15, %edi
	movl	$lrmdir, %esi
	callq	init_subr_1
	movl	$.L.str.16, %edi
	movl	$lmkdir, %esi
	callq	init_subr_2
	movl	$.L.str.18, %edi
	movl	$lrename, %esi
	callq	init_subr_2
	movl	$.L.str.17, %edi
	movl	$lreadlink, %esi
	callq	init_subr_1
	movl	$.L.str.90, %edi
	movl	$l_chown, %esi
	callq	init_subr_3
	movl	$.L.str.178, %edi
	movl	$http_date, %esi
	callq	init_subr_1
	movl	$.L.str.179, %edi
	movl	$popen_l, %esi
	callq	init_subr_2
	movl	$.L.str.92, %edi
	movl	$pclose_l, %esi
	callq	init_subr_1
	movl	$.L.str.180, %edi
	movl	$load_so, %esi
	callq	init_subr_2
	movl	$.L.str.181, %edi
	movl	$require_so, %esi
	callq	init_subr_1
	movl	$.L.str.182, %edi
	movl	$so_ext, %esi
	callq	init_subr_1
	movl	$.L.str.183, %edi
	callq	cintern
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	callq	flocons
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	movl	$.L.str.184, %edi
	callq	cintern
	movq	%rax, %rbx
	movsd	.LCPI107_0(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	flocons
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	movl	$.L.str.185, %edi
	callq	cintern
	movq	%rax, %rbx
	movsd	.LCPI107_1(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	flocons
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	movl	$.L.str.186, %edi
	callq	cintern
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	callq	flocons
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	movl	$.L.str.187, %edi
	callq	cintern
	movq	%rax, %rbx
	movsd	.LCPI107_0(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	flocons
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	movl	$.L.str.188, %edi
	callq	cintern
	movq	%rax, %rbx
	movsd	.LCPI107_1(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	flocons
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	movl	$.L.str.189, %edi
	movl	$lF_SETLK, %esi
	callq	init_subr_5
	movl	$.L.str.190, %edi
	movl	$lF_SETLKW, %esi
	callq	init_subr_5
	movl	$.L.str.191, %edi
	movl	$lF_GETLK, %esi
	callq	init_subr_5
	movl	$.L.str.192, %edi
	movl	$siod_lib_l, %esi
	callq	init_subr_0
	movq	ld_library_path_env(%rip), %rbx
	movq	%rbx, %rdi
	callq	getenv
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB107_5
# BB#1:
	movq	siod_lib(%rip), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	strstr
	testq	%rax, %rax
	jne	.LBB107_4
# BB#2:
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r15
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rbx
	incq	%rbx
	jmp	.LBB107_3
.LBB107_5:
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r15
	xorl	%ebx, %ebx
	movq	siod_lib(%rip), %r14
.LBB107_3:
	movq	%r14, %rdi
	callq	strlen
	addq	%rbx, %r15
	leaq	2(%rax,%r15), %rdi
	callq	must_malloc
	movq	%rax, %rbx
	testq	%r12, %r12
	movq	ld_library_path_env(%rip), %rdx
	movl	$.L.str.114, %eax
	cmoveq	%rax, %r12
	movl	$.L.str.194, %r8d
	cmoveq	%rax, %r8
	movq	siod_lib(%rip), %r9
	movl	$.L.str.193, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r12, %rcx
	callq	sprintf
	movq	%rbx, %rdi
	callq	putenv
.LBB107_4:
	movl	$.L.str.110, %edi
	movl	$llocaltime, %esi
	callq	init_subr_1
	movl	$.L.str.111, %edi
	movl	$lgmtime, %esi
	callq	init_subr_1
	movl	$.L.str.195, %edi
	movl	$ltzset, %esi
	callq	init_subr_0
	movl	$.L.str.196, %edi
	movl	$lmktime, %esi
	callq	init_subr_1
	movl	$.L.str.113, %edi
	movl	$lchdir, %esi
	callq	init_subr_1
	movl	$.L.str.197, %edi
	movl	$lstrptime, %esi
	callq	init_subr_3
	movl	$.L.str.198, %edi
	movl	$lstrftime, %esi
	callq	init_subr_2
	movl	$.L.str.199, %edi
	movl	$lgetpass, %esi
	callq	init_subr_1
	movl	$.L.str.116, %edi
	movl	$lpipe, %esi
	callq	init_subr_0
	movl	$.L.str.200, %edi
	movl	$lalarm, %esi
	callq	init_subr_2
	movl	$.L.str.201, %edi
	callq	cintern
	movq	%rax, %rbx
	movsd	.LCPI107_0(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	flocons
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	movl	$.L.str.202, %edi
	callq	cintern
	movq	%rax, %rbx
	movsd	.LCPI107_1(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	flocons
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	movl	$.L.str.203, %edi
	callq	cintern
	movq	%rax, %rbx
	movsd	.LCPI107_2(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	flocons
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	movl	$.L.str.204, %edi
	callq	cintern
	movq	%rax, %rbx
	movsd	.LCPI107_3(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	flocons
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	movl	$.L.str.205, %edi
	callq	cintern
	movq	%rax, %rbx
	movsd	.LCPI107_4(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	flocons
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	movl	$.L.str.206, %edi
	callq	cintern
	movq	%rax, %rbx
	movsd	.LCPI107_5(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	flocons
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	movl	$.L.str.207, %edi
	callq	cintern
	movq	%rax, %rbx
	movsd	.LCPI107_6(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	flocons
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	movl	$.L.str.208, %edi
	callq	cintern
	movq	%rax, %rbx
	movsd	.LCPI107_7(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	flocons
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	movl	$.L.str.209, %edi
	callq	cintern
	movq	%rax, %rbx
	movsd	.LCPI107_8(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	flocons
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	movl	$.L.str.210, %edi
	callq	cintern
	movq	%rax, %rbx
	movsd	.LCPI107_9(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	flocons
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	movl	$.L.str.211, %edi
	movl	$datref, %esi
	callq	init_subr_3
	movl	$.L.str.120, %edi
	movl	$sdatref, %esi
	callq	init_subr_2
	movl	$.L.str.212, %edi
	movl	$mkdatref, %esi
	callq	init_subr_2
	movl	$.L.str.213, %edi
	movl	$datlength, %esi
	callq	init_subr_2
	movl	$.L.str.214, %edi
	movl	$lposition_script, %esi
	callq	init_subr_1
	movl	$.L.str.229, %edi
	callq	cintern
	movq	%rax, %rbx
	movl	$.L.str.230, %edi
	callq	cintern
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	setvar
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end107:
	.size	init_slibu, .Lfunc_end107-init_slibu
	.cfi_endproc

	.p2align	4, 0x90
	.type	cgi_main,@function
cgi_main:                               # @cgi_main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi635:
	.cfi_def_cfa_offset 16
.Lcfi636:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB108_7
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB108_7
# BB#2:
	movq	%rbx, %rdi
	callq	car
	testq	%rax, %rax
	je	.LBB108_7
# BB#3:
	movq	%rbx, %rdi
	callq	car
	movzwl	2(%rax), %eax
	cmpl	$13, %eax
	jne	.LBB108_7
# BB#4:
	movl	$.L.str.215, %edi
	callq	put_st
	movl	$.L.str.216, %edi
	callq	put_st
	movl	$.L.str.217, %edi
	callq	put_st
	movl	$.L.str.218, %edi
	callq	put_st
	movl	$.L.str.219, %edi
	callq	put_st
	movl	$.L.str.220, %edi
	callq	put_st
	movl	$.L.str.221, %edi
	callq	put_st
	movq	%rbx, %rdi
	callq	car
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	lprint
	movq	%rbx, %rdi
	callq	cdr
	testq	%rax, %rax
	je	.LBB108_6
# BB#5:
	movl	$.L.str.98, %edi
	callq	put_st
	movq	%rbx, %rdi
	callq	cdr
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	lprint
.LBB108_6:
	movl	$.L.str.222, %edi
	callq	put_st
	movl	$.L.str.223, %edi
	xorl	%esi, %esi
	callq	err
.LBB108_7:                              # %.critedge
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end108:
	.size	cgi_main, .Lfunc_end108-cgi_main
	.cfi_endproc

	.p2align	4, 0x90
	.type	htqs_arg,@function
htqs_arg:                               # @htqs_arg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi637:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi638:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi639:
	.cfi_def_cfa_offset 32
	subq	$1024, %rsp             # imm = 0x400
.Lcfi640:
	.cfi_def_cfa_offset 1056
.Lcfi641:
	.cfi_offset %rbx, -32
.Lcfi642:
	.cfi_offset %r14, -24
.Lcfi643:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$.L.str.224, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB109_2
# BB#1:
	movl	$.L.str.225, %esi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB109_2
# BB#3:
	movl	$40, %esi
	movq	%r14, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB109_4
# BB#10:
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	jmp	.LBB109_11
.LBB109_2:
	movl	$1, %edi
	movl	$1, %esi
	xorl	%edx, %edx
	callq	repl_driver
	jmp	.LBB109_12
.LBB109_4:
	movabsq	$7310021098310300200, %rax # imm = 0x6572697571657228
	movq	%rax, (%rsp)
	movb	$0, 10(%rsp)
	movw	$8736, 8(%rsp)          # imm = 0x2220
	movq	%rsp, %rdi
	callq	strlen
	leaq	(%rsp,%rax), %rbx
	movb	(%r14), %bpl
	testb	%bpl, %bpl
	je	.LBB109_9
# BB#5:                                 # %.lr.ph.preheader
	incq	%r14
	.p2align	4, 0x90
.LBB109_6:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	%bpl, %esi
	movl	$.L.str.227, %edi
	movl	$3, %edx
	callq	memchr
	testq	%rax, %rax
	je	.LBB109_8
# BB#7:                                 #   in Loop: Header=BB109_6 Depth=1
	movb	$92, (%rbx)
	incq	%rbx
	movzbl	-1(%r14), %ebp
.LBB109_8:                              #   in Loop: Header=BB109_6 Depth=1
	movb	%bpl, (%rbx)
	incq	%rbx
	movzbl	(%r14), %ebp
	incq	%r14
	testb	%bpl, %bpl
	jne	.LBB109_6
.LBB109_9:                              # %._crit_edge
	movb	$0, (%rbx)
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	callq	strlen
	movb	$0, 2(%rsp,%rax)
	movw	$10530, (%rsp,%rax)     # imm = 0x2922
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
.LBB109_11:
	callq	repl_c_string
.LBB109_12:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$1024, %rsp             # imm = 0x400
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end109:
	.size	htqs_arg, .Lfunc_end109-htqs_arg
	.cfi_endproc

	.globl	position_script
	.p2align	4, 0x90
	.type	position_script,@function
position_script:                        # @position_script
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi644:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi645:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi646:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi647:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi648:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi649:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi650:
	.cfi_def_cfa_offset 64
.Lcfi651:
	.cfi_offset %rbx, -56
.Lcfi652:
	.cfi_offset %r12, -48
.Lcfi653:
	.cfi_offset %r13, -40
.Lcfi654:
	.cfi_offset %r14, -32
.Lcfi655:
	.cfi_offset %r15, -24
.Lcfi656:
	.cfi_offset %rbp, -16
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movb	$0, (%r12)
	movq	$-1, %r14
	xorl	%r15d, %r15d
	movl	$33, %r13d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB110_1:                              # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$34, %eax
	jle	.LBB110_2
# BB#5:                                 #   in Loop: Header=BB110_1 Depth=1
	cmpl	$47, %eax
	je	.LBB110_11
# BB#6:                                 #   in Loop: Header=BB110_1 Depth=1
	cmpl	$35, %eax
	jne	.LBB110_4
# BB#7:                                 # %._crit_edge
                                        #   in Loop: Header=BB110_1 Depth=1
	movl	$35, %eax
	movq	%rbp, %r14
	jmp	.LBB110_9
	.p2align	4, 0x90
.LBB110_2:                              #   in Loop: Header=BB110_1 Depth=1
	cmpl	$33, %eax
	jne	.LBB110_3
# BB#8:                                 #   in Loop: Header=BB110_1 Depth=1
	cmpl	$35, %r15d
	movl	$0, %eax
	cmovel	%r13d, %eax
	jmp	.LBB110_9
	.p2align	4, 0x90
.LBB110_11:                             #   in Loop: Header=BB110_1 Depth=1
	xorl	%eax, %eax
	cmpl	$33, %r15d
	jne	.LBB110_9
	jmp	.LBB110_12
.LBB110_3:                              #   in Loop: Header=BB110_1 Depth=1
	cmpl	$-1, %eax
	je	.LBB110_10
.LBB110_4:                              #   in Loop: Header=BB110_1 Depth=1
	xorl	%eax, %eax
.LBB110_9:                              #   in Loop: Header=BB110_1 Depth=1
	incq	%rbp
	cmpq	$250000, %rbp           # imm = 0x3D090
	movl	%eax, %r15d
	jl	.LBB110_1
.LBB110_10:
	movq	$-1, %r14
.LBB110_26:                             # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB110_12:                             # %.preheader55.preheader
	movq	(%rsp), %r15            # 8-byte Reload
	.p2align	4, 0x90
.LBB110_13:                             # %.preheader55
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$32, %eax
	je	.LBB110_15
# BB#14:                                # %.preheader55
                                        #   in Loop: Header=BB110_13 Depth=1
	cmpl	$-1, %eax
	jne	.LBB110_13
.LBB110_15:                             # %.preheader.preheader
	movl	$1, %ebp
	jmp	.LBB110_16
	.p2align	4, 0x90
.LBB110_19:                             #   in Loop: Header=BB110_16 Depth=1
	movb	%al, -1(%r12,%rbp)
	movb	$0, (%r12,%rbp)
	incq	%rbp
.LBB110_16:                             # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB110_20
# BB#17:                                # %.preheader
                                        #   in Loop: Header=BB110_16 Depth=1
	cmpl	$10, %eax
	je	.LBB110_20
# BB#18:                                #   in Loop: Header=BB110_16 Depth=1
	cmpq	%r15, %rbp
	jbe	.LBB110_19
.LBB110_20:                             # %.critedge
	xorl	%ebx, %ebx
	movabsq	$4294976000, %rax       # imm = 0x100002200
	jmp	.LBB110_21
	.p2align	4, 0x90
.LBB110_23:                             # %.backedge.backedge
                                        #   in Loop: Header=BB110_21 Depth=1
	incq	%rbx
.LBB110_21:                             # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%r12,%rbx), %ecx
	cmpl	$32, %ecx
	ja	.LBB110_24
# BB#22:                                # %.backedge
                                        #   in Loop: Header=BB110_21 Depth=1
	btq	%rcx, %rax
	jb	.LBB110_23
.LBB110_24:                             # %__strspn_c3.exit
	movq	%r12, %rdi
	callq	strlen
	cmpq	%rbx, %rax
	jne	.LBB110_26
# BB#25:
	movb	$0, (%r12)
	jmp	.LBB110_26
.Lfunc_end110:
	.size	position_script, .Lfunc_end110-position_script
	.cfi_endproc

	.globl	siod_shuffle_args
	.p2align	4, 0x90
	.type	siod_shuffle_args,@function
siod_shuffle_args:                      # @siod_shuffle_args
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi657:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi658:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi659:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi660:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi661:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi662:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi663:
	.cfi_def_cfa_offset 208
.Lcfi664:
	.cfi_offset %rbx, -56
.Lcfi665:
	.cfi_offset %r12, -48
.Lcfi666:
	.cfi_offset %r13, -40
.Lcfi667:
	.cfi_offset %r14, -32
.Lcfi668:
	.cfi_offset %r15, -24
.Lcfi669:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r15
	movl	(%r15), %ebp
	movq	(%r13), %r14
	movq	(%r14), %rdi
	movl	$.L.str.132, %esi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB111_26
# BB#1:
	leaq	48(%rsp), %rsi
	movl	$100, %edx
	movq	%rbx, %rdi
	callq	position_script
	movq	%rax, %r12
	movq	%rbx, %rdi
	callq	fclose
	testq	%r12, %r12
	js	.LBB111_26
# BB#2:
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movb	48(%rsp), %r15b
	xorl	%eax, %eax
	cmpb	$0, %r15b
	setne	%al
	leal	1(%rax,%rbp), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movslq	%eax, %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, %rbx
	cmpb	$0, %r15b
	movq	$.L.str.133, (%rbx)
	movq	%r13, 24(%rsp)          # 8-byte Spill
	je	.LBB111_4
# BB#3:
	leaq	48(%rsp), %rdi
	callq	__strdup
	movq	%rax, 8(%rbx)
	movl	$2, %r15d
	jmp	.LBB111_5
.LBB111_4:
	movl	$1, %r15d
.LBB111_5:
	leaq	38(%rsp), %r13
	movl	$.L.str.134, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r12, %rdx
	callq	sprintf
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %r12
	movq	(%r14), %rdi
	callq	strlen
	leaq	2(%r12,%rax), %rdi
	callq	malloc
	movq	%rax, %rdx
	movq	%rdx, (%rbx,%r15,8)
	movq	(%r14), %r8
	movl	$.L.str.135, %esi
	movl	$124, %ecx
	xorl	%eax, %eax
	movq	%rdx, %rdi
	movq	%r13, %rdx
	callq	sprintf
	cmpl	$2, %ebp
	movq	24(%rsp), %rdi          # 8-byte Reload
	jl	.LBB111_25
# BB#6:                                 # %.lr.ph.preheader
	leaq	-1(%rbp), %rax
	cmpq	$4, %rax
	jae	.LBB111_8
# BB#7:
	movl	$1, %ecx
	jmp	.LBB111_19
.LBB111_8:                              # %min.iters.checked
	movq	%rax, %rcx
	andq	$-4, %rcx
	je	.LBB111_12
# BB#9:                                 # %vector.memcheck
	leaq	8(%rbx,%r15,8), %rdx
	leaq	(%r14,%rbp,8), %rsi
	cmpq	%rsi, %rdx
	jae	.LBB111_13
# BB#10:                                # %vector.memcheck
	leaq	(,%rbp,8), %rdx
	leaq	(%rdx,%r15,8), %rdx
	addq	%rbx, %rdx
	leaq	8(%r14), %rsi
	cmpq	%rdx, %rsi
	jae	.LBB111_13
.LBB111_12:
	movl	$1, %ecx
.LBB111_19:                             # %.lr.ph.preheader65
	movl	%ebp, %edx
	subl	%ecx, %edx
	subq	%rcx, %rax
	andq	$3, %rdx
	je	.LBB111_22
# BB#20:                                # %.lr.ph.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB111_21:                             # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rcx,8), %rsi
	movq	%rsi, 8(%rbx,%r15,8)
	incq	%r15
	incq	%rcx
	incq	%rdx
	jne	.LBB111_21
.LBB111_22:                             # %.lr.ph.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB111_25
# BB#23:                                # %.lr.ph.preheader65.new
	subq	%rcx, %rbp
	leaq	24(%r14,%rcx,8), %rax
	leaq	32(%rbx,%r15,8), %rcx
	.p2align	4, 0x90
.LBB111_24:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%rax), %rdx
	movq	%rdx, -24(%rcx)
	movq	-16(%rax), %rdx
	movq	%rdx, -16(%rcx)
	movq	-8(%rax), %rdx
	movq	%rdx, -8(%rcx)
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	addq	$32, %rax
	addq	$32, %rcx
	addq	$-4, %rbp
	jne	.LBB111_24
.LBB111_25:                             # %._crit_edge
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, (%rax)
	movq	%rbx, (%rdi)
.LBB111_26:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB111_13:                             # %vector.body.preheader
	leaq	-4(%rcx), %rdx
	movq	%rdx, %rsi
	shrq	$2, %rsi
	btl	$2, %edx
	jb	.LBB111_15
# BB#14:                                # %vector.body.prol
	movups	8(%r14), %xmm0
	movups	24(%r14), %xmm1
	movups	%xmm0, 8(%rbx,%r15,8)
	movups	%xmm1, 24(%rbx,%r15,8)
	movl	$4, %edx
	testq	%rsi, %rsi
	jne	.LBB111_16
	jmp	.LBB111_17
.LBB111_15:
	xorl	%edx, %edx
	testq	%rsi, %rsi
	je	.LBB111_17
	.p2align	4, 0x90
.LBB111_16:                             # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rsi
	orq	%rdx, %rsi
	movups	8(%r14,%rdx,8), %xmm0
	movups	24(%r14,%rdx,8), %xmm1
	movups	%xmm0, 8(%rbx,%rsi,8)
	movups	%xmm1, 24(%rbx,%rsi,8)
	leaq	4(%rdx), %rsi
	orq	%r15, %rsi
	movups	40(%r14,%rdx,8), %xmm0
	movups	56(%r14,%rdx,8), %xmm1
	movups	%xmm0, 8(%rbx,%rsi,8)
	movups	%xmm1, 24(%rbx,%rsi,8)
	addq	$8, %rdx
	cmpq	%rcx, %rdx
	jne	.LBB111_16
.LBB111_17:                             # %middle.block
	cmpq	%rcx, %rax
	je	.LBB111_25
# BB#18:
	orq	%rcx, %r15
	orq	$1, %rcx
	jmp	.LBB111_19
.Lfunc_end111:
	.size	siod_shuffle_args, .Lfunc_end111-siod_shuffle_args
	.cfi_endproc

	.globl	lposition_script
	.p2align	4, 0x90
	.type	lposition_script,@function
lposition_script:                       # @lposition_script
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi670:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi671:
	.cfi_def_cfa_offset 24
	subq	$104, %rsp
.Lcfi672:
	.cfi_def_cfa_offset 128
.Lcfi673:
	.cfi_offset %rbx, -24
.Lcfi674:
	.cfi_offset %r14, -16
	movq	stdin(%rip), %rsi
	callq	get_c_file
	movq	%rax, %rbx
	movl	$1, %edi
	callq	no_interrupt
	movq	%rax, %r14
	movq	%rsp, %rsi
	movl	$100, %edx
	movq	%rbx, %rdi
	callq	position_script
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	no_interrupt
	testq	%rbx, %rbx
	js	.LBB112_1
# BB#2:
	cvtsi2sdq	%rbx, %xmm0
	callq	flocons
	movq	%rax, %rbx
	movq	%rsp, %rsi
	movq	$-1, %rdi
	callq	strcons
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	cons
	jmp	.LBB112_3
.LBB112_1:
	xorl	%eax, %eax
.LBB112_3:
	addq	$104, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end112:
	.size	lposition_script, .Lfunc_end112-lposition_script
	.cfi_endproc

	.globl	siod_init
	.p2align	4, 0x90
	.type	siod_init,@function
siod_init:                              # @siod_init
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi675:
	.cfi_def_cfa_offset 16
	xorl	%edx, %edx
	callq	process_cla
	callq	init_storage
	callq	init_subrs
	callq	init_trace
	popq	%rax
	jmp	init_slibu              # TAILCALL
.Lfunc_end113:
	.size	siod_init, .Lfunc_end113-siod_init
	.cfi_endproc

	.type	sym_channels,@object    # @sym_channels
	.bss
	.globl	sym_channels
	.p2align	3
sym_channels:
	.quad	0
	.size	sym_channels, 8

	.type	tc_opendir,@object      # @tc_opendir
	.globl	tc_opendir
	.p2align	3
tc_opendir:
	.quad	0                       # 0x0
	.size	tc_opendir, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"LD_LIBRARY_PATH"
	.size	.L.str, 16

	.type	ld_library_path_env,@object # @ld_library_path_env
	.data
	.globl	ld_library_path_env
	.p2align	3
ld_library_path_env:
	.quad	.L.str
	.size	ld_library_path_env, 8

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"getcwd"
	.size	.L.str.1, 7

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"name"
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"passwd"
	.size	.L.str.3, 7

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"uid"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"gid"
	.size	.L.str.5, 4

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"dir"
	.size	.L.str.6, 4

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"gecos"
	.size	.L.str.7, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"shell"
	.size	.L.str.8, 6

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"setuid"
	.size	.L.str.9, 7

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"seteuid"
	.size	.L.str.10, 8

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"bad access mode"
	.size	.L.str.11, 16

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"symlink"
	.size	.L.str.12, 8

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"link"
	.size	.L.str.13, 5

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"unlink"
	.size	.L.str.14, 7

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"rmdir"
	.size	.L.str.15, 6

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"mkdir"
	.size	.L.str.16, 6

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"readlink"
	.size	.L.str.17, 9

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"rename"
	.size	.L.str.18, 7

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"fork"
	.size	.L.str.19, 5

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"exec"
	.size	.L.str.20, 5

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"nice"
	.size	.L.str.21, 5

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"contains undefined options"
	.size	.L.str.22, 27

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"WCONTINUED"
	.size	.L.str.23, 11

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"WNOWAIT"
	.size	.L.str.24, 8

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"WNOHANG"
	.size	.L.str.25, 8

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"WUNTRACED"
	.size	.L.str.26, 10

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"wait"
	.size	.L.str.27, 5

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"kill"
	.size	.L.str.28, 5

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"setpgid"
	.size	.L.str.29, 8

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"wta to trunc"
	.size	.L.str.30, 13

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"putenv"
	.size	.L.str.31, 7

	.type	handle_sigalrm_flag,@object # @handle_sigalrm_flag
	.local	handle_sigalrm_flag
	.comm	handle_sigalrm_flag,4,4
	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"alarm signal"
	.size	.L.str.32, 13

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"opendir"
	.size	.L.str.33, 8

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"not an opendir"
	.size	.L.str.34, 15

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"opendir not open"
	.size	.L.str.35, 17

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"closedir"
	.size	.L.str.36, 9

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"#<OPENDIR %p>"
	.size	.L.str.37, 14

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"SUID"
	.size	.L.str.38, 5

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"SGID"
	.size	.L.str.39, 5

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"RUSR"
	.size	.L.str.40, 5

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"WUSR"
	.size	.L.str.41, 5

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"XUSR"
	.size	.L.str.42, 5

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"RGRP"
	.size	.L.str.43, 5

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"WGRP"
	.size	.L.str.44, 5

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"XGRP"
	.size	.L.str.45, 5

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"ROTH"
	.size	.L.str.46, 5

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"WOTH"
	.size	.L.str.47, 5

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"XOTH"
	.size	.L.str.48, 5

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"FIFO"
	.size	.L.str.49, 5

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"DIR"
	.size	.L.str.50, 4

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"CHR"
	.size	.L.str.51, 4

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"BLK"
	.size	.L.str.52, 4

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"REG"
	.size	.L.str.53, 4

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"LNK"
	.size	.L.str.54, 4

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"SOCK"
	.size	.L.str.55, 5

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"dev"
	.size	.L.str.56, 4

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"ino"
	.size	.L.str.57, 4

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"mode"
	.size	.L.str.58, 5

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"nlink"
	.size	.L.str.59, 6

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"rdev"
	.size	.L.str.60, 5

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"size"
	.size	.L.str.61, 5

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"atime"
	.size	.L.str.62, 6

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"mtime"
	.size	.L.str.63, 6

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"ctime"
	.size	.L.str.64, 6

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"blksize"
	.size	.L.str.65, 8

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"blocks"
	.size	.L.str.66, 7

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"chmod"
	.size	.L.str.67, 6

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"utime"
	.size	.L.str.68, 6

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"fchmod"
	.size	.L.str.69, 7

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"NONBLOCK"
	.size	.L.str.70, 9

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"APPEND"
	.size	.L.str.71, 7

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"RDONLY"
	.size	.L.str.72, 7

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"WRONLY"
	.size	.L.str.73, 7

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"RDWR"
	.size	.L.str.74, 5

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"CREAT"
	.size	.L.str.75, 6

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"TRUNC"
	.size	.L.str.76, 6

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"EXCL"
	.size	.L.str.77, 5

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"%04d%02d%02d%02d%02d%02d%02d"
	.size	.L.str.78, 29

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"%s, %02d %s %04d %02d:%02d:%02d GMT"
	.size	.L.str.79, 36

	.type	.L.str.80,@object       # @.str.80
	.section	.rodata,"a",@progbits
.L.str.80:
	.asciz	"Sun\000Mon\000Tue\000Wed\000Thu\000Fri\000Sat"
	.size	.L.str.80, 28

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"Jan\000Feb\000Mar\000Apr\000May\000Jun\000Jul\000Aug\000Sep\000Oct\000Nov\000Dec"
	.size	.L.str.81, 48

	.type	.L.str.82,@object       # @.str.82
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.82:
	.asciz	"*-._@"
	.size	.L.str.82, 6

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"%%%02X"
	.size	.L.str.83, 7

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"&gt;"
	.size	.L.str.84, 5

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"&lt;"
	.size	.L.str.85, 5

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"&amp;"
	.size	.L.str.86, 6

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"&quot;"
	.size	.L.str.87, 7

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"size must be >= 0"
	.size	.L.str.88, 18

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"not handling buffer of size"
	.size	.L.str.89, 28

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"chown"
	.size	.L.str.90, 6

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"r"
	.size	.L.str.91, 2

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"pclose"
	.size	.L.str.92, 7

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"/"
	.size	.L.str.93, 2

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"."
	.size	.L.str.94, 2

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"init_"
	.size	.L.str.95, 6

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	".so"
	.size	.L.str.96, 4

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"so-loading "
	.size	.L.str.97, 12

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"\n"
	.size	.L.str.98, 2

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"did not load function"
	.size	.L.str.99, 22

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"done.\n"
	.size	.L.str.100, 7

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"sec"
	.size	.L.str.101, 4

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"min"
	.size	.L.str.102, 4

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"hour"
	.size	.L.str.103, 5

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"mday"
	.size	.L.str.104, 5

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"mon"
	.size	.L.str.105, 4

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"year"
	.size	.L.str.106, 5

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"wday"
	.size	.L.str.107, 5

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"yday"
	.size	.L.str.108, 5

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"isdst"
	.size	.L.str.109, 6

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"localtime"
	.size	.L.str.110, 10

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"gmtime"
	.size	.L.str.111, 7

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"fchdir"
	.size	.L.str.112, 7

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"chdir"
	.size	.L.str.113, 6

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.zero	1
	.size	.L.str.114, 1

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"w"
	.size	.L.str.115, 2

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"pipe"
	.size	.L.str.116, 5

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"index too large"
	.size	.L.str.117, 16

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"negative index"
	.size	.L.str.118, 15

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"unknown CTYPE"
	.size	.L.str.119, 14

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"sdatref"
	.size	.L.str.120, 8

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	",-"
	.size	.L.str.121, 3

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"-v"
	.size	.L.str.122, 3

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"-m"
	.size	.L.str.124, 3

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"*args*"
	.size	.L.str.125, 7

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"*env*"
	.size	.L.str.126, 6

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"__cgi-main"
	.size	.L.str.127, 11

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"(__cgi-main (*catch 'errobj (main))))"
	.size	.L.str.128, 38

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"(main)"
	.size	.L.str.129, 7

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	" \t\r"
	.size	.L.str.131, 4

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"rb"
	.size	.L.str.132, 3

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"siod.exe"
	.size	.L.str.133, 9

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"%ld"
	.size	.L.str.134, 4

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"%s%c%s"
	.size	.L.str.135, 7

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"*channels*"
	.size	.L.str.136, 11

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"system"
	.size	.L.str.137, 7

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"getgid"
	.size	.L.str.138, 7

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"getuid"
	.size	.L.str.139, 7

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"getpwuid"
	.size	.L.str.140, 9

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"getpwnam"
	.size	.L.str.141, 9

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"getpwent"
	.size	.L.str.142, 9

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"setpwent"
	.size	.L.str.143, 9

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"endpwent"
	.size	.L.str.144, 9

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"geteuid"
	.size	.L.str.145, 8

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"access-problem?"
	.size	.L.str.146, 16

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"random"
	.size	.L.str.147, 7

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	"srandom"
	.size	.L.str.148, 8

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"first"
	.size	.L.str.149, 6

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"rest"
	.size	.L.str.150, 5

	.type	.L.str.151,@object      # @.str.151
.L.str.151:
	.asciz	"getpgrp"
	.size	.L.str.151, 8

	.type	.L.str.152,@object      # @.str.152
.L.str.152:
	.asciz	"getgrgid"
	.size	.L.str.152, 9

	.type	.L.str.153,@object      # @.str.153
.L.str.153:
	.asciz	"%%%memref"
	.size	.L.str.153, 10

	.type	.L.str.154,@object      # @.str.154
.L.str.154:
	.asciz	"getpid"
	.size	.L.str.154, 7

	.type	.L.str.155,@object      # @.str.155
.L.str.155:
	.asciz	"getppid"
	.size	.L.str.155, 8

	.type	.L.str.156,@object      # @.str.156
.L.str.156:
	.asciz	"exit"
	.size	.L.str.156, 5

	.type	.L.str.157,@object      # @.str.157
.L.str.157:
	.asciz	"trunc"
	.size	.L.str.157, 6

	.type	.L.str.158,@object      # @.str.158
.L.str.158:
	.asciz	"readdir"
	.size	.L.str.158, 8

	.type	.L.str.159,@object      # @.str.159
.L.str.159:
	.asciz	"delete-file"
	.size	.L.str.159, 12

	.type	.L.str.160,@object      # @.str.160
.L.str.160:
	.asciz	"file-times"
	.size	.L.str.160, 11

	.type	.L.str.161,@object      # @.str.161
.L.str.161:
	.asciz	"unix-time->strtime"
	.size	.L.str.161, 19

	.type	.L.str.162,@object      # @.str.162
.L.str.162:
	.asciz	"unix-time"
	.size	.L.str.162, 10

	.type	.L.str.163,@object      # @.str.163
.L.str.163:
	.asciz	"unix-ctime"
	.size	.L.str.163, 11

	.type	.L.str.164,@object      # @.str.164
.L.str.164:
	.asciz	"getenv"
	.size	.L.str.164, 7

	.type	.L.str.165,@object      # @.str.165
.L.str.165:
	.asciz	"sleep"
	.size	.L.str.165, 6

	.type	.L.str.166,@object      # @.str.166
.L.str.166:
	.asciz	"url-encode"
	.size	.L.str.166, 11

	.type	.L.str.167,@object      # @.str.167
.L.str.167:
	.asciz	"url-decode"
	.size	.L.str.167, 11

	.type	.L.str.168,@object      # @.str.168
.L.str.168:
	.asciz	"gets"
	.size	.L.str.168, 5

	.type	.L.str.169,@object      # @.str.169
.L.str.169:
	.asciz	"readline"
	.size	.L.str.169, 9

	.type	.L.str.170,@object      # @.str.170
.L.str.170:
	.asciz	"html-encode"
	.size	.L.str.170, 12

	.type	.L.str.171,@object      # @.str.171
.L.str.171:
	.asciz	"html-decode"
	.size	.L.str.171, 12

	.type	.L.str.172,@object      # @.str.172
.L.str.172:
	.asciz	"decode-file-mode"
	.size	.L.str.172, 17

	.type	.L.str.173,@object      # @.str.173
.L.str.173:
	.asciz	"encode-file-mode"
	.size	.L.str.173, 17

	.type	.L.str.174,@object      # @.str.174
.L.str.174:
	.asciz	"stat"
	.size	.L.str.174, 5

	.type	.L.str.175,@object      # @.str.175
.L.str.175:
	.asciz	"fstat"
	.size	.L.str.175, 6

	.type	.L.str.176,@object      # @.str.176
.L.str.176:
	.asciz	"encode-open-flags"
	.size	.L.str.176, 18

	.type	.L.str.177,@object      # @.str.177
.L.str.177:
	.asciz	"lstat"
	.size	.L.str.177, 6

	.type	.L.str.178,@object      # @.str.178
.L.str.178:
	.asciz	"http-date"
	.size	.L.str.178, 10

	.type	.L.str.179,@object      # @.str.179
.L.str.179:
	.asciz	"popen"
	.size	.L.str.179, 6

	.type	.L.str.180,@object      # @.str.180
.L.str.180:
	.asciz	"load-so"
	.size	.L.str.180, 8

	.type	.L.str.181,@object      # @.str.181
.L.str.181:
	.asciz	"require-so"
	.size	.L.str.181, 11

	.type	.L.str.182,@object      # @.str.182
.L.str.182:
	.asciz	"so-ext"
	.size	.L.str.182, 7

	.type	.L.str.183,@object      # @.str.183
.L.str.183:
	.asciz	"SEEK_SET"
	.size	.L.str.183, 9

	.type	.L.str.184,@object      # @.str.184
.L.str.184:
	.asciz	"SEEK_CUR"
	.size	.L.str.184, 9

	.type	.L.str.185,@object      # @.str.185
.L.str.185:
	.asciz	"SEEK_END"
	.size	.L.str.185, 9

	.type	.L.str.186,@object      # @.str.186
.L.str.186:
	.asciz	"F_RDLCK"
	.size	.L.str.186, 8

	.type	.L.str.187,@object      # @.str.187
.L.str.187:
	.asciz	"F_WRLCK"
	.size	.L.str.187, 8

	.type	.L.str.188,@object      # @.str.188
.L.str.188:
	.asciz	"F_UNLCK"
	.size	.L.str.188, 8

	.type	.L.str.189,@object      # @.str.189
.L.str.189:
	.asciz	"F_SETLK"
	.size	.L.str.189, 8

	.type	.L.str.190,@object      # @.str.190
.L.str.190:
	.asciz	"F_SETLKW"
	.size	.L.str.190, 9

	.type	.L.str.191,@object      # @.str.191
.L.str.191:
	.asciz	"F_GETLK"
	.size	.L.str.191, 8

	.type	.L.str.192,@object      # @.str.192
.L.str.192:
	.asciz	"siod-lib"
	.size	.L.str.192, 9

	.type	.L.str.193,@object      # @.str.193
.L.str.193:
	.asciz	"%s=%s%s%s"
	.size	.L.str.193, 10

	.type	.L.str.194,@object      # @.str.194
.L.str.194:
	.asciz	":"
	.size	.L.str.194, 2

	.type	.L.str.195,@object      # @.str.195
.L.str.195:
	.asciz	"tzset"
	.size	.L.str.195, 6

	.type	.L.str.196,@object      # @.str.196
.L.str.196:
	.asciz	"mktime"
	.size	.L.str.196, 7

	.type	.L.str.197,@object      # @.str.197
.L.str.197:
	.asciz	"strptime"
	.size	.L.str.197, 9

	.type	.L.str.198,@object      # @.str.198
.L.str.198:
	.asciz	"strftime"
	.size	.L.str.198, 9

	.type	.L.str.199,@object      # @.str.199
.L.str.199:
	.asciz	"getpass"
	.size	.L.str.199, 8

	.type	.L.str.200,@object      # @.str.200
.L.str.200:
	.asciz	"alarm"
	.size	.L.str.200, 6

	.type	.L.str.201,@object      # @.str.201
.L.str.201:
	.asciz	"CTYPE_FLOAT"
	.size	.L.str.201, 12

	.type	.L.str.202,@object      # @.str.202
.L.str.202:
	.asciz	"CTYPE_DOUBLE"
	.size	.L.str.202, 13

	.type	.L.str.203,@object      # @.str.203
.L.str.203:
	.asciz	"CTYPE_LONG"
	.size	.L.str.203, 11

	.type	.L.str.204,@object      # @.str.204
.L.str.204:
	.asciz	"CTYPE_SHORT"
	.size	.L.str.204, 12

	.type	.L.str.205,@object      # @.str.205
.L.str.205:
	.asciz	"CTYPE_CHAR"
	.size	.L.str.205, 11

	.type	.L.str.206,@object      # @.str.206
.L.str.206:
	.asciz	"CTYPE_INT"
	.size	.L.str.206, 10

	.type	.L.str.207,@object      # @.str.207
.L.str.207:
	.asciz	"CTYPE_ULONG"
	.size	.L.str.207, 12

	.type	.L.str.208,@object      # @.str.208
.L.str.208:
	.asciz	"CTYPE_USHORT"
	.size	.L.str.208, 13

	.type	.L.str.209,@object      # @.str.209
.L.str.209:
	.asciz	"CTYPE_UCHAR"
	.size	.L.str.209, 12

	.type	.L.str.210,@object      # @.str.210
.L.str.210:
	.asciz	"CTYPE_UINT"
	.size	.L.str.210, 11

	.type	.L.str.211,@object      # @.str.211
.L.str.211:
	.asciz	"datref"
	.size	.L.str.211, 7

	.type	.L.str.212,@object      # @.str.212
.L.str.212:
	.asciz	"mkdatref"
	.size	.L.str.212, 9

	.type	.L.str.213,@object      # @.str.213
.L.str.213:
	.asciz	"datlength"
	.size	.L.str.213, 10

	.type	.L.str.214,@object      # @.str.214
.L.str.214:
	.asciz	"position-script"
	.size	.L.str.214, 16

	.type	.L.str.215,@object      # @.str.215
.L.str.215:
	.asciz	"Status: 500 Server Error (Application)\n"
	.size	.L.str.215, 40

	.type	.L.str.216,@object      # @.str.216
.L.str.216:
	.asciz	"Content-type: text/html\n\n"
	.size	.L.str.216, 26

	.type	.L.str.217,@object      # @.str.217
.L.str.217:
	.asciz	"<HTML><HEAD><TITLE>Server Error (Application)</TITLE></HEAD>\n"
	.size	.L.str.217, 62

	.type	.L.str.218,@object      # @.str.218
.L.str.218:
	.asciz	"<BODY><H1>Server Error (Application)</H1>\n"
	.size	.L.str.218, 43

	.type	.L.str.219,@object      # @.str.219
.L.str.219:
	.asciz	"An application on this server has encountered an error\n"
	.size	.L.str.219, 56

	.type	.L.str.220,@object      # @.str.220
.L.str.220:
	.asciz	"which prevents it from fulfilling your request."
	.size	.L.str.220, 48

	.type	.L.str.221,@object      # @.str.221
.L.str.221:
	.asciz	"<P><PRE><B>Error Message:</B> "
	.size	.L.str.221, 31

	.type	.L.str.222,@object      # @.str.222
.L.str.222:
	.asciz	"</PRE></BODY></HTML>\n"
	.size	.L.str.222, 22

	.type	.L.str.223,@object      # @.str.223
.L.str.223:
	.asciz	"cgi-main"
	.size	.L.str.223, 9

	.type	.L.str.224,@object      # @.str.224
.L.str.224:
	.asciz	"(repl)"
	.size	.L.str.224, 7

	.type	.L.str.225,@object      # @.str.225
.L.str.225:
	.asciz	"repl"
	.size	.L.str.225, 5

	.type	.L.str.226,@object      # @.str.226
.L.str.226:
	.asciz	"(require \""
	.size	.L.str.226, 11

	.type	.L.str.227,@object      # @.str.227
.L.str.227:
	.asciz	"\\\""
	.size	.L.str.227, 3

	.type	.L.str.228,@object      # @.str.228
.L.str.228:
	.asciz	"\")"
	.size	.L.str.228, 3

	.type	.L.str.229,@object      # @.str.229
.L.str.229:
	.asciz	"*slibu-version*"
	.size	.L.str.229, 16

	.type	.L.str.230,@object      # @.str.230
.L.str.230:
	.asciz	"$Id: slibu.c 35752 2007-04-07 20:56:03Z jeffc $"
	.size	.L.str.230, 48

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"EXIT"
	.size	.Lstr, 5

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.1:
	.asciz	"Content-type: text/plain\r\n\r"
	.size	.Lstr.1, 28

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.quad	.L.str.49
	.quad	.L.str.51
	.quad	.L.str.49
	.quad	.L.str.50
	.quad	.L.str.49
	.quad	.L.str.52
	.quad	.L.str.49
	.quad	.L.str.53
	.quad	.L.str.49
	.quad	.L.str.54
	.quad	.L.str.49
	.quad	.L.str.55
	.size	.Lswitch.table, 96


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
