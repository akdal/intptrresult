	.text
	.file	"Crystal_Cholesky.bc"
	.globl	Crystal_Cholesky
	.p2align	4, 0x90
	.type	Crystal_Cholesky,@function
Crystal_Cholesky:                       # @Crystal_Cholesky
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	jle	.LBB0_55
# BB#1:                                 # %.lr.ph183.preheader
	movl	%edi, %r14d
	cmpl	$3, %edi
	jbe	.LBB0_2
# BB#9:                                 # %min.iters.checked
	movl	%edi, %r8d
	andl	$3, %r8d
	movq	%r14, %rax
	subq	%r8, %rax
	je	.LBB0_2
# BB#10:                                # %vector.memcheck
	leaq	(%rdx,%r14,8), %rbp
	cmpq	%rcx, %rbp
	jbe	.LBB0_12
# BB#11:                                # %vector.memcheck
	leaq	(%rcx,%r14,8), %rbp
	cmpq	%rdx, %rbp
	jbe	.LBB0_12
.LBB0_2:
	xorl	%eax, %eax
.LBB0_3:                                # %.lr.ph183.preheader257
	movl	%r14d, %ebx
	subl	%eax, %ebx
	leaq	-1(%r14), %r8
	subq	%rax, %r8
	andq	$7, %rbx
	je	.LBB0_6
# BB#4:                                 # %.lr.ph183.prol.preheader
	negq	%rbx
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph183.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rax,8), %rbp
	movq	%rbp, (%rcx,%rax,8)
	incq	%rax
	incq	%rbx
	jne	.LBB0_5
.LBB0_6:                                # %.lr.ph183.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB0_15
# BB#7:                                 # %.lr.ph183.preheader257.new
	movq	%r14, %rbx
	subq	%rax, %rbx
	leaq	56(%rcx,%rax,8), %rbp
	leaq	56(%rdx,%rax,8), %rax
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph183
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rax), %rdx
	movq	%rdx, -56(%rbp)
	movq	-48(%rax), %rdx
	movq	%rdx, -48(%rbp)
	movq	-40(%rax), %rdx
	movq	%rdx, -40(%rbp)
	movq	-32(%rax), %rdx
	movq	%rdx, -32(%rbp)
	movq	-24(%rax), %rdx
	movq	%rdx, -24(%rbp)
	movq	-16(%rax), %rdx
	movq	%rdx, -16(%rbp)
	movq	-8(%rax), %rdx
	movq	%rdx, -8(%rbp)
	movq	(%rax), %rdx
	movq	%rdx, (%rbp)
	addq	$64, %rbp
	addq	$64, %rax
	addq	$-8, %rbx
	jne	.LBB0_8
.LBB0_15:                               # %.preheader131
	cmpl	$2, %edi
	jl	.LBB0_55
# BB#16:                                # %.lr.ph180
	testb	$1, %dil
	jne	.LBB0_17
# BB#18:
	movsd	96(%rsi), %xmm0         # xmm0 = mem[0],zero
	divsd	(%rsi), %xmm0
	movsd	%xmm0, 96(%rsi)
	movl	$2, %edx
	cmpl	$2, %edi
	jne	.LBB0_20
	jmp	.LBB0_22
.LBB0_17:
	movl	$1, %edx
	cmpl	$2, %edi
	je	.LBB0_22
.LBB0_20:                               # %.lr.ph180.new
	movq	%r14, %rax
	subq	%rdx, %rax
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$5, %rdx
	addq	%rsi, %rdx
	.p2align	4, 0x90
.LBB0_21:                               # =>This Inner Loop Header: Depth=1
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	divsd	(%rsi), %xmm0
	movsd	%xmm0, (%rdx)
	movsd	96(%rdx), %xmm0         # xmm0 = mem[0],zero
	divsd	(%rsi), %xmm0
	movsd	%xmm0, 96(%rdx)
	addq	$192, %rdx
	addq	$-2, %rax
	jne	.LBB0_21
.LBB0_22:                               # %.preheader130
	cmpl	$2, %edi
	jl	.LBB0_55
# BB#23:                                # %.preheader129.preheader
	movslq	%edi, %r9
	leaq	104(%rsi), %rdx
	leaq	112(%rsi), %r11
	leaq	200(%rsi), %r10
	movl	$2, %r15d
	movl	$1, %ebp
	xorl	%eax, %eax
	xorpd	%xmm0, %xmm0
	movq	%rdx, -80(%rsp)         # 8-byte Spill
	movq	%rsi, -24(%rsp)         # 8-byte Spill
	movq	%rdi, -64(%rsp)         # 8-byte Spill
	movq	%r9, -72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_25:                               # %.lr.ph148.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_30 Depth 2
                                        #     Child Loop BB0_33 Depth 2
                                        #       Child Loop BB0_38 Depth 3
                                        #       Child Loop BB0_46 Depth 3
	movq	%rbp, %r8
	movq	%rax, -88(%rsp)         # 8-byte Spill
	testb	$1, %al
	jne	.LBB0_26
# BB#27:                                # %.lr.ph148.prol
                                        #   in Loop: Header=BB0_25 Depth=1
	leaq	(%r8,%r8,2), %rax
	shlq	$5, %rax
	movsd	(%rsi,%rax), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%rsi,%r8,8), %xmm1
	addsd	%xmm0, %xmm1
	movl	$1, %ebx
	movapd	%xmm1, %xmm2
	cmpq	$0, -88(%rsp)           # 8-byte Folded Reload
	jne	.LBB0_29
	jmp	.LBB0_31
	.p2align	4, 0x90
.LBB0_26:                               #   in Loop: Header=BB0_25 Depth=1
                                        # implicit-def: %XMM1
	xorl	%ebx, %ebx
	xorpd	%xmm2, %xmm2
	cmpq	$0, -88(%rsp)           # 8-byte Folded Reload
	je	.LBB0_31
.LBB0_29:                               # %.lr.ph148.preheader.new
                                        #   in Loop: Header=BB0_25 Depth=1
	movq	%r8, %rax
	subq	%rbx, %rax
	leaq	(%rdx,%rbx,8), %rbp
	leaq	(%rbx,%rbx,2), %rbx
	shlq	$5, %rbx
	addq	-80(%rsp), %rbx         # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB0_30:                               # %.lr.ph148
                                        #   Parent Loop BB0_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rbp), %xmm3         # xmm3 = mem[0],zero
	movsd	(%rbp), %xmm1           # xmm1 = mem[0],zero
	mulsd	-96(%rbx), %xmm3
	addsd	%xmm2, %xmm3
	mulsd	(%rbx), %xmm1
	addsd	%xmm3, %xmm1
	addq	$16, %rbp
	addq	$192, %rbx
	addq	$-2, %rax
	movapd	%xmm1, %xmm2
	jne	.LBB0_30
.LBB0_31:                               # %._crit_edge149
                                        #   in Loop: Header=BB0_25 Depth=1
	leaq	(%r8,%r8,2), %rbx
	shlq	$5, %rbx
	addq	%rsi, %rbx
	movsd	(%rbx,%r8,8), %xmm2     # xmm2 = mem[0],zero
	subsd	%xmm1, %xmm2
	movsd	%xmm2, (%rbx,%r8,8)
	leaq	1(%r8), %rbp
	cmpq	%r9, %rbp
	jge	.LBB0_24
# BB#32:                                # %.preheader128.us.preheader
                                        #   in Loop: Header=BB0_25 Depth=1
	movq	%rbp, -56(%rsp)         # 8-byte Spill
	leaq	(%rbx,%r8,8), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movq	-88(%rsp), %rax         # 8-byte Reload
	movl	%eax, %r13d
	andl	$1, %r13d
	leaq	1(%rax), %r12
	movq	%r10, -40(%rsp)         # 8-byte Spill
	movq	%r11, -32(%rsp)         # 8-byte Spill
	movq	%r11, %r9
	movq	%r15, -48(%rsp)         # 8-byte Spill
	movq	%r15, %r11
	movq	%r14, -8(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_33:                               # %.preheader128.us
                                        #   Parent Loop BB0_25 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_38 Depth 3
                                        #       Child Loop BB0_46 Depth 3
	testq	%r13, %r13
	jne	.LBB0_34
# BB#35:                                #   in Loop: Header=BB0_33 Depth=2
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	mulsd	(%rsi,%r11,8), %xmm1
	addsd	%xmm0, %xmm1
	movl	$1, %edi
	jmp	.LBB0_36
	.p2align	4, 0x90
.LBB0_34:                               #   in Loop: Header=BB0_33 Depth=2
	xorpd	%xmm1, %xmm1
	xorl	%edi, %edi
.LBB0_36:                               # %.prol.loopexit272
                                        #   in Loop: Header=BB0_33 Depth=2
	movq	-88(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	je	.LBB0_39
# BB#37:                                # %.preheader128.us.new
                                        #   in Loop: Header=BB0_33 Depth=2
	leaq	(%rdi,%rdi,2), %rbp
	shlq	$5, %rbp
	addq	%r9, %rbp
	.p2align	4, 0x90
.LBB0_38:                               #   Parent Loop BB0_25 Depth=1
                                        #     Parent Loop BB0_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rdx,%rdi,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	-96(%rbp), %xmm2
	addsd	%xmm1, %xmm2
	movsd	(%rdx,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%rbp), %xmm1
	addsd	%xmm2, %xmm1
	addq	$2, %rdi
	addq	$192, %rbp
	cmpq	%rdi, %r12
	jne	.LBB0_38
.LBB0_39:                               # %.lr.ph160.us.preheader
                                        #   in Loop: Header=BB0_33 Depth=2
	movsd	(%rbx,%r11,8), %xmm2    # xmm2 = mem[0],zero
	subsd	%xmm1, %xmm2
	movsd	%xmm2, (%rbx,%r11,8)
	testq	%r13, %r13
	jne	.LBB0_40
# BB#43:                                # %.lr.ph160.us.prol
                                        #   in Loop: Header=BB0_33 Depth=2
	leaq	(%r11,%r11,2), %rdi
	shlq	$5, %rdi
	movsd	(%rsi,%rdi), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%rsi,%r8,8), %xmm1
	addsd	%xmm0, %xmm1
	movl	$1, %ebp
	movapd	%xmm1, %xmm2
	testq	%rax, %rax
	jne	.LBB0_45
	jmp	.LBB0_41
	.p2align	4, 0x90
.LBB0_40:                               #   in Loop: Header=BB0_33 Depth=2
	xorpd	%xmm2, %xmm2
                                        # implicit-def: %XMM1
	xorl	%ebp, %ebp
	testq	%rax, %rax
	je	.LBB0_41
.LBB0_45:                               # %.lr.ph160.us.preheader.new
                                        #   in Loop: Header=BB0_33 Depth=2
	movq	%r8, %rdi
	subq	%rbp, %rdi
	leaq	(%r10,%rbp,8), %r14
	leaq	(%rbp,%rbp,2), %r15
	shlq	$5, %r15
	addq	-80(%rsp), %r15         # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB0_46:                               # %.lr.ph160.us
                                        #   Parent Loop BB0_25 Depth=1
                                        #     Parent Loop BB0_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%r14), %xmm3         # xmm3 = mem[0],zero
	movsd	(%r14), %xmm1           # xmm1 = mem[0],zero
	mulsd	-96(%r15), %xmm3
	addsd	%xmm2, %xmm3
	mulsd	(%r15), %xmm1
	addsd	%xmm3, %xmm1
	addq	$16, %r14
	addq	$192, %r15
	addq	$-2, %rdi
	movapd	%xmm1, %xmm2
	jne	.LBB0_46
.LBB0_41:                               # %._crit_edge161.us
                                        #   in Loop: Header=BB0_33 Depth=2
	leaq	(%r11,%r11,2), %rdi
	shlq	$5, %rdi
	movq	-24(%rsp), %rsi         # 8-byte Reload
	addq	%rsi, %rdi
	movsd	(%rdi,%r8,8), %xmm2     # xmm2 = mem[0],zero
	subsd	%xmm1, %xmm2
	movq	-16(%rsp), %rax         # 8-byte Reload
	divsd	(%rax), %xmm2
	movsd	%xmm2, (%rdi,%r8,8)
	incq	%r11
	addq	$8, %r9
	addq	$96, %r10
	movq	-8(%rsp), %r14          # 8-byte Reload
	cmpq	%r14, %r11
	jne	.LBB0_33
# BB#42:                                #   in Loop: Header=BB0_25 Depth=1
	movq	-64(%rsp), %rdi         # 8-byte Reload
	movq	-72(%rsp), %r9          # 8-byte Reload
	movq	-32(%rsp), %r11         # 8-byte Reload
	movq	-40(%rsp), %r10         # 8-byte Reload
	movq	-48(%rsp), %r15         # 8-byte Reload
	movq	-56(%rsp), %rbp         # 8-byte Reload
.LBB0_24:                               # %.loopexit
                                        #   in Loop: Header=BB0_25 Depth=1
	incq	%r15
	movq	-88(%rsp), %rax         # 8-byte Reload
	incq	%rax
	addq	$96, %rdx
	addq	$8, -80(%rsp)           # 8-byte Folded Spill
	addq	$8, %r11
	addq	$96, %r10
	cmpq	%r14, %rbp
	jne	.LBB0_25
# BB#47:                                # %.lr.ph142.preheader
	leaq	104(%rsi), %r8
	movl	$1, %edx
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB0_48:                               # %.lr.ph142
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_53 Depth 2
	movsd	(%rcx,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	testb	$1, %r9b
	jne	.LBB0_49
# BB#50:                                #   in Loop: Header=BB0_48 Depth=1
	leaq	(%rdx,%rdx,2), %rax
	shlq	$5, %rax
	movsd	(%rsi,%rax), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%rcx), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%rcx,%rdx,8)
	movl	$1, %ebp
	testq	%r9, %r9
	jne	.LBB0_52
	jmp	.LBB0_54
	.p2align	4, 0x90
.LBB0_49:                               #   in Loop: Header=BB0_48 Depth=1
	xorl	%ebp, %ebp
	testq	%r9, %r9
	je	.LBB0_54
.LBB0_52:                               # %.lr.ph142.new
                                        #   in Loop: Header=BB0_48 Depth=1
	leaq	1(%r9), %rax
	leaq	(%r8,%rbp,8), %rbx
	.p2align	4, 0x90
.LBB0_53:                               #   Parent Loop BB0_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rbx), %xmm1         # xmm1 = mem[0],zero
	mulsd	(%rcx,%rbp,8), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%rcx,%rdx,8)
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	mulsd	8(%rcx,%rbp,8), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%rcx,%rdx,8)
	addq	$2, %rbp
	addq	$16, %rbx
	cmpq	%rbp, %rax
	jne	.LBB0_53
.LBB0_54:                               # %._crit_edge143
                                        #   in Loop: Header=BB0_48 Depth=1
	incq	%rdx
	incq	%r9
	addq	$96, %r8
	cmpq	%r14, %rdx
	jne	.LBB0_48
.LBB0_55:                               # %._crit_edge145
	leal	-1(%rdi), %r11d
	movslq	%r11d, %rax
	movsd	(%rcx,%rax,8), %xmm0    # xmm0 = mem[0],zero
	leaq	(%rax,%rax,2), %rdx
	shlq	$5, %rdx
	addq	%rsi, %rdx
	divsd	(%rdx,%rax,8), %xmm0
	movsd	%xmm0, (%rcx,%rax,8)
	movl	%edi, %eax
	addl	$-2, %eax
	js	.LBB0_64
# BB#56:                                # %.preheader.preheader
	movslq	%eax, %rdx
	movslq	%edi, %r9
	leaq	(%rdx,%rdx,2), %rax
	shlq	$5, %rax
	leaq	8(%rsi,%rax), %r14
	leaq	8(%rcx), %r8
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_57:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_62 Depth 2
	leaq	1(%rdx), %rax
	movsd	(%rcx,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	cmpq	%r9, %rax
	jge	.LBB0_63
# BB#58:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_57 Depth=1
	movslq	%r11d, %r10
	testb	$1, %r15b
	jne	.LBB0_60
# BB#59:                                # %.lr.ph.prol
                                        #   in Loop: Header=BB0_57 Depth=1
	leaq	(%rdx,%rdx,2), %rax
	shlq	$5, %rax
	addq	%rsi, %rax
	movsd	(%rax,%r10,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%rcx,%r10,8), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%rcx,%rdx,8)
	incq	%r10
.LBB0_60:                               # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB0_57 Depth=1
	testl	%r15d, %r15d
	je	.LBB0_63
# BB#61:                                # %.lr.ph.preheader.new
                                        #   in Loop: Header=BB0_57 Depth=1
	movl	%edi, %ebx
	subl	%r10d, %ebx
	leaq	(%r14,%r10,8), %rax
	leaq	(%r8,%r10,8), %rbp
	.p2align	4, 0x90
.LBB0_62:                               # %.lr.ph
                                        #   Parent Loop BB0_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rax), %xmm1         # xmm1 = mem[0],zero
	mulsd	-8(%rbp), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%rcx,%rdx,8)
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	mulsd	(%rbp), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%rcx,%rdx,8)
	addq	$16, %rax
	addq	$16, %rbp
	addl	$-2, %ebx
	jne	.LBB0_62
.LBB0_63:                               # %._crit_edge
                                        #   in Loop: Header=BB0_57 Depth=1
	leaq	(%rdx,%rdx,2), %rax
	shlq	$5, %rax
	addq	%rsi, %rax
	divsd	(%rax,%rdx,8), %xmm0
	movsd	%xmm0, (%rcx,%rdx,8)
	decl	%r11d
	incl	%r15d
	addq	$-96, %r14
	testq	%rdx, %rdx
	leaq	-1(%rdx), %rdx
	jg	.LBB0_57
.LBB0_64:                               # %._crit_edge140
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_12:                               # %vector.body.preheader
	leaq	16(%rdx), %rbx
	leaq	16(%rcx), %rbp
	movq	%rax, %r9
	.p2align	4, 0x90
.LBB0_13:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rbx), %xmm0
	movupd	(%rbx), %xmm1
	movupd	%xmm0, -16(%rbp)
	movupd	%xmm1, (%rbp)
	addq	$32, %rbx
	addq	$32, %rbp
	addq	$-4, %r9
	jne	.LBB0_13
# BB#14:                                # %middle.block
	testl	%r8d, %r8d
	jne	.LBB0_3
	jmp	.LBB0_15
.Lfunc_end0:
	.size	Crystal_Cholesky, .Lfunc_end0-Crystal_Cholesky
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
