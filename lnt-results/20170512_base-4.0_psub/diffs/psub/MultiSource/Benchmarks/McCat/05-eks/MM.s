	.text
	.file	"MM.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	-4623935809413835653    # double -0.32000000000000001
.LCPI0_1:
	.quad	4599420984095052251     # double 0.3191538243211462
	.text
	.globl	MakeMatrix
	.p2align	4, 0x90
	.type	MakeMatrix,@function
MakeMatrix:                             # @MakeMatrix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movl	$408, %edi              # imm = 0x198
	callq	malloc
	movq	%rax, %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movl	$408, %edi              # imm = 0x198
	callq	malloc
	movq	%rax, (%r15,%rbx,8)
	incq	%rbx
	cmpq	$51, %rbx
	jne	.LBB0_1
# BB#2:                                 # %.preheader.preheader
	movslq	%ebp, %r14
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB0_4:                                #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorpd	%xmm0, %xmm0
	cmpq	%r14, %rbp
	jg	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=2
	cvtsi2sdl	%r13d, %xmm1
	movapd	%xmm1, %xmm0
	mulsd	.LCPI0_0(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	callq	exp
	mulsd	.LCPI0_1(%rip), %xmm0
.LBB0_6:                                #   in Loop: Header=BB0_4 Depth=2
	movq	(%r15,%rbx,8), %rax
	movsd	%xmm0, (%rax,%r12,8)
	movq	(%r15,%r12,8), %rax
	movsd	%xmm0, (%rax,%rbx,8)
	incq	%rbx
	incq	%rbp
	decl	%r13d
	cmpq	$51, %rbx
	jne	.LBB0_4
# BB#7:                                 # %newMatrix.exit
                                        #   in Loop: Header=BB0_3 Depth=1
	incq	%r12
	cmpq	$51, %r12
	jne	.LBB0_3
# BB#8:
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	MakeMatrix, .Lfunc_end0-MakeMatrix
	.cfi_endproc

	.globl	newMatrix
	.p2align	4, 0x90
	.type	newMatrix,@function
newMatrix:                              # @newMatrix
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movl	$408, %edi              # imm = 0x198
	callq	malloc
	movq	%rax, %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movl	$408, %edi              # imm = 0x198
	callq	malloc
	movq	%rax, (%r14,%rbx,8)
	incq	%rbx
	cmpq	$51, %rbx
	jne	.LBB1_1
# BB#2:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	newMatrix, .Lfunc_end1-newMatrix
	.cfi_endproc

	.globl	newVector
	.p2align	4, 0x90
	.type	newVector,@function
newVector:                              # @newVector
	.cfi_startproc
# BB#0:
	movl	$408, %edi              # imm = 0x198
	jmp	malloc                  # TAILCALL
.Lfunc_end2:
	.size	newVector, .Lfunc_end2-newVector
	.cfi_endproc

	.globl	matrixMult
	.p2align	4, 0x90
	.type	matrixMult,@function
matrixMult:                             # @matrixMult
	.cfi_startproc
# BB#0:
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB3_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_2 Depth 2
                                        #       Child Loop BB3_3 Depth 3
	movq	(%rdi,%r8,8), %r9
	movq	(%rsi,%r8,8), %r10
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB3_2:                                #   Parent Loop BB3_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_3 Depth 3
	movq	$0, (%r9,%r11,8)
	xorpd	%xmm0, %xmm0
	movl	$2, %eax
	.p2align	4, 0x90
.LBB3_3:                                #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-16(%r10,%rax,8), %xmm1 # xmm1 = mem[0],zero
	movq	-16(%rdx,%rax,8), %rcx
	mulsd	(%rcx,%r11,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%r9,%r11,8)
	movsd	-8(%r10,%rax,8), %xmm2  # xmm2 = mem[0],zero
	movq	-8(%rdx,%rax,8), %rcx
	mulsd	(%rcx,%r11,8), %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, (%r9,%r11,8)
	movsd	(%r10,%rax,8), %xmm0    # xmm0 = mem[0],zero
	movq	(%rdx,%rax,8), %rcx
	mulsd	(%rcx,%r11,8), %xmm0
	addsd	%xmm2, %xmm0
	movsd	%xmm0, (%r9,%r11,8)
	addq	$3, %rax
	cmpq	$53, %rax
	jne	.LBB3_3
# BB#4:                                 #   in Loop: Header=BB3_2 Depth=2
	incq	%r11
	cmpq	$51, %r11
	jne	.LBB3_2
# BB#5:                                 #   in Loop: Header=BB3_1 Depth=1
	incq	%r8
	cmpq	$51, %r8
	jne	.LBB3_1
# BB#6:
	retq
.Lfunc_end3:
	.size	matrixMult, .Lfunc_end3-matrixMult
	.cfi_endproc

	.globl	matrixTranspose
	.p2align	4, 0x90
	.type	matrixTranspose,@function
matrixTranspose:                        # @matrixTranspose
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -32
.Lcfi22:
	.cfi_offset %r14, -24
.Lcfi23:
	.cfi_offset %r15, -16
	movb	$2, %r9b
	movl	$1, %r11d
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB4_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_5 Depth 2
                                        #     Child Loop BB4_8 Depth 2
	movq	%r8, %r15
	leaq	1(%r15), %r8
	cmpq	$50, %r8
	jg	.LBB4_1
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	$49, %r10d
	subq	%r15, %r10
	movl	$50, %eax
	subl	%r15d, %eax
	movq	(%rdi,%r15,8), %rdx
	testb	$3, %al
	movq	%r11, %rcx
	je	.LBB4_6
# BB#4:                                 # %.prol.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	%r9d, %eax
	andb	$3, %al
	movzbl	%al, %eax
	negq	%rax
	movq	%r11, %rcx
	.p2align	4, 0x90
.LBB4_5:                                #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rcx,8), %r14
	movq	(%rdi,%rcx,8), %rbx
	movq	(%rbx,%r15,8), %rsi
	movq	%rsi, (%rdx,%rcx,8)
	movq	%r14, (%rbx,%r15,8)
	incq	%rcx
	incq	%rax
	jne	.LBB4_5
.LBB4_6:                                # %.prol.loopexit
                                        #   in Loop: Header=BB4_2 Depth=1
	cmpq	$3, %r10
	jb	.LBB4_1
# BB#7:                                 # %.lr.ph.new
                                        #   in Loop: Header=BB4_2 Depth=1
	addq	$3, %rcx
	.p2align	4, 0x90
.LBB4_8:                                #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rdx,%rcx,8), %rax
	movq	-24(%rdi,%rcx,8), %rsi
	movq	(%rsi,%r15,8), %rbx
	movq	%rbx, -24(%rdx,%rcx,8)
	movq	%rax, (%rsi,%r15,8)
	movq	-16(%rdx,%rcx,8), %rax
	movq	-16(%rdi,%rcx,8), %rsi
	movq	(%rsi,%r15,8), %rbx
	movq	%rbx, -16(%rdx,%rcx,8)
	movq	%rax, (%rsi,%r15,8)
	movq	-8(%rdx,%rcx,8), %rax
	movq	-8(%rdi,%rcx,8), %rsi
	movq	(%rsi,%r15,8), %rbx
	movq	%rbx, -8(%rdx,%rcx,8)
	movq	%rax, (%rsi,%r15,8)
	movq	(%rdx,%rcx,8), %rax
	movq	(%rdi,%rcx,8), %rsi
	movq	(%rsi,%r15,8), %rbx
	movq	%rbx, (%rdx,%rcx,8)
	movq	%rax, (%rsi,%r15,8)
	addq	$4, %rcx
	cmpq	$54, %rcx
	jne	.LBB4_8
.LBB4_1:                                # %.loopexit
                                        #   in Loop: Header=BB4_2 Depth=1
	incq	%r11
	addb	$3, %r9b
	cmpq	$51, %r8
	jne	.LBB4_2
# BB#9:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	matrixTranspose, .Lfunc_end4-matrixTranspose
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	newIdMatrix
	.p2align	4, 0x90
	.type	newIdMatrix,@function
newIdMatrix:                            # @newIdMatrix
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -24
.Lcfi28:
	.cfi_offset %r14, -16
	movl	$408, %edi              # imm = 0x198
	callq	malloc
	movq	%rax, %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	movl	$408, %edi              # imm = 0x198
	callq	malloc
	movq	%rax, (%r14,%rbx,8)
	incq	%rbx
	cmpq	$51, %rbx
	jne	.LBB5_1
# BB#2:                                 # %.lr.ph.i.preheader
	xorl	%eax, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_6 Depth 2
	movl	$51, %esi
	subl	%eax, %esi
	movq	(%r14,%rax,8), %rdx
	testb	$1, %sil
	movq	%rax, %rsi
	je	.LBB5_5
# BB#4:                                 #   in Loop: Header=BB5_3 Depth=1
	movq	%rcx, (%rdx,%rax,8)
	leaq	1(%rax), %rsi
.LBB5_5:                                # %.prol.loopexit
                                        #   in Loop: Header=BB5_3 Depth=1
	cmpq	$50, %rax
	je	.LBB5_11
	.p2align	4, 0x90
.LBB5_6:                                #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rsi, %rax
	movapd	%xmm0, %xmm1
	je	.LBB5_8
# BB#7:                                 #   in Loop: Header=BB5_6 Depth=2
	movq	(%r14,%rsi,8), %rdi
	movq	$0, (%rdi,%rax,8)
	xorpd	%xmm1, %xmm1
.LBB5_8:                                #   in Loop: Header=BB5_6 Depth=2
	movsd	%xmm1, (%rdx,%rsi,8)
	leaq	1(%rsi), %rdi
	cmpq	%rdi, %rax
	movapd	%xmm0, %xmm1
	je	.LBB5_10
# BB#9:                                 #   in Loop: Header=BB5_6 Depth=2
	movq	8(%r14,%rsi,8), %rbx
	movq	$0, (%rbx,%rax,8)
	xorpd	%xmm1, %xmm1
.LBB5_10:                               #   in Loop: Header=BB5_6 Depth=2
	movsd	%xmm1, 8(%rdx,%rsi,8)
	incq	%rdi
	cmpq	$51, %rdi
	movq	%rdi, %rsi
	jne	.LBB5_6
.LBB5_11:                               # %._crit_edge.i
                                        #   in Loop: Header=BB5_3 Depth=1
	incq	%rax
	cmpq	$51, %rax
	jne	.LBB5_3
# BB#12:                                # %MakeID.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	newIdMatrix, .Lfunc_end5-newIdMatrix
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	MakeID
	.p2align	4, 0x90
	.type	MakeID,@function
MakeID:                                 # @MakeID
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	movsd	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movabsq	$4607182418800017408, %r8 # imm = 0x3FF0000000000000
	.p2align	4, 0x90
.LBB6_1:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_4 Depth 2
	movl	$51, %ecx
	subl	%eax, %ecx
	movq	(%rdi,%rax,8), %r9
	testb	$1, %cl
	movq	%rax, %rcx
	je	.LBB6_3
# BB#2:                                 #   in Loop: Header=BB6_1 Depth=1
	movq	%r8, (%r9,%rax,8)
	leaq	1(%rax), %rcx
.LBB6_3:                                # %.prol.loopexit
                                        #   in Loop: Header=BB6_1 Depth=1
	cmpq	$50, %rax
	je	.LBB6_9
	.p2align	4, 0x90
.LBB6_4:                                #   Parent Loop BB6_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %rax
	movapd	%xmm0, %xmm1
	je	.LBB6_6
# BB#5:                                 #   in Loop: Header=BB6_4 Depth=2
	movq	(%rdi,%rcx,8), %rsi
	movq	$0, (%rsi,%rax,8)
	xorpd	%xmm1, %xmm1
.LBB6_6:                                #   in Loop: Header=BB6_4 Depth=2
	movsd	%xmm1, (%r9,%rcx,8)
	leaq	1(%rcx), %rsi
	cmpq	%rsi, %rax
	movapd	%xmm0, %xmm1
	je	.LBB6_8
# BB#7:                                 #   in Loop: Header=BB6_4 Depth=2
	movq	8(%rdi,%rcx,8), %rdx
	movq	$0, (%rdx,%rax,8)
	xorpd	%xmm1, %xmm1
.LBB6_8:                                #   in Loop: Header=BB6_4 Depth=2
	movsd	%xmm1, 8(%r9,%rcx,8)
	incq	%rsi
	cmpq	$51, %rsi
	movq	%rsi, %rcx
	jne	.LBB6_4
.LBB6_9:                                # %._crit_edge
                                        #   in Loop: Header=BB6_1 Depth=1
	incq	%rax
	cmpq	$51, %rax
	jne	.LBB6_1
# BB#10:
	retq
.Lfunc_end6:
	.size	MakeID, .Lfunc_end6-MakeID
	.cfi_endproc

	.globl	freeMatrix
	.p2align	4, 0x90
	.type	freeMatrix,@function
freeMatrix:                             # @freeMatrix
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -24
.Lcfi33:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbx,8), %rdi
	callq	free
	incq	%rbx
	cmpq	$51, %rbx
	jne	.LBB7_1
# BB#2:
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end7:
	.size	freeMatrix, .Lfunc_end7-freeMatrix
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	NormInf
	.p2align	4, 0x90
	.type	NormInf,@function
NormInf:                                # @NormInf
	.cfi_startproc
# BB#0:
	xorpd	%xmm2, %xmm2
	xorl	%eax, %eax
	movapd	.LCPI8_0(%rip), %xmm1   # xmm1 = [nan,nan]
	.p2align	4, 0x90
.LBB8_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_2 Depth 2
	movq	(%rdi,%rax,8), %rcx
	movl	$2, %edx
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB8_2:                                #   Parent Loop BB8_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-16(%rcx,%rdx,8), %xmm3 # xmm3 = mem[0],zero
	andpd	%xmm1, %xmm3
	addsd	%xmm0, %xmm3
	movsd	-8(%rcx,%rdx,8), %xmm4  # xmm4 = mem[0],zero
	andpd	%xmm1, %xmm4
	addsd	%xmm3, %xmm4
	movsd	(%rcx,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	andpd	%xmm1, %xmm0
	addsd	%xmm4, %xmm0
	addq	$3, %rdx
	cmpq	$53, %rdx
	jne	.LBB8_2
# BB#3:                                 #   in Loop: Header=BB8_1 Depth=1
	maxsd	%xmm2, %xmm0
	incq	%rax
	cmpq	$51, %rax
	movapd	%xmm0, %xmm2
	jne	.LBB8_1
# BB#4:
	retq
.Lfunc_end8:
	.size	NormInf, .Lfunc_end8-NormInf
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	NormOne
	.p2align	4, 0x90
	.type	NormOne,@function
NormOne:                                # @NormOne
	.cfi_startproc
# BB#0:
	xorpd	%xmm2, %xmm2
	xorl	%eax, %eax
	movapd	.LCPI9_0(%rip), %xmm1   # xmm1 = [nan,nan]
	.p2align	4, 0x90
.LBB9_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_2 Depth 2
	movl	$2, %ecx
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB9_2:                                #   Parent Loop BB9_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-16(%rdi,%rcx,8), %rdx
	movsd	(%rdx,%rax,8), %xmm3    # xmm3 = mem[0],zero
	andpd	%xmm1, %xmm3
	addsd	%xmm0, %xmm3
	movq	-8(%rdi,%rcx,8), %rdx
	movsd	(%rdx,%rax,8), %xmm4    # xmm4 = mem[0],zero
	andpd	%xmm1, %xmm4
	addsd	%xmm3, %xmm4
	movq	(%rdi,%rcx,8), %rdx
	movsd	(%rdx,%rax,8), %xmm0    # xmm0 = mem[0],zero
	andpd	%xmm1, %xmm0
	addsd	%xmm4, %xmm0
	addq	$3, %rcx
	cmpq	$53, %rcx
	jne	.LBB9_2
# BB#3:                                 #   in Loop: Header=BB9_1 Depth=1
	maxsd	%xmm2, %xmm0
	incq	%rax
	cmpq	$51, %rax
	movapd	%xmm0, %xmm2
	jne	.LBB9_1
# BB#4:
	retq
.Lfunc_end9:
	.size	NormOne, .Lfunc_end9-NormOne
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
