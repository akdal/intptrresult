	.text
	.file	"asearch.bc"
	.globl	asearch0
	.p2align	4, 0x90
	.type	asearch0,@function
asearch0:                               # @asearch0
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$98568, %rsp            # imm = 0x18108
.Lcfi6:
	.cfi_def_cfa_offset 98624
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	%esi, 32(%rsp)          # 4-byte Spill
	movq	%rdi, 232(%rsp)         # 8-byte Spill
	callq	strlen
	movb	$10, 49407(%rsp)
	movl	D_endpos(%rip), %ecx
	cmpl	$2, %eax
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	%ecx, %edx
	jb	.LBB0_8
# BB#1:                                 # %.lr.ph229.preheader
	leal	7(%rax), %ebp
	leal	-2(%rax), %edi
	andl	$7, %ebp
	je	.LBB0_5
# BB#2:                                 # %.lr.ph229.prol.preheader
	xorl	%ecx, %ecx
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	%esi, %edx
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph229.prol
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rdx), %esi
	orl	%esi, %edx
	incl	%ecx
	cmpl	%ecx, %ebp
	jne	.LBB0_3
# BB#4:                                 # %.lr.ph229.prol.loopexit.unr-lcssa
	incl	%ecx
	cmpl	$7, %edi
	jae	.LBB0_6
	jmp	.LBB0_8
.LBB0_5:
	movl	$1, %ecx
	movl	12(%rsp), %edx          # 4-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	cmpl	$7, %edi
	jb	.LBB0_8
.LBB0_6:                                # %.lr.ph229.preheader.new
	movl	%eax, %esi
	subl	%ecx, %esi
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph229
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rdx), %ecx
	orl	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	orl	%ecx, %edx
	leal	(%rdx,%rdx), %ecx
	orl	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	orl	%ecx, %edx
	leal	(%rdx,%rdx), %ecx
	orl	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	orl	%ecx, %edx
	leal	(%rdx,%rdx), %ecx
	orl	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	orl	%ecx, %edx
	addl	$-8, %esi
	jne	.LBB0_7
.LBB0_8:                                # %._crit_edge230
	movl	Init1(%rip), %r13d
	movl	NO_ERR_MASK(%rip), %r14d
	movl	Init(%rip), %ebx
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	1(%rcx), %ecx
	cmpl	$7, %ecx
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	jbe	.LBB0_13
# BB#9:                                 # %min.iters.checked
	movl	%ecx, %eax
	andl	$7, %eax
	movq	%rcx, %r12
	subq	%rax, %r12
	je	.LBB0_13
# BB#10:                                # %vector.ph
	movd	%ebx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	96(%rsp), %rsi
	leaq	144(%rsp), %rdi
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB0_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, -16(%rsi)
	movdqa	%xmm0, (%rsi)
	movdqa	%xmm0, -16(%rdi)
	movdqa	%xmm0, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-8, %rbp
	jne	.LBB0_11
# BB#12:                                # %middle.block
	testl	%eax, %eax
	jne	.LBB0_14
	jmp	.LBB0_15
.LBB0_13:
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_14:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, 80(%rsp,%r12,4)
	movl	%ebx, 128(%rsp,%r12,4)
	incq	%r12
	cmpq	%r12, %rcx
	jne	.LBB0_14
.LBB0_15:                               # %.preheader195
	leaq	49408(%rsp), %rsi
	movl	$49152, 8(%rsp)         # 4-byte Folded Spill
                                        # imm = 0xC000
	movl	$49152, %edx            # imm = 0xC000
	movl	32(%rsp), %edi          # 4-byte Reload
	callq	fill_buf
	movl	%eax, %ebp
	testl	%ebp, %ebp
	movq	16(%rsp), %r10          # 8-byte Reload
	jle	.LBB0_105
# BB#16:                                # %.lr.ph224
	movq	48(%rsp), %rax          # 8-byte Reload
	notl	%eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rcx          # 8-byte Reload
	movslq	%ecx, %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movl	%r10d, %eax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	movl	%ecx, %eax
	notl	%eax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	leal	-1(%r12), %eax
	leaq	-2(%r12), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	leaq	-8(%r12), %rcx
	movq	%rcx, 208(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	shrl	$3, %ecx
	incl	%ecx
	andl	$1, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%r12, %rax
	andq	$-8, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	andl	$7, %ecx
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	negq	%rcx
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	movl	$49151, %r11d           # imm = 0xBFFF
	xorl	%r9d, %r9d
.LBB0_17:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_21 Depth 2
                                        #       Child Loop BB0_26 Depth 3
                                        #       Child Loop BB0_43 Depth 3
                                        #       Child Loop BB0_83 Depth 3
                                        #       Child Loop BB0_46 Depth 3
                                        #       Child Loop BB0_53 Depth 3
                                        #       Child Loop BB0_60 Depth 3
                                        #       Child Loop BB0_78 Depth 3
                                        #       Child Loop BB0_88 Depth 3
                                        #       Child Loop BB0_91 Depth 3
                                        #       Child Loop BB0_97 Depth 3
	leal	49152(%rbp), %eax
	cmpl	$49151, %ebp            # imm = 0xBFFF
	movq	%rax, 176(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	jg	.LBB0_19
# BB#18:                                #   in Loop: Header=BB0_17 Depth=1
	movq	176(%rsp), %r15         # 8-byte Reload
	movl	%r15d, %eax
	leaq	256(%rsp,%rax), %rdi
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	224(%rsp), %rdx         # 8-byte Reload
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movq	%r11, %rbx
	callq	strncpy
	movq	%rbx, %r11
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	leal	(%r15,%rax), %eax
	movb	$0, 256(%rsp,%rax)
.LBB0_19:                               # %.preheader
                                        #   in Loop: Header=BB0_17 Depth=1
	cmpl	%eax, %r11d
	movq	%rbp, 240(%rsp)         # 8-byte Spill
	jae	.LBB0_99
# BB#20:                                # %.lr.ph218
                                        #   in Loop: Header=BB0_17 Depth=1
	leal	49151(%rbp), %ecx
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	movq	%rax, 248(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_21:                               #   Parent Loop BB0_17 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_26 Depth 3
                                        #       Child Loop BB0_43 Depth 3
                                        #       Child Loop BB0_83 Depth 3
                                        #       Child Loop BB0_46 Depth 3
                                        #       Child Loop BB0_53 Depth 3
                                        #       Child Loop BB0_60 Depth 3
                                        #       Child Loop BB0_78 Depth 3
                                        #       Child Loop BB0_88 Depth 3
                                        #       Child Loop BB0_91 Depth 3
                                        #       Child Loop BB0_97 Depth 3
	movl	%r11d, %eax
	movzbl	256(%rsp,%rax), %eax
	movl	Mask(,%rax,4), %r15d
	movl	80(%rsp), %r8d
	movl	%r8d, %eax
	andl	%r13d, %eax
	movl	%r8d, %edx
	shrl	%edx
	andl	%r15d, %edx
	orl	%edx, %eax
	testl	%r10d, %r10d
	movl	%eax, 128(%rsp)
	je	.LBB0_28
# BB#22:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_21 Depth=2
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_24
# BB#23:                                #   in Loop: Header=BB0_21 Depth=2
	movl	$1, %edi
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_25
	jmp	.LBB0_27
	.p2align	4, 0x90
.LBB0_24:                               # %.lr.ph.prol
                                        #   in Loop: Header=BB0_21 Depth=2
	movl	84(%rsp), %esi
	movl	%esi, %eax
	andl	%r13d, %eax
	orl	%r8d, %edx
	shrl	%edx
	andl	%r14d, %edx
	orl	%r8d, %eax
	movl	%esi, %r8d
	shrl	%esi
	andl	%r15d, %esi
	orl	%esi, %eax
	orl	%edx, %eax
	movl	%eax, 132(%rsp)
	movl	$2, %edi
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	je	.LBB0_27
.LBB0_25:                               # %.lr.ph.preheader.new
                                        #   in Loop: Header=BB0_21 Depth=2
	movq	%r12, %rdx
	subq	%rdi, %rdx
	leaq	84(%rsp), %rcx
	leaq	(%rcx,%rdi,4), %rsi
	leaq	132(%rsp), %rcx
	leaq	(%rcx,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB0_26:                               # %.lr.ph
                                        #   Parent Loop BB0_17 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rsi), %ecx
	movl	%ecx, %ebx
	andl	%r13d, %ebx
	orl	%r8d, %eax
	shrl	%eax
	andl	%r14d, %eax
	movl	%ecx, %ebp
	shrl	%ebp
	andl	%r15d, %ebp
	orl	%r8d, %ebx
	orl	%ebp, %ebx
	orl	%eax, %ebx
	movl	%ebx, -4(%rdi)
	movl	(%rsi), %r8d
	movl	%r8d, %eax
	andl	%r13d, %eax
	orl	%ecx, %ebx
	shrl	%ebx
	andl	%r14d, %ebx
	movl	%r8d, %ebp
	shrl	%ebp
	andl	%r15d, %ebp
	orl	%ecx, %eax
	orl	%ebp, %eax
	orl	%ebx, %eax
	movl	%eax, (%rdi)
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rdx
	jne	.LBB0_26
.LBB0_27:                               # %._crit_edge.loopexit
                                        #   in Loop: Header=BB0_21 Depth=2
	movl	128(%rsp), %eax
.LBB0_28:                               # %._crit_edge
                                        #   in Loop: Header=BB0_21 Depth=2
	leal	1(%r11), %edi
	testl	12(%rsp), %eax          # 4-byte Folded Reload
	je	.LBB0_55
# BB#29:                                #   in Loop: Header=BB0_21 Depth=2
	incl	%r9d
	movq	216(%rsp), %rax         # 8-byte Reload
	movl	128(%rsp,%rax,4), %eax
	movl	AND(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB0_33
# BB#30:                                #   in Loop: Header=BB0_21 Depth=2
	cmpl	$1, %ecx
	jne	.LBB0_34
# BB#31:                                #   in Loop: Header=BB0_21 Depth=2
	movl	endposition(%rip), %ecx
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.LBB0_36
.LBB0_34:                               #   in Loop: Header=BB0_21 Depth=2
	xorl	%eax, %eax
	jmp	.LBB0_35
	.p2align	4, 0x90
.LBB0_33:                               #   in Loop: Header=BB0_21 Depth=2
	testl	endposition(%rip), %eax
	setne	%al
.LBB0_35:                               # %thread-pre-split.thread
                                        #   in Loop: Header=BB0_21 Depth=2
	movzbl	%al, %eax
	cmpl	INVERSE(%rip), %eax
	je	.LBB0_39
.LBB0_36:                               #   in Loop: Header=BB0_21 Depth=2
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB0_104
# BB#37:                                #   in Loop: Header=BB0_21 Depth=2
	movl	8(%rsp), %eax           # 4-byte Reload
	cmpl	36(%rsp), %eax          # 4-byte Folded Reload
	jge	.LBB0_39
# BB#38:                                #   in Loop: Header=BB0_21 Depth=2
	movq	192(%rsp), %rax         # 8-byte Reload
	leal	(%rdi,%rax), %edx
	movq	%rdi, 168(%rsp)         # 8-byte Spill
	leaq	256(%rsp), %rdi
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	%r9d, %ecx
	movq	%r9, %rbx
	movq	%r11, %rbp
	callq	output
	movq	168(%rsp), %rdi         # 8-byte Reload
	movq	%rbp, %r11
	movq	%rbx, %r9
	movq	16(%rsp), %r10          # 8-byte Reload
.LBB0_39:                               #   in Loop: Header=BB0_21 Depth=2
	cmpq	$8, %r12
	movl	Init(%rip), %eax
	movq	%r11, 40(%rsp)          # 8-byte Spill
	jb	.LBB0_44
# BB#40:                                # %min.iters.checked285
                                        #   in Loop: Header=BB0_21 Depth=2
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB0_44
# BB#41:                                # %vector.ph289
                                        #   in Loop: Header=BB0_21 Depth=2
	cmpq	$0, 200(%rsp)           # 8-byte Folded Reload
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	je	.LBB0_80
# BB#42:                                # %vector.body281.prol.preheader
                                        #   in Loop: Header=BB0_21 Depth=2
	movq	184(%rsp), %rcx         # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_43:                               # %vector.body281.prol
                                        #   Parent Loop BB0_17 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqa	%xmm0, 80(%rsp,%rdx,4)
	movdqa	%xmm0, 96(%rsp,%rdx,4)
	addq	$8, %rdx
	incq	%rcx
	jne	.LBB0_43
	jmp	.LBB0_81
	.p2align	4, 0x90
.LBB0_44:                               #   in Loop: Header=BB0_21 Depth=2
	movq	%r9, %r11
	xorl	%esi, %esi
.LBB0_45:                               # %scalar.ph283.preheader
                                        #   in Loop: Header=BB0_21 Depth=2
	leaq	80(%rsp,%rsi,4), %rcx
	movq	%r12, %rdx
	subq	%rsi, %rdx
	.p2align	4, 0x90
.LBB0_46:                               # %scalar.ph283
                                        #   Parent Loop BB0_17 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, (%rcx)
	addq	$4, %rcx
	decq	%rdx
	jne	.LBB0_46
.LBB0_47:                               # %.loopexit300
                                        #   in Loop: Header=BB0_21 Depth=2
	movl	%edi, %eax
	subl	56(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	80(%rsp), %r9d
	movl	%r9d, %edx
	andl	%r13d, %edx
	movl	%r9d, %eax
	shrl	%eax
	andl	%r15d, %eax
	orl	%edx, %eax
	andl	48(%rsp), %eax          # 4-byte Folded Reload
	testl	%r10d, %r10d
	movl	%eax, 128(%rsp)
	je	.LBB0_50
# BB#48:                                # %.lr.ph206
                                        #   in Loop: Header=BB0_21 Depth=2
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	movl	Init1(%rip), %r8d
	movq	%rdi, 168(%rsp)         # 8-byte Spill
	jne	.LBB0_51
# BB#49:                                #   in Loop: Header=BB0_21 Depth=2
	movl	$1, %edx
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_52
	jmp	.LBB0_54
	.p2align	4, 0x90
.LBB0_50:                               #   in Loop: Header=BB0_21 Depth=2
	movq	%r11, %r9
	movq	40(%rsp), %r11          # 8-byte Reload
	jmp	.LBB0_55
.LBB0_51:                               #   in Loop: Header=BB0_21 Depth=2
	movl	84(%rsp), %edx
	movl	%edx, %esi
	andl	%r8d, %esi
	orl	%r9d, %eax
	shrl	%eax
	andl	%r14d, %eax
	orl	%r9d, %esi
	movl	%edx, %r9d
	shrl	%edx
	andl	%r15d, %edx
	orl	%edx, %esi
	orl	%esi, %eax
	movl	%eax, 132(%rsp)
	movl	$2, %edx
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	je	.LBB0_54
.LBB0_52:                               # %.lr.ph206.new
                                        #   in Loop: Header=BB0_21 Depth=2
	movq	%r12, %rsi
	subq	%rdx, %rsi
	leaq	84(%rsp), %rcx
	leaq	(%rcx,%rdx,4), %rdi
	leaq	132(%rsp), %rcx
	leaq	(%rcx,%rdx,4), %rbx
	.p2align	4, 0x90
.LBB0_53:                               #   Parent Loop BB0_17 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rdi), %ecx
	movl	%ecx, %ebp
	andl	%r8d, %ebp
	orl	%r9d, %eax
	shrl	%eax
	andl	%r14d, %eax
	movl	%ecx, %edx
	shrl	%edx
	andl	%r15d, %edx
	orl	%r9d, %ebp
	orl	%edx, %ebp
	orl	%eax, %ebp
	movl	%ebp, -4(%rbx)
	movl	(%rdi), %r9d
	movl	%r9d, %eax
	andl	%r8d, %eax
	orl	%ecx, %ebp
	shrl	%ebp
	andl	%r14d, %ebp
	movl	%r9d, %edx
	shrl	%edx
	andl	%r15d, %edx
	orl	%ecx, %eax
	orl	%edx, %eax
	orl	%ebp, %eax
	movl	%eax, (%rbx)
	addq	$8, %rdi
	addq	$8, %rbx
	addq	$-2, %rsi
	jne	.LBB0_53
.LBB0_54:                               # %.loopexit194.loopexit
                                        #   in Loop: Header=BB0_21 Depth=2
	movl	128(%rsp), %eax
	movq	%r11, %r9
	movq	40(%rsp), %r11          # 8-byte Reload
	movq	168(%rsp), %rdi         # 8-byte Reload
.LBB0_55:                               # %.loopexit194
                                        #   in Loop: Header=BB0_21 Depth=2
	movl	%edi, %ecx
	movzbl	256(%rsp,%rcx), %ecx
	movl	Mask(,%rcx,4), %r15d
	movl	%eax, %ecx
	andl	%r13d, %ecx
	movl	%eax, %edx
	shrl	%edx
	andl	%r15d, %edx
	orl	%edx, %ecx
	testl	%r10d, %r10d
	movl	%ecx, 80(%rsp)
	je	.LBB0_62
# BB#56:                                # %.lr.ph209.preheader
                                        #   in Loop: Header=BB0_21 Depth=2
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_58
# BB#57:                                #   in Loop: Header=BB0_21 Depth=2
	movl	$1, %edi
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_59
	jmp	.LBB0_61
	.p2align	4, 0x90
.LBB0_58:                               # %.lr.ph209.prol
                                        #   in Loop: Header=BB0_21 Depth=2
	movl	132(%rsp), %esi
	movl	%esi, %ecx
	andl	%r13d, %ecx
	orl	%eax, %edx
	shrl	%edx
	andl	%r14d, %edx
	orl	%eax, %ecx
	movl	%esi, %eax
	shrl	%esi
	andl	%r15d, %esi
	orl	%esi, %ecx
	orl	%edx, %ecx
	movl	%ecx, 84(%rsp)
	movl	$2, %edi
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	je	.LBB0_61
.LBB0_59:                               # %.lr.ph209.preheader.new
                                        #   in Loop: Header=BB0_21 Depth=2
	movq	%r12, %r8
	subq	%rdi, %r8
	leaq	132(%rsp), %rdx
	leaq	(%rdx,%rdi,4), %rsi
	leaq	84(%rsp), %rdx
	leaq	(%rdx,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB0_60:                               # %.lr.ph209
                                        #   Parent Loop BB0_17 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rsi), %edx
	movl	%edx, %ebx
	andl	%r13d, %ebx
	orl	%eax, %ecx
	shrl	%ecx
	andl	%r14d, %ecx
	movl	%edx, %ebp
	shrl	%ebp
	andl	%r15d, %ebp
	orl	%eax, %ebx
	orl	%ebp, %ebx
	orl	%ecx, %ebx
	movl	%ebx, -4(%rdi)
	movl	(%rsi), %eax
	movl	%eax, %ecx
	andl	%r13d, %ecx
	orl	%edx, %ebx
	shrl	%ebx
	andl	%r14d, %ebx
	movl	%eax, %ebp
	shrl	%ebp
	andl	%r15d, %ebp
	orl	%edx, %ecx
	orl	%ebp, %ecx
	orl	%ebx, %ecx
	movl	%ecx, (%rdi)
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %r8
	jne	.LBB0_60
.LBB0_61:                               # %._crit_edge210.loopexit
                                        #   in Loop: Header=BB0_21 Depth=2
	movl	80(%rsp), %ecx
.LBB0_62:                               # %._crit_edge210
                                        #   in Loop: Header=BB0_21 Depth=2
	addl	$2, %r11d
	testl	12(%rsp), %ecx          # 4-byte Folded Reload
	je	.LBB0_98
# BB#63:                                #   in Loop: Header=BB0_21 Depth=2
	incl	%r9d
	movq	216(%rsp), %rax         # 8-byte Reload
	movl	80(%rsp,%rax,4), %eax
	movl	AND(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB0_67
# BB#64:                                #   in Loop: Header=BB0_21 Depth=2
	cmpl	$1, %ecx
	jne	.LBB0_68
# BB#65:                                #   in Loop: Header=BB0_21 Depth=2
	movl	endposition(%rip), %ecx
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.LBB0_70
.LBB0_68:                               #   in Loop: Header=BB0_21 Depth=2
	xorl	%eax, %eax
	jmp	.LBB0_69
	.p2align	4, 0x90
.LBB0_67:                               #   in Loop: Header=BB0_21 Depth=2
	testl	endposition(%rip), %eax
	setne	%al
.LBB0_69:                               # %thread-pre-split192.thread
                                        #   in Loop: Header=BB0_21 Depth=2
	movzbl	%al, %eax
	cmpl	INVERSE(%rip), %eax
	je	.LBB0_73
.LBB0_70:                               #   in Loop: Header=BB0_21 Depth=2
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB0_104
# BB#71:                                #   in Loop: Header=BB0_21 Depth=2
	movl	8(%rsp), %eax           # 4-byte Reload
	cmpl	36(%rsp), %eax          # 4-byte Folded Reload
	jge	.LBB0_73
# BB#72:                                #   in Loop: Header=BB0_21 Depth=2
	movq	192(%rsp), %rax         # 8-byte Reload
	leal	(%r11,%rax), %edx
	leaq	256(%rsp), %rdi
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	%r9d, %ecx
	movq	%r9, %rbx
	movq	%r11, %rbp
	callq	output
	movq	%rbp, %r11
	movq	%rbx, %r9
	movq	16(%rsp), %r10          # 8-byte Reload
.LBB0_73:                               #   in Loop: Header=BB0_21 Depth=2
	cmpq	$8, %r12
	movl	Init(%rip), %eax
	jae	.LBB0_75
# BB#74:                                #   in Loop: Header=BB0_21 Depth=2
	xorl	%esi, %esi
	jmp	.LBB0_90
	.p2align	4, 0x90
.LBB0_75:                               # %min.iters.checked266
                                        #   in Loop: Header=BB0_21 Depth=2
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB0_79
# BB#76:                                # %vector.ph270
                                        #   in Loop: Header=BB0_21 Depth=2
	cmpq	$0, 200(%rsp)           # 8-byte Folded Reload
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	je	.LBB0_85
# BB#77:                                # %vector.body262.prol.preheader
                                        #   in Loop: Header=BB0_21 Depth=2
	movq	184(%rsp), %rcx         # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_78:                               # %vector.body262.prol
                                        #   Parent Loop BB0_17 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqa	%xmm0, 128(%rsp,%rdx,4)
	movdqa	%xmm0, 144(%rsp,%rdx,4)
	addq	$8, %rdx
	incq	%rcx
	jne	.LBB0_78
	jmp	.LBB0_86
.LBB0_79:                               #   in Loop: Header=BB0_21 Depth=2
	xorl	%esi, %esi
	jmp	.LBB0_90
.LBB0_80:                               #   in Loop: Header=BB0_21 Depth=2
	xorl	%edx, %edx
.LBB0_81:                               # %vector.body281.prol.loopexit
                                        #   in Loop: Header=BB0_21 Depth=2
	movq	%r9, %r11
	cmpq	$56, 208(%rsp)          # 8-byte Folded Reload
	jb	.LBB0_84
# BB#82:                                # %vector.ph289.new
                                        #   in Loop: Header=BB0_21 Depth=2
	movq	24(%rsp), %rcx          # 8-byte Reload
	subq	%rdx, %rcx
	leaq	84(%rsp), %rsi
	leaq	236(%rsi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB0_83:                               # %vector.body281
                                        #   Parent Loop BB0_17 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqa	%xmm0, -240(%rdx)
	movdqa	%xmm0, -224(%rdx)
	movdqa	%xmm0, -208(%rdx)
	movdqa	%xmm0, -192(%rdx)
	movdqa	%xmm0, -176(%rdx)
	movdqa	%xmm0, -160(%rdx)
	movdqa	%xmm0, -144(%rdx)
	movdqa	%xmm0, -128(%rdx)
	movdqa	%xmm0, -112(%rdx)
	movdqa	%xmm0, -96(%rdx)
	movdqa	%xmm0, -80(%rdx)
	movdqa	%xmm0, -64(%rdx)
	movdqa	%xmm0, -48(%rdx)
	movdqa	%xmm0, -32(%rdx)
	movdqa	%xmm0, -16(%rdx)
	movdqa	%xmm0, (%rdx)
	addq	$256, %rdx              # imm = 0x100
	addq	$-64, %rcx
	jne	.LBB0_83
.LBB0_84:                               # %middle.block282
                                        #   in Loop: Header=BB0_21 Depth=2
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, %r12
	movq	%rcx, %rsi
	jne	.LBB0_45
	jmp	.LBB0_47
.LBB0_85:                               #   in Loop: Header=BB0_21 Depth=2
	xorl	%edx, %edx
.LBB0_86:                               # %vector.body262.prol.loopexit
                                        #   in Loop: Header=BB0_21 Depth=2
	cmpq	$56, 208(%rsp)          # 8-byte Folded Reload
	jb	.LBB0_89
# BB#87:                                # %vector.ph270.new
                                        #   in Loop: Header=BB0_21 Depth=2
	movq	24(%rsp), %rcx          # 8-byte Reload
	subq	%rdx, %rcx
	leaq	132(%rsp), %rsi
	leaq	236(%rsi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB0_88:                               # %vector.body262
                                        #   Parent Loop BB0_17 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqa	%xmm0, -240(%rdx)
	movdqa	%xmm0, -224(%rdx)
	movdqa	%xmm0, -208(%rdx)
	movdqa	%xmm0, -192(%rdx)
	movdqa	%xmm0, -176(%rdx)
	movdqa	%xmm0, -160(%rdx)
	movdqa	%xmm0, -144(%rdx)
	movdqa	%xmm0, -128(%rdx)
	movdqa	%xmm0, -112(%rdx)
	movdqa	%xmm0, -96(%rdx)
	movdqa	%xmm0, -80(%rdx)
	movdqa	%xmm0, -64(%rdx)
	movdqa	%xmm0, -48(%rdx)
	movdqa	%xmm0, -32(%rdx)
	movdqa	%xmm0, -16(%rdx)
	movdqa	%xmm0, (%rdx)
	addq	$256, %rdx              # imm = 0x100
	addq	$-64, %rcx
	jne	.LBB0_88
.LBB0_89:                               # %middle.block263
                                        #   in Loop: Header=BB0_21 Depth=2
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, %r12
	movq	%rcx, %rsi
	je	.LBB0_92
	.p2align	4, 0x90
.LBB0_90:                               # %scalar.ph264.preheader
                                        #   in Loop: Header=BB0_21 Depth=2
	leaq	128(%rsp,%rsi,4), %rcx
	movq	%r12, %rdx
	subq	%rsi, %rdx
	.p2align	4, 0x90
.LBB0_91:                               # %scalar.ph264
                                        #   Parent Loop BB0_17 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, (%rcx)
	addq	$4, %rcx
	decq	%rdx
	jne	.LBB0_91
.LBB0_92:                               # %.loopexit
                                        #   in Loop: Header=BB0_21 Depth=2
	movl	%r11d, %eax
	subl	56(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	128(%rsp), %r8d
	movl	%r8d, %edx
	andl	%r13d, %edx
	movl	%r8d, %eax
	shrl	%eax
	andl	%r15d, %eax
	orl	%edx, %eax
	andl	48(%rsp), %eax          # 4-byte Folded Reload
	testl	%r10d, %r10d
	movl	%eax, 80(%rsp)
	je	.LBB0_98
# BB#93:                                # %.lr.ph214.preheader
                                        #   in Loop: Header=BB0_21 Depth=2
	cmpq	$0, 72(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_95
# BB#94:                                #   in Loop: Header=BB0_21 Depth=2
	movl	$1, %edi
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_96
	jmp	.LBB0_98
.LBB0_95:                               # %.lr.ph214.prol
                                        #   in Loop: Header=BB0_21 Depth=2
	movl	132(%rsp), %edx
	movl	%edx, %esi
	andl	%r13d, %esi
	orl	%r8d, %eax
	shrl	%eax
	andl	%r14d, %eax
	orl	%r8d, %esi
	movl	%edx, %r8d
	shrl	%edx
	andl	%r15d, %edx
	orl	%edx, %esi
	orl	%esi, %eax
	movl	%eax, 84(%rsp)
	movl	$2, %edi
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	je	.LBB0_98
.LBB0_96:                               # %.lr.ph214.preheader.new
                                        #   in Loop: Header=BB0_21 Depth=2
	movq	%r12, %rdx
	subq	%rdi, %rdx
	leaq	132(%rsp), %rcx
	leaq	(%rcx,%rdi,4), %rsi
	leaq	84(%rsp), %rcx
	leaq	(%rcx,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB0_97:                               # %.lr.ph214
                                        #   Parent Loop BB0_17 Depth=1
                                        #     Parent Loop BB0_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rsi), %ecx
	movl	%ecx, %ebx
	andl	%r13d, %ebx
	orl	%r8d, %eax
	shrl	%eax
	andl	%r14d, %eax
	movl	%ecx, %ebp
	shrl	%ebp
	andl	%r15d, %ebp
	orl	%r8d, %ebx
	orl	%ebp, %ebx
	orl	%eax, %ebx
	movl	%ebx, -4(%rdi)
	movl	(%rsi), %r8d
	movl	%r8d, %eax
	andl	%r13d, %eax
	orl	%ecx, %ebx
	shrl	%ebx
	andl	%r14d, %ebx
	movl	%r8d, %ebp
	shrl	%ebp
	andl	%r15d, %ebp
	orl	%ecx, %eax
	orl	%ebp, %eax
	orl	%ebx, %eax
	movl	%eax, (%rdi)
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rdx
	jne	.LBB0_97
.LBB0_98:                               # %.backedge
                                        #   in Loop: Header=BB0_21 Depth=2
	movq	248(%rsp), %rax         # 8-byte Reload
	cmpl	%eax, %r11d
	jb	.LBB0_21
.LBB0_99:                               # %._crit_edge219
                                        #   in Loop: Header=BB0_17 Depth=1
	movq	%r9, %r15
	cmpl	$49152, 240(%rsp)       # 4-byte Folded Reload
                                        # imm = 0xC000
	movl	$49152, %ebx            # imm = 0xC000
	jl	.LBB0_103
# BB#100:                               #   in Loop: Header=BB0_17 Depth=1
	movq	176(%rsp), %rax         # 8-byte Reload
	subl	8(%rsp), %eax           # 4-byte Folded Reload
	cmpl	$49153, %eax            # imm = 0xC001
	jl	.LBB0_102
# BB#101:                               #   in Loop: Header=BB0_17 Depth=1
	movl	$1, TRUNCATE(%rip)
	movl	$49152, %eax            # imm = 0xC000
.LBB0_102:                              #   in Loop: Header=BB0_17 Depth=1
	movslq	%eax, %rbp
	leaq	49408(%rsp), %rdi
	subq	%rbp, %rdi
	movslq	8(%rsp), %rax           # 4-byte Folded Reload
	leaq	256(%rsp,%rax), %rsi
	movq	%rbp, %rdx
	callq	strncpy
	movl	$49152, %ebx            # imm = 0xC000
	subl	%ebp, %ebx
	movl	$1, %eax
	cmovel	%eax, %ebx
.LBB0_103:                              # %.backedge197
                                        #   in Loop: Header=BB0_17 Depth=1
	movl	$49152, %edx            # imm = 0xC000
	movl	32(%rsp), %edi          # 4-byte Reload
	leaq	49408(%rsp), %rsi
	callq	fill_buf
	movl	%eax, %ebp
	testl	%ebp, %ebp
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	%r15, %r9
	movl	$49152, %r11d           # imm = 0xC000
	jg	.LBB0_17
	jmp	.LBB0_105
.LBB0_104:
	incl	num_of_matched(%rip)
	movl	$CurrentFileName, %edi
	callq	puts
.LBB0_105:                              # %.loopexit196
	addq	$98568, %rsp            # imm = 0x18108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	asearch0, .Lfunc_end0-asearch0
	.cfi_endproc

	.globl	asearch
	.p2align	4, 0x90
	.type	asearch,@function
asearch:                                # @asearch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$98600, %rsp            # imm = 0x18128
.Lcfi19:
	.cfi_def_cfa_offset 98656
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%esi, %r15d
	cmpl	$0, I(%rip)
	jne	.LBB1_2
# BB#1:
	movl	$-1, Init1(%rip)
.LBB1_2:
	cmpl	$5, %edx
	jb	.LBB1_4
# BB#3:
	movl	%r15d, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	asearch0
	jmp	.LBB1_120
.LBB1_4:
	movq	%rdi, 216(%rsp)         # 8-byte Spill
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	callq	strlen
	movq	96(%rsp), %rdi          # 8-byte Reload
	movb	$10, 49439(%rsp)
	movl	D_endpos(%rip), %ecx
	cmpl	$2, %eax
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	%ecx, %ebx
	jb	.LBB1_12
# BB#5:                                 # %.lr.ph419.preheader
	leal	7(%rax), %edx
	leal	-2(%rax), %ebp
	andl	$7, %edx
	je	.LBB1_9
# BB#6:                                 # %.lr.ph419.prol.preheader
	xorl	%ecx, %ecx
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	%esi, %ebx
	.p2align	4, 0x90
.LBB1_7:                                # %.lr.ph419.prol
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rbx,%rbx), %esi
	orl	%esi, %ebx
	incl	%ecx
	cmpl	%ecx, %edx
	jne	.LBB1_7
# BB#8:                                 # %.lr.ph419.prol.loopexit.unr-lcssa
	incl	%ecx
	cmpl	$7, %ebp
	jae	.LBB1_10
	jmp	.LBB1_12
.LBB1_9:
	movl	$1, %ecx
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	%edx, %ebx
	cmpl	$7, %ebp
	jb	.LBB1_12
.LBB1_10:                               # %.lr.ph419.preheader.new
	movl	%eax, %esi
	subl	%ecx, %esi
	.p2align	4, 0x90
.LBB1_11:                               # %.lr.ph419
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rbx,%rbx), %ecx
	orl	%ebx, %ecx
	leal	(%rcx,%rcx), %edx
	orl	%ecx, %edx
	leal	(%rdx,%rdx), %ecx
	orl	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	orl	%ecx, %edx
	leal	(%rdx,%rdx), %ecx
	orl	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	orl	%ecx, %edx
	leal	(%rdx,%rdx), %ecx
	orl	%edx, %ecx
	leal	(%rcx,%rcx), %ebx
	orl	%ecx, %ebx
	addl	$-8, %esi
	jne	.LBB1_11
.LBB1_12:                               # %._crit_edge420
	movl	Init1(%rip), %ecx
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	movl	NO_ERR_MASK(%rip), %r12d
	movl	Init(%rip), %ecx
	leal	1(%rdi), %ebp
	cmpl	$7, %ebp
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	jbe	.LBB1_17
# BB#13:                                # %min.iters.checked
	movl	%ebp, %eax
	andl	$7, %eax
	movq	%rbp, %r14
	subq	%rax, %r14
	je	.LBB1_17
# BB#14:                                # %vector.ph
	movd	%ecx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	128(%rsp), %rdx
	leaq	176(%rsp), %rsi
	movq	%r14, %rdi
	.p2align	4, 0x90
.LBB1_15:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, -16(%rdx)
	movdqa	%xmm0, (%rdx)
	movdqa	%xmm0, -16(%rsi)
	movdqa	%xmm0, (%rsi)
	addq	$32, %rdx
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB1_15
# BB#16:                                # %middle.block
	testl	%eax, %eax
	jne	.LBB1_18
	jmp	.LBB1_19
.LBB1_17:
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_18:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, 112(%rsp,%r14,4)
	movl	%ecx, 160(%rsp,%r14,4)
	incq	%r14
	cmpq	%r14, %rbp
	jne	.LBB1_18
.LBB1_19:                               # %.preheader345
	leaq	49440(%rsp), %rsi
	movl	$49152, %edx            # imm = 0xC000
	movl	%r15d, 212(%rsp)        # 4-byte Spill
	movl	%r15d, %edi
	movl	%ecx, %ebx
	callq	fill_buf
	movl	$49152, 12(%rsp)        # 4-byte Folded Spill
                                        # imm = 0xC000
	movl	%eax, %r10d
	testl	%r10d, %r10d
	jle	.LBB1_120
# BB#20:                                # %.lr.ph414
	movq	104(%rsp), %rax         # 8-byte Reload
	notl	%eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	80(%rsp), %rax          # 8-byte Reload
	movslq	%eax, %r13
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	notl	%eax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	leaq	-8(%r14), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	shrl	$3, %eax
	incl	%eax
	leal	-1(%r14), %ecx
	leaq	-2(%r14), %rdx
	movq	%rdx, 264(%rsp)         # 8-byte Spill
	andl	$1, %ecx
	movq	%rcx, 272(%rsp)         # 8-byte Spill
	movq	%r14, %rcx
	andq	$-8, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	andl	$7, %eax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	negq	%rax
	movq	%rax, 232(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movl	%ebx, 24(%rsp)          # 4-byte Spill
	movl	%ebx, 28(%rsp)          # 4-byte Spill
	movl	%ebx, %r11d
	movl	%ebx, %edi
	movl	%ebx, 32(%rsp)          # 4-byte Spill
	movl	%ebx, %ebp
	movl	%ebx, %r15d
	movq	%r14, 56(%rsp)          # 8-byte Spill
	movq	%r13, 280(%rsp)         # 8-byte Spill
.LBB1_21:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_25 Depth 2
                                        #     Child Loop BB1_33 Depth 2
                                        #       Child Loop BB1_55 Depth 3
                                        #       Child Loop BB1_60 Depth 3
                                        #       Child Loop BB1_63 Depth 3
                                        #       Child Loop BB1_70 Depth 3
                                        #       Child Loop BB1_95 Depth 3
                                        #       Child Loop BB1_100 Depth 3
                                        #       Child Loop BB1_103 Depth 3
                                        #       Child Loop BB1_111 Depth 3
	movl	$49152, %r8d            # imm = 0xC000
	testb	$1, %al
	movl	%edi, 20(%rsp)          # 4-byte Spill
	jne	.LBB1_29
# BB#22:                                #   in Loop: Header=BB1_21 Depth=1
	movl	$49151, %r8d            # imm = 0xBFFF
	cmpl	$0, DELIMITER(%rip)
	movq	216(%rsp), %rsi         # 8-byte Reload
	je	.LBB1_29
# BB#23:                                # %.preheader344
                                        #   in Loop: Header=BB1_21 Depth=1
	movl	$-1, %eax
	cmpl	$0, 80(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_28
# BB#24:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_21 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_25:                               # %.lr.ph
                                        #   Parent Loop BB1_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rsi,%rcx), %edx
	cmpb	49440(%rsp,%rcx), %dl
	jne	.LBB1_27
# BB#26:                                #   in Loop: Header=BB1_25 Depth=2
	incq	%rcx
	cmpq	%r13, %rcx
	jl	.LBB1_25
	jmp	.LBB1_28
.LBB1_27:                               #   in Loop: Header=BB1_21 Depth=1
	xorl	%eax, %eax
.LBB1_28:                               # %._crit_edge
                                        #   in Loop: Header=BB1_21 Depth=1
	movq	72(%rsp), %rcx          # 8-byte Reload
	addl	%eax, %ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
.LBB1_29:                               #   in Loop: Header=BB1_21 Depth=1
	leal	49152(%r10), %eax
	movq	%r10, 224(%rsp)         # 8-byte Spill
	cmpl	$49151, %r10d           # imm = 0xBFFF
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movl	%eax, %ecx
	movl	%r11d, 16(%rsp)         # 4-byte Spill
	jg	.LBB1_31
# BB#30:                                #   in Loop: Header=BB1_21 Depth=1
	movl	200(%rsp), %eax         # 4-byte Reload
	leaq	288(%rsp,%rax), %rdi
	movq	216(%rsp), %rsi         # 8-byte Reload
	movq	%r13, %rdx
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movl	%ebx, 40(%rsp)          # 4-byte Spill
	callq	strncpy
	movl	40(%rsp), %ebx          # 4-byte Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	200(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%rax), %ecx
	movb	$0, 288(%rsp,%rcx)
.LBB1_31:                               # %.preheader
                                        #   in Loop: Header=BB1_21 Depth=1
	cmpl	%ecx, %r8d
	movq	96(%rsp), %r10          # 8-byte Reload
	movl	36(%rsp), %r11d         # 4-byte Reload
	jae	.LBB1_114
# BB#32:                                # %.lr.ph391
                                        #   in Loop: Header=BB1_21 Depth=1
	movq	224(%rsp), %rax         # 8-byte Reload
	leal	49151(%rax), %eax
	movl	%eax, 92(%rsp)          # 4-byte Spill
	movl	8(%rsp), %edi           # 4-byte Reload
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_33:                               #   Parent Loop BB1_21 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_55 Depth 3
                                        #       Child Loop BB1_60 Depth 3
                                        #       Child Loop BB1_63 Depth 3
                                        #       Child Loop BB1_70 Depth 3
                                        #       Child Loop BB1_95 Depth 3
                                        #       Child Loop BB1_100 Depth 3
                                        #       Child Loop BB1_103 Depth 3
                                        #       Child Loop BB1_111 Depth 3
	movl	%r8d, %eax
	movzbl	288(%rsp,%rax), %eax
	movl	Mask(,%rax,4), %r13d
	movl	%r15d, %eax
	andl	%r11d, %eax
	movl	%r15d, %esi
	shrl	%esi
	andl	%r13d, %esi
	orl	%esi, %eax
	movl	%ebp, %edx
	andl	%r11d, %edx
	orl	%r15d, %esi
	shrl	%esi
	andl	%r12d, %esi
	movl	%ebp, %ecx
	shrl	%ecx
	andl	%r13d, %ecx
	orl	%r15d, %ecx
	orl	%esi, %ecx
	movl	%ebx, %r15d
	cmpl	$1, %r10d
	je	.LBB1_38
# BB#34:                                #   in Loop: Header=BB1_33 Depth=2
	movl	20(%rsp), %ebx          # 4-byte Reload
	movq	%r10, %r9
	movl	%ebx, %r10d
	andl	%r11d, %r10d
	movl	%ecx, %esi
	orl	%ebp, %esi
	shrl	%esi
	andl	%r12d, %esi
	movl	%ebx, %edi
	shrl	%edi
	andl	%r13d, %edi
	orl	%ebp, %edi
	orl	%esi, %edi
	orl	%edi, %r10d
	movl	%r10d, 32(%rsp)         # 4-byte Spill
	movq	%r9, %r10
	cmpl	$2, %r10d
	je	.LBB1_37
# BB#35:                                #   in Loop: Header=BB1_33 Depth=2
	movl	28(%rsp), %r9d          # 4-byte Reload
	movl	%r9d, %ebp
	andl	%r11d, %ebp
	orl	%ebx, %edi
	shrl	%edi
	andl	%r12d, %edi
	movl	%r9d, %esi
	shrl	%esi
	andl	%r13d, %esi
	orl	%ebx, %esi
	orl	%edi, %esi
	orl	%esi, %ebp
	cmpl	$3, %r10d
	movl	%ebp, 16(%rsp)          # 4-byte Spill
	je	.LBB1_37
# BB#36:                                #   in Loop: Header=BB1_33 Depth=2
	movl	%r15d, %edi
	movl	%r9d, %ebx
	movl	%edi, %ebp
	andl	%r11d, %ebp
	orl	%ebx, %esi
	shrl	%esi
	andl	%r12d, %esi
	shrl	%edi
	andl	%r13d, %edi
	orl	%ebx, %ebp
	orl	%edi, %ebp
	orl	%esi, %ebp
	movl	%ebp, 24(%rsp)          # 4-byte Spill
.LBB1_37:                               #   in Loop: Header=BB1_33 Depth=2
	movl	8(%rsp), %edi           # 4-byte Reload
.LBB1_38:                               #   in Loop: Header=BB1_33 Depth=2
	orl	%edx, %ecx
	leal	1(%r8), %ebp
	testl	%edi, %eax
	je	.LBB1_43
# BB#39:                                #   in Loop: Header=BB1_33 Depth=2
	movq	72(%rsp), %rbx          # 8-byte Reload
	incl	%ebx
	cmpl	$1, %r10d
	cmovel	%ecx, %eax
	cmpl	$2, %r10d
	cmovel	32(%rsp), %eax          # 4-byte Folded Reload
	cmpl	$3, %r10d
	cmovel	16(%rsp), %eax          # 4-byte Folded Reload
	cmpl	$4, %r10d
	cmovel	24(%rsp), %eax          # 4-byte Folded Reload
	movl	AND(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB1_44
# BB#40:                                #   in Loop: Header=BB1_33 Depth=2
	cmpl	$1, %ecx
	jne	.LBB1_45
# BB#41:                                #   in Loop: Header=BB1_33 Depth=2
	movl	endposition(%rip), %ecx
	andl	%ecx, %eax
	cmpl	%ecx, %eax
	je	.LBB1_47
.LBB1_45:                               #   in Loop: Header=BB1_33 Depth=2
	xorl	%eax, %eax
	jmp	.LBB1_46
	.p2align	4, 0x90
.LBB1_43:                               #   in Loop: Header=BB1_33 Depth=2
	movl	%r15d, %ebx
	movl	32(%rsp), %r14d         # 4-byte Reload
	jmp	.LBB1_73
	.p2align	4, 0x90
.LBB1_44:                               #   in Loop: Header=BB1_33 Depth=2
	testl	endposition(%rip), %eax
	setne	%al
.LBB1_46:                               # %thread-pre-split.thread
                                        #   in Loop: Header=BB1_33 Depth=2
	movzbl	%al, %eax
	cmpl	INVERSE(%rip), %eax
	je	.LBB1_50
.LBB1_47:                               #   in Loop: Header=BB1_33 Depth=2
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB1_119
# BB#48:                                #   in Loop: Header=BB1_33 Depth=2
	movl	12(%rsp), %eax          # 4-byte Reload
	cmpl	92(%rsp), %eax          # 4-byte Folded Reload
	jge	.LBB1_50
# BB#49:                                #   in Loop: Header=BB1_33 Depth=2
	movq	240(%rsp), %rax         # 8-byte Reload
	leal	(%rbp,%rax), %edx
	leaq	288(%rsp), %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	%ebx, %ecx
	movq	%rbp, %r15
	movq	%r8, %rbp
	callq	output
	movl	8(%rsp), %edi           # 4-byte Reload
	movq	%rbp, %r8
	movq	%r15, %rbp
	movq	96(%rsp), %r10          # 8-byte Reload
.LBB1_50:                               #   in Loop: Header=BB1_33 Depth=2
	cmpq	$8, %r14
	movl	$0, TRUNCATE(%rip)
	movl	Init(%rip), %eax
	movq	%r8, 64(%rsp)           # 8-byte Spill
	jae	.LBB1_52
# BB#51:                                #   in Loop: Header=BB1_33 Depth=2
	xorl	%esi, %esi
	jmp	.LBB1_62
	.p2align	4, 0x90
.LBB1_52:                               # %min.iters.checked472
                                        #   in Loop: Header=BB1_33 Depth=2
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB1_56
# BB#53:                                # %vector.ph476
                                        #   in Loop: Header=BB1_33 Depth=2
	cmpq	$0, 248(%rsp)           # 8-byte Folded Reload
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	je	.LBB1_57
# BB#54:                                # %vector.body468.prol.preheader
                                        #   in Loop: Header=BB1_33 Depth=2
	movq	232(%rsp), %rcx         # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_55:                               # %vector.body468.prol
                                        #   Parent Loop BB1_21 Depth=1
                                        #     Parent Loop BB1_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqa	%xmm0, 112(%rsp,%rdx,4)
	movdqa	%xmm0, 128(%rsp,%rdx,4)
	addq	$8, %rdx
	incq	%rcx
	jne	.LBB1_55
	jmp	.LBB1_58
.LBB1_56:                               #   in Loop: Header=BB1_33 Depth=2
	xorl	%esi, %esi
	jmp	.LBB1_62
.LBB1_57:                               #   in Loop: Header=BB1_33 Depth=2
	xorl	%edx, %edx
.LBB1_58:                               # %vector.body468.prol.loopexit
                                        #   in Loop: Header=BB1_33 Depth=2
	cmpq	$56, 256(%rsp)          # 8-byte Folded Reload
	jb	.LBB1_61
# BB#59:                                # %vector.ph476.new
                                        #   in Loop: Header=BB1_33 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	subq	%rdx, %rcx
	leaq	352(%rsp), %rsi
	leaq	(%rsi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB1_60:                               # %vector.body468
                                        #   Parent Loop BB1_21 Depth=1
                                        #     Parent Loop BB1_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqa	%xmm0, -240(%rdx)
	movdqa	%xmm0, -224(%rdx)
	movdqa	%xmm0, -208(%rdx)
	movdqa	%xmm0, -192(%rdx)
	movdqa	%xmm0, -176(%rdx)
	movdqa	%xmm0, -160(%rdx)
	movdqa	%xmm0, -144(%rdx)
	movdqa	%xmm0, -128(%rdx)
	movdqa	%xmm0, -112(%rdx)
	movdqa	%xmm0, -96(%rdx)
	movdqa	%xmm0, -80(%rdx)
	movdqa	%xmm0, -64(%rdx)
	movdqa	%xmm0, -48(%rdx)
	movdqa	%xmm0, -32(%rdx)
	movdqa	%xmm0, -16(%rdx)
	movdqa	%xmm0, (%rdx)
	addq	$256, %rdx              # imm = 0x100
	addq	$-64, %rcx
	jne	.LBB1_60
.LBB1_61:                               # %middle.block469
                                        #   in Loop: Header=BB1_33 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, %r14
	movq	%rcx, %rsi
	je	.LBB1_64
	.p2align	4, 0x90
.LBB1_62:                               # %scalar.ph470.preheader
                                        #   in Loop: Header=BB1_33 Depth=2
	leaq	112(%rsp,%rsi,4), %rcx
	movq	%r14, %rdx
	subq	%rsi, %rdx
	.p2align	4, 0x90
.LBB1_63:                               # %scalar.ph470
                                        #   Parent Loop BB1_21 Depth=1
                                        #     Parent Loop BB1_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, (%rcx)
	addq	$4, %rcx
	decq	%rdx
	jne	.LBB1_63
.LBB1_64:                               # %.loopexit488
                                        #   in Loop: Header=BB1_33 Depth=2
	movl	112(%rsp), %r9d
	movl	Init1(%rip), %r8d
	movl	%r8d, %ecx
	andl	%r9d, %ecx
	movl	%r9d, %eax
	shrl	%eax
	andl	%r13d, %eax
	orl	%ecx, %eax
	andl	104(%rsp), %eax         # 4-byte Folded Reload
	testl	%r10d, %r10d
	movl	%eax, 160(%rsp)
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	je	.LBB1_67
# BB#65:                                # %.lr.ph373.preheader
                                        #   in Loop: Header=BB1_33 Depth=2
	cmpq	$0, 272(%rsp)           # 8-byte Folded Reload
	jne	.LBB1_68
# BB#66:                                #   in Loop: Header=BB1_33 Depth=2
	movq	%rbp, %r15
	movl	$1, %ecx
	cmpq	$0, 264(%rsp)           # 8-byte Folded Reload
	jne	.LBB1_69
	jmp	.LBB1_71
	.p2align	4, 0x90
.LBB1_67:                               #   in Loop: Header=BB1_33 Depth=2
	movl	36(%rsp), %r11d         # 4-byte Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	jmp	.LBB1_72
.LBB1_68:                               # %.lr.ph373.prol
                                        #   in Loop: Header=BB1_33 Depth=2
	movq	%rbp, %r15
	movl	116(%rsp), %ecx
	movl	%ecx, %esi
	andl	%r8d, %esi
	orl	%r9d, %eax
	shrl	%eax
	andl	%r12d, %eax
	orl	%r9d, %esi
	movl	%ecx, %r9d
	shrl	%ecx
	andl	%r13d, %ecx
	orl	%ecx, %esi
	orl	%esi, %eax
	movl	%eax, 164(%rsp)
	movl	$2, %ecx
	cmpq	$0, 264(%rsp)           # 8-byte Folded Reload
	je	.LBB1_71
.LBB1_69:                               # %.lr.ph373.preheader.new
                                        #   in Loop: Header=BB1_33 Depth=2
	movq	%r14, %rsi
	subq	%rcx, %rsi
	leaq	352(%rsp), %rdx
	leaq	-236(%rdx,%rcx,4), %rdi
	leaq	164(%rsp), %rdx
	leaq	(%rdx,%rcx,4), %rbp
	.p2align	4, 0x90
.LBB1_70:                               # %.lr.ph373
                                        #   Parent Loop BB1_21 Depth=1
                                        #     Parent Loop BB1_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rdi), %edx
	movl	%edx, %ebx
	andl	%r8d, %ebx
	orl	%r9d, %eax
	shrl	%eax
	andl	%r12d, %eax
	movl	%edx, %ecx
	shrl	%ecx
	andl	%r13d, %ecx
	orl	%r9d, %ebx
	orl	%ecx, %ebx
	orl	%eax, %ebx
	movl	%ebx, -4(%rbp)
	movl	(%rdi), %r9d
	movl	%r9d, %eax
	andl	%r8d, %eax
	orl	%edx, %ebx
	shrl	%ebx
	andl	%r12d, %ebx
	movl	%r9d, %ecx
	shrl	%ecx
	andl	%r13d, %ecx
	orl	%edx, %eax
	orl	%ecx, %eax
	orl	%ebx, %eax
	movl	%eax, (%rbp)
	addq	$8, %rdi
	addq	$8, %rbp
	addq	$-2, %rsi
	jne	.LBB1_70
.LBB1_71:                               # %._crit_edge374.loopexit
                                        #   in Loop: Header=BB1_33 Depth=2
	movl	160(%rsp), %eax
	movl	8(%rsp), %edi           # 4-byte Reload
	movl	36(%rsp), %r11d         # 4-byte Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	%r15, %rbp
.LBB1_72:                               # %._crit_edge374
                                        #   in Loop: Header=BB1_33 Depth=2
	movl	%ebp, %ecx
	subl	80(%rsp), %ecx          # 4-byte Folded Reload
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	164(%rsp), %ecx
	movl	168(%rsp), %r14d
	movl	120(%rsp), %edx
	movl	%edx, 20(%rsp)          # 4-byte Spill
	movl	124(%rsp), %edx
	movl	%edx, 28(%rsp)          # 4-byte Spill
	movl	172(%rsp), %edx
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movl	176(%rsp), %edx
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movl	128(%rsp), %ebx
.LBB1_73:                               #   in Loop: Header=BB1_33 Depth=2
	movl	%ebp, %edx
	movzbl	288(%rsp,%rdx), %edx
	movl	Mask(,%rdx,4), %r13d
	movl	%eax, %r15d
	andl	%r11d, %r15d
	movl	%eax, %esi
	shrl	%esi
	andl	%r13d, %esi
	orl	%esi, %r15d
	movl	%ecx, %edx
	andl	%r11d, %edx
	orl	%eax, %esi
	shrl	%esi
	andl	%r12d, %esi
	movl	%ecx, %ebp
	shrl	%ebp
	andl	%r13d, %ebp
	orl	%eax, %ebp
	orl	%esi, %ebp
	cmpl	$1, %r10d
	je	.LBB1_78
# BB#74:                                #   in Loop: Header=BB1_33 Depth=2
	movl	%edi, %r9d
	movl	%r14d, %edi
	andl	%r11d, %edi
	movl	%ebp, %eax
	orl	%ecx, %eax
	shrl	%eax
	andl	%r12d, %eax
	movl	%r14d, %esi
	shrl	%esi
	andl	%r13d, %esi
	orl	%ecx, %esi
	orl	%eax, %esi
	orl	%esi, %edi
	movl	%edi, 20(%rsp)          # 4-byte Spill
	cmpl	$2, %r10d
	je	.LBB1_77
# BB#75:                                #   in Loop: Header=BB1_33 Depth=2
	movl	16(%rsp), %edi          # 4-byte Reload
	movl	%edi, %ecx
	andl	%r11d, %ecx
	orl	%r14d, %esi
	shrl	%esi
	andl	%r12d, %esi
	movl	%edi, %eax
	shrl	%eax
	andl	%r13d, %eax
	orl	%r14d, %eax
	orl	%esi, %eax
	orl	%eax, %ecx
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	cmpl	$3, %r10d
	je	.LBB1_77
# BB#76:                                #   in Loop: Header=BB1_33 Depth=2
	movl	24(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %ebx
	andl	%r11d, %ebx
	orl	%edi, %eax
	shrl	%eax
	andl	%r12d, %eax
	shrl	%ecx
	andl	%r13d, %ecx
	orl	%edi, %ebx
	orl	%ecx, %ebx
	orl	%eax, %ebx
.LBB1_77:                               #   in Loop: Header=BB1_33 Depth=2
	movl	%r9d, %edi
.LBB1_78:                               #   in Loop: Header=BB1_33 Depth=2
	orl	%edx, %ebp
	addl	$2, %r8d
	testl	%edi, %r15d
	je	.LBB1_83
# BB#79:                                #   in Loop: Header=BB1_33 Depth=2
	movq	72(%rsp), %r14          # 8-byte Reload
	incl	%r14d
	cmpl	$1, %r10d
	cmovel	%ebp, %r15d
	cmpl	$2, %r10d
	cmovel	20(%rsp), %r15d         # 4-byte Folded Reload
	cmpl	$3, %r10d
	cmovel	28(%rsp), %r15d         # 4-byte Folded Reload
	cmpl	$4, %r10d
	cmovel	%ebx, %r15d
	movl	AND(%rip), %eax
	testl	%eax, %eax
	je	.LBB1_84
# BB#80:                                #   in Loop: Header=BB1_33 Depth=2
	cmpl	$1, %eax
	jne	.LBB1_85
# BB#81:                                #   in Loop: Header=BB1_33 Depth=2
	movl	endposition(%rip), %eax
	andl	%eax, %r15d
	cmpl	%eax, %r15d
	je	.LBB1_87
.LBB1_85:                               #   in Loop: Header=BB1_33 Depth=2
	xorl	%eax, %eax
	jmp	.LBB1_86
	.p2align	4, 0x90
.LBB1_83:                               #   in Loop: Header=BB1_33 Depth=2
	movl	%r14d, 32(%rsp)         # 4-byte Spill
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %r14          # 8-byte Reload
	cmpl	%ecx, %r8d
	jb	.LBB1_33
	jmp	.LBB1_114
	.p2align	4, 0x90
.LBB1_84:                               #   in Loop: Header=BB1_33 Depth=2
	testl	endposition(%rip), %r15d
	setne	%al
.LBB1_86:                               # %thread-pre-split342.thread
                                        #   in Loop: Header=BB1_33 Depth=2
	movzbl	%al, %eax
	cmpl	INVERSE(%rip), %eax
	je	.LBB1_90
.LBB1_87:                               #   in Loop: Header=BB1_33 Depth=2
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB1_119
# BB#88:                                #   in Loop: Header=BB1_33 Depth=2
	movl	12(%rsp), %eax          # 4-byte Reload
	cmpl	92(%rsp), %eax          # 4-byte Folded Reload
	jge	.LBB1_90
# BB#89:                                #   in Loop: Header=BB1_33 Depth=2
	movq	240(%rsp), %rax         # 8-byte Reload
	leal	(%r8,%rax), %edx
	movl	%edi, %ebx
	leaq	288(%rsp), %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	%r14d, %ecx
	movq	%r8, %rbp
	callq	output
	movl	%ebx, %edi
	movq	%rbp, %r8
	movq	96(%rsp), %r10          # 8-byte Reload
.LBB1_90:                               #   in Loop: Header=BB1_33 Depth=2
	cmpq	$8, 56(%rsp)            # 8-byte Folded Reload
	movl	$0, TRUNCATE(%rip)
	movl	Init(%rip), %eax
	movq	%r8, 64(%rsp)           # 8-byte Spill
	jae	.LBB1_92
# BB#91:                                #   in Loop: Header=BB1_33 Depth=2
	xorl	%esi, %esi
	jmp	.LBB1_102
	.p2align	4, 0x90
.LBB1_92:                               # %min.iters.checked453
                                        #   in Loop: Header=BB1_33 Depth=2
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB1_96
# BB#93:                                # %vector.ph457
                                        #   in Loop: Header=BB1_33 Depth=2
	cmpq	$0, 248(%rsp)           # 8-byte Folded Reload
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	je	.LBB1_97
# BB#94:                                # %vector.body449.prol.preheader
                                        #   in Loop: Header=BB1_33 Depth=2
	movq	232(%rsp), %rcx         # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_95:                               # %vector.body449.prol
                                        #   Parent Loop BB1_21 Depth=1
                                        #     Parent Loop BB1_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqa	%xmm0, 160(%rsp,%rdx,4)
	movdqa	%xmm0, 176(%rsp,%rdx,4)
	addq	$8, %rdx
	incq	%rcx
	jne	.LBB1_95
	jmp	.LBB1_98
.LBB1_96:                               #   in Loop: Header=BB1_33 Depth=2
	xorl	%esi, %esi
	jmp	.LBB1_102
.LBB1_97:                               #   in Loop: Header=BB1_33 Depth=2
	xorl	%edx, %edx
.LBB1_98:                               # %vector.body449.prol.loopexit
                                        #   in Loop: Header=BB1_33 Depth=2
	cmpq	$56, 256(%rsp)          # 8-byte Folded Reload
	jb	.LBB1_101
# BB#99:                                # %vector.ph457.new
                                        #   in Loop: Header=BB1_33 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	subq	%rdx, %rcx
	leaq	164(%rsp), %rsi
	leaq	236(%rsi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB1_100:                              # %vector.body449
                                        #   Parent Loop BB1_21 Depth=1
                                        #     Parent Loop BB1_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqa	%xmm0, -240(%rdx)
	movdqa	%xmm0, -224(%rdx)
	movdqa	%xmm0, -208(%rdx)
	movdqa	%xmm0, -192(%rdx)
	movdqa	%xmm0, -176(%rdx)
	movdqa	%xmm0, -160(%rdx)
	movdqa	%xmm0, -144(%rdx)
	movdqa	%xmm0, -128(%rdx)
	movdqa	%xmm0, -112(%rdx)
	movdqa	%xmm0, -96(%rdx)
	movdqa	%xmm0, -80(%rdx)
	movdqa	%xmm0, -64(%rdx)
	movdqa	%xmm0, -48(%rdx)
	movdqa	%xmm0, -32(%rdx)
	movdqa	%xmm0, -16(%rdx)
	movdqa	%xmm0, (%rdx)
	addq	$256, %rdx              # imm = 0x100
	addq	$-64, %rcx
	jne	.LBB1_100
.LBB1_101:                              # %middle.block450
                                        #   in Loop: Header=BB1_33 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, 56(%rsp)          # 8-byte Folded Reload
	movq	%rcx, %rsi
	je	.LBB1_104
	.p2align	4, 0x90
.LBB1_102:                              # %scalar.ph451.preheader
                                        #   in Loop: Header=BB1_33 Depth=2
	leaq	160(%rsp,%rsi,4), %rcx
	movq	56(%rsp), %rdx          # 8-byte Reload
	subq	%rsi, %rdx
	.p2align	4, 0x90
.LBB1_103:                              # %scalar.ph451
                                        #   Parent Loop BB1_21 Depth=1
                                        #     Parent Loop BB1_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, (%rcx)
	addq	$4, %rcx
	decq	%rdx
	jne	.LBB1_103
.LBB1_104:                              # %.loopexit487
                                        #   in Loop: Header=BB1_33 Depth=2
	movl	160(%rsp), %r9d
	movl	Init1(%rip), %r8d
	movl	%r8d, %edx
	andl	%r9d, %edx
	movl	%r9d, %r15d
	shrl	%r15d
	andl	%r13d, %r15d
	orl	%edx, %r15d
	andl	104(%rsp), %r15d        # 4-byte Folded Reload
	testl	%r10d, %r10d
	movl	%r15d, 112(%rsp)
	movq	%r14, 72(%rsp)          # 8-byte Spill
	je	.LBB1_107
# BB#105:                               # %.lr.ph378.preheader
                                        #   in Loop: Header=BB1_33 Depth=2
	cmpq	$0, 272(%rsp)           # 8-byte Folded Reload
	jne	.LBB1_108
# BB#106:                               #   in Loop: Header=BB1_33 Depth=2
	movl	$1, %edi
	jmp	.LBB1_109
	.p2align	4, 0x90
.LBB1_107:                              #   in Loop: Header=BB1_33 Depth=2
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %r14          # 8-byte Reload
	jmp	.LBB1_113
.LBB1_108:                              # %.lr.ph378.prol
                                        #   in Loop: Header=BB1_33 Depth=2
	movl	164(%rsp), %edx
	movl	%edx, %esi
	andl	%r8d, %esi
	orl	%r9d, %r15d
	shrl	%r15d
	andl	%r12d, %r15d
	orl	%r9d, %esi
	movl	%edx, %r9d
	shrl	%edx
	andl	%r13d, %edx
	orl	%edx, %esi
	orl	%esi, %r15d
	movl	%r15d, 116(%rsp)
	movl	$2, %edi
.LBB1_109:                              # %.lr.ph378.prol.loopexit
                                        #   in Loop: Header=BB1_33 Depth=2
	cmpq	$0, 264(%rsp)           # 8-byte Folded Reload
	movq	56(%rsp), %r14          # 8-byte Reload
	je	.LBB1_112
# BB#110:                               # %.lr.ph378.preheader.new
                                        #   in Loop: Header=BB1_33 Depth=2
	movq	%r14, %rdx
	subq	%rdi, %rdx
	leaq	164(%rsp), %rax
	leaq	(%rax,%rdi,4), %rsi
	leaq	352(%rsp), %rax
	leaq	-236(%rax,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB1_111:                              # %.lr.ph378
                                        #   Parent Loop BB1_21 Depth=1
                                        #     Parent Loop BB1_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rsi), %eax
	movl	%eax, %ebp
	andl	%r8d, %ebp
	orl	%r9d, %r15d
	shrl	%r15d
	andl	%r12d, %r15d
	movl	%eax, %ebx
	shrl	%ebx
	andl	%r13d, %ebx
	orl	%r9d, %ebp
	orl	%ebx, %ebp
	orl	%r15d, %ebp
	movl	%ebp, -4(%rdi)
	movl	(%rsi), %r9d
	movl	%r9d, %r15d
	andl	%r8d, %r15d
	orl	%eax, %ebp
	shrl	%ebp
	andl	%r12d, %ebp
	movl	%r9d, %ecx
	shrl	%ecx
	andl	%r13d, %ecx
	orl	%eax, %r15d
	orl	%ecx, %r15d
	orl	%ebp, %r15d
	movl	%r15d, (%rdi)
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rdx
	jne	.LBB1_111
.LBB1_112:                              # %._crit_edge379.loopexit
                                        #   in Loop: Header=BB1_33 Depth=2
	movl	112(%rsp), %r15d
	movl	8(%rsp), %edi           # 4-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
.LBB1_113:                              # %._crit_edge379
                                        #   in Loop: Header=BB1_33 Depth=2
	movq	64(%rsp), %r8           # 8-byte Reload
	movl	%r8d, %eax
	subl	80(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	116(%rsp), %ebp
	movl	120(%rsp), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	168(%rsp), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	172(%rsp), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	124(%rsp), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movl	176(%rsp), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	128(%rsp), %ebx
	movl	36(%rsp), %r11d         # 4-byte Reload
	cmpl	%ecx, %r8d
	jb	.LBB1_33
.LBB1_114:                              # %._crit_edge392
                                        #   in Loop: Header=BB1_21 Depth=1
	movl	%ebx, 40(%rsp)          # 4-byte Spill
	movl	$49152, %ebx            # imm = 0xC000
	cmpl	$49152, 224(%rsp)       # 4-byte Folded Reload
                                        # imm = 0xC000
	jl	.LBB1_118
# BB#115:                               #   in Loop: Header=BB1_21 Depth=1
	movq	200(%rsp), %rax         # 8-byte Reload
	subl	12(%rsp), %eax          # 4-byte Folded Reload
	cmpl	$49153, %eax            # imm = 0xC001
	jl	.LBB1_117
# BB#116:                               #   in Loop: Header=BB1_21 Depth=1
	movl	$1, TRUNCATE(%rip)
	movl	$49152, %eax            # imm = 0xC000
.LBB1_117:                              #   in Loop: Header=BB1_21 Depth=1
	movslq	%eax, %r13
	leaq	49440(%rsp), %rdi
	subq	%r13, %rdi
	movslq	12(%rsp), %rax          # 4-byte Folded Reload
	leaq	288(%rsp,%rax), %rsi
	movq	%r13, %rdx
	callq	strncpy
	movl	$49152, %ebx            # imm = 0xC000
	subl	%r13d, %ebx
	movl	$1, %eax
	cmovel	%eax, %ebx
.LBB1_118:                              # %.backedge346
                                        #   in Loop: Header=BB1_21 Depth=1
	movl	$49152, %edx            # imm = 0xC000
	movl	212(%rsp), %edi         # 4-byte Reload
	leaq	49440(%rsp), %rsi
	callq	fill_buf
	movl	%eax, %r10d
	movb	$1, %al
	testl	%r10d, %r10d
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	movq	280(%rsp), %r13         # 8-byte Reload
	movl	40(%rsp), %ebx          # 4-byte Reload
	movl	16(%rsp), %r11d         # 4-byte Reload
	movl	20(%rsp), %edi          # 4-byte Reload
	jg	.LBB1_21
	jmp	.LBB1_120
.LBB1_119:
	incl	num_of_matched(%rip)
	movl	$CurrentFileName, %edi
	callq	puts
.LBB1_120:                              # %.loopexit
	addq	$98600, %rsp            # imm = 0x18128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	asearch, .Lfunc_end1-asearch
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
