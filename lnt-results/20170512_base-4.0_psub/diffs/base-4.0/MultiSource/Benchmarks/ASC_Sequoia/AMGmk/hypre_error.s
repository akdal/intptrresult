	.text
	.file	"hypre_error.bc"
	.globl	hypre_error_handler
	.p2align	4, 0x90
	.type	hypre_error_handler,@function
hypre_error_handler:                    # @hypre_error_handler
	.cfi_startproc
# BB#0:
	orl	%edx, hypre__global_error(%rip)
	retq
.Lfunc_end0:
	.size	hypre_error_handler, .Lfunc_end0-hypre_error_handler
	.cfi_endproc

	.globl	HYPRE_GetError
	.p2align	4, 0x90
	.type	HYPRE_GetError,@function
HYPRE_GetError:                         # @HYPRE_GetError
	.cfi_startproc
# BB#0:
	movl	hypre__global_error(%rip), %eax
	retq
.Lfunc_end1:
	.size	HYPRE_GetError, .Lfunc_end1-HYPRE_GetError
	.cfi_endproc

	.globl	HYPRE_CheckError
	.p2align	4, 0x90
	.type	HYPRE_CheckError,@function
HYPRE_CheckError:                       # @HYPRE_CheckError
	.cfi_startproc
# BB#0:
	andl	%esi, %edi
	movl	%edi, %eax
	retq
.Lfunc_end2:
	.size	HYPRE_CheckError, .Lfunc_end2-HYPRE_CheckError
	.cfi_endproc

	.globl	HYPRE_DescribeError
	.p2align	4, 0x90
	.type	HYPRE_DescribeError,@function
HYPRE_DescribeError:                    # @HYPRE_DescribeError
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movl	%edi, %ebx
	testl	%ebx, %ebx
	je	.LBB3_1
# BB#2:
	testb	$1, %bl
	je	.LBB3_4
# BB#3:
	movups	.L.str.1(%rip), %xmm0
	movups	%xmm0, (%r14)
	movb	$0, 16(%r14)
.LBB3_4:
	testb	$2, %bl
	je	.LBB3_6
# BB#5:
	movups	.L.str.2(%rip), %xmm0
	movups	%xmm0, (%r14)
.LBB3_6:
	testb	$4, %bl
	je	.LBB3_8
# BB#7:
	movl	hypre__global_error(%rip), %edx
	shrl	$3, %edx
	andl	$31, %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sprintf
.LBB3_8:
	testb	$1, %bh
	je	.LBB3_10
# BB#9:
	movups	.L.str.4+11(%rip), %xmm0
	movups	%xmm0, 11(%r14)
	movups	.L.str.4(%rip), %xmm0
	movups	%xmm0, (%r14)
	jmp	.LBB3_10
.LBB3_1:                                # %.thread11
	movabsq	$8030606864216444507, %rax # imm = 0x6F727265206F4E5B
	movq	%rax, (%r14)
	movl	$2121074, 8(%r14)       # imm = 0x205D72
.LBB3_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	HYPRE_DescribeError, .Lfunc_end3-HYPRE_DescribeError
	.cfi_endproc

	.globl	HYPRE_GetErrorArg
	.p2align	4, 0x90
	.type	HYPRE_GetErrorArg,@function
HYPRE_GetErrorArg:                      # @HYPRE_GetErrorArg
	.cfi_startproc
# BB#0:
	movl	hypre__global_error(%rip), %eax
	shrl	$3, %eax
	andl	$31, %eax
	retq
.Lfunc_end4:
	.size	HYPRE_GetErrorArg, .Lfunc_end4-HYPRE_GetErrorArg
	.cfi_endproc

	.type	hypre__global_error,@object # @hypre__global_error
	.bss
	.globl	hypre__global_error
	.p2align	2
hypre__global_error:
	.long	0                       # 0x0
	.size	hypre__global_error, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"[No error] "
	.size	.L.str, 12

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[Generic error] "
	.size	.L.str.1, 17

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"[Memory error] "
	.size	.L.str.2, 16

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"[Error in argument %d] "
	.size	.L.str.3, 24

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"[Method did not converge] "
	.size	.L.str.4, 27


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
