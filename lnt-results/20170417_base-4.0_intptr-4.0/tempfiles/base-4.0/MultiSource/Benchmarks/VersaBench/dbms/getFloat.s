	.text
	.file	"getFloat.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	4286578687              # float -3.40282347E+38
.LCPI0_2:
	.long	2139095039              # float 3.40282347E+38
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_1:
	.quad	-4039728866278637763    # double -3.4028234699999998E+38
.LCPI0_3:
	.quad	5183643170576138045     # double 3.4028234699999998E+38
	.text
	.globl	getFloat
	.p2align	4, 0x90
	.type	getFloat,@function
getFloat:                               # @getFloat
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	callq	getString
	movss	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	testq	%rax, %rax
	je	.LBB0_1
# BB#2:
	leaq	8(%rsp), %rsi
	movq	%rax, %rdi
	callq	strtod
	cvtsd2ss	%xmm0, %xmm1
	movss	%xmm1, (%rbx)
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm1
	jne	.LBB0_6
	jp	.LBB0_6
# BB#3:
	movq	8(%rsp), %rax
	cmpb	$0, (%rax)
	je	.LBB0_6
# BB#4:
	movss	%xmm1, 4(%rsp)          # 4-byte Spill
	callq	__errno_location
	movss	4(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	cmpl	$34, (%rax)
	jne	.LBB0_6
# BB#5:
	movl	$3, %eax
	movss	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB0_11
.LBB0_6:
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB0_8
# BB#7:
	movl	$2, %eax
	movss	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB0_11
.LBB0_1:
	movl	$1, %eax
	jmp	.LBB0_11
.LBB0_8:
	ucomisd	.LCPI0_3(%rip), %xmm0
	jbe	.LBB0_9
# BB#10:
	movss	.LCPI0_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movl	$2, %eax
.LBB0_11:                               # %.sink.split
	movss	%xmm0, (%rbx)
.LBB0_12:
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB0_9:
	xorl	%eax, %eax
	jmp	.LBB0_12
.Lfunc_end0:
	.size	getFloat, .Lfunc_end0-getFloat
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
