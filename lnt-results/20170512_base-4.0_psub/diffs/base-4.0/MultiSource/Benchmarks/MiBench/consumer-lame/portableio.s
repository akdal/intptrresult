	.text
	.file	"portableio.bc"
	.globl	ReadByte
	.p2align	4, 0x90
	.type	ReadByte,@function
ReadByte:                               # @ReadByte
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	callq	_IO_getc
	movzbl	%al, %ecx
	orl	$-256, %eax
	testb	%cl, %cl
	cmovnsl	%ecx, %eax
	popq	%rcx
	retq
.Lfunc_end0:
	.size	ReadByte, .Lfunc_end0-ReadByte
	.cfi_endproc

	.globl	Read16BitsLowHigh
	.p2align	4, 0x90
	.type	Read16BitsLowHigh,@function
Read16BitsLowHigh:                      # @Read16BitsLowHigh
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 32
.Lcfi4:
	.cfi_offset %rbx, -24
.Lcfi5:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	callq	_IO_getc
	movzbl	%al, %r14d
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %eax
	movzwl	%ax, %eax
	leal	(%rax,%r14), %ecx
	testw	%ax, %ax
	leal	-65536(%rax,%r14), %eax
	cmovnsl	%ecx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	Read16BitsLowHigh, .Lfunc_end1-Read16BitsLowHigh
	.cfi_endproc

	.globl	Read16BitsHighLow
	.p2align	4, 0x90
	.type	Read16BitsHighLow,@function
Read16BitsHighLow:                      # @Read16BitsHighLow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	movzbl	%al, %eax
	shll	$8, %ebp
	movzwl	%bp, %ecx
	leal	(%rcx,%rax), %edx
	testw	%cx, %cx
	leal	-65536(%rcx,%rax), %eax
	cmovnsl	%edx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end2:
	.size	Read16BitsHighLow, .Lfunc_end2-Read16BitsHighLow
	.cfi_endproc

	.globl	Write8Bits
	.p2align	4, 0x90
	.type	Write8Bits,@function
Write8Bits:                             # @Write8Bits
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movzbl	%sil, %edi
	movq	%rax, %rsi
	jmp	_IO_putc                # TAILCALL
.Lfunc_end3:
	.size	Write8Bits, .Lfunc_end3-Write8Bits
	.cfi_endproc

	.globl	Write16BitsLowHigh
	.p2align	4, 0x90
	.type	Write16BitsLowHigh,@function
Write16BitsLowHigh:                     # @Write16BitsLowHigh
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %r14, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	movzbl	%bl, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movzbl	%bh, %edi  # NOREX
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_IO_putc                # TAILCALL
.Lfunc_end4:
	.size	Write16BitsLowHigh, .Lfunc_end4-Write16BitsLowHigh
	.cfi_endproc

	.globl	Write16BitsHighLow
	.p2align	4, 0x90
	.type	Write16BitsHighLow,@function
Write16BitsHighLow:                     # @Write16BitsHighLow
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %r14, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	movzbl	%bh, %edi  # NOREX
	movq	%r14, %rsi
	callq	_IO_putc
	movzbl	%bl, %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_IO_putc                # TAILCALL
.Lfunc_end5:
	.size	Write16BitsHighLow, .Lfunc_end5-Write16BitsHighLow
	.cfi_endproc

	.globl	Read24BitsHighLow
	.p2align	4, 0x90
	.type	Read24BitsHighLow,@function
Read24BitsHighLow:                      # @Read24BitsHighLow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	callq	_IO_getc
	movl	%eax, %r14d
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	movzbl	%al, %eax
	shll	$16, %r14d
	movl	%r14d, %ecx
	andl	$16711680, %ecx         # imm = 0xFF0000
	shll	$8, %ebp
	movzwl	%bp, %edx
	orl	%ecx, %edx
	leal	(%rdx,%rax), %ecx
	testl	$8388608, %r14d         # imm = 0x800000
	leal	-16777216(%rax,%rdx), %eax
	cmovel	%ecx, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end6:
	.size	Read24BitsHighLow, .Lfunc_end6-Read24BitsHighLow
	.cfi_endproc

	.globl	Read32Bits
	.p2align	4, 0x90
	.type	Read32Bits,@function
Read32Bits:                             # @Read32Bits
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -32
.Lcfi31:
	.cfi_offset %r14, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	callq	_IO_getc
	movzbl	%al, %r14d
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %eax
	movzwl	%ax, %ebp
	orl	%r14d, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	movzbl	%al, %r14d
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %eax
	orl	%r14d, %eax
	shll	$16, %eax
	orl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end7:
	.size	Read32Bits, .Lfunc_end7-Read32Bits
	.cfi_endproc

	.globl	Read32BitsHighLow
	.p2align	4, 0x90
	.type	Read32BitsHighLow,@function
Read32BitsHighLow:                      # @Read32BitsHighLow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 32
.Lcfi36:
	.cfi_offset %rbx, -32
.Lcfi37:
	.cfi_offset %r14, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	_IO_getc
	movl	%eax, %ebx
	movq	%r14, %rdi
	callq	_IO_getc
	movzbl	%al, %eax
	shll	$8, %ebx
	orl	%eax, %ebx
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	_IO_getc
	movzbl	%al, %eax
	shll	$8, %ebp
	movzwl	%bp, %ecx
	orl	%eax, %ecx
	shll	$16, %ebx
	orl	%ecx, %ebx
	movl	%ebx, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end8:
	.size	Read32BitsHighLow, .Lfunc_end8-Read32BitsHighLow
	.cfi_endproc

	.globl	Write32Bits
	.p2align	4, 0x90
	.type	Write32Bits,@function
Write32Bits:                            # @Write32Bits
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 32
.Lcfi42:
	.cfi_offset %rbx, -24
.Lcfi43:
	.cfi_offset %r14, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	movzbl	%bl, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movzbl	%bh, %edi  # NOREX
	movq	%r14, %rsi
	callq	_IO_putc
	movl	%ebx, %eax
	shrl	$16, %eax
	movzbl	%al, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	shrl	$24, %ebx
	movl	%ebx, %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_IO_putc                # TAILCALL
.Lfunc_end9:
	.size	Write32Bits, .Lfunc_end9-Write32Bits
	.cfi_endproc

	.globl	Write32BitsLowHigh
	.p2align	4, 0x90
	.type	Write32BitsLowHigh,@function
Write32BitsLowHigh:                     # @Write32BitsLowHigh
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -24
.Lcfi48:
	.cfi_offset %r14, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	movzbl	%bl, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movzbl	%bh, %edi  # NOREX
	movq	%r14, %rsi
	callq	_IO_putc
	movl	%ebx, %eax
	shrl	$16, %eax
	movzbl	%al, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	shrl	$24, %ebx
	movl	%ebx, %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_IO_putc                # TAILCALL
.Lfunc_end10:
	.size	Write32BitsLowHigh, .Lfunc_end10-Write32BitsLowHigh
	.cfi_endproc

	.globl	Write32BitsHighLow
	.p2align	4, 0x90
	.type	Write32BitsHighLow,@function
Write32BitsHighLow:                     # @Write32BitsHighLow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -32
.Lcfi53:
	.cfi_offset %r14, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	movl	%ebx, %ebp
	shrl	$16, %ebp
	movl	%ebx, %edi
	shrl	$24, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movzbl	%bpl, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movzbl	%bh, %edi  # NOREX
	movq	%r14, %rsi
	callq	_IO_putc
	movzbl	%bl, %edi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	_IO_putc                # TAILCALL
.Lfunc_end11:
	.size	Write32BitsHighLow, .Lfunc_end11-Write32BitsHighLow
	.cfi_endproc

	.globl	ReadBytes
	.p2align	4, 0x90
	.type	ReadBytes,@function
ReadBytes:                              # @ReadBytes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 32
.Lcfi58:
	.cfi_offset %rbx, -32
.Lcfi59:
	.cfi_offset %r14, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	feof
	testl	%ebp, %ebp
	jg	.LBB12_1
	jmp	.LBB12_4
	.p2align	4, 0x90
.LBB12_3:                               # %.lr.ph
                                        #   in Loop: Header=BB12_1 Depth=1
	decl	%ebp
	incq	%rbx
.LBB12_1:                               # =>This Inner Loop Header: Depth=1
	testl	%eax, %eax
	jne	.LBB12_4
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB12_1 Depth=1
	movq	%r14, %rdi
	callq	_IO_getc
	movb	%al, (%rbx)
	movq	%r14, %rdi
	callq	feof
	cmpl	$2, %ebp
	jge	.LBB12_3
.LBB12_4:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end12:
	.size	ReadBytes, .Lfunc_end12-ReadBytes
	.cfi_endproc

	.globl	ReadBytesSwapped
	.p2align	4, 0x90
	.type	ReadBytesSwapped,@function
ReadBytesSwapped:                       # @ReadBytesSwapped
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 48
.Lcfi66:
	.cfi_offset %rbx, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r15
	movq	%rdi, %r14
	callq	feof
	testl	%ebp, %ebp
	movq	%r15, %rbx
	jle	.LBB13_4
# BB#1:
	testl	%eax, %eax
	movq	%r15, %rbx
	jne	.LBB13_4
# BB#2:
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB13_3:                               # %.lr.ph23
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	_IO_getc
	movb	%al, (%rbx)
	incq	%rbx
	movq	%r14, %rdi
	callq	feof
	cmpl	$2, %ebp
	jl	.LBB13_4
# BB#8:                                 # %.lr.ph23
                                        #   in Loop: Header=BB13_3 Depth=1
	decl	%ebp
	testl	%eax, %eax
	je	.LBB13_3
.LBB13_4:                               # %.preheader
	decq	%rbx
	cmpq	%r15, %rbx
	jbe	.LBB13_7
# BB#5:                                 # %.lr.ph.preheader
	incq	%r15
	.p2align	4, 0x90
.LBB13_6:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%r15), %eax
	movzbl	(%rbx), %ecx
	movb	%cl, -1(%r15)
	movb	%al, (%rbx)
	decq	%rbx
	cmpq	%rbx, %r15
	leaq	1(%r15), %r15
	jb	.LBB13_6
.LBB13_7:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	ReadBytesSwapped, .Lfunc_end13-ReadBytesSwapped
	.cfi_endproc

	.globl	WriteBytes
	.p2align	4, 0x90
	.type	WriteBytes,@function
WriteBytes:                             # @WriteBytes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 32
.Lcfi73:
	.cfi_offset %rbx, -32
.Lcfi74:
	.cfi_offset %r14, -24
.Lcfi75:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testl	%ebp, %ebp
	jle	.LBB14_3
# BB#1:                                 # %.lr.ph.preheader
	incl	%ebp
	.p2align	4, 0x90
.LBB14_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%rbx), %edi
	incq	%rbx
	movq	%r14, %rsi
	callq	_IO_putc
	decl	%ebp
	cmpl	$1, %ebp
	jg	.LBB14_2
.LBB14_3:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end14:
	.size	WriteBytes, .Lfunc_end14-WriteBytes
	.cfi_endproc

	.globl	WriteBytesSwapped
	.p2align	4, 0x90
	.type	WriteBytesSwapped,@function
WriteBytesSwapped:                      # @WriteBytesSwapped
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 32
.Lcfi79:
	.cfi_offset %rbx, -32
.Lcfi80:
	.cfi_offset %r14, -24
.Lcfi81:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rdi, %r14
	testl	%ebp, %ebp
	jle	.LBB15_3
# BB#1:                                 # %.lr.ph.preheader
	movslq	%ebp, %rax
	leaq	-1(%rsi,%rax), %rbx
	incl	%ebp
	.p2align	4, 0x90
.LBB15_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%rbx), %edi
	decq	%rbx
	movq	%r14, %rsi
	callq	_IO_putc
	decl	%ebp
	cmpl	$1, %ebp
	jg	.LBB15_2
.LBB15_3:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end15:
	.size	WriteBytesSwapped, .Lfunc_end15-WriteBytesSwapped
	.cfi_endproc

	.globl	ReadIeeeFloatHighLow
	.p2align	4, 0x90
	.type	ReadIeeeFloatHighLow,@function
ReadIeeeFloatHighLow:                   # @ReadIeeeFloatHighLow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi85:
	.cfi_def_cfa_offset 48
.Lcfi86:
	.cfi_offset %rbx, -32
.Lcfi87:
	.cfi_offset %r14, -24
.Lcfi88:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	feof
	testl	%eax, %eax
	jne	.LBB16_4
# BB#1:                                 # %.lr.ph.i.preheader
	movl	$4, %ebp
	leaq	12(%rsp), %rbx
	.p2align	4, 0x90
.LBB16_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	_IO_getc
	movb	%al, (%rbx)
	movq	%r14, %rdi
	callq	feof
	cmpl	$2, %ebp
	jl	.LBB16_4
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB16_2 Depth=1
	decl	%ebp
	incq	%rbx
	testl	%eax, %eax
	je	.LBB16_2
.LBB16_4:                               # %ReadBytes.exit
	leaq	12(%rsp), %rdi
	callq	ConvertFromIeeeSingle
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end16:
	.size	ReadIeeeFloatHighLow, .Lfunc_end16-ReadIeeeFloatHighLow
	.cfi_endproc

	.globl	ReadIeeeFloatLowHigh
	.p2align	4, 0x90
	.type	ReadIeeeFloatLowHigh,@function
ReadIeeeFloatLowHigh:                   # @ReadIeeeFloatLowHigh
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi93:
	.cfi_def_cfa_offset 48
.Lcfi94:
	.cfi_offset %rbx, -40
.Lcfi95:
	.cfi_offset %r14, -32
.Lcfi96:
	.cfi_offset %r15, -24
.Lcfi97:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	feof
	testl	%eax, %eax
	jne	.LBB17_6
# BB#1:                                 # %.lr.ph23.i.preheader
	leaq	3(%rsp), %rbx
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB17_2:                               # %.lr.ph23.i
                                        # =>This Inner Loop Header: Depth=1
	leal	3(%r15), %ebp
	movq	%r14, %rdi
	callq	_IO_getc
	movb	%al, 1(%rbx)
	incq	%rbx
	movq	%r14, %rdi
	callq	feof
	decq	%r15
	cmpl	$2, %ebp
	jl	.LBB17_3
# BB#7:                                 # %.lr.ph23.i
                                        #   in Loop: Header=BB17_2 Depth=1
	testl	%eax, %eax
	je	.LBB17_2
.LBB17_3:                               # %.preheader.i
	negq	%r15
	testq	%r15, %r15
	jle	.LBB17_6
# BB#4:                                 # %.lr.ph.i.preheader
	leaq	5(%rsp), %rax
	.p2align	4, 0x90
.LBB17_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rax), %ecx
	movzbl	(%rbx), %edx
	movb	%dl, -1(%rax)
	movb	%cl, (%rbx)
	decq	%rbx
	cmpq	%rbx, %rax
	leaq	1(%rax), %rax
	jb	.LBB17_5
.LBB17_6:                               # %ReadBytesSwapped.exit
	leaq	4(%rsp), %rdi
	callq	ConvertFromIeeeSingle
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	ReadIeeeFloatLowHigh, .Lfunc_end17-ReadIeeeFloatLowHigh
	.cfi_endproc

	.globl	ReadIeeeDoubleHighLow
	.p2align	4, 0x90
	.type	ReadIeeeDoubleHighLow,@function
ReadIeeeDoubleHighLow:                  # @ReadIeeeDoubleHighLow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi101:
	.cfi_def_cfa_offset 48
.Lcfi102:
	.cfi_offset %rbx, -32
.Lcfi103:
	.cfi_offset %r14, -24
.Lcfi104:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	feof
	testl	%eax, %eax
	jne	.LBB18_4
# BB#1:                                 # %.lr.ph.i.preheader
	movl	$8, %ebp
	leaq	8(%rsp), %rbx
	.p2align	4, 0x90
.LBB18_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	_IO_getc
	movb	%al, (%rbx)
	movq	%r14, %rdi
	callq	feof
	cmpl	$2, %ebp
	jl	.LBB18_4
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB18_2 Depth=1
	decl	%ebp
	incq	%rbx
	testl	%eax, %eax
	je	.LBB18_2
.LBB18_4:                               # %ReadBytes.exit
	leaq	8(%rsp), %rdi
	callq	ConvertFromIeeeDouble
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end18:
	.size	ReadIeeeDoubleHighLow, .Lfunc_end18-ReadIeeeDoubleHighLow
	.cfi_endproc

	.globl	ReadIeeeDoubleLowHigh
	.p2align	4, 0x90
	.type	ReadIeeeDoubleLowHigh,@function
ReadIeeeDoubleLowHigh:                  # @ReadIeeeDoubleLowHigh
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi107:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi109:
	.cfi_def_cfa_offset 48
.Lcfi110:
	.cfi_offset %rbx, -40
.Lcfi111:
	.cfi_offset %r14, -32
.Lcfi112:
	.cfi_offset %r15, -24
.Lcfi113:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	feof
	testl	%eax, %eax
	jne	.LBB19_6
# BB#1:                                 # %.lr.ph23.i.preheader
	leaq	-1(%rsp), %rbx
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB19_2:                               # %.lr.ph23.i
                                        # =>This Inner Loop Header: Depth=1
	leal	7(%r15), %ebp
	movq	%r14, %rdi
	callq	_IO_getc
	movb	%al, 1(%rbx)
	incq	%rbx
	movq	%r14, %rdi
	callq	feof
	decq	%r15
	cmpl	$2, %ebp
	jl	.LBB19_3
# BB#7:                                 # %.lr.ph23.i
                                        #   in Loop: Header=BB19_2 Depth=1
	testl	%eax, %eax
	je	.LBB19_2
.LBB19_3:                               # %.preheader.i
	negq	%r15
	testq	%r15, %r15
	jle	.LBB19_6
# BB#4:                                 # %.lr.ph.i.preheader
	leaq	1(%rsp), %rax
	.p2align	4, 0x90
.LBB19_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rax), %ecx
	movzbl	(%rbx), %edx
	movb	%dl, -1(%rax)
	movb	%cl, (%rbx)
	decq	%rbx
	cmpq	%rbx, %rax
	leaq	1(%rax), %rax
	jb	.LBB19_5
.LBB19_6:                               # %ReadBytesSwapped.exit
	movq	%rsp, %rdi
	callq	ConvertFromIeeeDouble
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	ReadIeeeDoubleLowHigh, .Lfunc_end19-ReadIeeeDoubleLowHigh
	.cfi_endproc

	.globl	ReadIeeeExtendedHighLow
	.p2align	4, 0x90
	.type	ReadIeeeExtendedHighLow,@function
ReadIeeeExtendedHighLow:                # @ReadIeeeExtendedHighLow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi114:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi117:
	.cfi_def_cfa_offset 48
.Lcfi118:
	.cfi_offset %rbx, -32
.Lcfi119:
	.cfi_offset %r14, -24
.Lcfi120:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	feof
	testl	%eax, %eax
	jne	.LBB20_4
# BB#1:                                 # %.lr.ph.i.preheader
	movl	$10, %ebp
	leaq	6(%rsp), %rbx
	.p2align	4, 0x90
.LBB20_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	_IO_getc
	movb	%al, (%rbx)
	movq	%r14, %rdi
	callq	feof
	cmpl	$2, %ebp
	jl	.LBB20_4
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB20_2 Depth=1
	decl	%ebp
	incq	%rbx
	testl	%eax, %eax
	je	.LBB20_2
.LBB20_4:                               # %ReadBytes.exit
	leaq	6(%rsp), %rdi
	callq	ConvertFromIeeeExtended
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end20:
	.size	ReadIeeeExtendedHighLow, .Lfunc_end20-ReadIeeeExtendedHighLow
	.cfi_endproc

	.globl	ReadIeeeExtendedLowHigh
	.p2align	4, 0x90
	.type	ReadIeeeExtendedLowHigh,@function
ReadIeeeExtendedLowHigh:                # @ReadIeeeExtendedLowHigh
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi125:
	.cfi_def_cfa_offset 64
.Lcfi126:
	.cfi_offset %rbx, -40
.Lcfi127:
	.cfi_offset %r14, -32
.Lcfi128:
	.cfi_offset %r15, -24
.Lcfi129:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	feof
	testl	%eax, %eax
	jne	.LBB21_6
# BB#1:                                 # %.lr.ph23.i.preheader
	leaq	13(%rsp), %rbx
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB21_2:                               # %.lr.ph23.i
                                        # =>This Inner Loop Header: Depth=1
	leal	9(%r15), %ebp
	movq	%r14, %rdi
	callq	_IO_getc
	movb	%al, 1(%rbx)
	incq	%rbx
	movq	%r14, %rdi
	callq	feof
	decq	%r15
	cmpl	$2, %ebp
	jl	.LBB21_3
# BB#7:                                 # %.lr.ph23.i
                                        #   in Loop: Header=BB21_2 Depth=1
	testl	%eax, %eax
	je	.LBB21_2
.LBB21_3:                               # %.preheader.i
	negq	%r15
	testq	%r15, %r15
	jle	.LBB21_6
# BB#4:                                 # %.lr.ph.i.preheader
	leaq	15(%rsp), %rax
	.p2align	4, 0x90
.LBB21_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rax), %ecx
	movzbl	(%rbx), %edx
	movb	%dl, -1(%rax)
	movb	%cl, (%rbx)
	decq	%rbx
	cmpq	%rbx, %rax
	leaq	1(%rax), %rax
	jb	.LBB21_5
.LBB21_6:                               # %ReadBytesSwapped.exit
	leaq	14(%rsp), %rdi
	callq	ConvertFromIeeeExtended
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	ReadIeeeExtendedLowHigh, .Lfunc_end21-ReadIeeeExtendedLowHigh
	.cfi_endproc

	.globl	WriteIeeeFloatLowHigh
	.p2align	4, 0x90
	.type	WriteIeeeFloatLowHigh,@function
WriteIeeeFloatLowHigh:                  # @WriteIeeeFloatLowHigh
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	pushq	%rbx
.Lcfi130:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi131:
	.cfi_def_cfa_offset 32
.Lcfi132:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	12(%rsp), %rdi
	callq	ConvertToIeeeSingle
	movsbl	15(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	14(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	13(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	12(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end22:
	.size	WriteIeeeFloatLowHigh, .Lfunc_end22-WriteIeeeFloatLowHigh
	.cfi_endproc

	.globl	WriteIeeeFloatHighLow
	.p2align	4, 0x90
	.type	WriteIeeeFloatHighLow,@function
WriteIeeeFloatHighLow:                  # @WriteIeeeFloatHighLow
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi134:
	.cfi_def_cfa_offset 32
.Lcfi135:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	12(%rsp), %rdi
	callq	ConvertToIeeeSingle
	movsbl	12(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	13(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	14(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	15(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end23:
	.size	WriteIeeeFloatHighLow, .Lfunc_end23-WriteIeeeFloatHighLow
	.cfi_endproc

	.globl	WriteIeeeDoubleLowHigh
	.p2align	4, 0x90
	.type	WriteIeeeDoubleLowHigh,@function
WriteIeeeDoubleLowHigh:                 # @WriteIeeeDoubleLowHigh
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	pushq	%rbx
.Lcfi136:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi137:
	.cfi_def_cfa_offset 32
.Lcfi138:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	8(%rsp), %rdi
	callq	ConvertToIeeeDouble
	movsbl	15(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	14(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	13(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	12(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	11(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	10(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	9(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	8(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end24:
	.size	WriteIeeeDoubleLowHigh, .Lfunc_end24-WriteIeeeDoubleLowHigh
	.cfi_endproc

	.globl	WriteIeeeDoubleHighLow
	.p2align	4, 0x90
	.type	WriteIeeeDoubleHighLow,@function
WriteIeeeDoubleHighLow:                 # @WriteIeeeDoubleHighLow
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	pushq	%rbx
.Lcfi139:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi140:
	.cfi_def_cfa_offset 32
.Lcfi141:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	8(%rsp), %rdi
	callq	ConvertToIeeeDouble
	movsbl	8(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	9(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	10(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	11(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	12(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	13(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	14(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	15(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end25:
	.size	WriteIeeeDoubleHighLow, .Lfunc_end25-WriteIeeeDoubleHighLow
	.cfi_endproc

	.globl	WriteIeeeExtendedLowHigh
	.p2align	4, 0x90
	.type	WriteIeeeExtendedLowHigh,@function
WriteIeeeExtendedLowHigh:               # @WriteIeeeExtendedLowHigh
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	pushq	%rbx
.Lcfi142:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi143:
	.cfi_def_cfa_offset 32
.Lcfi144:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	6(%rsp), %rdi
	callq	ConvertToIeeeExtended
	movsbl	15(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	14(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	13(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	12(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	11(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	10(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	9(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	8(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	7(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	6(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end26:
	.size	WriteIeeeExtendedLowHigh, .Lfunc_end26-WriteIeeeExtendedLowHigh
	.cfi_endproc

	.globl	WriteIeeeExtendedHighLow
	.p2align	4, 0x90
	.type	WriteIeeeExtendedHighLow,@function
WriteIeeeExtendedHighLow:               # @WriteIeeeExtendedHighLow
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	pushq	%rbx
.Lcfi145:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi146:
	.cfi_def_cfa_offset 32
.Lcfi147:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	6(%rsp), %rdi
	callq	ConvertToIeeeExtended
	movsbl	6(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	7(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	8(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	9(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	10(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	11(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	12(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	13(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	14(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movsbl	15(%rsp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end27:
	.size	WriteIeeeExtendedHighLow, .Lfunc_end27-WriteIeeeExtendedHighLow
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
