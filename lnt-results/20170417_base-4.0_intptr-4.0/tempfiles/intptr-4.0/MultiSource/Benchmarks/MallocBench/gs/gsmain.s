	.text
	.file	"gsmain.bc"
	.globl	gs_main
	.p2align	4, 0x90
	.type	gs_main,@function
gs_main:                                # @gs_main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movq	$0, proc_reloc(%rip)
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	callq	gp_init
	cmpl	$2, %ebp
	jl	.LBB0_20
# BB#1:                                 # %.lr.ph
	movl	%ebp, %r13d
	addq	$8, %rbx
	decq	%r13
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r14, 32(%rsp)          # 8-byte Spill
	jmp	.LBB0_2
.LBB0_10:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	printf
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r15
	cmpb	$45, (%r15)
	jne	.LBB0_18
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movsbl	1(%r15), %edi
	cmpl	$84, %edi
	je	.LBB0_8
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	cmpl	$90, %edi
	jne	.LBB0_5
# BB#7:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$.Lstr, %edi
	callq	puts
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_18:                               #   in Loop: Header=BB0_2 Depth=1
	leal	1(%r12), %ebp
	movq	%r15, %rdi
	movl	%r12d, %esi
	callq	*%r14
	movl	%ebp, %r12d
	jmp	.LBB0_19
.LBB0_8:                                #   in Loop: Header=BB0_2 Depth=1
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_12
# BB#9:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$proc_reloc, %esi
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	trace_open_map
	movq	%rax, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_10
# BB#11:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.3, %edi
	movl	$main, %esi
	xorl	%eax, %eax
	callq	printf
.LBB0_12:                               #   in Loop: Header=BB0_2 Depth=1
	incq	%r15
	movl	$0, 12(%rsp)
	movl	$58, %esi
	movq	%r15, %rdi
	callq	strchr
	movq	%rax, %rbp
	testq	%rbp, %rbp
	movq	%r12, 48(%rsp)          # 8-byte Spill
	je	.LBB0_13
# BB#14:                                #   in Loop: Header=BB0_2 Depth=1
	movq	%rbp, %r14
	incq	%r14
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	leaq	12(%rsp), %rdx
	callq	sscanf
	movb	$0, (%rbp)
	movl	$58, %esi
	movq	%r14, %rdi
	callq	strchr
	leaq	1(%rax), %r14
	testq	%rax, %rax
	cmoveq	%rax, %r14
	jmp	.LBB0_15
.LBB0_5:                                #   in Loop: Header=BB0_2 Depth=1
	leaq	2(%r15), %rsi
	callq	*40(%rsp)               # 8-byte Folded Reload
	testl	%eax, %eax
	jns	.LBB0_19
# BB#6:                                 #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	printf
	jmp	.LBB0_19
.LBB0_13:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%r14d, %r14d
.LBB0_15:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r15, %rdi
	callq	strlen
	leal	1(%rax), %edi
	movl	$1, %esi
	movl	$.L.str.5, %edx
	callq	gs_malloc
	movq	%rax, %r12
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movb	$95, (%r12)
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	strupr
	movl	12(%rsp), %ecx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	callq	trace_name
	testl	%eax, %eax
	jns	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	printf
.LBB0_17:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$1, trace_flush_flag(%rip)
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_19:                               #   in Loop: Header=BB0_2 Depth=1
	addq	$8, %rbx
	decq	%r13
	jne	.LBB0_2
.LBB0_20:                               # %._crit_edge
	movl	%r12d, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	gs_main, .Lfunc_end0-gs_main
	.cfi_endproc

	.globl	gs_exit
	.p2align	4, 0x90
	.type	gs_exit,@function
gs_exit:                                # @gs_exit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	testl	%ebp, %ebp
	je	.LBB1_2
# BB#1:
	movq	stderr(%rip), %rdi
	callq	fflush
.LBB1_2:                                # %.preheader
	movq	gx_device_list(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_7
# BB#3:                                 # %.lr.ph.preheader
	movl	$gx_device_list+8, %ebx
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 52(%rdi)
	je	.LBB1_6
# BB#5:                                 #   in Loop: Header=BB1_4 Depth=1
	movq	8(%rdi), %rax
	callq	*32(%rax)
.LBB1_6:                                #   in Loop: Header=BB1_4 Depth=1
	movq	(%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.LBB1_4
.LBB1_7:                                # %._crit_edge
	movl	%ebp, %edi
	callq	exit
.Lfunc_end1:
	.size	gs_exit, .Lfunc_end1-gs_exit
	.cfi_endproc

	.globl	gs_dump_C_stack
	.p2align	4, 0x90
	.type	gs_dump_C_stack,@function
gs_dump_C_stack:                        # @gs_dump_C_stack
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	xorl	%eax, %eax
	callq	stack_top_frame
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	stack_return
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	stack_return
	movq	%rax, %rcx
	subq	proc_reloc(%rip), %rcx
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	printf
	movq	%rbx, %rdi
	callq	stack_next_frame
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB2_1
# BB#2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	gs_dump_C_stack, .Lfunc_end2-gs_dump_C_stack
	.cfi_endproc

	.type	proc_reloc,@object      # @proc_reloc
	.comm	proc_reloc,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Unknown switch %s - ignoring\n"
	.size	.L.str, 30

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Map file %s is apparently missing or malformed\n"
	.size	.L.str.2, 48

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"[T]main = %lx\n"
	.size	.L.str.3, 15

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%d"
	.size	.L.str.4, 3

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"-T switch"
	.size	.L.str.5, 10

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%s not found\n"
	.size	.L.str.6, 14

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"frame %8lx called from %8lx (%8lx)\n"
	.size	.L.str.7, 36

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Not a debugging configuration, -Z switch ignored"
	.size	.Lstr, 49


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
