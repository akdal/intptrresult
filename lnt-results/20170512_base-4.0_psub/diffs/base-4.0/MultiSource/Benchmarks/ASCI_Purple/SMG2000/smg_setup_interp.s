	.text
	.file	"smg_setup_interp.bc"
	.globl	hypre_SMGCreateInterpOp
	.p2align	4, 0x90
	.type	hypre_SMGCreateInterpOp,@function
hypre_SMGCreateInterpOp:                # @hypre_SMGCreateInterpOp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 64
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	.Lhypre_SMGCreateInterpOp.num_ghost+16(%rip), %rax
	movq	%rax, 16(%rsp)
	movaps	.Lhypre_SMGCreateInterpOp.num_ghost(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movq	24(%rbx), %rax
	movl	16(%rax), %ebp
	movl	$2, %edi
	movl	$12, %esi
	callq	hypre_CAlloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	$0, 16(%rax)
	movslq	%r15d, %rcx
	movl	$-1, (%rax,%rcx,4)
	movl	$1, 12(%rax,%rcx,4)
	movl	$2, %esi
	movl	%ebp, %edi
	movq	%rax, %rdx
	callq	hypre_StructStencilCreate
	movq	%rax, %rbp
	movl	(%rbx), %edi
	movq	%r14, %rsi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixCreate
	movq	%rax, %rbx
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	hypre_StructMatrixSetNumGhost
	movq	%rbp, %rdi
	callq	hypre_StructStencilDestroy
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_SMGCreateInterpOp, .Lfunc_end0-hypre_SMGCreateInterpOp
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	hypre_SMGSetupInterpOp
	.p2align	4, 0x90
	.type	hypre_SMGSetupInterpOp,@function
hypre_SMGSetupInterpOp:                 # @hypre_SMGSetupInterpOp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$456, %rsp              # imm = 0x1C8
.Lcfi15:
	.cfi_def_cfa_offset 512
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rdx, 312(%rsp)         # 8-byte Spill
	movq	%rdi, %rbx
	movq	8(%rsi), %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	%rsi, 304(%rsp)         # 8-byte Spill
	movq	24(%rsi), %rax
	movq	(%rax), %rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movl	8(%rax), %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r8, 160(%rsp)          # 8-byte Spill
	movq	24(%r8), %rax
	movq	(%rax), %rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	8(%rax), %ebp
	movl	$1, %esi
	callq	hypre_SMGRelaxSetMaxIter
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	hypre_SMGRelaxSetNumPreSpaces
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	hypre_SMGRelaxSetNumRegSpaces
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	movq	%rbx, %rdi
	callq	hypre_SMGRelaxSetRegSpaceRank
	movl	$1, %edi
	movl	$12, %esi
	callq	hypre_CAlloc
	movl	$1, %edi
	movl	$1, %esi
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	callq	hypre_StructStencilCreate
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	%rbp, 288(%rsp)         # 8-byte Spill
	testl	%ebp, %ebp
	jle	.LBB1_46
# BB#1:                                 # %.lr.ph492
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	(,%rax,4), %ecx
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	movslq	%r14d, %rcx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 272(%rsp)         # 8-byte Spill
	leaq	(%rax,%rcx,4), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
                                        # implicit-def: %RBX
	.p2align	4, 0x90
.LBB1_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_10 Depth 2
                                        #     Child Loop BB1_16 Depth 2
                                        #       Child Loop BB1_23 Depth 3
                                        #         Child Loop BB1_25 Depth 4
                                        #           Child Loop BB1_29 Depth 5
                                        #             Child Loop BB1_31 Depth 6
                                        #               Child Loop BB1_33 Depth 7
                                        #               Child Loop BB1_37 Depth 7
	movq	%rbx, %r12
	movl	72(%rsp), %edi          # 4-byte Reload
	callq	hypre_MAlloc
	movq	%rax, %r14
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	movl	$0, %esi
	jle	.LBB1_15
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB1_2 Depth=1
	cmpq	$0, 264(%rsp)           # 8-byte Folded Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rax,2), %rax
	movq	152(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,4), %rax
	movq	272(%rsp), %rdx         # 8-byte Reload
	leaq	(%rax,%rdx,4), %rax
	jne	.LBB1_6
# BB#4:                                 #   in Loop: Header=BB1_2 Depth=1
	xorl	%ecx, %ecx
                                        # implicit-def: %ESI
	jmp	.LBB1_5
	.p2align	4, 0x90
.LBB1_6:                                #   in Loop: Header=BB1_2 Depth=1
	movq	120(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rdx,4), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB1_8
# BB#7:                                 #   in Loop: Header=BB1_2 Depth=1
	xorl	%esi, %esi
	movl	$1, %ecx
.LBB1_5:                                #   in Loop: Header=BB1_2 Depth=1
	xorl	%ebx, %ebx
	cmpl	$1, 32(%rsp)            # 4-byte Folded Reload
	jne	.LBB1_9
	jmp	.LBB1_15
.LBB1_8:                                #   in Loop: Header=BB1_2 Depth=1
	movl	$0, (%r14)
	movl	$1, %esi
	movl	$1, %ecx
	movl	$1, %ebx
	cmpl	$1, 32(%rsp)            # 4-byte Folded Reload
	je	.LBB1_15
.LBB1_9:                                # %.lr.ph.new
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	32(%rsp), %rdx          # 8-byte Reload
	subq	%rcx, %rdx
	leaq	(%rcx,%rcx,2), %rsi
	movq	256(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rsi,4), %rdi
	xorl	%ebp, %ebp
	movl	%ebx, %esi
	.p2align	4, 0x90
.LBB1_10:                               #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %ebx
	cmpl	(%rax), %ebx
	je	.LBB1_12
# BB#11:                                #   in Loop: Header=BB1_10 Depth=2
	movslq	%esi, %rsi
	leal	(%rcx,%rbp), %ebx
	movl	%ebx, (%r14,%rsi,4)
	incl	%esi
.LBB1_12:                               #   in Loop: Header=BB1_10 Depth=2
	movl	12(%rdi), %ebx
	cmpl	(%rax), %ebx
	je	.LBB1_14
# BB#13:                                #   in Loop: Header=BB1_10 Depth=2
	movslq	%esi, %rsi
	leal	1(%rcx,%rbp), %ebx
	movl	%ebx, (%r14,%rsi,4)
	incl	%esi
.LBB1_14:                               #   in Loop: Header=BB1_10 Depth=2
	addq	$2, %rbp
	addq	$24, %rdi
	cmpq	%rbp, %rdx
	jne	.LBB1_10
.LBB1_15:                               # %._crit_edge
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	304(%rsp), %rdi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r14, %rdx
	callq	hypre_StructMatrixCreateMask
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	hypre_Free
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	callq	hypre_StructVectorClearGhostValues
	movq	%r14, %rdi
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	hypre_StructVectorSetConstantValues
	xorps	%xmm0, %xmm0
	movq	312(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdi
	callq	hypre_StructVectorSetConstantValues
	movq	144(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rdi
	movq	136(%rsp), %rsi         # 8-byte Reload
	callq	hypre_SMGRelaxSetNewMatrixStencil
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	movq	%r14, %rcx
	callq	hypre_SMGRelaxSetup
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	movq	%r14, %rcx
	callq	hypre_SMGRelax
	movq	%r15, %rdi
	callq	hypre_StructMatrixDestroy
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rax,2), %rax
	movq	152(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,4), %rcx
	movq	%rcx, 336(%rsp)         # 8-byte Spill
	movl	(%rdx,%rax,4), %ecx
	movq	280(%rsp), %rsi         # 8-byte Reload
	movl	%ecx, (%rsi)
	movl	4(%rdx,%rax,4), %ecx
	movl	%ecx, 4(%rsi)
	movl	8(%rdx,%rax,4), %eax
	movl	%eax, 8(%rsi)
	movq	296(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rdi
	movq	128(%rsp), %rsi         # 8-byte Reload
	leaq	224(%rsp), %rdx
	leaq	216(%rsp), %rcx
	leaq	448(%rsp), %r8
	leaq	440(%rsp), %r9
	leaq	200(%rsp), %rax
	pushq	%rax
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	leaq	216(%rsp), %rax
	pushq	%rax
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	callq	hypre_CreateComputeInfo
	addq	$16, %rsp
.Lcfi24:
	.cfi_adjust_cfa_offset -16
	movq	224(%rsp), %rdi
	movq	520(%rsp), %rbx
	movq	%rbx, %rsi
	movq	528(%rsp), %rbp
	movq	%rbp, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	216(%rsp), %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	208(%rsp), %rdi
	movq	512(%rsp), %rbx
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	200(%rsp), %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	224(%rsp), %rdi
	movq	216(%rsp), %rsi
	movq	448(%rsp), %r8
	movq	440(%rsp), %r9
	subq	$8, %rsp
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	movq	%rbp, %rdx
	movq	%rbp, %rcx
	leaq	56(%rsp), %rax
	pushq	%rax
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	pushq	16(%r14)
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	pushq	248(%rsp)
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	pushq	264(%rsp)
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	callq	hypre_ComputePkgCreate
	addq	$64, %rsp
.Lcfi33:
	.cfi_adjust_cfa_offset -64
	xorl	%eax, %eax
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB1_16:                               #   Parent Loop BB1_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_23 Depth 3
                                        #         Child Loop BB1_25 Depth 4
                                        #           Child Loop BB1_29 Depth 5
                                        #             Child Loop BB1_31 Depth 6
                                        #               Child Loop BB1_33 Depth 7
                                        #               Child Loop BB1_37 Depth 7
	cmpl	$1, %eax
	movl	%eax, 76(%rsp)          # 4-byte Spill
	je	.LBB1_19
# BB#17:                                #   in Loop: Header=BB1_16 Depth=2
	testl	%eax, %eax
	jne	.LBB1_21
# BB#18:                                #   in Loop: Header=BB1_16 Depth=2
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rsi
	movq	48(%rsp), %rdi
	leaq	432(%rsp), %rdx
	callq	hypre_InitializeIndtComputations
	movq	48(%rsp), %rax
	addq	$8, %rax
	jmp	.LBB1_20
	.p2align	4, 0x90
.LBB1_19:                               #   in Loop: Header=BB1_16 Depth=2
	movq	432(%rsp), %rdi
	callq	hypre_FinalizeIndtComputations
	movq	48(%rsp), %rax
	addq	$16, %rax
.LBB1_20:                               # %.sink.split
                                        #   in Loop: Header=BB1_16 Depth=2
	movq	(%rax), %rbx
.LBB1_21:                               #   in Loop: Header=BB1_16 Depth=2
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB1_44
# BB#22:                                # %.lr.ph479
                                        #   in Loop: Header=BB1_16 Depth=2
	xorl	%edi, %edi
	movq	%rbx, 320(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_23:                               #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_25 Depth 4
                                        #           Child Loop BB1_29 Depth 5
                                        #             Child Loop BB1_31 Depth 6
                                        #               Child Loop BB1_33 Depth 7
                                        #               Child Loop BB1_37 Depth 7
	movq	(%rbx), %rcx
	movq	(%rcx,%rdi,8), %rbp
	cmpl	$0, 8(%rbp)
	jle	.LBB1_43
# BB#24:                                # %.lr.ph471
                                        #   in Loop: Header=BB1_23 Depth=3
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	16(%rdx), %rax
	movq	24(%rdx), %rbx
	movq	(%rax), %rcx
	movq	160(%rsp), %rsi         # 8-byte Reload
	movq	40(%rsi), %rax
	movq	48(%rsi), %r8
	movq	(%rax), %r9
	movq	40(%rdx), %rdx
	movslq	(%rdx,%rdi,4), %rax
	movq	%rbx, 376(%rsp)         # 8-byte Spill
	leaq	(%rbx,%rax,8), %rdx
	movq	%rdx, 408(%rsp)         # 8-byte Spill
	movq	64(%rsi), %rdx
	movq	(%rdx,%rdi,8), %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movslq	(%rdx,%rsi,4), %rdx
	movq	%rdi, 328(%rsp)         # 8-byte Spill
	leaq	(,%rdi,8), %rsi
	leaq	(%rsi,%rsi,2), %rsi
	leaq	4(%rcx,%rsi), %rdi
	movq	%rdi, 176(%rsp)         # 8-byte Spill
	addq	%rsi, %rcx
	movq	%rcx, 352(%rsp)         # 8-byte Spill
	leaq	(%r9,%rsi), %rcx
	movq	%rcx, 344(%rsp)         # 8-byte Spill
	leaq	4(%r9,%rsi), %rcx
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	leaq	(%r8,%rdx,8), %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	shlq	$3, %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rbp, 360(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_25:                               #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        #       Parent Loop BB1_23 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB1_29 Depth 5
                                        #             Child Loop BB1_31 Depth 6
                                        #               Child Loop BB1_33 Depth 7
                                        #               Child Loop BB1_37 Depth 7
	movq	(%rbp), %rax
	movq	%rcx, 368(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,8), %rbx
	movl	(%rax,%rcx,8), %edx
	movl	%edx, 4(%rsp)
	movl	4(%rax,%rcx,8), %edx
	movl	%edx, 8(%rsp)
	movl	8(%rax,%rcx,8), %eax
	movl	%eax, 12(%rsp)
	leaq	4(%rsp), %rdi
	movq	512(%rsp), %rsi
	movq	528(%rsp), %rbp
	movq	%rbp, %rdx
	leaq	232(%rsp), %rcx
	callq	hypre_StructMapFineToCoarse
	movq	336(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx), %eax
	addl	%eax, 4(%rsp)
	movl	4(%rcx), %eax
	addl	%eax, 8(%rsp)
	movl	8(%rcx), %eax
	addl	%eax, 12(%rsp)
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	leaq	244(%rsp), %rdx
	callq	hypre_BoxGetStrideSize
	movq	344(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r13d
	movq	168(%rsp), %rcx         # 8-byte Reload
	movl	8(%rcx), %eax
	movl	%eax, %esi
	subl	%r13d, %esi
	incl	%esi
	cmpl	%r13d, %eax
	movl	(%rcx), %r12d
	movl	12(%rcx), %ecx
	movl	$0, %ebp
	cmovsl	%ebp, %esi
	movl	%esi, 28(%rsp)          # 4-byte Spill
	movl	%ecx, %edi
	subl	%r12d, %edi
	incl	%edi
	cmpl	%r12d, %ecx
	movq	352(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx), %eax
	movq	176(%rsp), %rsi         # 8-byte Reload
	movl	8(%rsi), %ecx
	cmovsl	%ebp, %edi
	movl	%ecx, %r11d
	subl	%eax, %r11d
	incl	%r11d
	cmpl	%eax, %ecx
	movl	(%rsi), %r15d
	movl	12(%rsi), %esi
	cmovsl	%ebp, %r11d
	movl	%esi, %ecx
	subl	%r15d, %ecx
	incl	%ecx
	cmpl	%r15d, %esi
	cmovsl	%ebp, %ecx
	movl	244(%rsp), %esi
	movl	248(%rsp), %ebp
	movl	252(%rsp), %ebx
	cmpl	%esi, %ebp
	movq	%rsi, 64(%rsp)          # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	cmovgel	%ebp, %esi
	cmpl	%esi, %ebx
	movl	%ebx, 24(%rsp)          # 4-byte Spill
	cmovgel	%ebx, %esi
	testl	%esi, %esi
	jle	.LBB1_41
# BB#26:                                # %.lr.ph465
                                        #   in Loop: Header=BB1_25 Depth=4
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_41
# BB#27:                                # %.lr.ph465
                                        #   in Loop: Header=BB1_25 Depth=4
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_41
# BB#28:                                # %.preheader425.us.preheader
                                        #   in Loop: Header=BB1_25 Depth=4
	movq	528(%rsp), %rsi
	movq	%rsi, %rbp
	movslq	(%rbp), %r8
	movl	%eax, (%rsp)            # 4-byte Spill
	movl	4(%rbp), %eax
	imull	%r11d, %eax
	movl	%r11d, %edx
	imull	%ecx, %edx
	imull	8(%rbp), %edx
	movq	64(%rsp), %r14          # 8-byte Reload
	movl	%r14d, %ebp
	imull	%r8d, %ebp
	movl	%eax, %esi
	subl	%ebp, %esi
	movq	40(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %r10d
	imull	%eax, %r10d
	leal	-1(%rbx), %ebx
	movl	%esi, 116(%rsp)         # 4-byte Spill
	imull	%ebx, %esi
	addl	%eax, %esi
	movl	28(%rsp), %r9d          # 4-byte Reload
	movl	%r9d, %eax
	subl	%r14d, %eax
	movl	%r10d, 84(%rsp)         # 4-byte Spill
	subl	%r10d, %edx
	movl	%edx, 100(%rsp)         # 4-byte Spill
	subl	%ebp, %esi
	movl	%esi, 96(%rsp)          # 4-byte Spill
	movl	%edi, %edx
	movq	40(%rsp), %r10          # 8-byte Reload
	subl	%r10d, %edx
	imull	%r9d, %edx
	movl	%edx, 88(%rsp)          # 4-byte Spill
	imull	%ebx, %eax
	addl	%r9d, %eax
	subl	%r14d, %eax
	movl	%eax, 92(%rsp)          # 4-byte Spill
	movl	232(%rsp), %esi
	subl	%r13d, %esi
	movl	236(%rsp), %edx
	subl	%r12d, %edx
	movl	240(%rsp), %ebp
	movq	168(%rsp), %rax         # 8-byte Reload
	subl	4(%rax), %ebp
	imull	%edi, %ebp
	leal	-1(%r14), %eax
	addl	%edx, %ebp
	movq	%rax, 424(%rsp)         # 8-byte Spill
	incq	%rax
	imulq	%r8, %rax
	movq	%rax, 416(%rsp)         # 8-byte Spill
	imull	%r9d, %ebp
	addl	%esi, %ebp
	movl	4(%rsp), %eax
	subl	(%rsp), %eax            # 4-byte Folded Reload
	movl	8(%rsp), %edx
	subl	%r15d, %edx
	movl	12(%rsp), %edi
	movq	176(%rsp), %rsi         # 8-byte Reload
	subl	4(%rsi), %edi
	imull	%ecx, %edi
	imull	%r10d, %r9d
	movl	%r9d, 80(%rsp)          # 4-byte Spill
	addl	%edx, %edi
	imull	%r11d, %edi
	addl	%eax, %edi
	movl	%r14d, %r11d
	andl	$3, %r11d
	movq	%r8, %r9
	shlq	$5, %r9
	leaq	(%r8,%r8,2), %rax
	movq	184(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, 400(%rsp)         # 8-byte Spill
	movq	%r8, %rax
	shlq	$4, %rax
	addq	%rcx, %rax
	movq	%rax, 392(%rsp)         # 8-byte Spill
	leaq	(%rcx,%r8,8), %rax
	movq	%rax, 384(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_29:                               # %.preheader425.us
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        #       Parent Loop BB1_23 Depth=3
                                        #         Parent Loop BB1_25 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB1_31 Depth 6
                                        #               Child Loop BB1_33 Depth 7
                                        #               Child Loop BB1_37 Depth 7
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	movl	96(%rsp), %eax          # 4-byte Reload
	movl	92(%rsp), %edx          # 4-byte Reload
	jle	.LBB1_40
# BB#30:                                # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB1_29 Depth=5
	movl	%ecx, 104(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	movl	%ebp, 112(%rsp)         # 4-byte Spill
	movl	%ebp, %r13d
	movl	%edi, 108(%rsp)         # 4-byte Spill
	movl	%edi, %r12d
	.p2align	4, 0x90
.LBB1_31:                               # %.preheader.us.us
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        #       Parent Loop BB1_23 Depth=3
                                        #         Parent Loop BB1_25 Depth=4
                                        #           Parent Loop BB1_29 Depth=5
                                        # =>          This Loop Header: Depth=6
                                        #               Child Loop BB1_33 Depth 7
                                        #               Child Loop BB1_37 Depth 7
	movl	%eax, (%rsp)            # 4-byte Spill
	movslq	%r12d, %r12
	testl	%r11d, %r11d
	movslq	%r13d, %r13
	movq	%r12, %r10
	movq	%r13, %rsi
	movl	$0, %r15d
	je	.LBB1_35
# BB#32:                                # %.prol.preheader523
                                        #   in Loop: Header=BB1_31 Depth=6
	movq	192(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r13,8), %rax
	xorl	%r15d, %r15d
	movq	%r12, %r10
	movq	408(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_33:                               #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        #       Parent Loop BB1_23 Depth=3
                                        #         Parent Loop BB1_25 Depth=4
                                        #           Parent Loop BB1_29 Depth=5
                                        #             Parent Loop BB1_31 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movq	(%rcx,%r10,8), %rdx
	movq	%rdx, (%rax,%r15,8)
	addq	%r8, %r10
	incq	%r15
	cmpl	%r15d, %r11d
	jne	.LBB1_33
# BB#34:                                # %.prol.loopexit524.unr-lcssa
                                        #   in Loop: Header=BB1_31 Depth=6
	leaq	(%r13,%r15), %rsi
.LBB1_35:                               # %.prol.loopexit524
                                        #   in Loop: Header=BB1_31 Depth=6
	addq	416(%rsp), %r12         # 8-byte Folded Reload
	cmpl	$3, 424(%rsp)           # 4-byte Folded Reload
	jb	.LBB1_38
# BB#36:                                # %.preheader.us.us.new
                                        #   in Loop: Header=BB1_31 Depth=6
	movq	184(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r10,8), %r14
	movq	400(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r10,8), %rax
	movq	392(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r10,8), %rdx
	movq	384(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r10,8), %rbx
	movq	192(%rsp), %rcx         # 8-byte Reload
	leaq	24(%rcx,%rsi,8), %rbp
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %esi
	subl	%r15d, %esi
	movq	376(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_37:                               #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        #       Parent Loop BB1_23 Depth=3
                                        #         Parent Loop BB1_25 Depth=4
                                        #           Parent Loop BB1_29 Depth=5
                                        #             Parent Loop BB1_31 Depth=6
                                        # =>            This Inner Loop Header: Depth=7
	movq	(%r14,%rdi), %rcx
	movq	%rcx, -24(%rbp)
	movq	(%rbx,%rdi), %rcx
	movq	%rcx, -16(%rbp)
	movq	(%rdx,%rdi), %rcx
	movq	%rcx, -8(%rbp)
	movq	(%rax,%rdi), %rcx
	movq	%rcx, (%rbp)
	addq	%r9, %rdi
	addq	$32, %rbp
	addl	$-4, %esi
	jne	.LBB1_37
.LBB1_38:                               # %._crit_edge433.us.us
                                        #   in Loop: Header=BB1_31 Depth=6
	addl	116(%rsp), %r12d        # 4-byte Folded Reload
	addl	28(%rsp), %r13d         # 4-byte Folded Reload
	movl	(%rsp), %eax            # 4-byte Reload
	incl	%eax
	cmpl	40(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB1_31
# BB#39:                                #   in Loop: Header=BB1_29 Depth=5
	movl	84(%rsp), %eax          # 4-byte Reload
	movl	80(%rsp), %edx          # 4-byte Reload
	movl	112(%rsp), %ebp         # 4-byte Reload
	movl	108(%rsp), %edi         # 4-byte Reload
	movl	104(%rsp), %ecx         # 4-byte Reload
.LBB1_40:                               # %._crit_edge439.us
                                        #   in Loop: Header=BB1_29 Depth=5
	addl	%ebp, %edx
	addl	%edi, %eax
	addl	100(%rsp), %eax         # 4-byte Folded Reload
	addl	88(%rsp), %edx          # 4-byte Folded Reload
	incl	%ecx
	cmpl	24(%rsp), %ecx          # 4-byte Folded Reload
	movl	%edx, %ebp
	movl	%eax, %edi
	jne	.LBB1_29
.LBB1_41:                               # %._crit_edge466
                                        #   in Loop: Header=BB1_25 Depth=4
	movq	368(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	movq	360(%rsp), %rbp         # 8-byte Reload
	movslq	8(%rbp), %rax
	cmpq	%rax, %rcx
	jl	.LBB1_25
# BB#42:                                # %._crit_edge472.loopexit
                                        #   in Loop: Header=BB1_23 Depth=3
	movq	320(%rsp), %rbx         # 8-byte Reload
	movl	8(%rbx), %eax
	movq	328(%rsp), %rdi         # 8-byte Reload
.LBB1_43:                               # %._crit_edge472
                                        #   in Loop: Header=BB1_23 Depth=3
	incq	%rdi
	movslq	%eax, %rcx
	cmpq	%rcx, %rdi
	jl	.LBB1_23
.LBB1_44:                               # %._crit_edge480
                                        #   in Loop: Header=BB1_16 Depth=2
	movl	76(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	$2, %eax
	jne	.LBB1_16
# BB#45:                                #   in Loop: Header=BB1_2 Depth=1
	movq	48(%rsp), %rdi
	callq	hypre_ComputePkgDestroy
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpq	288(%rsp), %rcx         # 8-byte Folded Reload
	jne	.LBB1_2
.LBB1_46:                               # %._crit_edge493
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	136(%rsp), %rsi         # 8-byte Reload
	callq	hypre_SMGRelaxSetNewMatrixStencil
	movq	128(%rsp), %rdi         # 8-byte Reload
	callq	hypre_StructStencilDestroy
	movq	160(%rsp), %rdi         # 8-byte Reload
	callq	hypre_StructMatrixAssemble
	xorl	%eax, %eax
	addq	$456, %rsp              # imm = 0x1C8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hypre_SMGSetupInterpOp, .Lfunc_end1-hypre_SMGSetupInterpOp
	.cfi_endproc

	.type	.Lhypre_SMGCreateInterpOp.num_ghost,@object # @hypre_SMGCreateInterpOp.num_ghost
	.section	.rodata,"a",@progbits
	.p2align	4
.Lhypre_SMGCreateInterpOp.num_ghost:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.size	.Lhypre_SMGCreateInterpOp.num_ghost, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
