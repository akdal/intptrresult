	.text
	.file	"jdatadst.bc"
	.globl	jpeg_stdio_dest
	.p2align	4, 0x90
	.type	jpeg_stdio_dest,@function
jpeg_stdio_dest:                        # @jpeg_stdio_dest
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	32(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB0_2
# BB#1:
	movq	8(%rbx), %rax
	xorl	%esi, %esi
	movl	$56, %edx
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, 32(%rbx)
.LBB0_2:
	movq	$init_destination, 16(%rax)
	movq	$empty_output_buffer, 24(%rax)
	movq	$term_destination, 32(%rax)
	movq	%r14, 40(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	jpeg_stdio_dest, .Lfunc_end0-jpeg_stdio_dest
	.cfi_endproc

	.p2align	4, 0x90
	.type	init_destination,@function
init_destination:                       # @init_destination
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	movq	8(%rdi), %rax
	movq	32(%rdi), %rbx
	movl	$1, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	*(%rax)
	movq	%rax, 48(%rbx)
	movq	%rax, (%rbx)
	movq	$4096, 8(%rbx)          # imm = 0x1000
	popq	%rbx
	retq
.Lfunc_end1:
	.size	init_destination, .Lfunc_end1-init_destination
	.cfi_endproc

	.p2align	4, 0x90
	.type	empty_output_buffer,@function
empty_output_buffer:                    # @empty_output_buffer
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	32(%r14), %rbx
	movq	40(%rbx), %rcx
	movq	48(%rbx), %rdi
	movl	$1, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	fwrite
	cmpq	$4096, %rax             # imm = 0x1000
	je	.LBB2_2
# BB#1:
	movq	(%r14), %rax
	movl	$36, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB2_2:
	movq	48(%rbx), %rax
	movq	%rax, (%rbx)
	movq	$4096, 8(%rbx)          # imm = 0x1000
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	empty_output_buffer, .Lfunc_end2-empty_output_buffer
	.cfi_endproc

	.p2align	4, 0x90
	.type	term_destination,@function
term_destination:                       # @term_destination
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	32(%r14), %rbx
	movl	$4096, %r15d            # imm = 0x1000
	subq	8(%rbx), %r15
	je	.LBB3_1
# BB#2:
	movq	40(%rbx), %rcx
	movq	48(%rbx), %rdi
	addq	$40, %rbx
	movl	$1, %esi
	movq	%r15, %rdx
	callq	fwrite
	cmpq	%r15, %rax
	je	.LBB3_4
# BB#3:
	movq	(%r14), %rax
	movl	$36, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
	jmp	.LBB3_4
.LBB3_1:                                # %._crit_edge
	addq	$40, %rbx
.LBB3_4:
	movq	(%rbx), %rdi
	callq	fflush
	movq	(%rbx), %rdi
	callq	ferror
	testl	%eax, %eax
	je	.LBB3_5
# BB#6:
	movq	(%r14), %rax
	movl	$36, 40(%rax)
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*(%rax)                 # TAILCALL
.LBB3_5:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	term_destination, .Lfunc_end3-term_destination
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
