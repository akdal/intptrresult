if [ "$#" -ne 1 ]; then
echo "make-sortedcsv.sh <dir>"
exit 1
fi
python ../../lnt/parse.py $1 cc.csv rt.csv
echo "Making cc.sorted.1per.csv : "
python ../../lnt/sort.1per.py cc.csv cc.sorted.1per.csv
echo "Making rt.sorted.1per.csv : "
python ../../lnt/sort.1per.py rt.csv rt.sorted.1per.csv
