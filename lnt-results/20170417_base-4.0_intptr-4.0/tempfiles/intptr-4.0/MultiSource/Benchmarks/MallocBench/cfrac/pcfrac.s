	.text
	.file	"pcfrac.bc"
	.globl	setBit
	.p2align	4, 0x90
	.type	setBit,@function
setBit:                                 # @setBit
	.cfi_startproc
# BB#0:
	movl	%esi, %r8d
	shrl	$3, %r8d
	xorl	%eax, %eax
	testl	%edx, %edx
	setne	%al
	andb	$7, %sil
	movl	%esi, %ecx
	shll	%cl, %eax
	movzbl	(%rdi,%r8), %ecx
	orl	%eax, %ecx
	movb	%cl, (%rdi,%r8)
	retq
.Lfunc_end0:
	.size	setBit, .Lfunc_end0-setBit
	.cfi_endproc

	.globl	getBit
	.p2align	4, 0x90
	.type	getBit,@function
getBit:                                 # @getBit
	.cfi_startproc
# BB#0:
	movl	%esi, %eax
	shrl	$3, %eax
	movzbl	(%rdi,%rax), %eax
	andb	$7, %sil
	movl	%esi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	retq
.Lfunc_end1:
	.size	getBit, .Lfunc_end1-getBit
	.cfi_endproc

	.globl	newBitVector
	.p2align	4, 0x90
	.type	newBitVector,@function
newBitVector:                           # @newBitVector
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r14
	movl	%r15d, %r13d
	leaq	7(%r13), %r12
	shrq	$3, %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_10
# BB#1:
	leaq	(%r14,%r13), %rbp
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	callq	memset
	testb	$1, %r13b
	je	.LBB2_4
# BB#2:
	cmpb	$0, -1(%rbp)
	leaq	-1(%rbp), %rbp
	je	.LBB2_4
# BB#3:
	movq	%rbp, %rcx
	subq	%r14, %rcx
	movl	%ecx, %eax
	shrl	$3, %eax
	andb	$7, %cl
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edx
	movzbl	(%rbx,%rax), %ecx
	orl	%edx, %ecx
	movb	%cl, (%rbx,%rax)
.LBB2_4:                                # %.prol.loopexit
	cmpl	$1, %r15d
	je	.LBB2_10
	.p2align	4, 0x90
.LBB2_5:                                # =>This Inner Loop Header: Depth=1
	cmpb	$0, -1(%rbp)
	je	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=1
	leaq	-1(%rbp), %rcx
	subq	%r14, %rcx
	movl	%ecx, %eax
	shrl	$3, %eax
	andb	$7, %cl
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edx
	movzbl	(%rbx,%rax), %ecx
	orl	%edx, %ecx
	movb	%cl, (%rbx,%rax)
.LBB2_7:                                #   in Loop: Header=BB2_5 Depth=1
	cmpb	$0, -2(%rbp)
	leaq	-2(%rbp), %rbp
	je	.LBB2_9
# BB#8:                                 #   in Loop: Header=BB2_5 Depth=1
	movq	%rbp, %rcx
	subq	%r14, %rcx
	movl	%ecx, %eax
	shrl	$3, %eax
	andb	$7, %cl
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edx
	movzbl	(%rbx,%rax), %ecx
	orl	%edx, %ecx
	movb	%cl, (%rbx,%rax)
.LBB2_9:                                #   in Loop: Header=BB2_5 Depth=1
	cmpq	%rbp, %r14
	jne	.LBB2_5
.LBB2_10:                               # %.loopexit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	newBitVector, .Lfunc_end2-newBitVector
	.cfi_endproc

	.globl	printSoln
	.p2align	4, 0x90
	.type	printSoln,@function
printSoln:                              # @printSoln
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movl	%r8d, %ebp
	movq	%rcx, %rbx
	movq	%rdi, %r13
	movq	88(%rsp), %r12
	movq	80(%rsp), %rax
	testl	%ebp, %ebp
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	je	.LBB3_1
# BB#2:                                 # %.lr.ph.preheader
	movl	$1, %eax
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ecx
	cmpb	$1, (%r12,%rcx)
	sbbl	$-1, %r14d
	incl	%eax
	cmpl	%ebp, %eax
	jbe	.LBB3_3
	jmp	.LBB3_4
.LBB3_1:
	xorl	%r14d, %r14d
.LBB3_4:                                # %._crit_edge
	movq	%rsi, %rdi
	movq	%r13, %rsi
	callq	fputs
	testq	%r15, %r15
	je	.LBB3_6
# BB#5:
	incw	(%r15)
.LBB3_6:
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	fputp
	movl	$.L.str, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
	testb	$1, (%r12)
	jne	.LBB3_7
# BB#8:
	movl	$43, %edi
	jmp	.LBB3_9
.LBB3_7:
	movl	$45, %edi
.LBB3_9:
	movq	%r13, %rsi
	callq	_IO_putc
	movq	8(%rsp), %rsi           # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB3_11
# BB#10:
	incw	(%rsi)
.LBB3_11:
	movq	%r13, %rdi
	callq	fputp
	testl	%r14d, %r14d
	je	.LBB3_13
# BB#12:
	movl	$.L.str.1, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
.LBB3_13:                               # %.preheader.preheader
	incq	%r12
	.p2align	4, 0x90
.LBB3_14:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r12), %ecx
	testl	%ecx, %ecx
	je	.LBB3_18
# BB#15:                                # %.preheader
                                        #   in Loop: Header=BB3_14 Depth=1
	cmpb	$1, %cl
	jne	.LBB3_17
# BB#16:                                #   in Loop: Header=BB3_14 Depth=1
	movl	(%rbx), %edx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	jmp	.LBB3_18
	.p2align	4, 0x90
.LBB3_17:                               #   in Loop: Header=BB3_14 Depth=1
	movl	(%rbx), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
.LBB3_18:                               #   in Loop: Header=BB3_14 Depth=1
	addq	$4, %rbx
	incq	%r12
	decl	%ebp
	jne	.LBB3_14
# BB#19:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	callq	fputs
	movq	%r13, %rdi
	callq	fflush
	testq	%r15, %r15
	je	.LBB3_22
# BB#20:
	decw	(%r15)
	jne	.LBB3_22
# BB#21:
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	pfree
.LBB3_22:
	movq	8(%rsp), %rdi           # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB3_24
# BB#23:
	decw	(%rdi)
	je	.LBB3_25
.LBB3_24:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_25:
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	pfree                   # TAILCALL
.Lfunc_end3:
	.size	printSoln, .Lfunc_end3-printSoln
	.cfi_endproc

	.globl	combineSoln
	.p2align	4, 0x90
	.type	combineSoln,@function
combineSoln:                            # @combineSoln
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 80
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movl	%r8d, 4(%rsp)           # 4-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	80(%rsp), %rbp
	testq	%r14, %r14
	je	.LBB4_2
# BB#1:
	incw	(%r14)
.LBB4_2:
	testq	%rbp, %rbp
	je	.LBB4_3
# BB#4:
	movq	8(%rbp), %rdi
	movq	(%rbx), %rsi
	callq	pmul
	xorl	%edx, %edx
	movq	$-1, %rcx
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	pdivmod
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	16(%rbp), %rdi
	movq	(%r13), %rsi
	callq	pmul
	xorl	%edx, %edx
	movq	$-1, %rcx
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	pdivmod
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	24(%rbp), %rdi
	movq	(%r13), %rsi
	callq	pmul
	xorl	%edx, %edx
	movq	$-1, %rcx
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	pdivmod
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	32(%rbp), %rax
	movb	(%rax), %al
	andb	$1, %al
	addb	(%r12), %al
	movb	%al, (%r12)
	jmp	.LBB4_5
.LBB4_3:                                # %._crit_edge53
	movb	(%r12), %al
.LBB4_5:
	andb	$1, %al
	movb	%al, (%r12)
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	je	.LBB4_21
# BB#6:                                 # %.lr.ph
	testq	%rbp, %rbp
	movq	%r14, 16(%rsp)          # 8-byte Spill
	je	.LBB4_7
# BB#10:                                # %.lr.ph.split.us.preheader
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB4_11:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%rbp), %rax
	movl	%r15d, %ecx
	shrl	$3, %ecx
	movzbl	(%rax,%rcx), %eax
	movl	%r15d, %ecx
	andb	$7, %cl
	shrl	%cl, %eax
	andl	$1, %eax
	movl	%r15d, %r15d
	movzbl	(%r12,%r15), %ecx
	addl	%eax, %ecx
	movb	%cl, (%r12,%r15)
	cmpb	$2, %cl
	jbe	.LBB4_12
# BB#14:                                #   in Loop: Header=BB4_11 Depth=1
	movq	(%r13), %rbx
	leal	-1(%r15), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	(%rcx,%rax,4), %edi
	callq	utop
	movq	%rax, %r14
	movzbl	(%r12,%r15), %edi
	shrl	%edi
	callq	utop
	movq	%r14, %rdi
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%rax, %rsi
	movq	%r14, %rdx
	callq	ppowmod
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	pmul
	xorl	%edx, %edx
	movq	$-1, %rcx
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	pdivmod
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	psetq
	movb	(%r12,%r15), %bl
	andb	$1, %bl
	jmp	.LBB4_15
	.p2align	4, 0x90
.LBB4_12:                               #   in Loop: Header=BB4_11 Depth=1
	jne	.LBB4_16
# BB#13:                                #   in Loop: Header=BB4_11 Depth=1
	movq	(%r13), %rbx
	leal	-1(%r15), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	(%rcx,%rax,4), %edi
	callq	utop
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	pmul
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	movq	$-1, %rcx
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	pdivmod
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	psetq
.LBB4_15:                               # %.sink.split.us
                                        #   in Loop: Header=BB4_11 Depth=1
	movb	%bl, (%r12,%r15)
.LBB4_16:                               #   in Loop: Header=BB4_11 Depth=1
	incl	%r15d
	cmpl	4(%rsp), %r15d          # 4-byte Folded Reload
	jbe	.LBB4_11
	jmp	.LBB4_21
.LBB4_7:                                # %.lr.ph.split.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB4_8:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %ebp
	movzbl	(%r12,%rbp), %eax
	cmpb	$3, %al
	jb	.LBB4_17
# BB#9:                                 #   in Loop: Header=BB4_8 Depth=1
	movq	(%r13), %r14
	leal	-1(%rbp), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	(%rcx,%rax,4), %edi
	callq	utop
	movq	%rax, %rbx
	movzbl	(%r12,%rbp), %edi
	shrl	%edi
	callq	utop
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	ppowmod
	movq	%r14, %rdi
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%rax, %rsi
	callq	pmul
	xorl	%edx, %edx
	movq	$-1, %rcx
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	pdivmod
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	psetq
	movb	(%r12,%rbp), %bl
	andb	$1, %bl
	jmp	.LBB4_19
	.p2align	4, 0x90
.LBB4_17:                               #   in Loop: Header=BB4_8 Depth=1
	cmpb	$2, %al
	jne	.LBB4_20
# BB#18:                                #   in Loop: Header=BB4_8 Depth=1
	movq	(%r13), %rbx
	leal	-1(%rbp), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	(%rcx,%rax,4), %edi
	callq	utop
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	pmul
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	movq	$-1, %rcx
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	pdivmod
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	psetq
.LBB4_19:                               # %.sink.split
                                        #   in Loop: Header=BB4_8 Depth=1
	movb	%bl, (%r12,%rbp)
.LBB4_20:                               #   in Loop: Header=BB4_8 Depth=1
	incl	%ebp
	cmpl	4(%rsp), %ebp           # 4-byte Folded Reload
	jbe	.LBB4_8
.LBB4_21:                               # %._crit_edge
	testq	%r14, %r14
	je	.LBB4_23
# BB#22:
	decw	(%r14)
	je	.LBB4_24
.LBB4_23:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_24:
	xorl	%eax, %eax
	movq	%r14, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	pfree                   # TAILCALL
.Lfunc_end4:
	.size	combineSoln, .Lfunc_end4-combineSoln
	.cfi_endproc

	.globl	newSoln
	.p2align	4, 0x90
	.type	newSoln,@function
newSoln:                                # @newSoln
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 96
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rcx, %r14
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movl	$40, %edi
	callq	malloc
	movq	%rax, %rbp
	movq	%rbp, %r13
	testq	%rbp, %rbp
	je	.LBB5_14
# BB#1:
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%r12, %rbx
	movq	96(%rsp), %r12
	movq	%r14, (%r13)
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	pnew
	movq	%rax, 8(%r13)
	movq	%rbx, %rdi
	callq	pnew
	movq	%rax, 16(%r13)
	movq	pone(%rip), %rdi
	callq	pnew
	leaq	8(%rbp), %rdi
	addq	$24, %rbp
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	%rax, 24(%r13)
	testq	%r15, %r15
	movq	%r15, %rax
	je	.LBB5_3
# BB#2:
	incw	(%rax)
.LBB5_3:
	movq	$0, (%rsp)
	movq	%rbp, %rsi
	movq	%r12, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %r8d
	movq	%rax, %r15
	movq	%rax, %r9
	callq	combineSoln
	leal	1(%rbx), %r13d
	leaq	7(%r13), %r14
	shrq	$3, %r14
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB5_13
# BB#4:
	leaq	(%r12,%r13), %rbp
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	memset
	testb	$1, %r13b
	je	.LBB5_7
# BB#5:
	cmpb	$0, -1(%rbp)
	leaq	-1(%rbp), %rbp
	je	.LBB5_7
# BB#6:
	movq	%rbp, %rcx
	subq	%r12, %rcx
	movl	%ecx, %eax
	shrl	$3, %eax
	andb	$7, %cl
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edx
	movzbl	(%rbx,%rax), %ecx
	orl	%edx, %ecx
	movb	%cl, (%rbx,%rax)
.LBB5_7:                                # %.prol.loopexit
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB5_13
	.p2align	4, 0x90
.LBB5_8:                                # =>This Inner Loop Header: Depth=1
	cmpb	$0, -1(%rbp)
	je	.LBB5_10
# BB#9:                                 #   in Loop: Header=BB5_8 Depth=1
	leaq	-1(%rbp), %rcx
	subq	%r12, %rcx
	movl	%ecx, %eax
	shrl	$3, %eax
	andb	$7, %cl
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edx
	movzbl	(%rbx,%rax), %ecx
	orl	%edx, %ecx
	movb	%cl, (%rbx,%rax)
.LBB5_10:                               #   in Loop: Header=BB5_8 Depth=1
	cmpb	$0, -2(%rbp)
	leaq	-2(%rbp), %rbp
	je	.LBB5_12
# BB#11:                                #   in Loop: Header=BB5_8 Depth=1
	movq	%rbp, %rcx
	subq	%r12, %rcx
	movl	%ecx, %eax
	shrl	$3, %eax
	andb	$7, %cl
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edx
	movzbl	(%rbx,%rax), %ecx
	orl	%edx, %ecx
	movb	%cl, (%rbx,%rax)
.LBB5_12:                               #   in Loop: Header=BB5_8 Depth=1
	cmpq	%rbp, %r12
	jne	.LBB5_8
.LBB5_13:                               # %newBitVector.exit
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	%rbx, 32(%r13)
.LBB5_14:
	testq	%r15, %r15
	je	.LBB5_17
# BB#15:
	decw	(%r15)
	jne	.LBB5_17
# BB#16:
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	pfree
.LBB5_17:
	movq	%r13, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	newSoln, .Lfunc_end5-newSoln
	.cfi_endproc

	.globl	freeSoln
	.p2align	4, 0x90
	.type	freeSoln,@function
freeSoln:                               # @freeSoln
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 16
.Lcfi53:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB6_11
# BB#1:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_4
# BB#2:
	decw	(%rdi)
	jne	.LBB6_4
# BB#3:
	xorl	%eax, %eax
	callq	pfree
.LBB6_4:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_7
# BB#5:
	decw	(%rdi)
	jne	.LBB6_7
# BB#6:
	xorl	%eax, %eax
	callq	pfree
.LBB6_7:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_10
# BB#8:
	decw	(%rdi)
	jne	.LBB6_10
# BB#9:
	xorl	%eax, %eax
	callq	pfree
.LBB6_10:
	movq	32(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB6_11:
	popq	%rbx
	retq
.Lfunc_end6:
	.size	freeSoln, .Lfunc_end6-freeSoln
	.cfi_endproc

	.globl	freeSolns
	.p2align	4, 0x90
	.type	freeSolns,@function
freeSolns:                              # @freeSolns
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 32
.Lcfi57:
	.cfi_offset %rbx, -24
.Lcfi58:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	jne	.LBB7_2
	jmp	.LBB7_12
	.p2align	4, 0x90
.LBB7_11:                               # %freeSoln.exit
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	32(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	testq	%r14, %r14
	je	.LBB7_12
.LBB7_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rbx
	movq	(%rbx), %r14
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_5
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	decw	(%rdi)
	jne	.LBB7_5
# BB#4:                                 #   in Loop: Header=BB7_2 Depth=1
	xorl	%eax, %eax
	callq	pfree
.LBB7_5:                                #   in Loop: Header=BB7_2 Depth=1
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_8
# BB#6:                                 #   in Loop: Header=BB7_2 Depth=1
	decw	(%rdi)
	jne	.LBB7_8
# BB#7:                                 #   in Loop: Header=BB7_2 Depth=1
	xorl	%eax, %eax
	callq	pfree
.LBB7_8:                                #   in Loop: Header=BB7_2 Depth=1
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_11
# BB#9:                                 #   in Loop: Header=BB7_2 Depth=1
	decw	(%rdi)
	jne	.LBB7_11
# BB#10:                                #   in Loop: Header=BB7_2 Depth=1
	xorl	%eax, %eax
	callq	pfree
	jmp	.LBB7_11
.LBB7_12:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	freeSolns, .Lfunc_end7-freeSolns
	.cfi_endproc

	.globl	findSoln
	.p2align	4, 0x90
	.type	findSoln,@function
findSoln:                               # @findSoln
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi61:
	.cfi_def_cfa_offset 32
.Lcfi62:
	.cfi_offset %rbx, -24
.Lcfi63:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%r14, %r14
	je	.LBB8_2
# BB#1:
	incw	(%r14)
	testq	%rbx, %rbx
	jne	.LBB8_3
	jmp	.LBB8_5
	.p2align	4, 0x90
.LBB8_4:
	movq	(%rbx), %rbx
.LBB8_2:                                # %.preheader
	testq	%rbx, %rbx
	je	.LBB8_5
.LBB8_3:                                # %.lr.ph
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	callq	pcmp
	testl	%eax, %eax
	jne	.LBB8_4
	jmp	.LBB8_6
.LBB8_5:
	xorl	%ebx, %ebx
.LBB8_6:                                # %._crit_edge
	testq	%r14, %r14
	je	.LBB8_9
# BB#7:
	decw	(%r14)
	jne	.LBB8_9
# BB#8:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	pfree
.LBB8_9:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	findSoln, .Lfunc_end8-findSoln
	.cfi_endproc

	.globl	freeEas
	.p2align	4, 0x90
	.type	freeEas,@function
freeEas:                                # @freeEas
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi66:
	.cfi_def_cfa_offset 32
.Lcfi67:
	.cfi_offset %rbx, -24
.Lcfi68:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB9_8
# BB#1:                                 # %.preheader
	cmpq	$0, (%r14)
	je	.LBB9_7
# BB#2:                                 # %.lr.ph.preheader
	leaq	16(%r14), %rbx
	.p2align	4, 0x90
.LBB9_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_6
# BB#4:                                 #   in Loop: Header=BB9_3 Depth=1
	decw	(%rdi)
	jne	.LBB9_6
# BB#5:                                 #   in Loop: Header=BB9_3 Depth=1
	xorl	%eax, %eax
	callq	pfree
.LBB9_6:                                #   in Loop: Header=BB9_3 Depth=1
	cmpq	$0, (%rbx)
	leaq	16(%rbx), %rbx
	jne	.LBB9_3
.LBB9_7:                                # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.LBB9_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	freeEas, .Lfunc_end9-freeEas
	.cfi_endproc

	.globl	pomeranceLpow
	.p2align	4, 0x90
	.type	pomeranceLpow,@function
pomeranceLpow:                          # @pomeranceLpow
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi69:
	.cfi_def_cfa_offset 32
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	callq	log
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	log
	movapd	%xmm0, %xmm1
	mulsd	8(%rsp), %xmm1          # 8-byte Folded Reload
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB10_2
# BB#1:                                 # %call.sqrt
	movapd	%xmm1, %xmm0
	callq	sqrt
.LBB10_2:                               # %.split
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	addq	$24, %rsp
	jmp	exp                     # TAILCALL
.Lfunc_end10:
	.size	pomeranceLpow, .Lfunc_end10-pomeranceLpow
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI11_0:
	.quad	4607182418800017408     # double 1
.LCPI11_1:
	.quad	4611686018427387904     # double 2
.LCPI11_2:
	.quad	4618441417868443648     # double 6
	.text
	.globl	cfracA
	.p2align	4, 0x90
	.type	cfracA,@function
cfracA:                                 # @cfracA
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	cvtsi2sdq	%rax, %xmm1
	addsd	.LCPI11_0(%rip), %xmm1
	movsd	.LCPI11_1(%rip), %xmm0  # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	addsd	.LCPI11_2(%rip), %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB11_2
# BB#1:                                 # %call.sqrt
	pushq	%rax
.Lcfi70:
	.cfi_def_cfa_offset 16
	callq	sqrt
	movapd	%xmm0, %xmm1
	addq	$8, %rsp
.LBB11_2:                               # %.split
	movsd	.LCPI11_0(%rip), %xmm0  # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	retq
.Lfunc_end11:
	.size	cfracA, .Lfunc_end11-cfracA
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI12_0:
	.quad	4607182418800017408     # double 1
.LCPI12_1:
	.quad	4611686018427387904     # double 2
.LCPI12_2:
	.quad	4618441417868443648     # double 6
.LCPI12_3:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	pfactorbase
	.p2align	4, 0x90
	.type	pfactorbase,@function
pfactorbase:                            # @pfactorbase
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi74:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi75:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi77:
	.cfi_def_cfa_offset 80
.Lcfi78:
	.cfi_offset %rbx, -56
.Lcfi79:
	.cfi_offset %r12, -48
.Lcfi80:
	.cfi_offset %r13, -40
.Lcfi81:
	.cfi_offset %r14, -32
.Lcfi82:
	.cfi_offset %r15, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdi, %r15
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	(%rdx), %ebp
	movl	primesize(%rip), %eax
	decl	%eax
	movzwl	primes(%rax,%rax), %ebx
	testq	%r15, %r15
	je	.LBB12_2
# BB#1:
	incw	(%r15)
.LBB12_2:
	movl	%esi, %edi
	callq	utop
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	pmul
	movq	%rax, %rdi
	callq	pnew
	movq	%rax, %r13
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, (%rax)
	jne	.LBB12_11
# BB#3:
	movq	%r13, %rdi
	callq	ptod
	movapd	%xmm0, %xmm2
	movl	%r14d, %eax
	cvtsi2sdq	%rax, %xmm1
	addsd	.LCPI12_0(%rip), %xmm1
	movsd	.LCPI12_1(%rip), %xmm0  # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	addsd	.LCPI12_2(%rip), %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB12_5
# BB#4:                                 # %call.sqrt
	movsd	%xmm2, (%rsp)           # 8-byte Spill
	callq	sqrt
	movsd	(%rsp), %xmm2           # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB12_5:                               # %.split
	movsd	.LCPI12_0(%rip), %xmm0  # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movapd	%xmm2, %xmm0
	callq	log
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	callq	log
	movapd	%xmm0, %xmm1
	mulsd	16(%rsp), %xmm1         # 8-byte Folded Reload
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB12_7
# BB#6:                                 # %call.sqrt66
	movapd	%xmm1, %xmm0
	callq	sqrt
.LBB12_7:                               # %.split.split
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	callq	exp
	addsd	.LCPI12_3(%rip), %xmm0
	cvttsd2si	%xmm0, %rbx
	movl	$primes+2, %ecx
	movw	primes(%rip), %ax
	.p2align	4, 0x90
.LBB12_8:                               # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rbp
	movzwl	%ax, %eax
	cmpl	%ebx, %eax
	jae	.LBB12_10
# BB#9:                                 #   in Loop: Header=BB12_8 Depth=1
	movzwl	(%rbp), %eax
	leaq	2(%rbp), %rcx
	cmpl	$1, %eax
	jne	.LBB12_8
.LBB12_10:
	movl	$primes, %eax
	subq	%rax, %rbp
	shrq	%rbp
.LBB12_11:
	movl	%ebp, %edi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB12_20
# BB#12:
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movzwl	primes(%rip), %eax
	movq	%rbp, (%rsp)            # 8-byte Spill
	movl	%eax, (%rbp)
	movl	$1, %r14d
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$1, (%rax)
	je	.LBB12_19
# BB#13:                                # %.preheader.preheader
	movq	(%rsp), %r12            # 8-byte Reload
	addq	$4, %r12
	movl	$1, %r14d
	movw	primes+2(%rip), %ax
	movl	$primes+2, %r15d
	.p2align	4, 0x90
.LBB12_14:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzwl	%ax, %edi
	callq	utop
	movq	pone(%rip), %rsi
	movq	%rax, %rdi
	callq	psub
	movq	%rax, %rdi
	callq	phalf
	movq	%rax, %rbp
	movzwl	(%r15), %edi
	callq	utop
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	ppowmod
	movl	$1, %esi
	movq	%rax, %rdi
	callq	picmp
	testl	%eax, %eax
	jg	.LBB12_18
# BB#15:                                #   in Loop: Header=BB12_14 Depth=1
	movzwl	(%r15), %eax
	movl	%eax, (%r12)
	incl	%r14d
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	(%rcx), %r14d
	je	.LBB12_19
# BB#16:                                #   in Loop: Header=BB12_14 Depth=1
	cmpl	%ebx, %eax
	jae	.LBB12_19
# BB#17:                                #   in Loop: Header=BB12_14 Depth=1
	addq	$4, %r12
.LBB12_18:                              #   in Loop: Header=BB12_14 Depth=1
	movzwl	2(%r15), %eax
	addq	$2, %r15
	cmpl	$1, %eax
	jne	.LBB12_14
.LBB12_19:                              # %.loopexit
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%r14d, (%rax)
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
.LBB12_20:
	testq	%r13, %r13
	je	.LBB12_23
# BB#21:
	decw	(%r13)
	jne	.LBB12_23
# BB#22:
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	pfree
.LBB12_23:
	testq	%r15, %r15
	je	.LBB12_26
# BB#24:
	decw	(%r15)
	jne	.LBB12_26
# BB#25:
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	pfree
.LBB12_26:
	movq	%rbp, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	pfactorbase, .Lfunc_end12-pfactorbase
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI13_0:
	.quad	4607182418800017408     # double 1
.LCPI13_1:
	.quad	4618441417868443648     # double 6
.LCPI13_2:
	.quad	-4607182418800017408    # double -4
.LCPI13_3:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	getEas
	.p2align	4, 0x90
	.type	getEas,@function
getEas:                                 # @getEas
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi87:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi88:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi90:
	.cfi_def_cfa_offset 128
.Lcfi91:
	.cfi_offset %rbx, -56
.Lcfi92:
	.cfi_offset %r12, -48
.Lcfi93:
	.cfi_offset %r13, -40
.Lcfi94:
	.cfi_offset %r14, -32
.Lcfi95:
	.cfi_offset %r15, -24
.Lcfi96:
	.cfi_offset %rbp, -16
	movl	%r8d, %r12d
	movl	%ecx, %r15d
	movq	%rdx, %rbx
	movl	%esi, %r13d
	movq	%rdi, %rbp
	movl	%r12d, %eax
	cvtsi2sdq	%rax, %xmm0
	movsd	.LCPI13_0(%rip), %xmm2  # xmm2 = mem[0],zero
	addsd	%xmm2, %xmm0
	movapd	%xmm2, %xmm1
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	addsd	%xmm0, %xmm0
	addsd	.LCPI13_1(%rip), %xmm0
	sqrtsd	%xmm0, %xmm3
	ucomisd	%xmm3, %xmm3
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	jnp	.LBB13_2
# BB#1:                                 # %call.sqrt
	callq	sqrt
	movsd	.LCPI13_0(%rip), %xmm2  # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm3
.LBB13_2:                               # %.split
	movq	$0, 32(%rsp)
	testl	%r12d, %r12d
	je	.LBB13_3
# BB#4:
	movsd	%xmm3, (%rsp)           # 8-byte Spill
	movsd	%xmm2, 24(%rsp)         # 8-byte Spill
	leal	1(%r12), %edi
	shlq	$4, %rdi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB13_27
# BB#5:
	movsd	.LCPI13_0(%rip), %xmm0  # xmm0 = mem[0],zero
	divsd	(%rsp), %xmm0           # 8-byte Folded Reload
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movl	%r13d, %edi
	callq	utop
	testq	%rbp, %rbp
	je	.LBB13_7
# BB#6:
	incw	(%rbp)
.LBB13_7:                               # %.lr.ph95
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	(%rsp), %xmm0           # 8-byte Folded Reload
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	pmul
	movq	%rax, %rdi
	callq	ptod
	testl	%r15d, %r15d
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	xorpd	%xmm1, %xmm1
	je	.LBB13_18
# BB#8:                                 # %.lr.ph95.split.us.preheader
	movl	$1, %r13d
	movsd	.LCPI13_2(%rip), %xmm3  # xmm3 = mem[0],zero
	xorps	%xmm4, %xmm4
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movq	%r14, 48(%rsp)          # 8-byte Spill
.LBB13_9:                               # %.lr.ph95.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_12 Depth 2
	movsd	24(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	leal	-1(%r13), %ebp
	shlq	$4, %rbp
	addsd	8(%rsp), %xmm1          # 8-byte Folded Reload
	movsd	%xmm1, 40(%rsp)         # 8-byte Spill
	movapd	%xmm1, %xmm5
	mulsd	%xmm3, %xmm5
	mulsd	%xmm1, %xmm5
	movl	%r13d, %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	divsd	%xmm1, %xmm5
	addsd	%xmm2, %xmm5
	movsd	%xmm5, 24(%rsp)         # 8-byte Spill
	movups	%xmm4, (%r14,%rbp)
	callq	log
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	callq	log
	movapd	%xmm0, %xmm1
	mulsd	16(%rsp), %xmm1         # 8-byte Folded Reload
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB13_11
# BB#10:                                # %call.sqrt126
                                        #   in Loop: Header=BB13_9 Depth=1
	movapd	%xmm1, %xmm0
	callq	sqrt
.LBB13_11:                              # %.lr.ph95.split.us.split
                                        #   in Loop: Header=BB13_9 Depth=1
	leaq	(%r14,%rbp), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	8(%r14,%rbp), %rbp
	mulsd	40(%rsp), %xmm0         # 8-byte Folded Reload
	callq	exp
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	.LCPI13_3(%rip), %xmm0  # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	pow
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	.LCPI13_3(%rip), %xmm1
	cvttsd2si	%xmm1, %r14
	callq	dtop
	leaq	32(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB13_12:                              #   Parent Loop BB13_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ecx
	movl	(%rbx,%rcx,4), %edx
	cmpl	%r14d, %edx
	jae	.LBB13_15
# BB#13:                                #   in Loop: Header=BB13_12 Depth=2
	incl	%eax
	cmpl	%r15d, %eax
	jb	.LBB13_12
	jmp	.LBB13_14
	.p2align	4, 0x90
.LBB13_15:                              #   in Loop: Header=BB13_9 Depth=1
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	leaq	(%rbx,%rcx,4), %rbp
	cmpl	$2, verbose(%rip)
	jl	.LBB13_17
# BB#16:                                #   in Loop: Header=BB13_9 Depth=1
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	movl	%r14d, %ecx
	callq	printf
	movq	stdout(%rip), %rdi
	movq	32(%rsp), %rsi
	callq	fputp
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB13_17:                              #   in Loop: Header=BB13_9 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rbp, (%rax)
	movq	32(%rsp), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	psetq
	incl	%r13d
	cmpl	%r12d, %r13d
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	48(%rsp), %r14          # 8-byte Reload
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	40(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	.LCPI13_2(%rip), %xmm3  # xmm3 = mem[0],zero
	xorps	%xmm4, %xmm4
	jbe	.LBB13_9
	jmp	.LBB13_21
.LBB13_3:
	xorl	%r14d, %r14d
	jmp	.LBB13_27
.LBB13_14:
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	48(%rsp), %r14          # 8-byte Reload
	jmp	.LBB13_21
.LBB13_18:                              # %.lr.ph95.split
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm2
	movsd	.LCPI13_2(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	mulsd	%xmm2, %xmm1
	addsd	.LCPI13_0(%rip), %xmm1
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	xorpd	%xmm1, %xmm1
	movupd	%xmm1, (%r14)
	callq	log
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	callq	log
	movapd	%xmm0, %xmm1
	mulsd	40(%rsp), %xmm1         # 8-byte Folded Reload
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB13_20
# BB#19:                                # %call.sqrt127
	movapd	%xmm1, %xmm0
	callq	sqrt
.LBB13_20:                              # %.lr.ph95.split.split
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	callq	exp
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	.LCPI13_3(%rip), %xmm1
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	pow
	callq	dtop
	leaq	32(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
	movl	$1, %r13d
.LBB13_21:                              # %.loopexit
	decl	%r13d
	shlq	$4, %r13
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, (%r14,%r13)
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_24
# BB#22:
	decw	(%rdi)
	jne	.LBB13_24
# BB#23:
	xorl	%eax, %eax
	callq	pfree
.LBB13_24:
	testq	%rbp, %rbp
	je	.LBB13_27
# BB#25:
	decw	(%rbp)
	jne	.LBB13_27
# BB#26:
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	pfree
.LBB13_27:
	movq	%r14, %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	getEas, .Lfunc_end13-getEas
	.cfi_endproc

	.globl	pfactorQ
	.p2align	4, 0x90
	.type	pfactorQ,@function
pfactorQ:                               # @pfactorQ
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi100:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi101:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi103:
	.cfi_def_cfa_offset 96
.Lcfi104:
	.cfi_offset %rbx, -56
.Lcfi105:
	.cfi_offset %r12, -48
.Lcfi106:
	.cfi_offset %r13, -40
.Lcfi107:
	.cfi_offset %r14, -32
.Lcfi108:
	.cfi_offset %r15, -24
.Lcfi109:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%rcx, %rbx
	movq	%rdx, %rbp
	movq	%rsi, %r14
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	%r14, 8(%rsp)
	movq	$0, 16(%rsp)
	leal	-1(%r8), %r15d
	movl	(%rbp,%r15,4), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	testq	%r14, %r14
	je	.LBB14_2
# BB#1:
	incw	(%r14)
.LBB14_2:
	testq	%r12, %r12
	je	.LBB14_3
# BB#4:
	movq	(%r12), %r13
	movq	8(%r12), %rsi
	leaq	16(%rsp), %rdi
	movq	%r8, %r14
	callq	psetq
	movq	%r14, %r8
	movq	8(%rsp), %r14
	jmp	.LBB14_5
.LBB14_3:
	xorl	%r13d, %r13d
.LBB14_5:
	movl	%r8d, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	memset
	movq	%r14, %rdi
	callq	podd
	testl	%eax, %eax
	jne	.LBB14_9
# BB#6:                                 # %.lr.ph37.preheader
	movq	%r14, %rdi
	callq	phalf
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
	incb	(%rbx)
	movq	8(%rsp), %rdi
	callq	podd
	testl	%eax, %eax
	jne	.LBB14_9
# BB#7:                                 # %.lr.ph37..lr.ph37_crit_edge.preheader
	leaq	8(%rsp), %r14
	.p2align	4, 0x90
.LBB14_8:                               # %.lr.ph37..lr.ph37_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rdi
	callq	phalf
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	psetq
	incb	(%rbx)
	movq	8(%rsp), %rdi
	callq	podd
	testl	%eax, %eax
	je	.LBB14_8
.LBB14_9:                               # %.preheader34.preheader
	leaq	8(%rsp), %r14
	.p2align	4, 0x90
.LBB14_10:                              # %.preheader34
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_16 Depth 2
	addq	$4, %rbp
	cmpq	%r13, %rbp
	jne	.LBB14_14
# BB#11:                                #   in Loop: Header=BB14_10 Depth=1
	movq	8(%rsp), %rdi
	movq	16(%rsp), %rsi
	callq	pcmp
	testl	%eax, %eax
	jg	.LBB14_12
# BB#13:                                #   in Loop: Header=BB14_10 Depth=1
	movq	16(%r12), %r13
	movq	24(%r12), %rsi
	leaq	16(%r12), %r12
	leaq	16(%rsp), %rdi
	callq	psetq
.LBB14_14:                              # %.preheader
                                        #   in Loop: Header=BB14_10 Depth=1
	incq	%rbx
	jmp	.LBB14_16
	.p2align	4, 0x90
.LBB14_15:                              # %.lr.ph
                                        #   in Loop: Header=BB14_16 Depth=2
	movq	8(%rsp), %rdi
	movl	(%rbp), %esi
	callq	pidiv
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	psetq
	incb	(%rbx)
.LBB14_16:                              # %.lr.ph
                                        #   Parent Loop BB14_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rsp), %rdi
	movl	(%rbp), %esi
	callq	pimod
	testl	%eax, %eax
	je	.LBB14_15
# BB#17:                                # %._crit_edge
                                        #   in Loop: Header=BB14_10 Depth=1
	decl	%r15d
	jne	.LBB14_10
# BB#18:
	movq	8(%rsp), %rdi
	movl	$1, %ebx
	movl	$1, %esi
	callq	picmp
	testl	%eax, %eax
	je	.LBB14_20
# BB#19:
	movq	8(%rsp), %rdi
	movl	(%rbp), %esi
	callq	pidiv
	movq	%rax, %rdi
	movl	28(%rsp), %esi          # 4-byte Reload
	callq	picmp
	xorl	%ecx, %ecx
	testl	%eax, %eax
	movl	$-1, %ebx
	cmovlel	%ecx, %ebx
	jmp	.LBB14_20
.LBB14_12:
	movl	$-2, %ebx
.LBB14_20:                              # %.loopexit
	movq	8(%rsp), %rsi
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	psetq
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_23
# BB#21:
	decw	(%rdi)
	jne	.LBB14_23
# BB#22:
	xorl	%eax, %eax
	callq	pfree
.LBB14_23:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_26
# BB#24:
	decw	(%rdi)
	jne	.LBB14_26
# BB#25:
	xorl	%eax, %eax
	callq	pfree
.LBB14_26:
	movl	%ebx, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	pfactorQ, .Lfunc_end14-pfactorQ
	.cfi_endproc

	.globl	pcfrac
	.p2align	4, 0x90
	.type	pcfrac,@function
pcfrac:                                 # @pcfrac
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi113:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi114:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi116:
	.cfi_def_cfa_offset 256
.Lcfi117:
	.cfi_offset %rbx, -56
.Lcfi118:
	.cfi_offset %r12, -48
.Lcfi119:
	.cfi_offset %r13, -40
.Lcfi120:
	.cfi_offset %r14, -32
.Lcfi121:
	.cfi_offset %r15, -24
.Lcfi122:
	.cfi_offset %rbp, -16
	movq	%rsi, 176(%rsp)         # 8-byte Spill
	movq	%rdi, %r15
	movl	pcfrac_k(%rip), %r12d
	movl	pcfrac_m(%rip), %eax
	movl	%eax, 124(%rsp)
	movl	pcfrac_aborts(%rip), %r14d
	movq	$0, (%rsp)
	movq	$0, 112(%rsp)
	movq	$0, 104(%rsp)
	movq	$0, 64(%rsp)
	movq	$0, 160(%rsp)
	movq	$0, 48(%rsp)
	movq	$0, 152(%rsp)
	movq	$0, 80(%rsp)
	movq	$0, 144(%rsp)
	movq	$0, 8(%rsp)
	movq	$0, 56(%rsp)
	movq	$0, 96(%rsp)
	movq	$0, 136(%rsp)
	testq	%r15, %r15
	je	.LBB15_2
# BB#1:
	incw	(%r15)
.LBB15_2:
	movq	%r15, %rdi
	callq	pnew
	movq	%rax, 128(%rsp)
	leaq	124(%rsp), %rdx
	movq	%r15, %rdi
	movl	%r12d, %esi
	movl	%r14d, %ecx
	callq	pfactorbase
	movq	%rax, %r13
	movl	124(%rsp), %ebp
	leal	16(,%rbp,8), %ebx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 88(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%r15, 40(%rsp)          # 8-byte Spill
	je	.LBB15_4
# BB#3:
	leal	1(%rbp), %edi
	callq	malloc
	movq	%rax, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB15_4
# BB#5:
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rdx
	callq	memset
	movq	176(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	je	.LBB15_7
# BB#6:
	movl	(%rax), %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
.LBB15_7:
	movl	$0, cfracNabort(%rip)
	movl	$0, cfracFsolns(%rip)
	movl	$0, cfracT2solns(%rip)
	movl	$0, cfracPsolns(%rip)
	movl	$0, cfracTsolns(%rip)
	movq	%r15, %rdi
	movl	%r12d, %esi
	movq	%r13, %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r14d, %r8d
	callq	getEas
	movq	%rax, 168(%rsp)         # 8-byte Spill
	cmpl	$2, verbose(%rip)
	jl	.LBB15_12
# BB#8:
	movq	stdout(%rip), %rdi
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	24(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %edx
	callq	fprintf
	testl	%ebx, %ebx
	movq	stdout(%rip), %rcx
	je	.LBB15_11
# BB#9:                                 # %.lr.ph
	xorl	%ebx, %ebx
	movq	24(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB15_10:                              # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rbx,4), %edx
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	fprintf
	incq	%rbx
	movq	stdout(%rip), %rcx
	cmpq	%rbp, %rbx
	jb	.LBB15_10
.LBB15_11:                              # %._crit_edge
	movl	$10, %edi
	movq	%rcx, %rsi
	callq	_IO_putc
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB15_12:
	movl	%r12d, %edi
	callq	utop
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	pmul
	movq	%rsp, %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	(%rsp), %rdi
	callq	psqrt
	leaq	112(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	112(%rsp), %rdi
	movq	%rdi, %rsi
	callq	padd
	leaq	104(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	104(%rsp), %rsi
	leaq	64(%rsp), %rdi
	callq	psetq
	movq	104(%rsp), %rsi
	leaq	160(%rsp), %rdi
	callq	psetq
	movq	pone(%rip), %rsi
	leaq	48(%rsp), %rdi
	callq	psetq
	movq	(%rsp), %rbx
	movq	112(%rsp), %rdi
	movq	%rdi, %rsi
	callq	pmul
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	psub
	leaq	152(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	pone(%rip), %rsi
	leaq	80(%rsp), %rdi
	callq	psetq
	movq	112(%rsp), %rsi
	leaq	144(%rsp), %rdi
	callq	psetq
	movq	pzero(%rip), %rsi
	leaq	96(%rsp), %rdi
	callq	psetq
	movq	16(%rsp), %rax          # 8-byte Reload
	incq	%rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	%r13, 32(%rsp)          # 8-byte Spill
	jmp	.LBB15_13
.LBB15_4:
	movl	$1, %edi
	movl	$.L.str.5, %esi
	movl	$.L.str.6, %edx
	callq	errorp
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	xorl	%r14d, %r14d
	jmp	.LBB15_68
.LBB15_58:                              #   in Loop: Header=BB15_13 Depth=1
	incl	cfracFsolns(%rip)
	movq	(%rsp), %rsi
	leaq	56(%rsp), %rdi
	callq	psetq
	movl	verbose(%rip), %eax
	testl	%eax, %eax
	movq	32(%rsp), %r13          # 8-byte Reload
	je	.LBB15_64
# BB#59:                                #   in Loop: Header=BB15_13 Depth=1
	cmpl	$2, %eax
	je	.LBB15_62
# BB#60:                                #   in Loop: Header=BB15_13 Depth=1
	cmpl	$1, %eax
	jne	.LBB15_63
# BB#61:                                #   in Loop: Header=BB15_13 Depth=1
	movq	stderr(%rip), %rsi
	movl	$47, %edi
	callq	_IO_putc
	jmp	.LBB15_64
.LBB15_62:                              #   in Loop: Header=BB15_13 Depth=1
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	jmp	.LBB15_64
.LBB15_63:                              #   in Loop: Header=BB15_13 Depth=1
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rdi
	movq	8(%rsp), %r9
	movl	$.L.str.17, %esi
	movl	$.L.str.11, %edx
	movq	%r13, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi123:
	.cfi_adjust_cfa_offset 8
	pushq	8(%rsp)
.Lcfi124:
	.cfi_adjust_cfa_offset 8
	callq	printSoln
	addq	$16, %rsp
.Lcfi125:
	.cfi_adjust_cfa_offset -16
	movq	stdout(%rip), %rcx
	movl	$.L.str.18, %edi
	movl	$9, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rdi
	movq	8(%rsp), %rsi
	callq	fputp
	movq	stdout(%rip), %rcx
	movl	$.L.str.19, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rdi
	movq	56(%rsp), %rsi
	callq	fputp
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB15_64:                              #   in Loop: Header=BB15_13 Depth=1
	incl	%r12d
	movq	8(%rsp), %rdi
	movq	56(%rsp), %rsi
	callq	pcmp
	testl	%eax, %eax
	je	.LBB15_13
# BB#65:                                #   in Loop: Header=BB15_13 Depth=1
	movq	8(%rsp), %rdi
	movq	56(%rsp), %rsi
	callq	padd
	movq	%rax, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	pcmp
	testl	%eax, %eax
	je	.LBB15_13
	jmp	.LBB15_66
.LBB15_36:                              #   in Loop: Header=BB15_13 Depth=1
	incl	cfracTsolns(%rip)
	cmpl	$2, verbose(%rip)
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	24(%rsp), %r12          # 8-byte Reload
	jl	.LBB15_39
# BB#37:                                #   in Loop: Header=BB15_13 Depth=1
	movq	stderr(%rip), %rsi
	movl	$46, %edi
	callq	_IO_putc
	cmpl	$4, verbose(%rip)
	jl	.LBB15_39
# BB#38:                                #   in Loop: Header=BB15_13 Depth=1
	movq	stdout(%rip), %rdi
	movq	8(%rsp), %r9
	movl	$.L.str.10, %esi
	movl	$.L.str.11, %edx
	movq	%rbx, %rcx
	movl	%r12d, %r8d
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi126:
	.cfi_adjust_cfa_offset 8
	pushq	8(%rsp)
.Lcfi127:
	.cfi_adjust_cfa_offset 8
	callq	printSoln
	addq	$16, %rsp
.Lcfi128:
	.cfi_adjust_cfa_offset -16
.LBB15_39:                              # %.thread
                                        #   in Loop: Header=BB15_13 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movq	8(%rsp), %r8
	movq	(%rsp), %r9
	subq	$8, %rsp
.Lcfi129:
	.cfi_adjust_cfa_offset 8
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	movl	%r12d, %edx
	movq	%rax, %r12
	movq	%r14, %rcx
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi130:
	.cfi_adjust_cfa_offset 8
	callq	newSoln
	addq	$16, %rsp
.Lcfi131:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %r14
	.p2align	4, 0x90
.LBB15_13:                              # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_14 Depth 2
                                        #       Child Loop BB15_15 Depth 3
                                        #       Child Loop BB15_29 Depth 3
                                        #       Child Loop BB15_49 Depth 3
	movl	%r12d, %r13d
	movq	168(%rsp), %r12         # 8-byte Reload
	jmp	.LBB15_14
.LBB15_51:                              #   in Loop: Header=BB15_14 Depth=2
	movq	168(%rsp), %r12         # 8-byte Reload
	movl	verbose(%rip), %eax
	cmpl	$4, %eax
	movq	24(%rsp), %rbp          # 8-byte Reload
	jl	.LBB15_53
# BB#52:                                #   in Loop: Header=BB15_14 Depth=2
	movq	stdout(%rip), %rdi
	movq	8(%rsp), %r9
	movl	$.L.str.16, %esi
	movl	$.L.str.14, %edx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%ebp, %r8d
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi132:
	.cfi_adjust_cfa_offset 8
	pushq	8(%rsp)
.Lcfi133:
	.cfi_adjust_cfa_offset 8
	callq	printSoln
	addq	$16, %rsp
.Lcfi134:
	.cfi_adjust_cfa_offset -16
	movl	verbose(%rip), %eax
.LBB15_53:                              #   in Loop: Header=BB15_14 Depth=2
	cmpl	$3, %eax
	jl	.LBB15_55
# BB#54:                                #   in Loop: Header=BB15_14 Depth=2
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
.LBB15_55:                              #   in Loop: Header=BB15_14 Depth=2
	movq	8(%rsp), %r8
	movq	(%rsp), %r9
	subq	$8, %rsp
.Lcfi135:
	.cfi_adjust_cfa_offset 8
	movl	$0, %ecx
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	%ebp, %edx
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi136:
	.cfi_adjust_cfa_offset 8
	callq	newSoln
	addq	$16, %rsp
.Lcfi137:
	.cfi_adjust_cfa_offset -16
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx,%rbx,8)
	.p2align	4, 0x90
.LBB15_14:                              #   Parent Loop BB15_13 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB15_15 Depth 3
                                        #       Child Loop BB15_29 Depth 3
                                        #       Child Loop BB15_49 Depth 3
	decl	%r13d
	movl	%r13d, %eax
	movl	%r15d, %r13d
	.p2align	4, 0x90
.LBB15_15:                              #   Parent Loop BB15_13 Depth=1
                                        #     Parent Loop BB15_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	testl	%eax, %eax
	je	.LBB15_16
# BB#17:                                #   in Loop: Header=BB15_15 Depth=3
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movq	80(%rsp), %rsi
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	callq	psetq
	movq	96(%rsp), %rdi
	movq	80(%rsp), %rsi
	callq	pmul
	movq	144(%rsp), %rsi
	movq	%rax, %rdi
	callq	padd
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	80(%rsp), %rcx
	callq	pdivmod
	movq	(%rsp), %rsi
	leaq	144(%rsp), %rdi
	callq	psetq
	movq	48(%rsp), %rsi
	movq	%rbx, %rdi
	callq	psetq
	movq	96(%rsp), %rbx
	movq	160(%rsp), %rdi
	movq	64(%rsp), %rsi
	callq	psub
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	pmul
	movq	152(%rsp), %rsi
	movq	%rax, %rdi
	callq	padd
	leaq	48(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	(%rsp), %rsi
	leaq	152(%rsp), %rdi
	callq	psetq
	movq	64(%rsp), %rsi
	leaq	160(%rsp), %rdi
	callq	psetq
	movq	pone(%rip), %rsi
	leaq	96(%rsp), %rbx
	movq	%rbx, %rdi
	callq	psetq
	movq	64(%rsp), %rdi
	movq	48(%rsp), %rsi
	callq	psub
	leaq	136(%rsp), %rbp
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	136(%rsp), %rdi
	movq	48(%rsp), %rsi
	callq	pcmp
	testl	%eax, %eax
	js	.LBB15_19
# BB#18:                                #   in Loop: Header=BB15_15 Depth=3
	movq	64(%rsp), %rdi
	movq	48(%rsp), %rsi
	movq	%rbx, %rdx
	movq	%rbp, %rcx
	callq	pdivmod
.LBB15_19:                              #   in Loop: Header=BB15_15 Depth=3
	movq	104(%rsp), %rdi
	movq	136(%rsp), %rsi
	callq	psub
	leaq	64(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
	movl	$1, %r15d
	subl	%r13d, %r15d
	movq	16(%rsp), %rax          # 8-byte Reload
	movb	%r15b, (%rax)
	movq	48(%rsp), %rsi
	movq	%rsp, %rdi
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdx
	movq	192(%rsp), %rcx         # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	%r12, %r9
	callq	pfactorQ
	movl	%eax, %ebx
	cmpl	$-2, %ebx
	jg	.LBB15_21
# BB#20:                                #   in Loop: Header=BB15_15 Depth=3
	incl	cfracNabort(%rip)
.LBB15_21:                              #   in Loop: Header=BB15_15 Depth=3
	movq	48(%rsp), %rdi
	movl	$1, %esi
	callq	picmp
	testl	%eax, %eax
	movq	184(%rsp), %rbp         # 8-byte Reload
	je	.LBB15_22
# BB#23:                                #   in Loop: Header=BB15_15 Depth=3
	leal	-1(%rbp), %eax
	testl	%ebx, %ebx
	movl	%r15d, %r13d
	js	.LBB15_15
# BB#24:                                #   in Loop: Header=BB15_14 Depth=2
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	80(%rsp), %rsi
	leaq	8(%rsp), %r12
	movq	%r12, %rdi
	callq	psetq
	testl	%ebx, %ebx
	je	.LBB15_25
# BB#45:                                #   in Loop: Header=BB15_14 Depth=2
	subq	$8, %rsp
.Lcfi138:
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdi
	leaq	8(%rsp), %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rcx
	movq	32(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %r8d
	movq	48(%rsp), %r9           # 8-byte Reload
	pushq	$0
.Lcfi139:
	.cfi_adjust_cfa_offset 8
	callq	combineSoln
	addq	$16, %rsp
.Lcfi140:
	.cfi_adjust_cfa_offset -16
	incl	cfracPsolns(%rip)
	cmpl	$0, verbose(%rip)
	movq	72(%rsp), %r12          # 8-byte Reload
	je	.LBB15_48
# BB#46:                                #   in Loop: Header=BB15_14 Depth=2
	movq	stderr(%rip), %rsi
	movl	$42, %edi
	callq	_IO_putc
	cmpl	$3, verbose(%rip)
	jl	.LBB15_48
# BB#47:                                #   in Loop: Header=BB15_14 Depth=2
	movq	stdout(%rip), %rdi
	movq	8(%rsp), %r9
	movl	$.L.str.15, %esi
	movl	$.L.str.14, %edx
	movq	%rbx, %rcx
	movl	%ebp, %r8d
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi141:
	.cfi_adjust_cfa_offset 8
	pushq	8(%rsp)
.Lcfi142:
	.cfi_adjust_cfa_offset 8
	callq	printSoln
	addq	$16, %rsp
.Lcfi143:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB15_48
.LBB15_25:                              #   in Loop: Header=BB15_14 Depth=2
	movq	(%rsp), %r13
	testq	%r13, %r13
	je	.LBB15_27
# BB#26:                                #   in Loop: Header=BB15_14 Depth=2
	incw	(%r13)
.LBB15_27:                              # %.preheader.i
                                        #   in Loop: Header=BB15_14 Depth=2
	testq	%r14, %r14
	movq	72(%rsp), %r12          # 8-byte Reload
	je	.LBB15_31
# BB#28:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB15_14 Depth=2
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB15_29:                              # %.lr.ph.i
                                        #   Parent Loop BB15_13 Depth=1
                                        #     Parent Loop BB15_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbx), %rdi
	movq	%r13, %rsi
	callq	pcmp
	testl	%eax, %eax
	je	.LBB15_32
# BB#30:                                #   in Loop: Header=BB15_29 Depth=3
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB15_29
.LBB15_31:                              #   in Loop: Header=BB15_14 Depth=2
	xorl	%ebx, %ebx
.LBB15_32:                              # %._crit_edge.i
                                        #   in Loop: Header=BB15_14 Depth=2
	testq	%r13, %r13
	je	.LBB15_35
# BB#33:                                #   in Loop: Header=BB15_14 Depth=2
	decw	(%r13)
	jne	.LBB15_35
# BB#34:                                #   in Loop: Header=BB15_14 Depth=2
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	pfree
.LBB15_35:                              # %findSoln.exit
                                        #   in Loop: Header=BB15_14 Depth=2
	testq	%rbx, %rbx
	je	.LBB15_36
# BB#40:                                #   in Loop: Header=BB15_14 Depth=2
	cmpl	$4, verbose(%rip)
	movq	32(%rsp), %rbp          # 8-byte Reload
	jl	.LBB15_42
# BB#41:                                #   in Loop: Header=BB15_14 Depth=2
	movq	stdout(%rip), %rdi
	movq	8(%rsp), %r9
	movl	$.L.str.10, %esi
	movl	$.L.str.12, %edx
	movq	%rbp, %rcx
	movq	24(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi144:
	.cfi_adjust_cfa_offset 8
	pushq	8(%rsp)
.Lcfi145:
	.cfi_adjust_cfa_offset 8
	callq	printSoln
	addq	$16, %rsp
.Lcfi146:
	.cfi_adjust_cfa_offset -16
.LBB15_42:                              #   in Loop: Header=BB15_14 Depth=2
	movq	pone(%rip), %rsi
	movq	%rsp, %r13
	movq	%r13, %rdi
	callq	psetq
	subq	$8, %rsp
.Lcfi147:
	.cfi_adjust_cfa_offset 8
	leaq	16(%rsp), %rdi
	movq	%r13, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rbp, %r13
	movq	%rbp, %rcx
	movq	32(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %r8d
	movq	48(%rsp), %r9           # 8-byte Reload
	pushq	%rbx
.Lcfi148:
	.cfi_adjust_cfa_offset 8
	callq	combineSoln
	addq	$16, %rsp
.Lcfi149:
	.cfi_adjust_cfa_offset -16
	incl	cfracT2solns(%rip)
	cmpl	$0, verbose(%rip)
	je	.LBB15_48
# BB#43:                                #   in Loop: Header=BB15_14 Depth=2
	movq	stderr(%rip), %rsi
	movl	$35, %edi
	callq	_IO_putc
	cmpl	$3, verbose(%rip)
	jl	.LBB15_48
# BB#44:                                #   in Loop: Header=BB15_14 Depth=2
	movq	stdout(%rip), %rdi
	movq	8(%rsp), %r9
	movl	$.L.str.13, %esi
	movl	$.L.str.14, %edx
	movq	%r13, %rcx
	movl	%ebp, %r8d
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi150:
	.cfi_adjust_cfa_offset 8
	pushq	8(%rsp)
.Lcfi151:
	.cfi_adjust_cfa_offset 8
	callq	printSoln
	addq	$16, %rsp
.Lcfi152:
	.cfi_adjust_cfa_offset -16
.LBB15_48:                              # %.thread139.preheader
                                        #   in Loop: Header=BB15_14 Depth=2
	leal	1(%r12), %r13d
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB15_49:                              # %.thread139
                                        #   Parent Loop BB15_13 Depth=1
                                        #     Parent Loop BB15_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpb	$0, (%rax,%rbx)
	je	.LBB15_57
# BB#50:                                #   in Loop: Header=BB15_49 Depth=3
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB15_51
# BB#56:                                #   in Loop: Header=BB15_49 Depth=3
	subq	$8, %rsp
.Lcfi153:
	.cfi_adjust_cfa_offset 8
	leaq	16(%rsp), %rdi
	leaq	8(%rsp), %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	48(%rsp), %r9           # 8-byte Reload
	pushq	%rax
.Lcfi154:
	.cfi_adjust_cfa_offset 8
	callq	combineSoln
	addq	$16, %rsp
.Lcfi155:
	.cfi_adjust_cfa_offset -16
.LBB15_57:                              #   in Loop: Header=BB15_49 Depth=3
	decq	%rbx
	cmpl	$-1, %ebx
	jne	.LBB15_49
	jmp	.LBB15_58
.LBB15_16:
	xorl	%ebp, %ebp
	movq	32(%rsp), %r13          # 8-byte Reload
	jmp	.LBB15_68
.LBB15_22:
	movl	$4, %edi
	movl	$.L.str.5, %esi
	movl	$.L.str.9, %edx
	callq	errorp
.LBB15_68:                              # %.loopexit143
	movq	%r12, %r15
	movq	176(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	je	.LBB15_70
# BB#69:
	movl	%ebp, (%rax)
.LBB15_70:
	cmpq	$0, 88(%rsp)            # 8-byte Folded Reload
	movq	24(%rsp), %r12          # 8-byte Reload
	je	.LBB15_84
# BB#71:                                # %.preheader.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB15_72:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %eax
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rbx
	testq	%rbx, %rbx
	je	.LBB15_83
# BB#73:                                #   in Loop: Header=BB15_72 Depth=1
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_76
# BB#74:                                #   in Loop: Header=BB15_72 Depth=1
	decw	(%rdi)
	jne	.LBB15_76
# BB#75:                                #   in Loop: Header=BB15_72 Depth=1
	xorl	%eax, %eax
	callq	pfree
.LBB15_76:                              #   in Loop: Header=BB15_72 Depth=1
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_79
# BB#77:                                #   in Loop: Header=BB15_72 Depth=1
	decw	(%rdi)
	jne	.LBB15_79
# BB#78:                                #   in Loop: Header=BB15_72 Depth=1
	xorl	%eax, %eax
	callq	pfree
.LBB15_79:                              #   in Loop: Header=BB15_72 Depth=1
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_82
# BB#80:                                #   in Loop: Header=BB15_72 Depth=1
	decw	(%rdi)
	jne	.LBB15_82
# BB#81:                                #   in Loop: Header=BB15_72 Depth=1
	xorl	%eax, %eax
	callq	pfree
.LBB15_82:                              #   in Loop: Header=BB15_72 Depth=1
	movq	32(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB15_83:                              # %freeSoln.exit
                                        #   in Loop: Header=BB15_72 Depth=1
	incl	%ebp
	cmpl	%r12d, %ebp
	jbe	.LBB15_72
.LBB15_84:                              # %.loopexit
	movq	%r15, %rbp
	testq	%rbp, %rbp
	movq	40(%rsp), %r15          # 8-byte Reload
	je	.LBB15_93
# BB#85:                                # %.preheader.i133
	cmpq	$0, (%rbp)
	je	.LBB15_91
# BB#86:                                # %.lr.ph.i134.preheader
	leaq	16(%rbp), %rbx
	.p2align	4, 0x90
.LBB15_87:                              # %.lr.ph.i134
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_90
# BB#88:                                #   in Loop: Header=BB15_87 Depth=1
	decw	(%rdi)
	jne	.LBB15_90
# BB#89:                                #   in Loop: Header=BB15_87 Depth=1
	xorl	%eax, %eax
	callq	pfree
.LBB15_90:                              #   in Loop: Header=BB15_87 Depth=1
	cmpq	$0, (%rbx)
	leaq	16(%rbx), %rbx
	jne	.LBB15_87
.LBB15_91:                              # %._crit_edge.i135
	movq	%rbp, %rdi
	jmp	.LBB15_92
.LBB15_66:
	movq	8(%rsp), %rdi
	movq	56(%rsp), %rsi
	callq	padd
	movq	%rax, %rdi
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rsi
	callq	pgcd
	leaq	128(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	128(%rsp), %rdi
	movq	pone(%rip), %rsi
	callq	pcmp
	testl	%eax, %eax
	movq	184(%rsp), %rbp         # 8-byte Reload
	movq	168(%rsp), %r12         # 8-byte Reload
	je	.LBB15_147
# BB#67:
	movq	128(%rsp), %rdi
	movq	%r15, %rsi
	callq	pcmp
	testl	%eax, %eax
	jne	.LBB15_68
.LBB15_147:
	movq	stdout(%rip), %rcx
	movl	$.L.str.20, %edi
	movl	$29, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rcx
	movl	$.L.str.21, %edi
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rdi
	movq	8(%rsp), %rsi
	callq	fputp
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rdi
	movq	56(%rsp), %rsi
	callq	fputp
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rdi
	callq	fflush
	callq	abort
.LBB15_92:                              # %freeEas.exit
	callq	free
.LBB15_93:                              # %freeEas.exit
	testq	%r14, %r14
	je	.LBB15_104
# BB#94:                                # %.lr.ph.i136
	movq	%r14, %rbx
	movq	(%rbx), %r14
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_97
# BB#95:
	decw	(%rdi)
	jne	.LBB15_97
# BB#96:
	xorl	%eax, %eax
	callq	pfree
.LBB15_97:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_100
# BB#98:
	decw	(%rdi)
	jne	.LBB15_100
# BB#99:
	xorl	%eax, %eax
	callq	pfree
.LBB15_100:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_103
# BB#101:
	decw	(%rdi)
	jne	.LBB15_103
# BB#102:
	xorl	%eax, %eax
	callq	pfree
.LBB15_103:                             # %freeSoln.exit.i
	movq	32(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	jmp	.LBB15_92
.LBB15_104:                             # %freeSolns.exit
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%r13, %rdi
	callq	free
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_107
# BB#105:
	decw	(%rdi)
	jne	.LBB15_107
# BB#106:
	xorl	%eax, %eax
	callq	pfree
.LBB15_107:
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_110
# BB#108:
	decw	(%rdi)
	jne	.LBB15_110
# BB#109:
	xorl	%eax, %eax
	callq	pfree
.LBB15_110:
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_113
# BB#111:
	decw	(%rdi)
	jne	.LBB15_113
# BB#112:
	xorl	%eax, %eax
	callq	pfree
.LBB15_113:
	movq	160(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_116
# BB#114:
	decw	(%rdi)
	jne	.LBB15_116
# BB#115:
	xorl	%eax, %eax
	callq	pfree
.LBB15_116:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_119
# BB#117:
	decw	(%rdi)
	jne	.LBB15_119
# BB#118:
	xorl	%eax, %eax
	callq	pfree
.LBB15_119:
	movq	152(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_122
# BB#120:
	decw	(%rdi)
	jne	.LBB15_122
# BB#121:
	xorl	%eax, %eax
	callq	pfree
.LBB15_122:
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_125
# BB#123:
	decw	(%rdi)
	jne	.LBB15_125
# BB#124:
	xorl	%eax, %eax
	callq	pfree
.LBB15_125:
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_128
# BB#126:
	decw	(%rdi)
	jne	.LBB15_128
# BB#127:
	xorl	%eax, %eax
	callq	pfree
.LBB15_128:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_131
# BB#129:
	decw	(%rdi)
	jne	.LBB15_131
# BB#130:
	xorl	%eax, %eax
	callq	pfree
.LBB15_131:
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_134
# BB#132:
	decw	(%rdi)
	jne	.LBB15_134
# BB#133:
	xorl	%eax, %eax
	callq	pfree
.LBB15_134:
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_137
# BB#135:
	decw	(%rdi)
	jne	.LBB15_137
# BB#136:
	xorl	%eax, %eax
	callq	pfree
.LBB15_137:
	movq	136(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_140
# BB#138:
	decw	(%rdi)
	jne	.LBB15_140
# BB#139:
	xorl	%eax, %eax
	callq	pfree
.LBB15_140:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_143
# BB#141:
	decw	(%rdi)
	jne	.LBB15_143
# BB#142:
	xorl	%eax, %eax
	callq	pfree
.LBB15_143:
	testq	%r15, %r15
	je	.LBB15_146
# BB#144:
	decw	(%r15)
	jne	.LBB15_146
# BB#145:
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	pfree
.LBB15_146:
	movq	128(%rsp), %rdi
	callq	presult
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	pcfrac, .Lfunc_end15-pcfrac
	.cfi_endproc

	.globl	pcfracInit
	.p2align	4, 0x90
	.type	pcfracInit,@function
pcfracInit:                             # @pcfracInit
	.cfi_startproc
# BB#0:
	movl	%edi, pcfrac_m(%rip)
	movl	%esi, pcfrac_k(%rip)
	movl	%edx, pcfrac_aborts(%rip)
	movl	$1, %eax
	retq
.Lfunc_end16:
	.size	pcfracInit, .Lfunc_end16-pcfracInit
	.cfi_endproc

	.type	cfracNabort,@object     # @cfracNabort
	.bss
	.globl	cfracNabort
	.p2align	2
cfracNabort:
	.long	0                       # 0x0
	.size	cfracNabort, 4

	.type	cfracTsolns,@object     # @cfracTsolns
	.globl	cfracTsolns
	.p2align	2
cfracTsolns:
	.long	0                       # 0x0
	.size	cfracTsolns, 4

	.type	cfracPsolns,@object     # @cfracPsolns
	.globl	cfracPsolns
	.p2align	2
cfracPsolns:
	.long	0                       # 0x0
	.size	cfracPsolns, 4

	.type	cfracT2solns,@object    # @cfracT2solns
	.globl	cfracT2solns
	.p2align	2
cfracT2solns:
	.long	0                       # 0x0
	.size	cfracT2solns, 4

	.type	cfracFsolns,@object     # @cfracFsolns
	.globl	cfracFsolns
	.p2align	2
cfracFsolns:
	.long	0                       # 0x0
	.size	cfracFsolns, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" = "
	.size	.L.str, 4

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	" *"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" %u"
	.size	.L.str.2, 4

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" %u^%u"
	.size	.L.str.3, 7

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	" Abort %u on p = %u (>=%u) and q > "
	.size	.L.str.4, 36

	.type	pcfrac_k,@object        # @pcfrac_k
	.data
	.p2align	2
pcfrac_k:
	.long	1                       # 0x1
	.size	pcfrac_k, 4

	.type	pcfrac_m,@object        # @pcfrac_m
	.local	pcfrac_m
	.comm	pcfrac_m,4,4
	.type	pcfrac_aborts,@object   # @pcfrac_aborts
	.p2align	2
pcfrac_aborts:
	.long	3                       # 0x3
	.size	pcfrac_aborts, 4

	.type	.L.str.5,@object        # @.str.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.5:
	.asciz	"pcfrac"
	.size	.L.str.5, 7

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"out of memory"
	.size	.L.str.6, 14

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"factorBase[%u]: "
	.size	.L.str.7, 17

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%u "
	.size	.L.str.8, 4

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"cycle encountered; pick bigger k"
	.size	.L.str.9, 33

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Partial: "
	.size	.L.str.10, 10

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"\n"
	.size	.L.str.11, 2

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	" -->\n"
	.size	.L.str.12, 6

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"PartSum: "
	.size	.L.str.13, 10

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.zero	1
	.size	.L.str.14, 1

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Full:    "
	.size	.L.str.15, 10

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	" -->\nFullSum: "
	.size	.L.str.16, 15

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	" -->\nSquare:  "
	.size	.L.str.17, 15

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"x,y:     "
	.size	.L.str.18, 10

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"  "
	.size	.L.str.19, 3

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Error!  Degenerate solution:\n"
	.size	.L.str.20, 30

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"x,y:   "
	.size	.L.str.21, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
