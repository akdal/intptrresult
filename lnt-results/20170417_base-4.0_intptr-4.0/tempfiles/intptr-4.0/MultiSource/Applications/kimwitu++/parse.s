	.text
	.file	"parse.bc"
	.globl	_ZN2kc23mergephylumdeclarationsEPNS_22impl_phylumdeclarationEPNS_23impl_phylumdeclarationsE
	.p2align	4, 0x90
	.type	_ZN2kc23mergephylumdeclarationsEPNS_22impl_phylumdeclarationEPNS_23impl_phylumdeclarationsE,@function
_ZN2kc23mergephylumdeclarationsEPNS_22impl_phylumdeclarationEPNS_23impl_phylumdeclarationsE: # @_ZN2kc23mergephylumdeclarationsEPNS_22impl_phylumdeclarationEPNS_23impl_phylumdeclarationsE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movq	(%rbp), %rax
	callq	*(%rax)
	cmpl	$15, %eax
	jne	.LBB0_52
# BB#1:
	movq	32(%rbp), %r13
	movq	40(%rbp), %r15
	movq	48(%rbp), %r12
	movq	56(%rbp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r13, %rdi
	callq	_ZN2kc16f_lookupuserdeclEPNS_7impl_IDE
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_4
# BB#2:
	cmpq	%rbp, %r14
	je	.LBB0_3
# BB#5:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$15, %eax
	jne	.LBB0_49
# BB#6:
	movq	32(%r14), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	40(%r14), %rbp
	movq	48(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	56(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$18, %eax
	jne	.LBB0_10
# BB#7:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$17, %eax
	jne	.LBB0_10
# BB#8:
	movq	8(%rbp), %rdi
	movq	8(%r15), %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	jne	.LBB0_9
.LBB0_10:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$17, %eax
	jne	.LBB0_13
# BB#11:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$18, %eax
	jne	.LBB0_13
# BB#12:
	movq	8(%rbp), %rdi
	movq	8(%r15), %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	je	.LBB0_13
.LBB0_9:
	movq	8(%r15), %rax
	movq	32(%rax), %rdi
	movl	24(%rax), %esi
	jmp	.LBB0_23
.LBB0_52:
	movl	$.L.str.5, %edi
	movl	$215, %esi
.LBB0_50:
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%ebx, %ebx
.LBB0_51:
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_3:
	movq	%r13, %rdi
	callq	_ZN2kc7f_addedEPNS_7impl_IDE
	testb	%al, %al
	jne	.LBB0_51
.LBB0_4:
	movq	%r13, %rdi
	callq	_ZN2kc5v_addEPNS_7impl_IDE
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc22ConsphylumdeclarationsEPNS_22impl_phylumdeclarationEPNS_23impl_phylumdeclarationsE # TAILCALL
.LBB0_49:
	movl	$.L.str.5, %edi
	movl	$209, %esi
	jmp	.LBB0_50
.LBB0_13:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$18, %eax
	jne	.LBB0_16
# BB#14:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$18, %eax
	jne	.LBB0_16
# BB#15:
	movq	8(%rbp), %rdi
	movq	8(%r15), %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	jne	.LBB0_24
.LBB0_16:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$17, %eax
	jne	.LBB0_19
# BB#17:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$17, %eax
	jne	.LBB0_19
# BB#18:
	movq	8(%rbp), %rdi
	movq	8(%r15), %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	jne	.LBB0_24
.LBB0_19:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$16, %eax
	je	.LBB0_24
# BB#20:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$16, %eax
	jne	.LBB0_22
# BB#21:
	movq	%r15, 40(%r14)
	jmp	.LBB0_24
.LBB0_22:
	movq	32(%r13), %rdi
	movl	24(%r13), %esi
.LBB0_23:
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %r15
	movl	$.L.str, %edi
	movl	$.L.str.1, %edx
	movq	%rbp, %rsi
	movq	8(%rsp), %rcx           # 8-byte Reload
	callq	_ZN2kc28Problem1S1storageoption1S1IDEPKcPNS_18impl_storageoptionES1_PNS_7impl_IDE
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB0_24:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$23, %eax
	movq	24(%rsp), %rbp          # 8-byte Reload
	jne	.LBB0_27
# BB#25:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$23, %eax
	jne	.LBB0_27
# BB#26:
	movq	8(%r12), %rdi
	movq	8(%rbp), %rsi
	callq	_ZN2kc6concatEPKNS_17impl_alternativesES2_
	movq	%rax, 8(%rbp)
	jmp	.LBB0_34
.LBB0_27:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$23, %eax
	jne	.LBB0_29
# BB#28:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$21, %eax
	je	.LBB0_33
.LBB0_29:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$24, %eax
	jne	.LBB0_31
# BB#30:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$21, %eax
	je	.LBB0_33
.LBB0_31:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$22, %eax
	jne	.LBB0_37
# BB#32:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$21, %eax
	jne	.LBB0_37
.LBB0_33:
	movq	%r12, 48(%r14)
	jmp	.LBB0_34
.LBB0_37:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$21, %eax
	jne	.LBB0_39
# BB#38:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$21, %eax
	je	.LBB0_34
.LBB0_39:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$23, %eax
	jne	.LBB0_42
# BB#40:
	movq	32(%r13), %rdi
	movl	24(%r13), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbp
	movl	$.L.str.2, %edi
	jmp	.LBB0_41
.LBB0_42:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$24, %eax
	jne	.LBB0_44
# BB#43:
	movq	32(%r13), %rdi
	movl	24(%r13), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbp
	movl	$.L.str.3, %edi
	jmp	.LBB0_41
.LBB0_44:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$22, %eax
	jne	.LBB0_46
# BB#45:
	movq	32(%r13), %rdi
	movl	24(%r13), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbp
	movl	$.L.str.4, %edi
.LBB0_41:
	movq	%r13, %rsi
	callq	_ZN2kc12Problem1S1IDEPKcPNS_7impl_IDE
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB0_34:
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$31, %eax
	jne	.LBB0_48
# BB#35:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$31, %eax
	jne	.LBB0_48
# BB#36:
	movq	8(%rbp), %rdi
	movq	16(%rbp), %r15
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rsi
	movq	16(%rax), %r12
	callq	_ZN2kc6concatEPKNS_15impl_attributesES2_
	movq	%rax, %rbp
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	_ZN2kc6concatEPKNS_11impl_CtextsES2_
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11CcodeOptionEPNS_15impl_attributesEPNS_11impl_CtextsE
	movq	%rax, 56(%r14)
	jmp	.LBB0_51
.LBB0_48:
	movl	$.L.str.5, %edi
	movl	$204, %esi
	jmp	.LBB0_50
.LBB0_46:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$21, %eax
	je	.LBB0_34
# BB#47:
	movl	$.L.str.5, %edi
	movl	$189, %esi
	jmp	.LBB0_50
.Lfunc_end0:
	.size	_ZN2kc23mergephylumdeclarationsEPNS_22impl_phylumdeclarationEPNS_23impl_phylumdeclarationsE, .Lfunc_end0-_ZN2kc23mergephylumdeclarationsEPNS_22impl_phylumdeclarationEPNS_23impl_phylumdeclarationsE
	.cfi_endproc

	.globl	_ZN2kc16f_lookupuserdeclEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc16f_lookupuserdeclEPNS_7impl_IDE,@function
_ZN2kc16f_lookupuserdeclEPNS_7impl_IDE: # @_ZN2kc16f_lookupuserdeclEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	(%r14), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB1_27
# BB#1:
	movq	40(%r14), %rax
	movq	8(%rax), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$184, %eax
	jne	.LBB1_4
# BB#2:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.10, %edi
	jmp	.LBB1_3
.LBB1_27:
	movl	$.L.str.21, %edi
	movl	$335, %esi              # imm = 0x14F
.LBB1_28:
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	jmp	.LBB1_29
.LBB1_4:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$183, %eax
	jne	.LBB1_6
# BB#5:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.11, %edi
	jmp	.LBB1_3
.LBB1_6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$182, %eax
	jne	.LBB1_8
# BB#7:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.12, %edi
	jmp	.LBB1_3
.LBB1_8:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$180, %eax
	jne	.LBB1_10
# BB#9:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.13, %edi
	jmp	.LBB1_3
.LBB1_10:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$179, %eax
	jne	.LBB1_12
# BB#11:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.14, %edi
	jmp	.LBB1_3
.LBB1_12:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$178, %eax
	jne	.LBB1_14
# BB#13:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.15, %edi
	jmp	.LBB1_3
.LBB1_14:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$177, %eax
	jne	.LBB1_16
# BB#15:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.16, %edi
	jmp	.LBB1_3
.LBB1_16:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB1_18
# BB#17:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.17, %edi
	jmp	.LBB1_3
.LBB1_18:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$175, %eax
	jne	.LBB1_20
# BB#19:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.18, %edi
.LBB1_3:
	movq	%r14, %rsi
	callq	_ZN2kc12Problem1S1IDEPKcPNS_7impl_IDE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB1_29:
	xorl	%eax, %eax
.LBB1_30:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_20:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB1_22
# BB#21:
	movq	8(%rbx), %rax
	jmp	.LBB1_30
.LBB1_22:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	jne	.LBB1_24
# BB#23:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.19, %edi
	jmp	.LBB1_3
.LBB1_24:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$172, %eax
	jne	.LBB1_26
# BB#25:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.20, %edi
	jmp	.LBB1_3
.LBB1_26:
	movl	$.L.str.21, %edi
	movl	$330, %esi              # imm = 0x14A
	jmp	.LBB1_28
.Lfunc_end1:
	.size	_ZN2kc16f_lookupuserdeclEPNS_7impl_IDE, .Lfunc_end1-_ZN2kc16f_lookupuserdeclEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc20makeListAlternativesEPNS_7impl_IDES1_
	.p2align	4, 0x90
	.type	_ZN2kc20makeListAlternativesEPNS_7impl_IDES1_,@function
_ZN2kc20makeListAlternativesEPNS_7impl_IDES1_: # @_ZN2kc20makeListAlternativesEPNS_7impl_IDES1_
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:                                 # %._crit_edge.i.i.i.i
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 160
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	leaq	24(%rsp), %rbx
	movq	%rbx, 8(%rsp)
	movb	$108, 26(%rsp)
	movw	$26958, 24(%rsp)        # imm = 0x694E
	movq	$3, 16(%rsp)
	movb	$0, 27(%rsp)
	movq	(%r12), %rax
.Ltmp0:
	callq	*(%rax)
.Ltmp1:
# BB#1:                                 # %.noexc39
	cmpl	$7, %eax
	jne	.LBB2_5
# BB#2:
	movq	40(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp2:
	callq	*(%rax)
.Ltmp3:
# BB#3:                                 # %.noexc40
	cmpl	$6, %eax
	jne	.LBB2_5
# BB#4:
	movq	40(%r12), %rax
	movq	40(%rax), %rax
	movq	8(%rax), %rdx
	jmp	.LBB2_7
.LBB2_5:
.Ltmp4:
	movl	$.L.str.9, %edi
	movl	$246, %esi
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.Ltmp5:
# BB#6:
	xorl	%edx, %edx
.LBB2_7:                                # %_ZN2kc9f_strofIDEPNS_7impl_IDE.exit
.Ltmp6:
	leaq	72(%rsp), %rdi
	leaq	8(%rsp), %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp7:
# BB#8:
	movq	72(%rsp), %rdi
.Ltmp9:
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
.Ltmp10:
# BB#9:
.Ltmp11:
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
.Ltmp12:
# BB#10:
.Ltmp13:
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r15
.Ltmp14:
# BB#11:
	movq	72(%rsp), %rdi
	leaq	88(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB2_13
# BB#12:
	callq	_ZdlPv
.LBB2_13:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
	movq	8(%rsp), %rdi
	cmpq	%rbx, %rdi
	je	.LBB2_15
# BB#14:
	callq	_ZdlPv
.LBB2_15:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit42
	movq	%rbx, 8(%rsp)
	movl	$1936617283, 24(%rsp)   # imm = 0x736E6F43
	movq	$4, 16(%rsp)
	movb	$0, 28(%rsp)
	movq	(%r12), %rax
.Ltmp16:
	movq	%r12, %rdi
	callq	*(%rax)
.Ltmp17:
# BB#16:                                # %.noexc50
	cmpl	$7, %eax
	jne	.LBB2_20
# BB#17:
	movq	40(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp18:
	callq	*(%rax)
.Ltmp19:
# BB#18:                                # %.noexc51
	cmpl	$6, %eax
	jne	.LBB2_20
# BB#19:
	movq	40(%r12), %rax
	movq	40(%rax), %rax
	movq	8(%rax), %rdx
	jmp	.LBB2_22
.LBB2_20:
.Ltmp20:
	movl	$.L.str.9, %edi
	movl	$246, %esi
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.Ltmp21:
# BB#21:
	xorl	%edx, %edx
.LBB2_22:                               # %_ZN2kc9f_strofIDEPNS_7impl_IDE.exit53
.Ltmp22:
	leaq	40(%rsp), %rdi
	leaq	8(%rsp), %rsi
	callq	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Ltmp23:
# BB#23:
	movq	40(%rsp), %rdi
.Ltmp25:
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
.Ltmp26:
# BB#24:
.Ltmp27:
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
.Ltmp28:
# BB#25:
.Ltmp29:
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %rbp
.Ltmp30:
# BB#26:
	movq	40(%rsp), %rdi
	leaq	56(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB2_28
# BB#27:
	callq	_ZdlPv
.LBB2_28:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit54
	movq	8(%rsp), %rdi
	cmpq	%rbx, %rdi
	je	.LBB2_30
# BB#29:
	callq	_ZdlPv
.LBB2_30:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit55
	movl	24(%r14), %eax
	movl	%eax, 24(%r15)
	movq	32(%r14), %rax
	movq	%rax, 32(%r15)
	movl	24(%r14), %eax
	movl	%eax, 24(%rbp)
	movq	32(%r14), %rax
	movq	%rax, 32(%rbp)
	callq	_ZN2kc12NilargumentsEv
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11AlternativeEPNS_7impl_IDEPNS_14impl_argumentsE
	movq	%rax, %r13
	callq	_ZN2kc12NilargumentsEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13ConsargumentsEPNS_7impl_IDEPNS_14impl_argumentsE
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13ConsargumentsEPNS_7impl_IDEPNS_14impl_argumentsE
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc11AlternativeEPNS_7impl_IDEPNS_14impl_argumentsE
	movq	%rax, %rbx
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	_ZN2kc14ITUserOperatorEPNS_16impl_alternativeEPNS_7impl_IDE
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13v_extendoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	_ZN2kc14ITUserOperatorEPNS_16impl_alternativeEPNS_7impl_IDE
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc13v_extendoccurEPNS_7impl_IDEPNS_11impl_IDtypeE
	callq	_ZN2kc15NilalternativesEv
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc16ConsalternativesEPNS_16impl_alternativeEPNS_17impl_alternativesE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc16ConsalternativesEPNS_16impl_alternativeEPNS_17impl_alternativesE
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_31:
.Ltmp31:
	movq	%rax, %rbp
	movq	40(%rsp), %rdi
	leaq	56(%rsp), %rax
	cmpq	%rax, %rdi
	jne	.LBB2_34
	jmp	.LBB2_37
.LBB2_32:
.Ltmp24:
	jmp	.LBB2_36
.LBB2_33:
.Ltmp15:
	movq	%rax, %rbp
	movq	72(%rsp), %rdi
	leaq	88(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB2_37
.LBB2_34:
	callq	_ZdlPv
	jmp	.LBB2_37
.LBB2_35:
.Ltmp8:
.LBB2_36:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit58
	movq	%rax, %rbp
.LBB2_37:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit58
	movq	8(%rsp), %rdi
	cmpq	%rbx, %rdi
	je	.LBB2_39
# BB#38:
	callq	_ZdlPv
.LBB2_39:
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN2kc20makeListAlternativesEPNS_7impl_IDES1_, .Lfunc_end2-_ZN2kc20makeListAlternativesEPNS_7impl_IDES1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp0           #   Call between .Ltmp0 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp14-.Ltmp9          #   Call between .Ltmp9 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin0   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp23-.Ltmp16         #   Call between .Ltmp16 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin0   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp30-.Ltmp25         #   Call between .Ltmp25 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin0   #     jumps to .Ltmp31
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Lfunc_end2-.Ltmp30     #   Call between .Ltmp30 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_,comdat
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
	.p2align	4, 0x90
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_,@function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_: # @_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 64
.Lcfi37:
	.cfi_offset %rbx, -48
.Lcfi38:
	.cfi_offset %r12, -40
.Lcfi39:
	.cfi_offset %r13, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rdi, %r12
	leaq	16(%r12), %r13
	movq	%r13, (%r12)
	movq	(%rsi), %r15
	movq	8(%rsi), %rbx
	testq	%r15, %r15
	jne	.LBB3_2
# BB#1:
	testq	%rbx, %rbx
	jne	.LBB3_17
.LBB3_2:
	movq	%rbx, 8(%rsp)
	cmpq	$15, %rbx
	jbe	.LBB3_3
# BB#4:                                 # %.noexc6.i
	leaq	8(%rsp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm
	movq	%rax, (%r12)
	movq	8(%rsp), %rcx
	movq	%rcx, 16(%r12)
	testq	%rbx, %rbx
	jne	.LBB3_6
	jmp	.LBB3_9
.LBB3_3:                                # %._crit_edge.i.i.i.i
	movq	%r13, %rax
	testq	%rbx, %rbx
	je	.LBB3_9
.LBB3_6:
	cmpq	$1, %rbx
	jne	.LBB3_8
# BB#7:
	movb	(%r15), %cl
	movb	%cl, (%rax)
	jmp	.LBB3_9
.LBB3_8:
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	memcpy
.LBB3_9:                                # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2ERKS4_.exit
	movq	8(%rsp), %rax
	movq	%rax, 8(%r12)
	movq	(%r12), %rcx
	movb	$0, (%rcx,%rax)
	movq	%r14, %rdi
	callq	strlen
	movabsq	$9223372036854775807, %rcx # imm = 0x7FFFFFFFFFFFFFFF
	subq	8(%r12), %rcx
	cmpq	%rax, %rcx
	jb	.LBB3_10
# BB#15:                                # %_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE15_M_check_lengthEmmPKc.exit.i
.Ltmp32:
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm
.Ltmp33:
# BB#16:                                # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc.exit
	movq	%r12, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB3_17:                               # %.noexc.i
	movl	$.L.str.51, %edi
	callq	_ZSt19__throw_logic_errorPKc
.LBB3_10:
.Ltmp34:
	movl	$.L.str.52, %edi
	callq	_ZSt20__throw_length_errorPKc
.Ltmp35:
# BB#11:                                # %.noexc
.LBB3_12:
.Ltmp36:
	movq	%rax, %rbx
	movq	(%r12), %rdi
	cmpq	%r13, %rdi
	je	.LBB3_14
# BB#13:
	callq	_ZdlPv
.LBB3_14:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_, .Lfunc_end3-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_PKS5_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp32-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp36-.Lfunc_begin1   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin1   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end3-.Ltmp35     #   Call between .Ltmp35 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN2kc9f_strofIDEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc9f_strofIDEPNS_7impl_IDE,@function
_ZN2kc9f_strofIDEPNS_7impl_IDE:         # @_ZN2kc9f_strofIDEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 16
.Lcfi43:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB4_3
# BB#1:
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$6, %eax
	jne	.LBB4_3
# BB#2:
	movq	40(%rbx), %rax
	movq	40(%rax), %rax
	movq	8(%rax), %rax
	popq	%rbx
	retq
.LBB4_3:
	movl	$.L.str.9, %edi
	movl	$246, %esi
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZN2kc9f_strofIDEPNS_7impl_IDE, .Lfunc_end4-_ZN2kc9f_strofIDEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc12f_lookupdeclEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc12f_lookupdeclEPNS_7impl_IDE,@function
_ZN2kc12f_lookupdeclEPNS_7impl_IDE:     # @_ZN2kc12f_lookupdeclEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -24
.Lcfi48:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	(%r14), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB5_27
# BB#1:
	movq	40(%r14), %rax
	movq	8(%rax), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$184, %eax
	jne	.LBB5_4
# BB#2:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.10, %edi
	jmp	.LBB5_3
.LBB5_27:
	movl	$.L.str.22, %edi
	movl	$422, %esi              # imm = 0x1A6
.LBB5_28:
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	jmp	.LBB5_29
.LBB5_4:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$183, %eax
	jne	.LBB5_6
# BB#5:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.11, %edi
	jmp	.LBB5_3
.LBB5_6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$182, %eax
	jne	.LBB5_8
# BB#7:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.12, %edi
	jmp	.LBB5_3
.LBB5_8:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$180, %eax
	jne	.LBB5_10
# BB#9:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.13, %edi
	jmp	.LBB5_3
.LBB5_10:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$179, %eax
	jne	.LBB5_12
# BB#11:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.14, %edi
	jmp	.LBB5_3
.LBB5_12:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$178, %eax
	jne	.LBB5_14
# BB#13:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.15, %edi
	jmp	.LBB5_3
.LBB5_14:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$177, %eax
	jne	.LBB5_16
# BB#15:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.16, %edi
	jmp	.LBB5_3
.LBB5_16:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB5_18
# BB#17:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.17, %edi
	jmp	.LBB5_3
.LBB5_18:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$175, %eax
	jne	.LBB5_20
# BB#19:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.18, %edi
.LBB5_3:
	movq	%r14, %rsi
	callq	_ZN2kc12Problem1S1IDEPKcPNS_7impl_IDE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB5_29:
	xorl	%eax, %eax
.LBB5_30:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB5_20:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB5_22
# BB#21:
	movq	8(%rbx), %rax
	jmp	.LBB5_30
.LBB5_22:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	jne	.LBB5_24
# BB#23:
	movq	8(%rbx), %rax
	jmp	.LBB5_30
.LBB5_24:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$172, %eax
	jne	.LBB5_26
# BB#25:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.20, %edi
	jmp	.LBB5_3
.LBB5_26:
	movl	$.L.str.22, %edi
	movl	$417, %esi              # imm = 0x1A1
	jmp	.LBB5_28
.Lfunc_end5:
	.size	_ZN2kc12f_lookupdeclEPNS_7impl_IDE, .Lfunc_end5-_ZN2kc12f_lookupdeclEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc21insert_in_argsnumbersEiPNS_16impl_argsnumbersE
	.p2align	4, 0x90
	.type	_ZN2kc21insert_in_argsnumbersEiPNS_16impl_argsnumbersE,@function
_ZN2kc21insert_in_argsnumbersEiPNS_16impl_argsnumbersE: # @_ZN2kc21insert_in_argsnumbersEiPNS_16impl_argsnumbersE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -32
.Lcfi53:
	.cfi_offset %r14, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$216, %eax
	jne	.LBB6_6
# BB#1:
	movq	8(%rbx), %r14
	cmpl	%ebp, 8(%r14)
	jg	.LBB6_2
# BB#4:
	je	.LBB6_8
# BB#5:
	movq	16(%rbx), %rsi
	movl	%ebp, %edi
	callq	_ZN2kc21insert_in_argsnumbersEiPNS_16impl_argsnumbersE
	movq	%r14, %rdi
	movq	%rax, %rsi
	jmp	.LBB6_3
.LBB6_6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$215, %eax
	jne	.LBB6_7
.LBB6_2:
	movl	%ebp, %edi
	callq	_ZN2kc9mkintegerEi
	movq	%rax, %rdi
	movq	%rbx, %rsi
.LBB6_3:
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	_ZN2kc15ConsargsnumbersEPNS_17impl_integer__IntEPNS_16impl_argsnumbersE # TAILCALL
.LBB6_7:
	movl	$.L.str.23, %edi
	movl	$446, %esi              # imm = 0x1BE
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%ebx, %ebx
.LBB6_8:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN2kc21insert_in_argsnumbersEiPNS_16impl_argsnumbersE, .Lfunc_end6-_ZN2kc21insert_in_argsnumbersEiPNS_16impl_argsnumbersE
	.cfi_endproc

	.globl	_ZN2kc16set_includefilesEPNS_17impl_includefilesEPNS_23impl_includedeclarationE
	.p2align	4, 0x90
	.type	_ZN2kc16set_includefilesEPNS_17impl_includefilesEPNS_23impl_includedeclarationE,@function
_ZN2kc16set_includefilesEPNS_17impl_includefilesEPNS_23impl_includedeclarationE: # @_ZN2kc16set_includefilesEPNS_17impl_includefilesEPNS_23impl_includedeclarationE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 48
.Lcfi60:
	.cfi_offset %rbx, -48
.Lcfi61:
	.cfi_offset %r12, -40
.Lcfi62:
	.cfi_offset %r13, -32
.Lcfi63:
	.cfi_offset %r14, -24
.Lcfi64:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	jmp	.LBB7_2
	.p2align	4, 0x90
.LBB7_1:                                # %.lr.ph
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	8(%rbx), %r15
	leaq	16(%r15), %r12
	leaq	24(%r15), %r13
	cmpl	$0, 8(%r15)
	movq	%r13, %rax
	cmoveq	%r12, %rax
	movq	(%rax), %rsi
	movq	%r14, %rdi
	callq	_ZN2kc23ConsincludedeclarationsEPNS_23impl_includedeclarationEPNS_24impl_includedeclarationsE
	cmpl	$0, 8(%r15)
	cmoveq	%r12, %r13
	movq	%rax, (%r13)
	movq	16(%rbx), %rbx
.LBB7_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$60, %eax
	je	.LBB7_1
# BB#3:                                 # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	_ZN2kc16set_includefilesEPNS_17impl_includefilesEPNS_23impl_includedeclarationE, .Lfunc_end7-_ZN2kc16set_includefilesEPNS_17impl_includefilesEPNS_23impl_includedeclarationE
	.cfi_endproc

	.globl	_ZN2kc19merge_languagenamesEPNS_18impl_languagenamesES1_
	.p2align	4, 0x90
	.type	_ZN2kc19merge_languagenamesEPNS_18impl_languagenamesES1_,@function
_ZN2kc19merge_languagenamesEPNS_18impl_languagenamesES1_: # @_ZN2kc19merge_languagenamesEPNS_18impl_languagenamesES1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 48
.Lcfi70:
	.cfi_offset %rbx, -48
.Lcfi71:
	.cfi_offset %r12, -40
.Lcfi72:
	.cfi_offset %r13, -32
.Lcfi73:
	.cfi_offset %r14, -24
.Lcfi74:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	jmp	.LBB8_1
	.p2align	4, 0x90
.LBB8_5:                                #   in Loop: Header=BB8_1 Depth=1
	movq	16(%rbx), %rbx
.LBB8_1:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$165, %eax
	jne	.LBB8_6
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB8_1 Depth=1
	movq	8(%rbx), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB8_5
# BB#3:                                 #   in Loop: Header=BB8_1 Depth=1
	movq	40(%r15), %r13
	movq	8(%r13), %r12
	callq	_ZN2kc9ITUnknownEv
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	je	.LBB8_5
# BB#4:                                 #   in Loop: Header=BB8_1 Depth=1
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	_ZN2kc17ConslanguagenamesEPNS_7impl_IDEPNS_18impl_languagenamesE
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_ZNK2kc18impl_abstract_list6lengthEv
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-1(%rax), %edi
	callq	_ZN2kc9mkintegerEi
	movq	%rax, %rdi
	callq	_ZN2kc14ITLanguageNameEPNS_17impl_integer__IntE
	movq	%rax, 8(%r13)
	jmp	.LBB8_5
.LBB8_6:                                # %._crit_edge
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	_ZN2kc19merge_languagenamesEPNS_18impl_languagenamesES1_, .Lfunc_end8-_ZN2kc19merge_languagenamesEPNS_18impl_languagenamesES1_
	.cfi_endproc

	.globl	_ZN2kc11get_text_nrEv
	.p2align	4, 0x90
	.type	_ZN2kc11get_text_nrEv,@function
_ZN2kc11get_text_nrEv:                  # @_ZN2kc11get_text_nrEv
	.cfi_startproc
# BB#0:
	movb	$1, _ZL21language_text_nr_used(%rip)
	movq	_ZL16language_text_nr(%rip), %rax
	retq
.Lfunc_end9:
	.size	_ZN2kc11get_text_nrEv, .Lfunc_end9-_ZN2kc11get_text_nrEv
	.cfi_endproc

	.globl	_ZN2kc11inc_text_nrEv
	.p2align	4, 0x90
	.type	_ZN2kc11inc_text_nrEv,@function
_ZN2kc11inc_text_nrEv:                  # @_ZN2kc11inc_text_nrEv
	.cfi_startproc
# BB#0:
	cmpb	$1, _ZL21language_text_nr_used(%rip)
	jne	.LBB10_2
# BB#1:
	incq	_ZL16language_text_nr(%rip)
	movb	$0, _ZL21language_text_nr_used(%rip)
.LBB10_2:
	retq
.Lfunc_end10:
	.size	_ZN2kc11inc_text_nrEv, .Lfunc_end10-_ZN2kc11inc_text_nrEv
	.cfi_endproc

	.globl	_ZN2kc12last_text_nrEv
	.p2align	4, 0x90
	.type	_ZN2kc12last_text_nrEv,@function
_ZN2kc12last_text_nrEv:                 # @_ZN2kc12last_text_nrEv
	.cfi_startproc
# BB#0:
	movb	_ZL21language_text_nr_used(%rip), %cl
	movq	_ZL16language_text_nr(%rip), %rax
	notb	%cl
	movzbl	%cl, %ecx
	andl	$1, %ecx
	subq	%rcx, %rax
	retq
.Lfunc_end11:
	.size	_ZN2kc12last_text_nrEv, .Lfunc_end11-_ZN2kc12last_text_nrEv
	.cfi_endproc

	.globl	_ZN2kc18f_ID_of_declaratorEPNS_18impl_ac_declaratorE
	.p2align	4, 0x90
	.type	_ZN2kc18f_ID_of_declaratorEPNS_18impl_ac_declaratorE,@function
_ZN2kc18f_ID_of_declaratorEPNS_18impl_ac_declaratorE: # @_ZN2kc18f_ID_of_declaratorEPNS_18impl_ac_declaratorE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 16
.Lcfi76:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$254, %eax
	jne	.LBB12_1
# BB#2:
	movq	24(%rbx), %rdi
	popq	%rbx
	jmp	_ZN2kcL19f_ID_of_direct_declEPNS_25impl_ac_direct_declaratorE # TAILCALL
.LBB12_1:
	movl	$.L.str.24, %edi
	movl	$550, %esi              # imm = 0x226
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end12:
	.size	_ZN2kc18f_ID_of_declaratorEPNS_18impl_ac_declaratorE, .Lfunc_end12-_ZN2kc18f_ID_of_declaratorEPNS_18impl_ac_declaratorE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL19f_ID_of_direct_declEPNS_25impl_ac_direct_declaratorE,@function
_ZN2kcL19f_ID_of_direct_declEPNS_25impl_ac_direct_declaratorE: # @_ZN2kcL19f_ID_of_direct_declEPNS_25impl_ac_direct_declaratorE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 16
.Lcfi78:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	jmp	.LBB13_1
	.p2align	4, 0x90
.LBB13_2:                               #   in Loop: Header=BB13_1 Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
.LBB13_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$259, %eax              # imm = 0x103
	je	.LBB13_2
# BB#3:                                 #   in Loop: Header=BB13_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$258, %eax              # imm = 0x102
	je	.LBB13_4
# BB#5:                                 #   in Loop: Header=BB13_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$257, %eax              # imm = 0x101
	jne	.LBB13_6
.LBB13_4:                               #   in Loop: Header=BB13_1 Depth=1
	addq	$8, %rbx
	movq	(%rbx), %rbx
	jmp	.LBB13_1
.LBB13_6:                               #   in Loop: Header=BB13_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$256, %eax              # imm = 0x100
	jne	.LBB13_9
# BB#7:                                 #   in Loop: Header=BB13_1 Depth=1
	movq	8(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$254, %eax
	jne	.LBB13_12
# BB#8:                                 #   in Loop: Header=BB13_1 Depth=1
	addq	$24, %rbx
	movq	(%rbx), %rbx
	jmp	.LBB13_1
.LBB13_9:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$255, %eax
	jne	.LBB13_11
# BB#10:
	movq	8(%rbx), %rax
	popq	%rbx
	retq
.LBB13_12:
	movl	$.L.str.24, %edi
	movl	$550, %esi              # imm = 0x226
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB13_11:
	popq	%rbx
	jmp	_ZN2kc9f_emptyIdEv      # TAILCALL
.Lfunc_end13:
	.size	_ZN2kcL19f_ID_of_direct_declEPNS_25impl_ac_direct_declaratorE, .Lfunc_end13-_ZN2kcL19f_ID_of_direct_declEPNS_25impl_ac_direct_declaratorE
	.cfi_endproc

	.globl	_ZN2kc21f_ID_of_fn_declaratorEPNS_18impl_ac_declaratorEPNS_12impl_fnclassE
	.p2align	4, 0x90
	.type	_ZN2kc21f_ID_of_fn_declaratorEPNS_18impl_ac_declaratorEPNS_12impl_fnclassE,@function
_ZN2kc21f_ID_of_fn_declaratorEPNS_18impl_ac_declaratorEPNS_12impl_fnclassE: # @_ZN2kc21f_ID_of_fn_declaratorEPNS_18impl_ac_declaratorEPNS_12impl_fnclassE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi81:
	.cfi_def_cfa_offset 64
.Lcfi82:
	.cfi_offset %rbx, -24
.Lcfi83:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$254, %eax
	jne	.LBB14_19
	.p2align	4, 0x90
.LBB14_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_2 Depth 2
	addq	$24, %rbx
	jmp	.LBB14_2
	.p2align	4, 0x90
.LBB14_3:                               #   in Loop: Header=BB14_2 Depth=2
	addq	$16, %rbx
.LBB14_2:                               # %tailrecurse.i
                                        #   Parent Loop BB14_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$259, %eax              # imm = 0x103
	je	.LBB14_3
# BB#4:                                 #   in Loop: Header=BB14_2 Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$258, %eax              # imm = 0x102
	jne	.LBB14_6
# BB#5:                                 #   in Loop: Header=BB14_2 Depth=2
	addq	$8, %rbx
	jmp	.LBB14_2
	.p2align	4, 0x90
.LBB14_6:                               #   in Loop: Header=BB14_2 Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$257, %eax              # imm = 0x101
	jne	.LBB14_8
# BB#7:                                 #   in Loop: Header=BB14_2 Depth=2
	addq	$8, %rbx
	jmp	.LBB14_2
	.p2align	4, 0x90
.LBB14_8:                               #   in Loop: Header=BB14_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$256, %eax              # imm = 0x100
	jne	.LBB14_11
# BB#9:                                 #   in Loop: Header=BB14_1 Depth=1
	movq	8(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$254, %eax
	je	.LBB14_1
# BB#10:
	movl	$.L.str.24, %edi
	movl	$550, %esi              # imm = 0x226
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%ebx, %ebx
	jmp	.LBB14_14
.LBB14_19:
	movl	$.L.str.27, %edi
	movl	$593, %esi              # imm = 0x251
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%ebx, %ebx
	jmp	.LBB14_20
.LBB14_11:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$255, %eax
	jne	.LBB14_13
# BB#12:
	movq	8(%rbx), %rbx
	jmp	.LBB14_14
.LBB14_13:
	callq	_ZN2kc9f_emptyIdEv
	movq	%rax, %rbx
.LBB14_14:                              # %_ZN2kcL19f_ID_of_direct_declEPNS_25impl_ac_direct_declaratorE.exit
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$115, %eax
	jne	.LBB14_17
# BB#15:
	movl	_ZZN2kc21f_ID_of_fn_declaratorEPNS_18impl_ac_declaratorEPNS_12impl_fnclassEE7dtor_nr(%rip), %edx
	incl	%edx
	movl	%edx, _ZZN2kc21f_ID_of_fn_declaratorEPNS_18impl_ac_declaratorEPNS_12impl_fnclassEE7dtor_nr(%rip)
	movq	%rsp, %r14
	movl	$.L.str.25, %esi
	jmp	.LBB14_16
.LBB14_17:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$114, %eax
	jne	.LBB14_20
# BB#18:
	movl	_ZZN2kc21f_ID_of_fn_declaratorEPNS_18impl_ac_declaratorEPNS_12impl_fnclassEE7ctor_nr(%rip), %edx
	incl	%edx
	movl	%edx, _ZZN2kc21f_ID_of_fn_declaratorEPNS_18impl_ac_declaratorEPNS_12impl_fnclassEE7ctor_nr(%rip)
	movq	%rsp, %r14
	movl	$.L.str.26, %esi
.LBB14_16:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sprintf
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	32(%rbx), %rcx
	movq	%rcx, 32(%rax)
	movl	24(%rbx), %ecx
	movl	%ecx, 24(%rax)
	movq	%rax, %rbx
.LBB14_20:
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end14:
	.size	_ZN2kc21f_ID_of_fn_declaratorEPNS_18impl_ac_declaratorEPNS_12impl_fnclassE, .Lfunc_end14-_ZN2kc21f_ID_of_fn_declaratorEPNS_18impl_ac_declaratorEPNS_12impl_fnclassE
	.cfi_endproc

	.globl	_ZN2kc21f_stars_of_declaratorEPNS_18impl_ac_declaratorE
	.p2align	4, 0x90
	.type	_ZN2kc21f_stars_of_declaratorEPNS_18impl_ac_declaratorE,@function
_ZN2kc21f_stars_of_declaratorEPNS_18impl_ac_declaratorE: # @_ZN2kc21f_stars_of_declaratorEPNS_18impl_ac_declaratorE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi86:
	.cfi_def_cfa_offset 32
.Lcfi87:
	.cfi_offset %rbx, -24
.Lcfi88:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	xorl	%ebp, %ebp
	cmpl	$254, %eax
	jne	.LBB15_5
# BB#1:
	movq	8(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$264, %eax              # imm = 0x108
	jne	.LBB15_5
# BB#2:
	movq	8(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movl	$1, %ebp
	cmpl	$266, %eax              # imm = 0x10A
	jne	.LBB15_5
# BB#3:                                 # %tailrecurse.i.i.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB15_4:                               # %tailrecurse.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rbx
	incl	%ebp
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$266, %eax              # imm = 0x10A
	je	.LBB15_4
.LBB15_5:                               # %_ZN2kcL28f_stars_of_ac_pointer_optionEPNS_22impl_ac_pointer_optionE.exit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end15:
	.size	_ZN2kc21f_stars_of_declaratorEPNS_18impl_ac_declaratorE, .Lfunc_end15-_ZN2kc21f_stars_of_declaratorEPNS_18impl_ac_declaratorE
	.cfi_endproc

	.globl	_ZN2kc14f_fnclass_infoEPNS_30impl_ac_declaration_specifiersEPNS_20impl_casestring__StrEPNS_18impl_ac_declaratorE
	.p2align	4, 0x90
	.type	_ZN2kc14f_fnclass_infoEPNS_30impl_ac_declaration_specifiersEPNS_20impl_casestring__StrEPNS_18impl_ac_declaratorE,@function
_ZN2kc14f_fnclass_infoEPNS_30impl_ac_declaration_specifiersEPNS_20impl_casestring__StrEPNS_18impl_ac_declaratorE: # @_ZN2kc14f_fnclass_infoEPNS_30impl_ac_declaration_specifiersEPNS_20impl_casestring__StrEPNS_18impl_ac_declaratorE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 32
.Lcfi92:
	.cfi_offset %rbx, -32
.Lcfi93:
	.cfi_offset %r14, -24
.Lcfi94:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB16_1:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$236, %eax
	jne	.LBB16_8
# BB#2:                                 #   in Loop: Header=BB16_1 Depth=1
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rbx
	callq	_ZN2kcL24f_static_in_ac_decl_specEPNS_29impl_ac_declaration_specifierE
	testb	%al, %al
	jne	.LBB16_3
# BB#4:                                 #   in Loop: Header=BB16_1 Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$254, %eax
	jne	.LBB16_1
# BB#5:                                 #   in Loop: Header=BB16_1 Depth=1
	movq	24(%r15), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$259, %eax              # imm = 0x103
	jne	.LBB16_1
# BB#6:                                 #   in Loop: Header=BB16_1 Depth=1
	movq	24(%r15), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$273, %eax              # imm = 0x111
	jne	.LBB16_1
# BB#7:
	callq	_ZN2kc8MemberFnEv
	jmp	.LBB16_11
.LBB16_8:                               # %tailrecurse._crit_edge
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$235, %eax
	jne	.LBB16_10
# BB#9:
	callq	_ZN2kc8GlobalFnEv
	jmp	.LBB16_11
.LBB16_10:
	movl	$.L.str.28, %edi
	movl	$705, %esi              # imm = 0x2C1
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	jmp	.LBB16_11
.LBB16_3:
	movq	%r14, %rdi
	callq	_ZN2kc8StaticFnEPNS_20impl_casestring__StrE
.LBB16_11:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end16:
	.size	_ZN2kc14f_fnclass_infoEPNS_30impl_ac_declaration_specifiersEPNS_20impl_casestring__StrEPNS_18impl_ac_declaratorE, .Lfunc_end16-_ZN2kc14f_fnclass_infoEPNS_30impl_ac_declaration_specifiersEPNS_20impl_casestring__StrEPNS_18impl_ac_declaratorE
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL24f_static_in_ac_decl_specEPNS_29impl_ac_declaration_specifierE,@function
_ZN2kcL24f_static_in_ac_decl_specEPNS_29impl_ac_declaration_specifierE: # @_ZN2kcL24f_static_in_ac_decl_specEPNS_29impl_ac_declaration_specifierE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 16
.Lcfi96:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$239, %eax
	je	.LBB17_11
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$238, %eax
	je	.LBB17_11
# BB#2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$237, %eax
	jne	.LBB17_9
# BB#3:
	movq	8(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$244, %eax
	je	.LBB17_11
# BB#4:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$243, %eax
	je	.LBB17_11
# BB#5:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movl	%eax, %ecx
	movb	$1, %al
	cmpl	$242, %ecx
	je	.LBB17_12
# BB#6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$241, %eax
	je	.LBB17_11
# BB#7:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$240, %eax
	je	.LBB17_11
# BB#8:
	movl	$.L.str.48, %edi
	movl	$790, %esi              # imm = 0x316
	jmp	.LBB17_10
.LBB17_9:
	movl	$.L.str.47, %edi
	movl	$766, %esi              # imm = 0x2FE
.LBB17_10:                              # %_ZN2kcL25f_static_in_ac_stor_classEPNS_31impl_ac_storage_class_specifierE.exit
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB17_11:                              # %_ZN2kcL25f_static_in_ac_stor_classEPNS_31impl_ac_storage_class_specifierE.exit
	xorl	%eax, %eax
.LBB17_12:                              # %_ZN2kcL25f_static_in_ac_stor_classEPNS_31impl_ac_storage_class_specifierE.exit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end17:
	.size	_ZN2kcL24f_static_in_ac_decl_specEPNS_29impl_ac_declaration_specifierE, .Lfunc_end17-_ZN2kcL24f_static_in_ac_decl_specEPNS_29impl_ac_declaration_specifierE
	.cfi_endproc

	.globl	_ZN2kc19f_member_class_infoEPNS_30impl_ac_declaration_specifiersEPNS_20impl_casestring__StrE
	.p2align	4, 0x90
	.type	_ZN2kc19f_member_class_infoEPNS_30impl_ac_declaration_specifiersEPNS_20impl_casestring__StrE,@function
_ZN2kc19f_member_class_infoEPNS_30impl_ac_declaration_specifiersEPNS_20impl_casestring__StrE: # @_ZN2kc19f_member_class_infoEPNS_30impl_ac_declaration_specifiersEPNS_20impl_casestring__StrE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi99:
	.cfi_def_cfa_offset 32
.Lcfi100:
	.cfi_offset %rbx, -24
.Lcfi101:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB18_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$236, %eax
	jne	.LBB18_4
# BB#2:                                 #   in Loop: Header=BB18_1 Depth=1
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rbx
	callq	_ZN2kcL24f_static_in_ac_decl_specEPNS_29impl_ac_declaration_specifierE
	testb	%al, %al
	je	.LBB18_1
# BB#3:
	movq	%r14, %rdi
	callq	_ZN2kc8StaticFnEPNS_20impl_casestring__StrE
	jmp	.LBB18_7
.LBB18_4:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$235, %eax
	jne	.LBB18_6
# BB#5:
	callq	_ZN2kc8MemberFnEv
	jmp	.LBB18_7
.LBB18_6:
	movl	$.L.str.29, %edi
	movl	$728, %esi              # imm = 0x2D8
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
.LBB18_7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end18:
	.size	_ZN2kc19f_member_class_infoEPNS_30impl_ac_declaration_specifiersEPNS_20impl_casestring__StrE, .Lfunc_end18-_ZN2kc19f_member_class_infoEPNS_30impl_ac_declaration_specifiersEPNS_20impl_casestring__StrE
	.cfi_endproc

	.globl	_ZN2kc25f_static_in_ac_decl_specsEPNS_30impl_ac_declaration_specifiersE
	.p2align	4, 0x90
	.type	_ZN2kc25f_static_in_ac_decl_specsEPNS_30impl_ac_declaration_specifiersE,@function
_ZN2kc25f_static_in_ac_decl_specsEPNS_30impl_ac_declaration_specifiersE: # @_ZN2kc25f_static_in_ac_decl_specsEPNS_30impl_ac_declaration_specifiersE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 16
.Lcfi103:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB19_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$235, %eax
	je	.LBB19_6
# BB#2:                                 #   in Loop: Header=BB19_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$236, %eax
	jne	.LBB19_5
# BB#3:                                 #   in Loop: Header=BB19_1 Depth=1
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rbx
	callq	_ZN2kcL24f_static_in_ac_decl_specEPNS_29impl_ac_declaration_specifierE
	testb	%al, %al
	je	.LBB19_1
# BB#4:
	movb	$1, %al
	jmp	.LBB19_7
.LBB19_5:
	movl	$.L.str.30, %edi
	movl	$747, %esi              # imm = 0x2EB
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB19_6:                               # %.loopexit
	xorl	%eax, %eax
.LBB19_7:                               # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end19:
	.size	_ZN2kc25f_static_in_ac_decl_specsEPNS_30impl_ac_declaration_specifiersE, .Lfunc_end19-_ZN2kc25f_static_in_ac_decl_specsEPNS_30impl_ac_declaration_specifiersE
	.cfi_endproc

	.globl	_ZN2kc33f_ID_of_ac_declaration_specifiersEPNS_30impl_ac_declaration_specifiersE
	.p2align	4, 0x90
	.type	_ZN2kc33f_ID_of_ac_declaration_specifiersEPNS_30impl_ac_declaration_specifiersE,@function
_ZN2kc33f_ID_of_ac_declaration_specifiersEPNS_30impl_ac_declaration_specifiersE: # @_ZN2kc33f_ID_of_ac_declaration_specifiersEPNS_30impl_ac_declaration_specifiersE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi106:
	.cfi_def_cfa_offset 32
.Lcfi107:
	.cfi_offset %rbx, -24
.Lcfi108:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	jmp	.LBB20_1
	.p2align	4, 0x90
.LBB20_5:                               # %tailrecurse.backedge
                                        #   in Loop: Header=BB20_1 Depth=1
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB20_1:                               # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$236, %eax
	jne	.LBB20_6
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB20_1 Depth=1
	movq	8(%r14), %rbx
	movq	16(%r14), %r14
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$238, %eax
	jne	.LBB20_9
# BB#3:                                 #   in Loop: Header=BB20_1 Depth=1
	movq	8(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$246, %eax
	jne	.LBB20_4
# BB#12:                                # %_ZN2kcL32f_ID_of_ac_declaration_specifierEPNS_29impl_ac_declaration_specifierE.exit
                                        #   in Loop: Header=BB20_1 Depth=1
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.LBB20_1
	jmp	.LBB20_13
	.p2align	4, 0x90
.LBB20_9:                               #   in Loop: Header=BB20_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$239, %eax
	je	.LBB20_1
# BB#10:                                #   in Loop: Header=BB20_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$237, %eax
	je	.LBB20_1
# BB#11:                                #   in Loop: Header=BB20_1 Depth=1
	movl	$.L.str.49, %edi
	movl	$834, %esi              # imm = 0x342
	jmp	.LBB20_5
	.p2align	4, 0x90
.LBB20_4:                               #   in Loop: Header=BB20_1 Depth=1
	movl	$.L.str.50, %edi
	movl	$847, %esi              # imm = 0x34F
	jmp	.LBB20_5
.LBB20_6:                               # %tailrecurse._crit_edge
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$235, %eax
	je	.LBB20_8
# BB#7:
	movl	$.L.str.31, %edi
	movl	$813, %esi              # imm = 0x32D
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB20_8:                               # %.loopexit
	xorl	%eax, %eax
.LBB20_13:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end20:
	.size	_ZN2kc33f_ID_of_ac_declaration_specifiersEPNS_30impl_ac_declaration_specifiersE, .Lfunc_end20-_ZN2kc33f_ID_of_ac_declaration_specifiersEPNS_30impl_ac_declaration_specifiersE
	.cfi_endproc

	.globl	_ZN2kc42check_no_patternchaingroup_in_patternchainEPNS_20impl_casestring__StrEiPNS_17impl_patternchainEPKc
	.p2align	4, 0x90
	.type	_ZN2kc42check_no_patternchaingroup_in_patternchainEPNS_20impl_casestring__StrEiPNS_17impl_patternchainEPKc,@function
_ZN2kc42check_no_patternchaingroup_in_patternchainEPNS_20impl_casestring__StrEiPNS_17impl_patternchainEPKc: # @_ZN2kc42check_no_patternchaingroup_in_patternchainEPNS_20impl_casestring__StrEiPNS_17impl_patternchainEPKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi112:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 48
.Lcfi114:
	.cfi_offset %rbx, -48
.Lcfi115:
	.cfi_offset %r12, -40
.Lcfi116:
	.cfi_offset %r14, -32
.Lcfi117:
	.cfi_offset %r15, -24
.Lcfi118:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbp
	movl	%esi, %r12d
	movq	%rdi, %rbx
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$73, %eax
	jne	.LBB21_1
.LBB21_8:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_1:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$74, %eax
	jne	.LBB21_7
# BB#2:
	movq	24(%rbp), %r15
	movq	32(%rbp), %rdx
	movq	%rbx, %rdi
	movl	%r12d, %esi
	movq	%r14, %rcx
	callq	_ZN2kc42check_no_patternchaingroup_in_patternchainEPNS_20impl_casestring__StrEiPNS_17impl_patternchainEPKc
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$78, %eax
	jne	.LBB21_3
# BB#9:
	movq	%rbx, %rdi
	movl	%r12d, %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.32, %edi
	movl	$.L.str.33, %edx
	movq	%r14, %rsi
	callq	_ZN2kc9Problem3SEPKcS1_S1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc8v_reportEPNS_10impl_errorE # TAILCALL
.LBB21_7:
	movl	$.L.str.34, %edi
	movl	$881, %esi              # imm = 0x371
.LBB21_6:
	movl	$.L.str.6, %edx
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc21kc_no_default_in_withEPKciS1_ # TAILCALL
.LBB21_3:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$79, %eax
	je	.LBB21_8
# BB#4:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$77, %eax
	je	.LBB21_8
# BB#5:
	movl	$.L.str.34, %edi
	movl	$877, %esi              # imm = 0x36D
	jmp	.LBB21_6
.Lfunc_end21:
	.size	_ZN2kc42check_no_patternchaingroup_in_patternchainEPNS_20impl_casestring__StrEiPNS_17impl_patternchainEPKc, .Lfunc_end21-_ZN2kc42check_no_patternchaingroup_in_patternchainEPNS_20impl_casestring__StrEiPNS_17impl_patternchainEPKc
	.cfi_endproc

	.globl	_ZN2kc43check_no_patternchaingroup_in_patternchainsEPNS_20impl_casestring__StrEiPNS_18impl_patternchainsEPKc
	.p2align	4, 0x90
	.type	_ZN2kc43check_no_patternchaingroup_in_patternchainsEPNS_20impl_casestring__StrEiPNS_18impl_patternchainsEPKc,@function
_ZN2kc43check_no_patternchaingroup_in_patternchainsEPNS_20impl_casestring__StrEiPNS_18impl_patternchainsEPKc: # @_ZN2kc43check_no_patternchaingroup_in_patternchainsEPNS_20impl_casestring__StrEiPNS_18impl_patternchainsEPKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi119:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi120:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi122:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 48
.Lcfi124:
	.cfi_offset %rbx, -48
.Lcfi125:
	.cfi_offset %r12, -40
.Lcfi126:
	.cfi_offset %r14, -32
.Lcfi127:
	.cfi_offset %r15, -24
.Lcfi128:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbp
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$71, %eax
	jne	.LBB22_1
# BB#3:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB22_1:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB22_2
# BB#4:
	movq	24(%rbp), %r12
	movq	32(%rbp), %rdx
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movq	%r14, %rcx
	callq	_ZN2kc43check_no_patternchaingroup_in_patternchainsEPNS_20impl_casestring__StrEiPNS_18impl_patternchainsEPKc
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movq	%r12, %rdx
	movq	%r14, %rcx
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc42check_no_patternchaingroup_in_patternchainEPNS_20impl_casestring__StrEiPNS_17impl_patternchainEPKc # TAILCALL
.LBB22_2:
	movl	$.L.str.35, %edi
	movl	$900, %esi              # imm = 0x384
	movl	$.L.str.6, %edx
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc21kc_no_default_in_withEPKciS1_ # TAILCALL
.Lfunc_end22:
	.size	_ZN2kc43check_no_patternchaingroup_in_patternchainsEPNS_20impl_casestring__StrEiPNS_18impl_patternchainsEPKc, .Lfunc_end22-_ZN2kc43check_no_patternchaingroup_in_patternchainsEPNS_20impl_casestring__StrEiPNS_18impl_patternchainsEPKc
	.cfi_endproc

	.globl	_ZN2kc53check_no_patternchaingroup_or_pattern_in_patternchainEPNS_20impl_casestring__StrEiPNS_17impl_patternchainEPKc
	.p2align	4, 0x90
	.type	_ZN2kc53check_no_patternchaingroup_or_pattern_in_patternchainEPNS_20impl_casestring__StrEiPNS_17impl_patternchainEPKc,@function
_ZN2kc53check_no_patternchaingroup_or_pattern_in_patternchainEPNS_20impl_casestring__StrEiPNS_17impl_patternchainEPKc: # @_ZN2kc53check_no_patternchaingroup_or_pattern_in_patternchainEPNS_20impl_casestring__StrEiPNS_17impl_patternchainEPKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi129:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi130:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi132:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 48
.Lcfi134:
	.cfi_offset %rbx, -48
.Lcfi135:
	.cfi_offset %r12, -40
.Lcfi136:
	.cfi_offset %r14, -32
.Lcfi137:
	.cfi_offset %r15, -24
.Lcfi138:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbp
	movl	%esi, %r12d
	movq	%rdi, %rbx
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$73, %eax
	jne	.LBB23_1
.LBB23_8:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB23_1:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$74, %eax
	jne	.LBB23_7
# BB#2:
	movq	24(%rbp), %r15
	movq	32(%rbp), %rdx
	movq	%rbx, %rdi
	movl	%r12d, %esi
	movq	%r14, %rcx
	callq	_ZN2kc53check_no_patternchaingroup_or_pattern_in_patternchainEPNS_20impl_casestring__StrEiPNS_17impl_patternchainEPKc
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$78, %eax
	jne	.LBB23_3
# BB#9:
	movq	%rbx, %rdi
	movl	%r12d, %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.32, %edi
	movl	$.L.str.33, %edx
	movq	%r14, %rsi
	callq	_ZN2kc9Problem3SEPKcS1_S1_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc8v_reportEPNS_10impl_errorE # TAILCALL
.LBB23_7:
	movl	$.L.str.36, %edi
	movl	$936, %esi              # imm = 0x3A8
.LBB23_6:
	movl	$.L.str.6, %edx
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc21kc_no_default_in_withEPKciS1_ # TAILCALL
.LBB23_3:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$79, %eax
	je	.LBB23_8
# BB#4:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$77, %eax
	je	.LBB23_8
# BB#5:
	movl	$.L.str.36, %edi
	movl	$932, %esi              # imm = 0x3A4
	jmp	.LBB23_6
.Lfunc_end23:
	.size	_ZN2kc53check_no_patternchaingroup_or_pattern_in_patternchainEPNS_20impl_casestring__StrEiPNS_17impl_patternchainEPKc, .Lfunc_end23-_ZN2kc53check_no_patternchaingroup_or_pattern_in_patternchainEPNS_20impl_casestring__StrEiPNS_17impl_patternchainEPKc
	.cfi_endproc

	.globl	_ZN2kc54check_no_patternchaingroup_or_pattern_in_patternchainsEPNS_20impl_casestring__StrEiPNS_18impl_patternchainsEPKc
	.p2align	4, 0x90
	.type	_ZN2kc54check_no_patternchaingroup_or_pattern_in_patternchainsEPNS_20impl_casestring__StrEiPNS_18impl_patternchainsEPKc,@function
_ZN2kc54check_no_patternchaingroup_or_pattern_in_patternchainsEPNS_20impl_casestring__StrEiPNS_18impl_patternchainsEPKc: # @_ZN2kc54check_no_patternchaingroup_or_pattern_in_patternchainsEPNS_20impl_casestring__StrEiPNS_18impl_patternchainsEPKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi139:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi140:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi141:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi142:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi143:
	.cfi_def_cfa_offset 48
.Lcfi144:
	.cfi_offset %rbx, -48
.Lcfi145:
	.cfi_offset %r12, -40
.Lcfi146:
	.cfi_offset %r14, -32
.Lcfi147:
	.cfi_offset %r15, -24
.Lcfi148:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbp
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$71, %eax
	jne	.LBB24_1
# BB#3:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB24_1:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB24_2
# BB#4:
	movq	24(%rbp), %r12
	movq	32(%rbp), %rdx
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movq	%r14, %rcx
	callq	_ZN2kc54check_no_patternchaingroup_or_pattern_in_patternchainsEPNS_20impl_casestring__StrEiPNS_18impl_patternchainsEPKc
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movq	%r12, %rdx
	movq	%r14, %rcx
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc53check_no_patternchaingroup_or_pattern_in_patternchainEPNS_20impl_casestring__StrEiPNS_17impl_patternchainEPKc # TAILCALL
.LBB24_2:
	movl	$.L.str.37, %edi
	movl	$955, %esi              # imm = 0x3BB
	movl	$.L.str.6, %edx
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc21kc_no_default_in_withEPKciS1_ # TAILCALL
.Lfunc_end24:
	.size	_ZN2kc54check_no_patternchaingroup_or_pattern_in_patternchainsEPNS_20impl_casestring__StrEiPNS_18impl_patternchainsEPKc, .Lfunc_end24-_ZN2kc54check_no_patternchaingroup_or_pattern_in_patternchainsEPNS_20impl_casestring__StrEiPNS_18impl_patternchainsEPKc
	.cfi_endproc

	.globl	_ZN2kc26syn_patternchains_filelineEPNS_18impl_patternchainsEPNS_20impl_casestring__StrEi
	.p2align	4, 0x90
	.type	_ZN2kc26syn_patternchains_filelineEPNS_18impl_patternchainsEPNS_20impl_casestring__StrEi,@function
_ZN2kc26syn_patternchains_filelineEPNS_18impl_patternchainsEPNS_20impl_casestring__StrEi: # @_ZN2kc26syn_patternchains_filelineEPNS_18impl_patternchainsEPNS_20impl_casestring__StrEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi149:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi150:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi151:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi152:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi153:
	.cfi_def_cfa_offset 48
.Lcfi154:
	.cfi_offset %rbx, -48
.Lcfi155:
	.cfi_offset %r12, -40
.Lcfi156:
	.cfi_offset %r14, -32
.Lcfi157:
	.cfi_offset %r15, -24
.Lcfi158:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$71, %eax
	jne	.LBB25_2
# BB#1:
	movq	%rbp, 16(%rbx)
	movl	%r14d, 8(%rbx)
	jmp	.LBB25_5
.LBB25_2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$72, %eax
	jne	.LBB25_4
# BB#3:
	movq	24(%rbx), %r15
	movq	32(%rbx), %r12
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movl	%r14d, %edx
	callq	_ZN2kc26syn_patternchains_filelineEPNS_18impl_patternchainsEPNS_20impl_casestring__StrEi
	movq	16(%r12), %rsi
	movl	8(%r12), %edx
	movq	%r15, %rdi
	callq	_ZN2kc25syn_patternchain_filelineEPNS_17impl_patternchainEPNS_20impl_casestring__StrEi
	movq	16(%r15), %rax
	movq	%rax, 16(%rbx)
	movl	8(%r15), %eax
	movl	%eax, 8(%rbx)
	jmp	.LBB25_5
.LBB25_4:
	movl	$.L.str.38, %edi
	movl	$981, %esi              # imm = 0x3D5
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%ebx, %ebx
.LBB25_5:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	_ZN2kc26syn_patternchains_filelineEPNS_18impl_patternchainsEPNS_20impl_casestring__StrEi, .Lfunc_end25-_ZN2kc26syn_patternchains_filelineEPNS_18impl_patternchainsEPNS_20impl_casestring__StrEi
	.cfi_endproc

	.globl	_ZN2kc25syn_patternchain_filelineEPNS_17impl_patternchainEPNS_20impl_casestring__StrEi
	.p2align	4, 0x90
	.type	_ZN2kc25syn_patternchain_filelineEPNS_17impl_patternchainEPNS_20impl_casestring__StrEi,@function
_ZN2kc25syn_patternchain_filelineEPNS_17impl_patternchainEPNS_20impl_casestring__StrEi: # @_ZN2kc25syn_patternchain_filelineEPNS_17impl_patternchainEPNS_20impl_casestring__StrEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi161:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi163:
	.cfi_def_cfa_offset 48
.Lcfi164:
	.cfi_offset %rbx, -40
.Lcfi165:
	.cfi_offset %r14, -32
.Lcfi166:
	.cfi_offset %r15, -24
.Lcfi167:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$73, %eax
	jne	.LBB26_2
# BB#1:
	movq	%rbp, 16(%rbx)
	movl	%r14d, 8(%rbx)
	jmp	.LBB26_5
.LBB26_2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$74, %eax
	jne	.LBB26_4
# BB#3:
	movq	24(%rbx), %r15
	movq	32(%rbx), %rdi
	movq	%rbp, %rsi
	movl	%r14d, %edx
	callq	_ZN2kc25syn_patternchain_filelineEPNS_17impl_patternchainEPNS_20impl_casestring__StrEi
	movq	16(%r15), %rax
	movq	%rax, 16(%rbx)
	movl	8(%r15), %eax
	movl	%eax, 8(%rbx)
	jmp	.LBB26_5
.LBB26_4:
	movl	$.L.str.39, %edi
	movl	$1007, %esi             # imm = 0x3EF
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%ebx, %ebx
.LBB26_5:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	_ZN2kc25syn_patternchain_filelineEPNS_17impl_patternchainEPNS_20impl_casestring__StrEi, .Lfunc_end26-_ZN2kc25syn_patternchain_filelineEPNS_17impl_patternchainEPNS_20impl_casestring__StrEi
	.cfi_endproc

	.globl	_ZN2kc23pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsE
	.p2align	4, 0x90
	.type	_ZN2kc23pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsE,@function
_ZN2kc23pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsE: # @_ZN2kc23pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi168:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi169:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi170:
	.cfi_def_cfa_offset 32
.Lcfi171:
	.cfi_offset %rbx, -24
.Lcfi172:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	_ZZN2kc23pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsEE16nrof_foreach_occ(%rip), %ebp
	incl	%ebp
	movl	%ebp, _ZZN2kc23pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsEE16nrof_foreach_occ(%rip)
	callq	_ZNK2kc18impl_abstract_list6lengthEv
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movl	%eax, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	_ZN2kc25t_pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsEiib # TAILCALL
.Lfunc_end27:
	.size	_ZN2kc23pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsE, .Lfunc_end27-_ZN2kc23pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsE
	.cfi_endproc

	.globl	_ZN2kc25t_pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsEiib
	.p2align	4, 0x90
	.type	_ZN2kc25t_pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsEiib,@function
_ZN2kc25t_pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsEiib: # @_ZN2kc25t_pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsEiib
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi173:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi174:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi175:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi176:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi177:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi178:
	.cfi_def_cfa_offset 56
	subq	$8232, %rsp             # imm = 0x2028
.Lcfi179:
	.cfi_def_cfa_offset 8288
.Lcfi180:
	.cfi_offset %rbx, -56
.Lcfi181:
	.cfi_offset %r12, -48
.Lcfi182:
	.cfi_offset %r13, -40
.Lcfi183:
	.cfi_offset %r14, -32
.Lcfi184:
	.cfi_offset %r15, -24
.Lcfi185:
	.cfi_offset %rbp, -16
	movl	%ecx, %r13d
	movl	%edx, %ebx
	movl	%esi, %r14d
	movq	%rdi, %rbp
	movq	(%rbp), %rax
	callq	*(%rax)
	cmpl	$55, %eax
	jne	.LBB28_8
# BB#1:
	movq	8(%rbp), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$56, %eax
	jne	.LBB28_8
# BB#2:
	movq	8(%rbp), %r12
	movq	16(%rbp), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	16(%r12), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	24(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	32(%rsp), %rdi
	testb	%r13b, %r13b
	je	.LBB28_4
# BB#3:
	movl	$.L.str.40, %esi
	jmp	.LBB28_5
.LBB28_8:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	cmpl	$54, %eax
	jne	.LBB28_9
# BB#11:
	addq	$8232, %rsp             # imm = 0x2028
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc18NilwithexpressionsEv # TAILCALL
.LBB28_9:
	movl	$.L.str.42, %edi
	movl	$1063, %esi             # imm = 0x427
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	jmp	.LBB28_10
.LBB28_4:
	movl	$.L.str.41, %esi
.LBB28_5:
	xorl	%eax, %eax
	movl	%r14d, %edx
	movl	%ebx, %ecx
	callq	sprintf
	leaq	32(%rsp), %rdi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	_ZN2kc10WEVariableEPNS_7impl_IDE
	movq	%rax, %rbp
	testb	%r13b, %r13b
	movq	8(%rsp), %rax           # 8-byte Reload
	jne	.LBB28_7
# BB#6:
	movq	%rax, %rdi
	callq	_ZN2kc19f_listelementphylumEPNS_7impl_IDE
.LBB28_7:
	movq	%rax, 8(%rbp)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rax
	movq	%rax, 24(%rbp)
	movl	8(%rcx), %eax
	movl	%eax, 16(%rbp)
	movq	%r15, 8(%r12)
	decl	%ebx
	movzbl	%r13b, %ecx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%r14d, %esi
	movl	%ebx, %edx
	callq	_ZN2kc25t_pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsEiib
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc19ConswithexpressionsEPNS_19impl_withexpressionEPNS_20impl_withexpressionsE
	movq	24(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movl	16(%rbp), %ecx
	movl	%ecx, 16(%rax)
.LBB28_10:
	addq	$8232, %rsp             # imm = 0x2028
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end28:
	.size	_ZN2kc25t_pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsEiib, .Lfunc_end28-_ZN2kc25t_pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsEiib
	.cfi_endproc

	.globl	_ZN2kc27pf_gen_foreachwith_listvarsEPNS_19impl_idCexpressionsE
	.p2align	4, 0x90
	.type	_ZN2kc27pf_gen_foreachwith_listvarsEPNS_19impl_idCexpressionsE,@function
_ZN2kc27pf_gen_foreachwith_listvarsEPNS_19impl_idCexpressionsE: # @_ZN2kc27pf_gen_foreachwith_listvarsEPNS_19impl_idCexpressionsE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi186:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi187:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi188:
	.cfi_def_cfa_offset 32
.Lcfi189:
	.cfi_offset %rbx, -24
.Lcfi190:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	_ZZN2kc27pf_gen_foreachwith_listvarsEPNS_19impl_idCexpressionsEE16nrof_foreach_occ(%rip), %ebp
	incl	%ebp
	movl	%ebp, _ZZN2kc27pf_gen_foreachwith_listvarsEPNS_19impl_idCexpressionsEE16nrof_foreach_occ(%rip)
	callq	_ZNK2kc18impl_abstract_list6lengthEv
	movl	$1, %ecx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movl	%eax, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	_ZN2kc25t_pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsEiib # TAILCALL
.Lfunc_end29:
	.size	_ZN2kc27pf_gen_foreachwith_listvarsEPNS_19impl_idCexpressionsE, .Lfunc_end29-_ZN2kc27pf_gen_foreachwith_listvarsEPNS_19impl_idCexpressionsE
	.cfi_endproc

	.globl	_ZN2kc28f_check_build_qualifier_tailEPNS_33impl_ac_class_qualifier_help_listE
	.p2align	4, 0x90
	.type	_ZN2kc28f_check_build_qualifier_tailEPNS_33impl_ac_class_qualifier_help_listE,@function
_ZN2kc28f_check_build_qualifier_tailEPNS_33impl_ac_class_qualifier_help_listE: # @_ZN2kc28f_check_build_qualifier_tailEPNS_33impl_ac_class_qualifier_help_listE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi191:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi192:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi193:
	.cfi_def_cfa_offset 32
.Lcfi194:
	.cfi_offset %rbx, -24
.Lcfi195:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$271, %eax              # imm = 0x10F
	jne	.LBB30_5
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$262, %eax              # imm = 0x106
	jne	.LBB30_5
# BB#2:
	movq	8(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$269, %eax              # imm = 0x10D
	jne	.LBB30_5
# BB#3:
	movq	8(%rbx), %rax
	movq	8(%rax), %rax
	movq	24(%rax), %rbx
	movq	16(%rax), %rdi
	movl	8(%rax), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %r14
	movq	8(%rbx), %rsi
	movl	$.L.str.43, %edi
	movl	$.L.str.44, %edx
	callq	_ZN2kc9Problem3SEPKcS1_S1_
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB30_4
.LBB30_5:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$271, %eax              # imm = 0x10F
	jne	.LBB30_7
# BB#6:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$255, %eax
	jne	.LBB30_7
# BB#9:
	movq	8(%rbx), %rax
	movq	16(%rbx), %rdi
	movq	8(%rax), %rbx
	callq	_ZN2kc28f_check_build_qualifier_tailEPNS_33impl_ac_class_qualifier_help_listE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc27Consac_class_qualifier_listEPNS_7impl_IDEPNS_28impl_ac_class_qualifier_listE # TAILCALL
.LBB30_7:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$270, %eax              # imm = 0x10E
	jne	.LBB30_8
.LBB30_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc26Nilac_class_qualifier_listEv # TAILCALL
.LBB30_8:
	movl	$.L.str.45, %edi
	movl	$1090, %esi             # imm = 0x442
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end30:
	.size	_ZN2kc28f_check_build_qualifier_tailEPNS_33impl_ac_class_qualifier_help_listE, .Lfunc_end30-_ZN2kc28f_check_build_qualifier_tailEPNS_33impl_ac_class_qualifier_help_listE
	.cfi_endproc

	.globl	_ZN2kc23f_check_build_qualifierEPNS_33impl_ac_class_qualifier_help_listERPNS_25impl_ac_direct_declaratorE
	.p2align	4, 0x90
	.type	_ZN2kc23f_check_build_qualifierEPNS_33impl_ac_class_qualifier_help_listERPNS_25impl_ac_direct_declaratorE,@function
_ZN2kc23f_check_build_qualifierEPNS_33impl_ac_class_qualifier_help_listERPNS_25impl_ac_direct_declaratorE: # @_ZN2kc23f_check_build_qualifierEPNS_33impl_ac_class_qualifier_help_listERPNS_25impl_ac_direct_declaratorE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi196:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi197:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi198:
	.cfi_def_cfa_offset 32
.Lcfi199:
	.cfi_offset %rbx, -24
.Lcfi200:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$271, %eax              # imm = 0x10F
	jne	.LBB31_1
# BB#2:
	movq	8(%rbx), %rax
	movq	16(%rbx), %rdi
	movq	%rax, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc28f_check_build_qualifier_tailEPNS_33impl_ac_class_qualifier_help_listE # TAILCALL
.LBB31_1:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end31:
	.size	_ZN2kc23f_check_build_qualifierEPNS_33impl_ac_class_qualifier_help_listERPNS_25impl_ac_direct_declaratorE, .Lfunc_end31-_ZN2kc23f_check_build_qualifierEPNS_33impl_ac_class_qualifier_help_listERPNS_25impl_ac_direct_declaratorE
	.cfi_endproc

	.globl	_ZN2kc10subst_nameEPNS_7impl_IDEPNS_20impl_casestring__StrES3_
	.p2align	4, 0x90
	.type	_ZN2kc10subst_nameEPNS_7impl_IDEPNS_20impl_casestring__StrES3_,@function
_ZN2kc10subst_nameEPNS_7impl_IDEPNS_20impl_casestring__StrES3_: # @_ZN2kc10subst_nameEPNS_7impl_IDEPNS_20impl_casestring__StrES3_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi201:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi202:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi203:
	.cfi_def_cfa_offset 32
.Lcfi204:
	.cfi_offset %rbx, -32
.Lcfi205:
	.cfi_offset %r14, -24
.Lcfi206:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB32_4
# BB#1:
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$6, %eax
	jne	.LBB32_4
# BB#2:
	movq	40(%rbx), %rax
	movq	40(%rax), %rdi
	movq	%r15, %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	je	.LBB32_5
# BB#3:
	movq	%r14, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	32(%rbx), %rcx
	movq	%rcx, 32(%rax)
	movl	24(%rbx), %ecx
	movl	%ecx, 24(%rax)
	movq	%rax, %rbx
	jmp	.LBB32_5
.LBB32_4:
	movl	$.L.str.46, %edi
	movl	$1129, %esi             # imm = 0x469
	movl	$.L.str.6, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%ebx, %ebx
.LBB32_5:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end32:
	.size	_ZN2kc10subst_nameEPNS_7impl_IDEPNS_20impl_casestring__StrES3_, .Lfunc_end32-_ZN2kc10subst_nameEPNS_7impl_IDEPNS_20impl_casestring__StrES3_
	.cfi_endproc

	.type	pg_lineno,@object       # @pg_lineno
	.data
	.globl	pg_lineno
	.p2align	2
pg_lineno:
	.long	1                       # 0x1
	.size	pg_lineno, 4

	.type	pg_column,@object       # @pg_column
	.bss
	.globl	pg_column
	.p2align	2
pg_column:
	.long	0                       # 0x0
	.size	pg_column, 4

	.type	pg_charpos,@object      # @pg_charpos
	.globl	pg_charpos
	.p2align	2
pg_charpos:
	.long	0                       # 0x0
	.size	pg_charpos, 4

	.type	pg_filename,@object     # @pg_filename
	.globl	pg_filename
	.p2align	3
pg_filename:
	.quad	0
	.size	pg_filename, 8

	.type	pg_no_of_arguments,@object # @pg_no_of_arguments
	.globl	pg_no_of_arguments
	.p2align	2
pg_no_of_arguments:
	.long	0                       # 0x0
	.size	pg_no_of_arguments, 4

	.type	Thephylumdeclarations,@object # @Thephylumdeclarations
	.globl	Thephylumdeclarations
	.p2align	3
Thephylumdeclarations:
	.quad	0
	.size	Thephylumdeclarations, 8

	.type	Therwdeclarations,@object # @Therwdeclarations
	.globl	Therwdeclarations
	.p2align	3
Therwdeclarations:
	.quad	0
	.size	Therwdeclarations, 8

	.type	Thefndeclarations,@object # @Thefndeclarations
	.globl	Thefndeclarations
	.p2align	3
Thefndeclarations:
	.quad	0
	.size	Thefndeclarations, 8

	.type	Thefnfiles,@object      # @Thefnfiles
	.globl	Thefnfiles
	.p2align	3
Thefnfiles:
	.quad	0
	.size	Thefnfiles, 8

	.type	Theincludefiles,@object # @Theincludefiles
	.globl	Theincludefiles
	.p2align	3
Theincludefiles:
	.quad	0
	.size	Theincludefiles, 8

	.type	Theunparsedeclarations,@object # @Theunparsedeclarations
	.globl	Theunparsedeclarations
	.p2align	3
Theunparsedeclarations:
	.quad	0
	.size	Theunparsedeclarations, 8

	.type	Theargsnumbers,@object  # @Theargsnumbers
	.globl	Theargsnumbers
	.p2align	3
Theargsnumbers:
	.quad	0
	.size	Theargsnumbers, 8

	.type	Theuviewnames,@object   # @Theuviewnames
	.globl	Theuviewnames
	.p2align	3
Theuviewnames:
	.quad	0
	.size	Theuviewnames, 8

	.type	Therviewnames,@object   # @Therviewnames
	.globl	Therviewnames
	.p2align	3
Therviewnames:
	.quad	0
	.size	Therviewnames, 8

	.type	Thestorageclasses,@object # @Thestorageclasses
	.globl	Thestorageclasses
	.p2align	3
Thestorageclasses:
	.quad	0
	.size	Thestorageclasses, 8

	.type	Thelanguages,@object    # @Thelanguages
	.globl	Thelanguages
	.p2align	3
Thelanguages:
	.quad	0
	.size	Thelanguages, 8

	.type	Thebaseclasses,@object  # @Thebaseclasses
	.globl	Thebaseclasses
	.p2align	3
Thebaseclasses:
	.quad	0
	.size	Thebaseclasses, 8

	.type	pg_uviewshavebeendefined,@object # @pg_uviewshavebeendefined
	.globl	pg_uviewshavebeendefined
pg_uviewshavebeendefined:
	.byte	0                       # 0x0
	.size	pg_uviewshavebeendefined, 1

	.type	pg_rviewshavebeendefined,@object # @pg_rviewshavebeendefined
	.globl	pg_rviewshavebeendefined
pg_rviewshavebeendefined:
	.byte	0                       # 0x0
	.size	pg_rviewshavebeendefined, 1

	.type	pg_storageclasseshavebeendefined,@object # @pg_storageclasseshavebeendefined
	.globl	pg_storageclasseshavebeendefined
pg_storageclasseshavebeendefined:
	.byte	0                       # 0x0
	.size	pg_storageclasseshavebeendefined, 1

	.type	pg_languageshavebeendefined,@object # @pg_languageshavebeendefined
	.globl	pg_languageshavebeendefined
pg_languageshavebeendefined:
	.byte	0                       # 0x0
	.size	pg_languageshavebeendefined, 1

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"storage option mismatch  ( declared as "
	.size	.L.str, 40

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	")  for phylum"
	.size	.L.str.1, 14

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"production block mismatch: trying to extend phylum"
	.size	.L.str.2, 51

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"production block mismatch: trying to predefine phylum"
	.size	.L.str.3, 54

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"production block mismatch: trying to redefine list phylum"
	.size	.L.str.4, 58

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"mergephylumdeclarations"
	.size	.L.str.5, 24

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/kimwitu++/parse.cc"
	.size	.L.str.6, 82

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Nil"
	.size	.L.str.7, 4

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"f_strofID"
	.size	.L.str.9, 10

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"expected user-defined phylum instead of user-defined function:"
	.size	.L.str.10, 63

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"expected user-defined phylum instead of user-defined rewrite view:"
	.size	.L.str.11, 67

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"expected user-defined phylum instead of predefined rewrite view:"
	.size	.L.str.12, 65

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"expected user-defined phylum instead of user-defined unparse view:"
	.size	.L.str.13, 67

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"expected user-defined phylum instead of predefined unparse view:"
	.size	.L.str.14, 65

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"expected user-defined phylum instead of user-defined storage class:"
	.size	.L.str.15, 68

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"expected user-defined phylum instead of predefined storage class:"
	.size	.L.str.16, 66

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"expected user-defined phylum instead of user-defined operator:"
	.size	.L.str.17, 63

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"expected user-defined phylum instead of predefined operator:"
	.size	.L.str.18, 61

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"expected user-defined phylum instead of predefined phylum:"
	.size	.L.str.19, 59

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"undefined phylum:"
	.size	.L.str.20, 18

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"f_lookupuserdecl"
	.size	.L.str.21, 17

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"f_lookupdecl"
	.size	.L.str.22, 13

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"insert_in_argsnumbers"
	.size	.L.str.23, 22

	.type	_ZL21language_text_nr_used,@object # @_ZL21language_text_nr_used
	.local	_ZL21language_text_nr_used
	.comm	_ZL21language_text_nr_used,1,1
	.type	_ZL16language_text_nr,@object # @_ZL16language_text_nr
	.local	_ZL16language_text_nr
	.comm	_ZL16language_text_nr,8,8
	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"f_ID_of_declarator"
	.size	.L.str.24, 19

	.type	_ZZN2kc21f_ID_of_fn_declaratorEPNS_18impl_ac_declaratorEPNS_12impl_fnclassEE7dtor_nr,@object # @_ZZN2kc21f_ID_of_fn_declaratorEPNS_18impl_ac_declaratorEPNS_12impl_fnclassEE7dtor_nr
	.local	_ZZN2kc21f_ID_of_fn_declaratorEPNS_18impl_ac_declaratorEPNS_12impl_fnclassEE7dtor_nr
	.comm	_ZZN2kc21f_ID_of_fn_declaratorEPNS_18impl_ac_declaratorEPNS_12impl_fnclassEE7dtor_nr,4,4
	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"destructor_%d"
	.size	.L.str.25, 14

	.type	_ZZN2kc21f_ID_of_fn_declaratorEPNS_18impl_ac_declaratorEPNS_12impl_fnclassEE7ctor_nr,@object # @_ZZN2kc21f_ID_of_fn_declaratorEPNS_18impl_ac_declaratorEPNS_12impl_fnclassEE7ctor_nr
	.local	_ZZN2kc21f_ID_of_fn_declaratorEPNS_18impl_ac_declaratorEPNS_12impl_fnclassEE7ctor_nr
	.comm	_ZZN2kc21f_ID_of_fn_declaratorEPNS_18impl_ac_declaratorEPNS_12impl_fnclassEE7ctor_nr,4,4
	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"constructor_%d"
	.size	.L.str.26, 15

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"f_ID_of_fn_declarator"
	.size	.L.str.27, 22

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"f_fnclass_info"
	.size	.L.str.28, 15

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"f_member_class_info"
	.size	.L.str.29, 20

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"f_static_in_ac_decl_specs"
	.size	.L.str.30, 26

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"f_ID_of_ac_declaration_specifiers"
	.size	.L.str.31, 34

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"no pattern grouping () allowed in"
	.size	.L.str.32, 34

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"context."
	.size	.L.str.33, 9

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"check_no_patternchaingroup_in_patternchain"
	.size	.L.str.34, 43

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"check_no_patternchaingroup_in_patternchains"
	.size	.L.str.35, 44

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"check_no_patternchaingroup_or_pattern_in_patternchain"
	.size	.L.str.36, 54

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"check_no_patternchaingroup_or_pattern_in_patternchains"
	.size	.L.str.37, 55

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"syn_patternchains_fileline"
	.size	.L.str.38, 27

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"syn_patternchain_fileline"
	.size	.L.str.39, 26

	.type	_ZZN2kc23pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsEE16nrof_foreach_occ,@object # @_ZZN2kc23pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsEE16nrof_foreach_occ
	.local	_ZZN2kc23pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsEE16nrof_foreach_occ
	.comm	_ZZN2kc23pf_gen_foreachwith_varsEPNS_19impl_idCexpressionsEE16nrof_foreach_occ,4,4
	.type	_ZZN2kc27pf_gen_foreachwith_listvarsEPNS_19impl_idCexpressionsEE16nrof_foreach_occ,@object # @_ZZN2kc27pf_gen_foreachwith_listvarsEPNS_19impl_idCexpressionsEE16nrof_foreach_occ
	.local	_ZZN2kc27pf_gen_foreachwith_listvarsEPNS_19impl_idCexpressionsEE16nrof_foreach_occ
	.comm	_ZZN2kc27pf_gen_foreachwith_listvarsEPNS_19impl_idCexpressionsEE16nrof_foreach_occ,4,4
	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"kc_fe_withlistvar_%d_%d"
	.size	.L.str.40, 24

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"kc_fe_withvar_%d_%d"
	.size	.L.str.41, 20

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"t_pf_gen_foreachwith_vars"
	.size	.L.str.42, 26

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"operator "
	.size	.L.str.43, 10

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"is not a type name"
	.size	.L.str.44, 19

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"f_check_build_qualifier_tail"
	.size	.L.str.45, 29

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"subst_name"
	.size	.L.str.46, 11

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"f_static_in_ac_decl_spec"
	.size	.L.str.47, 25

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"f_static_in_ac_stor_class"
	.size	.L.str.48, 26

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"f_ID_of_ac_declaration_specifier"
	.size	.L.str.49, 33

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"f_ID_of_ac_type_specifier"
	.size	.L.str.50, 26

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"basic_string::_M_construct null not valid"
	.size	.L.str.51, 42

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"basic_string::append"
	.size	.L.str.52, 21


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
