	.text
	.file	"btCapsuleShape.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	1056964608              # float 0.5
	.text
	.globl	_ZN14btCapsuleShapeC2Eff
	.p2align	4, 0x90
	.type	_ZN14btCapsuleShapeC2Eff,@function
_ZN14btCapsuleShapeC2Eff:               # @_ZN14btCapsuleShapeC2Eff
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rdi, %rbx
	callq	_ZN21btConvexInternalShapeC2Ev
	movq	$_ZTV14btCapsuleShape+16, (%rbx)
	movl	$10, 8(%rbx)
	movl	$1, 64(%rbx)
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	.LCPI0_0(%rip), %xmm1
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 40(%rbx)
	movss	%xmm1, 44(%rbx)
	movss	%xmm0, 48(%rbx)
	movl	$0, 52(%rbx)
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN14btCapsuleShapeC2Eff, .Lfunc_end0-_ZN14btCapsuleShapeC2Eff
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	1065353216              # float 1
.LCPI2_1:
	.long	953267991               # float 9.99999974E-5
.LCPI2_2:
	.long	3713928043              # float -9.99999984E+17
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_3:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZNK14btCapsuleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK14btCapsuleShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK14btCapsuleShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK14btCapsuleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 16
	subq	$144, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 160
.Lcfi5:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rsi), %xmm3           # xmm3 = mem[0],zero
	pshufd	$229, %xmm3, %xmm1      # xmm1 = xmm3[1,1,2,3]
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movdqa	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm2, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	.LCPI2_0(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movss	.LCPI2_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB2_2
# BB#1:
	xorps	%xmm2, %xmm2
	movaps	%xmm4, %xmm3
	jmp	.LBB2_5
.LBB2_2:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB2_4
# BB#3:                                 # %call.sqrt
	movss	%xmm2, 12(%rsp)         # 4-byte Spill
	movdqa	%xmm3, 48(%rsp)         # 16-byte Spill
	callq	sqrtf
	movss	.LCPI2_0(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movdqa	48(%rsp), %xmm3         # 16-byte Reload
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB2_4:                                # %.split
	divss	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm4, %xmm3
.LBB2_5:
	movaps	%xmm3, 48(%rsp)         # 16-byte Spill
	movss	%xmm2, 12(%rsp)         # 4-byte Spill
	movslq	64(%rbx), %rax
	leal	2(%rax), %ecx
	movslq	%ecx, %rcx
	imulq	$1431655766, %rcx, %rdx # imm = 0x55555556
	movq	%rdx, %rsi
	shrq	$63, %rsi
	shrq	$32, %rdx
	addl	%esi, %edx
	leal	(%rdx,%rdx,2), %edx
	subl	%edx, %ecx
	movslq	%ecx, %rcx
	movss	40(%rbx,%rcx,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	40(%rbx,%rax,4), %ecx
	movl	%ecx, 16(%rsp,%rax,4)
	movsd	24(%rbx), %xmm4         # xmm4 = mem[0],zero
	mulps	%xmm3, %xmm4
	movss	32(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	movaps	%xmm1, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	mulps	%xmm0, %xmm4
	movaps	%xmm1, 112(%rsp)        # 16-byte Spill
	mulss	%xmm1, %xmm3
	addps	16(%rsp), %xmm4
	movaps	%xmm4, 64(%rsp)         # 16-byte Spill
	addss	24(%rsp), %xmm3
	movss	%xmm3, 80(%rsp)         # 4-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movss	80(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm1
	mulss	%xmm0, %xmm1
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm3, %xmm0
	movaps	64(%rsp), %xmm4         # 16-byte Reload
	subps	%xmm0, %xmm4
	subss	%xmm1, %xmm5
	movaps	%xmm4, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm3, %xmm1
	movaps	%xmm4, 64(%rsp)         # 16-byte Spill
	mulss	%xmm4, %xmm1
	movaps	%xmm3, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	movaps	%xmm4, 80(%rsp)         # 16-byte Spill
	mulss	%xmm4, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm2, %xmm4
	mulss	%xmm5, %xmm4
	addss	%xmm0, %xmm4
	movss	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm4
	ja	.LBB2_6
# BB#7:
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	xorps	%xmm1, %xmm1
	jmp	.LBB2_8
.LBB2_6:
	movss	%xmm5, %xmm1            # xmm1 = xmm5[0],xmm1[1,2,3]
.LBB2_8:
	movaps	%xmm1, 128(%rsp)        # 16-byte Spill
	maxss	%xmm0, %xmm4
	movss	%xmm4, 44(%rsp)         # 4-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movslq	64(%rbx), %rax
	movss	40(%rbx,%rax,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	xorps	.LCPI2_3(%rip), %xmm0
	movss	%xmm0, 16(%rsp,%rax,4)
	movsd	24(%rbx), %xmm0         # xmm0 = mem[0],zero
	mulps	%xmm3, %xmm0
	movss	32(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	movaps	96(%rsp), %xmm4         # 16-byte Reload
	mulps	%xmm0, %xmm4
	movaps	112(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm1, %xmm0
	addps	16(%rsp), %xmm4
	movaps	%xmm4, 96(%rsp)         # 16-byte Spill
	addss	24(%rsp), %xmm0
	movaps	%xmm0, 112(%rsp)        # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm1
	mulss	%xmm0, %xmm1
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movaps	48(%rsp), %xmm2         # 16-byte Reload
	mulps	%xmm2, %xmm0
	movaps	96(%rsp), %xmm5         # 16-byte Reload
	subps	%xmm0, %xmm5
	movaps	112(%rsp), %xmm4        # 16-byte Reload
	subss	%xmm1, %xmm4
	movaps	%xmm5, %xmm0
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	mulss	%xmm0, %xmm2
	movaps	%xmm2, %xmm5
	movaps	80(%rsp), %xmm2         # 16-byte Reload
	mulss	%xmm1, %xmm2
	movaps	%xmm4, %xmm1
	addss	%xmm5, %xmm2
	mulss	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	ucomiss	44(%rsp), %xmm3         # 4-byte Folded Reload
	ja	.LBB2_9
# BB#10:
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	movaps	128(%rsp), %xmm1        # 16-byte Reload
	jmp	.LBB2_11
.LBB2_9:
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movaps	%xmm2, %xmm1
.LBB2_11:
	addq	$144, %rsp
	popq	%rbx
	retq
.Lfunc_end2:
	.size	_ZNK14btCapsuleShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end2-_ZNK14btCapsuleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	3713928043              # float -9.99999984E+17
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI3_2:
	.zero	16
	.text
	.globl	_ZNK14btCapsuleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK14btCapsuleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK14btCapsuleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK14btCapsuleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 40
	subq	$104, %rsp
.Lcfi10:
	.cfi_def_cfa_offset 144
.Lcfi11:
	.cfi_offset %rbx, -40
.Lcfi12:
	.cfi_offset %r12, -32
.Lcfi13:
	.cfi_offset %r14, -24
.Lcfi14:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	testl	%ecx, %ecx
	jle	.LBB3_8
# BB#1:                                 # %.lr.ph
	movl	64(%rbx), %eax
	leal	2(%rax), %edx
	movslq	%edx, %rdx
	imulq	$1431655766, %rdx, %rdx # imm = 0x55555556
	movq	%rdx, %rsi
	shrq	$63, %rsi
	shrq	$32, %rdx
	addl	%esi, %edx
	leal	(%rdx,%rdx,2), %edx
	negl	%edx
	leal	2(%rax,%rdx), %edx
	movslq	%edx, %rdx
	movss	40(%rbx,%rdx,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movl	%ecx, %r12d
	movaps	%xmm1, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	addq	$8, %r14
	addq	$8, %r15
	decq	%r12
	xorps	%xmm0, %xmm0
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	movaps	%xmm2, 48(%rsp)         # 16-byte Spill
	jmp	.LBB3_2
	.p2align	4, 0x90
.LBB3_7:                                # %._crit_edge164
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	64(%rbx), %eax
	addq	$16, %r14
	addq	$16, %r15
	decq	%r12
	movaps	64(%rsp), %xmm1         # 16-byte Reload
	movaps	48(%rsp), %xmm2         # 16-byte Reload
	xorps	%xmm0, %xmm0
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	cltq
	movaps	%xmm0, 16(%rsp)
	movl	40(%rbx,%rax,4), %ecx
	movl	%ecx, 16(%rsp,%rax,4)
	movsd	-8(%r15), %xmm0         # xmm0 = mem[0],zero
	movsd	24(%rbx), %xmm3         # xmm3 = mem[0],zero
	mulps	%xmm0, %xmm3
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	32(%rbx), %xmm0
	mulps	%xmm2, %xmm3
	mulss	%xmm1, %xmm0
	addps	16(%rsp), %xmm3
	movaps	%xmm3, 32(%rsp)         # 16-byte Spill
	addss	24(%rsp), %xmm0
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movss	12(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm5         # 16-byte Reload
	movq	-8(%r15), %xmm1         # xmm1 = mem[0],zero
	pshufd	$229, %xmm1, %xmm2      # xmm2 = xmm1[1,1,2,3]
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	movss	(%r15), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	subps	%xmm3, %xmm5
	movaps	%xmm5, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	subss	%xmm0, %xmm6
	movaps	%xmm1, %xmm0
	mulss	%xmm5, %xmm0
	mulss	%xmm2, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm4, %xmm0
	mulss	%xmm6, %xmm0
	addss	%xmm3, %xmm0
	movss	.LCPI3_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	jbe	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	xorps	%xmm1, %xmm1
	movss	%xmm6, %xmm1            # xmm1 = xmm6[0],xmm1[1,2,3]
	movlps	%xmm5, -8(%r14)
	movlps	%xmm1, (%r14)
	movsd	-8(%r15), %xmm1         # xmm1 = mem[0],zero
	movss	(%r15), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	movss	%xmm2, 12(%rsp)         # 4-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movslq	64(%rbx), %rax
	movss	40(%rbx,%rax,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	xorps	.LCPI3_1(%rip), %xmm0
	movss	%xmm0, 16(%rsp,%rax,4)
	movsd	24(%rbx), %xmm0         # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	mulss	32(%rbx), %xmm4
	mulps	48(%rsp), %xmm0         # 16-byte Folded Reload
	mulss	64(%rsp), %xmm4         # 16-byte Folded Reload
	addps	16(%rsp), %xmm0
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	addss	24(%rsp), %xmm4
	movss	%xmm4, 32(%rsp)         # 4-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	80(%rsp), %xmm5         # 16-byte Reload
	movss	32(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movsd	-8(%r15), %xmm1         # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	movss	(%r15), %xmm3           # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	subps	%xmm2, %xmm5
	movaps	%xmm5, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	subss	%xmm0, %xmm4
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	mulss	%xmm5, %xmm1
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	mulss	%xmm4, %xmm3
	addss	%xmm0, %xmm3
	ucomiss	12(%rsp), %xmm3         # 4-byte Folded Reload
	jbe	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm5, -8(%r14)
	movlps	%xmm0, (%r14)
.LBB3_6:                                #   in Loop: Header=BB3_2 Depth=1
	testq	%r12, %r12
	jne	.LBB3_7
.LBB3_8:                                # %._crit_edge
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_ZNK14btCapsuleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end3-_ZNK14btCapsuleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1025758986              # float 0.0399999991
.LCPI4_1:
	.long	1034594986              # float 0.0833333284
	.text
	.globl	_ZNK14btCapsuleShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK14btCapsuleShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK14btCapsuleShape21calculateLocalInertiaEfR9btVector3: # @_ZNK14btCapsuleShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	movslq	64(%rdi), %r8
	leal	2(%r8), %ecx
	movslq	%ecx, %rcx
	imulq	$1431655766, %rcx, %rdx # imm = 0x55555556
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$32, %rdx
	addl	%eax, %edx
	leal	(%rdx,%rdx,2), %eax
	subl	%eax, %ecx
	movslq	%ecx, %rax
	movl	40(%rdi,%rax,4), %eax
	movl	%eax, -16(%rsp)
	movl	%eax, -12(%rsp)
	movl	%eax, -8(%rsp)
	movl	$0, -4(%rsp)
	movss	40(%rdi,%r8,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	-16(%rsp,%r8,4), %xmm1
	movss	%xmm1, -16(%rsp,%r8,4)
	movss	.LCPI4_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movss	-16(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm1
	addss	%xmm1, %xmm1
	movss	-12(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm3
	addss	%xmm3, %xmm3
	addss	-8(%rsp), %xmm2
	addss	%xmm2, %xmm2
	mulss	%xmm1, %xmm1
	mulss	%xmm3, %xmm3
	mulss	%xmm2, %xmm2
	mulss	.LCPI4_1(%rip), %xmm0
	movaps	%xmm3, %xmm4
	addss	%xmm2, %xmm4
	mulss	%xmm0, %xmm4
	movss	%xmm4, (%rsi)
	addss	%xmm1, %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm2, 4(%rsi)
	addss	%xmm1, %xmm3
	mulss	%xmm0, %xmm3
	movss	%xmm3, 8(%rsi)
	retq
.Lfunc_end4:
	.size	_ZNK14btCapsuleShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end4-_ZNK14btCapsuleShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	1056964608              # float 0.5
	.text
	.globl	_ZN15btCapsuleShapeXC2Eff
	.p2align	4, 0x90
	.type	_ZN15btCapsuleShapeXC2Eff,@function
_ZN15btCapsuleShapeXC2Eff:              # @_ZN15btCapsuleShapeXC2Eff
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -16
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rdi, %rbx
	callq	_ZN21btConvexInternalShapeC2Ev
	movl	$10, 8(%rbx)
	movq	$_ZTV15btCapsuleShapeX+16, (%rbx)
	movl	$0, 64(%rbx)
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI5_0(%rip), %xmm0
	movss	%xmm0, 40(%rbx)
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 44(%rbx)
	movss	%xmm0, 48(%rbx)
	movl	$0, 52(%rbx)
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end5:
	.size	_ZN15btCapsuleShapeXC2Eff, .Lfunc_end5-_ZN15btCapsuleShapeXC2Eff
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_0:
	.long	1056964608              # float 0.5
	.text
	.globl	_ZN15btCapsuleShapeZC2Eff
	.p2align	4, 0x90
	.type	_ZN15btCapsuleShapeZC2Eff,@function
_ZN15btCapsuleShapeZC2Eff:              # @_ZN15btCapsuleShapeZC2Eff
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -16
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movq	%rdi, %rbx
	callq	_ZN21btConvexInternalShapeC2Ev
	movl	$10, 8(%rbx)
	movq	$_ZTV15btCapsuleShapeZ+16, (%rbx)
	movl	$2, 64(%rbx)
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI6_0(%rip), %xmm0
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 40(%rbx)
	movss	%xmm1, 44(%rbx)
	movss	%xmm0, 48(%rbx)
	movl	$0, 52(%rbx)
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end6:
	.size	_ZN15btCapsuleShapeZC2Eff, .Lfunc_end6-_ZN15btCapsuleShapeZC2Eff
	.cfi_endproc

	.section	.text._ZN14btCapsuleShapeD0Ev,"axG",@progbits,_ZN14btCapsuleShapeD0Ev,comdat
	.weak	_ZN14btCapsuleShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN14btCapsuleShapeD0Ev,@function
_ZN14btCapsuleShapeD0Ev:                # @_ZN14btCapsuleShapeD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -24
.Lcfi25:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp1:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB7_2:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB7_4:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN14btCapsuleShapeD0Ev, .Lfunc_end7-_ZN14btCapsuleShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end7-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.text._ZNK14btCapsuleShape7getAabbERK11btTransformR9btVector3S4_,"axG",@progbits,_ZNK14btCapsuleShape7getAabbERK11btTransformR9btVector3S4_,comdat
	.weak	_ZNK14btCapsuleShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK14btCapsuleShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK14btCapsuleShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK14btCapsuleShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 40
	subq	$56, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 96
.Lcfi31:
	.cfi_offset %rbx, -40
.Lcfi32:
	.cfi_offset %r12, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movslq	64(%rbx), %rax
	leal	2(%rax), %ecx
	movslq	%ecx, %rcx
	imulq	$1431655766, %rcx, %rdx # imm = 0x55555556
	movq	%rdx, %rsi
	shrq	$63, %rsi
	shrq	$32, %rdx
	addl	%esi, %edx
	leal	(%rdx,%rdx,2), %edx
	subl	%edx, %ecx
	movslq	%ecx, %rcx
	movl	40(%rbx,%rcx,4), %ecx
	movl	%ecx, (%rsp)
	movl	%ecx, 4(%rsp)
	movl	%ecx, 8(%rsp)
	movl	$0, 12(%rsp)
	movd	%ecx, %xmm0
	addss	40(%rbx,%rax,4), %xmm0
	movss	%xmm0, (%rsp,%rax,4)
	movq	(%rbx), %rax
	callq	*88(%rax)
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	addss	(%rsp), %xmm1
	movss	%xmm1, (%rsp)
	movaps	%xmm1, %xmm5
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	addss	4(%rsp), %xmm1
	movss	%xmm1, 4(%rsp)
	movaps	%xmm1, %xmm10
	addss	8(%rsp), %xmm0
	movss	16(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%r12), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	movaps	.LCPI8_0(%rip), %xmm8   # xmm8 = [nan,nan,nan,nan]
	andps	%xmm8, %xmm4
	movss	20(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	andps	%xmm8, %xmm3
	movss	24(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	andps	%xmm8, %xmm2
	movss	32(%r12), %xmm6         # xmm6 = mem[0],zero,zero,zero
	andps	%xmm8, %xmm6
	movss	36(%r12), %xmm7         # xmm7 = mem[0],zero,zero,zero
	andps	%xmm8, %xmm7
	movss	40(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	andps	%xmm8, %xmm1
	movsd	48(%r12), %xmm9         # xmm9 = mem[0],zero
	movss	56(%r12), %xmm8         # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm6
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm4, %xmm5
	movaps	%xmm10, %xmm4
	mulss	%xmm4, %xmm7
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm5, %xmm4
	mulss	%xmm0, %xmm1
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	addps	%xmm4, %xmm0
	addss	%xmm6, %xmm7
	addss	%xmm7, %xmm1
	movaps	%xmm9, %xmm2
	subps	%xmm0, %xmm2
	movaps	%xmm8, %xmm3
	subss	%xmm1, %xmm3
	xorps	%xmm4, %xmm4
	xorps	%xmm6, %xmm6
	movss	%xmm3, %xmm6            # xmm6 = xmm3[0],xmm6[1,2,3]
	movlps	%xmm2, (%r15)
	movlps	%xmm6, 8(%r15)
	addps	%xmm9, %xmm0
	addss	%xmm8, %xmm1
	movss	%xmm1, %xmm4            # xmm4 = xmm1[0],xmm4[1,2,3]
	movlps	%xmm0, (%r14)
	movlps	%xmm4, 8(%r14)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	_ZNK14btCapsuleShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end8-_ZNK14btCapsuleShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.text._ZN14btCapsuleShape15setLocalScalingERK9btVector3,"axG",@progbits,_ZN14btCapsuleShape15setLocalScalingERK9btVector3,comdat
	.weak	_ZN14btCapsuleShape15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN14btCapsuleShape15setLocalScalingERK9btVector3,@function
_ZN14btCapsuleShape15setLocalScalingERK9btVector3: # @_ZN14btCapsuleShape15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	subq	$56, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 80
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*88(%rax)
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movsd	40(%rbx), %xmm2         # xmm2 = mem[0],zero
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	unpcklps	16(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	addps	%xmm1, %xmm2
	movss	48(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movsd	24(%rbx), %xmm0         # xmm0 = mem[0],zero
	divps	%xmm0, %xmm2
	movaps	%xmm2, 16(%rsp)         # 16-byte Spill
	divss	32(%rbx), %xmm1
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	movsd	24(%rbx), %xmm0         # xmm0 = mem[0],zero
	mulps	16(%rsp), %xmm0         # 16-byte Folded Reload
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	32(%rbx), %xmm1
	subps	32(%rsp), %xmm0         # 16-byte Folded Reload
	subss	12(%rsp), %xmm1         # 4-byte Folded Reload
	movaps	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm0, 40(%rbx)
	movlps	%xmm1, 48(%rbx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	_ZN14btCapsuleShape15setLocalScalingERK9btVector3, .Lfunc_end9-_ZN14btCapsuleShape15setLocalScalingERK9btVector3
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape15getLocalScalingEv,"axG",@progbits,_ZNK21btConvexInternalShape15getLocalScalingEv,comdat
	.weak	_ZNK21btConvexInternalShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape15getLocalScalingEv,@function
_ZNK21btConvexInternalShape15getLocalScalingEv: # @_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	24(%rdi), %rax
	retq
.Lfunc_end10:
	.size	_ZNK21btConvexInternalShape15getLocalScalingEv, .Lfunc_end10-_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK14btCapsuleShape7getNameEv,"axG",@progbits,_ZNK14btCapsuleShape7getNameEv,comdat
	.weak	_ZNK14btCapsuleShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK14btCapsuleShape7getNameEv,@function
_ZNK14btCapsuleShape7getNameEv:         # @_ZNK14btCapsuleShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end11:
	.size	_ZNK14btCapsuleShape7getNameEv, .Lfunc_end11-_ZNK14btCapsuleShape7getNameEv
	.cfi_endproc

	.section	.text._ZN21btConvexInternalShape9setMarginEf,"axG",@progbits,_ZN21btConvexInternalShape9setMarginEf,comdat
	.weak	_ZN21btConvexInternalShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN21btConvexInternalShape9setMarginEf,@function
_ZN21btConvexInternalShape9setMarginEf: # @_ZN21btConvexInternalShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 56(%rdi)
	retq
.Lfunc_end12:
	.size	_ZN21btConvexInternalShape9setMarginEf, .Lfunc_end12-_ZN21btConvexInternalShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape9getMarginEv,"axG",@progbits,_ZNK21btConvexInternalShape9getMarginEv,comdat
	.weak	_ZNK21btConvexInternalShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape9getMarginEv,@function
_ZNK21btConvexInternalShape9getMarginEv: # @_ZNK21btConvexInternalShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	56(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end13:
	.size	_ZNK21btConvexInternalShape9getMarginEv, .Lfunc_end13-_ZNK21btConvexInternalShape9getMarginEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,"axG",@progbits,_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,comdat
	.weak	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end14:
	.size	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end14-_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,"axG",@progbits,_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,comdat
	.weak	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end15:
	.size	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end15-_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.section	.text._ZN15btCapsuleShapeXD0Ev,"axG",@progbits,_ZN15btCapsuleShapeXD0Ev,comdat
	.weak	_ZN15btCapsuleShapeXD0Ev
	.p2align	4, 0x90
	.type	_ZN15btCapsuleShapeXD0Ev,@function
_ZN15btCapsuleShapeXD0Ev:               # @_ZN15btCapsuleShapeXD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -24
.Lcfi44:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp6:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp7:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB16_2:
.Ltmp8:
	movq	%rax, %r14
.Ltmp9:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp10:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB16_4:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN15btCapsuleShapeXD0Ev, .Lfunc_end16-_ZN15btCapsuleShapeXD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp9-.Ltmp7           #   Call between .Ltmp7 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end16-.Ltmp10    #   Call between .Ltmp10 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK15btCapsuleShapeX7getNameEv,"axG",@progbits,_ZNK15btCapsuleShapeX7getNameEv,comdat
	.weak	_ZNK15btCapsuleShapeX7getNameEv
	.p2align	4, 0x90
	.type	_ZNK15btCapsuleShapeX7getNameEv,@function
_ZNK15btCapsuleShapeX7getNameEv:        # @_ZNK15btCapsuleShapeX7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str.1, %eax
	retq
.Lfunc_end17:
	.size	_ZNK15btCapsuleShapeX7getNameEv, .Lfunc_end17-_ZNK15btCapsuleShapeX7getNameEv
	.cfi_endproc

	.section	.text._ZN15btCapsuleShapeZD0Ev,"axG",@progbits,_ZN15btCapsuleShapeZD0Ev,comdat
	.weak	_ZN15btCapsuleShapeZD0Ev
	.p2align	4, 0x90
	.type	_ZN15btCapsuleShapeZD0Ev,@function
_ZN15btCapsuleShapeZD0Ev:               # @_ZN15btCapsuleShapeZD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi47:
	.cfi_def_cfa_offset 32
.Lcfi48:
	.cfi_offset %rbx, -24
.Lcfi49:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp12:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp13:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB18_2:
.Ltmp14:
	movq	%rax, %r14
.Ltmp15:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp16:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB18_4:
.Ltmp17:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN15btCapsuleShapeZD0Ev, .Lfunc_end18-_ZN15btCapsuleShapeZD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp12-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin2   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp15-.Ltmp13         #   Call between .Ltmp13 and .Ltmp15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin2   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end18-.Ltmp16    #   Call between .Ltmp16 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK15btCapsuleShapeZ7getNameEv,"axG",@progbits,_ZNK15btCapsuleShapeZ7getNameEv,comdat
	.weak	_ZNK15btCapsuleShapeZ7getNameEv
	.p2align	4, 0x90
	.type	_ZNK15btCapsuleShapeZ7getNameEv,@function
_ZNK15btCapsuleShapeZ7getNameEv:        # @_ZNK15btCapsuleShapeZ7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str.2, %eax
	retq
.Lfunc_end19:
	.size	_ZNK15btCapsuleShapeZ7getNameEv, .Lfunc_end19-_ZNK15btCapsuleShapeZ7getNameEv
	.cfi_endproc

	.type	_ZTV14btCapsuleShape,@object # @_ZTV14btCapsuleShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV14btCapsuleShape
	.p2align	3
_ZTV14btCapsuleShape:
	.quad	0
	.quad	_ZTI14btCapsuleShape
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN14btCapsuleShapeD0Ev
	.quad	_ZNK14btCapsuleShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN14btCapsuleShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK14btCapsuleShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK14btCapsuleShape7getNameEv
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK14btCapsuleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK14btCapsuleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.size	_ZTV14btCapsuleShape, 160

	.type	_ZTV15btCapsuleShapeX,@object # @_ZTV15btCapsuleShapeX
	.section	.rodata._ZTV15btCapsuleShapeX,"aG",@progbits,_ZTV15btCapsuleShapeX,comdat
	.weak	_ZTV15btCapsuleShapeX
	.p2align	3
_ZTV15btCapsuleShapeX:
	.quad	0
	.quad	_ZTI15btCapsuleShapeX
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN15btCapsuleShapeXD0Ev
	.quad	_ZNK14btCapsuleShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN14btCapsuleShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK14btCapsuleShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK15btCapsuleShapeX7getNameEv
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK14btCapsuleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK14btCapsuleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.size	_ZTV15btCapsuleShapeX, 160

	.type	_ZTV15btCapsuleShapeZ,@object # @_ZTV15btCapsuleShapeZ
	.section	.rodata._ZTV15btCapsuleShapeZ,"aG",@progbits,_ZTV15btCapsuleShapeZ,comdat
	.weak	_ZTV15btCapsuleShapeZ
	.p2align	3
_ZTV15btCapsuleShapeZ:
	.quad	0
	.quad	_ZTI15btCapsuleShapeZ
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN15btCapsuleShapeZD0Ev
	.quad	_ZNK14btCapsuleShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN14btCapsuleShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK14btCapsuleShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK15btCapsuleShapeZ7getNameEv
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK14btCapsuleShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK14btCapsuleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.size	_ZTV15btCapsuleShapeZ, 160

	.type	_ZTS14btCapsuleShape,@object # @_ZTS14btCapsuleShape
	.section	.rodata,"a",@progbits
	.globl	_ZTS14btCapsuleShape
	.p2align	4
_ZTS14btCapsuleShape:
	.asciz	"14btCapsuleShape"
	.size	_ZTS14btCapsuleShape, 17

	.type	_ZTI14btCapsuleShape,@object # @_ZTI14btCapsuleShape
	.globl	_ZTI14btCapsuleShape
	.p2align	4
_ZTI14btCapsuleShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14btCapsuleShape
	.quad	_ZTI21btConvexInternalShape
	.size	_ZTI14btCapsuleShape, 24

	.type	_ZTS15btCapsuleShapeX,@object # @_ZTS15btCapsuleShapeX
	.section	.rodata._ZTS15btCapsuleShapeX,"aG",@progbits,_ZTS15btCapsuleShapeX,comdat
	.weak	_ZTS15btCapsuleShapeX
	.p2align	4
_ZTS15btCapsuleShapeX:
	.asciz	"15btCapsuleShapeX"
	.size	_ZTS15btCapsuleShapeX, 18

	.type	_ZTI15btCapsuleShapeX,@object # @_ZTI15btCapsuleShapeX
	.section	.rodata._ZTI15btCapsuleShapeX,"aG",@progbits,_ZTI15btCapsuleShapeX,comdat
	.weak	_ZTI15btCapsuleShapeX
	.p2align	4
_ZTI15btCapsuleShapeX:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15btCapsuleShapeX
	.quad	_ZTI14btCapsuleShape
	.size	_ZTI15btCapsuleShapeX, 24

	.type	_ZTS15btCapsuleShapeZ,@object # @_ZTS15btCapsuleShapeZ
	.section	.rodata._ZTS15btCapsuleShapeZ,"aG",@progbits,_ZTS15btCapsuleShapeZ,comdat
	.weak	_ZTS15btCapsuleShapeZ
	.p2align	4
_ZTS15btCapsuleShapeZ:
	.asciz	"15btCapsuleShapeZ"
	.size	_ZTS15btCapsuleShapeZ, 18

	.type	_ZTI15btCapsuleShapeZ,@object # @_ZTI15btCapsuleShapeZ
	.section	.rodata._ZTI15btCapsuleShapeZ,"aG",@progbits,_ZTI15btCapsuleShapeZ,comdat
	.weak	_ZTI15btCapsuleShapeZ
	.p2align	4
_ZTI15btCapsuleShapeZ:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15btCapsuleShapeZ
	.quad	_ZTI14btCapsuleShape
	.size	_ZTI15btCapsuleShapeZ, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"CapsuleShape"
	.size	.L.str, 13

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"CapsuleX"
	.size	.L.str.1, 9

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"CapsuleZ"
	.size	.L.str.2, 9


	.globl	_ZN14btCapsuleShapeC1Eff
	.type	_ZN14btCapsuleShapeC1Eff,@function
_ZN14btCapsuleShapeC1Eff = _ZN14btCapsuleShapeC2Eff
	.globl	_ZN15btCapsuleShapeXC1Eff
	.type	_ZN15btCapsuleShapeXC1Eff,@function
_ZN15btCapsuleShapeXC1Eff = _ZN15btCapsuleShapeXC2Eff
	.globl	_ZN15btCapsuleShapeZC1Eff
	.type	_ZN15btCapsuleShapeZC1Eff,@function
_ZN15btCapsuleShapeZC1Eff = _ZN15btCapsuleShapeZC2Eff
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
