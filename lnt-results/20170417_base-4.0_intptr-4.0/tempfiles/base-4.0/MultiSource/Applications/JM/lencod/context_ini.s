	.text
	.file	"context_ini.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182418800017408     # double 1
.LCPI0_1:
	.quad	4599094494223104511     # double 0.3010299956639812
	.text
	.globl	create_context_memory
	.p2align	4, 0x90
	.type	create_context_memory,@function
create_context_memory:                  # @create_context_memory
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	movl	15352(%rax), %eax
	movq	input(%rip), %rdx
	cmpl	$1, 264(%rdx)
	movl	%eax, %ecx
	jne	.LBB0_2
# BB#1:
	movl	268(%rdx), %ecx
.LBB0_2:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movl	%ecx, num_mb_per_slice(%rip)
	leal	-1(%rax,%rcx), %eax
	cltd
	idivl	%ecx
	movl	%eax, number_of_slices(%rip)
	movl	$24, %edi
	callq	malloc
	movq	%rax, initialized(%rip)
	testq	%rax, %rax
	jne	.LBB0_4
# BB#3:
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB0_4:
	movl	$24, %edi
	callq	malloc
	movq	%rax, model_number(%rip)
	testq	%rax, %rax
	jne	.LBB0_5
# BB#66:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB0_5:                                # %.preheader43.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_6:                                # %.preheader43
                                        # =>This Inner Loop Header: Depth=1
	movl	$32, %edi
	callq	malloc
	movq	initialized(%rip), %rcx
	movq	%rax, (%rcx,%r14)
	testq	%rax, %rax
	jne	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=1
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB0_8:                                #   in Loop: Header=BB0_6 Depth=1
	movl	$32, %edi
	callq	malloc
	movq	model_number(%rip), %rcx
	movq	%rax, (%rcx,%r14)
	testq	%rax, %rax
	jne	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_6 Depth=1
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB0_10:                               # %.preheader42.preheader
                                        #   in Loop: Header=BB0_6 Depth=1
	movslq	number_of_slices(%rip), %rbx
	movq	%rbx, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	initialized(%rip), %rcx
	movq	(%rcx,%r14), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	jne	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_6 Depth=1
	movl	$.L.str, %edi
	callq	no_mem_exit
	movl	number_of_slices(%rip), %ebx
.LBB0_12:                               #   in Loop: Header=BB0_6 Depth=1
	movslq	%ebx, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	model_number(%rip), %rcx
	movq	(%rcx,%r14), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	jne	.LBB0_14
# BB#13:                                #   in Loop: Header=BB0_6 Depth=1
	movl	$.L.str.1, %edi
	callq	no_mem_exit
	movl	number_of_slices(%rip), %ebx
.LBB0_14:                               # %.preheader42.161
                                        #   in Loop: Header=BB0_6 Depth=1
	movslq	%ebx, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	initialized(%rip), %rcx
	movq	(%rcx,%r14), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	jne	.LBB0_16
# BB#15:                                #   in Loop: Header=BB0_6 Depth=1
	movl	$.L.str, %edi
	callq	no_mem_exit
	movl	number_of_slices(%rip), %ebx
.LBB0_16:                               #   in Loop: Header=BB0_6 Depth=1
	movslq	%ebx, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	model_number(%rip), %rcx
	movq	(%rcx,%r14), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	jne	.LBB0_18
# BB#17:                                #   in Loop: Header=BB0_6 Depth=1
	movl	$.L.str.1, %edi
	callq	no_mem_exit
	movl	number_of_slices(%rip), %ebx
.LBB0_18:                               # %.preheader42.262
                                        #   in Loop: Header=BB0_6 Depth=1
	movslq	%ebx, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	initialized(%rip), %rcx
	movq	(%rcx,%r14), %rcx
	movq	%rax, 16(%rcx)
	testq	%rax, %rax
	jne	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_6 Depth=1
	movl	$.L.str, %edi
	callq	no_mem_exit
	movl	number_of_slices(%rip), %ebx
.LBB0_20:                               #   in Loop: Header=BB0_6 Depth=1
	movslq	%ebx, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	model_number(%rip), %rcx
	movq	(%rcx,%r14), %rcx
	movq	%rax, 16(%rcx)
	testq	%rax, %rax
	jne	.LBB0_22
# BB#21:                                #   in Loop: Header=BB0_6 Depth=1
	movl	$.L.str.1, %edi
	callq	no_mem_exit
	movl	number_of_slices(%rip), %ebx
.LBB0_22:                               # %.preheader42.363
                                        #   in Loop: Header=BB0_6 Depth=1
	movslq	%ebx, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	initialized(%rip), %rcx
	movq	(%rcx,%r14), %rcx
	movq	%rax, 24(%rcx)
	testq	%rax, %rax
	jne	.LBB0_24
# BB#23:                                #   in Loop: Header=BB0_6 Depth=1
	movl	$.L.str, %edi
	callq	no_mem_exit
	movl	number_of_slices(%rip), %ebx
.LBB0_24:                               #   in Loop: Header=BB0_6 Depth=1
	movslq	%ebx, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	model_number(%rip), %rcx
	movq	(%rcx,%r14), %rcx
	movq	%rax, 24(%rcx)
	testq	%rax, %rax
	jne	.LBB0_26
# BB#25:                                #   in Loop: Header=BB0_6 Depth=1
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB0_26:                               #   in Loop: Header=BB0_6 Depth=1
	addq	$8, %r14
	cmpq	$24, %r14
	jne	.LBB0_6
# BB#27:                                # %.preheader40
	movq	initialized(%rip), %rax
	movl	number_of_slices(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB0_39
# BB#28:                                # %.lr.ph
	movq	(%rax), %rcx
	movq	(%rcx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_29:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rdx,%rsi,4)
	incq	%rsi
	movslq	number_of_slices(%rip), %rcx
	cmpq	%rcx, %rsi
	jl	.LBB0_29
# BB#30:                                # %._crit_edge
	testl	%ecx, %ecx
	jle	.LBB0_39
# BB#31:                                # %.lr.ph.1
	movq	(%rax), %rcx
	movq	8(%rcx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_32:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rdx,%rsi,4)
	incq	%rsi
	movslq	number_of_slices(%rip), %rcx
	cmpq	%rcx, %rsi
	jl	.LBB0_32
# BB#33:                                # %._crit_edge.1
	testl	%ecx, %ecx
	jle	.LBB0_39
# BB#34:                                # %.lr.ph.2
	movq	(%rax), %rcx
	movq	16(%rcx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_35:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rdx,%rsi,4)
	incq	%rsi
	movslq	number_of_slices(%rip), %rcx
	cmpq	%rcx, %rsi
	jl	.LBB0_35
# BB#36:                                # %._crit_edge.2
	testl	%ecx, %ecx
	jle	.LBB0_39
# BB#37:                                # %.lr.ph.3
	movq	(%rax), %rcx
	movq	24(%rcx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_38:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rdx,%rsi,4)
	incq	%rsi
	movslq	number_of_slices(%rip), %rcx
	cmpq	%rcx, %rsi
	jl	.LBB0_38
.LBB0_39:                               # %._crit_edge.3
	testl	%ecx, %ecx
	jle	.LBB0_51
# BB#40:                                # %.lr.ph.184
	movq	8(%rax), %rcx
	movq	(%rcx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_41:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rdx,%rsi,4)
	incq	%rsi
	movslq	number_of_slices(%rip), %rcx
	cmpq	%rcx, %rsi
	jl	.LBB0_41
# BB#42:                                # %._crit_edge.187
	testl	%ecx, %ecx
	jle	.LBB0_51
# BB#43:                                # %.lr.ph.1.1
	movq	8(%rax), %rcx
	movq	8(%rcx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_44:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rdx,%rsi,4)
	incq	%rsi
	movslq	number_of_slices(%rip), %rcx
	cmpq	%rcx, %rsi
	jl	.LBB0_44
# BB#45:                                # %._crit_edge.1.1
	testl	%ecx, %ecx
	jle	.LBB0_51
# BB#46:                                # %.lr.ph.2.1
	movq	8(%rax), %rcx
	movq	16(%rcx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_47:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rdx,%rsi,4)
	incq	%rsi
	movslq	number_of_slices(%rip), %rcx
	cmpq	%rcx, %rsi
	jl	.LBB0_47
# BB#48:                                # %._crit_edge.2.1
	testl	%ecx, %ecx
	jle	.LBB0_51
# BB#49:                                # %.lr.ph.3.1
	movq	8(%rax), %rcx
	movq	24(%rcx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_50:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rdx,%rsi,4)
	incq	%rsi
	movslq	number_of_slices(%rip), %rcx
	cmpq	%rcx, %rsi
	jl	.LBB0_50
.LBB0_51:                               # %._crit_edge.3.1
	testl	%ecx, %ecx
	jle	.LBB0_63
# BB#52:                                # %.lr.ph.288
	movq	16(%rax), %rcx
	movq	(%rcx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_53:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rcx,%rdx,4)
	incq	%rdx
	movslq	number_of_slices(%rip), %rsi
	cmpq	%rsi, %rdx
	jl	.LBB0_53
# BB#54:                                # %._crit_edge.291
	testl	%esi, %esi
	jle	.LBB0_63
# BB#55:                                # %.lr.ph.1.2
	movq	16(%rax), %rcx
	movq	8(%rcx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_56:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rcx,%rdx,4)
	incq	%rdx
	movslq	number_of_slices(%rip), %rsi
	cmpq	%rsi, %rdx
	jl	.LBB0_56
# BB#57:                                # %._crit_edge.1.2
	testl	%esi, %esi
	jle	.LBB0_63
# BB#58:                                # %.lr.ph.2.2
	movq	16(%rax), %rcx
	movq	16(%rcx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_59:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rcx,%rdx,4)
	incq	%rdx
	movslq	number_of_slices(%rip), %rsi
	cmpq	%rsi, %rdx
	jl	.LBB0_59
# BB#60:                                # %._crit_edge.2.2
	testl	%esi, %esi
	jle	.LBB0_63
# BB#61:                                # %.lr.ph.3.2
	movq	16(%rax), %rax
	movq	24(%rax), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_62:                               # =>This Inner Loop Header: Depth=1
	movl	$0, (%rax,%rcx,4)
	incq	%rcx
	movslq	number_of_slices(%rip), %rdx
	cmpq	%rdx, %rcx
	jl	.LBB0_62
.LBB0_63:                               # %._crit_edge.3.2
	movq	$-512, %r14             # imm = 0xFE00
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_64:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	subsd	probability+1016(%rbx), %xmm0
	movsd	%xmm0, probability+512(%r14)
	callq	log10
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm1, %xmm0
	movsd	%xmm0, entropy+512(%r14)
	movsd	probability+1016(%rbx), %xmm0 # xmm0 = mem[0],zero
	callq	log10
	divsd	.LCPI0_1(%rip), %xmm0
	movsd	%xmm0, entropy+1016(%rbx)
	addq	$-8, %rbx
	addq	$8, %r14
	jne	.LBB0_64
# BB#65:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	create_context_memory, .Lfunc_end0-create_context_memory
	.cfi_endproc

	.globl	free_context_memory
	.p2align	4, 0x90
	.type	free_context_memory,@function
free_context_memory:                    # @free_context_memory
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	movq	initialized(%rip), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	callq	free
	movq	model_number(%rip), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	callq	free
	movq	initialized(%rip), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	model_number(%rip), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	initialized(%rip), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rdi
	callq	free
	movq	model_number(%rip), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rdi
	callq	free
	movq	initialized(%rip), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rdi
	callq	free
	movq	model_number(%rip), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rdi
	callq	free
	movq	initialized(%rip), %rax
	movq	(%rax), %rdi
	callq	free
	movq	model_number(%rip), %rax
	movq	(%rax), %rdi
	callq	free
	movq	initialized(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	callq	free
	movq	model_number(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	callq	free
	movq	initialized(%rip), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	model_number(%rip), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	initialized(%rip), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rdi
	callq	free
	movq	model_number(%rip), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rdi
	callq	free
	movq	initialized(%rip), %rax
	movq	8(%rax), %rax
	movq	24(%rax), %rdi
	callq	free
	movq	model_number(%rip), %rax
	movq	8(%rax), %rax
	movq	24(%rax), %rdi
	callq	free
	movq	initialized(%rip), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	model_number(%rip), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	initialized(%rip), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rdi
	callq	free
	movq	model_number(%rip), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rdi
	callq	free
	movq	initialized(%rip), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	model_number(%rip), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	initialized(%rip), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rdi
	callq	free
	movq	model_number(%rip), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rdi
	callq	free
	movq	initialized(%rip), %rax
	movq	16(%rax), %rax
	movq	24(%rax), %rdi
	callq	free
	movq	model_number(%rip), %rax
	movq	16(%rax), %rax
	movq	24(%rax), %rdi
	callq	free
	movq	initialized(%rip), %rax
	movq	16(%rax), %rdi
	callq	free
	movq	model_number(%rip), %rax
	movq	16(%rax), %rdi
	callq	free
	movq	initialized(%rip), %rdi
	callq	free
	movq	model_number(%rip), %rdi
	popq	%rax
	jmp	free                    # TAILCALL
.Lfunc_end1:
	.size	free_context_memory, .Lfunc_end1-free_context_memory
	.cfi_endproc

	.globl	SetCtxModelNumber
	.p2align	4, 0x90
	.type	SetCtxModelNumber,@function
SetCtxModelNumber:                      # @SetCtxModelNumber
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rcx
	movslq	15312(%rcx), %r10
	movslq	20(%rcx), %rsi
	movq	14216(%rcx), %rax
	movl	12(%rax), %eax
	cltd
	idivl	num_mb_per_slice(%rip)
	xorl	%edx, %edx
	cmpq	$2, %rsi
	je	.LBB2_8
# BB#1:
	movq	input(%rip), %rdi
	cmpl	$0, 5092(%rdi)
	je	.LBB2_2
# BB#3:
	movq	initialized(%rip), %rdi
	movq	(%rdi,%r10,8), %rdi
	movq	(%rdi,%rsi,8), %r9
	movslq	%eax, %r8
	cmpl	$0, (%r9,%r8,4)
	jne	.LBB2_7
# BB#4:
	testl	%eax, %eax
	je	.LBB2_8
# BB#5:
	cmpl	$0, -4(%r9,%r8,4)
	je	.LBB2_8
# BB#6:
	decq	%r8
.LBB2_7:
	movq	model_number(%rip), %rax
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%rsi,8), %rax
	movl	(%rax,%r8,4), %edx
.LBB2_8:
	movl	%edx, 15384(%rcx)
	retq
.LBB2_2:
	movl	5096(%rdi), %edx
	movl	%edx, 15384(%rcx)
	retq
.Lfunc_end2:
	.size	SetCtxModelNumber, .Lfunc_end2-SetCtxModelNumber
	.cfi_endproc

	.globl	init_contexts
	.p2align	4, 0x90
	.type	init_contexts,@function
init_contexts:                          # @init_contexts
	.cfi_startproc
# BB#0:                                 # %.preheader206
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 80
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movq	14216(%rax), %rcx
	movq	32(%rcx), %r14
	movq	40(%rcx), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	xorl	%ebp, %ebp
	movq	%r14, %rbx
	jmp	.LBB3_1
	.p2align	4, 0x90
.LBB3_8:                                # %._crit_edge
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	img(%rip), %rax
	addq	$16, %rbx
	addq	$8, %rbp
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movslq	15384(%rax), %rcx
	cmpl	$2, 20(%rax)
	jne	.LBB3_3
# BB#2:                                 #   in Loop: Header=BB3_1 Depth=1
	imulq	$264, %rcx, %rax        # imm = 0x108
	leaq	INIT_MB_TYPE_I(%rbp,%rax), %rsi
	jmp	.LBB3_4
	.p2align	4, 0x90
.LBB3_3:                                #   in Loop: Header=BB3_1 Depth=1
	imulq	$264, %rcx, %rax        # imm = 0x108
	leaq	INIT_MB_TYPE_P(%rbp,%rax), %rsi
.LBB3_4:                                #   in Loop: Header=BB3_1 Depth=1
	movq	%rbx, %rdi
	callq	biari_init_context
	cmpq	$80, %rbp
	jne	.LBB3_8
# BB#5:                                 # %.preheader206.1341.preheader
	leaq	176(%r14), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_6:                                # %.preheader206.1341
                                        # =>This Inner Loop Header: Depth=1
	movq	img(%rip), %rax
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	jne	.LBB3_7
# BB#190:                               #   in Loop: Header=BB3_6 Depth=1
	imulq	$264, %rax, %rax        # imm = 0x108
	leaq	INIT_MB_TYPE_I+88(%rbp,%rax), %rsi
	jmp	.LBB3_191
	.p2align	4, 0x90
.LBB3_7:                                #   in Loop: Header=BB3_6 Depth=1
	imulq	$264, %rax, %rax        # imm = 0x108
	leaq	INIT_MB_TYPE_P+88(%rbp,%rax), %rsi
.LBB3_191:                              #   in Loop: Header=BB3_6 Depth=1
	movq	%rbx, %rdi
	callq	biari_init_context
	addq	$8, %rbp
	addq	$16, %rbx
	cmpq	$88, %rbp
	jne	.LBB3_6
# BB#192:                               # %.preheader206.2342.preheader
	leaq	352(%r14), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_193:                              # %.preheader206.2342
                                        # =>This Inner Loop Header: Depth=1
	movq	img(%rip), %rax
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	jne	.LBB3_194
# BB#195:                               #   in Loop: Header=BB3_193 Depth=1
	imulq	$264, %rax, %rax        # imm = 0x108
	leaq	INIT_MB_TYPE_I+176(%rbp,%rax), %rsi
	jmp	.LBB3_196
	.p2align	4, 0x90
.LBB3_194:                              #   in Loop: Header=BB3_193 Depth=1
	imulq	$264, %rax, %rax        # imm = 0x108
	leaq	INIT_MB_TYPE_P+176(%rbp,%rax), %rsi
.LBB3_196:                              #   in Loop: Header=BB3_193 Depth=1
	movq	%rbx, %rdi
	callq	biari_init_context
	addq	$8, %rbp
	addq	$16, %rbx
	cmpq	$88, %rbp
	jne	.LBB3_193
# BB#197:                               # %.preheader204.preheader343.preheader
	leaq	528(%r14), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_9:                                # %.preheader204.preheader343
                                        # =>This Inner Loop Header: Depth=1
	movq	img(%rip), %rcx
	movslq	15384(%rcx), %rax
	cmpl	$2, 20(%rcx)
	jne	.LBB3_11
# BB#10:                                #   in Loop: Header=BB3_9 Depth=1
	leaq	(%rax,%rax,8), %rax
	shlq	$4, %rax
	leaq	INIT_B8_TYPE_I(%rbp,%rax), %rsi
	jmp	.LBB3_12
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_9 Depth=1
	leaq	(%rax,%rax,8), %rax
	shlq	$4, %rax
	leaq	INIT_B8_TYPE_P(%rbp,%rax), %rsi
.LBB3_12:                               #   in Loop: Header=BB3_9 Depth=1
	movq	%rbx, %rdi
	callq	biari_init_context
	addq	$16, %rbx
	addq	$8, %rbp
	cmpq	$72, %rbp
	jne	.LBB3_9
# BB#13:                                # %.preheader204.1333.preheader
	leaq	672(%r14), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_14:                               # %.preheader204.1333
                                        # =>This Inner Loop Header: Depth=1
	movq	img(%rip), %rax
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,8), %rax
	jne	.LBB3_15
# BB#187:                               #   in Loop: Header=BB3_14 Depth=1
	shlq	$4, %rax
	leaq	INIT_B8_TYPE_I+72(%rbp,%rax), %rsi
	jmp	.LBB3_188
	.p2align	4, 0x90
.LBB3_15:                               #   in Loop: Header=BB3_14 Depth=1
	shlq	$4, %rax
	leaq	INIT_B8_TYPE_P+72(%rbp,%rax), %rsi
.LBB3_188:                              #   in Loop: Header=BB3_14 Depth=1
	movq	%rbx, %rdi
	callq	biari_init_context
	addq	$8, %rbp
	addq	$16, %rbx
	cmpq	$72, %rbp
	jne	.LBB3_14
# BB#189:                               # %.preheader202.preheader334.preheader
	leaq	816(%r14), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_16:                               # %.preheader202.preheader334
                                        # =>This Inner Loop Header: Depth=1
	movq	img(%rip), %rcx
	movslq	15384(%rcx), %rax
	cmpl	$2, 20(%rcx)
	jne	.LBB3_18
# BB#17:                                #   in Loop: Header=BB3_16 Depth=1
	leaq	(%rax,%rax,4), %rax
	shlq	$5, %rax
	leaq	INIT_MV_RES_I(%rbp,%rax), %rsi
	jmp	.LBB3_19
	.p2align	4, 0x90
.LBB3_18:                               #   in Loop: Header=BB3_16 Depth=1
	leaq	(%rax,%rax,4), %rax
	shlq	$5, %rax
	leaq	INIT_MV_RES_P(%rbp,%rax), %rsi
.LBB3_19:                               #   in Loop: Header=BB3_16 Depth=1
	movq	%rbx, %rdi
	callq	biari_init_context
	addq	$16, %rbx
	addq	$8, %rbp
	cmpq	$80, %rbp
	jne	.LBB3_16
# BB#20:                                # %.preheader202.1325.preheader
	leaq	976(%r14), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_21:                               # %.preheader202.1325
                                        # =>This Inner Loop Header: Depth=1
	movq	img(%rip), %rax
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	jne	.LBB3_22
# BB#184:                               #   in Loop: Header=BB3_21 Depth=1
	shlq	$5, %rax
	leaq	INIT_MV_RES_I+80(%rbp,%rax), %rsi
	jmp	.LBB3_185
	.p2align	4, 0x90
.LBB3_22:                               #   in Loop: Header=BB3_21 Depth=1
	shlq	$5, %rax
	leaq	INIT_MV_RES_P+80(%rbp,%rax), %rsi
.LBB3_185:                              #   in Loop: Header=BB3_21 Depth=1
	movq	%rbx, %rdi
	callq	biari_init_context
	addq	$8, %rbp
	addq	$16, %rbx
	cmpq	$80, %rbp
	jne	.LBB3_21
# BB#186:                               # %.preheader200.preheader
	leaq	1136(%r14), %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_23:                               # %.preheader200
                                        # =>This Inner Loop Header: Depth=1
	movq	img(%rip), %rcx
	movslq	15384(%rcx), %rax
	cmpl	$2, 20(%rcx)
	jne	.LBB3_25
# BB#24:                                #   in Loop: Header=BB3_23 Depth=1
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	leaq	INIT_REF_NO_I(%rbx,%rax), %rsi
	jmp	.LBB3_26
	.p2align	4, 0x90
.LBB3_25:                               #   in Loop: Header=BB3_23 Depth=1
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	leaq	INIT_REF_NO_P(%rbx,%rax), %rsi
.LBB3_26:                               #   in Loop: Header=BB3_23 Depth=1
	movq	%r12, %rdi
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	16(%r12), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,2), %rax
	jne	.LBB3_27
# BB#170:                               #   in Loop: Header=BB3_23 Depth=1
	shlq	$5, %rax
	leaq	INIT_REF_NO_I+8(%rbx,%rax), %rsi
	jmp	.LBB3_171
	.p2align	4, 0x90
.LBB3_27:                               #   in Loop: Header=BB3_23 Depth=1
	shlq	$5, %rax
	leaq	INIT_REF_NO_P+8(%rbx,%rax), %rsi
.LBB3_171:                              #   in Loop: Header=BB3_23 Depth=1
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	32(%r12), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,2), %rax
	jne	.LBB3_172
# BB#173:                               #   in Loop: Header=BB3_23 Depth=1
	shlq	$5, %rax
	leaq	INIT_REF_NO_I+16(%rbx,%rax), %rsi
	jmp	.LBB3_174
	.p2align	4, 0x90
.LBB3_172:                              #   in Loop: Header=BB3_23 Depth=1
	shlq	$5, %rax
	leaq	INIT_REF_NO_P+16(%rbx,%rax), %rsi
.LBB3_174:                              #   in Loop: Header=BB3_23 Depth=1
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	48(%r12), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,2), %rax
	jne	.LBB3_175
# BB#176:                               #   in Loop: Header=BB3_23 Depth=1
	shlq	$5, %rax
	leaq	INIT_REF_NO_I+24(%rbx,%rax), %rsi
	jmp	.LBB3_177
	.p2align	4, 0x90
.LBB3_175:                              #   in Loop: Header=BB3_23 Depth=1
	shlq	$5, %rax
	leaq	INIT_REF_NO_P+24(%rbx,%rax), %rsi
.LBB3_177:                              #   in Loop: Header=BB3_23 Depth=1
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	64(%r12), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,2), %rax
	jne	.LBB3_178
# BB#179:                               #   in Loop: Header=BB3_23 Depth=1
	shlq	$5, %rax
	leaq	INIT_REF_NO_I+32(%rbx,%rax), %rsi
	jmp	.LBB3_180
	.p2align	4, 0x90
.LBB3_178:                              #   in Loop: Header=BB3_23 Depth=1
	shlq	$5, %rax
	leaq	INIT_REF_NO_P+32(%rbx,%rax), %rsi
.LBB3_180:                              #   in Loop: Header=BB3_23 Depth=1
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	80(%r12), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,2), %rax
	jne	.LBB3_181
# BB#182:                               #   in Loop: Header=BB3_23 Depth=1
	shlq	$5, %rax
	leaq	INIT_REF_NO_I+40(%rbx,%rax), %rsi
	jmp	.LBB3_183
	.p2align	4, 0x90
.LBB3_181:                              #   in Loop: Header=BB3_23 Depth=1
	shlq	$5, %rax
	leaq	INIT_REF_NO_P+40(%rbx,%rax), %rsi
.LBB3_183:                              #   in Loop: Header=BB3_23 Depth=1
	callq	biari_init_context
	addq	$96, %r12
	addq	$48, %rbx
	cmpq	$96, %rbx
	jne	.LBB3_23
# BB#28:                                # %.preheader199.preheader
	movq	img(%rip), %rcx
	leaq	1328(%r14), %rdi
	movslq	15384(%rcx), %rax
	cmpl	$2, 20(%rcx)
	jne	.LBB3_30
# BB#29:
	shlq	$5, %rax
	leaq	INIT_DELTA_QP_I(%rax), %rsi
	jmp	.LBB3_31
.LBB3_30:
	shlq	$5, %rax
	leaq	INIT_DELTA_QP_P(%rax), %rsi
.LBB3_31:                               # %.preheader199.1309
	callq	biari_init_context
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	img(%rip), %rax
	leaq	1344(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	jne	.LBB3_32
# BB#161:
	shlq	$5, %rax
	leaq	INIT_DELTA_QP_I+8(%rax), %rsi
	jmp	.LBB3_162
.LBB3_32:
	shlq	$5, %rax
	leaq	INIT_DELTA_QP_P+8(%rax), %rsi
.LBB3_162:                              # %.preheader199.2310
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	1360(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	jne	.LBB3_163
# BB#164:
	shlq	$5, %rax
	leaq	INIT_DELTA_QP_I+16(%rax), %rsi
	jmp	.LBB3_165
.LBB3_163:
	shlq	$5, %rax
	leaq	INIT_DELTA_QP_P+16(%rax), %rsi
.LBB3_165:                              # %.preheader199.3311
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	1376(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	jne	.LBB3_166
# BB#167:
	shlq	$5, %rax
	leaq	INIT_DELTA_QP_I+24(%rax), %rsi
	jmp	.LBB3_168
.LBB3_166:
	shlq	$5, %rax
	leaq	INIT_DELTA_QP_P+24(%rax), %rsi
.LBB3_168:                              # %.preheader198.preheader312
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	1392(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	jne	.LBB3_33
# BB#169:
	shlq	$5, %rax
	leaq	INIT_MB_AFF_I(%rax), %rsi
	jmp	.LBB3_34
.LBB3_33:
	shlq	$5, %rax
	leaq	INIT_MB_AFF_P(%rax), %rsi
.LBB3_34:                               # %.preheader198.1302
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	1408(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	jne	.LBB3_35
# BB#152:
	shlq	$5, %rax
	leaq	INIT_MB_AFF_I+8(%rax), %rsi
	jmp	.LBB3_153
.LBB3_35:
	shlq	$5, %rax
	leaq	INIT_MB_AFF_P+8(%rax), %rsi
.LBB3_153:                              # %.preheader198.2303
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	1424(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	jne	.LBB3_154
# BB#155:
	shlq	$5, %rax
	leaq	INIT_MB_AFF_I+16(%rax), %rsi
	jmp	.LBB3_156
.LBB3_154:
	shlq	$5, %rax
	leaq	INIT_MB_AFF_P+16(%rax), %rsi
.LBB3_156:                              # %.preheader198.3304
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	1440(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	jne	.LBB3_157
# BB#158:
	shlq	$5, %rax
	leaq	INIT_MB_AFF_I+24(%rax), %rsi
	jmp	.LBB3_159
.LBB3_157:
	shlq	$5, %rax
	leaq	INIT_MB_AFF_P+24(%rax), %rsi
.LBB3_159:                              # %.preheader197.preheader305
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	1456(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,2), %rax
	jne	.LBB3_36
# BB#160:
	leaq	INIT_TRANSFORM_SIZE_I(,%rax,8), %rsi
	jmp	.LBB3_37
.LBB3_36:
	leaq	INIT_TRANSFORM_SIZE_P(,%rax,8), %rsi
.LBB3_37:                               # %.preheader197.1296
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	1472(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,2), %rax
	jne	.LBB3_38
# BB#146:
	leaq	INIT_TRANSFORM_SIZE_I+8(,%rax,8), %rsi
	jmp	.LBB3_147
.LBB3_38:
	leaq	INIT_TRANSFORM_SIZE_P+8(,%rax,8), %rsi
.LBB3_147:                              # %.preheader197.2297
	callq	biari_init_context
	movq	img(%rip), %rax
	addq	$1488, %r14             # imm = 0x5D0
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,2), %rax
	jne	.LBB3_148
# BB#149:
	leaq	INIT_TRANSFORM_SIZE_I+16(,%rax,8), %rsi
	jmp	.LBB3_150
.LBB3_148:
	leaq	INIT_TRANSFORM_SIZE_P+16(,%rax,8), %rsi
.LBB3_150:                              # %.preheader196.preheader298
	movq	%r14, %rdi
	callq	biari_init_context
	movq	img(%rip), %rax
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	jne	.LBB3_39
# BB#151:
	shlq	$4, %rax
	leaq	INIT_IPR_I(%rax), %rsi
	jmp	.LBB3_40
.LBB3_39:
	shlq	$4, %rax
	leaq	INIT_IPR_P(%rax), %rsi
.LBB3_40:                               # %.preheader196.1291
	movq	%rbx, %rdi
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	16(%rbx), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	jne	.LBB3_41
# BB#143:
	shlq	$4, %rax
	leaq	INIT_IPR_I+8(%rax), %rsi
	jmp	.LBB3_144
.LBB3_41:
	shlq	$4, %rax
	leaq	INIT_IPR_P+8(%rax), %rsi
.LBB3_144:                              # %.preheader195.preheader292
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	32(%rbx), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	jne	.LBB3_42
# BB#145:
	shlq	$5, %rax
	leaq	INIT_CIPR_I(%rax), %rsi
	jmp	.LBB3_43
.LBB3_42:
	shlq	$5, %rax
	leaq	INIT_CIPR_P(%rax), %rsi
.LBB3_43:                               # %.preheader195.1284
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	48(%rbx), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	jne	.LBB3_44
# BB#135:
	shlq	$5, %rax
	leaq	INIT_CIPR_I+8(%rax), %rsi
	jmp	.LBB3_136
.LBB3_44:
	shlq	$5, %rax
	leaq	INIT_CIPR_P+8(%rax), %rsi
.LBB3_136:                              # %.preheader195.2285
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	64(%rbx), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	jne	.LBB3_137
# BB#138:
	shlq	$5, %rax
	leaq	INIT_CIPR_I+16(%rax), %rsi
	jmp	.LBB3_139
.LBB3_137:
	shlq	$5, %rax
	leaq	INIT_CIPR_P+16(%rax), %rsi
.LBB3_139:                              # %.preheader195.3286
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	80(%rbx), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	jne	.LBB3_140
# BB#142:
	shlq	$5, %rax
	leaq	INIT_CIPR_I+24(%rax), %rsi
	jmp	.LBB3_141
.LBB3_140:
	shlq	$5, %rax
	leaq	INIT_CIPR_P+24(%rax), %rsi
.LBB3_141:                              # %.preheader193.preheader
	callq	biari_init_context
	leaq	96(%rbx), %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_45:                               # %.preheader193
                                        # =>This Inner Loop Header: Depth=1
	movq	img(%rip), %rcx
	movslq	15384(%rcx), %rax
	cmpl	$2, 20(%rcx)
	jne	.LBB3_47
# BB#46:                                #   in Loop: Header=BB3_45 Depth=1
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	leaq	INIT_CBP_I(%rbx,%rax), %rsi
	jmp	.LBB3_48
	.p2align	4, 0x90
.LBB3_47:                               #   in Loop: Header=BB3_45 Depth=1
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	leaq	INIT_CBP_P(%rbx,%rax), %rsi
.LBB3_48:                               #   in Loop: Header=BB3_45 Depth=1
	movq	%r14, %rdi
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	16(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,2), %rax
	jne	.LBB3_49
# BB#126:                               #   in Loop: Header=BB3_45 Depth=1
	shlq	$5, %rax
	leaq	INIT_CBP_I+8(%rbx,%rax), %rsi
	jmp	.LBB3_127
	.p2align	4, 0x90
.LBB3_49:                               #   in Loop: Header=BB3_45 Depth=1
	shlq	$5, %rax
	leaq	INIT_CBP_P+8(%rbx,%rax), %rsi
.LBB3_127:                              #   in Loop: Header=BB3_45 Depth=1
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	32(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,2), %rax
	jne	.LBB3_128
# BB#129:                               #   in Loop: Header=BB3_45 Depth=1
	shlq	$5, %rax
	leaq	INIT_CBP_I+16(%rbx,%rax), %rsi
	jmp	.LBB3_130
	.p2align	4, 0x90
.LBB3_128:                              #   in Loop: Header=BB3_45 Depth=1
	shlq	$5, %rax
	leaq	INIT_CBP_P+16(%rbx,%rax), %rsi
.LBB3_130:                              #   in Loop: Header=BB3_45 Depth=1
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	48(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,2), %rax
	jne	.LBB3_131
# BB#132:                               #   in Loop: Header=BB3_45 Depth=1
	shlq	$5, %rax
	leaq	INIT_CBP_I+24(%rbx,%rax), %rsi
	jmp	.LBB3_133
	.p2align	4, 0x90
.LBB3_131:                              #   in Loop: Header=BB3_45 Depth=1
	shlq	$5, %rax
	leaq	INIT_CBP_P+24(%rbx,%rax), %rsi
.LBB3_133:                              #   in Loop: Header=BB3_45 Depth=1
	callq	biari_init_context
	addq	$64, %r14
	addq	$32, %rbx
	cmpq	$96, %rbx
	jne	.LBB3_45
# BB#134:                               # %.preheader191.preheader
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	288(%rax), %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_50:                               # %.preheader191
                                        # =>This Inner Loop Header: Depth=1
	movq	img(%rip), %rcx
	movslq	15384(%rcx), %rax
	cmpl	$2, 20(%rcx)
	jne	.LBB3_52
# BB#51:                                #   in Loop: Header=BB3_50 Depth=1
	shlq	$8, %rax
	leaq	INIT_BCBP_I(%rbx,%rax), %rsi
	jmp	.LBB3_53
	.p2align	4, 0x90
.LBB3_52:                               #   in Loop: Header=BB3_50 Depth=1
	shlq	$8, %rax
	leaq	INIT_BCBP_P(%rbx,%rax), %rsi
.LBB3_53:                               #   in Loop: Header=BB3_50 Depth=1
	movq	%r14, %rdi
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	16(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	jne	.LBB3_54
# BB#117:                               #   in Loop: Header=BB3_50 Depth=1
	shlq	$8, %rax
	leaq	INIT_BCBP_I+8(%rbx,%rax), %rsi
	jmp	.LBB3_118
	.p2align	4, 0x90
.LBB3_54:                               #   in Loop: Header=BB3_50 Depth=1
	shlq	$8, %rax
	leaq	INIT_BCBP_P+8(%rbx,%rax), %rsi
.LBB3_118:                              #   in Loop: Header=BB3_50 Depth=1
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	32(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	jne	.LBB3_119
# BB#120:                               #   in Loop: Header=BB3_50 Depth=1
	shlq	$8, %rax
	leaq	INIT_BCBP_I+16(%rbx,%rax), %rsi
	jmp	.LBB3_121
	.p2align	4, 0x90
.LBB3_119:                              #   in Loop: Header=BB3_50 Depth=1
	shlq	$8, %rax
	leaq	INIT_BCBP_P+16(%rbx,%rax), %rsi
.LBB3_121:                              #   in Loop: Header=BB3_50 Depth=1
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	48(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	jne	.LBB3_122
# BB#123:                               #   in Loop: Header=BB3_50 Depth=1
	shlq	$8, %rax
	leaq	INIT_BCBP_I+24(%rbx,%rax), %rsi
	jmp	.LBB3_124
	.p2align	4, 0x90
.LBB3_122:                              #   in Loop: Header=BB3_50 Depth=1
	shlq	$8, %rax
	leaq	INIT_BCBP_P+24(%rbx,%rax), %rsi
.LBB3_124:                              #   in Loop: Header=BB3_50 Depth=1
	callq	biari_init_context
	addq	$64, %r14
	addq	$32, %rbx
	cmpq	$256, %rbx              # imm = 0x100
	jne	.LBB3_50
# BB#125:                               # %.preheader189.preheader
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	928(%rax), %rbx
	movl	$INIT_MAP_P, %r12d
	movl	$INIT_MAP_I, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_55:                               # %.preheader189
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_56 Depth 2
	movl	$15, %r15d
	movq	%r14, %r13
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB3_56:                               #   Parent Loop BB3_55 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	img(%rip), %rcx
	movslq	15384(%rcx), %rax
	cmpl	$2, 20(%rcx)
	jne	.LBB3_58
# BB#57:                                #   in Loop: Header=BB3_56 Depth=2
	imulq	$960, %rax, %rsi        # imm = 0x3C0
	addq	%r13, %rsi
	jmp	.LBB3_59
	.p2align	4, 0x90
.LBB3_58:                               #   in Loop: Header=BB3_56 Depth=2
	imulq	$960, %rax, %rsi        # imm = 0x3C0
	addq	%r12, %rsi
.LBB3_59:                               #   in Loop: Header=BB3_56 Depth=2
	movq	%rbx, %rdi
	callq	biari_init_context
	addq	$16, %rbx
	addq	$8, %r12
	addq	$8, %r13
	decq	%r15
	jne	.LBB3_56
# BB#60:                                #   in Loop: Header=BB3_55 Depth=1
	incq	%rbp
	movq	8(%rsp), %rbx           # 8-byte Reload
	addq	$240, %rbx
	movq	16(%rsp), %r12          # 8-byte Reload
	addq	$120, %r12
	addq	$120, %r14
	cmpq	$10, %rbp
	jne	.LBB3_55
# BB#61:                                # %.preheader187.preheader
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	3328(%rax), %rbx
	movl	$INIT_LAST_P, %r12d
	movl	$INIT_LAST_I, %r14d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_62:                               # %.preheader187
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_63 Depth 2
	movl	$15, %ebp
	movq	%r14, %r13
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB3_63:                               #   Parent Loop BB3_62 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	img(%rip), %rcx
	movslq	15384(%rcx), %rax
	cmpl	$2, 20(%rcx)
	jne	.LBB3_65
# BB#64:                                #   in Loop: Header=BB3_63 Depth=2
	imulq	$960, %rax, %rsi        # imm = 0x3C0
	addq	%r13, %rsi
	jmp	.LBB3_66
	.p2align	4, 0x90
.LBB3_65:                               #   in Loop: Header=BB3_63 Depth=2
	imulq	$960, %rax, %rsi        # imm = 0x3C0
	addq	%r12, %rsi
.LBB3_66:                               #   in Loop: Header=BB3_63 Depth=2
	movq	%rbx, %rdi
	callq	biari_init_context
	addq	$16, %rbx
	addq	$8, %r12
	addq	$8, %r13
	decq	%rbp
	jne	.LBB3_63
# BB#67:                                #   in Loop: Header=BB3_62 Depth=1
	incq	%r15
	movq	8(%rsp), %rbx           # 8-byte Reload
	addq	$240, %rbx
	movq	16(%rsp), %r12          # 8-byte Reload
	addq	$120, %r12
	addq	$120, %r14
	cmpq	$10, %r15
	jne	.LBB3_62
# BB#68:                                # %.preheader185.preheader
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	5728(%rax), %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_69:                               # %.preheader185
                                        # =>This Inner Loop Header: Depth=1
	movq	img(%rip), %rcx
	movslq	15384(%rcx), %rax
	cmpl	$2, 20(%rcx)
	jne	.LBB3_71
# BB#70:                                #   in Loop: Header=BB3_69 Depth=1
	leaq	(%rax,%rax,4), %rax
	shlq	$6, %rax
	leaq	INIT_ONE_I(%rbx,%rax), %rsi
	jmp	.LBB3_72
	.p2align	4, 0x90
.LBB3_71:                               #   in Loop: Header=BB3_69 Depth=1
	leaq	(%rax,%rax,4), %rax
	shlq	$6, %rax
	leaq	INIT_ONE_P(%rbx,%rax), %rsi
.LBB3_72:                               #   in Loop: Header=BB3_69 Depth=1
	movq	%r14, %rdi
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	16(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	jne	.LBB3_73
# BB#105:                               #   in Loop: Header=BB3_69 Depth=1
	shlq	$6, %rax
	leaq	INIT_ONE_I+8(%rbx,%rax), %rsi
	jmp	.LBB3_106
	.p2align	4, 0x90
.LBB3_73:                               #   in Loop: Header=BB3_69 Depth=1
	shlq	$6, %rax
	leaq	INIT_ONE_P+8(%rbx,%rax), %rsi
.LBB3_106:                              #   in Loop: Header=BB3_69 Depth=1
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	32(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	jne	.LBB3_107
# BB#108:                               #   in Loop: Header=BB3_69 Depth=1
	shlq	$6, %rax
	leaq	INIT_ONE_I+16(%rbx,%rax), %rsi
	jmp	.LBB3_109
	.p2align	4, 0x90
.LBB3_107:                              #   in Loop: Header=BB3_69 Depth=1
	shlq	$6, %rax
	leaq	INIT_ONE_P+16(%rbx,%rax), %rsi
.LBB3_109:                              #   in Loop: Header=BB3_69 Depth=1
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	48(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	jne	.LBB3_110
# BB#111:                               #   in Loop: Header=BB3_69 Depth=1
	shlq	$6, %rax
	leaq	INIT_ONE_I+24(%rbx,%rax), %rsi
	jmp	.LBB3_112
	.p2align	4, 0x90
.LBB3_110:                              #   in Loop: Header=BB3_69 Depth=1
	shlq	$6, %rax
	leaq	INIT_ONE_P+24(%rbx,%rax), %rsi
.LBB3_112:                              #   in Loop: Header=BB3_69 Depth=1
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	64(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	jne	.LBB3_113
# BB#114:                               #   in Loop: Header=BB3_69 Depth=1
	shlq	$6, %rax
	leaq	INIT_ONE_I+32(%rbx,%rax), %rsi
	jmp	.LBB3_115
	.p2align	4, 0x90
.LBB3_113:                              #   in Loop: Header=BB3_69 Depth=1
	shlq	$6, %rax
	leaq	INIT_ONE_P+32(%rbx,%rax), %rsi
.LBB3_115:                              #   in Loop: Header=BB3_69 Depth=1
	callq	biari_init_context
	addq	$80, %r14
	addq	$40, %rbx
	cmpq	$400, %rbx              # imm = 0x190
	jne	.LBB3_69
# BB#116:                               # %.preheader183.preheader
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	6528(%rax), %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_74:                               # %.preheader183
                                        # =>This Inner Loop Header: Depth=1
	movq	img(%rip), %rcx
	movslq	15384(%rcx), %rax
	cmpl	$2, 20(%rcx)
	jne	.LBB3_76
# BB#75:                                #   in Loop: Header=BB3_74 Depth=1
	leaq	(%rax,%rax,4), %rax
	shlq	$6, %rax
	leaq	INIT_ABS_I(%rbx,%rax), %rsi
	jmp	.LBB3_77
	.p2align	4, 0x90
.LBB3_76:                               #   in Loop: Header=BB3_74 Depth=1
	leaq	(%rax,%rax,4), %rax
	shlq	$6, %rax
	leaq	INIT_ABS_P(%rbx,%rax), %rsi
.LBB3_77:                               #   in Loop: Header=BB3_74 Depth=1
	movq	%r14, %rdi
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	16(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	jne	.LBB3_78
# BB#93:                                #   in Loop: Header=BB3_74 Depth=1
	shlq	$6, %rax
	leaq	INIT_ABS_I+8(%rbx,%rax), %rsi
	jmp	.LBB3_94
	.p2align	4, 0x90
.LBB3_78:                               #   in Loop: Header=BB3_74 Depth=1
	shlq	$6, %rax
	leaq	INIT_ABS_P+8(%rbx,%rax), %rsi
.LBB3_94:                               #   in Loop: Header=BB3_74 Depth=1
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	32(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	jne	.LBB3_95
# BB#96:                                #   in Loop: Header=BB3_74 Depth=1
	shlq	$6, %rax
	leaq	INIT_ABS_I+16(%rbx,%rax), %rsi
	jmp	.LBB3_97
	.p2align	4, 0x90
.LBB3_95:                               #   in Loop: Header=BB3_74 Depth=1
	shlq	$6, %rax
	leaq	INIT_ABS_P+16(%rbx,%rax), %rsi
.LBB3_97:                               #   in Loop: Header=BB3_74 Depth=1
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	48(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	jne	.LBB3_98
# BB#99:                                #   in Loop: Header=BB3_74 Depth=1
	shlq	$6, %rax
	leaq	INIT_ABS_I+24(%rbx,%rax), %rsi
	jmp	.LBB3_100
	.p2align	4, 0x90
.LBB3_98:                               #   in Loop: Header=BB3_74 Depth=1
	shlq	$6, %rax
	leaq	INIT_ABS_P+24(%rbx,%rax), %rsi
.LBB3_100:                              #   in Loop: Header=BB3_74 Depth=1
	callq	biari_init_context
	movq	img(%rip), %rax
	leaq	64(%r14), %rdi
	cmpl	$2, 20(%rax)
	movslq	15384(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	jne	.LBB3_101
# BB#102:                               #   in Loop: Header=BB3_74 Depth=1
	shlq	$6, %rax
	leaq	INIT_ABS_I+32(%rbx,%rax), %rsi
	jmp	.LBB3_103
	.p2align	4, 0x90
.LBB3_101:                              #   in Loop: Header=BB3_74 Depth=1
	shlq	$6, %rax
	leaq	INIT_ABS_P+32(%rbx,%rax), %rsi
.LBB3_103:                              #   in Loop: Header=BB3_74 Depth=1
	callq	biari_init_context
	addq	$80, %r14
	addq	$40, %rbx
	cmpq	$400, %rbx              # imm = 0x190
	jne	.LBB3_74
# BB#104:                               # %.preheader181.preheader
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	7328(%rax), %rbx
	movl	$INIT_FLD_MAP_P, %r12d
	movl	$INIT_FLD_MAP_I, %r14d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_79:                               # %.preheader181
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_80 Depth 2
	movl	$15, %ebp
	movq	%r14, %r13
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB3_80:                               #   Parent Loop BB3_79 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	img(%rip), %rcx
	movslq	15384(%rcx), %rax
	cmpl	$2, 20(%rcx)
	jne	.LBB3_82
# BB#81:                                #   in Loop: Header=BB3_80 Depth=2
	imulq	$960, %rax, %rsi        # imm = 0x3C0
	addq	%r13, %rsi
	jmp	.LBB3_83
	.p2align	4, 0x90
.LBB3_82:                               #   in Loop: Header=BB3_80 Depth=2
	imulq	$960, %rax, %rsi        # imm = 0x3C0
	addq	%r12, %rsi
.LBB3_83:                               #   in Loop: Header=BB3_80 Depth=2
	movq	%rbx, %rdi
	callq	biari_init_context
	addq	$16, %rbx
	addq	$8, %r12
	addq	$8, %r13
	decq	%rbp
	jne	.LBB3_80
# BB#84:                                #   in Loop: Header=BB3_79 Depth=1
	incq	%r15
	movq	8(%rsp), %rbx           # 8-byte Reload
	addq	$240, %rbx
	movq	16(%rsp), %r12          # 8-byte Reload
	addq	$120, %r12
	addq	$120, %r14
	cmpq	$10, %r15
	jne	.LBB3_79
# BB#85:                                # %.preheader.preheader
	movq	(%rsp), %rbx            # 8-byte Reload
	addq	$9728, %rbx             # imm = 0x2600
	movl	$INIT_FLD_LAST_P, %r14d
	movl	$INIT_FLD_LAST_I, %r12d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_86:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_87 Depth 2
	movl	$15, %r13d
	movq	%r12, %rbp
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	%rbx, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB3_87:                               #   Parent Loop BB3_86 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	img(%rip), %rcx
	movslq	15384(%rcx), %rax
	cmpl	$2, 20(%rcx)
	jne	.LBB3_89
# BB#88:                                #   in Loop: Header=BB3_87 Depth=2
	imulq	$960, %rax, %rsi        # imm = 0x3C0
	addq	%rbp, %rsi
	jmp	.LBB3_90
	.p2align	4, 0x90
.LBB3_89:                               #   in Loop: Header=BB3_87 Depth=2
	imulq	$960, %rax, %rsi        # imm = 0x3C0
	addq	%r14, %rsi
.LBB3_90:                               #   in Loop: Header=BB3_87 Depth=2
	movq	%rbx, %rdi
	callq	biari_init_context
	addq	$16, %rbx
	addq	$8, %r14
	addq	$8, %rbp
	decq	%r13
	jne	.LBB3_87
# BB#91:                                #   in Loop: Header=BB3_86 Depth=1
	incq	%r15
	movq	(%rsp), %rbx            # 8-byte Reload
	addq	$240, %rbx
	movq	8(%rsp), %r14           # 8-byte Reload
	addq	$120, %r14
	addq	$120, %r12
	cmpq	$10, %r15
	jne	.LBB3_86
# BB#92:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	init_contexts, .Lfunc_end3-init_contexts
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI4_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_2:
	.quad	4584664420663164928     # double 0.03125
.LCPI4_3:
	.quad	4607182418800017408     # double 1
	.text
	.globl	XRate
	.p2align	4, 0x90
	.type	XRate,@function
XRate:                                  # @XRate
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	movl	36(%rax), %eax
	xorl	%ecx, %ecx
	testl	%eax, %eax
	cmovsl	%ecx, %eax
	movq	8(%rdi), %xmm0          # xmm0 = mem[0],zero
	punpckldq	.LCPI4_0(%rip), %xmm0 # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	.LCPI4_1(%rip), %xmm0
	pshufd	$78, %xmm0, %xmm2       # xmm2 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm2
	mulsd	.LCPI4_2(%rip), %xmm2
	movsd	.LCPI4_3(%rip), %xmm1   # xmm1 = mem[0],zero
	minsd	%xmm2, %xmm1
	imull	(%rsi), %eax
	sarl	$4, %eax
	addl	4(%rsi), %eax
	cmovsl	%ecx, %eax
	cmpl	$128, %eax
	movl	$127, %ecx
	cmovgel	%ecx, %eax
	movzwl	(%rdi), %edx
	movl	$63, %esi
	subl	%edx, %esi
	addl	$64, %edx
	cmpb	$0, 2(%rdi)
	cmovel	%esi, %edx
	movslq	%edx, %rdx
	movsd	probability(,%rdx,8), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	mulsd	entropy(,%rax,8), %xmm2
	xorpd	%xmm0, %xmm0
	subsd	%xmm2, %xmm0
	subl	%edx, %ecx
	movslq	%ecx, %rcx
	mulsd	probability(,%rcx,8), %xmm1
	xorl	$127, %eax
	mulsd	entropy(,%rax,8), %xmm1
	subsd	%xmm1, %xmm0
	retq
.Lfunc_end4:
	.size	XRate, .Lfunc_end4-XRate
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	5055640609639927018     # double 1.0E+30
.LCPI5_3:
	.quad	4584664420663164928     # double 0.03125
.LCPI5_4:
	.quad	4607182418800017408     # double 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_1:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI5_2:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.text
	.globl	GetCtxModelNumber
	.p2align	4, 0x90
	.type	GetCtxModelNumber,@function
GetCtxModelNumber:                      # @GetCtxModelNumber
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$336, %rsp              # imm = 0x150
.Lcfi25:
	.cfi_def_cfa_offset 392
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%rdx, %r10
	movq	%rdi, 264(%rsp)         # 8-byte Spill
	movq	img(%rip), %rbp
	movl	20(%rbp), %r8d
	xorl	%eax, %eax
	cmpl	$2, %r8d
	setne	%al
	leaq	1(%rax,%rax), %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	leaq	296(%r10), %rax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	leaq	936(%r10), %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	leaq	3336(%r10), %rax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	leaq	5736(%r10), %rax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	leaq	6536(%r10), %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	leaq	7336(%r10), %rax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	leaq	9736(%r10), %rax
	movq	%rax, -112(%rsp)        # 8-byte Spill
	movsd	.LCPI5_0(%rip), %xmm8   # xmm8 = mem[0],zero
	xorl	%r15d, %r15d
	movl	$INIT_MB_TYPE_P+4, %eax
	movdqa	.LCPI5_1(%rip), %xmm1   # xmm1 = [1127219200,1160773632,0,0]
	movapd	.LCPI5_2(%rip), %xmm2   # xmm2 = [4.503600e+15,1.934281e+25]
	movsd	.LCPI5_3(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI5_4(%rip), %xmm4   # xmm4 = mem[0],zero
	movapd	.LCPI5_2(%rip), %xmm9   # xmm9 = [4.503600e+15,1.934281e+25]
	movl	$INIT_MB_TYPE_P+92, %edx
	movl	$INIT_MB_TYPE_P+180, %r11d
	movl	$INIT_MB_TYPE_I+4, %edi
	movl	$INIT_MB_TYPE_I+92, %r12d
	movl	$INIT_MB_TYPE_I+180, %r13d
	movl	$INIT_B8_TYPE_P+4, %ecx
	movq	%rcx, 248(%rsp)         # 8-byte Spill
	movl	$INIT_B8_TYPE_P+76, %ecx
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	movl	$INIT_B8_TYPE_I+4, %ecx
	movq	%rcx, 232(%rsp)         # 8-byte Spill
	movl	$INIT_B8_TYPE_I+76, %ecx
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	movl	$INIT_MV_RES_P+4, %ecx
	movq	%rcx, 216(%rsp)         # 8-byte Spill
	movl	$INIT_MV_RES_P+84, %ecx
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	movl	$INIT_MV_RES_I+4, %ecx
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	movl	$INIT_MV_RES_I+84, %ecx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	movl	$INIT_REF_NO_P+4, %ecx
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	movl	$INIT_REF_NO_P+52, %ecx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movl	$INIT_REF_NO_I+4, %ecx
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	movl	$INIT_REF_NO_I+52, %ecx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movl	$INIT_DELTA_QP_P+4, %ecx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movl	$INIT_DELTA_QP_I+4, %ecx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movl	$INIT_MB_AFF_P+4, %ecx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movl	$INIT_MB_AFF_I+4, %ecx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movl	$INIT_CIPR_P+4, %ecx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movl	$INIT_CIPR_I+4, %ecx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movl	$INIT_CBP_P+4, %ecx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movl	$INIT_CBP_P+36, %ecx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movl	$INIT_CBP_P+68, %ecx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movl	$INIT_CBP_I+4, %ecx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movl	$INIT_CBP_I+36, %ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	$INIT_CBP_I+68, %ecx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movl	$INIT_BCBP_P+4, %ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	$INIT_BCBP_I+4, %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	$INIT_MAP_P+4, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	$INIT_MAP_I+4, %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	$INIT_LAST_P+4, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	$INIT_LAST_I+4, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	$INIT_ONE_P+4, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	$INIT_ONE_I+4, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movl	$INIT_ABS_P+4, %ecx
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	movl	$INIT_ABS_I+4, %ecx
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	movl	$INIT_FLD_MAP_P+4, %ecx
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	movl	$INIT_FLD_MAP_I+4, %ecx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	movl	$INIT_FLD_LAST_P+4, %ecx
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	movl	$INIT_FLD_LAST_I+4, %ecx
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	movl	$127, %r14d
	xorl	%ecx, %ecx
	movq	%rcx, -128(%rsp)        # 8-byte Spill
	movq	%rsi, 280(%rsp)         # 8-byte Spill
	movq	%r10, -56(%rsp)         # 8-byte Spill
	jmp	.LBB5_1
	.p2align	4, 0x90
.LBB5_126:                              # %.preheader.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	-112(%rsp), %r8         # 8-byte Reload
	movq	-40(%rsp), %rax         # 8-byte Reload
	xorl	%r9d, %r9d
	movq	-56(%rsp), %r10         # 8-byte Reload
	movq	256(%rsp), %r11         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_127:                              # %.preheader
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_128 Depth 3
	movl	$15, %ebp
	movq	%r8, %rcx
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB5_128:                              #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_127 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rbx), %edi
	imull	%r13d, %edi
	sarl	$4, %edi
	addl	(%rbx), %edi
	cmovsl	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r14d, %edi
	movzwl	-8(%rcx), %edx
	movl	$63, %esi
	subl	%edx, %esi
	addl	$64, %edx
	cmpb	$0, -6(%rcx)
	cmovel	%esi, %edx
	movslq	%edx, %rdx
	movsd	probability(,%rdx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rdi,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	mulsd	probability(,%rdx,8), %xmm0
	xorl	$127, %edi
	mulsd	entropy(,%rdi,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rbx
	addq	$16, %rcx
	decq	%rbp
	jne	.LBB5_128
# BB#129:                               # %.us-lcssa455
                                        #   in Loop: Header=BB5_127 Depth=2
	incq	%r9
	addq	$120, %rax
	addq	$240, %r8
	cmpq	$10, %r9
	jne	.LBB5_127
.LBB5_134:                              # %.us-lcssa458.us
                                        #   in Loop: Header=BB5_1 Depth=1
	ucomisd	%xmm6, %xmm8
	jbe	.LBB5_136
# BB#135:                               #   in Loop: Header=BB5_1 Depth=1
	movq	264(%rsp), %rax         # 8-byte Reload
	movq	-128(%rsp), %rcx        # 8-byte Reload
	movl	%ecx, (%rax)
	movapd	%xmm6, %xmm8
.LBB5_136:                              #   in Loop: Header=BB5_1 Depth=1
	movq	-128(%rsp), %rax        # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, -128(%rsp)        # 8-byte Spill
	cmpq	272(%rsp), %rcx         # 8-byte Folded Reload
	movq	280(%rsp), %rsi         # 8-byte Reload
	movq	328(%rsp), %rdx         # 8-byte Reload
	movq	320(%rsp), %rdi         # 8-byte Reload
	movq	312(%rsp), %r12         # 8-byte Reload
	movq	304(%rsp), %r13         # 8-byte Reload
	movq	296(%rsp), %rbp         # 8-byte Reload
	jge	.LBB5_138
# BB#137:                               # %..preheader322_crit_edge
                                        #   in Loop: Header=BB5_1 Depth=1
	movl	20(%rbp), %r8d
	movq	288(%rsp), %rax         # 8-byte Reload
	addq	$264, %rax              # imm = 0x108
	addq	$264, %rdx              # imm = 0x108
	addq	$264, %r11              # imm = 0x108
	addq	$264, %rdi              # imm = 0x108
	addq	$264, %r12              # imm = 0x108
	addq	$264, %r13              # imm = 0x108
	addq	$144, 248(%rsp)         # 8-byte Folded Spill
	addq	$144, 240(%rsp)         # 8-byte Folded Spill
	addq	$144, 232(%rsp)         # 8-byte Folded Spill
	addq	$144, 224(%rsp)         # 8-byte Folded Spill
	addq	$160, 216(%rsp)         # 8-byte Folded Spill
	addq	$160, 208(%rsp)         # 8-byte Folded Spill
	addq	$160, 200(%rsp)         # 8-byte Folded Spill
	addq	$160, 192(%rsp)         # 8-byte Folded Spill
	addq	$96, 184(%rsp)          # 8-byte Folded Spill
	addq	$96, 176(%rsp)          # 8-byte Folded Spill
	addq	$96, 168(%rsp)          # 8-byte Folded Spill
	addq	$96, 160(%rsp)          # 8-byte Folded Spill
	addq	$32, 152(%rsp)          # 8-byte Folded Spill
	addq	$32, 144(%rsp)          # 8-byte Folded Spill
	addq	$32, 136(%rsp)          # 8-byte Folded Spill
	addq	$32, 128(%rsp)          # 8-byte Folded Spill
	addq	$32, 120(%rsp)          # 8-byte Folded Spill
	addq	$32, 112(%rsp)          # 8-byte Folded Spill
	addq	$96, 104(%rsp)          # 8-byte Folded Spill
	addq	$96, 96(%rsp)           # 8-byte Folded Spill
	addq	$96, 88(%rsp)           # 8-byte Folded Spill
	addq	$96, 80(%rsp)           # 8-byte Folded Spill
	addq	$96, 72(%rsp)           # 8-byte Folded Spill
	addq	$96, 64(%rsp)           # 8-byte Folded Spill
	addq	$256, 56(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x100
	addq	$256, 48(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x100
	addq	$960, 40(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x3C0
	addq	$960, 32(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x3C0
	addq	$960, 24(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x3C0
	addq	$960, 16(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x3C0
	addq	$320, 8(%rsp)           # 8-byte Folded Spill
                                        # imm = 0x140
	addq	$320, (%rsp)            # 8-byte Folded Spill
                                        # imm = 0x140
	addq	$320, -8(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x140
	addq	$320, -16(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x140
	addq	$960, -24(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x3C0
	addq	$960, -32(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x3C0
	addq	$960, -40(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x3C0
	addq	$960, -48(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x3C0
.LBB5_1:                                # %.preheader322
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_3 Depth 2
                                        #     Child Loop BB5_5 Depth 2
                                        #     Child Loop BB5_7 Depth 2
                                        #     Child Loop BB5_8 Depth 2
                                        #     Child Loop BB5_10 Depth 2
                                        #     Child Loop BB5_12 Depth 2
                                        #     Child Loop BB5_15 Depth 2
                                        #     Child Loop BB5_17 Depth 2
                                        #     Child Loop BB5_19 Depth 2
                                        #     Child Loop BB5_21 Depth 2
                                        #     Child Loop BB5_24 Depth 2
                                        #     Child Loop BB5_26 Depth 2
                                        #     Child Loop BB5_28 Depth 2
                                        #     Child Loop BB5_30 Depth 2
                                        #     Child Loop BB5_33 Depth 2
                                        #     Child Loop BB5_35 Depth 2
                                        #     Child Loop BB5_37 Depth 2
                                        #     Child Loop BB5_39 Depth 2
                                        #     Child Loop BB5_42 Depth 2
                                        #     Child Loop BB5_44 Depth 2
                                        #     Child Loop BB5_47 Depth 2
                                        #     Child Loop BB5_49 Depth 2
                                        #     Child Loop BB5_55 Depth 2
                                        #     Child Loop BB5_57 Depth 2
                                        #     Child Loop BB5_60 Depth 2
                                        #     Child Loop BB5_62 Depth 2
                                        #     Child Loop BB5_64 Depth 2
                                        #     Child Loop BB5_66 Depth 2
                                        #     Child Loop BB5_68 Depth 2
                                        #     Child Loop BB5_70 Depth 2
                                        #     Child Loop BB5_73 Depth 2
                                        #       Child Loop BB5_74 Depth 3
                                        #     Child Loop BB5_77 Depth 2
                                        #       Child Loop BB5_78 Depth 3
                                        #     Child Loop BB5_82 Depth 2
                                        #       Child Loop BB5_83 Depth 3
                                        #     Child Loop BB5_86 Depth 2
                                        #       Child Loop BB5_87 Depth 3
                                        #     Child Loop BB5_91 Depth 2
                                        #       Child Loop BB5_92 Depth 3
                                        #     Child Loop BB5_95 Depth 2
                                        #       Child Loop BB5_96 Depth 3
                                        #     Child Loop BB5_100 Depth 2
                                        #       Child Loop BB5_101 Depth 3
                                        #     Child Loop BB5_104 Depth 2
                                        #       Child Loop BB5_105 Depth 3
                                        #     Child Loop BB5_109 Depth 2
                                        #       Child Loop BB5_110 Depth 3
                                        #     Child Loop BB5_113 Depth 2
                                        #       Child Loop BB5_114 Depth 3
                                        #     Child Loop BB5_118 Depth 2
                                        #       Child Loop BB5_119 Depth 3
                                        #     Child Loop BB5_122 Depth 2
                                        #       Child Loop BB5_123 Depth 3
                                        #     Child Loop BB5_127 Depth 2
                                        #       Child Loop BB5_128 Depth 3
                                        #     Child Loop BB5_131 Depth 2
                                        #       Child Loop BB5_132 Depth 3
	movl	36(%rbp), %r9d
	testl	%r9d, %r9d
	cmovsl	%r15d, %r9d
	cmpl	$2, %r8d
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movq	%rdx, 328(%rsp)         # 8-byte Spill
	movq	%rdi, 320(%rsp)         # 8-byte Spill
	xorpd	%xmm6, %xmm6
	movl	$1, %ecx
	jne	.LBB5_2
	.p2align	4, 0x90
.LBB5_8:                                # %.preheader305.us.preheader
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm7       # xmm7 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm7
	mulsd	%xmm3, %xmm7
	movapd	%xmm4, %xmm0
	minsd	%xmm7, %xmm0
	movl	-4(%rdi), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rdi), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	-8(%rsi,%rcx,8), %ebx
	movl	$63, %eax
	subl	%ebx, %eax
	addl	$64, %ebx
	cmpb	$0, -6(%rsi,%rcx,8)
	cmovel	%eax, %ebx
	movslq	%ebx, %rax
	movsd	probability(,%rax,8), %xmm7 # xmm7 = mem[0],zero
	mulsd	%xmm0, %xmm7
	mulsd	entropy(,%rbp,8), %xmm7
	xorpd	%xmm5, %xmm5
	subsd	%xmm7, %xmm5
	movl	$127, %ebx
	subl	%eax, %ebx
	movslq	%ebx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm5
	addsd	%xmm5, %xmm6
	addq	$8, %rdi
	addq	$2, %rcx
	cmpq	$23, %rcx
	jne	.LBB5_8
# BB#9:                                 # %.us-lcssa.us.us.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	%r12, %rcx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_10:                               # %.us-lcssa.us.us
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	184(%rsi,%rdi,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rcx), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rcx), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	176(%rsi,%rdi,8), %eax
	movl	$63, %edx
	subl	%eax, %edx
	addl	$64, %eax
	cmpb	$0, 178(%rsi,%rdi,8)
	cmovel	%edx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %edx
	subl	%eax, %edx
	movslq	%edx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$2, %rdi
	addq	$8, %rcx
	cmpq	$22, %rdi
	jne	.LBB5_10
# BB#11:                                # %.us-lcssa.us.us.1.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	%r13, %rcx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_12:                               # %.us-lcssa.us.us.1
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	360(%rsi,%rdi,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rcx), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rcx), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	352(%rsi,%rdi,8), %eax
	movl	$63, %edx
	subl	%eax, %edx
	addl	$64, %eax
	cmpb	$0, 354(%rsi,%rdi,8)
	cmovel	%edx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %edx
	subl	%eax, %edx
	movslq	%edx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$2, %rdi
	addq	$8, %rcx
	cmpq	$22, %rdi
	jne	.LBB5_12
	jmp	.LBB5_13
	.p2align	4, 0x90
.LBB5_2:                                # %.preheader305.preheader.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB5_3:                                # %.preheader305.preheader
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rdi), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rdi), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	-8(%rsi,%rcx,8), %eax
	movl	$63, %ebx
	subl	%eax, %ebx
	addl	$64, %eax
	cmpb	$0, -6(%rsi,%rcx,8)
	cmovel	%ebx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %ebx
	subl	%eax, %ebx
	movslq	%ebx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rdi
	addq	$2, %rcx
	cmpq	$23, %rcx
	jne	.LBB5_3
# BB#4:                                 # %.us-lcssa.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	%rdx, %rcx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_5:                                # %.us-lcssa
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	184(%rsi,%rdi,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rcx), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rcx), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	176(%rsi,%rdi,8), %eax
	movl	$63, %edx
	subl	%eax, %edx
	addl	$64, %eax
	cmpb	$0, 178(%rsi,%rdi,8)
	cmovel	%edx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %edx
	subl	%eax, %edx
	movslq	%edx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$2, %rdi
	addq	$8, %rcx
	cmpq	$22, %rdi
	jne	.LBB5_5
# BB#6:                                 # %.us-lcssa.1.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	%r11, %rcx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_7:                                # %.us-lcssa.1
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	360(%rsi,%rdi,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rcx), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rcx), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	352(%rsi,%rdi,8), %eax
	movl	$63, %edx
	subl	%eax, %edx
	addl	$64, %eax
	cmpb	$0, 354(%rsi,%rdi,8)
	cmovel	%edx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %edx
	subl	%eax, %edx
	movslq	%edx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$2, %rdi
	addq	$8, %rcx
	cmpq	$22, %rdi
	jne	.LBB5_7
.LBB5_13:                               # %.preheader321
                                        #   in Loop: Header=BB5_1 Depth=1
	xorl	%ecx, %ecx
	cmpl	$2, %r8d
	jne	.LBB5_14
# BB#18:                                # %.preheader304.us.preheader.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	232(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_19:                               # %.preheader304.us.preheader
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	536(%rsi,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rdi), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rdi), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	528(%rsi,%rcx,8), %eax
	movl	$63, %ebx
	subl	%eax, %ebx
	addl	$64, %eax
	cmpb	$0, 530(%rsi,%rcx,8)
	cmovel	%ebx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %ebx
	subl	%eax, %ebx
	movslq	%ebx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rdi
	addq	$2, %rcx
	cmpq	$18, %rcx
	jne	.LBB5_19
# BB#20:                                # %.us-lcssa335.us.us.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	224(%rsp), %rcx         # 8-byte Reload
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_21:                               # %.us-lcssa335.us.us
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	680(%rsi,%rdi,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rcx), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rcx), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	672(%rsi,%rdi,8), %eax
	movl	$63, %edx
	subl	%eax, %edx
	addl	$64, %eax
	cmpb	$0, 674(%rsi,%rdi,8)
	cmovel	%edx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %edx
	subl	%eax, %edx
	movslq	%edx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$2, %rdi
	addq	$8, %rcx
	cmpq	$18, %rdi
	jne	.LBB5_21
	jmp	.LBB5_22
	.p2align	4, 0x90
.LBB5_14:                               # %.preheader304.preheader.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	248(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_15:                               # %.preheader304.preheader
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	536(%rsi,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rdi), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rdi), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	528(%rsi,%rcx,8), %eax
	movl	$63, %ebx
	subl	%eax, %ebx
	addl	$64, %eax
	cmpb	$0, 530(%rsi,%rcx,8)
	cmovel	%ebx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %ebx
	subl	%eax, %ebx
	movslq	%ebx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rdi
	addq	$2, %rcx
	cmpq	$18, %rcx
	jne	.LBB5_15
# BB#16:                                # %.us-lcssa335.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	240(%rsp), %rcx         # 8-byte Reload
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_17:                               # %.us-lcssa335
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	680(%rsi,%rdi,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rcx), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rcx), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	672(%rsi,%rdi,8), %eax
	movl	$63, %edx
	subl	%eax, %edx
	addl	$64, %eax
	cmpb	$0, 674(%rsi,%rdi,8)
	cmovel	%edx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %edx
	subl	%eax, %edx
	movslq	%edx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$2, %rdi
	addq	$8, %rcx
	cmpq	$18, %rdi
	jne	.LBB5_17
.LBB5_22:                               # %.preheader320
                                        #   in Loop: Header=BB5_1 Depth=1
	xorl	%ecx, %ecx
	cmpl	$2, %r8d
	jne	.LBB5_23
# BB#27:                                # %.preheader303.us.preheader.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	200(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_28:                               # %.preheader303.us.preheader
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	824(%rsi,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rdi), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rdi), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	816(%rsi,%rcx,8), %eax
	movl	$63, %ebx
	subl	%eax, %ebx
	addl	$64, %eax
	cmpb	$0, 818(%rsi,%rcx,8)
	cmovel	%ebx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %ebx
	subl	%eax, %ebx
	movslq	%ebx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rdi
	addq	$2, %rcx
	cmpq	$20, %rcx
	jne	.LBB5_28
# BB#29:                                # %.us-lcssa346.us.us.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	192(%rsp), %rcx         # 8-byte Reload
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_30:                               # %.us-lcssa346.us.us
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	984(%rsi,%rdi,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rcx), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rcx), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	976(%rsi,%rdi,8), %eax
	movl	$63, %edx
	subl	%eax, %edx
	addl	$64, %eax
	cmpb	$0, 978(%rsi,%rdi,8)
	cmovel	%edx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %edx
	subl	%eax, %edx
	movslq	%edx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$2, %rdi
	addq	$8, %rcx
	cmpq	$20, %rdi
	jne	.LBB5_30
	jmp	.LBB5_31
	.p2align	4, 0x90
.LBB5_23:                               # %.preheader303.preheader.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	216(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_24:                               # %.preheader303.preheader
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	824(%rsi,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rdi), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rdi), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	816(%rsi,%rcx,8), %eax
	movl	$63, %ebx
	subl	%eax, %ebx
	addl	$64, %eax
	cmpb	$0, 818(%rsi,%rcx,8)
	cmovel	%ebx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %ebx
	subl	%eax, %ebx
	movslq	%ebx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rdi
	addq	$2, %rcx
	cmpq	$20, %rcx
	jne	.LBB5_24
# BB#25:                                # %.us-lcssa346.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	208(%rsp), %rcx         # 8-byte Reload
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_26:                               # %.us-lcssa346
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	984(%rsi,%rdi,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rcx), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rcx), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	976(%rsi,%rdi,8), %eax
	movl	$63, %edx
	subl	%eax, %edx
	addl	$64, %eax
	cmpb	$0, 978(%rsi,%rdi,8)
	cmovel	%edx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %edx
	subl	%eax, %edx
	movslq	%edx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$2, %rdi
	addq	$8, %rcx
	cmpq	$20, %rdi
	jne	.LBB5_26
.LBB5_31:                               # %.preheader319
                                        #   in Loop: Header=BB5_1 Depth=1
	xorl	%ecx, %ecx
	cmpl	$2, %r8d
	jne	.LBB5_32
# BB#36:                                # %.preheader302.us.preheader.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	168(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_37:                               # %.preheader302.us.preheader
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	1144(%rsi,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rdi), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rdi), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	1136(%rsi,%rcx,8), %eax
	movl	$63, %ebx
	subl	%eax, %ebx
	addl	$64, %eax
	cmpb	$0, 1138(%rsi,%rcx,8)
	cmovel	%ebx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %ebx
	subl	%eax, %ebx
	movslq	%ebx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rdi
	addq	$2, %rcx
	cmpq	$12, %rcx
	jne	.LBB5_37
# BB#38:                                # %.us-lcssa357.us.us.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	160(%rsp), %rcx         # 8-byte Reload
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_39:                               # %.us-lcssa357.us.us
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	1240(%rsi,%rdi,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rcx), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rcx), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	1232(%rsi,%rdi,8), %eax
	movl	$63, %edx
	subl	%eax, %edx
	addl	$64, %eax
	cmpb	$0, 1234(%rsi,%rdi,8)
	cmovel	%edx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %edx
	subl	%eax, %edx
	movslq	%edx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$2, %rdi
	addq	$8, %rcx
	cmpq	$12, %rdi
	jne	.LBB5_39
	jmp	.LBB5_40
	.p2align	4, 0x90
.LBB5_32:                               # %.preheader302.preheader.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	184(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_33:                               # %.preheader302.preheader
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	1144(%rsi,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rdi), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rdi), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	1136(%rsi,%rcx,8), %eax
	movl	$63, %ebx
	subl	%eax, %ebx
	addl	$64, %eax
	cmpb	$0, 1138(%rsi,%rcx,8)
	cmovel	%ebx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %ebx
	subl	%eax, %ebx
	movslq	%ebx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rdi
	addq	$2, %rcx
	cmpq	$12, %rcx
	jne	.LBB5_33
# BB#34:                                # %.us-lcssa357.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	176(%rsp), %rcx         # 8-byte Reload
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_35:                               # %.us-lcssa357
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	1240(%rsi,%rdi,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rcx), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rcx), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	1232(%rsi,%rdi,8), %eax
	movl	$63, %edx
	subl	%eax, %edx
	addl	$64, %eax
	cmpb	$0, 1234(%rsi,%rdi,8)
	cmovel	%edx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %edx
	subl	%eax, %edx
	movslq	%edx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$2, %rdi
	addq	$8, %rcx
	cmpq	$12, %rdi
	jne	.LBB5_35
.LBB5_40:                               # %.preheader318
                                        #   in Loop: Header=BB5_1 Depth=1
	xorl	%ecx, %ecx
	cmpl	$2, %r8d
	jne	.LBB5_41
# BB#43:                                # %.preheader318.split.us.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	144(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_44:                               # %.preheader318.split.us
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	1336(%rsi,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rdi), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rdi), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	1328(%rsi,%rcx,8), %eax
	movl	$63, %ebx
	subl	%eax, %ebx
	addl	$64, %eax
	cmpb	$0, 1330(%rsi,%rcx,8)
	cmovel	%ebx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %ebx
	subl	%eax, %ebx
	movslq	%ebx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rdi
	addq	$2, %rcx
	cmpq	$8, %rcx
	jne	.LBB5_44
	jmp	.LBB5_45
	.p2align	4, 0x90
.LBB5_41:                               # %.preheader318.split.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	152(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_42:                               # %.preheader318.split
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	1336(%rsi,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rdi), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rdi), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	1328(%rsi,%rcx,8), %eax
	movl	$63, %ebx
	subl	%eax, %ebx
	addl	$64, %eax
	cmpb	$0, 1330(%rsi,%rcx,8)
	cmovel	%ebx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %ebx
	subl	%eax, %ebx
	movslq	%ebx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rdi
	addq	$2, %rcx
	cmpq	$8, %rcx
	jne	.LBB5_42
.LBB5_45:                               # %.preheader317
                                        #   in Loop: Header=BB5_1 Depth=1
	xorl	%ecx, %ecx
	cmpl	$2, %r8d
	jne	.LBB5_46
# BB#48:                                # %.preheader317.split.us.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	128(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_49:                               # %.preheader317.split.us
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	1400(%rsi,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rdi), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rdi), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	1392(%rsi,%rcx,8), %eax
	movl	$63, %ebx
	subl	%eax, %ebx
	addl	$64, %eax
	cmpb	$0, 1394(%rsi,%rcx,8)
	cmovel	%ebx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %ebx
	subl	%eax, %ebx
	movslq	%ebx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rdi
	addq	$2, %rcx
	cmpq	$8, %rcx
	jne	.LBB5_49
	jmp	.LBB5_50
	.p2align	4, 0x90
.LBB5_46:                               # %.preheader317.split.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	136(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_47:                               # %.preheader317.split
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	1400(%rsi,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rdi), %ebp
	imull	%r9d, %ebp
	sarl	$4, %ebp
	addl	(%rdi), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	1392(%rsi,%rcx,8), %eax
	movl	$63, %ebx
	subl	%eax, %ebx
	addl	$64, %eax
	cmpb	$0, 1394(%rsi,%rcx,8)
	cmovel	%ebx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %ebx
	subl	%eax, %ebx
	movslq	%ebx, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rdi
	addq	$2, %rcx
	cmpq	$8, %rcx
	jne	.LBB5_47
.LBB5_50:                               # %.preheader316
                                        #   in Loop: Header=BB5_1 Depth=1
	cmpl	$2, %r8d
	movq	%r12, 312(%rsp)         # 8-byte Spill
	movq	%r13, 304(%rsp)         # 8-byte Spill
	jne	.LBB5_51
# BB#52:                                # %.preheader316.split.us.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	1464(%rsi), %xmm0       # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm9, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	movl	%r9d, %edi
	shll	$5, %edi
	subl	%r9d, %edi
	shrl	$4, %edi
	leal	21(%rdi), %ecx
	cmpl	$127, %ecx
	cmovael	%r14d, %ecx
	movzwl	1456(%rsi), %eax
	movl	$63, %ebp
	subl	%eax, %ebp
	addl	$64, %eax
	cmpb	$0, 1458(%rsi)
	minsd	%xmm5, %xmm0
	cmovel	%ebp, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rcx,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %ebp
	subl	%eax, %ebp
	movslq	%ebp, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ecx
	mulsd	entropy(,%rcx,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	movq	1480(%rsi), %xmm0       # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm9, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm7
	minsd	%xmm5, %xmm7
	addl	$31, %edi
	cmpl	$127, %edi
	cmovael	%r14d, %edi
	movzwl	1472(%rsi), %eax
	movl	$63, %ecx
	subl	%eax, %ecx
	addl	$64, %eax
	cmpb	$0, 1474(%rsi)
	cmovel	%ecx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm7, %xmm5
	mulsd	entropy(,%rdi,8), %xmm5
	xorpd	%xmm0, %xmm0
	subsd	%xmm5, %xmm0
	movl	$127, %ecx
	subl	%eax, %ecx
	movslq	%ecx, %rax
	mulsd	probability(,%rax,8), %xmm7
	xorl	$127, %edi
	mulsd	entropy(,%rdi,8), %xmm7
	subsd	%xmm7, %xmm0
	addsd	%xmm6, %xmm0
	movq	1496(%rsi), %xmm5       # xmm5 = mem[0],zero
	punpckldq	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1]
	subpd	%xmm9, %xmm5
	pshufd	$78, %xmm5, %xmm7       # xmm7 = xmm5[2,3,0,1]
	addpd	%xmm5, %xmm7
	mulsd	%xmm3, %xmm7
	movapd	%xmm4, %xmm6
	minsd	%xmm7, %xmm6
	leal	(%r9,%r9,4), %eax
	leal	(%rax,%rax,4), %r9d
	shrl	$4, %r9d
	addl	$50, %r9d
	cmpl	$127, %r9d
	cmovael	%r14d, %r9d
	jmp	.LBB5_53
	.p2align	4, 0x90
.LBB5_51:                               # %.preheader316.split.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	-128(%rsp), %rax        # 8-byte Reload
	leaq	(,%rax,8), %rbx
	movq	1464(%rsi), %xmm0       # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm9, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	movl	INIT_TRANSFORM_SIZE_P(%rbx,%rbx,2), %ecx
	imull	%r9d, %ecx
	sarl	$4, %ecx
	addl	INIT_TRANSFORM_SIZE_P+4(%rbx,%rbx,2), %ecx
	cmovsl	%r15d, %ecx
	cmpl	$128, %ecx
	cmovgel	%r14d, %ecx
	movzwl	1456(%rsi), %eax
	movl	$63, %edi
	subl	%eax, %edi
	addl	$64, %eax
	cmpb	$0, 1458(%rsi)
	minsd	%xmm5, %xmm0
	cmovel	%edi, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rcx,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %edi
	subl	%eax, %edi
	movslq	%edi, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ecx
	mulsd	entropy(,%rcx,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	movq	1480(%rsi), %xmm0       # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm9, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm7
	minsd	%xmm5, %xmm7
	movl	INIT_TRANSFORM_SIZE_P+8(%rbx,%rbx,2), %ecx
	imull	%r9d, %ecx
	sarl	$4, %ecx
	addl	INIT_TRANSFORM_SIZE_P+12(%rbx,%rbx,2), %ecx
	cmovsl	%r15d, %ecx
	cmpl	$128, %ecx
	cmovgel	%r14d, %ecx
	movzwl	1472(%rsi), %eax
	movl	$63, %edi
	subl	%eax, %edi
	addl	$64, %eax
	cmpb	$0, 1474(%rsi)
	cmovel	%edi, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm7, %xmm5
	mulsd	entropy(,%rcx,8), %xmm5
	xorpd	%xmm0, %xmm0
	subsd	%xmm5, %xmm0
	movl	$127, %edi
	subl	%eax, %edi
	movslq	%edi, %rax
	mulsd	probability(,%rax,8), %xmm7
	xorl	$127, %ecx
	mulsd	entropy(,%rcx,8), %xmm7
	subsd	%xmm7, %xmm0
	addsd	%xmm6, %xmm0
	movq	1496(%rsi), %xmm5       # xmm5 = mem[0],zero
	punpckldq	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1]
	subpd	%xmm9, %xmm5
	pshufd	$78, %xmm5, %xmm7       # xmm7 = xmm5[2,3,0,1]
	addpd	%xmm5, %xmm7
	mulsd	%xmm3, %xmm7
	movapd	%xmm4, %xmm6
	minsd	%xmm7, %xmm6
	imull	INIT_TRANSFORM_SIZE_P+16(%rbx,%rbx,2), %r9d
	sarl	$4, %r9d
	addl	INIT_TRANSFORM_SIZE_P+20(%rbx,%rbx,2), %r9d
	cmovsl	%r15d, %r9d
	cmpl	$128, %r9d
	cmovgel	%r14d, %r9d
.LBB5_53:                               # %.preheader315
                                        #   in Loop: Header=BB5_1 Depth=1
	movzwl	1488(%rsi), %eax
	movl	$63, %ecx
	subl	%eax, %ecx
	addl	$64, %eax
	cmpb	$0, 1490(%rsi)
	cmovel	%ecx, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm6, %xmm5
	movl	%r9d, %ecx
	mulsd	entropy(,%rcx,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %ecx
	subl	%eax, %ecx
	movslq	%ecx, %rax
	mulsd	probability(,%rax,8), %xmm6
	xorl	$127, %r9d
	mulsd	entropy(,%r9,8), %xmm6
	subsd	%xmm6, %xmm7
	addsd	%xmm0, %xmm7
	movq	img(%rip), %r8
	movl	20(%r8), %r9d
	movl	36(%r8), %r13d
	testl	%r13d, %r13d
	cmovsl	%r15d, %r13d
	cmpl	$2, %r9d
	jne	.LBB5_54
# BB#56:                                # %.preheader314.split.us.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	8(%r10), %xmm0          # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm9, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	imull	$13, %r13d, %eax
	shrl	$4, %eax
	addl	$41, %eax
	cmpl	$127, %eax
	cmovael	%r14d, %eax
	movzwl	(%r10), %ecx
	movl	$63, %edi
	subl	%ecx, %edi
	addl	$64, %ecx
	cmpb	$0, 2(%r10)
	cmovel	%edi, %ecx
	movslq	%ecx, %rcx
	movsd	probability(,%rcx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rax,8), %xmm5
	xorpd	%xmm6, %xmm6
	subsd	%xmm5, %xmm6
	movl	$127, %edi
	subl	%ecx, %edi
	movslq	%edi, %rcx
	mulsd	probability(,%rcx,8), %xmm0
	xorl	$127, %eax
	mulsd	entropy(,%rax,8), %xmm0
	subsd	%xmm0, %xmm6
	addsd	%xmm6, %xmm7
	movq	24(%r10), %xmm0         # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm9, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	leal	(%r13,%r13,2), %eax
	shrl	$4, %eax
	addl	$62, %eax
	cmpl	$127, %eax
	cmovael	%r14d, %eax
	movzwl	16(%r10), %ecx
	movl	$63, %edi
	subl	%ecx, %edi
	addl	$64, %ecx
	cmpb	$0, 18(%r10)
	cmovel	%edi, %ecx
	movslq	%ecx, %rcx
	movsd	probability(,%rcx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rax,8), %xmm5
	xorpd	%xmm6, %xmm6
	subsd	%xmm5, %xmm6
	movl	$127, %edi
	subl	%ecx, %edi
	movslq	%edi, %rcx
	mulsd	probability(,%rcx,8), %xmm0
	xorl	$127, %eax
	mulsd	entropy(,%rax,8), %xmm0
	subsd	%xmm0, %xmm6
	addsd	%xmm7, %xmm6
	xorl	%eax, %eax
	movq	112(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_57:                               # %.preheader314.split.us
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	40(%r10,%rax,8), %xmm0  # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rcx), %edi
	imull	%r13d, %edi
	sarl	$4, %edi
	addl	(%rcx), %edi
	cmovsl	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r14d, %edi
	movzwl	32(%r10,%rax,8), %ebp
	movl	$63, %ebx
	subl	%ebp, %ebx
	addl	$64, %ebp
	cmpb	$0, 34(%r10,%rax,8)
	cmovel	%ebx, %ebp
	movslq	%ebp, %rbp
	movsd	probability(,%rbp,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rdi,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %ebx
	subl	%ebp, %ebx
	movslq	%ebx, %rbp
	mulsd	probability(,%rbp,8), %xmm0
	xorl	$127, %edi
	mulsd	entropy(,%rdi,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rcx
	addq	$2, %rax
	cmpq	$8, %rax
	jne	.LBB5_57
	jmp	.LBB5_58
	.p2align	4, 0x90
.LBB5_54:                               # %.preheader314.split.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	-128(%rsp), %rax        # 8-byte Reload
	shlq	$4, %rax
	movq	8(%r10), %xmm0          # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm9, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	movl	INIT_IPR_P(%rax), %ecx
	imull	%r13d, %ecx
	sarl	$4, %ecx
	addl	INIT_IPR_P+4(%rax), %ecx
	cmovsl	%r15d, %ecx
	cmpl	$128, %ecx
	cmovgel	%r14d, %ecx
	movzwl	(%r10), %edi
	movl	$63, %ebp
	subl	%edi, %ebp
	addl	$64, %edi
	cmpb	$0, 2(%r10)
	minsd	%xmm5, %xmm0
	cmovel	%ebp, %edi
	movslq	%edi, %rdi
	movsd	probability(,%rdi,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rcx,8), %xmm5
	xorpd	%xmm6, %xmm6
	subsd	%xmm5, %xmm6
	movl	$127, %ebp
	subl	%edi, %ebp
	movslq	%ebp, %rdi
	mulsd	probability(,%rdi,8), %xmm0
	xorl	$127, %ecx
	mulsd	entropy(,%rcx,8), %xmm0
	subsd	%xmm0, %xmm6
	addsd	%xmm6, %xmm7
	movq	24(%r10), %xmm0         # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm9, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	INIT_IPR_P+8(%rax), %ecx
	imull	%r13d, %ecx
	sarl	$4, %ecx
	addl	INIT_IPR_P+12(%rax), %ecx
	cmovsl	%r15d, %ecx
	cmpl	$128, %ecx
	cmovgel	%r14d, %ecx
	movzwl	16(%r10), %eax
	movl	$63, %edi
	subl	%eax, %edi
	addl	$64, %eax
	cmpb	$0, 18(%r10)
	cmovel	%edi, %eax
	cltq
	movsd	probability(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rcx,8), %xmm5
	xorpd	%xmm6, %xmm6
	subsd	%xmm5, %xmm6
	movl	$127, %edi
	subl	%eax, %edi
	movslq	%edi, %rax
	mulsd	probability(,%rax,8), %xmm0
	xorl	$127, %ecx
	mulsd	entropy(,%rcx,8), %xmm0
	subsd	%xmm0, %xmm6
	addsd	%xmm7, %xmm6
	xorl	%eax, %eax
	movq	120(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_55:                               # %.preheader314.split
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	40(%r10,%rax,8), %xmm0  # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rcx), %edi
	imull	%r13d, %edi
	sarl	$4, %edi
	addl	(%rcx), %edi
	cmovsl	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r14d, %edi
	movzwl	32(%r10,%rax,8), %ebp
	movl	$63, %ebx
	subl	%ebp, %ebx
	addl	$64, %ebp
	cmpb	$0, 34(%r10,%rax,8)
	cmovel	%ebx, %ebp
	movslq	%ebp, %rbp
	movsd	probability(,%rbp,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rdi,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %ebx
	subl	%ebp, %ebx
	movslq	%ebx, %rbp
	mulsd	probability(,%rbp,8), %xmm0
	xorl	$127, %edi
	mulsd	entropy(,%rdi,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rcx
	addq	$2, %rax
	cmpq	$8, %rax
	jne	.LBB5_55
.LBB5_58:                               # %.preheader313
                                        #   in Loop: Header=BB5_1 Depth=1
	xorl	%eax, %eax
	cmpl	$2, %r9d
	jne	.LBB5_59
# BB#65:                                # %.preheader301.us.preheader.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	80(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB5_66:                               # %.preheader301.us.preheader
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	104(%r10,%rax,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rcx), %edi
	imull	%r13d, %edi
	sarl	$4, %edi
	addl	(%rcx), %edi
	cmovsl	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r14d, %edi
	movzwl	96(%r10,%rax,8), %ebp
	movl	$63, %ebx
	subl	%ebp, %ebx
	addl	$64, %ebp
	cmpb	$0, 98(%r10,%rax,8)
	cmovel	%ebx, %ebp
	movslq	%ebp, %rbp
	movsd	probability(,%rbp,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rdi,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %ebx
	subl	%ebp, %ebx
	movslq	%ebx, %rbp
	mulsd	probability(,%rbp,8), %xmm0
	xorl	$127, %edi
	mulsd	entropy(,%rdi,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rcx
	addq	$2, %rax
	cmpq	$8, %rax
	jne	.LBB5_66
# BB#67:                                # %.us-lcssa378.us.us.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_68:                               # %.us-lcssa378.us.us
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	168(%r10,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rax), %edi
	imull	%r13d, %edi
	sarl	$4, %edi
	addl	(%rax), %edi
	cmovsl	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r14d, %edi
	movzwl	160(%r10,%rcx,8), %edx
	movl	$63, %esi
	subl	%edx, %esi
	addl	$64, %edx
	cmpb	$0, 162(%r10,%rcx,8)
	cmovel	%esi, %edx
	movslq	%edx, %rdx
	movsd	probability(,%rdx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rdi,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	mulsd	probability(,%rdx,8), %xmm0
	xorl	$127, %edi
	mulsd	entropy(,%rdi,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$2, %rcx
	addq	$8, %rax
	cmpq	$8, %rcx
	jne	.LBB5_68
# BB#69:                                # %.us-lcssa378.us.us.1.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_70:                               # %.us-lcssa378.us.us.1
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	232(%r10,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rax), %edi
	imull	%r13d, %edi
	sarl	$4, %edi
	addl	(%rax), %edi
	cmovsl	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r14d, %edi
	movzwl	224(%r10,%rcx,8), %edx
	movl	$63, %esi
	subl	%edx, %esi
	addl	$64, %edx
	cmpb	$0, 226(%r10,%rcx,8)
	cmovel	%esi, %edx
	movslq	%edx, %rdx
	movsd	probability(,%rdx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rdi,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	mulsd	probability(,%rdx,8), %xmm0
	xorl	$127, %edi
	mulsd	entropy(,%rdi,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$2, %rcx
	addq	$8, %rax
	cmpq	$8, %rcx
	jne	.LBB5_70
	jmp	.LBB5_71
	.p2align	4, 0x90
.LBB5_59:                               # %.preheader301.preheader.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	104(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_60:                               # %.preheader301.preheader
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	104(%r10,%rax,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rcx), %edi
	imull	%r13d, %edi
	sarl	$4, %edi
	addl	(%rcx), %edi
	cmovsl	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r14d, %edi
	movzwl	96(%r10,%rax,8), %ebp
	movl	$63, %ebx
	subl	%ebp, %ebx
	addl	$64, %ebp
	cmpb	$0, 98(%r10,%rax,8)
	cmovel	%ebx, %ebp
	movslq	%ebp, %rbp
	movsd	probability(,%rbp,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rdi,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %ebx
	subl	%ebp, %ebx
	movslq	%ebx, %rbp
	mulsd	probability(,%rbp,8), %xmm0
	xorl	$127, %edi
	mulsd	entropy(,%rdi,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rcx
	addq	$2, %rax
	cmpq	$8, %rax
	jne	.LBB5_60
# BB#61:                                # %.us-lcssa378.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	96(%rsp), %rax          # 8-byte Reload
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_62:                               # %.us-lcssa378
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	168(%r10,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rax), %edi
	imull	%r13d, %edi
	sarl	$4, %edi
	addl	(%rax), %edi
	cmovsl	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r14d, %edi
	movzwl	160(%r10,%rcx,8), %edx
	movl	$63, %esi
	subl	%edx, %esi
	addl	$64, %edx
	cmpb	$0, 162(%r10,%rcx,8)
	cmovel	%esi, %edx
	movslq	%edx, %rdx
	movsd	probability(,%rdx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rdi,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	mulsd	probability(,%rdx,8), %xmm0
	xorl	$127, %edi
	mulsd	entropy(,%rdi,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$2, %rcx
	addq	$8, %rax
	cmpq	$8, %rcx
	jne	.LBB5_62
# BB#63:                                # %.us-lcssa378.1.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_64:                               # %.us-lcssa378.1
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	232(%r10,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rax), %edi
	imull	%r13d, %edi
	sarl	$4, %edi
	addl	(%rax), %edi
	cmovsl	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r14d, %edi
	movzwl	224(%r10,%rcx,8), %edx
	movl	$63, %esi
	subl	%edx, %esi
	addl	$64, %edx
	cmpb	$0, 226(%r10,%rcx,8)
	cmovel	%esi, %edx
	movslq	%edx, %rdx
	movsd	probability(,%rdx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rdi,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	mulsd	probability(,%rdx,8), %xmm0
	xorl	$127, %edi
	mulsd	entropy(,%rdi,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$2, %rcx
	addq	$8, %rax
	cmpq	$8, %rcx
	jne	.LBB5_64
.LBB5_71:                               # %.preheader312
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	%r8, 296(%rsp)          # 8-byte Spill
	movq	%r11, 256(%rsp)         # 8-byte Spill
	movl	%r9d, -116(%rsp)        # 4-byte Spill
	cmpl	$2, %r9d
	jne	.LBB5_72
# BB#76:                                # %.preheader300.us.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	-64(%rsp), %r8          # 8-byte Reload
	movq	48(%rsp), %r9           # 8-byte Reload
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB5_77:                               # %.preheader300.us
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_78 Depth 3
	movl	$4, %r10d
	movq	%r8, %rax
	movq	%r9, %rbp
	.p2align	4, 0x90
.LBB5_78:                               #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_77 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %xmm0           # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rbp), %edi
	imull	%r13d, %edi
	sarl	$4, %edi
	addl	(%rbp), %edi
	cmovsl	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r14d, %edi
	movzwl	-8(%rax), %r12d
	movl	$63, %r15d
	subl	%r12d, %r15d
	addl	$64, %r12d
	cmpb	$0, -6(%rax)
	cmovel	%r15d, %r12d
	xorl	%r15d, %r15d
	movslq	%r12d, %rcx
	movsd	probability(,%rcx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rdi,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %ebx
	subl	%ecx, %ebx
	movslq	%ebx, %rcx
	mulsd	probability(,%rcx,8), %xmm0
	xorl	$127, %edi
	mulsd	entropy(,%rdi,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rbp
	addq	$16, %rax
	decq	%r10
	jne	.LBB5_78
# BB#79:                                # %.us-lcssa389.us.us
                                        #   in Loop: Header=BB5_77 Depth=2
	incq	%r11
	addq	$32, %r9
	addq	$64, %r8
	cmpq	$10, %r11
	jne	.LBB5_77
	jmp	.LBB5_80
	.p2align	4, 0x90
.LBB5_72:                               # %.preheader300.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	-64(%rsp), %rax         # 8-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB5_73:                               # %.preheader300
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_74 Depth 3
	movl	$4, %edi
	movq	%rax, %rcx
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB5_74:                               #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_73 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rbx), %r11d
	imull	%r13d, %r11d
	sarl	$4, %r11d
	addl	(%rbx), %r11d
	cmovsl	%r15d, %r11d
	cmpl	$128, %r11d
	cmovgel	%r14d, %r11d
	movzwl	-8(%rcx), %edx
	movl	$63, %esi
	subl	%edx, %esi
	addl	$64, %edx
	cmpb	$0, -6(%rcx)
	cmovel	%esi, %edx
	movslq	%edx, %rdx
	movsd	probability(,%rdx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%r11,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	mulsd	probability(,%rdx,8), %xmm0
	xorl	$127, %r11d
	mulsd	entropy(,%r11,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rbx
	addq	$16, %rcx
	decq	%rdi
	jne	.LBB5_74
# BB#75:                                # %.us-lcssa389
                                        #   in Loop: Header=BB5_73 Depth=2
	incq	%r8
	addq	$32, %rbp
	addq	$64, %rax
	cmpq	$10, %r8
	jne	.LBB5_73
.LBB5_80:                               # %.preheader311
                                        #   in Loop: Header=BB5_1 Depth=1
	movl	-116(%rsp), %r9d        # 4-byte Reload
	cmpl	$2, %r9d
	jne	.LBB5_81
# BB#85:                                # %.preheader299.us.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	-72(%rsp), %r8          # 8-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB5_86:                               # %.preheader299.us
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_87 Depth 3
	movl	$15, %ebx
	movq	%r8, %rcx
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB5_87:                               #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_86 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rbp), %edi
	imull	%r13d, %edi
	sarl	$4, %edi
	addl	(%rbp), %edi
	cmovsl	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r14d, %edi
	movzwl	-8(%rcx), %edx
	movl	$63, %esi
	subl	%edx, %esi
	addl	$64, %edx
	cmpb	$0, -6(%rcx)
	cmovel	%esi, %edx
	movslq	%edx, %rdx
	movsd	probability(,%rdx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rdi,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	mulsd	probability(,%rdx,8), %xmm0
	xorl	$127, %edi
	mulsd	entropy(,%rdi,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rbp
	addq	$16, %rcx
	decq	%rbx
	jne	.LBB5_87
# BB#88:                                # %.us-lcssa400.us.us
                                        #   in Loop: Header=BB5_86 Depth=2
	incq	%r11
	addq	$120, %rax
	addq	$240, %r8
	cmpq	$10, %r11
	jne	.LBB5_86
	jmp	.LBB5_89
	.p2align	4, 0x90
.LBB5_81:                               # %.preheader299.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	-72(%rsp), %r8          # 8-byte Reload
	movq	40(%rsp), %rax          # 8-byte Reload
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB5_82:                               # %.preheader299
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_83 Depth 3
	movl	$15, %ebx
	movq	%r8, %rcx
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB5_83:                               #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_82 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rbp), %edi
	imull	%r13d, %edi
	sarl	$4, %edi
	addl	(%rbp), %edi
	cmovsl	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r14d, %edi
	movzwl	-8(%rcx), %edx
	movl	$63, %esi
	subl	%edx, %esi
	addl	$64, %edx
	cmpb	$0, -6(%rcx)
	cmovel	%esi, %edx
	movslq	%edx, %rdx
	movsd	probability(,%rdx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rdi,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	mulsd	probability(,%rdx,8), %xmm0
	xorl	$127, %edi
	mulsd	entropy(,%rdi,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rbp
	addq	$16, %rcx
	decq	%rbx
	jne	.LBB5_83
# BB#84:                                # %.us-lcssa400
                                        #   in Loop: Header=BB5_82 Depth=2
	incq	%r11
	addq	$120, %rax
	addq	$240, %r8
	cmpq	$10, %r11
	jne	.LBB5_82
.LBB5_89:                               # %.preheader310
                                        #   in Loop: Header=BB5_1 Depth=1
	cmpl	$2, %r9d
	jne	.LBB5_90
# BB#94:                                # %.preheader298.us.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	-80(%rsp), %r8          # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB5_95:                               # %.preheader298.us
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_96 Depth 3
	movl	$15, %ebx
	movq	%r8, %rcx
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB5_96:                               #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_95 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rbp), %edi
	imull	%r13d, %edi
	sarl	$4, %edi
	addl	(%rbp), %edi
	cmovsl	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r14d, %edi
	movzwl	-8(%rcx), %edx
	movl	$63, %esi
	subl	%edx, %esi
	addl	$64, %edx
	cmpb	$0, -6(%rcx)
	cmovel	%esi, %edx
	movslq	%edx, %rdx
	movsd	probability(,%rdx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rdi,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	mulsd	probability(,%rdx,8), %xmm0
	xorl	$127, %edi
	mulsd	entropy(,%rdi,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rbp
	addq	$16, %rcx
	decq	%rbx
	jne	.LBB5_96
# BB#97:                                # %.us-lcssa411.us.us
                                        #   in Loop: Header=BB5_95 Depth=2
	incq	%r11
	addq	$120, %rax
	addq	$240, %r8
	cmpq	$10, %r11
	jne	.LBB5_95
	jmp	.LBB5_98
	.p2align	4, 0x90
.LBB5_90:                               # %.preheader298.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	-80(%rsp), %r8          # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB5_91:                               # %.preheader298
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_92 Depth 3
	movl	$15, %ebx
	movq	%r8, %rcx
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB5_92:                               #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_91 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rbp), %edi
	imull	%r13d, %edi
	sarl	$4, %edi
	addl	(%rbp), %edi
	cmovsl	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r14d, %edi
	movzwl	-8(%rcx), %edx
	movl	$63, %esi
	subl	%edx, %esi
	addl	$64, %edx
	cmpb	$0, -6(%rcx)
	cmovel	%esi, %edx
	movslq	%edx, %rdx
	movsd	probability(,%rdx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rdi,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	mulsd	probability(,%rdx,8), %xmm0
	xorl	$127, %edi
	mulsd	entropy(,%rdi,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rbp
	addq	$16, %rcx
	decq	%rbx
	jne	.LBB5_92
# BB#93:                                # %.us-lcssa411
                                        #   in Loop: Header=BB5_91 Depth=2
	incq	%r11
	addq	$120, %rax
	addq	$240, %r8
	cmpq	$10, %r11
	jne	.LBB5_91
.LBB5_98:                               # %.preheader309
                                        #   in Loop: Header=BB5_1 Depth=1
	cmpl	$2, %r9d
	jne	.LBB5_99
# BB#103:                               # %.preheader297.us.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	-88(%rsp), %r8          # 8-byte Reload
	movq	(%rsp), %rax            # 8-byte Reload
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB5_104:                              # %.preheader297.us
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_105 Depth 3
	movl	$5, %ebx
	movq	%r8, %rcx
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB5_105:                              #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_104 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rdi), %ebp
	imull	%r13d, %ebp
	sarl	$4, %ebp
	addl	(%rdi), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	-8(%rcx), %edx
	movl	$63, %esi
	subl	%edx, %esi
	addl	$64, %edx
	cmpb	$0, -6(%rcx)
	cmovel	%esi, %edx
	movslq	%edx, %rdx
	movsd	probability(,%rdx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	mulsd	probability(,%rdx,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rdi
	addq	$16, %rcx
	decq	%rbx
	jne	.LBB5_105
# BB#106:                               # %.us-lcssa422.us.us
                                        #   in Loop: Header=BB5_104 Depth=2
	incq	%r11
	addq	$40, %rax
	addq	$80, %r8
	cmpq	$10, %r11
	jne	.LBB5_104
	jmp	.LBB5_107
	.p2align	4, 0x90
.LBB5_99:                               # %.preheader297.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	-88(%rsp), %r8          # 8-byte Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB5_100:                              # %.preheader297
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_101 Depth 3
	movl	$5, %ebx
	movq	%r8, %rcx
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB5_101:                              #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_100 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rbp), %edi
	imull	%r13d, %edi
	sarl	$4, %edi
	addl	(%rbp), %edi
	cmovsl	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r14d, %edi
	movzwl	-8(%rcx), %edx
	movl	$63, %esi
	subl	%edx, %esi
	addl	$64, %edx
	cmpb	$0, -6(%rcx)
	cmovel	%esi, %edx
	movslq	%edx, %rdx
	movsd	probability(,%rdx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rdi,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	mulsd	probability(,%rdx,8), %xmm0
	xorl	$127, %edi
	mulsd	entropy(,%rdi,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rbp
	addq	$16, %rcx
	decq	%rbx
	jne	.LBB5_101
# BB#102:                               # %.us-lcssa422
                                        #   in Loop: Header=BB5_100 Depth=2
	incq	%r11
	addq	$40, %rax
	addq	$80, %r8
	cmpq	$10, %r11
	jne	.LBB5_100
.LBB5_107:                              # %.preheader308
                                        #   in Loop: Header=BB5_1 Depth=1
	cmpl	$2, %r9d
	jne	.LBB5_108
# BB#112:                               # %.preheader296.us.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	-96(%rsp), %r8          # 8-byte Reload
	movq	-16(%rsp), %rax         # 8-byte Reload
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB5_113:                              # %.preheader296.us
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_114 Depth 3
	movl	$5, %ebx
	movq	%r8, %rcx
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB5_114:                              #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_113 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rdi), %ebp
	imull	%r13d, %ebp
	sarl	$4, %ebp
	addl	(%rdi), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	-8(%rcx), %edx
	movl	$63, %esi
	subl	%edx, %esi
	addl	$64, %edx
	cmpb	$0, -6(%rcx)
	cmovel	%esi, %edx
	movslq	%edx, %rdx
	movsd	probability(,%rdx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	mulsd	probability(,%rdx,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rdi
	addq	$16, %rcx
	decq	%rbx
	jne	.LBB5_114
# BB#115:                               # %.us-lcssa433.us.us
                                        #   in Loop: Header=BB5_113 Depth=2
	incq	%r11
	addq	$40, %rax
	addq	$80, %r8
	cmpq	$10, %r11
	jne	.LBB5_113
	jmp	.LBB5_116
	.p2align	4, 0x90
.LBB5_108:                              # %.preheader296.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	-96(%rsp), %r8          # 8-byte Reload
	movq	-8(%rsp), %rax          # 8-byte Reload
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB5_109:                              # %.preheader296
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_110 Depth 3
	movl	$5, %ebx
	movq	%r8, %rcx
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB5_110:                              #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_109 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rbp), %edi
	imull	%r13d, %edi
	sarl	$4, %edi
	addl	(%rbp), %edi
	cmovsl	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r14d, %edi
	movzwl	-8(%rcx), %edx
	movl	$63, %esi
	subl	%edx, %esi
	addl	$64, %edx
	cmpb	$0, -6(%rcx)
	cmovel	%esi, %edx
	movslq	%edx, %rdx
	movsd	probability(,%rdx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rdi,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	mulsd	probability(,%rdx,8), %xmm0
	xorl	$127, %edi
	mulsd	entropy(,%rdi,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rbp
	addq	$16, %rcx
	decq	%rbx
	jne	.LBB5_110
# BB#111:                               # %.us-lcssa433
                                        #   in Loop: Header=BB5_109 Depth=2
	incq	%r11
	addq	$40, %rax
	addq	$80, %r8
	cmpq	$10, %r11
	jne	.LBB5_109
.LBB5_116:                              # %.preheader307
                                        #   in Loop: Header=BB5_1 Depth=1
	cmpl	$2, %r9d
	jne	.LBB5_117
# BB#121:                               # %.preheader295.us.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	-104(%rsp), %r8         # 8-byte Reload
	movq	-32(%rsp), %rax         # 8-byte Reload
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB5_122:                              # %.preheader295.us
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_123 Depth 3
	movl	$15, %ebx
	movq	%r8, %rcx
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB5_123:                              #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_122 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rdi), %ebp
	imull	%r13d, %ebp
	sarl	$4, %ebp
	addl	(%rdi), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	-8(%rcx), %edx
	movl	$63, %esi
	subl	%edx, %esi
	addl	$64, %edx
	cmpb	$0, -6(%rcx)
	cmovel	%esi, %edx
	movslq	%edx, %rdx
	movsd	probability(,%rdx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	mulsd	probability(,%rdx,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rdi
	addq	$16, %rcx
	decq	%rbx
	jne	.LBB5_123
# BB#124:                               # %.us-lcssa444.us.us
                                        #   in Loop: Header=BB5_122 Depth=2
	incq	%r11
	addq	$120, %rax
	addq	$240, %r8
	cmpq	$10, %r11
	jne	.LBB5_122
	jmp	.LBB5_125
	.p2align	4, 0x90
.LBB5_117:                              # %.preheader295.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	-104(%rsp), %r8         # 8-byte Reload
	movq	-24(%rsp), %rax         # 8-byte Reload
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB5_118:                              # %.preheader295
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_119 Depth 3
	movl	$15, %ebx
	movq	%r8, %rcx
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB5_119:                              #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_118 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rbp), %edi
	imull	%r13d, %edi
	sarl	$4, %edi
	addl	(%rbp), %edi
	cmovsl	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r14d, %edi
	movzwl	-8(%rcx), %edx
	movl	$63, %esi
	subl	%edx, %esi
	addl	$64, %edx
	cmpb	$0, -6(%rcx)
	cmovel	%esi, %edx
	movslq	%edx, %rdx
	movsd	probability(,%rdx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rdi,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	mulsd	probability(,%rdx,8), %xmm0
	xorl	$127, %edi
	mulsd	entropy(,%rdi,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rbp
	addq	$16, %rcx
	decq	%rbx
	jne	.LBB5_119
# BB#120:                               # %.us-lcssa444
                                        #   in Loop: Header=BB5_118 Depth=2
	incq	%r11
	addq	$120, %rax
	addq	$240, %r8
	cmpq	$10, %r11
	jne	.LBB5_118
.LBB5_125:                              # %.preheader306
                                        #   in Loop: Header=BB5_1 Depth=1
	cmpl	$2, %r9d
	jne	.LBB5_126
# BB#130:                               # %.preheader.us.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	-112(%rsp), %r8         # 8-byte Reload
	movq	-48(%rsp), %rax         # 8-byte Reload
	xorl	%r9d, %r9d
	movq	-56(%rsp), %r10         # 8-byte Reload
	movq	256(%rsp), %r11         # 8-byte Reload
	.p2align	4, 0x90
.LBB5_131:                              # %.preheader.us
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_132 Depth 3
	movl	$15, %ebx
	movq	%r8, %rcx
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB5_132:                              #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_131 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subpd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm5       # xmm5 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm5
	mulsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm0
	minsd	%xmm5, %xmm0
	movl	-4(%rdi), %ebp
	imull	%r13d, %ebp
	sarl	$4, %ebp
	addl	(%rdi), %ebp
	cmovsl	%r15d, %ebp
	cmpl	$128, %ebp
	cmovgel	%r14d, %ebp
	movzwl	-8(%rcx), %edx
	movl	$63, %esi
	subl	%edx, %esi
	addl	$64, %edx
	cmpb	$0, -6(%rcx)
	cmovel	%esi, %edx
	movslq	%edx, %rdx
	movsd	probability(,%rdx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	mulsd	entropy(,%rbp,8), %xmm5
	xorpd	%xmm7, %xmm7
	subsd	%xmm5, %xmm7
	movl	$127, %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	mulsd	probability(,%rdx,8), %xmm0
	xorl	$127, %ebp
	mulsd	entropy(,%rbp,8), %xmm0
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm6
	addq	$8, %rdi
	addq	$16, %rcx
	decq	%rbx
	jne	.LBB5_132
# BB#133:                               # %.us-lcssa455.us.us
                                        #   in Loop: Header=BB5_131 Depth=2
	incq	%r9
	addq	$120, %rax
	addq	$240, %r8
	cmpq	$10, %r9
	jne	.LBB5_131
	jmp	.LBB5_134
.LBB5_138:
	addq	$336, %rsp              # imm = 0x150
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	GetCtxModelNumber, .Lfunc_end5-GetCtxModelNumber
	.cfi_endproc

	.globl	store_contexts
	.p2align	4, 0x90
	.type	store_contexts,@function
store_contexts:                         # @store_contexts
	.cfi_startproc
# BB#0:
	movq	input(%rip), %rax
	cmpl	$0, 5092(%rax)
	je	.LBB6_1
# BB#2:
	movq	img(%rip), %rcx
	movq	14216(%rcx), %r8
	movl	12(%r8), %eax
	cltd
	idivl	num_mb_per_slice(%rip)
	movq	initialized(%rip), %rdx
	movslq	15312(%rcx), %rsi
	movq	(%rdx,%rsi,8), %rdx
	movslq	20(%rcx), %rcx
	movq	(%rdx,%rcx,8), %rdx
	movslq	%eax, %rdi
	movl	$1, (%rdx,%rdi,4)
	shlq	$2, %rdi
	movq	model_number(%rip), %rax
	movq	(%rax,%rsi,8), %rax
	addq	(%rax,%rcx,8), %rdi
	movq	32(%r8), %rsi
	movq	40(%r8), %rdx
	jmp	GetCtxModelNumber       # TAILCALL
.LBB6_1:
	retq
.Lfunc_end6:
	.size	store_contexts, .Lfunc_end6-store_contexts
	.cfi_endproc

	.globl	update_field_frame_contexts
	.p2align	4, 0x90
	.type	update_field_frame_contexts,@function
update_field_frame_contexts:            # @update_field_frame_contexts
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 16
.Lcfi33:
	.cfi_offset %rbx, -16
	testl	%edi, %edi
	movq	initialized(%rip), %r9
	movq	model_number(%rip), %r8
	movl	number_of_slices(%rip), %edx
	je	.LBB7_6
# BB#1:                                 # %.preheader31.preheader
	testl	%edx, %edx
	jle	.LBB7_5
# BB#2:                                 # %.lr.ph37
	movq	(%r9), %rax
	movq	8(%r9), %rcx
	movq	(%rcx), %r10
	movq	(%rax), %r11
	movq	(%r8), %rax
	movq	8(%r8), %rdx
	movq	(%rdx), %rdx
	movq	(%rax), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB7_3:                                # =>This Inner Loop Header: Depth=1
	movl	%edi, %eax
	sarl	%eax
	cltq
	movl	(%r10,%rax,4), %ecx
	movl	%ecx, (%r11,%rdi,4)
	movl	(%rdx,%rax,4), %eax
	movl	%eax, (%rsi,%rdi,4)
	incq	%rdi
	movslq	number_of_slices(%rip), %rax
	cmpq	%rax, %rdi
	jl	.LBB7_3
# BB#4:                                 # %._crit_edge38
	testl	%eax, %eax
	jle	.LBB7_5
# BB#18:                                # %.lr.ph37.1
	movq	(%r9), %rax
	movq	8(%r9), %rcx
	movq	8(%rcx), %r10
	movq	8(%rax), %rcx
	movq	(%r8), %rax
	movq	8(%r8), %rdx
	movq	8(%rdx), %rdx
	movq	8(%rax), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB7_19:                               # =>This Inner Loop Header: Depth=1
	movl	%edi, %eax
	sarl	%eax
	cltq
	movl	(%r10,%rax,4), %ebx
	movl	%ebx, (%rcx,%rdi,4)
	movl	(%rdx,%rax,4), %eax
	movl	%eax, (%rsi,%rdi,4)
	incq	%rdi
	movslq	number_of_slices(%rip), %rax
	cmpq	%rax, %rdi
	jl	.LBB7_19
# BB#20:                                # %._crit_edge38.1
	testl	%eax, %eax
	jle	.LBB7_5
# BB#21:                                # %.lr.ph37.2
	movq	(%r9), %rax
	movq	8(%r9), %rcx
	movq	16(%rcx), %r10
	movq	16(%rax), %rcx
	movq	(%r8), %rax
	movq	8(%r8), %rdx
	movq	16(%rdx), %rdx
	movq	16(%rax), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB7_22:                               # =>This Inner Loop Header: Depth=1
	movl	%edi, %eax
	sarl	%eax
	cltq
	movl	(%r10,%rax,4), %ebx
	movl	%ebx, (%rcx,%rdi,4)
	movl	(%rdx,%rax,4), %eax
	movl	%eax, (%rsi,%rdi,4)
	incq	%rdi
	movslq	number_of_slices(%rip), %rax
	cmpq	%rax, %rdi
	jl	.LBB7_22
# BB#23:                                # %._crit_edge38.2
	testl	%eax, %eax
	jle	.LBB7_5
# BB#24:                                # %.lr.ph37.3
	movq	(%r9), %rcx
	movq	8(%r9), %rax
	movq	24(%rax), %r9
	movq	24(%rcx), %rcx
	movq	(%r8), %rsi
	movq	8(%r8), %rdx
	movq	24(%rdx), %rdx
	movq	24(%rsi), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB7_25:                               # =>This Inner Loop Header: Depth=1
	movl	%edi, %ebx
	sarl	%ebx
	movslq	%ebx, %rbx
	movl	(%r9,%rbx,4), %eax
	movl	%eax, (%rcx,%rdi,4)
	movl	(%rdx,%rbx,4), %eax
	movl	%eax, (%rsi,%rdi,4)
	incq	%rdi
	movslq	number_of_slices(%rip), %rax
	cmpq	%rax, %rdi
	jl	.LBB7_25
	jmp	.LBB7_5
.LBB7_6:                                # %.preheader.preheader
	leal	1(%rdx), %eax
	sarl	%eax
	testl	%eax, %eax
	jle	.LBB7_9
# BB#7:                                 # %.lr.ph
	movq	(%r9), %rax
	movq	8(%r9), %rcx
	movq	(%rax), %r10
	movq	(%rcx), %r11
	movq	(%r8), %rax
	movq	8(%r8), %rcx
	movq	(%rax), %rax
	movq	(%rcx), %rcx
	xorl	%esi, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB7_8:                                # =>This Inner Loop Header: Depth=1
	movslq	%esi, %rsi
	movl	(%r10,%rsi,4), %edx
	movl	%edx, (%r11,%rdi,4)
	movl	(%rax,%rsi,4), %edx
	movl	%edx, (%rcx,%rdi,4)
	incq	%rdi
	movl	number_of_slices(%rip), %edx
	leal	1(%rdx), %ebx
	sarl	%ebx
	movslq	%ebx, %rbx
	addl	$2, %esi
	cmpq	%rbx, %rdi
	jl	.LBB7_8
.LBB7_9:                                # %._crit_edge
	leal	1(%rdx), %eax
	sarl	%eax
	testl	%eax, %eax
	jle	.LBB7_12
# BB#10:                                # %.lr.ph.1
	movq	(%r9), %rax
	movq	8(%r9), %rcx
	movq	8(%rax), %r10
	movq	8(%rcx), %r11
	movq	(%r8), %rax
	movq	8(%r8), %rcx
	movq	8(%rax), %rax
	movq	8(%rcx), %rcx
	xorl	%esi, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB7_11:                               # =>This Inner Loop Header: Depth=1
	movslq	%esi, %rsi
	movl	(%r10,%rsi,4), %edx
	movl	%edx, (%r11,%rdi,4)
	movl	(%rax,%rsi,4), %edx
	movl	%edx, (%rcx,%rdi,4)
	incq	%rdi
	movl	number_of_slices(%rip), %edx
	leal	1(%rdx), %ebx
	sarl	%ebx
	movslq	%ebx, %rbx
	addl	$2, %esi
	cmpq	%rbx, %rdi
	jl	.LBB7_11
.LBB7_12:                               # %._crit_edge.1
	leal	1(%rdx), %eax
	sarl	%eax
	testl	%eax, %eax
	jle	.LBB7_15
# BB#13:                                # %.lr.ph.2
	movq	(%r9), %rax
	movq	8(%r9), %rcx
	movq	16(%rax), %r10
	movq	16(%rcx), %r11
	movq	(%r8), %rax
	movq	8(%r8), %rcx
	movq	16(%rax), %rax
	movq	16(%rcx), %rcx
	xorl	%esi, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB7_14:                               # =>This Inner Loop Header: Depth=1
	movslq	%esi, %rsi
	movl	(%r10,%rsi,4), %edx
	movl	%edx, (%r11,%rdi,4)
	movl	(%rax,%rsi,4), %edx
	movl	%edx, (%rcx,%rdi,4)
	incq	%rdi
	movl	number_of_slices(%rip), %edx
	leal	1(%rdx), %ebx
	sarl	%ebx
	movslq	%ebx, %rbx
	addl	$2, %esi
	cmpq	%rbx, %rdi
	jl	.LBB7_14
.LBB7_15:                               # %._crit_edge.2
	incl	%edx
	sarl	%edx
	testl	%edx, %edx
	jle	.LBB7_5
# BB#16:                                # %.lr.ph.3
	movq	(%r9), %rax
	movq	8(%r9), %rcx
	movq	24(%rax), %r9
	movq	24(%rcx), %rdx
	movq	(%r8), %rax
	movq	8(%r8), %rcx
	movq	24(%rax), %rax
	movq	24(%rcx), %rsi
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_17:                               # =>This Inner Loop Header: Depth=1
	movslq	%edi, %rdi
	movl	(%r9,%rdi,4), %ebx
	movl	%ebx, (%rdx,%rcx,4)
	movl	(%rax,%rdi,4), %ebx
	movl	%ebx, (%rsi,%rcx,4)
	incq	%rcx
	movl	number_of_slices(%rip), %ebx
	incl	%ebx
	sarl	%ebx
	movslq	%ebx, %rbx
	addl	$2, %edi
	cmpq	%rbx, %rcx
	jl	.LBB7_17
.LBB7_5:                                # %.loopexit
	popq	%rbx
	retq
.Lfunc_end7:
	.size	update_field_frame_contexts, .Lfunc_end7-update_field_frame_contexts
	.cfi_endproc

	.type	probability,@object     # @probability
	.data
	.globl	probability
	.p2align	4
probability:
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	0                       # double 0
	.quad	4602678819172646912     # double 0.5
	.quad	4602221415580092655     # double 0.474609
	.quad	4601787232547217120     # double 0.45050699999999999
	.quad	4601375099138117191     # double 0.42762899999999998
	.quad	4600983880445686771     # double 0.405912
	.quad	4600612549649210819     # double 0.385299
	.quad	4600260061913575785     # double 0.365732
	.quad	4599925480490059176     # double 0.347159
	.quad	4599607904658735518     # double 0.32952999999999999
	.quad	4599306433699679337     # double 0.31279499999999999
	.quad	4599020292993754725     # double 0.29691099999999998
	.quad	4598748671893028756     # double 0.281833
	.quad	4598490831807162540     # double 0.26751999999999998
	.quad	4598246106203411228     # double 0.25393500000000002
	.quad	4597852365495189480     # double 0.241039
	.quad	4597411373019677361     # double 0.228799
	.quad	4596992754427114019     # double 0.21718000000000001
	.quad	4596595392824791865     # double 0.206151
	.quad	4596218207348800331     # double 0.19568199999999999
	.quad	4595860153164025867     # double 0.18574399999999999
	.quad	4595520329550542999     # double 0.176312
	.quad	4595197727702035196     # double 0.16735800000000001
	.quad	4594891518956171021     # double 0.158859
	.quad	4594600874650619039     # double 0.15079200000000001
	.quad	4594324966123047813     # double 0.14313400000000001
	.quad	4594063108826313982     # double 0.13586599999999999
	.quad	4593814510126883131     # double 0.128966
	.quad	4593485495152505952     # double 0.122417
	.quad	4593037513090372154     # double 0.1162
	.quad	4592612301227954341     # double 0.11029899999999999
	.quad	4592208706643747907     # double 0.104698
	.quad	4591825576416248244     # double 0.099380999999999997
	.quad	4591461901739138822     # double 0.094334000000000001
	.quad	4591116673806103109     # double 0.089542999999999998
	.quad	4590789027926012651     # double 0.084996000000000002
	.quad	4590478027350144954     # double 0.080680000000000001
	.quad	4590182807387371563     # double 0.076582999999999998
	.quad	4589902575404158061     # double 0.072693999999999995
	.quad	4589636538766970031     # double 0.069001999999999994
	.quad	4589384048957461132     # double 0.065498000000000001
	.quad	4589120750508846543     # double 0.062171999999999998
	.quad	4588665634744902990     # double 0.059013999999999997
	.quad	4588233865641427726     # double 0.056017999999999998
	.quad	4587823857931351916     # double 0.053172999999999998
	.quad	4587434746923547105     # double 0.050472999999999997
	.quad	4587065235581320611     # double 0.047909
	.quad	4586714603328732054     # double 0.045476000000000003
	.quad	4586381841359464902     # double 0.043166999999999997
	.quad	4586065940867202626     # double 0.040974999999999998
	.quad	4585766037160816770     # double 0.038893999999999998
	.quad	4585481409664366955     # double 0.036919
	.quad	4585211193686724725     # double 0.035043999999999999
	.quad	4584954668651949702     # double 0.033264000000000002
	.quad	4584711258099289581     # double 0.031574999999999999
	.quad	4584296062242443040     # double 0.029971999999999999
	.quad	4583857375609940135     # double 0.02845
	.quad	4583440882716400912     # double 0.027005000000000001
	.quad	4583045430640320763     # double 0.025633
	.quad	4582670442920947386     # double 0.024331999999999999
	.quad	4582314190176023870     # double 0.023095999999999998
	.quad	4581976095944797913     # double 0.021923000000000002
	.quad	4581655295536141057     # double 0.020809999999999999
	.quad	4581350636028548698     # double 0.019753
	.quad	4581061540961268531     # double 0.018749999999999999
	.size	probability, 1024

	.type	num_mb_per_slice,@object # @num_mb_per_slice
	.comm	num_mb_per_slice,4,4
	.type	number_of_slices,@object # @number_of_slices
	.comm	number_of_slices,4,4
	.type	initialized,@object     # @initialized
	.comm	initialized,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"create_context_memory: initialized"
	.size	.L.str, 35

	.type	model_number,@object    # @model_number
	.comm	model_number,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"create_context_memory: model_number"
	.size	.L.str.1, 36

	.type	entropy,@object         # @entropy
	.comm	entropy,1024,16
	.type	INIT_MB_TYPE_I,@object  # @INIT_MB_TYPE_I
	.section	.rodata,"a",@progbits
	.p2align	4
INIT_MB_TYPE_I:
	.long	20                      # 0x14
	.long	4294967281              # 0xfffffff1
	.long	2                       # 0x2
	.long	54                      # 0x36
	.long	3                       # 0x3
	.long	74                      # 0x4a
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967268              # 0xffffffe4
	.long	127                     # 0x7f
	.long	4294967273              # 0xffffffe9
	.long	104                     # 0x68
	.long	4294967290              # 0xfffffffa
	.long	53                      # 0x35
	.long	4294967295              # 0xffffffff
	.long	54                      # 0x36
	.long	7                       # 0x7
	.long	51                      # 0x33
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	20                      # 0x14
	.long	4294967281              # 0xfffffff1
	.long	2                       # 0x2
	.long	54                      # 0x36
	.long	3                       # 0x3
	.long	74                      # 0x4a
	.long	20                      # 0x14
	.long	4294967281              # 0xfffffff1
	.long	2                       # 0x2
	.long	54                      # 0x36
	.long	3                       # 0x3
	.long	74                      # 0x4a
	.long	4294967268              # 0xffffffe4
	.long	127                     # 0x7f
	.long	4294967273              # 0xffffffe9
	.long	104                     # 0x68
	.long	4294967290              # 0xfffffffa
	.long	53                      # 0x35
	.long	4294967295              # 0xffffffff
	.long	54                      # 0x36
	.long	7                       # 0x7
	.long	51                      # 0x33
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.size	INIT_MB_TYPE_I, 264

	.type	INIT_MB_TYPE_P,@object  # @INIT_MB_TYPE_P
	.p2align	4
INIT_MB_TYPE_P:
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	23                      # 0x17
	.long	33                      # 0x21
	.long	23                      # 0x17
	.long	2                       # 0x2
	.long	21                      # 0x15
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	1                       # 0x1
	.long	9                       # 0x9
	.long	0                       # 0x0
	.long	49                      # 0x31
	.long	4294967259              # 0xffffffdb
	.long	118                     # 0x76
	.long	5                       # 0x5
	.long	57                      # 0x39
	.long	4294967283              # 0xfffffff3
	.long	78                      # 0x4e
	.long	4294967285              # 0xfffffff5
	.long	65                      # 0x41
	.long	1                       # 0x1
	.long	62                      # 0x3e
	.long	26                      # 0x1a
	.long	67                      # 0x43
	.long	16                      # 0x10
	.long	90                      # 0x5a
	.long	9                       # 0x9
	.long	104                     # 0x68
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967250              # 0xffffffd2
	.long	127                     # 0x7f
	.long	4294967276              # 0xffffffec
	.long	104                     # 0x68
	.long	1                       # 0x1
	.long	67                      # 0x43
	.long	18                      # 0x12
	.long	64                      # 0x40
	.long	9                       # 0x9
	.long	43                      # 0x2b
	.long	29                      # 0x1d
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	22                      # 0x16
	.long	25                      # 0x19
	.long	34                      # 0x22
	.long	0                       # 0x0
	.long	16                      # 0x10
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967294              # 0xfffffffe
	.long	9                       # 0x9
	.long	4                       # 0x4
	.long	41                      # 0x29
	.long	4294967267              # 0xffffffe3
	.long	118                     # 0x76
	.long	2                       # 0x2
	.long	65                      # 0x41
	.long	4294967290              # 0xfffffffa
	.long	71                      # 0x47
	.long	4294967283              # 0xfffffff3
	.long	79                      # 0x4f
	.long	5                       # 0x5
	.long	52                      # 0x34
	.long	57                      # 0x39
	.long	2                       # 0x2
	.long	41                      # 0x29
	.long	36                      # 0x24
	.long	26                      # 0x1a
	.long	69                      # 0x45
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967251              # 0xffffffd3
	.long	127                     # 0x7f
	.long	4294967281              # 0xfffffff1
	.long	101                     # 0x65
	.long	4294967292              # 0xfffffffc
	.long	76                      # 0x4c
	.long	26                      # 0x1a
	.long	34                      # 0x22
	.long	19                      # 0x13
	.long	22                      # 0x16
	.long	40                      # 0x28
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	29                      # 0x1d
	.long	16                      # 0x10
	.long	25                      # 0x19
	.long	0                       # 0x0
	.long	14                      # 0xe
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967286              # 0xfffffff6
	.long	51                      # 0x33
	.long	4294967293              # 0xfffffffd
	.long	62                      # 0x3e
	.long	4294967269              # 0xffffffe5
	.long	99                      # 0x63
	.long	26                      # 0x1a
	.long	16                      # 0x10
	.long	4294967292              # 0xfffffffc
	.long	85                      # 0x55
	.long	4294967272              # 0xffffffe8
	.long	102                     # 0x66
	.long	5                       # 0x5
	.long	57                      # 0x39
	.long	54                      # 0x36
	.long	0                       # 0x0
	.long	37                      # 0x25
	.long	42                      # 0x2a
	.long	12                      # 0xc
	.long	97                      # 0x61
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967264              # 0xffffffe0
	.long	127                     # 0x7f
	.long	4294967274              # 0xffffffea
	.long	117                     # 0x75
	.long	4294967294              # 0xfffffffe
	.long	74                      # 0x4a
	.long	20                      # 0x14
	.long	40                      # 0x28
	.long	20                      # 0x14
	.long	10                      # 0xa
	.long	29                      # 0x1d
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	64                      # 0x40
	.size	INIT_MB_TYPE_P, 792

	.type	INIT_B8_TYPE_I,@object  # @INIT_B8_TYPE_I
	.p2align	4
INIT_B8_TYPE_I:
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.size	INIT_B8_TYPE_I, 144

	.type	INIT_B8_TYPE_P,@object  # @INIT_B8_TYPE_P
	.p2align	4
INIT_B8_TYPE_P:
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	12                      # 0xc
	.long	49                      # 0x31
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967292              # 0xfffffffc
	.long	73                      # 0x49
	.long	17                      # 0x11
	.long	50                      # 0x32
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967290              # 0xfffffffa
	.long	86                      # 0x56
	.long	4294967279              # 0xffffffef
	.long	95                      # 0x5f
	.long	4294967290              # 0xfffffffa
	.long	61                      # 0x3d
	.long	9                       # 0x9
	.long	45                      # 0x2d
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	9                       # 0x9
	.long	50                      # 0x32
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967293              # 0xfffffffd
	.long	70                      # 0x46
	.long	10                      # 0xa
	.long	54                      # 0x36
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	6                       # 0x6
	.long	69                      # 0x45
	.long	4294967283              # 0xfffffff3
	.long	90                      # 0x5a
	.long	0                       # 0x0
	.long	52                      # 0x34
	.long	8                       # 0x8
	.long	43                      # 0x2b
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	6                       # 0x6
	.long	57                      # 0x39
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967279              # 0xffffffef
	.long	73                      # 0x49
	.long	14                      # 0xe
	.long	57                      # 0x39
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967290              # 0xfffffffa
	.long	93                      # 0x5d
	.long	4294967282              # 0xfffffff2
	.long	88                      # 0x58
	.long	4294967290              # 0xfffffffa
	.long	44                      # 0x2c
	.long	4                       # 0x4
	.long	55                      # 0x37
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.size	INIT_B8_TYPE_P, 432

	.type	INIT_MV_RES_I,@object   # @INIT_MV_RES_I
	.p2align	4
INIT_MV_RES_I:
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.size	INIT_MV_RES_I, 160

	.type	INIT_MV_RES_P,@object   # @INIT_MV_RES_P
	.p2align	4
INIT_MV_RES_P:
	.long	4294967293              # 0xfffffffd
	.long	69                      # 0x45
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967290              # 0xfffffffa
	.long	81                      # 0x51
	.long	4294967285              # 0xfffffff5
	.long	96                      # 0x60
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	58                      # 0x3a
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967293              # 0xfffffffd
	.long	76                      # 0x4c
	.long	4294967286              # 0xfffffff6
	.long	94                      # 0x5e
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	6                       # 0x6
	.long	55                      # 0x37
	.long	7                       # 0x7
	.long	67                      # 0x43
	.long	4294967291              # 0xfffffffb
	.long	86                      # 0x56
	.long	2                       # 0x2
	.long	88                      # 0x58
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	5                       # 0x5
	.long	54                      # 0x36
	.long	4                       # 0x4
	.long	69                      # 0x45
	.long	4294967293              # 0xfffffffd
	.long	81                      # 0x51
	.long	0                       # 0x0
	.long	88                      # 0x58
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967294              # 0xfffffffe
	.long	69                      # 0x45
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967291              # 0xfffffffb
	.long	82                      # 0x52
	.long	4294967286              # 0xfffffff6
	.long	96                      # 0x60
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	1                       # 0x1
	.long	56                      # 0x38
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967293              # 0xfffffffd
	.long	74                      # 0x4a
	.long	4294967290              # 0xfffffffa
	.long	85                      # 0x55
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	2                       # 0x2
	.long	59                      # 0x3b
	.long	2                       # 0x2
	.long	75                      # 0x4b
	.long	4294967293              # 0xfffffffd
	.long	87                      # 0x57
	.long	4294967293              # 0xfffffffd
	.long	100                     # 0x64
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	59                      # 0x3b
	.long	4294967293              # 0xfffffffd
	.long	81                      # 0x51
	.long	4294967289              # 0xfffffff9
	.long	86                      # 0x56
	.long	4294967291              # 0xfffffffb
	.long	95                      # 0x5f
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967285              # 0xfffffff5
	.long	89                      # 0x59
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967281              # 0xfffffff1
	.long	103                     # 0x67
	.long	4294967275              # 0xffffffeb
	.long	116                     # 0x74
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	1                       # 0x1
	.long	63                      # 0x3f
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967291              # 0xfffffffb
	.long	85                      # 0x55
	.long	4294967283              # 0xfffffff3
	.long	106                     # 0x6a
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	19                      # 0x13
	.long	57                      # 0x39
	.long	20                      # 0x14
	.long	58                      # 0x3a
	.long	4                       # 0x4
	.long	84                      # 0x54
	.long	6                       # 0x6
	.long	96                      # 0x60
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	5                       # 0x5
	.long	63                      # 0x3f
	.long	6                       # 0x6
	.long	75                      # 0x4b
	.long	4294967293              # 0xfffffffd
	.long	90                      # 0x5a
	.long	4294967295              # 0xffffffff
	.long	101                     # 0x65
	.long	0                       # 0x0
	.long	64                      # 0x40
	.size	INIT_MV_RES_P, 480

	.type	INIT_REF_NO_I,@object   # @INIT_REF_NO_I
	.p2align	4
INIT_REF_NO_I:
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.size	INIT_REF_NO_I, 96

	.type	INIT_REF_NO_P,@object   # @INIT_REF_NO_P
	.p2align	4
INIT_REF_NO_P:
	.long	4294967289              # 0xfffffff9
	.long	67                      # 0x43
	.long	4294967291              # 0xfffffffb
	.long	74                      # 0x4a
	.long	4294967292              # 0xfffffffc
	.long	74                      # 0x4a
	.long	4294967291              # 0xfffffffb
	.long	80                      # 0x50
	.long	4294967289              # 0xfffffff9
	.long	72                      # 0x48
	.long	1                       # 0x1
	.long	58                      # 0x3a
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967295              # 0xffffffff
	.long	66                      # 0x42
	.long	4294967295              # 0xffffffff
	.long	77                      # 0x4d
	.long	1                       # 0x1
	.long	70                      # 0x46
	.long	4294967294              # 0xfffffffe
	.long	86                      # 0x56
	.long	4294967291              # 0xfffffffb
	.long	72                      # 0x48
	.long	0                       # 0x0
	.long	61                      # 0x3d
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	3                       # 0x3
	.long	55                      # 0x37
	.long	4294967292              # 0xfffffffc
	.long	79                      # 0x4f
	.long	4294967294              # 0xfffffffe
	.long	75                      # 0x4b
	.long	4294967284              # 0xfffffff4
	.long	97                      # 0x61
	.long	4294967289              # 0xfffffff9
	.long	50                      # 0x32
	.long	1                       # 0x1
	.long	60                      # 0x3c
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.size	INIT_REF_NO_P, 288

	.type	INIT_DELTA_QP_I,@object # @INIT_DELTA_QP_I
	.p2align	4
INIT_DELTA_QP_I:
	.long	0                       # 0x0
	.long	41                      # 0x29
	.long	0                       # 0x0
	.long	63                      # 0x3f
	.long	0                       # 0x0
	.long	63                      # 0x3f
	.long	0                       # 0x0
	.long	63                      # 0x3f
	.size	INIT_DELTA_QP_I, 32

	.type	INIT_DELTA_QP_P,@object # @INIT_DELTA_QP_P
	.p2align	4
INIT_DELTA_QP_P:
	.long	0                       # 0x0
	.long	41                      # 0x29
	.long	0                       # 0x0
	.long	63                      # 0x3f
	.long	0                       # 0x0
	.long	63                      # 0x3f
	.long	0                       # 0x0
	.long	63                      # 0x3f
	.long	0                       # 0x0
	.long	41                      # 0x29
	.long	0                       # 0x0
	.long	63                      # 0x3f
	.long	0                       # 0x0
	.long	63                      # 0x3f
	.long	0                       # 0x0
	.long	63                      # 0x3f
	.long	0                       # 0x0
	.long	41                      # 0x29
	.long	0                       # 0x0
	.long	63                      # 0x3f
	.long	0                       # 0x0
	.long	63                      # 0x3f
	.long	0                       # 0x0
	.long	63                      # 0x3f
	.size	INIT_DELTA_QP_P, 96

	.type	INIT_MB_AFF_I,@object   # @INIT_MB_AFF_I
	.p2align	4
INIT_MB_AFF_I:
	.long	0                       # 0x0
	.long	11                      # 0xb
	.long	1                       # 0x1
	.long	55                      # 0x37
	.long	0                       # 0x0
	.long	69                      # 0x45
	.long	0                       # 0x0
	.long	64                      # 0x40
	.size	INIT_MB_AFF_I, 32

	.type	INIT_MB_AFF_P,@object   # @INIT_MB_AFF_P
	.p2align	4
INIT_MB_AFF_P:
	.long	0                       # 0x0
	.long	45                      # 0x2d
	.long	4294967292              # 0xfffffffc
	.long	78                      # 0x4e
	.long	4294967293              # 0xfffffffd
	.long	96                      # 0x60
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	13                      # 0xd
	.long	15                      # 0xf
	.long	7                       # 0x7
	.long	51                      # 0x33
	.long	2                       # 0x2
	.long	80                      # 0x50
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	7                       # 0x7
	.long	34                      # 0x22
	.long	4294967287              # 0xfffffff7
	.long	88                      # 0x58
	.long	4294967276              # 0xffffffec
	.long	127                     # 0x7f
	.long	0                       # 0x0
	.long	64                      # 0x40
	.size	INIT_MB_AFF_P, 96

	.type	INIT_TRANSFORM_SIZE_I,@object # @INIT_TRANSFORM_SIZE_I
	.p2align	4
INIT_TRANSFORM_SIZE_I:
	.long	31                      # 0x1f
	.long	21                      # 0x15
	.long	31                      # 0x1f
	.long	31                      # 0x1f
	.long	25                      # 0x19
	.long	50                      # 0x32
	.size	INIT_TRANSFORM_SIZE_I, 24

	.type	INIT_TRANSFORM_SIZE_P,@object # @INIT_TRANSFORM_SIZE_P
	.p2align	4
INIT_TRANSFORM_SIZE_P:
	.long	12                      # 0xc
	.long	40                      # 0x28
	.long	11                      # 0xb
	.long	51                      # 0x33
	.long	14                      # 0xe
	.long	59                      # 0x3b
	.long	25                      # 0x19
	.long	32                      # 0x20
	.long	21                      # 0x15
	.long	49                      # 0x31
	.long	21                      # 0x15
	.long	54                      # 0x36
	.long	21                      # 0x15
	.long	33                      # 0x21
	.long	19                      # 0x13
	.long	50                      # 0x32
	.long	17                      # 0x11
	.long	61                      # 0x3d
	.size	INIT_TRANSFORM_SIZE_P, 72

	.type	INIT_IPR_I,@object      # @INIT_IPR_I
	.p2align	4
INIT_IPR_I:
	.long	13                      # 0xd
	.long	41                      # 0x29
	.long	3                       # 0x3
	.long	62                      # 0x3e
	.size	INIT_IPR_I, 16

	.type	INIT_IPR_P,@object      # @INIT_IPR_P
	.p2align	4
INIT_IPR_P:
	.long	13                      # 0xd
	.long	41                      # 0x29
	.long	3                       # 0x3
	.long	62                      # 0x3e
	.long	13                      # 0xd
	.long	41                      # 0x29
	.long	3                       # 0x3
	.long	62                      # 0x3e
	.long	13                      # 0xd
	.long	41                      # 0x29
	.long	3                       # 0x3
	.long	62                      # 0x3e
	.size	INIT_IPR_P, 48

	.type	INIT_CIPR_I,@object     # @INIT_CIPR_I
	.p2align	4
INIT_CIPR_I:
	.long	4294967287              # 0xfffffff7
	.long	83                      # 0x53
	.long	4                       # 0x4
	.long	86                      # 0x56
	.long	0                       # 0x0
	.long	97                      # 0x61
	.long	4294967289              # 0xfffffff9
	.long	72                      # 0x48
	.size	INIT_CIPR_I, 32

	.type	INIT_CIPR_P,@object     # @INIT_CIPR_P
	.p2align	4
INIT_CIPR_P:
	.long	4294967287              # 0xfffffff7
	.long	83                      # 0x53
	.long	4                       # 0x4
	.long	86                      # 0x56
	.long	0                       # 0x0
	.long	97                      # 0x61
	.long	4294967289              # 0xfffffff9
	.long	72                      # 0x48
	.long	4294967287              # 0xfffffff7
	.long	83                      # 0x53
	.long	4                       # 0x4
	.long	86                      # 0x56
	.long	0                       # 0x0
	.long	97                      # 0x61
	.long	4294967289              # 0xfffffff9
	.long	72                      # 0x48
	.long	4294967287              # 0xfffffff7
	.long	83                      # 0x53
	.long	4                       # 0x4
	.long	86                      # 0x56
	.long	0                       # 0x0
	.long	97                      # 0x61
	.long	4294967289              # 0xfffffff9
	.long	72                      # 0x48
	.size	INIT_CIPR_P, 96

	.type	INIT_CBP_I,@object      # @INIT_CBP_I
	.p2align	4
INIT_CBP_I:
	.long	4294967279              # 0xffffffef
	.long	127                     # 0x7f
	.long	4294967283              # 0xfffffff3
	.long	102                     # 0x66
	.long	0                       # 0x0
	.long	82                      # 0x52
	.long	4294967289              # 0xfffffff9
	.long	74                      # 0x4a
	.long	4294967275              # 0xffffffeb
	.long	107                     # 0x6b
	.long	4294967269              # 0xffffffe5
	.long	127                     # 0x7f
	.long	4294967265              # 0xffffffe1
	.long	127                     # 0x7f
	.long	4294967272              # 0xffffffe8
	.long	127                     # 0x7f
	.long	4294967278              # 0xffffffee
	.long	95                      # 0x5f
	.long	4294967269              # 0xffffffe5
	.long	127                     # 0x7f
	.long	4294967275              # 0xffffffeb
	.long	114                     # 0x72
	.long	4294967266              # 0xffffffe2
	.long	127                     # 0x7f
	.size	INIT_CBP_I, 96

	.type	INIT_CBP_P,@object      # @INIT_CBP_P
	.p2align	4
INIT_CBP_P:
	.long	4294967269              # 0xffffffe5
	.long	126                     # 0x7e
	.long	4294967268              # 0xffffffe4
	.long	98                      # 0x62
	.long	4294967271              # 0xffffffe7
	.long	101                     # 0x65
	.long	4294967273              # 0xffffffe9
	.long	67                      # 0x43
	.long	4294967268              # 0xffffffe4
	.long	82                      # 0x52
	.long	4294967276              # 0xffffffec
	.long	94                      # 0x5e
	.long	4294967280              # 0xfffffff0
	.long	83                      # 0x53
	.long	4294967274              # 0xffffffea
	.long	110                     # 0x6e
	.long	4294967275              # 0xffffffeb
	.long	91                      # 0x5b
	.long	4294967278              # 0xffffffee
	.long	102                     # 0x66
	.long	4294967283              # 0xfffffff3
	.long	93                      # 0x5d
	.long	4294967267              # 0xffffffe3
	.long	127                     # 0x7f
	.long	4294967257              # 0xffffffd9
	.long	127                     # 0x7f
	.long	4294967278              # 0xffffffee
	.long	91                      # 0x5b
	.long	4294967279              # 0xffffffef
	.long	96                      # 0x60
	.long	4294967270              # 0xffffffe6
	.long	81                      # 0x51
	.long	4294967261              # 0xffffffdd
	.long	98                      # 0x62
	.long	4294967272              # 0xffffffe8
	.long	102                     # 0x66
	.long	4294967273              # 0xffffffe9
	.long	97                      # 0x61
	.long	4294967269              # 0xffffffe5
	.long	119                     # 0x77
	.long	4294967272              # 0xffffffe8
	.long	99                      # 0x63
	.long	4294967275              # 0xffffffeb
	.long	110                     # 0x6e
	.long	4294967278              # 0xffffffee
	.long	102                     # 0x66
	.long	4294967260              # 0xffffffdc
	.long	127                     # 0x7f
	.long	4294967260              # 0xffffffdc
	.long	127                     # 0x7f
	.long	4294967279              # 0xffffffef
	.long	91                      # 0x5b
	.long	4294967282              # 0xfffffff2
	.long	95                      # 0x5f
	.long	4294967271              # 0xffffffe7
	.long	84                      # 0x54
	.long	4294967271              # 0xffffffe7
	.long	86                      # 0x56
	.long	4294967284              # 0xfffffff4
	.long	89                      # 0x59
	.long	4294967279              # 0xffffffef
	.long	91                      # 0x5b
	.long	4294967265              # 0xffffffe1
	.long	127                     # 0x7f
	.long	4294967282              # 0xfffffff2
	.long	76                      # 0x4c
	.long	4294967278              # 0xffffffee
	.long	103                     # 0x67
	.long	4294967283              # 0xfffffff3
	.long	90                      # 0x5a
	.long	4294967259              # 0xffffffdb
	.long	127                     # 0x7f
	.size	INIT_CBP_P, 288

	.type	INIT_BCBP_I,@object     # @INIT_BCBP_I
	.p2align	4
INIT_BCBP_I:
	.long	4294967279              # 0xffffffef
	.long	123                     # 0x7b
	.long	4294967284              # 0xfffffff4
	.long	115                     # 0x73
	.long	4294967280              # 0xfffffff0
	.long	122                     # 0x7a
	.long	4294967285              # 0xfffffff5
	.long	115                     # 0x73
	.long	4294967284              # 0xfffffff4
	.long	63                      # 0x3f
	.long	4294967294              # 0xfffffffe
	.long	68                      # 0x44
	.long	4294967281              # 0xfffffff1
	.long	84                      # 0x54
	.long	4294967283              # 0xfffffff3
	.long	104                     # 0x68
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967293              # 0xfffffffd
	.long	70                      # 0x46
	.long	4294967288              # 0xfffffff8
	.long	93                      # 0x5d
	.long	4294967286              # 0xfffffff6
	.long	90                      # 0x5a
	.long	4294967266              # 0xffffffe2
	.long	127                     # 0x7f
	.long	4294967295              # 0xffffffff
	.long	74                      # 0x4a
	.long	4294967290              # 0xfffffffa
	.long	97                      # 0x61
	.long	4294967289              # 0xfffffff9
	.long	91                      # 0x5b
	.long	4294967276              # 0xffffffec
	.long	127                     # 0x7f
	.long	4294967292              # 0xfffffffc
	.long	56                      # 0x38
	.long	4294967291              # 0xfffffffb
	.long	82                      # 0x52
	.long	4294967289              # 0xfffffff9
	.long	76                      # 0x4c
	.long	4294967274              # 0xffffffea
	.long	125                     # 0x7d
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.size	INIT_BCBP_I, 256

	.type	INIT_BCBP_P,@object     # @INIT_BCBP_P
	.p2align	4
INIT_BCBP_P:
	.long	4294967289              # 0xfffffff9
	.long	92                      # 0x5c
	.long	4294967291              # 0xfffffffb
	.long	89                      # 0x59
	.long	4294967289              # 0xfffffff9
	.long	96                      # 0x60
	.long	4294967283              # 0xfffffff3
	.long	108                     # 0x6c
	.long	4294967293              # 0xfffffffd
	.long	46                      # 0x2e
	.long	4294967295              # 0xffffffff
	.long	65                      # 0x41
	.long	4294967295              # 0xffffffff
	.long	57                      # 0x39
	.long	4294967287              # 0xfffffff7
	.long	93                      # 0x5d
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967293              # 0xfffffffd
	.long	74                      # 0x4a
	.long	4294967287              # 0xfffffff7
	.long	92                      # 0x5c
	.long	4294967288              # 0xfffffff8
	.long	87                      # 0x57
	.long	4294967273              # 0xffffffe9
	.long	126                     # 0x7e
	.long	5                       # 0x5
	.long	54                      # 0x36
	.long	6                       # 0x6
	.long	60                      # 0x3c
	.long	6                       # 0x6
	.long	59                      # 0x3b
	.long	6                       # 0x6
	.long	69                      # 0x45
	.long	4294967295              # 0xffffffff
	.long	48                      # 0x30
	.long	0                       # 0x0
	.long	68                      # 0x44
	.long	4294967292              # 0xfffffffc
	.long	69                      # 0x45
	.long	4294967288              # 0xfffffff8
	.long	88                      # 0x58
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	80                      # 0x50
	.long	4294967291              # 0xfffffffb
	.long	89                      # 0x59
	.long	4294967289              # 0xfffffff9
	.long	94                      # 0x5e
	.long	4294967292              # 0xfffffffc
	.long	92                      # 0x5c
	.long	0                       # 0x0
	.long	39                      # 0x27
	.long	0                       # 0x0
	.long	65                      # 0x41
	.long	4294967281              # 0xfffffff1
	.long	84                      # 0x54
	.long	4294967261              # 0xffffffdd
	.long	127                     # 0x7f
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967294              # 0xfffffffe
	.long	73                      # 0x49
	.long	4294967284              # 0xfffffff4
	.long	104                     # 0x68
	.long	4294967287              # 0xfffffff7
	.long	91                      # 0x5b
	.long	4294967265              # 0xffffffe1
	.long	127                     # 0x7f
	.long	3                       # 0x3
	.long	55                      # 0x37
	.long	7                       # 0x7
	.long	56                      # 0x38
	.long	7                       # 0x7
	.long	55                      # 0x37
	.long	8                       # 0x8
	.long	61                      # 0x3d
	.long	4294967293              # 0xfffffffd
	.long	53                      # 0x35
	.long	0                       # 0x0
	.long	68                      # 0x44
	.long	4294967289              # 0xfffffff9
	.long	74                      # 0x4a
	.long	4294967287              # 0xfffffff7
	.long	88                      # 0x58
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	11                      # 0xb
	.long	80                      # 0x50
	.long	5                       # 0x5
	.long	76                      # 0x4c
	.long	2                       # 0x2
	.long	84                      # 0x54
	.long	5                       # 0x5
	.long	78                      # 0x4e
	.long	4294967290              # 0xfffffffa
	.long	55                      # 0x37
	.long	4                       # 0x4
	.long	61                      # 0x3d
	.long	4294967282              # 0xfffffff2
	.long	83                      # 0x53
	.long	4294967259              # 0xffffffdb
	.long	127                     # 0x7f
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967291              # 0xfffffffb
	.long	79                      # 0x4f
	.long	4294967285              # 0xfffffff5
	.long	104                     # 0x68
	.long	4294967285              # 0xfffffff5
	.long	91                      # 0x5b
	.long	4294967266              # 0xffffffe2
	.long	127                     # 0x7f
	.long	0                       # 0x0
	.long	65                      # 0x41
	.long	4294967294              # 0xfffffffe
	.long	79                      # 0x4f
	.long	0                       # 0x0
	.long	72                      # 0x48
	.long	4294967292              # 0xfffffffc
	.long	92                      # 0x5c
	.long	4294967290              # 0xfffffffa
	.long	56                      # 0x38
	.long	3                       # 0x3
	.long	68                      # 0x44
	.long	4294967288              # 0xfffffff8
	.long	71                      # 0x47
	.long	4294967283              # 0xfffffff3
	.long	98                      # 0x62
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.size	INIT_BCBP_P, 768

	.type	INIT_MAP_I,@object      # @INIT_MAP_I
	.p2align	4
INIT_MAP_I:
	.long	4294967289              # 0xfffffff9
	.long	93                      # 0x5d
	.long	4294967285              # 0xfffffff5
	.long	87                      # 0x57
	.long	4294967293              # 0xfffffffd
	.long	77                      # 0x4d
	.long	4294967291              # 0xfffffffb
	.long	71                      # 0x47
	.long	4294967292              # 0xfffffffc
	.long	63                      # 0x3f
	.long	4294967292              # 0xfffffffc
	.long	68                      # 0x44
	.long	4294967284              # 0xfffffff4
	.long	84                      # 0x54
	.long	4294967289              # 0xfffffff9
	.long	62                      # 0x3e
	.long	4294967289              # 0xfffffff9
	.long	65                      # 0x41
	.long	8                       # 0x8
	.long	61                      # 0x3d
	.long	5                       # 0x5
	.long	56                      # 0x38
	.long	4294967294              # 0xfffffffe
	.long	66                      # 0x42
	.long	1                       # 0x1
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	61                      # 0x3d
	.long	4294967294              # 0xfffffffe
	.long	78                      # 0x4e
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	1                       # 0x1
	.long	50                      # 0x32
	.long	7                       # 0x7
	.long	52                      # 0x34
	.long	10                      # 0xa
	.long	35                      # 0x23
	.long	0                       # 0x0
	.long	44                      # 0x2c
	.long	11                      # 0xb
	.long	38                      # 0x26
	.long	1                       # 0x1
	.long	45                      # 0x2d
	.long	0                       # 0x0
	.long	46                      # 0x2e
	.long	5                       # 0x5
	.long	44                      # 0x2c
	.long	31                      # 0x1f
	.long	17                      # 0x11
	.long	1                       # 0x1
	.long	51                      # 0x33
	.long	7                       # 0x7
	.long	50                      # 0x32
	.long	28                      # 0x1c
	.long	19                      # 0x13
	.long	16                      # 0x10
	.long	33                      # 0x21
	.long	14                      # 0xe
	.long	62                      # 0x3e
	.long	4294967279              # 0xffffffef
	.long	120                     # 0x78
	.long	4294967276              # 0xffffffec
	.long	112                     # 0x70
	.long	4294967278              # 0xffffffee
	.long	114                     # 0x72
	.long	4294967285              # 0xfffffff5
	.long	85                      # 0x55
	.long	4294967281              # 0xfffffff1
	.long	92                      # 0x5c
	.long	4294967282              # 0xfffffff2
	.long	89                      # 0x59
	.long	4294967270              # 0xffffffe6
	.long	71                      # 0x47
	.long	4294967281              # 0xfffffff1
	.long	81                      # 0x51
	.long	4294967282              # 0xfffffff2
	.long	80                      # 0x50
	.long	0                       # 0x0
	.long	68                      # 0x44
	.long	4294967282              # 0xfffffff2
	.long	70                      # 0x46
	.long	4294967272              # 0xffffffe8
	.long	56                      # 0x38
	.long	4294967273              # 0xffffffe9
	.long	68                      # 0x44
	.long	4294967272              # 0xffffffe8
	.long	50                      # 0x32
	.long	4294967285              # 0xfffffff5
	.long	74                      # 0x4a
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967283              # 0xfffffff3
	.long	108                     # 0x6c
	.long	4294967281              # 0xfffffff1
	.long	100                     # 0x64
	.long	4294967283              # 0xfffffff3
	.long	101                     # 0x65
	.long	4294967283              # 0xfffffff3
	.long	91                      # 0x5b
	.long	4294967284              # 0xfffffff4
	.long	94                      # 0x5e
	.long	4294967286              # 0xfffffff6
	.long	88                      # 0x58
	.long	4294967280              # 0xfffffff0
	.long	84                      # 0x54
	.long	4294967286              # 0xfffffff6
	.long	86                      # 0x56
	.long	4294967289              # 0xfffffff9
	.long	83                      # 0x53
	.long	4294967283              # 0xfffffff3
	.long	87                      # 0x57
	.long	4294967277              # 0xffffffed
	.long	94                      # 0x5e
	.long	1                       # 0x1
	.long	70                      # 0x46
	.long	0                       # 0x0
	.long	72                      # 0x48
	.long	4294967291              # 0xfffffffb
	.long	74                      # 0x4a
	.long	18                      # 0x12
	.long	59                      # 0x3b
	.long	4294967288              # 0xfffffff8
	.long	102                     # 0x66
	.long	4294967281              # 0xfffffff1
	.long	100                     # 0x64
	.long	0                       # 0x0
	.long	95                      # 0x5f
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967292              # 0xfffffffc
	.long	75                      # 0x4b
	.long	2                       # 0x2
	.long	72                      # 0x48
	.long	4294967285              # 0xfffffff5
	.long	75                      # 0x4b
	.long	4294967293              # 0xfffffffd
	.long	71                      # 0x47
	.long	15                      # 0xf
	.long	46                      # 0x2e
	.long	4294967283              # 0xfffffff3
	.long	69                      # 0x45
	.long	0                       # 0x0
	.long	62                      # 0x3e
	.long	0                       # 0x0
	.long	65                      # 0x41
	.long	21                      # 0x15
	.long	37                      # 0x25
	.long	4294967281              # 0xfffffff1
	.long	72                      # 0x48
	.long	9                       # 0x9
	.long	57                      # 0x39
	.long	16                      # 0x10
	.long	54                      # 0x36
	.long	0                       # 0x0
	.long	62                      # 0x3e
	.long	12                      # 0xc
	.long	72                      # 0x48
	.size	INIT_MAP_I, 960

	.type	INIT_MAP_P,@object      # @INIT_MAP_P
	.p2align	4
INIT_MAP_P:
	.long	4294967294              # 0xfffffffe
	.long	85                      # 0x55
	.long	4294967290              # 0xfffffffa
	.long	78                      # 0x4e
	.long	4294967295              # 0xffffffff
	.long	75                      # 0x4b
	.long	4294967289              # 0xfffffff9
	.long	77                      # 0x4d
	.long	2                       # 0x2
	.long	54                      # 0x36
	.long	5                       # 0x5
	.long	50                      # 0x32
	.long	4294967293              # 0xfffffffd
	.long	68                      # 0x44
	.long	1                       # 0x1
	.long	50                      # 0x32
	.long	6                       # 0x6
	.long	42                      # 0x2a
	.long	4294967292              # 0xfffffffc
	.long	81                      # 0x51
	.long	1                       # 0x1
	.long	63                      # 0x3f
	.long	4294967292              # 0xfffffffc
	.long	70                      # 0x46
	.long	0                       # 0x0
	.long	67                      # 0x43
	.long	2                       # 0x2
	.long	57                      # 0x39
	.long	4294967294              # 0xfffffffe
	.long	76                      # 0x4c
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	11                      # 0xb
	.long	35                      # 0x23
	.long	4                       # 0x4
	.long	64                      # 0x40
	.long	1                       # 0x1
	.long	61                      # 0x3d
	.long	11                      # 0xb
	.long	35                      # 0x23
	.long	18                      # 0x12
	.long	25                      # 0x19
	.long	12                      # 0xc
	.long	24                      # 0x18
	.long	13                      # 0xd
	.long	29                      # 0x1d
	.long	13                      # 0xd
	.long	36                      # 0x24
	.long	4294967286              # 0xfffffff6
	.long	93                      # 0x5d
	.long	4294967289              # 0xfffffff9
	.long	73                      # 0x49
	.long	4294967294              # 0xfffffffe
	.long	73                      # 0x49
	.long	13                      # 0xd
	.long	46                      # 0x2e
	.long	9                       # 0x9
	.long	49                      # 0x31
	.long	4294967289              # 0xfffffff9
	.long	100                     # 0x64
	.long	4294967292              # 0xfffffffc
	.long	79                      # 0x4f
	.long	4294967289              # 0xfffffff9
	.long	71                      # 0x47
	.long	4294967291              # 0xfffffffb
	.long	69                      # 0x45
	.long	4294967287              # 0xfffffff7
	.long	70                      # 0x46
	.long	4294967288              # 0xfffffff8
	.long	66                      # 0x42
	.long	4294967286              # 0xfffffff6
	.long	68                      # 0x44
	.long	4294967277              # 0xffffffed
	.long	73                      # 0x49
	.long	4294967284              # 0xfffffff4
	.long	69                      # 0x45
	.long	4294967280              # 0xfffffff0
	.long	70                      # 0x46
	.long	4294967281              # 0xfffffff1
	.long	67                      # 0x43
	.long	4294967276              # 0xffffffec
	.long	62                      # 0x3e
	.long	4294967277              # 0xffffffed
	.long	70                      # 0x46
	.long	4294967280              # 0xfffffff0
	.long	66                      # 0x42
	.long	4294967274              # 0xffffffea
	.long	65                      # 0x41
	.long	4294967276              # 0xffffffec
	.long	63                      # 0x3f
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	9                       # 0x9
	.long	53                      # 0x35
	.long	2                       # 0x2
	.long	53                      # 0x35
	.long	5                       # 0x5
	.long	53                      # 0x35
	.long	4294967294              # 0xfffffffe
	.long	61                      # 0x3d
	.long	0                       # 0x0
	.long	56                      # 0x38
	.long	0                       # 0x0
	.long	56                      # 0x38
	.long	4294967283              # 0xfffffff3
	.long	63                      # 0x3f
	.long	4294967291              # 0xfffffffb
	.long	60                      # 0x3c
	.long	4294967295              # 0xffffffff
	.long	62                      # 0x3e
	.long	4                       # 0x4
	.long	57                      # 0x39
	.long	4294967290              # 0xfffffffa
	.long	69                      # 0x45
	.long	4                       # 0x4
	.long	57                      # 0x39
	.long	14                      # 0xe
	.long	39                      # 0x27
	.long	4                       # 0x4
	.long	51                      # 0x33
	.long	13                      # 0xd
	.long	68                      # 0x44
	.long	3                       # 0x3
	.long	64                      # 0x40
	.long	1                       # 0x1
	.long	61                      # 0x3d
	.long	9                       # 0x9
	.long	63                      # 0x3f
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	7                       # 0x7
	.long	50                      # 0x32
	.long	16                      # 0x10
	.long	39                      # 0x27
	.long	5                       # 0x5
	.long	44                      # 0x2c
	.long	4                       # 0x4
	.long	52                      # 0x34
	.long	11                      # 0xb
	.long	48                      # 0x30
	.long	4294967291              # 0xfffffffb
	.long	60                      # 0x3c
	.long	4294967295              # 0xffffffff
	.long	59                      # 0x3b
	.long	0                       # 0x0
	.long	59                      # 0x3b
	.long	22                      # 0x16
	.long	33                      # 0x21
	.long	5                       # 0x5
	.long	44                      # 0x2c
	.long	14                      # 0xe
	.long	43                      # 0x2b
	.long	4294967295              # 0xffffffff
	.long	78                      # 0x4e
	.long	0                       # 0x0
	.long	60                      # 0x3c
	.long	9                       # 0x9
	.long	69                      # 0x45
	.long	4294967283              # 0xfffffff3
	.long	103                     # 0x67
	.long	4294967283              # 0xfffffff3
	.long	91                      # 0x5b
	.long	4294967287              # 0xfffffff7
	.long	89                      # 0x59
	.long	4294967282              # 0xfffffff2
	.long	92                      # 0x5c
	.long	4294967288              # 0xfffffff8
	.long	76                      # 0x4c
	.long	4294967284              # 0xfffffff4
	.long	87                      # 0x57
	.long	4294967273              # 0xffffffe9
	.long	110                     # 0x6e
	.long	4294967272              # 0xffffffe8
	.long	105                     # 0x69
	.long	4294967286              # 0xfffffff6
	.long	78                      # 0x4e
	.long	4294967276              # 0xffffffec
	.long	112                     # 0x70
	.long	4294967279              # 0xffffffef
	.long	99                      # 0x63
	.long	4294967218              # 0xffffffb2
	.long	127                     # 0x7f
	.long	4294967226              # 0xffffffba
	.long	127                     # 0x7f
	.long	4294967246              # 0xffffffce
	.long	127                     # 0x7f
	.long	4294967250              # 0xffffffd2
	.long	127                     # 0x7f
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967292              # 0xfffffffc
	.long	66                      # 0x42
	.long	4294967291              # 0xfffffffb
	.long	78                      # 0x4e
	.long	4294967292              # 0xfffffffc
	.long	71                      # 0x47
	.long	4294967288              # 0xfffffff8
	.long	72                      # 0x48
	.long	2                       # 0x2
	.long	59                      # 0x3b
	.long	4294967295              # 0xffffffff
	.long	55                      # 0x37
	.long	4294967289              # 0xfffffff9
	.long	70                      # 0x46
	.long	4294967290              # 0xfffffffa
	.long	75                      # 0x4b
	.long	4294967288              # 0xfffffff8
	.long	89                      # 0x59
	.long	4294967262              # 0xffffffde
	.long	119                     # 0x77
	.long	4294967293              # 0xfffffffd
	.long	75                      # 0x4b
	.long	32                      # 0x20
	.long	20                      # 0x14
	.long	30                      # 0x1e
	.long	22                      # 0x16
	.long	4294967252              # 0xffffffd4
	.long	127                     # 0x7f
	.long	4294967291              # 0xfffffffb
	.long	85                      # 0x55
	.long	4294967290              # 0xfffffffa
	.long	81                      # 0x51
	.long	4294967286              # 0xfffffff6
	.long	77                      # 0x4d
	.long	4294967289              # 0xfffffff9
	.long	81                      # 0x51
	.long	4294967279              # 0xffffffef
	.long	80                      # 0x50
	.long	4294967278              # 0xffffffee
	.long	73                      # 0x49
	.long	4294967292              # 0xfffffffc
	.long	74                      # 0x4a
	.long	4294967286              # 0xfffffff6
	.long	83                      # 0x53
	.long	4294967287              # 0xfffffff7
	.long	71                      # 0x47
	.long	4294967287              # 0xfffffff7
	.long	67                      # 0x43
	.long	4294967295              # 0xffffffff
	.long	61                      # 0x3d
	.long	4294967288              # 0xfffffff8
	.long	66                      # 0x42
	.long	4294967282              # 0xfffffff2
	.long	66                      # 0x42
	.long	0                       # 0x0
	.long	59                      # 0x3b
	.long	2                       # 0x2
	.long	59                      # 0x3b
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	54                      # 0x36
	.long	4294967291              # 0xfffffffb
	.long	61                      # 0x3d
	.long	0                       # 0x0
	.long	58                      # 0x3a
	.long	4294967295              # 0xffffffff
	.long	60                      # 0x3c
	.long	4294967293              # 0xfffffffd
	.long	61                      # 0x3d
	.long	4294967288              # 0xfffffff8
	.long	67                      # 0x43
	.long	4294967271              # 0xffffffe7
	.long	84                      # 0x54
	.long	4294967282              # 0xfffffff2
	.long	74                      # 0x4a
	.long	4294967291              # 0xfffffffb
	.long	65                      # 0x41
	.long	5                       # 0x5
	.long	52                      # 0x34
	.long	2                       # 0x2
	.long	57                      # 0x39
	.long	0                       # 0x0
	.long	61                      # 0x3d
	.long	4294967287              # 0xfffffff7
	.long	69                      # 0x45
	.long	4294967285              # 0xfffffff5
	.long	70                      # 0x46
	.long	18                      # 0x12
	.long	55                      # 0x37
	.long	4294967292              # 0xfffffffc
	.long	71                      # 0x47
	.long	0                       # 0x0
	.long	58                      # 0x3a
	.long	7                       # 0x7
	.long	61                      # 0x3d
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	9                       # 0x9
	.long	41                      # 0x29
	.long	18                      # 0x12
	.long	25                      # 0x19
	.long	9                       # 0x9
	.long	32                      # 0x20
	.long	5                       # 0x5
	.long	43                      # 0x2b
	.long	9                       # 0x9
	.long	47                      # 0x2f
	.long	0                       # 0x0
	.long	44                      # 0x2c
	.long	0                       # 0x0
	.long	51                      # 0x33
	.long	2                       # 0x2
	.long	46                      # 0x2e
	.long	19                      # 0x13
	.long	38                      # 0x26
	.long	4294967292              # 0xfffffffc
	.long	66                      # 0x42
	.long	15                      # 0xf
	.long	38                      # 0x26
	.long	12                      # 0xc
	.long	42                      # 0x2a
	.long	9                       # 0x9
	.long	34                      # 0x22
	.long	0                       # 0x0
	.long	89                      # 0x59
	.long	4294967292              # 0xfffffffc
	.long	86                      # 0x56
	.long	4294967284              # 0xfffffff4
	.long	88                      # 0x58
	.long	4294967291              # 0xfffffffb
	.long	82                      # 0x52
	.long	4294967293              # 0xfffffffd
	.long	72                      # 0x48
	.long	4294967292              # 0xfffffffc
	.long	67                      # 0x43
	.long	4294967288              # 0xfffffff8
	.long	72                      # 0x48
	.long	4294967280              # 0xfffffff0
	.long	89                      # 0x59
	.long	4294967287              # 0xfffffff7
	.long	69                      # 0x45
	.long	4294967295              # 0xffffffff
	.long	59                      # 0x3b
	.long	5                       # 0x5
	.long	66                      # 0x42
	.long	4                       # 0x4
	.long	57                      # 0x39
	.long	4294967292              # 0xfffffffc
	.long	71                      # 0x47
	.long	4294967294              # 0xfffffffe
	.long	71                      # 0x47
	.long	2                       # 0x2
	.long	58                      # 0x3a
	.long	4294967295              # 0xffffffff
	.long	74                      # 0x4a
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967292              # 0xfffffffc
	.long	44                      # 0x2c
	.long	4294967295              # 0xffffffff
	.long	69                      # 0x45
	.long	0                       # 0x0
	.long	62                      # 0x3e
	.long	4294967289              # 0xfffffff9
	.long	51                      # 0x33
	.long	4294967292              # 0xfffffffc
	.long	47                      # 0x2f
	.long	4294967290              # 0xfffffffa
	.long	42                      # 0x2a
	.long	4294967293              # 0xfffffffd
	.long	41                      # 0x29
	.long	4294967290              # 0xfffffffa
	.long	53                      # 0x35
	.long	8                       # 0x8
	.long	76                      # 0x4c
	.long	4294967287              # 0xfffffff7
	.long	78                      # 0x4e
	.long	4294967285              # 0xfffffff5
	.long	83                      # 0x53
	.long	9                       # 0x9
	.long	52                      # 0x34
	.long	0                       # 0x0
	.long	67                      # 0x43
	.long	4294967291              # 0xfffffffb
	.long	90                      # 0x5a
	.long	4294967293              # 0xfffffffd
	.long	78                      # 0x4e
	.long	4294967288              # 0xfffffff8
	.long	74                      # 0x4a
	.long	4294967287              # 0xfffffff7
	.long	72                      # 0x48
	.long	4294967286              # 0xfffffff6
	.long	72                      # 0x48
	.long	4294967278              # 0xffffffee
	.long	75                      # 0x4b
	.long	4294967284              # 0xfffffff4
	.long	71                      # 0x47
	.long	4294967285              # 0xfffffff5
	.long	63                      # 0x3f
	.long	4294967291              # 0xfffffffb
	.long	70                      # 0x46
	.long	4294967279              # 0xffffffef
	.long	75                      # 0x4b
	.long	4294967282              # 0xfffffff2
	.long	72                      # 0x48
	.long	4294967280              # 0xfffffff0
	.long	67                      # 0x43
	.long	4294967288              # 0xfffffff8
	.long	53                      # 0x35
	.long	4294967282              # 0xfffffff2
	.long	59                      # 0x3b
	.long	4294967287              # 0xfffffff7
	.long	52                      # 0x34
	.long	4294967285              # 0xfffffff5
	.long	68                      # 0x44
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	1                       # 0x1
	.long	67                      # 0x43
	.long	4294967281              # 0xfffffff1
	.long	72                      # 0x48
	.long	4294967291              # 0xfffffffb
	.long	75                      # 0x4b
	.long	4294967288              # 0xfffffff8
	.long	80                      # 0x50
	.long	4294967275              # 0xffffffeb
	.long	83                      # 0x53
	.long	4294967275              # 0xffffffeb
	.long	64                      # 0x40
	.long	4294967283              # 0xfffffff3
	.long	31                      # 0x1f
	.long	4294967271              # 0xffffffe7
	.long	64                      # 0x40
	.long	4294967267              # 0xffffffe3
	.long	94                      # 0x5e
	.long	9                       # 0x9
	.long	75                      # 0x4b
	.long	17                      # 0x11
	.long	63                      # 0x3f
	.long	4294967288              # 0xfffffff8
	.long	74                      # 0x4a
	.long	4294967291              # 0xfffffffb
	.long	35                      # 0x23
	.long	4294967294              # 0xfffffffe
	.long	27                      # 0x1b
	.long	13                      # 0xd
	.long	91                      # 0x5b
	.long	3                       # 0x3
	.long	65                      # 0x41
	.long	4294967289              # 0xfffffff9
	.long	69                      # 0x45
	.long	8                       # 0x8
	.long	77                      # 0x4d
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967286              # 0xfffffff6
	.long	66                      # 0x42
	.long	3                       # 0x3
	.long	62                      # 0x3e
	.long	4294967293              # 0xfffffffd
	.long	68                      # 0x44
	.long	4294967276              # 0xffffffec
	.long	81                      # 0x51
	.long	0                       # 0x0
	.long	30                      # 0x1e
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	4294967293              # 0xfffffffd
	.long	23                      # 0x17
	.long	4294967275              # 0xffffffeb
	.long	74                      # 0x4a
	.long	16                      # 0x10
	.long	66                      # 0x42
	.long	4294967273              # 0xffffffe9
	.long	124                     # 0x7c
	.long	17                      # 0x11
	.long	37                      # 0x25
	.long	44                      # 0x2c
	.long	4294967278              # 0xffffffee
	.long	50                      # 0x32
	.long	4294967262              # 0xffffffde
	.long	4294967274              # 0xffffffea
	.long	127                     # 0x7f
	.size	INIT_MAP_P, 2880

	.type	INIT_LAST_I,@object     # @INIT_LAST_I
	.p2align	4
INIT_LAST_I:
	.long	24                      # 0x18
	.long	0                       # 0x0
	.long	15                      # 0xf
	.long	9                       # 0x9
	.long	8                       # 0x8
	.long	25                      # 0x19
	.long	13                      # 0xd
	.long	18                      # 0x12
	.long	15                      # 0xf
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	19                      # 0x13
	.long	10                      # 0xa
	.long	37                      # 0x25
	.long	12                      # 0xc
	.long	18                      # 0x12
	.long	6                       # 0x6
	.long	29                      # 0x1d
	.long	20                      # 0x14
	.long	33                      # 0x21
	.long	15                      # 0xf
	.long	30                      # 0x1e
	.long	4                       # 0x4
	.long	45                      # 0x2d
	.long	1                       # 0x1
	.long	58                      # 0x3a
	.long	0                       # 0x0
	.long	62                      # 0x3e
	.long	7                       # 0x7
	.long	61                      # 0x3d
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	12                      # 0xc
	.long	38                      # 0x26
	.long	11                      # 0xb
	.long	45                      # 0x2d
	.long	15                      # 0xf
	.long	39                      # 0x27
	.long	11                      # 0xb
	.long	42                      # 0x2a
	.long	13                      # 0xd
	.long	44                      # 0x2c
	.long	16                      # 0x10
	.long	45                      # 0x2d
	.long	12                      # 0xc
	.long	41                      # 0x29
	.long	10                      # 0xa
	.long	49                      # 0x31
	.long	30                      # 0x1e
	.long	34                      # 0x22
	.long	18                      # 0x12
	.long	42                      # 0x2a
	.long	10                      # 0xa
	.long	55                      # 0x37
	.long	17                      # 0x11
	.long	51                      # 0x33
	.long	17                      # 0x11
	.long	46                      # 0x2e
	.long	0                       # 0x0
	.long	89                      # 0x59
	.long	23                      # 0x17
	.long	4294967283              # 0xfffffff3
	.long	26                      # 0x1a
	.long	4294967283              # 0xfffffff3
	.long	40                      # 0x28
	.long	4294967281              # 0xfffffff1
	.long	49                      # 0x31
	.long	4294967282              # 0xfffffff2
	.long	44                      # 0x2c
	.long	3                       # 0x3
	.long	45                      # 0x2d
	.long	6                       # 0x6
	.long	44                      # 0x2c
	.long	34                      # 0x22
	.long	33                      # 0x21
	.long	54                      # 0x36
	.long	19                      # 0x13
	.long	82                      # 0x52
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	26                      # 0x1a
	.long	4294967277              # 0xffffffed
	.long	22                      # 0x16
	.long	4294967279              # 0xffffffef
	.long	26                      # 0x1a
	.long	4294967279              # 0xffffffef
	.long	30                      # 0x1e
	.long	4294967271              # 0xffffffe7
	.long	28                      # 0x1c
	.long	4294967276              # 0xffffffec
	.long	33                      # 0x21
	.long	4294967273              # 0xffffffe9
	.long	37                      # 0x25
	.long	4294967269              # 0xffffffe5
	.long	33                      # 0x21
	.long	4294967273              # 0xffffffe9
	.long	40                      # 0x28
	.long	4294967268              # 0xffffffe4
	.long	38                      # 0x26
	.long	4294967279              # 0xffffffef
	.long	33                      # 0x21
	.long	4294967285              # 0xfffffff5
	.long	40                      # 0x28
	.long	4294967281              # 0xfffffff1
	.long	41                      # 0x29
	.long	4294967290              # 0xfffffffa
	.long	38                      # 0x26
	.long	1                       # 0x1
	.long	41                      # 0x29
	.long	17                      # 0x11
	.long	30                      # 0x1e
	.long	4294967290              # 0xfffffffa
	.long	27                      # 0x1b
	.long	3                       # 0x3
	.long	26                      # 0x1a
	.long	22                      # 0x16
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	37                      # 0x25
	.long	4294967280              # 0xfffffff0
	.long	35                      # 0x23
	.long	4294967292              # 0xfffffffc
	.long	38                      # 0x26
	.long	4294967288              # 0xfffffff8
	.long	38                      # 0x26
	.long	4294967293              # 0xfffffffd
	.long	37                      # 0x25
	.long	3                       # 0x3
	.long	38                      # 0x26
	.long	5                       # 0x5
	.long	42                      # 0x2a
	.long	0                       # 0x0
	.long	35                      # 0x23
	.long	16                      # 0x10
	.long	39                      # 0x27
	.long	22                      # 0x16
	.long	14                      # 0xe
	.long	48                      # 0x30
	.long	27                      # 0x1b
	.long	37                      # 0x25
	.long	21                      # 0x15
	.long	60                      # 0x3c
	.long	12                      # 0xc
	.long	68                      # 0x44
	.long	2                       # 0x2
	.long	97                      # 0x61
	.size	INIT_LAST_I, 960

	.type	INIT_LAST_P,@object     # @INIT_LAST_P
	.p2align	4
INIT_LAST_P:
	.long	11                      # 0xb
	.long	28                      # 0x1c
	.long	2                       # 0x2
	.long	40                      # 0x28
	.long	3                       # 0x3
	.long	44                      # 0x2c
	.long	0                       # 0x0
	.long	49                      # 0x31
	.long	0                       # 0x0
	.long	46                      # 0x2e
	.long	2                       # 0x2
	.long	44                      # 0x2c
	.long	2                       # 0x2
	.long	51                      # 0x33
	.long	0                       # 0x0
	.long	47                      # 0x2f
	.long	4                       # 0x4
	.long	39                      # 0x27
	.long	2                       # 0x2
	.long	62                      # 0x3e
	.long	6                       # 0x6
	.long	46                      # 0x2e
	.long	0                       # 0x0
	.long	54                      # 0x36
	.long	3                       # 0x3
	.long	54                      # 0x36
	.long	2                       # 0x2
	.long	58                      # 0x3a
	.long	4                       # 0x4
	.long	63                      # 0x3f
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	6                       # 0x6
	.long	51                      # 0x33
	.long	6                       # 0x6
	.long	57                      # 0x39
	.long	7                       # 0x7
	.long	53                      # 0x35
	.long	6                       # 0x6
	.long	52                      # 0x34
	.long	6                       # 0x6
	.long	55                      # 0x37
	.long	11                      # 0xb
	.long	45                      # 0x2d
	.long	14                      # 0xe
	.long	36                      # 0x24
	.long	8                       # 0x8
	.long	53                      # 0x35
	.long	4294967295              # 0xffffffff
	.long	82                      # 0x52
	.long	7                       # 0x7
	.long	55                      # 0x37
	.long	4294967293              # 0xfffffffd
	.long	78                      # 0x4e
	.long	15                      # 0xf
	.long	46                      # 0x2e
	.long	22                      # 0x16
	.long	31                      # 0x1f
	.long	4294967295              # 0xffffffff
	.long	84                      # 0x54
	.long	9                       # 0x9
	.long	4294967294              # 0xfffffffe
	.long	26                      # 0x1a
	.long	4294967287              # 0xfffffff7
	.long	33                      # 0x21
	.long	4294967287              # 0xfffffff7
	.long	39                      # 0x27
	.long	4294967289              # 0xfffffff9
	.long	41                      # 0x29
	.long	4294967294              # 0xfffffffe
	.long	45                      # 0x2d
	.long	3                       # 0x3
	.long	49                      # 0x31
	.long	9                       # 0x9
	.long	45                      # 0x2d
	.long	27                      # 0x1b
	.long	36                      # 0x24
	.long	59                      # 0x3b
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	25                      # 0x19
	.long	7                       # 0x7
	.long	30                      # 0x1e
	.long	4294967289              # 0xfffffff9
	.long	28                      # 0x1c
	.long	3                       # 0x3
	.long	28                      # 0x1c
	.long	4                       # 0x4
	.long	32                      # 0x20
	.long	0                       # 0x0
	.long	34                      # 0x22
	.long	4294967295              # 0xffffffff
	.long	30                      # 0x1e
	.long	6                       # 0x6
	.long	30                      # 0x1e
	.long	6                       # 0x6
	.long	32                      # 0x20
	.long	9                       # 0x9
	.long	31                      # 0x1f
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	27                      # 0x1b
	.long	26                      # 0x1a
	.long	30                      # 0x1e
	.long	37                      # 0x25
	.long	20                      # 0x14
	.long	28                      # 0x1c
	.long	34                      # 0x22
	.long	17                      # 0x11
	.long	70                      # 0x46
	.long	1                       # 0x1
	.long	67                      # 0x43
	.long	5                       # 0x5
	.long	59                      # 0x3b
	.long	9                       # 0x9
	.long	67                      # 0x43
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	16                      # 0x10
	.long	30                      # 0x1e
	.long	18                      # 0x12
	.long	32                      # 0x20
	.long	18                      # 0x12
	.long	35                      # 0x23
	.long	22                      # 0x16
	.long	29                      # 0x1d
	.long	24                      # 0x18
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	38                      # 0x26
	.long	18                      # 0x12
	.long	43                      # 0x2b
	.long	20                      # 0x14
	.long	41                      # 0x29
	.long	11                      # 0xb
	.long	63                      # 0x3f
	.long	9                       # 0x9
	.long	59                      # 0x3b
	.long	9                       # 0x9
	.long	64                      # 0x40
	.long	4294967295              # 0xffffffff
	.long	94                      # 0x5e
	.long	4294967294              # 0xfffffffe
	.long	89                      # 0x59
	.long	4294967287              # 0xfffffff7
	.long	108                     # 0x6c
	.long	4                       # 0x4
	.long	45                      # 0x2d
	.long	10                      # 0xa
	.long	28                      # 0x1c
	.long	10                      # 0xa
	.long	31                      # 0x1f
	.long	33                      # 0x21
	.long	4294967285              # 0xfffffff5
	.long	52                      # 0x34
	.long	4294967253              # 0xffffffd5
	.long	18                      # 0x12
	.long	15                      # 0xf
	.long	28                      # 0x1c
	.long	0                       # 0x0
	.long	35                      # 0x23
	.long	4294967274              # 0xffffffea
	.long	38                      # 0x26
	.long	4294967271              # 0xffffffe7
	.long	34                      # 0x22
	.long	0                       # 0x0
	.long	39                      # 0x27
	.long	4294967278              # 0xffffffee
	.long	32                      # 0x20
	.long	4294967284              # 0xfffffff4
	.long	102                     # 0x66
	.long	4294967202              # 0xffffffa2
	.zero	8
	.long	56                      # 0x38
	.long	4294967281              # 0xfffffff1
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	33                      # 0x21
	.long	4294967292              # 0xfffffffc
	.long	29                      # 0x1d
	.long	10                      # 0xa
	.long	37                      # 0x25
	.long	4294967291              # 0xfffffffb
	.long	51                      # 0x33
	.long	4294967267              # 0xffffffe3
	.long	39                      # 0x27
	.long	4294967287              # 0xfffffff7
	.long	52                      # 0x34
	.long	4294967262              # 0xffffffde
	.long	69                      # 0x45
	.long	4294967238              # 0xffffffc6
	.long	67                      # 0x43
	.long	4294967233              # 0xffffffc1
	.long	44                      # 0x2c
	.long	4294967291              # 0xfffffffb
	.long	32                      # 0x20
	.long	7                       # 0x7
	.long	55                      # 0x37
	.long	4294967267              # 0xffffffe3
	.long	32                      # 0x20
	.long	1                       # 0x1
	.zero	8
	.long	27                      # 0x1b
	.long	36                      # 0x24
	.long	17                      # 0x11
	.long	4294967286              # 0xfffffff6
	.long	32                      # 0x20
	.long	4294967283              # 0xfffffff3
	.long	42                      # 0x2a
	.long	4294967287              # 0xfffffff7
	.long	49                      # 0x31
	.long	4294967291              # 0xfffffffb
	.long	53                      # 0x35
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	3                       # 0x3
	.long	68                      # 0x44
	.long	10                      # 0xa
	.long	66                      # 0x42
	.long	27                      # 0x1b
	.long	47                      # 0x2f
	.long	57                      # 0x39
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	33                      # 0x21
	.long	4294967271              # 0xffffffe7
	.long	34                      # 0x22
	.long	4294967266              # 0xffffffe2
	.long	36                      # 0x24
	.long	4294967268              # 0xffffffe4
	.long	38                      # 0x26
	.long	4294967268              # 0xffffffe4
	.long	38                      # 0x26
	.long	4294967269              # 0xffffffe5
	.long	34                      # 0x22
	.long	4294967278              # 0xffffffee
	.long	35                      # 0x23
	.long	4294967280              # 0xfffffff0
	.long	34                      # 0x22
	.long	4294967282              # 0xfffffff2
	.long	32                      # 0x20
	.long	4294967288              # 0xfffffff8
	.long	37                      # 0x25
	.long	4294967290              # 0xfffffffa
	.long	35                      # 0x23
	.long	0                       # 0x0
	.long	30                      # 0x1e
	.long	10                      # 0xa
	.long	28                      # 0x1c
	.long	18                      # 0x12
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	29                      # 0x1d
	.long	41                      # 0x29
	.long	0                       # 0x0
	.long	75                      # 0x4b
	.long	2                       # 0x2
	.long	72                      # 0x48
	.long	8                       # 0x8
	.long	77                      # 0x4d
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	14                      # 0xe
	.long	35                      # 0x23
	.long	18                      # 0x12
	.long	31                      # 0x1f
	.long	17                      # 0x11
	.long	35                      # 0x23
	.long	21                      # 0x15
	.long	30                      # 0x1e
	.long	17                      # 0x11
	.long	45                      # 0x2d
	.long	20                      # 0x14
	.long	42                      # 0x2a
	.long	18                      # 0x12
	.long	45                      # 0x2d
	.long	27                      # 0x1b
	.long	26                      # 0x1a
	.long	16                      # 0x10
	.long	54                      # 0x36
	.long	7                       # 0x7
	.long	66                      # 0x42
	.long	16                      # 0x10
	.long	56                      # 0x38
	.long	11                      # 0xb
	.long	73                      # 0x49
	.long	10                      # 0xa
	.long	67                      # 0x43
	.long	4294967286              # 0xfffffff6
	.long	116                     # 0x74
	.long	4                       # 0x4
	.long	39                      # 0x27
	.long	0                       # 0x0
	.long	42                      # 0x2a
	.long	7                       # 0x7
	.long	34                      # 0x22
	.long	11                      # 0xb
	.long	29                      # 0x1d
	.long	8                       # 0x8
	.long	31                      # 0x1f
	.long	6                       # 0x6
	.long	37                      # 0x25
	.long	7                       # 0x7
	.long	42                      # 0x2a
	.long	3                       # 0x3
	.long	40                      # 0x28
	.long	8                       # 0x8
	.long	33                      # 0x21
	.long	13                      # 0xd
	.long	43                      # 0x2b
	.long	13                      # 0xd
	.long	36                      # 0x24
	.long	4                       # 0x4
	.long	47                      # 0x2f
	.long	3                       # 0x3
	.long	55                      # 0x37
	.long	2                       # 0x2
	.long	58                      # 0x3a
	.long	6                       # 0x6
	.long	60                      # 0x3c
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	8                       # 0x8
	.long	44                      # 0x2c
	.long	11                      # 0xb
	.long	44                      # 0x2c
	.long	14                      # 0xe
	.long	42                      # 0x2a
	.long	7                       # 0x7
	.long	48                      # 0x30
	.long	4                       # 0x4
	.long	56                      # 0x38
	.long	4                       # 0x4
	.long	52                      # 0x34
	.long	13                      # 0xd
	.long	37                      # 0x25
	.long	9                       # 0x9
	.long	49                      # 0x31
	.long	19                      # 0x13
	.long	58                      # 0x3a
	.long	10                      # 0xa
	.long	48                      # 0x30
	.long	12                      # 0xc
	.long	45                      # 0x2d
	.long	0                       # 0x0
	.long	69                      # 0x45
	.long	20                      # 0x14
	.long	33                      # 0x21
	.long	8                       # 0x8
	.long	63                      # 0x3f
	.long	9                       # 0x9
	.long	4294967294              # 0xfffffffe
	.long	30                      # 0x1e
	.long	4294967286              # 0xfffffff6
	.long	31                      # 0x1f
	.long	4294967292              # 0xfffffffc
	.long	33                      # 0x21
	.long	4294967295              # 0xffffffff
	.long	33                      # 0x21
	.long	7                       # 0x7
	.long	31                      # 0x1f
	.long	12                      # 0xc
	.long	37                      # 0x25
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	38                      # 0x26
	.long	20                      # 0x14
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	35                      # 0x23
	.long	4294967278              # 0xffffffee
	.long	33                      # 0x21
	.long	4294967271              # 0xffffffe7
	.long	28                      # 0x1c
	.long	4294967293              # 0xfffffffd
	.long	24                      # 0x18
	.long	10                      # 0xa
	.long	27                      # 0x1b
	.long	0                       # 0x0
	.long	34                      # 0x22
	.long	4294967282              # 0xfffffff2
	.long	52                      # 0x34
	.long	4294967252              # 0xffffffd4
	.long	39                      # 0x27
	.long	4294967272              # 0xffffffe8
	.long	19                      # 0x13
	.long	17                      # 0x11
	.long	31                      # 0x1f
	.long	25                      # 0x19
	.long	36                      # 0x24
	.long	29                      # 0x1d
	.long	24                      # 0x18
	.long	33                      # 0x21
	.long	34                      # 0x22
	.long	15                      # 0xf
	.long	30                      # 0x1e
	.long	20                      # 0x14
	.long	22                      # 0x16
	.long	73                      # 0x49
	.long	20                      # 0x14
	.long	34                      # 0x22
	.long	19                      # 0x13
	.long	31                      # 0x1f
	.long	27                      # 0x1b
	.long	44                      # 0x2c
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	19                      # 0x13
	.long	16                      # 0x10
	.long	15                      # 0xf
	.long	36                      # 0x24
	.long	15                      # 0xf
	.long	36                      # 0x24
	.long	21                      # 0x15
	.long	28                      # 0x1c
	.long	25                      # 0x19
	.long	21                      # 0x15
	.long	30                      # 0x1e
	.long	20                      # 0x14
	.long	31                      # 0x1f
	.long	12                      # 0xc
	.long	27                      # 0x1b
	.long	16                      # 0x10
	.long	24                      # 0x18
	.long	42                      # 0x2a
	.long	0                       # 0x0
	.long	93                      # 0x5d
	.long	14                      # 0xe
	.long	56                      # 0x38
	.long	15                      # 0xf
	.long	57                      # 0x39
	.long	26                      # 0x1a
	.long	38                      # 0x26
	.long	4294967272              # 0xffffffe8
	.long	127                     # 0x7f
	.size	INIT_LAST_P, 2880

	.type	INIT_ONE_I,@object      # @INIT_ONE_I
	.p2align	4
INIT_ONE_I:
	.long	4294967293              # 0xfffffffd
	.long	71                      # 0x47
	.long	4294967290              # 0xfffffffa
	.long	42                      # 0x2a
	.long	4294967291              # 0xfffffffb
	.long	50                      # 0x32
	.long	4294967293              # 0xfffffffd
	.long	54                      # 0x36
	.long	4294967294              # 0xfffffffe
	.long	62                      # 0x3e
	.long	4294967291              # 0xfffffffb
	.long	67                      # 0x43
	.long	4294967291              # 0xfffffffb
	.long	27                      # 0x1b
	.long	4294967293              # 0xfffffffd
	.long	39                      # 0x27
	.long	4294967294              # 0xfffffffe
	.long	44                      # 0x2c
	.long	0                       # 0x0
	.long	46                      # 0x2e
	.long	4294967293              # 0xfffffffd
	.long	75                      # 0x4b
	.long	4294967295              # 0xffffffff
	.long	23                      # 0x17
	.long	1                       # 0x1
	.long	34                      # 0x22
	.long	1                       # 0x1
	.long	43                      # 0x2b
	.long	0                       # 0x0
	.long	54                      # 0x36
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967284              # 0xfffffff4
	.long	92                      # 0x5c
	.long	4294967281              # 0xfffffff1
	.long	55                      # 0x37
	.long	4294967286              # 0xfffffff6
	.long	60                      # 0x3c
	.long	4294967290              # 0xfffffffa
	.long	62                      # 0x3e
	.long	4294967292              # 0xfffffffc
	.long	65                      # 0x41
	.long	4294967285              # 0xfffffff5
	.long	97                      # 0x61
	.long	4294967276              # 0xffffffec
	.long	84                      # 0x54
	.long	4294967285              # 0xfffffff5
	.long	79                      # 0x4f
	.long	4294967290              # 0xfffffffa
	.long	73                      # 0x49
	.long	4294967292              # 0xfffffffc
	.long	74                      # 0x4a
	.long	4294967288              # 0xfffffff8
	.long	78                      # 0x4e
	.long	4294967291              # 0xfffffffb
	.long	33                      # 0x21
	.long	4294967292              # 0xfffffffc
	.long	48                      # 0x30
	.long	4294967294              # 0xfffffffe
	.long	53                      # 0x35
	.long	4294967293              # 0xfffffffd
	.long	62                      # 0x3e
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.size	INIT_ONE_I, 320

	.type	INIT_ONE_P,@object      # @INIT_ONE_P
	.p2align	4
INIT_ONE_P:
	.long	4294967290              # 0xfffffffa
	.long	76                      # 0x4c
	.long	4294967294              # 0xfffffffe
	.long	44                      # 0x2c
	.long	0                       # 0x0
	.long	45                      # 0x2d
	.long	0                       # 0x0
	.long	52                      # 0x34
	.long	4294967293              # 0xfffffffd
	.long	64                      # 0x40
	.long	4294967287              # 0xfffffff7
	.long	77                      # 0x4d
	.long	3                       # 0x3
	.long	24                      # 0x18
	.long	0                       # 0x0
	.long	42                      # 0x2a
	.long	0                       # 0x0
	.long	48                      # 0x30
	.long	0                       # 0x0
	.long	55                      # 0x37
	.long	4294967290              # 0xfffffffa
	.long	66                      # 0x42
	.long	4294967289              # 0xfffffff9
	.long	35                      # 0x23
	.long	4294967289              # 0xfffffff9
	.long	42                      # 0x2a
	.long	4294967288              # 0xfffffff8
	.long	45                      # 0x2d
	.long	4294967291              # 0xfffffffb
	.long	48                      # 0x30
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	1                       # 0x1
	.long	58                      # 0x3a
	.long	4294967293              # 0xfffffffd
	.long	29                      # 0x1d
	.long	4294967295              # 0xffffffff
	.long	36                      # 0x24
	.long	1                       # 0x1
	.long	38                      # 0x26
	.long	2                       # 0x2
	.long	43                      # 0x2b
	.long	0                       # 0x0
	.long	70                      # 0x46
	.long	4294967292              # 0xfffffffc
	.long	29                      # 0x1d
	.long	5                       # 0x5
	.long	31                      # 0x1f
	.long	7                       # 0x7
	.long	42                      # 0x2a
	.long	1                       # 0x1
	.long	59                      # 0x3b
	.long	0                       # 0x0
	.long	58                      # 0x3a
	.long	8                       # 0x8
	.long	5                       # 0x5
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	18                      # 0x12
	.long	13                      # 0xd
	.long	27                      # 0x1b
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967273              # 0xffffffe9
	.long	112                     # 0x70
	.long	4294967281              # 0xfffffff1
	.long	71                      # 0x47
	.long	4294967289              # 0xfffffff9
	.long	61                      # 0x3d
	.long	0                       # 0x0
	.long	53                      # 0x35
	.long	4294967291              # 0xfffffffb
	.long	66                      # 0x42
	.long	4294967275              # 0xffffffeb
	.long	101                     # 0x65
	.long	4294967293              # 0xfffffffd
	.long	39                      # 0x27
	.long	4294967291              # 0xfffffffb
	.long	53                      # 0x35
	.long	4294967289              # 0xfffffff9
	.long	61                      # 0x3d
	.long	4294967285              # 0xfffffff5
	.long	75                      # 0x4b
	.long	4294967291              # 0xfffffffb
	.long	71                      # 0x47
	.long	0                       # 0x0
	.long	24                      # 0x18
	.long	4294967295              # 0xffffffff
	.long	36                      # 0x24
	.long	4294967294              # 0xfffffffe
	.long	42                      # 0x2a
	.long	4294967294              # 0xfffffffe
	.long	52                      # 0x34
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967285              # 0xfffffff5
	.long	76                      # 0x4c
	.long	4294967286              # 0xfffffff6
	.long	44                      # 0x2c
	.long	4294967286              # 0xfffffff6
	.long	52                      # 0x34
	.long	4294967286              # 0xfffffff6
	.long	57                      # 0x39
	.long	4294967287              # 0xfffffff7
	.long	58                      # 0x3a
	.long	2                       # 0x2
	.long	66                      # 0x42
	.long	4294967287              # 0xfffffff7
	.long	34                      # 0x22
	.long	1                       # 0x1
	.long	32                      # 0x20
	.long	11                      # 0xb
	.long	31                      # 0x1f
	.long	5                       # 0x5
	.long	52                      # 0x34
	.long	3                       # 0x3
	.long	52                      # 0x34
	.long	7                       # 0x7
	.long	4                       # 0x4
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	17                      # 0x11
	.long	8                       # 0x8
	.long	16                      # 0x10
	.long	19                      # 0x13
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967272              # 0xffffffe8
	.long	115                     # 0x73
	.long	4294967274              # 0xffffffea
	.long	82                      # 0x52
	.long	4294967287              # 0xfffffff7
	.long	62                      # 0x3e
	.long	0                       # 0x0
	.long	53                      # 0x35
	.long	0                       # 0x0
	.long	59                      # 0x3b
	.long	4294967275              # 0xffffffeb
	.long	100                     # 0x64
	.long	4294967282              # 0xfffffff2
	.long	57                      # 0x39
	.long	4294967284              # 0xfffffff4
	.long	67                      # 0x43
	.long	4294967285              # 0xfffffff5
	.long	71                      # 0x47
	.long	4294967286              # 0xfffffff6
	.long	77                      # 0x4d
	.long	4294967287              # 0xfffffff7
	.long	71                      # 0x47
	.long	4294967289              # 0xfffffff9
	.long	37                      # 0x25
	.long	4294967288              # 0xfffffff8
	.long	44                      # 0x2c
	.long	4294967285              # 0xfffffff5
	.long	49                      # 0x31
	.long	4294967286              # 0xfffffff6
	.long	56                      # 0x38
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967286              # 0xfffffff6
	.long	82                      # 0x52
	.long	4294967288              # 0xfffffff8
	.long	48                      # 0x30
	.long	4294967288              # 0xfffffff8
	.long	61                      # 0x3d
	.long	4294967288              # 0xfffffff8
	.long	66                      # 0x42
	.long	4294967289              # 0xfffffff9
	.long	70                      # 0x46
	.long	4294967292              # 0xfffffffc
	.long	79                      # 0x4f
	.long	4294967274              # 0xffffffea
	.long	69                      # 0x45
	.long	4294967280              # 0xfffffff0
	.long	75                      # 0x4b
	.long	4294967294              # 0xfffffffe
	.long	58                      # 0x3a
	.long	1                       # 0x1
	.long	58                      # 0x3a
	.long	4294967283              # 0xfffffff3
	.long	81                      # 0x51
	.long	4294967290              # 0xfffffffa
	.long	38                      # 0x26
	.long	4294967283              # 0xfffffff3
	.long	62                      # 0x3e
	.long	4294967290              # 0xfffffffa
	.long	58                      # 0x3a
	.long	4294967294              # 0xfffffffe
	.long	59                      # 0x3b
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.size	INIT_ONE_P, 960

	.type	INIT_ABS_I,@object      # @INIT_ABS_I
	.p2align	4
INIT_ABS_I:
	.long	0                       # 0x0
	.long	58                      # 0x3a
	.long	1                       # 0x1
	.long	63                      # 0x3f
	.long	4294967294              # 0xfffffffe
	.long	72                      # 0x48
	.long	4294967295              # 0xffffffff
	.long	74                      # 0x4a
	.long	4294967287              # 0xfffffff7
	.long	91                      # 0x5b
	.long	4294967280              # 0xfffffff0
	.long	64                      # 0x40
	.long	4294967288              # 0xfffffff8
	.long	68                      # 0x44
	.long	4294967286              # 0xfffffff6
	.long	78                      # 0x4e
	.long	4294967290              # 0xfffffffa
	.long	77                      # 0x4d
	.long	4294967286              # 0xfffffff6
	.long	86                      # 0x56
	.long	4294967294              # 0xfffffffe
	.long	55                      # 0x37
	.long	0                       # 0x0
	.long	61                      # 0x3d
	.long	1                       # 0x1
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	68                      # 0x44
	.long	4294967287              # 0xfffffff7
	.long	92                      # 0x5c
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967284              # 0xfffffff4
	.long	73                      # 0x49
	.long	4294967288              # 0xfffffff8
	.long	76                      # 0x4c
	.long	4294967289              # 0xfffffff9
	.long	80                      # 0x50
	.long	4294967287              # 0xfffffff7
	.long	88                      # 0x58
	.long	4294967279              # 0xffffffef
	.long	110                     # 0x6e
	.long	4294967283              # 0xfffffff3
	.long	86                      # 0x56
	.long	4294967283              # 0xfffffff3
	.long	96                      # 0x60
	.long	4294967285              # 0xfffffff5
	.long	97                      # 0x61
	.long	4294967277              # 0xffffffed
	.long	117                     # 0x75
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967283              # 0xfffffff3
	.long	71                      # 0x47
	.long	4294967286              # 0xfffffff6
	.long	79                      # 0x4f
	.long	4294967284              # 0xfffffff4
	.long	86                      # 0x56
	.long	4294967283              # 0xfffffff3
	.long	90                      # 0x5a
	.long	4294967282              # 0xfffffff2
	.long	97                      # 0x61
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.size	INIT_ABS_I, 320

	.type	INIT_ABS_P,@object      # @INIT_ABS_P
	.p2align	4
INIT_ABS_P:
	.long	4294967294              # 0xfffffffe
	.long	59                      # 0x3b
	.long	4294967292              # 0xfffffffc
	.long	70                      # 0x46
	.long	4294967292              # 0xfffffffc
	.long	75                      # 0x4b
	.long	4294967288              # 0xfffffff8
	.long	82                      # 0x52
	.long	4294967279              # 0xffffffef
	.long	102                     # 0x66
	.long	4294967290              # 0xfffffffa
	.long	59                      # 0x3b
	.long	4294967289              # 0xfffffff9
	.long	71                      # 0x47
	.long	4294967284              # 0xfffffff4
	.long	83                      # 0x53
	.long	4294967285              # 0xfffffff5
	.long	87                      # 0x57
	.long	4294967266              # 0xffffffe2
	.long	119                     # 0x77
	.long	4294967284              # 0xfffffff4
	.long	56                      # 0x38
	.long	4294967290              # 0xfffffffa
	.long	60                      # 0x3c
	.long	4294967291              # 0xfffffffb
	.long	62                      # 0x3e
	.long	4294967288              # 0xfffffff8
	.long	66                      # 0x42
	.long	4294967288              # 0xfffffff8
	.long	76                      # 0x4c
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967290              # 0xfffffffa
	.long	55                      # 0x37
	.long	0                       # 0x0
	.long	58                      # 0x3a
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967293              # 0xfffffffd
	.long	74                      # 0x4a
	.long	4294967286              # 0xfffffff6
	.long	90                      # 0x5a
	.long	4294967294              # 0xfffffffe
	.long	58                      # 0x3a
	.long	4294967293              # 0xfffffffd
	.long	72                      # 0x48
	.long	4294967293              # 0xfffffffd
	.long	81                      # 0x51
	.long	4294967285              # 0xfffffff5
	.long	97                      # 0x61
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	2                       # 0x2
	.long	40                      # 0x28
	.long	0                       # 0x0
	.long	58                      # 0x3a
	.long	4294967293              # 0xfffffffd
	.long	70                      # 0x46
	.long	4294967290              # 0xfffffffa
	.long	79                      # 0x4f
	.long	4294967288              # 0xfffffff8
	.long	85                      # 0x55
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967285              # 0xfffffff5
	.long	77                      # 0x4d
	.long	4294967287              # 0xfffffff7
	.long	80                      # 0x50
	.long	4294967287              # 0xfffffff7
	.long	84                      # 0x54
	.long	4294967286              # 0xfffffff6
	.long	87                      # 0x57
	.long	4294967262              # 0xffffffde
	.long	127                     # 0x7f
	.long	4294967281              # 0xfffffff1
	.long	77                      # 0x4d
	.long	4294967279              # 0xffffffef
	.long	91                      # 0x5b
	.long	4294967271              # 0xffffffe7
	.long	107                     # 0x6b
	.long	4294967271              # 0xffffffe7
	.long	111                     # 0x6f
	.long	4294967268              # 0xffffffe4
	.long	122                     # 0x7a
	.long	4294967287              # 0xfffffff7
	.long	57                      # 0x39
	.long	4294967290              # 0xfffffffa
	.long	63                      # 0x3f
	.long	4294967292              # 0xfffffffc
	.long	65                      # 0x41
	.long	4294967292              # 0xfffffffc
	.long	67                      # 0x43
	.long	4294967289              # 0xfffffff9
	.long	82                      # 0x52
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967280              # 0xfffffff0
	.long	72                      # 0x48
	.long	4294967289              # 0xfffffff9
	.long	69                      # 0x45
	.long	4294967292              # 0xfffffffc
	.long	69                      # 0x45
	.long	4294967291              # 0xfffffffb
	.long	74                      # 0x4a
	.long	4294967287              # 0xfffffff7
	.long	86                      # 0x56
	.long	4294967294              # 0xfffffffe
	.long	55                      # 0x37
	.long	4294967294              # 0xfffffffe
	.long	67                      # 0x43
	.long	0                       # 0x0
	.long	73                      # 0x49
	.long	4294967288              # 0xfffffff8
	.long	89                      # 0x59
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	3                       # 0x3
	.long	37                      # 0x25
	.long	4294967295              # 0xffffffff
	.long	61                      # 0x3d
	.long	4294967291              # 0xfffffffb
	.long	73                      # 0x49
	.long	4294967295              # 0xffffffff
	.long	70                      # 0x46
	.long	4294967292              # 0xfffffffc
	.long	78                      # 0x4e
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967282              # 0xfffffff2
	.long	85                      # 0x55
	.long	4294967283              # 0xfffffff3
	.long	89                      # 0x59
	.long	4294967283              # 0xfffffff3
	.long	94                      # 0x5e
	.long	4294967285              # 0xfffffff5
	.long	92                      # 0x5c
	.long	4294967267              # 0xffffffe3
	.long	127                     # 0x7f
	.long	4294967275              # 0xffffffeb
	.long	85                      # 0x55
	.long	4294967280              # 0xfffffff0
	.long	88                      # 0x58
	.long	4294967273              # 0xffffffe9
	.long	104                     # 0x68
	.long	4294967281              # 0xfffffff1
	.long	98                      # 0x62
	.long	4294967259              # 0xffffffdb
	.long	127                     # 0x7f
	.long	4294967284              # 0xfffffff4
	.long	59                      # 0x3b
	.long	4294967288              # 0xfffffff8
	.long	63                      # 0x3f
	.long	4294967287              # 0xfffffff7
	.long	67                      # 0x43
	.long	4294967290              # 0xfffffffa
	.long	68                      # 0x44
	.long	4294967286              # 0xfffffff6
	.long	79                      # 0x4f
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967282              # 0xfffffff2
	.long	75                      # 0x4b
	.long	4294967286              # 0xfffffff6
	.long	79                      # 0x4f
	.long	4294967287              # 0xfffffff7
	.long	83                      # 0x53
	.long	4294967284              # 0xfffffff4
	.long	92                      # 0x5c
	.long	4294967278              # 0xffffffee
	.long	108                     # 0x6c
	.long	4294967283              # 0xfffffff3
	.long	78                      # 0x4e
	.long	4294967287              # 0xfffffff7
	.long	83                      # 0x53
	.long	4294967292              # 0xfffffffc
	.long	81                      # 0x51
	.long	4294967283              # 0xfffffff3
	.long	99                      # 0x63
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967280              # 0xfffffff0
	.long	73                      # 0x49
	.long	4294967286              # 0xfffffff6
	.long	76                      # 0x4c
	.long	4294967283              # 0xfffffff3
	.long	86                      # 0x56
	.long	4294967287              # 0xfffffff7
	.long	83                      # 0x53
	.long	4294967286              # 0xfffffff6
	.long	87                      # 0x57
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.size	INIT_ABS_P, 960

	.type	INIT_FLD_MAP_I,@object  # @INIT_FLD_MAP_I
	.p2align	4
INIT_FLD_MAP_I:
	.long	4294967290              # 0xfffffffa
	.long	93                      # 0x5d
	.long	4294967290              # 0xfffffffa
	.long	84                      # 0x54
	.long	4294967288              # 0xfffffff8
	.long	79                      # 0x4f
	.long	0                       # 0x0
	.long	66                      # 0x42
	.long	4294967295              # 0xffffffff
	.long	71                      # 0x47
	.long	0                       # 0x0
	.long	62                      # 0x3e
	.long	4294967294              # 0xfffffffe
	.long	60                      # 0x3c
	.long	4294967294              # 0xfffffffe
	.long	59                      # 0x3b
	.long	4294967291              # 0xfffffffb
	.long	75                      # 0x4b
	.long	4294967293              # 0xfffffffd
	.long	62                      # 0x3e
	.long	4294967292              # 0xfffffffc
	.long	58                      # 0x3a
	.long	4294967287              # 0xfffffff7
	.long	66                      # 0x42
	.long	4294967295              # 0xffffffff
	.long	79                      # 0x4f
	.long	0                       # 0x0
	.long	71                      # 0x47
	.long	3                       # 0x3
	.long	68                      # 0x44
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	10                      # 0xa
	.long	44                      # 0x2c
	.long	4294967289              # 0xfffffff9
	.long	62                      # 0x3e
	.long	15                      # 0xf
	.long	36                      # 0x24
	.long	14                      # 0xe
	.long	40                      # 0x28
	.long	16                      # 0x10
	.long	27                      # 0x1b
	.long	12                      # 0xc
	.long	29                      # 0x1d
	.long	1                       # 0x1
	.long	44                      # 0x2c
	.long	20                      # 0x14
	.long	36                      # 0x24
	.long	18                      # 0x12
	.long	32                      # 0x20
	.long	5                       # 0x5
	.long	42                      # 0x2a
	.long	1                       # 0x1
	.long	48                      # 0x30
	.long	10                      # 0xa
	.long	62                      # 0x3e
	.long	17                      # 0x11
	.long	46                      # 0x2e
	.long	9                       # 0x9
	.long	64                      # 0x40
	.long	4294967282              # 0xfffffff2
	.long	106                     # 0x6a
	.long	4294967283              # 0xfffffff3
	.long	97                      # 0x61
	.long	4294967281              # 0xfffffff1
	.long	90                      # 0x5a
	.long	4294967284              # 0xfffffff4
	.long	90                      # 0x5a
	.long	4294967278              # 0xffffffee
	.long	88                      # 0x58
	.long	4294967286              # 0xfffffff6
	.long	73                      # 0x49
	.long	4294967287              # 0xfffffff7
	.long	79                      # 0x4f
	.long	4294967282              # 0xfffffff2
	.long	86                      # 0x56
	.long	4294967286              # 0xfffffff6
	.long	73                      # 0x49
	.long	4294967286              # 0xfffffff6
	.long	70                      # 0x46
	.long	4294967286              # 0xfffffff6
	.long	69                      # 0x45
	.long	4294967291              # 0xfffffffb
	.long	66                      # 0x42
	.long	4294967287              # 0xfffffff7
	.long	64                      # 0x40
	.long	4294967291              # 0xfffffffb
	.long	58                      # 0x3a
	.long	2                       # 0x2
	.long	59                      # 0x3b
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967284              # 0xfffffff4
	.long	104                     # 0x68
	.long	4294967285              # 0xfffffff5
	.long	97                      # 0x61
	.long	4294967280              # 0xfffffff0
	.long	96                      # 0x60
	.long	4294967289              # 0xfffffff9
	.long	88                      # 0x58
	.long	4294967288              # 0xfffffff8
	.long	85                      # 0x55
	.long	4294967289              # 0xfffffff9
	.long	85                      # 0x55
	.long	4294967287              # 0xfffffff7
	.long	85                      # 0x55
	.long	4294967283              # 0xfffffff3
	.long	88                      # 0x58
	.long	4                       # 0x4
	.long	66                      # 0x42
	.long	4294967293              # 0xfffffffd
	.long	77                      # 0x4d
	.long	4294967293              # 0xfffffffd
	.long	76                      # 0x4c
	.long	4294967290              # 0xfffffffa
	.long	76                      # 0x4c
	.long	10                      # 0xa
	.long	58                      # 0x3a
	.long	4294967295              # 0xffffffff
	.long	76                      # 0x4c
	.long	4294967295              # 0xffffffff
	.long	83                      # 0x53
	.long	4294967289              # 0xfffffff9
	.long	99                      # 0x63
	.long	4294967282              # 0xfffffff2
	.long	95                      # 0x5f
	.long	2                       # 0x2
	.long	95                      # 0x5f
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	76                      # 0x4c
	.long	4294967291              # 0xfffffffb
	.long	74                      # 0x4a
	.long	0                       # 0x0
	.long	70                      # 0x46
	.long	4294967285              # 0xfffffff5
	.long	75                      # 0x4b
	.long	1                       # 0x1
	.long	68                      # 0x44
	.long	0                       # 0x0
	.long	65                      # 0x41
	.long	4294967282              # 0xfffffff2
	.long	73                      # 0x49
	.long	3                       # 0x3
	.long	62                      # 0x3e
	.long	4                       # 0x4
	.long	62                      # 0x3e
	.long	4294967295              # 0xffffffff
	.long	68                      # 0x44
	.long	4294967283              # 0xfffffff3
	.long	75                      # 0x4b
	.long	11                      # 0xb
	.long	55                      # 0x37
	.long	5                       # 0x5
	.long	64                      # 0x40
	.long	12                      # 0xc
	.long	70                      # 0x46
	.size	INIT_FLD_MAP_I, 960

	.type	INIT_FLD_MAP_P,@object  # @INIT_FLD_MAP_P
	.p2align	4
INIT_FLD_MAP_P:
	.long	4294967283              # 0xfffffff3
	.long	106                     # 0x6a
	.long	4294967280              # 0xfffffff0
	.long	106                     # 0x6a
	.long	4294967286              # 0xfffffff6
	.long	87                      # 0x57
	.long	4294967275              # 0xffffffeb
	.long	114                     # 0x72
	.long	4294967278              # 0xffffffee
	.long	110                     # 0x6e
	.long	4294967282              # 0xfffffff2
	.long	98                      # 0x62
	.long	4294967274              # 0xffffffea
	.long	110                     # 0x6e
	.long	4294967275              # 0xffffffeb
	.long	106                     # 0x6a
	.long	4294967278              # 0xffffffee
	.long	103                     # 0x67
	.long	4294967275              # 0xffffffeb
	.long	107                     # 0x6b
	.long	4294967273              # 0xffffffe9
	.long	108                     # 0x6c
	.long	4294967270              # 0xffffffe6
	.long	112                     # 0x70
	.long	4294967286              # 0xfffffff6
	.long	96                      # 0x60
	.long	4294967284              # 0xfffffff4
	.long	95                      # 0x5f
	.long	4294967291              # 0xfffffffb
	.long	91                      # 0x5b
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967287              # 0xfffffff7
	.long	93                      # 0x5d
	.long	4294967274              # 0xffffffea
	.long	94                      # 0x5e
	.long	4294967291              # 0xfffffffb
	.long	86                      # 0x56
	.long	9                       # 0x9
	.long	67                      # 0x43
	.long	4294967292              # 0xfffffffc
	.long	80                      # 0x50
	.long	4294967286              # 0xfffffff6
	.long	85                      # 0x55
	.long	4294967295              # 0xffffffff
	.long	70                      # 0x46
	.long	7                       # 0x7
	.long	60                      # 0x3c
	.long	9                       # 0x9
	.long	58                      # 0x3a
	.long	5                       # 0x5
	.long	61                      # 0x3d
	.long	12                      # 0xc
	.long	50                      # 0x32
	.long	15                      # 0xf
	.long	50                      # 0x32
	.long	18                      # 0x12
	.long	49                      # 0x31
	.long	17                      # 0x11
	.long	54                      # 0x36
	.long	4294967291              # 0xfffffffb
	.long	85                      # 0x55
	.long	4294967290              # 0xfffffffa
	.long	81                      # 0x51
	.long	4294967286              # 0xfffffff6
	.long	77                      # 0x4d
	.long	4294967289              # 0xfffffff9
	.long	81                      # 0x51
	.long	4294967279              # 0xffffffef
	.long	80                      # 0x50
	.long	4294967278              # 0xffffffee
	.long	73                      # 0x49
	.long	4294967292              # 0xfffffffc
	.long	74                      # 0x4a
	.long	4294967286              # 0xfffffff6
	.long	83                      # 0x53
	.long	4294967287              # 0xfffffff7
	.long	71                      # 0x47
	.long	4294967287              # 0xfffffff7
	.long	67                      # 0x43
	.long	4294967295              # 0xffffffff
	.long	61                      # 0x3d
	.long	4294967288              # 0xfffffff8
	.long	66                      # 0x42
	.long	4294967282              # 0xfffffff2
	.long	66                      # 0x42
	.long	0                       # 0x0
	.long	59                      # 0x3b
	.long	2                       # 0x2
	.long	59                      # 0x3b
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	10                      # 0xa
	.long	41                      # 0x29
	.long	7                       # 0x7
	.long	46                      # 0x2e
	.long	4294967295              # 0xffffffff
	.long	51                      # 0x33
	.long	7                       # 0x7
	.long	49                      # 0x31
	.long	8                       # 0x8
	.long	52                      # 0x34
	.long	9                       # 0x9
	.long	41                      # 0x29
	.long	6                       # 0x6
	.long	47                      # 0x2f
	.long	2                       # 0x2
	.long	55                      # 0x37
	.long	13                      # 0xd
	.long	41                      # 0x29
	.long	10                      # 0xa
	.long	44                      # 0x2c
	.long	6                       # 0x6
	.long	50                      # 0x32
	.long	5                       # 0x5
	.long	53                      # 0x35
	.long	13                      # 0xd
	.long	49                      # 0x31
	.long	4                       # 0x4
	.long	63                      # 0x3f
	.long	6                       # 0x6
	.long	64                      # 0x40
	.long	4294967294              # 0xfffffffe
	.long	69                      # 0x45
	.long	4294967294              # 0xfffffffe
	.long	59                      # 0x3b
	.long	6                       # 0x6
	.long	70                      # 0x46
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	10                      # 0xa
	.long	44                      # 0x2c
	.long	9                       # 0x9
	.long	31                      # 0x1f
	.long	12                      # 0xc
	.long	43                      # 0x2b
	.long	3                       # 0x3
	.long	53                      # 0x35
	.long	14                      # 0xe
	.long	34                      # 0x22
	.long	10                      # 0xa
	.long	38                      # 0x26
	.long	4294967293              # 0xfffffffd
	.long	52                      # 0x34
	.long	13                      # 0xd
	.long	40                      # 0x28
	.long	17                      # 0x11
	.long	32                      # 0x20
	.long	7                       # 0x7
	.long	44                      # 0x2c
	.long	7                       # 0x7
	.long	38                      # 0x26
	.long	13                      # 0xd
	.long	50                      # 0x32
	.long	10                      # 0xa
	.long	57                      # 0x39
	.long	26                      # 0x1a
	.long	43                      # 0x2b
	.long	4294967275              # 0xffffffeb
	.long	126                     # 0x7e
	.long	4294967273              # 0xffffffe9
	.long	124                     # 0x7c
	.long	4294967276              # 0xffffffec
	.long	110                     # 0x6e
	.long	4294967270              # 0xffffffe6
	.long	126                     # 0x7e
	.long	4294967271              # 0xffffffe7
	.long	124                     # 0x7c
	.long	4294967279              # 0xffffffef
	.long	105                     # 0x69
	.long	4294967269              # 0xffffffe5
	.long	121                     # 0x79
	.long	4294967269              # 0xffffffe5
	.long	117                     # 0x75
	.long	4294967279              # 0xffffffef
	.long	102                     # 0x66
	.long	4294967270              # 0xffffffe6
	.long	117                     # 0x75
	.long	4294967269              # 0xffffffe5
	.long	116                     # 0x74
	.long	4294967263              # 0xffffffdf
	.long	122                     # 0x7a
	.long	4294967286              # 0xfffffff6
	.long	95                      # 0x5f
	.long	4294967282              # 0xfffffff2
	.long	100                     # 0x64
	.long	4294967288              # 0xfffffff8
	.long	95                      # 0x5f
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967279              # 0xffffffef
	.long	111                     # 0x6f
	.long	4294967268              # 0xffffffe4
	.long	114                     # 0x72
	.long	4294967290              # 0xfffffffa
	.long	89                      # 0x59
	.long	4294967294              # 0xfffffffe
	.long	80                      # 0x50
	.long	4294967292              # 0xfffffffc
	.long	82                      # 0x52
	.long	4294967287              # 0xfffffff7
	.long	85                      # 0x55
	.long	4294967288              # 0xfffffff8
	.long	81                      # 0x51
	.long	4294967295              # 0xffffffff
	.long	72                      # 0x48
	.long	5                       # 0x5
	.long	64                      # 0x40
	.long	1                       # 0x1
	.long	67                      # 0x43
	.long	9                       # 0x9
	.long	56                      # 0x38
	.long	0                       # 0x0
	.long	69                      # 0x45
	.long	1                       # 0x1
	.long	69                      # 0x45
	.long	7                       # 0x7
	.long	69                      # 0x45
	.long	4294967293              # 0xfffffffd
	.long	81                      # 0x51
	.long	4294967293              # 0xfffffffd
	.long	76                      # 0x4c
	.long	4294967289              # 0xfffffff9
	.long	72                      # 0x48
	.long	4294967290              # 0xfffffffa
	.long	78                      # 0x4e
	.long	4294967284              # 0xfffffff4
	.long	72                      # 0x48
	.long	4294967282              # 0xfffffff2
	.long	68                      # 0x44
	.long	4294967293              # 0xfffffffd
	.long	70                      # 0x46
	.long	4294967290              # 0xfffffffa
	.long	76                      # 0x4c
	.long	4294967291              # 0xfffffffb
	.long	66                      # 0x42
	.long	4294967291              # 0xfffffffb
	.long	62                      # 0x3e
	.long	0                       # 0x0
	.long	57                      # 0x39
	.long	4294967292              # 0xfffffffc
	.long	61                      # 0x3d
	.long	4294967287              # 0xfffffff7
	.long	60                      # 0x3c
	.long	1                       # 0x1
	.long	54                      # 0x36
	.long	2                       # 0x2
	.long	58                      # 0x3a
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967289              # 0xfffffff9
	.long	69                      # 0x45
	.long	4294967290              # 0xfffffffa
	.long	67                      # 0x43
	.long	4294967280              # 0xfffffff0
	.long	77                      # 0x4d
	.long	4294967294              # 0xfffffffe
	.long	64                      # 0x40
	.long	2                       # 0x2
	.long	61                      # 0x3d
	.long	4294967290              # 0xfffffffa
	.long	67                      # 0x43
	.long	4294967293              # 0xfffffffd
	.long	64                      # 0x40
	.long	2                       # 0x2
	.long	57                      # 0x39
	.long	4294967293              # 0xfffffffd
	.long	65                      # 0x41
	.long	4294967293              # 0xfffffffd
	.long	66                      # 0x42
	.long	0                       # 0x0
	.long	62                      # 0x3e
	.long	9                       # 0x9
	.long	51                      # 0x33
	.long	4294967295              # 0xffffffff
	.long	66                      # 0x42
	.long	4294967294              # 0xfffffffe
	.long	71                      # 0x47
	.long	4294967294              # 0xfffffffe
	.long	75                      # 0x4b
	.long	4294967295              # 0xffffffff
	.long	70                      # 0x46
	.long	4294967287              # 0xfffffff7
	.long	72                      # 0x48
	.long	14                      # 0xe
	.long	60                      # 0x3c
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	16                      # 0x10
	.long	37                      # 0x25
	.long	0                       # 0x0
	.long	47                      # 0x2f
	.long	18                      # 0x12
	.long	35                      # 0x23
	.long	11                      # 0xb
	.long	37                      # 0x25
	.long	12                      # 0xc
	.long	41                      # 0x29
	.long	10                      # 0xa
	.long	41                      # 0x29
	.long	2                       # 0x2
	.long	48                      # 0x30
	.long	12                      # 0xc
	.long	41                      # 0x29
	.long	13                      # 0xd
	.long	41                      # 0x29
	.long	0                       # 0x0
	.long	59                      # 0x3b
	.long	3                       # 0x3
	.long	50                      # 0x32
	.long	19                      # 0x13
	.long	40                      # 0x28
	.long	3                       # 0x3
	.long	66                      # 0x42
	.long	18                      # 0x12
	.long	50                      # 0x32
	.long	4294967274              # 0xffffffea
	.long	127                     # 0x7f
	.long	4294967271              # 0xffffffe7
	.long	127                     # 0x7f
	.long	4294967271              # 0xffffffe7
	.long	120                     # 0x78
	.long	4294967269              # 0xffffffe5
	.long	127                     # 0x7f
	.long	4294967277              # 0xffffffed
	.long	114                     # 0x72
	.long	4294967273              # 0xffffffe9
	.long	117                     # 0x75
	.long	4294967271              # 0xffffffe7
	.long	118                     # 0x76
	.long	4294967270              # 0xffffffe6
	.long	117                     # 0x75
	.long	4294967272              # 0xffffffe8
	.long	113                     # 0x71
	.long	4294967268              # 0xffffffe4
	.long	118                     # 0x76
	.long	4294967265              # 0xffffffe1
	.long	120                     # 0x78
	.long	4294967259              # 0xffffffdb
	.long	124                     # 0x7c
	.long	4294967286              # 0xfffffff6
	.long	94                      # 0x5e
	.long	4294967281              # 0xfffffff1
	.long	102                     # 0x66
	.long	4294967286              # 0xfffffff6
	.long	99                      # 0x63
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	4294967283              # 0xfffffff3
	.long	106                     # 0x6a
	.long	4294967246              # 0xffffffce
	.long	127                     # 0x7f
	.long	4294967291              # 0xfffffffb
	.long	92                      # 0x5c
	.long	17                      # 0x11
	.long	57                      # 0x39
	.long	4294967291              # 0xfffffffb
	.long	86                      # 0x56
	.long	4294967283              # 0xfffffff3
	.long	94                      # 0x5e
	.long	4294967284              # 0xfffffff4
	.long	91                      # 0x5b
	.long	4294967294              # 0xfffffffe
	.long	77                      # 0x4d
	.long	0                       # 0x0
	.long	71                      # 0x47
	.long	4294967295              # 0xffffffff
	.long	73                      # 0x49
	.long	4                       # 0x4
	.long	64                      # 0x40
	.long	4294967289              # 0xfffffff9
	.long	81                      # 0x51
	.long	5                       # 0x5
	.long	64                      # 0x40
	.long	15                      # 0xf
	.long	57                      # 0x39
	.long	4294967293              # 0xfffffffd
	.long	78                      # 0x4e
	.long	4294967288              # 0xfffffff8
	.long	74                      # 0x4a
	.long	4294967287              # 0xfffffff7
	.long	72                      # 0x48
	.long	4294967286              # 0xfffffff6
	.long	72                      # 0x48
	.long	4294967278              # 0xffffffee
	.long	75                      # 0x4b
	.long	4294967284              # 0xfffffff4
	.long	71                      # 0x47
	.long	4294967285              # 0xfffffff5
	.long	63                      # 0x3f
	.long	4294967291              # 0xfffffffb
	.long	70                      # 0x46
	.long	4294967279              # 0xffffffef
	.long	75                      # 0x4b
	.long	4294967282              # 0xfffffff2
	.long	72                      # 0x48
	.long	4294967280              # 0xfffffff0
	.long	67                      # 0x43
	.long	4294967288              # 0xfffffff8
	.long	53                      # 0x35
	.long	4294967282              # 0xfffffff2
	.long	59                      # 0x3b
	.long	4294967287              # 0xfffffff7
	.long	52                      # 0x34
	.long	4294967285              # 0xfffffff5
	.long	68                      # 0x44
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	1                       # 0x1
	.long	67                      # 0x43
	.long	0                       # 0x0
	.long	68                      # 0x44
	.long	4294967286              # 0xfffffff6
	.long	67                      # 0x43
	.long	1                       # 0x1
	.long	68                      # 0x44
	.long	0                       # 0x0
	.long	77                      # 0x4d
	.long	2                       # 0x2
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	68                      # 0x44
	.long	4294967291              # 0xfffffffb
	.long	78                      # 0x4e
	.long	7                       # 0x7
	.long	55                      # 0x37
	.long	5                       # 0x5
	.long	59                      # 0x3b
	.long	2                       # 0x2
	.long	65                      # 0x41
	.long	14                      # 0xe
	.long	54                      # 0x36
	.long	15                      # 0xf
	.long	44                      # 0x2c
	.long	5                       # 0x5
	.long	60                      # 0x3c
	.long	2                       # 0x2
	.long	70                      # 0x46
	.long	4294967294              # 0xfffffffe
	.long	76                      # 0x4c
	.long	4294967278              # 0xffffffee
	.long	86                      # 0x56
	.long	12                      # 0xc
	.long	70                      # 0x46
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	5                       # 0x5
	.long	64                      # 0x40
	.long	4294967284              # 0xfffffff4
	.long	70                      # 0x46
	.long	11                      # 0xb
	.long	55                      # 0x37
	.long	5                       # 0x5
	.long	56                      # 0x38
	.long	0                       # 0x0
	.long	69                      # 0x45
	.long	2                       # 0x2
	.long	65                      # 0x41
	.long	4294967290              # 0xfffffffa
	.long	74                      # 0x4a
	.long	5                       # 0x5
	.long	54                      # 0x36
	.long	7                       # 0x7
	.long	54                      # 0x36
	.long	4294967290              # 0xfffffffa
	.long	76                      # 0x4c
	.long	4294967285              # 0xfffffff5
	.long	82                      # 0x52
	.long	4294967294              # 0xfffffffe
	.long	77                      # 0x4d
	.long	4294967294              # 0xfffffffe
	.long	77                      # 0x4d
	.long	25                      # 0x19
	.long	42                      # 0x2a
	.size	INIT_FLD_MAP_P, 2880

	.type	INIT_FLD_LAST_I,@object # @INIT_FLD_LAST_I
	.p2align	4
INIT_FLD_LAST_I:
	.long	15                      # 0xf
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	19                      # 0x13
	.long	7                       # 0x7
	.long	16                      # 0x10
	.long	12                      # 0xc
	.long	14                      # 0xe
	.long	18                      # 0x12
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	15                      # 0xf
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	12                      # 0xc
	.long	23                      # 0x17
	.long	13                      # 0xd
	.long	23                      # 0x17
	.long	15                      # 0xf
	.long	20                      # 0x14
	.long	14                      # 0xe
	.long	26                      # 0x1a
	.long	14                      # 0xe
	.long	44                      # 0x2c
	.long	17                      # 0x11
	.long	40                      # 0x28
	.long	17                      # 0x11
	.long	47                      # 0x2f
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	24                      # 0x18
	.long	17                      # 0x11
	.long	21                      # 0x15
	.long	21                      # 0x15
	.long	25                      # 0x19
	.long	22                      # 0x16
	.long	31                      # 0x1f
	.long	27                      # 0x1b
	.long	22                      # 0x16
	.long	29                      # 0x1d
	.long	19                      # 0x13
	.long	35                      # 0x23
	.long	14                      # 0xe
	.long	50                      # 0x32
	.long	10                      # 0xa
	.long	57                      # 0x39
	.long	7                       # 0x7
	.long	63                      # 0x3f
	.long	4294967294              # 0xfffffffe
	.long	77                      # 0x4d
	.long	4294967292              # 0xfffffffc
	.long	82                      # 0x52
	.long	4294967293              # 0xfffffffd
	.long	94                      # 0x5e
	.long	9                       # 0x9
	.long	69                      # 0x45
	.long	4294967284              # 0xfffffff4
	.long	109                     # 0x6d
	.long	21                      # 0x15
	.long	4294967286              # 0xfffffff6
	.long	24                      # 0x18
	.long	4294967285              # 0xfffffff5
	.long	28                      # 0x1c
	.long	4294967288              # 0xfffffff8
	.long	28                      # 0x1c
	.long	4294967295              # 0xffffffff
	.long	29                      # 0x1d
	.long	3                       # 0x3
	.long	29                      # 0x1d
	.long	9                       # 0x9
	.long	35                      # 0x23
	.long	20                      # 0x14
	.long	29                      # 0x1d
	.long	36                      # 0x24
	.long	14                      # 0xe
	.long	67                      # 0x43
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	36                      # 0x24
	.long	4294967261              # 0xffffffdd
	.long	36                      # 0x24
	.long	4294967262              # 0xffffffde
	.long	32                      # 0x20
	.long	4294967270              # 0xffffffe6
	.long	37                      # 0x25
	.long	4294967266              # 0xffffffe2
	.long	44                      # 0x2c
	.long	4294967264              # 0xffffffe0
	.long	34                      # 0x22
	.long	4294967278              # 0xffffffee
	.long	34                      # 0x22
	.long	4294967281              # 0xfffffff1
	.long	40                      # 0x28
	.long	4294967281              # 0xfffffff1
	.long	33                      # 0x21
	.long	4294967289              # 0xfffffff9
	.long	35                      # 0x23
	.long	4294967291              # 0xfffffffb
	.long	33                      # 0x21
	.long	0                       # 0x0
	.long	38                      # 0x26
	.long	2                       # 0x2
	.long	33                      # 0x21
	.long	13                      # 0xd
	.long	23                      # 0x17
	.long	35                      # 0x23
	.long	13                      # 0xd
	.long	58                      # 0x3a
	.long	29                      # 0x1d
	.long	4294967293              # 0xfffffffd
	.long	26                      # 0x1a
	.long	0                       # 0x0
	.long	22                      # 0x16
	.long	30                      # 0x1e
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	31                      # 0x1f
	.long	4294967289              # 0xfffffff9
	.long	35                      # 0x23
	.long	4294967281              # 0xfffffff1
	.long	34                      # 0x22
	.long	4294967293              # 0xfffffffd
	.long	34                      # 0x22
	.long	3                       # 0x3
	.long	36                      # 0x24
	.long	4294967295              # 0xffffffff
	.long	34                      # 0x22
	.long	5                       # 0x5
	.long	32                      # 0x20
	.long	11                      # 0xb
	.long	35                      # 0x23
	.long	5                       # 0x5
	.long	34                      # 0x22
	.long	12                      # 0xc
	.long	39                      # 0x27
	.long	11                      # 0xb
	.long	30                      # 0x1e
	.long	29                      # 0x1d
	.long	34                      # 0x22
	.long	26                      # 0x1a
	.long	29                      # 0x1d
	.long	39                      # 0x27
	.long	19                      # 0x13
	.long	66                      # 0x42
	.size	INIT_FLD_LAST_I, 960

	.type	INIT_FLD_LAST_P,@object # @INIT_FLD_LAST_P
	.p2align	4
INIT_FLD_LAST_P:
	.long	14                      # 0xe
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	14                      # 0xe
	.long	9                       # 0x9
	.long	11                      # 0xb
	.long	18                      # 0x12
	.long	11                      # 0xb
	.long	21                      # 0x15
	.long	9                       # 0x9
	.long	23                      # 0x17
	.long	4294967294              # 0xfffffffe
	.long	32                      # 0x20
	.long	4294967281              # 0xfffffff1
	.long	32                      # 0x20
	.long	4294967281              # 0xfffffff1
	.long	34                      # 0x22
	.long	4294967275              # 0xffffffeb
	.long	39                      # 0x27
	.long	4294967273              # 0xffffffe9
	.long	42                      # 0x2a
	.long	4294967263              # 0xffffffdf
	.long	41                      # 0x29
	.long	4294967265              # 0xffffffe1
	.long	46                      # 0x2e
	.long	4294967268              # 0xffffffe4
	.long	38                      # 0x26
	.long	4294967284              # 0xfffffff4
	.long	21                      # 0x15
	.long	29                      # 0x1d
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	45                      # 0x2d
	.long	4294967272              # 0xffffffe8
	.long	53                      # 0x35
	.long	4294967251              # 0xffffffd3
	.long	48                      # 0x30
	.long	4294967270              # 0xffffffe6
	.long	65                      # 0x41
	.long	4294967253              # 0xffffffd5
	.long	43                      # 0x2b
	.long	4294967277              # 0xffffffed
	.long	39                      # 0x27
	.long	4294967286              # 0xfffffff6
	.long	30                      # 0x1e
	.long	9                       # 0x9
	.long	18                      # 0x12
	.long	26                      # 0x1a
	.long	20                      # 0x14
	.long	27                      # 0x1b
	.long	0                       # 0x0
	.long	57                      # 0x39
	.long	4294967282              # 0xfffffff2
	.long	82                      # 0x52
	.long	4294967291              # 0xfffffffb
	.long	75                      # 0x4b
	.long	4294967277              # 0xffffffed
	.long	97                      # 0x61
	.long	4294967261              # 0xffffffdd
	.long	125                     # 0x7d
	.long	21                      # 0x15
	.long	4294967283              # 0xfffffff3
	.long	33                      # 0x21
	.long	4294967282              # 0xfffffff2
	.long	39                      # 0x27
	.long	4294967289              # 0xfffffff9
	.long	46                      # 0x2e
	.long	4294967294              # 0xfffffffe
	.long	51                      # 0x33
	.long	2                       # 0x2
	.long	60                      # 0x3c
	.long	6                       # 0x6
	.long	61                      # 0x3d
	.long	17                      # 0x11
	.long	55                      # 0x37
	.long	34                      # 0x22
	.long	42                      # 0x2a
	.long	62                      # 0x3e
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	27                      # 0x1b
	.long	0                       # 0x0
	.long	28                      # 0x1c
	.long	0                       # 0x0
	.long	31                      # 0x1f
	.long	4294967292              # 0xfffffffc
	.long	27                      # 0x1b
	.long	6                       # 0x6
	.long	34                      # 0x22
	.long	8                       # 0x8
	.long	30                      # 0x1e
	.long	10                      # 0xa
	.long	24                      # 0x18
	.long	22                      # 0x16
	.long	33                      # 0x21
	.long	19                      # 0x13
	.long	22                      # 0x16
	.long	32                      # 0x20
	.long	26                      # 0x1a
	.long	31                      # 0x1f
	.long	21                      # 0x15
	.long	41                      # 0x29
	.long	26                      # 0x1a
	.long	44                      # 0x2c
	.long	23                      # 0x17
	.long	47                      # 0x2f
	.long	16                      # 0x10
	.long	65                      # 0x41
	.long	14                      # 0xe
	.long	71                      # 0x47
	.long	8                       # 0x8
	.long	60                      # 0x3c
	.long	6                       # 0x6
	.long	63                      # 0x3f
	.long	17                      # 0x11
	.long	65                      # 0x41
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	21                      # 0x15
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	20                      # 0x14
	.long	26                      # 0x1a
	.long	23                      # 0x17
	.long	27                      # 0x1b
	.long	32                      # 0x20
	.long	28                      # 0x1c
	.long	23                      # 0x17
	.long	28                      # 0x1c
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	40                      # 0x28
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	28                      # 0x1c
	.long	29                      # 0x1d
	.long	23                      # 0x17
	.long	42                      # 0x2a
	.long	19                      # 0x13
	.long	57                      # 0x39
	.long	22                      # 0x16
	.long	53                      # 0x35
	.long	22                      # 0x16
	.long	61                      # 0x3d
	.long	11                      # 0xb
	.long	86                      # 0x56
	.long	19                      # 0x13
	.long	4294967290              # 0xfffffffa
	.long	18                      # 0x12
	.long	4294967290              # 0xfffffffa
	.long	14                      # 0xe
	.long	0                       # 0x0
	.long	26                      # 0x1a
	.long	4294967284              # 0xfffffff4
	.long	31                      # 0x1f
	.long	4294967280              # 0xfffffff0
	.long	33                      # 0x21
	.long	4294967271              # 0xffffffe7
	.long	33                      # 0x21
	.long	4294967274              # 0xffffffea
	.long	37                      # 0x25
	.long	4294967268              # 0xffffffe4
	.long	39                      # 0x27
	.long	4294967266              # 0xffffffe2
	.long	42                      # 0x2a
	.long	4294967266              # 0xffffffe2
	.long	47                      # 0x2f
	.long	4294967254              # 0xffffffd6
	.long	45                      # 0x2d
	.long	4294967260              # 0xffffffdc
	.long	49                      # 0x31
	.long	4294967262              # 0xffffffde
	.long	41                      # 0x29
	.long	4294967279              # 0xffffffef
	.long	32                      # 0x20
	.long	9                       # 0x9
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	69                      # 0x45
	.long	4294967225              # 0xffffffb9
	.long	63                      # 0x3f
	.long	4294967233              # 0xffffffc1
	.long	66                      # 0x42
	.long	4294967232              # 0xffffffc0
	.long	77                      # 0x4d
	.long	4294967222              # 0xffffffb6
	.long	54                      # 0x36
	.long	4294967257              # 0xffffffd9
	.long	52                      # 0x34
	.long	4294967261              # 0xffffffdd
	.long	41                      # 0x29
	.long	4294967286              # 0xfffffff6
	.long	36                      # 0x24
	.long	0                       # 0x0
	.long	40                      # 0x28
	.long	4294967295              # 0xffffffff
	.long	30                      # 0x1e
	.long	14                      # 0xe
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	23                      # 0x17
	.long	37                      # 0x25
	.long	12                      # 0xc
	.long	55                      # 0x37
	.long	11                      # 0xb
	.long	65                      # 0x41
	.long	17                      # 0x11
	.long	4294967286              # 0xfffffff6
	.long	32                      # 0x20
	.long	4294967283              # 0xfffffff3
	.long	42                      # 0x2a
	.long	4294967287              # 0xfffffff7
	.long	49                      # 0x31
	.long	4294967291              # 0xfffffffb
	.long	53                      # 0x35
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	3                       # 0x3
	.long	68                      # 0x44
	.long	10                      # 0xa
	.long	66                      # 0x42
	.long	27                      # 0x1b
	.long	47                      # 0x2f
	.long	57                      # 0x39
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	37                      # 0x25
	.long	4294967263              # 0xffffffdf
	.long	39                      # 0x27
	.long	4294967260              # 0xffffffdc
	.long	40                      # 0x28
	.long	4294967259              # 0xffffffdb
	.long	38                      # 0x26
	.long	4294967266              # 0xffffffe2
	.long	46                      # 0x2e
	.long	4294967263              # 0xffffffdf
	.long	42                      # 0x2a
	.long	4294967266              # 0xffffffe2
	.long	40                      # 0x28
	.long	4294967272              # 0xffffffe8
	.long	49                      # 0x31
	.long	4294967267              # 0xffffffe3
	.long	38                      # 0x26
	.long	4294967284              # 0xfffffff4
	.long	40                      # 0x28
	.long	4294967286              # 0xfffffff6
	.long	38                      # 0x26
	.long	4294967293              # 0xfffffffd
	.long	46                      # 0x2e
	.long	4294967291              # 0xfffffffb
	.long	31                      # 0x1f
	.long	20                      # 0x14
	.long	29                      # 0x1d
	.long	30                      # 0x1e
	.long	25                      # 0x19
	.long	44                      # 0x2c
	.long	12                      # 0xc
	.long	48                      # 0x30
	.long	11                      # 0xb
	.long	49                      # 0x31
	.long	26                      # 0x1a
	.long	45                      # 0x2d
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	22                      # 0x16
	.long	22                      # 0x16
	.long	23                      # 0x17
	.long	22                      # 0x16
	.long	27                      # 0x1b
	.long	21                      # 0x15
	.long	33                      # 0x21
	.long	20                      # 0x14
	.long	26                      # 0x1a
	.long	28                      # 0x1c
	.long	30                      # 0x1e
	.long	24                      # 0x18
	.long	27                      # 0x1b
	.long	34                      # 0x22
	.long	18                      # 0x12
	.long	42                      # 0x2a
	.long	25                      # 0x19
	.long	39                      # 0x27
	.long	18                      # 0x12
	.long	50                      # 0x32
	.long	12                      # 0xc
	.long	70                      # 0x46
	.long	21                      # 0x15
	.long	54                      # 0x36
	.long	14                      # 0xe
	.long	71                      # 0x47
	.long	11                      # 0xb
	.long	83                      # 0x53
	.long	17                      # 0x11
	.long	4294967283              # 0xfffffff3
	.long	16                      # 0x10
	.long	4294967287              # 0xfffffff7
	.long	17                      # 0x11
	.long	4294967284              # 0xfffffff4
	.long	27                      # 0x1b
	.long	4294967275              # 0xffffffeb
	.long	37                      # 0x25
	.long	4294967266              # 0xffffffe2
	.long	41                      # 0x29
	.long	4294967256              # 0xffffffd8
	.long	42                      # 0x2a
	.long	4294967255              # 0xffffffd7
	.long	48                      # 0x30
	.long	4294967249              # 0xffffffd1
	.long	39                      # 0x27
	.long	4294967264              # 0xffffffe0
	.long	46                      # 0x2e
	.long	4294967256              # 0xffffffd8
	.long	52                      # 0x34
	.long	4294967245              # 0xffffffcd
	.long	46                      # 0x2e
	.long	4294967255              # 0xffffffd7
	.long	52                      # 0x34
	.long	4294967257              # 0xffffffd9
	.long	43                      # 0x2b
	.long	4294967277              # 0xffffffed
	.long	32                      # 0x20
	.long	11                      # 0xb
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	61                      # 0x3d
	.long	4294967241              # 0xffffffc9
	.long	56                      # 0x38
	.long	4294967250              # 0xffffffd2
	.long	62                      # 0x3e
	.long	4294967246              # 0xffffffce
	.long	81                      # 0x51
	.long	4294967229              # 0xffffffbd
	.long	45                      # 0x2d
	.long	4294967276              # 0xffffffec
	.long	35                      # 0x23
	.long	4294967294              # 0xfffffffe
	.long	28                      # 0x1c
	.long	15                      # 0xf
	.long	34                      # 0x22
	.long	1                       # 0x1
	.long	39                      # 0x27
	.long	1                       # 0x1
	.long	30                      # 0x1e
	.long	17                      # 0x11
	.long	20                      # 0x14
	.long	38                      # 0x26
	.long	18                      # 0x12
	.long	45                      # 0x2d
	.long	15                      # 0xf
	.long	54                      # 0x36
	.long	0                       # 0x0
	.long	79                      # 0x4f
	.long	9                       # 0x9
	.long	4294967294              # 0xfffffffe
	.long	30                      # 0x1e
	.long	4294967286              # 0xfffffff6
	.long	31                      # 0x1f
	.long	4294967292              # 0xfffffffc
	.long	33                      # 0x21
	.long	4294967295              # 0xffffffff
	.long	33                      # 0x21
	.long	7                       # 0x7
	.long	31                      # 0x1f
	.long	12                      # 0xc
	.long	37                      # 0x25
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	38                      # 0x26
	.long	20                      # 0x14
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	36                      # 0x24
	.long	4294967280              # 0xfffffff0
	.long	37                      # 0x25
	.long	4294967282              # 0xfffffff2
	.long	37                      # 0x25
	.long	4294967279              # 0xffffffef
	.long	32                      # 0x20
	.long	1                       # 0x1
	.long	34                      # 0x22
	.long	15                      # 0xf
	.long	29                      # 0x1d
	.long	15                      # 0xf
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	34                      # 0x22
	.long	22                      # 0x16
	.long	31                      # 0x1f
	.long	16                      # 0x10
	.long	35                      # 0x23
	.long	18                      # 0x12
	.long	31                      # 0x1f
	.long	28                      # 0x1c
	.long	33                      # 0x21
	.long	41                      # 0x29
	.long	36                      # 0x24
	.long	28                      # 0x1c
	.long	27                      # 0x1b
	.long	47                      # 0x2f
	.long	21                      # 0x15
	.long	62                      # 0x3e
	.long	18                      # 0x12
	.long	31                      # 0x1f
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	36                      # 0x24
	.long	24                      # 0x18
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	0                       # 0x0
	.long	64                      # 0x40
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	27                      # 0x1b
	.long	16                      # 0x10
	.long	24                      # 0x18
	.long	30                      # 0x1e
	.long	31                      # 0x1f
	.long	29                      # 0x1d
	.long	22                      # 0x16
	.long	41                      # 0x29
	.long	22                      # 0x16
	.long	42                      # 0x2a
	.long	16                      # 0x10
	.long	60                      # 0x3c
	.long	15                      # 0xf
	.long	52                      # 0x34
	.long	14                      # 0xe
	.long	60                      # 0x3c
	.long	3                       # 0x3
	.long	78                      # 0x4e
	.long	4294967280              # 0xfffffff0
	.long	123                     # 0x7b
	.long	21                      # 0x15
	.long	53                      # 0x35
	.long	22                      # 0x16
	.long	56                      # 0x38
	.long	25                      # 0x19
	.long	61                      # 0x3d
	.size	INIT_FLD_LAST_P, 2880

	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
