	.text
	.file	"btCollisionDispatcher.bc"
	.globl	_ZN21btCollisionDispatcherC2EP24btCollisionConfiguration
	.p2align	4, 0x90
	.type	_ZN21btCollisionDispatcherC2EP24btCollisionConfiguration,@function
_ZN21btCollisionDispatcherC2EP24btCollisionConfiguration: # @_ZN21btCollisionDispatcherC2EP24btCollisionConfiguration
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	$_ZTV21btCollisionDispatcher+16, (%r14)
	movl	$0, 8(%r14)
	movb	$1, 40(%r14)
	movq	$0, 32(%r14)
	movl	$0, 20(%r14)
	movl	$0, 24(%r14)
	movb	$1, 48(%r14)
	movb	$0, 49(%r14)
	movq	$_ZTV16btManifoldResult+16, 56(%r14)
	movq	%rbx, 10624(%r14)
	movq	$_ZN21btCollisionDispatcher19defaultNearCallbackER16btBroadphasePairRS_RK16btDispatcherInfo, 232(%r14)
	movq	(%rbx), %rax
.Ltmp0:
	movq	%rbx, %rdi
	callq	*24(%rax)
.Ltmp1:
# BB#1:
	movq	%rax, 240(%r14)
	movq	(%rbx), %rax
.Ltmp2:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp3:
# BB#2:
	movq	%rax, 248(%r14)
	leaq	256(%r14), %r12
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
	movq	%r12, %r13
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_4:                                #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	10624(%r14), %rdi
	movq	(%rdi), %rax
.Ltmp5:
	movl	%r15d, %esi
	movl	%ebx, %edx
	callq	*40(%rax)
.Ltmp6:
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=2
	movq	%rax, (%r13)
	incq	%rbx
	addq	$8, %r13
	cmpq	$36, %rbx
	jl	.LBB0_4
# BB#6:                                 #   in Loop: Header=BB0_3 Depth=1
	incq	%r15
	addq	$288, %r12              # imm = 0x120
	cmpq	$36, %r15
	jl	.LBB0_3
# BB#7:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB0_8:
.Ltmp4:
	jmp	.LBB0_9
.LBB0_16:
.Ltmp7:
.LBB0_9:
	movq	%rax, %rbx
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_13
# BB#10:
	cmpb	$0, 40(%r14)
	je	.LBB0_12
# BB#11:
.Ltmp8:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp9:
.LBB0_12:                               # %.noexc
	movq	$0, 32(%r14)
.LBB0_13:
	movb	$1, 40(%r14)
	movq	$0, 32(%r14)
	movq	$0, 20(%r14)
.Ltmp10:
	movq	%r14, %rdi
	callq	_ZN12btDispatcherD2Ev
.Ltmp11:
# BB#14:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_15:
.Ltmp12:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN21btCollisionDispatcherC2EP24btCollisionConfiguration, .Lfunc_end0-_ZN21btCollisionDispatcherC2EP24btCollisionConfiguration
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp11-.Ltmp8          #   Call between .Ltmp8 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	1                       #   On action: 1
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp11     #   Call between .Ltmp11 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN21btCollisionDispatcher19defaultNearCallbackER16btBroadphasePairRS_RK16btDispatcherInfo
	.p2align	4, 0x90
	.type	_ZN21btCollisionDispatcher19defaultNearCallbackER16btBroadphasePairRS_RK16btDispatcherInfo,@function
_ZN21btCollisionDispatcher19defaultNearCallbackER16btBroadphasePairRS_RK16btDispatcherInfo: # @_ZN21btCollisionDispatcher19defaultNearCallbackER16btBroadphasePairRS_RK16btDispatcherInfo
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 48
	subq	$176, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 224
.Lcfi16:
	.cfi_offset %rbx, -48
.Lcfi17:
	.cfi_offset %r12, -40
.Lcfi18:
	.cfi_offset %r13, -32
.Lcfi19:
	.cfi_offset %r14, -24
.Lcfi20:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movq	(%r13), %rax
	movq	8(%r13), %rcx
	movq	(%rax), %r12
	movq	(%rcx), %r15
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	*48(%rax)
	testb	%al, %al
	je	.LBB1_9
# BB#1:
	cmpq	$0, 16(%r13)
	jne	.LBB1_3
# BB#2:
	movq	(%rbx), %rax
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	*16(%rax)
	movq	%rax, 16(%r13)
	testq	%rax, %rax
	je	.LBB1_9
.LBB1_3:                                # %.thread
	movq	%rsp, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	_ZN16btManifoldResultC1EP17btCollisionObjectS1_
	movq	16(%r13), %rdi
	cmpl	$1, 8(%r14)
	jne	.LBB1_6
# BB#4:
	movq	(%rdi), %rax
.Ltmp15:
	movq	%rsp, %r8
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	*16(%rax)
.Ltmp16:
	jmp	.LBB1_9
.LBB1_6:
	movq	(%rdi), %rax
.Ltmp13:
	movq	%rsp, %r8
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	*24(%rax)
.Ltmp14:
# BB#7:
	movss	12(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB1_9
# BB#8:
	movss	%xmm0, 12(%r14)
.LBB1_9:
	addq	$176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB1_5:
.Ltmp17:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZN21btCollisionDispatcher19defaultNearCallbackER16btBroadphasePairRS_RK16btDispatcherInfo, .Lfunc_end1-_ZN21btCollisionDispatcher19defaultNearCallbackER16btBroadphasePairRS_RK16btDispatcherInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp15-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp14-.Ltmp15         #   Call between .Ltmp15 and .Ltmp14
	.long	.Ltmp17-.Lfunc_begin1   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Lfunc_end1-.Ltmp14     #   Call between .Ltmp14 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN21btCollisionDispatcher27registerCollisionCreateFuncEiiP30btCollisionAlgorithmCreateFunc
	.p2align	4, 0x90
	.type	_ZN21btCollisionDispatcher27registerCollisionCreateFuncEiiP30btCollisionAlgorithmCreateFunc,@function
_ZN21btCollisionDispatcher27registerCollisionCreateFuncEiiP30btCollisionAlgorithmCreateFunc: # @_ZN21btCollisionDispatcher27registerCollisionCreateFuncEiiP30btCollisionAlgorithmCreateFunc
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	movslq	%edx, %rdx
	leaq	(%rax,%rax,8), %rax
	shlq	$5, %rax
	addq	%rdi, %rax
	movq	%rcx, 256(%rax,%rdx,8)
	retq
.Lfunc_end3:
	.size	_ZN21btCollisionDispatcher27registerCollisionCreateFuncEiiP30btCollisionAlgorithmCreateFunc, .Lfunc_end3-_ZN21btCollisionDispatcher27registerCollisionCreateFuncEiiP30btCollisionAlgorithmCreateFunc
	.cfi_endproc

	.globl	_ZN21btCollisionDispatcherD2Ev
	.p2align	4, 0x90
	.type	_ZN21btCollisionDispatcherD2Ev,@function
_ZN21btCollisionDispatcherD2Ev:         # @_ZN21btCollisionDispatcherD2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -24
.Lcfi25:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV21btCollisionDispatcher+16, (%rbx)
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_4
# BB#1:
	cmpb	$0, 40(%rbx)
	je	.LBB4_3
# BB#2:
.Ltmp18:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp19:
.LBB4_3:                                # %.noexc
	movq	$0, 32(%rbx)
.LBB4_4:
	movb	$1, 40(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 20(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN12btDispatcherD2Ev   # TAILCALL
.LBB4_5:
.Ltmp20:
	movq	%rax, %r14
.Ltmp21:
	movq	%rbx, %rdi
	callq	_ZN12btDispatcherD2Ev
.Ltmp22:
# BB#6:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_7:
.Ltmp23:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN21btCollisionDispatcherD2Ev, .Lfunc_end4-_ZN21btCollisionDispatcherD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp18-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin2   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp21-.Ltmp19         #   Call between .Ltmp19 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin2   #     jumps to .Ltmp23
	.byte	1                       #   On action: 1
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end4-.Ltmp22     #   Call between .Ltmp22 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN21btCollisionDispatcherD0Ev
	.p2align	4, 0x90
	.type	_ZN21btCollisionDispatcherD0Ev,@function
_ZN21btCollisionDispatcherD0Ev:         # @_ZN21btCollisionDispatcherD0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV21btCollisionDispatcher+16, (%rbx)
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_4
# BB#1:
	cmpb	$0, 40(%rbx)
	je	.LBB5_3
# BB#2:
.Ltmp24:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp25:
.LBB5_3:                                # %.noexc.i
	movq	$0, 32(%rbx)
.LBB5_4:
	movb	$1, 40(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 20(%rbx)
.Ltmp30:
	movq	%rbx, %rdi
	callq	_ZN12btDispatcherD2Ev
.Ltmp31:
# BB#5:                                 # %_ZN21btCollisionDispatcherD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB5_6:
.Ltmp26:
	movq	%rax, %r14
.Ltmp27:
	movq	%rbx, %rdi
	callq	_ZN12btDispatcherD2Ev
.Ltmp28:
	jmp	.LBB5_9
.LBB5_7:
.Ltmp29:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB5_8:
.Ltmp32:
	movq	%rax, %r14
.LBB5_9:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZN21btCollisionDispatcherD0Ev, .Lfunc_end5-_ZN21btCollisionDispatcherD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp24-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin3   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin3   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin3   #     jumps to .Ltmp29
	.byte	1                       #   On action: 1
	.long	.Ltmp28-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp28     #   Call between .Ltmp28 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN21btCollisionDispatcher14getNewManifoldEPvS0_
	.p2align	4, 0x90
	.type	_ZN21btCollisionDispatcher14getNewManifoldEPvS0_,@function
_ZN21btCollisionDispatcher14getNewManifoldEPvS0_: # @_ZN21btCollisionDispatcher14getNewManifoldEPvS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 64
.Lcfi37:
	.cfi_offset %rbx, -48
.Lcfi38:
	.cfi_offset %r12, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r14
	incl	gNumManifold(%rip)
	movq	200(%r12), %rdi
	movq	(%rdi), %rax
	callq	*40(%rax)
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movq	200(%r15), %rdi
	movq	(%rdi), %rax
	callq	*40(%rax)
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	minss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	movss	188(%r12), %xmm1        # xmm1 = mem[0],zero,zero,zero
	minss	188(%r15), %xmm1
	movq	248(%r14), %rax
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB6_2
# BB#1:
	movq	16(%rax), %rbx
	movq	(%rbx), %rdx
	movq	%rdx, 16(%rax)
	decl	%ecx
	movl	%ecx, 8(%rax)
	jmp	.LBB6_3
.LBB6_2:
	movl	$744, %edi              # imm = 0x2E8
	movl	$16, %esi
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	callq	_Z22btAlignedAllocInternalmi
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movq	%rax, %rbx
.LBB6_3:
	movl	$1, (%rbx)
	movq	$0, 120(%rbx)
	movl	$0, 128(%rbx)
	movb	$0, 132(%rbx)
	movl	$0, 136(%rbx)
	movl	$0, 140(%rbx)
	movl	$0, 144(%rbx)
	movq	$0, 296(%rbx)
	movl	$0, 304(%rbx)
	movb	$0, 308(%rbx)
	movl	$0, 312(%rbx)
	movl	$0, 316(%rbx)
	movl	$0, 320(%rbx)
	movq	$0, 472(%rbx)
	movl	$0, 480(%rbx)
	movb	$0, 484(%rbx)
	movl	$0, 488(%rbx)
	movl	$0, 492(%rbx)
	movl	$0, 496(%rbx)
	movq	$0, 648(%rbx)
	movl	$0, 656(%rbx)
	movb	$0, 660(%rbx)
	movl	$0, 664(%rbx)
	movl	$0, 668(%rbx)
	movl	$0, 672(%rbx)
	movq	%r12, 712(%rbx)
	movq	%r15, 720(%rbx)
	movl	$0, 728(%rbx)
	movss	%xmm0, 732(%rbx)
	movss	%xmm1, 736(%rbx)
	movl	20(%r14), %eax
	movl	%eax, 740(%rbx)
	cmpl	24(%r14), %eax
	jne	.LBB6_18
# BB#4:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%ecx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB6_18
# BB#5:
	testl	%ebp, %ebp
	je	.LBB6_6
# BB#7:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	20(%r14), %eax
	testl	%eax, %eax
	jg	.LBB6_9
	jmp	.LBB6_13
.LBB6_6:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.LBB6_13
.LBB6_9:                                # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB6_11
	.p2align	4, 0x90
.LBB6_10:                               # =>This Inner Loop Header: Depth=1
	movq	32(%r14), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB6_10
.LBB6_11:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB6_13
	.p2align	4, 0x90
.LBB6_12:                               # =>This Inner Loop Header: Depth=1
	movq	32(%r14), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	movq	32(%r14), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%r15,%rdx,8)
	movq	32(%r14), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%r15,%rdx,8)
	movq	32(%r14), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%r15,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB6_12
.LBB6_13:                               # %_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_.exit.i.i
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB6_17
# BB#14:
	cmpb	$0, 40(%r14)
	je	.LBB6_16
# BB#15:
	callq	_Z21btAlignedFreeInternalPv
	movl	20(%r14), %eax
.LBB6_16:
	movq	$0, 32(%r14)
.LBB6_17:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv.exit.i.i
	movb	$1, 40(%r14)
	movq	%r15, 32(%r14)
	movl	%ebp, 24(%r14)
.LBB6_18:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_.exit
	movq	32(%r14), %rcx
	cltq
	movq	%rbx, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, 20(%r14)
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN21btCollisionDispatcher14getNewManifoldEPvS0_, .Lfunc_end6-_ZN21btCollisionDispatcher14getNewManifoldEPvS0_
	.cfi_endproc

	.globl	_ZN21btCollisionDispatcher13clearManifoldEP20btPersistentManifold
	.p2align	4, 0x90
	.type	_ZN21btCollisionDispatcher13clearManifoldEP20btPersistentManifold,@function
_ZN21btCollisionDispatcher13clearManifoldEP20btPersistentManifold: # @_ZN21btCollisionDispatcher13clearManifoldEP20btPersistentManifold
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 32
.Lcfi45:
	.cfi_offset %rbx, -32
.Lcfi46:
	.cfi_offset %r14, -24
.Lcfi47:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	cmpl	$0, 728(%r14)
	jle	.LBB7_3
# BB#1:                                 # %.lr.ph.i.preheader
	leaq	8(%r14), %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_ZN20btPersistentManifold14clearUserCacheER15btManifoldPoint
	incq	%rbx
	movslq	728(%r14), %rax
	addq	$176, %r15
	cmpq	%rax, %rbx
	jl	.LBB7_2
.LBB7_3:                                # %_ZN20btPersistentManifold13clearManifoldEv.exit
	movl	$0, 728(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	_ZN21btCollisionDispatcher13clearManifoldEP20btPersistentManifold, .Lfunc_end7-_ZN21btCollisionDispatcher13clearManifoldEP20btPersistentManifold
	.cfi_endproc

	.globl	_ZN21btCollisionDispatcher15releaseManifoldEP20btPersistentManifold
	.p2align	4, 0x90
	.type	_ZN21btCollisionDispatcher15releaseManifoldEP20btPersistentManifold,@function
_ZN21btCollisionDispatcher15releaseManifoldEP20btPersistentManifold: # @_ZN21btCollisionDispatcher15releaseManifoldEP20btPersistentManifold
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 32
.Lcfi51:
	.cfi_offset %rbx, -24
.Lcfi52:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	decl	gNumManifold(%rip)
	movq	(%rbx), %rax
	callq	*40(%rax)
	movslq	740(%r14), %rax
	movslq	20(%rbx), %rcx
	leaq	-1(%rcx), %r8
	movq	32(%rbx), %rsi
	movq	(%rsi,%rax,8), %rdi
	movq	-8(%rsi,%rcx,8), %rdx
	movq	%rdx, (%rsi,%rax,8)
	movq	32(%rbx), %rdx
	movq	%rdi, -8(%rdx,%rcx,8)
	movq	32(%rbx), %rcx
	movq	(%rcx,%rax,8), %rcx
	movl	%eax, 740(%rcx)
	movl	%r8d, 20(%rbx)
	testq	%r14, %r14
	je	.LBB8_4
# BB#1:
	movq	248(%rbx), %rax
	movq	24(%rax), %rcx
	cmpq	%r14, %rcx
	ja	.LBB8_4
# BB#2:
	movslq	(%rax), %rdx
	movslq	4(%rax), %rsi
	imulq	%rdx, %rsi
	addq	%rsi, %rcx
	cmpq	%r14, %rcx
	jbe	.LBB8_4
# BB#3:
	movq	16(%rax), %rcx
	movq	%rcx, (%r14)
	movq	%r14, 16(%rax)
	incl	8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB8_4:                                # %_ZN15btPoolAllocator8validPtrEPv.exit
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.Lfunc_end8:
	.size	_ZN21btCollisionDispatcher15releaseManifoldEP20btPersistentManifold, .Lfunc_end8-_ZN21btCollisionDispatcher15releaseManifoldEP20btPersistentManifold
	.cfi_endproc

	.globl	_ZN21btCollisionDispatcher13findAlgorithmEP17btCollisionObjectS1_P20btPersistentManifold
	.p2align	4, 0x90
	.type	_ZN21btCollisionDispatcher13findAlgorithmEP17btCollisionObjectS1_P20btPersistentManifold,@function
_ZN21btCollisionDispatcher13findAlgorithmEP17btCollisionObjectS1_P20btPersistentManifold: # @_ZN21btCollisionDispatcher13findAlgorithmEP17btCollisionObjectS1_P20btPersistentManifold
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi53:
	.cfi_def_cfa_offset 32
	movq	%rdx, %rax
	movq	%rsi, %rdx
	movq	%rdi, 8(%rsp)
	movq	%rcx, 16(%rsp)
	movq	200(%rdx), %rcx
	movslq	8(%rcx), %rcx
	movq	200(%rax), %rsi
	movslq	8(%rsi), %rsi
	leaq	(%rcx,%rcx,8), %rcx
	shlq	$5, %rcx
	addq	%rdi, %rcx
	movq	256(%rcx,%rsi,8), %rdi
	movq	(%rdi), %r8
	leaq	8(%rsp), %rsi
	movq	%rax, %rcx
	callq	*16(%r8)
	addq	$24, %rsp
	retq
.Lfunc_end9:
	.size	_ZN21btCollisionDispatcher13findAlgorithmEP17btCollisionObjectS1_P20btPersistentManifold, .Lfunc_end9-_ZN21btCollisionDispatcher13findAlgorithmEP17btCollisionObjectS1_P20btPersistentManifold
	.cfi_endproc

	.globl	_ZN21btCollisionDispatcher13needsResponseEP17btCollisionObjectS1_
	.p2align	4, 0x90
	.type	_ZN21btCollisionDispatcher13needsResponseEP17btCollisionObjectS1_,@function
_ZN21btCollisionDispatcher13needsResponseEP17btCollisionObjectS1_: # @_ZN21btCollisionDispatcher13needsResponseEP17btCollisionObjectS1_
	.cfi_startproc
# BB#0:
	movl	216(%rsi), %eax
	testb	$4, %al
	jne	.LBB10_1
# BB#2:
	movl	216(%rdx), %ecx
	testb	$4, %cl
	jne	.LBB10_3
# BB#4:
	testb	$3, %al
	je	.LBB10_5
# BB#6:
	testb	$3, %cl
	sete	%al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB10_1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB10_3:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB10_5:
	movb	$1, %al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end10:
	.size	_ZN21btCollisionDispatcher13needsResponseEP17btCollisionObjectS1_, .Lfunc_end10-_ZN21btCollisionDispatcher13needsResponseEP17btCollisionObjectS1_
	.cfi_endproc

	.globl	_ZN21btCollisionDispatcher14needsCollisionEP17btCollisionObjectS1_
	.p2align	4, 0x90
	.type	_ZN21btCollisionDispatcher14needsCollisionEP17btCollisionObjectS1_,@function
_ZN21btCollisionDispatcher14needsCollisionEP17btCollisionObjectS1_: # @_ZN21btCollisionDispatcher14needsCollisionEP17btCollisionObjectS1_
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 16
	movl	228(%rsi), %eax
	cmpl	$5, %eax
	je	.LBB11_2
# BB#1:
	cmpl	$2, %eax
	jne	.LBB11_4
.LBB11_2:
	movl	228(%rdx), %ecx
	xorl	%eax, %eax
	cmpl	$2, %ecx
	je	.LBB11_8
# BB#3:
	cmpl	$5, %ecx
	je	.LBB11_8
.LBB11_4:
	cmpb	$0, 272(%rsi)
	je	.LBB11_7
# BB#5:                                 # %_ZN17btCollisionObject16checkCollideWithEPS_.exit
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	callq	*(%rax)
	testb	%al, %al
	je	.LBB11_6
.LBB11_7:                               # %_ZN17btCollisionObject16checkCollideWithEPS_.exit.thread
	movb	$1, %al
	jmp	.LBB11_8
.LBB11_6:
	xorl	%eax, %eax
.LBB11_8:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rcx
	retq
.Lfunc_end11:
	.size	_ZN21btCollisionDispatcher14needsCollisionEP17btCollisionObjectS1_, .Lfunc_end11-_ZN21btCollisionDispatcher14needsCollisionEP17btCollisionObjectS1_
	.cfi_endproc

	.globl	_ZN21btCollisionDispatcher25dispatchAllCollisionPairsEP22btOverlappingPairCacheRK16btDispatcherInfoP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN21btCollisionDispatcher25dispatchAllCollisionPairsEP22btOverlappingPairCacheRK16btDispatcherInfoP12btDispatcher,@function
_ZN21btCollisionDispatcher25dispatchAllCollisionPairsEP22btOverlappingPairCacheRK16btDispatcherInfoP12btDispatcher: # @_ZN21btCollisionDispatcher25dispatchAllCollisionPairsEP22btOverlappingPairCacheRK16btDispatcherInfoP12btDispatcher
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	subq	$24, %rsp
.Lcfi55:
	.cfi_def_cfa_offset 32
	movq	$_ZTV23btCollisionPairCallback+16, (%rsp)
	movq	%rdx, 8(%rsp)
	movq	%rdi, 16(%rsp)
	movq	(%rsi), %rax
.Ltmp33:
	movq	%rsp, %rdx
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	callq	*96(%rax)
.Ltmp34:
# BB#1:
	addq	$24, %rsp
	retq
.LBB12_2:
.Ltmp35:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN21btCollisionDispatcher25dispatchAllCollisionPairsEP22btOverlappingPairCacheRK16btDispatcherInfoP12btDispatcher, .Lfunc_end12-_ZN21btCollisionDispatcher25dispatchAllCollisionPairsEP22btOverlappingPairCacheRK16btDispatcherInfoP12btDispatcher
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp33-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin4   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end12-.Ltmp34    #   Call between .Ltmp34 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN21btCollisionDispatcher26allocateCollisionAlgorithmEi
	.p2align	4, 0x90
	.type	_ZN21btCollisionDispatcher26allocateCollisionAlgorithmEi,@function
_ZN21btCollisionDispatcher26allocateCollisionAlgorithmEi: # @_ZN21btCollisionDispatcher26allocateCollisionAlgorithmEi
	.cfi_startproc
# BB#0:
	movq	240(%rdi), %rcx
	movl	8(%rcx), %edx
	testl	%edx, %edx
	je	.LBB13_2
# BB#1:
	movq	16(%rcx), %rax
	movq	(%rax), %rsi
	movq	%rsi, 16(%rcx)
	decl	%edx
	movl	%edx, 8(%rcx)
	retq
.LBB13_2:
	movslq	%esi, %rdi
	movl	$16, %esi
	jmp	_Z22btAlignedAllocInternalmi # TAILCALL
.Lfunc_end13:
	.size	_ZN21btCollisionDispatcher26allocateCollisionAlgorithmEi, .Lfunc_end13-_ZN21btCollisionDispatcher26allocateCollisionAlgorithmEi
	.cfi_endproc

	.globl	_ZN21btCollisionDispatcher22freeCollisionAlgorithmEPv
	.p2align	4, 0x90
	.type	_ZN21btCollisionDispatcher22freeCollisionAlgorithmEPv,@function
_ZN21btCollisionDispatcher22freeCollisionAlgorithmEPv: # @_ZN21btCollisionDispatcher22freeCollisionAlgorithmEPv
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB14_4
# BB#1:
	movq	240(%rdi), %rax
	movq	24(%rax), %rcx
	cmpq	%rsi, %rcx
	ja	.LBB14_4
# BB#2:
	movslq	(%rax), %rdx
	movslq	4(%rax), %rdi
	imulq	%rdx, %rdi
	addq	%rdi, %rcx
	cmpq	%rsi, %rcx
	jbe	.LBB14_4
# BB#3:
	movq	16(%rax), %rcx
	movq	%rcx, (%rsi)
	movq	%rsi, 16(%rax)
	incl	8(%rax)
	retq
.LBB14_4:                               # %_ZN15btPoolAllocator8validPtrEPv.exit
	movq	%rsi, %rdi
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.Lfunc_end14:
	.size	_ZN21btCollisionDispatcher22freeCollisionAlgorithmEPv, .Lfunc_end14-_ZN21btCollisionDispatcher22freeCollisionAlgorithmEPv
	.cfi_endproc

	.section	.text._ZNK21btCollisionDispatcher15getNumManifoldsEv,"axG",@progbits,_ZNK21btCollisionDispatcher15getNumManifoldsEv,comdat
	.weak	_ZNK21btCollisionDispatcher15getNumManifoldsEv
	.p2align	4, 0x90
	.type	_ZNK21btCollisionDispatcher15getNumManifoldsEv,@function
_ZNK21btCollisionDispatcher15getNumManifoldsEv: # @_ZNK21btCollisionDispatcher15getNumManifoldsEv
	.cfi_startproc
# BB#0:
	movl	20(%rdi), %eax
	retq
.Lfunc_end15:
	.size	_ZNK21btCollisionDispatcher15getNumManifoldsEv, .Lfunc_end15-_ZNK21btCollisionDispatcher15getNumManifoldsEv
	.cfi_endproc

	.section	.text._ZN21btCollisionDispatcher26getManifoldByIndexInternalEi,"axG",@progbits,_ZN21btCollisionDispatcher26getManifoldByIndexInternalEi,comdat
	.weak	_ZN21btCollisionDispatcher26getManifoldByIndexInternalEi
	.p2align	4, 0x90
	.type	_ZN21btCollisionDispatcher26getManifoldByIndexInternalEi,@function
_ZN21btCollisionDispatcher26getManifoldByIndexInternalEi: # @_ZN21btCollisionDispatcher26getManifoldByIndexInternalEi
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rax
	retq
.Lfunc_end16:
	.size	_ZN21btCollisionDispatcher26getManifoldByIndexInternalEi, .Lfunc_end16-_ZN21btCollisionDispatcher26getManifoldByIndexInternalEi
	.cfi_endproc

	.section	.text._ZN21btCollisionDispatcher26getInternalManifoldPointerEv,"axG",@progbits,_ZN21btCollisionDispatcher26getInternalManifoldPointerEv,comdat
	.weak	_ZN21btCollisionDispatcher26getInternalManifoldPointerEv
	.p2align	4, 0x90
	.type	_ZN21btCollisionDispatcher26getInternalManifoldPointerEv,@function
_ZN21btCollisionDispatcher26getInternalManifoldPointerEv: # @_ZN21btCollisionDispatcher26getInternalManifoldPointerEv
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rax
	retq
.Lfunc_end17:
	.size	_ZN21btCollisionDispatcher26getInternalManifoldPointerEv, .Lfunc_end17-_ZN21btCollisionDispatcher26getInternalManifoldPointerEv
	.cfi_endproc

	.section	.text._ZN23btCollisionPairCallbackD0Ev,"axG",@progbits,_ZN23btCollisionPairCallbackD0Ev,comdat
	.weak	_ZN23btCollisionPairCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN23btCollisionPairCallbackD0Ev,@function
_ZN23btCollisionPairCallbackD0Ev:       # @_ZN23btCollisionPairCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end18:
	.size	_ZN23btCollisionPairCallbackD0Ev, .Lfunc_end18-_ZN23btCollisionPairCallbackD0Ev
	.cfi_endproc

	.section	.text._ZN23btCollisionPairCallback14processOverlapER16btBroadphasePair,"axG",@progbits,_ZN23btCollisionPairCallback14processOverlapER16btBroadphasePair,comdat
	.weak	_ZN23btCollisionPairCallback14processOverlapER16btBroadphasePair
	.p2align	4, 0x90
	.type	_ZN23btCollisionPairCallback14processOverlapER16btBroadphasePair,@function
_ZN23btCollisionPairCallback14processOverlapER16btBroadphasePair: # @_ZN23btCollisionPairCallback14processOverlapER16btBroadphasePair
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 16
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	*232(%rax)
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end19:
	.size	_ZN23btCollisionPairCallback14processOverlapER16btBroadphasePair, .Lfunc_end19-_ZN23btCollisionPairCallback14processOverlapER16btBroadphasePair
	.cfi_endproc

	.section	.text._ZN17btOverlapCallbackD2Ev,"axG",@progbits,_ZN17btOverlapCallbackD2Ev,comdat
	.weak	_ZN17btOverlapCallbackD2Ev
	.p2align	4, 0x90
	.type	_ZN17btOverlapCallbackD2Ev,@function
_ZN17btOverlapCallbackD2Ev:             # @_ZN17btOverlapCallbackD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end20:
	.size	_ZN17btOverlapCallbackD2Ev, .Lfunc_end20-_ZN17btOverlapCallbackD2Ev
	.cfi_endproc

	.type	gNumManifold,@object    # @gNumManifold
	.bss
	.globl	gNumManifold
	.p2align	2
gNumManifold:
	.long	0                       # 0x0
	.size	gNumManifold, 4

	.type	_ZTV21btCollisionDispatcher,@object # @_ZTV21btCollisionDispatcher
	.section	.rodata,"a",@progbits
	.globl	_ZTV21btCollisionDispatcher
	.p2align	3
_ZTV21btCollisionDispatcher:
	.quad	0
	.quad	_ZTI21btCollisionDispatcher
	.quad	_ZN21btCollisionDispatcherD2Ev
	.quad	_ZN21btCollisionDispatcherD0Ev
	.quad	_ZN21btCollisionDispatcher13findAlgorithmEP17btCollisionObjectS1_P20btPersistentManifold
	.quad	_ZN21btCollisionDispatcher14getNewManifoldEPvS0_
	.quad	_ZN21btCollisionDispatcher15releaseManifoldEP20btPersistentManifold
	.quad	_ZN21btCollisionDispatcher13clearManifoldEP20btPersistentManifold
	.quad	_ZN21btCollisionDispatcher14needsCollisionEP17btCollisionObjectS1_
	.quad	_ZN21btCollisionDispatcher13needsResponseEP17btCollisionObjectS1_
	.quad	_ZN21btCollisionDispatcher25dispatchAllCollisionPairsEP22btOverlappingPairCacheRK16btDispatcherInfoP12btDispatcher
	.quad	_ZNK21btCollisionDispatcher15getNumManifoldsEv
	.quad	_ZN21btCollisionDispatcher26getManifoldByIndexInternalEi
	.quad	_ZN21btCollisionDispatcher26getInternalManifoldPointerEv
	.quad	_ZN21btCollisionDispatcher26allocateCollisionAlgorithmEi
	.quad	_ZN21btCollisionDispatcher22freeCollisionAlgorithmEPv
	.size	_ZTV21btCollisionDispatcher, 128

	.type	_ZTS21btCollisionDispatcher,@object # @_ZTS21btCollisionDispatcher
	.globl	_ZTS21btCollisionDispatcher
	.p2align	4
_ZTS21btCollisionDispatcher:
	.asciz	"21btCollisionDispatcher"
	.size	_ZTS21btCollisionDispatcher, 24

	.type	_ZTI21btCollisionDispatcher,@object # @_ZTI21btCollisionDispatcher
	.globl	_ZTI21btCollisionDispatcher
	.p2align	4
_ZTI21btCollisionDispatcher:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS21btCollisionDispatcher
	.quad	_ZTI12btDispatcher
	.size	_ZTI21btCollisionDispatcher, 24

	.type	_ZTV23btCollisionPairCallback,@object # @_ZTV23btCollisionPairCallback
	.section	.rodata._ZTV23btCollisionPairCallback,"aG",@progbits,_ZTV23btCollisionPairCallback,comdat
	.weak	_ZTV23btCollisionPairCallback
	.p2align	3
_ZTV23btCollisionPairCallback:
	.quad	0
	.quad	_ZTI23btCollisionPairCallback
	.quad	_ZN17btOverlapCallbackD2Ev
	.quad	_ZN23btCollisionPairCallbackD0Ev
	.quad	_ZN23btCollisionPairCallback14processOverlapER16btBroadphasePair
	.size	_ZTV23btCollisionPairCallback, 40

	.type	_ZTS23btCollisionPairCallback,@object # @_ZTS23btCollisionPairCallback
	.section	.rodata._ZTS23btCollisionPairCallback,"aG",@progbits,_ZTS23btCollisionPairCallback,comdat
	.weak	_ZTS23btCollisionPairCallback
	.p2align	4
_ZTS23btCollisionPairCallback:
	.asciz	"23btCollisionPairCallback"
	.size	_ZTS23btCollisionPairCallback, 26

	.type	_ZTS17btOverlapCallback,@object # @_ZTS17btOverlapCallback
	.section	.rodata._ZTS17btOverlapCallback,"aG",@progbits,_ZTS17btOverlapCallback,comdat
	.weak	_ZTS17btOverlapCallback
	.p2align	4
_ZTS17btOverlapCallback:
	.asciz	"17btOverlapCallback"
	.size	_ZTS17btOverlapCallback, 20

	.type	_ZTI17btOverlapCallback,@object # @_ZTI17btOverlapCallback
	.section	.rodata._ZTI17btOverlapCallback,"aG",@progbits,_ZTI17btOverlapCallback,comdat
	.weak	_ZTI17btOverlapCallback
	.p2align	3
_ZTI17btOverlapCallback:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS17btOverlapCallback
	.size	_ZTI17btOverlapCallback, 16

	.type	_ZTI23btCollisionPairCallback,@object # @_ZTI23btCollisionPairCallback
	.section	.rodata._ZTI23btCollisionPairCallback,"aG",@progbits,_ZTI23btCollisionPairCallback,comdat
	.weak	_ZTI23btCollisionPairCallback
	.p2align	4
_ZTI23btCollisionPairCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS23btCollisionPairCallback
	.quad	_ZTI17btOverlapCallback
	.size	_ZTI23btCollisionPairCallback, 24


	.globl	_ZN21btCollisionDispatcherC1EP24btCollisionConfiguration
	.type	_ZN21btCollisionDispatcherC1EP24btCollisionConfiguration,@function
_ZN21btCollisionDispatcherC1EP24btCollisionConfiguration = _ZN21btCollisionDispatcherC2EP24btCollisionConfiguration
	.globl	_ZN21btCollisionDispatcherD1Ev
	.type	_ZN21btCollisionDispatcherD1Ev,@function
_ZN21btCollisionDispatcherD1Ev = _ZN21btCollisionDispatcherD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
