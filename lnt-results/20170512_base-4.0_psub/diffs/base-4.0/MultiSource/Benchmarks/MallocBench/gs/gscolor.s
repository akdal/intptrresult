	.text
	.file	"gscolor.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182418800017408     # double 1
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_1:
	.long	1199570688              # float 65535
	.text
	.globl	gs_setgray
	.p2align	4, 0x90
	.type	gs_setgray,@function
gs_setgray:                             # @gs_setgray
	.cfi_startproc
# BB#0:
	movl	$-21, %eax
	cmpb	$0, 436(%rdi)
	jne	.LBB0_5
# BB#1:
	movq	304(%rdi), %rcx
	xorl	%eax, %eax
	xorps	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	movl	$0, %edx
	ja	.LBB0_4
# BB#2:
	movw	$-1, %dx
	ucomisd	.LCPI0_0(%rip), %xmm0
	ja	.LBB0_4
# BB#3:
	cvtsd2ss	%xmm0, %xmm0
	mulss	.LCPI0_1(%rip), %xmm0
	cvttss2si	%xmm0, %edx
.LBB0_4:                                # %check_unit.exit
	movw	%dx, 6(%rcx)
	movw	%dx, 4(%rcx)
	movw	%dx, 2(%rcx)
	movw	%dx, (%rcx)
	movw	$257, 8(%rcx)           # imm = 0x101
.LBB0_5:
	retq
.Lfunc_end0:
	.size	gs_setgray, .Lfunc_end0-gs_setgray
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1065353216              # float 1
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_1:
	.quad	4607182418800017408     # double 1
	.text
	.globl	check_unit
	.p2align	4, 0x90
	.type	check_unit,@function
check_unit:                             # @check_unit
	.cfi_startproc
# BB#0:
	xorps	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
	ucomisd	%xmm0, %xmm2
	ja	.LBB1_4
# BB#1:
	ucomisd	.LCPI1_1(%rip), %xmm0
	jbe	.LBB1_3
# BB#2:
	movss	.LCPI1_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	jmp	.LBB1_4
.LBB1_3:
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm0, %xmm1
.LBB1_4:
	movss	%xmm1, (%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	check_unit, .Lfunc_end1-check_unit
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	1199570688              # float 65535
	.text
	.globl	gs_currentgray
	.p2align	4, 0x90
	.type	gs_currentgray,@function
gs_currentgray:                         # @gs_currentgray
	.cfi_startproc
# BB#0:
	movq	304(%rdi), %rdi
	cmpb	$0, 9(%rdi)
	je	.LBB2_2
# BB#1:
	movzwl	6(%rdi), %eax
	jmp	.LBB2_3
.LBB2_2:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	callq	gx_color_luminance
	addq	$8, %rsp
.LBB2_3:
	movzwl	%ax, %eax
	cvtsi2ssl	%eax, %xmm0
	divss	.LCPI2_0(%rip), %xmm0
	retq
.Lfunc_end2:
	.size	gs_currentgray, .Lfunc_end2-gs_currentgray
	.cfi_endproc

	.globl	gs_setgscolor
	.p2align	4, 0x90
	.type	gs_setgscolor,@function
gs_setgscolor:                          # @gs_setgscolor
	.cfi_startproc
# BB#0:
	movl	$-21, %eax
	cmpb	$0, 436(%rdi)
	jne	.LBB3_2
# BB#1:
	movq	304(%rdi), %rax
	movzwl	8(%rsi), %ecx
	movw	%cx, 8(%rax)
	movq	(%rsi), %rcx
	movq	%rcx, (%rax)
	xorl	%eax, %eax
.LBB3_2:
	retq
.Lfunc_end3:
	.size	gs_setgscolor, .Lfunc_end3-gs_setgscolor
	.cfi_endproc

	.globl	gs_currentgscolor
	.p2align	4, 0x90
	.type	gs_currentgscolor,@function
gs_currentgscolor:                      # @gs_currentgscolor
	.cfi_startproc
# BB#0:
	movq	304(%rdi), %rax
	movzwl	8(%rax), %ecx
	movw	%cx, 8(%rsi)
	movq	(%rax), %rax
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end4:
	.size	gs_currentgscolor, .Lfunc_end4-gs_currentgscolor
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4607182418800017408     # double 1
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_1:
	.long	1199570688              # float 65535
	.text
	.globl	gs_sethsbcolor
	.p2align	4, 0x90
	.type	gs_sethsbcolor,@function
gs_sethsbcolor:                         # @gs_sethsbcolor
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 16
.Lcfi2:
	.cfi_offset %rbx, -16
	movl	$-21, %ebx
	cmpb	$0, 436(%rdi)
	jne	.LBB5_13
# BB#1:
	xorps	%xmm3, %xmm3
	ucomisd	%xmm0, %xmm3
	jbe	.LBB5_3
# BB#2:
	xorl	%edx, %edx
	ucomisd	%xmm1, %xmm3
	ja	.LBB5_6
	jmp	.LBB5_7
.LBB5_3:
	movw	$-1, %dx
	ucomisd	.LCPI5_0(%rip), %xmm0
	ja	.LBB5_5
# BB#4:
	cvtsd2ss	%xmm0, %xmm0
	mulss	.LCPI5_1(%rip), %xmm0
	cvttss2si	%xmm0, %edx
.LBB5_5:                                # %check_unit.exit.i
	ucomisd	%xmm1, %xmm3
	jbe	.LBB5_7
.LBB5_6:
	xorl	%ecx, %ecx
	jmp	.LBB5_9
.LBB5_7:
	movw	$-1, %cx
	ucomisd	.LCPI5_0(%rip), %xmm1
	ja	.LBB5_9
# BB#8:
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	mulss	.LCPI5_1(%rip), %xmm0
	cvttss2si	%xmm0, %ecx
.LBB5_9:                                # %check_unit.exit12.i
	xorl	%ebx, %ebx
	xorps	%xmm0, %xmm0
	ucomisd	%xmm2, %xmm0
	movl	$0, %eax
	ja	.LBB5_12
# BB#10:
	movw	$-1, %ax
	ucomisd	.LCPI5_0(%rip), %xmm2
	ja	.LBB5_12
# BB#11:
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm2, %xmm0
	mulss	.LCPI5_1(%rip), %xmm0
	cvttss2si	%xmm0, %eax
.LBB5_12:                               # %tri_param.exit
	movq	304(%rdi), %rdi
	movzwl	%dx, %esi
	movzwl	%cx, %edx
	movzwl	%ax, %ecx
	callq	gx_color_from_hsb
.LBB5_13:
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	gs_sethsbcolor, .Lfunc_end5-gs_sethsbcolor
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4607182418800017408     # double 1
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_1:
	.long	1199570688              # float 65535
	.text
	.globl	tri_param
	.p2align	4, 0x90
	.type	tri_param,@function
tri_param:                              # @tri_param
	.cfi_startproc
# BB#0:
	xorps	%xmm3, %xmm3
	ucomisd	%xmm0, %xmm3
	jbe	.LBB6_2
# BB#1:
	xorl	%eax, %eax
	ucomisd	%xmm1, %xmm3
	ja	.LBB6_5
	jmp	.LBB6_6
.LBB6_2:
	movw	$-1, %ax
	ucomisd	.LCPI6_0(%rip), %xmm0
	ja	.LBB6_4
# BB#3:
	cvtsd2ss	%xmm0, %xmm0
	mulss	.LCPI6_1(%rip), %xmm0
	cvttss2si	%xmm0, %eax
.LBB6_4:                                # %check_unit.exit
	ucomisd	%xmm1, %xmm3
	jbe	.LBB6_6
.LBB6_5:
	xorl	%ecx, %ecx
	jmp	.LBB6_8
.LBB6_6:
	movw	$-1, %cx
	ucomisd	.LCPI6_0(%rip), %xmm1
	ja	.LBB6_8
# BB#7:
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	mulss	.LCPI6_1(%rip), %xmm0
	cvttss2si	%xmm0, %ecx
.LBB6_8:                                # %check_unit.exit12
	xorps	%xmm0, %xmm0
	ucomisd	%xmm2, %xmm0
	jbe	.LBB6_10
# BB#9:
	xorl	%edx, %edx
	jmp	.LBB6_12
.LBB6_10:
	movw	$-1, %dx
	ucomisd	.LCPI6_0(%rip), %xmm2
	ja	.LBB6_12
# BB#11:
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm2, %xmm0
	mulss	.LCPI6_1(%rip), %xmm0
	cvttss2si	%xmm0, %edx
.LBB6_12:                               # %check_unit.exit10
	movw	%ax, (%rdi)
	movw	%cx, 2(%rdi)
	movw	%dx, 4(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end6:
	.size	tri_param, .Lfunc_end6-tri_param
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI7_0:
	.long	1199570688              # float 65535
	.text
	.globl	gs_currenthsbcolor
	.p2align	4, 0x90
	.type	gs_currenthsbcolor,@function
gs_currenthsbcolor:                     # @gs_currenthsbcolor
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	304(%rdi), %rdi
	leaq	10(%rsp), %rsi
	callq	gx_color_to_hsb
	movzwl	10(%rsp), %eax
	cvtsi2ssl	%eax, %xmm0
	movss	.LCPI7_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movzwl	12(%rsp), %eax
	movzwl	14(%rsp), %ecx
	movss	%xmm0, (%rbx)
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	%xmm1, %xmm0
	movss	%xmm0, 4(%rbx)
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	divss	%xmm1, %xmm0
	movss	%xmm0, 8(%rbx)
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end7:
	.size	gs_currenthsbcolor, .Lfunc_end7-gs_currenthsbcolor
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_0:
	.long	1199570688              # float 65535
	.text
	.globl	tri_return
	.p2align	4, 0x90
	.type	tri_return,@function
tri_return:                             # @tri_return
	.cfi_startproc
# BB#0:
	cvtsi2ssl	%edi, %xmm0
	movss	.LCPI8_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	%xmm0, (%rcx)
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%esi, %xmm0
	divss	%xmm1, %xmm0
	movss	%xmm0, 4(%rcx)
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	divss	%xmm1, %xmm0
	movss	%xmm0, 8(%rcx)
	retq
.Lfunc_end8:
	.size	tri_return, .Lfunc_end8-tri_return
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI9_0:
	.quad	4607182418800017408     # double 1
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_1:
	.long	1199570688              # float 65535
	.text
	.globl	gs_setrgbcolor
	.p2align	4, 0x90
	.type	gs_setrgbcolor,@function
gs_setrgbcolor:                         # @gs_setrgbcolor
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movl	$-21, %ebx
	cmpb	$0, 436(%rdi)
	jne	.LBB9_13
# BB#1:
	xorps	%xmm3, %xmm3
	ucomisd	%xmm0, %xmm3
	jbe	.LBB9_3
# BB#2:
	xorl	%eax, %eax
	ucomisd	%xmm1, %xmm3
	ja	.LBB9_6
	jmp	.LBB9_7
.LBB9_3:
	movw	$-1, %ax
	ucomisd	.LCPI9_0(%rip), %xmm0
	ja	.LBB9_5
# BB#4:
	cvtsd2ss	%xmm0, %xmm0
	mulss	.LCPI9_1(%rip), %xmm0
	cvttss2si	%xmm0, %eax
.LBB9_5:                                # %check_unit.exit.i
	ucomisd	%xmm1, %xmm3
	jbe	.LBB9_7
.LBB9_6:
	xorl	%ecx, %ecx
	jmp	.LBB9_9
.LBB9_7:
	movw	$-1, %cx
	ucomisd	.LCPI9_0(%rip), %xmm1
	ja	.LBB9_9
# BB#8:
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	mulss	.LCPI9_1(%rip), %xmm0
	cvttss2si	%xmm0, %ecx
.LBB9_9:                                # %check_unit.exit12.i
	xorl	%ebx, %ebx
	xorps	%xmm0, %xmm0
	ucomisd	%xmm2, %xmm0
	movl	$0, %edx
	ja	.LBB9_12
# BB#10:
	movw	$-1, %dx
	ucomisd	.LCPI9_0(%rip), %xmm2
	ja	.LBB9_12
# BB#11:
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm2, %xmm0
	mulss	.LCPI9_1(%rip), %xmm0
	cvttss2si	%xmm0, %edx
.LBB9_12:                               # %tri_param.exit
	movq	304(%rdi), %rdi
	movw	%ax, (%rdi)
	movw	%cx, 2(%rdi)
	movw	%dx, 4(%rdi)
	callq	gx_color_from_rgb
.LBB9_13:
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end9:
	.size	gs_setrgbcolor, .Lfunc_end9-gs_setrgbcolor
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI10_0:
	.long	1199570688              # float 65535
	.text
	.globl	gs_currentrgbcolor
	.p2align	4, 0x90
	.type	gs_currentrgbcolor,@function
gs_currentrgbcolor:                     # @gs_currentrgbcolor
	.cfi_startproc
# BB#0:
	movq	304(%rdi), %rax
	movzwl	(%rax), %ecx
	cvtsi2ssl	%ecx, %xmm0
	movss	.LCPI10_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movzwl	2(%rax), %ecx
	movzwl	4(%rax), %eax
	movss	%xmm0, (%rsi)
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	divss	%xmm1, %xmm0
	movss	%xmm0, 4(%rsi)
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	%xmm1, %xmm0
	movss	%xmm0, 8(%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	gs_currentrgbcolor, .Lfunc_end10-gs_currentrgbcolor
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI11_0:
	.long	1199570688              # float 65535
	.text
	.globl	gs_colorrgb
	.p2align	4, 0x90
	.type	gs_colorrgb,@function
gs_colorrgb:                            # @gs_colorrgb
	.cfi_startproc
# BB#0:
	movzwl	(%rdi), %eax
	cvtsi2ssl	%eax, %xmm0
	movss	.LCPI11_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movzwl	2(%rdi), %eax
	movzwl	4(%rdi), %ecx
	movss	%xmm0, (%rsi)
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	%xmm1, %xmm0
	movss	%xmm0, 4(%rsi)
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	divss	%xmm1, %xmm0
	movss	%xmm0, 8(%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end11:
	.size	gs_colorrgb, .Lfunc_end11-gs_colorrgb
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI12_0:
	.long	3212836864              # float -1
.LCPI12_1:
	.long	1065353216              # float 1
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI12_2:
	.quad	4674736138332667904     # double 32767
	.text
	.globl	gs_setscreen
	.p2align	4, 0x90
	.type	gs_setscreen,@function
gs_setscreen:                           # @gs_setscreen
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 48
	subq	$144, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 192
.Lcfi14:
	.cfi_offset %rbx, -48
.Lcfi15:
	.cfi_offset %r12, -40
.Lcfi16:
	.cfi_offset %r13, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	gs_screen_init
	testl	%eax, %eax
	js	.LBB12_9
# BB#1:                                 # %gs_screen_next.exit.thread.preheader
	leaq	8(%rsp), %rdi
	movq	%rsp, %rsi
	callq	gs_screen_currentpoint
	testl	%eax, %eax
	jne	.LBB12_7
# BB#2:                                 # %.lr.ph
	leaq	8(%rsp), %r15
	movq	%rsp, %r12
	.p2align	4, 0x90
.LBB12_3:                               # =>This Inner Loop Header: Depth=1
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	callq	*%r13
	movss	.LCPI12_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	movl	$-15, %eax
	ja	.LBB12_9
# BB#4:                                 #   in Loop: Header=BB12_3 Depth=1
	ucomiss	.LCPI12_1(%rip), %xmm0
	ja	.LBB12_9
# BB#5:                                 #   in Loop: Header=BB12_3 Depth=1
	cvtss2sd	%xmm0, %xmm0
	movq	16(%rsp), %rcx
	mulsd	.LCPI12_2(%rip), %xmm0
	cvttsd2si	%xmm0, %edx
	addl	$32767, %edx            # imm = 0x7FFF
	movl	132(%rsp), %eax
	movl	24(%rsp), %esi
	movl	%esi, %edi
	imull	%eax, %edi
	movl	128(%rsp), %ebx
	addl	%ebx, %edi
	movslq	%edi, %rdi
	movw	%dx, 2(%rcx,%rdi,4)
	incl	%ebx
	movl	%ebx, 128(%rsp)
	cmpl	%esi, %ebx
	jl	.LBB12_6
# BB#10:                                #   in Loop: Header=BB12_3 Depth=1
	movl	$0, 128(%rsp)
	incl	%eax
	movl	%eax, 132(%rsp)
.LBB12_6:                               # %gs_screen_next.exit.thread.backedge
                                        #   in Loop: Header=BB12_3 Depth=1
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	gs_screen_currentpoint
	testl	%eax, %eax
	je	.LBB12_3
.LBB12_7:                               # %gs_screen_next.exit.thread._crit_edge
	testl	%eax, %eax
	js	.LBB12_9
# BB#8:
	movq	%r13, 296(%r14)
	xorl	%eax, %eax
.LBB12_9:                               # %gs_screen_next.exit
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	gs_setscreen, .Lfunc_end12-gs_setscreen
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI13_0:
	.quad	4634766966517661696     # double 72
.LCPI13_1:
	.quad	4611686018427387904     # double 2
.LCPI13_2:
	.quad	4602678819172646912     # double 0.5
.LCPI13_3:
	.quad	-4616189618054758400    # double -1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_4:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	gs_screen_init
	.p2align	4, 0x90
	.type	gs_screen_init,@function
gs_screen_init:                         # @gs_screen_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 40
	subq	$152, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 192
.Lcfi24:
	.cfi_offset %rbx, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$-15, %eax
	xorpd	%xmm2, %xmm2
	ucomisd	%xmm0, %xmm2
	ja	.LBB13_9
# BB#1:
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	movsd	.LCPI13_0(%rip), %xmm1  # xmm1 = mem[0],zero
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	divsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movq	%r15, %rdi
	callq	gs_currentdevice
	leaq	48(%rsp), %rbx
	leaq	28(%rsp), %rdx
	leaq	24(%rsp), %rcx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	gs_deviceparams
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	leaq	16(%rsp), %rsi
	movapd	%xmm0, %xmm1
	movq	%rbx, %rdi
	callq	gs_distance_transform
	testl	%eax, %eax
	js	.LBB13_2
# BB#3:
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvttss2si	%xmm0, %ecx
	movl	%ecx, %ebx
	negl	%ebx
	xorpd	%xmm2, %xmm2
	ucomiss	%xmm0, %xmm2
	cmovbel	%ecx, %ebx
	cvttss2si	%xmm1, %ecx
	movl	%ecx, %ebp
	negl	%ebp
	ucomiss	%xmm1, %xmm2
	cmovbel	%ecx, %ebp
	movb	$1, %cl
	testb	%cl, %cl
	jne	.LBB13_5
	jmp	.LBB13_9
.LBB13_2:
	xorl	%ecx, %ecx
                                        # implicit-def: %EBP
                                        # implicit-def: %EBX
	testb	%cl, %cl
	je	.LBB13_9
.LBB13_5:
	testl	%ebx, %ebx
	movl	$1, %eax
	cmovel	%eax, %ebx
	testl	%ebp, %ebp
	cmovel	%eax, %ebp
	movl	$65535, %eax            # imm = 0xFFFF
	xorl	%edx, %edx
	idivl	%ebp
	cmpl	%eax, %ebx
	movl	$-15, %eax
	jg	.LBB13_9
# BB#6:
	movl	%ebx, %edi
	imull	%ebp, %edi
	movl	$4, %esi
	movl	$.L.str, %edx
	callq	gs_malloc
	testq	%rax, %rax
	je	.LBB13_7
# BB#8:
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%r14)
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 4(%r14)
	movq	%rax, 8(%r14)
	movl	%ebx, 16(%r14)
	movl	%ebp, 20(%r14)
	movl	$0, 124(%r14)
	movl	$0, 120(%r14)
	movq	%r15, 128(%r14)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movsd	.LCPI13_1(%rip), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	divsd	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm2, %xmm0
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	divsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	leaq	48(%rsp), %rbx
	movq	%rbx, %rdi
	callq	gs_make_identity
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 48(%rsp)
	movss	8(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, 96(%rsp)
	cvtss2sd	%xmm0, %xmm0
	movsd	.LCPI13_2(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	.LCPI13_3(%rip), %xmm2  # xmm2 = mem[0],zero
	addsd	%xmm2, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 112(%rsp)
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm3, %xmm0
	mulsd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 128(%rsp)
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	xorpd	.LCPI13_4(%rip), %xmm0
	addq	$24, %r14
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	gs_matrix_rotate
	movl	%eax, %ecx
	sarl	$31, %ecx
	andl	%eax, %ecx
	movl	%ecx, %eax
	jmp	.LBB13_9
.LBB13_7:
	movl	$-25, %eax
.LBB13_9:
	addq	$152, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	gs_screen_init, .Lfunc_end13-gs_screen_init
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI14_0:
	.quad	4611686018427387904     # double 2
.LCPI14_2:
	.quad	-4611686018427387904    # double -2
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI14_1:
	.long	3212836864              # float -1
.LCPI14_3:
	.long	1065353216              # float 1
	.text
	.globl	gs_screen_currentpoint
	.p2align	4, 0x90
	.type	gs_screen_currentpoint,@function
gs_screen_currentpoint:                 # @gs_screen_currentpoint
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	20(%rbx), %esi
	movl	124(%rbx), %eax
	cmpl	%esi, %eax
	jge	.LBB14_1
# BB#8:
	movl	120(%rbx), %ecx
	cvtsi2sdl	%ecx, %xmm0
	cvtsi2sdl	%eax, %xmm1
	addq	$24, %rbx
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	gs_point_transform
	testl	%eax, %eax
	js	.LBB14_20
# BB#9:
	movss	(%rsp), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI14_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jbe	.LBB14_11
# BB#10:
	movsd	.LCPI14_0(%rip), %xmm2  # xmm2 = mem[0],zero
	jmp	.LBB14_13
.LBB14_1:
	movq	8(%rbx), %rdi
	imull	16(%rbx), %esi
	testl	%esi, %esi
	je	.LBB14_6
# BB#2:                                 # %.lr.ph.preheader.i
	movl	%esi, %edx
	leaq	-1(%rdx), %r8
	movq	%rdx, %rax
	xorl	%ecx, %ecx
	andq	$7, %rax
	je	.LBB14_4
	.p2align	4, 0x90
.LBB14_3:                               # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movw	%cx, (%rdi,%rcx,4)
	incq	%rcx
	cmpq	%rcx, %rax
	jne	.LBB14_3
.LBB14_4:                               # %.lr.ph.i.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB14_6
	.p2align	4, 0x90
.LBB14_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movw	%cx, (%rdi,%rcx,4)
	leal	1(%rcx), %eax
	movw	%ax, 4(%rdi,%rcx,4)
	leaq	2(%rcx), %rax
	movw	%ax, 8(%rdi,%rcx,4)
	incl	%eax
	movw	%ax, 12(%rdi,%rcx,4)
	leaq	4(%rcx), %rax
	movw	%ax, 16(%rdi,%rcx,4)
	incl	%eax
	movw	%ax, 20(%rdi,%rcx,4)
	leaq	6(%rcx), %rax
	movw	%ax, 24(%rdi,%rcx,4)
	incl	%eax
	movw	%ax, 28(%rdi,%rcx,4)
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB14_5
.LBB14_6:                               # %._crit_edge.i
	callq	gx_sort_ht_order
	movq	8(%rbx), %rdi
	movl	16(%rbx), %esi
	movl	20(%rbx), %edx
	xorl	%eax, %eax
	callq	gx_ht_construct_order
	testl	%eax, %eax
	js	.LBB14_20
# BB#7:
	movq	128(%rbx), %rax
	movq	288(%rax), %rax
	movl	(%rbx), %ecx
	movl	%ecx, (%rax)
	movl	4(%rbx), %ecx
	movl	%ecx, 4(%rax)
	movl	16(%rbx), %ecx
	movl	%ecx, 8(%rax)
	movl	20(%rbx), %edx
	movl	%edx, 12(%rax)
	movq	8(%rbx), %rsi
	movq	%rsi, 16(%rax)
	imull	%ecx, %edx
	movl	%edx, 24(%rax)
	movl	$1, %eax
	jmp	.LBB14_20
.LBB14_11:
	ucomiss	.LCPI14_3(%rip), %xmm1
	jbe	.LBB14_14
# BB#12:
	movsd	.LCPI14_2(%rip), %xmm2  # xmm2 = mem[0],zero
.LBB14_13:                              # %.sink.split
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm2, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	%xmm1, (%rsp)
.LBB14_14:
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jbe	.LBB14_16
# BB#15:
	movsd	.LCPI14_0(%rip), %xmm0  # xmm0 = mem[0],zero
	jmp	.LBB14_18
.LBB14_16:
	ucomiss	.LCPI14_3(%rip), %xmm1
	jbe	.LBB14_19
# BB#17:
	movsd	.LCPI14_2(%rip), %xmm0  # xmm0 = mem[0],zero
.LBB14_18:                              # %.sink.split3
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%rsp)
.LBB14_19:
	movq	(%rsp), %rax
	movq	%rax, (%r14)
	xorl	%eax, %eax
.LBB14_20:                              # %gx_screen_finish.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end14:
	.size	gs_screen_currentpoint, .Lfunc_end14-gs_screen_currentpoint
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI15_0:
	.quad	-4616189618054758400    # double -1
.LCPI15_1:
	.quad	4607182418800017408     # double 1
.LCPI15_2:
	.quad	4674736138332667904     # double 32767
	.text
	.globl	gs_screen_next
	.p2align	4, 0x90
	.type	gs_screen_next,@function
gs_screen_next:                         # @gs_screen_next
	.cfi_startproc
# BB#0:
	movsd	.LCPI15_0(%rip), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	movl	$-15, %eax
	ja	.LBB15_4
# BB#1:
	ucomisd	.LCPI15_1(%rip), %xmm0
	ja	.LBB15_4
# BB#2:
	movq	8(%rdi), %rax
	mulsd	.LCPI15_2(%rip), %xmm0
	cvttsd2si	%xmm0, %edx
	addl	$32767, %edx            # imm = 0x7FFF
	movl	124(%rdi), %r8d
	movl	16(%rdi), %r9d
	movl	%r9d, %esi
	imull	%r8d, %esi
	movl	120(%rdi), %ecx
	addl	%ecx, %esi
	movslq	%esi, %rsi
	movw	%dx, 2(%rax,%rsi,4)
	incl	%ecx
	movl	%ecx, 120(%rdi)
	xorl	%eax, %eax
	cmpl	%r9d, %ecx
	jl	.LBB15_4
# BB#3:
	movl	$0, 120(%rdi)
	incl	%r8d
	movl	%r8d, 124(%rdi)
.LBB15_4:
	retq
.Lfunc_end15:
	.size	gs_screen_next, .Lfunc_end15-gs_screen_next
	.cfi_endproc

	.globl	gs_currentscreen
	.p2align	4, 0x90
	.type	gs_currentscreen,@function
gs_currentscreen:                       # @gs_currentscreen
	.cfi_startproc
# BB#0:
	movq	288(%rdi), %r8
	movl	(%r8), %eax
	movl	%eax, (%rsi)
	movl	4(%r8), %eax
	movl	%eax, (%rdx)
	movq	296(%rdi), %rax
	movq	%rax, (%rcx)
	xorl	%eax, %eax
	retq
.Lfunc_end16:
	.size	gs_currentscreen, .Lfunc_end16-gs_currentscreen
	.cfi_endproc

	.globl	gx_screen_finish
	.p2align	4, 0x90
	.type	gx_screen_finish,@function
gx_screen_finish:                       # @gx_screen_finish
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 16
.Lcfi34:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movl	20(%rbx), %esi
	imull	16(%rbx), %esi
	testl	%esi, %esi
	je	.LBB17_5
# BB#1:                                 # %.lr.ph.preheader
	movl	%esi, %edx
	leaq	-1(%rdx), %r8
	movq	%rdx, %rax
	xorl	%ecx, %ecx
	andq	$7, %rax
	je	.LBB17_3
	.p2align	4, 0x90
.LBB17_2:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movw	%cx, (%rdi,%rcx,4)
	incq	%rcx
	cmpq	%rcx, %rax
	jne	.LBB17_2
.LBB17_3:                               # %.lr.ph.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB17_5
	.p2align	4, 0x90
.LBB17_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movw	%cx, (%rdi,%rcx,4)
	leal	1(%rcx), %eax
	movw	%ax, 4(%rdi,%rcx,4)
	leaq	2(%rcx), %rax
	movw	%ax, 8(%rdi,%rcx,4)
	incl	%eax
	movw	%ax, 12(%rdi,%rcx,4)
	leaq	4(%rcx), %rax
	movw	%ax, 16(%rdi,%rcx,4)
	incl	%eax
	movw	%ax, 20(%rdi,%rcx,4)
	leaq	6(%rcx), %rax
	movw	%ax, 24(%rdi,%rcx,4)
	incl	%eax
	movw	%ax, 28(%rdi,%rcx,4)
	addq	$8, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB17_4
.LBB17_5:                               # %._crit_edge
	callq	gx_sort_ht_order
	movq	8(%rbx), %rdi
	movl	16(%rbx), %esi
	movl	20(%rbx), %edx
	xorl	%eax, %eax
	callq	gx_ht_construct_order
	testl	%eax, %eax
	js	.LBB17_7
# BB#6:
	movq	128(%rbx), %rax
	movq	288(%rax), %rax
	movl	(%rbx), %ecx
	movl	%ecx, (%rax)
	movl	4(%rbx), %ecx
	movl	%ecx, 4(%rax)
	movl	16(%rbx), %ecx
	movl	%ecx, 8(%rax)
	movl	20(%rbx), %edx
	movl	%edx, 12(%rax)
	movq	8(%rbx), %rsi
	movq	%rsi, 16(%rax)
	imull	%ecx, %edx
	movl	%edx, 24(%rax)
	movl	$1, %eax
.LBB17_7:
	popq	%rbx
	retq
.Lfunc_end17:
	.size	gx_screen_finish, .Lfunc_end17-gx_screen_finish
	.cfi_endproc

	.type	gs_screen_enum_sizeof,@object # @gs_screen_enum_sizeof
	.data
	.globl	gs_screen_enum_sizeof
	.p2align	2
gs_screen_enum_sizeof:
	.long	136                     # 0x88
	.size	gs_screen_enum_sizeof, 4

	.type	gs_color_sizeof,@object # @gs_color_sizeof
	.globl	gs_color_sizeof
	.p2align	2
gs_color_sizeof:
	.long	10                      # 0xa
	.size	gs_color_sizeof, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"halftone samples"
	.size	.L.str, 17


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
