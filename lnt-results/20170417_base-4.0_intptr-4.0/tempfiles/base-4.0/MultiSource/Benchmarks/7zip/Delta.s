	.text
	.file	"Delta.bc"
	.globl	Delta_Init
	.p2align	4, 0x90
	.type	Delta_Init,@function
Delta_Init:                             # @Delta_Init
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 240(%rdi)
	movups	%xmm0, 224(%rdi)
	movups	%xmm0, 208(%rdi)
	movups	%xmm0, 192(%rdi)
	movups	%xmm0, 176(%rdi)
	movups	%xmm0, 160(%rdi)
	movups	%xmm0, 144(%rdi)
	movups	%xmm0, 128(%rdi)
	movups	%xmm0, 112(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, (%rdi)
	retq
.Lfunc_end0:
	.size	Delta_Init, .Lfunc_end0-Delta_Init
	.cfi_endproc

	.globl	Delta_Encode
	.p2align	4, 0x90
	.type	Delta_Encode,@function
Delta_Encode:                           # @Delta_Encode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi6:
	.cfi_def_cfa_offset 336
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %rbp
	movl	%esi, %r12d
	movq	%rdi, %r14
	testl	%r12d, %r12d
	je	.LBB1_2
# BB#1:                                 # %.lr.ph.preheader.i
	movl	%r12d, %edx
	leaq	16(%rsp), %rdi
	movq	%r14, %rsi
	callq	memcpy
.LBB1_2:                                # %.critedge.preheader
	xorl	%r15d, %r15d
	testq	%rbx, %rbx
	movl	$0, %esi
	je	.LBB1_21
# BB#3:                                 # %.preheader.lr.ph
	testl	%r12d, %r12d
	je	.LBB1_26
# BB#4:                                 # %.preheader.us.preheader
	movl	%r12d, %eax
	movq	%rax, %r10
	negq	%r10
	leaq	48(%rbp), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_5:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_16 Depth 2
                                        #     Child Loop BB1_18 Depth 2
	movq	%rdi, %r13
	subq	%rbx, %r13
	jae	.LBB1_6
# BB#7:                                 # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	cmpq	%r10, %r13
	cmovbq	%r10, %r13
	negq	%r13
	cmpq	$32, %r13
	jae	.LBB1_9
# BB#8:                                 #   in Loop: Header=BB1_5 Depth=1
	xorl	%esi, %esi
	jmp	.LBB1_18
	.p2align	4, 0x90
.LBB1_6:                                #   in Loop: Header=BB1_5 Depth=1
	xorl	%esi, %esi
	cmpq	%rbx, %rdi
	jb	.LBB1_5
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_9:                                # %min.iters.checked
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	%r13, %r11
	andq	$-32, %r11
	movq	%r13, %rsi
	andq	$-32, %rsi
	je	.LBB1_10
# BB#11:                                # %vector.body.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	leaq	-32(%rsi), %rcx
	movq	%rcx, %rdx
	shrq	$5, %rdx
	btl	$5, %ecx
	jb	.LBB1_12
# BB#13:                                # %vector.body.prol
                                        #   in Loop: Header=BB1_5 Depth=1
	movdqu	(%rbp,%rdi), %xmm0
	movdqu	16(%rbp,%rdi), %xmm1
	movdqa	%xmm0, %xmm2
	psubb	16(%rsp), %xmm2
	movdqa	%xmm1, %xmm3
	psubb	32(%rsp), %xmm3
	movdqu	%xmm2, (%rbp,%rdi)
	movdqu	%xmm3, 16(%rbp,%rdi)
	movdqa	%xmm0, 16(%rsp)
	movdqa	%xmm1, 32(%rsp)
	movl	$32, %ecx
	testq	%rdx, %rdx
	jne	.LBB1_15
	jmp	.LBB1_17
.LBB1_10:                               #   in Loop: Header=BB1_5 Depth=1
	xorl	%esi, %esi
	jmp	.LBB1_18
.LBB1_12:                               #   in Loop: Header=BB1_5 Depth=1
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.LBB1_17
.LBB1_15:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	leaq	(%rdi,%rcx), %r8
	addq	8(%rsp), %r8            # 8-byte Folded Reload
	leaq	64(%rsp), %r9
	addq	%r9, %rcx
	.p2align	4, 0x90
.LBB1_16:                               # %vector.body
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-48(%r8), %xmm0
	movdqu	-32(%r8), %xmm1
	movdqa	%xmm0, %xmm2
	psubb	-48(%rcx), %xmm2
	movdqa	%xmm1, %xmm3
	psubb	-32(%rcx), %xmm3
	movdqu	%xmm2, -48(%r8)
	movdqu	%xmm3, -32(%r8)
	movdqa	%xmm0, -48(%rcx)
	movdqa	%xmm1, -32(%rcx)
	movdqu	-16(%r8), %xmm0
	movdqu	(%r8), %xmm1
	movdqa	%xmm0, %xmm2
	psubb	-16(%rcx), %xmm2
	movdqa	%xmm1, %xmm3
	psubb	(%rcx), %xmm3
	movdqu	%xmm2, -16(%r8)
	movdqu	%xmm3, (%r8)
	movdqa	%xmm0, -16(%rcx)
	movdqa	%xmm1, (%rcx)
	addq	$64, %r8
	addq	$64, %rcx
	addq	$-64, %rdx
	jne	.LBB1_16
.LBB1_17:                               # %middle.block
                                        #   in Loop: Header=BB1_5 Depth=1
	addq	%r11, %rdi
	cmpq	%r13, %rsi
	je	.LBB1_20
	.p2align	4, 0x90
.LBB1_18:                               # %.lr.ph.us
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbp,%rdi), %ecx
	movl	%ecx, %edx
	subb	16(%rsp,%rsi), %dl
	movb	%dl, (%rbp,%rdi)
	movb	%cl, 16(%rsp,%rsi)
	incq	%rdi
	incq	%rsi
	cmpq	%rbx, %rdi
	jae	.LBB1_20
# BB#19:                                # %.lr.ph.us
                                        #   in Loop: Header=BB1_18 Depth=2
	cmpq	%rax, %rsi
	jb	.LBB1_18
.LBB1_20:                               # %.critedge.loopexit.us
                                        #   in Loop: Header=BB1_5 Depth=1
	cmpq	%rbx, %rdi
	jb	.LBB1_5
.LBB1_21:                               # %.critedge._crit_edge
	cmpl	%r12d, %esi
	cmovnel	%esi, %r15d
	movl	%r12d, %eax
	subl	%r15d, %eax
	je	.LBB1_23
# BB#22:                                # %.lr.ph.preheader.i40
	leaq	16(%rsp,%r15), %rsi
	movl	%eax, %edx
	movq	%r14, %rdi
	callq	memcpy
.LBB1_23:                               # %MyMemCpy.exit45
	testl	%r15d, %r15d
	je	.LBB1_25
# BB#24:                                # %.lr.ph.i37.preheader
	movl	%r12d, %eax
	addq	%rax, %r14
	subq	%r15, %r14
	leaq	16(%rsp), %rsi
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	memcpy
.LBB1_25:                               # %MyMemCpy.exit38
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.p2align	4, 0x90
.LBB1_26:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	jmp	.LBB1_26
.Lfunc_end1:
	.size	Delta_Encode, .Lfunc_end1-Delta_Encode
	.cfi_endproc

	.globl	Delta_Decode
	.p2align	4, 0x90
	.type	Delta_Decode,@function
Delta_Decode:                           # @Delta_Decode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi19:
	.cfi_def_cfa_offset 336
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %rbp
	movl	%esi, %r12d
	movq	%rdi, %r14
	testl	%r12d, %r12d
	je	.LBB2_2
# BB#1:                                 # %.lr.ph.preheader.i
	movl	%r12d, %edx
	leaq	16(%rsp), %rdi
	movq	%r14, %rsi
	callq	memcpy
.LBB2_2:                                # %.critedge.preheader
	xorl	%r15d, %r15d
	testq	%rbx, %rbx
	movl	$0, %esi
	je	.LBB2_21
# BB#3:                                 # %.preheader.lr.ph
	testl	%r12d, %r12d
	je	.LBB2_26
# BB#4:                                 # %.preheader.us.preheader
	movl	%r12d, %eax
	movq	%rax, %r10
	negq	%r10
	leaq	48(%rbp), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_5:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_16 Depth 2
                                        #     Child Loop BB2_18 Depth 2
	movq	%rdi, %r13
	subq	%rbx, %r13
	jae	.LBB2_6
# BB#7:                                 # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB2_5 Depth=1
	cmpq	%r10, %r13
	cmovbq	%r10, %r13
	negq	%r13
	cmpq	$32, %r13
	jae	.LBB2_9
# BB#8:                                 #   in Loop: Header=BB2_5 Depth=1
	xorl	%esi, %esi
	jmp	.LBB2_18
	.p2align	4, 0x90
.LBB2_6:                                #   in Loop: Header=BB2_5 Depth=1
	xorl	%esi, %esi
	cmpq	%rbx, %rdi
	jb	.LBB2_5
	jmp	.LBB2_21
	.p2align	4, 0x90
.LBB2_9:                                # %min.iters.checked
                                        #   in Loop: Header=BB2_5 Depth=1
	movq	%r13, %r11
	andq	$-32, %r11
	movq	%r13, %rsi
	andq	$-32, %rsi
	je	.LBB2_10
# BB#11:                                # %vector.body.preheader
                                        #   in Loop: Header=BB2_5 Depth=1
	leaq	-32(%rsi), %rcx
	movq	%rcx, %rdx
	shrq	$5, %rdx
	btl	$5, %ecx
	jb	.LBB2_12
# BB#13:                                # %vector.body.prol
                                        #   in Loop: Header=BB2_5 Depth=1
	movdqu	(%rbp,%rdi), %xmm0
	movdqu	16(%rbp,%rdi), %xmm1
	paddb	16(%rsp), %xmm0
	paddb	32(%rsp), %xmm1
	movdqu	%xmm0, (%rbp,%rdi)
	movdqu	%xmm1, 16(%rbp,%rdi)
	movdqa	%xmm0, 16(%rsp)
	movdqa	%xmm1, 32(%rsp)
	movl	$32, %ecx
	testq	%rdx, %rdx
	jne	.LBB2_15
	jmp	.LBB2_17
.LBB2_10:                               #   in Loop: Header=BB2_5 Depth=1
	xorl	%esi, %esi
	jmp	.LBB2_18
.LBB2_12:                               #   in Loop: Header=BB2_5 Depth=1
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.LBB2_17
.LBB2_15:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB2_5 Depth=1
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	leaq	(%rdi,%rcx), %r8
	addq	8(%rsp), %r8            # 8-byte Folded Reload
	leaq	64(%rsp), %r9
	addq	%r9, %rcx
	.p2align	4, 0x90
.LBB2_16:                               # %vector.body
                                        #   Parent Loop BB2_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-48(%r8), %xmm0
	movdqu	-32(%r8), %xmm1
	paddb	-48(%rcx), %xmm0
	paddb	-32(%rcx), %xmm1
	movdqu	%xmm0, -48(%r8)
	movdqu	%xmm1, -32(%r8)
	movdqa	%xmm0, -48(%rcx)
	movdqa	%xmm1, -32(%rcx)
	movdqu	-16(%r8), %xmm0
	movdqu	(%r8), %xmm1
	paddb	-16(%rcx), %xmm0
	paddb	(%rcx), %xmm1
	movdqu	%xmm0, -16(%r8)
	movdqu	%xmm1, (%r8)
	movdqa	%xmm0, -16(%rcx)
	movdqa	%xmm1, (%rcx)
	addq	$64, %r8
	addq	$64, %rcx
	addq	$-64, %rdx
	jne	.LBB2_16
.LBB2_17:                               # %middle.block
                                        #   in Loop: Header=BB2_5 Depth=1
	addq	%r11, %rdi
	cmpq	%r13, %rsi
	je	.LBB2_20
	.p2align	4, 0x90
.LBB2_18:                               # %.lr.ph.us
                                        #   Parent Loop BB2_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbp,%rdi), %ecx
	addb	16(%rsp,%rsi), %cl
	movb	%cl, (%rbp,%rdi)
	movb	%cl, 16(%rsp,%rsi)
	incq	%rdi
	incq	%rsi
	cmpq	%rax, %rsi
	jae	.LBB2_20
# BB#19:                                # %.lr.ph.us
                                        #   in Loop: Header=BB2_18 Depth=2
	cmpq	%rbx, %rdi
	jb	.LBB2_18
.LBB2_20:                               # %.critedge.loopexit.us
                                        #   in Loop: Header=BB2_5 Depth=1
	cmpq	%rbx, %rdi
	jb	.LBB2_5
.LBB2_21:                               # %.critedge._crit_edge
	cmpl	%r12d, %esi
	cmovnel	%esi, %r15d
	movl	%r12d, %eax
	subl	%r15d, %eax
	je	.LBB2_23
# BB#22:                                # %.lr.ph.preheader.i38
	leaq	16(%rsp,%r15), %rsi
	movl	%eax, %edx
	movq	%r14, %rdi
	callq	memcpy
.LBB2_23:                               # %MyMemCpy.exit43
	testl	%r15d, %r15d
	je	.LBB2_25
# BB#24:                                # %.lr.ph.i35.preheader
	movl	%r12d, %eax
	addq	%rax, %r14
	subq	%r15, %r14
	leaq	16(%rsp), %rsi
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	memcpy
.LBB2_25:                               # %MyMemCpy.exit36
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.p2align	4, 0x90
.LBB2_26:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	jmp	.LBB2_26
.Lfunc_end2:
	.size	Delta_Decode, .Lfunc_end2-Delta_Decode
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
