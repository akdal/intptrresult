	.text
	.file	"z15.bc"
	.globl	MinConstraint
	.p2align	4, 0x90
	.type	MinConstraint,@function
MinConstraint:                          # @MinConstraint
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	cmpl	(%rsi), %eax
	movq	%rsi, %rax
	cmovlq	%rdi, %rax
	movl	(%rax), %eax
	movl	%eax, (%rdi)
	movl	4(%rdi), %eax
	cmpl	4(%rsi), %eax
	movq	%rsi, %rax
	cmovlq	%rdi, %rax
	movl	4(%rax), %eax
	movl	%eax, 4(%rdi)
	movl	8(%rdi), %eax
	cmpl	8(%rsi), %eax
	cmovlq	%rdi, %rsi
	movl	8(%rsi), %eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end0:
	.size	MinConstraint, .Lfunc_end0-MinConstraint
	.cfi_endproc

	.globl	SetSizeToMaxForwardConstraint
	.p2align	4, 0x90
	.type	SetSizeToMaxForwardConstraint,@function
SetSizeToMaxForwardConstraint:          # @SetSizeToMaxForwardConstraint
	.cfi_startproc
# BB#0:
	movl	4(%rdx), %eax
	movl	8(%rdx), %ecx
	cmpl	%ecx, %eax
	cmovlel	%eax, %ecx
	movl	%ecx, (%rsi)
	movl	(%rdx), %eax
	movl	4(%rdx), %edx
	subl	%ecx, %edx
	cmpl	%edx, %eax
	cmovlel	%eax, %edx
	movl	%edx, (%rdi)
	retq
.Lfunc_end1:
	.size	SetSizeToMaxForwardConstraint, .Lfunc_end1-SetSizeToMaxForwardConstraint
	.cfi_endproc

	.globl	EnlargeToConstraint
	.p2align	4, 0x90
	.type	EnlargeToConstraint,@function
EnlargeToConstraint:                    # @EnlargeToConstraint
	.cfi_startproc
# BB#0:
	movl	4(%rdx), %eax
	movl	8(%rdx), %ecx
	subl	(%rdi), %eax
	cmpl	%ecx, %eax
	cmovgl	%ecx, %eax
	movl	%eax, (%rsi)
	retq
.Lfunc_end2:
	.size	EnlargeToConstraint, .Lfunc_end2-EnlargeToConstraint
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	1065353216              # float 1
.LCPI3_1:
	.long	1124073472              # float 128
	.text
	.globl	ScaleToConstraint
	.p2align	4, 0x90
	.type	ScaleToConstraint,@function
ScaleToConstraint:                      # @ScaleToConstraint
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	jle	.LBB3_1
# BB#2:
	cvtsi2ssl	(%rdx), %xmm1
	cvtsi2ssl	%edi, %xmm0
	divss	%xmm0, %xmm1
	movss	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	minss	%xmm1, %xmm0
	addl	%esi, %edi
	jg	.LBB3_4
	jmp	.LBB3_5
.LBB3_1:
	movss	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	addl	%esi, %edi
	jle	.LBB3_5
.LBB3_4:
	xorps	%xmm1, %xmm1
	cvtsi2ssl	4(%rdx), %xmm1
	cvtsi2ssl	%edi, %xmm2
	divss	%xmm2, %xmm1
	minss	%xmm1, %xmm0
.LBB3_5:
	testl	%esi, %esi
	jle	.LBB3_7
# BB#6:
	xorps	%xmm1, %xmm1
	cvtsi2ssl	8(%rdx), %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%esi, %xmm2
	divss	%xmm2, %xmm1
	minss	%xmm1, %xmm0
.LBB3_7:
	mulss	.LCPI3_1(%rip), %xmm0
	cvttss2si	%xmm0, %eax
	retq
.Lfunc_end3:
	.size	ScaleToConstraint, .Lfunc_end3-ScaleToConstraint
	.cfi_endproc

	.globl	InvScaleConstraint
	.p2align	4, 0x90
	.type	InvScaleConstraint,@function
InvScaleConstraint:                     # @InvScaleConstraint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movl	%esi, %r14d
	movq	%rdi, %rbx
	testl	%r14d, %r14d
	jg	.LBB4_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_2:
	movl	(%rbp), %eax
	movl	$8388607, %ecx          # imm = 0x7FFFFF
	cmpl	$8388607, %eax          # imm = 0x7FFFFF
	movl	$8388607, %edx          # imm = 0x7FFFFF
	je	.LBB4_4
# BB#3:
	shll	$7, %eax
	cltd
	idivl	%r14d
	cmpl	$8388608, %eax          # imm = 0x800000
	movl	$8388607, %edx          # imm = 0x7FFFFF
	cmovll	%eax, %edx
.LBB4_4:
	movl	%edx, (%rbx)
	movl	4(%rbp), %eax
	cmpl	$8388607, %eax          # imm = 0x7FFFFF
	je	.LBB4_6
# BB#5:
	shll	$7, %eax
	cltd
	idivl	%r14d
	cmpl	$8388608, %eax          # imm = 0x800000
	movl	$8388607, %ecx          # imm = 0x7FFFFF
	cmovll	%eax, %ecx
.LBB4_6:
	movl	%ecx, 4(%rbx)
	movl	8(%rbp), %eax
	movl	$8388607, %ecx          # imm = 0x7FFFFF
	cmpl	$8388607, %eax          # imm = 0x7FFFFF
	je	.LBB4_8
# BB#7:
	shll	$7, %eax
	cltd
	idivl	%r14d
	cmpl	$8388608, %eax          # imm = 0x800000
	movl	$8388607, %ecx          # imm = 0x7FFFFF
	cmovll	%eax, %ecx
.LBB4_8:
	movl	%ecx, 8(%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end4:
	.size	InvScaleConstraint, .Lfunc_end4-InvScaleConstraint
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4614256656552045848     # double 3.1415926535897931
.LCPI5_1:
	.quad	4676566000559194112     # double 46080
.LCPI5_2:
	.quad	4618760256179416344     # double 6.2831853071795862
.LCPI5_3:
	.quad	-4604611780675359464    # double -6.2831853071795862
.LCPI5_4:
	.quad	4609753056924675352     # double 1.5707963267948966
.LCPI5_5:
	.quad	-4613618979930100456    # double -1.5707963267948966
.LCPI5_6:
	.quad	4616991696741409234     # double 4.7123889803846897
.LCPI5_7:
	.quad	-4609115380302729960    # double -3.1415926535897931
.LCPI5_8:
	.quad	-4606380340113366574    # double -4.7123889803846897
.LCPI5_10:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_9:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_11:
	.long	1258291198              # float 8388607
	.text
	.globl	RotateConstraint
	.p2align	4, 0x90
	.type	RotateConstraint,@function
RotateConstraint:                       # @RotateConstraint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 160
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %r14
	movq	%rsi, %r13
	movq	%rdi, %r12
	cvtsi2ssl	%edx, %xmm0
	addss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI5_0(%rip), %xmm0
	divsd	.LCPI5_1(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm2
	xorpd	%xmm0, %xmm0
	ucomiss	%xmm2, %xmm0
	jbe	.LBB5_3
# BB#1:
	movsd	.LCPI5_2(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph146
                                        # =>This Inner Loop Header: Depth=1
	cvtss2sd	%xmm2, %xmm2
	addsd	%xmm1, %xmm2
	cvtsd2ss	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm0
	ja	.LBB5_2
.LBB5_3:                                # %.preheader
	cvtss2sd	%xmm2, %xmm4
	ucomisd	.LCPI5_2(%rip), %xmm4
	jb	.LBB5_6
# BB#4:
	movsd	.LCPI5_3(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI5_2(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	addsd	%xmm0, %xmm4
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm4, %xmm2
	xorps	%xmm4, %xmm4
	cvtss2sd	%xmm2, %xmm4
	ucomisd	%xmm1, %xmm4
	jae	.LBB5_5
.LBB5_6:                                # %._crit_edge
	xorpd	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm2
	jb	.LBB5_8
# BB#7:                                 # %._crit_edge
	movsd	.LCPI5_2(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm4, %xmm0
	jae	.LBB5_9
.LBB5_8:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	%r9d, %ebx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	movsd	%xmm4, 32(%rsp)         # 8-byte Spill
	callq	Error
	movl	%ebx, %r9d
	movsd	32(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
.LBB5_9:
	movsd	.LCPI5_4(%rip), %xmm3   # xmm3 = mem[0],zero
	ucomisd	%xmm4, %xmm3
	jae	.LBB5_10
# BB#11:
	movsd	.LCPI5_0(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm4, %xmm1
	jae	.LBB5_12
# BB#13:
	movsd	.LCPI5_6(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm4, %xmm1
	jae	.LBB5_14
# BB#15:
	addsd	.LCPI5_8(%rip), %xmm4
	movq	%r15, %rcx
	movq	%r15, %rax
	leaq	8(%r15), %rdx
	movq	%r14, %r15
	jmp	.LBB5_16
.LBB5_10:
	leaq	4(%r14), %rax
	movq	%r14, %rdx
	addq	$8, %rdx
	leaq	4(%r15), %rsi
	movq	%r15, %rdi
	addq	$8, %r15
	movq	%r14, %rcx
	jmp	.LBB5_17
.LBB5_12:
	addsd	.LCPI5_5(%rip), %xmm4
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm4, %xmm2
	leaq	4(%r15), %rax
	leaq	8(%r15), %rcx
	leaq	4(%r14), %rsi
	movq	%r15, %rdx
	movq	%r14, %r15
	addq	$8, %r15
	movq	%r14, %rdi
	jmp	.LBB5_17
.LBB5_14:
	addsd	.LCPI5_7(%rip), %xmm4
	leaq	8(%r14), %rcx
	movq	%r14, %rax
	movq	%r14, %rdx
.LBB5_16:
	addq	$4, %rax
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm4, %xmm2
	leaq	8(%r15), %rdi
	leaq	4(%r15), %rsi
.LBB5_17:
	movl	(%rax), %r14d
	movl	(%rcx), %ebp
	movl	(%rdx), %ebx
	movl	(%rsi), %eax
	movl	(%rdi), %ecx
	movl	(%r15), %edx
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm2, %xmm0
	subsd	%xmm0, %xmm3
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm3, %xmm1
	testl	%r9d, %r9d
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	%ebx, 48(%rsp)          # 4-byte Spill
	je	.LBB5_18
# BB#22:
	movl	48(%r13), %r15d
	movl	56(%r13), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	callq	cos
	cvtsd2ss	%xmm0, %xmm0
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movaps	64(%rsp), %xmm4         # 16-byte Reload
	movaps	.LCPI5_9(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	andps	%xmm4, %xmm1
	cvtss2sd	%xmm1, %xmm1
	movq	%r13, %rdi
	movl	$8388607, %ebx          # imm = 0x7FFFFF
	movsd	.LCPI5_10(%rip), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	movl	$8388607, %eax          # imm = 0x7FFFFF
	movl	$8388607, %ecx          # imm = 0x7FFFFF
	movl	$8388607, %edx          # imm = 0x7FFFFF
	ja	.LBB5_24
# BB#23:
	cvtsd2ss	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ebp, %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%r15d, %xmm2
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm1
	divss	%xmm4, %xmm1
	movss	.LCPI5_11(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm1
	cvttss2si	%xmm1, %eax
	movl	$8388607, %esi          # imm = 0x7FFFFF
	cmoval	%esi, %eax
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%r14d, %xmm1
	subss	%xmm2, %xmm1
	movl	20(%rsp), %ecx          # 4-byte Reload
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ecx, %xmm2
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm1
	divss	%xmm4, %xmm1
	ucomiss	%xmm3, %xmm1
	cvttss2si	%xmm1, %ecx
	cmoval	%esi, %ecx
	movl	48(%rsp), %edx          # 4-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	subss	%xmm2, %xmm0
	divss	%xmm4, %xmm0
	ucomiss	%xmm3, %xmm0
	cvttss2si	%xmm0, %edx
	cmoval	%esi, %edx
.LBB5_24:                               # %SemiRotateConstraint.exit85
	movl	%eax, (%r12)
	leaq	4(%r12), %r14
	movl	%ecx, 4(%r12)
	leaq	8(%r12), %r15
	movl	%edx, 8(%r12)
	movl	48(%rdi), %r13d
	movl	56(%rdi), %ebp
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	cos
	cvtsd2ss	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movaps	48(%rsp), %xmm4         # 16-byte Reload
	movaps	.LCPI5_9(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	andps	%xmm4, %xmm1
	cvtss2sd	%xmm1, %xmm1
	movsd	.LCPI5_10(%rip), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	ja	.LBB5_25
# BB#26:
	cvtsd2ss	%xmm0, %xmm0
	movl	28(%rsp), %eax          # 4-byte Reload
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ebp, %xmm2
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm1
	divss	%xmm4, %xmm1
	movss	.LCPI5_11(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm1
	cvttss2si	%xmm1, %ebx
	movl	$8388607, %edx          # imm = 0x7FFFFF
	cmoval	%edx, %ebx
	movl	32(%rsp), %eax          # 4-byte Reload
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	subss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%r13d, %xmm2
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm1
	divss	%xmm4, %xmm1
	ucomiss	%xmm3, %xmm1
	cvttss2si	%xmm1, %eax
	cmoval	%edx, %eax
	movl	24(%rsp), %ecx          # 4-byte Reload
	jmp	.LBB5_27
.LBB5_18:
	movl	%ebp, %r15d
	movss	%xmm1, 40(%rsp)         # 4-byte Spill
	movl	52(%r13), %ebp
	movl	60(%r13), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	callq	cos
	cvtsd2ss	%xmm0, %xmm0
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movaps	64(%rsp), %xmm4         # 16-byte Reload
	movaps	.LCPI5_9(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	andps	%xmm4, %xmm1
	cvtss2sd	%xmm1, %xmm1
	movq	%r13, %rdi
	movl	$8388607, %ebx          # imm = 0x7FFFFF
	movsd	.LCPI5_10(%rip), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	movl	$8388607, %eax          # imm = 0x7FFFFF
	movl	$8388607, %ecx          # imm = 0x7FFFFF
	movl	$8388607, %edx          # imm = 0x7FFFFF
	ja	.LBB5_20
# BB#19:
	cvtsd2ss	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%r15d, %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ebp, %xmm2
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm1
	divss	%xmm4, %xmm1
	movss	.LCPI5_11(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm1
	cvttss2si	%xmm1, %eax
	movl	$8388607, %esi          # imm = 0x7FFFFF
	cmoval	%esi, %eax
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%r14d, %xmm1
	subss	%xmm2, %xmm1
	movl	20(%rsp), %ecx          # 4-byte Reload
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ecx, %xmm2
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm1
	divss	%xmm4, %xmm1
	ucomiss	%xmm3, %xmm1
	cvttss2si	%xmm1, %ecx
	cmoval	%esi, %ecx
	movl	48(%rsp), %edx          # 4-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	subss	%xmm2, %xmm0
	divss	%xmm4, %xmm0
	ucomiss	%xmm3, %xmm0
	cvttss2si	%xmm0, %edx
	cmoval	%esi, %edx
.LBB5_20:                               # %SemiRotateConstraint.exit
	movl	%eax, (%r12)
	leaq	4(%r12), %r14
	movl	%ecx, 4(%r12)
	leaq	8(%r12), %r15
	movl	%edx, 8(%r12)
	movl	52(%rdi), %r13d
	movl	60(%rdi), %ebp
	movss	40(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	cos
	cvtsd2ss	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movaps	48(%rsp), %xmm4         # 16-byte Reload
	movaps	.LCPI5_9(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	andps	%xmm4, %xmm1
	cvtss2sd	%xmm1, %xmm1
	movsd	.LCPI5_10(%rip), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	jbe	.LBB5_21
.LBB5_25:
	movl	$8388607, %eax          # imm = 0x7FFFFF
	movl	$8388607, %ecx          # imm = 0x7FFFFF
	jmp	.LBB5_28
.LBB5_21:
	cvtsd2ss	%xmm0, %xmm0
	movl	24(%rsp), %eax          # 4-byte Reload
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ebp, %xmm2
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm1
	divss	%xmm4, %xmm1
	movss	.LCPI5_11(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm1
	cvttss2si	%xmm1, %ebx
	movl	$8388607, %edx          # imm = 0x7FFFFF
	cmoval	%edx, %ebx
	movl	32(%rsp), %eax          # 4-byte Reload
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	subss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%r13d, %xmm2
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm1
	divss	%xmm4, %xmm1
	ucomiss	%xmm3, %xmm1
	cvttss2si	%xmm1, %eax
	cmoval	%edx, %eax
	movl	28(%rsp), %ecx          # 4-byte Reload
.LBB5_27:                               # %SemiRotateConstraint.exit93
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	subss	%xmm2, %xmm0
	divss	%xmm4, %xmm0
	ucomiss	%xmm3, %xmm0
	cvttss2si	%xmm0, %ecx
	cmoval	%edx, %ecx
.LBB5_28:                               # %SemiRotateConstraint.exit93
	movl	%ebx, 88(%rsp)
	movl	%eax, 92(%rsp)
	movl	%ecx, 96(%rsp)
	cmpl	%ebx, (%r12)
	leaq	88(%rsp), %rdx
	movq	%rdx, %rsi
	cmovlq	%r12, %rsi
	movl	(%rsi), %esi
	movl	%esi, (%r12)
	cmpl	%eax, (%r14)
	movq	%rdx, %rax
	cmovlq	%r12, %rax
	movl	4(%rax), %eax
	movl	%eax, (%r14)
	cmpl	%ecx, (%r15)
	cmovlq	%r12, %rdx
	movl	8(%rdx), %eax
	movl	%eax, (%r15)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	RotateConstraint, .Lfunc_end5-RotateConstraint
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_0:
	.long	1065353216              # float 1
.LCPI6_1:
	.long	1124073472              # float 128
	.text
	.globl	InsertScale
	.p2align	4, 0x90
	.type	InsertScale,@function
InsertScale:                            # @InsertScale
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 48
.Lcfi24:
	.cfi_offset %rbx, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	48(%r14), %ecx
	movl	56(%r14), %eax
	testl	%ecx, %ecx
	jle	.LBB6_1
# BB#2:
	cvtsi2ssl	(%r15), %xmm1
	cvtsi2ssl	%ecx, %xmm0
	divss	%xmm0, %xmm1
	movss	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	minss	%xmm1, %xmm0
	addl	%eax, %ecx
	jg	.LBB6_4
	jmp	.LBB6_5
.LBB6_1:
	movss	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	addl	%eax, %ecx
	jle	.LBB6_5
.LBB6_4:
	xorps	%xmm1, %xmm1
	cvtsi2ssl	4(%r15), %xmm1
	cvtsi2ssl	%ecx, %xmm2
	divss	%xmm2, %xmm1
	minss	%xmm1, %xmm0
.LBB6_5:
	testl	%eax, %eax
	jle	.LBB6_7
# BB#6:
	xorps	%xmm1, %xmm1
	cvtsi2ssl	8(%r15), %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%eax, %xmm2
	divss	%xmm2, %xmm1
	minss	%xmm1, %xmm0
.LBB6_7:                                # %ScaleToConstraint.exit
	mulss	.LCPI6_1(%rip), %xmm0
	cvttss2si	%xmm0, %ebp
	xorl	%ecx, %ecx
	cmpl	$26, %ebp
	jl	.LBB6_23
# BB#8:
	movzbl	zz_lengths+34(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB6_9
# BB#10:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB6_11
.LBB6_9:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB6_11:
	movb	$34, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movl	$1610612736, %eax       # imm = 0x60000000
	andl	40(%r14), %eax
	movl	$-1610612737, %ecx      # imm = 0x9FFFFFFF
	andl	40(%rbx), %ecx
	orl	%eax, %ecx
	movl	%ecx, 40(%rbx)
	movzwl	34(%r14), %eax
	movw	%ax, 34(%rbx)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r14), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%rbx), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%rbx)
	andl	36(%r14), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
	movl	%ebp, 64(%rbx)
	imull	48(%r14), %ebp
	movl	%ebp, %eax
	sarl	$31, %eax
	shrl	$25, %eax
	addl	%ebp, %eax
	sarl	$7, %eax
	movl	%eax, 48(%rbx)
	movl	4(%r15), %ecx
	movl	8(%r15), %edx
	subl	%eax, %ecx
	cmpl	%edx, %ecx
	cmovgl	%edx, %ecx
	movl	%ecx, 56(%rbx)
	movl	$128, 72(%rbx)
	movl	52(%r14), %eax
	movl	%eax, 52(%rbx)
	movl	60(%r14), %eax
	movl	%eax, 60(%rbx)
	movq	%r14, zz_hold(%rip)
	movq	24(%r14), %rax
	cmpq	%r14, %rax
	je	.LBB6_12
# BB#13:
	movq	16(%r14), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r14), %rcx
	movq	%rax, 24(%rcx)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	sete	%bpl
	je	.LBB6_16
# BB#14:
	testq	%rax, %rax
	je	.LBB6_16
# BB#15:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB6_16
.LBB6_12:                               # %.thread
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	testq	%rbx, %rbx
	sete	%bpl
.LBB6_16:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB6_17
# BB#18:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB6_19
.LBB6_17:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB6_19:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	sete	%cl
	orb	%cl, %bpl
	jne	.LBB6_21
# BB#20:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB6_21:
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	movl	$1, %ecx
	testq	%rax, %rax
	je	.LBB6_23
# BB#22:
	movq	16(%r14), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%r14)
	movq	16(%rax), %rsi
	movq	%r14, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB6_23:
	movl	%ecx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	InsertScale, .Lfunc_end6-InsertScale
	.cfi_endproc

	.globl	Constrained
	.p2align	4, 0x90
	.type	Constrained,@function
Constrained:                            # @Constrained
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 160
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movl	%ebp, %edi
	callq	SetLengthDim
	cmpq	%r12, 24(%r12)
	jne	.LBB7_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_2:
	cmpb	$2, 32(%r12)
	jne	.LBB7_6
# BB#3:
	cmpl	$1, %ebp
	setne	%cl
	movzwl	42(%r12), %eax
	testb	$16, %al
	sete	%dl
	orb	%cl, %dl
	cmpb	$1, %dl
	jne	.LBB7_5
# BB#4:
	andl	$8, %eax
	testw	%ax, %ax
	jne	.LBB7_5
.LBB7_6:
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	leaq	24(%r12), %rax
	leaq	16(%r12), %r13
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	testl	%ebp, %ebp
	cmoveq	%rax, %r13
	movq	(%r13), %r15
	leaq	8(%r15), %r14
	xorl	%ebp, %ebp
	movl	$1, %eax
	movq	%r14, %rbx
	jmp	.LBB7_7
	.p2align	4, 0x90
.LBB7_13:                               # %.loopexit323.loopexit
                                        #   in Loop: Header=BB7_7 Depth=1
	addq	$8, %rbx
.LBB7_7:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_10 Depth 2
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %edi
	cmpq	$99, %rdi
	ja	.LBB7_150
# BB#8:                                 #   in Loop: Header=BB7_7 Depth=1
	jmpq	*.LJTI7_0(,%rdi,8)
.LBB7_9:                                # %.preheader322.preheader
                                        #   in Loop: Header=BB7_7 Depth=1
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB7_10:                               # %.preheader322
                                        #   Parent Loop BB7_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB7_10
# BB#11:                                # %.preheader322
                                        #   in Loop: Header=BB7_7 Depth=1
	cmpb	$1, %dl
	jne	.LBB7_13
# BB#12:                                #   in Loop: Header=BB7_7 Depth=1
	testb	$1, 45(%rcx)
	cmovnel	%eax, %ebp
	jmp	.LBB7_13
.LBB7_14:
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
.LBB7_18:
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	Constrained
	jmp	.LBB7_151
.LBB7_5:
	movabsq	$36028792732385279, %rax # imm = 0x7FFFFF007FFFFF
	movq	%rax, (%rbx)
	movl	$8388607, 8(%rbx)       # imm = 0x7FFFFF
.LBB7_151:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_65:
	cmpb	$19, %dil
	sete	%al
	movl	12(%rsp), %edx          # 4-byte Reload
	cmpl	$1, %edx
	sete	%cl
	leaq	32(%rsp), %rsi
	movq	%rbx, %rdi
	xorb	%al, %cl
	je	.LBB7_66
# BB#113:
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	Constrained
	movl	32(%rsp), %ebp
	cmpl	$8388607, %ebp          # imm = 0x7FFFFF
	movq	16(%rsp), %r8           # 8-byte Reload
	jne	.LBB7_116
# BB#114:
	movl	$8388607, %ebp          # imm = 0x7FFFFF
	cmpl	$8388607, 36(%rsp)      # imm = 0x7FFFFF
	jne	.LBB7_116
# BB#115:
	movl	$8388607, %ebp          # imm = 0x7FFFFF
	cmpl	$8388607, 40(%rsp)      # imm = 0x7FFFFF
	movl	$8388607, %ecx          # imm = 0x7FFFFF
	movl	$8388607, %esi          # imm = 0x7FFFFF
	jne	.LBB7_116
	jmp	.LBB7_149
.LBB7_52:
	movl	64(%rbx), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movl	68(%rbx), %eax
	movl	%eax, 4(%rcx)
	movl	72(%rbx), %eax
	movl	%eax, 8(%rcx)
	jmp	.LBB7_151
.LBB7_59:
	movl	12(%rsp), %ebp          # 4-byte Reload
	testl	%ebp, %ebp
	sete	%al
	cmpb	$16, %dil
	sete	%cl
	cmpb	%cl, %al
	je	.LBB7_61
# BB#60:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.5, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_61:
	leaq	32(%rsp), %rsi
	movq	%rbx, %rdi
	movl	%ebp, %edx
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	Constrained
	movl	36(%rsp), %eax
	cmpl	$8388607, %eax          # imm = 0x7FFFFF
	jne	.LBB7_63
# BB#62:                                # %.thread
	movl	32(%rsp), %ecx
	cmpl	$8388608, %ecx          # imm = 0x800000
	movl	$8388607, %edx          # imm = 0x7FFFFF
	cmovgel	%edx, %ecx
	jmp	.LBB7_64
.LBB7_32:
	movl	12(%rsp), %edx          # 4-byte Reload
	testl	%edx, %edx
	sete	%r14b
	movq	%rbx, %rdi
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rsi
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rcx
	callq	Constrained
	cmpb	$26, 32(%rbx)
	sete	%al
	xorb	%r14b, %al
	jne	.LBB7_151
# BB#33:
	leaq	64(%rbx), %rax
	movl	(%rbp), %ecx
	cmpl	64(%rbx), %ecx
	movq	%rax, %rcx
	cmovlq	%rbp, %rcx
	movl	(%rcx), %ecx
	movl	%ecx, (%rbp)
	movl	4(%rbp), %ecx
	cmpl	68(%rbx), %ecx
	movq	%rax, %rcx
	cmovlq	%rbp, %rcx
	movl	4(%rcx), %ecx
	movl	%ecx, 4(%rbp)
	movl	8(%rbp), %ecx
	cmpl	72(%rbx), %ecx
	cmovlq	%rbp, %rax
	movl	8(%rax), %eax
	movl	%eax, 8(%rbp)
	movq	%rbx, (%r15)
	jmp	.LBB7_151
.LBB7_53:
	movl	12(%rsp), %ebp          # 4-byte Reload
	testl	%ebp, %ebp
	sete	%al
	cmpb	$28, %dil
	sete	%cl
	xorb	%al, %cl
	je	.LBB7_54
# BB#55:
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%ebp, %edx
	jmp	.LBB7_18
.LBB7_15:
	movl	12(%rsp), %edx          # 4-byte Reload
	testl	%edx, %edx
	sete	%al
	cmpb	$30, %dil
	jmp	.LBB7_16
.LBB7_19:
	movl	12(%rsp), %edx          # 4-byte Reload
	testl	%edx, %edx
	sete	%al
	cmpb	$32, %dil
.LBB7_16:
	sete	%cl
	cmpb	%cl, %al
	jne	.LBB7_17
.LBB7_57:
	movabsq	$36028792732385279, %rax # imm = 0x7FFFFF007FFFFF
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	movl	$8388607, 8(%rcx)       # imm = 0x7FFFFF
	jmp	.LBB7_151
.LBB7_34:
	movl	12(%rsp), %edx          # 4-byte Reload
	testl	%edx, %edx
	sete	%al
	cmpb	$38, %dil
	sete	%cl
	xorb	%al, %cl
	je	.LBB7_35
.LBB7_17:
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB7_18
.LBB7_66:
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	Constrained
	cmpl	$8388607, 32(%rsp)      # imm = 0x7FFFFF
	jne	.LBB7_69
# BB#67:
	cmpl	$8388607, 36(%rsp)      # imm = 0x7FFFFF
	jne	.LBB7_69
# BB#68:
	movl	$8388607, %edx          # imm = 0x7FFFFF
	cmpl	$8388607, 40(%rsp)      # imm = 0x7FFFFF
	movl	$8388607, %edi          # imm = 0x7FFFFF
	movl	$8388607, %eax          # imm = 0x7FFFFF
	je	.LBB7_112
.LBB7_69:
	movq	(%r13), %rdi
	leaq	68(%rsp), %rax
	movq	%rax, (%rsp)
	leaq	88(%rsp), %rdx
	leaq	56(%rsp), %rcx
	leaq	72(%rsp), %r8
	leaq	48(%rsp), %r9
	movl	%ebp, %esi
	callq	SetNeighbours
	movq	88(%rsp), %rdx
	xorl	%r14d, %r14d
	testq	%rdx, %rdx
	movl	$0, %r15d
	movl	12(%rsp), %ebp          # 4-byte Reload
	je	.LBB7_71
# BB#70:
	movq	56(%rsp), %rax
	movslq	%ebp, %rcx
	movl	56(%rax,%rcx,4), %edi
	addq	$44, %rdx
	xorl	%esi, %esi
	movl	$151, %ecx
	callq	ExtraGap
	movl	%eax, %r15d
.LBB7_71:
	movq	72(%rsp), %rdx
	testq	%rdx, %rdx
	je	.LBB7_73
# BB#72:
	movq	48(%rsp), %rax
	movslq	%ebp, %rcx
	movl	48(%rax,%rcx,4), %esi
	addq	$44, %rdx
	xorl	%edi, %edi
	movl	$153, %ecx
	callq	ExtraGap
	movl	%eax, %r14d
.LBB7_73:
	movb	32(%r12), %al
	addb	$-2, %al
	movq	88(%rsp), %rcx
	cmpb	$6, %al
	movl	%r14d, 24(%rsp)         # 4-byte Spill
	ja	.LBB7_90
# BB#74:
	xorl	%r14d, %r14d
	testq	%rcx, %rcx
	movl	$0, %r12d
	je	.LBB7_76
# BB#75:
	movq	56(%rsp), %rax
	movslq	%ebp, %rdx
	movl	56(%rax,%rdx,4), %edi
	addq	$44, %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	MinGap
	movl	%eax, %r12d
.LBB7_76:
	movq	72(%rsp), %rcx
	testq	%rcx, %rcx
	je	.LBB7_77
# BB#78:
	movq	48(%rsp), %rax
	movslq	%ebp, %rdx
	movl	48(%rax,%rdx,4), %esi
	movl	56(%rax,%rdx,4), %edx
	addq	$44, %rcx
	xorl	%edi, %edi
	callq	MinGap
	movl	%eax, %r14d
	movq	72(%rsp), %rcx
	testq	%rcx, %rcx
	sete	%dl
	cmpq	$0, 88(%rsp)
	jne	.LBB7_82
	jmp	.LBB7_80
.LBB7_150:
	movq	no_fpos(%rip), %rbx
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	Image
	movq	%rax, (%rsp)
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.6, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	jmp	.LBB7_151
.LBB7_56:
	cmpl	$1, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB7_57
# BB#58:
	movl	64(%rbx), %ebp
	movl	%ebp, 32(%rsp)
	movl	68(%rbx), %eax
	movl	%eax, 36(%rsp)
	movl	72(%rbx), %eax
	movl	%eax, 40(%rsp)
	movq	16(%rsp), %r8           # 8-byte Reload
.LBB7_116:
	movq	(%r15), %rdx
	movb	$1, %sil
	cmpq	%rbx, %rdx
	jne	.LBB7_130
	jmp	.LBB7_118
	.p2align	4, 0x90
.LBB7_135:                              # %.loopexit321
                                        #   in Loop: Header=BB7_130 Depth=1
	movq	(%rdx), %rdx
	cmpq	%rbx, %rdx
	je	.LBB7_118
.LBB7_130:                              # %.preheader320
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_131 Depth 2
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB7_131:                              #   Parent Loop BB7_130 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %eax
	testb	%al, %al
	je	.LBB7_131
# BB#132:                               #   in Loop: Header=BB7_130 Depth=1
	cmpb	$1, %al
	jne	.LBB7_135
# BB#133:                               #   in Loop: Header=BB7_130 Depth=1
	testb	$2, 45(%rcx)
	jne	.LBB7_135
# BB#134:
	xorl	%esi, %esi
	jmp	.LBB7_119
.LBB7_118:
	movq	%rbx, %rdx
.LBB7_119:                              # %.preheader319
	movq	(%r14), %rcx
	cmpq	%rbx, %rcx
	jne	.LBB7_121
	jmp	.LBB7_126
	.p2align	4, 0x90
.LBB7_125:                              # %.loopexit318
                                        #   in Loop: Header=BB7_121 Depth=1
	movq	8(%rcx), %rcx
	cmpq	%rbx, %rcx
	je	.LBB7_126
.LBB7_121:                              # %.preheader317
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_122 Depth 2
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB7_122:                              #   Parent Loop BB7_121 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	movzbl	32(%rdi), %eax
	testb	%al, %al
	je	.LBB7_122
# BB#123:                               #   in Loop: Header=BB7_121 Depth=1
	cmpb	$1, %al
	jne	.LBB7_125
# BB#124:                               #   in Loop: Header=BB7_121 Depth=1
	testb	$2, 45(%rdi)
	jne	.LBB7_125
	jmp	.LBB7_128
.LBB7_126:                              # %._crit_edge423
	testb	%sil, %sil
	je	.LBB7_127
# BB#136:
	cmpb	$8, 32(%rbx)
	jne	.LBB7_138
# BB#137:
	testb	$32, 42(%rbx)
	jne	.LBB7_127
.LBB7_138:
	movl	36(%rsp), %ecx
	cmpl	$8388607, %ecx          # imm = 0x7FFFFF
	jne	.LBB7_140
# BB#139:                               # %.thread312
	cmpl	$8388608, %ebp          # imm = 0x800000
	movl	$8388607, %esi          # imm = 0x7FFFFF
	cmovgel	%esi, %ebp
	jmp	.LBB7_141
.LBB7_20:
	leaq	32(%rsp), %rsi
	movq	%rbx, %rdi
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %edx
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	Constrained
	leaq	64(%rbx), %rax
	testl	%ebp, %ebp
	jne	.LBB7_22
# BB#21:
	cmpl	$0, (%rax)
	je	.LBB7_57
.LBB7_22:                               # %._crit_edge521
	addq	$72, %rbx
	testl	%ebp, %ebp
	cmoveq	%rax, %rbx
	movl	(%rbx), %ebx
	testl	%ebx, %ebx
	jg	.LBB7_24
# BB#23:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB7_24:
	movl	32(%rsp), %eax
	movl	$8388607, %ecx          # imm = 0x7FFFFF
	cmpl	$8388607, %eax          # imm = 0x7FFFFF
	movl	$8388607, %edx          # imm = 0x7FFFFF
	movq	16(%rsp), %rsi          # 8-byte Reload
	je	.LBB7_26
# BB#25:
	shll	$7, %eax
	cltd
	idivl	%ebx
	cmpl	$8388608, %eax          # imm = 0x800000
	movl	$8388607, %edx          # imm = 0x7FFFFF
	cmovll	%eax, %edx
.LBB7_26:
	movl	%edx, (%rsi)
	movl	36(%rsp), %eax
	cmpl	$8388607, %eax          # imm = 0x7FFFFF
	je	.LBB7_28
# BB#27:
	shll	$7, %eax
	cltd
	idivl	%ebx
	cmpl	$8388608, %eax          # imm = 0x800000
	movl	$8388607, %ecx          # imm = 0x7FFFFF
	cmovll	%eax, %ecx
.LBB7_28:
	movl	%ecx, 4(%rsi)
	movl	40(%rsp), %eax
	movl	$8388607, %ecx          # imm = 0x7FFFFF
	cmpl	$8388607, %eax          # imm = 0x7FFFFF
	je	.LBB7_30
# BB#29:
	shll	$7, %eax
	cltd
	idivl	%ebx
	cmpl	$8388608, %eax          # imm = 0x800000
	movl	$8388607, %ecx          # imm = 0x7FFFFF
	cmovll	%eax, %ecx
.LBB7_30:                               # %InvScaleConstraint.exit
	movl	%ecx, 8(%rsi)
	jmp	.LBB7_151
.LBB7_31:
	leaq	72(%rsp), %r15
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	callq	Constrained
	leaq	88(%rsp), %r14
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%rbp, %rcx
	callq	Constrained
	movl	76(%rbx), %edx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rsi
	movq	%r15, %rcx
	movq	%r14, %r8
	movl	12(%rsp), %r9d          # 4-byte Reload
	callq	RotateConstraint
	jmp	.LBB7_151
.LBB7_63:
	movslq	%ebp, %rsi
	movl	%eax, %ecx
	subl	56(%rbx,%rsi,4), %ecx
	movl	32(%rsp), %edx
	cmpl	%ecx, %edx
	cmovlel	%edx, %ecx
	movl	%eax, %edx
	subl	48(%rbx,%rsi,4), %edx
.LBB7_64:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	40(%rsp), %esi
	cmpl	%edx, %esi
	cmovlel	%esi, %edx
	movl	%ecx, (%rdi)
	movl	%eax, 4(%rdi)
	movl	%edx, 8(%rdi)
	jmp	.LBB7_151
.LBB7_54:
	leaq	32(%rsp), %rsi
	movq	%rbx, %rdi
	movl	%ebp, %edx
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	Constrained
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	%ebp, %edx
	callq	FindShift
	movl	32(%rsp), %ecx
	movl	36(%rsp), %edx
	cmpl	%edx, %ecx
	cmovgl	%edx, %ecx
	subl	%eax, %ecx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%ecx, (%rsi)
	movl	%edx, 4(%rsi)
	movl	40(%rsp), %ecx
	cmpl	%edx, %ecx
	cmovlel	%ecx, %edx
	addl	%eax, %edx
	movl	%edx, 8(%rsi)
	jmp	.LBB7_151
.LBB7_35:
	movslq	%edx, %rax
	movl	48(%rbx,%rax,4), %ecx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%ecx, (%rdi)
	movl	56(%rbx,%rax,4), %ecx
	addl	48(%rbx,%rax,4), %ecx
	testl	%edx, %edx
	movl	%ecx, 4(%rdi)
	movl	56(%rbx,%rax,4), %ecx
	movl	%ecx, 8(%rdi)
	movq	24(%rbx), %rcx
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB7_43
# BB#36:                                # %.lr.ph.preheader
	cmpq	%rbx, %rcx
	je	.LBB7_51
# BB#37:
	movabsq	$14224982114816, %rdx   # imm = 0xCF003018200
	movq	%rbx, %rcx
.LBB7_38:                               # %.lr.ph680
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_39 Depth 2
	movq	16(%rcx), %rcx
	.p2align	4, 0x90
.LBB7_39:                               #   Parent Loop BB7_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rcx
	movzbl	32(%rcx), %esi
	cmpq	$43, %rsi
	ja	.LBB7_51
# BB#40:                                #   in Loop: Header=BB7_39 Depth=2
	testq	%rsi, %rsi
	je	.LBB7_39
# BB#41:                                #   in Loop: Header=BB7_38 Depth=1
	btq	%rsi, %rdx
	jae	.LBB7_49
# BB#42:                                # %.sink.split.backedge
                                        #   in Loop: Header=BB7_38 Depth=1
	movl	48(%rcx,%rax,4), %esi
	movl	%esi, (%rdi)
	movl	56(%rcx,%rax,4), %esi
	addl	48(%rcx,%rax,4), %esi
	movl	%esi, 4(%rdi)
	movl	56(%rcx,%rax,4), %esi
	movl	%esi, 8(%rdi)
	cmpq	%rcx, 24(%rcx)
	jne	.LBB7_38
	jmp	.LBB7_51
.LBB7_127:
	movq	%rbx, %rcx
.LBB7_128:                              # %.preheader
	movl	12(%rsp), %esi          # 4-byte Reload
	movq	8(%rdx), %rax
	cmpq	%rcx, %rax
	je	.LBB7_129
# BB#142:                               # %.lr.ph410.lr.ph
	movslq	%esi, %rdi
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.LBB7_143
.LBB7_90:
	testq	%rcx, %rcx
	je	.LBB7_91
# BB#92:
	movq	56(%rsp), %rax
	movslq	%ebp, %rbp
	movl	56(%rax,%rbp,4), %edi
	movl	48(%r12,%rbp,4), %esi
	movl	56(%r12,%rbp,4), %edx
	addq	$56, %r12
	addq	$44, %rcx
	callq	MinGap
	movl	%eax, %r13d
	movq	56(%rsp), %rax
	movl	56(%rax,%rbp,4), %edi
	movq	88(%rsp), %rcx
	addq	$44, %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	MinGap
	subl	%eax, %r13d
	jmp	.LBB7_93
.LBB7_129:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.LBB7_148
.LBB7_153:                              # %.outer
                                        #   in Loop: Header=BB7_143 Depth=1
	movl	48(%rbp,%rdi,4), %ebx
	cmpl	%ebx, %edx
	cmovll	%ebx, %edx
	movl	56(%rbp,%rdi,4), %ebp
	cmpl	%ebp, %esi
	cmovll	%ebp, %esi
	jmp	.LBB7_147
	.p2align	4, 0x90
.LBB7_143:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_144 Depth 2
	leaq	16(%rax), %rbp
	jmp	.LBB7_144
	.p2align	4, 0x90
.LBB7_152:                              #   in Loop: Header=BB7_144 Depth=2
	addq	$16, %rbp
.LBB7_144:                              #   Parent Loop BB7_143 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %ebx
	testb	%bl, %bl
	je	.LBB7_152
# BB#145:                               #   in Loop: Header=BB7_143 Depth=1
	cmpb	$1, %bl
	je	.LBB7_147
# BB#146:                               #   in Loop: Header=BB7_143 Depth=1
	addb	$-119, %bl
	cmpb	$19, %bl
	ja	.LBB7_153
.LBB7_147:                              # %.backedge
                                        #   in Loop: Header=BB7_143 Depth=1
	movq	8(%rax), %rax
	cmpq	%rcx, %rax
	jne	.LBB7_143
.LBB7_148:                              # %.outer._crit_edge
	movl	36(%rsp), %eax
	movl	40(%rsp), %ecx
	cmpl	%ecx, %eax
	cmovlel	%eax, %ecx
	movl	%ecx, %ebp
	subl	%esi, %ebp
	movl	%ecx, %esi
	subl	%edx, %esi
	cmpl	$8388607, %ecx          # imm = 0x7FFFFF
	cmovel	%ecx, %ebp
	cmovel	%ecx, %esi
	jmp	.LBB7_149
.LBB7_43:                               # %.lr.ph.us.preheader
	cmpq	%rbx, %rcx
	je	.LBB7_51
# BB#44:                                # %.preheader530.preheader.preheader
	movabsq	$14224982114816, %rdx   # imm = 0xCF003018200
	.p2align	4, 0x90
.LBB7_46:                               # %.preheader530
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rcx
	movzbl	32(%rcx), %esi
	cmpq	$43, %rsi
	ja	.LBB7_51
# BB#47:                                # %.preheader530
                                        #   in Loop: Header=BB7_46 Depth=1
	testq	%rsi, %rsi
	je	.LBB7_46
# BB#48:                                # %.preheader530
                                        #   in Loop: Header=BB7_46 Depth=1
	btq	%rsi, %rdx
	jae	.LBB7_49
# BB#45:                                # %.sink.split.backedge.us
                                        #   in Loop: Header=BB7_46 Depth=1
	movl	48(%rcx,%rax,4), %esi
	movl	%esi, (%rdi)
	movl	56(%rcx,%rax,4), %esi
	addl	48(%rcx,%rax,4), %esi
	movl	%esi, 4(%rdi)
	movl	56(%rcx,%rax,4), %esi
	movl	%esi, 8(%rdi)
	movq	24(%rcx), %rsi
	cmpq	%rcx, %rsi
	movq	%rsi, %rcx
	jne	.LBB7_46
	jmp	.LBB7_51
.LBB7_49:                               # %.preheader530
	movl	$24576, %eax            # imm = 0x6000
	btq	%rsi, %rax
	jae	.LBB7_51
# BB#50:                                # %.sink.split.backedge.thread
	movl	64(%rcx), %eax
	movl	%eax, (%rdi)
	movl	68(%rcx), %eax
	movl	%eax, 4(%rdi)
	movl	72(%rcx), %eax
	movl	%eax, 8(%rdi)
.LBB7_51:                               # %.critedge
	movq	%rbx, (%rbp)
	jmp	.LBB7_151
.LBB7_140:
	movslq	12(%rsp), %rax          # 4-byte Folded Reload
	movl	%ecx, %edx
	subl	56(%rbx,%rax,4), %edx
	cmpl	%edx, %ebp
	cmovgl	%edx, %ebp
	movl	%ecx, %esi
	subl	48(%rbx,%rax,4), %esi
.LBB7_141:
	movl	40(%rsp), %eax
	cmpl	%esi, %eax
	cmovlel	%eax, %esi
.LBB7_149:
	movl	%ebp, (%r8)
	movl	%ecx, 4(%r8)
	movl	%esi, 8(%r8)
	jmp	.LBB7_151
.LBB7_77:
	movb	$1, %dl
	xorl	%ecx, %ecx
	cmpq	$0, 88(%rsp)
	je	.LBB7_80
.LBB7_82:
	movq	56(%rsp), %rax
	movslq	%ebp, %rdi
	movl	56(%rax,%rdi,4), %eax
	testb	%dl, %dl
	jne	.LBB7_84
# BB#83:
	movq	48(%rsp), %rdx
	movl	48(%rdx,%rdi,4), %esi
	movl	56(%rdx,%rdi,4), %edx
	addq	$44, %rcx
	movl	%eax, %edi
	callq	MinGap
	jmp	.LBB7_84
.LBB7_91:
	movslq	%ebp, %rbp
	movl	48(%r12,%rbp,4), %r13d
	addq	$56, %r12
.LBB7_93:
	movq	72(%rsp), %rcx
	testq	%rcx, %rcx
	movl	(%r12,%rbp,4), %r12d
	je	.LBB7_95
# BB#94:
	movq	48(%rsp), %rax
	movl	48(%rax,%rbp,4), %esi
	movl	56(%rax,%rbp,4), %edx
	addq	$44, %rcx
	movl	%r12d, %edi
	callq	MinGap
	movl	%eax, %r12d
	movq	48(%rsp), %rax
	movl	48(%rax,%rbp,4), %esi
	movl	56(%rax,%rbp,4), %edx
	movq	72(%rsp), %rcx
	addq	$44, %rcx
	xorl	%edi, %edi
	callq	MinGap
	subl	%eax, %r12d
.LBB7_95:
	movl	68(%rsp), %ecx
	cmpl	$153, %ecx
	je	.LBB7_100
# BB#96:
	cmpl	$152, %ecx
	je	.LBB7_99
# BB#97:
	cmpl	$151, %ecx
                                        # implicit-def: %EDI
                                        # implicit-def: %R14D
	jne	.LBB7_101
# BB#98:
	movl	48(%rbx,%rbp,4), %r14d
	addl	%r13d, %r12d
	subl	%r12d, %r14d
	movl	56(%rbx,%rbp,4), %edi
	movl	$151, %ecx
	jmp	.LBB7_101
.LBB7_80:
	xorl	%eax, %eax
	testb	%dl, %dl
	jne	.LBB7_84
# BB#81:
	movq	48(%rsp), %rax
	movslq	%ebp, %rcx
	movl	48(%rax,%rcx,4), %eax
.LBB7_84:
	movl	68(%rsp), %ecx
	cmpl	$153, %ecx
	je	.LBB7_89
# BB#85:
	cmpl	$152, %ecx
	je	.LBB7_88
# BB#86:
	cmpl	$151, %ecx
                                        # implicit-def: %EDI
	jne	.LBB7_101
# BB#87:
	movslq	%ebp, %rcx
	addl	%r12d, %r14d
	subl	%eax, %r14d
	addl	48(%rbx,%rcx,4), %r14d
	movl	56(%rbx,%rcx,4), %edi
	movl	$151, %ecx
	jmp	.LBB7_101
.LBB7_100:
	movl	48(%rbx,%rbp,4), %r14d
	movl	56(%rbx,%rbp,4), %edi
	addl	%r13d, %r12d
	subl	%r12d, %edi
	movl	$153, %ecx
	jmp	.LBB7_101
.LBB7_99:
	movl	48(%rbx,%rbp,4), %r14d
	subl	%r13d, %r14d
	movl	56(%rbx,%rbp,4), %edi
	subl	%r12d, %edi
	movl	$152, %ecx
	jmp	.LBB7_101
.LBB7_89:
	movslq	%ebp, %rcx
	movl	48(%rbx,%rcx,4), %edx
	addl	%r12d, %r14d
	subl	%eax, %r14d
	addl	56(%rbx,%rcx,4), %r14d
	movl	$153, %ecx
	movl	%r14d, %edi
	movl	%edx, %r14d
	jmp	.LBB7_101
.LBB7_88:
	movslq	%ebp, %rax
	addl	56(%rbx,%rax,4), %r14d
	movl	$152, %ecx
	movl	%r14d, %edi
	xorl	%r14d, %r14d
.LBB7_101:
	movl	32(%rsp), %ebp
	movl	$-1, %edx
	movl	%ebp, %esi
	subl	%r14d, %esi
	jl	.LBB7_102
# BB#103:
	addl	%edi, %r14d
	movl	36(%rsp), %r10d
	movl	%r10d, %ebx
	subl	%r14d, %ebx
	jge	.LBB7_104
.LBB7_102:
	movl	$-1, %edi
	movl	$-1, %eax
.LBB7_112:                              # %.sink.split4.i
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%edx, (%rcx)
	movl	%edi, 4(%rcx)
	movl	%eax, 8(%rcx)
	jmp	.LBB7_151
.LBB7_104:
	movl	40(%rsp), %r9d
	movl	%r9d, %r8d
	subl	%edi, %r8d
	movl	$-1, %edi
	movl	$-1, %eax
	jl	.LBB7_112
# BB#105:
	cmpl	$153, %ecx
	je	.LBB7_110
# BB#106:
	cmpl	$152, %ecx
	movl	24(%rsp), %edi          # 4-byte Reload
	je	.LBB7_109
# BB#107:
	cmpl	$151, %ecx
	jne	.LBB7_151
# BB#108:
	cmpl	$8388607, %ebp          # imm = 0x7FFFFF
	cmovel	%ebp, %esi
	cmpl	$8388607, %r10d         # imm = 0x7FFFFF
	cmovel	%r10d, %ebx
	cmpl	%ebx, %esi
	cmovlel	%esi, %ebx
	leal	(%rbx,%r15), %edx
	cmpl	$8388608, %edx          # imm = 0x800000
	movl	$8388607, %eax          # imm = 0x7FFFFF
	cmovgel	%eax, %edx
	addl	%edi, %r15d
	addl	%ebx, %r15d
	cmpl	$8388608, %r15d         # imm = 0x800000
	cmovgel	%eax, %r15d
	addl	%edi, %ebx
	jmp	.LBB7_111
.LBB7_110:
	cmpl	$8388607, %r9d          # imm = 0x7FFFFF
	cmovel	%r9d, %r8d
	cmpl	$8388607, %r10d         # imm = 0x7FFFFF
	cmovel	%r10d, %ebx
	cmpl	%ebx, %r8d
	cmovlel	%r8d, %ebx
	leal	(%rbx,%r15), %edx
	cmpl	$8388608, %edx          # imm = 0x800000
	movl	$8388607, %eax          # imm = 0x7FFFFF
	cmovgel	%eax, %edx
	movl	24(%rsp), %ecx          # 4-byte Reload
	addl	%ecx, %r15d
	addl	%ebx, %r15d
	cmpl	$8388608, %r15d         # imm = 0x800000
	cmovgel	%eax, %r15d
	addl	%ecx, %ebx
.LBB7_111:                              # %.sink.split4.i
	cmpl	$8388608, %ebx          # imm = 0x800000
	cmovgel	%eax, %ebx
	movl	%r15d, %edi
	movl	%ebx, %eax
	jmp	.LBB7_112
.LBB7_109:
	cmpl	$8388607, %ebp          # imm = 0x7FFFFF
	cmovel	%ebp, %esi
	cmpl	$8388607, %r10d         # imm = 0x7FFFFF
	cmovel	%r10d, %ebx
	cmpl	$8388607, %r9d          # imm = 0x7FFFFF
	cmovel	%r9d, %r8d
	cmpl	%ebx, %esi
	cmovgl	%ebx, %esi
	cmpl	%ebx, %r8d
	cmovgl	%ebx, %r8d
	addl	%r15d, %esi
	cmpl	$8388608, %esi          # imm = 0x800000
	movl	$8388607, %eax          # imm = 0x7FFFFF
	cmovgel	%eax, %esi
	addl	%edi, %r15d
	addl	%ebx, %r15d
	cmpl	$8388608, %r15d         # imm = 0x800000
	cmovgel	%eax, %r15d
	addl	%edi, %r8d
	cmpl	$8388608, %r8d          # imm = 0x800000
	cmovgel	%eax, %r8d
	movl	%esi, %edx
	movl	%r15d, %edi
	movl	%r8d, %eax
	jmp	.LBB7_112
.Lfunc_end7:
	.size	Constrained, .Lfunc_end7-Constrained
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_9
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_56
	.quad	.LBB7_14
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_52
	.quad	.LBB7_52
	.quad	.LBB7_59
	.quad	.LBB7_59
	.quad	.LBB7_65
	.quad	.LBB7_65
	.quad	.LBB7_65
	.quad	.LBB7_14
	.quad	.LBB7_150
	.quad	.LBB7_14
	.quad	.LBB7_150
	.quad	.LBB7_14
	.quad	.LBB7_14
	.quad	.LBB7_32
	.quad	.LBB7_32
	.quad	.LBB7_53
	.quad	.LBB7_53
	.quad	.LBB7_15
	.quad	.LBB7_15
	.quad	.LBB7_19
	.quad	.LBB7_19
	.quad	.LBB7_20
	.quad	.LBB7_14
	.quad	.LBB7_14
	.quad	.LBB7_14
	.quad	.LBB7_34
	.quad	.LBB7_34
	.quad	.LBB7_14
	.quad	.LBB7_14
	.quad	.LBB7_14
	.quad	.LBB7_14
	.quad	.LBB7_14
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_31
	.quad	.LBB7_14
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_150
	.quad	.LBB7_14
	.quad	.LBB7_14
	.quad	.LBB7_14
	.quad	.LBB7_14

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"InvScaleConstraint: sf <= 0!"
	.size	.L.str.1, 29

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"RotateConstraint: theta!"
	.size	.L.str.2, 25

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Constrained: x has no parent!"
	.size	.L.str.4, 30

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Constrained: COL_THR!"
	.size	.L.str.5, 22

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"assert failed in %s %s"
	.size	.L.str.6, 23

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Constrained:"
	.size	.L.str.7, 13


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
