	.text
	.file	"box_neighbors.bc"
	.globl	hypre_RankLinkCreate
	.p2align	4, 0x90
	.type	hypre_RankLinkCreate,@function
hypre_RankLinkCreate:                   # @hypre_RankLinkCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	$16, %edi
	callq	hypre_MAlloc
	movl	%ebp, (%rax)
	movq	$0, 8(%rax)
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_RankLinkCreate, .Lfunc_end0-hypre_RankLinkCreate
	.cfi_endproc

	.globl	hypre_RankLinkDestroy
	.p2align	4, 0x90
	.type	hypre_RankLinkDestroy,@function
hypre_RankLinkDestroy:                  # @hypre_RankLinkDestroy
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	callq	hypre_Free
	addq	$8, %rsp
.LBB1_2:
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	hypre_RankLinkDestroy, .Lfunc_end1-hypre_RankLinkDestroy
	.cfi_endproc

	.globl	hypre_BoxNeighborsCreate
	.p2align	4, 0x90
	.type	hypre_BoxNeighborsCreate,@function
hypre_BoxNeighborsCreate:               # @hypre_BoxNeighborsCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 64
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	%r9d, 4(%rsp)           # 4-byte Spill
	movl	%r8d, %ebp
	movl	%ecx, %r15d
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movl	$1, %edi
	movl	$48, %esi
	callq	hypre_CAlloc
	movq	%rax, %r14
	movl	$216, %esi
	movl	%ebp, %edi
	callq	hypre_CAlloc
	movq	%rax, 40(%r14)
	movq	%rbx, (%r14)
	movq	%r13, 8(%r14)
	movq	%r12, 16(%r14)
	movl	%r15d, 24(%r14)
	movl	%ebp, 28(%r14)
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 32(%r14)
	movq	64(%rsp), %rax
	movq	%r14, (%rax)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	hypre_BoxNeighborsCreate, .Lfunc_end2-hypre_BoxNeighborsCreate
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	hypre_BoxNeighborsAssemble
	.p2align	4, 0x90
	.type	hypre_BoxNeighborsAssemble,@function
hypre_BoxNeighborsAssemble:             # @hypre_BoxNeighborsAssemble
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 176
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movl	%esi, 32(%rsp)          # 4-byte Spill
	movq	(%rdi), %r11
	movl	24(%rdi), %esi
	movl	8(%r11), %eax
	testl	%eax, %eax
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	jle	.LBB3_1
# BB#2:                                 # %.preheader166.lr.ph
	movq	8(%rdi), %r9
	movq	16(%rdi), %rdx
	movslq	28(%rdi), %rbp
	movl	32(%rdi), %ecx
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	addl	%ebp, %ecx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	jle	.LBB3_3
# BB#16:                                # %.preheader166.us.preheader
	movslq	%esi, %rax
	movl	%ecx, %ecx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	leaq	(%rax,%rax,2), %rax
	leaq	20(,%rax,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
	xorl	%edi, %edi
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movq	%r11, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_17:                               # %.preheader166.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_18 Depth 2
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movq	72(%rsp), %r12          # 8-byte Reload
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
	xorl	%r8d, %r8d
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_18:                               #   Parent Loop BB3_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rsi,%rbp), %eax
	cmpq	%rax, %rcx
	je	.LBB3_22
# BB#19:                                #   in Loop: Header=BB3_18 Depth=2
	movq	(%r11), %rsi
	leaq	(%rcx,%rcx,2), %rdi
	movl	(%rsi,%rdi,8), %r9d
	subl	-8(%rsi,%r12), %r9d
	movl	$0, %eax
	cmovnsl	%r9d, %eax
	movl	12(%rsi,%rdi,8), %edx
	movl	-20(%rsi,%r12), %r14d
	movl	-16(%rsi,%r12), %ebx
	movl	%r14d, %ecx
	subl	%edx, %ecx
	cmpl	%ecx, %eax
	cmovgel	%eax, %ecx
	subl	%edx, %r14d
	cmovlel	%eax, %ecx
	movl	4(%rsi,%rdi,8), %r10d
	movl	-4(%rsi,%r12), %eax
	movl	%r10d, %edx
	subl	%eax, %edx
	cmpl	%edx, %ecx
	cmovgel	%ecx, %edx
	subl	%eax, %r10d
	cmovlel	%ecx, %edx
	movl	16(%rsi,%rdi,8), %eax
	movl	%ebx, %ecx
	subl	%eax, %ecx
	cmpl	%ecx, %edx
	cmovgel	%edx, %ecx
	subl	%eax, %ebx
	movl	%ebx, 36(%rsp)          # 4-byte Spill
	cmovlel	%edx, %ecx
	movl	8(%rsi,%rdi,8), %ebx
	movl	(%rsi,%r12), %edx
	movl	%ebx, %eax
	subl	%edx, %eax
	cmpl	%eax, %ecx
	cmovgel	%ecx, %eax
	subl	%edx, %ebx
	cmovlel	%ecx, %eax
	movl	-12(%rsi,%r12), %r13d
	movl	20(%rsi,%rdi,8), %ecx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%r13d, %edx
	subl	%ecx, %edx
	cmpl	%edx, %eax
	cmovgel	%eax, %edx
	subl	%ecx, %r13d
	movq	56(%rsp), %rcx          # 8-byte Reload
	cmovlel	%eax, %edx
	cmpl	32(%rsp), %edx          # 4-byte Folded Reload
	jg	.LBB3_23
# BB#20:                                #   in Loop: Header=BB3_18 Depth=2
	cmpq	104(%rsp), %rbp         # 8-byte Folded Reload
	jge	.LBB3_22
# BB#21:                                #   in Loop: Header=BB3_18 Depth=2
	xorl	%eax, %eax
	testl	%ebx, %ebx
	setg	%al
	movq	%rax, 96(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	testl	%r10d, %r10d
	setg	%al
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	testl	%r9d, %r9d
	setg	%bl
	movl	$16, %edi
	callq	hypre_MAlloc
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	80(%rsp), %r11          # 8-byte Reload
	incq	%rbx
	testl	%r14d, %r14d
	movl	$0, %r8d
	cmovgq	%r8, %rbx
	movq	%rbx, %rdx
	movq	88(%rsp), %rbx          # 8-byte Reload
	incq	%rbx
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	cmovgq	%r8, %rbx
	movq	96(%rsp), %rdi          # 8-byte Reload
	incq	%rdi
	testl	%r13d, %r13d
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, (%rax)
	movq	$0, 8(%rax)
	cmovgq	%r8, %rdi
	leaq	(%rdx,%rdx,8), %rcx
	shlq	$3, %rcx
	movq	48(%rsp), %rdx          # 8-byte Reload
	addq	40(%rdx), %rcx
	leaq	(%rbx,%rbx,2), %rdx
	leaq	(%rcx,%rdx,8), %rcx
	leaq	(%rcx,%rdi,8), %rcx
	movq	(%r15,%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	%rax, (%r15,%rcx)
	movq	56(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_22:                               #   in Loop: Header=BB3_18 Depth=2
	movl	$1, %r8d
.LBB3_23:                               #   in Loop: Header=BB3_18 Depth=2
	incq	%rbp
	addq	$216, %r15
	addq	$24, %r12
	cmpq	%rbp, 112(%rsp)         # 8-byte Folded Reload
	jne	.LBB3_18
# BB#24:                                # %._crit_edge.us
                                        #   in Loop: Header=BB3_17 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB3_30
# BB#25:                                #   in Loop: Header=BB3_17 Depth=1
	testl	%r8d, %r8d
	movq	40(%rsp), %r9           # 8-byte Reload
	je	.LBB3_26
# BB#27:                                #   in Loop: Header=BB3_17 Depth=1
	negl	(%r9,%rcx,4)
	movslq	28(%rsp), %rax          # 4-byte Folded Reload
	cmpq	%rcx, %rax
	movl	12(%rsp), %edi          # 4-byte Reload
	jge	.LBB3_29
# BB#28:                                #   in Loop: Header=BB3_17 Depth=1
	movl	%ecx, (%r9,%rax,4)
.LBB3_29:                               #   in Loop: Header=BB3_17 Depth=1
	leal	1(%rcx), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	incl	%edi
	jmp	.LBB3_31
	.p2align	4, 0x90
.LBB3_30:                               #   in Loop: Header=BB3_17 Depth=1
	movl	12(%rsp), %edi          # 4-byte Reload
	incl	%edi
	movq	40(%rsp), %r9           # 8-byte Reload
	jmp	.LBB3_31
	.p2align	4, 0x90
.LBB3_26:                               #   in Loop: Header=BB3_17 Depth=1
	movl	12(%rsp), %edi          # 4-byte Reload
.LBB3_31:                               #   in Loop: Header=BB3_17 Depth=1
	incq	%rcx
	movslq	8(%r11), %rax
	cmpq	%rax, %rcx
	jl	.LBB3_17
	jmp	.LBB3_32
.LBB3_1:
	xorl	%esi, %esi
	jmp	.LBB3_37
.LBB3_3:                                # %.preheader166.preheader
	xorl	%edx, %edx
	cmpl	$7, %eax
	ja	.LBB3_7
# BB#4:
	xorl	%edi, %edi
	jmp	.LBB3_5
.LBB3_7:                                # %min.iters.checked
	movl	%eax, %ecx
	andl	$-8, %ecx
	movl	$0, %edi
	je	.LBB3_5
# BB#8:                                 # %vector.ph
	xorl	%edx, %edx
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	sete	%dl
	movd	%edx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leal	-8(%rcx), %edx
	movl	%edx, %edi
	shrl	$3, %edi
	incl	%edi
	andl	$3, %edi
	je	.LBB3_9
# BB#10:                                # %vector.body.prol.preheader
	movdqa	.LCPI3_0(%rip), %xmm3   # xmm3 = [1,1,1,1]
	pand	%xmm0, %xmm3
	negl	%edi
	pxor	%xmm1, %xmm1
	xorl	%esi, %esi
	pxor	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB3_11:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	paddd	%xmm3, %xmm1
	paddd	%xmm3, %xmm2
	addl	$8, %esi
	incl	%edi
	jne	.LBB3_11
	jmp	.LBB3_12
.LBB3_9:
	xorl	%esi, %esi
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
.LBB3_12:                               # %vector.body.prol.loopexit
	cmpl	$24, %edx
	jb	.LBB3_15
# BB#13:                                # %vector.ph.new
	pand	.LCPI3_0(%rip), %xmm0
	movl	%ecx, %edx
	subl	%esi, %edx
	.p2align	4, 0x90
.LBB3_14:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm2
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm2
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm2
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm2
	addl	$-32, %edx
	jne	.LBB3_14
.LBB3_15:                               # %middle.block
	paddd	%xmm2, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %edi
	cmpl	%ecx, %eax
	movl	%ecx, %edx
	je	.LBB3_32
.LBB3_5:                                # %.preheader166.preheader205
	xorl	%ecx, %ecx
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	sete	%cl
	.p2align	4, 0x90
.LBB3_6:                                # %.preheader166
                                        # =>This Inner Loop Header: Depth=1
	addl	%ecx, %edi
	incl	%edx
	cmpl	%eax, %edx
	jl	.LBB3_6
.LBB3_32:                               # %._crit_edge180
	movl	%edi, %esi
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB3_37
# BB#33:                                # %._crit_edge180
	testl	%edi, %edi
	jle	.LBB3_37
# BB#34:                                # %.lr.ph
	movq	(%r11), %rax
	leaq	20(%rax), %rcx
	xorl	%edx, %edx
	xorl	%edi, %edi
	movq	64(%rsp), %r10          # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_35:                               # =>This Inner Loop Header: Depth=1
	movl	%ebx, %r8d
	movslq	%edi, %rdi
	movl	(%r9,%rdi,4), %ebx
	testl	%ebx, %ebx
	cmovgl	%ebx, %edi
	movslq	%edi, %rdi
	leaq	(%rdi,%rdi,2), %rbx
	movl	(%rax,%rbx,8), %ebp
	movl	%ebp, -20(%rcx)
	movl	4(%rax,%rbx,8), %ebp
	movl	%ebp, -16(%rcx)
	movl	8(%rax,%rbx,8), %ebp
	movl	%ebp, -12(%rcx)
	movl	12(%rax,%rbx,8), %ebp
	movl	%ebp, -8(%rcx)
	movl	16(%rax,%rbx,8), %ebp
	movl	%ebp, -4(%rcx)
	movl	20(%rax,%rbx,8), %ebp
	movl	%ebp, (%rcx)
	xorl	%ebp, %ebp
	subl	(%r9,%rdi,4), %ebp
	movl	%ebp, (%r9,%rdx,4)
	movl	(%r10,%rdi,4), %ebp
	movl	%ebp, (%r10,%rdx,4)
	cmpl	%r8d, %edi
	movl	%edx, %ebx
	cmovnel	%r8d, %ebx
	incl	%edi
	incq	%rdx
	addq	$24, %rcx
	cmpq	%rdx, %rsi
	jne	.LBB3_35
# BB#36:                                # %.loopexit.loopexit
	movq	%rbx, 16(%rsp)          # 8-byte Spill
.LBB3_37:                               # %.loopexit
	movq	%r11, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	hypre_BoxArraySetSize
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 24(%rax)
	xorl	%eax, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	hypre_BoxNeighborsAssemble, .Lfunc_end3-hypre_BoxNeighborsAssemble
	.cfi_endproc

	.globl	hypre_BoxNeighborsDestroy
	.p2align	4, 0x90
	.type	hypre_BoxNeighborsDestroy,@function
hypre_BoxNeighborsDestroy:              # @hypre_BoxNeighborsDestroy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 48
.Lcfi37:
	.cfi_offset %rbx, -48
.Lcfi38:
	.cfi_offset %r12, -40
.Lcfi39:
	.cfi_offset %r13, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB4_33
# BB#1:                                 # %.preheader46
	cmpl	$0, 28(%r15)
	jle	.LBB4_32
# BB#2:                                 # %.preheader45.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_3:                                # %.preheader45
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_4 Depth 2
                                        #       Child Loop BB4_5 Depth 3
                                        #       Child Loop BB4_8 Depth 3
                                        #       Child Loop BB4_11 Depth 3
                                        #       Child Loop BB4_14 Depth 3
                                        #       Child Loop BB4_17 Depth 3
                                        #       Child Loop BB4_20 Depth 3
                                        #       Child Loop BB4_23 Depth 3
                                        #       Child Loop BB4_26 Depth 3
                                        #       Child Loop BB4_29 Depth 3
	movq	$-1, %r13
	.p2align	4, 0x90
.LBB4_4:                                # %.preheader44
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_5 Depth 3
                                        #       Child Loop BB4_8 Depth 3
                                        #       Child Loop BB4_11 Depth 3
                                        #       Child Loop BB4_14 Depth 3
                                        #       Child Loop BB4_17 Depth 3
                                        #       Child Loop BB4_20 Depth 3
                                        #       Child Loop BB4_23 Depth 3
                                        #       Child Loop BB4_26 Depth 3
                                        #       Child Loop BB4_29 Depth 3
	movq	40(%r15), %rax
	imulq	$216, %r14, %r12
	leaq	(%rax,%r12), %rcx
	movq	8(%rcx,%r13,8), %rdi
	incq	%r13
	testq	%rdi, %rdi
	je	.LBB4_7
	.p2align	4, 0x90
.LBB4_5:                                # %hypre_RankLinkDestroy.exit
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rdi), %rbx
	callq	hypre_Free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB4_5
# BB#6:                                 # %.loopexit.loopexit
                                        #   in Loop: Header=BB4_4 Depth=2
	movq	40(%r15), %rax
.LBB4_7:                                # %.loopexit
                                        #   in Loop: Header=BB4_4 Depth=2
	leaq	(%rax,%r12), %rcx
	movq	72(%rcx,%r13,8), %rdi
	testq	%rdi, %rdi
	je	.LBB4_10
	.p2align	4, 0x90
.LBB4_8:                                # %hypre_RankLinkDestroy.exit.1
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rdi), %rbx
	callq	hypre_Free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB4_8
# BB#9:                                 # %.loopexit.loopexit.1
                                        #   in Loop: Header=BB4_4 Depth=2
	movq	40(%r15), %rax
.LBB4_10:                               # %.loopexit.1
                                        #   in Loop: Header=BB4_4 Depth=2
	leaq	(%rax,%r12), %rcx
	movq	144(%rcx,%r13,8), %rdi
	testq	%rdi, %rdi
	je	.LBB4_13
	.p2align	4, 0x90
.LBB4_11:                               # %hypre_RankLinkDestroy.exit.2
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rdi), %rbx
	callq	hypre_Free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB4_11
# BB#12:                                # %.loopexit.loopexit.2
                                        #   in Loop: Header=BB4_4 Depth=2
	movq	40(%r15), %rax
.LBB4_13:                               # %.loopexit.2
                                        #   in Loop: Header=BB4_4 Depth=2
	leaq	(%rax,%r12), %rcx
	movq	24(%rcx,%r13,8), %rdi
	testq	%rdi, %rdi
	je	.LBB4_16
	.p2align	4, 0x90
.LBB4_14:                               # %hypre_RankLinkDestroy.exit.154
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rdi), %rbx
	callq	hypre_Free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB4_14
# BB#15:                                # %.loopexit.loopexit.155
                                        #   in Loop: Header=BB4_4 Depth=2
	movq	40(%r15), %rax
.LBB4_16:                               # %.loopexit.156
                                        #   in Loop: Header=BB4_4 Depth=2
	leaq	(%rax,%r12), %rcx
	movq	96(%rcx,%r13,8), %rdi
	testq	%rdi, %rdi
	je	.LBB4_19
	.p2align	4, 0x90
.LBB4_17:                               # %hypre_RankLinkDestroy.exit.1.1
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rdi), %rbx
	callq	hypre_Free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB4_17
# BB#18:                                # %.loopexit.loopexit.1.1
                                        #   in Loop: Header=BB4_4 Depth=2
	movq	40(%r15), %rax
.LBB4_19:                               # %.loopexit.1.1
                                        #   in Loop: Header=BB4_4 Depth=2
	leaq	(%rax,%r12), %rcx
	movq	168(%rcx,%r13,8), %rdi
	testq	%rdi, %rdi
	je	.LBB4_22
	.p2align	4, 0x90
.LBB4_20:                               # %hypre_RankLinkDestroy.exit.2.1
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rdi), %rbx
	callq	hypre_Free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB4_20
# BB#21:                                # %.loopexit.loopexit.2.1
                                        #   in Loop: Header=BB4_4 Depth=2
	movq	40(%r15), %rax
.LBB4_22:                               # %.loopexit.2.1
                                        #   in Loop: Header=BB4_4 Depth=2
	leaq	(%rax,%r12), %rcx
	movq	48(%rcx,%r13,8), %rdi
	testq	%rdi, %rdi
	je	.LBB4_25
	.p2align	4, 0x90
.LBB4_23:                               # %hypre_RankLinkDestroy.exit.259
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rdi), %rbx
	callq	hypre_Free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB4_23
# BB#24:                                # %.loopexit.loopexit.260
                                        #   in Loop: Header=BB4_4 Depth=2
	movq	40(%r15), %rax
.LBB4_25:                               # %.loopexit.261
                                        #   in Loop: Header=BB4_4 Depth=2
	leaq	(%rax,%r12), %rcx
	movq	120(%rcx,%r13,8), %rdi
	testq	%rdi, %rdi
	je	.LBB4_28
	.p2align	4, 0x90
.LBB4_26:                               # %hypre_RankLinkDestroy.exit.1.2
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rdi), %rbx
	callq	hypre_Free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB4_26
# BB#27:                                # %.loopexit.loopexit.1.2
                                        #   in Loop: Header=BB4_4 Depth=2
	movq	40(%r15), %rax
.LBB4_28:                               # %.loopexit.1.2
                                        #   in Loop: Header=BB4_4 Depth=2
	addq	%r12, %rax
	movq	192(%rax,%r13,8), %rdi
	testq	%rdi, %rdi
	je	.LBB4_30
	.p2align	4, 0x90
.LBB4_29:                               # %hypre_RankLinkDestroy.exit.2.2
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rdi), %rbx
	callq	hypre_Free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB4_29
.LBB4_30:                               # %.loopexit.2.2
                                        #   in Loop: Header=BB4_4 Depth=2
	cmpq	$2, %r13
	jne	.LBB4_4
# BB#31:                                #   in Loop: Header=BB4_3 Depth=1
	incq	%r14
	movslq	28(%r15), %rax
	cmpq	%rax, %r14
	jl	.LBB4_3
.LBB4_32:                               # %._crit_edge
	movq	(%r15), %rdi
	callq	hypre_BoxArrayDestroy
	movq	8(%r15), %rdi
	callq	hypre_Free
	movq	$0, 8(%r15)
	movq	16(%r15), %rdi
	callq	hypre_Free
	movq	$0, 16(%r15)
	movq	40(%r15), %rdi
	callq	hypre_Free
	movq	$0, 40(%r15)
	movq	%r15, %rdi
	callq	hypre_Free
.LBB4_33:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	hypre_BoxNeighborsDestroy, .Lfunc_end4-hypre_BoxNeighborsDestroy
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
