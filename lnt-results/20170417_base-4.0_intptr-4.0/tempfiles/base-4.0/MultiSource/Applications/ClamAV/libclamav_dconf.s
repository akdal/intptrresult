	.text
	.file	"libclamav_dconf.bc"
	.globl	cli_dconf_init
	.p2align	4, 0x90
	.type	cli_dconf_init,@function
cli_dconf_init:                         # @cli_dconf_init
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movl	$28, %edi
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_1
# BB#2:                                 # %.preheader
	movl	$.L.str, %eax
	xorl	%r15d, %r15d
	movl	$modules+24, %r12d
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	cmpb	$80, (%rax)
	jne	.LBB0_8
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	cmpb	$69, 1(%rax)
	jne	.LBB0_8
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	cmpb	$0, 2(%rax)
	je	.LBB0_6
	.p2align	4, 0x90
.LBB0_8:                                # %.thread
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	-24(%r12), %rbx
	cmpb	$69, (%rbx)
	jne	.LBB0_14
# BB#9:                                 #   in Loop: Header=BB0_3 Depth=1
	cmpb	$76, 1(%rbx)
	jne	.LBB0_14
# BB#10:                                #   in Loop: Header=BB0_3 Depth=1
	cmpb	$70, 2(%rbx)
	jne	.LBB0_14
# BB#11:                                #   in Loop: Header=BB0_3 Depth=1
	cmpb	$0, 3(%rbx)
	je	.LBB0_12
	.p2align	4, 0x90
.LBB0_14:                               # %.thread253
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	$.L.str.2, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_15
# BB#17:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$.L.str.3, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_18
# BB#20:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$.L.str.4, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_21
# BB#23:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$.L.str.5, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_24
# BB#26:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$.L.str.6, %esi
	movq	%rbx, %rdi
	callq	strcmp
	movl	%r15d, %ecx
	andl	$-2, %ecx
	cmpq	$42, %rcx
	je	.LBB0_29
# BB#27:                                #   in Loop: Header=BB0_3 Depth=1
	testl	%eax, %eax
	jne	.LBB0_29
# BB#28:                                #   in Loop: Header=BB0_3 Depth=1
	movl	-8(%r12), %eax
	orl	%eax, 24(%r14)
	jmp	.LBB0_29
	.p2align	4, 0x90
.LBB0_15:                               #   in Loop: Header=BB0_3 Depth=1
	movl	%r15d, %eax
	andl	$-2, %eax
	cmpq	$42, %rax
	je	.LBB0_29
# BB#16:                                #   in Loop: Header=BB0_3 Depth=1
	movl	-8(%r12), %eax
	orl	%eax, 8(%r14)
	jmp	.LBB0_29
	.p2align	4, 0x90
.LBB0_18:                               #   in Loop: Header=BB0_3 Depth=1
	movl	%r15d, %eax
	andl	$-2, %eax
	cmpq	$42, %rax
	je	.LBB0_29
# BB#19:                                #   in Loop: Header=BB0_3 Depth=1
	movl	-8(%r12), %eax
	orl	%eax, 12(%r14)
	jmp	.LBB0_29
.LBB0_21:                               #   in Loop: Header=BB0_3 Depth=1
	movl	%r15d, %eax
	andl	$-2, %eax
	cmpq	$42, %rax
	je	.LBB0_29
# BB#22:                                #   in Loop: Header=BB0_3 Depth=1
	movl	-8(%r12), %eax
	orl	%eax, 16(%r14)
	jmp	.LBB0_29
.LBB0_6:                                #   in Loop: Header=BB0_3 Depth=1
	movl	%r15d, %eax
	andl	$-2, %eax
	cmpq	$42, %rax
	je	.LBB0_29
# BB#7:                                 #   in Loop: Header=BB0_3 Depth=1
	movl	-8(%r12), %eax
	orl	%eax, (%r14)
	jmp	.LBB0_29
.LBB0_24:                               #   in Loop: Header=BB0_3 Depth=1
	movl	%r15d, %eax
	andl	$-2, %eax
	cmpq	$42, %rax
	je	.LBB0_29
# BB#25:                                #   in Loop: Header=BB0_3 Depth=1
	movl	-8(%r12), %eax
	orl	%eax, 20(%r14)
	jmp	.LBB0_29
.LBB0_12:                               #   in Loop: Header=BB0_3 Depth=1
	movl	%r15d, %eax
	andl	$-2, %eax
	cmpq	$42, %rax
	je	.LBB0_29
# BB#13:                                #   in Loop: Header=BB0_3 Depth=1
	movl	-8(%r12), %eax
	orl	%eax, 4(%r14)
	.p2align	4, 0x90
.LBB0_29:                               #   in Loop: Header=BB0_3 Depth=1
	incq	%r15
	movq	(%r12), %rax
	addq	$24, %r12
	cmpq	$43, %r15
	jne	.LBB0_3
	jmp	.LBB0_30
.LBB0_1:
	xorl	%r14d, %r14d
.LBB0_30:                               # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	cli_dconf_init, .Lfunc_end0-cli_dconf_init
	.cfi_endproc

	.globl	cli_dconf_print
	.p2align	4, 0x90
	.type	cli_dconf_print,@function
cli_dconf_print:                        # @cli_dconf_print
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 80
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str, %eax
	movq	$-1032, %r15            # imm = 0xFBF8
	movl	$.L.str.10, %r12d
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	jmp	.LBB1_1
.LBB1_11:                               #   in Loop: Header=BB1_1 Depth=1
	cmpb	$0, 4(%rsp)             # 1-byte Folded Reload
	jne	.LBB1_35
# BB#12:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$0, 4(%r14)
	movl	$.L.str.11, %esi
	cmovneq	%r12, %rsi
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	$1, %al
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	cmpb	$80, (%rax)
	jne	.LBB1_7
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	cmpb	$69, 1(%rax)
	jne	.LBB1_7
# BB#3:                                 #   in Loop: Header=BB1_1 Depth=1
	cmpb	$0, 2(%rax)
	je	.LBB1_4
	.p2align	4, 0x90
.LBB1_7:                                # %.thread
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	modules+1032(%r15), %rbx
	cmpb	$69, (%rbx)
	jne	.LBB1_13
# BB#8:                                 #   in Loop: Header=BB1_1 Depth=1
	cmpb	$76, 1(%rbx)
	jne	.LBB1_13
# BB#9:                                 #   in Loop: Header=BB1_1 Depth=1
	cmpb	$70, 2(%rbx)
	jne	.LBB1_13
# BB#10:                                #   in Loop: Header=BB1_1 Depth=1
	cmpb	$0, 3(%rbx)
	je	.LBB1_11
	.p2align	4, 0x90
.LBB1_13:                               # %.thread283
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	$.L.str.2, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_14
# BB#17:                                #   in Loop: Header=BB1_1 Depth=1
	movl	$.L.str.3, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_18
# BB#21:                                #   in Loop: Header=BB1_1 Depth=1
	movl	$.L.str.4, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_22
# BB#25:                                #   in Loop: Header=BB1_1 Depth=1
	movl	$.L.str.5, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_26
# BB#29:                                #   in Loop: Header=BB1_1 Depth=1
	movl	$.L.str.6, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB1_35
# BB#30:                                #   in Loop: Header=BB1_1 Depth=1
	cmpb	$0, 8(%rsp)             # 1-byte Folded Reload
	jne	.LBB1_32
# BB#31:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$0, 24(%r14)
	movl	$.L.str.11, %esi
	cmovneq	%r12, %rsi
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	$1, %al
	movl	%eax, 8(%rsp)           # 4-byte Spill
.LBB1_32:                               #   in Loop: Header=BB1_1 Depth=1
	movl	24(%r14), %eax
	testl	%eax, %eax
	jne	.LBB1_34
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_14:                               #   in Loop: Header=BB1_1 Depth=1
	testb	%r13b, %r13b
	jne	.LBB1_16
# BB#15:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$0, 8(%r14)
	movl	$.L.str.11, %esi
	cmovneq	%r12, %rsi
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	$1, %r13b
.LBB1_16:                               #   in Loop: Header=BB1_1 Depth=1
	movl	8(%r14), %eax
	testl	%eax, %eax
	jne	.LBB1_34
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_18:                               #   in Loop: Header=BB1_1 Depth=1
	testb	%bpl, %bpl
	jne	.LBB1_20
# BB#19:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$0, 12(%r14)
	movl	$.L.str.11, %esi
	cmovneq	%r12, %rsi
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	$1, %bpl
.LBB1_20:                               #   in Loop: Header=BB1_1 Depth=1
	movl	12(%r14), %eax
	testl	%eax, %eax
	jne	.LBB1_34
	jmp	.LBB1_35
.LBB1_22:                               #   in Loop: Header=BB1_1 Depth=1
	cmpb	$0, 20(%rsp)            # 1-byte Folded Reload
	jne	.LBB1_24
# BB#23:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$0, 16(%r14)
	movl	$.L.str.11, %esi
	cmovneq	%r12, %rsi
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	$1, %al
	movl	%eax, 20(%rsp)          # 4-byte Spill
.LBB1_24:                               #   in Loop: Header=BB1_1 Depth=1
	movl	16(%r14), %eax
	testl	%eax, %eax
	jne	.LBB1_34
	jmp	.LBB1_35
.LBB1_4:                                #   in Loop: Header=BB1_1 Depth=1
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	jne	.LBB1_6
# BB#5:                                 #   in Loop: Header=BB1_1 Depth=1
	cmpl	$0, (%r14)
	movl	$.L.str.11, %esi
	cmovneq	%r12, %rsi
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	$1, %al
	movl	%eax, 16(%rsp)          # 4-byte Spill
.LBB1_6:                                #   in Loop: Header=BB1_1 Depth=1
	movl	(%r14), %eax
	testl	%eax, %eax
	jne	.LBB1_34
	jmp	.LBB1_35
.LBB1_26:                               #   in Loop: Header=BB1_1 Depth=1
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	jne	.LBB1_28
# BB#27:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$0, 20(%r14)
	movl	$.L.str.11, %esi
	cmovneq	%r12, %rsi
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	$1, %al
	movl	%eax, 12(%rsp)          # 4-byte Spill
.LBB1_28:                               #   in Loop: Header=BB1_1 Depth=1
	movl	20(%r14), %eax
	testl	%eax, %eax
	je	.LBB1_35
	.p2align	4, 0x90
.LBB1_34:                               #   in Loop: Header=BB1_1 Depth=1
	movq	modules+1040(%r15), %rsi
	testl	modules+1048(%r15), %eax
	movl	$.L.str.13, %edx
	cmovneq	%r12, %rdx
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_35:                               #   in Loop: Header=BB1_1 Depth=1
	movq	modules+1056(%r15), %rax
	addq	$24, %r15
	jne	.LBB1_1
# BB#36:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	cli_dconf_print, .Lfunc_end1-cli_dconf_print
	.cfi_endproc

	.globl	cli_dconf_load
	.p2align	4, 0x90
	.type	cli_dconf_load,@function
cli_dconf_load:                         # @cli_dconf_load
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	subq	$8216, %rsp             # imm = 0x2018
.Lcfi28:
	.cfi_def_cfa_offset 8272
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%r14, %rdi
	movl	%edx, %esi
	callq	cli_initengine
	movl	%eax, %ebp
	testl	%ebp, %ebp
	movq	(%r14), %rdi
	je	.LBB2_2
# BB#1:
	callq	cl_free
.LBB2_36:                               # %._crit_edge
	movl	%ebp, %eax
	addq	$8216, %rsp             # imm = 0x2018
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_2:
	movq	80(%rdi), %r12
	leaq	16(%rsp), %rdi
	movl	$8192, %esi             # imm = 0x2000
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB2_3
# BB#4:                                 # %.lr.ph
	movl	$1, %ebp
	leaq	16(%rsp), %r13
	leaq	12(%rsp), %r15
	.p2align	4, 0x90
.LBB2_5:                                # =>This Inner Loop Header: Depth=1
	movq	%r13, %rdi
	callq	cli_chomp
	movl	$.L.str.20, %esi
	movl	$3, %edx
	movq	%r13, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB2_9
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=1
	movq	%r13, %rdi
	callq	chkflevel
	testl	%eax, %eax
	je	.LBB2_9
# BB#7:                                 #   in Loop: Header=BB2_5 Depth=1
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	leaq	19(%rsp), %rdi
	movq	%r15, %rdx
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB2_35
# BB#8:                                 #   in Loop: Header=BB2_5 Depth=1
	movl	12(%rsp), %eax
	movl	%eax, (%r12)
.LBB2_9:                                #   in Loop: Header=BB2_5 Depth=1
	movl	$.L.str.22, %esi
	movl	$4, %edx
	movq	%r13, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB2_13
# BB#10:                                #   in Loop: Header=BB2_5 Depth=1
	movq	%r13, %rdi
	callq	chkflevel
	testl	%eax, %eax
	je	.LBB2_13
# BB#11:                                #   in Loop: Header=BB2_5 Depth=1
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	leaq	20(%rsp), %rdi
	movq	%r15, %rdx
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB2_35
# BB#12:                                #   in Loop: Header=BB2_5 Depth=1
	movl	12(%rsp), %eax
	movl	%eax, 4(%r12)
.LBB2_13:                               #   in Loop: Header=BB2_5 Depth=1
	movl	$.L.str.23, %esi
	movl	$8, %edx
	movq	%r13, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB2_17
# BB#14:                                #   in Loop: Header=BB2_5 Depth=1
	movq	%r13, %rdi
	callq	chkflevel
	testl	%eax, %eax
	je	.LBB2_17
# BB#15:                                #   in Loop: Header=BB2_5 Depth=1
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	leaq	24(%rsp), %rdi
	movq	%r15, %rdx
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB2_35
# BB#16:                                #   in Loop: Header=BB2_5 Depth=1
	movl	12(%rsp), %eax
	movl	%eax, 8(%r12)
.LBB2_17:                               #   in Loop: Header=BB2_5 Depth=1
	movl	$.L.str.24, %esi
	movl	$9, %edx
	movq	%r13, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB2_21
# BB#18:                                #   in Loop: Header=BB2_5 Depth=1
	movq	%r13, %rdi
	callq	chkflevel
	testl	%eax, %eax
	je	.LBB2_21
# BB#19:                                #   in Loop: Header=BB2_5 Depth=1
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	leaq	25(%rsp), %rdi
	movq	%r15, %rdx
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB2_35
# BB#20:                                #   in Loop: Header=BB2_5 Depth=1
	movl	12(%rsp), %eax
	movl	%eax, 12(%r12)
.LBB2_21:                               #   in Loop: Header=BB2_5 Depth=1
	movl	$.L.str.25, %esi
	movl	$5, %edx
	movq	%r13, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB2_25
# BB#22:                                #   in Loop: Header=BB2_5 Depth=1
	movq	%r13, %rdi
	callq	chkflevel
	testl	%eax, %eax
	je	.LBB2_25
# BB#23:                                #   in Loop: Header=BB2_5 Depth=1
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	leaq	21(%rsp), %rdi
	movq	%r15, %rdx
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB2_35
# BB#24:                                #   in Loop: Header=BB2_5 Depth=1
	movl	12(%rsp), %eax
	movl	%eax, 16(%r12)
.LBB2_25:                               #   in Loop: Header=BB2_5 Depth=1
	movl	$.L.str.26, %esi
	movl	$6, %edx
	movq	%r13, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB2_29
# BB#26:                                #   in Loop: Header=BB2_5 Depth=1
	movq	%r13, %rdi
	callq	chkflevel
	testl	%eax, %eax
	je	.LBB2_29
# BB#27:                                #   in Loop: Header=BB2_5 Depth=1
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	leaq	22(%rsp), %rdi
	movq	%r15, %rdx
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB2_35
# BB#28:                                #   in Loop: Header=BB2_5 Depth=1
	movl	12(%rsp), %eax
	movl	%eax, 20(%r12)
.LBB2_29:                               #   in Loop: Header=BB2_5 Depth=1
	movl	$.L.str.27, %esi
	movl	$9, %edx
	movq	%r13, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB2_33
# BB#30:                                #   in Loop: Header=BB2_5 Depth=1
	movq	%r13, %rdi
	callq	chkflevel
	testl	%eax, %eax
	je	.LBB2_33
# BB#31:                                #   in Loop: Header=BB2_5 Depth=1
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	leaq	25(%rsp), %rdi
	movq	%r15, %rdx
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB2_35
# BB#32:                                #   in Loop: Header=BB2_5 Depth=1
	movl	12(%rsp), %eax
	movl	%eax, 24(%r12)
.LBB2_33:                               # %.backedge
                                        #   in Loop: Header=BB2_5 Depth=1
	movl	$8192, %esi             # imm = 0x2000
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	fgets
	incl	%ebp
	testq	%rax, %rax
	jne	.LBB2_5
# BB#34:
	xorl	%ebp, %ebp
	jmp	.LBB2_36
.LBB2_35:                               # %.thread
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_errmsg
	movq	(%r14), %rdi
	callq	cl_free
	movl	$-116, %ebp
	jmp	.LBB2_36
.LBB2_3:
	xorl	%ebp, %ebp
	jmp	.LBB2_36
.Lfunc_end2:
	.size	cli_dconf_load, .Lfunc_end2-cli_dconf_load
	.cfi_endproc

	.p2align	4, 0x90
	.type	chkflevel,@function
chkflevel:                              # @chkflevel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 64
.Lcfi42:
	.cfi_offset %rbx, -56
.Lcfi43:
	.cfi_offset %r12, -48
.Lcfi44:
	.cfi_offset %r13, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movl	$2, %esi
	movl	$.L.str.71, %edx
	callq	cli_strtok
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_8
# BB#1:
	callq	__ctype_b_loc
	movq	%rax, %r15
	movq	(%r15), %rax
	movsbq	(%rbx), %rcx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB3_3
# BB#2:
	movq	%rbx, %rdi
	callq	free
	xorl	%r14d, %r14d
	jmp	.LBB3_9
.LBB3_3:
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, %r13
	callq	cl_retflevel
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	free
	cmpl	%ebp, %r13d
	ja	.LBB3_9
# BB#4:
	movl	$3, %esi
	movl	$.L.str.71, %edx
	movq	%r12, %rdi
	callq	cli_strtok
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_8
# BB#5:
	movq	(%r15), %rax
	movsbq	(%rbx), %rcx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB3_7
# BB#6:
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB3_9
.LBB3_7:
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, %r15
	callq	cl_retflevel
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	free
	cmpl	%ebp, %r15d
	jb	.LBB3_9
.LBB3_8:
	movl	$1, %r14d
.LBB3_9:
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	chkflevel, .Lfunc_end3-chkflevel
	.cfi_endproc

	.type	modules,@object         # @modules
	.section	.rodata,"a",@progbits
	.p2align	4
modules:
	.quad	.L.str
	.quad	.L.str.29
	.long	1                       # 0x1
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str
	.quad	.L.str.30
	.long	2                       # 0x2
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str
	.quad	.L.str.31
	.long	4                       # 0x4
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str
	.quad	.L.str.32
	.long	8                       # 0x8
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str
	.quad	.L.str.33
	.long	16                      # 0x10
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str
	.quad	.L.str.34
	.long	32                      # 0x20
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str
	.quad	.L.str.35
	.long	64                      # 0x40
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str
	.quad	.L.str.36
	.long	256                     # 0x100
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str
	.quad	.L.str.37
	.long	512                     # 0x200
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str
	.quad	.L.str.38
	.long	1024                    # 0x400
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str
	.quad	.L.str.39
	.long	2048                    # 0x800
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str
	.quad	.L.str.40
	.long	4096                    # 0x1000
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str
	.quad	.L.str.41
	.long	8192                    # 0x2000
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str
	.quad	.L.str.42
	.long	16384                   # 0x4000
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str
	.quad	.L.str.43
	.long	32768                   # 0x8000
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.1
	.quad	0
	.long	1                       # 0x1
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.2
	.quad	.L.str.44
	.long	1                       # 0x1
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.2
	.quad	.L.str.45
	.long	2                       # 0x2
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.2
	.quad	.L.str.46
	.long	4                       # 0x4
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.2
	.quad	.L.str.47
	.long	8                       # 0x8
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.2
	.quad	.L.str.48
	.long	4096                    # 0x1000
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.2
	.quad	.L.str.49
	.long	16                      # 0x10
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.2
	.quad	.L.str.50
	.long	32                      # 0x20
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.2
	.quad	.L.str.51
	.long	64                      # 0x40
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.2
	.quad	.L.str.52
	.long	128                     # 0x80
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.2
	.quad	.L.str.53
	.long	256                     # 0x100
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.2
	.quad	.L.str.54
	.long	512                     # 0x200
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.2
	.quad	.L.str.55
	.long	1024                    # 0x400
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.2
	.quad	.L.str.56
	.long	2048                    # 0x800
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.2
	.quad	.L.str.57
	.long	8192                    # 0x2000
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.3
	.quad	.L.str.58
	.long	1                       # 0x1
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.3
	.quad	.L.str.59
	.long	2                       # 0x2
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.3
	.quad	.L.str.60
	.long	4                       # 0x4
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.4
	.quad	.L.str.61
	.long	1                       # 0x1
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.4
	.quad	.L.str.62
	.long	2                       # 0x2
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.4
	.quad	.L.str.63
	.long	4                       # 0x4
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.5
	.quad	.L.str.64
	.long	1                       # 0x1
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.5
	.quad	.L.str.65
	.long	2                       # 0x2
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.5
	.quad	.L.str.66
	.long	4                       # 0x4
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.5
	.quad	.L.str.67
	.long	8                       # 0x8
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.5
	.quad	.L.str.68
	.long	16                      # 0x10
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.6
	.quad	.L.str.69
	.long	1                       # 0x1
	.byte	1                       # 0x1
	.zero	3
	.quad	.L.str.6
	.quad	.L.str.70
	.long	2                       # 0x2
	.byte	0                       # 0x0
	.zero	3
	.zero	24
	.size	modules, 1056

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"PE"
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"ELF"
	.size	.L.str.1, 4

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"ARCHIVE"
	.size	.L.str.2, 8

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"DOCUMENT"
	.size	.L.str.3, 9

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"MAIL"
	.size	.L.str.4, 5

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"OTHER"
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"PHISHING"
	.size	.L.str.6, 9

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Dynamic engine configuration settings:\n"
	.size	.L.str.7, 40

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"--------------------------------------\n"
	.size	.L.str.8, 40

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Module PE: %s\n"
	.size	.L.str.9, 15

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"On"
	.size	.L.str.10, 3

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Off"
	.size	.L.str.11, 4

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"   * Submodule %10s:\t%s\n"
	.size	.L.str.12, 25

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"** Off **"
	.size	.L.str.13, 10

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Module ELF: %s\n"
	.size	.L.str.14, 16

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Module ARCHIVE: %s\n"
	.size	.L.str.15, 20

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Module DOCUMENT: %s\n"
	.size	.L.str.16, 21

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Module MAIL: %s\n"
	.size	.L.str.17, 17

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Module OTHER: %s\n"
	.size	.L.str.18, 18

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Module PHISHING %s\n"
	.size	.L.str.19, 20

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"PE:"
	.size	.L.str.20, 4

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"0x%x"
	.size	.L.str.21, 5

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"ELF:"
	.size	.L.str.22, 5

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"ARCHIVE:"
	.size	.L.str.23, 9

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"DOCUMENT:"
	.size	.L.str.24, 10

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"MAIL:"
	.size	.L.str.25, 6

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"OTHER:"
	.size	.L.str.26, 7

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"PHISHING:"
	.size	.L.str.27, 10

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Problem parsing configuration file at line %u\n"
	.size	.L.str.28, 47

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"PARITE"
	.size	.L.str.29, 7

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"KRIZ"
	.size	.L.str.30, 5

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"MAGISTR"
	.size	.L.str.31, 8

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"POLIPOS"
	.size	.L.str.32, 8

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"MD5SECT"
	.size	.L.str.33, 8

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"UPX"
	.size	.L.str.34, 4

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"FSG"
	.size	.L.str.35, 4

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"PETITE"
	.size	.L.str.36, 7

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"PESPIN"
	.size	.L.str.37, 7

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"YC"
	.size	.L.str.38, 3

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"WWPACK"
	.size	.L.str.39, 7

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"NSPACK"
	.size	.L.str.40, 7

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"MEW"
	.size	.L.str.41, 4

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"UPACK"
	.size	.L.str.42, 6

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"ASPACK"
	.size	.L.str.43, 7

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"RAR"
	.size	.L.str.44, 4

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"ZIP"
	.size	.L.str.45, 4

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"GZIP"
	.size	.L.str.46, 5

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"BZIP"
	.size	.L.str.47, 5

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"ARJ"
	.size	.L.str.48, 4

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"SZDD"
	.size	.L.str.49, 5

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"CAB"
	.size	.L.str.50, 4

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"CHM"
	.size	.L.str.51, 4

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"OLE2"
	.size	.L.str.52, 5

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"TAR"
	.size	.L.str.53, 4

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"BINHEX"
	.size	.L.str.54, 7

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"SIS"
	.size	.L.str.55, 4

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"NSIS"
	.size	.L.str.56, 5

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"AUTOIT"
	.size	.L.str.57, 7

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"HTML"
	.size	.L.str.58, 5

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"RTF"
	.size	.L.str.59, 4

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"PDF"
	.size	.L.str.60, 4

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"MBOX"
	.size	.L.str.61, 5

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"TNEF"
	.size	.L.str.62, 5

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"PST"
	.size	.L.str.63, 4

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"UUENCODED"
	.size	.L.str.64, 10

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"SCRENC"
	.size	.L.str.65, 7

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"RIFF"
	.size	.L.str.66, 5

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"JPEG"
	.size	.L.str.67, 5

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"CRYPTFF"
	.size	.L.str.68, 8

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"ENGINE"
	.size	.L.str.69, 7

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"ENTCONV"
	.size	.L.str.70, 8

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	":"
	.size	.L.str.71, 2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
