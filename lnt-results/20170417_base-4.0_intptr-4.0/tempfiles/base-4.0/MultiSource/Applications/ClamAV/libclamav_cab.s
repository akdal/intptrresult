	.text
	.file	"libclamav_cab.bc"
	.globl	cab_free
	.p2align	4, 0x90
	.type	cab_free,@function
cab_free:                               # @cab_free
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_2:                                #   in Loop: Header=BB0_1 Depth=1
	movq	24(%rdi), %rax
	movq	%rax, 16(%r14)
	callq	free
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_2
	jmp	.LBB0_4
	.p2align	4, 0x90
.LBB0_3:                                #   in Loop: Header=BB0_4 Depth=1
	movq	48(%rbx), %rax
	movq	%rax, 24(%r14)
	movq	16(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB0_4:                                # =>This Inner Loop Header: Depth=1
	movq	24(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_3
# BB#5:                                 # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	cab_free, .Lfunc_end0-cab_free
	.cfi_endproc

	.globl	cab_open
	.p2align	4, 0x90
	.type	cab_open,@function
cab_open:                               # @cab_open
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	subq	$536, %rsp              # imm = 0x218
.Lcfi11:
	.cfi_def_cfa_offset 592
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r12
	movl	%edi, %ebx
	xorl	%edx, %edx
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB1_7
# BB#1:
	leaq	88(%rsp), %rsi
	movl	$36, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$36, %eax
	jne	.LBB1_9
# BB#2:
	cmpl	$1178817357, 88(%rsp)   # imm = 0x4643534D
	jne	.LBB1_11
# BB#3:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	leaq	392(%rsp), %rdx
	movl	$1, %edi
	movl	%ebx, %esi
	callq	__fxstat
	cmpl	$-1, %eax
	je	.LBB1_15
# BB#4:
	movl	%ebx, (%rsp)            # 4-byte Spill
	movq	440(%rsp), %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movl	96(%rsp), %esi
	movl	%esi, (%rbp)
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	(%rbp), %r13d
	movzwl	114(%rsp), %esi
	testl	%esi, %esi
	movw	%si, 4(%rbp)
	je	.LBB1_16
# BB#5:
	xorl	%ebx, %ebx
	cmpq	%r14, %r13
	setg	%r15b
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	4(%rbp), %eax
	cmpl	$5001, %eax             # imm = 0x1389
	jb	.LBB1_17
# BB#6:
	xorl	%ebx, %ebx
	cmpq	%r14, %r13
	setg	%bl
	movw	$5000, 4(%rbp)          # imm = 0x1388
	movl	$.L.str.8, %edi
	movl	$5000, %esi             # imm = 0x1388
	xorl	%eax, %eax
	callq	cli_dbgmsg
	incl	%ebx
	jmp	.LBB1_18
.LBB1_7:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
.LBB1_8:                                # %cab_free.exit
	callq	cli_errmsg
	movl	$-123, %ebp
	jmp	.LBB1_14
.LBB1_9:
	movl	$.L.str.1, %edi
.LBB1_10:                               # %cab_free.exit
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-123, %ebp
	jmp	.LBB1_14
.LBB1_11:
	movl	$.L.str.2, %edi
.LBB1_12:                               # %cab_free.exit
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_13:                               # %cab_free.exit
	movl	$-124, %ebp
.LBB1_14:                               # %cab_free.exit
	movl	%ebp, %eax
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_15:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	jmp	.LBB1_8
.LBB1_16:
	movl	$.L.str.6, %edi
	jmp	.LBB1_12
.LBB1_17:
	movb	%r15b, %bl
.LBB1_18:
	movzwl	116(%rsp), %esi
	testl	%esi, %esi
	movw	%si, 6(%rbp)
	je	.LBB1_28
# BB#19:
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	6(%rbp), %eax
	cmpl	$5001, %eax             # imm = 0x1389
	movq	%r14, 40(%rsp)          # 8-byte Spill
	jb	.LBB1_21
# BB#20:
	movw	$5000, 6(%rbp)          # imm = 0x1388
	movl	$.L.str.11, %edi
	movl	$5000, %esi             # imm = 0x1388
	xorl	%eax, %eax
	callq	cli_dbgmsg
	incl	%ebx
.LBB1_21:
	movzbl	113(%rsp), %esi
	movzbl	112(%rsp), %edx
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpb	$1, 113(%rsp)
	setne	%r14b
	cmpb	$3, 112(%rsp)
	setne	%r15b
	movzwl	118(%rsp), %eax
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movw	%ax, 8(%rbp)
	testb	$4, %al
	jne	.LBB1_29
# BB#22:
	movl	%ebx, 24(%rsp)          # 4-byte Spill
	xorl	%ebx, %ebx
.LBB1_23:
	movq	16(%rsp), %rax          # 8-byte Reload
	movzwl	8(%rax), %eax
	testb	$1, %al
	jne	.LBB1_33
# BB#24:
	testb	$2, %al
	jne	.LBB1_166
.LBB1_25:
	orb	%r15b, %r14b
	movzbl	%r14b, %esi
	addl	24(%rsp), %esi          # 4-byte Folded Reload
	addl	8(%rsp), %esi           # 4-byte Folded Reload
	cmpl	$3, %esi
	jbe	.LBB1_40
# BB#26:
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
.LBB1_27:                               # %cab_free.exit
	callq	cli_dbgmsg
	jmp	.LBB1_13
.LBB1_28:
	movl	$.L.str.9, %edi
	jmp	.LBB1_12
.LBB1_29:
	leaq	56(%rsp), %rsi
	movl	$4, %edx
	movl	(%rsp), %edi            # 4-byte Reload
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB1_39
# BB#30:
	movl	%ebx, 24(%rsp)          # 4-byte Spill
	movzwl	56(%rsp), %esi
	testq	%rsi, %rsi
	movq	16(%rsp), %rcx          # 8-byte Reload
	movw	%si, 10(%rcx)
	movzbl	58(%rsp), %ebx
	movb	59(%rsp), %al
	movb	%al, 12(%rcx)
	je	.LBB1_23
# BB#31:
	movl	$1, %edx
	movl	(%rsp), %edi            # 4-byte Reload
	callq	lseek
	cmpq	$-1, %rax
	jne	.LBB1_23
# BB#32:
	movq	16(%rsp), %rax          # 8-byte Reload
	movzwl	10(%rax), %esi
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-123, %ebp
	jmp	.LBB1_14
.LBB1_33:
	movb	%r15b, 7(%rsp)          # 1-byte Spill
	movb	%r14b, 64(%rsp)         # 1-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movl	$1, %edx
	movl	(%rsp), %edi            # 4-byte Reload
	callq	lseek
	movq	%rax, %r15
	movl	$-123, %ebp
	cmpq	$-1, %r15
	je	.LBB1_14
# BB#34:
	leaq	128(%rsp), %rsi
	movl	$256, %edx              # imm = 0x100
	movl	(%rsp), %edi            # 4-byte Reload
	callq	read
	testl	%eax, %eax
	jle	.LBB1_13
# BB#35:                                # %.lr.ph.preheader.i
	movslq	%eax, %rcx
	xorl	%eax, %eax
.LBB1_36:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, 128(%rsp,%rax)
	je	.LBB1_49
# BB#37:                                #   in Loop: Header=BB1_36 Depth=1
	incq	%rax
	cmpq	%rcx, %rax
	jl	.LBB1_36
	jmp	.LBB1_13
.LBB1_39:
	movl	$.L.str.13, %edi
	jmp	.LBB1_10
.LBB1_40:                               # %.preheader302
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpw	$0, 4(%rax)
	je	.LBB1_62
# BB#41:                                # %.lr.ph352
	leaq	16(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testq	%rbx, %rbx
	je	.LBB1_57
# BB#42:                                # %.lr.ph352.split.preheader
	xorl	%r15d, %r15d
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB1_43:                               # %.lr.ph352.split
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, %r14d
	movl	$8, %edx
	movl	(%rsp), %edi            # 4-byte Reload
	leaq	48(%rsp), %rsi
	callq	cli_readn
	cmpl	$8, %eax
	jne	.LBB1_108
# BB#44:                                #   in Loop: Header=BB1_43 Depth=1
	movl	$1, %edx
	movl	(%rsp), %edi            # 4-byte Reload
	movq	%rbx, %rsi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB1_129
# BB#45:                                #   in Loop: Header=BB1_43 Depth=1
	movq	%r15, 64(%rsp)          # 8-byte Spill
	movl	$1, %edi
	movl	$32, %esi
	callq	cli_calloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_112
# BB#46:                                #   in Loop: Header=BB1_43 Depth=1
	movq	%r13, 8(%r15)
	movl	48(%rsp), %eax
	addq	%r12, %rax
	movq	%rax, 16(%r15)
	xorl	%ebx, %ebx
	cmpq	40(%rsp), %rax          # 8-byte Folded Reload
	setg	%bl
	addl	%r14d, %ebx
	movzwl	52(%rsp), %eax
	movw	%ax, 2(%r15)
	movzwl	54(%rsp), %eax
	movw	%ax, (%r15)
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	movl	8(%rsp), %ebp           # 4-byte Reload
	movl	%ebp, %esi
	callq	cli_dbgmsg
	movl	16(%r15), %esi
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	(%r15), %esi
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	(%r15), %eax
	xorl	%esi, %esi
	testb	$12, %al
	setne	%sil
	addl	%ebx, %esi
	movq	64(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	leaq	24(%rax), %rax
	movq	24(%rsp), %rbx          # 8-byte Reload
	cmoveq	%rbx, %rax
	movq	%r15, (%rax)
	cmpl	$11, %esi
	jae	.LBB1_117
# BB#47:                                #   in Loop: Header=BB1_43 Depth=1
	incl	%ebp
	movzwl	4(%r13), %eax
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	cmpl	%eax, %ebp
	movq	32(%rsp), %rbx          # 8-byte Reload
	jb	.LBB1_43
	jmp	.LBB1_62
.LBB1_49:
	leaq	1(%r15,%rax), %rsi
	xorl	%edx, %edx
	movl	(%rsp), %edi            # 4-byte Reload
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB1_14
# BB#50:
	leaq	128(%rsp), %rdi
	callq	cli_strdup
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB1_107
# BB#51:
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB1_56
# BB#52:                                # %.lr.ph.i192.preheader
	xorl	%r15d, %r15d
.LBB1_53:                               # %.lr.ph.i192
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%r14,%r15), %ebp
	movl	$.L.str.51, %edi
	movl	$16, %edx
	movl	%ebp, %esi
	callq	memchr
	testl	%ebp, %ebp
	js	.LBB1_122
# BB#54:                                # %.lr.ph.i192
                                        #   in Loop: Header=BB1_53 Depth=1
	testq	%rax, %rax
	jne	.LBB1_122
# BB#55:                                #   in Loop: Header=BB1_53 Depth=1
	incq	%r15
	cmpq	%rbx, %r15
	jb	.LBB1_53
.LBB1_56:                               # %.loopexit305
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	jmp	.LBB1_123
.LBB1_57:                               # %.lr.ph352.split.us.preheader
	xorl	%ebp, %ebp
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB1_58:                               # %.lr.ph352.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, %r13d
	movl	$8, %edx
	movl	(%rsp), %edi            # 4-byte Reload
	leaq	48(%rsp), %rsi
	callq	cli_readn
	cmpl	$8, %eax
	jne	.LBB1_108
# BB#59:                                #   in Loop: Header=BB1_58 Depth=1
	movl	$1, %edi
	movl	$32, %esi
	callq	cli_calloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_112
# BB#60:                                #   in Loop: Header=BB1_58 Depth=1
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%r14, 8(%r15)
	movl	48(%rsp), %eax
	addq	%r12, %rax
	movq	%rax, 16(%r15)
	xorl	%ebx, %ebx
	cmpq	40(%rsp), %rax          # 8-byte Folded Reload
	setg	%bl
	addl	%r13d, %ebx
	movzwl	52(%rsp), %eax
	movw	%ax, 2(%r15)
	movzwl	54(%rsp), %eax
	movw	%ax, (%r15)
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	movl	8(%rsp), %ebp           # 4-byte Reload
	movl	%ebp, %esi
	callq	cli_dbgmsg
	movl	16(%r15), %esi
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	(%r15), %esi
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	(%r15), %eax
	xorl	%esi, %esi
	testb	$12, %al
	setne	%sil
	addl	%ebx, %esi
	movq	32(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	leaq	24(%rax), %rax
	movq	24(%rsp), %rbx          # 8-byte Reload
	cmoveq	%rbx, %rax
	movq	%r15, (%rax)
	cmpl	$10, %esi
	ja	.LBB1_117
# BB#61:                                #   in Loop: Header=BB1_58 Depth=1
	incl	%ebp
	movzwl	4(%r14), %eax
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	cmpl	%eax, %ebp
	movq	%r15, %rbp
	jb	.LBB1_58
.LBB1_62:                               # %.preheader
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpw	$0, 6(%rax)
	je	.LBB1_106
# BB#63:                                # %.lr.ph348
	leaq	24(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	72(%rsp), %r15
	movl	$0, 40(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
.LBB1_64:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_70 Depth 2
                                        #     Child Loop BB1_93 Depth 2
	movl	%esi, 8(%rsp)           # 4-byte Spill
	cmpl	$11, %esi
	jae	.LBB1_143
# BB#65:                                #   in Loop: Header=BB1_64 Depth=1
	movl	$16, %edx
	movl	(%rsp), %edi            # 4-byte Reload
	movq	%r15, %rsi
	callq	cli_readn
	cmpl	$16, %eax
	jne	.LBB1_150
# BB#66:                                #   in Loop: Header=BB1_64 Depth=1
	movl	$1, %edi
	movl	$72, %esi
	callq	cli_calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	movq	16(%rsp), %rbp          # 8-byte Reload
	je	.LBB1_156
# BB#67:                                #   in Loop: Header=BB1_64 Depth=1
	movq	%rbp, 56(%rbx)
	movl	(%rsp), %edi            # 4-byte Reload
	movl	%edi, 28(%rbx)
	movl	72(%rsp), %eax
	movl	%eax, (%rbx)
	movl	76(%rsp), %eax
	movq	%rax, 8(%rbx)
	movzwl	86(%rsp), %eax
	movw	%ax, 4(%rbx)
	movzwl	80(%rsp), %r12d
	xorl	%esi, %esi
	movl	$1, %edx
	callq	lseek
	movq	%rax, %r15
	movl	$-123, %ebp
	cmpq	$-1, %r15
	je	.LBB1_100
# BB#68:                                #   in Loop: Header=BB1_64 Depth=1
	movl	$256, %edx              # imm = 0x100
	movl	(%rsp), %edi            # 4-byte Reload
	leaq	128(%rsp), %rsi
	callq	read
	testl	%eax, %eax
	jle	.LBB1_99
# BB#69:                                # %.lr.ph.preheader.i265
                                        #   in Loop: Header=BB1_64 Depth=1
	movzwl	%r12w, %r14d
	cltq
	xorl	%ecx, %ecx
.LBB1_70:                               # %.lr.ph.i268
                                        #   Parent Loop BB1_64 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, 128(%rsp,%rcx)
	je	.LBB1_72
# BB#71:                                #   in Loop: Header=BB1_70 Depth=2
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB1_70
	jmp	.LBB1_99
.LBB1_72:                               #   in Loop: Header=BB1_64 Depth=1
	leaq	1(%r15,%rcx), %rsi
	xorl	%edx, %edx
	movl	(%rsp), %edi            # 4-byte Reload
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB1_100
# BB#73:                                #   in Loop: Header=BB1_64 Depth=1
	leaq	128(%rsp), %rdi
	callq	cli_strdup
	testq	%rax, %rax
	movq	16(%rsp), %rbp          # 8-byte Reload
	je	.LBB1_199
# BB#74:                                #   in Loop: Header=BB1_64 Depth=1
	movq	%rax, 16(%rbx)
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	movl	40(%rsp), %esi          # 4-byte Reload
	callq	cli_dbgmsg
	movq	16(%rbx), %rsi
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	8(%rbx), %esi
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	cli_dbgmsg
	movzwl	4(%rbx), %esi
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	4(%rbx), %eax
	testb	$1, %al
	je	.LBB1_76
# BB#75:                                #   in Loop: Header=BB1_64 Depth=1
	movl	$.L.str.33, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movw	4(%rbx), %ax
.LBB1_76:                               #   in Loop: Header=BB1_64 Depth=1
	testb	$2, %al
	je	.LBB1_78
# BB#77:                                #   in Loop: Header=BB1_64 Depth=1
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movw	4(%rbx), %ax
.LBB1_78:                               #   in Loop: Header=BB1_64 Depth=1
	testb	$4, %al
	je	.LBB1_80
# BB#79:                                #   in Loop: Header=BB1_64 Depth=1
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movw	4(%rbx), %ax
.LBB1_80:                               #   in Loop: Header=BB1_64 Depth=1
	testb	$32, %al
	je	.LBB1_82
# BB#81:                                #   in Loop: Header=BB1_64 Depth=1
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movw	4(%rbx), %ax
.LBB1_82:                               #   in Loop: Header=BB1_64 Depth=1
	testb	$64, %al
	je	.LBB1_84
# BB#83:                                #   in Loop: Header=BB1_64 Depth=1
	movl	$.L.str.37, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movw	4(%rbx), %ax
.LBB1_84:                               #   in Loop: Header=BB1_64 Depth=1
	testb	%al, %al
	jns	.LBB1_86
# BB#85:                                #   in Loop: Header=BB1_64 Depth=1
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_86:                               #   in Loop: Header=BB1_64 Depth=1
	cmpl	$65532, %r14d           # imm = 0xFFFC
	ja	.LBB1_91
# BB#87:                                #   in Loop: Header=BB1_64 Depth=1
	cmpw	4(%rbp), %r12w
	jbe	.LBB1_92
# BB#88:                                #   in Loop: Header=BB1_64 Depth=1
	movl	8(%rsp), %eax           # 4-byte Reload
	cmpl	$2, %eax
	leaq	72(%rsp), %r15
	ja	.LBB1_90
# BB#89:                                #   in Loop: Header=BB1_64 Depth=1
	movq	16(%rbx), %rsi
	movl	$.L.str.39, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	8(%rsp), %eax           # 4-byte Reload
.LBB1_90:                               #   in Loop: Header=BB1_64 Depth=1
	incl	%eax
	movq	16(%rbx), %rdi
	movl	%eax, %ebp
	callq	free
	movq	%rbx, %rdi
	callq	free
	movl	%ebp, %esi
	jmp	.LBB1_98
.LBB1_91:                               #   in Loop: Header=BB1_64 Depth=1
	movl	$.L.str.41, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	16(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB1_97
.LBB1_92:                               #   in Loop: Header=BB1_64 Depth=1
	movq	16(%rbp), %rcx
	movq	%rcx, 40(%rbx)
	testq	%rcx, %rcx
	sete	%al
	setne	%dl
	testw	%r12w, %r12w
	je	.LBB1_95
.LBB1_93:                               #   Parent Loop BB1_64 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	%dl, %dl
	je	.LBB1_95
# BB#94:                                # %.lr.ph
                                        #   in Loop: Header=BB1_93 Depth=2
	notb	%al
	movzbl	%al, %esi
	andl	$1, %esi
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	sete	%al
	setne	%dl
	subw	%si, %r12w
	movq	%rcx, 40(%rbx)
	jne	.LBB1_93
.LBB1_95:                               # %._crit_edge
                                        #   in Loop: Header=BB1_64 Depth=1
	testb	%al, %al
	jne	.LBB1_202
# BB#96:                                #   in Loop: Header=BB1_64 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	leaq	48(%rax), %rax
	cmoveq	24(%rsp), %rax          # 8-byte Folded Reload
	movq	%rbx, (%rax)
	movq	%rbx, 32(%rsp)          # 8-byte Spill
.LBB1_97:                               #   in Loop: Header=BB1_64 Depth=1
	movl	8(%rsp), %esi           # 4-byte Reload
	leaq	72(%rsp), %r15
.LBB1_98:                               #   in Loop: Header=BB1_64 Depth=1
	movl	40(%rsp), %ecx          # 4-byte Reload
	incl	%ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	movzwl	6(%rax), %eax
	xorl	%ebp, %ebp
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	cmpl	%eax, %ecx
	jb	.LBB1_64
	jmp	.LBB1_14
.LBB1_99:
	movl	$-124, %ebp
.LBB1_100:                              # %select.unfold290
	movq	%rbx, %rdi
	callq	free
	movq	16(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB1_102
.LBB1_101:                              #   in Loop: Header=BB1_102 Depth=1
	movq	24(%rdi), %rax
	movq	%rax, 16(%rbx)
	callq	free
.LBB1_102:                              # %select.unfold290
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_101
# BB#103:                               # %.preheader.i274
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB1_105
.LBB1_104:                              #   in Loop: Header=BB1_105 Depth=1
	movq	48(%rbx), %rax
	movq	%rax, (%r14)
	movq	16(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB1_105:                              # %.preheader.i274
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_104
	jmp	.LBB1_14
.LBB1_106:
	xorl	%ebp, %ebp
	jmp	.LBB1_14
.LBB1_107:
	movl	$-114, %ebp
	jmp	.LBB1_14
.LBB1_108:                              # %.us-lcssa.us
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movl	8(%rsp), %esi           # 4-byte Reload
	callq	cli_errmsg
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	movq	16(%rsp), %r14          # 8-byte Reload
	je	.LBB1_110
.LBB1_109:                              # =>This Inner Loop Header: Depth=1
	movq	24(%rdi), %rax
	movq	%rax, (%rbx)
	callq	free
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_109
.LBB1_110:                              # %.preheader.i
	movq	24(%r14), %rbx
	movl	$-123, %ebp
	testq	%rbx, %rbx
	je	.LBB1_14
.LBB1_111:                              # =>This Inner Loop Header: Depth=1
	movq	48(%rbx), %rax
	movq	%rax, 24(%r14)
	movq	16(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	24(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_111
	jmp	.LBB1_14
.LBB1_112:                              # %.us-lcssa355.us
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	24(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB1_114
.LBB1_113:                              #   in Loop: Header=BB1_114 Depth=1
	movq	24(%rdi), %rax
	movq	%rax, (%rbx)
	callq	free
.LBB1_114:                              # %.us-lcssa355.us
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_113
# BB#115:                               # %.preheader.i245
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	24(%r14), %rbx
	movl	$-114, %ebp
	testq	%rbx, %rbx
	je	.LBB1_14
.LBB1_116:                              # =>This Inner Loop Header: Depth=1
	movq	48(%rbx), %rax
	movq	%rax, 24(%r14)
	movq	16(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	24(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_116
	jmp	.LBB1_14
.LBB1_117:
	movl	%esi, %r14d
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	movq	16(%rsp), %rbp          # 8-byte Reload
	je	.LBB1_120
.LBB1_118:                              # =>This Inner Loop Header: Depth=1
	movq	24(%rdi), %rax
	movq	%rax, (%rbx)
	callq	free
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_118
	jmp	.LBB1_120
.LBB1_119:                              #   in Loop: Header=BB1_120 Depth=1
	movq	48(%rbx), %rax
	movq	%rax, 24(%rbp)
	movq	16(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB1_120:                              # =>This Inner Loop Header: Depth=1
	movq	24(%rbp), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_119
# BB#121:                               # %cab_free.exit251
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	jmp	.LBB1_27
.LBB1_122:                              # %cab_chkname.exit
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB1_123:
	movq	%r14, %rdi
	callq	free
	xorl	%esi, %esi
	movl	$1, %edx
	movl	(%rsp), %edi            # 4-byte Reload
	callq	lseek
	movq	%rax, %r15
	movl	$-123, %ebp
	cmpq	$-1, %r15
	je	.LBB1_14
# BB#124:
	leaq	128(%rsp), %rsi
	movl	$256, %edx              # imm = 0x100
	movl	(%rsp), %edi            # 4-byte Reload
	callq	read
	testl	%eax, %eax
	jle	.LBB1_13
# BB#125:                               # %.lr.ph.preheader.i195
	movslq	%eax, %rcx
	xorl	%eax, %eax
.LBB1_126:                              # %.lr.ph.i198
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, 128(%rsp,%rax)
	je	.LBB1_135
# BB#127:                               #   in Loop: Header=BB1_126 Depth=1
	incq	%rax
	cmpq	%rcx, %rax
	jl	.LBB1_126
	jmp	.LBB1_13
.LBB1_129:                              # %.us-lcssa354
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_errmsg
	movq	24(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB1_131
.LBB1_130:                              #   in Loop: Header=BB1_131 Depth=1
	movq	24(%rdi), %rax
	movq	%rax, (%rbx)
	callq	free
.LBB1_131:                              # %.us-lcssa354
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_130
# BB#132:                               # %.preheader.i241
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	24(%r14), %rbx
	movl	$-123, %ebp
	testq	%rbx, %rbx
	je	.LBB1_14
.LBB1_133:                              # =>This Inner Loop Header: Depth=1
	movq	48(%rbx), %rax
	movq	%rax, 24(%r14)
	movq	16(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	24(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_133
	jmp	.LBB1_14
.LBB1_135:
	leaq	1(%r15,%rax), %rsi
	xorl	%edx, %edx
	movl	(%rsp), %edi            # 4-byte Reload
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB1_14
# BB#136:
	leaq	128(%rsp), %rdi
	callq	cli_strdup
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB1_163
# BB#137:
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB1_142
# BB#138:                               # %.lr.ph.i206.preheader
	xorl	%r15d, %r15d
.LBB1_139:                              # %.lr.ph.i206
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%r14,%r15), %ebp
	movl	$.L.str.51, %edi
	movl	$16, %edx
	movl	%ebp, %esi
	callq	memchr
	testl	%ebp, %ebp
	js	.LBB1_164
# BB#140:                               # %.lr.ph.i206
                                        #   in Loop: Header=BB1_139 Depth=1
	testq	%rax, %rax
	jne	.LBB1_164
# BB#141:                               #   in Loop: Header=BB1_139 Depth=1
	incq	%r15
	cmpq	%rbx, %r15
	jb	.LBB1_139
.LBB1_142:                              # %.loopexit304
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	jmp	.LBB1_165
.LBB1_143:
	movq	16(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB1_145
.LBB1_144:                              #   in Loop: Header=BB1_145 Depth=1
	movq	24(%rdi), %rax
	movq	%rax, 16(%rbx)
	callq	free
.LBB1_145:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_144
# BB#146:                               # %.preheader.i253
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB1_148
.LBB1_147:                              #   in Loop: Header=BB1_148 Depth=1
	movq	48(%rbx), %rax
	movq	%rax, (%rbp)
	movq	16(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB1_148:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_147
# BB#149:                               # %cab_free.exit255
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	movl	8(%rsp), %esi           # 4-byte Reload
	jmp	.LBB1_27
.LBB1_150:
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	movl	40(%rsp), %esi          # 4-byte Reload
	callq	cli_errmsg
	movq	16(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB1_152
.LBB1_151:                              #   in Loop: Header=BB1_152 Depth=1
	movq	24(%rdi), %rax
	movq	%rax, 16(%rbx)
	callq	free
.LBB1_152:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_151
# BB#153:                               # %.preheader.i257
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rbx
	movl	$-123, %ebp
	testq	%rbx, %rbx
	je	.LBB1_14
# BB#154:
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB1_155:                              # =>This Inner Loop Header: Depth=1
	movq	48(%rbx), %rax
	movq	%rax, (%r14)
	movq	16(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_155
	jmp	.LBB1_14
.LBB1_156:
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB1_158
.LBB1_157:                              #   in Loop: Header=BB1_158 Depth=1
	movq	24(%rdi), %rax
	movq	%rax, 16(%rbp)
	callq	free
.LBB1_158:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_157
# BB#159:                               # %.preheader.i261
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rbx
	movl	$-114, %ebp
	testq	%rbx, %rbx
	je	.LBB1_14
# BB#160:
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB1_161:                              # =>This Inner Loop Header: Depth=1
	movq	48(%rbx), %rax
	movq	%rax, (%r14)
	movq	16(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_161
	jmp	.LBB1_14
.LBB1_163:
	movl	$-114, %ebp
	jmp	.LBB1_14
.LBB1_164:                              # %cab_chkname.exit208
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB1_165:
	movq	%r14, %rdi
	callq	free
	movq	16(%rsp), %rax          # 8-byte Reload
	movw	8(%rax), %ax
	movq	32(%rsp), %rbx          # 8-byte Reload
	movb	64(%rsp), %r14b         # 1-byte Reload
	movb	7(%rsp), %r15b          # 1-byte Reload
	testb	$2, %al
	je	.LBB1_25
.LBB1_166:
	movb	%r15b, 7(%rsp)          # 1-byte Spill
	movb	%r14b, 64(%rsp)         # 1-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movl	$1, %edx
	movl	(%rsp), %edi            # 4-byte Reload
	callq	lseek
	movq	%rax, %r15
	movl	$-123, %ebp
	cmpq	$-1, %r15
	je	.LBB1_14
# BB#167:
	leaq	128(%rsp), %rsi
	movl	$256, %edx              # imm = 0x100
	movl	(%rsp), %edi            # 4-byte Reload
	callq	read
	testl	%eax, %eax
	jle	.LBB1_13
# BB#168:                               # %.lr.ph.preheader.i210
	movslq	%eax, %rcx
	xorl	%eax, %eax
.LBB1_169:                              # %.lr.ph.i213
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, 128(%rsp,%rax)
	je	.LBB1_173
# BB#170:                               #   in Loop: Header=BB1_169 Depth=1
	incq	%rax
	cmpq	%rcx, %rax
	jl	.LBB1_169
	jmp	.LBB1_13
.LBB1_173:
	leaq	1(%r15,%rax), %rsi
	xorl	%edx, %edx
	movl	(%rsp), %edi            # 4-byte Reload
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB1_14
# BB#174:
	leaq	128(%rsp), %rdi
	callq	cli_strdup
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB1_181
# BB#175:
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB1_180
# BB#176:                               # %.lr.ph.i221.preheader
	xorl	%r15d, %r15d
.LBB1_177:                              # %.lr.ph.i221
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%r14,%r15), %ebp
	movl	$.L.str.51, %edi
	movl	$16, %edx
	movl	%ebp, %esi
	callq	memchr
	testl	%ebp, %ebp
	js	.LBB1_182
# BB#178:                               # %.lr.ph.i221
                                        #   in Loop: Header=BB1_177 Depth=1
	testq	%rax, %rax
	jne	.LBB1_182
# BB#179:                               #   in Loop: Header=BB1_177 Depth=1
	incq	%r15
	cmpq	%rbx, %r15
	jb	.LBB1_177
.LBB1_180:                              # %.loopexit303
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	jmp	.LBB1_183
.LBB1_181:
	movl	$-114, %ebp
	jmp	.LBB1_14
.LBB1_182:                              # %cab_chkname.exit223
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB1_183:
	movq	%r14, %rdi
	callq	free
	xorl	%esi, %esi
	movl	$1, %edx
	movl	(%rsp), %edi            # 4-byte Reload
	callq	lseek
	movq	%rax, %r15
	movl	$-123, %ebp
	cmpq	$-1, %r15
	je	.LBB1_14
# BB#184:
	leaq	128(%rsp), %rsi
	movl	$256, %edx              # imm = 0x100
	movl	(%rsp), %edi            # 4-byte Reload
	callq	read
	testl	%eax, %eax
	jle	.LBB1_13
# BB#185:                               # %.lr.ph.preheader.i225
	movslq	%eax, %rcx
	xorl	%eax, %eax
.LBB1_186:                              # %.lr.ph.i228
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, 128(%rsp,%rax)
	je	.LBB1_190
# BB#187:                               #   in Loop: Header=BB1_186 Depth=1
	incq	%rax
	cmpq	%rcx, %rax
	jl	.LBB1_186
	jmp	.LBB1_13
.LBB1_190:
	leaq	1(%r15,%rax), %rsi
	xorl	%edx, %edx
	movl	(%rsp), %edi            # 4-byte Reload
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB1_14
# BB#191:
	leaq	128(%rsp), %rdi
	callq	cli_strdup
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB1_198
# BB#192:
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB1_197
# BB#193:                               # %.lr.ph.i236.preheader
	xorl	%r15d, %r15d
.LBB1_194:                              # %.lr.ph.i236
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%r14,%r15), %ebp
	movl	$.L.str.51, %edi
	movl	$16, %edx
	movl	%ebp, %esi
	callq	memchr
	testl	%ebp, %ebp
	js	.LBB1_200
# BB#195:                               # %.lr.ph.i236
                                        #   in Loop: Header=BB1_194 Depth=1
	testq	%rax, %rax
	jne	.LBB1_200
# BB#196:                               #   in Loop: Header=BB1_194 Depth=1
	incq	%r15
	cmpq	%rbx, %r15
	jb	.LBB1_194
.LBB1_197:                              # %.loopexit
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	jmp	.LBB1_201
.LBB1_198:
	movl	$-114, %ebp
	jmp	.LBB1_14
.LBB1_199:
	movl	$-114, %ebp
	jmp	.LBB1_100
.LBB1_200:                              # %cab_chkname.exit238
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB1_201:
	movq	%r14, %rdi
	callq	free
	movq	32(%rsp), %rbx          # 8-byte Reload
	movb	64(%rsp), %r14b         # 1-byte Reload
	movb	7(%rsp), %r15b          # 1-byte Reload
	jmp	.LBB1_25
.LBB1_202:
	movq	16(%rbx), %rsi
	movl	$.L.str.40, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	16(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	16(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB1_204
.LBB1_203:                              #   in Loop: Header=BB1_204 Depth=1
	movq	24(%rdi), %rax
	movq	%rax, 16(%rbx)
	callq	free
.LBB1_204:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_203
# BB#205:                               # %.preheader.i278
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rbx
	movl	$-124, %ebp
	testq	%rbx, %rbx
	je	.LBB1_14
# BB#206:
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB1_207:                              # =>This Inner Loop Header: Depth=1
	movq	48(%rbx), %rax
	movq	%rax, (%r14)
	movq	16(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_207
	jmp	.LBB1_14
.Lfunc_end1:
	.size	cab_open, .Lfunc_end1-cab_open
	.cfi_endproc

	.globl	cab_extract
	.p2align	4, 0x90
	.type	cab_extract,@function
cab_extract:                            # @cab_extract
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 32
	subq	$4096, %rsp             # imm = 0x1000
.Lcfi21:
	.cfi_def_cfa_offset 4128
.Lcfi22:
	.cfi_offset %rbx, -32
.Lcfi23:
	.cfi_offset %r14, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB2_2
# BB#1:
	testq	%rbp, %rbp
	je	.LBB2_2
# BB#4:
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.LBB2_5
# BB#6:
	movl	28(%rbx), %edi
	movq	16(%rax), %rsi
	xorl	%edx, %edx
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB2_7
# BB#8:
	movl	$1, %edi
	movl	$38952, %esi            # imm = 0x9828
	callq	cli_calloc
	movq	%rax, 64(%rbx)
	testq	%rax, %rax
	je	.LBB2_9
# BB#10:
	movl	$577, %esi              # imm = 0x241
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	open
	movl	%eax, 32(%rbx)
	cmpl	$-1, %eax
	je	.LBB2_11
# BB#12:
	movq	40(%rbx), %rax
	movzwl	(%rax), %esi
	movl	%esi, %eax
	andb	$15, %al
	andl	$15, %esi
	cmpb	$3, %al
	ja	.LBB2_51
# BB#13:
	jmpq	*.LJTI2_0(,%rsi,8)
.LBB2_14:
	movq	8(%rbx), %rbp
	testq	%rbp, %rbp
	jle	.LBB2_22
# BB#15:
	testl	%ebp, %ebp
	js	.LBB2_55
# BB#16:                                # %.preheader.i
	cmpl	$4097, %ebp             # imm = 0x1001
	jb	.LBB2_20
# BB#17:                                # %.lr.ph.split.us.i.preheader
	movq	%rsp, %r14
.LBB2_18:                               # %.lr.ph.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$4096, %edx             # imm = 0x1000
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	cab_read
	cmpl	$-1, %eax
	je	.LBB2_21
# BB#19:                                #   in Loop: Header=BB2_18 Depth=1
	addl	$-4096, %ebp            # imm = 0xF000
	cmpl	$4097, %ebp             # imm = 0x1001
	jae	.LBB2_18
.LBB2_20:                               # %._crit_edge.i
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	cab_read
	cmpl	$-1, %eax
	jne	.LBB2_22
.LBB2_21:                               # %.us-lcssa.us.i
	movl	28(%rbx), %esi
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB2_22
.LBB2_2:
	movl	$.L.str.42, %edi
	jmp	.LBB2_3
.LBB2_5:
	movl	$.L.str.43, %edi
.LBB2_3:
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-111, %ebp
.LBB2_54:
	movl	%ebp, %eax
	addq	$4096, %rsp             # imm = 0x1000
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB2_7:
	movq	40(%rbx), %rax
	movl	16(%rax), %esi
	movl	$.L.str.44, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-123, %ebp
	jmp	.LBB2_54
.LBB2_9:
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-123, %ebp
	jmp	.LBB2_54
.LBB2_11:
	movl	$.L.str.46, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_errmsg
	movq	64(%rbx), %rdi
	callq	free
	movl	$-123, %ebp
	jmp	.LBB2_54
.LBB2_51:
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_warnmsg
	jmp	.LBB2_52
.LBB2_33:
	movl	$.L.str.47, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	28(%rbx), %edi
	movl	32(%rbx), %esi
	movl	$4096, %edx             # imm = 0x1000
	movl	$1, %ecx
	movl	$cab_read, %r9d
	movq	%rbx, %r8
	callq	mszip_init
	movq	%rax, %rdi
	movq	64(%rbx), %rax
	movq	%rdi, 38936(%rax)
	testq	%rdi, %rdi
	je	.LBB2_34
# BB#35:
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	jle	.LBB2_40
# BB#36:
	movb	$0, 8(%rdi)
	callq	mszip_decompress
	movq	64(%rbx), %rcx
	movq	38936(%rcx), %rdi
	movb	$1, 8(%rdi)
	testl	%eax, %eax
	jns	.LBB2_40
# BB#37:
	callq	mszip_free
	movq	64(%rbx), %rdi
	xorl	%esi, %esi
	movl	$38952, %edx            # imm = 0x9828
	callq	memset
	movl	28(%rbx), %edi
	movl	32(%rbx), %esi
	movl	$4096, %edx             # imm = 0x1000
	movl	$1, %ecx
	movl	$cab_read, %r9d
	movq	%rbx, %r8
	callq	mszip_init
	movq	64(%rbx), %rdi
	movq	%rax, 38936(%rdi)
	testq	%rax, %rax
	je	.LBB2_38
# BB#39:
	movl	28(%rbx), %edi
	movq	40(%rbx), %rax
	movq	16(%rax), %rsi
	xorl	%edx, %edx
	callq	lseek
	movq	64(%rbx), %rax
	movq	38936(%rax), %rdi
.LBB2_40:
	movl	(%rbx), %esi
	callq	mszip_decompress
	movl	%eax, %ebp
	movq	64(%rbx), %rax
	movq	38936(%rax), %rdi
	callq	mszip_free
	jmp	.LBB2_53
.LBB2_41:
	movl	$.L.str.48, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	28(%rbx), %edi
	movl	32(%rbx), %esi
	movq	40(%rbx), %rax
	movl	(%rax), %edx
	shrl	$8, %edx
	andl	$31, %edx
	movl	$4096, %ecx             # imm = 0x1000
	movl	$cab_read, %r9d
	movq	%rbx, %r8
	callq	qtm_init
	movq	%rax, %rdi
	movq	64(%rbx), %rax
	movq	%rdi, 38936(%rax)
	testq	%rdi, %rdi
	je	.LBB2_34
# BB#42:
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	jle	.LBB2_44
# BB#43:
	movb	$0, 8(%rdi)
	callq	qtm_decompress
	movq	64(%rbx), %rax
	movq	38936(%rax), %rdi
	movb	$1, 8(%rdi)
.LBB2_44:
	movl	(%rbx), %esi
	callq	qtm_decompress
	movl	%eax, %ebp
	movq	64(%rbx), %rax
	movq	38936(%rax), %rdi
	callq	qtm_free
	jmp	.LBB2_53
.LBB2_45:
	movl	$.L.str.49, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	28(%rbx), %edi
	movl	32(%rbx), %esi
	movq	40(%rbx), %rax
	movl	(%rax), %edx
	shrl	$8, %edx
	andl	$31, %edx
	movl	$0, %ecx
	movl	$4096, %r8d             # imm = 0x1000
	movl	$0, %r9d
	pushq	$cab_read
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	callq	lzx_init
	addq	$16, %rsp
.Lcfi27:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %rdi
	movq	64(%rbx), %rax
	movq	%rdi, 38936(%rax)
	testq	%rdi, %rdi
	je	.LBB2_34
# BB#46:
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	jle	.LBB2_50
# BB#47:
	movb	$0, 8(%rdi)
	callq	lzx_decompress
	movq	64(%rbx), %rcx
	movq	38936(%rcx), %rdi
	movb	$1, 8(%rdi)
	testl	%eax, %eax
	jns	.LBB2_50
# BB#48:
	callq	lzx_free
	movq	64(%rbx), %rdi
	xorl	%esi, %esi
	movl	$38952, %edx            # imm = 0x9828
	callq	memset
	movl	28(%rbx), %edi
	movl	32(%rbx), %esi
	movq	40(%rbx), %rax
	movl	(%rax), %edx
	shrl	$8, %edx
	andl	$31, %edx
	movl	$0, %ecx
	movl	$4096, %r8d             # imm = 0x1000
	movl	$0, %r9d
	pushq	$cab_read
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	callq	lzx_init
	addq	$16, %rsp
.Lcfi30:
	.cfi_adjust_cfa_offset -16
	movq	64(%rbx), %rdi
	movq	%rax, 38936(%rdi)
	testq	%rax, %rax
	je	.LBB2_38
# BB#49:
	movl	28(%rbx), %edi
	movq	40(%rbx), %rax
	movq	16(%rax), %rsi
	xorl	%edx, %edx
	callq	lseek
	movq	64(%rbx), %rax
	movq	38936(%rax), %rdi
.LBB2_50:
	movl	(%rbx), %esi
	callq	lzx_decompress
	movl	%eax, %ebp
	movq	64(%rbx), %rax
	movq	38936(%rax), %rdi
	callq	lzx_free
	jmp	.LBB2_53
.LBB2_34:
	movq	%rax, %rdi
.LBB2_38:
	callq	free
	movl	32(%rbx), %edi
	callq	close
	movl	$-109, %ebp
	jmp	.LBB2_54
.LBB2_55:
	movl	$.L.str.53, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB2_22:                               # %cab_unstore.exit
	movq	%rsp, %r14
	movl	(%rbx), %ebp
	testl	%ebp, %ebp
	jns	.LBB2_23
# BB#32:
	movl	$.L.str.53, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB2_52:
	movl	$-124, %ebp
	jmp	.LBB2_53
.LBB2_31:                               #   in Loop: Header=BB2_23 Depth=1
	addl	$-4096, %ebp            # imm = 0xF000
.LBB2_23:                               # %.preheader.i95
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$4097, %ebp             # imm = 0x1001
	jb	.LBB2_24
# BB#29:                                # %.lr.ph.split.i
                                        #   in Loop: Header=BB2_23 Depth=1
	movl	$4096, %edx             # imm = 0x1000
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	cab_read
	cmpl	$-1, %eax
	je	.LBB2_25
# BB#30:                                #   in Loop: Header=BB2_23 Depth=1
	movl	32(%rbx), %edi
	movl	$4096, %edx             # imm = 0x1000
	movq	%r14, %rsi
	callq	cli_writen
	cmpl	$-1, %eax
	jne	.LBB2_31
	jmp	.LBB2_28
.LBB2_24:                               # %._crit_edge.i98
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	cab_read
	cmpl	$-1, %eax
	je	.LBB2_25
# BB#27:
	movl	32(%rbx), %edi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	cli_writen
	xorl	%ebp, %ebp
	cmpl	$-1, %eax
	jne	.LBB2_53
.LBB2_28:
	movl	32(%rbx), %esi
	movl	$.L.str.55, %edi
	jmp	.LBB2_26
.LBB2_25:
	movl	28(%rbx), %esi
	movl	$.L.str.54, %edi
.LBB2_26:                               # %cab_unstore.exit101
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-123, %ebp
.LBB2_53:
	movq	64(%rbx), %rdi
	callq	free
	movl	32(%rbx), %edi
	callq	close
	jmp	.LBB2_54
.Lfunc_end2:
	.size	cab_extract, .Lfunc_end2-cab_extract
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_14
	.quad	.LBB2_33
	.quad	.LBB2_41
	.quad	.LBB2_45

	.text
	.p2align	4, 0x90
	.type	cab_read,@function
cab_read:                               # @cab_read
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 80
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r13
	movzwl	%dx, %ecx
	testw	%cx, %cx
	movl	%edx, 4(%rsp)           # 4-byte Spill
	je	.LBB3_6
# BB#1:                                 # %.lr.ph.lr.ph
	movl	%edx, %r14d
	jmp	.LBB3_2
	.p2align	4, 0x90
.LBB3_3:                                #   in Loop: Header=BB3_2 Depth=1
	leaq	8(%r15), %r12
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	38944(%r15), %eax
	movl	%eax, %ecx
	incl	%ecx
	movw	%cx, 38944(%r15)
	movq	40(%r13), %rcx
	cmpw	2(%rcx), %ax
	jae	.LBB3_5
# BB#7:                                 #   in Loop: Header=BB3_4 Depth=2
	movl	28(%r13), %ebp
	movq	56(%r13), %rax
	movzbl	12(%rax), %ebx
	movl	$8, %edx
	movl	%ebp, %edi
	leaq	8(%rsp), %rsi
	callq	cli_readn
	cmpl	$8, %eax
	jne	.LBB3_8
# BB#9:                                 #   in Loop: Header=BB3_4 Depth=2
	testb	%bl, %bl
	je	.LBB3_12
# BB#10:                                #   in Loop: Header=BB3_4 Depth=2
	movl	$1, %edx
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB3_11
.LBB3_12:                               #   in Loop: Header=BB3_4 Depth=2
	movzwl	12(%rsp), %edx
	cmpl	$38913, %edx            # imm = 0x9801
	movw	%dx, 38928(%r15)
	jae	.LBB3_13
# BB#15:                                #   in Loop: Header=BB3_4 Depth=2
	movzwl	14(%rsp), %eax
	movw	%ax, 38930(%r15)
	cmpl	$32769, %eax            # imm = 0x8001
	jae	.LBB3_16
# BB#17:                                #   in Loop: Header=BB3_4 Depth=2
	leaq	16(%r15), %rbx
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	cli_readn
	movzwl	38928(%r15), %ecx
	cmpl	%ecx, %eax
	jne	.LBB3_18
# BB#22:                                #   in Loop: Header=BB3_4 Depth=2
	movq	%rbx, (%r15)
	leaq	16(%r15,%rcx), %rax
	movq	%rax, (%r12)
	movl	$0, 24(%r13)
	movq	40(%r13), %rcx
	movzwl	(%rcx), %eax
	andl	$15, %eax
	cmpl	$2, %eax
	jne	.LBB3_24
# BB#23:                                #   in Loop: Header=BB3_4 Depth=2
	movq	64(%r13), %rax
	movq	8(%rax), %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movb	$-1, (%rcx)
	movq	40(%r13), %rcx
.LBB3_24:                               #   in Loop: Header=BB3_4 Depth=2
	movq	64(%r13), %rdx
	movzwl	38944(%rdx), %eax
	cmpw	2(%rcx), %ax
	jae	.LBB3_25
# BB#29:                                #   in Loop: Header=BB3_4 Depth=2
	movzwl	38930(%rdx), %eax
	cmpl	$32768, %eax            # imm = 0x8000
	je	.LBB3_27
# BB#30:                                #   in Loop: Header=BB3_4 Depth=2
	movl	$.L.str.56, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB3_27
	.p2align	4, 0x90
.LBB3_25:                               #   in Loop: Header=BB3_4 Depth=2
	movzwl	(%rcx), %ecx
	andl	$15, %ecx
	cmpl	$3, %ecx
	jne	.LBB3_27
# BB#26:                                #   in Loop: Header=BB3_4 Depth=2
	movq	38936(%rdx), %rdi
	shll	$15, %eax
	movzwl	38930(%rdx), %ecx
	leal	-32768(%rax,%rcx), %eax
	movslq	%eax, %rsi
	callq	lzx_set_output_length
.LBB3_27:                               # %.lr.ph.split.backedge
                                        #   in Loop: Header=BB3_4 Depth=2
	movq	64(%r13), %r15
	leaq	8(%r15), %r12
	movq	(%r15), %rsi
	movq	8(%r15), %rax
	subq	%rsi, %rax
	testw	%ax, %ax
	je	.LBB3_4
	jmp	.LBB3_28
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph.split.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_4 Depth 2
	movl	%ecx, (%rsp)            # 4-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	64(%r13), %r15
	movq	(%r15), %rsi
	movq	8(%r15), %rax
	subq	%rsi, %rax
	testw	%ax, %ax
	je	.LBB3_3
.LBB3_28:                               # %.outer
                                        #   in Loop: Header=BB3_2 Depth=1
	movzwl	%ax, %ecx
	cmpl	(%rsp), %ecx            # 4-byte Folded Reload
	cmovaw	%r14w, %ax
	movzwl	%ax, %ebx
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	movq	64(%r13), %rax
	addq	%rbx, (%rax)
	addq	%rbx, %rbp
	subw	%bx, %r14w
	movzwl	%r14w, %ecx
	jne	.LBB3_2
	jmp	.LBB3_6
.LBB3_5:                                # %.us-lcssa81.us
	movl	$-124, 24(%r13)
	movl	(%rsp), %ecx            # 4-byte Reload
.LBB3_6:                                # %.loopexit
	movl	4(%rsp), %eax           # 4-byte Reload
	subl	%ecx, %eax
	jmp	.LBB3_21
.LBB3_8:                                # %.us-lcssa82.us
	movl	$.L.str.57, %edi
	jmp	.LBB3_19
.LBB3_13:                               # %.us-lcssa84.us
	movl	$.L.str.59, %edi
	jmp	.LBB3_14
.LBB3_16:                               # %.us-lcssa85.us
	movl	$.L.str.60, %edi
.LBB3_14:                               # %cab_read_block.exit.thread
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-124, %eax
	jmp	.LBB3_20
.LBB3_18:                               # %.us-lcssa86.us
	movl	$.L.str.61, %edi
	jmp	.LBB3_19
.LBB3_11:                               # %.us-lcssa83.us
	movl	$.L.str.58, %edi
.LBB3_19:                               # %cab_read_block.exit.thread
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-123, %eax
.LBB3_20:                               # %cab_read_block.exit.thread
	movl	%eax, 24(%r13)
	movl	$-1, %eax
.LBB3_21:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	cab_read, .Lfunc_end3-cab_read
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"cab_open: Can't lseek to %u (offset)\n"
	.size	.L.str, 38

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"cab_open: Can't read cabinet header\n"
	.size	.L.str.1, 37

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"cab_open: Incorrect CAB signature\n"
	.size	.L.str.2, 35

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"CAB: -------------- Cabinet file ----------------\n"
	.size	.L.str.3, 51

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"cab_open: Can't fstat descriptor %d\n"
	.size	.L.str.4, 37

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"CAB: Cabinet length: %u\n"
	.size	.L.str.5, 25

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"cab_open: No folders in cabinet (fake cab?)\n"
	.size	.L.str.6, 45

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"CAB: Folders: %u\n"
	.size	.L.str.7, 18

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"CAB: *** Number of folders limited to %u ***\n"
	.size	.L.str.8, 46

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"cab_open: No files in cabinet (fake cab?)\n"
	.size	.L.str.9, 43

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"CAB: Files: %u\n"
	.size	.L.str.10, 16

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"CAB: *** Number of files limited to %u ***\n"
	.size	.L.str.11, 44

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"CAB: File format version: %u.%u\n"
	.size	.L.str.12, 33

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"cab_open: Can't read file header (fake cab?)\n"
	.size	.L.str.13, 46

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"cab_open: Can't lseek to %u (fake cab?)\n"
	.size	.L.str.14, 41

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"CAB: Preceeding cabinet name: %s\n"
	.size	.L.str.15, 34

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"CAB: Preceeding cabinet info: %s\n"
	.size	.L.str.16, 34

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"CAB: Next cabinet name: %s\n"
	.size	.L.str.17, 28

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"CAB: Next cabinet info: %s\n"
	.size	.L.str.18, 28

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"CAB: bscore == %u, most likely a fake cabinet\n"
	.size	.L.str.19, 47

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"cab_open: Can't read header for folder %u\n"
	.size	.L.str.20, 43

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"cab_open: Can't lseek to %u (resfold)\n"
	.size	.L.str.21, 39

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"cab_open: Can't allocate memory for folder\n"
	.size	.L.str.22, 44

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"CAB: Folder record %u\n"
	.size	.L.str.23, 23

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"CAB: Folder offset: %u\n"
	.size	.L.str.24, 24

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"CAB: Folder compression method: %d\n"
	.size	.L.str.25, 36

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"cab_open: Can't read file %u header\n"
	.size	.L.str.26, 37

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"cab_open: Can't allocate memory for file\n"
	.size	.L.str.27, 42

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"CAB: File record %u\n"
	.size	.L.str.28, 21

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"CAB: File name: %s\n"
	.size	.L.str.29, 20

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"CAB: File offset: %u\n"
	.size	.L.str.30, 22

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"CAB: File folder index: %u\n"
	.size	.L.str.31, 28

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"CAB: File attribs: 0x%x\n"
	.size	.L.str.32, 25

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"CAB:   * file is read-only\n"
	.size	.L.str.33, 28

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"CAB:   * file is hidden\n"
	.size	.L.str.34, 25

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"CAB:   * file is a system file\n"
	.size	.L.str.35, 32

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"CAB:   * file modified since last backup\n"
	.size	.L.str.36, 42

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"CAB:   * file to be run after extraction\n"
	.size	.L.str.37, 42

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"CAB:   * file name contains UTF\n"
	.size	.L.str.38, 33

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"cab_open: File %s is not associated with any folder\n"
	.size	.L.str.39, 53

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"cab_open: Folder not found for file %s\n"
	.size	.L.str.40, 40

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"CAB: File is split *skipping*\n"
	.size	.L.str.41, 31

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"cab_extract: !file || !name\n"
	.size	.L.str.42, 29

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"cab_extract: file->folder == NULL\n"
	.size	.L.str.43, 35

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"cab_extract: Can't lseek to %u\n"
	.size	.L.str.44, 32

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"cab_extract: Can't allocate memory for internal state\n"
	.size	.L.str.45, 55

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"cab_extract: Can't open file %s in write mode\n"
	.size	.L.str.46, 47

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"CAB: Compression method: MSZIP\n"
	.size	.L.str.47, 32

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"CAB: Compression method: QUANTUM\n"
	.size	.L.str.48, 34

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"CAB: Compression method: LZX\n"
	.size	.L.str.49, 30

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"CAB: Not supported compression method: 0x%x\n"
	.size	.L.str.50, 45

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"%/*?|\\\"+=<>;:\t "
	.size	.L.str.51, 16

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"cab_chkname: File name contains disallowed characters\n"
	.size	.L.str.52, 55

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"cab_unstore: bytes < 0\n"
	.size	.L.str.53, 24

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"cab_unstore: cab_read failed for descriptor %d\n"
	.size	.L.str.54, 48

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"cab_unstore: Can't write to descriptor %d\n"
	.size	.L.str.55, 43

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"cab_read: WARNING: partial data block\n"
	.size	.L.str.56, 39

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"cab_read_block: Can't read block header\n"
	.size	.L.str.57, 41

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"cab_read_block: lseek failed\n"
	.size	.L.str.58, 30

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"cab_read_block: block size > CAB_INPUTMAX\n"
	.size	.L.str.59, 43

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"cab_read_block: output size > CAB_BLOCKMAX\n"
	.size	.L.str.60, 44

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"cab_read_block: Can't read block data\n"
	.size	.L.str.61, 39


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
