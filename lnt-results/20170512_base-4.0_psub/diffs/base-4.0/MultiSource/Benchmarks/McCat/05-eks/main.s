	.text
	.file	"main.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	comp
	.p2align	4, 0x90
	.type	comp,@function
comp:                                   # @comp
	.cfi_startproc
# BB#0:
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movaps	.LCPI0_0(%rip), %xmm1   # xmm1 = [nan,nan]
	andps	%xmm1, %xmm0
	movsd	(%rsi), %xmm2           # xmm2 = mem[0],zero
	andps	%xmm1, %xmm2
	xorl	%eax, %eax
	ucomisd	%xmm2, %xmm0
	movl	$-1, %ecx
	cmovbel	%eax, %ecx
	ucomisd	%xmm0, %xmm2
	movl	$1, %eax
	cmovbel	%ecx, %eax
	retq
.Lfunc_end0:
	.size	comp, .Lfunc_end0-comp
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 96
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movl	$2, %r12d
	leaq	13(%rsp), %r14
	leaq	16(%rsp), %r15
	.p2align	4, 0x90
.LBB1_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_2 Depth 2
                                        #     Child Loop BB1_4 Depth 2
	movl	$7102838, 16(%rsp)      # imm = 0x6C6176
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%r12d, %edx
	callq	sprintf
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	strcat
	movl	%r12d, %edi
	callq	MakeMatrix
	movq	%rax, A(%rip)
	movq	%rax, %rdi
	movl	%r12d, %esi
	callq	Jacobi
	movq	%rax, U(%rip)
	movq	A(%rip), %rdi
	movq	%rax, %rsi
	callq	QRiterate
	callq	newVector
	movq	%rax, %rbx
	movq	A(%rip), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_2:                                #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rcx), %rdx
	movq	(%rdx,%rcx), %rdx
	movq	%rdx, (%rbx,%rcx)
	movq	8(%rax,%rcx), %rdx
	movq	8(%rdx,%rcx), %rdx
	movq	%rdx, 8(%rbx,%rcx)
	movq	16(%rax,%rcx), %rdx
	movq	16(%rdx,%rcx), %rdx
	movq	%rdx, 16(%rbx,%rcx)
	addq	$24, %rcx
	cmpq	$408, %rcx              # imm = 0x198
	jne	.LBB1_2
# BB#3:                                 #   in Loop: Header=BB1_1 Depth=1
	movl	$51, %esi
	movl	$8, %edx
	movl	$comp, %ecx
	movq	%rbx, %rdi
	callq	qsort
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_4:                                #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	stdout(%rip), %rdi
	movsd	(%rbx,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$1, %al
	movl	%ebp, %edx
	callq	fprintf
	incq	%rbp
	cmpq	$51, %rbp
	jne	.LBB1_4
# BB#5:                                 # %.loopexit
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	U(%rip), %rdi
	callq	freeMatrix
	movq	A(%rip), %rdi
	callq	freeMatrix
	incl	%r12d
	cmpl	$6, %r12d
	jne	.LBB1_1
# BB#6:
	xorl	%eax, %eax
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	Check
	.p2align	4, 0x90
	.type	Check,@function
Check:                                  # @Check
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 48
.Lcfi16:
	.cfi_offset %rbx, -48
.Lcfi17:
	.cfi_offset %r12, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	callq	newMatrix
	movq	%rax, %r14
	movl	%r12d, %edi
	callq	MakeMatrix
	movq	%rax, %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	matrixMult
	movq	%rbp, %rdi
	callq	matrixTranspose
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	matrixMult
	movq	%rbp, %rdi
	callq	matrixTranspose
	xorpd	%xmm0, %xmm0
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_2 Depth 2
	movq	(%rbx,%rax,8), %rcx
	movq	(%r15,%rax,8), %rdx
	movl	$2, %esi
	.p2align	4, 0x90
.LBB2_2:                                #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-16(%rcx,%rsi,8), %xmm1 # xmm1 = mem[0],zero
	subsd	-16(%rdx,%rsi,8), %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movsd	-8(%rcx,%rsi,8), %xmm2  # xmm2 = mem[0],zero
	subsd	-8(%rdx,%rsi,8), %xmm2
	mulsd	%xmm2, %xmm2
	addsd	%xmm1, %xmm2
	movsd	(%rcx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	subsd	(%rdx,%rsi,8), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm2, %xmm0
	addq	$3, %rsi
	cmpq	$53, %rsi
	jne	.LBB2_2
# BB#3:                                 #   in Loop: Header=BB2_1 Depth=1
	incq	%rax
	cmpq	$51, %rax
	jne	.LBB2_1
# BB#4:
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB2_6
# BB#5:                                 # %call.sqrt
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB2_6:                                # %.split
	movl	$.L.str.5, %edi
	movb	$1, %al
	movl	%r12d, %esi
	movapd	%xmm1, %xmm0
	callq	printf
	xorpd	%xmm0, %xmm0
	movl	$1, %eax
	xorl	%ecx, %ecx
	movapd	.LCPI2_0(%rip), %xmm1   # xmm1 = [nan,nan]
	movapd	.LCPI2_0(%rip), %xmm2   # xmm2 = [nan,nan]
	.p2align	4, 0x90
.LBB2_8:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_14 Depth 2
	movq	%rcx, %rdx
	leaq	1(%rdx), %rcx
	cmpq	$50, %rcx
	jg	.LBB2_7
# BB#9:                                 # %.lr.ph
                                        #   in Loop: Header=BB2_8 Depth=1
	movq	(%rbx,%rdx,8), %rsi
	testb	$1, %dl
	jne	.LBB2_11
# BB#10:                                #   in Loop: Header=BB2_8 Depth=1
	movq	%rax, %rdi
	cmpq	$49, %rdx
	jne	.LBB2_13
	jmp	.LBB2_7
	.p2align	4, 0x90
.LBB2_11:                               #   in Loop: Header=BB2_8 Depth=1
	movsd	(%rsi,%rax,8), %xmm3    # xmm3 = mem[0],zero
	movq	(%rbx,%rax,8), %rdi
	subsd	(%rdi,%rdx,8), %xmm3
	andpd	%xmm2, %xmm3
	addsd	%xmm3, %xmm0
	leaq	1(%rax), %rdi
	cmpq	$49, %rdx
	je	.LBB2_7
.LBB2_13:                               # %.lr.ph.new
                                        #   in Loop: Header=BB2_8 Depth=1
	incq	%rdi
	.p2align	4, 0x90
.LBB2_14:                               #   Parent Loop BB2_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rsi,%rdi,8), %xmm3  # xmm3 = mem[0],zero
	movq	-8(%rbx,%rdi,8), %rbp
	subsd	(%rbp,%rdx,8), %xmm3
	andpd	%xmm1, %xmm3
	addsd	%xmm0, %xmm3
	movsd	(%rsi,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	movq	(%rbx,%rdi,8), %rbp
	subsd	(%rbp,%rdx,8), %xmm0
	andpd	%xmm1, %xmm0
	addsd	%xmm3, %xmm0
	addq	$2, %rdi
	cmpq	$52, %rdi
	jne	.LBB2_14
.LBB2_7:                                # %.loopexit
                                        #   in Loop: Header=BB2_8 Depth=1
	incq	%rax
	cmpq	$51, %rcx
	jne	.LBB2_8
# BB#15:
	movl	$.L.str.6, %edi
	movb	$1, %al
	callq	printf
	movl	$.Lstr, %edi
	callq	puts
	movq	%r14, %rdi
	callq	freeMatrix
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	freeMatrix              # TAILCALL
.Lfunc_end2:
	.size	Check, .Lfunc_end2-Check
	.cfi_endproc

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.cst4,"aM",@progbits,4
.L.str.1:
	.asciz	"%i\000"
	.size	.L.str.1, 4

	.type	A,@object               # @A
	.comm	A,8,8
	.type	U,@object               # @U
	.comm	U,8,8
	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"%i %e\n"
	.size	.L.str.2, 7

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Step: %i !! The frobenius norm of X-T is %e\n"
	.size	.L.str.5, 45

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Is A symmetric? %e\n"
	.size	.L.str.6, 20

	.type	Q,@object               # @Q
	.comm	Q,8,8
	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"\n"
	.size	.Lstr, 2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
