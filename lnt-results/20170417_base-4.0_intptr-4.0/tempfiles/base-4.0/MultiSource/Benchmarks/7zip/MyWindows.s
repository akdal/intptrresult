	.text
	.file	"MyWindows.bc"
	.globl	SysAllocStringByteLen
	.p2align	4, 0x90
	.type	SysAllocStringByteLen,@function
SysAllocStringByteLen:                  # @SysAllocStringByteLen
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movl	%esi, %r12d
	movq	%rdi, %r14
	leal	11(%r12), %eax
	movslq	%eax, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_1
# BB#2:
	movl	%r12d, %r15d
	movl	%r12d, (%rbx)
	addq	$4, %rbx
	testq	%r14, %r14
	je	.LBB0_4
# BB#3:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	memcpy
.LBB0_4:
	movb	$0, 6(%rbx,%r15)
	movw	$0, 4(%rbx,%r15)
	movl	$0, (%rbx,%r15)
	jmp	.LBB0_5
.LBB0_1:
	xorl	%ebx, %ebx
.LBB0_5:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	SysAllocStringByteLen, .Lfunc_end0-SysAllocStringByteLen
	.cfi_endproc

	.globl	SysAllocString
	.p2align	4, 0x90
	.type	SysAllocString,@function
SysAllocString:                         # @SysAllocString
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r12, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	xorl	%r12d, %r12d
	testq	%r14, %r14
	je	.LBB1_6
# BB#1:                                 # %.preheader.preheader
	movl	$-4, %ebx
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	%r12d, %eax
	incl	%r12d
	addl	$4, %ebx
	cmpl	$0, (%r14,%rax,4)
	jne	.LBB1_2
# BB#3:                                 # %_ZL11MyStringLenPKw.exit
	leal	4(%rbx), %r15d
	leaq	4(%r15), %rdi
	callq	malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB1_4
# BB#5:
	movl	%ebx, (%r12)
	addq	$4, %r12
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	memcpy
	jmp	.LBB1_6
.LBB1_4:
	xorl	%r12d, %r12d
.LBB1_6:
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	SysAllocString, .Lfunc_end1-SysAllocString
	.cfi_endproc

	.globl	SysFreeString
	.p2align	4, 0x90
	.type	SysFreeString,@function
SysFreeString:                          # @SysFreeString
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB2_1
# BB#2:
	addq	$-4, %rdi
	jmp	free                    # TAILCALL
.LBB2_1:
	retq
.Lfunc_end2:
	.size	SysFreeString, .Lfunc_end2-SysFreeString
	.cfi_endproc

	.globl	SysStringByteLen
	.p2align	4, 0x90
	.type	SysStringByteLen,@function
SysStringByteLen:                       # @SysStringByteLen
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB3_1
# BB#2:
	movl	-4(%rdi), %eax
	retq
.LBB3_1:
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	SysStringByteLen, .Lfunc_end3-SysStringByteLen
	.cfi_endproc

	.globl	SysStringLen
	.p2align	4, 0x90
	.type	SysStringLen,@function
SysStringLen:                           # @SysStringLen
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB4_1
# BB#2:
	movl	-4(%rdi), %eax
	shrl	$2, %eax
	retq
.LBB4_1:
	xorl	%eax, %eax
	retq
.Lfunc_end4:
	.size	SysStringLen, .Lfunc_end4-SysStringLen
	.cfi_endproc

	.globl	VariantClear
	.p2align	4, 0x90
	.type	VariantClear,@function
VariantClear:                           # @VariantClear
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
.Lcfi19:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movzwl	(%rbx), %eax
	cmpl	$8, %eax
	jne	.LBB5_3
# BB#1:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_3
# BB#2:
	addq	$-4, %rdi
	callq	free
.LBB5_3:                                # %SysFreeString.exit
	movw	$0, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	VariantClear, .Lfunc_end5-VariantClear
	.cfi_endproc

	.globl	VariantCopy
	.p2align	4, 0x90
	.type	VariantCopy,@function
VariantCopy:                            # @VariantCopy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 48
.Lcfi25:
	.cfi_offset %rbx, -48
.Lcfi26:
	.cfi_offset %r12, -40
.Lcfi27:
	.cfi_offset %r13, -32
.Lcfi28:
	.cfi_offset %r14, -24
.Lcfi29:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movzwl	(%r15), %eax
	cmpl	$8, %eax
	jne	.LBB6_3
# BB#1:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB6_3
# BB#2:
	addq	$-4, %rdi
	callq	free
.LBB6_3:                                # %VariantClear.exit
	movw	$0, (%r15)
	movzwl	(%rbx), %eax
	cmpl	$8, %eax
	jne	.LBB6_12
# BB#4:
	movq	8(%rbx), %r14
	testq	%r14, %r14
	je	.LBB6_5
# BB#6:
	movl	-4(%r14), %r13d
	jmp	.LBB6_7
.LBB6_12:
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r15)
	jmp	.LBB6_13
.LBB6_5:
	xorl	%r13d, %r13d
.LBB6_7:                                # %SysStringByteLen.exit
	leal	11(%r13), %eax
	movslq	%eax, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB6_8
# BB#9:
	movl	%r13d, %r12d
	movq	%rbx, %rax
	addq	$4, %rbx
	testq	%r14, %r14
	movl	%r13d, (%rax)
	je	.LBB6_11
# BB#10:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	memcpy
.LBB6_11:                               # %SysAllocStringByteLen.exit
	movb	$0, 6(%rbx,%r12)
	movw	$0, 4(%rbx,%r12)
	movl	$0, (%rbx,%r12)
	movq	%rbx, 8(%r15)
	movw	$8, (%r15)
.LBB6_13:
	xorl	%eax, %eax
	jmp	.LBB6_14
.LBB6_8:                                # %SysAllocStringByteLen.exit.thread
	movq	$0, 8(%r15)
	movl	$-2147024882, %eax      # imm = 0x8007000E
.LBB6_14:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	VariantCopy, .Lfunc_end6-VariantCopy
	.cfi_endproc

	.globl	CompareFileTime
	.p2align	4, 0x90
	.type	CompareFileTime,@function
CompareFileTime:                        # @CompareFileTime
	.cfi_startproc
# BB#0:
	movl	4(%rsi), %ecx
	movl	$-1, %eax
	cmpl	%ecx, 4(%rdi)
	jb	.LBB7_4
# BB#1:
	movl	$1, %eax
	ja	.LBB7_4
# BB#2:
	movl	(%rdi), %ecx
	movl	(%rsi), %edx
	cmpl	%edx, %ecx
	movl	$-1, %eax
	jb	.LBB7_4
# BB#3:
	cmpl	%ecx, %edx
	sbbl	%eax, %eax
	andl	$1, %eax
.LBB7_4:
	retq
.Lfunc_end7:
	.size	CompareFileTime, .Lfunc_end7-CompareFileTime
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
