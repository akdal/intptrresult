	.text
	.file	"args.bc"
	.globl	mylog
	.p2align	4, 0x90
	.type	mylog,@function
mylog:                                  # @mylog
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	$2, %edi
	jl	.LBB0_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	addl	%ecx, %ecx
	incl	%eax
	cmpl	%edi, %ecx
	jl	.LBB0_2
.LBB0_3:                                # %._crit_edge
	retq
.Lfunc_end0:
	.size	mylog, .Lfunc_end0-mylog
	.cfi_endproc

	.globl	dealwithargs
	.p2align	4, 0x90
	.type	dealwithargs,@function
dealwithargs:                           # @dealwithargs
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	cmpl	$4, %edi
	jl	.LBB1_2
# BB#1:                                 # %.thread
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, flag(%rip)
	jmp	.LBB1_3
.LBB1_2:
	movl	$1, flag(%rip)
	cmpl	$3, %edi
	jne	.LBB1_5
.LBB1_3:                                # %.thread8
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, NumNodes(%rip)
.LBB1_4:
	movq	8(%rbx), %rdi
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	NumNodes(%rip), %ecx
	cmpl	$2, %ecx
	jge	.LBB1_7
	jmp	.LBB1_9
.LBB1_5:
	movl	$4, NumNodes(%rip)
	cmpl	$2, %edi
	jge	.LBB1_4
# BB#6:
	movl	$4, %ecx
	movl	$32768, %eax            # imm = 0x8000
.LBB1_7:                                # %.lr.ph.i.preheader
	xorl	%ebx, %ebx
	movl	$1, %edx
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	addl	%edx, %edx
	incl	%ebx
	cmpl	%ecx, %edx
	jl	.LBB1_8
.LBB1_9:                                # %mylog.exit
	movl	%ebx, NDim(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	retq
.Lfunc_end1:
	.size	dealwithargs, .Lfunc_end1-dealwithargs
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
