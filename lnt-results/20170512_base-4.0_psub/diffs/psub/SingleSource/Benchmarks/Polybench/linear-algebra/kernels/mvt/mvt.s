	.text
	.file	"mvt.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4661014508095930368     # double 4000
.LCPI6_1:
	.quad	4607182418800017408     # double 1
.LCPI6_2:
	.quad	4613937818241073152     # double 3
.LCPI6_3:
	.quad	4616189618054758400     # double 4
.LCPI6_5:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_4:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 80
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$128000000, %edx        # imm = 0x7A12000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_50
# BB#1:
	movq	(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_50
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_50
# BB#3:                                 # %polybench_alloc_data.exit
	movq	(%rsp), %r15
	testq	%r15, %r15
	je	.LBB6_50
# BB#4:                                 # %polybench_alloc_data.exit52
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_50
# BB#5:                                 # %polybench_alloc_data.exit52
	movq	(%rsp), %r12
	testq	%r12, %r12
	je	.LBB6_50
# BB#6:                                 # %polybench_alloc_data.exit54
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_50
# BB#7:                                 # %polybench_alloc_data.exit54
	movq	(%rsp), %r13
	testq	%r13, %r13
	je	.LBB6_50
# BB#8:                                 # %polybench_alloc_data.exit56
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_50
# BB#9:                                 # %polybench_alloc_data.exit56
	movq	(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_50
# BB#10:                                # %polybench_alloc_data.exit58
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_50
# BB#11:                                # %polybench_alloc_data.exit58
	movq	(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB6_50
# BB#12:                                # %polybench_alloc_data.exit60
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_50
# BB#13:                                # %polybench_alloc_data.exit60
	movq	(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_50
# BB#14:                                # %polybench_alloc_data.exit62
	movq	16(%rsp), %r8           # 8-byte Reload
	leaq	8(%r8), %rax
	xorl	%ecx, %ecx
	movsd	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI6_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI6_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI6_3(%rip), %xmm3   # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB6_15:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_16 Depth 2
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%ecx, %xmm4
	movapd	%xmm4, %xmm5
	divsd	%xmm0, %xmm5
	movsd	%xmm5, (%r15,%rcx,8)
	movsd	%xmm5, (%r13,%rcx,8)
	movapd	%xmm4, %xmm5
	addsd	%xmm1, %xmm5
	divsd	%xmm0, %xmm5
	movsd	%xmm5, (%r12,%rcx,8)
	movsd	%xmm5, (%rbx,%rcx,8)
	movapd	%xmm4, %xmm5
	addsd	%xmm2, %xmm5
	divsd	%xmm0, %xmm5
	movsd	%xmm5, (%rbp,%rcx,8)
	movapd	%xmm4, %xmm5
	addsd	%xmm3, %xmm5
	divsd	%xmm0, %xmm5
	movsd	%xmm5, (%r14,%rcx,8)
	movq	%rax, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_16:                               #   Parent Loop BB6_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%esi, %xmm5
	mulsd	%xmm4, %xmm5
	divsd	%xmm0, %xmm5
	movsd	%xmm5, -8(%rdx)
	movl	%esi, %edi
	orl	$1, %edi
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%edi, %xmm5
	mulsd	%xmm4, %xmm5
	divsd	%xmm0, %xmm5
	movsd	%xmm5, (%rdx)
	addq	$2, %rsi
	addq	$16, %rdx
	cmpq	$4000, %rsi             # imm = 0xFA0
	jne	.LBB6_16
# BB#17:                                #   in Loop: Header=BB6_15 Depth=1
	incq	%rcx
	addq	$32000, %rax            # imm = 0x7D00
	cmpq	$4000, %rcx             # imm = 0xFA0
	jne	.LBB6_15
# BB#18:                                # %.preheader2.i.preheader
	leaq	8(%r8), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_19:                               # %.preheader2.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_20 Depth 2
	movsd	(%r15,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movl	$1, %edx
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_20:                               #   Parent Loop BB6_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	mulsd	-8(%rbp,%rdx,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%r15,%rcx,8)
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rbp,%rdx,8), %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%r15,%rcx,8)
	addq	$16, %rsi
	addq	$2, %rdx
	cmpq	$4001, %rdx             # imm = 0xFA1
	jne	.LBB6_20
# BB#21:                                #   in Loop: Header=BB6_19 Depth=1
	incq	%rcx
	addq	$32000, %rax            # imm = 0x7D00
	cmpq	$4000, %rcx             # imm = 0xFA0
	jne	.LBB6_19
# BB#22:                                # %.preheader.i.preheader
	xorl	%eax, %eax
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB6_23:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_24 Depth 2
	movsd	(%r12,%rax,8), %xmm0    # xmm0 = mem[0],zero
	movl	$1, %edx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB6_24:                               #   Parent Loop BB6_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	mulsd	-8(%r14,%rdx,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%r12,%rax,8)
	movsd	32000(%rsi), %xmm0      # xmm0 = mem[0],zero
	mulsd	(%r14,%rdx,8), %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%r12,%rax,8)
	addq	$64000, %rsi            # imm = 0xFA00
	addq	$2, %rdx
	cmpq	$4001, %rdx             # imm = 0xFA1
	jne	.LBB6_24
# BB#25:                                #   in Loop: Header=BB6_23 Depth=1
	incq	%rax
	addq	$8, %rcx
	cmpq	$4000, %rax             # imm = 0xFA0
	jne	.LBB6_23
# BB#26:                                # %.preheader2.i68.preheader
	leaq	8(%r8), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_27:                               # %.preheader2.i68
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_28 Depth 2
	movsd	(%r13,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movl	$1, %edx
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_28:                               #   Parent Loop BB6_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	mulsd	-8(%rbp,%rdx,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%r13,%rcx,8)
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rbp,%rdx,8), %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%r13,%rcx,8)
	addq	$16, %rsi
	addq	$2, %rdx
	cmpq	$4001, %rdx             # imm = 0xFA1
	jne	.LBB6_28
# BB#29:                                #   in Loop: Header=BB6_27 Depth=1
	incq	%rcx
	addq	$32000, %rax            # imm = 0x7D00
	cmpq	$4000, %rcx             # imm = 0xFA0
	jne	.LBB6_27
# BB#30:                                # %.preheader.i76.preheader
	xorl	%eax, %eax
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB6_31:                               # %.preheader.i76
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_32 Depth 2
	movsd	(%rbx,%rax,8), %xmm0    # xmm0 = mem[0],zero
	movl	$1, %edx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB6_32:                               #   Parent Loop BB6_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	mulsd	-8(%r14,%rdx,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rbx,%rax,8)
	movsd	32000(%rsi), %xmm0      # xmm0 = mem[0],zero
	mulsd	(%r14,%rdx,8), %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%rbx,%rax,8)
	addq	$64000, %rsi            # imm = 0xFA00
	addq	$2, %rdx
	cmpq	$4001, %rdx             # imm = 0xFA1
	jne	.LBB6_32
# BB#33:                                #   in Loop: Header=BB6_31 Depth=1
	incq	%rax
	addq	$8, %rcx
	cmpq	$4000, %rax             # imm = 0xFA0
	jne	.LBB6_31
# BB#34:                                # %kernel_mvt_StrictFP.exit.preheader
	movl	$1, %edx
	movapd	.LCPI6_4(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_5(%rip), %xmm3   # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB6_35:                               # %kernel_mvt_StrictFP.exit
                                        # =>This Inner Loop Header: Depth=1
	movsd	-8(%r15,%rdx,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%r13,%rdx,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_36
# BB#38:                                # %kernel_mvt_StrictFP.exit.1141
                                        #   in Loop: Header=BB6_35 Depth=1
	movsd	(%r15,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r13,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_37
# BB#39:                                #   in Loop: Header=BB6_35 Depth=1
	leaq	2(%rdx), %rax
	incq	%rdx
	cmpq	$4000, %rdx             # imm = 0xFA0
	movq	%rax, %rdx
	jl	.LBB6_35
# BB#40:                                # %check_FP.exit.preheader
	movl	$1, %edx
	.p2align	4, 0x90
.LBB6_41:                               # %check_FP.exit
                                        # =>This Inner Loop Header: Depth=1
	movsd	-8(%r12,%rdx,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%rbx,%rdx,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_36
# BB#42:                                # %check_FP.exit.1140
                                        #   in Loop: Header=BB6_41 Depth=1
	movsd	(%r12,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rbx,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_37
# BB#43:                                #   in Loop: Header=BB6_41 Depth=1
	leaq	2(%rdx), %rax
	incq	%rdx
	cmpq	$4000, %rdx             # imm = 0xFA0
	movq	%rax, %rdx
	jl	.LBB6_41
# BB#44:                                # %check_FP.exit88
	movl	$64001, %edi            # imm = 0xFA01
	callq	malloc
	movb	$0, 64000(%rax)
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_45:                               # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rsi), %rcx
	movl	%ecx, %edx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, (%rax,%rsi,2)
	movb	%dl, 1(%rax,%rsi,2)
	movq	%rcx, %rdx
	shrq	$8, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 2(%rax,%rsi,2)
	movb	%dl, 3(%rax,%rsi,2)
	movq	%rcx, %rdx
	shrq	$16, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 4(%rax,%rsi,2)
	movb	%dl, 5(%rax,%rsi,2)
	movl	%ecx, %edx
	shrl	$24, %edx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 6(%rax,%rsi,2)
	movb	%dl, 7(%rax,%rsi,2)
	movq	%rcx, %rdx
	shrq	$32, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 8(%rax,%rsi,2)
	movb	%dl, 9(%rax,%rsi,2)
	movq	%rcx, %rdx
	shrq	$40, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 10(%rax,%rsi,2)
	movb	%dl, 11(%rax,%rsi,2)
	movq	%rcx, %rdx
	shrq	$48, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 12(%rax,%rsi,2)
	movb	%dl, 13(%rax,%rsi,2)
	shrq	$56, %rcx
	andb	$15, %cl
	orb	$48, %cl
	movb	%cl, 14(%rax,%rsi,2)
	movb	%cl, 15(%rax,%rsi,2)
	addq	$8, %rsi
	cmpq	$32000, %rsi            # imm = 0x7D00
	jne	.LBB6_45
# BB#46:
	movq	stderr(%rip), %rsi
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	fputs
	movq	8(%rsp), %rdi           # 8-byte Reload
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_47:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rax), %rcx
	movl	%ecx, %edx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, (%rdi,%rax,2)
	movb	%dl, 1(%rdi,%rax,2)
	movq	%rcx, %rdx
	shrq	$8, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 2(%rdi,%rax,2)
	movb	%dl, 3(%rdi,%rax,2)
	movq	%rcx, %rdx
	shrq	$16, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 4(%rdi,%rax,2)
	movb	%dl, 5(%rdi,%rax,2)
	movl	%ecx, %edx
	shrl	$24, %edx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 6(%rdi,%rax,2)
	movb	%dl, 7(%rdi,%rax,2)
	movq	%rcx, %rdx
	shrq	$32, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 8(%rdi,%rax,2)
	movb	%dl, 9(%rdi,%rax,2)
	movq	%rcx, %rdx
	shrq	$40, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 10(%rdi,%rax,2)
	movb	%dl, 11(%rdi,%rax,2)
	movq	%rcx, %rdx
	shrq	$48, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 12(%rdi,%rax,2)
	movb	%dl, 13(%rdi,%rax,2)
	shrq	$56, %rcx
	andb	$15, %cl
	orb	$48, %cl
	movb	%cl, 14(%rdi,%rax,2)
	movb	%cl, 15(%rdi,%rax,2)
	addq	$8, %rax
	cmpq	$32000, %rax            # imm = 0x7D00
	jne	.LBB6_47
# BB#48:                                # %print_array.exit
	movq	stderr(%rip), %rsi
	callq	fputs
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	free
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	%rbp, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_49
.LBB6_36:                               # %check_FP.exit.threadsplit
	decq	%rdx
.LBB6_37:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_5(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %ecx
	callq	fprintf
	movl	$1, %eax
.LBB6_49:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_50:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d] = %lf and B[%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 68


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
