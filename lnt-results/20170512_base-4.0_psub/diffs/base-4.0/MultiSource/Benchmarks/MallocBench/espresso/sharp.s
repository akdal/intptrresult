	.text
	.file	"sharp.bc"
	.globl	cv_sharp
	.p2align	4, 0x90
	.type	cv_sharp,@function
cv_sharp:                               # @cv_sharp
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	cube(%rip), %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r12
	movslq	(%r14), %rcx
	movslq	12(%r14), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB0_6
# BB#1:                                 # %.lr.ph
	movq	24(%r14), %rbx
	leaq	(%rbx,%rax,4), %r13
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	cmpl	$0, 12(%r15)
	je	.LBB0_3
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, start_time(%rip)
	movl	12(%r15), %ecx
	decl	%ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	cb_recur_sharp
	jmp	.LBB0_5
	.p2align	4, 0x90
.LBB0_3:                                #   in Loop: Header=BB0_2 Depth=1
	movl	cube(%rip), %esi
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbx, %rsi
	callq	sf_addset
.LBB0_5:                                # %cb_sharp.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sf_union
	movq	%rax, %r12
	movslq	(%r14), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r13, %rbx
	jb	.LBB0_2
.LBB0_6:                                # %._crit_edge
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	cv_sharp, .Lfunc_end0-cv_sharp
	.cfi_endproc

	.globl	cb_sharp
	.p2align	4, 0x90
	.type	cb_sharp,@function
cb_sharp:                               # @cb_sharp
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	cmpl	$0, 12(%rbx)
	je	.LBB1_1
# BB#2:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, start_time(%rip)
	movl	12(%rbx), %ecx
	decl	%ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rbx, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	cb_recur_sharp          # TAILCALL
.LBB1_1:
	movl	cube(%rip), %esi
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	sf_addset               # TAILCALL
.Lfunc_end1:
	.size	cb_sharp, .Lfunc_end1-cb_sharp
	.cfi_endproc

	.globl	cb_recur_sharp
	.p2align	4, 0x90
	.type	cb_recur_sharp,@function
cb_recur_sharp:                         # @cb_recur_sharp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 80
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movl	%r8d, %r13d
	movl	%ecx, %r12d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rsi, %rbx
	movq	%rdi, %r15
	cmpl	%r12d, %edx
	jne	.LBB2_1
# BB#5:
	movq	24(%rbx), %rax
	movslq	(%rbx), %rcx
	movslq	%edx, %rdx
	imulq	%rcx, %rdx
	leaq	(%rax,%rdx,4), %rsi
	movq	%r15, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sharp                   # TAILCALL
.LBB2_1:
	leal	(%r12,%rdx), %eax
	movl	%eax, %ebp
	shrl	$31, %ebp
	addl	%eax, %ebp
	sarl	%ebp
	leal	1(%r13), %r14d
	movq	%r15, %rdi
	movq	%rbx, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%ebp, %ecx
	movl	%r14d, %r8d
	callq	cb_recur_sharp
	incl	%ebp
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rbx
	movl	%ebp, %edx
	movl	%r12d, %ecx
	movl	%r14d, %r8d
	callq	cb_recur_sharp
	movq	%rax, %r15
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	cv_intersect
	movq	%rax, %r12
	cmpl	$3, %r13d
	jg	.LBB2_4
# BB#2:
	movl	$8192, %eax             # imm = 0x2000
	andl	debug(%rip), %eax
	je	.LBB2_4
# BB#3:
	movl	12(%r12), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	12(%rbx), %r14d
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movl	12(%r15), %ebx
	xorl	%eax, %eax
	callq	util_cpu_time
	subq	start_time(%rip), %rax
	movq	%rax, %rdi
	callq	util_print_time
	movq	%rax, %rbp
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	%r14d, %ecx
	movl	%ebx, %r8d
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbp, %r9
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB2_4:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sf_free
	movq	%r12, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	cb_recur_sharp, .Lfunc_end2-cb_recur_sharp
	.cfi_endproc

	.globl	sharp
	.p2align	4, 0x90
	.type	sharp,@function
sharp:                                  # @sharp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 64
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r14
	movq	cube+80(%rip), %rax
	movq	(%rax), %r15
	movq	8(%rax), %r13
	movq	16(%rax), %r12
	movl	cube+4(%rip), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	cdist0
	testl	%eax, %eax
	je	.LBB3_7
# BB#1:
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbp, %rdx
	callq	set_diff
	cmpl	$0, cube+4(%rip)
	jle	.LBB3_6
# BB#2:                                 # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movq	cube+72(%rip), %rax
	movq	(%rax,%rbp,8), %rdx
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	set_and
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	setp_empty
	testl	%eax, %eax
	jne	.LBB3_5
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	cube+72(%rip), %rax
	movq	(%rax,%rbp,8), %rdx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	set_diff
	movq	24(%rbx), %rax
	movl	12(%rbx), %ecx
	leal	1(%rcx), %edx
	imull	(%rbx), %ecx
	movl	%edx, 12(%rbx)
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %rdi
	xorl	%eax, %eax
	movq	%r13, %rsi
	movq	%r12, %rdx
	callq	set_or
.LBB3_5:                                #   in Loop: Header=BB3_3 Depth=1
	incq	%rbp
	movslq	cube+4(%rip), %rax
	cmpq	%rax, %rbp
	jl	.LBB3_3
.LBB3_6:                                # %.loopexit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_7:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sf_addset               # TAILCALL
.Lfunc_end3:
	.size	sharp, .Lfunc_end3-sharp
	.cfi_endproc

	.globl	make_disjoint
	.p2align	4, 0x90
	.type	make_disjoint,@function
make_disjoint:                          # @make_disjoint
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 48
.Lcfi46:
	.cfi_offset %rbx, -40
.Lcfi47:
	.cfi_offset %r12, -32
.Lcfi48:
	.cfi_offset %r14, -24
.Lcfi49:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	cube(%rip), %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r15
	movslq	(%r14), %rcx
	movslq	12(%r14), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB4_3
# BB#1:                                 # %.lr.ph.preheader
	movq	24(%r14), %rbx
	leaq	(%rbx,%rax,4), %r12
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	cb_dsharp
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rcx, %rsi
	callq	sf_append
	movq	%rax, %r15
	movslq	(%r14), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r12, %rbx
	jb	.LBB4_2
.LBB4_3:                                # %._crit_edge
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	make_disjoint, .Lfunc_end4-make_disjoint
	.cfi_endproc

	.globl	cv_dsharp
	.p2align	4, 0x90
	.type	cv_dsharp,@function
cv_dsharp:                              # @cv_dsharp
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 48
.Lcfi55:
	.cfi_offset %rbx, -48
.Lcfi56:
	.cfi_offset %r12, -40
.Lcfi57:
	.cfi_offset %r13, -32
.Lcfi58:
	.cfi_offset %r14, -24
.Lcfi59:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	cube(%rip), %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r12
	movslq	(%r15), %rcx
	movslq	12(%r15), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB5_3
# BB#1:                                 # %.lr.ph.preheader
	movq	24(%r15), %rbx
	leaq	(%rbx,%rax,4), %r13
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	cb_dsharp
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rcx, %rsi
	callq	sf_union
	movq	%rax, %r12
	movslq	(%r15), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r13, %rbx
	jb	.LBB5_2
.LBB5_3:                                # %._crit_edge
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	cv_dsharp, .Lfunc_end5-cv_dsharp
	.cfi_endproc

	.globl	cb1_dsharp
	.p2align	4, 0x90
	.type	cb1_dsharp,@function
cb1_dsharp:                             # @cb1_dsharp
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 48
.Lcfi65:
	.cfi_offset %rbx, -48
.Lcfi66:
	.cfi_offset %r12, -40
.Lcfi67:
	.cfi_offset %r13, -32
.Lcfi68:
	.cfi_offset %r14, -24
.Lcfi69:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	12(%r15), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r12
	movslq	(%r15), %rcx
	movslq	12(%r15), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB6_3
# BB#1:                                 # %.lr.ph.preheader
	movq	24(%r15), %rbx
	leaq	(%rbx,%rax,4), %r13
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	dsharp
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rcx, %rsi
	callq	sf_union
	movq	%rax, %r12
	movslq	(%r15), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r13, %rbx
	jb	.LBB6_2
.LBB6_3:                                # %._crit_edge
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	cb1_dsharp, .Lfunc_end6-cb1_dsharp
	.cfi_endproc

	.globl	cb_dsharp
	.p2align	4, 0x90
	.type	cb_dsharp,@function
cb_dsharp:                              # @cb_dsharp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi76:
	.cfi_def_cfa_offset 64
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movl	12(%rbx), %edi
	movl	cube(%rip), %esi
	testl	%edi, %edi
	je	.LBB7_9
# BB#1:
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r15
	movq	24(%r15), %rax
	movl	12(%r15), %ecx
	leal	1(%rcx), %edx
	imull	(%r15), %ecx
	movl	%edx, 12(%r15)
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %rdi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	set_copy
	movslq	(%rbx), %rcx
	movslq	12(%rbx), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB7_2
# BB#3:                                 # %.lr.ph.preheader
	movq	24(%rbx), %r12
	leaq	(%r12,%rax,4), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB7_4:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_6 Depth 2
	movl	12(%r15), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r13
	movslq	(%r15), %rcx
	movslq	12(%r15), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB7_7
# BB#5:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	24(%r15), %rbx
	leaq	(%rbx,%rax,4), %r14
	.p2align	4, 0x90
.LBB7_6:                                # %.lr.ph.i
                                        #   Parent Loop BB7_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	dsharp
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rcx, %rsi
	callq	sf_union
	movq	%rax, %r13
	movslq	(%r15), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r14, %rbx
	jb	.LBB7_6
.LBB7_7:                                # %cb1_dsharp.exit
                                        #   in Loop: Header=BB7_4 Depth=1
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sf_free
	movq	%rbp, %rax
	movslq	(%rax), %rax
	leaq	(%r12,%rax,4), %r12
	cmpq	(%rsp), %r12            # 8-byte Folded Reload
	movq	%r13, %r15
	jb	.LBB7_4
	jmp	.LBB7_8
.LBB7_9:
	movl	$1, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbp, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sf_addset               # TAILCALL
.LBB7_2:
	movq	%r15, %r13
.LBB7_8:                                # %.loopexit
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	cb_dsharp, .Lfunc_end7-cb_dsharp
	.cfi_endproc

	.globl	dsharp
	.p2align	4, 0x90
	.type	dsharp,@function
dsharp:                                 # @dsharp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi87:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi89:
	.cfi_def_cfa_offset 192
.Lcfi90:
	.cfi_offset %rbx, -56
.Lcfi91:
	.cfi_offset %r12, -48
.Lcfi92:
	.cfi_offset %r13, -40
.Lcfi93:
	.cfi_offset %r14, -32
.Lcfi94:
	.cfi_offset %r15, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	cube+80(%rip), %rax
	movq	(%rax), %r14
	movl	cube+4(%rip), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r15
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	cdist0
	testl	%eax, %eax
	je	.LBB8_3
# BB#1:
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jge	.LBB8_4
# BB#2:
	movl	$8, %edi
	jmp	.LBB8_5
.LBB8_3:
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r12, %rsi
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sf_addset               # TAILCALL
.LBB8_4:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB8_5:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	set_diff
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jge	.LBB8_7
# BB#6:
	movl	$8, %edi
	jmp	.LBB8_8
.LBB8_7:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB8_8:
	movq	%r15, 80(%rsp)          # 8-byte Spill
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	set_and
	movq	%rax, %r13
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	jge	.LBB8_10
# BB#9:
	movl	$8, %edi
	jmp	.LBB8_11
.LBB8_10:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB8_11:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, %rbx
	cmpl	$0, cube+4(%rip)
	movq	8(%rsp), %r9            # 8-byte Reload
	jle	.LBB8_74
# BB#12:                                # %.lr.ph
	leaq	-4(%r14), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	4(%r14), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	-4(%r12), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	4(%r12), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	leaq	-4(%rbx), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	4(%rbx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	-4(%r13), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leaq	4(%r13), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	-12(%r13), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	-12(%rbx), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	-12(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	-12(%r12), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	jmp	.LBB8_13
.LBB8_63:                               # %vector.body.preheader
                                        #   in Loop: Header=BB8_13 Depth=1
	leaq	-8(%r8), %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB8_65
# BB#64:                                # %vector.body.prol
                                        #   in Loop: Header=BB8_13 Depth=1
	movups	-12(%rbp,%rax,4), %xmm0
	movups	-28(%rbp,%rax,4), %xmm1
	movups	-12(%r14,%rax,4), %xmm2
	movups	-28(%r14,%rax,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbp,%rax,4)
	movups	%xmm3, -28(%rbp,%rax,4)
	movl	$8, %esi
	jmp	.LBB8_66
.LBB8_65:                               #   in Loop: Header=BB8_13 Depth=1
	xorl	%esi, %esi
.LBB8_66:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB8_69
# BB#67:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	%r9, %rdi
	andq	$-8, %rdi
	negq	%rdi
	negq	%rsi
	leaq	(%rdx,%r10,4), %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rcx
	.p2align	4, 0x90
.LBB8_68:                               # %vector.body
                                        #   Parent Loop BB8_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-12(%rdx,%rsi,4), %xmm0
	movups	-28(%rdx,%rsi,4), %xmm1
	movups	(%rcx,%rsi,4), %xmm2
	movups	-16(%rcx,%rsi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rdx,%rsi,4)
	movups	%xmm3, -28(%rdx,%rsi,4)
	movups	-44(%rdx,%rsi,4), %xmm0
	movups	-60(%rdx,%rsi,4), %xmm1
	movups	-32(%rcx,%rsi,4), %xmm2
	movups	-48(%rcx,%rsi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -44(%rdx,%rsi,4)
	movups	%xmm3, -60(%rdx,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB8_68
.LBB8_69:                               # %middle.block
                                        #   in Loop: Header=BB8_13 Depth=1
	cmpq	%r8, %r9
	movq	8(%rsp), %r9            # 8-byte Reload
	je	.LBB8_73
# BB#70:                                #   in Loop: Header=BB8_13 Depth=1
	subq	%r8, %rax
	jmp	.LBB8_71
	.p2align	4, 0x90
.LBB8_13:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_22 Depth 2
                                        #     Child Loop BB8_26 Depth 2
                                        #     Child Loop BB8_35 Depth 2
                                        #     Child Loop BB8_39 Depth 2
                                        #     Child Loop BB8_51 Depth 2
                                        #     Child Loop BB8_55 Depth 2
                                        #     Child Loop BB8_68 Depth 2
                                        #     Child Loop BB8_72 Depth 2
	movq	cube+72(%rip), %rax
	movq	(%rax,%r15,8), %rsi
	xorl	%eax, %eax
	movq	%r9, %rdi
	callq	setp_disjoint
	movq	8(%rsp), %r9            # 8-byte Reload
	testl	%eax, %eax
	jne	.LBB8_73
# BB#14:                                #   in Loop: Header=BB8_13 Depth=1
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	24(%rdx), %rsi
	movl	12(%rdx), %eax
	leal	1(%rax), %ecx
	imull	(%rdx), %eax
	movl	%ecx, 12(%rdx)
	cltq
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	(%rsi,%rax,4), %rbp
	movq	cube+72(%rip), %rax
	movq	(%rax,%r15,8), %rdx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r9, %rsi
	callq	set_and
	movl	(%r13), %eax
	movl	(%r14), %ecx
	movl	$-1024, %edx            # imm = 0xFC00
	andl	%edx, %ecx
	andl	$1023, %eax             # imm = 0x3FF
	movq	%rax, %r9
	movl	$1, %edx
	cmoveq	%rdx, %r9
	movl	%eax, %edx
	orl	%ecx, %edx
	movl	%edx, (%r14)
	cmpq	$8, %r9
	jb	.LBB8_25
# BB#15:                                # %min.iters.checked243
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	%r9, %rcx
	andq	$1016, %rcx             # imm = 0x3F8
	je	.LBB8_25
# BB#16:                                # %vector.memcheck275
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	1(%rax), %rcx
	testl	%eax, %eax
	movl	$2, %edx
	cmovneq	%rdx, %rcx
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,4), %rdx
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rax,4), %rsi
	movq	112(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rcx,4), %r10
	movq	104(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rax,4), %r8
	movq	72(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rcx,4), %r11
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rcx
	cmpq	%r8, %rdx
	sbbb	%dil, %dil
	cmpq	%rsi, %r10
	sbbb	%r10b, %r10b
	andb	%dil, %r10b
	cmpq	%rcx, %rdx
	sbbb	%cl, %cl
	cmpq	%rsi, %r11
	sbbb	%dl, %dl
	testb	$1, %r10b
	jne	.LBB8_25
# BB#17:                                # %vector.memcheck275
                                        #   in Loop: Header=BB8_13 Depth=1
	andb	%dl, %cl
	andb	$1, %cl
	jne	.LBB8_25
# BB#18:                                # %vector.body238.preheader
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	48(%rsp), %r8           # 8-byte Reload
	leaq	-8(%r8), %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB8_20
# BB#19:                                # %vector.body238.prol
                                        #   in Loop: Header=BB8_13 Depth=1
	movups	-12(%r13,%rax,4), %xmm0
	movups	-28(%r13,%rax,4), %xmm1
	movups	-12(%rbx,%rax,4), %xmm2
	movups	-28(%rbx,%rax,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%r14,%rax,4)
	movups	%xmm3, -28(%r14,%rax,4)
	movl	$8, %esi
	testq	%rcx, %rcx
	jne	.LBB8_21
	jmp	.LBB8_23
.LBB8_20:                               #   in Loop: Header=BB8_13 Depth=1
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.LBB8_23
.LBB8_21:                               # %vector.body238.preheader.new
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	%r9, %r10
	andq	$-8, %r10
	negq	%r10
	negq	%rsi
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rdx
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rcx
	movq	24(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rax,4), %rdi
	.p2align	4, 0x90
.LBB8_22:                               # %vector.body238
                                        #   Parent Loop BB8_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdx,%rsi,4), %xmm0
	movups	-16(%rdx,%rsi,4), %xmm1
	movups	(%rcx,%rsi,4), %xmm2
	movups	-16(%rcx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, (%rdi,%rsi,4)
	movups	%xmm3, -16(%rdi,%rsi,4)
	movups	-32(%rdx,%rsi,4), %xmm0
	movups	-48(%rdx,%rsi,4), %xmm1
	movups	-32(%rcx,%rsi,4), %xmm2
	movups	-48(%rcx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -32(%rdi,%rsi,4)
	movups	%xmm3, -48(%rdi,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %r10
	jne	.LBB8_22
.LBB8_23:                               # %middle.block239
                                        #   in Loop: Header=BB8_13 Depth=1
	cmpq	%r8, %r9
	je	.LBB8_27
# BB#24:                                #   in Loop: Header=BB8_13 Depth=1
	subq	%r8, %rax
	.p2align	4, 0x90
.LBB8_25:                               # %scalar.ph240.preheader
                                        #   in Loop: Header=BB8_13 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB8_26:                               # %scalar.ph240
                                        #   Parent Loop BB8_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rbx,%rax,4), %ecx
	andl	-4(%r13,%rax,4), %ecx
	movl	%ecx, -4(%r14,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB8_26
.LBB8_27:                               # %.loopexit299
                                        #   in Loop: Header=BB8_13 Depth=1
	movl	(%rbp), %ecx
	movl	%ecx, %eax
	andl	$1023, %eax             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movl	$1, %r10d
	cmovneq	%rax, %r10
	cmpq	$8, %r10
	jb	.LBB8_38
# BB#28:                                # %min.iters.checked194
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	%r10, %r9
	andq	$1016, %r9              # imm = 0x3F8
	je	.LBB8_38
# BB#29:                                # %vector.memcheck216
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	%rax, %rcx
	notq	%rcx
	testl	%eax, %eax
	movq	$-2, %rdx
	cmovneq	%rdx, %rcx
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	-4(%rdi,%rdx,4), %rdx
	leaq	(%rsi,%rax), %r8
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rax,4), %rsi
	cmpq	%rsi, %rdx
	jae	.LBB8_31
# BB#30:                                # %vector.memcheck216
                                        #   in Loop: Header=BB8_13 Depth=1
	leaq	4(%rdi,%r8,4), %rdx
	shlq	$2, %rcx
	movq	32(%rsp), %rsi          # 8-byte Reload
	subq	%rcx, %rsi
	cmpq	%rdx, %rsi
	jb	.LBB8_38
.LBB8_31:                               # %vector.body189.preheader
                                        #   in Loop: Header=BB8_13 Depth=1
	leaq	-8(%r9), %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB8_33
# BB#32:                                # %vector.body189.prol
                                        #   in Loop: Header=BB8_13 Depth=1
	movups	-12(%rbp,%rax,4), %xmm0
	movups	-28(%rbp,%rax,4), %xmm1
	movups	-12(%r14,%rax,4), %xmm2
	movups	-28(%r14,%rax,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbp,%rax,4)
	movups	%xmm3, -28(%rbp,%rax,4)
	movl	$8, %edi
	testq	%rcx, %rcx
	jne	.LBB8_34
	jmp	.LBB8_36
.LBB8_33:                               #   in Loop: Header=BB8_13 Depth=1
	xorl	%edi, %edi
	testq	%rcx, %rcx
	je	.LBB8_36
.LBB8_34:                               # %vector.body189.preheader.new
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	%r10, %rcx
	andq	$-8, %rcx
	negq	%rcx
	negq	%rdi
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r8,4), %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,4), %rdx
	.p2align	4, 0x90
.LBB8_35:                               # %vector.body189
                                        #   Parent Loop BB8_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-12(%rsi,%rdi,4), %xmm0
	movups	-28(%rsi,%rdi,4), %xmm1
	movups	(%rdx,%rdi,4), %xmm2
	movups	-16(%rdx,%rdi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rsi,%rdi,4)
	movups	%xmm3, -28(%rsi,%rdi,4)
	movups	-44(%rsi,%rdi,4), %xmm0
	movups	-60(%rsi,%rdi,4), %xmm1
	movups	-32(%rdx,%rdi,4), %xmm2
	movups	-48(%rdx,%rdi,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -44(%rsi,%rdi,4)
	movups	%xmm3, -60(%rsi,%rdi,4)
	addq	$-16, %rdi
	cmpq	%rdi, %rcx
	jne	.LBB8_35
.LBB8_36:                               # %middle.block190
                                        #   in Loop: Header=BB8_13 Depth=1
	cmpq	%r9, %r10
	je	.LBB8_40
# BB#37:                                #   in Loop: Header=BB8_13 Depth=1
	subq	%r9, %rax
	.p2align	4, 0x90
.LBB8_38:                               # %scalar.ph191.preheader
                                        #   in Loop: Header=BB8_13 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB8_39:                               # %scalar.ph191
                                        #   Parent Loop BB8_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%r14,%rax,4), %ecx
	orl	%ecx, -4(%rbp,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB8_39
.LBB8_40:                               # %.loopexit298
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	cube+72(%rip), %rax
	movq	(%rax,%r15,8), %rdx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	callq	set_or
	movl	(%r12), %eax
	movl	(%r14), %ecx
	movl	$-1024, %edx            # imm = 0xFC00
	andl	%edx, %ecx
	andl	$1023, %eax             # imm = 0x3FF
	movl	$1, %r9d
	cmovneq	%rax, %r9
	movl	%eax, %edx
	orl	%ecx, %edx
	movl	%edx, (%r14)
	cmpq	$8, %r9
	jae	.LBB8_42
# BB#41:                                #   in Loop: Header=BB8_13 Depth=1
	movq	(%rsp), %r10            # 8-byte Reload
	jmp	.LBB8_54
.LBB8_42:                               # %min.iters.checked136
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	%r9, %rcx
	andq	$1016, %rcx             # imm = 0x3F8
	je	.LBB8_48
# BB#43:                                # %vector.memcheck167
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	1(%rax), %rcx
	testl	%eax, %eax
	movl	$2, %edx
	cmovneq	%rdx, %rcx
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,4), %rdx
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rax,4), %rsi
	movq	128(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rcx,4), %r10
	movq	120(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rax,4), %r8
	movq	72(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rcx,4), %r11
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rcx
	cmpq	%r8, %rdx
	sbbb	%dil, %dil
	cmpq	%rsi, %r10
	sbbb	%r10b, %r10b
	andb	%dil, %r10b
	cmpq	%rcx, %rdx
	sbbb	%cl, %cl
	cmpq	%rsi, %r11
	sbbb	%dl, %dl
	testb	$1, %r10b
	jne	.LBB8_48
# BB#44:                                # %vector.memcheck167
                                        #   in Loop: Header=BB8_13 Depth=1
	andb	%dl, %cl
	andb	$1, %cl
	movq	(%rsp), %r10            # 8-byte Reload
	jne	.LBB8_54
# BB#45:                                # %vector.body131.preheader
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	48(%rsp), %r8           # 8-byte Reload
	leaq	-8(%r8), %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB8_49
# BB#46:                                # %vector.body131.prol
                                        #   in Loop: Header=BB8_13 Depth=1
	movups	-12(%r12,%rax,4), %xmm0
	movups	-28(%r12,%rax,4), %xmm1
	movups	-12(%rbx,%rax,4), %xmm2
	movups	-28(%rbx,%rax,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -12(%r14,%rax,4)
	movups	%xmm3, -28(%r14,%rax,4)
	movl	$8, %esi
	testq	%rcx, %rcx
	jne	.LBB8_50
	jmp	.LBB8_52
.LBB8_48:                               #   in Loop: Header=BB8_13 Depth=1
	movq	(%rsp), %r10            # 8-byte Reload
	jmp	.LBB8_54
.LBB8_49:                               #   in Loop: Header=BB8_13 Depth=1
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.LBB8_52
.LBB8_50:                               # %vector.body131.preheader.new
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	%r9, %r10
	andq	$-8, %r10
	negq	%r10
	negq	%rsi
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rdx
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rcx
	movq	24(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rax,4), %rdi
	.p2align	4, 0x90
.LBB8_51:                               # %vector.body131
                                        #   Parent Loop BB8_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdx,%rsi,4), %xmm0
	movups	-16(%rdx,%rsi,4), %xmm1
	movups	(%rcx,%rsi,4), %xmm2
	movups	-16(%rcx,%rsi,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, (%rdi,%rsi,4)
	movups	%xmm3, -16(%rdi,%rsi,4)
	movups	-32(%rdx,%rsi,4), %xmm0
	movups	-48(%rdx,%rsi,4), %xmm1
	movups	-32(%rcx,%rsi,4), %xmm2
	movups	-48(%rcx,%rsi,4), %xmm3
	andnps	%xmm0, %xmm2
	andnps	%xmm1, %xmm3
	movups	%xmm2, -32(%rdi,%rsi,4)
	movups	%xmm3, -48(%rdi,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %r10
	jne	.LBB8_51
.LBB8_52:                               # %middle.block132
                                        #   in Loop: Header=BB8_13 Depth=1
	cmpq	%r8, %r9
	movq	(%rsp), %r10            # 8-byte Reload
	je	.LBB8_56
# BB#53:                                #   in Loop: Header=BB8_13 Depth=1
	subq	%r8, %rax
	.p2align	4, 0x90
.LBB8_54:                               # %scalar.ph133.preheader
                                        #   in Loop: Header=BB8_13 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB8_55:                               # %scalar.ph133
                                        #   Parent Loop BB8_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rbx,%rax,4), %ecx
	notl	%ecx
	andl	-4(%r12,%rax,4), %ecx
	movl	%ecx, -4(%r14,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB8_55
.LBB8_56:                               # %.loopexit297
                                        #   in Loop: Header=BB8_13 Depth=1
	movl	(%rbp), %ecx
	movl	%ecx, %eax
	andl	$1023, %eax             # imm = 0x3FF
	testw	$1023, %cx              # imm = 0x3FF
	movl	$1, %r9d
	cmovneq	%rax, %r9
	cmpq	$8, %r9
	jae	.LBB8_58
# BB#57:                                #   in Loop: Header=BB8_13 Depth=1
	movq	8(%rsp), %r9            # 8-byte Reload
	jmp	.LBB8_71
.LBB8_58:                               # %min.iters.checked
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	%r9, %r8
	andq	$1016, %r8              # imm = 0x3F8
	je	.LBB8_62
# BB#59:                                # %vector.memcheck
                                        #   in Loop: Header=BB8_13 Depth=1
	movq	%rax, %rcx
	notq	%rcx
	testl	%eax, %eax
	movq	$-2, %rdx
	cmovneq	%rdx, %rcx
	movq	%r10, %rdx
	subq	%rcx, %rdx
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	-4(%rdi,%rdx,4), %rdx
	addq	%rax, %r10
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rax,4), %rsi
	cmpq	%rsi, %rdx
	jae	.LBB8_63
# BB#60:                                # %vector.memcheck
                                        #   in Loop: Header=BB8_13 Depth=1
	leaq	4(%rdi,%r10,4), %rdx
	shlq	$2, %rcx
	movq	32(%rsp), %rsi          # 8-byte Reload
	subq	%rcx, %rsi
	cmpq	%rdx, %rsi
	jae	.LBB8_63
.LBB8_62:                               #   in Loop: Header=BB8_13 Depth=1
	movq	8(%rsp), %r9            # 8-byte Reload
.LBB8_71:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB8_13 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB8_72:                               # %scalar.ph
                                        #   Parent Loop BB8_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%r14,%rax,4), %ecx
	orl	%ecx, -4(%rbp,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB8_72
.LBB8_73:                               # %.loopexit
                                        #   in Loop: Header=BB8_13 Depth=1
	incq	%r15
	movslq	cube+4(%rip), %rax
	cmpq	%rax, %r15
	jl	.LBB8_13
.LBB8_74:                               # %._crit_edge
	testq	%r9, %r9
	je	.LBB8_76
# BB#75:
	movq	%r9, %rdi
	callq	free
.LBB8_76:
	testq	%r13, %r13
	movq	80(%rsp), %rbp          # 8-byte Reload
	je	.LBB8_78
# BB#77:
	movq	%r13, %rdi
	callq	free
.LBB8_78:
	testq	%rbx, %rbx
	je	.LBB8_80
# BB#79:
	movq	%rbx, %rdi
	callq	free
.LBB8_80:
	movq	%rbp, %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	dsharp, .Lfunc_end8-dsharp
	.cfi_endproc

	.globl	cv_intersect
	.p2align	4, 0x90
	.type	cv_intersect,@function
cv_intersect:                           # @cv_intersect
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi100:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi102:
	.cfi_def_cfa_offset 96
.Lcfi103:
	.cfi_offset %rbx, -56
.Lcfi104:
	.cfi_offset %r12, -48
.Lcfi105:
	.cfi_offset %r13, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	movl	cube(%rip), %esi
	movl	$500, %edi              # imm = 0x1F4
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rbx
	movl	(%r14), %eax
	movl	12(%r14), %ecx
	imull	%eax, %ecx
	testl	%ecx, %ecx
	jle	.LBB9_15
# BB#1:                                 # %.lr.ph68
	movq	24(%r14), %r12
	movslq	%ecx, %rcx
	leaq	(%r12,%rcx,4), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	24(%rbx), %r15
	movl	(%r13), %ecx
	xorl	%edx, %edx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB9_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_4 Depth 2
	movslq	12(%r13), %rsi
	movslq	%ecx, %rdx
	imulq	%rsi, %rdx
	testl	%edx, %edx
	jle	.LBB9_12
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	24(%r13), %r14
	leaq	(%r14,%rdx,4), %rbp
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB9_4:                                # %.lr.ph
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	cdist0
	testl	%eax, %eax
	je	.LBB9_10
# BB#5:                                 #   in Loop: Header=BB9_4 Depth=2
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	set_and
	movl	12(%rbx), %eax
	incl	%eax
	movl	%eax, 12(%rbx)
	cmpl	8(%rbx), %eax
	jge	.LBB9_6
# BB#9:                                 #   in Loop: Header=BB9_4 Depth=2
	movslq	(%rbx), %rax
	leaq	(%r15,%rax,4), %r15
	jmp	.LBB9_10
	.p2align	4, 0x90
.LBB9_6:                                #   in Loop: Header=BB9_4 Depth=2
	movq	%r13, %rbp
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_contain
	movq	%rax, %r13
	movq	8(%rsp), %rdi           # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB9_8
# BB#7:                                 #   in Loop: Header=BB9_4 Depth=2
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	sf_union
	movq	%rax, %r13
.LBB9_8:                                #   in Loop: Header=BB9_4 Depth=2
	movl	cube(%rip), %esi
	movl	$500, %edi              # imm = 0x1F4
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %rbx
	movq	24(%rbx), %r15
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %r13
	movq	32(%rsp), %rbp          # 8-byte Reload
.LBB9_10:                               #   in Loop: Header=BB9_4 Depth=2
	movslq	(%r13), %rcx
	leaq	(%r14,%rcx,4), %r14
	cmpq	%rbp, %r14
	jb	.LBB9_4
# BB#11:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	16(%rsp), %r14          # 8-byte Reload
	movl	(%r14), %eax
.LBB9_12:                               # %._crit_edge
                                        #   in Loop: Header=BB9_2 Depth=1
	movslq	%eax, %rdx
	leaq	(%r12,%rdx,4), %r12
	cmpq	24(%rsp), %r12          # 8-byte Folded Reload
	jb	.LBB9_2
# BB#13:                                # %._crit_edge69
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_contain
	movq	%rax, %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB9_14
# BB#16:
	xorl	%eax, %eax
	movq	%rcx, %rsi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sf_union                # TAILCALL
.LBB9_15:                               # %._crit_edge69.thread
	xorl	%eax, %eax
	movq	%rbx, %rdi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sf_contain              # TAILCALL
.LBB9_14:
	movq	%rcx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	cv_intersect, .Lfunc_end9-cv_intersect
	.cfi_endproc

	.type	start_time,@object      # @start_time
	.comm	start_time,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"# SHARP[%d]: %4d = %4d x %4d, time = %s\n"
	.size	.L.str, 41


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
