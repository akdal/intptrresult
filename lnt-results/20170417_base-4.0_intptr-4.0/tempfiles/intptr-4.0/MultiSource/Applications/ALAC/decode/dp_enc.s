	.text
	.file	"dp_enc.bc"
	.globl	init_coefs
	.p2align	4, 0x90
	.type	init_coefs,@function
init_coefs:                             # @init_coefs
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%esi, %ecx
	movl	$38, %eax
	shll	%cl, %eax
	shrl	$4, %eax
	movw	%ax, (%rdi)
	movl	$-29, %eax
	shll	%cl, %eax
	shrl	$4, %eax
	movw	%ax, 2(%rdi)
	movl	$-2, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	shrl	$4, %eax
	movw	%ax, 4(%rdi)
	cmpl	$4, %edx
	jl	.LBB0_2
# BB#1:                                 # %.lr.ph.preheader
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	addq	$6, %rdi
	addl	$-4, %edx
	leaq	2(%rdx,%rdx), %rdx
	xorl	%esi, %esi
	callq	memset
	addq	$8, %rsp
.LBB0_2:                                # %._crit_edge
	retq
.Lfunc_end0:
	.size	init_coefs, .Lfunc_end0-init_coefs
	.cfi_endproc

	.globl	copy_coefs
	.p2align	4, 0x90
	.type	copy_coefs,@function
copy_coefs:                             # @copy_coefs
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	testl	%edx, %edx
	jle	.LBB1_17
# BB#1:                                 # %.lr.ph.preheader
	movl	%edx, %r8d
	cmpl	$16, %edx
	jae	.LBB1_3
# BB#2:
	xorl	%ecx, %ecx
	jmp	.LBB1_11
.LBB1_3:                                # %min.iters.checked
	andl	$15, %edx
	movq	%r8, %rcx
	subq	%rdx, %rcx
	je	.LBB1_7
# BB#4:                                 # %vector.memcheck
	leaq	(%rdi,%r8,2), %rax
	cmpq	%rsi, %rax
	jbe	.LBB1_8
# BB#5:                                 # %vector.memcheck
	leaq	(%rsi,%r8,2), %rax
	cmpq	%rdi, %rax
	jbe	.LBB1_8
.LBB1_7:
	xorl	%ecx, %ecx
	jmp	.LBB1_11
.LBB1_8:                                # %vector.body.preheader
	leaq	16(%rdi), %r9
	leaq	16(%rsi), %rax
	movq	%rcx, %r10
	.p2align	4, 0x90
.LBB1_9:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%r9), %xmm0
	movups	(%r9), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$32, %r9
	addq	$32, %rax
	addq	$-16, %r10
	jne	.LBB1_9
# BB#10:                                # %middle.block
	testl	%edx, %edx
	je	.LBB1_17
.LBB1_11:                               # %.lr.ph.preheader13
	movl	%r8d, %eax
	subl	%ecx, %eax
	leaq	-1(%r8), %r9
	subq	%rcx, %r9
	andq	$7, %rax
	je	.LBB1_14
# BB#12:                                # %.lr.ph.prol.preheader
	negq	%rax
	.p2align	4, 0x90
.LBB1_13:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rdi,%rcx,2), %edx
	movw	%dx, (%rsi,%rcx,2)
	incq	%rcx
	incq	%rax
	jne	.LBB1_13
.LBB1_14:                               # %.lr.ph.prol.loopexit
	cmpq	$7, %r9
	jb	.LBB1_17
# BB#15:                                # %.lr.ph.preheader13.new
	subq	%rcx, %r8
	leaq	14(%rsi,%rcx,2), %rdx
	leaq	14(%rdi,%rcx,2), %rcx
	.p2align	4, 0x90
.LBB1_16:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-14(%rcx), %eax
	movw	%ax, -14(%rdx)
	movzwl	-12(%rcx), %eax
	movw	%ax, -12(%rdx)
	movzwl	-10(%rcx), %eax
	movw	%ax, -10(%rdx)
	movzwl	-8(%rcx), %eax
	movw	%ax, -8(%rdx)
	movzwl	-6(%rcx), %eax
	movw	%ax, -6(%rdx)
	movzwl	-4(%rcx), %eax
	movw	%ax, -4(%rdx)
	movzwl	-2(%rcx), %eax
	movw	%ax, -2(%rdx)
	movzwl	(%rcx), %eax
	movw	%ax, (%rdx)
	addq	$16, %rdx
	addq	$16, %rcx
	addq	$-8, %r8
	jne	.LBB1_16
.LBB1_17:                               # %._crit_edge
	retq
.Lfunc_end1:
	.size	copy_coefs, .Lfunc_end1-copy_coefs
	.cfi_endproc

	.globl	pc_block
	.p2align	4, 0x90
	.type	pc_block,@function
pc_block:                               # @pc_block
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$32, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 88
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%rcx, %r11
	movl	%edx, -124(%rsp)        # 4-byte Spill
	movq	%rdi, %r10
	movl	88(%rsp), %edx
	leal	-1(%rdx), %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movl	(%r10), %eax
	movl	%eax, (%rsi)
	testl	%r8d, %r8d
	je	.LBB2_13
# BB#1:
	movq	%rdx, -104(%rsp)        # 8-byte Spill
	movl	$32, %r15d
	subl	%r9d, %r15d
	cmpl	$31, %r8d
	jne	.LBB2_16
# BB#2:                                 # %.preheader537
	movl	-124(%rsp), %eax        # 4-byte Reload
	cmpl	$2, %eax
	jl	.LBB2_112
# BB#3:                                 # %.lr.ph579.preheader
	movl	%eax, %edx
	leaq	-1(%rdx), %rdi
	cmpq	$7, %rdi
	jbe	.LBB2_7
# BB#4:                                 # %min.iters.checked
	movq	%rdi, %r8
	andq	$-8, %r8
	je	.LBB2_7
# BB#5:                                 # %vector.memcheck
	leaq	4(%rsi), %rax
	leaq	(%r10,%rdx,4), %rcx
	cmpq	%rcx, %rax
	jae	.LBB2_74
# BB#6:                                 # %vector.memcheck
	leaq	(%rsi,%rdx,4), %rax
	cmpq	%r10, %rax
	jbe	.LBB2_74
.LBB2_7:
	movl	$1, %ebp
.LBB2_8:                                # %.lr.ph579.preheader707
	movl	%edx, %eax
	subl	%ebp, %eax
	testb	$1, %al
	movq	%rbp, %rcx
	je	.LBB2_10
# BB#9:                                 # %.lr.ph579.prol
	movl	(%r10,%rbp,4), %eax
	subl	-4(%r10,%rbp,4), %eax
	movl	%r15d, %ecx
	shll	%cl, %eax
	sarl	%cl, %eax
	movl	%eax, (%rsi,%rbp,4)
	leaq	1(%rbp), %rcx
.LBB2_10:                               # %.lr.ph579.prol.loopexit
	cmpq	%rbp, %rdi
	je	.LBB2_112
# BB#11:                                # %.lr.ph579.preheader707.new
	subq	%rcx, %rdx
	leaq	(%r10,%rcx,4), %rax
	leaq	4(%rsi,%rcx,4), %rsi
	.p2align	4, 0x90
.LBB2_12:                               # %.lr.ph579
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edi
	subl	-4(%rax), %edi
	movl	%r15d, %ecx
	shll	%cl, %edi
	sarl	%cl, %edi
	movl	%edi, -4(%rsi)
	movl	4(%rax), %edi
	subl	(%rax), %edi
	shll	%cl, %edi
	sarl	%cl, %edi
	movl	%edi, (%rsi)
	addq	$8, %rax
	addq	$8, %rsi
	addq	$-2, %rdx
	jne	.LBB2_12
	jmp	.LBB2_112
.LBB2_13:
	cmpq	%rsi, %r10
	movl	-124(%rsp), %eax        # 4-byte Reload
	je	.LBB2_112
# BB#14:
	cmpl	$2, %eax
	jl	.LBB2_112
# BB#15:
	addq	$4, %rsi
	addq	$4, %r10
	cltq
	leaq	-4(,%rax,4), %rdx
	movq	%rsi, %rdi
	movq	%r10, %rsi
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	memcpy                  # TAILCALL
.LBB2_16:                               # %.preheader536
	leal	1(%r8), %r12d
	testl	%r8d, %r8d
	jle	.LBB2_27
# BB#17:                                # %.lr.ph576.preheader
	movl	%r12d, %edx
	leaq	-1(%rdx), %rdi
	cmpq	$7, %rdi
	jbe	.LBB2_21
# BB#18:                                # %min.iters.checked642
	movq	%rdi, %r9
	andq	$-8, %r9
	je	.LBB2_21
# BB#19:                                # %vector.memcheck657
	leaq	4(%rsi), %rax
	leaq	(%r10,%rdx,4), %rcx
	cmpq	%rcx, %rax
	jae	.LBB2_71
# BB#20:                                # %vector.memcheck657
	leaq	(%rsi,%rdx,4), %rax
	cmpq	%r10, %rax
	jbe	.LBB2_71
.LBB2_21:
	movl	$1, %ebp
.LBB2_22:                               # %.lr.ph576.preheader706
	movl	%edx, %eax
	subl	%ebp, %eax
	testb	$1, %al
	movq	%rbp, %rcx
	je	.LBB2_24
# BB#23:                                # %.lr.ph576.prol
	movl	(%r10,%rbp,4), %eax
	subl	-4(%r10,%rbp,4), %eax
	movl	%r15d, %ecx
	shll	%cl, %eax
	sarl	%cl, %eax
	movl	%eax, (%rsi,%rbp,4)
	leaq	1(%rbp), %rcx
.LBB2_24:                               # %.lr.ph576.prol.loopexit
	cmpq	%rbp, %rdi
	je	.LBB2_27
# BB#25:                                # %.lr.ph576.preheader706.new
	subq	%rcx, %rdx
	leaq	(%r10,%rcx,4), %rax
	leaq	4(%rsi,%rcx,4), %rdi
	.p2align	4, 0x90
.LBB2_26:                               # %.lr.ph576
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ebp
	subl	-4(%rax), %ebp
	movl	%r15d, %ecx
	shll	%cl, %ebp
	sarl	%cl, %ebp
	movl	%ebp, -4(%rdi)
	movl	4(%rax), %ebp
	subl	(%rax), %ebp
	shll	%cl, %ebp
	sarl	%cl, %ebp
	movl	%ebp, (%rdi)
	addq	$8, %rax
	addq	$8, %rdi
	addq	$-2, %rdx
	jne	.LBB2_26
.LBB2_27:                               # %._crit_edge577
	cmpl	$4, %r8d
	movl	%r15d, -60(%rsp)        # 4-byte Spill
	je	.LBB2_31
# BB#28:                                # %._crit_edge577
	cmpl	$8, %r8d
	movl	-124(%rsp), %edi        # 4-byte Reload
	jne	.LBB2_49
# BB#29:
	movzwl	(%r11), %eax
	movw	%ax, -40(%rsp)          # 2-byte Spill
	movzwl	2(%r11), %eax
	movw	%ax, -48(%rsp)          # 2-byte Spill
	movzwl	4(%r11), %eax
	movw	%ax, -56(%rsp)          # 2-byte Spill
	movzwl	6(%r11), %eax
	movw	%ax, -88(%rsp)          # 2-byte Spill
	movzwl	8(%r11), %eax
	movw	%ax, -72(%rsp)          # 2-byte Spill
	movzwl	10(%r11), %eax
	movw	%ax, -120(%rsp)         # 2-byte Spill
	movzwl	12(%r11), %r13d
	movzwl	14(%r11), %r9d
	cmpl	%edi, %r12d
	jge	.LBB2_111
# BB#30:                                # %.lr.ph565.preheader
	movslq	%r12d, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(%rsi,%rax,4), %r15
	decl	%edi
	subl	%r8d, %edi
	movq	%r11, -96(%rsp)         # 8-byte Spill
	jmp	.LBB2_92
.LBB2_31:
	movzwl	(%r11), %eax
	movw	%ax, -120(%rsp)         # 2-byte Spill
	movzwl	2(%r11), %eax
	movw	%ax, -112(%rsp)         # 2-byte Spill
	movzwl	4(%r11), %r9d
	movzwl	6(%r11), %ecx
	movl	-124(%rsp), %edi        # 4-byte Reload
	cmpl	%edi, %r12d
	jge	.LBB2_69
# BB#32:                                # %.lr.ph549.preheader
	movslq	%r12d, %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	leaq	(%rsi,%rax,4), %rsi
	decl	%edi
	subl	%r8d, %edi
	movl	%ecx, %r12d
	movq	-104(%rsp), %r15        # 8-byte Reload
	.p2align	4, 0x90
.LBB2_33:                               # %.lr.ph549
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r10), %edx
	movl	%edx, %r8d
	movl	%edx, %r13d
	movl	%edx, %r14d
	movq	-56(%rsp), %rax         # 8-byte Reload
	movl	(%r10,%rax,4), %ebx
	subl	%edx, %ebx
	subl	-4(%r10,%rax,4), %edx
	subl	-8(%r10,%rax,4), %r8d
	subl	-12(%r10,%rax,4), %r13d
	subl	-16(%r10,%rax,4), %r14d
	movswl	-120(%rsp), %ecx        # 2-byte Folded Reload
	movl	%edx, %eax
	movl	%ecx, -88(%rsp)         # 4-byte Spill
	imull	%ecx, %eax
	movswl	-112(%rsp), %ebp        # 2-byte Folded Reload
	movl	%r8d, %ecx
	movl	%ebp, -72(%rsp)         # 4-byte Spill
	imull	%ebp, %ecx
	movw	%r9w, -124(%rsp)        # 2-byte Spill
	movswl	%r9w, %r9d
	movl	%r13d, %ebp
	imull	%r9d, %ebp
	addl	%eax, %ecx
	movswl	%r12w, %eax
	addl	%ebp, %ecx
	movl	%r14d, %ebp
	imull	%eax, %ebp
	addl	%ebp, %ecx
	movq	-32(%rsp), %rbp         # 8-byte Reload
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill>
	subl	%ecx, %ebp
	movl	%r15d, %ecx
	sarl	%cl, %ebp
	subl	%ebp, %ebx
	movl	-60(%rsp), %ecx         # 4-byte Reload
	shll	%cl, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %ebx
	movl	%ebx, %ecx
	negl	%ecx
	shrl	$31, %ecx
	movl	%ebx, %ebp
	sarl	$31, %ebp
	orl	%ecx, %ebp
	testl	%ebp, %ebp
	movl	%ebx, (%rsi)
	jle	.LBB2_39
# BB#34:                                #   in Loop: Header=BB2_33 Depth=1
	movl	%r14d, %ecx
	negl	%ecx
	shrl	$31, %ecx
	movl	%r14d, %ebp
	sarl	$31, %ebp
	orl	%ecx, %ebp
	subl	%ebp, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	movw	%ax, -80(%rsp)          # 2-byte Spill
	imull	%r14d, %ebp
	movl	%r15d, %ecx
	sarl	%cl, %ebp
	subl	%ebp, %ebx
	jle	.LBB2_41
# BB#35:                                #   in Loop: Header=BB2_33 Depth=1
	movl	%r13d, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%r13d, %ebp
	sarl	$31, %ebp
	orl	%eax, %ebp
	subl	%ebp, %r9d
                                        # kill: %R9W<def> %R9W<kill> %R9D<kill>
	imull	%r13d, %ebp
	movl	%r15d, %ecx
	sarl	%cl, %ebp
	addl	%ebp, %ebp
	subl	%ebp, %ebx
	jle	.LBB2_42
# BB#36:                                #   in Loop: Header=BB2_33 Depth=1
	movl	%edi, %ebp
	movl	%r8d, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%r8d, %edi
	sarl	$31, %edi
	orl	%eax, %edi
	movl	-72(%rsp), %eax         # 4-byte Reload
	subl	%edi, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	movw	%ax, -112(%rsp)         # 2-byte Spill
	imull	%r8d, %edi
	movl	%r15d, %ecx
	sarl	%cl, %edi
	imull	$-3, %edi, %eax
	addl	%eax, %ebx
	testl	%ebx, %ebx
	jle	.LBB2_38
# BB#37:                                #   in Loop: Header=BB2_33 Depth=1
	movl	%edx, %eax
	negl	%eax
	shrl	$31, %eax
	sarl	$31, %edx
	orl	%eax, %edx
	movl	-88(%rsp), %eax         # 4-byte Reload
	subl	%edx, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	movw	%ax, -120(%rsp)         # 2-byte Spill
.LBB2_38:                               #   in Loop: Header=BB2_33 Depth=1
	movl	%ebp, %edi
	movzwl	-80(%rsp), %r12d        # 2-byte Folded Reload
	jmp	.LBB2_48
	.p2align	4, 0x90
.LBB2_39:                               #   in Loop: Header=BB2_33 Depth=1
	js	.LBB2_43
# BB#40:                                #   in Loop: Header=BB2_33 Depth=1
	movzwl	-124(%rsp), %r9d        # 2-byte Folded Reload
	jmp	.LBB2_48
	.p2align	4, 0x90
.LBB2_41:                               #   in Loop: Header=BB2_33 Depth=1
	movzwl	-80(%rsp), %r12d        # 2-byte Folded Reload
	movzwl	-124(%rsp), %r9d        # 2-byte Folded Reload
	jmp	.LBB2_48
.LBB2_42:                               #   in Loop: Header=BB2_33 Depth=1
	movzwl	-80(%rsp), %r12d        # 2-byte Folded Reload
	jmp	.LBB2_48
.LBB2_43:                               #   in Loop: Header=BB2_33 Depth=1
	movl	%edi, %ebp
	movl	%r14d, %ecx
	negl	%ecx
	movl	%ecx, %edi
	shrl	$31, %edi
	sarl	$31, %r14d
	orl	%edi, %r14d
	addl	%r14d, %eax
	movl	%eax, %r12d
	imull	%ecx, %r14d
	movq	-104(%rsp), %rcx        # 8-byte Reload
	sarl	%cl, %r14d
	subl	%r14d, %ebx
	js	.LBB2_45
# BB#44:                                #   in Loop: Header=BB2_33 Depth=1
	movl	%ebp, %edi
	movzwl	-124(%rsp), %r9d        # 2-byte Folded Reload
	jmp	.LBB2_48
.LBB2_45:                               #   in Loop: Header=BB2_33 Depth=1
	movl	%r13d, %eax
	negl	%eax
	movl	%eax, %edi
	shrl	$31, %edi
	sarl	$31, %r13d
	orl	%edi, %r13d
	addl	%r13d, %r9d
                                        # kill: %R9W<def> %R9W<kill> %R9D<kill>
	imull	%eax, %r13d
	sarl	%cl, %r13d
	addl	%r13d, %r13d
	subl	%r13d, %ebx
	movl	%ebp, %edi
	jns	.LBB2_48
# BB#46:                                #   in Loop: Header=BB2_33 Depth=1
	movl	%r8d, %eax
	negl	%eax
	movl	%eax, %ebp
	shrl	$31, %ebp
	sarl	$31, %r8d
	orl	%ebp, %r8d
	movl	-72(%rsp), %ebp         # 4-byte Reload
	addl	%r8d, %ebp
                                        # kill: %BP<def> %BP<kill> %EBP<kill>
	movw	%bp, -112(%rsp)         # 2-byte Spill
	imull	%eax, %r8d
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	sarl	%cl, %r8d
	imull	$-3, %r8d, %eax
	addl	%eax, %ebx
	jns	.LBB2_48
# BB#47:                                #   in Loop: Header=BB2_33 Depth=1
	movl	%edx, %eax
	negl	%eax
	shrl	$31, %eax
	sarl	$31, %edx
	orl	%eax, %edx
	addl	-88(%rsp), %edx         # 4-byte Folded Reload
	movl	%edx, %eax
	movw	%ax, -120(%rsp)         # 2-byte Spill
	.p2align	4, 0x90
.LBB2_48:                               #   in Loop: Header=BB2_33 Depth=1
	addq	$4, %r10
	addq	$4, %rsi
	decl	%edi
	jne	.LBB2_33
	jmp	.LBB2_70
.LBB2_49:                               # %.preheader534
	cmpl	%edi, %r12d
	jge	.LBB2_112
# BB#50:                                # %.lr.ph542
	movabsq	$4294967296, %rdx       # imm = 0x100000000
	testl	%r8d, %r8d
	movslq	%r8d, %rdi
	movq	%rdi, -120(%rsp)        # 8-byte Spill
	jle	.LBB2_77
# BB#51:                                # %.lr.ph542.split.us.preheader
	movslq	%r12d, %r14
	movl	%r8d, %eax
	movl	%r8d, %ecx
	andl	$7, %ecx
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	subq	%rcx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%r11), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	-16(%r10,%r14,4), %rbx
	leaq	-4(%r10,%r14,4), %r9
	movq	%rdi, %rax
	shlq	$32, %rax
	subq	%rax, %rdx
	movq	%rdx, -48(%rsp)         # 8-byte Spill
	movq	%r14, -88(%rsp)         # 8-byte Spill
	movq	%rsi, -72(%rsp)         # 8-byte Spill
	movq	%r8, -80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_52:                               # %.lr.ph542.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_55 Depth 2
                                        #     Child Loop BB2_59 Depth 2
                                        #     Child Loop BB2_66 Depth 2
                                        #     Child Loop BB2_62 Depth 2
	movq	%r14, %rax
	subq	-88(%rsp), %rax         # 8-byte Folded Reload
	movl	(%r10,%rax,4), %r12d
	leaq	(%r10,%r14,4), %rcx
	cmpl	$7, %r8d
	movq	%rbx, -112(%rsp)        # 8-byte Spill
	jbe	.LBB2_57
# BB#53:                                # %min.iters.checked679
                                        #   in Loop: Header=BB2_52 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB2_57
# BB#54:                                # %vector.ph683
                                        #   in Loop: Header=BB2_52 Depth=1
	movd	%r12d, %xmm0
	pshufd	$0, %xmm0, %xmm2        # xmm2 = xmm0[0,0,0,0]
	pxor	%xmm0, %xmm0
	movq	%rax, %rdx
	movq	%rbx, %rdi
	movq	8(%rsp), %rbp           # 8-byte Reload
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB2_55:                               # %vector.body675
                                        #   Parent Loop BB2_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rbp), %xmm3         # xmm3 = mem[0],zero
	punpcklwd	%xmm3, %xmm3    # xmm3 = xmm3[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm3
	movq	(%rbp), %xmm4           # xmm4 = mem[0],zero
	punpcklwd	%xmm4, %xmm4    # xmm4 = xmm4[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm4
	movdqu	-16(%rdi), %xmm5
	movdqu	(%rdi), %xmm6
	pshufd	$27, %xmm6, %xmm6       # xmm6 = xmm6[3,2,1,0]
	pshufd	$27, %xmm5, %xmm5       # xmm5 = xmm5[3,2,1,0]
	movdqa	%xmm2, %xmm7
	psubd	%xmm6, %xmm7
	movdqa	%xmm2, %xmm6
	psubd	%xmm5, %xmm6
	pshufd	$245, %xmm7, %xmm5      # xmm5 = xmm7[1,1,3,3]
	pmuludq	%xmm3, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pshufd	$245, %xmm3, %xmm3      # xmm3 = xmm3[1,1,3,3]
	pmuludq	%xmm5, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm7    # xmm7 = xmm7[0],xmm3[0],xmm7[1],xmm3[1]
	pshufd	$245, %xmm6, %xmm3      # xmm3 = xmm6[1,1,3,3]
	pmuludq	%xmm4, %xmm6
	pshufd	$232, %xmm6, %xmm5      # xmm5 = xmm6[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm3, %xmm4
	pshufd	$232, %xmm4, %xmm3      # xmm3 = xmm4[0,2,2,3]
	punpckldq	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	psubd	%xmm7, %xmm0
	psubd	%xmm5, %xmm1
	addq	$16, %rbp
	addq	$-32, %rdi
	addq	$-8, %rdx
	jne	.LBB2_55
# BB#56:                                # %middle.block676
                                        #   in Loop: Header=BB2_52 Depth=1
	cmpl	$0, -40(%rsp)           # 4-byte Folded Reload
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %ebp
	movq	%rax, %rbx
	jne	.LBB2_58
	jmp	.LBB2_60
	.p2align	4, 0x90
.LBB2_57:                               #   in Loop: Header=BB2_52 Depth=1
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
.LBB2_58:                               # %scalar.ph677.preheader
                                        #   in Loop: Header=BB2_52 Depth=1
	leaq	(,%rbx,4), %rdx
	movq	%r9, %rax
	subq	%rdx, %rax
	leaq	(%r11,%rbx,2), %rdx
	movq	-56(%rsp), %rdi         # 8-byte Reload
	subq	%rbx, %rdi
	.p2align	4, 0x90
.LBB2_59:                               # %scalar.ph677
                                        #   Parent Loop BB2_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	(%rdx), %esi
	movl	%r12d, %ebx
	subl	(%rax), %ebx
	imull	%esi, %ebx
	subl	%ebx, %ebp
	addq	$-4, %rax
	addq	$2, %rdx
	decq	%rdi
	jne	.LBB2_59
.LBB2_60:                               # %._crit_edge.us
                                        #   in Loop: Header=BB2_52 Depth=1
	leaq	-4(%r10,%r14,4), %r8
	movl	(%rcx), %r13d
	addl	-32(%rsp), %ebp         # 4-byte Folded Reload
	movq	-104(%rsp), %rcx        # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	sarl	%cl, %ebp
	addl	%r12d, %ebp
	subl	%ebp, %r13d
	movl	%r15d, %ecx
	shll	%cl, %r13d
	sarl	%cl, %r13d
	movq	-72(%rsp), %rax         # 8-byte Reload
	movl	%r13d, (%rax,%r14,4)
	movl	%r13d, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%r13d, %ecx
	sarl	$31, %ecx
	orl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.LBB2_64
# BB#61:                                # %.preheader.us.preheader
                                        #   in Loop: Header=BB2_52 Depth=1
	movl	$1, %ebp
	movq	-48(%rsp), %rax         # 8-byte Reload
	movq	-120(%rsp), %rbx        # 8-byte Reload
	movq	-104(%rsp), %rcx        # 8-byte Reload
	movabsq	$4294967296, %r15       # imm = 0x100000000
	.p2align	4, 0x90
.LBB2_62:                               # %.preheader.us
                                        #   Parent Loop BB2_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rbx, %rbx
	jle	.LBB2_68
# BB#63:                                #   in Loop: Header=BB2_62 Depth=2
	movq	%rax, %rsi
	sarq	$30, %rsi
	movl	%r12d, %edi
	subl	(%r8,%rsi), %edi
	movl	%edi, %edx
	negl	%edx
	shrl	$31, %edx
	movl	%edi, %esi
	sarl	$31, %esi
	orl	%edx, %esi
	movzwl	-2(%r11,%rbx,2), %edx
	subl	%esi, %edx
	movw	%dx, -2(%r11,%rbx,2)
	decq	%rbx
	imull	%edi, %esi
	sarl	%cl, %esi
	imull	%ebp, %esi
	addq	%r15, %rax
	incl	%ebp
	subl	%esi, %r13d
	jg	.LBB2_62
	jmp	.LBB2_68
	.p2align	4, 0x90
.LBB2_64:                               #   in Loop: Header=BB2_52 Depth=1
	jns	.LBB2_68
# BB#65:                                # %.preheader532.us.preheader
                                        #   in Loop: Header=BB2_52 Depth=1
	movl	$1, %ebp
	movq	-48(%rsp), %rax         # 8-byte Reload
	movq	-120(%rsp), %rbx        # 8-byte Reload
	.p2align	4, 0x90
.LBB2_66:                               # %.preheader532.us
                                        #   Parent Loop BB2_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rbx, %rbx
	jle	.LBB2_68
# BB#67:                                #   in Loop: Header=BB2_66 Depth=2
	movq	%rax, %rcx
	sarq	$30, %rcx
	movl	%r12d, %r15d
	subl	(%r8,%rcx), %r15d
	movl	%r15d, %esi
	negl	%esi
	movl	%esi, %ecx
	shrl	$31, %ecx
	sarl	$31, %r15d
	orl	%ecx, %r15d
	movzwl	-2(%r11,%rbx,2), %ecx
	addl	%r15d, %ecx
	movw	%cx, -2(%r11,%rbx,2)
	decq	%rbx
	imull	%esi, %r15d
	movq	-104(%rsp), %rcx        # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	sarl	%cl, %r15d
	imull	%ebp, %r15d
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	addq	%rcx, %rax
	incl	%ebp
	subl	%r15d, %r13d
	js	.LBB2_66
	.p2align	4, 0x90
.LBB2_68:                               # %.loopexit.us
                                        #   in Loop: Header=BB2_52 Depth=1
	incq	%r14
	movq	-112(%rsp), %rbx        # 8-byte Reload
	addq	$4, %rbx
	addq	$4, %r9
	cmpl	-124(%rsp), %r14d       # 4-byte Folded Reload
	movl	-60(%rsp), %r15d        # 4-byte Reload
	movq	-80(%rsp), %r8          # 8-byte Reload
	jne	.LBB2_52
	jmp	.LBB2_112
.LBB2_69:
	movl	%ecx, %r12d
.LBB2_70:                               # %._crit_edge550
	movzwl	-120(%rsp), %eax        # 2-byte Folded Reload
	movw	%ax, (%r11)
	movzwl	-112(%rsp), %eax        # 2-byte Folded Reload
	movw	%ax, 2(%r11)
	movw	%r9w, 4(%r11)
	movw	%r12w, 6(%r11)
	jmp	.LBB2_112
.LBB2_71:                               # %vector.ph658
	movq	%r9, %rbp
	orq	$1, %rbp
	movd	%r15d, %xmm1
	leaq	20(%r10), %rax
	leaq	20(%rsi), %rbx
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movq	%r9, %rcx
	.p2align	4, 0x90
.LBB2_72:                               # %vector.body638
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-20(%rax), %xmm1
	movdqu	-16(%rax), %xmm2
	movdqu	-4(%rax), %xmm3
	movdqu	(%rax), %xmm4
	psubd	%xmm1, %xmm2
	psubd	%xmm3, %xmm4
	pslld	%xmm0, %xmm2
	pslld	%xmm0, %xmm4
	psrad	%xmm0, %xmm2
	psrad	%xmm0, %xmm4
	movdqu	%xmm2, -16(%rbx)
	movdqu	%xmm4, (%rbx)
	addq	$32, %rax
	addq	$32, %rbx
	addq	$-8, %rcx
	jne	.LBB2_72
# BB#73:                                # %middle.block639
	cmpq	%r9, %rdi
	jne	.LBB2_22
	jmp	.LBB2_27
.LBB2_74:                               # %vector.ph
	movq	%r8, %rbp
	orq	$1, %rbp
	movd	%r15d, %xmm1
	leaq	20(%r10), %rax
	leaq	20(%rsi), %rbx
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB2_75:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-20(%rax), %xmm1
	movdqu	-16(%rax), %xmm2
	movdqu	-4(%rax), %xmm3
	movdqu	(%rax), %xmm4
	psubd	%xmm1, %xmm2
	psubd	%xmm3, %xmm4
	pslld	%xmm0, %xmm2
	pslld	%xmm0, %xmm4
	psrad	%xmm0, %xmm2
	psrad	%xmm0, %xmm4
	movdqu	%xmm2, -16(%rbx)
	movdqu	%xmm4, (%rbx)
	addq	$32, %rax
	addq	$32, %rbx
	addq	$-8, %rcx
	jne	.LBB2_75
# BB#76:                                # %middle.block
	cmpq	%r8, %rdi
	jne	.LBB2_8
	jmp	.LBB2_112
.LBB2_77:                               # %.lr.ph542.split.preheader
	leaq	1(%rdi), %r14
	movslq	%r12d, %r9
	movq	-104(%rsp), %rcx        # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	movq	-32(%rsp), %rax         # 8-byte Reload
	sarl	%cl, %eax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movq	%rdi, %rax
	shlq	$32, %rax
	movq	%rdx, %r8
	subq	%rax, %r8
	.p2align	4, 0x90
.LBB2_78:                               # %.lr.ph542.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_84 Depth 2
                                        #     Child Loop BB2_80 Depth 2
	movq	%r14, %rax
	subq	%r9, %rax
	movl	(%r10,%rax,4), %r12d
	movl	%r15d, %ecx
	leaq	-4(%r10,%r14,4), %r15
	movl	(%r10,%r14,4), %r13d
	movq	-32(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r12), %eax
	subl	%eax, %r13d
	shll	%cl, %r13d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r13d
	movl	%r13d, (%rsi,%r14,4)
	movl	%r13d, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%r13d, %ecx
	sarl	$31, %ecx
	orl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.LBB2_82
# BB#79:                                # %.preheader.preheader
                                        #   in Loop: Header=BB2_78 Depth=1
	movl	$1, %eax
	movq	%r8, %rdx
	movq	-120(%rsp), %rdi        # 8-byte Reload
	.p2align	4, 0x90
.LBB2_80:                               # %.preheader
                                        #   Parent Loop BB2_78 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rdi, %rdi
	jle	.LBB2_86
# BB#81:                                #   in Loop: Header=BB2_80 Depth=2
	movq	%rdx, %rcx
	sarq	$30, %rcx
	movl	%r12d, %ebp
	subl	(%r15,%rcx), %ebp
	movl	%ebp, %ecx
	negl	%ecx
	shrl	$31, %ecx
	movl	%ebp, %ebx
	sarl	$31, %ebx
	orl	%ecx, %ebx
	movzwl	-2(%r11,%rdi,2), %ecx
	subl	%ebx, %ecx
	movw	%cx, -2(%r11,%rdi,2)
	decq	%rdi
	imull	%ebp, %ebx
	movq	-104(%rsp), %rcx        # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	sarl	%cl, %ebx
	imull	%eax, %ebx
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	addq	%rcx, %rdx
	incl	%eax
	subl	%ebx, %r13d
	jg	.LBB2_80
	jmp	.LBB2_86
	.p2align	4, 0x90
.LBB2_82:                               #   in Loop: Header=BB2_78 Depth=1
	jns	.LBB2_86
# BB#83:                                # %.preheader532.preheader
                                        #   in Loop: Header=BB2_78 Depth=1
	movl	$1, %eax
	movq	%r8, %rdx
	movq	-120(%rsp), %rdi        # 8-byte Reload
	.p2align	4, 0x90
.LBB2_84:                               # %.preheader532
                                        #   Parent Loop BB2_78 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rdi, %rdi
	jle	.LBB2_86
# BB#85:                                #   in Loop: Header=BB2_84 Depth=2
	movq	%rdx, %rcx
	sarq	$30, %rcx
	movl	%r12d, %ebp
	subl	(%r15,%rcx), %ebp
	movl	%ebp, %ecx
	negl	%ecx
	movl	%ecx, %ebx
	shrl	$31, %ebx
	sarl	$31, %ebp
	orl	%ebx, %ebp
	movzwl	-2(%r11,%rdi,2), %ebx
	addl	%ebp, %ebx
	movw	%bx, -2(%r11,%rdi,2)
	decq	%rdi
	imull	%ecx, %ebp
	movq	-104(%rsp), %rcx        # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	sarl	%cl, %ebp
	imull	%eax, %ebp
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	addq	%rcx, %rdx
	incl	%eax
	subl	%ebp, %r13d
	js	.LBB2_84
	.p2align	4, 0x90
.LBB2_86:                               # %.loopexit
                                        #   in Loop: Header=BB2_78 Depth=1
	incq	%r14
	cmpl	-124(%rsp), %r14d       # 4-byte Folded Reload
	movl	-60(%rsp), %r15d        # 4-byte Reload
	jne	.LBB2_78
	jmp	.LBB2_112
.LBB2_87:                               #   in Loop: Header=BB2_92 Depth=1
	movl	%r14d, %eax
	negl	%eax
	movl	%eax, %edx
	shrl	$31, %edx
	sarl	$31, %r14d
	orl	%edx, %r14d
	movl	16(%rsp), %edx          # 4-byte Reload
	addl	%r14d, %edx
                                        # kill: %DX<def> %DX<kill> %EDX<kill>
	movw	%dx, -72(%rsp)          # 2-byte Spill
	imull	%eax, %r14d
	sarl	%cl, %r14d
	shll	$2, %r14d
	subl	%r14d, %r12d
	movl	-124(%rsp), %edi        # 4-byte Reload
	movq	-96(%rsp), %r11         # 8-byte Reload
	jns	.LBB2_110
# BB#88:                                #   in Loop: Header=BB2_92 Depth=1
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	%esi, %eax
	negl	%eax
	movl	%eax, %edx
	shrl	$31, %edx
	sarl	$31, %esi
	orl	%edx, %esi
	movl	4(%rsp), %edx           # 4-byte Reload
	addl	%esi, %edx
                                        # kill: %DX<def> %DX<kill> %EDX<kill>
	movw	%dx, -88(%rsp)          # 2-byte Spill
	imull	%eax, %esi
	sarl	%cl, %esi
	imull	$-5, %esi, %eax
	addl	%eax, %r12d
	jns	.LBB2_110
# BB#89:                                #   in Loop: Header=BB2_92 Depth=1
	movl	(%rsp), %esi            # 4-byte Reload
	movl	%esi, %eax
	negl	%eax
	movl	%eax, %edx
	shrl	$31, %edx
	sarl	$31, %esi
	orl	%edx, %esi
	movl	-4(%rsp), %edx          # 4-byte Reload
	addl	%esi, %edx
                                        # kill: %DX<def> %DX<kill> %EDX<kill>
	movw	%dx, -56(%rsp)          # 2-byte Spill
	imull	%eax, %esi
	sarl	%cl, %esi
	imull	$-6, %esi, %eax
	addl	%eax, %r12d
	jns	.LBB2_110
# BB#90:                                #   in Loop: Header=BB2_92 Depth=1
	movl	-8(%rsp), %esi          # 4-byte Reload
	movl	%esi, %eax
	negl	%eax
	movl	%eax, %edx
	shrl	$31, %edx
	sarl	$31, %esi
	orl	%edx, %esi
	movl	-12(%rsp), %edx         # 4-byte Reload
	addl	%esi, %edx
                                        # kill: %DX<def> %DX<kill> %EDX<kill>
	movw	%dx, -48(%rsp)          # 2-byte Spill
	imull	%eax, %esi
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	sarl	%cl, %esi
	imull	$-7, %esi, %eax
	addl	%eax, %r12d
	jns	.LBB2_110
# BB#91:                                #   in Loop: Header=BB2_92 Depth=1
	movl	-16(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, %eax
	negl	%eax
	shrl	$31, %eax
	sarl	$31, %ecx
	orl	%eax, %ecx
	addl	-20(%rsp), %ecx         # 4-byte Folded Reload
	movl	%ecx, %eax
	jmp	.LBB2_101
	.p2align	4, 0x90
.LBB2_92:                               # %.lr.ph565
                                        # =>This Inner Loop Header: Depth=1
	movl	%edi, -124(%rsp)        # 4-byte Spill
	movl	(%r10), %ebx
	movl	%ebx, %edi
	movl	%ebx, %ecx
	movl	%ebx, %edx
	movl	%ebx, %r14d
	movl	%ebx, %r11d
	movl	%ebx, %eax
	movq	24(%rsp), %rbp          # 8-byte Reload
	subl	-4(%r10,%rbp,4), %eax
	subl	-8(%r10,%rbp,4), %edi
	subl	-12(%r10,%rbp,4), %ecx
	subl	-16(%r10,%rbp,4), %edx
	subl	-20(%r10,%rbp,4), %r14d
	subl	-24(%r10,%rbp,4), %r11d
	movswl	-40(%rsp), %esi         # 2-byte Folded Reload
	movl	%eax, -16(%rsp)         # 4-byte Spill
	movl	%eax, %r8d
	movl	%esi, -20(%rsp)         # 4-byte Spill
	imull	%esi, %r8d
	movswl	-48(%rsp), %esi         # 2-byte Folded Reload
	movl	%edi, -8(%rsp)          # 4-byte Spill
	movl	%edi, %eax
	movl	%esi, -12(%rsp)         # 4-byte Spill
	imull	%esi, %eax
	movswl	-56(%rsp), %edi         # 2-byte Folded Reload
	movl	%ecx, (%rsp)            # 4-byte Spill
	movl	%ecx, %esi
	movl	%edi, -4(%rsp)          # 4-byte Spill
	imull	%edi, %esi
	movswl	-88(%rsp), %ecx         # 2-byte Folded Reload
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movl	%edx, %edi
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	imull	%ecx, %edi
	addl	%r8d, %eax
	movswl	-72(%rsp), %ecx         # 2-byte Folded Reload
	addl	%esi, %eax
	movl	%r14d, %edx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	imull	%ecx, %edx
	addl	%edi, %eax
	movswl	-120(%rsp), %ecx        # 2-byte Folded Reload
	addl	%edx, %eax
	movl	%r11d, %edx
	movl	%ecx, -80(%rsp)         # 4-byte Spill
	imull	%ecx, %edx
	addl	%edx, %eax
	movl	%ebx, %edx
	subl	-28(%r10,%rbp,4), %edx
	movw	%r13w, -112(%rsp)       # 2-byte Spill
	movswl	%r13w, %edi
	movl	%edx, %esi
	imull	%edi, %esi
	addl	%esi, %eax
	movl	(%r10,%rbp,4), %r12d
	subl	%ebx, %r12d
	movl	%ebx, %r13d
	subl	-32(%r10,%rbp,4), %r13d
	movl	%r9d, %esi
	movswl	%si, %r9d
	movl	%r13d, %ecx
	imull	%r9d, %ecx
	addl	%ecx, %eax
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movq	%r15, %rbp
	movl	%ecx, %r15d
	subl	%eax, %r15d
	movq	-104(%rsp), %r8         # 8-byte Reload
	movl	%r8d, %ecx
	sarl	%cl, %r15d
	subl	%r15d, %r12d
	movq	%rbp, %r15
	movl	-60(%rsp), %ecx         # 4-byte Reload
	shll	%cl, %r12d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r12d
	movl	%r12d, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%r12d, %ecx
	sarl	$31, %ecx
	orl	%eax, %ecx
	testl	%ecx, %ecx
	movl	%r12d, (%r15)
	jle	.LBB2_102
# BB#93:                                #   in Loop: Header=BB2_92 Depth=1
	movl	%r13d, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%r13d, %esi
	sarl	$31, %esi
	orl	%eax, %esi
	subl	%esi, %r9d
                                        # kill: %R9W<def> %R9W<kill> %R9D<kill>
	imull	%r13d, %esi
	movl	%r8d, %ecx
	sarl	%cl, %esi
	subl	%esi, %r12d
	jle	.LBB2_105
# BB#94:                                #   in Loop: Header=BB2_92 Depth=1
	movl	%edx, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%edx, %esi
	sarl	$31, %esi
	orl	%eax, %esi
	subl	%esi, %edi
	movl	%edi, %r13d
	imull	%edx, %esi
	movl	%r8d, %ecx
	sarl	%cl, %esi
	addl	%esi, %esi
	subl	%esi, %r12d
	jle	.LBB2_108
# BB#95:                                #   in Loop: Header=BB2_92 Depth=1
	movl	%r11d, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%r11d, %edx
	sarl	$31, %edx
	orl	%eax, %edx
	movl	-80(%rsp), %eax         # 4-byte Reload
	subl	%edx, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	movw	%ax, -120(%rsp)         # 2-byte Spill
	imull	%r11d, %edx
	movl	%r8d, %ecx
	sarl	%cl, %edx
	imull	$-3, %edx, %eax
	addl	%eax, %r12d
	testl	%r12d, %r12d
	jle	.LBB2_108
# BB#96:                                #   in Loop: Header=BB2_92 Depth=1
	movl	%r14d, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%r14d, %edx
	sarl	$31, %edx
	orl	%eax, %edx
	movl	16(%rsp), %eax          # 4-byte Reload
	subl	%edx, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	movw	%ax, -72(%rsp)          # 2-byte Spill
	imull	%r14d, %edx
	movl	%r8d, %ecx
	sarl	%cl, %edx
	shll	$2, %edx
	subl	%edx, %r12d
	movl	-124(%rsp), %edi        # 4-byte Reload
	movq	-96(%rsp), %r11         # 8-byte Reload
	jle	.LBB2_110
# BB#97:                                #   in Loop: Header=BB2_92 Depth=1
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	%esi, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%esi, %edx
	sarl	$31, %edx
	orl	%eax, %edx
	movl	4(%rsp), %eax           # 4-byte Reload
	subl	%edx, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	movw	%ax, -88(%rsp)          # 2-byte Spill
	imull	%esi, %edx
	movl	%r8d, %ecx
	sarl	%cl, %edx
	imull	$-5, %edx, %eax
	addl	%eax, %r12d
	testl	%r12d, %r12d
	jle	.LBB2_110
# BB#98:                                #   in Loop: Header=BB2_92 Depth=1
	movl	(%rsp), %esi            # 4-byte Reload
	movl	%esi, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%esi, %edx
	sarl	$31, %edx
	orl	%eax, %edx
	movl	-4(%rsp), %eax          # 4-byte Reload
	subl	%edx, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	movw	%ax, -56(%rsp)          # 2-byte Spill
	imull	%esi, %edx
	movl	%r8d, %ecx
	sarl	%cl, %edx
	imull	$-6, %edx, %eax
	addl	%eax, %r12d
	testl	%r12d, %r12d
	jle	.LBB2_110
# BB#99:                                #   in Loop: Header=BB2_92 Depth=1
	movl	-8(%rsp), %esi          # 4-byte Reload
	movl	%esi, %eax
	negl	%eax
	shrl	$31, %eax
	movl	%esi, %edx
	sarl	$31, %edx
	orl	%eax, %edx
	movl	-12(%rsp), %eax         # 4-byte Reload
	subl	%edx, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	movw	%ax, -48(%rsp)          # 2-byte Spill
	imull	%esi, %edx
	movl	%r8d, %ecx
	sarl	%cl, %edx
	imull	$-7, %edx, %eax
	addl	%eax, %r12d
	testl	%r12d, %r12d
	jle	.LBB2_110
# BB#100:                               #   in Loop: Header=BB2_92 Depth=1
	movl	-16(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, %eax
	negl	%eax
	shrl	$31, %eax
	sarl	$31, %ecx
	orl	%eax, %ecx
	movl	-20(%rsp), %eax         # 4-byte Reload
	subl	%ecx, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
.LBB2_101:                              #   in Loop: Header=BB2_92 Depth=1
	movw	%ax, -40(%rsp)          # 2-byte Spill
	jmp	.LBB2_110
	.p2align	4, 0x90
.LBB2_102:                              #   in Loop: Header=BB2_92 Depth=1
	js	.LBB2_104
# BB#103:                               #   in Loop: Header=BB2_92 Depth=1
	movl	-124(%rsp), %edi        # 4-byte Reload
	movzwl	-112(%rsp), %r13d       # 2-byte Folded Reload
	movl	%esi, %r9d
	jmp	.LBB2_109
.LBB2_104:                              #   in Loop: Header=BB2_92 Depth=1
	movl	%r13d, %eax
	negl	%eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	sarl	$31, %r13d
	orl	%ecx, %r13d
	addl	%r13d, %r9d
                                        # kill: %R9W<def> %R9W<kill> %R9D<kill>
	imull	%eax, %r13d
	movq	-104(%rsp), %rcx        # 8-byte Reload
	sarl	%cl, %r13d
	subl	%r13d, %r12d
	js	.LBB2_106
.LBB2_105:                              #   in Loop: Header=BB2_92 Depth=1
	movl	-124(%rsp), %edi        # 4-byte Reload
	movzwl	-112(%rsp), %r13d       # 2-byte Folded Reload
	jmp	.LBB2_109
.LBB2_106:                              #   in Loop: Header=BB2_92 Depth=1
	movl	%edx, %eax
	negl	%eax
	movl	%eax, %esi
	shrl	$31, %esi
	sarl	$31, %edx
	orl	%esi, %edx
	addl	%edx, %edi
	movl	%edi, %r13d
	imull	%eax, %edx
	sarl	%cl, %edx
	addl	%edx, %edx
	subl	%edx, %r12d
	jns	.LBB2_108
# BB#107:                               #   in Loop: Header=BB2_92 Depth=1
	movl	%r11d, %eax
	negl	%eax
	movl	%eax, %edx
	shrl	$31, %edx
	sarl	$31, %r11d
	orl	%edx, %r11d
	movl	-80(%rsp), %edx         # 4-byte Reload
	addl	%r11d, %edx
                                        # kill: %DX<def> %DX<kill> %EDX<kill>
	movw	%dx, -120(%rsp)         # 2-byte Spill
	imull	%eax, %r11d
	sarl	%cl, %r11d
	imull	$-3, %r11d, %eax
	addl	%eax, %r12d
	js	.LBB2_87
	.p2align	4, 0x90
.LBB2_108:                              #   in Loop: Header=BB2_92 Depth=1
	movl	-124(%rsp), %edi        # 4-byte Reload
.LBB2_109:                              #   in Loop: Header=BB2_92 Depth=1
	movq	-96(%rsp), %r11         # 8-byte Reload
.LBB2_110:                              #   in Loop: Header=BB2_92 Depth=1
	addq	$4, %r10
	addq	$4, %r15
	decl	%edi
	jne	.LBB2_92
.LBB2_111:                              # %._crit_edge566
	movzwl	-40(%rsp), %eax         # 2-byte Folded Reload
	movw	%ax, (%r11)
	movzwl	-48(%rsp), %eax         # 2-byte Folded Reload
	movw	%ax, 2(%r11)
	movzwl	-56(%rsp), %eax         # 2-byte Folded Reload
	movw	%ax, 4(%r11)
	movzwl	-88(%rsp), %eax         # 2-byte Folded Reload
	movw	%ax, 6(%r11)
	movzwl	-72(%rsp), %eax         # 2-byte Folded Reload
	movw	%ax, 8(%r11)
	movzwl	-120(%rsp), %eax        # 2-byte Folded Reload
	movw	%ax, 10(%r11)
	movw	%r13w, 12(%r11)
	movw	%r9w, 14(%r11)
.LBB2_112:                              # %.loopexit535
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	pc_block, .Lfunc_end2-pc_block
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
