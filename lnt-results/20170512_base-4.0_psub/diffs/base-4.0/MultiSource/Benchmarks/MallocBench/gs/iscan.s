	.text
	.file	"iscan.bc"
	.globl	dynamic_grow
	.p2align	4, 0x90
	.type	dynamic_grow,@function
dynamic_grow:                           # @dynamic_grow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	16(%rbx), %esi
	movl	20(%rbx), %ecx
	movl	%ecx, %eax
	imull	%esi, %eax
	movq	(%rbx), %r14
	movq	8(%rbx), %rbp
	cmpl	$10, %eax
	jae	.LBB0_2
# BB#1:
	movl	$20, %r15d
	jmp	.LBB0_3
.LBB0_2:
	movl	%eax, %edx
	addq	%rdx, %rdx
	cmpl	$2147483646, %eax       # imm = 0x7FFFFFFE
	movq	$-1, %r15
	cmovbeq	%rdx, %r15
.LBB0_3:
	xorl	%r12d, %r12d
	xorl	%edx, %edx
	movq	%r15, %rax
	divq	%rcx
	movq	%rax, %r13
	cmpl	$0, 24(%rbx)
	je	.LBB0_6
# BB#4:
	movl	$.L.str, %r8d
	movq	%r14, %rdi
	movl	%r13d, %edx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	alloc_grow
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.LBB0_9
# BB#5:
	movl	%r13d, 16(%rbx)
	addq	%rax, %r15
	movq	%r15, 32(%rbx)
	jmp	.LBB0_8
.LBB0_6:
	movl	%r13d, 16(%rbx)
	movl	$.L.str, %edx
	movl	%r13d, %edi
	movl	%ecx, %esi
	callq	alloc
	movq	%rax, (%rbx)
	movl	20(%rbx), %ecx
	imulq	%r13, %rcx
	addq	%rax, %rcx
	movq	%rcx, 32(%rbx)
	movl	$1, 24(%rbx)
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.LBB0_9
# BB#7:                                 # %.critedge
	movl	%r15d, %edx
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	memcpy
	movl	$1, 24(%rbx)
	movq	(%rbx), %rax
.LBB0_8:
	subq	%r14, %rbp
	movl	%ebp, %ecx
	addq	%rax, %rcx
	movq	%rcx, 8(%rbx)
	movl	$1, %r12d
.LBB0_9:
	movl	%r12d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	dynamic_grow, .Lfunc_end0-dynamic_grow
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.zero	16,100
.LCPI1_1:
	.byte	10                      # 0xa
	.byte	11                      # 0xb
	.byte	12                      # 0xc
	.byte	13                      # 0xd
	.byte	14                      # 0xe
	.byte	15                      # 0xf
	.byte	16                      # 0x10
	.byte	17                      # 0x11
	.byte	18                      # 0x12
	.byte	19                      # 0x13
	.byte	20                      # 0x14
	.byte	21                      # 0x15
	.byte	22                      # 0x16
	.byte	23                      # 0x17
	.byte	24                      # 0x18
	.byte	25                      # 0x19
	.text
	.globl	scan_init
	.p2align	4, 0x90
	.type	scan_init,@function
scan_init:                              # @scan_init
	.cfi_startproc
# BB#0:                                 # %.preheader30.preheader39
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movl	$.L.str.1, %edi
	movl	$1, %esi
	movl	$left_bracket, %edx
	xorl	%ecx, %ecx
	callq	name_ref
	movl	$.L.str.2, %edi
	movl	$1, %esi
	movl	$right_bracket, %edx
	xorl	%ecx, %ecx
	callq	name_ref
	movl	$.L.str.3, %edi
	xorl	%esi, %esi
	movl	$empty_name, %edx
	xorl	%ecx, %ecx
	callq	name_ref
	movb	$103, scan_char_array(%rip)
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100]
	movups	%xmm0, scan_char_array+241(%rip)
	movups	%xmm0, scan_char_array+226(%rip)
	movups	%xmm0, scan_char_array+210(%rip)
	movups	%xmm0, scan_char_array+194(%rip)
	movups	%xmm0, scan_char_array+178(%rip)
	movups	%xmm0, scan_char_array+162(%rip)
	movups	%xmm0, scan_char_array+146(%rip)
	movups	%xmm0, scan_char_array+130(%rip)
	movups	%xmm0, scan_char_array+114(%rip)
	movups	%xmm0, scan_char_array+98(%rip)
	movups	%xmm0, scan_char_array+82(%rip)
	movups	%xmm0, scan_char_array+66(%rip)
	movups	%xmm0, scan_char_array+50(%rip)
	movups	%xmm0, scan_char_array+34(%rip)
	movups	%xmm0, scan_char_array+18(%rip)
	movups	%xmm0, scan_char_array+2(%rip)
	movb	$101, scan_char_array+33(%rip)
	movb	$101, scan_char_array+27(%rip)
	movb	$101, scan_char_array+1(%rip)
	movb	$101, scan_char_array+14(%rip)
	movl	$1701143909, scan_char_array+10(%rip) # imm = 0x65656565
	movb	$102, scan_char_array+41(%rip)
	movb	$102, scan_char_array+42(%rip)
	movb	$102, scan_char_array+61(%rip)
	movb	$102, scan_char_array+63(%rip)
	movb	$102, scan_char_array+92(%rip)
	movb	$102, scan_char_array+94(%rip)
	movb	$102, scan_char_array+124(%rip)
	movb	$102, scan_char_array+126(%rip)
	movb	$102, scan_char_array+48(%rip)
	movb	$102, scan_char_array+38(%rip)
	movb	$0, scan_char_array+49(%rip)
	movb	$1, scan_char_array+50(%rip)
	movb	$2, scan_char_array+51(%rip)
	movb	$3, scan_char_array+52(%rip)
	movb	$4, scan_char_array+53(%rip)
	movb	$5, scan_char_array+54(%rip)
	movb	$6, scan_char_array+55(%rip)
	movb	$7, scan_char_array+56(%rip)
	movb	$8, scan_char_array+57(%rip)
	movb	$9, scan_char_array+58(%rip)
	movaps	.LCPI1_1(%rip), %xmm0   # xmm0 = [10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25]
	movups	%xmm0, scan_char_array+98(%rip)
	movups	%xmm0, scan_char_array+66(%rip)
	movb	$26, scan_char_array+114(%rip)
	movb	$26, scan_char_array+82(%rip)
	movb	$27, scan_char_array+115(%rip)
	movb	$27, scan_char_array+83(%rip)
	movb	$28, scan_char_array+116(%rip)
	movb	$28, scan_char_array+84(%rip)
	movb	$29, scan_char_array+117(%rip)
	movb	$29, scan_char_array+85(%rip)
	movb	$30, scan_char_array+118(%rip)
	movb	$30, scan_char_array+86(%rip)
	movb	$31, scan_char_array+119(%rip)
	movb	$31, scan_char_array+87(%rip)
	movb	$32, scan_char_array+120(%rip)
	movb	$32, scan_char_array+88(%rip)
	movb	$33, scan_char_array+121(%rip)
	movb	$33, scan_char_array+89(%rip)
	movb	$34, scan_char_array+122(%rip)
	movb	$34, scan_char_array+90(%rip)
	movb	$35, scan_char_array+123(%rip)
	movb	$35, scan_char_array+91(%rip)
	movl	$0, array_packing(%rip)
	popq	%rax
	retq
.Lfunc_end1:
	.size	scan_init, .Lfunc_end1-scan_init
	.cfi_endproc

	.globl	scan_token
	.p2align	4, 0x90
	.type	scan_token,@function
scan_token:                             # @scan_token
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 256
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	xorl	%r13d, %r13d
                                        # implicit-def: %R14D
                                        # implicit-def: %RAX
	movq	%rax, 48(%rsp)          # 8-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 20(%rsp)          # 4-byte Spill
                                        # implicit-def: %R15
                                        # implicit-def: %R9D
	xorl	%r12d, %r12d
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	jmp	.LBB2_123
.LBB2_1:                                #   in Loop: Header=BB2_123 Depth=1
	decq	%r12
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%r12, (%rax)
	movb	scan_char_array+1(%r13), %al
	xorl	%ebp, %ebp
	movq	%rbx, %r14
	cmpb	$101, %al
	jne	.LBB2_26
	jmp	.LBB2_28
.LBB2_53:                               #   in Loop: Header=BB2_123 Depth=1
	xorl	%esi, %esi
	movq	%rbp, %rdi
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rdx
	movl	%r12d, %r13d
	movq	%r15, %r12
	movq	%r9, %r15
	callq	scan_string
	jmp	.LBB2_57
.LBB2_54:                               #   in Loop: Header=BB2_123 Depth=1
	movq	(%rbp), %rax
	cmpq	8(%rbp), %rax
	movq	%r9, 32(%rsp)           # 8-byte Spill
	jae	.LBB2_70
# BB#55:                                #   in Loop: Header=BB2_123 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbp)
	movzbl	1(%rax), %eax
	jmp	.LBB2_71
.LBB2_70:                               #   in Loop: Header=BB2_123 Depth=1
	movq	%rbp, %rdi
	callq	*40(%rbp)
.LBB2_71:                               #   in Loop: Header=BB2_123 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	$1, %edx
	cmpl	$47, %eax
	jne	.LBB2_98
# BB#72:                                #   in Loop: Header=BB2_123 Depth=1
	movq	(%rbp), %rax
	cmpq	8(%rbp), %rax
	jae	.LBB2_96
# BB#73:                                #   in Loop: Header=BB2_123 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbp)
	movzbl	1(%rax), %eax
	jmp	.LBB2_97
.LBB2_96:                               #   in Loop: Header=BB2_123 Depth=1
	movq	%rbp, %rdi
	callq	*40(%rbp)
.LBB2_97:                               #   in Loop: Header=BB2_123 Depth=1
	movl	$2, %edx
.LBB2_98:                               #   in Loop: Header=BB2_123 Depth=1
	movslq	%eax, %rcx
	movb	scan_char_array+1(%rcx), %cl
	cmpb	$101, %cl
	je	.LBB2_103
# BB#99:                                #   in Loop: Header=BB2_123 Depth=1
	cmpb	$102, %cl
	je	.LBB2_106
# BB#100:                               #   in Loop: Header=BB2_123 Depth=1
	cmpb	$103, %cl
	jne	.LBB2_109
# BB#101:                               #   in Loop: Header=BB2_123 Depth=1
	movups	empty_name(%rip), %xmm0
	jmp	.LBB2_102
.LBB2_103:                              #   in Loop: Header=BB2_123 Depth=1
	movups	empty_name(%rip), %xmm0
	movups	%xmm0, (%rbx)
	cmpl	$13, %eax
	jne	.LBB2_42
# BB#104:                               #   in Loop: Header=BB2_123 Depth=1
	movq	(%rbp), %rax
	cmpq	8(%rbp), %rax
	jae	.LBB2_119
# BB#105:                               #   in Loop: Header=BB2_123 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbp)
	movzbl	1(%rax), %eax
	cmpl	$-1, %eax
	jne	.LBB2_120
	jmp	.LBB2_42
.LBB2_106:                              #   in Loop: Header=BB2_123 Depth=1
	cmpl	$93, %eax
	je	.LBB2_117
# BB#107:                               #   in Loop: Header=BB2_123 Depth=1
	cmpl	$91, %eax
	jne	.LBB2_118
# BB#108:                               #   in Loop: Header=BB2_123 Depth=1
	movups	left_bracket(%rip), %xmm0
	jmp	.LBB2_102
.LBB2_109:                              #   in Loop: Header=BB2_123 Depth=1
	movl	%edx, %ecx
	movq	32(%rsp), %r9           # 8-byte Reload
	jmp	.LBB2_52
.LBB2_117:                              #   in Loop: Header=BB2_123 Depth=1
	movups	right_bracket(%rip), %xmm0
.LBB2_102:                              #   in Loop: Header=BB2_123 Depth=1
	movups	%xmm0, (%rbx)
	testl	%r13d, %r13d
	jns	.LBB2_43
	jmp	.LBB2_133
.LBB2_118:                              #   in Loop: Header=BB2_123 Depth=1
	movups	empty_name(%rip), %xmm0
	movups	%xmm0, (%rbx)
	jmp	.LBB2_121
.LBB2_119:                              #   in Loop: Header=BB2_123 Depth=1
	movq	%rbp, %rdi
	movl	%edx, 16(%rsp)          # 4-byte Spill
	callq	*40(%rbp)
	movl	16(%rsp), %edx          # 4-byte Reload
	cmpl	$-1, %eax
	je	.LBB2_42
.LBB2_120:                              #   in Loop: Header=BB2_123 Depth=1
	cmpl	$10, %eax
	je	.LBB2_42
.LBB2_121:                              #   in Loop: Header=BB2_123 Depth=1
	decq	(%rbp)
	testl	%r13d, %r13d
	jns	.LBB2_43
	jmp	.LBB2_133
.LBB2_56:                               #   in Loop: Header=BB2_123 Depth=1
	movq	%rbp, %rdi
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rsi
	movl	%r12d, %r13d
	movq	%r15, %r12
	movq	%r9, %r15
	callq	scan_hex_string
.LBB2_57:                               #   in Loop: Header=BB2_123 Depth=1
	movq	%r15, %r9
	movq	%r12, %r15
	movl	%r13d, %r12d
	movl	%eax, %r13d
	testl	%r13d, %r13d
	jns	.LBB2_78
	jmp	.LBB2_135
.LBB2_58:                               #   in Loop: Header=BB2_123 Depth=1
	movups	left_bracket(%rip), %xmm0
	jmp	.LBB2_60
.LBB2_59:                               #   in Loop: Header=BB2_123 Depth=1
	movups	right_bracket(%rip), %xmm0
.LBB2_60:                               #   in Loop: Header=BB2_123 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	movups	%xmm0, (%rbx)
	orb	$1, 8(%rbx)
	testl	%r13d, %r13d
	jns	.LBB2_78
	jmp	.LBB2_135
.LBB2_61:                               #   in Loop: Header=BB2_123 Depth=1
	testl	%r12d, %r12d
	movq	48(%rsp), %rbx          # 8-byte Reload
	jne	.LBB2_63
# BB#62:                                #   in Loop: Header=BB2_123 Depth=1
	movq	osp(%rip), %rbx
	movq	ostop(%rip), %r14
	leaq	16(%r14), %r15
	subq	%rbx, %r14
	addq	$16, %rbx
	shrq	$4, %r14
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
.LBB2_63:                               #   in Loop: Header=BB2_123 Depth=1
	movq	%r15, %rax
	subq	8(%rsp), %rax           # 8-byte Folded Reload
	cmpq	$31, %rax
	ja	.LBB2_74
# BB#64:                                #   in Loop: Header=BB2_123 Depth=1
	movq	%r14, %rsi
	movl	%r14d, %eax
	shll	$4, %eax
	cmpl	$10, %eax
	jae	.LBB2_92
# BB#65:                                #   in Loop: Header=BB2_123 Depth=1
	movl	$20, %r14d
	jmp	.LBB2_93
.LBB2_74:                               #   in Loop: Header=BB2_123 Depth=1
                                        # kill: %R14D<def> %R14D<kill> %R14<kill> %R14<def>
	movq	8(%rsp), %rax           # 8-byte Reload
	jmp	.LBB2_113
.LBB2_92:                               #   in Loop: Header=BB2_123 Depth=1
	movl	%eax, %r14d
	addq	%r14, %r14
	cmpl	$2147483646, %eax       # imm = 0x7FFFFFFE
	movq	$-1, %rax
	cmovaq	%rax, %r14
.LBB2_93:                               #   in Loop: Header=BB2_123 Depth=1
	movq	%r14, %r15
	shrq	$4, %r14
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	movl	%r12d, 4(%rsp)          # 4-byte Spill
	movq	%r9, %r12
	je	.LBB2_110
# BB#94:                                #   in Loop: Header=BB2_123 Depth=1
	movl	$16, %ecx
	movl	$.L.str, %r8d
	movq	%rbx, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r14d, %edx
	callq	alloc_grow
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB2_131
# BB#95:                                #   in Loop: Header=BB2_123 Depth=1
	addq	%rbp, %r15
	jmp	.LBB2_112
.LBB2_110:                              #   in Loop: Header=BB2_123 Depth=1
	movl	$16, %esi
	movl	$.L.str, %edx
	movl	%r14d, %edi
	callq	alloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB2_131
# BB#111:                               # %.critedge.i
                                        #   in Loop: Header=BB2_123 Depth=1
	movq	%r14, %rax
	shlq	$4, %rax
	addq	%rbp, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%r15d, %edx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	memcpy
	movl	$1, 20(%rsp)            # 4-byte Folded Spill
	movq	40(%rsp), %r15          # 8-byte Reload
.LBB2_112:                              #   in Loop: Header=BB2_123 Depth=1
	movq	%r12, %r9
	movl	4(%rsp), %r12d          # 4-byte Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rbx, %rax
	movl	%eax, %eax
	addq	%rbp, %rax
	movq	%rbp, %rbx
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB2_113:                              #   in Loop: Header=BB2_123 Depth=1
	movw	%r12w, 10(%rax)
	addq	$16, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%eax, %r12d
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	subl	%ebx, %r12d
                                        # kill: %R14D<def> %R14D<kill> %R14<kill> %R14<def>
	jmp	.LBB2_123
.LBB2_66:                               #   in Loop: Header=BB2_123 Depth=1
	testl	%r12d, %r12d
	je	.LBB2_132
# BB#67:                                #   in Loop: Header=BB2_123 Depth=1
	movl	%r13d, 16(%rsp)         # 4-byte Spill
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r15, 56(%rsp)          # 8-byte Spill
	movslq	%r12d, %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax), %r15
	movq	8(%rsp), %r12           # 8-byte Reload
	subq	%r15, %r12
	movq	%r12, %rbx
	shrq	$4, %r12
	leaq	-16(%rcx,%rax), %r14
	movzwl	-6(%rcx,%rax), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	testl	%eax, %eax
	setne	%al
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB2_75
# BB#68:                                #   in Loop: Header=BB2_123 Depth=1
	testb	%al, %al
	jne	.LBB2_75
# BB#69:                                #   in Loop: Header=BB2_123 Depth=1
	movabsq	$68719476720, %rax      # imm = 0xFFFFFFFF0
	movq	%rbx, %rdx
	andq	%rax, %rdx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	memcpy
	movl	$16, %ecx
	movl	$.L.str.4, %r8d
	movq	%r14, %rdi
	movq	40(%rsp), %r14          # 8-byte Reload
	movl	%r14d, %esi
	movl	%r12d, %edx
	callq	alloc_shrink
	movq	%rax, %r13
	testq	%r13, %r13
	movq	72(%rsp), %rbx          # 8-byte Reload
	jne	.LBB2_77
	jmp	.LBB2_131
.LBB2_75:                               #   in Loop: Header=BB2_123 Depth=1
	movl	$16, %esi
	movl	$.L.str.5, %edx
	movl	%r12d, %edi
	callq	alloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB2_131
# BB#76:                                #   in Loop: Header=BB2_123 Depth=1
	movabsq	$68719476720, %rax      # imm = 0xFFFFFFFF0
	movq	%rbx, %rdx
	andq	%rax, %rdx
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	memcpy
	cmpw	$0, 4(%rsp)             # 2-byte Folded Reload
	cmoveq	72(%rsp), %r14          # 8-byte Folded Reload
	movq	%r14, %rbx
	movq	40(%rsp), %r14          # 8-byte Reload
.LBB2_77:                               #   in Loop: Header=BB2_123 Depth=1
	cmpl	$0, array_packing(%rip)
	movq	%r13, (%rbx)
	movw	$555, %ax               # imm = 0x22B
	movw	$771, %cx               # imm = 0x303
	cmovew	%cx, %ax
	movw	%ax, 8(%rbx)
	movw	%r12w, 10(%rbx)
	movl	4(%rsp), %r12d          # 4-byte Reload
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	16(%rsp), %r13d         # 4-byte Reload
	testl	%r13d, %r13d
	jns	.LBB2_78
	jmp	.LBB2_135
	.p2align	4, 0x90
.LBB2_123:                              # %.backedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_2 Depth 2
                                        #     Child Loop BB2_129 Depth 2
                                        #     Child Loop BB2_14 Depth 2
	movq	(%rbp), %rax
	cmpq	8(%rbp), %rax
	jae	.LBB2_125
# BB#124:                               #   in Loop: Header=BB2_123 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbp)
	movzbl	1(%rax), %eax
	jmp	.LBB2_126
	.p2align	4, 0x90
.LBB2_125:                              #   in Loop: Header=BB2_123 Depth=1
	movq	%rbp, %rdi
	movq	%r9, %rbx
	callq	*40(%rbp)
	movq	%rbx, %r9
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
.LBB2_126:                              #   in Loop: Header=BB2_123 Depth=1
	incl	%eax
	cmpl	$126, %eax
	ja	.LBB2_51
# BB#127:                               #   in Loop: Header=BB2_123 Depth=1
	movl	$1, %edx
	xorl	%ecx, %ecx
	movl	$-18, %esi
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_7:                                # %.critedge
                                        #   in Loop: Header=BB2_123 Depth=1
	testl	%r12d, %r12d
	movl	$1, %r13d
	movl	$-18, %eax
	cmovnel	%eax, %r13d
	movq	8(%rsp), %rbx           # 8-byte Reload
	testl	%r13d, %r13d
	jns	.LBB2_78
	jmp	.LBB2_135
	.p2align	4, 0x90
.LBB2_122:                              #   in Loop: Header=BB2_123 Depth=1
	addq	$16, %rbx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
                                        # kill: %R14D<def> %R14D<kill> %R14<kill> %R14<def>
	jmp	.LBB2_123
	.p2align	4, 0x90
.LBB2_2:                                # %.preheader
                                        #   Parent Loop BB2_123 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	cmpq	8(%rbp), %rax
	jae	.LBB2_4
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbp)
	movzbl	1(%rax), %eax
	jmp	.LBB2_5
	.p2align	4, 0x90
.LBB2_4:                                #   in Loop: Header=BB2_2 Depth=2
	movq	%rbp, %rdi
	movq	%r9, %rbx
	callq	*40(%rbp)
	movq	%rbx, %r9
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
.LBB2_5:                                #   in Loop: Header=BB2_2 Depth=2
	incl	%eax
	cmpl	$14, %eax
	ja	.LBB2_2
# BB#6:                                 #   in Loop: Header=BB2_2 Depth=2
	jmpq	*.LJTI2_1(,%rax,8)
.LBB2_90:                               #   in Loop: Header=BB2_123 Depth=1
	movq	(%rbp), %rax
	cmpq	8(%rbp), %rax
	jae	.LBB2_114
# BB#91:                                #   in Loop: Header=BB2_123 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbp)
	movzbl	1(%rax), %eax
	cmpl	$-1, %eax
	jne	.LBB2_115
	jmp	.LBB2_123
.LBB2_114:                              #   in Loop: Header=BB2_123 Depth=1
	movq	%rbp, %rdi
	movq	%r9, %rbx
	callq	*40(%rbp)
	movq	%rbx, %r9
	cmpl	$-1, %eax
	je	.LBB2_123
.LBB2_115:                              #   in Loop: Header=BB2_123 Depth=1
	cmpl	$10, %eax
	je	.LBB2_123
# BB#116:                               #   in Loop: Header=BB2_123 Depth=1
	decq	(%rbp)
	jmp	.LBB2_123
.LBB2_8:                                #   in Loop: Header=BB2_123 Depth=1
	leaq	-1(%r12), %rax
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rax, (%rbp)
	subq	%rbx, %r12
	cmpl	$10, %r12d
	jae	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_123 Depth=1
	movl	$20, %edi
	jmp	.LBB2_11
.LBB2_10:                               #   in Loop: Header=BB2_123 Depth=1
	leaq	(%r12,%r12), %rdi
	movabsq	$8589934590, %rax       # imm = 0x1FFFFFFFE
	andq	%rax, %rdi
	cmpl	$2147483646, %r12d      # imm = 0x7FFFFFFE
	movq	$-1, %rax
	cmovaq	%rax, %rdi
.LBB2_11:                               #   in Loop: Header=BB2_123 Depth=1
	movl	$1, %esi
	movl	$.L.str, %edx
	movq	%rdi, %r13
	callq	alloc
	movq	%rax, %r14
	movl	$-25, %esi
	testq	%r14, %r14
	je	.LBB2_132
# BB#12:                                #   in Loop: Header=BB2_123 Depth=1
	movq	%r15, 56(%rsp)          # 8-byte Spill
	leaq	(%r14,%r13), %r15
	movl	%r13d, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	memcpy
	movl	%r12d, %r12d
	addq	%r14, %r12
	movq	%r13, %r9
	jmp	.LBB2_14
	.p2align	4, 0x90
.LBB2_13:                               #   in Loop: Header=BB2_14 Depth=2
	movb	%r13b, (%r12)
	incq	%r12
	movl	%ebp, %r9d
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB2_14:                               #   Parent Loop BB2_123 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	cmpq	8(%rbp), %rax
	jae	.LBB2_16
# BB#15:                                #   in Loop: Header=BB2_14 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbp)
	movzbl	1(%rax), %r13d
	jmp	.LBB2_17
	.p2align	4, 0x90
.LBB2_16:                               #   in Loop: Header=BB2_14 Depth=2
	movq	%rbp, %rdi
	movq	%r9, %rbx
	callq	*40(%rbp)
	movq	%rbx, %r9
	movl	%eax, %r13d
.LBB2_17:                               #   in Loop: Header=BB2_14 Depth=2
	movslq	%r13d, %rax
	movzbl	scan_char_array+1(%rax), %eax
	cmpb	$100, %al
	ja	.LBB2_25
# BB#18:                                #   in Loop: Header=BB2_14 Depth=2
	cmpq	%r15, %r12
	je	.LBB2_20
# BB#19:                                #   in Loop: Header=BB2_14 Depth=2
	movl	%r9d, %ebp
	jmp	.LBB2_13
	.p2align	4, 0x90
.LBB2_20:                               #   in Loop: Header=BB2_14 Depth=2
	cmpl	$10, %r9d
	jae	.LBB2_22
# BB#21:                                #   in Loop: Header=BB2_14 Depth=2
	movl	$20, %ebp
	jmp	.LBB2_23
.LBB2_22:                               #   in Loop: Header=BB2_14 Depth=2
	movl	%r9d, %ebp
	addq	%rbp, %rbp
	cmpl	$2147483646, %r9d       # imm = 0x7FFFFFFE
	movq	$-1, %rax
	cmovaq	%rax, %rbp
.LBB2_23:                               #   in Loop: Header=BB2_14 Depth=2
	movl	$1, %ecx
	movl	$.L.str, %r8d
	movq	%r14, %rdi
	movl	%r9d, %esi
	movl	%ebp, %edx
	callq	alloc_grow
	testq	%rax, %rax
	je	.LBB2_131
# BB#24:                                #   in Loop: Header=BB2_14 Depth=2
	subq	%r14, %r15
	movl	%r15d, %r12d
	movq	%rbp, %r15
	addq	%rax, %r15
	addq	%rax, %r12
	movq	%rax, %r14
	jmp	.LBB2_13
.LBB2_25:                               #   in Loop: Header=BB2_123 Depth=1
	movl	$1, %ebp
	movq	56(%rsp), %r15          # 8-byte Reload
	cmpb	$101, %al
	je	.LBB2_28
.LBB2_26:                               # %.loopexit
                                        #   in Loop: Header=BB2_123 Depth=1
	cmpb	$102, %al
	jne	.LBB2_34
# BB#27:                                #   in Loop: Header=BB2_123 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	decq	(%rax)
.LBB2_28:                               #   in Loop: Header=BB2_123 Depth=1
	cmpl	$13, %r13d
	jne	.LBB2_34
# BB#29:                                #   in Loop: Header=BB2_123 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	cmpq	8(%rdi), %rax
	jae	.LBB2_31
# BB#30:                                #   in Loop: Header=BB2_123 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rdi)
	movzbl	1(%rax), %eax
	cmpl	$-1, %eax
	jne	.LBB2_32
	jmp	.LBB2_34
.LBB2_31:                               #   in Loop: Header=BB2_123 Depth=1
	movq	%r9, %rbx
	callq	*40(%rdi)
	movq	%rbx, %r9
	cmpl	$-1, %eax
	je	.LBB2_34
.LBB2_32:                               #   in Loop: Header=BB2_123 Depth=1
	cmpl	$10, %eax
	je	.LBB2_34
# BB#33:                                #   in Loop: Header=BB2_123 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	decq	(%rax)
	.p2align	4, 0x90
.LBB2_34:                               #   in Loop: Header=BB2_123 Depth=1
	cmpl	$0, 68(%rsp)            # 4-byte Folded Reload
	movq	%r9, 32(%rsp)           # 8-byte Spill
	je	.LBB2_37
# BB#35:                                #   in Loop: Header=BB2_123 Depth=1
	subl	%r14d, %r12d
	leaq	80(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%r12d, %edx
	callq	sread_string
	movq	%rbx, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	scan_number
	movl	%eax, %r13d
	cmpl	$-18, %eax
	jne	.LBB2_39
# BB#36:                                #   in Loop: Header=BB2_123 Depth=1
	movl	%ebp, %ebx
	jmp	.LBB2_38
.LBB2_37:                               # %._crit_edge
                                        #   in Loop: Header=BB2_123 Depth=1
	movl	%ebp, %ebx
	subl	%r14d, %r12d
.LBB2_38:                               #   in Loop: Header=BB2_123 Depth=1
	movq	24(%rsp), %rbp          # 8-byte Reload
	movl	$1, %ecx
	movq	%r14, %rdi
	movl	%r12d, %esi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	name_ref
	movl	%eax, %r13d
	testl	%ebx, %ebx
	jne	.LBB2_40
	jmp	.LBB2_41
.LBB2_39:                               #   in Loop: Header=BB2_123 Depth=1
	testl	%ebp, %ebp
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB2_41
.LBB2_40:                               #   in Loop: Header=BB2_123 Depth=1
	movl	$1, %edx
	movl	$.L.str, %ecx
	movq	%r14, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	alloc_free
.LBB2_41:                               # %.thread293
                                        #   in Loop: Header=BB2_123 Depth=1
	movl	4(%rsp), %r12d          # 4-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
.LBB2_42:                               # %.thread293
                                        #   in Loop: Header=BB2_123 Depth=1
	testl	%r13d, %r13d
	js	.LBB2_133
.LBB2_43:                               #   in Loop: Header=BB2_123 Depth=1
	cmpl	$2, %edx
	je	.LBB2_47
# BB#44:                                #   in Loop: Header=BB2_123 Depth=1
	testl	%edx, %edx
	jne	.LBB2_49
# BB#45:                                #   in Loop: Header=BB2_123 Depth=1
	movzwl	8(%rbx), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	cmpl	$28, %ecx
	jne	.LBB2_49
# BB#46:                                #   in Loop: Header=BB2_123 Depth=1
	orl	$1, %eax
	movw	%ax, 8(%rbx)
	jmp	.LBB2_49
.LBB2_47:                               #   in Loop: Header=BB2_123 Depth=1
	movq	dsp(%rip), %rsi
	movl	$dstack, %edi
	movq	%rbx, %rdx
	leaq	80(%rsp), %rcx
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB2_137
# BB#48:                                # %.thread294
                                        #   in Loop: Header=BB2_123 Depth=1
	movq	80(%rsp), %rax
	movups	(%rax), %xmm0
	movups	%xmm0, (%rbx)
.LBB2_49:                               #   in Loop: Header=BB2_123 Depth=1
	movq	32(%rsp), %r9           # 8-byte Reload
	testl	%r13d, %r13d
	js	.LBB2_135
.LBB2_78:                               #   in Loop: Header=BB2_123 Depth=1
	testl	%r12d, %r12d
	je	.LBB2_135
# BB#79:                                #   in Loop: Header=BB2_123 Depth=1
	movq	%r15, %rax
	subq	%rbx, %rax
	cmpq	$31, %rax
	ja	.LBB2_82
# BB#80:                                #   in Loop: Header=BB2_123 Depth=1
	movl	%r12d, 4(%rsp)          # 4-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movl	%r14d, %eax
	shll	$4, %eax
	movq	%r14, %rsi
	cmpl	$10, %eax
	jae	.LBB2_83
# BB#81:                                #   in Loop: Header=BB2_123 Depth=1
	movl	$20, %ecx
	jmp	.LBB2_84
.LBB2_82:                               #   in Loop: Header=BB2_123 Depth=1
                                        # kill: %R14D<def> %R14D<kill> %R14<kill> %R14<def>
	jmp	.LBB2_122
.LBB2_83:                               #   in Loop: Header=BB2_123 Depth=1
	movl	%eax, %ecx
	addq	%rcx, %rcx
	cmpl	$2147483646, %eax       # imm = 0x7FFFFFFE
	movq	$-1, %rax
	cmovaq	%rax, %rcx
.LBB2_84:                               #   in Loop: Header=BB2_123 Depth=1
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rcx, %r14
	shrq	$4, %r14
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	movl	%r13d, 16(%rsp)         # 4-byte Spill
	je	.LBB2_87
# BB#85:                                #   in Loop: Header=BB2_123 Depth=1
	movq	%rcx, %r15
	movq	%r9, %r13
	movl	$16, %ecx
	movl	$.L.str, %r8d
	movq	%rbx, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r14d, %edx
	callq	alloc_grow
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB2_131
# BB#86:                                #   in Loop: Header=BB2_123 Depth=1
	addq	%rbp, %r15
	jmp	.LBB2_89
.LBB2_87:                               #   in Loop: Header=BB2_123 Depth=1
	movq	%rbx, %r12
	movq	%rcx, %rbx
	movq	%r9, %r13
	movl	$16, %esi
	movl	$.L.str, %edx
	movl	%r14d, %edi
	callq	alloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB2_131
# BB#88:                                # %.critedge.i235
                                        #   in Loop: Header=BB2_123 Depth=1
	movq	%rbx, %rax
	movq	%r14, %rbx
	shlq	$4, %rbx
	addq	%rbp, %rbx
	movl	%eax, %edx
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	memcpy
	movl	$1, 20(%rsp)            # 4-byte Folded Spill
	movq	%rbx, %r15
	movq	%r12, %rbx
.LBB2_89:                               #   in Loop: Header=BB2_123 Depth=1
	movq	8(%rsp), %r12           # 8-byte Reload
	subq	%rbx, %r12
	movq	%r12, %rbx
	movl	%ebx, %ebx
	addq	%rbp, %rbx
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	%r13, %r9
	movl	16(%rsp), %r13d         # 4-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	movl	4(%rsp), %r12d          # 4-byte Reload
	jmp	.LBB2_122
.LBB2_51:                               #   in Loop: Header=BB2_123 Depth=1
	xorl	%ecx, %ecx
.LBB2_52:                               #   in Loop: Header=BB2_123 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_128:                              #   in Loop: Header=BB2_123 Depth=1
	movl	%edx, 68(%rsp)          # 4-byte Spill
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movl	%r12d, 4(%rsp)          # 4-byte Spill
	movq	(%rbp), %rbx
	movq	8(%rbp), %rax
	leaq	1(%rbx), %r12
	movq	%r14, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_129:                              #   Parent Loop BB2_123 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rax, %r12
	jae	.LBB2_8
# BB#130:                               #   in Loop: Header=BB2_129 Depth=2
	movzbl	(%r12), %r13d
	incq	%r12
	cmpb	$101, scan_char_array+1(%r13)
	jb	.LBB2_129
	jmp	.LBB2_1
.LBB2_131:
	movl	$-25, %esi
	jmp	.LBB2_132
.LBB2_135:
	movl	%r13d, %esi
	jmp	.LBB2_132
.LBB2_133:
	movl	%r13d, %esi
	jmp	.LBB2_132
.LBB2_137:
	movl	$-21, %esi
.LBB2_132:                              # %dynamic_grow.exit.thread
	movl	%esi, %eax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	scan_token, .Lfunc_end2-scan_token
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_7
	.quad	.LBB2_123
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_123
	.quad	.LBB2_123
	.quad	.LBB2_123
	.quad	.LBB2_123
	.quad	.LBB2_123
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_123
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_123
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_2
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_53
	.quad	.LBB2_132
	.quad	.LBB2_51
	.quad	.LBB2_128
	.quad	.LBB2_51
	.quad	.LBB2_128
	.quad	.LBB2_128
	.quad	.LBB2_54
	.quad	.LBB2_128
	.quad	.LBB2_128
	.quad	.LBB2_128
	.quad	.LBB2_128
	.quad	.LBB2_128
	.quad	.LBB2_128
	.quad	.LBB2_128
	.quad	.LBB2_128
	.quad	.LBB2_128
	.quad	.LBB2_128
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_56
	.quad	.LBB2_51
	.quad	.LBB2_132
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_58
	.quad	.LBB2_51
	.quad	.LBB2_59
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_51
	.quad	.LBB2_61
	.quad	.LBB2_51
	.quad	.LBB2_66
.LJTI2_1:
	.quad	.LBB2_7
	.quad	.LBB2_2
	.quad	.LBB2_2
	.quad	.LBB2_2
	.quad	.LBB2_2
	.quad	.LBB2_2
	.quad	.LBB2_2
	.quad	.LBB2_2
	.quad	.LBB2_2
	.quad	.LBB2_2
	.quad	.LBB2_2
	.quad	.LBB2_123
	.quad	.LBB2_2
	.quad	.LBB2_123
	.quad	.LBB2_90

	.text
	.globl	scan_hex_string
	.p2align	4, 0x90
	.type	scan_hex_string,@function
scan_hex_string:                        # @scan_hex_string
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 80
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movl	$100, %edi
	movl	$1, %esi
	movl	$.L.str, %edx
	callq	alloc
	movl	$-25, %r12d
	testq	%rax, %rax
	je	.LBB3_1
# BB#2:                                 # %.preheader65
	movl	$100, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rax, %rbp
	addq	$100, %rbp
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, %r13
	jmp	.LBB3_3
	.p2align	4, 0x90
.LBB3_19:                               #   in Loop: Header=BB3_7 Depth=2
	cmpb	$101, %r14b
	je	.LBB3_7
	jmp	.LBB3_20
.LBB3_11:                               #   in Loop: Header=BB3_3 Depth=1
	cmpq	%rbp, %r13
	je	.LBB3_13
# BB#12:                                #   in Loop: Header=BB3_3 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, %edx
	jmp	.LBB3_18
.LBB3_13:                               #   in Loop: Header=BB3_3 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	cmpl	$10, %eax
	jae	.LBB3_15
# BB#14:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$20, %edx
	jmp	.LBB3_16
.LBB3_15:                               #   in Loop: Header=BB3_3 Depth=1
	movl	%eax, %edx
	addq	%rdx, %rdx
	cmpl	$2147483646, %eax       # imm = 0x7FFFFFFE
	movq	$-1, %rcx
	cmovaq	%rcx, %rdx
.LBB3_16:                               #   in Loop: Header=BB3_3 Depth=1
	movq	%rax, %rsi
	movq	8(%rsp), %r13           # 8-byte Reload
	movl	$1, %ecx
	movl	$.L.str, %r8d
	movq	%r13, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%rdx, (%rsp)            # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	alloc_grow
	testq	%rax, %rax
	je	.LBB3_33
# BB#17:                                #   in Loop: Header=BB3_3 Depth=1
	subq	%r13, %rbp
	movl	%ebp, %r13d
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	%rdx, %rbp
	addq	%rax, %rbp
	addq	%rax, %r13
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB3_18:                               #   in Loop: Header=BB3_3 Depth=1
	shlb	$4, %r15b
	addb	%r15b, %r14b
	movb	%r14b, (%r13)
	incq	%r13
	movl	%edx, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB3_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_7 Depth 2
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB3_5
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB3_6
	.p2align	4, 0x90
.LBB3_5:                                #   in Loop: Header=BB3_3 Depth=1
	movq	%rbx, %rdi
	callq	*40(%rbx)
.LBB3_6:                                #   in Loop: Header=BB3_3 Depth=1
	movslq	%eax, %rcx
	movb	scan_char_array+1(%rcx), %r15b
	cmpb	$15, %r15b
	jbe	.LBB3_7
# BB#29:                                #   in Loop: Header=BB3_3 Depth=1
	cmpb	$101, %r15b
	je	.LBB3_3
	jmp	.LBB3_30
	.p2align	4, 0x90
.LBB3_7:                                # %.preheader
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB3_9
# BB#8:                                 #   in Loop: Header=BB3_7 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB3_10
	.p2align	4, 0x90
.LBB3_9:                                #   in Loop: Header=BB3_7 Depth=2
	movq	%rbx, %rdi
	callq	*40(%rbx)
.LBB3_10:                               #   in Loop: Header=BB3_7 Depth=2
	movslq	%eax, %rcx
	movzbl	scan_char_array+1(%rcx), %r14d
	cmpb	$15, %r14b
	ja	.LBB3_19
	jmp	.LBB3_11
.LBB3_30:
	movl	$-18, %r12d
	cmpl	$62, %eax
	je	.LBB3_31
	jmp	.LBB3_33
.LBB3_1:
	movl	$-25, %r12d
	jmp	.LBB3_33
.LBB3_20:
	movl	$-18, %r12d
	cmpl	$62, %eax
	jne	.LBB3_33
# BB#21:
	cmpq	%rbp, %r13
	je	.LBB3_23
# BB#22:
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, %ebx
	jmp	.LBB3_28
.LBB3_23:
	movl	$20, %ebx
	movq	(%rsp), %rsi            # 8-byte Reload
	cmpl	$10, %esi
	jb	.LBB3_25
# BB#24:
	leal	(%rsi,%rsi), %eax
	cmpl	$2147483646, %esi       # imm = 0x7FFFFFFE
	movl	$-1, %ebx
	cmovbel	%eax, %ebx
.LBB3_25:
	movl	$1, %ecx
	movl	$.L.str, %r8d
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	%r14, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%ebx, %edx
	callq	alloc_grow
	testq	%rax, %rax
	je	.LBB3_26
# BB#27:
	subq	%r14, %rbp
	movl	%ebp, %r13d
	addq	%rax, %r13
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB3_28:
	shlb	$4, %r15b
	movb	%r15b, (%r13)
	incq	%r13
	movl	%ebx, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB3_31:
	movq	8(%rsp), %rdi           # 8-byte Reload
	subq	%rdi, %r13
	movl	$1, %ecx
	movl	$.L.str.6, %r8d
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r13d, %edx
	callq	alloc_shrink
	testq	%rax, %rax
	movl	$-25, %r12d
	je	.LBB3_33
# BB#32:
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rax, (%rbp)
	movw	$822, 8(%rbp)           # imm = 0x336
	movw	%r13w, 10(%rbp)
	xorl	%r12d, %r12d
.LBB3_33:                               # %mk_string.exit
	movl	%r12d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_26:
	movl	$-25, %r12d
	jmp	.LBB3_33
.Lfunc_end3:
	.size	scan_hex_string, .Lfunc_end3-scan_hex_string
	.cfi_endproc

	.globl	scan_string
	.p2align	4, 0x90
	.type	scan_string,@function
scan_string:                            # @scan_string
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 96
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, 20(%rsp)          # 4-byte Spill
	movq	%rdi, %r12
	movl	$100, %r13d
	movl	$100, %edi
	movl	$1, %esi
	movl	$.L.str, %edx
	callq	alloc
	testq	%rax, %rax
	je	.LBB4_43
# BB#1:                                 # %.preheader
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%rax, %rcx
	addq	$100, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	xorl	%r15d, %r15d
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rax, %rbp
	jmp	.LBB4_3
	.p2align	4, 0x90
.LBB4_2:                                #   in Loop: Header=BB4_3 Depth=1
	movb	%r14b, (%rbp)
	incq	%rbp
	movl	%ebx, %r13d
.LBB4_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	cmpq	8(%r12), %rax
	jae	.LBB4_5
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r12)
	movzbl	1(%rax), %r14d
	cmpl	$40, %r14d
	jne	.LBB4_6
	jmp	.LBB4_12
	.p2align	4, 0x90
.LBB4_5:                                #   in Loop: Header=BB4_3 Depth=1
	movq	%r12, %rdi
	callq	*40(%r12)
	movl	%eax, %r14d
	cmpl	$40, %r14d
	je	.LBB4_12
.LBB4_6:                                #   in Loop: Header=BB4_3 Depth=1
	cmpl	$41, %r14d
	je	.LBB4_13
# BB#7:                                 #   in Loop: Header=BB4_3 Depth=1
	movl	$-18, %ebx
	cmpl	$-1, %r14d
	je	.LBB4_46
# BB#8:                                 #   in Loop: Header=BB4_3 Depth=1
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	jne	.LBB4_15
# BB#9:                                 #   in Loop: Header=BB4_3 Depth=1
	cmpl	$92, %r14d
	jne	.LBB4_15
# BB#10:                                #   in Loop: Header=BB4_3 Depth=1
	movq	(%r12), %rax
	cmpq	8(%r12), %rax
	jae	.LBB4_17
# BB#11:                                #   in Loop: Header=BB4_3 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r12)
	movzbl	1(%rax), %r14d
	jmp	.LBB4_18
	.p2align	4, 0x90
.LBB4_12:                               #   in Loop: Header=BB4_3 Depth=1
	incl	%r15d
	movl	$40, %r14d
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jne	.LBB4_16
	jmp	.LBB4_32
	.p2align	4, 0x90
.LBB4_13:                               #   in Loop: Header=BB4_3 Depth=1
	testl	%r15d, %r15d
	jle	.LBB4_44
# BB#14:                                #   in Loop: Header=BB4_3 Depth=1
	decl	%r15d
	movl	$41, %r14d
.LBB4_15:                               # %.thread
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	je	.LBB4_32
.LBB4_16:                               #   in Loop: Header=BB4_3 Depth=1
	movl	%r13d, %ebx
	jmp	.LBB4_2
.LBB4_17:                               #   in Loop: Header=BB4_3 Depth=1
	movq	%r12, %rdi
	callq	*40(%r12)
	movl	%eax, %r14d
.LBB4_18:                               #   in Loop: Header=BB4_3 Depth=1
	leal	-10(%r14), %eax
	cmpl	$106, %eax
	ja	.LBB4_15
# BB#19:                                #   in Loop: Header=BB4_3 Depth=1
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_20:                               #   in Loop: Header=BB4_3 Depth=1
	movq	(%r12), %rax
	cmpq	8(%r12), %rax
	jae	.LBB4_22
# BB#21:                                #   in Loop: Header=BB4_3 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r12)
	movzbl	1(%rax), %edx
	jmp	.LBB4_23
.LBB4_22:                               #   in Loop: Header=BB4_3 Depth=1
	movq	%r12, %rdi
	callq	*40(%r12)
	movl	%eax, %edx
.LBB4_23:                               #   in Loop: Header=BB4_3 Depth=1
	addl	$-48, %r14d
	movl	%edx, %eax
	andl	$-8, %eax
	cmpl	$48, %eax
	jne	.LBB4_30
# BB#24:                                #   in Loop: Header=BB4_3 Depth=1
	movq	(%r12), %rax
	cmpq	8(%r12), %rax
	jae	.LBB4_26
# BB#25:                                #   in Loop: Header=BB4_3 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%r12)
	movzbl	1(%rax), %eax
	jmp	.LBB4_27
.LBB4_26:                               #   in Loop: Header=BB4_3 Depth=1
	movq	%r12, %rdi
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	callq	*40(%r12)
	movq	24(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
.LBB4_27:                               #   in Loop: Header=BB4_3 Depth=1
	leal	-48(%rdx,%r14,8), %r14d
	movl	%eax, %ecx
	andl	$-8, %ecx
	cmpl	$48, %ecx
	jne	.LBB4_29
# BB#28:                                #   in Loop: Header=BB4_3 Depth=1
	leal	-48(%rax,%r14,8), %r14d
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jne	.LBB4_16
	jmp	.LBB4_32
.LBB4_29:                               #   in Loop: Header=BB4_3 Depth=1
	movl	%eax, %edx
.LBB4_30:                               #   in Loop: Header=BB4_3 Depth=1
	cmpl	$-1, %edx
	je	.LBB4_46
# BB#31:                                #   in Loop: Header=BB4_3 Depth=1
	decq	(%r12)
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jne	.LBB4_16
	.p2align	4, 0x90
.LBB4_32:                               #   in Loop: Header=BB4_3 Depth=1
	cmpl	$10, %r13d
	jae	.LBB4_34
# BB#33:                                #   in Loop: Header=BB4_3 Depth=1
	movl	$20, %ebx
	jmp	.LBB4_35
.LBB4_34:                               #   in Loop: Header=BB4_3 Depth=1
	movl	%r13d, %ebx
	addq	%rbx, %rbx
	cmpl	$2147483646, %r13d      # imm = 0x7FFFFFFE
	movq	$-1, %rax
	cmovaq	%rax, %rbx
.LBB4_35:                               #   in Loop: Header=BB4_3 Depth=1
	movq	(%rsp), %rbp            # 8-byte Reload
	movl	$1, %ecx
	movl	$.L.str, %r8d
	movq	%rbp, %rdi
	movl	%r13d, %esi
	movl	%ebx, %edx
	callq	alloc_grow
	testq	%rax, %rax
	je	.LBB4_43
# BB#36:                                #   in Loop: Header=BB4_3 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	subq	%rbp, %rcx
	movl	%ecx, %ebp
	movq	%rbx, %rcx
	addq	%rax, %rcx
	addq	%rax, %rbp
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rax, (%rsp)            # 8-byte Spill
	jmp	.LBB4_2
.LBB4_37:                               #   in Loop: Header=BB4_3 Depth=1
	movl	$8, %r14d
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jne	.LBB4_16
	jmp	.LBB4_32
.LBB4_38:                               #   in Loop: Header=BB4_3 Depth=1
	movl	$12, %r14d
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jne	.LBB4_16
	jmp	.LBB4_32
.LBB4_39:                               #   in Loop: Header=BB4_3 Depth=1
	movl	$10, %r14d
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jne	.LBB4_16
	jmp	.LBB4_32
.LBB4_40:                               #   in Loop: Header=BB4_3 Depth=1
	movl	$13, %r14d
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jne	.LBB4_16
	jmp	.LBB4_32
.LBB4_41:                               #   in Loop: Header=BB4_3 Depth=1
	movl	$9, %r14d
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jne	.LBB4_16
	jmp	.LBB4_32
.LBB4_43:
	movl	$-25, %ebx
.LBB4_46:                               # %mk_string.exit
	movl	%ebx, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_44:
	movq	(%rsp), %rdi            # 8-byte Reload
	subq	%rdi, %rbp
	movl	$1, %ecx
	movl	$.L.str.6, %r8d
	movl	%r13d, %esi
	movl	%ebp, %edx
	callq	alloc_shrink
	testq	%rax, %rax
	movl	$-25, %ebx
	je	.LBB4_46
# BB#45:
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	movw	$822, 8(%rcx)           # imm = 0x336
	movw	%bp, 10(%rcx)
	xorl	%ebx, %ebx
	jmp	.LBB4_46
.Lfunc_end4:
	.size	scan_string, .Lfunc_end4-scan_string
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_3
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_20
	.quad	.LBB4_20
	.quad	.LBB4_20
	.quad	.LBB4_20
	.quad	.LBB4_20
	.quad	.LBB4_20
	.quad	.LBB4_20
	.quad	.LBB4_20
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_37
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_38
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_39
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_15
	.quad	.LBB4_40
	.quad	.LBB4_15
	.quad	.LBB4_41

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4621819117588971520     # double 10
.LCPI5_2:
	.quad	4696837146684686336     # double 1.0E+6
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_1:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	scan_number
	.p2align	4, 0x90
	.type	scan_number,@function
scan_number:                            # @scan_number
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi59:
	.cfi_def_cfa_offset 96
.Lcfi60:
	.cfi_offset %rbx, -56
.Lcfi61:
	.cfi_offset %r12, -48
.Lcfi62:
	.cfi_offset %r13, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB5_2
# BB#1:
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %ebp
	cmpl	$45, %ebp
	jne	.LBB5_4
	jmp	.LBB5_8
.LBB5_2:
	movq	%rbx, %rdi
	callq	*40(%rbx)
	movl	%eax, %ebp
	cmpl	$45, %ebp
	je	.LBB5_8
.LBB5_4:
	xorl	%r12d, %r12d
	cmpl	$43, %ebp
	jne	.LBB5_12
# BB#5:
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB5_7
# BB#6:
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %ebp
	movl	$1, %r12d
	jmp	.LBB5_12
.LBB5_8:
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB5_10
# BB#9:
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %ebp
	jmp	.LBB5_11
.LBB5_7:
	movq	%rbx, %rdi
	callq	*40(%rbx)
	movl	%eax, %ebp
	movl	$1, %r12d
	jmp	.LBB5_12
.LBB5_10:
	movq	%rbx, %rdi
	callq	*40(%rbx)
	movl	%eax, %ebp
.LBB5_11:
	movl	$-1, %r12d
.LBB5_12:
	callq	__ctype_b_loc
	movq	%rax, %r14
	movq	(%r14), %rax
	movslq	%ebp, %rcx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB5_23
# BB#13:
	movl	$-18, %r13d
	cmpl	$46, %ebp
	jne	.LBB5_136
# BB#14:
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movl	%r12d, 4(%rsp)          # 4-byte Spill
	jae	.LBB5_16
# BB#15:
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB5_17
.LBB5_23:
	movq	(%rbx), %rax
	decq	%rax
	movq	%rax, (%rbx)
	xorl	%ebp, %ebp
	cmpq	8(%rbx), %rax
	jae	.LBB5_26
	jmp	.LBB5_25
	.p2align	4, 0x90
.LBB5_35:
	leal	(%rbp,%rbp,4), %eax
	leal	(%rcx,%rax,2), %ebp
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB5_26
.LBB5_25:
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB5_27
	.p2align	4, 0x90
.LBB5_26:
	movq	%rbx, %rdi
	callq	*40(%rbx)
.LBB5_27:
	movslq	%eax, %rcx
	movzbl	scan_char_array+1(%rcx), %ecx
	cmpl	$10, %ecx
	jae	.LBB5_28
# BB#32:
	cmpl	$214748364, %ebp        # imm = 0xCCCCCCC
	jl	.LBB5_35
# BB#33:
	jne	.LBB5_36
# BB#34:
	cmpb	$7, %cl
	jbe	.LBB5_35
.LBB5_36:
	cvtsi2sdl	%ebp, %xmm1
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB5_37:                               # =>This Inner Loop Header: Depth=1
	mulsd	%xmm0, %xmm1
	movzbl	%cl, %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB5_39
# BB#38:                                #   in Loop: Header=BB5_37 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB5_40
	.p2align	4, 0x90
.LBB5_39:                               #   in Loop: Header=BB5_37 Depth=1
	movq	%rbx, %rdi
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	movsd	%xmm2, 32(%rsp)         # 8-byte Spill
	callq	*40(%rbx)
	movsd	32(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	16(%rsp), %xmm1         # 16-byte Reload
.LBB5_40:                               #   in Loop: Header=BB5_37 Depth=1
	addsd	%xmm2, %xmm1
	movslq	%eax, %rcx
	movb	scan_char_array+1(%rcx), %cl
	cmpb	$10, %cl
	jb	.LBB5_37
# BB#41:
	cmpl	$-1, %eax
	movq	(%rbx), %rax
	je	.LBB5_43
# BB#42:
	decq	%rax
	movq	%rax, (%rbx)
.LBB5_43:                               # %._crit_edge253
	cmpq	8(%rbx), %rax
	movq	%r15, 8(%rsp)           # 8-byte Spill
	jae	.LBB5_45
# BB#44:
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	cmpl	$-1, %eax
	jne	.LBB5_47
	jmp	.LBB5_56
.LBB5_28:
	cmpl	$-1, %eax
	movq	(%rbx), %rax
	je	.LBB5_30
# BB#29:
	decq	%rax
	movq	%rax, (%rbx)
.LBB5_30:                               # %._crit_edge249
	cmpq	8(%rbx), %rax
	movl	%r12d, 4(%rsp)          # 4-byte Spill
	jae	.LBB5_59
# BB#31:
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB5_60
.LBB5_59:
	movq	%rbx, %rdi
	callq	*40(%rbx)
.LBB5_60:
	movslq	%ebp, %r12
	cmpl	$-1, %eax
	je	.LBB5_86
# BB#61:
	movl	$-18, %r13d
	cmpl	$35, %eax
	je	.LBB5_66
# BB#62:
	movq	%r15, 8(%rsp)           # 8-byte Spill
	cmpl	$46, %eax
	movl	$-18, %r13d
	jne	.LBB5_136
# BB#63:
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB5_65
# BB#64:
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB5_19
.LBB5_16:
	movq	%rbx, %rdi
	callq	*40(%rbx)
.LBB5_17:
	movq	(%r14), %rcx
	movslq	%eax, %rdx
	testb	$8, 1(%rcx,%rdx,2)
	je	.LBB5_136
# BB#18:
	xorl	%r12d, %r12d
	jmp	.LBB5_19
.LBB5_66:
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jne	.LBB5_136
# BB#67:
	addq	$-2, %r12
	cmpq	$34, %r12
	ja	.LBB5_136
# BB#68:
	cmpl	$10, %ebp
	movq	%r15, 8(%rsp)           # 8-byte Spill
	jne	.LBB5_70
# BB#69:
	movl	$7, %r15d
	movl	$214748364, %r12d       # imm = 0xCCCCCCC
	jmp	.LBB5_71
.LBB5_45:
	movq	%rbx, %rdi
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	callq	*40(%rbx)
	movapd	16(%rsp), %xmm1         # 16-byte Reload
	cmpl	$-1, %eax
	je	.LBB5_56
.LBB5_47:
	movl	%r12d, 4(%rsp)          # 4-byte Spill
	movl	$-18, %r13d
	cmpl	$46, %eax
	jne	.LBB5_136
# BB#48:
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB5_55
# BB#49:
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB5_50
.LBB5_56:
	testl	%r12d, %r12d
	jns	.LBB5_58
# BB#57:
	xorpd	.LCPI5_1(%rip), %xmm1
.LBB5_58:
	movq	8(%rsp), %rax           # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	jmp	.LBB5_134
.LBB5_65:
	movq	%rbx, %rdi
	callq	*40(%rbx)
.LBB5_19:                               # %.preheader181
	movq	(%r14), %rcx
	movslq	%eax, %rdx
	xorl	%r15d, %r15d
	testb	$8, 1(%rcx,%rdx,2)
	je	.LBB5_91
# BB#20:                                # %.lr.ph218
	xorl	%r15d, %r15d
	movabsq	$922337203685477579, %r13 # imm = 0xCCCCCCCCCCCCCCB
	.p2align	4, 0x90
.LBB5_21:                               # =>This Inner Loop Header: Depth=1
	cmpq	%r13, %r12
	jae	.LBB5_22
# BB#87:                                #   in Loop: Header=BB5_21 Depth=1
	leaq	(%r12,%r12,4), %r12
	movslq	%eax, %rbp
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB5_89
# BB#88:                                #   in Loop: Header=BB5_21 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB5_90
	.p2align	4, 0x90
.LBB5_89:                               #   in Loop: Header=BB5_21 Depth=1
	movq	%rbx, %rdi
	callq	*40(%rbx)
.LBB5_90:                               #   in Loop: Header=BB5_21 Depth=1
	leaq	-48(%rbp,%r12,2), %r12
	decl	%r15d
	movq	(%r14), %rcx
	movslq	%eax, %rdx
	testb	$8, 1(%rcx,%rdx,2)
	jne	.LBB5_21
.LBB5_91:                               # %._crit_edge219
	movq	%r12, %rcx
	negq	%rcx
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	cmovnsq	%r12, %rcx
	cmpl	$-6, %r15d
	jl	.LBB5_94
# BB#92:                                # %._crit_edge219
	movl	%eax, %edx
	orl	$32, %edx
	cmpl	$101, %edx
	je	.LBB5_94
# BB#93:
	cvtsi2ssq	%rcx, %xmm0
	negl	%r15d
	movslq	%r15d, %rax
	divss	scan_number.powers_10(,%rax,4), %xmm0
	jmp	.LBB5_133
.LBB5_94:
	cvtsi2sdq	%rcx, %xmm1
	jmp	.LBB5_99
.LBB5_22:
	cvtsi2sdq	%r12, %xmm1
	jmp	.LBB5_51
.LBB5_55:
	movq	%rbx, %rdi
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	callq	*40(%rbx)
	movapd	16(%rsp), %xmm1         # 16-byte Reload
.LBB5_50:                               # %.preheader180
	xorl	%r15d, %r15d
.LBB5_51:                               # %.preheader180
	movq	(%r14), %rcx
	movslq	%eax, %rdx
	testb	$8, 1(%rcx,%rdx,2)
	movl	4(%rsp), %ebp           # 4-byte Reload
	je	.LBB5_97
# BB#52:
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB5_53:                               # =>This Inner Loop Header: Depth=1
	mulsd	%xmm0, %xmm1
	addl	$-48, %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB5_95
# BB#54:                                #   in Loop: Header=BB5_53 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB5_96
	.p2align	4, 0x90
.LBB5_95:                               #   in Loop: Header=BB5_53 Depth=1
	movq	%rbx, %rdi
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	movsd	%xmm2, 32(%rsp)         # 8-byte Spill
	callq	*40(%rbx)
	movsd	32(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	16(%rsp), %xmm1         # 16-byte Reload
.LBB5_96:                               #   in Loop: Header=BB5_53 Depth=1
	addsd	%xmm2, %xmm1
	decl	%r15d
	movq	(%r14), %rcx
	movslq	%eax, %rdx
	testb	$8, 1(%rcx,%rdx,2)
	jne	.LBB5_53
.LBB5_97:                               # %._crit_edge211
	testl	%ebp, %ebp
	jns	.LBB5_99
# BB#98:
	xorpd	.LCPI5_1(%rip), %xmm1
.LBB5_99:
	movl	%eax, %ecx
	orl	$32, %ecx
	cmpl	$101, %ecx
	jne	.LBB5_117
# BB#100:
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	jae	.LBB5_102
# BB#101:
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB5_103
.LBB5_102:
	movq	%rbx, %rdi
	callq	*40(%rbx)
.LBB5_103:
	xorl	%r14d, %r14d
	cmpl	$43, %eax
	movl	$0, %ebp
	je	.LBB5_107
# BB#104:
	cmpl	$45, %eax
	jne	.LBB5_106
# BB#105:
	movl	$1, %ebp
	jmp	.LBB5_107
.LBB5_106:
	decq	(%rbx)
	xorl	%ebp, %ebp
.LBB5_107:
	movl	$-13, %r13d
	jmp	.LBB5_108
	.p2align	4, 0x90
.LBB5_126:                              #   in Loop: Header=BB5_108 Depth=1
	leal	(%r14,%r14,4), %eax
	leal	(%rcx,%rax,2), %r14d
.LBB5_108:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB5_110
# BB#109:                               #   in Loop: Header=BB5_108 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB5_111
	.p2align	4, 0x90
.LBB5_110:                              #   in Loop: Header=BB5_108 Depth=1
	movq	%rbx, %rdi
	callq	*40(%rbx)
.LBB5_111:                              #   in Loop: Header=BB5_108 Depth=1
	movslq	%eax, %rcx
	movzbl	scan_char_array+1(%rcx), %ecx
	cmpl	$10, %ecx
	jae	.LBB5_112
# BB#123:                               #   in Loop: Header=BB5_108 Depth=1
	cmpl	$214748364, %r14d       # imm = 0xCCCCCCC
	jl	.LBB5_126
# BB#124:                               #   in Loop: Header=BB5_108 Depth=1
	jne	.LBB5_136
# BB#125:                               #   in Loop: Header=BB5_108 Depth=1
	cmpb	$7, %cl
	jbe	.LBB5_126
	jmp	.LBB5_136
.LBB5_112:
	cmpl	$-1, %eax
	je	.LBB5_114
# BB#113:
	decq	(%rbx)
.LBB5_114:
	movl	$-13, %r13d
	cmpl	$999, %r14d             # imm = 0x3E7
	jg	.LBB5_136
# BB#115:
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jb	.LBB5_79
# BB#116:
	movl	%r14d, %eax
	negl	%eax
	testl	%ebp, %ebp
	cmovel	%r14d, %eax
	addl	%eax, %r15d
	movq	%rbx, %rdi
	callq	*40(%rbx)
	movapd	16(%rsp), %xmm1         # 16-byte Reload
.LBB5_117:                              # %.thread178
	movl	$-18, %r13d
	cmpl	$-1, %eax
	jne	.LBB5_136
# BB#118:
	testl	%r15d, %r15d
	jle	.LBB5_127
# BB#119:                               # %.preheader
	cmpl	$7, %r15d
	jl	.LBB5_122
# BB#120:                               # %.lr.ph.preheader
	movsd	.LCPI5_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB5_121:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	mulsd	%xmm0, %xmm1
	addl	$-6, %r15d
	cmpl	$6, %r15d
	jg	.LBB5_121
.LBB5_122:                              # %._crit_edge
	movslq	%r15d, %rax
	movss	scan_number.powers_10(,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	mulsd	%xmm0, %xmm1
	jmp	.LBB5_132
.LBB5_70:
	movl	$2147483647, %eax       # imm = 0x7FFFFFFF
	xorl	%edx, %edx
	divl	%ebp
	movl	%edx, %r15d
	movl	%eax, %r12d
.LBB5_71:                               # %.preheader.i
	xorl	%r14d, %r14d
	movl	$-13, %r13d
	jmp	.LBB5_72
	.p2align	4, 0x90
.LBB5_83:                               #   in Loop: Header=BB5_72 Depth=1
	imull	%ebp, %r14d
	addl	%ecx, %r14d
.LBB5_72:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB5_74
# BB#73:                                #   in Loop: Header=BB5_72 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB5_75
	.p2align	4, 0x90
.LBB5_74:                               #   in Loop: Header=BB5_72 Depth=1
	movq	%rbx, %rdi
	callq	*40(%rbx)
.LBB5_75:                               #   in Loop: Header=BB5_72 Depth=1
	movslq	%eax, %rcx
	movzbl	scan_char_array+1(%rcx), %ecx
	cmpl	%ebp, %ecx
	jge	.LBB5_76
# BB#80:                                #   in Loop: Header=BB5_72 Depth=1
	cmpl	%r12d, %r14d
	jl	.LBB5_83
# BB#81:                                #   in Loop: Header=BB5_72 Depth=1
	jg	.LBB5_136
# BB#82:                                #   in Loop: Header=BB5_72 Depth=1
	cmpl	%r15d, %ecx
	jbe	.LBB5_83
	jmp	.LBB5_136
.LBB5_76:
	cmpl	$-1, %eax
	movq	(%rbx), %rax
	je	.LBB5_78
# BB#77:
	decq	%rax
	movq	%rax, (%rbx)
.LBB5_78:                               # %._crit_edge251
	cmpq	8(%rbx), %rax
	jae	.LBB5_84
.LBB5_79:                               # %.thread
	incq	%rax
	movq	%rax, (%rbx)
	movl	$-18, %r13d
	jmp	.LBB5_136
.LBB5_127:
	jns	.LBB5_132
# BB#128:                               # %.preheader179
	cmpl	$-7, %r15d
	jg	.LBB5_131
# BB#129:                               # %.lr.ph203.preheader
	movsd	.LCPI5_2(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_130:                              # %.lr.ph203
                                        # =>This Inner Loop Header: Depth=1
	divsd	%xmm0, %xmm1
	addl	$6, %r15d
	cmpl	$-6, %r15d
	jl	.LBB5_130
.LBB5_131:                              # %._crit_edge204
	negl	%r15d
	movslq	%r15d, %rax
	movss	scan_number.powers_10(,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	divsd	%xmm0, %xmm1
.LBB5_132:
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
.LBB5_133:                              # %scan_int.exit139
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB5_134:                              # %scan_int.exit139
	movss	%xmm0, (%rax)
	movw	$44, 8(%rax)
	jmp	.LBB5_135
.LBB5_84:
	movq	%rbx, %rdi
	callq	*40(%rbx)
	cmpl	$-1, %eax
	movl	$-18, %r13d
	jne	.LBB5_136
# BB#85:
	movslq	%r14d, %r12
	movq	8(%rsp), %r15           # 8-byte Reload
.LBB5_86:
	movq	%r12, %rax
	negq	%rax
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	cmovnsq	%r12, %rax
	movq	%rax, (%r15)
	movw	$20, 8(%r15)
.LBB5_135:                              # %scan_int.exit139
	xorl	%r13d, %r13d
.LBB5_136:                              # %scan_int.exit139
	movl	%r13d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	scan_number, .Lfunc_end5-scan_number
	.cfi_endproc

	.globl	scan_int
	.p2align	4, 0x90
	.type	scan_int,@function
scan_int:                               # @scan_int
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi72:
	.cfi_def_cfa_offset 96
.Lcfi73:
	.cfi_offset %rbx, -56
.Lcfi74:
	.cfi_offset %r12, -48
.Lcfi75:
	.cfi_offset %r13, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	%esi, %ebp
	movq	%rdi, %rbx
	cmpl	$10, %ebp
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	jne	.LBB6_2
# BB#1:
	movl	$7, %r12d
	movl	$214748364, %r13d       # imm = 0xCCCCCCC
	jmp	.LBB6_3
.LBB6_2:
	movl	$2147483647, %eax       # imm = 0x7FFFFFFF
	xorl	%edx, %edx
	divl	%ebp
	movl	%edx, %r12d
	movl	%eax, %r13d
.LBB6_3:                                # %.preheader
	xorl	%r14d, %r14d
	jmp	.LBB6_4
	.p2align	4, 0x90
.LBB6_14:                               #   in Loop: Header=BB6_4 Depth=1
	imull	%ebp, %r14d
	addl	%edx, %r14d
.LBB6_4:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB6_6
# BB#5:                                 #   in Loop: Header=BB6_4 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB6_7
	.p2align	4, 0x90
.LBB6_6:                                #   in Loop: Header=BB6_4 Depth=1
	movq	%rbx, %rdi
	callq	*40(%rbx)
.LBB6_7:                                #   in Loop: Header=BB6_4 Depth=1
	movslq	%eax, %rcx
	movb	scan_char_array+1(%rcx), %cl
	movzbl	%cl, %edx
	cmpl	%ebp, %edx
	jge	.LBB6_8
# BB#11:                                #   in Loop: Header=BB6_4 Depth=1
	cmpl	%r13d, %r14d
	jl	.LBB6_14
# BB#12:                                #   in Loop: Header=BB6_4 Depth=1
	jg	.LBB6_15
# BB#13:                                #   in Loop: Header=BB6_4 Depth=1
	cmpl	%r12d, %edx
	jbe	.LBB6_14
.LBB6_15:
	movq	8(%rsp), %r15           # 8-byte Reload
	testq	%r15, %r15
	je	.LBB6_16
# BB#17:
	cvtsi2sdl	%r14d, %xmm0
	cvtsi2sdl	%ebp, %xmm1
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB6_18:                               # =>This Inner Loop Header: Depth=1
	mulsd	%xmm1, %xmm0
	movzbl	%cl, %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB6_20
# BB#19:                                #   in Loop: Header=BB6_18 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB6_21
	.p2align	4, 0x90
.LBB6_20:                               #   in Loop: Header=BB6_18 Depth=1
	movq	%rbx, %rdi
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	movsd	%xmm2, 24(%rsp)         # 8-byte Spill
	callq	*40(%rbx)
	movsd	24(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB6_21:                               #   in Loop: Header=BB6_18 Depth=1
	addsd	%xmm2, %xmm0
	movslq	%eax, %rcx
	movzbl	scan_char_array+1(%rcx), %ecx
	cmpl	%ebp, %ecx
	jl	.LBB6_18
# BB#22:
	cmpl	$-1, %eax
	je	.LBB6_24
# BB#23:
	decq	(%rbx)
.LBB6_24:
	movsd	%xmm0, (%r15)
	movl	$1, %eax
	jmp	.LBB6_25
.LBB6_8:
	cmpl	$-1, %eax
	je	.LBB6_10
# BB#9:
	decq	(%rbx)
.LBB6_10:
	movslq	%r14d, %rax
	movq	%rax, (%r15)
	xorl	%eax, %eax
.LBB6_25:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_16:
	movl	$-13, %eax
	jmp	.LBB6_25
.Lfunc_end6:
	.size	scan_int, .Lfunc_end6-scan_int
	.cfi_endproc

	.globl	mk_string
	.p2align	4, 0x90
	.type	mk_string,@function
mk_string:                              # @mk_string
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi81:
	.cfi_def_cfa_offset 32
.Lcfi82:
	.cfi_offset %rbx, -24
.Lcfi83:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	(%rsi), %rdi
	movq	8(%rsi), %rbx
	subq	%rdi, %rbx
	movl	16(%rsi), %esi
	movl	$1, %ecx
	movl	$.L.str.6, %r8d
	movl	%ebx, %edx
	callq	alloc_shrink
	testq	%rax, %rax
	je	.LBB7_1
# BB#2:
	movq	%rax, (%r14)
	movw	$822, 8(%r14)           # imm = 0x336
	movw	%bx, 10(%r14)
	xorl	%eax, %eax
	jmp	.LBB7_3
.LBB7_1:
	movl	$-25, %eax
.LBB7_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	mk_string, .Lfunc_end7-mk_string
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"scanner"
	.size	.L.str, 8

	.type	scan_char_array,@object # @scan_char_array
	.comm	scan_char_array,257,16
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"["
	.size	.L.str.1, 2

	.type	left_bracket,@object    # @left_bracket
	.comm	left_bracket,16,8
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"]"
	.size	.L.str.2, 2

	.type	right_bracket,@object   # @right_bracket
	.comm	right_bracket,16,8
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.zero	1
	.size	.L.str.3, 1

	.type	empty_name,@object      # @empty_name
	.comm	empty_name,16,8
	.type	array_packing,@object   # @array_packing
	.comm	array_packing,4,4
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"scanner(top proc)"
	.size	.L.str.4, 18

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"scanner(proc)"
	.size	.L.str.5, 14

	.type	scan_number.powers_10,@object # @scan_number.powers_10
	.section	.rodata,"a",@progbits
	.p2align	4
scan_number.powers_10:
	.long	1065353216              # float 1
	.long	1092616192              # float 10
	.long	1120403456              # float 100
	.long	1148846080              # float 1000
	.long	1176256512              # float 1.0E+4
	.long	1203982336              # float 1.0E+5
	.long	1232348160              # float 1.0E+6
	.size	scan_number.powers_10, 28

	.type	.L.str.6,@object        # @.str.6
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.6:
	.asciz	"scanner(string)"
	.size	.L.str.6, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
