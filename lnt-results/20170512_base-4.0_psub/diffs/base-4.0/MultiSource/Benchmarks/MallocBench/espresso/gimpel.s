	.text
	.file	"gimpel.bc"
	.globl	gimpel_reduce
	.p2align	4, 0x90
	.type	gimpel_reduce,@function
gimpel_reduce:                          # @gimpel_reduce
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.LBB0_15
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	cmpl	$2, 4(%rax)
	jne	.LBB0_13
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	movq	16(%rax), %rdi
	movslq	4(%rdi), %rbp
	testq	%rbp, %rbp
	js	.LBB0_6
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	cmpl	24(%r15), %ebp
	jge	.LBB0_6
# BB#4:                                 #   in Loop: Header=BB0_1 Depth=1
	movq	16(%r15), %rdi
	movq	(%rdi,%rbp,8), %r10
	jmp	.LBB0_7
	.p2align	4, 0x90
.LBB0_6:                                #   in Loop: Header=BB0_1 Depth=1
	xorl	%r10d, %r10d
.LBB0_7:                                #   in Loop: Header=BB0_1 Depth=1
	movq	24(%rax), %rdi
	movslq	4(%rdi), %rbx
	testq	%rbx, %rbx
	js	.LBB0_11
# BB#8:                                 #   in Loop: Header=BB0_1 Depth=1
	cmpl	24(%r15), %ebx
	jge	.LBB0_11
# BB#9:                                 #   in Loop: Header=BB0_1 Depth=1
	movq	16(%r15), %rdi
	movq	(%rdi,%rbx,8), %r13
	cmpl	$2, 4(%r10)
	jne	.LBB0_12
	jmp	.LBB0_16
	.p2align	4, 0x90
.LBB0_11:                               #   in Loop: Header=BB0_1 Depth=1
	xorl	%r13d, %r13d
	cmpl	$2, 4(%r10)
	je	.LBB0_16
.LBB0_12:                               #   in Loop: Header=BB0_1 Depth=1
	cmpl	$2, 4(%r13)
	je	.LBB0_17
.LBB0_13:                               #   in Loop: Header=BB0_1 Depth=1
	movq	32(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_1
.LBB0_15:
	xorl	%eax, %eax
	jmp	.LBB0_46
.LBB0_16:
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movl	%r8d, 24(%rsp)          # 4-byte Spill
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	movl	%r9d, 32(%rsp)          # 4-byte Spill
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	jmp	.LBB0_27
.LBB0_17:
	testl	%ebx, %ebx
	js	.LBB0_21
# BB#18:
	cmpl	24(%r15), %ebx
	jge	.LBB0_21
# BB#19:
	movq	16(%r15), %rdi
	movq	(%rdi,%rbx,8), %r10
	jmp	.LBB0_22
.LBB0_21:
	xorl	%r10d, %r10d
.LBB0_22:
	testl	%ebp, %ebp
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movl	%r9d, 32(%rsp)          # 4-byte Spill
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	movl	%r8d, 24(%rsp)          # 4-byte Spill
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	js	.LBB0_26
# BB#23:
	cmpl	24(%r15), %ebp
	jge	.LBB0_26
# BB#24:
	movq	16(%r15), %rsi
	movq	(%rsi,%rbp,8), %r13
	jmp	.LBB0_27
.LBB0_26:
	xorl	%r13d, %r13d
.LBB0_27:                               # %.thread
	movl	(%rax), %r14d
	movq	16(%r10), %rax
	movl	(%rax), %ecx
	cmpl	%r14d, %ecx
	jne	.LBB0_29
# BB#28:
	movq	24(%r10), %rax
	movl	(%rax), %ecx
.LBB0_29:
	movl	(%r10), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	(%r13), %ebx
	testl	%ecx, %ecx
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	js	.LBB0_33
# BB#30:
	cmpl	8(%r15), %ecx
	jge	.LBB0_33
# BB#31:
	movq	(%r15), %rax
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rdi
	jmp	.LBB0_34
.LBB0_33:
	xorl	%edi, %edi
.LBB0_34:
	xorl	%eax, %eax
	callq	sm_row_dup
	movq	%rax, %r12
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	20(%rsp), %esi          # 4-byte Reload
	callq	sm_row_remove
	movq	16(%r13), %rbp
	testq	%rbp, %rbp
	movl	%ebx, %r13d
	je	.LBB0_40
	.p2align	4, 0x90
.LBB0_35:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_38 Depth 2
	movl	(%rbp), %esi
	cmpl	%r14d, %esi
	je	.LBB0_39
# BB#36:                                #   in Loop: Header=BB0_35 Depth=1
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_38
	jmp	.LBB0_39
	.p2align	4, 0x90
.LBB0_37:                               # %.lr.ph..lr.ph_crit_edge
                                        #   in Loop: Header=BB0_38 Depth=2
	movl	(%rbp), %esi
.LBB0_38:                               # %.lr.ph..lr.ph_crit_edge
                                        #   Parent Loop BB0_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rbx), %edx
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sm_insert
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_37
.LBB0_39:                               # %.loopexit
                                        #   in Loop: Header=BB0_35 Depth=1
	movq	8(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB0_35
.LBB0_40:                               # %._crit_edge
	xorl	%eax, %eax
	movq	%r15, %rdi
	movl	20(%rsp), %esi          # 4-byte Reload
	callq	sm_delcol
	xorl	%eax, %eax
	movq	%r15, %rdi
	movl	%r13d, %esi
	callq	sm_delcol
	xorl	%eax, %eax
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	sm_delrow
	xorl	%eax, %eax
	movq	%r15, %rdi
	movl	36(%rsp), %esi          # 4-byte Reload
	callq	sm_delrow
	movq	112(%rsp), %rax
	movq	%rax, %rbx
	incl	24(%rbx)
	incl	28(%rbx)
	movl	28(%rsp), %ecx          # 4-byte Reload
	decl	%ecx
	movl	24(%rsp), %r8d          # 4-byte Reload
	decl	%r8d
	movq	%rbx, (%rsp)
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdx
	movl	32(%rsp), %r9d          # 4-byte Reload
	callq	sm_mincov
	movq	120(%rsp), %rbp
	movq	%rax, (%rbp)
	decl	28(%rbx)
	testq	%rax, %rax
	je	.LBB0_45
# BB#41:
	movq	(%rax), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sm_row_intersects
	testl	%eax, %eax
	movq	(%rbp), %rdi
	je	.LBB0_43
# BB#42:
	xorl	%eax, %eax
	movq	%r14, %rsi
	movl	%r13d, %edx
	jmp	.LBB0_44
.LBB0_43:
	xorl	%eax, %eax
	movq	%r14, %rsi
	movl	20(%rsp), %edx          # 4-byte Reload
.LBB0_44:
	callq	solution_add
.LBB0_45:
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sm_row_free
	movl	$1, %eax
.LBB0_46:                               # %.loopexit128
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	gimpel_reduce, .Lfunc_end0-gimpel_reduce
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
