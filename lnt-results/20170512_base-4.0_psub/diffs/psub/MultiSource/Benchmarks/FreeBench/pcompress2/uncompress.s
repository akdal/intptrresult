	.text
	.file	"uncompress.bc"
	.globl	uncompress
	.p2align	4, 0x90
	.type	uncompress,@function
uncompress:                             # @uncompress
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$2056, %rsp             # imm = 0x808
.Lcfi6:
	.cfi_def_cfa_offset 2112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	8(%rsi), %rdi
	movl	$.L.str, %esi
	callq	fopen
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_1
# BB#3:
	movl	$size, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fread
	movl	$orgpos, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fread
	movl	size(%rip), %ebx
	addl	%ebx, %ebx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r15
	movq	%r15, in(%rip)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r12
	movq	%r12, deari(%rip)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r13
	movq	%r13, derle(%rip)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, debw(%rip)
	testq	%rax, %rax
	je	.LBB0_7
# BB#4:
	testq	%r15, %r15
	je	.LBB0_7
# BB#5:
	testq	%r12, %r12
	je	.LBB0_7
# BB#6:
	testq	%r13, %r13
	je	.LBB0_7
# BB#8:
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rcx
	callq	fread
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	fclose
	movl	%ebx, %edi
	callq	do_deari
	movl	%eax, %r14d
	movq	in(%rip), %rdi
	callq	free
	testl	%r14d, %r14d
	movq	deari(%rip), %rsi
	je	.LBB0_18
# BB#9:                                 # %.lr.ph28.i.preheader
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_10:                               # %.lr.ph28.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_13 Depth 2
	movl	%ebp, %ebp
	movzbl	(%rsi,%rbp), %edx
	testb	%dl, %dl
	js	.LBB0_11
# BB#16:                                #   in Loop: Header=BB0_10 Depth=1
	addq	%rbp, %rsi
	movl	%r15d, %edi
	addq	derle(%rip), %rdi
	incq	%rsi
	callq	memcpy
	movq	deari(%rip), %rsi
	movzbl	(%rsi,%rbp), %eax
	addl	%eax, %r15d
	leal	1(%rbp,%rax), %ebp
	cmpl	%r14d, %ebp
	jb	.LBB0_10
	jmp	.LBB0_18
	.p2align	4, 0x90
.LBB0_11:                               # %.preheader.i
                                        #   in Loop: Header=BB0_10 Depth=1
	testb	$127, %dl
	je	.LBB0_15
# BB#12:                                # %.lr.ph.i
                                        #   in Loop: Header=BB0_10 Depth=1
	leal	1(%rbp), %ecx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_13:                               #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r15,%rax), %edx
	movzbl	(%rsi,%rcx), %ebx
	movq	derle(%rip), %rsi
	movb	%bl, (%rsi,%rdx)
	incl	%eax
	movq	deari(%rip), %rsi
	movzbl	(%rsi,%rbp), %edx
	andl	$127, %edx
	cmpl	%edx, %eax
	jb	.LBB0_13
# BB#14:                                # %._crit_edge.i.loopexit
                                        #   in Loop: Header=BB0_10 Depth=1
	addl	%eax, %r15d
.LBB0_15:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_10 Depth=1
	addl	$2, %ebp
	cmpl	%r14d, %ebp
	jb	.LBB0_10
.LBB0_18:                               # %do_derle.exit
	movq	%rsi, %rdi
	callq	free
	movq	derle(%rip), %r14
	movl	size(%rip), %r15d
	leaq	(,%r15,4), %rdi
	callq	malloc
	movq	%rax, %r12
	movq	%rsp, %rdi
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$1024, %edx             # imm = 0x400
	callq	memset
	testq	%r15, %r15
	je	.LBB0_24
# BB#19:                                # %.lr.ph52.i.preheader
	leaq	-1(%r15), %rax
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	andq	$3, %rdx
	je	.LBB0_21
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph52.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rcx), %esi
	incl	(%rsp,%rsi,4)
	incq	%rcx
	cmpq	%rcx, %rdx
	jne	.LBB0_20
.LBB0_21:                               # %.lr.ph52.i.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB0_24
# BB#22:                                # %.lr.ph52.i.preheader.new
	movq	%r15, %rax
	subq	%rcx, %rax
	leaq	3(%r14,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_23:                               # %.lr.ph52.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rcx), %edx
	incl	(%rsp,%rdx,4)
	movzbl	-2(%rcx), %edx
	incl	(%rsp,%rdx,4)
	movzbl	-1(%rcx), %edx
	incl	(%rsp,%rdx,4)
	movzbl	(%rcx), %edx
	incl	(%rsp,%rdx,4)
	addq	$4, %rcx
	addq	$-4, %rax
	jne	.LBB0_23
.LBB0_24:                               # %.preheader43.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_25:                               # %.preheader43.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, 1024(%rsp,%rax,4)
	addl	(%rsp,%rax,4), %ebp
	movl	$0, (%rsp,%rax,4)
	movl	%ebp, 1028(%rsp,%rax,4)
	addl	4(%rsp,%rax,4), %ebp
	movl	$0, 4(%rsp,%rax,4)
	movl	%ebp, 1032(%rsp,%rax,4)
	addl	8(%rsp,%rax,4), %ebp
	movl	$0, 8(%rsp,%rax,4)
	movl	%ebp, 1036(%rsp,%rax,4)
	addl	12(%rsp,%rax,4), %ebp
	movl	$0, 12(%rsp,%rax,4)
	addq	$4, %rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB0_25
# BB#26:                                # %.preheader.i17
	testl	%r15d, %r15d
	je	.LBB0_27
# BB#28:                                # %.lr.ph47.i.preheader
	testb	$1, %r15b
	jne	.LBB0_30
# BB#29:
	xorl	%edx, %edx
	cmpl	$1, %r15d
	jne	.LBB0_32
	jmp	.LBB0_34
.LBB0_27:
	movl	$4294967295, %r15d      # imm = 0xFFFFFFFF
	jmp	.LBB0_35
.LBB0_30:                               # %.lr.ph47.i.prol
	movzbl	(%r14), %eax
	movl	(%rsp,%rax,4), %ecx
	movl	1024(%rsp,%rax,4), %edx
	addl	%ecx, %edx
	movl	%edx, (%r12)
	incl	%ecx
	movl	%ecx, (%rsp,%rax,4)
	movl	$1, %edx
	cmpl	$1, %r15d
	je	.LBB0_34
.LBB0_32:                               # %.lr.ph47.i.preheader.new
	movq	%r15, %rax
	subq	%rdx, %rax
	leaq	4(%r12,%rdx,4), %rcx
	leaq	1(%r14,%rdx), %rdx
	.p2align	4, 0x90
.LBB0_33:                               # %.lr.ph47.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rdx), %esi
	movl	(%rsp,%rsi,4), %edi
	movl	1024(%rsp,%rsi,4), %ebp
	addl	%edi, %ebp
	movl	%ebp, -4(%rcx)
	incl	%edi
	movl	%edi, (%rsp,%rsi,4)
	movzbl	(%rdx), %esi
	movl	(%rsp,%rsi,4), %edi
	movl	1024(%rsp,%rsi,4), %ebp
	addl	%edi, %ebp
	movl	%ebp, (%rcx)
	incl	%edi
	movl	%edi, (%rsp,%rsi,4)
	addq	$8, %rcx
	addq	$2, %rdx
	addq	$-2, %rax
	jne	.LBB0_33
.LBB0_34:                               # %._crit_edge48.loopexit.i
	decl	%r15d
.LBB0_35:                               # %._crit_edge48.i
	movl	orgpos(%rip), %eax
	movb	(%r14,%rax), %al
	movq	debw(%rip), %rcx
	movb	%al, (%rcx,%r15)
	movl	size(%rip), %edx
	cmpl	$2, %edx
	jb	.LBB0_38
# BB#36:                                # %.lr.ph.preheader.i
	movl	$1, %eax
	movl	$-2, %ecx
	movl	orgpos(%rip), %esi
	.p2align	4, 0x90
.LBB0_37:                               # %.lr.ph.i19
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, %esi
	movl	(%r12,%rsi,4), %esi
	movzbl	(%r14,%rsi), %ebx
	movq	debw(%rip), %rsi
	addl	%ecx, %edx
	movb	%bl, (%rsi,%rdx)
	movl	orgpos(%rip), %edx
	movl	(%r12,%rdx,4), %esi
	movl	%esi, orgpos(%rip)
	incl	%eax
	movl	size(%rip), %edx
	decl	%ecx
	cmpl	%eax, %edx
	ja	.LBB0_37
.LBB0_38:                               # %do_debwe.exit
	movq	%r12, %rdi
	callq	free
	movq	derle(%rip), %rdi
	callq	free
	movq	debw(%rip), %rdi
	movl	size(%rip), %edx
	movq	stdout(%rip), %rcx
	movl	$1, %esi
	callq	fwrite
	movq	debw(%rip), %rdi
	addq	$2056, %rsp             # imm = 0x808
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB0_1:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$30, %esi
	jmp	.LBB0_2
.LBB0_7:
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$21, %esi
.LBB0_2:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	uncompress, .Lfunc_end0-uncompress
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"r"
	.size	.L.str, 2

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"ERROR: Could not find infile.\n"
	.size	.L.str.1, 31

	.type	size,@object            # @size
	.comm	size,4,4
	.type	orgpos,@object          # @orgpos
	.comm	orgpos,4,4
	.type	in,@object              # @in
	.comm	in,8,8
	.type	deari,@object           # @deari
	.comm	deari,8,8
	.type	derle,@object           # @derle
	.comm	derle,8,8
	.type	debw,@object            # @debw
	.comm	debw,8,8
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"ERROR: Out of memory\n"
	.size	.L.str.2, 22


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
