	.text
	.file	"gspath.bc"
	.globl	gs_newpath
	.p2align	4, 0x90
	.type	gs_newpath,@function
gs_newpath:                             # @gs_newpath
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	256(%rbx), %rdi
	callq	gx_path_release
	movq	256(%rbx), %rdi
	leaq	8(%rbx), %rsi
	callq	gx_path_init
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	gs_newpath, .Lfunc_end0-gs_newpath
	.cfi_endproc

	.globl	gs_closepath
	.p2align	4, 0x90
	.type	gs_closepath,@function
gs_closepath:                           # @gs_closepath
	.cfi_startproc
# BB#0:
	movq	256(%rdi), %rdi
	jmp	gx_path_close_subpath   # TAILCALL
.Lfunc_end1:
	.size	gs_closepath, .Lfunc_end1-gs_closepath
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4553139223271571456     # double 2.44140625E-4
	.text
	.globl	gs_currentpoint
	.p2align	4, 0x90
	.type	gs_currentpoint,@function
gs_currentpoint:                        # @gs_currentpoint
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	256(%rbx), %rdi
	leaq	8(%rsp), %rsi
	callq	gx_path_current_point
	testl	%eax, %eax
	js	.LBB2_2
# BB#1:
	cvtsi2sdq	8(%rsp), %xmm0
	movsd	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	cvtsi2sdq	16(%rsp), %xmm2
	mulsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	cvtss2sd	%xmm1, %xmm1
	movb	$2, %al
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	gs_itransform
.LBB2_2:
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	gs_currentpoint, .Lfunc_end2-gs_currentpoint
	.cfi_endproc

	.globl	gs_moveto
	.p2align	4, 0x90
	.type	gs_moveto,@function
gs_moveto:                              # @gs_moveto
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	24(%rbx), %rdi
	movq	%rsp, %rsi
	callq	gs_point_transform2fixed
	testl	%eax, %eax
	js	.LBB3_2
# BB#1:
	movq	256(%rbx), %rdi
	movq	(%rsp), %rsi
	movq	8(%rsp), %rdx
	callq	gx_path_add_point
.LBB3_2:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end3:
	.size	gs_moveto, .Lfunc_end3-gs_moveto
	.cfi_endproc

	.globl	gs_rmoveto
	.p2align	4, 0x90
	.type	gs_rmoveto,@function
gs_rmoveto:                             # @gs_rmoveto
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	24(%rbx), %rdi
	movq	%rsp, %rsi
	callq	gs_distance_transform2fixed
	testl	%eax, %eax
	js	.LBB4_2
# BB#1:
	movq	256(%rbx), %rdi
	movq	(%rsp), %rsi
	movq	8(%rsp), %rdx
	callq	gx_path_add_relative_point
.LBB4_2:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end4:
	.size	gs_rmoveto, .Lfunc_end4-gs_rmoveto
	.cfi_endproc

	.globl	gs_lineto
	.p2align	4, 0x90
	.type	gs_lineto,@function
gs_lineto:                              # @gs_lineto
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	24(%rbx), %rdi
	movq	%rsp, %rsi
	callq	gs_point_transform2fixed
	testl	%eax, %eax
	js	.LBB5_2
# BB#1:
	movq	256(%rbx), %rdi
	movq	(%rsp), %rsi
	movq	8(%rsp), %rdx
	callq	gx_path_add_line
.LBB5_2:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end5:
	.size	gs_lineto, .Lfunc_end5-gs_lineto
	.cfi_endproc

	.globl	gs_rlineto
	.p2align	4, 0x90
	.type	gs_rlineto,@function
gs_rlineto:                             # @gs_rlineto
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
	subq	$48, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 64
.Lcfi18:
	.cfi_offset %rbx, -16
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rdi, %rbx
	movq	256(%rbx), %rdi
	leaq	32(%rsp), %rsi
	callq	gx_path_current_point
	testl	%eax, %eax
	js	.LBB6_3
# BB#1:
	leaq	24(%rbx), %rdi
	leaq	16(%rsp), %rsi
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	gs_distance_transform2fixed
	testl	%eax, %eax
	js	.LBB6_3
# BB#2:
	movq	256(%rbx), %rdi
	movq	16(%rsp), %rsi
	movq	24(%rsp), %rdx
	addq	32(%rsp), %rsi
	addq	40(%rsp), %rdx
	callq	gx_path_add_line
.LBB6_3:
	addq	$48, %rsp
	popq	%rbx
	retq
.Lfunc_end6:
	.size	gs_rlineto, .Lfunc_end6-gs_rlineto
	.cfi_endproc

	.globl	gs_arc
	.p2align	4, 0x90
	.type	gs_arc,@function
gs_arc:                                 # @gs_arc
	.cfi_startproc
# BB#0:
	xorl	%esi, %esi
	jmp	arc_either              # TAILCALL
.Lfunc_end7:
	.size	gs_arc, .Lfunc_end7-gs_arc
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI8_0:
	.quad	4661225614328463360     # double 4096
.LCPI8_1:
	.quad	4553139223271571456     # double 2.44140625E-4
.LCPI8_2:
	.quad	4580687790476533049     # double 0.017453292519943295
.LCPI8_4:
	.quad	4576184190849162553     # double 0.0087266462599716477
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_3:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	arc_either
	.p2align	4, 0x90
	.type	arc_either,@function
arc_either:                             # @arc_either
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 48
	subq	$96, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 144
.Lcfi25:
	.cfi_offset %rbx, -48
.Lcfi26:
	.cfi_offset %r12, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	%rdi, %r12
	movl	$-15, %eax
	xorpd	%xmm0, %xmm0
	movsd	%xmm2, 16(%rsp)         # 8-byte Spill
	ucomisd	%xmm2, %xmm0
	ja	.LBB8_13
# BB#1:
	movsd	.LCPI8_0(%rip), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm3
	cvttsd2si	%xmm3, %rbp
	mulsd	%xmm0, %xmm4
	cvttsd2si	%xmm4, %r14
	cmpq	%r14, %rbp
	jne	.LBB8_3
# BB#2:
	movq	%rbp, %r14
	jmp	.LBB8_6
.LBB8_3:
	movabsq	$3279421168659475843, %rcx # imm = 0x2D82D82D82D82D83
	movq	%rbp, %rax
	imulq	%rcx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$18, %rdx
	addq	%rax, %rdx
	imulq	$1474560, %rdx, %rax    # imm = 0x168000
	subq	%rax, %rbp
	movq	%r14, %rax
	imulq	%rcx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$18, %rdx
	addq	%rax, %rdx
	imulq	$1474560, %rdx, %rax    # imm = 0x168000
	subq	%rax, %r14
	testl	%r15d, %r15d
	je	.LBB8_5
# BB#4:
	leaq	1474560(%rbp), %rax
	cmpq	%rbp, %r14
	cmovgeq	%rax, %rbp
	jmp	.LBB8_6
.LBB8_5:
	leaq	1474560(%r14), %rax
	cmpq	%rbp, %r14
	cmovleq	%rax, %r14
.LBB8_6:
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rbp, %xmm0
	mulsd	.LCPI8_1(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI8_2(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	callq	sin
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	cvtsd2ss	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	cos
	movaps	48(%rsp), %xmm7         # 16-byte Reload
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	cvtsd2ss	%xmm0, %xmm8
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm8, %xmm0
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	xorps	%xmm4, %xmm4
	cvtsd2ss	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm7, %xmm0
	addsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	cvtsd2ss	%xmm0, %xmm6
	movq	%r14, %rbx
	subq	%rbp, %rbx
	movl	$1, %ebp
	testl	%r15d, %r15d
	je	.LBB8_9
	.p2align	4, 0x90
.LBB8_7:                                # %.preheader159
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$-368641, %rbx          # imm = 0xFFFA5FFF
	jg	.LBB8_11
# BB#8:                                 #   in Loop: Header=BB8_7 Depth=1
	movaps	%xmm8, %xmm0
	xorps	.LCPI8_3(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm7, %xmm0
	movss	%xmm7, 32(%rsp)         # 4-byte Spill
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	cvtsd2ss	%xmm0, %xmm2
	movss	%xmm2, 4(%rsp)          # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm8, %xmm0
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm1, %xmm3
	movss	%xmm3, (%rsp)           # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm4, %xmm0
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm6, %xmm1
	cvtss2sd	%xmm2, %xmm2
	cvtss2sd	%xmm3, %xmm3
	addss	%xmm7, %xmm4
	cvtss2sd	%xmm4, %xmm4
	subss	%xmm8, %xmm6
	cvtss2sd	%xmm6, %xmm5
	movq	%r12, %rdi
	movl	%ebp, %esi
	callq	arc_add
	movaps	48(%rsp), %xmm7         # 16-byte Reload
	addq	$368640, %rbx           # imm = 0x5A000
	xorl	%ebp, %ebp
	testl	%eax, %eax
	movss	4(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	(%rsp), %xmm6           # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	jns	.LBB8_7
	jmp	.LBB8_13
	.p2align	4, 0x90
.LBB8_9:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$368641, %rbx           # imm = 0x5A001
	jl	.LBB8_11
# BB#10:                                #   in Loop: Header=BB8_9 Depth=1
	movaps	%xmm7, %xmm0
	xorps	.LCPI8_3(%rip), %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm7, %xmm0
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm2
	movss	%xmm2, 4(%rsp)          # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm8, %xmm0
	addsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	xorps	%xmm3, %xmm3
	cvtsd2ss	%xmm0, %xmm3
	movss	%xmm3, (%rsp)           # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm4, %xmm0
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm6, %xmm1
	cvtss2sd	%xmm2, %xmm2
	cvtss2sd	%xmm3, %xmm3
	subss	%xmm7, %xmm4
	cvtss2sd	%xmm4, %xmm4
	addss	%xmm8, %xmm6
	movaps	%xmm8, 48(%rsp)         # 16-byte Spill
	cvtss2sd	%xmm6, %xmm5
	movq	%r12, %rdi
	movl	%ebp, %esi
	callq	arc_add
	addq	$-368640, %rbx          # imm = 0xFFFA6000
	xorl	%ebp, %ebp
	testl	%eax, %eax
	movss	4(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	(%rsp), %xmm6           # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	48(%rsp), %xmm7         # 16-byte Reload
	movaps	32(%rsp), %xmm8         # 16-byte Reload
	jns	.LBB8_9
	jmp	.LBB8_13
.LBB8_11:                               # %.loopexit
	testq	%rbx, %rbx
	je	.LBB8_12
# BB#14:
	movss	%xmm7, 32(%rsp)         # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rbx, %xmm0
	mulsd	.LCPI8_1(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI8_4(%rip), %xmm0
	movaps	%xmm8, 48(%rsp)         # 16-byte Spill
	movss	%xmm6, 4(%rsp)          # 4-byte Spill
	movss	%xmm4, (%rsp)           # 4-byte Spill
	callq	tan
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 68(%rsp)         # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%r14, %xmm0
	mulsd	.LCPI8_1(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI8_2(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	(%rsp), %xmm1           # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	movsd	%xmm1, 88(%rsp)         # 8-byte Spill
	movss	4(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	movsd	%xmm1, 80(%rsp)         # 8-byte Spill
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 72(%rsp)         # 8-byte Spill
	callq	cos
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	72(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	24(%rsp), %xmm1         # 8-byte Folded Reload
	movapd	%xmm1, %xmm3
	movss	32(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	68(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	(%rsp), %xmm1           # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	xorps	%xmm4, %xmm4
	cvtss2sd	%xmm1, %xmm4
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	mulss	%xmm2, %xmm0
	addss	4(%rsp), %xmm0          # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm5
	movq	%r12, %rdi
	movsd	88(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	80(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movl	%ebp, %esi
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	arc_add                 # TAILCALL
.LBB8_12:
	xorl	%eax, %eax
.LBB8_13:                               # %.loopexit158
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	arc_either, .Lfunc_end8-arc_either
	.cfi_endproc

	.globl	gs_arcn
	.p2align	4, 0x90
	.type	gs_arcn,@function
gs_arcn:                                # @gs_arcn
	.cfi_startproc
# BB#0:
	movl	$1, %esi
	jmp	arc_either              # TAILCALL
.Lfunc_end9:
	.size	gs_arcn, .Lfunc_end9-gs_arcn
	.cfi_endproc

	.globl	arc_add
	.p2align	4, 0x90
	.type	arc_add,@function
arc_add:                                # @arc_add
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 32
	subq	$112, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 144
.Lcfi34:
	.cfi_offset %rbx, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movsd	%xmm5, 40(%rsp)         # 8-byte Spill
	movsd	%xmm4, 32(%rsp)         # 8-byte Spill
	movsd	%xmm3, 56(%rsp)         # 8-byte Spill
	movsd	%xmm2, 48(%rsp)         # 8-byte Spill
	movq	%rdi, %rbx
	movq	256(%rbx), %r14
	addq	$24, %rbx
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdi
	callq	gs_point_transform2fixed
	testl	%eax, %eax
	js	.LBB10_9
# BB#1:
	leaq	80(%rsp), %rsi
	movq	%rbx, %rdi
	movsd	48(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	56(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	gs_point_transform2fixed
	testl	%eax, %eax
	js	.LBB10_9
# BB#2:
	leaq	64(%rsp), %rsi
	movq	%rbx, %rdi
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	40(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	gs_point_transform2fixed
	testl	%eax, %eax
	js	.LBB10_9
# BB#3:
	testl	%ebp, %ebp
	je	.LBB10_8
# BB#4:
	leaq	96(%rsp), %rsi
	movq	%r14, %rdi
	callq	gx_path_current_point
	movq	16(%rsp), %rsi
	movq	24(%rsp), %rdx
	movq	%r14, %rdi
	testl	%eax, %eax
	js	.LBB10_6
# BB#5:
	callq	gx_path_add_line
	testl	%eax, %eax
	jns	.LBB10_8
	jmp	.LBB10_9
.LBB10_6:
	callq	gx_path_add_point
	testl	%eax, %eax
	js	.LBB10_9
.LBB10_8:
	movq	16(%rsp), %rsi
	movq	24(%rsp), %rdx
	movq	80(%rsp), %rcx
	movq	88(%rsp), %r8
	movq	64(%rsp), %r9
	movq	72(%rsp), %rax
	movq	%rax, (%rsp)
	movq	%r14, %rdi
	callq	gx_path_add_arc
.LBB10_9:
	addq	$112, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end10:
	.size	arc_add, .Lfunc_end10-arc_add
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI11_0:
	.quad	4553139223271571456     # double 2.44140625E-4
.LCPI11_2:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI11_3:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	gs_arcto
	.p2align	4, 0x90
	.type	gs_arcto,@function
gs_arcto:                               # @gs_arcto
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 24
	subq	$200, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 224
.Lcfi40:
	.cfi_offset %rbx, -24
.Lcfi41:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$-23, %eax
	xorps	%xmm5, %xmm5
	ucomisd	%xmm4, %xmm5
	ja	.LBB11_17
# BB#1:
	movapd	%xmm3, (%rsp)           # 16-byte Spill
	movapd	%xmm2, 16(%rsp)         # 16-byte Spill
	movsd	%xmm4, 80(%rsp)         # 8-byte Spill
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	movq	256(%rbx), %rdi
	leaq	120(%rsp), %rsi
	callq	gx_path_current_point
	testl	%eax, %eax
	js	.LBB11_17
# BB#2:                                 # %gs_currentpoint.exit
	xorps	%xmm0, %xmm0
	cvtsi2sdq	120(%rsp), %xmm0
	movsd	.LCPI11_0(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	xorps	%xmm2, %xmm2
	cvtsi2sdq	128(%rsp), %xmm2
	mulsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	cvtss2sd	%xmm1, %xmm1
	leaq	112(%rsp), %rsi
	movb	$2, %al
	movq	%rbx, %rdi
	callq	gs_itransform
	testl	%eax, %eax
	js	.LBB11_17
# BB#3:
	movss	112(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	116(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movapd	16(%rsp), %xmm2         # 16-byte Reload
	unpcklpd	(%rsp), %xmm2   # 16-byte Folded Reload
                                        # xmm2 = xmm2[0],mem[0]
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movapd	96(%rsp), %xmm8         # 16-byte Reload
	movapd	%xmm8, %xmm12
	movapd	32(%rsp), %xmm9         # 16-byte Reload
	unpcklpd	%xmm9, %xmm12   # xmm12 = xmm12[0],xmm9[0]
	subpd	%xmm12, %xmm0
	subpd	%xmm12, %xmm2
	cvtpd2ps	%xmm2, %xmm3
	cvtpd2ps	%xmm0, %xmm4
	movapd	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	movapd	%xmm4, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	cvtss2sd	%xmm1, %xmm11
	movapd	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	movapd	%xmm3, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	movaps	%xmm5, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	cvtss2sd	%xmm1, %xmm10
	movapd	%xmm3, %xmm7
	mulss	%xmm6, %xmm7
	movaps	%xmm5, %xmm0
	mulss	%xmm4, %xmm0
	subss	%xmm0, %xmm7
	movapd	%xmm10, %xmm1
	mulsd	%xmm11, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB11_5
# BB#4:                                 # %call.sqrt
	movapd	%xmm1, %xmm0
	movapd	%xmm12, 16(%rsp)        # 16-byte Spill
	movaps	%xmm4, (%rsp)           # 16-byte Spill
	movapd	%xmm3, 64(%rsp)         # 16-byte Spill
	movsd	%xmm10, 56(%rsp)        # 8-byte Spill
	movsd	%xmm11, 136(%rsp)       # 8-byte Spill
	movaps	%xmm6, 176(%rsp)        # 16-byte Spill
	movaps	%xmm7, 160(%rsp)        # 16-byte Spill
	movaps	%xmm5, 144(%rsp)        # 16-byte Spill
	callq	sqrt
	movaps	144(%rsp), %xmm5        # 16-byte Reload
	movaps	160(%rsp), %xmm7        # 16-byte Reload
	movaps	176(%rsp), %xmm6        # 16-byte Reload
	movsd	136(%rsp), %xmm11       # 8-byte Reload
                                        # xmm11 = mem[0],zero
	movsd	56(%rsp), %xmm10        # 8-byte Reload
                                        # xmm10 = mem[0],zero
	movapd	64(%rsp), %xmm3         # 16-byte Reload
	movaps	(%rsp), %xmm4           # 16-byte Reload
	movapd	16(%rsp), %xmm12        # 16-byte Reload
	movapd	96(%rsp), %xmm8         # 16-byte Reload
	movapd	32(%rsp), %xmm9         # 16-byte Reload
.LBB11_5:                               # %.split
	movaps	.LCPI11_1(%rip), %xmm1  # xmm1 = [nan,nan,nan,nan]
	andps	%xmm7, %xmm1
	xorps	%xmm2, %xmm2
	cvtss2sd	%xmm1, %xmm2
	movsd	.LCPI11_2(%rip), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm2, %xmm1
	ja	.LBB11_7
# BB#6:
	movapd	%xmm3, %xmm2
	mulss	%xmm4, %xmm2
	mulss	%xmm6, %xmm5
	addss	%xmm2, %xmm5
	xorps	%xmm2, %xmm2
	cvtss2sd	%xmm5, %xmm2
	subsd	%xmm2, %xmm0
	movapd	.LCPI11_3(%rip), %xmm2  # xmm2 = [nan,nan]
	andpd	%xmm0, %xmm2
	ucomisd	%xmm2, %xmm1
	jbe	.LBB11_10
.LBB11_7:
	leaq	24(%rbx), %rdi
	leaq	120(%rsp), %rsi
	movapd	%xmm8, %xmm0
	movapd	%xmm9, %xmm1
	callq	gs_point_transform2fixed
	testl	%eax, %eax
	js	.LBB11_9
# BB#8:
	movq	256(%rbx), %rdi
	movq	120(%rsp), %rsi
	movq	128(%rsp), %rdx
	callq	gx_path_add_line
.LBB11_9:
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm0, %xmm1
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	cvtsd2ss	%xmm0, %xmm0
	unpcklps	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1]
	unpcklps	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1]
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	testq	%r14, %r14
	jne	.LBB11_16
	jmp	.LBB11_17
.LBB11_10:
	xorps	%xmm2, %xmm2
	cvtss2sd	%xmm7, %xmm2
	mulsd	80(%rsp), %xmm2         # 8-byte Folded Reload
	divsd	%xmm0, %xmm2
	andpd	.LCPI11_3(%rip), %xmm2
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm11, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB11_12
# BB#11:                                # %call.sqrt109
	movapd	%xmm11, %xmm0
	movapd	%xmm12, 16(%rsp)        # 16-byte Spill
	movaps	%xmm4, (%rsp)           # 16-byte Spill
	movapd	%xmm3, 64(%rsp)         # 16-byte Spill
	movsd	%xmm10, 56(%rsp)        # 8-byte Spill
	movapd	%xmm2, 80(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	80(%rsp), %xmm2         # 16-byte Reload
	movsd	56(%rsp), %xmm10        # 8-byte Reload
                                        # xmm10 = mem[0],zero
	movapd	64(%rsp), %xmm3         # 16-byte Reload
	movaps	(%rsp), %xmm4           # 16-byte Reload
	movapd	16(%rsp), %xmm12        # 16-byte Reload
	movapd	96(%rsp), %xmm8         # 16-byte Reload
	movapd	32(%rsp), %xmm9         # 16-byte Reload
	movapd	%xmm0, %xmm1
.LBB11_12:                              # %.split108
	unpcklpd	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0]
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm10, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB11_14
# BB#13:                                # %call.sqrt110
	movapd	%xmm10, %xmm0
	movapd	%xmm12, 16(%rsp)        # 16-byte Spill
	movapd	%xmm4, (%rsp)           # 16-byte Spill
	movapd	%xmm2, 80(%rsp)         # 16-byte Spill
	movapd	%xmm1, 64(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	64(%rsp), %xmm1         # 16-byte Reload
	movapd	80(%rsp), %xmm2         # 16-byte Reload
	movapd	(%rsp), %xmm4           # 16-byte Reload
	movapd	16(%rsp), %xmm12        # 16-byte Reload
	movapd	96(%rsp), %xmm8         # 16-byte Reload
	movapd	32(%rsp), %xmm9         # 16-byte Reload
.LBB11_14:                              # %.split108.split
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	unpcklpd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	divpd	%xmm1, %xmm2
	cvtps2pd	%xmm4, %xmm0
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	cvtps2pd	%xmm4, %xmm1
	movapd	%xmm2, %xmm4
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	mulpd	%xmm1, %xmm2
	mulpd	%xmm0, %xmm4
	addpd	%xmm12, %xmm4
	addpd	%xmm12, %xmm2
	cvtpd2ps	%xmm2, %xmm3
	cvtpd2ps	%xmm4, %xmm1
	movapd	%xmm1, %xmm0
	unpcklpd	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0]
	movapd	%xmm0, 32(%rsp)         # 16-byte Spill
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	cvtss2sd	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
	cvtss2sd	%xmm3, %xmm2
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	cvtss2sd	%xmm3, %xmm3
	movl	$1, %esi
	movq	%rbx, %rdi
	movapd	%xmm8, %xmm4
	movapd	%xmm9, %xmm5
	callq	arc_add
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	testq	%r14, %r14
	je	.LBB11_17
.LBB11_16:
	movups	%xmm1, (%r14)
.LBB11_17:
	addq	$200, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end11:
	.size	gs_arcto, .Lfunc_end11-gs_arcto
	.cfi_endproc

	.globl	gs_curveto
	.p2align	4, 0x90
	.type	gs_curveto,@function
gs_curveto:                             # @gs_curveto
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 24
	subq	$88, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 112
.Lcfi45:
	.cfi_offset %rbx, -24
.Lcfi46:
	.cfi_offset %r14, -16
	movsd	%xmm5, 16(%rsp)         # 8-byte Spill
	movsd	%xmm4, 8(%rsp)          # 8-byte Spill
	movsd	%xmm3, 32(%rsp)         # 8-byte Spill
	movsd	%xmm2, 24(%rsp)         # 8-byte Spill
	movq	%rdi, %r14
	leaq	24(%r14), %rbx
	leaq	72(%rsp), %rsi
	movq	%rbx, %rdi
	callq	gs_point_transform2fixed
	testl	%eax, %eax
	js	.LBB12_4
# BB#1:
	leaq	56(%rsp), %rsi
	movq	%rbx, %rdi
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	32(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	gs_point_transform2fixed
	testl	%eax, %eax
	js	.LBB12_4
# BB#2:
	leaq	40(%rsp), %rsi
	movq	%rbx, %rdi
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	gs_point_transform2fixed
	testl	%eax, %eax
	js	.LBB12_4
# BB#3:
	movq	256(%r14), %rdi
	movq	72(%rsp), %rsi
	movq	80(%rsp), %rdx
	movq	56(%rsp), %rcx
	movq	64(%rsp), %r8
	movq	40(%rsp), %r9
	movq	48(%rsp), %rax
	movq	%rax, (%rsp)
	callq	gx_path_add_curve
.LBB12_4:
	addq	$88, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	gs_curveto, .Lfunc_end12-gs_curveto
	.cfi_endproc

	.globl	gs_rcurveto
	.p2align	4, 0x90
	.type	gs_rcurveto,@function
gs_rcurveto:                            # @gs_rcurveto
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 24
	subq	$120, %rsp
.Lcfi49:
	.cfi_def_cfa_offset 144
.Lcfi50:
	.cfi_offset %rbx, -24
.Lcfi51:
	.cfi_offset %r14, -16
	movsd	%xmm5, 16(%rsp)         # 8-byte Spill
	movsd	%xmm4, 8(%rsp)          # 8-byte Spill
	movsd	%xmm3, 32(%rsp)         # 8-byte Spill
	movsd	%xmm2, 24(%rsp)         # 8-byte Spill
	movsd	%xmm1, 48(%rsp)         # 8-byte Spill
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	movq	%rdi, %rbx
	movq	256(%rbx), %rdi
	leaq	104(%rsp), %rsi
	callq	gx_path_current_point
	testl	%eax, %eax
	js	.LBB13_5
# BB#1:
	leaq	24(%rbx), %r14
	leaq	88(%rsp), %rsi
	movq	%r14, %rdi
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	48(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	gs_distance_transform2fixed
	testl	%eax, %eax
	js	.LBB13_5
# BB#2:
	leaq	72(%rsp), %rsi
	movq	%r14, %rdi
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	32(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	gs_distance_transform2fixed
	testl	%eax, %eax
	js	.LBB13_5
# BB#3:
	leaq	56(%rsp), %rsi
	movq	%r14, %rdi
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	gs_distance_transform2fixed
	testl	%eax, %eax
	js	.LBB13_5
# BB#4:
	movq	256(%rbx), %rdi
	movq	104(%rsp), %r9
	movq	112(%rsp), %rax
	movq	88(%rsp), %rsi
	addq	%r9, %rsi
	movq	96(%rsp), %rdx
	addq	%rax, %rdx
	movq	72(%rsp), %rcx
	addq	%r9, %rcx
	movq	80(%rsp), %r8
	addq	%rax, %r8
	addq	56(%rsp), %r9
	addq	64(%rsp), %rax
	movq	%rax, (%rsp)
	callq	gx_path_add_curve
.LBB13_5:
	addq	$120, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end13:
	.size	gs_rcurveto, .Lfunc_end13-gs_rcurveto
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
