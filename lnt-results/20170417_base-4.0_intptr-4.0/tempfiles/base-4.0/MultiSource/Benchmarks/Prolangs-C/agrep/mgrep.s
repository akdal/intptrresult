	.text
	.file	"mgrep.bc"
	.globl	countline
	.p2align	4, 0x90
	.type	countline,@function
countline:                              # @countline
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB0_13
# BB#1:                                 # %.lr.ph.preheader
	movl	%esi, %eax
	testb	$1, %al
	jne	.LBB0_3
# BB#2:
	xorl	%ecx, %ecx
	cmpl	$1, %esi
	jne	.LBB0_7
	jmp	.LBB0_13
.LBB0_3:                                # %.lr.ph.prol
	cmpb	$10, (%rdi)
	jne	.LBB0_5
# BB#4:
	incl	total_line(%rip)
.LBB0_5:                                # %.lr.ph.prol.loopexit
	movl	$1, %ecx
	cmpl	$1, %esi
	je	.LBB0_13
.LBB0_7:                                # %.lr.ph.preheader.new
	subq	%rcx, %rax
	leaq	1(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$10, -1(%rcx)
	jne	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=1
	incl	total_line(%rip)
.LBB0_10:                               # %.lr.ph.16
                                        #   in Loop: Header=BB0_8 Depth=1
	cmpb	$10, (%rcx)
	jne	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_8 Depth=1
	incl	total_line(%rip)
.LBB0_12:                               #   in Loop: Header=BB0_8 Depth=1
	addq	$2, %rcx
	addq	$-2, %rax
	jne	.LBB0_8
.LBB0_13:                               # %._crit_edge
	retq
.Lfunc_end0:
	.size	countline, .Lfunc_end0-countline
	.cfi_endproc

	.globl	m_short
	.p2align	4, 0x90
	.type	m_short,@function
m_short:                                # @m_short
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movslq	%edx, %r15
	addq	%rdi, %r15
	movslq	%esi, %rax
	leaq	(%rdi,%rax), %r12
	leaq	1(%rdi,%rax), %r14
	cmpq	%r15, %r12
	jbe	.LBB1_2
# BB#1:
	movq	%r14, %r12
.LBB1_34:                               # %.thread.outer._crit_edge
	cmpl	$0, INVERSE(%rip)
	je	.LBB1_38
.LBB1_35:                               # %.thread.outer._crit_edge
	movl	COUNT(%rip), %eax
	testl	%eax, %eax
	je	.LBB1_37
	jmp	.LBB1_38
	.p2align	4, 0x90
.LBB1_36:                               # %.lr.ph
                                        #   in Loop: Header=BB1_37 Depth=1
	movzbl	(%r12), %edi
	incq	%r12
	movq	stdout(%rip), %rsi
	callq	_IO_putc
.LBB1_37:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r15, %r12
	jbe	.LBB1_36
	jmp	.LBB1_38
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph100
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_3 Depth 2
                                        #       Child Loop BB1_7 Depth 3
                                        #         Child Loop BB1_9 Depth 4
                                        #         Child Loop BB1_15 Depth 4
                                        #       Child Loop BB1_21 Depth 3
                                        #       Child Loop BB1_23 Depth 3
                                        #     Child Loop BB1_26 Depth 2
                                        #     Child Loop BB1_29 Depth 2
                                        #     Child Loop BB1_32 Depth 2
	movq	%r12, %rbx
.LBB1_3:                                #   Parent Loop BB1_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_7 Depth 3
                                        #         Child Loop BB1_9 Depth 4
                                        #         Child Loop BB1_15 Depth 4
                                        #       Child Loop BB1_21 Depth 3
                                        #       Child Loop BB1_23 Depth 3
	movzbl	(%rbx), %r8d
	movq	HASH(,%r8,8), %rax
	testq	%rax, %rax
	jne	.LBB1_7
	jmp	.LBB1_5
	.p2align	4, 0x90
.LBB1_16:                               #   in Loop: Header=BB1_7 Depth=3
	movb	$10, %r8b
.LBB1_4:                                #   in Loop: Header=BB1_7 Depth=3
	testq	%rax, %rax
	je	.LBB1_5
.LBB1_7:                                # %.preheader83
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_3 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_9 Depth 4
                                        #         Child Loop BB1_15 Depth 4
	movslq	(%rax), %r9
	movq	8(%rax), %rax
	movq	patt(,%r9,8), %rsi
	movzbl	(%rsi), %edi
	movb	tr(%rdi), %cl
	movzbl	%r8b, %edx
	xorl	%edi, %edi
	cmpb	tr(%rdx), %cl
	jne	.LBB1_10
# BB#8:                                 # %.lr.ph95.preheader
                                        #   in Loop: Header=BB1_7 Depth=3
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph95
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_3 Depth=2
                                        #       Parent Loop BB1_7 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	1(%rsi,%rdi), %ecx
	movzbl	tr(%rcx), %ecx
	movzbl	1(%rbx,%rdi), %edx
	incq	%rdi
	cmpb	tr(%rdx), %cl
	je	.LBB1_9
.LBB1_10:                               # %._crit_edge
                                        #   in Loop: Header=BB1_7 Depth=3
	movzbl	pat_len(%r9), %ecx
	cmpl	%edi, %ecx
	jg	.LBB1_4
# BB#11:                                #   in Loop: Header=BB1_7 Depth=3
	cmpq	%r15, %rbx
	jae	.LBB1_38
# BB#12:                                #   in Loop: Header=BB1_7 Depth=3
	incl	num_of_matched(%rip)
	movl	SILENT(%rip), %ecx
	orl	FILENAMEONLY(%rip), %ecx
	jne	.LBB1_38
# BB#13:                                #   in Loop: Header=BB1_7 Depth=3
	cmpl	$0, COUNT(%rip)
	je	.LBB1_17
# BB#14:                                # %.preheader77.preheader
                                        #   in Loop: Header=BB1_7 Depth=3
	decq	%rbx
	.p2align	4, 0x90
.LBB1_15:                               # %.preheader77
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_3 Depth=2
                                        #       Parent Loop BB1_7 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpb	$10, 1(%rbx)
	leaq	1(%rbx), %rbx
	jne	.LBB1_15
	jmp	.LBB1_16
.LBB1_17:                               #   in Loop: Header=BB1_3 Depth=2
	cmpl	$0, FNAME(%rip)
	je	.LBB1_19
# BB#18:                                #   in Loop: Header=BB1_3 Depth=2
	movl	$.L.str, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	callq	printf
.LBB1_19:                               #   in Loop: Header=BB1_3 Depth=2
	cmpl	$0, INVERSE(%rip)
	jne	.LBB1_26
# BB#20:                                # %.preheader80.preheader
                                        #   in Loop: Header=BB1_3 Depth=2
	incq	%rbx
	.p2align	4, 0x90
.LBB1_21:                               # %.preheader80
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$10, -2(%rbx)
	leaq	-1(%rbx), %rbx
	jne	.LBB1_21
# BB#22:                                # %.preheader79
                                        #   in Loop: Header=BB1_3 Depth=2
	movb	(%rbx), %al
	cmpb	$10, %al
	je	.LBB1_25
	.p2align	4, 0x90
.LBB1_23:                               # %.lr.ph97
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	%al, %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	movzbl	1(%rbx), %eax
	incq	%rbx
	cmpb	$10, %al
	jne	.LBB1_23
.LBB1_25:                               # %._crit_edge98
                                        #   in Loop: Header=BB1_3 Depth=2
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	.p2align	4, 0x90
.LBB1_5:                                # %.thread.backedge
                                        #   in Loop: Header=BB1_3 Depth=2
	incq	%rbx
	cmpq	%r15, %rbx
	jbe	.LBB1_3
	jmp	.LBB1_6
	.p2align	4, 0x90
.LBB1_26:                               # %.preheader82
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %r12
	leaq	-1(%r12), %rbx
	cmpb	$10, -1(%r12)
	jne	.LBB1_26
# BB#27:                                #   in Loop: Header=BB1_2 Depth=1
	cmpq	%rbx, %r14
	jae	.LBB1_32
# BB#28:                                # %.lr.ph105.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	leaq	1(%r14), %r13
.LBB1_29:                               # %.lr.ph105
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%r13), %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	incq	%r13
	cmpq	%r13, %r12
	jne	.LBB1_29
# BB#30:                                # %._crit_edge106
                                        #   in Loop: Header=BB1_2 Depth=1
	cmpq	%rbx, %r14
	jae	.LBB1_32
# BB#31:                                #   in Loop: Header=BB1_2 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	.p2align	4, 0x90
.LBB1_32:                               # %.preheader81
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$10, (%r12)
	leaq	1(%r12), %r12
	jne	.LBB1_32
# BB#33:                                # %.thread.outer
                                        #   in Loop: Header=BB1_2 Depth=1
	cmpq	%r15, %r12
	movq	%r12, %r14
	jbe	.LBB1_2
	jmp	.LBB1_34
.LBB1_6:
	movq	%r14, %r12
	cmpl	$0, INVERSE(%rip)
	jne	.LBB1_35
.LBB1_38:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	m_short, .Lfunc_end1-m_short
	.cfi_endproc

	.globl	f_prep
	.p2align	4, 0x90
	.type	f_prep,@function
f_prep:                                 # @f_prep
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movslq	p_size(%rip), %rbp
	leaq	-1(%rbp), %rcx
	movslq	LONG(%rip), %rdx
	cmpl	%edx, %ecx
	jle	.LBB2_6
# BB#1:                                 # %.lr.ph43.preheader
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph43
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rsi,%rcx), %ebx
	movzbl	(%rsi,%rcx), %eax
	shlb	$4, %al
	andb	$15, %bl
	orb	%al, %bl
	testl	%edx, %edx
	movzbl	%bl, %ebx
	je	.LBB2_4
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	shll	$4, %ebx
	movzbl	-2(%rsi,%rcx), %eax
	andl	$15, %eax
	orl	%eax, %ebx
.LBB2_4:                                #   in Loop: Header=BB2_2 Depth=1
	decq	%rcx
	movl	%ebx, %ebx
	movzbl	SHIFT1(%rbx), %eax
	cmpq	%rdi, %rax
	jl	.LBB2_5
# BB#15:                                #   in Loop: Header=BB2_2 Depth=1
	movb	%dil, SHIFT1(%rbx)
.LBB2_5:                                # %.backedge
                                        #   in Loop: Header=BB2_2 Depth=1
	incq	%rdi
	cmpq	%rdx, %rcx
	jg	.LBB2_2
.LBB2_6:                                # %._crit_edge44
	cmpl	$0, SHORT(%rip)
	movl	$15, %eax
	movl	$255, %ecx
	cmovel	%eax, %ecx
	xorl	%ebx, %ebx
	testl	%ebp, %ebp
	jle	.LBB2_7
# BB#8:                                 # %.lr.ph.preheader
	testb	$1, %bpl
	movq	%rbp, %rdx
	je	.LBB2_10
# BB#9:                                 # %.lr.ph.prol
	leaq	-1(%rbp), %rdx
	movzbl	-1(%rsi,%rbp), %eax
	movzbl	tr(%rax), %ebx
	andl	%ecx, %ebx
.LBB2_10:                               # %.lr.ph.prol.loopexit
	cmpl	$1, %ebp
	je	.LBB2_13
# BB#11:                                # %.lr.ph.preheader.new
	addq	$2, %rdx
	.p2align	4, 0x90
.LBB2_12:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	shll	$4, %ebx
	movzbl	-3(%rsi,%rdx), %eax
	movzbl	tr(%rax), %eax
	andl	%ecx, %eax
	addl	%ebx, %eax
	shll	$4, %eax
	movzbl	-4(%rsi,%rdx), %edi
	movzbl	tr(%rdi), %ebx
	andl	%ecx, %ebx
	addl	%eax, %ebx
	addq	$-2, %rdx
	cmpq	$2, %rdx
	jg	.LBB2_12
.LBB2_13:                               # %._crit_edge.loopexit
	andl	$8191, %ebx             # imm = 0x1FFF
	jmp	.LBB2_14
.LBB2_7:
	xorl	%ebx, %ebx
.LBB2_14:                               # %._crit_edge
	movl	$16, %edi
	callq	malloc
	movq	%rax, qt(%rip)
	movl	%r14d, (%rax)
	movq	HASH(,%rbx,8), %rcx
	movq	%rcx, pt(%rip)
	movq	%rcx, 8(%rax)
	movq	%rax, HASH(,%rbx,8)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	f_prep, .Lfunc_end2-f_prep
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	9                       # 0x9
	.byte	10                      # 0xa
	.byte	11                      # 0xb
	.byte	12                      # 0xc
	.byte	13                      # 0xd
	.byte	14                      # 0xe
	.byte	15                      # 0xf
.LCPI3_1:
	.byte	16                      # 0x10
	.byte	17                      # 0x11
	.byte	18                      # 0x12
	.byte	19                      # 0x13
	.byte	20                      # 0x14
	.byte	21                      # 0x15
	.byte	22                      # 0x16
	.byte	23                      # 0x17
	.byte	24                      # 0x18
	.byte	25                      # 0x19
	.byte	26                      # 0x1a
	.byte	27                      # 0x1b
	.byte	28                      # 0x1c
	.byte	29                      # 0x1d
	.byte	30                      # 0x1e
	.byte	31                      # 0x1f
.LCPI3_2:
	.byte	32                      # 0x20
	.byte	33                      # 0x21
	.byte	34                      # 0x22
	.byte	35                      # 0x23
	.byte	36                      # 0x24
	.byte	37                      # 0x25
	.byte	38                      # 0x26
	.byte	39                      # 0x27
	.byte	40                      # 0x28
	.byte	41                      # 0x29
	.byte	42                      # 0x2a
	.byte	43                      # 0x2b
	.byte	44                      # 0x2c
	.byte	45                      # 0x2d
	.byte	46                      # 0x2e
	.byte	47                      # 0x2f
.LCPI3_3:
	.byte	48                      # 0x30
	.byte	49                      # 0x31
	.byte	50                      # 0x32
	.byte	51                      # 0x33
	.byte	52                      # 0x34
	.byte	53                      # 0x35
	.byte	54                      # 0x36
	.byte	55                      # 0x37
	.byte	56                      # 0x38
	.byte	57                      # 0x39
	.byte	58                      # 0x3a
	.byte	59                      # 0x3b
	.byte	60                      # 0x3c
	.byte	61                      # 0x3d
	.byte	62                      # 0x3e
	.byte	63                      # 0x3f
.LCPI3_4:
	.byte	64                      # 0x40
	.byte	65                      # 0x41
	.byte	66                      # 0x42
	.byte	67                      # 0x43
	.byte	68                      # 0x44
	.byte	69                      # 0x45
	.byte	70                      # 0x46
	.byte	71                      # 0x47
	.byte	72                      # 0x48
	.byte	73                      # 0x49
	.byte	74                      # 0x4a
	.byte	75                      # 0x4b
	.byte	76                      # 0x4c
	.byte	77                      # 0x4d
	.byte	78                      # 0x4e
	.byte	79                      # 0x4f
.LCPI3_5:
	.byte	80                      # 0x50
	.byte	81                      # 0x51
	.byte	82                      # 0x52
	.byte	83                      # 0x53
	.byte	84                      # 0x54
	.byte	85                      # 0x55
	.byte	86                      # 0x56
	.byte	87                      # 0x57
	.byte	88                      # 0x58
	.byte	89                      # 0x59
	.byte	90                      # 0x5a
	.byte	91                      # 0x5b
	.byte	92                      # 0x5c
	.byte	93                      # 0x5d
	.byte	94                      # 0x5e
	.byte	95                      # 0x5f
.LCPI3_6:
	.byte	96                      # 0x60
	.byte	97                      # 0x61
	.byte	98                      # 0x62
	.byte	99                      # 0x63
	.byte	100                     # 0x64
	.byte	101                     # 0x65
	.byte	102                     # 0x66
	.byte	103                     # 0x67
	.byte	104                     # 0x68
	.byte	105                     # 0x69
	.byte	106                     # 0x6a
	.byte	107                     # 0x6b
	.byte	108                     # 0x6c
	.byte	109                     # 0x6d
	.byte	110                     # 0x6e
	.byte	111                     # 0x6f
.LCPI3_7:
	.byte	112                     # 0x70
	.byte	113                     # 0x71
	.byte	114                     # 0x72
	.byte	115                     # 0x73
	.byte	116                     # 0x74
	.byte	117                     # 0x75
	.byte	118                     # 0x76
	.byte	119                     # 0x77
	.byte	120                     # 0x78
	.byte	121                     # 0x79
	.byte	122                     # 0x7a
	.byte	123                     # 0x7b
	.byte	124                     # 0x7c
	.byte	125                     # 0x7d
	.byte	126                     # 0x7e
	.byte	127                     # 0x7f
.LCPI3_8:
	.byte	128                     # 0x80
	.byte	129                     # 0x81
	.byte	130                     # 0x82
	.byte	131                     # 0x83
	.byte	132                     # 0x84
	.byte	133                     # 0x85
	.byte	134                     # 0x86
	.byte	135                     # 0x87
	.byte	136                     # 0x88
	.byte	137                     # 0x89
	.byte	138                     # 0x8a
	.byte	139                     # 0x8b
	.byte	140                     # 0x8c
	.byte	141                     # 0x8d
	.byte	142                     # 0x8e
	.byte	143                     # 0x8f
.LCPI3_9:
	.byte	144                     # 0x90
	.byte	145                     # 0x91
	.byte	146                     # 0x92
	.byte	147                     # 0x93
	.byte	148                     # 0x94
	.byte	149                     # 0x95
	.byte	150                     # 0x96
	.byte	151                     # 0x97
	.byte	152                     # 0x98
	.byte	153                     # 0x99
	.byte	154                     # 0x9a
	.byte	155                     # 0x9b
	.byte	156                     # 0x9c
	.byte	157                     # 0x9d
	.byte	158                     # 0x9e
	.byte	159                     # 0x9f
.LCPI3_10:
	.byte	160                     # 0xa0
	.byte	161                     # 0xa1
	.byte	162                     # 0xa2
	.byte	163                     # 0xa3
	.byte	164                     # 0xa4
	.byte	165                     # 0xa5
	.byte	166                     # 0xa6
	.byte	167                     # 0xa7
	.byte	168                     # 0xa8
	.byte	169                     # 0xa9
	.byte	170                     # 0xaa
	.byte	171                     # 0xab
	.byte	172                     # 0xac
	.byte	173                     # 0xad
	.byte	174                     # 0xae
	.byte	175                     # 0xaf
.LCPI3_11:
	.byte	176                     # 0xb0
	.byte	177                     # 0xb1
	.byte	178                     # 0xb2
	.byte	179                     # 0xb3
	.byte	180                     # 0xb4
	.byte	181                     # 0xb5
	.byte	182                     # 0xb6
	.byte	183                     # 0xb7
	.byte	184                     # 0xb8
	.byte	185                     # 0xb9
	.byte	186                     # 0xba
	.byte	187                     # 0xbb
	.byte	188                     # 0xbc
	.byte	189                     # 0xbd
	.byte	190                     # 0xbe
	.byte	191                     # 0xbf
.LCPI3_12:
	.byte	192                     # 0xc0
	.byte	193                     # 0xc1
	.byte	194                     # 0xc2
	.byte	195                     # 0xc3
	.byte	196                     # 0xc4
	.byte	197                     # 0xc5
	.byte	198                     # 0xc6
	.byte	199                     # 0xc7
	.byte	200                     # 0xc8
	.byte	201                     # 0xc9
	.byte	202                     # 0xca
	.byte	203                     # 0xcb
	.byte	204                     # 0xcc
	.byte	205                     # 0xcd
	.byte	206                     # 0xce
	.byte	207                     # 0xcf
.LCPI3_13:
	.byte	208                     # 0xd0
	.byte	209                     # 0xd1
	.byte	210                     # 0xd2
	.byte	211                     # 0xd3
	.byte	212                     # 0xd4
	.byte	213                     # 0xd5
	.byte	214                     # 0xd6
	.byte	215                     # 0xd7
	.byte	216                     # 0xd8
	.byte	217                     # 0xd9
	.byte	218                     # 0xda
	.byte	219                     # 0xdb
	.byte	220                     # 0xdc
	.byte	221                     # 0xdd
	.byte	222                     # 0xde
	.byte	223                     # 0xdf
.LCPI3_14:
	.byte	224                     # 0xe0
	.byte	225                     # 0xe1
	.byte	226                     # 0xe2
	.byte	227                     # 0xe3
	.byte	228                     # 0xe4
	.byte	229                     # 0xe5
	.byte	230                     # 0xe6
	.byte	231                     # 0xe7
	.byte	232                     # 0xe8
	.byte	233                     # 0xe9
	.byte	234                     # 0xea
	.byte	235                     # 0xeb
	.byte	236                     # 0xec
	.byte	237                     # 0xed
	.byte	238                     # 0xee
	.byte	239                     # 0xef
.LCPI3_15:
	.byte	240                     # 0xf0
	.byte	241                     # 0xf1
	.byte	242                     # 0xf2
	.byte	243                     # 0xf3
	.byte	244                     # 0xf4
	.byte	245                     # 0xf5
	.byte	246                     # 0xf6
	.byte	247                     # 0xf7
	.byte	248                     # 0xf8
	.byte	249                     # 0xf9
	.byte	250                     # 0xfa
	.byte	251                     # 0xfb
	.byte	252                     # 0xfc
	.byte	253                     # 0xfd
	.byte	254                     # 0xfe
	.byte	255                     # 0xff
.LCPI3_16:
	.byte	97                      # 0x61
	.byte	98                      # 0x62
	.byte	99                      # 0x63
	.byte	100                     # 0x64
	.byte	101                     # 0x65
	.byte	102                     # 0x66
	.byte	103                     # 0x67
	.byte	104                     # 0x68
	.byte	105                     # 0x69
	.byte	106                     # 0x6a
	.byte	107                     # 0x6b
	.byte	108                     # 0x6c
	.byte	109                     # 0x6d
	.byte	110                     # 0x6e
	.byte	111                     # 0x6f
	.byte	112                     # 0x70
.LCPI3_17:
	.zero	16,15
	.text
	.globl	prepf
	.p2align	4, 0x90
	.type	prepf,@function
prepf:                                  # @prepf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 48
.Lcfi21:
	.cfi_offset %rbx, -48
.Lcfi22:
	.cfi_offset %r12, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movslq	%r14d, %rax
	leaq	buf(%rax), %rbx
	movl	$8192, %edx             # imm = 0x2000
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	read
	testl	%eax, %eax
	jle	.LBB3_5
# BB#2:                                 #   in Loop: Header=BB3_1 Depth=1
	addl	%r14d, %eax
	cmpl	$260001, %eax           # imm = 0x3F7A1
	movl	%eax, %r14d
	jl	.LBB3_1
# BB#3:
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	movl	$Progname, %edx
	movl	$260000, %ecx           # imm = 0x3F7A0
.LBB3_4:
	xorl	%eax, %eax
	callq	fprintf
	movl	$2, %edi
	callq	exit
.LBB3_5:
	movb	$10, (%rbx)
	movl	$pat_spool, %eax
	testl	%r14d, %r14d
	jle	.LBB3_6
# BB#7:                                 # %.lr.ph112.preheader
	xorl	%ecx, %ecx
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB3_8:                                # %.lr.ph112
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_13 Depth 2
	movq	%rax, patt(,%rbp,8)
	cmpl	$0, WORDBOUND(%rip)
	je	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_8 Depth=1
	movb	$-128, (%rax)
	incq	%rax
.LBB3_10:                               #   in Loop: Header=BB3_8 Depth=1
	cmpl	$0, WHOLELINE(%rip)
	je	.LBB3_12
# BB#11:                                #   in Loop: Header=BB3_8 Depth=1
	movb	$10, (%rax)
	incq	%rax
.LBB3_12:                               # %.preheader91.preheader
                                        #   in Loop: Header=BB3_8 Depth=1
	movslq	%ecx, %rcx
	.p2align	4, 0x90
.LBB3_13:                               # %.preheader91
                                        #   Parent Loop BB3_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	buf(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	cmpb	$10, %dl
	jne	.LBB3_13
# BB#14:                                #   in Loop: Header=BB3_8 Depth=1
	leaq	-1(%rax), %rdx
	cmpl	$0, WORDBOUND(%rip)
	je	.LBB3_15
# BB#16:                                #   in Loop: Header=BB3_8 Depth=1
	movb	$-128, (%rdx)
	cmpl	$0, WHOLELINE(%rip)
	jne	.LBB3_18
	jmp	.LBB3_19
	.p2align	4, 0x90
.LBB3_15:                               #   in Loop: Header=BB3_8 Depth=1
	movq	%rdx, %rax
	cmpl	$0, WHOLELINE(%rip)
	je	.LBB3_19
.LBB3_18:                               #   in Loop: Header=BB3_8 Depth=1
	movb	$10, (%rax)
	incq	%rax
.LBB3_19:                               #   in Loop: Header=BB3_8 Depth=1
	movb	$0, (%rax)
	incq	%rax
	incq	%rbp
	cmpl	%r14d, %ecx
	jl	.LBB3_8
# BB#20:                                # %._crit_edge113
	cmpl	$30000, %ebp            # imm = 0x7530
	jle	.LBB3_21
# BB#47:
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
	movl	$Progname, %edx
	movl	$30000, %ecx            # imm = 0x7530
	jmp	.LBB3_4
.LBB3_6:
	movl	$1, %ebp
.LBB3_21:                               # %vector.body
	movb	$19, (%rax)
	movaps	.LCPI3_0(%rip), %xmm0   # xmm0 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
	movaps	%xmm0, tr(%rip)
	movaps	.LCPI3_1(%rip), %xmm0   # xmm0 = [16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
	movaps	%xmm0, tr+16(%rip)
	movaps	.LCPI3_2(%rip), %xmm0   # xmm0 = [32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47]
	movaps	%xmm0, tr+32(%rip)
	movaps	.LCPI3_3(%rip), %xmm0   # xmm0 = [48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63]
	movaps	%xmm0, tr+48(%rip)
	movaps	.LCPI3_4(%rip), %xmm0   # xmm0 = [64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79]
	movaps	%xmm0, tr+64(%rip)
	movaps	.LCPI3_5(%rip), %xmm0   # xmm0 = [80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95]
	movaps	%xmm0, tr+80(%rip)
	movaps	.LCPI3_6(%rip), %xmm0   # xmm0 = [96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111]
	movaps	%xmm0, tr+96(%rip)
	movaps	.LCPI3_7(%rip), %xmm0   # xmm0 = [112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127]
	movaps	%xmm0, tr+112(%rip)
	movaps	.LCPI3_8(%rip), %xmm0   # xmm0 = [128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143]
	movaps	%xmm0, tr+128(%rip)
	movaps	.LCPI3_9(%rip), %xmm0   # xmm0 = [144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159]
	movaps	%xmm0, tr+144(%rip)
	movaps	.LCPI3_10(%rip), %xmm0  # xmm0 = [160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175]
	movaps	%xmm0, tr+160(%rip)
	movaps	.LCPI3_11(%rip), %xmm0  # xmm0 = [176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191]
	movaps	%xmm0, tr+176(%rip)
	movaps	.LCPI3_12(%rip), %xmm0  # xmm0 = [192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207]
	movaps	%xmm0, tr+192(%rip)
	movaps	.LCPI3_13(%rip), %xmm0  # xmm0 = [208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223]
	movaps	%xmm0, tr+208(%rip)
	movaps	.LCPI3_14(%rip), %xmm0  # xmm0 = [224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239]
	movaps	%xmm0, tr+224(%rip)
	movaps	.LCPI3_15(%rip), %xmm0  # xmm0 = [240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255]
	movaps	%xmm0, tr+240(%rip)
	cmpl	$0, NOUPPER(%rip)
	je	.LBB3_23
# BB#22:                                # %.preheader88.preheader
	movaps	.LCPI3_16(%rip), %xmm0  # xmm0 = [97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112]
	movups	%xmm0, tr+65(%rip)
	movabsq	$8680537053616894577, %rax # imm = 0x7877767574737271
	movq	%rax, tr+81(%rip)
	movw	$31353, tr+89(%rip)     # imm = 0x7A79
.LBB3_23:                               # %.loopexit
	cmpl	$0, WORDBOUND(%rip)
	je	.LBB3_30
# BB#24:                                # %.preheader87
	xorl	%ebx, %ebx
	callq	__ctype_b_loc
	.p2align	4, 0x90
.LBB3_25:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	testb	$8, (%rcx,%rbx,2)
	jne	.LBB3_27
# BB#26:                                #   in Loop: Header=BB3_25 Depth=1
	movb	$-128, tr(%rbx)
.LBB3_27:                               #   in Loop: Header=BB3_25 Depth=1
	movq	(%rax), %rcx
	testb	$8, 2(%rcx,%rbx,2)
	jne	.LBB3_29
# BB#28:                                #   in Loop: Header=BB3_25 Depth=1
	movb	$-128, tr+1(%rbx)
.LBB3_29:                               #   in Loop: Header=BB3_25 Depth=1
	addq	$2, %rbx
	cmpq	$128, %rbx
	jne	.LBB3_25
.LBB3_30:                               # %vector.body165
	movaps	.LCPI3_17(%rip), %xmm0  # xmm0 = [15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15]
	movaps	tr(%rip), %xmm1
	andps	%xmm0, %xmm1
	movaps	tr+16(%rip), %xmm2
	andps	%xmm0, %xmm2
	movaps	%xmm1, tr1(%rip)
	movaps	%xmm2, tr1+16(%rip)
	movaps	tr+32(%rip), %xmm1
	andps	%xmm0, %xmm1
	movaps	tr+48(%rip), %xmm2
	andps	%xmm0, %xmm2
	movaps	%xmm1, tr1+32(%rip)
	movaps	%xmm2, tr1+48(%rip)
	movaps	tr+64(%rip), %xmm1
	andps	%xmm0, %xmm1
	movaps	tr+80(%rip), %xmm2
	andps	%xmm0, %xmm2
	movaps	%xmm1, tr1+64(%rip)
	movaps	%xmm2, tr1+80(%rip)
	movaps	tr+96(%rip), %xmm1
	andps	%xmm0, %xmm1
	movaps	tr+112(%rip), %xmm2
	andps	%xmm0, %xmm2
	movaps	%xmm1, tr1+96(%rip)
	movaps	%xmm2, tr1+112(%rip)
	movaps	tr+128(%rip), %xmm1
	andps	%xmm0, %xmm1
	movaps	tr+144(%rip), %xmm2
	andps	%xmm0, %xmm2
	movaps	%xmm1, tr1+128(%rip)
	movaps	%xmm2, tr1+144(%rip)
	movaps	tr+160(%rip), %xmm1
	andps	%xmm0, %xmm1
	movaps	tr+176(%rip), %xmm2
	andps	%xmm0, %xmm2
	movaps	%xmm1, tr1+160(%rip)
	movaps	%xmm2, tr1+176(%rip)
	movaps	tr+192(%rip), %xmm1
	andps	%xmm0, %xmm1
	movaps	tr+208(%rip), %xmm2
	andps	%xmm0, %xmm2
	movaps	%xmm1, tr1+192(%rip)
	movaps	%xmm2, tr1+208(%rip)
	movaps	tr+224(%rip), %xmm1
	andps	%xmm0, %xmm1
	andps	tr+240(%rip), %xmm0
	movaps	%xmm1, tr1+224(%rip)
	movaps	%xmm0, tr1+240(%rip)
	movl	$256, p_size(%rip)      # imm = 0x100
	movl	$256, %r12d             # imm = 0x100
	cmpl	$2, %ebp
	jl	.LBB3_37
# BB#31:                                # %.lr.ph100.preheader
	movl	%ebp, %r15d
	movl	$256, %r12d             # imm = 0x100
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB3_32:                               # %.lr.ph100
                                        # =>This Inner Loop Header: Depth=1
	movq	patt(,%rbx,8), %rdi
	callq	strlen
	movb	%al, pat_len(%rbx)
	testl	%eax, %eax
	je	.LBB3_35
# BB#33:                                # %.lr.ph100
                                        #   in Loop: Header=BB3_32 Depth=1
	cmpl	%r12d, %eax
	jge	.LBB3_35
# BB#34:                                #   in Loop: Header=BB3_32 Depth=1
	movl	%eax, p_size(%rip)
	movl	%eax, %r12d
.LBB3_35:                               #   in Loop: Header=BB3_32 Depth=1
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB3_32
# BB#36:                                # %._crit_edge101
	testl	%r12d, %r12d
	je	.LBB3_48
.LBB3_37:                               # %._crit_edge101.thread
	cmpl	$401, %r14d             # imm = 0x191
	jl	.LBB3_40
# BB#38:                                # %._crit_edge101.thread
	cmpl	$2, %r12d
	jle	.LBB3_40
# BB#39:
	movl	$LONG, %eax
	jmp	.LBB3_42
.LBB3_40:
	cmpl	$1, %r12d
	jne	.LBB3_43
# BB#41:
	movl	$SHORT, %eax
.LBB3_42:                               # %.preheader85.sink.split
	movl	$1, (%rax)
.LBB3_43:                               # %.preheader85
	addl	$254, %r12d
	movl	$SHIFT1, %edi
	movl	$4096, %edx             # imm = 0x1000
	movl	%r12d, %esi
	callq	memset
	movl	$HASH, %edi
	xorl	%esi, %esi
	movl	$65536, %edx            # imm = 0x10000
	callq	memset
	cmpl	$2, %ebp
	jl	.LBB3_46
# BB#44:                                # %.lr.ph.preheader
	movl	%ebp, %ebp
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB3_45:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	patt(,%rbx,8), %rsi
	movl	%ebx, %edi
	callq	f_prep
	incq	%rbx
	cmpq	%rbx, %rbp
	jne	.LBB3_45
.LBB3_46:                               # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_48:
	movq	stderr(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$2, %edi
	callq	exit
.Lfunc_end3:
	.size	prepf, .Lfunc_end3-prepf
	.cfi_endproc

	.globl	monkey1
	.p2align	4, 0x90
	.type	monkey1,@function
monkey1:                                # @monkey1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 96
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movslq	p_size(%rip), %r10
	movslq	%edx, %r14
	addq	%rdi, %r14
	movslq	%esi, %rax
	leaq	(%rdi,%rax), %rcx
	leaq	1(%rdi,%rax), %r12
	leaq	-1(%r10,%rcx), %r13
	cmpq	%r14, %r13
	ja	.LBB4_41
# BB#1:                                 # %.lr.ph148
	movq	%r10, %r15
	decq	%r15
	movl	LONG(%rip), %r11d
	movl	$1, %eax
	subq	%r15, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r15, %rax
	negq	%rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB4_2
	.p2align	4, 0x90
.LBB4_5:                                # %.preheader124.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_6:                                # %.preheader124
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %edx
	shll	$4, %edx
	movl	%eax, %ecx
	movq	%r13, %rsi
	subq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	movzbl	tr1(%rcx), %ecx
	addl	%edx, %ecx
	incl	%eax
	cmpl	%r15d, %eax
	jbe	.LBB4_6
# BB#7:                                 #   in Loop: Header=BB4_2 Depth=1
	andl	$8191, %ecx             # imm = 0x1FFF
	movq	HASH(,%rcx,8), %rcx
	movb	$1, %r8b
	testq	%rcx, %rcx
	je	.LBB4_40
# BB#8:                                 # %.preheader122
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movzbl	(%r13,%rax), %eax
	movb	tr(%rax), %r9b
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%r13,%rax), %rsi
	.p2align	4, 0x90
.LBB4_9:                                #   Parent Loop BB4_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_11 Depth 3
	movslq	(%rcx), %rdi
	movq	8(%rcx), %rcx
	movq	patt(,%rdi,8), %rax
	movzbl	(%rax), %ebx
	xorl	%ebp, %ebp
	cmpb	%r9b, tr(%rbx)
	jne	.LBB4_12
# BB#10:                                # %.lr.ph136.preheader
                                        #   in Loop: Header=BB4_9 Depth=2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_11:                               # %.lr.ph136
                                        #   Parent Loop BB4_2 Depth=1
                                        #     Parent Loop BB4_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	1(%rax,%rbp), %ebx
	movzbl	tr(%rbx), %ebx
	movzbl	(%rsi,%rbp), %edx
	incq	%rbp
	cmpb	tr(%rdx), %bl
	je	.LBB4_11
.LBB4_12:                               # %._crit_edge
                                        #   in Loop: Header=BB4_9 Depth=2
	cmpl	%r10d, %ebp
	jl	.LBB4_39
# BB#13:                                #   in Loop: Header=BB4_9 Depth=2
	movzbl	pat_len(%rdi), %eax
	cmpl	%ebp, %eax
	jle	.LBB4_14
.LBB4_39:                               #   in Loop: Header=BB4_9 Depth=2
	testq	%rcx, %rcx
	jne	.LBB4_9
	jmp	.LBB4_40
.LBB4_14:                               #   in Loop: Header=BB4_2 Depth=1
	cmpq	%r14, %r13
	ja	.LBB4_45
# BB#15:                                #   in Loop: Header=BB4_2 Depth=1
	incl	num_of_matched(%rip)
	movl	SILENT(%rip), %eax
	orl	FILENAMEONLY(%rip), %eax
	jne	.LBB4_45
# BB#16:                                #   in Loop: Header=BB4_2 Depth=1
	cmpl	$0, COUNT(%rip)
	je	.LBB4_20
# BB#17:                                # %.preheader121.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	decq	%r13
	.p2align	4, 0x90
.LBB4_18:                               # %.preheader121
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$10, 1(%r13)
	leaq	1(%r13), %r13
	jne	.LBB4_18
# BB#19:                                #   in Loop: Header=BB4_2 Depth=1
	movl	%r15d, %r8d
	jmp	.LBB4_40
.LBB4_20:                               #   in Loop: Header=BB4_2 Depth=1
	movl	FNAME(%rip), %eax
	cmpl	$0, INVERSE(%rip)
	je	.LBB4_21
# BB#29:                                #   in Loop: Header=BB4_2 Depth=1
	testl	%eax, %eax
	je	.LBB4_31
# BB#30:                                #   in Loop: Header=BB4_2 Depth=1
	movl	$.L.str, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	movq	%r10, %rbx
	movl	%r11d, %ebp
	callq	printf
	movl	%ebp, %r11d
	movq	%rbx, %r10
	.p2align	4, 0x90
.LBB4_31:                               # %.preheader120
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rbp
	leaq	-1(%rbp), %r13
	cmpb	$10, -1(%rbp)
	jne	.LBB4_31
# BB#32:                                #   in Loop: Header=BB4_2 Depth=1
	cmpq	%r13, %r12
	jae	.LBB4_37
# BB#33:                                # %.lr.ph140.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	%r11d, %ebx
	movq	%r10, 8(%rsp)           # 8-byte Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
	leaq	1(%r12), %r14
	.p2align	4, 0x90
.LBB4_34:                               # %.lr.ph140
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%r14), %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	incq	%r14
	cmpq	%r14, %rbp
	jne	.LBB4_34
# BB#35:                                # %._crit_edge141
                                        #   in Loop: Header=BB4_2 Depth=1
	cmpq	%r13, %r12
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %r10           # 8-byte Reload
	movl	%ebx, %r11d
	jae	.LBB4_37
# BB#36:                                #   in Loop: Header=BB4_2 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	%ebx, %r11d
	movq	8(%rsp), %r10           # 8-byte Reload
	.p2align	4, 0x90
.LBB4_37:                               # %.preheader119
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$10, (%rbp)
	leaq	1(%rbp), %rbp
	jne	.LBB4_37
# BB#38:                                #   in Loop: Header=BB4_2 Depth=1
	leaq	-1(%rbp), %r13
	movl	%r15d, %r8d
	movq	%rbp, %r12
	jmp	.LBB4_40
.LBB4_21:                               #   in Loop: Header=BB4_2 Depth=1
	testl	%eax, %eax
	je	.LBB4_23
# BB#22:                                #   in Loop: Header=BB4_2 Depth=1
	movl	$.L.str, %edi
	movl	$CurrentFileName, %esi
	xorl	%eax, %eax
	movq	%r10, %rbx
	movl	%r11d, %ebp
	callq	printf
	movl	%ebp, %r11d
	movq	%rbx, %r10
.LBB4_23:                               # %.preheader118.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	incq	%r13
	.p2align	4, 0x90
.LBB4_24:                               # %.preheader118
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$10, -2(%r13)
	leaq	-1(%r13), %r13
	jne	.LBB4_24
# BB#25:                                # %.preheader117
                                        #   in Loop: Header=BB4_2 Depth=1
	movb	(%r13), %al
	movl	%r11d, %ebp
	movq	%r10, %rbx
	cmpb	$10, %al
	je	.LBB4_28
	.p2align	4, 0x90
.LBB4_26:                               # %.lr.ph142
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%al, %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	movzbl	1(%r13), %eax
	incq	%r13
	cmpb	$10, %al
	jne	.LBB4_26
.LBB4_28:                               # %._crit_edge143
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	%r15d, %r8d
	movq	%rbx, %r10
	movl	%ebp, %r11d
	jmp	.LBB4_40
	.p2align	4, 0x90
.LBB4_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_6 Depth 2
                                        #     Child Loop BB4_9 Depth 2
                                        #       Child Loop BB4_11 Depth 3
                                        #     Child Loop BB4_18 Depth 2
                                        #     Child Loop BB4_31 Depth 2
                                        #     Child Loop BB4_34 Depth 2
                                        #     Child Loop BB4_37 Depth 2
                                        #     Child Loop BB4_24 Depth 2
                                        #     Child Loop BB4_26 Depth 2
	movzbl	(%r13), %eax
	movzbl	tr1(%rax), %ecx
	shll	$4, %ecx
	movzbl	-1(%r13), %eax
	movzbl	tr1(%rax), %eax
	addl	%ecx, %eax
	testl	%r11d, %r11d
	je	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	shll	$4, %eax
	movzbl	-2(%r13), %ecx
	movzbl	tr1(%rcx), %ecx
	addl	%ecx, %eax
.LBB4_4:                                #   in Loop: Header=BB4_2 Depth=1
	movl	%eax, %eax
	movb	SHIFT1(%rax), %r8b
	testb	%r8b, %r8b
	je	.LBB4_5
.LBB4_40:                               # %.thread
                                        #   in Loop: Header=BB4_2 Depth=1
	movzbl	%r8b, %eax
	addq	%rax, %r13
	cmpq	%r14, %r13
	jbe	.LBB4_2
.LBB4_41:                               # %._crit_edge149
	cmpl	$0, INVERSE(%rip)
	je	.LBB4_45
# BB#42:                                # %._crit_edge149
	movl	COUNT(%rip), %eax
	testl	%eax, %eax
	je	.LBB4_44
	jmp	.LBB4_45
	.p2align	4, 0x90
.LBB4_43:                               # %.lr.ph
                                        #   in Loop: Header=BB4_44 Depth=1
	movzbl	(%r12), %edi
	incq	%r12
	movq	stdout(%rip), %rsi
	callq	_IO_putc
.LBB4_44:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r14, %r12
	jbe	.LBB4_43
.LBB4_45:                               # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	monkey1, .Lfunc_end4-monkey1
	.cfi_endproc

	.globl	mgrep
	.p2align	4, 0x90
	.type	mgrep,@function
mgrep:                                  # @mgrep
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$17416, %rsp            # imm = 0x4408
.Lcfi45:
	.cfi_def_cfa_offset 17472
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movl	%edi, %r12d
	movb	$10, 1023(%rsp)
	leaq	1024(%rsp), %rsi
	movl	$8192, %edx             # imm = 0x2000
	callq	read
	testl	%eax, %eax
	jle	.LBB5_31
# BB#1:                                 # %.lr.ph.preheader
	movl	$1023, %ebp             # imm = 0x3FF
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_11 Depth 2
                                        #     Child Loop BB5_17 Depth 2
	cmpl	$0, INVERSE(%rip)
	je	.LBB5_16
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	COUNT(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB5_16
# BB#4:                                 # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	%eax, %ecx
	testb	$1, %al
	jne	.LBB5_6
# BB#5:                                 #   in Loop: Header=BB5_2 Depth=1
	xorl	%edx, %edx
	cmpq	$1, %rcx
	jne	.LBB5_10
	jmp	.LBB5_16
	.p2align	4, 0x90
.LBB5_6:                                # %.lr.ph.i.prol
                                        #   in Loop: Header=BB5_2 Depth=1
	cmpb	$10, 1024(%rsp)
	jne	.LBB5_8
# BB#7:                                 #   in Loop: Header=BB5_2 Depth=1
	incl	total_line(%rip)
.LBB5_8:                                # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	$1, %edx
	cmpq	$1, %rcx
	je	.LBB5_16
.LBB5_10:                               # %.lr.ph.preheader.i.new
                                        #   in Loop: Header=BB5_2 Depth=1
	subq	%rdx, %rcx
	leaq	1025(%rsp), %rsi
	addq	%rsi, %rdx
	.p2align	4, 0x90
.LBB5_11:                               # %.lr.ph.i
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$10, -1(%rdx)
	jne	.LBB5_13
# BB#12:                                #   in Loop: Header=BB5_11 Depth=2
	incl	total_line(%rip)
.LBB5_13:                               # %.lr.ph.i.160
                                        #   in Loop: Header=BB5_11 Depth=2
	cmpb	$10, (%rdx)
	jne	.LBB5_15
# BB#14:                                #   in Loop: Header=BB5_11 Depth=2
	incl	total_line(%rip)
.LBB5_15:                               #   in Loop: Header=BB5_11 Depth=2
	addq	$2, %rdx
	addq	$-2, %rcx
	jne	.LBB5_11
.LBB5_16:                               # %countline.exit
                                        #   in Loop: Header=BB5_2 Depth=1
	movslq	%eax, %r14
	leaq	1023(%rsp,%r14), %rcx
	addq	$1023, %r14             # imm = 0x3FF
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_17:                               #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r14,%rdx), %rsi
	leaq	-1(%rdx), %r13
	cmpq	$1025, %rsi             # imm = 0x401
	jl	.LBB5_19
# BB#18:                                #   in Loop: Header=BB5_17 Depth=2
	cmpb	$10, (%rcx,%rdx)
	movq	%r13, %rdx
	jne	.LBB5_17
.LBB5_19:                               #   in Loop: Header=BB5_2 Depth=1
	movl	%eax, %r15d
	addl	$1024, %r15d            # imm = 0x400
	leaq	(%r15,%r13), %rdx
	movslq	%ebp, %rax
	movb	$10, -1(%rsp,%rax)
	cmpl	$0, SHORT(%rip)
	je	.LBB5_21
# BB#20:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%rsp, %rdi
	movl	%ebp, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	m_short
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB5_23
	jmp	.LBB5_25
	.p2align	4, 0x90
.LBB5_21:                               #   in Loop: Header=BB5_2 Depth=1
	movq	%rsp, %rdi
	movl	%ebp, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	monkey1
	cmpl	$0, FILENAMEONLY(%rip)
	je	.LBB5_25
.LBB5_23:                               #   in Loop: Header=BB5_2 Depth=1
	movl	num_of_matched(%rip), %eax
	testl	%eax, %eax
	jne	.LBB5_24
.LBB5_25:                               #   in Loop: Header=BB5_2 Depth=1
	leaq	(%rsp,%r14), %rax
	leaq	1(%r13,%rax), %rsi
	movl	%r13d, %ebp
	addl	$1024, %ebp             # imm = 0x400
	movl	$1, %eax
	cmovsl	%eax, %ebp
	movslq	%ebp, %rbx
	leaq	(%rsp,%rbx), %rdi
	movq	%r13, %rdx
	negq	%rdx
	callq	strncpy
	movl	$8192, %edx             # imm = 0x2000
	movl	%r12d, %edi
	leaq	1024(%rsp), %rsi
	callq	read
	testl	%eax, %eax
	jg	.LBB5_2
# BB#26:                                # %._crit_edge
	movl	%r13d, %eax
	notl	%eax
	movb	$10, 1024(%rsp)
	movb	$10, -1(%rsp,%rbx)
	testl	%eax, %eax
	jle	.LBB5_30
# BB#27:
	addq	%r13, %r15
	cmpl	$0, SHORT(%rip)
	je	.LBB5_29
# BB#28:
	movq	%rsp, %rdi
	movl	%ebp, %esi
	movl	%r15d, %edx
	callq	m_short
	jmp	.LBB5_30
.LBB5_31:                               # %._crit_edge.thread
	movb	$10, 1024(%rsp)
	movb	$10, 1022(%rsp)
	jmp	.LBB5_30
.LBB5_24:
	movl	$CurrentFileName, %edi
	callq	puts
.LBB5_30:
	addq	$17416, %rsp            # imm = 0x4408
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_29:
	movq	%rsp, %rdi
	movl	%ebp, %esi
	movl	%r15d, %edx
	callq	monkey1
	jmp	.LBB5_30
.Lfunc_end5:
	.size	mgrep, .Lfunc_end5-mgrep
	.cfi_endproc

	.type	LONG,@object            # @LONG
	.bss
	.globl	LONG
	.p2align	2
LONG:
	.long	0                       # 0x0
	.size	LONG, 4

	.type	SHORT,@object           # @SHORT
	.globl	SHORT
	.p2align	2
SHORT:
	.long	0                       # 0x0
	.size	SHORT, 4

	.type	p_size,@object          # @p_size
	.globl	p_size
	.p2align	2
p_size:
	.long	0                       # 0x0
	.size	p_size, 4

	.type	HASH,@object            # @HASH
	.comm	HASH,65536,16
	.type	tr,@object              # @tr
	.comm	tr,256,16
	.type	patt,@object            # @patt
	.comm	patt,240000,16
	.type	pat_len,@object         # @pat_len
	.comm	pat_len,30000,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s: "
	.size	.L.str, 5

	.type	SHIFT1,@object          # @SHIFT1
	.comm	SHIFT1,4096,16
	.type	qt,@object              # @qt
	.comm	qt,8,8
	.type	pt,@object              # @pt
	.comm	pt,8,8
	.type	pat_spool,@object       # @pat_spool
	.comm	pat_spool,320256,16
	.type	buf,@object             # @buf
	.comm	buf,268192,16
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%s: maximum pattern file size is %d\n"
	.size	.L.str.2, 37

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%s: maximum number of patterns is %d\n"
	.size	.L.str.3, 38

	.type	tr1,@object             # @tr1
	.comm	tr1,256,16
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"the pattern file is empty\n"
	.size	.L.str.4, 27


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
