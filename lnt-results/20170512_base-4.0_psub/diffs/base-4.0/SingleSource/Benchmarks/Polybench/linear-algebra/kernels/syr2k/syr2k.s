	.text
	.file	"syr2k.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4562146422526312448     # double 9.765625E-4
.LCPI6_2:
	.quad	4674638556675702784     # double 32412
.LCPI6_4:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_1:
	.quad	4656886941445259264     # double 2123
	.quad	4656886941445259264     # double 2123
.LCPI6_3:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 80
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8388608, %edx          # imm = 0x800000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_50
# BB#1:
	movq	8(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_50
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8388608, %edx          # imm = 0x800000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_50
# BB#3:                                 # %polybench_alloc_data.exit
	movq	8(%rsp), %r15
	testq	%r15, %r15
	je	.LBB6_50
# BB#4:                                 # %polybench_alloc_data.exit37
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8388608, %edx          # imm = 0x800000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_50
# BB#5:                                 # %polybench_alloc_data.exit37
	movq	8(%rsp), %r12
	testq	%r12, %r12
	je	.LBB6_50
# BB#6:                                 # %polybench_alloc_data.exit39
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8388608, %edx          # imm = 0x800000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_50
# BB#7:                                 # %polybench_alloc_data.exit39
	movq	8(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_50
# BB#8:                                 # %polybench_alloc_data.exit41
	xorl	%eax, %eax
	movl	$1, %ecx
	movsd	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_9:                                # %.preheader2.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_10 Depth 2
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movq	%rcx, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_10:                               #   Parent Loop BB6_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -8(%r12,%rdx,8)
	movsd	%xmm2, -8(%rbx,%rdx,8)
	movl	%esi, %edi
	orl	$1, %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, (%r12,%rdx,8)
	movsd	%xmm2, (%rbx,%rdx,8)
	addq	$2, %rsi
	addq	$2, %rdx
	cmpq	$1024, %rsi             # imm = 0x400
	jne	.LBB6_10
# BB#11:                                #   in Loop: Header=BB6_9 Depth=1
	incq	%rax
	addq	$1024, %rcx             # imm = 0x400
	cmpq	$1024, %rax             # imm = 0x400
	jne	.LBB6_9
# BB#12:                                # %.preheader.i.preheader
	xorl	%eax, %eax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB6_13:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_14 Depth 2
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movq	%rcx, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_14:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, -8(%r14,%rdx,8)
	movsd	%xmm2, -8(%r15,%rdx,8)
	movl	%esi, %edi
	orl	$1, %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, (%r14,%rdx,8)
	movsd	%xmm2, (%r15,%rdx,8)
	addq	$2, %rsi
	addq	$2, %rdx
	cmpq	$1024, %rsi             # imm = 0x400
	jne	.LBB6_14
# BB#15:                                #   in Loop: Header=BB6_13 Depth=1
	incq	%rax
	addq	$1024, %rcx             # imm = 0x400
	cmpq	$1024, %rax             # imm = 0x400
	jne	.LBB6_13
# BB#16:                                # %.preheader3.i.preheader
	leaq	48(%r14), %rax
	xorl	%ecx, %ecx
	movapd	.LCPI6_1(%rip), %xmm1   # xmm1 = [2.123000e+03,2.123000e+03]
	.p2align	4, 0x90
.LBB6_17:                               # %.preheader3.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_18 Depth 2
	movl	$1024, %edx             # imm = 0x400
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_18:                               # %vector.body
                                        #   Parent Loop BB6_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%rsi), %xmm0
	movupd	-32(%rsi), %xmm2
	mulpd	%xmm1, %xmm0
	mulpd	%xmm1, %xmm2
	movupd	%xmm0, -48(%rsi)
	movupd	%xmm2, -32(%rsi)
	movupd	-16(%rsi), %xmm0
	movupd	(%rsi), %xmm2
	mulpd	%xmm1, %xmm0
	mulpd	%xmm1, %xmm2
	movupd	%xmm0, -16(%rsi)
	movupd	%xmm2, (%rsi)
	addq	$64, %rsi
	addq	$-8, %rdx
	jne	.LBB6_18
# BB#19:                                # %middle.block
                                        #   in Loop: Header=BB6_17 Depth=1
	incq	%rcx
	addq	$8192, %rax             # imm = 0x2000
	cmpq	$1024, %rcx             # imm = 0x400
	jne	.LBB6_17
# BB#20:                                # %.preheader1.i.preheader
	xorl	%r8d, %r8d
	movsd	.LCPI6_2(%rip), %xmm0   # xmm0 = mem[0],zero
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB6_21:                               # %.preheader1.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_22 Depth 2
                                        #       Child Loop BB6_23 Depth 3
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_22:                               # %.preheader.i42
                                        #   Parent Loop BB6_21 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_23 Depth 3
	movq	%r9, %rax
	shlq	$13, %rax
	addq	%r14, %rax
	leaq	(%rax,%rsi,8), %rdi
	movsd	(%rax,%rsi,8), %xmm2    # xmm2 = mem[0],zero
	movq	%r8, %rax
	movq	%rbp, %rdx
	movl	$1024, %ecx             # imm = 0x400
	.p2align	4, 0x90
.LBB6_23:                               #   Parent Loop BB6_21 Depth=1
                                        #     Parent Loop BB6_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%r12,%rax), %xmm3      # xmm3 = mem[0],zero
	mulsd	%xmm0, %xmm3
	mulsd	(%rbx,%rdx), %xmm3
	addsd	%xmm2, %xmm3
	movsd	%xmm3, (%rdi)
	movsd	(%rbx,%rax), %xmm2      # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	mulsd	(%r12,%rdx), %xmm2
	addsd	%xmm3, %xmm2
	movsd	%xmm2, (%rdi)
	addq	$8, %rdx
	addq	$8, %rax
	decq	%rcx
	jne	.LBB6_23
# BB#24:                                #   in Loop: Header=BB6_22 Depth=2
	incq	%rsi
	addq	$8192, %rbp             # imm = 0x2000
	cmpq	$1024, %rsi             # imm = 0x400
	jne	.LBB6_22
# BB#25:                                #   in Loop: Header=BB6_21 Depth=1
	incq	%r9
	addq	$8192, %r8              # imm = 0x2000
	cmpq	$1024, %r9              # imm = 0x400
	jne	.LBB6_21
# BB#26:                                # %.preheader3.i47.preheader
	leaq	48(%r15), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_27:                               # %.preheader3.i47
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_28 Depth 2
	movl	$1024, %edx             # imm = 0x400
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_28:                               # %vector.body100
                                        #   Parent Loop BB6_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%rsi), %xmm2
	movupd	-32(%rsi), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	%xmm2, -48(%rsi)
	movupd	%xmm3, -32(%rsi)
	movupd	-16(%rsi), %xmm2
	movupd	(%rsi), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	%xmm2, -16(%rsi)
	movupd	%xmm3, (%rsi)
	addq	$64, %rsi
	addq	$-8, %rdx
	jne	.LBB6_28
# BB#29:                                # %middle.block101
                                        #   in Loop: Header=BB6_27 Depth=1
	incq	%rcx
	addq	$8192, %rax             # imm = 0x2000
	cmpq	$1024, %rcx             # imm = 0x400
	jne	.LBB6_27
# BB#30:                                # %.preheader1.i54.preheader
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB6_31:                               # %.preheader1.i54
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_32 Depth 2
                                        #       Child Loop BB6_33 Depth 3
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_32:                               # %.preheader.i57
                                        #   Parent Loop BB6_31 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_33 Depth 3
	movq	%r9, %rax
	shlq	$13, %rax
	addq	%r15, %rax
	leaq	(%rax,%rsi,8), %rdi
	movsd	(%rax,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	movq	%r8, %rax
	movq	%rbp, %rdx
	movl	$1024, %ecx             # imm = 0x400
	.p2align	4, 0x90
.LBB6_33:                               #   Parent Loop BB6_31 Depth=1
                                        #     Parent Loop BB6_32 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%r12,%rax), %xmm2      # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	mulsd	(%rbx,%rdx), %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, (%rdi)
	movsd	(%rbx,%rax), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	mulsd	(%r12,%rdx), %xmm1
	addsd	%xmm2, %xmm1
	movsd	%xmm1, (%rdi)
	addq	$8, %rdx
	addq	$8, %rax
	decq	%rcx
	jne	.LBB6_33
# BB#34:                                #   in Loop: Header=BB6_32 Depth=2
	incq	%rsi
	addq	$8192, %rbp             # imm = 0x2000
	cmpq	$1024, %rsi             # imm = 0x400
	jne	.LBB6_32
# BB#35:                                #   in Loop: Header=BB6_31 Depth=1
	incq	%r9
	addq	$8192, %r8              # imm = 0x2000
	cmpq	$1024, %r9              # imm = 0x400
	jne	.LBB6_31
# BB#36:                                # %.preheader.i65.preheader
	xorl	%edx, %edx
	movl	$1, %eax
	movapd	.LCPI6_3(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_4(%rip), %xmm3   # xmm3 = mem[0],zero
.LBB6_37:                               # %.preheader.i65
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_38 Depth 2
	movq	%rax, %rsi
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB6_38:                               #   Parent Loop BB6_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%r14,%rsi,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%r15,%rsi,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_39
# BB#41:                                #   in Loop: Header=BB6_38 Depth=2
	movsd	(%r14,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r15,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_40
# BB#42:                                #   in Loop: Header=BB6_38 Depth=2
	addq	$2, %rsi
	leaq	2(%rcx), %rdi
	incq	%rcx
	cmpq	$1024, %rcx             # imm = 0x400
	movq	%rdi, %rcx
	jl	.LBB6_38
# BB#43:                                #   in Loop: Header=BB6_37 Depth=1
	incq	%rdx
	addq	$1024, %rax             # imm = 0x400
	cmpq	$1024, %rdx             # imm = 0x400
	jl	.LBB6_37
# BB#44:                                # %check_FP.exit
	movl	$16385, %edi            # imm = 0x4001
	callq	malloc
	movq	%rax, %r13
	movb	$0, 16384(%r13)
	xorl	%eax, %eax
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB6_45:                               # %.preheader.i68
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_46 Depth 2
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rbp, %rsi
	movl	$15, %ecx
	.p2align	4, 0x90
.LBB6_46:                               #   Parent Loop BB6_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rdx
	movl	%edx, %eax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -15(%r13,%rcx)
	movb	%al, -14(%r13,%rcx)
	movq	%rdx, %rax
	shrq	$8, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -13(%r13,%rcx)
	movb	%al, -12(%r13,%rcx)
	movq	%rdx, %rax
	shrq	$16, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -11(%r13,%rcx)
	movb	%al, -10(%r13,%rcx)
	movl	%edx, %eax
	shrl	$24, %eax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -9(%r13,%rcx)
	movb	%al, -8(%r13,%rcx)
	movq	%rdx, %rax
	shrq	$32, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -7(%r13,%rcx)
	movb	%al, -6(%r13,%rcx)
	movq	%rdx, %rax
	shrq	$40, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -5(%r13,%rcx)
	movb	%al, -4(%r13,%rcx)
	movq	%rdx, %rax
	shrq	$48, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -3(%r13,%rcx)
	movb	%al, -2(%r13,%rcx)
	shrq	$56, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, -1(%r13,%rcx)
	movb	%dl, (%r13,%rcx)
	addq	$16, %rcx
	addq	$8, %rsi
	cmpq	$16399, %rcx            # imm = 0x400F
	jne	.LBB6_46
# BB#47:                                #   in Loop: Header=BB6_45 Depth=1
	movq	stderr(%rip), %rsi
	movq	%r13, %rdi
	callq	fputs
	movq	16(%rsp), %rax          # 8-byte Reload
	incq	%rax
	addq	$8192, %rbp             # imm = 0x2000
	cmpq	$1024, %rax             # imm = 0x400
	jne	.LBB6_45
# BB#48:                                # %print_array.exit
	movq	%r13, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_49
.LBB6_39:                               # %check_FP.exit.threadsplit
	decq	%rcx
.LBB6_40:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_4(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %r8d
	movl	%ecx, %r9d
	callq	fprintf
	movl	$1, %eax
.LBB6_49:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_50:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d][%d] = %lf and B[%d][%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 76


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
