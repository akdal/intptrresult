	.text
	.file	"btQuantizedBvh.bc"
	.globl	_ZN14btQuantizedBvhC2Ev
	.p2align	4, 0x90
	.type	_ZN14btQuantizedBvhC2Ev,@function
_ZN14btQuantizedBvhC2Ev:                # @_ZN14btQuantizedBvhC2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV14btQuantizedBvh+16, (%rdi)
	movl	$275, 56(%rdi)          # imm = 0x113
	movb	$0, 64(%rdi)
	movb	$1, 96(%rdi)
	movq	$0, 88(%rdi)
	movl	$0, 76(%rdi)
	movl	$0, 80(%rdi)
	movb	$1, 128(%rdi)
	movq	$0, 120(%rdi)
	movl	$0, 108(%rdi)
	movl	$0, 112(%rdi)
	movb	$1, 160(%rdi)
	movq	$0, 152(%rdi)
	movl	$0, 140(%rdi)
	movl	$0, 144(%rdi)
	movb	$1, 192(%rdi)
	movq	$0, 184(%rdi)
	movl	$0, 172(%rdi)
	movl	$0, 176(%rdi)
	movl	$0, 200(%rdi)
	movb	$1, 232(%rdi)
	movq	$0, 224(%rdi)
	movl	$0, 212(%rdi)
	movl	$0, 216(%rdi)
	movl	$0, 240(%rdi)
	movl	$-8388609, 8(%rdi)      # imm = 0xFF7FFFFF
	movl	$-8388609, 12(%rdi)     # imm = 0xFF7FFFFF
	movl	$-8388609, 16(%rdi)     # imm = 0xFF7FFFFF
	movl	$0, 20(%rdi)
	movabsq	$9187343237679939583, %rax # imm = 0x7F7FFFFF7F7FFFFF
	movq	%rax, 24(%rdi)
	movq	$2139095039, 32(%rdi)   # imm = 0x7F7FFFFF
	retq
.Lfunc_end0:
	.size	_ZN14btQuantizedBvhC2Ev, .Lfunc_end0-_ZN14btQuantizedBvhC2Ev
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN14btQuantizedBvh13buildInternalEv
	.p2align	4, 0x90
	.type	_ZN14btQuantizedBvh13buildInternalEv,@function
_ZN14btQuantizedBvh13buildInternalEv:   # @_ZN14btQuantizedBvh13buildInternalEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movb	$1, 64(%r15)
	movslq	140(%r15), %rdx
	movq	%rdx, %r12
	addq	%r12, %r12
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	172(%r15), %r14d
	cmpl	%r12d, %r14d
	jge	.LBB2_25
# BB#1:
	movslq	%r14d, %rbx
	cmpl	%r12d, 176(%r15)
	jge	.LBB2_2
# BB#3:
	testl	%edx, %edx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	je	.LBB2_4
# BB#5:
	movq	%r12, %rdi
	shlq	$4, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r13
	movl	172(%r15), %eax
	jmp	.LBB2_6
.LBB2_2:                                # %..lr.ph.i_crit_edge
	leaq	184(%r15), %rbp
	jmp	.LBB2_19
.LBB2_4:
	xorl	%r13d, %r13d
	movl	%r14d, %eax
.LBB2_6:                                # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE8allocateEi.exit.i.i
	leaq	184(%r15), %rbp
	testl	%eax, %eax
	jle	.LBB2_14
# BB#7:                                 # %.lr.ph.i.i.i
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB2_8
# BB#9:                                 # %.prol.preheader27
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_10:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	%xmm0, (%r13,%rdi)
	incq	%rcx
	addq	$16, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB2_10
	jmp	.LBB2_11
.LBB2_8:
	xorl	%ecx, %ecx
.LBB2_11:                               # %.prol.loopexit28
	cmpq	$3, %r8
	jb	.LBB2_14
# BB#12:                                # %.lr.ph.i.i.i.new
	subq	%rcx, %rax
	shlq	$4, %rcx
	addq	$48, %rcx
	.p2align	4, 0x90
.LBB2_13:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%r13,%rcx)
	movq	(%rbp), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%r13,%rcx)
	movq	(%rbp), %rdx
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%r13,%rcx)
	movq	(%rbp), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%r13,%rcx)
	addq	$64, %rcx
	addq	$-4, %rax
	jne	.LBB2_13
.LBB2_14:                               # %_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_.exit.i.i
	movq	184(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB2_18
# BB#15:
	cmpb	$0, 192(%r15)
	je	.LBB2_17
# BB#16:
	callq	_Z21btAlignedFreeInternalPv
.LBB2_17:
	movq	$0, (%rbp)
.LBB2_18:                               # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi.exit.preheader.i
	movb	$1, 192(%r15)
	movq	%r13, 184(%r15)
	movl	%r12d, 176(%r15)
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB2_19:                               # %.lr.ph.i
	movslq	%r12d, %rax
	movl	%r12d, %ecx
	subl	%r14d, %ecx
	leaq	-1(%rax), %r8
	subq	%rbx, %r8
	andq	$3, %rcx
	je	.LBB2_22
# BB#20:                                # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi.exit.i.prol.preheader
	movq	%rbx, %rsi
	shlq	$4, %rsi
	negq	%rcx
	.p2align	4, 0x90
.LBB2_21:                               # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movaps	(%rsp), %xmm0
	movups	%xmm0, (%rdi,%rsi)
	incq	%rbx
	addq	$16, %rsi
	incq	%rcx
	jne	.LBB2_21
.LBB2_22:                               # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi.exit.i.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB2_25
# BB#23:                                # %.lr.ph.i.new
	subq	%rbx, %rax
	shlq	$4, %rbx
	addq	$48, %rbx
	.p2align	4, 0x90
.LBB2_24:                               # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rcx
	movaps	(%rsp), %xmm0
	movups	%xmm0, -48(%rcx,%rbx)
	movq	(%rbp), %rcx
	movaps	(%rsp), %xmm0
	movups	%xmm0, -32(%rcx,%rbx)
	movq	(%rbp), %rcx
	movaps	(%rsp), %xmm0
	movups	%xmm0, -16(%rcx,%rbx)
	movq	(%rbp), %rcx
	movaps	(%rsp), %xmm0
	movups	%xmm0, (%rcx,%rbx)
	addq	$64, %rbx
	addq	$-4, %rax
	jne	.LBB2_24
.LBB2_25:                               # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE6resizeEiRKS0_.exit
	movl	%r12d, 172(%r15)
	movl	$0, 60(%r15)
	xorl	%esi, %esi
	movq	%r15, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	_ZN14btQuantizedBvh9buildTreeEii
	cmpb	$0, 64(%r15)
	je	.LBB2_42
# BB#26:
	cmpl	$0, 212(%r15)
	jne	.LBB2_42
# BB#27:
	movl	$1, %r14d
	cmpl	$0, 216(%r15)
	movl	$1, %eax
	jne	.LBB2_41
# BB#28:                                # %_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE8allocateEi.exit.i.i
	movl	$32, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movslq	212(%r15), %rax
	testq	%rax, %rax
	jle	.LBB2_36
# BB#29:                                # %.lr.ph.i.i.i13
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB2_30
# BB#31:                                # %.prol.preheader
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_32:                               # =>This Inner Loop Header: Depth=1
	movq	224(%r15), %rbp
	movups	(%rbp,%rdi), %xmm0
	movups	16(%rbp,%rdi), %xmm1
	movups	%xmm1, 16(%rbx,%rdi)
	movups	%xmm0, (%rbx,%rdi)
	incq	%rcx
	addq	$32, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB2_32
	jmp	.LBB2_33
.LBB2_30:
	xorl	%ecx, %ecx
.LBB2_33:                               # %.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB2_36
# BB#34:                                # %.lr.ph.i.i.i13.new
	subq	%rcx, %rax
	shlq	$5, %rcx
	addq	$96, %rcx
	.p2align	4, 0x90
.LBB2_35:                               # =>This Inner Loop Header: Depth=1
	movq	224(%r15), %rdx
	movups	-96(%rdx,%rcx), %xmm0
	movups	-80(%rdx,%rcx), %xmm1
	movups	%xmm1, -80(%rbx,%rcx)
	movups	%xmm0, -96(%rbx,%rcx)
	movq	224(%r15), %rdx
	movups	-64(%rdx,%rcx), %xmm0
	movups	-48(%rdx,%rcx), %xmm1
	movups	%xmm1, -48(%rbx,%rcx)
	movups	%xmm0, -64(%rbx,%rcx)
	movq	224(%r15), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	-16(%rdx,%rcx), %xmm1
	movups	%xmm1, -16(%rbx,%rcx)
	movups	%xmm0, -32(%rbx,%rcx)
	movq	224(%r15), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	16(%rdx,%rcx), %xmm1
	movups	%xmm1, 16(%rbx,%rcx)
	movups	%xmm0, (%rbx,%rcx)
	subq	$-128, %rcx
	addq	$-4, %rax
	jne	.LBB2_35
.LBB2_36:                               # %_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4copyEiiPS0_.exit.i.i
	movq	224(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB2_40
# BB#37:
	cmpb	$0, 232(%r15)
	je	.LBB2_39
# BB#38:
	callq	_Z21btAlignedFreeInternalPv
.LBB2_39:
	movq	$0, 224(%r15)
.LBB2_40:                               # %_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE10deallocateEv.exit.i.i
	movb	$1, 232(%r15)
	movq	%rbx, 224(%r15)
	movl	$1, 216(%r15)
	movl	212(%r15), %eax
	incl	%eax
.LBB2_41:                               # %_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_.exit
	movl	%eax, 212(%r15)
	movq	224(%r15), %rax
	movups	(%rsp), %xmm0
	movups	16(%rsp), %xmm1
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	224(%r15), %rax
	movq	184(%r15), %rcx
	movzwl	(%rcx), %edx
	movw	%dx, (%rax)
	movzwl	2(%rcx), %edx
	movw	%dx, 2(%rax)
	movzwl	4(%rcx), %edx
	movw	%dx, 4(%rax)
	movzwl	6(%rcx), %edx
	movw	%dx, 6(%rax)
	movzwl	8(%rcx), %edx
	movw	%dx, 8(%rax)
	movzwl	10(%rcx), %edx
	movw	%dx, 10(%rax)
	movl	$0, 12(%rax)
	movl	12(%rcx), %ecx
	movl	%ecx, %edx
	negl	%edx
	testl	%ecx, %ecx
	cmovnsl	%r14d, %edx
	movl	%edx, 16(%rax)
.LBB2_42:                               # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE6resizeEiRKS0_.exit._crit_edge
	movl	212(%r15), %eax
	movl	%eax, 240(%r15)
	movq	152(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB2_46
# BB#43:
	cmpb	$0, 160(%r15)
	je	.LBB2_45
# BB#44:
	callq	_Z21btAlignedFreeInternalPv
.LBB2_45:
	movq	$0, 152(%r15)
.LBB2_46:                               # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE5clearEv.exit
	movb	$1, 160(%r15)
	movq	$0, 152(%r15)
	movq	$0, 140(%r15)
	movq	88(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB2_50
# BB#47:
	cmpb	$0, 96(%r15)
	je	.LBB2_49
# BB#48:
	callq	_Z21btAlignedFreeInternalPv
.LBB2_49:
	movq	$0, 88(%r15)
.LBB2_50:                               # %_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE5clearEv.exit
	movb	$1, 96(%r15)
	movq	$0, 88(%r15)
	movq	$0, 76(%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN14btQuantizedBvh13buildInternalEv, .Lfunc_end2-_ZN14btQuantizedBvh13buildInternalEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN14btQuantizedBvh9buildTreeEii
	.p2align	4, 0x90
	.type	_ZN14btQuantizedBvh9buildTreeEii,@function
_ZN14btQuantizedBvh9buildTreeEii:       # @_ZN14btQuantizedBvh9buildTreeEii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 128
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movl	%r14d, %eax
	subl	%r15d, %eax
	movslq	60(%rbx), %rsi
	cmpl	$1, %eax
	jne	.LBB3_5
# BB#1:
	cmpb	$0, 64(%rbx)
	je	.LBB3_4
# BB#2:
	movslq	%r15d, %rax
	shlq	$4, %rax
	movq	152(%rbx), %rcx
	movq	184(%rbx), %rdx
	shlq	$4, %rsi
	movupd	(%rcx,%rax), %xmm0
	jmp	.LBB3_3
.LBB3_5:
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movl	%r14d, %edx
	callq	_ZN14btQuantizedBvh17calcSplittingAxisEii
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movl	%r14d, %edx
	movl	%eax, %ecx
	callq	_ZN14btQuantizedBvh25sortAndCalcSplittingIndexEiii
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movslq	60(%rbx), %rdi
	cmpb	$0, 64(%rbx)
	je	.LBB3_7
# BB#6:
	movq	184(%rbx), %rcx
	movq	%rdi, %rax
	shlq	$4, %rax
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	28(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	8(%rbx), %xmm0
	subss	12(%rbx), %xmm1
	movss	32(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	16(%rbx), %xmm2
	mulss	40(%rbx), %xmm0
	mulss	44(%rbx), %xmm1
	mulss	48(%rbx), %xmm2
	cvttss2si	%xmm0, %edx
	andl	$65534, %edx            # imm = 0xFFFE
	movw	%dx, (%rcx,%rax)
	cvttss2si	%xmm1, %edx
	andl	$65534, %edx            # imm = 0xFFFE
	cvttss2si	%xmm2, %esi
	andl	$65534, %esi            # imm = 0xFFFE
	movw	%dx, 2(%rcx,%rax)
	movw	%si, 4(%rcx,%rax)
	jmp	.LBB3_8
.LBB3_4:
	movslq	%r15d, %rax
	shlq	$6, %rax
	movq	88(%rbx), %rcx
	movq	120(%rbx), %rdx
	shlq	$6, %rsi
	movupd	(%rcx,%rax), %xmm0
	movups	16(%rcx,%rax), %xmm1
	movups	32(%rcx,%rax), %xmm2
	movups	48(%rcx,%rax), %xmm3
	movups	%xmm3, 48(%rdx,%rsi)
	movups	%xmm2, 32(%rdx,%rsi)
	movups	%xmm1, 16(%rdx,%rsi)
.LBB3_3:                                # %_ZN14btQuantizedBvh26setInternalNodeEscapeIndexEii.exit
	movupd	%xmm0, (%rdx,%rsi)
	incl	60(%rbx)
	jmp	.LBB3_23
.LBB3_7:
	leaq	24(%rbx), %rax
	movq	120(%rbx), %rcx
	movq	%rdi, %rdx
	shlq	$6, %rdx
	movups	(%rax), %xmm0
	movups	%xmm0, (%rcx,%rdx)
.LBB3_8:                                # %_ZN14btQuantizedBvh22setInternalNodeAabbMinEiRK9btVector3.exit
	movslq	60(%rbx), %rax
	cmpb	$0, 64(%rbx)
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	je	.LBB3_10
# BB#9:
	movq	184(%rbx), %rcx
	shlq	$4, %rax
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm0
	subss	%xmm1, %xmm1
	movss	16(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm2
	mulss	40(%rbx), %xmm0
	mulss	44(%rbx), %xmm1
	mulss	48(%rbx), %xmm2
	movss	.LCPI3_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm0
	cvttss2si	%xmm0, %edx
	orl	$1, %edx
	movw	%dx, 6(%rcx,%rax)
	addss	%xmm3, %xmm1
	cvttss2si	%xmm1, %edx
	orl	$1, %edx
	addss	%xmm3, %xmm2
	cvttss2si	%xmm2, %esi
	orl	$1, %esi
	movw	%dx, 8(%rcx,%rax)
	movw	%si, 10(%rcx,%rax)
	jmp	.LBB3_11
.LBB3_10:
	leaq	8(%rbx), %rcx
	movq	120(%rbx), %rdx
	shlq	$6, %rax
	movups	(%rcx), %xmm0
	movups	%xmm0, 16(%rdx,%rax)
.LBB3_11:                               # %_ZN14btQuantizedBvh22setInternalNodeAabbMaxEiRK9btVector3.exit.preheader
	movl	60(%rbx), %ebp
	movl	%r14d, 20(%rsp)         # 4-byte Spill
	subl	%r15d, %r14d
	movl	%r15d, 16(%rsp)         # 4-byte Spill
	jle	.LBB3_17
# BB#12:                                # %.lr.ph
	movslq	%r15d, %r12
	shlq	$4, %r12
	leaq	32(%rsp), %r13
	leaq	56(%rsp), %r15
	.p2align	4, 0x90
.LBB3_13:                               # =>This Inner Loop Header: Depth=1
	cmpb	$0, 64(%rbx)
	je	.LBB3_15
# BB#14:                                #   in Loop: Header=BB3_13 Depth=1
	movq	152(%rbx), %rax
	movzwl	(%rax,%r12), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	divss	40(%rbx), %xmm0
	movzwl	2(%rax,%r12), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ecx, %xmm1
	divss	44(%rbx), %xmm1
	movzwl	4(%rax,%r12), %eax
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%eax, %xmm2
	divss	48(%rbx), %xmm2
	addss	8(%rbx), %xmm0
	addss	12(%rbx), %xmm1
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	addss	16(%rbx), %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm0, 32(%rsp)
	movlps	%xmm1, 40(%rsp)
	movq	152(%rbx), %rax
	movzwl	6(%rax,%r12), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	divss	40(%rbx), %xmm0
	movzwl	8(%rax,%r12), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ecx, %xmm1
	divss	44(%rbx), %xmm1
	movzwl	10(%rax,%r12), %eax
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%eax, %xmm2
	divss	48(%rbx), %xmm2
	addss	8(%rbx), %xmm0
	addss	12(%rbx), %xmm1
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	addss	16(%rbx), %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	jmp	.LBB3_16
	.p2align	4, 0x90
.LBB3_15:                               #   in Loop: Header=BB3_13 Depth=1
	movq	88(%rbx), %rax
	movups	(%rax,%r12,4), %xmm0
	movaps	%xmm0, 32(%rsp)
	movsd	16(%rax,%r12,4), %xmm0  # xmm0 = mem[0],zero
	movsd	24(%rax,%r12,4), %xmm1  # xmm1 = mem[0],zero
.LBB3_16:                               # %_ZNK14btQuantizedBvh10getAabbMaxEi.exit
                                        #   in Loop: Header=BB3_13 Depth=1
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movupd	%xmm0, 56(%rsp)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r13, %rdx
	movq	%r15, %rcx
	callq	_ZN14btQuantizedBvh21mergeInternalNodeAabbEiRK9btVector3S2_
	movl	60(%rbx), %ebp
	addq	$16, %r12
	decl	%r14d
	jne	.LBB3_13
.LBB3_17:                               # %_ZN14btQuantizedBvh22setInternalNodeAabbMaxEiRK9btVector3.exit._crit_edge
	incl	%ebp
	movl	%ebp, 60(%rbx)
	movq	%rbx, %rdi
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	12(%rsp), %r14d         # 4-byte Reload
	movl	%r14d, %edx
	callq	_ZN14btQuantizedBvh9buildTreeEii
	movl	60(%rbx), %r15d
	movq	%rbx, %rdi
	movl	%r14d, %esi
	movl	20(%rsp), %edx          # 4-byte Reload
	callq	_ZN14btQuantizedBvh9buildTreeEii
	movl	60(%rbx), %r14d
	subl	48(%rsp), %r14d         # 4-byte Folded Reload
	movb	64(%rbx), %al
	testb	%al, %al
	je	.LBB3_20
# BB#18:                                # %_ZN14btQuantizedBvh22setInternalNodeAabbMaxEiRK9btVector3.exit._crit_edge
	movl	%r14d, %ecx
	shll	$4, %ecx
	cmpl	$2049, %ecx             # imm = 0x801
	jl	.LBB3_20
# BB#19:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movl	%r15d, %edx
	callq	_ZN14btQuantizedBvh20updateSubtreeHeadersEii
	movb	64(%rbx), %al
.LBB3_20:
	testb	%al, %al
	je	.LBB3_22
# BB#21:
	negl	%r14d
	movq	184(%rbx), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	shlq	$4, %rcx
	movl	%r14d, 12(%rax,%rcx)
	jmp	.LBB3_23
.LBB3_22:
	movq	120(%rbx), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	shlq	$6, %rcx
	movl	%r14d, 32(%rax,%rcx)
.LBB3_23:                               # %_ZN14btQuantizedBvh26setInternalNodeEscapeIndexEii.exit
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN14btQuantizedBvh9buildTreeEii, .Lfunc_end3-_ZN14btQuantizedBvh9buildTreeEii
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	1199570176              # float 65533
	.long	1199570176              # float 65533
	.zero	4
	.zero	4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_1:
	.long	1199570176              # float 65533
	.text
	.globl	_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f
	.p2align	4, 0x90
	.type	_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f,@function
_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f: # @_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f
	.cfi_startproc
# BB#0:
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	subps	%xmm2, %xmm1
	movss	8(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm3
	xorps	%xmm4, %xmm4
	xorps	%xmm5, %xmm5
	movss	%xmm3, %xmm5            # xmm5 = xmm3[0],xmm5[1,2,3]
	movlps	%xmm1, 8(%rdi)
	movlps	%xmm5, 16(%rdi)
	movsd	(%rdx), %xmm5           # xmm5 = mem[0],zero
	addps	%xmm2, %xmm5
	addss	8(%rdx), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm5, 24(%rdi)
	movlps	%xmm2, 32(%rdi)
	subps	%xmm1, %xmm5
	subss	%xmm3, %xmm0
	movaps	.LCPI4_0(%rip), %xmm1   # xmm1 = <65533,65533,u,u>
	divps	%xmm5, %xmm1
	movss	.LCPI4_1(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm2
	movss	%xmm2, %xmm4            # xmm4 = xmm2[0],xmm4[1,2,3]
	movlps	%xmm1, 40(%rdi)
	movlps	%xmm4, 48(%rdi)
	movb	$1, 64(%rdi)
	retq
.Lfunc_end4:
	.size	_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f, .Lfunc_end4-_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f
	.cfi_endproc

	.globl	_ZN14btQuantizedBvhD2Ev
	.p2align	4, 0x90
	.type	_ZN14btQuantizedBvhD2Ev,@function
_ZN14btQuantizedBvhD2Ev:                # @_ZN14btQuantizedBvhD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV14btQuantizedBvh+16, (%rbx)
	movq	224(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_4
# BB#1:
	cmpb	$0, 232(%rbx)
	je	.LBB5_3
# BB#2:
.Ltmp0:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp1:
.LBB5_3:                                # %.noexc
	movq	$0, 224(%rbx)
.LBB5_4:
	movb	$1, 232(%rbx)
	movq	$0, 224(%rbx)
	movq	$0, 212(%rbx)
	movq	184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_8
# BB#5:
	cmpb	$0, 192(%rbx)
	je	.LBB5_7
# BB#6:
.Ltmp5:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp6:
.LBB5_7:                                # %.noexc6
	movq	$0, 184(%rbx)
.LBB5_8:
	movb	$1, 192(%rbx)
	movq	$0, 184(%rbx)
	movq	$0, 172(%rbx)
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_12
# BB#9:
	cmpb	$0, 160(%rbx)
	je	.LBB5_11
# BB#10:
.Ltmp10:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp11:
.LBB5_11:                               # %.noexc8
	movq	$0, 152(%rbx)
.LBB5_12:
	movb	$1, 160(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 140(%rbx)
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_16
# BB#13:
	cmpb	$0, 128(%rbx)
	je	.LBB5_15
# BB#14:
.Ltmp15:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp16:
.LBB5_15:                               # %.noexc11
	movq	$0, 120(%rbx)
.LBB5_16:
	movb	$1, 128(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 108(%rbx)
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_20
# BB#17:
	cmpb	$0, 96(%rbx)
	je	.LBB5_19
# BB#18:
	callq	_Z21btAlignedFreeInternalPv
.LBB5_19:
	movq	$0, 88(%rbx)
.LBB5_20:                               # %_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeED2Ev.exit13
	movb	$1, 96(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 76(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB5_32:
.Ltmp17:
	movq	%rax, %r14
	jmp	.LBB5_39
.LBB5_33:
.Ltmp12:
	movq	%rax, %r14
	jmp	.LBB5_34
.LBB5_26:
.Ltmp7:
	movq	%rax, %r14
	jmp	.LBB5_27
.LBB5_21:
.Ltmp2:
	movq	%rax, %r14
	movq	184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_25
# BB#22:
	cmpb	$0, 192(%rbx)
	je	.LBB5_24
# BB#23:
.Ltmp3:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
.LBB5_24:                               # %.noexc15
	movq	$0, 184(%rbx)
.LBB5_25:                               # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeED2Ev.exit16
	movb	$1, 192(%rbx)
	movq	$0, 184(%rbx)
	movq	$0, 172(%rbx)
.LBB5_27:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_31
# BB#28:
	cmpb	$0, 160(%rbx)
	je	.LBB5_30
# BB#29:
.Ltmp8:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp9:
.LBB5_30:                               # %.noexc18
	movq	$0, 152(%rbx)
.LBB5_31:                               # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeED2Ev.exit19
	movb	$1, 160(%rbx)
	movq	$0, 152(%rbx)
	movq	$0, 140(%rbx)
.LBB5_34:
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_38
# BB#35:
	cmpb	$0, 128(%rbx)
	je	.LBB5_37
# BB#36:
.Ltmp13:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp14:
.LBB5_37:                               # %.noexc21
	movq	$0, 120(%rbx)
.LBB5_38:                               # %_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeED2Ev.exit22
	movb	$1, 128(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 108(%rbx)
.LBB5_39:
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_43
# BB#40:
	cmpb	$0, 96(%rbx)
	je	.LBB5_42
# BB#41:
.Ltmp18:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp19:
.LBB5_42:                               # %.noexc24
	movq	$0, 88(%rbx)
.LBB5_43:
	movb	$1, 96(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 76(%rbx)
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_44:
.Ltmp20:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN14btQuantizedBvhD2Ev, .Lfunc_end5-_ZN14btQuantizedBvhD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp3-.Ltmp16          #   Call between .Ltmp16 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp19-.Ltmp3          #   Call between .Ltmp3 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	1                       #   On action: 1
	.long	.Ltmp19-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Lfunc_end5-.Ltmp19     #   Call between .Ltmp19 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN14btQuantizedBvhD0Ev
	.p2align	4, 0x90
	.type	_ZN14btQuantizedBvhD0Ev,@function
_ZN14btQuantizedBvhD0Ev:                # @_ZN14btQuantizedBvhD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -24
.Lcfi35:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp21:
	callq	_ZN14btQuantizedBvhD2Ev
.Ltmp22:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB6_2:
.Ltmp23:
	movq	%rax, %r14
.Ltmp24:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp25:
# BB#3:                                 # %_ZN14btQuantizedBvhdlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_4:
.Ltmp26:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN14btQuantizedBvhD0Ev, .Lfunc_end6-_ZN14btQuantizedBvhD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp21-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin1   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp24-.Ltmp22         #   Call between .Ltmp22 and .Ltmp24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin1   #     jumps to .Ltmp26
	.byte	1                       #   On action: 1
	.long	.Ltmp25-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end6-.Ltmp25     #   Call between .Ltmp25 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN14btQuantizedBvh30assignInternalNodeFromLeafNodeEii
	.p2align	4, 0x90
	.type	_ZN14btQuantizedBvh30assignInternalNodeFromLeafNodeEii,@function
_ZN14btQuantizedBvh30assignInternalNodeFromLeafNodeEii: # @_ZN14btQuantizedBvh30assignInternalNodeFromLeafNodeEii
	.cfi_startproc
# BB#0:
	cmpb	$0, 64(%rdi)
	je	.LBB7_2
# BB#1:
	movslq	%edx, %rax
	shlq	$4, %rax
	movq	152(%rdi), %rcx
	movq	184(%rdi), %rdx
	movslq	%esi, %rsi
	shlq	$4, %rsi
	movups	(%rcx,%rax), %xmm0
	movups	%xmm0, (%rdx,%rsi)
	retq
.LBB7_2:
	movslq	%edx, %rax
	shlq	$6, %rax
	movq	88(%rdi), %rcx
	movq	120(%rdi), %rdx
	movslq	%esi, %rsi
	shlq	$6, %rsi
	movups	(%rcx,%rax), %xmm0
	movups	16(%rcx,%rax), %xmm1
	movups	32(%rcx,%rax), %xmm2
	movups	48(%rcx,%rax), %xmm3
	movups	%xmm3, 48(%rdx,%rsi)
	movups	%xmm2, 32(%rdx,%rsi)
	movups	%xmm1, 16(%rdx,%rsi)
	movups	%xmm0, (%rdx,%rsi)
	retq
.Lfunc_end7:
	.size	_ZN14btQuantizedBvh30assignInternalNodeFromLeafNodeEii, .Lfunc_end7-_ZN14btQuantizedBvh30assignInternalNodeFromLeafNodeEii
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_1:
	.long	1056964608              # float 0.5
.LCPI8_2:
	.long	1065353216              # float 1
.LCPI8_3:
	.long	3212836864              # float -1
	.text
	.globl	_ZN14btQuantizedBvh17calcSplittingAxisEii
	.p2align	4, 0x90
	.type	_ZN14btQuantizedBvh17calcSplittingAxisEii,@function
_ZN14btQuantizedBvh17calcSplittingAxisEii: # @_ZN14btQuantizedBvh17calcSplittingAxisEii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 24
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movl	%edx, %r8d
	subl	%esi, %r8d
	xorps	%xmm3, %xmm3
	movaps	%xmm3, -24(%rsp)
	jle	.LBB8_1
# BB#2:                                 # %.lr.ph187
	movb	64(%rdi), %r9b
	movslq	%esi, %r11
	movslq	%edx, %r10
	movq	%r11, %rax
	shlq	$4, %rax
	movq	%r10, %rcx
	subq	%r11, %rcx
	xorps	%xmm4, %xmm4
	movss	.LCPI8_1(%rip), %xmm10  # xmm10 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB8_3:                                # =>This Inner Loop Header: Depth=1
	testb	%r9b, %r9b
	je	.LBB8_5
# BB#4:                                 #   in Loop: Header=BB8_3 Depth=1
	movq	152(%rdi), %rbx
	movzwl	6(%rbx,%rax), %ebp
	cvtsi2ssl	%ebp, %xmm2
	movss	40(%rdi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	divss	%xmm8, %xmm2
	movzwl	8(%rbx,%rax), %ebp
	cvtsi2ssl	%ebp, %xmm1
	movss	44(%rdi), %xmm11        # xmm11 = mem[0],zero,zero,zero
	divss	%xmm11, %xmm1
	movzwl	10(%rbx,%rax), %ebp
	cvtsi2ssl	%ebp, %xmm6
	movss	48(%rdi), %xmm13        # xmm13 = mem[0],zero,zero,zero
	divss	%xmm13, %xmm6
	movss	8(%rdi), %xmm14         # xmm14 = mem[0],zero,zero,zero
	movss	12(%rdi), %xmm12        # xmm12 = mem[0],zero,zero,zero
	addss	%xmm14, %xmm2
	addss	%xmm12, %xmm1
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movss	16(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm6
	xorps	%xmm5, %xmm5
	movss	%xmm6, %xmm5            # xmm5 = xmm6[0],xmm5[1,2,3]
	movzwl	(%rbx,%rax), %ebp
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%ebp, %xmm6
	divss	%xmm8, %xmm6
	movzwl	2(%rbx,%rax), %ebp
	cvtsi2ssl	%ebp, %xmm0
	divss	%xmm11, %xmm0
	movzwl	4(%rbx,%rax), %ebx
	cvtsi2ssl	%ebx, %xmm7
	divss	%xmm13, %xmm7
	addss	%xmm14, %xmm6
	addss	%xmm12, %xmm0
	unpcklps	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	addss	%xmm1, %xmm7
	xorps	%xmm0, %xmm0
	movss	%xmm7, %xmm0            # xmm0 = xmm7[0],xmm0[1,2,3]
	jmp	.LBB8_6
	.p2align	4, 0x90
.LBB8_5:                                #   in Loop: Header=BB8_3 Depth=1
	movq	88(%rdi), %rbx
	movsd	16(%rbx,%rax,4), %xmm2  # xmm2 = mem[0],zero
	movsd	24(%rbx,%rax,4), %xmm5  # xmm5 = mem[0],zero
	movsd	(%rbx,%rax,4), %xmm6    # xmm6 = mem[0],zero
	movsd	8(%rbx,%rax,4), %xmm0   # xmm0 = mem[0],zero
.LBB8_6:                                # %_ZNK14btQuantizedBvh10getAabbMinEi.exit80
                                        #   in Loop: Header=BB8_3 Depth=1
	addps	%xmm6, %xmm2
	addss	%xmm0, %xmm5
	mulps	.LCPI8_0(%rip), %xmm2
	mulss	%xmm10, %xmm5
	addps	%xmm2, %xmm3
	addss	%xmm5, %xmm4
	addq	$16, %rax
	decq	%rcx
	jne	.LBB8_3
# BB#7:                                 # %._crit_edge188
	cmpl	%esi, %edx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%r8d, %xmm1
	jle	.LBB8_8
# BB#9:                                 # %.lr.ph
	movss	.LCPI8_2(%rip), %xmm12  # xmm12 = mem[0],zero,zero,zero
	movss	%xmm1, -4(%rsp)         # 4-byte Spill
	divss	%xmm1, %xmm12
	movaps	%xmm12, %xmm11
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	mulps	%xmm3, %xmm11
	mulss	%xmm4, %xmm12
	movb	64(%rdi), %dl
	subq	%r11, %r10
	shlq	$4, %r11
	xorps	%xmm9, %xmm9
	xorps	%xmm15, %xmm15
	.p2align	4, 0x90
.LBB8_10:                               # =>This Inner Loop Header: Depth=1
	testb	%dl, %dl
	je	.LBB8_12
# BB#11:                                #   in Loop: Header=BB8_10 Depth=1
	movq	152(%rdi), %rax
	movzwl	6(%rax,%r11), %ecx
	xorps	%xmm7, %xmm7
	cvtsi2ssl	%ecx, %xmm7
	movss	40(%rdi), %xmm13        # xmm13 = mem[0],zero,zero,zero
	divss	%xmm13, %xmm7
	movzwl	8(%rax,%r11), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	movss	44(%rdi), %xmm14        # xmm14 = mem[0],zero,zero,zero
	divss	%xmm14, %xmm0
	movzwl	10(%rax,%r11), %ecx
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%ecx, %xmm5
	movss	48(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm5
	movss	8(%rdi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	12(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm7
	addss	%xmm3, %xmm0
	unpcklps	%xmm0, %xmm7    # xmm7 = xmm7[0],xmm0[0],xmm7[1],xmm0[1]
	movss	16(%rdi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	addss	%xmm8, %xmm5
	xorps	%xmm2, %xmm2
	movss	%xmm5, %xmm2            # xmm2 = xmm5[0],xmm2[1,2,3]
	movzwl	(%rax,%r11), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	divss	%xmm13, %xmm0
	movzwl	2(%rax,%r11), %ecx
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%ecx, %xmm5
	divss	%xmm14, %xmm5
	movzwl	4(%rax,%r11), %eax
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%eax, %xmm4
	divss	%xmm1, %xmm4
	addss	%xmm6, %xmm0
	addss	%xmm3, %xmm5
	unpcklps	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1]
	addss	%xmm8, %xmm4
	xorps	%xmm1, %xmm1
	movss	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1,2,3]
	jmp	.LBB8_13
	.p2align	4, 0x90
.LBB8_12:                               #   in Loop: Header=BB8_10 Depth=1
	movq	88(%rdi), %rax
	movsd	16(%rax,%r11,4), %xmm7  # xmm7 = mem[0],zero
	movsd	24(%rax,%r11,4), %xmm2  # xmm2 = mem[0],zero
	movsd	(%rax,%r11,4), %xmm0    # xmm0 = mem[0],zero
	movsd	8(%rax,%r11,4), %xmm1   # xmm1 = mem[0],zero
.LBB8_13:                               # %_ZNK14btQuantizedBvh10getAabbMinEi.exit
                                        #   in Loop: Header=BB8_10 Depth=1
	addps	%xmm0, %xmm7
	addss	%xmm1, %xmm2
	mulps	.LCPI8_0(%rip), %xmm7
	mulss	%xmm10, %xmm2
	subps	%xmm11, %xmm7
	subss	%xmm12, %xmm2
	mulps	%xmm7, %xmm7
	mulss	%xmm2, %xmm2
	addps	%xmm7, %xmm9
	addss	%xmm2, %xmm15
	addq	$16, %r11
	decq	%r10
	jne	.LBB8_10
# BB#14:                                # %._crit_edge
	movss	%xmm9, -24(%rsp)
	movaps	%xmm9, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	%xmm0, -20(%rsp)
	movss	%xmm15, -16(%rsp)
	movss	-4(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	jmp	.LBB8_15
.LBB8_1:                                # %._crit_edge188.thread
	cvtsi2ssl	%r8d, %xmm1
.LBB8_8:                                # %._crit_edge188._crit_edge
	xorps	%xmm9, %xmm9
	xorps	%xmm15, %xmm15
.LBB8_15:
	addss	.LCPI8_3(%rip), %xmm1
	movss	.LCPI8_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm9, %xmm1
	movss	%xmm1, -24(%rsp)
	movaps	%xmm1, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movss	%xmm2, -20(%rsp)
	mulss	%xmm15, %xmm0
	movss	%xmm0, -16(%rsp)
	xorl	%ecx, %ecx
	ucomiss	%xmm1, %xmm2
	seta	%cl
	ucomiss	-24(%rsp,%rcx,4), %xmm0
	movl	$2, %eax
	cmovbel	%ecx, %eax
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN14btQuantizedBvh17calcSplittingAxisEii, .Lfunc_end8-_ZN14btQuantizedBvh17calcSplittingAxisEii
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_0:
	.long	1056964608              # float 0.5
.LCPI9_1:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_2:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
	.text
	.globl	_ZN14btQuantizedBvh25sortAndCalcSplittingIndexEiii
	.p2align	4, 0x90
	.type	_ZN14btQuantizedBvh25sortAndCalcSplittingIndexEiii,@function
_ZN14btQuantizedBvh25sortAndCalcSplittingIndexEiii: # @_ZN14btQuantizedBvh25sortAndCalcSplittingIndexEiii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -32
.Lcfi44:
	.cfi_offset %r14, -24
.Lcfi45:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	%edx, %r8d
	subl	%esi, %r8d
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -96(%rsp)
	jle	.LBB9_1
# BB#2:                                 # %.lr.ph157
	movb	64(%rdi), %r9b
	movslq	%esi, %rax
	movslq	%edx, %r10
	subq	%rax, %r10
	shlq	$4, %rax
	xorps	%xmm11, %xmm11
	movss	.LCPI9_0(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB9_3:                                # =>This Inner Loop Header: Depth=1
	testb	%r9b, %r9b
	je	.LBB9_5
# BB#4:                                 #   in Loop: Header=BB9_3 Depth=1
	movq	152(%rdi), %rbx
	movzwl	6(%rbx,%rax), %ebp
	cvtsi2ssl	%ebp, %xmm4
	movss	40(%rdi), %xmm9         # xmm9 = mem[0],zero,zero,zero
	divss	%xmm9, %xmm4
	movzwl	8(%rbx,%rax), %ebp
	cvtsi2ssl	%ebp, %xmm5
	movss	44(%rdi), %xmm10        # xmm10 = mem[0],zero,zero,zero
	divss	%xmm10, %xmm5
	movzwl	10(%rbx,%rax), %ebp
	cvtsi2ssl	%ebp, %xmm6
	movss	48(%rdi), %xmm13        # xmm13 = mem[0],zero,zero,zero
	divss	%xmm13, %xmm6
	movss	8(%rdi), %xmm14         # xmm14 = mem[0],zero,zero,zero
	movss	12(%rdi), %xmm12        # xmm12 = mem[0],zero,zero,zero
	addss	%xmm14, %xmm4
	addss	%xmm12, %xmm5
	unpcklps	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	movss	16(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm6
	xorps	%xmm5, %xmm5
	movss	%xmm6, %xmm5            # xmm5 = xmm6[0],xmm5[1,2,3]
	movzwl	(%rbx,%rax), %ebp
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%ebp, %xmm6
	divss	%xmm9, %xmm6
	movzwl	2(%rbx,%rax), %ebp
	cvtsi2ssl	%ebp, %xmm7
	divss	%xmm10, %xmm7
	movzwl	4(%rbx,%rax), %ebp
	cvtsi2ssl	%ebp, %xmm3
	divss	%xmm13, %xmm3
	addss	%xmm14, %xmm6
	addss	%xmm12, %xmm7
	unpcklps	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0],xmm6[1],xmm7[1]
	addss	%xmm0, %xmm3
	xorps	%xmm7, %xmm7
	movss	%xmm3, %xmm7            # xmm7 = xmm3[0],xmm7[1,2,3]
	jmp	.LBB9_6
	.p2align	4, 0x90
.LBB9_5:                                #   in Loop: Header=BB9_3 Depth=1
	movq	88(%rdi), %rbp
	movsd	16(%rbp,%rax,4), %xmm4  # xmm4 = mem[0],zero
	movsd	24(%rbp,%rax,4), %xmm5  # xmm5 = mem[0],zero
	movsd	(%rbp,%rax,4), %xmm6    # xmm6 = mem[0],zero
	movsd	8(%rbp,%rax,4), %xmm7   # xmm7 = mem[0],zero
.LBB9_6:                                # %_ZNK14btQuantizedBvh10getAabbMinEi.exit93
                                        #   in Loop: Header=BB9_3 Depth=1
	movaps	%xmm4, %xmm0
	addss	%xmm6, %xmm0
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	addss	%xmm4, %xmm6
	addss	%xmm7, %xmm5
	mulss	%xmm8, %xmm0
	mulss	%xmm8, %xmm6
	mulss	%xmm8, %xmm5
	addss	%xmm0, %xmm2
	addss	%xmm6, %xmm1
	addss	%xmm5, %xmm11
	addq	$16, %rax
	decq	%r10
	jne	.LBB9_3
# BB#7:                                 # %._crit_edge158
	movss	%xmm2, -96(%rsp)
	movss	%xmm1, -92(%rsp)
	movss	%xmm11, -88(%rsp)
	jmp	.LBB9_8
.LBB9_1:                                # %._crit_edge169
	xorps	%xmm11, %xmm11
	xorps	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
.LBB9_8:
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r8d, %xmm0
	movss	.LCPI9_1(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm3
	mulss	%xmm3, %xmm2
	movss	%xmm2, -96(%rsp)
	mulss	%xmm3, %xmm1
	movss	%xmm1, -92(%rsp)
	mulss	%xmm11, %xmm3
	movss	%xmm3, -88(%rsp)
	cmpl	%esi, %edx
	movl	%esi, %eax
	jle	.LBB9_19
# BB#9:                                 # %.lr.ph
	movslq	%ecx, %r9
	movss	-96(%rsp,%r9,4), %xmm10 # xmm10 = mem[0],zero,zero,zero
	movslq	%esi, %r11
	movslq	%edx, %r10
	movq	%r11, %rcx
	shlq	$4, %rcx
	subq	%r11, %r10
	shlq	$6, %r11
	movaps	.LCPI9_2(%rip), %xmm8   # xmm8 = <0.5,0.5,u,u>
	movss	.LCPI9_0(%rip), %xmm9   # xmm9 = mem[0],zero,zero,zero
	movl	%esi, %eax
	.p2align	4, 0x90
.LBB9_10:                               # =>This Inner Loop Header: Depth=1
	movzbl	64(%rdi), %r14d
	testb	%r14b, %r14b
	je	.LBB9_12
# BB#11:                                #   in Loop: Header=BB9_10 Depth=1
	movq	152(%rdi), %rbx
	movzwl	6(%rbx,%rcx), %ebp
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ebp, %xmm3
	movss	40(%rdi), %xmm11        # xmm11 = mem[0],zero,zero,zero
	divss	%xmm11, %xmm3
	movzwl	8(%rbx,%rcx), %ebp
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%ebp, %xmm4
	movss	44(%rdi), %xmm12        # xmm12 = mem[0],zero,zero,zero
	divss	%xmm12, %xmm4
	movzwl	10(%rbx,%rcx), %ebp
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%ebp, %xmm5
	movss	48(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm5
	movss	8(%rdi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	12(%rdi), %xmm13        # xmm13 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm3
	addss	%xmm13, %xmm4
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	movss	16(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm5
	xorps	%xmm4, %xmm4
	movss	%xmm5, %xmm4            # xmm4 = xmm5[0],xmm4[1,2,3]
	movzwl	(%rbx,%rcx), %ebp
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%ebp, %xmm5
	divss	%xmm11, %xmm5
	movzwl	2(%rbx,%rcx), %ebp
	cvtsi2ssl	%ebp, %xmm7
	divss	%xmm12, %xmm7
	movzwl	4(%rbx,%rcx), %ebx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebx, %xmm0
	divss	%xmm1, %xmm0
	addss	%xmm2, %xmm5
	addss	%xmm13, %xmm7
	unpcklps	%xmm7, %xmm5    # xmm5 = xmm5[0],xmm7[0],xmm5[1],xmm7[1]
	addss	%xmm6, %xmm0
	xorps	%xmm6, %xmm6
	movss	%xmm0, %xmm6            # xmm6 = xmm0[0],xmm6[1,2,3]
	jmp	.LBB9_13
	.p2align	4, 0x90
.LBB9_12:                               #   in Loop: Header=BB9_10 Depth=1
	movq	88(%rdi), %rbx
	movsd	16(%rbx,%rcx,4), %xmm3  # xmm3 = mem[0],zero
	movsd	24(%rbx,%rcx,4), %xmm4  # xmm4 = mem[0],zero
	movsd	(%rbx,%rcx,4), %xmm5    # xmm5 = mem[0],zero
	movsd	8(%rbx,%rcx,4), %xmm6   # xmm6 = mem[0],zero
.LBB9_13:                               # %_ZNK14btQuantizedBvh10getAabbMinEi.exit
                                        #   in Loop: Header=BB9_10 Depth=1
	addps	%xmm5, %xmm3
	addss	%xmm6, %xmm4
	mulps	%xmm8, %xmm3
	mulss	%xmm9, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm3, -80(%rsp)
	movlps	%xmm0, -72(%rsp)
	movss	-80(%rsp,%r9,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm10, %xmm0
	jbe	.LBB9_18
# BB#14:                                #   in Loop: Header=BB9_10 Depth=1
	testb	%r14b, %r14b
	je	.LBB9_16
# BB#15:                                #   in Loop: Header=BB9_10 Depth=1
	movq	152(%rdi), %rbx
	movups	(%rbx,%rcx), %xmm0
	movaps	%xmm0, -64(%rsp)
	movslq	%eax, %rbp
	shlq	$4, %rbp
	movups	(%rbx,%rbp), %xmm0
	movups	%xmm0, (%rbx,%rcx)
	movq	152(%rdi), %rbx
	movaps	-64(%rsp), %xmm0
	jmp	.LBB9_17
.LBB9_16:                               #   in Loop: Header=BB9_10 Depth=1
	movq	88(%rdi), %rbx
	movups	(%rbx,%r11), %xmm0
	movups	16(%rbx,%r11), %xmm1
	movups	32(%rbx,%r11), %xmm2
	movups	48(%rbx,%r11), %xmm3
	movaps	%xmm3, -16(%rsp)
	movaps	%xmm2, -32(%rsp)
	movaps	%xmm1, -48(%rsp)
	movaps	%xmm0, -64(%rsp)
	movslq	%eax, %rbp
	shlq	$6, %rbp
	movups	(%rbx,%rbp), %xmm0
	movups	16(%rbx,%rbp), %xmm1
	movups	32(%rbx,%rbp), %xmm2
	movups	48(%rbx,%rbp), %xmm3
	movups	%xmm3, 48(%rbx,%r11)
	movups	%xmm2, 32(%rbx,%r11)
	movups	%xmm1, 16(%rbx,%r11)
	movups	%xmm0, (%rbx,%r11)
	movq	88(%rdi), %rbx
	movaps	-64(%rsp), %xmm0
	movaps	-48(%rsp), %xmm1
	movaps	-32(%rsp), %xmm2
	movaps	-16(%rsp), %xmm3
	movups	%xmm3, 48(%rbx,%rbp)
	movups	%xmm2, 32(%rbx,%rbp)
	movups	%xmm1, 16(%rbx,%rbp)
.LBB9_17:                               # %_ZN14btQuantizedBvh13swapLeafNodesEii.exit
                                        #   in Loop: Header=BB9_10 Depth=1
	movups	%xmm0, (%rbx,%rbp)
	incl	%eax
.LBB9_18:                               #   in Loop: Header=BB9_10 Depth=1
	addq	$16, %rcx
	addq	$64, %r11
	decq	%r10
	jne	.LBB9_10
.LBB9_19:                               # %._crit_edge
	movslq	%r8d, %rcx
	imulq	$1431655766, %rcx, %rcx # imm = 0x55555556
	movq	%rcx, %rdi
	shrq	$63, %rdi
	shrq	$32, %rcx
	addl	%edi, %ecx
	leal	(%rcx,%rsi), %edi
	cmpl	%edi, %eax
	jle	.LBB9_21
# BB#20:
	decl	%edx
	subl	%ecx, %edx
	cmpl	%edx, %eax
	jl	.LBB9_22
.LBB9_21:                               # %.critedge
	sarl	%r8d
	addl	%esi, %r8d
	movl	%r8d, %eax
.LBB9_22:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end9:
	.size	_ZN14btQuantizedBvh25sortAndCalcSplittingIndexEiii, .Lfunc_end9-_ZN14btQuantizedBvh25sortAndCalcSplittingIndexEiii
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI10_0:
	.long	1065353216              # float 1
	.section	.text._ZN14btQuantizedBvh21mergeInternalNodeAabbEiRK9btVector3S2_,"axG",@progbits,_ZN14btQuantizedBvh21mergeInternalNodeAabbEiRK9btVector3S2_,comdat
	.weak	_ZN14btQuantizedBvh21mergeInternalNodeAabbEiRK9btVector3S2_
	.p2align	4, 0x90
	.type	_ZN14btQuantizedBvh21mergeInternalNodeAabbEiRK9btVector3S2_,@function
_ZN14btQuantizedBvh21mergeInternalNodeAabbEiRK9btVector3S2_: # @_ZN14btQuantizedBvh21mergeInternalNodeAabbEiRK9btVector3S2_
	.cfi_startproc
# BB#0:
	cmpb	$0, 64(%rdi)
	je	.LBB10_13
# BB#1:
	movss	(%rdx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movss	12(%rdi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	subss	%xmm9, %xmm1
	subss	%xmm8, %xmm2
	movss	8(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	16(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm0
	movss	40(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm1
	movss	44(%rdi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	movss	48(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	cvttss2si	%xmm1, %r10d
	andl	$65534, %r10d           # imm = 0xFFFE
	cvttss2si	%xmm2, %r8d
	andl	$65534, %r8d            # imm = 0xFFFE
	movss	(%rcx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	subss	%xmm9, %xmm2
	subss	%xmm8, %xmm3
	movss	8(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm1
	mulss	%xmm6, %xmm2
	mulss	%xmm7, %xmm3
	mulss	%xmm4, %xmm1
	movss	.LCPI10_0(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm2
	cvttss2si	%xmm2, %eax
	orl	$1, %eax
	addss	%xmm4, %xmm3
	cvttss2si	%xmm3, %r9d
	movq	184(%rdi), %rcx
	movslq	%esi, %rdx
	shlq	$4, %rdx
	cmpw	%r10w, (%rcx,%rdx)
	jbe	.LBB10_3
# BB#2:
	leaq	(%rcx,%rdx), %rsi
	movw	%r10w, (%rsi)
.LBB10_3:
	addss	%xmm4, %xmm1
	cvttss2si	%xmm0, %edi
	orl	$1, %r9d
	cmpw	%ax, 6(%rcx,%rdx)
	jae	.LBB10_5
# BB#4:
	leaq	6(%rcx,%rdx), %rsi
	movw	%ax, (%rsi)
.LBB10_5:
	cvttss2si	%xmm1, %esi
	andl	$65534, %edi            # imm = 0xFFFE
	cmpw	%r8w, 2(%rcx,%rdx)
	jbe	.LBB10_7
# BB#6:
	leaq	2(%rcx,%rdx), %rax
	movw	%r8w, (%rax)
.LBB10_7:
	orl	$1, %esi
	cmpw	%r9w, 8(%rcx,%rdx)
	jae	.LBB10_9
# BB#8:
	leaq	8(%rcx,%rdx), %rax
	movw	%r9w, (%rax)
.LBB10_9:
	cmpw	%di, 4(%rcx,%rdx)
	jbe	.LBB10_11
# BB#10:
	leaq	4(%rcx,%rdx), %rax
	movw	%di, (%rax)
.LBB10_11:
	cmpw	%si, 10(%rcx,%rdx)
	jae	.LBB10_29
# BB#12:
	leaq	10(%rcx,%rdx), %rax
	movw	%si, (%rax)
	retq
.LBB10_13:
	movq	120(%rdi), %rax
	movslq	%esi, %rsi
	shlq	$6, %rsi
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	(%rax,%rsi), %xmm1      # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB10_15
# BB#14:
	leaq	(%rax,%rsi), %rdi
	movss	%xmm0, (%rdi)
.LBB10_15:                              # %_Z8btSetMinIfEvRT_RKS0_.exit.i
	movss	4(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	4(%rax,%rsi), %xmm1     # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB10_17
# BB#16:
	leaq	4(%rax,%rsi), %rdi
	movss	%xmm0, (%rdi)
.LBB10_17:                              # %_Z8btSetMinIfEvRT_RKS0_.exit7.i
	movss	8(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%rax,%rsi), %xmm1     # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB10_19
# BB#18:
	leaq	8(%rax,%rsi), %rdi
	movss	%xmm0, (%rdi)
.LBB10_19:                              # %_Z8btSetMinIfEvRT_RKS0_.exit6.i
	movss	12(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	12(%rax,%rsi), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB10_21
# BB#20:
	leaq	12(%rax,%rsi), %rdx
	movss	%xmm0, (%rdx)
.LBB10_21:                              # %_ZN9btVector36setMinERKS_.exit
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	16(%rax,%rsi), %xmm0
	jbe	.LBB10_23
# BB#22:
	leaq	16(%rax,%rsi), %rdx
	movss	%xmm0, (%rdx)
.LBB10_23:                              # %_Z8btSetMaxIfEvRT_RKS0_.exit.i
	movss	4(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	20(%rax,%rsi), %xmm0
	jbe	.LBB10_25
# BB#24:
	leaq	20(%rax,%rsi), %rdx
	movss	%xmm0, (%rdx)
.LBB10_25:                              # %_Z8btSetMaxIfEvRT_RKS0_.exit7.i
	movss	8(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	24(%rax,%rsi), %xmm0
	jbe	.LBB10_27
# BB#26:
	leaq	24(%rax,%rsi), %rdx
	movss	%xmm0, (%rdx)
.LBB10_27:                              # %_Z8btSetMaxIfEvRT_RKS0_.exit6.i
	movss	12(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	28(%rax,%rsi), %xmm0
	jbe	.LBB10_29
# BB#28:
	leaq	28(%rax,%rsi), %rax
	movss	%xmm0, (%rax)
.LBB10_29:                              # %_ZN9btVector36setMaxERKS_.exit
	retq
.Lfunc_end10:
	.size	_ZN14btQuantizedBvh21mergeInternalNodeAabbEiRK9btVector3S2_, .Lfunc_end10-_ZN14btQuantizedBvh21mergeInternalNodeAabbEiRK9btVector3S2_
	.cfi_endproc

	.text
	.globl	_ZN14btQuantizedBvh20updateSubtreeHeadersEii
	.p2align	4, 0x90
	.type	_ZN14btQuantizedBvh20updateSubtreeHeadersEii,@function
_ZN14btQuantizedBvh20updateSubtreeHeadersEii: # @_ZN14btQuantizedBvh20updateSubtreeHeadersEii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi52:
	.cfi_def_cfa_offset 128
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movl	%esi, %r9d
	movq	%rdi, %rbx
	movq	184(%rbx), %r12
	movslq	%r9d, %r15
	shlq	$4, %r15
	movl	12(%r12,%r15), %eax
	movl	%eax, %r14d
	negl	%r14d
	testl	%eax, %eax
	movl	$1, %eax
	cmovnsl	%eax, %r14d
	movl	%r14d, %ecx
	shll	$4, %ecx
	movslq	%edx, %rdi
	shlq	$4, %rdi
	movl	12(%r12,%rdi), %esi
	movl	%esi, %ebp
	negl	%ebp
	testl	%esi, %esi
	cmovnsl	%eax, %ebp
	movl	%ebp, %r10d
	shll	$4, %r10d
	cmpl	$2048, %ecx             # imm = 0x800
	jg	.LBB11_20
# BB#1:
	movl	212(%rbx), %eax
	movslq	%eax, %r13
	cmpl	216(%rbx), %r13d
	jne	.LBB11_19
# BB#2:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %r11d
	cmovnel	%ecx, %r11d
	cmpl	%r11d, %eax
	jge	.LBB11_19
# BB#3:
	testl	%r11d, %r11d
	movl	%edx, 36(%rsp)          # 4-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movl	%r9d, 20(%rsp)          # 4-byte Spill
	movl	%r10d, 16(%rsp)         # 4-byte Spill
	je	.LBB11_4
# BB#5:
	movslq	%r11d, %rdi
	shlq	$5, %rdi
	movl	$16, %esi
	movl	%r11d, %ebp
	callq	_Z22btAlignedAllocInternalmi
	movl	%ebp, %r11d
	movl	16(%rsp), %r10d         # 4-byte Reload
	movl	20(%rsp), %r9d          # 4-byte Reload
	movq	%rax, %rbp
	movl	212(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB11_7
	jmp	.LBB11_14
.LBB11_4:
	xorl	%ebp, %ebp
	testl	%eax, %eax
	jle	.LBB11_14
.LBB11_7:                               # %.lr.ph.i.i.i41
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB11_8
# BB#9:                                 # %.prol.preheader53
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_10:                              # =>This Inner Loop Header: Depth=1
	movq	224(%rbx), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	16(%rdx,%rdi), %xmm1
	movups	%xmm1, 16(%rbp,%rdi)
	movups	%xmm0, (%rbp,%rdi)
	incq	%rcx
	addq	$32, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB11_10
	jmp	.LBB11_11
.LBB11_8:
	xorl	%ecx, %ecx
.LBB11_11:                              # %.prol.loopexit54
	cmpq	$3, %r8
	jb	.LBB11_14
# BB#12:                                # %.lr.ph.i.i.i41.new
	subq	%rcx, %rax
	shlq	$5, %rcx
	addq	$96, %rcx
	.p2align	4, 0x90
.LBB11_13:                              # =>This Inner Loop Header: Depth=1
	movq	224(%rbx), %rdx
	movups	-96(%rdx,%rcx), %xmm0
	movups	-80(%rdx,%rcx), %xmm1
	movups	%xmm1, -80(%rbp,%rcx)
	movups	%xmm0, -96(%rbp,%rcx)
	movq	224(%rbx), %rdx
	movups	-64(%rdx,%rcx), %xmm0
	movups	-48(%rdx,%rcx), %xmm1
	movups	%xmm1, -48(%rbp,%rcx)
	movups	%xmm0, -64(%rbp,%rcx)
	movq	224(%rbx), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	-16(%rdx,%rcx), %xmm1
	movups	%xmm1, -16(%rbp,%rcx)
	movups	%xmm0, -32(%rbp,%rcx)
	movq	224(%rbx), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	16(%rdx,%rcx), %xmm1
	movups	%xmm1, 16(%rbp,%rcx)
	movups	%xmm0, (%rbp,%rcx)
	subq	$-128, %rcx
	addq	$-4, %rax
	jne	.LBB11_13
.LBB11_14:                              # %_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4copyEiiPS0_.exit.i.i46
	movq	224(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_18
# BB#15:
	cmpb	$0, 232(%rbx)
	je	.LBB11_17
# BB#16:
	movl	%r11d, 32(%rsp)         # 4-byte Spill
	callq	_Z21btAlignedFreeInternalPv
	movl	32(%rsp), %r11d         # 4-byte Reload
	movl	16(%rsp), %r10d         # 4-byte Reload
	movl	20(%rsp), %r9d          # 4-byte Reload
.LBB11_17:
	movq	$0, 224(%rbx)
.LBB11_18:                              # %_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE10deallocateEv.exit.i.i48
	movb	$1, 232(%rbx)
	movq	%rbp, 224(%rbx)
	movl	%r11d, 216(%rbx)
	movl	212(%rbx), %eax
	movl	36(%rsp), %edx          # 4-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	12(%rsp), %ebp          # 4-byte Reload
.LBB11_19:                              # %_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_.exit49
	incl	%eax
	movl	%eax, 212(%rbx)
	movq	224(%rbx), %rax
	shlq	$5, %r13
	movups	40(%rsp), %xmm0
	movups	56(%rsp), %xmm1
	movups	%xmm1, 16(%rax,%r13)
	movups	%xmm0, (%rax,%r13)
	movq	224(%rbx), %rax
	movzwl	(%r12,%r15), %ecx
	movw	%cx, (%rax,%r13)
	movzwl	2(%r12,%r15), %ecx
	movw	%cx, 2(%rax,%r13)
	movzwl	4(%r12,%r15), %ecx
	movw	%cx, 4(%rax,%r13)
	movzwl	6(%r12,%r15), %ecx
	movw	%cx, 6(%rax,%r13)
	movzwl	8(%r12,%r15), %ecx
	movw	%cx, 8(%rax,%r13)
	movzwl	10(%r12,%r15), %ecx
	movw	%cx, 10(%rax,%r13)
	movl	%r9d, 12(%rax,%r13)
	movl	%r14d, 16(%rax,%r13)
.LBB11_20:
	cmpl	$2049, %r10d            # imm = 0x801
	jge	.LBB11_40
# BB#21:
	movl	212(%rbx), %eax
	movslq	%eax, %r14
	cmpl	216(%rbx), %r14d
	jne	.LBB11_39
# BB#22:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %r15d
	cmovnel	%ecx, %r15d
	cmpl	%r15d, %eax
	jge	.LBB11_39
# BB#23:
	testl	%r15d, %r15d
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movl	%edx, %r13d
	je	.LBB11_24
# BB#25:
	movslq	%r15d, %rdi
	shlq	$5, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
	movl	212(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB11_27
	jmp	.LBB11_34
.LBB11_24:
	xorl	%ebp, %ebp
	testl	%eax, %eax
	jle	.LBB11_34
.LBB11_27:                              # %.lr.ph.i.i.i
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB11_28
# BB#29:                                # %.prol.preheader
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_30:                              # =>This Inner Loop Header: Depth=1
	movq	224(%rbx), %rdx
	movups	(%rdx,%rdi), %xmm0
	movups	16(%rdx,%rdi), %xmm1
	movups	%xmm1, 16(%rbp,%rdi)
	movups	%xmm0, (%rbp,%rdi)
	incq	%rcx
	addq	$32, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB11_30
	jmp	.LBB11_31
.LBB11_28:
	xorl	%ecx, %ecx
.LBB11_31:                              # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB11_34
# BB#32:                                # %.lr.ph.i.i.i.new
	subq	%rcx, %rax
	shlq	$5, %rcx
	addq	$96, %rcx
	.p2align	4, 0x90
.LBB11_33:                              # =>This Inner Loop Header: Depth=1
	movq	224(%rbx), %rdx
	movups	-96(%rdx,%rcx), %xmm0
	movups	-80(%rdx,%rcx), %xmm1
	movups	%xmm1, -80(%rbp,%rcx)
	movups	%xmm0, -96(%rbp,%rcx)
	movq	224(%rbx), %rdx
	movups	-64(%rdx,%rcx), %xmm0
	movups	-48(%rdx,%rcx), %xmm1
	movups	%xmm1, -48(%rbp,%rcx)
	movups	%xmm0, -64(%rbp,%rcx)
	movq	224(%rbx), %rdx
	movups	-32(%rdx,%rcx), %xmm0
	movups	-16(%rdx,%rcx), %xmm1
	movups	%xmm1, -16(%rbp,%rcx)
	movups	%xmm0, -32(%rbp,%rcx)
	movq	224(%rbx), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	16(%rdx,%rcx), %xmm1
	movups	%xmm1, 16(%rbp,%rcx)
	movups	%xmm0, (%rbp,%rcx)
	subq	$-128, %rcx
	addq	$-4, %rax
	jne	.LBB11_33
.LBB11_34:                              # %_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4copyEiiPS0_.exit.i.i
	movq	224(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_38
# BB#35:
	cmpb	$0, 232(%rbx)
	je	.LBB11_37
# BB#36:
	callq	_Z21btAlignedFreeInternalPv
.LBB11_37:
	movq	$0, 224(%rbx)
.LBB11_38:                              # %_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE10deallocateEv.exit.i.i
	movb	$1, 232(%rbx)
	movq	%rbp, 224(%rbx)
	movl	%r15d, 216(%rbx)
	movl	212(%rbx), %eax
	movl	%r13d, %edx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	12(%rsp), %ebp          # 4-byte Reload
.LBB11_39:                              # %_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_.exit
	incl	%eax
	movl	%eax, 212(%rbx)
	movq	224(%rbx), %rax
	shlq	$5, %r14
	movups	40(%rsp), %xmm0
	movups	56(%rsp), %xmm1
	movups	%xmm1, 16(%rax,%r14)
	movups	%xmm0, (%rax,%r14)
	movq	224(%rbx), %rax
	movzwl	(%r12,%rdi), %ecx
	movw	%cx, (%rax,%r14)
	movzwl	2(%r12,%rdi), %ecx
	movw	%cx, 2(%rax,%r14)
	movzwl	4(%r12,%rdi), %ecx
	movw	%cx, 4(%rax,%r14)
	movzwl	6(%r12,%rdi), %ecx
	movw	%cx, 6(%rax,%r14)
	movzwl	8(%r12,%rdi), %ecx
	movw	%cx, 8(%rax,%r14)
	movzwl	10(%r12,%rdi), %ecx
	movw	%cx, 10(%rax,%r14)
	movl	%edx, 12(%rax,%r14)
	movl	%ebp, 16(%rax,%r14)
.LBB11_40:
	movl	212(%rbx), %eax
	movl	%eax, 240(%rbx)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZN14btQuantizedBvh20updateSubtreeHeadersEii, .Lfunc_end11-_ZN14btQuantizedBvh20updateSubtreeHeadersEii
	.cfi_endproc

	.globl	_ZN14btQuantizedBvh13swapLeafNodesEii
	.p2align	4, 0x90
	.type	_ZN14btQuantizedBvh13swapLeafNodesEii,@function
_ZN14btQuantizedBvh13swapLeafNodesEii:  # @_ZN14btQuantizedBvh13swapLeafNodesEii
	.cfi_startproc
# BB#0:
	cmpb	$0, 64(%rdi)
	je	.LBB12_2
# BB#1:
	movq	152(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	movups	(%rax,%rcx), %xmm0
	movaps	%xmm0, -72(%rsp)
	movslq	%edx, %rdx
	shlq	$4, %rdx
	movups	(%rax,%rdx), %xmm0
	movups	%xmm0, (%rax,%rcx)
	movq	152(%rdi), %rax
	movaps	-72(%rsp), %xmm0
	movups	%xmm0, (%rax,%rdx)
	retq
.LBB12_2:
	movq	88(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$6, %rcx
	movups	(%rax,%rcx), %xmm0
	movups	16(%rax,%rcx), %xmm1
	movups	32(%rax,%rcx), %xmm2
	movups	48(%rax,%rcx), %xmm3
	movaps	%xmm3, -24(%rsp)
	movaps	%xmm2, -40(%rsp)
	movaps	%xmm1, -56(%rsp)
	movaps	%xmm0, -72(%rsp)
	movslq	%edx, %rdx
	shlq	$6, %rdx
	movups	(%rax,%rdx), %xmm0
	movups	16(%rax,%rdx), %xmm1
	movups	32(%rax,%rdx), %xmm2
	movups	48(%rax,%rdx), %xmm3
	movups	%xmm3, 48(%rax,%rcx)
	movups	%xmm2, 32(%rax,%rcx)
	movups	%xmm1, 16(%rax,%rcx)
	movups	%xmm0, (%rax,%rcx)
	movq	88(%rdi), %rax
	movaps	-72(%rsp), %xmm0
	movaps	-56(%rsp), %xmm1
	movaps	-40(%rsp), %xmm2
	movaps	-24(%rsp), %xmm3
	movups	%xmm3, 48(%rax,%rdx)
	movups	%xmm2, 32(%rax,%rdx)
	movups	%xmm1, 16(%rax,%rdx)
	movups	%xmm0, (%rax,%rdx)
	retq
.Lfunc_end12:
	.size	_ZN14btQuantizedBvh13swapLeafNodesEii, .Lfunc_end12-_ZN14btQuantizedBvh13swapLeafNodesEii
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI13_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZNK14btQuantizedBvh26reportAabbOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK14btQuantizedBvh26reportAabbOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_,@function
_ZNK14btQuantizedBvh26reportAabbOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_: # @_ZNK14btQuantizedBvh26reportAabbOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi65:
	.cfi_def_cfa_offset 112
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, %r15
	cmpb	$0, 64(%r15)
	je	.LBB13_18
# BB#1:
	movss	8(%r15), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movss	12(%r15), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm0
	maxss	(%r13), %xmm0
	movaps	%xmm9, %xmm6
	maxss	4(%r13), %xmm6
	movss	16(%r15), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm7
	maxss	8(%r13), %xmm7
	movss	24(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	minss	%xmm0, %xmm2
	movss	28(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	minss	%xmm6, %xmm1
	movss	32(%r15), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	minss	%xmm7, %xmm0
	subss	%xmm10, %xmm2
	subss	%xmm9, %xmm1
	subss	%xmm5, %xmm0
	movss	40(%r15), %xmm8         # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	movss	44(%r15), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm1
	movss	48(%r15), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	cvttss2si	%xmm2, %esi
	andl	$65534, %esi            # imm = 0xFFFE
	movw	%si, 18(%rsp)
	cvttss2si	%xmm1, %edi
	andl	$65534, %edi            # imm = 0xFFFE
	cvttss2si	%xmm0, %r8d
	andl	$65534, %r8d            # imm = 0xFFFE
	movw	%di, 20(%rsp)
	movw	%r8w, 22(%rsp)
	movaps	%xmm10, %xmm0
	maxss	(%r12), %xmm0
	movaps	%xmm9, %xmm1
	maxss	4(%r12), %xmm1
	movaps	%xmm5, %xmm2
	maxss	8(%r12), %xmm2
	minss	%xmm0, %xmm3
	minss	%xmm1, %xmm4
	minss	%xmm2, %xmm6
	subss	%xmm10, %xmm3
	subss	%xmm9, %xmm4
	subss	%xmm5, %xmm6
	mulss	%xmm8, %xmm3
	mulss	%xmm11, %xmm4
	mulss	%xmm7, %xmm6
	movss	.LCPI13_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm3
	cvttss2si	%xmm3, %r9d
	orl	$1, %r9d
	movw	%r9w, 12(%rsp)
	addss	%xmm0, %xmm4
	cvttss2si	%xmm4, %r10d
	orl	$1, %r10d
	addss	%xmm0, %xmm6
	cvttss2si	%xmm6, %r11d
	orl	$1, %r11d
	movw	%r10w, 14(%rsp)
	movw	%r11w, 16(%rsp)
	movl	200(%r15), %eax
	cmpl	$2, %eax
	je	.LBB13_17
# BB#2:
	cmpl	$1, %eax
	je	.LBB13_16
# BB#3:
	testl	%eax, %eax
	jne	.LBB13_41
# BB#4:
	movl	60(%r15), %r13d
	testl	%r13d, %r13d
	jle	.LBB13_5
# BB#6:                                 # %.lr.ph.i23
	movq	184(%r15), %rbp
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	movl	%edi, 48(%rsp)          # 4-byte Spill
	movl	%esi, 44(%rsp)          # 4-byte Spill
	movl	%r10d, 40(%rsp)         # 4-byte Spill
	movl	%r8d, 36(%rsp)          # 4-byte Spill
	movl	%r13d, 32(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB13_7:                               # =>This Inner Loop Header: Depth=1
	cmpw	6(%rbp), %si
	setbe	%al
	cmpw	(%rbp), %r9w
	setae	%cl
	andb	%al, %cl
	cmpw	10(%rbp), %r8w
	setbe	%al
	cmpw	4(%rbp), %r11w
	setae	%dl
	andb	%al, %dl
	andb	%cl, %dl
	cmpw	8(%rbp), %di
	setbe	%al
	cmpw	2(%rbp), %r10w
	setae	%r12b
	andb	%al, %r12b
	andb	%dl, %r12b
	movl	12(%rbp), %r14d
	testl	%r14d, %r14d
	js	.LBB13_10
# BB#8:                                 #   in Loop: Header=BB13_7 Depth=1
	testb	%r12b, %r12b
	je	.LBB13_10
# BB#9:                                 #   in Loop: Header=BB13_7 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	%r14d, %esi
	shrl	$21, %esi
	movl	%r14d, %edx
	andl	$2097151, %edx          # imm = 0x1FFFFF
	movl	%r9d, %r13d
	movl	%r15d, 52(%rsp)         # 4-byte Spill
	movl	%r11d, %r15d
	callq	*16(%rax)
	movl	%r15d, %r11d
	movl	52(%rsp), %r15d         # 4-byte Reload
	movl	40(%rsp), %r10d         # 4-byte Reload
	movl	%r13d, %r9d
	movl	32(%rsp), %r13d         # 4-byte Reload
	movl	36(%rsp), %r8d          # 4-byte Reload
	movl	48(%rsp), %edi          # 4-byte Reload
	movl	44(%rsp), %esi          # 4-byte Reload
.LBB13_10:                              #   in Loop: Header=BB13_7 Depth=1
	testl	%r14d, %r14d
	setns	%al
	orb	%al, %r12b
	cmpb	$1, %r12b
	jne	.LBB13_12
# BB#11:                                #   in Loop: Header=BB13_7 Depth=1
	addq	$16, %rbp
	incl	%r15d
	jmp	.LBB13_13
	.p2align	4, 0x90
.LBB13_12:                              #   in Loop: Header=BB13_7 Depth=1
	movslq	12(%rbp), %rax
	subl	%eax, %r15d
	shlq	$4, %rax
	subq	%rax, %rbp
.LBB13_13:                              # %.backedge.i27
                                        #   in Loop: Header=BB13_7 Depth=1
	incl	%ebx
	cmpl	%r13d, %r15d
	jl	.LBB13_7
	jmp	.LBB13_14
.LBB13_18:
	movl	60(%r15), %esi
	testl	%esi, %esi
	jle	.LBB13_19
# BB#20:                                # %.lr.ph.i
	movq	120(%r15), %rbx
	xorl	%r14d, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_21:                              # =>This Inner Loop Header: Depth=1
	movss	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	16(%rbx), %xmm0
	jbe	.LBB13_23
# BB#22:                                #   in Loop: Header=BB13_21 Depth=1
	xorl	%edi, %edi
	jmp	.LBB13_26
	.p2align	4, 0x90
.LBB13_23:                              #   in Loop: Header=BB13_21 Depth=1
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%r12), %xmm0
	jbe	.LBB13_25
# BB#24:                                #   in Loop: Header=BB13_21 Depth=1
	xorl	%edi, %edi
	jmp	.LBB13_26
	.p2align	4, 0x90
.LBB13_25:                              #   in Loop: Header=BB13_21 Depth=1
	movb	$1, %dil
.LBB13_26:                              #   in Loop: Header=BB13_21 Depth=1
	movss	8(%r13), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	24(%rbx), %xmm0
	ja	.LBB13_28
# BB#27:                                #   in Loop: Header=BB13_21 Depth=1
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	8(%r12), %xmm0
	jbe	.LBB13_29
.LBB13_28:                              #   in Loop: Header=BB13_21 Depth=1
	xorl	%edi, %edi
.LBB13_29:                              #   in Loop: Header=BB13_21 Depth=1
	movss	4(%r13), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	20(%rbx), %xmm0
	ja	.LBB13_31
# BB#30:                                #   in Loop: Header=BB13_21 Depth=1
	movss	4(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%r12), %xmm0
	jbe	.LBB13_35
.LBB13_31:                              # %_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_.exit.thread.i
                                        #   in Loop: Header=BB13_21 Depth=1
	movl	32(%rbx), %edx
	cmpl	$-1, %edx
	sete	%al
	xorl	%edi, %edi
.LBB13_32:                              #   in Loop: Header=BB13_21 Depth=1
	testb	%al, %al
	jne	.LBB13_37
# BB#33:                                #   in Loop: Header=BB13_21 Depth=1
	testb	%dil, %dil
	jne	.LBB13_37
# BB#34:                                #   in Loop: Header=BB13_21 Depth=1
	movslq	%edx, %rax
	movq	%rax, %rcx
	shlq	$6, %rcx
	addq	%rcx, %rbx
	addl	%r14d, %eax
	movl	%eax, %r14d
	jmp	.LBB13_38
	.p2align	4, 0x90
.LBB13_35:                              # %_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_.exit.i
                                        #   in Loop: Header=BB13_21 Depth=1
	movl	32(%rbx), %edx
	cmpl	$-1, %edx
	sete	%al
	movl	%edi, %ecx
	andb	%al, %cl
	cmpb	$1, %cl
	jne	.LBB13_32
# BB#36:                                # %.thread.i
                                        #   in Loop: Header=BB13_21 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	36(%rbx), %esi
	movl	40(%rbx), %edx
	callq	*16(%rax)
	movl	60(%r15), %esi
.LBB13_37:                              #   in Loop: Header=BB13_21 Depth=1
	addq	$64, %rbx
	incl	%r14d
.LBB13_38:                              # %.backedge.i
                                        #   in Loop: Header=BB13_21 Depth=1
	incl	%ebp
	cmpl	%esi, %r14d
	jl	.LBB13_21
	jmp	.LBB13_39
.LBB13_17:
	movq	184(%r15), %rsi
	leaq	18(%rsp), %rcx
	leaq	12(%rsp), %r8
	movq	%r15, %rdi
	movq	24(%rsp), %rdx          # 8-byte Reload
	callq	_ZNK14btQuantizedBvh42walkRecursiveQuantizedTreeAgainstQueryAabbEPK18btQuantizedBvhNodeP21btNodeOverlapCallbackPtS5_
	jmp	.LBB13_41
.LBB13_16:
	leaq	18(%rsp), %rdx
	leaq	12(%rsp), %rcx
	movq	%r15, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	_ZNK14btQuantizedBvh39walkStacklessQuantizedTreeCacheFriendlyEP21btNodeOverlapCallbackPtS2_
	jmp	.LBB13_41
.LBB13_19:
	xorl	%ebp, %ebp
.LBB13_39:                              # %._crit_edge.i
	cmpl	%ebp, maxIterations(%rip)
	jge	.LBB13_41
# BB#40:
	movl	%ebp, maxIterations(%rip)
	jmp	.LBB13_41
.LBB13_5:
	xorl	%ebx, %ebx
.LBB13_14:                              # %._crit_edge.i28
	cmpl	%ebx, maxIterations(%rip)
	jge	.LBB13_41
# BB#15:
	movl	%ebx, maxIterations(%rip)
.LBB13_41:                              # %_ZNK14btQuantizedBvh17walkStacklessTreeEP21btNodeOverlapCallbackRK9btVector3S4_.exit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_ZNK14btQuantizedBvh26reportAabbOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_, .Lfunc_end13-_ZNK14btQuantizedBvh26reportAabbOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_
	.cfi_endproc

	.globl	_ZNK14btQuantizedBvh26walkStacklessQuantizedTreeEP21btNodeOverlapCallbackPtS2_ii
	.p2align	4, 0x90
	.type	_ZNK14btQuantizedBvh26walkStacklessQuantizedTreeEP21btNodeOverlapCallbackPtS2_ii,@function
_ZNK14btQuantizedBvh26walkStacklessQuantizedTreeEP21btNodeOverlapCallbackPtS2_ii: # @_ZNK14btQuantizedBvh26walkStacklessQuantizedTreeEP21btNodeOverlapCallbackPtS2_ii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi78:
	.cfi_def_cfa_offset 80
.Lcfi79:
	.cfi_offset %rbx, -56
.Lcfi80:
	.cfi_offset %r12, -48
.Lcfi81:
	.cfi_offset %r13, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %r11
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	cmpl	%r9d, %r8d
	jge	.LBB14_9
# BB#1:                                 # %.lr.ph
	movslq	%r8d, %r12
	shlq	$4, %r12
	addq	184(%rdi), %r12
	xorl	%ebp, %ebp
	movl	%r9d, 8(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB14_2:                               # =>This Inner Loop Header: Depth=1
	movzwl	(%r11), %eax
	movzwl	2(%r11), %r10d
	cmpw	6(%r12), %ax
	setbe	%al
	movzwl	(%r13), %edx
	movzwl	2(%r13), %esi
	cmpw	(%r12), %dx
	setae	%dl
	andb	%al, %dl
	movzwl	4(%r11), %eax
	cmpw	10(%r12), %ax
	setbe	%al
	movzwl	4(%r13), %edi
	cmpw	4(%r12), %di
	setae	%bl
	andb	%al, %bl
	andb	%dl, %bl
	cmpw	8(%r12), %r10w
	setbe	%al
	cmpw	2(%r12), %si
	setae	%r14b
	andb	%al, %r14b
	andb	%bl, %r14b
	movl	12(%r12), %r15d
	testl	%r15d, %r15d
	js	.LBB14_5
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	testb	%r14b, %r14b
	je	.LBB14_5
# BB#4:                                 #   in Loop: Header=BB14_2 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	%r15d, %esi
	shrl	$21, %esi
	movl	%r15d, %edx
	andl	$2097151, %edx          # imm = 0x1FFFFF
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movq	%r11, %rbx
	callq	*16(%rax)
	movq	%rbx, %r11
	movl	12(%rsp), %r8d          # 4-byte Reload
	movl	8(%rsp), %r9d           # 4-byte Reload
.LBB14_5:                               #   in Loop: Header=BB14_2 Depth=1
	testl	%r15d, %r15d
	setns	%al
	orb	%al, %r14b
	cmpb	$1, %r14b
	jne	.LBB14_7
# BB#6:                                 #   in Loop: Header=BB14_2 Depth=1
	addq	$16, %r12
	incl	%r8d
	jmp	.LBB14_8
	.p2align	4, 0x90
.LBB14_7:                               #   in Loop: Header=BB14_2 Depth=1
	movslq	12(%r12), %rax
	subl	%eax, %r8d
	shlq	$4, %rax
	subq	%rax, %r12
.LBB14_8:                               # %.backedge
                                        #   in Loop: Header=BB14_2 Depth=1
	incl	%ebp
	cmpl	%r9d, %r8d
	jl	.LBB14_2
.LBB14_9:                               # %._crit_edge
	cmpl	%ebp, maxIterations(%rip)
	jge	.LBB14_11
# BB#10:
	movl	%ebp, maxIterations(%rip)
.LBB14_11:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	_ZNK14btQuantizedBvh26walkStacklessQuantizedTreeEP21btNodeOverlapCallbackPtS2_ii, .Lfunc_end14-_ZNK14btQuantizedBvh26walkStacklessQuantizedTreeEP21btNodeOverlapCallbackPtS2_ii
	.cfi_endproc

	.globl	_ZNK14btQuantizedBvh39walkStacklessQuantizedTreeCacheFriendlyEP21btNodeOverlapCallbackPtS2_
	.p2align	4, 0x90
	.type	_ZNK14btQuantizedBvh39walkStacklessQuantizedTreeCacheFriendlyEP21btNodeOverlapCallbackPtS2_,@function
_ZNK14btQuantizedBvh39walkStacklessQuantizedTreeCacheFriendlyEP21btNodeOverlapCallbackPtS2_: # @_ZNK14btQuantizedBvh39walkStacklessQuantizedTreeCacheFriendlyEP21btNodeOverlapCallbackPtS2_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi88:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi89:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi91:
	.cfi_def_cfa_offset 96
.Lcfi92:
	.cfi_offset %rbx, -56
.Lcfi93:
	.cfi_offset %r12, -48
.Lcfi94:
	.cfi_offset %r13, -40
.Lcfi95:
	.cfi_offset %r14, -32
.Lcfi96:
	.cfi_offset %r15, -24
.Lcfi97:
	.cfi_offset %rbp, -16
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	cmpl	$0, 212(%rdi)
	jle	.LBB15_22
# BB#1:                                 # %.lr.ph
	xorl	%r10d, %r10d
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB15_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_11 Depth 2
	movq	224(%rdi), %r8
	movq	%r10, %r14
	shlq	$5, %r14
	movq	(%rsp), %rax            # 8-byte Reload
	movzwl	(%rax), %eax
	cmpw	6(%r8,%r14), %ax
	ja	.LBB15_21
# BB#3:                                 #   in Loop: Header=BB15_2 Depth=1
	movzwl	(%rcx), %edx
	cmpw	(%r8,%r14), %dx
	jb	.LBB15_21
# BB#4:                                 #   in Loop: Header=BB15_2 Depth=1
	movq	(%rsp), %rsi            # 8-byte Reload
	movzwl	4(%rsi), %r12d
	cmpw	10(%r8,%r14), %r12w
	ja	.LBB15_21
# BB#5:                                 #   in Loop: Header=BB15_2 Depth=1
	movzwl	4(%rcx), %ebp
	cmpw	4(%r8,%r14), %bp
	jb	.LBB15_21
# BB#6:                                 #   in Loop: Header=BB15_2 Depth=1
	movq	(%rsp), %rsi            # 8-byte Reload
	movzwl	2(%rsi), %r9d
	cmpw	8(%r8,%r14), %r9w
	ja	.LBB15_21
# BB#7:                                 #   in Loop: Header=BB15_2 Depth=1
	movzwl	2(%rcx), %esi
	cmpw	2(%r8,%r14), %si
	jb	.LBB15_21
# BB#8:                                 #   in Loop: Header=BB15_2 Depth=1
	movl	16(%r8,%r14), %r11d
	testl	%r11d, %r11d
	jle	.LBB15_9
# BB#10:                                # %.lr.ph.i
                                        #   in Loop: Header=BB15_2 Depth=1
	movq	%r10, 16(%rsp)          # 8-byte Spill
	movl	12(%r8,%r14), %r10d
	movslq	%r10d, %r15
	addl	%r15d, %r11d
	shlq	$4, %r15
	addq	184(%rdi), %r15
	movl	$1, %r13d
	jmp	.LBB15_11
	.p2align	4, 0x90
.LBB15_23:                              # %.backedge.i._crit_edge
                                        #   in Loop: Header=BB15_11 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	movzwl	(%rdi), %eax
	movzwl	2(%rdi), %r9d
	movzwl	(%rcx), %edx
	movzwl	2(%rcx), %esi
	movzwl	4(%rdi), %r12d
	movzwl	4(%rcx), %ebp
	incl	%r13d
.LBB15_11:                              #   Parent Loop BB15_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpw	6(%r15), %ax
	setbe	%al
	cmpw	(%r15), %dx
	setae	%bl
	andb	%al, %bl
	cmpw	10(%r15), %r12w
	setbe	%al
	cmpw	4(%r15), %bp
	setae	%dl
	andb	%al, %dl
	andb	%bl, %dl
	cmpw	8(%r15), %r9w
	setbe	%al
	cmpw	2(%r15), %si
	setae	%bl
	andb	%al, %bl
	andb	%dl, %bl
	movl	12(%r15), %ebp
	testl	%ebp, %ebp
	js	.LBB15_14
# BB#12:                                #   in Loop: Header=BB15_11 Depth=2
	testb	%bl, %bl
	je	.LBB15_14
# BB#13:                                #   in Loop: Header=BB15_11 Depth=2
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	%ebp, %esi
	shrl	$21, %esi
	movl	%ebp, %edx
	andl	$2097151, %edx          # imm = 0x1FFFFF
	movl	%r11d, %r14d
	movl	%r10d, %r12d
	callq	*16(%rax)
	movl	%r12d, %r10d
	movl	%r14d, %r11d
	movq	24(%rsp), %rcx          # 8-byte Reload
.LBB15_14:                              #   in Loop: Header=BB15_11 Depth=2
	testl	%ebp, %ebp
	setns	%al
	orb	%al, %bl
	cmpb	$1, %bl
	jne	.LBB15_16
# BB#15:                                #   in Loop: Header=BB15_11 Depth=2
	addq	$16, %r15
	incl	%r10d
	cmpl	%r11d, %r10d
	jl	.LBB15_23
	jmp	.LBB15_18
	.p2align	4, 0x90
.LBB15_16:                              #   in Loop: Header=BB15_11 Depth=2
	movslq	12(%r15), %rax
	subl	%eax, %r10d
	shlq	$4, %rax
	subq	%rax, %r15
	cmpl	%r11d, %r10d
	jl	.LBB15_23
.LBB15_18:                              #   in Loop: Header=BB15_2 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %r10          # 8-byte Reload
	cmpl	%r13d, maxIterations(%rip)
	jl	.LBB15_20
	jmp	.LBB15_21
.LBB15_9:                               #   in Loop: Header=BB15_2 Depth=1
	xorl	%r13d, %r13d
	cmpl	%r13d, maxIterations(%rip)
	jge	.LBB15_21
.LBB15_20:                              #   in Loop: Header=BB15_2 Depth=1
	movl	%r13d, maxIterations(%rip)
.LBB15_21:                              # %_ZNK14btQuantizedBvh26walkStacklessQuantizedTreeEP21btNodeOverlapCallbackPtS2_ii.exit
                                        #   in Loop: Header=BB15_2 Depth=1
	incq	%r10
	movslq	212(%rdi), %rax
	cmpq	%rax, %r10
	jl	.LBB15_2
.LBB15_22:                              # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	_ZNK14btQuantizedBvh39walkStacklessQuantizedTreeCacheFriendlyEP21btNodeOverlapCallbackPtS2_, .Lfunc_end15-_ZNK14btQuantizedBvh39walkStacklessQuantizedTreeCacheFriendlyEP21btNodeOverlapCallbackPtS2_
	.cfi_endproc

	.globl	_ZNK14btQuantizedBvh42walkRecursiveQuantizedTreeAgainstQueryAabbEPK18btQuantizedBvhNodeP21btNodeOverlapCallbackPtS5_
	.p2align	4, 0x90
	.type	_ZNK14btQuantizedBvh42walkRecursiveQuantizedTreeAgainstQueryAabbEPK18btQuantizedBvhNodeP21btNodeOverlapCallbackPtS5_,@function
_ZNK14btQuantizedBvh42walkRecursiveQuantizedTreeAgainstQueryAabbEPK18btQuantizedBvhNodeP21btNodeOverlapCallbackPtS5_: # @_ZNK14btQuantizedBvh42walkRecursiveQuantizedTreeAgainstQueryAabbEPK18btQuantizedBvhNodeP21btNodeOverlapCallbackPtS5_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi101:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi102:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi104:
	.cfi_def_cfa_offset 64
.Lcfi105:
	.cfi_offset %rbx, -56
.Lcfi106:
	.cfi_offset %r12, -48
.Lcfi107:
	.cfi_offset %r13, -40
.Lcfi108:
	.cfi_offset %r14, -32
.Lcfi109:
	.cfi_offset %r15, -24
.Lcfi110:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %r12
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movl	$1, %ebp
	jmp	.LBB16_1
	.p2align	4, 0x90
.LBB16_8:                               #   in Loop: Header=BB16_1 Depth=1
	leaq	16(%rbx), %rsi
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r12, %rcx
	movq	%r15, %r8
	callq	_ZNK14btQuantizedBvh42walkRecursiveQuantizedTreeAgainstQueryAabbEPK18btQuantizedBvhNodeP21btNodeOverlapCallbackPtS5_
	movslq	28(%rbx), %rax
	movq	%rax, %rcx
	negq	%rcx
	testq	%rax, %rax
	cmovnsq	%rbp, %rcx
	shlq	$4, %rcx
	leaq	16(%rbx,%rcx), %rbx
.LBB16_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%r12), %eax
	cmpw	6(%rbx), %ax
	ja	.LBB16_9
# BB#2:                                 # %tailrecurse
                                        #   in Loop: Header=BB16_1 Depth=1
	movzwl	(%rbx), %eax
	cmpw	%ax, (%r15)
	jb	.LBB16_9
# BB#3:                                 # %tailrecurse
                                        #   in Loop: Header=BB16_1 Depth=1
	movzwl	10(%rbx), %eax
	cmpw	%ax, 4(%r12)
	ja	.LBB16_9
# BB#4:                                 # %tailrecurse
                                        #   in Loop: Header=BB16_1 Depth=1
	movzwl	4(%rbx), %eax
	cmpw	%ax, 4(%r15)
	jb	.LBB16_9
# BB#5:                                 # %tailrecurse
                                        #   in Loop: Header=BB16_1 Depth=1
	movzwl	8(%rbx), %eax
	cmpw	%ax, 2(%r12)
	ja	.LBB16_9
# BB#6:                                 # %tailrecurse
                                        #   in Loop: Header=BB16_1 Depth=1
	movzwl	2(%rbx), %eax
	cmpw	%ax, 2(%r15)
	jb	.LBB16_9
# BB#7:                                 #   in Loop: Header=BB16_1 Depth=1
	movl	12(%rbx), %edx
	testl	%edx, %edx
	js	.LBB16_8
# BB#10:
	movq	(%r14), %rax
	movq	16(%rax), %rax
	movl	%edx, %esi
	shrl	$21, %esi
	andl	$2097151, %edx          # imm = 0x1FFFFF
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.LBB16_9:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	_ZNK14btQuantizedBvh42walkRecursiveQuantizedTreeAgainstQueryAabbEPK18btQuantizedBvhNodeP21btNodeOverlapCallbackPtS5_, .Lfunc_end16-_ZNK14btQuantizedBvh42walkRecursiveQuantizedTreeAgainstQueryAabbEPK18btQuantizedBvhNodeP21btNodeOverlapCallbackPtS5_
	.cfi_endproc

	.globl	_ZNK14btQuantizedBvh17walkStacklessTreeEP21btNodeOverlapCallbackRK9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK14btQuantizedBvh17walkStacklessTreeEP21btNodeOverlapCallbackRK9btVector3S4_,@function
_ZNK14btQuantizedBvh17walkStacklessTreeEP21btNodeOverlapCallbackRK9btVector3S4_: # @_ZNK14btQuantizedBvh17walkStacklessTreeEP21btNodeOverlapCallbackRK9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi111:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi112:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi113:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi114:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi115:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi117:
	.cfi_def_cfa_offset 64
.Lcfi118:
	.cfi_offset %rbx, -56
.Lcfi119:
	.cfi_offset %r12, -48
.Lcfi120:
	.cfi_offset %r13, -40
.Lcfi121:
	.cfi_offset %r14, -32
.Lcfi122:
	.cfi_offset %r15, -24
.Lcfi123:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r13
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, %r12
	cmpl	$0, 60(%r12)
	jle	.LBB17_1
# BB#2:                                 # %.lr.ph
	movq	120(%r12), %rbx
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB17_3:                               # =>This Inner Loop Header: Depth=1
	movss	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	16(%rbx), %xmm0
	jbe	.LBB17_5
# BB#4:                                 #   in Loop: Header=BB17_3 Depth=1
	xorl	%eax, %eax
	jmp	.LBB17_8
	.p2align	4, 0x90
.LBB17_5:                               #   in Loop: Header=BB17_3 Depth=1
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%r14), %xmm0
	jbe	.LBB17_7
# BB#6:                                 #   in Loop: Header=BB17_3 Depth=1
	xorl	%eax, %eax
	jmp	.LBB17_8
	.p2align	4, 0x90
.LBB17_7:                               #   in Loop: Header=BB17_3 Depth=1
	movb	$1, %al
.LBB17_8:                               #   in Loop: Header=BB17_3 Depth=1
	movss	8(%r13), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	24(%rbx), %xmm0
	ja	.LBB17_10
# BB#9:                                 #   in Loop: Header=BB17_3 Depth=1
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	8(%r14), %xmm0
	jbe	.LBB17_11
.LBB17_10:                              #   in Loop: Header=BB17_3 Depth=1
	xorl	%eax, %eax
.LBB17_11:                              #   in Loop: Header=BB17_3 Depth=1
	movss	4(%r13), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	20(%rbx), %xmm0
	ja	.LBB17_13
# BB#12:                                #   in Loop: Header=BB17_3 Depth=1
	movss	4(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%r14), %xmm0
	jbe	.LBB17_17
.LBB17_13:                              # %_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_.exit.thread
                                        #   in Loop: Header=BB17_3 Depth=1
	cmpl	$-1, 32(%rbx)
	sete	%cl
	xorl	%eax, %eax
.LBB17_14:                              #   in Loop: Header=BB17_3 Depth=1
	testb	%cl, %cl
	jne	.LBB17_19
# BB#15:                                #   in Loop: Header=BB17_3 Depth=1
	testb	%al, %al
	jne	.LBB17_19
# BB#16:                                #   in Loop: Header=BB17_3 Depth=1
	movslq	32(%rbx), %rax
	addl	%eax, %r15d
	shlq	$6, %rax
	addq	%rax, %rbx
	jmp	.LBB17_20
	.p2align	4, 0x90
.LBB17_17:                              # %_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_.exit
                                        #   in Loop: Header=BB17_3 Depth=1
	cmpl	$-1, 32(%rbx)
	sete	%cl
	movl	%eax, %edx
	andb	%cl, %dl
	cmpb	$1, %dl
	jne	.LBB17_14
# BB#18:                                # %.thread
                                        #   in Loop: Header=BB17_3 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	(%rdi), %rax
	movl	36(%rbx), %esi
	movl	40(%rbx), %edx
	callq	*16(%rax)
.LBB17_19:                              #   in Loop: Header=BB17_3 Depth=1
	addq	$64, %rbx
	incl	%r15d
.LBB17_20:                              # %.backedge
                                        #   in Loop: Header=BB17_3 Depth=1
	incl	%ebp
	cmpl	60(%r12), %r15d
	jl	.LBB17_3
	jmp	.LBB17_21
.LBB17_1:
	xorl	%ebp, %ebp
.LBB17_21:                              # %._crit_edge
	cmpl	%ebp, maxIterations(%rip)
	jge	.LBB17_23
# BB#22:
	movl	%ebp, maxIterations(%rip)
.LBB17_23:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	_ZNK14btQuantizedBvh17walkStacklessTreeEP21btNodeOverlapCallbackRK9btVector3S4_, .Lfunc_end17-_ZNK14btQuantizedBvh17walkStacklessTreeEP21btNodeOverlapCallbackRK9btVector3S4_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI18_0:
	.long	1065353216              # float 1
.LCPI18_2:
	.long	1566444395              # float 9.99999984E+17
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_1:
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.zero	4
	.zero	4
.LCPI18_3:
	.long	1566444395              # float 9.99999984E+17
	.long	1566444395              # float 9.99999984E+17
	.zero	4
	.zero	4
	.text
	.globl	_ZNK14btQuantizedBvh27walkStacklessTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii
	.p2align	4, 0x90
	.type	_ZNK14btQuantizedBvh27walkStacklessTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii,@function
_ZNK14btQuantizedBvh27walkStacklessTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii: # @_ZNK14btQuantizedBvh27walkStacklessTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii
	.cfi_startproc
# BB#0:                                 # %_Z8btSetMinIfEvRT_RKS0_.exit.i
	pushq	%rbp
.Lcfi124:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi125:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi126:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi127:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi128:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi129:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi130:
	.cfi_def_cfa_offset 288
.Lcfi131:
	.cfi_offset %rbx, -56
.Lcfi132:
	.cfi_offset %r12, -48
.Lcfi133:
	.cfi_offset %r13, -40
.Lcfi134:
	.cfi_offset %r14, -32
.Lcfi135:
	.cfi_offset %r15, -24
.Lcfi136:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r13
	movq	%rcx, %rbp
	movq	%rdx, %r8
	movq	%rsi, 200(%rsp)         # 8-byte Spill
	movq	%rdi, %r9
	movq	120(%r9), %r12
	movsd	(%r8), %xmm6            # xmm6 = mem[0],zero
	movss	8(%r8), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movsd	(%rbp), %xmm11          # xmm11 = mem[0],zero
	movss	8(%rbp), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movss	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movss	4(%r13), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 56(%rsp)         # 4-byte Spill
	movss	8(%r13), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 72(%rsp)         # 4-byte Spill
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 80(%rsp)         # 4-byte Spill
	movss	4(%r14), %xmm14         # xmm14 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm15         # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm5
	subps	%xmm6, %xmm5
	movaps	%xmm5, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm10, %xmm9
	movss	%xmm1, 64(%rsp)         # 4-byte Spill
	subss	%xmm1, %xmm9
	movaps	%xmm5, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm9, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB18_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movq	%r8, %r15
	movq	%r9, %rbx
	movaps	%xmm11, 160(%rsp)       # 16-byte Spill
	movss	%xmm10, 12(%rsp)        # 4-byte Spill
	movss	%xmm9, 8(%rsp)          # 4-byte Spill
	movss	%xmm15, 144(%rsp)       # 4-byte Spill
	movss	%xmm14, 128(%rsp)       # 4-byte Spill
	movaps	%xmm6, 112(%rsp)        # 16-byte Spill
	movaps	%xmm5, 96(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	96(%rsp), %xmm5         # 16-byte Reload
	movaps	112(%rsp), %xmm6        # 16-byte Reload
	movss	128(%rsp), %xmm14       # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movss	144(%rsp), %xmm15       # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm9          # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movaps	160(%rsp), %xmm11       # 16-byte Reload
	movq	%rbx, %r9
	movq	%r15, %r8
.LBB18_2:                               # %_Z8btSetMinIfEvRT_RKS0_.exit.i.split
	movss	.LCPI18_0(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm3
	divss	%xmm0, %xmm3
	mulss	%xmm3, %xmm9
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	(%r8), %xmm0
	subss	4(%r8), %xmm2
	movss	8(%rbp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	8(%r8), %xmm1
	xorps	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm9
	jne	.LBB18_4
	jp	.LBB18_4
# BB#3:
	movss	.LCPI18_2(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	cmpl	$0, 60(%r9)
	jg	.LBB18_5
	jmp	.LBB18_29
.LBB18_4:                               # %select.false.sink
	divss	%xmm9, %xmm8
	cmpl	$0, 60(%r9)
	jle	.LBB18_29
.LBB18_5:                               # %.lr.ph
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm3, %xmm5
	movaps	%xmm5, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	mulss	%xmm2, %xmm3
	mulss	%xmm5, %xmm0
	addss	%xmm0, %xmm3
	pshufd	$229, %xmm11, %xmm12    # xmm12 = xmm11[1,1,2,3]
	mulss	%xmm1, %xmm9
	movaps	.LCPI18_1(%rip), %xmm0  # xmm0 = <1,1,u,u>
	divps	%xmm5, %xmm0
	addss	%xmm3, %xmm9
	xorps	%xmm1, %xmm1
	cmpeqps	%xmm5, %xmm1
	shufps	$212, %xmm1, %xmm1      # xmm1 = xmm1[0,1,1,3]
	psllq	$32, %xmm1
	pshufd	$237, %xmm1, %xmm13     # xmm13 = xmm1[1,3,2,3]
	pslld	$31, %xmm13
	psrad	$31, %xmm13
	movdqa	%xmm13, %xmm1
	pandn	%xmm0, %xmm1
	pshufd	$229, %xmm6, %xmm0      # xmm0 = xmm6[1,1,2,3]
	pand	.LCPI18_3(%rip), %xmm13
	por	%xmm1, %xmm13
	movaps	%xmm11, %xmm5
	minss	%xmm6, %xmm5
	maxss	%xmm6, %xmm11
	movdqa	%xmm12, %xmm7
	minss	%xmm0, %xmm7
	maxss	%xmm0, %xmm12
	movaps	%xmm10, %xmm6
	movss	64(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	minss	%xmm0, %xmm6
	maxss	%xmm0, %xmm10
	addss	4(%rsp), %xmm5          # 4-byte Folded Reload
	addss	56(%rsp), %xmm7         # 4-byte Folded Reload
	addss	72(%rsp), %xmm6         # 4-byte Folded Reload
	addss	80(%rsp), %xmm11        # 4-byte Folded Reload
	addss	%xmm14, %xmm12
	pshufd	$229, %xmm13, %xmm14    # xmm14 = xmm13[1,1,2,3]
	addss	%xmm15, %xmm10
	xorps	%xmm15, %xmm15
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	ucomiss	%xmm13, %xmm15
	seta	%al
	setbe	%cl
	shlq	$4, %rax
	shlq	$4, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	ucomiss	%xmm14, %xmm15
	seta	%dl
	setbe	%sil
	shlq	$4, %rdx
	shlq	$4, %rsi
	xorl	%edi, %edi
	xorl	%ebp, %ebp
	ucomiss	%xmm8, %xmm15
	seta	%dil
	leaq	32(%rsp), %r10
	leaq	16(%rsp,%rax), %r11
	leaq	16(%rsp,%rcx), %rax
	leaq	20(%rsp,%rdx), %rcx
	leaq	20(%rsp,%rsi), %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%rax, %rsi
	setbe	%bpl
	shlq	$4, %rdi
	leaq	24(%rsp,%rdi), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rcx, %rdi
	shlq	$4, %rbp
	leaq	24(%rsp,%rbp), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB18_6:                               # =>This Inner Loop Header: Depth=1
	movups	(%r12), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	16(%r12), %xmm0
	movups	%xmm0, (%r10)
	movss	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	16(%rsp), %xmm0
	movss	%xmm0, 16(%rsp)
	movss	4(%r13), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	20(%rsp), %xmm0
	movss	%xmm0, 20(%rsp)
	movss	8(%r13), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	24(%rsp), %xmm0
	movss	%xmm0, 24(%rsp)
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	32(%rsp), %xmm0
	movss	%xmm0, 32(%rsp)
	movss	4(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	36(%rsp), %xmm0
	movss	%xmm0, 36(%rsp)
	movss	8(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	40(%rsp), %xmm0
	movss	%xmm0, 40(%rsp)
	ucomiss	16(%r12), %xmm5
	ja	.LBB18_7
# BB#8:                                 #   in Loop: Header=BB18_6 Depth=1
	movss	(%r12), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm11, %xmm0
	jbe	.LBB18_10
.LBB18_7:                               #   in Loop: Header=BB18_6 Depth=1
	xorl	%eax, %eax
	ucomiss	24(%r12), %xmm6
	jbe	.LBB18_11
	jmp	.LBB18_12
	.p2align	4, 0x90
.LBB18_10:                              #   in Loop: Header=BB18_6 Depth=1
	movb	$1, %al
	ucomiss	24(%r12), %xmm6
	ja	.LBB18_12
.LBB18_11:                              #   in Loop: Header=BB18_6 Depth=1
	movss	8(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm10, %xmm0
	jbe	.LBB18_13
.LBB18_12:                              #   in Loop: Header=BB18_6 Depth=1
	xorl	%eax, %eax
.LBB18_13:                              #   in Loop: Header=BB18_6 Depth=1
	ucomiss	20(%r12), %xmm7
	ja	.LBB18_23
# BB#14:                                #   in Loop: Header=BB18_6 Depth=1
	movss	4(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm12, %xmm0
	ja	.LBB18_23
# BB#15:                                #   in Loop: Header=BB18_6 Depth=1
	xorb	$1, %al
	jne	.LBB18_23
# BB#16:                                #   in Loop: Header=BB18_6 Depth=1
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movsd	(%r8), %xmm4            # xmm4 = mem[0],zero
	subss	%xmm4, %xmm3
	mulss	%xmm13, %xmm3
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	(%r11), %xmm2           # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	subps	%xmm4, %xmm2
	mulps	%xmm13, %xmm2
	movaps	%xmm2, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	ucomiss	%xmm3, %xmm0
	ja	.LBB18_23
# BB#17:                                #   in Loop: Header=BB18_6 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	pshufd	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	subss	%xmm4, %xmm1
	mulss	%xmm14, %xmm1
	ucomiss	%xmm1, %xmm2
	ja	.LBB18_23
# BB#18:                                #   in Loop: Header=BB18_6 Depth=1
	minss	%xmm3, %xmm1
	movq	72(%rsp), %rax          # 8-byte Reload
	movss	(%rax), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	8(%r8), %xmm4           # xmm4 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm3
	mulss	%xmm8, %xmm3
	ucomiss	%xmm1, %xmm3
	ja	.LBB18_23
# BB#19:                                #   in Loop: Header=BB18_6 Depth=1
	maxss	%xmm2, %xmm0
	movq	64(%rsp), %rax          # 8-byte Reload
	movss	(%rax), %xmm2           # xmm2 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm2
	mulss	%xmm8, %xmm2
	ucomiss	%xmm2, %xmm0
	ja	.LBB18_23
# BB#20:                                # %_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff.exit
                                        #   in Loop: Header=BB18_6 Depth=1
	maxss	%xmm0, %xmm3
	minss	%xmm1, %xmm2
	ucomiss	%xmm3, %xmm9
	seta	%al
	ucomiss	%xmm15, %xmm2
	seta	%cl
	andb	%al, %cl
	leaq	32(%r12), %rax
	movl	32(%r12), %edx
	cmpl	$-1, %edx
	sete	%bl
	cmpb	$1, %cl
	jne	.LBB18_24
# BB#21:                                # %_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff.exit
                                        #   in Loop: Header=BB18_6 Depth=1
	cmpl	$-1, %edx
	jne	.LBB18_24
# BB#22:                                # %.thread
                                        #   in Loop: Header=BB18_6 Depth=1
	movq	%rdi, 176(%rsp)         # 8-byte Spill
	movq	200(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
	movq	%rsi, 184(%rsp)         # 8-byte Spill
	movl	36(%r12), %esi
	movl	40(%r12), %edx
	movq	%r8, 56(%rsp)           # 8-byte Spill
	movq	%r9, %rbx
	movaps	%xmm11, 160(%rsp)       # 16-byte Spill
	movss	%xmm10, 12(%rsp)        # 4-byte Spill
	movss	%xmm9, 8(%rsp)          # 4-byte Spill
	movss	%xmm8, 4(%rsp)          # 4-byte Spill
	movaps	%xmm12, 144(%rsp)       # 16-byte Spill
	movaps	%xmm13, 128(%rsp)       # 16-byte Spill
	movss	%xmm5, 112(%rsp)        # 4-byte Spill
	movss	%xmm7, 96(%rsp)         # 4-byte Spill
	movss	%xmm6, 92(%rsp)         # 4-byte Spill
	movaps	%xmm14, 208(%rsp)       # 16-byte Spill
	movq	%r11, 192(%rsp)         # 8-byte Spill
	callq	*16(%rax)
	movq	176(%rsp), %rdi         # 8-byte Reload
	movq	184(%rsp), %rsi         # 8-byte Reload
	movq	192(%rsp), %r11         # 8-byte Reload
	leaq	32(%rsp), %r10
	xorps	%xmm15, %xmm15
	movaps	208(%rsp), %xmm14       # 16-byte Reload
	movss	92(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movss	112(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movaps	128(%rsp), %xmm13       # 16-byte Reload
	movaps	144(%rsp), %xmm12       # 16-byte Reload
	movss	4(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm9          # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movaps	160(%rsp), %xmm11       # 16-byte Reload
	movq	%rbx, %r9
	movq	56(%rsp), %r8           # 8-byte Reload
	movl	$1, %edx
	jmp	.LBB18_27
	.p2align	4, 0x90
.LBB18_23:                              # %_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff.exit.thread
                                        #   in Loop: Header=BB18_6 Depth=1
	leaq	32(%r12), %rax
	cmpl	$-1, 32(%r12)
	sete	%bl
	xorl	%ecx, %ecx
.LBB18_24:                              #   in Loop: Header=BB18_6 Depth=1
	movl	$1, %edx
	testb	%bl, %bl
	jne	.LBB18_27
# BB#25:                                #   in Loop: Header=BB18_6 Depth=1
	testb	%cl, %cl
	movl	$1, %ecx
	jne	.LBB18_28
# BB#26:                                #   in Loop: Header=BB18_6 Depth=1
	movl	(%rax), %edx
	movslq	%edx, %rcx
	jmp	.LBB18_28
	.p2align	4, 0x90
.LBB18_27:                              #   in Loop: Header=BB18_6 Depth=1
	movl	$1, %ecx
.LBB18_28:                              #   in Loop: Header=BB18_6 Depth=1
	incl	%r15d
	shlq	$6, %rcx
	addq	%rcx, %r12
	addl	%edx, %ebp
	cmpl	60(%r9), %ebp
	jl	.LBB18_6
	jmp	.LBB18_30
.LBB18_29:
	xorl	%r15d, %r15d
.LBB18_30:                              # %._crit_edge
	cmpl	%r15d, maxIterations(%rip)
	jge	.LBB18_32
# BB#31:
	movl	%r15d, maxIterations(%rip)
.LBB18_32:
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	_ZNK14btQuantizedBvh27walkStacklessTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii, .Lfunc_end18-_ZNK14btQuantizedBvh27walkStacklessTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI19_0:
	.long	1065353216              # float 1
.LCPI19_3:
	.long	1566444395              # float 9.99999984E+17
.LCPI19_4:
	.long	0                       # float 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI19_1:
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.zero	4
	.zero	4
.LCPI19_2:
	.long	1566444395              # float 9.99999984E+17
	.long	1566444395              # float 9.99999984E+17
	.zero	4
	.zero	4
	.text
	.globl	_ZNK14btQuantizedBvh36walkStacklessQuantizedTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii
	.p2align	4, 0x90
	.type	_ZNK14btQuantizedBvh36walkStacklessQuantizedTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii,@function
_ZNK14btQuantizedBvh36walkStacklessQuantizedTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii: # @_ZNK14btQuantizedBvh36walkStacklessQuantizedTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii
	.cfi_startproc
# BB#0:                                 # %_Z8btSetMinIfEvRT_RKS0_.exit.i
	pushq	%rbp
.Lcfi137:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi138:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi139:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi140:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi141:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi142:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi143:
	.cfi_def_cfa_offset 240
.Lcfi144:
	.cfi_offset %rbx, -56
.Lcfi145:
	.cfi_offset %r12, -48
.Lcfi146:
	.cfi_offset %r13, -40
.Lcfi147:
	.cfi_offset %r14, -32
.Lcfi148:
	.cfi_offset %r15, -24
.Lcfi149:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movq	%rdx, %r10
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movq	184(%rbx), %r12
	movsd	(%rbp), %xmm5           # xmm5 = mem[0],zero
	movsd	(%r10), %xmm0           # xmm0 = mem[0],zero
	subps	%xmm0, %xmm5
	movaps	%xmm5, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	8(%rbp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	8(%r10), %xmm2
	movaps	%xmm5, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm2, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB19_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movq	%r9, %r14
	movq	%r8, %r15
	movq	%r10, %r13
	movss	%xmm2, 4(%rsp)          # 4-byte Spill
	movaps	%xmm5, 64(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	64(%rsp), %xmm5         # 16-byte Reload
	movss	4(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movq	%r13, %r10
	movq	%r15, %r8
	movq	%r14, %r9
.LBB19_2:                               # %_Z8btSetMinIfEvRT_RKS0_.exit.i.split
	movl	248(%rsp), %r11d
	movl	240(%rsp), %r15d
	movss	.LCPI19_0(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm1
	divss	%xmm0, %xmm1
	mulss	%xmm1, %xmm2
	movss	(%rbp), %xmm12          # xmm12 = mem[0],zero,zero,zero
	movss	4(%rbp), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movss	(%r10), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%r10), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movss	8(%rbp), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movss	8(%r10), %xmm14         # xmm14 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm2
	jne	.LBB19_4
	jp	.LBB19_4
# BB#3:
	movss	.LCPI19_3(%rip), %xmm13 # xmm13 = mem[0],zero,zero,zero
	jmp	.LBB19_5
.LBB19_4:                               # %select.false.sink
	movaps	%xmm3, %xmm13
	divss	%xmm2, %xmm13
.LBB19_5:                               # %select.end
	xorl	%r13d, %r13d
	cmpl	%r11d, %r15d
	jge	.LBB19_26
# BB#6:                                 # %.lr.ph
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm1, %xmm5
	xorps	%xmm0, %xmm0
	cmpeqps	%xmm5, %xmm0
	shufps	$212, %xmm0, %xmm0      # xmm0 = xmm0[0,1,1,3]
	psllq	$32, %xmm0
	pshufd	$237, %xmm0, %xmm8      # xmm8 = xmm0[1,3,2,3]
	pslld	$31, %xmm8
	movaps	.LCPI19_1(%rip), %xmm0  # xmm0 = <1,1,u,u>
	movaps	%xmm12, %xmm1
	subss	%xmm6, %xmm1
	movaps	%xmm9, %xmm7
	subss	%xmm10, %xmm7
	movaps	%xmm5, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	psrad	$31, %xmm8
	divps	%xmm5, %xmm0
	movaps	%xmm11, %xmm3
	subss	%xmm14, %xmm3
	mulss	%xmm5, %xmm1
	mulss	%xmm7, %xmm4
	movdqa	%xmm8, %xmm7
	pandn	%xmm0, %xmm7
	pand	.LCPI19_2(%rip), %xmm8
	addss	%xmm1, %xmm4
	mulss	%xmm3, %xmm2
	por	%xmm7, %xmm8
	addss	%xmm4, %xmm2
	movss	%xmm2, 4(%rsp)          # 4-byte Spill
	movaps	%xmm12, %xmm0
	minss	%xmm6, %xmm0
	movaps	%xmm9, %xmm3
	minss	%xmm10, %xmm3
	movaps	%xmm11, %xmm4
	minss	%xmm14, %xmm4
	maxss	%xmm6, %xmm12
	maxss	%xmm10, %xmm9
	maxss	%xmm14, %xmm11
	addss	(%r8), %xmm0
	addss	4(%r8), %xmm3
	addss	8(%r8), %xmm4
	addss	(%r9), %xmm12
	addss	4(%r9), %xmm9
	addss	8(%r9), %xmm11
	movss	8(%rbx), %xmm14         # xmm14 = mem[0],zero,zero,zero
	movss	%xmm13, 8(%rsp)         # 4-byte Spill
	movss	12(%rbx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm5
	maxss	%xmm0, %xmm5
	movaps	%xmm13, %xmm7
	maxss	%xmm3, %xmm7
	movss	16(%rbx), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm3
	maxss	%xmm4, %xmm3
	movss	24(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm6
	minss	%xmm5, %xmm6
	movss	28(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm5
	minss	%xmm7, %xmm5
	movss	32(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm7
	minss	%xmm3, %xmm7
	subss	%xmm14, %xmm6
	subss	%xmm13, %xmm5
	subss	%xmm15, %xmm7
	movss	40(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm6
	cvttss2si	%xmm6, %r14d
	movss	44(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	cvttss2si	%xmm5, %r13d
	movss	48(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm7
	cvttss2si	%xmm7, %eax
	movaps	%xmm14, %xmm6
	maxss	%xmm12, %xmm6
	movaps	%xmm13, %xmm7
	maxss	%xmm9, %xmm7
	movaps	%xmm15, %xmm0
	maxss	%xmm11, %xmm0
	minss	%xmm6, %xmm2
	minss	%xmm7, %xmm1
	minss	%xmm0, %xmm4
	subss	%xmm14, %xmm2
	movdqa	%xmm8, %xmm14
	subss	%xmm13, %xmm1
	movss	8(%rsp), %xmm13         # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm12         # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	subss	%xmm15, %xmm4
	mulss	%xmm10, %xmm2
	mulss	%xmm3, %xmm1
	mulss	%xmm5, %xmm4
	movss	.LCPI19_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm2
	cvttss2si	%xmm2, %ecx
	addss	%xmm0, %xmm1
	cvttss2si	%xmm1, %edx
	addss	%xmm0, %xmm4
	movdqa	%xmm14, %xmm15
	shufps	$229, %xmm15, %xmm15    # xmm15 = xmm15[1,1,2,3]
	andl	$65534, %r14d           # imm = 0xFFFE
	andl	$65534, %r13d           # imm = 0xFFFE
	andl	$65534, %eax            # imm = 0xFFFE
	movl	%eax, 28(%rsp)          # 4-byte Spill
	orl	$1, %ecx
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	orl	$1, %edx
	movl	%edx, 20(%rsp)          # 4-byte Spill
	cvttss2si	%xmm4, %eax
	orl	$1, %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movslq	%r15d, %rax
	shlq	$4, %rax
	addq	%rax, %r12
	xorps	%xmm0, %xmm0
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	ucomiss	%xmm14, %xmm0
	seta	%al
	setbe	%cl
	shlq	$4, %rax
	shlq	$4, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	ucomiss	%xmm15, %xmm0
	seta	%dl
	setbe	%sil
	shlq	$4, %rdx
	shlq	$4, %rsi
	xorl	%edi, %edi
	xorl	%ebp, %ebp
	ucomiss	%xmm13, %xmm0
	seta	%dil
	leaq	32(%rsp,%rax), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	leaq	32(%rsp,%rcx), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	36(%rsp,%rdx), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	leaq	36(%rsp,%rsi), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	setbe	%bpl
	shlq	$4, %rdi
	leaq	40(%rsp,%rdi), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	shlq	$4, %rbp
	leaq	40(%rsp,%rbp), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	%r13d, %ebp
	xorl	%r13d, %r13d
	movq	%r10, 80(%rsp)          # 8-byte Spill
	movdqa	%xmm14, 160(%rsp)       # 16-byte Spill
	movl	%r14d, 16(%rsp)         # 4-byte Spill
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movaps	%xmm15, 144(%rsp)       # 16-byte Spill
	.p2align	4, 0x90
.LBB19_7:                               # =>This Inner Loop Header: Depth=1
	movl	12(%r12), %eax
	xorl	%ecx, %ecx
	cmpw	6(%r12), %r14w
	ja	.LBB19_21
# BB#8:                                 #   in Loop: Header=BB19_7 Depth=1
	movzwl	(%r12), %esi
	cmpw	%si, 64(%rsp)           # 2-byte Folded Reload
	jb	.LBB19_21
# BB#9:                                 #   in Loop: Header=BB19_7 Depth=1
	movl	28(%rsp), %edx          # 4-byte Reload
	cmpw	10(%r12), %dx
	ja	.LBB19_21
# BB#10:                                #   in Loop: Header=BB19_7 Depth=1
	movzwl	4(%r12), %edx
	cmpw	%dx, 24(%rsp)           # 2-byte Folded Reload
	jb	.LBB19_21
# BB#11:                                #   in Loop: Header=BB19_7 Depth=1
	cmpw	8(%r12), %bp
	ja	.LBB19_21
# BB#12:                                #   in Loop: Header=BB19_7 Depth=1
	movzwl	2(%r12), %edi
	cmpw	%di, 20(%rsp)           # 2-byte Folded Reload
	jb	.LBB19_21
# BB#13:                                #   in Loop: Header=BB19_7 Depth=1
	movzwl	%si, %ecx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	movss	40(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	divss	%xmm8, %xmm0
	movzwl	%di, %ecx
	cvtsi2ssl	%ecx, %xmm1
	movss	44(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	divss	%xmm9, %xmm1
	movzwl	%dx, %ecx
	cvtsi2ssl	%ecx, %xmm2
	movss	48(%rbx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	divss	%xmm11, %xmm2
	movss	8(%rbx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm0
	addss	%xmm10, %xmm1
	movss	16(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm2
	xorps	%xmm3, %xmm3
	shufps	$1, %xmm2, %xmm3        # xmm3 = xmm3[1,0],xmm2[0,0]
	shufps	$226, %xmm2, %xmm3      # xmm3 = xmm3[2,0],xmm2[2,3]
	movlps	%xmm3, 40(%rsp)
	movzwl	6(%r12), %ecx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ecx, %xmm3
	divss	%xmm8, %xmm3
	movzwl	8(%r12), %ecx
	cvtsi2ssl	%ecx, %xmm4
	divss	%xmm9, %xmm4
	movzwl	10(%r12), %ecx
	cvtsi2ssl	%ecx, %xmm6
	divss	%xmm11, %xmm6
	addss	%xmm7, %xmm3
	addss	%xmm10, %xmm4
	addss	%xmm5, %xmm6
	xorps	%xmm5, %xmm5
	shufps	$1, %xmm6, %xmm5        # xmm5 = xmm5[1,0],xmm6[0,0]
	shufps	$226, %xmm6, %xmm5      # xmm5 = xmm5[2,0],xmm6[2,3]
	movlps	%xmm5, 56(%rsp)
	addss	(%r8), %xmm0
	movss	%xmm0, 32(%rsp)
	addss	4(%r8), %xmm1
	movss	%xmm1, 36(%rsp)
	addss	8(%r8), %xmm2
	movss	%xmm2, 40(%rsp)
	addss	(%r9), %xmm3
	movss	%xmm3, 48(%rsp)
	addss	4(%r9), %xmm4
	movss	%xmm4, 52(%rsp)
	addss	8(%r9), %xmm6
	movss	%xmm6, 56(%rsp)
	movq	128(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movsd	(%r10), %xmm4           # xmm4 = mem[0],zero
	subss	%xmm4, %xmm3
	mulss	%xmm14, %xmm3
	movq	120(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	136(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	subps	%xmm4, %xmm2
	mulps	%xmm14, %xmm2
	movaps	%xmm2, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	ucomiss	%xmm3, %xmm0
	ja	.LBB19_20
# BB#14:                                #   in Loop: Header=BB19_7 Depth=1
	movq	112(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	pshufd	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	subss	%xmm4, %xmm1
	mulss	%xmm15, %xmm1
	ucomiss	%xmm1, %xmm2
	ja	.LBB19_20
# BB#15:                                #   in Loop: Header=BB19_7 Depth=1
	minss	%xmm3, %xmm1
	movq	104(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	8(%r10), %xmm4          # xmm4 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm3
	mulss	%xmm13, %xmm3
	ucomiss	%xmm1, %xmm3
	ja	.LBB19_20
# BB#16:                                #   in Loop: Header=BB19_7 Depth=1
	maxss	%xmm2, %xmm0
	movq	96(%rsp), %rcx          # 8-byte Reload
	movss	(%rcx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm2
	mulss	%xmm13, %xmm2
	ucomiss	%xmm2, %xmm0
	ja	.LBB19_20
# BB#17:                                #   in Loop: Header=BB19_7 Depth=1
	maxss	%xmm0, %xmm3
	minss	%xmm1, %xmm2
	ucomiss	%xmm3, %xmm12
	seta	%dl
	ucomiss	.LCPI19_4, %xmm2
	seta	%cl
	andb	%dl, %cl
	testl	%eax, %eax
	js	.LBB19_21
# BB#18:                                #   in Loop: Header=BB19_7 Depth=1
	testb	%cl, %cl
	je	.LBB19_21
# BB#19:                                # %.thread.thread
                                        #   in Loop: Header=BB19_7 Depth=1
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	12(%r12), %edx
	movl	%edx, %esi
	sarl	$21, %esi
	andl	$2097151, %edx          # imm = 0x1FFFFF
	movq	%r9, %rbp
	movq	%r8, %r14
	callq	*16(%rax)
	movaps	144(%rsp), %xmm15       # 16-byte Reload
	movaps	160(%rsp), %xmm14       # 16-byte Reload
	movss	8(%rsp), %xmm13         # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movl	248(%rsp), %r11d
	movss	4(%rsp), %xmm12         # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movq	80(%rsp), %r10          # 8-byte Reload
	movq	%r14, %r8
	movl	16(%rsp), %r14d         # 4-byte Reload
	movq	%rbp, %r9
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	$1, %eax
	jmp	.LBB19_24
	.p2align	4, 0x90
.LBB19_20:                              # %.thread195
                                        #   in Loop: Header=BB19_7 Depth=1
	xorl	%ecx, %ecx
.LBB19_21:                              # %.thread
                                        #   in Loop: Header=BB19_7 Depth=1
	testl	%eax, %eax
	movl	$1, %eax
	jns	.LBB19_24
# BB#22:                                # %.thread
                                        #   in Loop: Header=BB19_7 Depth=1
	testb	%cl, %cl
	movl	$1, %ecx
	jne	.LBB19_25
# BB#23:                                #   in Loop: Header=BB19_7 Depth=1
	movslq	12(%r12), %rcx
	negq	%rcx
	movl	%ecx, %eax
	jmp	.LBB19_25
	.p2align	4, 0x90
.LBB19_24:                              #   in Loop: Header=BB19_7 Depth=1
	movl	$1, %ecx
.LBB19_25:                              #   in Loop: Header=BB19_7 Depth=1
	incl	%r13d
	shlq	$4, %rcx
	addq	%rcx, %r12
	addl	%eax, %r15d
	cmpl	%r11d, %r15d
	jl	.LBB19_7
.LBB19_26:                              # %._crit_edge
	cmpl	%r13d, maxIterations(%rip)
	jge	.LBB19_28
# BB#27:
	movl	%r13d, maxIterations(%rip)
.LBB19_28:
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	_ZNK14btQuantizedBvh36walkStacklessQuantizedTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii, .Lfunc_end19-_ZNK14btQuantizedBvh36walkStacklessQuantizedTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii
	.cfi_endproc

	.globl	_ZNK14btQuantizedBvh25reportRayOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK14btQuantizedBvh25reportRayOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_,@function
_ZNK14btQuantizedBvh25reportRayOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_: # @_ZNK14btQuantizedBvh25reportRayOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_
	.cfi_startproc
# BB#0:
	subq	$56, %rsp
.Lcfi150:
	.cfi_def_cfa_offset 64
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	cmpb	$0, 64(%rdi)
	je	.LBB20_2
# BB#1:
	movl	60(%rdi), %eax
	movl	%eax, 8(%rsp)
	movl	$0, (%rsp)
	leaq	32(%rsp), %r8
	leaq	16(%rsp), %r9
	callq	_ZNK14btQuantizedBvh36walkStacklessQuantizedTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii
	addq	$56, %rsp
	retq
.LBB20_2:
	leaq	32(%rsp), %r8
	leaq	16(%rsp), %r9
	callq	_ZNK14btQuantizedBvh27walkStacklessTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii
	addq	$56, %rsp
	retq
.Lfunc_end20:
	.size	_ZNK14btQuantizedBvh25reportRayOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_, .Lfunc_end20-_ZNK14btQuantizedBvh25reportRayOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_
	.cfi_endproc

	.globl	_ZNK14btQuantizedBvh29reportBoxCastOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_
	.p2align	4, 0x90
	.type	_ZNK14btQuantizedBvh29reportBoxCastOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_,@function
_ZNK14btQuantizedBvh29reportBoxCastOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_: # @_ZNK14btQuantizedBvh29reportBoxCastOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi151:
	.cfi_def_cfa_offset 32
	cmpb	$0, 64(%rdi)
	je	.LBB21_2
# BB#1:
	movl	60(%rdi), %eax
	movl	%eax, 8(%rsp)
	movl	$0, (%rsp)
	callq	_ZNK14btQuantizedBvh36walkStacklessQuantizedTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii
	addq	$24, %rsp
	retq
.LBB21_2:
	callq	_ZNK14btQuantizedBvh27walkStacklessTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii
	addq	$24, %rsp
	retq
.Lfunc_end21:
	.size	_ZNK14btQuantizedBvh29reportBoxCastOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_, .Lfunc_end21-_ZNK14btQuantizedBvh29reportBoxCastOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_
	.cfi_endproc

	.globl	_ZN14btQuantizedBvh32getAlignmentSerializationPaddingEv
	.p2align	4, 0x90
	.type	_ZN14btQuantizedBvh32getAlignmentSerializationPaddingEv,@function
_ZN14btQuantizedBvh32getAlignmentSerializationPaddingEv: # @_ZN14btQuantizedBvh32getAlignmentSerializationPaddingEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end22:
	.size	_ZN14btQuantizedBvh32getAlignmentSerializationPaddingEv, .Lfunc_end22-_ZN14btQuantizedBvh32getAlignmentSerializationPaddingEv
	.cfi_endproc

	.globl	_ZN14btQuantizedBvh28calculateSerializeBufferSizeEv
	.p2align	4, 0x90
	.type	_ZN14btQuantizedBvh28calculateSerializeBufferSizeEv,@function
_ZN14btQuantizedBvh28calculateSerializeBufferSizeEv: # @_ZN14btQuantizedBvh28calculateSerializeBufferSizeEv
	.cfi_startproc
# BB#0:
	movl	60(%rdi), %eax
	movl	240(%rdi), %edx
	shll	$5, %edx
	cmpb	$0, 64(%rdi)
	movb	$6, %cl
	je	.LBB23_2
# BB#1:
	movb	$4, %cl
.LBB23_2:
	shll	%cl, %eax
	leal	248(%rdx,%rax), %eax
	retq
.Lfunc_end23:
	.size	_ZN14btQuantizedBvh28calculateSerializeBufferSizeEv, .Lfunc_end23-_ZN14btQuantizedBvh28calculateSerializeBufferSizeEv
	.cfi_endproc

	.globl	_ZN14btQuantizedBvh9serializeEPvjb
	.p2align	4, 0x90
	.type	_ZN14btQuantizedBvh9serializeEPvjb,@function
_ZN14btQuantizedBvh9serializeEPvjb:     # @_ZN14btQuantizedBvh9serializeEPvjb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi152:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi153:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi155:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi156:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi158:
	.cfi_def_cfa_offset 96
.Lcfi159:
	.cfi_offset %rbx, -56
.Lcfi160:
	.cfi_offset %r12, -48
.Lcfi161:
	.cfi_offset %r13, -40
.Lcfi162:
	.cfi_offset %r14, -32
.Lcfi163:
	.cfi_offset %r15, -24
.Lcfi164:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebx
	movq	%rsi, %r13
	movq	%rdi, %r14
	movl	212(%r14), %eax
	movl	%eax, 240(%r14)
	movq	$_ZTV14btQuantizedBvh+16, (%r13)
	movl	$275, 56(%r13)          # imm = 0x113
	movb	$0, 64(%r13)
	movb	$1, 96(%r13)
	movq	$0, 88(%r13)
	movl	$0, 76(%r13)
	movl	$0, 80(%r13)
	movb	$1, 128(%r13)
	movq	$0, 120(%r13)
	movl	$0, 108(%r13)
	movl	$0, 112(%r13)
	movb	$1, 160(%r13)
	movq	$0, 152(%r13)
	movl	$0, 140(%r13)
	movl	$0, 144(%r13)
	movb	$1, 192(%r13)
	movq	$0, 184(%r13)
	movl	$0, 172(%r13)
	movl	$0, 176(%r13)
	movl	$0, 200(%r13)
	movb	$1, 232(%r13)
	movq	$0, 224(%r13)
	movl	$0, 212(%r13)
	movl	$0, 216(%r13)
	movl	$0, 240(%r13)
	movl	$-8388609, 8(%r13)      # imm = 0xFF7FFFFF
	movl	$-8388609, 12(%r13)     # imm = 0xFF7FFFFF
	movl	$-8388609, 16(%r13)     # imm = 0xFF7FFFFF
	movl	$0, 20(%r13)
	movabsq	$9187343237679939583, %rax # imm = 0x7F7FFFFF7F7FFFFF
	movq	%rax, 24(%r13)
	movq	$2139095039, 32(%r13)   # imm = 0x7F7FFFFF
	movl	60(%r14), %eax
	testb	%bl, %bl
	je	.LBB24_2
# BB#1:
	bswapl	%eax
	movl	%eax, 60(%r13)
	movb	11(%r14), %al
	movb	%al, 8(%r13)
	movb	10(%r14), %al
	movb	%al, 9(%r13)
	movb	9(%r14), %al
	movb	%al, 10(%r13)
	movb	8(%r14), %al
	movb	%al, 11(%r13)
	movb	15(%r14), %al
	movb	%al, 12(%r13)
	movb	14(%r14), %al
	movb	%al, 13(%r13)
	movb	13(%r14), %al
	movb	%al, 14(%r13)
	movb	12(%r14), %al
	movb	%al, 15(%r13)
	movb	19(%r14), %al
	movb	%al, 16(%r13)
	movb	18(%r14), %al
	movb	%al, 17(%r13)
	movb	17(%r14), %al
	movb	%al, 18(%r13)
	movb	16(%r14), %al
	movb	%al, 19(%r13)
	movb	23(%r14), %al
	movb	%al, 20(%r13)
	movb	22(%r14), %al
	movb	%al, 21(%r13)
	movb	21(%r14), %al
	movb	%al, 22(%r13)
	movb	20(%r14), %al
	movb	%al, 23(%r13)
	movb	27(%r14), %al
	movb	%al, 24(%r13)
	movb	26(%r14), %al
	movb	%al, 25(%r13)
	movb	25(%r14), %al
	movb	%al, 26(%r13)
	movb	24(%r14), %al
	movb	%al, 27(%r13)
	movb	31(%r14), %al
	movb	%al, 28(%r13)
	movb	30(%r14), %al
	movb	%al, 29(%r13)
	movb	29(%r14), %al
	movb	%al, 30(%r13)
	movb	28(%r14), %al
	movb	%al, 31(%r13)
	movb	35(%r14), %al
	movb	%al, 32(%r13)
	movb	34(%r14), %al
	movb	%al, 33(%r13)
	movb	33(%r14), %al
	movb	%al, 34(%r13)
	movb	32(%r14), %al
	movb	%al, 35(%r13)
	movb	39(%r14), %al
	movb	%al, 36(%r13)
	movb	38(%r14), %al
	movb	%al, 37(%r13)
	movb	37(%r14), %al
	movb	%al, 38(%r13)
	movb	36(%r14), %al
	movb	%al, 39(%r13)
	movb	43(%r14), %al
	movb	%al, 40(%r13)
	movb	42(%r14), %al
	movb	%al, 41(%r13)
	movb	41(%r14), %al
	movb	%al, 42(%r13)
	movb	40(%r14), %al
	movb	%al, 43(%r13)
	movb	47(%r14), %al
	movb	%al, 44(%r13)
	movb	46(%r14), %al
	movb	%al, 45(%r13)
	movb	45(%r14), %al
	movb	%al, 46(%r13)
	movb	44(%r14), %al
	movb	%al, 47(%r13)
	movb	51(%r14), %al
	movb	%al, 48(%r13)
	movb	50(%r14), %al
	movb	%al, 49(%r13)
	movb	49(%r14), %al
	movb	%al, 50(%r13)
	movb	48(%r14), %al
	movb	%al, 51(%r13)
	movb	55(%r14), %al
	movb	%al, 52(%r13)
	movb	54(%r14), %al
	movb	%al, 53(%r13)
	movb	53(%r14), %al
	movb	%al, 54(%r13)
	movb	52(%r14), %al
	movb	%al, 55(%r13)
	movl	200(%r14), %eax
	bswapl	%eax
	movl	%eax, 200(%r13)
	movl	240(%r14), %eax
	bswapl	%eax
	jmp	.LBB24_3
.LBB24_2:
	leaq	8(%r13), %rsi
	leaq	24(%r13), %rdx
	movl	%eax, 60(%r13)
	movups	8(%r14), %xmm0
	movups	%xmm0, (%rsi)
	movups	24(%r14), %xmm0
	movups	%xmm0, (%rdx)
	movups	40(%r14), %xmm0
	movups	%xmm0, 40(%r13)
	movl	200(%r14), %eax
	movl	%eax, 200(%r13)
	movl	240(%r14), %eax
.LBB24_3:
	movl	%eax, 240(%r13)
	movb	64(%r14), %al
	movb	%al, 64(%r13)
	leaq	248(%r13), %rbp
	movl	60(%r14), %r12d
	movslq	%r12d, %r8
	cmpb	$0, 64(%r14)
	je	.LBB24_12
# BB#4:
	leaq	192(%r13), %rsi
	leaq	184(%r13), %r15
	leaq	172(%r13), %r9
	leaq	176(%r13), %r10
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB24_8
# BB#5:
	cmpb	$0, (%rsi)
	je	.LBB24_7
# BB#6:
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%r9, (%rsp)             # 8-byte Spill
	movq	%r10, 24(%rsp)          # 8-byte Spill
	callq	_Z21btAlignedFreeInternalPv
	movq	24(%rsp), %r10          # 8-byte Reload
	movq	(%rsp), %r9             # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB24_7:
	movq	$0, (%r15)
.LBB24_8:                               # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE20initializeFromBufferEPvii.exit233
	movb	$0, (%rsi)
	movq	%rbp, (%r15)
	movl	%r12d, (%r9)
	movl	%r12d, (%r10)
	testb	%bl, %bl
	je	.LBB24_20
# BB#9:                                 # %.preheader243
	testl	%r12d, %r12d
	jle	.LBB24_23
# BB#10:                                # %.lr.ph255
	movq	184(%r14), %rax
	addq	$12, %rax
	leaq	260(%r13), %rdi
	.p2align	4, 0x90
.LBB24_11:                              # =>This Inner Loop Header: Depth=1
	movzwl	-12(%rax), %edx
	rolw	$8, %dx
	movw	%dx, -12(%rdi)
	movzwl	-10(%rax), %edx
	rolw	$8, %dx
	movw	%dx, -10(%rdi)
	movzwl	-8(%rax), %edx
	rolw	$8, %dx
	movw	%dx, -8(%rdi)
	movzwl	-6(%rax), %edx
	rolw	$8, %dx
	movw	%dx, -6(%rdi)
	movzwl	-4(%rax), %edx
	rolw	$8, %dx
	movw	%dx, -4(%rdi)
	movzwl	-2(%rax), %edx
	rolw	$8, %dx
	movw	%dx, -2(%rdi)
	movl	(%rax), %edx
	bswapl	%edx
	movl	%edx, (%rdi)
	addq	$16, %rax
	addq	$16, %rdi
	decq	%r12
	jne	.LBB24_11
	jmp	.LBB24_23
.LBB24_12:
	movq	%r13, %rsi
	subq	$-128, %rsi
	leaq	120(%r13), %r15
	leaq	108(%r13), %r9
	leaq	112(%r13), %r10
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB24_16
# BB#13:
	cmpb	$0, (%rsi)
	je	.LBB24_15
# BB#14:
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%r9, (%rsp)             # 8-byte Spill
	movq	%r10, 24(%rsp)          # 8-byte Spill
	callq	_Z21btAlignedFreeInternalPv
	movq	24(%rsp), %r10          # 8-byte Reload
	movq	(%rsp), %r9             # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB24_15:
	movq	$0, (%r15)
.LBB24_16:                              # %_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE20initializeFromBufferEPvii.exit224
	movb	$0, (%rsi)
	movq	%rbp, (%r15)
	movl	%r12d, (%r9)
	movl	%r12d, (%r10)
	testb	%bl, %bl
	movl	%ebx, 20(%rsp)          # 4-byte Spill
	je	.LBB24_24
# BB#17:                                # %.preheader239
	testl	%r12d, %r12d
	jle	.LBB24_28
# BB#18:                                # %.lr.ph251
	movq	120(%r14), %rcx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	.p2align	4, 0x90
.LBB24_19:                              # =>This Inner Loop Header: Depth=1
	movzbl	3(%rcx,%rax), %edx
	movb	%dl, (%rdi,%rax)
	movzbl	2(%rcx,%rax), %edx
	movb	%dl, 1(%rdi,%rax)
	movzbl	1(%rcx,%rax), %edx
	movb	%dl, 2(%rdi,%rax)
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 3(%rdi,%rax)
	movzbl	7(%rcx,%rax), %edx
	movb	%dl, 4(%rdi,%rax)
	movzbl	6(%rcx,%rax), %edx
	movb	%dl, 5(%rdi,%rax)
	movzbl	5(%rcx,%rax), %edx
	movb	%dl, 6(%rdi,%rax)
	movzbl	4(%rcx,%rax), %edx
	movb	%dl, 7(%rdi,%rax)
	movzbl	11(%rcx,%rax), %edx
	movb	%dl, 8(%rdi,%rax)
	movzbl	10(%rcx,%rax), %edx
	movb	%dl, 9(%rdi,%rax)
	movzbl	9(%rcx,%rax), %edx
	movb	%dl, 10(%rdi,%rax)
	movzbl	8(%rcx,%rax), %edx
	movb	%dl, 11(%rdi,%rax)
	movzbl	15(%rcx,%rax), %edx
	movb	%dl, 12(%rdi,%rax)
	movzbl	14(%rcx,%rax), %edx
	movb	%dl, 13(%rdi,%rax)
	movzbl	13(%rcx,%rax), %edx
	movb	%dl, 14(%rdi,%rax)
	movzbl	12(%rcx,%rax), %ecx
	movb	%cl, 15(%rdi,%rax)
	movq	120(%r14), %rcx
	movq	(%r15), %rdx
	movzbl	19(%rcx,%rax), %ebx
	movb	%bl, 16(%rdx,%rax)
	movzbl	18(%rcx,%rax), %ebx
	movb	%bl, 17(%rdx,%rax)
	movzbl	17(%rcx,%rax), %ebx
	movb	%bl, 18(%rdx,%rax)
	movzbl	16(%rcx,%rax), %ebx
	movb	%bl, 19(%rdx,%rax)
	movzbl	23(%rcx,%rax), %ebx
	movb	%bl, 20(%rdx,%rax)
	movzbl	22(%rcx,%rax), %ebx
	movb	%bl, 21(%rdx,%rax)
	movzbl	21(%rcx,%rax), %ebx
	movb	%bl, 22(%rdx,%rax)
	movzbl	20(%rcx,%rax), %ebx
	movb	%bl, 23(%rdx,%rax)
	movzbl	27(%rcx,%rax), %ebx
	movb	%bl, 24(%rdx,%rax)
	movzbl	26(%rcx,%rax), %ebx
	movb	%bl, 25(%rdx,%rax)
	movzbl	25(%rcx,%rax), %ebx
	movb	%bl, 26(%rdx,%rax)
	movzbl	24(%rcx,%rax), %ebx
	movb	%bl, 27(%rdx,%rax)
	movzbl	31(%rcx,%rax), %ebx
	movb	%bl, 28(%rdx,%rax)
	movzbl	30(%rcx,%rax), %ebx
	movb	%bl, 29(%rdx,%rax)
	movzbl	29(%rcx,%rax), %ebx
	movb	%bl, 30(%rdx,%rax)
	movzbl	28(%rcx,%rax), %ecx
	movb	%cl, 31(%rdx,%rax)
	movq	120(%r14), %rcx
	movl	32(%rcx,%rax), %edx
	bswapl	%edx
	movq	(%r15), %rdi
	movl	%edx, 32(%rdi,%rax)
	movl	36(%rcx,%rax), %edx
	bswapl	%edx
	movl	%edx, 36(%rdi,%rax)
	movl	40(%rcx,%rax), %edx
	bswapl	%edx
	movl	%edx, 40(%rdi,%rax)
	addq	$64, %rax
	decq	%r12
	jne	.LBB24_19
	jmp	.LBB24_29
.LBB24_20:                              # %.preheader245
	testl	%r12d, %r12d
	jle	.LBB24_23
# BB#21:                                # %.lr.ph257
	movq	184(%r14), %rax
	addq	$12, %rax
	leaq	260(%r13), %rdi
	.p2align	4, 0x90
.LBB24_22:                              # =>This Inner Loop Header: Depth=1
	movzwl	-12(%rax), %edx
	movw	%dx, -12(%rdi)
	movzwl	-10(%rax), %edx
	movw	%dx, -10(%rdi)
	movzwl	-8(%rax), %edx
	movw	%dx, -8(%rdi)
	movzwl	-6(%rax), %edx
	movw	%dx, -6(%rdi)
	movzwl	-4(%rax), %edx
	movw	%dx, -4(%rdi)
	movzwl	-2(%rax), %edx
	movw	%dx, -2(%rdi)
	movl	(%rax), %edx
	movl	%edx, (%rdi)
	addq	$16, %rax
	addq	$16, %rdi
	decq	%r12
	jne	.LBB24_22
.LBB24_23:                              # %.loopexit
	shlq	$4, %r8
	movq	$0, (%r15)
	jmp	.LBB24_34
.LBB24_24:                              # %.preheader241
	testl	%r12d, %r12d
	jle	.LBB24_28
# BB#25:                                # %.lr.ph253
	movq	120(%r14), %rcx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	.p2align	4, 0x90
.LBB24_26:                              # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rax), %xmm0
	movups	%xmm0, (%rdi,%rax)
	movq	120(%r14), %rcx
	movq	(%r15), %rdx
	movups	16(%rcx,%rax), %xmm0
	movups	%xmm0, 16(%rdx,%rax)
	movq	120(%r14), %rcx
	movl	32(%rcx,%rax), %edx
	movq	(%r15), %rdi
	movl	%edx, 32(%rdi,%rax)
	movl	36(%rcx,%rax), %edx
	movl	%edx, 36(%rdi,%rax)
	movl	40(%rcx,%rax), %edx
	movl	%edx, 40(%rdi,%rax)
	addq	$64, %rax
	decq	%r12
	jne	.LBB24_26
	jmp	.LBB24_29
.LBB24_28:
	movq	%rbp, %rdi
.LBB24_29:                              # %.loopexit240
	shlq	$6, %r8
	testq	%rdi, %rdi
	je	.LBB24_33
# BB#30:
	cmpb	$0, (%rsi)
	je	.LBB24_32
# BB#31:
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%rsi, %rbx
	movq	%r9, (%rsp)             # 8-byte Spill
	movq	%r10, %r12
	callq	_Z21btAlignedFreeInternalPv
	movq	%r12, %r10
	movq	(%rsp), %r9             # 8-byte Reload
	movq	%rbx, %rsi
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB24_32:
	movq	$0, (%r15)
.LBB24_33:                              # %_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE20initializeFromBufferEPvii.exit
	movl	20(%rsp), %ebx          # 4-byte Reload
.LBB24_34:                              # %_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE20initializeFromBufferEPvii.exit
	movb	$0, (%rsi)
	movq	$0, (%r15)
	movl	$0, (%r9)
	movl	$0, (%r10)
	addq	%r8, %rbp
	movl	240(%r14), %r15d
	movq	224(%r13), %rdi
	testq	%rdi, %rdi
	movl	%r15d, %edx
	je	.LBB24_38
# BB#35:
	cmpb	$0, 232(%r13)
	movl	%r15d, %edx
	je	.LBB24_37
# BB#36:
	movq	%r8, %r12
	callq	_Z21btAlignedFreeInternalPv
	movq	%r12, %r8
	movl	240(%r14), %edx
.LBB24_37:
	movq	$0, 224(%r13)
.LBB24_38:                              # %_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE20initializeFromBufferEPvii.exit221
	movb	$0, 232(%r13)
	movq	%rbp, 224(%r13)
	movl	%r15d, 212(%r13)
	movl	%r15d, 216(%r13)
	testb	%bl, %bl
	je	.LBB24_42
# BB#39:                                # %.preheader
	testl	%edx, %edx
	jle	.LBB24_45
# BB#40:                                # %.lr.ph
	movq	224(%r14), %rax
	movslq	%edx, %rcx
	addq	$16, %rax
	leaq	264(%r13,%r8), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB24_41:                              # =>This Inner Loop Header: Depth=1
	movzwl	-16(%rax), %edi
	rolw	$8, %di
	movw	%di, -16(%rdx)
	movzwl	-14(%rax), %edi
	rolw	$8, %di
	movw	%di, -14(%rdx)
	movzwl	-12(%rax), %edi
	rolw	$8, %di
	movw	%di, -12(%rdx)
	movzwl	-10(%rax), %edi
	rolw	$8, %di
	movw	%di, -10(%rdx)
	movzwl	-8(%rax), %edi
	rolw	$8, %di
	movw	%di, -8(%rdx)
	movzwl	-6(%rax), %edi
	rolw	$8, %di
	movw	%di, -6(%rdx)
	movl	-4(%rax), %edi
	bswapl	%edi
	movl	%edi, -4(%rdx)
	movl	(%rax), %edi
	bswapl	%edi
	movl	%edi, (%rdx)
	incq	%rsi
	addq	$32, %rax
	addq	$32, %rdx
	cmpq	%rcx, %rsi
	jl	.LBB24_41
	jmp	.LBB24_45
.LBB24_42:                              # %.preheader237
	testl	%edx, %edx
	jle	.LBB24_45
# BB#43:                                # %.lr.ph249
	movq	224(%r14), %rax
	addq	$16, %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB24_44:                              # =>This Inner Loop Header: Depth=1
	movzwl	-16(%rax), %edx
	movw	%dx, (%rbp)
	movzwl	-14(%rax), %edx
	movw	%dx, 2(%rbp)
	movzwl	-12(%rax), %edx
	movw	%dx, 4(%rbp)
	movzwl	-10(%rax), %edx
	movw	%dx, 6(%rbp)
	movzwl	-8(%rax), %edx
	movw	%dx, 8(%rbp)
	movzwl	-6(%rax), %edx
	movw	%dx, 10(%rbp)
	movl	-4(%rax), %edx
	movl	%edx, 12(%rbp)
	movl	(%rax), %edx
	movl	%edx, 16(%rbp)
	movl	$0, 20(%rbp)
	movl	$0, 24(%rbp)
	movl	$0, 28(%rbp)
	incq	%rcx
	movslq	240(%r14), %rdx
	addq	$32, %rax
	addq	$32, %rbp
	cmpq	%rdx, %rcx
	jl	.LBB24_44
.LBB24_45:                              # %_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE20initializeFromBufferEPvii.exit
	movb	$0, 232(%r13)
	movq	$0, 224(%r13)
	movl	$0, 212(%r13)
	movl	$0, 216(%r13)
	movq	$0, (%r13)
	movb	$1, %al
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	_ZN14btQuantizedBvh9serializeEPvjb, .Lfunc_end24-_ZN14btQuantizedBvh9serializeEPvjb
	.cfi_endproc

	.globl	_ZN14btQuantizedBvh18deSerializeInPlaceEPvjb
	.p2align	4, 0x90
	.type	_ZN14btQuantizedBvh18deSerializeInPlaceEPvjb,@function
_ZN14btQuantizedBvh18deSerializeInPlaceEPvjb: # @_ZN14btQuantizedBvh18deSerializeInPlaceEPvjb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi165:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi166:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi167:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi168:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi169:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi170:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi171:
	.cfi_def_cfa_offset 80
.Lcfi172:
	.cfi_offset %rbx, -56
.Lcfi173:
	.cfi_offset %r12, -48
.Lcfi174:
	.cfi_offset %r13, -40
.Lcfi175:
	.cfi_offset %r14, -32
.Lcfi176:
	.cfi_offset %r15, -24
.Lcfi177:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB25_1
# BB#2:
	testb	%dl, %dl
	je	.LBB25_3
# BB#4:
	movl	60(%rbx), %eax
	bswapl	%eax
	movl	%eax, 60(%rbx)
	movb	11(%rbx), %al
	movb	%al, 3(%rsp)            # 1-byte Spill
	movb	10(%rbx), %cl
	movb	8(%rbx), %al
	movb	%al, 4(%rsp)            # 1-byte Spill
	movb	9(%rbx), %al
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movb	15(%rbx), %r8b
	movb	14(%rbx), %r9b
	movb	13(%rbx), %r10b
	movb	12(%rbx), %r11b
	movb	19(%rbx), %bpl
	movb	18(%rbx), %r15b
	movb	17(%rbx), %r12b
	movb	16(%rbx), %r13b
	movb	23(%rbx), %r14b
	movb	22(%rbx), %sil
	movb	21(%rbx), %dil
	movb	20(%rbx), %dl
	movb	%dl, 2(%rsp)            # 1-byte Spill
	movb	3(%rsp), %dl            # 1-byte Reload
	movb	%dl, 8(%rbx)
	movb	%cl, 9(%rbx)
	movb	%al, 10(%rbx)
	movb	4(%rsp), %al            # 1-byte Reload
	movb	%al, 11(%rbx)
	movb	%r8b, 12(%rbx)
	movb	%r9b, 13(%rbx)
	movb	%r10b, 14(%rbx)
	movb	%r11b, 15(%rbx)
	movb	%bpl, 16(%rbx)
	movb	%r15b, 17(%rbx)
	movb	%r12b, 18(%rbx)
	movb	%r13b, 19(%rbx)
	movb	%r14b, 20(%rbx)
	movb	%sil, 21(%rbx)
	movb	%dil, 22(%rbx)
	movb	2(%rsp), %al            # 1-byte Reload
	movb	%al, 23(%rbx)
	movb	27(%rbx), %al
	movb	%al, 2(%rsp)            # 1-byte Spill
	movb	26(%rbx), %cl
	movb	24(%rbx), %dl
	movb	%dl, 4(%rsp)            # 1-byte Spill
	movb	25(%rbx), %dl
	movb	31(%rbx), %dil
	movb	30(%rbx), %r8b
	movb	29(%rbx), %r9b
	movb	28(%rbx), %r10b
	movb	35(%rbx), %r11b
	movb	34(%rbx), %bpl
	movb	33(%rbx), %r14b
	movb	32(%rbx), %r15b
	movb	39(%rbx), %r12b
	movb	38(%rbx), %r13b
	movb	37(%rbx), %sil
	movb	36(%rbx), %al
	movb	%al, 3(%rsp)            # 1-byte Spill
	movb	2(%rsp), %al            # 1-byte Reload
	movb	%al, 24(%rbx)
	movb	%cl, 25(%rbx)
	movb	%dl, 26(%rbx)
	movb	4(%rsp), %al            # 1-byte Reload
	movb	%al, 27(%rbx)
	movb	%dil, 28(%rbx)
	movb	%r8b, 29(%rbx)
	movb	%r9b, 30(%rbx)
	movb	%r10b, 31(%rbx)
	movb	%r11b, 32(%rbx)
	movb	%bpl, 33(%rbx)
	movb	%r14b, 34(%rbx)
	movb	%r15b, 35(%rbx)
	movb	%r12b, 36(%rbx)
	movb	%r13b, 37(%rbx)
	movb	%sil, 38(%rbx)
	movb	3(%rsp), %al            # 1-byte Reload
	movb	%al, 39(%rbx)
	movb	43(%rbx), %al
	movb	%al, 5(%rsp)            # 1-byte Spill
	movb	42(%rbx), %cl
	movb	40(%rbx), %dl
	movb	%dl, 3(%rsp)            # 1-byte Spill
	movb	41(%rbx), %dl
	movb	47(%rbx), %dil
	movb	46(%rbx), %r8b
	movb	45(%rbx), %r9b
	movb	44(%rbx), %r10b
	movb	51(%rbx), %r11b
	movb	50(%rbx), %bpl
	movb	49(%rbx), %r14b
	movb	48(%rbx), %r15b
	movb	55(%rbx), %r12b
	movb	54(%rbx), %r13b
	movb	53(%rbx), %sil
	movb	%sil, 4(%rsp)           # 1-byte Spill
	movb	52(%rbx), %al
	movb	%al, 2(%rsp)            # 1-byte Spill
	movb	5(%rsp), %al            # 1-byte Reload
	movb	%al, 40(%rbx)
	movb	%cl, 41(%rbx)
	movb	%dl, 42(%rbx)
	movb	3(%rsp), %al            # 1-byte Reload
	movb	%al, 43(%rbx)
	movb	%dil, 44(%rbx)
	movb	%r8b, 45(%rbx)
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	movb	%r9b, 46(%rbx)
	movb	%r10b, 47(%rbx)
	movb	%r11b, 48(%rbx)
	movb	%bpl, 49(%rbx)
	movb	%r14b, 50(%rbx)
	movb	%r15b, 51(%rbx)
	movb	%r12b, 52(%rbx)
	movb	%r13b, 53(%rbx)
	movb	4(%rsp), %al            # 1-byte Reload
	movb	%al, 54(%rbx)
	movb	2(%rsp), %al            # 1-byte Reload
	movb	%al, 55(%rbx)
	movl	200(%rbx), %eax
	bswapl	%eax
	movl	%eax, 200(%rbx)
	movl	240(%rbx), %r15d
	bswapl	%r15d
	movl	%r15d, 240(%rbx)
	jmp	.LBB25_5
.LBB25_1:
	xorl	%ebx, %ebx
	jmp	.LBB25_28
.LBB25_3:                               # %._crit_edge
	movl	240(%rbx), %r15d
.LBB25_5:
	movl	%r15d, %ebp
	shll	$5, %ebp
	movb	64(%rbx), %r9b
	testb	%r9b, %r9b
	movl	60(%rbx), %edi
	movb	$6, %cl
	je	.LBB25_7
# BB#6:
	movb	$4, %cl
.LBB25_7:
	movl	%edi, %eax
	shll	%cl, %eax
	leal	248(%rbp,%rax), %ecx
	cmpl	%esi, %ecx
	jbe	.LBB25_9
# BB#8:
	xorl	%ebx, %ebx
	jmp	.LBB25_28
.LBB25_9:
	movslq	%edi, %r8
	leaq	248(%rbx), %rbp
	testb	%r9b, %r9b
	movq	$_ZTV14btQuantizedBvh+16, (%rbx)
	movl	$275, 56(%rbx)          # imm = 0x113
	movb	$1, 96(%rbx)
	movq	$0, 88(%rbx)
	movl	$0, 76(%rbx)
	movl	$0, 80(%rbx)
	movb	$1, 128(%rbx)
	movq	$0, 120(%rbx)
	movl	$0, 108(%rbx)
	movl	$0, 112(%rbx)
	movb	$1, 160(%rbx)
	movq	$0, 152(%rbx)
	movl	$0, 140(%rbx)
	movl	$0, 144(%rbx)
	movb	$1, 192(%rbx)
	movq	$0, 184(%rbx)
	movl	$0, 172(%rbx)
	movl	$0, 176(%rbx)
	movb	$1, 232(%rbx)
	movq	$0, 224(%rbx)
	movq	$0, 212(%rbx)
	je	.LBB25_15
# BB#10:                                # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE20initializeFromBufferEPvii.exit
	movb	$0, 192(%rbx)
	movq	%rbp, 184(%rbx)
	movl	%edi, 172(%rbx)
	movl	%edi, 176(%rbx)
	movl	$4, %ecx
	testl	%edi, %edi
	jle	.LBB25_14
# BB#11:                                # %_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE20initializeFromBufferEPvii.exit
	testb	%dl, %dl
	je	.LBB25_14
# BB#12:                                # %.lr.ph241.preheader
	leaq	260(%rbx), %rsi
	movl	$4, %ecx
	.p2align	4, 0x90
.LBB25_13:                              # %.lr.ph241
                                        # =>This Inner Loop Header: Depth=1
	rolw	$8, -12(%rsi)
	rolw	$8, -10(%rsi)
	rolw	$8, -8(%rsi)
	rolw	$8, -6(%rsi)
	rolw	$8, -4(%rsi)
	rolw	$8, -2(%rsi)
	movl	(%rsi), %eax
	bswapl	%eax
	movl	%eax, (%rsi)
	addq	$16, %rsi
	decq	%rdi
	jne	.LBB25_13
	jmp	.LBB25_14
.LBB25_15:                              # %_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE20initializeFromBufferEPvii.exit
	movb	$0, 128(%rbx)
	movq	%rbp, 120(%rbx)
	movl	%edi, 108(%rbx)
	movl	%edi, 112(%rbx)
	movl	$6, %ecx
	testl	%edi, %edi
	jle	.LBB25_14
# BB#16:                                # %_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE20initializeFromBufferEPvii.exit
	testb	%dl, %dl
	je	.LBB25_14
# BB#17:                                # %.lr.ph239.preheader
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movl	%edx, 12(%rsp)          # 4-byte Spill
	xorl	%ecx, %ecx
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB25_18:                              # %.lr.ph239
                                        # =>This Inner Loop Header: Depth=1
	movzbl	3(%rsi,%rcx), %r10d
	movzbl	2(%rsi,%rcx), %eax
	movb	%al, 3(%rsp)            # 1-byte Spill
	movzbl	(%rsi,%rcx), %eax
	movb	%al, 8(%rsp)            # 1-byte Spill
	movzbl	1(%rsi,%rcx), %eax
	movzbl	7(%rsi,%rcx), %r11d
	movzbl	6(%rsi,%rcx), %r14d
	movzbl	5(%rsi,%rcx), %r15d
	movzbl	4(%rsi,%rcx), %r12d
	movzbl	11(%rsi,%rcx), %r13d
	movzbl	10(%rsi,%rcx), %r8d
	movzbl	9(%rsi,%rcx), %r9d
	movzbl	8(%rsi,%rcx), %edx
	movb	%dl, 4(%rsp)            # 1-byte Spill
	movzbl	15(%rsi,%rcx), %edx
	movb	%dl, 2(%rsp)            # 1-byte Spill
	movzbl	14(%rsi,%rcx), %edx
	movb	%dl, 5(%rsp)            # 1-byte Spill
	movzbl	13(%rsi,%rcx), %edx
	movb	%dl, 7(%rsp)            # 1-byte Spill
	movzbl	12(%rsi,%rcx), %edx
	movb	%dl, 6(%rsp)            # 1-byte Spill
	movb	%r10b, (%rsi,%rcx)
	movzbl	3(%rsp), %edx           # 1-byte Folded Reload
	movb	%dl, 1(%rsi,%rcx)
	movb	%al, 2(%rsi,%rcx)
	movzbl	8(%rsp), %eax           # 1-byte Folded Reload
	movb	%al, 3(%rsi,%rcx)
	movb	%r11b, 4(%rsi,%rcx)
	movb	%r14b, 5(%rsi,%rcx)
	movb	%r15b, 6(%rsi,%rcx)
	movb	%r12b, 7(%rsi,%rcx)
	movb	%r13b, 8(%rsi,%rcx)
	movb	%r8b, 9(%rsi,%rcx)
	movb	%r9b, 10(%rsi,%rcx)
	movzbl	4(%rsp), %eax           # 1-byte Folded Reload
	movb	%al, 11(%rsi,%rcx)
	movzbl	2(%rsp), %eax           # 1-byte Folded Reload
	movb	%al, 12(%rsi,%rcx)
	movzbl	5(%rsp), %eax           # 1-byte Folded Reload
	movb	%al, 13(%rsi,%rcx)
	movzbl	7(%rsp), %eax           # 1-byte Folded Reload
	movb	%al, 14(%rsi,%rcx)
	movzbl	6(%rsp), %eax           # 1-byte Folded Reload
	movb	%al, 15(%rsi,%rcx)
	movq	120(%rbx), %rax
	movzbl	19(%rax,%rcx), %esi
	movzbl	18(%rax,%rcx), %edx
	movb	%dl, 4(%rsp)            # 1-byte Spill
	movzbl	16(%rax,%rcx), %edx
	movb	%dl, 8(%rsp)            # 1-byte Spill
	movzbl	17(%rax,%rcx), %r9d
	movzbl	23(%rax,%rcx), %r10d
	movzbl	22(%rax,%rcx), %r11d
	movzbl	21(%rax,%rcx), %r14d
	movzbl	20(%rax,%rcx), %r15d
	movzbl	27(%rax,%rcx), %r12d
	movzbl	26(%rax,%rcx), %r13d
	movzbl	25(%rax,%rcx), %r8d
	movzbl	24(%rax,%rcx), %edx
	movb	%dl, 3(%rsp)            # 1-byte Spill
	movzbl	31(%rax,%rcx), %edx
	movb	%dl, 2(%rsp)            # 1-byte Spill
	movzbl	30(%rax,%rcx), %edx
	movb	%dl, 5(%rsp)            # 1-byte Spill
	movzbl	29(%rax,%rcx), %edx
	movb	%dl, 7(%rsp)            # 1-byte Spill
	movzbl	28(%rax,%rcx), %edx
	movb	%dl, 6(%rsp)            # 1-byte Spill
	movb	%sil, 16(%rax,%rcx)
	movzbl	4(%rsp), %edx           # 1-byte Folded Reload
	movb	%dl, 17(%rax,%rcx)
	movb	%r9b, 18(%rax,%rcx)
	movzbl	8(%rsp), %edx           # 1-byte Folded Reload
	movb	%dl, 19(%rax,%rcx)
	movb	%r10b, 20(%rax,%rcx)
	movb	%r11b, 21(%rax,%rcx)
	movb	%r14b, 22(%rax,%rcx)
	movb	%r15b, 23(%rax,%rcx)
	movb	%r12b, 24(%rax,%rcx)
	movb	%r13b, 25(%rax,%rcx)
	movb	%r8b, 26(%rax,%rcx)
	movzbl	3(%rsp), %edx           # 1-byte Folded Reload
	movb	%dl, 27(%rax,%rcx)
	movzbl	2(%rsp), %edx           # 1-byte Folded Reload
	movb	%dl, 28(%rax,%rcx)
	movzbl	5(%rsp), %edx           # 1-byte Folded Reload
	movb	%dl, 29(%rax,%rcx)
	movzbl	7(%rsp), %edx           # 1-byte Folded Reload
	movb	%dl, 30(%rax,%rcx)
	movzbl	6(%rsp), %edx           # 1-byte Folded Reload
	movb	%dl, 31(%rax,%rcx)
	movq	120(%rbx), %rsi
	movl	32(%rsi,%rcx), %eax
	bswapl	%eax
	movl	%eax, 32(%rsi,%rcx)
	movl	36(%rsi,%rcx), %eax
	bswapl	%eax
	movl	%eax, 36(%rsi,%rcx)
	movl	40(%rsi,%rcx), %eax
	bswapl	%eax
	movl	%eax, 40(%rsi,%rcx)
	addq	$64, %rcx
	decq	%rdi
	jne	.LBB25_18
# BB#19:                                # %.loopexit234
	movl	240(%rbx), %r15d
	movq	224(%rbx), %rdi
	movq	16(%rsp), %rax          # 8-byte Reload
	shlq	$6, %rax
	addq	%rax, %rbp
	testq	%rdi, %rdi
	je	.LBB25_23
# BB#20:
	cmpb	$0, 232(%rbx)
	je	.LBB25_22
# BB#21:
	callq	_Z21btAlignedFreeInternalPv
.LBB25_22:
	movq	$0, 224(%rbx)
.LBB25_23:                              # %_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE20initializeFromBufferEPvii.exit
	movl	12(%rsp), %edx          # 4-byte Reload
	jmp	.LBB25_24
.LBB25_14:                              # %.loopexit234.thread
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %r8
	addq	%r8, %rbp
.LBB25_24:                              # %_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE20initializeFromBufferEPvii.exit
	movb	$0, 232(%rbx)
	movq	%rbp, 224(%rbx)
	movl	%r15d, 212(%rbx)
	movl	%r15d, 216(%rbx)
	testb	%dl, %dl
	je	.LBB25_28
# BB#25:                                # %.preheader
	movslq	240(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB25_28
# BB#26:                                # %.lr.ph
	addq	$16, %rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB25_27:                              # =>This Inner Loop Header: Depth=1
	rolw	$8, -16(%rbp)
	rolw	$8, -14(%rbp)
	rolw	$8, -12(%rbp)
	rolw	$8, -10(%rbp)
	rolw	$8, -8(%rbp)
	rolw	$8, -6(%rbp)
	movl	-4(%rbp), %edx
	bswapl	%edx
	movl	%edx, -4(%rbp)
	movl	(%rbp), %edx
	bswapl	%edx
	movl	%edx, (%rbp)
	incq	%rcx
	addq	$32, %rbp
	cmpq	%rax, %rcx
	jl	.LBB25_27
.LBB25_28:                              # %.loopexit
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	_ZN14btQuantizedBvh18deSerializeInPlaceEPvjb, .Lfunc_end25-_ZN14btQuantizedBvh18deSerializeInPlaceEPvjb
	.cfi_endproc

	.globl	_ZN14btQuantizedBvhC2ERS_b
	.p2align	4, 0x90
	.type	_ZN14btQuantizedBvhC2ERS_b,@function
_ZN14btQuantizedBvhC2ERS_b:             # @_ZN14btQuantizedBvhC2ERS_b
	.cfi_startproc
# BB#0:
	movq	$_ZTV14btQuantizedBvh+16, (%rdi)
	movups	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movups	24(%rsi), %xmm0
	movups	%xmm0, 24(%rdi)
	movups	40(%rsi), %xmm0
	movups	%xmm0, 40(%rdi)
	movl	$275, 56(%rdi)          # imm = 0x113
	movb	$1, 96(%rdi)
	movq	$0, 88(%rdi)
	movl	$0, 76(%rdi)
	movl	$0, 80(%rdi)
	movb	$1, 128(%rdi)
	movq	$0, 120(%rdi)
	movl	$0, 108(%rdi)
	movl	$0, 112(%rdi)
	movb	$1, 160(%rdi)
	movq	$0, 152(%rdi)
	movl	$0, 140(%rdi)
	movl	$0, 144(%rdi)
	movb	$1, 192(%rdi)
	movq	$0, 184(%rdi)
	movl	$0, 172(%rdi)
	movl	$0, 176(%rdi)
	movb	$1, 232(%rdi)
	movq	$0, 224(%rdi)
	movq	$0, 212(%rdi)
	retq
.Lfunc_end26:
	.size	_ZN14btQuantizedBvhC2ERS_b, .Lfunc_end26-_ZN14btQuantizedBvhC2ERS_b
	.cfi_endproc

	.type	_ZTV14btQuantizedBvh,@object # @_ZTV14btQuantizedBvh
	.section	.rodata,"a",@progbits
	.globl	_ZTV14btQuantizedBvh
	.p2align	3
_ZTV14btQuantizedBvh:
	.quad	0
	.quad	_ZTI14btQuantizedBvh
	.quad	_ZN14btQuantizedBvhD2Ev
	.quad	_ZN14btQuantizedBvhD0Ev
	.quad	_ZN14btQuantizedBvh9serializeEPvjb
	.size	_ZTV14btQuantizedBvh, 40

	.type	maxIterations,@object   # @maxIterations
	.bss
	.globl	maxIterations
	.p2align	2
maxIterations:
	.long	0                       # 0x0
	.size	maxIterations, 4

	.type	_ZTS14btQuantizedBvh,@object # @_ZTS14btQuantizedBvh
	.section	.rodata,"a",@progbits
	.globl	_ZTS14btQuantizedBvh
	.p2align	4
_ZTS14btQuantizedBvh:
	.asciz	"14btQuantizedBvh"
	.size	_ZTS14btQuantizedBvh, 17

	.type	_ZTI14btQuantizedBvh,@object # @_ZTI14btQuantizedBvh
	.globl	_ZTI14btQuantizedBvh
	.p2align	3
_ZTI14btQuantizedBvh:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS14btQuantizedBvh
	.size	_ZTI14btQuantizedBvh, 16


	.globl	_ZN14btQuantizedBvhC1Ev
	.type	_ZN14btQuantizedBvhC1Ev,@function
_ZN14btQuantizedBvhC1Ev = _ZN14btQuantizedBvhC2Ev
	.globl	_ZN14btQuantizedBvhD1Ev
	.type	_ZN14btQuantizedBvhD1Ev,@function
_ZN14btQuantizedBvhD1Ev = _ZN14btQuantizedBvhD2Ev
	.globl	_ZN14btQuantizedBvhC1ERS_b
	.type	_ZN14btQuantizedBvhC1ERS_b,@function
_ZN14btQuantizedBvhC1ERS_b = _ZN14btQuantizedBvhC2ERS_b
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
