	.text
	.file	"ZipRegister.bc"
	.p2align	4, 0x90
	.type	_ZL9CreateArcv,@function
_ZL9CreateArcv:                         # @_ZL9CreateArcv
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movl	$256, %edi              # imm = 0x100
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp0:
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NZip8CHandlerC1Ev
.Ltmp1:
# BB#1:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_2:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZL9CreateArcv, .Lfunc_end0-_ZL9CreateArcv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZL12CreateArcOutv,@function
_ZL12CreateArcOutv:                     # @_ZL12CreateArcOutv
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movl	$256, %edi              # imm = 0x100
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp3:
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NZip8CHandlerC1Ev
.Ltmp4:
# BB#1:
	addq	$8, %rbx
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_2:
.Ltmp5:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZL12CreateArcOutv, .Lfunc_end1-_ZL12CreateArcOutv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp3-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Lfunc_end1-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_ZipRegister.ii,@function
_GLOBAL__sub_I_ZipRegister.ii:          # @_GLOBAL__sub_I_ZipRegister.ii
	.cfi_startproc
# BB#0:
	movl	$_ZL9g_ArcInfo, %edi
	jmp	_Z11RegisterArcPK8CArcInfo # TAILCALL
.Lfunc_end2:
	.size	_GLOBAL__sub_I_ZipRegister.ii, .Lfunc_end2-_GLOBAL__sub_I_ZipRegister.ii
	.cfi_endproc

	.type	_ZL9g_ArcInfo,@object   # @_ZL9g_ArcInfo
	.data
	.p2align	3
_ZL9g_ArcInfo:
	.quad	.L.str
	.quad	.L.str.1
	.quad	0
	.byte	1                       # 0x1
	.asciz	"PK\003\004\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.zero	3
	.long	4                       # 0x4
	.byte	0                       # 0x0
	.zero	3
	.quad	_ZL9CreateArcv
	.quad	_ZL12CreateArcOutv
	.size	_ZL9g_ArcInfo, 80

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	122                     # 0x7a
	.long	105                     # 0x69
	.long	112                     # 0x70
	.long	0                       # 0x0
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
	.p2align	2
.L.str.1:
	.long	122                     # 0x7a
	.long	105                     # 0x69
	.long	112                     # 0x70
	.long	32                      # 0x20
	.long	106                     # 0x6a
	.long	97                      # 0x61
	.long	114                     # 0x72
	.long	32                      # 0x20
	.long	120                     # 0x78
	.long	112                     # 0x70
	.long	105                     # 0x69
	.long	32                      # 0x20
	.long	111                     # 0x6f
	.long	100                     # 0x64
	.long	116                     # 0x74
	.long	32                      # 0x20
	.long	111                     # 0x6f
	.long	100                     # 0x64
	.long	115                     # 0x73
	.long	32                      # 0x20
	.long	100                     # 0x64
	.long	111                     # 0x6f
	.long	99                      # 0x63
	.long	120                     # 0x78
	.long	32                      # 0x20
	.long	120                     # 0x78
	.long	108                     # 0x6c
	.long	115                     # 0x73
	.long	120                     # 0x78
	.long	0                       # 0x0
	.size	.L.str.1, 120

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_ZipRegister.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
