	.text
	.file	"KS-2.bc"
	.globl	CAiBj
	.p2align	4, 0x90
	.type	CAiBj,@function
CAiBj:                                  # @CAiBj
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movq	modules(,%rax,8), %rax
	testq	%rax, %rax
	je	.LBB0_1
# BB#2:                                 # %.lr.ph30.preheader
	movq	8(%rsi), %rcx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph30
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_5 Depth 2
	movq	8(%rax), %rsi
	movq	nets(,%rsi,8), %rdx
	testq	%rdx, %rdx
	je	.LBB0_8
# BB#4:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movss	cost(,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, 8(%rdx)
	movq	(%rdx), %rdx
	jne	.LBB0_7
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=2
	addss	%xmm1, %xmm0
.LBB0_7:                                # %.lr.ph
                                        #   in Loop: Header=BB0_5 Depth=2
	testq	%rdx, %rdx
	jne	.LBB0_5
.LBB0_8:                                # %._crit_edge
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_3
# BB#9:                                 # %._crit_edge31
	retq
.LBB0_1:
	xorps	%xmm0, %xmm0
	retq
.Lfunc_end0:
	.size	CAiBj, .Lfunc_end0-CAiBj
	.cfi_endproc

	.globl	SwapNode
	.p2align	4, 0x90
	.type	SwapNode,@function
SwapNode:                               # @SwapNode
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB1_1
# BB#4:
	cmpq	%rsi, 8(%rdx)
	jne	.LBB1_6
# BB#5:
	movq	%rdi, 8(%rdx)
.LBB1_6:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	jmp	.LBB1_7
.LBB1_1:
	movq	(%rdx), %rax
	cmpq	8(%rdx), %rax
	je	.LBB1_2
# BB#3:
	movq	(%rsi), %rax
	movq	%rax, (%rdx)
	jmp	.LBB1_7
.LBB1_2:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rdx)
.LBB1_7:
	movq	$0, (%rsi)
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.LBB1_8
# BB#9:
	addq	$8, %rcx
	movq	%rsi, (%rax)
	jmp	.LBB1_10
.LBB1_8:
	movq	%rsi, 8(%rcx)
.LBB1_10:
	movq	%rsi, (%rcx)
	movq	$0, (%rsi)
	retq
.Lfunc_end1:
	.size	SwapNode, .Lfunc_end1-SwapNode
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	UpdateDs
	.p2align	4, 0x90
	.type	UpdateDs,@function
UpdateDs:                               # @UpdateDs
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movq	modules(,%rax,8), %r8
	testq	%r8, %r8
	je	.LBB2_10
# BB#1:                                 # %.lr.ph25.preheader
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph25
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_4 Depth 2
	movq	8(%r8), %rcx
	movq	nets(,%rcx,8), %rdx
	testq	%rdx, %rdx
	jne	.LBB2_4
	jmp	.LBB2_9
	.p2align	4, 0x90
.LBB2_8:                                #   in Loop: Header=BB2_4 Depth=2
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB2_9
.LBB2_4:                                # %.lr.ph
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdx), %rdi
	movl	moduleToGroup(,%rdi,4), %eax
	cmpl	$1, %eax
	ja	.LBB2_8
# BB#5:                                 #   in Loop: Header=BB2_4 Depth=2
	cmpl	%esi, %eax
	movss	cost(,%rcx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	je	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_4 Depth=2
	xorps	%xmm0, %xmm1
.LBB2_7:                                #   in Loop: Header=BB2_4 Depth=2
	addss	D(,%rdi,4), %xmm1
	movss	%xmm1, D(,%rdi,4)
	jmp	.LBB2_8
	.p2align	4, 0x90
.LBB2_9:                                # %._crit_edge
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	(%r8), %r8
	testq	%r8, %r8
	jne	.LBB2_2
.LBB2_10:                               # %._crit_edge26
	retq
.Lfunc_end2:
	.size	UpdateDs, .Lfunc_end2-UpdateDs
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	3407386239              # float -9999999
.LCPI3_1:
	.long	3221225472              # float -2
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_2:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	FindMaxGpAndSwap
	.p2align	4, 0x90
	.type	FindMaxGpAndSwap,@function
FindMaxGpAndSwap:                       # @FindMaxGpAndSwap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movq	groupA(%rip), %r8
	movss	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	testq	%r8, %r8
	je	.LBB3_1
# BB#2:                                 # %.preheader.lr.ph
	movq	groupB(%rip), %r9
	testq	%r9, %r9
	je	.LBB3_16
# BB#3:                                 # %.preheader.preheader
	movss	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	xorl	%r14d, %r14d
	movss	.LCPI3_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movq	%r8, %rax
	xorl	%r10d, %r10d
	xorl	%r15d, %r15d
	xorl	%r11d, %r11d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_4:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_6 Depth 2
                                        #       Child Loop BB3_7 Depth 3
                                        #         Child Loop BB3_9 Depth 4
                                        #     Child Loop BB3_20 Depth 2
	movq	%rax, %r13
	movq	8(%r13), %rax
	movss	D(,%rax,4), %xmm2       # xmm2 = mem[0],zero,zero,zero
	movq	modules(,%rax,8), %rsi
	testq	%rsi, %rsi
	je	.LBB3_19
# BB#5:                                 # %.lr.ph.split.preheader
                                        #   in Loop: Header=BB3_4 Depth=1
	movq	%r9, %rax
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_6:                                # %.lr.ph.split
                                        #   Parent Loop BB3_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_7 Depth 3
                                        #         Child Loop BB3_9 Depth 4
	movq	%rbx, %rdx
	movq	%rax, %rbx
	movq	8(%rbx), %rax
	movss	D(,%rax,4), %xmm4       # xmm4 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm3
	movq	%rsi, %rdi
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph30.i
                                        #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_9 Depth 4
	movq	8(%rdi), %rbp
	movq	nets(,%rbp,8), %rcx
	testq	%rcx, %rcx
	je	.LBB3_12
# BB#8:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB3_7 Depth=3
	movss	cost(,%rbp,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB3_9:                                # %.lr.ph.i
                                        #   Parent Loop BB3_4 Depth=1
                                        #     Parent Loop BB3_6 Depth=2
                                        #       Parent Loop BB3_7 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%rax, 8(%rcx)
	movq	(%rcx), %rcx
	jne	.LBB3_11
# BB#10:                                #   in Loop: Header=BB3_9 Depth=4
	addss	%xmm5, %xmm3
.LBB3_11:                               # %.lr.ph.i
                                        #   in Loop: Header=BB3_9 Depth=4
	testq	%rcx, %rcx
	jne	.LBB3_9
.LBB3_12:                               # %._crit_edge.i
                                        #   in Loop: Header=BB3_7 Depth=3
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB3_7
# BB#13:                                # %CAiBj.exit
                                        #   in Loop: Header=BB3_6 Depth=2
	addss	%xmm2, %xmm4
	mulss	%xmm1, %xmm3
	addss	%xmm4, %xmm3
	ucomiss	%xmm0, %xmm3
	jbe	.LBB3_15
# BB#14:                                #   in Loop: Header=BB3_6 Depth=2
	movq	%r13, %r11
	movq	%r12, %r15
	movq	%rbx, %r10
	movq	%rdx, %r14
	movaps	%xmm3, %xmm0
.LBB3_15:                               #   in Loop: Header=BB3_6 Depth=2
	movq	(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB3_6
	jmp	.LBB3_23
	.p2align	4, 0x90
.LBB3_19:                               # %.lr.ph.split.us.preheader
                                        #   in Loop: Header=BB3_4 Depth=1
	movq	%r9, %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_20:                               # %.lr.ph.split.us
                                        #   Parent Loop BB3_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %rax
	movq	8(%rax), %rcx
	movss	D(,%rcx,4), %xmm3       # xmm3 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm3
	ucomiss	%xmm0, %xmm3
	jbe	.LBB3_22
# BB#21:                                #   in Loop: Header=BB3_20 Depth=2
	movq	%r13, %r11
	movq	%r12, %r15
	movq	%rax, %r10
	movq	%rdx, %r14
	movaps	%xmm3, %xmm0
.LBB3_22:                               #   in Loop: Header=BB3_20 Depth=2
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	movq	%rax, %rdx
	jne	.LBB3_20
.LBB3_23:                               # %._crit_edge
                                        #   in Loop: Header=BB3_4 Depth=1
	movq	(%r13), %rax
	testq	%rax, %rax
	movq	%r13, %r12
	jne	.LBB3_4
# BB#24:                                # %._crit_edge101
	testq	%r15, %r15
	je	.LBB3_25
# BB#28:
	cmpq	%r11, groupA+8(%rip)
	jne	.LBB3_30
# BB#29:
	movq	%r15, groupA+8(%rip)
.LBB3_30:
	movq	(%r11), %rax
	movq	%rax, (%r15)
	jmp	.LBB3_31
.LBB3_1:
	xorl	%r14d, %r14d
	jmp	.LBB3_18
.LBB3_16:                               # %.preheader.us.preheader
	xorl	%r14d, %r14d
	movq	%r8, %rax
	.p2align	4, 0x90
.LBB3_17:                               # %.preheader.us
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB3_17
.LBB3_18:
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
.LBB3_25:                               # %._crit_edge101.thread
	cmpq	groupA+8(%rip), %r8
	je	.LBB3_26
# BB#27:
	movq	(%r11), %rax
	movq	%rax, groupA(%rip)
	jmp	.LBB3_31
.LBB3_26:
	xorps	%xmm1, %xmm1
	movups	%xmm1, groupA(%rip)
.LBB3_31:                               # %SwapNode.exit79
	movl	$swapToB, %eax
	movq	swapToB+8(%rip), %rcx
	testq	%rcx, %rcx
	movl	$swapToB+8, %edx
	cmoveq	%rdx, %rcx
	cmovneq	%rdx, %rax
	movq	%r11, (%rcx)
	movq	%r11, (%rax)
	movq	$0, (%r11)
	testq	%r14, %r14
	je	.LBB3_32
# BB#35:
	cmpq	%r10, groupB+8(%rip)
	jne	.LBB3_37
# BB#36:
	movq	%r14, groupB+8(%rip)
.LBB3_37:
	movq	(%r10), %rax
	movq	%rax, (%r14)
	jmp	.LBB3_38
.LBB3_32:
	movq	groupB(%rip), %rax
	cmpq	groupB+8(%rip), %rax
	je	.LBB3_33
# BB#34:
	movq	(%r10), %rax
	movq	%rax, groupB(%rip)
	jmp	.LBB3_38
.LBB3_33:
	xorps	%xmm1, %xmm1
	movups	%xmm1, groupB(%rip)
.LBB3_38:                               # %SwapNode.exit
	movl	$swapToA, %eax
	movq	swapToA+8(%rip), %rcx
	testq	%rcx, %rcx
	movl	$swapToA+8, %edx
	cmoveq	%rdx, %rcx
	cmovneq	%rdx, %rax
	movq	%r10, (%rcx)
	movq	%r10, (%rax)
	movq	$0, (%r10)
	movq	8(%r11), %rax
	movl	$3, moduleToGroup(,%rax,4)
	movq	8(%r10), %rax
	movl	$2, moduleToGroup(,%rax,4)
	movq	8(%r11), %rax
	movq	modules(,%rax,8), %rax
	testq	%rax, %rax
	je	.LBB3_48
# BB#39:                                # %.lr.ph25.i67.preheader
	movaps	.LCPI3_2(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB3_40:                               # %.lr.ph25.i67
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_42 Depth 2
	movq	8(%rax), %rcx
	movq	nets(,%rcx,8), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_42
	jmp	.LBB3_47
	.p2align	4, 0x90
.LBB3_46:                               #   in Loop: Header=BB3_42 Depth=2
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB3_47
.LBB3_42:                               # %.lr.ph.i70
                                        #   Parent Loop BB3_40 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdx), %rsi
	movl	moduleToGroup(,%rsi,4), %edi
	cmpl	$1, %edi
	ja	.LBB3_46
# BB#43:                                #   in Loop: Header=BB3_42 Depth=2
	testl	%edi, %edi
	movss	cost(,%rcx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	je	.LBB3_45
# BB#44:                                #   in Loop: Header=BB3_42 Depth=2
	xorps	%xmm1, %xmm2
.LBB3_45:                               #   in Loop: Header=BB3_42 Depth=2
	addss	D(,%rsi,4), %xmm2
	movss	%xmm2, D(,%rsi,4)
	jmp	.LBB3_46
	.p2align	4, 0x90
.LBB3_47:                               # %._crit_edge.i75
                                        #   in Loop: Header=BB3_40 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB3_40
.LBB3_48:                               # %UpdateDs.exit76
	movq	8(%r10), %rax
	movq	modules(,%rax,8), %rax
	testq	%rax, %rax
	je	.LBB3_58
# BB#49:                                # %.lr.ph25.i.preheader
	movaps	.LCPI3_2(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB3_50:                               # %.lr.ph25.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_52 Depth 2
	movq	8(%rax), %rcx
	movq	nets(,%rcx,8), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_52
	jmp	.LBB3_57
	.p2align	4, 0x90
.LBB3_56:                               #   in Loop: Header=BB3_52 Depth=2
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB3_57
.LBB3_52:                               # %.lr.ph.i61
                                        #   Parent Loop BB3_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdx), %rsi
	cmpl	$1, moduleToGroup(,%rsi,4)
	ja	.LBB3_56
# BB#53:                                #   in Loop: Header=BB3_52 Depth=2
	movss	cost(,%rcx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	je	.LBB3_55
# BB#54:                                #   in Loop: Header=BB3_52 Depth=2
	xorps	%xmm1, %xmm2
.LBB3_55:                               #   in Loop: Header=BB3_52 Depth=2
	addss	D(,%rsi,4), %xmm2
	movss	%xmm2, D(,%rsi,4)
	jmp	.LBB3_56
	.p2align	4, 0x90
.LBB3_57:                               # %._crit_edge.i63
                                        #   in Loop: Header=BB3_50 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB3_50
.LBB3_58:                               # %UpdateDs.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	FindMaxGpAndSwap, .Lfunc_end3-FindMaxGpAndSwap
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	3407386239              # float -9999999
	.text
	.globl	FindGMax
	.p2align	4, 0x90
	.type	FindGMax,@function
FindGMax:                               # @FindGMax
	.cfi_startproc
# BB#0:
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, (%rdi)
	movq	numModules(%rip), %rax
	movss	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	cmpq	$2, %rax
	jb	.LBB4_5
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	GP(,%rcx,4), %xmm1      # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	%rcx, (%rdi)
	movq	numModules(%rip), %rax
	movaps	%xmm1, %xmm0
.LBB4_4:                                #   in Loop: Header=BB4_2 Depth=1
	incq	%rcx
	movq	%rax, %rdx
	shrq	%rdx
	cmpq	%rdx, %rcx
	jb	.LBB4_2
.LBB4_5:                                # %._crit_edge
	retq
.Lfunc_end4:
	.size	FindGMax, .Lfunc_end4-FindGMax
	.cfi_endproc

	.globl	SwapSubsetAndReset
	.p2align	4, 0x90
	.type	SwapSubsetAndReset,@function
SwapSubsetAndReset:                     # @SwapSubsetAndReset
	.cfi_startproc
# BB#0:
	movq	swapToB(%rip), %r8
	movq	swapToA(%rip), %r9
	xorl	%eax, %eax
	movq	%r9, %r10
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rcx
	movq	%r10, %rdx
	incq	%rax
	movq	(%rcx), %rsi
	movq	(%rdx), %r10
	cmpq	%rdi, %rax
	jbe	.LBB5_1
# BB#2:
	testq	%r10, %r10
	je	.LBB5_3
# BB#4:
	movq	%rsi, (%rdx)
	movq	%r9, groupA(%rip)
	movq	swapToB+8(%rip), %rax
	movq	%rax, groupA+8(%rip)
	movq	%r10, (%rcx)
	movq	%r8, groupB(%rip)
	movq	swapToA+8(%rip), %rax
	movq	%rax, groupB+8(%rip)
	jmp	.LBB5_5
.LBB5_3:
	movups	swapToA(%rip), %xmm0
	movups	%xmm0, groupA(%rip)
	movups	swapToB(%rip), %xmm0
	movups	%xmm0, groupB(%rip)
.LBB5_5:                                # %.preheader30
	movq	groupA(%rip), %rax
	testq	%rax, %rax
	je	.LBB5_7
	.p2align	4, 0x90
.LBB5_11:                               # %.lr.ph35
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movl	$0, moduleToGroup(,%rcx,4)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB5_11
.LBB5_7:                                # %.preheader
	movq	groupB(%rip), %rax
	testq	%rax, %rax
	je	.LBB5_10
	.p2align	4, 0x90
.LBB5_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movl	$1, moduleToGroup(,%rcx,4)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB5_8
.LBB5_10:                               # %._crit_edge
	xorps	%xmm0, %xmm0
	movups	%xmm0, swapToA(%rip)
	movups	%xmm0, swapToB(%rip)
	retq
.Lfunc_end5:
	.size	SwapSubsetAndReset, .Lfunc_end5-SwapSubsetAndReset
	.cfi_endproc

	.globl	PrintResults
	.p2align	4, 0x90
	.type	PrintResults,@function
PrintResults:                           # @PrintResults
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 64
.Lcfi19:
	.cfi_offset %rbx, -56
.Lcfi20:
	.cfi_offset %r12, -48
.Lcfi21:
	.cfi_offset %r13, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movq	stdout(%rip), %rcx
	movl	$.L.str, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	$-1, maxStat(%rip)
	movl	$netStats, %edi
	xorl	%esi, %esi
	movl	$6144, %edx             # imm = 0x1800
	callq	memset
	testl	%r14d, %r14d
	je	.LBB6_6
# BB#1:
	movq	stdout(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$11, %esi
	movl	$1, %edx
	callq	fwrite
	movq	groupA(%rip), %rbx
	testq	%rbx, %rbx
	movq	stdout(%rip), %rcx
	je	.LBB6_3
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph142
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdx
	incq	%rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	fprintf
	movq	(%rbx), %rbx
	movq	stdout(%rip), %rcx
	testq	%rbx, %rbx
	jne	.LBB6_2
.LBB6_3:                                # %._crit_edge143
	movl	$10, %edi
	movq	%rcx, %rsi
	callq	fputc
	movq	stdout(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$11, %esi
	movl	$1, %edx
	callq	fwrite
	movq	groupB(%rip), %rbx
	testq	%rbx, %rbx
	movq	stdout(%rip), %rcx
	je	.LBB6_5
	.p2align	4, 0x90
.LBB6_4:                                # %.lr.ph136
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdx
	incq	%rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	fprintf
	movq	(%rbx), %rbx
	movq	stdout(%rip), %rcx
	testq	%rbx, %rbx
	jne	.LBB6_4
.LBB6_5:                                # %._crit_edge137
	movl	$10, %edi
	movq	%rcx, %rsi
	callq	fputc
.LBB6_6:                                # %.preheader
	movq	groupA(%rip), %r15
	xorl	%r13d, %r13d
	testq	%r15, %r15
	jne	.LBB6_8
	jmp	.LBB6_29
	.p2align	4, 0x90
.LBB6_19:                               # %.lr.ph117.split
                                        #   Parent Loop BB6_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_21 Depth 3
                                        #       Child Loop BB6_24 Depth 3
	movq	8(%r12), %rcx
	movq	nets(,%rcx,8), %rcx
	testq	%rcx, %rcx
	je	.LBB6_27
# BB#20:                                # %.lr.ph103.preheader
                                        #   in Loop: Header=BB6_19 Depth=2
	xorl	%edx, %edx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB6_21:                               # %.lr.ph103
                                        #   Parent Loop BB6_8 Depth=1
                                        #     Parent Loop BB6_19 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%edx
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB6_21
# BB#22:                                # %._crit_edge104
                                        #   in Loop: Header=BB6_19 Depth=2
	testq	%rcx, %rcx
	je	.LBB6_27
# BB#23:                                # %.lr.ph110
                                        #   in Loop: Header=BB6_19 Depth=2
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	netStats+8(,%rdx,8), %rdx
	movl	moduleToGroup(,%rax,4), %esi
	.p2align	4, 0x90
.LBB6_24:                               #   Parent Loop BB6_8 Depth=1
                                        #     Parent Loop BB6_19 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rcx), %rdi
	cmpl	moduleToGroup(,%rdi,4), %esi
	je	.LBB6_26
# BB#25:                                #   in Loop: Header=BB6_24 Depth=3
	incq	(%rdx)
	incq	%r13
.LBB6_26:                               #   in Loop: Header=BB6_24 Depth=3
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB6_24
.LBB6_27:                               # %._crit_edge111
                                        #   in Loop: Header=BB6_19 Depth=2
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB6_19
.LBB6_28:                               # %._crit_edge118
                                        #   in Loop: Header=BB6_8 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.LBB6_29
.LBB6_8:                                # %.lr.ph130
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_19 Depth 2
                                        #       Child Loop BB6_21 Depth 3
                                        #       Child Loop BB6_24 Depth 3
                                        #     Child Loop BB6_10 Depth 2
                                        #       Child Loop BB6_12 Depth 3
                                        #       Child Loop BB6_16 Depth 3
	movq	8(%r15), %rax
	movq	modules(,%rax,8), %r12
	testq	%r12, %r12
	je	.LBB6_28
# BB#9:                                 # %.lr.ph117
                                        #   in Loop: Header=BB6_8 Depth=1
	testl	%r14d, %r14d
	je	.LBB6_19
	.p2align	4, 0x90
.LBB6_10:                               # %.lr.ph117.split.us
                                        #   Parent Loop BB6_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_12 Depth 3
                                        #       Child Loop BB6_16 Depth 3
	movq	8(%r12), %rax
	movq	nets(,%rax,8), %rbp
	testq	%rbp, %rbp
	je	.LBB6_14
# BB#11:                                # %.lr.ph103.us.preheader
                                        #   in Loop: Header=BB6_10 Depth=2
	xorl	%eax, %eax
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB6_12:                               # %.lr.ph103.us
                                        #   Parent Loop BB6_8 Depth=1
                                        #     Parent Loop BB6_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%eax
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB6_12
# BB#13:                                # %._crit_edge104.us
                                        #   in Loop: Header=BB6_10 Depth=2
	testq	%rbp, %rbp
	je	.LBB6_14
# BB#15:                                # %.lr.ph110.us
                                        #   in Loop: Header=BB6_10 Depth=2
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	netStats+8(,%rax,8), %rbx
	.p2align	4, 0x90
.LBB6_16:                               #   Parent Loop BB6_8 Depth=1
                                        #     Parent Loop BB6_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%r15), %rdx
	movl	moduleToGroup(,%rdx,4), %eax
	movq	8(%rbp), %rcx
	cmpl	moduleToGroup(,%rcx,4), %eax
	je	.LBB6_18
# BB#17:                                #   in Loop: Header=BB6_16 Depth=3
	movq	stdout(%rip), %rdi
	incq	%rdx
	incq	%rcx
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	callq	fprintf
	incq	(%rbx)
	incq	%r13
.LBB6_18:                               #   in Loop: Header=BB6_16 Depth=3
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB6_16
.LBB6_14:                               # %._crit_edge111.us-lcssa.us.us
                                        #   in Loop: Header=BB6_10 Depth=2
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB6_10
	jmp	.LBB6_28
.LBB6_29:                               # %._crit_edge131
	movq	stdout(%rip), %rdi
	xorl	%r15d, %r15d
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r13, %rdx
	callq	fprintf
	movq	numNets(%rip), %rax
	testq	%rax, %rax
	je	.LBB6_54
# BB#30:                                # %.lr.ph96
	testl	%r14d, %r14d
	je	.LBB6_31
# BB#42:                                # %.lr.ph96.split.us.preheader
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB6_43:                               # %.lr.ph96.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_45 Depth 2
                                        #     Child Loop BB6_49 Depth 2
	movq	nets(,%rbx,8), %rcx
	testq	%rcx, %rcx
	movl	$0, %edx
	je	.LBB6_46
# BB#44:                                # %.lr.ph91.us.preheader
                                        #   in Loop: Header=BB6_43 Depth=1
	xorl	%edx, %edx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB6_45:                               # %.lr.ph91.us
                                        #   Parent Loop BB6_43 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%edx
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB6_45
.LBB6_46:                               # %._crit_edge92.us
                                        #   in Loop: Header=BB6_43 Depth=1
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx,2), %rbp
	incq	netStats(,%rbp,8)
	cmpq	maxStat(%rip), %rdx
	jle	.LBB6_48
# BB#47:                                #   in Loop: Header=BB6_43 Depth=1
	movq	%rdx, maxStat(%rip)
.LBB6_48:                               #   in Loop: Header=BB6_43 Depth=1
	movq	8(%rcx), %rdx
	movl	moduleToGroup(,%rdx,4), %edx
	.p2align	4, 0x90
.LBB6_49:                               #   Parent Loop BB6_43 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB6_52
# BB#50:                                #   in Loop: Header=BB6_49 Depth=2
	movq	8(%rcx), %rsi
	cmpl	moduleToGroup(,%rsi,4), %edx
	je	.LBB6_49
# BB#51:                                #   in Loop: Header=BB6_43 Depth=1
	movq	stdout(%rip), %rdi
	incq	%rbx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	incq	%r15
	incq	netStats+16(,%rbp,8)
	movq	numNets(%rip), %rax
	cmpq	%rax, %rbx
	jb	.LBB6_43
	jmp	.LBB6_54
	.p2align	4, 0x90
.LBB6_52:                               # %.loopexit.us.loopexit
                                        #   in Loop: Header=BB6_43 Depth=1
	incq	%rbx
	cmpq	%rax, %rbx
	jb	.LBB6_43
	jmp	.LBB6_54
.LBB6_31:                               # %.lr.ph96.split.preheader
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB6_32:                               # %.lr.ph96.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_34 Depth 2
                                        #     Child Loop BB6_38 Depth 2
	movq	nets(,%rcx,8), %rdx
	testq	%rdx, %rdx
	movl	$0, %esi
	je	.LBB6_35
# BB#33:                                # %.lr.ph91.preheader
                                        #   in Loop: Header=BB6_32 Depth=1
	xorl	%esi, %esi
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB6_34:                               # %.lr.ph91
                                        #   Parent Loop BB6_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%esi
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB6_34
.LBB6_35:                               # %._crit_edge92
                                        #   in Loop: Header=BB6_32 Depth=1
	movslq	%esi, %rdi
	leaq	(%rdi,%rdi,2), %rsi
	incq	netStats(,%rsi,8)
	cmpq	maxStat(%rip), %rdi
	jle	.LBB6_37
# BB#36:                                #   in Loop: Header=BB6_32 Depth=1
	movq	%rdi, maxStat(%rip)
.LBB6_37:                               #   in Loop: Header=BB6_32 Depth=1
	movq	8(%rdx), %rdi
	movl	moduleToGroup(,%rdi,4), %edi
	.p2align	4, 0x90
.LBB6_38:                               #   Parent Loop BB6_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB6_41
# BB#39:                                #   in Loop: Header=BB6_38 Depth=2
	movq	8(%rdx), %rbp
	cmpl	moduleToGroup(,%rbp,4), %edi
	je	.LBB6_38
# BB#40:                                #   in Loop: Header=BB6_32 Depth=1
	incq	%r15
	incq	netStats+16(,%rsi,8)
.LBB6_41:                               # %.loopexit
                                        #   in Loop: Header=BB6_32 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB6_32
.LBB6_54:                               # %._crit_edge97
	movq	stdout(%rip), %rdi
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	callq	fprintf
	cmpq	$2, maxStat(%rip)
	jl	.LBB6_57
# BB#55:                                # %.lr.ph.preheader
	movl	$2, %ebx
	movl	$netStats+64, %ebp
	.p2align	4, 0x90
.LBB6_56:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rdi
	movq	-16(%rbp), %rcx
	movq	-8(%rbp), %r8
	movq	(%rbp), %r9
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	incq	%rbx
	addq	$24, %rbp
	cmpq	maxStat(%rip), %rbx
	jle	.LBB6_56
.LBB6_57:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	PrintResults, .Lfunc_end6-PrintResults
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI7_0:
	.long	3407386239              # float -9999999
.LCPI7_1:
	.long	0                       # float 0
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -16
	cmpl	$2, %edi
	jne	.LBB7_31
# BB#1:
	movq	8(%rsi), %rdi
	callq	ReadNetList
	callq	NetsToModules
	callq	ComputeNetCosts
	callq	InitLists
	xorps	%xmm0, %xmm0
	jmp	.LBB7_2
	.p2align	4, 0x90
.LBB7_23:                               # %SwapSubsetAndReset.exit
                                        #   in Loop: Header=BB7_2 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, swapToA(%rip)
	movups	%xmm0, swapToB(%rip)
	xorl	%edi, %edi
	callq	PrintResults
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
.LBB7_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_4 Depth 2
                                        #     Child Loop BB7_7 Depth 2
                                        #     Child Loop BB7_13 Depth 2
                                        #     Child Loop BB7_20 Depth 2
                                        #     Child Loop BB7_21 Depth 2
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movl	$groupA, %edi
	xorl	%esi, %esi
	movl	$2, %edx
	callq	ComputeDs
	movl	$groupB, %edi
	movl	$1, %esi
	movl	$3, %edx
	callq	ComputeDs
	cmpq	$2, numModules(%rip)
	movl	$4294967295, %ebx       # imm = 0xFFFFFFFF
	movss	.LCPI7_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	jb	.LBB7_8
# BB#3:                                 # %.lr.ph37.preheader
                                        #   in Loop: Header=BB7_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_4:                                # %.lr.ph37
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	FindMaxGpAndSwap
	movss	%xmm0, GP(,%rbx,4)
	incq	%rbx
	movq	numModules(%rip), %rax
	movq	%rax, %rcx
	shrq	%rcx
	cmpq	%rcx, %rbx
	jb	.LBB7_4
# BB#5:                                 # %._crit_edge38
                                        #   in Loop: Header=BB7_2 Depth=1
	cmpq	$2, %rax
	movl	$4294967295, %ebx       # imm = 0xFFFFFFFF
	movss	.LCPI7_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	jb	.LBB7_8
# BB#6:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB7_2 Depth=1
	movl	$4294967295, %ebx       # imm = 0xFFFFFFFF
	movq	%rax, %rcx
	xorl	%edx, %edx
	movss	.LCPI7_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB7_7:                                # %.lr.ph.i
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	GP(,%rdx,4), %xmm1      # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	cmovaq	%rdx, %rbx
	cmovaq	%rax, %rcx
	maxss	%xmm0, %xmm1
	incq	%rdx
	movq	%rcx, %rsi
	shrq	%rsi
	cmpq	%rsi, %rdx
	movaps	%xmm1, %xmm0
	jb	.LBB7_7
.LBB7_8:                                # %FindGMax.exit
                                        #   in Loop: Header=BB7_2 Depth=1
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	movss	%xmm1, 4(%rsp)          # 4-byte Spill
	jne	.LBB7_9
	jp	.LBB7_9
# BB#10:                                #   in Loop: Header=BB7_2 Depth=1
	movq	stdout(%rip), %rdi
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	$.L.str.11, %esi
	movb	$1, %al
	callq	fprintf
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	jmp	.LBB7_11
	.p2align	4, 0x90
.LBB7_9:                                # %FindGMax.exit._crit_edge
                                        #   in Loop: Header=BB7_2 Depth=1
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
.LBB7_11:                               #   in Loop: Header=BB7_2 Depth=1
	movq	stdout(%rip), %rdi
	movl	$.L.str.12, %esi
	movb	$1, %al
	movq	%rbx, %rdx
	callq	fprintf
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	ucomiss	.LCPI7_1, %xmm0
	jbe	.LBB7_24
# BB#12:                                #   in Loop: Header=BB7_2 Depth=1
	movq	swapToB(%rip), %r8
	movq	swapToA(%rip), %r9
	movq	%r9, %rax
	movq	%r8, %rdi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_13:                               #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdi, %rsi
	movq	%rax, %rdx
	incq	%rcx
	movq	(%rsi), %rdi
	movq	(%rdx), %rax
	cmpq	%rbx, %rcx
	jbe	.LBB7_13
# BB#14:                                #   in Loop: Header=BB7_2 Depth=1
	testq	%rax, %rax
	je	.LBB7_15
# BB#16:                                #   in Loop: Header=BB7_2 Depth=1
	movq	%rdi, (%rdx)
	movq	%r9, groupA(%rip)
	movq	swapToB+8(%rip), %rcx
	movq	%rcx, groupA+8(%rip)
	movq	%rax, (%rsi)
	movq	%r8, groupB(%rip)
	movq	swapToA+8(%rip), %rax
	movq	%rax, groupB+8(%rip)
	jmp	.LBB7_17
	.p2align	4, 0x90
.LBB7_15:                               #   in Loop: Header=BB7_2 Depth=1
	movups	swapToA(%rip), %xmm0
	movups	%xmm0, groupA(%rip)
	movups	swapToB(%rip), %xmm0
	movups	%xmm0, groupB(%rip)
.LBB7_17:                               # %.preheader30.i
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	groupA(%rip), %rax
	testq	%rax, %rax
	je	.LBB7_19
	.p2align	4, 0x90
.LBB7_20:                               # %.lr.ph35.i
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rax), %rcx
	movl	$0, moduleToGroup(,%rcx,4)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB7_20
.LBB7_19:                               # %.preheader.i
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	groupB(%rip), %rax
	testq	%rax, %rax
	je	.LBB7_23
	.p2align	4, 0x90
.LBB7_21:                               # %.lr.ph.i23
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rax), %rcx
	movl	$1, moduleToGroup(,%rcx,4)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB7_21
	jmp	.LBB7_23
.LBB7_24:                               # %.critedge
	xorl	%edi, %edi
	callq	PrintResults
	movups	swapToB(%rip), %xmm0
	movups	%xmm0, groupA(%rip)
	movq	groupA(%rip), %rax
	testq	%rax, %rax
	je	.LBB7_27
	.p2align	4, 0x90
.LBB7_25:                               # %.lr.ph33
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movl	$0, moduleToGroup(,%rcx,4)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB7_25
.LBB7_27:                               # %._crit_edge34
	movups	swapToA(%rip), %xmm0
	movups	%xmm0, groupB(%rip)
	movq	groupB(%rip), %rax
	testq	%rax, %rax
	je	.LBB7_30
	.p2align	4, 0x90
.LBB7_28:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movl	$1, moduleToGroup(,%rcx,4)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB7_28
.LBB7_30:                               # %._crit_edge
	movl	$1, %edi
	callq	PrintResults
	xorl	%edi, %edi
	callq	exit
.LBB7_31:
	movq	stderr(%rip), %rcx
	movl	$.L.str.10, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end7:
	.size	main, .Lfunc_end7-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"----------------------------------------------\n"
	.size	.L.str, 48

	.type	maxStat,@object         # @maxStat
	.comm	maxStat,8,8
	.type	netStats,@object        # @netStats
	.comm	netStats,6144,16
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Group A:  \n"
	.size	.L.str.1, 12

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%3lu "
	.size	.L.str.2, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Group B:  \n"
	.size	.L.str.4, 12

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Conn %3lu - %3lu cut.\n"
	.size	.L.str.5, 23

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Total edge cuts = %lu\n"
	.size	.L.str.6, 23

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Net %3lu cut.\n"
	.size	.L.str.7, 15

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Total net cuts  = %lu\n"
	.size	.L.str.8, 23

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"sz:%5lu     total:%5lu     edgesCut:%5lu     netsCuts:%5lu\n"
	.size	.L.str.9, 60

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Usage: KL <input_file>\n"
	.size	.L.str.10, 24

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"No progress: gMax = %f\n"
	.size	.L.str.11, 24

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"gMax = %f, iMax = %lu\n"
	.size	.L.str.12, 23


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
