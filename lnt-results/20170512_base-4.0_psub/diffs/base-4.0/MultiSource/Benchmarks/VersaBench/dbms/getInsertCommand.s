	.text
	.file	"getInsertCommand.bc"
	.globl	getInsertCommand
	.p2align	4, 0x90
	.type	getInsertCommand,@function
getInsertCommand:                       # @getInsertCommand
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	leaq	16(%rsp), %rsi
	callq	getInt
	cmpq	$3, %rax
	je	.LBB0_6
# BB#1:
	cmpq	$2, %rax
	je	.LBB0_4
# BB#2:
	cmpq	$1, %rax
	jne	.LBB0_7
# BB#3:
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getInsertCommand.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$2, %ebx
	jmp	.LBB0_25
.LBB0_6:
	movl	$.L.str.2, %edi
	jmp	.LBB0_5
.LBB0_4:
	movl	$.L.str.1, %edi
.LBB0_5:                                # %.loopexit
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getInsertCommand.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$1, %ebx
	jmp	.LBB0_25
.LBB0_7:
	movq	16(%rsp), %rax
	leaq	-1(%rax), %rdi
	cmpq	$3, %rdi
	jae	.LBB0_8
# BB#9:                                 # %switch.lookup
	incl	%edi
	movq	.Lswitch.table-8(,%rax,8), %r13
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	createDataObject
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.LBB0_15
# BB#10:                                # %.preheader54
	leaq	8(%rsp), %rsi
	movq	%r15, %rdi
	callq	getKeyAttribute
	testq	%rax, %rax
	je	.LBB0_16
# BB#11:                                # %.preheader54
	xorl	%r12d, %r12d
	cmpq	$1, %rax
	je	.LBB0_19
# BB#12:                                # %.preheader54
	cmpq	$2, %rax
	jne	.LBB0_17
	jmp	.LBB0_13
.LBB0_8:
	movl	$.L.str.1, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getInsertCommand.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$3, %ebx
	jmp	.LBB0_25
.LBB0_15:
	movl	$.L.str.3, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getInsertCommand.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$4, %ebx
	jmp	.LBB0_25
.LBB0_16:
	movl	8(%rsp), %eax
	movq	(%r14), %rcx
	movq	8(%rcx), %rcx
	movl	%eax, (%rcx)
.LBB0_17:
	leaq	8(%rsp), %rsi
	movq	%r15, %rdi
	callq	getKeyAttribute
	testq	%rax, %rax
	je	.LBB0_26
# BB#18:
	movl	$1, %r12d
	cmpq	$1, %rax
	je	.LBB0_19
# BB#20:
	cmpq	$2, %rax
	jne	.LBB0_27
	jmp	.LBB0_13
.LBB0_26:
	movl	8(%rsp), %eax
	movq	(%r14), %rcx
	movq	8(%rcx), %rcx
	movl	%eax, 8(%rcx)
.LBB0_27:
	leaq	8(%rsp), %rsi
	movq	%r15, %rdi
	callq	getKeyAttribute
	movl	$2, %r12d
	cmpq	$2, %rax
	je	.LBB0_13
# BB#28:
	cmpq	$1, %rax
	je	.LBB0_19
# BB#29:
	testq	%rax, %rax
	jne	.LBB0_31
# BB#30:
	movl	8(%rsp), %eax
	movq	(%r14), %rcx
	movq	8(%rcx), %rcx
	movl	%eax, 16(%rcx)
.LBB0_31:
	leaq	8(%rsp), %rsi
	movq	%r15, %rdi
	callq	getKeyAttribute
	movl	$3, %r12d
	cmpq	$2, %rax
	je	.LBB0_13
# BB#32:
	cmpq	$1, %rax
	je	.LBB0_19
# BB#33:
	testq	%rax, %rax
	jne	.LBB0_35
# BB#34:
	movl	8(%rsp), %eax
	movq	(%r14), %rcx
	movq	8(%rcx), %rcx
	movl	%eax, 24(%rcx)
.LBB0_35:
	leaq	8(%rsp), %rsi
	movq	%r15, %rdi
	callq	getKeyAttribute
	movl	$4, %r12d
	cmpq	$2, %rax
	je	.LBB0_13
# BB#36:
	cmpq	$1, %rax
	je	.LBB0_19
# BB#37:
	testq	%rax, %rax
	jne	.LBB0_39
# BB#38:
	movl	8(%rsp), %eax
	movq	(%r14), %rcx
	movq	8(%rcx), %rcx
	movl	%eax, 32(%rcx)
.LBB0_39:
	leaq	8(%rsp), %rsi
	movq	%r15, %rdi
	callq	getKeyAttribute
	movl	$5, %r12d
	cmpq	$2, %rax
	je	.LBB0_13
# BB#40:
	cmpq	$1, %rax
	je	.LBB0_19
# BB#41:
	testq	%rax, %rax
	jne	.LBB0_43
# BB#42:
	movl	8(%rsp), %eax
	movq	(%r14), %rcx
	movq	8(%rcx), %rcx
	movl	%eax, 40(%rcx)
.LBB0_43:
	leaq	8(%rsp), %rsi
	movq	%r15, %rdi
	callq	getKeyAttribute
	movl	$6, %r12d
	cmpq	$2, %rax
	je	.LBB0_13
# BB#44:
	cmpq	$1, %rax
	je	.LBB0_19
# BB#45:
	testq	%rax, %rax
	jne	.LBB0_47
# BB#46:
	movl	8(%rsp), %eax
	movq	(%r14), %rcx
	movq	8(%rcx), %rcx
	movl	%eax, 48(%rcx)
.LBB0_47:
	leaq	8(%rsp), %rsi
	movq	%r15, %rdi
	callq	getKeyAttribute
	movl	$7, %r12d
	cmpq	$2, %rax
	je	.LBB0_13
# BB#48:
	cmpq	$1, %rax
	je	.LBB0_19
# BB#49:
	testq	%rax, %rax
	jne	.LBB0_51
# BB#50:
	movl	8(%rsp), %eax
	movq	(%r14), %rcx
	movq	8(%rcx), %rcx
	movl	%eax, 56(%rcx)
.LBB0_51:                               # %.lr.ph
	movl	$8, %ebp
	leaq	8(%rsp), %r12
	xorl	%ebx, %ebx
.LBB0_52:                               # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	getNonKeyAttribute
	testq	%rax, %rax
	je	.LBB0_23
# BB#53:                                #   in Loop: Header=BB0_52 Depth=1
	cmpq	$1, %rax
	je	.LBB0_21
# BB#54:                                #   in Loop: Header=BB0_52 Depth=1
	cmpq	$2, %rax
	jne	.LBB0_24
	jmp	.LBB0_55
.LBB0_23:                               #   in Loop: Header=BB0_52 Depth=1
	movq	8(%rsp), %rax
	movq	(%r14), %rcx
	movq	8(%rcx), %rcx
	movq	%rax, (%rcx,%rbp,8)
.LBB0_24:                               #   in Loop: Header=BB0_52 Depth=1
	incq	%rbp
	cmpq	%r13, %rbp
	jl	.LBB0_52
	jmp	.LBB0_25
.LBB0_13:
	movl	$.L.str.4, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getInsertCommand.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$1, %ebx
	jmp	.LBB0_14
.LBB0_19:
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getInsertCommand.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$2, %ebx
.LBB0_14:
	movq	(%r14), %rax
	movq	8(%rax), %rax
	movl	$-8388609, (%rax,%r12,8) # imm = 0xFF7FFFFF
.LBB0_25:                               # %.loopexit
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_55:
	movl	$.L.str, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getInsertCommand.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$2, %ebx
	jmp	.LBB0_22
.LBB0_21:
	movl	$.L.str.5, %edi
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getInsertCommand.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$4, %ebx
.LBB0_22:
	movq	(%r14), %rax
	movq	8(%rax), %rax
	movq	$0, (%rax,%rbp,8)
	jmp	.LBB0_25
.Lfunc_end0:
	.size	getInsertCommand, .Lfunc_end0-getInsertCommand
	.cfi_endproc

	.type	getInsertCommand.name,@object # @getInsertCommand.name
	.data
	.p2align	4
getInsertCommand.name:
	.asciz	"getInsertCommand"
	.size	getInsertCommand.name, 17

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"improper format - early EOI"
	.size	.L.str, 28

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"unknown data object type"
	.size	.L.str.1, 25

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"improper format - type must be an integer"
	.size	.L.str.2, 42

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"allocation failure"
	.size	.L.str.3, 19

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"low-level I/O error"
	.size	.L.str.4, 20

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"allocation failure for non-key attribute"
	.size	.L.str.5, 41

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.quad	18                      # 0x12
	.quad	25                      # 0x19
	.quad	51                      # 0x33
	.size	.Lswitch.table, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
