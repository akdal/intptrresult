	.text
	.file	"me_distortion.bc"
	.globl	distortion4x4
	.p2align	4, 0x90
	.type	distortion4x4,@function
distortion4x4:                          # @distortion4x4
	.cfi_startproc
# BB#0:
	movq	input(%rip), %rax
	movl	5792(%rax), %eax
	cmpl	$1, %eax
	je	.LBB0_4
# BB#1:
	testl	%eax, %eax
	jne	.LBB0_5
# BB#2:                                 # %.preheader
	movq	byte_abs(%rip), %rcx
	jmp	.LBB0_3
.LBB0_4:                                # %.loopexit.loopexit2427
	movq	img(%rip), %rax
	movq	14232(%rax), %rcx
.LBB0_3:                                # %.loopexit
	movslq	(%rdi), %rdx
	movslq	4(%rdi), %rax
	movl	(%rcx,%rax,4), %eax
	addl	(%rcx,%rdx,4), %eax
	movslq	8(%rdi), %rdx
	addl	(%rcx,%rdx,4), %eax
	movslq	12(%rdi), %rdx
	addl	(%rcx,%rdx,4), %eax
	movslq	16(%rdi), %rdx
	addl	(%rcx,%rdx,4), %eax
	movslq	20(%rdi), %rdx
	addl	(%rcx,%rdx,4), %eax
	movslq	24(%rdi), %rdx
	addl	(%rcx,%rdx,4), %eax
	movslq	28(%rdi), %rdx
	addl	(%rcx,%rdx,4), %eax
	movslq	32(%rdi), %rdx
	addl	(%rcx,%rdx,4), %eax
	movslq	36(%rdi), %rdx
	addl	(%rcx,%rdx,4), %eax
	movslq	40(%rdi), %rdx
	addl	(%rcx,%rdx,4), %eax
	movslq	44(%rdi), %rdx
	addl	(%rcx,%rdx,4), %eax
	movslq	48(%rdi), %rdx
	addl	(%rcx,%rdx,4), %eax
	movslq	52(%rdi), %rdx
	addl	(%rcx,%rdx,4), %eax
	movslq	56(%rdi), %rdx
	addl	(%rcx,%rdx,4), %eax
	movslq	60(%rdi), %rdx
	addl	(%rcx,%rdx,4), %eax
	retq
.LBB0_5:
	jmp	HadamardSAD4x4          # TAILCALL
.Lfunc_end0:
	.size	distortion4x4, .Lfunc_end0-distortion4x4
	.cfi_endproc

	.globl	HadamardSAD4x4
	.p2align	4, 0x90
	.type	HadamardSAD4x4,@function
HadamardSAD4x4:                         # @HadamardSAD4x4
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movl	48(%rdi), %edx
	movl	(%rdi), %r12d
	movl	4(%rdi), %r13d
	leal	(%rdx,%r12), %eax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movl	52(%rdi), %esi
	movl	16(%rdi), %r10d
	movl	32(%rdi), %ecx
	leal	(%rcx,%r10), %eax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movl	20(%rdi), %r9d
	movl	36(%rdi), %ebp
	leal	(%rbp,%r9), %ebx
	movq	%rbx, -24(%rsp)         # 8-byte Spill
	movl	24(%rdi), %r11d
	movl	40(%rdi), %ebx
	leal	(%rbx,%r11), %eax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	subl	%ecx, %r10d
	movl	28(%rdi), %ecx
	subl	%ebp, %r9d
	movl	44(%rdi), %ebp
	subl	%ebx, %r11d
	leal	(%rbp,%rcx), %r14d
	subl	%ebp, %ecx
	leal	(%rsi,%r13), %r15d
	subl	%edx, %r12d
	movl	8(%rdi), %ebx
	subl	%esi, %r13d
	movl	56(%rdi), %esi
	leal	(%rsi,%rbx), %r8d
	subl	%esi, %ebx
	movl	12(%rdi), %esi
	movl	60(%rdi), %edi
	leal	(%rdi,%rsi), %ebp
	subl	%edi, %esi
	movq	-32(%rsp), %rdx         # 8-byte Reload
	movq	-56(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rdx), %edi
	movq	%rdi, -40(%rsp)         # 8-byte Spill
	subl	%eax, %edx
	movq	%rdx, -32(%rsp)         # 8-byte Spill
	movq	-24(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%r15), %eax
	subl	%edx, %r15d
	movq	-48(%rsp), %rdi         # 8-byte Reload
	leal	(%rdi,%r8), %edx
	subl	%edi, %r8d
	leal	(%r14,%rbp), %edi
	subl	%r14d, %ebp
	leal	(%r12,%r10), %r14d
	subl	%r10d, %r12d
	leal	(%r13,%r9), %r10d
	subl	%r9d, %r13d
	leal	(%rbx,%r11), %r9d
	subl	%r11d, %ebx
	leal	(%rsi,%rcx), %r11d
	subl	%ecx, %esi
	leal	(%rdx,%rax), %ecx
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	subl	%edx, %eax
	movq	-40(%rsp), %rdx         # 8-byte Reload
	leal	(%rdi,%rdx), %ecx
	subl	%edi, %edx
	movq	%rdx, -40(%rsp)         # 8-byte Spill
	leal	(%r9,%r10), %edx
	movq	%rdx, -56(%rsp)         # 8-byte Spill
	subl	%r9d, %r10d
	leal	(%r11,%r14), %edi
	subl	%r11d, %r14d
	leal	(%r8,%r15), %r9d
	subl	%r8d, %r15d
	movq	-32(%rsp), %rdx         # 8-byte Reload
	leal	(%rbp,%rdx), %r8d
	subl	%ebp, %edx
	movq	%rdx, -32(%rsp)         # 8-byte Spill
	leal	(%rbx,%r13), %r11d
	subl	%ebx, %r13d
	leal	(%rsi,%r12), %ebx
	subl	%esi, %r12d
	movl	%ecx, m.0(%rip)
	movq	-48(%rsp), %rsi         # 8-byte Reload
	movl	%esi, m.1(%rip)
	leal	(%rsi,%rcx), %edx
	movl	%edx, -24(%rsp)         # 4-byte Spill
	subl	%esi, %ecx
	movq	%rcx, %rdx
	movl	%eax, m.2(%rip)
	movq	-40(%rsp), %rsi         # 8-byte Reload
	movl	%esi, m.3(%rip)
	leal	(%rsi,%rax), %ecx
	subl	%eax, %esi
	movl	%edi, m.4(%rip)
	movq	-56(%rsp), %rax         # 8-byte Reload
	movl	%eax, m.5(%rip)
	leal	(%rax,%rdi), %ebp
	movl	%ebp, -40(%rsp)         # 4-byte Spill
	subl	%eax, %edi
	movq	%rdi, -8(%rsp)          # 8-byte Spill
	movl	%r10d, m.6(%rip)
	movl	%r14d, m.7(%rip)
	leal	(%r14,%r10), %eax
	movl	%eax, -48(%rsp)         # 4-byte Spill
	subl	%r10d, %r14d
	movl	%r8d, m.8(%rip)
	movq	%r9, %rax
	movl	%eax, m.9(%rip)
	leal	(%rax,%r8), %r10d
	subl	%eax, %r8d
	movq	%r8, -56(%rsp)          # 8-byte Spill
	movl	%r15d, m.10(%rip)
	movq	-32(%rsp), %r9          # 8-byte Reload
	movl	%r9d, m.11(%rip)
	leal	(%r9,%r15), %ebp
	movl	%ebp, -12(%rsp)         # 4-byte Spill
	subl	%r15d, %r9d
	movl	%ebx, m.12(%rip)
	movl	%r11d, m.13(%rip)
	leal	(%r11,%rbx), %r8d
	subl	%r11d, %ebx
	movq	%rbx, %r11
	movl	%r13d, m.14(%rip)
	movl	%r12d, m.15(%rip)
	leal	(%r12,%r13), %ebx
	subl	%r13d, %r12d
	movl	-24(%rsp), %eax         # 4-byte Reload
	movl	%eax, d(%rip)
	movq	%rdx, %r15
	movl	%r15d, d+4(%rip)
	movl	%ecx, d+8(%rip)
	movq	%rsi, %rdx
	movl	%edx, d+12(%rip)
	movl	-40(%rsp), %esi         # 4-byte Reload
	movl	%esi, d+16(%rip)
	movl	%edi, d+20(%rip)
	movl	-48(%rsp), %edi         # 4-byte Reload
	movl	%edi, d+24(%rip)
	movl	%r14d, d+28(%rip)
	movl	%r10d, d+32(%rip)
	movq	-56(%rsp), %r13         # 8-byte Reload
	movl	%r13d, d+36(%rip)
	movl	%ebp, d+40(%rip)
	movl	%r9d, d+44(%rip)
	movq	%r9, %rbp
	movl	%r8d, d+48(%rip)
	movl	%r11d, d+52(%rip)
	movl	%ebx, d+56(%rip)
	movl	%r12d, d+60(%rip)
	cltq
	movslq	%r15d, %r9
	movq	byte_abs(%rip), %r15
	movl	(%r15,%r9,4), %r9d
	addl	(%r15,%rax,4), %r9d
	movslq	%ecx, %rax
	addl	(%r15,%rax,4), %r9d
	movslq	%edx, %rax
	addl	(%r15,%rax,4), %r9d
	movslq	%esi, %rax
	addl	(%r15,%rax,4), %r9d
	movslq	-8(%rsp), %rax          # 4-byte Folded Reload
	addl	(%r15,%rax,4), %r9d
	movslq	%edi, %rax
	addl	(%r15,%rax,4), %r9d
	movslq	%r14d, %rax
	addl	(%r15,%rax,4), %r9d
	movslq	%r10d, %rax
	addl	(%r15,%rax,4), %r9d
	movslq	%r13d, %rax
	addl	(%r15,%rax,4), %r9d
	movslq	-12(%rsp), %rax         # 4-byte Folded Reload
	addl	(%r15,%rax,4), %r9d
	movslq	%ebp, %rax
	addl	(%r15,%rax,4), %r9d
	movslq	%r8d, %rax
	addl	(%r15,%rax,4), %r9d
	movslq	%r11d, %rax
	addl	(%r15,%rax,4), %r9d
	movslq	%ebx, %rax
	addl	(%r15,%rax,4), %r9d
	movslq	%r12d, %rax
	movl	(%r15,%rax,4), %eax
	leal	1(%rax,%r9), %eax
	sarl	%eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	HadamardSAD4x4, .Lfunc_end1-HadamardSAD4x4
	.cfi_endproc

	.globl	distortion8x8
	.p2align	4, 0x90
	.type	distortion8x8,@function
distortion8x8:                          # @distortion8x8
	.cfi_startproc
# BB#0:
	movq	input(%rip), %rax
	movl	5792(%rax), %eax
	cmpl	$1, %eax
	je	.LBB2_4
# BB#1:
	testl	%eax, %eax
	jne	.LBB2_7
# BB#2:                                 # %.preheader
	xorl	%ecx, %ecx
	movq	byte_abs(%rip), %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	movslq	(%rdi,%rcx,4), %rsi
	addl	(%rdx,%rsi,4), %eax
	movslq	4(%rdi,%rcx,4), %rsi
	addl	(%rdx,%rsi,4), %eax
	movslq	8(%rdi,%rcx,4), %rsi
	addl	(%rdx,%rsi,4), %eax
	movslq	12(%rdi,%rcx,4), %rsi
	addl	(%rdx,%rsi,4), %eax
	addq	$4, %rcx
	cmpq	$64, %rcx
	jne	.LBB2_3
	jmp	.LBB2_6
.LBB2_4:
	movq	img(%rip), %rax
	movq	14232(%rax), %rcx
	xorl	%edx, %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_5:                                # =>This Inner Loop Header: Depth=1
	movslq	(%rdi,%rdx,4), %rsi
	addl	(%rcx,%rsi,4), %eax
	movslq	4(%rdi,%rdx,4), %rsi
	addl	(%rcx,%rsi,4), %eax
	movslq	8(%rdi,%rdx,4), %rsi
	addl	(%rcx,%rsi,4), %eax
	movslq	12(%rdi,%rdx,4), %rsi
	addl	(%rcx,%rsi,4), %eax
	addq	$4, %rdx
	cmpq	$64, %rdx
	jne	.LBB2_5
.LBB2_6:                                # %.loopexit
	retq
.LBB2_7:
	jmp	HadamardSAD8x8          # TAILCALL
.Lfunc_end2:
	.size	distortion8x8, .Lfunc_end2-distortion8x8
	.cfi_endproc

	.globl	HadamardSAD8x8
	.p2align	4, 0x90
	.type	HadamardSAD8x8,@function
HadamardSAD8x8:                         # @HadamardSAD8x8
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 56
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movl	16(%rdi,%rax), %r14d
	movl	(%rdi,%rax), %ecx
	movl	4(%rdi,%rax), %r11d
	leal	(%r14,%rcx), %ebp
	movl	20(%rdi,%rax), %r12d
	leal	(%r12,%r11), %r8d
	movl	8(%rdi,%rax), %r9d
	movl	24(%rdi,%rax), %edx
	leal	(%rdx,%r9), %r15d
	movl	12(%rdi,%rax), %r10d
	movl	28(%rdi,%rax), %ebx
	leal	(%rbx,%r10), %r13d
	subl	%r14d, %ecx
	subl	%r12d, %r11d
	subl	%edx, %r9d
	subl	%ebx, %r10d
	leal	(%r15,%rbp), %edx
	movl	%edx, m1(%rax)
	leal	(%r13,%r8), %r14d
	movl	%r14d, m1+4(%rax)
	subl	%r15d, %ebp
	movl	%ebp, m1+8(%rax)
	subl	%r13d, %r8d
	movl	%r8d, m1+12(%rax)
	leal	(%r9,%rcx), %ebx
	movl	%ebx, m1+16(%rax)
	leal	(%r10,%r11), %r15d
	movl	%r15d, m1+20(%rax)
	subl	%r9d, %ecx
	movl	%ecx, m1+24(%rax)
	subl	%r10d, %r11d
	movl	%r11d, m1+28(%rax)
	leal	(%r14,%rdx), %esi
	movl	%esi, m2(%rax)
	subl	%r14d, %edx
	movl	%edx, m2+4(%rax)
	leal	(%r8,%rbp), %edx
	movl	%edx, m2+8(%rax)
	subl	%r8d, %ebp
	movl	%ebp, m2+12(%rax)
	leal	(%r15,%rbx), %edx
	movl	%edx, m2+16(%rax)
	subl	%r15d, %ebx
	movl	%ebx, m2+20(%rax)
	leal	(%r11,%rcx), %edx
	movl	%edx, m2+24(%rax)
	subl	%r11d, %ecx
	movl	%ecx, m2+28(%rax)
	addq	$32, %rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB3_1
# BB#2:                                 # %.preheader183.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_3:                                # %.preheader183
                                        # =>This Inner Loop Header: Depth=1
	movl	m2(%rax), %ecx
	movl	m2+128(%rax), %r11d
	leal	(%r11,%rcx), %esi
	movl	%esi, m3.0(%rax)
	movl	m2+32(%rax), %r10d
	movl	m2+160(%rax), %r14d
	leal	(%r14,%r10), %r8d
	movl	%r8d, m3.1(%rax)
	movl	m2+64(%rax), %r9d
	movl	m2+192(%rax), %ebx
	leal	(%rbx,%r9), %r15d
	movl	%r15d, m3.2(%rax)
	movl	m2+96(%rax), %edi
	movl	m2+224(%rax), %ebp
	leal	(%rbp,%rdi), %edx
	movl	%edx, m3.3(%rax)
	subl	%r11d, %ecx
	movl	%ecx, m3.4(%rax)
	subl	%r14d, %r10d
	movl	%r10d, m3.5(%rax)
	subl	%ebx, %r9d
	movl	%r9d, m3.6(%rax)
	subl	%ebp, %edi
	movl	%edi, m3.7(%rax)
	leal	(%r15,%rsi), %ebx
	movl	%ebx, m1(%rax)
	leal	(%rdx,%r8), %ebp
	movl	%ebp, m1+32(%rax)
	subl	%r15d, %esi
	movl	%esi, m1+64(%rax)
	subl	%edx, %r8d
	movl	%r8d, m1+96(%rax)
	leal	(%r9,%rcx), %edx
	movl	%edx, m1+128(%rax)
	leal	(%rdi,%r10), %r11d
	movl	%r11d, m1+160(%rax)
	subl	%r9d, %ecx
	movl	%ecx, m1+192(%rax)
	subl	%edi, %r10d
	movl	%r10d, m1+224(%rax)
	leal	(%rbp,%rbx), %edi
	movl	%edi, m2(%rax)
	subl	%ebp, %ebx
	movl	%ebx, m2+32(%rax)
	leal	(%r8,%rsi), %edi
	movl	%edi, m2+64(%rax)
	subl	%r8d, %esi
	movl	%esi, m2+96(%rax)
	leal	(%r11,%rdx), %esi
	movl	%esi, m2+128(%rax)
	subl	%r11d, %edx
	movl	%edx, m2+160(%rax)
	leal	(%r10,%rcx), %edx
	movl	%edx, m2+192(%rax)
	subl	%r10d, %ecx
	movl	%ecx, m2+224(%rax)
	addq	$4, %rax
	cmpq	$32, %rax
	jne	.LBB3_3
# BB#4:                                 # %.preheader
	movdqa	m2(%rip), %xmm0
	movdqa	m2+16(%rip), %xmm1
	movdqa	%xmm1, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm1
	pxor	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm0
	pxor	%xmm2, %xmm0
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	movdqa	m2+32(%rip), %xmm0
	movdqa	m2+48(%rip), %xmm1
	movdqa	%xmm1, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm1
	pxor	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm0
	pxor	%xmm2, %xmm0
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ecx
	addl	%eax, %ecx
	movdqa	m2+64(%rip), %xmm0
	movdqa	m2+80(%rip), %xmm1
	movdqa	%xmm1, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm1
	pxor	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm0
	pxor	%xmm2, %xmm0
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	addl	%ecx, %eax
	movdqa	m2+96(%rip), %xmm0
	movdqa	m2+112(%rip), %xmm1
	movdqa	%xmm1, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm1
	pxor	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm0
	pxor	%xmm2, %xmm0
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ecx
	addl	%eax, %ecx
	movdqa	m2+128(%rip), %xmm0
	movdqa	m2+144(%rip), %xmm1
	movdqa	%xmm1, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm1
	pxor	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm0
	pxor	%xmm2, %xmm0
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	addl	%ecx, %eax
	movdqa	m2+160(%rip), %xmm0
	movdqa	m2+176(%rip), %xmm1
	movdqa	%xmm1, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm1
	pxor	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm0
	pxor	%xmm2, %xmm0
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ecx
	addl	%eax, %ecx
	movdqa	m2+192(%rip), %xmm0
	movdqa	m2+208(%rip), %xmm1
	movdqa	%xmm1, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm1
	pxor	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm0
	pxor	%xmm2, %xmm0
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	addl	%ecx, %eax
	movdqa	m2+224(%rip), %xmm0
	movdqa	m2+240(%rip), %xmm1
	movdqa	%xmm1, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm1
	pxor	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm0
	pxor	%xmm2, %xmm0
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ecx
	leal	2(%rcx,%rax), %eax
	sarl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	HadamardSAD8x8, .Lfunc_end3-HadamardSAD8x8
	.cfi_endproc

	.globl	HadamardMB
	.p2align	4, 0x90
	.type	HadamardMB,@function
HadamardMB:                             # @HadamardMB
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -32
.Lcfi28:
	.cfi_offset %r14, -24
.Lcfi29:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %rbx
	leal	-2(%rsi), %eax
	cmpl	$2, %eax
	jb	.LBB4_5
# BB#1:
	cmpl	$4, %esi
	je	.LBB4_4
# BB#2:
	movl	$-1, %eax
	cmpl	$1, %esi
	jne	.LBB4_6
# BB#3:
	movq	%rbx, %rdi
	callq	HadamardSAD8x8
	movl	%eax, %r14d
	leaq	256(%rbx), %rdi
	callq	HadamardSAD8x8
	movl	%eax, %ebp
	addl	%r14d, %ebp
	leaq	512(%rbx), %rdi
	callq	HadamardSAD8x8
	movl	%eax, %r14d
	addl	%ebp, %r14d
	addq	$768, %rbx              # imm = 0x300
	movq	%rbx, %rdi
	callq	HadamardSAD8x8
	addl	%r14d, %eax
	jmp	.LBB4_6
.LBB4_5:
	movq	%rbx, %rdi
	callq	HadamardSAD8x8
	movl	%eax, %ebp
	addq	$256, %rbx              # imm = 0x100
	movq	%rbx, %rdi
	callq	HadamardSAD8x8
	addl	%ebp, %eax
.LBB4_6:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB4_4:
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	HadamardSAD8x8          # TAILCALL
.Lfunc_end4:
	.size	HadamardMB, .Lfunc_end4-HadamardMB
	.cfi_endproc

	.globl	computeSAD
	.p2align	4, 0x90
	.type	computeSAD,@function
computeSAD:                             # @computeSAD
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 96
.Lcfi37:
	.cfi_offset %rbx, -56
.Lcfi38:
	.cfi_offset %r12, -48
.Lcfi39:
	.cfi_offset %r13, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %r14d
	movl	%esi, %r13d
	movslq	img_padded_size_x(%rip), %r12
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, src_line(%rip)
	movslq	ref_access_method(%rip), %rax
	movq	ref_pic_sub(%rip), %rdi
	movl	%r9d, 8(%rsp)           # 4-byte Spill
	movl	%r9d, %esi
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movl	%r8d, %edx
	callq	*get_line(,%rax,8)
	movq	%rax, ref_line(%rip)
	testl	%r13d, %r13d
	jle	.LBB5_12
# BB#1:                                 # %.preheader66.lr.ph
	movslq	%r14d, %rcx
	subq	%rcx, %r12
	testl	%ecx, %ecx
	jle	.LBB5_9
# BB#2:                                 # %.preheader66.us.preheader
	movq	byte_abs(%rip), %rcx
	leal	-1(%r14), %esi
	andl	$-4, %esi
	movq	src_line(%rip), %rdx
	leaq	(%rsi,%rsi), %rdi
	leaq	8(%rdi,%r12,2), %rdi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	leaq	8(%rsi,%rsi), %r10
	leaq	4(%rsi), %rsi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	pxor	%xmm0, %xmm0
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB5_3:                                # %.preheader66.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_4 Depth 2
	movl	%r13d, %r9d
	movl	%r15d, %r13d
	leaq	(%rdx,%r10), %r11
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_4:                                #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rdi,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movq	(%rax,%rdi,2), %xmm2    # xmm2 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	psubd	%xmm2, %xmm1
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	movd	%xmm2, %rsi
	movd	%xmm1, %r8
	movslq	%r8d, %r15
	addl	(%rcx,%r15,4), %ebx
	sarq	$32, %r8
	addl	(%rcx,%r8,4), %ebx
	movslq	%esi, %rbp
	addl	(%rcx,%rbp,4), %ebx
	sarq	$32, %rsi
	addl	(%rcx,%rsi,4), %ebx
	addq	$4, %rdi
	cmpl	%r14d, %edi
	jl	.LBB5_4
# BB#5:                                 # %._crit_edge99.us
                                        #   in Loop: Header=BB5_3 Depth=1
	movl	%r13d, %r15d
	cmpl	%r15d, %ebx
	jge	.LBB5_21
# BB#6:                                 #   in Loop: Header=BB5_3 Depth=1
	addq	24(%rsp), %rax          # 8-byte Folded Reload
	incl	%r12d
	movl	%r9d, %r13d
	cmpl	%r13d, %r12d
	movq	%r11, %rdx
	jl	.LBB5_3
# BB#7:                                 # %._crit_edge107.loopexit
	movq	%r11, src_line(%rip)
	movq	%rax, ref_line(%rip)
	cmpl	$0, ChromaMEEnable(%rip)
	jne	.LBB5_13
	jmp	.LBB5_37
.LBB5_9:                                # %.preheader66.preheader
	leaq	(%rax,%r12,2), %rax
	addq	%r12, %r12
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_10:                               # %.preheader66
                                        # =>This Inner Loop Header: Depth=1
	testl	%r15d, %r15d
	jle	.LBB5_37
# BB#11:                                #   in Loop: Header=BB5_10 Depth=1
	movq	%rax, ref_line(%rip)
	incl	%ecx
	addq	%r12, %rax
	cmpl	%r13d, %ecx
	jl	.LBB5_10
.LBB5_12:
	xorl	%ebx, %ebx
	cmpl	$0, ChromaMEEnable(%rip)
	je	.LBB5_37
.LBB5_13:
	movb	shift_cr_x(%rip), %cl
	sarl	%cl, %r14d
	movb	shift_cr_y(%rip), %cl
	sarl	%cl, %r13d
	testl	%r13d, %r13d
	jle	.LBB5_22
# BB#14:                                # %.split.us.preheader
	movslq	img_cr_padded_size_x(%rip), %r12
	movslq	%r14d, %rbp
	subq	%rbp, %r12
	leal	-1(%rbp), %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	512(%rax), %rax
	movq	%rax, src_line(%rip)
	movslq	ref_access_method(%rip), %rax
	movq	ref_pic_sub+8(%rip), %rdi
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	*get_crline(,%rax,8)
	testl	%ebp, %ebp
	movq	24(%rsp), %rbp          # 8-byte Reload
	leaq	2(%rbp,%rbp), %rsi
	movq	%rax, ref_line(%rip)
	jle	.LBB5_23
# BB#15:                                # %.preheader.us.us.preheader
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	byte_abs(%rip), %rcx
	movq	src_line(%rip), %rsi
	leaq	(%r12,%r12), %rdx
	leaq	4(%rdx,%rbp,4), %r8
	leaq	4(,%rbp,4), %r9
	movl	%r14d, %edi
	xorl	%r11d, %r11d
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB5_16:                               # %.preheader.us.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_17 Depth 2
	leaq	(%rsi,%r9), %r10
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_17:                               #   Parent Loop BB5_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	(%rsi,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	pshufd	$212, %xmm1, %xmm1      # xmm1 = xmm1[0,1,1,3]
	movd	(%rax,%rdx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	pshufd	$212, %xmm2, %xmm2      # xmm2 = xmm2[0,1,1,3]
	psubq	%xmm2, %xmm1
	movd	%xmm1, %ebp
	movslq	%ebp, %rbp
	addl	(%rcx,%rbp,4), %ebx
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	movd	%xmm1, %ebp
	movslq	%ebp, %rbp
	addl	(%rcx,%rbp,4), %ebx
	incq	%rdx
	cmpl	%edx, %edi
	jne	.LBB5_17
# BB#18:                                # %._crit_edge.us.us
                                        #   in Loop: Header=BB5_16 Depth=1
	cmpl	%r15d, %ebx
	jge	.LBB5_34
# BB#19:                                #   in Loop: Header=BB5_16 Depth=1
	addq	%r8, %rax
	incl	%r11d
	cmpl	%r13d, %r11d
	movq	%r10, %rsi
	jl	.LBB5_16
# BB#20:                                # %._crit_edge76.us.loopexit
	movq	%r10, src_line(%rip)
	movq	%rax, ref_line(%rip)
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB5_27
.LBB5_21:                               # %.thread.loopexit143
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,2), %rax
	movq	%r11, src_line(%rip)
	jmp	.LBB5_36
.LBB5_22:                               # %.split.preheader
	movq	32(%rsp), %rbp          # 8-byte Reload
	leaq	512(%rbp), %rax
	movq	%rax, src_line(%rip)
	movslq	ref_access_method(%rip), %rax
	movq	ref_pic_sub+8(%rip), %rdi
	movl	8(%rsp), %r14d          # 4-byte Reload
	movl	%r14d, %esi
	movl	12(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %edx
	callq	*get_crline(,%rax,8)
	movq	%rax, ref_line(%rip)
	addq	$1024, %rbp             # imm = 0x400
	movq	%rbp, src_line(%rip)
	movslq	ref_access_method(%rip), %rax
	movq	ref_pic_sub+16(%rip), %rdi
	movl	%r14d, %esi
	movl	%r15d, %edx
	callq	*get_crline(,%rax,8)
	jmp	.LBB5_36
.LBB5_23:                               # %.preheader.us83.preheader
	leaq	(%rax,%r12,2), %rax
	leaq	(%r12,%r12), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_24:                               # %.preheader.us83
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r15d, %ebx
	jge	.LBB5_37
# BB#25:                                #   in Loop: Header=BB5_24 Depth=1
	movq	%rax, ref_line(%rip)
	incl	%edx
	addq	%rcx, %rax
	cmpl	%r13d, %edx
	jl	.LBB5_24
# BB#26:
	movq	%rsi, 16(%rsp)          # 8-byte Spill
.LBB5_27:                               # %._crit_edge76.us
	movq	32(%rsp), %rax          # 8-byte Reload
	addq	$1024, %rax             # imm = 0x400
	movq	%rax, src_line(%rip)
	movslq	ref_access_method(%rip), %rax
	movq	ref_pic_sub+16(%rip), %rdi
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	*get_crline(,%rax,8)
	testl	%r14d, %r14d
	movq	%rax, ref_line(%rip)
	jle	.LBB5_38
# BB#28:                                # %.preheader.us.us.preheader.1
	movq	byte_abs(%rip), %rcx
	movq	src_line(%rip), %rdx
	addq	%r12, %r12
	leaq	4(%r12,%rbp,4), %r8
	leaq	4(,%rbp,4), %r9
	movl	%r14d, %edi
	xorl	%r11d, %r11d
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB5_29:                               # %.preheader.us.us.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_30 Depth 2
	leaq	(%rdx,%r9), %r10
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_30:                               #   Parent Loop BB5_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	(%rdx,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	pshufd	$212, %xmm1, %xmm1      # xmm1 = xmm1[0,1,1,3]
	movd	(%rax,%rsi,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	pshufd	$212, %xmm2, %xmm2      # xmm2 = xmm2[0,1,1,3]
	psubq	%xmm2, %xmm1
	movd	%xmm1, %ebp
	movslq	%ebp, %rbp
	addl	(%rcx,%rbp,4), %ebx
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	movd	%xmm1, %ebp
	movslq	%ebp, %rbp
	addl	(%rcx,%rbp,4), %ebx
	incq	%rsi
	cmpl	%esi, %edi
	jne	.LBB5_30
# BB#31:                                # %._crit_edge.us.us.1
                                        #   in Loop: Header=BB5_29 Depth=1
	cmpl	%r15d, %ebx
	jge	.LBB5_34
# BB#32:                                #   in Loop: Header=BB5_29 Depth=1
	addq	%r8, %rax
	incl	%r11d
	cmpl	%r13d, %r11d
	movq	%r10, %rdx
	jl	.LBB5_29
	jmp	.LBB5_35
.LBB5_34:                               # %.thread.loopexit140
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,2), %rax
.LBB5_35:                               # %.thread
	movq	%r10, src_line(%rip)
.LBB5_36:                               # %.thread
	movq	%rax, ref_line(%rip)
.LBB5_37:                               # %.thread
	movl	%ebx, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_38:                               # %.preheader.us83.1.preheader
	leaq	(%rax,%r12,2), %rax
	addq	%r12, %r12
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_39:                               # %.preheader.us83.1
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r15d, %ebx
	jge	.LBB5_37
# BB#40:                                #   in Loop: Header=BB5_39 Depth=1
	movq	%rax, ref_line(%rip)
	incl	%ecx
	addq	%r12, %rax
	cmpl	%r13d, %ecx
	jl	.LBB5_39
	jmp	.LBB5_37
.Lfunc_end5:
	.size	computeSAD, .Lfunc_end5-computeSAD
	.cfi_endproc

	.globl	computeSADWP
	.p2align	4, 0x90
	.type	computeSADWP,@function
computeSADWP:                           # @computeSADWP
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi46:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi49:
	.cfi_def_cfa_offset 144
.Lcfi50:
	.cfi_offset %rbx, -56
.Lcfi51:
	.cfi_offset %r12, -48
.Lcfi52:
	.cfi_offset %r13, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movl	%r9d, %r13d
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movl	%edx, %ebp
	movl	%esi, %ebx
	movq	%rdi, %r12
	movslq	img_padded_size_x(%rip), %r15
	movq	%r12, src_line(%rip)
	movslq	ref_access_method(%rip), %rax
	movq	ref_pic_sub(%rip), %rdi
	movl	%r13d, %esi
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movl	%r8d, %edx
	callq	*get_line(,%rax,8)
	movq	%rax, ref_line(%rip)
	movl	%ebx, (%rsp)            # 4-byte Spill
	testl	%ebx, %ebx
	jle	.LBB6_12
# BB#1:                                 # %.preheader78.lr.ph
	movslq	%ebp, %rcx
	subq	%rcx, %r15
	testl	%ecx, %ecx
	jle	.LBB6_9
# BB#2:                                 # %.preheader78.us.preheader
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	byte_abs(%rip), %rcx
	movq	img(%rip), %rsi
	movq	%rbp, %r12
	leal	-1(%rbp), %ebp
	andl	$-4, %ebp
	movq	src_line(%rip), %rdx
	movd	weight_luma(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm8        # xmm8 = xmm0[0,0,0,0]
	movd	wp_luma_round(%rip), %xmm1 # xmm1 = mem[0],zero,zero,zero
	pshufd	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movss	luma_log_weight_denom(%rip), %xmm6 # xmm6 = mem[0],zero,zero,zero
	movd	offset_luma(%rip), %xmm2 # xmm2 = mem[0],zero,zero,zero
	pshufd	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	movd	15520(%rsi), %xmm3      # xmm3 = mem[0],zero,zero,zero
	pshufd	$0, %xmm3, %xmm3        # xmm3 = xmm3[0,0,0,0]
	leaq	8(%rbp,%rbp), %r9
	leaq	4(%rbp), %r10
	leaq	(%rbp,%rbp), %rsi
	leaq	8(%rsi,%r15,2), %r8
	xorl	%r14d, %r14d
	pxor	%xmm4, %xmm4
	xorps	%xmm5, %xmm5
	movss	%xmm6, %xmm5            # xmm5 = xmm6[0],xmm5[1,2,3]
	pshufd	$245, %xmm8, %xmm6      # xmm6 = xmm8[1,1,3,3]
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB6_3:                                # %.preheader78.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_4 Depth 2
	leaq	(%rax,%r10,2), %r11
	xorl	%esi, %esi
	movq	%r12, %r15
	.p2align	4, 0x90
.LBB6_4:                                #   Parent Loop BB6_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rsi,2), %xmm7    # xmm7 = mem[0],zero
	punpcklwd	%xmm4, %xmm7    # xmm7 = xmm7[0],xmm4[0],xmm7[1],xmm4[1],xmm7[2],xmm4[2],xmm7[3],xmm4[3]
	pshufd	$245, %xmm7, %xmm0      # xmm0 = xmm7[1,1,3,3]
	pmuludq	%xmm8, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pmuludq	%xmm6, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm7    # xmm7 = xmm7[0],xmm0[0],xmm7[1],xmm0[1]
	paddd	%xmm1, %xmm7
	psrad	%xmm5, %xmm7
	paddd	%xmm2, %xmm7
	movdqa	%xmm7, %xmm0
	pcmpgtd	%xmm4, %xmm0
	pand	%xmm7, %xmm0
	movdqa	%xmm3, %xmm7
	pcmpgtd	%xmm0, %xmm7
	pand	%xmm7, %xmm0
	pandn	%xmm3, %xmm7
	por	%xmm0, %xmm7
	movq	(%rdx,%rsi,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1],xmm0[2],xmm4[2],xmm0[3],xmm4[3]
	psubd	%xmm7, %xmm0
	pshufd	$78, %xmm0, %xmm7       # xmm7 = xmm0[2,3,0,1]
	movd	%xmm7, %rdi
	movd	%xmm0, %rbp
	movslq	%ebp, %rbx
	addl	(%rcx,%rbx,4), %r14d
	sarq	$32, %rbp
	addl	(%rcx,%rbp,4), %r14d
	movslq	%edi, %rbx
	addl	(%rcx,%rbx,4), %r14d
	sarq	$32, %rdi
	addl	(%rcx,%rdi,4), %r14d
	addq	$4, %rsi
	cmpl	%r15d, %esi
	jl	.LBB6_4
# BB#5:                                 # %._crit_edge95.us
                                        #   in Loop: Header=BB6_3 Depth=1
	addq	%r9, %rdx
	cmpl	4(%rsp), %r14d          # 4-byte Folded Reload
	jge	.LBB6_29
# BB#6:                                 #   in Loop: Header=BB6_3 Depth=1
	addq	%r8, %rax
	incl	%r13d
	cmpl	(%rsp), %r13d           # 4-byte Folded Reload
	jl	.LBB6_3
# BB#7:                                 # %._crit_edge103.loopexit
	movq	%rax, ref_line(%rip)
	movq	%rdx, src_line(%rip)
	movq	%r12, %rbp
	movq	24(%rsp), %r12          # 8-byte Reload
	movl	8(%rsp), %r13d          # 4-byte Reload
	jmp	.LBB6_13
.LBB6_9:                                # %.preheader78.preheader
	leaq	(%rax,%r15,2), %rax
	addq	%r15, %r15
	xorl	%r14d, %r14d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_10:                               # %.preheader78
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB6_30
# BB#11:                                #   in Loop: Header=BB6_10 Depth=1
	movq	%rax, ref_line(%rip)
	incl	%ecx
	addq	%r15, %rax
	cmpl	(%rsp), %ecx            # 4-byte Folded Reload
	jl	.LBB6_10
.LBB6_12:
	xorl	%r14d, %r14d
.LBB6_13:                               # %._crit_edge103
	cmpl	$0, ChromaMEEnable(%rip)
	movl	12(%rsp), %r15d         # 4-byte Reload
	je	.LBB6_30
# BB#14:
	movb	shift_cr_x(%rip), %cl
	sarl	%cl, %ebp
	movb	shift_cr_y(%rip), %cl
	sarl	%cl, (%rsp)             # 4-byte Folded Spill
	movslq	img_cr_padded_size_x(%rip), %rcx
	movslq	%ebp, %rax
	subq	%rax, %rcx
	decl	%eax
	leaq	2(%rax,%rax), %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rcx), %rbx
	leaq	4(,%rax,4), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	leaq	4(%rbx,%rax,4), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	jmp	.LBB6_18
	.p2align	4, 0x90
.LBB6_15:                               # %.preheader.preheader
                                        #   in Loop: Header=BB6_18 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,2), %rax
	xorl	%edx, %edx
	movq	16(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_16:                               # %.preheader
                                        #   Parent Loop BB6_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	4(%rsp), %r14d          # 4-byte Folded Reload
	jge	.LBB6_30
# BB#17:                                #   in Loop: Header=BB6_16 Depth=2
	movq	%rax, ref_line(%rip)
	incl	%edx
	addq	%rbx, %rax
	cmpl	(%rsp), %edx            # 4-byte Folded Reload
	jl	.LBB6_16
	jmp	.LBB6_27
	.p2align	4, 0x90
.LBB6_18:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_16 Depth 2
                                        #     Child Loop BB6_21 Depth 2
                                        #       Child Loop BB6_22 Depth 3
	movl	$256, %eax              # imm = 0x100
	shll	%cl, %eax
	cltq
	leaq	(%r12,%rax,2), %rax
	movq	%rax, src_line(%rip)
	movslq	ref_access_method(%rip), %rax
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	ref_pic_sub+8(,%rcx,8), %rdi
	movl	%r13d, %esi
	movl	%r15d, %edx
	callq	*get_crline(,%rax,8)
	movq	%rax, ref_line(%rip)
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB6_26
# BB#19:                                # %.preheader.lr.ph
                                        #   in Loop: Header=BB6_18 Depth=1
	testl	%ebp, %ebp
	jle	.LBB6_15
# BB#20:                                # %.preheader.us.preheader
                                        #   in Loop: Header=BB6_18 Depth=1
	movl	wp_chroma_round(%rip), %r8d
	movl	chroma_log_weight_denom(%rip), %ecx
	movq	byte_abs(%rip), %r11
	movq	img(%rip), %rdx
	movl	15524(%rdx), %ebx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	weight_cr(,%rdx,4), %r9d
	movl	offset_cr(,%rdx,4), %r12d
	xorl	%edx, %edx
	movq	src_line(%rip), %rdi
	.p2align	4, 0x90
.LBB6_21:                               # %.preheader.us
                                        #   Parent Loop BB6_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_22 Depth 3
	movl	%edx, 36(%rsp)          # 4-byte Spill
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%rax,%rdx,2), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_22:                               #   Parent Loop BB6_18 Depth=1
                                        #     Parent Loop BB6_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	(%rax,%rsi,4), %r15d
	imull	%r9d, %r15d
	addl	%r8d, %r15d
	sarl	%cl, %r15d
	addl	%r12d, %r15d
	movl	$0, %edx
	cmovsl	%edx, %r15d
	cmpl	%ebx, %r15d
	cmovgl	%ebx, %r15d
	movzwl	(%rdi,%rsi,4), %r10d
	movq	%rbp, %r13
	movslq	%r15d, %rbp
	subq	%rbp, %r10
	addl	(%r11,%r10,4), %r14d
	movzwl	2(%rax,%rsi,4), %ebp
	imull	%r9d, %ebp
	addl	%r8d, %ebp
	sarl	%cl, %ebp
	addl	%r12d, %ebp
	cmovsl	%edx, %ebp
	cmpl	%ebx, %ebp
	cmovgl	%ebx, %ebp
	movzwl	2(%rdi,%rsi,4), %edx
	movslq	%ebp, %rbp
	subq	%rbp, %rdx
	movq	%r13, %rbp
	addl	(%r11,%rdx,4), %r14d
	incq	%rsi
	cmpl	%esi, %ebp
	jne	.LBB6_22
# BB#23:                                # %._crit_edge.us
                                        #   in Loop: Header=BB6_21 Depth=2
	addq	72(%rsp), %rdi          # 8-byte Folded Reload
	cmpl	4(%rsp), %r14d          # 4-byte Folded Reload
	jge	.LBB6_28
# BB#24:                                #   in Loop: Header=BB6_21 Depth=2
	addq	56(%rsp), %rax          # 8-byte Folded Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	incl	%edx
	cmpl	(%rsp), %edx            # 4-byte Folded Reload
	jl	.LBB6_21
# BB#25:                                # %._crit_edge88.loopexit
                                        #   in Loop: Header=BB6_18 Depth=1
	movq	%rax, ref_line(%rip)
	movq	%rdi, src_line(%rip)
	movl	8(%rsp), %r13d          # 4-byte Reload
	movl	12(%rsp), %r15d         # 4-byte Reload
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
.LBB6_26:                               # %._crit_edge88
                                        #   in Loop: Header=BB6_18 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB6_27:                               # %._crit_edge88
                                        #   in Loop: Header=BB6_18 Depth=1
	incq	%rcx
	cmpq	$2, %rcx
	jl	.LBB6_18
	jmp	.LBB6_30
.LBB6_28:                               # %.thread.loopexit
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, ref_line(%rip)
	movq	%rdi, src_line(%rip)
	jmp	.LBB6_30
.LBB6_29:                               # %.thread.loopexit133
	movq	%r11, ref_line(%rip)
	movq	%rdx, src_line(%rip)
.LBB6_30:                               # %.thread
	movl	%r14d, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	computeSADWP, .Lfunc_end6-computeSADWP
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI7_1:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
.LCPI7_2:
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.text
	.globl	computeBiPredSAD1
	.p2align	4, 0x90
	.type	computeBiPredSAD1,@function
computeBiPredSAD1:                      # @computeBiPredSAD1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi62:
	.cfi_def_cfa_offset 112
.Lcfi63:
	.cfi_offset %rbx, -56
.Lcfi64:
	.cfi_offset %r12, -48
.Lcfi65:
	.cfi_offset %r13, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebx
	movl	%r8d, %ebp
	movl	%ecx, %r15d
	movl	%edx, %r14d
	movl	%esi, %r13d
	movl	120(%rsp), %esi
	movl	112(%rsp), %edx
	movslq	img_padded_size_x(%rip), %r12
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movq	%rdi, src_line(%rip)
	movslq	bipred2_access_method(%rip), %rax
	movq	ref_pic2_sub(%rip), %rdi
	callq	*get_line(,%rax,8)
	movq	%rax, ref2_line(%rip)
	movslq	bipred1_access_method(%rip), %rax
	movq	ref_pic1_sub(%rip), %rdi
	movl	%ebx, 36(%rsp)          # 4-byte Spill
	movl	%ebx, %esi
	movl	%ebp, %edx
	callq	*get_line(,%rax,8)
	movq	%rax, ref1_line(%rip)
	testl	%r13d, %r13d
	jle	.LBB7_12
# BB#1:                                 # %.preheader81.lr.ph
	movslq	%r14d, %rcx
	subq	%rcx, %r12
	testl	%ecx, %ecx
	jle	.LBB7_9
# BB#2:                                 # %.preheader81.us.preheader
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movl	%r13d, 24(%rsp)         # 4-byte Spill
	movq	byte_abs(%rip), %rcx
	leal	-1(%r14), %edi
	andl	$-4, %edi
	movq	src_line(%rip), %rsi
	movq	ref2_line(%rip), %r13
	leaq	(%rdi,%rdi), %rbx
	leaq	8(%rbx,%r12,2), %r10
	leaq	8(%rdi,%rdi), %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	leaq	4(%rdi), %r8
	xorl	%ebx, %ebx
	pxor	%xmm0, %xmm0
	movdqa	.LCPI7_0(%rip), %xmm1   # xmm1 = [1,1,1,1]
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB7_3:                                # %.preheader81.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_4 Depth 2
	movl	%r15d, %r9d
	leaq	(%rax,%r8,2), %r11
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB7_4:                                #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi,%rdi,2), %xmm2    # xmm2 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	movq	(%rax,%rdi,2), %xmm3    # xmm3 = mem[0],zero
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	movq	(%r13,%rdi,2), %xmm4    # xmm4 = mem[0],zero
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	paddd	%xmm3, %xmm4
	paddd	%xmm1, %xmm4
	psrld	$1, %xmm4
	psubd	%xmm4, %xmm2
	pshufd	$78, %xmm2, %xmm3       # xmm3 = xmm2[2,3,0,1]
	movd	%xmm3, %rbp
	movd	%xmm2, %rdx
	movslq	%edx, %r15
	addl	(%rcx,%r15,4), %ebx
	sarq	$32, %rdx
	addl	(%rcx,%rdx,4), %ebx
	movslq	%ebp, %rdx
	addl	(%rcx,%rdx,4), %ebx
	sarq	$32, %rbp
	addl	(%rcx,%rbp,4), %ebx
	addq	$4, %rdi
	cmpl	%r14d, %edi
	jl	.LBB7_4
# BB#5:                                 # %._crit_edge99.us
                                        #   in Loop: Header=BB7_3 Depth=1
	addq	16(%rsp), %rsi          # 8-byte Folded Reload
	movl	%r9d, %r15d
	cmpl	%r15d, %ebx
	jge	.LBB7_21
# BB#6:                                 #   in Loop: Header=BB7_3 Depth=1
	addq	%r10, %r13
	addq	%r10, %rax
	incl	%r12d
	cmpl	24(%rsp), %r12d         # 4-byte Folded Reload
	jl	.LBB7_3
# BB#7:                                 # %._crit_edge109.loopexit
	movq	%rsi, src_line(%rip)
	movq	%rax, ref1_line(%rip)
	movq	%r13, ref2_line(%rip)
	movl	24(%rsp), %r13d         # 4-byte Reload
	movl	12(%rsp), %ebp          # 4-byte Reload
	cmpl	$0, ChromaMEEnable(%rip)
	jne	.LBB7_13
	jmp	.LBB7_35
.LBB7_9:                                # %.preheader81.preheader
	leaq	(%rax,%r12,2), %rax
	addq	%r12, %r12
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_10:                               # %.preheader81
                                        # =>This Inner Loop Header: Depth=1
	testl	%r15d, %r15d
	jle	.LBB7_35
# BB#11:                                #   in Loop: Header=BB7_10 Depth=1
	addq	%r12, ref2_line(%rip)
	movq	%rax, ref1_line(%rip)
	incl	%ecx
	addq	%r12, %rax
	cmpl	%r13d, %ecx
	jl	.LBB7_10
.LBB7_12:
	xorl	%ebx, %ebx
	cmpl	$0, ChromaMEEnable(%rip)
	je	.LBB7_35
.LBB7_13:
	movb	shift_cr_x(%rip), %cl
	sarl	%cl, %r14d
	movb	shift_cr_y(%rip), %cl
	sarl	%cl, %r13d
	movl	%ebp, %r12d
	movslq	img_cr_padded_size_x(%rip), %rbp
	movslq	%r14d, %rax
	subq	%rax, %rbp
	decl	%eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	2(%rax,%rax), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	512(%rax), %rax
	movq	%rax, src_line(%rip)
	movslq	bipred2_access_method(%rip), %rax
	movq	ref_pic2_sub+8(%rip), %rdi
	movl	120(%rsp), %esi
	movl	112(%rsp), %edx
	callq	*get_crline(,%rax,8)
	movq	%rax, ref2_line(%rip)
	movslq	bipred1_access_method(%rip), %rax
	movq	ref_pic1_sub+8(%rip), %rdi
	movl	36(%rsp), %esi          # 4-byte Reload
	movl	%r12d, 12(%rsp)         # 4-byte Spill
	movl	%r12d, %edx
	callq	*get_crline(,%rax,8)
	movq	%rax, ref1_line(%rip)
	testl	%r13d, %r13d
	jle	.LBB7_25
# BB#14:                                # %.preheader.lr.ph
	testl	%r14d, %r14d
	jle	.LBB7_22
# BB#15:                                # %.preheader.us.preheader
	movq	byte_abs(%rip), %rdi
	movq	src_line(%rip), %rcx
	movq	ref2_line(%rip), %rsi
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	leaq	(%rbp,%rbp), %rdx
	movq	24(%rsp), %rbp          # 8-byte Reload
	leaq	4(%rdx,%rbp,4), %r10
	leaq	4(,%rbp,4), %r9
	movl	%r14d, %edx
	xorl	%r11d, %r11d
	pxor	%xmm0, %xmm0
	movdqa	.LCPI7_1(%rip), %xmm1   # xmm1 = [1,1]
	movdqa	.LCPI7_2(%rip), %xmm2   # xmm2 = [4294967295,0,4294967295,0]
	.p2align	4, 0x90
.LBB7_16:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_17 Depth 2
	movl	%r13d, %r12d
	movq	16(%rsp), %rbp          # 8-byte Reload
	leaq	(%rax,%rbp,2), %r8
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB7_17:                               #   Parent Loop BB7_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	(%rcx,%r13,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	pshufd	$212, %xmm3, %xmm3      # xmm3 = xmm3[0,1,1,3]
	movd	(%rax,%r13,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	pshufd	$212, %xmm4, %xmm4      # xmm4 = xmm4[0,1,1,3]
	movd	(%rsi,%r13,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1],xmm5[2],xmm0[2],xmm5[3],xmm0[3]
	pshufd	$212, %xmm5, %xmm5      # xmm5 = xmm5[0,1,1,3]
	paddq	%xmm4, %xmm5
	paddq	%xmm1, %xmm5
	pand	%xmm2, %xmm5
	psrlq	$1, %xmm5
	psubq	%xmm5, %xmm3
	movd	%xmm3, %ebp
	movslq	%ebp, %rbp
	addl	(%rdi,%rbp,4), %ebx
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	movd	%xmm3, %ebp
	movslq	%ebp, %rbp
	addl	(%rdi,%rbp,4), %ebx
	incq	%r13
	cmpl	%r13d, %edx
	jne	.LBB7_17
# BB#18:                                # %._crit_edge.us
                                        #   in Loop: Header=BB7_16 Depth=1
	addq	%r9, %rcx
	cmpl	%r15d, %ebx
	jge	.LBB7_33
# BB#19:                                #   in Loop: Header=BB7_16 Depth=1
	addq	%r10, %rsi
	addq	%r10, %rax
	incl	%r11d
	movl	%r12d, %r13d
	cmpl	%r13d, %r11d
	jl	.LBB7_16
# BB#20:                                # %._crit_edge92.loopexit
	movq	%rcx, src_line(%rip)
	movq	%rax, ref1_line(%rip)
	movq	%rsi, ref2_line(%rip)
	movq	48(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB7_25
.LBB7_21:                               # %.thread.loopexit142
	leaq	(%r13,%r8,2), %rax
	movq	%rsi, src_line(%rip)
	movq	%r11, ref1_line(%rip)
	jmp	.LBB7_34
.LBB7_22:                               # %.preheader.preheader
	leaq	(%rax,%rbp,2), %rax
	leaq	(%rbp,%rbp), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB7_23:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r15d, %ebx
	jge	.LBB7_35
# BB#24:                                #   in Loop: Header=BB7_23 Depth=1
	addq	%rcx, ref2_line(%rip)
	movq	%rax, ref1_line(%rip)
	incl	%edx
	addq	%rcx, %rax
	cmpl	%r13d, %edx
	jl	.LBB7_23
.LBB7_25:                               # %._crit_edge92
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	$1024, %rax             # imm = 0x400
	movq	%rax, src_line(%rip)
	movslq	bipred2_access_method(%rip), %rax
	movq	ref_pic2_sub+16(%rip), %rdi
	movl	120(%rsp), %esi
	movl	112(%rsp), %edx
	callq	*get_crline(,%rax,8)
	movq	%rax, ref2_line(%rip)
	movslq	bipred1_access_method(%rip), %rax
	movq	ref_pic1_sub+16(%rip), %rdi
	movl	36(%rsp), %esi          # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	*get_crline(,%rax,8)
	testl	%r13d, %r13d
	movq	%rax, ref1_line(%rip)
	jle	.LBB7_35
# BB#26:                                # %.preheader.lr.ph.1
	testl	%r14d, %r14d
	jle	.LBB7_36
# BB#27:                                # %.preheader.us.preheader.1
	movq	byte_abs(%rip), %rdi
	movq	src_line(%rip), %rcx
	movq	ref2_line(%rip), %rsi
	addq	%rbp, %rbp
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	4(%rbp,%rdx,4), %r10
	leaq	4(,%rdx,4), %r9
	movl	%r14d, %r14d
	xorl	%r11d, %r11d
	pxor	%xmm0, %xmm0
	movdqa	.LCPI7_1(%rip), %xmm1   # xmm1 = [1,1]
	movdqa	.LCPI7_2(%rip), %xmm2   # xmm2 = [4294967295,0,4294967295,0]
	.p2align	4, 0x90
.LBB7_28:                               # %.preheader.us.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_29 Depth 2
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rax,%rdx,2), %r8
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB7_29:                               #   Parent Loop BB7_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	(%rcx,%rdx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	pshufd	$212, %xmm3, %xmm3      # xmm3 = xmm3[0,1,1,3]
	movd	(%rax,%rdx,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	pshufd	$212, %xmm4, %xmm4      # xmm4 = xmm4[0,1,1,3]
	movd	(%rsi,%rdx,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1],xmm5[2],xmm0[2],xmm5[3],xmm0[3]
	pshufd	$212, %xmm5, %xmm5      # xmm5 = xmm5[0,1,1,3]
	paddq	%xmm4, %xmm5
	paddq	%xmm1, %xmm5
	pand	%xmm2, %xmm5
	psrlq	$1, %xmm5
	psubq	%xmm5, %xmm3
	movd	%xmm3, %ebp
	movslq	%ebp, %rbp
	addl	(%rdi,%rbp,4), %ebx
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	movd	%xmm3, %ebp
	movslq	%ebp, %rbp
	addl	(%rdi,%rbp,4), %ebx
	incq	%rdx
	cmpl	%edx, %r14d
	jne	.LBB7_29
# BB#30:                                # %._crit_edge.us.1
                                        #   in Loop: Header=BB7_28 Depth=1
	addq	%r9, %rcx
	cmpl	%r15d, %ebx
	jge	.LBB7_33
# BB#31:                                #   in Loop: Header=BB7_28 Depth=1
	addq	%r10, %rsi
	addq	%r10, %rax
	incl	%r11d
	cmpl	%r13d, %r11d
	jl	.LBB7_28
# BB#32:                                # %._crit_edge92.loopexit.1
	movq	%rcx, src_line(%rip)
	movq	%rax, ref1_line(%rip)
	movq	%rsi, ref2_line(%rip)
	jmp	.LBB7_35
.LBB7_33:
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rsi,%rax,2), %rax
	movq	%rcx, src_line(%rip)
	movq	%r8, ref1_line(%rip)
.LBB7_34:                               # %.thread
	movq	%rax, ref2_line(%rip)
.LBB7_35:                               # %.thread
	movl	%ebx, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_36:                               # %.preheader.preheader.1
	leaq	(%rax,%rbp,2), %rax
	addq	%rbp, %rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_37:                               # %.preheader.1
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r15d, %ebx
	jge	.LBB7_35
# BB#38:                                #   in Loop: Header=BB7_37 Depth=1
	addq	%rbp, ref2_line(%rip)
	movq	%rax, ref1_line(%rip)
	incl	%ecx
	addq	%rbp, %rax
	cmpl	%r13d, %ecx
	jl	.LBB7_37
	jmp	.LBB7_35
.Lfunc_end7:
	.size	computeBiPredSAD1, .Lfunc_end7-computeBiPredSAD1
	.cfi_endproc

	.globl	computeBiPredSAD2
	.p2align	4, 0x90
	.type	computeBiPredSAD2,@function
computeBiPredSAD2:                      # @computeBiPredSAD2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi73:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi75:
	.cfi_def_cfa_offset 176
.Lcfi76:
	.cfi_offset %rbx, -56
.Lcfi77:
	.cfi_offset %r12, -48
.Lcfi78:
	.cfi_offset %r13, -40
.Lcfi79:
	.cfi_offset %r14, -32
.Lcfi80:
	.cfi_offset %r15, -24
.Lcfi81:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movl	%r14d, 20(%rsp)         # 4-byte Spill
	movl	%r8d, %r15d
	movl	%r15d, 16(%rsp)         # 4-byte Spill
	movl	%ecx, 4(%rsp)           # 4-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%esi, %r13d
	movl	184(%rsp), %esi
	movl	176(%rsp), %edx
	movl	luma_log_weight_denom(%rip), %ebp
	incl	%ebp
	movl	wp_luma_round(%rip), %ebx
	addl	%ebx, %ebx
	movslq	img_padded_size_x(%rip), %r12
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	movq	%rdi, src_line(%rip)
	movslq	bipred2_access_method(%rip), %rax
	movq	ref_pic2_sub(%rip), %rdi
	callq	*get_line(,%rax,8)
	movq	%rax, ref2_line(%rip)
	movslq	bipred1_access_method(%rip), %rax
	movq	ref_pic1_sub(%rip), %rdi
	movl	%r14d, %esi
	movl	%r15d, %edx
	callq	*get_line(,%rax,8)
	movq	%rax, ref1_line(%rip)
	testl	%r13d, %r13d
	movl	%ebx, 44(%rsp)          # 4-byte Spill
	movl	%ebp, 40(%rsp)          # 4-byte Spill
	jle	.LBB8_1
# BB#2:                                 # %.preheader127.lr.ph
	movq	8(%rsp), %rdi           # 8-byte Reload
	movslq	%edi, %r10
	subq	%r10, %r12
	movswl	weight1(%rip), %edx
	movswl	weight2(%rip), %r8d
	movq	img(%rip), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movswl	offsetBi(%rip), %ecx
	movq	byte_abs(%rip), %rsi
	decl	%r10d
	andl	$-4, %r10d
	addq	$4, %r10
	movd	%edx, %xmm0
	pshufd	$0, %xmm0, %xmm8        # xmm8 = xmm0[0,0,0,0]
	movd	%ebx, %xmm0
	pshufd	$0, %xmm0, %xmm9        # xmm9 = xmm0[0,0,0,0]
	movd	%r8d, %xmm0
	pshufd	$0, %xmm0, %xmm10       # xmm10 = xmm0[0,0,0,0]
	movd	%ebp, %xmm0
	movd	%ecx, %xmm3
	pshufd	$0, %xmm3, %xmm11       # xmm11 = xmm3[0,0,0,0]
	xorl	%r15d, %r15d
	pxor	%xmm4, %xmm4
	xorps	%xmm5, %xmm5
	movss	%xmm0, %xmm5            # xmm5 = xmm0[0],xmm5[1,2,3]
	pshufd	$245, %xmm8, %xmm6      # xmm6 = xmm8[1,1,3,3]
	pshufd	$245, %xmm10, %xmm7     # xmm7 = xmm10[1,1,3,3]
	xorl	%r11d, %r11d
	movq	%rdi, %rdx
	movq	%r12, %r14
	.p2align	4, 0x90
.LBB8_3:                                # %.preheader127
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_5 Depth 2
	testl	%edx, %edx
	jle	.LBB8_7
# BB#4:                                 # %.lr.ph144
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	ref2_line(%rip), %rbx
	movq	src_line(%rip), %r12
	leaq	(%rbx,%r10,2), %r9
	movq	24(%rsp), %rcx          # 8-byte Reload
	movd	15520(%rcx), %xmm0      # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_5:                                #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rbp,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1],xmm1[2],xmm4[2],xmm1[3],xmm4[3]
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm8, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pmuludq	%xmm6, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movq	(%rbx,%rbp,2), %xmm2    # xmm2 = mem[0],zero
	punpcklwd	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1],xmm2[2],xmm4[2],xmm2[3],xmm4[3]
	pshufd	$245, %xmm2, %xmm3      # xmm3 = xmm2[1,1,3,3]
	pmuludq	%xmm10, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pmuludq	%xmm7, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	paddd	%xmm9, %xmm1
	paddd	%xmm2, %xmm1
	psrad	%xmm5, %xmm1
	paddd	%xmm11, %xmm1
	movdqa	%xmm1, %xmm2
	pcmpgtd	%xmm4, %xmm2
	pand	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	pcmpgtd	%xmm2, %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm0, %xmm1
	por	%xmm2, %xmm1
	movq	(%r12,%rbp,2), %xmm2    # xmm2 = mem[0],zero
	punpcklwd	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1],xmm2[2],xmm4[2],xmm2[3],xmm4[3]
	psubd	%xmm1, %xmm2
	pshufd	$78, %xmm2, %xmm1       # xmm1 = xmm2[2,3,0,1]
	movd	%xmm1, %rcx
	movd	%xmm2, %rdi
	movslq	%edi, %r8
	addl	(%rsi,%r8,4), %r15d
	sarq	$32, %rdi
	addl	(%rsi,%rdi,4), %r15d
	movslq	%ecx, %rdi
	addl	(%rsi,%rdi,4), %r15d
	sarq	$32, %rcx
	addl	(%rsi,%rcx,4), %r15d
	addq	$4, %rbp
	cmpl	%edx, %ebp
	jl	.LBB8_5
# BB#6:                                 # %._crit_edge145
                                        #   in Loop: Header=BB8_3 Depth=1
	leaq	(%rax,%r10,2), %rax
	leaq	(%r12,%r10,2), %rcx
	movq	%rax, ref1_line(%rip)
	movq	%r9, ref2_line(%rip)
	movq	%rcx, src_line(%rip)
.LBB8_7:                                #   in Loop: Header=BB8_3 Depth=1
	cmpl	4(%rsp), %r15d          # 4-byte Folded Reload
	jge	.LBB8_24
# BB#8:                                 #   in Loop: Header=BB8_3 Depth=1
	leaq	(%r14,%r14), %rcx
	addq	%rcx, ref2_line(%rip)
	leaq	(%rax,%r14,2), %rax
	movq	%rax, ref1_line(%rip)
	incl	%r11d
	cmpl	%r13d, %r11d
	jl	.LBB8_3
	jmp	.LBB8_9
.LBB8_1:
	xorl	%r15d, %r15d
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB8_9:                                # %._crit_edge155
	cmpl	$0, ChromaMEEnable(%rip)
	je	.LBB8_24
# BB#10:
	movb	shift_cr_x(%rip), %cl
	sarl	%cl, %edx
	movb	shift_cr_y(%rip), %cl
	sarl	%cl, %r13d
	movslq	img_cr_padded_size_x(%rip), %rcx
	movslq	%edx, %rax
	subq	%rax, %rcx
	decl	%eax
	leaq	2(%rax,%rax), %rsi
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rcx), %rbp
	leaq	4(,%rax,4), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	leaq	4(%rbp,%rax,4), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	movl	20(%rsp), %ebx          # 4-byte Reload
	movl	16(%rsp), %r12d         # 4-byte Reload
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%r13d, 32(%rsp)         # 4-byte Spill
	movq	%rbp, 64(%rsp)          # 8-byte Spill
.LBB8_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_14 Depth 2
                                        #     Child Loop BB8_17 Depth 2
                                        #       Child Loop BB8_18 Depth 3
	movl	$256, %eax              # imm = 0x100
	movl	%r14d, %ecx
	shll	%cl, %eax
	cltq
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,2), %rax
	movq	%rax, src_line(%rip)
	movslq	bipred2_access_method(%rip), %rax
	movq	ref_pic2_sub+8(,%r14,8), %rdi
	movl	184(%rsp), %esi
	movl	176(%rsp), %edx
	callq	*get_crline(,%rax,8)
	movq	%rax, ref2_line(%rip)
	movslq	bipred1_access_method(%rip), %rax
	movq	ref_pic1_sub+8(,%r14,8), %rdi
	movl	%ebx, %esi
	movl	%r12d, %edx
	callq	*get_crline(,%rax,8)
	movq	%rax, ref1_line(%rip)
	testl	%r13d, %r13d
	jle	.LBB8_22
# BB#12:                                # %.preheader.lr.ph
                                        #   in Loop: Header=BB8_11 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	testl	%edx, %edx
	jle	.LBB8_13
# BB#16:                                # %.preheader.us.preheader
                                        #   in Loop: Header=BB8_11 Depth=1
	movq	byte_abs(%rip), %rdx
	movq	img(%rip), %rcx
	movl	15524(%rcx), %r8d
	movq	ref2_line(%rip), %rbp
	movq	src_line(%rip), %r9
	movswl	weight1_cr(%r14,%r14), %r12d
	movswl	weight2_cr(%r14,%r14), %esi
	movq	%r14, 80(%rsp)          # 8-byte Spill
	movswl	offsetBi_cr(%r14,%r14), %ebx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_17:                               # %.preheader.us
                                        #   Parent Loop BB8_11 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_18 Depth 3
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rbp,%rcx,2), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB8_18:                               #   Parent Loop BB8_11 Depth=1
                                        #     Parent Loop BB8_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	(%rax,%r11,4), %r10d
	imull	%r12d, %r10d
	movq	56(%rsp), %r14          # 8-byte Reload
	movzwl	(%r14,%r11,4), %ecx
	imull	%esi, %ecx
	movl	44(%rsp), %r13d         # 4-byte Reload
	addl	%r13d, %r10d
	addl	%ecx, %r10d
	movl	40(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %ecx
	sarl	%cl, %r10d
	addl	%ebx, %r10d
	movl	$0, %ecx
	cmovsl	%ecx, %r10d
	cmpl	%r8d, %r10d
	cmovgl	%r8d, %r10d
	movq	24(%rsp), %r9           # 8-byte Reload
	movzwl	(%r9,%r11,4), %ecx
	movslq	%r10d, %rdi
	subq	%rdi, %rcx
	addl	(%rdx,%rcx,4), %r15d
	movzwl	2(%rax,%r11,4), %edi
	imull	%r12d, %edi
	movzwl	2(%r14,%r11,4), %ecx
	imull	%esi, %ecx
	addl	%r13d, %edi
	addl	%ecx, %edi
	movl	%ebp, %ecx
	sarl	%cl, %edi
	addl	%ebx, %edi
	movl	$0, %ecx
	cmovsl	%ecx, %edi
	cmpl	%r8d, %edi
	cmovgl	%r8d, %edi
	movzwl	2(%r9,%r11,4), %ecx
	movslq	%edi, %rdi
	subq	%rdi, %rcx
	addl	(%rdx,%rcx,4), %r15d
	incq	%r11
	cmpl	%r11d, 8(%rsp)          # 4-byte Folded Reload
	jne	.LBB8_18
# BB#19:                                # %._crit_edge.us
                                        #   in Loop: Header=BB8_17 Depth=2
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	addq	112(%rsp), %rcx         # 8-byte Folded Reload
	cmpl	4(%rsp), %r15d          # 4-byte Folded Reload
	movq	%rcx, %r9
	jge	.LBB8_23
# BB#20:                                #   in Loop: Header=BB8_17 Depth=2
	movq	96(%rsp), %r10          # 8-byte Reload
	addq	%r10, %rbp
	addq	%r10, %rax
	movl	36(%rsp), %ecx          # 4-byte Reload
	incl	%ecx
	movl	32(%rsp), %r13d         # 4-byte Reload
	cmpl	%r13d, %ecx
	jl	.LBB8_17
# BB#21:                                # %._crit_edge138.loopexit
                                        #   in Loop: Header=BB8_11 Depth=1
	movq	%rax, ref1_line(%rip)
	movq	%rbp, ref2_line(%rip)
	movq	%r9, src_line(%rip)
	movl	20(%rsp), %ebx          # 4-byte Reload
	movl	16(%rsp), %r12d         # 4-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	80(%rsp), %r14          # 8-byte Reload
	jmp	.LBB8_22
	.p2align	4, 0x90
.LBB8_13:                               # %.preheader.preheader
                                        #   in Loop: Header=BB8_11 Depth=1
	movq	72(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,2), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_14:                               # %.preheader
                                        #   Parent Loop BB8_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	4(%rsp), %r15d          # 4-byte Folded Reload
	jge	.LBB8_24
# BB#15:                                #   in Loop: Header=BB8_14 Depth=2
	addq	%rbp, ref2_line(%rip)
	movq	%rax, ref1_line(%rip)
	incl	%ecx
	addq	%rbp, %rax
	cmpl	%r13d, %ecx
	jl	.LBB8_14
	.p2align	4, 0x90
.LBB8_22:                               # %._crit_edge138
                                        #   in Loop: Header=BB8_11 Depth=1
	incq	%r14
	cmpq	$2, %r14
	jl	.LBB8_11
	jmp	.LBB8_24
.LBB8_23:                               # %.thread.loopexit
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,2), %rax
	movq	%rax, ref1_line(%rip)
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%rax, ref2_line(%rip)
	movq	%r9, src_line(%rip)
.LBB8_24:                               # %.thread
	movl	%r15d, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	computeBiPredSAD2, .Lfunc_end8-computeBiPredSAD2
	.cfi_endproc

	.globl	computeSATD
	.p2align	4, 0x90
	.type	computeSATD,@function
computeSATD:                            # @computeSATD
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi88:
	.cfi_def_cfa_offset 128
.Lcfi89:
	.cfi_offset %rbx, -56
.Lcfi90:
	.cfi_offset %r12, -48
.Lcfi91:
	.cfi_offset %r13, -40
.Lcfi92:
	.cfi_offset %r14, -32
.Lcfi93:
	.cfi_offset %r15, -24
.Lcfi94:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movl	%ecx, 28(%rsp)          # 4-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movslq	img_padded_size_x(%rip), %rbx
	leal	(,%rsi,4), %ecx
	movq	%r9, 16(%rsp)           # 8-byte Spill
	leal	(%r9,%rsi,4), %esi
	cmpl	$0, test8x8transform(%rip)
	je	.LBB9_1
# BB#8:
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.LBB9_15
# BB#9:                                 # %.preheader91.lr.ph
	testl	%edx, %edx
	jle	.LBB9_15
# BB#10:                                # %.preheader91.us.preheader
	leal	(,%rdx,8), %eax
	leal	-8(%rdx), %ecx
	addl	$-8, %ebx
	movslq	%eax, %rbp
	movslq	%ebx, %r13
	movslq	%ecx, %rax
	leaq	64(,%r13,8), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movslq	%edx, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	addq	%rbp, %rbp
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	leaq	16(%rax,%rax), %r15
	xorl	%eax, %eax
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movl	%esi, 8(%rsp)           # 4-byte Spill
.LBB9_11:                               # %.preheader91.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_13 Depth 2
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %r14
	movl	%r8d, %ebp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB9_13:                               #   Parent Loop BB9_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebx
	movslq	ref_access_method(%rip), %rax
	movq	ref_pic_sub(%rip), %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%ebp, %edx
	callq	*get_line(,%rax,8)
	movq	%rax, ref_line(%rip)
	movq	%r14, src_line(%rip)
	movq	(%r14), %xmm0           # xmm0 = mem[0],zero
	pxor	%xmm2, %xmm2
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rax), %xmm1           # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff(%rip)
	movq	8(%r14), %xmm0          # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	leaq	(%rax,%r13,2), %rcx
	movq	8(%rax), %xmm1          # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+16(%rip)
	leaq	(%r14,%r15), %rsi
	movq	(%r14,%r15), %xmm0      # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	16(%rax,%r13,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+32(%rip)
	movq	8(%r14,%r15), %xmm0     # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	leaq	16(%rcx,%r13,2), %rdx
	movq	24(%rax,%r13,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+48(%rip)
	movq	(%r15,%rsi), %xmm0      # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	32(%rcx,%r13,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+64(%rip)
	movq	8(%r15,%rsi), %xmm0     # xmm0 = mem[0],zero
	leaq	(%rsi,%r15), %rdi
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	leaq	16(%rdx,%r13,2), %rsi
	movq	40(%rcx,%r13,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+80(%rip)
	movq	(%r15,%rdi), %xmm0      # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	32(%rdx,%r13,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+96(%rip)
	movq	8(%r15,%rdi), %xmm0     # xmm0 = mem[0],zero
	leaq	(%rdi,%r15), %rdi
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	leaq	16(%rsi,%r13,2), %rcx
	movq	40(%rdx,%r13,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+112(%rip)
	movq	(%r15,%rdi), %xmm0      # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	32(%rsi,%r13,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+128(%rip)
	movq	8(%r15,%rdi), %xmm0     # xmm0 = mem[0],zero
	leaq	(%rdi,%r15), %rdi
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	leaq	16(%rcx,%r13,2), %rdx
	movq	40(%rsi,%r13,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+144(%rip)
	movq	(%r15,%rdi), %xmm0      # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	32(%rcx,%r13,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+160(%rip)
	movq	8(%r15,%rdi), %xmm0     # xmm0 = mem[0],zero
	leaq	(%rdi,%r15), %rdi
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	leaq	16(%rdx,%r13,2), %rsi
	movq	40(%rcx,%r13,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+176(%rip)
	movq	(%r15,%rdi), %xmm0      # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	32(%rdx,%r13,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+192(%rip)
	movq	8(%r15,%rdi), %xmm0     # xmm0 = mem[0],zero
	leaq	(%rdi,%r15), %rcx
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	40(%rdx,%r13,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+208(%rip)
	movq	(%r15,%rcx), %xmm0      # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	32(%rsi,%r13,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+224(%rip)
	movq	8(%r15,%rcx), %xmm0     # xmm0 = mem[0],zero
	leaq	(%rcx,%r15), %rcx
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	40(%rsi,%r13,2), %xmm1  # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+240(%rip)
	movq	56(%rsp), %rdx          # 8-byte Reload
	leaq	(%rax,%rdx,2), %rax
	addq	%r15, %rcx
	movq	%rcx, src_line(%rip)
	movq	%rax, ref_line(%rip)
	movl	$diff, %edi
	callq	HadamardSAD8x8
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	addl	%ebx, %eax
	cmpl	28(%rsp), %eax          # 4-byte Folded Reload
	jg	.LBB9_15
# BB#12:                                #   in Loop: Header=BB9_13 Depth=2
	addq	$8, %r12
	addl	$32, %ebp
	addq	$16, %r14
	cmpq	48(%rsp), %r12          # 8-byte Folded Reload
	jl	.LBB9_13
# BB#14:                                # %._crit_edge118.us
                                        #   in Loop: Header=BB9_11 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	addl	$32, %ecx
	movq	32(%rsp), %rdi          # 8-byte Reload
	addq	40(%rsp), %rdi          # 8-byte Folded Reload
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	8(%rsp), %esi           # 4-byte Reload
	cmpl	%esi, %ecx
	movl	12(%rsp), %r8d          # 4-byte Reload
	jl	.LBB9_11
	jmp	.LBB9_15
.LBB9_1:
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.LBB9_15
# BB#2:                                 # %.preheader.lr.ph
	testl	%edx, %edx
	jle	.LBB9_15
# BB#3:                                 # %.preheader.us.preheader
	leal	(,%rdx,4), %eax
	leal	-4(%rdx), %ecx
	leaq	-4(%rbx), %rbp
	cltq
	movslq	%ecx, %rcx
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	leaq	16(,%rbp,4), %rbp
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movslq	%edx, %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	addq	%rax, %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	8(%rcx,%rcx), %r15
	xorl	%eax, %eax
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movl	%esi, 8(%rsp)           # 4-byte Spill
.LBB9_4:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_6 Depth 2
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %r13
	movl	%r8d, %ebp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB9_6:                                #   Parent Loop BB9_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %r14d
	movslq	ref_access_method(%rip), %rax
	movq	ref_pic_sub(%rip), %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%ebp, %edx
	callq	*get_line(,%rax,8)
	movq	%rax, ref_line(%rip)
	movq	%r13, src_line(%rip)
	movq	(%r13), %xmm0           # xmm0 = mem[0],zero
	pxor	%xmm2, %xmm2
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	56(%rsp), %rsi          # 8-byte Reload
	leaq	(%rax,%rsi,2), %rdx
	movq	(%rax), %xmm1           # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff(%rip)
	leaq	(%r13,%r15), %rcx
	movq	(%r13,%r15), %xmm0      # xmm0 = mem[0],zero
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rax,%rbx,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+16(%rip)
	leaq	16(%rdx,%rsi,2), %rsi
	movq	(%r15,%rcx), %xmm0      # xmm0 = mem[0],zero
	addq	%r15, %rcx
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	8(%rdx,%rbx,2), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+32(%rip)
	movq	(%r15,%rcx), %xmm0      # xmm0 = mem[0],zero
	addq	%r15, %rcx
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	movq	(%rsi,%rbx,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	psubd	%xmm1, %xmm0
	movdqa	%xmm0, diff+48(%rip)
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	(%rax,%rdx,2), %rax
	addq	%r15, %rcx
	movq	%rcx, src_line(%rip)
	movq	%rax, ref_line(%rip)
	movl	$diff, %edi
	callq	HadamardSAD4x4
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	addl	%r14d, %eax
	cmpl	28(%rsp), %eax          # 4-byte Folded Reload
	jg	.LBB9_15
# BB#5:                                 #   in Loop: Header=BB9_6 Depth=2
	addq	$4, %r12
	addl	$16, %ebp
	addq	$8, %r13
	cmpq	40(%rsp), %r12          # 8-byte Folded Reload
	jl	.LBB9_6
# BB#7:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	addl	$16, %ecx
	movq	32(%rsp), %rdi          # 8-byte Reload
	addq	64(%rsp), %rdi          # 8-byte Folded Reload
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	8(%rsp), %esi           # 4-byte Reload
	cmpl	%esi, %ecx
	movl	12(%rsp), %r8d          # 4-byte Reload
	jl	.LBB9_4
.LBB9_15:                               # %.loopexit
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	computeSATD, .Lfunc_end9-computeSATD
	.cfi_endproc

	.globl	computeSATDWP
	.p2align	4, 0x90
	.type	computeSATDWP,@function
computeSATDWP:                          # @computeSATDWP
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi98:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi99:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi101:
	.cfi_def_cfa_offset 176
.Lcfi102:
	.cfi_offset %rbx, -56
.Lcfi103:
	.cfi_offset %r12, -48
.Lcfi104:
	.cfi_offset %r13, -40
.Lcfi105:
	.cfi_offset %r14, -32
.Lcfi106:
	.cfi_offset %r15, -24
.Lcfi107:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%r8, 112(%rsp)          # 8-byte Spill
	movl	%ecx, 28(%rsp)          # 4-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, (%rsp)            # 8-byte Spill
	movslq	img_padded_size_x(%rip), %rax
	leal	(,%rsi,4), %ecx
	movq	%r9, 16(%rsp)           # 8-byte Spill
	leal	(%r9,%rsi,4), %esi
	movl	%esi, 24(%rsp)          # 4-byte Spill
	cmpl	$0, test8x8transform(%rip)
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	je	.LBB10_10
# BB#1:
	testl	%ecx, %ecx
	jle	.LBB10_20
# BB#2:                                 # %.preheader105.lr.ph
	leal	(,%rdx,8), %ecx
	movslq	%ecx, %rsi
	leaq	-64(,%rax,8), %rcx
	addq	$64, %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movslq	%edx, %rdi
	leaq	-64(,%rdi,8), %rcx
	addq	$64, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	14(%rcx), %rbp
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	leaq	(%rsi,%rsi), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	leaq	-16(%rdi,%rdi), %r12
	addq	$16, %r12
	leaq	-16(%rax,%rax), %r15
	addq	$16, %r15
	xorl	%r13d, %r13d
	xorl	%eax, %eax
.LBB10_3:                               # %.preheader105
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_5 Depth 2
                                        #       Child Loop BB10_6 Depth 3
	testl	%edx, %edx
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	jle	.LBB10_9
# BB#4:                                 # %.lr.ph131.preheader
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	%rbp, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB10_5:                               # %.lr.ph131
                                        #   Parent Loop BB10_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_6 Depth 3
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movslq	ref_access_method(%rip), %rax
	movq	ref_pic_sub(%rip), %rdi
	movq	112(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%r14,4), %edx
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*get_line(,%rax,8)
	movq	%rax, ref_line(%rip)
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	(%rcx,%r14,2), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	%rcx, src_line(%rip)
	movq	img(%rip), %r10
	movl	weight_luma(%rip), %r11d
	movl	wp_luma_round(%rip), %r9d
	movl	luma_log_weight_denom(%rip), %ecx
	movl	offset_luma(%rip), %edx
	movq	72(%rsp), %rsi          # 8-byte Reload
	leaq	(%rax,%rsi,2), %rsi
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	addq	$14, %rax
	xorl	%ebp, %ebp
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB10_6:                               #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	15520(%r10), %edi
	movzwl	-14(%rax), %r8d
	imull	%r11d, %r8d
	addl	%r9d, %r8d
	sarl	%cl, %r8d
	addl	%edx, %r8d
	cmovsl	%r13d, %r8d
	cmpl	%edi, %r8d
	cmovgl	%edi, %r8d
	movzwl	-14(%rsi), %edi
	subl	%r8d, %edi
	movl	%edi, diff(%rbp)
	movl	15520(%r10), %edi
	movzwl	-12(%rax), %ebx
	imull	%r11d, %ebx
	addl	%r9d, %ebx
	sarl	%cl, %ebx
	addl	%edx, %ebx
	cmovsl	%r13d, %ebx
	cmpl	%edi, %ebx
	cmovgl	%edi, %ebx
	movzwl	-12(%rsi), %edi
	subl	%ebx, %edi
	movl	%edi, diff+4(%rbp)
	movl	15520(%r10), %edi
	movzwl	-10(%rax), %ebx
	imull	%r11d, %ebx
	addl	%r9d, %ebx
	sarl	%cl, %ebx
	addl	%edx, %ebx
	cmovsl	%r13d, %ebx
	cmpl	%edi, %ebx
	cmovgl	%edi, %ebx
	movzwl	-10(%rsi), %edi
	subl	%ebx, %edi
	movl	%edi, diff+8(%rbp)
	movl	15520(%r10), %edi
	movzwl	-8(%rax), %ebx
	imull	%r11d, %ebx
	addl	%r9d, %ebx
	sarl	%cl, %ebx
	addl	%edx, %ebx
	cmovsl	%r13d, %ebx
	cmpl	%edi, %ebx
	cmovgl	%edi, %ebx
	movzwl	-8(%rsi), %edi
	subl	%ebx, %edi
	movl	%edi, diff+12(%rbp)
	movl	15520(%r10), %edi
	movzwl	-6(%rax), %ebx
	imull	%r11d, %ebx
	addl	%r9d, %ebx
	sarl	%cl, %ebx
	addl	%edx, %ebx
	cmovsl	%r13d, %ebx
	cmpl	%edi, %ebx
	cmovgl	%edi, %ebx
	movzwl	-6(%rsi), %edi
	subl	%ebx, %edi
	movl	%edi, diff+16(%rbp)
	movl	15520(%r10), %edi
	movzwl	-4(%rax), %ebx
	imull	%r11d, %ebx
	addl	%r9d, %ebx
	sarl	%cl, %ebx
	addl	%edx, %ebx
	cmovsl	%r13d, %ebx
	cmpl	%edi, %ebx
	cmovgl	%edi, %ebx
	movzwl	-4(%rsi), %edi
	subl	%ebx, %edi
	movl	%edi, diff+20(%rbp)
	movl	15520(%r10), %edi
	movzwl	-2(%rax), %ebx
	imull	%r11d, %ebx
	addl	%r9d, %ebx
	sarl	%cl, %ebx
	addl	%edx, %ebx
	cmovsl	%r13d, %ebx
	cmpl	%edi, %ebx
	cmovgl	%edi, %ebx
	movzwl	-2(%rsi), %edi
	subl	%ebx, %edi
	movl	%edi, diff+24(%rbp)
	movl	15520(%r10), %edi
	movzwl	(%rax), %ebx
	imull	%r11d, %ebx
	addl	%r9d, %ebx
	sarl	%cl, %ebx
	addl	%edx, %ebx
	cmovsl	%r13d, %ebx
	cmpl	%edi, %ebx
	cmovgl	%edi, %ebx
	movzwl	(%rsi), %edi
	subl	%ebx, %edi
	movl	%edi, diff+28(%rbp)
	addq	%r12, %rsi
	addq	%r15, %rax
	addq	$32, %rbp
	cmpl	$256, %ebp              # imm = 0x100
	jne	.LBB10_6
# BB#7:                                 #   in Loop: Header=BB10_5 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,2), %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, ref_line(%rip)
	movq	%rax, src_line(%rip)
	movl	$diff, %edi
	callq	HadamardSAD8x8
	addl	12(%rsp), %eax          # 4-byte Folded Reload
	cmpl	28(%rsp), %eax          # 4-byte Folded Reload
	jg	.LBB10_21
# BB#8:                                 #   in Loop: Header=BB10_5 Depth=2
	addq	$8, %r14
	movq	80(%rsp), %rbx          # 8-byte Reload
	addq	$16, %rbx
	cmpq	56(%rsp), %r14          # 8-byte Folded Reload
	jl	.LBB10_5
.LBB10_9:                               # %._crit_edge132
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	(%rcx,%rdx,2), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	16(%rsp), %rcx          # 8-byte Reload
	addl	$32, %ecx
	movq	48(%rsp), %rbp          # 8-byte Reload
	addq	32(%rsp), %rbp          # 8-byte Folded Reload
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	cmpl	24(%rsp), %ecx          # 4-byte Folded Reload
	movq	104(%rsp), %rdx         # 8-byte Reload
	jl	.LBB10_3
	jmp	.LBB10_21
.LBB10_10:
	testl	%ecx, %ecx
	jle	.LBB10_20
# BB#11:                                # %.preheader.lr.ph
	leal	(,%rdx,4), %ecx
	movslq	%ecx, %rsi
	leaq	-16(,%rax,4), %rcx
	addq	$16, %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movslq	%edx, %rdi
	leaq	-16(,%rdi,4), %rcx
	addq	$16, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	6(%rcx), %rbp
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	leaq	(%rsi,%rsi), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	leaq	-8(%rdi,%rdi), %r13
	addq	$8, %r13
	leaq	-8(%rax,%rax), %r14
	addq	$8, %r14
	xorl	%r12d, %r12d
	xorl	%eax, %eax
.LBB10_12:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_14 Depth 2
                                        #       Child Loop BB10_15 Depth 3
	testl	%edx, %edx
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	jle	.LBB10_18
# BB#13:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB10_12 Depth=1
	movq	%rbp, %rbx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB10_14:                              # %.lr.ph
                                        #   Parent Loop BB10_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_15 Depth 3
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movslq	ref_access_method(%rip), %rax
	movq	ref_pic_sub(%rip), %rdi
	movq	112(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%r15,4), %edx
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*get_line(,%rax,8)
	movq	%rax, ref_line(%rip)
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	(%rcx,%r15,2), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	%rcx, src_line(%rip)
	movq	img(%rip), %r10
	movl	weight_luma(%rip), %r11d
	movl	wp_luma_round(%rip), %r9d
	movl	luma_log_weight_denom(%rip), %ecx
	movl	offset_luma(%rip), %edx
	movq	72(%rsp), %rsi          # 8-byte Reload
	leaq	(%rax,%rsi,2), %rsi
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	addq	$6, %rax
	xorl	%ebp, %ebp
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB10_15:                              #   Parent Loop BB10_12 Depth=1
                                        #     Parent Loop BB10_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	15520(%r10), %edi
	movzwl	-6(%rax), %r8d
	imull	%r11d, %r8d
	addl	%r9d, %r8d
	sarl	%cl, %r8d
	addl	%edx, %r8d
	cmovsl	%r12d, %r8d
	cmpl	%edi, %r8d
	cmovgl	%edi, %r8d
	movzwl	-6(%rsi), %edi
	subl	%r8d, %edi
	movl	%edi, diff(%rbp)
	movl	15520(%r10), %edi
	movzwl	-4(%rax), %ebx
	imull	%r11d, %ebx
	addl	%r9d, %ebx
	sarl	%cl, %ebx
	addl	%edx, %ebx
	cmovsl	%r12d, %ebx
	cmpl	%edi, %ebx
	cmovgl	%edi, %ebx
	movzwl	-4(%rsi), %edi
	subl	%ebx, %edi
	movl	%edi, diff+4(%rbp)
	movl	15520(%r10), %edi
	movzwl	-2(%rax), %ebx
	imull	%r11d, %ebx
	addl	%r9d, %ebx
	sarl	%cl, %ebx
	addl	%edx, %ebx
	cmovsl	%r12d, %ebx
	cmpl	%edi, %ebx
	cmovgl	%edi, %ebx
	movzwl	-2(%rsi), %edi
	subl	%ebx, %edi
	movl	%edi, diff+8(%rbp)
	movl	15520(%r10), %edi
	movzwl	(%rax), %ebx
	imull	%r11d, %ebx
	addl	%r9d, %ebx
	sarl	%cl, %ebx
	addl	%edx, %ebx
	cmovsl	%r12d, %ebx
	cmpl	%edi, %ebx
	cmovgl	%edi, %ebx
	movzwl	(%rsi), %edi
	subl	%ebx, %edi
	movl	%edi, diff+12(%rbp)
	addq	%r13, %rsi
	addq	%r14, %rax
	addq	$16, %rbp
	cmpl	$64, %ebp
	jne	.LBB10_15
# BB#16:                                #   in Loop: Header=BB10_14 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,2), %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, ref_line(%rip)
	movq	%rax, src_line(%rip)
	movl	$diff, %edi
	callq	HadamardSAD4x4
	addl	12(%rsp), %eax          # 4-byte Folded Reload
	cmpl	28(%rsp), %eax          # 4-byte Folded Reload
	jg	.LBB10_21
# BB#17:                                #   in Loop: Header=BB10_14 Depth=2
	addq	$4, %r15
	movq	80(%rsp), %rbx          # 8-byte Reload
	addq	$8, %rbx
	cmpq	56(%rsp), %r15          # 8-byte Folded Reload
	jl	.LBB10_14
.LBB10_18:                              # %._crit_edge
                                        #   in Loop: Header=BB10_12 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	(%rcx,%rdx,2), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	16(%rsp), %rcx          # 8-byte Reload
	addl	$16, %ecx
	movq	48(%rsp), %rbp          # 8-byte Reload
	addq	32(%rsp), %rbp          # 8-byte Folded Reload
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	cmpl	24(%rsp), %ecx          # 4-byte Folded Reload
	movq	104(%rsp), %rdx         # 8-byte Reload
	jl	.LBB10_12
	jmp	.LBB10_21
.LBB10_20:
	xorl	%eax, %eax
.LBB10_21:                              # %.loopexit
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	computeSATDWP, .Lfunc_end10-computeSATDWP
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	computeBiPredSATD1
	.p2align	4, 0x90
	.type	computeBiPredSATD1,@function
computeBiPredSATD1:                     # @computeBiPredSATD1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi111:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi112:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi114:
	.cfi_def_cfa_offset 192
.Lcfi115:
	.cfi_offset %rbx, -56
.Lcfi116:
	.cfi_offset %r12, -48
.Lcfi117:
	.cfi_offset %r13, -40
.Lcfi118:
	.cfi_offset %r14, -32
.Lcfi119:
	.cfi_offset %r15, -24
.Lcfi120:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movq	%r9, 80(%rsp)           # 8-byte Spill
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movl	%ecx, 36(%rsp)          # 4-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movslq	img_padded_size_x(%rip), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpl	$0, test8x8transform(%rip)
	je	.LBB11_1
# BB#8:
	testl	%esi, %esi
	jle	.LBB11_9
# BB#10:                                # %.lr.ph149
	leal	(,%rdx,8), %eax
	cltq
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	-64(,%rax,8), %rbx
	addq	$64, %rbx
	movslq	%edx, %rcx
	leaq	-64(,%rcx,8), %rdi
	addq	$64, %rdi
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	leaq	-16(%rax,%rax), %r12
	addq	$16, %r12
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leaq	-16(%rcx,%rcx), %r14
	addq	$16, %r14
	xorl	%eax, %eax
	xorl	%edi, %edi
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
.LBB11_11:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_14 Depth 2
                                        #       Child Loop BB11_15 Depth 3
	testl	%edx, %edx
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	jle	.LBB11_17
# BB#12:                                # %.lr.ph142.preheader
                                        #   in Loop: Header=BB11_11 Depth=1
	movl	200(%rsp), %ecx
	leal	(%rcx,%rdi,4), %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	80(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rdi,4), %ecx
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB11_14:                              # %.lr.ph142
                                        #   Parent Loop BB11_11 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_15 Depth 3
	movl	%eax, %r15d
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r13,2), %rax
	movq	%rax, src_line(%rip)
	movslq	bipred2_access_method(%rip), %rax
	movq	ref_pic2_sub(%rip), %rdi
	movl	192(%rsp), %ecx
	leal	(%rcx,%r13,4), %edx
	movl	12(%rsp), %esi          # 4-byte Reload
	callq	*get_line(,%rax,8)
	movq	%rax, ref2_line(%rip)
	movslq	bipred1_access_method(%rip), %rax
	movq	ref_pic1_sub(%rip), %rdi
	movq	88(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%r13,4), %edx
	movl	72(%rsp), %esi          # 4-byte Reload
	callq	*get_line(,%rax,8)
	movq	%rax, ref1_line(%rip)
	movq	src_line(%rip), %rdx
	movq	ref2_line(%rip), %rsi
	movq	%rbx, %rcx
	leaq	(%rsi,%rbx,2), %r8
	leaq	8(%rdx), %rdi
	xorl	%ebx, %ebx
	movl	$8, %ebp
	pxor	%xmm3, %xmm3
	movdqa	.LCPI11_0(%rip), %xmm4  # xmm4 = [1,1,1,1]
	.p2align	4, 0x90
.LBB11_15:                              #   Parent Loop BB11_11 Depth=1
                                        #     Parent Loop BB11_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-8(%rdi), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3]
	movq	-8(%rax,%rbp), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	movq	-8(%rsi,%rbp), %xmm2    # xmm2 = mem[0],zero
	punpcklwd	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1],xmm2[2],xmm3[2],xmm2[3],xmm3[3]
	paddd	%xmm1, %xmm2
	paddd	%xmm4, %xmm2
	psrld	$1, %xmm2
	psubd	%xmm2, %xmm0
	movdqu	%xmm0, diff(%rbx)
	movq	(%rdi), %xmm0           # xmm0 = mem[0],zero
	punpcklwd	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3]
	movq	(%rax,%rbp), %xmm1      # xmm1 = mem[0],zero
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	movq	(%rsi,%rbp), %xmm2      # xmm2 = mem[0],zero
	punpcklwd	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1],xmm2[2],xmm3[2],xmm2[3],xmm3[3]
	paddd	%xmm1, %xmm2
	paddd	%xmm4, %xmm2
	psrld	$1, %xmm2
	psubd	%xmm2, %xmm0
	movdqu	%xmm0, diff+16(%rbx)
	addq	%r12, %rbp
	addq	%r14, %rdi
	addq	$32, %rbx
	cmpl	$256, %ebx              # imm = 0x100
	jne	.LBB11_15
# BB#16:                                #   in Loop: Header=BB11_14 Depth=2
	movq	%rcx, %rbx
	leaq	(%rax,%rbx,2), %rax
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rdx,%rcx,2), %rdx
	movq	%rdx, src_line(%rip)
	movq	%rax, ref1_line(%rip)
	movq	%r8, ref2_line(%rip)
	movl	$diff, %edi
	callq	HadamardSAD8x8
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	addl	%r15d, %eax
	cmpl	36(%rsp), %eax          # 4-byte Folded Reload
	jg	.LBB11_18
# BB#13:                                #   in Loop: Header=BB11_14 Depth=2
	addq	$8, %r13
	cmpq	16(%rsp), %r13          # 8-byte Folded Reload
	jl	.LBB11_14
.LBB11_17:                              # %._crit_edge143
                                        #   in Loop: Header=BB11_11 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	(%rcx,%rdx,2), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rdi          # 8-byte Reload
	addl	$8, %edi
	movl	8(%rsp), %esi           # 4-byte Reload
	cmpl	%esi, %edi
	movq	40(%rsp), %rdx          # 8-byte Reload
	jl	.LBB11_11
	jmp	.LBB11_18
.LBB11_1:
	shll	$2, %esi
	xorl	%eax, %eax
	testl	%esi, %esi
	jle	.LBB11_18
# BB#2:                                 # %.preheader.lr.ph
	testl	%edx, %edx
	jle	.LBB11_18
# BB#3:                                 # %.preheader.us.preheader
	leal	(,%rdx,4), %eax
	leal	-4(%rdx), %ecx
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	-4(%rdi), %rdi
	cltq
	movslq	%ecx, %rcx
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	leaq	16(,%rdi,4), %rdi
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	leaq	16(,%rcx,4), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movslq	%edx, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	addq	%rax, %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	xorl	%edx, %edx
	movl	%esi, 8(%rsp)           # 4-byte Spill
.LBB11_4:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_6 Depth 2
	movl	200(%rsp), %ecx
	leal	(%rdx,%rcx), %ecx
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	leal	(%rdx,%rcx), %ecx
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	movq	24(%rsp), %r14          # 8-byte Reload
	movl	192(%rsp), %ecx
	movl	%ecx, %r15d
	movq	88(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %r13d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB11_6:                               #   Parent Loop BB11_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	%r14, src_line(%rip)
	movslq	bipred2_access_method(%rip), %rax
	movq	ref_pic2_sub(%rip), %rdi
	movl	64(%rsp), %esi          # 4-byte Reload
	movl	%r15d, %edx
	callq	*get_line(,%rax,8)
	movq	%rax, ref2_line(%rip)
	movslq	bipred1_access_method(%rip), %rax
	movq	ref_pic1_sub(%rip), %rdi
	movl	56(%rsp), %esi          # 4-byte Reload
	movl	%r13d, %edx
	callq	*get_line(,%rax,8)
	movq	%rax, ref1_line(%rip)
	movq	src_line(%rip), %rdx
	movq	ref2_line(%rip), %rbx
	movq	120(%rsp), %r8          # 8-byte Reload
	leaq	(%rbx,%r8,2), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	128(%rsp), %r10         # 8-byte Reload
	leaq	(%rdx,%r10,2), %r11
	movq	(%rdx), %xmm0           # xmm0 = mem[0],zero
	pxor	%xmm3, %xmm3
	punpcklwd	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3]
	movq	48(%rsp), %rbp          # 8-byte Reload
	leaq	(%rax,%rbp,2), %rdi
	movq	(%rax), %xmm1           # xmm1 = mem[0],zero
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	leaq	(%rbx,%rbp,2), %rcx
	movq	(%rbx), %xmm2           # xmm2 = mem[0],zero
	punpcklwd	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1],xmm2[2],xmm3[2],xmm2[3],xmm3[3]
	movdqa	.LCPI11_0(%rip), %xmm4  # xmm4 = [1,1,1,1]
	paddd	%xmm4, %xmm2
	paddd	%xmm1, %xmm2
	psrld	$1, %xmm2
	psubd	%xmm2, %xmm0
	movdqa	%xmm0, diff(%rip)
	leaq	8(%r11,%r10,2), %r9
	movq	8(%rdx,%r10,2), %xmm0   # xmm0 = mem[0],zero
	punpcklwd	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3]
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	(%rax,%rsi,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	movq	(%rbx,%rsi,2), %xmm2    # xmm2 = mem[0],zero
	punpcklwd	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1],xmm2[2],xmm3[2],xmm2[3],xmm3[3]
	paddd	%xmm4, %xmm2
	paddd	%xmm1, %xmm2
	psrld	$1, %xmm2
	psubd	%xmm2, %xmm0
	movdqa	%xmm0, diff+16(%rip)
	leaq	16(%rdi,%rbp,2), %rbx
	leaq	16(%rcx,%rbp,2), %rbp
	movq	16(%r11,%r10,2), %xmm0  # xmm0 = mem[0],zero
	punpcklwd	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3]
	movq	8(%rdi,%rsi,2), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	movq	8(%rcx,%rsi,2), %xmm2   # xmm2 = mem[0],zero
	punpcklwd	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1],xmm2[2],xmm3[2],xmm2[3],xmm3[3]
	paddd	%xmm4, %xmm2
	paddd	%xmm1, %xmm2
	psrld	$1, %xmm2
	psubd	%xmm2, %xmm0
	movdqa	%xmm0, diff+32(%rip)
	movq	16(%r9,%r10,2), %xmm0   # xmm0 = mem[0],zero
	punpcklwd	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3]
	movq	(%rbx,%rsi,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	movq	(%rbp,%rsi,2), %xmm2    # xmm2 = mem[0],zero
	punpcklwd	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1],xmm2[2],xmm3[2],xmm2[3],xmm3[3]
	paddd	%xmm4, %xmm2
	paddd	%xmm1, %xmm2
	psrld	$1, %xmm2
	psubd	%xmm2, %xmm0
	movdqa	%xmm0, diff+48(%rip)
	leaq	(%rax,%r8,2), %rax
	movq	112(%rsp), %rcx         # 8-byte Reload
	leaq	(%rdx,%rcx,2), %rcx
	movq	%rcx, src_line(%rip)
	movq	%rax, ref1_line(%rip)
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, ref2_line(%rip)
	movl	$diff, %edi
	callq	HadamardSAD4x4
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	addl	12(%rsp), %eax          # 4-byte Folded Reload
	cmpl	36(%rsp), %eax          # 4-byte Folded Reload
	jg	.LBB11_18
# BB#5:                                 #   in Loop: Header=BB11_6 Depth=2
	addq	$4, %r12
	addl	$16, %r13d
	addl	$16, %r15d
	addq	$8, %r14
	cmpq	40(%rsp), %r12          # 8-byte Folded Reload
	jl	.LBB11_6
# BB#7:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB11_4 Depth=1
	movq	104(%rsp), %rdx         # 8-byte Reload
	addl	$16, %edx
	movq	96(%rsp), %rcx          # 8-byte Reload
	addq	%rcx, 24(%rsp)          # 8-byte Folded Spill
	movl	8(%rsp), %esi           # 4-byte Reload
	cmpl	%esi, %edx
	jl	.LBB11_4
	jmp	.LBB11_18
.LBB11_9:
	xorl	%eax, %eax
.LBB11_18:                              # %.loopexit
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	computeBiPredSATD1, .Lfunc_end11-computeBiPredSATD1
	.cfi_endproc

	.globl	computeBiPredSATD2
	.p2align	4, 0x90
	.type	computeBiPredSATD2,@function
computeBiPredSATD2:                     # @computeBiPredSATD2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi124:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi125:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi127:
	.cfi_def_cfa_offset 224
.Lcfi128:
	.cfi_offset %rbx, -56
.Lcfi129:
	.cfi_offset %r12, -48
.Lcfi130:
	.cfi_offset %r13, -40
.Lcfi131:
	.cfi_offset %r14, -32
.Lcfi132:
	.cfi_offset %r15, -24
.Lcfi133:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%r8, 152(%rsp)          # 8-byte Spill
	movl	%ecx, 28(%rsp)          # 4-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	luma_log_weight_denom(%rip), %eax
	incl	%eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	wp_luma_round(%rip), %r15d
	addl	%r15d, %r15d
	movslq	img_padded_size_x(%rip), %rax
	cmpl	$0, test8x8transform(%rip)
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	movl	%r15d, 36(%rsp)         # 4-byte Spill
	je	.LBB12_10
# BB#1:
	testl	%esi, %esi
	jle	.LBB12_20
# BB#2:                                 # %.lr.ph219
	leal	(,%rdx,8), %ecx
	movslq	%ecx, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	-64(,%rax,8), %rbp
	addq	$64, %rbp
	movslq	%edx, %rcx
	leaq	-8(,%rcx,8), %rdi
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	leaq	-2(%rcx,%rcx), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	-16(%rax,%rax), %rax
	addq	$16, %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	xorl	%edi, %edi
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movq	%rbp, 88(%rsp)          # 8-byte Spill
.LBB12_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_5 Depth 2
                                        #       Child Loop BB12_6 Depth 3
	testl	%edx, %edx
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	jle	.LBB12_9
# BB#4:                                 # %.lr.ph212.preheader
                                        #   in Loop: Header=BB12_3 Depth=1
	movl	232(%rsp), %ecx
	leal	(%rcx,%rdi,4), %ecx
	movl	%ecx, 96(%rsp)          # 4-byte Spill
	leal	(%r9,%rdi,4), %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_5:                               # %.lr.ph212
                                        #   Parent Loop BB12_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_6 Depth 3
	movl	%eax, 128(%rsp)         # 4-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbx,2), %rax
	movq	%rax, src_line(%rip)
	movslq	bipred2_access_method(%rip), %rax
	movq	ref_pic2_sub(%rip), %rdi
	movl	224(%rsp), %ecx
	leal	(%rcx,%rbx,4), %edx
	movl	96(%rsp), %esi          # 4-byte Reload
	callq	*get_line(,%rax,8)
	movq	%rax, ref2_line(%rip)
	movslq	bipred1_access_method(%rip), %rax
	movq	ref_pic1_sub(%rip), %rdi
	movq	152(%rsp), %rcx         # 8-byte Reload
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	leal	(%rcx,%rbx,4), %edx
	movl	12(%rsp), %esi          # 4-byte Reload
	callq	*get_line(,%rax,8)
	movq	%rax, ref1_line(%rip)
	movswl	weight1(%rip), %r14d
	movswl	weight2(%rip), %r11d
	movq	img(%rip), %r12
	movswl	offsetBi(%rip), %r13d
	movq	ref2_line(%rip), %r10
	movq	src_line(%rip), %rdx
	leaq	(%r10,%rbp,2), %rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	leaq	14(%rdx), %rdx
	xorl	%esi, %esi
	movl	$14, %r8d
	.p2align	4, 0x90
.LBB12_6:                               #   Parent Loop BB12_3 Depth=1
                                        #     Parent Loop BB12_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	-14(%rax,%r8), %r9d
	imull	%r14d, %r9d
	movzwl	-14(%r10,%r8), %ecx
	imull	%r11d, %ecx
	addl	%r15d, %r9d
	addl	%ecx, %r9d
	movl	15520(%r12), %r15d
	movl	32(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %ecx
	sarl	%cl, %r9d
	addl	%r13d, %r9d
	movl	$0, %ecx
	cmovsl	%ecx, %r9d
	cmpl	%r15d, %r9d
	cmovgl	%r15d, %r9d
	movl	36(%rsp), %r15d         # 4-byte Reload
	movzwl	-14(%rdx), %ecx
	subl	%r9d, %ecx
	movl	%ecx, diff(%rsi)
	movzwl	-12(%rax,%r8), %edi
	imull	%r14d, %edi
	movzwl	-12(%r10,%r8), %ecx
	imull	%r11d, %ecx
	addl	%r15d, %edi
	addl	%ecx, %edi
	movl	15520(%r12), %ebp
	movl	%ebx, %ecx
	sarl	%cl, %edi
	addl	%r13d, %edi
	movl	$0, %r9d
	cmovsl	%r9d, %edi
	cmpl	%ebp, %edi
	cmovgl	%ebp, %edi
	movzwl	-12(%rdx), %ecx
	subl	%edi, %ecx
	movl	%ecx, diff+4(%rsi)
	movzwl	-10(%rax,%r8), %edi
	imull	%r14d, %edi
	movzwl	-10(%r10,%r8), %ecx
	imull	%r11d, %ecx
	addl	%r15d, %edi
	addl	%ecx, %edi
	movl	15520(%r12), %ebp
	movl	%ebx, %ecx
	sarl	%cl, %edi
	addl	%r13d, %edi
	cmovsl	%r9d, %edi
	xorl	%r9d, %r9d
	cmpl	%ebp, %edi
	cmovgl	%ebp, %edi
	movzwl	-10(%rdx), %ecx
	subl	%edi, %ecx
	movl	%ecx, diff+8(%rsi)
	movzwl	-8(%rax,%r8), %edi
	imull	%r14d, %edi
	movzwl	-8(%r10,%r8), %ecx
	imull	%r11d, %ecx
	addl	%r15d, %edi
	addl	%ecx, %edi
	movl	15520(%r12), %ebp
	movl	%ebx, %ecx
	sarl	%cl, %edi
	addl	%r13d, %edi
	cmovsl	%r9d, %edi
	cmpl	%ebp, %edi
	cmovgl	%ebp, %edi
	movzwl	-8(%rdx), %ecx
	subl	%edi, %ecx
	movl	%ecx, diff+12(%rsi)
	movzwl	-6(%rax,%r8), %edi
	imull	%r14d, %edi
	movzwl	-6(%r10,%r8), %ecx
	imull	%r11d, %ecx
	addl	%r15d, %edi
	addl	%ecx, %edi
	movl	15520(%r12), %ebp
	movl	%ebx, %ecx
	sarl	%cl, %edi
	addl	%r13d, %edi
	cmovsl	%r9d, %edi
	cmpl	%ebp, %edi
	cmovgl	%ebp, %edi
	movzwl	-6(%rdx), %ecx
	subl	%edi, %ecx
	movl	%ecx, diff+16(%rsi)
	movzwl	-4(%rax,%r8), %edi
	imull	%r14d, %edi
	movzwl	-4(%r10,%r8), %ecx
	imull	%r11d, %ecx
	addl	%r15d, %edi
	addl	%ecx, %edi
	movl	15520(%r12), %ebp
	movl	%ebx, %ecx
	sarl	%cl, %edi
	addl	%r13d, %edi
	cmovsl	%r9d, %edi
	cmpl	%ebp, %edi
	cmovgl	%ebp, %edi
	movzwl	-4(%rdx), %ecx
	subl	%edi, %ecx
	movl	%ecx, diff+20(%rsi)
	movzwl	-2(%rax,%r8), %edi
	imull	%r14d, %edi
	movzwl	-2(%r10,%r8), %ecx
	imull	%r11d, %ecx
	addl	%r15d, %edi
	addl	%ecx, %edi
	movl	15520(%r12), %ebp
	movl	%ebx, %ecx
	sarl	%cl, %edi
	addl	%r13d, %edi
	cmovsl	%r9d, %edi
	cmpl	%ebp, %edi
	cmovgl	%ebp, %edi
	movzwl	-2(%rdx), %ecx
	subl	%edi, %ecx
	movl	%ecx, diff+24(%rsi)
	movzwl	(%rax,%r8), %edi
	imull	%r14d, %edi
	movzwl	(%r10,%r8), %ecx
	imull	%r11d, %ecx
	addl	%r15d, %edi
	addl	%ecx, %edi
	movl	15520(%r12), %ebp
	movl	%ebx, %ecx
	sarl	%cl, %edi
	addl	%r13d, %edi
	cmovsl	%r9d, %edi
	cmpl	%ebp, %edi
	cmovgl	%ebp, %edi
	movzwl	(%rdx), %ecx
	subl	%edi, %ecx
	movl	%ecx, diff+28(%rsi)
	addq	40(%rsp), %rdx          # 8-byte Folded Reload
	addq	136(%rsp), %r8          # 8-byte Folded Reload
	addq	$32, %rsi
	cmpl	$256, %esi              # imm = 0x100
	jne	.LBB12_6
# BB#7:                                 #   in Loop: Header=BB12_5 Depth=2
	movq	88(%rsp), %rbp          # 8-byte Reload
	leaq	(%rax,%rbp,2), %rax
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,2), %rcx
	movq	%rax, ref1_line(%rip)
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	%rax, ref2_line(%rip)
	movq	%rcx, src_line(%rip)
	movl	$diff, %edi
	callq	HadamardSAD8x8
	addl	128(%rsp), %eax         # 4-byte Folded Reload
	cmpl	28(%rsp), %eax          # 4-byte Folded Reload
	jg	.LBB12_21
# BB#8:                                 #   in Loop: Header=BB12_5 Depth=2
	movq	104(%rsp), %rbx         # 8-byte Reload
	addq	$8, %rbx
	cmpq	72(%rsp), %rbx          # 8-byte Folded Reload
	jl	.LBB12_5
.LBB12_9:                               # %._crit_edge213
                                        #   in Loop: Header=BB12_3 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	(%rcx,%rdx,2), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	64(%rsp), %rdi          # 8-byte Reload
	addl	$8, %edi
	cmpl	8(%rsp), %edi           # 4-byte Folded Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	144(%rsp), %rdx         # 8-byte Reload
	jl	.LBB12_3
	jmp	.LBB12_21
.LBB12_10:
	shll	$2, %esi
	testl	%esi, %esi
	jle	.LBB12_20
# BB#11:                                # %.preheader.lr.ph
	leal	(,%rdx,4), %ecx
	movslq	%ecx, %rcx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	leaq	-16(,%rax,4), %rbx
	addq	$16, %rbx
	movslq	%edx, %rcx
	leaq	-16(,%rcx,4), %rdi
	addq	$16, %rdi
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	leaq	-8(%rcx,%rcx), %rcx
	addq	$8, %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	leaq	-8(%rax,%rax), %rax
	addq	$8, %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	xorl	%edi, %edi
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movq	%rbx, 72(%rsp)          # 8-byte Spill
.LBB12_12:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_14 Depth 2
                                        #       Child Loop BB12_15 Depth 3
	testl	%edx, %edx
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	jle	.LBB12_18
# BB#13:                                # %.lr.ph
                                        #   in Loop: Header=BB12_12 Depth=1
	movl	232(%rsp), %ecx
	leal	(%rdi,%rcx), %ecx
	movl	%ecx, 88(%rsp)          # 4-byte Spill
	leal	(%rdi,%r9), %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB12_14:                              #   Parent Loop BB12_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_15 Depth 3
	movl	%eax, 120(%rsp)         # 4-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbp,2), %rax
	movq	%rax, src_line(%rip)
	movslq	bipred2_access_method(%rip), %rax
	movq	ref_pic2_sub(%rip), %rdi
	movl	224(%rsp), %ecx
	leal	(%rcx,%rbp,4), %edx
	movl	88(%rsp), %esi          # 4-byte Reload
	callq	*get_line(,%rax,8)
	movq	%rax, ref2_line(%rip)
	movslq	bipred1_access_method(%rip), %rax
	movq	ref_pic1_sub(%rip), %rdi
	movq	152(%rsp), %rcx         # 8-byte Reload
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	leal	(%rcx,%rbp,4), %edx
	movl	12(%rsp), %esi          # 4-byte Reload
	callq	*get_line(,%rax,8)
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rax, ref1_line(%rip)
	movswl	weight1(%rip), %eax
	movswl	weight2(%rip), %r11d
	movq	img(%rip), %r12
	movswl	offsetBi(%rip), %r10d
	movq	ref2_line(%rip), %rbp
	movq	src_line(%rip), %rdx
	leaq	(%rbp,%rbx,2), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	leaq	6(%rdx), %rdx
	xorl	%esi, %esi
	movl	$6, %r8d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB12_15:                              #   Parent Loop BB12_12 Depth=1
                                        #     Parent Loop BB12_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	40(%rsp), %r14          # 8-byte Reload
	movzwl	-6(%r14,%r8), %r9d
	imull	%eax, %r9d
	movzwl	-6(%rbp,%r8), %ecx
	imull	%r11d, %ecx
	addl	%r15d, %r9d
	addl	%ecx, %r9d
	movl	15520(%r12), %r15d
	movl	32(%rsp), %ecx          # 4-byte Reload
	sarl	%cl, %r9d
	addl	%r10d, %r9d
	cmovsl	%r13d, %r9d
	cmpl	%r15d, %r9d
	cmovgl	%r15d, %r9d
	movl	36(%rsp), %r15d         # 4-byte Reload
	movzwl	-6(%rdx), %edi
	subl	%r9d, %edi
	movl	%edi, diff(%rsi)
	movzwl	-4(%r14,%r8), %edi
	imull	%eax, %edi
	movzwl	-4(%rbp,%r8), %ebx
	imull	%r11d, %ebx
	addl	%r15d, %edi
	addl	%ebx, %edi
	movl	15520(%r12), %ebx
	sarl	%cl, %edi
	addl	%r10d, %edi
	cmovsl	%r13d, %edi
	cmpl	%ebx, %edi
	cmovgl	%ebx, %edi
	movzwl	-4(%rdx), %ebx
	subl	%edi, %ebx
	movl	%ebx, diff+4(%rsi)
	movzwl	-2(%r14,%r8), %edi
	imull	%eax, %edi
	movzwl	-2(%rbp,%r8), %ebx
	imull	%r11d, %ebx
	addl	%r15d, %edi
	addl	%ebx, %edi
	movl	15520(%r12), %ebx
	sarl	%cl, %edi
	addl	%r10d, %edi
	cmovsl	%r13d, %edi
	cmpl	%ebx, %edi
	cmovgl	%ebx, %edi
	movzwl	-2(%rdx), %ebx
	subl	%edi, %ebx
	movl	%ebx, diff+8(%rsi)
	movzwl	(%r14,%r8), %edi
	imull	%eax, %edi
	movzwl	(%rbp,%r8), %ebx
	imull	%r11d, %ebx
	addl	%r15d, %edi
	addl	%ebx, %edi
	movl	15520(%r12), %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edi
	addl	%r10d, %edi
	cmovsl	%r13d, %edi
	cmpl	%ebx, %edi
	cmovgl	%ebx, %edi
	movzwl	(%rdx), %ecx
	subl	%edi, %ecx
	movl	%ecx, diff+12(%rsi)
	addq	136(%rsp), %rdx         # 8-byte Folded Reload
	addq	128(%rsp), %r8          # 8-byte Folded Reload
	addq	$16, %rsi
	cmpl	$64, %esi
	jne	.LBB12_15
# BB#16:                                #   in Loop: Header=BB12_14 Depth=2
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbx,2), %rax
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	104(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,2), %rcx
	movq	%rax, ref1_line(%rip)
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	%rax, ref2_line(%rip)
	movq	%rcx, src_line(%rip)
	movl	$diff, %edi
	callq	HadamardSAD4x4
	addl	120(%rsp), %eax         # 4-byte Folded Reload
	cmpl	28(%rsp), %eax          # 4-byte Folded Reload
	jg	.LBB12_21
# BB#17:                                #   in Loop: Header=BB12_14 Depth=2
	movq	96(%rsp), %rbp          # 8-byte Reload
	addq	$4, %rbp
	cmpq	64(%rsp), %rbp          # 8-byte Folded Reload
	jl	.LBB12_14
.LBB12_18:                              # %._crit_edge
                                        #   in Loop: Header=BB12_12 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	160(%rsp), %rdx         # 8-byte Reload
	leaq	(%rcx,%rdx,2), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	48(%rsp), %rdi          # 8-byte Reload
	addl	$16, %edi
	cmpl	8(%rsp), %edi           # 4-byte Folded Reload
	movq	144(%rsp), %rdx         # 8-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	jl	.LBB12_12
	jmp	.LBB12_21
.LBB12_20:
	xorl	%eax, %eax
.LBB12_21:                              # %.loopexit
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	computeBiPredSATD2, .Lfunc_end12-computeBiPredSATD2
	.cfi_endproc

	.globl	computeSSE
	.p2align	4, 0x90
	.type	computeSSE,@function
computeSSE:                             # @computeSSE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi134:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi135:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi136:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi137:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi138:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi139:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi140:
	.cfi_def_cfa_offset 96
.Lcfi141:
	.cfi_offset %rbx, -56
.Lcfi142:
	.cfi_offset %r12, -48
.Lcfi143:
	.cfi_offset %r13, -40
.Lcfi144:
	.cfi_offset %r14, -32
.Lcfi145:
	.cfi_offset %r15, -24
.Lcfi146:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %ebp
	movl	%esi, %r13d
	movq	img(%rip), %rax
	movq	14232(%rax), %r12
	movslq	img_padded_size_x(%rip), %r15
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, src_line(%rip)
	movslq	ref_access_method(%rip), %rax
	movq	ref_pic_sub(%rip), %rdi
	movl	%r9d, (%rsp)            # 4-byte Spill
	movl	%r9d, %esi
	movl	%r8d, 4(%rsp)           # 4-byte Spill
	movl	%r8d, %edx
	callq	*get_line(,%rax,8)
	movq	%rax, ref_line(%rip)
	testl	%r13d, %r13d
	jle	.LBB13_12
# BB#1:                                 # %.preheader74.lr.ph
	movslq	%ebp, %rcx
	subq	%rcx, %r15
	testl	%ecx, %ecx
	jle	.LBB13_9
# BB#2:                                 # %.preheader74.us.preheader
	leal	-1(%rbp), %ecx
	andl	$-4, %ecx
	movq	src_line(%rip), %rdx
	leaq	(%rcx,%rcx), %rsi
	leaq	8(%rsi,%r15,2), %r9
	leaq	8(%rcx,%rcx), %r10
	leaq	4(%rcx), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	xorl	%ebx, %ebx
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB13_3:                               # %.preheader74.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_4 Depth 2
	leaq	(%rdx,%r10), %r11
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB13_4:                               #   Parent Loop BB13_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rsi,2), %xmm1    # xmm1 = mem[0],zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movq	(%rax,%rsi,2), %xmm2    # xmm2 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	psubd	%xmm2, %xmm1
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	movd	%xmm2, %rcx
	movd	%xmm1, %rdi
	movslq	%edi, %r8
	addl	(%r12,%r8,4), %ebx
	sarq	$32, %rdi
	addl	(%r12,%rdi,4), %ebx
	movslq	%ecx, %rdi
	addl	(%r12,%rdi,4), %ebx
	sarq	$32, %rcx
	addl	(%r12,%rcx,4), %ebx
	addq	$4, %rsi
	cmpl	%ebp, %esi
	jl	.LBB13_4
# BB#5:                                 # %._crit_edge107.us
                                        #   in Loop: Header=BB13_3 Depth=1
	cmpl	%r14d, %ebx
	jge	.LBB13_21
# BB#6:                                 #   in Loop: Header=BB13_3 Depth=1
	addq	%r9, %rax
	incl	%r15d
	cmpl	%r13d, %r15d
	movq	%r11, %rdx
	jl	.LBB13_3
# BB#7:                                 # %._crit_edge115.loopexit
	movq	%r11, src_line(%rip)
	movq	%rax, ref_line(%rip)
	cmpl	$0, ChromaMEEnable(%rip)
	jne	.LBB13_13
	jmp	.LBB13_37
.LBB13_9:                               # %.preheader74.preheader
	leaq	(%rax,%r15,2), %rax
	addq	%r15, %r15
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB13_10:                              # %.preheader74
                                        # =>This Inner Loop Header: Depth=1
	testl	%r14d, %r14d
	jle	.LBB13_37
# BB#11:                                #   in Loop: Header=BB13_10 Depth=1
	movq	%rax, ref_line(%rip)
	incl	%ecx
	addq	%r15, %rax
	cmpl	%r13d, %ecx
	jl	.LBB13_10
.LBB13_12:
	xorl	%ebx, %ebx
	cmpl	$0, ChromaMEEnable(%rip)
	je	.LBB13_37
.LBB13_13:
	movb	shift_cr_x(%rip), %cl
	sarl	%cl, %ebp
	movb	shift_cr_y(%rip), %cl
	sarl	%cl, %r13d
	testl	%r13d, %r13d
	jle	.LBB13_22
# BB#14:                                # %.split.us.preheader
	movslq	img_cr_padded_size_x(%rip), %rax
	movslq	%ebp, %r15
	subq	%r15, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leal	-1(%r15), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	512(%rax), %rax
	movq	%rax, src_line(%rip)
	movslq	ref_access_method(%rip), %rax
	movq	ref_pic_sub+8(%rip), %rdi
	movl	(%rsp), %esi            # 4-byte Reload
	movl	4(%rsp), %edx           # 4-byte Reload
	callq	*get_crline(,%rax,8)
	movq	8(%rsp), %rsi           # 8-byte Reload
	testl	%r15d, %r15d
	leaq	2(%rsi,%rsi), %r9
	movq	%rax, ref_line(%rip)
	jle	.LBB13_23
# BB#15:                                # %.preheader.us.us.preheader
	movq	%r9, %rdi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	src_line(%rip), %rcx
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %r15
	leaq	(%rdx,%rdx), %rdx
	leaq	4(%rdx,%rsi,4), %r8
	leaq	4(,%rsi,4), %r9
	movl	%ebp, %esi
	xorl	%r11d, %r11d
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB13_16:                              # %.preheader.us.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_17 Depth 2
	leaq	(%rcx,%r9), %r10
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB13_17:                              #   Parent Loop BB13_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	(%rcx,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	pshufd	$212, %xmm1, %xmm1      # xmm1 = xmm1[0,1,1,3]
	movd	(%rax,%rdx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	pshufd	$212, %xmm2, %xmm2      # xmm2 = xmm2[0,1,1,3]
	psubq	%xmm2, %xmm1
	movd	%xmm1, %edi
	movslq	%edi, %rdi
	addl	(%r12,%rdi,4), %ebx
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	movd	%xmm1, %edi
	movslq	%edi, %rdi
	addl	(%r12,%rdi,4), %ebx
	incq	%rdx
	cmpl	%edx, %esi
	jne	.LBB13_17
# BB#18:                                # %._crit_edge.us.us
                                        #   in Loop: Header=BB13_16 Depth=1
	cmpl	%r14d, %ebx
	jge	.LBB13_34
# BB#19:                                #   in Loop: Header=BB13_16 Depth=1
	addq	%r8, %rax
	incl	%r11d
	cmpl	%r13d, %r11d
	movq	%r10, %rcx
	jl	.LBB13_16
# BB#20:                                # %._crit_edge84.us.loopexit
	movq	%r10, src_line(%rip)
	movq	%rax, ref_line(%rip)
	jmp	.LBB13_27
.LBB13_21:                              # %.thread.loopexit151
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rax,%rcx,2), %rax
	movq	%r11, src_line(%rip)
	jmp	.LBB13_36
.LBB13_22:                              # %.split.preheader
	movq	24(%rsp), %rbp          # 8-byte Reload
	leaq	512(%rbp), %rax
	movq	%rax, src_line(%rip)
	movslq	ref_access_method(%rip), %rax
	movq	ref_pic_sub+8(%rip), %rdi
	movl	(%rsp), %r14d           # 4-byte Reload
	movl	%r14d, %esi
	movl	4(%rsp), %r15d          # 4-byte Reload
	movl	%r15d, %edx
	callq	*get_crline(,%rax,8)
	movq	%rax, ref_line(%rip)
	addq	$1024, %rbp             # imm = 0x400
	movq	%rbp, src_line(%rip)
	movslq	ref_access_method(%rip), %rax
	movq	ref_pic_sub+16(%rip), %rdi
	movl	%r14d, %esi
	movl	%r15d, %edx
	callq	*get_crline(,%rax,8)
	jmp	.LBB13_36
.LBB13_23:                              # %.preheader.us91.preheader
	movq	32(%rsp), %r15          # 8-byte Reload
	leaq	(%rax,%r15,2), %rax
	leaq	(%r15,%r15), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB13_24:                              # %.preheader.us91
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r14d, %ebx
	jge	.LBB13_37
# BB#25:                                #   in Loop: Header=BB13_24 Depth=1
	movq	%rax, ref_line(%rip)
	incl	%edx
	addq	%rcx, %rax
	cmpl	%r13d, %edx
	jl	.LBB13_24
# BB#26:
	movq	%r9, %rdi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
.LBB13_27:                              # %._crit_edge84.us
	movq	24(%rsp), %rax          # 8-byte Reload
	addq	$1024, %rax             # imm = 0x400
	movq	%rax, src_line(%rip)
	movslq	ref_access_method(%rip), %rax
	movq	ref_pic_sub+16(%rip), %rdi
	movl	(%rsp), %esi            # 4-byte Reload
	movl	4(%rsp), %edx           # 4-byte Reload
	callq	*get_crline(,%rax,8)
	testl	%ebp, %ebp
	movq	%rax, ref_line(%rip)
	jle	.LBB13_38
# BB#28:                                # %.preheader.us.us.preheader.1
	movq	src_line(%rip), %rcx
	addq	%r15, %r15
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	4(%r15,%rdx,4), %r8
	leaq	4(,%rdx,4), %r9
	movl	%ebp, %esi
	xorl	%edi, %edi
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB13_29:                              # %.preheader.us.us.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_30 Depth 2
	leaq	(%rcx,%r9), %r10
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB13_30:                              #   Parent Loop BB13_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	(%rcx,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	pshufd	$212, %xmm1, %xmm1      # xmm1 = xmm1[0,1,1,3]
	movd	(%rax,%rdx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	pshufd	$212, %xmm2, %xmm2      # xmm2 = xmm2[0,1,1,3]
	psubq	%xmm2, %xmm1
	movd	%xmm1, %ebp
	movslq	%ebp, %rbp
	addl	(%r12,%rbp,4), %ebx
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	movd	%xmm1, %ebp
	movslq	%ebp, %rbp
	addl	(%r12,%rbp,4), %ebx
	incq	%rdx
	cmpl	%edx, %esi
	jne	.LBB13_30
# BB#31:                                # %._crit_edge.us.us.1
                                        #   in Loop: Header=BB13_29 Depth=1
	cmpl	%r14d, %ebx
	jge	.LBB13_34
# BB#32:                                #   in Loop: Header=BB13_29 Depth=1
	addq	%r8, %rax
	incl	%edi
	cmpl	%r13d, %edi
	movq	%r10, %rcx
	jl	.LBB13_29
	jmp	.LBB13_35
.LBB13_34:                              # %.thread.loopexit148
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,2), %rax
.LBB13_35:                              # %.thread
	movq	%r10, src_line(%rip)
.LBB13_36:                              # %.thread
	movq	%rax, ref_line(%rip)
.LBB13_37:                              # %.thread
	movl	%ebx, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_38:                              # %.preheader.us91.1.preheader
	leaq	(%rax,%r15,2), %rax
	addq	%r15, %r15
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB13_39:                              # %.preheader.us91.1
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r14d, %ebx
	jge	.LBB13_37
# BB#40:                                #   in Loop: Header=BB13_39 Depth=1
	movq	%rax, ref_line(%rip)
	incl	%ecx
	addq	%r15, %rax
	cmpl	%r13d, %ecx
	jl	.LBB13_39
	jmp	.LBB13_37
.Lfunc_end13:
	.size	computeSSE, .Lfunc_end13-computeSSE
	.cfi_endproc

	.globl	computeSSEWP
	.p2align	4, 0x90
	.type	computeSSEWP,@function
computeSSEWP:                           # @computeSSEWP
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi147:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi148:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi149:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi150:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi151:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi152:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi153:
	.cfi_def_cfa_offset 144
.Lcfi154:
	.cfi_offset %rbx, -56
.Lcfi155:
	.cfi_offset %r12, -48
.Lcfi156:
	.cfi_offset %r13, -40
.Lcfi157:
	.cfi_offset %r14, -32
.Lcfi158:
	.cfi_offset %r15, -24
.Lcfi159:
	.cfi_offset %rbp, -16
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	%edx, %r12d
	movl	%esi, %ebp
	movq	img(%rip), %rax
	movq	14232(%rax), %r13
	movslq	img_padded_size_x(%rip), %rbx
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movq	%rdi, src_line(%rip)
	movslq	ref_access_method(%rip), %rax
	movq	ref_pic_sub(%rip), %rdi
	movl	%r9d, 16(%rsp)          # 4-byte Spill
	movl	%r9d, %esi
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movl	%r8d, %edx
	callq	*get_line(,%rax,8)
	movq	%rax, ref_line(%rip)
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	testl	%ebp, %ebp
	jle	.LBB14_12
# BB#1:                                 # %.preheader86.lr.ph
	movslq	%r12d, %rcx
	subq	%rcx, %rbx
	testl	%ecx, %ecx
	jle	.LBB14_9
# BB#2:                                 # %.preheader86.us.preheader
	movq	img(%rip), %rdx
	leal	-1(%r12), %edi
	andl	$-4, %edi
	movq	src_line(%rip), %rcx
	movd	weight_luma(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm8        # xmm8 = xmm0[0,0,0,0]
	movd	wp_luma_round(%rip), %xmm1 # xmm1 = mem[0],zero,zero,zero
	pshufd	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movss	luma_log_weight_denom(%rip), %xmm6 # xmm6 = mem[0],zero,zero,zero
	movd	offset_luma(%rip), %xmm2 # xmm2 = mem[0],zero,zero,zero
	pshufd	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	movd	15520(%rdx), %xmm3      # xmm3 = mem[0],zero,zero,zero
	pshufd	$0, %xmm3, %xmm3        # xmm3 = xmm3[0,0,0,0]
	leaq	8(%rdi,%rdi), %r9
	leaq	4(%rdi), %r10
	leaq	(%rdi,%rdi), %rdi
	leaq	8(%rdi,%rbx,2), %r8
	xorl	%r14d, %r14d
	pxor	%xmm4, %xmm4
	xorps	%xmm5, %xmm5
	movss	%xmm6, %xmm5            # xmm5 = xmm6[0],xmm5[1,2,3]
	pshufd	$245, %xmm8, %xmm6      # xmm6 = xmm8[1,1,3,3]
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB14_3:                               # %.preheader86.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_4 Depth 2
	leaq	(%rax,%r10,2), %r11
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB14_4:                               #   Parent Loop BB14_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rbx,2), %xmm7    # xmm7 = mem[0],zero
	punpcklwd	%xmm4, %xmm7    # xmm7 = xmm7[0],xmm4[0],xmm7[1],xmm4[1],xmm7[2],xmm4[2],xmm7[3],xmm4[3]
	pshufd	$245, %xmm7, %xmm0      # xmm0 = xmm7[1,1,3,3]
	pmuludq	%xmm8, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pmuludq	%xmm6, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm7    # xmm7 = xmm7[0],xmm0[0],xmm7[1],xmm0[1]
	paddd	%xmm1, %xmm7
	psrad	%xmm5, %xmm7
	paddd	%xmm2, %xmm7
	movdqa	%xmm7, %xmm0
	pcmpgtd	%xmm4, %xmm0
	pand	%xmm7, %xmm0
	movdqa	%xmm3, %xmm7
	pcmpgtd	%xmm0, %xmm7
	pand	%xmm7, %xmm0
	pandn	%xmm3, %xmm7
	por	%xmm0, %xmm7
	movq	(%rcx,%rbx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1],xmm0[2],xmm4[2],xmm0[3],xmm4[3]
	psubd	%xmm7, %xmm0
	pshufd	$78, %xmm0, %xmm7       # xmm7 = xmm0[2,3,0,1]
	movd	%xmm7, %rdx
	movd	%xmm0, %rsi
	movslq	%esi, %rbp
	addl	(%r13,%rbp,4), %r14d
	sarq	$32, %rsi
	addl	(%r13,%rsi,4), %r14d
	movslq	%edx, %rsi
	addl	(%r13,%rsi,4), %r14d
	sarq	$32, %rdx
	addl	(%r13,%rdx,4), %r14d
	addq	$4, %rbx
	cmpl	%r12d, %ebx
	jl	.LBB14_4
# BB#5:                                 # %._crit_edge103.us
                                        #   in Loop: Header=BB14_3 Depth=1
	addq	%r9, %rcx
	cmpl	8(%rsp), %r14d          # 4-byte Folded Reload
	jge	.LBB14_27
# BB#6:                                 #   in Loop: Header=BB14_3 Depth=1
	addq	%r8, %rax
	incl	%edi
	cmpl	4(%rsp), %edi           # 4-byte Folded Reload
	jl	.LBB14_3
# BB#7:                                 # %._crit_edge111.loopexit
	movq	%rax, ref_line(%rip)
	movq	%rcx, src_line(%rip)
	cmpl	$0, ChromaMEEnable(%rip)
	jne	.LBB14_13
	jmp	.LBB14_28
.LBB14_9:                               # %.preheader86.preheader
	leaq	(%rax,%rbx,2), %rax
	addq	%rbx, %rbx
	xorl	%r14d, %r14d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB14_10:                              # %.preheader86
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB14_28
# BB#11:                                #   in Loop: Header=BB14_10 Depth=1
	movq	%rax, ref_line(%rip)
	incl	%ecx
	addq	%rbx, %rax
	cmpl	4(%rsp), %ecx           # 4-byte Folded Reload
	jl	.LBB14_10
.LBB14_12:
	xorl	%r14d, %r14d
	cmpl	$0, ChromaMEEnable(%rip)
	je	.LBB14_28
.LBB14_13:
	movb	shift_cr_x(%rip), %cl
	sarl	%cl, %r12d
	movb	shift_cr_y(%rip), %cl
	sarl	%cl, 4(%rsp)            # 4-byte Folded Spill
	movslq	img_cr_padded_size_x(%rip), %rcx
	movslq	%r12d, %rax
	subq	%rax, %rcx
	decl	%eax
	leaq	2(%rax,%rax), %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rcx), %rbx
	leaq	4(,%rax,4), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	leaq	4(%rbx,%rax,4), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
	movq	%rbx, 24(%rsp)          # 8-byte Spill
.LBB14_14:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_23 Depth 2
                                        #     Child Loop BB14_17 Depth 2
                                        #       Child Loop BB14_18 Depth 3
	movl	$256, %eax              # imm = 0x100
	movl	%ebp, %ecx
	shll	%cl, %eax
	cltq
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,2), %rax
	movq	%rax, src_line(%rip)
	movslq	ref_access_method(%rip), %rax
	movq	ref_pic_sub+8(,%rbp,8), %rdi
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	*get_crline(,%rax,8)
	movq	%rax, ref_line(%rip)
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB14_25
# BB#15:                                # %.preheader.lr.ph
                                        #   in Loop: Header=BB14_14 Depth=1
	testl	%r12d, %r12d
	jle	.LBB14_22
# BB#16:                                # %.preheader.us.preheader
                                        #   in Loop: Header=BB14_14 Depth=1
	movl	wp_chroma_round(%rip), %r10d
	movl	chroma_log_weight_denom(%rip), %ecx
	movq	img(%rip), %rdx
	movl	15524(%rdx), %edi
	movl	weight_cr(,%rbp,4), %r11d
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movl	offset_cr(,%rbp,4), %r8d
	xorl	%edx, %edx
	movq	src_line(%rip), %rsi
	.p2align	4, 0x90
.LBB14_17:                              # %.preheader.us
                                        #   Parent Loop BB14_14 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_18 Depth 3
	movl	%edx, 20(%rsp)          # 4-byte Spill
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%rax,%rdx,2), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB14_18:                              #   Parent Loop BB14_14 Depth=1
                                        #     Parent Loop BB14_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	(%rax,%rdx,4), %ebx
	imull	%r11d, %ebx
	addl	%r10d, %ebx
	sarl	%cl, %ebx
	addl	%r8d, %ebx
	cmovsl	%r15d, %ebx
	cmpl	%edi, %ebx
	cmovgl	%edi, %ebx
	movzwl	(%rsi,%rdx,4), %r9d
	movslq	%ebx, %rbx
	subq	%rbx, %r9
	addl	(%r13,%r9,4), %r14d
	movzwl	2(%rax,%rdx,4), %ebx
	imull	%r11d, %ebx
	addl	%r10d, %ebx
	sarl	%cl, %ebx
	addl	%r8d, %ebx
	cmovsl	%r15d, %ebx
	cmpl	%edi, %ebx
	cmovgl	%edi, %ebx
	movzwl	2(%rsi,%rdx,4), %ebp
	movslq	%ebx, %rbx
	subq	%rbx, %rbp
	addl	(%r13,%rbp,4), %r14d
	incq	%rdx
	cmpl	%edx, %r12d
	jne	.LBB14_18
# BB#19:                                # %._crit_edge.us
                                        #   in Loop: Header=BB14_17 Depth=2
	addq	72(%rsp), %rsi          # 8-byte Folded Reload
	cmpl	8(%rsp), %r14d          # 4-byte Folded Reload
	jge	.LBB14_26
# BB#20:                                #   in Loop: Header=BB14_17 Depth=2
	addq	56(%rsp), %rax          # 8-byte Folded Reload
	movl	20(%rsp), %edx          # 4-byte Reload
	incl	%edx
	cmpl	4(%rsp), %edx           # 4-byte Folded Reload
	jl	.LBB14_17
# BB#21:                                # %._crit_edge96.loopexit
                                        #   in Loop: Header=BB14_14 Depth=1
	movq	%rax, ref_line(%rip)
	movq	%rsi, src_line(%rip)
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB14_25
	.p2align	4, 0x90
.LBB14_22:                              # %.preheader.preheader
                                        #   in Loop: Header=BB14_14 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,2), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB14_23:                              # %.preheader
                                        #   Parent Loop BB14_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	8(%rsp), %r14d          # 4-byte Folded Reload
	jge	.LBB14_28
# BB#24:                                #   in Loop: Header=BB14_23 Depth=2
	movq	%rax, ref_line(%rip)
	incl	%ecx
	addq	%rbx, %rax
	cmpl	4(%rsp), %ecx           # 4-byte Folded Reload
	jl	.LBB14_23
	.p2align	4, 0x90
.LBB14_25:                              # %._crit_edge96
                                        #   in Loop: Header=BB14_14 Depth=1
	incq	%rbp
	cmpq	$2, %rbp
	jl	.LBB14_14
	jmp	.LBB14_28
.LBB14_26:                              # %.thread.loopexit
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, ref_line(%rip)
	movq	%rsi, src_line(%rip)
	jmp	.LBB14_28
.LBB14_27:                              # %.thread.loopexit141
	movq	%r11, ref_line(%rip)
	movq	%rcx, src_line(%rip)
.LBB14_28:                              # %.thread
	movl	%r14d, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	computeSSEWP, .Lfunc_end14-computeSSEWP
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI15_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI15_1:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
.LCPI15_2:
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.text
	.globl	computeBiPredSSE1
	.p2align	4, 0x90
	.type	computeBiPredSSE1,@function
computeBiPredSSE1:                      # @computeBiPredSSE1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi161:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi162:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi163:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi164:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi166:
	.cfi_def_cfa_offset 96
.Lcfi167:
	.cfi_offset %rbx, -56
.Lcfi168:
	.cfi_offset %r12, -48
.Lcfi169:
	.cfi_offset %r13, -40
.Lcfi170:
	.cfi_offset %r14, -32
.Lcfi171:
	.cfi_offset %r15, -24
.Lcfi172:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movl	%r8d, 16(%rsp)          # 4-byte Spill
	movl	%ecx, %r15d
	movl	%edx, %r12d
	movl	%esi, %ebx
	movl	104(%rsp), %esi
	movl	96(%rsp), %edx
	movq	img(%rip), %rax
	movq	14232(%rax), %r13
	movslq	img_padded_size_x(%rip), %rbp
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, src_line(%rip)
	movslq	bipred2_access_method(%rip), %rax
	movq	ref_pic2_sub(%rip), %rdi
	callq	*get_line(,%rax,8)
	movq	%rax, ref2_line(%rip)
	movslq	bipred1_access_method(%rip), %rax
	movq	ref_pic1_sub(%rip), %rdi
	movl	%r14d, 20(%rsp)         # 4-byte Spill
	movl	%r14d, %esi
	movl	16(%rsp), %r14d         # 4-byte Reload
	movl	%r14d, %edx
	callq	*get_line(,%rax,8)
	movq	%rax, ref1_line(%rip)
	movl	%ebx, (%rsp)            # 4-byte Spill
	testl	%ebx, %ebx
	jle	.LBB15_12
# BB#1:                                 # %.preheader89.lr.ph
	movslq	%r12d, %rcx
	subq	%rcx, %rbp
	testl	%ecx, %ecx
	jle	.LBB15_9
# BB#2:                                 # %.preheader89.us.preheader
	leal	-1(%r12), %esi
	andl	$-4, %esi
	movq	src_line(%rip), %rdx
	movq	ref2_line(%rip), %rcx
	leaq	(%rsi,%rsi), %rdi
	leaq	8(%rdi,%rbp,2), %r11
	leaq	8(%rsi,%rsi), %rdi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	leaq	4(%rsi), %rsi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	xorl	%ebx, %ebx
	pxor	%xmm0, %xmm0
	movdqa	.LCPI15_0(%rip), %xmm1  # xmm1 = [1,1,1,1]
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB15_3:                               # %.preheader89.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_4 Depth 2
	movl	%r15d, %r9d
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	(%rax,%rsi,2), %r10
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB15_4:                               #   Parent Loop BB15_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rsi,2), %xmm2    # xmm2 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	movq	(%rax,%rsi,2), %xmm3    # xmm3 = mem[0],zero
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	movq	(%rcx,%rsi,2), %xmm4    # xmm4 = mem[0],zero
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	paddd	%xmm3, %xmm4
	paddd	%xmm1, %xmm4
	psrld	$1, %xmm4
	psubd	%xmm4, %xmm2
	pshufd	$78, %xmm2, %xmm3       # xmm3 = xmm2[2,3,0,1]
	movd	%xmm3, %rdi
	movd	%xmm2, %r14
	movslq	%r14d, %r15
	addl	(%r13,%r15,4), %ebx
	sarq	$32, %r14
	addl	(%r13,%r14,4), %ebx
	movslq	%edi, %rbp
	addl	(%r13,%rbp,4), %ebx
	sarq	$32, %rdi
	addl	(%r13,%rdi,4), %ebx
	addq	$4, %rsi
	cmpl	%r12d, %esi
	jl	.LBB15_4
# BB#5:                                 # %._crit_edge107.us
                                        #   in Loop: Header=BB15_3 Depth=1
	addq	24(%rsp), %rdx          # 8-byte Folded Reload
	movl	%r9d, %r15d
	cmpl	%r15d, %ebx
	jge	.LBB15_21
# BB#6:                                 #   in Loop: Header=BB15_3 Depth=1
	addq	%r11, %rcx
	addq	%r11, %rax
	incl	%r8d
	cmpl	(%rsp), %r8d            # 4-byte Folded Reload
	jl	.LBB15_3
# BB#7:                                 # %._crit_edge117.loopexit
	movq	%rdx, src_line(%rip)
	movq	%rax, ref1_line(%rip)
	movq	%rcx, ref2_line(%rip)
	movl	16(%rsp), %r14d         # 4-byte Reload
	cmpl	$0, ChromaMEEnable(%rip)
	jne	.LBB15_13
	jmp	.LBB15_35
.LBB15_9:                               # %.preheader89.preheader
	leaq	(%rax,%rbp,2), %rax
	addq	%rbp, %rbp
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB15_10:                              # %.preheader89
                                        # =>This Inner Loop Header: Depth=1
	testl	%r15d, %r15d
	jle	.LBB15_35
# BB#11:                                #   in Loop: Header=BB15_10 Depth=1
	addq	%rbp, ref2_line(%rip)
	movq	%rax, ref1_line(%rip)
	incl	%ecx
	addq	%rbp, %rax
	cmpl	(%rsp), %ecx            # 4-byte Folded Reload
	jl	.LBB15_10
.LBB15_12:
	xorl	%ebx, %ebx
	cmpl	$0, ChromaMEEnable(%rip)
	je	.LBB15_35
.LBB15_13:
	movb	shift_cr_x(%rip), %cl
	sarl	%cl, %r12d
	movb	shift_cr_y(%rip), %cl
	movl	(%rsp), %ebp            # 4-byte Reload
	sarl	%cl, %ebp
	movslq	img_cr_padded_size_x(%rip), %rax
	movslq	%r12d, %rcx
	subq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	decl	%ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leaq	2(%rcx,%rcx), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	512(%rax), %rax
	movq	%rax, src_line(%rip)
	movslq	bipred2_access_method(%rip), %rax
	movq	ref_pic2_sub+8(%rip), %rdi
	movl	104(%rsp), %esi
	movl	96(%rsp), %edx
	callq	*get_crline(,%rax,8)
	movq	%rax, ref2_line(%rip)
	movslq	bipred1_access_method(%rip), %rax
	movq	ref_pic1_sub+8(%rip), %rdi
	movl	20(%rsp), %esi          # 4-byte Reload
	movl	%r14d, %edx
	callq	*get_crline(,%rax,8)
	movq	%rax, ref1_line(%rip)
	movl	%ebp, %r14d
	testl	%ebp, %ebp
	jle	.LBB15_25
# BB#14:                                # %.preheader.lr.ph
	testl	%r12d, %r12d
	jle	.LBB15_22
# BB#15:                                # %.preheader.us.preheader
	movq	src_line(%rip), %rcx
	movq	ref2_line(%rip), %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	(%rdx,%rdx), %rdx
	movq	24(%rsp), %rdi          # 8-byte Reload
	leaq	4(%rdx,%rdi,4), %r10
	leaq	4(,%rdi,4), %r8
	movl	%r12d, %edx
	xorl	%r11d, %r11d
	pxor	%xmm0, %xmm0
	movdqa	.LCPI15_1(%rip), %xmm1  # xmm1 = [1,1]
	movdqa	.LCPI15_2(%rip), %xmm2  # xmm2 = [4294967295,0,4294967295,0]
	.p2align	4, 0x90
.LBB15_16:                              # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_17 Depth 2
	movq	(%rsp), %rdi            # 8-byte Reload
	leaq	(%rax,%rdi,2), %r9
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB15_17:                              #   Parent Loop BB15_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	(%rcx,%rdi,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	pshufd	$212, %xmm3, %xmm3      # xmm3 = xmm3[0,1,1,3]
	movd	(%rax,%rdi,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	pshufd	$212, %xmm4, %xmm4      # xmm4 = xmm4[0,1,1,3]
	movd	(%rsi,%rdi,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1],xmm5[2],xmm0[2],xmm5[3],xmm0[3]
	pshufd	$212, %xmm5, %xmm5      # xmm5 = xmm5[0,1,1,3]
	paddq	%xmm4, %xmm5
	paddq	%xmm1, %xmm5
	pand	%xmm2, %xmm5
	psrlq	$1, %xmm5
	psubq	%xmm5, %xmm3
	movd	%xmm3, %ebp
	movslq	%ebp, %rbp
	addl	(%r13,%rbp,4), %ebx
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	movd	%xmm3, %ebp
	movslq	%ebp, %rbp
	addl	(%r13,%rbp,4), %ebx
	incq	%rdi
	cmpl	%edi, %edx
	jne	.LBB15_17
# BB#18:                                # %._crit_edge.us
                                        #   in Loop: Header=BB15_16 Depth=1
	addq	%r8, %rcx
	cmpl	%r15d, %ebx
	jge	.LBB15_33
# BB#19:                                #   in Loop: Header=BB15_16 Depth=1
	addq	%r10, %rsi
	addq	%r10, %rax
	incl	%r11d
	cmpl	%r14d, %r11d
	jl	.LBB15_16
# BB#20:                                # %._crit_edge100.loopexit
	movq	%rcx, src_line(%rip)
	movq	%rax, ref1_line(%rip)
	movq	%rsi, ref2_line(%rip)
	jmp	.LBB15_25
.LBB15_21:                              # %.thread.loopexit150
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%rcx,%rax,2), %rax
	movq	%rdx, src_line(%rip)
	movq	%r10, ref1_line(%rip)
	jmp	.LBB15_34
.LBB15_22:                              # %.preheader.preheader
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rax,%rcx,2), %rax
	leaq	(%rcx,%rcx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB15_23:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r15d, %ebx
	jge	.LBB15_35
# BB#24:                                #   in Loop: Header=BB15_23 Depth=1
	addq	%rcx, ref2_line(%rip)
	movq	%rax, ref1_line(%rip)
	incl	%edx
	addq	%rcx, %rax
	cmpl	%r14d, %edx
	jl	.LBB15_23
.LBB15_25:                              # %._crit_edge100
	movq	32(%rsp), %rax          # 8-byte Reload
	addq	$1024, %rax             # imm = 0x400
	movq	%rax, src_line(%rip)
	movslq	bipred2_access_method(%rip), %rax
	movq	ref_pic2_sub+16(%rip), %rdi
	movl	104(%rsp), %esi
	movl	96(%rsp), %edx
	callq	*get_crline(,%rax,8)
	movq	%rax, ref2_line(%rip)
	movslq	bipred1_access_method(%rip), %rax
	movq	ref_pic1_sub+16(%rip), %rdi
	movl	20(%rsp), %esi          # 4-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	callq	*get_crline(,%rax,8)
	testl	%r14d, %r14d
	movq	%rax, ref1_line(%rip)
	jle	.LBB15_35
# BB#26:                                # %.preheader.lr.ph.1
	testl	%r12d, %r12d
	jle	.LBB15_36
# BB#27:                                # %.preheader.us.preheader.1
	movq	src_line(%rip), %rcx
	movq	ref2_line(%rip), %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	addq	%rdx, %rdx
	movq	24(%rsp), %rdi          # 8-byte Reload
	leaq	4(%rdx,%rdi,4), %r10
	leaq	4(,%rdi,4), %r8
	movl	%r12d, %ebp
	xorl	%r11d, %r11d
	pxor	%xmm0, %xmm0
	movdqa	.LCPI15_1(%rip), %xmm1  # xmm1 = [1,1]
	movdqa	.LCPI15_2(%rip), %xmm2  # xmm2 = [4294967295,0,4294967295,0]
	.p2align	4, 0x90
.LBB15_28:                              # %.preheader.us.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_29 Depth 2
	movq	(%rsp), %rdx            # 8-byte Reload
	leaq	(%rax,%rdx,2), %r9
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB15_29:                              #   Parent Loop BB15_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movd	(%rcx,%rdi,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	pshufd	$212, %xmm3, %xmm3      # xmm3 = xmm3[0,1,1,3]
	movd	(%rax,%rdi,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	pshufd	$212, %xmm4, %xmm4      # xmm4 = xmm4[0,1,1,3]
	movd	(%rsi,%rdi,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1],xmm5[2],xmm0[2],xmm5[3],xmm0[3]
	pshufd	$212, %xmm5, %xmm5      # xmm5 = xmm5[0,1,1,3]
	paddq	%xmm4, %xmm5
	paddq	%xmm1, %xmm5
	pand	%xmm2, %xmm5
	psrlq	$1, %xmm5
	psubq	%xmm5, %xmm3
	movd	%xmm3, %edx
	movslq	%edx, %rdx
	addl	(%r13,%rdx,4), %ebx
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	movd	%xmm3, %edx
	movslq	%edx, %rdx
	addl	(%r13,%rdx,4), %ebx
	incq	%rdi
	cmpl	%edi, %ebp
	jne	.LBB15_29
# BB#30:                                # %._crit_edge.us.1
                                        #   in Loop: Header=BB15_28 Depth=1
	addq	%r8, %rcx
	cmpl	%r15d, %ebx
	jge	.LBB15_33
# BB#31:                                #   in Loop: Header=BB15_28 Depth=1
	addq	%r10, %rsi
	addq	%r10, %rax
	incl	%r11d
	cmpl	%r14d, %r11d
	jl	.LBB15_28
# BB#32:                                # %._crit_edge100.loopexit.1
	movq	%rcx, src_line(%rip)
	movq	%rax, ref1_line(%rip)
	movq	%rsi, ref2_line(%rip)
	jmp	.LBB15_35
.LBB15_33:
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%rsi,%rax,2), %rax
	movq	%rcx, src_line(%rip)
	movq	%r9, ref1_line(%rip)
.LBB15_34:                              # %.thread
	movq	%rax, ref2_line(%rip)
.LBB15_35:                              # %.thread
	movl	%ebx, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_36:                              # %.preheader.preheader.1
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	(%rax,%rdx,2), %rax
	addq	%rdx, %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB15_37:                              # %.preheader.1
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r15d, %ebx
	jge	.LBB15_35
# BB#38:                                #   in Loop: Header=BB15_37 Depth=1
	addq	%rdx, ref2_line(%rip)
	movq	%rax, ref1_line(%rip)
	incl	%ecx
	addq	%rdx, %rax
	cmpl	%r14d, %ecx
	jl	.LBB15_37
	jmp	.LBB15_35
.Lfunc_end15:
	.size	computeBiPredSSE1, .Lfunc_end15-computeBiPredSSE1
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI16_0:
	.short	65535                   # 0xffff
	.short	0                       # 0x0
	.short	65535                   # 0xffff
	.short	0                       # 0x0
	.short	65535                   # 0xffff
	.short	0                       # 0x0
	.short	65535                   # 0xffff
	.short	0                       # 0x0
.LCPI16_1:
	.zero	16
	.text
	.globl	computeBiPredSSE2
	.p2align	4, 0x90
	.type	computeBiPredSSE2,@function
computeBiPredSSE2:                      # @computeBiPredSSE2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi173:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi174:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi175:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi176:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi177:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi178:
	.cfi_def_cfa_offset 56
	subq	$360, %rsp              # imm = 0x168
.Lcfi179:
	.cfi_def_cfa_offset 416
.Lcfi180:
	.cfi_offset %rbx, -56
.Lcfi181:
	.cfi_offset %r12, -48
.Lcfi182:
	.cfi_offset %r13, -40
.Lcfi183:
	.cfi_offset %r14, -32
.Lcfi184:
	.cfi_offset %r15, -24
.Lcfi185:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebx
	movl	%r8d, %r12d
	movl	%ecx, %r13d
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	%esi, %r14d
	movl	424(%rsp), %esi
	movl	416(%rsp), %edx
	movl	luma_log_weight_denom(%rip), %r15d
	incl	%r15d
	movl	wp_luma_round(%rip), %ebp
	addl	%ebp, %ebp
	movslq	img_padded_size_x(%rip), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	%rdi, 344(%rsp)         # 8-byte Spill
	movq	%rdi, src_line(%rip)
	movslq	bipred2_access_method(%rip), %rax
	movq	ref_pic2_sub(%rip), %rdi
	callq	*get_line(,%rax,8)
	movq	%rax, ref2_line(%rip)
	movslq	bipred1_access_method(%rip), %rax
	movq	ref_pic1_sub(%rip), %rdi
	movl	%ebx, %esi
	movl	%r12d, 28(%rsp)         # 4-byte Spill
	movl	%r12d, %edx
	callq	*get_line(,%rax,8)
	movq	%rax, %r11
	movq	%r11, ref1_line(%rip)
	movl	%r14d, %r8d
	testl	%r14d, %r14d
	movl	%r13d, 36(%rsp)         # 4-byte Spill
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	movl	%r15d, 16(%rsp)         # 4-byte Spill
	movl	%ebx, 32(%rsp)          # 4-byte Spill
	jle	.LBB16_1
# BB#2:                                 # %.preheader133.lr.ph
	movl	12(%rsp), %esi          # 4-byte Reload
	movslq	%esi, %r12
	movq	112(%rsp), %r14         # 8-byte Reload
	subq	%r12, %r14
	movswl	weight1(%rip), %eax
	movswl	weight2(%rip), %edi
	movq	img(%rip), %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movswl	offsetBi(%rip), %edx
	decl	%r12d
	movl	%r12d, %ebx
	andl	$-4, %ebx
	addq	$4, %rbx
	shrl	$2, %r12d
	incl	%r12d
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm1        # xmm1 = xmm0[0,0,0,0]
	movd	%ebp, %xmm0
	pshufd	$0, %xmm0, %xmm8        # xmm8 = xmm0[0,0,0,0]
	movd	%edi, %xmm0
	pshufd	$0, %xmm0, %xmm15       # xmm15 = xmm0[0,0,0,0]
	movd	%r15d, %xmm4
	movd	%edx, %xmm0
	pshufd	$0, %xmm0, %xmm11       # xmm11 = xmm0[0,0,0,0]
	movl	%r12d, %eax
	andl	$3, %eax
	movq	%r12, %rdi
	movq	%rax, 40(%rsp)          # 8-byte Spill
	subq	%rax, %rdi
	leaq	(,%rdi,4), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leal	(,%rdi,4), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	xorl	%r15d, %r15d
	pxor	%xmm13, %xmm13
	xorps	%xmm5, %xmm5
	movss	%xmm4, %xmm5            # xmm5 = xmm4[0],xmm5[1,2,3]
	movdqa	%xmm1, %xmm7
	pshufd	$245, %xmm1, %xmm0      # xmm0 = xmm1[1,1,3,3]
	movdqa	%xmm0, 112(%rsp)        # 16-byte Spill
	pshufd	$245, %xmm15, %xmm6     # xmm6 = xmm15[1,1,3,3]
	xorl	%edx, %edx
	movl	%esi, %ebp
	movl	%r8d, %esi
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	movl	%esi, 24(%rsp)          # 4-byte Spill
	movq	%r12, 64(%rsp)          # 8-byte Spill
	movdqa	%xmm7, 176(%rsp)        # 16-byte Spill
	movdqa	%xmm8, 160(%rsp)        # 16-byte Spill
	movdqa	%xmm15, 144(%rsp)       # 16-byte Spill
	movaps	%xmm4, 192(%rsp)        # 16-byte Spill
	movdqa	%xmm11, 128(%rsp)       # 16-byte Spill
	movaps	%xmm5, 304(%rsp)        # 16-byte Spill
	movdqa	%xmm6, 96(%rsp)         # 16-byte Spill
	movq	%rbx, 352(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB16_3:                               # %.preheader133
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_8 Depth 2
                                        #     Child Loop BB16_11 Depth 2
	testl	%ebp, %ebp
	jle	.LBB16_13
# BB#4:                                 # %.lr.ph150
                                        #   in Loop: Header=BB16_3 Depth=1
	xorl	%esi, %esi
	cmpl	$4, %r12d
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	15520(%rax), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	ref2_line(%rip), %rcx
	movq	src_line(%rip), %r8
	leaq	(%rcx,%rbx,2), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	jb	.LBB16_5
# BB#6:                                 # %min.iters.checked
                                        #   in Loop: Header=BB16_3 Depth=1
	testq	%rdi, %rdi
	je	.LBB16_5
# BB#7:                                 # %vector.ph
                                        #   in Loop: Header=BB16_3 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%r8,%rax,2), %r10
	leaq	(%rcx,%rax,2), %r9
	leaq	(%r11,%rax,2), %rax
	movd	%r15d, %xmm10
	movd	12(%rsp), %xmm0         # 4-byte Folded Reload
                                        # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	%xmm0, 208(%rsp)        # 16-byte Spill
	movq	%r8, %rbx
	movq	%r11, %rsi
	.p2align	4, 0x90
.LBB16_8:                               # %vector.body
                                        #   Parent Loop BB16_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	pxor	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movdqu	(%rsi), %xmm2
	movdqa	%xmm2, 288(%rsp)        # 16-byte Spill
	movdqu	16(%rsi), %xmm1
	movdqa	%xmm1, 272(%rsp)        # 16-byte Spill
	pshufd	$232, %xmm1, %xmm6      # xmm6 = xmm1[0,2,2,3]
	pshuflw	$232, %xmm6, %xmm1      # xmm1 = xmm6[0,2,2,3,4,5,6,7]
	pshufd	$232, %xmm2, %xmm12     # xmm12 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm12, %xmm2     # xmm2 = xmm12[0,2,2,3,4,5,6,7]
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	punpcklwd	%xmm13, %xmm2   # xmm2 = xmm2[0],xmm13[0],xmm2[1],xmm13[1],xmm2[2],xmm13[2],xmm2[3],xmm13[3]
	pshufd	$245, %xmm2, %xmm3      # xmm3 = xmm2[1,1,3,3]
	pmuludq	%xmm7, %xmm2
	pshufd	$232, %xmm2, %xmm1      # xmm1 = xmm2[0,2,2,3]
	movdqa	112(%rsp), %xmm7        # 16-byte Reload
	pmuludq	%xmm7, %xmm3
	pshufd	$232, %xmm3, %xmm2      # xmm2 = xmm3[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movdqu	(%rcx), %xmm2
	movdqa	%xmm2, 256(%rsp)        # 16-byte Spill
	movdqu	16(%rcx), %xmm3
	movdqa	%xmm3, 320(%rsp)        # 16-byte Spill
	pshufd	$232, %xmm3, %xmm5      # xmm5 = xmm3[0,2,2,3]
	pshuflw	$232, %xmm5, %xmm9      # xmm9 = xmm5[0,2,2,3,4,5,6,7]
	pshufd	$232, %xmm2, %xmm14     # xmm14 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm14, %xmm3     # xmm3 = xmm14[0,2,2,3,4,5,6,7]
	punpckldq	%xmm9, %xmm3    # xmm3 = xmm3[0],xmm9[0],xmm3[1],xmm9[1]
	punpcklwd	%xmm13, %xmm3   # xmm3 = xmm3[0],xmm13[0],xmm3[1],xmm13[1],xmm3[2],xmm13[2],xmm3[3],xmm13[3]
	pshufd	$245, %xmm3, %xmm9      # xmm9 = xmm3[1,1,3,3]
	pmuludq	%xmm15, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pmuludq	96(%rsp), %xmm9         # 16-byte Folded Reload
	pshufd	$232, %xmm9, %xmm4      # xmm4 = xmm9[0,2,2,3]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	paddd	%xmm8, %xmm1
	paddd	%xmm3, %xmm1
	psrad	%xmm0, %xmm1
	movdqa	%xmm0, %xmm2
	paddd	%xmm11, %xmm1
	movdqa	%xmm1, %xmm3
	pcmpgtd	%xmm13, %xmm3
	pand	%xmm1, %xmm3
	movdqa	208(%rsp), %xmm9        # 16-byte Reload
	movdqa	%xmm9, %xmm1
	pcmpgtd	%xmm3, %xmm1
	pand	%xmm1, %xmm3
	pandn	%xmm9, %xmm1
	por	%xmm3, %xmm1
	movdqu	(%rbx), %xmm0
	movdqa	%xmm0, 240(%rsp)        # 16-byte Spill
	movdqu	16(%rbx), %xmm3
	movdqa	%xmm3, 224(%rsp)        # 16-byte Spill
	pshufd	$232, %xmm3, %xmm15     # xmm15 = xmm3[0,2,2,3]
	pshuflw	$232, %xmm15, %xmm4     # xmm4 = xmm15[0,2,2,3,4,5,6,7]
	pshufd	$232, %xmm0, %xmm11     # xmm11 = xmm0[0,2,2,3]
	pshuflw	$232, %xmm11, %xmm8     # xmm8 = xmm11[0,2,2,3,4,5,6,7]
	punpckldq	%xmm4, %xmm8    # xmm8 = xmm8[0],xmm4[0],xmm8[1],xmm4[1]
	punpcklwd	%xmm13, %xmm8   # xmm8 = xmm8[0],xmm13[0],xmm8[1],xmm13[1],xmm8[2],xmm13[2],xmm8[3],xmm13[3]
	psubd	%xmm1, %xmm8
	pshufd	$245, %xmm8, %xmm4      # xmm4 = xmm8[1,1,3,3]
	pmuludq	%xmm8, %xmm8
	pshufd	$232, %xmm8, %xmm1      # xmm1 = xmm8[0,2,2,3]
	pmuludq	%xmm4, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	paddd	%xmm10, %xmm1
	pshuflw	$237, %xmm6, %xmm4      # xmm4 = xmm6[1,3,2,3,4,5,6,7]
	pshuflw	$237, %xmm12, %xmm6     # xmm6 = xmm12[1,3,2,3,4,5,6,7]
	punpckldq	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	punpcklwd	%xmm13, %xmm6   # xmm6 = xmm6[0],xmm13[0],xmm6[1],xmm13[1],xmm6[2],xmm13[2],xmm6[3],xmm13[3]
	pshufd	$245, %xmm6, %xmm4      # xmm4 = xmm6[1,1,3,3]
	pmuludq	176(%rsp), %xmm6        # 16-byte Folded Reload
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pmuludq	%xmm7, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	pshuflw	$237, %xmm5, %xmm4      # xmm4 = xmm5[1,3,2,3,4,5,6,7]
	pshuflw	$237, %xmm14, %xmm5     # xmm5 = xmm14[1,3,2,3,4,5,6,7]
	movdqa	%xmm7, %xmm14
	punpckldq	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	punpcklwd	%xmm13, %xmm5   # xmm5 = xmm5[0],xmm13[0],xmm5[1],xmm13[1],xmm5[2],xmm13[2],xmm5[3],xmm13[3]
	pshufd	$245, %xmm5, %xmm4      # xmm4 = xmm5[1,1,3,3]
	pmuludq	144(%rsp), %xmm5        # 16-byte Folded Reload
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	movdqa	96(%rsp), %xmm0         # 16-byte Reload
	pmuludq	%xmm0, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	paddd	160(%rsp), %xmm6        # 16-byte Folded Reload
	paddd	%xmm5, %xmm6
	movdqa	%xmm2, %xmm3
	psrad	%xmm3, %xmm6
	paddd	128(%rsp), %xmm6        # 16-byte Folded Reload
	movdqa	%xmm6, %xmm4
	pcmpgtd	%xmm13, %xmm4
	pand	%xmm6, %xmm4
	movdqa	%xmm9, %xmm5
	pcmpgtd	%xmm4, %xmm5
	pand	%xmm5, %xmm4
	pandn	%xmm9, %xmm5
	por	%xmm4, %xmm5
	pshuflw	$237, %xmm15, %xmm4     # xmm4 = xmm15[1,3,2,3,4,5,6,7]
	pshuflw	$237, %xmm11, %xmm6     # xmm6 = xmm11[1,3,2,3,4,5,6,7]
	punpckldq	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	punpcklwd	%xmm13, %xmm6   # xmm6 = xmm6[0],xmm13[0],xmm6[1],xmm13[1],xmm6[2],xmm13[2],xmm6[3],xmm13[3]
	psubd	%xmm5, %xmm6
	pshufd	$245, %xmm6, %xmm4      # xmm4 = xmm6[1,1,3,3]
	pmuludq	%xmm6, %xmm6
	pshufd	$232, %xmm6, %xmm15     # xmm15 = xmm6[0,2,2,3]
	pmuludq	%xmm4, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm15   # xmm15 = xmm15[0],xmm4[0],xmm15[1],xmm4[1]
	pshufd	$231, 272(%rsp), %xmm10 # 16-byte Folded Reload
                                        # xmm10 = mem[3,1,2,3]
	pshuflw	$226, %xmm10, %xmm4     # xmm4 = xmm10[2,0,2,3,4,5,6,7]
	pshufd	$231, 288(%rsp), %xmm12 # 16-byte Folded Reload
                                        # xmm12 = mem[3,1,2,3]
	pshuflw	$226, %xmm12, %xmm5     # xmm5 = xmm12[2,0,2,3,4,5,6,7]
	punpckldq	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	punpcklwd	%xmm13, %xmm5   # xmm5 = xmm5[0],xmm13[0],xmm5[1],xmm13[1],xmm5[2],xmm13[2],xmm5[3],xmm13[3]
	pshufd	$245, %xmm5, %xmm4      # xmm4 = xmm5[1,1,3,3]
	pmuludq	176(%rsp), %xmm5        # 16-byte Folded Reload
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pmuludq	%xmm14, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	pshufd	$231, 320(%rsp), %xmm8  # 16-byte Folded Reload
                                        # xmm8 = mem[3,1,2,3]
	pshuflw	$226, %xmm8, %xmm2      # xmm2 = xmm8[2,0,2,3,4,5,6,7]
	pshufd	$231, 256(%rsp), %xmm11 # 16-byte Folded Reload
                                        # xmm11 = mem[3,1,2,3]
	pshuflw	$226, %xmm11, %xmm7     # xmm7 = xmm11[2,0,2,3,4,5,6,7]
	punpckldq	%xmm2, %xmm7    # xmm7 = xmm7[0],xmm2[0],xmm7[1],xmm2[1]
	punpcklwd	%xmm13, %xmm7   # xmm7 = xmm7[0],xmm13[0],xmm7[1],xmm13[1],xmm7[2],xmm13[2],xmm7[3],xmm13[3]
	pshufd	$245, %xmm7, %xmm2      # xmm2 = xmm7[1,1,3,3]
	pmuludq	144(%rsp), %xmm7        # 16-byte Folded Reload
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pmuludq	%xmm0, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm7    # xmm7 = xmm7[0],xmm2[0],xmm7[1],xmm2[1]
	paddd	160(%rsp), %xmm5        # 16-byte Folded Reload
	paddd	%xmm7, %xmm5
	movdqa	%xmm3, %xmm0
	psrad	%xmm0, %xmm5
	paddd	128(%rsp), %xmm5        # 16-byte Folded Reload
	movdqa	%xmm5, %xmm2
	pcmpgtd	%xmm13, %xmm2
	pand	%xmm5, %xmm2
	movdqa	%xmm9, %xmm7
	pcmpgtd	%xmm2, %xmm7
	pand	%xmm7, %xmm2
	pandn	%xmm9, %xmm7
	por	%xmm2, %xmm7
	pshufd	$231, 224(%rsp), %xmm5  # 16-byte Folded Reload
                                        # xmm5 = mem[3,1,2,3]
	movdqa	176(%rsp), %xmm6        # 16-byte Reload
	pshuflw	$226, %xmm5, %xmm2      # xmm2 = xmm5[2,0,2,3,4,5,6,7]
	pshufd	$231, 240(%rsp), %xmm3  # 16-byte Folded Reload
                                        # xmm3 = mem[3,1,2,3]
	pshuflw	$226, %xmm3, %xmm4      # xmm4 = xmm3[2,0,2,3,4,5,6,7]
	punpckldq	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	punpcklwd	%xmm13, %xmm4   # xmm4 = xmm4[0],xmm13[0],xmm4[1],xmm13[1],xmm4[2],xmm13[2],xmm4[3],xmm13[3]
	psubd	%xmm7, %xmm4
	pshufd	$245, %xmm4, %xmm7      # xmm7 = xmm4[1,1,3,3]
	pmuludq	%xmm4, %xmm4
	pshufd	$232, %xmm4, %xmm2      # xmm2 = xmm4[0,2,2,3]
	pmuludq	%xmm7, %xmm7
	pshufd	$232, %xmm7, %xmm4      # xmm4 = xmm7[0,2,2,3]
	punpckldq	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	paddd	%xmm15, %xmm2
	paddd	%xmm1, %xmm2
	pshuflw	$231, %xmm10, %xmm1     # xmm1 = xmm10[3,1,2,3,4,5,6,7]
	pshuflw	$231, %xmm12, %xmm4     # xmm4 = xmm12[3,1,2,3,4,5,6,7]
	punpckldq	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	punpcklwd	%xmm13, %xmm4   # xmm4 = xmm4[0],xmm13[0],xmm4[1],xmm13[1],xmm4[2],xmm13[2],xmm4[3],xmm13[3]
	pshufd	$245, %xmm4, %xmm1      # xmm1 = xmm4[1,1,3,3]
	movdqa	%xmm6, %xmm7
	pmuludq	%xmm6, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm14, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	pshuflw	$231, %xmm8, %xmm1      # xmm1 = xmm8[3,1,2,3,4,5,6,7]
	movdqa	144(%rsp), %xmm15       # 16-byte Reload
	movdqa	160(%rsp), %xmm8        # 16-byte Reload
	pshuflw	$231, %xmm11, %xmm6     # xmm6 = xmm11[3,1,2,3,4,5,6,7]
	movdqa	128(%rsp), %xmm11       # 16-byte Reload
	punpckldq	%xmm1, %xmm6    # xmm6 = xmm6[0],xmm1[0],xmm6[1],xmm1[1]
	punpcklwd	%xmm13, %xmm6   # xmm6 = xmm6[0],xmm13[0],xmm6[1],xmm13[1],xmm6[2],xmm13[2],xmm6[3],xmm13[3]
	pshufd	$245, %xmm6, %xmm1      # xmm1 = xmm6[1,1,3,3]
	pmuludq	%xmm15, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pmuludq	96(%rsp), %xmm1         # 16-byte Folded Reload
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm6    # xmm6 = xmm6[0],xmm1[0],xmm6[1],xmm1[1]
	paddd	%xmm8, %xmm4
	paddd	%xmm6, %xmm4
	psrad	%xmm0, %xmm4
	paddd	%xmm11, %xmm4
	movdqa	%xmm4, %xmm0
	pcmpgtd	%xmm13, %xmm0
	pand	%xmm4, %xmm0
	movaps	192(%rsp), %xmm4        # 16-byte Reload
	movdqa	%xmm9, %xmm1
	pcmpgtd	%xmm0, %xmm1
	pand	%xmm1, %xmm0
	pandn	%xmm9, %xmm1
	por	%xmm0, %xmm1
	pshuflw	$231, %xmm5, %xmm0      # xmm0 = xmm5[3,1,2,3,4,5,6,7]
	pshuflw	$231, %xmm3, %xmm3      # xmm3 = xmm3[3,1,2,3,4,5,6,7]
	punpckldq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	punpcklwd	%xmm13, %xmm3   # xmm3 = xmm3[0],xmm13[0],xmm3[1],xmm13[1],xmm3[2],xmm13[2],xmm3[3],xmm13[3]
	psubd	%xmm1, %xmm3
	pshufd	$245, %xmm3, %xmm0      # xmm0 = xmm3[1,1,3,3]
	pmuludq	%xmm3, %xmm3
	pshufd	$232, %xmm3, %xmm10     # xmm10 = xmm3[0,2,2,3]
	pmuludq	%xmm0, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm10   # xmm10 = xmm10[0],xmm0[0],xmm10[1],xmm0[1]
	paddd	%xmm2, %xmm10
	addq	$32, %rsi
	addq	$32, %rcx
	addq	$32, %rbx
	addq	$-4, %rdi
	jne	.LBB16_8
# BB#9:                                 # %middle.block
                                        #   in Loop: Header=BB16_3 Depth=1
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	pshufd	$78, %xmm10, %xmm0      # xmm0 = xmm10[2,3,0,1]
	paddd	%xmm10, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %r15d
	movl	48(%rsp), %esi          # 4-byte Reload
	movl	36(%rsp), %r13d         # 4-byte Reload
	movq	64(%rsp), %r12          # 8-byte Reload
	movaps	304(%rsp), %xmm5        # 16-byte Reload
	movdqa	96(%rsp), %xmm6         # 16-byte Reload
	movq	352(%rsp), %rbx         # 8-byte Reload
	jne	.LBB16_10
	jmp	.LBB16_12
	.p2align	4, 0x90
.LBB16_5:                               #   in Loop: Header=BB16_3 Depth=1
	movq	%r8, %r10
	movq	%rcx, %r9
	movq	%r11, %rax
.LBB16_10:                              # %scalar.ph
                                        #   in Loop: Header=BB16_3 Depth=1
	movd	12(%rsp), %xmm0         # 4-byte Folded Reload
                                        # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	.p2align	4, 0x90
.LBB16_11:                              #   Parent Loop BB16_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %xmm1           # xmm1 = mem[0],zero
	punpcklwd	%xmm13, %xmm1   # xmm1 = xmm1[0],xmm13[0],xmm1[1],xmm13[1],xmm1[2],xmm13[2],xmm1[3],xmm13[3]
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm7, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pmuludq	112(%rsp), %xmm2        # 16-byte Folded Reload
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movq	(%r9), %xmm2            # xmm2 = mem[0],zero
	punpcklwd	%xmm13, %xmm2   # xmm2 = xmm2[0],xmm13[0],xmm2[1],xmm13[1],xmm2[2],xmm13[2],xmm2[3],xmm13[3]
	pshufd	$245, %xmm2, %xmm3      # xmm3 = xmm2[1,1,3,3]
	pmuludq	%xmm15, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pmuludq	%xmm6, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	paddd	%xmm8, %xmm1
	paddd	%xmm2, %xmm1
	psrad	%xmm5, %xmm1
	paddd	%xmm11, %xmm1
	movdqa	%xmm1, %xmm2
	pcmpgtd	%xmm13, %xmm2
	pand	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	pcmpgtd	%xmm2, %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm0, %xmm1
	por	%xmm2, %xmm1
	movq	(%r10), %xmm2           # xmm2 = mem[0],zero
	punpcklwd	%xmm13, %xmm2   # xmm2 = xmm2[0],xmm13[0],xmm2[1],xmm13[1],xmm2[2],xmm13[2],xmm2[3],xmm13[3]
	psubd	%xmm1, %xmm2
	pshufd	$245, %xmm2, %xmm1      # xmm1 = xmm2[1,1,3,3]
	pmuludq	%xmm2, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pmuludq	%xmm1, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	addq	$8, %rax
	addq	$8, %r9
	addq	$8, %r10
	pshufd	$78, %xmm2, %xmm1       # xmm1 = xmm2[2,3,0,1]
	paddd	%xmm2, %xmm1
	pshufd	$229, %xmm1, %xmm2      # xmm2 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm2
	movd	%xmm2, %ecx
	addl	%ecx, %r15d
	addl	$4, %esi
	cmpl	%ebp, %esi
	jl	.LBB16_11
.LBB16_12:                              # %._crit_edge151
                                        #   in Loop: Header=BB16_3 Depth=1
	leaq	(%r11,%rbx,2), %r11
	leaq	(%r8,%rbx,2), %rax
	movq	%r11, ref1_line(%rip)
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, ref2_line(%rip)
	movq	%rax, src_line(%rip)
	movq	72(%rsp), %rdi          # 8-byte Reload
	movl	24(%rsp), %esi          # 4-byte Reload
.LBB16_13:                              #   in Loop: Header=BB16_3 Depth=1
	cmpl	%r13d, %r15d
	jge	.LBB16_36
# BB#14:                                #   in Loop: Header=BB16_3 Depth=1
	leaq	(%r14,%r14), %rax
	addq	%rax, ref2_line(%rip)
	leaq	(%r11,%r14,2), %r11
	movq	%r11, ref1_line(%rip)
	incl	%edx
	cmpl	%esi, %edx
	jl	.LBB16_3
	jmp	.LBB16_15
.LBB16_1:
	xorl	%r15d, %r15d
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	%r8d, %esi
.LBB16_15:                              # %._crit_edge161
	cmpl	$0, ChromaMEEnable(%rip)
	je	.LBB16_36
# BB#16:
	movb	shift_cr_x(%rip), %cl
	sarl	%cl, %ebp
	movb	shift_cr_y(%rip), %cl
	sarl	%cl, %esi
	movl	%esi, 24(%rsp)          # 4-byte Spill
	movslq	img_cr_padded_size_x(%rip), %rcx
	movslq	%ebp, %rax
	subq	%rax, %rcx
	decl	%eax
	leaq	2(%rax,%rax), %rdx
	movq	%rdx, 304(%rsp)         # 8-byte Spill
	leaq	1(%rax), %rdx
	movabsq	$8589934588, %rsi       # imm = 0x1FFFFFFFC
	movq	%rdx, 256(%rsp)         # 8-byte Spill
	andq	%rdx, %rsi
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	leaq	(%rsi,%rsi), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movd	20(%rsp), %xmm0         # 4-byte Folded Reload
                                        # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm1        # xmm1 = xmm0[0,0,0,0]
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rcx), %rsi
	leaq	4(%rsi,%rax,4), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	leaq	4(,%rax,4), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	pxor	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	xorl	%edx, %edx
	movl	32(%rsp), %r14d         # 4-byte Reload
	movl	28(%rsp), %r12d         # 4-byte Reload
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movdqa	%xmm1, 160(%rsp)        # 16-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movaps	%xmm2, 144(%rsp)        # 16-byte Spill
.LBB16_17:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_20 Depth 2
                                        #     Child Loop BB16_23 Depth 2
                                        #       Child Loop BB16_29 Depth 3
                                        #       Child Loop BB16_26 Depth 3
	movl	$256, %eax              # imm = 0x100
	movl	%edx, %ecx
	shll	%cl, %eax
	cltq
	movq	344(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,2), %rax
	movq	%rax, src_line(%rip)
	movslq	bipred2_access_method(%rip), %rax
	movq	ref_pic2_sub+8(,%rdx,8), %rdi
	movl	424(%rsp), %esi
	movq	%rdx, %rbp
	movl	416(%rsp), %edx
	callq	*get_crline(,%rax,8)
	movq	%rax, ref2_line(%rip)
	movslq	bipred1_access_method(%rip), %rax
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	ref_pic1_sub+8(,%rbp,8), %rdi
	movl	%r14d, %esi
	movl	%r12d, %edx
	callq	*get_crline(,%rax,8)
	movq	%rax, %r9
	movq	%r9, ref1_line(%rip)
	movl	24(%rsp), %edx          # 4-byte Reload
	testl	%edx, %edx
	jle	.LBB16_34
# BB#18:                                # %.preheader.lr.ph
                                        #   in Loop: Header=BB16_17 Depth=1
	movl	12(%rsp), %ebp          # 4-byte Reload
	testl	%ebp, %ebp
	jle	.LBB16_19
# BB#22:                                # %.preheader.us.preheader
                                        #   in Loop: Header=BB16_17 Depth=1
	movq	img(%rip), %rax
	movl	15524(%rax), %r12d
	movq	ref2_line(%rip), %r11
	movq	src_line(%rip), %r10
	movq	40(%rsp), %rax          # 8-byte Reload
	movswl	weight1_cr(%rax,%rax), %ecx
	movswl	weight2_cr(%rax,%rax), %edx
	movswl	offsetBi_cr(%rax,%rax), %eax
	movd	%ecx, %xmm0
	pshufd	$0, %xmm0, %xmm2        # xmm2 = xmm0[0,0,0,0]
	movd	%edx, %xmm0
	pshufd	$0, %xmm0, %xmm3        # xmm3 = xmm0[0,0,0,0]
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm1        # xmm1 = xmm0[0,0,0,0]
	movd	%r12d, %xmm0
	pshufd	$0, %xmm0, %xmm15       # xmm15 = xmm0[0,0,0,0]
	xorl	%edi, %edi
	movl	%ecx, 240(%rsp)         # 4-byte Spill
	movl	%edx, 224(%rsp)         # 4-byte Spill
	movl	%eax, 208(%rsp)         # 4-byte Spill
	movl	%r12d, 192(%rsp)        # 4-byte Spill
	movdqa	%xmm1, 112(%rsp)        # 16-byte Spill
	movdqa	%xmm2, 96(%rsp)         # 16-byte Spill
	movdqa	%xmm3, 176(%rsp)        # 16-byte Spill
	.p2align	4, 0x90
.LBB16_23:                              # %.preheader.us
                                        #   Parent Loop BB16_17 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB16_29 Depth 3
                                        #       Child Loop BB16_26 Depth 3
	cmpq	$3, 256(%rsp)           # 8-byte Folded Reload
	movq	%r9, 288(%rsp)          # 8-byte Spill
	movq	%r10, 128(%rsp)         # 8-byte Spill
	movq	%r11, 272(%rsp)         # 8-byte Spill
	movl	%edi, 320(%rsp)         # 4-byte Spill
	jbe	.LBB16_24
# BB#27:                                # %min.iters.checked255
                                        #   in Loop: Header=BB16_23 Depth=2
	movq	72(%rsp), %r8           # 8-byte Reload
	testq	%r8, %r8
	je	.LBB16_24
# BB#28:                                # %vector.ph259
                                        #   in Loop: Header=BB16_23 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%r10,%rax,2), %rbp
	leaq	(%r11,%rax,2), %r14
	leaq	(%r9,%rax,2), %r13
	movd	%r15d, %xmm4
	xorl	%ecx, %ecx
	movdqa	.LCPI16_0(%rip), %xmm9  # xmm9 = [65535,0,65535,0,65535,0,65535,0]
	.p2align	4, 0x90
.LBB16_29:                              # %vector.body251
                                        #   Parent Loop BB16_17 Depth=1
                                        #     Parent Loop BB16_23 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	(%r9,%rcx,4), %xmm5
	movdqa	%xmm5, %xmm0
	pand	%xmm9, %xmm0
	pshufd	$245, %xmm0, %xmm1      # xmm1 = xmm0[1,1,3,3]
	movdqa	96(%rsp), %xmm13        # 16-byte Reload
	pmuludq	%xmm13, %xmm0
	pshufd	$232, %xmm0, %xmm2      # xmm2 = xmm0[0,2,2,3]
	pshufd	$245, %xmm13, %xmm12    # xmm12 = xmm13[1,1,3,3]
	pmuludq	%xmm12, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movdqu	(%r11,%rcx,4), %xmm6
	movdqa	%xmm6, %xmm1
	pand	%xmm9, %xmm1
	pshufd	$245, %xmm1, %xmm7      # xmm7 = xmm1[1,1,3,3]
	movdqa	176(%rsp), %xmm14       # 16-byte Reload
	pmuludq	%xmm14, %xmm1
	pshufd	$232, %xmm1, %xmm3      # xmm3 = xmm1[0,2,2,3]
	pshufd	$245, %xmm14, %xmm1     # xmm1 = xmm14[1,1,3,3]
	pmuludq	%xmm1, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	punpckldq	%xmm7, %xmm3    # xmm3 = xmm3[0],xmm7[0],xmm3[1],xmm7[1]
	movdqa	160(%rsp), %xmm8        # 16-byte Reload
	paddd	%xmm8, %xmm2
	paddd	%xmm3, %xmm2
	movdqa	144(%rsp), %xmm10       # 16-byte Reload
	psrad	%xmm10, %xmm2
	movdqa	112(%rsp), %xmm11       # 16-byte Reload
	paddd	%xmm11, %xmm2
	movdqa	%xmm2, %xmm3
	pxor	%xmm0, %xmm0
	pcmpgtd	%xmm0, %xmm3
	pand	%xmm2, %xmm3
	movdqa	%xmm15, %xmm2
	pcmpgtd	%xmm3, %xmm2
	pand	%xmm2, %xmm3
	pandn	%xmm15, %xmm2
	por	%xmm3, %xmm2
	movdqu	(%r10,%rcx,4), %xmm7
	movdqa	%xmm7, %xmm3
	pand	%xmm9, %xmm3
	psubd	%xmm2, %xmm3
	pshufd	$245, %xmm3, %xmm0      # xmm0 = xmm3[1,1,3,3]
	pmuludq	%xmm3, %xmm3
	pshufd	$232, %xmm3, %xmm2      # xmm2 = xmm3[0,2,2,3]
	pmuludq	%xmm0, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	paddd	%xmm4, %xmm2
	psrld	$16, %xmm5
	pshufd	$245, %xmm5, %xmm0      # xmm0 = xmm5[1,1,3,3]
	pmuludq	%xmm13, %xmm5
	pshufd	$232, %xmm5, %xmm3      # xmm3 = xmm5[0,2,2,3]
	pmuludq	%xmm12, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	psrld	$16, %xmm6
	pshufd	$245, %xmm6, %xmm0      # xmm0 = xmm6[1,1,3,3]
	pmuludq	%xmm14, %xmm6
	pshufd	$232, %xmm6, %xmm4      # xmm4 = xmm6[0,2,2,3]
	pmuludq	%xmm1, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	paddd	%xmm8, %xmm3
	paddd	%xmm4, %xmm3
	psrad	%xmm10, %xmm3
	paddd	%xmm11, %xmm3
	movdqa	%xmm3, %xmm0
	pcmpgtd	.LCPI16_1, %xmm0
	pand	%xmm3, %xmm0
	movdqa	%xmm15, %xmm1
	pcmpgtd	%xmm0, %xmm1
	pand	%xmm1, %xmm0
	pandn	%xmm15, %xmm1
	por	%xmm0, %xmm1
	psrld	$16, %xmm7
	psubd	%xmm1, %xmm7
	pshufd	$245, %xmm7, %xmm0      # xmm0 = xmm7[1,1,3,3]
	pmuludq	%xmm7, %xmm7
	pshufd	$232, %xmm7, %xmm4      # xmm4 = xmm7[0,2,2,3]
	pmuludq	%xmm0, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	paddd	%xmm2, %xmm4
	addq	$4, %rcx
	cmpq	%rcx, %r8
	jne	.LBB16_29
# BB#30:                                # %middle.block252
                                        #   in Loop: Header=BB16_23 Depth=2
	cmpq	%r8, 256(%rsp)          # 8-byte Folded Reload
	pshufd	$78, %xmm4, %xmm0       # xmm0 = xmm4[2,3,0,1]
	paddd	%xmm4, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %r15d
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	20(%rsp), %esi          # 4-byte Reload
	movl	16(%rsp), %r11d         # 4-byte Reload
	movl	$0, %eax
	movl	240(%rsp), %ebx         # 4-byte Reload
	movl	224(%rsp), %edi         # 4-byte Reload
	movl	208(%rsp), %edx         # 4-byte Reload
	movl	192(%rsp), %r12d        # 4-byte Reload
	jne	.LBB16_25
	jmp	.LBB16_31
	.p2align	4, 0x90
.LBB16_24:                              #   in Loop: Header=BB16_23 Depth=2
	movl	%ecx, %ebx
	movl	%edx, %edi
	movq	%r10, %rbp
	movq	%r11, %r14
	movl	%eax, %edx
	movq	%r9, %r13
	xorl	%r8d, %r8d
	movl	20(%rsp), %esi          # 4-byte Reload
	movl	16(%rsp), %r11d         # 4-byte Reload
	xorl	%eax, %eax
.LBB16_25:                              # %scalar.ph253.preheader
                                        #   in Loop: Header=BB16_23 Depth=2
	movl	12(%rsp), %r9d          # 4-byte Reload
	subl	%r8d, %r9d
	.p2align	4, 0x90
.LBB16_26:                              # %scalar.ph253
                                        #   Parent Loop BB16_17 Depth=1
                                        #     Parent Loop BB16_23 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorl	%r8d, %r8d
	movzwl	(%r13), %r10d
	imull	%ebx, %r10d
	movq	%rbp, %rax
	movzwl	(%r14), %ebp
	imull	%edi, %ebp
	addl	%esi, %r10d
	addl	%ebp, %r10d
	movq	%rax, %rbp
	movl	%r11d, %ecx
	sarl	%cl, %r10d
	addl	%edx, %r10d
	cmovsl	%r8d, %r10d
	cmpl	%r12d, %r10d
	cmovgl	%r12d, %r10d
	movq	%r14, %rax
	movl	%edx, %r14d
	movl	%edi, %edx
	movl	%ebx, %edi
	movzwl	(%rbp), %r8d
	subl	%r10d, %r8d
	xorl	%r10d, %r10d
	imull	%r8d, %r8d
	addl	%r15d, %r8d
	movzwl	2(%r13), %ebx
	imull	%edi, %ebx
	movzwl	2(%rax), %ecx
	imull	%edx, %ecx
	addl	%esi, %ebx
	addl	%ecx, %ebx
	movl	%r11d, %ecx
	sarl	%cl, %ebx
	addl	%r14d, %ebx
	cmovsl	%r10d, %ebx
	cmpl	%r12d, %ebx
	cmovgl	%r12d, %ebx
	movzwl	2(%rbp), %r15d
	subl	%ebx, %r15d
	imull	%r15d, %r15d
	addl	%r8d, %r15d
	movl	%edi, %ebx
	movl	%edx, %edi
	movl	%r14d, %edx
	movq	%rax, %r14
	addq	$4, %r13
	addq	$4, %r14
	addq	$4, %rbp
	decl	%r9d
	jne	.LBB16_26
.LBB16_31:                              # %._crit_edge.us
                                        #   in Loop: Header=BB16_23 Depth=2
	movq	128(%rsp), %r10         # 8-byte Reload
	addq	88(%rsp), %r10          # 8-byte Folded Reload
	movl	36(%rsp), %r13d         # 4-byte Reload
	cmpl	%r13d, %r15d
	jge	.LBB16_35
# BB#32:                                #   in Loop: Header=BB16_23 Depth=2
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	272(%rsp), %r11         # 8-byte Reload
	addq	%rax, %r11
	movq	288(%rsp), %r9          # 8-byte Reload
	addq	%rax, %r9
	movl	320(%rsp), %edi         # 4-byte Reload
	incl	%edi
	cmpl	24(%rsp), %edi          # 4-byte Folded Reload
	movl	240(%rsp), %ecx         # 4-byte Reload
	movl	224(%rsp), %edx         # 4-byte Reload
	movl	208(%rsp), %eax         # 4-byte Reload
	movl	192(%rsp), %r12d        # 4-byte Reload
	jl	.LBB16_23
# BB#33:                                # %._crit_edge144.loopexit
                                        #   in Loop: Header=BB16_17 Depth=1
	movq	%r9, ref1_line(%rip)
	movq	%r11, ref2_line(%rip)
	movq	%r10, src_line(%rip)
	movl	32(%rsp), %r14d         # 4-byte Reload
	movl	28(%rsp), %r12d         # 4-byte Reload
	jmp	.LBB16_34
	.p2align	4, 0x90
.LBB16_19:                              # %.preheader.preheader
                                        #   in Loop: Header=BB16_17 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%r9,%rax,2), %rax
	xorl	%ecx, %ecx
	movq	48(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB16_20:                              # %.preheader
                                        #   Parent Loop BB16_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%r13d, %r15d
	jge	.LBB16_36
# BB#21:                                #   in Loop: Header=BB16_20 Depth=2
	addq	%rsi, ref2_line(%rip)
	movq	%rax, ref1_line(%rip)
	incl	%ecx
	addq	%rsi, %rax
	cmpl	%edx, %ecx
	jl	.LBB16_20
	.p2align	4, 0x90
.LBB16_34:                              # %._crit_edge144
                                        #   in Loop: Header=BB16_17 Depth=1
	movq	40(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	cmpq	$2, %rdx
	jl	.LBB16_17
	jmp	.LBB16_36
.LBB16_35:                              # %.thread.loopexit
	movq	304(%rsp), %rcx         # 8-byte Reload
	movq	272(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,2), %rax
	movq	288(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,2), %rcx
	movq	%rcx, ref1_line(%rip)
	movq	%rax, ref2_line(%rip)
	movq	%r10, src_line(%rip)
.LBB16_36:                              # %.thread
	movl	%r15d, %eax
	addq	$360, %rsp              # imm = 0x168
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	computeBiPredSSE2, .Lfunc_end16-computeBiPredSSE2
	.cfi_endproc

	.type	d,@object               # @d
	.local	d
	.comm	d,64,16
	.type	m2,@object              # @m2
	.local	m2
	.comm	m2,256,32
	.type	m1,@object              # @m1
	.local	m1
	.comm	m1,256,16
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	src_line,@object        # @src_line
	.local	src_line
	.comm	src_line,8,8
	.type	get_line,@object        # @get_line
	.comm	get_line,16,16
	.type	ref_access_method,@object # @ref_access_method
	.comm	ref_access_method,4,4
	.type	ref_pic_sub,@object     # @ref_pic_sub
	.comm	ref_pic_sub,24,8
	.type	ref_line,@object        # @ref_line
	.local	ref_line
	.comm	ref_line,8,8
	.type	ChromaMEEnable,@object  # @ChromaMEEnable
	.comm	ChromaMEEnable,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	get_crline,@object      # @get_crline
	.comm	get_crline,16,16
	.type	weight_luma,@object     # @weight_luma
	.comm	weight_luma,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	offset_luma,@object     # @offset_luma
	.comm	offset_luma,4,4
	.type	weight_cr,@object       # @weight_cr
	.comm	weight_cr,8,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	offset_cr,@object       # @offset_cr
	.comm	offset_cr,8,4
	.type	bipred2_access_method,@object # @bipred2_access_method
	.comm	bipred2_access_method,4,4
	.type	ref_pic2_sub,@object    # @ref_pic2_sub
	.comm	ref_pic2_sub,24,8
	.type	ref2_line,@object       # @ref2_line
	.local	ref2_line
	.comm	ref2_line,8,8
	.type	bipred1_access_method,@object # @bipred1_access_method
	.comm	bipred1_access_method,4,4
	.type	ref_pic1_sub,@object    # @ref_pic1_sub
	.comm	ref_pic1_sub,24,8
	.type	ref1_line,@object       # @ref1_line
	.local	ref1_line
	.comm	ref1_line,8,8
	.type	weight1,@object         # @weight1
	.comm	weight1,2,2
	.type	weight2,@object         # @weight2
	.comm	weight2,2,2
	.type	offsetBi,@object        # @offsetBi
	.comm	offsetBi,2,2
	.type	weight1_cr,@object      # @weight1_cr
	.comm	weight1_cr,4,2
	.type	weight2_cr,@object      # @weight2_cr
	.comm	weight2_cr,4,2
	.type	offsetBi_cr,@object     # @offsetBi_cr
	.comm	offsetBi_cr,4,2
	.type	test8x8transform,@object # @test8x8transform
	.comm	test8x8transform,4,4
	.type	diff,@object            # @diff
	.local	diff
	.comm	diff,1024,16
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	height_pad,@object      # @height_pad
	.comm	height_pad,4,4
	.type	width_pad,@object       # @width_pad
	.comm	width_pad,4,4
	.type	height_pad_cr,@object   # @height_pad_cr
	.comm	height_pad_cr,4,4
	.type	width_pad_cr,@object    # @width_pad_cr
	.comm	width_pad_cr,4,4
	.type	get_line1,@object       # @get_line1
	.comm	get_line1,16,16
	.type	get_line2,@object       # @get_line2
	.comm	get_line2,16,16
	.type	get_crline1,@object     # @get_crline1
	.comm	get_crline1,16,16
	.type	get_crline2,@object     # @get_crline2
	.comm	get_crline2,16,16
	.type	img_width,@object       # @img_width
	.comm	img_width,2,2
	.type	img_height,@object      # @img_height
	.comm	img_height,2,2
	.type	computeUniPred,@object  # @computeUniPred
	.comm	computeUniPred,48,16
	.type	computeBiPred,@object   # @computeBiPred
	.comm	computeBiPred,8,8
	.type	computeBiPred1,@object  # @computeBiPred1
	.comm	computeBiPred1,24,16
	.type	computeBiPred2,@object  # @computeBiPred2
	.comm	computeBiPred2,24,16
	.type	m.0,@object             # @m.0
	.local	m.0
	.comm	m.0,4,16
	.type	m.1,@object             # @m.1
	.local	m.1
	.comm	m.1,4,16
	.type	m.2,@object             # @m.2
	.local	m.2
	.comm	m.2,4,8
	.type	m.3,@object             # @m.3
	.local	m.3
	.comm	m.3,4,16
	.type	m.4,@object             # @m.4
	.local	m.4
	.comm	m.4,4,16
	.type	m.5,@object             # @m.5
	.local	m.5
	.comm	m.5,4,16
	.type	m.6,@object             # @m.6
	.local	m.6
	.comm	m.6,4,8
	.type	m.7,@object             # @m.7
	.local	m.7
	.comm	m.7,4,16
	.type	m.8,@object             # @m.8
	.local	m.8
	.comm	m.8,4,16
	.type	m.9,@object             # @m.9
	.local	m.9
	.comm	m.9,4,16
	.type	m.10,@object            # @m.10
	.local	m.10
	.comm	m.10,4,8
	.type	m.11,@object            # @m.11
	.local	m.11
	.comm	m.11,4,16
	.type	m.12,@object            # @m.12
	.local	m.12
	.comm	m.12,4,16
	.type	m.13,@object            # @m.13
	.local	m.13
	.comm	m.13,4,16
	.type	m.14,@object            # @m.14
	.local	m.14
	.comm	m.14,4,8
	.type	m.15,@object            # @m.15
	.local	m.15
	.comm	m.15,4,16
	.type	m3.0,@object            # @m3.0
	.local	m3.0
	.comm	m3.0,32,16
	.type	m3.1,@object            # @m3.1
	.local	m3.1
	.comm	m3.1,32,16
	.type	m3.2,@object            # @m3.2
	.local	m3.2
	.comm	m3.2,32,16
	.type	m3.3,@object            # @m3.3
	.local	m3.3
	.comm	m3.3,32,16
	.type	m3.4,@object            # @m3.4
	.local	m3.4
	.comm	m3.4,32,16
	.type	m3.5,@object            # @m3.5
	.local	m3.5
	.comm	m3.5,32,16
	.type	m3.6,@object            # @m3.6
	.local	m3.6
	.comm	m3.6,32,16
	.type	m3.7,@object            # @m3.7
	.local	m3.7
	.comm	m3.7,32,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
