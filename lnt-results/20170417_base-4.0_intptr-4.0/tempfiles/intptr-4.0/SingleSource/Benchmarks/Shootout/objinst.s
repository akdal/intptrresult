	.text
	.file	"objinst.bc"
	.globl	toggle_value
	.p2align	4, 0x90
	.type	toggle_value,@function
toggle_value:                           # @toggle_value
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	retq
.Lfunc_end0:
	.size	toggle_value, .Lfunc_end0-toggle_value
	.cfi_endproc

	.globl	toggle_activate
	.p2align	4, 0x90
	.type	toggle_activate,@function
toggle_activate:                        # @toggle_activate
	.cfi_startproc
# BB#0:
	cmpb	$0, (%rdi)
	sete	(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end1:
	.size	toggle_activate, .Lfunc_end1-toggle_activate
	.cfi_endproc

	.globl	init_Toggle
	.p2align	4, 0x90
	.type	init_Toggle,@function
init_Toggle:                            # @init_Toggle
	.cfi_startproc
# BB#0:
	movb	%sil, (%rdi)
	movq	$toggle_value, 8(%rdi)
	movq	$toggle_activate, 16(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end2:
	.size	init_Toggle, .Lfunc_end2-init_Toggle
	.cfi_endproc

	.globl	new_Toggle
	.p2align	4, 0x90
	.type	new_Toggle,@function
new_Toggle:                             # @new_Toggle
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movl	$24, %edi
	callq	malloc
	movb	%bl, (%rax)
	movq	$toggle_value, 8(%rax)
	movq	$toggle_activate, 16(%rax)
	popq	%rbx
	retq
.Lfunc_end3:
	.size	new_Toggle, .Lfunc_end3-new_Toggle
	.cfi_endproc

	.globl	nth_toggle_activate
	.p2align	4, 0x90
	.type	nth_toggle_activate,@function
nth_toggle_activate:                    # @nth_toggle_activate
	.cfi_startproc
# BB#0:
	movl	28(%rdi), %eax
	incl	%eax
	movl	%eax, 28(%rdi)
	cmpl	24(%rdi), %eax
	jl	.LBB4_2
# BB#1:
	cmpb	$0, (%rdi)
	sete	(%rdi)
	movl	$0, 28(%rdi)
.LBB4_2:
	movq	%rdi, %rax
	retq
.Lfunc_end4:
	.size	nth_toggle_activate, .Lfunc_end4-nth_toggle_activate
	.cfi_endproc

	.globl	init_NthToggle
	.p2align	4, 0x90
	.type	init_NthToggle,@function
init_NthToggle:                         # @init_NthToggle
	.cfi_startproc
# BB#0:
	movl	%esi, 24(%rdi)
	movl	$0, 28(%rdi)
	movq	$nth_toggle_activate, 16(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end5:
	.size	init_NthToggle, .Lfunc_end5-init_NthToggle
	.cfi_endproc

	.globl	new_NthToggle
	.p2align	4, 0x90
	.type	new_NthToggle,@function
new_NthToggle:                          # @new_NthToggle
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movl	%edi, %ebp
	movl	$32, %edi
	callq	malloc
	movb	%bpl, (%rax)
	movq	$toggle_value, 8(%rax)
	movl	%ebx, 24(%rax)
	movl	$0, 28(%rax)
	movq	$nth_toggle_activate, 16(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end6:
	.size	new_NthToggle, .Lfunc_end6-new_NthToggle
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -32
.Lcfi11:
	.cfi_offset %r14, -24
.Lcfi12:
	.cfi_offset %r15, -16
	cmpl	$2, %edi
	jne	.LBB7_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
.LBB7_2:                                # %nth_toggle_activate.exit34
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	$toggle_value, 8(%rbx)
	movq	$toggle_activate, 16(%rbx)
	movl	$.L.str.1, %r14d
	movl	$.L.str.1, %edi
	callq	puts
	movl	$.L.str, %r15d
	movl	$.L.str, %edi
	callq	puts
	movl	$.L.str.1, %edi
	callq	puts
	movb	$1, (%rbx)
	movq	%rbx, %rdi
	callq	toggle_value
	testb	%al, %al
	movl	$.L.str.1, %edi
	cmovneq	%r15, %rdi
	callq	puts
	movq	%rbx, %rdi
	callq	toggle_activate
	movq	%rbx, %rdi
	callq	*8(%rax)
	testb	%al, %al
	movl	$.L.str.1, %edi
	cmovneq	%r15, %rdi
	callq	puts
	movq	%rbx, %rdi
	callq	free
	movl	$10, %edi
	callq	putchar
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	$toggle_value, 8(%rbx)
	movl	$3, 24(%rbx)
	movq	$nth_toggle_activate, 16(%rbx)
	movl	$.L.str, %edi
	callq	puts
	movl	$.L.str, %edi
	callq	puts
	movb	$0, (%rbx)
	movl	$.L.str.1, %edi
	callq	puts
	movl	$1, 28(%rbx)
	movq	%rbx, %rdi
	callq	toggle_value
	testb	%al, %al
	movl	$.L.str.1, %edi
	cmovneq	%r15, %rdi
	callq	puts
	movq	%rbx, %rdi
	callq	nth_toggle_activate
	movq	%rbx, %rdi
	callq	*8(%rax)
	testb	%al, %al
	movl	$.L.str.1, %edi
	cmovneq	%r15, %rdi
	callq	puts
	movq	%rbx, %rdi
	callq	*16(%rbx)
	movq	%rbx, %rdi
	callq	*8(%rax)
	testb	%al, %al
	movl	$.L.str.1, %edi
	cmovneq	%r15, %rdi
	callq	puts
	movq	%rbx, %rdi
	callq	*16(%rbx)
	movq	%rbx, %rdi
	callq	*8(%rax)
	testb	%al, %al
	movl	$.L.str.1, %edi
	cmovneq	%r15, %rdi
	callq	puts
	movq	%rbx, %rdi
	callq	*16(%rbx)
	movq	%rbx, %rdi
	callq	*8(%rax)
	testb	%al, %al
	cmovneq	%r15, %r14
	movq	%r14, %rdi
	callq	puts
	movq	%rbx, %rdi
	callq	free
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	main, .Lfunc_end7-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"true"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"false"
	.size	.L.str.1, 6


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
