	.text
	.file	"pi.bc"
	.globl	myadd
	.p2align	4, 0x90
	.type	myadd,@function
myadd:                                  # @myadd
	.cfi_startproc
# BB#0:
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rdi)
	retq
.Lfunc_end0:
	.size	myadd, .Lfunc_end0-myadd
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1169532928              # float 5813
.LCPI1_1:
	.long	1151557632              # float 1307
.LCPI1_2:
	.long	1168832512              # float 5471
.LCPI1_4:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_3:
	.long	1200703360              # float 74383
	.long	1168832512              # float 5471
	.zero	4
	.zero	4
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_5:
	.quad	4616189618054758400     # double 4
.LCPI1_6:
	.quad	4720637518976909312     # double 4.0E+7
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movl	$.Lstr, %edi
	callq	puts
	xorps	%xmm3, %xmm3
	movss	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movl	$1, %ebx
	movl	$1907, %ecx             # imm = 0x773
	movl	$40000000, %esi         # imm = 0x2625A00
	movabsq	$8126358305087380011, %rdi # imm = 0x70C69FD2BC475A2B
	movss	.LCPI1_1(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	movss	.LCPI1_2(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	.LCPI1_3(%rip), %xmm1   # xmm1 = <74383,5471,u,u>
	movss	.LCPI1_4(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	imulq	$27611, %rcx, %rcx      # imm = 0x6BDB
	movq	%rcx, %rax
	imulq	%rdi
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	imulq	$74383, %rdx, %rax      # imm = 0x1228F
	subq	%rax, %rcx
	xorps	%xmm5, %xmm5
	cvtsi2ssq	%rcx, %xmm5
	mulss	%xmm8, %xmm0
	movaps	%xmm0, %xmm6
	divss	%xmm2, %xmm6
	cvttss2si	%xmm6, %rax
	xorps	%xmm6, %xmm6
	cvtsi2ssq	%rax, %xmm6
	mulss	%xmm2, %xmm6
	subss	%xmm6, %xmm0
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	divps	%xmm1, %xmm5
	movaps	%xmm5, %xmm6
	mulps	%xmm6, %xmm6
	movaps	%xmm6, %xmm7
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	addss	%xmm6, %xmm7
	addss	%xmm7, %xmm3
	xorl	%eax, %eax
	ucomiss	%xmm7, %xmm4
	setae	%al
	addq	%rax, %rbx
	decq	%rsi
	jne	.LBB1_1
# BB#2:
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm5, %xmm0
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm5, %xmm1
	movl	$.L.str.1, %edi
	movl	$40000001, %edx         # imm = 0x2625A01
	movb	$2, %al
	movl	%ebx, %esi
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	callq	printf
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rbx, %xmm0
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI1_5(%rip), %xmm0
	divsd	.LCPI1_6(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	cvtss2sd	%xmm1, %xmm2
	xorpd	%xmm1, %xmm1
	mulsd	%xmm2, %xmm1
	movl	$.L.str.2, %edi
	movl	$40000000, %esi         # imm = 0x2625A00
	movb	$2, %al
	callq	printf
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	" x = %9.6f    y = %12.2f  low = %8d j = %7d\n"
	.size	.L.str.1, 45

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Pi = %9.6f ztot = %12.2f itot = %8d\n"
	.size	.L.str.2, 37

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"Starting PI..."
	.size	.Lstr, 15


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
