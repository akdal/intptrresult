	.text
	.file	"closure.bc"
	.globl	initialize_closure
	.p2align	4, 0x90
	.type	initialize_closure,@function
initialize_closure:                     # @initialize_closure
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	addl	%edi, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, itemset(%rip)
	movl	nrules(%rip), %eax
	leal	32(%rax), %ecx
	sarl	$31, %ecx
	shrl	$27, %ecx
	leal	32(%rax,%rcx), %edi
	sarl	$5, %edi
	movl	%edi, rulesetsize(%rip)
	shll	$2, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, ruleset(%rip)
	popq	%rax
	jmp	set_fderives            # TAILCALL
.Lfunc_end0:
	.size	initialize_closure, .Lfunc_end0-initialize_closure
	.cfi_endproc

	.globl	set_fderives
	.p2align	4, 0x90
	.type	set_fderives,@function
set_fderives:                           # @set_fderives
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 48
.Lcfi6:
	.cfi_offset %rbx, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	nvars(%rip), %edi
	imull	rulesetsize(%rip), %edi
	shll	$2, %edi
	xorl	%eax, %eax
	callq	mallocate
	movslq	rulesetsize(%rip), %rcx
	movslq	ntokens(%rip), %rdx
	imulq	%rcx, %rdx
	shlq	$2, %rdx
	subq	%rdx, %rax
	movq	%rax, fderives(%rip)
	callq	set_firsts
	movl	ntokens(%rip), %ebx
	movl	nsyms(%rip), %ecx
	movq	firsts(%rip), %rdi
	cmpl	%ecx, %ebx
	jge	.LBB1_15
# BB#1:                                 # %.lr.ph50
	movslq	%ebx, %rax
	movslq	rulesetsize(%rip), %r9
	imulq	%r9, %rax
	shlq	$2, %rax
	addq	fderives(%rip), %rax
	movq	derives(%rip), %r11
	movslq	varsetsize(%rip), %r8
	movl	%ebx, %r10d
	cmpl	%ecx, %ebx
	jl	.LBB1_3
	.p2align	4, 0x90
.LBB1_13:                               # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	incl	%r10d
	cmpl	%ecx, %r10d
	jge	.LBB1_15
# BB#14:                                # %._crit_edge._crit_edge
                                        #   in Loop: Header=BB1_13 Depth=1
	leaq	(%rax,%r9,4), %rax
	movl	ntokens(%rip), %ebx
	cmpl	%ecx, %ebx
	jge	.LBB1_13
.LBB1_3:                                # %.lr.ph46.preheader
	movl	%r10d, %ecx
	subl	%ebx, %ecx
	movslq	%ecx, %rcx
	imulq	%r8, %rcx
	leaq	(%rdi,%rcx,4), %r14
	movl	(%r14), %r15d
	addq	$4, %r14
	movslq	%ebx, %rbx
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph46
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_7 Depth 2
	testl	%ebp, %r15d
	je	.LBB1_8
# BB#5:                                 #   in Loop: Header=BB1_4 Depth=1
	movq	(%r11,%rbx,8), %rdx
	movzwl	(%rdx), %ecx
	testw	%cx, %cx
	jle	.LBB1_8
# BB#6:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_4 Depth=1
	addq	$2, %rdx
	.p2align	4, 0x90
.LBB1_7:                                # %.lr.ph
                                        #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	%cx, %ecx
	movl	$1, %esi
	shll	%cl, %esi
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%esi, (%rax,%rcx,4)
	movzwl	(%rdx), %ecx
	addq	$2, %rdx
	testw	%cx, %cx
	jg	.LBB1_7
.LBB1_8:                                # %.loopexit
                                        #   in Loop: Header=BB1_4 Depth=1
	addl	%ebp, %ebp
	je	.LBB1_10
# BB#9:                                 # %.loopexit._crit_edge
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	nsyms(%rip), %ecx
	incq	%rbx
	jmp	.LBB1_12
	.p2align	4, 0x90
.LBB1_10:                               #   in Loop: Header=BB1_4 Depth=1
	incq	%rbx
	movslq	nsyms(%rip), %rcx
	xorl	%ebp, %ebp
	cmpq	%rcx, %rbx
	jge	.LBB1_12
# BB#11:                                #   in Loop: Header=BB1_4 Depth=1
	movl	(%r14), %r15d
	addq	$4, %r14
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB1_12:                               #   in Loop: Header=BB1_4 Depth=1
	movslq	%ecx, %rdx
	cmpq	%rdx, %rbx
	jl	.LBB1_4
	jmp	.LBB1_13
.LBB1_15:                               # %._crit_edge51
	addq	$8, %rsp
	testq	%rdi, %rdi
	je	.LBB1_16
# BB#17:
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB1_16:
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	set_fderives, .Lfunc_end1-set_fderives
	.cfi_endproc

	.globl	set_firsts
	.p2align	4, 0x90
	.type	set_firsts,@function
set_firsts:                             # @set_firsts
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	movl	nvars(%rip), %edi
	leal	31(%rdi), %eax
	sarl	$31, %eax
	shrl	$27, %eax
	leal	31(%rdi,%rax), %ebx
	sarl	$5, %ebx
	movl	%ebx, varsetsize(%rip)
	imull	%ebx, %edi
	shll	$2, %edi
	xorl	%eax, %eax
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	mallocate
	movq	%rax, firsts(%rip)
	movslq	ntokens(%rip), %r10
	movl	nsyms(%rip), %ecx
	cmpl	%ecx, %r10d
	jge	.LBB2_9
# BB#1:                                 # %.lr.ph27
	movq	derives(%rip), %r8
	movslq	%ebx, %r9
	movq	ritem(%rip), %rsi
	movq	rrhs(%rip), %rdi
	movq	%rax, %r11
	.p2align	4, 0x90
.LBB2_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_4 Depth 2
	movq	(%r8,%r10,8), %rdx
	movzwl	(%rdx), %ebx
	testw	%bx, %bx
	js	.LBB2_8
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	addq	$2, %rdx
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	%bx, %rcx
	movswq	(%rdi,%rcx,2), %rcx
	movswl	(%rsi,%rcx,2), %ecx
	subl	ntokens(%rip), %ecx
	jl	.LBB2_6
# BB#5:                                 #   in Loop: Header=BB2_4 Depth=2
	movl	$1, %ebx
	shll	%cl, %ebx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%ebx, (%r11,%rcx,4)
.LBB2_6:                                # %.backedge
                                        #   in Loop: Header=BB2_4 Depth=2
	movzwl	(%rdx), %ebx
	addq	$2, %rdx
	testw	%bx, %bx
	jns	.LBB2_4
# BB#7:                                 # %._crit_edge.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	nsyms(%rip), %ecx
.LBB2_8:                                # %._crit_edge
                                        #   in Loop: Header=BB2_2 Depth=1
	leaq	(%r11,%r9,4), %r11
	incq	%r10
	movslq	%ecx, %rdx
	cmpq	%rdx, %r10
	jl	.LBB2_2
.LBB2_9:                                # %._crit_edge28
	movl	nvars(%rip), %esi
	movq	%rax, %rdi
	popq	%rbx
	jmp	RTC                     # TAILCALL
.Lfunc_end2:
	.size	set_firsts, .Lfunc_end2-set_firsts
	.cfi_endproc

	.globl	closure
	.p2align	4, 0x90
	.type	closure,@function
closure:                                # @closure
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi18:
	.cfi_def_cfa_offset 128
.Lcfi19:
	.cfi_offset %rbx, -56
.Lcfi20:
	.cfi_offset %r12, -48
.Lcfi21:
	.cfi_offset %r13, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movabsq	$9223372036854775800, %r14 # imm = 0x7FFFFFFFFFFFFFF8
	movq	ruleset(%rip), %r13
	movslq	rulesetsize(%rip), %rsi
	leaq	(%r13,%rsi,4), %r15
	movslq	%ebp, %rax
	leaq	(%rbx,%rax,2), %r12
	testl	%eax, %eax
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	je	.LBB3_27
# BB#1:                                 # %.preheader73
	testl	%esi, %esi
	jle	.LBB3_3
# BB#2:                                 # %.lr.ph106.preheader
	leaq	4(%r13), %rax
	cmpq	%rax, %r15
	cmovaq	%r15, %rax
	movq	%r13, %rcx
	notq	%rcx
	leaq	4(%rax,%rcx), %rdx
	andq	$-4, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	memset
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB3_3:                                # %.preheader71
	testl	%ebp, %ebp
	jle	.LBB3_50
# BB#4:                                 # %.lr.ph104
	testl	%esi, %esi
	jle	.LBB3_30
# BB#5:                                 # %.lr.ph104.split.us.preheader
	movq	ritem(%rip), %rax
	movq	fderives(%rip), %r8
	leaq	4(%r13), %rcx
	cmpq	%rcx, %r15
	cmovaq	%r15, %rcx
	movq	%r13, %rdx
	notq	%rdx
	addq	%rcx, %rdx
	shrq	$2, %rdx
	leaq	4(%r13,%rdx,4), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	leaq	4(%r8,%rdx,4), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	leaq	1(%rdx), %rdi
	andq	%rdi, %r14
	leaq	-8(%r14), %rcx
	shrq	$3, %rcx
	movq	%r14, %r10
	leaq	(%r13,%r14,4), %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	48(%r13), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	leaq	48(%r8), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rbx, %r9
	jmp	.LBB3_6
.LBB3_14:                               # %vector.body.preheader
                                        #   in Loop: Header=BB3_6 Depth=1
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	jne	.LBB3_16
# BB#15:                                # %vector.body.prol
                                        #   in Loop: Header=BB3_6 Depth=1
	movups	(%r11), %xmm0
	movups	16(%r11), %xmm1
	movups	(%r13), %xmm2
	movups	16(%r13), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%r13)
	movups	%xmm3, 16(%r13)
	movl	$8, %ecx
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	jne	.LBB3_17
	jmp	.LBB3_19
.LBB3_16:                               #   in Loop: Header=BB3_6 Depth=1
	xorl	%ecx, %ecx
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB3_19
.LBB3_17:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB3_6 Depth=1
	movq	%r10, %rsi
	subq	%rcx, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,4), %rdx
	addq	%rcx, %rbp
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB3_18:                               # %vector.body
                                        #   Parent Loop BB3_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	-48(%rdx), %xmm2
	movups	-32(%rdx), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -48(%rdx)
	movups	%xmm3, -32(%rdx)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	-16(%rdx), %xmm2
	movups	(%rdx), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -16(%rdx)
	movups	%xmm3, (%rdx)
	addq	$64, %rdx
	addq	$64, %rbp
	addq	$-16, %rsi
	jne	.LBB3_18
.LBB3_19:                               # %middle.block
                                        #   in Loop: Header=BB3_6 Depth=1
	cmpq	%r10, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	je	.LBB3_26
# BB#20:                                #   in Loop: Header=BB3_6 Depth=1
	leaq	(%r11,%r10,4), %r11
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB3_21
	.p2align	4, 0x90
.LBB3_6:                                # %.lr.ph104.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_18 Depth 2
                                        #     Child Loop BB3_23 Depth 2
                                        #     Child Loop BB3_25 Depth 2
	movswq	(%r9), %rcx
	addq	$2, %r9
	movswl	(%rax,%rcx,2), %ecx
	cmpl	ntokens(%rip), %ecx
	jl	.LBB3_26
# BB#7:                                 # %.lr.ph102.us
                                        #   in Loop: Header=BB3_6 Depth=1
	imull	%esi, %ecx
	cmpq	$8, %rdi
	movslq	%ecx, %rbp
	leaq	(%r8,%rbp,4), %r11
	jae	.LBB3_9
# BB#8:                                 #   in Loop: Header=BB3_6 Depth=1
	movq	%r13, %r14
	jmp	.LBB3_21
	.p2align	4, 0x90
.LBB3_9:                                # %min.iters.checked
                                        #   in Loop: Header=BB3_6 Depth=1
	testq	%r10, %r10
	je	.LBB3_13
# BB#10:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_6 Depth=1
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rbp,4), %rcx
	cmpq	%rcx, %r13
	jae	.LBB3_14
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_6 Depth=1
	cmpq	56(%rsp), %r11          # 8-byte Folded Reload
	jae	.LBB3_14
.LBB3_13:                               #   in Loop: Header=BB3_6 Depth=1
	movq	%r13, %r14
.LBB3_21:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB3_6 Depth=1
	leaq	4(%r14), %rbp
	cmpq	%rbp, %r15
	cmovaq	%r15, %rbp
	subq	%r14, %rbp
	decq	%rbp
	movl	%ebp, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB3_24
# BB#22:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB3_6 Depth=1
	negq	%rdx
	.p2align	4, 0x90
.LBB3_23:                               # %scalar.ph.prol
                                        #   Parent Loop BB3_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r11), %ecx
	addq	$4, %r11
	orl	%ecx, (%r14)
	addq	$4, %r14
	incq	%rdx
	jne	.LBB3_23
.LBB3_24:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB3_6 Depth=1
	cmpq	$12, %rbp
	jb	.LBB3_26
	.p2align	4, 0x90
.LBB3_25:                               # %scalar.ph
                                        #   Parent Loop BB3_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r11), %ecx
	orl	%ecx, (%r14)
	movl	4(%r11), %ecx
	orl	%ecx, 4(%r14)
	movl	8(%r11), %ecx
	orl	%ecx, 8(%r14)
	movl	12(%r11), %ecx
	orl	%ecx, 12(%r14)
	addq	$16, %r14
	addq	$16, %r11
	cmpq	%r15, %r14
	jb	.LBB3_25
.LBB3_26:                               # %.backedge.us
                                        #   in Loop: Header=BB3_6 Depth=1
	cmpq	%r12, %r9
	jb	.LBB3_6
	jmp	.LBB3_50
.LBB3_27:
	testl	%esi, %esi
	jle	.LBB3_30
# BB#28:                                # %.lr.ph98.preheader
	movq	fderives(%rip), %r8
	movq	%rsi, %rax
	movslq	start_symbol(%rip), %rsi
	imulq	%rax, %rsi
	leaq	(%r8,%rsi,4), %rax
	leaq	4(%r13), %rdx
	cmpq	%rdx, %r15
	movq	%rdx, %rcx
	cmovaq	%r15, %rcx
	movq	%r13, %rdi
	notq	%rdi
	addq	%rdi, %rcx
	shrq	$2, %rcx
	incq	%rcx
	cmpq	$8, %rcx
	jae	.LBB3_31
# BB#29:
	movq	%r13, %rcx
	jmp	.LBB3_45
.LBB3_30:                               # %.loopexit69.thread
	movq	itemset(%rip), %rax
	movq	%rax, itemsetend(%rip)
	cmpq	%r12, %rbx
	jb	.LBB3_64
	jmp	.LBB3_75
.LBB3_31:                               # %min.iters.checked181
	andq	%rcx, %r14
	je	.LBB3_35
# BB#32:                                # %vector.memcheck199
	cmpq	%rdx, %r15
	cmovaq	%r15, %rdx
	addq	%rdi, %rdx
	shrq	$2, %rdx
	leaq	(%rdx,%rsi), %rdi
	leaq	4(%r8,%rdi,4), %rdi
	cmpq	%rdi, %r13
	jae	.LBB3_36
# BB#33:                                # %vector.memcheck199
	leaq	4(%r13,%rdx,4), %rdx
	cmpq	%rdx, %rax
	jae	.LBB3_36
.LBB3_35:
	movq	%r13, %rcx
.LBB3_45:                               # %.lr.ph98.preheader258
	leaq	4(%rcx), %rsi
	cmpq	%rsi, %r15
	cmovaq	%r15, %rsi
	movq	%rcx, %rdx
	notq	%rdx
	addq	%rsi, %rdx
	movl	%edx, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB3_48
# BB#46:                                # %.lr.ph98.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB3_47:                               # %.lr.ph98.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edi
	addq	$4, %rax
	movl	%edi, (%rcx)
	addq	$4, %rcx
	incq	%rsi
	jne	.LBB3_47
.LBB3_48:                               # %.lr.ph98.prol.loopexit
	cmpq	$28, %rdx
	movq	8(%rsp), %rsi           # 8-byte Reload
	jb	.LBB3_50
	.p2align	4, 0x90
.LBB3_49:                               # %.lr.ph98
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	movl	%edx, (%rcx)
	movl	4(%rax), %edx
	movl	%edx, 4(%rcx)
	movl	8(%rax), %edx
	movl	%edx, 8(%rcx)
	movl	12(%rax), %edx
	movl	%edx, 12(%rcx)
	movl	16(%rax), %edx
	movl	%edx, 16(%rcx)
	movl	20(%rax), %edx
	movl	%edx, 20(%rcx)
	movl	24(%rax), %edx
	movl	%edx, 24(%rcx)
	movl	28(%rax), %edx
	movl	%edx, 28(%rcx)
	addq	$32, %rcx
	addq	$32, %rax
	cmpq	%r15, %rcx
	jb	.LBB3_49
.LBB3_50:                               # %.loopexit69
	movq	itemset(%rip), %rax
	movq	%rax, itemsetend(%rip)
	testl	%esi, %esi
	jle	.LBB3_63
# BB#51:                                # %.lr.ph80.lr.ph
	xorl	%r9d, %r9d
	movq	rrhs(%rip), %r8
	movq	%rax, %rsi
.LBB3_52:                               # %.lr.ph80
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_53 Depth 2
                                        #     Child Loop BB3_56 Depth 2
                                        #       Child Loop BB3_58 Depth 3
	movslq	%r9d, %r9
	.p2align	4, 0x90
.LBB3_53:                               #   Parent Loop BB3_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r13), %r10d
	addq	$4, %r13
	testl	%r10d, %r10d
	jne	.LBB3_55
# BB#54:                                #   in Loop: Header=BB3_53 Depth=2
	addq	$32, %r9
	cmpq	%r15, %r13
	jb	.LBB3_53
	jmp	.LBB3_63
	.p2align	4, 0x90
.LBB3_55:                               # %.preheader68
                                        #   in Loop: Header=BB3_52 Depth=1
	movl	$1, %edx
	movq	%r9, %rcx
	.p2align	4, 0x90
.LBB3_56:                               #   Parent Loop BB3_52 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_58 Depth 3
	testl	%r10d, %edx
	je	.LBB3_61
# BB#57:                                #   in Loop: Header=BB3_56 Depth=2
	movzwl	(%r8,%rcx,2), %ebp
	cmpq	%r12, %rbx
	jae	.LBB3_60
	.p2align	4, 0x90
.LBB3_58:                               # %.lr.ph84
                                        #   Parent Loop BB3_52 Depth=1
                                        #     Parent Loop BB3_56 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	(%rbx), %edi
	cmpw	%bp, %di
	jge	.LBB3_60
# BB#59:                                #   in Loop: Header=BB3_58 Depth=3
	addq	$2, %rbx
	leaq	2(%rax), %rsi
	movq	%rsi, itemsetend(%rip)
	movw	%di, (%rax)
	cmpq	%r12, %rbx
	movq	%rsi, %rax
	jb	.LBB3_58
.LBB3_60:                               # %.critedge
                                        #   in Loop: Header=BB3_56 Depth=2
	leaq	2(%rsi), %rax
	movq	%rax, itemsetend(%rip)
	movw	%bp, (%rsi)
	movq	%rax, %rsi
.LBB3_61:                               #   in Loop: Header=BB3_56 Depth=2
	addl	%edx, %edx
	incq	%rcx
	testl	%edx, %edx
	jne	.LBB3_56
# BB#62:                                # %.loopexit
                                        #   in Loop: Header=BB3_52 Depth=1
	addl	$32, %r9d
	cmpq	%r15, %r13
	jb	.LBB3_52
.LBB3_63:                               # %.preheader
	cmpq	%r12, %rbx
	jae	.LBB3_75
.LBB3_64:                               # %.lr.ph
	movq	%rbx, %rsi
	notq	%rsi
	addq	%r12, %rsi
	movq	%rsi, %rcx
	shrq	%rcx
	incq	%rcx
	cmpq	$15, %rcx
	jbe	.LBB3_68
# BB#65:                                # %min.iters.checked222
	movq	%rcx, %rdx
	andq	$-16, %rdx
	je	.LBB3_68
# BB#66:                                # %vector.memcheck241
	andq	$-2, %rsi
	leaq	2(%rbx,%rsi), %rdi
	cmpq	%rdi, %rax
	jae	.LBB3_76
# BB#67:                                # %vector.memcheck241
	leaq	2(%rax,%rsi), %rsi
	cmpq	%rsi, %rbx
	jae	.LBB3_76
.LBB3_68:
	movq	%rax, %rsi
.LBB3_69:                               # %scalar.ph216.preheader
	leaq	2(%rbx), %rdi
	cmpq	%rdi, %r12
	cmovaq	%r12, %rdi
	movq	%rbx, %rdx
	notq	%rdx
	addq	%rdi, %rdx
	movl	%edx, %edi
	shrl	%edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB3_72
# BB#70:                                # %scalar.ph216.prol.preheader
	negq	%rdi
	.p2align	4, 0x90
.LBB3_71:                               # %scalar.ph216.prol
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rbx), %ebp
	addq	$2, %rbx
	movw	%bp, (%rsi)
	addq	$2, %rsi
	incq	%rdi
	jne	.LBB3_71
.LBB3_72:                               # %scalar.ph216.prol.loopexit
	cmpq	$14, %rdx
	jb	.LBB3_74
	.p2align	4, 0x90
.LBB3_73:                               # %scalar.ph216
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rbx), %edx
	movw	%dx, (%rsi)
	movzwl	2(%rbx), %edx
	movw	%dx, 2(%rsi)
	movzwl	4(%rbx), %edx
	movw	%dx, 4(%rsi)
	movzwl	6(%rbx), %edx
	movw	%dx, 6(%rsi)
	movzwl	8(%rbx), %edx
	movw	%dx, 8(%rsi)
	movzwl	10(%rbx), %edx
	movw	%dx, 10(%rsi)
	movzwl	12(%rbx), %edx
	movw	%dx, 12(%rsi)
	movzwl	14(%rbx), %edx
	movw	%dx, 14(%rsi)
	addq	$16, %rbx
	addq	$16, %rsi
	cmpq	%r12, %rbx
	jb	.LBB3_73
.LBB3_74:                               # %._crit_edge
	leaq	(%rax,%rcx,2), %rax
	movq	%rax, itemsetend(%rip)
.LBB3_75:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_76:                               # %vector.body214.preheader
	leaq	-16(%rdx), %rsi
	movl	%esi, %edi
	shrl	$4, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB3_79
# BB#77:                                # %vector.body214.prol.preheader
	negq	%rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_78:                               # %vector.body214.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbx,%rbp,2), %xmm0
	movups	16(%rbx,%rbp,2), %xmm1
	movups	%xmm0, (%rax,%rbp,2)
	movups	%xmm1, 16(%rax,%rbp,2)
	addq	$16, %rbp
	incq	%rdi
	jne	.LBB3_78
	jmp	.LBB3_80
.LBB3_36:                               # %vector.body172.preheader
	leaq	-8(%r14), %r9
	movl	%r9d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB3_39
# BB#37:                                # %vector.body172.prol.preheader
	leaq	16(%r8,%rsi,4), %rbp
	negq	%rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_38:                               # %vector.body172.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbp,%rdx,4), %xmm0
	movups	(%rbp,%rdx,4), %xmm1
	movups	%xmm0, (%r13,%rdx,4)
	movups	%xmm1, 16(%r13,%rdx,4)
	addq	$8, %rdx
	incq	%rdi
	jne	.LBB3_38
	jmp	.LBB3_40
.LBB3_79:
	xorl	%ebp, %ebp
.LBB3_80:                               # %vector.body214.prol.loopexit
	cmpq	$48, %rsi
	jb	.LBB3_83
# BB#81:                                # %vector.body214.preheader.new
	movq	%rdx, %rsi
	subq	%rbp, %rsi
	leaq	112(%rbx,%rbp,2), %rdi
	leaq	112(%rax,%rbp,2), %rbp
	.p2align	4, 0x90
.LBB3_82:                               # %vector.body214
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rdi
	subq	$-128, %rbp
	addq	$-64, %rsi
	jne	.LBB3_82
.LBB3_83:                               # %middle.block215
	cmpq	%rdx, %rcx
	je	.LBB3_74
# BB#84:
	leaq	(%rax,%rdx,2), %rsi
	leaq	(%rbx,%rdx,2), %rbx
	jmp	.LBB3_69
.LBB3_39:
	xorl	%edx, %edx
.LBB3_40:                               # %vector.body172.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB3_43
# BB#41:                                # %vector.body172.preheader.new
	movq	%r14, %rdi
	subq	%rdx, %rdi
	leaq	112(%r13,%rdx,4), %rbp
	addq	%rdx, %rsi
	leaq	112(%r8,%rsi,4), %rdx
	.p2align	4, 0x90
.LBB3_42:                               # %vector.body172
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rbp
	subq	$-128, %rdx
	addq	$-32, %rdi
	jne	.LBB3_42
.LBB3_43:                               # %middle.block173
	cmpq	%r14, %rcx
	movq	8(%rsp), %rsi           # 8-byte Reload
	je	.LBB3_50
# BB#44:
	leaq	(%rax,%r14,4), %rax
	leaq	(%r13,%r14,4), %rcx
	jmp	.LBB3_45
.Lfunc_end3:
	.size	closure, .Lfunc_end3-closure
	.cfi_endproc

	.globl	finalize_closure
	.p2align	4, 0x90
	.type	finalize_closure,@function
finalize_closure:                       # @finalize_closure
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 16
	movq	itemset(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_2
# BB#1:
	callq	free
.LBB4_2:
	movq	ruleset(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_4
# BB#3:
	callq	free
.LBB4_4:
	movslq	rulesetsize(%rip), %rax
	movslq	ntokens(%rip), %rdi
	imulq	%rax, %rdi
	shlq	$2, %rdi
	addq	fderives(%rip), %rdi
	je	.LBB4_5
# BB#6:
	popq	%rax
	jmp	free                    # TAILCALL
.LBB4_5:
	popq	%rax
	retq
.Lfunc_end4:
	.size	finalize_closure, .Lfunc_end4-finalize_closure
	.cfi_endproc

	.type	itemset,@object         # @itemset
	.comm	itemset,8,8
	.type	rulesetsize,@object     # @rulesetsize
	.local	rulesetsize
	.comm	rulesetsize,4,4
	.type	ruleset,@object         # @ruleset
	.local	ruleset
	.comm	ruleset,8,8
	.type	fderives,@object        # @fderives
	.local	fderives
	.comm	fderives,8,8
	.type	firsts,@object          # @firsts
	.local	firsts
	.comm	firsts,8,8
	.type	varsetsize,@object      # @varsetsize
	.local	varsetsize
	.comm	varsetsize,4,4
	.type	itemsetend,@object      # @itemsetend
	.comm	itemsetend,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
