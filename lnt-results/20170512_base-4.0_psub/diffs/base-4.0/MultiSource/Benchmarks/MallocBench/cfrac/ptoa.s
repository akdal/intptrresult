	.text
	.file	"ptoa.bc"
	.globl	ptoa
	.p2align	4, 0x90
	.type	ptoa,@function
ptoa:                                   # @ptoa
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB0_2
# BB#1:
	incw	(%rbx)
.LBB0_2:
	movq	$0, 32(%rsp)
	movq	$0, 16(%rsp)
	movq	$0, 24(%rsp)
	movzwl	4(%rbx), %eax
	imull	$525, %eax, %r12d       # imm = 0x20D
	imulq	$748664025, %r12, %rax  # imm = 0x2C9FB4D9
	shrq	$32, %rax
	subl	%eax, %r12d
	shrl	%r12d
	addl	%eax, %r12d
	shrl	$6, %r12d
	addq	$11, %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_25
# BB#3:
	movq	%rbx, %rdi
	callq	pabs
	leaq	16(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
	movl	$1000000000, %edi       # imm = 0x3B9ACA00
	callq	utop
	leaq	24(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
	leaq	-1(%rbp,%r12), %r13
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movb	$0, -1(%rbp,%r12)
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movb	6(%rbx), %al
	movb	%al, 15(%rsp)           # 1-byte Spill
	movl	$3435973837, %r14d      # imm = 0xCCCCCCCD
	movl	$3518437209, %r15d      # imm = 0xD1B71759
	.p2align	4, 0x90
.LBB0_4:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rdi
	movq	24(%rsp), %rsi
	leaq	16(%rsp), %rdx
	leaq	32(%rsp), %rcx
	callq	pdivmod
	movq	32(%rsp), %rdi
	callq	ptou
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movl	%eax, %r8d
	imulq	$1374389535, %r8, %r9   # imm = 0x51EB851F
	imulq	$274877907, %r8, %r10   # imm = 0x10624DD3
	movq	%r8, %rdi
	imulq	$1125899907, %r8, %rbp  # imm = 0x431BDE83
	imulq	$1801439851, %r8, %rcx  # imm = 0x6B5FCA6B
	imulq	$1441151881, %r8, %rdx  # imm = 0x55E63B89
	imulq	%r14, %r8
	shrq	$35, %r8
	leal	(%r8,%r8), %esi
	leal	(%rsi,%rsi,4), %esi
	movl	%eax, %ebx
	subl	%esi, %ebx
	orb	$48, %bl
	movb	%bl, -1(%r13)
	movq	%r8, %rsi
	imulq	%r14, %rsi
	shrq	$35, %rsi
	addl	%esi, %esi
	leal	(%rsi,%rsi,4), %esi
	subl	%esi, %r8d
	orb	$48, %r8b
	movb	%r8b, -2(%r13)
	shrq	$37, %r9
	movq	%r9, %rsi
	imulq	%r14, %rsi
	shrq	$35, %rsi
	addl	%esi, %esi
	leal	(%rsi,%rsi,4), %esi
	subl	%esi, %r9d
	orb	$48, %r9b
	movb	%r9b, -3(%r13)
	shrq	$38, %r10
	movq	%r10, %rsi
	imulq	%r14, %rsi
	shrq	$35, %rsi
	addl	%esi, %esi
	leal	(%rsi,%rsi,4), %esi
	subl	%esi, %r10d
	orb	$48, %r10b
	movb	%r10b, -4(%r13)
	imulq	%r15, %rdi
	shrq	$45, %rdi
	movq	%rdi, %rsi
	imulq	%r14, %rsi
	shrq	$35, %rsi
	addl	%esi, %esi
	leal	(%rsi,%rsi,4), %esi
	subl	%esi, %edi
	orb	$48, %dil
	movb	%dil, -5(%r13)
	shrl	$5, %eax
	imulq	$175921861, %rax, %rax  # imm = 0xA7C5AC5
	shrq	$39, %rax
	movq	%rax, %rsi
	imulq	%r14, %rsi
	shrq	$35, %rsi
	addl	%esi, %esi
	leal	(%rsi,%rsi,4), %esi
	subl	%esi, %eax
	orb	$48, %al
	movb	%al, -6(%r13)
	shrq	$50, %rbp
	movq	%rbp, %rax
	imulq	%r14, %rax
	shrq	$35, %rax
	addl	%eax, %eax
	leal	(%rax,%rax,4), %eax
	subl	%eax, %ebp
	orb	$48, %bpl
	movb	%bpl, -7(%r13)
	shrq	$54, %rcx
	movq	%rcx, %rax
	imulq	%r14, %rax
	shrq	$35, %rax
	addl	%eax, %eax
	leal	(%rax,%rax,4), %eax
	subl	%eax, %ecx
	orb	$48, %cl
	movb	%cl, -8(%r13)
	shrq	$57, %rdx
	movq	%rdx, %rax
	imulq	%r14, %rax
	shrq	$35, %rax
	addl	%eax, %eax
	leal	(%rax,%rax,4), %eax
	subl	%eax, %edx
	orb	$48, %dl
	movb	%dl, -9(%r13)
	addq	$-9, %r13
	movq	16(%rsp), %rdi
	callq	pcmpz
	testl	%eax, %eax
	jne	.LBB0_4
# BB#5:                                 # %.preheader.preheader
	movq	48(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_6
	.p2align	4, 0x90
.LBB0_26:                               #   in Loop: Header=BB0_6 Depth=1
	incq	%r13
.LBB0_6:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r13), %eax
	cmpb	$48, %al
	je	.LBB0_26
# BB#7:                                 # %.preheader
	testb	%al, %al
	jne	.LBB0_9
# BB#8:
	decq	%r13
.LBB0_9:                                # %.loopexit.loopexit
	movq	40(%rsp), %rbx          # 8-byte Reload
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	je	.LBB0_11
# BB#10:
	movb	$45, -1(%r13)
	decq	%r13
.LBB0_11:
	cmpq	%rbp, %r13
	jbe	.LBB0_13
# BB#12:
	addq	%rbp, %r12
	subq	%r13, %r12
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	callq	memmove
.LBB0_13:
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_16
# BB#14:
	decw	(%rdi)
	jne	.LBB0_16
# BB#15:
	xorl	%eax, %eax
	callq	pfree
.LBB0_16:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_19
# BB#17:
	decw	(%rdi)
	jne	.LBB0_19
# BB#18:
	xorl	%eax, %eax
	callq	pfree
.LBB0_19:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_22
# BB#20:
	decw	(%rdi)
	jne	.LBB0_22
# BB#21:
	xorl	%eax, %eax
	callq	pfree
.LBB0_22:
	testq	%rbx, %rbx
	je	.LBB0_25
# BB#23:
	decw	(%rbx)
	jne	.LBB0_25
# BB#24:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pfree
.LBB0_25:
	movq	%rbp, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	ptoa, .Lfunc_end0-ptoa
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
