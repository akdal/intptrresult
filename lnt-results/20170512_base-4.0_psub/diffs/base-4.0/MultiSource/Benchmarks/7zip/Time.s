	.text
	.file	"Time.bc"
	.globl	_ZN8NWindows5NTime17DosTimeToFileTimeEjR9_FILETIME
	.p2align	4, 0x90
	.type	_ZN8NWindows5NTime17DosTimeToFileTimeEjR9_FILETIME,@function
_ZN8NWindows5NTime17DosTimeToFileTimeEjR9_FILETIME: # @_ZN8NWindows5NTime17DosTimeToFileTimeEjR9_FILETIME
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movq	%rsi, %rax
	movl	%edi, %ecx
	shrl	$16, %ecx
	movzwl	%di, %esi
	movl	%ecx, %edi
	movq	%rax, %rdx
	callq	DosDateTimeToFileTime
	testl	%eax, %eax
	setne	%al
	popq	%rcx
	retq
.Lfunc_end0:
	.size	_ZN8NWindows5NTime17DosTimeToFileTimeEjR9_FILETIME, .Lfunc_end0-_ZN8NWindows5NTime17DosTimeToFileTimeEjR9_FILETIME
	.cfi_endproc

	.globl	_ZN8NWindows5NTime17FileTimeToDosTimeERK9_FILETIMERj
	.p2align	4, 0x90
	.type	_ZN8NWindows5NTime17FileTimeToDosTimeERK9_FILETIMERj,@function
_ZN8NWindows5NTime17FileTimeToDosTimeERK9_FILETIMERj: # @_ZN8NWindows5NTime17FileTimeToDosTimeERK9_FILETIMERj
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 32
.Lcfi4:
	.cfi_offset %rbx, -24
.Lcfi5:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	6(%rsp), %rsi
	leaq	4(%rsp), %rdx
	callq	FileTimeToDosDateTime
	testl	%eax, %eax
	je	.LBB1_1
# BB#2:
	movzwl	6(%rsp), %eax
	shll	$16, %eax
	movzwl	4(%rsp), %ecx
	orl	%eax, %ecx
	movb	$1, %al
	jmp	.LBB1_3
.LBB1_1:
	cmpl	$29360127, 4(%rbx)      # imm = 0x1BFFFFF
	movl	$-6307971, %eax         # imm = 0xFF9FBF7D
	movl	$2162688, %ecx          # imm = 0x210000
	cmoval	%eax, %ecx
	xorl	%eax, %eax
.LBB1_3:
	movl	%ecx, (%r14)
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	_ZN8NWindows5NTime17FileTimeToDosTimeERK9_FILETIMERj, .Lfunc_end1-_ZN8NWindows5NTime17FileTimeToDosTimeERK9_FILETIMERj
	.cfi_endproc

	.globl	_ZN8NWindows5NTime18UnixTimeToFileTimeEjR9_FILETIME
	.p2align	4, 0x90
	.type	_ZN8NWindows5NTime18UnixTimeToFileTimeEjR9_FILETIME,@function
_ZN8NWindows5NTime18UnixTimeToFileTimeEjR9_FILETIME: # @_ZN8NWindows5NTime18UnixTimeToFileTimeEjR9_FILETIME
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	imulq	$10000000, %rax, %rax   # imm = 0x989680
	movabsq	$116444736000000000, %rcx # imm = 0x19DB1DED53E8000
	addq	%rax, %rcx
	movl	%ecx, (%rsi)
	shrq	$32, %rcx
	movl	%ecx, 4(%rsi)
	retq
.Lfunc_end2:
	.size	_ZN8NWindows5NTime18UnixTimeToFileTimeEjR9_FILETIME, .Lfunc_end2-_ZN8NWindows5NTime18UnixTimeToFileTimeEjR9_FILETIME
	.cfi_endproc

	.globl	_ZN8NWindows5NTime18FileTimeToUnixTimeERK9_FILETIMERj
	.p2align	4, 0x90
	.type	_ZN8NWindows5NTime18FileTimeToUnixTimeERK9_FILETIMERj,@function
_ZN8NWindows5NTime18FileTimeToUnixTimeERK9_FILETIMERj: # @_ZN8NWindows5NTime18FileTimeToUnixTimeERK9_FILETIMERj
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	movl	4(%rdi), %ecx
	shlq	$32, %rcx
	orq	%rcx, %rax
	xorl	%edx, %edx
	movabsq	$116444736000000000, %rcx # imm = 0x19DB1DED53E8000
	cmpq	%rcx, %rax
	movl	$0, %ecx
	jb	.LBB3_4
# BB#1:
	movabsq	$-116444736000000000, %rcx # imm = 0xFE624E212AC18000
	addq	%rcx, %rax
	movq	%rax, %rcx
	shrq	$39, %rcx
	cmpq	$78124, %rcx            # imm = 0x1312C
	jbe	.LBB3_3
# BB#2:
	movl	$-1, %edx
	xorl	%ecx, %ecx
	jmp	.LBB3_4
.LBB3_3:
	movabsq	$-2972493582642298179, %rcx # imm = 0xD6BF94D5E57A42BD
	mulq	%rcx
	shrq	$23, %rdx
	movb	$1, %cl
.LBB3_4:
	movl	%edx, (%rsi)
	movl	%ecx, %eax
	retq
.Lfunc_end3:
	.size	_ZN8NWindows5NTime18FileTimeToUnixTimeERK9_FILETIMERj, .Lfunc_end3-_ZN8NWindows5NTime18FileTimeToUnixTimeERK9_FILETIMERj
	.cfi_endproc

	.globl	_ZN8NWindows5NTime19GetSecondsSince1601EjjjjjjRy
	.p2align	4, 0x90
	.type	_ZN8NWindows5NTime19GetSecondsSince1601EjjjjjjRy,@function
_ZN8NWindows5NTime19GetSecondsSince1601EjjjjjjRy: # @_ZN8NWindows5NTime19GetSecondsSince1601EjjjjjjRy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	32(%rsp), %r10
	movq	$0, (%r10)
	xorl	%eax, %eax
	cmpl	$59, %r9d
	ja	.LBB4_17
# BB#1:
	cmpl	$59, %r8d
	ja	.LBB4_17
# BB#2:
	cmpl	$23, %ecx
	ja	.LBB4_17
# BB#3:
	decl	%edx
	cmpl	$30, %edx
	ja	.LBB4_17
# BB#4:
	leal	-1601(%rdi), %r11d
	cmpl	$8398, %r11d            # imm = 0x20CE
	ja	.LBB4_17
# BB#5:
	decl	%esi
	cmpl	$11, %esi
	ja	.LBB4_17
# BB#6:
	imull	$365, %r11d, %eax       # imm = 0x16D
	movl	%r11d, %ebx
	shrl	$2, %r11d
	addl	%eax, %r11d
	imulq	$1374389535, %rbx, %rax # imm = 0x51EB851F
	movq	%rax, %rbx
	shrq	$37, %rbx
	subl	%ebx, %r11d
	shrq	$39, %rax
	movabsq	$2242544258451971103, %rbx # imm = 0x1F1F1E1F1E1F1C1F
	movq	%rbx, -16(%rsp)
	movl	$522067742, -8(%rsp)    # imm = 0x1F1E1F1E
	testb	$3, %dil
	jne	.LBB4_10
# BB#7:
	movl	%edi, %ebx
	imulq	$1374389535, %rbx, %r14 # imm = 0x51EB851F
	movq	%r14, %rbx
	shrq	$37, %rbx
	imull	$100, %ebx, %ebx
	cmpl	%ebx, %edi
	jne	.LBB4_9
# BB#8:
	shrq	$39, %r14
	imull	$400, %r14d, %ebx       # imm = 0x190
	subl	%ebx, %edi
	jne	.LBB4_10
.LBB4_9:
	movb	$29, -15(%rsp)
.LBB4_10:                               # %.preheader
	addl	%eax, %r11d
	testl	%esi, %esi
	je	.LBB4_16
# BB#11:                                # %.lr.ph.preheader
	addl	$31, %r11d
	cmpl	$1, %esi
	je	.LBB4_16
# BB#12:                                # %.lr.ph..lr.ph_crit_edge.lr.ph
	movl	%esi, %eax
	leaq	-1(%rax), %r15
	cmpq	$7, %r15
	jbe	.LBB4_13
# BB#18:                                # %min.iters.checked
	movq	%r15, %rsi
	andq	$-8, %rsi
	movq	%r15, %r14
	andq	$-8, %r14
	je	.LBB4_13
# BB#19:                                # %vector.ph
	movd	%r11d, %xmm2
	leaq	-8(%r14), %rdi
	movq	%rdi, %rbx
	shrq	$3, %rbx
	btl	$3, %edi
	jb	.LBB4_20
# BB#21:                                # %vector.body.prol
	movd	-15(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movd	-11(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	pxor	%xmm3, %xmm3
	punpcklbw	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3],xmm1[4],xmm3[4],xmm1[5],xmm3[5],xmm1[6],xmm3[6],xmm1[7],xmm3[7]
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	punpcklbw	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3],xmm0[4],xmm3[4],xmm0[5],xmm3[5],xmm0[6],xmm3[6],xmm0[7],xmm3[7]
	punpcklwd	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3]
	paddd	%xmm2, %xmm1
	movl	$8, %r11d
	movdqa	%xmm1, %xmm2
	testq	%rbx, %rbx
	jne	.LBB4_23
	jmp	.LBB4_25
.LBB4_13:
	movl	$1, %esi
.LBB4_14:                               # %.lr.ph..lr.ph_crit_edge.preheader
	subq	%rsi, %rax
	leaq	-16(%rsp,%rsi), %rsi
	.p2align	4, 0x90
.LBB4_15:                               # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %edi
	addl	%edi, %r11d
	incq	%rsi
	decq	%rax
	jne	.LBB4_15
.LBB4_16:                               # %._crit_edge
	addl	%edx, %r11d
	leal	(%r11,%r11,2), %eax
	leal	(%rcx,%rax,8), %eax
	imulq	$60, %rax, %rax
	movl	%r8d, %ecx
	addq	%rax, %rcx
	imulq	$60, %rcx, %rax
	movl	%r9d, %ecx
	addq	%rax, %rcx
	movq	%rcx, (%r10)
	movb	$1, %al
.LBB4_17:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB4_20:
	pxor	%xmm0, %xmm0
	xorl	%r11d, %r11d
                                        # implicit-def: %XMM1
	testq	%rbx, %rbx
	je	.LBB4_25
.LBB4_23:                               # %vector.ph.new
	movq	%r14, %rbx
	subq	%r11, %rbx
	leaq	-3(%rsp,%r11), %rdi
	pxor	%xmm3, %xmm3
	movdqa	%xmm2, %xmm1
	.p2align	4, 0x90
.LBB4_24:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	-12(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movd	-8(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1],xmm2[2],xmm3[2],xmm2[3],xmm3[3],xmm2[4],xmm3[4],xmm2[5],xmm3[5],xmm2[6],xmm3[6],xmm2[7],xmm3[7]
	punpcklwd	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1],xmm2[2],xmm3[2],xmm2[3],xmm3[3]
	punpcklbw	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1],xmm4[2],xmm3[2],xmm4[3],xmm3[3],xmm4[4],xmm3[4],xmm4[5],xmm3[5],xmm4[6],xmm3[6],xmm4[7],xmm3[7]
	punpcklwd	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1],xmm4[2],xmm3[2],xmm4[3],xmm3[3]
	paddd	%xmm1, %xmm2
	paddd	%xmm0, %xmm4
	movd	-4(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3],xmm1[4],xmm3[4],xmm1[5],xmm3[5],xmm1[6],xmm3[6],xmm1[7],xmm3[7]
	punpcklwd	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1],xmm1[2],xmm3[2],xmm1[3],xmm3[3]
	punpcklbw	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3],xmm0[4],xmm3[4],xmm0[5],xmm3[5],xmm0[6],xmm3[6],xmm0[7],xmm3[7]
	punpcklwd	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1],xmm0[2],xmm3[2],xmm0[3],xmm3[3]
	paddd	%xmm2, %xmm1
	paddd	%xmm4, %xmm0
	addq	$16, %rdi
	addq	$-16, %rbx
	jne	.LBB4_24
.LBB4_25:                               # %middle.block
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %r11d
	cmpq	%r14, %r15
	je	.LBB4_16
# BB#26:
	orq	$1, %rsi
	jmp	.LBB4_14
.Lfunc_end4:
	.size	_ZN8NWindows5NTime19GetSecondsSince1601EjjjjjjRy, .Lfunc_end4-_ZN8NWindows5NTime19GetSecondsSince1601EjjjjjjRy
	.cfi_endproc

	.globl	_ZN8NWindows5NTime17GetCurUtcFileTimeER9_FILETIME
	.p2align	4, 0x90
	.type	_ZN8NWindows5NTime17GetCurUtcFileTimeER9_FILETIME,@function
_ZN8NWindows5NTime17GetCurUtcFileTimeER9_FILETIME: # @_ZN8NWindows5NTime17GetCurUtcFileTimeER9_FILETIME
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -24
.Lcfi16:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	8(%rsp), %r14
	movq	%r14, %rdi
	callq	GetSystemTime
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	SystemTimeToFileTime
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	_ZN8NWindows5NTime17GetCurUtcFileTimeER9_FILETIME, .Lfunc_end5-_ZN8NWindows5NTime17GetCurUtcFileTimeER9_FILETIME
	.cfi_endproc

	.type	.L_ZZN8NWindows5NTime19GetSecondsSince1601EjjjjjjRyE2ms,@object # @_ZZN8NWindows5NTime19GetSecondsSince1601EjjjjjjRyE2ms
	.section	.rodata,"a",@progbits
.L_ZZN8NWindows5NTime19GetSecondsSince1601EjjjjjjRyE2ms:
	.ascii	"\037\034\037\036\037\036\037\037\036\037\036\037"
	.size	.L_ZZN8NWindows5NTime19GetSecondsSince1601EjjjjjjRyE2ms, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
