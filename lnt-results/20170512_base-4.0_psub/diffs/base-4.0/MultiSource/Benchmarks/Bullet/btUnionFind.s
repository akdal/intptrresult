	.text
	.file	"btUnionFind.bc"
	.globl	_ZN11btUnionFindD2Ev
	.p2align	4, 0x90
	.type	_ZN11btUnionFindD2Ev,@function
_ZN11btUnionFindD2Ev:                   # @_ZN11btUnionFindD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#1:
	cmpb	$0, 24(%rbx)
	je	.LBB0_3
# BB#2:
.Ltmp0:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp1:
.LBB0_3:                                # %.noexc
	movq	$0, 16(%rbx)
.LBB0_4:                                # %_ZN20btAlignedObjectArrayI9btElementED2Ev.exit
	movb	$1, 24(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 4(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_5:
.Ltmp2:
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_9
# BB#6:
	cmpb	$0, 24(%rbx)
	je	.LBB0_8
# BB#7:
.Ltmp3:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
.LBB0_8:                                # %.noexc4
	movq	$0, 16(%rbx)
.LBB0_9:
	movb	$1, 24(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 4(%rbx)
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_10:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN11btUnionFindD2Ev, .Lfunc_end0-_ZN11btUnionFindD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\257\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN11btUnionFind4FreeEv
	.p2align	4, 0x90
	.type	_ZN11btUnionFind4FreeEv,@function
_ZN11btUnionFind4FreeEv:                # @_ZN11btUnionFind4FreeEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#1:
	cmpb	$0, 24(%rbx)
	je	.LBB1_3
# BB#2:
	callq	_Z21btAlignedFreeInternalPv
.LBB1_3:
	movq	$0, 16(%rbx)
.LBB1_4:                                # %_ZN20btAlignedObjectArrayI9btElementE5clearEv.exit
	movb	$1, 24(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 4(%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN11btUnionFind4FreeEv, .Lfunc_end1-_ZN11btUnionFind4FreeEv
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN11btUnionFindC2Ev
	.p2align	4, 0x90
	.type	_ZN11btUnionFindC2Ev,@function
_ZN11btUnionFindC2Ev:                   # @_ZN11btUnionFindC2Ev
	.cfi_startproc
# BB#0:
	movb	$1, 24(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 4(%rdi)
	retq
.Lfunc_end3:
	.size	_ZN11btUnionFindC2Ev, .Lfunc_end3-_ZN11btUnionFindC2Ev
	.cfi_endproc

	.globl	_ZN11btUnionFind8allocateEi
	.p2align	4, 0x90
	.type	_ZN11btUnionFind8allocateEi,@function
_ZN11btUnionFind8allocateEi:            # @_ZN11btUnionFind8allocateEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 64
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	movl	4(%r15), %r13d
	cmpl	%r14d, %r13d
	jge	.LBB4_21
# BB#1:
	movslq	%r13d, %rbx
	cmpl	%r14d, 8(%r15)
	jge	.LBB4_2
# BB#3:
	testl	%r14d, %r14d
	je	.LBB4_4
# BB#5:
	movslq	%r14d, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r12
	movl	4(%r15), %eax
	jmp	.LBB4_6
.LBB4_2:                                # %..lr.ph.i_crit_edge
	leaq	16(%r15), %rbp
	jmp	.LBB4_16
.LBB4_4:
	xorl	%r12d, %r12d
	movl	%r13d, %eax
.LBB4_6:                                # %_ZN20btAlignedObjectArrayI9btElementE8allocateEi.exit.i.i
	leaq	16(%r15), %rbp
	testl	%eax, %eax
	jle	.LBB4_11
# BB#7:                                 # %.lr.ph.i.i.i
	cltq
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB4_9
	.p2align	4, 0x90
.LBB4_8:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movq	(%rdi,%rcx,8), %rdi
	movq	%rdi, (%r12,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB4_8
.LBB4_9:                                # %.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB4_11
	.p2align	4, 0x90
.LBB4_10:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, (%r12,%rcx,8)
	movq	(%rbp), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movq	%rdx, 8(%r12,%rcx,8)
	movq	(%rbp), %rdx
	movq	16(%rdx,%rcx,8), %rdx
	movq	%rdx, 16(%r12,%rcx,8)
	movq	(%rbp), %rdx
	movq	24(%rdx,%rcx,8), %rdx
	movq	%rdx, 24(%r12,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB4_10
.LBB4_11:                               # %_ZNK20btAlignedObjectArrayI9btElementE4copyEiiPS0_.exit.i.i
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB4_15
# BB#12:
	cmpb	$0, 24(%r15)
	je	.LBB4_14
# BB#13:
	callq	_Z21btAlignedFreeInternalPv
.LBB4_14:
	movq	$0, (%rbp)
.LBB4_15:                               # %_ZN20btAlignedObjectArrayI9btElementE7reserveEi.exit.preheader.i
	movb	$1, 24(%r15)
	movq	%r12, 16(%r15)
	movl	%r14d, 8(%r15)
.LBB4_16:                               # %.lr.ph.i
	movslq	%r14d, %rax
	movl	%r14d, %edx
	subl	%r13d, %edx
	leaq	-1(%rax), %rcx
	subq	%rbx, %rcx
	andq	$7, %rdx
	je	.LBB4_19
# BB#17:                                # %_ZN20btAlignedObjectArrayI9btElementE7reserveEi.exit.i.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB4_18:                               # %_ZN20btAlignedObjectArrayI9btElementE7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rsi
	movq	$0, (%rsi,%rbx,8)
	incq	%rbx
	incq	%rdx
	jne	.LBB4_18
.LBB4_19:                               # %_ZN20btAlignedObjectArrayI9btElementE7reserveEi.exit.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB4_21
	.p2align	4, 0x90
.LBB4_20:                               # %_ZN20btAlignedObjectArrayI9btElementE7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rcx
	movq	$0, (%rcx,%rbx,8)
	movq	(%rbp), %rcx
	movq	$0, 8(%rcx,%rbx,8)
	movq	(%rbp), %rcx
	movq	$0, 16(%rcx,%rbx,8)
	movq	(%rbp), %rcx
	movq	$0, 24(%rcx,%rbx,8)
	movq	(%rbp), %rcx
	movq	$0, 32(%rcx,%rbx,8)
	movq	(%rbp), %rcx
	movq	$0, 40(%rcx,%rbx,8)
	movq	(%rbp), %rcx
	movq	$0, 48(%rcx,%rbx,8)
	movq	(%rbp), %rcx
	movq	$0, 56(%rcx,%rbx,8)
	addq	$8, %rbx
	cmpq	%rbx, %rax
	jne	.LBB4_20
.LBB4_21:                               # %_ZN20btAlignedObjectArrayI9btElementE6resizeEiRKS0_.exit
	movl	%r14d, 4(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN11btUnionFind8allocateEi, .Lfunc_end4-_ZN11btUnionFind8allocateEi
	.cfi_endproc

	.globl	_ZN11btUnionFind5resetEi
	.p2align	4, 0x90
	.type	_ZN11btUnionFind5resetEi,@function
_ZN11btUnionFind5resetEi:               # @_ZN11btUnionFind5resetEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 64
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r14
	movl	4(%r14), %r13d
	cmpl	%r15d, %r13d
	jge	.LBB5_21
# BB#1:
	movslq	%r13d, %rbp
	cmpl	%r15d, 8(%r14)
	jge	.LBB5_2
# BB#3:
	testl	%r15d, %r15d
	je	.LBB5_4
# BB#5:
	movslq	%r15d, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r12
	movl	4(%r14), %eax
	jmp	.LBB5_6
.LBB5_2:                                # %..lr.ph.i_crit_edge.i
	leaq	16(%r14), %rbx
	jmp	.LBB5_16
.LBB5_4:
	xorl	%r12d, %r12d
	movl	%r13d, %eax
.LBB5_6:                                # %_ZN20btAlignedObjectArrayI9btElementE8allocateEi.exit.i.i.i
	leaq	16(%r14), %rbx
	testl	%eax, %eax
	jle	.LBB5_11
# BB#7:                                 # %.lr.ph.i.i.i.i
	cltq
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB5_9
	.p2align	4, 0x90
.LBB5_8:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	(%rdi,%rcx,8), %rdi
	movq	%rdi, (%r12,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB5_8
.LBB5_9:                                # %.prol.loopexit13
	cmpq	$3, %rdx
	jb	.LBB5_11
	.p2align	4, 0x90
.LBB5_10:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, (%r12,%rcx,8)
	movq	(%rbx), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movq	%rdx, 8(%r12,%rcx,8)
	movq	(%rbx), %rdx
	movq	16(%rdx,%rcx,8), %rdx
	movq	%rdx, 16(%r12,%rcx,8)
	movq	(%rbx), %rdx
	movq	24(%rdx,%rcx,8), %rdx
	movq	%rdx, 24(%r12,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB5_10
.LBB5_11:                               # %_ZNK20btAlignedObjectArrayI9btElementE4copyEiiPS0_.exit.i.i.i
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB5_15
# BB#12:
	cmpb	$0, 24(%r14)
	je	.LBB5_14
# BB#13:
	callq	_Z21btAlignedFreeInternalPv
.LBB5_14:
	movq	$0, (%rbx)
.LBB5_15:                               # %_ZN20btAlignedObjectArrayI9btElementE7reserveEi.exit.preheader.i.i
	movb	$1, 24(%r14)
	movq	%r12, 16(%r14)
	movl	%r15d, 8(%r14)
.LBB5_16:                               # %.lr.ph.i.i
	movslq	%r15d, %rax
	movl	%r15d, %edx
	subl	%r13d, %edx
	leaq	-1(%rax), %rcx
	subq	%rbp, %rcx
	andq	$7, %rdx
	je	.LBB5_19
# BB#17:                                # %_ZN20btAlignedObjectArrayI9btElementE7reserveEi.exit.i.i.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB5_18:                               # %_ZN20btAlignedObjectArrayI9btElementE7reserveEi.exit.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	$0, (%rsi,%rbp,8)
	incq	%rbp
	incq	%rdx
	jne	.LBB5_18
.LBB5_19:                               # %_ZN20btAlignedObjectArrayI9btElementE7reserveEi.exit.i.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB5_21
	.p2align	4, 0x90
.LBB5_20:                               # %_ZN20btAlignedObjectArrayI9btElementE7reserveEi.exit.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	$0, (%rcx,%rbp,8)
	movq	(%rbx), %rcx
	movq	$0, 8(%rcx,%rbp,8)
	movq	(%rbx), %rcx
	movq	$0, 16(%rcx,%rbp,8)
	movq	(%rbx), %rcx
	movq	$0, 24(%rcx,%rbp,8)
	movq	(%rbx), %rcx
	movq	$0, 32(%rcx,%rbp,8)
	movq	(%rbx), %rcx
	movq	$0, 40(%rcx,%rbp,8)
	movq	(%rbx), %rcx
	movq	$0, 48(%rcx,%rbp,8)
	movq	(%rbx), %rcx
	movq	$0, 56(%rcx,%rbp,8)
	addq	$8, %rbp
	cmpq	%rbp, %rax
	jne	.LBB5_20
.LBB5_21:                               # %_ZN11btUnionFind8allocateEi.exit
	movl	%r15d, 4(%r14)
	testl	%r15d, %r15d
	jle	.LBB5_26
# BB#22:                                # %.lr.ph
	movq	16(%r14), %rax
	movl	%r15d, %ecx
	leaq	-1(%rcx), %rsi
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$7, %rdi
	je	.LBB5_24
	.p2align	4, 0x90
.LBB5_23:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rax,%rdx,8)
	movl	$1, 4(%rax,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB5_23
.LBB5_24:                               # %.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB5_26
	.p2align	4, 0x90
.LBB5_25:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rax,%rdx,8)
	movl	$1, 4(%rax,%rdx,8)
	leal	1(%rdx), %esi
	movl	%esi, 8(%rax,%rdx,8)
	movl	$1, 12(%rax,%rdx,8)
	leal	2(%rdx), %esi
	movl	%esi, 16(%rax,%rdx,8)
	movl	$1, 20(%rax,%rdx,8)
	leal	3(%rdx), %esi
	movl	%esi, 24(%rax,%rdx,8)
	movl	$1, 28(%rax,%rdx,8)
	leal	4(%rdx), %esi
	movl	%esi, 32(%rax,%rdx,8)
	movl	$1, 36(%rax,%rdx,8)
	leal	5(%rdx), %esi
	movl	%esi, 40(%rax,%rdx,8)
	movl	$1, 44(%rax,%rdx,8)
	leal	6(%rdx), %esi
	movl	%esi, 48(%rax,%rdx,8)
	movl	$1, 52(%rax,%rdx,8)
	leal	7(%rdx), %esi
	movl	%esi, 56(%rax,%rdx,8)
	movl	$1, 60(%rax,%rdx,8)
	addq	$8, %rdx
	cmpq	%rcx, %rdx
	jne	.LBB5_25
.LBB5_26:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN11btUnionFind5resetEi, .Lfunc_end5-_ZN11btUnionFind5resetEi
	.cfi_endproc

	.globl	_ZN11btUnionFind11sortIslandsEv
	.p2align	4, 0x90
	.type	_ZN11btUnionFind11sortIslandsEv,@function
_ZN11btUnionFind11sortIslandsEv:        # @_ZN11btUnionFind11sortIslandsEv
	.cfi_startproc
# BB#0:
	movl	4(%rdi), %edx
	testl	%edx, %edx
	jle	.LBB6_7
# BB#1:                                 # %.lr.ph
	movq	16(%rdi), %r9
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB6_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_4 Depth 2
	movl	(%r9,%r8,8), %esi
	cmpq	%r8, %rsi
	movl	%r8d, %ecx
	je	.LBB6_5
# BB#3:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB6_2 Depth=1
	leaq	(%r9,%r8,8), %r10
	movq	%r8, %rax
	.p2align	4, 0x90
.LBB6_4:                                # %.lr.ph.i
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%esi, %rcx
	movl	(%r9,%rcx,8), %ecx
	movl	%ecx, (%r10)
	movl	(%r9,%rax,8), %ecx
	movslq	%ecx, %rax
	leaq	(%r9,%rax,8), %r10
	movl	(%r9,%rax,8), %esi
	cmpl	%esi, %ecx
	jne	.LBB6_4
.LBB6_5:                                # %_ZN11btUnionFind4findEi.exit
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	%ecx, (%r9,%r8,8)
	movl	%r8d, 4(%r9,%r8,8)
	incq	%r8
	cmpq	%rdx, %r8
	jne	.LBB6_2
# BB#6:                                 # %._crit_edge
	cmpl	$2, %edx
	jl	.LBB6_7
# BB#8:
	decl	%edx
	xorl	%esi, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	jmp	_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvT_ii # TAILCALL
.LBB6_7:                                # %_ZN20btAlignedObjectArrayI9btElementE9quickSortI31btUnionFindElementSortPredicateEEvT_.exit
	retq
.Lfunc_end6:
	.size	_ZN11btUnionFind11sortIslandsEv, .Lfunc_end6-_ZN11btUnionFind11sortIslandsEv
	.cfi_endproc

	.section	.text._ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvT_ii,"axG",@progbits,_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvT_ii,comdat
	.weak	_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvT_ii
	.p2align	4, 0x90
	.type	_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvT_ii,@function
_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvT_ii: # @_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvT_ii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 48
.Lcfi38:
	.cfi_offset %rbx, -48
.Lcfi39:
	.cfi_offset %r12, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r12d
	movq	%rdi, %r15
	.p2align	4, 0x90
.LBB7_1:                                # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_2 Depth 2
                                        #       Child Loop BB7_3 Depth 3
                                        #       Child Loop BB7_5 Depth 3
	movl	%r12d, %esi
	movq	16(%r15), %r8
	leal	(%rsi,%r14), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	movl	(%r8,%rax,8), %eax
	movl	%r14d, %edi
	jmp	.LBB7_2
	.p2align	4, 0x90
.LBB7_9:                                # %._crit_edge
                                        #   in Loop: Header=BB7_2 Depth=2
	movq	16(%r15), %r8
	movl	%edx, %edi
.LBB7_2:                                #   Parent Loop BB7_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_3 Depth 3
                                        #       Child Loop BB7_5 Depth 3
	movslq	%r12d, %rcx
	leaq	-8(%r8,%rcx,8), %rbp
	movl	%r12d, %ebx
	.p2align	4, 0x90
.LBB7_3:                                #   Parent Loop BB7_1 Depth=1
                                        #     Parent Loop BB7_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%ebx
	cmpl	%eax, 8(%rbp)
	leaq	8(%rbp), %rbp
	jl	.LBB7_3
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB7_2 Depth=2
	movslq	%edi, %rdx
	incq	%rdx
	.p2align	4, 0x90
.LBB7_5:                                #   Parent Loop BB7_1 Depth=1
                                        #     Parent Loop BB7_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	leaq	-1(%rcx), %rdx
	decl	%edi
	cmpl	-8(%r8,%rcx,8), %eax
	jl	.LBB7_5
# BB#6:                                 #   in Loop: Header=BB7_2 Depth=2
	leal	-1(%rbx), %r12d
	leal	1(%rdi), %edx
	cmpl	%edx, %r12d
	jg	.LBB7_8
# BB#7:                                 #   in Loop: Header=BB7_2 Depth=2
	movq	(%rbp), %r9
	movq	-8(%r8,%rcx,8), %rdx
	movq	%rdx, (%rbp)
	movq	16(%r15), %rdx
	movq	%r9, -8(%rdx,%rcx,8)
	movl	%ebx, %r12d
	movl	%edi, %edx
.LBB7_8:                                #   in Loop: Header=BB7_2 Depth=2
	cmpl	%edx, %r12d
	jle	.LBB7_9
# BB#10:                                #   in Loop: Header=BB7_1 Depth=1
	cmpl	%esi, %edx
	jle	.LBB7_12
# BB#11:                                #   in Loop: Header=BB7_1 Depth=1
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvT_ii
.LBB7_12:                               #   in Loop: Header=BB7_1 Depth=1
	cmpl	%r14d, %r12d
	jl	.LBB7_1
# BB#13:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvT_ii, .Lfunc_end7-_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvT_ii
	.cfi_endproc


	.globl	_ZN11btUnionFindD1Ev
	.type	_ZN11btUnionFindD1Ev,@function
_ZN11btUnionFindD1Ev = _ZN11btUnionFindD2Ev
	.globl	_ZN11btUnionFindC1Ev
	.type	_ZN11btUnionFindC1Ev,@function
_ZN11btUnionFindC1Ev = _ZN11btUnionFindC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
