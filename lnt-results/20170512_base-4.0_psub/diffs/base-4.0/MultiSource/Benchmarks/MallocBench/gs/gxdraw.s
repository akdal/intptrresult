	.text
	.file	"gxdraw.bc"
	.globl	gz_fill_rectangle
	.p2align	4, 0x90
	.type	gz_fill_rectangle,@function
gz_fill_rectangle:                      # @gz_fill_rectangle
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%ecx, %r10d
	movl	%edx, %r15d
	movl	%esi, %r11d
	movl	%edi, %r13d
	movq	(%r8), %rbx
	movq	448(%r9), %rax
	movq	(%rax), %rbp
	movl	16(%r8), %edi
	testl	%edi, %edi
	je	.LBB0_12
# BB#1:
	movq	8(%r8), %r14
	movq	24(%r8), %r12
	movl	12(%r12), %esi
	cmpl	%r15d, %esi
	jl	.LBB0_8
# BB#2:
	movl	16(%r12), %ecx
	cmpl	%r10d, %ecx
	jl	.LBB0_8
# BB#3:
	movl	%r13d, %eax
	cltd
	idivl	%esi
	movl	%edx, %r8d
	leal	(%r8,%r15), %eax
	cmpl	%esi, %eax
	jg	.LBB0_8
# BB#4:
	movl	%r11d, %eax
	cltd
	idivl	%ecx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	leal	(%rdx,%r10), %eax
	cmpl	%ecx, %eax
	jle	.LBB0_5
.LBB0_8:                                # %.thread
	testl	%edi, %edi
	movq	$-1, %rax
	cmovsq	%rax, %r14
	cmovsq	%rax, %rbx
	movq	8(%rbp), %rax
	movq	%rbp, %rdi
	movq	%r12, %rsi
	movl	%r13d, %edx
	movl	%r11d, %ecx
	movl	%r15d, %r8d
	movl	%r10d, %r9d
	pushq	%r14
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	movq	%r10, 32(%rsp)          # 8-byte Spill
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movl	%r13d, %r15d
	movl	%r11d, %r13d
	callq	*64(%rax)
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jns	.LBB0_10
# BB#9:
	movq	%rbp, %rdi
	movq	%r12, %rsi
	movl	%r15d, %edx
	movl	%r13d, %ecx
	movq	8(%rsp), %r8            # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	16(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%r14
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	callq	gx_default_tile_rectangle
	addq	$16, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset -16
	xorl	%eax, %eax
.LBB0_10:
	addq	$24, %rsp
.LBB0_11:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_12:
	movq	8(%rbp), %rax
	movq	56(%rax), %rax
	movq	%rbp, %rdi
	movl	%r13d, %esi
	movl	%r11d, %edx
	movl	%r15d, %ecx
	movl	%r10d, %r8d
	movq	%rbx, %r9
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.LBB0_5:
	movl	8(%r12), %ecx
	imull	%ecx, %edx
	movslq	%edx, %rsi
	addq	(%r12), %rsi
	movq	8(%rbp), %rax
	testl	%edi, %edi
	js	.LBB0_6
# BB#7:
	movq	%rbp, %rdi
	movl	%r8d, %edx
	movl	%r13d, %r8d
	movl	%r11d, %r9d
	pushq	%r14
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	callq	*72(%rax)
	addq	$56, %rsp
.Lcfi23:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB0_11
.LBB0_6:
	movq	%rbp, %rdi
	movl	%r8d, %edx
	movl	%r13d, %r8d
	movl	%r11d, %r9d
	pushq	%r10
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	callq	*80(%rax)
	addq	$40, %rsp
.Lcfi26:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB0_11
.Lfunc_end0:
	.size	gz_fill_rectangle, .Lfunc_end0-gz_fill_rectangle
	.cfi_endproc

	.globl	gx_default_tile_rectangle
	.p2align	4, 0x90
	.type	gx_default_tile_rectangle,@function
gx_default_tile_rectangle:              # @gx_default_tile_rectangle
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 192
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movl	%ecx, %ebx
	movl	%edx, %r10d
	movl	8(%rsi), %r13d
	movl	12(%rsi), %ecx
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movl	16(%rsi), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	leal	-1(%rcx), %esi
	testl	%ecx, %esi
	je	.LBB1_1
# BB#2:
	movl	%r10d, %eax
	cltd
	idivl	%ecx
	movl	%edx, %esi
	jmp	.LBB1_3
.LBB1_1:
	andl	%r10d, %esi
.LBB1_3:
	movl	%ebx, %eax
	cltd
	idivl	8(%rsp)                 # 4-byte Folded Reload
	movl	%ecx, %eax
	movl	%esi, %ebp
	subl	%esi, %eax
	movq	%rax, %r14
	movl	%edx, %eax
	imull	%r13d, %eax
	movslq	%eax, %r11
	movq	104(%rsp), %rax         # 8-byte Reload
	addq	(%rax), %r11
	movq	200(%rsp), %rax
	movq	%rax, %rsi
	andq	192(%rsp), %rsi
	movq	8(%rdi), %rax
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	cmpq	$-1, %rsi
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	je	.LBB1_4
# BB#5:
	movq	72(%rax), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 40(%rsp)          # 8-byte Spill
	jmp	.LBB1_6
.LBB1_4:
	movq	80(%rax), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 48(%rsp)          # 8-byte Spill
.LBB1_6:
	movl	%r8d, %eax
	movq	%r14, %rcx
	cmpl	%eax, %ecx
	cmovgl	%eax, %ecx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	testl	%r9d, %r9d
	movq	%r11, %rsi
	movl	%ebp, %r11d
	jle	.LBB1_21
# BB#7:                                 # %.lr.ph163
	addl	%r10d, %r8d
	leal	(%r9,%rbx), %eax
	movl	%r8d, 100(%rsp)         # 4-byte Spill
	movl	%r8d, %r12d
	subl	56(%rsp), %r12d         # 4-byte Folded Reload
	movl	%eax, 88(%rsp)          # 4-byte Spill
	movl	%eax, %ecx
	movl	8(%rsp), %eax           # 4-byte Reload
	subl	%eax, %ecx
	movl	%ecx, 84(%rsp)          # 4-byte Spill
	movl	%eax, %ebp
	subl	%edx, %ebp
	cmpl	%r9d, %ebp
	cmovgl	%r9d, %ebp
	movq	64(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r10), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	%r10, 120(%rsp)         # 8-byte Spill
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	%r13d, 96(%rsp)         # 4-byte Spill
	movl	%r11d, 92(%rsp)         # 4-byte Spill
	movl	%r12d, 36(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB1_8:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_15 Depth 2
                                        #     Child Loop BB1_14 Depth 2
	cmpq	$-1, 72(%rsp)           # 8-byte Folded Reload
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	movl	%r11d, %edx
	movl	%r13d, %ecx
	movl	%r10d, %r8d
	movl	%ebx, %r9d
	je	.LBB1_9
# BB#10:                                #   in Loop: Header=BB1_8 Depth=1
	pushq	200(%rsp)
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	movq	%rbp, %r14
	pushq	%rbp
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)                # 8-byte Folded Reload
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	callq	*80(%rsp)               # 8-byte Folded Reload
	addq	$32, %rsp
.Lcfi44:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB1_11
	.p2align	4, 0x90
.LBB1_9:                                #   in Loop: Header=BB1_8 Depth=1
	movq	%rbp, %r14
	pushq	%rbp
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	pushq	72(%rsp)                # 8-byte Folded Reload
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	callq	*56(%rsp)               # 8-byte Folded Reload
	addq	$16, %rsp
.Lcfi47:
	.cfi_adjust_cfa_offset -16
.LBB1_11:                               #   in Loop: Header=BB1_8 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
	cmpl	%r12d, %eax
	movl	%r12d, %edi
	movl	%eax, %r12d
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	96(%rsp), %ecx          # 4-byte Reload
	movq	128(%rsp), %rbx         # 8-byte Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rbp
	jg	.LBB1_16
# BB#12:                                # %.lr.ph
                                        #   in Loop: Header=BB1_8 Depth=1
	cmpq	$-1, 72(%rsp)           # 8-byte Folded Reload
	movl	12(%rsp), %r12d         # 4-byte Reload
	movl	%ecx, %r13d
	movq	%rdx, %r15
	movq	%rbx, %r14
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	je	.LBB1_13
	.p2align	4, 0x90
.LBB1_15:                               # %.lr.ph.split
                                        #   Parent Loop BB1_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	%r13d, %ecx
	movl	%r12d, %r8d
	movl	%r14d, %r9d
	pushq	200(%rsp)
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	callq	*80(%rsp)               # 8-byte Folded Reload
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	movl	68(%rsp), %edi          # 4-byte Reload
	movq	144(%rsp), %rbp         # 8-byte Reload
	movq	%r14, %rbx
	movq	%r15, %rdx
	movl	%r13d, %ecx
	addq	$32, %rsp
.Lcfi52:
	.cfi_adjust_cfa_offset -32
	addl	%eax, %r12d
	cmpl	%edi, %r12d
	jle	.LBB1_15
	jmp	.LBB1_16
.LBB1_13:                               # %.lr.ph.split.us.preheader
                                        #   in Loop: Header=BB1_8 Depth=1
	movl	12(%rsp), %r12d         # 4-byte Reload
	.p2align	4, 0x90
.LBB1_14:                               # %.lr.ph.split.us
                                        #   Parent Loop BB1_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	%r13d, %ecx
	movl	%r12d, %r8d
	movl	%r14d, %r9d
	pushq	%rbp
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	callq	*56(%rsp)               # 8-byte Folded Reload
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	52(%rsp), %edi          # 4-byte Reload
	movq	128(%rsp), %rbp         # 8-byte Reload
	movq	%r14, %rbx
	movq	%r15, %rdx
	movl	%r13d, %ecx
	addq	$16, %rsp
.Lcfi55:
	.cfi_adjust_cfa_offset -16
	addl	%eax, %r12d
	cmpl	%edi, %r12d
	jle	.LBB1_14
	.p2align	4, 0x90
.LBB1_16:                               # %._crit_edge
                                        #   in Loop: Header=BB1_8 Depth=1
	movl	%edi, %r14d
	movl	100(%rsp), %eax         # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	subl	%r12d, %eax
	movq	%rdx, %rdi
	movl	%ecx, %r13d
	movl	92(%rsp), %r11d         # 4-byte Reload
	movq	%rbp, %r15
	jle	.LBB1_20
# BB#17:                                #   in Loop: Header=BB1_8 Depth=1
	cmpq	$-1, 72(%rsp)           # 8-byte Folded Reload
	je	.LBB1_18
# BB#19:                                #   in Loop: Header=BB1_8 Depth=1
	xorl	%edx, %edx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%r13d, %ecx
	movl	%r12d, %r8d
	movl	%ebx, %r9d
	pushq	200(%rsp)
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	movl	%r11d, %ebp
	callq	*80(%rsp)               # 8-byte Folded Reload
	movl	%ebp, %r11d
	movq	48(%rsp), %rdi          # 8-byte Reload
	addq	$32, %rsp
.Lcfi60:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB1_20
.LBB1_18:                               #   in Loop: Header=BB1_8 Depth=1
	xorl	%edx, %edx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%r13d, %ecx
	movl	%r12d, %r8d
	movl	%ebx, %r9d
	pushq	%r15
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	movl	%r11d, %ebp
	callq	*56(%rsp)               # 8-byte Folded Reload
	movl	%ebp, %r11d
	movq	32(%rsp), %rdi          # 8-byte Reload
	addq	$16, %rsp
.Lcfi63:
	.cfi_adjust_cfa_offset -16
	.p2align	4, 0x90
.LBB1_20:                               #   in Loop: Header=BB1_8 Depth=1
	addl	%r15d, %ebx
	movl	88(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebp
	subl	%ebx, %ebp
	cmpl	84(%rsp), %ebx          # 4-byte Folded Reload
	cmovlel	8(%rsp), %ebp           # 4-byte Folded Reload
	cmpl	%ebx, %eax
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rsi
	movq	120(%rsp), %r10         # 8-byte Reload
	movl	%r14d, %r12d
	jg	.LBB1_8
.LBB1_21:                               # %._crit_edge164
	xorl	%eax, %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	gx_default_tile_rectangle, .Lfunc_end1-gx_default_tile_rectangle
	.cfi_endproc

	.globl	gz_fill_trapezoid_fixed
	.p2align	4, 0x90
	.type	gz_fill_trapezoid_fixed,@function
gz_fill_trapezoid_fixed:                # @gz_fill_trapezoid_fixed
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi70:
	.cfi_def_cfa_offset 112
.Lcfi71:
	.cfi_offset %rbx, -56
.Lcfi72:
	.cfi_offset %r12, -48
.Lcfi73:
	.cfi_offset %r13, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movq	%rcx, %r11
	movq	%rdx, %r10
	movq	%rdi, %r15
	leaq	4095(%r15,%rsi), %r14
	shrq	$12, %r15
	shrq	$12, %r14
	movl	%r14d, %r12d
	subl	%r15d, %r12d
	leaq	4095(%r11,%r8), %r8
	shrq	$12, %r11
	shrq	$12, %r8
	movl	%r8d, %eax
	subl	%r11d, %eax
	movl	%eax, %ecx
	orl	%r12d, %ecx
	je	.LBB2_42
# BB#1:
	leaq	4095(%r10,%r9), %rdx
	shrq	$12, %r10
	shrq	$12, %rdx
	movq	%rdx, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	%edx, %r9d
	subl	%r10d, %r9d
	testl	%r9d, %r9d
	jle	.LBB2_42
# BB#2:
	movl	112(%rsp), %esi
	testl	%esi, %esi
	movl	%r9d, 20(%rsp)          # 4-byte Spill
	jne	.LBB2_5
# BB#3:
	movq	120(%rsp), %rcx
	cmpl	$0, 16(%rcx)
	je	.LBB2_4
.LBB2_5:
	subl	%r14d, %r8d
	subl	%r15d, %r11d
	js	.LBB2_9
# BB#6:
	cmpl	%r9d, %r11d
	jge	.LBB2_8
# BB#7:
	movl	$1, 4(%rsp)             # 4-byte Folded Spill
	jmp	.LBB2_12
.LBB2_9:
	movl	%r9d, %eax
	negl	%eax
	cmpl	%eax, %r11d
	jle	.LBB2_10
# BB#11:
	negl	%r11d
	movl	$-1, 4(%rsp)            # 4-byte Folded Spill
.LBB2_12:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testl	%r8d, %r8d
	jns	.LBB2_14
	jmp	.LBB2_22
.LBB2_8:
	movl	%r11d, %eax
	cltd
	idivl	%r9d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	1(%rax), %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movq	%rax, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r9d, %eax
	subl	%eax, %r11d
	testl	%r8d, %r8d
	jns	.LBB2_14
	jmp	.LBB2_22
.LBB2_10:
	movl	%r11d, %eax
	cltd
	idivl	%r9d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-1(%rax), %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movq	%rax, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r9d, %eax
	subl	%r11d, %eax
	movl	%eax, %r11d
	testl	%r8d, %r8d
	js	.LBB2_22
.LBB2_14:
	cmpl	%r9d, %r8d
	jge	.LBB2_15
# BB#16:
	movq	8(%rsp), %rax           # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	orl	%r8d, %eax
	orl	%r11d, %eax
	je	.LBB2_18
# BB#17:
	movl	$1, (%rsp)              # 4-byte Folded Spill
	jmp	.LBB2_25
.LBB2_22:
	movl	%r9d, %eax
	negl	%eax
	cmpl	%eax, %r8d
	jle	.LBB2_23
# BB#24:
	negl	%r8d
	movl	$-1, (%rsp)             # 4-byte Folded Spill
.LBB2_25:
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB2_26
.LBB2_15:
	movl	%r8d, %eax
	cltd
	idivl	%r9d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	1(%rax), %ecx
	movl	%ecx, (%rsp)            # 4-byte Spill
	movq	%rax, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r9d, %eax
	subl	%eax, %r8d
	jmp	.LBB2_26
.LBB2_23:
	movl	%r8d, %eax
	cltd
	idivl	%r9d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-1(%rax), %ecx
	movl	%ecx, (%rsp)            # 4-byte Spill
	movq	%rax, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%r9d, %eax
	subl	%r8d, %eax
	movl	%eax, %r8d
.LBB2_26:
	movl	%r11d, %ebp
	sarl	%ebp
	movl	%r8d, %ebx
	sarl	%ebx
	testl	%esi, %esi
	movq	%r11, 48(%rsp)          # 8-byte Spill
	movq	%r8, 40(%rsp)           # 8-byte Spill
	je	.LBB2_27
# BB#32:                                # %.split.us.preheader
	xorl	%edx, %edx
	movl	%r15d, %esi
	movl	%r14d, %ecx
	movl	%r10d, %eax
	.p2align	4, 0x90
.LBB2_33:                               # %.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	%r14d, %r12d
	movl	%r15d, %r13d
	cmpl	%esi, %r13d
	jne	.LBB2_35
# BB#34:                                # %.split.us
                                        #   in Loop: Header=BB2_33 Depth=1
	cmpl	%ecx, %r12d
	je	.LBB2_36
.LBB2_35:                               #   in Loop: Header=BB2_33 Depth=1
	movl	%r10d, %edx
	subl	%eax, %edx
	subl	%esi, %ecx
	movl	%eax, %edi
	movq	120(%rsp), %r8
	movq	128(%rsp), %r9
	movq	%r10, %r14
	callq	gz_fill_rectangle
	xorl	%edx, %edx
	movl	20(%rsp), %r9d          # 4-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	48(%rsp), %r11          # 8-byte Reload
	movq	%r14, %r10
	movl	%r10d, %eax
.LBB2_36:                               #   in Loop: Header=BB2_33 Depth=1
	addl	%r11d, %ebp
	cmpl	%r9d, %ebp
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, %r15d
	cmovll	8(%rsp), %r15d          # 4-byte Folded Reload
	movl	%r9d, %ecx
	cmovll	%edx, %ecx
	subl	%ecx, %ebp
	addl	%r13d, %r15d
	addl	%r8d, %ebx
	cmpl	%r9d, %ebx
	movl	(%rsp), %ecx            # 4-byte Reload
	movl	%ecx, %r14d
	cmovll	24(%rsp), %r14d         # 4-byte Folded Reload
	movl	%r9d, %ecx
	cmovll	%edx, %ecx
	subl	%ecx, %ebx
	addl	%r12d, %r14d
	incl	%r10d
	cmpl	32(%rsp), %r10d         # 4-byte Folded Reload
	movl	%r13d, %esi
	movl	%r12d, %ecx
	jl	.LBB2_33
	jmp	.LBB2_37
.LBB2_27:
	xorl	%esi, %esi
	movl	%r15d, %edi
	movl	%r14d, %edx
	movl	%r10d, %eax
	.p2align	4, 0x90
.LBB2_28:                               # %.split
                                        # =>This Inner Loop Header: Depth=1
	movl	%r14d, %r12d
	movl	%r15d, %r13d
	cmpl	%edi, %r13d
	jne	.LBB2_30
# BB#29:                                # %.split
                                        #   in Loop: Header=BB2_28 Depth=1
	cmpl	%edx, %r12d
	je	.LBB2_31
.LBB2_30:                               #   in Loop: Header=BB2_28 Depth=1
	subl	%edi, %edx
	movl	%r10d, %ecx
	subl	%eax, %ecx
	movl	%eax, %esi
	movq	120(%rsp), %r8
	movq	128(%rsp), %r9
	movq	%r10, %r14
	callq	gz_fill_rectangle
	xorl	%esi, %esi
	movl	20(%rsp), %r9d          # 4-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	48(%rsp), %r11          # 8-byte Reload
	movq	%r14, %r10
	movl	%r10d, %eax
.LBB2_31:                               #   in Loop: Header=BB2_28 Depth=1
	addl	%r11d, %ebp
	cmpl	%r9d, %ebp
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, %r15d
	cmovll	8(%rsp), %r15d          # 4-byte Folded Reload
	movl	%r9d, %ecx
	cmovll	%esi, %ecx
	subl	%ecx, %ebp
	addl	%r13d, %r15d
	addl	%r8d, %ebx
	cmpl	%r9d, %ebx
	movl	(%rsp), %ecx            # 4-byte Reload
	movl	%ecx, %r14d
	cmovll	24(%rsp), %r14d         # 4-byte Folded Reload
	movl	%r9d, %ecx
	cmovll	%esi, %ecx
	subl	%ecx, %ebx
	addl	%r12d, %r14d
	incl	%r10d
	cmpl	32(%rsp), %r10d         # 4-byte Folded Reload
	movl	%r13d, %edi
	movl	%r12d, %edx
	jl	.LBB2_28
.LBB2_37:                               # %.us-lcssa.us
	subl	%eax, %r10d
	movl	112(%rsp), %ecx
	je	.LBB2_42
# BB#38:
	subl	%r13d, %r12d
	testl	%ecx, %ecx
	je	.LBB2_40
# BB#39:
	movl	%eax, %edi
	movl	%r13d, %esi
	movl	%r10d, %edx
	movl	%r12d, %ecx
	jmp	.LBB2_41
.LBB2_40:
	movl	%r13d, %edi
	movl	%eax, %esi
	movl	%r12d, %edx
	movl	%r10d, %ecx
	jmp	.LBB2_41
.LBB2_4:
	movq	128(%rsp), %rcx
	movq	448(%rcx), %rcx
	movq	(%rcx), %rdi
	movq	8(%rdi), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	%r15d, %esi
	movl	%r10d, %edx
	movl	%r12d, %ecx
	movq	%r8, %r13
	movl	%r11d, %r8d
	movq	32(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	movq	120(%rsp), %rbx
	pushq	(%rbx)
.Lcfi77:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi78:
	.cfi_adjust_cfa_offset 8
	movq	%r10, %rbx
	movq	%r11, %rbp
	movq	24(%rsp), %rax          # 8-byte Reload
	callq	*96(%rax)
	movl	128(%rsp), %esi
	movl	36(%rsp), %r9d          # 4-byte Reload
	movq	%r13, %r8
	movq	%rbp, %r11
	movq	%rbx, %r10
	addq	$16, %rsp
.Lcfi79:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jns	.LBB2_42
	jmp	.LBB2_5
.LBB2_18:
	testl	%esi, %esi
	je	.LBB2_21
# BB#19:                                # %.critedge235
	movl	%r10d, %edi
	movl	%r15d, %esi
	movl	%r9d, %edx
	movl	%r12d, %ecx
	jmp	.LBB2_41
.LBB2_21:                               # %.critedge
	movl	%r15d, %edi
	movl	%r10d, %esi
	movl	%r12d, %edx
	movl	%r9d, %ecx
.LBB2_41:
	movq	120(%rsp), %r8
	movq	128(%rsp), %r9
	callq	gz_fill_rectangle
.LBB2_42:
	xorl	%eax, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	gz_fill_trapezoid_fixed, .Lfunc_end2-gz_fill_trapezoid_fixed
	.cfi_endproc

	.globl	gz_draw_line_fixed
	.p2align	4, 0x90
	.type	gz_draw_line_fixed,@function
gz_draw_line_fixed:                     # @gz_draw_line_fixed
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi83:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi84:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi86:
	.cfi_def_cfa_offset 64
.Lcfi87:
	.cfi_offset %rbx, -56
.Lcfi88:
	.cfi_offset %r12, -48
.Lcfi89:
	.cfi_offset %r13, -40
.Lcfi90:
	.cfi_offset %r14, -32
.Lcfi91:
	.cfi_offset %r15, -24
.Lcfi92:
	.cfi_offset %rbp, -16
	movq	%r9, %r10
	movq	%r8, %r15
	movq	%rcx, %r14
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	%rbx, %rdx
	shrq	$12, %rdx
	movq	%r13, %rax
	shrq	$12, %rax
	movq	%r12, %rbp
	shrq	$12, %rbp
	movq	%r14, %r8
	shrq	$12, %r8
	cmpl	%eax, %r8d
	jne	.LBB3_5
# BB#1:
	cmpl	%ebp, %edx
	jle	.LBB3_2
# BB#4:
	addq	$4095, %rbx             # imm = 0xFFF
	shrq	$12, %rbx
	subl	%ebp, %ebx
	movl	$1, %ecx
	movl	%ebp, %edi
	movl	%eax, %esi
	movl	%ebx, %edx
	jmp	.LBB3_3
.LBB3_5:
	cmpl	$0, 16(%r15)
	jne	.LBB3_7
# BB#6:
	movq	448(%r10), %rcx
	movq	(%rcx), %rdi
	movq	%r10, (%rsp)            # 8-byte Spill
	movq	8(%rdi), %r10
	movq	(%r15), %r9
	movl	%edx, %esi
	movl	%eax, %edx
	movl	%ebp, %ecx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	*88(%r10)
	movq	(%rsp), %r10            # 8-byte Reload
	testl	%eax, %eax
	jns	.LBB3_10
.LBB3_7:
	movq	%r14, %rcx
	subq	%r13, %rcx
	movq	%rcx, %r9
	negq	%r9
	cmovlq	%rcx, %r9
	movq	%r12, %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rax
	negq	%rax
	cmovlq	%rdx, %rax
	cmpq	%r9, %rax
	jle	.LBB3_8
# BB#9:
	testq	%rdx, %rdx
	movq	%r14, %rcx
	cmovsq	%r13, %rcx
	cmovsq	%r14, %r13
	cmovsq	%r12, %rbx
	subq	$8, %rsp
.Lcfi93:
	.cfi_adjust_cfa_offset 8
	movl	$1, %esi
	movl	$1, %r8d
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%rax, %r9
	pushq	%r10
.Lcfi94:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi95:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi96:
	.cfi_adjust_cfa_offset 8
	callq	gz_fill_trapezoid_fixed
	addq	$32, %rsp
.Lcfi97:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB3_10
.LBB3_2:
	addq	$4095, %r12             # imm = 0xFFF
	shrq	$12, %r12
	subl	%edx, %r12d
	movl	$1, %ecx
	movl	%edx, %edi
	movl	%eax, %esi
	movl	%r12d, %edx
.LBB3_3:                                # %.critedge
	movq	%r15, %r8
	movq	%r10, %r9
	callq	gz_fill_rectangle
	jmp	.LBB3_10
.LBB3_8:
	testq	%rcx, %rcx
	movq	%r12, %rcx
	cmovsq	%rbx, %rcx
	cmovsq	%r14, %r13
	cmovsq	%r12, %rbx
	subq	$8, %rsp
.Lcfi98:
	.cfi_adjust_cfa_offset 8
	movl	$1, %esi
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	%r13, %rdx
	pushq	%r10
.Lcfi99:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi100:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi101:
	.cfi_adjust_cfa_offset 8
	callq	gz_fill_trapezoid_fixed
	addq	$32, %rsp
.Lcfi102:
	.cfi_adjust_cfa_offset -32
.LBB3_10:                               # %.critedge
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	gz_draw_line_fixed, .Lfunc_end3-gz_draw_line_fixed
	.cfi_endproc

	.globl	gx_default_draw_line
	.p2align	4, 0x90
	.type	gx_default_draw_line,@function
gx_default_draw_line:                   # @gx_default_draw_line
	.cfi_startproc
# BB#0:
	movl	$-1, %eax
	retq
.Lfunc_end4:
	.size	gx_default_draw_line, .Lfunc_end4-gx_default_draw_line
	.cfi_endproc

	.globl	gx_default_fill_trapezoid
	.p2align	4, 0x90
	.type	gx_default_fill_trapezoid,@function
gx_default_fill_trapezoid:              # @gx_default_fill_trapezoid
	.cfi_startproc
# BB#0:
	movl	$-1, %eax
	retq
.Lfunc_end5:
	.size	gx_default_fill_trapezoid, .Lfunc_end5-gx_default_fill_trapezoid
	.cfi_endproc

	.globl	gx_default_tile_trapezoid
	.p2align	4, 0x90
	.type	gx_default_tile_trapezoid,@function
gx_default_tile_trapezoid:              # @gx_default_tile_trapezoid
	.cfi_startproc
# BB#0:
	movl	$-1, %eax
	retq
.Lfunc_end6:
	.size	gx_default_tile_trapezoid, .Lfunc_end6-gx_default_tile_trapezoid
	.cfi_endproc

	.globl	gx_default_tile_trapezoid_color
	.p2align	4, 0x90
	.type	gx_default_tile_trapezoid_color,@function
gx_default_tile_trapezoid_color:        # @gx_default_tile_trapezoid_color
	.cfi_startproc
# BB#0:
	movl	$-1, %eax
	retq
.Lfunc_end7:
	.size	gx_default_tile_trapezoid_color, .Lfunc_end7-gx_default_tile_trapezoid_color
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
