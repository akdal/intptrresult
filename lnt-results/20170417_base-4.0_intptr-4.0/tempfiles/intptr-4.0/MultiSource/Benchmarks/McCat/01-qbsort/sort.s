	.text
	.file	"sort.bc"
	.globl	BubbleSort
	.p2align	4, 0x90
	.type	BubbleSort,@function
BubbleSort:                             # @BubbleSort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	(%r13), %eax
	testl	%eax, %eax
	jle	.LBB0_7
# BB#1:                                 # %.preheader.lr.ph
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
	movl	%r15d, %r12d
	notl	%r12d
	leal	(%rax,%r12), %ecx
	testl	%ecx, %ecx
	jle	.LBB0_6
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rax
	leaq	1(%rbp), %rbx
	movl	4(%rax,%rbp,4), %edi
	movl	(%rax,%rbp,4), %esi
	callq	*%r14
	testl	%eax, %eax
	je	.LBB0_5
# BB#8:                                 #   in Loop: Header=BB0_4 Depth=2
	movq	8(%r13), %rax
	movl	(%rax,%rbp,4), %ecx
	movl	4(%rax,%rbp,4), %edx
	movl	%edx, (%rax,%rbp,4)
	movl	%ecx, 4(%rax,%rbp,4)
.LBB0_5:                                # %.backedge
                                        #   in Loop: Header=BB0_4 Depth=2
	movl	(%r13), %eax
	leal	(%rax,%r12), %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rbx
	movq	%rbx, %rbp
	jl	.LBB0_4
.LBB0_6:                                # %._crit_edge
                                        #   in Loop: Header=BB0_2 Depth=1
	incl	%r15d
	cmpl	%eax, %r15d
	jl	.LBB0_2
.LBB0_7:                                # %._crit_edge29
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	BubbleSort, .Lfunc_end0-BubbleSort
	.cfi_endproc

	.globl	QuickSort
	.p2align	4, 0x90
	.type	QuickSort,@function
QuickSort:                              # @QuickSort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB1_1
# BB#2:
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r14
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r14)
	movq	8(%rbx), %r15
	testq	%r15, %r15
	je	.LBB1_3
# BB#4:                                 # %.lr.ph
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_5:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rbp
	movl	(%r15), %edi
	movl	(%r14), %esi
	callq	*%r12
	movl	%eax, %ebx
	movl	$16, %edi
	callq	malloc
	testl	%ebx, %ebx
	movl	(%r15), %ecx
	movl	%ecx, (%rax)
	movq	%rax, %rbx
	cmoveq	%rbp, %rbx
	cmoveq	%r13, %rbp
	cmoveq	%rax, %r13
	movq	%rbp, 8(%rax)
	movq	8(%r15), %r15
	testq	%r15, %r15
	jne	.LBB1_5
# BB#6:                                 # %._crit_edge
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	QuickSort
	movq	%rax, %r15
	testq	%r13, %r13
	je	.LBB1_8
	.p2align	4, 0x90
.LBB1_7:                                # %.lr.ph.i53
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r13), %rbp
	movq	%r13, %rdi
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %r13
	jne	.LBB1_7
.LBB1_8:                                # %FreeLinkList.exit54
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	QuickSort
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.LBB1_10
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rbp
	movq	%rbx, %rdi
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB1_9
	jmp	.LBB1_10
.LBB1_1:
	xorl	%r12d, %r12d
	jmp	.LBB1_16
.LBB1_3:                                # %FreeLinkList.exit54.thread
	xorl	%edi, %edi
	movq	%r12, %rsi
	callq	QuickSort
	movq	%rax, %r15
	xorl	%edi, %edi
	movq	%r12, %rsi
	callq	QuickSort
	movq	%rax, %r12
.LBB1_10:                               # %FreeLinkList.exit
	testq	%r12, %r12
	je	.LBB1_11
# BB#12:                                # %.preheader.preheader
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB1_13:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB1_13
# BB#14:
	movq	%r14, 8(%rax)
	jmp	.LBB1_15
.LBB1_11:
	movq	%r14, %r12
.LBB1_15:
	movq	%r15, 8(%r14)
.LBB1_16:
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	QuickSort, .Lfunc_end1-QuickSort
	.cfi_endproc

	.globl	FreeLinkList
	.p2align	4, 0x90
	.type	FreeLinkList,@function
FreeLinkList:                           # @FreeLinkList
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 16
.Lcfi27:
	.cfi_offset %rbx, -16
	testq	%rdi, %rdi
	je	.LBB2_2
	.p2align	4, 0x90
.LBB2_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB2_1
.LBB2_2:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end2:
	.size	FreeLinkList, .Lfunc_end2-FreeLinkList
	.cfi_endproc

	.globl	PrintList
	.p2align	4, 0x90
	.type	PrintList,@function
PrintList:                              # @PrintList
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$91, %edi
	callq	putchar
	movl	(%r14), %ecx
	movq	8(%r14), %rax
	cmpl	$2, %ecx
	jl	.LBB3_1
# BB#2:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rbx,4), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	movslq	(%r14), %rcx
	decq	%rcx
	movq	8(%r14), %rax
	cmpq	%rcx, %rbx
	jl	.LBB3_3
	jmp	.LBB3_4
.LBB3_1:
	decl	%ecx
.LBB3_4:                                # %._crit_edge
	movslq	%ecx, %rcx
	movl	(%rax,%rcx,4), %esi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	printf                  # TAILCALL
.Lfunc_end3:
	.size	PrintList, .Lfunc_end3-PrintList
	.cfi_endproc

	.globl	PrintLinkList
	.p2align	4, 0x90
	.type	PrintLinkList,@function
PrintLinkList:                          # @PrintLinkList
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 16
.Lcfi34:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB4_5
# BB#1:
	movl	$91, %edi
	callq	putchar
	cmpq	$0, 8(%rbx)
	movl	(%rbx), %esi
	je	.LBB4_4
# BB#2:                                 # %.lr.ph.preheader
	addq	$8, %rbx
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rbx), %rax
	movl	(%rax), %esi
	cmpq	$0, 8(%rax)
	leaq	8(%rax), %rbx
	jne	.LBB4_3
.LBB4_4:                                # %._crit_edge
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	popq	%rbx
	jmp	printf                  # TAILCALL
.LBB4_5:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	popq	%rbx
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	PrintLinkList, .Lfunc_end4-PrintLinkList
	.cfi_endproc

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"%d,"
	.size	.L.str.1, 4

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%d]"
	.size	.L.str.2, 4

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"[]"
	.size	.L.str.3, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
