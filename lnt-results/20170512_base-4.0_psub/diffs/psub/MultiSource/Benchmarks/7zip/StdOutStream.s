	.text
	.file	"StdOutStream.bc"
	.globl	_ZN13CStdOutStream4OpenEPKc
	.p2align	4, 0x90
	.type	_ZN13CStdOutStream4OpenEPKc,@function
_ZN13CStdOutStream4OpenEPKc:            # @_ZN13CStdOutStream4OpenEPKc
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpb	$0, (%rbx)
	je	.LBB0_3
# BB#1:
	movq	8(%rbx), %rdi
	callq	fclose
	testl	%eax, %eax
	jne	.LBB0_3
# BB#2:
	movq	$0, 8(%rbx)
	movb	$0, (%rbx)
.LBB0_3:                                # %_ZN13CStdOutStream5CloseEv.exit
	movl	$.L.str, %esi
	movq	%r14, %rdi
	callq	fopen64
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	setne	%al
	setne	(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	_ZN13CStdOutStream4OpenEPKc, .Lfunc_end0-_ZN13CStdOutStream4OpenEPKc
	.cfi_endproc

	.globl	_ZN13CStdOutStream5CloseEv
	.p2align	4, 0x90
	.type	_ZN13CStdOutStream5CloseEv,@function
_ZN13CStdOutStream5CloseEv:             # @_ZN13CStdOutStream5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movb	$1, %bpl
	cmpb	$0, (%rbx)
	je	.LBB1_4
# BB#1:
	movq	8(%rbx), %rdi
	callq	fclose
	testl	%eax, %eax
	je	.LBB1_3
# BB#2:
	xorl	%ebp, %ebp
	jmp	.LBB1_4
.LBB1_3:
	movq	$0, 8(%rbx)
	movb	$0, (%rbx)
.LBB1_4:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN13CStdOutStream5CloseEv, .Lfunc_end1-_ZN13CStdOutStream5CloseEv
	.cfi_endproc

	.globl	_ZN13CStdOutStream5FlushEv
	.p2align	4, 0x90
	.type	_ZN13CStdOutStream5FlushEv,@function
_ZN13CStdOutStream5FlushEv:             # @_ZN13CStdOutStream5FlushEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 16
	movq	8(%rdi), %rdi
	callq	fflush
	testl	%eax, %eax
	sete	%al
	popq	%rcx
	retq
.Lfunc_end2:
	.size	_ZN13CStdOutStream5FlushEv, .Lfunc_end2-_ZN13CStdOutStream5FlushEv
	.cfi_endproc

	.globl	_ZN13CStdOutStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN13CStdOutStreamD2Ev,@function
_ZN13CStdOutStreamD2Ev:                 # @_ZN13CStdOutStreamD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
.Lcfi12:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpb	$0, (%rbx)
	je	.LBB3_3
# BB#1:
	movq	8(%rbx), %rdi
	callq	fclose
	testl	%eax, %eax
	jne	.LBB3_3
# BB#2:
	movq	$0, 8(%rbx)
	movb	$0, (%rbx)
.LBB3_3:                                # %_ZN13CStdOutStream5CloseEv.exit
	popq	%rbx
	retq
.Lfunc_end3:
	.size	_ZN13CStdOutStreamD2Ev, .Lfunc_end3-_ZN13CStdOutStreamD2Ev
	.cfi_endproc

	.globl	_ZN13CStdOutStreamlsEPFRS_S0_E
	.p2align	4, 0x90
	.type	_ZN13CStdOutStreamlsEPFRS_S0_E,@function
_ZN13CStdOutStreamlsEPFRS_S0_E:         # @_ZN13CStdOutStreamlsEPFRS_S0_E
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	*%rsi
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZN13CStdOutStreamlsEPFRS_S0_E, .Lfunc_end4-_ZN13CStdOutStreamlsEPFRS_S0_E
	.cfi_endproc

	.globl	_Z4endlR13CStdOutStream
	.p2align	4, 0x90
	.type	_Z4endlR13CStdOutStream,@function
_Z4endlR13CStdOutStream:                # @_Z4endlR13CStdOutStream
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 16
.Lcfi16:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rsi
	movl	$10, %edi
	callq	fputc
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	_Z4endlR13CStdOutStream, .Lfunc_end5-_Z4endlR13CStdOutStream
	.cfi_endproc

	.globl	_ZN13CStdOutStreamlsEc
	.p2align	4, 0x90
	.type	_ZN13CStdOutStreamlsEc,@function
_ZN13CStdOutStreamlsEc:                 # @_ZN13CStdOutStreamlsEc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 16
.Lcfi18:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	%esi, %edi
	movq	%rax, %rsi
	callq	fputc
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end6:
	.size	_ZN13CStdOutStreamlsEc, .Lfunc_end6-_ZN13CStdOutStreamlsEc
	.cfi_endproc

	.globl	_ZN13CStdOutStreamlsEPKc
	.p2align	4, 0x90
	.type	_ZN13CStdOutStreamlsEPKc,@function
_ZN13CStdOutStreamlsEPKc:               # @_ZN13CStdOutStreamlsEPKc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 16
.Lcfi20:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	fputs
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end7:
	.size	_ZN13CStdOutStreamlsEPKc, .Lfunc_end7-_ZN13CStdOutStreamlsEPKc
	.cfi_endproc

	.globl	_ZN13CStdOutStreamlsEPKw
	.p2align	4, 0x90
	.type	_ZN13CStdOutStreamlsEPKw,@function
_ZN13CStdOutStreamlsEPKw:               # @_ZN13CStdOutStreamlsEPKw
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 80
.Lcfi26:
	.cfi_offset %rbx, -40
.Lcfi27:
	.cfi_offset %r12, -32
.Lcfi28:
	.cfi_offset %r14, -24
.Lcfi29:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %r12d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB8_1:                                # =>This Inner Loop Header: Depth=1
	incl	%r12d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB8_1
# BB#2:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%r12), %eax
	movslq	%eax, %r15
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%rsp)
	movl	$0, (%rax)
	movl	%r15d, 12(%rsp)
	.p2align	4, 0x90
.LBB8_3:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_3
# BB#4:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
	movl	%r12d, 8(%rsp)
.Ltmp0:
	leaq	24(%rsp), %rdi
	movq	%rsp, %rsi
	movl	$1, %edx
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp1:
# BB#5:
	movq	24(%rsp), %rdi
	movq	8(%r14), %rsi
	callq	fputs
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_7
# BB#6:
	callq	_ZdaPv
.LBB8_7:                                # %_ZN11CStringBaseIcED2Ev.exit
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_9
# BB#8:
	callq	_ZdaPv
.LBB8_9:                                # %_ZN11CStringBaseIwED2Ev.exit4
	movq	%r14, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB8_10:
.Ltmp2:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_12
# BB#11:
	callq	_ZdaPv
.LBB8_12:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN13CStdOutStreamlsEPKw, .Lfunc_end8-_ZN13CStdOutStreamlsEPKw
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end8-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN13CStdOutStreamlsEi
	.p2align	4, 0x90
	.type	_ZN13CStdOutStreamlsEi,@function
_ZN13CStdOutStreamlsEi:                 # @_ZN13CStdOutStreamlsEi
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -24
.Lcfi34:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movq	%rsp, %r14
	movq	%r14, %rsi
	callq	_Z20ConvertInt64ToStringxPc
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	fputs
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	_ZN13CStdOutStreamlsEi, .Lfunc_end9-_ZN13CStdOutStreamlsEi
	.cfi_endproc

	.globl	_ZN13CStdOutStreamlsEy
	.p2align	4, 0x90
	.type	_ZN13CStdOutStreamlsEy,@function
_ZN13CStdOutStreamlsEy:                 # @_ZN13CStdOutStreamlsEy
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 64
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	%rsp, %r14
	movl	$10, %edx
	movq	%rsi, %rdi
	movq	%r14, %rsi
	callq	_Z21ConvertUInt64ToStringyPcj
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	fputs
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	_ZN13CStdOutStreamlsEy, .Lfunc_end10-_ZN13CStdOutStreamlsEy
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_StdOutStream.ii,@function
_GLOBAL__sub_I_StdOutStream.ii:         # @_GLOBAL__sub_I_StdOutStream.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 16
	movq	stdout(%rip), %rax
	movb	$0, g_StdOut(%rip)
	movq	%rax, g_StdOut+8(%rip)
	movl	$_ZN13CStdOutStreamD2Ev, %edi
	movl	$g_StdOut, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movq	stderr(%rip), %rax
	movb	$0, g_StdErr(%rip)
	movq	%rax, g_StdErr+8(%rip)
	movl	$_ZN13CStdOutStreamD2Ev, %edi
	movl	$g_StdErr, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end11:
	.size	_GLOBAL__sub_I_StdOutStream.ii, .Lfunc_end11-_GLOBAL__sub_I_StdOutStream.ii
	.cfi_endproc

	.type	g_StdOut,@object        # @g_StdOut
	.bss
	.globl	g_StdOut
	.p2align	3
g_StdOut:
	.zero	16
	.size	g_StdOut, 16

	.type	g_StdErr,@object        # @g_StdErr
	.globl	g_StdErr
	.p2align	3
g_StdErr:
	.zero	16
	.size	g_StdErr, 16

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"wt"
	.size	.L.str, 3

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_StdOutStream.ii

	.globl	_ZN13CStdOutStreamD1Ev
	.type	_ZN13CStdOutStreamD1Ev,@function
_ZN13CStdOutStreamD1Ev = _ZN13CStdOutStreamD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
