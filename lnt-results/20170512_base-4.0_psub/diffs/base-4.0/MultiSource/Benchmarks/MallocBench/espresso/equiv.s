	.text
	.file	"equiv.bc"
	.globl	find_equiv_outputs
	.p2align	4, 0x90
	.type	find_equiv_outputs,@function
find_equiv_outputs:                     # @find_equiv_outputs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	xorl	%eax, %eax
	callq	makeup_labels
	movq	cube+32(%rip), %rax
	movslq	cube+124(%rip), %rbp
	movslq	(%rax,%rbp,4), %r14
	leaq	(,%r14,8), %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r15
	testq	%r14, %r14
	jle	.LBB0_21
# BB#1:                                 # %.lr.ph99
	xorl	%ebx, %ebx
	movq	8(%rsp), %r14           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movq	cube+16(%rip), %rax
	movl	(%rax,%rbp,4), %esi
	addl	%ebx, %esi
	movq	16(%r12), %rdi
	xorl	%eax, %eax
	callq	cof_output
	movq	%rax, %rcx
	movq	%rcx, (%r15,%rbx,8)
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	cube1list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	complement
	movq	%rax, (%r14,%rbx,8)
	incq	%rbx
	movq	cube+32(%rip), %rax
	movl	cube+124(%rip), %edi
	movslq	%edi, %rbp
	movslq	(%rax,%rbp,4), %rdx
	cmpq	%rdx, %rbx
	jl	.LBB0_2
# BB#3:                                 # %.preheader86
	cmpl	$2, %edx
	jl	.LBB0_21
# BB#4:                                 # %.lr.ph94
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movl	$1, %ebp
	xorl	%ecx, %ecx
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movq	%r12, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_8 Depth 2
	movq	%rcx, %rbx
	leaq	1(%rbx), %rcx
	movslq	%edx, %rsi
	cmpq	%rsi, %rcx
	jge	.LBB0_5
# BB#7:                                 # %.lr.ph91
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movslq	%edi, %rsi
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%rbx,8), %r15
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	%r15, 64(%rsp)          # 8-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	jmp	.LBB0_8
.LBB0_12:                               #   in Loop: Header=BB0_8 Depth=2
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r15,8), %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	check_equiv
	testl	%eax, %eax
	je	.LBB0_14
# BB#13:                                #   in Loop: Header=BB0_8 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movslq	%r12d, %rcx
	movq	(%rax,%rcx,8), %rcx
	movslq	%r13d, %rdx
	movq	(%rax,%rdx,8), %r8
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	movl	%ebp, %edx
	callq	printf
	movl	$1, 4(%rsp)             # 4-byte Folded Spill
	movq	%r15, %rbx
	jmp	.LBB0_18
.LBB0_14:                               #   in Loop: Header=BB0_8 Depth=2
	movq	%rbx, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	check_equiv
	testl	%eax, %eax
	movq	%r15, %rbx
	je	.LBB0_18
# BB#15:                                #   in Loop: Header=BB0_8 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movslq	%r12d, %rcx
	movq	(%rax,%rcx,8), %rcx
	movslq	%r13d, %rdx
	movq	(%rax,%rdx,8), %r8
	movl	$.L.str.3, %edi
	jmp	.LBB0_16
	.p2align	4, 0x90
.LBB0_8:                                #   Parent Loop BB0_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	cube+16(%rip), %rax
	movl	(%rax,%rsi,4), %r13d
	leal	(%r13,%rbx), %r12d
	addq	%rbp, %r13
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%rbp,8), %r14
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	check_equiv
	testl	%eax, %eax
	je	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movslq	%r12d, %rcx
	movq	(%rax,%rcx,8), %rcx
	movslq	%r13d, %rdx
	movq	(%rax,%rdx,8), %r8
	movl	$.L.str, %edi
.LBB0_16:                               #   in Loop: Header=BB0_8 Depth=2
	xorl	%eax, %eax
	jmp	.LBB0_17
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_8 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbp,8), %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	check_equiv
	testl	%eax, %eax
	je	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_8 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movslq	%r12d, %rcx
	movq	(%rax,%rcx,8), %rcx
	movslq	%r13d, %rdx
	movq	(%rax,%rdx,8), %r8
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	32(%rsp), %rbx          # 8-byte Reload
.LBB0_17:                               #   in Loop: Header=BB0_8 Depth=2
	movl	%ebx, %esi
	movl	%ebp, %edx
	callq	printf
	movl	$1, 4(%rsp)             # 4-byte Folded Spill
.LBB0_18:                               #   in Loop: Header=BB0_8 Depth=2
	incq	%rbp
	movq	cube+32(%rip), %rax
	movl	cube+124(%rip), %edi
	movslq	%edi, %rsi
	movslq	(%rax,%rsi,4), %rdx
	cmpq	%rdx, %rbp
	movq	64(%rsp), %r15          # 8-byte Reload
	jl	.LBB0_8
# BB#19:                                #   in Loop: Header=BB0_6 Depth=1
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
.LBB0_5:                                # %.loopexit
                                        #   in Loop: Header=BB0_6 Depth=1
	leal	-1(%rdx), %esi
	movslq	%esi, %rsi
	incq	%rbp
	cmpq	%rsi, %rcx
	jl	.LBB0_6
# BB#20:                                # %._crit_edge95
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	jne	.LBB0_22
.LBB0_21:                               # %._crit_edge95.thread
	movl	$.Lstr, %edi
	callq	puts
	movq	cube+32(%rip), %rax
	movl	cube+124(%rip), %edi
.LBB0_22:                               # %.preheader
	movslq	%edi, %rcx
	cmpl	$0, (%rax,%rcx,4)
	jle	.LBB0_25
# BB#23:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_24:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%rbx,8), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	(%r15,%rbx,8), %rdi
	xorl	%eax, %eax
	callq	sf_free
	incq	%rbx
	movq	cube+32(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movslq	(%rax,%rcx,4), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_24
	jmp	.LBB0_26
.LBB0_25:                               # %._crit_edge
	movq	8(%rsp), %rbp           # 8-byte Reload
	testq	%rbp, %rbp
	je	.LBB0_27
.LBB0_26:                               # %._crit_edge.thread
	movq	%rbp, %rdi
	callq	free
.LBB0_27:
	testq	%r15, %r15
	je	.LBB0_29
# BB#28:
	movq	%r15, %rdi
	callq	free
.LBB0_29:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	find_equiv_outputs, .Lfunc_end0-find_equiv_outputs
	.cfi_endproc

	.globl	check_equiv
	.p2align	4, 0x90
	.type	check_equiv,@function
check_equiv:                            # @check_equiv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	xorl	%eax, %eax
	callq	cube1list
	movq	%rax, %r12
	movslq	(%r15), %rcx
	movslq	12(%r15), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB1_4
# BB#1:                                 # %.lr.ph42.preheader
	movq	24(%r15), %rbx
	leaq	(%rbx,%rax,4), %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph42
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	cube_is_covered
	testl	%eax, %eax
	je	.LBB1_13
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	movslq	(%r15), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r13, %rbx
	jb	.LBB1_2
.LBB1_4:                                # %._crit_edge43
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB1_6
# BB#5:
	callq	free
.LBB1_6:
	movq	%r12, %rdi
	callq	free
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	cube1list
	movq	%rax, %r15
	movslq	(%r14), %rcx
	movslq	12(%r14), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB1_10
# BB#7:                                 # %.lr.ph.preheader
	movq	24(%r14), %rbx
	leaq	(%rbx,%rax,4), %r12
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	cube_is_covered
	testl	%eax, %eax
	je	.LBB1_13
# BB#9:                                 #   in Loop: Header=BB1_8 Depth=1
	movslq	(%r14), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%r12, %rbx
	jb	.LBB1_8
.LBB1_10:                               # %._crit_edge
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB1_12
# BB#11:
	callq	free
.LBB1_12:
	movq	%r15, %rdi
	callq	free
	movl	$1, %ebp
.LBB1_13:                               # %.loopexit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	check_equiv, .Lfunc_end1-check_equiv
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"# Outputs %d and %d (%s and %s) are equivalent\n"
	.size	.L.str, 48

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"# Outputs %d and NOT %d (%s and %s) are equivalent\n"
	.size	.L.str.1, 52

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"# Outputs NOT %d and %d (%s and %s) are equivalent\n"
	.size	.L.str.2, 52

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"# Outputs NOT %d and NOT %d (%s and %s) are equivalent\n"
	.size	.L.str.3, 56

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"# No outputs are equivalent"
	.size	.Lstr, 28


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
