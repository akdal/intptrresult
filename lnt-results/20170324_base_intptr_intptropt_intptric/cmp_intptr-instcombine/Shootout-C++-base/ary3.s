	.text
	.file	"ary3.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	2                       # 0x2
	.quad	3                       # 0x3
.LCPI0_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI0_2:
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
.LCPI0_3:
	.quad	8                       # 0x8
	.quad	8                       # 0x8
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	cmpl	$2, %edi
	jne	.LBB0_1
# BB#7:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r15
	testl	%r15d, %r15d
	js	.LBB0_9
# BB#8:
	movslq	%r15d, %r13
	jmp	.LBB0_2
.LBB0_1:
	movl	$1500000, %r15d         # imm = 0x16E360
	movl	$1500000, %r13d         # imm = 0x16E360
.LBB0_2:                                # %_ZN9__gnu_cxx14__alloc_traitsISaIiEE8allocateERS1_m.exit.i.i.i.i
	leaq	(,%r13,4), %r12
.Ltmp0:
	movq	%r12, %rdi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp1:
# BB#3:                                 # %_ZN9__gnu_cxx14__alloc_traitsISaIiEE8allocateERS1_m.exit.i.i.i.i36
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	memset
.Ltmp2:
	movq	%r12, %rdi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp3:
# BB#4:                                 # %.lr.ph97.preheader
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	callq	memset
	movl	%r15d, %eax
	cmpq	$7, %rax
	jbe	.LBB0_14
# BB#5:                                 # %min.iters.checked
	andl	$7, %r15d
	movq	%rax, %rbp
	subq	%r15, %rbp
	je	.LBB0_6
# BB#11:                                # %vector.body.preheader
	leaq	16(%r14), %rcx
	movl	$1, %edx
	movd	%rdx, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI0_0(%rip), %xmm1   # xmm1 = [2,3]
	movdqa	.LCPI0_1(%rip), %xmm2   # xmm2 = [1,1,1,1]
	movdqa	.LCPI0_2(%rip), %xmm3   # xmm3 = [5,5,5,5]
	movdqa	.LCPI0_3(%rip), %xmm4   # xmm4 = [8,8]
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB0_12:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm5
	shufps	$136, %xmm1, %xmm5      # xmm5 = xmm5[0,2],xmm1[0,2]
	movaps	%xmm5, %xmm6
	paddd	%xmm2, %xmm6
	paddd	%xmm3, %xmm5
	movdqu	%xmm6, -16(%rcx)
	movdqu	%xmm5, (%rcx)
	paddq	%xmm4, %xmm0
	paddq	%xmm4, %xmm1
	addq	$32, %rcx
	addq	$-8, %rdx
	jne	.LBB0_12
# BB#13:                                # %middle.block
	testq	%r15, %r15
	jne	.LBB0_14
	jmp	.LBB0_16
.LBB0_6:
	xorl	%ebp, %ebp
.LBB0_14:                               # %.lr.ph97.preheader132
	leaq	(%r14,%rbp,4), %rcx
	subq	%rbp, %rax
	incl	%ebp
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph97
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, (%rcx)
	addq	$4, %rcx
	incl	%ebp
	decq	%rax
	jne	.LBB0_15
.LBB0_16:                               # %.preheader.us.preheader
	movq	%r13, %rax
	notq	%rax
	cmpq	$-3, %rax
	movq	$-2, %rdx
	cmovgq	%rax, %rdx
	leaq	2(%r13,%rdx), %rcx
	leaq	-6(%r13,%rdx), %r11
	shrq	$3, %r11
	movq	%rcx, %r15
	andq	$-8, %r15
	movq	%r13, %r8
	subq	%r15, %r8
	leaq	-16(%rbx,%r13,4), %r9
	leaq	-16(%r14,%r13,4), %r10
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_17:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_24 Depth 2
                                        #     Child Loop BB0_27 Depth 2
	cmpq	$8, %rcx
	movq	%r13, %rax
	jb	.LBB0_26
# BB#18:                                # %min.iters.checked114
                                        #   in Loop: Header=BB0_17 Depth=1
	testq	%r15, %r15
	movq	%r13, %rax
	je	.LBB0_26
# BB#19:                                # %vector.body110.preheader
                                        #   in Loop: Header=BB0_17 Depth=1
	testb	$1, %r11b
	jne	.LBB0_20
# BB#21:                                # %vector.body110.prol
                                        #   in Loop: Header=BB0_17 Depth=1
	movdqu	-16(%r14,%r13,4), %xmm0
	movdqu	-32(%r14,%r13,4), %xmm1
	movdqu	-16(%rbx,%r13,4), %xmm2
	movdqu	-32(%rbx,%r13,4), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rbx,%r13,4)
	movdqu	%xmm3, -32(%rbx,%r13,4)
	movl	$8, %edi
	testq	%r11, %r11
	jne	.LBB0_23
	jmp	.LBB0_25
.LBB0_20:                               #   in Loop: Header=BB0_17 Depth=1
	xorl	%edi, %edi
	testq	%r11, %r11
	je	.LBB0_25
.LBB0_23:                               # %vector.body110.preheader.new
                                        #   in Loop: Header=BB0_17 Depth=1
	movq	%r15, %rax
	subq	%rdi, %rax
	shlq	$2, %rdi
	movq	%r9, %rdx
	subq	%rdi, %rdx
	movq	%r10, %rbp
	subq	%rdi, %rbp
	.p2align	4, 0x90
.LBB0_24:                               # %vector.body110
                                        #   Parent Loop BB0_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-16(%rbp), %xmm0
	movdqu	(%rbp), %xmm1
	movdqu	-16(%rdx), %xmm2
	movdqu	(%rdx), %xmm3
	paddd	%xmm1, %xmm3
	paddd	%xmm0, %xmm2
	movdqu	%xmm3, (%rdx)
	movdqu	%xmm2, -16(%rdx)
	movdqu	-48(%rbp), %xmm0
	movdqu	-32(%rbp), %xmm1
	movdqu	-48(%rdx), %xmm2
	movdqu	-32(%rdx), %xmm3
	paddd	%xmm1, %xmm3
	paddd	%xmm0, %xmm2
	movdqu	%xmm3, -32(%rdx)
	movdqu	%xmm2, -48(%rdx)
	addq	$-64, %rdx
	addq	$-64, %rbp
	addq	$-16, %rax
	jne	.LBB0_24
.LBB0_25:                               # %middle.block111
                                        #   in Loop: Header=BB0_17 Depth=1
	cmpq	%r15, %rcx
	movq	%r8, %rax
	je	.LBB0_28
	.p2align	4, 0x90
.LBB0_26:                               # %scalar.ph112.preheader
                                        #   in Loop: Header=BB0_17 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB0_27:                               # %scalar.ph112
                                        #   Parent Loop BB0_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-8(%r14,%rax,4), %edx
	addl	%edx, -8(%rbx,%rax,4)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB0_27
.LBB0_28:                               # %._crit_edge.us
                                        #   in Loop: Header=BB0_17 Depth=1
	incl	%esi
	cmpl	$1000, %esi             # imm = 0x3E8
	jne	.LBB0_17
# BB#29:                                # %.us-lcssa.us
	movl	(%rbx), %esi
.Ltmp5:
	movl	$_ZSt4cout, %edi
	callq	_ZNSolsEi
	movq	%rax, %r15
.Ltmp6:
# BB#30:
.Ltmp7:
	movl	$.L.str, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp8:
# BB#31:
	movl	-4(%rbx,%r13,4), %esi
.Ltmp9:
	movq	%r15, %rdi
	callq	_ZNSolsEi
	movq	%rax, %r15
.Ltmp10:
# BB#32:
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %r12
	testq	%r12, %r12
	je	.LBB0_33
# BB#37:                                # %.noexc53
	cmpb	$0, 56(%r12)
	je	.LBB0_39
# BB#38:
	movb	67(%r12), %al
	jmp	.LBB0_41
.LBB0_39:
.Ltmp11:
	movq	%r12, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp12:
# BB#40:                                # %.noexc55
	movq	(%r12), %rax
.Ltmp13:
	movl	$10, %esi
	movq	%r12, %rdi
	callq	*48(%rax)
.Ltmp14:
.LBB0_41:                               # %.noexc48
.Ltmp15:
	movsbl	%al, %esi
	movq	%r15, %rdi
	callq	_ZNSo3putEc
.Ltmp16:
# BB#42:                                # %.noexc49
.Ltmp17:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp18:
# BB#43:                                # %_ZNSt6vectorIiSaIiEED2Ev.exit45
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_ZdlPv
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_33:
.Ltmp19:
	callq	_ZSt16__throw_bad_castv
.Ltmp20:
# BB#36:                                # %.noexc58
.LBB0_9:                                # %.noexc.i.i
.Ltmp22:
	callq	_ZSt17__throw_bad_allocv
.Ltmp23:
# BB#10:                                # %.noexc
.LBB0_35:                               # %.thread
.Ltmp4:
	movq	%rax, %r15
	jmp	.LBB0_45
.LBB0_34:
.Ltmp24:
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB0_44:
.Ltmp21:
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB0_45:
	movq	%r14, %rdi
	callq	_ZdlPv
	movq	%r15, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp24-.Lfunc_begin0   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp2-.Ltmp1           #   Call between .Ltmp1 and .Ltmp2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp2-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp2           #   Call between .Ltmp2 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp5-.Ltmp3           #   Call between .Ltmp3 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp20-.Ltmp5          #   Call between .Ltmp5 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin0   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin0   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Lfunc_end0-.Ltmp23     #   Call between .Ltmp23 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_ary3.ii,@function
_GLOBAL__sub_I_ary3.ii:                 # @_GLOBAL__sub_I_ary3.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end1:
	.size	_GLOBAL__sub_I_ary3.ii, .Lfunc_end1-_GLOBAL__sub_I_ary3.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" "
	.size	.L.str, 2

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_ary3.ii

	.ident	"clang version 5.0.0 (https://github.com/aqjune/clang-intptr.git 8e2e88f063bfae95e3be980dbf26473d5a086d27) (https://github.com/aqjune/llvm-intptr.git 51d41dcad277119de970b737044ce4a7e91ad33d)"
	.section	".note.GNU-stack","",@progbits
