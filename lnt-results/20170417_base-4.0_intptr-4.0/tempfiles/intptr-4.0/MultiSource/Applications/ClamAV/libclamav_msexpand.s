	.text
	.file	"libclamav_msexpand.bc"
	.globl	cli_msexpand
	.p2align	4, 0x90
	.type	cli_msexpand,@function
cli_msexpand:                           # @cli_msexpand
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	leaq	36(%rsp), %rdi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	movl	$-1, %r13d
	cmpq	$1, %rax
	jne	.LBB0_32
# BB#1:
	movl	36(%rsp), %eax
	cmpl	$1245796171, %eax       # imm = 0x4A41574B
	je	.LBB0_24
# BB#2:
	cmpl	$1145330259, %eax       # imm = 0x44445A53
	jne	.LBB0_30
# BB#3:
	leaq	8(%rsp), %rdi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB0_32
# BB#4:
	leaq	14(%rsp), %rdi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB0_32
# BB#5:
	leaq	52(%rsp), %rdi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB0_32
# BB#6:
	cmpl	$858255496, 8(%rsp)     # imm = 0x3327F088
	jne	.LBB0_30
# BB#7:
	movl	$4096, %edi             # imm = 0x1000
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_34
# BB#8:                                 # %.preheader59
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	fgetc
	movl	%eax, 24(%rsp)          # 4-byte Spill
	cmpl	$-1, %eax
	je	.LBB0_23
# BB#9:                                 # %.preheader.preheader
	movl	$4080, %ebp             # imm = 0xFF0
.LBB0_10:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_11 Depth 2
                                        #       Child Loop BB0_17 Depth 3
	movl	$1, %r14d
	movl	%ebp, %r12d
.LBB0_11:                               #   Parent Loop BB0_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_17 Depth 3
	movq	%rbx, %rdi
	callq	fgetc
	movl	%eax, %ebp
	testl	24(%rsp), %r14d         # 4-byte Folded Reload
	movl	%r14d, 28(%rsp)         # 4-byte Spill
	je	.LBB0_15
# BB#12:                                #   in Loop: Header=BB0_11 Depth=2
	cmpl	$-1, %ebp
	je	.LBB0_21
# BB#13:                                #   in Loop: Header=BB0_11 Depth=2
	movslq	%r12d, %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax), %rdi
	movb	%bpl, (%rcx,%rax)
	movl	$1, %esi
	movl	$1, %edx
	movq	40(%rsp), %rcx          # 8-byte Reload
	callq	fwrite
	cmpq	$1, %rax
	jne	.LBB0_33
# BB#14:                                #   in Loop: Header=BB0_11 Depth=2
	leal	1(%r12), %ebp
	movl	%ebp, %eax
	sarl	$31, %eax
	shrl	$20, %eax
	leal	1(%r12,%rax), %eax
	andl	$-4096, %eax            # imm = 0xF000
	subl	%eax, %ebp
	jmp	.LBB0_19
.LBB0_15:                               #   in Loop: Header=BB0_11 Depth=2
	cmpl	$-1, %ebp
	je	.LBB0_21
# BB#16:                                #   in Loop: Header=BB0_11 Depth=2
	movq	%rbx, %rdi
	callq	fgetc
	movl	%eax, %r14d
	shll	$4, %r14d
	andl	$3840, %r14d            # imm = 0xF00
	addl	%ebp, %r14d
	andl	$15, %eax
	movl	$-3, %r15d
	subl	%eax, %r15d
.LBB0_17:                               #   Parent Loop BB0_10 Depth=1
                                        #     Parent Loop BB0_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%r14d, %rax
	movq	16(%rsp), %rdx          # 8-byte Reload
	movzbl	(%rdx,%rax), %eax
	movslq	%r12d, %rcx
	leaq	(%rdx,%rcx), %rdi
	movb	%al, (%rdx,%rcx)
	movl	$1, %esi
	movl	$1, %edx
	movq	40(%rsp), %rcx          # 8-byte Reload
	callq	fwrite
	cmpq	$1, %rax
	jne	.LBB0_33
# BB#18:                                #   in Loop: Header=BB0_17 Depth=3
	leal	1(%r14), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$20, %ecx
	leal	1(%r14,%rcx), %ecx
	andl	$-4096, %ecx            # imm = 0xF000
	subl	%ecx, %eax
	leal	1(%r12), %ebp
	movl	%ebp, %ecx
	sarl	$31, %ecx
	shrl	$20, %ecx
	leal	1(%r12,%rcx), %ecx
	andl	$-4096, %ecx            # imm = 0xF000
	subl	%ecx, %ebp
	incl	%r15d
	movl	%eax, %r14d
	movl	%ebp, %r12d
	jne	.LBB0_17
.LBB0_19:                               # %.loopexit
                                        #   in Loop: Header=BB0_11 Depth=2
	movl	28(%rsp), %r14d         # 4-byte Reload
	addl	%r14d, %r14d
	testb	$-2, %r14b
	movl	%ebp, %r12d
	jne	.LBB0_11
	jmp	.LBB0_22
.LBB0_21:                               #   in Loop: Header=BB0_10 Depth=1
	movl	%r12d, %ebp
.LBB0_22:                               # %.loopexit58
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	%rbx, %rdi
	callq	fgetc
	movl	%eax, 24(%rsp)          # 4-byte Spill
	cmpl	$-1, %eax
	jne	.LBB0_10
.LBB0_23:                               # %._crit_edge
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	xorl	%r13d, %r13d
	jmp	.LBB0_32
.LBB0_24:
	leaq	8(%rsp), %rdi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB0_32
# BB#25:
	leaq	32(%rsp), %rdi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB0_32
# BB#26:
	leaq	14(%rsp), %rdi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpq	$1, %rax
	jne	.LBB0_32
# BB#27:
	cmpl	$-785911672, 8(%rsp)    # imm = 0xD127F088
	jne	.LBB0_30
# BB#28:
	cmpl	$1179651, 32(%rsp)      # imm = 0x120003
	jne	.LBB0_30
# BB#29:
	movl	$.L.str.1, %edi
	jmp	.LBB0_31
.LBB0_30:
	movl	$.L.str, %edi
.LBB0_31:
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB0_32:
	movl	%r13d, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_33:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	jmp	.LBB0_32
.LBB0_34:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB0_32
.Lfunc_end0:
	.size	cli_msexpand, .Lfunc_end0-cli_msexpand
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"msexpand: Not a MS-compressed file\n"
	.size	.L.str, 36

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"msexpand: unsupported version 6.22\n"
	.size	.L.str.1, 36

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"msexpand: Can't allocate memory\n"
	.size	.L.str.2, 33


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
