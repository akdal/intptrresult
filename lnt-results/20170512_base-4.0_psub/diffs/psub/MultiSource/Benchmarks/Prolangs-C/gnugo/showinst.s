	.text
	.file	"showinst.bc"
	.globl	showinst
	.p2align	4, 0x90
	.type	showinst,@function
showinst:                               # @showinst
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr, %edi
	callq	puts
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.13, %edi
	callq	puts
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.14, %edi
	callq	puts
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.13, %edi
	callq	puts
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.14, %edi
	callq	puts
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.13, %edi
	callq	puts
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.14, %edi
	callq	puts
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.13, %edi
	callq	puts
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.14, %edi
	callq	puts
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.13, %edi
	callq	puts
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.14, %edi
	callq	puts
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.13, %edi
	callq	puts
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.14, %edi
	callq	puts
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.13, %edi
	callq	puts
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.14, %edi
	callq	puts
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.15, %edi
	callq	puts
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	printf
	movq	stdin(%rip), %rdi
	callq	_IO_getc
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.16, %edi
	popq	%rax
	jmp	puts                    # TAILCALL
.Lfunc_end0:
	.size	showinst, .Lfunc_end0-showinst
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOX"
	.size	.L.str, 68

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"O                                                                  "
	.size	.L.str.2, 68

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"X                           GNU GO (Previously Hugo)               "
	.size	.L.str.4, 68

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"O                           the game of Go (Wei-Chi)               "
	.size	.L.str.6, 68

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"X                                                                  "
	.size	.L.str.7, 68

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"O                            version 1.1    3-1-89                 "
	.size	.L.str.8, 68

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"X              Copyright (C) 1989 Free Software Foundation, Inc.   "
	.size	.L.str.9, 68

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"O                              Author: Man L. Li                   "
	.size	.L.str.10, 68

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"X         GNU GO comes with ABSOLUTELY NO WARRANTY; see COPYING for"
	.size	.L.str.11, 68

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"O         detail.   This is free software, and you are welcome to  "
	.size	.L.str.12, 68

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"X         redistribute it; see COPYING for copying conditions.     "
	.size	.L.str.13, 68

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"X              Please report all bugs, modifications, suggestions  "
	.size	.L.str.14, 68

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"O                         to manli@cs.uh.edu  (Internet)           "
	.size	.L.str.15, 68

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"OXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO"
	.size	.L.str.16, 68

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"\n\n\n\n\n\n\n\nPress return to continue"
	.size	.L.str.18, 33

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"\n\nTo play this game first select number of handicap pieces (0 to"
	.size	.L.str.19, 65

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	" 17) for the\nblack side.  Next choose your color (black or white)."
	.size	.L.str.20, 67

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"  To place your piece,\nenter your move as coordinate on the board"
	.size	.L.str.21, 66

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	" in column and row.  The column\nis from 'A' to 'T'(excluding 'I')."
	.size	.L.str.22, 67

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"  The row is from 1 to 19.\n\nTo pass your move enter 'pass' for"
	.size	.L.str.23, 63

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	" your turn.  After both you and the computer\npassed the game will"
	.size	.L.str.24, 66

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	" end.  To save the board and exit enter 'save'.  The game\nwill"
	.size	.L.str.25, 63

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	" continue the next time you start the program.  To stop the game in"
	.size	.L.str.26, 68

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	" the\nmiddle of play enter 'stop' for your move.  You will be"
	.size	.L.str.27, 61

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	" asked whether you want\nto count the result of the game.  If you"
	.size	.L.str.28, 65

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	" answer 'y' then you need to remove the\nremaining dead pieces and"
	.size	.L.str.29, 66

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	" fill up neutral turf on the board as instructed.\nFinally, the"
	.size	.L.str.30, 63

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"OXOXOXOXOXOX"
	.size	.Lstr, 13

	.type	.Lstr.13,@object        # @str.13
.Lstr.13:
	.asciz	"           O"
	.size	.Lstr.13, 13

	.type	.Lstr.14,@object        # @str.14
.Lstr.14:
	.asciz	"           X"
	.size	.Lstr.14, 13

	.type	.Lstr.15,@object        # @str.15
.Lstr.15:
	.asciz	"XOXOXOXOXOXO"
	.size	.Lstr.15, 13

	.type	.Lstr.16,@object        # @str.16
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.16:
	.asciz	" computer will count all pieces for both side and show the result.\n"
	.size	.Lstr.16, 68


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
