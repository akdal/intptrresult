	.text
	.file	"misc.bc"
	.globl	misc_ErrorReport
	.p2align	4, 0x90
	.type	misc_ErrorReport,@function
misc_ErrorReport:                       # @misc_ErrorReport
	.cfi_startproc
# BB#0:
	subq	$216, %rsp
.Lcfi0:
	.cfi_def_cfa_offset 224
	movq	%rdi, %r10
	testb	%al, %al
	je	.LBB0_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB0_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rsi, 40(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	224(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$8, (%rsp)
	movq	stderr(%rip), %rdi
	movq	%rsp, %rdx
	movq	%r10, %rsi
	callq	vfprintf
	addq	$216, %rsp
	retq
.Lfunc_end0:
	.size	misc_ErrorReport, .Lfunc_end0-misc_ErrorReport
	.cfi_endproc

	.globl	misc_UserErrorReport
	.p2align	4, 0x90
	.type	misc_UserErrorReport,@function
misc_UserErrorReport:                   # @misc_UserErrorReport
	.cfi_startproc
# BB#0:
	subq	$216, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 224
	movq	%rdi, %r10
	testb	%al, %al
	je	.LBB1_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB1_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rsi, 40(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	224(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$8, (%rsp)
	movq	stderr(%rip), %rdi
	movq	%rsp, %rdx
	movq	%r10, %rsi
	callq	vfprintf
	addq	$216, %rsp
	retq
.Lfunc_end1:
	.size	misc_UserErrorReport, .Lfunc_end1-misc_UserErrorReport
	.cfi_endproc

	.globl	misc_DumpCoreOut
	.p2align	4, 0x90
	.type	misc_DumpCoreOut,@function
misc_DumpCoreOut:                       # @misc_DumpCoreOut
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rcx
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	callq	abort
.Lfunc_end2:
	.size	misc_DumpCoreOut, .Lfunc_end2-misc_DumpCoreOut
	.cfi_endproc

	.globl	misc_ReturnValue
	.p2align	4, 0x90
	.type	misc_ReturnValue,@function
misc_ReturnValue:                       # @misc_ReturnValue
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	misc_ReturnValue, .Lfunc_end3-misc_ReturnValue
	.cfi_endproc

	.globl	misc_Max
	.p2align	4, 0x90
	.type	misc_Max,@function
misc_Max:                               # @misc_Max
	.cfi_startproc
# BB#0:
	cmpl	%esi, %edi
	cmovgel	%edi, %esi
	movl	%esi, %eax
	retq
.Lfunc_end4:
	.size	misc_Max, .Lfunc_end4-misc_Max
	.cfi_endproc

	.globl	misc_OpenFile
	.p2align	4, 0x90
	.type	misc_OpenFile,@function
misc_OpenFile:                          # @misc_OpenFile
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 32
.Lcfi6:
	.cfi_offset %rbx, -24
.Lcfi7:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	fopen
	testq	%rax, %rax
	je	.LBB5_2
# BB#1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB5_2:
	movq	stdout(%rip), %rdi
	callq	fflush
	movb	(%rbx), %al
	cmpb	$119, %al
	movl	$.L.str.3, %ecx
	movl	$.L.str.4, %esi
	cmoveq	%rcx, %rsi
	cmpb	$114, %al
	movl	$.L.str.2, %edx
	cmovneq	%rsi, %rdx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end5:
	.size	misc_OpenFile, .Lfunc_end5-misc_OpenFile
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_Error,@function
misc_Error:                             # @misc_Error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	misc_Error, .Lfunc_end6-misc_Error
	.cfi_endproc

	.globl	misc_CloseFile
	.p2align	4, 0x90
	.type	misc_CloseFile,@function
misc_CloseFile:                         # @misc_CloseFile
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	callq	fclose
	testl	%eax, %eax
	jne	.LBB7_2
# BB#1:
	popq	%rbx
	retq
.LBB7_2:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end7:
	.size	misc_CloseFile, .Lfunc_end7-misc_CloseFile
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n %s \n"
	.size	.L.str, 7

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\n\tError in opening file %s for %s !\n\n"
	.size	.L.str.1, 38

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"reading"
	.size	.L.str.2, 8

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"writing"
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"i/o operations"
	.size	.L.str.4, 15

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\n\tError in closing file %s !\n\n"
	.size	.L.str.5, 31

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\n\n"
	.size	.L.str.6, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
