	.text
	.file	"LoadCodecs.bc"
	.globl	_Z11RegisterArcPK8CArcInfo
	.p2align	4, 0x90
	.type	_Z11RegisterArcPK8CArcInfo,@function
_Z11RegisterArcPK8CArcInfo:             # @_Z11RegisterArcPK8CArcInfo
	.cfi_startproc
# BB#0:
	movl	_ZL9g_NumArcs(%rip), %eax
	cmpq	$47, %rax
	ja	.LBB0_2
# BB#1:
	leal	1(%rax), %ecx
	movl	%ecx, _ZL9g_NumArcs(%rip)
	movq	%rdi, _ZL6g_Arcs(,%rax,8)
.LBB0_2:
	retq
.Lfunc_end0:
	.size	_Z11RegisterArcPK8CArcInfo, .Lfunc_end0-_Z11RegisterArcPK8CArcInfo
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.zero	16
	.text
	.globl	_ZN10CArcInfoEx7AddExtsEPKwS1_
	.p2align	4, 0x90
	.type	_ZN10CArcInfoEx7AddExtsEPKwS1_,@function
_ZN10CArcInfoEx7AddExtsEPKwS1_:         # @_ZN10CArcInfoEx7AddExtsEPKwS1_
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 160
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	xorps	%xmm0, %xmm0
	movups	%xmm0, 72(%rsp)
	movq	$8, 88(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 64(%rsp)
	movups	%xmm0, 40(%rsp)
	movq	$8, 56(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 32(%rsp)
	testq	%r12, %r12
	je	.LBB1_8
# BB#1:
	movaps	%xmm0, (%rsp)
	movl	$-1, %ebp
	movq	%r12, %rax
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB1_2
# BB#3:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%rbp), %r14d
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp0:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp1:
# BB#4:                                 # %.noexc
	movq	%rbx, (%rsp)
	movl	$0, (%rbx)
	movl	%r14d, 12(%rsp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_5:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB1_5
# BB#6:
	movl	%ebp, 8(%rsp)
.Ltmp3:
	movq	%rsp, %rdi
	leaq	64(%rsp), %rsi
	callq	_ZL11SplitStringRK11CStringBaseIwER13CObjectVectorIS0_E
.Ltmp4:
# BB#7:                                 # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB1_8:
	testq	%r15, %r15
	je	.LBB1_16
# BB#9:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %ebp
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB1_10:                               # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB1_10
# BB#11:                                # %_Z11MyStringLenIwEiPKT_.exit.i24
	leal	1(%rbp), %r14d
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp6:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp7:
# BB#12:                                # %.noexc28
	movq	%rbx, (%rsp)
	movl	$0, (%rbx)
	movl	%r14d, 12(%rsp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_13:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i27
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB1_13
# BB#14:
	movl	%ebp, 8(%rsp)
.Ltmp9:
	movq	%rsp, %rdi
	leaq	32(%rsp), %rsi
	callq	_ZL11SplitStringRK11CStringBaseIwER13CObjectVectorIS0_E
.Ltmp10:
# BB#15:                                # %_ZN11CStringBaseIwED2Ev.exit30
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB1_16:                               # %.preheader
	cmpl	$0, 76(%rsp)
	jle	.LBB1_49
# BB#17:                                # %.lr.ph
	addq	$40, 96(%rsp)           # 8-byte Folded Spill
	xorl	%r14d, %r14d
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB1_18:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_25 Depth 2
                                        #     Child Loop BB1_38 Depth 2
	movaps	%xmm0, (%rsp)
.Ltmp12:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp13:
# BB#19:                                # %.noexc36
                                        #   in Loop: Header=BB1_18 Depth=1
	movq	%rbx, (%rsp)
	movl	$0, (%rbx)
	movl	$4, 12(%rsp)
	leaq	16(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
.Ltmp15:
	movl	$16, %edi
	callq	_Znam
.Ltmp16:
# BB#20:                                #   in Loop: Header=BB1_18 Depth=1
	movq	%rax, 16(%rsp)
	movl	$0, (%rax)
	movl	$4, 28(%rsp)
	movq	80(%rsp), %rax
	movq	(%rax,%r14,8), %rbp
	movq	%rsp, %rax
	cmpq	%rax, %rbp
	je	.LBB1_27
# BB#21:                                #   in Loop: Header=BB1_18 Depth=1
	movl	$0, 8(%rsp)
	movl	$0, (%rbx)
	movslq	8(%rbp), %r12
	incq	%r12
	cmpl	$4, %r12d
	je	.LBB1_24
# BB#22:                                #   in Loop: Header=BB1_18 Depth=1
	movq	%r12, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp18:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
.Ltmp19:
# BB#23:                                # %._crit_edge16.i.i
                                        #   in Loop: Header=BB1_18 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%rsp), %rax
	movq	%r13, (%rsp)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 12(%rsp)
	movq	%r13, %rbx
.LBB1_24:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i39
                                        #   in Loop: Header=BB1_18 Depth=1
	movq	(%rbp), %rax
	.p2align	4, 0x90
.LBB1_25:                               #   Parent Loop BB1_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB1_25
# BB#26:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
                                        #   in Loop: Header=BB1_18 Depth=1
	movl	8(%rbp), %eax
	movl	%eax, 8(%rsp)
.LBB1_27:                               # %_ZN11CStringBaseIwEaSERKS0_.exit
                                        #   in Loop: Header=BB1_18 Depth=1
	movslq	44(%rsp), %rax
	cmpq	%rax, %r14
	jge	.LBB1_43
# BB#28:                                #   in Loop: Header=BB1_18 Depth=1
	movq	48(%rsp), %rax
	movq	(%rax,%r14,8), %rbp
	leaq	16(%rsp), %rax
	cmpq	%rax, %rbp
	je	.LBB1_34
# BB#29:                                #   in Loop: Header=BB1_18 Depth=1
	movl	$0, 24(%rsp)
	movq	16(%rsp), %r12
	movl	$0, (%r12)
	movslq	8(%rbp), %rbx
	incq	%rbx
	movl	28(%rsp), %r15d
	cmpl	%r15d, %ebx
	je	.LBB1_37
# BB#30:                                #   in Loop: Header=BB1_18 Depth=1
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp20:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
.Ltmp21:
# BB#31:                                # %.noexc52
                                        #   in Loop: Header=BB1_18 Depth=1
	testq	%r12, %r12
	je	.LBB1_35
# BB#32:                                # %.noexc52
                                        #   in Loop: Header=BB1_18 Depth=1
	testl	%r15d, %r15d
	movl	$0, %eax
	jle	.LBB1_36
# BB#33:                                # %._crit_edge.thread.i.i46
                                        #   in Loop: Header=BB1_18 Depth=1
	movq	%r12, %rdi
	callq	_ZdaPv
	movslq	24(%rsp), %rax
	jmp	.LBB1_36
.LBB1_34:                               # %._ZN11CStringBaseIwEaSERKS0_.exit53_crit_edge
                                        #   in Loop: Header=BB1_18 Depth=1
	movq	16(%rsp), %r12
	jmp	.LBB1_40
.LBB1_35:                               #   in Loop: Header=BB1_18 Depth=1
	xorl	%eax, %eax
.LBB1_36:                               # %._crit_edge16.i.i47
                                        #   in Loop: Header=BB1_18 Depth=1
	movq	%r13, 16(%rsp)
	movl	$0, (%r13,%rax,4)
	movl	%ebx, 28(%rsp)
	movq	%r13, %r12
.LBB1_37:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i48
                                        #   in Loop: Header=BB1_18 Depth=1
	movq	(%rbp), %rax
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB1_38:                               #   Parent Loop BB1_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB1_38
# BB#39:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i51
                                        #   in Loop: Header=BB1_18 Depth=1
	movl	8(%rbp), %eax
	movl	%eax, 24(%rsp)
.LBB1_40:                               # %_ZN11CStringBaseIwEaSERKS0_.exit53
                                        #   in Loop: Header=BB1_18 Depth=1
.Ltmp22:
	movl	$.L.str, %esi
	movq	%r12, %rdi
	callq	_Z15MyStringComparePKwS0_
.Ltmp23:
# BB#41:                                #   in Loop: Header=BB1_18 Depth=1
	testl	%eax, %eax
	jne	.LBB1_43
# BB#42:                                #   in Loop: Header=BB1_18 Depth=1
	movl	$0, 24(%rsp)
	movq	16(%rsp), %rax
	movl	$0, (%rax)
	.p2align	4, 0x90
.LBB1_43:                               #   in Loop: Header=BB1_18 Depth=1
.Ltmp24:
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	%rsp, %rsi
	callq	_ZN13CObjectVectorI11CArcExtInfoE3AddERKS0_
.Ltmp25:
# BB#44:                                #   in Loop: Header=BB1_18 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_46
# BB#45:                                #   in Loop: Header=BB1_18 Depth=1
	callq	_ZdaPv
.LBB1_46:                               # %_ZN11CStringBaseIwED2Ev.exit.i56
                                        #   in Loop: Header=BB1_18 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_48
# BB#47:                                #   in Loop: Header=BB1_18 Depth=1
	callq	_ZdaPv
.LBB1_48:                               # %_ZN11CArcExtInfoD2Ev.exit57
                                        #   in Loop: Header=BB1_18 Depth=1
	incq	%r14
	movslq	76(%rsp), %rax
	cmpq	%rax, %r14
	xorps	%xmm0, %xmm0
	jl	.LBB1_18
.LBB1_49:                               # %._crit_edge
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 32(%rsp)
.Ltmp35:
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp36:
# BB#50:
.Ltmp41:
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp42:
# BB#51:                                # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit35
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 64(%rsp)
.Ltmp53:
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp54:
# BB#52:                                # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit58
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_53:
.Ltmp11:
	jmp	.LBB1_65
.LBB1_54:
.Ltmp8:
	jmp	.LBB1_63
.LBB1_55:
.Ltmp5:
	jmp	.LBB1_65
.LBB1_56:
.Ltmp2:
	jmp	.LBB1_63
.LBB1_57:
.Ltmp55:
	movq	%rax, %r14
.Ltmp56:
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp57:
	jmp	.LBB1_74
.LBB1_58:
.Ltmp58:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_59:
.Ltmp43:
	movq	%rax, %r14
	jmp	.LBB1_72
.LBB1_60:
.Ltmp37:
	movq	%rax, %r14
.Ltmp38:
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp39:
	jmp	.LBB1_72
.LBB1_61:
.Ltmp40:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_62:
.Ltmp14:
.LBB1_63:
	movq	%rax, %r14
	jmp	.LBB1_70
.LBB1_64:                               # %_ZN11CStringBaseIwED2Ev.exit.i
.Ltmp17:
.LBB1_65:
	movq	%rax, %r14
	movq	%rbx, %rdi
	jmp	.LBB1_69
.LBB1_66:
.Ltmp26:
	movq	%rax, %r14
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_68
# BB#67:
	callq	_ZdaPv
.LBB1_68:                               # %_ZN11CStringBaseIwED2Ev.exit.i55
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_70
.LBB1_69:
	callq	_ZdaPv
.LBB1_70:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 32(%rsp)
.Ltmp27:
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp28:
# BB#71:
.Ltmp33:
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp34:
.LBB1_72:                               # %._crit_edge74
	leaq	64(%rsp), %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 64(%rsp)
.Ltmp44:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp45:
# BB#73:
.Ltmp50:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp51:
.LBB1_74:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB1_75:
.Ltmp29:
	movq	%rax, %r14
.Ltmp30:
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp31:
	jmp	.LBB1_80
.LBB1_76:
.Ltmp32:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_77:
.Ltmp46:
	movq	%rax, %r14
.Ltmp47:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp48:
	jmp	.LBB1_80
.LBB1_78:
.Ltmp49:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_79:
.Ltmp52:
	movq	%rax, %r14
.LBB1_80:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end1:
	.size	_ZN10CArcInfoEx7AddExtsEPKwS1_, .Lfunc_end1-_ZN10CArcInfoEx7AddExtsEPKwS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\215\202\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\204\002"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp25-.Ltmp18         #   Call between .Ltmp18 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin0   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin0   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin0   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp54-.Ltmp53         #   Call between .Ltmp53 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin0   #     jumps to .Ltmp55
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp56-.Ltmp54         #   Call between .Ltmp54 and .Ltmp56
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin0   #     jumps to .Ltmp58
	.byte	1                       #   On action: 1
	.long	.Ltmp38-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin0   #     jumps to .Ltmp40
	.byte	1                       #   On action: 1
	.long	.Ltmp27-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin0   #     jumps to .Ltmp29
	.byte	1                       #   On action: 1
	.long	.Ltmp33-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp52-.Lfunc_begin0   #     jumps to .Ltmp52
	.byte	1                       #   On action: 1
	.long	.Ltmp44-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin0   #     jumps to .Ltmp46
	.byte	1                       #   On action: 1
	.long	.Ltmp50-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin0   #     jumps to .Ltmp52
	.byte	1                       #   On action: 1
	.long	.Ltmp51-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp30-.Ltmp51         #   Call between .Ltmp51 and .Ltmp30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin0   #     jumps to .Ltmp32
	.byte	1                       #   On action: 1
	.long	.Ltmp47-.Lfunc_begin0   # >> Call Site 20 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin0   #     jumps to .Ltmp49
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.zero	16
	.text
	.p2align	4, 0x90
	.type	_ZL11SplitStringRK11CStringBaseIwER13CObjectVectorIS0_E,@function
_ZL11SplitStringRK11CStringBaseIwER13CObjectVectorIS0_E: # @_ZL11SplitStringRK11CStringBaseIwER13CObjectVectorIS0_E
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 112
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	$16, %edi
	callq	_Znam
	movl	$0, (%rax)
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movslq	8(%rbx), %rdi
	testq	%rdi, %rdi
	jle	.LBB2_63
# BB#1:                                 # %.lr.ph
	movl	$4, %r13d
	xorl	%r15d, %r15d
	movq	%rax, %r12
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, %rbx
	xorl	%r14d, %r14d
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	jmp	.LBB2_2
.LBB2_33:                               # %vector.body.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB2_34
# BB#35:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_36:                               # %vector.body.prol
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rdi,4), %xmm0
	movups	16(%r12,%rdi,4), %xmm1
	movups	%xmm0, (%rbx,%rdi,4)
	movups	%xmm1, 16(%rbx,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB2_36
	jmp	.LBB2_37
.LBB2_34:                               #   in Loop: Header=BB2_2 Depth=1
	xorl	%edi, %edi
.LBB2_37:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	cmpq	$24, %rdx
	jb	.LBB2_40
# BB#38:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%rbx,%rdi,4), %rsi
	leaq	112(%r12,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB2_39:                               # %vector.body
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB2_39
.LBB2_40:                               # %middle.block
                                        #   in Loop: Header=BB2_2 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB2_24
	jmp	.LBB2_42
	.p2align	4, 0x90
.LBB2_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_36 Depth 2
                                        #     Child Loop BB2_39 Depth 2
                                        #     Child Loop BB2_26 Depth 2
                                        #     Child Loop BB2_29 Depth 2
                                        #     Child Loop BB2_8 Depth 2
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movl	(%rax,%r15,4), %ebp
	cmpl	$32, %ebp
	jne	.LBB2_13
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	testl	%r14d, %r14d
	je	.LBB2_4
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
.Ltmp61:
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp62:
# BB#6:                                 # %.noexc31
                                        #   in Loop: Header=BB2_2 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	leal	1(%r14), %ebp
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp63:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp64:
# BB#7:                                 # %.noexc.i27
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%ebp, 12(%rbx)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_8:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i28
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r12,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB2_8
# BB#9:                                 #   in Loop: Header=BB2_2 Depth=1
	movl	%r14d, 8(%rbx)
.Ltmp66:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp67:
# BB#10:                                #   in Loop: Header=BB2_2 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rdx)
	xorl	%r14d, %r14d
	movq	%r12, %rax
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB2_45
	.p2align	4, 0x90
.LBB2_13:                               #   in Loop: Header=BB2_2 Depth=1
	movl	%r13d, %eax
	subl	%r14d, %eax
	cmpl	$1, %eax
	jle	.LBB2_15
# BB#14:                                #   in Loop: Header=BB2_2 Depth=1
	movl	%r13d, %edx
	jmp	.LBB2_44
	.p2align	4, 0x90
.LBB2_15:                               #   in Loop: Header=BB2_2 Depth=1
	leal	-1(%rax), %ecx
	cmpl	$8, %r13d
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %r13d
	jl	.LBB2_17
# BB#16:                                # %select.true.sink
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	%r13d, %edx
	shrl	$31, %edx
	addl	%r13d, %edx
	sarl	%edx
.LBB2_17:                               # %select.end
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%r13,%rsi), %eax
	cmpl	%r13d, %eax
	jne	.LBB2_19
# BB#18:                                #   in Loop: Header=BB2_2 Depth=1
	movl	%r13d, %edx
	jmp	.LBB2_44
.LBB2_4:                                #   in Loop: Header=BB2_2 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB2_46
.LBB2_19:                               #   in Loop: Header=BB2_2 Depth=1
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movl	%eax, 44(%rsp)          # 4-byte Spill
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp59:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp60:
# BB#20:                                # %.noexc37
                                        #   in Loop: Header=BB2_2 Depth=1
	testl	%r13d, %r13d
	movq	32(%rsp), %rdi          # 8-byte Reload
	jle	.LBB2_43
# BB#21:                                # %.preheader.i.i
                                        #   in Loop: Header=BB2_2 Depth=1
	testl	%r14d, %r14d
	jle	.LBB2_41
# BB#22:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB2_2 Depth=1
	movslq	%r14d, %rax
	cmpl	$7, %r14d
	jbe	.LBB2_23
# BB#30:                                # %min.iters.checked
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB2_23
# BB#31:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_2 Depth=1
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB2_33
# BB#32:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_2 Depth=1
	leaq	(%rbx,%rax,4), %rdx
	cmpq	%rdx, 8(%rsp)           # 8-byte Folded Reload
	jae	.LBB2_33
.LBB2_23:                               #   in Loop: Header=BB2_2 Depth=1
	xorl	%ecx, %ecx
.LBB2_24:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	%r14d, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB2_27
# BB#25:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB2_26:                               # %scalar.ph.prol
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r12,%rcx,4), %edi
	movl	%edi, (%rbx,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB2_26
.LBB2_27:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	cmpq	$7, %rdx
	jb	.LBB2_42
# BB#28:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB2_2 Depth=1
	subq	%rcx, %rax
	leaq	28(%rbx,%rcx,4), %rdx
	leaq	28(%r12,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB2_29:                               # %scalar.ph
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB2_29
	jmp	.LBB2_42
.LBB2_41:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB2_2 Depth=1
	testq	%r12, %r12
	je	.LBB2_43
.LBB2_42:                               # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZdaPv
	movq	32(%rsp), %rdi          # 8-byte Reload
.LBB2_43:                               # %._crit_edge16.i.i
                                        #   in Loop: Header=BB2_2 Depth=1
	movslq	%r14d, %rax
	movl	$0, (%rbx,%rax,4)
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	%rbx, %r12
	movl	44(%rsp), %edx          # 4-byte Reload
.LBB2_44:                               # %_ZN11CStringBaseIwEpLEw.exit
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%r12, %rax
	movslq	%r14d, %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	%r14d
	leaq	4(%rax,%rcx,4), %r12
	movl	%edx, %r13d
.LBB2_45:                               # %.sink.split
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	$0, (%r12)
	movq	%rax, %r12
.LBB2_46:                               #   in Loop: Header=BB2_2 Depth=1
	incq	%r15
	cmpq	%rdi, %r15
	jl	.LBB2_2
# BB#47:                                # %._crit_edge
	testl	%r14d, %r14d
	je	.LBB2_54
# BB#48:
.Ltmp69:
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp70:
# BB#49:                                # %.noexc
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	leal	1(%r14), %r15d
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp71:
	callq	_Znam
.Ltmp72:
# BB#50:                                # %.noexc.i
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%r15d, 12(%rbx)
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB2_51:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB2_51
# BB#52:
	movl	%r14d, 8(%rbx)
.Ltmp74:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp75:
# BB#53:                                # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rdx)
.LBB2_54:
	testq	%r12, %r12
	movq	8(%rsp), %rax           # 8-byte Reload
	je	.LBB2_55
.LBB2_63:                               # %.thread
	movq	%rax, %rdi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZdaPv                  # TAILCALL
.LBB2_55:                               # %_ZN11CStringBaseIwED2Ev.exit24
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_61:
.Ltmp73:
	jmp	.LBB2_12
.LBB2_62:
.Ltmp76:
	jmp	.LBB2_57
.LBB2_11:
.Ltmp65:
.LBB2_12:                               # %.body33
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	testq	%r12, %r12
	jne	.LBB2_59
	jmp	.LBB2_60
.LBB2_56:
.Ltmp68:
.LBB2_57:                               # %.body33
	movq	%rax, %r14
	testq	%r12, %r12
	je	.LBB2_60
.LBB2_59:
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
.LBB2_60:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZL11SplitStringRK11CStringBaseIwER13CObjectVectorIS0_E, .Lfunc_end2-_ZL11SplitStringRK11CStringBaseIwER13CObjectVectorIS0_E
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\352\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp61-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp61
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp68-.Lfunc_begin1   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin1   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp60-.Ltmp66         #   Call between .Ltmp66 and .Ltmp60
	.long	.Ltmp68-.Lfunc_begin1   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp70-.Ltmp69         #   Call between .Ltmp69 and .Ltmp70
	.long	.Ltmp76-.Lfunc_begin1   #     jumps to .Ltmp76
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin1   #     jumps to .Ltmp73
	.byte	0                       #   On action: cleanup
	.long	.Ltmp74-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp75-.Ltmp74         #   Call between .Ltmp74 and .Ltmp75
	.long	.Ltmp76-.Lfunc_begin1   #     jumps to .Ltmp76
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Lfunc_end2-.Ltmp75     #   Call between .Ltmp75 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end3:
	.size	__clang_call_terminate, .Lfunc_end3-__clang_call_terminate

	.section	.text._ZN13CObjectVectorI11CArcExtInfoE3AddERKS0_,"axG",@progbits,_ZN13CObjectVectorI11CArcExtInfoE3AddERKS0_,comdat
	.weak	_ZN13CObjectVectorI11CArcExtInfoE3AddERKS0_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CArcExtInfoE3AddERKS0_,@function
_ZN13CObjectVectorI11CArcExtInfoE3AddERKS0_: # @_ZN13CObjectVectorI11CArcExtInfoE3AddERKS0_
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	movslq	8(%r15), %rbp
	leaq	1(%rbp), %r13
	testl	%r13d, %r13d
	je	.LBB4_1
# BB#2:                                 # %._crit_edge16.i.i.i
	movl	$4, %ecx
	movq	%r13, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp77:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp78:
# BB#3:                                 # %.noexc
	movq	%rbx, (%r12)
	movl	$0, (%rbx)
	movl	%r13d, 12(%r12)
	jmp	.LBB4_4
.LBB4_1:
	xorl	%ebx, %ebx
.LBB4_4:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movq	(%r15), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_5:                                # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rcx), %edx
	movl	%edx, (%rbx,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB4_5
# BB#6:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
	movl	%ebp, 8(%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r12)
	movslq	24(%r15), %rbp
	leaq	1(%rbp), %r13
	testl	%r13d, %r13d
	je	.LBB4_7
# BB#8:                                 # %._crit_edge16.i.i4.i
	movl	$4, %ecx
	movq	%r13, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp80:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp81:
# BB#9:                                 # %.noexc.i
	movq	%rcx, 16(%r12)
	movl	$0, (%rcx)
	movl	%r13d, 28(%r12)
	jmp	.LBB4_10
.LBB4_7:
	xorl	%ecx, %ecx
.LBB4_10:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i5.i
	movq	16(%r15), %rsi
	.p2align	4, 0x90
.LBB4_11:                               # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB4_11
# BB#12:
	movl	%ebp, 24(%r12)
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r14), %rcx
	movslq	12(%r14), %rax
	movq	%r12, (%rcx,%rax,8)
	leal	1(%rax), %ecx
	movl	%ecx, 12(%r14)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_13:
.Ltmp82:
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB4_16
# BB#14:
	movq	%rbx, %rdi
	callq	_ZdaPv
	jmp	.LBB4_16
.LBB4_15:
.Ltmp79:
	movq	%rax, %r14
.LBB4_16:                               # %.body
	movq	%r12, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN13CObjectVectorI11CArcExtInfoE3AddERKS0_, .Lfunc_end4-_ZN13CObjectVectorI11CArcExtInfoE3AddERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp77-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp77
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp77-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp78-.Ltmp77         #   Call between .Ltmp77 and .Ltmp78
	.long	.Ltmp79-.Lfunc_begin2   #     jumps to .Ltmp79
	.byte	0                       #   On action: cleanup
	.long	.Ltmp80-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp81-.Ltmp80         #   Call between .Ltmp80 and .Ltmp81
	.long	.Ltmp82-.Lfunc_begin2   #     jumps to .Ltmp82
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end4-.Ltmp81     #   Call between .Ltmp81 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED2Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED2Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED2Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED2Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 32
.Lcfi42:
	.cfi_offset %rbx, -24
.Lcfi43:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp83:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp84:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB5_2:
.Ltmp85:
	movq	%rax, %r14
.Ltmp86:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp87:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_4:
.Ltmp88:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED2Ev, .Lfunc_end5-_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp83-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp84-.Ltmp83         #   Call between .Ltmp83 and .Ltmp84
	.long	.Ltmp85-.Lfunc_begin3   #     jumps to .Ltmp85
	.byte	0                       #   On action: cleanup
	.long	.Ltmp84-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp86-.Ltmp84         #   Call between .Ltmp84 and .Ltmp86
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp86-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp87-.Ltmp86         #   Call between .Ltmp86 and .Ltmp87
	.long	.Ltmp88-.Lfunc_begin3   #     jumps to .Ltmp88
	.byte	1                       #   On action: 1
	.long	.Ltmp87-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp87     #   Call between .Ltmp87 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN7CCodecs4LoadEv
	.p2align	4, 0x90
	.type	_ZN7CCodecs4LoadEv,@function
_ZN7CCodecs4LoadEv:                     # @_ZN7CCodecs4LoadEv
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi50:
	.cfi_def_cfa_offset 192
.Lcfi51:
	.cfi_offset %rbx, -56
.Lcfi52:
	.cfi_offset %r12, -48
.Lcfi53:
	.cfi_offset %r13, -40
.Lcfi54:
	.cfi_offset %r14, -32
.Lcfi55:
	.cfi_offset %r15, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	leaq	16(%rdi), %rdi
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	callq	_ZN17CBaseRecordVector5ClearEv
	cmpl	$0, _ZL9g_NumArcs(%rip)
	je	.LBB6_29
# BB#1:                                 # %.lr.ph
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_3 Depth 2
                                        #     Child Loop BB6_7 Depth 2
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movl	%ecx, %eax
	movq	_ZL6g_Arcs(,%rax,8), %r15
	movb	$0, 8(%rsp)
	leaq	16(%rsp), %rax
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbp
	movq	%rbp, 32(%rsp)
	movl	$4, 44(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	$8, 72(%rsp)
	movq	$_ZTV13CObjectVectorI11CArcExtInfoE+16, 48(%rsp)
	movq	$_ZTV7CBufferIhE+16, 80(%rsp)
	movups	%xmm0, 72(%rbx)
	movb	$0, 88(%rbx)
	movq	(%r15), %rbx
	movl	$0, 40(%rsp)
	movl	$0, (%rbp)
	movl	$-1, %r12d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB6_3:                                #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%r12d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB6_3
# BB#4:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
                                        #   in Loop: Header=BB6_2 Depth=1
	cmpl	$3, %r12d
	je	.LBB6_7
# BB#5:                                 #   in Loop: Header=BB6_2 Depth=1
	leal	1(%r12), %r13d
	movslq	%r13d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp89:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
.Ltmp90:
# BB#6:                                 # %._crit_edge16.i.i
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdaPv
	movslq	40(%rsp), %rax
	movq	%r14, 32(%rsp)
	movl	$0, (%r14,%rax,4)
	movl	%r13d, 44(%rsp)
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB6_7:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rbp)
	addq	$4, %rbp
	testl	%eax, %eax
	jne	.LBB6_7
# BB#8:                                 #   in Loop: Header=BB6_2 Depth=1
	movl	%r12d, 40(%rsp)
	movups	64(%r15), %xmm0
	movups	%xmm0, 16(%rsp)
	movq	8(%r15), %rsi
	movq	16(%r15), %rdx
.Ltmp91:
	leaq	8(%rsp), %rdi
	callq	_ZN10CArcInfoEx7AddExtsEPKwS1_
.Ltmp92:
# BB#9:                                 #   in Loop: Header=BB6_2 Depth=1
	cmpq	$0, 72(%r15)
	setne	8(%rsp)
	movb	60(%r15), %al
	movb	%al, 104(%rsp)
	movslq	56(%r15), %rbp
	movq	88(%rsp), %r12
	cmpq	%rbp, %r12
	jne	.LBB6_11
# BB#10:                                # %._ZN7CBufferIhE11SetCapacityEm.exit_crit_edge.i
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	96(%rsp), %r14
	jmp	.LBB6_19
	.p2align	4, 0x90
.LBB6_11:                               #   in Loop: Header=BB6_2 Depth=1
	testl	%ebp, %ebp
	je	.LBB6_12
# BB#13:                                #   in Loop: Header=BB6_2 Depth=1
.Ltmp93:
	movq	%rbp, %rdi
	callq	_Znam
	movq	%rax, %r14
.Ltmp94:
# BB#14:                                # %.noexc21
                                        #   in Loop: Header=BB6_2 Depth=1
	testq	%r12, %r12
	je	.LBB6_16
# BB#15:                                #   in Loop: Header=BB6_2 Depth=1
	movq	96(%rsp), %rsi
	cmpq	%rbp, %r12
	cmovaeq	%rbp, %r12
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	memmove
	jmp	.LBB6_16
.LBB6_12:                               #   in Loop: Header=BB6_2 Depth=1
	xorl	%r14d, %r14d
.LBB6_16:                               #   in Loop: Header=BB6_2 Depth=1
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_18
# BB#17:                                #   in Loop: Header=BB6_2 Depth=1
	callq	_ZdaPv
.LBB6_18:                               #   in Loop: Header=BB6_2 Depth=1
	movq	%r14, 96(%rsp)
	movq	%rbp, 88(%rsp)
.LBB6_19:                               #   in Loop: Header=BB6_2 Depth=1
	addq	$25, %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	memmove
.Ltmp95:
	movl	$104, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp96:
# BB#20:                                # %.noexc22
                                        #   in Loop: Header=BB6_2 Depth=1
.Ltmp97:
	movq	%rbp, %rdi
	leaq	8(%rsp), %rsi
	callq	_ZN10CArcInfoExC2ERKS_
.Ltmp98:
# BB#21:                                #   in Loop: Header=BB6_2 Depth=1
.Ltmp100:
	movq	120(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp101:
# BB#22:                                #   in Loop: Header=BB6_2 Depth=1
	movq	112(%rsp), %rdx         # 8-byte Reload
	movq	32(%rdx), %rax
	movslq	28(%rdx), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 28(%rdx)
	movq	$_ZTV7CBufferIhE+16, 80(%rsp)
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_24
# BB#23:                                #   in Loop: Header=BB6_2 Depth=1
	callq	_ZdaPv
.LBB6_24:                               # %_ZN7CBufferIhED2Ev.exit.i
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	$_ZTV13CObjectVectorI11CArcExtInfoE+16, 48(%rsp)
.Ltmp112:
	leaq	48(%rsp), %rbp
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp113:
# BB#25:                                #   in Loop: Header=BB6_2 Depth=1
.Ltmp118:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp119:
# BB#26:                                # %_ZN13CObjectVectorI11CArcExtInfoED2Ev.exit.i
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_28
# BB#27:                                #   in Loop: Header=BB6_2 Depth=1
	callq	_ZdaPv
.LBB6_28:                               # %_ZN10CArcInfoExD2Ev.exit
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	128(%rsp), %rcx         # 8-byte Reload
	incl	%ecx
	cmpl	_ZL9g_NumArcs(%rip), %ecx
	jb	.LBB6_2
.LBB6_29:                               # %._crit_edge
	xorl	%eax, %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_30:
.Ltmp114:
	movq	%rax, %rbx
.Ltmp115:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp116:
	jmp	.LBB6_33
.LBB6_31:
.Ltmp117:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_32:
.Ltmp120:
	movq	%rax, %rbx
	jmp	.LBB6_33
.LBB6_47:
.Ltmp99:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB6_37
.LBB6_36:
.Ltmp102:
	movq	%rax, %rbx
.LBB6_37:                               # %.body
	movq	$_ZTV7CBufferIhE+16, 80(%rsp)
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_39
# BB#38:
	callq	_ZdaPv
.LBB6_39:                               # %_ZN7CBufferIhED2Ev.exit.i24
	movq	$_ZTV13CObjectVectorI11CArcExtInfoE+16, 48(%rsp)
.Ltmp103:
	leaq	48(%rsp), %rbp
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp104:
# BB#40:
.Ltmp109:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp110:
.LBB6_33:                               # %.body.i
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_35
# BB#34:
	callq	_ZdaPv
.LBB6_35:                               # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB6_43:
.Ltmp111:
	movq	%rax, %rbx
	jmp	.LBB6_44
.LBB6_41:
.Ltmp105:
	movq	%rax, %rbx
.Ltmp106:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp107:
.LBB6_44:                               # %.body.i27
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_46
# BB#45:
	callq	_ZdaPv
.LBB6_46:                               # %.body29
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB6_42:
.Ltmp108:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN7CCodecs4LoadEv, .Lfunc_end6-_ZN7CCodecs4LoadEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\262\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\251\001"              # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp89-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp89
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp89-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp94-.Ltmp89         #   Call between .Ltmp89 and .Ltmp94
	.long	.Ltmp102-.Lfunc_begin4  #     jumps to .Ltmp102
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp95-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp96-.Ltmp95         #   Call between .Ltmp95 and .Ltmp96
	.long	.Ltmp102-.Lfunc_begin4  #     jumps to .Ltmp102
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin4   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp100-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Ltmp101-.Ltmp100       #   Call between .Ltmp100 and .Ltmp101
	.long	.Ltmp102-.Lfunc_begin4  #     jumps to .Ltmp102
	.byte	0                       #   On action: cleanup
	.long	.Ltmp112-.Lfunc_begin4  # >> Call Site 7 <<
	.long	.Ltmp113-.Ltmp112       #   Call between .Ltmp112 and .Ltmp113
	.long	.Ltmp114-.Lfunc_begin4  #     jumps to .Ltmp114
	.byte	0                       #   On action: cleanup
	.long	.Ltmp118-.Lfunc_begin4  # >> Call Site 8 <<
	.long	.Ltmp119-.Ltmp118       #   Call between .Ltmp118 and .Ltmp119
	.long	.Ltmp120-.Lfunc_begin4  #     jumps to .Ltmp120
	.byte	0                       #   On action: cleanup
	.long	.Ltmp115-.Lfunc_begin4  # >> Call Site 9 <<
	.long	.Ltmp116-.Ltmp115       #   Call between .Ltmp115 and .Ltmp116
	.long	.Ltmp117-.Lfunc_begin4  #     jumps to .Ltmp117
	.byte	1                       #   On action: 1
	.long	.Ltmp103-.Lfunc_begin4  # >> Call Site 10 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin4  #     jumps to .Ltmp105
	.byte	1                       #   On action: 1
	.long	.Ltmp109-.Lfunc_begin4  # >> Call Site 11 <<
	.long	.Ltmp110-.Ltmp109       #   Call between .Ltmp109 and .Ltmp110
	.long	.Ltmp111-.Lfunc_begin4  #     jumps to .Ltmp111
	.byte	1                       #   On action: 1
	.long	.Ltmp110-.Lfunc_begin4  # >> Call Site 12 <<
	.long	.Ltmp106-.Ltmp110       #   Call between .Ltmp110 and .Ltmp106
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp106-.Lfunc_begin4  # >> Call Site 13 <<
	.long	.Ltmp107-.Ltmp106       #   Call between .Ltmp106 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin4  #     jumps to .Ltmp108
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZNK7CCodecs24FindFormatForArchiveNameERK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZNK7CCodecs24FindFormatForArchiveNameERK11CStringBaseIwE,@function
_ZNK7CCodecs24FindFormatForArchiveNameERK11CStringBaseIwE: # @_ZNK7CCodecs24FindFormatForArchiveNameERK11CStringBaseIwE
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi63:
	.cfi_def_cfa_offset 80
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movslq	8(%rsi), %rcx
	testq	%rcx, %rcx
	movl	$-1, %ebp
	je	.LBB7_14
# BB#1:
	movq	(%rsi), %rax
	leaq	(,%rcx,4), %rdx
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	cmpl	$47, -4(%rax,%rdx)
	je	.LBB7_3
# BB#4:                                 #   in Loop: Header=BB7_2 Depth=1
	addq	$-4, %rdx
	jne	.LBB7_2
# BB#5:
	movl	$-1, %edi
	jmp	.LBB7_6
.LBB7_3:
	leaq	-4(%rax,%rdx), %rdi
	subq	%rax, %rdi
	shrq	$2, %rdi
.LBB7_6:                                # %_ZNK11CStringBaseIwE11ReverseFindEw.exit
	leaq	(,%rcx,4), %rdx
	.p2align	4, 0x90
.LBB7_7:                                # =>This Inner Loop Header: Depth=1
	cmpl	$46, -4(%rax,%rdx)
	je	.LBB7_8
# BB#9:                                 #   in Loop: Header=BB7_7 Depth=1
	addq	$-4, %rdx
	jne	.LBB7_7
# BB#10:
	movl	$-1, %ebx
	jmp	.LBB7_11
.LBB7_8:
	leaq	-4(%rax,%rdx), %rbx
	subq	%rax, %rbx
	shrq	$2, %rbx
.LBB7_11:                               # %_ZNK11CStringBaseIwE11ReverseFindEw.exit41
	leaq	(,%rcx,4), %rdx
	.p2align	4, 0x90
.LBB7_12:                               # =>This Inner Loop Header: Depth=1
	cmpl	$46, -4(%rax,%rdx)
	je	.LBB7_15
# BB#13:                                #   in Loop: Header=BB7_12 Depth=1
	addq	$-4, %rdx
	jne	.LBB7_12
	jmp	.LBB7_14
.LBB7_15:                               # %_ZNK11CStringBaseIwE11ReverseFindEw.exit45
	leaq	-4(%rax,%rdx), %rdx
	subq	%rax, %rdx
	shrq	$2, %rdx
	cmpl	%ebx, %edx
	jl	.LBB7_14
# BB#16:                                # %_ZNK11CStringBaseIwE11ReverseFindEw.exit45
	testl	%edx, %edx
	js	.LBB7_14
# BB#17:                                # %_ZNK11CStringBaseIwE11ReverseFindEw.exit45
	cmpl	%edi, %edx
	jl	.LBB7_14
# BB#18:
	incl	%edx
	subl	%edx, %ecx
	leaq	8(%rsp), %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	_ZNK11CStringBaseIwE3MidEii
	cmpl	$0, 28(%r15)
	jle	.LBB7_19
# BB#22:                                # %.lr.ph
	xorl	%r13d, %r13d
                                        # implicit-def: %R14D
	.p2align	4, 0x90
.LBB7_23:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_27 Depth 2
	movq	32(%r15), %rax
	movq	(%rax,%r13,8), %rbx
	cmpb	$0, (%rbx)
	je	.LBB7_24
# BB#25:                                #   in Loop: Header=BB7_23 Depth=1
	movl	$-1, %r12d
	cmpl	$0, 52(%rbx)
	jle	.LBB7_31
# BB#26:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB7_23 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_27:                               # %.lr.ph.i
                                        #   Parent Loop BB7_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movq	8(%rsp), %rdi
	movq	(%rax), %rsi
.Ltmp121:
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp122:
# BB#28:                                # %.noexc
                                        #   in Loop: Header=BB7_27 Depth=2
	testl	%eax, %eax
	je	.LBB7_30
# BB#29:                                #   in Loop: Header=BB7_27 Depth=2
	incq	%rbp
	movslq	52(%rbx), %rax
	cmpq	%rax, %rbp
	jl	.LBB7_27
	jmp	.LBB7_31
	.p2align	4, 0x90
.LBB7_24:                               #   in Loop: Header=BB7_23 Depth=1
	movl	$4, %ecx
	jmp	.LBB7_32
.LBB7_30:                               # %.noexc._ZNK10CArcInfoEx13FindExtensionERK11CStringBaseIwE.exit.loopexit_crit_edge
                                        #   in Loop: Header=BB7_23 Depth=1
	movl	%ebp, %r12d
.LBB7_31:                               # %_ZNK10CArcInfoEx13FindExtensionERK11CStringBaseIwE.exit
                                        #   in Loop: Header=BB7_23 Depth=1
	movl	%r12d, %ecx
	shrl	$31, %ecx
	xorl	$1, %ecx
	testl	%r12d, %r12d
	cmovnsl	%r13d, %r14d
.LBB7_32:                               #   in Loop: Header=BB7_23 Depth=1
	movl	%ecx, %eax
	orb	$4, %al
	andb	$7, %al
	cmpb	$4, %al
	jne	.LBB7_20
# BB#33:                                #   in Loop: Header=BB7_23 Depth=1
	incq	%r13
	movslq	28(%r15), %rax
	cmpq	%rax, %r13
	jl	.LBB7_23
# BB#34:
	movl	$2, %ecx
	jmp	.LBB7_20
.LBB7_19:                               # %.._crit_edge_crit_edge
	movl	$2, %ecx
                                        # implicit-def: %R14D
.LBB7_20:                               # %._crit_edge
	cmpl	$2, %ecx
	movl	$-1, %ebp
	cmovnel	%r14d, %ebp
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_14
# BB#21:
	callq	_ZdaPv
.LBB7_14:                               # %_ZNK11CStringBaseIwE11ReverseFindEw.exit45.thread
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_35:
.Ltmp123:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_37
# BB#36:
	callq	_ZdaPv
.LBB7_37:                               # %_ZN11CStringBaseIwED2Ev.exit42
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZNK7CCodecs24FindFormatForArchiveNameERK11CStringBaseIwE, .Lfunc_end7-_ZNK7CCodecs24FindFormatForArchiveNameERK11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp121-.Lfunc_begin5  #   Call between .Lfunc_begin5 and .Ltmp121
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp121-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp122-.Ltmp121       #   Call between .Ltmp121 and .Ltmp122
	.long	.Ltmp123-.Lfunc_begin5  #     jumps to .Ltmp123
	.byte	0                       #   On action: cleanup
	.long	.Ltmp122-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Lfunc_end7-.Ltmp122    #   Call between .Ltmp122 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZNK7CCodecs22FindFormatForExtensionERK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZNK7CCodecs22FindFormatForExtensionERK11CStringBaseIwE,@function
_ZNK7CCodecs22FindFormatForExtensionERK11CStringBaseIwE: # @_ZNK7CCodecs22FindFormatForExtensionERK11CStringBaseIwE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi76:
	.cfi_def_cfa_offset 64
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r15
	movl	$-1, %r14d
	cmpl	$0, 8(%r13)
	je	.LBB8_10
# BB#1:                                 # %.preheader
	cmpl	$0, 28(%r15)
	jle	.LBB8_10
# BB#2:                                 # %.lr.ph
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB8_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_5 Depth 2
	movq	32(%r15), %rax
	movq	(%rax,%r12,8), %rbp
	cmpl	$0, 52(%rbp)
	jle	.LBB8_7
# BB#4:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB8_3 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_5:                                # %.lr.ph.i
                                        #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%rbp), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%r13), %rdi
	movq	(%rax), %rsi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB8_8
# BB#6:                                 #   in Loop: Header=BB8_5 Depth=2
	incq	%rbx
	movslq	52(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB8_5
	jmp	.LBB8_7
	.p2align	4, 0x90
.LBB8_8:                                # %_ZNK10CArcInfoEx13FindExtensionERK11CStringBaseIwE.exit
                                        #   in Loop: Header=BB8_3 Depth=1
	testl	%ebx, %ebx
	jns	.LBB8_9
.LBB8_7:                                # %_ZNK10CArcInfoEx13FindExtensionERK11CStringBaseIwE.exit.thread
                                        #   in Loop: Header=BB8_3 Depth=1
	incq	%r12
	movslq	28(%r15), %rax
	cmpq	%rax, %r12
	jl	.LBB8_3
	jmp	.LBB8_10
.LBB8_9:
	movl	%r12d, %r14d
.LBB8_10:                               # %.loopexit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZNK7CCodecs22FindFormatForExtensionERK11CStringBaseIwE, .Lfunc_end8-_ZNK7CCodecs22FindFormatForExtensionERK11CStringBaseIwE
	.cfi_endproc

	.globl	_ZNK7CCodecs24FindFormatForArchiveTypeERK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZNK7CCodecs24FindFormatForArchiveTypeERK11CStringBaseIwE,@function
_ZNK7CCodecs24FindFormatForArchiveTypeERK11CStringBaseIwE: # @_ZNK7CCodecs24FindFormatForArchiveTypeERK11CStringBaseIwE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi87:
	.cfi_def_cfa_offset 48
.Lcfi88:
	.cfi_offset %rbx, -40
.Lcfi89:
	.cfi_offset %r14, -32
.Lcfi90:
	.cfi_offset %r15, -24
.Lcfi91:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movl	$-1, %r14d
	cmpl	$0, 28(%rbp)
	jle	.LBB9_5
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	movq	32(%rbp), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movq	(%r15), %rsi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB9_4
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	incq	%rbx
	movslq	28(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB9_2
	jmp	.LBB9_5
.LBB9_4:                                # %.._crit_edge.loopexit_crit_edge
	movl	%ebx, %r14d
.LBB9_5:                                # %._crit_edge
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	_ZNK7CCodecs24FindFormatForArchiveTypeERK11CStringBaseIwE, .Lfunc_end9-_ZNK7CCodecs24FindFormatForArchiveTypeERK11CStringBaseIwE
	.cfi_endproc

	.globl	_ZNK7CCodecs24FindFormatForArchiveTypeERK11CStringBaseIwER13CRecordVectorIiE
	.p2align	4, 0x90
	.type	_ZNK7CCodecs24FindFormatForArchiveTypeERK11CStringBaseIwER13CRecordVectorIiE,@function
_ZNK7CCodecs24FindFormatForArchiveTypeERK11CStringBaseIwER13CRecordVectorIiE: # @_ZNK7CCodecs24FindFormatForArchiveTypeERK11CStringBaseIwER13CRecordVectorIiE
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi95:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi96:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi98:
	.cfi_def_cfa_offset 80
.Lcfi99:
	.cfi_offset %rbx, -56
.Lcfi100:
	.cfi_offset %r12, -48
.Lcfi101:
	.cfi_offset %r13, -40
.Lcfi102:
	.cfi_offset %r14, -32
.Lcfi103:
	.cfi_offset %r15, -24
.Lcfi104:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rdx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB10_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_6 Depth 2
                                        #     Child Loop BB10_12 Depth 2
	movl	8(%r15), %eax
	cmpl	%eax, %r13d
	jge	.LBB10_2
# BB#4:                                 #   in Loop: Header=BB10_1 Depth=1
	movq	(%r15), %rcx
	movslq	%r13d, %rdx
	leaq	(%rcx,%rdx,4), %rbp
	movl	(%rcx,%rdx,4), %edx
	cmpl	$46, %edx
	jne	.LBB10_6
	jmp	.LBB10_8
	.p2align	4, 0x90
.LBB10_7:                               #   in Loop: Header=BB10_6 Depth=2
	movl	4(%rbp), %edx
	addq	$4, %rbp
	cmpl	$46, %edx
	je	.LBB10_8
.LBB10_6:                               # %.lr.ph.i
                                        #   Parent Loop BB10_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%edx, %edx
	jne	.LBB10_7
	jmp	.LBB10_9
	.p2align	4, 0x90
.LBB10_8:                               # %_ZNK11CStringBaseIwE4FindEwi.exit
                                        #   in Loop: Header=BB10_1 Depth=1
	subq	%rcx, %rbp
	shrq	$2, %rbp
	testl	%ebp, %ebp
	jns	.LBB10_10
.LBB10_9:                               # %_ZNK11CStringBaseIwE4FindEwi.exit.thread
                                        #   in Loop: Header=BB10_1 Depth=1
	movl	%eax, %ebp
.LBB10_10:                              #   in Loop: Header=BB10_1 Depth=1
	movl	%ebp, %ecx
	subl	%r13d, %ecx
	leaq	8(%rsp), %rdi
	movq	%r15, %rsi
	movl	%r13d, %edx
	callq	_ZNK11CStringBaseIwE3MidEii
	movl	$-1, %r12d
	cmpl	$0, 28(%rbx)
	jle	.LBB10_16
# BB#11:                                # %.lr.ph.i38.preheader
                                        #   in Loop: Header=BB10_1 Depth=1
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB10_12:                              # %.lr.ph.i38
                                        #   Parent Loop BB10_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	32(%rbx), %rax
	movq	(%rax,%r14,8), %rax
	movq	24(%rax), %rdi
	movq	8(%rsp), %rsi
.Ltmp124:
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp125:
# BB#13:                                # %.noexc
                                        #   in Loop: Header=BB10_12 Depth=2
	testl	%eax, %eax
	je	.LBB10_15
# BB#14:                                #   in Loop: Header=BB10_12 Depth=2
	incq	%r14
	movslq	28(%rbx), %rax
	cmpq	%rax, %r14
	jl	.LBB10_12
	jmp	.LBB10_16
	.p2align	4, 0x90
.LBB10_15:                              # %_ZNK7CCodecs24FindFormatForArchiveTypeERK11CStringBaseIwE.exit
                                        #   in Loop: Header=BB10_1 Depth=1
	movl	%r14d, %r12d
	testl	%r14d, %r14d
	jns	.LBB10_20
	.p2align	4, 0x90
.LBB10_16:                              # %_ZNK7CCodecs24FindFormatForArchiveTypeERK11CStringBaseIwE.exit.thread
                                        #   in Loop: Header=BB10_1 Depth=1
	movq	8(%rsp), %rdi
.Ltmp127:
	movl	$.L.str, %esi
	callq	_Z15MyStringComparePKwS0_
.Ltmp128:
# BB#17:                                #   in Loop: Header=BB10_1 Depth=1
	testl	%eax, %eax
	je	.LBB10_20
# BB#18:                                #   in Loop: Header=BB10_1 Depth=1
	movl	$1, %r14d
.Ltmp129:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp130:
	jmp	.LBB10_22
	.p2align	4, 0x90
.LBB10_20:                              #   in Loop: Header=BB10_1 Depth=1
.Ltmp131:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp132:
# BB#21:                                #   in Loop: Header=BB10_1 Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movl	%r12d, (%rax,%rcx,4)
	incl	12(%rdx)
	incl	%ebp
	xorl	%r14d, %r14d
	movl	%ebp, %r13d
.LBB10_22:                              #   in Loop: Header=BB10_1 Depth=1
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_24
# BB#23:                                #   in Loop: Header=BB10_1 Depth=1
	callq	_ZdaPv
.LBB10_24:                              # %_ZN11CStringBaseIwED2Ev.exit37
                                        #   in Loop: Header=BB10_1 Depth=1
	testl	%r14d, %r14d
	je	.LBB10_1
# BB#25:
	xorl	%eax, %eax
	jmp	.LBB10_3
.LBB10_2:
	movb	$1, %al
.LBB10_3:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_19:
.Ltmp133:
	jmp	.LBB10_27
.LBB10_26:
.Ltmp126:
.LBB10_27:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_29
# BB#28:
	callq	_ZdaPv
.LBB10_29:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZNK7CCodecs24FindFormatForArchiveTypeERK11CStringBaseIwER13CRecordVectorIiE, .Lfunc_end10-_ZNK7CCodecs24FindFormatForArchiveTypeERK11CStringBaseIwER13CRecordVectorIiE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp124-.Lfunc_begin6  #   Call between .Lfunc_begin6 and .Ltmp124
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp124-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp125-.Ltmp124       #   Call between .Ltmp124 and .Ltmp125
	.long	.Ltmp126-.Lfunc_begin6  #     jumps to .Ltmp126
	.byte	0                       #   On action: cleanup
	.long	.Ltmp127-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp132-.Ltmp127       #   Call between .Ltmp127 and .Ltmp132
	.long	.Ltmp133-.Lfunc_begin6  #     jumps to .Ltmp133
	.byte	0                       #   On action: cleanup
	.long	.Ltmp132-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Lfunc_end10-.Ltmp132   #   Call between .Ltmp132 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%rbp
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi107:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi108:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi109:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi111:
	.cfi_def_cfa_offset 64
.Lcfi112:
	.cfi_offset %rbx, -56
.Lcfi113:
	.cfi_offset %r12, -48
.Lcfi114:
	.cfi_offset %r13, -40
.Lcfi115:
	.cfi_offset %r14, -32
.Lcfi116:
	.cfi_offset %r15, -24
.Lcfi117:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB11_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB11_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB11_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB11_5
.LBB11_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB11_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp134:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp135:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB11_35
# BB#12:
	movq	%rbx, %r13
.LBB11_13:                              # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB11_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB11_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB11_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB11_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB11_15
.LBB11_14:
	xorl	%esi, %esi
.LBB11_15:                              # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB11_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB11_16
.LBB11_29:
	movq	%r13, %rbx
.LBB11_30:                              # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp136:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp137:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB11_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB11_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB11_8
.LBB11_3:
	xorl	%eax, %eax
.LBB11_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB11_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB11_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB11_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_35:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB11_30
.LBB11_21:                              # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB11_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_24:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB11_24
	jmp	.LBB11_25
.LBB11_22:
	xorl	%ecx, %ecx
.LBB11_25:                              # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB11_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB11_27:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB11_27
.LBB11_28:                              # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB11_15
	jmp	.LBB11_29
.LBB11_34:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp138:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end11-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin7-.Lfunc_begin7 # >> Call Site 1 <<
	.long	.Ltmp134-.Lfunc_begin7  #   Call between .Lfunc_begin7 and .Ltmp134
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp134-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp137-.Ltmp134       #   Call between .Ltmp134 and .Ltmp137
	.long	.Ltmp138-.Lfunc_begin7  #     jumps to .Ltmp138
	.byte	0                       #   On action: cleanup
	.long	.Ltmp137-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Lfunc_end11-.Ltmp137   #   Call between .Ltmp137 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CArcExtInfoED2Ev,"axG",@progbits,_ZN13CObjectVectorI11CArcExtInfoED2Ev,comdat
	.weak	_ZN13CObjectVectorI11CArcExtInfoED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CArcExtInfoED2Ev,@function
_ZN13CObjectVectorI11CArcExtInfoED2Ev:  # @_ZN13CObjectVectorI11CArcExtInfoED2Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi118:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi119:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi120:
	.cfi_def_cfa_offset 32
.Lcfi121:
	.cfi_offset %rbx, -24
.Lcfi122:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CArcExtInfoE+16, (%rbx)
.Ltmp139:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp140:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB12_2:
.Ltmp141:
	movq	%rax, %r14
.Ltmp142:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp143:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB12_4:
.Ltmp144:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN13CObjectVectorI11CArcExtInfoED2Ev, .Lfunc_end12-_ZN13CObjectVectorI11CArcExtInfoED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp139-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp140-.Ltmp139       #   Call between .Ltmp139 and .Ltmp140
	.long	.Ltmp141-.Lfunc_begin8  #     jumps to .Ltmp141
	.byte	0                       #   On action: cleanup
	.long	.Ltmp140-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp142-.Ltmp140       #   Call between .Ltmp140 and .Ltmp142
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp142-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp143-.Ltmp142       #   Call between .Ltmp142 and .Ltmp143
	.long	.Ltmp144-.Lfunc_begin8  #     jumps to .Ltmp144
	.byte	1                       #   On action: 1
	.long	.Ltmp143-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Lfunc_end12-.Ltmp143   #   Call between .Ltmp143 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CArcExtInfoED0Ev,"axG",@progbits,_ZN13CObjectVectorI11CArcExtInfoED0Ev,comdat
	.weak	_ZN13CObjectVectorI11CArcExtInfoED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CArcExtInfoED0Ev,@function
_ZN13CObjectVectorI11CArcExtInfoED0Ev:  # @_ZN13CObjectVectorI11CArcExtInfoED0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi125:
	.cfi_def_cfa_offset 32
.Lcfi126:
	.cfi_offset %rbx, -24
.Lcfi127:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CArcExtInfoE+16, (%rbx)
.Ltmp145:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp146:
# BB#1:
.Ltmp151:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp152:
# BB#2:                                 # %_ZN13CObjectVectorI11CArcExtInfoED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB13_5:
.Ltmp153:
	movq	%rax, %r14
	jmp	.LBB13_6
.LBB13_3:
.Ltmp147:
	movq	%rax, %r14
.Ltmp148:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp149:
.LBB13_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB13_4:
.Ltmp150:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end13:
	.size	_ZN13CObjectVectorI11CArcExtInfoED0Ev, .Lfunc_end13-_ZN13CObjectVectorI11CArcExtInfoED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp145-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp146-.Ltmp145       #   Call between .Ltmp145 and .Ltmp146
	.long	.Ltmp147-.Lfunc_begin9  #     jumps to .Ltmp147
	.byte	0                       #   On action: cleanup
	.long	.Ltmp151-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp152-.Ltmp151       #   Call between .Ltmp151 and .Ltmp152
	.long	.Ltmp153-.Lfunc_begin9  #     jumps to .Ltmp153
	.byte	0                       #   On action: cleanup
	.long	.Ltmp148-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp149-.Ltmp148       #   Call between .Ltmp148 and .Ltmp149
	.long	.Ltmp150-.Lfunc_begin9  #     jumps to .Ltmp150
	.byte	1                       #   On action: 1
	.long	.Ltmp149-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Lfunc_end13-.Ltmp149   #   Call between .Ltmp149 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CArcExtInfoE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI11CArcExtInfoE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI11CArcExtInfoE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CArcExtInfoE6DeleteEii,@function
_ZN13CObjectVectorI11CArcExtInfoE6DeleteEii: # @_ZN13CObjectVectorI11CArcExtInfoE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi130:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi131:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi132:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi134:
	.cfi_def_cfa_offset 64
.Lcfi135:
	.cfi_offset %rbx, -56
.Lcfi136:
	.cfi_offset %r12, -48
.Lcfi137:
	.cfi_offset %r13, -40
.Lcfi138:
	.cfi_offset %r14, -32
.Lcfi139:
	.cfi_offset %r15, -24
.Lcfi140:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB14_9
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB14_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB14_8
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_5
# BB#4:                                 #   in Loop: Header=BB14_2 Depth=1
	callq	_ZdaPv
.LBB14_5:                               # %_ZN11CStringBaseIwED2Ev.exit.i
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_7
# BB#6:                                 #   in Loop: Header=BB14_2 Depth=1
	callq	_ZdaPv
.LBB14_7:                               # %_ZN11CArcExtInfoD2Ev.exit
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB14_8:                               #   in Loop: Header=BB14_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB14_2
.LBB14_9:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end14:
	.size	_ZN13CObjectVectorI11CArcExtInfoE6DeleteEii, .Lfunc_end14-_ZN13CObjectVectorI11CArcExtInfoE6DeleteEii
	.cfi_endproc

	.section	.text._ZN7CBufferIhED2Ev,"axG",@progbits,_ZN7CBufferIhED2Ev,comdat
	.weak	_ZN7CBufferIhED2Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED2Ev,@function
_ZN7CBufferIhED2Ev:                     # @_ZN7CBufferIhED2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV7CBufferIhE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB15_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB15_1:
	retq
.Lfunc_end15:
	.size	_ZN7CBufferIhED2Ev, .Lfunc_end15-_ZN7CBufferIhED2Ev
	.cfi_endproc

	.section	.text._ZN7CBufferIhED0Ev,"axG",@progbits,_ZN7CBufferIhED0Ev,comdat
	.weak	_ZN7CBufferIhED0Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED0Ev,@function
_ZN7CBufferIhED0Ev:                     # @_ZN7CBufferIhED0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi141:
	.cfi_def_cfa_offset 16
.Lcfi142:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV7CBufferIhE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB16_2
# BB#1:
	callq	_ZdaPv
.LBB16_2:                               # %_ZN7CBufferIhED2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end16:
	.size	_ZN7CBufferIhED0Ev, .Lfunc_end16-_ZN7CBufferIhED0Ev
	.cfi_endproc

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED0Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED0Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED0Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED0Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED0Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi143:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi144:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi145:
	.cfi_def_cfa_offset 32
.Lcfi146:
	.cfi_offset %rbx, -24
.Lcfi147:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp154:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp155:
# BB#1:
.Ltmp160:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp161:
# BB#2:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB17_5:
.Ltmp162:
	movq	%rax, %r14
	jmp	.LBB17_6
.LBB17_3:
.Ltmp156:
	movq	%rax, %r14
.Ltmp157:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp158:
.LBB17_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB17_4:
.Ltmp159:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end17:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED0Ev, .Lfunc_end17-_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp154-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp155-.Ltmp154       #   Call between .Ltmp154 and .Ltmp155
	.long	.Ltmp156-.Lfunc_begin10 #     jumps to .Ltmp156
	.byte	0                       #   On action: cleanup
	.long	.Ltmp160-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp161-.Ltmp160       #   Call between .Ltmp160 and .Ltmp161
	.long	.Ltmp162-.Lfunc_begin10 #     jumps to .Ltmp162
	.byte	0                       #   On action: cleanup
	.long	.Ltmp157-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp158-.Ltmp157       #   Call between .Ltmp157 and .Ltmp158
	.long	.Ltmp159-.Lfunc_begin10 #     jumps to .Ltmp159
	.byte	1                       #   On action: 1
	.long	.Ltmp158-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Lfunc_end17-.Ltmp158   #   Call between .Ltmp158 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,@function
_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii: # @_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi148:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi149:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi150:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi151:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi152:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi153:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi154:
	.cfi_def_cfa_offset 64
.Lcfi155:
	.cfi_offset %rbx, -56
.Lcfi156:
	.cfi_offset %r12, -48
.Lcfi157:
	.cfi_offset %r13, -40
.Lcfi158:
	.cfi_offset %r14, -32
.Lcfi159:
	.cfi_offset %r15, -24
.Lcfi160:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB18_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB18_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB18_6
# BB#3:                                 #   in Loop: Header=BB18_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_5
# BB#4:                                 #   in Loop: Header=BB18_2 Depth=1
	callq	_ZdaPv
.LBB18_5:                               # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB18_6:                               #   in Loop: Header=BB18_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB18_2
.LBB18_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end18:
	.size	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii, .Lfunc_end18-_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN10CArcInfoExC2ERKS_,"axG",@progbits,_ZN10CArcInfoExC2ERKS_,comdat
	.weak	_ZN10CArcInfoExC2ERKS_
	.p2align	4, 0x90
	.type	_ZN10CArcInfoExC2ERKS_,@function
_ZN10CArcInfoExC2ERKS_:                 # @_ZN10CArcInfoExC2ERKS_
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%rbp
.Lcfi161:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi162:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi163:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi164:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi165:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi166:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi167:
	.cfi_def_cfa_offset 64
.Lcfi168:
	.cfi_offset %rbx, -56
.Lcfi169:
	.cfi_offset %r12, -48
.Lcfi170:
	.cfi_offset %r13, -40
.Lcfi171:
	.cfi_offset %r14, -32
.Lcfi172:
	.cfi_offset %r15, -24
.Lcfi173:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	16(%r15), %rax
	movq	%rax, 16(%r14)
	movups	(%r15), %xmm0
	movups	%xmm0, (%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%r14)
	movslq	32(%r15), %r12
	leaq	1(%r12), %rbx
	testl	%ebx, %ebx
	je	.LBB19_1
# BB#2:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, 24(%r14)
	movl	$0, (%rax)
	movl	%ebx, 36(%r14)
	jmp	.LBB19_3
.LBB19_1:
	xorl	%eax, %eax
.LBB19_3:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	leaq	24(%r14), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	24(%r15), %rcx
	.p2align	4, 0x90
.LBB19_4:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB19_4
# BB#5:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit
	movl	%r12d, 32(%r14)
	leaq	40(%r14), %r13
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%r14)
	movq	$8, 64(%r14)
	movq	$_ZTV13CObjectVectorI11CArcExtInfoE+16, 40(%r14)
.Ltmp163:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp164:
# BB#6:                                 # %.noexc.i
	movl	52(%r15), %ebx
	movl	52(%r14), %esi
	addl	%ebx, %esi
.Ltmp165:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp166:
# BB#7:                                 # %.noexc3.i
	testl	%ebx, %ebx
	jle	.LBB19_11
# BB#8:                                 # %.lr.ph.i.i.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB19_9:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbp,8), %rsi
.Ltmp168:
	movq	%r13, %rdi
	callq	_ZN13CObjectVectorI11CArcExtInfoE3AddERKS0_
.Ltmp169:
# BB#10:                                # %.noexc4.i
                                        #   in Loop: Header=BB19_9 Depth=1
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB19_9
.LBB19_11:                              # %_ZN13CObjectVectorI11CArcExtInfoEC2ERKS1_.exit
	movq	$_ZTV7CBufferIhE+16, 72(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 80(%r14)
	movq	80(%r15), %r12
	testq	%r12, %r12
	je	.LBB19_14
# BB#12:                                # %_ZN7CBufferIhE11SetCapacityEm.exit.i.i
.Ltmp174:
	movq	%r12, %rdi
	callq	_Znam
.Ltmp175:
# BB#13:                                # %.noexc
	movq	%rax, 88(%r14)
	movq	%r12, 80(%r14)
	movq	80(%r15), %rdx
	movq	88(%r15), %rsi
	movq	%rax, %rdi
	callq	memmove
.LBB19_14:                              # %_ZN7CBufferIhEC2ERKS0_.exit
	movb	96(%r15), %al
	movb	%al, 96(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB19_22:
.Ltmp176:
	movq	%rax, %r14
	movq	$_ZTV13CObjectVectorI11CArcExtInfoE+16, (%r13)
.Ltmp177:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp178:
# BB#23:
.Ltmp183:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp184:
	jmp	.LBB19_18
.LBB19_26:
.Ltmp185:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB19_24:
.Ltmp179:
	movq	%rax, %r14
.Ltmp180:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp181:
# BB#27:                                # %.body9
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB19_25:
.Ltmp182:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB19_16:                              # %.loopexit.split-lp.i
.Ltmp167:
	jmp	.LBB19_17
.LBB19_15:                              # %.loopexit.i
.Ltmp170:
.LBB19_17:
	movq	%rax, %r14
.Ltmp171:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp172:
.LBB19_18:                              # %_ZN13CObjectVectorI11CArcExtInfoED2Ev.exit
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB19_20
# BB#19:
	callq	_ZdaPv
.LBB19_20:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB19_21:
.Ltmp173:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end19:
	.size	_ZN10CArcInfoExC2ERKS_, .Lfunc_end19-_ZN10CArcInfoExC2ERKS_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Lfunc_begin11-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp163-.Lfunc_begin11 #   Call between .Lfunc_begin11 and .Ltmp163
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp163-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp166-.Ltmp163       #   Call between .Ltmp163 and .Ltmp166
	.long	.Ltmp167-.Lfunc_begin11 #     jumps to .Ltmp167
	.byte	0                       #   On action: cleanup
	.long	.Ltmp168-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp169-.Ltmp168       #   Call between .Ltmp168 and .Ltmp169
	.long	.Ltmp170-.Lfunc_begin11 #     jumps to .Ltmp170
	.byte	0                       #   On action: cleanup
	.long	.Ltmp174-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Ltmp175-.Ltmp174       #   Call between .Ltmp174 and .Ltmp175
	.long	.Ltmp176-.Lfunc_begin11 #     jumps to .Ltmp176
	.byte	0                       #   On action: cleanup
	.long	.Ltmp175-.Lfunc_begin11 # >> Call Site 5 <<
	.long	.Ltmp177-.Ltmp175       #   Call between .Ltmp175 and .Ltmp177
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp177-.Lfunc_begin11 # >> Call Site 6 <<
	.long	.Ltmp178-.Ltmp177       #   Call between .Ltmp177 and .Ltmp178
	.long	.Ltmp179-.Lfunc_begin11 #     jumps to .Ltmp179
	.byte	1                       #   On action: 1
	.long	.Ltmp183-.Lfunc_begin11 # >> Call Site 7 <<
	.long	.Ltmp184-.Ltmp183       #   Call between .Ltmp183 and .Ltmp184
	.long	.Ltmp185-.Lfunc_begin11 #     jumps to .Ltmp185
	.byte	1                       #   On action: 1
	.long	.Ltmp180-.Lfunc_begin11 # >> Call Site 8 <<
	.long	.Ltmp181-.Ltmp180       #   Call between .Ltmp180 and .Ltmp181
	.long	.Ltmp182-.Lfunc_begin11 #     jumps to .Ltmp182
	.byte	1                       #   On action: 1
	.long	.Ltmp171-.Lfunc_begin11 # >> Call Site 9 <<
	.long	.Ltmp172-.Ltmp171       #   Call between .Ltmp171 and .Ltmp172
	.long	.Ltmp173-.Lfunc_begin11 #     jumps to .Ltmp173
	.byte	1                       #   On action: 1
	.long	.Ltmp172-.Lfunc_begin11 # >> Call Site 10 <<
	.long	.Lfunc_end19-.Ltmp172   #   Call between .Ltmp172 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.type	_ZL9g_NumArcs,@object   # @_ZL9g_NumArcs
	.local	_ZL9g_NumArcs
	.comm	_ZL9g_NumArcs,4,4
	.type	_ZL6g_Arcs,@object      # @_ZL6g_Arcs
	.local	_ZL6g_Arcs
	.comm	_ZL6g_Arcs,384,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	42                      # 0x2a
	.long	0                       # 0x0
	.size	.L.str, 8

	.type	_ZTV13CObjectVectorI11CArcExtInfoE,@object # @_ZTV13CObjectVectorI11CArcExtInfoE
	.section	.rodata._ZTV13CObjectVectorI11CArcExtInfoE,"aG",@progbits,_ZTV13CObjectVectorI11CArcExtInfoE,comdat
	.weak	_ZTV13CObjectVectorI11CArcExtInfoE
	.p2align	3
_ZTV13CObjectVectorI11CArcExtInfoE:
	.quad	0
	.quad	_ZTI13CObjectVectorI11CArcExtInfoE
	.quad	_ZN13CObjectVectorI11CArcExtInfoED2Ev
	.quad	_ZN13CObjectVectorI11CArcExtInfoED0Ev
	.quad	_ZN13CObjectVectorI11CArcExtInfoE6DeleteEii
	.size	_ZTV13CObjectVectorI11CArcExtInfoE, 40

	.type	_ZTS13CObjectVectorI11CArcExtInfoE,@object # @_ZTS13CObjectVectorI11CArcExtInfoE
	.section	.rodata._ZTS13CObjectVectorI11CArcExtInfoE,"aG",@progbits,_ZTS13CObjectVectorI11CArcExtInfoE,comdat
	.weak	_ZTS13CObjectVectorI11CArcExtInfoE
	.p2align	4
_ZTS13CObjectVectorI11CArcExtInfoE:
	.asciz	"13CObjectVectorI11CArcExtInfoE"
	.size	_ZTS13CObjectVectorI11CArcExtInfoE, 31

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorI11CArcExtInfoE,@object # @_ZTI13CObjectVectorI11CArcExtInfoE
	.section	.rodata._ZTI13CObjectVectorI11CArcExtInfoE,"aG",@progbits,_ZTI13CObjectVectorI11CArcExtInfoE,comdat
	.weak	_ZTI13CObjectVectorI11CArcExtInfoE
	.p2align	4
_ZTI13CObjectVectorI11CArcExtInfoE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI11CArcExtInfoE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI11CArcExtInfoE, 24

	.type	_ZTV7CBufferIhE,@object # @_ZTV7CBufferIhE
	.section	.rodata._ZTV7CBufferIhE,"aG",@progbits,_ZTV7CBufferIhE,comdat
	.weak	_ZTV7CBufferIhE
	.p2align	3
_ZTV7CBufferIhE:
	.quad	0
	.quad	_ZTI7CBufferIhE
	.quad	_ZN7CBufferIhED2Ev
	.quad	_ZN7CBufferIhED0Ev
	.size	_ZTV7CBufferIhE, 32

	.type	_ZTS7CBufferIhE,@object # @_ZTS7CBufferIhE
	.section	.rodata._ZTS7CBufferIhE,"aG",@progbits,_ZTS7CBufferIhE,comdat
	.weak	_ZTS7CBufferIhE
_ZTS7CBufferIhE:
	.asciz	"7CBufferIhE"
	.size	_ZTS7CBufferIhE, 12

	.type	_ZTI7CBufferIhE,@object # @_ZTI7CBufferIhE
	.section	.rodata._ZTI7CBufferIhE,"aG",@progbits,_ZTI7CBufferIhE,comdat
	.weak	_ZTI7CBufferIhE
	.p2align	3
_ZTI7CBufferIhE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS7CBufferIhE
	.size	_ZTI7CBufferIhE, 16

	.type	_ZTV13CObjectVectorI11CStringBaseIwEE,@object # @_ZTV13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTV13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTV13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTV13CObjectVectorI11CStringBaseIwEE
	.p2align	3
_ZTV13CObjectVectorI11CStringBaseIwEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI11CStringBaseIwEE
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.size	_ZTV13CObjectVectorI11CStringBaseIwEE, 40

	.type	_ZTS13CObjectVectorI11CStringBaseIwEE,@object # @_ZTS13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTS13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTS13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTS13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTS13CObjectVectorI11CStringBaseIwEE:
	.asciz	"13CObjectVectorI11CStringBaseIwEE"
	.size	_ZTS13CObjectVectorI11CStringBaseIwEE, 34

	.type	_ZTI13CObjectVectorI11CStringBaseIwEE,@object # @_ZTI13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTI13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTI13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTI13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTI13CObjectVectorI11CStringBaseIwEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI11CStringBaseIwEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI11CStringBaseIwEE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
