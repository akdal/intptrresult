	.text
	.file	"strcat.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	$5000000, %r14d         # imm = 0x4C4B40
	cmpl	$2, %edi
	jne	.LBB0_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r14
.LBB0_2:
	leaq	24(%rsp), %rax
	movq	%rax, 8(%rsp)
	movq	$0, 16(%rsp)
	movb	$0, 24(%rsp)
.Ltmp0:
	leaq	8(%rsp), %rdi
	movl	$31, %esi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm
.Ltmp1:
# BB#3:                                 # %.preheader
	testl	%r14d, %r14d
	jle	.LBB0_14
# BB#4:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	movl	$31, %r13d
	movl	$6, %ebx
	leaq	8(%rsp), %r15
	movabsq	$9223372036854775807, %r12 # imm = 0x7FFFFFFFFFFFFFFF
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r13, %rbx
	jbe	.LBB0_7
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=1
	addq	%r13, %r13
.Ltmp3:
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm
.Ltmp4:
.LBB0_7:                                #   in Loop: Header=BB0_5 Depth=1
	movq	%r12, %rax
	subq	16(%rsp), %rax
	cmpq	$5, %rax
	jbe	.LBB0_8
# BB#12:                                # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc.exit.i
                                        #   in Loop: Header=BB0_5 Depth=1
.Ltmp5:
	movl	$.L.str, %esi
	movl	$6, %edx
	movq	%r15, %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm
.Ltmp6:
# BB#13:                                # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEpLEPKc.exit
                                        #   in Loop: Header=BB0_5 Depth=1
	addq	$6, %rbx
	incl	%ebp
	cmpl	%r14d, %ebp
	jl	.LBB0_5
.LBB0_14:                               # %._crit_edge
	movq	16(%rsp), %rsi
.Ltmp8:
	movl	$_ZSt4cout, %edi
	callq	_ZNSo9_M_insertImEERSoT_
	movq	%rax, %r14
.Ltmp9:
# BB#15:                                # %_ZNSolsEm.exit
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_16
# BB#18:                                # %.noexc37
	cmpb	$0, 56(%rbx)
	je	.LBB0_20
# BB#19:
	movb	67(%rbx), %al
	jmp	.LBB0_22
.LBB0_20:
.Ltmp10:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp11:
# BB#21:                                # %.noexc39
	movq	(%rbx), %rax
.Ltmp12:
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.Ltmp13:
.LBB0_22:                               # %.noexc32
.Ltmp14:
	movsbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZNSo3putEc
.Ltmp15:
# BB#23:                                # %.noexc33
.Ltmp16:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp17:
# BB#24:                                # %_ZNSolsEPFRSoS_E.exit
	movq	8(%rsp), %rdi
	leaq	24(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB0_26
# BB#25:
	callq	_ZdlPv
.LBB0_26:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_8:
.Ltmp20:
	movl	$.L.str.1, %edi
	callq	_ZSt20__throw_length_errorPKc
.Ltmp21:
# BB#11:                                # %.noexc
.LBB0_16:
.Ltmp18:
	callq	_ZSt16__throw_bad_castv
.Ltmp19:
# BB#17:                                # %.noexc41
.LBB0_27:
.Ltmp2:
	jmp	.LBB0_28
.LBB0_10:                               # %.loopexit.split-lp
.Ltmp22:
	jmp	.LBB0_28
.LBB0_9:                                # %.loopexit
.Ltmp7:
.LBB0_28:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	leaq	24(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB0_30
# BB#29:
	callq	_ZdlPv
.LBB0_30:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit36
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp3           #   Call between .Ltmp3 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp19-.Ltmp8          #   Call between .Ltmp8 and .Ltmp19
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp19     #   Call between .Ltmp19 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_strcat.ii,@function
_GLOBAL__sub_I_strcat.ii:               # @_GLOBAL__sub_I_strcat.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end1:
	.size	_GLOBAL__sub_I_strcat.ii, .Lfunc_end1-_GLOBAL__sub_I_strcat.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"hello\n"
	.size	.L.str, 7

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"basic_string::append"
	.size	.L.str.1, 21

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_strcat.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
