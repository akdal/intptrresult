	.text
	.file	"health.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	alloc_tree
	.p2align	4, 0x90
	.type	alloc_tree,@function
alloc_tree:                             # @alloc_tree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 80
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	%esi, %r14d
	movl	%edi, %ebp
	testl	%ebp, %ebp
	je	.LBB0_1
# BB#2:
	movl	$192, %edi
	callq	malloc
	movq	%rax, %rbx
	decl	%ebp
	leal	1(,%r14,4), %r12d
	leal	4(,%r14,4), %esi
	movl	%ebp, %edi
	movq	%rbx, %rdx
	callq	alloc_tree
	movq	%rax, 24(%rsp)
	leal	3(,%r14,4), %esi
	movl	%ebp, %edi
	movq	%rbx, %rdx
	callq	alloc_tree
	movq	%rax, 16(%rsp)
	leal	2(,%r14,4), %esi
	movl	%ebp, %edi
	movq	%rbx, %rdx
	callq	alloc_tree
	movq	%rax, 8(%rsp)
	movl	%ebp, %edi
	movl	%r12d, %esi
	movq	%rbx, %rdx
	callq	alloc_tree
	movq	%rax, (%rsp)
	movq	%r15, 32(%rbx)
	movl	%r14d, 176(%rbx)
	movslq	%r14d, %rax
	movl	$127773, %ecx           # imm = 0x1F31D
	addq	seed(%rip), %rcx
	imulq	%rax, %rcx
	movq	%rcx, 184(%rbx)
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movl	%ebp, %edi
	callq	ldexp
	cvttsd2si	%xmm0, %eax
	movl	%eax, 64(%rbx)
	movl	%eax, 68(%rbx)
	movl	$0, 72(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 40(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 160(%rbx)
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movaps	(%rsp), %xmm0
	movaps	16(%rsp), %xmm1
	movups	%xmm1, 16(%rbx)
	movups	%xmm0, (%rbx)
	jmp	.LBB0_3
.LBB0_1:
	xorl	%ebx, %ebx
.LBB0_3:
	movq	%rbx, %rax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	alloc_tree, .Lfunc_end0-alloc_tree
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1065353216              # float 1
	.text
	.globl	get_results
	.p2align	4, 0x90
	.type	get_results,@function
get_results:                            # @get_results
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
	subq	$64, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 80
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB1_1
# BB#2:                                 # %.preheader.preheader
	movq	24(%rbx), %rdi
	callq	get_results
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movq	16(%rbx), %rdi
	callq	get_results
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	movq	8(%rbx), %rdi
	callq	get_results
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movss	%xmm1, 4(%rsp)          # 4-byte Spill
	movq	(%rbx), %rdi
	callq	get_results
	movaps	%xmm0, %xmm2
	movaps	%xmm1, %xmm3
	xorps	%xmm1, %xmm1
	addss	12(%rsp), %xmm1         # 4-byte Folded Reload
	xorps	%xmm0, %xmm0
	addps	48(%rsp), %xmm0         # 16-byte Folded Reload
	addss	8(%rsp), %xmm1          # 4-byte Folded Reload
	addps	32(%rsp), %xmm0         # 16-byte Folded Reload
	addss	4(%rsp), %xmm1          # 4-byte Folded Reload
	addps	16(%rsp), %xmm0         # 16-byte Folded Reload
	addss	%xmm3, %xmm1
	addps	%xmm2, %xmm0
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.LBB1_5
# BB#3:                                 # %.lr.ph.preheader
	movss	.LCPI1_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	xorps	%xmm3, %xmm3
	cvtsi2ssl	(%rcx), %xmm3
	addss	%xmm3, %xmm1
	xorps	%xmm3, %xmm3
	cvtsi2ssl	4(%rcx), %xmm3
	movaps	%xmm2, %xmm4
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	addps	%xmm4, %xmm0
	testq	%rax, %rax
	jne	.LBB1_4
	jmp	.LBB1_5
.LBB1_1:
	xorps	%xmm1, %xmm1
	xorps	%xmm0, %xmm0
.LBB1_5:
	addq	$64, %rsp
	popq	%rbx
	retq
.Lfunc_end1:
	.size	get_results, .Lfunc_end1-get_results
	.cfi_endproc

	.globl	check_patients_inside
	.p2align	4, 0x90
	.type	check_patients_inside,@function
check_patients_inside:                  # @check_patients_inside
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 48
.Lcfi19:
	.cfi_offset %rbx, -48
.Lcfi20:
	.cfi_offset %r12, -40
.Lcfi21:
	.cfi_offset %r13, -32
.Lcfi22:
	.cfi_offset %r14, -24
.Lcfi23:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB2_5
# BB#1:                                 # %.lr.ph
	movq	%r14, %r15
	subq	$-128, %r15
	leaq	40(%r14), %r12
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r13
	decl	8(%r13)
	jne	.LBB2_4
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	incl	68(%r14)
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	removeList
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	addList
.LBB2_4:                                #   in Loop: Header=BB2_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_2
.LBB2_5:                                # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	check_patients_inside, .Lfunc_end2-check_patients_inside
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	1325400064              # float 2.14748365E+9
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_1:
	.quad	4591870180066957722     # double 0.10000000000000001
	.text
	.globl	check_patients_assess
	.p2align	4, 0x90
	.type	check_patients_assess,@function
check_patients_assess:                  # @check_patients_assess
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 64
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB3_1
# BB#2:                                 # %.lr.ph
	leaq	104(%r14), %r15
	movq	%r14, %rax
	subq	$-128, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	152(%r14), %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r12
	decl	8(%r12)
	jne	.LBB3_8
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	184(%r14), %rdi
	callq	my_rand
	cvtss2sd	%xmm0, %xmm1
	mulss	.LCPI3_0(%rip), %xmm0
	cvttss2si	%xmm0, %rax
	movq	%rax, 184(%r14)
	ucomisd	.LCPI3_1(%rip), %xmm1
	ja	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_3 Depth=1
	movl	176(%r14), %eax
	testl	%eax, %eax
	je	.LBB3_6
# BB#7:                                 #   in Loop: Header=BB3_3 Depth=1
	incl	68(%r14)
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	removeList
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	addList
	movq	%r13, %rbp
	jmp	.LBB3_8
.LBB3_6:                                #   in Loop: Header=BB3_3 Depth=1
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	removeList
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r12, %rsi
	callq	addList
	movl	$10, 8(%r12)
	addl	$10, 4(%r12)
	.p2align	4, 0x90
.LBB3_8:                                #   in Loop: Header=BB3_3 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_3
	jmp	.LBB3_9
.LBB3_1:
	xorl	%ebp, %ebp
.LBB3_9:                                # %._crit_edge
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	check_patients_assess, .Lfunc_end3-check_patients_assess
	.cfi_endproc

	.globl	check_patients_waiting
	.p2align	4, 0x90
	.type	check_patients_waiting,@function
check_patients_waiting:                 # @check_patients_waiting
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 48
.Lcfi42:
	.cfi_offset %rbx, -48
.Lcfi43:
	.cfi_offset %r12, -40
.Lcfi44:
	.cfi_offset %r13, -32
.Lcfi45:
	.cfi_offset %r14, -24
.Lcfi46:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB4_6
# BB#1:                                 # %.lr.ph
	leaq	80(%r14), %r15
	leaq	104(%r14), %r12
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movl	68(%r14), %eax
	movq	8(%rbx), %r13
	testl	%eax, %eax
	jle	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	decl	%eax
	movl	%eax, 68(%r14)
	movl	$3, 8(%r13)
	addl	$3, 4(%r13)
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	removeList
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	addList
	jmp	.LBB4_5
	.p2align	4, 0x90
.LBB4_4:                                #   in Loop: Header=BB4_2 Depth=1
	incl	4(%r13)
.LBB4_5:                                #   in Loop: Header=BB4_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB4_2
.LBB4_6:                                # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	check_patients_waiting, .Lfunc_end4-check_patients_waiting
	.cfi_endproc

	.globl	put_in_hosp
	.p2align	4, 0x90
	.type	put_in_hosp,@function
put_in_hosp:                            # @put_in_hosp
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 16
.Lcfi48:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	incl	(%rbx)
	movl	4(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB5_2
# BB#1:
	decl	%eax
	movl	%eax, 4(%rdi)
	addq	$40, %rdi
	movq	%rbx, %rsi
	callq	addList
	movl	$3, 8(%rbx)
	addl	$3, 4(%rbx)
	popq	%rbx
	retq
.LBB5_2:
	addq	$16, %rdi
	movq	%rbx, %rsi
	popq	%rbx
	jmp	addList                 # TAILCALL
.Lfunc_end5:
	.size	put_in_hosp, .Lfunc_end5-put_in_hosp
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_0:
	.long	1325400064              # float 2.14748365E+9
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_1:
	.quad	4604174014248933917     # double 0.66600000000000004
	.text
	.globl	generate_patient
	.p2align	4, 0x90
	.type	generate_patient,@function
generate_patient:                       # @generate_patient
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 16
.Lcfi50:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	184(%rbx), %rdi
	callq	my_rand
	cvtss2sd	%xmm0, %xmm1
	mulss	.LCPI6_0(%rip), %xmm0
	cvttss2si	%xmm0, %rax
	movq	%rax, 184(%rbx)
	ucomisd	.LCPI6_1(%rip), %xmm1
	jbe	.LBB6_1
# BB#2:
	movl	$24, %edi
	callq	malloc
	movl	$0, (%rax)
	movl	$0, 4(%rax)
	movl	$0, 8(%rax)
	movq	%rbx, 16(%rax)
	popq	%rbx
	retq
.LBB6_1:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end6:
	.size	generate_patient, .Lfunc_end6-generate_patient
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 24
	subq	$56, %rsp
.Lcfi53:
	.cfi_def_cfa_offset 80
.Lcfi54:
	.cfi_offset %rbx, -24
.Lcfi55:
	.cfi_offset %r14, -16
	callq	dealwithargs
	movl	max_level(%rip), %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	alloc_tree
	movq	%rax, %r14
	movl	$.Lstr, %edi
	callq	puts
	movl	$.Lstr.1, %edi
	callq	puts
	cmpq	$0, max_time(%rip)
	jle	.LBB7_5
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %rax
	imulq	$1374389535, %rax, %rcx # imm = 0x51EB851F
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$36, %rcx
	addl	%edx, %ecx
	imull	$50, %ecx, %ecx
	cmpl	%ecx, %eax
	jne	.LBB7_4
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
.LBB7_4:                                #   in Loop: Header=BB7_2 Depth=1
	movq	%r14, %rdi
	callq	sim
	incq	%rbx
	cmpq	max_time(%rip), %rbx
	jl	.LBB7_2
.LBB7_5:                                # %._crit_edge
	movl	$.Lstr.2, %edi
	callq	puts
	movq	%r14, %rdi
	callq	get_results
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movl	$.Lstr.3, %edi
	callq	puts
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.5, %edi
	movb	$1, %al
	callq	printf
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	divss	16(%rsp), %xmm0         # 16-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.6, %edi
	movb	$1, %al
	callq	printf
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	divss	16(%rsp), %xmm0         # 16-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.7, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	main, .Lfunc_end7-main
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_0:
	.long	1325400064              # float 2.14748365E+9
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI8_1:
	.quad	4604174014248933917     # double 0.66600000000000004
	.text
	.globl	sim
	.p2align	4, 0x90
	.type	sim,@function
sim:                                    # @sim
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi62:
	.cfi_def_cfa_offset 112
.Lcfi63:
	.cfi_offset %rbx, -56
.Lcfi64:
	.cfi_offset %r12, -48
.Lcfi65:
	.cfi_offset %r13, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	testq	%r13, %r13
	je	.LBB8_1
# BB#2:                                 # %.preheader54.preheader
	movq	24(%r13), %rdi
	callq	sim
	movq	%rax, %r12
	movq	%r12, 40(%rsp)
	movq	16(%r13), %rdi
	callq	sim
	movq	%rax, 32(%rsp)
	movq	8(%r13), %rdi
	callq	sim
	movq	%rax, 24(%rsp)
	movq	(%r13), %rdi
	callq	sim
	movq	%rax, 16(%rsp)
	leaq	104(%r13), %r14
	leaq	80(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$3, %ebp
	testq	%r12, %r12
	jne	.LBB8_4
	jmp	.LBB8_10
	.p2align	4, 0x90
.LBB8_11:                               # %.loopexit._crit_edge
                                        #   in Loop: Header=BB8_10 Depth=1
	movq	8(%rsp,%rbp,8), %r12
	decq	%rbp
	testq	%r12, %r12
	je	.LBB8_10
.LBB8_4:                                # %.preheader
	movq	(%r12), %r15
	testq	%r15, %r15
	jne	.LBB8_6
	jmp	.LBB8_10
	.p2align	4, 0x90
.LBB8_9:                                # %put_in_hosp.exit52
                                        #   in Loop: Header=BB8_6 Depth=1
	movq	8(%r15), %rsi
	movq	%r12, %rdi
	callq	removeList
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.LBB8_10
.LBB8_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %rbx
	incl	(%rbx)
	movl	68(%r13), %eax
	testl	%eax, %eax
	jle	.LBB8_8
# BB#7:                                 #   in Loop: Header=BB8_6 Depth=1
	decl	%eax
	movl	%eax, 68(%r13)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	addList
	movl	$3, 8(%rbx)
	addl	$3, 4(%rbx)
	jmp	.LBB8_9
	.p2align	4, 0x90
.LBB8_8:                                #   in Loop: Header=BB8_6 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbx, %rsi
	callq	addList
	jmp	.LBB8_9
	.p2align	4, 0x90
.LBB8_10:                               # %.loopexit
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbp, %rbp
	jg	.LBB8_11
# BB#12:
	movq	128(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB8_17
# BB#13:                                # %.lr.ph.i
	movq	%r13, %r15
	subq	$-128, %r15
	leaq	40(%r13), %r12
	.p2align	4, 0x90
.LBB8_14:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rbp
	decl	8(%rbp)
	jne	.LBB8_16
# BB#15:                                #   in Loop: Header=BB8_14 Depth=1
	incl	68(%r13)
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	removeList
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	addList
.LBB8_16:                               #   in Loop: Header=BB8_14 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_14
.LBB8_17:                               # %check_patients_inside.exit
	movq	104(%r13), %rsi
	movq	%r13, %rdi
	callq	check_patients_assess
	movq	%rax, %r12
	movq	80(%r13), %rbp
	testq	%rbp, %rbp
	jne	.LBB8_19
	jmp	.LBB8_23
	.p2align	4, 0x90
.LBB8_20:                               #   in Loop: Header=BB8_19 Depth=1
	decl	%eax
	movl	%eax, 68(%r13)
	movl	$3, 8(%rbx)
	addl	$3, 4(%rbx)
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbx, %rsi
	callq	removeList
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	addList
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB8_23
.LBB8_19:                               # %.lr.ph.i53
                                        # =>This Inner Loop Header: Depth=1
	movl	68(%r13), %eax
	movq	8(%rbp), %rbx
	testl	%eax, %eax
	jg	.LBB8_20
# BB#21:                                #   in Loop: Header=BB8_19 Depth=1
	incl	4(%rbx)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB8_19
.LBB8_23:                               # %check_patients_waiting.exit
	movq	184(%r13), %rdi
	callq	my_rand
	cvtss2sd	%xmm0, %xmm1
	mulss	.LCPI8_0(%rip), %xmm0
	cvttss2si	%xmm0, %rax
	movq	%rax, 184(%r13)
	ucomisd	.LCPI8_1(%rip), %xmm1
	jbe	.LBB8_28
# BB#24:                                # %generate_patient.exit
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	$0, (%rbx)
	movl	$0, 4(%rbx)
	movl	$0, 8(%rbx)
	movq	%r13, 16(%rbx)
	testq	%rbx, %rbx
	je	.LBB8_28
# BB#25:
	movl	$1, (%rbx)
	movl	68(%r13), %eax
	testl	%eax, %eax
	jle	.LBB8_27
# BB#26:
	decl	%eax
	movl	%eax, 68(%r13)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	addList
	movl	$3, 8(%rbx)
	addl	$3, 4(%rbx)
	jmp	.LBB8_28
.LBB8_1:
	xorl	%r12d, %r12d
.LBB8_28:                               # %put_in_hosp.exit
	movq	%r12, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_27:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbx, %rsi
	callq	addList
	jmp	.LBB8_28
.Lfunc_end8:
	.size	sim, .Lfunc_end8-sim
	.cfi_endproc

	.type	seed,@object            # @seed
	.comm	seed,8,8
	.type	max_level,@object       # @max_level
	.comm	max_level,4,4
	.type	max_time,@object        # @max_time
	.comm	max_time,8,8
	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"%d\n"
	.size	.L.str.2, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"# of people treated:              %f people\n"
	.size	.L.str.5, 45

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Average length of stay:           %0.2f time units\n"
	.size	.L.str.6, 52

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Average # of hospitals visited:   %f hospitals\n\n"
	.size	.L.str.7, 49

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"\n\n    Columbian Health Care Simulator\n"
	.size	.Lstr, 39

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.1:
	.asciz	"Working..."
	.size	.Lstr.1, 11

	.type	.Lstr.2,@object         # @str.2
.Lstr.2:
	.asciz	"Getting Results"
	.size	.Lstr.2, 16

	.type	.Lstr.3,@object         # @str.3
.Lstr.3:
	.asciz	"Done.\n"
	.size	.Lstr.3, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
