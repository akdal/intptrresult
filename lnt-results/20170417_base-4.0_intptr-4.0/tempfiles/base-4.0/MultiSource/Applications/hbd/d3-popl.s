	.text
	.file	"d3-popl.bc"
	.globl	_Z10storelocalP9Classfile
	.p2align	4, 0x90
	.type	_Z10storelocalP9Classfile,@function
_Z10storelocalP9Classfile:              # @_Z10storelocalP9Classfile
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	currpc(%rip), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	ch(%rip), %r14d
	cmpl	$58, %r14d
	jg	.LBB0_2
# BB#1:                                 # %switch.lookup
	leal	-54(%r14), %eax
	addl	$-50, %r14d
	xorl	%r15d, %r15d
	cmpl	$5, %eax
	cmovbl	%r14d, %r15d
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	1(%rax), %eax
	movl	%eax, currpc(%rip)
	decl	bufflength(%rip)
	movq	inbuff(%rip), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, inbuff(%rip)
	movsbl	(%rax), %r14d
	jmp	.LBB0_11
.LBB0_2:
	cmpl	$62, %r14d
	jg	.LBB0_4
# BB#3:
	addl	$-59, %r14d
	movl	$4, %r15d
	jmp	.LBB0_11
.LBB0_4:
	cmpl	$66, %r14d
	jg	.LBB0_6
# BB#5:
	addl	$-63, %r14d
	movl	$5, %r15d
	jmp	.LBB0_11
.LBB0_6:
	cmpl	$70, %r14d
	jg	.LBB0_8
# BB#7:
	addl	$-67, %r14d
	movl	$6, %r15d
	jmp	.LBB0_11
.LBB0_8:
	cmpl	$74, %r14d
	jg	.LBB0_10
# BB#9:
	addl	$-71, %r14d
	movl	$7, %r15d
	jmp	.LBB0_11
.LBB0_10:
	addl	$-75, %r14d
	movl	$8, %r15d
.LBB0_11:
	movq	miptr(%rip), %rax
	movq	88(%rax), %rbp
	movslq	%r14d, %rbx
	movq	(%rbp,%rbx,8), %r13
	testq	%r13, %r13
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%r13, 32(%rsp)          # 8-byte Spill
	je	.LBB0_13
# BB#12:
	movq	%r13, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, %r12
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	strcpy
	movq	miptr(%rip), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	112(%rax), %rax
	movl	(%rax,%rbx,4), %r15d
	jmp	.LBB0_16
.LBB0_13:
	movl	$7, %edi
	callq	_Znam
	movq	%rax, %r12
	movq	%r12, (%rbp,%rbx,8)
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	%ebx, %edx
	callq	sprintf
	movq	miptr(%rip), %rdx
	movq	112(%rdx), %rax
	movl	(%rax,%rbx,4), %ecx
	cmpl	$13, %ecx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	je	.LBB0_15
# BB#14:
	testl	%ecx, %ecx
	jne	.LBB0_16
.LBB0_15:
	cmpl	$4, %r15d
	movl	$13, %ecx
	cmovnel	%r15d, %ecx
	movl	%ecx, (%rax,%rbx,4)
.LBB0_16:
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp0:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp1:
# BB#17:                                # %.noexc
	movq	16(%rsp), %rax          # 8-byte Reload
	decl	%eax
	movq	%r12, (%rbp)
	movl	%r15d, 8(%rbp)
	movl	$3, 12(%rbp)
	movl	%r14d, 16(%rbp)
	movl	$1, 8(%r13)
	movl	%eax, 16(%r13)
	movq	%rax, %r14
	movl	%eax, 12(%r13)
.Ltmp2:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp3:
# BB#18:
	movl	$0, (%rbx)
	movl	$1, 4(%rbx)
	movl	%r15d, 8(%rbx)
	movl	$39, 12(%rbx)
	movq	%rbp, 16(%rbx)
	movq	%rbx, (%r13)
	movq	stkptr(%rip), %rax
	leaq	-8(%rax), %rcx
	movq	%rcx, stkptr(%rip)
	movq	-8(%rax), %rbp
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r12
	movq	%r14, %rax
	movl	%r15d, %r14d
	movl	16(%rbp), %r15d
	cmpl	%r15d, %eax
	cmovbl	%eax, %r15d
	movl	$1, 8(%r12)
	movl	%eax, 12(%r12)
	movl	%r15d, 16(%r12)
.Ltmp5:
	movl	$24, %edi
	callq	_Znwm
.Ltmp6:
# BB#19:
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	movl	$0, (%rax)
	movl	$4, 4(%rax)
	movl	%r14d, 8(%rax)
	movl	$6, 12(%rax)
	movq	%rax, (%r12)
	movq	%r13, 24(%r12)
	movq	%rbp, 32(%r12)
	jne	.LBB0_21
# BB#20:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	104(%rax), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%r15d, (%rax,%rcx,4)
.LBB0_21:
	movq	(%rbp), %rax
	cmpl	$4, 8(%rax)
	jne	.LBB0_26
# BB#22:
	cmpl	$10, 8(%rbx)
	jne	.LBB0_26
# BB#23:
	movl	$std_exps+48, %ecx
	cmpq	%rcx, %rax
	je	.LBB0_25
# BB#24:
	movl	$std_exps+72, %ecx
	cmpq	%rcx, %rax
	jne	.LBB0_26
.LBB0_25:
	addq	$312, %rax              # imm = 0x138
	movq	%rax, (%rbp)
.LBB0_26:
	movq	donestkptr(%rip), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, donestkptr(%rip)
	movq	%r12, (%rax)
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_29:
.Ltmp7:
	movq	%rax, %rbx
	movq	%r12, %rdi
	jmp	.LBB0_28
.LBB0_27:
.Ltmp4:
	movq	%rax, %rbx
	movq	%r13, %rdi
.LBB0_28:
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z10storelocalP9Classfile, .Lfunc_end0-_Z10storelocalP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp5-.Ltmp3           #   Call between .Ltmp3 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end0-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	4                       # 0x4
	.long	39                      # 0x27
	.text
	.globl	_Z9iinclocalP9Classfile
	.p2align	4, 0x90
	.type	_Z9iinclocalP9Classfile,@function
_Z9iinclocalP9Classfile:                # @_Z9iinclocalP9Classfile
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	currpc(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, currpc(%rip)
	movl	bufflength(%rip), %ecx
	leal	-1(%rcx), %edx
	movl	%edx, bufflength(%rip)
	movq	inbuff(%rip), %rsi
	leaq	1(%rsi), %rdx
	movq	%rdx, inbuff(%rip)
	movsbq	(%rsi), %r13
	movq	miptr(%rip), %rdx
	movq	88(%rdx), %rdi
	movq	(%rdi,%r13,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_30
# BB#1:
	movq	112(%rdx), %rdi
	movl	(%rdi,%r13,4), %edx
	cmpq	$13, %rdx
	jne	.LBB1_14
# BB#2:                                 # %.thread
	movl	$4, (%rdi,%r13,4)
	jmp	.LBB1_3
.LBB1_30:
	movl	$.Lstr, %edi
	callq	puts
	jmp	.LBB1_31
.LBB1_14:
	leal	-3(%rdx), %edi
	cmpl	$2, %edi
	jae	.LBB1_15
.LBB1_3:
	leal	-1(%rax), %r12d
	addl	$2, %eax
	movl	%eax, currpc(%rip)
	addl	$-2, %ecx
	movl	%ecx, bufflength(%rip)
	leaq	2(%rsi), %rax
	movq	%rax, inbuff(%rip)
	movsbq	1(%rsi), %r14
	cmpq	$-1, %r14
	movl	%r12d, 4(%rsp)          # 4-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	je	.LBB1_20
# BB#4:
	cmpb	$1, %r14b
	jne	.LBB1_5
.LBB1_20:
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp8:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp9:
# BB#21:                                # %.noexc78
	movq	%rbp, (%rbx)
	movl	$4, 8(%rbx)
	movl	$3, 12(%rbx)
	movl	%r13d, 16(%rbx)
	movl	$1, 8(%r15)
	movl	%r12d, 16(%r15)
	movl	%r12d, 12(%r15)
.Ltmp10:
	movl	$24, %edi
	callq	_Znwm
.Ltmp11:
# BB#22:
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [0,1,4,39]
	movups	%xmm0, (%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, (%r15)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r13
	xorl	%ebp, %ebp
	testb	%r14b, %r14b
	sets	%r14b
	movl	$1, 8(%r13)
	movl	%r12d, 16(%r13)
	movl	%r12d, 12(%r13)
.Ltmp13:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp14:
# BB#23:
	movb	%r14b, %bpl
	orl	$22, %ebp
	movl	$2, %eax
	xorl	%r12d, %r12d
	jmp	.LBB1_11
.LBB1_15:
	movq	type2str(,%rdx,8), %rcx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	printf
.LBB1_31:
	movl	$1, %eax
	jmp	.LBB1_32
.LBB1_5:
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp16:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %r12
.Ltmp17:
# BB#6:                                 # %.noexc
	movq	%rbp, (%r12)
	movl	$4, 8(%r12)
	movl	$3, 12(%r12)
	movl	%r13d, 16(%r12)
	movl	$1, 8(%r15)
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 16(%r15)
	movl	%eax, 12(%r15)
.Ltmp18:
	movl	$24, %edi
	callq	_Znwm
.Ltmp19:
# BB#7:
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [0,1,4,39]
	movups	%xmm0, (%rax)
	movq	%r12, 16(%rax)
	movq	%rax, (%r15)
	movl	$5, %edi
	callq	_Znam
	movq	%rax, %r13
	movq	%r14, %rdx
	negq	%rdx
	testb	%r14b, %r14b
	movq	%r14, 8(%rsp)           # 8-byte Spill
	cmovnsq	%r14, %rdx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	sprintf
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r12
.Ltmp21:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp22:
	movl	4(%rsp), %r14d          # 4-byte Reload
# BB#8:                                 # %.noexc74
	movq	%r13, (%rbx)
	movl	$4, 8(%rbx)
	movl	$1, 12(%rbx)
	movl	$0, 16(%rbx)
	movl	$1, 8(%r12)
	movl	%r14d, 16(%r12)
	movl	%r14d, 12(%r12)
.Ltmp23:
	movl	$24, %edi
	callq	_Znwm
.Ltmp24:
# BB#9:
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [0,1,4,39]
	movups	%xmm0, (%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, (%r12)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r13
	xorl	%ebp, %ebp
	cmpb	$0, 8(%rsp)             # 1-byte Folded Reload
	sets	8(%rsp)                 # 1-byte Folded Spill
	movl	$1, 8(%r13)
	movl	%r14d, 16(%r13)
	movl	%r14d, 12(%r13)
.Ltmp26:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp27:
# BB#10:
	movb	8(%rsp), %al            # 1-byte Reload
	movb	%al, %bpl
	orl	$20, %ebp
	movl	$4, %eax
.LBB1_11:                               # %_ZN3ExpC2Ej7Exptype4Type2OpPS_S3_.exit82
	movl	$0, (%rbx)
	movl	%eax, 4(%rbx)
	movl	$4, 8(%rbx)
	movl	%ebp, 12(%rbx)
	movq	%rbx, (%r13)
	movq	%r15, 24(%r13)
	movq	%r12, 32(%r13)
	cmpl	$4, lastaction(%rip)
	jne	.LBB1_26
# BB#12:
	movq	stkptr(%rip), %rbp
	movq	-8(%rbp), %r14
	movq	(%r14), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_13
.LBB1_26:
	movq	donestkptr(%rip), %rbp
	leaq	8(%rbp), %rax
	movq	%rax, donestkptr(%rip)
.LBB1_27:
	movq	%r13, (%rbp)
	xorl	%eax, %eax
.LBB1_32:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_13:
	addq	$-8, %rbp
	movl	$3, 4(%rbx)
	movl	4(%rsp), %eax           # 4-byte Reload
	cmpl	16(%r14), %eax
	cmovbq	%r13, %r14
	movl	16(%r14), %eax
	movl	%eax, 16(%r13)
	jmp	.LBB1_27
.LBB1_18:
.Ltmp28:
	jmp	.LBB1_19
.LBB1_28:
.Ltmp25:
	movq	%rax, %rbx
	movq	%r12, %rdi
	jmp	.LBB1_29
.LBB1_16:
.Ltmp20:
	jmp	.LBB1_17
.LBB1_25:
.Ltmp15:
.LBB1_19:
	movq	%rax, %rbx
	movq	%r13, %rdi
	jmp	.LBB1_29
.LBB1_24:
.Ltmp12:
.LBB1_17:
	movq	%rax, %rbx
	movq	%r15, %rdi
.LBB1_29:
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_Z9iinclocalP9Classfile, .Lfunc_end1-_Z9iinclocalP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\222\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp8-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp11-.Ltmp8          #   Call between .Ltmp8 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin1   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp13-.Ltmp11         #   Call between .Ltmp11 and .Ltmp13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin1   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp16-.Ltmp14         #   Call between .Ltmp14 and .Ltmp16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp19-.Ltmp16         #   Call between .Ltmp16 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin1   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp21-.Ltmp19         #   Call between .Ltmp19 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp24-.Ltmp21         #   Call between .Ltmp21 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin1   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp26-.Ltmp24         #   Call between .Ltmp24 and .Ltmp26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin1   #     jumps to .Ltmp28
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin1   # >> Call Site 11 <<
	.long	.Lfunc_end1-.Ltmp27     #   Call between .Ltmp27 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"var%d"
	.size	.L.str, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Incrementation of local var%d of type %d i.e. %s.\n"
	.size	.L.str.2, 51

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%ld"
	.size	.L.str.3, 4

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Local int used before defined."
	.size	.Lstr, 31


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
