	.text
	.file	"MyString.bc"
	.globl	_Z9CharPrevAPKcS0_
	.p2align	4, 0x90
	.type	_Z9CharPrevAPKcS0_,@function
_Z9CharPrevAPKcS0_:                     # @_Z9CharPrevAPKcS0_
	.cfi_startproc
# BB#0:
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rax
	cmpq	%rsi, %rax
	jae	.LBB0_4
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	leaq	1(%rax), %rdi
	cmpq	%rsi, %rdi
	jb	.LBB0_1
.LBB0_4:                                # %.critedge
	retq
.Lfunc_end0:
	.size	_Z9CharPrevAPKcS0_, .Lfunc_end0-_Z9CharPrevAPKcS0_
	.cfi_endproc

	.globl	_Z9CharNextAPKc
	.p2align	4, 0x90
	.type	_Z9CharNextAPKc,@function
_Z9CharNextAPKc:                        # @_Z9CharNextAPKc
	.cfi_startproc
# BB#0:
	leaq	1(%rdi), %rax
	cmpb	$0, (%rdi)
	cmoveq	%rdi, %rax
	retq
.Lfunc_end1:
	.size	_Z9CharNextAPKc, .Lfunc_end1-_Z9CharNextAPKc
	.cfi_endproc

	.globl	_Z11MyCharLowerc
	.p2align	4, 0x90
	.type	_Z11MyCharLowerc,@function
_Z11MyCharLowerc:                       # @_Z11MyCharLowerc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movzbl	%bl, %eax
	movl	%eax, %ecx
	subl	$-128, %ecx
	cmpl	$383, %ecx              # imm = 0x17F
	ja	.LBB2_2
# BB#1:
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movzbl	%bl, %ecx
	movl	(%rax,%rcx,4), %eax
.LBB2_2:                                # %tolower.exit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end2:
	.size	_Z11MyCharLowerc, .Lfunc_end2-_Z11MyCharLowerc
	.cfi_endproc

	.globl	_Z11MyCharLowerw
	.p2align	4, 0x90
	.type	_Z11MyCharLowerw,@function
_Z11MyCharLowerw:                       # @_Z11MyCharLowerw
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 16
	callq	towlower
	popq	%rcx
	retq
.Lfunc_end3:
	.size	_Z11MyCharLowerw, .Lfunc_end3-_Z11MyCharLowerw
	.cfi_endproc

	.globl	_Z13MyStringLowerPc
	.p2align	4, 0x90
	.type	_Z13MyStringLowerPc,@function
_Z13MyStringLowerPc:                    # @_Z13MyStringLowerPc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 32
.Lcfi6:
	.cfi_offset %rbx, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB4_1
# BB#2:                                 # %.preheader
	movb	(%r14), %al
	testb	%al, %al
	je	.LBB4_7
# BB#3:                                 # %.lr.ph.preheader
	leaq	1(%r14), %rbx
	.p2align	4, 0x90
.LBB4_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%al, %r15d
	movl	%r15d, %eax
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB4_6
# BB#5:                                 #   in Loop: Header=BB4_4 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movl	(%rax,%r15,4), %r15d
.LBB4_6:                                # %_Z11MyCharLowerc.exit
                                        #   in Loop: Header=BB4_4 Depth=1
	movb	%r15b, -1(%rbx)
	movzbl	(%rbx), %eax
	incq	%rbx
	testb	%al, %al
	jne	.LBB4_4
	jmp	.LBB4_7
.LBB4_1:
	xorl	%r14d, %r14d
.LBB4_7:                                # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	_Z13MyStringLowerPc, .Lfunc_end4-_Z13MyStringLowerPc
	.cfi_endproc

	.globl	_Z13MyStringLowerPw
	.p2align	4, 0x90
	.type	_Z13MyStringLowerPw,@function
_Z13MyStringLowerPw:                    # @_Z13MyStringLowerPw
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -24
.Lcfi13:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB5_1
# BB#2:                                 # %.preheader
	movl	(%r14), %edi
	testl	%edi, %edi
	je	.LBB5_5
# BB#3:                                 # %.lr.ph.preheader
	leaq	4(%r14), %rbx
	.p2align	4, 0x90
.LBB5_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	callq	towlower
	movl	%eax, -4(%rbx)
	movl	(%rbx), %edi
	addq	$4, %rbx
	testl	%edi, %edi
	jne	.LBB5_4
	jmp	.LBB5_5
.LBB5_1:
	xorl	%r14d, %r14d
.LBB5_5:                                # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	_Z13MyStringLowerPw, .Lfunc_end5-_Z13MyStringLowerPw
	.cfi_endproc

	.globl	_Z11MyCharUpperw
	.p2align	4, 0x90
	.type	_Z11MyCharUpperw,@function
_Z11MyCharUpperw:                       # @_Z11MyCharUpperw
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 16
	callq	towupper
	popq	%rcx
	retq
.Lfunc_end6:
	.size	_Z11MyCharUpperw, .Lfunc_end6-_Z11MyCharUpperw
	.cfi_endproc

	.globl	_Z13MyStringUpperPw
	.p2align	4, 0x90
	.type	_Z13MyStringUpperPw,@function
_Z13MyStringUpperPw:                    # @_Z13MyStringUpperPw
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB7_1
# BB#2:                                 # %.preheader
	movl	(%r14), %edi
	testl	%edi, %edi
	je	.LBB7_5
# BB#3:                                 # %.lr.ph.preheader
	leaq	4(%r14), %rbx
	.p2align	4, 0x90
.LBB7_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	callq	towupper
	movl	%eax, -4(%rbx)
	movl	(%rbx), %edi
	addq	$4, %rbx
	testl	%edi, %edi
	jne	.LBB7_4
	jmp	.LBB7_5
.LBB7_1:
	xorl	%r14d, %r14d
.LBB7_5:                                # %.loopexit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	_Z13MyStringUpperPw, .Lfunc_end7-_Z13MyStringUpperPw
	.cfi_endproc

	.globl	_Z15MyStringComparePKcS0_
	.p2align	4, 0x90
	.type	_Z15MyStringComparePKcS0_,@function
_Z15MyStringComparePKcS0_:              # @_Z15MyStringComparePKcS0_
	.cfi_startproc
# BB#0:
	.p2align	4, 0x90
.LBB8_1:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %eax
	incq	%rdi
	cmpb	(%rsi), %al
	leaq	1(%rsi), %rsi
	jb	.LBB8_2
# BB#3:                                 #   in Loop: Header=BB8_1 Depth=1
	ja	.LBB8_4
# BB#5:                                 #   in Loop: Header=BB8_1 Depth=1
	testb	%al, %al
	jne	.LBB8_1
# BB#6:
	xorl	%eax, %eax
	retq
.LBB8_2:
	movl	$-1, %eax
	retq
.LBB8_4:
	movl	$1, %eax
	retq
.Lfunc_end8:
	.size	_Z15MyStringComparePKcS0_, .Lfunc_end8-_Z15MyStringComparePKcS0_
	.cfi_endproc

	.globl	_Z15MyStringComparePKwS0_
	.p2align	4, 0x90
	.type	_Z15MyStringComparePKwS0_,@function
_Z15MyStringComparePKwS0_:              # @_Z15MyStringComparePKwS0_
	.cfi_startproc
# BB#0:
	.p2align	4, 0x90
.LBB9_1:                                # =>This Inner Loop Header: Depth=1
	movl	(%rdi), %eax
	addq	$4, %rdi
	cmpl	(%rsi), %eax
	leaq	4(%rsi), %rsi
	jl	.LBB9_2
# BB#3:                                 #   in Loop: Header=BB9_1 Depth=1
	jg	.LBB9_4
# BB#5:                                 #   in Loop: Header=BB9_1 Depth=1
	testl	%eax, %eax
	jne	.LBB9_1
# BB#6:
	xorl	%eax, %eax
	retq
.LBB9_2:
	movl	$-1, %eax
	retq
.LBB9_4:
	movl	$1, %eax
	retq
.Lfunc_end9:
	.size	_Z15MyStringComparePKwS0_, .Lfunc_end9-_Z15MyStringComparePKwS0_
	.cfi_endproc

	.globl	_Z21MyStringCompareNoCasePKwS0_
	.p2align	4, 0x90
	.type	_Z21MyStringCompareNoCasePKwS0_,@function
_Z21MyStringCompareNoCasePKwS0_:        # @_Z21MyStringCompareNoCasePKwS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 48
.Lcfi25:
	.cfi_offset %rbx, -48
.Lcfi26:
	.cfi_offset %r12, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB10_1:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %r15d
	movl	(%r14), %ebp
	cmpl	%ebp, %r15d
	je	.LBB10_6
# BB#2:                                 #   in Loop: Header=BB10_1 Depth=1
	movl	%r15d, %edi
	callq	towupper
	movl	%eax, %r12d
	movl	%ebp, %edi
	callq	towupper
	cmpl	%eax, %r12d
	jl	.LBB10_3
# BB#4:                                 #   in Loop: Header=BB10_1 Depth=1
	jg	.LBB10_5
.LBB10_6:                               #   in Loop: Header=BB10_1 Depth=1
	addq	$4, %rbx
	addq	$4, %r14
	testl	%r15d, %r15d
	jne	.LBB10_1
# BB#7:
	xorl	%eax, %eax
	jmp	.LBB10_8
.LBB10_3:
	movl	$-1, %eax
	jmp	.LBB10_8
.LBB10_5:
	movl	$1, %eax
.LBB10_8:                               # %.thread
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_Z21MyStringCompareNoCasePKwS0_, .Lfunc_end10-_Z21MyStringCompareNoCasePKwS0_
	.cfi_endproc

	.globl	_Z21MyStringCompareNoCasePKcS0_
	.p2align	4, 0x90
	.type	_Z21MyStringCompareNoCasePKcS0_,@function
_Z21MyStringCompareNoCasePKcS0_:        # @_Z21MyStringCompareNoCasePKcS0_
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 48
	subq	$64, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 112
.Lcfi36:
	.cfi_offset %rbx, -48
.Lcfi37:
	.cfi_offset %r12, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	$-1, %ebp
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB11_1:                               # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB11_1
# BB#2:                                 # %_Z11MyStringLenIcEiPKT_.exit.i
	leal	1(%rbp), %eax
	movslq	%eax, %r15
	cmpl	$-1, %ebp
	movq	$-1, %rdi
	cmovgeq	%r15, %rdi
	callq	_Znam
	movq	%rax, 16(%rsp)
	movb	$0, (%rax)
	movl	%r15d, 28(%rsp)
	.p2align	4, 0x90
.LBB11_3:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %ecx
	incq	%rbx
	movb	%cl, (%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB11_3
# BB#4:                                 # %_ZN11CStringBaseIcEC2EPKc.exit
	movl	%ebp, 24(%rsp)
.Ltmp0:
	leaq	32(%rsp), %rdi
	leaq	16(%rsp), %rsi
	xorl	%edx, %edx
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp1:
# BB#5:
	movq	32(%rsp), %r15
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %ebx
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB11_6:                               # =>This Inner Loop Header: Depth=1
	incl	%ebx
	cmpb	$0, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB11_6
# BB#7:                                 # %_Z11MyStringLenIcEiPKT_.exit.i7
	leal	1(%rbx), %ebp
	movslq	%ebp, %rax
	cmpl	$-1, %ebx
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
.Ltmp3:
	callq	_Znam
.Ltmp4:
# BB#8:                                 # %.noexc
	movq	%rax, (%rsp)
	movb	$0, (%rax)
	movl	%ebp, 12(%rsp)
	.p2align	4, 0x90
.LBB11_9:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i10
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14), %ecx
	incq	%r14
	movb	%cl, (%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB11_9
# BB#10:
	movl	%ebx, 8(%rsp)
.Ltmp6:
	leaq	48(%rsp), %rdi
	movq	%rsp, %rsi
	xorl	%edx, %edx
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp7:
# BB#11:
	movq	48(%rsp), %rbx
	.p2align	4, 0x90
.LBB11_12:                              # =>This Inner Loop Header: Depth=1
	movl	(%r15), %r12d
	movl	(%rbx), %ebp
	cmpl	%ebp, %r12d
	je	.LBB11_17
# BB#13:                                #   in Loop: Header=BB11_12 Depth=1
	movl	%r12d, %edi
	callq	towupper
	movl	%eax, %r14d
	movl	%ebp, %edi
	callq	towupper
	cmpl	%eax, %r14d
	jl	.LBB11_14
# BB#15:                                #   in Loop: Header=BB11_12 Depth=1
	jg	.LBB11_16
.LBB11_17:                              #   in Loop: Header=BB11_12 Depth=1
	addq	$4, %r15
	addq	$4, %rbx
	testl	%r12d, %r12d
	jne	.LBB11_12
# BB#18:
	xorl	%ebx, %ebx
	jmp	.LBB11_19
.LBB11_14:
	movl	$-1, %ebx
	jmp	.LBB11_19
.LBB11_16:
	movl	$1, %ebx
.LBB11_19:                              # %_Z21MyStringCompareNoCasePKwS0_.exit
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_21
# BB#20:
	callq	_ZdaPv
.LBB11_21:                              # %_ZN11CStringBaseIwED2Ev.exit12
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_23
# BB#22:
	callq	_ZdaPv
.LBB11_23:                              # %_ZN11CStringBaseIcED2Ev.exit13
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_25
# BB#24:
	callq	_ZdaPv
.LBB11_25:                              # %_ZN11CStringBaseIwED2Ev.exit14
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_27
# BB#26:
	callq	_ZdaPv
.LBB11_27:                              # %_ZN11CStringBaseIcED2Ev.exit15
	movl	%ebx, %eax
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_30:
.Ltmp8:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_32
# BB#31:
	callq	_ZdaPv
	jmp	.LBB11_32
.LBB11_29:
.Ltmp5:
	movq	%rax, %rbx
.LBB11_32:                              # %_ZN11CStringBaseIcED2Ev.exit16
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_34
# BB#33:
	callq	_ZdaPv
	jmp	.LBB11_34
.LBB11_28:
.Ltmp2:
	movq	%rax, %rbx
.LBB11_34:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_36
# BB#35:
	callq	_ZdaPv
.LBB11_36:                              # %_ZN11CStringBaseIcED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_Z21MyStringCompareNoCasePKcS0_, .Lfunc_end11-_Z21MyStringCompareNoCasePKcS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end11-.Ltmp7     #   Call between .Ltmp7 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
