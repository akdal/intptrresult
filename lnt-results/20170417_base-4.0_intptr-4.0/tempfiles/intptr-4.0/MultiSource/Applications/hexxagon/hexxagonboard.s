	.text
	.file	"hexxagonboard.bc"
	.globl	_Z16getHexxagonIndexii
	.p2align	4, 0x90
	.type	_Z16getHexxagonIndexii,@function
_Z16getHexxagonIndexii:                 # @_Z16getHexxagonIndexii
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	leal	-1(%rdi), %ecx
	movl	$-1, %eax
	cmpl	$8, %ecx
	ja	.LBB0_7
# BB#1:
	leal	-1(%rsi), %ecx
	cmpl	$8, %ecx
	ja	.LBB0_7
# BB#2:
	cmpl	$4, %esi
	jg	.LBB0_4
# BB#3:
	leal	4(%rsi), %ecx
	cmpl	%edi, %ecx
	jl	.LBB0_7
.LBB0_4:
	cmpl	$6, %esi
	setl	%cl
	leal	-5(%rsi), %edx
	cmpl	%edi, %edx
	jl	.LBB0_6
# BB#5:
	testb	%cl, %cl
	je	.LBB0_7
.LBB0_6:
	xorl	%eax, %eax
	cmpl	$5, %esi
	setg	%al
	leal	(%rsi,%rsi,8), %ecx
	addl	%edi, %ecx
	xorl	%edx, %edx
	cmpl	$2, %esi
	setl	%dl
	leal	-14(%rcx,%rdx,4), %edi
	leal	-17(%rcx,%rdx,4), %ecx
	cmovlel	%edi, %ecx
	leal	-2(%rcx), %edx
	cmpl	$3, %esi
	cmovlel	%ecx, %edx
	xorl	%ecx, %ecx
	cmpl	$4, %esi
	movl	$-1, %edi
	cmovlel	%ecx, %edi
	subl	%eax, %edi
	leal	(%rdi,%rdx), %eax
	cmpl	$6, %esi
	leal	-2(%rdi,%rdx), %ecx
	cmovlel	%eax, %ecx
	leal	-3(%rcx), %edx
	cmpl	$7, %esi
	cmovlel	%ecx, %edx
	leal	-4(%rdx), %eax
	cmpl	$8, %esi
	cmovlel	%edx, %eax
.LBB0_7:
	retq
.Lfunc_end0:
	.size	_Z16getHexxagonIndexii, .Lfunc_end0-_Z16getHexxagonIndexii
	.cfi_endproc

	.globl	_Z16initCloneLookupsv
	.p2align	4, 0x90
	.type	_Z16initCloneLookupsv,@function
_Z16initCloneLookupsv:                  # @_Z16initCloneLookupsv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 176
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	$488, %edi              # imm = 0x1E8
	callq	_Znam
	movq	%rax, clone_lookups(%rip)
	movq	$0, 112(%rsp)
	leaq	112(%rsp), %rbx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	_ZN10BitBoard64aSERKS_
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB1_1:                                # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	clone_lookups(%rip), %rdi
	addq	%rbp, %rdi
	movq	$0, 112(%rsp)
	movq	%rbx, %rsi
	callq	_ZN10BitBoard64aSERKS_
	addq	$8, %rbp
	cmpq	$488, %rbp              # imm = 0x1E8
	jne	.LBB1_1
# BB#2:                                 # %.preheader.preheader
	xorl	%r12d, %r12d
	movl	$1, %r13d
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
	leal	-1(%r13), %r8d
	leal	4(%r13), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	leal	-5(%r13), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	leal	-2(%r13), %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	leal	3(%r13), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	cmpl	$5, %r8d
	setg	%al
	cmpl	$6, %r8d
	setl	7(%rsp)                 # 1-byte Folded Spill
	leal	-6(%r13), %ecx
	movl	%ecx, 44(%rsp)          # 4-byte Spill
	xorl	%ecx, %ecx
	cmpl	$2, %r8d
	setl	%cl
	leal	(%r13,%r13,8), %edx
	leal	-23(%rdx,%rcx,4), %ecx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	cmpl	$4, %r8d
	movl	$0, %ecx
	movl	$-1, %edi
	cmovgl	%edi, %ecx
	subl	%eax, %ecx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	cmpl	$6, %r13d
	setl	6(%rsp)                 # 1-byte Folded Spill
	xorl	%eax, %eax
	cmpl	$2, %r13d
	setl	%al
	xorl	%ecx, %ecx
	cmpl	$4, %r13d
	movl	$0, %ebp
	cmovgl	%edi, %ebp
	setg	%cl
	xorl	%esi, %esi
	cmpl	$5, %r13d
	setg	%sil
	leal	-14(%rdx,%rax,4), %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	setl	5(%rsp)                 # 1-byte Folded Spill
	subl	%esi, %ebp
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	leal	1(%r13), %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	cmpl	$3, %r13d
	movl	$0, %eax
	cmovgl	%edi, %eax
	subl	%ecx, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leal	5(%r13), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	leal	-4(%r13), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	leal	-5(%r13,%r13,8), %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	$1, %r15d
	movl	%r8d, 32(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB1_4:                                #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$4, %r13d
	setg	%al
	cmpl	$6, %r13d
	setl	%cl
	cmpl	24(%rsp), %r15d         # 4-byte Folded Reload
	setg	%dl
	orb	%cl, %dl
	cmpl	28(%rsp), %r15d         # 4-byte Folded Reload
	setle	%cl
	cmpb	$1, %dl
	jne	.LBB1_6
# BB#5:                                 #   in Loop: Header=BB1_4 Depth=2
	orb	%cl, %al
	je	.LBB1_6
# BB#9:                                 # %.thread
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpl	$4, %r8d
	setg	%al
	movslq	%r12d, %rcx
	shlq	$3, %rcx
	movq	clone_lookups(%rip), %rdi
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	addq	%rcx, %rdi
	leal	-1(%r15), %ebp
	leal	-2(%r15), %ebx
	cmpl	%ebp, 48(%rsp)          # 4-byte Folded Reload
	setge	%dl
	cmpl	%ebp, 44(%rsp)          # 4-byte Folded Reload
	setl	%cl
	movl	$-1, %r14d
	cmpl	$8, 52(%rsp)            # 4-byte Folded Reload
	movl	$-1, %esi
	ja	.LBB1_14
# BB#10:                                # %.thread
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpl	$8, %ebx
	movl	$-1, %esi
	ja	.LBB1_14
# BB#11:                                # %.thread
                                        #   in Loop: Header=BB1_4 Depth=2
	orb	%al, %dl
	movl	$-1, %esi
	je	.LBB1_14
# BB#12:                                # %.thread
                                        #   in Loop: Header=BB1_4 Depth=2
	orb	7(%rsp), %cl            # 1-byte Folded Reload
	movl	$-1, %esi
	je	.LBB1_14
# BB#13:                                #   in Loop: Header=BB1_4 Depth=2
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	leal	(%rcx,%rbp), %eax
	cmpl	$2, %r8d
	leal	-3(%rcx,%rbp), %ecx
	cmovlel	%eax, %ecx
	leal	-2(%rcx), %eax
	cmpl	$3, %r8d
	cmovlel	%ecx, %eax
	movq	96(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	cmpl	$6, %r8d
	leal	-2(%rdx,%rax), %eax
	cmovlel	%ecx, %eax
	leal	-3(%rax), %esi
	cmpl	$7, %r8d
	cmovlel	%eax, %esi
.LBB1_14:                               # %_Z16getHexxagonIndexii.exit134
                                        #   in Loop: Header=BB1_4 Depth=2
	movl	%ebx, 60(%rsp)          # 4-byte Spill
	cmpl	$4, %r8d
	setg	%bl
	callq	_ZN10BitBoard646setBitEi
	movq	clone_lookups(%rip), %rdi
	addq	8(%rsp), %rdi           # 8-byte Folded Reload
	cmpl	%r15d, 48(%rsp)         # 4-byte Folded Reload
	setge	%cl
	cmpl	%r15d, 44(%rsp)         # 4-byte Folded Reload
	setl	%al
	cmpl	$8, 52(%rsp)            # 4-byte Folded Reload
	movl	%r12d, 20(%rsp)         # 4-byte Spill
	ja	.LBB1_18
# BB#15:                                # %_Z16getHexxagonIndexii.exit134
                                        #   in Loop: Header=BB1_4 Depth=2
	orb	%bl, %cl
	je	.LBB1_18
# BB#16:                                # %_Z16getHexxagonIndexii.exit134
                                        #   in Loop: Header=BB1_4 Depth=2
	orb	7(%rsp), %al            # 1-byte Folded Reload
	je	.LBB1_18
# BB#17:                                #   in Loop: Header=BB1_4 Depth=2
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	leal	(%rcx,%r15), %eax
	movl	32(%rsp), %edx          # 4-byte Reload
	cmpl	$2, %edx
	leal	-3(%rcx,%r15), %ecx
	cmovlel	%eax, %ecx
	leal	-2(%rcx), %eax
	cmpl	$3, %edx
	cmovlel	%ecx, %eax
	movq	96(%rsp), %rsi          # 8-byte Reload
	leal	(%rsi,%rax), %ecx
	cmpl	$6, %edx
	leal	-2(%rsi,%rax), %eax
	cmovlel	%ecx, %eax
	leal	-3(%rax), %r14d
	cmpl	$7, %edx
	cmovlel	%eax, %r14d
.LBB1_18:                               # %_Z16getHexxagonIndexii.exit118
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpl	$4, %r13d
	setg	%r12b
	movl	%r14d, %esi
	callq	_ZN10BitBoard646setBitEi
	movq	clone_lookups(%rip), %rdi
	addq	8(%rsp), %rdi           # 8-byte Folded Reload
	cmpl	%ebp, 28(%rsp)          # 4-byte Folded Reload
	setge	%cl
	cmpl	%ebp, 24(%rsp)          # 4-byte Folded Reload
	setl	%al
	cmpl	$8, 60(%rsp)            # 4-byte Folded Reload
	movl	$-1, %esi
	ja	.LBB1_22
# BB#19:                                # %_Z16getHexxagonIndexii.exit118
                                        #   in Loop: Header=BB1_4 Depth=2
	orb	%r12b, %cl
	movl	$-1, %esi
	je	.LBB1_22
# BB#20:                                # %_Z16getHexxagonIndexii.exit118
                                        #   in Loop: Header=BB1_4 Depth=2
	orb	6(%rsp), %al            # 1-byte Folded Reload
	movl	$-1, %esi
	je	.LBB1_22
# BB#21:                                #   in Loop: Header=BB1_4 Depth=2
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	leal	(%rcx,%rbp), %eax
	cmpl	$2, %r13d
	leal	-3(%rcx,%rbp), %ecx
	cmovlel	%eax, %ecx
	leal	-2(%rcx), %eax
	cmpl	$3, %r13d
	cmovlel	%ecx, %eax
	movq	88(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	cmpl	$6, %r13d
	leal	-2(%rdx,%rax), %eax
	cmovlel	%ecx, %eax
	leal	-3(%rax), %ecx
	cmpl	$7, %r13d
	cmovlel	%eax, %ecx
	leal	-4(%rcx), %esi
	cmpl	$8, %r13d
	cmovlel	%ecx, %esi
.LBB1_22:                               # %_Z16getHexxagonIndexii.exit102
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpl	$4, %r13d
	setg	%bl
	callq	_ZN10BitBoard646setBitEi
	movq	clone_lookups(%rip), %rdi
	addq	8(%rsp), %rdi           # 8-byte Folded Reload
	leal	1(%r15), %r14d
	cmpl	%r15d, 28(%rsp)         # 4-byte Folded Reload
	setg	%cl
	cmpl	%r15d, 24(%rsp)         # 4-byte Folded Reload
	setle	%al
	cmpl	$8, %r15d
	ja	.LBB1_23
# BB#24:                                # %_Z16getHexxagonIndexii.exit102
                                        #   in Loop: Header=BB1_4 Depth=2
	orb	%bl, %cl
	je	.LBB1_23
# BB#25:                                # %_Z16getHexxagonIndexii.exit102
                                        #   in Loop: Header=BB1_4 Depth=2
	orb	6(%rsp), %al            # 1-byte Folded Reload
	movl	20(%rsp), %r12d         # 4-byte Reload
	movl	$-1, %esi
	je	.LBB1_27
# BB#26:                                #   in Loop: Header=BB1_4 Depth=2
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	leal	(%rcx,%r14), %eax
	cmpl	$2, %r13d
	leal	-3(%rcx,%r14), %ecx
	cmovlel	%eax, %ecx
	leal	-2(%rcx), %eax
	cmpl	$3, %r13d
	cmovlel	%ecx, %eax
	movq	88(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	cmpl	$6, %r13d
	leal	-2(%rdx,%rax), %eax
	cmovlel	%ecx, %eax
	leal	-3(%rax), %ecx
	cmpl	$7, %r13d
	cmovlel	%eax, %ecx
	leal	-4(%rcx), %esi
	cmpl	$8, %r13d
	cmovlel	%ecx, %esi
	jmp	.LBB1_27
	.p2align	4, 0x90
.LBB1_6:                                # %..thread135_crit_edge
                                        #   in Loop: Header=BB1_4 Depth=2
	incl	%r15d
	movl	%r15d, %r14d
	jmp	.LBB1_37
.LBB1_23:                               #   in Loop: Header=BB1_4 Depth=2
	movl	20(%rsp), %r12d         # 4-byte Reload
	movl	$-1, %esi
.LBB1_27:                               # %_Z16getHexxagonIndexii.exit86
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpl	$3, %r13d
	setg	%bl
	callq	_ZN10BitBoard646setBitEi
	movq	clone_lookups(%rip), %rdi
	addq	8(%rsp), %rdi           # 8-byte Folded Reload
	cmpl	%r15d, 40(%rsp)         # 4-byte Folded Reload
	setge	%cl
	cmpl	%r15d, 36(%rsp)         # 4-byte Folded Reload
	setl	%al
	movl	$-1, %ebp
	cmpl	$8, %r13d
	movl	$-1, %esi
	ja	.LBB1_31
# BB#28:                                # %_Z16getHexxagonIndexii.exit86
                                        #   in Loop: Header=BB1_4 Depth=2
	orb	%bl, %cl
	movl	$-1, %esi
	je	.LBB1_31
# BB#29:                                # %_Z16getHexxagonIndexii.exit86
                                        #   in Loop: Header=BB1_4 Depth=2
	orb	5(%rsp), %al            # 1-byte Folded Reload
	movl	$-1, %esi
	je	.LBB1_31
# BB#30:                                #   in Loop: Header=BB1_4 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	leal	(%rcx,%r15), %eax
	cmpl	$1, %r13d
	leal	-3(%rcx,%r15), %ecx
	cmovlel	%eax, %ecx
	leal	-2(%rcx), %eax
	cmpl	$2, %r13d
	cmovlel	%ecx, %eax
	movq	72(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	cmpl	$5, %r13d
	leal	-2(%rdx,%rax), %eax
	cmovlel	%ecx, %eax
	leal	-3(%rax), %ecx
	cmpl	$6, %r13d
	cmovlel	%eax, %ecx
	leal	-4(%rcx), %esi
	cmpl	$7, %r13d
	cmovlel	%ecx, %esi
.LBB1_31:                               # %_Z16getHexxagonIndexii.exit72
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpl	$3, %r13d
	setg	%bl
	callq	_ZN10BitBoard646setBitEi
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	clone_lookups(%rip), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpl	%r15d, 40(%rsp)         # 4-byte Folded Reload
	setg	%cl
	cmpl	%r15d, 36(%rsp)         # 4-byte Folded Reload
	setle	%al
	cmpl	$8, %r13d
	ja	.LBB1_36
# BB#32:                                # %_Z16getHexxagonIndexii.exit72
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpl	$8, %r15d
	ja	.LBB1_36
# BB#33:                                # %_Z16getHexxagonIndexii.exit72
                                        #   in Loop: Header=BB1_4 Depth=2
	orb	%bl, %cl
	je	.LBB1_36
# BB#34:                                # %_Z16getHexxagonIndexii.exit72
                                        #   in Loop: Header=BB1_4 Depth=2
	orb	5(%rsp), %al            # 1-byte Folded Reload
	je	.LBB1_36
# BB#35:                                #   in Loop: Header=BB1_4 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	leal	(%rcx,%r14), %eax
	cmpl	$1, %r13d
	leal	-3(%rcx,%r14), %ecx
	cmovlel	%eax, %ecx
	leal	-2(%rcx), %eax
	cmpl	$2, %r13d
	cmovlel	%ecx, %eax
	movq	72(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	cmpl	$5, %r13d
	leal	-2(%rdx,%rax), %eax
	cmovlel	%ecx, %eax
	leal	-3(%rax), %ecx
	cmpl	$6, %r13d
	cmovlel	%eax, %ecx
	leal	-4(%rcx), %ebp
	cmpl	$7, %r13d
	cmovlel	%ecx, %ebp
.LBB1_36:                               # %_Z16getHexxagonIndexii.exit
                                        #   in Loop: Header=BB1_4 Depth=2
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%ebp, %esi
	callq	_ZN10BitBoard646setBitEi
	incl	%r12d
	movl	32(%rsp), %r8d          # 4-byte Reload
.LBB1_37:                               # %.thread135
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpl	$10, %r14d
	movl	%r14d, %r15d
	jne	.LBB1_4
# BB#7:                                 #   in Loop: Header=BB1_3 Depth=1
	movl	56(%rsp), %eax          # 4-byte Reload
	cmpl	$10, %eax
	movl	%eax, %r13d
	jne	.LBB1_3
# BB#8:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_Z16initCloneLookupsv, .Lfunc_end1-_Z16initCloneLookupsv
	.cfi_endproc

	.globl	_Z15initJumpLookupsv
	.p2align	4, 0x90
	.type	_Z15initJumpLookupsv,@function
_Z15initJumpLookupsv:                   # @_Z15initJumpLookupsv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 240
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	$488, %edi              # imm = 0x1E8
	callq	_Znam
	movq	%rax, jump_lookups(%rip)
	movq	$0, 168(%rsp)
	leaq	168(%rsp), %rbx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	_ZN10BitBoard64aSERKS_
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB2_1:                                # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	jump_lookups(%rip), %rdi
	addq	%rbp, %rdi
	movq	$0, 168(%rsp)
	movq	%rbx, %rsi
	callq	_ZN10BitBoard64aSERKS_
	addq	$8, %rbp
	cmpq	$488, %rbp              # imm = 0x1E8
	jne	.LBB2_1
# BB#2:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB2_3:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_12 Depth 2
	leal	-1(%r14), %esi
	leal	-2(%r14), %r15d
	leal	2(%r14), %ebp
	xorl	%ecx, %ecx
	cmpl	$5, %r15d
	setg	%cl
	cmpl	$6, %r15d
	setl	4(%rsp)                 # 1-byte Folded Spill
	xorl	%edx, %edx
	cmpl	$2, %r15d
	setl	%dl
	leal	(%r14,%r14,8), %eax
	leal	-32(%rax,%rdx,4), %edx
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	cmpl	$4, %r15d
	movl	$0, %edx
	movl	$-1, %r8d
	cmovgl	%r8d, %edx
	subl	%ecx, %edx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	cmpl	$5, %esi
	setg	%cl
	cmpl	$6, %esi
	setl	7(%rsp)                 # 1-byte Folded Spill
	xorl	%edx, %edx
	cmpl	$2, %esi
	setl	%dl
	leal	-23(%rax,%rdx,4), %edx
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movl	%esi, 24(%rsp)          # 4-byte Spill
	cmpl	$4, %esi
	movl	$0, %edx
	cmovgl	%r8d, %edx
	subl	%ecx, %edx
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	cmpl	$6, %r14d
	setl	6(%rsp)                 # 1-byte Folded Spill
	xorl	%ecx, %ecx
	cmpl	$2, %r14d
	setl	%cl
	xorl	%edx, %edx
	cmpl	$4, %r14d
	movl	$0, %edi
	cmovgl	%r8d, %edi
	setg	%dl
	xorl	%esi, %esi
	cmpl	$5, %r14d
	setg	%sil
	leal	-14(%rax,%rcx,4), %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	setl	5(%rsp)                 # 1-byte Folded Spill
	subl	%esi, %edi
	movq	%rdi, 144(%rsp)         # 8-byte Spill
	cmpl	$3, %r14d
	movl	$0, %eax
	cmovgl	%r8d, %eax
	subl	%edx, %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	cmpl	$5, %ebp
	setg	%al
	cmpl	$6, %ebp
	setl	3(%rsp)                 # 1-byte Folded Spill
	xorl	%ecx, %ecx
	cmpl	$4, %ebp
	movl	$0, %edx
	cmovgl	%r8d, %edx
	setl	%cl
	subl	%eax, %edx
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	leal	-5(%rcx,%rcx), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	leal	4(%r14), %eax
	movl	%eax, 44(%rsp)          # 4-byte Spill
	leal	-5(%r14), %esi
	leal	-3(%r14), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	leal	-7(%r14), %eax
	movl	%eax, 72(%rsp)          # 4-byte Spill
	leal	3(%r14), %eax
	movl	%eax, 68(%rsp)          # 4-byte Spill
	leal	-6(%r14), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	leal	1(%r14), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	leal	5(%r14), %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	leal	-4(%r14), %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	leal	-5(%r14,%r14,8), %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	leal	6(%r14), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	leal	4(%r14,%r14,8), %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	$1, %r13d
	movl	%r15d, 80(%rsp)         # 4-byte Spill
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	movl	%esi, 76(%rsp)          # 4-byte Spill
	jmp	.LBB2_12
.LBB2_4:                                #   in Loop: Header=BB2_12 Depth=2
	movl	24(%rsp), %ebx          # 4-byte Reload
	jmp	.LBB2_33
.LBB2_7:                                #   in Loop: Header=BB2_12 Depth=2
	movl	28(%rsp), %ebx          # 4-byte Reload
	jmp	.LBB2_70
.LBB2_8:                                #   in Loop: Header=BB2_12 Depth=2
	movl	24(%rsp), %ebx          # 4-byte Reload
	jmp	.LBB2_33
.LBB2_9:                                #   in Loop: Header=BB2_12 Depth=2
	movl	28(%rsp), %ebx          # 4-byte Reload
	jmp	.LBB2_70
.LBB2_10:                               #   in Loop: Header=BB2_12 Depth=2
	movl	24(%rsp), %ebx          # 4-byte Reload
	jmp	.LBB2_33
.LBB2_11:                               #   in Loop: Header=BB2_12 Depth=2
	movl	28(%rsp), %ebx          # 4-byte Reload
	movq	48(%rsp), %r13          # 8-byte Reload
	jmp	.LBB2_70
	.p2align	4, 0x90
.LBB2_12:                               #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$4, %r14d
	setg	%al
	cmpl	$6, %r14d
	setl	%cl
	cmpl	%esi, %r13d
	setg	%dl
	orb	%cl, %dl
	cmpl	44(%rsp), %r13d         # 4-byte Folded Reload
	setle	%cl
	cmpb	$1, %dl
	jne	.LBB2_71
# BB#13:                                #   in Loop: Header=BB2_12 Depth=2
	orb	%cl, %al
	je	.LBB2_71
# BB#14:                                # %.thread
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpl	$4, %r15d
	setg	%al
	movl	%ebx, 28(%rsp)          # 4-byte Spill
	movslq	%ebx, %rcx
	shlq	$3, %rcx
	movq	jump_lookups(%rip), %rdi
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	addq	%rcx, %rdi
	leal	-2(%r13), %ecx
	leal	-3(%r13), %ebx
	movq	%rcx, %r12
	cmpl	%ecx, %ebp
	setge	%dl
	cmpl	%r13d, %esi
	setl	%cl
	movl	$-1, %ebp
	cmpl	$8, 12(%rsp)            # 4-byte Folded Reload
	movl	$-1, %esi
	ja	.LBB2_19
# BB#15:                                # %.thread
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpl	$8, %ebx
	movl	$-1, %esi
	ja	.LBB2_19
# BB#16:                                # %.thread
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	%al, %dl
	movl	$-1, %esi
	je	.LBB2_19
# BB#17:                                # %.thread
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	4(%rsp), %cl            # 1-byte Folded Reload
	movl	$-1, %esi
	je	.LBB2_19
# BB#18:                                #   in Loop: Header=BB2_12 Depth=2
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	movq	%r12, %rdx
	leal	(%rcx,%rdx), %eax
	cmpl	$2, %r15d
	leal	-3(%rcx,%rdx), %ecx
	cmovlel	%eax, %ecx
	leal	-2(%rcx), %eax
	cmpl	$3, %r15d
	cmovlel	%ecx, %eax
	movq	104(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	cmpl	$6, %r15d
	leal	-2(%rdx,%rax), %esi
	cmovlel	%ecx, %esi
.LBB2_19:                               # %_Z16getHexxagonIndexii.exit248
                                        #   in Loop: Header=BB2_12 Depth=2
	movl	%ebx, 84(%rsp)          # 4-byte Spill
	cmpl	$4, %r15d
	setg	%bl
	callq	_ZN10BitBoard646setBitEi
	movq	jump_lookups(%rip), %rdi
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	leal	-1(%r13), %esi
	cmpl	%esi, 8(%rsp)           # 4-byte Folded Reload
	setge	%cl
	cmpl	%esi, 72(%rsp)          # 4-byte Folded Reload
	setl	%al
	cmpl	$8, 12(%rsp)            # 4-byte Folded Reload
	ja	.LBB2_24
# BB#20:                                # %_Z16getHexxagonIndexii.exit248
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpl	$8, %r12d
	ja	.LBB2_24
# BB#21:                                # %_Z16getHexxagonIndexii.exit248
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	%bl, %cl
	je	.LBB2_24
# BB#22:                                # %_Z16getHexxagonIndexii.exit248
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	4(%rsp), %al            # 1-byte Folded Reload
	je	.LBB2_24
# BB#23:                                #   in Loop: Header=BB2_12 Depth=2
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	leal	(%rcx,%rsi), %eax
	cmpl	$2, %r15d
	leal	-3(%rcx,%rsi), %ecx
	cmovlel	%eax, %ecx
	leal	-2(%rcx), %eax
	cmpl	$3, %r15d
	cmovlel	%ecx, %eax
	movq	104(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	cmpl	$6, %r15d
	leal	-2(%rdx,%rax), %ebp
	cmovlel	%ecx, %ebp
.LBB2_24:                               # %_Z16getHexxagonIndexii.exit232
                                        #   in Loop: Header=BB2_12 Depth=2
	movq	%rsi, 176(%rsp)         # 8-byte Spill
	cmpl	$4, %r15d
	setg	%bl
	movl	%ebp, %esi
	callq	_ZN10BitBoard646setBitEi
	movq	jump_lookups(%rip), %rdi
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	cmpl	%r13d, 8(%rsp)          # 4-byte Folded Reload
	setge	%cl
	cmpl	%r13d, 72(%rsp)         # 4-byte Folded Reload
	setl	%al
	movl	$-1, %ebp
	cmpl	$8, 12(%rsp)            # 4-byte Folded Reload
	movl	$-1, %esi
	ja	.LBB2_28
# BB#25:                                # %_Z16getHexxagonIndexii.exit232
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	%bl, %cl
	movl	$-1, %esi
	je	.LBB2_28
# BB#26:                                # %_Z16getHexxagonIndexii.exit232
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	4(%rsp), %al            # 1-byte Folded Reload
	movl	$-1, %esi
	je	.LBB2_28
# BB#27:                                #   in Loop: Header=BB2_12 Depth=2
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	leal	(%rcx,%r13), %eax
	cmpl	$2, %r15d
	leal	-3(%rcx,%r13), %ecx
	cmovlel	%eax, %ecx
	leal	-2(%rcx), %eax
	cmpl	$3, %r15d
	cmovlel	%ecx, %eax
	movq	104(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	cmpl	$6, %r15d
	leal	-2(%rdx,%rax), %esi
	cmovlel	%ecx, %esi
.LBB2_28:                               # %_Z16getHexxagonIndexii.exit216
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpl	$4, 24(%rsp)            # 4-byte Folded Reload
	setg	%bl
	callq	_ZN10BitBoard646setBitEi
	movq	jump_lookups(%rip), %rdi
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	movq	%r12, %rax
	cmpl	%eax, 68(%rsp)          # 4-byte Folded Reload
	setge	%cl
	cmpl	%eax, 64(%rsp)          # 4-byte Folded Reload
	setl	%al
	cmpl	$8, %r15d
	ja	.LBB2_10
# BB#29:                                # %_Z16getHexxagonIndexii.exit216
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpl	$8, 84(%rsp)            # 4-byte Folded Reload
	ja	.LBB2_8
# BB#30:                                # %_Z16getHexxagonIndexii.exit216
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	%bl, %cl
	je	.LBB2_4
# BB#31:                                # %_Z16getHexxagonIndexii.exit216
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	7(%rsp), %al            # 1-byte Folded Reload
	movl	24(%rsp), %ebx          # 4-byte Reload
	je	.LBB2_33
# BB#32:                                #   in Loop: Header=BB2_12 Depth=2
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	movq	%r12, %rdx
	leal	(%rcx,%rdx), %eax
	cmpl	$2, %ebx
	leal	-3(%rcx,%rdx), %ecx
	cmovlel	%eax, %ecx
	leal	-2(%rcx), %eax
	cmpl	$3, %ebx
	cmovlel	%ecx, %eax
	movq	152(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	cmpl	$6, %ebx
	leal	-2(%rdx,%rax), %eax
	cmovlel	%ecx, %eax
	leal	-3(%rax), %ebp
	cmpl	$7, %ebx
	cmovlel	%eax, %ebp
.LBB2_33:                               # %_Z16getHexxagonIndexii.exit200
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpl	$4, %ebx
	setg	%r15b
	movl	%ebp, %esi
	callq	_ZN10BitBoard646setBitEi
	movq	jump_lookups(%rip), %rdi
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	leal	1(%r13), %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	cmpl	%r13d, 68(%rsp)         # 4-byte Folded Reload
	setg	%cl
	cmpl	%r13d, 64(%rsp)         # 4-byte Folded Reload
	setle	%al
	movl	$-1, %ebp
	cmpl	$8, 80(%rsp)            # 4-byte Folded Reload
	movl	$-1, %esi
	ja	.LBB2_38
# BB#34:                                # %_Z16getHexxagonIndexii.exit200
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpl	$8, %r13d
	movl	$-1, %esi
	ja	.LBB2_38
# BB#35:                                # %_Z16getHexxagonIndexii.exit200
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	%r15b, %cl
	movl	$-1, %esi
	je	.LBB2_38
# BB#36:                                # %_Z16getHexxagonIndexii.exit200
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	7(%rsp), %al            # 1-byte Folded Reload
	movl	$-1, %esi
	je	.LBB2_38
# BB#37:                                #   in Loop: Header=BB2_12 Depth=2
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	movq	48(%rsp), %rdx          # 8-byte Reload
	leal	(%rcx,%rdx), %eax
	cmpl	$2, %ebx
	leal	-3(%rcx,%rdx), %ecx
	cmovlel	%eax, %ecx
	leal	-2(%rcx), %eax
	cmpl	$3, %ebx
	cmovlel	%ecx, %eax
	movq	152(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	cmpl	$6, %ebx
	leal	-2(%rdx,%rax), %eax
	cmovlel	%ecx, %eax
	leal	-3(%rax), %esi
	cmpl	$7, %ebx
	cmovlel	%eax, %esi
.LBB2_38:                               # %_Z16getHexxagonIndexii.exit184
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpl	$4, %r14d
	setg	%bl
	callq	_ZN10BitBoard646setBitEi
	movq	jump_lookups(%rip), %rdi
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	cmpl	%r12d, 44(%rsp)         # 4-byte Folded Reload
	setge	%cl
	cmpl	%r13d, 12(%rsp)         # 4-byte Folded Reload
	setl	%al
	cmpl	$8, 84(%rsp)            # 4-byte Folded Reload
	ja	.LBB2_42
# BB#39:                                # %_Z16getHexxagonIndexii.exit184
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	%bl, %cl
	je	.LBB2_42
# BB#40:                                # %_Z16getHexxagonIndexii.exit184
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	6(%rsp), %al            # 1-byte Folded Reload
	je	.LBB2_42
# BB#41:                                #   in Loop: Header=BB2_12 Depth=2
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	movq	%r12, %rdx
	leal	(%rcx,%rdx), %eax
	cmpl	$2, %r14d
	leal	-3(%rcx,%rdx), %ecx
	cmovlel	%eax, %ecx
	leal	-2(%rcx), %eax
	cmpl	$3, %r14d
	cmovlel	%ecx, %eax
	movq	144(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	cmpl	$6, %r14d
	leal	-2(%rdx,%rax), %eax
	cmovlel	%ecx, %eax
	leal	-3(%rax), %ecx
	cmpl	$7, %r14d
	cmovlel	%eax, %ecx
	leal	-4(%rcx), %ebp
	cmpl	$8, %r14d
	cmovlel	%ecx, %ebp
.LBB2_42:                               # %_Z16getHexxagonIndexii.exit169
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpl	$4, %r14d
	setg	%bl
	movl	%ebp, %esi
	callq	_ZN10BitBoard646setBitEi
	movq	jump_lookups(%rip), %rdi
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	leal	2(%r13), %r15d
	cmpl	%r15d, 44(%rsp)         # 4-byte Folded Reload
	setge	%cl
	cmpl	%r15d, 76(%rsp)         # 4-byte Folded Reload
	setl	%al
	movl	$-1, %ebp
	cmpl	$8, 48(%rsp)            # 4-byte Folded Reload
	movl	$-1, %esi
	ja	.LBB2_46
# BB#43:                                # %_Z16getHexxagonIndexii.exit169
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	%bl, %cl
	movl	$-1, %esi
	je	.LBB2_46
# BB#44:                                # %_Z16getHexxagonIndexii.exit169
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	6(%rsp), %al            # 1-byte Folded Reload
	movl	$-1, %esi
	je	.LBB2_46
# BB#45:                                #   in Loop: Header=BB2_12 Depth=2
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	leal	(%rcx,%r15), %eax
	cmpl	$2, %r14d
	leal	-3(%rcx,%r15), %ecx
	cmovlel	%eax, %ecx
	leal	-2(%rcx), %eax
	cmpl	$3, %r14d
	cmovlel	%ecx, %eax
	movq	144(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	cmpl	$6, %r14d
	leal	-2(%rdx,%rax), %eax
	cmovlel	%ecx, %eax
	leal	-3(%rax), %ecx
	cmpl	$7, %r14d
	cmovlel	%eax, %ecx
	leal	-4(%rcx), %esi
	cmpl	$8, %r14d
	cmovlel	%ecx, %esi
.LBB2_46:                               # %_Z16getHexxagonIndexii.exit153
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpl	$3, %r14d
	setg	%bl
	callq	_ZN10BitBoard646setBitEi
	movq	jump_lookups(%rip), %rdi
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	movq	176(%rsp), %rdx         # 8-byte Reload
	cmpl	%edx, 60(%rsp)          # 4-byte Folded Reload
	setge	%cl
	cmpl	%edx, 56(%rsp)          # 4-byte Folded Reload
	setl	%al
	cmpl	$8, %r14d
	ja	.LBB2_51
# BB#47:                                # %_Z16getHexxagonIndexii.exit153
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpl	$8, %r12d
	ja	.LBB2_51
# BB#48:                                # %_Z16getHexxagonIndexii.exit153
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	%bl, %cl
	je	.LBB2_51
# BB#49:                                # %_Z16getHexxagonIndexii.exit153
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	5(%rsp), %al            # 1-byte Folded Reload
	je	.LBB2_51
# BB#50:                                #   in Loop: Header=BB2_12 Depth=2
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	leal	(%rcx,%rdx), %eax
	cmpl	$1, %r14d
	leal	-3(%rcx,%rdx), %ecx
	cmovlel	%eax, %ecx
	leal	-2(%rcx), %eax
	cmpl	$2, %r14d
	cmovlel	%ecx, %eax
	movq	128(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	cmpl	$5, %r14d
	leal	-2(%rdx,%rax), %eax
	cmovlel	%ecx, %eax
	leal	-3(%rax), %ecx
	cmpl	$6, %r14d
	cmovlel	%eax, %ecx
	leal	-4(%rcx), %ebp
	cmpl	$7, %r14d
	cmovlel	%ecx, %ebp
.LBB2_51:                               # %_Z16getHexxagonIndexii.exit137
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpl	$3, %r14d
	setg	%bl
	movl	%ebp, %esi
	callq	_ZN10BitBoard646setBitEi
	movq	jump_lookups(%rip), %rdi
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	cmpl	%r15d, 60(%rsp)         # 4-byte Folded Reload
	setge	%cl
	cmpl	%r15d, 56(%rsp)         # 4-byte Folded Reload
	setl	%al
	movl	$-1, %ebp
	cmpl	$8, %r14d
	movl	$-1, %esi
	ja	.LBB2_56
# BB#52:                                # %_Z16getHexxagonIndexii.exit137
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpl	$8, 48(%rsp)            # 4-byte Folded Reload
	movl	$-1, %esi
	ja	.LBB2_56
# BB#53:                                # %_Z16getHexxagonIndexii.exit137
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	%bl, %cl
	movl	$-1, %esi
	je	.LBB2_56
# BB#54:                                # %_Z16getHexxagonIndexii.exit137
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	5(%rsp), %al            # 1-byte Folded Reload
	movl	$-1, %esi
	je	.LBB2_56
# BB#55:                                #   in Loop: Header=BB2_12 Depth=2
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	leal	(%rcx,%r15), %eax
	cmpl	$1, %r14d
	leal	-3(%rcx,%r15), %ecx
	cmovlel	%eax, %ecx
	leal	-2(%rcx), %eax
	cmpl	$2, %r14d
	cmovlel	%ecx, %eax
	movq	128(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	cmpl	$5, %r14d
	leal	-2(%rdx,%rax), %eax
	cmovlel	%ecx, %eax
	leal	-3(%rax), %ecx
	cmpl	$6, %r14d
	cmovlel	%eax, %ecx
	leal	-4(%rcx), %esi
	cmpl	$7, %r14d
	cmovlel	%ecx, %esi
.LBB2_56:                               # %_Z16getHexxagonIndexii.exit122
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpl	%r13d, 12(%rsp)         # 4-byte Folded Reload
	setl	%bl
	cmpl	$4, 8(%rsp)             # 4-byte Folded Reload
	setg	%r12b
	callq	_ZN10BitBoard646setBitEi
	movq	jump_lookups(%rip), %rdi
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	cmpl	%r13d, 40(%rsp)         # 4-byte Folded Reload
	setge	%al
	cmpl	$8, 32(%rsp)            # 4-byte Folded Reload
	ja	.LBB2_6
# BB#57:                                # %_Z16getHexxagonIndexii.exit122
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	%r12b, %al
	je	.LBB2_6
# BB#58:                                # %_Z16getHexxagonIndexii.exit122
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	3(%rsp), %bl            # 1-byte Folded Reload
	movl	8(%rsp), %edx           # 4-byte Reload
	je	.LBB2_60
# BB#59:                                #   in Loop: Header=BB2_12 Depth=2
	movq	88(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r13), %eax
	addl	36(%rsp), %eax          # 4-byte Folded Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	leal	(%rsi,%rax), %ecx
	cmpl	$6, %edx
	leal	-2(%rsi,%rax), %eax
	cmovlel	%ecx, %eax
	leal	-3(%rax), %ecx
	cmpl	$7, %edx
	cmovlel	%eax, %ecx
	leal	-4(%rcx), %ebp
	cmpl	$8, %edx
	cmovlel	%ecx, %ebp
	jmp	.LBB2_60
	.p2align	4, 0x90
.LBB2_71:                               # %..thread249_crit_edge
                                        #   in Loop: Header=BB2_12 Depth=2
	incl	%r13d
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	jmp	.LBB2_72
.LBB2_6:                                #   in Loop: Header=BB2_12 Depth=2
	movl	8(%rsp), %edx           # 4-byte Reload
.LBB2_60:                               # %_Z16getHexxagonIndexii.exit107
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpl	$4, %edx
	setg	%bl
	movl	%ebp, %esi
	callq	_ZN10BitBoard646setBitEi
	movl	8(%rsp), %edx           # 4-byte Reload
	movq	jump_lookups(%rip), %rdi
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	cmpl	%r13d, 40(%rsp)         # 4-byte Folded Reload
	setg	%cl
	cmpl	%r13d, 12(%rsp)         # 4-byte Folded Reload
	setle	%al
	movl	$-1, %ebp
	cmpl	$8, 32(%rsp)            # 4-byte Folded Reload
	movl	$-1, %esi
	ja	.LBB2_65
# BB#61:                                # %_Z16getHexxagonIndexii.exit107
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpl	$8, %r13d
	movl	$-1, %esi
	ja	.LBB2_65
# BB#62:                                # %_Z16getHexxagonIndexii.exit107
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	%bl, %cl
	movl	$-1, %esi
	je	.LBB2_65
# BB#63:                                # %_Z16getHexxagonIndexii.exit107
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	3(%rsp), %al            # 1-byte Folded Reload
	movl	$-1, %esi
	je	.LBB2_65
# BB#64:                                #   in Loop: Header=BB2_12 Depth=2
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	leal	(%rax,%rcx), %eax
	addl	36(%rsp), %eax          # 4-byte Folded Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	leal	(%rsi,%rax), %ecx
	cmpl	$6, %edx
	leal	-2(%rsi,%rax), %eax
	cmovlel	%ecx, %eax
	leal	-3(%rax), %ecx
	cmpl	$7, %edx
	cmovlel	%eax, %ecx
	leal	-4(%rcx), %esi
	cmpl	$8, %edx
	cmovlel	%ecx, %esi
.LBB2_65:                               # %_Z16getHexxagonIndexii.exit91
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpl	$4, %edx
	setg	%bl
	callq	_ZN10BitBoard646setBitEi
	movq	16(%rsp), %rax          # 8-byte Reload
	addq	jump_lookups(%rip), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpl	%r15d, 40(%rsp)         # 4-byte Folded Reload
	setge	%cl
	cmpl	%r15d, 12(%rsp)         # 4-byte Folded Reload
	setl	%al
	cmpl	$8, 32(%rsp)            # 4-byte Folded Reload
	ja	.LBB2_11
# BB#66:                                # %_Z16getHexxagonIndexii.exit91
                                        #   in Loop: Header=BB2_12 Depth=2
	movq	48(%rsp), %r13          # 8-byte Reload
	cmpl	$8, %r13d
	ja	.LBB2_9
# BB#67:                                # %_Z16getHexxagonIndexii.exit91
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	%bl, %cl
	movl	8(%rsp), %edx           # 4-byte Reload
	je	.LBB2_7
# BB#68:                                # %_Z16getHexxagonIndexii.exit91
                                        #   in Loop: Header=BB2_12 Depth=2
	orb	3(%rsp), %al            # 1-byte Folded Reload
	movl	28(%rsp), %ebx          # 4-byte Reload
	je	.LBB2_70
# BB#69:                                #   in Loop: Header=BB2_12 Depth=2
	addl	88(%rsp), %r15d         # 4-byte Folded Reload
	addl	36(%rsp), %r15d         # 4-byte Folded Reload
	movq	96(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%r15), %eax
	cmpl	$6, %edx
	leal	-2(%rcx,%r15), %ecx
	cmovlel	%eax, %ecx
	leal	-3(%rcx), %eax
	cmpl	$7, %edx
	cmovlel	%ecx, %eax
	leal	-4(%rax), %ebp
	cmpl	$8, %edx
	cmovlel	%eax, %ebp
.LBB2_70:                               # %_Z16getHexxagonIndexii.exit
                                        #   in Loop: Header=BB2_12 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	callq	_ZN10BitBoard646setBitEi
	movl	8(%rsp), %ebp           # 4-byte Reload
	incl	%ebx
	movl	80(%rsp), %r15d         # 4-byte Reload
	movl	76(%rsp), %esi          # 4-byte Reload
.LBB2_72:                               # %.thread249
                                        #   in Loop: Header=BB2_12 Depth=2
	cmpl	$10, %r13d
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	jne	.LBB2_12
# BB#73:                                #   in Loop: Header=BB2_3 Depth=1
	movl	32(%rsp), %eax          # 4-byte Reload
	cmpl	$10, %eax
	movl	%eax, %r14d
	jne	.LBB2_3
# BB#74:
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_Z15initJumpLookupsv, .Lfunc_end2-_Z15initJumpLookupsv
	.cfi_endproc

	.globl	_ZN13HexxagonBoardC2ERKS_
	.p2align	4, 0x90
	.type	_ZN13HexxagonBoardC2ERKS_,@function
_ZN13HexxagonBoardC2ERKS_:              # @_ZN13HexxagonBoardC2ERKS_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	leaq	8(%rdi), %r14
	callq	_ZN10BitBoard64aSERKS_
	addq	$8, %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN10BitBoard64aSERKS_  # TAILCALL
.Lfunc_end3:
	.size	_ZN13HexxagonBoardC2ERKS_, .Lfunc_end3-_ZN13HexxagonBoardC2ERKS_
	.cfi_endproc

	.globl	_ZN13HexxagonBoardaSERKS_
	.p2align	4, 0x90
	.type	_ZN13HexxagonBoardaSERKS_,@function
_ZN13HexxagonBoardaSERKS_:              # @_ZN13HexxagonBoardaSERKS_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi35:
	.cfi_def_cfa_offset 48
.Lcfi36:
	.cfi_offset %rbx, -40
.Lcfi37:
	.cfi_offset %r12, -32
.Lcfi38:
	.cfi_offset %r14, -24
.Lcfi39:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	leaq	8(%r14), %rsi
	leaq	8(%rbx), %r12
	movq	%r12, %rdi
	callq	_ZN10BitBoard64aSERKS_
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN10BitBoard64aSERKS_
	leaq	8(%r15), %r14
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	_ZN10BitBoard64aSERKS_
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	_ZN10BitBoard64aSERKS_
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	_ZN13HexxagonBoardaSERKS_, .Lfunc_end4-_ZN13HexxagonBoardaSERKS_
	.cfi_endproc

	.globl	_ZN13HexxagonBoard4initEv
	.p2align	4, 0x90
	.type	_ZN13HexxagonBoard4initEv,@function
_ZN13HexxagonBoard4initEv:              # @_ZN13HexxagonBoard4initEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -24
.Lcfi44:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$0, (%rsp)
	movq	%rsp, %rsi
	callq	_ZN10BitBoard64aSERKS_
	movq	$0, (%rsp)
	leaq	8(%rbx), %r14
	movq	%rsp, %rsi
	movq	%r14, %rdi
	callq	_ZN10BitBoard64aSERKS_
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN10BitBoard646setBitEi
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	_ZN10BitBoard646setBitEi
	movl	$34, %esi
	movq	%rbx, %rdi
	callq	_ZN10BitBoard646setBitEi
	movl	$34, %esi
	movq	%r14, %rdi
	callq	_ZN10BitBoard646setBitEi
	movl	$56, %esi
	movq	%rbx, %rdi
	callq	_ZN10BitBoard646setBitEi
	movl	$56, %esi
	movq	%r14, %rdi
	callq	_ZN10BitBoard646setBitEi
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	_ZN10BitBoard646setBitEi
	movl	$26, %esi
	movq	%rbx, %rdi
	callq	_ZN10BitBoard646setBitEi
	movl	$60, %esi
	movq	%rbx, %rdi
	callq	_ZN10BitBoard646setBitEi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	_ZN13HexxagonBoard4initEv, .Lfunc_end5-_ZN13HexxagonBoard4initEv
	.cfi_endproc

	.globl	_ZN13HexxagonBoard11countBricksEi
	.p2align	4, 0x90
	.type	_ZN13HexxagonBoard11countBricksEi,@function
_ZN13HexxagonBoard11countBricksEi:      # @_ZN13HexxagonBoard11countBricksEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 64
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbp
	leaq	8(%rbp), %r15
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rdi
	movl	%ebx, %esi
	callq	_ZN10BitBoard646getBitEi
	testl	%eax, %eax
	je	.LBB6_9
# BB#2:                                 #   in Loop: Header=BB6_1 Depth=1
	movq	%r15, %rdi
	movl	%ebx, %esi
	callq	_ZN10BitBoard646getBitEi
	testl	%eax, %eax
	je	.LBB6_8
# BB#3:                                 #   in Loop: Header=BB6_1 Depth=1
	incl	%r12d
	jmp	.LBB6_9
	.p2align	4, 0x90
.LBB6_8:                                #   in Loop: Header=BB6_1 Depth=1
	incl	%r13d
.LBB6_9:                                #   in Loop: Header=BB6_1 Depth=1
	incl	%ebx
	cmpl	$61, %ebx
	jne	.LBB6_1
# BB#4:
	testl	%r14d, %r14d
	je	.LBB6_10
# BB#5:
	cmpl	$1, %r14d
	je	.LBB6_11
# BB#6:
	cmpl	$2, %r14d
	movl	%r13d, %r12d
	je	.LBB6_11
# BB#7:
	xorl	%r12d, %r12d
	jmp	.LBB6_11
.LBB6_10:
	subl	%r13d, %r12d
.LBB6_11:
	movl	%r12d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN13HexxagonBoard11countBricksEi, .Lfunc_end6-_ZN13HexxagonBoard11countBricksEi
	.cfi_endproc

	.globl	_ZN13HexxagonBoard8evaluateEv
	.p2align	4, 0x90
	.type	_ZN13HexxagonBoard8evaluateEv,@function
_ZN13HexxagonBoard8evaluateEv:          # @_ZN13HexxagonBoard8evaluateEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi62:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 64
.Lcfi65:
	.cfi_offset %rbx, -56
.Lcfi66:
	.cfi_offset %r12, -48
.Lcfi67:
	.cfi_offset %r13, -40
.Lcfi68:
	.cfi_offset %r14, -32
.Lcfi69:
	.cfi_offset %r15, -24
.Lcfi70:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	leaq	8(%r13), %r14
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	movq	%r13, %rdi
	movl	%ebp, %esi
	callq	_ZN10BitBoard646getBitEi
	testl	%eax, %eax
	je	.LBB7_11
# BB#2:                                 #   in Loop: Header=BB7_1 Depth=1
	incl	%ebx
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	_ZN10BitBoard646getBitEi
	testl	%eax, %eax
	je	.LBB7_10
# BB#3:                                 #   in Loop: Header=BB7_1 Depth=1
	incl	%r12d
	jmp	.LBB7_11
	.p2align	4, 0x90
.LBB7_10:                               #   in Loop: Header=BB7_1 Depth=1
	incl	%r15d
.LBB7_11:                               #   in Loop: Header=BB7_1 Depth=1
	incl	%ebp
	cmpl	$61, %ebp
	jne	.LBB7_1
# BB#4:
	movl	%r12d, %eax
	subl	%r15d, %eax
	testl	%r12d, %r12d
	je	.LBB7_7
# BB#5:
	testl	%r15d, %r15d
	je	.LBB7_7
# BB#6:
	cmpl	$61, %ebx
	jne	.LBB7_9
.LBB7_7:
	leal	-20000(%rax), %ecx
	testl	%r12d, %r12d
	movl	%eax, %edx
	cmovel	%ecx, %edx
	leal	20000(%rdx), %eax
	testl	%r15d, %r15d
	cmovnel	%edx, %eax
	cmpl	$61, %ebx
	jne	.LBB7_9
# BB#8:
	leal	20000(%rax), %ecx
	cmpl	%r15d, %r12d
	cmovgl	%ecx, %eax
	addl	$-20000, %eax           # imm = 0xB1E0
	cmpl	%r15d, %r12d
	cmovgl	%ecx, %eax
.LBB7_9:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZN13HexxagonBoard8evaluateEv, .Lfunc_end7-_ZN13HexxagonBoard8evaluateEv
	.cfi_endproc

	.globl	_ZN13HexxagonBoard11getHexxagonEi
	.p2align	4, 0x90
	.type	_ZN13HexxagonBoard11getHexxagonEi,@function
_ZN13HexxagonBoard11getHexxagonEi:      # @_ZN13HexxagonBoard11getHexxagonEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi73:
	.cfi_def_cfa_offset 32
.Lcfi74:
	.cfi_offset %rbx, -24
.Lcfi75:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	callq	_ZN10BitBoard646getBitEi
	testl	%eax, %eax
	je	.LBB8_1
# BB#2:
	addq	$8, %rbx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	_ZN10BitBoard646getBitEi
	cmpl	$1, %eax
	movl	$1, %eax
	adcl	$0, %eax
	jmp	.LBB8_3
.LBB8_1:
	movl	$3, %eax
.LBB8_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN13HexxagonBoard11getHexxagonEi, .Lfunc_end8-_ZN13HexxagonBoard11getHexxagonEi
	.cfi_endproc

	.globl	_ZN13HexxagonBoard9applyMoveER12HexxagonMove
	.p2align	4, 0x90
	.type	_ZN13HexxagonBoard9applyMoveER12HexxagonMove,@function
_ZN13HexxagonBoard9applyMoveER12HexxagonMove: # @_ZN13HexxagonBoard9applyMoveER12HexxagonMove
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi79:
	.cfi_def_cfa_offset 48
.Lcfi80:
	.cfi_offset %rbx, -32
.Lcfi81:
	.cfi_offset %r14, -24
.Lcfi82:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movsbl	1(%rbx), %esi
	callq	_ZN10BitBoard646setBitEi
	leaq	8(%r14), %r15
	movsbl	1(%rbx), %esi
	movq	%r15, %rdi
	callq	_ZN10BitBoard646setBitEi
	movsbq	1(%rbx), %rsi
	shlq	$3, %rsi
	addq	clone_lookups(%rip), %rsi
	movq	%r15, %rdi
	callq	_ZN10BitBoard64orERKS_
	movq	%rax, 8(%rsp)
	leaq	8(%rsp), %rsi
	movq	%r15, %rdi
	callq	_ZN10BitBoard64aSERKS_
	movsbl	(%rbx), %esi
	cmpb	1(%rbx), %sil
	je	.LBB9_2
# BB#1:
	movq	%r14, %rdi
	callq	_ZN10BitBoard648unSetBitEi
.LBB9_2:
	movq	%r15, %rdi
	callq	_ZN10BitBoard64coEv
	movq	%rax, 8(%rsp)
	leaq	8(%rsp), %rsi
	movq	%r15, %rdi
	callq	_ZN10BitBoard64aSERKS_
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	_ZN13HexxagonBoard9applyMoveER12HexxagonMove, .Lfunc_end9-_ZN13HexxagonBoard9applyMoveER12HexxagonMove
	.cfi_endproc

	.globl	_ZN13HexxagonBoard11isMoveValidER12HexxagonMove
	.p2align	4, 0x90
	.type	_ZN13HexxagonBoard11isMoveValidER12HexxagonMove,@function
_ZN13HexxagonBoard11isMoveValidER12HexxagonMove: # @_ZN13HexxagonBoard11isMoveValidER12HexxagonMove
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi85:
	.cfi_def_cfa_offset 64
.Lcfi86:
	.cfi_offset %rbx, -24
.Lcfi87:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	8(%rbx), %rdi
	movq	%rbx, %rsi
	callq	_ZN10BitBoard64anERKS_
	movq	%rax, (%rsp)
	movsbl	1(%r14), %esi
	movq	%rbx, %rdi
	callq	_ZN10BitBoard646getBitEi
	testl	%eax, %eax
	jne	.LBB10_5
# BB#1:
	movsbq	(%r14), %rsi
	cmpb	1(%r14), %sil
	jne	.LBB10_3
# BB#2:
	movq	clone_lookups(%rip), %rax
	leaq	(%rax,%rsi,8), %rsi
	movq	%rsp, %rdi
	callq	_ZN10BitBoard64anERKS_
	movq	%rax, 32(%rsp)
	leaq	32(%rsp), %rdi
	jmp	.LBB10_4
.LBB10_3:
	movq	$0, 24(%rsp)
	leaq	24(%rsp), %rbx
	movq	%rbx, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN10BitBoard646setBitEi
	movsbq	1(%r14), %rsi
	shlq	$3, %rsi
	addq	jump_lookups(%rip), %rsi
	movq	%rbx, %rdi
	callq	_ZN10BitBoard64anERKS_
	movq	%rax, 16(%rsp)
	leaq	16(%rsp), %rdi
	movq	%rsp, %rsi
	callq	_ZN10BitBoard64anERKS_
	movq	%rax, 8(%rsp)
	leaq	8(%rsp), %rdi
.LBB10_4:
	callq	_ZN10BitBoard64cvbEv
	movl	%eax, %ecx
	movl	$1, %eax
	testb	%cl, %cl
	jne	.LBB10_6
.LBB10_5:
	xorl	%eax, %eax
.LBB10_6:
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	_ZN13HexxagonBoard11isMoveValidER12HexxagonMove, .Lfunc_end10-_ZN13HexxagonBoard11isMoveValidER12HexxagonMove
	.cfi_endproc

	.globl	_ZN13HexxagonBoard16generateMoveListEv
	.p2align	4, 0x90
	.type	_ZN13HexxagonBoard16generateMoveListEv,@function
_ZN13HexxagonBoard16generateMoveListEv: # @_ZN13HexxagonBoard16generateMoveListEv
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi91:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi92:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi94:
	.cfi_def_cfa_offset 112
.Lcfi95:
	.cfi_offset %rbx, -56
.Lcfi96:
	.cfi_offset %r12, -48
.Lcfi97:
	.cfi_offset %r13, -40
.Lcfi98:
	.cfi_offset %r14, -32
.Lcfi99:
	.cfi_offset %r15, -24
.Lcfi100:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r12
.Ltmp0:
	movl	$128, %edi
	callq	_Znam
.Ltmp1:
# BB#1:                                 # %_ZN16HexxagonMoveListC2Ev.exit
	movq	%rax, 8(%r12)
	movl	$0, (%r12)
	leaq	8(%rbp), %r15
	xorl	%ebx, %ebx
	leaq	8(%rsp), %r13
	movq	%rsp, %r14
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB11_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_7 Depth 2
	movq	%rbp, %rdi
	movl	%ebx, %esi
	callq	_ZN10BitBoard646getBitEi
	testl	%eax, %eax
	jne	.LBB11_11
# BB#3:                                 #   in Loop: Header=BB11_2 Depth=1
	movq	clone_lookups(%rip), %rax
	leaq	(%rax,%rbx,8), %rsi
	movq	%rbp, %rdi
	callq	_ZN10BitBoard64anERKS_
	movq	%rax, 8(%rsp)
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	_ZN10BitBoard64anERKS_
	movq	%rax, 48(%rsp)
	leaq	48(%rsp), %rdi
	callq	_ZN10BitBoard64cvbEv
	testb	%al, %al
	je	.LBB11_5
# BB#4:                                 #   in Loop: Header=BB11_2 Depth=1
	movb	%bl, 8(%rsp)
	movb	%bl, 9(%rsp)
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	_ZN16HexxagonMoveList7addMoveER12HexxagonMove
.LBB11_5:                               #   in Loop: Header=BB11_2 Depth=1
	movq	jump_lookups(%rip), %rax
	leaq	(%rax,%rbx,8), %rsi
	movq	%rbp, %rdi
	callq	_ZN10BitBoard64anERKS_
	movq	%rax, 40(%rsp)
	leaq	40(%rsp), %rdi
	movq	%r15, %rsi
	callq	_ZN10BitBoard64anERKS_
	movq	%rax, (%rsp)
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	_ZN10BitBoard64aSERKS_
	movq	%rax, 32(%rsp)
	leaq	32(%rsp), %rdi
	callq	_ZN10BitBoard64cvbEv
	testb	%al, %al
	je	.LBB11_10
# BB#6:                                 # %.preheader
                                        #   in Loop: Header=BB11_2 Depth=1
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB11_7:                               #   Parent Loop BB11_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	movl	%r15d, %esi
	callq	_ZN10BitBoard646getBitEi
	testl	%eax, %eax
	je	.LBB11_9
# BB#8:                                 #   in Loop: Header=BB11_7 Depth=2
	movb	%r15b, (%rsp)
	movb	%bl, 1(%rsp)
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZN16HexxagonMoveList7addMoveER12HexxagonMove
.LBB11_9:                               #   in Loop: Header=BB11_7 Depth=2
	incl	%r15d
	cmpl	$61, %r15d
	jne	.LBB11_7
.LBB11_10:                              # %.loopexit
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB11_11:                              #   in Loop: Header=BB11_2 Depth=1
	incq	%rbx
	cmpq	$61, %rbx
	jne	.LBB11_2
# BB#12:
	movq	%r12, %rdi
	callq	_ZN16HexxagonMoveList10getNrMovesEv
	testl	%eax, %eax
	jne	.LBB11_16
# BB#13:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB11_15
# BB#14:
	callq	_ZdlPv
.LBB11_15:                              # %_ZN16HexxagonMoveListD2Ev.exit
	movq	%r12, %rdi
	callq	_ZdlPv
	xorl	%r12d, %r12d
.LBB11_16:
	movq	%r12, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_17:
.Ltmp2:
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_ZN13HexxagonBoard16generateMoveListEv, .Lfunc_end11-_ZN13HexxagonBoard16generateMoveListEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end11-.Ltmp1     #   Call between .Ltmp1 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN13HexxagonBoard9endOfGameEv
	.p2align	4, 0x90
	.type	_ZN13HexxagonBoard9endOfGameEv,@function
_ZN13HexxagonBoard9endOfGameEv:         # @_ZN13HexxagonBoard9endOfGameEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 16
.Lcfi102:
	.cfi_offset %rbx, -16
	callq	_ZN13HexxagonBoard16generateMoveListEv
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB12_1
# BB#2:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_4
# BB#3:
	callq	_ZdlPv
.LBB12_4:                               # %_ZN16HexxagonMoveListD2Ev.exit
	movq	%rbx, %rdi
	callq	_ZdlPv
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB12_1:
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end12:
	.size	_ZN13HexxagonBoard9endOfGameEv, .Lfunc_end12-_ZN13HexxagonBoard9endOfGameEv
	.cfi_endproc

	.globl	_ZN13HexxagonBoard12computerMoveEiPFvvEi
	.p2align	4, 0x90
	.type	_ZN13HexxagonBoard12computerMoveEiPFvvEi,@function
_ZN13HexxagonBoard12computerMoveEiPFvvEi: # @_ZN13HexxagonBoard12computerMoveEiPFvvEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi103:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi104:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi106:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi107:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi109:
	.cfi_def_cfa_offset 80
.Lcfi110:
	.cfi_offset %rbx, -56
.Lcfi111:
	.cfi_offset %r12, -48
.Lcfi112:
	.cfi_offset %r13, -40
.Lcfi113:
	.cfi_offset %r14, -32
.Lcfi114:
	.cfi_offset %r15, -24
.Lcfi115:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movq	%rdx, %r12
	movl	%esi, %ebp
	movq	%rdi, %r14
	callq	_ZN13HexxagonBoard16generateMoveListEv
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB13_1
# BB#2:
	leaq	8(%rsp), %r13
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	_ZN10BitBoard64aSERKS_
	leaq	8(%r14), %rsi
	leaq	16(%rsp), %rdi
	callq	_ZN10BitBoard64aSERKS_
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	%ebp, %edx
	movq	%r12, %rcx
	movl	%r15d, %r8d
	callq	_ZN16HexxagonMoveList13scoreAllMovesE13HexxagonBoardiPFvvEi
	movq	%rbx, %rdi
	callq	_ZN16HexxagonMoveList11getBestMoveEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN13HexxagonBoard9applyMoveER12HexxagonMove
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB13_4
# BB#3:
	callq	_ZdlPv
.LBB13_4:                               # %_ZN16HexxagonMoveListD2Ev.exit
	movq	%rbx, %rdi
	callq	_ZdlPv
	xorl	%eax, %eax
	jmp	.LBB13_5
.LBB13_1:
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$-1, %eax
.LBB13_5:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_ZN13HexxagonBoard12computerMoveEiPFvvEi, .Lfunc_end13-_ZN13HexxagonBoard12computerMoveEiPFvvEi
	.cfi_endproc

	.globl	_ZN13HexxagonBoard12readFromFileEP8_IO_FILE
	.p2align	4, 0x90
	.type	_ZN13HexxagonBoard12readFromFileEP8_IO_FILE,@function
_ZN13HexxagonBoard12readFromFileEP8_IO_FILE: # @_ZN13HexxagonBoard12readFromFileEP8_IO_FILE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi116:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi117:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi118:
	.cfi_def_cfa_offset 32
.Lcfi119:
	.cfi_offset %rbx, -24
.Lcfi120:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	8(%rbx), %rdi
	callq	_ZN10BitBoard6412readFromFileEP8_IO_FILE
	testl	%eax, %eax
	je	.LBB14_1
# BB#2:
	movl	$-1, %eax
	jmp	.LBB14_3
.LBB14_1:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN10BitBoard6412readFromFileEP8_IO_FILE
	negl	%eax
	sbbl	%eax, %eax
.LBB14_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end14:
	.size	_ZN13HexxagonBoard12readFromFileEP8_IO_FILE, .Lfunc_end14-_ZN13HexxagonBoard12readFromFileEP8_IO_FILE
	.cfi_endproc

	.globl	_ZN13HexxagonBoard11writeToFileEP8_IO_FILE
	.p2align	4, 0x90
	.type	_ZN13HexxagonBoard11writeToFileEP8_IO_FILE,@function
_ZN13HexxagonBoard11writeToFileEP8_IO_FILE: # @_ZN13HexxagonBoard11writeToFileEP8_IO_FILE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi123:
	.cfi_def_cfa_offset 32
.Lcfi124:
	.cfi_offset %rbx, -24
.Lcfi125:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	8(%rbx), %rdi
	callq	_ZN10BitBoard6411writeToFileEP8_IO_FILE
	testl	%eax, %eax
	je	.LBB15_1
# BB#2:
	movl	$-1, %eax
	jmp	.LBB15_3
.LBB15_1:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN10BitBoard6411writeToFileEP8_IO_FILE
	negl	%eax
	sbbl	%eax, %eax
.LBB15_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end15:
	.size	_ZN13HexxagonBoard11writeToFileEP8_IO_FILE, .Lfunc_end15-_ZN13HexxagonBoard11writeToFileEP8_IO_FILE
	.cfi_endproc

	.globl	_ZN13HexxagonBoard16displayBoardTextEi
	.p2align	4, 0x90
	.type	_ZN13HexxagonBoard16displayBoardTextEi,@function
_ZN13HexxagonBoard16displayBoardTextEi: # @_ZN13HexxagonBoard16displayBoardTextEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi126:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi127:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi128:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi129:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi130:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi131:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi132:
	.cfi_def_cfa_offset 128
.Lcfi133:
	.cfi_offset %rbx, -56
.Lcfi134:
	.cfi_offset %r12, -48
.Lcfi135:
	.cfi_offset %r13, -40
.Lcfi136:
	.cfi_offset %r14, -32
.Lcfi137:
	.cfi_offset %r15, -24
.Lcfi138:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movq	8(%rbp), %rax
	movq	%rax, 48(%rsp)
	movl	%esi, 16(%rsp)          # 4-byte Spill
	testl	%esi, %esi
	jne	.LBB16_2
# BB#1:
	leaq	48(%rsp), %rbx
	movq	%rbx, %rdi
	callq	_ZN10BitBoard64coEv
	movq	%rax, 64(%rsp)
	leaq	64(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_ZN10BitBoard64aSERKS_
.LBB16_2:
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	leaq	8(%rbp), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$_ZSt4cout, %edi
	movl	$.L.str.2, %esi
	movl	$27, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$_ZSt4cout, %edi
	movl	$.L.str.3, %esi
	movl	$26, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$1, %ebp
	movl	$4, %r14d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB16_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_5 Depth 2
                                        #     Child Loop BB16_7 Depth 2
	movl	$_ZSt4cout, %edi
	movl	%ebp, %esi
	callq	_ZNSolsEi
	movl	$.L.str.4, %esi
	movl	$2, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	cmpl	$4, %ebp
	jg	.LBB16_6
# BB#4:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB16_3 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_5:                               # %.lr.ph
                                        #   Parent Loop BB16_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$_ZSt4cout, %edi
	movl	$.L.str.5, %esi
	movl	$1, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	incl	%ebx
	cmpl	%ebx, %r14d
	jne	.LBB16_5
.LBB16_6:                               # %.preheader.split.us.preheader
                                        #   in Loop: Header=BB16_3 Depth=1
	movl	%r14d, 44(%rsp)         # 4-byte Spill
	leal	4(%rbp), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	cmpl	$5, %ebp
	setg	%al
	cmpl	$6, %ebp
	setl	15(%rsp)                # 1-byte Folded Spill
	leal	-5(%rbp), %r15d
	xorl	%ecx, %ecx
	cmpl	$2, %ebp
	setl	%cl
	cmpl	$4, %ebp
	movl	$0, %r13d
	movl	$-1, %edx
	cmovgl	%edx, %r13d
	subl	%eax, %r13d
	movq	%r12, 56(%rsp)          # 8-byte Spill
	leal	-14(%r12,%rcx,4), %r12d
	movl	$10, %r14d
	jmp	.LBB16_7
.LBB16_44:                              #   in Loop: Header=BB16_7 Depth=2
	movl	$_ZSt4cout, %edi
	movl	$.L.str.8, %esi
	movl	$2, %edx
	jmp	.LBB16_46
.LBB16_43:                              #   in Loop: Header=BB16_7 Depth=2
	movl	$.L.str.7, %esi
	movl	$2, %edx
	jmp	.LBB16_46
	.p2align	4, 0x90
.LBB16_7:                               # %.preheader.split.us
                                        #   Parent Loop BB16_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$4, %ebp
	setg	%al
	leal	-9(%r14), %edx
	cmpl	%edx, 20(%rsp)          # 4-byte Folded Reload
	setge	%cl
	cmpl	%edx, %r15d
	setl	%dl
	orb	15(%rsp), %dl           # 1-byte Folded Reload
	cmpb	$1, %dl
	jne	.LBB16_45
# BB#8:                                 # %.preheader.split.us
                                        #   in Loop: Header=BB16_7 Depth=2
	orb	%cl, %al
	je	.LBB16_45
# BB#9:                                 # %_Z16getHexxagonIndexii.exit.us
                                        #   in Loop: Header=BB16_7 Depth=2
	leal	(%r12,%r14), %eax
	cmpl	$2, %ebp
	leal	-3(%r12,%r14), %ecx
	cmovlel	%eax, %ecx
	leal	-2(%rcx), %eax
	cmpl	$3, %ebp
	cmovlel	%ecx, %eax
	leal	(%r13,%rax), %ecx
	cmpl	$6, %ebp
	leal	-2(%r13,%rax), %eax
	cmovlel	%ecx, %eax
	leal	-3(%rax), %ecx
	cmpl	$7, %ebp
	cmovlel	%eax, %ecx
	leal	-4(%rcx), %ebx
	cmpl	$8, %ebp
	cmovlel	%ecx, %ebx
	cmpl	$-1, %ebx
	je	.LBB16_45
# BB#10:                                #   in Loop: Header=BB16_7 Depth=2
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, %esi
	callq	_ZN10BitBoard646getBitEi
	testl	%eax, %eax
	je	.LBB16_44
# BB#11:                                # %_Z16getHexxagonIndexii.exit76.us
                                        #   in Loop: Header=BB16_7 Depth=2
	leaq	48(%rsp), %rdi
	movl	%ebx, %esi
	callq	_ZN10BitBoard646getBitEi
	movl	$_ZSt4cout, %edi
	testl	%eax, %eax
	je	.LBB16_43
# BB#12:                                #   in Loop: Header=BB16_7 Depth=2
	movl	$.L.str.6, %esi
	movl	$2, %edx
	jmp	.LBB16_46
	.p2align	4, 0x90
.LBB16_45:                              # %_Z16getHexxagonIndexii.exit.thread.us
                                        #   in Loop: Header=BB16_7 Depth=2
	movl	$_ZSt4cout, %edi
	movl	$.L.str.5, %esi
	movl	$1, %edx
.LBB16_46:                              #   in Loop: Header=BB16_7 Depth=2
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	incl	%r14d
	cmpl	$19, %r14d
	jne	.LBB16_7
# BB#47:                                # %.us-lcssa.us
                                        #   in Loop: Header=BB16_3 Depth=1
	movl	$_ZSt4cout, %edi
	movl	$.L.str.9, %esi
	movl	$1, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	incl	%ebp
	movl	44(%rsp), %r14d         # 4-byte Reload
	decl	%r14d
	movq	56(%rsp), %r12          # 8-byte Reload
	addl	$9, %r12d
	cmpl	$10, %ebp
	jne	.LBB16_3
# BB#48:                                # %.preheader.preheader
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_14:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	callq	_ZN10BitBoard646getBitEi
	testl	%eax, %eax
	je	.LBB16_18
# BB#15:                                #   in Loop: Header=BB16_14 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	callq	_ZN10BitBoard646getBitEi
	testl	%eax, %eax
	je	.LBB16_17
# BB#16:                                #   in Loop: Header=BB16_14 Depth=1
	incl	%ebx
	jmp	.LBB16_18
	.p2align	4, 0x90
.LBB16_17:                              #   in Loop: Header=BB16_14 Depth=1
	incl	%r15d
.LBB16_18:                              #   in Loop: Header=BB16_14 Depth=1
	incl	%ebp
	cmpl	$61, %ebp
	jne	.LBB16_14
# BB#19:                                # %_ZN13HexxagonBoard11countBricksEi.exit.preheader
	xorl	%ebp, %ebp
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB16_20:                              # %_ZN13HexxagonBoard11countBricksEi.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	callq	_ZN10BitBoard646getBitEi
	testl	%eax, %eax
	je	.LBB16_24
# BB#21:                                #   in Loop: Header=BB16_20 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	callq	_ZN10BitBoard646getBitEi
	testl	%eax, %eax
	je	.LBB16_23
# BB#22:                                #   in Loop: Header=BB16_20 Depth=1
	incl	%r12d
	jmp	.LBB16_24
	.p2align	4, 0x90
.LBB16_23:                              #   in Loop: Header=BB16_20 Depth=1
	incl	%r14d
.LBB16_24:                              #   in Loop: Header=BB16_20 Depth=1
	incl	%ebp
	cmpl	$61, %ebp
	jne	.LBB16_20
# BB#25:                                # %_ZN13HexxagonBoard11countBricksEi.exit43
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	cmovnel	%ebx, %r15d
	movl	$61, %eax
	subl	%r15d, %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	$_ZSt4cout, %edi
	movl	$.L.str.10, %esi
	movl	$11, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	xorl	%ebp, %ebp
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	movq	32(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB16_26:                              # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movl	%ebp, %esi
	callq	_ZN10BitBoard646getBitEi
	testl	%eax, %eax
	je	.LBB16_30
# BB#27:                                #   in Loop: Header=BB16_26 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	callq	_ZN10BitBoard646getBitEi
	testl	%eax, %eax
	je	.LBB16_29
# BB#28:                                #   in Loop: Header=BB16_26 Depth=1
	incl	%ebx
	jmp	.LBB16_30
	.p2align	4, 0x90
.LBB16_29:                              #   in Loop: Header=BB16_26 Depth=1
	incl	%r13d
.LBB16_30:                              #   in Loop: Header=BB16_26 Depth=1
	incl	%ebp
	cmpl	$61, %ebp
	jne	.LBB16_26
# BB#31:                                # %_ZN13HexxagonBoard11countBricksEi.exit51
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	cmovnel	%r14d, %r12d
	cmovnel	%ebx, %r13d
	movl	20(%rsp), %r14d         # 4-byte Reload
	subl	%r12d, %r14d
	movl	$_ZSt4cout, %edi
	movl	%r13d, %esi
	callq	_ZNSolsEi
	movl	$_ZSt4cout, %edi
	movl	$.L.str.11, %esi
	movl	$4, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB16_32:                              # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movl	%ebp, %esi
	callq	_ZN10BitBoard646getBitEi
	testl	%eax, %eax
	je	.LBB16_38
# BB#33:                                #   in Loop: Header=BB16_32 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	callq	_ZN10BitBoard646getBitEi
	testl	%eax, %eax
	je	.LBB16_37
# BB#34:                                #   in Loop: Header=BB16_32 Depth=1
	incl	%r13d
	jmp	.LBB16_38
	.p2align	4, 0x90
.LBB16_37:                              #   in Loop: Header=BB16_32 Depth=1
	incl	%ebx
.LBB16_38:                              #   in Loop: Header=BB16_32 Depth=1
	incl	%ebp
	cmpl	$61, %ebp
	jne	.LBB16_32
# BB#35:                                # %_ZN13HexxagonBoard11countBricksEi.exit59
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	cmovnel	%ebx, %r13d
	movl	$.L.str.15, %eax
	movl	$.L.str.16, %ebp
	cmovneq	%rax, %rbp
	movl	$_ZSt4cout, %edi
	movl	%r13d, %esi
	callq	_ZNSolsEi
	movl	$_ZSt4cout, %edi
	movl	$.L.str.12, %esi
	movl	$8, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$_ZSt4cout, %edi
	movl	%r14d, %esi
	callq	_ZNSolsEi
	movl	$.L.str.13, %esi
	movl	$2, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$_ZSt4cout, %edi
	movl	$.L.str.14, %esi
	movl	$14, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$_ZSt4cout, %edi
	movl	$1, %edx
	movq	%rbp, %rsi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	%r15, %rdi
	callq	_ZN13HexxagonBoard16generateMoveListEv
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB16_36
# BB#39:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB16_41
# BB#40:
	callq	_ZdlPv
.LBB16_41:                              # %_ZN16HexxagonMoveListD2Ev.exit.i
	movq	%rbx, %rdi
	callq	_ZdlPv
	movl	$.L.str.18, %ebx
	jmp	.LBB16_42
.LBB16_36:
	movl	$.L.str.17, %ebx
.LBB16_42:                              # %_ZN13HexxagonBoard9endOfGameEv.exit
	movq	%rbx, %rdi
	callq	strlen
	movl	$_ZSt4cout, %edi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$_ZSt4cout, %edi
	movl	$.L.str.9, %esi
	movl	$1, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	_ZN13HexxagonBoard16displayBoardTextEi, .Lfunc_end16-_ZN13HexxagonBoard16displayBoardTextEi
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_hexxagonboard.ii,@function
_GLOBAL__sub_I_hexxagonboard.ii:        # @_GLOBAL__sub_I_hexxagonboard.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi139:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end17:
	.size	_GLOBAL__sub_I_hexxagonboard.ii, .Lfunc_end17-_GLOBAL__sub_I_hexxagonboard.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	clone_lookups,@object   # @clone_lookups
	.bss
	.globl	clone_lookups
	.p2align	3
clone_lookups:
	.quad	0
	.size	clone_lookups, 8

	.type	jump_lookups,@object    # @jump_lookups
	.globl	jump_lookups
	.p2align	3
jump_lookups:
	.quad	0
	.size	jump_lookups, 8

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"         A B C D E F G H I\n"
	.size	.L.str.2, 28

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"        / / / / / / / / /\n"
	.size	.L.str.3, 27

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"- "
	.size	.L.str.4, 3

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	" "
	.size	.L.str.5, 2

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"x "
	.size	.L.str.6, 3

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"o "
	.size	.L.str.7, 3

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	". "
	.size	.L.str.8, 3

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"\n"
	.size	.L.str.9, 2

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"\nBricks: x "
	.size	.L.str.10, 12

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	", o "
	.size	.L.str.11, 5

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	". Empty "
	.size	.L.str.12, 9

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	".\n"
	.size	.L.str.13, 3

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Next to move: "
	.size	.L.str.14, 15

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"x"
	.size	.L.str.15, 2

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"o"
	.size	.L.str.16, 2

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	", Game over."
	.size	.L.str.17, 13

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.zero	1
	.size	.L.str.18, 1

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_hexxagonboard.ii
	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.1:
	.asciz	"No more moves."
	.size	.Lstr.1, 15


	.globl	_ZN13HexxagonBoardC1ERKS_
	.type	_ZN13HexxagonBoardC1ERKS_,@function
_ZN13HexxagonBoardC1ERKS_ = _ZN13HexxagonBoardC2ERKS_
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
