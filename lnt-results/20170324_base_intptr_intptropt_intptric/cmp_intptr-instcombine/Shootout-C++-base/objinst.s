	.text
	.file	"objinst.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	cmpl	$2, %edi
	jne	.LBB0_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
.LBB0_2:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r14
	movq	$_ZTV6Toggle+16, (%r14)
	movb	$0, 8(%r14)
	movl	$_ZSt4cout, %edi
	movl	$.L.str.1, %esi
	movl	$5, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#3:                                 # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit41
	cmpb	$0, 56(%rbx)
	je	.LBB0_5
# BB#4:
	movb	67(%rbx), %al
	jmp	.LBB0_6
.LBB0_5:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_6:                                # %_ZNKSt5ctypeIcE5widenEc.exit38
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
	xorl	%edx, %edx
	cmpb	$0, 8(%rax)
	movl	$.L.str, %eax
	movl	$.L.str.1, %esi
	cmovneq	%rax, %rsi
	sete	%dl
	orq	$4, %rdx
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#7:                                 # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit41.1
	cmpb	$0, 56(%rbx)
	je	.LBB0_45
# BB#8:
	movb	67(%rbx), %al
	jmp	.LBB0_46
.LBB0_45:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_46:                               # %_ZNKSt5ctypeIcE5widenEc.exit38.1
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
	xorl	%edx, %edx
	cmpb	$0, 8(%rax)
	movl	$.L.str, %eax
	movl	$.L.str.1, %esi
	cmovneq	%rax, %rsi
	sete	%dl
	orq	$4, %rdx
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#47:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit41.2
	cmpb	$0, 56(%rbx)
	je	.LBB0_49
# BB#48:
	movb	67(%rbx), %al
	jmp	.LBB0_50
.LBB0_49:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_50:                               # %_ZNKSt5ctypeIcE5widenEc.exit38.2
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
	xorl	%edx, %edx
	cmpb	$0, 8(%rax)
	movl	$.L.str, %eax
	movl	$.L.str.1, %esi
	cmovneq	%rax, %rsi
	sete	%dl
	orq	$4, %rdx
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#51:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit41.3
	cmpb	$0, 56(%rbx)
	je	.LBB0_53
# BB#52:
	movb	67(%rbx), %al
	jmp	.LBB0_54
.LBB0_53:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_54:                               # %_ZNKSt5ctypeIcE5widenEc.exit38.3
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
	xorl	%edx, %edx
	cmpb	$0, 8(%rax)
	movl	$.L.str, %eax
	movl	$.L.str.1, %esi
	cmovneq	%rax, %rsi
	sete	%dl
	orq	$4, %rdx
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#55:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit41.4
	cmpb	$0, 56(%rbx)
	je	.LBB0_57
# BB#56:
	movb	67(%rbx), %al
	jmp	.LBB0_58
.LBB0_57:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_58:                               # %_ZNKSt5ctypeIcE5widenEc.exit38.4
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#9:                                 # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit
	cmpb	$0, 56(%rbx)
	je	.LBB0_11
# BB#10:
	movb	67(%rbx), %al
	jmp	.LBB0_12
.LBB0_11:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_12:                               # %_ZN9NthToggle8activateEv.exit
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r14
	movb	$1, 8(%r14)
	movq	$_ZTV9NthToggle+16, (%r14)
	movabsq	$4294967299, %rax       # imm = 0x100000003
	movq	%rax, 12(%r14)
	movl	$_ZSt4cout, %edi
	movl	$.L.str, %esi
	movl	$4, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#13:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit42
	cmpb	$0, 56(%rbx)
	je	.LBB0_15
# BB#14:
	movb	67(%rbx), %al
	jmp	.LBB0_16
.LBB0_15:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_16:                               # %_ZNKSt5ctypeIcE5widenEc.exit40
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
	xorl	%edx, %edx
	cmpb	$0, 8(%rax)
	movl	$.L.str, %eax
	movl	$.L.str.1, %esi
	cmovneq	%rax, %rsi
	sete	%dl
	orq	$4, %rdx
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#17:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit42.1
	cmpb	$0, 56(%rbx)
	je	.LBB0_19
# BB#18:
	movb	67(%rbx), %al
	jmp	.LBB0_20
.LBB0_19:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_20:                               # %_ZNKSt5ctypeIcE5widenEc.exit40.1
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
	xorl	%edx, %edx
	cmpb	$0, 8(%rax)
	movl	$.L.str, %eax
	movl	$.L.str.1, %esi
	cmovneq	%rax, %rsi
	sete	%dl
	orq	$4, %rdx
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#21:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit42.2
	cmpb	$0, 56(%rbx)
	je	.LBB0_23
# BB#22:
	movb	67(%rbx), %al
	jmp	.LBB0_24
.LBB0_23:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_24:                               # %_ZNKSt5ctypeIcE5widenEc.exit40.2
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
	xorl	%edx, %edx
	cmpb	$0, 8(%rax)
	movl	$.L.str, %eax
	movl	$.L.str.1, %esi
	cmovneq	%rax, %rsi
	sete	%dl
	orq	$4, %rdx
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#25:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit42.3
	cmpb	$0, 56(%rbx)
	je	.LBB0_27
# BB#26:
	movb	67(%rbx), %al
	jmp	.LBB0_28
.LBB0_27:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_28:                               # %_ZNKSt5ctypeIcE5widenEc.exit40.3
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
	xorl	%edx, %edx
	cmpb	$0, 8(%rax)
	movl	$.L.str, %eax
	movl	$.L.str.1, %esi
	cmovneq	%rax, %rsi
	sete	%dl
	orq	$4, %rdx
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#29:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit42.4
	cmpb	$0, 56(%rbx)
	je	.LBB0_31
# BB#30:
	movb	67(%rbx), %al
	jmp	.LBB0_32
.LBB0_31:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_32:                               # %_ZNKSt5ctypeIcE5widenEc.exit40.4
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
	xorl	%edx, %edx
	cmpb	$0, 8(%rax)
	movl	$.L.str, %eax
	movl	$.L.str.1, %esi
	cmovneq	%rax, %rsi
	sete	%dl
	orq	$4, %rdx
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#33:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit42.5
	cmpb	$0, 56(%rbx)
	je	.LBB0_35
# BB#34:
	movb	67(%rbx), %al
	jmp	.LBB0_36
.LBB0_35:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_36:                               # %_ZNKSt5ctypeIcE5widenEc.exit40.5
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
	xorl	%edx, %edx
	cmpb	$0, 8(%rax)
	movl	$.L.str, %eax
	movl	$.L.str.1, %esi
	cmovneq	%rax, %rsi
	sete	%dl
	orq	$4, %rdx
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#37:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit42.6
	cmpb	$0, 56(%rbx)
	je	.LBB0_39
# BB#38:
	movb	67(%rbx), %al
	jmp	.LBB0_40
.LBB0_39:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_40:                               # %_ZNKSt5ctypeIcE5widenEc.exit40.6
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
	xorl	%edx, %edx
	cmpb	$0, 8(%rax)
	movl	$.L.str, %eax
	movl	$.L.str.1, %esi
	cmovneq	%rax, %rsi
	sete	%dl
	orq	$4, %rdx
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_59
# BB#41:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit42.7
	cmpb	$0, 56(%rbx)
	je	.LBB0_43
# BB#42:
	movb	67(%rbx), %al
	jmp	.LBB0_44
.LBB0_43:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_44:                               # %_ZNKSt5ctypeIcE5widenEc.exit40.7
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_59:
	callq	_ZSt16__throw_bad_castv
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.section	.text._ZN6ToggleD2Ev,"axG",@progbits,_ZN6ToggleD2Ev,comdat
	.weak	_ZN6ToggleD2Ev
	.p2align	4, 0x90
	.type	_ZN6ToggleD2Ev,@function
_ZN6ToggleD2Ev:                         # @_ZN6ToggleD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	_ZN6ToggleD2Ev, .Lfunc_end1-_ZN6ToggleD2Ev
	.cfi_endproc

	.section	.text._ZN6ToggleD0Ev,"axG",@progbits,_ZN6ToggleD0Ev,comdat
	.weak	_ZN6ToggleD0Ev
	.p2align	4, 0x90
	.type	_ZN6ToggleD0Ev,@function
_ZN6ToggleD0Ev:                         # @_ZN6ToggleD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end2:
	.size	_ZN6ToggleD0Ev, .Lfunc_end2-_ZN6ToggleD0Ev
	.cfi_endproc

	.section	.text._ZN6Toggle8activateEv,"axG",@progbits,_ZN6Toggle8activateEv,comdat
	.weak	_ZN6Toggle8activateEv
	.p2align	4, 0x90
	.type	_ZN6Toggle8activateEv,@function
_ZN6Toggle8activateEv:                  # @_ZN6Toggle8activateEv
	.cfi_startproc
# BB#0:
	xorb	$1, 8(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end3:
	.size	_ZN6Toggle8activateEv, .Lfunc_end3-_ZN6Toggle8activateEv
	.cfi_endproc

	.section	.text._ZN9NthToggleD0Ev,"axG",@progbits,_ZN9NthToggleD0Ev,comdat
	.weak	_ZN9NthToggleD0Ev
	.p2align	4, 0x90
	.type	_ZN9NthToggleD0Ev,@function
_ZN9NthToggleD0Ev:                      # @_ZN9NthToggleD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end4:
	.size	_ZN9NthToggleD0Ev, .Lfunc_end4-_ZN9NthToggleD0Ev
	.cfi_endproc

	.section	.text._ZN9NthToggle8activateEv,"axG",@progbits,_ZN9NthToggle8activateEv,comdat
	.weak	_ZN9NthToggle8activateEv
	.p2align	4, 0x90
	.type	_ZN9NthToggle8activateEv,@function
_ZN9NthToggle8activateEv:               # @_ZN9NthToggle8activateEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	cmpl	12(%rdi), %eax
	jl	.LBB5_2
# BB#1:
	xorb	$1, 8(%rdi)
	movl	$0, 16(%rdi)
.LBB5_2:
	movq	%rdi, %rax
	retq
.Lfunc_end5:
	.size	_ZN9NthToggle8activateEv, .Lfunc_end5-_ZN9NthToggle8activateEv
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_objinst.ii,@function
_GLOBAL__sub_I_objinst.ii:              # @_GLOBAL__sub_I_objinst.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end6:
	.size	_GLOBAL__sub_I_objinst.ii, .Lfunc_end6-_GLOBAL__sub_I_objinst.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"true"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"false"
	.size	.L.str.1, 6

	.type	_ZTV6Toggle,@object     # @_ZTV6Toggle
	.section	.rodata._ZTV6Toggle,"aG",@progbits,_ZTV6Toggle,comdat
	.weak	_ZTV6Toggle
	.p2align	3
_ZTV6Toggle:
	.quad	0
	.quad	_ZTI6Toggle
	.quad	_ZN6ToggleD2Ev
	.quad	_ZN6ToggleD0Ev
	.quad	_ZN6Toggle8activateEv
	.size	_ZTV6Toggle, 40

	.type	_ZTS6Toggle,@object     # @_ZTS6Toggle
	.section	.rodata._ZTS6Toggle,"aG",@progbits,_ZTS6Toggle,comdat
	.weak	_ZTS6Toggle
_ZTS6Toggle:
	.asciz	"6Toggle"
	.size	_ZTS6Toggle, 8

	.type	_ZTI6Toggle,@object     # @_ZTI6Toggle
	.section	.rodata._ZTI6Toggle,"aG",@progbits,_ZTI6Toggle,comdat
	.weak	_ZTI6Toggle
	.p2align	3
_ZTI6Toggle:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS6Toggle
	.size	_ZTI6Toggle, 16

	.type	_ZTV9NthToggle,@object  # @_ZTV9NthToggle
	.section	.rodata._ZTV9NthToggle,"aG",@progbits,_ZTV9NthToggle,comdat
	.weak	_ZTV9NthToggle
	.p2align	3
_ZTV9NthToggle:
	.quad	0
	.quad	_ZTI9NthToggle
	.quad	_ZN6ToggleD2Ev
	.quad	_ZN9NthToggleD0Ev
	.quad	_ZN9NthToggle8activateEv
	.size	_ZTV9NthToggle, 40

	.type	_ZTS9NthToggle,@object  # @_ZTS9NthToggle
	.section	.rodata._ZTS9NthToggle,"aG",@progbits,_ZTS9NthToggle,comdat
	.weak	_ZTS9NthToggle
_ZTS9NthToggle:
	.asciz	"9NthToggle"
	.size	_ZTS9NthToggle, 11

	.type	_ZTI9NthToggle,@object  # @_ZTI9NthToggle
	.section	.rodata._ZTI9NthToggle,"aG",@progbits,_ZTI9NthToggle,comdat
	.weak	_ZTI9NthToggle
	.p2align	4
_ZTI9NthToggle:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS9NthToggle
	.quad	_ZTI6Toggle
	.size	_ZTI9NthToggle, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_objinst.ii

	.ident	"clang version 5.0.0 (https://github.com/aqjune/clang-intptr.git 8e2e88f063bfae95e3be980dbf26473d5a086d27) (https://github.com/aqjune/llvm-intptr.git 51d41dcad277119de970b737044ce4a7e91ad33d)"
	.section	".note.GNU-stack","",@progbits
