	.text
	.file	"struct_stencil.bc"
	.globl	hypre_StructStencilCreate
	.p2align	4, 0x90
	.type	hypre_StructStencilCreate,@function
hypre_StructStencilCreate:              # @hypre_StructStencilCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %ebp
	movl	%edi, %r14d
	movl	$24, %edi
	callq	hypre_MAlloc
	movq	%rbx, (%rax)
	movl	%ebp, 8(%rax)
	movl	%r14d, 16(%rax)
	movl	$1, 20(%rax)
	testl	%ebp, %ebp
	jle	.LBB0_1
# BB#2:                                 # %.preheader.preheader
	movl	%ebp, %ecx
	addq	$8, %rbx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	-8(%rbx), %ebp
	movl	-4(%rbx), %r8d
	movl	%ebp, %esi
	negl	%esi
	cmovll	%ebp, %esi
	cmpl	%edx, %esi
	cmovll	%edx, %esi
	movl	%r8d, %edi
	negl	%edi
	cmovll	%r8d, %edi
	cmpl	%esi, %edi
	cmovll	%esi, %edi
	movl	(%rbx), %esi
	movl	%esi, %edx
	negl	%edx
	cmovll	%esi, %edx
	cmpl	%edi, %edx
	cmovll	%edi, %edx
	addq	$12, %rbx
	decq	%rcx
	jne	.LBB0_3
	jmp	.LBB0_4
.LBB0_1:
	xorl	%edx, %edx
.LBB0_4:                                # %._crit_edge
	movl	%edx, 12(%rax)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_StructStencilCreate, .Lfunc_end0-hypre_StructStencilCreate
	.cfi_endproc

	.globl	hypre_StructStencilRef
	.p2align	4, 0x90
	.type	hypre_StructStencilRef,@function
hypre_StructStencilRef:                 # @hypre_StructStencilRef
	.cfi_startproc
# BB#0:
	incl	20(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end1:
	.size	hypre_StructStencilRef, .Lfunc_end1-hypre_StructStencilRef
	.cfi_endproc

	.globl	hypre_StructStencilDestroy
	.p2align	4, 0x90
	.type	hypre_StructStencilDestroy,@function
hypre_StructStencilDestroy:             # @hypre_StructStencilDestroy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB2_3
# BB#1:
	decl	20(%rbx)
	jne	.LBB2_3
# BB#2:
	movq	(%rbx), %rdi
	callq	hypre_Free
	movq	$0, (%rbx)
	movq	%rbx, %rdi
	callq	hypre_Free
.LBB2_3:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	hypre_StructStencilDestroy, .Lfunc_end2-hypre_StructStencilDestroy
	.cfi_endproc

	.globl	hypre_StructStencilElementRank
	.p2align	4, 0x90
	.type	hypre_StructStencilElementRank,@function
hypre_StructStencilElementRank:         # @hypre_StructStencilElementRank
	.cfi_startproc
# BB#0:
	movslq	8(%rdi), %r8
	testq	%r8, %r8
	movl	$-1, %eax
	jle	.LBB3_7
# BB#1:                                 # %.lr.ph
	movq	(%rdi), %rdx
	movl	(%rsi), %r9d
	addq	$8, %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	cmpl	%r9d, -8(%rdx)
	jne	.LBB3_5
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	-4(%rdx), %edi
	cmpl	4(%rsi), %edi
	jne	.LBB3_5
# BB#4:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	(%rdx), %edi
	cmpl	8(%rsi), %edi
	je	.LBB3_6
.LBB3_5:                                #   in Loop: Header=BB3_2 Depth=1
	incq	%rcx
	addq	$12, %rdx
	cmpq	%r8, %rcx
	jl	.LBB3_2
	jmp	.LBB3_7
.LBB3_6:                                # %.._crit_edge.loopexit_crit_edge
	movl	%ecx, %eax
.LBB3_7:                                # %._crit_edge
	retq
.Lfunc_end3:
	.size	hypre_StructStencilElementRank, .Lfunc_end3-hypre_StructStencilElementRank
	.cfi_endproc

	.globl	hypre_StructStencilSymmetrize
	.p2align	4, 0x90
	.type	hypre_StructStencilSymmetrize,@function
hypre_StructStencilSymmetrize:          # @hypre_StructStencilSymmetrize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 96
.Lcfi15:
	.cfi_offset %rbx, -56
.Lcfi16:
	.cfi_offset %r12, -48
.Lcfi17:
	.cfi_offset %r13, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	(%rdi), %r15
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	8(%rdi), %r14d
	leal	(%r14,%r14), %ebp
	movl	$12, %esi
	movl	%ebp, %edi
	callq	hypre_CAlloc
	movq	%rax, %r13
	testl	%r14d, %r14d
	jle	.LBB4_27
# BB#1:                                 # %.lr.ph113.preheader
	testb	$1, %r14b
	jne	.LBB4_3
# BB#2:
	xorl	%ecx, %ecx
	cmpl	$1, %r14d
	jne	.LBB4_5
	jmp	.LBB4_7
.LBB4_27:                               # %._crit_edge114.thread
	movl	$4, %esi
	movl	%ebp, %edi
	callq	hypre_CAlloc
	movq	%rax, (%rsp)            # 8-byte Spill
	jmp	.LBB4_22
.LBB4_3:                                # %.lr.ph113.prol
	movl	(%r15), %eax
	movl	%eax, (%r13)
	movl	4(%r15), %eax
	movl	%eax, 4(%r13)
	movl	8(%r15), %eax
	movl	%eax, 8(%r13)
	movl	$1, %ecx
	cmpl	$1, %r14d
	je	.LBB4_7
.LBB4_5:                                # %.lr.ph113.preheader.new
	movq	%r14, %rax
	subq	%rcx, %rax
	shlq	$2, %rcx
	leaq	(%rcx,%rcx,2), %rdx
	leaq	20(%r15,%rdx), %rcx
	leaq	20(%r13,%rdx), %rdx
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph113
                                        # =>This Inner Loop Header: Depth=1
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$24, %rcx
	addq	$24, %rdx
	addq	$-2, %rax
	jne	.LBB4_6
.LBB4_7:                                # %._crit_edge114
	movl	$4, %esi
	movl	%ebp, %edi
	callq	hypre_CAlloc
	testl	%r14d, %r14d
	movq	%rax, (%rsp)            # 8-byte Spill
	jle	.LBB4_22
# BB#8:                                 # %.lr.ph105.preheader
	movq	%r12, 24(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	movl	$1, %ecx
	cmovgl	%ebp, %ecx
	decl	%ecx
	leaq	4(,%rcx,4), %rdx
	movl	$255, %esi
	movq	%rax, %rdi
	callq	memset
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	8(%r13), %r15
	movq	%r14, %rcx
	negq	%rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	movq	%rax, %r12
	movl	%r14d, %r9d
	.p2align	4, 0x90
.LBB4_9:                                # %.lr.ph105
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_11 Depth 2
	cmpl	$0, (%rax,%rdx,4)
	jns	.LBB4_20
# BB#10:                                # %.lr.ph
                                        #   in Loop: Header=BB4_9 Depth=1
	leaq	(%rdx,%rdx,2), %r10
	leaq	(%r13,%r10,4), %rcx
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx), %rsi
	movl	$1, %r11d
	xorl	%ebp, %ebp
	movq	%r15, %rax
	movq	%r12, %rdi
	.p2align	4, 0x90
.LBB4_11:                               #   Parent Loop BB4_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-8(%rax), %r8d
	addl	(%rcx), %r8d
	jne	.LBB4_16
# BB#12:                                #   in Loop: Header=BB4_11 Depth=2
	movl	-4(%rax), %ebx
	addl	4(%rcx), %ebx
	jne	.LBB4_16
# BB#13:                                #   in Loop: Header=BB4_11 Depth=2
	movl	(%rax), %ebx
	addl	8(%rcx), %ebx
	jne	.LBB4_16
# BB#14:                                #   in Loop: Header=BB4_11 Depth=2
	xorl	%r11d, %r11d
	testq	%rbp, %rbp
	je	.LBB4_16
# BB#15:                                #   in Loop: Header=BB4_11 Depth=2
	movl	%edx, (%rdi)
	.p2align	4, 0x90
.LBB4_16:                               #   in Loop: Header=BB4_11 Depth=2
	addq	$4, %rdi
	addq	$12, %rax
	decq	%rbp
	cmpq	%rbp, %rsi
	jne	.LBB4_11
# BB#17:                                # %._crit_edge
                                        #   in Loop: Header=BB4_9 Depth=1
	testl	%r11d, %r11d
	je	.LBB4_18
# BB#19:                                # %.preheader
                                        #   in Loop: Header=BB4_9 Depth=1
	movslq	%r9d, %r9
	xorl	%eax, %eax
	subl	(%r13,%r10,4), %eax
	leaq	(%r9,%r9,2), %rcx
	movl	%eax, (%r13,%rcx,4)
	xorl	%eax, %eax
	subl	4(%r13,%r10,4), %eax
	movl	%eax, 4(%r13,%rcx,4)
	xorl	%eax, %eax
	subl	8(%r13,%r10,4), %eax
	movl	%eax, 8(%r13,%rcx,4)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%edx, (%rax,%r9,4)
	incl	%r9d
	jmp	.LBB4_20
.LBB4_18:                               #   in Loop: Header=BB4_9 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	.p2align	4, 0x90
.LBB4_20:                               #   in Loop: Header=BB4_9 Depth=1
	incq	%rdx
	addq	$4, %r12
	addq	$12, %r15
	cmpq	%r14, %rdx
	jne	.LBB4_9
# BB#21:
	movl	%r9d, %r14d
	movq	24(%rsp), %r12          # 8-byte Reload
.LBB4_22:                               # %._crit_edge106
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	16(%rax), %ebx
	movl	$24, %edi
	callq	hypre_MAlloc
	movq	%r13, (%rax)
	movl	%r14d, 8(%rax)
	movl	%ebx, 16(%rax)
	movl	$1, 20(%rax)
	testl	%r14d, %r14d
	jle	.LBB4_23
# BB#24:                                # %.preheader.preheader.i
	movl	%r14d, %ecx
	addq	$8, %r13
	xorl	%edx, %edx
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	(%rsp), %r9             # 8-byte Reload
	.p2align	4, 0x90
.LBB4_25:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-8(%r13), %ebp
	movl	-4(%r13), %ebx
	movl	%ebp, %esi
	negl	%esi
	cmovll	%ebp, %esi
	cmpl	%edx, %esi
	cmovll	%edx, %esi
	movl	%ebx, %edi
	negl	%edi
	cmovll	%ebx, %edi
	cmpl	%esi, %edi
	cmovll	%esi, %edi
	movl	(%r13), %esi
	movl	%esi, %edx
	negl	%edx
	cmovll	%esi, %edx
	cmpl	%edi, %edx
	cmovll	%edi, %edx
	addq	$12, %r13
	decq	%rcx
	jne	.LBB4_25
	jmp	.LBB4_26
.LBB4_23:
	xorl	%edx, %edx
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	(%rsp), %r9             # 8-byte Reload
.LBB4_26:                               # %hypre_StructStencilCreate.exit
	movl	%edx, 12(%rax)
	movq	%rax, (%r8)
	movq	%r9, (%r12)
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	hypre_StructStencilSymmetrize, .Lfunc_end4-hypre_StructStencilSymmetrize
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
