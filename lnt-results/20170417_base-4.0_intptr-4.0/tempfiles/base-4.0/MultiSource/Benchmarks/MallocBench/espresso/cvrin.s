	.text
	.file	"cvrin.bc"
	.globl	skip_line
	.p2align	4, 0x90
	.type	skip_line,@function
skip_line:                              # @skip_line
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	testl	%r14d, %r14d
	jne	.LBB0_1
	.p2align	4, 0x90
.LBB0_4:                                # %.split
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB0_6
# BB#5:                                 # %.split
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpl	$-1, %eax
	jne	.LBB0_4
	jmp	.LBB0_6
	.p2align	4, 0x90
.LBB0_3:                                # %.backedge.us
                                        #   in Loop: Header=BB0_1 Depth=1
	movl	%eax, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
.LBB0_1:                                # %.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB0_6
# BB#2:                                 # %.split.us
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpl	$10, %eax
	jne	.LBB0_3
.LBB0_6:                                # %.us-lcssa.us
	testl	%r14d, %r14d
	je	.LBB0_8
# BB#7:
	movl	$10, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
.LBB0_8:
	incl	lineno(%rip)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	skip_line, .Lfunc_end0-skip_line
	.cfi_endproc

	.globl	get_word
	.p2align	4, 0x90
	.type	get_word,@function
get_word:                               # @get_word
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 48
.Lcfi11:
	.cfi_offset %rbx, -48
.Lcfi12:
	.cfi_offset %r12, -40
.Lcfi13:
	.cfi_offset %r14, -32
.Lcfi14:
	.cfi_offset %r15, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	je	.LBB1_2
# BB#3:                                 #   in Loop: Header=BB1_1 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movslq	%ebp, %rcx
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB1_1
	jmp	.LBB1_4
.LBB1_2:
	movl	$-1, %ebp
.LBB1_4:                                # %.critedge.preheader
	movb	%bpl, (%r14)
	movq	%r12, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	je	.LBB1_5
# BB#6:                                 # %.lr.ph
	movl	$1, %ebx
	callq	__ctype_b_loc
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB1_7:                                # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movslq	%ebp, %rcx
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB1_9
# BB#8:                                 # %.critedge
                                        #   in Loop: Header=BB1_7 Depth=1
	movb	%bpl, (%r14,%rbx)
	incq	%rbx
	movq	%r12, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB1_7
	jmp	.LBB1_9
.LBB1_5:
	movl	$1, %ebx
.LBB1_9:                                # %.critedge5
	movslq	%ebx, %rax
	movb	$0, (%r14,%rax)
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	get_word, .Lfunc_end1-get_word
	.cfi_endproc

	.globl	read_cube
	.p2align	4, 0x90
	.type	read_cube,@function
read_cube:                              # @read_cube
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi22:
	.cfi_def_cfa_offset 368
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	cube+80(%rip), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	16(%rax), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	callq	set_clear
	movl	cube+8(%rip), %ebp
	testl	%ebp, %ebp
	jle	.LBB2_8
# BB#1:                                 # %.lr.ph286.preheader
	xorl	%ebx, %ebx
	jmp	.LBB2_2
.LBB2_22:                               #   in Loop: Header=BB2_2 Depth=1
	movzbl	line_length_error(%rip), %eax
	testb	%al, %al
	je	.LBB2_23
.LBB2_24:                               #   in Loop: Header=BB2_2 Depth=1
	movb	$1, line_length_error(%rip)
	incl	lineno(%rip)
.LBB2_25:                               #   in Loop: Header=BB2_2 Depth=1
	decl	%ebx
	jmp	.LBB2_29
.LBB2_21:                               # %.lr.ph286._crit_edge
                                        #   in Loop: Header=BB2_2 Depth=1
	leal	(%rbx,%rbx), %eax
	andl	$30, %eax
	movl	%ebx, %ecx
	sarl	$4, %ecx
	movslq	%ecx, %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	4(%rdx,%rcx,4), %rdx
.LBB2_27:                               #   in Loop: Header=BB2_2 Depth=1
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	orl	%esi, (%rdx)
	jmp	.LBB2_29
.LBB2_28:                               #   in Loop: Header=BB2_2 Depth=1
	movl	%ebx, %eax
	andl	$15, %eax
	leal	1(%rax,%rax), %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movl	%ebx, %ecx
	sarl	$4, %ecx
	movslq	%ecx, %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	orl	%eax, 4(%rdx,%rcx,4)
	.p2align	4, 0x90
.LBB2_29:                               #   in Loop: Header=BB2_2 Depth=1
	incl	%ebx
	movl	cube+8(%rip), %ebp
	cmpl	%ebp, %ebx
	jl	.LBB2_2
	jmp	.LBB2_8
.LBB2_23:                               #   in Loop: Header=BB2_2 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB2_24
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph286
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-9(%rax), %ecx
	cmpl	$54, %ecx
	ja	.LBB2_3
# BB#20:                                # %.lr.ph286
                                        #   in Loop: Header=BB2_2 Depth=1
	jmpq	*.LJTI2_0(,%rcx,8)
.LBB2_26:                               #   in Loop: Header=BB2_2 Depth=1
	leal	(%rbx,%rbx), %eax
	andl	$30, %eax
	leal	1(%rax), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movl	%ebx, %ecx
	sarl	$4, %ecx
	movslq	%ecx, %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	leaq	4(%rdi,%rcx,4), %rdx
	orl	%esi, 4(%rdi,%rcx,4)
	jmp	.LBB2_27
.LBB2_3:                                # %.lr.ph286
                                        #   in Loop: Header=BB2_2 Depth=1
	cmpl	$124, %eax
	je	.LBB2_25
	jmp	.LBB2_4
.LBB2_8:                                # %.preheader
	movl	cube+4(%rip), %eax
	leal	-1(%rax), %ecx
	cmpl	%ecx, %ebp
	jge	.LBB2_55
# BB#9:                                 # %.lr.ph279
	movslq	%ebp, %rbp
.LBB2_10:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_45 Depth 2
                                        #     Child Loop BB2_38 Depth 2
	movq	cube+32(%rip), %rax
	cmpl	$0, (%rax,%rbp,4)
	js	.LBB2_11
# BB#44:                                #   in Loop: Header=BB2_10 Depth=1
	movq	cube+16(%rip), %rax
	movl	(%rax,%rbp,4), %ebx
	movq	cube+24(%rip), %rax
	cmpl	(%rax,%rbp,4), %ebx
	jle	.LBB2_45
	jmp	.LBB2_54
	.p2align	4, 0x90
.LBB2_11:                               #   in Loop: Header=BB2_10 Depth=1
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	leaq	48(%rsp), %rdx
	callq	fscanf
	movb	48(%rsp), %al
	cmpb	$45, %al
	jne	.LBB2_13
# BB#12:                                #   in Loop: Header=BB2_10 Depth=1
	movb	49(%rsp), %cl
	testb	%cl, %cl
	je	.LBB2_17
.LBB2_13:                               # %.thread318
                                        #   in Loop: Header=BB2_10 Depth=1
	cmpb	$65, %al
	jne	.LBB2_30
# BB#14:                                # %.thread318
                                        #   in Loop: Header=BB2_10 Depth=1
	cmpb	$78, 49(%rsp)
	jne	.LBB2_30
# BB#15:                                # %.thread318
                                        #   in Loop: Header=BB2_10 Depth=1
	cmpb	$89, 50(%rsp)
	jne	.LBB2_30
# BB#16:                                # %.thread318
                                        #   in Loop: Header=BB2_10 Depth=1
	movb	51(%rsp), %cl
	testb	%cl, %cl
	jne	.LBB2_30
.LBB2_17:                               #   in Loop: Header=BB2_10 Depth=1
	cmpl	$0, kiss(%rip)
	je	.LBB2_19
# BB#18:                                #   in Loop: Header=BB2_10 Depth=1
	movl	cube+4(%rip), %eax
	addl	$-2, %eax
	cmpl	%eax, %ebp
	je	.LBB2_54
.LBB2_19:                               #   in Loop: Header=BB2_10 Depth=1
	movq	cube+72(%rip), %rax
	movq	(%rax,%rbp,8), %rdx
	xorl	%eax, %eax
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rdi, %rsi
	callq	set_or
	jmp	.LBB2_54
.LBB2_30:                               # %.thread319
                                        #   in Loop: Header=BB2_10 Depth=1
	cmpb	$126, %al
	jne	.LBB2_32
# BB#31:                                # %.thread319
                                        #   in Loop: Header=BB2_10 Depth=1
	movb	49(%rsp), %al
	testb	%al, %al
	je	.LBB2_54
.LBB2_32:                               # %.thread320
                                        #   in Loop: Header=BB2_10 Depth=1
	xorl	%r12d, %r12d
	cmpl	$0, kiss(%rip)
	movq	%r15, 32(%rsp)          # 8-byte Spill
	je	.LBB2_33
# BB#34:                                #   in Loop: Header=BB2_10 Depth=1
	movl	cube+4(%rip), %eax
	addl	$-2, %eax
	cmpl	%eax, %ebp
	movl	%ebp, %eax
	jne	.LBB2_36
# BB#35:                                #   in Loop: Header=BB2_10 Depth=1
	leal	-1(%rbp), %eax
	movq	cube+32(%rip), %rcx
	movl	-4(%rcx,%rbp,4), %ecx
	movl	%ecx, %r12d
	negl	%r12d
	cmovll	%ecx, %r12d
	jmp	.LBB2_36
.LBB2_49:                               #   in Loop: Header=BB2_45 Depth=2
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB2_50
	.p2align	4, 0x90
.LBB2_45:                               # %.lr.ph269
                                        #   Parent Loop BB2_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-9(%rax), %ecx
	cmpl	$40, %ecx
	ja	.LBB2_46
# BB#47:                                # %.lr.ph269
                                        #   in Loop: Header=BB2_45 Depth=2
	jmpq	*.LJTI2_1(,%rcx,8)
.LBB2_48:                               #   in Loop: Header=BB2_45 Depth=2
	movzbl	line_length_error(%rip), %eax
	testb	%al, %al
	je	.LBB2_49
.LBB2_50:                               #   in Loop: Header=BB2_45 Depth=2
	movb	$1, line_length_error(%rip)
	incl	lineno(%rip)
	jmp	.LBB2_51
.LBB2_46:                               # %.lr.ph269
                                        #   in Loop: Header=BB2_45 Depth=2
	cmpl	$124, %eax
	jne	.LBB2_4
.LBB2_51:                               #   in Loop: Header=BB2_45 Depth=2
	decl	%ebx
	jmp	.LBB2_53
.LBB2_52:                               #   in Loop: Header=BB2_45 Depth=2
	movl	$1, %eax
	movl	%ebx, %ecx
	shll	%cl, %eax
	movl	%ebx, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	orl	%eax, 4(%rdx,%rcx,4)
	.p2align	4, 0x90
.LBB2_53:                               #   in Loop: Header=BB2_45 Depth=2
	movq	cube+24(%rip), %rax
	cmpl	(%rax,%rbp,4), %ebx
	leal	1(%rbx), %eax
	movl	%eax, %ebx
	jl	.LBB2_45
	jmp	.LBB2_54
.LBB2_33:                               #   in Loop: Header=BB2_10 Depth=1
	movl	%ebp, %eax
.LBB2_36:                               #   in Loop: Header=BB2_10 Depth=1
	movq	cube+16(%rip), %rcx
	cltq
	movslq	(%rcx,%rax,4), %rbx
	movq	cube+24(%rip), %rcx
	movslq	(%rcx,%rax,4), %r15
	cmpl	%r15d, %ebx
	jg	.LBB2_43
# BB#37:                                # %.lr.ph272
                                        #   in Loop: Header=BB2_10 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %r13
	addl	%ebx, %r12d
	decq	%rbx
	.p2align	4, 0x90
.LBB2_38:                               #   Parent Loop BB2_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB2_39
# BB#41:                                #   in Loop: Header=BB2_38 Depth=2
	leaq	48(%rsp), %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB2_40
# BB#42:                                #   in Loop: Header=BB2_38 Depth=2
	incl	%r12d
	incq	%rbx
	cmpq	%r15, %rbx
	jl	.LBB2_38
	jmp	.LBB2_43
.LBB2_39:                               #   in Loop: Header=BB2_10 Depth=1
	leaq	48(%rsp), %rdi
	callq	util_strsav
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	56(%rcx), %rcx
	movq	%rax, 8(%rcx,%rbx,8)
.LBB2_40:                               # %.loopexit234
                                        #   in Loop: Header=BB2_10 Depth=1
	movl	$1, %eax
	movl	%r12d, %ecx
	shll	%cl, %eax
	sarl	$5, %r12d
	movslq	%r12d, %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	orl	%eax, 4(%rdx,%rcx,4)
	movq	32(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_54:                               # %.loopexit235
                                        #   in Loop: Header=BB2_10 Depth=1
	incq	%rbp
	movslq	cube+4(%rip), %rax
	leaq	-1(%rax), %rcx
	cmpq	%rcx, %rbp
	jl	.LBB2_10
.LBB2_55:                               # %._crit_edge280
	cmpl	$0, kiss(%rip)
	movq	8(%rsp), %rbx           # 8-byte Reload
	je	.LBB2_57
# BB#56:
	movq	cube+72(%rip), %rcx
	cltq
	movq	-16(%rcx,%rax,8), %rdx
	xorl	%eax, %eax
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	set_xor
	movl	$1, %r13d
	jmp	.LBB2_58
.LBB2_57:
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	set_copy
.LBB2_58:
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	set_copy
	movq	cube+16(%rip), %rax
	movslq	%ebp, %r12
	movl	(%rax,%r12,4), %ebp
	movq	cube+24(%rip), %rax
	cmpl	(%rax,%r12,4), %ebp
	jle	.LBB2_60
# BB#59:
	movl	%r13d, %ebx
	testl	%r13d, %r13d
	jne	.LBB2_77
	jmp	.LBB2_78
.LBB2_60:                               # %.lr.ph
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	movl	%r13d, %ebx
	jmp	.LBB2_61
.LBB2_70:                               #   in Loop: Header=BB2_61 Depth=1
	testb	$4, 32(%r15)
	je	.LBB2_75
# BB#71:                                #   in Loop: Header=BB2_61 Depth=1
	movl	$1, %ebx
	movl	$1, %eax
	movl	%ebp, %ecx
	shll	%cl, %eax
	movl	%ebp, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movq	24(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB2_74
.LBB2_68:                               #   in Loop: Header=BB2_61 Depth=1
	testb	$1, 32(%r15)
	je	.LBB2_75
# BB#69:                                #   in Loop: Header=BB2_61 Depth=1
	movl	$1, %r13d
	movl	$1, %eax
	movl	%ebp, %ecx
	shll	%cl, %eax
	movl	%ebp, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB2_74:                               #   in Loop: Header=BB2_61 Depth=1
	orl	%eax, 4(%rdx,%rcx,4)
	jmp	.LBB2_75
.LBB2_64:                               #   in Loop: Header=BB2_61 Depth=1
	movzbl	line_length_error(%rip), %eax
	testb	%al, %al
	je	.LBB2_65
.LBB2_66:                               #   in Loop: Header=BB2_61 Depth=1
	movb	$1, line_length_error(%rip)
	incl	lineno(%rip)
.LBB2_67:                               #   in Loop: Header=BB2_61 Depth=1
	decl	%ebp
.LBB2_75:                               #   in Loop: Header=BB2_61 Depth=1
	movq	cube+24(%rip), %rax
	cmpl	(%rax,%r12,4), %ebp
	leal	1(%rbp), %eax
	movl	%eax, %ebp
	jl	.LBB2_61
	jmp	.LBB2_76
.LBB2_65:                               #   in Loop: Header=BB2_61 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB2_66
	.p2align	4, 0x90
.LBB2_61:                               # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-9(%rax), %ecx
	cmpl	$43, %ecx
	ja	.LBB2_62
# BB#84:                                #   in Loop: Header=BB2_61 Depth=1
	jmpq	*.LJTI2_2(,%rcx,8)
.LBB2_72:                               #   in Loop: Header=BB2_61 Depth=1
	testb	$2, 32(%r15)
	je	.LBB2_75
# BB#73:                                #   in Loop: Header=BB2_61 Depth=1
	movl	$1, 20(%rsp)            # 4-byte Folded Spill
	movl	$1, %eax
	movl	%ebp, %ecx
	shll	%cl, %eax
	movl	%ebp, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movq	40(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB2_74
	.p2align	4, 0x90
.LBB2_62:                               #   in Loop: Header=BB2_61 Depth=1
	cmpl	$124, %eax
	je	.LBB2_67
# BB#63:                                #   in Loop: Header=BB2_61 Depth=1
	cmpl	$126, %eax
	je	.LBB2_75
.LBB2_4:                                # %.loopexit
	movq	stderr(%rip), %rdi
	movl	lineno(%rip), %edx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stdout(%rip), %rbx
	jmp	.LBB2_5
	.p2align	4, 0x90
.LBB2_7:                                # %.backedge.us.i
                                        #   in Loop: Header=BB2_5 Depth=1
	movl	%eax, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
.LBB2_5:                                # %.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB2_82
# BB#6:                                 # %.split.us.i
                                        #   in Loop: Header=BB2_5 Depth=1
	cmpl	$10, %eax
	jne	.LBB2_7
.LBB2_82:                               # %skip_line.exit
	movl	$10, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	incl	lineno(%rip)
.LBB2_83:
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_76:                               # %._crit_edge
	testl	%r13d, %r13d
	je	.LBB2_78
.LBB2_77:
	movq	(%r15), %rdi
	xorl	%eax, %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	sf_addset
	movq	%rax, (%r15)
.LBB2_78:
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB2_80
# BB#79:
	movq	8(%r15), %rdi
	xorl	%eax, %eax
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	sf_addset
	movq	%rax, 8(%r15)
.LBB2_80:
	testl	%ebx, %ebx
	je	.LBB2_83
# BB#81:
	movq	16(%r15), %rdi
	xorl	%eax, %eax
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	sf_addset
	movq	%rax, 16(%r15)
	jmp	.LBB2_83
.LBB2_43:                               # %.thread
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	fprintf
	movl	$-1, %edi
	callq	exit
.Lfunc_end2:
	.size	read_cube, .Lfunc_end2-read_cube
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_25
	.quad	.LBB2_22
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_25
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_26
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_21
	.quad	.LBB2_28
	.quad	.LBB2_26
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_29
.LJTI2_1:
	.quad	.LBB2_51
	.quad	.LBB2_48
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_51
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_53
	.quad	.LBB2_52
.LJTI2_2:
	.quad	.LBB2_67
	.quad	.LBB2_64
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_67
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_72
	.quad	.LBB2_4
	.quad	.LBB2_4
	.quad	.LBB2_70
	.quad	.LBB2_68
	.quad	.LBB2_72
	.quad	.LBB2_70
	.quad	.LBB2_68

	.text
	.globl	parse_pla
	.p2align	4, 0x90
	.type	parse_pla,@function
parse_pla:                              # @parse_pla
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$344, %rsp              # imm = 0x158
.Lcfi35:
	.cfi_def_cfa_offset 400
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	$1, lineno(%rip)
	movb	$0, line_length_error(%rip)
	leaq	80(%rsp), %r12
	jmp	.LBB3_1
	.p2align	4, 0x90
.LBB3_236:                              # %.backedge.us.i616
                                        #   in Loop: Header=BB3_234 Depth=2
	movl	%eax, %edi
	movq	%rbp, %rsi
	callq	_IO_putc
.LBB3_234:                              # %.split.us.i615
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB3_239
# BB#235:                               # %.split.us.i615
                                        #   in Loop: Header=BB3_234 Depth=2
	cmpl	$10, %eax
	jne	.LBB3_236
.LBB3_239:                              #   in Loop: Header=BB3_1 Depth=1
	movl	$10, %edi
	movq	%rbp, %rsi
	callq	_IO_putc
.LBB3_240:                              # %skip_line.exit619
                                        #   in Loop: Header=BB3_1 Depth=1
	incl	lineno(%rip)
	jmp	.LBB3_1
.LBB3_44:                               # %.thread699
                                        #   in Loop: Header=BB3_1 Depth=1
	cmpb	$109, %bpl
	jne	.LBB3_75
# BB#45:                                # %.thread699
                                        #   in Loop: Header=BB3_1 Depth=1
	cmpb	$118, 81(%rsp)
	jne	.LBB3_75
# BB#46:                                # %.thread699
                                        #   in Loop: Header=BB3_1 Depth=1
	movb	82(%rsp), %al
	testb	%al, %al
	jne	.LBB3_75
# BB#47:                                #   in Loop: Header=BB3_1 Depth=1
	cmpq	$0, cube+88(%rip)
	jne	.LBB3_48
# BB#52:                                #   in Loop: Header=BB3_1 Depth=1
	cmpq	$0, cube+32(%rip)
	je	.LBB3_54
# BB#53:                                #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB3_54:                               #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.19, %esi
	movl	$cube+4, %edx
	movl	$cube+8, %ecx
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fscanf
	cmpl	$2, %eax
	je	.LBB3_56
# BB#55:                                #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB3_56:                               #   in Loop: Header=BB3_1 Depth=1
	movl	cube+8(%rip), %ebp
	testl	%ebp, %ebp
	jns	.LBB3_58
# BB#57:                                #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	fatal
	movl	cube+8(%rip), %ebp
.LBB3_58:                               #   in Loop: Header=BB3_1 Depth=1
	movl	cube+4(%rip), %eax
	cmpl	%ebp, %eax
	jge	.LBB3_60
# BB#59:                                #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	fatal
	movl	cube+4(%rip), %eax
	movl	cube+8(%rip), %ebp
.LBB3_60:                               #   in Loop: Header=BB3_1 Depth=1
	movslq	%eax, %rbx
	leaq	(,%rbx,4), %rdi
	callq	malloc
	movq	%rax, cube+32(%rip)
	movl	%ebp, 4(%rsp)
	cmpl	%ebx, %ebp
	jl	.LBB3_61
	jmp	.LBB3_65
	.p2align	4, 0x90
.LBB3_64:                               # %..lr.ph660_crit_edge
                                        #   in Loop: Header=BB3_61 Depth=2
	movq	cube+32(%rip), %rax
.LBB3_61:                               # %.lr.ph660
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebp, %rcx
	leaq	(%rax,%rcx,4), %rdx
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fscanf
	cmpl	$1, %eax
	je	.LBB3_63
# BB#62:                                #   in Loop: Header=BB3_61 Depth=2
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB3_63:                               #   in Loop: Header=BB3_61 Depth=2
	movl	4(%rsp), %ebp
	incl	%ebp
	movl	%ebp, 4(%rsp)
	cmpl	cube+4(%rip), %ebp
	jl	.LBB3_64
.LBB3_65:                               # %._crit_edge
                                        #   in Loop: Header=BB3_1 Depth=1
	xorl	%eax, %eax
	callq	cube_setup
	movslq	cube(%rip), %rbp
	leaq	(,%rbp,8), %rdi
	callq	malloc
	testq	%rbp, %rbp
	movq	%rax, 56(%r14)
	jle	.LBB3_1
# BB#66:                                # %.lr.ph.i569
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	$0, (%rax)
	cmpl	$1, %ebp
	je	.LBB3_1
# BB#67:                                # %._crit_edge7.i573.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	$0, 8(%rax)
	cmpl	$3, %ebp
	jl	.LBB3_1
# BB#68:                                # %._crit_edge7.i573.._crit_edge7.i573_crit_edge.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	leal	6(%rbp), %edx
	leaq	-3(%rbp), %rcx
	andq	$7, %rdx
	je	.LBB3_69
# BB#70:                                # %._crit_edge7.i573.._crit_edge7.i573_crit_edge.prol.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_71:                               # %._crit_edge7.i573.._crit_edge7.i573_crit_edge.prol
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r14), %rsi
	movq	$0, 16(%rsi,%rax,8)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB3_71
# BB#72:                                # %._crit_edge7.i573.._crit_edge7.i573_crit_edge.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB3_1 Depth=1
	addq	$2, %rax
	cmpq	$7, %rcx
	jb	.LBB3_1
	jmp	.LBB3_74
.LBB3_75:                               # %.thread700
                                        #   in Loop: Header=BB3_1 Depth=1
	cmpb	$112, %bpl
	jne	.LBB3_78
# BB#76:                                # %.thread700
                                        #   in Loop: Header=BB3_1 Depth=1
	movb	81(%rsp), %al
	testb	%al, %al
	jne	.LBB3_78
# BB#77:                                #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	leaq	76(%rsp), %rdx
	callq	fscanf
	jmp	.LBB3_1
.LBB3_78:                               # %.thread701
                                        #   in Loop: Header=BB3_1 Depth=1
	cmpb	$101, %bpl
	jne	.LBB3_83
# BB#79:                                #   in Loop: Header=BB3_1 Depth=1
	cmpb	$0, 81(%rsp)
	je	.LBB3_244
# BB#80:                                #   in Loop: Header=BB3_1 Depth=1
	cmpb	$110, 81(%rsp)
	jne	.LBB3_83
# BB#81:                                #   in Loop: Header=BB3_1 Depth=1
	cmpb	$100, 82(%rsp)
	jne	.LBB3_83
# BB#82:                                #   in Loop: Header=BB3_1 Depth=1
	movb	83(%rsp), %al
	testb	%al, %al
	je	.LBB3_244
.LBB3_83:                               # %.thread705
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.26, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_84
# BB#85:                                #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.27, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_86
# BB#93:                                #   in Loop: Header=BB3_1 Depth=1
	cmpb	$105, %bpl
	jne	.LBB3_111
# BB#94:                                #   in Loop: Header=BB3_1 Depth=1
	cmpb	$108, 81(%rsp)
	jne	.LBB3_111
# BB#95:                                #   in Loop: Header=BB3_1 Depth=1
	cmpb	$98, 82(%rsp)
	jne	.LBB3_111
# BB#96:                                #   in Loop: Header=BB3_1 Depth=1
	movb	83(%rsp), %al
	testb	%al, %al
	jne	.LBB3_111
# BB#97:                                #   in Loop: Header=BB3_1 Depth=1
	cmpq	$0, cube+88(%rip)
	jne	.LBB3_99
# BB#98:                                #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB3_99:                               #   in Loop: Header=BB3_1 Depth=1
	cmpq	$0, 56(%r14)
	je	.LBB3_100
.LBB3_109:                              # %PLA_labels.exit580.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	$0, 4(%rsp)
	cmpl	$0, cube+8(%rip)
	jle	.LBB3_1
	.p2align	4, 0x90
.LBB3_110:                              # %PLA_labels.exit580
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	get_word
	movq	cube+16(%rip), %rax
	movslq	4(%rsp), %rcx
	movslq	(%rax,%rcx,4), %rbx
	movq	%r12, %rdi
	callq	util_strsav
	movq	56(%r14), %rcx
	movq	%rax, 8(%rcx,%rbx,8)
	movq	%r12, %rdi
	callq	strlen
	leaq	6(%rax), %rdi
	callq	malloc
	movq	56(%r14), %rcx
	movq	%rax, (%rcx,%rbx,8)
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	movl	$.L.str.31, %esi
	xorl	%eax, %eax
	movq	%r12, %rdx
	callq	sprintf
	movl	4(%rsp), %eax
	incl	%eax
	movl	%eax, 4(%rsp)
	cmpl	cube+8(%rip), %eax
	jl	.LBB3_110
	jmp	.LBB3_1
.LBB3_16:                               #   in Loop: Header=BB3_1 Depth=1
	movq	stderr(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	.p2align	4, 0x90
.LBB3_17:                               # %.split.i560
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB3_19
# BB#18:                                # %.split.i560
                                        #   in Loop: Header=BB3_17 Depth=2
	cmpl	$-1, %eax
	jne	.LBB3_17
.LBB3_19:                               # %skip_line.exit562
                                        #   in Loop: Header=BB3_1 Depth=1
	incl	lineno(%rip)
	jmp	.LBB3_1
.LBB3_84:                               #   in Loop: Header=BB3_1 Depth=1
	movl	$1, kiss(%rip)
	jmp	.LBB3_1
.LBB3_26:                               #   in Loop: Header=BB3_1 Depth=1
	movq	stderr(%rip), %rcx
	movl	$.L.str.13, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	.p2align	4, 0x90
.LBB3_27:                               # %.split.i563
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB3_29
# BB#28:                                # %.split.i563
                                        #   in Loop: Header=BB3_27 Depth=2
	cmpl	$-1, %eax
	jne	.LBB3_27
.LBB3_29:                               # %skip_line.exit565
                                        #   in Loop: Header=BB3_1 Depth=1
	incl	lineno(%rip)
	jmp	.LBB3_1
.LBB3_86:                               #   in Loop: Header=BB3_1 Depth=1
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	get_word
	movq	pla_types(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB3_91
# BB#87:                                # %.lr.ph658.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	$pla_types+16, %ebx
	.p2align	4, 0x90
.LBB3_89:                               # %.lr.ph658
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rdi
	movq	%r12, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_90
# BB#88:                                #   in Loop: Header=BB3_89 Depth=2
	movq	(%rbx), %rdi
	addq	$16, %rbx
	testq	%rdi, %rdi
	jne	.LBB3_89
.LBB3_91:                               # %.thread
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.28, %edi
.LBB3_92:                               # %PLA_labels.exit
                                        #   in Loop: Header=BB3_1 Depth=1
	xorl	%eax, %eax
	callq	fatal
	jmp	.LBB3_1
.LBB3_111:                              # %.thread707
                                        #   in Loop: Header=BB3_1 Depth=1
	cmpb	$111, %bpl
	jne	.LBB3_130
# BB#112:                               # %.thread707
                                        #   in Loop: Header=BB3_1 Depth=1
	cmpb	$98, 81(%rsp)
	jne	.LBB3_130
# BB#113:                               # %.thread707
                                        #   in Loop: Header=BB3_1 Depth=1
	movb	82(%rsp), %al
	testb	%al, %al
	jne	.LBB3_130
# BB#114:                               #   in Loop: Header=BB3_1 Depth=1
	cmpq	$0, cube+88(%rip)
	jne	.LBB3_116
# BB#115:                               #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB3_116:                              #   in Loop: Header=BB3_1 Depth=1
	cmpq	$0, 56(%r14)
	je	.LBB3_117
.LBB3_127:                              # %PLA_labels.exit586
                                        #   in Loop: Header=BB3_1 Depth=1
	movslq	cube+4(%rip), %rax
	leaq	-1(%rax), %rcx
	movl	%ecx, 4(%rsp)
	movq	cube+16(%rip), %rcx
	movslq	-4(%rcx,%rax,4), %rbx
	movq	cube+24(%rip), %rcx
	cmpl	-4(%rcx,%rax,4), %ebx
	jg	.LBB3_1
# BB#128:                               # %.lr.ph654.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	decq	%rbx
	.p2align	4, 0x90
.LBB3_129:                              # %.lr.ph654
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	get_word
	movq	%r12, %rdi
	callq	util_strsav
	movq	56(%r14), %rcx
	movq	%rax, 8(%rcx,%rbx,8)
	movq	cube+24(%rip), %rax
	movslq	4(%rsp), %rcx
	movslq	(%rax,%rcx,4), %rax
	incq	%rbx
	cmpq	%rax, %rbx
	jl	.LBB3_129
	jmp	.LBB3_1
.LBB3_38:                               #   in Loop: Header=BB3_1 Depth=1
	movl	$2, %eax
	cmpq	$7, %rcx
	jb	.LBB3_1
	.p2align	4, 0x90
.LBB3_43:                               # %._crit_edge7.i.._crit_edge7.i_crit_edge
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r14), %rcx
	movq	$0, (%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 8(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 16(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 24(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 32(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 40(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 48(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 56(%rcx,%rax,8)
	addq	$8, %rax
	cmpq	%rbp, %rax
	jl	.LBB3_43
	jmp	.LBB3_1
.LBB3_48:                               #   in Loop: Header=BB3_1 Depth=1
	movq	stderr(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$18, %esi
	movl	$1, %edx
	callq	fwrite
	.p2align	4, 0x90
.LBB3_49:                               # %.split.i566
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB3_51
# BB#50:                                # %.split.i566
                                        #   in Loop: Header=BB3_49 Depth=2
	cmpl	$-1, %eax
	jne	.LBB3_49
.LBB3_51:                               # %skip_line.exit568
                                        #   in Loop: Header=BB3_1 Depth=1
	incl	lineno(%rip)
	jmp	.LBB3_1
.LBB3_130:                              # %.thread709
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.33, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_131
# BB#149:                               #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.37, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_150
# BB#156:                               #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.39, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_157
# BB#162:                               #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.41, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_163
# BB#180:                               #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.45, %esi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_181
# BB#220:                               #   in Loop: Header=BB3_1 Depth=1
	cmpl	$0, echo_unknown_commands(%rip)
	je	.LBB3_226
# BB#221:                               #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.48, %edi
	movl	$46, %esi
	xorl	%eax, %eax
	movq	%r12, %rdx
	callq	printf
	cmpl	$0, echo_unknown_commands(%rip)
	je	.LBB3_226
# BB#222:                               #   in Loop: Header=BB3_1 Depth=1
	movq	stdout(%rip), %rbp
	jmp	.LBB3_223
.LBB3_226:                              # %.split.i612
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB3_229
# BB#227:                               # %.split.i612
                                        #   in Loop: Header=BB3_226 Depth=2
	cmpl	$10, %eax
	jne	.LBB3_226
	jmp	.LBB3_229
.LBB3_225:                              # %.backedge.us.i611
                                        #   in Loop: Header=BB3_223 Depth=2
	movl	%eax, %edi
	movq	%rbp, %rsi
	callq	_IO_putc
.LBB3_223:                              # %.split.us.i610
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB3_228
# BB#224:                               # %.split.us.i610
                                        #   in Loop: Header=BB3_223 Depth=2
	cmpl	$10, %eax
	jne	.LBB3_225
.LBB3_228:                              #   in Loop: Header=BB3_1 Depth=1
	movl	$10, %edi
	movq	%rbp, %rsi
	callq	_IO_putc
.LBB3_229:                              # %skip_line.exit614
                                        #   in Loop: Header=BB3_1 Depth=1
	incl	lineno(%rip)
	jmp	.LBB3_1
.LBB3_69:                               #   in Loop: Header=BB3_1 Depth=1
	movl	$2, %eax
	cmpq	$7, %rcx
	jb	.LBB3_1
	.p2align	4, 0x90
.LBB3_74:                               # %._crit_edge7.i573.._crit_edge7.i573_crit_edge
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r14), %rcx
	movq	$0, (%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 8(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 16(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 24(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 32(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 40(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 48(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 56(%rcx,%rax,8)
	addq	$8, %rax
	cmpq	%rbp, %rax
	jl	.LBB3_74
	jmp	.LBB3_1
.LBB3_90:                               #   in Loop: Header=BB3_1 Depth=1
	movl	-8(%rbx), %eax
	movl	%eax, 32(%r14)
	jmp	.LBB3_1
.LBB3_100:                              #   in Loop: Header=BB3_1 Depth=1
	movslq	cube(%rip), %rbp
	leaq	(,%rbp,8), %rdi
	callq	malloc
	testq	%rbp, %rbp
	movq	%rax, 56(%r14)
	jle	.LBB3_109
# BB#101:                               # %.lr.ph.i575
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	$0, (%rax)
	cmpl	$1, %ebp
	je	.LBB3_109
# BB#102:                               # %._crit_edge7.i579.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	$0, 8(%rax)
	cmpl	$3, %ebp
	jl	.LBB3_109
# BB#103:                               # %._crit_edge7.i579.._crit_edge7.i579_crit_edge.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	leal	6(%rbp), %edx
	leaq	-3(%rbp), %rcx
	andq	$7, %rdx
	je	.LBB3_104
# BB#105:                               # %._crit_edge7.i579.._crit_edge7.i579_crit_edge.prol.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	xorl	%eax, %eax
.LBB3_106:                              # %._crit_edge7.i579.._crit_edge7.i579_crit_edge.prol
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r14), %rsi
	movq	$0, 16(%rsi,%rax,8)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB3_106
# BB#107:                               # %._crit_edge7.i579.._crit_edge7.i579_crit_edge.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB3_1 Depth=1
	addq	$2, %rax
	cmpq	$7, %rcx
	jae	.LBB3_245
	jmp	.LBB3_109
.LBB3_117:                              #   in Loop: Header=BB3_1 Depth=1
	movslq	cube(%rip), %rbp
	leaq	(,%rbp,8), %rdi
	callq	malloc
	testq	%rbp, %rbp
	movq	%rax, 56(%r14)
	jle	.LBB3_127
# BB#118:                               # %.lr.ph.i581
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	$0, (%rax)
	cmpl	$1, %ebp
	je	.LBB3_127
# BB#119:                               # %._crit_edge7.i585.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	$0, 8(%rax)
	cmpl	$3, %ebp
	jl	.LBB3_127
# BB#120:                               # %._crit_edge7.i585.._crit_edge7.i585_crit_edge.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	leal	6(%rbp), %edx
	leaq	-3(%rbp), %rcx
	andq	$7, %rdx
	je	.LBB3_121
# BB#122:                               # %._crit_edge7.i585.._crit_edge7.i585_crit_edge.prol.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	xorl	%eax, %eax
.LBB3_123:                              # %._crit_edge7.i585.._crit_edge7.i585_crit_edge.prol
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r14), %rsi
	movq	$0, 16(%rsi,%rax,8)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB3_123
# BB#124:                               # %._crit_edge7.i585.._crit_edge7.i585_crit_edge.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB3_1 Depth=1
	addq	$2, %rax
	cmpq	$7, %rcx
	jae	.LBB3_126
	jmp	.LBB3_127
.LBB3_131:                              #   in Loop: Header=BB3_1 Depth=1
	cmpq	$0, cube+88(%rip)
	jne	.LBB3_133
# BB#132:                               #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB3_133:                              #   in Loop: Header=BB3_1 Depth=1
	cmpq	$0, 56(%r14)
	je	.LBB3_134
.LBB3_144:                              # %PLA_labels.exit592
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	leaq	4(%rsp), %rdx
	callq	fscanf
	cmpl	$1, %eax
	je	.LBB3_146
# BB#145:                               #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB3_146:                              #   in Loop: Header=BB3_1 Depth=1
	movq	cube+16(%rip), %rax
	movslq	4(%rsp), %rcx
	movslq	(%rax,%rcx,4), %rbx
	movq	cube+24(%rip), %rax
	cmpl	(%rax,%rcx,4), %ebx
	jg	.LBB3_1
# BB#147:                               # %.lr.ph651.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	decq	%rbx
	.p2align	4, 0x90
.LBB3_148:                              # %.lr.ph651
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	get_word
	movq	%r12, %rdi
	callq	util_strsav
	movq	56(%r14), %rcx
	movq	%rax, 8(%rcx,%rbx,8)
	movq	cube+24(%rip), %rax
	movslq	4(%rsp), %rcx
	movslq	(%rax,%rcx,4), %rax
	incq	%rbx
	cmpq	%rax, %rbx
	jl	.LBB3_148
	jmp	.LBB3_1
.LBB3_150:                              #   in Loop: Header=BB3_1 Depth=1
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	leaq	16(%rsp), %rcx
	callq	read_symbolic
	testl	%eax, %eax
	je	.LBB3_155
# BB#151:                               #   in Loop: Header=BB3_1 Depth=1
	movq	64(%r14), %rax
	testq	%rax, %rax
	je	.LBB3_152
.LBB3_153:                              # %.preheader
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rcx
	movq	32(%rcx), %rax
	testq	%rax, %rax
	jne	.LBB3_153
	jmp	.LBB3_154
.LBB3_157:                              #   in Loop: Header=BB3_1 Depth=1
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	leaq	16(%rsp), %rcx
	callq	read_symbolic
	testl	%eax, %eax
	je	.LBB3_161
# BB#158:                               #   in Loop: Header=BB3_1 Depth=1
	movq	72(%r14), %rax
	testq	%rax, %rax
	je	.LBB3_159
.LBB3_160:                              # %.preheader632
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rcx
	movq	32(%rcx), %rax
	testq	%rax, %rax
	jne	.LBB3_160
.LBB3_154:                              #   in Loop: Header=BB3_1 Depth=1
	movq	16(%rsp), %rax
	movq	%rax, 32(%rcx)
	jmp	.LBB3_1
.LBB3_134:                              #   in Loop: Header=BB3_1 Depth=1
	movslq	cube(%rip), %rbp
	leaq	(,%rbp,8), %rdi
	callq	malloc
	testq	%rbp, %rbp
	movq	%rax, 56(%r14)
	jle	.LBB3_144
# BB#135:                               # %.lr.ph.i587
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	$0, (%rax)
	cmpl	$1, %ebp
	je	.LBB3_144
# BB#136:                               # %._crit_edge7.i591.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	$0, 8(%rax)
	cmpl	$3, %ebp
	jl	.LBB3_144
# BB#137:                               # %._crit_edge7.i591.._crit_edge7.i591_crit_edge.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	leal	6(%rbp), %edx
	leaq	-3(%rbp), %rcx
	andq	$7, %rdx
	je	.LBB3_138
# BB#139:                               # %._crit_edge7.i591.._crit_edge7.i591_crit_edge.prol.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	xorl	%eax, %eax
.LBB3_140:                              # %._crit_edge7.i591.._crit_edge7.i591_crit_edge.prol
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r14), %rsi
	movq	$0, 16(%rsi,%rax,8)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB3_140
# BB#141:                               # %._crit_edge7.i591.._crit_edge7.i591_crit_edge.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB3_1 Depth=1
	addq	$2, %rax
	cmpq	$7, %rcx
	jae	.LBB3_143
	jmp	.LBB3_144
.LBB3_163:                              #   in Loop: Header=BB3_1 Depth=1
	cmpq	$0, cube+88(%rip)
	jne	.LBB3_165
# BB#164:                               #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB3_165:                              #   in Loop: Header=BB3_1 Depth=1
	cmpq	$0, 40(%r14)
	jne	.LBB3_166
.LBB3_170:                              # %.preheader634
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$9, %eax
	je	.LBB3_170
# BB#171:                               # %.preheader634
                                        #   in Loop: Header=BB3_170 Depth=2
	cmpl	$32, %eax
	je	.LBB3_170
# BB#172:                               #   in Loop: Header=BB3_1 Depth=1
	movl	%eax, %edi
	movq	%r13, %rsi
	callq	ungetc
	movq	cube+88(%rip), %rbp
	movl	(%rbp), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	movl	$2, %ecx
	cmpl	$33, %eax
	jb	.LBB3_174
# BB#173:                               #   in Loop: Header=BB3_1 Depth=1
	decl	%eax
	shrl	$5, %eax
	addl	$2, %eax
	movl	%eax, %ecx
.LBB3_174:                              #   in Loop: Header=BB3_1 Depth=1
	movl	%ecx, %edi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbp, %rsi
	callq	set_copy
	movq	%rax, 40(%r14)
	movq	cube+24(%rip), %rax
	movslq	cube+4(%rip), %rcx
	movl	-4(%rax,%rcx,4), %r15d
	movq	cube+16(%rip), %rax
	movl	-4(%rax,%rcx,4), %ebp
	cmpl	%r15d, %ebp
	jg	.LBB3_1
.LBB3_175:                              # %.lr.ph648
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$49, %eax
	je	.LBB3_179
# BB#176:                               # %.lr.ph648
                                        #   in Loop: Header=BB3_175 Depth=2
	cmpl	$48, %eax
	jne	.LBB3_178
# BB#177:                               #   in Loop: Header=BB3_175 Depth=2
	movl	$-2, %eax
	movl	%ebp, %ecx
	roll	%cl, %eax
	movq	40(%r14), %rcx
	movl	%ebp, %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	andl	%eax, 4(%rcx,%rdx,4)
	jmp	.LBB3_179
.LBB3_178:                              #   in Loop: Header=BB3_175 Depth=2
	movl	$.L.str.44, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB3_179:                              #   in Loop: Header=BB3_175 Depth=2
	cmpl	%r15d, %ebp
	leal	1(%rbp), %eax
	movl	%eax, %ebp
	jl	.LBB3_175
	jmp	.LBB3_1
.LBB3_155:                              #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.38, %edi
	jmp	.LBB3_92
.LBB3_181:                              #   in Loop: Header=BB3_1 Depth=1
	cmpq	$0, 48(%r14)
	jne	.LBB3_182
# BB#183:                               #   in Loop: Header=BB3_1 Depth=1
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rcx
	movq	%rcx, 48(%r14)
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rcx, %rdx
	callq	fscanf
	cmpl	$1, %eax
	je	.LBB3_185
# BB#184:                               #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.47, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB3_185:                              #   in Loop: Header=BB3_1 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	movslq	(%rbx), %r15
	leaq	(,%r15,4), %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, 8(%rbx)
	movq	%rbp, %rdi
	callq	malloc
	testq	%r15, %r15
	movq	%rax, 16(%rbx)
	jle	.LBB3_1
# BB#186:                               # %.lr.ph.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	xorl	%edx, %edx
.LBB3_187:                              # %.lr.ph
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_193 Depth 3
                                        #         Child Loop BB3_195 Depth 4
                                        #       Child Loop BB3_209 Depth 3
                                        #         Child Loop BB3_211 Depth 4
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	get_word
	cmpb	$40, 80(%rsp)
	jne	.LBB3_189
# BB#188:                               #   in Loop: Header=BB3_187 Depth=2
	movq	%r12, %rdi
	leaq	81(%rsp), %rsi
	callq	strcpy
.LBB3_189:                              #   in Loop: Header=BB3_187 Depth=2
	movq	56(%r14), %rax
	testq	%rax, %rax
	je	.LBB3_198
# BB#190:                               #   in Loop: Header=BB3_187 Depth=2
	cmpq	$0, (%rax)
	je	.LBB3_198
# BB#191:                               # %.preheader29.i
                                        #   in Loop: Header=BB3_187 Depth=2
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movslq	cube+4(%rip), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jle	.LBB3_202
# BB#192:                               # %.preheader.lr.ph.i
                                        #   in Loop: Header=BB3_187 Depth=2
	movq	cube+32(%rip), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	cube+16(%rip), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
.LBB3_193:                              # %.preheader.i
                                        #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_187 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_195 Depth 4
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	56(%rsp), %rcx          # 8-byte Reload
	movslq	(%rcx,%rax,4), %rbx
	testq	%rbx, %rbx
	jle	.LBB3_201
# BB#194:                               # %.lr.ph.i596
                                        #   in Loop: Header=BB3_193 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movslq	(%rcx,%rax,4), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_195:                              #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_187 Depth=2
                                        #       Parent Loop BB3_193 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%r15,%rbp,8), %rdi
	movq	%r12, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_196
# BB#200:                               #   in Loop: Header=BB3_195 Depth=4
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB3_195
.LBB3_201:                              # %._crit_edge.i
                                        #   in Loop: Header=BB3_193 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	incq	%rax
	cmpq	48(%rsp), %rax          # 8-byte Folded Reload
	jl	.LBB3_193
	jmp	.LBB3_202
.LBB3_198:                              #   in Loop: Header=BB3_187 Depth=2
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	4(%rsp), %rdx
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB3_202
# BB#199:                               # %.label_index.exit_crit_edge
                                        #   in Loop: Header=BB3_187 Depth=2
	movl	4(%rsp), %edx
	jmp	.LBB3_197
.LBB3_202:                              # %.loopexit625
                                        #   in Loop: Header=BB3_187 Depth=2
	movl	$.L.str.47, %edi
	xorl	%eax, %eax
	callq	fatal
	jmp	.LBB3_203
.LBB3_196:                              #   in Loop: Header=BB3_187 Depth=2
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%edx, 4(%rsp)
.LBB3_197:                              # %label_index.exit
                                        #   in Loop: Header=BB3_187 Depth=2
	incl	%edx
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rax
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%edx, (%rax,%rcx,4)
.LBB3_203:                              #   in Loop: Header=BB3_187 Depth=2
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	get_word
	movq	%r12, %rdi
	callq	strlen
	cmpb	$41, 79(%rsp,%rax)
	jne	.LBB3_205
# BB#204:                               #   in Loop: Header=BB3_187 Depth=2
	movb	$0, 79(%rsp,%rax)
.LBB3_205:                              #   in Loop: Header=BB3_187 Depth=2
	movq	56(%r14), %rax
	testq	%rax, %rax
	je	.LBB3_214
# BB#206:                               #   in Loop: Header=BB3_187 Depth=2
	cmpq	$0, (%rax)
	je	.LBB3_214
# BB#207:                               # %.preheader29.i599
                                        #   in Loop: Header=BB3_187 Depth=2
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movslq	cube+4(%rip), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jle	.LBB3_218
# BB#208:                               # %.preheader.lr.ph.i600
                                        #   in Loop: Header=BB3_187 Depth=2
	movq	cube+32(%rip), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	cube+16(%rip), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
.LBB3_209:                              # %.preheader.i602
                                        #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_187 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_211 Depth 4
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	56(%rsp), %rcx          # 8-byte Reload
	movslq	(%rcx,%rax,4), %rbx
	testq	%rbx, %rbx
	jle	.LBB3_217
# BB#210:                               # %.lr.ph.i603
                                        #   in Loop: Header=BB3_209 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movslq	(%rcx,%rax,4), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_211:                              #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_187 Depth=2
                                        #       Parent Loop BB3_209 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%r15,%rbp,8), %rdi
	movq	%r12, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_212
# BB#216:                               #   in Loop: Header=BB3_211 Depth=4
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB3_211
.LBB3_217:                              # %._crit_edge.i607
                                        #   in Loop: Header=BB3_209 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	incq	%rax
	cmpq	48(%rsp), %rax          # 8-byte Folded Reload
	jl	.LBB3_209
	jmp	.LBB3_218
.LBB3_214:                              #   in Loop: Header=BB3_187 Depth=2
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	4(%rsp), %rdx
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB3_218
# BB#215:                               # %.label_index.exit609_crit_edge
                                        #   in Loop: Header=BB3_187 Depth=2
	movl	4(%rsp), %esi
	jmp	.LBB3_213
.LBB3_218:                              # %.loopexit
                                        #   in Loop: Header=BB3_187 Depth=2
	movl	$.L.str.47, %edi
	xorl	%eax, %eax
	callq	fatal
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB3_219
.LBB3_212:                              #   in Loop: Header=BB3_187 Depth=2
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%esi, 4(%rsp)
.LBB3_213:                              # %label_index.exit609
                                        #   in Loop: Header=BB3_187 Depth=2
	incl	%esi
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rax
	movq	64(%rsp), %rdx          # 8-byte Reload
	movl	%esi, (%rax,%rdx,4)
.LBB3_219:                              #   in Loop: Header=BB3_187 Depth=2
	incq	%rdx
	movslq	(%rcx), %rax
	cmpq	%rax, %rdx
	jl	.LBB3_187
	jmp	.LBB3_1
.LBB3_152:                              #   in Loop: Header=BB3_1 Depth=1
	movq	16(%rsp), %rax
	movq	%rax, 64(%r14)
	jmp	.LBB3_1
.LBB3_161:                              #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.40, %edi
	jmp	.LBB3_92
.LBB3_104:                              #   in Loop: Header=BB3_1 Depth=1
	movl	$2, %eax
	cmpq	$7, %rcx
	jb	.LBB3_109
.LBB3_245:                              # %._crit_edge7.i579.._crit_edge7.i579_crit_edge
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r14), %rcx
	movq	$0, (%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 8(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 16(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 24(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 32(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 40(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 48(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 56(%rcx,%rax,8)
	addq	$8, %rax
	cmpq	%rbp, %rax
	jl	.LBB3_245
	jmp	.LBB3_109
.LBB3_159:                              #   in Loop: Header=BB3_1 Depth=1
	movq	16(%rsp), %rax
	movq	%rax, 72(%r14)
	jmp	.LBB3_1
.LBB3_121:                              #   in Loop: Header=BB3_1 Depth=1
	movl	$2, %eax
	cmpq	$7, %rcx
	jb	.LBB3_127
.LBB3_126:                              # %._crit_edge7.i585.._crit_edge7.i585_crit_edge
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r14), %rcx
	movq	$0, (%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 8(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 16(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 24(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 32(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 40(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 48(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 56(%rcx,%rax,8)
	addq	$8, %rax
	cmpq	%rbp, %rax
	jl	.LBB3_126
	jmp	.LBB3_127
.LBB3_138:                              #   in Loop: Header=BB3_1 Depth=1
	movl	$2, %eax
	cmpq	$7, %rcx
	jb	.LBB3_144
.LBB3_143:                              # %._crit_edge7.i591.._crit_edge7.i591_crit_edge
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r14), %rcx
	movq	$0, (%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 8(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 16(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 24(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 32(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 40(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 48(%rcx,%rax,8)
	movq	56(%r14), %rcx
	movq	$0, 56(%rcx,%rax,8)
	addq	$8, %rax
	cmpq	%rbp, %rax
	jl	.LBB3_143
	jmp	.LBB3_144
.LBB3_166:                              #   in Loop: Header=BB3_1 Depth=1
	movq	stderr(%rip), %rcx
	movl	$.L.str.43, %edi
	movl	$21, %esi
	movl	$1, %edx
	callq	fwrite
.LBB3_167:                              # %.split.i593
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB3_169
# BB#168:                               # %.split.i593
                                        #   in Loop: Header=BB3_167 Depth=2
	cmpl	$-1, %eax
	jne	.LBB3_167
.LBB3_169:                              # %skip_line.exit595
                                        #   in Loop: Header=BB3_1 Depth=1
	incl	lineno(%rip)
	jmp	.LBB3_1
.LBB3_182:                              #   in Loop: Header=BB3_1 Depth=1
	movq	stderr(%rip), %rcx
	movl	$.L.str.46, %edi
	movl	$20, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB3_1
.LBB3_12:                               # %skip_line.exit
                                        #   in Loop: Header=BB3_1 Depth=1
	incl	lineno(%rip)
	.p2align	4, 0x90
.LBB3_1:                                # %PLA_labels.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_223 Depth 2
                                        #     Child Loop BB3_226 Depth 2
                                        #     Child Loop BB3_187 Depth 2
                                        #       Child Loop BB3_193 Depth 3
                                        #         Child Loop BB3_195 Depth 4
                                        #       Child Loop BB3_209 Depth 3
                                        #         Child Loop BB3_211 Depth 4
                                        #     Child Loop BB3_167 Depth 2
                                        #     Child Loop BB3_170 Depth 2
                                        #     Child Loop BB3_175 Depth 2
                                        #     Child Loop BB3_160 Depth 2
                                        #     Child Loop BB3_153 Depth 2
                                        #     Child Loop BB3_140 Depth 2
                                        #     Child Loop BB3_143 Depth 2
                                        #     Child Loop BB3_148 Depth 2
                                        #     Child Loop BB3_123 Depth 2
                                        #     Child Loop BB3_126 Depth 2
                                        #     Child Loop BB3_129 Depth 2
                                        #     Child Loop BB3_106 Depth 2
                                        #     Child Loop BB3_245 Depth 2
                                        #     Child Loop BB3_110 Depth 2
                                        #     Child Loop BB3_89 Depth 2
                                        #     Child Loop BB3_49 Depth 2
                                        #     Child Loop BB3_61 Depth 2
                                        #     Child Loop BB3_71 Depth 2
                                        #     Child Loop BB3_74 Depth 2
                                        #     Child Loop BB3_27 Depth 2
                                        #     Child Loop BB3_40 Depth 2
                                        #     Child Loop BB3_43 Depth 2
                                        #     Child Loop BB3_17 Depth 2
                                        #     Child Loop BB3_8 Depth 2
                                        #     Child Loop BB3_5 Depth 2
                                        #     Child Loop BB3_234 Depth 2
                                        #     Child Loop BB3_237 Depth 2
	movq	%r13, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	1(%rax), %ecx
	cmpl	$47, %ecx
	ja	.LBB3_230
# BB#2:                                 # %PLA_labels.exit
                                        #   in Loop: Header=BB3_1 Depth=1
	jmpq	*.LJTI3_0(,%rcx,8)
.LBB3_3:                                #   in Loop: Header=BB3_1 Depth=1
	incl	lineno(%rip)
	jmp	.LBB3_1
.LBB3_230:                              #   in Loop: Header=BB3_1 Depth=1
	movl	%eax, %edi
	movq	%r13, %rsi
	callq	ungetc
	cmpq	$0, cube+88(%rip)
	je	.LBB3_231
# BB#241:                               #   in Loop: Header=BB3_1 Depth=1
	cmpq	$0, (%r14)
	jne	.LBB3_243
# BB#242:                               #   in Loop: Header=BB3_1 Depth=1
	movl	cube(%rip), %esi
	movl	$10, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, (%r14)
	movl	cube(%rip), %esi
	movl	$10, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, 8(%r14)
	movl	cube(%rip), %esi
	movl	$10, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, 16(%r14)
.LBB3_243:                              #   in Loop: Header=BB3_1 Depth=1
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	read_cube
	jmp	.LBB3_1
.LBB3_4:                                #   in Loop: Header=BB3_1 Depth=1
	movl	$35, %edi
	movq	%r13, %rsi
	callq	ungetc
	movq	stdout(%rip), %rbp
	movl	echo_comments(%rip), %ebx
	testl	%ebx, %ebx
	jne	.LBB3_5
	.p2align	4, 0x90
.LBB3_8:                                # %.split.i
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB3_10
# BB#9:                                 # %.split.i
                                        #   in Loop: Header=BB3_8 Depth=2
	cmpl	$-1, %eax
	jne	.LBB3_8
	jmp	.LBB3_10
	.p2align	4, 0x90
.LBB3_7:                                # %.backedge.us.i
                                        #   in Loop: Header=BB3_5 Depth=2
	movl	%eax, %edi
	movq	%rbp, %rsi
	callq	_IO_putc
.LBB3_5:                                # %.split.us.i
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB3_10
# BB#6:                                 # %.split.us.i
                                        #   in Loop: Header=BB3_5 Depth=2
	cmpl	$10, %eax
	jne	.LBB3_7
.LBB3_10:                               # %.us-lcssa.us.i
                                        #   in Loop: Header=BB3_1 Depth=1
	testl	%ebx, %ebx
	je	.LBB3_12
# BB#11:                                #   in Loop: Header=BB3_1 Depth=1
	movl	$10, %edi
	movq	%rbp, %rsi
	callq	_IO_putc
	jmp	.LBB3_12
.LBB3_13:                               #   in Loop: Header=BB3_1 Depth=1
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	get_word
	movb	(%rax), %bpl
	cmpb	$105, %bpl
	jne	.LBB3_23
# BB#14:                                #   in Loop: Header=BB3_1 Depth=1
	movb	81(%rsp), %al
	testb	%al, %al
	jne	.LBB3_23
# BB#15:                                #   in Loop: Header=BB3_1 Depth=1
	cmpq	$0, cube+88(%rip)
	jne	.LBB3_16
# BB#20:                                #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.10, %esi
	movl	$cube+8, %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fscanf
	cmpl	$1, %eax
	je	.LBB3_22
# BB#21:                                #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB3_22:                               #   in Loop: Header=BB3_1 Depth=1
	movslq	cube+8(%rip), %rax
	leaq	1(%rax), %rcx
	movl	%ecx, cube+4(%rip)
	leaq	4(,%rax,4), %rdi
	callq	malloc
	movq	%rax, cube+32(%rip)
	jmp	.LBB3_1
.LBB3_23:                               # %.thread698
                                        #   in Loop: Header=BB3_1 Depth=1
	cmpb	$111, %bpl
	jne	.LBB3_44
# BB#24:                                # %.thread698
                                        #   in Loop: Header=BB3_1 Depth=1
	movb	81(%rsp), %al
	testb	%al, %al
	jne	.LBB3_44
# BB#25:                                #   in Loop: Header=BB3_1 Depth=1
	cmpq	$0, cube+88(%rip)
	jne	.LBB3_26
# BB#30:                                #   in Loop: Header=BB3_1 Depth=1
	movq	cube+32(%rip), %rax
	testq	%rax, %rax
	jne	.LBB3_32
# BB#31:                                #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	fatal
	movq	cube+32(%rip), %rax
.LBB3_32:                               #   in Loop: Header=BB3_1 Depth=1
	movslq	cube+4(%rip), %rcx
	leaq	-4(%rax,%rcx,4), %rdx
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fscanf
	cmpl	$1, %eax
	je	.LBB3_34
# BB#33:                                #   in Loop: Header=BB3_1 Depth=1
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB3_34:                               #   in Loop: Header=BB3_1 Depth=1
	xorl	%eax, %eax
	callq	cube_setup
	movslq	cube(%rip), %rbp
	leaq	(,%rbp,8), %rdi
	callq	malloc
	testq	%rbp, %rbp
	movq	%rax, 56(%r14)
	jle	.LBB3_1
# BB#35:                                # %.lr.ph.i
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	$0, (%rax)
	cmpl	$1, %ebp
	je	.LBB3_1
# BB#36:                                # %._crit_edge7.i.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	$0, 8(%rax)
	cmpl	$3, %ebp
	jl	.LBB3_1
# BB#37:                                # %._crit_edge7.i.._crit_edge7.i_crit_edge.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	leal	6(%rbp), %edx
	leaq	-3(%rbp), %rcx
	andq	$7, %rdx
	je	.LBB3_38
# BB#39:                                # %._crit_edge7.i.._crit_edge7.i_crit_edge.prol.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_40:                               # %._crit_edge7.i.._crit_edge7.i_crit_edge.prol
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r14), %rsi
	movq	$0, 16(%rsi,%rax,8)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB3_40
# BB#41:                                # %._crit_edge7.i.._crit_edge7.i_crit_edge.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB3_1 Depth=1
	addq	$2, %rax
	cmpq	$7, %rcx
	jb	.LBB3_1
	jmp	.LBB3_43
.LBB3_231:                              #   in Loop: Header=BB3_1 Depth=1
	cmpl	$0, echo_comments(%rip)
	je	.LBB3_237
# BB#232:                               #   in Loop: Header=BB3_1 Depth=1
	movq	stdout(%rip), %rsi
	movl	$35, %edi
	callq	_IO_putc
	cmpl	$0, echo_comments(%rip)
	je	.LBB3_237
# BB#233:                               # %.split.us.i615.preheader
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	stdout(%rip), %rbp
	jmp	.LBB3_234
	.p2align	4, 0x90
.LBB3_237:                              # %.split.i617
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB3_240
# BB#238:                               # %.split.i617
                                        #   in Loop: Header=BB3_237 Depth=2
	cmpl	$10, %eax
	jne	.LBB3_237
	jmp	.LBB3_240
.LBB3_244:
	addq	$344, %rsp              # imm = 0x158
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	parse_pla, .Lfunc_end3-parse_pla
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_244
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_1
	.quad	.LBB3_3
	.quad	.LBB3_230
	.quad	.LBB3_1
	.quad	.LBB3_1
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_1
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_4
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_230
	.quad	.LBB3_13

	.text
	.globl	read_pla
	.p2align	4, 0x90
	.type	read_pla,@function
read_pla:                               # @read_pla
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi48:
	.cfi_def_cfa_offset 96
.Lcfi49:
	.cfi_offset %rbx, -56
.Lcfi50:
	.cfi_offset %r12, -48
.Lcfi51:
	.cfi_offset %r13, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%r8, %rbp
	movl	%ecx, %r15d
	movl	%edx, %r14d
	movl	%esi, %ebx
	movq	%rdi, %r13
	movl	$80, %edi
	callq	malloc
	movq	%rax, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r12)
	movups	%xmm0, (%r12)
	movups	%xmm0, 56(%r12)
	movups	%xmm0, 40(%r12)
	movq	$0, 72(%r12)
	movq	%r12, (%rbp)
	movl	%r15d, 32(%r12)
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r15
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	parse_pla
	movl	$-1, %r13d
	cmpq	$0, (%r12)
	je	.LBB4_46
# BB#1:                                 # %.preheader99
	movl	cube+4(%rip), %eax
	testl	%eax, %eax
	jle	.LBB4_4
# BB#2:                                 # %.lr.ph107
	movq	cube+32(%rip), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_3:                                # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rdx,4), %eax
	movl	%eax, %esi
	negl	%esi
	cmovll	%eax, %esi
	movl	%esi, (%rcx,%rdx,4)
	incq	%rdx
	movslq	cube+4(%rip), %rax
	cmpq	%rax, %rdx
	jl	.LBB4_3
.LBB4_4:                                # %._crit_edge108
	cmpl	$0, kiss(%rip)
	je	.LBB4_13
# BB#5:
	movslq	%eax, %rbp
	movq	cube+32(%rip), %rcx
	movl	-12(%rcx,%rbp,4), %edx
	cmpl	-8(%rcx,%rbp,4), %edx
	jne	.LBB4_8
# BB#6:                                 # %.preheader
	testl	%edx, %edx
	jle	.LBB4_7
# BB#9:                                 # %.lr.ph
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	movl	%ebx, %r14d
	leaq	-3(%rbp), %r13
	addq	$-2, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_10:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rax
	movq	cube+16(%rip), %rcx
	movl	(%rcx,%r13,4), %ecx
	addl	%ebx, %ecx
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rdi
	callq	util_strsav
	movq	56(%r12), %rcx
	movq	cube+16(%rip), %rdx
	movl	(%rdx,%rbp,4), %edx
	addl	%ebx, %edx
	movslq	%edx, %rdx
	movq	%rax, (%rcx,%rdx,8)
	incl	%ebx
	movq	cube+32(%rip), %rcx
	movl	(%rcx,%rbp,4), %edx
	cmpl	%edx, %ebx
	jl	.LBB4_10
# BB#11:                                # %._crit_edge.loopexit
	leaq	(%rcx,%rbp,4), %rsi
	movl	cube+4(%rip), %eax
	movl	%r14d, %ebx
	movl	12(%rsp), %r14d         # 4-byte Reload
	jmp	.LBB4_12
.LBB4_7:
	leaq	-8(%rcx,%rbp,4), %rsi
.LBB4_12:                               # %._crit_edge
	cltq
	addl	-4(%rcx,%rax,4), %edx
	movl	%edx, (%rsi)
	decl	cube+4(%rip)
	xorl	%eax, %eax
	callq	setdown_cube
	xorl	%eax, %eax
	callq	cube_setup
.LBB4_13:
	cmpl	$0, trace(%rip)
	je	.LBB4_15
# BB#14:
	movq	(%r12), %rdx
	leaq	16(%rsp), %rcx
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	totals
.LBB4_15:
	movq	%r12, %r13
	addq	$40, %r13
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r15
	cmpl	$0, pos(%rip)
	jne	.LBB4_20
# BB#16:
	cmpq	$0, (%r13)
	je	.LBB4_17
.LBB4_20:                               # %.thread
	movl	32(%r12), %eax
	movl	%eax, %ecx
	orl	$2, %ecx
	cmpl	$3, %ecx
	jne	.LBB4_21
# BB#47:
	movq	16(%r12), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	(%r12), %rdi
	movq	8(%r12), %rsi
	xorl	%eax, %eax
	callq	cube2list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	complement
	movq	%rax, 16(%r12)
	movb	$1, %bl
	cmpl	$0, trace(%rip)
	jne	.LBB4_28
	jmp	.LBB4_29
.LBB4_21:
	movl	%ebx, %ecx
	movb	$1, %bl
	testl	%ecx, %ecx
	je	.LBB4_25
.LBB4_23:
	cmpl	$5, %eax
	jne	.LBB4_25
# BB#24:
	movq	8(%r12), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	(%r12), %rdi
	movq	16(%r12), %rsi
	xorl	%eax, %eax
	callq	sf_join
	movq	%rax, %rcx
	movl	cube+4(%rip), %esi
	decl	%esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	d1merge
	movq	%rax, %rbp
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	cube1list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	complement
	movq	%rax, 8(%r12)
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sf_free
	cmpl	$0, trace(%rip)
	jne	.LBB4_28
	jmp	.LBB4_29
.LBB4_17:
	testl	%r14d, %r14d
	jne	.LBB4_20
# BB#18:
	movq	72(%r12), %rax
	testq	%rax, %rax
	jne	.LBB4_20
# BB#19:                                # %._crit_edge114
	movl	%ebx, %ecx
	movl	32(%r12), %eax
	xorl	%ebx, %ebx
	testl	%ecx, %ecx
	jne	.LBB4_23
.LBB4_25:                               # %thread-pre-split
	orl	$2, %eax
	cmpl	$6, %eax
	jne	.LBB4_27
# BB#26:
	movq	(%r12), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	8(%r12), %rdi
	movq	16(%r12), %rsi
	xorl	%eax, %eax
	callq	cube2list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	complement
	movq	%rax, (%r12)
.LBB4_27:
	cmpl	$0, trace(%rip)
	je	.LBB4_29
.LBB4_28:
	movq	16(%r12), %rdx
	leaq	16(%rsp), %rcx
	movl	$1, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	totals
.LBB4_29:
	cmpl	$0, pos(%rip)
	je	.LBB4_34
# BB#30:
	movq	(%r12), %rax
	movq	16(%r12), %rcx
	movq	%rcx, (%r12)
	movq	%rax, 16(%r12)
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jge	.LBB4_32
# BB#31:
	movl	$8, %edi
	jmp	.LBB4_33
.LBB4_34:
	cmpq	$0, (%r13)
	je	.LBB4_36
# BB#35:
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	set_phase
	cmpq	$0, 48(%r12)
	jne	.LBB4_37
	jmp	.LBB4_38
.LBB4_32:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB4_33:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %rcx
	movq	%rcx, (%r13)
	movq	cube+88(%rip), %rsi
	movq	cube+72(%rip), %rax
	movslq	cube+4(%rip), %rdx
	movq	-8(%rax,%rdx,8), %rdx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	set_diff
.LBB4_36:
	cmpq	$0, 48(%r12)
	je	.LBB4_38
.LBB4_37:
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	set_pair
.LBB4_38:
	cmpq	$0, 64(%r12)
	je	.LBB4_41
# BB#39:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbp
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	map_symbolic
	cmpl	$0, trace(%rip)
	je	.LBB4_41
# BB#40:
	movq	(%r12), %r14
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%rbp, %rcx
	movl	$.L.str.51, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB4_41:
	movl	$1, %r13d
	cmpq	$0, 72(%r12)
	je	.LBB4_46
# BB#42:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r14
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	map_output_symbolic
	cmpl	$0, trace(%rip)
	je	.LBB4_44
# BB#43:
	movq	(%r12), %r15
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r14, %rcx
	movl	$.L.str.52, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB4_44:
	testb	%bl, %bl
	je	.LBB4_46
# BB#45:
	movq	16(%r12), %rdi
	xorl	%eax, %eax
	callq	sf_free
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r14
	movq	(%r12), %rdi
	movq	8(%r12), %rsi
	xorl	%eax, %eax
	callq	cube2list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	complement
	movq	%rax, %rdx
	movq	%rdx, 16(%r12)
	movl	$1, %r13d
	leaq	16(%rsp), %rcx
	movl	$1, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	totals
.LBB4_46:
	movl	%r13d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_8:
	movq	stderr(%rip), %rcx
	movl	$.L.str.49, %edi
	movl	$45, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.50, %edi
	movl	$41, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB4_46
.Lfunc_end4:
	.size	read_pla, .Lfunc_end4-read_pla
	.cfi_endproc

	.globl	PLA_summary
	.p2align	4, 0x90
	.type	PLA_summary,@function
PLA_summary:                            # @PLA_summary
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 32
.Lcfi58:
	.cfi_offset %rbx, -32
.Lcfi59:
	.cfi_offset %r14, -24
.Lcfi60:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	24(%r14), %rbx
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rsi
	cmoveq	%rbx, %rsi
	movl	$.L.str.53, %edi
	xorl	%eax, %eax
	callq	printf
	movslq	cube+8(%rip), %rdx
	movl	cube+4(%rip), %esi
	leal	-1(%rsi), %eax
	cmpl	%eax, %edx
	jne	.LBB5_2
# BB#1:
	movq	cube+32(%rip), %rax
	movl	(%rax,%rdx,4), %ecx
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	movl	%edx, %esi
	movl	%ecx, %edx
	callq	printf
	jmp	.LBB5_5
.LBB5_2:
	movl	$.L.str.55, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	printf
	movslq	cube+8(%rip), %rbx
	cmpl	cube+4(%rip), %ebx
	jge	.LBB5_4
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph64
                                        # =>This Inner Loop Header: Depth=1
	movq	cube+32(%rip), %rax
	movl	(%rax,%rbx,4), %esi
	movl	$.L.str.56, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	movslq	cube+4(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB5_3
.LBB5_4:                                # %._crit_edge65
	movl	$.Lstr, %edi
	callq	puts
.LBB5_5:
	movq	(%r14), %rdi
	xorl	%eax, %eax
	callq	print_cost
	movq	%rax, %rcx
	movl	$.L.str.58, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	movq	16(%r14), %rdi
	xorl	%eax, %eax
	callq	print_cost
	movq	%rax, %rcx
	movl	$.L.str.59, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	movq	8(%r14), %rdi
	xorl	%eax, %eax
	callq	print_cost
	movq	%rax, %rcx
	movl	$.L.str.60, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	movq	40(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB5_7
# BB#6:
	xorl	%eax, %eax
	callq	pc1
	movq	%rax, %rcx
	movl	$.L.str.61, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
.LBB5_7:
	cmpq	$0, 48(%r14)
	je	.LBB5_12
# BB#8:
	movl	$.L.str.62, %edi
	xorl	%eax, %eax
	callq	printf
	movq	48(%r14), %rax
	cmpl	$0, (%rax)
	jle	.LBB5_11
# BB#9:                                 # %.lr.ph60.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_10:                               # %.lr.ph60
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movq	16(%rax), %rax
	movl	(%rcx,%rbx,4), %esi
	movl	(%rax,%rbx,4), %edx
	movl	$.L.str.63, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	movq	48(%r14), %rax
	movslq	(%rax), %rcx
	cmpq	%rcx, %rbx
	jl	.LBB5_10
.LBB5_11:                               # %._crit_edge61
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
.LBB5_12:
	movq	64(%r14), %r15
	testq	%r15, %r15
	jne	.LBB5_14
	jmp	.LBB5_18
	.p2align	4, 0x90
.LBB5_17:                               # %._crit_edge55
                                        #   in Loop: Header=BB5_14 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	32(%r15), %r15
	testq	%r15, %r15
	je	.LBB5_18
.LBB5_14:                               # %.lr.ph57
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_15 Depth 2
	movl	$.L.str.65, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%r15), %rbx
	testq	%rbx, %rbx
	je	.LBB5_17
	.p2align	4, 0x90
.LBB5_15:                               # %.lr.ph54
                                        #   Parent Loop BB5_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %esi
	movl	$.L.str.56, %edi
	xorl	%eax, %eax
	callq	printf
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB5_15
	jmp	.LBB5_17
.LBB5_18:                               # %.loopexit46
	movq	72(%r14), %r14
	testq	%r14, %r14
	jne	.LBB5_20
	jmp	.LBB5_24
	.p2align	4, 0x90
.LBB5_23:                               # %._crit_edge
                                        #   in Loop: Header=BB5_20 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	32(%r14), %r14
	testq	%r14, %r14
	je	.LBB5_24
.LBB5_20:                               # %.lr.ph50
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_21 Depth 2
	movl	$.L.str.66, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB5_23
	.p2align	4, 0x90
.LBB5_21:                               # %.lr.ph
                                        #   Parent Loop BB5_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rbx), %esi
	movl	$.L.str.56, %edi
	xorl	%eax, %eax
	callq	printf
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB5_21
	jmp	.LBB5_23
.LBB5_24:                               # %.loopexit
	movq	stdout(%rip), %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	fflush                  # TAILCALL
.Lfunc_end5:
	.size	PLA_summary, .Lfunc_end5-PLA_summary
	.cfi_endproc

	.globl	new_PLA
	.p2align	4, 0x90
	.type	new_PLA,@function
new_PLA:                                # @new_PLA
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi61:
	.cfi_def_cfa_offset 16
	movl	$80, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movl	$0, 32(%rax)
	movups	%xmm0, 56(%rax)
	movups	%xmm0, 40(%rax)
	movq	$0, 72(%rax)
	popq	%rcx
	retq
.Lfunc_end6:
	.size	new_PLA, .Lfunc_end6-new_PLA
	.cfi_endproc

	.globl	PLA_labels
	.p2align	4, 0x90
	.type	PLA_labels,@function
PLA_labels:                             # @PLA_labels
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 32
.Lcfi65:
	.cfi_offset %rbx, -24
.Lcfi66:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movslq	cube(%rip), %rbx
	leaq	(,%rbx,8), %rdi
	callq	malloc
	testq	%rbx, %rbx
	movq	%rax, 56(%r14)
	jle	.LBB7_4
# BB#1:                                 # %.lr.ph
	movq	$0, (%rax)
	cmpl	$1, %ebx
	je	.LBB7_4
# BB#2:                                 # %._crit_edge7.preheader
	movl	$1, %eax
	.p2align	4, 0x90
.LBB7_3:                                # %._crit_edge7
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rcx
	movq	$0, (%rcx,%rax,8)
	incq	%rax
	cmpq	%rbx, %rax
	jl	.LBB7_3
.LBB7_4:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	PLA_labels, .Lfunc_end7-PLA_labels
	.cfi_endproc

	.globl	free_PLA
	.p2align	4, 0x90
	.type	free_PLA,@function
free_PLA:                               # @free_PLA
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 32
.Lcfi70:
	.cfi_offset %rbx, -32
.Lcfi71:
	.cfi_offset %r14, -24
.Lcfi72:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB8_2
# BB#1:
	xorl	%eax, %eax
	callq	sf_free
.LBB8_2:
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB8_4
# BB#3:
	xorl	%eax, %eax
	callq	sf_free
.LBB8_4:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB8_6
# BB#5:
	xorl	%eax, %eax
	callq	sf_free
.LBB8_6:
	movq	40(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB8_8
# BB#7:
	callq	free
	movq	$0, 40(%r14)
.LBB8_8:
	movq	48(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB8_15
# BB#9:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.LBB8_11
# BB#10:
	movq	%rax, %rdi
	callq	free
	movq	48(%r14), %rdi
	movq	$0, 8(%rdi)
.LBB8_11:
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB8_13
# BB#12:
	movq	%rax, %rdi
	callq	free
	movq	48(%r14), %rdi
	movq	$0, 16(%rdi)
.LBB8_13:
	testq	%rdi, %rdi
	je	.LBB8_15
# BB#14:
	callq	free
	movq	$0, 48(%r14)
.LBB8_15:
	movq	56(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB8_23
# BB#16:                                # %.preheader
	movl	cube(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB8_22
# BB#17:                                # %.lr.ph98.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_18:                               # %.lr.ph98
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB8_20
# BB#19:                                #   in Loop: Header=BB8_18 Depth=1
	movq	%rax, %rdi
	callq	free
	movq	56(%r14), %rax
	movq	$0, (%rax,%rbx,8)
	movq	56(%r14), %rdi
	movl	cube(%rip), %ecx
.LBB8_20:                               #   in Loop: Header=BB8_18 Depth=1
	incq	%rbx
	movslq	%ecx, %rax
	cmpq	%rax, %rbx
	jl	.LBB8_18
# BB#21:                                # %._crit_edge99
	testq	%rdi, %rdi
	je	.LBB8_23
.LBB8_22:                               # %._crit_edge99.thread
	callq	free
	movq	$0, 56(%r14)
.LBB8_23:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB8_25
# BB#24:
	callq	free
	movq	$0, 24(%r14)
.LBB8_25:
	movq	64(%r14), %r15
	testq	%r15, %r15
	je	.LBB8_29
	.p2align	4, 0x90
.LBB8_26:                               # %.lr.ph95
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_27 Depth 2
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB8_28
	.p2align	4, 0x90
.LBB8_27:                               # %.lr.ph91
                                        #   Parent Loop BB8_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB8_27
.LBB8_28:                               # %._crit_edge92
                                        #   in Loop: Header=BB8_26 Depth=1
	movq	32(%r15), %rbx
	movq	%r15, %rdi
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %r15
	jne	.LBB8_26
.LBB8_29:                               # %._crit_edge96
	movq	$0, 64(%r14)
	movq	72(%r14), %r15
	testq	%r15, %r15
	je	.LBB8_35
	.p2align	4, 0x90
.LBB8_30:                               # %.lr.ph87
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_31 Depth 2
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB8_32
	.p2align	4, 0x90
.LBB8_31:                               # %.lr.ph
                                        #   Parent Loop BB8_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB8_31
.LBB8_32:                               # %._crit_edge
                                        #   in Loop: Header=BB8_30 Depth=1
	movq	32(%r15), %rbx
	movq	%r15, %rdi
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %r15
	jne	.LBB8_30
# BB#33:                                # %._crit_edge88
	movq	$0, 72(%r14)
	testq	%r14, %r14
	je	.LBB8_34
.LBB8_35:                               # %._crit_edge88.thread
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB8_34:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	free_PLA, .Lfunc_end8-free_PLA
	.cfi_endproc

	.globl	read_symbolic
	.p2align	4, 0x90
	.type	read_symbolic,@function
read_symbolic:                          # @read_symbolic
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi76:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi77:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi79:
	.cfi_def_cfa_offset 144
.Lcfi80:
	.cfi_offset %rbx, -56
.Lcfi81:
	.cfi_offset %r12, -48
.Lcfi82:
	.cfi_offset %r13, -40
.Lcfi83:
	.cfi_offset %r14, -32
.Lcfi84:
	.cfi_offset %r15, -24
.Lcfi85:
	.cfi_offset %rbp, -16
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rdx, %rbx
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rdi, %r15
	movl	$40, %edi
	callq	malloc
	movq	%rax, %r12
	movq	$0, 32(%r12)
	movq	$0, (%r12)
	movl	$0, 8(%r12)
	leaq	16(%r12), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	$0, 16(%r12)
	movl	$0, 24(%r12)
	xorl	%r14d, %r14d
	xorl	%ebp, %ebp
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movq	%r12, 8(%rsp)           # 8-byte Spill
	jmp	.LBB9_1
	.p2align	4, 0x90
.LBB9_16:                               #   in Loop: Header=BB9_1 Depth=1
	xorl	%ebp, %ebp
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	leaq	4(%rsp), %rdx
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB9_22
# BB#17:                                #   in Loop: Header=BB9_1 Depth=1
	movl	4(%rsp), %r13d
	movq	8(%rsp), %r12           # 8-byte Reload
	movl	8(%r12), %r14d
	movl	%r13d, %r15d
	movq	16(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB9_15
	.p2align	4, 0x90
.LBB9_14:                               #   in Loop: Header=BB9_1 Depth=1
	movl	%r13d, 4(%rsp)
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB9_15:                               # %label_index.exit
                                        #   in Loop: Header=BB9_1 Depth=1
	movl	$16, %edi
	callq	malloc
	testq	%rbp, %rbp
	leaq	8(%rbp), %rcx
	movl	%r13d, (%rax)
	movl	%r15d, 4(%rax)
	movq	$0, 8(%rax)
	cmoveq	%r12, %rcx
	movq	%rax, (%rcx)
	incl	%r14d
	movl	%r14d, 8(%r12)
	movq	%rax, %rbp
	movq	40(%rsp), %r15          # 8-byte Reload
.LBB9_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_11 Depth 2
                                        #       Child Loop BB9_13 Depth 3
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	get_word
	cmpb	$59, (%rbx)
	jne	.LBB9_7
# BB#2:                                 #   in Loop: Header=BB9_1 Depth=1
	cmpb	$0, 1(%rbx)
	je	.LBB9_3
.LBB9_7:                                # %.thread
                                        #   in Loop: Header=BB9_1 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %r12
	testq	%r12, %r12
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	je	.LBB9_16
# BB#8:                                 #   in Loop: Header=BB9_1 Depth=1
	cmpq	$0, (%r12)
	je	.LBB9_16
# BB#9:                                 # %.preheader29.i
                                        #   in Loop: Header=BB9_1 Depth=1
	movslq	cube+4(%rip), %rcx
	xorl	%ebp, %ebp
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	testq	%rcx, %rcx
	jle	.LBB9_22
# BB#10:                                # %.preheader.lr.ph.i
                                        #   in Loop: Header=BB9_1 Depth=1
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	cube+32(%rip), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	cube+16(%rip), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
.LBB9_11:                               # %.preheader.i
                                        #   Parent Loop BB9_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_13 Depth 3
	movq	80(%rsp), %rax          # 8-byte Reload
	movslq	(%rax,%r13,4), %rbp
	testq	%rbp, %rbp
	jle	.LBB9_19
# BB#12:                                # %.lr.ph.i
                                        #   in Loop: Header=BB9_11 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	movslq	(%rax,%r13,4), %rax
	leaq	(%r12,%rax,8), %r14
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB9_13:                               #   Parent Loop BB9_1 Depth=1
                                        #     Parent Loop BB9_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r14,%r15,8), %rdi
	movq	%rbx, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB9_14
# BB#18:                                #   in Loop: Header=BB9_13 Depth=3
	incq	%r15
	cmpq	%rbp, %r15
	jl	.LBB9_13
.LBB9_19:                               # %._crit_edge.i
                                        #   in Loop: Header=BB9_11 Depth=2
	incq	%r13
	cmpq	72(%rsp), %r13          # 8-byte Folded Reload
	jl	.LBB9_11
# BB#20:
	xorl	%ebp, %ebp
	jmp	.LBB9_22
.LBB9_3:                                # %.preheader.preheader
	xorl	%r14d, %r14d
	jmp	.LBB9_4
	.p2align	4, 0x90
.LBB9_6:                                # %.thread112
                                        #   in Loop: Header=BB9_4 Depth=1
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	util_strsav
	movq	%rax, (%rbp)
	movq	$0, 8(%rbp)
	testq	%r14, %r14
	leaq	8(%r14), %rax
	cmoveq	56(%rsp), %rax          # 8-byte Folded Reload
	movq	%rbp, (%rax)
	incl	24(%r12)
	movq	%rbp, %r14
.LBB9_4:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	get_word
	cmpb	$59, (%rbx)
	jne	.LBB9_6
# BB#5:                                 #   in Loop: Header=BB9_4 Depth=1
	cmpb	$0, 1(%rbx)
	jne	.LBB9_6
# BB#21:
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%r12, (%rax)
	movl	$1, %ebp
.LBB9_22:                               # %label_index.exit.thread
	movl	%ebp, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	read_symbolic, .Lfunc_end9-read_symbolic
	.cfi_endproc

	.globl	label_index
	.p2align	4, 0x90
	.type	label_index,@function
label_index:                            # @label_index
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi89:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi90:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi92:
	.cfi_def_cfa_offset 96
.Lcfi93:
	.cfi_offset %rbx, -56
.Lcfi94:
	.cfi_offset %r12, -48
.Lcfi95:
	.cfi_offset %r13, -40
.Lcfi96:
	.cfi_offset %r14, -32
.Lcfi97:
	.cfi_offset %r15, -24
.Lcfi98:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	56(%rdi), %r12
	testq	%r12, %r12
	je	.LBB10_4
# BB#1:
	cmpq	$0, (%r12)
	je	.LBB10_4
# BB#2:                                 # %.preheader29
	movslq	cube+4(%rip), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jle	.LBB10_3
# BB#6:                                 # %.preheader.lr.ph
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	cube+32(%rip), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	cube+16(%rip), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
.LBB10_7:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_9 Depth 2
	movq	32(%rsp), %rax          # 8-byte Reload
	movslq	(%rax,%r14,4), %rbp
	testq	%rbp, %rbp
	jle	.LBB10_12
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB10_7 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	(%rax,%r14,4), %rax
	leaq	(%r12,%rax,8), %r15
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB10_9:                               #   Parent Loop BB10_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15,%r13,8), %rdi
	movq	%rbx, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB10_10
# BB#11:                                #   in Loop: Header=BB10_9 Depth=2
	incq	%r13
	cmpq	%rbp, %r13
	jl	.LBB10_9
.LBB10_12:                              # %._crit_edge
                                        #   in Loop: Header=BB10_7 Depth=1
	incq	%r14
	xorl	%ebp, %ebp
	cmpq	24(%rsp), %r14          # 8-byte Folded Reload
	jl	.LBB10_7
	jmp	.LBB10_13
.LBB10_4:
	movq	%rcx, %r14
	xorl	%ebp, %ebp
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rdx, %rbx
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB10_13
# BB#5:
	movl	(%rbx), %eax
	movl	%eax, (%r14)
	movl	$1, %ebp
	jmp	.LBB10_13
.LBB10_10:
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%r14d, (%rax)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%r13d, (%rax)
	movl	$1, %ebp
	jmp	.LBB10_13
.LBB10_3:
	xorl	%ebp, %ebp
.LBB10_13:                              # %.loopexit
	movl	%ebp, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	label_index, .Lfunc_end10-label_index
	.cfi_endproc

	.type	lineno,@object          # @lineno
	.local	lineno
	.comm	lineno,4,4
	.type	line_length_error,@object # @line_length_error
	.local	line_length_error
	.comm	line_length_error,1,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"product term(s) %s\n"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"span more than one line (warning only)"
	.size	.L.str.1, 39

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%s"
	.size	.L.str.2, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"declared size of variable %d (counting from variable 0) is too small\n"
	.size	.L.str.6, 70

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"(warning): input line #%d ignored\n"
	.size	.L.str.7, 35

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"extra .i ignored\n"
	.size	.L.str.9, 18

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%d"
	.size	.L.str.10, 3

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"error reading .i"
	.size	.L.str.11, 17

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"extra .o ignored\n"
	.size	.L.str.13, 18

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	".o cannot appear before .i"
	.size	.L.str.14, 27

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"error reading .o"
	.size	.L.str.15, 17

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"extra .mv ignored\n"
	.size	.L.str.17, 19

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"cannot mix .i and .mv"
	.size	.L.str.18, 22

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"%d %d"
	.size	.L.str.19, 6

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"error reading .mv"
	.size	.L.str.20, 18

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"num_binary_vars (second field of .mv) cannot be negative"
	.size	.L.str.21, 57

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"num_vars (1st field of .mv) must exceed num_binary_vars (2nd field of .mv)"
	.size	.L.str.22, 75

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"kiss"
	.size	.L.str.26, 5

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"type"
	.size	.L.str.27, 5

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"unknown type in .type command"
	.size	.L.str.28, 30

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"PLA size must be declared before .ilb or .ob"
	.size	.L.str.30, 45

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"%s.bar"
	.size	.L.str.31, 7

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"label"
	.size	.L.str.33, 6

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"PLA size must be declared before .label"
	.size	.L.str.34, 40

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"var=%d"
	.size	.L.str.35, 7

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"Error reading labels"
	.size	.L.str.36, 21

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"symbolic"
	.size	.L.str.37, 9

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"error reading .symbolic"
	.size	.L.str.38, 24

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"symbolic-output"
	.size	.L.str.39, 16

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"error reading .symbolic-output"
	.size	.L.str.40, 31

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"phase"
	.size	.L.str.41, 6

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"PLA size must be declared before .phase"
	.size	.L.str.42, 40

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"extra .phase ignored\n"
	.size	.L.str.43, 22

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"only 0 or 1 allowed in phase description"
	.size	.L.str.44, 41

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"pair"
	.size	.L.str.45, 5

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"extra .pair ignored\n"
	.size	.L.str.46, 21

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"syntax error in .pair"
	.size	.L.str.47, 22

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"%c%s "
	.size	.L.str.48, 6

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	" with .kiss option, third to last and second\n"
	.size	.L.str.49, 46

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"to last variables must be the same size.\n"
	.size	.L.str.50, 42

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"MAP-INPUT  "
	.size	.L.str.51, 12

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"MAP-OUTPUT "
	.size	.L.str.52, 12

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"# PLA is %s"
	.size	.L.str.53, 12

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	" with %d inputs and %d outputs\n"
	.size	.L.str.54, 32

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	" with %d variables (%d binary, mv sizes"
	.size	.L.str.55, 40

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	" %d"
	.size	.L.str.56, 4

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"# ON-set cost is  %s\n"
	.size	.L.str.58, 22

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"# OFF-set cost is %s\n"
	.size	.L.str.59, 22

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"# DC-set cost is  %s\n"
	.size	.L.str.60, 22

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"# phase is %s\n"
	.size	.L.str.61, 15

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"# two-bit decoders:"
	.size	.L.str.62, 20

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	" (%d %d)"
	.size	.L.str.63, 9

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"# symbolic: "
	.size	.L.str.65, 13

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"# output symbolic: "
	.size	.L.str.66, 20

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	")"
	.size	.Lstr, 2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
