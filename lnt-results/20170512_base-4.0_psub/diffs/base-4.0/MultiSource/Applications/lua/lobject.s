	.text
	.file	"lobject.bc"
	.hidden	luaO_int2fb
	.globl	luaO_int2fb
	.p2align	4, 0x90
	.type	luaO_int2fb,@function
luaO_int2fb:                            # @luaO_int2fb
	.cfi_startproc
# BB#0:
	movl	$8, %ecx
	cmpl	$16, %edi
	jb	.LBB0_1
# BB#2:                                 # %.lr.ph.preheader
	movl	$8, %ecx
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incl	%edi
	movl	%edi, %edx
	shrl	%edx
	addl	$8, %ecx
	cmpl	$31, %edi
	movl	%edx, %edi
	ja	.LBB0_3
	jmp	.LBB0_4
.LBB0_1:
	movl	%edi, %edx
.LBB0_4:                                # %._crit_edge
	leal	-8(%rdx), %eax
	orl	%ecx, %eax
	cmpl	$8, %edx
	cmovbl	%edx, %eax
	retq
.Lfunc_end0:
	.size	luaO_int2fb, .Lfunc_end0-luaO_int2fb
	.cfi_endproc

	.hidden	luaO_fb2int
	.globl	luaO_fb2int
	.p2align	4, 0x90
	.type	luaO_fb2int,@function
luaO_fb2int:                            # @luaO_fb2int
	.cfi_startproc
# BB#0:
	movl	%edi, %ecx
	shrl	$3, %ecx
	andl	$31, %ecx
	je	.LBB1_2
# BB#1:
	andl	$7, %edi
	orl	$8, %edi
	decl	%ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
.LBB1_2:
	movl	%edi, %eax
	retq
.Lfunc_end1:
	.size	luaO_fb2int, .Lfunc_end1-luaO_fb2int
	.cfi_endproc

	.hidden	luaO_log2
	.globl	luaO_log2
	.p2align	4, 0x90
	.type	luaO_log2,@function
luaO_log2:                              # @luaO_log2
	.cfi_startproc
# BB#0:
	movl	$-1, %ecx
	cmpl	$256, %edi              # imm = 0x100
	jb	.LBB2_1
# BB#2:                                 # %.lr.ph.preheader
	movl	$-1, %ecx
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	addl	$8, %ecx
	movl	%edi, %eax
	shrl	$8, %eax
	cmpl	$65535, %edi            # imm = 0xFFFF
	movl	%eax, %edi
	ja	.LBB2_3
	jmp	.LBB2_4
.LBB2_1:
	movl	%edi, %eax
.LBB2_4:                                # %._crit_edge
	movl	%eax, %eax
	movzbl	luaO_log2.log_2(%rax), %eax
	addl	%ecx, %eax
	retq
.Lfunc_end2:
	.size	luaO_log2, .Lfunc_end2-luaO_log2
	.cfi_endproc

	.hidden	luaO_rawequalObj
	.globl	luaO_rawequalObj
	.p2align	4, 0x90
	.type	luaO_rawequalObj,@function
luaO_rawequalObj:                       # @luaO_rawequalObj
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %ecx
	cmpl	8(%rsi), %ecx
	jne	.LBB3_1
# BB#2:
	cmpl	$3, %ecx
	ja	.LBB3_6
# BB#3:
	movb	$1, %al
	movl	%ecx, %ecx
	jmpq	*.LJTI3_0(,%rcx,8)
.LBB3_5:
	movl	(%rdi), %eax
	cmpl	(%rsi), %eax
	jmp	.LBB3_7
.LBB3_1:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB3_6:
	movq	(%rdi), %rax
	cmpq	(%rsi), %rax
.LBB3_7:
	sete	%al
.LBB3_8:
	movzbl	%al, %eax
	retq
.LBB3_4:
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	cmpeqsd	(%rsi), %xmm0
	movd	%xmm0, %rax
	andl	$1, %eax
	movzbl	%al, %eax
	retq
.Lfunc_end3:
	.size	luaO_rawequalObj, .Lfunc_end3-luaO_rawequalObj
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_8
	.quad	.LBB3_5
	.quad	.LBB3_6
	.quad	.LBB3_4

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI4_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.text
	.hidden	luaO_str2d
	.globl	luaO_str2d
	.p2align	4, 0x90
	.type	luaO_str2d,@function
luaO_str2d:                             # @luaO_str2d
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	%rsp, %rsi
	callq	strtod
	movsd	%xmm0, (%r14)
	movq	(%rsp), %rbx
	cmpq	%r15, %rbx
	je	.LBB4_1
# BB#2:
	movb	(%rbx), %r12b
	movl	%r12d, %eax
	orb	$32, %al
	cmpb	$120, %al
	jne	.LBB4_4
# BB#3:
	movq	%rsp, %rsi
	movl	$16, %edx
	movq	%r15, %rdi
	callq	strtoul
	movd	%rax, %xmm0
	punpckldq	.LCPI4_0(%rip), %xmm0 # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	.LCPI4_1(%rip), %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm1
	movlpd	%xmm1, (%r14)
	movq	(%rsp), %rbx
	movb	(%rbx), %r12b
.LBB4_4:
	testb	%r12b, %r12b
	je	.LBB4_5
# BB#6:                                 # %.preheader
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzbl	%r12b, %ecx
	testb	$32, 1(%rax,%rcx,2)
	je	.LBB4_9
	.p2align	4, 0x90
.LBB4_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1(%rbx), %r12d
	incq	%rbx
	testb	$32, 1(%rax,%r12,2)
	jne	.LBB4_7
# BB#8:                                 # %._crit_edge
	movq	%rbx, (%rsp)
.LBB4_9:
	xorl	%eax, %eax
	testb	%r12b, %r12b
	sete	%al
	jmp	.LBB4_10
.LBB4_1:
	xorl	%eax, %eax
	jmp	.LBB4_10
.LBB4_5:
	movl	$1, %eax
.LBB4_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	luaO_str2d, .Lfunc_end4-luaO_str2d
	.cfi_endproc

	.hidden	luaO_pushvfstring
	.globl	luaO_pushvfstring
	.p2align	4, 0x90
	.type	luaO_pushvfstring,@function
luaO_pushvfstring:                      # @luaO_pushvfstring
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 96
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movq	16(%r13), %rbp
	movl	$.L.str, %esi
	xorl	%edx, %edx
	callq	luaS_newlstr
	movq	%rax, (%rbp)
	movl	$4, 8(%rbp)
	movq	16(%r13), %r12
	movq	56(%r13), %rax
	subq	%r12, %rax
	cmpq	$16, %rax
	jg	.LBB5_2
# BB#1:
	movl	$1, %esi
	movq	%r13, %rdi
	callq	luaD_growstack
	movq	16(%r13), %r12
.LBB5_2:                                # %pushstr.exit
	addq	$16, %r12
	movq	%r12, 16(%r13)
	movl	$37, %esi
	movq	%rbx, %rdi
	callq	strchr
	movq	%rax, %r15
	movl	$1, %ebp
	testq	%r15, %r15
	jne	.LBB5_4
	jmp	.LBB5_37
	.p2align	4, 0x90
.LBB5_36:                               # %pushstr.exit80
                                        #   in Loop: Header=BB5_4 Depth=1
	addq	$16, %r12
	movq	%r12, 16(%r13)
	addl	$2, %ebp
	movq	%r15, %rbx
	addq	$2, %rbx
	movl	$37, %esi
	movq	%rbx, %rdi
	callq	strchr
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB5_37
.LBB5_4:                                # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdx
	subq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	luaS_newlstr
	movq	%rax, (%r12)
	movl	$4, 8(%r12)
	movq	16(%r13), %rbx
	movq	56(%r13), %rax
	subq	%rbx, %rax
	cmpq	$16, %rax
	jg	.LBB5_6
# BB#5:                                 #   in Loop: Header=BB5_4 Depth=1
	movl	$1, %esi
	movq	%r13, %rdi
	callq	luaD_growstack
	movq	16(%r13), %rbx
.LBB5_6:                                #   in Loop: Header=BB5_4 Depth=1
	leaq	16(%rbx), %rax
	movq	%rax, 16(%r13)
	movzbl	1(%r15), %eax
	movsbl	%al, %ecx
	leal	-99(%rcx), %edx
	cmpl	$16, %edx
	ja	.LBB5_7
# BB#9:                                 #   in Loop: Header=BB5_4 Depth=1
	jmpq	*.LJTI5_0(,%rdx,8)
.LBB5_14:                               #   in Loop: Header=BB5_4 Depth=1
	movslq	(%r14), %rcx
	cmpq	$40, %rcx
	ja	.LBB5_16
# BB#15:                                #   in Loop: Header=BB5_4 Depth=1
	movq	%rcx, %rax
	addq	16(%r14), %rax
	leal	8(%rcx), %ecx
	movl	%ecx, (%r14)
	jmp	.LBB5_17
	.p2align	4, 0x90
.LBB5_7:                                #   in Loop: Header=BB5_4 Depth=1
	cmpl	$37, %ecx
	jne	.LBB5_32
# BB#8:                                 #   in Loop: Header=BB5_4 Depth=1
	movl	$.L.str.3, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	jmp	.LBB5_33
.LBB5_32:                               #   in Loop: Header=BB5_4 Depth=1
	movb	$37, (%rsp)
	movb	%al, 1(%rsp)
	movb	$0, 2(%rsp)
	movq	%rsp, %r12
	movq	%r12, %rdi
	callq	strlen
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
.LBB5_33:                               #   in Loop: Header=BB5_4 Depth=1
	callq	luaS_newlstr
	movq	%rax, 16(%rbx)
	movl	$4, 24(%rbx)
	jmp	.LBB5_34
.LBB5_19:                               #   in Loop: Header=BB5_4 Depth=1
	movslq	(%r14), %rcx
	cmpq	$40, %rcx
	ja	.LBB5_21
# BB#20:                                #   in Loop: Header=BB5_4 Depth=1
	movq	%rcx, %rax
	addq	16(%r14), %rax
	leal	8(%rcx), %ecx
	movl	%ecx, (%r14)
	jmp	.LBB5_22
.LBB5_24:                               #   in Loop: Header=BB5_4 Depth=1
	movslq	4(%r14), %rcx
	cmpq	$160, %rcx
	ja	.LBB5_26
# BB#25:                                #   in Loop: Header=BB5_4 Depth=1
	movq	%rcx, %rax
	addq	16(%r14), %rax
	leal	16(%rcx), %ecx
	movl	%ecx, 4(%r14)
	jmp	.LBB5_27
.LBB5_28:                               #   in Loop: Header=BB5_4 Depth=1
	movslq	(%r14), %rcx
	cmpq	$40, %rcx
	ja	.LBB5_30
# BB#29:                                #   in Loop: Header=BB5_4 Depth=1
	movq	%rcx, %rax
	addq	16(%r14), %rax
	leal	8(%rcx), %ecx
	movl	%ecx, (%r14)
	jmp	.LBB5_31
.LBB5_10:                               #   in Loop: Header=BB5_4 Depth=1
	movslq	(%r14), %rcx
	cmpq	$40, %rcx
	ja	.LBB5_12
# BB#11:                                #   in Loop: Header=BB5_4 Depth=1
	movq	%rcx, %rax
	addq	16(%r14), %rax
	leal	8(%rcx), %ecx
	movl	%ecx, (%r14)
	jmp	.LBB5_13
.LBB5_16:                               #   in Loop: Header=BB5_4 Depth=1
	movq	8(%r14), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, 8(%r14)
.LBB5_17:                               #   in Loop: Header=BB5_4 Depth=1
	movzbl	(%rax), %eax
	movb	%al, (%rsp)
	movb	$0, 1(%rsp)
	movq	16(%r13), %rbx
	movq	%rsp, %r12
	jmp	.LBB5_18
.LBB5_21:                               #   in Loop: Header=BB5_4 Depth=1
	movq	8(%r14), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, 8(%r14)
.LBB5_22:                               #   in Loop: Header=BB5_4 Depth=1
	cvtsi2sdl	(%rax), %xmm0
	movsd	%xmm0, 16(%rbx)
	movl	$3, 24(%rbx)
	jmp	.LBB5_34
.LBB5_26:                               #   in Loop: Header=BB5_4 Depth=1
	movq	8(%r14), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, 8(%r14)
.LBB5_27:                               #   in Loop: Header=BB5_4 Depth=1
	movq	(%rax), %rax
	movq	%rax, 16(%rbx)
	movl	$3, 24(%rbx)
	jmp	.LBB5_34
.LBB5_30:                               #   in Loop: Header=BB5_4 Depth=1
	movq	8(%r14), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, 8(%r14)
.LBB5_31:                               #   in Loop: Header=BB5_4 Depth=1
	movq	(%rax), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rsp, %r12
	movq	%r12, %rdi
	callq	sprintf
	movq	16(%r13), %rbx
.LBB5_18:                               #   in Loop: Header=BB5_4 Depth=1
	movq	%r12, %rdi
	callq	strlen
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	callq	luaS_newlstr
	movq	%rax, (%rbx)
	movl	$4, 8(%rbx)
	jmp	.LBB5_34
.LBB5_12:                               #   in Loop: Header=BB5_4 Depth=1
	movq	8(%r14), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, 8(%r14)
.LBB5_13:                               #   in Loop: Header=BB5_4 Depth=1
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	movl	$.L.str.1, %eax
	cmoveq	%rax, %rbx
	movq	16(%r13), %r12
	movq	%rbx, %rdi
	callq	strlen
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	luaS_newlstr
	movq	%rax, (%r12)
	movl	$4, 8(%r12)
	.p2align	4, 0x90
.LBB5_34:                               #   in Loop: Header=BB5_4 Depth=1
	movq	16(%r13), %r12
	movq	56(%r13), %rax
	subq	%r12, %rax
	cmpq	$16, %rax
	jg	.LBB5_36
# BB#35:                                #   in Loop: Header=BB5_4 Depth=1
	movl	$1, %esi
	movq	%r13, %rdi
	callq	luaD_growstack
	movq	16(%r13), %r12
	jmp	.LBB5_36
.LBB5_37:                               # %._crit_edge
	movq	%rbx, %rdi
	callq	strlen
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	luaS_newlstr
	movq	%rax, (%r12)
	movl	$4, 8(%r12)
	movq	16(%r13), %rdx
	movq	56(%r13), %rax
	subq	%rdx, %rax
	cmpq	$16, %rax
	jg	.LBB5_39
# BB#38:
	movl	$1, %esi
	movq	%r13, %rdi
	callq	luaD_growstack
	movq	16(%r13), %rdx
.LBB5_39:                               # %pushstr.exit82
	addq	$16, %rdx
	movq	%rdx, 16(%r13)
	leal	1(%rbp), %esi
	subq	24(%r13), %rdx
	shrq	$4, %rdx
	decl	%edx
	movq	%r13, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	luaV_concat
	movq	16(%r13), %rax
	movslq	%ebp, %rcx
	shlq	$4, %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	negq	%rcx
	movq	%rdx, 16(%r13)
	movq	-16(%rax,%rcx), %rax
	addq	$24, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	luaO_pushvfstring, .Lfunc_end5-luaO_pushvfstring
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI5_0:
	.quad	.LBB5_14
	.quad	.LBB5_19
	.quad	.LBB5_32
	.quad	.LBB5_24
	.quad	.LBB5_32
	.quad	.LBB5_32
	.quad	.LBB5_32
	.quad	.LBB5_32
	.quad	.LBB5_32
	.quad	.LBB5_32
	.quad	.LBB5_32
	.quad	.LBB5_32
	.quad	.LBB5_32
	.quad	.LBB5_28
	.quad	.LBB5_32
	.quad	.LBB5_32
	.quad	.LBB5_10

	.text
	.hidden	luaO_pushfstring
	.globl	luaO_pushfstring
	.p2align	4, 0x90
	.type	luaO_pushfstring,@function
luaO_pushfstring:                       # @luaO_pushfstring
	.cfi_startproc
# BB#0:
	subq	$216, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 224
	testb	%al, %al
	je	.LBB6_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB6_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	224(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$16, (%rsp)
	movq	%rsp, %rdx
	callq	luaO_pushvfstring
	addq	$216, %rsp
	retq
.Lfunc_end6:
	.size	luaO_pushfstring, .Lfunc_end6-luaO_pushfstring
	.cfi_endproc

	.hidden	luaO_chunkid
	.globl	luaO_chunkid
	.p2align	4, 0x90
	.type	luaO_chunkid,@function
luaO_chunkid:                           # @luaO_chunkid
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -32
.Lcfi27:
	.cfi_offset %r14, -24
.Lcfi28:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movb	(%rbx), %cl
	testb	%cl, %cl
	je	.LBB7_1
# BB#2:
	cmpb	$61, %cl
	je	.LBB7_16
# BB#3:
	cmpb	$64, %cl
	jne	.LBB7_4
# BB#10:
	incq	%rbx
	addq	$-8, %r15
	movq	%rbx, %rdi
	callq	strlen
	movb	$0, (%r14)
	subq	%r15, %rax
	jbe	.LBB7_12
# BB#11:
	addq	%rax, %rbx
	movq	%r14, %rdi
	callq	strlen
	movl	$3026478, (%r14,%rax)   # imm = 0x2E2E2E
.LBB7_12:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	strcat                  # TAILCALL
.LBB7_1:
	xorl	%eax, %eax
	jmp	.LBB7_8
.LBB7_16:
	incq	%rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	strncpy
	movb	$0, -1(%r14,%r15)
	jmp	.LBB7_15
.LBB7_4:                                # %switch.early.test.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_5:                                # %switch.early.test
                                        # =>This Inner Loop Header: Depth=1
	movsbl	%cl, %ecx
	cmpl	$10, %ecx
	je	.LBB7_8
# BB#6:                                 # %switch.early.test
                                        #   in Loop: Header=BB7_5 Depth=1
	cmpl	$13, %ecx
	je	.LBB7_8
# BB#7:                                 # %switch.early.test..preheader_crit_edge
                                        #   in Loop: Header=BB7_5 Depth=1
	movzbl	1(%rbx,%rax), %ecx
	incq	%rax
	testb	%cl, %cl
	jne	.LBB7_5
.LBB7_8:                                # %__strcspn_c2.exit
	addq	$-17, %r15
	cmpq	%r15, %rax
	cmovbeq	%rax, %r15
	movabsq	$2334956330985747291, %rax # imm = 0x20676E697274735B
	movq	%rax, (%r14)
	movw	$34, 8(%r14)
	cmpb	$0, (%rbx,%r15)
	je	.LBB7_13
# BB#9:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	strncat
	movq	%r14, %rdi
	callq	strlen
	movl	$3026478, (%r14,%rax)   # imm = 0x2E2E2E
	jmp	.LBB7_14
.LBB7_13:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	strcat
.LBB7_14:
	movq	%r14, %rdi
	callq	strlen
	movb	$0, 2(%r14,%rax)
	movw	$23842, (%r14,%rax)     # imm = 0x5D22
.LBB7_15:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	luaO_chunkid, .Lfunc_end7-luaO_chunkid
	.cfi_endproc

	.hidden	luaO_nilobject_         # @luaO_nilobject_
	.type	luaO_nilobject_,@object
	.section	.rodata,"a",@progbits
	.globl	luaO_nilobject_
	.p2align	3
luaO_nilobject_:
	.zero	16
	.size	luaO_nilobject_, 16

	.type	luaO_log2.log_2,@object # @luaO_log2.log_2
	.p2align	4
luaO_log2.log_2:
	.ascii	"\000\001\002\002\003\003\003\003\004\004\004\004\004\004\004\004\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\005\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\007\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b"
	.size	luaO_log2.log_2, 256

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.zero	1
	.size	.L.str, 1

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"(null)"
	.size	.L.str.1, 7

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%p"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%"
	.size	.L.str.3, 2

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"[string \""
	.size	.L.str.6, 10

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\"]"
	.size	.L.str.7, 3

	.hidden	luaS_newlstr
	.hidden	luaD_growstack
	.hidden	luaV_concat

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
