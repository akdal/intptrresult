	.text
	.file	"warshall.bc"
	.globl	TC
	.p2align	4, 0x90
	.type	TC,@function
TC:                                     # @TC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, -48(%rsp)         # 8-byte Spill
	leal	31(%rsi), %eax
	sarl	$31, %eax
	shrl	$27, %eax
	leal	31(%rsi,%rax), %eax
	sarl	$5, %eax
	movl	%eax, %ecx
	imull	%esi, %ecx
	testl	%ecx, %ecx
	jle	.LBB0_15
# BB#1:                                 # %.preheader53.lr.ph
	movslq	%ecx, %rcx
	movq	-48(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,4), %r13
	movslq	%eax, %r11
	leaq	(,%r11,4), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rdx, %r12
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB0_2:                                # %.preheader53.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_3 Depth 2
                                        #       Child Loop BB0_23 Depth 3
                                        #       Child Loop BB0_9 Depth 3
                                        #       Child Loop BB0_11 Depth 3
	movq	%r11, %rax
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	imulq	%rcx, %rax
	movq	-48(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,4), %rdi
	movq	%rdi, -16(%rsp)         # 8-byte Spill
	leaq	4(%rcx,%rax,4), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movq	%rdx, -32(%rsp)         # 8-byte Spill
	movq	%rdx, %rbx
	movq	%rcx, %rax
	jmp	.LBB0_3
.LBB0_19:                               # %vector.body.preheader
                                        #   in Loop: Header=BB0_3 Depth=2
	leaq	-8(%rdx), %rdi
	movq	%rdi, %rcx
	shrq	$3, %rcx
	btl	$3, %edi
	jb	.LBB0_20
# BB#21:                                # %vector.body.prol
                                        #   in Loop: Header=BB0_3 Depth=2
	movups	(%r12), %xmm0
	movups	16(%r12), %xmm1
	movups	(%rax), %xmm2
	movups	16(%rax), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movl	$8, %ebp
	testq	%rcx, %rcx
	jne	.LBB0_23
	jmp	.LBB0_24
.LBB0_20:                               #   in Loop: Header=BB0_3 Depth=2
	xorl	%ebp, %ebp
	testq	%rcx, %rcx
	je	.LBB0_24
	.p2align	4, 0x90
.LBB0_23:                               # %vector.body
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	(%r12,%rbp,4), %xmm0
	movups	16(%r12,%rbp,4), %xmm1
	movups	(%rax,%rbp,4), %xmm2
	movups	16(%rax,%rbp,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rax,%rbp,4)
	movups	%xmm3, 16(%rax,%rbp,4)
	movups	32(%r12,%rbp,4), %xmm0
	movups	48(%r12,%rbp,4), %xmm1
	movups	32(%rax,%rbp,4), %xmm2
	movups	48(%rax,%rbp,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, 32(%rax,%rbp,4)
	movups	%xmm3, 48(%rax,%rbp,4)
	addq	$16, %rbp
	cmpq	%rbp, %rdx
	jne	.LBB0_23
.LBB0_24:                               # %middle.block
                                        #   in Loop: Header=BB0_3 Depth=2
	cmpq	%rdx, %r14
	je	.LBB0_12
# BB#25:                                #   in Loop: Header=BB0_3 Depth=2
	leaq	(%rax,%rdx,4), %rax
	leaq	(%r12,%rdx,4), %rdx
	jmp	.LBB0_7
	.p2align	4, 0x90
.LBB0_3:                                #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_23 Depth 3
                                        #       Child Loop BB0_9 Depth 3
                                        #       Child Loop BB0_11 Depth 3
	movl	(%rbx), %edx
	andl	%r15d, %edx
	leaq	(%rax,%r11,4), %r8
	movq	%rax, %rcx
	cmoveq	%r8, %rcx
	testl	%edx, %edx
	je	.LBB0_13
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=2
	testl	%esi, %esi
	jle	.LBB0_13
# BB#5:                                 # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB0_3 Depth=2
	leaq	4(%rax), %r10
	cmpq	%r10, %r8
	movq	%r10, %r9
	cmovaq	%r8, %r9
	subq	%rax, %r9
	decq	%r9
	shrq	$2, %r9
	leaq	1(%r9), %r14
	cmpq	$7, %r14
	jbe	.LBB0_6
# BB#16:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_3 Depth=2
	movq	%r14, %rdx
	movabsq	$9223372036854775800, %rcx # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rcx, %rdx
	je	.LBB0_6
# BB#17:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_3 Depth=2
	cmpq	%r10, %r8
	movq	%r10, %rcx
	cmovaq	%r8, %rcx
	subq	%rax, %rcx
	decq	%rcx
	andq	$-4, %rcx
	movq	-8(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rcx), %rdi
	cmpq	%rdi, %rax
	jae	.LBB0_19
# BB#18:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_3 Depth=2
	leaq	4(%rax,%rcx), %rcx
	cmpq	%rcx, -16(%rsp)         # 8-byte Folded Reload
	jae	.LBB0_19
	.p2align	4, 0x90
.LBB0_6:                                #   in Loop: Header=BB0_3 Depth=2
	movq	%r12, %rdx
.LBB0_7:                                # %.lr.ph.us.preheader97
                                        #   in Loop: Header=BB0_3 Depth=2
	leaq	4(%rax), %rcx
	cmpq	%rcx, %r8
	cmovaq	%r8, %rcx
	subq	%rax, %rcx
	decq	%rcx
	movl	%ecx, %ebp
	shrl	$2, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB0_10
# BB#8:                                 # %.lr.ph.us.prol.preheader
                                        #   in Loop: Header=BB0_3 Depth=2
	negq	%rbp
	.p2align	4, 0x90
.LBB0_9:                                # %.lr.ph.us.prol
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdx), %edi
	addq	$4, %rdx
	orl	%edi, (%rax)
	addq	$4, %rax
	incq	%rbp
	jne	.LBB0_9
.LBB0_10:                               # %.lr.ph.us.prol.loopexit
                                        #   in Loop: Header=BB0_3 Depth=2
	cmpq	$12, %rcx
	jb	.LBB0_12
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph.us
                                        #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdx), %ecx
	orl	%ecx, (%rax)
	movl	4(%rdx), %ecx
	orl	%ecx, 4(%rax)
	movl	8(%rdx), %ecx
	orl	%ecx, 8(%rax)
	movl	12(%rdx), %ecx
	orl	%ecx, 12(%rax)
	addq	$16, %rax
	addq	$16, %rdx
	cmpq	%r8, %rax
	jb	.LBB0_11
.LBB0_12:                               # %.loopexit.us.loopexit
                                        #   in Loop: Header=BB0_3 Depth=2
	leaq	(%r10,%r9,4), %rcx
.LBB0_13:                               # %.loopexit.us
                                        #   in Loop: Header=BB0_3 Depth=2
	leaq	(%rbx,%r11,4), %rbx
	cmpq	%r13, %rcx
	movq	%rcx, %rax
	jb	.LBB0_3
# BB#14:                                # %._crit_edge.us
                                        #   in Loop: Header=BB0_2 Depth=1
	addl	%r15d, %r15d
	movq	-32(%rsp), %rdx         # 8-byte Reload
	leaq	4(%rdx), %rax
	testl	%r15d, %r15d
	movl	$1, %ecx
	cmovel	%ecx, %r15d
	cmoveq	%rax, %rdx
	addq	-40(%rsp), %r12         # 8-byte Folded Reload
	movq	-24(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	cmpq	%r13, %r12
	jb	.LBB0_2
.LBB0_15:                               # %._crit_edge64
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	TC, .Lfunc_end0-TC
	.cfi_endproc

	.globl	RTC
	.p2align	4, 0x90
	.type	RTC,@function
RTC:                                    # @RTC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 56
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	leal	31(%rsi), %eax
	sarl	$31, %eax
	shrl	$27, %eax
	leal	31(%rsi,%rax), %eax
	sarl	$5, %eax
	movl	%eax, %ecx
	imull	%esi, %ecx
	testl	%ecx, %ecx
	jle	.LBB1_22
# BB#1:                                 # %.preheader53.lr.ph.i
	movl	%ecx, -44(%rsp)         # 4-byte Spill
	movslq	%ecx, %rcx
	leaq	(%rdi,%rcx,4), %r15
	movslq	%eax, %rcx
	leaq	(,%rcx,4), %rax
	testl	%esi, %esi
	jle	.LBB1_15
# BB#2:                                 # %.preheader53.us.i.preheader
	movq	%rax, -40(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	movq	%rdi, %r14
	movq	%rdi, %rsi
	movl	$1, %r12d
	movq	%rdi, -32(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader53.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
                                        #       Child Loop BB1_30 Depth 3
                                        #       Child Loop BB1_9 Depth 3
                                        #       Child Loop BB1_11 Depth 3
	movq	%rcx, %rax
	movq	%rdx, -16(%rsp)         # 8-byte Spill
	imulq	%rdx, %rax
	leaq	(%rdi,%rax,4), %rdx
	movq	%rdx, -8(%rsp)          # 8-byte Spill
	movq	%rdi, %rdx
	leaq	4(%rdx,%rax,4), %r13
	movq	%rsi, -24(%rsp)         # 8-byte Spill
	movq	%rsi, %rbx
	jmp	.LBB1_4
.LBB1_26:                               # %vector.body.preheader
                                        #   in Loop: Header=BB1_4 Depth=2
	leaq	-8(%rsi), %rax
	movq	%rax, %rbp
	shrq	$3, %rbp
	btl	$3, %eax
	jb	.LBB1_27
# BB#28:                                # %vector.body.prol
                                        #   in Loop: Header=BB1_4 Depth=2
	movups	(%r14), %xmm0
	movups	16(%r14), %xmm1
	movups	(%rdx), %xmm2
	movups	16(%rdx), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rdx)
	movups	%xmm3, 16(%rdx)
	movl	$8, %eax
	testq	%rbp, %rbp
	jne	.LBB1_30
	jmp	.LBB1_31
.LBB1_27:                               #   in Loop: Header=BB1_4 Depth=2
	xorl	%eax, %eax
	testq	%rbp, %rbp
	je	.LBB1_31
	.p2align	4, 0x90
.LBB1_30:                               # %vector.body
                                        #   Parent Loop BB1_3 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	(%r14,%rax,4), %xmm0
	movups	16(%r14,%rax,4), %xmm1
	movups	(%rdx,%rax,4), %xmm2
	movups	16(%rdx,%rax,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rdx,%rax,4)
	movups	%xmm3, 16(%rdx,%rax,4)
	movups	32(%r14,%rax,4), %xmm0
	movups	48(%r14,%rax,4), %xmm1
	movups	32(%rdx,%rax,4), %xmm2
	movups	48(%rdx,%rax,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, 32(%rdx,%rax,4)
	movups	%xmm3, 48(%rdx,%rax,4)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.LBB1_30
.LBB1_31:                               # %middle.block
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpq	%rsi, %r9
	je	.LBB1_12
# BB#32:                                #   in Loop: Header=BB1_4 Depth=2
	leaq	(%rdx,%rsi,4), %rdx
	leaq	(%r14,%rsi,4), %rsi
	jmp	.LBB1_7
	.p2align	4, 0x90
.LBB1_4:                                #   Parent Loop BB1_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_30 Depth 3
                                        #       Child Loop BB1_9 Depth 3
                                        #       Child Loop BB1_11 Depth 3
	testl	(%rbx), %r12d
	leaq	(%rdx,%rcx,4), %r8
	je	.LBB1_13
# BB#5:                                 # %.lr.ph.us.preheader.i
                                        #   in Loop: Header=BB1_4 Depth=2
	leaq	4(%rdx), %r11
	cmpq	%r11, %r8
	movq	%r11, %r10
	cmovaq	%r8, %r10
	subq	%rdx, %r10
	decq	%r10
	movq	%r10, %r9
	shrq	$2, %r9
	incq	%r9
	cmpq	$7, %r9
	jbe	.LBB1_6
# BB#23:                                # %min.iters.checked
                                        #   in Loop: Header=BB1_4 Depth=2
	movq	%r9, %rsi
	movabsq	$9223372036854775800, %rax # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rax, %rsi
	je	.LBB1_6
# BB#24:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpq	%r11, %r8
	movq	%r11, %rax
	cmovaq	%r8, %rax
	subq	%rdx, %rax
	decq	%rax
	andq	$-4, %rax
	leaq	(%r13,%rax), %rdi
	cmpq	%rdi, %rdx
	jae	.LBB1_26
# BB#25:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_4 Depth=2
	leaq	4(%rdx,%rax), %rax
	cmpq	%rax, -8(%rsp)          # 8-byte Folded Reload
	jae	.LBB1_26
	.p2align	4, 0x90
.LBB1_6:                                #   in Loop: Header=BB1_4 Depth=2
	movq	%r14, %rsi
.LBB1_7:                                # %.lr.ph.us.i.preheader
                                        #   in Loop: Header=BB1_4 Depth=2
	leaq	4(%rdx), %rbp
	cmpq	%rbp, %r8
	cmovaq	%r8, %rbp
	subq	%rdx, %rbp
	decq	%rbp
	movl	%ebp, %eax
	shrl	$2, %eax
	incl	%eax
	andq	$3, %rax
	je	.LBB1_10
# BB#8:                                 # %.lr.ph.us.i.prol.preheader
                                        #   in Loop: Header=BB1_4 Depth=2
	negq	%rax
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph.us.i.prol
                                        #   Parent Loop BB1_3 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rsi), %edi
	addq	$4, %rsi
	orl	%edi, (%rdx)
	addq	$4, %rdx
	incq	%rax
	jne	.LBB1_9
.LBB1_10:                               # %.lr.ph.us.i.prol.loopexit
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpq	$12, %rbp
	jb	.LBB1_12
	.p2align	4, 0x90
.LBB1_11:                               # %.lr.ph.us.i
                                        #   Parent Loop BB1_3 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rsi), %eax
	orl	%eax, (%rdx)
	movl	4(%rsi), %eax
	orl	%eax, 4(%rdx)
	movl	8(%rsi), %eax
	orl	%eax, 8(%rdx)
	movl	12(%rsi), %eax
	orl	%eax, 12(%rdx)
	addq	$16, %rdx
	addq	$16, %rsi
	cmpq	%r8, %rdx
	jb	.LBB1_11
.LBB1_12:                               # %.loopexit.us.loopexit.i
                                        #   in Loop: Header=BB1_4 Depth=2
	andq	$-4, %r10
	addq	%r10, %r11
	movq	%r11, %r8
.LBB1_13:                               # %.loopexit.us.i
                                        #   in Loop: Header=BB1_4 Depth=2
	leaq	(%rbx,%rcx,4), %rbx
	cmpq	%r15, %r8
	movq	%r8, %rdx
	jb	.LBB1_4
# BB#14:                                # %._crit_edge.us.i
                                        #   in Loop: Header=BB1_3 Depth=1
	addl	%r12d, %r12d
	movq	-24(%rsp), %rsi         # 8-byte Reload
	leaq	4(%rsi), %rax
	testl	%r12d, %r12d
	movl	$1, %edx
	cmovel	%edx, %r12d
	cmoveq	%rax, %rsi
	addq	-40(%rsp), %r14         # 8-byte Folded Reload
	movq	-16(%rsp), %rdx         # 8-byte Reload
	incq	%rdx
	cmpq	%r15, %r14
	movq	-32(%rsp), %rdi         # 8-byte Reload
	jb	.LBB1_3
	jmp	.LBB1_19
.LBB1_15:                               # %.preheader53.us.i.us.preheader
	movl	$1, %r8d
	movq	%rdi, %r9
	movq	%rdi, %r10
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB1_16:                               # %.preheader53.us.i.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_17 Depth 2
	movq	%r10, %rbp
	movq	%rdi, %rdx
	.p2align	4, 0x90
.LBB1_17:                               # %.loopexit.us.i.us.us
                                        #   Parent Loop BB1_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	(%rbp), %ebx
	leaq	(%rdx,%rcx,4), %rsi
	cmoveq	%rsi, %rdx
	addq	%rax, %rbp
	cmpq	%r15, %rdx
	jb	.LBB1_17
# BB#18:                                # %._crit_edge.us.i.us-lcssa.us.us
                                        #   in Loop: Header=BB1_16 Depth=1
	addl	%ebx, %ebx
	leaq	4(%r10), %rdx
	testl	%ebx, %ebx
	cmovel	%r8d, %ebx
	cmoveq	%rdx, %r10
	leaq	(%r9,%rcx,4), %r9
	cmpq	%r15, %r9
	jb	.LBB1_16
.LBB1_19:                               # %TC.exit.preheader
	cmpl	$0, -44(%rsp)           # 4-byte Folded Reload
	jle	.LBB1_22
# BB#20:                                # %.lr.ph
	movl	$1, %eax
	movl	$1, %edx
	.p2align	4, 0x90
.LBB1_21:                               # %TC.exit
                                        # =>This Inner Loop Header: Depth=1
	orl	%edx, (%rdi)
	addl	%edx, %edx
	leaq	4(%rdi), %rsi
	testl	%edx, %edx
	cmovneq	%rdi, %rsi
	cmovel	%eax, %edx
	leaq	(%rsi,%rcx,4), %rdi
	cmpq	%r15, %rdi
	jb	.LBB1_21
.LBB1_22:                               # %TC.exit._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	RTC, .Lfunc_end1-RTC
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
