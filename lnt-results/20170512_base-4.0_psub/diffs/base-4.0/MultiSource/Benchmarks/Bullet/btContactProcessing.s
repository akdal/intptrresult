	.text
	.file	"btContactProcessing.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	1148846080              # float 1000
.LCPI0_1:
	.long	1065353216              # float 1
.LCPI0_2:
	.long	1151770624              # float 1333
.LCPI0_3:
	.long	1157976064              # float 2133
.LCPI0_4:
	.long	1077936128              # float 3
.LCPI0_5:
	.long	3072837036              # float -9.99999974E-6
.LCPI0_6:
	.long	925353388               # float 9.99999974E-6
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_7:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	_ZN14btContactArray14merge_contactsERKS_b
	.p2align	4, 0x90
	.type	_ZN14btContactArray14merge_contactsERKS_b,@function
_ZN14btContactArray14merge_contactsERKS_b: # @_ZN14btContactArray14merge_contactsERKS_b
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 272
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %r9
	movq	%rdi, %r12
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	movq	%r9, (%rsp)             # 8-byte Spill
	je	.LBB0_4
# BB#1:
	cmpb	$0, 24(%r12)
	je	.LBB0_3
# BB#2:
	callq	_Z21btAlignedFreeInternalPv
	movq	(%rsp), %r9             # 8-byte Reload
.LBB0_3:
	movq	$0, 16(%r12)
.LBB0_4:                                # %_ZN20btAlignedObjectArrayI11GIM_CONTACTE5clearEv.exit
	movb	$1, 24(%r12)
	movq	$0, 16(%r12)
	movq	$0, 4(%r12)
	movslq	4(%r9), %rbp
	testq	%rbp, %rbp
	je	.LBB0_88
# BB#5:                                 # %_ZN20btAlignedObjectArrayI11GIM_CONTACTE5clearEv.exit
	cmpl	$1, %ebp
	jne	.LBB0_6
# BB#96:
	movq	16(%r9), %rsi
	movq	%r12, %rdi
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_ # TAILCALL
.LBB0_6:
	movl	%ebx, 60(%rsp)          # 4-byte Spill
	movb	$1, 40(%rsp)
	movq	$0, 32(%rsp)
	movq	$0, 20(%rsp)
	testl	%ebp, %ebp
	jle	.LBB0_7
# BB#8:
	movq	%rbp, %rdi
	shlq	$3, %rdi
.Ltmp0:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	(%rsp), %r9             # 8-byte Reload
	movq	%rax, %r13
.Ltmp1:
# BB#9:                                 # %_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE8allocateEi.exit.i
	movslq	20(%rsp), %rax
	testq	%rax, %rax
	movq	32(%rsp), %rdi
	jle	.LBB0_15
# BB#10:                                # %.lr.ph.i.i
	leaq	-1(%rax), %rcx
	movq	%rax, %rsi
	xorl	%edx, %edx
	andq	$3, %rsi
	je	.LBB0_12
	.p2align	4, 0x90
.LBB0_11:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx,8), %ebx
	movl	%ebx, (%r13,%rdx,8)
	movl	4(%rdi,%rdx,8), %ebx
	movl	%ebx, 4(%r13,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rsi
	jne	.LBB0_11
.LBB0_12:                               # %.prol.loopexit118
	cmpq	$3, %rcx
	jb	.LBB0_16
# BB#13:                                # %.lr.ph.i.i.new
	subq	%rdx, %rax
	leaq	28(%r13,%rdx,8), %rcx
	leaq	28(%rdi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB0_14:                               # =>This Inner Loop Header: Depth=1
	movl	-28(%rdx), %esi
	movl	%esi, -28(%rcx)
	movl	-24(%rdx), %esi
	movl	%esi, -24(%rcx)
	movl	-20(%rdx), %esi
	movl	%esi, -20(%rcx)
	movl	-16(%rdx), %esi
	movl	%esi, -16(%rcx)
	movl	-12(%rdx), %esi
	movl	%esi, -12(%rcx)
	movl	-8(%rdx), %esi
	movl	%esi, -8(%rcx)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rcx)
	movl	(%rdx), %esi
	movl	%esi, (%rcx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-4, %rax
	jne	.LBB0_14
	jmp	.LBB0_16
.LBB0_7:
	xorl	%r8d, %r8d
	jmp	.LBB0_21
.LBB0_15:                               # %_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4copyEiiPS0_.exit.i
	testq	%rdi, %rdi
	je	.LBB0_19
.LBB0_16:                               # %_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4copyEiiPS0_.exit.thread.i
	cmpb	$0, 40(%rsp)
	je	.LBB0_18
# BB#17:
.Ltmp2:
	callq	_Z21btAlignedFreeInternalPv
	movq	(%rsp), %r9             # 8-byte Reload
.Ltmp3:
.LBB0_18:                               # %.noexc70
	movq	$0, 32(%rsp)
.LBB0_19:                               # %_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7reserveEi.exit.preheader
	movb	$1, 40(%rsp)
	movq	%r13, 32(%rsp)
	movl	%ebp, 24(%rsp)
	movl	4(%r9), %edi
	testl	%edi, %edi
	jle	.LBB0_20
# BB#32:                                # %.lr.ph86
	movl	20(%rsp), %ecx
	xorl	%r15d, %r15d
	movss	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI0_2(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI0_3(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI0_4(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movq	%r13, %r8
	movq	%r13, %rsi
	.p2align	4, 0x90
.LBB0_33:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_43 Depth 2
                                        #     Child Loop BB0_46 Depth 2
	movq	16(%r9), %rax
	leaq	(%r15,%r15,2), %rdx
	shlq	$4, %rdx
	movss	(%rax,%rdx), %xmm7      # xmm7 = mem[0],zero,zero,zero
	movss	4(%rax,%rdx), %xmm6     # xmm6 = mem[0],zero,zero,zero
	movss	8(%rax,%rdx), %xmm5     # xmm5 = mem[0],zero,zero,zero
	cmpl	%ebp, %ecx
	jne	.LBB0_34
# BB#35:                                #   in Loop: Header=BB0_33 Depth=1
	leal	(%rbp,%rbp), %r14d
	testl	%ebp, %ebp
	movl	$1, %eax
	cmovel	%eax, %r14d
	cmpl	%r14d, %ebp
	jge	.LBB0_36
# BB#37:                                #   in Loop: Header=BB0_33 Depth=1
	testl	%r14d, %r14d
	movss	%xmm5, 48(%rsp)         # 4-byte Spill
	movss	%xmm6, 64(%rsp)         # 4-byte Spill
	movss	%xmm7, 12(%rsp)         # 4-byte Spill
	je	.LBB0_38
# BB#39:                                #   in Loop: Header=BB0_33 Depth=1
	movslq	%r14d, %rdi
	shlq	$3, %rdi
.Ltmp4:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
.Ltmp5:
# BB#40:                                # %.noexc73
                                        #   in Loop: Header=BB0_33 Depth=1
	movl	20(%rsp), %ebp
	movq	32(%rsp), %r13
	movq	%r13, %rsi
	movq	(%rsp), %r9             # 8-byte Reload
	movss	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI0_2(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI0_3(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI0_4(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movss	48(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	64(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	testl	%ebp, %ebp
	jg	.LBB0_42
	jmp	.LBB0_47
	.p2align	4, 0x90
.LBB0_34:                               #   in Loop: Header=BB0_33 Depth=1
	movq	%r13, %rbx
	movl	%ebp, %r14d
	jmp	.LBB0_54
	.p2align	4, 0x90
.LBB0_36:                               #   in Loop: Header=BB0_33 Depth=1
	movq	%r13, %rbx
	movl	%ebp, %r14d
	jmp	.LBB0_53
.LBB0_38:                               #   in Loop: Header=BB0_33 Depth=1
	xorl	%ebx, %ebx
	testl	%ebp, %ebp
	jle	.LBB0_47
.LBB0_42:                               # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB0_33 Depth=1
	movslq	%ebp, %rax
	leaq	-1(%rax), %r8
	movq	%rax, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB0_44
	.p2align	4, 0x90
.LBB0_43:                               #   Parent Loop BB0_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi,%rdx,8), %ecx
	movl	%ecx, (%rbx,%rdx,8)
	movl	4(%rsi,%rdx,8), %ecx
	movl	%ecx, 4(%rbx,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB0_43
.LBB0_44:                               # %.prol.loopexit
                                        #   in Loop: Header=BB0_33 Depth=1
	cmpq	$3, %r8
	jb	.LBB0_48
# BB#45:                                # %.lr.ph.i.i.i.new
                                        #   in Loop: Header=BB0_33 Depth=1
	subq	%rdx, %rax
	leaq	28(%rbx,%rdx,8), %rcx
	leaq	28(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB0_46:                               #   Parent Loop BB0_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rdx), %esi
	movl	%esi, -28(%rcx)
	movl	-24(%rdx), %esi
	movl	%esi, -24(%rcx)
	movl	-20(%rdx), %esi
	movl	%esi, -20(%rcx)
	movl	-16(%rdx), %esi
	movl	%esi, -16(%rcx)
	movl	-12(%rdx), %esi
	movl	%esi, -12(%rcx)
	movl	-8(%rdx), %esi
	movl	%esi, -8(%rcx)
	movl	-4(%rdx), %esi
	movl	%esi, -4(%rcx)
	movl	(%rdx), %esi
	movl	%esi, (%rcx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-4, %rax
	jne	.LBB0_46
	jmp	.LBB0_48
.LBB0_47:                               # %_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4copyEiiPS0_.exit.i.i
                                        #   in Loop: Header=BB0_33 Depth=1
	testq	%rsi, %rsi
	je	.LBB0_52
.LBB0_48:                               # %_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4copyEiiPS0_.exit.thread.i.i
                                        #   in Loop: Header=BB0_33 Depth=1
	cmpb	$0, 40(%rsp)
	je	.LBB0_51
# BB#49:                                #   in Loop: Header=BB0_33 Depth=1
.Ltmp6:
	movq	%r13, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp7:
# BB#50:                                # %.noexc74
                                        #   in Loop: Header=BB0_33 Depth=1
	movl	20(%rsp), %ebp
	movq	(%rsp), %r9             # 8-byte Reload
	movss	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI0_2(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI0_3(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI0_4(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movss	48(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	64(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
.LBB0_51:                               #   in Loop: Header=BB0_33 Depth=1
	movq	$0, 32(%rsp)
.LBB0_52:                               # %_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE10deallocateEv.exit.i.i
                                        #   in Loop: Header=BB0_33 Depth=1
	movb	$1, 40(%rsp)
	movq	%rbx, 32(%rsp)
	movl	%r14d, 24(%rsp)
	movl	4(%r9), %edi
	movq	%rbx, %r8
	movq	%rbx, %rsi
.LBB0_53:                               # %_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7reserveEi.exit
                                        #   in Loop: Header=BB0_33 Depth=1
	movl	%ebp, %ecx
.LBB0_54:                               # %_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7reserveEi.exit
                                        #   in Loop: Header=BB0_33 Depth=1
	mulss	%xmm0, %xmm7
	addss	%xmm1, %xmm7
	cvttss2si	%xmm7, %eax
	mulss	%xmm2, %xmm6
	cvttss2si	%xmm6, %edx
	mulss	%xmm3, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %ebp
	shll	$4, %edx
	addl	%eax, %edx
	shll	$8, %ebp
	addl	%edx, %ebp
	movslq	%ecx, %rcx
	movl	%ebp, (%rsi,%rcx,8)
	movl	%r15d, 4(%rsi,%rcx,8)
	incl	%ecx
	movl	%ecx, 20(%rsp)
	incq	%r15
	movslq	%edi, %rax
	cmpq	%rax, %r15
	movq	%rbx, %r13
	movl	%r14d, %ebp
	jl	.LBB0_33
	jmp	.LBB0_22
.LBB0_20:
	movq	%r13, %r8
.LBB0_21:                               # %_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7reserveEi.exit.preheader._ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7reserveEi.exit._crit_edge_crit_edge
	movl	20(%rsp), %ecx
.LBB0_22:                               # %_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7reserveEi.exit._crit_edge
	cmpl	$2, %ecx
	jl	.LBB0_25
# BB#23:
	decl	%ecx
.Ltmp9:
	leaq	16(%rsp), %rdi
	xorl	%esi, %esi
	movl	%ecx, %edx
	callq	_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvT_ii
	movq	(%rsp), %r9             # 8-byte Reload
.Ltmp10:
# BB#24:                                # %._ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9quickSortI22CONTACT_KEY_TOKEN_COMPEEvT_.exit_crit_edge
	movq	32(%rsp), %r8
.LBB0_25:                               # %_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9quickSortI22CONTACT_KEY_TOKEN_COMPEEvT_.exit
	movl	(%r8), %r14d
	movslq	4(%r8), %rax
	leaq	(%rax,%rax,2), %rsi
	shlq	$4, %rsi
	addq	16(%r9), %rsi
.Ltmp12:
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	%r12, %rdi
	callq	_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_
.Ltmp13:
# BB#26:
	movl	20(%rsp), %ecx
	cmpl	$2, %ecx
	movq	(%rsp), %rbp            # 8-byte Reload
	movl	60(%rsp), %edx          # 4-byte Reload
	jl	.LBB0_84
# BB#27:                                # %.lr.ph
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %r12
	testb	%dl, %dl
	je	.LBB0_28
# BB#57:                                # %.lr.ph.split.us.preheader
	xorl	%r15d, %r15d
	movl	$1, %r13d
	.p2align	4, 0x90
.LBB0_58:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_65 Depth 2
	movl	%r14d, %ebx
	movq	32(%rsp), %rdx
	movl	(%rdx,%r13,8), %r14d
	movq	16(%rbp), %rax
	movslq	4(%rdx,%r13,8), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$4, %rdx
	leaq	(%rax,%rdx), %rsi
	cmpl	%r14d, %ebx
	jne	.LBB0_59
# BB#73:                                #   in Loop: Header=BB0_58 Depth=1
	movss	32(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	addss	.LCPI0_5(%rip), %xmm2
	movss	32(%rax,%rdx), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jbe	.LBB0_74
# BB#77:                                #   in Loop: Header=BB0_58 Depth=1
	movups	(%rsi), %xmm0
	movups	16(%rsi), %xmm1
	movups	32(%rsi), %xmm2
	movups	%xmm2, 32(%r12)
	movups	%xmm1, 16(%r12)
	movups	%xmm0, (%r12)
	xorl	%r15d, %r15d
	jmp	.LBB0_78
	.p2align	4, 0x90
.LBB0_59:                               #   in Loop: Header=BB0_58 Depth=1
	testl	%r15d, %r15d
	jle	.LBB0_71
# BB#60:                                # %.lr.ph.preheader.i.us
                                        #   in Loop: Header=BB0_58 Depth=1
	movsd	16(%r12), %xmm2         # xmm2 = mem[0],zero
	movss	24(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movl	%r15d, %eax
	testb	$1, %al
	jne	.LBB0_62
# BB#61:                                #   in Loop: Header=BB0_58 Depth=1
	xorl	%ecx, %ecx
	cmpl	$1, %r15d
	jne	.LBB0_64
	jmp	.LBB0_66
	.p2align	4, 0x90
.LBB0_74:                               #   in Loop: Header=BB0_58 Depth=1
	cmpl	$7, %r15d
	jg	.LBB0_78
# BB#75:                                #   in Loop: Header=BB0_58 Depth=1
	subss	%xmm1, %xmm0
	andps	.LCPI0_7(%rip), %xmm0
	movss	.LCPI0_6(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB0_78
# BB#76:                                #   in Loop: Header=BB0_58 Depth=1
	movslq	%r15d, %rsi
	shlq	$4, %rsi
	movups	16(%rax,%rdx), %xmm0
	movups	%xmm0, 80(%rsp,%rsi)
	incl	%r15d
	jmp	.LBB0_78
.LBB0_62:                               # %.lr.ph.i.us.prol
                                        #   in Loop: Header=BB0_58 Depth=1
	addps	80(%rsp), %xmm2
	addss	88(%rsp), %xmm3
	movl	$1, %ecx
	cmpl	$1, %r15d
	je	.LBB0_66
.LBB0_64:                               # %.lr.ph.preheader.i.us.new
                                        #   in Loop: Header=BB0_58 Depth=1
	subq	%rcx, %rax
	shlq	$4, %rcx
	leaq	104(%rsp), %rdx
	addq	%rdx, %rcx
	.p2align	4, 0x90
.LBB0_65:                               # %.lr.ph.i.us
                                        #   Parent Loop BB0_58 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addps	-24(%rcx), %xmm2
	addss	-16(%rcx), %xmm3
	addps	-8(%rcx), %xmm2
	addss	(%rcx), %xmm3
	addq	$32, %rcx
	addq	$-2, %rax
	jne	.LBB0_65
.LBB0_66:                               # %._crit_edge.i.us
                                        #   in Loop: Header=BB0_58 Depth=1
	movaps	%xmm2, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm2, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	.LCPI0_6(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB0_68
# BB#67:                                #   in Loop: Header=BB0_58 Depth=1
	xorl	%r15d, %r15d
	jmp	.LBB0_71
.LBB0_68:                               #   in Loop: Header=BB0_58 Depth=1
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB0_70
# BB#69:                                # %call.sqrt
                                        #   in Loop: Header=BB0_58 Depth=1
	movq	%rsi, %rbx
	movaps	%xmm2, 64(%rsp)         # 16-byte Spill
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	movq	%rbx, %rsi
	movaps	%xmm0, %xmm1
.LBB0_70:                               # %.split
                                        #   in Loop: Header=BB0_58 Depth=1
	xorl	%r15d, %r15d
	movss	.LCPI0_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm3
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm3, %xmm0            # xmm0 = xmm3[0],xmm0[1,2,3]
	movlps	%xmm2, 16(%r12)
	movlps	%xmm0, 24(%r12)
.LBB0_71:                               # %_ZN11GIM_CONTACT19interpolate_normalsEP9btVector3i.exit.us
                                        #   in Loop: Header=BB0_58 Depth=1
.Ltmp18:
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_
.Ltmp19:
# BB#72:                                #   in Loop: Header=BB0_58 Depth=1
	movslq	4(%rbx), %rax
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	movq	16(%rbx), %rcx
	leaq	-48(%rcx,%rax), %r12
	movl	20(%rsp), %ecx
	movq	(%rsp), %rbp            # 8-byte Reload
.LBB0_78:                               #   in Loop: Header=BB0_58 Depth=1
	incq	%r13
	movslq	%ecx, %rax
	cmpq	%rax, %r13
	jl	.LBB0_58
	jmp	.LBB0_84
.LBB0_28:                               # %.lr.ph.split.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB0_29:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movl	%r14d, %edi
	movq	32(%rsp), %rdx
	movl	(%rdx,%rbx,8), %r14d
	movq	16(%rbp), %rax
	movslq	4(%rdx,%rbx,8), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$4, %rdx
	leaq	(%rax,%rdx), %rsi
	cmpl	%r14d, %edi
	jne	.LBB0_81
# BB#30:                                #   in Loop: Header=BB0_29 Depth=1
	movss	32(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	.LCPI0_5(%rip), %xmm0
	ucomiss	32(%rax,%rdx), %xmm0
	jbe	.LBB0_83
# BB#31:                                #   in Loop: Header=BB0_29 Depth=1
	movups	(%rsi), %xmm0
	movups	16(%rsi), %xmm1
	movups	32(%rsi), %xmm2
	movups	%xmm2, 32(%r12)
	movups	%xmm1, 16(%r12)
	movups	%xmm0, (%r12)
	jmp	.LBB0_83
	.p2align	4, 0x90
.LBB0_81:                               # %_ZN11GIM_CONTACT19interpolate_normalsEP9btVector3i.exit
                                        #   in Loop: Header=BB0_29 Depth=1
.Ltmp15:
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_
.Ltmp16:
# BB#82:                                #   in Loop: Header=BB0_29 Depth=1
	movslq	4(%rbp), %rax
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	movq	16(%rbp), %rcx
	leaq	-48(%rcx,%rax), %r12
	movl	20(%rsp), %ecx
	movq	(%rsp), %rbp            # 8-byte Reload
.LBB0_83:                               #   in Loop: Header=BB0_29 Depth=1
	incq	%rbx
	movslq	%ecx, %rax
	cmpq	%rax, %rbx
	jl	.LBB0_29
.LBB0_84:                               # %._crit_edge
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_88
# BB#85:
	cmpb	$0, 40(%rsp)
	je	.LBB0_87
# BB#86:
	callq	_Z21btAlignedFreeInternalPv
.LBB0_87:
	movq	$0, 32(%rsp)
.LBB0_88:
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_79:
.Ltmp14:
	jmp	.LBB0_90
.LBB0_55:
.Ltmp11:
	jmp	.LBB0_90
.LBB0_56:
.Ltmp8:
	jmp	.LBB0_90
.LBB0_80:                               # %.us-lcssa
.Ltmp17:
	jmp	.LBB0_90
.LBB0_89:                               # %.us-lcssa.us
.Ltmp20:
.LBB0_90:
	movq	%rax, %rbx
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_94
# BB#91:
	cmpb	$0, 40(%rsp)
	je	.LBB0_93
# BB#92:
.Ltmp21:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp22:
.LBB0_93:                               # %.noexc
	movq	$0, 32(%rsp)
.LBB0_94:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_95:
.Ltmp23:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN14btContactArray14merge_contactsERKS_b, .Lfunc_end0-_ZN14btContactArray14merge_contactsERKS_b
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp4           #   Call between .Ltmp4 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp21-.Ltmp16         #   Call between .Ltmp16 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin0   #     jumps to .Ltmp23
	.byte	1                       #   On action: 1
	.long	.Ltmp22-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Lfunc_end0-.Ltmp22     #   Call between .Ltmp22 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_,"axG",@progbits,_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_,comdat
	.weak	_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_
	.p2align	4, 0x90
	.type	_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_,@function
_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_: # @_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	4(%r15), %eax
	cmpl	8(%r15), %eax
	jne	.LBB1_13
# BB#1:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%ecx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB1_13
# BB#2:
	testl	%ebp, %ebp
	je	.LBB1_3
# BB#4:
	movslq	%ebp, %rax
	shlq	$4, %rax
	leaq	(%rax,%rax,2), %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	4(%r15), %eax
	testl	%eax, %eax
	jg	.LBB1_6
	jmp	.LBB1_8
.LBB1_3:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB1_8
.LBB1_6:                                # %.lr.ph.i.i
	cltq
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_7:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%rbx,%rcx)
	movups	16(%rdx,%rcx), %xmm0
	movups	%xmm0, 16(%rbx,%rcx)
	movl	32(%rdx,%rcx), %esi
	movl	%esi, 32(%rbx,%rcx)
	movl	40(%rdx,%rcx), %esi
	movl	%esi, 40(%rbx,%rcx)
	movl	44(%rdx,%rcx), %edx
	movl	%edx, 44(%rbx,%rcx)
	addq	$48, %rcx
	decq	%rax
	jne	.LBB1_7
.LBB1_8:                                # %_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4copyEiiPS0_.exit.i
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB1_12
# BB#9:
	cmpb	$0, 24(%r15)
	je	.LBB1_11
# BB#10:
	callq	_Z21btAlignedFreeInternalPv
.LBB1_11:
	movq	$0, 16(%r15)
.LBB1_12:                               # %_ZN20btAlignedObjectArrayI11GIM_CONTACTE10deallocateEv.exit.i
	movb	$1, 24(%r15)
	movq	%rbx, 16(%r15)
	movl	%ebp, 8(%r15)
	movl	4(%r15), %eax
.LBB1_13:                               # %_ZN20btAlignedObjectArrayI11GIM_CONTACTE7reserveEi.exit
	movq	16(%r15), %rcx
	cltq
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	movups	(%r14), %xmm0
	movups	%xmm0, (%rcx,%rax)
	movups	16(%r14), %xmm0
	movups	%xmm0, 16(%rcx,%rax)
	movl	32(%r14), %edx
	movl	%edx, 32(%rcx,%rax)
	movl	40(%r14), %edx
	movl	%edx, 40(%rcx,%rax)
	movl	44(%r14), %edx
	movl	%edx, 44(%rcx,%rax)
	incl	4(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_, .Lfunc_end1-_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN14btContactArray21merge_contacts_uniqueERKS_
	.p2align	4, 0x90
	.type	_ZN14btContactArray21merge_contacts_uniqueERKS_,@function
_ZN14btContactArray21merge_contacts_uniqueERKS_: # @_ZN14btContactArray21merge_contacts_uniqueERKS_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -24
.Lcfi26:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#1:
	cmpb	$0, 24(%rbx)
	je	.LBB3_3
# BB#2:
	callq	_Z21btAlignedFreeInternalPv
.LBB3_3:
	movq	$0, 16(%rbx)
.LBB3_4:                                # %_ZN20btAlignedObjectArrayI11GIM_CONTACTE5clearEv.exit
	movb	$1, 24(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 4(%rbx)
	cmpl	$1, 4(%r14)
	jne	.LBB3_5
# BB#6:
	movq	16(%r14), %rsi
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_ # TAILCALL
.LBB3_5:                                # %cdce.end
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	_ZN14btContactArray21merge_contacts_uniqueERKS_, .Lfunc_end3-_ZN14btContactArray21merge_contacts_uniqueERKS_
	.cfi_endproc

	.section	.text._ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvT_ii,"axG",@progbits,_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvT_ii,comdat
	.weak	_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvT_ii
	.p2align	4, 0x90
	.type	_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvT_ii,@function
_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvT_ii: # @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvT_ii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 48
.Lcfi32:
	.cfi_offset %rbx, -48
.Lcfi33:
	.cfi_offset %r12, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r12d
	movq	%rdi, %r15
	.p2align	4, 0x90
.LBB4_1:                                # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_2 Depth 2
                                        #       Child Loop BB4_3 Depth 3
                                        #       Child Loop BB4_5 Depth 3
	movl	%r12d, %esi
	movq	16(%r15), %r8
	leal	(%rsi,%r14), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	movl	(%r8,%rax,8), %r9d
	movl	%r14d, %edi
	jmp	.LBB4_2
	.p2align	4, 0x90
.LBB4_9:                                # %._crit_edge
                                        #   in Loop: Header=BB4_2 Depth=2
	movq	16(%r15), %r8
	movl	%edx, %edi
.LBB4_2:                                #   Parent Loop BB4_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_3 Depth 3
                                        #       Child Loop BB4_5 Depth 3
	movslq	%r12d, %rax
	leaq	(%r8,%rax,8), %rbp
	movl	%r12d, %ebx
	.p2align	4, 0x90
.LBB4_3:                                #   Parent Loop BB4_1 Depth=1
                                        #     Parent Loop BB4_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbp), %ecx
	addq	$8, %rbp
	incl	%ebx
	cmpl	%r9d, %ecx
	jb	.LBB4_3
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB4_2 Depth=2
	movslq	%edi, %rdx
	incq	%rdx
	.p2align	4, 0x90
.LBB4_5:                                #   Parent Loop BB4_1 Depth=1
                                        #     Parent Loop BB4_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rax
	leaq	-1(%rax), %rdx
	decl	%edi
	cmpl	-8(%r8,%rax,8), %r9d
	jb	.LBB4_5
# BB#6:                                 #   in Loop: Header=BB4_2 Depth=2
	leal	-1(%rbx), %r12d
	leal	1(%rdi), %edx
	cmpl	%edx, %r12d
	jg	.LBB4_8
# BB#7:                                 #   in Loop: Header=BB4_2 Depth=2
	movq	-8(%r8,%rax,8), %r8
	movl	-4(%rbp), %edx
	movq	%r8, -8(%rbp)
	movq	16(%r15), %rbp
	shlq	$32, %rdx
	orq	%rcx, %rdx
	movq	%rdx, -8(%rbp,%rax,8)
	movl	%ebx, %r12d
	movl	%edi, %edx
.LBB4_8:                                #   in Loop: Header=BB4_2 Depth=2
	cmpl	%edx, %r12d
	jle	.LBB4_9
# BB#10:                                #   in Loop: Header=BB4_1 Depth=1
	cmpl	%esi, %edx
	jle	.LBB4_12
# BB#11:                                #   in Loop: Header=BB4_1 Depth=1
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvT_ii
.LBB4_12:                               #   in Loop: Header=BB4_1 Depth=1
	cmpl	%r14d, %r12d
	jl	.LBB4_1
# BB#13:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvT_ii, .Lfunc_end4-_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvT_ii
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
