	.text
	.file	"smg.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	0                       # 0x0
.LCPI0_1:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	hypre_SMGCreate
	.p2align	4, 0x90
	.type	hypre_SMGCreate,@function
hypre_SMGCreate:                        # @hypre_SMGCreate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movl	$1, %edi
	movl	$232, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbx
	movl	%ebp, (%rbx)
	movl	$.L.str, %edi
	callq	hypre_InitializeTiming
	movl	%eax, 204(%rbx)
	movl	$0, 4(%rbx)
	movabsq	$4517329193108106637, %rax # imm = 0x3EB0C6F7A0B5ED8D
	movq	%rax, 8(%rbx)
	movl	$200, %eax
	movd	%eax, %xmm0
	movdqu	%xmm0, 16(%rbx)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [1,1,2,0]
	movups	%xmm0, 36(%rbx)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [0,0,1,1]
	movups	%xmm0, 52(%rbx)
	movl	$1, 68(%rbx)
	movl	$0, 208(%rbx)
	movl	$-1, 32(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_SMGCreate, .Lfunc_end0-hypre_SMGCreate
	.cfi_endproc

	.globl	hypre_SMGDestroy
	.p2align	4, 0x90
	.type	hypre_SMGDestroy,@function
hypre_SMGDestroy:                       # @hypre_SMGDestroy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -48
.Lcfi11:
	.cfi_offset %r12, -40
.Lcfi12:
	.cfi_offset %r13, -32
.Lcfi13:
	.cfi_offset %r14, -24
.Lcfi14:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB1_18
# BB#1:
	cmpl	$0, 208(%r14)
	jle	.LBB1_3
# BB#2:
	movq	216(%r14), %rdi
	callq	hypre_Free
	movq	$0, 216(%r14)
	movq	224(%r14), %rdi
	callq	hypre_Free
	movq	$0, 224(%r14)
.LBB1_3:
	movl	32(%r14), %ebx
	testl	%ebx, %ebx
	js	.LBB1_17
# BB#4:                                 # %.preheader
	movq	168(%r14), %rax
	movq	(%rax), %rdi
	callq	hypre_SMGRelaxDestroy
	cmpl	$2, %ebx
	jl	.LBB1_5
# BB#7:                                 # %.lr.ph91
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_8:                                # =>This Inner Loop Header: Depth=1
	movq	176(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	hypre_SMGResidualDestroy
	movq	184(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	hypre_SemiRestrictDestroy
	movq	192(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	hypre_SemiInterpDestroy
	movslq	32(%r14), %r15
	decq	%r15
	movq	168(%r14), %rax
	movq	8(%rax,%rbx,8), %rdi
	incq	%rbx
	callq	hypre_SMGRelaxDestroy
	cmpq	%r15, %rbx
	jl	.LBB1_8
# BB#9:                                 # %._crit_edge92
	testl	%ebx, %ebx
	je	.LBB1_6
# BB#10:                                # %._crit_edge92._crit_edge
	leaq	176(%r14), %r15
	jmp	.LBB1_11
.LBB1_5:
	xorl	%ebx, %ebx
.LBB1_6:                                # %._crit_edge92.thread
	leaq	176(%r14), %r15
	movq	176(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	hypre_SMGResidualDestroy
.LBB1_11:
	movq	168(%r14), %rdi
	callq	hypre_Free
	movq	$0, 168(%r14)
	movq	(%r15), %rdi
	callq	hypre_Free
	movq	$0, 176(%r14)
	movq	184(%r14), %rdi
	callq	hypre_Free
	movq	$0, 184(%r14)
	movq	192(%r14), %rdi
	callq	hypre_Free
	movq	$0, 192(%r14)
	movq	136(%r14), %rax
	movq	(%rax), %rdi
	callq	hypre_StructVectorDestroy
	movq	144(%r14), %rax
	movq	(%rax), %rdi
	callq	hypre_StructVectorDestroy
	movq	72(%r14), %rax
	movq	(%rax), %rdi
	callq	hypre_StructGridDestroy
	movq	96(%r14), %rax
	movq	(%rax), %rdi
	callq	hypre_StructMatrixDestroy
	movq	120(%r14), %rax
	movq	(%rax), %rdi
	callq	hypre_StructVectorDestroy
	movq	128(%r14), %rax
	movq	(%rax), %rdi
	callq	hypre_StructVectorDestroy
	cmpl	$2, 32(%r14)
	jl	.LBB1_16
# BB#12:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_13:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	72(%r14), %rax
	leaq	1(%rbx), %r15
	movq	8(%rax,%rbx,8), %rdi
	callq	hypre_StructGridDestroy
	movq	80(%r14), %rax
	movq	8(%rax,%rbx,8), %rdi
	callq	hypre_StructGridDestroy
	movq	96(%r14), %rax
	movq	8(%rax,%rbx,8), %rdi
	callq	hypre_StructMatrixDestroy
	movq	104(%r14), %rax
	movq	112(%r14), %rcx
	movq	(%rax,%rbx,8), %r12
	movq	(%rcx,%rbx,8), %r13
	movq	%r12, %rdi
	callq	hypre_StructMatrixDestroy
	cmpq	%r13, %r12
	je	.LBB1_15
# BB#14:                                #   in Loop: Header=BB1_13 Depth=1
	movq	112(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	hypre_StructMatrixDestroy
.LBB1_15:                               #   in Loop: Header=BB1_13 Depth=1
	movq	120(%r14), %rax
	movq	8(%rax,%rbx,8), %rdi
	callq	hypre_StructVectorDestroy
	movq	128(%r14), %rax
	movq	8(%rax,%rbx,8), %rdi
	callq	hypre_StructVectorDestroy
	movq	136(%r14), %rax
	movq	8(%rax,%rbx,8), %rdi
	callq	hypre_StructVectorDestroy
	movq	144(%r14), %rax
	movq	8(%rax,%rbx,8), %rdi
	callq	hypre_StructVectorDestroy
	movslq	32(%r14), %rax
	decq	%rax
	cmpq	%rax, %r15
	movq	%r15, %rbx
	jl	.LBB1_13
.LBB1_16:                               # %._crit_edge
	movq	88(%r14), %rdi
	callq	hypre_Free
	movq	$0, 88(%r14)
	movq	72(%r14), %rdi
	callq	hypre_Free
	movq	$0, 72(%r14)
	movq	80(%r14), %rdi
	callq	hypre_Free
	movq	$0, 80(%r14)
	movq	96(%r14), %rdi
	callq	hypre_Free
	movq	$0, 96(%r14)
	movq	104(%r14), %rdi
	callq	hypre_Free
	movq	$0, 104(%r14)
	movq	112(%r14), %rdi
	callq	hypre_Free
	movq	$0, 112(%r14)
	movq	120(%r14), %rdi
	callq	hypre_Free
	movq	$0, 120(%r14)
	movq	128(%r14), %rdi
	callq	hypre_Free
	movq	$0, 128(%r14)
	movq	136(%r14), %rdi
	callq	hypre_Free
	movq	$0, 136(%r14)
	movq	144(%r14), %rdi
	callq	hypre_Free
	movq	$0, 144(%r14)
.LBB1_17:
	movl	204(%r14), %edi
	callq	hypre_FinalizeTiming
	movq	%r14, %rdi
	callq	hypre_Free
.LBB1_18:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	hypre_SMGDestroy, .Lfunc_end1-hypre_SMGDestroy
	.cfi_endproc

	.globl	hypre_SMGSetMemoryUse
	.p2align	4, 0x90
	.type	hypre_SMGSetMemoryUse,@function
hypre_SMGSetMemoryUse:                  # @hypre_SMGSetMemoryUse
	.cfi_startproc
# BB#0:
	movl	%esi, 4(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	hypre_SMGSetMemoryUse, .Lfunc_end2-hypre_SMGSetMemoryUse
	.cfi_endproc

	.globl	hypre_SMGSetTol
	.p2align	4, 0x90
	.type	hypre_SMGSetTol,@function
hypre_SMGSetTol:                        # @hypre_SMGSetTol
	.cfi_startproc
# BB#0:
	movsd	%xmm0, 8(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	hypre_SMGSetTol, .Lfunc_end3-hypre_SMGSetTol
	.cfi_endproc

	.globl	hypre_SMGSetMaxIter
	.p2align	4, 0x90
	.type	hypre_SMGSetMaxIter,@function
hypre_SMGSetMaxIter:                    # @hypre_SMGSetMaxIter
	.cfi_startproc
# BB#0:
	movl	%esi, 16(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end4:
	.size	hypre_SMGSetMaxIter, .Lfunc_end4-hypre_SMGSetMaxIter
	.cfi_endproc

	.globl	hypre_SMGSetRelChange
	.p2align	4, 0x90
	.type	hypre_SMGSetRelChange,@function
hypre_SMGSetRelChange:                  # @hypre_SMGSetRelChange
	.cfi_startproc
# BB#0:
	movl	%esi, 20(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	hypre_SMGSetRelChange, .Lfunc_end5-hypre_SMGSetRelChange
	.cfi_endproc

	.globl	hypre_SMGSetZeroGuess
	.p2align	4, 0x90
	.type	hypre_SMGSetZeroGuess,@function
hypre_SMGSetZeroGuess:                  # @hypre_SMGSetZeroGuess
	.cfi_startproc
# BB#0:
	movl	%esi, 24(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end6:
	.size	hypre_SMGSetZeroGuess, .Lfunc_end6-hypre_SMGSetZeroGuess
	.cfi_endproc

	.globl	hypre_SMGSetNumPreRelax
	.p2align	4, 0x90
	.type	hypre_SMGSetNumPreRelax,@function
hypre_SMGSetNumPreRelax:                # @hypre_SMGSetNumPreRelax
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	movl	$1, %eax
	cmovgl	%esi, %eax
	movl	%eax, 36(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end7:
	.size	hypre_SMGSetNumPreRelax, .Lfunc_end7-hypre_SMGSetNumPreRelax
	.cfi_endproc

	.globl	hypre_SMGSetNumPostRelax
	.p2align	4, 0x90
	.type	hypre_SMGSetNumPostRelax,@function
hypre_SMGSetNumPostRelax:               # @hypre_SMGSetNumPostRelax
	.cfi_startproc
# BB#0:
	movl	%esi, 40(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end8:
	.size	hypre_SMGSetNumPostRelax, .Lfunc_end8-hypre_SMGSetNumPostRelax
	.cfi_endproc

	.globl	hypre_SMGSetBase
	.p2align	4, 0x90
	.type	hypre_SMGSetBase,@function
hypre_SMGSetBase:                       # @hypre_SMGSetBase
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	movl	%eax, 48(%rdi)
	movl	(%rdx), %eax
	movl	%eax, 60(%rdi)
	movl	4(%rsi), %eax
	movl	%eax, 52(%rdi)
	movl	4(%rdx), %eax
	movl	%eax, 64(%rdi)
	movl	8(%rsi), %eax
	movl	%eax, 56(%rdi)
	movl	8(%rdx), %eax
	movl	%eax, 68(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end9:
	.size	hypre_SMGSetBase, .Lfunc_end9-hypre_SMGSetBase
	.cfi_endproc

	.globl	hypre_SMGSetLogging
	.p2align	4, 0x90
	.type	hypre_SMGSetLogging,@function
hypre_SMGSetLogging:                    # @hypre_SMGSetLogging
	.cfi_startproc
# BB#0:
	movl	%esi, 208(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	hypre_SMGSetLogging, .Lfunc_end10-hypre_SMGSetLogging
	.cfi_endproc

	.globl	hypre_SMGGetNumIterations
	.p2align	4, 0x90
	.type	hypre_SMGGetNumIterations,@function
hypre_SMGGetNumIterations:              # @hypre_SMGGetNumIterations
	.cfi_startproc
# BB#0:
	movl	200(%rdi), %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end11:
	.size	hypre_SMGGetNumIterations, .Lfunc_end11-hypre_SMGGetNumIterations
	.cfi_endproc

	.globl	hypre_SMGPrintLogging
	.p2align	4, 0x90
	.type	hypre_SMGPrintLogging,@function
hypre_SMGPrintLogging:                  # @hypre_SMGPrintLogging
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 48
.Lcfi20:
	.cfi_offset %rbx, -40
.Lcfi21:
	.cfi_offset %r12, -32
.Lcfi22:
	.cfi_offset %r14, -24
.Lcfi23:
	.cfi_offset %r15, -16
	testl	%esi, %esi
	jne	.LBB12_5
# BB#1:
	movl	208(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB12_5
# BB#2:
	movl	200(%rdi), %r14d
	testl	%r14d, %r14d
	jle	.LBB12_5
# BB#3:                                 # %.lr.ph.preheader
	movq	216(%rdi), %r15
	movq	224(%rdi), %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r15,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.1, %edi
	movb	$1, %al
	movl	%ebx, %esi
	callq	printf
	movsd	(%r12,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.2, %edi
	movb	$1, %al
	movl	%ebx, %esi
	callq	printf
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB12_4
.LBB12_5:                               # %.loopexit
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	hypre_SMGPrintLogging, .Lfunc_end12-hypre_SMGPrintLogging
	.cfi_endproc

	.globl	hypre_SMGGetFinalRelativeResidualNorm
	.p2align	4, 0x90
	.type	hypre_SMGGetFinalRelativeResidualNorm,@function
hypre_SMGGetFinalRelativeResidualNorm:  # @hypre_SMGGetFinalRelativeResidualNorm
	.cfi_startproc
# BB#0:
	cmpl	$0, 208(%rdi)
	jle	.LBB13_1
# BB#2:
	movl	200(%rdi), %eax
	movq	224(%rdi), %rcx
	xorl	%edx, %edx
	cmpl	16(%rdi), %eax
	sete	%dl
	subl	%edx, %eax
	cltq
	movq	(%rcx,%rax,8), %rax
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	retq
.LBB13_1:
	movl	$-1, %eax
	retq
.Lfunc_end13:
	.size	hypre_SMGGetFinalRelativeResidualNorm, .Lfunc_end13-hypre_SMGGetFinalRelativeResidualNorm
	.cfi_endproc

	.globl	hypre_SMGSetStructVectorConstantValues
	.p2align	4, 0x90
	.type	hypre_SMGSetStructVectorConstantValues,@function
hypre_SMGSetStructVectorConstantValues: # @hypre_SMGSetStructVectorConstantValues
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 240
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	cmpl	$0, 8(%rsi)
	jle	.LBB14_19
# BB#1:                                 # %.lr.ph264
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	xorl	%edx, %edx
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movaps	%xmm0, 160(%rsp)        # 16-byte Spill
	movaps	%xmm1, 144(%rsp)        # 16-byte Spill
	.p2align	4, 0x90
.LBB14_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_7 Depth 2
                                        #       Child Loop BB14_8 Depth 3
                                        #         Child Loop BB14_25 Depth 4
                                        #         Child Loop BB14_28 Depth 4
                                        #         Child Loop BB14_12 Depth 4
                                        #         Child Loop BB14_15 Depth 4
	movq	(%rsi), %r15
	leaq	(,%rdx,8), %rax
	leaq	(%rax,%rax,2), %rbx
	leaq	(%r15,%rbx), %r14
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rax
	movq	24(%rcx), %r13
	movq	(%rax), %r12
	movq	40(%rcx), %rax
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movslq	(%rax,%rdx,4), %rbp
	movq	%r14, %rdi
	movq	48(%rsp), %rsi          # 8-byte Reload
	leaq	68(%rsp), %rdx
	callq	hypre_BoxGetStrideSize
	movaps	144(%rsp), %xmm1        # 16-byte Reload
	movapd	160(%rsp), %xmm0        # 16-byte Reload
	movl	(%r12,%rbx), %edx
	movl	4(%r12,%rbx), %r10d
	movl	12(%r12,%rbx), %eax
	movl	%eax, %r8d
	subl	%edx, %r8d
	incl	%r8d
	cmpl	%edx, %eax
	movl	16(%r12,%rbx), %eax
	movl	$0, %ecx
	cmovsl	%ecx, %r8d
	movl	%eax, %r9d
	subl	%r10d, %r9d
	incl	%r9d
	cmpl	%r10d, %eax
	cmovsl	%ecx, %r9d
	movl	68(%rsp), %eax
	movl	72(%rsp), %esi
	movl	76(%rsp), %edi
	cmpl	%eax, %esi
	movl	%eax, %ecx
	movl	%esi, 12(%rsp)          # 4-byte Spill
	cmovgel	%esi, %ecx
	cmpl	%ecx, %edi
	movl	%edi, 8(%rsp)           # 4-byte Spill
	cmovgel	%edi, %ecx
	testl	%ecx, %ecx
	jle	.LBB14_18
# BB#3:                                 # %.lr.ph226
                                        #   in Loop: Header=BB14_2 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB14_18
# BB#4:                                 # %.lr.ph226.split.us.preheader
                                        #   in Loop: Header=BB14_2 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB14_18
# BB#5:                                 # %.lr.ph226.split.us.preheader
                                        #   in Loop: Header=BB14_2 Depth=1
	testl	%eax, %eax
	jle	.LBB14_18
# BB#6:                                 # %.preheader202.us.us.us.preheader
                                        #   in Loop: Header=BB14_2 Depth=1
	leaq	(%r13,%rbp,8), %rsi
	movq	48(%rsp), %rcx          # 8-byte Reload
	movslq	(%rcx), %rdi
	movl	8(%rcx), %r11d
	movl	4(%rcx), %ecx
	imull	%r8d, %ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	%r8d, %ecx
	imull	%r9d, %ecx
	imull	%r11d, %ecx
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	movl	(%r14), %ecx
	subl	%edx, %ecx
	movl	4(%r15,%rbx), %edx
	movl	8(%r15,%rbx), %ebp
	subl	%r10d, %edx
	subl	8(%r12,%rbx), %ebp
	imull	%r9d, %ebp
	addl	%edx, %ebp
	imull	%r8d, %ebp
	addl	%ecx, %ebp
	leal	-1(%rax), %ecx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	leaq	1(%rcx), %rdx
	imull	%r8d, %r11d
	imull	%r9d, %r11d
	movl	%r11d, 32(%rsp)         # 4-byte Spill
	movq	%rdx, %rbx
	movabsq	$8589934588, %rcx       # imm = 0x1FFFFFFFC
	andq	%rcx, %rbx
	leaq	-4(%rbx), %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	shrl	$2, %ecx
	incl	%ecx
	movq	%rdi, %r8
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	imulq	%rbx, %r8
	movq	%r8, 104(%rsp)          # 8-byte Spill
	andl	$3, %ecx
	movq	%rdi, %r9
	shlq	$5, %r9
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	negq	%rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	%rdi, %r10
	shlq	$7, %r10
	movq	%rdi, %r11
	shlq	$6, %r11
	leaq	(,%rdi,8), %r12
	xorl	%ecx, %ecx
	movl	%ebp, 24(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB14_7:                               # %.preheader202.us.us.us
                                        #   Parent Loop BB14_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_8 Depth 3
                                        #         Child Loop BB14_25 Depth 4
                                        #         Child Loop BB14_28 Depth 4
                                        #         Child Loop BB14_12 Depth 4
                                        #         Child Loop BB14_15 Depth 4
	movl	32(%rsp), %ebx          # 4-byte Reload
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	imull	%ecx, %ebx
	addl	24(%rsp), %ebx          # 4-byte Folded Reload
	movl	%ebx, 44(%rsp)          # 4-byte Spill
	movl	%ebp, 36(%rsp)          # 4-byte Spill
	movl	%ebp, %ecx
	xorl	%r13d, %r13d
	jmp	.LBB14_8
.LBB14_23:                              #   in Loop: Header=BB14_8 Depth=3
	xorl	%ecx, %ecx
.LBB14_26:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB14_8 Depth=3
	cmpq	$12, 128(%rsp)          # 8-byte Folded Reload
	jb	.LBB14_29
# BB#27:                                # %vector.ph.new
                                        #   in Loop: Header=BB14_8 Depth=3
	movq	16(%rsp), %rbx          # 8-byte Reload
	subq	%rcx, %rbx
	imulq	%rdi, %rcx
	addq	%r14, %rcx
	leaq	(%rsi,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB14_28:                              # %vector.body
                                        #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_7 Depth=2
                                        #       Parent Loop BB14_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	%xmm1, (%rcx)
	movups	%xmm1, 16(%rcx)
	leaq	(%rcx,%r9), %rbp
	movups	%xmm1, (%rcx,%r9)
	movups	%xmm1, 16(%rcx,%r9)
	movups	%xmm1, (%r9,%rbp)
	movups	%xmm1, 16(%r9,%rbp)
	leaq	(%rbp,%r9), %rbp
	movups	%xmm1, (%r9,%rbp)
	movups	%xmm1, 16(%r9,%rbp)
	addq	%r10, %rcx
	addq	$-16, %rbx
	jne	.LBB14_28
.LBB14_29:                              # %middle.block
                                        #   in Loop: Header=BB14_8 Depth=3
	cmpq	16(%rsp), %rdx          # 8-byte Folded Reload
	je	.LBB14_16
# BB#30:                                #   in Loop: Header=BB14_8 Depth=3
	movq	56(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	imull	%r13d, %ecx
	addl	44(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %r8
	addq	104(%rsp), %r8          # 8-byte Folded Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %r15d
	jmp	.LBB14_10
	.p2align	4, 0x90
.LBB14_8:                               # %.preheader.us.us.us.us
                                        #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB14_25 Depth 4
                                        #         Child Loop BB14_28 Depth 4
                                        #         Child Loop BB14_12 Depth 4
                                        #         Child Loop BB14_15 Depth 4
	movslq	%ecx, %r14
	xorl	%r15d, %r15d
	cmpq	$3, %rdx
	jbe	.LBB14_9
# BB#20:                                # %min.iters.checked
                                        #   in Loop: Header=BB14_8 Depth=3
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB14_9
# BB#21:                                # %vector.scevcheck
                                        #   in Loop: Header=BB14_8 Depth=3
	cmpl	$1, %edi
	jne	.LBB14_9
# BB#22:                                # %vector.ph
                                        #   in Loop: Header=BB14_8 Depth=3
	cmpq	$0, 120(%rsp)           # 8-byte Folded Reload
	je	.LBB14_23
# BB#24:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB14_8 Depth=3
	leaq	16(%rsi,%r14,8), %rbx
	movq	112(%rsp), %rbp         # 8-byte Reload
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB14_25:                              # %vector.body.prol
                                        #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_7 Depth=2
                                        #       Parent Loop BB14_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	%xmm1, -16(%rbx)
	movups	%xmm1, (%rbx)
	addq	$4, %rcx
	addq	%r9, %rbx
	incq	%rbp
	jne	.LBB14_25
	jmp	.LBB14_26
	.p2align	4, 0x90
.LBB14_9:                               #   in Loop: Header=BB14_8 Depth=3
	movq	%r14, %r8
.LBB14_10:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB14_8 Depth=3
	movl	%eax, %ebx
	subl	%r15d, %ebx
	movq	136(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%r15d, %ecx
	andl	$7, %ebx
	je	.LBB14_13
# BB#11:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB14_8 Depth=3
	negl	%ebx
	.p2align	4, 0x90
.LBB14_12:                              # %scalar.ph.prol
                                        #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_7 Depth=2
                                        #       Parent Loop BB14_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	%xmm0, (%rsi,%r8,8)
	addq	%rdi, %r8
	incl	%r15d
	incl	%ebx
	jne	.LBB14_12
.LBB14_13:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB14_8 Depth=3
	cmpl	$7, %ecx
	jb	.LBB14_16
# BB#14:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB14_8 Depth=3
	leaq	(%rsi,%r8,8), %rcx
	movl	%eax, %ebx
	subl	%r15d, %ebx
	.p2align	4, 0x90
.LBB14_15:                              # %scalar.ph
                                        #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_7 Depth=2
                                        #       Parent Loop BB14_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	%xmm0, (%rcx)
	leaq	(%rcx,%r12), %rbp
	movsd	%xmm0, (%rcx,%rdi,8)
	movsd	%xmm0, (%rbp,%rdi,8)
	addq	%r12, %rbp
	movsd	%xmm0, (%rbp,%rdi,8)
	addq	%r12, %rbp
	movsd	%xmm0, (%rbp,%rdi,8)
	addq	%r12, %rbp
	movsd	%xmm0, (%rbp,%rdi,8)
	addq	%r12, %rbp
	movsd	%xmm0, (%rbp,%rdi,8)
	addq	%r12, %rbp
	movsd	%xmm0, (%rbp,%rdi,8)
	addq	%r11, %rcx
	addl	$-8, %ebx
	jne	.LBB14_15
.LBB14_16:                              # %._crit_edge.us.us.us.us
                                        #   in Loop: Header=BB14_8 Depth=3
	movq	56(%rsp), %rcx          # 8-byte Reload
	leal	(%r14,%rcx), %ecx
	incl	%r13d
	cmpl	12(%rsp), %r13d         # 4-byte Folded Reload
	jne	.LBB14_8
# BB#17:                                # %._crit_edge207.us-lcssa.us.us.us.us
                                        #   in Loop: Header=BB14_7 Depth=2
	movl	40(%rsp), %ecx          # 4-byte Reload
	incl	%ecx
	movl	36(%rsp), %ebp          # 4-byte Reload
	addl	28(%rsp), %ebp          # 4-byte Folded Reload
	cmpl	8(%rsp), %ecx           # 4-byte Folded Reload
	jne	.LBB14_7
	.p2align	4, 0x90
.LBB14_18:                              # %._crit_edge227
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	96(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	movq	80(%rsp), %rsi          # 8-byte Reload
	movslq	8(%rsi), %rax
	cmpq	%rax, %rdx
	jl	.LBB14_2
.LBB14_19:                              # %._crit_edge265
	xorl	%eax, %eax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	hypre_SMGSetStructVectorConstantValues, .Lfunc_end14-hypre_SMGSetStructVectorConstantValues
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"SMG"
	.size	.L.str, 4

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Residual norm[%d] = %e   "
	.size	.L.str.1, 26

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Relative residual norm[%d] = %e\n"
	.size	.L.str.2, 33


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
