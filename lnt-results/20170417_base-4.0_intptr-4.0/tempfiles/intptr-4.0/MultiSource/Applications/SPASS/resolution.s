	.text
	.file	"resolution.bc"
	.globl	res_InsertClauseIndex
	.p2align	4, 0x90
	.type	res_InsertClauseIndex,@function
res_InsertClauseIndex:                  # @res_InsertClauseIndex
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	68(%r15), %eax
	addl	64(%r15), %eax
	addl	72(%r15), %eax
	jle	.LBB0_3
# BB#1:                                 # %.lr.ph
	movl	%eax, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %rsi
	movq	24(%rsi), %rdx
	movq	cont_LEFTCONTEXT(%rip), %rcx
	movq	%r14, %rdi
	callq	st_EntryCreate
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB0_2
.LBB0_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	res_InsertClauseIndex, .Lfunc_end0-res_InsertClauseIndex
	.cfi_endproc

	.globl	res_DeleteClauseIndex
	.p2align	4, 0x90
	.type	res_DeleteClauseIndex,@function
res_DeleteClauseIndex:                  # @res_DeleteClauseIndex
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r12, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	68(%r15), %rax
	movslq	72(%r15), %rcx
	movslq	64(%r15), %r12
	addq	%rax, %r12
	addq	%rcx, %r12
	testl	%r12d, %r12d
	jle	.LBB1_5
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %rsi
	movq	24(%rsi), %rdx
	movq	cont_LEFTCONTEXT(%rip), %rcx
	movq	%r14, %rdi
	callq	st_EntryDelete
	testl	%eax, %eax
	je	.LBB1_4
# BB#2:                                 #   in Loop: Header=BB1_3 Depth=1
	incq	%rbx
	cmpq	%r12, %rbx
	jl	.LBB1_3
.LBB1_5:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB1_4:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	callq	abort
.Lfunc_end1:
	.size	res_DeleteClauseIndex, .Lfunc_end1-res_DeleteClauseIndex
	.cfi_endproc

	.globl	res_SelectLightestClause
	.p2align	4, 0x90
	.type	res_SelectLightestClause,@function
res_SelectLightestClause:               # @res_SelectLightestClause
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rdx
	movl	4(%rdx), %esi
.LBB2_1:                                # %.sink.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_2 Depth 2
	movq	%rdx, %rax
	movl	%esi, %ecx
	.p2align	4, 0x90
.LBB2_2:                                #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=2
	movq	8(%rdi), %rdx
	movl	4(%rdx), %esi
	cmpl	%ecx, %esi
	jae	.LBB2_2
	jmp	.LBB2_1
.LBB2_4:
	retq
.Lfunc_end2:
	.size	res_SelectLightestClause, .Lfunc_end2-res_SelectLightestClause
	.cfi_endproc

	.globl	res_HasTautology
	.p2align	4, 0x90
	.type	res_HasTautology,@function
res_HasTautology:                       # @res_HasTautology
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 64
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movslq	68(%r14), %rax
	movslq	72(%r14), %rcx
	movslq	64(%r14), %r12
	addq	%rax, %r12
	addq	%rcx, %r12
	testl	%r12d, %r12d
	jle	.LBB3_1
# BB#2:                                 # %.lr.ph40.split.us.preheader
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph40.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_4 Depth 2
	movq	56(%r14), %rax
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %rdi
	callq	fol_ComplementaryTerm
	movq	%rax, %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_4:                                #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%ebp, %ebp
	cmpq	%rbx, %r13
	je	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=2
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rsi
	movq	%r15, %rdi
	callq	term_Equal
	xorl	%ebp, %ebp
	testl	%eax, %eax
	setne	%bpl
.LBB3_6:                                #   in Loop: Header=BB3_4 Depth=2
	incq	%rbx
	cmpq	%r12, %rbx
	jge	.LBB3_8
# BB#7:                                 #   in Loop: Header=BB3_4 Depth=2
	testl	%ebp, %ebp
	je	.LBB3_4
.LBB3_8:                                # %..critedge1_crit_edge.us
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	%r15, %rdi
	callq	term_Delete
	incq	%r13
	cmpq	%r12, %r13
	jge	.LBB3_10
# BB#9:                                 # %..critedge1_crit_edge.us
                                        #   in Loop: Header=BB3_3 Depth=1
	testl	%ebp, %ebp
	je	.LBB3_3
	jmp	.LBB3_10
.LBB3_1:
	xorl	%ebp, %ebp
.LBB3_10:                               # %.critedge
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	res_HasTautology, .Lfunc_end3-res_HasTautology
	.cfi_endproc

	.globl	res_BackSubWithLength
	.p2align	4, 0x90
	.type	res_BackSubWithLength,@function
res_BackSubWithLength:                  # @res_BackSubWithLength
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 64
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	68(%r15), %rax
	movslq	72(%r15), %rcx
	movslq	64(%r15), %r12
	addq	%rax, %r12
	addq	%rcx, %r12
	testl	%r12d, %r12d
	jle	.LBB4_13
# BB#1:                                 # %.lr.ph62
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_4 Depth 2
                                        #     Child Loop BB4_11 Depth 2
	movq	56(%r15), %rax
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %rdx
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r14, %rsi
	callq	st_GetGen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB4_12
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB4_4:                                # %.lr.ph
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rax
	movq	16(%rax), %rdi
	movq	56(%rdi), %rcx
	cmpq	(%rcx), %rax
	jne	.LBB4_10
# BB#5:                                 #   in Loop: Header=BB4_4 Depth=2
	movl	68(%r15), %eax
	addl	64(%r15), %eax
	addl	72(%r15), %eax
	movl	68(%rdi), %ecx
	addl	64(%rdi), %ecx
	addl	72(%rdi), %ecx
	cmpl	%ecx, %eax
	jl	.LBB4_10
# BB#6:                                 #   in Loop: Header=BB4_4 Depth=2
	movl	4(%r15), %eax
	cmpl	4(%rdi), %eax
	jb	.LBB4_10
# BB#7:                                 #   in Loop: Header=BB4_4 Depth=2
	movq	%r15, %rsi
	callq	subs_Idc
	testl	%eax, %eax
	jne	.LBB4_8
	.p2align	4, 0x90
.LBB4_10:                               #   in Loop: Header=BB4_4 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB4_4
	.p2align	4, 0x90
.LBB4_11:                               # %.lr.ph.i
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB4_11
.LBB4_12:                               # %list_Delete.exit
                                        #   in Loop: Header=BB4_2 Depth=1
	incq	%r13
	cmpq	%r12, %r13
	jl	.LBB4_2
.LBB4_13:
	xorl	%eax, %eax
	jmp	.LBB4_14
	.p2align	4, 0x90
.LBB4_8:                                # %.lr.ph.i55
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB4_8
# BB#9:
	movl	$1, %eax
.LBB4_14:                               # %list_Delete.exit56
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	res_BackSubWithLength, .Lfunc_end4-res_BackSubWithLength
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n\n"
	.size	.L.str, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
