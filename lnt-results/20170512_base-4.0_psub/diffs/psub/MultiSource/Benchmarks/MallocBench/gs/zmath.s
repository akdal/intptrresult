	.text
	.file	"zmath.bc"
	.globl	zmath_init
	.p2align	4, 0x90
	.type	zmath_init,@function
zmath_init:                             # @zmath_init
	.cfi_startproc
# BB#0:
	movq	$1, rand_state(%rip)
	retq
.Lfunc_end0:
	.size	zmath_init, .Lfunc_end0-zmath_init
	.cfi_endproc

	.globl	zsqrt
	.p2align	4, 0x90
	.type	zsqrt,@function
zsqrt:                                  # @zsqrt
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	12(%rsp), %rdx
	movl	$1, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB1_5
# BB#1:
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movl	$-15, %eax
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	ja	.LBB1_5
# BB#2:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB1_4
# BB#3:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB1_4:                                # %.split
	movss	%xmm1, (%rbx)
	movw	$44, 8(%rbx)
	xorl	%eax, %eax
.LBB1_5:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end1:
	.size	zsqrt, .Lfunc_end1-zsqrt
	.cfi_endproc

	.globl	zarccos
	.p2align	4, 0x90
	.type	zarccos,@function
zarccos:                                # @zarccos
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	12(%rsp), %rdx
	movl	$1, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB2_2
# BB#1:
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	callq	acos
	mulsd	radians_to_degrees(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rbx)
	movw	$44, 8(%rbx)
	xorl	%eax, %eax
.LBB2_2:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end2:
	.size	zarccos, .Lfunc_end2-zarccos
	.cfi_endproc

	.globl	zarcsin
	.p2align	4, 0x90
	.type	zarcsin,@function
zarcsin:                                # @zarcsin
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	12(%rsp), %rdx
	movl	$1, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB3_2
# BB#1:
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	callq	asin
	mulsd	radians_to_degrees(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rbx)
	movw	$44, 8(%rbx)
	xorl	%eax, %eax
.LBB3_2:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end3:
	.size	zarcsin, .Lfunc_end3-zarcsin
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1127481344              # float 180
.LCPI4_1:
	.long	3274964992              # float -180
.LCPI4_2:
	.long	0                       # float 0
	.text
	.globl	zatan
	.p2align	4, 0x90
	.type	zatan,@function
zatan:                                  # @zatan
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	8(%rsp), %rdx
	movl	$2, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB4_13
# BB#1:
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm0
	jne	.LBB4_4
	jp	.LBB4_4
# BB#2:
	movss	12(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movl	$-23, %eax
	ucomiss	%xmm2, %xmm1
	jne	.LBB4_3
	jnp	.LBB4_13
.LBB4_3:
	xorps	%xmm0, %xmm0
	cmpltss	%xmm0, %xmm1
	movss	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	andps	%xmm1, %xmm0
	jmp	.LBB4_12
.LBB4_4:
	cvtss2sd	%xmm0, %xmm0
	movss	12(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	callq	atan2
	mulsd	radians_to_degrees(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jbe	.LBB4_7
# BB#5:
	movss	.LCPI4_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB4_6:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	addss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm2
	ja	.LBB4_6
	jmp	.LBB4_10
.LBB4_7:                                # %.preheader16
	ucomiss	.LCPI4_0(%rip), %xmm0
	jb	.LBB4_10
# BB#8:
	movss	.LCPI4_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI4_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB4_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	addss	%xmm1, %xmm0
	ucomiss	%xmm2, %xmm0
	jae	.LBB4_9
.LBB4_10:                               # %.loopexit
	xorps	%xmm1, %xmm1
	ucomiss	8(%rsp), %xmm1
	jbe	.LBB4_12
# BB#11:
	addss	.LCPI4_0(%rip), %xmm0
.LBB4_12:
	movss	%xmm0, -16(%rbx)
	movw	$44, -8(%rbx)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB4_13:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end4:
	.size	zatan, .Lfunc_end4-zatan
	.cfi_endproc

	.globl	zcos
	.p2align	4, 0x90
	.type	zcos,@function
zcos:                                   # @zcos
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	12(%rsp), %rdx
	movl	$1, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB5_2
# BB#1:
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	mulsd	degrees_to_radians(%rip), %xmm0
	callq	cos
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rbx)
	movw	$44, 8(%rbx)
	xorl	%eax, %eax
.LBB5_2:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end5:
	.size	zcos, .Lfunc_end5-zcos
	.cfi_endproc

	.globl	zsin
	.p2align	4, 0x90
	.type	zsin,@function
zsin:                                   # @zsin
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	12(%rsp), %rdx
	movl	$1, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB6_2
# BB#1:
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	mulsd	degrees_to_radians(%rip), %xmm0
	callq	sin
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rbx)
	movw	$44, 8(%rbx)
	xorl	%eax, %eax
.LBB6_2:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end6:
	.size	zsin, .Lfunc_end6-zsin
	.cfi_endproc

	.globl	zexp
	.p2align	4, 0x90
	.type	zexp,@function
zexp:                                   # @zexp
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	%rsp, %rdx
	movl	$2, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB7_7
# BB#1:
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB7_3
	jp	.LBB7_3
# BB#2:
	movss	4(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movl	$-23, %eax
	ucomiss	%xmm1, %xmm2
	jne	.LBB7_3
	jnp	.LBB7_7
.LBB7_3:                                # %thread-pre-split
	ucomiss	%xmm0, %xmm1
	jbe	.LBB7_6
# BB#4:
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	leaq	8(%rsp), %rdi
	callq	modf
	movl	$-23, %eax
	xorps	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jne	.LBB7_7
	jp	.LBB7_7
# BB#5:                                 # %._crit_edge
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
.LBB7_6:                                # %._crit_edge9
	cvtss2sd	%xmm0, %xmm0
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	callq	pow
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, -16(%rbx)
	movw	$44, -8(%rbx)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB7_7:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end7:
	.size	zexp, .Lfunc_end7-zexp
	.cfi_endproc

	.globl	zln
	.p2align	4, 0x90
	.type	zln,@function
zln:                                    # @zln
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	12(%rsp), %rdx
	movl	$1, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB8_3
# BB#1:
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movl	$-15, %eax
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jae	.LBB8_3
# BB#2:
	cvtss2sd	%xmm0, %xmm0
	callq	log
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rbx)
	movw	$44, 8(%rbx)
	xorl	%eax, %eax
.LBB8_3:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end8:
	.size	zln, .Lfunc_end8-zln
	.cfi_endproc

	.globl	zlog
	.p2align	4, 0x90
	.type	zlog,@function
zlog:                                   # @zlog
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	12(%rsp), %rdx
	movl	$1, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB9_3
# BB#1:
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movl	$-15, %eax
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jae	.LBB9_3
# BB#2:
	cvtss2sd	%xmm0, %xmm0
	callq	log10
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rbx)
	movw	$44, 8(%rbx)
	xorl	%eax, %eax
.LBB9_3:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end9:
	.size	zlog, .Lfunc_end9-zlog
	.cfi_endproc

	.globl	zrand
	.p2align	4, 0x90
	.type	zrand,@function
zrand:                                  # @zrand
	.cfi_startproc
# BB#0:
	imulq	$1103515245, rand_state(%rip), %rdx # imm = 0x41C64E6D
	addq	$12345, %rdx            # imm = 0x3039
	imulq	$1103515245, %rdx, %rcx # imm = 0x41C64E6D
	addq	$12345, %rcx            # imm = 0x3039
	imulq	$1103515245, %rcx, %rax # imm = 0x41C64E6D
	addq	$12345, %rax            # imm = 0x3039
	movq	%rax, rand_state(%rip)
	leaq	16(%rdi), %rsi
	movq	%rsi, osp(%rip)
	cmpq	ostop(%rip), %rsi
	jbe	.LBB10_2
# BB#1:
	movq	%rdi, osp(%rip)
	movl	$-16, %eax
	retq
.LBB10_2:
	shll	$21, %edx
	shll	$10, %ecx
	addl	%edx, %ecx
	shrq	$3, %rax
	addl	%ecx, %eax
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	movq	%rax, 16(%rdi)
	movw	$20, 24(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	zrand, .Lfunc_end10-zrand
	.cfi_endproc

	.globl	zsrand
	.p2align	4, 0x90
	.type	zsrand,@function
zsrand:                                 # @zsrand
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$20, %ecx
	jne	.LBB11_2
# BB#1:
	movq	(%rdi), %rax
	movq	%rax, rand_state(%rip)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB11_2:
	retq
.Lfunc_end11:
	.size	zsrand, .Lfunc_end11-zsrand
	.cfi_endproc

	.globl	zrrand
	.p2align	4, 0x90
	.type	zrrand,@function
zrrand:                                 # @zrrand
	.cfi_startproc
# BB#0:
	leaq	16(%rdi), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB12_2
# BB#1:
	movq	%rdi, osp(%rip)
	movl	$-16, %eax
	retq
.LBB12_2:
	movq	rand_state(%rip), %rax
	movq	%rax, 16(%rdi)
	movw	$20, 24(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end12:
	.size	zrrand, .Lfunc_end12-zrrand
	.cfi_endproc

	.globl	zmath_op_init
	.p2align	4, 0x90
	.type	zmath_op_init,@function
zmath_op_init:                          # @zmath_op_init
	.cfi_startproc
# BB#0:
	movl	$zmath_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end13:
	.size	zmath_op_init, .Lfunc_end13-zmath_op_init
	.cfi_endproc

	.type	degrees_to_radians,@object # @degrees_to_radians
	.data
	.globl	degrees_to_radians
	.p2align	3
degrees_to_radians:
	.quad	4580687790476533049     # double 0.017453292519943295
	.size	degrees_to_radians, 8

	.type	radians_to_degrees,@object # @radians_to_degrees
	.globl	radians_to_degrees
	.p2align	3
radians_to_degrees:
	.quad	4633260481411531256     # double 57.295779513082323
	.size	radians_to_degrees, 8

	.type	rand_state,@object      # @rand_state
	.comm	rand_state,8,8
	.type	zmath_op_init.my_defs,@object # @zmath_op_init.my_defs
	.p2align	4
zmath_op_init.my_defs:
	.quad	.L.str
	.quad	zarccos
	.quad	.L.str.1
	.quad	zarcsin
	.quad	.L.str.2
	.quad	zatan
	.quad	.L.str.3
	.quad	zcos
	.quad	.L.str.4
	.quad	zexp
	.quad	.L.str.5
	.quad	zln
	.quad	.L.str.6
	.quad	zlog
	.quad	.L.str.7
	.quad	zrand
	.quad	.L.str.8
	.quad	zrrand
	.quad	.L.str.9
	.quad	zsin
	.quad	.L.str.10
	.quad	zsqrt
	.quad	.L.str.11
	.quad	zsrand
	.zero	16
	.size	zmath_op_init.my_defs, 208

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"1arccos"
	.size	.L.str, 8

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"1arcsin"
	.size	.L.str.1, 8

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"2atan"
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"1cos"
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"2exp"
	.size	.L.str.4, 5

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"1ln"
	.size	.L.str.5, 4

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"1log"
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"0rand"
	.size	.L.str.7, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"0rrand"
	.size	.L.str.8, 7

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"1sin"
	.size	.L.str.9, 5

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"1sqrt"
	.size	.L.str.10, 6

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"1srand"
	.size	.L.str.11, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
