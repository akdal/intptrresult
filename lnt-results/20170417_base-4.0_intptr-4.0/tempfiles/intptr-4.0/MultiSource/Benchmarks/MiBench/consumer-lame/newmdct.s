	.text
	.file	"newmdct.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_1:
	.quad	4629418941960159232     # double 31
.LCPI0_2:
	.quad	4609753056924675352     # double 1.5707963267948966
	.text
	.globl	mdct_sub48
	.p2align	4, 0x90
	.type	mdct_sub48,@function
mdct_sub48:                             # @mdct_sub48
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi6:
	.cfi_def_cfa_offset 368
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, 152(%rsp)          # 8-byte Spill
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %r15
	cmpl	$0, mdct_sub48.init(%rip)
	jne	.LBB0_2
# BB#1:
	callq	mdct_init48
	incl	mdct_sub48.init(%rip)
.LBB0_2:                                # %.preheader239
	cmpl	$0, 204(%r15)
	jle	.LBB0_21
# BB#3:                                 # %.preheader238.lr.ph
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$sb_sample+8960, %r14d
	movl	$sb_sample+2048, %ebp
	movl	$sb_sample+6656, %esi
	movl	$sb_sample+2304, %edi
	movl	$sb_sample+6912, %ebx
	.p2align	4, 0x90
.LBB0_4:                                # %.preheader238
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_6 Depth 2
                                        #       Child Loop BB0_7 Depth 3
                                        #       Child Loop BB0_24 Depth 3
                                        #       Child Loop BB0_10 Depth 3
                                        #         Child Loop BB0_33 Depth 4
                                        #         Child Loop BB0_35 Depth 4
                                        #         Child Loop BB0_38 Depth 4
	cmpl	$0, 200(%r15)
	jle	.LBB0_20
# BB#5:                                 # %.lr.ph257.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	%rbp, 128(%rsp)         # 8-byte Spill
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	%r14, 136(%rsp)         # 8-byte Spill
	movq	%r14, 40(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph257
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_7 Depth 3
                                        #       Child Loop BB0_24 Depth 3
                                        #       Child Loop BB0_10 Depth 3
                                        #         Child Loop BB0_33 Depth 4
                                        #         Child Loop BB0_35 Depth 4
                                        #         Child Loop BB0_38 Depth 4
	movl	$1, %eax
	subq	%r14, %rax
	leaq	(%rax,%rax,8), %rcx
	shlq	$9, %rcx
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%rax,%rax,8), %r13
	movq	%r13, %rax
	shlq	$10, %rax
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	movq	%rax, 176(%rsp)         # 8-byte Spill
	leaq	sb_sample(%rax,%rcx), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_7:                                #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%r12,%rbx), %rdi
	movq	%rbp, %rsi
	callq	window_subband
	leaq	64(%r12,%rbx), %rdi
	leaq	256(%rbp), %rsi
	callq	window_subband
	movsd	264(%rbp), %xmm0        # xmm0 = mem[0],zero
	movapd	.LCPI0_0(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm1, %xmm0
	movlpd	%xmm0, 264(%rbp)
	movsd	280(%rbp), %xmm0        # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm0
	movlpd	%xmm0, 280(%rbp)
	movsd	296(%rbp), %xmm0        # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm0
	movlpd	%xmm0, 296(%rbp)
	movsd	312(%rbp), %xmm0        # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm0
	movlpd	%xmm0, 312(%rbp)
	movsd	328(%rbp), %xmm0        # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm0
	movlpd	%xmm0, 328(%rbp)
	movsd	344(%rbp), %xmm0        # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm0
	movlpd	%xmm0, 344(%rbp)
	movsd	360(%rbp), %xmm0        # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm0
	movlpd	%xmm0, 360(%rbp)
	movsd	376(%rbp), %xmm0        # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm0
	movlpd	%xmm0, 376(%rbp)
	movsd	392(%rbp), %xmm0        # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm0
	movlpd	%xmm0, 392(%rbp)
	movsd	408(%rbp), %xmm0        # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm0
	movlpd	%xmm0, 408(%rbp)
	movsd	424(%rbp), %xmm0        # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm0
	movlpd	%xmm0, 424(%rbp)
	movsd	440(%rbp), %xmm0        # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm0
	movlpd	%xmm0, 440(%rbp)
	movsd	456(%rbp), %xmm0        # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm0
	movlpd	%xmm0, 456(%rbp)
	movsd	472(%rbp), %xmm0        # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm0
	movlpd	%xmm0, 472(%rbp)
	movsd	488(%rbp), %xmm0        # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm0
	movlpd	%xmm0, 488(%rbp)
	movsd	504(%rbp), %xmm0        # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm0
	movlpd	%xmm0, 504(%rbp)
	subq	$-128, %rbx
	addq	$512, %rbp              # imm = 0x200
	cmpl	$1152, %ebx             # imm = 0x480
	jne	.LBB0_7
# BB#8:                                 #   in Loop: Header=BB0_6 Depth=2
	movq	%r12, 88(%rsp)          # 8-byte Spill
	movq	%r14, 80(%rsp)          # 8-byte Spill
	leaq	(%r14,%r14,8), %rbp
	movq	%rbp, 168(%rsp)         # 8-byte Spill
	shlq	$10, %rbp
	addq	144(%rsp), %rbp         # 8-byte Folded Reload
	shlq	$9, %r13
	cmpl	$0, 256(%r15)
	jne	.LBB0_9
# BB#22:                                #   in Loop: Header=BB0_6 Depth=2
	movslq	252(%r15), %r12
	incq	%r12
	cmpl	248(%r15), %r12d
	jge	.LBB0_9
# BB#23:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_6 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r12,8), %r14
	.p2align	4, 0x90
.LBB0_24:                               # %.lr.ph
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%r12d, %xmm4
	divsd	.LCPI0_1(%rip), %xmm4
	movss	232(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm4
	jbe	.LBB0_27
# BB#25:                                #   in Loop: Header=BB0_24 Depth=3
	movss	236(%r15), %xmm2        # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm3
	ucomisd	%xmm4, %xmm3
	jbe	.LBB0_27
# BB#26:                                # %.loopexit236.loopexit264
                                        #   in Loop: Header=BB0_24 Depth=3
	subsd	%xmm4, %xmm0
	mulsd	.LCPI0_2(%rip), %xmm0
	subss	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm2, %xmm1
	divsd	%xmm1, %xmm0
	movsd	%xmm4, 96(%rsp)         # 8-byte Spill
	callq	cos
	movsd	96(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movsd	-4352(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -4352(%r14)
	movsd	-4096(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -4096(%r14)
	movsd	-3840(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -3840(%r14)
	movsd	-3584(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -3584(%r14)
	movsd	-3328(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -3328(%r14)
	movsd	-3072(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -3072(%r14)
	movsd	-2816(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -2816(%r14)
	movsd	-2560(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -2560(%r14)
	movsd	-2304(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -2304(%r14)
	movsd	-2048(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -2048(%r14)
	movsd	-1792(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -1792(%r14)
	movsd	-1536(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -1536(%r14)
	movsd	-1280(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -1280(%r14)
	movsd	-1024(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -1024(%r14)
	movsd	-768(%r14), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -768(%r14)
	movsd	-512(%r14), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -512(%r14)
	movsd	-256(%r14), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -256(%r14)
	mulsd	(%r14), %xmm0
	movsd	%xmm0, (%r14)
.LBB0_27:                               # %.loopexit236
                                        #   in Loop: Header=BB0_24 Depth=3
	movss	240(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm4
	jbe	.LBB0_28
# BB#29:                                #   in Loop: Header=BB0_24 Depth=3
	movss	244(%r15), %xmm2        # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm2, %xmm0
	ucomisd	%xmm4, %xmm0
	jbe	.LBB0_28
# BB#30:                                # %.backedge.loopexit266
                                        #   in Loop: Header=BB0_24 Depth=3
	subsd	%xmm4, %xmm0
	mulsd	.LCPI0_2(%rip), %xmm0
	subss	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm2, %xmm1
	divsd	%xmm1, %xmm0
	callq	cos
	movsd	-4352(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -4352(%r14)
	movsd	-4096(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -4096(%r14)
	movsd	-3840(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -3840(%r14)
	movsd	-3584(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -3584(%r14)
	movsd	-3328(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -3328(%r14)
	movsd	-3072(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -3072(%r14)
	movsd	-2816(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -2816(%r14)
	movsd	-2560(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -2560(%r14)
	movsd	-2304(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -2304(%r14)
	movsd	-2048(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -2048(%r14)
	movsd	-1792(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -1792(%r14)
	movsd	-1536(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -1536(%r14)
	movsd	-1280(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -1280(%r14)
	movsd	-1024(%r14), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -1024(%r14)
	movsd	-768(%r14), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -768(%r14)
	movsd	-512(%r14), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -512(%r14)
	movsd	-256(%r14), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -256(%r14)
	mulsd	(%r14), %xmm0
	movsd	%xmm0, (%r14)
.LBB0_28:                               # %.backedge
                                        #   in Loop: Header=BB0_24 Depth=3
	incq	%r12
	movslq	248(%r15), %rax
	addq	$8, %r14
	cmpq	%rax, %r12
	jl	.LBB0_24
	.p2align	4, 0x90
.LBB0_9:                                # %.preheader237
                                        #   in Loop: Header=BB0_6 Depth=2
	addq	%r13, %rbp
	addq	$1152, 88(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x480
	imulq	$240, 80(%rsp), %rax    # 8-byte Folded Reload
	addq	152(%rsp), %rax         # 8-byte Folded Reload
	imulq	$120, 8(%rsp), %rcx     # 8-byte Folded Reload
	leaq	72(%rcx,%rax), %rdi
	movapd	ca+48(%rip), %xmm10
	movapd	cs+48(%rip), %xmm0
	movapd	ca+32(%rip), %xmm11
	movapd	cs+32(%rip), %xmm1
	movapd	ca+16(%rip), %xmm12
	movapd	cs+16(%rip), %xmm2
	movapd	ca(%rip), %xmm13
	movapd	cs(%rip), %xmm3
	movapd	%xmm0, 96(%rsp)         # 16-byte Spill
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	movapd	%xmm0, 240(%rsp)        # 16-byte Spill
	movapd	%xmm1, 288(%rsp)        # 16-byte Spill
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	movapd	%xmm1, 224(%rsp)        # 16-byte Spill
	movapd	%xmm2, 272(%rsp)        # 16-byte Spill
	shufpd	$1, %xmm2, %xmm2        # xmm2 = xmm2[1,0]
	movapd	%xmm2, 208(%rsp)        # 16-byte Spill
	movapd	%xmm3, 256(%rsp)        # 16-byte Spill
	shufpd	$1, %xmm3, %xmm3        # xmm3 = xmm3[1,0]
	movapd	%xmm3, 192(%rsp)        # 16-byte Spill
	movq	72(%rsp), %r10          # 8-byte Reload
	movq	64(%rsp), %r11          # 8-byte Reload
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	xorl	%eax, %eax
	movq	%rdi, 160(%rsp)         # 8-byte Spill
	jmp	.LBB0_10
.LBB0_32:                               # %.preheader234
                                        #   in Loop: Header=BB0_10 Depth=3
	leaq	(%r9,%r9,8), %rcx
	shlq	$5, %rcx
	leaq	win+280(%rcx), %rcx
	movq	%r10, %rbx
	movq	%r11, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB0_33:                               #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        #       Parent Loop BB0_10 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	-216(%rcx,%r8,8), %xmm0 # xmm0 = mem[0],zero
	mulsd	(%rdi), %xmm0
	movsd	-144(%rcx,%r8,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	(%rdx), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, win+672(,%r8,8)
	movsd	-72(%rcx,%r8,8), %xmm0  # xmm0 = mem[0],zero
	mulsd	(%rsi), %xmm0
	movsd	(%rcx,%r8,8), %xmm1     # xmm1 = mem[0],zero
	mulsd	(%rbx), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, win+744(,%r8,8)
	leaq	-1(%r8), %r13
	addq	$8, %r8
	addq	$-256, %rdi
	addq	$-256, %rsi
	addq	$256, %rdx              # imm = 0x100
	addq	$256, %rbx              # imm = 0x100
	testq	%r8, %r8
	movq	%r13, %r8
	jg	.LBB0_33
# BB#34:                                # %.preheader233.preheader
                                        #   in Loop: Header=BB0_10 Depth=3
	movl	$12, %ecx
	movl	$cos_l, %ebx
	.p2align	4, 0x90
.LBB0_35:                               # %.preheader233
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        #       Parent Loop BB0_10 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	win+608(%rip), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%rbx), %xmm0
	movsd	win+616(%rip), %xmm1    # xmm1 = mem[0],zero
	mulsd	8(%rbx), %xmm1
	addsd	%xmm0, %xmm1
	movsd	win+624(%rip), %xmm0    # xmm0 = mem[0],zero
	mulsd	16(%rbx), %xmm0
	movsd	win+632(%rip), %xmm2    # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm0
	mulsd	24(%rbx), %xmm2
	movsd	win+640(%rip), %xmm1    # xmm1 = mem[0],zero
	mulsd	32(%rbx), %xmm1
	addsd	%xmm0, %xmm2
	addsd	%xmm2, %xmm1
	movsd	win+648(%rip), %xmm0    # xmm0 = mem[0],zero
	mulsd	40(%rbx), %xmm0
	addsd	%xmm1, %xmm0
	movsd	win+656(%rip), %xmm1    # xmm1 = mem[0],zero
	mulsd	48(%rbx), %xmm1
	movsd	win+664(%rip), %xmm2    # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm1
	mulsd	56(%rbx), %xmm2
	movsd	win+672(%rip), %xmm0    # xmm0 = mem[0],zero
	mulsd	64(%rbx), %xmm0
	addsd	%xmm1, %xmm2
	addsd	%xmm2, %xmm0
	movsd	win+680(%rip), %xmm1    # xmm1 = mem[0],zero
	mulsd	72(%rbx), %xmm1
	addsd	%xmm0, %xmm1
	movsd	win+688(%rip), %xmm0    # xmm0 = mem[0],zero
	mulsd	80(%rbx), %xmm0
	movsd	win+696(%rip), %xmm2    # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm0
	mulsd	88(%rbx), %xmm2
	movsd	win+704(%rip), %xmm1    # xmm1 = mem[0],zero
	mulsd	96(%rbx), %xmm1
	addsd	%xmm0, %xmm2
	addsd	%xmm2, %xmm1
	movsd	win+712(%rip), %xmm0    # xmm0 = mem[0],zero
	mulsd	104(%rbx), %xmm0
	addsd	%xmm1, %xmm0
	movsd	win+720(%rip), %xmm1    # xmm1 = mem[0],zero
	mulsd	112(%rbx), %xmm1
	movsd	win+728(%rip), %xmm2    # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm1
	mulsd	120(%rbx), %xmm2
	movsd	win+736(%rip), %xmm0    # xmm0 = mem[0],zero
	mulsd	128(%rbx), %xmm0
	addsd	%xmm1, %xmm2
	addsd	%xmm2, %xmm0
	movsd	win+744(%rip), %xmm1    # xmm1 = mem[0],zero
	mulsd	136(%rbx), %xmm1
	addsd	%xmm0, %xmm1
	movslq	all-4(,%rcx,4), %rdx
	addq	$144, %rbx
	decq	%rcx
	movsd	%xmm1, (%rbp,%rdx,8)
	jg	.LBB0_35
# BB#36:                                # %mdct_long.exit
                                        #   in Loop: Header=BB0_10 Depth=3
	movsd	win+608(%rip), %xmm1    # xmm1 = mem[0],zero
	addsd	win+648(%rip), %xmm1
	addsd	win+728(%rip), %xmm1
	movsd	win+616(%rip), %xmm8    # xmm8 = mem[0],zero
	addsd	win+640(%rip), %xmm8
	addsd	win+736(%rip), %xmm8
	movsd	win+624(%rip), %xmm3    # xmm3 = mem[0],zero
	addsd	win+632(%rip), %xmm3
	addsd	win+744(%rip), %xmm3
	movsd	win+656(%rip), %xmm7    # xmm7 = mem[0],zero
	subsd	win+680(%rip), %xmm7
	addsd	win+720(%rip), %xmm7
	movsd	win+664(%rip), %xmm5    # xmm5 = mem[0],zero
	subsd	win+688(%rip), %xmm5
	addsd	win+712(%rip), %xmm5
	movsd	win+672(%rip), %xmm9    # xmm9 = mem[0],zero
	subsd	win+696(%rip), %xmm9
	addsd	win+704(%rip), %xmm9
	movsd	cos_l+1728(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	cos_l+1736(%rip), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm8, %xmm2
	addsd	%xmm0, %xmm2
	movsd	cos_l+1744(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	%xmm3, %xmm0
	addsd	%xmm2, %xmm0
	movsd	cos_l+1752(%rip), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm7, %xmm2
	addsd	%xmm0, %xmm2
	movsd	cos_l+1760(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	%xmm5, %xmm0
	addsd	%xmm2, %xmm0
	movsd	cos_l+1768(%rip), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm9, %xmm2
	addsd	%xmm0, %xmm2
	movsd	%xmm2, 128(%rbp)
	movsd	cos_l+1776(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	cos_l+1784(%rip), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm8, %xmm2
	addsd	%xmm0, %xmm2
	movsd	cos_l+1792(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	%xmm3, %xmm0
	addsd	%xmm2, %xmm0
	movsd	cos_l+1800(%rip), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm7, %xmm2
	addsd	%xmm0, %xmm2
	movsd	cos_l+1808(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	%xmm5, %xmm0
	addsd	%xmm2, %xmm0
	movsd	cos_l+1816(%rip), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm9, %xmm2
	addsd	%xmm0, %xmm2
	movsd	%xmm2, 80(%rbp)
	movsd	cos_l+1824(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	cos_l+1832(%rip), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm8, %xmm2
	addsd	%xmm0, %xmm2
	movsd	cos_l+1840(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	%xmm3, %xmm0
	addsd	%xmm2, %xmm0
	movsd	cos_l+1848(%rip), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm7, %xmm2
	addsd	%xmm0, %xmm2
	movsd	cos_l+1856(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	%xmm5, %xmm0
	addsd	%xmm2, %xmm0
	movsd	cos_l+1864(%rip), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm9, %xmm2
	addsd	%xmm0, %xmm2
	movsd	%xmm2, 56(%rbp)
	movsd	cos_l+1872(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	subsd	%xmm8, %xmm1
	mulsd	cos_l+1880(%rip), %xmm8
	addsd	%xmm0, %xmm8
	movsd	cos_l+1888(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	%xmm3, %xmm0
	addsd	%xmm8, %xmm0
	subsd	%xmm7, %xmm3
	mulsd	cos_l+1896(%rip), %xmm7
	addsd	%xmm0, %xmm7
	subsd	%xmm5, %xmm3
	mulsd	cos_l+1904(%rip), %xmm5
	addsd	%xmm7, %xmm5
	addsd	%xmm9, %xmm1
	mulsd	cos_l+1912(%rip), %xmm9
	addsd	%xmm5, %xmm9
	movsd	%xmm9, 8(%rbp)
	movsd	cos_l+1920(%rip), %xmm0 # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	cos_l+1928(%rip), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm3, %xmm2
	addsd	%xmm0, %xmm2
	movsd	%xmm2, 104(%rbp)
	mulsd	cos_l+1936(%rip), %xmm1
	mulsd	cos_l+1944(%rip), %xmm3
	addsd	%xmm1, %xmm3
	movsd	%xmm3, 32(%rbp)
	movq	160(%rsp), %rdi         # 8-byte Reload
	testq	%rax, %rax
	jne	.LBB0_14
	jmp	.LBB0_16
	.p2align	4, 0x90
.LBB0_10:                               #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_33 Depth 4
                                        #         Child Loop BB0_35 Depth 4
                                        #         Child Loop BB0_38 Depth 4
	movslq	(%rdi), %r9
	movslq	248(%r15), %rcx
	cmpq	%rcx, %rax
	jge	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_10 Depth=3
	movslq	252(%r15), %rcx
	cmpq	%rcx, %rax
	jle	.LBB0_12
# BB#31:                                #   in Loop: Header=BB0_10 Depth=3
	cmpl	$2, %r9d
	jne	.LBB0_32
# BB#37:                                # %.preheader232
                                        #   in Loop: Header=BB0_10 Depth=3
	movq	168(%rsp), %rcx         # 8-byte Reload
	shlq	$9, %rcx
	movq	176(%rsp), %rsi         # 8-byte Reload
	leaq	sb_sample(%rsi,%rcx), %rcx
	movups	win+584(%rip), %xmm14
	movaps	%xmm14, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	movsd	2048(%rcx,%rax,8), %xmm0 # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	subsd	2304(%rcx,%rax,8), %xmm0
	movsd	%xmm0, win+624(%rip)
	movsd	3584(%rcx,%rax,8), %xmm0 # xmm0 = mem[0],zero
	movsd	3840(%rcx,%rax,8), %xmm2 # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm4
	mulsd	%xmm2, %xmm4
	addsd	%xmm0, %xmm4
	movsd	%xmm4, win+648(%rip)
	mulsd	%xmm1, %xmm0
	subsd	%xmm2, %xmm0
	movsd	%xmm0, win+672(%rip)
	movq	184(%rsp), %rdx         # 8-byte Reload
	leaq	sb_sample(%rsi,%rdx), %rbx
	movsd	512(%rbx,%rax,8), %xmm2 # xmm2 = mem[0],zero
	movsd	768(%rbx,%rax,8), %xmm0 # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	movsd	%xmm1, win+696(%rip)
	movsd	256(%rbx,%rax,8), %xmm8 # xmm8 = mem[0],zero
	movapd	%xmm8, %xmm15
	unpcklpd	%xmm2, %xmm15   # xmm15 = xmm15[0],xmm2[0]
	mulpd	%xmm14, %xmm15
	movsd	1024(%rbx,%rax,8), %xmm1 # xmm1 = mem[0],zero
	movsd	1280(%rbx,%rax,8), %xmm2 # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm4
	unpcklpd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0]
	unpcklpd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	subpd	%xmm1, %xmm15
	movsd	2304(%rbx,%rax,8), %xmm0 # xmm0 = mem[0],zero
	movsd	2048(%rbx,%rax,8), %xmm1 # xmm1 = mem[0],zero
	movsd	1792(%rcx,%rax,8), %xmm6 # xmm6 = mem[0],zero
	movsd	2560(%rcx,%rax,8), %xmm7 # xmm7 = mem[0],zero
	movsd	4096(%rcx,%rax,8), %xmm5 # xmm5 = mem[0],zero
	movupd	%xmm15, win+712(%rip)
	movsd	2560(%rbx,%rax,8), %xmm3 # xmm3 = mem[0],zero
	unpcklpd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0]
	mulpd	%xmm14, %xmm3
	movsd	1792(%rbx,%rax,8), %xmm9 # xmm9 = mem[0],zero
	unpcklpd	%xmm1, %xmm9    # xmm9 = xmm9[0],xmm1[0]
	addpd	%xmm3, %xmm9
	movapd	%xmm9, win+736(%rip)
	movsd	win+576(%rip), %xmm0    # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	unpcklpd	%xmm14, %xmm1   # xmm1 = xmm1[0],xmm14[0]
	movsd	1536(%rcx,%rax,8), %xmm3 # xmm3 = mem[0],zero
	unpcklpd	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0]
	movsd	2816(%rcx,%rax,8), %xmm6 # xmm6 = mem[0],zero
	unpcklpd	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0]
	movsd	3328(%rcx,%rax,8), %xmm7 # xmm7 = mem[0],zero
	mulpd	%xmm1, %xmm3
	subpd	%xmm6, %xmm3
	movapd	%xmm3, win+608(%rip)
	movsd	4352(%rcx,%rax,8), %xmm3 # xmm3 = mem[0],zero
	unpcklpd	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0]
	movapd	%xmm1, %xmm5
	mulpd	%xmm3, %xmm5
	movsd	3072(%rcx,%rax,8), %xmm6 # xmm6 = mem[0],zero
	unpcklpd	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0]
	addpd	%xmm6, %xmm5
	movupd	%xmm5, win+632(%rip)
	mulpd	%xmm1, %xmm6
	subpd	%xmm3, %xmm6
	movapd	%xmm6, win+656(%rip)
	movsd	(%rbx,%rax,8), %xmm3    # xmm3 = mem[0],zero
	mulpd	%xmm1, %xmm4
	movapd	%xmm0, %xmm14
	mulsd	%xmm3, %xmm14
	unpcklpd	%xmm8, %xmm3    # xmm3 = xmm3[0],xmm8[0]
	addpd	%xmm4, %xmm3
	movupd	%xmm3, win+680(%rip)
	subsd	%xmm2, %xmm14
	movsd	%xmm14, win+704(%rip)
	mulsd	2816(%rbx,%rax,8), %xmm0
	addsd	1536(%rbx,%rax,8), %xmm0
	movsd	%xmm0, win+728(%rip)
	movapd	%xmm15, %xmm8
	movhlps	%xmm8, %xmm8            # xmm8 = xmm8[1,1]
	movapd	%xmm9, %xmm7
	movhlps	%xmm7, %xmm7            # xmm7 = xmm7[1,1]
	movl	$120, %ebx
	movl	$5, %ecx
	jmp	.LBB0_38
	.p2align	4, 0x90
.LBB0_39:                               # %.preheader231..preheader231_crit_edge
                                        #   in Loop: Header=BB0_38 Depth=4
	decq	%rcx
	movsd	win+704(%rip), %xmm14   # xmm14 = mem[0],zero
	movsd	win+712(%rip), %xmm15   # xmm15 = mem[0],zero
	movsd	win+720(%rip), %xmm8    # xmm8 = mem[0],zero
	movsd	win+728(%rip), %xmm0    # xmm0 = mem[0],zero
	movsd	win+736(%rip), %xmm9    # xmm9 = mem[0],zero
	movsd	win+744(%rip), %xmm7    # xmm7 = mem[0],zero
	addq	$-24, %rbx
.LBB0_38:                               # %.preheader231
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_6 Depth=2
                                        #       Parent Loop BB0_10 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	cos_s(%rbx,%rbx), %xmm1 # xmm1 = mem[0],zero
	movsd	cos_s+8(%rbx,%rbx), %xmm6 # xmm6 = mem[0],zero
	movsd	cos_s+16(%rbx,%rbx), %xmm4 # xmm4 = mem[0],zero
	movsd	cos_s+24(%rbx,%rbx), %xmm2 # xmm2 = mem[0],zero
	movsd	cos_s+32(%rbx,%rbx), %xmm3 # xmm3 = mem[0],zero
	movsd	cos_s+40(%rbx,%rbx), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm1, %xmm14
	mulsd	%xmm6, %xmm15
	addsd	%xmm14, %xmm15
	mulsd	%xmm4, %xmm8
	addsd	%xmm15, %xmm8
	mulsd	%xmm2, %xmm0
	addsd	%xmm8, %xmm0
	mulsd	%xmm3, %xmm9
	addsd	%xmm0, %xmm9
	mulsd	%xmm5, %xmm7
	addsd	%xmm9, %xmm7
	movsd	%xmm7, 16(%rbp,%rbx)
	movsd	win+656(%rip), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	win+664(%rip), %xmm7    # xmm7 = mem[0],zero
	mulsd	%xmm6, %xmm7
	addsd	%xmm0, %xmm7
	movsd	win+672(%rip), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	addsd	%xmm7, %xmm0
	movsd	win+680(%rip), %xmm7    # xmm7 = mem[0],zero
	mulsd	%xmm2, %xmm7
	addsd	%xmm0, %xmm7
	movsd	win+688(%rip), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm3, %xmm0
	addsd	%xmm7, %xmm0
	movsd	win+696(%rip), %xmm7    # xmm7 = mem[0],zero
	mulsd	%xmm5, %xmm7
	addsd	%xmm0, %xmm7
	movsd	%xmm7, 8(%rbp,%rbx)
	mulsd	win+608(%rip), %xmm1
	mulsd	win+616(%rip), %xmm6
	addsd	%xmm1, %xmm6
	mulsd	win+624(%rip), %xmm4
	addsd	%xmm6, %xmm4
	mulsd	win+632(%rip), %xmm2
	addsd	%xmm4, %xmm2
	mulsd	win+640(%rip), %xmm3
	addsd	%xmm2, %xmm3
	mulsd	win+648(%rip), %xmm5
	addsd	%xmm3, %xmm5
	movsd	%xmm5, (%rbp,%rbx)
	testq	%rcx, %rcx
	jg	.LBB0_39
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_12:                               #   in Loop: Header=BB0_10 Depth=3
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 128(%rbp)
	movupd	%xmm0, 112(%rbp)
	movupd	%xmm0, 96(%rbp)
	movupd	%xmm0, 80(%rbp)
	movupd	%xmm0, 64(%rbp)
	movupd	%xmm0, 48(%rbp)
	movupd	%xmm0, 32(%rbp)
	movupd	%xmm0, 16(%rbp)
	movupd	%xmm0, (%rbp)
.LBB0_13:                               # %mdct_short.exit
                                        #   in Loop: Header=BB0_10 Depth=3
	testq	%rax, %rax
	je	.LBB0_16
.LBB0_14:                               # %mdct_short.exit
                                        #   in Loop: Header=BB0_10 Depth=3
	cmpl	$2, %r9d
	je	.LBB0_16
# BB#15:                                # %.preheader.preheader
                                        #   in Loop: Header=BB0_10 Depth=3
	movupd	-64(%rbp), %xmm0
	movupd	48(%rbp), %xmm1
	movapd	%xmm1, %xmm2
	mulpd	%xmm10, %xmm2
	shufpd	$1, %xmm2, %xmm2        # xmm2 = xmm2[1,0]
	movapd	%xmm0, %xmm3
	mulpd	240(%rsp), %xmm3        # 16-byte Folded Reload
	addpd	%xmm2, %xmm3
	mulpd	96(%rsp), %xmm1         # 16-byte Folded Reload
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	mulpd	%xmm10, %xmm0
	subpd	%xmm0, %xmm1
	movupd	%xmm3, -64(%rbp)
	movupd	%xmm1, 48(%rbp)
	movupd	-48(%rbp), %xmm0
	movupd	32(%rbp), %xmm1
	movapd	%xmm1, %xmm2
	mulpd	%xmm11, %xmm2
	shufpd	$1, %xmm2, %xmm2        # xmm2 = xmm2[1,0]
	movapd	%xmm0, %xmm3
	mulpd	224(%rsp), %xmm3        # 16-byte Folded Reload
	addpd	%xmm2, %xmm3
	mulpd	288(%rsp), %xmm1        # 16-byte Folded Reload
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	mulpd	%xmm11, %xmm0
	subpd	%xmm0, %xmm1
	movupd	%xmm3, -48(%rbp)
	movupd	%xmm1, 32(%rbp)
	movupd	-32(%rbp), %xmm0
	movupd	16(%rbp), %xmm1
	movapd	%xmm1, %xmm2
	mulpd	%xmm12, %xmm2
	shufpd	$1, %xmm2, %xmm2        # xmm2 = xmm2[1,0]
	movapd	%xmm0, %xmm3
	mulpd	208(%rsp), %xmm3        # 16-byte Folded Reload
	addpd	%xmm2, %xmm3
	mulpd	272(%rsp), %xmm1        # 16-byte Folded Reload
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	mulpd	%xmm12, %xmm0
	subpd	%xmm0, %xmm1
	movupd	%xmm3, -32(%rbp)
	movupd	%xmm1, 16(%rbp)
	movupd	-16(%rbp), %xmm0
	movupd	(%rbp), %xmm1
	movapd	%xmm1, %xmm2
	mulpd	%xmm13, %xmm2
	shufpd	$1, %xmm2, %xmm2        # xmm2 = xmm2[1,0]
	movapd	%xmm0, %xmm3
	mulpd	192(%rsp), %xmm3        # 16-byte Folded Reload
	addpd	%xmm2, %xmm3
	mulpd	256(%rsp), %xmm1        # 16-byte Folded Reload
	shufpd	$1, %xmm0, %xmm0        # xmm0 = xmm0[1,0]
	mulpd	%xmm13, %xmm0
	subpd	%xmm0, %xmm1
	movupd	%xmm3, -16(%rbp)
	movupd	%xmm1, (%rbp)
.LBB0_16:                               # %.loopexit
                                        #   in Loop: Header=BB0_10 Depth=3
	incq	%rax
	addq	$144, %rbp
	addq	$8, %r12
	addq	$8, %r14
	addq	$8, %r11
	addq	$8, %r10
	cmpq	$32, %rax
	jne	.LBB0_10
# BB#17:                                #   in Loop: Header=BB0_6 Depth=2
	movq	80(%rsp), %r14          # 8-byte Reload
	incq	%r14
	movslq	200(%r15), %rax
	addq	$-4608, 40(%rsp)        # 8-byte Folded Spill
                                        # imm = 0xEE00
	addq	$4608, 48(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x1200
	addq	$-4608, 56(%rsp)        # 8-byte Folded Spill
                                        # imm = 0xEE00
	addq	$4608, 64(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x1200
	addq	$-4608, 72(%rsp)        # 8-byte Folded Spill
                                        # imm = 0xEE00
	cmpq	%rax, %r14
	movq	88(%rsp), %r12          # 8-byte Reload
	jl	.LBB0_6
# BB#18:                                # %._crit_edge
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpl	$1, %eax
	movq	136(%rsp), %r14         # 8-byte Reload
	movq	128(%rsp), %rbp         # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	jne	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_4 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	shlq	$10, %rax
	leaq	sb_sample(%rax,%rax,8), %rdi
	leaq	sb_sample+4608(%rax,%rax,8), %rsi
	movl	$4608, %edx             # imm = 0x1200
	callq	memcpy
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
.LBB0_20:                               # %._crit_edge.thread
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rdx
	incq	%rdx
	movslq	204(%r15), %rax
	addq	$9216, %r14             # imm = 0x2400
	addq	$9216, %rbp             # imm = 0x2400
	addq	$9216, %rsi             # imm = 0x2400
	addq	$9216, %rdi             # imm = 0x2400
	addq	$9216, %rbx             # imm = 0x2400
	movq	%rdx, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	cmpq	%rax, %rdx
	movq	120(%rsp), %r12         # 8-byte Reload
	jl	.LBB0_4
.LBB0_21:                               # %._crit_edge260
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	mdct_sub48, .Lfunc_end0-mdct_sub48
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.quad	-4620562651524645171    # double -0.51449575542752657
	.quad	-4621202449265478048    # double -0.47173196856497235
.LCPI1_1:
	.quad	4605898829186700118     # double 0.85749292571254432
	.quad	4606117245406390284     # double 0.88174199731770519
.LCPI1_2:
	.quad	-4624055110592953860    # double -0.31337745420390184
	.quad	-4627649902820386068    # double -0.18191319961098118
.LCPI1_3:
	.quad	4606728714005755246     # double 0.94962864910273281
	.quad	4607032130009944411     # double 0.9833145924917902
.LCPI1_4:
	.quad	-4631892827420077222    # double -0.094574192526420658
	.quad	-4637307453136828720    # double -0.040965582885304053
.LCPI1_5:
	.quad	4607142046876241755     # double 0.99551781606758582
	.quad	4607174857780265220     # double 0.99916055817814752
.LCPI1_6:
	.quad	-4644033497552803903    # double -0.01419856857247115
	.quad	-4652694053592706060    # double -0.0036999746737600373
.LCPI1_7:
	.quad	4607181510831498318     # double 0.99989919524444715
	.quad	4607182357146371538     # double 0.9999931550702803
.LCPI1_30:
	.quad	-4633833629089670977    # double -0.067640158778746504
	.quad	4586288653794550921     # double 0.042520381373898415
.LCPI1_31:
	.quad	-4643858056522336715    # double -0.014502910246672144
	.quad	4591016324321313816     # double 0.088150371143470685
.LCPI1_32:
	.quad	-4631310667714538755    # double -0.10265328139014325
	.quad	4592602324144483129     # double 0.11016054015264566
.LCPI1_33:
	.quad	4579513980332439130     # double 0.014502910246672208
	.quad	4592061369140237039     # double 0.10265328139014306
.LCPI1_34:
	.quad	-4633833629089670976    # double -0.067640158778746517
	.quad	-4630769712710292680    # double -0.11016054015264565
.LCPI1_35:
	.quad	4586288653794550975     # double 0.04252038137389879
	.quad	4591016324321313813     # double 0.088150371143470643
.LCPI1_36:
	.quad	4592602324144483125     # double 0.11016054015264561
	.quad	4586288653794550974     # double 0.042520381373898783
.LCPI1_37:
	.quad	-4632355712533461995    # double -0.088150371143470643
	.quad	4579513980332439224     # double 0.014502910246672371
.LCPI1_38:
	.quad	-4631310667714538774    # double -0.10265328139014299
	.quad	-4633833629089670965    # double -0.06764015877874667
.LCPI1_39:
	.quad	-4632355712533462001    # double -0.08815037114347056
	.quad	-4631310667714538776    # double -0.10265328139014296
.LCPI1_40:
	.quad	-4630769712710292684    # double -0.11016054015264559
	.quad	-4633833629089670958    # double -0.067640158778746767
.LCPI1_41:
	.quad	-4637083383060224814    # double -0.042520381373898922
	.quad	-4643858056522336511    # double -0.014502910246672497
.LCPI1_42:
	.quad	-4631310667714538768    # double -0.10265328139014307
	.quad	-4637083383060224831    # double -0.042520381373898804
.LCPI1_43:
	.quad	4586288653794550996     # double 0.042520381373898936
	.quad	-4631310667714538778    # double -0.10265328139014293
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_8:
	.quad	4602678819172646912     # double 0.5
.LCPI1_9:
	.quad	4590952631998301319     # double 0.087266462599716474
.LCPI1_10:
	.quad	4581945432743560327     # double 0.021816615649929118
.LCPI1_11:
	.quad	4630544841867001856     # double 38
.LCPI1_12:
	.quad	4592670820000712476     # double 0.1111111111111111
.LCPI1_13:
	.quad	4631107791820423168     # double 42
.LCPI1_14:
	.quad	4631670741773844480     # double 46
.LCPI1_15:
	.quad	4632233691727265792     # double 50
.LCPI1_16:
	.quad	4632796641680687104     # double 54
.LCPI1_17:
	.quad	4633359591634108416     # double 58
.LCPI1_18:
	.quad	4633922541587529728     # double 62
.LCPI1_19:
	.quad	4634344754052595712     # double 66
.LCPI1_20:
	.quad	4634626229029306368     # double 70
.LCPI1_21:
	.quad	4637440978796412928     # double 110
.LCPI1_22:
	.quad	4637722453773123584     # double 114
.LCPI1_23:
	.quad	4638003928749834240     # double 118
.LCPI1_24:
	.quad	4638285403726544896     # double 122
.LCPI1_25:
	.quad	4638566878703255552     # double 126
.LCPI1_26:
	.quad	4638777984935788544     # double 130
.LCPI1_27:
	.quad	4638918722424143872     # double 134
.LCPI1_28:
	.quad	4639059459912499200     # double 138
.LCPI1_29:
	.quad	4639200197400854528     # double 142
.LCPI1_44:
	.quad	4614256656552045848     # double 3.1415926535897931
.LCPI1_45:
	.quad	4580160821035794432     # double 0.015625
.LCPI1_46:
	.quad	4539628424389459968     # double 3.0517578125E-5
.LCPI1_47:
	.quad	4598387778419258213     # double 0.26179938779914941
.LCPI1_48:
	.quad	4599676419421066581     # double 0.33333333333333331
.LCPI1_49:
	.quad	4589380579164517221     # double 0.065449846949787352
.LCPI1_50:
	.quad	4596242258042563864     # double 0.19634954084936207
.LCPI1_51:
	.quad	4599566818044596286     # double 0.32724923474893675
.LCPI1_52:
	.quad	4601924897295272433     # double 0.45814892864851148
.LCPI1_53:
	.quad	4603480897859297746     # double 0.58904862254808621
.LCPI1_54:
	.quad	4604659937484635819     # double 0.71994831644766089
	.text
	.globl	mdct_init48
	.p2align	4, 0x90
	.type	mdct_init48,@function
mdct_init48:                            # @mdct_init48
	.cfi_startproc
# BB#0:                                 # %.preheader231.preheader350
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
	subq	$304, %rsp              # imm = 0x130
.Lcfi18:
	.cfi_def_cfa_offset 352
.Lcfi19:
	.cfi_offset %rbx, -48
.Lcfi20:
	.cfi_offset %r12, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [-5.144958e-01,-4.717320e-01]
	movaps	%xmm0, ca(%rip)
	movaps	.LCPI1_1(%rip), %xmm0   # xmm0 = [8.574929e-01,8.817420e-01]
	movaps	%xmm0, cs(%rip)
	movaps	.LCPI1_2(%rip), %xmm0   # xmm0 = [-3.133775e-01,-1.819132e-01]
	movaps	%xmm0, ca+16(%rip)
	movaps	.LCPI1_3(%rip), %xmm0   # xmm0 = [9.496286e-01,9.833146e-01]
	movaps	%xmm0, cs+16(%rip)
	movaps	.LCPI1_4(%rip), %xmm0   # xmm0 = [-9.457419e-02,-4.096558e-02]
	movaps	%xmm0, ca+32(%rip)
	movaps	.LCPI1_5(%rip), %xmm0   # xmm0 = [9.955178e-01,9.991606e-01]
	movaps	%xmm0, cs+32(%rip)
	movaps	.LCPI1_6(%rip), %xmm0   # xmm0 = [-1.419857e-02,-3.699975e-03]
	movaps	%xmm0, ca+48(%rip)
	movapd	.LCPI1_7(%rip), %xmm0   # xmm0 = [9.998992e-01,9.999932e-01]
	movapd	%xmm0, cs+48(%rip)
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_1:                                # %.preheader231
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movsd	.LCPI1_8(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	.LCPI1_9(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	callq	sin
	movsd	%xmm0, win(,%rbx,8)
	leal	1(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	addsd	.LCPI1_8(%rip), %xmm0
	mulsd	.LCPI1_9(%rip), %xmm0
	callq	sin
	movsd	%xmm0, win+8(,%rbx,8)
	leal	2(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	addsd	.LCPI1_8(%rip), %xmm0
	mulsd	.LCPI1_9(%rip), %xmm0
	callq	sin
	movsd	%xmm0, win+16(,%rbx,8)
	leal	3(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	addsd	.LCPI1_8(%rip), %xmm0
	mulsd	.LCPI1_9(%rip), %xmm0
	callq	sin
	movsd	%xmm0, win+24(,%rbx,8)
	addq	$4, %rbx
	cmpq	$36, %rbx
	jne	.LBB1_1
# BB#2:                                 # %.lr.ph265
	movaps	win(%rip), %xmm0
	movaps	%xmm0, win+288(%rip)
	movaps	win+16(%rip), %xmm0
	movaps	%xmm0, win+304(%rip)
	movaps	win+32(%rip), %xmm0
	movaps	%xmm0, win+320(%rip)
	movaps	win+48(%rip), %xmm0
	movaps	%xmm0, win+336(%rip)
	movaps	win+64(%rip), %xmm0
	movaps	%xmm0, win+352(%rip)
	movaps	win+80(%rip), %xmm0
	movaps	%xmm0, win+368(%rip)
	movaps	win+96(%rip), %xmm0
	movaps	%xmm0, win+384(%rip)
	movaps	win+112(%rip), %xmm0
	movaps	%xmm0, win+400(%rip)
	movapd	win+128(%rip), %xmm0
	movapd	%xmm0, win+416(%rip)
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, win+432(%rip)
	movq	%rax, win+440(%rip)
	movq	%rax, win+448(%rip)
	movq	%rax, win+456(%rip)
	movq	%rax, win+464(%rip)
	movq	%rax, win+472(%rip)
	movabsq	$4607105360961759388, %rax # imm = 0x3FEFB9EA92EC689C
	movq	%rax, win+480(%rip)
	movabsq	$4606496786581982537, %rax # imm = 0x3FED906BCF328D49
	movq	%rax, win+488(%rip)
	movabsq	$4605321111160693912, %rax # imm = 0x3FE963268B572498
	movq	%rax, win+496(%rip)
	movabsq	$4603658455034958829, %rax # imm = 0x3FE37AF93F9513ED
	movq	%rax, win+504(%rip)
	movabsq	$4600565431771507055, %rax # imm = 0x3FD87DE2A6AEA96F
	movq	%rax, win+512(%rip)
	movabsq	$4593870721975689974, %rax # imm = 0x3FC0B5150F6DA2F6
	movq	%rax, win+520(%rip)
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, win+560(%rip)
	movapd	%xmm0, win+544(%rip)
	movapd	%xmm0, win+528(%rip)
	movapd	%xmm0, win+864(%rip)
	movapd	%xmm0, win+880(%rip)
	movapd	%xmm0, win+896(%rip)
	movq	%rax, win+912(%rip)
	movq	win+512(%rip), %rax
	movq	%rax, win+920(%rip)
	movq	win+504(%rip), %rax
	movq	%rax, win+928(%rip)
	movq	win+496(%rip), %rax
	movq	%rax, win+936(%rip)
	movq	win+488(%rip), %rax
	movq	%rax, win+944(%rip)
	movq	win+480(%rip), %rax
	movq	%rax, win+952(%rip)
	movq	win+472(%rip), %rax
	movq	%rax, win+960(%rip)
	movq	win+464(%rip), %rax
	movq	%rax, win+968(%rip)
	movq	win+456(%rip), %rax
	movq	%rax, win+976(%rip)
	movq	win+448(%rip), %rax
	movq	%rax, win+984(%rip)
	movq	win+440(%rip), %rax
	movq	%rax, win+992(%rip)
	movq	win+432(%rip), %rax
	movq	%rax, win+1000(%rip)
	movq	win+424(%rip), %rax
	movq	%rax, win+1008(%rip)
	movq	win+416(%rip), %rax
	movq	%rax, win+1016(%rip)
	movq	win+408(%rip), %rax
	movq	%rax, win+1024(%rip)
	movq	win+400(%rip), %rax
	movq	%rax, win+1032(%rip)
	movq	win+392(%rip), %rax
	movq	%rax, win+1040(%rip)
	movq	win+384(%rip), %rax
	movq	%rax, win+1048(%rip)
	movq	win+376(%rip), %rax
	movq	%rax, win+1056(%rip)
	movq	win+368(%rip), %rax
	movq	%rax, win+1064(%rip)
	movq	win+360(%rip), %rax
	movq	%rax, win+1072(%rip)
	movq	win+352(%rip), %rax
	movq	%rax, win+1080(%rip)
	movq	win+344(%rip), %rax
	movq	%rax, win+1088(%rip)
	movq	win+336(%rip), %rax
	movq	%rax, win+1096(%rip)
	movq	win+328(%rip), %rax
	movq	%rax, win+1104(%rip)
	movq	win+320(%rip), %rax
	movq	%rax, win+1112(%rip)
	movq	win+312(%rip), %rax
	movq	%rax, win+1120(%rip)
	movq	win+304(%rip), %rax
	movq	%rax, win+1128(%rip)
	movq	win+296(%rip), %rax
	movq	%rax, win+1136(%rip)
	movq	win+288(%rip), %rax
	movq	%rax, win+1144(%rip)
	movl	$cos_l, %ebx
	movl	$12, %r14d
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader225
                                        # =>This Inner Loop Header: Depth=1
	movl	all-4(,%r14,4), %eax
	leal	1(%rax,%rax), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI1_10(%rip), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	mulsd	.LCPI1_11(%rip), %xmm0
	callq	cos
	movsd	.LCPI1_12(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_13(%rip), %xmm0
	callq	cos
	mulsd	.LCPI1_12(%rip), %xmm0
	movsd	%xmm0, 8(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_14(%rip), %xmm0
	callq	cos
	mulsd	.LCPI1_12(%rip), %xmm0
	movsd	%xmm0, 16(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_15(%rip), %xmm0
	callq	cos
	mulsd	.LCPI1_12(%rip), %xmm0
	movsd	%xmm0, 24(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_16(%rip), %xmm0
	callq	cos
	mulsd	.LCPI1_12(%rip), %xmm0
	movsd	%xmm0, 32(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_17(%rip), %xmm0
	callq	cos
	mulsd	.LCPI1_12(%rip), %xmm0
	movsd	%xmm0, 40(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_18(%rip), %xmm0
	callq	cos
	mulsd	.LCPI1_12(%rip), %xmm0
	movsd	%xmm0, 48(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_19(%rip), %xmm0
	callq	cos
	mulsd	.LCPI1_12(%rip), %xmm0
	movsd	%xmm0, 56(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_20(%rip), %xmm0
	callq	cos
	mulsd	.LCPI1_12(%rip), %xmm0
	movsd	%xmm0, 64(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_21(%rip), %xmm0
	callq	cos
	mulsd	.LCPI1_12(%rip), %xmm0
	movsd	%xmm0, 72(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_22(%rip), %xmm0
	callq	cos
	mulsd	.LCPI1_12(%rip), %xmm0
	movsd	%xmm0, 80(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_23(%rip), %xmm0
	callq	cos
	mulsd	.LCPI1_12(%rip), %xmm0
	movsd	%xmm0, 88(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_24(%rip), %xmm0
	callq	cos
	mulsd	.LCPI1_12(%rip), %xmm0
	movsd	%xmm0, 96(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_25(%rip), %xmm0
	callq	cos
	mulsd	.LCPI1_12(%rip), %xmm0
	movsd	%xmm0, 104(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_26(%rip), %xmm0
	callq	cos
	mulsd	.LCPI1_12(%rip), %xmm0
	movsd	%xmm0, 112(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_27(%rip), %xmm0
	callq	cos
	mulsd	.LCPI1_12(%rip), %xmm0
	movsd	%xmm0, 120(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_28(%rip), %xmm0
	callq	cos
	mulsd	.LCPI1_12(%rip), %xmm0
	movsd	%xmm0, 128(%rbx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_29(%rip), %xmm0
	callq	cos
	mulsd	.LCPI1_12(%rip), %xmm0
	movsd	%xmm0, 136(%rbx)
	addq	$144, %rbx
	decq	%r14
	jg	.LBB1_3
# BB#4:                                 # %.preheader223.preheader
	movaps	.LCPI1_30(%rip), %xmm0  # xmm0 = [-6.764016e-02,4.252038e-02]
	movaps	%xmm0, cos_l+1728(%rip)
	movaps	.LCPI1_31(%rip), %xmm0  # xmm0 = [-1.450291e-02,8.815037e-02]
	movaps	%xmm0, cos_l+1744(%rip)
	movaps	.LCPI1_32(%rip), %xmm0  # xmm0 = [-1.026533e-01,1.101605e-01]
	movaps	%xmm0, cos_l+1760(%rip)
	movaps	.LCPI1_33(%rip), %xmm0  # xmm0 = [1.450291e-02,1.026533e-01]
	movaps	%xmm0, cos_l+1776(%rip)
	movaps	.LCPI1_34(%rip), %xmm0  # xmm0 = [-6.764016e-02,-1.101605e-01]
	movaps	%xmm0, cos_l+1792(%rip)
	movaps	.LCPI1_35(%rip), %xmm0  # xmm0 = [4.252038e-02,8.815037e-02]
	movaps	%xmm0, cos_l+1808(%rip)
	movaps	.LCPI1_36(%rip), %xmm0  # xmm0 = [1.101605e-01,4.252038e-02]
	movaps	%xmm0, cos_l+1824(%rip)
	movaps	.LCPI1_37(%rip), %xmm0  # xmm0 = [-8.815037e-02,1.450291e-02]
	movaps	%xmm0, cos_l+1840(%rip)
	movaps	.LCPI1_38(%rip), %xmm0  # xmm0 = [-1.026533e-01,-6.764016e-02]
	movaps	%xmm0, cos_l+1856(%rip)
	movaps	.LCPI1_39(%rip), %xmm0  # xmm0 = [-8.815037e-02,-1.026533e-01]
	movaps	%xmm0, cos_l+1872(%rip)
	movaps	.LCPI1_40(%rip), %xmm0  # xmm0 = [-1.101605e-01,-6.764016e-02]
	movaps	%xmm0, cos_l+1888(%rip)
	movaps	.LCPI1_41(%rip), %xmm0  # xmm0 = [-4.252038e-02,-1.450291e-02]
	movaps	%xmm0, cos_l+1904(%rip)
	movaps	.LCPI1_42(%rip), %xmm0  # xmm0 = [-1.026533e-01,-4.252038e-02]
	movaps	%xmm0, cos_l+1920(%rip)
	movaps	.LCPI1_43(%rip), %xmm0  # xmm0 = [4.252038e-02,-1.026533e-01]
	movaps	%xmm0, cos_l+1936(%rip)
	movsd	enwindow+1984(%rip), %xmm3 # xmm3 = mem[0],zero
	movsd	enwindow(%rip), %xmm0   # xmm0 = mem[0],zero
	movupd	enwindow+8(%rip), %xmm1
	movaps	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	divpd	%xmm2, %xmm1
	movapd	%xmm1, enwindow(%rip)
	movupd	enwindow+24(%rip), %xmm1
	divpd	%xmm2, %xmm1
	movapd	%xmm1, enwindow+16(%rip)
	movupd	enwindow+40(%rip), %xmm1
	divpd	%xmm2, %xmm1
	movapd	%xmm1, enwindow+32(%rip)
	movsd	enwindow+56(%rip), %xmm1 # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	divsd	%xmm3, %xmm0
	movsd	%xmm0, 168(%rsp)
	movsd	%xmm1, enwindow+48(%rip)
	leaq	176(%rsp), %rax
	movl	$enwindow+64, %ecx
	movl	$enwindow+56, %edx
	movl	$15, %esi
	.p2align	4, 0x90
.LBB1_5:                                # %.preheader220
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rcx), %xmm1          # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdx)
	movsd	16(%rcx), %xmm1         # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rdx)
	movsd	24(%rcx), %xmm1         # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 16(%rdx)
	movsd	32(%rcx), %xmm1         # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 24(%rdx)
	movsd	40(%rcx), %xmm1         # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 32(%rdx)
	movsd	48(%rcx), %xmm1         # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 40(%rdx)
	movsd	56(%rcx), %xmm1         # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 48(%rdx)
	movsd	64(%rcx), %xmm1         # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 56(%rdx)
	movsd	72(%rcx), %xmm1         # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 64(%rdx)
	movsd	80(%rcx), %xmm1         # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 72(%rdx)
	movsd	88(%rcx), %xmm1         # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 80(%rdx)
	movsd	96(%rcx), %xmm1         # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 88(%rdx)
	movsd	104(%rcx), %xmm1        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 96(%rdx)
	movsd	112(%rcx), %xmm1        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 104(%rdx)
	movsd	120(%rcx), %xmm1        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	divsd	%xmm3, %xmm0
	movsd	%xmm0, (%rax)
	movsd	%xmm0, 40(%rsp,%rsi,8)
	movsd	%xmm1, 112(%rdx)
	addq	$8, %rax
	subq	$-128, %rcx
	addq	$120, %rdx
	decq	%rsi
	jg	.LBB1_5
# BB#6:                                 # %.preheader219
	movupd	enwindow+1992(%rip), %xmm0
	movapd	%xmm3, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	divpd	%xmm1, %xmm0
	movapd	%xmm0, enwindow+1856(%rip)
	movupd	enwindow+2008(%rip), %xmm0
	divpd	%xmm1, %xmm0
	movapd	%xmm0, enwindow+1872(%rip)
	movupd	enwindow+2024(%rip), %xmm0
	divpd	%xmm1, %xmm0
	movapd	%xmm0, enwindow+1888(%rip)
	movsd	enwindow+2040(%rip), %xmm0 # xmm0 = mem[0],zero
	movapd	%xmm3, 32(%rsp)         # 16-byte Spill
	divsd	%xmm3, %xmm0
	movsd	%xmm0, enwindow+1904(%rip)
	movl	$mm, %r14d
	movl	$15, %r15d
	movsd	.LCPI1_44(%rip), %xmm1  # xmm1 = mem[0],zero
	movsd	.LCPI1_45(%rip), %xmm2  # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB1_7:                                # %.preheader217
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_8 Depth 2
	leaq	1(%r15,%r15), %r12
	movl	$1, %ebp
	movq	%r14, %rbx
	jmp	.LBB1_8
	.p2align	4, 0x90
.LBB1_15:                               #   in Loop: Header=BB1_8 Depth=2
	imull	%r12d, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI1_44(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	.LCPI1_45(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	callq	cos
	mulsd	48(%rsp,%rbp,8), %xmm0
	movsd	%xmm0, 8(%rbx)
	addq	$16, %rbx
	addq	$2, %rbp
	movsd	.LCPI1_44(%rip), %xmm1  # xmm1 = mem[0],zero
	movsd	.LCPI1_45(%rip), %xmm2  # xmm2 = mem[0],zero
.LBB1_8:                                #   Parent Loop BB1_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %eax
	imull	%r12d, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	%xmm2, %xmm0
	callq	cos
	mulsd	40(%rsp,%rbp,8), %xmm0
	movsd	%xmm0, (%rbx)
	leaq	1(%rbp), %rax
	cmpq	$32, %rax
	jne	.LBB1_15
# BB#9:                                 #   in Loop: Header=BB1_7 Depth=1
	addq	$248, %r14
	testq	%r15, %r15
	leaq	-1(%r15), %r15
	movsd	.LCPI1_44(%rip), %xmm1  # xmm1 = mem[0],zero
	movsd	.LCPI1_45(%rip), %xmm2  # xmm2 = mem[0],zero
	jg	.LBB1_7
# BB#10:                                # %.preheader216288
	movq	win+136(%rip), %rax
	movq	win+72(%rip), %rcx
	movq	%rcx, win+136(%rip)
	movq	%rax, win+72(%rip)
	movq	win+280(%rip), %rax
	movq	win+216(%rip), %rcx
	movq	%rcx, win+280(%rip)
	movq	%rax, win+216(%rip)
	movq	win+424(%rip), %rax
	movq	win+360(%rip), %rcx
	movq	%rcx, win+424(%rip)
	movq	%rax, win+360(%rip)
	movq	win+568(%rip), %rax
	movq	win+504(%rip), %rcx
	movq	%rcx, win+568(%rip)
	movq	%rax, win+504(%rip)
	movq	win+1000(%rip), %rax
	movq	win+936(%rip), %rcx
	movq	%rcx, win+1000(%rip)
	movq	%rax, win+936(%rip)
	movq	win+1144(%rip), %rax
	movq	win+1080(%rip), %rcx
	movq	%rcx, win+1144(%rip)
	movq	%rax, win+1080(%rip)
	movq	win+128(%rip), %rax
	movq	win+80(%rip), %rcx
	movq	%rcx, win+128(%rip)
	movq	%rax, win+80(%rip)
	movq	win+272(%rip), %rax
	movq	win+224(%rip), %rcx
	movq	%rcx, win+272(%rip)
	movq	%rax, win+224(%rip)
	movq	win+416(%rip), %rax
	movq	win+368(%rip), %rcx
	movq	%rcx, win+416(%rip)
	movq	%rax, win+368(%rip)
	movq	win+560(%rip), %rax
	movq	win+512(%rip), %rcx
	movq	%rcx, win+560(%rip)
	movq	%rax, win+512(%rip)
	movq	win+992(%rip), %rax
	movq	win+944(%rip), %rcx
	movq	%rcx, win+992(%rip)
	movq	%rax, win+944(%rip)
	movq	win+1136(%rip), %rax
	movq	win+1088(%rip), %rcx
	movq	%rcx, win+1136(%rip)
	movq	%rax, win+1088(%rip)
	movq	win+120(%rip), %rax
	movq	win+88(%rip), %rcx
	movq	%rcx, win+120(%rip)
	movq	%rax, win+88(%rip)
	movq	win+264(%rip), %rax
	movq	win+232(%rip), %rcx
	movq	%rcx, win+264(%rip)
	movq	%rax, win+232(%rip)
	movq	win+408(%rip), %rax
	movq	win+376(%rip), %rcx
	movq	%rcx, win+408(%rip)
	movq	%rax, win+376(%rip)
	movq	win+552(%rip), %rax
	movq	win+520(%rip), %rcx
	movq	%rcx, win+552(%rip)
	movq	%rax, win+520(%rip)
	movq	win+984(%rip), %rax
	movq	win+952(%rip), %rcx
	movq	%rcx, win+984(%rip)
	movq	%rax, win+952(%rip)
	movq	win+1128(%rip), %rax
	movq	win+1096(%rip), %rcx
	movq	%rcx, win+1128(%rip)
	movq	%rax, win+1096(%rip)
	movq	win+112(%rip), %rax
	movq	win+96(%rip), %rcx
	movq	%rcx, win+112(%rip)
	movq	%rax, win+96(%rip)
	movq	win+256(%rip), %rax
	movq	win+240(%rip), %rcx
	movq	%rcx, win+256(%rip)
	movq	%rax, win+240(%rip)
	movq	win+400(%rip), %rax
	movq	win+384(%rip), %rcx
	movq	%rcx, win+400(%rip)
	movq	%rax, win+384(%rip)
	movq	win+544(%rip), %rax
	movq	win+528(%rip), %rcx
	movq	%rcx, win+544(%rip)
	movq	%rax, win+528(%rip)
	movq	win+976(%rip), %rax
	movq	win+960(%rip), %rcx
	movq	%rcx, win+976(%rip)
	movq	%rax, win+960(%rip)
	movq	win+1120(%rip), %rax
	movq	win+1104(%rip), %rcx
	movq	%rcx, win+1120(%rip)
	movq	%rax, win+1104(%rip)
	movsd	.LCPI1_46(%rip), %xmm0  # xmm0 = mem[0],zero
	mulsd	32(%rsp), %xmm0         # 16-byte Folded Reload
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movq	$-288, %rax             # imm = 0xFEE0
	.p2align	4, 0x90
.LBB1_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movapd	win+288(%rax), %xmm1
	mulpd	%xmm0, %xmm1
	movapd	%xmm1, win+288(%rax)
	movapd	win+576(%rax), %xmm1
	mulpd	%xmm0, %xmm1
	movapd	%xmm1, win+576(%rax)
	movapd	win+1152(%rax), %xmm1
	mulpd	%xmm0, %xmm1
	movapd	%xmm1, win+1152(%rax)
	movapd	win+304(%rax), %xmm1
	mulpd	%xmm0, %xmm1
	movapd	%xmm1, win+304(%rax)
	movapd	win+592(%rax), %xmm1
	mulpd	%xmm0, %xmm1
	movapd	%xmm1, win+592(%rax)
	movapd	win+1168(%rax), %xmm1
	mulpd	%xmm0, %xmm1
	movapd	%xmm1, win+1168(%rax)
	addq	$32, %rax
	jne	.LBB1_11
# BB#12:                                # %.preheader.preheader
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_13:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	addsd	.LCPI1_8(%rip), %xmm0
	mulsd	.LCPI1_47(%rip), %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	callq	cos
	mulsd	32(%rsp), %xmm0         # 16-byte Folded Reload
	mulsd	.LCPI1_46(%rip), %xmm0
	mulsd	.LCPI1_48(%rip), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	tan
	movsd	%xmm0, win+576(%rbx,%rbx)
	leal	14(%rbx), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	leal	38(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movapd	%xmm1, %xmm0
	movsd	.LCPI1_49(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, cos_s(%rbx,%rbx)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_49(%rip), %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, cos_s+24(%rbx,%rbx)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	.LCPI1_50(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, cos_s+48(%rbx,%rbx)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_50(%rip), %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, cos_s+72(%rbx,%rbx)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	.LCPI1_51(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, cos_s+96(%rbx,%rbx)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_51(%rip), %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, cos_s+120(%rbx,%rbx)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	.LCPI1_52(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, cos_s+144(%rbx,%rbx)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_52(%rip), %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, cos_s+168(%rbx,%rbx)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	.LCPI1_53(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, cos_s+192(%rbx,%rbx)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_53(%rip), %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, cos_s+216(%rbx,%rbx)
	movsd	.LCPI1_54(%rip), %xmm0  # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, cos_s+240(%rbx,%rbx)
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI1_54(%rip), %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, cos_s+264(%rbx,%rbx)
	incl	%ebp
	addq	$4, %rbx
	cmpq	$12, %rbx
	jne	.LBB1_13
# BB#14:
	addq	$304, %rsp              # imm = 0x130
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	mdct_init48, .Lfunc_end1-mdct_init48
	.cfi_endproc

	.p2align	4, 0x90
	.type	window_subband,@function
window_subband:                         # @window_subband
	.cfi_startproc
# BB#0:
	movswl	510(%rdi), %eax
	cvtsi2sdl	%eax, %xmm0
	movswl	446(%rdi), %eax
	movswl	574(%rdi), %ecx
	subl	%ecx, %eax
	cvtsi2sdl	%eax, %xmm1
	mulsd	enwindow(%rip), %xmm1
	addsd	%xmm0, %xmm1
	movswl	382(%rdi), %eax
	movswl	638(%rdi), %ecx
	addl	%eax, %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	mulsd	enwindow+8(%rip), %xmm0
	addsd	%xmm1, %xmm0
	movswl	318(%rdi), %eax
	movswl	702(%rdi), %ecx
	subl	%ecx, %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	enwindow+16(%rip), %xmm1
	addsd	%xmm0, %xmm1
	movswl	254(%rdi), %eax
	movswl	766(%rdi), %ecx
	addl	%eax, %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	mulsd	enwindow+24(%rip), %xmm0
	addsd	%xmm1, %xmm0
	movswl	190(%rdi), %eax
	movswl	830(%rdi), %ecx
	subl	%ecx, %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	enwindow+32(%rip), %xmm1
	addsd	%xmm0, %xmm1
	movswl	126(%rdi), %eax
	movswl	894(%rdi), %ecx
	addl	%eax, %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	mulsd	enwindow+40(%rip), %xmm0
	addsd	%xmm1, %xmm0
	movswl	62(%rdi), %eax
	movswl	958(%rdi), %ecx
	subl	%ecx, %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	enwindow+48(%rip), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, win+728(%rip)
	movl	$enwindow+56, %r8d
	movl	$510, %edx              # imm = 0x1FE
	movl	$960, %ecx              # imm = 0x3C0
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movswl	-448(%rdi,%rcx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movswl	-512(%rdi,%rdx,2), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movswl	-320(%rdi,%rcx), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movsd	(%r8), %xmm3            # xmm3 = mem[0],zero
	movsd	8(%r8), %xmm4           # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm2
	addsd	%xmm0, %xmm2
	movswl	-640(%rdi,%rdx,2), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm3, %xmm0
	addsd	%xmm1, %xmm0
	movswl	-192(%rdi,%rcx), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm4, %xmm1
	addsd	%xmm2, %xmm1
	movswl	-768(%rdi,%rdx,2), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	mulsd	%xmm4, %xmm2
	addsd	%xmm0, %xmm2
	movsd	16(%r8), %xmm0          # xmm0 = mem[0],zero
	movswl	-64(%rdi,%rcx), %eax
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	mulsd	%xmm0, %xmm3
	addsd	%xmm1, %xmm3
	movswl	-896(%rdi,%rdx,2), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	movsd	24(%r8), %xmm0          # xmm0 = mem[0],zero
	movswl	-960(%rdi,%rcx), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	mulsd	%xmm0, %xmm2
	addsd	%xmm3, %xmm2
	movswl	(%rdi,%rdx,2), %eax
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	mulsd	%xmm0, %xmm3
	addsd	%xmm1, %xmm3
	movsd	32(%r8), %xmm0          # xmm0 = mem[0],zero
	movswl	-832(%rdi,%rcx), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	movswl	-128(%rdi,%rdx,2), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	mulsd	%xmm0, %xmm2
	addsd	%xmm3, %xmm2
	movsd	40(%r8), %xmm0          # xmm0 = mem[0],zero
	movswl	-704(%rdi,%rcx), %eax
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	mulsd	%xmm0, %xmm3
	addsd	%xmm1, %xmm3
	movswl	-256(%rdi,%rdx,2), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	movsd	48(%r8), %xmm2          # xmm2 = mem[0],zero
	movswl	-576(%rdi,%rcx), %eax
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%eax, %xmm4
	mulsd	%xmm2, %xmm4
	addsd	%xmm3, %xmm4
	movswl	-384(%rdi,%rdx,2), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	movsd	56(%r8), %xmm1          # xmm1 = mem[0],zero
	movswl	-960(%rdi,%rdx,2), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	mulsd	%xmm1, %xmm2
	addsd	%xmm4, %xmm2
	movswl	(%rdi,%rcx), %eax
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	mulsd	%xmm1, %xmm3
	subsd	%xmm3, %xmm0
	movsd	64(%r8), %xmm1          # xmm1 = mem[0],zero
	movswl	-832(%rdi,%rdx,2), %eax
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	mulsd	%xmm1, %xmm3
	addsd	%xmm2, %xmm3
	movswl	-128(%rdi,%rcx), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	mulsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm0
	movsd	72(%r8), %xmm1          # xmm1 = mem[0],zero
	movswl	-704(%rdi,%rdx,2), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	mulsd	%xmm1, %xmm2
	addsd	%xmm3, %xmm2
	movswl	-256(%rdi,%rcx), %eax
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	mulsd	%xmm1, %xmm3
	subsd	%xmm3, %xmm0
	movsd	80(%r8), %xmm3          # xmm3 = mem[0],zero
	movswl	-576(%rdi,%rdx,2), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm3, %xmm1
	addsd	%xmm2, %xmm1
	movswl	-384(%rdi,%rcx), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	mulsd	%xmm3, %xmm2
	subsd	%xmm2, %xmm0
	movsd	88(%r8), %xmm2          # xmm2 = mem[0],zero
	movswl	-448(%rdi,%rdx,2), %eax
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	mulsd	%xmm2, %xmm3
	subsd	%xmm3, %xmm1
	movswl	-512(%rdi,%rcx), %eax
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	mulsd	%xmm2, %xmm3
	addsd	%xmm0, %xmm3
	movsd	96(%r8), %xmm0          # xmm0 = mem[0],zero
	movswl	-320(%rdi,%rdx,2), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	movswl	-640(%rdi,%rcx), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	mulsd	%xmm0, %xmm2
	addsd	%xmm3, %xmm2
	movsd	104(%r8), %xmm0         # xmm0 = mem[0],zero
	movswl	-192(%rdi,%rdx,2), %eax
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	mulsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm1
	movswl	-768(%rdi,%rcx), %eax
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	mulsd	%xmm0, %xmm3
	addsd	%xmm2, %xmm3
	movsd	112(%r8), %xmm2         # xmm2 = mem[0],zero
	movswl	-64(%rdi,%rdx,2), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm1
	movswl	-896(%rdi,%rcx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	movsd	%xmm1, win-3104(,%rcx,4)
	movsd	%xmm0, win-3360(,%rdx,8)
	leaq	-1(%rdx), %rax
	addq	$-496, %rdx             # imm = 0xFE10
	addq	$2, %rcx
	addq	$120, %r8
	testq	%rdx, %rdx
	movq	%rax, %rdx
	jg	.LBB2_1
# BB#2:
	movswl	478(%rdi), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movswl	350(%rdi), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	mulsd	enwindow+1856(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movswl	222(%rdi), %eax
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	mulsd	enwindow+1864(%rip), %xmm3
	addsd	%xmm2, %xmm3
	movswl	94(%rdi), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	enwindow+1872(%rip), %xmm1
	addsd	%xmm3, %xmm1
	movswl	606(%rdi), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	mulsd	enwindow+1880(%rip), %xmm2
	subsd	%xmm2, %xmm1
	movswl	734(%rdi), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	mulsd	enwindow+1888(%rip), %xmm2
	subsd	%xmm2, %xmm1
	movswl	862(%rdi), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	mulsd	enwindow+1896(%rip), %xmm2
	subsd	%xmm2, %xmm1
	movswl	990(%rdi), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	mulsd	enwindow+1904(%rip), %xmm2
	subsd	%xmm2, %xmm1
	movl	$mm, %eax
	movl	$16, %ecx
	movl	$128, %edx
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	movsd	(%rax), %xmm2           # xmm2 = mem[0],zero
	movsd	8(%rax), %xmm3          # xmm3 = mem[0],zero
	mulsd	%xmm0, %xmm2
	movsd	16(%rax), %xmm4         # xmm4 = mem[0],zero
	mulsd	win+616(%rip), %xmm3
	mulsd	win+624(%rip), %xmm4
	addsd	%xmm1, %xmm3
	addsd	%xmm2, %xmm4
	movsd	24(%rax), %xmm2         # xmm2 = mem[0],zero
	mulsd	win+632(%rip), %xmm2
	addsd	%xmm3, %xmm2
	movsd	32(%rax), %xmm3         # xmm3 = mem[0],zero
	mulsd	win+640(%rip), %xmm3
	movsd	40(%rax), %xmm5         # xmm5 = mem[0],zero
	addsd	%xmm4, %xmm3
	mulsd	win+648(%rip), %xmm5
	movsd	48(%rax), %xmm4         # xmm4 = mem[0],zero
	mulsd	win+656(%rip), %xmm4
	addsd	%xmm2, %xmm5
	addsd	%xmm3, %xmm4
	movsd	56(%rax), %xmm2         # xmm2 = mem[0],zero
	mulsd	win+664(%rip), %xmm2
	addsd	%xmm5, %xmm2
	movsd	64(%rax), %xmm3         # xmm3 = mem[0],zero
	mulsd	win+672(%rip), %xmm3
	movsd	72(%rax), %xmm5         # xmm5 = mem[0],zero
	addsd	%xmm4, %xmm3
	mulsd	win+680(%rip), %xmm5
	movsd	80(%rax), %xmm4         # xmm4 = mem[0],zero
	mulsd	win+688(%rip), %xmm4
	addsd	%xmm2, %xmm5
	addsd	%xmm3, %xmm4
	movsd	88(%rax), %xmm2         # xmm2 = mem[0],zero
	mulsd	win+696(%rip), %xmm2
	addsd	%xmm5, %xmm2
	movsd	96(%rax), %xmm3         # xmm3 = mem[0],zero
	mulsd	win+704(%rip), %xmm3
	movsd	104(%rax), %xmm5        # xmm5 = mem[0],zero
	addsd	%xmm4, %xmm3
	mulsd	win+712(%rip), %xmm5
	movsd	112(%rax), %xmm4        # xmm4 = mem[0],zero
	mulsd	win+720(%rip), %xmm4
	addsd	%xmm2, %xmm5
	addsd	%xmm3, %xmm4
	movsd	120(%rax), %xmm2        # xmm2 = mem[0],zero
	mulsd	win+728(%rip), %xmm2
	addsd	%xmm5, %xmm2
	movsd	128(%rax), %xmm3        # xmm3 = mem[0],zero
	mulsd	win+736(%rip), %xmm3
	movsd	136(%rax), %xmm5        # xmm5 = mem[0],zero
	addsd	%xmm4, %xmm3
	mulsd	win+744(%rip), %xmm5
	movsd	144(%rax), %xmm4        # xmm4 = mem[0],zero
	mulsd	win+752(%rip), %xmm4
	addsd	%xmm2, %xmm5
	addsd	%xmm3, %xmm4
	movsd	152(%rax), %xmm2        # xmm2 = mem[0],zero
	mulsd	win+760(%rip), %xmm2
	addsd	%xmm5, %xmm2
	movsd	160(%rax), %xmm3        # xmm3 = mem[0],zero
	mulsd	win+768(%rip), %xmm3
	movsd	168(%rax), %xmm5        # xmm5 = mem[0],zero
	addsd	%xmm4, %xmm3
	mulsd	win+776(%rip), %xmm5
	movsd	176(%rax), %xmm4        # xmm4 = mem[0],zero
	mulsd	win+784(%rip), %xmm4
	addsd	%xmm2, %xmm5
	addsd	%xmm3, %xmm4
	movsd	184(%rax), %xmm2        # xmm2 = mem[0],zero
	mulsd	win+792(%rip), %xmm2
	addsd	%xmm5, %xmm2
	movsd	192(%rax), %xmm3        # xmm3 = mem[0],zero
	mulsd	win+800(%rip), %xmm3
	movsd	200(%rax), %xmm5        # xmm5 = mem[0],zero
	addsd	%xmm4, %xmm3
	mulsd	win+808(%rip), %xmm5
	movsd	208(%rax), %xmm4        # xmm4 = mem[0],zero
	mulsd	win+816(%rip), %xmm4
	addsd	%xmm2, %xmm5
	addsd	%xmm3, %xmm4
	movsd	216(%rax), %xmm2        # xmm2 = mem[0],zero
	mulsd	win+824(%rip), %xmm2
	addsd	%xmm5, %xmm2
	movsd	224(%rax), %xmm3        # xmm3 = mem[0],zero
	mulsd	win+832(%rip), %xmm3
	movsd	232(%rax), %xmm5        # xmm5 = mem[0],zero
	addsd	%xmm4, %xmm3
	mulsd	win+840(%rip), %xmm5
	movsd	240(%rax), %xmm4        # xmm4 = mem[0],zero
	mulsd	win+848(%rip), %xmm4
	addsd	%xmm2, %xmm5
	addsd	%xmm3, %xmm4
	movapd	%xmm5, %xmm2
	addsd	%xmm4, %xmm2
	subsd	%xmm4, %xmm5
	movsd	%xmm2, -8(%rsi,%rcx,8)
	movsd	%xmm5, (%rsi,%rdx)
	addq	$8, %rdx
	addq	$248, %rax
	decq	%rcx
	jg	.LBB2_3
# BB#4:
	retq
.Lfunc_end2:
	.size	window_subband, .Lfunc_end2-window_subband
	.cfi_endproc

	.type	mdct_sub48.init,@object # @mdct_sub48.init
	.local	mdct_sub48.init
	.comm	mdct_sub48.init,4,4
	.type	sb_sample,@object       # @sb_sample
	.local	sb_sample
	.comm	sb_sample,18432,16
	.type	win,@object             # @win
	.local	win
	.comm	win,1152,16
	.type	ca,@object              # @ca
	.local	ca
	.comm	ca,64,16
	.type	cs,@object              # @cs
	.local	cs
	.comm	cs,64,16
	.type	cos_l,@object           # @cos_l
	.local	cos_l
	.comm	cos_l,1952,16
	.type	all,@object             # @all
	.section	.rodata,"a",@progbits
	.p2align	4
all:
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	17                      # 0x11
	.size	all, 48

	.type	enwindow,@object        # @enwindow
	.data
	.p2align	4
enwindow:
	.quad	4585317393177624140     # double 0.035780907000000001
	.quad	4580809670270607606     # double 0.017876148000000001
	.quad	4569374610865055863     # double 0.0031347269999999999
	.quad	4567812206229657802     # double 0.0024571419999999998
	.quad	4562098041328293126     # double 9.7131699999999997E-4
	.quad	4552206827980679788     # double 2.1886799999999999E-4
	.quad	4547122672423821505     # double 1.01566E-4
	.quad	4534280235972988662     # double 1.3828E-5
	.quad	4585314232010973696     # double 0.035758972
	.quad	4569990337817963187     # double 0.0034017560000000001
	.quad	4562179407610559250     # double 9.8371500000000002E-4
	.quad	4546946764272334611     # double 9.9182E-5
	.quad	-4710763672232251670    # double -4.7700000000000005E-7
	.quad	4547298654362284694     # double 1.0395099999999999E-4
	.quad	4561935313375446897     # double 9.5367399999999994E-4
	.quad	4568698413179231911     # double 0.0028414730000000002
	.quad	4533436112964175713     # double 1.2398000000000001E-5
	.quad	4551186501672474766     # double 1.9121200000000001E-4
	.quad	4567410883477276196     # double 0.0022830960000000001
	.quad	4580555545618405174     # double 0.016994476000000001
	.quad	4581063519951031189     # double 0.018756866000000001
	.quad	4568212429094924014     # double 0.002630711
	.quad	4553200786974074452     # double 2.4747799999999998E-4
	.quad	4534843378176070867     # double 1.4782E-5
	.quad	4585304886141026977     # double 0.035694122000000002
	.quad	4570546691619226267     # double 0.0036430360000000001
	.quad	4562216789937424622     # double 9.9182099999999994E-4
	.quad	4546735659733155079     # double 9.6321000000000002E-5
	.quad	-4710763672232251670    # double -4.7700000000000005E-7
	.quad	4547439366126078951     # double 1.05858E-4
	.quad	4561724208836267365     # double 9.3078599999999996E-4
	.quad	4567960640261689916     # double 0.0025215150000000002
	.quad	4532872970761093507     # double 1.1443999999999999E-5
	.quad	4550236494352678724     # double 1.6546200000000001E-4
	.quad	4567011760499125379     # double 0.002110004
	.quad	4580301420966202742     # double 0.016112804000000001
	.quad	4581316407806689554     # double 0.019634247000000001
	.quad	4568610452185959436     # double 0.0028033260000000001
	.quad	4553746146515869602     # double 2.7704200000000002E-4
	.quad	4535546948524257195     # double 1.6688999999999999E-5
	.quad	4585289355567783982     # double 0.035586356999999999
	.quad	4571043669963002094     # double 0.0038585659999999999
	.quad	4562232183745354133     # double 9.9515900000000002E-4
	.quad	4546524555193975547     # double 9.3460000000000003E-5
	.quad	-4710763672232251670    # double -4.7700000000000005E-7
	.quad	4547544881502180569     # double 1.0728799999999999E-4
	.quad	4561464727710754530     # double 9.0265300000000001E-4
	.quad	4567161294418272887     # double 0.0021748539999999999
	.quad	4532028847752280558     # double 1.0013999999999999E-5
	.quad	4549304122120217148     # double 1.4019E-4
	.quad	4566577452662496971     # double 0.0019373890000000001
	.quad	4579935146451100431     # double 0.015233517
	.quad	4581567921091684051     # double 0.020506858999999999
	.quad	4569004075728533278     # double 0.0029740330000000001
	.quad	4554309104251511070     # double 3.0756000000000002E-4
	.quad	4535969305176568849     # double 1.8119999999999999E-5
	.quad	4585267571548300000     # double 0.0354352
	.quad	4571318548355208954     # double 0.0040493009999999999
	.quad	4562227784196892553     # double 9.9420499999999996E-4
	.quad	4546313450654796015     # double 9.0599000000000005E-5
	.quad	-4710763672232251670    # double -4.7700000000000005E-7
	.quad	4547615274277565845     # double 1.08242E-4
	.quad	4561152461227074775     # double 8.6879700000000003E-4
	.quad	4565946334207503146     # double 0.0018005370000000001
	.quad	4531465705549198353     # double 9.0599999999999997E-6
	.quad	4548213391507411804     # double 1.16348E-4
	.quad	4565790205577349287     # double 0.0017666820000000001
	.quad	4579430745598677945     # double 0.014358521000000001
	.quad	4581817372664797936     # double 0.021372318000000001
	.quad	4569391104560100769     # double 0.0031418800000000001
	.quad	4554889641734254783     # double 3.39031E-4
	.quad	4536391366680975324     # double 1.9550000000000001E-5
	.quad	4585239740167293979     # double 0.035242081000000001
	.quad	4571509862996761910     # double 0.0042152400000000003
	.quad	4562205795677956691     # double 9.8943699999999991E-4
	.quad	4546067149727923845     # double 8.7261000000000004E-5
	.quad	-4710763672232251670    # double -4.7700000000000005E-7
	.quad	4547650470665258483     # double 1.08719E-4
	.quad	4560787427831972173     # double 8.2921999999999997E-4
	.quad	4564096955880393395     # double 0.0013995170000000001
	.quad	4530902563346116148     # double 8.106E-6
	.quad	4546559751581668185     # double 9.3937000000000005E-5
	.quad	4565011748365752725     # double 0.001597881
	.quad	4578929642678219386     # double 0.013489246
	.quad	4582064213158934262     # double 0.022228718000000001
	.quad	4569771536374818899     # double 0.0033068659999999999
	.quad	4555487777410844815     # double 3.7145599999999999E-4
	.quad	4536954508884057529     # double 2.1458E-5
	.quad	4585205861424765919     # double 0.035007000000000003
	.quad	4571674240780281231     # double 0.0043578150000000001
	.quad	4562166213576860529     # double 9.8085399999999997E-4
	.quad	4545820848801051675     # double 8.3923000000000003E-5
	.quad	-4710763672232251670    # double -4.7700000000000005E-7
	.quad	4547650470665258483     # double 1.08719E-4
	.quad	4560369609078702652     # double 7.8392000000000002E-4
	.quad	4562098041328293126     # double 9.7131699999999997E-4
	.quad	4530620759354431115     # double 7.6290000000000001E-6
	.quad	4545011627032026184     # double 7.2955999999999997E-5
	.quad	4564248689573771693     # double 0.0014324190000000001
	.quad	4578432938729761655     # double 0.012627602
	.quad	4582307892342304956     # double 0.023074150000000002
	.quad	4570140971624226090     # double 0.0034670830000000001
	.quad	4556094712184358007     # double 4.0435800000000001E-4
	.quad	4537517355939234555     # double 2.3365000000000001E-5
	.quad	4585166004063660531     # double 0.034730433999999998
	.quad	4571811679399923908     # double 0.0044770239999999996
	.quad	4562076052809357264     # double 9.6893299999999995E-4
	.quad	4545574547874179505     # double 8.0585000000000002E-5
	.quad	-4706260072604881174    # double -9.540000000000001E-7
	.quad	4547615274277565845     # double 1.08242E-4
	.quad	4559890224317087125     # double 7.3194500000000001E-4
	.quad	4557897911393522235     # double 5.1593799999999997E-4
	.quad	4529495655539887422     # double 6.6760000000000001E-6
	.quad	4542935766498708455     # double 5.2929000000000001E-5
	.quad	4563498820203803362     # double 0.0012698169999999999
	.quad	4577941456939259041     # double 0.011775017
	.quad	4582547998333702497     # double 0.023907185000000001
	.quad	4570499412614165349     # double 0.0036225319999999999
	.quad	4556719226704973444     # double 4.3821300000000001E-4
	.quad	4538080202994411581     # double 2.5272000000000001E-5
	.quad	4585120236971037718     # double 0.034412861000000003
	.quad	4571923279895726842     # double 0.004573822
	.quad	4561939712923908477     # double 9.5415099999999997E-4
	.quad	4545293124346590992     # double 7.6771000000000001E-5
	.quad	-4706260072604881174    # double -9.540000000000001E-7
	.quad	4547509758901464227     # double 1.0681200000000001E-4
	.quad	4559358063420676715     # double 6.7424800000000001E-4
	.quad	4540050695725580281     # double 3.3379E-5
	.quad	4528932513336805217     # double 6.1990000000000003E-6
	.quad	4540191333702398243     # double 3.4332000000000003E-5
	.quad	4562766549027681351     # double 0.001111031
	.quad	4577456297193826938     # double 0.010933399
	.quad	4582783843415449387     # double 0.024725436999999999
	.quad	4570843559683290493     # double 0.003771782
	.quad	4557352558769256114     # double 4.7254600000000001E-4
	.quad	4538784130748264337     # double 2.7657E-5
	.quad	4585068766087501238     # double 0.034055710000000003
	.quad	4572010141001883922     # double 0.0046491620000000001
	.quad	4561768195097511126     # double 9.3555500000000002E-4
	.quad	4545046823419718822     # double 7.3433E-5
	.quad	-4706260072604881174    # double -9.540000000000001E-7
	.quad	4547404169738386313     # double 1.05381E-4
	.quad	4558768726841009842     # double 6.1035199999999999E-4
	.quad	-4665957921300545725    # double -4.7588300000000002E-4
	.quad	4527806228930640806     # double 5.2449999999999998E-6
	.quad	4535687734075027747     # double 1.7166000000000001E-5
	.quad	4561961701442844339     # double 9.56535E-4
	.quad	4576978010013483797     # double 0.010103703
	.quad	4583014878220448682     # double 0.025527000000000001
	.quad	4571162967362769783     # double 0.0039143559999999999
	.quad	4557818747191329911     # double 5.0735500000000002E-4
	.quad	4539487763354211914     # double 3.0040999999999999E-5
	.quad	4585011728898940516     # double 0.033659935000000002
	.quad	4572072263871316653     # double 0.0047030450000000003
	.quad	4561579079077267455     # double 9.1505099999999995E-4
	.quad	4544800522492846652     # double 7.0094999999999999E-5
	.quad	-4706260072604881174    # double -9.540000000000001E-7
	.quad	4547193065199206781     # double 1.0252E-4
	.quad	4558113415481163347     # double 5.3930299999999998E-4
	.quad	-4661062888681460140    # double -0.0010118480000000001
	.quad	4527243086727558601     # double 4.7679999999999999E-6
	.quad	4517111964249894634     # double 9.540000000000001E-7
	.quad	4560580713617882184     # double 8.06808E-4
	.quad	4576507693555962755     # double 0.009287834
	.quad	4583240828065151908     # double 0.026310921000000001
	.quad	4571317998411651257     # double 0.0040488240000000003
	.quad	4558139803548560789     # double 5.4216400000000004E-4
	.quad	4539909910174809730     # double 3.2425000000000002E-5
	.quad	4584949125405355552     # double 0.033225536
	.quad	4572111845972412815     # double 0.0047373770000000001
	.quad	4561363565766254307     # double 8.9168500000000002E-4
	.quad	4544519025178281844     # double 6.6279999999999996E-5
	.quad	-4704007503792552354    # double -1.4309999999999999E-6
	.quad	4546946764272334611     # double 9.9182E-5
	.quad	4557167833074101986     # double 4.6253199999999998E-4
	.quad	-4658472435469619200    # double -0.001573563
	.quad	4526681125116097113     # double 4.2919999999999997E-6
	.quad	-4689091800881787146    # double -1.3828E-5
	.quad	4559243712054163789     # double 6.6184999999999996E-4
	.quad	4576046173889521863     # double 0.0084872249999999992
	.quad	4583460730260102719     # double 0.027073860000000002
	.quad	4571463133935258690     # double 0.0041747090000000004
	.quad	4558460859905791667     # double 5.7697300000000005E-4
	.quad	4540261726477783518     # double 3.4808999999999998E-5
	.quad	4584881299321469907     # double 0.032754897999999998
	.quad	4572128888458093913     # double 0.0047521589999999997
	.quad	4561130472708138913     # double 8.6641300000000001E-4
	.quad	4544272798038385969     # double 6.2942999999999997E-5
	.quad	-4704007503792552354    # double -1.4309999999999999E-6
	.quad	4546665266957769803     # double 9.5366999999999997E-5
	.quad	4555619726971204060     # double 3.7860900000000002E-4
	.quad	-4656241527746518933    # double -0.0021615029999999999
	.quad	4526117982913014908     # double 3.8149999999999999E-6
	.quad	-4684728691657282022    # double -2.7180000000000001E-5
	.quad	4557955087076778698     # double 5.2213699999999999E-4
	.quad	4575531326991806887     # double 0.0077033040000000002
	.quad	4583674447895872442     # double 0.027815342
	.quad	4571596725255840494     # double 0.0042905809999999999
	.quad	4558781916263022544     # double 6.1178199999999995E-4
	.quad	4540683935556142582     # double 3.7669999999999997E-5
	.quad	4584808250503168394     # double 0.032248020000000002
	.quad	4572125040006111536     # double 0.0047488210000000003
	.quad	4560875391131087657     # double 8.3875699999999997E-4
	.quad	4543920970206197135     # double 5.9605000000000003E-5
	.quad	-4701759657346706403    # double -1.9069999999999999E-6
	.quad	4546278254267103377     # double 9.0122000000000002E-5
	.quad	4553957251055049134     # double 2.8848600000000002E-4
	.quad	-4654828654724425371    # double -0.002774239
	.quad	4524992055912516926     # double 3.3380000000000001E-6
	.quad	-4682406677771044713    # double -3.9576999999999997E-5
	.quad	4555795635122690954     # double 3.88145E-4
	.quad	4574647869760021266     # double 0.0069370270000000001
	.quad	4583881293543013956     # double 0.028532981999999998
	.quad	4571718221276917468     # double 0.0043959619999999998
	.quad	4559102972620253422     # double 6.4659099999999996E-4
	.quad	4541106144634501647     # double 4.0531000000000002E-5
	.quad	4584730253922229860     # double 0.031706810000000002
	.quad	4572101400503581077     # double 0.0047283170000000001
	.quad	4560607101685279625     # double 8.0966899999999994E-4
	.quad	4543357975577067519     # double 5.5789999999999999E-5
	.quad	-4701759657346706403    # double -1.9069999999999999E-6
	.quad	4545856045188744313     # double 8.4400000000000005E-5
	.quad	4551204099866321084     # double 1.9168899999999999E-4
	.quad	-4653359708212033750    # double -0.0034112930000000001
	.quad	4524992055912516926     # double 3.3380000000000001E-6
	.quad	-4680788086659041142    # double -5.0544999999999998E-5
	.quad	4553429489707100303     # double 2.5987599999999998E-4
	.quad	4573785852256535314     # double 0.0061893460000000001
	.quad	4584080717546199940     # double 0.029224872999999998
	.quad	4571826523264295722     # double 0.0044898990000000003
	.quad	4559419629429022721     # double 6.8092300000000005E-4
	.quad	4541528353712860711     # double 4.3392000000000001E-5
	.quad	4584630610663581580     # double 0.031132698
	.quad	4572058519894060234     # double 0.0046911239999999996
	.quad	4560325632040830928     # double 7.7915199999999997E-4
	.quad	4542935766498708455     # double 5.2929000000000001E-5
	.quad	-4700632549754587703    # double -2.384E-6
	.quad	4545363443334999973     # double 7.7724000000000003E-5
	.quad	4546137542503309120     # double 8.8214999999999995E-5
	.quad	-4652027100432169412    # double -0.0040721890000000004
	.quad	4523865771506352515     # double 2.8609999999999998E-6
	.quad	-4679310428671760712    # double -6.0557999999999999E-5
	.quad	4549198569850627382     # double 1.37329E-4
	.quad	4572947475408501325     # double 0.0054621699999999997
	.quad	4584272444645421169     # double 0.02989006
	.quad	4571919431443744464     # double 0.0045704839999999997
	.quad	4559727496364240896     # double 7.1430199999999999E-4
	.quad	4541950562791219775     # double 4.6252999999999999E-5
	.quad	4584455925761811073     # double 0.030526637999999998
	.quad	4571997496911742899     # double 0.0046381950000000003
	.quad	4560030963750997492     # double 7.4720400000000001E-4
	.quad	4542443164644964115     # double 4.9591E-5
	.quad	4572133836797191686     # double 0.0047564510000000001
	.quad	4536954508884057529     # double 2.1458E-5
	.quad	-4678606710749621794    # double -6.9617999999999997E-5
	.quad	4522739487100188105     # double 2.384E-6
	.size	enwindow, 2048

	.type	mm,@object              # @mm
	.local	mm
	.comm	mm,3968,16
	.type	cos_s,@object           # @cos_s
	.local	cos_s
	.comm	cos_s,288,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
