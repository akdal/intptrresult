	.text
	.file	"Main.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	55                      # 0x37
	.long	122                     # 0x7a
	.long	67                      # 0x43
	.long	111                     # 0x6f
.LCPI0_1:
	.long	110                     # 0x6e
	.long	46                      # 0x2e
	.long	115                     # 0x73
	.long	102                     # 0x66
.LCPI0_2:
	.zero	16
	.text
	.globl	_Z5Main2iPPKc
	.p2align	4, 0x90
	.type	_Z5Main2iPPKc,@function
_Z5Main2iPPKc:                          # @_Z5Main2iPPKc
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1384, %rsp             # imm = 0x568
.Lcfi6:
	.cfi_def_cfa_offset 1440
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 88(%rsp)
	movq	$8, 104(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 80(%rsp)
.Ltmp0:
.Lcfi13:
	.cfi_escape 0x2e, 0x00
	leaq	80(%rsp), %r14
	movq	%r14, %rdx
	callq	_Z18mySplitCommandLineiPPKcR13CObjectVectorI11CStringBaseIwEE
.Ltmp1:
# BB#1:
	movl	92(%rsp), %eax
	cmpl	$1, %eax
	jne	.LBB0_3
# BB#2:
	xorl	%ebx, %ebx
.Ltmp457:
.Lcfi14:
	.cfi_escape 0x2e, 0x00
	movl	$g_StdOut, %edi
	movl	$1, %esi
	callq	_ZL20ShowCopyrightAndHelpR13CStdOutStreamb
.Ltmp458:
	jmp	.LBB0_488
.LBB0_3:
	cmpl	$2, %eax
	movl	$1, %ebx
	cmovll	%eax, %ebx
	testl	%eax, %eax
	jle	.LBB0_8
# BB#4:                                 # %.lr.ph.i
	movq	96(%rsp), %rax
	movq	(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB0_8
# BB#5:
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_7
# BB#6:
.Lcfi15:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_7:                                # %_ZN11CStringBaseIwED2Ev.exit.i
.Lcfi16:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB0_8:                                # %._crit_edge.i
.Ltmp2:
.Lcfi17:
	.cfi_escape 0x2e, 0x00
	leaq	80(%rsp), %r15
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%ebx, %edx
	callq	_ZN17CBaseRecordVector6DeleteEii
.Ltmp3:
# BB#9:                                 # %_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii.exit
.Ltmp4:
.Lcfi18:
	.cfi_escape 0x2e, 0x00
	leaq	744(%rsp), %rdi
	callq	_ZN26CArchiveCommandLineOptionsC2Ev
.Ltmp5:
# BB#10:
.Ltmp6:
.Lcfi19:
	.cfi_escape 0x2e, 0x00
	leaq	1336(%rsp), %rdi
	callq	_ZN25CArchiveCommandLineParserC1Ev
.Ltmp7:
# BB#11:
.Ltmp9:
.Lcfi20:
	.cfi_escape 0x2e, 0x00
	leaq	1336(%rsp), %rdi
	leaq	80(%rsp), %rsi
	leaq	744(%rsp), %rdx
	callq	_ZN25CArchiveCommandLineParser6Parse1ERK13CObjectVectorI11CStringBaseIwEER26CArchiveCommandLineOptions
.Ltmp10:
# BB#12:
	cmpb	$0, 744(%rsp)
	je	.LBB0_14
# BB#13:
	xorl	%ebx, %ebx
.Ltmp11:
.Lcfi21:
	.cfi_escape 0x2e, 0x00
	movl	$g_StdOut, %edi
	movl	$1, %esi
	callq	_ZL20ShowCopyrightAndHelpR13CStdOutStreamb
.Ltmp12:
	jmp	.LBB0_486
.LBB0_14:
	cmpb	$0, 745(%rsp)
	je	.LBB0_16
# BB#15:
.Ltmp13:
.Lcfi22:
	.cfi_escape 0x2e, 0x00
	callq	SetLargePageSize
.Ltmp14:
.LBB0_16:
	cmpb	$0, 750(%rsp)
	movl	$g_StdErr, %eax
	movl	$g_StdOut, %ebp
	cmovneq	%rax, %rbp
	movq	%rbp, g_StdStream(%rip)
	cmpb	$0, 751(%rsp)
	je	.LBB0_18
# BB#17:
.Ltmp16:
.Lcfi23:
	.cfi_escape 0x2e, 0x00
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	_ZL20ShowCopyrightAndHelpR13CStdOutStreamb
.Ltmp17:
.LBB0_18:
.Ltmp18:
.Lcfi24:
	.cfi_escape 0x2e, 0x00
	leaq	1336(%rsp), %rdi
	leaq	744(%rsp), %rsi
	callq	_ZN25CArchiveCommandLineParser6Parse2ER26CArchiveCommandLineOptions
.Ltmp19:
# BB#19:
.Ltmp20:
.Lcfi25:
	.cfi_escape 0x2e, 0x00
	movl	$48, %edi
	callq	_Znwm
.Ltmp21:
# BB#20:                                # %_ZN9CMyComPtrI8IUnknownEC2EPS0_.exit
	movq	$_ZTV7CCodecs+16, (%rax)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%rax)
	movq	$8, 40(%rax)
	movq	$_ZTV13CObjectVectorI10CArcInfoExE+16, 16(%rax)
	movl	$1, 8(%rax)
.Ltmp23:
.Lcfi26:
	.cfi_escape 0x2e, 0x00
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	callq	_ZN7CCodecs4LoadEv
	movl	%eax, %ebx
.Ltmp24:
# BB#21:
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	testl	%ebx, %ebx
	jne	.LBB0_704
# BB#22:
	leaq	792(%rsp), %r15
.Ltmp28:
.Lcfi27:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZNK15CArchiveCommand18IsFromExtractGroupEv
	movl	%eax, %ebx
.Ltmp29:
# BB#23:
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 28(%rax)
	jne	.LBB0_28
# BB#24:
	testb	%bl, %bl
	jne	.LBB0_703
# BB#25:
	cmpl	$6, 792(%rsp)
	je	.LBB0_703
# BB#26:
.Ltmp31:
.Lcfi28:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZNK15CArchiveCommand17IsFromUpdateGroupEv
.Ltmp32:
# BB#27:
	testb	%al, %al
	movq	40(%rsp), %rax          # 8-byte Reload
	jne	.LBB0_703
.LBB0_28:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 432(%rsp)
	movq	$4, 448(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 424(%rsp)
	leaq	1288(%rsp), %rsi
.Ltmp33:
.Lcfi29:
	.cfi_escape 0x2e, 0x00
	leaq	424(%rsp), %rdx
	movq	%rax, %rdi
	callq	_ZNK7CCodecs24FindFormatForArchiveTypeERK11CStringBaseIwER13CRecordVectorIiE
.Ltmp34:
# BB#29:
	testb	%al, %al
	je	.LBB0_705
# BB#30:
	movl	792(%rsp), %eax
	cmpl	$7, %eax
	je	.LBB0_244
# BB#31:
	cmpl	$8, %eax
	jne	.LBB0_250
# BB#32:
.Ltmp53:
.Lcfi30:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp54:
# BB#33:
.Ltmp55:
.Lcfi31:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp56:
# BB#34:
.Ltmp57:
.Lcfi32:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp58:
# BB#35:                                # %.preheader812
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, 28(%rcx)
	movq	8(%rsp), %r14           # 8-byte Reload
	jle	.LBB0_241
# BB#36:                                # %.lr.ph858
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_37:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_49 Depth 2
                                        #     Child Loop BB0_96 Depth 2
                                        #       Child Loop BB0_58 Depth 3
                                        #       Child Loop BB0_74 Depth 3
                                        #       Child Loop BB0_111 Depth 3
                                        #       Child Loop BB0_114 Depth 3
                                        #       Child Loop BB0_119 Depth 3
                                        #       Child Loop BB0_64 Depth 3
                                        #       Child Loop BB0_84 Depth 3
                                        #       Child Loop BB0_138 Depth 3
                                        #       Child Loop BB0_141 Depth 3
                                        #       Child Loop BB0_70 Depth 3
                                        #       Child Loop BB0_94 Depth 3
                                        #       Child Loop BB0_161 Depth 3
                                        #       Child Loop BB0_164 Depth 3
                                        #       Child Loop BB0_169 Depth 3
                                        #       Child Loop BB0_67 Depth 3
                                        #       Child Loop BB0_89 Depth 3
                                        #       Child Loop BB0_186 Depth 3
                                        #       Child Loop BB0_189 Depth 3
                                        #       Child Loop BB0_61 Depth 3
                                        #       Child Loop BB0_79 Depth 3
                                        #       Child Loop BB0_210 Depth 3
                                        #       Child Loop BB0_213 Depth 3
                                        #     Child Loop BB0_221 Depth 2
                                        #     Child Loop BB0_226 Depth 2
	movq	32(%rcx), %rax
	movq	%rsi, 456(%rsp)         # 8-byte Spill
	movq	(%rax,%rsi,8), %r15
.Ltmp59:
.Lcfi33:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.1, %esi
	movq	%r14, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp60:
# BB#38:                                #   in Loop: Header=BB0_37 Depth=1
.Ltmp61:
.Lcfi34:
	.cfi_escape 0x2e, 0x00
	movl	$32, %esi
	movq	%r14, %rdi
	callq	_ZN13CStdOutStreamlsEc
.Ltmp62:
# BB#39:                                #   in Loop: Header=BB0_37 Depth=1
	cmpb	$0, (%r15)
	movb	$67, %al
	jne	.LBB0_41
# BB#40:                                #   in Loop: Header=BB0_37 Depth=1
	movb	$32, %al
.LBB0_41:                               #   in Loop: Header=BB0_37 Depth=1
	movzbl	%al, %esi
.Ltmp63:
.Lcfi35:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN13CStdOutStreamlsEc
.Ltmp64:
# BB#42:                                #   in Loop: Header=BB0_37 Depth=1
	cmpb	$0, 96(%r15)
	movb	$75, %al
	jne	.LBB0_44
# BB#43:                                #   in Loop: Header=BB0_37 Depth=1
	movb	$32, %al
.LBB0_44:                               #   in Loop: Header=BB0_37 Depth=1
	movzbl	%al, %esi
.Ltmp65:
.Lcfi36:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN13CStdOutStreamlsEc
.Ltmp66:
# BB#45:                                #   in Loop: Header=BB0_37 Depth=1
.Ltmp67:
.Lcfi37:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.1, %esi
	movq	%r14, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp68:
# BB#46:                                #   in Loop: Header=BB0_37 Depth=1
	movq	24(%r15), %rsi
	movl	32(%r15), %ebp
.Ltmp69:
.Lcfi38:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN13CStdOutStreamlsEPKw
.Ltmp70:
# BB#47:                                # %.noexc
                                        #   in Loop: Header=BB0_37 Depth=1
	cmpl	$5, %ebp
	jg	.LBB0_51
# BB#48:                                # %.lr.ph.i314.preheader
                                        #   in Loop: Header=BB0_37 Depth=1
	movl	$6, %ebx
	subl	%ebp, %ebx
	.p2align	4, 0x90
.LBB0_49:                               # %.lr.ph.i314
                                        #   Parent Loop BB0_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp71:
.Lcfi39:
	.cfi_escape 0x2e, 0x00
	movl	$32, %esi
	movq	%r14, %rdi
	callq	_ZN13CStdOutStreamlsEc
.Ltmp72:
# BB#50:                                # %.noexc315
                                        #   in Loop: Header=BB0_49 Depth=2
	decl	%ebx
	jne	.LBB0_49
.LBB0_51:                               # %_ZL11PrintStringR13CStdOutStreamRK11CStringBaseIwEi.exit
                                        #   in Loop: Header=BB0_37 Depth=1
.Ltmp74:
.Lcfi40:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.1, %esi
	movq	%r14, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp75:
# BB#52:                                #   in Loop: Header=BB0_37 Depth=1
.Ltmp76:
.Lcfi41:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r12
.Ltmp77:
# BB#53:                                # %_ZN11CStringBaseIwEC2Ev.exit
                                        #   in Loop: Header=BB0_37 Depth=1
	movl	$0, (%r12)
	movq	%r15, 72(%rsp)          # 8-byte Spill
	cmpl	$0, 52(%r15)
	jle	.LBB0_55
# BB#54:                                # %.lr.ph849
                                        #   in Loop: Header=BB0_37 Depth=1
	xorl	%edx, %edx
	movl	$4, %r13d
	movq	%r12, %r10
	movq	%r12, 128(%rsp)         # 8-byte Spill
	movq	%r12, 112(%rsp)         # 8-byte Spill
	movq	%r12, 120(%rsp)         # 8-byte Spill
	movq	%r12, 136(%rsp)         # 8-byte Spill
	movq	%r12, %r8
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	%r12, %rdi
	movq	%r12, %r11
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%r12, %r9
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	%r12, %r14
	xorl	%ebp, %ebp
	movq	72(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB0_96
	.p2align	4, 0x90
.LBB0_55:                               #   in Loop: Header=BB0_37 Depth=1
	xorl	%ebp, %ebp
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	%r12, %r8
	jmp	.LBB0_218
.LBB0_56:                               # %vector.body1100.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_71
# BB#57:                                # %vector.body1100.prol.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_58:                               # %vector.body1100.prol
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	(%rbp,%rdi,4), %xmm0
	movdqu	16(%rbp,%rdi,4), %xmm1
	movdqu	%xmm0, (%r12,%rdi,4)
	movdqu	%xmm1, 16(%r12,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB0_58
	jmp	.LBB0_72
.LBB0_59:                               # %vector.body.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_76
# BB#60:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_61:                               # %vector.body.prol
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	(%rbp,%rbx,4), %xmm0
	movdqu	16(%rbp,%rbx,4), %xmm1
	movdqu	%xmm0, (%r12,%rbx,4)
	movdqu	%xmm1, 16(%r12,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB0_61
	jmp	.LBB0_77
.LBB0_62:                               # %vector.body1071.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB0_81
# BB#63:                                # %vector.body1071.prol.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_64:                               # %vector.body1071.prol
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	(%rdi,%rsi,4), %xmm0
	movdqu	16(%rdi,%rsi,4), %xmm1
	movdqu	%xmm0, (%r14,%rsi,4)
	movdqu	%xmm1, 16(%r14,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB0_64
	jmp	.LBB0_82
.LBB0_65:                               # %vector.body1013.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB0_86
# BB#66:                                # %vector.body1013.prol.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_67:                               # %vector.body1013.prol
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	(%r8,%rsi,4), %xmm0
	movdqu	16(%r8,%rsi,4), %xmm1
	movdqu	%xmm0, (%r14,%rsi,4)
	movdqu	%xmm1, 16(%r14,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB0_67
	jmp	.LBB0_87
.LBB0_68:                               # %vector.body1042.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_91
# BB#69:                                # %vector.body1042.prol.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_70:                               # %vector.body1042.prol
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	(%r8,%rbx,4), %xmm0
	movdqu	16(%r8,%rbx,4), %xmm1
	movdqu	%xmm0, (%r12,%rbx,4)
	movdqu	%xmm1, 16(%r12,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB0_70
	jmp	.LBB0_92
.LBB0_71:                               #   in Loop: Header=BB0_96 Depth=2
	xorl	%edi, %edi
.LBB0_72:                               # %vector.body1100.prol.loopexit
                                        #   in Loop: Header=BB0_96 Depth=2
	cmpq	$24, %rdx
	jb	.LBB0_75
# BB#73:                                # %vector.body1100.preheader.new
                                        #   in Loop: Header=BB0_96 Depth=2
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%r12,%rdi,4), %rsi
	leaq	112(%rbp,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB0_74:                               # %vector.body1100
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB0_74
.LBB0_75:                               # %middle.block1101
                                        #   in Loop: Header=BB0_96 Depth=2
	cmpq	%rcx, %rax
	jne	.LBB0_109
	jmp	.LBB0_116
.LBB0_76:                               #   in Loop: Header=BB0_96 Depth=2
	xorl	%ebx, %ebx
.LBB0_77:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB0_96 Depth=2
	cmpq	$24, %rdx
	jb	.LBB0_80
# BB#78:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_96 Depth=2
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx,4), %rsi
	leaq	112(%rbp,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB0_79:                               # %vector.body
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rbx), %xmm0
	movdqu	(%rbx), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB0_79
.LBB0_80:                               # %middle.block
                                        #   in Loop: Header=BB0_96 Depth=2
	cmpq	%rcx, %rax
	jne	.LBB0_208
	jmp	.LBB0_215
.LBB0_81:                               #   in Loop: Header=BB0_96 Depth=2
	xorl	%esi, %esi
.LBB0_82:                               # %vector.body1071.prol.loopexit
                                        #   in Loop: Header=BB0_96 Depth=2
	cmpq	$24, %rcx
	jb	.LBB0_85
# BB#83:                                # %vector.body1071.preheader.new
                                        #   in Loop: Header=BB0_96 Depth=2
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%r14,%rsi,4), %rdx
	leaq	112(%rdi,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB0_84:                               # %vector.body1071
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rsi), %xmm0
	movdqu	(%rsi), %xmm1
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB0_84
.LBB0_85:                               # %middle.block1072
                                        #   in Loop: Header=BB0_96 Depth=2
	cmpq	%rax, %rbp
	jne	.LBB0_136
	jmp	.LBB0_143
.LBB0_86:                               #   in Loop: Header=BB0_96 Depth=2
	xorl	%esi, %esi
.LBB0_87:                               # %vector.body1013.prol.loopexit
                                        #   in Loop: Header=BB0_96 Depth=2
	cmpq	$24, %rcx
	jb	.LBB0_90
# BB#88:                                # %vector.body1013.preheader.new
                                        #   in Loop: Header=BB0_96 Depth=2
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%r14,%rsi,4), %rdx
	leaq	112(%r8,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB0_89:                               # %vector.body1013
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rsi), %xmm0
	movdqu	(%rsi), %xmm1
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB0_89
.LBB0_90:                               # %middle.block1014
                                        #   in Loop: Header=BB0_96 Depth=2
	cmpq	%rax, %r15
	jne	.LBB0_184
	jmp	.LBB0_191
.LBB0_91:                               #   in Loop: Header=BB0_96 Depth=2
	xorl	%ebx, %ebx
.LBB0_92:                               # %vector.body1042.prol.loopexit
                                        #   in Loop: Header=BB0_96 Depth=2
	cmpq	$24, %rdx
	jb	.LBB0_95
# BB#93:                                # %vector.body1042.preheader.new
                                        #   in Loop: Header=BB0_96 Depth=2
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx,4), %rsi
	leaq	112(%r8,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB0_94:                               # %vector.body1042
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rbx), %xmm0
	movdqu	(%rbx), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB0_94
.LBB0_95:                               # %middle.block1043
                                        #   in Loop: Header=BB0_96 Depth=2
	cmpq	%rcx, %rax
	jne	.LBB0_159
	jmp	.LBB0_166
	.p2align	4, 0x90
.LBB0_96:                               #   Parent Loop BB0_37 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_58 Depth 3
                                        #       Child Loop BB0_74 Depth 3
                                        #       Child Loop BB0_111 Depth 3
                                        #       Child Loop BB0_114 Depth 3
                                        #       Child Loop BB0_119 Depth 3
                                        #       Child Loop BB0_64 Depth 3
                                        #       Child Loop BB0_84 Depth 3
                                        #       Child Loop BB0_138 Depth 3
                                        #       Child Loop BB0_141 Depth 3
                                        #       Child Loop BB0_70 Depth 3
                                        #       Child Loop BB0_94 Depth 3
                                        #       Child Loop BB0_161 Depth 3
                                        #       Child Loop BB0_164 Depth 3
                                        #       Child Loop BB0_169 Depth 3
                                        #       Child Loop BB0_67 Depth 3
                                        #       Child Loop BB0_89 Depth 3
                                        #       Child Loop BB0_186 Depth 3
                                        #       Child Loop BB0_189 Depth 3
                                        #       Child Loop BB0_61 Depth 3
                                        #       Child Loop BB0_79 Depth 3
                                        #       Child Loop BB0_210 Depth 3
                                        #       Child Loop BB0_213 Depth 3
	movq	56(%rcx), %rax
	movq	(%rax,%rdx,8), %rbx
	movl	8(%rbx), %eax
	movl	%r13d, %ecx
	subl	%ebp, %ecx
	cmpl	%eax, %ecx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	jg	.LBB0_100
# BB#97:                                #   in Loop: Header=BB0_96 Depth=2
	decl	%ecx
	cmpl	$8, %r13d
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %r13d
	jl	.LBB0_99
# BB#98:                                # %select.true.sink
                                        #   in Loop: Header=BB0_96 Depth=2
	movl	%r13d, %edx
	shrl	$31, %edx
	addl	%r13d, %edx
	sarl	%edx
.LBB0_99:                               # %select.end
                                        #   in Loop: Header=BB0_96 Depth=2
	movl	%eax, %esi
	subl	%ecx, %esi
	addl	%edx, %ecx
	cmpl	%eax, %ecx
	cmovgel	%edx, %esi
	leal	1(%r13,%rsi), %eax
	cmpl	%r13d, %eax
	jne	.LBB0_101
.LBB0_100:                              #   in Loop: Header=BB0_96 Depth=2
	movq	%r14, %r12
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	jmp	.LBB0_118
	.p2align	4, 0x90
.LBB0_101:                              #   in Loop: Header=BB0_96 Depth=2
	movq	%r10, 56(%rsp)          # 8-byte Spill
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movq	%rax, %r15
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp79:
.Lcfi42:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
.Ltmp80:
# BB#102:                               # %.noexc327
                                        #   in Loop: Header=BB0_96 Depth=2
	testl	%r13d, %r13d
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%r15, %r13
	jle	.LBB0_117
# BB#103:                               # %.preheader.i.i
                                        #   in Loop: Header=BB0_96 Depth=2
	movq	64(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	jle	.LBB0_115
# BB#104:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB0_96 Depth=2
	movslq	%ecx, %rax
	cmpl	$7, %ecx
	jbe	.LBB0_108
# BB#105:                               # %min.iters.checked1104
                                        #   in Loop: Header=BB0_96 Depth=2
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB0_108
# BB#106:                               # %vector.memcheck1117
                                        #   in Loop: Header=BB0_96 Depth=2
	leaq	(%rbp,%rax,4), %rdx
	cmpq	%rdx, %r12
	jae	.LBB0_56
# BB#107:                               # %vector.memcheck1117
                                        #   in Loop: Header=BB0_96 Depth=2
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, 56(%rsp)          # 8-byte Folded Reload
	jae	.LBB0_56
.LBB0_108:                              #   in Loop: Header=BB0_96 Depth=2
	xorl	%ecx, %ecx
.LBB0_109:                              # %scalar.ph1102.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	movq	64(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB0_112
# BB#110:                               # %scalar.ph1102.prol.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	negq	%rsi
	.p2align	4, 0x90
.LBB0_111:                              # %scalar.ph1102.prol
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbp,%rcx,4), %edi
	movl	%edi, (%r12,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_111
.LBB0_112:                              # %scalar.ph1102.prol.loopexit
                                        #   in Loop: Header=BB0_96 Depth=2
	cmpq	$7, %rdx
	jb	.LBB0_116
# BB#113:                               # %scalar.ph1102.preheader.new
                                        #   in Loop: Header=BB0_96 Depth=2
	subq	%rcx, %rax
	leaq	28(%r12,%rcx,4), %rdx
	leaq	28(%rbp,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB0_114:                              # %scalar.ph1102
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB0_114
	jmp	.LBB0_116
.LBB0_115:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB0_96 Depth=2
	testq	%rbp, %rbp
	je	.LBB0_117
.LBB0_116:                              # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB0_96 Depth=2
.Lcfi43:
	.cfi_escape 0x2e, 0x00
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB0_117:                              # %._crit_edge16.i.i
                                        #   in Loop: Header=BB0_96 Depth=2
	movq	64(%rsp), %rbp          # 8-byte Reload
	movslq	%ebp, %rax
	movl	$0, (%r12,%rax,4)
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	%r12, %r9
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%r12, %r11
	movq	%r12, %rdi
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	%r12, %r10
	movq	%r12, 128(%rsp)         # 8-byte Spill
	movq	%r12, 112(%rsp)         # 8-byte Spill
	movq	%r12, 120(%rsp)         # 8-byte Spill
	movq	%r12, 136(%rsp)         # 8-byte Spill
	movq	%r12, %r8
.LBB0_118:                              # %.noexc324
                                        #   in Loop: Header=BB0_96 Depth=2
	movslq	%ebp, %rax
	leaq	(%r8,%rax,4), %rcx
	movq	(%rbx), %rdx
	.p2align	4, 0x90
.LBB0_119:                              #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdx), %esi
	addq	$4, %rdx
	movl	%esi, (%rcx)
	addq	$4, %rcx
	testl	%esi, %esi
	jne	.LBB0_119
# BB#120:                               #   in Loop: Header=BB0_96 Depth=2
	movslq	8(%rbx), %rbp
	addq	%rax, %rbp
	movl	24(%rbx), %eax
	testl	%eax, %eax
	je	.LBB0_123
# BB#121:                               # %.preheader800.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	movl	%r13d, %ecx
	subl	%ebp, %ecx
	cmpl	$2, %ecx
	jle	.LBB0_124
# BB#122:                               #   in Loop: Header=BB0_96 Depth=2
	movq	%r12, %r14
	movl	%r13d, %r15d
	jmp	.LBB0_145
	.p2align	4, 0x90
.LBB0_123:                              #   in Loop: Header=BB0_96 Depth=2
	movq	%r12, %r14
	jmp	.LBB0_194
	.p2align	4, 0x90
.LBB0_124:                              #   in Loop: Header=BB0_96 Depth=2
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	cmpl	$8, %r13d
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %r13d
	jl	.LBB0_126
# BB#125:                               # %select.true.sink2260
                                        #   in Loop: Header=BB0_96 Depth=2
	movl	%r13d, %edx
	shrl	$31, %edx
	addl	%r13d, %edx
	sarl	%edx
.LBB0_126:                              # %select.end2259
                                        #   in Loop: Header=BB0_96 Depth=2
	leal	-1(%rdx,%rcx), %esi
	movl	$3, %edi
	subl	%ecx, %edi
	cmpl	$2, %esi
	cmovgel	%edx, %edi
	leal	1(%r13,%rdi), %ecx
	cmpl	%r13d, %ecx
	jne	.LBB0_128
# BB#127:                               #   in Loop: Header=BB0_96 Depth=2
	movq	%r12, %r14
	movl	%r13d, %r15d
	movq	64(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_145
.LBB0_128:                              #   in Loop: Header=BB0_96 Depth=2
	movq	%rbp, %r15
	movq	%r13, %rbp
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movq	%rcx, %r13
	movslq	%ecx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp81:
.Lcfi44:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
.Ltmp82:
# BB#129:                               # %.noexc345
                                        #   in Loop: Header=BB0_96 Depth=2
	testl	%ebp, %ebp
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%r15, %rbp
	movq	%r13, %r15
	jle	.LBB0_144
# BB#130:                               # %.preheader.i.i336
                                        #   in Loop: Header=BB0_96 Depth=2
	testl	%ebp, %ebp
	jle	.LBB0_142
# BB#131:                               # %.lr.ph.i.i337
                                        #   in Loop: Header=BB0_96 Depth=2
	cmpl	$7, %ebp
	jbe	.LBB0_135
# BB#132:                               # %min.iters.checked1075
                                        #   in Loop: Header=BB0_96 Depth=2
	movq	%rbp, %rax
	andq	$-8, %rax
	je	.LBB0_135
# BB#133:                               # %vector.memcheck1088
                                        #   in Loop: Header=BB0_96 Depth=2
	leaq	(%rdi,%rbp,4), %rcx
	cmpq	%rcx, %r14
	jae	.LBB0_62
# BB#134:                               # %vector.memcheck1088
                                        #   in Loop: Header=BB0_96 Depth=2
	leaq	(%r14,%rbp,4), %rcx
	cmpq	%rcx, 128(%rsp)         # 8-byte Folded Reload
	jae	.LBB0_62
.LBB0_135:                              #   in Loop: Header=BB0_96 Depth=2
	xorl	%eax, %eax
.LBB0_136:                              # %scalar.ph1073.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	movl	%ebp, %edx
	subl	%eax, %edx
	leaq	-1(%rbp), %rcx
	subq	%rax, %rcx
	andq	$7, %rdx
	je	.LBB0_139
# BB#137:                               # %scalar.ph1073.prol.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	negq	%rdx
	.p2align	4, 0x90
.LBB0_138:                              # %scalar.ph1073.prol
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdi,%rax,4), %esi
	movl	%esi, (%r14,%rax,4)
	incq	%rax
	incq	%rdx
	jne	.LBB0_138
.LBB0_139:                              # %scalar.ph1073.prol.loopexit
                                        #   in Loop: Header=BB0_96 Depth=2
	cmpq	$7, %rcx
	jb	.LBB0_143
# BB#140:                               # %scalar.ph1073.preheader.new
                                        #   in Loop: Header=BB0_96 Depth=2
	movq	%rbp, %rcx
	subq	%rax, %rcx
	leaq	28(%r14,%rax,4), %rdx
	leaq	28(%rdi,%rax,4), %rax
	.p2align	4, 0x90
.LBB0_141:                              # %scalar.ph1073
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-28(%rax), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rax), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rax), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rax), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rax), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rax), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rax), %esi
	movl	%esi, -4(%rdx)
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rax
	addq	$-8, %rcx
	jne	.LBB0_141
	jmp	.LBB0_143
.LBB0_142:                              # %._crit_edge.i.i338
                                        #   in Loop: Header=BB0_96 Depth=2
	testq	%rdi, %rdi
	je	.LBB0_144
.LBB0_143:                              # %._crit_edge.thread.i.i343
                                        #   in Loop: Header=BB0_96 Depth=2
.Lcfi45:
	.cfi_escape 0x2e, 0x00
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB0_144:                              # %._crit_edge16.i.i344
                                        #   in Loop: Header=BB0_96 Depth=2
	movslq	%ebp, %rax
	movl	$0, (%r14,%rax,4)
	movl	24(%rbx), %eax
	movq	%r14, 56(%rsp)          # 8-byte Spill
	movq	%r14, %r9
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%r14, %r11
	movq	%r14, %rdi
	movq	%r14, 48(%rsp)          # 8-byte Spill
	movq	%r14, %r10
	movq	%r14, 128(%rsp)         # 8-byte Spill
	movq	%r14, 112(%rsp)         # 8-byte Spill
	movq	%r14, 120(%rsp)         # 8-byte Spill
	movq	%r14, 136(%rsp)         # 8-byte Spill
	movq	%r14, %r8
.LBB0_145:                              # %.noexc332
                                        #   in Loop: Header=BB0_96 Depth=2
	movslq	%ebp, %rcx
	movabsq	$171798691872, %rdx     # imm = 0x2800000020
	movq	%rdx, (%r8,%rcx,4)
	movl	$0, 8(%r8,%rcx,4)
	leal	2(%rbp), %r13d
	movl	%r15d, %ecx
	subl	%r13d, %ecx
	cmpl	%eax, %ecx
	jle	.LBB0_147
# BB#146:                               #   in Loop: Header=BB0_96 Depth=2
	movq	%r14, %r12
	movl	%r15d, %r14d
	jmp	.LBB0_168
	.p2align	4, 0x90
.LBB0_147:                              #   in Loop: Header=BB0_96 Depth=2
	movl	%r13d, 220(%rsp)        # 4-byte Spill
	movq	%rdi, %r13
	decl	%ecx
	cmpl	$8, %r15d
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %r15d
	jl	.LBB0_149
# BB#148:                               # %select.true.sink2284
                                        #   in Loop: Header=BB0_96 Depth=2
	movl	%r15d, %edx
	shrl	$31, %edx
	addl	%r15d, %edx
	sarl	%edx
.LBB0_149:                              # %select.end2283
                                        #   in Loop: Header=BB0_96 Depth=2
	leal	(%rdx,%rcx), %esi
	movl	%eax, %edi
	subl	%ecx, %edi
	cmpl	%eax, %esi
	cmovgel	%edx, %edi
	leal	1(%r15,%rdi), %eax
	cmpl	%r15d, %eax
	jne	.LBB0_151
# BB#150:                               #   in Loop: Header=BB0_96 Depth=2
	movq	%r14, %r12
	movl	%r15d, %r14d
	movq	%r13, %rdi
	movl	220(%rsp), %r13d        # 4-byte Reload
	jmp	.LBB0_168
.LBB0_151:                              #   in Loop: Header=BB0_96 Depth=2
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	%r11, %rbp
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movq	%rax, %r13
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp84:
.Lcfi46:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
.Ltmp85:
# BB#152:                               # %.noexc363
                                        #   in Loop: Header=BB0_96 Depth=2
	testl	%r15d, %r15d
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%r13, %r14
	movl	220(%rsp), %r13d        # 4-byte Reload
	jle	.LBB0_167
# BB#153:                               # %.preheader.i.i354
                                        #   in Loop: Header=BB0_96 Depth=2
	cmpl	$-1, 64(%rsp)           # 4-byte Folded Reload
	jl	.LBB0_165
# BB#154:                               # %.lr.ph.i.i355
                                        #   in Loop: Header=BB0_96 Depth=2
	movslq	%r13d, %rax
	cmpl	$7, %r13d
	jbe	.LBB0_158
# BB#155:                               # %min.iters.checked1046
                                        #   in Loop: Header=BB0_96 Depth=2
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB0_158
# BB#156:                               # %vector.memcheck1059
                                        #   in Loop: Header=BB0_96 Depth=2
	leaq	(%r8,%rax,4), %rdx
	cmpq	%rdx, %r12
	jae	.LBB0_68
# BB#157:                               # %vector.memcheck1059
                                        #   in Loop: Header=BB0_96 Depth=2
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, 112(%rsp)         # 8-byte Folded Reload
	jae	.LBB0_68
.LBB0_158:                              #   in Loop: Header=BB0_96 Depth=2
	xorl	%ecx, %ecx
.LBB0_159:                              # %scalar.ph1044.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	movl	%r13d, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB0_162
# BB#160:                               # %scalar.ph1044.prol.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	negq	%rsi
	.p2align	4, 0x90
.LBB0_161:                              # %scalar.ph1044.prol
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r8,%rcx,4), %ebx
	movl	%ebx, (%r12,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_161
.LBB0_162:                              # %scalar.ph1044.prol.loopexit
                                        #   in Loop: Header=BB0_96 Depth=2
	cmpq	$7, %rdx
	jb	.LBB0_166
# BB#163:                               # %scalar.ph1044.preheader.new
                                        #   in Loop: Header=BB0_96 Depth=2
	subq	%rcx, %rax
	leaq	28(%r12,%rcx,4), %rdx
	leaq	28(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB0_164:                              # %scalar.ph1044
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB0_164
	jmp	.LBB0_166
.LBB0_165:                              # %._crit_edge.i.i356
                                        #   in Loop: Header=BB0_96 Depth=2
	testq	%r8, %r8
	je	.LBB0_167
.LBB0_166:                              # %._crit_edge.thread.i.i361
                                        #   in Loop: Header=BB0_96 Depth=2
.Lcfi47:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdaPv
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB0_167:                              # %._crit_edge16.i.i362
                                        #   in Loop: Header=BB0_96 Depth=2
	movslq	%r13d, %rax
	movl	$0, (%r12,%rax,4)
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	%r12, %r9
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%r12, %r11
	movq	%r12, %rdi
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	%r12, %r10
	movq	%r12, 128(%rsp)         # 8-byte Spill
	movq	%r12, 112(%rsp)         # 8-byte Spill
	movq	%r12, 120(%rsp)         # 8-byte Spill
	movq	%r12, 136(%rsp)         # 8-byte Spill
	movq	%r12, %r8
.LBB0_168:                              # %.noexc349
                                        #   in Loop: Header=BB0_96 Depth=2
	movslq	%r13d, %rax
	leaq	(%r8,%rax,4), %rcx
	movq	16(%rbx), %rdx
	.p2align	4, 0x90
.LBB0_169:                              #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdx), %esi
	addq	$4, %rdx
	movl	%esi, (%rcx)
	addq	$4, %rcx
	testl	%esi, %esi
	jne	.LBB0_169
# BB#170:                               #   in Loop: Header=BB0_96 Depth=2
	movslq	24(%rbx), %r15
	addq	%rax, %r15
	movl	%r14d, %eax
	subl	%r15d, %eax
	cmpl	$1, %eax
	jle	.LBB0_172
# BB#171:                               #   in Loop: Header=BB0_96 Depth=2
	movq	%r14, %rax
	jmp	.LBB0_175
	.p2align	4, 0x90
.LBB0_172:                              #   in Loop: Header=BB0_96 Depth=2
	cmpl	$8, %r14d
	movl	$4, %ecx
	movl	$16, %edx
	cmovgl	%edx, %ecx
	cmpl	$65, %r14d
	jl	.LBB0_174
# BB#173:                               # %select.true.sink2310
                                        #   in Loop: Header=BB0_96 Depth=2
	movl	%r14d, %ecx
	shrl	$31, %ecx
	addl	%r14d, %ecx
	sarl	%ecx
.LBB0_174:                              # %select.end2309
                                        #   in Loop: Header=BB0_96 Depth=2
	leal	-1(%rcx,%rax), %edx
	movl	$2, %esi
	subl	%eax, %esi
	testl	%edx, %edx
	cmovgl	%ecx, %esi
	leal	1(%r14,%rsi), %r13d
	cmpl	%r14d, %r13d
	movq	%r14, %rax
	jne	.LBB0_176
.LBB0_175:                              #   in Loop: Header=BB0_96 Depth=2
	movq	%r12, %r14
	movl	%eax, %r13d
	jmp	.LBB0_193
.LBB0_176:                              #   in Loop: Header=BB0_96 Depth=2
	movq	%rax, %rbp
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movslq	%r13d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp86:
.Lcfi48:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
.Ltmp87:
# BB#177:                               # %.noexc378
                                        #   in Loop: Header=BB0_96 Depth=2
	testl	%ebp, %ebp
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	%rbx, %rdi
	jle	.LBB0_192
# BB#178:                               # %.preheader.i.i369
                                        #   in Loop: Header=BB0_96 Depth=2
	testl	%r15d, %r15d
	jle	.LBB0_190
# BB#179:                               # %.lr.ph.i.i370
                                        #   in Loop: Header=BB0_96 Depth=2
	cmpl	$7, %r15d
	jbe	.LBB0_183
# BB#180:                               # %min.iters.checked1017
                                        #   in Loop: Header=BB0_96 Depth=2
	movq	%r15, %rax
	andq	$-8, %rax
	je	.LBB0_183
# BB#181:                               # %vector.memcheck1030
                                        #   in Loop: Header=BB0_96 Depth=2
	leaq	(%r8,%r15,4), %rcx
	cmpq	%rcx, %r14
	jae	.LBB0_65
# BB#182:                               # %vector.memcheck1030
                                        #   in Loop: Header=BB0_96 Depth=2
	leaq	(%r14,%r15,4), %rcx
	cmpq	%rcx, 120(%rsp)         # 8-byte Folded Reload
	jae	.LBB0_65
.LBB0_183:                              #   in Loop: Header=BB0_96 Depth=2
	xorl	%eax, %eax
.LBB0_184:                              # %scalar.ph1015.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	movl	%r15d, %edx
	subl	%eax, %edx
	leaq	-1(%r15), %rcx
	subq	%rax, %rcx
	andq	$7, %rdx
	je	.LBB0_187
# BB#185:                               # %scalar.ph1015.prol.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	negq	%rdx
	.p2align	4, 0x90
.LBB0_186:                              # %scalar.ph1015.prol
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r8,%rax,4), %esi
	movl	%esi, (%r14,%rax,4)
	incq	%rax
	incq	%rdx
	jne	.LBB0_186
.LBB0_187:                              # %scalar.ph1015.prol.loopexit
                                        #   in Loop: Header=BB0_96 Depth=2
	cmpq	$7, %rcx
	jb	.LBB0_191
# BB#188:                               # %scalar.ph1015.preheader.new
                                        #   in Loop: Header=BB0_96 Depth=2
	movq	%r15, %rcx
	subq	%rax, %rcx
	leaq	28(%r14,%rax,4), %rdx
	leaq	28(%r8,%rax,4), %rax
	.p2align	4, 0x90
.LBB0_189:                              # %scalar.ph1015
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-28(%rax), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rax), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rax), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rax), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rax), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rax), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rax), %esi
	movl	%esi, -4(%rdx)
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rax
	addq	$-8, %rcx
	jne	.LBB0_189
	jmp	.LBB0_191
.LBB0_190:                              # %._crit_edge.i.i371
                                        #   in Loop: Header=BB0_96 Depth=2
	testq	%r8, %r8
	je	.LBB0_192
.LBB0_191:                              # %._crit_edge.thread.i.i376
                                        #   in Loop: Header=BB0_96 Depth=2
.Lcfi49:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_192:                              # %._crit_edge16.i.i377
                                        #   in Loop: Header=BB0_96 Depth=2
	movslq	%r15d, %rax
	movl	$0, (%r14,%rax,4)
	movq	%r14, 56(%rsp)          # 8-byte Spill
	movq	%r14, %r9
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%r14, %r11
	movq	%r14, %rdi
	movq	%r14, 48(%rsp)          # 8-byte Spill
	movq	%r14, %r10
	movq	%r14, 128(%rsp)         # 8-byte Spill
	movq	%r14, 112(%rsp)         # 8-byte Spill
	movq	%r14, 120(%rsp)         # 8-byte Spill
	movq	%r14, 136(%rsp)         # 8-byte Spill
	movq	%r14, %r8
.LBB0_193:                              # %_ZN11CStringBaseIwEpLEw.exit
                                        #   in Loop: Header=BB0_96 Depth=2
	movslq	%r15d, %rax
	movq	$41, (%r8,%rax,4)
	leal	1(%r15), %ebp
.LBB0_194:                              #   in Loop: Header=BB0_96 Depth=2
	movl	%r13d, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jle	.LBB0_196
# BB#195:                               #   in Loop: Header=BB0_96 Depth=2
	movq	%r14, %r12
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	jmp	.LBB0_217
	.p2align	4, 0x90
.LBB0_196:                              #   in Loop: Header=BB0_96 Depth=2
	leal	-1(%rax), %ecx
	cmpl	$8, %r13d
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %r13d
	jl	.LBB0_198
# BB#197:                               # %select.true.sink2333
                                        #   in Loop: Header=BB0_96 Depth=2
	movl	%r13d, %edx
	shrl	$31, %edx
	addl	%r13d, %edx
	sarl	%edx
.LBB0_198:                              # %select.end2332
                                        #   in Loop: Header=BB0_96 Depth=2
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	movq	%r13, %rax
	leal	1(%rax,%rsi), %r13d
	cmpl	%eax, %r13d
	jne	.LBB0_200
# BB#199:                               #   in Loop: Header=BB0_96 Depth=2
	movq	%r14, %r12
	movl	%eax, %r13d
	jmp	.LBB0_217
	.p2align	4, 0x90
.LBB0_200:                              #   in Loop: Header=BB0_96 Depth=2
	movq	%rbp, %r15
	movq	%rax, %rbp
	movq	%r9, %rbx
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movslq	%r13d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp89:
.Lcfi50:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
.Ltmp90:
# BB#201:                               # %.noexc394
                                        #   in Loop: Header=BB0_96 Depth=2
	testl	%ebp, %ebp
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbx, %rdi
	jle	.LBB0_216
# BB#202:                               # %.preheader.i.i385
                                        #   in Loop: Header=BB0_96 Depth=2
	movq	%r15, %rcx
	testl	%ecx, %ecx
	jle	.LBB0_214
# BB#203:                               # %.lr.ph.i.i386
                                        #   in Loop: Header=BB0_96 Depth=2
	movslq	%ecx, %rax
	cmpl	$7, %ecx
	jbe	.LBB0_207
# BB#204:                               # %min.iters.checked
                                        #   in Loop: Header=BB0_96 Depth=2
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB0_207
# BB#205:                               # %vector.memcheck
                                        #   in Loop: Header=BB0_96 Depth=2
	leaq	(%rbp,%rax,4), %rdx
	cmpq	%rdx, %r12
	jae	.LBB0_59
# BB#206:                               # %vector.memcheck
                                        #   in Loop: Header=BB0_96 Depth=2
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, 136(%rsp)         # 8-byte Folded Reload
	jae	.LBB0_59
.LBB0_207:                              #   in Loop: Header=BB0_96 Depth=2
	xorl	%ecx, %ecx
.LBB0_208:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	movl	%r15d, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB0_211
# BB#209:                               # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB0_96 Depth=2
	negq	%rsi
	.p2align	4, 0x90
.LBB0_210:                              # %scalar.ph.prol
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbp,%rcx,4), %ebx
	movl	%ebx, (%r12,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_210
.LBB0_211:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB0_96 Depth=2
	cmpq	$7, %rdx
	jb	.LBB0_215
# BB#212:                               # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB0_96 Depth=2
	subq	%rcx, %rax
	leaq	28(%r12,%rcx,4), %rdx
	leaq	28(%rbp,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB0_213:                              # %scalar.ph
                                        #   Parent Loop BB0_37 Depth=1
                                        #     Parent Loop BB0_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB0_213
	jmp	.LBB0_215
.LBB0_214:                              # %._crit_edge.i.i387
                                        #   in Loop: Header=BB0_96 Depth=2
	testq	%rbp, %rbp
	je	.LBB0_216
.LBB0_215:                              # %._crit_edge.thread.i.i392
                                        #   in Loop: Header=BB0_96 Depth=2
.Lcfi51:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_216:                              # %._crit_edge16.i.i393
                                        #   in Loop: Header=BB0_96 Depth=2
	movq	%r15, %rbp
	movslq	%ebp, %rax
	movl	$0, (%r12,%rax,4)
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	%r12, %r9
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%r12, %r11
	movq	%r12, %rdi
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	%r12, %r10
	movq	%r12, 128(%rsp)         # 8-byte Spill
	movq	%r12, 112(%rsp)         # 8-byte Spill
	movq	%r12, 120(%rsp)         # 8-byte Spill
	movq	%r12, 136(%rsp)         # 8-byte Spill
	movq	%r12, %r8
.LBB0_217:                              #   in Loop: Header=BB0_96 Depth=2
	movq	24(%rsp), %rdx          # 8-byte Reload
	movslq	%ebp, %rax
	movq	$32, (%r8,%rax,4)
	incl	%ebp
	incq	%rdx
	movq	72(%rsp), %rcx          # 8-byte Reload
	movslq	52(%rcx), %rax
	cmpq	%rax, %rdx
	movq	%r12, %r14
	jl	.LBB0_96
.LBB0_218:                              # %._crit_edge850
                                        #   in Loop: Header=BB0_37 Depth=1
.Ltmp92:
.Lcfi52:
	.cfi_escape 0x2e, 0x00
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	%r14, %rdi
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movq	%r8, %rsi
	callq	_ZN13CStdOutStreamlsEPKw
.Ltmp93:
# BB#219:                               # %.noexc321
                                        #   in Loop: Header=BB0_37 Depth=1
	cmpl	$13, %ebp
	jg	.LBB0_223
# BB#220:                               # %.lr.ph.i320.preheader
                                        #   in Loop: Header=BB0_37 Depth=1
	movl	$14, %ebx
	subl	%ebp, %ebx
	.p2align	4, 0x90
.LBB0_221:                              # %.lr.ph.i320
                                        #   Parent Loop BB0_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp94:
.Lcfi53:
	.cfi_escape 0x2e, 0x00
	movl	$32, %esi
	movq	%r14, %rdi
	callq	_ZN13CStdOutStreamlsEc
.Ltmp95:
# BB#222:                               # %.noexc322
                                        #   in Loop: Header=BB0_221 Depth=2
	decl	%ebx
	jne	.LBB0_221
.LBB0_223:                              # %_ZL11PrintStringR13CStdOutStreamRK11CStringBaseIwEi.exit323
                                        #   in Loop: Header=BB0_37 Depth=1
.Ltmp97:
.Lcfi54:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.1, %esi
	movq	%r14, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp98:
# BB#224:                               # %.preheader801
                                        #   in Loop: Header=BB0_37 Depth=1
	movq	72(%rsp), %r15          # 8-byte Reload
	cmpq	$0, 80(%r15)
	je	.LBB0_237
# BB#225:                               # %.lr.ph855
                                        #   in Loop: Header=BB0_37 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_226:                              #   Parent Loop BB0_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	88(%r15), %rax
	movzbl	(%rax,%rbp), %ebx
	cmpb	$33, %bl
	jb	.LBB0_229
# BB#227:                               #   in Loop: Header=BB0_226 Depth=2
	testb	%bl, %bl
	js	.LBB0_229
# BB#228:                               #   in Loop: Header=BB0_226 Depth=2
.Ltmp104:
.Lcfi55:
	.cfi_escape 0x2e, 0x00
	movsbl	%bl, %esi
	movq	%r14, %rdi
	callq	_ZN13CStdOutStreamlsEc
.Ltmp105:
	jmp	.LBB0_235
	.p2align	4, 0x90
.LBB0_229:                              #   in Loop: Header=BB0_226 Depth=2
	movl	%ebx, %eax
	shrb	$4, %al
	cmpb	$-96, %bl
	movb	$48, %cl
	jb	.LBB0_231
# BB#230:                               #   in Loop: Header=BB0_226 Depth=2
	movb	$55, %cl
.LBB0_231:                              #   in Loop: Header=BB0_226 Depth=2
	addb	%al, %cl
	movzbl	%cl, %esi
.Ltmp100:
.Lcfi56:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN13CStdOutStreamlsEc
.Ltmp101:
# BB#232:                               #   in Loop: Header=BB0_226 Depth=2
	andb	$15, %bl
	cmpb	$10, %bl
	movb	$48, %al
	jb	.LBB0_234
# BB#233:                               #   in Loop: Header=BB0_226 Depth=2
	movb	$55, %al
.LBB0_234:                              #   in Loop: Header=BB0_226 Depth=2
	addb	%bl, %al
	movzbl	%al, %esi
.Ltmp102:
.Lcfi57:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN13CStdOutStreamlsEc
.Ltmp103:
.LBB0_235:                              #   in Loop: Header=BB0_226 Depth=2
.Ltmp106:
.Lcfi58:
	.cfi_escape 0x2e, 0x00
	movl	$32, %esi
	movq	%r14, %rdi
	callq	_ZN13CStdOutStreamlsEc
.Ltmp107:
# BB#236:                               #   in Loop: Header=BB0_226 Depth=2
	incq	%rbp
	cmpq	80(%r15), %rbp
	jb	.LBB0_226
.LBB0_237:                              # %._crit_edge856
                                        #   in Loop: Header=BB0_37 Depth=1
.Ltmp109:
.Lcfi59:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%r14, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp110:
# BB#238:                               #   in Loop: Header=BB0_37 Depth=1
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB0_240
# BB#239:                               #   in Loop: Header=BB0_37 Depth=1
.Lcfi60:
	.cfi_escape 0x2e, 0x00
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
.LBB0_240:                              # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB0_37 Depth=1
	movq	456(%rsp), %rsi         # 8-byte Reload
	incq	%rsi
	movq	40(%rsp), %rcx          # 8-byte Reload
	movslq	28(%rcx), %rax
	cmpq	%rax, %rsi
	jl	.LBB0_37
.LBB0_241:                              # %._crit_edge859
.Ltmp112:
.Lcfi61:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%r14, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp113:
# BB#242:
.Ltmp114:
.Lcfi62:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.3, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp115:
# BB#243:
	xorl	%ebx, %ebx
.Ltmp116:
.Lcfi63:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp117:
	jmp	.LBB0_484
.LBB0_244:
	movq	1320(%rsp), %rdi
.Ltmp37:
.Lcfi64:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.4, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp38:
# BB#245:                               # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	8(%rcx), %rdi
	movl	1308(%rsp), %esi
	movl	1312(%rsp), %edx
	movl	1316(%rsp), %ecx
	testl	%eax, %eax
	je	.LBB0_321
# BB#246:
.Ltmp39:
.Lcfi65:
	.cfi_escape 0x2e, 0x00
	callq	_Z12LzmaBenchConP8_IO_FILEjjj
	movl	%eax, %ebx
.Ltmp40:
# BB#247:
	testl	%ebx, %ebx
	je	.LBB0_483
# BB#248:
	cmpl	$1, %ebx
	jne	.LBB0_706
# BB#249:
	movl	$2, %ebx
.Ltmp41:
.Lcfi66:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.6, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp42:
	jmp	.LBB0_484
.LBB0_250:
	cmpl	$6, %eax
	sete	%al
	orb	%bl, %al
	cmpb	$1, %al
	jne	.LBB0_257
# BB#251:
	testb	%bl, %bl
	je	.LBB0_325
# BB#252:
.Ltmp301:
.Lcfi67:
	.cfi_escape 0x2e, 0x00
	movl	$80, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp302:
# BB#253:
	movl	$0, 16(%rbp)
	movl	$_ZTV23CExtractCallbackConsole+168, %eax
	movd	%rax, %xmm0
	movl	$_ZTV23CExtractCallbackConsole+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%rbp)
.Ltmp304:
.Lcfi68:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp305:
# BB#254:
	movq	%rax, 24(%rbp)
	movl	$0, (%rax)
	movl	$4, 36(%rbp)
.Ltmp307:
.Lcfi69:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	*_ZTV23CExtractCallbackConsole+24(%rip)
.Ltmp308:
	movq	8(%rsp), %rax           # 8-byte Reload
# BB#255:                               # %_ZN9CMyComPtrI29IFolderArchiveExtractCallbackEC2EPS0_.exit
	movq	%rax, 72(%rbp)
	movb	816(%rsp), %al
	movb	%al, 20(%rbp)
	movl	$0, 32(%rbp)
	movq	24(%rbp), %rbx
	movl	$0, (%rbx)
	movslq	832(%rsp), %r13
	incq	%r13
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	36(%rbp), %r12d
	cmpl	%r12d, %r13d
	jne	.LBB0_362
# BB#256:
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB0_411
.LBB0_257:
.Ltmp119:
.Lcfi70:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZNK15CArchiveCommand17IsFromUpdateGroupEv
.Ltmp120:
# BB#258:
	testb	%al, %al
	je	.LBB0_707
# BB#259:
	cmpb	$0, 1168(%rsp)
	je	.LBB0_268
# BB#260:
	cmpl	$0, 1184(%rsp)
	jne	.LBB0_268
# BB#261:                               # %_Z11MyStringLenIwEiPKT_.exit.i466
	movl	$0, 1184(%rsp)
	movq	1176(%rsp), %rbx
	movl	$0, (%rbx)
	movl	1188(%rsp), %ebp
	cmpl	$10, %ebp
	je	.LBB0_267
# BB#262:
.Ltmp129:
.Lcfi71:
	.cfi_escape 0x2e, 0x00
	movl	$40, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp130:
# BB#263:                               # %.noexc475
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB0_266
# BB#264:                               # %.noexc475
	testl	%ebp, %ebp
	jle	.LBB0_266
# BB#265:                               # %._crit_edge.thread.i.i470
.Lcfi72:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	1184(%rsp), %rax
.LBB0_266:                              # %._crit_edge16.i.i471
	movq	%r14, 1176(%rsp)
	movl	$0, (%r14,%rax,4)
	movl	$10, 1188(%rsp)
	movq	%r14, %rbx
.LBB0_267:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i474.preheader
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [55,122,67,111]
	movups	%xmm0, (%rbx)
	movdqa	.LCPI0_1(%rip), %xmm0   # xmm0 = [110,46,115,102]
	movdqu	%xmm0, 16(%rbx)
	movl	$120, 32(%rbx)
	movl	$0, 36(%rbx)
	movl	$9, 1184(%rsp)
.LBB0_268:
	movq	$_ZTV20COpenCallbackConsole+16, 344(%rsp)
	movb	$0, 360(%rsp)
	movb	$0, 361(%rsp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 368(%rsp)
.Ltmp131:
.Lcfi73:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
.Ltmp132:
# BB#269:
	movq	%r13, 368(%rsp)
	movl	$4, 380(%rsp)
	movslq	832(%rsp), %rbx
	testq	%rbx, %rbx
	setne	%al
	cmpb	$0, 816(%rsp)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, 352(%rsp)
	setne	%r12b
	andb	%al, %r12b
	movb	%r12b, 360(%rsp)
	movl	$0, 376(%rsp)
	movl	$0, (%r13)
	incq	%rbx
	cmpl	$4, %ebx
	je	.LBB0_272
# BB#270:
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp134:
.Lcfi74:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %r14
.Ltmp135:
# BB#271:                               # %._crit_edge16.i.i482
.Lcfi75:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	_ZdaPv
	movslq	376(%rsp), %rax
	movq	%r14, 368(%rsp)
	movl	$0, (%r14,%rax,4)
	movl	%ebx, 380(%rsp)
	movq	%r14, %r13
.LBB0_272:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i483
	leaq	968(%rsp), %r14
	movq	824(%rsp), %rbp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_273:                              # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rax), %ecx
	movl	%ecx, (%r13,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB0_273
# BB#274:
	movslq	832(%rsp), %r15
	movl	%r15d, 376(%rsp)
	movq	$_ZTV22CUpdateCallbackConsole+16, 464(%rsp)
	movq	$65536, 472(%rsp)       # imm = 0x10000
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 480(%rsp)
	movq	$1, 496(%rsp)
	movl	$0, 504(%rsp)
	movb	$0, 522(%rsp)
	movb	$1, 536(%rsp)
	movb	$0, 537(%rsp)
	movb	$0, 538(%rsp)
	movdqu	%xmm0, 544(%rsp)
.Ltmp136:
.Lcfi76:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp137:
# BB#275:
	movq	%rbx, 544(%rsp)
	movl	$4, 556(%rsp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 576(%rsp)
	movq	$8, 592(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 568(%rsp)
	movdqu	%xmm0, 608(%rsp)
	movq	$4, 624(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 600(%rsp)
	movdqu	%xmm0, 640(%rsp)
	movq	$8, 656(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 632(%rsp)
	movdqu	%xmm0, 672(%rsp)
	movq	$4, 688(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 664(%rsp)
	movb	1304(%rsp), %al
	movb	%al, 536(%rsp)
	testl	%r15d, %r15d
	sete	%al
	cmpb	$0, 816(%rsp)
	movb	%r12b, 538(%rsp)
	setne	%cl
	andb	%al, %cl
	movb	%cl, 560(%rsp)
	movl	$0, 552(%rsp)
	movl	$0, (%rbx)
	incq	%r15
	cmpl	$4, %r15d
	movq	8(%rsp), %r13           # 8-byte Reload
	je	.LBB0_278
# BB#276:
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp139:
.Lcfi77:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %r12
.Ltmp140:
# BB#277:                               # %._crit_edge16.i.i493
.Lcfi78:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	552(%rsp), %rax
	movq	824(%rsp), %rbp
	movq	%r12, 544(%rsp)
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 556(%rsp)
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB0_278:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i494
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %eax
	addq	$4, %rbp
	movl	%eax, (%rbx)
	addq	$4, %rbx
	testl	%eax, %eax
	jne	.LBB0_278
# BB#279:
	movl	832(%rsp), %eax
	movl	%eax, 552(%rsp)
	movb	1216(%rsp), %al
	movb	%al, 537(%rsp)
	movb	$0, 520(%rsp)
	movb	$0, 521(%rsp)
	leaq	568(%rsp), %rdi
.Ltmp141:
.Lcfi79:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp142:
# BB#280:                               # %.noexc499
	leaq	600(%rsp), %rdi
.Ltmp143:
.Lcfi80:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp144:
# BB#281:
	movq	%r13, 528(%rsp)
	movq	%r13, 512(%rsp)
	movl	$0, 280(%rsp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 288(%rsp)
.Ltmp145:
.Lcfi81:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp146:
# BB#282:                               # %.noexc504
	movq	%rbx, 288(%rsp)
	movl	$0, (%rbx)
	movl	$4, 300(%rsp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 304(%rsp)
.Ltmp148:
.Lcfi82:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp149:
# BB#283:
	movq	%rbp, 304(%rsp)
	movl	$0, (%rbp)
	movl	$4, 316(%rsp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 320(%rsp)
.Ltmp151:
.Lcfi83:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp152:
# BB#284:
	movq	%rax, 320(%rsp)
	movl	$0, (%rax)
	movl	$4, 332(%rsp)
	leaq	800(%rsp), %rcx
.Ltmp154:
.Lcfi84:
	.cfi_escape 0x2e, 0x00
	leaq	424(%rsp), %rdx
	movq	%r14, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	_ZN14CUpdateOptions4InitEPK7CCodecsRK13CRecordVectorIiERK11CStringBaseIwE
.Ltmp155:
# BB#285:
	testb	%al, %al
	je	.LBB0_712
# BB#286:
	leaq	760(%rsp), %rsi
.Ltmp158:
.Lcfi85:
	.cfi_escape 0x2e, 0x00
	leaq	280(%rsp), %rcx
	leaq	344(%rsp), %r8
	leaq	464(%rsp), %r9
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rdx
	callq	_Z13UpdateArchiveP7CCodecsRKN9NWildcard7CCensorER14CUpdateOptionsR16CUpdateErrorInfoP15IOpenCallbackUIP18IUpdateCallbackUI2
	movl	%eax, 32(%rsp)          # 4-byte Spill
.Ltmp159:
# BB#287:
	cmpb	$0, 1168(%rsp)
	je	.LBB0_297
# BB#288:                               # %.preheader798
	movl	1020(%rsp), %ecx
	testl	%ecx, %ecx
	jle	.LBB0_297
# BB#289:                               # %.lr.ph844
	xorl	%ebp, %ebp
	leaq	144(%rsp), %rbx
	.p2align	4, 0x90
.LBB0_290:                              # =>This Inner Loop Header: Depth=1
	cmpb	$0, 1216(%rsp)
	jne	.LBB0_296
# BB#291:                               #   in Loop: Header=BB0_290 Depth=1
	movq	1024(%rsp), %rax
	movq	(%rax,%rbp,8), %rsi
	addq	$16, %rsi
.Ltmp161:
.Lcfi86:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZNK12CArchivePath12GetFinalPathEv
.Ltmp162:
# BB#292:                               #   in Loop: Header=BB0_290 Depth=1
.Ltmp164:
.Lcfi87:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_Z12myAddExeFlagRK11CStringBaseIwE
.Ltmp165:
# BB#293:                               #   in Loop: Header=BB0_290 Depth=1
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_295
# BB#294:                               #   in Loop: Header=BB0_290 Depth=1
.Lcfi88:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_295:                              # %_ZN11CStringBaseIwED2Ev.exit507
                                        #   in Loop: Header=BB0_290 Depth=1
	movl	1020(%rsp), %ecx
.LBB0_296:                              #   in Loop: Header=BB0_290 Depth=1
	incq	%rbp
	movslq	%ecx, %rax
	cmpq	%rax, %rbp
	jl	.LBB0_290
.LBB0_297:                              # %.loopexit799
	cmpl	$0, 644(%rsp)
	movq	8(%rsp), %r12           # 8-byte Reload
	jle	.LBB0_331
# BB#298:
.Ltmp167:
.Lcfi89:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp168:
# BB#299:
.Ltmp169:
.Lcfi90:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.17, %esi
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp170:
# BB#300:
.Ltmp171:
.Lcfi91:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp172:
# BB#301:
.Ltmp173:
.Lcfi92:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp174:
# BB#302:
	movslq	644(%rsp), %r15
	testq	%r15, %r15
	jle	.LBB0_313
# BB#303:                               # %.lr.ph841
	xorl	%ebx, %ebx
	leaq	240(%rsp), %r14
	.p2align	4, 0x90
.LBB0_304:                              # =>This Inner Loop Header: Depth=1
	movq	648(%rsp), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax), %rsi
.Ltmp175:
.Lcfi93:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPKw
.Ltmp176:
# BB#305:                               #   in Loop: Header=BB0_304 Depth=1
.Ltmp177:
.Lcfi94:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.18, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp178:
# BB#306:                               #   in Loop: Header=BB0_304 Depth=1
	movq	680(%rsp), %rax
	movl	(%rax,%rbx,4), %ebp
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 240(%rsp)
.Ltmp179:
.Lcfi95:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp180:
# BB#307:                               # %.noexc509
                                        #   in Loop: Header=BB0_304 Depth=1
	movq	%rax, 240(%rsp)
	movl	$0, (%rax)
	movl	$4, 252(%rsp)
.Ltmp182:
.Lcfi96:
	.cfi_escape 0x2e, 0x00
	movl	%ebp, %edi
	movq	%r14, %rsi
	callq	_ZN8NWindows6NError15MyFormatMessageEjR11CStringBaseIwE
.Ltmp183:
# BB#308:                               # %_ZN8NWindows6NError16MyFormatMessageWEj.exit
                                        #   in Loop: Header=BB0_304 Depth=1
	movq	240(%rsp), %rsi
.Ltmp185:
.Lcfi97:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPKw
.Ltmp186:
# BB#309:                               #   in Loop: Header=BB0_304 Depth=1
.Ltmp187:
.Lcfi98:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp188:
# BB#310:                               #   in Loop: Header=BB0_304 Depth=1
	movq	240(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_312
# BB#311:                               #   in Loop: Header=BB0_304 Depth=1
.Lcfi99:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_312:                              # %_ZN11CStringBaseIwED2Ev.exit512
                                        #   in Loop: Header=BB0_304 Depth=1
	incq	%rbx
	cmpq	%r15, %rbx
	jl	.LBB0_304
.LBB0_313:                              # %._crit_edge842
.Ltmp190:
.Lcfi100:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.19, %esi
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp191:
# BB#314:
.Ltmp192:
.Lcfi101:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp193:
# BB#315:
.Ltmp194:
.Lcfi102:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.20, %esi
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp195:
# BB#316:
.Ltmp196:
.Lcfi103:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	movl	%r15d, %esi
	callq	_ZN13CStdOutStreamlsEi
.Ltmp197:
# BB#317:
.Ltmp198:
.Lcfi104:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.21, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp199:
# BB#318:
	cmpl	$2, %r15d
	jl	.LBB0_320
# BB#319:
.Ltmp200:
.Lcfi105:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.22, %esi
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp201:
.LBB0_320:
	movl	$1, %ebx
.Ltmp202:
.Lcfi106:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp203:
	jmp	.LBB0_332
.LBB0_321:
.Ltmp46:
.Lcfi107:
	.cfi_escape 0x2e, 0x00
	callq	_Z11CrcBenchConP8_IO_FILEjjj
	movl	%eax, %ebx
.Ltmp47:
# BB#322:
	testl	%ebx, %ebx
	je	.LBB0_483
# BB#323:
	cmpl	$1, %ebx
	jne	.LBB0_713
# BB#324:
	movl	$2, %ebx
.Ltmp48:
.Lcfi108:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.5, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp49:
	jmp	.LBB0_484
.LBB0_325:
	movq	$0, 336(%rsp)
	movq	776(%rsp), %rax
	movq	(%rax), %r9
	leaq	904(%rsp), %r8
	leaq	872(%rsp), %rcx
	addq	$16, %r9
	leaq	816(%rsp), %r10
	leaq	824(%rsp), %r11
	movzbl	749(%rsp), %edx
	movzbl	751(%rsp), %ebx
	movzbl	840(%rsp), %eax
.Ltmp289:
.Lcfi109:
	.cfi_escape 0x2e, 0x30
	subq	$8, %rsp
.Lcfi110:
	.cfi_adjust_cfa_offset 8
	leaq	344(%rsp), %rbp
	leaq	432(%rsp), %rsi
	movq	48(%rsp), %rdi          # 8-byte Reload
	pushq	%rbp
.Lcfi111:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi112:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi113:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi114:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi115:
	.cfi_adjust_cfa_offset 8
	callq	_Z12ListArchivesP7CCodecsRK13CRecordVectorIiEbR13CObjectVectorI11CStringBaseIwEES9_RKN9NWildcard11CCensorNodeEbbRbRS7_Ry
	addq	$48, %rsp
.Lcfi116:
	.cfi_adjust_cfa_offset -48
	movl	%eax, %ebx
.Ltmp290:
# BB#326:
	cmpq	$0, 336(%rsp)
	je	.LBB0_367
# BB#327:
.Ltmp292:
.Lcfi117:
	.cfi_escape 0x2e, 0x00
	movl	$g_StdOut, %edi
	movl	$_Z4endlR13CStdOutStream, %esi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp293:
# BB#328:
.Ltmp294:
.Lcfi118:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.16, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp295:
# BB#329:
	movq	336(%rsp), %rsi
.Ltmp296:
.Lcfi119:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEy
.Ltmp297:
# BB#330:                               # %.critedge308
	movl	$2, %ebx
	jmp	.LBB0_484
.LBB0_331:
	xorl	%ebx, %ebx
.LBB0_332:
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	je	.LBB0_338
# BB#333:
.Ltmp205:
.Lcfi120:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp206:
# BB#334:
	movl	$0, (%r15)
	movl	328(%rsp), %eax
	testl	%eax, %eax
	je	.LBB0_366
# BB#335:
	cmpl	$4, %eax
	jl	.LBB0_337
# BB#336:
	leal	2(%rax), %ecx
	cmpl	$7, %eax
	movl	$9, %ebp
	cmovgl	%ecx, %ebp
	cmpl	$4, %ebp
	jne	.LBB0_381
.LBB0_337:
	movl	$4, %ebp
	movq	%r15, %rbx
	jmp	.LBB0_383
.LBB0_338:
	movslq	580(%rsp), %r15
	testq	%r15, %r15
	je	.LBB0_369
# BB#339:
.Ltmp242:
.Lcfi121:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp243:
# BB#340:
.Ltmp244:
.Lcfi122:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.17, %esi
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp245:
# BB#341:
.Ltmp246:
.Lcfi123:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp247:
# BB#342:
.Ltmp248:
.Lcfi124:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp249:
# BB#343:                               # %.preheader
	testl	%r15d, %r15d
	jle	.LBB0_354
# BB#344:                               # %.lr.ph
	xorl	%ebp, %ebp
	leaq	224(%rsp), %r14
.LBB0_345:                              # =>This Inner Loop Header: Depth=1
	movq	584(%rsp), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax), %rsi
.Ltmp250:
.Lcfi125:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPKw
.Ltmp251:
# BB#346:                               #   in Loop: Header=BB0_345 Depth=1
.Ltmp252:
.Lcfi126:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.18, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp253:
# BB#347:                               #   in Loop: Header=BB0_345 Depth=1
	movq	616(%rsp), %rax
	movl	(%rax,%rbp,4), %ebx
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 224(%rsp)
.Ltmp254:
.Lcfi127:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp255:
# BB#348:                               # %.noexc690
                                        #   in Loop: Header=BB0_345 Depth=1
	movq	%rax, 224(%rsp)
	movl	$0, (%rax)
	movl	$4, 236(%rsp)
.Ltmp257:
.Lcfi128:
	.cfi_escape 0x2e, 0x00
	movl	%ebx, %edi
	movq	%r14, %rsi
	callq	_ZN8NWindows6NError15MyFormatMessageEjR11CStringBaseIwE
.Ltmp258:
# BB#349:                               # %_ZN8NWindows6NError16MyFormatMessageWEj.exit693
                                        #   in Loop: Header=BB0_345 Depth=1
	movq	224(%rsp), %rsi
.Ltmp260:
.Lcfi129:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPKw
.Ltmp261:
# BB#350:                               #   in Loop: Header=BB0_345 Depth=1
.Ltmp262:
.Lcfi130:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp263:
# BB#351:                               #   in Loop: Header=BB0_345 Depth=1
	movq	224(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_353
# BB#352:                               #   in Loop: Header=BB0_345 Depth=1
.Lcfi131:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_353:                              # %_ZN11CStringBaseIwED2Ev.exit694
                                        #   in Loop: Header=BB0_345 Depth=1
	incq	%rbp
	cmpq	%r15, %rbp
	jl	.LBB0_345
.LBB0_354:                              # %._crit_edge
.Ltmp265:
.Lcfi132:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.19, %esi
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp266:
# BB#355:
.Ltmp267:
.Lcfi133:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp268:
# BB#356:
.Ltmp269:
.Lcfi134:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.25, %esi
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp270:
# BB#357:
.Ltmp271:
.Lcfi135:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	movl	%r15d, %esi
	callq	_ZN13CStdOutStreamlsEi
.Ltmp272:
# BB#358:
.Ltmp273:
.Lcfi136:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.21, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp274:
# BB#359:
	cmpl	$2, %r15d
	jl	.LBB0_361
# BB#360:
.Ltmp275:
.Lcfi137:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.22, %esi
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp276:
.LBB0_361:
	movl	$1, %ebx
.Ltmp277:
.Lcfi138:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp278:
	jmp	.LBB0_372
.LBB0_362:
	movl	$4, %ecx
	movq	%r13, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp310:
.Lcfi139:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %r14
.Ltmp311:
# BB#363:                               # %.noexc406
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB0_409
# BB#364:                               # %.noexc406
	testl	%r12d, %r12d
	movq	16(%rsp), %r12          # 8-byte Reload
	jle	.LBB0_410
# BB#365:                               # %._crit_edge.thread.i.i402
.Lcfi140:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	32(%r12), %rax
	jmp	.LBB0_410
.LBB0_366:
	xorl	%ebp, %ebp
	movl	$4, %r13d
	movq	%r15, %r12
	jmp	.LBB0_398
.LBB0_367:
	testl	%ebx, %ebx
	je	.LBB0_483
# BB#368:
.Lcfi141:
	.cfi_escape 0x2e, 0x00
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	%ebx, (%rax)
.Ltmp298:
.Lcfi142:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTI16CSystemException, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp299:
	jmp	.LBB0_662
.LBB0_369:
	cmpl	$0, 644(%rsp)
	jne	.LBB0_372
# BB#370:
.Ltmp279:
.Lcfi143:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.39, %esi
	movq	%r12, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp280:
# BB#371:
.Ltmp281:
.Lcfi144:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp282:
.LBB0_372:
	movq	320(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_374
# BB#373:
.Lcfi145:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_374:                              # %_ZN11CStringBaseIwED2Ev.exit.i696
	movq	304(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_376
# BB#375:
.Lcfi146:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_376:                              # %_ZN11CStringBaseIwED2Ev.exit1.i
	movq	288(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_378
# BB#377:
.Lcfi147:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_378:                              # %_ZN10CErrorInfoD2Ev.exit
.Ltmp286:
.Lcfi148:
	.cfi_escape 0x2e, 0x00
	leaq	464(%rsp), %rdi
	callq	_ZN22CUpdateCallbackConsoleD2Ev
.Ltmp287:
# BB#379:
	movq	$_ZTV20COpenCallbackConsole+16, 344(%rsp)
	movq	368(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_484
# BB#380:
.Lcfi149:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB0_484
.LBB0_381:
	movq	%r15, %r12
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp208:
.Lcfi150:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %rbx
.Ltmp209:
# BB#382:                               # %._crit_edge16.i.i532
.Lcfi151:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	movq	%rbx, %r15
.LBB0_383:                              # %.noexc519
	movq	320(%rsp), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_384:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rcx), %edx
	movl	%edx, (%r15,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB0_384
# BB#385:                               # %_Z11MyStringLenIwEiPKT_.exit.i538
	movslq	328(%rsp), %r14
	movl	%ebp, %eax
	subl	%r14d, %eax
	cmpl	$1, %eax
	jle	.LBB0_387
# BB#386:
	movl	%ebp, %r13d
	jmp	.LBB0_397
.LBB0_387:
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	cmpl	$8, %ebp
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebp
	jl	.LBB0_389
# BB#388:                               # %select.true.sink2612
	movl	%ebp, %ecx
	shrl	$31, %ecx
	addl	%ebp, %ecx
	sarl	%ecx
.LBB0_389:                              # %select.end2611
	leal	-1(%rcx,%rax), %edx
	movl	$2, %esi
	subl	%eax, %esi
	testl	%edx, %edx
	cmovgl	%ecx, %esi
	leal	1(%rbp,%rsi), %r13d
	cmpl	%ebp, %r13d
	jne	.LBB0_391
# BB#390:
	movl	%ebp, %r13d
	movq	24(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB0_397
.LBB0_391:
	movslq	%r13d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp210:
.Lcfi152:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %rbx
.Ltmp211:
# BB#392:                               # %.noexc556
	testl	%ebp, %ebp
	jle	.LBB0_396
# BB#393:                               # %.preheader.i.i546
	testl	%r14d, %r14d
	movq	24(%rsp), %rbp          # 8-byte Reload
	jle	.LBB0_395
# BB#394:                               # %.lr.ph.i.i547
	leaq	(,%r14,4), %rdx
.Lcfi153:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	memcpy
.LBB0_395:                              # %._crit_edge.thread.i.i553
.Lcfi154:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdaPv
.LBB0_396:                              # %._crit_edge16.i.i554
	movl	$0, (%rbx,%r14,4)
	movq	%rbx, %r15
.LBB0_397:                              # %.noexc541
	movq	%r15, %r12
	movq	$10, (%r12,%r14,4)
	leal	1(%r14), %ebp
	movq	%rbx, %r15
.LBB0_398:
	movl	296(%rsp), %eax
	testl	%eax, %eax
	je	.LBB0_559
# BB#399:
	movl	%r13d, %ecx
	subl	%ebp, %ecx
	cmpl	%eax, %ecx
	jg	.LBB0_403
# BB#400:
	decl	%ecx
	cmpl	$8, %r13d
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %r13d
	jl	.LBB0_402
# BB#401:                               # %select.true.sink2614
	movl	%r13d, %edx
	shrl	$31, %edx
	addl	%r13d, %edx
	sarl	%edx
.LBB0_402:                              # %select.end2613
	movl	%eax, %esi
	subl	%ecx, %esi
	addl	%edx, %ecx
	cmpl	%eax, %ecx
	cmovgel	%edx, %esi
	leal	1(%r13,%rsi), %ebx
	cmpl	%r13d, %ebx
	jne	.LBB0_404
.LBB0_403:
	movl	%r13d, %ebx
	movq	%r15, %r14
	movq	%r12, %r15
	jmp	.LBB0_533
.LBB0_404:
	movslq	%ebx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp213:
.Lcfi155:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
.Ltmp214:
# BB#405:                               # %.noexc575
	testl	%r13d, %r13d
	movq	%rax, %r14
	jle	.LBB0_532
# BB#406:                               # %.preheader.i.i565
	testl	%ebp, %ebp
	jle	.LBB0_531
# BB#407:                               # %.lr.ph.i.i566
	movslq	%ebp, %rax
	cmpl	$8, %ebp
	jae	.LBB0_512
# BB#408:
	xorl	%ecx, %ecx
	jmp	.LBB0_525
.LBB0_409:
	movq	16(%rsp), %r12          # 8-byte Reload
.LBB0_410:                              # %._crit_edge16.i.i403
	movq	%r14, 24(%r12)
	movl	$0, (%r14,%rax,4)
	movl	%r13d, 36(%r12)
	movq	%r14, %rbx
.LBB0_411:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	824(%rsp), %rbp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_412:                              # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB0_412
# BB#413:
	movslq	832(%rsp), %rbx
	movl	%ebx, 32(%r12)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 56(%r12)
	movdqu	%xmm0, 40(%r12)
	movq	$_ZTV20COpenCallbackConsole+16, 384(%rsp)
	movb	$0, 400(%rsp)
	movb	$0, 401(%rsp)
	movdqu	%xmm0, 408(%rsp)
.Ltmp312:
.Lcfi156:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp313:
# BB#414:
	movq	%r14, 408(%rsp)
	movl	$4, 420(%rsp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 392(%rsp)
	movb	816(%rsp), %al
	movb	%al, 400(%rsp)
	movl	$0, 416(%rsp)
	movl	$0, (%r14)
	incq	%rbx
	cmpl	$4, %ebx
	je	.LBB0_417
# BB#415:
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp315:
.Lcfi157:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %r12
.Ltmp316:
# BB#416:                               # %._crit_edge16.i.i412
.Lcfi158:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZdaPv
	movslq	416(%rsp), %rax
	movq	824(%rsp), %rbp
	movq	%r12, 408(%rsp)
	movl	$0, (%r12,%rax,4)
	movl	%ebx, 420(%rsp)
	movq	%r12, %r14
.LBB0_417:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i413
	movq	16(%rsp), %rax          # 8-byte Reload
	addq	$40, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB0_418:                              # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %ecx
	addq	$4, %rbp
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB0_418
# BB#419:
	movl	832(%rsp), %eax
	movl	%eax, 416(%rsp)
	movb	$0, 148(%rsp)
	movl	$0, 144(%rsp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 152(%rsp)
	movq	$0, 168(%rsp)
.Ltmp317:
.Lcfi159:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp318:
# BB#420:
	movq	%rax, 160(%rsp)
	movl	$0, (%rax)
	movl	$4, 172(%rsp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 184(%rsp)
	movq	$8, 200(%rsp)
	movq	$_ZTV13CObjectVectorI9CPropertyE+16, 176(%rsp)
	movb	749(%rsp), %al
	movb	%al, 144(%rsp)
	movb	750(%rsp), %al
	movb	%al, 145(%rsp)
.Ltmp320:
.Lcfi160:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZNK15CArchiveCommand11GetPathModeEv
.Ltmp321:
# BB#421:
	cmpl	$3, 792(%rsp)
	movl	%eax, 152(%rsp)
	sete	147(%rsp)
	movl	864(%rsp), %eax
	movl	%eax, 156(%rsp)
	movl	$0, 168(%rsp)
	movq	160(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	856(%rsp), %rbp
	incq	%rbp
	movl	172(%rsp), %r15d
	cmpl	%r15d, %ebp
	je	.LBB0_427
# BB#422:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp322:
.Lcfi161:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %r14
.Ltmp323:
# BB#423:                               # %.noexc427
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB0_426
# BB#424:                               # %.noexc427
	testl	%r15d, %r15d
	jle	.LBB0_426
# BB#425:                               # %._crit_edge.thread.i.i422
.Lcfi162:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	168(%rsp), %rax
.LBB0_426:                              # %._crit_edge16.i.i423
	movq	%r14, 160(%rsp)
	movl	$0, (%r14,%rax,4)
	movl	%ebp, 172(%rsp)
	movq	%r14, %rbx
.LBB0_427:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i424
	movq	848(%rsp), %rax
	.p2align	4, 0x90
.LBB0_428:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB0_428
# BB#429:
	movl	856(%rsp), %eax
	movl	%eax, 168(%rsp)
	movb	752(%rsp), %al
	movb	%al, 146(%rsp)
	movb	841(%rsp), %al
	movb	%al, 148(%rsp)
	leaq	176(%rsp), %rbx
.Ltmp324:
.Lcfi163:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp325:
# BB#430:                               # %.noexc429
	movslq	948(%rsp), %rbp
	movl	188(%rsp), %esi
	addl	%ebp, %esi
.Ltmp326:
.Lcfi164:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp327:
# BB#431:                               # %.noexc435
	testl	%ebp, %ebp
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	jle	.LBB0_448
# BB#432:                               # %.lr.ph.i431
	xorl	%r12d, %r12d
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_433:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_439 Depth 2
                                        #     Child Loop BB0_445 Depth 2
	movq	952(%rsp), %rax
	movq	(%rax,%r12,8), %r13
.Ltmp328:
.Lcfi165:
	.cfi_escape 0x2e, 0x00
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp329:
# BB#434:                               # %.noexc436
                                        #   in Loop: Header=BB0_433 Depth=1
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%r15)
	movslq	8(%r13), %rsi
	leaq	1(%rsi), %rbx
	testl	%ebx, %ebx
	je	.LBB0_437
# BB#435:                               # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB0_433 Depth=1
	movq	%r12, %rbp
	movq	%rsi, %r12
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp330:
.Lcfi166:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
.Ltmp331:
# BB#436:                               # %.noexc444
                                        #   in Loop: Header=BB0_433 Depth=1
	movq	%rax, (%r15)
	movl	$0, (%rax)
	movl	%ebx, 12(%r15)
	movq	%rax, %r14
	movq	%r12, %rsi
	movq	%rbp, %r12
	jmp	.LBB0_438
.LBB0_437:                              #   in Loop: Header=BB0_433 Depth=1
	xorl	%eax, %eax
	xorl	%r14d, %r14d
.LBB0_438:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB0_433 Depth=1
	movq	(%r13), %rdi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_439:                              #   Parent Loop BB0_433 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rcx), %edx
	movl	%edx, (%r14,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB0_439
# BB#440:                               # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
                                        #   in Loop: Header=BB0_433 Depth=1
	movl	%esi, 8(%r15)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 16(%r15)
	movslq	24(%r13), %rbp
	leaq	1(%rbp), %rbx
	testl	%ebx, %ebx
	je	.LBB0_443
# BB#441:                               # %._crit_edge16.i.i4.i
                                        #   in Loop: Header=BB0_433 Depth=1
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp333:
.Lcfi167:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rcx
.Ltmp334:
# BB#442:                               # %.noexc.i
                                        #   in Loop: Header=BB0_433 Depth=1
	movq	%rcx, 16(%r15)
	movl	$0, (%rcx)
	movl	%ebx, 28(%r15)
	jmp	.LBB0_444
.LBB0_443:                              #   in Loop: Header=BB0_433 Depth=1
	xorl	%ecx, %ecx
.LBB0_444:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i5.i
                                        #   in Loop: Header=BB0_433 Depth=1
	movq	16(%r13), %rsi
	leaq	176(%rsp), %rdi
	movq	32(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_445:                              #   Parent Loop BB0_433 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB0_445
# BB#446:                               # %_ZN13CObjectVectorI9CPropertyE3AddERKS0_.exit.i
                                        #   in Loop: Header=BB0_433 Depth=1
	movl	%ebp, 24(%r15)
.Ltmp336:
.Lcfi168:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp337:
# BB#447:                               # %.noexc439
                                        #   in Loop: Header=BB0_433 Depth=1
	movq	192(%rsp), %rax
	movslq	188(%rsp), %rcx
	movq	%r15, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 188(%rsp)
	incq	%r12
	cmpq	%rbx, %r12
	jl	.LBB0_433
.LBB0_448:                              # %_ZN13CObjectVectorI9CPropertyEaSERKS1_.exit
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 256(%rsp)
.Ltmp339:
.Lcfi169:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	movq	16(%rsp), %r15          # 8-byte Reload
	callq	_Znam
.Ltmp340:
	movq	8(%rsp), %rbx           # 8-byte Reload
# BB#449:
	movq	%rax, 256(%rsp)
	movl	$0, (%rax)
	movl	$4, 268(%rsp)
	movq	776(%rsp), %rax
	movq	(%rax), %r8
	leaq	904(%rsp), %rcx
	leaq	872(%rsp), %rdx
	addq	$16, %r8
.Ltmp342:
.Lcfi170:
	.cfi_escape 0x2e, 0x20
	leaq	696(%rsp), %rax
	leaq	256(%rsp), %rbp
	leaq	384(%rsp), %r10
	leaq	424(%rsp), %rsi
	leaq	144(%rsp), %r9
	movq	40(%rsp), %rdi          # 8-byte Reload
	pushq	%rax
.Lcfi171:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi172:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi173:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi174:
	.cfi_adjust_cfa_offset 8
	callq	_Z18DecompressArchivesP7CCodecsRK13CRecordVectorIiER13CObjectVectorI11CStringBaseIwEES9_RKN9NWildcard11CCensorNodeERK15CExtractOptionsP15IOpenCallbackUIP18IExtractCallbackUIRS7_R15CDecompressStat
	addq	$32, %rsp
.Lcfi175:
	.cfi_adjust_cfa_offset -32
	movl	%eax, %r14d
.Ltmp343:
# BB#450:
	cmpl	$0, 264(%rsp)
	je	.LBB0_455
# BB#451:
.Ltmp345:
.Lcfi176:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rbx, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp346:
# BB#452:
.Ltmp347:
.Lcfi177:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.7, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp348:
# BB#453:
	movq	256(%rsp), %rsi
.Ltmp349:
.Lcfi178:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKw
.Ltmp350:
# BB#454:
	testl	%r14d, %r14d
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovel	%eax, %r14d
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB0_455:
.Ltmp351:
.Lcfi179:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rbx, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp352:
# BB#456:
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpq	$2, (%rax)
	jb	.LBB0_460
# BB#457:
.Ltmp353:
.Lcfi180:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.8, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp354:
# BB#458:
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rsi
.Ltmp355:
.Lcfi181:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEy
.Ltmp356:
# BB#459:
.Ltmp357:
.Lcfi182:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp358:
.LBB0_460:
	cmpq	$0, 48(%r15)
	jne	.LBB0_462
# BB#461:
	cmpq	$0, 56(%r15)
	je	.LBB0_490
.LBB0_462:
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpq	$2, (%rax)
	jb	.LBB0_472
# BB#463:
.Ltmp359:
.Lcfi183:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp360:
# BB#464:
	cmpq	$0, 48(%r15)
	je	.LBB0_468
# BB#465:
.Ltmp361:
.Lcfi184:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.9, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp362:
# BB#466:
	movq	48(%r15), %rsi
.Ltmp363:
.Lcfi185:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEy
.Ltmp364:
# BB#467:
.Ltmp365:
.Lcfi186:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp366:
.LBB0_468:
	cmpq	$0, 56(%r15)
	je	.LBB0_472
# BB#469:
.Ltmp367:
.Lcfi187:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.10, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp368:
# BB#470:
	movq	56(%r15), %rsi
.Ltmp369:
.Lcfi188:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEy
.Ltmp370:
# BB#471:
.Ltmp371:
.Lcfi189:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp372:
.LBB0_472:
	movl	$1, %ebp
	testl	%r14d, %r14d
	jne	.LBB0_717
.LBB0_473:
	movq	256(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_475
# BB#474:
.Lcfi190:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_475:                              # %_ZN11CStringBaseIwED2Ev.exit447
	movq	$_ZTV13CObjectVectorI9CPropertyE+16, 176(%rsp)
.Ltmp420:
.Lcfi191:
	.cfi_escape 0x2e, 0x00
	leaq	176(%rsp), %rbx
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp421:
# BB#476:
.Ltmp426:
.Lcfi192:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp427:
# BB#477:                               # %_ZN13CObjectVectorI9CPropertyED2Ev.exit.i
	movq	160(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_479
# BB#478:
.Lcfi193:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_479:                              # %_ZN15CExtractOptionsD2Ev.exit
	movq	$_ZTV20COpenCallbackConsole+16, 384(%rsp)
	movq	408(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_481
# BB#480:
.Lcfi194:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_481:
	movq	(%r15), %rax
.Ltmp431:
.Lcfi195:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp432:
# BB#482:                               # %_ZN9CMyComPtrI29IFolderArchiveExtractCallbackED2Ev.exit
	movl	$2, %ebx
	testl	%ebp, %ebp
	jne	.LBB0_484
.LBB0_483:
	xorl	%ebx, %ebx
.LBB0_484:                              # %.critedge
.Ltmp436:
.Lcfi196:
	.cfi_escape 0x2e, 0x00
	leaq	424(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp437:
# BB#485:
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp439:
.Lcfi197:
	.cfi_escape 0x2e, 0x00
	leaq	80(%rsp), %r15
	callq	*16(%rax)
.Ltmp440:
.LBB0_486:                              # %_ZN9CMyComPtrI8IUnknownED2Ev.exit
.Ltmp442:
.Lcfi198:
	.cfi_escape 0x2e, 0x00
	leaq	1336(%rsp), %rdi
	callq	_ZN18NCommandLineParser7CParserD1Ev
.Ltmp443:
# BB#487:                               # %_ZN25CArchiveCommandLineParserD2Ev.exit
.Ltmp445:
.Lcfi199:
	.cfi_escape 0x2e, 0x00
	leaq	744(%rsp), %rdi
	callq	_ZN26CArchiveCommandLineOptionsD2Ev
.Ltmp446:
.LBB0_488:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 80(%rsp)
.Ltmp469:
.Lcfi200:
	.cfi_escape 0x2e, 0x00
	leaq	80(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp470:
# BB#489:                               # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit312
.Lcfi201:
	.cfi_escape 0x2e, 0x00
	leaq	80(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	movl	%ebx, %eax
	addq	$1384, %rsp             # imm = 0x568
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_490:
	testl	%r14d, %r14d
	jne	.LBB0_718
# BB#491:
	cmpq	$0, 720(%rsp)
	movq	8(%rsp), %rdi           # 8-byte Reload
	je	.LBB0_496
# BB#492:
.Ltmp377:
.Lcfi202:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.11, %esi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp378:
# BB#493:
	movq	720(%rsp), %rsi
.Ltmp379:
.Lcfi203:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEy
.Ltmp380:
# BB#494:
.Ltmp381:
.Lcfi204:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp382:
# BB#495:                               # %._crit_edge897
	movq	720(%rsp), %rax
	testq	%rax, %rax
	je	.LBB0_497
	jmp	.LBB0_498
.LBB0_496:
	xorl	%eax, %eax
	testq	%rax, %rax
	jne	.LBB0_498
.LBB0_497:
	cmpq	$1, 728(%rsp)
	je	.LBB0_501
.LBB0_498:
.Ltmp383:
.Lcfi205:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.12, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp384:
# BB#499:
	movq	728(%rsp), %rsi
.Ltmp385:
.Lcfi206:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEy
.Ltmp386:
# BB#500:
.Ltmp387:
.Lcfi207:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp388:
.LBB0_501:
.Ltmp389:
.Lcfi208:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.13, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp390:
# BB#502:
	movq	704(%rsp), %rsi
.Ltmp391:
.Lcfi209:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEy
.Ltmp392:
# BB#503:
.Ltmp393:
.Lcfi210:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp394:
# BB#504:
.Ltmp395:
.Lcfi211:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.14, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp396:
# BB#505:
	movq	712(%rsp), %rsi
.Ltmp397:
.Lcfi212:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEy
.Ltmp398:
# BB#506:
.Ltmp399:
.Lcfi213:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp400:
# BB#507:
	xorl	%ebp, %ebp
	cmpb	$0, 841(%rsp)
	je	.LBB0_473
# BB#508:
	movl	736(%rsp), %edi
.Ltmp402:
.Lcfi214:
	.cfi_escape 0x2e, 0x00
	leaq	464(%rsp), %rsi
	callq	_Z27ConvertUInt32ToHexWithZerosjPc
.Ltmp403:
# BB#509:
.Ltmp404:
.Lcfi215:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.15, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp405:
# BB#510:
.Ltmp406:
.Lcfi216:
	.cfi_escape 0x2e, 0x00
	leaq	464(%rsp), %rsi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp407:
# BB#511:
.Ltmp408:
.Lcfi217:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp409:
	jmp	.LBB0_473
.LBB0_512:                              # %min.iters.checked1133
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB0_516
# BB#513:                               # %vector.memcheck1146
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %r14
	jae	.LBB0_517
# BB#514:                               # %vector.memcheck1146
	leaq	(%r14,%rax,4), %rdx
	cmpq	%rdx, %r15
	jae	.LBB0_517
.LBB0_516:
	xorl	%ecx, %ecx
.LBB0_525:                              # %scalar.ph1131.preheader
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB0_528
# BB#526:                               # %scalar.ph1131.prol.preheader
	negq	%rsi
.LBB0_527:                              # %scalar.ph1131.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12,%rcx,4), %edi
	movl	%edi, (%r14,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_527
.LBB0_528:                              # %scalar.ph1131.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB0_531
# BB#529:                               # %scalar.ph1131.preheader.new
	subq	%rcx, %rax
	leaq	28(%r14,%rcx,4), %rdx
	leaq	28(%r12,%rcx,4), %rcx
.LBB0_530:                              # %scalar.ph1131
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB0_530
.LBB0_531:                              # %._crit_edge.thread.i.i572
.Lcfi218:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB0_532:                              # %._crit_edge16.i.i573
	movslq	%ebp, %rax
	movl	$0, (%r14,%rax,4)
	movq	%r14, %r15
.LBB0_533:                              # %.noexc560
	movslq	%ebp, %rax
	leaq	(%r15,%rax,4), %rcx
	movq	288(%rsp), %rdx
	.p2align	4, 0x90
.LBB0_534:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdx), %esi
	addq	$4, %rdx
	movl	%esi, (%rcx)
	addq	$4, %rcx
	testl	%esi, %esi
	jne	.LBB0_534
# BB#535:                               # %_Z11MyStringLenIwEiPKT_.exit.i579
	movslq	296(%rsp), %rbp
	addq	%rax, %rbp
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jle	.LBB0_537
# BB#536:
	movl	%ebx, %r13d
	jmp	.LBB0_558
.LBB0_537:
	movq	%r14, 24(%rsp)          # 8-byte Spill
	cmpl	$8, %ebx
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebx
	jl	.LBB0_539
# BB#538:                               # %select.true.sink2637
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
.LBB0_539:                              # %select.end2636
	leal	-1(%rcx,%rax), %edx
	movl	$2, %esi
	subl	%eax, %esi
	testl	%edx, %edx
	cmovgl	%ecx, %esi
	leal	1(%rbx,%rsi), %r13d
	cmpl	%ebx, %r13d
	jne	.LBB0_541
# BB#540:
	movl	%ebx, %r13d
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB0_558
.LBB0_541:
	movslq	%r13d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp215:
.Lcfi219:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %r14
.Ltmp216:
# BB#542:                               # %.noexc597
	testl	%ebx, %ebx
	jle	.LBB0_557
# BB#543:                               # %.preheader.i.i587
	testl	%ebp, %ebp
	jle	.LBB0_555
# BB#544:                               # %.lr.ph.i.i588
	cmpl	$7, %ebp
	jbe	.LBB0_548
# BB#545:                               # %min.iters.checked1162
	movq	%rbp, %rax
	andq	$-8, %rax
	je	.LBB0_548
# BB#546:                               # %vector.memcheck1175
	leaq	(%r15,%rbp,4), %rcx
	cmpq	%rcx, %r14
	jae	.LBB0_674
# BB#547:                               # %vector.memcheck1175
	leaq	(%r14,%rbp,4), %rcx
	cmpq	%rcx, 24(%rsp)          # 8-byte Folded Reload
	jae	.LBB0_674
.LBB0_548:
	xorl	%eax, %eax
.LBB0_549:                              # %scalar.ph1160.preheader
	movl	%ebp, %edx
	subl	%eax, %edx
	leaq	-1(%rbp), %rcx
	subq	%rax, %rcx
	andq	$7, %rdx
	je	.LBB0_552
# BB#550:                               # %scalar.ph1160.prol.preheader
	negq	%rdx
.LBB0_551:                              # %scalar.ph1160.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rax,4), %esi
	movl	%esi, (%r14,%rax,4)
	incq	%rax
	incq	%rdx
	jne	.LBB0_551
.LBB0_552:                              # %scalar.ph1160.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB0_556
# BB#553:                               # %scalar.ph1160.preheader.new
	movq	%rbp, %rcx
	subq	%rax, %rcx
	leaq	28(%r14,%rax,4), %rdx
	leaq	28(%r15,%rax,4), %rax
.LBB0_554:                              # %scalar.ph1160
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rax), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rax), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rax), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rax), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rax), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rax), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rax), %esi
	movl	%esi, -4(%rdx)
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rax
	addq	$-8, %rcx
	jne	.LBB0_554
	jmp	.LBB0_556
.LBB0_555:                              # %._crit_edge.i.i589
	testq	%r15, %r15
	je	.LBB0_557
.LBB0_556:                              # %._crit_edge.thread.i.i594
.Lcfi220:
	.cfi_escape 0x2e, 0x00
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
.LBB0_557:                              # %._crit_edge16.i.i595
	movslq	%ebp, %rax
	movl	$0, (%r14,%rax,4)
	movq	%r14, %r15
.LBB0_558:                              # %.noexc582
	movq	%r15, %r12
	movslq	%ebp, %rax
	movq	$10, (%r12,%rax,4)
	leal	1(%rbp), %ebp
	movq	%r14, %r15
.LBB0_559:
	movl	312(%rsp), %eax
	testl	%eax, %eax
	je	.LBB0_608
# BB#560:
	movl	%r13d, %ecx
	subl	%ebp, %ecx
	cmpl	%eax, %ecx
	jg	.LBB0_564
# BB#561:
	decl	%ecx
	cmpl	$8, %r13d
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %r13d
	jl	.LBB0_563
# BB#562:                               # %select.true.sink2659
	movl	%r13d, %edx
	shrl	$31, %edx
	addl	%r13d, %edx
	sarl	%edx
.LBB0_563:                              # %select.end2658
	movl	%eax, %esi
	subl	%ecx, %esi
	addl	%edx, %ecx
	cmpl	%eax, %ecx
	cmovgel	%edx, %esi
	leal	1(%r13,%rsi), %ebx
	cmpl	%r13d, %ebx
	jne	.LBB0_565
.LBB0_564:
	movl	%r13d, %ebx
	movq	%r15, %r14
	movq	%r12, %r15
	jmp	.LBB0_582
.LBB0_565:
	movslq	%ebx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp218:
.Lcfi221:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
.Ltmp219:
# BB#566:                               # %.noexc616
	testl	%r13d, %r13d
	movq	%rax, %r14
	jle	.LBB0_581
# BB#567:                               # %.preheader.i.i606
	testl	%ebp, %ebp
	jle	.LBB0_579
# BB#568:                               # %.lr.ph.i.i607
	movslq	%ebp, %rax
	cmpl	$7, %ebp
	jbe	.LBB0_572
# BB#569:                               # %min.iters.checked1191
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB0_572
# BB#570:                               # %vector.memcheck1204
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %r14
	jae	.LBB0_677
# BB#571:                               # %vector.memcheck1204
	leaq	(%r14,%rax,4), %rdx
	cmpq	%rdx, %r15
	jae	.LBB0_677
.LBB0_572:
	xorl	%ecx, %ecx
.LBB0_573:                              # %scalar.ph1189.preheader
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB0_576
# BB#574:                               # %scalar.ph1189.prol.preheader
	negq	%rsi
.LBB0_575:                              # %scalar.ph1189.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12,%rcx,4), %edi
	movl	%edi, (%r14,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_575
.LBB0_576:                              # %scalar.ph1189.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB0_580
# BB#577:                               # %scalar.ph1189.preheader.new
	subq	%rcx, %rax
	leaq	28(%r14,%rcx,4), %rdx
	leaq	28(%r12,%rcx,4), %rcx
.LBB0_578:                              # %scalar.ph1189
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB0_578
	jmp	.LBB0_580
.LBB0_579:                              # %._crit_edge.i.i608
	testq	%r12, %r12
	je	.LBB0_581
.LBB0_580:                              # %._crit_edge.thread.i.i613
.Lcfi222:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB0_581:                              # %._crit_edge16.i.i614
	movslq	%ebp, %rax
	movl	$0, (%r14,%rax,4)
	movq	%r14, %r15
.LBB0_582:                              # %.noexc601
	movslq	%ebp, %rax
	leaq	(%r15,%rax,4), %rcx
	movq	304(%rsp), %rdx
	.p2align	4, 0x90
.LBB0_583:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdx), %esi
	addq	$4, %rdx
	movl	%esi, (%rcx)
	addq	$4, %rcx
	testl	%esi, %esi
	jne	.LBB0_583
# BB#584:                               # %_Z11MyStringLenIwEiPKT_.exit.i620
	movslq	312(%rsp), %rbp
	addq	%rax, %rbp
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jle	.LBB0_586
# BB#585:
	movl	%ebx, %r13d
	jmp	.LBB0_607
.LBB0_586:
	movq	%r14, 24(%rsp)          # 8-byte Spill
	cmpl	$8, %ebx
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebx
	jl	.LBB0_588
# BB#587:                               # %select.true.sink2682
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
.LBB0_588:                              # %select.end2681
	leal	-1(%rcx,%rax), %edx
	movl	$2, %esi
	subl	%eax, %esi
	testl	%edx, %edx
	cmovgl	%ecx, %esi
	leal	1(%rbx,%rsi), %r13d
	cmpl	%ebx, %r13d
	jne	.LBB0_590
# BB#589:
	movl	%ebx, %r13d
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB0_607
.LBB0_590:
	movslq	%r13d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp220:
.Lcfi223:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %r14
.Ltmp221:
# BB#591:                               # %.noexc638
	testl	%ebx, %ebx
	jle	.LBB0_606
# BB#592:                               # %.preheader.i.i628
	testl	%ebp, %ebp
	jle	.LBB0_604
# BB#593:                               # %.lr.ph.i.i629
	cmpl	$7, %ebp
	jbe	.LBB0_597
# BB#594:                               # %min.iters.checked1220
	movq	%rbp, %rax
	andq	$-8, %rax
	je	.LBB0_597
# BB#595:                               # %vector.memcheck1233
	leaq	(%r15,%rbp,4), %rcx
	cmpq	%rcx, %r14
	jae	.LBB0_680
# BB#596:                               # %vector.memcheck1233
	leaq	(%r14,%rbp,4), %rcx
	cmpq	%rcx, 24(%rsp)          # 8-byte Folded Reload
	jae	.LBB0_680
.LBB0_597:
	xorl	%eax, %eax
.LBB0_598:                              # %scalar.ph1218.preheader
	movl	%ebp, %edx
	subl	%eax, %edx
	leaq	-1(%rbp), %rcx
	subq	%rax, %rcx
	andq	$7, %rdx
	je	.LBB0_601
# BB#599:                               # %scalar.ph1218.prol.preheader
	negq	%rdx
.LBB0_600:                              # %scalar.ph1218.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rax,4), %esi
	movl	%esi, (%r14,%rax,4)
	incq	%rax
	incq	%rdx
	jne	.LBB0_600
.LBB0_601:                              # %scalar.ph1218.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB0_605
# BB#602:                               # %scalar.ph1218.preheader.new
	movq	%rbp, %rcx
	subq	%rax, %rcx
	leaq	28(%r14,%rax,4), %rdx
	leaq	28(%r15,%rax,4), %rax
.LBB0_603:                              # %scalar.ph1218
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rax), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rax), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rax), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rax), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rax), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rax), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rax), %esi
	movl	%esi, -4(%rdx)
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rax
	addq	$-8, %rcx
	jne	.LBB0_603
	jmp	.LBB0_605
.LBB0_604:                              # %._crit_edge.i.i630
	testq	%r15, %r15
	je	.LBB0_606
.LBB0_605:                              # %._crit_edge.thread.i.i635
.Lcfi224:
	.cfi_escape 0x2e, 0x00
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
.LBB0_606:                              # %._crit_edge16.i.i636
	movslq	%ebp, %rax
	movl	$0, (%r14,%rax,4)
	movq	%r14, %r15
.LBB0_607:                              # %.noexc623
	movq	%r15, %r12
	movslq	%ebp, %rax
	movq	$10, (%r12,%rax,4)
	leal	1(%rbp), %ebp
	movq	%r14, %r15
.LBB0_608:
	movl	280(%rsp), %ebx
	testl	%ebx, %ebx
	je	.LBB0_658
# BB#609:
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 144(%rsp)
.Ltmp223:
.Lcfi225:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp224:
# BB#610:                               # %.noexc641
	movq	%rax, 144(%rsp)
	movl	$0, (%rax)
	movl	$4, 156(%rsp)
.Ltmp226:
.Lcfi226:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rsi
	movl	%ebx, %edi
	callq	_ZN8NWindows6NError15MyFormatMessageEjR11CStringBaseIwE
.Ltmp227:
# BB#611:                               # %_ZN8NWindows6NError16MyFormatMessageWEj.exit644
	movl	152(%rsp), %eax
	movl	%r13d, %ecx
	subl	%ebp, %ecx
	cmpl	%eax, %ecx
	jg	.LBB0_615
# BB#612:
	decl	%ecx
	cmpl	$8, %r13d
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %r13d
	jl	.LBB0_614
# BB#613:                               # %select.true.sink2704
	movl	%r13d, %edx
	shrl	$31, %edx
	addl	%r13d, %edx
	sarl	%edx
.LBB0_614:                              # %select.end2703
	movl	%eax, %esi
	subl	%ecx, %esi
	addl	%edx, %ecx
	cmpl	%eax, %ecx
	cmovgel	%edx, %esi
	leal	1(%r13,%rsi), %r14d
	cmpl	%r13d, %r14d
	jne	.LBB0_616
.LBB0_615:
	movl	%r13d, %r14d
	movq	%r15, %r13
	movq	%r12, %r15
	jmp	.LBB0_633
.LBB0_616:
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp229:
.Lcfi227:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
.Ltmp230:
# BB#617:                               # %.noexc662
	testl	%r13d, %r13d
	movq	%rax, %r13
	jle	.LBB0_632
# BB#618:                               # %.preheader.i.i652
	testl	%ebp, %ebp
	jle	.LBB0_630
# BB#619:                               # %.lr.ph.i.i653
	movslq	%ebp, %rax
	cmpl	$7, %ebp
	jbe	.LBB0_623
# BB#620:                               # %min.iters.checked1249
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB0_623
# BB#621:                               # %vector.memcheck1262
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %r13
	jae	.LBB0_671
# BB#622:                               # %vector.memcheck1262
	leaq	(%r13,%rax,4), %rdx
	cmpq	%rdx, %r15
	jae	.LBB0_671
.LBB0_623:
	xorl	%ecx, %ecx
.LBB0_624:                              # %scalar.ph1247.preheader
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB0_627
# BB#625:                               # %scalar.ph1247.prol.preheader
	negq	%rsi
.LBB0_626:                              # %scalar.ph1247.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12,%rcx,4), %edi
	movl	%edi, (%r13,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_626
.LBB0_627:                              # %scalar.ph1247.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB0_631
# BB#628:                               # %scalar.ph1247.preheader.new
	subq	%rcx, %rax
	leaq	28(%r13,%rcx,4), %rdx
	leaq	28(%r12,%rcx,4), %rcx
.LBB0_629:                              # %scalar.ph1247
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB0_629
	jmp	.LBB0_631
.LBB0_630:                              # %._crit_edge.i.i654
	testq	%r12, %r12
	je	.LBB0_632
.LBB0_631:                              # %._crit_edge.thread.i.i659
.Lcfi228:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB0_632:                              # %._crit_edge16.i.i660
	movslq	%ebp, %rax
	movl	$0, (%r13,%rax,4)
	movq	%r13, %r15
.LBB0_633:                              # %.noexc647
	movslq	%ebp, %rax
	leaq	(%r15,%rax,4), %rcx
	movq	144(%rsp), %rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_634:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rdx), %esi
	movl	%esi, (%rcx,%rdx)
	addq	$4, %rdx
	testl	%esi, %esi
	jne	.LBB0_634
# BB#635:
	movslq	152(%rsp), %rbp
	addq	%rax, %rbp
	testq	%rdi, %rdi
	je	.LBB0_637
# BB#636:
.Lcfi229:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_637:                              # %_ZN11CStringBaseIwED2Ev.exit664
	movl	%r14d, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jg	.LBB0_715
# BB#638:
	cmpl	$8, %r14d
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %r14d
	jl	.LBB0_640
# BB#639:                               # %select.true.sink2727
	movl	%r14d, %ecx
	shrl	$31, %ecx
	addl	%r14d, %ecx
	sarl	%ecx
.LBB0_640:                              # %select.end2726
	leal	-1(%rcx,%rax), %edx
	movl	$2, %esi
	subl	%eax, %esi
	testl	%edx, %edx
	cmovgl	%ecx, %esi
	leal	1(%r14,%rsi), %eax
	cmpl	%r14d, %eax
	je	.LBB0_715
# BB#641:
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp232:
.Lcfi230:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %rbx
.Ltmp233:
# BB#642:                               # %.noexc685
	testl	%r14d, %r14d
	jle	.LBB0_656
# BB#643:                               # %.preheader.i.i675
	testl	%ebp, %ebp
	jle	.LBB0_716
# BB#644:                               # %.lr.ph.i.i676
	cmpl	$7, %ebp
	jbe	.LBB0_648
# BB#645:                               # %min.iters.checked1278
	movq	%rbp, %rax
	andq	$-8, %rax
	je	.LBB0_648
# BB#646:                               # %vector.memcheck1291
	leaq	(%r15,%rbp,4), %rcx
	cmpq	%rcx, %rbx
	jae	.LBB0_663
# BB#647:                               # %vector.memcheck1291
	leaq	(%rbx,%rbp,4), %rcx
	cmpq	%rcx, %r13
	jae	.LBB0_663
.LBB0_648:
	xorl	%eax, %eax
.LBB0_649:                              # %scalar.ph1276.preheader
	movl	%ebp, %edx
	subl	%eax, %edx
	leaq	-1(%rbp), %rcx
	subq	%rax, %rcx
	andq	$7, %rdx
	je	.LBB0_652
# BB#650:                               # %scalar.ph1276.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB0_651:                              # %scalar.ph1276.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rax,4), %esi
	movl	%esi, (%rbx,%rax,4)
	incq	%rax
	incq	%rdx
	jne	.LBB0_651
.LBB0_652:                              # %scalar.ph1276.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB0_655
# BB#653:                               # %scalar.ph1276.preheader.new
	movq	%rbp, %rcx
	subq	%rax, %rcx
	leaq	28(%rbx,%rax,4), %rdx
	leaq	28(%r15,%rax,4), %rax
	.p2align	4, 0x90
.LBB0_654:                              # %scalar.ph1276
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rax), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rax), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rax), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rax), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rax), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rax), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rax), %esi
	movl	%esi, -4(%rdx)
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rax
	addq	$-8, %rcx
	jne	.LBB0_654
.LBB0_655:                              # %._crit_edge.thread.i.i682
.Lcfi231:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	_ZdaPv
.LBB0_656:                              # %._crit_edge16.i.i683
	movslq	%ebp, %rax
	movl	$0, (%rbx,%rax,4)
	movq	%rbx, %r15
.LBB0_657:                              # %.noexc670
	movq	%r15, %r12
	movslq	%ebp, %rax
	movq	$10, (%r12,%rax,4)
	leal	1(%rbp), %ebp
	movq	%rbx, %r15
.LBB0_658:
	testl	%ebp, %ebp
	je	.LBB0_661
# BB#659:
.Ltmp235:
.Lcfi232:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.24, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN13CStdOutStreamlsEPKw
.Ltmp236:
# BB#660:
.Ltmp237:
.Lcfi233:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	_ZN13CStdOutStreamlsEPKw
.Ltmp238:
.LBB0_661:
.Lcfi234:
	.cfi_escape 0x2e, 0x00
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	32(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, (%rax)
.Ltmp239:
.Lcfi235:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTI16CSystemException, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp240:
.LBB0_662:
.LBB0_663:                              # %vector.body1274.preheader
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB0_666
# BB#664:                               # %vector.body1274.prol.preheader
	negq	%rdx
	xorl	%esi, %esi
.LBB0_665:                              # %vector.body1274.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%r15,%rsi,4), %xmm0
	movdqu	16(%r15,%rsi,4), %xmm1
	movdqu	%xmm0, (%rbx,%rsi,4)
	movdqu	%xmm1, 16(%rbx,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB0_665
	jmp	.LBB0_667
.LBB0_666:
	xorl	%esi, %esi
.LBB0_667:                              # %vector.body1274.prol.loopexit
	cmpq	$24, %rcx
	jb	.LBB0_670
# BB#668:                               # %vector.body1274.preheader.new
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%rbx,%rsi,4), %rdx
	leaq	112(%r15,%rsi,4), %rsi
.LBB0_669:                              # %vector.body1274
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rsi), %xmm0
	movdqu	(%rsi), %xmm1
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB0_669
.LBB0_670:                              # %middle.block1275
	cmpq	%rax, %rbp
	jne	.LBB0_649
	jmp	.LBB0_655
.LBB0_671:                              # %vector.body1245.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_683
# BB#672:                               # %vector.body1245.prol.preheader
	negq	%rsi
	xorl	%edi, %edi
.LBB0_673:                              # %vector.body1245.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%r12,%rdi,4), %xmm0
	movdqu	16(%r12,%rdi,4), %xmm1
	movdqu	%xmm0, (%r13,%rdi,4)
	movdqu	%xmm1, 16(%r13,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB0_673
	jmp	.LBB0_684
.LBB0_517:                              # %vector.body1129.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_520
# BB#518:                               # %vector.body1129.prol.preheader
	negq	%rsi
	xorl	%edi, %edi
.LBB0_519:                              # %vector.body1129.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%r12,%rdi,4), %xmm0
	movdqu	16(%r12,%rdi,4), %xmm1
	movdqu	%xmm0, (%r14,%rdi,4)
	movdqu	%xmm1, 16(%r14,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB0_519
	jmp	.LBB0_521
.LBB0_674:                              # %vector.body1158.preheader
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB0_688
# BB#675:                               # %vector.body1158.prol.preheader
	negq	%rdx
	xorl	%esi, %esi
.LBB0_676:                              # %vector.body1158.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%r15,%rsi,4), %xmm0
	movdqu	16(%r15,%rsi,4), %xmm1
	movdqu	%xmm0, (%r14,%rsi,4)
	movdqu	%xmm1, 16(%r14,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB0_676
	jmp	.LBB0_689
.LBB0_677:                              # %vector.body1187.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_693
# BB#678:                               # %vector.body1187.prol.preheader
	negq	%rsi
	xorl	%edi, %edi
.LBB0_679:                              # %vector.body1187.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%r12,%rdi,4), %xmm0
	movdqu	16(%r12,%rdi,4), %xmm1
	movdqu	%xmm0, (%r14,%rdi,4)
	movdqu	%xmm1, 16(%r14,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB0_679
	jmp	.LBB0_694
.LBB0_680:                              # %vector.body1216.preheader
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB0_698
# BB#681:                               # %vector.body1216.prol.preheader
	negq	%rdx
	xorl	%esi, %esi
.LBB0_682:                              # %vector.body1216.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%r15,%rsi,4), %xmm0
	movdqu	16(%r15,%rsi,4), %xmm1
	movdqu	%xmm0, (%r14,%rsi,4)
	movdqu	%xmm1, 16(%r14,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB0_682
	jmp	.LBB0_699
.LBB0_683:
	xorl	%edi, %edi
.LBB0_684:                              # %vector.body1245.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB0_687
# BB#685:                               # %vector.body1245.preheader.new
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%r13,%rdi,4), %rsi
	leaq	112(%r12,%rdi,4), %rdi
.LBB0_686:                              # %vector.body1245
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB0_686
.LBB0_687:                              # %middle.block1246
	cmpq	%rcx, %rax
	jne	.LBB0_624
	jmp	.LBB0_631
.LBB0_520:
	xorl	%edi, %edi
.LBB0_521:                              # %vector.body1129.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB0_524
# BB#522:                               # %vector.body1129.preheader.new
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%r14,%rdi,4), %rsi
	leaq	112(%r12,%rdi,4), %rdi
.LBB0_523:                              # %vector.body1129
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB0_523
.LBB0_524:                              # %middle.block1130
	cmpq	%rcx, %rax
	je	.LBB0_531
	jmp	.LBB0_525
.LBB0_688:
	xorl	%esi, %esi
.LBB0_689:                              # %vector.body1158.prol.loopexit
	cmpq	$24, %rcx
	jb	.LBB0_692
# BB#690:                               # %vector.body1158.preheader.new
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%r14,%rsi,4), %rdx
	leaq	112(%r15,%rsi,4), %rsi
.LBB0_691:                              # %vector.body1158
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rsi), %xmm0
	movdqu	(%rsi), %xmm1
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB0_691
.LBB0_692:                              # %middle.block1159
	cmpq	%rax, %rbp
	jne	.LBB0_549
	jmp	.LBB0_556
.LBB0_693:
	xorl	%edi, %edi
.LBB0_694:                              # %vector.body1187.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB0_697
# BB#695:                               # %vector.body1187.preheader.new
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%r14,%rdi,4), %rsi
	leaq	112(%r12,%rdi,4), %rdi
.LBB0_696:                              # %vector.body1187
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB0_696
.LBB0_697:                              # %middle.block1188
	cmpq	%rcx, %rax
	jne	.LBB0_573
	jmp	.LBB0_580
.LBB0_698:
	xorl	%esi, %esi
.LBB0_699:                              # %vector.body1216.prol.loopexit
	cmpq	$24, %rcx
	jb	.LBB0_702
# BB#700:                               # %vector.body1216.preheader.new
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%r14,%rsi,4), %rdx
	leaq	112(%r15,%rsi,4), %rsi
.LBB0_701:                              # %vector.body1216
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rsi), %xmm0
	movdqu	(%rsi), %xmm1
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB0_701
.LBB0_702:                              # %middle.block1217
	cmpq	%rax, %rbp
	jne	.LBB0_598
	jmp	.LBB0_605
.LBB0_703:
.Lcfi236:
	.cfi_escape 0x2e, 0x00
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.36, (%rax)
.Ltmp448:
.Lcfi237:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp449:
	jmp	.LBB0_662
.LBB0_704:
.Lcfi238:
	.cfi_escape 0x2e, 0x00
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	%ebx, (%rax)
.Ltmp26:
.Lcfi239:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTI16CSystemException, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp27:
	jmp	.LBB0_662
.LBB0_705:
.Lcfi240:
	.cfi_escape 0x2e, 0x00
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.37, (%rax)
.Ltmp35:
.Lcfi241:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp36:
	jmp	.LBB0_662
.LBB0_706:
.Lcfi242:
	.cfi_escape 0x2e, 0x00
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	%ebx, (%rax)
.Ltmp43:
.Lcfi243:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTI16CSystemException, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp44:
	jmp	.LBB0_662
.LBB0_715:
	movq	%r13, %rbx
	jmp	.LBB0_657
.LBB0_707:
.Ltmp121:
.Lcfi244:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.35, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp122:
# BB#708:                               # %.noexc706
.Ltmp123:
.Lcfi245:
	.cfi_escape 0x2e, 0x00
	movl	$.L.str.40, %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN13CStdOutStreamlsEPKc
.Ltmp124:
# BB#709:                               # %.noexc707
.Ltmp125:
.Lcfi246:
	.cfi_escape 0x2e, 0x00
	movl	$_Z4endlR13CStdOutStream, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPFRS_S0_E
.Ltmp126:
# BB#710:                               # %.noexc708
.Lcfi247:
	.cfi_escape 0x2e, 0x00
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$7, (%rax)
.Ltmp127:
.Lcfi248:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTIN9NExitCode5EEnumE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp128:
# BB#711:                               # %.noexc709
.LBB0_712:
.Lcfi249:
	.cfi_escape 0x2e, 0x00
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.37, (%rax)
.Ltmp156:
.Lcfi250:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp157:
	jmp	.LBB0_662
.LBB0_713:
.Lcfi251:
	.cfi_escape 0x2e, 0x00
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	%ebx, (%rax)
.Ltmp50:
.Lcfi252:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTI16CSystemException, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp51:
	jmp	.LBB0_662
.LBB0_716:                              # %._crit_edge.i.i677
	testq	%r15, %r15
	jne	.LBB0_655
	jmp	.LBB0_656
.LBB0_717:
.Lcfi253:
	.cfi_escape 0x2e, 0x00
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	%r14d, (%rax)
.Ltmp373:
.Lcfi254:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTI16CSystemException, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp374:
	jmp	.LBB0_662
.LBB0_718:
.Lcfi255:
	.cfi_escape 0x2e, 0x00
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	%r14d, (%rax)
.Ltmp375:
.Lcfi256:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTI16CSystemException, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp376:
	jmp	.LBB0_662
.LBB0_719:
.Ltmp222:
	movq	%rax, %r14
	jmp	.LBB0_752
.LBB0_720:
.Ltmp217:
	movq	%rax, %r14
	jmp	.LBB0_752
.LBB0_721:
.Ltmp212:
	movq	%rax, %r14
	jmp	.LBB0_752
.LBB0_722:
.Ltmp231:
	movq	%rax, %r14
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_753
# BB#723:
.Lcfi257:
	.cfi_escape 0x2e, 0x00
	jmp	.LBB0_740
.LBB0_724:
.Ltmp410:
	jmp	.LBB0_763
.LBB0_725:
.Ltmp288:
	movq	%rax, %rbx
	jmp	.LBB0_822
.LBB0_726:
.Ltmp52:
	jmp	.LBB0_745
.LBB0_727:
.Ltmp433:
	jmp	.LBB0_745
.LBB0_728:
.Ltmp428:
	movq	%rax, %r12
	jmp	.LBB0_730
.LBB0_729:
.Ltmp422:
	movq	%rax, %r12
.Ltmp423:
.Lcfi258:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp424:
.LBB0_730:                              # %.body.i
	movq	160(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_798
# BB#731:
.Lcfi259:
	.cfi_escape 0x2e, 0x00
	jmp	.LBB0_797
.LBB0_732:
.Ltmp425:
.Lcfi260:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_733:
.Ltmp344:
	jmp	.LBB0_763
.LBB0_734:
.Ltmp309:
	jmp	.LBB0_745
.LBB0_735:
.Ltmp306:
	movq	%rax, %rbx
.Lcfi261:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB0_837
.LBB0_736:
.Ltmp291:
	jmp	.LBB0_745
.LBB0_737:
.Ltmp234:
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%rax, %r14
	jmp	.LBB0_752
.LBB0_738:
.Ltmp228:
	movq	%rax, %r14
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_753
# BB#739:
.Lcfi262:
	.cfi_escape 0x2e, 0x00
.LBB0_740:
	callq	_ZdaPv
	testq	%r12, %r12
	jne	.LBB0_754
	jmp	.LBB0_814
.LBB0_741:
.Ltmp225:
	movq	%rax, %r14
	testq	%r12, %r12
	jne	.LBB0_754
	jmp	.LBB0_814
.LBB0_742:                              # %.thread
.Ltmp319:
	movq	%rax, %r12
	movq	$_ZTV20COpenCallbackConsole+16, 384(%rsp)
	jmp	.LBB0_799
.LBB0_743:
.Ltmp314:
	movq	%rax, %r12
	jmp	.LBB0_800
.LBB0_744:
.Ltmp300:
	jmp	.LBB0_745
.LBB0_746:                              # %_ZN11CStringBaseIwED2Ev.exit.i503
.Ltmp153:
	movq	%rax, %r14
.Lcfi263:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdaPv
	movq	288(%rsp), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_748
	jmp	.LBB0_821
.LBB0_747:                              # %_ZN11CStringBaseIwED2Ev.exit.thread.i
.Ltmp150:
	movq	%rax, %r14
.LBB0_748:
.Lcfi264:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	jmp	.LBB0_820
.LBB0_749:
.Ltmp133:
	jmp	.LBB0_745
.LBB0_750:                              # %.thread918
.Ltmp138:
	movq	%rax, %rbx
	movq	$_ZTV20COpenCallbackConsole+16, 344(%rsp)
	jmp	.LBB0_823
.LBB0_751:
.Ltmp241:
	movq	%rax, %r14
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%r12, %r15
.LBB0_752:
	movq	%r15, %r12
	movq	24(%rsp), %r15          # 8-byte Reload
.LBB0_753:
	testq	%r12, %r12
	je	.LBB0_814
.LBB0_754:
.Lcfi265:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB0_814
.LBB0_755:
.Ltmp45:
	jmp	.LBB0_745
.LBB0_756:
.Ltmp160:
	jmp	.LBB0_813
.LBB0_757:                              # %.loopexit.split-lp
.Ltmp341:
	movq	%rax, %r12
	leaq	176(%rsp), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	jmp	.LBB0_793
.LBB0_758:
.Ltmp450:
	jmp	.LBB0_775
.LBB0_759:
.Ltmp283:
	jmp	.LBB0_813
.LBB0_760:
.Ltmp207:
	jmp	.LBB0_813
.LBB0_761:
.Ltmp147:
	movq	%rax, %r14
	jmp	.LBB0_821
.LBB0_762:
.Ltmp401:
.LBB0_763:
	movq	%rax, %r12
	movq	256(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_793
# BB#764:
.Lcfi266:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB0_793
.LBB0_765:
.Ltmp204:
	jmp	.LBB0_813
.LBB0_766:
.Ltmp259:
	movq	%rax, %r14
	movq	224(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_814
# BB#767:
.Lcfi267:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB0_814
.LBB0_768:
.Ltmp163:
	jmp	.LBB0_813
.LBB0_769:
.Ltmp166:
	movq	%rax, %r14
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_814
# BB#770:
.Lcfi268:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB0_814
.LBB0_771:
.Ltmp441:
	jmp	.LBB0_809
.LBB0_772:
.Ltmp438:
	jmp	.LBB0_775
.LBB0_773:
.Ltmp118:
	jmp	.LBB0_745
.LBB0_774:
.Ltmp30:
.LBB0_775:
	movq	%rax, %rbx
	leaq	80(%rsp), %r15
	jmp	.LBB0_839
.LBB0_776:
.Ltmp25:
	movq	%rax, %rbx
	jmp	.LBB0_839
.LBB0_777:
.Ltmp332:
	movq	%rax, %r12
	jmp	.LBB0_780
.LBB0_778:
.Ltmp335:
	movq	%rax, %r12
	testq	%r14, %r14
	je	.LBB0_780
# BB#779:
.Lcfi269:
	.cfi_escape 0x2e, 0x00
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
.LBB0_780:                              # %.body445
.Lcfi270:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZdlPv
	jmp	.LBB0_793
.LBB0_781:
.Ltmp264:
	movq	%rax, %r14
	movq	224(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_814
# BB#782:
.Lcfi271:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB0_814
.LBB0_783:
.Ltmp184:
	movq	%rax, %r14
	movq	240(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_814
# BB#784:
.Lcfi272:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB0_814
.LBB0_785:
.Ltmp256:
	jmp	.LBB0_813
.LBB0_786:
.Ltmp447:
	movq	%rax, %rbx
	jmp	.LBB0_842
.LBB0_787:
.Ltmp444:
	jmp	.LBB0_790
.LBB0_788:
.Ltmp22:
	jmp	.LBB0_809
.LBB0_789:
.Ltmp8:
.LBB0_790:
	movq	%rax, %rbx
	jmp	.LBB0_841
.LBB0_791:
.Ltmp303:
	jmp	.LBB0_745
.LBB0_792:                              # %.loopexit
.Ltmp338:
	movq	%rax, %r12
.LBB0_793:
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	$_ZTV13CObjectVectorI9CPropertyE+16, (%rdi)
.Ltmp411:
.Lcfi273:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp412:
# BB#794:
.Ltmp417:
.Lcfi274:
	.cfi_escape 0x2e, 0x00
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp418:
# BB#795:                               # %_ZN13CObjectVectorI9CPropertyED2Ev.exit.i454
	movq	160(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_798
# BB#796:
.Lcfi275:
	.cfi_escape 0x2e, 0x00
.LBB0_797:
	callq	_ZdaPv
.LBB0_798:
	movq	408(%rsp), %r14
	movq	$_ZTV20COpenCallbackConsole+16, 384(%rsp)
	testq	%r14, %r14
	je	.LBB0_800
.LBB0_799:
.Lcfi276:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB0_800:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp429:
.Lcfi277:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp430:
	leaq	80(%rsp), %r15
	movq	%r12, %rbx
	jmp	.LBB0_838
.LBB0_801:
.Ltmp419:
	movq	%rax, %rbx
	jmp	.LBB0_803
.LBB0_802:
.Ltmp413:
	movq	%rax, %rbx
.Ltmp414:
.Lcfi278:
	.cfi_escape 0x2e, 0x00
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp415:
.LBB0_803:                              # %.body.i457
	movq	160(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_849
# BB#804:
.Lcfi279:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB0_849
.LBB0_805:
.Ltmp416:
.Lcfi280:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_806:
.Ltmp189:
	movq	%rax, %r14
	movq	240(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_814
# BB#807:
.Lcfi281:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB0_814
.LBB0_808:
.Ltmp15:
.LBB0_809:                              # %_ZN9CMyComPtrI8IUnknownED2Ev.exit712
	movq	%rax, %rbx
	jmp	.LBB0_840
.LBB0_810:
.Ltmp471:
	movq	%rax, %rbx
.Ltmp472:
.Lcfi282:
	.cfi_escape 0x2e, 0x00
	leaq	80(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp473:
	jmp	.LBB0_845
.LBB0_811:
.Ltmp474:
.Lcfi283:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_812:
.Ltmp181:
.LBB0_813:                              # %_ZN11CStringBaseIwED2Ev.exit513
	movq	%rax, %r14
.LBB0_814:                              # %_ZN11CStringBaseIwED2Ev.exit513
	movq	320(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_816
# BB#815:
.Lcfi284:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_816:                              # %_ZN11CStringBaseIwED2Ev.exit.i698
	movq	304(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_818
# BB#817:
.Lcfi285:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB0_818:                              # %_ZN11CStringBaseIwED2Ev.exit1.i699
	movq	288(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_821
# BB#819:
.Lcfi286:
	.cfi_escape 0x2e, 0x00
.LBB0_820:
	callq	_ZdaPv
.LBB0_821:
.Ltmp284:
.Lcfi287:
	.cfi_escape 0x2e, 0x00
	leaq	464(%rsp), %rdi
	callq	_ZN22CUpdateCallbackConsoleD2Ev
.Ltmp285:
	movq	%r14, %rbx
.LBB0_822:
	movq	368(%rsp), %r13
	movq	$_ZTV20COpenCallbackConsole+16, 344(%rsp)
	testq	%r13, %r13
	je	.LBB0_837
.LBB0_823:
.Lcfi288:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	jmp	.LBB0_836
.LBB0_824:
.Ltmp111:
	jmp	.LBB0_834
.LBB0_825:                              # %.loopexit.split-lp803
.Ltmp99:
	jmp	.LBB0_834
.LBB0_826:
.Ltmp459:
	movq	%rax, %rbx
	jmp	.LBB0_843
.LBB0_827:
.Ltmp88:
	jmp	.LBB0_834
.LBB0_828:
.Ltmp83:
	jmp	.LBB0_834
.LBB0_829:                              # %.loopexit.split-lp808
.Ltmp78:
	jmp	.LBB0_745
.LBB0_830:
.Ltmp91:
	movq	%rax, %rbx
	movq	%r14, %r12
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_835
	jmp	.LBB0_837
.LBB0_831:                              # %.loopexit802
.Ltmp96:
	jmp	.LBB0_834
.LBB0_832:                              # %.loopexit807
.Ltmp73:
.LBB0_745:
	movq	%rax, %rbx
	jmp	.LBB0_837
.LBB0_833:
.Ltmp108:
.LBB0_834:
	movq	%rax, %rbx
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB0_837
.LBB0_835:
.Lcfi289:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
.LBB0_836:                              # %_ZN11CStringBaseIwED2Ev.exit397
	callq	_ZdaPv
.LBB0_837:                              # %_ZN11CStringBaseIwED2Ev.exit397
	leaq	80(%rsp), %r15
.LBB0_838:                              # %_ZN11CStringBaseIwED2Ev.exit397
.Ltmp434:
.Lcfi290:
	.cfi_escape 0x2e, 0x00
	leaq	424(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp435:
.LBB0_839:
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp451:
.Lcfi291:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp452:
.LBB0_840:                              # %_ZN9CMyComPtrI8IUnknownED2Ev.exit712
.Ltmp453:
.Lcfi292:
	.cfi_escape 0x2e, 0x00
	leaq	1336(%rsp), %rdi
	callq	_ZN18NCommandLineParser7CParserD1Ev
.Ltmp454:
.LBB0_841:
.Ltmp455:
.Lcfi293:
	.cfi_escape 0x2e, 0x00
	leaq	744(%rsp), %rdi
	callq	_ZN26CArchiveCommandLineOptionsD2Ev
.Ltmp456:
.LBB0_842:
	movq	%r15, %r14
.LBB0_843:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 80(%rsp)
.Ltmp460:
.Lcfi294:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp461:
# BB#844:
.Ltmp466:
.Lcfi295:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp467:
.LBB0_845:                              # %unwind_resume
.Lcfi296:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_846:
.Ltmp462:
	movq	%rax, %rbx
.Ltmp463:
.Lcfi297:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp464:
	jmp	.LBB0_849
.LBB0_847:
.Ltmp465:
.Lcfi298:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_848:
.Ltmp468:
	movq	%rax, %rbx
.LBB0_849:                              # %.body
.Lcfi299:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_Z5Main2iPPKc, .Lfunc_end0-_Z5Main2iPPKc
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\256\214\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\245\f"                # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp5-.Ltmp0           #   Call between .Ltmp0 and .Ltmp5
	.long	.Ltmp459-.Lfunc_begin0  #     jumps to .Ltmp459
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp14-.Ltmp9          #   Call between .Ltmp9 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin0   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp21-.Ltmp16         #   Call between .Ltmp16 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin0   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin0   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp450-.Lfunc_begin0  #     jumps to .Ltmp450
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp58-.Ltmp33         #   Call between .Ltmp33 and .Ltmp58
	.long	.Ltmp303-.Lfunc_begin0  #     jumps to .Ltmp303
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp70-.Ltmp59         #   Call between .Ltmp59 and .Ltmp70
	.long	.Ltmp78-.Lfunc_begin0   #     jumps to .Ltmp78
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin0   #     jumps to .Ltmp73
	.byte	0                       #   On action: cleanup
	.long	.Ltmp74-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp77-.Ltmp74         #   Call between .Ltmp74 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin0   #     jumps to .Ltmp78
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp91-.Lfunc_begin0   #     jumps to .Ltmp91
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp82-.Ltmp81         #   Call between .Ltmp81 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin0   #     jumps to .Ltmp83
	.byte	0                       #   On action: cleanup
	.long	.Ltmp84-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp85-.Ltmp84         #   Call between .Ltmp84 and .Ltmp85
	.long	.Ltmp91-.Lfunc_begin0   #     jumps to .Ltmp91
	.byte	0                       #   On action: cleanup
	.long	.Ltmp86-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp87-.Ltmp86         #   Call between .Ltmp86 and .Ltmp87
	.long	.Ltmp88-.Lfunc_begin0   #     jumps to .Ltmp88
	.byte	0                       #   On action: cleanup
	.long	.Ltmp89-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp90-.Ltmp89         #   Call between .Ltmp89 and .Ltmp90
	.long	.Ltmp91-.Lfunc_begin0   #     jumps to .Ltmp91
	.byte	0                       #   On action: cleanup
	.long	.Ltmp92-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp93-.Ltmp92         #   Call between .Ltmp92 and .Ltmp93
	.long	.Ltmp99-.Lfunc_begin0   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin0   #     jumps to .Ltmp96
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin0   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin0  # >> Call Site 20 <<
	.long	.Ltmp107-.Ltmp104       #   Call between .Ltmp104 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin0  #     jumps to .Ltmp108
	.byte	0                       #   On action: cleanup
	.long	.Ltmp109-.Lfunc_begin0  # >> Call Site 21 <<
	.long	.Ltmp110-.Ltmp109       #   Call between .Ltmp109 and .Ltmp110
	.long	.Ltmp111-.Lfunc_begin0  #     jumps to .Ltmp111
	.byte	0                       #   On action: cleanup
	.long	.Ltmp112-.Lfunc_begin0  # >> Call Site 22 <<
	.long	.Ltmp117-.Ltmp112       #   Call between .Ltmp112 and .Ltmp117
	.long	.Ltmp118-.Lfunc_begin0  #     jumps to .Ltmp118
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin0   # >> Call Site 23 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp303-.Lfunc_begin0  #     jumps to .Ltmp303
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin0   # >> Call Site 24 <<
	.long	.Ltmp42-.Ltmp39         #   Call between .Ltmp39 and .Ltmp42
	.long	.Ltmp45-.Lfunc_begin0   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp301-.Lfunc_begin0  # >> Call Site 25 <<
	.long	.Ltmp302-.Ltmp301       #   Call between .Ltmp301 and .Ltmp302
	.long	.Ltmp303-.Lfunc_begin0  #     jumps to .Ltmp303
	.byte	0                       #   On action: cleanup
	.long	.Ltmp304-.Lfunc_begin0  # >> Call Site 26 <<
	.long	.Ltmp305-.Ltmp304       #   Call between .Ltmp304 and .Ltmp305
	.long	.Ltmp306-.Lfunc_begin0  #     jumps to .Ltmp306
	.byte	0                       #   On action: cleanup
	.long	.Ltmp307-.Lfunc_begin0  # >> Call Site 27 <<
	.long	.Ltmp308-.Ltmp307       #   Call between .Ltmp307 and .Ltmp308
	.long	.Ltmp309-.Lfunc_begin0  #     jumps to .Ltmp309
	.byte	0                       #   On action: cleanup
	.long	.Ltmp119-.Lfunc_begin0  # >> Call Site 28 <<
	.long	.Ltmp120-.Ltmp119       #   Call between .Ltmp119 and .Ltmp120
	.long	.Ltmp303-.Lfunc_begin0  #     jumps to .Ltmp303
	.byte	0                       #   On action: cleanup
	.long	.Ltmp129-.Lfunc_begin0  # >> Call Site 29 <<
	.long	.Ltmp132-.Ltmp129       #   Call between .Ltmp129 and .Ltmp132
	.long	.Ltmp133-.Lfunc_begin0  #     jumps to .Ltmp133
	.byte	0                       #   On action: cleanup
	.long	.Ltmp134-.Lfunc_begin0  # >> Call Site 30 <<
	.long	.Ltmp137-.Ltmp134       #   Call between .Ltmp134 and .Ltmp137
	.long	.Ltmp138-.Lfunc_begin0  #     jumps to .Ltmp138
	.byte	0                       #   On action: cleanup
	.long	.Ltmp139-.Lfunc_begin0  # >> Call Site 31 <<
	.long	.Ltmp146-.Ltmp139       #   Call between .Ltmp139 and .Ltmp146
	.long	.Ltmp147-.Lfunc_begin0  #     jumps to .Ltmp147
	.byte	0                       #   On action: cleanup
	.long	.Ltmp148-.Lfunc_begin0  # >> Call Site 32 <<
	.long	.Ltmp149-.Ltmp148       #   Call between .Ltmp148 and .Ltmp149
	.long	.Ltmp150-.Lfunc_begin0  #     jumps to .Ltmp150
	.byte	0                       #   On action: cleanup
	.long	.Ltmp151-.Lfunc_begin0  # >> Call Site 33 <<
	.long	.Ltmp152-.Ltmp151       #   Call between .Ltmp151 and .Ltmp152
	.long	.Ltmp153-.Lfunc_begin0  #     jumps to .Ltmp153
	.byte	0                       #   On action: cleanup
	.long	.Ltmp154-.Lfunc_begin0  # >> Call Site 34 <<
	.long	.Ltmp159-.Ltmp154       #   Call between .Ltmp154 and .Ltmp159
	.long	.Ltmp160-.Lfunc_begin0  #     jumps to .Ltmp160
	.byte	0                       #   On action: cleanup
	.long	.Ltmp161-.Lfunc_begin0  # >> Call Site 35 <<
	.long	.Ltmp162-.Ltmp161       #   Call between .Ltmp161 and .Ltmp162
	.long	.Ltmp163-.Lfunc_begin0  #     jumps to .Ltmp163
	.byte	0                       #   On action: cleanup
	.long	.Ltmp164-.Lfunc_begin0  # >> Call Site 36 <<
	.long	.Ltmp165-.Ltmp164       #   Call between .Ltmp164 and .Ltmp165
	.long	.Ltmp166-.Lfunc_begin0  #     jumps to .Ltmp166
	.byte	0                       #   On action: cleanup
	.long	.Ltmp167-.Lfunc_begin0  # >> Call Site 37 <<
	.long	.Ltmp174-.Ltmp167       #   Call between .Ltmp167 and .Ltmp174
	.long	.Ltmp207-.Lfunc_begin0  #     jumps to .Ltmp207
	.byte	0                       #   On action: cleanup
	.long	.Ltmp175-.Lfunc_begin0  # >> Call Site 38 <<
	.long	.Ltmp180-.Ltmp175       #   Call between .Ltmp175 and .Ltmp180
	.long	.Ltmp181-.Lfunc_begin0  #     jumps to .Ltmp181
	.byte	0                       #   On action: cleanup
	.long	.Ltmp182-.Lfunc_begin0  # >> Call Site 39 <<
	.long	.Ltmp183-.Ltmp182       #   Call between .Ltmp182 and .Ltmp183
	.long	.Ltmp184-.Lfunc_begin0  #     jumps to .Ltmp184
	.byte	0                       #   On action: cleanup
	.long	.Ltmp185-.Lfunc_begin0  # >> Call Site 40 <<
	.long	.Ltmp188-.Ltmp185       #   Call between .Ltmp185 and .Ltmp188
	.long	.Ltmp189-.Lfunc_begin0  #     jumps to .Ltmp189
	.byte	0                       #   On action: cleanup
	.long	.Ltmp190-.Lfunc_begin0  # >> Call Site 41 <<
	.long	.Ltmp203-.Ltmp190       #   Call between .Ltmp190 and .Ltmp203
	.long	.Ltmp204-.Lfunc_begin0  #     jumps to .Ltmp204
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin0   # >> Call Site 42 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp303-.Lfunc_begin0  #     jumps to .Ltmp303
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin0   # >> Call Site 43 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp52-.Lfunc_begin0   #     jumps to .Ltmp52
	.byte	0                       #   On action: cleanup
	.long	.Ltmp289-.Lfunc_begin0  # >> Call Site 44 <<
	.long	.Ltmp290-.Ltmp289       #   Call between .Ltmp289 and .Ltmp290
	.long	.Ltmp291-.Lfunc_begin0  #     jumps to .Ltmp291
	.byte	0                       #   On action: cleanup
	.long	.Ltmp292-.Lfunc_begin0  # >> Call Site 45 <<
	.long	.Ltmp297-.Ltmp292       #   Call between .Ltmp292 and .Ltmp297
	.long	.Ltmp300-.Lfunc_begin0  #     jumps to .Ltmp300
	.byte	0                       #   On action: cleanup
	.long	.Ltmp205-.Lfunc_begin0  # >> Call Site 46 <<
	.long	.Ltmp206-.Ltmp205       #   Call between .Ltmp205 and .Ltmp206
	.long	.Ltmp207-.Lfunc_begin0  #     jumps to .Ltmp207
	.byte	0                       #   On action: cleanup
	.long	.Ltmp242-.Lfunc_begin0  # >> Call Site 47 <<
	.long	.Ltmp249-.Ltmp242       #   Call between .Ltmp242 and .Ltmp249
	.long	.Ltmp283-.Lfunc_begin0  #     jumps to .Ltmp283
	.byte	0                       #   On action: cleanup
	.long	.Ltmp250-.Lfunc_begin0  # >> Call Site 48 <<
	.long	.Ltmp255-.Ltmp250       #   Call between .Ltmp250 and .Ltmp255
	.long	.Ltmp256-.Lfunc_begin0  #     jumps to .Ltmp256
	.byte	0                       #   On action: cleanup
	.long	.Ltmp257-.Lfunc_begin0  # >> Call Site 49 <<
	.long	.Ltmp258-.Ltmp257       #   Call between .Ltmp257 and .Ltmp258
	.long	.Ltmp259-.Lfunc_begin0  #     jumps to .Ltmp259
	.byte	0                       #   On action: cleanup
	.long	.Ltmp260-.Lfunc_begin0  # >> Call Site 50 <<
	.long	.Ltmp263-.Ltmp260       #   Call between .Ltmp260 and .Ltmp263
	.long	.Ltmp264-.Lfunc_begin0  #     jumps to .Ltmp264
	.byte	0                       #   On action: cleanup
	.long	.Ltmp265-.Lfunc_begin0  # >> Call Site 51 <<
	.long	.Ltmp278-.Ltmp265       #   Call between .Ltmp265 and .Ltmp278
	.long	.Ltmp283-.Lfunc_begin0  #     jumps to .Ltmp283
	.byte	0                       #   On action: cleanup
	.long	.Ltmp310-.Lfunc_begin0  # >> Call Site 52 <<
	.long	.Ltmp311-.Ltmp310       #   Call between .Ltmp310 and .Ltmp311
	.long	.Ltmp314-.Lfunc_begin0  #     jumps to .Ltmp314
	.byte	0                       #   On action: cleanup
	.long	.Ltmp311-.Lfunc_begin0  # >> Call Site 53 <<
	.long	.Ltmp298-.Ltmp311       #   Call between .Ltmp311 and .Ltmp298
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp298-.Lfunc_begin0  # >> Call Site 54 <<
	.long	.Ltmp299-.Ltmp298       #   Call between .Ltmp298 and .Ltmp299
	.long	.Ltmp300-.Lfunc_begin0  #     jumps to .Ltmp300
	.byte	0                       #   On action: cleanup
	.long	.Ltmp279-.Lfunc_begin0  # >> Call Site 55 <<
	.long	.Ltmp282-.Ltmp279       #   Call between .Ltmp279 and .Ltmp282
	.long	.Ltmp283-.Lfunc_begin0  #     jumps to .Ltmp283
	.byte	0                       #   On action: cleanup
	.long	.Ltmp286-.Lfunc_begin0  # >> Call Site 56 <<
	.long	.Ltmp287-.Ltmp286       #   Call between .Ltmp286 and .Ltmp287
	.long	.Ltmp288-.Lfunc_begin0  #     jumps to .Ltmp288
	.byte	0                       #   On action: cleanup
	.long	.Ltmp208-.Lfunc_begin0  # >> Call Site 57 <<
	.long	.Ltmp209-.Ltmp208       #   Call between .Ltmp208 and .Ltmp209
	.long	.Ltmp241-.Lfunc_begin0  #     jumps to .Ltmp241
	.byte	0                       #   On action: cleanup
	.long	.Ltmp210-.Lfunc_begin0  # >> Call Site 58 <<
	.long	.Ltmp211-.Ltmp210       #   Call between .Ltmp210 and .Ltmp211
	.long	.Ltmp212-.Lfunc_begin0  #     jumps to .Ltmp212
	.byte	0                       #   On action: cleanup
	.long	.Ltmp211-.Lfunc_begin0  # >> Call Site 59 <<
	.long	.Ltmp213-.Ltmp211       #   Call between .Ltmp211 and .Ltmp213
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp213-.Lfunc_begin0  # >> Call Site 60 <<
	.long	.Ltmp214-.Ltmp213       #   Call between .Ltmp213 and .Ltmp214
	.long	.Ltmp241-.Lfunc_begin0  #     jumps to .Ltmp241
	.byte	0                       #   On action: cleanup
	.long	.Ltmp312-.Lfunc_begin0  # >> Call Site 61 <<
	.long	.Ltmp313-.Ltmp312       #   Call between .Ltmp312 and .Ltmp313
	.long	.Ltmp314-.Lfunc_begin0  #     jumps to .Ltmp314
	.byte	0                       #   On action: cleanup
	.long	.Ltmp315-.Lfunc_begin0  # >> Call Site 62 <<
	.long	.Ltmp318-.Ltmp315       #   Call between .Ltmp315 and .Ltmp318
	.long	.Ltmp319-.Lfunc_begin0  #     jumps to .Ltmp319
	.byte	0                       #   On action: cleanup
	.long	.Ltmp320-.Lfunc_begin0  # >> Call Site 63 <<
	.long	.Ltmp327-.Ltmp320       #   Call between .Ltmp320 and .Ltmp327
	.long	.Ltmp341-.Lfunc_begin0  #     jumps to .Ltmp341
	.byte	0                       #   On action: cleanup
	.long	.Ltmp328-.Lfunc_begin0  # >> Call Site 64 <<
	.long	.Ltmp329-.Ltmp328       #   Call between .Ltmp328 and .Ltmp329
	.long	.Ltmp338-.Lfunc_begin0  #     jumps to .Ltmp338
	.byte	0                       #   On action: cleanup
	.long	.Ltmp330-.Lfunc_begin0  # >> Call Site 65 <<
	.long	.Ltmp331-.Ltmp330       #   Call between .Ltmp330 and .Ltmp331
	.long	.Ltmp332-.Lfunc_begin0  #     jumps to .Ltmp332
	.byte	0                       #   On action: cleanup
	.long	.Ltmp333-.Lfunc_begin0  # >> Call Site 66 <<
	.long	.Ltmp334-.Ltmp333       #   Call between .Ltmp333 and .Ltmp334
	.long	.Ltmp335-.Lfunc_begin0  #     jumps to .Ltmp335
	.byte	0                       #   On action: cleanup
	.long	.Ltmp336-.Lfunc_begin0  # >> Call Site 67 <<
	.long	.Ltmp337-.Ltmp336       #   Call between .Ltmp336 and .Ltmp337
	.long	.Ltmp338-.Lfunc_begin0  #     jumps to .Ltmp338
	.byte	0                       #   On action: cleanup
	.long	.Ltmp339-.Lfunc_begin0  # >> Call Site 68 <<
	.long	.Ltmp340-.Ltmp339       #   Call between .Ltmp339 and .Ltmp340
	.long	.Ltmp341-.Lfunc_begin0  #     jumps to .Ltmp341
	.byte	0                       #   On action: cleanup
	.long	.Ltmp342-.Lfunc_begin0  # >> Call Site 69 <<
	.long	.Ltmp343-.Ltmp342       #   Call between .Ltmp342 and .Ltmp343
	.long	.Ltmp344-.Lfunc_begin0  #     jumps to .Ltmp344
	.byte	0                       #   On action: cleanup
	.long	.Ltmp345-.Lfunc_begin0  # >> Call Site 70 <<
	.long	.Ltmp372-.Ltmp345       #   Call between .Ltmp345 and .Ltmp372
	.long	.Ltmp401-.Lfunc_begin0  #     jumps to .Ltmp401
	.byte	0                       #   On action: cleanup
	.long	.Ltmp420-.Lfunc_begin0  # >> Call Site 71 <<
	.long	.Ltmp421-.Ltmp420       #   Call between .Ltmp420 and .Ltmp421
	.long	.Ltmp422-.Lfunc_begin0  #     jumps to .Ltmp422
	.byte	0                       #   On action: cleanup
	.long	.Ltmp426-.Lfunc_begin0  # >> Call Site 72 <<
	.long	.Ltmp427-.Ltmp426       #   Call between .Ltmp426 and .Ltmp427
	.long	.Ltmp428-.Lfunc_begin0  #     jumps to .Ltmp428
	.byte	0                       #   On action: cleanup
	.long	.Ltmp431-.Lfunc_begin0  # >> Call Site 73 <<
	.long	.Ltmp432-.Ltmp431       #   Call between .Ltmp431 and .Ltmp432
	.long	.Ltmp433-.Lfunc_begin0  #     jumps to .Ltmp433
	.byte	0                       #   On action: cleanup
	.long	.Ltmp436-.Lfunc_begin0  # >> Call Site 74 <<
	.long	.Ltmp437-.Ltmp436       #   Call between .Ltmp436 and .Ltmp437
	.long	.Ltmp438-.Lfunc_begin0  #     jumps to .Ltmp438
	.byte	0                       #   On action: cleanup
	.long	.Ltmp439-.Lfunc_begin0  # >> Call Site 75 <<
	.long	.Ltmp440-.Ltmp439       #   Call between .Ltmp439 and .Ltmp440
	.long	.Ltmp441-.Lfunc_begin0  #     jumps to .Ltmp441
	.byte	0                       #   On action: cleanup
	.long	.Ltmp442-.Lfunc_begin0  # >> Call Site 76 <<
	.long	.Ltmp443-.Ltmp442       #   Call between .Ltmp442 and .Ltmp443
	.long	.Ltmp444-.Lfunc_begin0  #     jumps to .Ltmp444
	.byte	0                       #   On action: cleanup
	.long	.Ltmp445-.Lfunc_begin0  # >> Call Site 77 <<
	.long	.Ltmp446-.Ltmp445       #   Call between .Ltmp445 and .Ltmp446
	.long	.Ltmp447-.Lfunc_begin0  #     jumps to .Ltmp447
	.byte	0                       #   On action: cleanup
	.long	.Ltmp469-.Lfunc_begin0  # >> Call Site 78 <<
	.long	.Ltmp470-.Ltmp469       #   Call between .Ltmp469 and .Ltmp470
	.long	.Ltmp471-.Lfunc_begin0  #     jumps to .Ltmp471
	.byte	0                       #   On action: cleanup
	.long	.Ltmp470-.Lfunc_begin0  # >> Call Site 79 <<
	.long	.Ltmp377-.Ltmp470       #   Call between .Ltmp470 and .Ltmp377
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp377-.Lfunc_begin0  # >> Call Site 80 <<
	.long	.Ltmp400-.Ltmp377       #   Call between .Ltmp377 and .Ltmp400
	.long	.Ltmp401-.Lfunc_begin0  #     jumps to .Ltmp401
	.byte	0                       #   On action: cleanup
	.long	.Ltmp402-.Lfunc_begin0  # >> Call Site 81 <<
	.long	.Ltmp409-.Ltmp402       #   Call between .Ltmp402 and .Ltmp409
	.long	.Ltmp410-.Lfunc_begin0  #     jumps to .Ltmp410
	.byte	0                       #   On action: cleanup
	.long	.Ltmp215-.Lfunc_begin0  # >> Call Site 82 <<
	.long	.Ltmp216-.Ltmp215       #   Call between .Ltmp215 and .Ltmp216
	.long	.Ltmp217-.Lfunc_begin0  #     jumps to .Ltmp217
	.byte	0                       #   On action: cleanup
	.long	.Ltmp218-.Lfunc_begin0  # >> Call Site 83 <<
	.long	.Ltmp219-.Ltmp218       #   Call between .Ltmp218 and .Ltmp219
	.long	.Ltmp241-.Lfunc_begin0  #     jumps to .Ltmp241
	.byte	0                       #   On action: cleanup
	.long	.Ltmp220-.Lfunc_begin0  # >> Call Site 84 <<
	.long	.Ltmp221-.Ltmp220       #   Call between .Ltmp220 and .Ltmp221
	.long	.Ltmp222-.Lfunc_begin0  #     jumps to .Ltmp222
	.byte	0                       #   On action: cleanup
	.long	.Ltmp223-.Lfunc_begin0  # >> Call Site 85 <<
	.long	.Ltmp224-.Ltmp223       #   Call between .Ltmp223 and .Ltmp224
	.long	.Ltmp225-.Lfunc_begin0  #     jumps to .Ltmp225
	.byte	0                       #   On action: cleanup
	.long	.Ltmp226-.Lfunc_begin0  # >> Call Site 86 <<
	.long	.Ltmp227-.Ltmp226       #   Call between .Ltmp226 and .Ltmp227
	.long	.Ltmp228-.Lfunc_begin0  #     jumps to .Ltmp228
	.byte	0                       #   On action: cleanup
	.long	.Ltmp229-.Lfunc_begin0  # >> Call Site 87 <<
	.long	.Ltmp230-.Ltmp229       #   Call between .Ltmp229 and .Ltmp230
	.long	.Ltmp231-.Lfunc_begin0  #     jumps to .Ltmp231
	.byte	0                       #   On action: cleanup
	.long	.Ltmp232-.Lfunc_begin0  # >> Call Site 88 <<
	.long	.Ltmp233-.Ltmp232       #   Call between .Ltmp232 and .Ltmp233
	.long	.Ltmp234-.Lfunc_begin0  #     jumps to .Ltmp234
	.byte	0                       #   On action: cleanup
	.long	.Ltmp235-.Lfunc_begin0  # >> Call Site 89 <<
	.long	.Ltmp238-.Ltmp235       #   Call between .Ltmp235 and .Ltmp238
	.long	.Ltmp241-.Lfunc_begin0  #     jumps to .Ltmp241
	.byte	0                       #   On action: cleanup
	.long	.Ltmp238-.Lfunc_begin0  # >> Call Site 90 <<
	.long	.Ltmp239-.Ltmp238       #   Call between .Ltmp238 and .Ltmp239
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp239-.Lfunc_begin0  # >> Call Site 91 <<
	.long	.Ltmp240-.Ltmp239       #   Call between .Ltmp239 and .Ltmp240
	.long	.Ltmp241-.Lfunc_begin0  #     jumps to .Ltmp241
	.byte	0                       #   On action: cleanup
	.long	.Ltmp240-.Lfunc_begin0  # >> Call Site 92 <<
	.long	.Ltmp448-.Ltmp240       #   Call between .Ltmp240 and .Ltmp448
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp448-.Lfunc_begin0  # >> Call Site 93 <<
	.long	.Ltmp449-.Ltmp448       #   Call between .Ltmp448 and .Ltmp449
	.long	.Ltmp450-.Lfunc_begin0  #     jumps to .Ltmp450
	.byte	0                       #   On action: cleanup
	.long	.Ltmp449-.Lfunc_begin0  # >> Call Site 94 <<
	.long	.Ltmp26-.Ltmp449        #   Call between .Ltmp449 and .Ltmp26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin0   # >> Call Site 95 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp30-.Lfunc_begin0   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin0   # >> Call Site 96 <<
	.long	.Ltmp35-.Ltmp27         #   Call between .Ltmp27 and .Ltmp35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin0   # >> Call Site 97 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp303-.Lfunc_begin0  #     jumps to .Ltmp303
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin0   # >> Call Site 98 <<
	.long	.Ltmp43-.Ltmp36         #   Call between .Ltmp36 and .Ltmp43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin0   # >> Call Site 99 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin0   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp121-.Lfunc_begin0  # >> Call Site 100 <<
	.long	.Ltmp126-.Ltmp121       #   Call between .Ltmp121 and .Ltmp126
	.long	.Ltmp303-.Lfunc_begin0  #     jumps to .Ltmp303
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin0  # >> Call Site 101 <<
	.long	.Ltmp127-.Ltmp126       #   Call between .Ltmp126 and .Ltmp127
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp127-.Lfunc_begin0  # >> Call Site 102 <<
	.long	.Ltmp128-.Ltmp127       #   Call between .Ltmp127 and .Ltmp128
	.long	.Ltmp303-.Lfunc_begin0  #     jumps to .Ltmp303
	.byte	0                       #   On action: cleanup
	.long	.Ltmp128-.Lfunc_begin0  # >> Call Site 103 <<
	.long	.Ltmp156-.Ltmp128       #   Call between .Ltmp128 and .Ltmp156
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp156-.Lfunc_begin0  # >> Call Site 104 <<
	.long	.Ltmp157-.Ltmp156       #   Call between .Ltmp156 and .Ltmp157
	.long	.Ltmp160-.Lfunc_begin0  #     jumps to .Ltmp160
	.byte	0                       #   On action: cleanup
	.long	.Ltmp157-.Lfunc_begin0  # >> Call Site 105 <<
	.long	.Ltmp50-.Ltmp157        #   Call between .Ltmp157 and .Ltmp50
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin0   # >> Call Site 106 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin0   #     jumps to .Ltmp52
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin0   # >> Call Site 107 <<
	.long	.Ltmp373-.Ltmp51        #   Call between .Ltmp51 and .Ltmp373
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp373-.Lfunc_begin0  # >> Call Site 108 <<
	.long	.Ltmp374-.Ltmp373       #   Call between .Ltmp373 and .Ltmp374
	.long	.Ltmp401-.Lfunc_begin0  #     jumps to .Ltmp401
	.byte	0                       #   On action: cleanup
	.long	.Ltmp374-.Lfunc_begin0  # >> Call Site 109 <<
	.long	.Ltmp375-.Ltmp374       #   Call between .Ltmp374 and .Ltmp375
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp375-.Lfunc_begin0  # >> Call Site 110 <<
	.long	.Ltmp376-.Ltmp375       #   Call between .Ltmp375 and .Ltmp376
	.long	.Ltmp401-.Lfunc_begin0  #     jumps to .Ltmp401
	.byte	0                       #   On action: cleanup
	.long	.Ltmp423-.Lfunc_begin0  # >> Call Site 111 <<
	.long	.Ltmp424-.Ltmp423       #   Call between .Ltmp423 and .Ltmp424
	.long	.Ltmp425-.Lfunc_begin0  #     jumps to .Ltmp425
	.byte	1                       #   On action: 1
	.long	.Ltmp411-.Lfunc_begin0  # >> Call Site 112 <<
	.long	.Ltmp412-.Ltmp411       #   Call between .Ltmp411 and .Ltmp412
	.long	.Ltmp413-.Lfunc_begin0  #     jumps to .Ltmp413
	.byte	1                       #   On action: 1
	.long	.Ltmp417-.Lfunc_begin0  # >> Call Site 113 <<
	.long	.Ltmp418-.Ltmp417       #   Call between .Ltmp417 and .Ltmp418
	.long	.Ltmp419-.Lfunc_begin0  #     jumps to .Ltmp419
	.byte	1                       #   On action: 1
	.long	.Ltmp429-.Lfunc_begin0  # >> Call Site 114 <<
	.long	.Ltmp430-.Ltmp429       #   Call between .Ltmp429 and .Ltmp430
	.long	.Ltmp468-.Lfunc_begin0  #     jumps to .Ltmp468
	.byte	1                       #   On action: 1
	.long	.Ltmp414-.Lfunc_begin0  # >> Call Site 115 <<
	.long	.Ltmp415-.Ltmp414       #   Call between .Ltmp414 and .Ltmp415
	.long	.Ltmp416-.Lfunc_begin0  #     jumps to .Ltmp416
	.byte	1                       #   On action: 1
	.long	.Ltmp472-.Lfunc_begin0  # >> Call Site 116 <<
	.long	.Ltmp473-.Ltmp472       #   Call between .Ltmp472 and .Ltmp473
	.long	.Ltmp474-.Lfunc_begin0  #     jumps to .Ltmp474
	.byte	1                       #   On action: 1
	.long	.Ltmp284-.Lfunc_begin0  # >> Call Site 117 <<
	.long	.Ltmp456-.Ltmp284       #   Call between .Ltmp284 and .Ltmp456
	.long	.Ltmp468-.Lfunc_begin0  #     jumps to .Ltmp468
	.byte	1                       #   On action: 1
	.long	.Ltmp460-.Lfunc_begin0  # >> Call Site 118 <<
	.long	.Ltmp461-.Ltmp460       #   Call between .Ltmp460 and .Ltmp461
	.long	.Ltmp462-.Lfunc_begin0  #     jumps to .Ltmp462
	.byte	1                       #   On action: 1
	.long	.Ltmp466-.Lfunc_begin0  # >> Call Site 119 <<
	.long	.Ltmp467-.Ltmp466       #   Call between .Ltmp466 and .Ltmp467
	.long	.Ltmp468-.Lfunc_begin0  #     jumps to .Ltmp468
	.byte	1                       #   On action: 1
	.long	.Ltmp467-.Lfunc_begin0  # >> Call Site 120 <<
	.long	.Ltmp463-.Ltmp467       #   Call between .Ltmp467 and .Ltmp463
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp463-.Lfunc_begin0  # >> Call Site 121 <<
	.long	.Ltmp464-.Ltmp463       #   Call between .Ltmp463 and .Ltmp464
	.long	.Ltmp465-.Lfunc_begin0  #     jumps to .Ltmp465
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZL20ShowCopyrightAndHelpR13CStdOutStreamb,@function
_ZL20ShowCopyrightAndHelpR13CStdOutStreamb: # @_ZL20ShowCopyrightAndHelpR13CStdOutStreamb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi300:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi301:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi302:
	.cfi_def_cfa_offset 32
.Lcfi303:
	.cfi_offset %rbx, -32
.Lcfi304:
	.cfi_offset %r14, -24
.Lcfi305:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movl	$.L.str.34, %esi
	callq	_ZN13CStdOutStreamlsEPKc
	movl	$.L.str.26, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
	movq	%rax, %rbp
	callq	_Z12my_getlocalev
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	_ZN13CStdOutStreamlsEPKc
	movl	$.L.str.27, %esi
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
	cmpl	$0, global_use_utf16_conversion(%rip)
	je	.LBB1_2
# BB#1:
	movl	$.L.str.28, %esi
	jmp	.LBB1_3
.LBB1_2:
	movl	$.L.str.29, %esi
.LBB1_3:
	movq	%rbx, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
	movl	$.L.str.30, %esi
	movq	%rbx, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
	movl	$.L.str.31, %esi
	movq	%rbx, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
	callq	_ZN8NWindows7NSystem21GetNumberOfProcessorsEv
	movl	%eax, %ebp
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	_ZN13CStdOutStreamlsEi
	cmpl	$2, %ebp
	jl	.LBB1_5
# BB#4:
	movl	$.L.str.32, %esi
	jmp	.LBB1_6
.LBB1_5:
	movl	$.L.str.33, %esi
.LBB1_6:
	movq	%rax, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
	testb	%r14b, %r14b
	je	.LBB1_8
# BB#7:
	movl	$.L.str.35, %esi
	movq	%rbx, %rdi
	callq	_ZN13CStdOutStreamlsEPKc
.LBB1_8:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZL20ShowCopyrightAndHelpR13CStdOutStreamb, .Lfunc_end1-_ZL20ShowCopyrightAndHelpR13CStdOutStreamb
	.cfi_endproc

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,@function
_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii: # @_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi306:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi307:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi308:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi309:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi310:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi311:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi312:
	.cfi_def_cfa_offset 64
.Lcfi313:
	.cfi_offset %rbx, -56
.Lcfi314:
	.cfi_offset %r12, -48
.Lcfi315:
	.cfi_offset %r13, -40
.Lcfi316:
	.cfi_offset %r14, -32
.Lcfi317:
	.cfi_offset %r15, -24
.Lcfi318:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB2_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB2_6
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_5
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	callq	_ZdaPv
.LBB2_5:                                # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB2_6:                                #   in Loop: Header=BB2_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB2_2
.LBB2_7:                                # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end2:
	.size	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii, .Lfunc_end2-_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.zero	16
	.section	.text._ZN26CArchiveCommandLineOptionsC2Ev,"axG",@progbits,_ZN26CArchiveCommandLineOptionsC2Ev,comdat
	.weak	_ZN26CArchiveCommandLineOptionsC2Ev
	.p2align	4, 0x90
	.type	_ZN26CArchiveCommandLineOptionsC2Ev,@function
_ZN26CArchiveCommandLineOptionsC2Ev:    # @_ZN26CArchiveCommandLineOptionsC2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi319:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi320:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi321:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi322:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi323:
	.cfi_def_cfa_offset 48
.Lcfi324:
	.cfi_offset %rbx, -40
.Lcfi325:
	.cfi_offset %r12, -32
.Lcfi326:
	.cfi_offset %r14, -24
.Lcfi327:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movb	$0, 5(%rbx)
	movb	$0, 6(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	$8, 40(%rbx)
	movq	$_ZTV13CObjectVectorIN9NWildcard5CPairEE+16, 16(%rbx)
	movups	%xmm0, 56(%rbx)
.Ltmp475:
	movl	$16, %edi
	callq	_Znam
.Ltmp476:
# BB#1:
	movq	%rax, 56(%rbx)
	movl	$0, (%rax)
	movl	$4, 68(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 80(%rbx)
.Ltmp478:
	movl	$16, %edi
	callq	_Znam
.Ltmp479:
# BB#2:
	movq	%rax, 80(%rbx)
	movl	$0, (%rax)
	movl	$4, 92(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 104(%rbx)
.Ltmp481:
	movl	$16, %edi
	callq	_Znam
.Ltmp482:
# BB#3:
	movq	%rax, 104(%rbx)
	movl	$0, (%rax)
	movl	$4, 116(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 136(%rbx)
	movq	$8, 152(%rbx)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 128(%rbx)
	movups	%xmm0, 168(%rbx)
	movq	$8, 184(%rbx)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 160(%rbx)
	movups	%xmm0, 200(%rbx)
	movq	$8, 216(%rbx)
	movq	$_ZTV13CObjectVectorI9CPropertyE+16, 192(%rbx)
	leaq	224(%rbx), %r15
.Ltmp484:
	movq	%r15, %rdi
	callq	_ZN14CUpdateOptionsC2Ev
.Ltmp485:
# BB#4:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 544(%rbx)
.Ltmp487:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r12
.Ltmp488:
# BB#5:
	movq	%r12, 544(%rbx)
	movl	$0, (%r12)
	movl	$4, 556(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 576(%rbx)
.Ltmp490:
	movl	$16, %edi
	callq	_Znam
.Ltmp491:
# BB#6:
	movq	%rax, 576(%rbx)
	movl	$0, (%rax)
	movl	$4, 588(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB3_19:
.Ltmp492:
	movq	%rax, %r14
	movq	%r12, %rdi
	callq	_ZdaPv
	jmp	.LBB3_20
.LBB3_18:
.Ltmp489:
	movq	%rax, %r14
.LBB3_20:                               # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp493:
	movq	%r15, %rdi
	callq	_ZN14CUpdateOptionsD2Ev
.Ltmp494:
	jmp	.LBB3_21
.LBB3_17:
.Ltmp486:
	movq	%rax, %r14
.LBB3_21:
	leaq	192(%rbx), %r15
	movq	$_ZTV13CObjectVectorI9CPropertyE+16, (%r15)
.Ltmp495:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp496:
# BB#22:
.Ltmp501:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp502:
# BB#23:                                # %_ZN13CObjectVectorI9CPropertyED2Ev.exit
	leaq	160(%rbx), %r15
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%r15)
.Ltmp503:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp504:
# BB#24:
.Ltmp509:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp510:
# BB#25:                                # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rbx, %r15
	subq	$-128, %r15
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%r15)
.Ltmp511:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp512:
# BB#26:
.Ltmp517:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp518:
# BB#27:                                # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit20
	leaq	104(%rbx), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB3_15
# BB#28:
	callq	_ZdaPv
	jmp	.LBB3_15
.LBB3_33:
.Ltmp513:
	movq	%rax, %r14
.Ltmp514:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp515:
	jmp	.LBB3_38
.LBB3_34:
.Ltmp516:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_31:
.Ltmp505:
	movq	%rax, %r14
.Ltmp506:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp507:
	jmp	.LBB3_38
.LBB3_32:
.Ltmp508:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_29:
.Ltmp497:
	movq	%rax, %r14
.Ltmp498:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp499:
	jmp	.LBB3_38
.LBB3_30:
.Ltmp500:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_14:
.Ltmp483:
	movq	%rax, %r14
.LBB3_15:                               # %_ZN11CStringBaseIwED2Ev.exit21
	leaq	80(%rbx), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB3_12
# BB#16:
	callq	_ZdaPv
	jmp	.LBB3_12
.LBB3_11:
.Ltmp480:
	movq	%rax, %r14
.LBB3_12:                               # %_ZN11CStringBaseIwED2Ev.exit22
	leaq	56(%rbx), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB3_8
# BB#13:
	callq	_ZdaPv
	jmp	.LBB3_8
.LBB3_7:
.Ltmp477:
	movq	%rax, %r14
.LBB3_8:                                # %_ZN11CStringBaseIwED2Ev.exit23
	addq	$16, %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CPairEE+16, (%rbx)
.Ltmp519:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp520:
# BB#9:                                 # %_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev.exit.i
.Ltmp525:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp526:
# BB#10:                                # %_ZN9NWildcard7CCensorD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_35:
.Ltmp521:
	movq	%rax, %r14
.Ltmp522:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp523:
	jmp	.LBB3_38
.LBB3_36:
.Ltmp524:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_37:
.Ltmp527:
	movq	%rax, %r14
.LBB3_38:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN26CArchiveCommandLineOptionsC2Ev, .Lfunc_end3-_ZN26CArchiveCommandLineOptionsC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\215\202\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\204\002"              # Call site table length
	.long	.Ltmp475-.Lfunc_begin1  # >> Call Site 1 <<
	.long	.Ltmp476-.Ltmp475       #   Call between .Ltmp475 and .Ltmp476
	.long	.Ltmp477-.Lfunc_begin1  #     jumps to .Ltmp477
	.byte	0                       #   On action: cleanup
	.long	.Ltmp478-.Lfunc_begin1  # >> Call Site 2 <<
	.long	.Ltmp479-.Ltmp478       #   Call between .Ltmp478 and .Ltmp479
	.long	.Ltmp480-.Lfunc_begin1  #     jumps to .Ltmp480
	.byte	0                       #   On action: cleanup
	.long	.Ltmp481-.Lfunc_begin1  # >> Call Site 3 <<
	.long	.Ltmp482-.Ltmp481       #   Call between .Ltmp481 and .Ltmp482
	.long	.Ltmp483-.Lfunc_begin1  #     jumps to .Ltmp483
	.byte	0                       #   On action: cleanup
	.long	.Ltmp484-.Lfunc_begin1  # >> Call Site 4 <<
	.long	.Ltmp485-.Ltmp484       #   Call between .Ltmp484 and .Ltmp485
	.long	.Ltmp486-.Lfunc_begin1  #     jumps to .Ltmp486
	.byte	0                       #   On action: cleanup
	.long	.Ltmp487-.Lfunc_begin1  # >> Call Site 5 <<
	.long	.Ltmp488-.Ltmp487       #   Call between .Ltmp487 and .Ltmp488
	.long	.Ltmp489-.Lfunc_begin1  #     jumps to .Ltmp489
	.byte	0                       #   On action: cleanup
	.long	.Ltmp490-.Lfunc_begin1  # >> Call Site 6 <<
	.long	.Ltmp491-.Ltmp490       #   Call between .Ltmp490 and .Ltmp491
	.long	.Ltmp492-.Lfunc_begin1  #     jumps to .Ltmp492
	.byte	0                       #   On action: cleanup
	.long	.Ltmp493-.Lfunc_begin1  # >> Call Site 7 <<
	.long	.Ltmp494-.Ltmp493       #   Call between .Ltmp493 and .Ltmp494
	.long	.Ltmp527-.Lfunc_begin1  #     jumps to .Ltmp527
	.byte	1                       #   On action: 1
	.long	.Ltmp495-.Lfunc_begin1  # >> Call Site 8 <<
	.long	.Ltmp496-.Ltmp495       #   Call between .Ltmp495 and .Ltmp496
	.long	.Ltmp497-.Lfunc_begin1  #     jumps to .Ltmp497
	.byte	1                       #   On action: 1
	.long	.Ltmp501-.Lfunc_begin1  # >> Call Site 9 <<
	.long	.Ltmp502-.Ltmp501       #   Call between .Ltmp501 and .Ltmp502
	.long	.Ltmp527-.Lfunc_begin1  #     jumps to .Ltmp527
	.byte	1                       #   On action: 1
	.long	.Ltmp503-.Lfunc_begin1  # >> Call Site 10 <<
	.long	.Ltmp504-.Ltmp503       #   Call between .Ltmp503 and .Ltmp504
	.long	.Ltmp505-.Lfunc_begin1  #     jumps to .Ltmp505
	.byte	1                       #   On action: 1
	.long	.Ltmp509-.Lfunc_begin1  # >> Call Site 11 <<
	.long	.Ltmp510-.Ltmp509       #   Call between .Ltmp509 and .Ltmp510
	.long	.Ltmp527-.Lfunc_begin1  #     jumps to .Ltmp527
	.byte	1                       #   On action: 1
	.long	.Ltmp511-.Lfunc_begin1  # >> Call Site 12 <<
	.long	.Ltmp512-.Ltmp511       #   Call between .Ltmp511 and .Ltmp512
	.long	.Ltmp513-.Lfunc_begin1  #     jumps to .Ltmp513
	.byte	1                       #   On action: 1
	.long	.Ltmp517-.Lfunc_begin1  # >> Call Site 13 <<
	.long	.Ltmp518-.Ltmp517       #   Call between .Ltmp517 and .Ltmp518
	.long	.Ltmp527-.Lfunc_begin1  #     jumps to .Ltmp527
	.byte	1                       #   On action: 1
	.long	.Ltmp514-.Lfunc_begin1  # >> Call Site 14 <<
	.long	.Ltmp515-.Ltmp514       #   Call between .Ltmp514 and .Ltmp515
	.long	.Ltmp516-.Lfunc_begin1  #     jumps to .Ltmp516
	.byte	1                       #   On action: 1
	.long	.Ltmp506-.Lfunc_begin1  # >> Call Site 15 <<
	.long	.Ltmp507-.Ltmp506       #   Call between .Ltmp506 and .Ltmp507
	.long	.Ltmp508-.Lfunc_begin1  #     jumps to .Ltmp508
	.byte	1                       #   On action: 1
	.long	.Ltmp498-.Lfunc_begin1  # >> Call Site 16 <<
	.long	.Ltmp499-.Ltmp498       #   Call between .Ltmp498 and .Ltmp499
	.long	.Ltmp500-.Lfunc_begin1  #     jumps to .Ltmp500
	.byte	1                       #   On action: 1
	.long	.Ltmp519-.Lfunc_begin1  # >> Call Site 17 <<
	.long	.Ltmp520-.Ltmp519       #   Call between .Ltmp519 and .Ltmp520
	.long	.Ltmp521-.Lfunc_begin1  #     jumps to .Ltmp521
	.byte	1                       #   On action: 1
	.long	.Ltmp525-.Lfunc_begin1  # >> Call Site 18 <<
	.long	.Ltmp526-.Ltmp525       #   Call between .Ltmp525 and .Ltmp526
	.long	.Ltmp527-.Lfunc_begin1  #     jumps to .Ltmp527
	.byte	1                       #   On action: 1
	.long	.Ltmp526-.Lfunc_begin1  # >> Call Site 19 <<
	.long	.Ltmp522-.Ltmp526       #   Call between .Ltmp526 and .Ltmp522
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp522-.Lfunc_begin1  # >> Call Site 20 <<
	.long	.Ltmp523-.Ltmp522       #   Call between .Ltmp522 and .Ltmp523
	.long	.Ltmp524-.Lfunc_begin1  #     jumps to .Ltmp524
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end4:
	.size	__clang_call_terminate, .Lfunc_end4-__clang_call_terminate

	.section	.text._ZNK12CArchivePath12GetFinalPathEv,"axG",@progbits,_ZNK12CArchivePath12GetFinalPathEv,comdat
	.weak	_ZNK12CArchivePath12GetFinalPathEv
	.p2align	4, 0x90
	.type	_ZNK12CArchivePath12GetFinalPathEv,@function
_ZNK12CArchivePath12GetFinalPathEv:     # @_ZNK12CArchivePath12GetFinalPathEv
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi328:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi329:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi330:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi331:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi332:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi333:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi334:
	.cfi_def_cfa_offset 64
.Lcfi335:
	.cfi_offset %rbx, -56
.Lcfi336:
	.cfi_offset %r12, -48
.Lcfi337:
	.cfi_offset %r13, -40
.Lcfi338:
	.cfi_offset %r14, -32
.Lcfi339:
	.cfi_offset %r15, -24
.Lcfi340:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	leaq	16(%r12), %rsi
	leaq	32(%r12), %rdx
	callq	_ZplIwE11CStringBaseIT_ERKS2_S4_
	movl	56(%r12), %ebx
	testl	%ebx, %ebx
	je	.LBB5_15
# BB#1:
.Ltmp528:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp529:
# BB#2:                                 # %._crit_edge16.i.i.i
	movq	$46, (%r15)
.Ltmp531:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r13
.Ltmp532:
# BB#3:                                 # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movl	$46, (%r13)
	movl	$4, %eax
	.p2align	4, 0x90
.LBB5_4:                                # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rax), %ecx
	movl	%ecx, (%r13,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB5_4
# BB#5:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
	testl	%ebx, %ebx
	jle	.LBB5_6
# BB#7:
	cmpl	$3, %ebx
	movl	$4, %eax
	cmovgl	%ebx, %eax
	addl	$3, %eax
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp534:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp535:
# BB#8:                                 # %.lr.ph.i.i
	movl	(%r13), %eax
	movl	%eax, (%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, 4(%rbx)
	movq	%rbx, %r13
	jmp	.LBB5_9
.LBB5_6:
	movq	%r13, %rbx
.LBB5_9:                                # %.noexc.i
	movq	48(%r12), %rcx
	leaq	4(%rbx), %rsi
	.p2align	4, 0x90
.LBB5_10:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %eax
	addq	$4, %rcx
	movl	%eax, (%rsi)
	addq	$4, %rsi
	testl	%eax, %eax
	jne	.LBB5_10
# BB#11:
	movl	56(%r12), %ebp
	incl	%ebp
.Ltmp537:
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp538:
# BB#12:                                # %.noexc5
	movslq	8(%r14), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%r14), %rcx
	.p2align	4, 0x90
.LBB5_13:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %edx
	addq	$4, %rbx
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB5_13
# BB#14:                                # %_ZN11CStringBaseIwED2Ev.exit7
	addl	%eax, %ebp
	movl	%ebp, 8(%r14)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB5_15:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_24:                               # %_ZN11CStringBaseIwED2Ev.exit.i
.Ltmp536:
	jmp	.LBB5_19
.LBB5_18:
.Ltmp539:
.LBB5_19:                               # %_ZN11CStringBaseIwED2Ev.exit9
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	jmp	.LBB5_20
.LBB5_17:
.Ltmp533:
	movq	%rax, %rbx
.LBB5_20:                               # %_ZN11CStringBaseIwED2Ev.exit9
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB5_21
.LBB5_16:
.Ltmp530:
	movq	%rax, %rbx
.LBB5_21:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB5_23
# BB#22:
	callq	_ZdaPv
.LBB5_23:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZNK12CArchivePath12GetFinalPathEv, .Lfunc_end5-_ZNK12CArchivePath12GetFinalPathEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\320"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp528-.Lfunc_begin2  #   Call between .Lfunc_begin2 and .Ltmp528
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp528-.Lfunc_begin2  # >> Call Site 2 <<
	.long	.Ltmp529-.Ltmp528       #   Call between .Ltmp528 and .Ltmp529
	.long	.Ltmp530-.Lfunc_begin2  #     jumps to .Ltmp530
	.byte	0                       #   On action: cleanup
	.long	.Ltmp531-.Lfunc_begin2  # >> Call Site 3 <<
	.long	.Ltmp532-.Ltmp531       #   Call between .Ltmp531 and .Ltmp532
	.long	.Ltmp533-.Lfunc_begin2  #     jumps to .Ltmp533
	.byte	0                       #   On action: cleanup
	.long	.Ltmp534-.Lfunc_begin2  # >> Call Site 4 <<
	.long	.Ltmp535-.Ltmp534       #   Call between .Ltmp534 and .Ltmp535
	.long	.Ltmp536-.Lfunc_begin2  #     jumps to .Ltmp536
	.byte	0                       #   On action: cleanup
	.long	.Ltmp537-.Lfunc_begin2  # >> Call Site 5 <<
	.long	.Ltmp538-.Ltmp537       #   Call between .Ltmp537 and .Ltmp538
	.long	.Ltmp539-.Lfunc_begin2  #     jumps to .Ltmp539
	.byte	0                       #   On action: cleanup
	.long	.Ltmp538-.Lfunc_begin2  # >> Call Site 6 <<
	.long	.Lfunc_end5-.Ltmp538    #   Call between .Ltmp538 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN22CUpdateCallbackConsoleD2Ev,"axG",@progbits,_ZN22CUpdateCallbackConsoleD2Ev,comdat
	.weak	_ZN22CUpdateCallbackConsoleD2Ev
	.p2align	4, 0x90
	.type	_ZN22CUpdateCallbackConsoleD2Ev,@function
_ZN22CUpdateCallbackConsoleD2Ev:        # @_ZN22CUpdateCallbackConsoleD2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r15
.Lcfi341:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi342:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi343:
	.cfi_def_cfa_offset 32
.Lcfi344:
	.cfi_offset %rbx, -32
.Lcfi345:
	.cfi_offset %r14, -24
.Lcfi346:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	$_ZTV22CUpdateCallbackConsole+16, (%r15)
.Ltmp540:
	callq	_ZN22CUpdateCallbackConsole8FinilizeEv
.Ltmp541:
# BB#1:
	leaq	200(%r15), %rdi
.Ltmp545:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp546:
# BB#2:
	leaq	168(%r15), %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 168(%r15)
.Ltmp556:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp557:
# BB#3:
.Ltmp562:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp563:
# BB#4:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	leaq	136(%r15), %rdi
.Ltmp567:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp568:
# BB#5:
	leaq	104(%r15), %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 104(%r15)
.Ltmp579:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp580:
# BB#6:
.Ltmp585:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp586:
# BB#7:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit8
	movq	80(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB6_13
# BB#8:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZdaPv                  # TAILCALL
.LBB6_13:                               # %_ZN11CStringBaseIwED2Ev.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB6_22:
.Ltmp587:
	movq	%rax, %r14
	jmp	.LBB6_26
.LBB6_11:
.Ltmp581:
	movq	%rax, %r14
.Ltmp582:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp583:
	jmp	.LBB6_26
.LBB6_12:
.Ltmp584:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_23:
.Ltmp569:
	movq	%rax, %r14
	jmp	.LBB6_24
.LBB6_15:
.Ltmp564:
	movq	%rax, %r14
	jmp	.LBB6_19
.LBB6_9:
.Ltmp558:
	movq	%rax, %r14
.Ltmp559:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp560:
	jmp	.LBB6_19
.LBB6_10:
.Ltmp561:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_16:
.Ltmp547:
	movq	%rax, %r14
	jmp	.LBB6_17
.LBB6_14:
.Ltmp542:
	movq	%rax, %r14
	leaq	200(%r15), %rdi
.Ltmp543:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp544:
.LBB6_17:
	leaq	168(%r15), %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 168(%r15)
.Ltmp548:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp549:
# BB#18:
.Ltmp554:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp555:
.LBB6_19:                               # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit11
	leaq	136(%r15), %rdi
.Ltmp565:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp566:
.LBB6_24:
	leaq	104(%r15), %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 104(%r15)
.Ltmp570:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp571:
# BB#25:
.Ltmp576:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp577:
.LBB6_26:                               # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit14
	movq	80(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB6_28
# BB#27:
	callq	_ZdaPv
.LBB6_28:                               # %_ZN11CStringBaseIwED2Ev.exit15
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_20:
.Ltmp550:
	movq	%rax, %r14
.Ltmp551:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp552:
	jmp	.LBB6_32
.LBB6_21:
.Ltmp553:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_29:
.Ltmp572:
	movq	%rax, %r14
.Ltmp573:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp574:
	jmp	.LBB6_32
.LBB6_30:
.Ltmp575:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_31:
.Ltmp578:
	movq	%rax, %r14
.LBB6_32:                               # %.body9
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN22CUpdateCallbackConsoleD2Ev, .Lfunc_end6-_ZN22CUpdateCallbackConsoleD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\346\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\335\001"              # Call site table length
	.long	.Ltmp540-.Lfunc_begin3  # >> Call Site 1 <<
	.long	.Ltmp541-.Ltmp540       #   Call between .Ltmp540 and .Ltmp541
	.long	.Ltmp542-.Lfunc_begin3  #     jumps to .Ltmp542
	.byte	0                       #   On action: cleanup
	.long	.Ltmp545-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Ltmp546-.Ltmp545       #   Call between .Ltmp545 and .Ltmp546
	.long	.Ltmp547-.Lfunc_begin3  #     jumps to .Ltmp547
	.byte	0                       #   On action: cleanup
	.long	.Ltmp556-.Lfunc_begin3  # >> Call Site 3 <<
	.long	.Ltmp557-.Ltmp556       #   Call between .Ltmp556 and .Ltmp557
	.long	.Ltmp558-.Lfunc_begin3  #     jumps to .Ltmp558
	.byte	0                       #   On action: cleanup
	.long	.Ltmp562-.Lfunc_begin3  # >> Call Site 4 <<
	.long	.Ltmp563-.Ltmp562       #   Call between .Ltmp562 and .Ltmp563
	.long	.Ltmp564-.Lfunc_begin3  #     jumps to .Ltmp564
	.byte	0                       #   On action: cleanup
	.long	.Ltmp567-.Lfunc_begin3  # >> Call Site 5 <<
	.long	.Ltmp568-.Ltmp567       #   Call between .Ltmp567 and .Ltmp568
	.long	.Ltmp569-.Lfunc_begin3  #     jumps to .Ltmp569
	.byte	0                       #   On action: cleanup
	.long	.Ltmp579-.Lfunc_begin3  # >> Call Site 6 <<
	.long	.Ltmp580-.Ltmp579       #   Call between .Ltmp579 and .Ltmp580
	.long	.Ltmp581-.Lfunc_begin3  #     jumps to .Ltmp581
	.byte	0                       #   On action: cleanup
	.long	.Ltmp585-.Lfunc_begin3  # >> Call Site 7 <<
	.long	.Ltmp586-.Ltmp585       #   Call between .Ltmp585 and .Ltmp586
	.long	.Ltmp587-.Lfunc_begin3  #     jumps to .Ltmp587
	.byte	0                       #   On action: cleanup
	.long	.Ltmp582-.Lfunc_begin3  # >> Call Site 8 <<
	.long	.Ltmp583-.Ltmp582       #   Call between .Ltmp582 and .Ltmp583
	.long	.Ltmp584-.Lfunc_begin3  #     jumps to .Ltmp584
	.byte	1                       #   On action: 1
	.long	.Ltmp559-.Lfunc_begin3  # >> Call Site 9 <<
	.long	.Ltmp560-.Ltmp559       #   Call between .Ltmp559 and .Ltmp560
	.long	.Ltmp561-.Lfunc_begin3  #     jumps to .Ltmp561
	.byte	1                       #   On action: 1
	.long	.Ltmp543-.Lfunc_begin3  # >> Call Site 10 <<
	.long	.Ltmp544-.Ltmp543       #   Call between .Ltmp543 and .Ltmp544
	.long	.Ltmp578-.Lfunc_begin3  #     jumps to .Ltmp578
	.byte	1                       #   On action: 1
	.long	.Ltmp548-.Lfunc_begin3  # >> Call Site 11 <<
	.long	.Ltmp549-.Ltmp548       #   Call between .Ltmp548 and .Ltmp549
	.long	.Ltmp550-.Lfunc_begin3  #     jumps to .Ltmp550
	.byte	1                       #   On action: 1
	.long	.Ltmp554-.Lfunc_begin3  # >> Call Site 12 <<
	.long	.Ltmp566-.Ltmp554       #   Call between .Ltmp554 and .Ltmp566
	.long	.Ltmp578-.Lfunc_begin3  #     jumps to .Ltmp578
	.byte	1                       #   On action: 1
	.long	.Ltmp570-.Lfunc_begin3  # >> Call Site 13 <<
	.long	.Ltmp571-.Ltmp570       #   Call between .Ltmp570 and .Ltmp571
	.long	.Ltmp572-.Lfunc_begin3  #     jumps to .Ltmp572
	.byte	1                       #   On action: 1
	.long	.Ltmp576-.Lfunc_begin3  # >> Call Site 14 <<
	.long	.Ltmp577-.Ltmp576       #   Call between .Ltmp576 and .Ltmp577
	.long	.Ltmp578-.Lfunc_begin3  #     jumps to .Ltmp578
	.byte	1                       #   On action: 1
	.long	.Ltmp577-.Lfunc_begin3  # >> Call Site 15 <<
	.long	.Ltmp551-.Ltmp577       #   Call between .Ltmp577 and .Ltmp551
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp551-.Lfunc_begin3  # >> Call Site 16 <<
	.long	.Ltmp552-.Ltmp551       #   Call between .Ltmp551 and .Ltmp552
	.long	.Ltmp553-.Lfunc_begin3  #     jumps to .Ltmp553
	.byte	1                       #   On action: 1
	.long	.Ltmp573-.Lfunc_begin3  # >> Call Site 17 <<
	.long	.Ltmp574-.Ltmp573       #   Call between .Ltmp573 and .Ltmp574
	.long	.Ltmp575-.Lfunc_begin3  #     jumps to .Ltmp575
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN26CArchiveCommandLineOptionsD2Ev,"axG",@progbits,_ZN26CArchiveCommandLineOptionsD2Ev,comdat
	.weak	_ZN26CArchiveCommandLineOptionsD2Ev
	.p2align	4, 0x90
	.type	_ZN26CArchiveCommandLineOptionsD2Ev,@function
_ZN26CArchiveCommandLineOptionsD2Ev:    # @_ZN26CArchiveCommandLineOptionsD2Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r15
.Lcfi347:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi348:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi349:
	.cfi_def_cfa_offset 32
.Lcfi350:
	.cfi_offset %rbx, -32
.Lcfi351:
	.cfi_offset %r14, -24
.Lcfi352:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	576(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB7_2
# BB#1:
	callq	_ZdaPv
.LBB7_2:                                # %_ZN11CStringBaseIwED2Ev.exit
	movq	544(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB7_4
# BB#3:
	callq	_ZdaPv
.LBB7_4:                                # %_ZN11CStringBaseIwED2Ev.exit5
	leaq	224(%r15), %rdi
.Ltmp588:
	callq	_ZN14CUpdateOptionsD2Ev
.Ltmp589:
# BB#5:
	leaq	192(%r15), %rbx
	movq	$_ZTV13CObjectVectorI9CPropertyE+16, 192(%r15)
.Ltmp599:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp600:
# BB#6:
.Ltmp605:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp606:
# BB#7:                                 # %_ZN13CObjectVectorI9CPropertyED2Ev.exit
	leaq	160(%r15), %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 160(%r15)
.Ltmp616:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp617:
# BB#8:
.Ltmp622:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp623:
# BB#9:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%r15, %rbx
	subq	$-128, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 128(%r15)
.Ltmp633:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp634:
# BB#10:
.Ltmp639:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp640:
# BB#11:                                # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit10
	movq	104(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB7_13
# BB#12:
	callq	_ZdaPv
.LBB7_13:                               # %_ZN11CStringBaseIwED2Ev.exit11
	movq	80(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB7_15
# BB#14:
	callq	_ZdaPv
.LBB7_15:                               # %_ZN11CStringBaseIwED2Ev.exit12
	movq	56(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB7_17
# BB#16:
	callq	_ZdaPv
.LBB7_17:                               # %_ZN11CStringBaseIwED2Ev.exit13
	movq	$_ZTV13CObjectVectorIN9NWildcard5CPairEE+16, 16(%r15)
	addq	$16, %r15
.Ltmp651:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp652:
# BB#18:                                # %_ZN9NWildcard7CCensorD2Ev.exit
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB7_25:
.Ltmp653:
	movq	%rax, %r14
.Ltmp654:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp655:
	jmp	.LBB7_26
.LBB7_27:
.Ltmp656:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_48:
.Ltmp641:
	movq	%rax, %r14
	jmp	.LBB7_34
.LBB7_23:
.Ltmp635:
	movq	%rax, %r14
.Ltmp636:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp637:
	jmp	.LBB7_34
.LBB7_24:
.Ltmp638:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_45:
.Ltmp624:
	movq	%rax, %r14
	jmp	.LBB7_32
.LBB7_21:
.Ltmp618:
	movq	%rax, %r14
.Ltmp619:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp620:
	jmp	.LBB7_32
.LBB7_22:
.Ltmp621:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_44:
.Ltmp607:
	movq	%rax, %r14
	jmp	.LBB7_30
.LBB7_19:
.Ltmp601:
	movq	%rax, %r14
.Ltmp602:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp603:
	jmp	.LBB7_30
.LBB7_20:
.Ltmp604:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_28:
.Ltmp590:
	movq	%rax, %r14
	leaq	192(%r15), %rbx
	movq	$_ZTV13CObjectVectorI9CPropertyE+16, 192(%r15)
.Ltmp591:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp592:
# BB#29:
.Ltmp597:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp598:
.LBB7_30:                               # %_ZN13CObjectVectorI9CPropertyED2Ev.exit16
	leaq	160(%r15), %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 160(%r15)
.Ltmp608:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp609:
# BB#31:
.Ltmp614:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp615:
.LBB7_32:                               # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit19
	movq	%r15, %rbx
	subq	$-128, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 128(%r15)
.Ltmp625:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp626:
# BB#33:
.Ltmp631:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp632:
.LBB7_34:                               # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit22
	movq	104(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB7_36
# BB#35:
	callq	_ZdaPv
.LBB7_36:                               # %_ZN11CStringBaseIwED2Ev.exit23
	movq	80(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB7_38
# BB#37:
	callq	_ZdaPv
.LBB7_38:                               # %_ZN11CStringBaseIwED2Ev.exit24
	movq	56(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB7_40
# BB#39:
	callq	_ZdaPv
.LBB7_40:                               # %_ZN11CStringBaseIwED2Ev.exit25
	movq	$_ZTV13CObjectVectorIN9NWildcard5CPairEE+16, 16(%r15)
	addq	$16, %r15
.Ltmp642:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp643:
# BB#41:                                # %_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev.exit.i
.Ltmp648:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp649:
.LBB7_26:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB7_42:
.Ltmp593:
	movq	%rax, %r14
.Ltmp594:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp595:
	jmp	.LBB7_54
.LBB7_43:
.Ltmp596:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_46:
.Ltmp610:
	movq	%rax, %r14
.Ltmp611:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp612:
	jmp	.LBB7_54
.LBB7_47:
.Ltmp613:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_49:
.Ltmp627:
	movq	%rax, %r14
.Ltmp628:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp629:
	jmp	.LBB7_54
.LBB7_50:
.Ltmp630:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_51:
.Ltmp644:
	movq	%rax, %r14
.Ltmp645:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp646:
	jmp	.LBB7_54
.LBB7_52:
.Ltmp647:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_53:
.Ltmp650:
	movq	%rax, %r14
.LBB7_54:                               # %.body14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN26CArchiveCommandLineOptionsD2Ev, .Lfunc_end7-_ZN26CArchiveCommandLineOptionsD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\333\202"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\322\002"              # Call site table length
	.long	.Ltmp588-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp589-.Ltmp588       #   Call between .Ltmp588 and .Ltmp589
	.long	.Ltmp590-.Lfunc_begin4  #     jumps to .Ltmp590
	.byte	0                       #   On action: cleanup
	.long	.Ltmp599-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp600-.Ltmp599       #   Call between .Ltmp599 and .Ltmp600
	.long	.Ltmp601-.Lfunc_begin4  #     jumps to .Ltmp601
	.byte	0                       #   On action: cleanup
	.long	.Ltmp605-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp606-.Ltmp605       #   Call between .Ltmp605 and .Ltmp606
	.long	.Ltmp607-.Lfunc_begin4  #     jumps to .Ltmp607
	.byte	0                       #   On action: cleanup
	.long	.Ltmp616-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp617-.Ltmp616       #   Call between .Ltmp616 and .Ltmp617
	.long	.Ltmp618-.Lfunc_begin4  #     jumps to .Ltmp618
	.byte	0                       #   On action: cleanup
	.long	.Ltmp622-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Ltmp623-.Ltmp622       #   Call between .Ltmp622 and .Ltmp623
	.long	.Ltmp624-.Lfunc_begin4  #     jumps to .Ltmp624
	.byte	0                       #   On action: cleanup
	.long	.Ltmp633-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Ltmp634-.Ltmp633       #   Call between .Ltmp633 and .Ltmp634
	.long	.Ltmp635-.Lfunc_begin4  #     jumps to .Ltmp635
	.byte	0                       #   On action: cleanup
	.long	.Ltmp639-.Lfunc_begin4  # >> Call Site 7 <<
	.long	.Ltmp640-.Ltmp639       #   Call between .Ltmp639 and .Ltmp640
	.long	.Ltmp641-.Lfunc_begin4  #     jumps to .Ltmp641
	.byte	0                       #   On action: cleanup
	.long	.Ltmp651-.Lfunc_begin4  # >> Call Site 8 <<
	.long	.Ltmp652-.Ltmp651       #   Call between .Ltmp651 and .Ltmp652
	.long	.Ltmp653-.Lfunc_begin4  #     jumps to .Ltmp653
	.byte	0                       #   On action: cleanup
	.long	.Ltmp652-.Lfunc_begin4  # >> Call Site 9 <<
	.long	.Ltmp654-.Ltmp652       #   Call between .Ltmp652 and .Ltmp654
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp654-.Lfunc_begin4  # >> Call Site 10 <<
	.long	.Ltmp655-.Ltmp654       #   Call between .Ltmp654 and .Ltmp655
	.long	.Ltmp656-.Lfunc_begin4  #     jumps to .Ltmp656
	.byte	1                       #   On action: 1
	.long	.Ltmp636-.Lfunc_begin4  # >> Call Site 11 <<
	.long	.Ltmp637-.Ltmp636       #   Call between .Ltmp636 and .Ltmp637
	.long	.Ltmp638-.Lfunc_begin4  #     jumps to .Ltmp638
	.byte	1                       #   On action: 1
	.long	.Ltmp619-.Lfunc_begin4  # >> Call Site 12 <<
	.long	.Ltmp620-.Ltmp619       #   Call between .Ltmp619 and .Ltmp620
	.long	.Ltmp621-.Lfunc_begin4  #     jumps to .Ltmp621
	.byte	1                       #   On action: 1
	.long	.Ltmp602-.Lfunc_begin4  # >> Call Site 13 <<
	.long	.Ltmp603-.Ltmp602       #   Call between .Ltmp602 and .Ltmp603
	.long	.Ltmp604-.Lfunc_begin4  #     jumps to .Ltmp604
	.byte	1                       #   On action: 1
	.long	.Ltmp591-.Lfunc_begin4  # >> Call Site 14 <<
	.long	.Ltmp592-.Ltmp591       #   Call between .Ltmp591 and .Ltmp592
	.long	.Ltmp593-.Lfunc_begin4  #     jumps to .Ltmp593
	.byte	1                       #   On action: 1
	.long	.Ltmp597-.Lfunc_begin4  # >> Call Site 15 <<
	.long	.Ltmp598-.Ltmp597       #   Call between .Ltmp597 and .Ltmp598
	.long	.Ltmp650-.Lfunc_begin4  #     jumps to .Ltmp650
	.byte	1                       #   On action: 1
	.long	.Ltmp608-.Lfunc_begin4  # >> Call Site 16 <<
	.long	.Ltmp609-.Ltmp608       #   Call between .Ltmp608 and .Ltmp609
	.long	.Ltmp610-.Lfunc_begin4  #     jumps to .Ltmp610
	.byte	1                       #   On action: 1
	.long	.Ltmp614-.Lfunc_begin4  # >> Call Site 17 <<
	.long	.Ltmp615-.Ltmp614       #   Call between .Ltmp614 and .Ltmp615
	.long	.Ltmp650-.Lfunc_begin4  #     jumps to .Ltmp650
	.byte	1                       #   On action: 1
	.long	.Ltmp625-.Lfunc_begin4  # >> Call Site 18 <<
	.long	.Ltmp626-.Ltmp625       #   Call between .Ltmp625 and .Ltmp626
	.long	.Ltmp627-.Lfunc_begin4  #     jumps to .Ltmp627
	.byte	1                       #   On action: 1
	.long	.Ltmp631-.Lfunc_begin4  # >> Call Site 19 <<
	.long	.Ltmp632-.Ltmp631       #   Call between .Ltmp631 and .Ltmp632
	.long	.Ltmp650-.Lfunc_begin4  #     jumps to .Ltmp650
	.byte	1                       #   On action: 1
	.long	.Ltmp642-.Lfunc_begin4  # >> Call Site 20 <<
	.long	.Ltmp643-.Ltmp642       #   Call between .Ltmp642 and .Ltmp643
	.long	.Ltmp644-.Lfunc_begin4  #     jumps to .Ltmp644
	.byte	1                       #   On action: 1
	.long	.Ltmp648-.Lfunc_begin4  # >> Call Site 21 <<
	.long	.Ltmp649-.Ltmp648       #   Call between .Ltmp648 and .Ltmp649
	.long	.Ltmp650-.Lfunc_begin4  #     jumps to .Ltmp650
	.byte	1                       #   On action: 1
	.long	.Ltmp649-.Lfunc_begin4  # >> Call Site 22 <<
	.long	.Ltmp594-.Ltmp649       #   Call between .Ltmp649 and .Ltmp594
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp594-.Lfunc_begin4  # >> Call Site 23 <<
	.long	.Ltmp595-.Ltmp594       #   Call between .Ltmp594 and .Ltmp595
	.long	.Ltmp596-.Lfunc_begin4  #     jumps to .Ltmp596
	.byte	1                       #   On action: 1
	.long	.Ltmp611-.Lfunc_begin4  # >> Call Site 24 <<
	.long	.Ltmp612-.Ltmp611       #   Call between .Ltmp611 and .Ltmp612
	.long	.Ltmp613-.Lfunc_begin4  #     jumps to .Ltmp613
	.byte	1                       #   On action: 1
	.long	.Ltmp628-.Lfunc_begin4  # >> Call Site 25 <<
	.long	.Ltmp629-.Ltmp628       #   Call between .Ltmp628 and .Ltmp629
	.long	.Ltmp630-.Lfunc_begin4  #     jumps to .Ltmp630
	.byte	1                       #   On action: 1
	.long	.Ltmp645-.Lfunc_begin4  # >> Call Site 26 <<
	.long	.Ltmp646-.Ltmp645       #   Call between .Ltmp645 and .Ltmp646
	.long	.Ltmp647-.Lfunc_begin4  #     jumps to .Ltmp647
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED2Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED2Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED2Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED2Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED2Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi353:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi354:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi355:
	.cfi_def_cfa_offset 32
.Lcfi356:
	.cfi_offset %rbx, -24
.Lcfi357:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp657:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp658:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB8_2:
.Ltmp659:
	movq	%rax, %r14
.Ltmp660:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp661:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_4:
.Ltmp662:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED2Ev, .Lfunc_end8-_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp657-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp658-.Ltmp657       #   Call between .Ltmp657 and .Ltmp658
	.long	.Ltmp659-.Lfunc_begin5  #     jumps to .Ltmp659
	.byte	0                       #   On action: cleanup
	.long	.Ltmp658-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp660-.Ltmp658       #   Call between .Ltmp658 and .Ltmp660
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp660-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp661-.Ltmp660       #   Call between .Ltmp660 and .Ltmp661
	.long	.Ltmp662-.Lfunc_begin5  #     jumps to .Ltmp662
	.byte	1                       #   On action: 1
	.long	.Ltmp661-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Lfunc_end8-.Ltmp661    #   Call between .Ltmp661 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_0:
	.zero	16
	.section	.text._ZN14CUpdateOptionsC2Ev,"axG",@progbits,_ZN14CUpdateOptionsC2Ev,comdat
	.weak	_ZN14CUpdateOptionsC2Ev
	.p2align	4, 0x90
	.type	_ZN14CUpdateOptionsC2Ev,@function
_ZN14CUpdateOptionsC2Ev:                # @_ZN14CUpdateOptionsC2Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r15
.Lcfi358:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi359:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi360:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi361:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi362:
	.cfi_def_cfa_offset 48
.Lcfi363:
	.cfi_offset %rbx, -48
.Lcfi364:
	.cfi_offset %r12, -40
.Lcfi365:
	.cfi_offset %r13, -32
.Lcfi366:
	.cfi_offset %r14, -24
.Lcfi367:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$-1, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movq	$8, 32(%rbx)
	movq	$_ZTV13CObjectVectorI9CPropertyE+16, 8(%rbx)
	movups	%xmm0, 48(%rbx)
	movq	$8, 64(%rbx)
	movq	$_ZTV13CObjectVectorI21CUpdateArchiveCommandE+16, 40(%rbx)
	movb	$1, 72(%rbx)
	leaq	80(%rbx), %r14
.Ltmp663:
	movq	%r14, %rdi
	callq	_ZN12CArchivePathC2Ev
.Ltmp664:
# BB#1:
	movb	$0, 200(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 208(%rbx)
.Ltmp666:
	movl	$16, %edi
	callq	_Znam
.Ltmp667:
# BB#2:
	movq	%rax, 208(%rbx)
	movl	$0, (%rax)
	movl	$4, 220(%rbx)
	movb	$0, 224(%rbx)
	movb	$0, 225(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 232(%rbx)
.Ltmp669:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r12
.Ltmp670:
# BB#3:
	movq	%r12, 232(%rbx)
	movl	$0, (%r12)
	movl	$4, 244(%rbx)
	movb	$0, 248(%rbx)
	movb	$0, 249(%rbx)
	movb	$0, 250(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 256(%rbx)
.Ltmp672:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
.Ltmp673:
# BB#4:
	leaq	232(%rbx), %r12
	movq	%r13, 256(%rbx)
	movl	$0, (%r13)
	movl	$4, 268(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 272(%rbx)
.Ltmp675:
	movl	$16, %edi
	callq	_Znam
.Ltmp676:
# BB#5:
	movq	%rax, 272(%rbx)
	movl	$0, (%rax)
	movl	$4, 284(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 296(%rbx)
	movq	$8, 312(%rbx)
	movq	$_ZTV13CRecordVectorIyE+16, 288(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB9_10:                               # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp677:
	movq	%rax, %r15
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB9_11
	jmp	.LBB9_12
.LBB9_9:                                # %_ZN11CStringBaseIwED2Ev.exit.thread
.Ltmp674:
	movq	%rax, %r15
.LBB9_11:
	movq	%r12, %rdi
	callq	_ZdaPv
	jmp	.LBB9_12
.LBB9_8:
.Ltmp671:
	movq	%rax, %r15
.LBB9_12:                               # %_ZN11CStringBaseIwED2Ev.exit10
	leaq	208(%rbx), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB9_14
# BB#13:
	callq	_ZdaPv
	jmp	.LBB9_14
.LBB9_7:
.Ltmp668:
	movq	%rax, %r15
.LBB9_14:                               # %_ZN11CStringBaseIwED2Ev.exit11
	movq	184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_16
# BB#15:
	callq	_ZdaPv
.LBB9_16:                               # %_ZN11CStringBaseIwED2Ev.exit.i
	movq	168(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_18
# BB#17:
	callq	_ZdaPv
.LBB9_18:                               # %_ZN11CStringBaseIwED2Ev.exit1.i
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_20
# BB#19:
	callq	_ZdaPv
.LBB9_20:                               # %_ZN11CStringBaseIwED2Ev.exit2.i
	movq	128(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_22
# BB#21:
	callq	_ZdaPv
.LBB9_22:                               # %_ZN11CStringBaseIwED2Ev.exit3.i
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_24
# BB#23:
	callq	_ZdaPv
.LBB9_24:                               # %_ZN11CStringBaseIwED2Ev.exit4.i
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_26
# BB#25:
	callq	_ZdaPv
.LBB9_26:                               # %_ZN11CStringBaseIwED2Ev.exit5.i
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB9_28
# BB#27:
	callq	_ZdaPv
	jmp	.LBB9_28
.LBB9_6:
.Ltmp665:
	movq	%rax, %r15
.LBB9_28:                               # %_ZN12CArchivePathD2Ev.exit
	leaq	40(%rbx), %r12
	movq	$_ZTV13CObjectVectorI21CUpdateArchiveCommandE+16, (%r12)
.Ltmp678:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp679:
# BB#29:
.Ltmp684:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp685:
# BB#30:                                # %_ZN13CObjectVectorI21CUpdateArchiveCommandED2Ev.exit
	movq	$_ZTV13CObjectVectorI9CPropertyE+16, 8(%rbx)
	addq	$8, %rbx
.Ltmp686:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp687:
# BB#31:                                # %_ZN13CObjectVectorI9CPropertyED2Ev.exit.i
.Ltmp692:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp693:
# BB#32:                                # %_ZN22CCompressionMethodModeD2Ev.exit
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB9_35:
.Ltmp688:
	movq	%rax, %r14
.Ltmp689:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp690:
	jmp	.LBB9_38
.LBB9_36:
.Ltmp691:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_33:
.Ltmp680:
	movq	%rax, %r14
.Ltmp681:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp682:
	jmp	.LBB9_38
.LBB9_34:
.Ltmp683:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_37:
.Ltmp694:
	movq	%rax, %r14
.LBB9_38:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN14CUpdateOptionsC2Ev, .Lfunc_end9-_ZN14CUpdateOptionsC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\245\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Ltmp663-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp664-.Ltmp663       #   Call between .Ltmp663 and .Ltmp664
	.long	.Ltmp665-.Lfunc_begin6  #     jumps to .Ltmp665
	.byte	0                       #   On action: cleanup
	.long	.Ltmp666-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp667-.Ltmp666       #   Call between .Ltmp666 and .Ltmp667
	.long	.Ltmp668-.Lfunc_begin6  #     jumps to .Ltmp668
	.byte	0                       #   On action: cleanup
	.long	.Ltmp669-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp670-.Ltmp669       #   Call between .Ltmp669 and .Ltmp670
	.long	.Ltmp671-.Lfunc_begin6  #     jumps to .Ltmp671
	.byte	0                       #   On action: cleanup
	.long	.Ltmp672-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp673-.Ltmp672       #   Call between .Ltmp672 and .Ltmp673
	.long	.Ltmp674-.Lfunc_begin6  #     jumps to .Ltmp674
	.byte	0                       #   On action: cleanup
	.long	.Ltmp675-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp676-.Ltmp675       #   Call between .Ltmp675 and .Ltmp676
	.long	.Ltmp677-.Lfunc_begin6  #     jumps to .Ltmp677
	.byte	0                       #   On action: cleanup
	.long	.Ltmp678-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp679-.Ltmp678       #   Call between .Ltmp678 and .Ltmp679
	.long	.Ltmp680-.Lfunc_begin6  #     jumps to .Ltmp680
	.byte	1                       #   On action: 1
	.long	.Ltmp684-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp685-.Ltmp684       #   Call between .Ltmp684 and .Ltmp685
	.long	.Ltmp694-.Lfunc_begin6  #     jumps to .Ltmp694
	.byte	1                       #   On action: 1
	.long	.Ltmp686-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Ltmp687-.Ltmp686       #   Call between .Ltmp686 and .Ltmp687
	.long	.Ltmp688-.Lfunc_begin6  #     jumps to .Ltmp688
	.byte	1                       #   On action: 1
	.long	.Ltmp692-.Lfunc_begin6  # >> Call Site 9 <<
	.long	.Ltmp693-.Ltmp692       #   Call between .Ltmp692 and .Ltmp693
	.long	.Ltmp694-.Lfunc_begin6  #     jumps to .Ltmp694
	.byte	1                       #   On action: 1
	.long	.Ltmp693-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp689-.Ltmp693       #   Call between .Ltmp693 and .Ltmp689
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp689-.Lfunc_begin6  # >> Call Site 11 <<
	.long	.Ltmp690-.Ltmp689       #   Call between .Ltmp689 and .Ltmp690
	.long	.Ltmp691-.Lfunc_begin6  #     jumps to .Ltmp691
	.byte	1                       #   On action: 1
	.long	.Ltmp681-.Lfunc_begin6  # >> Call Site 12 <<
	.long	.Ltmp682-.Ltmp681       #   Call between .Ltmp681 and .Ltmp682
	.long	.Ltmp683-.Lfunc_begin6  #     jumps to .Ltmp683
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN14CUpdateOptionsD2Ev,"axG",@progbits,_ZN14CUpdateOptionsD2Ev,comdat
	.weak	_ZN14CUpdateOptionsD2Ev
	.p2align	4, 0x90
	.type	_ZN14CUpdateOptionsD2Ev,@function
_ZN14CUpdateOptionsD2Ev:                # @_ZN14CUpdateOptionsD2Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r15
.Lcfi368:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi369:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi370:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi371:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi372:
	.cfi_def_cfa_offset 48
.Lcfi373:
	.cfi_offset %rbx, -40
.Lcfi374:
	.cfi_offset %r12, -32
.Lcfi375:
	.cfi_offset %r14, -24
.Lcfi376:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	leaq	288(%r12), %rdi
.Ltmp695:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp696:
# BB#1:
	movq	272(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_3
# BB#2:
	callq	_ZdaPv
.LBB10_3:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_5
# BB#4:
	callq	_ZdaPv
.LBB10_5:                               # %_ZN11CStringBaseIwED2Ev.exit4
	movq	232(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_7
# BB#6:
	callq	_ZdaPv
.LBB10_7:                               # %_ZN11CStringBaseIwED2Ev.exit5
	movq	208(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_9
# BB#8:
	callq	_ZdaPv
.LBB10_9:                               # %_ZN11CStringBaseIwED2Ev.exit6
	movq	184(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_11
# BB#10:
	callq	_ZdaPv
.LBB10_11:                              # %_ZN11CStringBaseIwED2Ev.exit.i
	movq	168(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_13
# BB#12:
	callq	_ZdaPv
.LBB10_13:                              # %_ZN11CStringBaseIwED2Ev.exit1.i
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_15
# BB#14:
	callq	_ZdaPv
.LBB10_15:                              # %_ZN11CStringBaseIwED2Ev.exit2.i
	movq	128(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_17
# BB#16:
	callq	_ZdaPv
.LBB10_17:                              # %_ZN11CStringBaseIwED2Ev.exit3.i
	movq	112(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_19
# BB#18:
	callq	_ZdaPv
.LBB10_19:                              # %_ZN11CStringBaseIwED2Ev.exit4.i
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_21
# BB#20:
	callq	_ZdaPv
.LBB10_21:                              # %_ZN11CStringBaseIwED2Ev.exit5.i
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_23
# BB#22:
	callq	_ZdaPv
.LBB10_23:                              # %_ZN12CArchivePathD2Ev.exit
	leaq	40(%r12), %rbx
	movq	$_ZTV13CObjectVectorI21CUpdateArchiveCommandE+16, 40(%r12)
.Ltmp706:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp707:
# BB#24:
.Ltmp712:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp713:
# BB#25:                                # %_ZN13CObjectVectorI21CUpdateArchiveCommandED2Ev.exit
	movq	$_ZTV13CObjectVectorI9CPropertyE+16, 8(%r12)
	addq	$8, %r12
.Ltmp724:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp725:
# BB#26:                                # %_ZN22CCompressionMethodModeD2Ev.exit
	movq	%r12, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB10_29:
.Ltmp726:
	movq	%rax, %r14
.Ltmp727:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp728:
	jmp	.LBB10_30
.LBB10_31:
.Ltmp729:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB10_58:
.Ltmp714:
	movq	%rax, %r14
	jmp	.LBB10_56
.LBB10_27:
.Ltmp708:
	movq	%rax, %r14
.Ltmp709:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp710:
	jmp	.LBB10_56
.LBB10_28:
.Ltmp711:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB10_32:
.Ltmp697:
	movq	%rax, %r14
	movq	272(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_34
# BB#33:
	callq	_ZdaPv
.LBB10_34:                              # %_ZN11CStringBaseIwED2Ev.exit7
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_36
# BB#35:
	callq	_ZdaPv
.LBB10_36:                              # %_ZN11CStringBaseIwED2Ev.exit8
	movq	232(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_38
# BB#37:
	callq	_ZdaPv
.LBB10_38:                              # %_ZN11CStringBaseIwED2Ev.exit9
	movq	208(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_40
# BB#39:
	callq	_ZdaPv
.LBB10_40:                              # %_ZN11CStringBaseIwED2Ev.exit10
	movq	184(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_42
# BB#41:
	callq	_ZdaPv
.LBB10_42:                              # %_ZN11CStringBaseIwED2Ev.exit.i11
	movq	168(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_44
# BB#43:
	callq	_ZdaPv
.LBB10_44:                              # %_ZN11CStringBaseIwED2Ev.exit1.i12
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_46
# BB#45:
	callq	_ZdaPv
.LBB10_46:                              # %_ZN11CStringBaseIwED2Ev.exit2.i13
	movq	128(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_48
# BB#47:
	callq	_ZdaPv
.LBB10_48:                              # %_ZN11CStringBaseIwED2Ev.exit3.i14
	movq	112(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_50
# BB#49:
	callq	_ZdaPv
.LBB10_50:                              # %_ZN11CStringBaseIwED2Ev.exit4.i15
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_52
# BB#51:
	callq	_ZdaPv
.LBB10_52:                              # %_ZN11CStringBaseIwED2Ev.exit5.i16
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_54
# BB#53:
	callq	_ZdaPv
.LBB10_54:                              # %_ZN12CArchivePathD2Ev.exit17
	leaq	40(%r12), %r15
	movq	$_ZTV13CObjectVectorI21CUpdateArchiveCommandE+16, 40(%r12)
.Ltmp698:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp699:
# BB#55:
.Ltmp704:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp705:
.LBB10_56:                              # %_ZN13CObjectVectorI21CUpdateArchiveCommandED2Ev.exit20
	movq	$_ZTV13CObjectVectorI9CPropertyE+16, 8(%r12)
	addq	$8, %r12
.Ltmp715:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp716:
# BB#57:                                # %_ZN13CObjectVectorI9CPropertyED2Ev.exit.i
.Ltmp721:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp722:
.LBB10_30:                              # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB10_59:
.Ltmp700:
	movq	%rax, %r14
.Ltmp701:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp702:
	jmp	.LBB10_64
.LBB10_60:
.Ltmp703:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB10_61:
.Ltmp717:
	movq	%rax, %r14
.Ltmp718:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp719:
	jmp	.LBB10_64
.LBB10_62:
.Ltmp720:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB10_63:
.Ltmp723:
	movq	%rax, %r14
.LBB10_64:                              # %.body18
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end10:
	.size	_ZN14CUpdateOptionsD2Ev, .Lfunc_end10-_ZN14CUpdateOptionsD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\277\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\266\001"              # Call site table length
	.long	.Ltmp695-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp696-.Ltmp695       #   Call between .Ltmp695 and .Ltmp696
	.long	.Ltmp697-.Lfunc_begin7  #     jumps to .Ltmp697
	.byte	0                       #   On action: cleanup
	.long	.Ltmp706-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp707-.Ltmp706       #   Call between .Ltmp706 and .Ltmp707
	.long	.Ltmp708-.Lfunc_begin7  #     jumps to .Ltmp708
	.byte	0                       #   On action: cleanup
	.long	.Ltmp712-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp713-.Ltmp712       #   Call between .Ltmp712 and .Ltmp713
	.long	.Ltmp714-.Lfunc_begin7  #     jumps to .Ltmp714
	.byte	0                       #   On action: cleanup
	.long	.Ltmp724-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp725-.Ltmp724       #   Call between .Ltmp724 and .Ltmp725
	.long	.Ltmp726-.Lfunc_begin7  #     jumps to .Ltmp726
	.byte	0                       #   On action: cleanup
	.long	.Ltmp725-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Ltmp727-.Ltmp725       #   Call between .Ltmp725 and .Ltmp727
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp727-.Lfunc_begin7  # >> Call Site 6 <<
	.long	.Ltmp728-.Ltmp727       #   Call between .Ltmp727 and .Ltmp728
	.long	.Ltmp729-.Lfunc_begin7  #     jumps to .Ltmp729
	.byte	1                       #   On action: 1
	.long	.Ltmp709-.Lfunc_begin7  # >> Call Site 7 <<
	.long	.Ltmp710-.Ltmp709       #   Call between .Ltmp709 and .Ltmp710
	.long	.Ltmp711-.Lfunc_begin7  #     jumps to .Ltmp711
	.byte	1                       #   On action: 1
	.long	.Ltmp698-.Lfunc_begin7  # >> Call Site 8 <<
	.long	.Ltmp699-.Ltmp698       #   Call between .Ltmp698 and .Ltmp699
	.long	.Ltmp700-.Lfunc_begin7  #     jumps to .Ltmp700
	.byte	1                       #   On action: 1
	.long	.Ltmp704-.Lfunc_begin7  # >> Call Site 9 <<
	.long	.Ltmp705-.Ltmp704       #   Call between .Ltmp704 and .Ltmp705
	.long	.Ltmp723-.Lfunc_begin7  #     jumps to .Ltmp723
	.byte	1                       #   On action: 1
	.long	.Ltmp715-.Lfunc_begin7  # >> Call Site 10 <<
	.long	.Ltmp716-.Ltmp715       #   Call between .Ltmp715 and .Ltmp716
	.long	.Ltmp717-.Lfunc_begin7  #     jumps to .Ltmp717
	.byte	1                       #   On action: 1
	.long	.Ltmp721-.Lfunc_begin7  # >> Call Site 11 <<
	.long	.Ltmp722-.Ltmp721       #   Call between .Ltmp721 and .Ltmp722
	.long	.Ltmp723-.Lfunc_begin7  #     jumps to .Ltmp723
	.byte	1                       #   On action: 1
	.long	.Ltmp722-.Lfunc_begin7  # >> Call Site 12 <<
	.long	.Ltmp701-.Ltmp722       #   Call between .Ltmp722 and .Ltmp701
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp701-.Lfunc_begin7  # >> Call Site 13 <<
	.long	.Ltmp702-.Ltmp701       #   Call between .Ltmp701 and .Ltmp702
	.long	.Ltmp703-.Lfunc_begin7  #     jumps to .Ltmp703
	.byte	1                       #   On action: 1
	.long	.Ltmp718-.Lfunc_begin7  # >> Call Site 14 <<
	.long	.Ltmp719-.Ltmp718       #   Call between .Ltmp718 and .Ltmp719
	.long	.Ltmp720-.Lfunc_begin7  #     jumps to .Ltmp720
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CPropertyED2Ev,"axG",@progbits,_ZN13CObjectVectorI9CPropertyED2Ev,comdat
	.weak	_ZN13CObjectVectorI9CPropertyED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CPropertyED2Ev,@function
_ZN13CObjectVectorI9CPropertyED2Ev:     # @_ZN13CObjectVectorI9CPropertyED2Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi377:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi378:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi379:
	.cfi_def_cfa_offset 32
.Lcfi380:
	.cfi_offset %rbx, -24
.Lcfi381:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CPropertyE+16, (%rbx)
.Ltmp730:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp731:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB11_2:
.Ltmp732:
	movq	%rax, %r14
.Ltmp733:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp734:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB11_4:
.Ltmp735:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end11:
	.size	_ZN13CObjectVectorI9CPropertyED2Ev, .Lfunc_end11-_ZN13CObjectVectorI9CPropertyED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp730-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp731-.Ltmp730       #   Call between .Ltmp730 and .Ltmp731
	.long	.Ltmp732-.Lfunc_begin8  #     jumps to .Ltmp732
	.byte	0                       #   On action: cleanup
	.long	.Ltmp731-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp733-.Ltmp731       #   Call between .Ltmp731 and .Ltmp733
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp733-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp734-.Ltmp733       #   Call between .Ltmp733 and .Ltmp734
	.long	.Ltmp735-.Lfunc_begin8  #     jumps to .Ltmp735
	.byte	1                       #   On action: 1
	.long	.Ltmp734-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Lfunc_end11-.Ltmp734   #   Call between .Ltmp734 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard5CPairEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev,@function
_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev: # @_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi382:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi383:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi384:
	.cfi_def_cfa_offset 32
.Lcfi385:
	.cfi_offset %rbx, -24
.Lcfi386:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CPairEE+16, (%rbx)
.Ltmp736:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp737:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB12_2:
.Ltmp738:
	movq	%rax, %r14
.Ltmp739:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp740:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB12_4:
.Ltmp741:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev, .Lfunc_end12-_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp736-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp737-.Ltmp736       #   Call between .Ltmp736 and .Ltmp737
	.long	.Ltmp738-.Lfunc_begin9  #     jumps to .Ltmp738
	.byte	0                       #   On action: cleanup
	.long	.Ltmp737-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp739-.Ltmp737       #   Call between .Ltmp737 and .Ltmp739
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp739-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp740-.Ltmp739       #   Call between .Ltmp739 and .Ltmp740
	.long	.Ltmp741-.Lfunc_begin9  #     jumps to .Ltmp741
	.byte	1                       #   On action: 1
	.long	.Ltmp740-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Lfunc_end12-.Ltmp740   #   Call between .Ltmp740 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard5CPairEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard5CPairEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard5CPairEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard5CPairEED0Ev,@function
_ZN13CObjectVectorIN9NWildcard5CPairEED0Ev: # @_ZN13CObjectVectorIN9NWildcard5CPairEED0Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi387:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi388:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi389:
	.cfi_def_cfa_offset 32
.Lcfi390:
	.cfi_offset %rbx, -24
.Lcfi391:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CPairEE+16, (%rbx)
.Ltmp742:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp743:
# BB#1:
.Ltmp748:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp749:
# BB#2:                                 # %_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB13_5:
.Ltmp750:
	movq	%rax, %r14
	jmp	.LBB13_6
.LBB13_3:
.Ltmp744:
	movq	%rax, %r14
.Ltmp745:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp746:
.LBB13_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB13_4:
.Ltmp747:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end13:
	.size	_ZN13CObjectVectorIN9NWildcard5CPairEED0Ev, .Lfunc_end13-_ZN13CObjectVectorIN9NWildcard5CPairEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp742-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp743-.Ltmp742       #   Call between .Ltmp742 and .Ltmp743
	.long	.Ltmp744-.Lfunc_begin10 #     jumps to .Ltmp744
	.byte	0                       #   On action: cleanup
	.long	.Ltmp748-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp749-.Ltmp748       #   Call between .Ltmp748 and .Ltmp749
	.long	.Ltmp750-.Lfunc_begin10 #     jumps to .Ltmp750
	.byte	0                       #   On action: cleanup
	.long	.Ltmp745-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp746-.Ltmp745       #   Call between .Ltmp745 and .Ltmp746
	.long	.Ltmp747-.Lfunc_begin10 #     jumps to .Ltmp747
	.byte	1                       #   On action: 1
	.long	.Ltmp746-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Lfunc_end13-.Ltmp746   #   Call between .Ltmp746 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard5CPairEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard5CPairEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard5CPairEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard5CPairEE6DeleteEii,@function
_ZN13CObjectVectorIN9NWildcard5CPairEE6DeleteEii: # @_ZN13CObjectVectorIN9NWildcard5CPairEE6DeleteEii
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi392:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi393:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi394:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi395:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi396:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi397:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi398:
	.cfi_def_cfa_offset 64
.Lcfi399:
	.cfi_offset %rbx, -56
.Lcfi400:
	.cfi_offset %r12, -48
.Lcfi401:
	.cfi_offset %r13, -40
.Lcfi402:
	.cfi_offset %r14, -32
.Lcfi403:
	.cfi_offset %r15, -24
.Lcfi404:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB14_8
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB14_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB14_7
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	leaq	16(%rbp), %rdi
.Ltmp751:
	callq	_ZN9NWildcard11CCensorNodeD2Ev
.Ltmp752:
# BB#4:                                 #   in Loop: Header=BB14_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_6
# BB#5:                                 #   in Loop: Header=BB14_2 Depth=1
	callq	_ZdaPv
.LBB14_6:                               # %_ZN9NWildcard5CPairD2Ev.exit
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB14_7:                               #   in Loop: Header=BB14_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB14_2
.LBB14_8:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB14_9:
.Ltmp753:
	movq	%rax, %rbx
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB14_11
# BB#10:
	callq	_ZdaPv
.LBB14_11:                              # %.body
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZN13CObjectVectorIN9NWildcard5CPairEE6DeleteEii, .Lfunc_end14-_ZN13CObjectVectorIN9NWildcard5CPairEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp751-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp752-.Ltmp751       #   Call between .Ltmp751 and .Ltmp752
	.long	.Ltmp753-.Lfunc_begin11 #     jumps to .Ltmp753
	.byte	0                       #   On action: cleanup
	.long	.Ltmp752-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Lfunc_end14-.Ltmp752   #   Call between .Ltmp752 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN9NWildcard11CCensorNodeD2Ev,"axG",@progbits,_ZN9NWildcard11CCensorNodeD2Ev,comdat
	.weak	_ZN9NWildcard11CCensorNodeD2Ev
	.p2align	4, 0x90
	.type	_ZN9NWildcard11CCensorNodeD2Ev,@function
_ZN9NWildcard11CCensorNodeD2Ev:         # @_ZN9NWildcard11CCensorNodeD2Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r15
.Lcfi405:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi406:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi407:
	.cfi_def_cfa_offset 32
.Lcfi408:
	.cfi_offset %rbx, -32
.Lcfi409:
	.cfi_offset %r14, -24
.Lcfi410:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	88(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, 88(%r15)
.Ltmp754:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp755:
# BB#1:
.Ltmp760:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp761:
# BB#2:                                 # %_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev.exit
	leaq	56(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, 56(%r15)
.Ltmp771:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp772:
# BB#3:
.Ltmp777:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp778:
# BB#4:                                 # %_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev.exit6
	leaq	24(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE+16, 24(%r15)
.Ltmp789:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp790:
# BB#5:
.Ltmp795:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp796:
# BB#6:                                 # %_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev.exit
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB15_14
# BB#7:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZdaPv                  # TAILCALL
.LBB15_14:                              # %_ZN11CStringBaseIwED2Ev.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB15_26:
.Ltmp797:
	movq	%rax, %r14
	jmp	.LBB15_20
.LBB15_12:
.Ltmp791:
	movq	%rax, %r14
.Ltmp792:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp793:
	jmp	.LBB15_20
.LBB15_13:
.Ltmp794:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_25:
.Ltmp779:
	movq	%rax, %r14
	jmp	.LBB15_18
.LBB15_10:
.Ltmp773:
	movq	%rax, %r14
.Ltmp774:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp775:
	jmp	.LBB15_18
.LBB15_11:
.Ltmp776:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_15:
.Ltmp762:
	movq	%rax, %r14
	jmp	.LBB15_16
.LBB15_8:
.Ltmp756:
	movq	%rax, %r14
.Ltmp757:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp758:
.LBB15_16:                              # %.body
	leaq	56(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, 56(%r15)
.Ltmp763:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp764:
# BB#17:
.Ltmp769:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp770:
.LBB15_18:                              # %_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev.exit11
	leaq	24(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE+16, 24(%r15)
.Ltmp780:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp781:
# BB#19:
.Ltmp786:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp787:
.LBB15_20:                              # %_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev.exit14
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB15_22
# BB#21:
	callq	_ZdaPv
.LBB15_22:                              # %_ZN11CStringBaseIwED2Ev.exit15
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB15_9:
.Ltmp759:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_23:
.Ltmp765:
	movq	%rax, %r14
.Ltmp766:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp767:
	jmp	.LBB15_30
.LBB15_24:
.Ltmp768:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_27:
.Ltmp782:
	movq	%rax, %r14
.Ltmp783:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp784:
	jmp	.LBB15_30
.LBB15_28:
.Ltmp785:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_29:
.Ltmp788:
	movq	%rax, %r14
.LBB15_30:                              # %.body9
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZN9NWildcard11CCensorNodeD2Ev, .Lfunc_end15-_ZN9NWildcard11CCensorNodeD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\331\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\320\001"              # Call site table length
	.long	.Ltmp754-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp755-.Ltmp754       #   Call between .Ltmp754 and .Ltmp755
	.long	.Ltmp756-.Lfunc_begin12 #     jumps to .Ltmp756
	.byte	0                       #   On action: cleanup
	.long	.Ltmp760-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp761-.Ltmp760       #   Call between .Ltmp760 and .Ltmp761
	.long	.Ltmp762-.Lfunc_begin12 #     jumps to .Ltmp762
	.byte	0                       #   On action: cleanup
	.long	.Ltmp771-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp772-.Ltmp771       #   Call between .Ltmp771 and .Ltmp772
	.long	.Ltmp773-.Lfunc_begin12 #     jumps to .Ltmp773
	.byte	0                       #   On action: cleanup
	.long	.Ltmp777-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Ltmp778-.Ltmp777       #   Call between .Ltmp777 and .Ltmp778
	.long	.Ltmp779-.Lfunc_begin12 #     jumps to .Ltmp779
	.byte	0                       #   On action: cleanup
	.long	.Ltmp789-.Lfunc_begin12 # >> Call Site 5 <<
	.long	.Ltmp790-.Ltmp789       #   Call between .Ltmp789 and .Ltmp790
	.long	.Ltmp791-.Lfunc_begin12 #     jumps to .Ltmp791
	.byte	0                       #   On action: cleanup
	.long	.Ltmp795-.Lfunc_begin12 # >> Call Site 6 <<
	.long	.Ltmp796-.Ltmp795       #   Call between .Ltmp795 and .Ltmp796
	.long	.Ltmp797-.Lfunc_begin12 #     jumps to .Ltmp797
	.byte	0                       #   On action: cleanup
	.long	.Ltmp792-.Lfunc_begin12 # >> Call Site 7 <<
	.long	.Ltmp793-.Ltmp792       #   Call between .Ltmp792 and .Ltmp793
	.long	.Ltmp794-.Lfunc_begin12 #     jumps to .Ltmp794
	.byte	1                       #   On action: 1
	.long	.Ltmp774-.Lfunc_begin12 # >> Call Site 8 <<
	.long	.Ltmp775-.Ltmp774       #   Call between .Ltmp774 and .Ltmp775
	.long	.Ltmp776-.Lfunc_begin12 #     jumps to .Ltmp776
	.byte	1                       #   On action: 1
	.long	.Ltmp757-.Lfunc_begin12 # >> Call Site 9 <<
	.long	.Ltmp758-.Ltmp757       #   Call between .Ltmp757 and .Ltmp758
	.long	.Ltmp759-.Lfunc_begin12 #     jumps to .Ltmp759
	.byte	1                       #   On action: 1
	.long	.Ltmp763-.Lfunc_begin12 # >> Call Site 10 <<
	.long	.Ltmp764-.Ltmp763       #   Call between .Ltmp763 and .Ltmp764
	.long	.Ltmp765-.Lfunc_begin12 #     jumps to .Ltmp765
	.byte	1                       #   On action: 1
	.long	.Ltmp769-.Lfunc_begin12 # >> Call Site 11 <<
	.long	.Ltmp770-.Ltmp769       #   Call between .Ltmp769 and .Ltmp770
	.long	.Ltmp788-.Lfunc_begin12 #     jumps to .Ltmp788
	.byte	1                       #   On action: 1
	.long	.Ltmp780-.Lfunc_begin12 # >> Call Site 12 <<
	.long	.Ltmp781-.Ltmp780       #   Call between .Ltmp780 and .Ltmp781
	.long	.Ltmp782-.Lfunc_begin12 #     jumps to .Ltmp782
	.byte	1                       #   On action: 1
	.long	.Ltmp786-.Lfunc_begin12 # >> Call Site 13 <<
	.long	.Ltmp787-.Ltmp786       #   Call between .Ltmp786 and .Ltmp787
	.long	.Ltmp788-.Lfunc_begin12 #     jumps to .Ltmp788
	.byte	1                       #   On action: 1
	.long	.Ltmp787-.Lfunc_begin12 # >> Call Site 14 <<
	.long	.Ltmp766-.Ltmp787       #   Call between .Ltmp787 and .Ltmp766
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp766-.Lfunc_begin12 # >> Call Site 15 <<
	.long	.Ltmp767-.Ltmp766       #   Call between .Ltmp766 and .Ltmp767
	.long	.Ltmp768-.Lfunc_begin12 #     jumps to .Ltmp768
	.byte	1                       #   On action: 1
	.long	.Ltmp783-.Lfunc_begin12 # >> Call Site 16 <<
	.long	.Ltmp784-.Ltmp783       #   Call between .Ltmp783 and .Ltmp784
	.long	.Ltmp785-.Lfunc_begin12 #     jumps to .Ltmp785
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard5CItemEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev,@function
_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev: # @_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r14
.Lcfi411:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi412:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi413:
	.cfi_def_cfa_offset 32
.Lcfi414:
	.cfi_offset %rbx, -24
.Lcfi415:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, (%rbx)
.Ltmp798:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp799:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB16_2:
.Ltmp800:
	movq	%rax, %r14
.Ltmp801:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp802:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB16_4:
.Ltmp803:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev, .Lfunc_end16-_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp798-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp799-.Ltmp798       #   Call between .Ltmp798 and .Ltmp799
	.long	.Ltmp800-.Lfunc_begin13 #     jumps to .Ltmp800
	.byte	0                       #   On action: cleanup
	.long	.Ltmp799-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp801-.Ltmp799       #   Call between .Ltmp799 and .Ltmp801
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp801-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp802-.Ltmp801       #   Call between .Ltmp801 and .Ltmp802
	.long	.Ltmp803-.Lfunc_begin13 #     jumps to .Ltmp803
	.byte	1                       #   On action: 1
	.long	.Ltmp802-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Lfunc_end16-.Ltmp802   #   Call between .Ltmp802 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev,@function
_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev: # @_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi416:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi417:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi418:
	.cfi_def_cfa_offset 32
.Lcfi419:
	.cfi_offset %rbx, -24
.Lcfi420:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE+16, (%rbx)
.Ltmp804:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp805:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB17_2:
.Ltmp806:
	movq	%rax, %r14
.Ltmp807:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp808:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB17_4:
.Ltmp809:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end17:
	.size	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev, .Lfunc_end17-_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp804-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp805-.Ltmp804       #   Call between .Ltmp804 and .Ltmp805
	.long	.Ltmp806-.Lfunc_begin14 #     jumps to .Ltmp806
	.byte	0                       #   On action: cleanup
	.long	.Ltmp805-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp807-.Ltmp805       #   Call between .Ltmp805 and .Ltmp807
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp807-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp808-.Ltmp807       #   Call between .Ltmp807 and .Ltmp808
	.long	.Ltmp809-.Lfunc_begin14 #     jumps to .Ltmp809
	.byte	1                       #   On action: 1
	.long	.Ltmp808-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Lfunc_end17-.Ltmp808   #   Call between .Ltmp808 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard5CItemEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev,@function
_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev: # @_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi421:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi422:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi423:
	.cfi_def_cfa_offset 32
.Lcfi424:
	.cfi_offset %rbx, -24
.Lcfi425:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard5CItemEE+16, (%rbx)
.Ltmp810:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp811:
# BB#1:
.Ltmp816:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp817:
# BB#2:                                 # %_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB18_5:
.Ltmp818:
	movq	%rax, %r14
	jmp	.LBB18_6
.LBB18_3:
.Ltmp812:
	movq	%rax, %r14
.Ltmp813:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp814:
.LBB18_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB18_4:
.Ltmp815:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev, .Lfunc_end18-_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp810-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp811-.Ltmp810       #   Call between .Ltmp810 and .Ltmp811
	.long	.Ltmp812-.Lfunc_begin15 #     jumps to .Ltmp812
	.byte	0                       #   On action: cleanup
	.long	.Ltmp816-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp817-.Ltmp816       #   Call between .Ltmp816 and .Ltmp817
	.long	.Ltmp818-.Lfunc_begin15 #     jumps to .Ltmp818
	.byte	0                       #   On action: cleanup
	.long	.Ltmp813-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp814-.Ltmp813       #   Call between .Ltmp813 and .Ltmp814
	.long	.Ltmp815-.Lfunc_begin15 #     jumps to .Ltmp815
	.byte	1                       #   On action: 1
	.long	.Ltmp814-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Lfunc_end18-.Ltmp814   #   Call between .Ltmp814 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii,@function
_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii: # @_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi426:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi427:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi428:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi429:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi430:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi431:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi432:
	.cfi_def_cfa_offset 64
.Lcfi433:
	.cfi_offset %rbx, -56
.Lcfi434:
	.cfi_offset %r12, -48
.Lcfi435:
	.cfi_offset %r13, -40
.Lcfi436:
	.cfi_offset %r14, -32
.Lcfi437:
	.cfi_offset %r15, -24
.Lcfi438:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB19_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB19_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB19_6
# BB#3:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbp)
.Ltmp819:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp820:
# BB#4:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit.i
                                        #   in Loop: Header=BB19_2 Depth=1
.Ltmp825:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp826:
# BB#5:                                 # %_ZN9NWildcard5CItemD2Ev.exit
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB19_6:                               #   in Loop: Header=BB19_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB19_2
.LBB19_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB19_8:
.Ltmp821:
	movq	%rax, %rbx
.Ltmp822:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp823:
	jmp	.LBB19_11
.LBB19_9:
.Ltmp824:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB19_10:
.Ltmp827:
	movq	%rax, %rbx
.LBB19_11:                              # %.body
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end19:
	.size	_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii, .Lfunc_end19-_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp819-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp820-.Ltmp819       #   Call between .Ltmp819 and .Ltmp820
	.long	.Ltmp821-.Lfunc_begin16 #     jumps to .Ltmp821
	.byte	0                       #   On action: cleanup
	.long	.Ltmp825-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp826-.Ltmp825       #   Call between .Ltmp825 and .Ltmp826
	.long	.Ltmp827-.Lfunc_begin16 #     jumps to .Ltmp827
	.byte	0                       #   On action: cleanup
	.long	.Ltmp826-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Ltmp822-.Ltmp826       #   Call between .Ltmp826 and .Ltmp822
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp822-.Lfunc_begin16 # >> Call Site 4 <<
	.long	.Ltmp823-.Ltmp822       #   Call between .Ltmp822 and .Ltmp823
	.long	.Ltmp824-.Lfunc_begin16 #     jumps to .Ltmp824
	.byte	1                       #   On action: 1
	.long	.Ltmp823-.Lfunc_begin16 # >> Call Site 5 <<
	.long	.Lfunc_end19-.Ltmp823   #   Call between .Ltmp823 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev,@function
_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev: # @_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi439:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi440:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi441:
	.cfi_def_cfa_offset 32
.Lcfi442:
	.cfi_offset %rbx, -24
.Lcfi443:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE+16, (%rbx)
.Ltmp828:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp829:
# BB#1:
.Ltmp834:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp835:
# BB#2:                                 # %_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB20_5:
.Ltmp836:
	movq	%rax, %r14
	jmp	.LBB20_6
.LBB20_3:
.Ltmp830:
	movq	%rax, %r14
.Ltmp831:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp832:
.LBB20_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB20_4:
.Ltmp833:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end20:
	.size	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev, .Lfunc_end20-_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp828-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp829-.Ltmp828       #   Call between .Ltmp828 and .Ltmp829
	.long	.Ltmp830-.Lfunc_begin17 #     jumps to .Ltmp830
	.byte	0                       #   On action: cleanup
	.long	.Ltmp834-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Ltmp835-.Ltmp834       #   Call between .Ltmp834 and .Ltmp835
	.long	.Ltmp836-.Lfunc_begin17 #     jumps to .Ltmp836
	.byte	0                       #   On action: cleanup
	.long	.Ltmp831-.Lfunc_begin17 # >> Call Site 3 <<
	.long	.Ltmp832-.Ltmp831       #   Call between .Ltmp831 and .Ltmp832
	.long	.Ltmp833-.Lfunc_begin17 #     jumps to .Ltmp833
	.byte	1                       #   On action: 1
	.long	.Ltmp832-.Lfunc_begin17 # >> Call Site 4 <<
	.long	.Lfunc_end20-.Ltmp832   #   Call between .Ltmp832 and .Lfunc_end20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii,@function
_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii: # @_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi444:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi445:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi446:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi447:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi448:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi449:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi450:
	.cfi_def_cfa_offset 64
.Lcfi451:
	.cfi_offset %rbx, -56
.Lcfi452:
	.cfi_offset %r12, -48
.Lcfi453:
	.cfi_offset %r13, -40
.Lcfi454:
	.cfi_offset %r14, -32
.Lcfi455:
	.cfi_offset %r15, -24
.Lcfi456:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB21_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB21_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB21_5
# BB#3:                                 #   in Loop: Header=BB21_2 Depth=1
.Ltmp837:
	movq	%rbp, %rdi
	callq	_ZN9NWildcard11CCensorNodeD2Ev
.Ltmp838:
# BB#4:                                 #   in Loop: Header=BB21_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB21_5:                               #   in Loop: Header=BB21_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB21_2
.LBB21_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB21_7:
.Ltmp839:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end21:
	.size	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii, .Lfunc_end21-_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp837-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp838-.Ltmp837       #   Call between .Ltmp837 and .Ltmp838
	.long	.Ltmp839-.Lfunc_begin18 #     jumps to .Ltmp839
	.byte	0                       #   On action: cleanup
	.long	.Ltmp838-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Lfunc_end21-.Ltmp838   #   Call between .Ltmp838 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CPropertyED0Ev,"axG",@progbits,_ZN13CObjectVectorI9CPropertyED0Ev,comdat
	.weak	_ZN13CObjectVectorI9CPropertyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CPropertyED0Ev,@function
_ZN13CObjectVectorI9CPropertyED0Ev:     # @_ZN13CObjectVectorI9CPropertyED0Ev
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:
	pushq	%r14
.Lcfi457:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi458:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi459:
	.cfi_def_cfa_offset 32
.Lcfi460:
	.cfi_offset %rbx, -24
.Lcfi461:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CPropertyE+16, (%rbx)
.Ltmp840:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp841:
# BB#1:
.Ltmp846:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp847:
# BB#2:                                 # %_ZN13CObjectVectorI9CPropertyED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB22_5:
.Ltmp848:
	movq	%rax, %r14
	jmp	.LBB22_6
.LBB22_3:
.Ltmp842:
	movq	%rax, %r14
.Ltmp843:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp844:
.LBB22_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_4:
.Ltmp845:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end22:
	.size	_ZN13CObjectVectorI9CPropertyED0Ev, .Lfunc_end22-_ZN13CObjectVectorI9CPropertyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp840-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp841-.Ltmp840       #   Call between .Ltmp840 and .Ltmp841
	.long	.Ltmp842-.Lfunc_begin19 #     jumps to .Ltmp842
	.byte	0                       #   On action: cleanup
	.long	.Ltmp846-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Ltmp847-.Ltmp846       #   Call between .Ltmp846 and .Ltmp847
	.long	.Ltmp848-.Lfunc_begin19 #     jumps to .Ltmp848
	.byte	0                       #   On action: cleanup
	.long	.Ltmp843-.Lfunc_begin19 # >> Call Site 3 <<
	.long	.Ltmp844-.Ltmp843       #   Call between .Ltmp843 and .Ltmp844
	.long	.Ltmp845-.Lfunc_begin19 #     jumps to .Ltmp845
	.byte	1                       #   On action: 1
	.long	.Ltmp844-.Lfunc_begin19 # >> Call Site 4 <<
	.long	.Lfunc_end22-.Ltmp844   #   Call between .Ltmp844 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CPropertyE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI9CPropertyE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI9CPropertyE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CPropertyE6DeleteEii,@function
_ZN13CObjectVectorI9CPropertyE6DeleteEii: # @_ZN13CObjectVectorI9CPropertyE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi462:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi463:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi464:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi465:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi466:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi467:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi468:
	.cfi_def_cfa_offset 64
.Lcfi469:
	.cfi_offset %rbx, -56
.Lcfi470:
	.cfi_offset %r12, -48
.Lcfi471:
	.cfi_offset %r13, -40
.Lcfi472:
	.cfi_offset %r14, -32
.Lcfi473:
	.cfi_offset %r15, -24
.Lcfi474:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB23_9
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB23_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB23_8
# BB#3:                                 #   in Loop: Header=BB23_2 Depth=1
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB23_5
# BB#4:                                 #   in Loop: Header=BB23_2 Depth=1
	callq	_ZdaPv
.LBB23_5:                               # %_ZN11CStringBaseIwED2Ev.exit.i
                                        #   in Loop: Header=BB23_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB23_7
# BB#6:                                 #   in Loop: Header=BB23_2 Depth=1
	callq	_ZdaPv
.LBB23_7:                               # %_ZN9CPropertyD2Ev.exit
                                        #   in Loop: Header=BB23_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB23_8:                               #   in Loop: Header=BB23_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB23_2
.LBB23_9:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end23:
	.size	_ZN13CObjectVectorI9CPropertyE6DeleteEii, .Lfunc_end23-_ZN13CObjectVectorI9CPropertyE6DeleteEii
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI24_0:
	.zero	16
	.section	.text._ZN12CArchivePathC2Ev,"axG",@progbits,_ZN12CArchivePathC2Ev,comdat
	.weak	_ZN12CArchivePathC2Ev
	.p2align	4, 0x90
	.type	_ZN12CArchivePathC2Ev,@function
_ZN12CArchivePathC2Ev:                  # @_ZN12CArchivePathC2Ev
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%r15
.Lcfi475:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi476:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi477:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi478:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi479:
	.cfi_def_cfa_offset 48
.Lcfi480:
	.cfi_offset %rbx, -40
.Lcfi481:
	.cfi_offset %r12, -32
.Lcfi482:
	.cfi_offset %r14, -24
.Lcfi483:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	$4, 12(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
.Ltmp849:
	movl	$16, %edi
	callq	_Znam
.Ltmp850:
# BB#1:
	movq	%rax, 16(%rbx)
	movl	$0, (%rax)
	movl	$4, 28(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
.Ltmp852:
	movl	$16, %edi
	callq	_Znam
.Ltmp853:
# BB#2:
	movq	%rax, 32(%rbx)
	movl	$0, (%rax)
	movl	$4, 44(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
.Ltmp855:
	movl	$16, %edi
	callq	_Znam
.Ltmp856:
# BB#3:
	movq	%rax, 48(%rbx)
	movl	$0, (%rax)
	movl	$4, 60(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%rbx)
.Ltmp858:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp859:
# BB#4:
	movq	%r15, 64(%rbx)
	movl	$0, (%r15)
	movl	$4, 76(%rbx)
	movb	$0, 80(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 88(%rbx)
.Ltmp861:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r12
.Ltmp862:
# BB#5:
	leaq	64(%rbx), %r15
	movq	%r12, 88(%rbx)
	movl	$0, (%r12)
	movl	$4, 100(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 104(%rbx)
.Ltmp864:
	movl	$16, %edi
	callq	_Znam
.Ltmp865:
# BB#6:
	movq	%rax, 104(%rbx)
	movl	$0, (%rax)
	movl	$4, 116(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB24_12:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp866:
	movq	%rax, %r14
	movq	%r12, %rdi
	callq	_ZdaPv
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB24_13
	jmp	.LBB24_14
.LBB24_11:                              # %_ZN11CStringBaseIwED2Ev.exit.thread
.Ltmp863:
	movq	%rax, %r14
.LBB24_13:
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB24_14
.LBB24_10:
.Ltmp860:
	movq	%rax, %r14
.LBB24_14:                              # %_ZN11CStringBaseIwED2Ev.exit12
	leaq	48(%rbx), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB24_16
# BB#15:
	callq	_ZdaPv
	jmp	.LBB24_16
.LBB24_9:
.Ltmp857:
	movq	%rax, %r14
.LBB24_16:                              # %_ZN11CStringBaseIwED2Ev.exit13
	leaq	32(%rbx), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB24_18
# BB#17:
	callq	_ZdaPv
	jmp	.LBB24_18
.LBB24_8:
.Ltmp854:
	movq	%rax, %r14
.LBB24_18:                              # %_ZN11CStringBaseIwED2Ev.exit14
	leaq	16(%rbx), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB24_20
# BB#19:
	callq	_ZdaPv
	jmp	.LBB24_20
.LBB24_7:
.Ltmp851:
	movq	%rax, %r14
.LBB24_20:                              # %_ZN11CStringBaseIwED2Ev.exit15
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB24_22
# BB#21:
	callq	_ZdaPv
.LBB24_22:                              # %_ZN11CStringBaseIwED2Ev.exit16
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end24:
	.size	_ZN12CArchivePathC2Ev, .Lfunc_end24-_ZN12CArchivePathC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table24:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\352\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Lfunc_begin20-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp849-.Lfunc_begin20 #   Call between .Lfunc_begin20 and .Ltmp849
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp849-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Ltmp850-.Ltmp849       #   Call between .Ltmp849 and .Ltmp850
	.long	.Ltmp851-.Lfunc_begin20 #     jumps to .Ltmp851
	.byte	0                       #   On action: cleanup
	.long	.Ltmp852-.Lfunc_begin20 # >> Call Site 3 <<
	.long	.Ltmp853-.Ltmp852       #   Call between .Ltmp852 and .Ltmp853
	.long	.Ltmp854-.Lfunc_begin20 #     jumps to .Ltmp854
	.byte	0                       #   On action: cleanup
	.long	.Ltmp855-.Lfunc_begin20 # >> Call Site 4 <<
	.long	.Ltmp856-.Ltmp855       #   Call between .Ltmp855 and .Ltmp856
	.long	.Ltmp857-.Lfunc_begin20 #     jumps to .Ltmp857
	.byte	0                       #   On action: cleanup
	.long	.Ltmp858-.Lfunc_begin20 # >> Call Site 5 <<
	.long	.Ltmp859-.Ltmp858       #   Call between .Ltmp858 and .Ltmp859
	.long	.Ltmp860-.Lfunc_begin20 #     jumps to .Ltmp860
	.byte	0                       #   On action: cleanup
	.long	.Ltmp861-.Lfunc_begin20 # >> Call Site 6 <<
	.long	.Ltmp862-.Ltmp861       #   Call between .Ltmp861 and .Ltmp862
	.long	.Ltmp863-.Lfunc_begin20 #     jumps to .Ltmp863
	.byte	0                       #   On action: cleanup
	.long	.Ltmp864-.Lfunc_begin20 # >> Call Site 7 <<
	.long	.Ltmp865-.Ltmp864       #   Call between .Ltmp864 and .Ltmp865
	.long	.Ltmp866-.Lfunc_begin20 #     jumps to .Ltmp866
	.byte	0                       #   On action: cleanup
	.long	.Ltmp865-.Lfunc_begin20 # >> Call Site 8 <<
	.long	.Lfunc_end24-.Ltmp865   #   Call between .Ltmp865 and .Lfunc_end24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI21CUpdateArchiveCommandED2Ev,"axG",@progbits,_ZN13CObjectVectorI21CUpdateArchiveCommandED2Ev,comdat
	.weak	_ZN13CObjectVectorI21CUpdateArchiveCommandED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI21CUpdateArchiveCommandED2Ev,@function
_ZN13CObjectVectorI21CUpdateArchiveCommandED2Ev: # @_ZN13CObjectVectorI21CUpdateArchiveCommandED2Ev
.Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception21
# BB#0:
	pushq	%r14
.Lcfi484:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi485:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi486:
	.cfi_def_cfa_offset 32
.Lcfi487:
	.cfi_offset %rbx, -24
.Lcfi488:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI21CUpdateArchiveCommandE+16, (%rbx)
.Ltmp867:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp868:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB25_2:
.Ltmp869:
	movq	%rax, %r14
.Ltmp870:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp871:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB25_4:
.Ltmp872:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end25:
	.size	_ZN13CObjectVectorI21CUpdateArchiveCommandED2Ev, .Lfunc_end25-_ZN13CObjectVectorI21CUpdateArchiveCommandED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception21:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp867-.Lfunc_begin21 # >> Call Site 1 <<
	.long	.Ltmp868-.Ltmp867       #   Call between .Ltmp867 and .Ltmp868
	.long	.Ltmp869-.Lfunc_begin21 #     jumps to .Ltmp869
	.byte	0                       #   On action: cleanup
	.long	.Ltmp868-.Lfunc_begin21 # >> Call Site 2 <<
	.long	.Ltmp870-.Ltmp868       #   Call between .Ltmp868 and .Ltmp870
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp870-.Lfunc_begin21 # >> Call Site 3 <<
	.long	.Ltmp871-.Ltmp870       #   Call between .Ltmp870 and .Ltmp871
	.long	.Ltmp872-.Lfunc_begin21 #     jumps to .Ltmp872
	.byte	1                       #   On action: 1
	.long	.Ltmp871-.Lfunc_begin21 # >> Call Site 4 <<
	.long	.Lfunc_end25-.Ltmp871   #   Call between .Ltmp871 and .Lfunc_end25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI21CUpdateArchiveCommandED0Ev,"axG",@progbits,_ZN13CObjectVectorI21CUpdateArchiveCommandED0Ev,comdat
	.weak	_ZN13CObjectVectorI21CUpdateArchiveCommandED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI21CUpdateArchiveCommandED0Ev,@function
_ZN13CObjectVectorI21CUpdateArchiveCommandED0Ev: # @_ZN13CObjectVectorI21CUpdateArchiveCommandED0Ev
.Lfunc_begin22:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception22
# BB#0:
	pushq	%r14
.Lcfi489:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi490:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi491:
	.cfi_def_cfa_offset 32
.Lcfi492:
	.cfi_offset %rbx, -24
.Lcfi493:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI21CUpdateArchiveCommandE+16, (%rbx)
.Ltmp873:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp874:
# BB#1:
.Ltmp879:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp880:
# BB#2:                                 # %_ZN13CObjectVectorI21CUpdateArchiveCommandED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB26_5:
.Ltmp881:
	movq	%rax, %r14
	jmp	.LBB26_6
.LBB26_3:
.Ltmp875:
	movq	%rax, %r14
.Ltmp876:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp877:
.LBB26_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB26_4:
.Ltmp878:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end26:
	.size	_ZN13CObjectVectorI21CUpdateArchiveCommandED0Ev, .Lfunc_end26-_ZN13CObjectVectorI21CUpdateArchiveCommandED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception22:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp873-.Lfunc_begin22 # >> Call Site 1 <<
	.long	.Ltmp874-.Ltmp873       #   Call between .Ltmp873 and .Ltmp874
	.long	.Ltmp875-.Lfunc_begin22 #     jumps to .Ltmp875
	.byte	0                       #   On action: cleanup
	.long	.Ltmp879-.Lfunc_begin22 # >> Call Site 2 <<
	.long	.Ltmp880-.Ltmp879       #   Call between .Ltmp879 and .Ltmp880
	.long	.Ltmp881-.Lfunc_begin22 #     jumps to .Ltmp881
	.byte	0                       #   On action: cleanup
	.long	.Ltmp876-.Lfunc_begin22 # >> Call Site 3 <<
	.long	.Ltmp877-.Ltmp876       #   Call between .Ltmp876 and .Ltmp877
	.long	.Ltmp878-.Lfunc_begin22 #     jumps to .Ltmp878
	.byte	1                       #   On action: 1
	.long	.Ltmp877-.Lfunc_begin22 # >> Call Site 4 <<
	.long	.Lfunc_end26-.Ltmp877   #   Call between .Ltmp877 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI21CUpdateArchiveCommandE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI21CUpdateArchiveCommandE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI21CUpdateArchiveCommandE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI21CUpdateArchiveCommandE6DeleteEii,@function
_ZN13CObjectVectorI21CUpdateArchiveCommandE6DeleteEii: # @_ZN13CObjectVectorI21CUpdateArchiveCommandE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi494:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi495:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi496:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi497:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi498:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi499:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi500:
	.cfi_def_cfa_offset 64
.Lcfi501:
	.cfi_offset %rbx, -56
.Lcfi502:
	.cfi_offset %r12, -48
.Lcfi503:
	.cfi_offset %r13, -40
.Lcfi504:
	.cfi_offset %r14, -32
.Lcfi505:
	.cfi_offset %r15, -24
.Lcfi506:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB27_21
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB27_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB27_20
# BB#3:                                 #   in Loop: Header=BB27_2 Depth=1
	movq	120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB27_5
# BB#4:                                 #   in Loop: Header=BB27_2 Depth=1
	callq	_ZdaPv
.LBB27_5:                               # %_ZN11CStringBaseIwED2Ev.exit.i
                                        #   in Loop: Header=BB27_2 Depth=1
	movq	104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB27_7
# BB#6:                                 #   in Loop: Header=BB27_2 Depth=1
	callq	_ZdaPv
.LBB27_7:                               # %_ZN11CStringBaseIwED2Ev.exit1.i
                                        #   in Loop: Header=BB27_2 Depth=1
	movq	80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB27_9
# BB#8:                                 #   in Loop: Header=BB27_2 Depth=1
	callq	_ZdaPv
.LBB27_9:                               # %_ZN11CStringBaseIwED2Ev.exit2.i10
                                        #   in Loop: Header=BB27_2 Depth=1
	movq	64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB27_11
# BB#10:                                #   in Loop: Header=BB27_2 Depth=1
	callq	_ZdaPv
.LBB27_11:                              # %_ZN11CStringBaseIwED2Ev.exit3.i
                                        #   in Loop: Header=BB27_2 Depth=1
	movq	48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB27_13
# BB#12:                                #   in Loop: Header=BB27_2 Depth=1
	callq	_ZdaPv
.LBB27_13:                              # %_ZN11CStringBaseIwED2Ev.exit4.i
                                        #   in Loop: Header=BB27_2 Depth=1
	movq	32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB27_15
# BB#14:                                #   in Loop: Header=BB27_2 Depth=1
	callq	_ZdaPv
.LBB27_15:                              # %_ZN11CStringBaseIwED2Ev.exit5.i
                                        #   in Loop: Header=BB27_2 Depth=1
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB27_17
# BB#16:                                #   in Loop: Header=BB27_2 Depth=1
	callq	_ZdaPv
.LBB27_17:                              # %_ZN12CArchivePathD2Ev.exit
                                        #   in Loop: Header=BB27_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB27_19
# BB#18:                                #   in Loop: Header=BB27_2 Depth=1
	callq	_ZdaPv
.LBB27_19:                              # %_ZN21CUpdateArchiveCommandD2Ev.exit
                                        #   in Loop: Header=BB27_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB27_20:                              #   in Loop: Header=BB27_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB27_2
.LBB27_21:                              # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end27:
	.size	_ZN13CObjectVectorI21CUpdateArchiveCommandE6DeleteEii, .Lfunc_end27-_ZN13CObjectVectorI21CUpdateArchiveCommandE6DeleteEii
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIyED0Ev,"axG",@progbits,_ZN13CRecordVectorIyED0Ev,comdat
	.weak	_ZN13CRecordVectorIyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyED0Ev,@function
_ZN13CRecordVectorIyED0Ev:              # @_ZN13CRecordVectorIyED0Ev
.Lfunc_begin23:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception23
# BB#0:
	pushq	%r14
.Lcfi507:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi508:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi509:
	.cfi_def_cfa_offset 32
.Lcfi510:
	.cfi_offset %rbx, -24
.Lcfi511:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp882:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp883:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB28_2:
.Ltmp884:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end28:
	.size	_ZN13CRecordVectorIyED0Ev, .Lfunc_end28-_ZN13CRecordVectorIyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception23:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp882-.Lfunc_begin23 # >> Call Site 1 <<
	.long	.Ltmp883-.Ltmp882       #   Call between .Ltmp882 and .Ltmp883
	.long	.Ltmp884-.Lfunc_begin23 #     jumps to .Ltmp884
	.byte	0                       #   On action: cleanup
	.long	.Ltmp883-.Lfunc_begin23 # >> Call Site 2 <<
	.long	.Lfunc_end28-.Ltmp883   #   Call between .Ltmp883 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN7CCodecs14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN7CCodecs14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN7CCodecs14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN7CCodecs14QueryInterfaceERK4GUIDPPv,@function
_ZN7CCodecs14QueryInterfaceERK4GUIDPPv: # @_ZN7CCodecs14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi512:
	.cfi_def_cfa_offset 16
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	$0, (%rsi)
	jne	.LBB29_17
# BB#1:
	cmpb	$0, 1(%rsi)
	jne	.LBB29_17
# BB#2:
	cmpb	$0, 2(%rsi)
	jne	.LBB29_17
# BB#3:
	cmpb	$0, 3(%rsi)
	jne	.LBB29_17
# BB#4:
	cmpb	$0, 4(%rsi)
	jne	.LBB29_17
# BB#5:
	cmpb	$0, 5(%rsi)
	jne	.LBB29_17
# BB#6:
	cmpb	$0, 6(%rsi)
	jne	.LBB29_17
# BB#7:
	cmpb	$0, 7(%rsi)
	jne	.LBB29_17
# BB#8:
	cmpb	$-64, 8(%rsi)
	jne	.LBB29_17
# BB#9:
	cmpb	$0, 9(%rsi)
	jne	.LBB29_17
# BB#10:
	cmpb	$0, 10(%rsi)
	jne	.LBB29_17
# BB#11:
	cmpb	$0, 11(%rsi)
	jne	.LBB29_17
# BB#12:
	cmpb	$0, 12(%rsi)
	jne	.LBB29_17
# BB#13:
	cmpb	$0, 13(%rsi)
	jne	.LBB29_17
# BB#14:
	cmpb	$0, 14(%rsi)
	jne	.LBB29_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	cmpb	$70, 15(%rsi)
	jne	.LBB29_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB29_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end29:
	.size	_ZN7CCodecs14QueryInterfaceERK4GUIDPPv, .Lfunc_end29-_ZN7CCodecs14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN7CCodecs6AddRefEv,"axG",@progbits,_ZN7CCodecs6AddRefEv,comdat
	.weak	_ZN7CCodecs6AddRefEv
	.p2align	4, 0x90
	.type	_ZN7CCodecs6AddRefEv,@function
_ZN7CCodecs6AddRefEv:                   # @_ZN7CCodecs6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end30:
	.size	_ZN7CCodecs6AddRefEv, .Lfunc_end30-_ZN7CCodecs6AddRefEv
	.cfi_endproc

	.section	.text._ZN7CCodecs7ReleaseEv,"axG",@progbits,_ZN7CCodecs7ReleaseEv,comdat
	.weak	_ZN7CCodecs7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN7CCodecs7ReleaseEv,@function
_ZN7CCodecs7ReleaseEv:                  # @_ZN7CCodecs7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi513:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB31_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB31_2:
	popq	%rcx
	retq
.Lfunc_end31:
	.size	_ZN7CCodecs7ReleaseEv, .Lfunc_end31-_ZN7CCodecs7ReleaseEv
	.cfi_endproc

	.section	.text._ZN7CCodecsD2Ev,"axG",@progbits,_ZN7CCodecsD2Ev,comdat
	.weak	_ZN7CCodecsD2Ev
	.p2align	4, 0x90
	.type	_ZN7CCodecsD2Ev,@function
_ZN7CCodecsD2Ev:                        # @_ZN7CCodecsD2Ev
.Lfunc_begin24:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception24
# BB#0:
	pushq	%r14
.Lcfi514:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi515:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi516:
	.cfi_def_cfa_offset 32
.Lcfi517:
	.cfi_offset %rbx, -24
.Lcfi518:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV7CCodecs+16, (%rbx)
	movq	$_ZTV13CObjectVectorI10CArcInfoExE+16, 16(%rbx)
	addq	$16, %rbx
.Ltmp885:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp886:
# BB#1:                                 # %_ZN13CObjectVectorI10CArcInfoExED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB32_3:
.Ltmp887:
	movq	%rax, %r14
.Ltmp888:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp889:
# BB#4:                                 # %.body
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB32_2:
.Ltmp890:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end32:
	.size	_ZN7CCodecsD2Ev, .Lfunc_end32-_ZN7CCodecsD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception24:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp885-.Lfunc_begin24 # >> Call Site 1 <<
	.long	.Ltmp886-.Ltmp885       #   Call between .Ltmp885 and .Ltmp886
	.long	.Ltmp887-.Lfunc_begin24 #     jumps to .Ltmp887
	.byte	0                       #   On action: cleanup
	.long	.Ltmp886-.Lfunc_begin24 # >> Call Site 2 <<
	.long	.Ltmp888-.Ltmp886       #   Call between .Ltmp886 and .Ltmp888
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp888-.Lfunc_begin24 # >> Call Site 3 <<
	.long	.Ltmp889-.Ltmp888       #   Call between .Ltmp888 and .Ltmp889
	.long	.Ltmp890-.Lfunc_begin24 #     jumps to .Ltmp890
	.byte	1                       #   On action: 1
	.long	.Ltmp889-.Lfunc_begin24 # >> Call Site 4 <<
	.long	.Lfunc_end32-.Ltmp889   #   Call between .Ltmp889 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN7CCodecsD0Ev,"axG",@progbits,_ZN7CCodecsD0Ev,comdat
	.weak	_ZN7CCodecsD0Ev
	.p2align	4, 0x90
	.type	_ZN7CCodecsD0Ev,@function
_ZN7CCodecsD0Ev:                        # @_ZN7CCodecsD0Ev
.Lfunc_begin25:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception25
# BB#0:
	pushq	%r15
.Lcfi519:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi520:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi521:
	.cfi_def_cfa_offset 32
.Lcfi522:
	.cfi_offset %rbx, -32
.Lcfi523:
	.cfi_offset %r14, -24
.Lcfi524:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	$_ZTV7CCodecs+16, (%rbx)
	leaq	16(%rbx), %r15
	movq	$_ZTV13CObjectVectorI10CArcInfoExE+16, 16(%rbx)
.Ltmp891:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp892:
# BB#1:                                 # %_ZN13CObjectVectorI10CArcInfoExED2Ev.exit.i
.Ltmp897:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp898:
# BB#2:                                 # %_ZN7CCodecsD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZdlPv                  # TAILCALL
.LBB33_5:
.Ltmp899:
	movq	%rax, %r14
	jmp	.LBB33_6
.LBB33_3:
.Ltmp893:
	movq	%rax, %r14
.Ltmp894:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp895:
.LBB33_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB33_4:
.Ltmp896:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end33:
	.size	_ZN7CCodecsD0Ev, .Lfunc_end33-_ZN7CCodecsD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception25:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp891-.Lfunc_begin25 # >> Call Site 1 <<
	.long	.Ltmp892-.Ltmp891       #   Call between .Ltmp891 and .Ltmp892
	.long	.Ltmp893-.Lfunc_begin25 #     jumps to .Ltmp893
	.byte	0                       #   On action: cleanup
	.long	.Ltmp897-.Lfunc_begin25 # >> Call Site 2 <<
	.long	.Ltmp898-.Ltmp897       #   Call between .Ltmp897 and .Ltmp898
	.long	.Ltmp899-.Lfunc_begin25 #     jumps to .Ltmp899
	.byte	0                       #   On action: cleanup
	.long	.Ltmp894-.Lfunc_begin25 # >> Call Site 3 <<
	.long	.Ltmp895-.Ltmp894       #   Call between .Ltmp894 and .Ltmp895
	.long	.Ltmp896-.Lfunc_begin25 #     jumps to .Ltmp896
	.byte	1                       #   On action: 1
	.long	.Ltmp895-.Lfunc_begin25 # >> Call Site 4 <<
	.long	.Lfunc_end33-.Ltmp895   #   Call between .Ltmp895 and .Lfunc_end33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI10CArcInfoExED2Ev,"axG",@progbits,_ZN13CObjectVectorI10CArcInfoExED2Ev,comdat
	.weak	_ZN13CObjectVectorI10CArcInfoExED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI10CArcInfoExED2Ev,@function
_ZN13CObjectVectorI10CArcInfoExED2Ev:   # @_ZN13CObjectVectorI10CArcInfoExED2Ev
.Lfunc_begin26:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception26
# BB#0:
	pushq	%r14
.Lcfi525:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi526:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi527:
	.cfi_def_cfa_offset 32
.Lcfi528:
	.cfi_offset %rbx, -24
.Lcfi529:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI10CArcInfoExE+16, (%rbx)
.Ltmp900:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp901:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB34_2:
.Ltmp902:
	movq	%rax, %r14
.Ltmp903:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp904:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB34_4:
.Ltmp905:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end34:
	.size	_ZN13CObjectVectorI10CArcInfoExED2Ev, .Lfunc_end34-_ZN13CObjectVectorI10CArcInfoExED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table34:
.Lexception26:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp900-.Lfunc_begin26 # >> Call Site 1 <<
	.long	.Ltmp901-.Ltmp900       #   Call between .Ltmp900 and .Ltmp901
	.long	.Ltmp902-.Lfunc_begin26 #     jumps to .Ltmp902
	.byte	0                       #   On action: cleanup
	.long	.Ltmp901-.Lfunc_begin26 # >> Call Site 2 <<
	.long	.Ltmp903-.Ltmp901       #   Call between .Ltmp901 and .Ltmp903
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp903-.Lfunc_begin26 # >> Call Site 3 <<
	.long	.Ltmp904-.Ltmp903       #   Call between .Ltmp903 and .Ltmp904
	.long	.Ltmp905-.Lfunc_begin26 #     jumps to .Ltmp905
	.byte	1                       #   On action: 1
	.long	.Ltmp904-.Lfunc_begin26 # >> Call Site 4 <<
	.long	.Lfunc_end34-.Ltmp904   #   Call between .Ltmp904 and .Lfunc_end34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI10CArcInfoExED0Ev,"axG",@progbits,_ZN13CObjectVectorI10CArcInfoExED0Ev,comdat
	.weak	_ZN13CObjectVectorI10CArcInfoExED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI10CArcInfoExED0Ev,@function
_ZN13CObjectVectorI10CArcInfoExED0Ev:   # @_ZN13CObjectVectorI10CArcInfoExED0Ev
.Lfunc_begin27:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception27
# BB#0:
	pushq	%r14
.Lcfi530:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi531:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi532:
	.cfi_def_cfa_offset 32
.Lcfi533:
	.cfi_offset %rbx, -24
.Lcfi534:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI10CArcInfoExE+16, (%rbx)
.Ltmp906:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp907:
# BB#1:
.Ltmp912:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp913:
# BB#2:                                 # %_ZN13CObjectVectorI10CArcInfoExED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB35_5:
.Ltmp914:
	movq	%rax, %r14
	jmp	.LBB35_6
.LBB35_3:
.Ltmp908:
	movq	%rax, %r14
.Ltmp909:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp910:
.LBB35_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB35_4:
.Ltmp911:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end35:
	.size	_ZN13CObjectVectorI10CArcInfoExED0Ev, .Lfunc_end35-_ZN13CObjectVectorI10CArcInfoExED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table35:
.Lexception27:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp906-.Lfunc_begin27 # >> Call Site 1 <<
	.long	.Ltmp907-.Ltmp906       #   Call between .Ltmp906 and .Ltmp907
	.long	.Ltmp908-.Lfunc_begin27 #     jumps to .Ltmp908
	.byte	0                       #   On action: cleanup
	.long	.Ltmp912-.Lfunc_begin27 # >> Call Site 2 <<
	.long	.Ltmp913-.Ltmp912       #   Call between .Ltmp912 and .Ltmp913
	.long	.Ltmp914-.Lfunc_begin27 #     jumps to .Ltmp914
	.byte	0                       #   On action: cleanup
	.long	.Ltmp909-.Lfunc_begin27 # >> Call Site 3 <<
	.long	.Ltmp910-.Ltmp909       #   Call between .Ltmp909 and .Ltmp910
	.long	.Ltmp911-.Lfunc_begin27 #     jumps to .Ltmp911
	.byte	1                       #   On action: 1
	.long	.Ltmp910-.Lfunc_begin27 # >> Call Site 4 <<
	.long	.Lfunc_end35-.Ltmp910   #   Call between .Ltmp910 and .Lfunc_end35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI10CArcInfoExE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI10CArcInfoExE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI10CArcInfoExE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI10CArcInfoExE6DeleteEii,@function
_ZN13CObjectVectorI10CArcInfoExE6DeleteEii: # @_ZN13CObjectVectorI10CArcInfoExE6DeleteEii
.Lfunc_begin28:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception28
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi535:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi536:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi537:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi538:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi539:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi540:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi541:
	.cfi_def_cfa_offset 80
.Lcfi542:
	.cfi_offset %rbx, -56
.Lcfi543:
	.cfi_offset %r12, -48
.Lcfi544:
	.cfi_offset %r13, -40
.Lcfi545:
	.cfi_offset %r14, -32
.Lcfi546:
	.cfi_offset %r15, -24
.Lcfi547:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %edi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	subl	%esi, %edi
	cmpl	%ecx, %eax
	cmovlel	%edx, %edi
	movl	%edi, 12(%rsp)          # 4-byte Spill
	testl	%edi, %edi
	jle	.LBB36_11
# BB#1:                                 # %.lr.ph
	movslq	16(%rsp), %r13          # 4-byte Folded Reload
	movslq	12(%rsp), %r14          # 4-byte Folded Reload
	shlq	$3, %r13
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB36_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%r13, %rax
	movq	(%rax,%r15,8), %rbp
	testq	%rbp, %rbp
	je	.LBB36_10
# BB#3:                                 #   in Loop: Header=BB36_2 Depth=1
	movq	$_ZTV7CBufferIhE+16, 72(%rbp)
	movq	88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB36_5
# BB#4:                                 #   in Loop: Header=BB36_2 Depth=1
	callq	_ZdaPv
.LBB36_5:                               # %_ZN7CBufferIhED2Ev.exit.i
                                        #   in Loop: Header=BB36_2 Depth=1
	leaq	40(%rbp), %rbx
	movq	$_ZTV13CObjectVectorI11CArcExtInfoE+16, 40(%rbp)
.Ltmp915:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp916:
# BB#6:                                 #   in Loop: Header=BB36_2 Depth=1
.Ltmp921:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp922:
# BB#7:                                 # %_ZN13CObjectVectorI11CArcExtInfoED2Ev.exit.i
                                        #   in Loop: Header=BB36_2 Depth=1
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB36_9
# BB#8:                                 #   in Loop: Header=BB36_2 Depth=1
	callq	_ZdaPv
.LBB36_9:                               # %_ZN10CArcInfoExD2Ev.exit
                                        #   in Loop: Header=BB36_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB36_10:                              #   in Loop: Header=BB36_2 Depth=1
	incq	%r15
	cmpq	%r14, %r15
	jl	.LBB36_2
.LBB36_11:                              # %._crit_edge
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	12(%rsp), %edx          # 4-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB36_12:
.Ltmp917:
	movq	%rax, %r14
.Ltmp918:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp919:
	jmp	.LBB36_15
.LBB36_13:
.Ltmp920:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB36_14:
.Ltmp923:
	movq	%rax, %r14
.LBB36_15:                              # %.body.i
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB36_17
# BB#16:
	callq	_ZdaPv
.LBB36_17:                              # %.body
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end36:
	.size	_ZN13CObjectVectorI10CArcInfoExE6DeleteEii, .Lfunc_end36-_ZN13CObjectVectorI10CArcInfoExE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table36:
.Lexception28:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp915-.Lfunc_begin28 # >> Call Site 1 <<
	.long	.Ltmp916-.Ltmp915       #   Call between .Ltmp915 and .Ltmp916
	.long	.Ltmp917-.Lfunc_begin28 #     jumps to .Ltmp917
	.byte	0                       #   On action: cleanup
	.long	.Ltmp921-.Lfunc_begin28 # >> Call Site 2 <<
	.long	.Ltmp922-.Ltmp921       #   Call between .Ltmp921 and .Ltmp922
	.long	.Ltmp923-.Lfunc_begin28 #     jumps to .Ltmp923
	.byte	0                       #   On action: cleanup
	.long	.Ltmp922-.Lfunc_begin28 # >> Call Site 3 <<
	.long	.Ltmp918-.Ltmp922       #   Call between .Ltmp922 and .Ltmp918
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp918-.Lfunc_begin28 # >> Call Site 4 <<
	.long	.Ltmp919-.Ltmp918       #   Call between .Ltmp918 and .Ltmp919
	.long	.Ltmp920-.Lfunc_begin28 #     jumps to .Ltmp920
	.byte	1                       #   On action: 1
	.long	.Ltmp919-.Lfunc_begin28 # >> Call Site 5 <<
	.long	.Lfunc_end36-.Ltmp919   #   Call between .Ltmp919 and .Lfunc_end36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN7CBufferIhED2Ev,"axG",@progbits,_ZN7CBufferIhED2Ev,comdat
	.weak	_ZN7CBufferIhED2Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED2Ev,@function
_ZN7CBufferIhED2Ev:                     # @_ZN7CBufferIhED2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV7CBufferIhE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB37_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB37_1:
	retq
.Lfunc_end37:
	.size	_ZN7CBufferIhED2Ev, .Lfunc_end37-_ZN7CBufferIhED2Ev
	.cfi_endproc

	.section	.text._ZN13CObjectVectorI11CArcExtInfoED2Ev,"axG",@progbits,_ZN13CObjectVectorI11CArcExtInfoED2Ev,comdat
	.weak	_ZN13CObjectVectorI11CArcExtInfoED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CArcExtInfoED2Ev,@function
_ZN13CObjectVectorI11CArcExtInfoED2Ev:  # @_ZN13CObjectVectorI11CArcExtInfoED2Ev
.Lfunc_begin29:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception29
# BB#0:
	pushq	%r14
.Lcfi548:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi549:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi550:
	.cfi_def_cfa_offset 32
.Lcfi551:
	.cfi_offset %rbx, -24
.Lcfi552:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CArcExtInfoE+16, (%rbx)
.Ltmp924:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp925:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB38_2:
.Ltmp926:
	movq	%rax, %r14
.Ltmp927:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp928:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB38_4:
.Ltmp929:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end38:
	.size	_ZN13CObjectVectorI11CArcExtInfoED2Ev, .Lfunc_end38-_ZN13CObjectVectorI11CArcExtInfoED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table38:
.Lexception29:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp924-.Lfunc_begin29 # >> Call Site 1 <<
	.long	.Ltmp925-.Ltmp924       #   Call between .Ltmp924 and .Ltmp925
	.long	.Ltmp926-.Lfunc_begin29 #     jumps to .Ltmp926
	.byte	0                       #   On action: cleanup
	.long	.Ltmp925-.Lfunc_begin29 # >> Call Site 2 <<
	.long	.Ltmp927-.Ltmp925       #   Call between .Ltmp925 and .Ltmp927
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp927-.Lfunc_begin29 # >> Call Site 3 <<
	.long	.Ltmp928-.Ltmp927       #   Call between .Ltmp927 and .Ltmp928
	.long	.Ltmp929-.Lfunc_begin29 #     jumps to .Ltmp929
	.byte	1                       #   On action: 1
	.long	.Ltmp928-.Lfunc_begin29 # >> Call Site 4 <<
	.long	.Lfunc_end38-.Ltmp928   #   Call between .Ltmp928 and .Lfunc_end38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN7CBufferIhED0Ev,"axG",@progbits,_ZN7CBufferIhED0Ev,comdat
	.weak	_ZN7CBufferIhED0Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED0Ev,@function
_ZN7CBufferIhED0Ev:                     # @_ZN7CBufferIhED0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi553:
	.cfi_def_cfa_offset 16
.Lcfi554:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV7CBufferIhE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB39_2
# BB#1:
	callq	_ZdaPv
.LBB39_2:                               # %_ZN7CBufferIhED2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end39:
	.size	_ZN7CBufferIhED0Ev, .Lfunc_end39-_ZN7CBufferIhED0Ev
	.cfi_endproc

	.section	.text._ZN13CObjectVectorI11CArcExtInfoED0Ev,"axG",@progbits,_ZN13CObjectVectorI11CArcExtInfoED0Ev,comdat
	.weak	_ZN13CObjectVectorI11CArcExtInfoED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CArcExtInfoED0Ev,@function
_ZN13CObjectVectorI11CArcExtInfoED0Ev:  # @_ZN13CObjectVectorI11CArcExtInfoED0Ev
.Lfunc_begin30:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception30
# BB#0:
	pushq	%r14
.Lcfi555:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi556:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi557:
	.cfi_def_cfa_offset 32
.Lcfi558:
	.cfi_offset %rbx, -24
.Lcfi559:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CArcExtInfoE+16, (%rbx)
.Ltmp930:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp931:
# BB#1:
.Ltmp936:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp937:
# BB#2:                                 # %_ZN13CObjectVectorI11CArcExtInfoED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB40_5:
.Ltmp938:
	movq	%rax, %r14
	jmp	.LBB40_6
.LBB40_3:
.Ltmp932:
	movq	%rax, %r14
.Ltmp933:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp934:
.LBB40_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB40_4:
.Ltmp935:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end40:
	.size	_ZN13CObjectVectorI11CArcExtInfoED0Ev, .Lfunc_end40-_ZN13CObjectVectorI11CArcExtInfoED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception30:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp930-.Lfunc_begin30 # >> Call Site 1 <<
	.long	.Ltmp931-.Ltmp930       #   Call between .Ltmp930 and .Ltmp931
	.long	.Ltmp932-.Lfunc_begin30 #     jumps to .Ltmp932
	.byte	0                       #   On action: cleanup
	.long	.Ltmp936-.Lfunc_begin30 # >> Call Site 2 <<
	.long	.Ltmp937-.Ltmp936       #   Call between .Ltmp936 and .Ltmp937
	.long	.Ltmp938-.Lfunc_begin30 #     jumps to .Ltmp938
	.byte	0                       #   On action: cleanup
	.long	.Ltmp933-.Lfunc_begin30 # >> Call Site 3 <<
	.long	.Ltmp934-.Ltmp933       #   Call between .Ltmp933 and .Ltmp934
	.long	.Ltmp935-.Lfunc_begin30 #     jumps to .Ltmp935
	.byte	1                       #   On action: 1
	.long	.Ltmp934-.Lfunc_begin30 # >> Call Site 4 <<
	.long	.Lfunc_end40-.Ltmp934   #   Call between .Ltmp934 and .Lfunc_end40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CArcExtInfoE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI11CArcExtInfoE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI11CArcExtInfoE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CArcExtInfoE6DeleteEii,@function
_ZN13CObjectVectorI11CArcExtInfoE6DeleteEii: # @_ZN13CObjectVectorI11CArcExtInfoE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi560:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi561:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi562:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi563:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi564:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi565:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi566:
	.cfi_def_cfa_offset 64
.Lcfi567:
	.cfi_offset %rbx, -56
.Lcfi568:
	.cfi_offset %r12, -48
.Lcfi569:
	.cfi_offset %r13, -40
.Lcfi570:
	.cfi_offset %r14, -32
.Lcfi571:
	.cfi_offset %r15, -24
.Lcfi572:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB41_9
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB41_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB41_8
# BB#3:                                 #   in Loop: Header=BB41_2 Depth=1
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB41_5
# BB#4:                                 #   in Loop: Header=BB41_2 Depth=1
	callq	_ZdaPv
.LBB41_5:                               # %_ZN11CStringBaseIwED2Ev.exit.i
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB41_7
# BB#6:                                 #   in Loop: Header=BB41_2 Depth=1
	callq	_ZdaPv
.LBB41_7:                               # %_ZN11CArcExtInfoD2Ev.exit
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB41_8:                               #   in Loop: Header=BB41_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB41_2
.LBB41_9:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end41:
	.size	_ZN13CObjectVectorI11CArcExtInfoE6DeleteEii, .Lfunc_end41-_ZN13CObjectVectorI11CArcExtInfoE6DeleteEii
	.cfi_endproc

	.section	.text._ZplIwE11CStringBaseIT_ERKS2_S4_,"axG",@progbits,_ZplIwE11CStringBaseIT_ERKS2_S4_,comdat
	.weak	_ZplIwE11CStringBaseIT_ERKS2_S4_
	.p2align	4, 0x90
	.type	_ZplIwE11CStringBaseIT_ERKS2_S4_,@function
_ZplIwE11CStringBaseIT_ERKS2_S4_:       # @_ZplIwE11CStringBaseIT_ERKS2_S4_
.Lfunc_begin31:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception31
# BB#0:
	pushq	%r15
.Lcfi573:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi574:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi575:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi576:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi577:
	.cfi_def_cfa_offset 48
.Lcfi578:
	.cfi_offset %rbx, -48
.Lcfi579:
	.cfi_offset %r12, -40
.Lcfi580:
	.cfi_offset %r13, -32
.Lcfi581:
	.cfi_offset %r14, -24
.Lcfi582:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	8(%r15), %r13
	leaq	1(%r13), %r12
	testl	%r12d, %r12d
	je	.LBB42_1
# BB#2:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%r12d, 12(%rbx)
	jmp	.LBB42_3
.LBB42_1:
	xorl	%eax, %eax
.LBB42_3:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB42_4:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB42_4
# BB#5:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit
	movl	%r13d, 8(%rbx)
	movl	8(%r14), %esi
.Ltmp939:
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp940:
# BB#6:                                 # %.noexc
	movslq	8(%rbx), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%rbx), %rcx
	movq	(%r14), %rsi
	.p2align	4, 0x90
.LBB42_7:                               # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %edx
	addq	$4, %rsi
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB42_7
# BB#8:
	movl	8(%r14), %ecx
	addl	%eax, %ecx
	movl	%ecx, 8(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB42_9:
.Ltmp941:
	movq	%rax, %r14
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB42_11
# BB#10:
	callq	_ZdaPv
.LBB42_11:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end42:
	.size	_ZplIwE11CStringBaseIT_ERKS2_S4_, .Lfunc_end42-_ZplIwE11CStringBaseIT_ERKS2_S4_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table42:
.Lexception31:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin31-.Lfunc_begin31 # >> Call Site 1 <<
	.long	.Ltmp939-.Lfunc_begin31 #   Call between .Lfunc_begin31 and .Ltmp939
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp939-.Lfunc_begin31 # >> Call Site 2 <<
	.long	.Ltmp940-.Ltmp939       #   Call between .Ltmp939 and .Ltmp940
	.long	.Ltmp941-.Lfunc_begin31 #     jumps to .Ltmp941
	.byte	0                       #   On action: cleanup
	.long	.Ltmp940-.Lfunc_begin31 # >> Call Site 3 <<
	.long	.Lfunc_end42-.Ltmp940   #   Call between .Ltmp940 and .Lfunc_end42
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED0Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED0Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED0Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED0Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED0Ev
.Lfunc_begin32:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception32
# BB#0:
	pushq	%r14
.Lcfi583:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi584:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi585:
	.cfi_def_cfa_offset 32
.Lcfi586:
	.cfi_offset %rbx, -24
.Lcfi587:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp942:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp943:
# BB#1:
.Ltmp948:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp949:
# BB#2:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB43_5:
.Ltmp950:
	movq	%rax, %r14
	jmp	.LBB43_6
.LBB43_3:
.Ltmp944:
	movq	%rax, %r14
.Ltmp945:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp946:
.LBB43_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB43_4:
.Ltmp947:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end43:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED0Ev, .Lfunc_end43-_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table43:
.Lexception32:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp942-.Lfunc_begin32 # >> Call Site 1 <<
	.long	.Ltmp943-.Ltmp942       #   Call between .Ltmp942 and .Ltmp943
	.long	.Ltmp944-.Lfunc_begin32 #     jumps to .Ltmp944
	.byte	0                       #   On action: cleanup
	.long	.Ltmp948-.Lfunc_begin32 # >> Call Site 2 <<
	.long	.Ltmp949-.Ltmp948       #   Call between .Ltmp948 and .Ltmp949
	.long	.Ltmp950-.Lfunc_begin32 #     jumps to .Ltmp950
	.byte	0                       #   On action: cleanup
	.long	.Ltmp945-.Lfunc_begin32 # >> Call Site 3 <<
	.long	.Ltmp946-.Ltmp945       #   Call between .Ltmp945 and .Ltmp946
	.long	.Ltmp947-.Lfunc_begin32 #     jumps to .Ltmp947
	.byte	1                       #   On action: 1
	.long	.Ltmp946-.Lfunc_begin32 # >> Call Site 4 <<
	.long	.Lfunc_end43-.Ltmp946   #   Call between .Ltmp946 and .Lfunc_end43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN11CStringBaseIwE10GrowLengthEi,"axG",@progbits,_ZN11CStringBaseIwE10GrowLengthEi,comdat
	.weak	_ZN11CStringBaseIwE10GrowLengthEi
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE10GrowLengthEi,@function
_ZN11CStringBaseIwE10GrowLengthEi:      # @_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi588:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi589:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi590:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi591:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi592:
	.cfi_def_cfa_offset 48
.Lcfi593:
	.cfi_offset %rbx, -48
.Lcfi594:
	.cfi_offset %r12, -40
.Lcfi595:
	.cfi_offset %r14, -32
.Lcfi596:
	.cfi_offset %r15, -24
.Lcfi597:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	8(%r14), %ebp
	movl	12(%r14), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	%esi, %eax
	jg	.LBB44_28
# BB#1:
	decl	%eax
	cmpl	$8, %ebx
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebx
	jl	.LBB44_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
.LBB44_3:                               # %select.end
	movl	%esi, %edx
	subl	%eax, %edx
	addl	%ecx, %eax
	cmpl	%esi, %eax
	cmovgel	%ecx, %edx
	leal	1(%rbx,%rdx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB44_28
# BB#4:
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB44_27
# BB#5:                                 # %.preheader.i
	movq	(%r14), %rdi
	testl	%ebp, %ebp
	jle	.LBB44_25
# BB#6:                                 # %.lr.ph.i
	movslq	%ebp, %rax
	cmpl	$7, %ebp
	jbe	.LBB44_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB44_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r12
	jae	.LBB44_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB44_17
.LBB44_7:
	xorl	%ecx, %ecx
.LBB44_8:                               # %scalar.ph.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB44_11
# BB#9:                                 # %scalar.ph.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB44_10:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %esi
	movl	%esi, (%r12,%rcx,4)
	incq	%rcx
	incq	%rbp
	jne	.LBB44_10
.LBB44_11:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB44_26
# BB#12:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%r12,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB44_13:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB44_13
	jmp	.LBB44_26
.LBB44_25:                              # %._crit_edge.i
	testq	%rdi, %rdi
	je	.LBB44_27
.LBB44_26:                              # %._crit_edge.thread.i
	callq	_ZdaPv
	movl	8(%r14), %ebp
.LBB44_27:                              # %._crit_edge16.i
	movq	%r12, (%r14)
	movslq	%ebp, %rax
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
.LBB44_28:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB44_17:                              # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB44_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB44_20:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r12,%rbx,4)
	movups	%xmm1, 16(%r12,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB44_20
	jmp	.LBB44_21
.LBB44_18:
	xorl	%ebx, %ebx
.LBB44_21:                              # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB44_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx,4), %rsi
	leaq	112(%rdi,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB44_23:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB44_23
.LBB44_24:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB44_8
	jmp	.LBB44_26
.Lfunc_end44:
	.size	_ZN11CStringBaseIwE10GrowLengthEi, .Lfunc_end44-_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIiED0Ev,"axG",@progbits,_ZN13CRecordVectorIiED0Ev,comdat
	.weak	_ZN13CRecordVectorIiED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIiED0Ev,@function
_ZN13CRecordVectorIiED0Ev:              # @_ZN13CRecordVectorIiED0Ev
.Lfunc_begin33:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception33
# BB#0:
	pushq	%r14
.Lcfi598:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi599:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi600:
	.cfi_def_cfa_offset 32
.Lcfi601:
	.cfi_offset %rbx, -24
.Lcfi602:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp951:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp952:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB45_2:
.Ltmp953:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end45:
	.size	_ZN13CRecordVectorIiED0Ev, .Lfunc_end45-_ZN13CRecordVectorIiED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table45:
.Lexception33:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp951-.Lfunc_begin33 # >> Call Site 1 <<
	.long	.Ltmp952-.Ltmp951       #   Call between .Ltmp951 and .Ltmp952
	.long	.Ltmp953-.Lfunc_begin33 #     jumps to .Ltmp953
	.byte	0                       #   On action: cleanup
	.long	.Ltmp952-.Lfunc_begin33 # >> Call Site 2 <<
	.long	.Lfunc_end45-.Ltmp952   #   Call between .Ltmp952 and .Lfunc_end45
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	IID_IUnknown,@object    # @IID_IUnknown
	.section	.rodata,"a",@progbits
	.globl	IID_IUnknown
	.p2align	2
IID_IUnknown:
	.long	0                       # 0x0
	.short	0                       # 0x0
	.short	0                       # 0x0
	.ascii	"\300\000\000\000\000\000\000F"
	.size	IID_IUnknown, 16

	.type	IID_IProgress,@object   # @IID_IProgress
	.globl	IID_IProgress
	.p2align	2
IID_IProgress:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\000\000\005\000"
	.size	IID_IProgress, 16

	.type	IID_ISequentialInStream,@object # @IID_ISequentialInStream
	.globl	IID_ISequentialInStream
	.p2align	2
IID_ISequentialInStream:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\003\000\001\000"
	.size	IID_ISequentialInStream, 16

	.type	IID_ISequentialOutStream,@object # @IID_ISequentialOutStream
	.globl	IID_ISequentialOutStream
	.p2align	2
IID_ISequentialOutStream:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\003\000\002\000"
	.size	IID_ISequentialOutStream, 16

	.type	IID_IInStream,@object   # @IID_IInStream
	.globl	IID_IInStream
	.p2align	2
IID_IInStream:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\003\000\003\000"
	.size	IID_IInStream, 16

	.type	IID_IOutStream,@object  # @IID_IOutStream
	.globl	IID_IOutStream
	.p2align	2
IID_IOutStream:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\003\000\004\000"
	.size	IID_IOutStream, 16

	.type	IID_IStreamGetSize,@object # @IID_IStreamGetSize
	.globl	IID_IStreamGetSize
	.p2align	2
IID_IStreamGetSize:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\003\000\006\000"
	.size	IID_IStreamGetSize, 16

	.type	IID_IOutStreamFlush,@object # @IID_IOutStreamFlush
	.globl	IID_IOutStreamFlush
	.p2align	2
IID_IOutStreamFlush:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\003\000\007\000"
	.size	IID_IOutStreamFlush, 16

	.type	IID_IArchiveOpenCallback,@object # @IID_IArchiveOpenCallback
	.globl	IID_IArchiveOpenCallback
	.p2align	2
IID_IArchiveOpenCallback:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\006\000\020\000"
	.size	IID_IArchiveOpenCallback, 16

	.type	IID_IArchiveExtractCallback,@object # @IID_IArchiveExtractCallback
	.globl	IID_IArchiveExtractCallback
	.p2align	2
IID_IArchiveExtractCallback:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\006\000 \000"
	.size	IID_IArchiveExtractCallback, 16

	.type	IID_IArchiveOpenVolumeCallback,@object # @IID_IArchiveOpenVolumeCallback
	.globl	IID_IArchiveOpenVolumeCallback
	.p2align	2
IID_IArchiveOpenVolumeCallback:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\006\0000\000"
	.size	IID_IArchiveOpenVolumeCallback, 16

	.type	IID_IInArchiveGetStream,@object # @IID_IInArchiveGetStream
	.globl	IID_IInArchiveGetStream
	.p2align	2
IID_IInArchiveGetStream:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\006\000@\000"
	.size	IID_IInArchiveGetStream, 16

	.type	IID_IArchiveOpenSetSubArchiveName,@object # @IID_IArchiveOpenSetSubArchiveName
	.globl	IID_IArchiveOpenSetSubArchiveName
	.p2align	2
IID_IArchiveOpenSetSubArchiveName:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\006\000P\000"
	.size	IID_IArchiveOpenSetSubArchiveName, 16

	.type	IID_IInArchive,@object  # @IID_IInArchive
	.globl	IID_IInArchive
	.p2align	2
IID_IInArchive:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\006\000`\000"
	.size	IID_IInArchive, 16

	.type	IID_IArchiveOpenSeq,@object # @IID_IArchiveOpenSeq
	.globl	IID_IArchiveOpenSeq
	.p2align	2
IID_IArchiveOpenSeq:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\006\000a\000"
	.size	IID_IArchiveOpenSeq, 16

	.type	IID_IArchiveUpdateCallback,@object # @IID_IArchiveUpdateCallback
	.globl	IID_IArchiveUpdateCallback
	.p2align	2
IID_IArchiveUpdateCallback:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\006\000\200\000"
	.size	IID_IArchiveUpdateCallback, 16

	.type	IID_IArchiveUpdateCallback2,@object # @IID_IArchiveUpdateCallback2
	.globl	IID_IArchiveUpdateCallback2
	.p2align	2
IID_IArchiveUpdateCallback2:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\006\000\202\000"
	.size	IID_IArchiveUpdateCallback2, 16

	.type	IID_IOutArchive,@object # @IID_IOutArchive
	.globl	IID_IOutArchive
	.p2align	2
IID_IOutArchive:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\006\000\240\000"
	.size	IID_IOutArchive, 16

	.type	IID_ISetProperties,@object # @IID_ISetProperties
	.globl	IID_ISetProperties
	.p2align	2
IID_ISetProperties:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\006\000\003\000"
	.size	IID_ISetProperties, 16

	.type	IID_ICryptoGetTextPassword,@object # @IID_ICryptoGetTextPassword
	.globl	IID_ICryptoGetTextPassword
	.p2align	2
IID_ICryptoGetTextPassword:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\005\000\020\000"
	.size	IID_ICryptoGetTextPassword, 16

	.type	IID_ICryptoGetTextPassword2,@object # @IID_ICryptoGetTextPassword2
	.globl	IID_ICryptoGetTextPassword2
	.p2align	2
IID_ICryptoGetTextPassword2:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\005\000\021\000"
	.size	IID_ICryptoGetTextPassword2, 16

	.type	IID_ICompressProgressInfo,@object # @IID_ICompressProgressInfo
	.globl	IID_ICompressProgressInfo
	.p2align	2
IID_ICompressProgressInfo:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\000\004\000"
	.size	IID_ICompressProgressInfo, 16

	.type	IID_ICompressCoder,@object # @IID_ICompressCoder
	.globl	IID_ICompressCoder
	.p2align	2
IID_ICompressCoder:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\000\005\000"
	.size	IID_ICompressCoder, 16

	.type	IID_ICompressCoder2,@object # @IID_ICompressCoder2
	.globl	IID_ICompressCoder2
	.p2align	2
IID_ICompressCoder2:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\000\030\000"
	.size	IID_ICompressCoder2, 16

	.type	IID_ICompressSetCoderProperties,@object # @IID_ICompressSetCoderProperties
	.globl	IID_ICompressSetCoderProperties
	.p2align	2
IID_ICompressSetCoderProperties:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\000 \000"
	.size	IID_ICompressSetCoderProperties, 16

	.type	IID_ICompressSetDecoderProperties2,@object # @IID_ICompressSetDecoderProperties2
	.globl	IID_ICompressSetDecoderProperties2
	.p2align	2
IID_ICompressSetDecoderProperties2:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\000\"\000"
	.size	IID_ICompressSetDecoderProperties2, 16

	.type	IID_ICompressWriteCoderProperties,@object # @IID_ICompressWriteCoderProperties
	.globl	IID_ICompressWriteCoderProperties
	.p2align	2
IID_ICompressWriteCoderProperties:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\000#\000"
	.size	IID_ICompressWriteCoderProperties, 16

	.type	IID_ICompressGetInStreamProcessedSize,@object # @IID_ICompressGetInStreamProcessedSize
	.globl	IID_ICompressGetInStreamProcessedSize
	.p2align	2
IID_ICompressGetInStreamProcessedSize:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\000$\000"
	.size	IID_ICompressGetInStreamProcessedSize, 16

	.type	IID_ICompressSetCoderMt,@object # @IID_ICompressSetCoderMt
	.globl	IID_ICompressSetCoderMt
	.p2align	2
IID_ICompressSetCoderMt:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\000%\000"
	.size	IID_ICompressSetCoderMt, 16

	.type	IID_ICompressGetSubStreamSize,@object # @IID_ICompressGetSubStreamSize
	.globl	IID_ICompressGetSubStreamSize
	.p2align	2
IID_ICompressGetSubStreamSize:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\0000\000"
	.size	IID_ICompressGetSubStreamSize, 16

	.type	IID_ICompressSetInStream,@object # @IID_ICompressSetInStream
	.globl	IID_ICompressSetInStream
	.p2align	2
IID_ICompressSetInStream:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\0001\000"
	.size	IID_ICompressSetInStream, 16

	.type	IID_ICompressSetOutStream,@object # @IID_ICompressSetOutStream
	.globl	IID_ICompressSetOutStream
	.p2align	2
IID_ICompressSetOutStream:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\0002\000"
	.size	IID_ICompressSetOutStream, 16

	.type	IID_ICompressSetInStreamSize,@object # @IID_ICompressSetInStreamSize
	.globl	IID_ICompressSetInStreamSize
	.p2align	2
IID_ICompressSetInStreamSize:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\0003\000"
	.size	IID_ICompressSetInStreamSize, 16

	.type	IID_ICompressSetOutStreamSize,@object # @IID_ICompressSetOutStreamSize
	.globl	IID_ICompressSetOutStreamSize
	.p2align	2
IID_ICompressSetOutStreamSize:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\0004\000"
	.size	IID_ICompressSetOutStreamSize, 16

	.type	IID_ICompressSetBufSize,@object # @IID_ICompressSetBufSize
	.globl	IID_ICompressSetBufSize
	.p2align	2
IID_ICompressSetBufSize:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\0005\000"
	.size	IID_ICompressSetBufSize, 16

	.type	IID_ICompressFilter,@object # @IID_ICompressFilter
	.globl	IID_ICompressFilter
	.p2align	2
IID_ICompressFilter:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\000@\000"
	.size	IID_ICompressFilter, 16

	.type	IID_ICompressCodecsInfo,@object # @IID_ICompressCodecsInfo
	.globl	IID_ICompressCodecsInfo
	.p2align	2
IID_ICompressCodecsInfo:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\000`\000"
	.size	IID_ICompressCodecsInfo, 16

	.type	IID_ISetCompressCodecsInfo,@object # @IID_ISetCompressCodecsInfo
	.globl	IID_ISetCompressCodecsInfo
	.p2align	2
IID_ISetCompressCodecsInfo:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\000a\000"
	.size	IID_ISetCompressCodecsInfo, 16

	.type	IID_ICryptoProperties,@object # @IID_ICryptoProperties
	.globl	IID_ICryptoProperties
	.p2align	2
IID_ICryptoProperties:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\000\200\000"
	.size	IID_ICryptoProperties, 16

	.type	IID_ICryptoResetInitVector,@object # @IID_ICryptoResetInitVector
	.globl	IID_ICryptoResetInitVector
	.p2align	2
IID_ICryptoResetInitVector:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\000\214\000"
	.size	IID_ICryptoResetInitVector, 16

	.type	IID_ICryptoSetPassword,@object # @IID_ICryptoSetPassword
	.globl	IID_ICryptoSetPassword
	.p2align	2
IID_ICryptoSetPassword:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\000\220\000"
	.size	IID_ICryptoSetPassword, 16

	.type	IID_ICryptoSetCRC,@object # @IID_ICryptoSetCRC
	.globl	IID_ICryptoSetCRC
	.p2align	2
IID_ICryptoSetCRC:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\004\000\240\000"
	.size	IID_ICryptoSetCRC, 16

	.type	IID_IFolderArchiveExtractCallback,@object # @IID_IFolderArchiveExtractCallback
	.globl	IID_IFolderArchiveExtractCallback
	.p2align	2
IID_IFolderArchiveExtractCallback:
	.long	588713833               # 0x23170f69
	.short	16577                   # 0x40c1
	.short	10122                   # 0x278a
	.asciz	"\000\000\000\001\000\007\000"
	.size	IID_IFolderArchiveExtractCallback, 16

	.type	_ZTS16CSystemException,@object # @_ZTS16CSystemException
	.section	.rodata._ZTS16CSystemException,"aG",@progbits,_ZTS16CSystemException,comdat
	.weak	_ZTS16CSystemException
	.p2align	4
_ZTS16CSystemException:
	.asciz	"16CSystemException"
	.size	_ZTS16CSystemException, 19

	.type	_ZTI16CSystemException,@object # @_ZTI16CSystemException
	.section	.rodata._ZTI16CSystemException,"aG",@progbits,_ZTI16CSystemException,comdat
	.weak	_ZTI16CSystemException
	.p2align	3
_ZTI16CSystemException:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16CSystemException
	.size	_ZTI16CSystemException, 16

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Formats:"
	.size	.L.str, 9

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"  "
	.size	.L.str.1, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Codecs:"
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.4:
	.long	67                      # 0x43
	.long	82                      # 0x52
	.long	67                      # 0x43
	.long	0                       # 0x0
	.size	.L.str.4, 16

	.type	.L.str.5,@object        # @.str.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.5:
	.asciz	"\nCRC Error\n"
	.size	.L.str.5, 12

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\nDecoding Error\n"
	.size	.L.str.6, 17

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Error: "
	.size	.L.str.7, 8

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Archives: "
	.size	.L.str.8, 11

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Archive Errors: "
	.size	.L.str.9, 17

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Sub items Errors: "
	.size	.L.str.10, 19

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Folders: "
	.size	.L.str.11, 10

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Files: "
	.size	.L.str.12, 8

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Size:       "
	.size	.L.str.13, 13

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Compressed: "
	.size	.L.str.14, 13

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"CRC: "
	.size	.L.str.15, 6

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Errors: "
	.size	.L.str.16, 9

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"WARNINGS for files:"
	.size	.L.str.17, 20

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	" : "
	.size	.L.str.18, 4

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"----------------"
	.size	.L.str.19, 17

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"WARNING: Cannot find "
	.size	.L.str.20, 22

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	" file"
	.size	.L.str.21, 6

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"s"
	.size	.L.str.22, 2

	.type	.L.str.24,@object       # @.str.24
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.24:
	.long	10                      # 0xa
	.long	69                      # 0x45
	.long	114                     # 0x72
	.long	114                     # 0x72
	.long	111                     # 0x6f
	.long	114                     # 0x72
	.long	58                      # 0x3a
	.long	10                      # 0xa
	.long	0                       # 0x0
	.size	.L.str.24, 36

	.type	.L.str.25,@object       # @.str.25
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.25:
	.asciz	"WARNING: Cannot open "
	.size	.L.str.25, 22

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	" (locale="
	.size	.L.str.26, 10

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	",Utf16="
	.size	.L.str.27, 8

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"on"
	.size	.L.str.28, 3

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"off"
	.size	.L.str.29, 4

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	",HugeFiles="
	.size	.L.str.30, 12

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"on,"
	.size	.L.str.31, 4

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	" CPUs)\n"
	.size	.L.str.32, 8

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	" CPU)\n"
	.size	.L.str.33, 7

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"\n7-Zip (A) [64] 9.20  Copyright (c) 1999-2010 Igor Pavlov  2010-11-18\np7zip Version 9.20"
	.size	.L.str.34, 89

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"\nUsage: 7za <command> [<switches>...] <archive_name> [<file_names>...]\n       [<@listfiles...>]\n\n<Commands>\n  a: Add files to archive\n  b: Benchmark\n  d: Delete files from archive\n  e: Extract files from archive (without using directory names)\n  l: List contents of archive\n  t: Test integrity of archive\n  u: Update files to archive\n  x: eXtract files with full paths\n<Switches>\n  -ai[r[-|0]]{@listfile|!wildcard}: Include archives\n  -ax[r[-|0]]{@listfile|!wildcard}: eXclude archives\n  -bd: Disable percentage indicator\n  -i[r[-|0]]{@listfile|!wildcard}: Include filenames\n  -m{Parameters}: set compression Method\n  -o{Directory}: set Output directory\n  -p{Password}: set Password\n  -r[-|0]: Recurse subdirectories\n  -scs{UTF-8 | WIN | DOS}: set charset for list files\n  -sfx[{name}]: Create SFX archive\n  -si[{name}]: read data from stdin\n  -slt: show technical information for l (List) command\n  -so: write data to stdout\n  -ssc[-]: set sensitive case mode\n  -t{Type}: Set type of archive\n  -u[-][p#][q#][r#][x#][y#][z#][!newArchiveName]: Update options\n  -v{Size}[b|k|m|g]: Create volumes\n  -w[{path}]: assign Work directory. Empty path means a temporary directory\n  -x[r[-|0]]]{@listfile|!wildcard}: eXclude filenames\n  -y: assume Yes on all queries\n"
	.size	.L.str.35, 1257

	.type	_ZTV13CObjectVectorIN9NWildcard5CPairEE,@object # @_ZTV13CObjectVectorIN9NWildcard5CPairEE
	.section	.rodata._ZTV13CObjectVectorIN9NWildcard5CPairEE,"aG",@progbits,_ZTV13CObjectVectorIN9NWildcard5CPairEE,comdat
	.weak	_ZTV13CObjectVectorIN9NWildcard5CPairEE
	.p2align	3
_ZTV13CObjectVectorIN9NWildcard5CPairEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN9NWildcard5CPairEE
	.quad	_ZN13CObjectVectorIN9NWildcard5CPairEED2Ev
	.quad	_ZN13CObjectVectorIN9NWildcard5CPairEED0Ev
	.quad	_ZN13CObjectVectorIN9NWildcard5CPairEE6DeleteEii
	.size	_ZTV13CObjectVectorIN9NWildcard5CPairEE, 40

	.type	_ZTS13CObjectVectorIN9NWildcard5CPairEE,@object # @_ZTS13CObjectVectorIN9NWildcard5CPairEE
	.section	.rodata._ZTS13CObjectVectorIN9NWildcard5CPairEE,"aG",@progbits,_ZTS13CObjectVectorIN9NWildcard5CPairEE,comdat
	.weak	_ZTS13CObjectVectorIN9NWildcard5CPairEE
	.p2align	4
_ZTS13CObjectVectorIN9NWildcard5CPairEE:
	.asciz	"13CObjectVectorIN9NWildcard5CPairEE"
	.size	_ZTS13CObjectVectorIN9NWildcard5CPairEE, 36

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN9NWildcard5CPairEE,@object # @_ZTI13CObjectVectorIN9NWildcard5CPairEE
	.section	.rodata._ZTI13CObjectVectorIN9NWildcard5CPairEE,"aG",@progbits,_ZTI13CObjectVectorIN9NWildcard5CPairEE,comdat
	.weak	_ZTI13CObjectVectorIN9NWildcard5CPairEE
	.p2align	4
_ZTI13CObjectVectorIN9NWildcard5CPairEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN9NWildcard5CPairEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN9NWildcard5CPairEE, 24

	.type	_ZTV13CObjectVectorIN9NWildcard5CItemEE,@object # @_ZTV13CObjectVectorIN9NWildcard5CItemEE
	.section	.rodata._ZTV13CObjectVectorIN9NWildcard5CItemEE,"aG",@progbits,_ZTV13CObjectVectorIN9NWildcard5CItemEE,comdat
	.weak	_ZTV13CObjectVectorIN9NWildcard5CItemEE
	.p2align	3
_ZTV13CObjectVectorIN9NWildcard5CItemEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN9NWildcard5CItemEE
	.quad	_ZN13CObjectVectorIN9NWildcard5CItemEED2Ev
	.quad	_ZN13CObjectVectorIN9NWildcard5CItemEED0Ev
	.quad	_ZN13CObjectVectorIN9NWildcard5CItemEE6DeleteEii
	.size	_ZTV13CObjectVectorIN9NWildcard5CItemEE, 40

	.type	_ZTS13CObjectVectorIN9NWildcard5CItemEE,@object # @_ZTS13CObjectVectorIN9NWildcard5CItemEE
	.section	.rodata._ZTS13CObjectVectorIN9NWildcard5CItemEE,"aG",@progbits,_ZTS13CObjectVectorIN9NWildcard5CItemEE,comdat
	.weak	_ZTS13CObjectVectorIN9NWildcard5CItemEE
	.p2align	4
_ZTS13CObjectVectorIN9NWildcard5CItemEE:
	.asciz	"13CObjectVectorIN9NWildcard5CItemEE"
	.size	_ZTS13CObjectVectorIN9NWildcard5CItemEE, 36

	.type	_ZTI13CObjectVectorIN9NWildcard5CItemEE,@object # @_ZTI13CObjectVectorIN9NWildcard5CItemEE
	.section	.rodata._ZTI13CObjectVectorIN9NWildcard5CItemEE,"aG",@progbits,_ZTI13CObjectVectorIN9NWildcard5CItemEE,comdat
	.weak	_ZTI13CObjectVectorIN9NWildcard5CItemEE
	.p2align	4
_ZTI13CObjectVectorIN9NWildcard5CItemEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN9NWildcard5CItemEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN9NWildcard5CItemEE, 24

	.type	_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE,@object # @_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE
	.section	.rodata._ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE,"aG",@progbits,_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE,comdat
	.weak	_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE
	.p2align	3
_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE
	.quad	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED2Ev
	.quad	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEED0Ev
	.quad	_ZN13CObjectVectorIN9NWildcard11CCensorNodeEE6DeleteEii
	.size	_ZTV13CObjectVectorIN9NWildcard11CCensorNodeEE, 40

	.type	_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE,@object # @_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE
	.section	.rodata._ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE,"aG",@progbits,_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE,comdat
	.weak	_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE
	.p2align	4
_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE:
	.asciz	"13CObjectVectorIN9NWildcard11CCensorNodeEE"
	.size	_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE, 43

	.type	_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE,@object # @_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE
	.section	.rodata._ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE,"aG",@progbits,_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE,comdat
	.weak	_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE
	.p2align	4
_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN9NWildcard11CCensorNodeEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN9NWildcard11CCensorNodeEE, 24

	.type	_ZTV13CObjectVectorI9CPropertyE,@object # @_ZTV13CObjectVectorI9CPropertyE
	.section	.rodata._ZTV13CObjectVectorI9CPropertyE,"aG",@progbits,_ZTV13CObjectVectorI9CPropertyE,comdat
	.weak	_ZTV13CObjectVectorI9CPropertyE
	.p2align	3
_ZTV13CObjectVectorI9CPropertyE:
	.quad	0
	.quad	_ZTI13CObjectVectorI9CPropertyE
	.quad	_ZN13CObjectVectorI9CPropertyED2Ev
	.quad	_ZN13CObjectVectorI9CPropertyED0Ev
	.quad	_ZN13CObjectVectorI9CPropertyE6DeleteEii
	.size	_ZTV13CObjectVectorI9CPropertyE, 40

	.type	_ZTS13CObjectVectorI9CPropertyE,@object # @_ZTS13CObjectVectorI9CPropertyE
	.section	.rodata._ZTS13CObjectVectorI9CPropertyE,"aG",@progbits,_ZTS13CObjectVectorI9CPropertyE,comdat
	.weak	_ZTS13CObjectVectorI9CPropertyE
	.p2align	4
_ZTS13CObjectVectorI9CPropertyE:
	.asciz	"13CObjectVectorI9CPropertyE"
	.size	_ZTS13CObjectVectorI9CPropertyE, 28

	.type	_ZTI13CObjectVectorI9CPropertyE,@object # @_ZTI13CObjectVectorI9CPropertyE
	.section	.rodata._ZTI13CObjectVectorI9CPropertyE,"aG",@progbits,_ZTI13CObjectVectorI9CPropertyE,comdat
	.weak	_ZTI13CObjectVectorI9CPropertyE
	.p2align	4
_ZTI13CObjectVectorI9CPropertyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI9CPropertyE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI9CPropertyE, 24

	.type	_ZTV13CObjectVectorI21CUpdateArchiveCommandE,@object # @_ZTV13CObjectVectorI21CUpdateArchiveCommandE
	.section	.rodata._ZTV13CObjectVectorI21CUpdateArchiveCommandE,"aG",@progbits,_ZTV13CObjectVectorI21CUpdateArchiveCommandE,comdat
	.weak	_ZTV13CObjectVectorI21CUpdateArchiveCommandE
	.p2align	3
_ZTV13CObjectVectorI21CUpdateArchiveCommandE:
	.quad	0
	.quad	_ZTI13CObjectVectorI21CUpdateArchiveCommandE
	.quad	_ZN13CObjectVectorI21CUpdateArchiveCommandED2Ev
	.quad	_ZN13CObjectVectorI21CUpdateArchiveCommandED0Ev
	.quad	_ZN13CObjectVectorI21CUpdateArchiveCommandE6DeleteEii
	.size	_ZTV13CObjectVectorI21CUpdateArchiveCommandE, 40

	.type	_ZTS13CObjectVectorI21CUpdateArchiveCommandE,@object # @_ZTS13CObjectVectorI21CUpdateArchiveCommandE
	.section	.rodata._ZTS13CObjectVectorI21CUpdateArchiveCommandE,"aG",@progbits,_ZTS13CObjectVectorI21CUpdateArchiveCommandE,comdat
	.weak	_ZTS13CObjectVectorI21CUpdateArchiveCommandE
	.p2align	4
_ZTS13CObjectVectorI21CUpdateArchiveCommandE:
	.asciz	"13CObjectVectorI21CUpdateArchiveCommandE"
	.size	_ZTS13CObjectVectorI21CUpdateArchiveCommandE, 41

	.type	_ZTI13CObjectVectorI21CUpdateArchiveCommandE,@object # @_ZTI13CObjectVectorI21CUpdateArchiveCommandE
	.section	.rodata._ZTI13CObjectVectorI21CUpdateArchiveCommandE,"aG",@progbits,_ZTI13CObjectVectorI21CUpdateArchiveCommandE,comdat
	.weak	_ZTI13CObjectVectorI21CUpdateArchiveCommandE
	.p2align	4
_ZTI13CObjectVectorI21CUpdateArchiveCommandE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI21CUpdateArchiveCommandE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI21CUpdateArchiveCommandE, 24

	.type	_ZTV13CRecordVectorIyE,@object # @_ZTV13CRecordVectorIyE
	.section	.rodata._ZTV13CRecordVectorIyE,"aG",@progbits,_ZTV13CRecordVectorIyE,comdat
	.weak	_ZTV13CRecordVectorIyE
	.p2align	3
_ZTV13CRecordVectorIyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIyE, 40

	.type	_ZTS13CRecordVectorIyE,@object # @_ZTS13CRecordVectorIyE
	.section	.rodata._ZTS13CRecordVectorIyE,"aG",@progbits,_ZTS13CRecordVectorIyE,comdat
	.weak	_ZTS13CRecordVectorIyE
	.p2align	4
_ZTS13CRecordVectorIyE:
	.asciz	"13CRecordVectorIyE"
	.size	_ZTS13CRecordVectorIyE, 19

	.type	_ZTI13CRecordVectorIyE,@object # @_ZTI13CRecordVectorIyE
	.section	.rodata._ZTI13CRecordVectorIyE,"aG",@progbits,_ZTI13CRecordVectorIyE,comdat
	.weak	_ZTI13CRecordVectorIyE
	.p2align	4
_ZTI13CRecordVectorIyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIyE, 24

	.type	_ZTV7CCodecs,@object    # @_ZTV7CCodecs
	.section	.rodata._ZTV7CCodecs,"aG",@progbits,_ZTV7CCodecs,comdat
	.weak	_ZTV7CCodecs
	.p2align	3
_ZTV7CCodecs:
	.quad	0
	.quad	_ZTI7CCodecs
	.quad	_ZN7CCodecs14QueryInterfaceERK4GUIDPPv
	.quad	_ZN7CCodecs6AddRefEv
	.quad	_ZN7CCodecs7ReleaseEv
	.quad	_ZN7CCodecsD2Ev
	.quad	_ZN7CCodecsD0Ev
	.size	_ZTV7CCodecs, 56

	.type	_ZTS7CCodecs,@object    # @_ZTS7CCodecs
	.section	.rodata._ZTS7CCodecs,"aG",@progbits,_ZTS7CCodecs,comdat
	.weak	_ZTS7CCodecs
_ZTS7CCodecs:
	.asciz	"7CCodecs"
	.size	_ZTS7CCodecs, 9

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI7CCodecs,@object    # @_ZTI7CCodecs
	.section	.rodata._ZTI7CCodecs,"aG",@progbits,_ZTI7CCodecs,comdat
	.weak	_ZTI7CCodecs
	.p2align	4
_ZTI7CCodecs:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS7CCodecs
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI8IUnknown
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI7CCodecs, 56

	.type	_ZTV13CObjectVectorI10CArcInfoExE,@object # @_ZTV13CObjectVectorI10CArcInfoExE
	.section	.rodata._ZTV13CObjectVectorI10CArcInfoExE,"aG",@progbits,_ZTV13CObjectVectorI10CArcInfoExE,comdat
	.weak	_ZTV13CObjectVectorI10CArcInfoExE
	.p2align	3
_ZTV13CObjectVectorI10CArcInfoExE:
	.quad	0
	.quad	_ZTI13CObjectVectorI10CArcInfoExE
	.quad	_ZN13CObjectVectorI10CArcInfoExED2Ev
	.quad	_ZN13CObjectVectorI10CArcInfoExED0Ev
	.quad	_ZN13CObjectVectorI10CArcInfoExE6DeleteEii
	.size	_ZTV13CObjectVectorI10CArcInfoExE, 40

	.type	_ZTS13CObjectVectorI10CArcInfoExE,@object # @_ZTS13CObjectVectorI10CArcInfoExE
	.section	.rodata._ZTS13CObjectVectorI10CArcInfoExE,"aG",@progbits,_ZTS13CObjectVectorI10CArcInfoExE,comdat
	.weak	_ZTS13CObjectVectorI10CArcInfoExE
	.p2align	4
_ZTS13CObjectVectorI10CArcInfoExE:
	.asciz	"13CObjectVectorI10CArcInfoExE"
	.size	_ZTS13CObjectVectorI10CArcInfoExE, 30

	.type	_ZTI13CObjectVectorI10CArcInfoExE,@object # @_ZTI13CObjectVectorI10CArcInfoExE
	.section	.rodata._ZTI13CObjectVectorI10CArcInfoExE,"aG",@progbits,_ZTI13CObjectVectorI10CArcInfoExE,comdat
	.weak	_ZTI13CObjectVectorI10CArcInfoExE
	.p2align	4
_ZTI13CObjectVectorI10CArcInfoExE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI10CArcInfoExE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI10CArcInfoExE, 24

	.type	_ZTV7CBufferIhE,@object # @_ZTV7CBufferIhE
	.section	.rodata._ZTV7CBufferIhE,"aG",@progbits,_ZTV7CBufferIhE,comdat
	.weak	_ZTV7CBufferIhE
	.p2align	3
_ZTV7CBufferIhE:
	.quad	0
	.quad	_ZTI7CBufferIhE
	.quad	_ZN7CBufferIhED2Ev
	.quad	_ZN7CBufferIhED0Ev
	.size	_ZTV7CBufferIhE, 32

	.type	_ZTS7CBufferIhE,@object # @_ZTS7CBufferIhE
	.section	.rodata._ZTS7CBufferIhE,"aG",@progbits,_ZTS7CBufferIhE,comdat
	.weak	_ZTS7CBufferIhE
_ZTS7CBufferIhE:
	.asciz	"7CBufferIhE"
	.size	_ZTS7CBufferIhE, 12

	.type	_ZTI7CBufferIhE,@object # @_ZTI7CBufferIhE
	.section	.rodata._ZTI7CBufferIhE,"aG",@progbits,_ZTI7CBufferIhE,comdat
	.weak	_ZTI7CBufferIhE
	.p2align	3
_ZTI7CBufferIhE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS7CBufferIhE
	.size	_ZTI7CBufferIhE, 16

	.type	_ZTV13CObjectVectorI11CArcExtInfoE,@object # @_ZTV13CObjectVectorI11CArcExtInfoE
	.section	.rodata._ZTV13CObjectVectorI11CArcExtInfoE,"aG",@progbits,_ZTV13CObjectVectorI11CArcExtInfoE,comdat
	.weak	_ZTV13CObjectVectorI11CArcExtInfoE
	.p2align	3
_ZTV13CObjectVectorI11CArcExtInfoE:
	.quad	0
	.quad	_ZTI13CObjectVectorI11CArcExtInfoE
	.quad	_ZN13CObjectVectorI11CArcExtInfoED2Ev
	.quad	_ZN13CObjectVectorI11CArcExtInfoED0Ev
	.quad	_ZN13CObjectVectorI11CArcExtInfoE6DeleteEii
	.size	_ZTV13CObjectVectorI11CArcExtInfoE, 40

	.type	_ZTS13CObjectVectorI11CArcExtInfoE,@object # @_ZTS13CObjectVectorI11CArcExtInfoE
	.section	.rodata._ZTS13CObjectVectorI11CArcExtInfoE,"aG",@progbits,_ZTS13CObjectVectorI11CArcExtInfoE,comdat
	.weak	_ZTS13CObjectVectorI11CArcExtInfoE
	.p2align	4
_ZTS13CObjectVectorI11CArcExtInfoE:
	.asciz	"13CObjectVectorI11CArcExtInfoE"
	.size	_ZTS13CObjectVectorI11CArcExtInfoE, 31

	.type	_ZTI13CObjectVectorI11CArcExtInfoE,@object # @_ZTI13CObjectVectorI11CArcExtInfoE
	.section	.rodata._ZTI13CObjectVectorI11CArcExtInfoE,"aG",@progbits,_ZTI13CObjectVectorI11CArcExtInfoE,comdat
	.weak	_ZTI13CObjectVectorI11CArcExtInfoE
	.p2align	4
_ZTI13CObjectVectorI11CArcExtInfoE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI11CArcExtInfoE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI11CArcExtInfoE, 24

	.type	.L.str.36,@object       # @.str.36
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.36:
	.asciz	"7-Zip cannot find the code that works with archives."
	.size	.L.str.36, 53

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"Unsupported archive type"
	.size	.L.str.37, 25

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"Everything is Ok"
	.size	.L.str.39, 17

	.type	_ZTSN9NExitCode5EEnumE,@object # @_ZTSN9NExitCode5EEnumE
	.section	.rodata._ZTSN9NExitCode5EEnumE,"aG",@progbits,_ZTSN9NExitCode5EEnumE,comdat
	.weak	_ZTSN9NExitCode5EEnumE
	.p2align	4
_ZTSN9NExitCode5EEnumE:
	.asciz	"N9NExitCode5EEnumE"
	.size	_ZTSN9NExitCode5EEnumE, 19

	.type	_ZTIN9NExitCode5EEnumE,@object # @_ZTIN9NExitCode5EEnumE
	.section	.rodata._ZTIN9NExitCode5EEnumE,"aG",@progbits,_ZTIN9NExitCode5EEnumE,comdat
	.weak	_ZTIN9NExitCode5EEnumE
	.p2align	3
_ZTIN9NExitCode5EEnumE:
	.quad	_ZTVN10__cxxabiv116__enum_type_infoE+16
	.quad	_ZTSN9NExitCode5EEnumE
	.size	_ZTIN9NExitCode5EEnumE, 16

	.type	.L.str.40,@object       # @.str.40
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.40:
	.asciz	"Incorrect command line"
	.size	.L.str.40, 23

	.type	_ZTV13CObjectVectorI11CStringBaseIwEE,@object # @_ZTV13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTV13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTV13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTV13CObjectVectorI11CStringBaseIwEE
	.p2align	3
_ZTV13CObjectVectorI11CStringBaseIwEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI11CStringBaseIwEE
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.size	_ZTV13CObjectVectorI11CStringBaseIwEE, 40

	.type	_ZTS13CObjectVectorI11CStringBaseIwEE,@object # @_ZTS13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTS13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTS13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTS13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTS13CObjectVectorI11CStringBaseIwEE:
	.asciz	"13CObjectVectorI11CStringBaseIwEE"
	.size	_ZTS13CObjectVectorI11CStringBaseIwEE, 34

	.type	_ZTI13CObjectVectorI11CStringBaseIwEE,@object # @_ZTI13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTI13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTI13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTI13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTI13CObjectVectorI11CStringBaseIwEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI11CStringBaseIwEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI11CStringBaseIwEE, 24

	.type	_ZTV13CRecordVectorIiE,@object # @_ZTV13CRecordVectorIiE
	.section	.rodata._ZTV13CRecordVectorIiE,"aG",@progbits,_ZTV13CRecordVectorIiE,comdat
	.weak	_ZTV13CRecordVectorIiE
	.p2align	3
_ZTV13CRecordVectorIiE:
	.quad	0
	.quad	_ZTI13CRecordVectorIiE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIiED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIiE, 40

	.type	_ZTS13CRecordVectorIiE,@object # @_ZTS13CRecordVectorIiE
	.section	.rodata._ZTS13CRecordVectorIiE,"aG",@progbits,_ZTS13CRecordVectorIiE,comdat
	.weak	_ZTS13CRecordVectorIiE
	.p2align	4
_ZTS13CRecordVectorIiE:
	.asciz	"13CRecordVectorIiE"
	.size	_ZTS13CRecordVectorIiE, 19

	.type	_ZTI13CRecordVectorIiE,@object # @_ZTI13CRecordVectorIiE
	.section	.rodata._ZTI13CRecordVectorIiE,"aG",@progbits,_ZTI13CRecordVectorIiE,comdat
	.weak	_ZTI13CRecordVectorIiE
	.p2align	4
_ZTI13CRecordVectorIiE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIiE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIiE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
