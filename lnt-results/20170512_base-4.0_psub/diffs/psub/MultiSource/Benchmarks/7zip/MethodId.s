	.text
	.file	"MethodId.bc"
	.globl	_Z23ConvertMethodIdToStringy
	.p2align	4, 0x90
	.type	_Z23ConvertMethodIdToStringy,@function
_Z23ConvertMethodIdToStringy:           # @_Z23ConvertMethodIdToStringy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$128, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 176
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$0, 124(%rsp)
	leaq	128(%rsp), %rbx
	movl	$48, %eax
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movl	%esi, %ecx
	andb	$15, %cl
	movl	%esi, %edx
	andl	$15, %edx
	cmpb	$10, %cl
	movl	$55, %ecx
	cmovbl	%eax, %ecx
	addl	%edx, %ecx
	movl	%ecx, -8(%rbx)
	movq	%rsi, %rcx
	shrq	$4, %rcx
	movl	%ecx, %edx
	andb	$15, %dl
	andl	$15, %ecx
	cmpb	$10, %dl
	movl	$55, %ebp
	cmovbl	%eax, %ebp
	addl	%ecx, %ebp
	movl	%ebp, -12(%rbx)
	leaq	-8(%rbx), %rbx
	shrq	$8, %rsi
	jne	.LBB0_1
# BB#2:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	xorl	%r12d, %r12d
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB0_3:                                # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	incl	%r12d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB0_3
# BB#4:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%r12), %eax
	movslq	%eax, %r15
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%r15d, 12(%r14)
	movl	%ebp, (%rax)
	addq	$4, %rax
	.p2align	4, 0x90
.LBB0_5:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i._ZN11CStringBaseIwE11SetCapacityEi.exit.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	movl	%ecx, (%rax)
	addq	$4, %rbx
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB0_5
# BB#6:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
	movl	%r12d, 8(%r14)
	movq	%r14, %rax
	addq	$128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_Z23ConvertMethodIdToStringy, .Lfunc_end0-_Z23ConvertMethodIdToStringy
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
