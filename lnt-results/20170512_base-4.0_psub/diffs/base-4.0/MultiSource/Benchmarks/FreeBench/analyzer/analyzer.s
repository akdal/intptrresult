	.text
	.file	"analyzer.bc"
	.globl	version
	.p2align	4, 0x90
	.type	version,@function
version:                                # @version
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	movl	$.L.str.3, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
	movl	$.L.str.5, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	movl	$.L.str.7, %edx
	xorl	%eax, %eax
	popq	%rcx
	jmp	fprintf                 # TAILCALL
.Lfunc_end0:
	.size	version, .Lfunc_end0-version
	.cfi_endproc

	.globl	init_def_table
	.p2align	4, 0x90
	.type	init_def_table,@function
init_def_table:                         # @init_def_table
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 16
.Lcfi2:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movslq	%ebx, %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, def_table(%rip)
	testq	%rax, %rax
	je	.LBB1_11
# BB#1:                                 # %.preheader
	testl	%ebx, %ebx
	jle	.LBB1_10
# BB#2:                                 # %.lr.ph.preheader
	movq	$0, (%rax)
	cmpl	$1, %ebx
	je	.LBB1_10
# BB#3:                                 # %.lr.ph..lr.ph_crit_edge.preheader
	movl	%ebx, %eax
	leal	7(%rax), %esi
	leaq	-2(%rax), %rdx
	andq	$7, %rsi
	je	.LBB1_4
# BB#5:                                 # %.lr.ph..lr.ph_crit_edge.prol.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph..lr.ph_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	def_table(%rip), %rdi
	movq	$0, 8(%rdi,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB1_6
# BB#7:                                 # %.lr.ph..lr.ph_crit_edge.prol.loopexit.unr-lcssa
	incq	%rcx
	cmpq	$7, %rdx
	jae	.LBB1_9
	jmp	.LBB1_10
.LBB1_4:
	movl	$1, %ecx
	cmpq	$7, %rdx
	jb	.LBB1_10
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	def_table(%rip), %rdx
	movq	$0, (%rdx,%rcx,8)
	movq	def_table(%rip), %rdx
	movq	$0, 8(%rdx,%rcx,8)
	movq	def_table(%rip), %rdx
	movq	$0, 16(%rdx,%rcx,8)
	movq	def_table(%rip), %rdx
	movq	$0, 24(%rdx,%rcx,8)
	movq	def_table(%rip), %rdx
	movq	$0, 32(%rdx,%rcx,8)
	movq	def_table(%rip), %rdx
	movq	$0, 40(%rdx,%rcx,8)
	movq	def_table(%rip), %rdx
	movq	$0, 48(%rdx,%rcx,8)
	movq	def_table(%rip), %rdx
	movq	$0, 56(%rdx,%rcx,8)
	addq	$8, %rcx
	cmpq	%rcx, %rax
	jne	.LBB1_9
.LBB1_10:                               # %._crit_edge
	popq	%rbx
	retq
.LBB1_11:
	movq	stderr(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	init_def_table, .Lfunc_end1-init_def_table
	.cfi_endproc

	.globl	error
	.p2align	4, 0x90
	.type	error,@function
error:                                  # @error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 16
	movl	$1, %edi
	callq	exit
.Lfunc_end2:
	.size	error, .Lfunc_end2-error
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi7:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi10:
	.cfi_def_cfa_offset 240
.Lcfi11:
	.cfi_offset %rbx, -56
.Lcfi12:
	.cfi_offset %r12, -48
.Lcfi13:
	.cfi_offset %r13, -40
.Lcfi14:
	.cfi_offset %r14, -32
.Lcfi15:
	.cfi_offset %r15, -24
.Lcfi16:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movl	%edi, %ebx
	movq	stderr(%rip), %rdi
	movl	$.L.str.10, %esi
	movl	$.L.str.3, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.11, %esi
	movl	$.L.str.5, %edx
	xorl	%eax, %eax
	callq	fprintf
	cmpl	$1, %ebx
	jle	.LBB3_75
# BB#1:
	movslq	def_table_size(%rip), %rbp
	leaq	(,%rbp,8), %rdi
	callq	malloc
	movq	%rax, def_table(%rip)
	testq	%rax, %rax
	je	.LBB3_17
# BB#2:                                 # %.preheader.i
	movl	%ebp, %ecx
	testl	%ecx, %ecx
	jle	.LBB3_12
# BB#3:                                 # %.lr.ph.preheader.i
	movq	$0, (%rax)
	cmpl	$1, %ecx
	je	.LBB3_12
# BB#4:                                 # %.lr.ph..lr.ph_crit_edge.i.preheader
	movq	$0, 8(%rax)
	cmpl	$2, %ecx
	je	.LBB3_12
# BB#5:                                 # %.lr.ph..lr.ph_crit_edge.i..lr.ph..lr.ph_crit_edge.i_crit_edge.preheader
	leal	6(%rcx), %esi
	leaq	-3(%rcx), %rdx
	andq	$7, %rsi
	je	.LBB3_6
# BB#7:                                 # %.lr.ph..lr.ph_crit_edge.i..lr.ph..lr.ph_crit_edge.i_crit_edge.prol.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_8:                                # %.lr.ph..lr.ph_crit_edge.i..lr.ph..lr.ph_crit_edge.i_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	def_table(%rip), %rdi
	movq	$0, 16(%rdi,%rax,8)
	incq	%rax
	cmpq	%rax, %rsi
	jne	.LBB3_8
# BB#9:                                 # %.lr.ph..lr.ph_crit_edge.i..lr.ph..lr.ph_crit_edge.i_crit_edge.prol.loopexit.unr-lcssa
	addq	$2, %rax
	cmpq	$7, %rdx
	jae	.LBB3_11
	jmp	.LBB3_12
.LBB3_6:
	movl	$2, %eax
	cmpq	$7, %rdx
	jb	.LBB3_12
	.p2align	4, 0x90
.LBB3_11:                               # %.lr.ph..lr.ph_crit_edge.i..lr.ph..lr.ph_crit_edge.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	def_table(%rip), %rdx
	movq	$0, (%rdx,%rax,8)
	movq	def_table(%rip), %rdx
	movq	$0, 8(%rdx,%rax,8)
	movq	def_table(%rip), %rdx
	movq	$0, 16(%rdx,%rax,8)
	movq	def_table(%rip), %rdx
	movq	$0, 24(%rdx,%rax,8)
	movq	def_table(%rip), %rdx
	movq	$0, 32(%rdx,%rax,8)
	movq	def_table(%rip), %rdx
	movq	$0, 40(%rdx,%rax,8)
	movq	def_table(%rip), %rdx
	movq	$0, 48(%rdx,%rax,8)
	movq	def_table(%rip), %rdx
	movq	$0, 56(%rdx,%rax,8)
	addq	$8, %rax
	cmpq	%rax, %rcx
	jne	.LBB3_11
.LBB3_12:                               # %init_def_table.exit
	movslq	%ebx, %r12
	movq	-8(%r15,%r12,8), %rdi
	decq	%r12
	movl	$.L.str.12, %esi
	callq	fopen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_19
# BB#13:
	leaq	80(%rsp), %rbx
	.p2align	4, 0x90
.LBB3_14:                               # %.preheader447
                                        # =>This Inner Loop Header: Depth=1
	movl	$100, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	cmpb	$69, 80(%rsp)
	jne	.LBB3_14
# BB#15:
	leaq	85(%rsp), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r14
	movl	epoch_length(%rip), %ecx
	cmpl	%ecx, %r14d
	jl	.LBB3_16
# BB#21:
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movl	%r14d, %eax
	cltd
	idivl	%ecx
	xorl	%esi, %esi
	testl	%edx, %edx
	setne	%sil
	addl	%eax, %esi
	je	.LBB3_76
# BB#22:
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	movl	%esi, %ebx
	callq	printf
	movq	%rbp, %rdi
	callq	rewind
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	callq	imix_test
	movq	%rax, %rbp
	movslq	%ebx, %rax
	imulq	$56, %rax, %rdi
	callq	malloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB3_77
# BB#23:                                # %.preheader446
	testl	%ebx, %ebx
	jle	.LBB3_30
# BB#24:                                # %.lr.ph470.preheader
	movl	%ebx, %eax
	leaq	-1(%rax), %rcx
	movq	%rax, %rsi
	xorl	%edx, %edx
	andq	$3, %rsi
	je	.LBB3_27
# BB#25:                                # %.lr.ph470.prol.preheader
	xorps	%xmm0, %xmm0
	movq	%r13, %rdi
	.p2align	4, 0x90
.LBB3_26:                               # %.lr.ph470.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, (%rdi)
	movl	$1, 16(%rdi)
	incq	%rdx
	addq	$56, %rdi
	cmpq	%rdx, %rsi
	jne	.LBB3_26
.LBB3_27:                               # %.lr.ph470.prol.loopexit
	cmpq	$3, %rcx
	jb	.LBB3_30
# BB#28:                                # %.lr.ph470.preheader.new
	subq	%rdx, %rax
	imulq	$56, %rdx, %rcx
	addq	%r13, %rcx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB3_29:                               # %.lr.ph470
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, (%rcx)
	movl	$1, 16(%rcx)
	movups	%xmm0, 56(%rcx)
	movl	$1, 72(%rcx)
	movups	%xmm0, 112(%rcx)
	movl	$1, 128(%rcx)
	movups	%xmm0, 168(%rcx)
	movl	$1, 184(%rcx)
	addq	$224, %rcx
	addq	$-4, %rax
	jne	.LBB3_29
.LBB3_30:                               # %._crit_edge471
	movl	%ebx, 52(%rsp)          # 4-byte Spill
	movslq	%r14d, %rax
	addq	%rax, %rbp
	leaq	80(,%rbp,8), %rdi
	callq	malloc
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	feof
	testl	%eax, %eax
                                        # implicit-def: %RDI
	jne	.LBB3_34
# BB#31:                                # %.lr.ph467.preheader
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_32:                               # %.lr.ph467
                                        # =>This Inner Loop Header: Depth=1
	movl	$50, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	$50, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	fgets
	movq	%rbx, (%rbp)
	movq	%r14, %rdi
	callq	feof
	addq	$8, %rbp
	testl	%eax, %eax
	je	.LBB3_32
# BB#33:                                # %._crit_edge468.loopexit
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
.LBB3_34:                               # %._crit_edge468
	movq	%r15, 64(%rsp)          # 8-byte Spill
	leaq	80(%rsp), %rbx
	leaq	8(%rsp), %rcx
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	sscanf
	movq	8(%rsp), %rax
	movq	%rax, 24(%r13)
	movl	$.L.str.19, %esi
	movq	%rbx, %rdi
	callq	strcmp
	movl	$-1, %ebx
	testl	%eax, %eax
	jne	.LBB3_70
# BB#35:                                # %.lr.ph463
	movl	$-1, %ebx
	movl	$1, %ebp
	xorl	%r15d, %r15d
	movl	$-1, 20(%rsp)           # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB3_37:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_42 Depth 2
                                        #       Child Loop BB3_50 Depth 3
                                        #         Child Loop BB3_52 Depth 4
	incl	%ebx
	movl	20(%rsp), %eax          # 4-byte Reload
	incl	%eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	cmpl	epoch_length(%rip), %eax
	jne	.LBB3_41
# BB#38:                                #   in Loop: Header=BB3_37 Depth=1
	movq	24(%rsp), %rdx
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
	leaq	80(%rsp), %r14
	movq	%r14, %rdi
	callq	sprintf
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	leaq	8(%rsp), %rdx
	callq	sscanf
	movq	8(%rsp), %rcx
	movl	%ebx, %eax
	cltd
	idivl	epoch_length(%rip)
	movslq	%eax, %rdx
	imulq	$56, %rdx, %rsi
	movq	%rcx, 24(%r13,%rsi)
	testl	%edx, %edx
	je	.LBB3_40
# BB#39:                                #   in Loop: Header=BB3_37 Depth=1
	decl	%eax
	cltq
	imulq	$56, %rax, %rax
	movq	%rcx, 32(%r13,%rax)
.LBB3_40:                               # %.preheader444
                                        #   in Loop: Header=BB3_37 Depth=1
	xorl	%r15d, %r15d
.LBB3_41:                               # %.preheader444
                                        #   in Loop: Header=BB3_37 Depth=1
	movslq	%ebp, %rbp
	jmp	.LBB3_42
.LBB3_68:                               # %.critedge441
                                        #   in Loop: Header=BB3_42 Depth=2
	movq	8(%r13,%rax), %rcx
	movq	%rbp, 24(%rcx)
	movq	%rbp, 8(%r13,%rax)
	.p2align	4, 0x90
.LBB3_69:                               # %.loopexit
                                        #   in Loop: Header=BB3_42 Depth=2
	incl	%r15d
	movq	72(%rsp), %rbp          # 8-byte Reload
.LBB3_42:                               #   Parent Loop BB3_37 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_50 Depth 3
                                        #         Child Loop BB3_52 Depth 4
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbp,8), %rdi
	incq	%rbp
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	leaq	80(%rsp), %rdx
	leaq	24(%rsp), %rcx
	leaq	8(%rsp), %r8
	callq	sscanf
	movb	80(%rsp), %al
	cmpb	$76, %al
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	jne	.LBB3_62
# BB#43:                                #   in Loop: Header=BB3_42 Depth=2
	cmpb	$68, 81(%rsp)
	jne	.LBB3_62
# BB#44:                                #   in Loop: Header=BB3_42 Depth=2
	cmpb	$58, 82(%rsp)
	jne	.LBB3_62
# BB#45:                                #   in Loop: Header=BB3_42 Depth=2
	movb	83(%rsp), %cl
	testb	%cl, %cl
	jne	.LBB3_62
# BB#46:                                #   in Loop: Header=BB3_42 Depth=2
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rcx
	movl	$1, (%rcx)
	movq	24(%rsp), %rax
	movq	%rax, 8(%rcx)
	movq	8(%rsp), %rdi
	movl	epoch_length(%rip), %esi
	movl	%ebx, %eax
	cltd
	idivl	%esi
	movslq	%eax, %rdx
	imulq	$56, %rdx, %rdx
	subq	24(%r13,%rdx), %rdi
	movq	%rdi, 16(%rcx)
	movq	$0, 24(%rcx)
	cmpl	$0, 16(%r13,%rdx)
	je	.LBB3_61
# BB#47:                                #   in Loop: Header=BB3_42 Depth=2
	leaq	16(%r13,%rdx), %rdi
	movq	%rcx, (%r13,%rdx)
	movq	%rcx, 8(%r13,%rdx)
	movl	$0, (%rdi)
	testl	%eax, %eax
	jg	.LBB3_49
	jmp	.LBB3_69
	.p2align	4, 0x90
.LBB3_62:                               # %.thread
                                        #   in Loop: Header=BB3_42 Depth=2
	cmpb	$83, %al
	jne	.LBB3_36
# BB#63:                                # %.thread
                                        #   in Loop: Header=BB3_42 Depth=2
	cmpb	$84, 81(%rsp)
	jne	.LBB3_36
# BB#64:                                # %.thread
                                        #   in Loop: Header=BB3_42 Depth=2
	cmpb	$58, 82(%rsp)
	jne	.LBB3_36
# BB#65:                                # %.thread
                                        #   in Loop: Header=BB3_42 Depth=2
	movb	83(%rsp), %al
	testb	%al, %al
	jne	.LBB3_36
# BB#66:                                #   in Loop: Header=BB3_42 Depth=2
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbp
	movl	$2, (%rbp)
	movq	24(%rsp), %rdi
	movq	%rdi, 8(%rbp)
	movq	8(%rsp), %rcx
	movl	%ebx, %eax
	cltd
	idivl	epoch_length(%rip)
	movslq	%eax, %rsi
	imulq	$56, %rsi, %rax
	subq	24(%r13,%rax), %rcx
	movq	%rcx, 16(%rbp)
	movq	$0, 24(%rbp)
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	callq	def_list_mod
	movl	%ebx, %eax
	cltd
	idivl	epoch_length(%rip)
	cltq
	imulq	$56, %rax, %rax
	cmpl	$0, 16(%r13,%rax)
	je	.LBB3_68
# BB#67:                                # %.critedge
                                        #   in Loop: Header=BB3_42 Depth=2
	leaq	16(%r13,%rax), %rcx
	movq	%rbp, (%r13,%rax)
	movq	%rbp, 8(%r13,%rax)
	movl	$0, (%rcx)
	jmp	.LBB3_69
.LBB3_61:                               #   in Loop: Header=BB3_42 Depth=2
	movq	8(%r13,%rdx), %rdi
	movq	%rcx, 24(%rdi)
	movq	%rcx, 8(%r13,%rdx)
	testl	%eax, %eax
	jle	.LBB3_69
.LBB3_49:                               # %.lr.ph457.preheader
                                        #   in Loop: Header=BB3_42 Depth=2
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_50:                               # %.lr.ph457
                                        #   Parent Loop BB3_37 Depth=1
                                        #     Parent Loop BB3_42 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_52 Depth 4
	imulq	$56, %r12, %rax
	movq	(%r13,%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB3_60
# BB#51:                                # %.lr.ph454.preheader
                                        #   in Loop: Header=BB3_50 Depth=3
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_52:                               # %.lr.ph454
                                        #   Parent Loop BB3_37 Depth=1
                                        #     Parent Loop BB3_42 Depth=2
                                        #       Parent Loop BB3_50 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	$2, (%rbp)
	jne	.LBB3_58
# BB#53:                                #   in Loop: Header=BB3_52 Depth=4
	movq	8(%rbp), %rdi
	cmpq	24(%rsp), %rdi
	jne	.LBB3_58
# BB#54:                                #   in Loop: Header=BB3_52 Depth=4
	callq	def_list_lookup
	testq	%rax, %rax
	je	.LBB3_58
# BB#55:                                #   in Loop: Header=BB3_52 Depth=4
	movl	16(%rax), %ecx
	cmpq	%r12, %rcx
	jne	.LBB3_58
# BB#56:                                #   in Loop: Header=BB3_52 Depth=4
	cmpl	20(%rax), %r14d
	jne	.LBB3_58
# BB#57:                                #   in Loop: Header=BB3_52 Depth=4
	movq	24(%rsp), %rdi
	callq	conflict_list
	movq	24(%rsp), %rdi
	movq	16(%rbp), %rcx
	movl	%ebx, %eax
	cltd
	idivl	epoch_length(%rip)
	movq	8(%rsp), %rdx
	movslq	%eax, %r8
	imulq	$56, %r8, %rax
	subq	24(%r13,%rax), %rdx
	movq	%rdx, (%rsp)
	movl	%r12d, %esi
	movl	%r14d, %edx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%r15d, %r9d
	callq	hard_raw_mod
	.p2align	4, 0x90
.LBB3_58:                               #   in Loop: Header=BB3_52 Depth=4
	movq	24(%rbp), %rbp
	incl	%r14d
	testq	%rbp, %rbp
	jne	.LBB3_52
# BB#59:                                # %._crit_edge455.loopexit
                                        #   in Loop: Header=BB3_50 Depth=3
	movl	epoch_length(%rip), %esi
.LBB3_60:                               # %._crit_edge455
                                        #   in Loop: Header=BB3_50 Depth=3
	incq	%r12
	movl	%ebx, %eax
	cltd
	idivl	%esi
	cltq
	cmpq	%rax, %r12
	jl	.LBB3_50
	jmp	.LBB3_69
	.p2align	4, 0x90
.LBB3_36:                               # %.loopexit445
                                        #   in Loop: Header=BB3_37 Depth=1
	movl	$.L.str.19, %esi
	leaq	80(%rsp), %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB3_37
.LBB3_70:                               # %._crit_edge464
	leaq	80(%rsp), %rdi
	movl	$.L.str.27, %esi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB3_78
# BB#71:                                # %.preheader
	movq	list(%rip), %rbp
	testq	%rbp, %rbp
	je	.LBB3_74
	.p2align	4, 0x90
.LBB3_72:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	16(%rbp), %esi
	movq	8(%rbp), %rdx
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB3_72
.LBB3_74:                               # %._crit_edge
	movl	%ebx, %eax
	cltd
	idivl	epoch_length(%rip)
	cltq
	imulq	$56, %rax, %rax
	movq	8(%r13,%rax), %rcx
	movq	16(%rcx), %rcx
	addq	24(%r13,%rax), %rcx
	movq	%rcx, 32(%r13,%rax)
	xorl	%eax, %eax
	callq	find_hard_raws
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	speedup_test
	xorl	%edx, %edx
	movl	$3, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	movl	52(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %esi
	callq	specul_time_o
	movl	$0, (%rsp)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$3, %r8d
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	movl	%ebx, %esi
	callq	specul_time_r
	xorl	%eax, %eax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_75:
	movl	$1, %edi
	callq	exit
.LBB3_17:
	movq	stderr(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$17, %esi
	jmp	.LBB3_18
.LBB3_19:
	movq	stderr(%rip), %rdi
	movq	(%r15,%r12,8), %rdx
	movl	$.L.str.13, %esi
	jmp	.LBB3_20
.LBB3_16:
	movq	stderr(%rip), %rdi
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	fprintf
	xorl	%edi, %edi
	callq	exit
.LBB3_76:
	movq	stderr(%rip), %rdi
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%r15,%rax,8), %rdx
	movl	$.L.str.15, %esi
	xorl	%eax, %eax
	callq	fprintf
	xorl	%edi, %edi
	callq	exit
.LBB3_77:
	movq	stderr(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$35, %esi
.LBB3_18:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB3_78:
	movq	stderr(%rip), %rdi
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rdx
	movl	$.L.str.30, %esi
.LBB3_20:
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"--- Dependency Analyzer version: %s, Copyleft 1999 Peter Rundberg ---\n"
	.size	.L.str, 71

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"1.00"
	.size	.L.str.1, 5

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"--- Compiled: %s ---\n"
	.size	.L.str.2, 22

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"today"
	.size	.L.str.3, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"--- Flags: %s ---\n"
	.size	.L.str.4, 19

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.zero	1
	.size	.L.str.5, 1

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"--- Host: %s ---\n\n"
	.size	.L.str.6, 19

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"thishost"
	.size	.L.str.7, 9

	.type	list_len,@object        # @list_len
	.bss
	.globl	list_len
	.p2align	2
list_len:
	.long	0                       # 0x0
	.size	list_len, 4

	.type	top_list_len,@object    # @top_list_len
	.data
	.globl	top_list_len
	.p2align	2
top_list_len:
	.long	10                      # 0xa
	.size	top_list_len, 4

	.type	epoch_length,@object    # @epoch_length
	.globl	epoch_length
	.p2align	2
epoch_length:
	.long	1                       # 0x1
	.size	epoch_length, 4

	.type	def_table_size,@object  # @def_table_size
	.globl	def_table_size
	.p2align	2
def_table_size:
	.long	10007                   # 0x2717
	.size	def_table_size, 4

	.type	list,@object            # @list
	.bss
	.globl	list
	.p2align	3
list:
	.quad	0
	.size	list, 8

	.type	first,@object           # @first
	.data
	.globl	first
	.p2align	2
first:
	.long	1                       # 0x1
	.size	first, 4

	.type	def_table,@object       # @def_table
	.bss
	.globl	def_table
	.p2align	3
def_table:
	.quad	0
	.size	def_table, 8

	.type	hard_raw_list,@object   # @hard_raw_list
	.globl	hard_raw_list
	.p2align	3
hard_raw_list:
	.quad	0
	.size	hard_raw_list, 8

	.type	.L.str.8,@object        # @.str.8
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.8:
	.asciz	"Out of memory...\n"
	.size	.L.str.8, 18

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Compile date: %s\n"
	.size	.L.str.10, 18

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Compiler switches: %s\n"
	.size	.L.str.11, 23

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"r"
	.size	.L.str.12, 2

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"ERROR: \tCould not find file: %s\n\n"
	.size	.L.str.13, 34

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"ERROR: Number of loop iterations is less than epoch length: %d<%d\n"
	.size	.L.str.14, 67

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"File %s is empty\n"
	.size	.L.str.15, 18

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Num_epochs: %d\n"
	.size	.L.str.16, 16

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"ALERT: \tOut of memory, aborting...\n"
	.size	.L.str.17, 36

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"%s %lu"
	.size	.L.str.18, 7

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"START:"
	.size	.L.str.19, 7

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"%lx"
	.size	.L.str.20, 4

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"%lu"
	.size	.L.str.21, 4

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"%s %lx %lu"
	.size	.L.str.22, 11

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"END:"
	.size	.L.str.27, 5

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"ERROR: \tWrong format on file %s\n"
	.size	.L.str.30, 33

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"%d RAW:s for 0x%lx\n"
	.size	.L.str.31, 20


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
