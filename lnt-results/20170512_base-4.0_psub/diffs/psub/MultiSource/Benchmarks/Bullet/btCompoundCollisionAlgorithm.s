	.text
	.file	"btCompoundCollisionAlgorithm.bc"
	.globl	_ZN28btCompoundCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
	.p2align	4, 0x90
	.type	_ZN28btCompoundCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b,@function
_ZN28btCompoundCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b: # @_ZN28btCompoundCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movq	%rcx, %r15
	movq	%rdx, %rbp
	movq	%rsi, %r12
	movq	%rdi, %rbx
	callq	_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	movq	$_ZTV28btCompoundCollisionAlgorithm+16, (%rbx)
	movb	$1, 40(%rbx)
	movq	$0, 32(%rbx)
	movl	$0, 20(%rbx)
	movl	$0, 24(%rbx)
	movb	%r14b, 48(%rbx)
	movq	8(%r12), %rax
	movq	%rax, 56(%rbx)
	movb	$0, 64(%rbx)
	testb	%r14b, %r14b
	movq	%rbp, %rax
	cmovneq	%r15, %rax
	movq	200(%rax), %rax
	movl	96(%rax), %eax
	movl	%eax, 68(%rbx)
.Ltmp0:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	callq	_ZN28btCompoundCollisionAlgorithm26preallocateChildAlgorithmsEP17btCollisionObjectS1_
.Ltmp1:
# BB#1:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_2:
.Ltmp2:
	movq	%rax, %r14
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_6
# BB#3:
	cmpb	$0, 40(%rbx)
	je	.LBB0_5
# BB#4:
.Ltmp3:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
.LBB0_5:                                # %.noexc
	movq	$0, 32(%rbx)
.LBB0_6:
	movb	$1, 40(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 20(%rbx)
.Ltmp5:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp6:
# BB#7:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_8:
.Ltmp7:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN28btCompoundCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b, .Lfunc_end0-_ZN28btCompoundCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp3           #   Call between .Ltmp3 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	1                       #   On action: 1
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN28btCompoundCollisionAlgorithm26preallocateChildAlgorithmsEP17btCollisionObjectS1_
	.p2align	4, 0x90
	.type	_ZN28btCompoundCollisionAlgorithm26preallocateChildAlgorithmsEP17btCollisionObjectS1_,@function
_ZN28btCompoundCollisionAlgorithm26preallocateChildAlgorithmsEP17btCollisionObjectS1_: # @_ZN28btCompoundCollisionAlgorithm26preallocateChildAlgorithmsEP17btCollisionObjectS1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 96
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	cmpb	$0, 48(%r13)
	movq	%rsi, %r15
	cmovneq	%rdx, %r15
	cmovneq	%rsi, %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	200(%r15), %r12
	movl	28(%r12), %edi
	movl	20(%r13), %r8d
	cmpl	%edi, %r8d
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	jge	.LBB1_21
# BB#1:
	movslq	%edi, %rbx
	movslq	%r8d, %r14
	cmpl	%edi, 24(%r13)
	jge	.LBB1_2
# BB#3:
	testl	%edi, %edi
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	je	.LBB1_4
# BB#5:
	leaq	(,%rbx,8), %rdi
	movl	$16, %esi
	movl	%r8d, %ebx
	callq	_Z22btAlignedAllocInternalmi
	movl	%ebx, %r8d
	movq	%rax, %r12
	movl	20(%r13), %eax
	jmp	.LBB1_6
.LBB1_2:                                # %..lr.ph.i_crit_edge
	leaq	32(%r13), %rbp
	jmp	.LBB1_16
.LBB1_4:
	xorl	%r12d, %r12d
	movl	%r8d, %eax
.LBB1_6:                                # %_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE8allocateEi.exit.i.i
	leaq	32(%r13), %rbp
	testl	%eax, %eax
	jle	.LBB1_11
# BB#7:                                 # %.lr.ph.i.i.i
	cltq
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB1_9
	.p2align	4, 0x90
.LBB1_8:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movq	(%rdi,%rcx,8), %rdi
	movq	%rdi, (%r12,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB1_8
.LBB1_9:                                # %.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB1_11
	.p2align	4, 0x90
.LBB1_10:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, (%r12,%rcx,8)
	movq	(%rbp), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movq	%rdx, 8(%r12,%rcx,8)
	movq	(%rbp), %rdx
	movq	16(%rdx,%rcx,8), %rdx
	movq	%rdx, 16(%r12,%rcx,8)
	movq	(%rbp), %rdx
	movq	24(%rdx,%rcx,8), %rdx
	movq	%rdx, 24(%r12,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB1_10
.LBB1_11:                               # %_ZNK20btAlignedObjectArrayIP20btCollisionAlgorithmE4copyEiiPS1_.exit.i.i
	movq	32(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB1_15
# BB#12:
	cmpb	$0, 40(%r13)
	je	.LBB1_14
# BB#13:
	movl	%r8d, %ebx
	callq	_Z21btAlignedFreeInternalPv
	movl	%ebx, %r8d
.LBB1_14:
	movq	$0, (%rbp)
.LBB1_15:                               # %_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7reserveEi.exit.preheader.i
	movb	$1, 40(%r13)
	movq	%r12, 32(%r13)
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%edi, 24(%r13)
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB1_16:                               # %.lr.ph.i
	movl	%ebx, %ecx
	subl	%r8d, %ecx
	leaq	-1(%rbx), %rax
	subq	%r14, %rax
	andq	$7, %rcx
	je	.LBB1_19
# BB#17:                                # %_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7reserveEi.exit.i.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB1_18:                               # %_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7reserveEi.exit.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdx
	movq	$0, (%rdx,%r14,8)
	incq	%r14
	incq	%rcx
	jne	.LBB1_18
.LBB1_19:                               # %_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7reserveEi.exit.i.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB1_21
	.p2align	4, 0x90
.LBB1_20:                               # %_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE7reserveEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	$0, (%rax,%r14,8)
	movq	(%rbp), %rax
	movq	$0, 8(%rax,%r14,8)
	movq	(%rbp), %rax
	movq	$0, 16(%rax,%r14,8)
	movq	(%rbp), %rax
	movq	$0, 24(%rax,%r14,8)
	movq	(%rbp), %rax
	movq	$0, 32(%rax,%r14,8)
	movq	(%rbp), %rax
	movq	$0, 40(%rax,%r14,8)
	movq	(%rbp), %rax
	movq	$0, 48(%rax,%r14,8)
	movq	(%rbp), %rax
	movq	$0, 56(%rax,%r14,8)
	addq	$8, %r14
	cmpq	%r14, %rbx
	jne	.LBB1_20
.LBB1_21:                               # %_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmE6resizeEiRKS1_.exit
	movl	%edi, 20(%r13)
	testl	%edi, %edi
	jle	.LBB1_27
# BB#22:                                # %.lr.ph
	xorl	%ebp, %ebp
	movl	$8, %ebx
	.p2align	4, 0x90
.LBB1_23:                               # =>This Inner Loop Header: Depth=1
	cmpq	$0, 88(%r12)
	je	.LBB1_25
# BB#24:                                #   in Loop: Header=BB1_23 Depth=1
	movq	32(%r13), %rax
	movq	$0, (%rax,%rbp,8)
	jmp	.LBB1_26
	.p2align	4, 0x90
.LBB1_25:                               #   in Loop: Header=BB1_23 Depth=1
	movq	200(%r15), %r14
	movq	40(%r12), %rax
	movq	(%rax,%rbx,8), %rax
	movq	%rax, 200(%r15)
	movq	8(%r13), %rdi
	movq	56(%r13), %rcx
	movq	(%rdi), %rax
	movq	%r15, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	*16(%rax)
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	32(%r13), %rcx
	movq	%rax, (%rcx,%rbp,8)
	movq	%r14, 200(%r15)
.LBB1_26:                               #   in Loop: Header=BB1_23 Depth=1
	incq	%rbp
	addq	$11, %rbx
	cmpq	%rbp, %rdi
	jne	.LBB1_23
.LBB1_27:                               # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN28btCompoundCollisionAlgorithm26preallocateChildAlgorithmsEP17btCollisionObjectS1_, .Lfunc_end1-_ZN28btCompoundCollisionAlgorithm26preallocateChildAlgorithmsEP17btCollisionObjectS1_
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN28btCompoundCollisionAlgorithm21removeChildAlgorithmsEv
	.p2align	4, 0x90
	.type	_ZN28btCompoundCollisionAlgorithm21removeChildAlgorithmsEv,@function
_ZN28btCompoundCollisionAlgorithm21removeChildAlgorithmsEv: # @_ZN28btCompoundCollisionAlgorithm21removeChildAlgorithmsEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -32
.Lcfi27:
	.cfi_offset %r14, -24
.Lcfi28:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	20(%r15), %r14d
	testl	%r14d, %r14d
	jle	.LBB3_5
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movq	32(%r15), %rax
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	8(%r15), %rdi
	movq	32(%r15), %rax
	movq	(%rdi), %rcx
	movq	(%rax,%rbx,8), %rsi
	callq	*104(%rcx)
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB3_2
.LBB3_5:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_ZN28btCompoundCollisionAlgorithm21removeChildAlgorithmsEv, .Lfunc_end3-_ZN28btCompoundCollisionAlgorithm21removeChildAlgorithmsEv
	.cfi_endproc

	.globl	_ZN28btCompoundCollisionAlgorithmD2Ev
	.p2align	4, 0x90
	.type	_ZN28btCompoundCollisionAlgorithmD2Ev,@function
_ZN28btCompoundCollisionAlgorithmD2Ev:  # @_ZN28btCompoundCollisionAlgorithmD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	$_ZTV28btCompoundCollisionAlgorithm+16, (%r15)
	movl	20(%r15), %r14d
	testl	%r14d, %r14d
	jle	.LBB4_6
# BB#1:                                 # %.lr.ph.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	32(%r15), %rax
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB4_5
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	(%rdi), %rax
.Ltmp8:
	callq	*(%rax)
.Ltmp9:
# BB#4:                                 # %.noexc
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	8(%r15), %rdi
	movq	32(%r15), %rax
	movq	(%rdi), %rcx
	movq	(%rax,%rbx,8), %rsi
.Ltmp10:
	callq	*104(%rcx)
.Ltmp11:
.LBB4_5:                                # %.noexc3
                                        #   in Loop: Header=BB4_2 Depth=1
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB4_2
.LBB4_6:                                # %_ZN28btCompoundCollisionAlgorithm21removeChildAlgorithmsEv.exit
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB4_10
# BB#7:
	cmpb	$0, 40(%r15)
	je	.LBB4_9
# BB#8:
.Ltmp15:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp16:
.LBB4_9:                                # %.noexc4
	movq	$0, 32(%r15)
.LBB4_10:
	movb	$1, 40(%r15)
	movq	$0, 32(%r15)
	movq	$0, 20(%r15)
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN30btActivatingCollisionAlgorithmD2Ev # TAILCALL
.LBB4_16:
.Ltmp17:
	movq	%rax, %r14
	jmp	.LBB4_17
.LBB4_11:
.Ltmp12:
	movq	%rax, %r14
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB4_15
# BB#12:
	cmpb	$0, 40(%r15)
	je	.LBB4_14
# BB#13:
.Ltmp13:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp14:
.LBB4_14:                               # %.noexc6
	movq	$0, 32(%r15)
.LBB4_15:                               # %_ZN20btAlignedObjectArrayIP20btCollisionAlgorithmED2Ev.exit7
	movb	$1, 40(%r15)
	movq	$0, 32(%r15)
	movq	$0, 20(%r15)
.LBB4_17:
.Ltmp18:
	movq	%r15, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp19:
# BB#18:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_19:
.Ltmp20:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN28btCompoundCollisionAlgorithmD2Ev, .Lfunc_end4-_ZN28btCompoundCollisionAlgorithmD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp11-.Ltmp8          #   Call between .Ltmp8 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin1   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin1   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp13-.Ltmp16         #   Call between .Ltmp16 and .Ltmp13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp19-.Ltmp13         #   Call between .Ltmp13 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin1   #     jumps to .Ltmp20
	.byte	1                       #   On action: 1
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end4-.Ltmp19     #   Call between .Ltmp19 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN28btCompoundCollisionAlgorithmD0Ev
	.p2align	4, 0x90
	.type	_ZN28btCompoundCollisionAlgorithmD0Ev,@function
_ZN28btCompoundCollisionAlgorithmD0Ev:  # @_ZN28btCompoundCollisionAlgorithmD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp21:
	callq	_ZN28btCompoundCollisionAlgorithmD2Ev
.Ltmp22:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB5_2:
.Ltmp23:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZN28btCompoundCollisionAlgorithmD0Ev, .Lfunc_end5-_ZN28btCompoundCollisionAlgorithmD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin2   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end5-.Ltmp22     #   Call between .Ltmp22 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN28btCompoundCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN28btCompoundCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN28btCompoundCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN28btCompoundCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	subq	$424, %rsp              # imm = 0x1A8
.Lcfi46:
	.cfi_def_cfa_offset 480
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%r8, %rbp
	movq	%rsi, %r12
	movq	%rdi, %rbx
	cmpb	$0, 48(%rbx)
	movq	%r12, %r13
	cmovneq	%rdx, %r13
	movq	%rdx, %rax
	cmovneq	%r12, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	200(%r13), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	96(%rax), %eax
	cmpl	68(%rbx), %eax
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	je	.LBB6_7
# BB#1:
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movl	20(%rbx), %ebx
	testl	%ebx, %ebx
	jle	.LBB6_6
# BB#2:                                 # %.lr.ph.i
	xorl	%r15d, %r15d
	movq	80(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_3:                                # =>This Inner Loop Header: Depth=1
	movq	32(%r14), %rax
	movq	(%rax,%r15,8), %rdi
	testq	%rdi, %rdi
	je	.LBB6_5
# BB#4:                                 #   in Loop: Header=BB6_3 Depth=1
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	8(%r14), %rdi
	movq	32(%r14), %rax
	movq	(%rdi), %rcx
	movq	(%rax,%r15,8), %rsi
	callq	*104(%rcx)
.LBB6_5:                                #   in Loop: Header=BB6_3 Depth=1
	incq	%r15
	cmpq	%r15, %rbx
	jne	.LBB6_3
.LBB6_6:                                # %_ZN28btCompoundCollisionAlgorithm21removeChildAlgorithmsEv.exit
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	112(%rsp), %rdx         # 8-byte Reload
	callq	_ZN28btCompoundCollisionAlgorithm26preallocateChildAlgorithmsEP17btCollisionObjectS1_
	movq	128(%rsp), %rcx         # 8-byte Reload
.LBB6_7:                                # %._crit_edge298
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	88(%rax), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	8(%rbx), %rax
	movq	32(%rbx), %rdi
	movq	56(%rbx), %rdx
	movq	$_ZTV22btCompoundLeafCallback+16, 304(%rsp)
	movq	%r13, 312(%rsp)
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, 320(%rsp)
	movq	%rax, 328(%rsp)
	movq	%rcx, 336(%rsp)
	movq	%rbp, 344(%rsp)
	movq	%rdi, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movb	$1, 32(%rsp)
	movq	$0, 24(%rsp)
	movq	$0, 12(%rsp)
	movl	20(%rbx), %eax
	testl	%eax, %eax
	movq	%rbx, %r12
	jle	.LBB6_27
# BB#8:                                 # %.lr.ph285
	leaq	16(%rbp), %r15
	leaq	80(%rbp), %rbx
	xorl	%r14d, %r14d
	jmp	.LBB6_9
	.p2align	4, 0x90
.LBB6_16:                               # %._crit_edge
                                        #   in Loop: Header=BB6_9 Depth=1
	testq	%rdi, %rdi
	movq	80(%rsp), %r12          # 8-byte Reload
	je	.LBB6_20
# BB#17:                                #   in Loop: Header=BB6_9 Depth=1
	cmpb	$0, 32(%rsp)
	je	.LBB6_19
# BB#18:                                #   in Loop: Header=BB6_9 Depth=1
.Ltmp31:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp32:
.LBB6_19:                               # %.noexc113
                                        #   in Loop: Header=BB6_9 Depth=1
	movq	$0, 24(%rsp)
.LBB6_20:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv.exit
                                        #   in Loop: Header=BB6_9 Depth=1
	movb	$1, 32(%rsp)
	movq	$0, 24(%rsp)
	movq	$0, 12(%rsp)
	movl	20(%r12), %eax
.LBB6_21:                               #   in Loop: Header=BB6_9 Depth=1
	incq	%r14
	movslq	%eax, %rcx
	cmpq	%rcx, %r14
	jge	.LBB6_27
# BB#22:                                # %._crit_edge294
                                        #   in Loop: Header=BB6_9 Depth=1
	movq	32(%r12), %rdi
.LBB6_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_13 Depth 2
	movq	(%rdi,%r14,8), %rdi
	testq	%rdi, %rdi
	je	.LBB6_21
# BB#10:                                #   in Loop: Header=BB6_9 Depth=1
	movq	(%rdi), %rax
.Ltmp24:
	leaq	8(%rsp), %rsi
	callq	*32(%rax)
.Ltmp25:
# BB#11:                                # %.preheader
                                        #   in Loop: Header=BB6_9 Depth=1
	movl	12(%rsp), %ecx
	movq	24(%rsp), %rdi
	testl	%ecx, %ecx
	jle	.LBB6_16
# BB#12:                                # %.lr.ph282.preheader
                                        #   in Loop: Header=BB6_9 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB6_13:                               # %.lr.ph282
                                        #   Parent Loop BB6_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%r12,8), %rax
	cmpl	$0, 728(%rax)
	je	.LBB6_26
# BB#14:                                #   in Loop: Header=BB6_13 Depth=2
	movq	%rax, 8(%rbp)
	movq	712(%rax), %rcx
	cmpq	144(%rbp), %rcx
	je	.LBB6_24
# BB#15:                                #   in Loop: Header=BB6_13 Depth=2
.Ltmp26:
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_
.Ltmp27:
	jmp	.LBB6_25
	.p2align	4, 0x90
.LBB6_24:                               #   in Loop: Header=BB6_13 Depth=2
.Ltmp28:
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_
.Ltmp29:
.LBB6_25:                               # %_ZN16btManifoldResult20refreshContactPointsEv.exit
                                        #   in Loop: Header=BB6_13 Depth=2
	movq	$0, 8(%rbp)
	movl	12(%rsp), %ecx
	movq	24(%rsp), %rdi
.LBB6_26:                               #   in Loop: Header=BB6_13 Depth=2
	incq	%r12
	movslq	%ecx, %rax
	cmpq	%rax, %r12
	jl	.LBB6_13
	jmp	.LBB6_16
.LBB6_27:                               # %._crit_edge286
	movq	128(%rsp), %rbx         # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB6_37
# BB#28:
	movsd	8(%r13), %xmm8          # xmm8 = mem[0],zero
	movsd	24(%r13), %xmm6         # xmm6 = mem[0],zero
	movsd	40(%r13), %xmm3         # xmm3 = mem[0],zero
	movss	16(%r13), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	%xmm7, 176(%rsp)        # 4-byte Spill
	movss	32(%r13), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	48(%r13), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	%xmm10, 4(%rsp)         # 4-byte Spill
	movd	56(%r13), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movdqa	.LCPI6_0(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm0, %xmm13
	movd	60(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movdqa	%xmm1, %xmm2
	pxor	%xmm0, %xmm2
	movd	64(%r13), %xmm4         # xmm4 = mem[0],zero,zero,zero
	pxor	%xmm4, %xmm0
	pshufd	$224, %xmm13, %xmm5     # xmm5 = xmm13[0,0,2,3]
	mulps	%xmm8, %xmm5
	pshufd	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm6, %xmm2
	addps	%xmm5, %xmm2
	pshufd	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm3, %xmm0
	addps	%xmm2, %xmm0
	movaps	%xmm0, 112(%rsp)        # 16-byte Spill
	mulss	%xmm7, %xmm13
	mulss	%xmm9, %xmm1
	subss	%xmm1, %xmm13
	mulss	%xmm10, %xmm4
	subss	%xmm4, %xmm13
	movq	72(%rsp), %rax          # 8-byte Reload
	movss	8(%rax), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm2
	movaps	%xmm2, %xmm0
	mulss	%xmm12, %xmm0
	movss	24(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm1
	mulss	%xmm7, %xmm1
	addss	%xmm0, %xmm1
	movss	40(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	%xmm4, 80(%rsp)         # 4-byte Spill
	movaps	%xmm3, %xmm0
	mulss	%xmm4, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 240(%rsp)        # 16-byte Spill
	movaps	%xmm2, %xmm0
	movaps	%xmm2, 368(%rsp)        # 16-byte Spill
	mulss	%xmm14, %xmm0
	movss	28(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm4
	movaps	%xmm6, %xmm10
	movaps	%xmm10, 384(%rsp)       # 16-byte Spill
	mulss	%xmm5, %xmm4
	addss	%xmm0, %xmm4
	movss	44(%rax), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	movaps	%xmm3, %xmm1
	mulss	%xmm15, %xmm0
	addss	%xmm4, %xmm0
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	movss	16(%rax), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm6
	mulss	%xmm11, %xmm6
	movss	32(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm8
	mulss	%xmm4, %xmm8
	addss	%xmm6, %xmm8
	movss	48(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	movaps	%xmm1, 400(%rsp)        # 16-byte Spill
	mulss	%xmm6, %xmm0
	addss	%xmm8, %xmm0
	movaps	%xmm0, 208(%rsp)        # 16-byte Spill
	pshufd	$229, %xmm2, %xmm3      # xmm3 = xmm2[1,1,2,3]
	movdqa	%xmm3, %xmm8
	mulss	%xmm12, %xmm8
	pshufd	$229, %xmm10, %xmm2     # xmm2 = xmm10[1,1,2,3]
	movdqa	%xmm2, %xmm0
	mulss	%xmm7, %xmm0
	addss	%xmm8, %xmm0
	pshufd	$229, %xmm1, %xmm10     # xmm10 = xmm1[1,1,2,3]
	movdqa	%xmm10, %xmm1
	mulss	80(%rsp), %xmm1         # 4-byte Folded Reload
	addss	%xmm0, %xmm1
	movaps	%xmm1, 192(%rsp)        # 16-byte Spill
	movdqa	%xmm3, %xmm0
	mulss	%xmm14, %xmm0
	movdqa	%xmm2, %xmm1
	mulss	%xmm5, %xmm1
	addss	%xmm0, %xmm1
	movdqa	%xmm10, %xmm8
	mulss	%xmm15, %xmm8
	addss	%xmm1, %xmm8
	mulss	%xmm11, %xmm3
	mulss	%xmm4, %xmm2
	addss	%xmm3, %xmm2
	mulss	%xmm6, %xmm10
	addss	%xmm2, %xmm10
	movss	176(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm12
	mulss	%xmm9, %xmm7
	addss	%xmm12, %xmm7
	movss	4(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	80(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	%xmm7, %xmm2
	movaps	%xmm2, %xmm7
	mulss	%xmm0, %xmm14
	mulss	%xmm9, %xmm5
	addss	%xmm14, %xmm5
	mulss	%xmm1, %xmm15
	addss	%xmm5, %xmm15
	mulss	%xmm0, %xmm11
	movaps	%xmm0, %xmm2
	mulss	%xmm9, %xmm4
	addss	%xmm11, %xmm4
	mulss	%xmm1, %xmm6
	movaps	%xmm1, %xmm3
	addss	%xmm4, %xmm6
	movss	56(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	368(%rsp), %xmm0        # 16-byte Folded Reload
	movss	60(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm9
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	384(%rsp), %xmm1        # 16-byte Folded Reload
	addps	%xmm0, %xmm1
	movss	64(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	400(%rsp), %xmm0        # 16-byte Folded Reload
	addps	%xmm1, %xmm0
	addps	112(%rsp), %xmm0        # 16-byte Folded Reload
	addss	%xmm2, %xmm9
	addss	%xmm3, %xmm9
	addss	%xmm13, %xmm9
	xorps	%xmm1, %xmm1
	movss	%xmm9, %xmm1            # xmm1 = xmm9[0],xmm1[1,2,3]
	movaps	240(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 8(%rsp)
	movaps	224(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 12(%rsp)
	movaps	208(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 16(%rsp)
	movl	$0, 20(%rsp)
	movaps	192(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 24(%rsp)
	movss	%xmm8, 28(%rsp)
	movss	%xmm10, 32(%rsp)
	movl	$0, 36(%rsp)
	movss	%xmm7, 40(%rsp)
	movss	%xmm15, 44(%rsp)
	movss	%xmm6, 48(%rsp)
	movl	$0, 52(%rsp)
	movlps	%xmm0, 56(%rsp)
	movlps	%xmm1, 64(%rsp)
	movq	200(%rax), %rdi
	movq	(%rdi), %rax
.Ltmp37:
	leaq	8(%rsp), %rsi
	leaq	160(%rsp), %rdx
	leaq	144(%rsp), %rcx
	callq	*16(%rax)
.Ltmp38:
# BB#29:
	movups	160(%rsp), %xmm0
	movaps	%xmm0, 272(%rsp)
	movups	144(%rsp), %xmm0
	movaps	%xmm0, 288(%rsp)
	movq	(%rbx), %rsi
.Ltmp40:
	leaq	272(%rsp), %rdx
	leaq	304(%rsp), %rcx
	movq	%rbx, %rdi
	callq	_ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE
.Ltmp41:
	jmp	.LBB6_41
.LBB6_37:
	movslq	20(%r12), %r15
	testq	%r15, %r15
	jle	.LBB6_61
# BB#38:                                # %.lr.ph280
	xorl	%ebx, %ebx
	movl	$64, %ebp
	leaq	304(%rsp), %r14
	.p2align	4, 0x90
.LBB6_39:                               # =>This Inner Loop Header: Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	40(%rax), %rax
	movq	(%rax,%rbp), %rsi
.Ltmp43:
	movq	%r14, %rdi
	movl	%ebx, %edx
	callq	_ZN22btCompoundLeafCallback17ProcessChildShapeEP16btCollisionShapei
.Ltmp44:
# BB#40:                                #   in Loop: Header=BB6_39 Depth=1
	incq	%rbx
	addq	$88, %rbp
	cmpq	%r15, %rbx
	jl	.LBB6_39
.LBB6_41:                               # %.loopexit
	movslq	20(%r12), %rbx
	testq	%rbx, %rbx
	jle	.LBB6_61
# BB#42:                                # %.lr.ph
	movq	72(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %r14
	xorl	%ebp, %ebp
	movl	$8, %r15d
	.p2align	4, 0x90
.LBB6_43:                               # =>This Inner Loop Header: Depth=1
	movq	32(%r12), %rax
	cmpq	$0, (%rax,%rbp,8)
	je	.LBB6_60
# BB#44:                                #   in Loop: Header=BB6_43 Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	40(%rax), %rax
	movss	8(%r13), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, 176(%rsp)        # 16-byte Spill
	movss	12(%r13), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, 192(%rsp)        # 16-byte Spill
	movss	16(%r13), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	-64(%rax,%r15,8), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm4
	movss	%xmm4, 208(%rsp)        # 4-byte Spill
	movss	-48(%rax,%r15,8), %xmm11 # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm1
	mulss	%xmm11, %xmm1
	addss	%xmm0, %xmm1
	movss	-32(%rax,%r15,8), %xmm14 # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm14, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movss	-60(%rax,%r15,8), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm1, %xmm0
	movss	%xmm1, 4(%rsp)          # 4-byte Spill
	movss	-44(%rax,%r15,8), %xmm9 # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm2
	mulss	%xmm9, %xmm2
	addss	%xmm0, %xmm2
	movss	-28(%rax,%r15,8), %xmm12 # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	movaps	%xmm3, %xmm5
	movaps	%xmm5, 224(%rsp)        # 16-byte Spill
	mulss	%xmm12, %xmm0
	addss	%xmm2, %xmm0
	movaps	%xmm0, 128(%rsp)        # 16-byte Spill
	movss	-56(%rax,%r15,8), %xmm15 # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm2
	mulss	%xmm15, %xmm2
	movss	-40(%rax,%r15,8), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm3
	mulss	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	movss	-24(%rax,%r15,8), %xmm7 # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm2
	mulss	%xmm7, %xmm2
	addss	%xmm3, %xmm2
	movaps	%xmm2, 112(%rsp)        # 16-byte Spill
	movss	24(%r13), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm6
	mulss	%xmm4, %xmm6
	movss	28(%r13), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm3
	mulss	%xmm11, %xmm3
	addss	%xmm6, %xmm3
	movss	32(%r13), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm5
	mulss	%xmm14, %xmm5
	addss	%xmm3, %xmm5
	movaps	%xmm5, 240(%rsp)        # 16-byte Spill
	movaps	%xmm2, %xmm3
	mulss	%xmm1, %xmm3
	movaps	%xmm8, %xmm5
	mulss	%xmm9, %xmm5
	addss	%xmm3, %xmm5
	movaps	%xmm10, %xmm13
	mulss	%xmm12, %xmm13
	addss	%xmm5, %xmm13
	movaps	176(%rsp), %xmm1        # 16-byte Reload
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movaps	192(%rsp), %xmm6        # 16-byte Reload
	unpcklps	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1]
	mulss	%xmm15, %xmm2
	mulss	%xmm0, %xmm8
	addss	%xmm2, %xmm8
	movaps	224(%rsp), %xmm4        # 16-byte Reload
	unpcklps	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1]
	mulss	%xmm7, %xmm10
	addss	%xmm8, %xmm10
	movss	40(%r13), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	208(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	movss	44(%r13), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm11
	addss	%xmm5, %xmm11
	movss	48(%r13), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm14
	addss	%xmm11, %xmm14
	movss	4(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm8
	mulss	%xmm3, %xmm9
	addss	%xmm8, %xmm9
	mulss	%xmm5, %xmm12
	addss	%xmm9, %xmm12
	mulss	%xmm2, %xmm15
	mulss	%xmm3, %xmm0
	addss	%xmm15, %xmm0
	mulss	%xmm5, %xmm7
	addss	%xmm0, %xmm7
	movss	-16(%rax,%r15,8), %xmm0 # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm1, %xmm0
	movss	-12(%rax,%r15,8), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm6, %xmm1
	addps	%xmm0, %xmm1
	movss	-8(%rax,%r15,8), %xmm0  # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm4, %xmm0
	addps	%xmm1, %xmm0
	movsd	56(%r13), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm1, %xmm0
	addss	%xmm2, %xmm3
	addss	%xmm5, %xmm3
	addss	64(%r13), %xmm3
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	movq	(%rax,%r15,8), %rdi
	movaps	80(%rsp), %xmm2         # 16-byte Reload
	movss	%xmm2, 8(%rsp)
	movaps	128(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 12(%rsp)
	movaps	112(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 16(%rsp)
	movl	$0, 20(%rsp)
	movaps	240(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 24(%rsp)
	movss	%xmm13, 28(%rsp)
	movss	%xmm10, 32(%rsp)
	movl	$0, 36(%rsp)
	movss	%xmm14, 40(%rsp)
	movss	%xmm12, 44(%rsp)
	movss	%xmm7, 48(%rsp)
	movl	$0, 52(%rsp)
	movlps	%xmm0, 56(%rsp)
	movlps	%xmm1, 64(%rsp)
	movq	(%rdi), %rax
.Ltmp46:
	leaq	8(%rsp), %rsi
	leaq	272(%rsp), %rdx
	leaq	160(%rsp), %rcx
	callq	*16(%rax)
.Ltmp47:
# BB#45:                                #   in Loop: Header=BB6_43 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	200(%rax), %rdi
	movq	(%rdi), %rax
.Ltmp48:
	movq	%r14, %rsi
	leaq	144(%rsp), %rdx
	leaq	256(%rsp), %rcx
	callq	*16(%rax)
.Ltmp49:
# BB#46:                                #   in Loop: Header=BB6_43 Depth=1
	movss	272(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	256(%rsp), %xmm0
	jbe	.LBB6_48
# BB#47:                                #   in Loop: Header=BB6_43 Depth=1
	xorl	%eax, %eax
	jmp	.LBB6_51
	.p2align	4, 0x90
.LBB6_48:                               #   in Loop: Header=BB6_43 Depth=1
	movss	144(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	160(%rsp), %xmm0
	jbe	.LBB6_50
# BB#49:                                #   in Loop: Header=BB6_43 Depth=1
	xorl	%eax, %eax
	jmp	.LBB6_51
.LBB6_50:                               #   in Loop: Header=BB6_43 Depth=1
	movb	$1, %al
.LBB6_51:                               #   in Loop: Header=BB6_43 Depth=1
	movss	280(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	264(%rsp), %xmm0
	ja	.LBB6_53
# BB#52:                                #   in Loop: Header=BB6_43 Depth=1
	movss	152(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	168(%rsp), %xmm0
	jbe	.LBB6_54
.LBB6_53:                               #   in Loop: Header=BB6_43 Depth=1
	xorl	%eax, %eax
.LBB6_54:                               #   in Loop: Header=BB6_43 Depth=1
	movss	276(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	260(%rsp), %xmm0
	jbe	.LBB6_55
.LBB6_57:                               # %_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_.exit.thread
                                        #   in Loop: Header=BB6_43 Depth=1
	movq	32(%r12), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	(%rdi), %rax
.Ltmp50:
	callq	*(%rax)
.Ltmp51:
# BB#58:                                #   in Loop: Header=BB6_43 Depth=1
	movq	8(%r12), %rdi
	movq	32(%r12), %rax
	movq	(%rdi), %rcx
	movq	(%rax,%rbp,8), %rsi
.Ltmp52:
	callq	*104(%rcx)
.Ltmp53:
# BB#59:                                #   in Loop: Header=BB6_43 Depth=1
	movq	32(%r12), %rax
	movq	$0, (%rax,%rbp,8)
	jmp	.LBB6_60
	.p2align	4, 0x90
.LBB6_55:                               #   in Loop: Header=BB6_43 Depth=1
	movss	148(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	164(%rsp), %xmm0
	ja	.LBB6_57
# BB#56:                                #   in Loop: Header=BB6_43 Depth=1
	xorb	$1, %al
	jne	.LBB6_57
	.p2align	4, 0x90
.LBB6_60:                               #   in Loop: Header=BB6_43 Depth=1
	incq	%rbp
	addq	$11, %r15
	cmpq	%rbx, %rbp
	jl	.LBB6_43
.LBB6_61:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev.exit109
	addq	$424, %rsp              # imm = 0x1A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_36:
.Ltmp42:
	jmp	.LBB6_63
.LBB6_35:
.Ltmp39:
	jmp	.LBB6_63
.LBB6_66:
.Ltmp45:
	jmp	.LBB6_63
.LBB6_30:
.Ltmp33:
	jmp	.LBB6_31
.LBB6_62:
.Ltmp54:
.LBB6_63:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB6_23:
.Ltmp30:
.LBB6_31:
	movq	%rax, %rbx
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB6_32
# BB#64:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB6_32:
	cmpb	$0, 32(%rsp)
	je	.LBB6_34
# BB#33:
.Ltmp34:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp35:
.LBB6_34:                               # %.noexc111
	movq	$0, 24(%rsp)
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB6_65:
.Ltmp36:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN28btCompoundCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end6-_ZN28btCompoundCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp31-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp25-.Ltmp31         #   Call between .Ltmp31 and .Ltmp25
	.long	.Ltmp33-.Lfunc_begin3   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp29-.Ltmp26         #   Call between .Ltmp26 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin3   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin3   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin3   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin3   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp53-.Ltmp46         #   Call between .Ltmp46 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin3   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp34-.Ltmp53         #   Call between .Ltmp53 and .Ltmp34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin3   #     jumps to .Ltmp36
	.byte	1                       #   On action: 1
	.long	.Ltmp35-.Lfunc_begin3   # >> Call Site 10 <<
	.long	.Lfunc_end6-.Ltmp35     #   Call between .Ltmp35 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE,"axG",@progbits,_ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE,comdat
	.weak	_ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE
	.p2align	4, 0x90
	.type	_ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE,@function
_ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE: # @_ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi59:
	.cfi_def_cfa_offset 144
.Lcfi60:
	.cfi_offset %rbx, -56
.Lcfi61:
	.cfi_offset %r12, -48
.Lcfi62:
	.cfi_offset %r13, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	testq	%rbp, %rbp
	je	.LBB7_79
# BB#1:
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 28(%rsp)         # 4-byte Spill
	movss	4(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 24(%rsp)         # 4-byte Spill
	movss	8(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 76(%rsp)         # 4-byte Spill
	movss	16(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 20(%rsp)         # 4-byte Spill
	movss	20(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movss	24(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
.Ltmp55:
	movl	$512, %edi              # imm = 0x200
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r13
.Ltmp56:
# BB#2:                                 # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_.exit
	movq	%rbp, (%r13)
	movl	$64, %esi
	movl	$1, %ecx
	movl	$1, %r14d
	movq	%r13, %r10
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%r13, %r9
	movq	%r13, %r11
	movq	%r13, %r8
	movq	%r13, %rbx
	movss	28(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	jmp	.LBB7_3
.LBB7_60:                               # %vector.body.preheader
                                        #   in Loop: Header=BB7_3 Depth=1
	leaq	-4(%rax), %rcx
	movl	%ecx, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB7_63
# BB#61:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB7_3 Depth=1
	negq	%rdx
	xorl	%esi, %esi
.LBB7_62:                               # %vector.body.prol
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rsi,8), %xmm0
	movups	16(%r12,%rsi,8), %xmm1
	movups	%xmm0, (%r13,%rsi,8)
	movups	%xmm1, 16(%r13,%rsi,8)
	addq	$4, %rsi
	incq	%rdx
	jne	.LBB7_62
	jmp	.LBB7_64
.LBB7_28:                               # %vector.body191.preheader
                                        #   in Loop: Header=BB7_3 Depth=1
	leaq	-4(%rax), %rcx
	movl	%ecx, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB7_31
# BB#29:                                # %vector.body191.prol.preheader
                                        #   in Loop: Header=BB7_3 Depth=1
	negq	%rdx
	xorl	%esi, %esi
.LBB7_30:                               # %vector.body191.prol
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbx,%rsi,8), %xmm0
	movups	16(%rbx,%rsi,8), %xmm1
	movups	%xmm0, (%r13,%rsi,8)
	movups	%xmm1, 16(%r13,%rsi,8)
	addq	$4, %rsi
	incq	%rdx
	jne	.LBB7_30
	jmp	.LBB7_32
.LBB7_63:                               #   in Loop: Header=BB7_3 Depth=1
	xorl	%esi, %esi
.LBB7_64:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB7_3 Depth=1
	cmpq	$12, %rcx
	jb	.LBB7_67
# BB#65:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%r12,%rsi,8), %rdx
	leaq	112(%r13,%rsi,8), %rsi
.LBB7_66:                               # %vector.body
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-16, %rcx
	jne	.LBB7_66
.LBB7_67:                               # %middle.block
                                        #   in Loop: Header=BB7_3 Depth=1
	cmpq	%rax, %r15
	je	.LBB7_74
	jmp	.LBB7_68
.LBB7_31:                               #   in Loop: Header=BB7_3 Depth=1
	xorl	%esi, %esi
.LBB7_32:                               # %vector.body191.prol.loopexit
                                        #   in Loop: Header=BB7_3 Depth=1
	cmpq	$12, %rcx
	jb	.LBB7_35
# BB#33:                                # %vector.body191.preheader.new
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%rbx,%rsi,8), %rdx
	leaq	112(%r13,%rsi,8), %rsi
.LBB7_34:                               # %vector.body191
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-16, %rcx
	jne	.LBB7_34
.LBB7_35:                               # %middle.block192
                                        #   in Loop: Header=BB7_3 Depth=1
	cmpq	%rax, 32(%rsp)          # 8-byte Folded Reload
	je	.LBB7_42
	jmp	.LBB7_36
	.p2align	4, 0x90
.LBB7_3:                                # %_Z9IntersectRK12btDbvtAabbMmS1_.exit.thread._crit_edge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_30 Depth 2
                                        #     Child Loop BB7_34 Depth 2
                                        #     Child Loop BB7_38 Depth 2
                                        #     Child Loop BB7_41 Depth 2
                                        #     Child Loop BB7_62 Depth 2
                                        #     Child Loop BB7_66 Depth 2
                                        #     Child Loop BB7_70 Depth 2
                                        #     Child Loop BB7_73 Depth 2
	leal	-1(%r14), %r12d
	movslq	%r14d, %r15
	movq	-8(%rbx,%r15,8), %rbp
	ucomiss	(%rbp), %xmm3
	jb	.LBB7_76
# BB#4:                                 #   in Loop: Header=BB7_3 Depth=1
	movss	16(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jb	.LBB7_76
# BB#5:                                 #   in Loop: Header=BB7_3 Depth=1
	ucomiss	4(%rbp), %xmm4
	jb	.LBB7_76
# BB#6:                                 #   in Loop: Header=BB7_3 Depth=1
	movss	20(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	jb	.LBB7_76
# BB#7:                                 #   in Loop: Header=BB7_3 Depth=1
	ucomiss	8(%rbp), %xmm5
	jb	.LBB7_76
# BB#8:                                 # %_Z9IntersectRK12btDbvtAabbMmS1_.exit
                                        #   in Loop: Header=BB7_3 Depth=1
	movss	24(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	76(%rsp), %xmm0         # 4-byte Folded Reload
	jb	.LBB7_76
# BB#9:                                 #   in Loop: Header=BB7_3 Depth=1
	cmpq	$0, 48(%rbp)
	je	.LBB7_13
# BB#10:                                #   in Loop: Header=BB7_3 Depth=1
	leaq	-1(%r15), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	40(%rbp), %rdx
	cmpl	%esi, %r12d
	jne	.LBB7_12
# BB#11:                                #   in Loop: Header=BB7_3 Depth=1
	leal	(%rsi,%rsi), %edi
	testl	%esi, %esi
	cmovel	%ecx, %edi
	cmpl	%edi, %r14d
	jle	.LBB7_15
.LBB7_12:                               #   in Loop: Header=BB7_3 Depth=1
	movl	%esi, %edi
	movq	%rbx, %r12
	jmp	.LBB7_43
.LBB7_13:                               #   in Loop: Header=BB7_3 Depth=1
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	%r8, %r14
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	%r11, %r13
	movq	%r10, 40(%rsp)          # 8-byte Spill
	movq	%r9, %r15
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp68:
	movq	%rbp, %rsi
	callq	*24(%rax)
.Ltmp69:
# BB#14:                                #   in Loop: Header=BB7_3 Depth=1
	movss	28(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movl	$1, %ecx
	movq	%r15, %r9
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	%r13, %r11
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	%r14, %r8
	movq	56(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB7_76
.LBB7_15:                               #   in Loop: Header=BB7_3 Depth=1
	testl	%edi, %edi
	movq	%r10, 40(%rsp)          # 8-byte Spill
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	je	.LBB7_17
# BB#16:                                #   in Loop: Header=BB7_3 Depth=1
	movl	%edi, %r13d
	movslq	%r13d, %rdi
	shlq	$3, %rdi
.Ltmp58:
	movl	$16, %esi
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movq	%r11, 64(%rsp)          # 8-byte Spill
	callq	_Z22btAlignedAllocInternalmi
	movq	64(%rsp), %r11          # 8-byte Reload
	movq	48(%rsp), %r9           # 8-byte Reload
	movl	%r13d, %edi
	movl	$1, %ecx
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	28(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movq	%rax, %r13
.Ltmp59:
	jmp	.LBB7_18
.LBB7_17:                               #   in Loop: Header=BB7_3 Depth=1
	xorl	%r13d, %r13d
.LBB7_18:                               # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8allocateEi.exit.i.i30
                                        #   in Loop: Header=BB7_3 Depth=1
	cmpl	$2, %r14d
	jl	.LBB7_21
# BB#19:                                # %.lr.ph.i.i.i32.preheader
                                        #   in Loop: Header=BB7_3 Depth=1
	cmpl	$4, %r12d
	jae	.LBB7_23
# BB#20:                                #   in Loop: Header=BB7_3 Depth=1
	xorl	%eax, %eax
	jmp	.LBB7_36
.LBB7_21:                               # %_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_.exit.i.i37
                                        #   in Loop: Header=BB7_3 Depth=1
	testq	%rbx, %rbx
	jne	.LBB7_42
# BB#22:                                #   in Loop: Header=BB7_3 Depth=1
	movq	%r13, %r10
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%r13, %r9
	movq	%r13, %r11
	movq	%r13, %r8
	movq	%r13, %r12
	movq	56(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB7_43
.LBB7_23:                               # %min.iters.checked195
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	andq	$-4, %rax
	je	.LBB7_27
# BB#24:                                # %vector.memcheck208
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rbx,%rcx,8), %rcx
	cmpq	%rcx, %r13
	jae	.LBB7_28
# BB#25:                                # %vector.memcheck208
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%r13,%rcx,8), %rcx
	cmpq	%rcx, %r11
	jae	.LBB7_28
.LBB7_27:                               #   in Loop: Header=BB7_3 Depth=1
	xorl	%eax, %eax
.LBB7_36:                               # %.lr.ph.i.i.i32.preheader220
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %edx
	subl	%eax, %edx
	decq	%rcx
	subq	%rax, %rcx
	andq	$7, %rdx
	je	.LBB7_39
# BB#37:                                # %.lr.ph.i.i.i32.prol.preheader
                                        #   in Loop: Header=BB7_3 Depth=1
	negq	%rdx
	.p2align	4, 0x90
.LBB7_38:                               # %.lr.ph.i.i.i32.prol
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rax,8), %rsi
	movq	%rsi, (%r13,%rax,8)
	incq	%rax
	incq	%rdx
	jne	.LBB7_38
.LBB7_39:                               # %.lr.ph.i.i.i32.prol.loopexit
                                        #   in Loop: Header=BB7_3 Depth=1
	cmpq	$7, %rcx
	jb	.LBB7_42
# BB#40:                                # %.lr.ph.i.i.i32.preheader220.new
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	subq	%rax, %rcx
	leaq	56(%rbx,%rax,8), %rdx
	leaq	56(%r13,%rax,8), %rax
	.p2align	4, 0x90
.LBB7_41:                               # %.lr.ph.i.i.i32
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-56(%rdx), %rsi
	movq	%rsi, -56(%rax)
	movq	-48(%rdx), %rsi
	movq	%rsi, -48(%rax)
	movq	-40(%rdx), %rsi
	movq	%rsi, -40(%rax)
	movq	-32(%rdx), %rsi
	movq	%rsi, -32(%rax)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rax)
	movq	-16(%rdx), %rsi
	movq	%rsi, -16(%rax)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rax)
	movq	(%rdx), %rsi
	movq	%rsi, (%rax)
	addq	$64, %rdx
	addq	$64, %rax
	addq	$-8, %rcx
	jne	.LBB7_41
.LBB7_42:                               # %_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_.exit.i.i37.thread
                                        #   in Loop: Header=BB7_3 Depth=1
.Ltmp60:
	movl	%edi, 64(%rsp)          # 4-byte Spill
	movq	%r9, %rdi
	callq	_Z21btAlignedFreeInternalPv
	movl	64(%rsp), %edi          # 4-byte Reload
	movl	$1, %ecx
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	28(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
.Ltmp61:
	movq	%r13, %r10
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%r13, %r9
	movq	%r13, %r11
	movq	%r13, %r8
	movq	%r13, %r12
	movq	56(%rsp), %rdx          # 8-byte Reload
.LBB7_43:                               #   in Loop: Header=BB7_3 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rdx, (%r12,%rax,8)
	movq	48(%rbp), %rdx
	cmpl	%edi, %r14d
	jne	.LBB7_48
# BB#44:                                #   in Loop: Header=BB7_3 Depth=1
	leal	(%r14,%r14), %edi
	testl	%r14d, %r14d
	cmovel	%ecx, %edi
	cmpl	%edi, %r14d
	jge	.LBB7_49
# BB#45:                                #   in Loop: Header=BB7_3 Depth=1
	testl	%edi, %edi
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	je	.LBB7_50
# BB#46:                                #   in Loop: Header=BB7_3 Depth=1
	movq	%r10, %rbp
	movl	%edi, %ebx
	movslq	%ebx, %rdi
	shlq	$3, %rdi
.Ltmp63:
	movl	$16, %esi
	movq	%r8, %r13
	callq	_Z22btAlignedAllocInternalmi
	movq	%r13, %r8
	movl	%ebx, %edi
	movl	$1, %ecx
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	28(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movq	%rax, %r13
.Ltmp64:
# BB#47:                                #   in Loop: Header=BB7_3 Depth=1
	movq	%rbp, %r10
	testl	%r14d, %r14d
	jg	.LBB7_51
	jmp	.LBB7_53
.LBB7_48:                               #   in Loop: Header=BB7_3 Depth=1
	movq	%r12, %rbx
	jmp	.LBB7_75
.LBB7_49:                               #   in Loop: Header=BB7_3 Depth=1
	movl	%r14d, %edi
	movq	%r12, %rbx
	jmp	.LBB7_75
.LBB7_50:                               #   in Loop: Header=BB7_3 Depth=1
	xorl	%r13d, %r13d
	testl	%r14d, %r14d
	jle	.LBB7_53
.LBB7_51:                               # %.lr.ph.i.i.i50
                                        #   in Loop: Header=BB7_3 Depth=1
	cmpl	$4, %r14d
	jae	.LBB7_55
# BB#52:                                #   in Loop: Header=BB7_3 Depth=1
	xorl	%eax, %eax
	jmp	.LBB7_68
.LBB7_53:                               # %_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_.exit.i.i55
                                        #   in Loop: Header=BB7_3 Depth=1
	testq	%r12, %r12
	jne	.LBB7_74
# BB#54:                                #   in Loop: Header=BB7_3 Depth=1
	movq	%r13, %r10
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%r13, %r9
	movq	%r13, %r11
	movq	%r13, %r8
	movq	%r13, %rbx
	movq	40(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB7_75
.LBB7_55:                               # %min.iters.checked
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	%r15, %rax
	andq	$-4, %rax
	je	.LBB7_59
# BB#56:                                # %vector.memcheck
                                        #   in Loop: Header=BB7_3 Depth=1
	leaq	(%r12,%r15,8), %rcx
	cmpq	%rcx, %r13
	jae	.LBB7_60
# BB#57:                                # %vector.memcheck
                                        #   in Loop: Header=BB7_3 Depth=1
	leaq	(%r13,%r15,8), %rcx
	cmpq	%rcx, %r8
	jae	.LBB7_60
.LBB7_59:                               #   in Loop: Header=BB7_3 Depth=1
	xorl	%eax, %eax
.LBB7_68:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB7_3 Depth=1
	movl	%r14d, %ecx
	subl	%eax, %ecx
	subq	%rax, 32(%rsp)          # 8-byte Folded Spill
	andq	$7, %rcx
	je	.LBB7_71
# BB#69:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB7_3 Depth=1
	negq	%rcx
	.p2align	4, 0x90
.LBB7_70:                               # %scalar.ph.prol
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%rax,8), %rdx
	movq	%rdx, (%r13,%rax,8)
	incq	%rax
	incq	%rcx
	jne	.LBB7_70
.LBB7_71:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB7_3 Depth=1
	cmpq	$7, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB7_74
# BB#72:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	%r15, %rcx
	subq	%rax, %rcx
	leaq	56(%r12,%rax,8), %rdx
	leaq	56(%r13,%rax,8), %rax
	.p2align	4, 0x90
.LBB7_73:                               # %scalar.ph
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-56(%rdx), %rsi
	movq	%rsi, -56(%rax)
	movq	-48(%rdx), %rsi
	movq	%rsi, -48(%rax)
	movq	-40(%rdx), %rsi
	movq	%rsi, -40(%rax)
	movq	-32(%rdx), %rsi
	movq	%rsi, -32(%rax)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rax)
	movq	-16(%rdx), %rsi
	movq	%rsi, -16(%rax)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rax)
	movq	(%rdx), %rsi
	movq	%rsi, (%rax)
	addq	$64, %rdx
	addq	$64, %rax
	addq	$-8, %rcx
	jne	.LBB7_73
.LBB7_74:                               # %_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_.exit.i.i55.thread
                                        #   in Loop: Header=BB7_3 Depth=1
.Ltmp65:
	movq	%r10, %rbp
	movl	%edi, %ebx
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	_Z21btAlignedFreeInternalPv
	movl	%ebx, %edi
	movl	$1, %ecx
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	28(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
.Ltmp66:
	movq	%r13, %r10
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%r13, %r9
	movq	%r13, %r11
	movq	%r13, %r8
	movq	%r13, %rbx
	movq	40(%rsp), %rdx          # 8-byte Reload
.LBB7_75:                               #   in Loop: Header=BB7_3 Depth=1
	movq	%rdx, (%rbx,%r15,8)
	incl	%r14d
	movl	%r14d, %r12d
	movl	%edi, %esi
	.p2align	4, 0x90
.LBB7_76:                               # %_Z9IntersectRK12btDbvtAabbMmS1_.exit.thread
                                        #   in Loop: Header=BB7_3 Depth=1
	testl	%r12d, %r12d
	movl	%r12d, %r14d
	jg	.LBB7_3
# BB#77:
	testq	%rbx, %rbx
	je	.LBB7_79
# BB#78:
	movq	%r13, %rdi
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB7_79:                               # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev.exit27
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_80:
.Ltmp62:
	jmp	.LBB7_83
.LBB7_81:
.Ltmp67:
	movq	%rax, %r14
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%r12, %rbx
	testq	%rbx, %rbx
	jne	.LBB7_84
	jmp	.LBB7_85
.LBB7_82:
.Ltmp70:
.LBB7_83:
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB7_85
.LBB7_84:
.Ltmp71:
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	_Z21btAlignedFreeInternalPv
.Ltmp72:
.LBB7_85:                               # %_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB7_86:
.Ltmp73:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_87:                               # %.thread
.Ltmp57:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE, .Lfunc_end7-_ZN6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp55-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin4   #     jumps to .Ltmp57
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin4   #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp61-.Ltmp58         #   Call between .Ltmp58 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin4   #     jumps to .Ltmp62
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp66-.Ltmp63         #   Call between .Ltmp63 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin4   #     jumps to .Ltmp67
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Ltmp71-.Ltmp66         #   Call between .Ltmp66 and .Ltmp71
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin4   # >> Call Site 6 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin4   #     jumps to .Ltmp73
	.byte	1                       #   On action: 1
	.long	.Ltmp72-.Lfunc_begin4   # >> Call Site 7 <<
	.long	.Lfunc_end7-.Ltmp72     #   Call between .Ltmp72 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN22btCompoundLeafCallback17ProcessChildShapeEP16btCollisionShapei,"axG",@progbits,_ZN22btCompoundLeafCallback17ProcessChildShapeEP16btCollisionShapei,comdat
	.weak	_ZN22btCompoundLeafCallback17ProcessChildShapeEP16btCollisionShapei
	.p2align	4, 0x90
	.type	_ZN22btCompoundLeafCallback17ProcessChildShapeEP16btCollisionShapei,@function
_ZN22btCompoundLeafCallback17ProcessChildShapeEP16btCollisionShapei: # @_ZN22btCompoundLeafCallback17ProcessChildShapeEP16btCollisionShapei
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 56
	subq	$424, %rsp              # imm = 0x1A8
.Lcfi72:
	.cfi_def_cfa_offset 480
.Lcfi73:
	.cfi_offset %rbx, -56
.Lcfi74:
	.cfi_offset %r12, -48
.Lcfi75:
	.cfi_offset %r13, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movq	200(%rax), %rcx
	movss	8(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, 256(%rsp)        # 16-byte Spill
	movss	12(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, 240(%rsp)        # 16-byte Spill
	movss	16(%rax), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, 224(%rsp)       # 16-byte Spill
	movl	20(%rax), %edx
	movl	%edx, 28(%rsp)          # 4-byte Spill
	movss	24(%rax), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movss	28(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 192(%rsp)        # 16-byte Spill
	movss	32(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 208(%rsp)        # 16-byte Spill
	movl	36(%rax), %r13d
	movss	40(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movss	44(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movss	48(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movl	52(%rax), %edx
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movsd	56(%rax), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 272(%rsp)        # 16-byte Spill
	movss	64(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 20(%rsp)         # 4-byte Spill
	movl	68(%rax), %r12d
	movups	72(%rax), %xmm0
	movaps	%xmm0, 352(%rsp)
	movups	88(%rax), %xmm0
	movaps	%xmm0, 368(%rsp)
	movups	104(%rax), %xmm0
	movaps	%xmm0, 384(%rsp)
	movups	120(%rax), %xmm0
	movaps	%xmm0, 400(%rsp)
	movq	40(%rcx), %rax
	movslq	%r14d, %r15
	imulq	$88, %r15, %rcx
	movss	(%rax,%rcx), %xmm3      # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm0
	mulss	%xmm3, %xmm0
	movss	16(%rax,%rcx), %xmm7    # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm1
	mulss	%xmm7, %xmm1
	addss	%xmm0, %xmm1
	movss	32(%rax,%rcx), %xmm12   # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 176(%rsp)        # 16-byte Spill
	movss	4(%rax,%rcx), %xmm11    # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	mulss	%xmm11, %xmm1
	movss	20(%rax,%rcx), %xmm6    # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm2
	mulss	%xmm6, %xmm2
	addss	%xmm1, %xmm2
	movss	36(%rax,%rcx), %xmm13   # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm0
	mulss	%xmm13, %xmm0
	addss	%xmm2, %xmm0
	movaps	%xmm0, 336(%rsp)        # 16-byte Spill
	movss	8(%rax,%rcx), %xmm9     # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	mulss	%xmm9, %xmm1
	movss	24(%rax,%rcx), %xmm5    # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm4
	mulss	%xmm5, %xmm4
	addss	%xmm1, %xmm4
	movss	40(%rax,%rcx), %xmm15   # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm10
	addss	%xmm4, %xmm10
	movaps	%xmm10, 320(%rsp)       # 16-byte Spill
	movaps	%xmm14, %xmm0
	movaps	%xmm0, %xmm1
	mulss	%xmm3, %xmm1
	movaps	192(%rsp), %xmm8        # 16-byte Reload
	movaps	%xmm8, %xmm4
	mulss	%xmm7, %xmm4
	addss	%xmm1, %xmm4
	movaps	208(%rsp), %xmm14       # 16-byte Reload
	movaps	%xmm14, %xmm1
	mulss	%xmm12, %xmm1
	addss	%xmm4, %xmm1
	movaps	%xmm1, 304(%rsp)        # 16-byte Spill
	movaps	%xmm0, %xmm1
	mulss	%xmm11, %xmm1
	movaps	%xmm8, %xmm4
	mulss	%xmm6, %xmm4
	addss	%xmm1, %xmm4
	movaps	%xmm14, %xmm10
	mulss	%xmm13, %xmm10
	addss	%xmm4, %xmm10
	movaps	%xmm0, %xmm1
	movaps	%xmm0, %xmm2
	mulss	%xmm9, %xmm1
	movaps	%xmm8, %xmm4
	mulss	%xmm5, %xmm4
	addss	%xmm1, %xmm4
	movaps	%xmm14, %xmm8
	mulss	%xmm15, %xmm8
	addss	%xmm4, %xmm8
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	addss	%xmm3, %xmm7
	movss	8(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm12
	addss	%xmm7, %xmm12
	mulss	%xmm0, %xmm11
	mulss	%xmm1, %xmm6
	addss	%xmm11, %xmm6
	mulss	%xmm3, %xmm13
	addss	%xmm6, %xmm13
	mulss	%xmm0, %xmm9
	movaps	%xmm0, %xmm7
	mulss	%xmm1, %xmm5
	addss	%xmm9, %xmm5
	mulss	%xmm3, %xmm15
	movaps	%xmm3, %xmm6
	addss	%xmm5, %xmm15
	movaps	256(%rsp), %xmm0        # 16-byte Reload
	movaps	%xmm2, 288(%rsp)        # 16-byte Spill
	unpcklps	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	movss	48(%rax,%rcx), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm3
	mulss	%xmm2, %xmm3
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm0, %xmm2
	movaps	240(%rsp), %xmm0        # 16-byte Reload
	unpcklps	192(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	movss	52(%rax,%rcx), %xmm4    # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm0, %xmm4
	addps	%xmm2, %xmm4
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	unpcklps	%xmm14, %xmm0   # xmm0 = xmm0[0],xmm14[0],xmm0[1],xmm14[1]
	movss	56(%rax,%rcx), %xmm2    # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm6
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm0, %xmm2
	addps	%xmm4, %xmm2
	addss	%xmm3, %xmm1
	addss	%xmm6, %xmm1
	addss	20(%rsp), %xmm1         # 4-byte Folded Reload
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movaps	176(%rsp), %xmm3        # 16-byte Reload
	movss	%xmm3, 48(%rsp)
	movaps	336(%rsp), %xmm3        # 16-byte Reload
	movss	%xmm3, 52(%rsp)
	movaps	320(%rsp), %xmm1        # 16-byte Reload
	movss	%xmm1, 56(%rsp)
	movl	$0, 60(%rsp)
	movaps	304(%rsp), %xmm1        # 16-byte Reload
	movss	%xmm1, 64(%rsp)
	movss	%xmm10, 68(%rsp)
	movss	%xmm8, 72(%rsp)
	movl	$0, 76(%rsp)
	movss	%xmm12, 80(%rsp)
	movss	%xmm13, 84(%rsp)
	movss	%xmm15, 88(%rsp)
	addps	272(%rsp), %xmm2        # 16-byte Folded Reload
	movl	$0, 92(%rsp)
	movlps	%xmm2, 96(%rsp)
	movlps	%xmm0, 104(%rsp)
	movq	(%rbp), %rax
	leaq	48(%rsp), %rsi
	leaq	160(%rsp), %rdx
	leaq	144(%rsp), %rcx
	movq	%rbp, %rdi
	callq	*16(%rax)
	movq	16(%rbx), %rsi
	movq	200(%rsi), %rdi
	movq	(%rdi), %rax
	addq	$8, %rsi
	leaq	128(%rsp), %rdx
	leaq	112(%rsp), %rcx
	callq	*16(%rax)
	movss	160(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	112(%rsp), %xmm0
	jbe	.LBB8_2
# BB#1:
	xorl	%eax, %eax
	jmp	.LBB8_5
.LBB8_2:
	movss	128(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	144(%rsp), %xmm0
	jbe	.LBB8_4
# BB#3:
	xorl	%eax, %eax
	jmp	.LBB8_5
.LBB8_4:
	movb	$1, %al
.LBB8_5:
	movss	168(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	120(%rsp), %xmm0
	ja	.LBB8_7
# BB#6:
	movss	136(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	152(%rsp), %xmm0
	jbe	.LBB8_8
.LBB8_7:
	xorl	%eax, %eax
.LBB8_8:
	movss	164(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	116(%rsp), %xmm0
	ja	.LBB8_20
# BB#9:
	movss	132(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	148(%rsp), %xmm0
	ja	.LBB8_20
# BB#10:
	xorb	$1, %al
	jne	.LBB8_20
# BB#11:
	movl	%r12d, 176(%rsp)        # 4-byte Spill
	leaq	96(%rsp), %rax
	movq	8(%rbx), %rcx
	movups	48(%rsp), %xmm0
	movups	%xmm0, 8(%rcx)
	movups	64(%rsp), %xmm0
	movups	%xmm0, 24(%rcx)
	movups	80(%rsp), %xmm0
	movups	%xmm0, 40(%rcx)
	movups	(%rax), %xmm0
	movups	%xmm0, 56(%rcx)
	movq	8(%rbx), %rcx
	movups	48(%rsp), %xmm0
	movups	%xmm0, 72(%rcx)
	movups	64(%rsp), %xmm0
	movups	%xmm0, 88(%rcx)
	movups	80(%rsp), %xmm0
	movups	%xmm0, 104(%rcx)
	movups	(%rax), %xmm0
	movups	%xmm0, 120(%rcx)
	movq	8(%rbx), %rsi
	movq	200(%rsi), %r12
	movq	%rbp, 200(%rsi)
	movq	48(%rbx), %rax
	cmpq	$0, (%rax,%r15,8)
	jne	.LBB8_13
# BB#12:
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	movq	56(%rbx), %rcx
	callq	*16(%rax)
	movq	48(%rbx), %rcx
	movq	%rax, (%rcx,%r15,8)
	movq	8(%rbx), %rsi
.LBB8_13:
	movq	40(%rbx), %rdi
	movq	(%rdi), %rax
	cmpq	%rsi, 144(%rdi)
	je	.LBB8_14
# BB#15:
	movl	$-1, %esi
	movl	%r14d, %edx
	callq	*24(%rax)
	jmp	.LBB8_16
.LBB8_14:
	movl	$-1, %esi
	movl	%r14d, %edx
	callq	*16(%rax)
.LBB8_16:
	leaq	368(%rsp), %rbp
	movq	48(%rbx), %rax
	movq	(%rax,%r15,8), %rdi
	movq	(%rdi), %rax
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	movq	32(%rbx), %rcx
	movq	40(%rbx), %r8
	callq	*16(%rax)
	movq	32(%rbx), %rax
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB8_19
# BB#17:
	movq	(%rdi), %rax
	callq	*96(%rax)
	testb	$2, %al
	je	.LBB8_19
# BB#18:
	movq	32(%rbx), %rax
	movq	24(%rax), %rdi
	movabsq	$4575657222473777152, %r14 # imm = 0x3F8000003F800000
	movq	%r14, 32(%rsp)
	movq	$1065353216, 40(%rsp)   # imm = 0x3F800000
	leaq	160(%rsp), %rsi
	leaq	144(%rsp), %rdx
	leaq	32(%rsp), %rcx
	callq	_ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_
	movq	32(%rbx), %rax
	movq	24(%rax), %rdi
	movq	%r14, 32(%rsp)
	movq	$1065353216, 40(%rsp)   # imm = 0x3F800000
	leaq	128(%rsp), %rsi
	leaq	112(%rsp), %rdx
	leaq	32(%rsp), %rcx
	callq	_ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_
.LBB8_19:
	movq	8(%rbx), %rax
	movq	%r12, 200(%rax)
	movaps	256(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 8(%rax)
	movaps	240(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 12(%rax)
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 16(%rax)
	movl	28(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 20(%rax)
	movaps	288(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 24(%rax)
	movaps	192(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 28(%rax)
	movaps	208(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 32(%rax)
	movl	%r13d, 36(%rax)
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 40(%rax)
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 44(%rax)
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 48(%rax)
	movl	24(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 52(%rax)
	movaps	272(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 56(%rax)
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	%xmm0, 60(%rax)
	movss	20(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 64(%rax)
	movl	176(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, 68(%rax)
	movq	8(%rbx), %rax
	movaps	352(%rsp), %xmm0
	movups	%xmm0, 72(%rax)
	movups	(%rbp), %xmm0
	movups	%xmm0, 88(%rax)
	movups	16(%rbp), %xmm0
	movups	%xmm0, 104(%rax)
	movups	32(%rbp), %xmm0
	movups	%xmm0, 120(%rax)
.LBB8_20:                               # %_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_.exit.thread
	addq	$424, %rsp              # imm = 0x1A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN22btCompoundLeafCallback17ProcessChildShapeEP16btCollisionShapei, .Lfunc_end8-_ZN22btCompoundLeafCallback17ProcessChildShapeEP16btCollisionShapei
	.cfi_endproc

	.section	.text._ZN6btDbvt8ICollideD2Ev,"axG",@progbits,_ZN6btDbvt8ICollideD2Ev,comdat
	.weak	_ZN6btDbvt8ICollideD2Ev
	.p2align	4, 0x90
	.type	_ZN6btDbvt8ICollideD2Ev,@function
_ZN6btDbvt8ICollideD2Ev:                # @_ZN6btDbvt8ICollideD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end9:
	.size	_ZN6btDbvt8ICollideD2Ev, .Lfunc_end9-_ZN6btDbvt8ICollideD2Ev
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI10_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN28btCompoundCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN28btCompoundCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN28btCompoundCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN28btCompoundCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi82:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi83:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi85:
	.cfi_def_cfa_offset 368
.Lcfi86:
	.cfi_offset %rbx, -56
.Lcfi87:
	.cfi_offset %r12, -48
.Lcfi88:
	.cfi_offset %r13, -40
.Lcfi89:
	.cfi_offset %r14, -32
.Lcfi90:
	.cfi_offset %r15, -24
.Lcfi91:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rcx, %r13
	movq	%rdi, %r14
	cmpb	$0, 48(%r14)
	movq	%rsi, %rbx
	cmovneq	%rdx, %rbx
	cmovneq	%rsi, %rdx
	movl	20(%r14), %eax
	testl	%eax, %eax
	jle	.LBB10_1
# BB#2:                                 # %.lr.ph
	movq	200(%rbx), %rsi
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	16(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movl	20(%rbx), %esi
	movl	%esi, 40(%rsp)          # 4-byte Spill
	movss	24(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	28(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	32(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movl	36(%rbx), %esi
	movl	%esi, 36(%rsp)          # 4-byte Spill
	movss	40(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, 24(%rsp)         # 4-byte Spill
	movss	44(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, 20(%rsp)         # 4-byte Spill
	movss	48(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, 16(%rsp)         # 4-byte Spill
	movl	52(%rbx), %esi
	movl	%esi, 32(%rsp)          # 4-byte Spill
	movq	56(%rbx), %xmm6         # xmm6 = mem[0],zero
	movdqa	%xmm6, 96(%rsp)         # 16-byte Spill
	pshufd	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movdqa	%xmm6, 256(%rsp)        # 16-byte Spill
	movss	64(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, 12(%rsp)         # 4-byte Spill
	movl	68(%rbx), %esi
	movl	%esi, 28(%rsp)          # 4-byte Spill
	movaps	%xmm0, 192(%rsp)        # 16-byte Spill
	movaps	%xmm3, 144(%rsp)        # 16-byte Spill
	unpcklps	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1]
	movaps	%xmm0, 240(%rsp)        # 16-byte Spill
	movaps	%xmm1, 176(%rsp)        # 16-byte Spill
	movaps	%xmm4, 128(%rsp)        # 16-byte Spill
	unpcklps	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	movaps	%xmm1, 224(%rsp)        # 16-byte Spill
	movaps	%xmm2, 160(%rsp)        # 16-byte Spill
	movaps	%xmm5, 112(%rsp)        # 16-byte Spill
	unpcklps	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	movaps	%xmm2, 208(%rsp)        # 16-byte Spill
	movss	.LCPI10_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	xorl	%esi, %esi
	movl	$8, %r15d
	movq	%rax, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB10_3:                               # =>This Inner Loop Header: Depth=1
	movss	%xmm1, 44(%rsp)         # 4-byte Spill
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	40(%rax), %rax
	movss	-64(%rax,%r15,8), %xmm5 # xmm5 = mem[0],zero,zero,zero
	movaps	192(%rsp), %xmm2        # 16-byte Reload
	movaps	%xmm2, %xmm0
	movaps	%xmm2, %xmm4
	mulss	%xmm5, %xmm0
	movss	-48(%rax,%r15,8), %xmm3 # xmm3 = mem[0],zero,zero,zero
	movaps	176(%rsp), %xmm2        # 16-byte Reload
	movaps	%xmm2, %xmm1
	movaps	%xmm2, %xmm10
	mulss	%xmm3, %xmm1
	addss	%xmm0, %xmm1
	movss	-32(%rax,%r15,8), %xmm11 # xmm11 = mem[0],zero,zero,zero
	movaps	160(%rsp), %xmm0        # 16-byte Reload
	movaps	%xmm0, %xmm2
	movaps	%xmm0, %xmm7
	mulss	%xmm11, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm2, 48(%rsp)         # 16-byte Spill
	movss	-60(%rax,%r15,8), %xmm6 # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	mulss	%xmm6, %xmm1
	movss	-44(%rax,%r15,8), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm2
	mulss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	movss	-28(%rax,%r15,8), %xmm13 # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm1
	movaps	%xmm7, %xmm12
	mulss	%xmm13, %xmm1
	addss	%xmm2, %xmm1
	movaps	%xmm1, 288(%rsp)        # 16-byte Spill
	movss	-56(%rax,%r15,8), %xmm7 # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	mulss	%xmm7, %xmm1
	movss	-40(%rax,%r15,8), %xmm2 # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm4
	mulss	%xmm2, %xmm4
	addss	%xmm1, %xmm4
	movss	-24(%rax,%r15,8), %xmm15 # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm12
	addss	%xmm4, %xmm12
	movaps	%xmm12, 272(%rsp)       # 16-byte Spill
	movaps	144(%rsp), %xmm14       # 16-byte Reload
	movaps	%xmm14, %xmm1
	mulss	%xmm5, %xmm1
	movaps	128(%rsp), %xmm12       # 16-byte Reload
	movaps	%xmm12, %xmm4
	mulss	%xmm3, %xmm4
	addss	%xmm1, %xmm4
	movaps	112(%rsp), %xmm8        # 16-byte Reload
	movaps	%xmm8, %xmm10
	mulss	%xmm11, %xmm10
	addss	%xmm4, %xmm10
	movaps	%xmm14, %xmm1
	mulss	%xmm6, %xmm1
	movaps	%xmm12, %xmm4
	mulss	%xmm0, %xmm4
	addss	%xmm1, %xmm4
	movaps	%xmm8, %xmm9
	mulss	%xmm13, %xmm9
	addss	%xmm4, %xmm9
	mulss	%xmm7, %xmm14
	mulss	%xmm2, %xmm12
	addss	%xmm14, %xmm12
	movaps	%xmm8, %xmm1
	mulss	%xmm15, %xmm1
	addss	%xmm12, %xmm1
	movss	24(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	movaps	%xmm4, %xmm8
	movss	20(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	addss	%xmm5, %xmm3
	movss	16(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm11
	addss	%xmm3, %xmm11
	mulss	%xmm8, %xmm6
	mulss	%xmm4, %xmm0
	addss	%xmm6, %xmm0
	mulss	%xmm5, %xmm13
	addss	%xmm0, %xmm13
	mulss	%xmm8, %xmm7
	movaps	%xmm4, %xmm6
	mulss	%xmm6, %xmm2
	addss	%xmm7, %xmm2
	mulss	%xmm5, %xmm15
	movaps	%xmm5, %xmm7
	addss	%xmm2, %xmm15
	movss	-16(%rax,%r15,8), %xmm2 # xmm2 = mem[0],zero,zero,zero
	movss	-12(%rax,%r15,8), %xmm3 # xmm3 = mem[0],zero,zero,zero
	movss	-8(%rax,%r15,8), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm4
	mulss	%xmm2, %xmm4
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	240(%rsp), %xmm2        # 16-byte Folded Reload
	movaps	%xmm6, %xmm5
	mulss	%xmm3, %xmm5
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	224(%rsp), %xmm3        # 16-byte Folded Reload
	addps	%xmm2, %xmm3
	movaps	%xmm7, %xmm2
	mulss	%xmm0, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	208(%rsp), %xmm0        # 16-byte Folded Reload
	addps	%xmm3, %xmm0
	addss	%xmm4, %xmm5
	addss	%xmm2, %xmm5
	addss	12(%rsp), %xmm5         # 4-byte Folded Reload
	xorps	%xmm2, %xmm2
	movss	%xmm5, %xmm2            # xmm2 = xmm5[0],xmm2[1,2,3]
	movq	(%rax,%r15,8), %rax
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	movss	%xmm3, 8(%rbx)
	movaps	288(%rsp), %xmm3        # 16-byte Reload
	movss	%xmm3, 12(%rbx)
	movaps	272(%rsp), %xmm3        # 16-byte Reload
	movss	%xmm3, 16(%rbx)
	movl	$0, 20(%rbx)
	movss	%xmm10, 24(%rbx)
	movss	%xmm9, 28(%rbx)
	movss	%xmm1, 32(%rbx)
	movl	$0, 36(%rbx)
	movss	%xmm11, 40(%rbx)
	movss	%xmm13, 44(%rbx)
	movss	%xmm15, 48(%rbx)
	addps	96(%rsp), %xmm0         # 16-byte Folded Reload
	movl	$0, 52(%rbx)
	movlps	%xmm0, 56(%rbx)
	movlps	%xmm2, 64(%rbx)
	movq	200(%rbx), %rbp
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	%rax, 200(%rbx)
	movq	32(%r14), %rax
	movq	(%rax,%rsi,8), %rdi
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	movq	%rdx, %rbp
	movq	%r13, %rcx
	movq	%r12, %r8
	callq	*24(%rax)
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rbp, %rdx
	minss	44(%rsp), %xmm0         # 4-byte Folded Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, 200(%rbx)
	movaps	192(%rsp), %xmm1        # 16-byte Reload
	movss	%xmm1, 8(%rbx)
	movaps	176(%rsp), %xmm1        # 16-byte Reload
	movss	%xmm1, 12(%rbx)
	movaps	160(%rsp), %xmm1        # 16-byte Reload
	movss	%xmm1, 16(%rbx)
	movl	40(%rsp), %esi          # 4-byte Reload
	movl	%esi, 20(%rbx)
	movaps	144(%rsp), %xmm1        # 16-byte Reload
	movss	%xmm1, 24(%rbx)
	movaps	128(%rsp), %xmm1        # 16-byte Reload
	movss	%xmm1, 28(%rbx)
	movaps	112(%rsp), %xmm1        # 16-byte Reload
	movss	%xmm1, 32(%rbx)
	movl	36(%rsp), %esi          # 4-byte Reload
	movl	%esi, 36(%rbx)
	movss	24(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 40(%rbx)
	movss	20(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 44(%rbx)
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 48(%rbx)
	movl	32(%rsp), %esi          # 4-byte Reload
	movl	%esi, 52(%rbx)
	movaps	96(%rsp), %xmm1         # 16-byte Reload
	movss	%xmm1, 56(%rbx)
	movaps	256(%rsp), %xmm1        # 16-byte Reload
	movss	%xmm1, 60(%rbx)
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 64(%rbx)
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	%esi, 68(%rbx)
	movq	88(%rsp), %rsi          # 8-byte Reload
	incq	%rsi
	addq	$11, %r15
	cmpq	%rsi, %rax
	movaps	%xmm0, %xmm1
	jne	.LBB10_3
	jmp	.LBB10_4
.LBB10_1:
	movss	.LCPI10_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
.LBB10_4:                               # %._crit_edge
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZN28btCompoundCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end10-_ZN28btCompoundCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.text._ZN28btCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,"axG",@progbits,_ZN28btCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,comdat
	.weak	_ZN28btCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.p2align	4, 0x90
	.type	_ZN28btCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,@function
_ZN28btCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE: # @_ZN28btCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 32
.Lcfi95:
	.cfi_offset %rbx, -32
.Lcfi96:
	.cfi_offset %r14, -24
.Lcfi97:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	20(%r15), %eax
	testl	%eax, %eax
	jle	.LBB11_5
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_2:                               # =>This Inner Loop Header: Depth=1
	movq	32(%r15), %rcx
	movq	(%rcx,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB11_4
# BB#3:                                 #   in Loop: Header=BB11_2 Depth=1
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*32(%rax)
	movl	20(%r15), %eax
.LBB11_4:                               #   in Loop: Header=BB11_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB11_2
.LBB11_5:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	_ZN28btCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE, .Lfunc_end11-_ZN28btCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_endproc

	.section	.text._ZN22btCompoundLeafCallbackD0Ev,"axG",@progbits,_ZN22btCompoundLeafCallbackD0Ev,comdat
	.weak	_ZN22btCompoundLeafCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN22btCompoundLeafCallbackD0Ev,@function
_ZN22btCompoundLeafCallbackD0Ev:        # @_ZN22btCompoundLeafCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end12:
	.size	_ZN22btCompoundLeafCallbackD0Ev, .Lfunc_end12-_ZN22btCompoundLeafCallbackD0Ev
	.cfi_endproc

	.section	.text._ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_,"axG",@progbits,_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_,comdat
	.weak	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_
	.p2align	4, 0x90
	.type	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_,@function
_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_: # @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end13:
	.size	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_, .Lfunc_end13-_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI14_0:
	.long	1056964608              # float 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI14_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.text._ZN22btCompoundLeafCallback7ProcessEPK10btDbvtNode,"axG",@progbits,_ZN22btCompoundLeafCallback7ProcessEPK10btDbvtNode,comdat
	.weak	_ZN22btCompoundLeafCallback7ProcessEPK10btDbvtNode
	.p2align	4, 0x90
	.type	_ZN22btCompoundLeafCallback7ProcessEPK10btDbvtNode,@function
_ZN22btCompoundLeafCallback7ProcessEPK10btDbvtNode: # @_ZN22btCompoundLeafCallback7ProcessEPK10btDbvtNode
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi100:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 40
	subq	$56, %rsp
.Lcfi102:
	.cfi_def_cfa_offset 96
.Lcfi103:
	.cfi_offset %rbx, -40
.Lcfi104:
	.cfi_offset %r12, -32
.Lcfi105:
	.cfi_offset %r14, -24
.Lcfi106:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movslq	40(%rbx), %r14
	movq	8(%r12), %rax
	movq	32(%r12), %rcx
	movq	200(%rax), %rax
	movq	40(%rax), %rax
	imulq	$88, %r14, %rdx
	movq	64(%rax,%rdx), %r15
	movq	24(%rcx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_3
# BB#1:
	movq	(%rdi), %rax
	callq	*96(%rax)
	testb	$2, %al
	je	.LBB14_3
# BB#2:
	movq	8(%r12), %rax
	movss	40(%rax), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	44(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	48(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	16(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm13          # xmm13 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm10
	subss	%xmm13, %xmm10
	movss	20(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm14
	subss	%xmm12, %xmm14
	movss	24(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm11
	subss	%xmm6, %xmm11
	movss	.LCPI14_0(%rip), %xmm5  # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm10
	mulss	%xmm5, %xmm14
	mulss	%xmm5, %xmm11
	xorps	%xmm0, %xmm0
	addss	%xmm0, %xmm10
	addss	%xmm0, %xmm14
	addss	%xmm0, %xmm11
	addss	%xmm13, %xmm7
	addss	%xmm12, %xmm3
	addss	%xmm6, %xmm1
	mulss	%xmm5, %xmm7
	mulss	%xmm5, %xmm3
	mulss	%xmm5, %xmm1
	movss	24(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm13        # xmm13 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm12   # xmm12 = xmm12[0],xmm0[0],xmm12[1],xmm0[1]
	movaps	.LCPI14_1(%rip), %xmm5  # xmm5 = [nan,nan,nan,nan]
	movaps	%xmm9, %xmm6
	mulss	%xmm7, %xmm9
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm12, %xmm7
	andps	%xmm5, %xmm12
	movss	28(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm13   # xmm13 = xmm13[0],xmm0[0],xmm13[1],xmm0[1]
	movss	32(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movaps	%xmm4, %xmm0
	mulss	%xmm3, %xmm4
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm13, %xmm3
	andps	%xmm5, %xmm13
	addps	%xmm7, %xmm3
	movaps	%xmm2, %xmm7
	andps	%xmm5, %xmm7
	andps	%xmm5, %xmm6
	andps	%xmm5, %xmm0
	andps	%xmm8, %xmm5
	mulss	%xmm1, %xmm8
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	addps	%xmm3, %xmm1
	movsd	56(%rax), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm2, %xmm1
	addss	%xmm9, %xmm4
	addss	%xmm8, %xmm4
	mulss	%xmm10, %xmm6
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	mulps	%xmm12, %xmm10
	mulss	%xmm14, %xmm0
	shufps	$224, %xmm14, %xmm14    # xmm14 = xmm14[0,0,2,3]
	mulps	%xmm13, %xmm14
	addps	%xmm10, %xmm14
	mulss	%xmm11, %xmm5
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	mulps	%xmm7, %xmm11
	addps	%xmm14, %xmm11
	addss	64(%rax), %xmm4
	addss	%xmm6, %xmm0
	addss	%xmm0, %xmm5
	movaps	%xmm4, %xmm0
	subss	%xmm5, %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movaps	%xmm1, %xmm0
	subps	%xmm11, %xmm0
	movlps	%xmm0, 40(%rsp)
	movlps	%xmm2, 48(%rsp)
	movaps	%xmm11, %xmm0
	addss	%xmm1, %xmm0
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	shufps	$229, %xmm11, %xmm11    # xmm11 = xmm11[1,1,2,3]
	addss	%xmm1, %xmm11
	addss	%xmm4, %xmm5
	unpcklps	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1]
	xorps	%xmm1, %xmm1
	movss	%xmm5, %xmm1            # xmm1 = xmm5[0],xmm1[1,2,3]
	movlps	%xmm0, 24(%rsp)
	movlps	%xmm1, 32(%rsp)
	movq	32(%r12), %rax
	movq	24(%rax), %rdi
	movq	$1065353216, 8(%rsp)    # imm = 0x3F800000
	movq	$0, 16(%rsp)
	leaq	40(%rsp), %rsi
	leaq	24(%rsp), %rdx
	leaq	8(%rsp), %rcx
	callq	_ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_
.LBB14_3:
	movq	%r12, %rdi
	movq	%r15, %rsi
	movl	%r14d, %edx
	callq	_ZN22btCompoundLeafCallback17ProcessChildShapeEP16btCollisionShapei
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	_ZN22btCompoundLeafCallback7ProcessEPK10btDbvtNode, .Lfunc_end14-_ZN22btCompoundLeafCallback7ProcessEPK10btDbvtNode
	.cfi_endproc

	.section	.text._ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef,"axG",@progbits,_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef,comdat
	.weak	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef
	.p2align	4, 0x90
	.type	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef,@function
_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef: # @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end15:
	.size	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef, .Lfunc_end15-_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef
	.cfi_endproc

	.section	.text._ZN6btDbvt8ICollide7DescentEPK10btDbvtNode,"axG",@progbits,_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode,comdat
	.weak	_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode
	.p2align	4, 0x90
	.type	_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode,@function
_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode: # @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode
	.cfi_startproc
# BB#0:
	movb	$1, %al
	retq
.Lfunc_end16:
	.size	_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode, .Lfunc_end16-_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode
	.cfi_endproc

	.section	.text._ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode,"axG",@progbits,_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode,comdat
	.weak	_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode
	.p2align	4, 0x90
	.type	_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode,@function
_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode: # @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode
	.cfi_startproc
# BB#0:
	movb	$1, %al
	retq
.Lfunc_end17:
	.size	_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode, .Lfunc_end17-_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI18_0:
	.long	1056964608              # float 0.5
.LCPI18_1:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_2:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_,"axG",@progbits,_ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_,comdat
	.weak	_ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_
	.p2align	4, 0x90
	.type	_ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_,@function
_ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_: # @_ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi107:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi108:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi109:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi110:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi111:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi112:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi113:
	.cfi_def_cfa_offset 160
.Lcfi114:
	.cfi_offset %rbx, -56
.Lcfi115:
	.cfi_offset %r12, -48
.Lcfi116:
	.cfi_offset %r13, -40
.Lcfi117:
	.cfi_offset %r14, -32
.Lcfi118:
	.cfi_offset %r15, -24
.Lcfi119:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdi, %r13
	movss	(%rdx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm6
	subss	%xmm9, %xmm6
	movaps	%xmm5, %xmm7
	subss	%xmm8, %xmm7
	movss	8(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	subss	%xmm2, %xmm0
	movss	.LCPI18_0(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	movss	%xmm6, 88(%rsp)         # 4-byte Spill
	mulss	%xmm3, %xmm7
	movss	%xmm7, 84(%rsp)         # 4-byte Spill
	mulss	%xmm3, %xmm0
	movss	%xmm0, 80(%rsp)         # 4-byte Spill
	addss	%xmm9, %xmm4
	addss	%xmm8, %xmm5
	addss	%xmm2, %xmm1
	mulss	%xmm3, %xmm4
	movss	%xmm4, 92(%rsp)         # 4-byte Spill
	mulss	%xmm3, %xmm5
	movss	%xmm5, 52(%rsp)         # 4-byte Spill
	mulss	%xmm3, %xmm1
	movss	%xmm1, 48(%rsp)         # 4-byte Spill
	movabsq	$4575657222473777152, %rax # imm = 0x3F8000003F800000
	movq	%rax, 64(%rsp)
	movq	$1065353216, 72(%rsp)   # imm = 0x3F800000
	movss	.LCPI18_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	xorl	%ebx, %ebx
	leaq	32(%rsp), %r15
	leaq	16(%rsp), %r12
	movl	$3212836864, %ebp       # imm = 0xBF800000
	movaps	%xmm1, %xmm2
	movaps	%xmm1, %xmm0
	jmp	.LBB18_1
	.p2align	4, 0x90
.LBB18_4:                               # %..preheader_crit_edge
                                        #   in Loop: Header=BB18_1 Depth=1
	incq	%rbx
	movss	64(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	68(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	72(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
.LBB18_1:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	mulss	88(%rsp), %xmm0         # 4-byte Folded Reload
	mulss	84(%rsp), %xmm2         # 4-byte Folded Reload
	mulss	80(%rsp), %xmm1         # 4-byte Folded Reload
	movl	$0, 44(%rsp)
	movaps	%xmm2, %xmm4
	movss	%xmm4, 12(%rsp)         # 4-byte Spill
	movss	92(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	movss	%xmm3, 100(%rsp)        # 4-byte Spill
	movaps	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, 32(%rsp)
	movss	52(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, 96(%rsp)         # 4-byte Spill
	movss	%xmm5, 36(%rsp)
	movss	48(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm1
	movss	%xmm1, 56(%rsp)         # 4-byte Spill
	movss	%xmm1, 40(%rsp)
	movl	$0, 28(%rsp)
	subss	%xmm0, %xmm2
	movss	%xmm2, 60(%rsp)         # 4-byte Spill
	movss	%xmm2, 16(%rsp)
	movss	%xmm5, 20(%rsp)
	movss	%xmm1, 24(%rsp)
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movl	$0, 44(%rsp)
	movss	60(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 32(%rsp)
	movss	96(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 36(%rsp)
	movss	56(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 40(%rsp)
	movl	$0, 28(%rsp)
	movss	%xmm0, 16(%rsp)
	movss	52(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	subss	12(%rsp), %xmm0         # 4-byte Folded Reload
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movss	%xmm0, 20(%rsp)
	movss	%xmm1, 24(%rsp)
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movl	$0, 44(%rsp)
	movss	60(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 32(%rsp)
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 36(%rsp)
	movss	56(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 40(%rsp)
	movl	$0, 28(%rsp)
	movss	%xmm0, 16(%rsp)
	movss	%xmm1, 20(%rsp)
	movss	48(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	subss	100(%rsp), %xmm0        # 4-byte Folded Reload
	movss	%xmm0, 24(%rsp)
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movabsq	$-4647714812233515008, %rax # imm = 0xBF800000BF800000
	movq	%rax, 64(%rsp)
	movq	%rbp, 72(%rsp)
	cmpq	$2, %rbx
	jg	.LBB18_3
# BB#2:                                 #   in Loop: Header=BB18_1 Depth=1
	movss	64(%rsp,%rbx,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	xorps	.LCPI18_2(%rip), %xmm0
	movss	%xmm0, 64(%rsp,%rbx,4)
.LBB18_3:                               #   in Loop: Header=BB18_1 Depth=1
	cmpq	$3, %rbx
	jne	.LBB18_4
# BB#5:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	_ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_, .Lfunc_end18-_ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_
	.cfi_endproc

	.type	_ZTV28btCompoundCollisionAlgorithm,@object # @_ZTV28btCompoundCollisionAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTV28btCompoundCollisionAlgorithm
	.p2align	3
_ZTV28btCompoundCollisionAlgorithm:
	.quad	0
	.quad	_ZTI28btCompoundCollisionAlgorithm
	.quad	_ZN28btCompoundCollisionAlgorithmD2Ev
	.quad	_ZN28btCompoundCollisionAlgorithmD0Ev
	.quad	_ZN28btCompoundCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN28btCompoundCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN28btCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.size	_ZTV28btCompoundCollisionAlgorithm, 56

	.type	_ZTS28btCompoundCollisionAlgorithm,@object # @_ZTS28btCompoundCollisionAlgorithm
	.globl	_ZTS28btCompoundCollisionAlgorithm
	.p2align	4
_ZTS28btCompoundCollisionAlgorithm:
	.asciz	"28btCompoundCollisionAlgorithm"
	.size	_ZTS28btCompoundCollisionAlgorithm, 31

	.type	_ZTI28btCompoundCollisionAlgorithm,@object # @_ZTI28btCompoundCollisionAlgorithm
	.globl	_ZTI28btCompoundCollisionAlgorithm
	.p2align	4
_ZTI28btCompoundCollisionAlgorithm:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS28btCompoundCollisionAlgorithm
	.quad	_ZTI30btActivatingCollisionAlgorithm
	.size	_ZTI28btCompoundCollisionAlgorithm, 24

	.type	_ZTV22btCompoundLeafCallback,@object # @_ZTV22btCompoundLeafCallback
	.section	.rodata._ZTV22btCompoundLeafCallback,"aG",@progbits,_ZTV22btCompoundLeafCallback,comdat
	.weak	_ZTV22btCompoundLeafCallback
	.p2align	3
_ZTV22btCompoundLeafCallback:
	.quad	0
	.quad	_ZTI22btCompoundLeafCallback
	.quad	_ZN6btDbvt8ICollideD2Ev
	.quad	_ZN22btCompoundLeafCallbackD0Ev
	.quad	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_
	.quad	_ZN22btCompoundLeafCallback7ProcessEPK10btDbvtNode
	.quad	_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef
	.quad	_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode
	.quad	_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode
	.size	_ZTV22btCompoundLeafCallback, 72

	.type	_ZTS22btCompoundLeafCallback,@object # @_ZTS22btCompoundLeafCallback
	.section	.rodata._ZTS22btCompoundLeafCallback,"aG",@progbits,_ZTS22btCompoundLeafCallback,comdat
	.weak	_ZTS22btCompoundLeafCallback
	.p2align	4
_ZTS22btCompoundLeafCallback:
	.asciz	"22btCompoundLeafCallback"
	.size	_ZTS22btCompoundLeafCallback, 25

	.type	_ZTSN6btDbvt8ICollideE,@object # @_ZTSN6btDbvt8ICollideE
	.section	.rodata._ZTSN6btDbvt8ICollideE,"aG",@progbits,_ZTSN6btDbvt8ICollideE,comdat
	.weak	_ZTSN6btDbvt8ICollideE
	.p2align	4
_ZTSN6btDbvt8ICollideE:
	.asciz	"N6btDbvt8ICollideE"
	.size	_ZTSN6btDbvt8ICollideE, 19

	.type	_ZTIN6btDbvt8ICollideE,@object # @_ZTIN6btDbvt8ICollideE
	.section	.rodata._ZTIN6btDbvt8ICollideE,"aG",@progbits,_ZTIN6btDbvt8ICollideE,comdat
	.weak	_ZTIN6btDbvt8ICollideE
	.p2align	3
_ZTIN6btDbvt8ICollideE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6btDbvt8ICollideE
	.size	_ZTIN6btDbvt8ICollideE, 16

	.type	_ZTI22btCompoundLeafCallback,@object # @_ZTI22btCompoundLeafCallback
	.section	.rodata._ZTI22btCompoundLeafCallback,"aG",@progbits,_ZTI22btCompoundLeafCallback,comdat
	.weak	_ZTI22btCompoundLeafCallback
	.p2align	4
_ZTI22btCompoundLeafCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22btCompoundLeafCallback
	.quad	_ZTIN6btDbvt8ICollideE
	.size	_ZTI22btCompoundLeafCallback, 24


	.globl	_ZN28btCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
	.type	_ZN28btCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b,@function
_ZN28btCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b = _ZN28btCompoundCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
	.globl	_ZN28btCompoundCollisionAlgorithmD1Ev
	.type	_ZN28btCompoundCollisionAlgorithmD1Ev,@function
_ZN28btCompoundCollisionAlgorithmD1Ev = _ZN28btCompoundCollisionAlgorithmD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
