	.text
	.file	"z46.bc"
	.globl	FindOptimize
	.p2align	4, 0x90
	.type	FindOptimize,@function
FindOptimize:                           # @FindOptimize
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$64, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 112
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r13, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	cmpb	$2, 32(%r13)
	je	.LBB0_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_2:
	movq	80(%r13), %rax
	movzwl	41(%rax), %eax
	testb	$64, %al
	jne	.LBB0_4
# BB#3:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_4:                                # %.preheader117
	leaq	32(%r13), %r15
	movq	8(%r13), %rax
	cmpq	%r13, %rax
	jne	.LBB0_6
	jmp	.LBB0_16
	.p2align	4, 0x90
.LBB0_10:                               # %.backedge
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	8(%rax), %rax
	cmpq	%r13, %rax
	je	.LBB0_16
.LBB0_6:                                # %.preheader115
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_7 Depth 2
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB0_7:                                #   Parent Loop BB0_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %ecx
	testb	%cl, %cl
	je	.LBB0_7
# BB#8:                                 #   in Loop: Header=BB0_6 Depth=1
	cmpb	$10, %cl
	jne	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_6 Depth=1
	movq	80(%rbx), %rcx
	testb	$1, 126(%rcx)
	je	.LBB0_10
# BB#11:
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB0_13
# BB#12:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rbx), %rax
.LBB0_13:
	addq	$16, %rax
	.p2align	4, 0x90
.LBB0_14:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB0_14
# BB#15:
	movq	%r15, %rsi
	callq	CopyObject
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.LBB0_24
.LBB0_16:                               # %.thread
	movq	80(%r13), %rax
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	jne	.LBB0_19
	jmp	.LBB0_23
	.p2align	4, 0x90
.LBB0_17:                               #   in Loop: Header=BB0_19 Depth=1
	movq	8(%rcx), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_23
.LBB0_19:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_20 Depth 2
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB0_20:                               #   Parent Loop BB0_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdx), %rdx
	cmpb	$0, 32(%rdx)
	je	.LBB0_20
# BB#21:                                #   in Loop: Header=BB0_19 Depth=1
	testb	$1, 126(%rdx)
	je	.LBB0_17
# BB#22:                                # %.loopexit
	movq	56(%rdx), %rdi
	movq	%r15, %rsi
	callq	CopyObject
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.LBB0_24
.LBB0_23:                               # %.loopexit.thread
	movq	no_fpos(%rip), %r8
	xorl	%r12d, %r12d
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_24:                               # %.thread108
	movq	$0, 8(%rsp)
	movq	$0, 16(%rsp)
	movq	$0, 24(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 48(%rsp)
	addq	$64, %r13
	subq	$8, %rsp
.Lcfi11:
	.cfi_adjust_cfa_offset 8
	leaq	24(%rsp), %rax
	leaq	16(%rsp), %rbx
	leaq	56(%rsp), %rcx
	leaq	40(%rsp), %r8
	leaq	32(%rsp), %r9
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	pushq	$0
.Lcfi12:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	Manifest
	addq	$48, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -48
	movl	$1, %esi
	movq	%rax, %rdi
	callq	ReplaceWithTidy
	movb	32(%rax), %cl
	addb	$-11, %cl
	cmpb	$2, %cl
	jae	.LBB0_25
# BB#26:
	movb	64(%rax), %cl
	cmpb	$89, %cl
	jne	.LBB0_31
# BB#27:
	cmpb	$101, 65(%rax)
	jne	.LBB0_35
# BB#28:
	cmpb	$115, 66(%rax)
	jne	.LBB0_35
# BB#29:
	cmpb	$0, 67(%rax)
	jne	.LBB0_35
# BB#30:
	movl	$1, %ebx
	jmp	.LBB0_37
.LBB0_25:
	xorl	%ebx, %ebx
	movl	$46, %edi
	movl	$1, %esi
	movl	$.L.str.5, %edx
	jmp	.LBB0_36
.LBB0_31:                               # %.thread110
	cmpb	$78, %cl
	jne	.LBB0_35
# BB#32:
	cmpb	$111, 65(%rax)
	jne	.LBB0_35
# BB#33:
	cmpb	$0, 66(%rax)
	je	.LBB0_34
.LBB0_35:                               # %.thread113
	xorl	%ebx, %ebx
	movl	$46, %edi
	movl	$2, %esi
	movl	$.L.str.9, %edx
.LBB0_36:
	movl	$2, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	movq	%r15, %r8
	callq	Error
.LBB0_37:
	movl	%ebx, %eax
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB0_34:
	xorl	%ebx, %ebx
	jmp	.LBB0_37
.Lfunc_end0:
	.size	FindOptimize, .Lfunc_end0-FindOptimize
	.cfi_endproc

	.globl	SetOptimize
	.p2align	4, 0x90
	.type	SetOptimize,@function
SetOptimize:                            # @SetOptimize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$1080, %rsp             # imm = 0x438
.Lcfi24:
	.cfi_def_cfa_offset 1136
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	80(%r12), %rdi
	callq	SymName
	leaq	48(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	strcpy
	movq	%rbx, %rdi
	callq	strlen
	movw	$46, 48(%rsp,%rax)
	movl	36(%r12), %edi
	andl	$1048575, %edi          # imm = 0xFFFFF
	callq	StringInt
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	strcat
	movq	OldCrossDb(%rip), %rdi
	movq	OptGallSym(%rip), %rdx
	subq	$8, %rsp
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	leaq	48(%rsp), %rax
	leaq	28(%rsp), %rbp
	leaq	40(%rsp), %r10
	leaq	568(%rsp), %r8
	leaq	26(%rsp), %r9
	movl	$0, %esi
	movq	%rbx, %rcx
	pushq	%rax
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	callq	DbRetrieve
	addq	$32, %rsp
.Lcfi35:
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	je	.LBB1_51
# BB#1:
	xorl	%edi, %edi
	callq	SwitchScope
	movq	32(%rsp), %rsi
	movl	20(%rsp), %edx
	movzwl	18(%rsp), %edi
	callq	ReadFromFile
	movq	%rax, %rbp
	xorl	%edi, %edi
	callq	UnSwitchScope
	testq	%rbp, %rbp
	jne	.LBB1_3
# BB#2:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.11, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_3:
	cmpb	$2, 32(%rbp)
	je	.LBB1_5
# BB#4:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_5:
	movq	80(%rbp), %rax
	cmpq	OptGallSym(%rip), %rax
	je	.LBB1_7
# BB#6:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_7:
	movq	8(%rbp), %rbx
	cmpq	%rbp, %rbx
	jne	.LBB1_9
# BB#8:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.14, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rbp), %rbx
	.p2align	4, 0x90
.LBB1_9:                                # %.preheader189
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB1_9
# BB#10:                                # %.preheader189
	cmpb	$10, %al
	je	.LBB1_12
# BB#11:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_12:                               # %.loopexit164
	movq	8(%rbx), %rbx
	.p2align	4, 0x90
.LBB1_13:                               # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB1_13
# BB#14:
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	cmpb	$17, %al
	je	.LBB1_16
# BB#15:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.16, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_16:                               # %.loopexit
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	ReplaceWithTidy
	movq	%rax, %r13
	andb	$-3, 43(%r12)
	cmpb	$17, 32(%r13)
	je	.LBB1_18
# BB#17:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.17, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_18:                               # %.preheader
	movq	8(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_41
# BB#19:                                # %.lr.ph
	leaq	8(%r13), %rcx
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB1_20:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_22 Depth 2
	leaq	16(%rax), %r15
	jmp	.LBB1_22
	.p2align	4, 0x90
.LBB1_21:                               #   in Loop: Header=BB1_22 Depth=2
	addq	$16, %r15
.LBB1_22:                               #   Parent Loop BB1_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15), %r15
	movzbl	32(%r15), %ebx
	testb	%bl, %bl
	je	.LBB1_21
# BB#23:                                #   in Loop: Header=BB1_20 Depth=1
	movl	%ebx, %edx
	addb	$-11, %dl
	cmpb	$2, %dl
	jae	.LBB1_29
# BB#24:                                #   in Loop: Header=BB1_20 Depth=1
	cmpb	$104, 64(%r15)
	jne	.LBB1_26
# BB#25:                                #   in Loop: Header=BB1_20 Depth=1
	cmpb	$0, 65(%r15)
	je	.LBB1_33
.LBB1_26:                               # %.thread188
                                        #   in Loop: Header=BB1_20 Depth=1
	leaq	64(%r15), %rdi
	movl	$0, 12(%rsp)
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	leaq	12(%rsp), %rdx
	callq	sscanf
	movl	12(%rsp), %eax
	testl	%eax, %eax
	jg	.LBB1_28
# BB#27:                                #   in Loop: Header=BB1_20 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.20, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	12(%rsp), %eax
.LBB1_28:                               #   in Loop: Header=BB1_20 Depth=1
	movl	%eax, 48(%r15)
	jmp	.LBB1_40
	.p2align	4, 0x90
.LBB1_29:                               #   in Loop: Header=BB1_20 Depth=1
	cmpb	$1, %bl
	je	.LBB1_30
# BB#32:                                #   in Loop: Header=BB1_20 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.21, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB1_40
.LBB1_33:                               #   in Loop: Header=BB1_20 Depth=1
	orb	$2, 43(%r12)
	movq	(%rcx), %rax
.LBB1_30:                               #   in Loop: Header=BB1_20 Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_34
# BB#31:                                #   in Loop: Header=BB1_20 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_35
.LBB1_34:                               #   in Loop: Header=BB1_20 Depth=1
	xorl	%ecx, %ecx
.LBB1_35:                               #   in Loop: Header=BB1_20 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_37
# BB#36:                                #   in Loop: Header=BB1_20 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_37:                               #   in Loop: Header=BB1_20 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_39
# BB#38:                                #   in Loop: Header=BB1_20 Depth=1
	callq	DisposeObject
.LBB1_39:                               #   in Loop: Header=BB1_20 Depth=1
	movq	(%rbp), %rbp
.LBB1_40:                               #   in Loop: Header=BB1_20 Depth=1
	movq	8(%rbp), %rbp
	leaq	8(%rbp), %rcx
	movq	8(%rbp), %rax
	cmpq	%r13, %rax
	jne	.LBB1_20
.LBB1_41:                               # %._crit_edge
	movq	24(%r13), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_43
# BB#42:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB1_43:
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_45
# BB#44:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_45:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	DisposeObject
	movq	%r13, 120(%r12)
	testq	%r13, %r13
	je	.LBB1_52
# BB#46:
	movq	8(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_52
# BB#47:
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_48:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	leaq	16(%rcx), %rax
	cmpb	$0, 32(%rcx)
	je	.LBB1_48
# BB#49:
	movl	48(%rcx), %eax
	decl	%eax
	movl	%eax, 160(%r12)
	movq	24(%rcx), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_62
# BB#50:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_63
.LBB1_51:                               # %.thread
	movq	$0, 120(%r12)
.LBB1_52:
	movl	$65535, 160(%r12)       # imm = 0xFFFF
.LBB1_53:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_55
# BB#54:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_56
.LBB1_55:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_56:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, 104(%r12)
	andb	$-5, 43(%r12)
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_58
# BB#57:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_59
.LBB1_58:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_59:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, 112(%r12)
	movzwl	(%r14), %eax
	andl	$128, %eax
	movq	104(%r12), %rcx
	movzwl	64(%rcx), %edx
	andl	$65407, %edx            # imm = 0xFF7F
	orl	%eax, %edx
	movw	%dx, 64(%rcx)
	movzwl	(%r14), %eax
	andl	$256, %eax              # imm = 0x100
	movq	104(%r12), %rcx
	movzwl	64(%rcx), %edx
	andl	$65279, %edx            # imm = 0xFEFF
	orl	%eax, %edx
	movw	%dx, 64(%rcx)
	movzwl	(%r14), %eax
	andl	$512, %eax              # imm = 0x200
	movq	104(%r12), %rcx
	movzwl	64(%rcx), %edx
	andl	$65023, %edx            # imm = 0xFDFF
	orl	%eax, %edx
	movw	%dx, 64(%rcx)
	movzwl	(%r14), %eax
	andl	$7168, %eax             # imm = 0x1C00
	movq	104(%r12), %rcx
	movzwl	64(%rcx), %edx
	andl	$58367, %edx            # imm = 0xE3FF
	orl	%eax, %edx
	movw	%dx, 64(%rcx)
	movzwl	(%r14), %eax
	andl	$57344, %eax            # imm = 0xE000
	movq	104(%r12), %rcx
	movzwl	64(%rcx), %edx
	andl	$8191, %edx             # imm = 0x1FFF
	orl	%eax, %edx
	movw	%dx, 64(%rcx)
	movzwl	2(%r14), %eax
	movq	104(%r12), %rcx
	movw	%ax, 66(%rcx)
	movb	4(%r14), %al
	andb	$3, %al
	movb	68(%rcx), %dl
	andb	$-4, %dl
	orb	%al, %dl
	movb	%dl, 68(%rcx)
	movb	4(%r14), %al
	andb	$12, %al
	movq	104(%r12), %rcx
	movb	68(%rcx), %dl
	andb	$-13, %dl
	orb	%al, %dl
	movb	%dl, 68(%rcx)
	movb	4(%r14), %al
	andb	$112, %al
	movq	104(%r12), %rcx
	movb	68(%rcx), %dl
	andb	$-113, %dl
	orb	%al, %dl
	movb	%dl, 68(%rcx)
	movb	(%r14), %al
	andb	$8, %al
	movq	104(%r12), %rcx
	movb	64(%rcx), %dl
	andb	$-9, %dl
	orb	%al, %dl
	movb	%dl, 64(%rcx)
	movzwl	4(%r14), %eax
	andl	$128, %eax
	movq	104(%r12), %rcx
	movzwl	68(%rcx), %edx
	andl	$65407, %edx            # imm = 0xFF7F
	orl	%eax, %edx
	movw	%dx, 68(%rcx)
	movzwl	4(%r14), %eax
	andl	$256, %eax              # imm = 0x100
	movq	104(%r12), %rcx
	movzwl	68(%rcx), %edx
	andl	$65279, %edx            # imm = 0xFEFF
	orl	%eax, %edx
	movw	%dx, 68(%rcx)
	movzwl	4(%r14), %eax
	andl	$512, %eax              # imm = 0x200
	movq	104(%r12), %rcx
	movzwl	68(%rcx), %edx
	andl	$65023, %edx            # imm = 0xFDFF
	orl	%eax, %edx
	movw	%dx, 68(%rcx)
	movzwl	4(%r14), %eax
	andl	$7168, %eax             # imm = 0x1C00
	movq	104(%r12), %rcx
	movzwl	68(%rcx), %edx
	andl	$58367, %edx            # imm = 0xE3FF
	orl	%eax, %edx
	movw	%dx, 68(%rcx)
	movzwl	4(%r14), %eax
	andl	$57344, %eax            # imm = 0xE000
	movq	104(%r12), %rcx
	movzwl	68(%rcx), %edx
	andl	$8191, %edx             # imm = 0x1FFF
	orl	%eax, %edx
	movw	%dx, 68(%rcx)
	movzwl	6(%r14), %eax
	movq	104(%r12), %rcx
	movw	%ax, 70(%rcx)
	movl	$4095, %eax             # imm = 0xFFF
	andl	12(%r14), %eax
	movl	$-4096, %edx            # imm = 0xF000
	andl	76(%rcx), %edx
	orl	%eax, %edx
	movl	%edx, 76(%rcx)
	movl	$4190208, %eax          # imm = 0x3FF000
	andl	12(%r14), %eax
	movq	104(%r12), %rcx
	movl	$-4190209, %edx         # imm = 0xFFC00FFF
	andl	76(%rcx), %edx
	orl	%eax, %edx
	movl	%edx, 76(%rcx)
	movl	$12582912, %eax         # imm = 0xC00000
	andl	12(%r14), %eax
	movq	104(%r12), %rcx
	movl	$-12582913, %edx        # imm = 0xFF3FFFFF
	andl	76(%rcx), %edx
	orl	%eax, %edx
	movl	%edx, 76(%rcx)
	movl	$1056964608, %eax       # imm = 0x3F000000
	andl	12(%r14), %eax
	movq	104(%r12), %rcx
	movl	$-1056964609, %edx      # imm = 0xC0FFFFFF
	andl	76(%rcx), %edx
	orl	%eax, %edx
	movl	%edx, 76(%rcx)
	movl	$-2147483648, %eax      # imm = 0x80000000
	andl	12(%r14), %eax
	movq	104(%r12), %rcx
	movl	$2147483647, %edx       # imm = 0x7FFFFFFF
	andl	76(%rcx), %edx
	orl	%eax, %edx
	movl	%edx, 76(%rcx)
	movl	$1073741824, %eax       # imm = 0x40000000
	andl	12(%r14), %eax
	movq	104(%r12), %rcx
	movl	$-1073741825, %edx      # imm = 0xBFFFFFFF
	andl	76(%rcx), %edx
	orl	%eax, %edx
	movl	%edx, 76(%rcx)
	movb	(%r14), %al
	andb	$1, %al
	movq	104(%r12), %rcx
	movb	64(%rcx), %dl
	andb	$-2, %dl
	orb	%al, %dl
	movb	%dl, 64(%rcx)
	movb	(%r14), %al
	andb	$2, %al
	movq	104(%r12), %rcx
	movb	64(%rcx), %dl
	andb	$-3, %dl
	orb	%al, %dl
	movb	%dl, 64(%rcx)
	movb	(%r14), %al
	andb	$4, %al
	movq	104(%r12), %rcx
	movb	64(%rcx), %dl
	andb	$-5, %dl
	orb	%al, %dl
	movb	%dl, 64(%rcx)
	movb	(%r14), %al
	andb	$112, %al
	movq	104(%r12), %rcx
	movb	64(%rcx), %dl
	andb	$-113, %dl
	orb	%al, %dl
	movb	%dl, 64(%rcx)
	movzwl	8(%r14), %ecx
	movq	104(%r12), %rax
	movw	%cx, 72(%rax)
	movzwl	10(%r14), %ecx
	movw	%cx, 74(%rax)
	testb	$1, 43(%r12)
	je	.LBB1_61
# BB#60:
	movb	68(%rax), %cl
	andb	$-4, %cl
	orb	$1, %cl
	movb	%cl, 68(%rax)
.LBB1_61:
	addq	$1080, %rsp             # imm = 0x438
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_62:
	xorl	%ecx, %ecx
.LBB1_63:
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_65
# BB#64:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_65:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_53
# BB#66:
	callq	DisposeObject
	jmp	.LBB1_53
.Lfunc_end1:
	.size	SetOptimize, .Lfunc_end1-SetOptimize
	.cfi_endproc

	.globl	GazumpOptimize
	.p2align	4, 0x90
	.type	GazumpOptimize,@function
GazumpOptimize:                         # @GazumpOptimize
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 48
.Lcfi41:
	.cfi_offset %rbx, -40
.Lcfi42:
	.cfi_offset %r12, -32
.Lcfi43:
	.cfi_offset %r14, -24
.Lcfi44:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	cmpb	$8, 32(%r14)
	je	.LBB2_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.23, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_2:
	cmpq	$0, 104(%r14)
	jne	.LBB2_4
# BB#3:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.24, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_4:
	movzbl	zz_lengths+26(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB2_5
# BB#6:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_7
.LBB2_5:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB2_7:
	movb	$26, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzwl	42(%r14), %edx
	testb	$1, %dh
	jne	.LBB2_11
# BB#8:
	testb	$8, 42(%r15)
	jne	.LBB2_10
# BB#9:
	testb	$8, 42(%r15)
	jne	.LBB2_10
.LBB2_11:
	movq	24(%r15), %rdi
	.p2align	4, 0x90
.LBB2_12:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB2_12
# BB#13:
	leaq	64(%rbx), %rsi
	shrl	$8, %edx
	andl	$1, %edx
	movq	%rsp, %rcx
	callq	Constrained
	jmp	.LBB2_14
.LBB2_10:
	movabsq	$36028792732385279, %rax # imm = 0x7FFFFF007FFFFF
	movq	%rax, 64(%rbx)
	movl	$8388607, 72(%rbx)      # imm = 0x7FFFFF
.LBB2_14:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_15
# BB#16:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_17
.LBB2_15:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_17:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	112(%r14), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_20
# BB#18:
	testq	%rcx, %rcx
	je	.LBB2_20
# BB#19:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_20:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB2_23
# BB#21:
	testq	%rax, %rax
	je	.LBB2_23
# BB#22:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_23:
	movq	104(%r14), %rax
	movq	(%rax), %rbx
	cmpq	%rax, %rbx
	je	.LBB2_75
	.p2align	4, 0x90
.LBB2_24:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB2_24
# BB#25:                                # %.preheader
	cmpb	$1, %al
	je	.LBB2_27
# BB#26:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.25, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_27:                               # %.loopexit
	leaq	32(%rbx), %r15
	movzwl	44(%rbx), %eax
	andl	$127, %eax
	orl	$52736, %eax            # imm = 0xCE00
	movw	%ax, 44(%rbx)
	movw	$4096, 46(%rbx)         # imm = 0x1000
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB2_35
# BB#28:
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_29
# BB#30:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB2_31
.LBB2_29:
	xorl	%ecx, %ecx
.LBB2_31:
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_33
# BB#32:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_33:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB2_35
# BB#34:
	callq	DisposeObject
.LBB2_35:
	movl	$11, %edi
	movl	$.L.str.26, %esi
	movq	%r15, %rdx
	callq	MakeWord
	movq	%rax, %r12
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_36
# BB#37:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_38
.LBB2_36:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_38:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB2_41
# BB#39:
	testq	%rax, %rax
	je	.LBB2_41
# BB#40:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_41:
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB2_44
# BB#42:
	testq	%rax, %rax
	je	.LBB2_44
# BB#43:
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_44:
	movl	$11, %edi
	movl	$.L.str.27, %esi
	movq	%r15, %rdx
	callq	MakeWord
	movq	%rax, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%r15)
	andl	$1610612736, 40(%r15)   # imm = 0x60000000
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_45
# BB#46:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_47
.LBB2_45:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_47:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	104(%r14), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_50
# BB#48:
	testq	%rcx, %rcx
	je	.LBB2_50
# BB#49:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_50:
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB2_53
# BB#51:
	testq	%rax, %rax
	je	.LBB2_53
# BB#52:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_53:
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB2_54
# BB#55:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_56
.LBB2_54:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB2_56:
	leaq	32(%rbx), %rdx
	movb	$1, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movw	$1, 41(%rbx)
	movzwl	34(%r15), %eax
	movw	%ax, 34(%rbx)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r15), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%rbx), %esi
	andl	%ecx, %esi
	orl	%eax, %esi
	movl	%esi, 36(%rbx)
	andl	36(%r15), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
	movzwl	44(%rbx), %eax
	andl	$127, %eax
	orl	$9728, %eax             # imm = 0x2600
	movw	%ax, 44(%rbx)
	movw	$567, 46(%rbx)          # imm = 0x237
	movl	$11, %edi
	movl	$.L.str.28, %esi
	callq	MakeWord
	movq	%rax, %r15
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_57
# BB#58:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_59
.LBB2_57:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_59:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB2_62
# BB#60:
	testq	%rax, %rax
	je	.LBB2_62
# BB#61:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_62:
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB2_65
# BB#63:
	testq	%rax, %rax
	je	.LBB2_65
# BB#64:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_65:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_66
# BB#67:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_68
.LBB2_66:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_68:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	104(%r14), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_71
# BB#69:
	testq	%rcx, %rcx
	je	.LBB2_71
# BB#70:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_71:
	testq	%rbx, %rbx
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB2_74
# BB#72:
	testq	%rax, %rax
	je	.LBB2_74
# BB#73:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_74:
	orb	$4, 43(%r14)
.LBB2_75:
	movq	120(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB2_86
# BB#76:
	movq	8(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB2_86
# BB#77:
	addq	$16, %rax
	.p2align	4, 0x90
.LBB2_78:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	leaq	16(%rcx), %rax
	cmpb	$0, 32(%rcx)
	je	.LBB2_78
# BB#79:
	movl	48(%rcx), %eax
	movl	160(%r14), %edx
	leal	-1(%rax,%rdx), %eax
	movl	%eax, 160(%r14)
	movq	24(%rcx), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_80
# BB#81:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB2_82
.LBB2_86:
	movl	$65535, 160(%r14)       # imm = 0xFFFF
	jmp	.LBB2_87
.LBB2_80:
	xorl	%ecx, %ecx
.LBB2_82:
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_84
# BB#83:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_84:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB2_87
# BB#85:
	callq	DisposeObject
.LBB2_87:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	GazumpOptimize, .Lfunc_end2-GazumpOptimize
	.cfi_endproc

	.globl	CalculateOptimize
	.p2align	4, 0x90
	.type	CalculateOptimize,@function
CalculateOptimize:                      # @CalculateOptimize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	subq	$552, %rsp              # imm = 0x228
.Lcfi51:
	.cfi_def_cfa_offset 608
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	104(%r15), %rax
	cmpq	%rax, (%rax)
	jne	.LBB3_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.29, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	104(%r15), %rax
.LBB3_2:
	movq	(%rax), %rbx
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB3_3
# BB#4:
	cmpb	$1, %al
	je	.LBB3_6
# BB#5:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.30, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_6:                                # %.loopexit178
	movq	24(%rbx), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_7
# BB#8:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB3_9
.LBB3_7:
	xorl	%ecx, %ecx
.LBB3_9:
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_11
# BB#10:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_11:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB3_13
# BB#12:
	callq	DisposeObject
.LBB3_13:
	movq	112(%r15), %rax
	testq	%rax, %rax
	jne	.LBB3_15
# BB#14:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.31, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	112(%r15), %rax
.LBB3_15:
	cmpq	%rax, 8(%rax)
	jne	.LBB3_17
# BB#16:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.32, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	112(%r15), %rax
.LBB3_17:
	movq	(%rax), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB3_18:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rbx
	leaq	16(%rbx), %rax
	cmpb	$0, 32(%rbx)
	je	.LBB3_18
# BB#19:
	xorl	%edi, %edi
	callq	EnterErrorBlock
	addq	$64, %rbx
	movq	104(%r15), %rdi
	movq	112(%r15), %rdx
	subq	$8, %rsp
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	leaq	12(%rsp), %rax
	movl	$0, %ecx
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movq	%rbx, %rsi
	pushq	%rax
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	callq	FillObject
	addq	$16, %rsp
.Lcfi60:
	.cfi_adjust_cfa_offset -16
	movq	%rax, 104(%r15)
	xorl	%edi, %edi
	callq	LeaveErrorBlock
	movq	104(%r15), %rax
	cmpb	$19, 32(%rax)
	jne	.LBB3_95
# BB#20:
	movq	8(%rax), %rcx
	cmpq	(%rax), %rcx
	je	.LBB3_95
# BB#21:
	movzbl	zz_lengths+2(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB3_22
# BB#23:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB3_24
.LBB3_22:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB3_24:
	movb	$2, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movq	OptGallSym(%rip), %rax
	movq	%rax, 80(%rbp)
	movzwl	34(%r15), %eax
	movw	%ax, 34(%rbp)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r15), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%rbp), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%rbp)
	andl	36(%r15), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbp)
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB3_25
# BB#26:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB3_27
.LBB3_25:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB3_27:
	movb	$10, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movq	OptGallSym(%rip), %rdi
	movl	$146, %esi
	callq	ChildSym
	movq	%rax, 80(%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_28
# BB#29:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_30
.LBB3_28:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_30:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB3_33
# BB#31:
	testq	%rax, %rax
	je	.LBB3_33
# BB#32:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_33:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB3_36
# BB#34:
	testq	%rax, %rax
	je	.LBB3_36
# BB#35:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_36:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r13
	testq	%r13, %r13
	je	.LBB3_37
# BB#38:
	movq	%r13, zz_hold(%rip)
	movq	(%r13), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB3_39
.LBB3_37:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r13
	movq	%r13, zz_hold(%rip)
.LBB3_39:
	movb	$17, 32(%r13)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%r13, 8(%r13)
	movq	%r13, (%r13)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_40
# BB#41:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_42
.LBB3_40:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_42:
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB3_45
# BB#43:
	testq	%rax, %rax
	je	.LBB3_45
# BB#44:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_45:
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB3_48
# BB#46:
	testq	%rax, %rax
	je	.LBB3_48
# BB#47:
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_48:
	leaq	32(%r15), %r14
	cmpl	$0, 4(%rsp)
	je	.LBB3_58
# BB#49:
	movl	$11, %edi
	movl	$.L.str.18, %esi
	movq	%r14, %rdx
	callq	MakeWord
	movq	%rax, %rbx
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_50
# BB#51:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_52
.LBB3_50:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_52:
	testq	%r13, %r13
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	je	.LBB3_55
# BB#53:
	testq	%rax, %rax
	je	.LBB3_55
# BB#54:
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_55:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB3_58
# BB#56:
	testq	%rax, %rax
	je	.LBB3_58
# BB#57:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_58:
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	104(%r15), %rax
	movq	8(%rax), %r14
	cmpq	%rax, %r14
	je	.LBB3_93
# BB#59:                                # %.preheader176.lr.ph
	movl	$0, (%rsp)              # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB3_60:                               # %.preheader176
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_61 Depth 2
                                        #     Child Loop BB3_68 Depth 2
                                        #       Child Loop BB3_69 Depth 3
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB3_61:                               #   Parent Loop BB3_60 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rax), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB3_61
# BB#62:                                #   in Loop: Header=BB3_60 Depth=1
	cmpb	$17, %cl
	jne	.LBB3_92
# BB#63:                                # %.preheader
                                        #   in Loop: Header=BB3_60 Depth=1
	leaq	32(%rax), %rbp
	movq	8(%rax), %rcx
	xorl	%edi, %edi
	cmpq	%rax, %rcx
	jne	.LBB3_68
	jmp	.LBB3_65
	.p2align	4, 0x90
.LBB3_72:                               # %.loopexit
                                        #   in Loop: Header=BB3_68 Depth=2
	movq	8(%rcx), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_65
.LBB3_68:                               #   Parent Loop BB3_60 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_69 Depth 3
	leaq	16(%rcx), %rdx
	jmp	.LBB3_69
	.p2align	4, 0x90
.LBB3_96:                               #   in Loop: Header=BB3_69 Depth=3
	addq	$16, %rdx
.LBB3_69:                               #   Parent Loop BB3_60 Depth=1
                                        #     Parent Loop BB3_68 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdx), %rdx
	movzbl	32(%rdx), %ebx
	testb	%bl, %bl
	je	.LBB3_96
# BB#70:                                #   in Loop: Header=BB3_68 Depth=2
	cmpb	$1, %bl
	je	.LBB3_72
# BB#71:                                # %.outer
                                        #   in Loop: Header=BB3_68 Depth=2
	incl	%edi
	jmp	.LBB3_72
	.p2align	4, 0x90
.LBB3_65:                               # %.outer._crit_edge
                                        #   in Loop: Header=BB3_60 Depth=1
	callq	StringInt
	movl	$11, %edi
	movq	%rax, %rsi
	movq	%rbp, %rdx
	callq	MakeWord
	movq	%rax, %rbp
	cmpq	%r13, 8(%r13)
	je	.LBB3_83
# BB#66:                                #   in Loop: Header=BB3_60 Depth=1
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r12
	testq	%r12, %r12
	je	.LBB3_67
# BB#73:                                #   in Loop: Header=BB3_60 Depth=1
	movq	%r12, zz_hold(%rip)
	movq	(%r12), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB3_74
.LBB3_67:                               #   in Loop: Header=BB3_60 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r12
	movq	%r12, zz_hold(%rip)
.LBB3_74:                               #   in Loop: Header=BB3_60 Depth=1
	movb	$1, 32(%r12)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%r12, 8(%r12)
	movq	%r12, (%r12)
	movzwl	44(%r12), %eax
	andl	$127, %eax
	orl	$9728, %eax             # imm = 0x2600
	movw	%ax, 44(%r12)
	movw	$120, 46(%r12)
	movl	(%rsp), %eax            # 4-byte Reload
	incl	%eax
	movl	%eax, (%rsp)            # 4-byte Spill
	cltq
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	setne	41(%r12)
	sete	42(%r12)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_75
# BB#76:                                #   in Loop: Header=BB3_60 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_77
.LBB3_75:                               #   in Loop: Header=BB3_60 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_77:                               #   in Loop: Header=BB3_60 Depth=1
	testq	%r13, %r13
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	je	.LBB3_80
# BB#78:                                #   in Loop: Header=BB3_60 Depth=1
	testq	%rax, %rax
	je	.LBB3_80
# BB#79:                                #   in Loop: Header=BB3_60 Depth=1
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_80:                               #   in Loop: Header=BB3_60 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB3_83
# BB#81:                                #   in Loop: Header=BB3_60 Depth=1
	testq	%rax, %rax
	je	.LBB3_83
# BB#82:                                #   in Loop: Header=BB3_60 Depth=1
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_83:                               #   in Loop: Header=BB3_60 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_84
# BB#85:                                #   in Loop: Header=BB3_60 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_86
.LBB3_84:                               #   in Loop: Header=BB3_60 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_86:                               #   in Loop: Header=BB3_60 Depth=1
	testq	%r13, %r13
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	je	.LBB3_89
# BB#87:                                #   in Loop: Header=BB3_60 Depth=1
	testq	%rax, %rax
	je	.LBB3_89
# BB#88:                                #   in Loop: Header=BB3_60 Depth=1
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_89:                               #   in Loop: Header=BB3_60 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB3_92
# BB#90:                                #   in Loop: Header=BB3_60 Depth=1
	testq	%rax, %rax
	je	.LBB3_92
# BB#91:                                #   in Loop: Header=BB3_60 Depth=1
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_92:                               # %.loopexit177
                                        #   in Loop: Header=BB3_60 Depth=1
	movq	8(%r14), %r14
	cmpq	104(%r15), %r14
	jne	.LBB3_60
.LBB3_93:                               # %._crit_edge196
	movq	%r14, %rdi
	callq	DisposeObject
	movq	$0, 104(%r15)
	movq	112(%r15), %rdi
	callq	DisposeObject
	movq	$0, 112(%r15)
	cmpl	$0, AllowCrossDb(%rip)
	je	.LBB3_95
# BB#94:
	movq	80(%r15), %rdi
	callq	SymName
	leaq	32(%rsp), %r14
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	strcpy
	movq	%r14, %rdi
	callq	strlen
	movw	$46, 32(%rsp,%rax)
	movl	36(%r15), %edi
	andl	$1048575, %edi          # imm = 0xFFFFF
	callq	StringInt
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	strcat
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	DatabaseFileNum
	movzwl	%ax, %ebp
	leaq	12(%rsp), %rdx
	leaq	8(%rsp), %rcx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	callq	AppendToFile
	movq	NewCrossDb(%rip), %rdi
	movq	OptGallSym(%rip), %rdx
	movslq	12(%rsp), %r10
	movl	8(%rsp), %eax
	xorl	%esi, %esi
	movl	$.L.str.33, %r9d
	movq	%r14, %rcx
	movq	%rbx, %r8
	pushq	$0
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	callq	DbInsert
	addq	$32, %rsp
.Lcfi65:
	.cfi_adjust_cfa_offset -32
.LBB3_95:
	addq	$552, %rsp              # imm = 0x228
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	CalculateOptimize, .Lfunc_end3-CalculateOptimize
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"FindOptimize: type(x) != CLOSURE!"
	.size	.L.str.1, 34

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"FindOptimize: x has no target!"
	.size	.L.str.2, 31

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"FindOptimize: Down(PAR)!"
	.size	.L.str.3, 25

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"FindOptimize: res == nilobj!"
	.size	.L.str.4, 29

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"unable to evaluate %s parameter, assuming value is No"
	.size	.L.str.5, 54

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"@Optimize"
	.size	.L.str.6, 10

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Yes"
	.size	.L.str.7, 4

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"No"
	.size	.L.str.8, 3

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"value of %s operator is neither Yes nor No, assuming No"
	.size	.L.str.9, 56

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"SetOptimize: res == nilobj!"
	.size	.L.str.11, 28

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"SetOptimize: type(res) != CLOSURE!"
	.size	.L.str.12, 35

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"SetOptimize: actual(res) != Opt!"
	.size	.L.str.13, 33

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"SetOptimize: Down(res) == res!"
	.size	.L.str.14, 31

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"SetOptimize: type(y) != PAR!"
	.size	.L.str.15, 29

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"SetOptimize: type(y) != ACAT!"
	.size	.L.str.16, 30

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"SetOptimize: type(y) != ACAT (2)!"
	.size	.L.str.17, 34

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"h"
	.size	.L.str.18, 2

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"%d"
	.size	.L.str.19, 3

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"SetOptimize: num <= 0!"
	.size	.L.str.20, 23

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"SetOptimize: type(z)!"
	.size	.L.str.21, 22

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"GazumpOptimize: type(hd) != HEAD!"
	.size	.L.str.23, 34

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"GazumpOptimize: opt_c!"
	.size	.L.str.24, 23

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"FlushGalley: type(g) != GAP_OBJ!"
	.size	.L.str.25, 33

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"1rt"
	.size	.L.str.26, 4

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.zero	1
	.size	.L.str.27, 1

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"1c"
	.size	.L.str.28, 3

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"CO!"
	.size	.L.str.29, 4

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"CalculateOptimize: type(last)!"
	.size	.L.str.30, 31

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"KillGalley: no opt_constraints!"
	.size	.L.str.31, 32

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"KillGalleyo!"
	.size	.L.str.32, 13

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"0"
	.size	.L.str.33, 2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
