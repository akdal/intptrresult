	.text
	.file	"jacobi-1d-imper.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.quad	4611686018427387904     # double 2
	.quad	4611686018427387904     # double 2
.LCPI6_1:
	.quad	4666723172467343360     # double 1.0E+4
	.quad	4666723172467343360     # double 1.0E+4
.LCPI6_2:
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
.LCPI6_3:
	.quad	2                       # 0x2
	.quad	2                       # 0x2
.LCPI6_7:
	.quad	4599676359373071550     # double 0.33333000000000002
	.quad	4599676359373071550     # double 0.33333000000000002
.LCPI6_9:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_4:
	.quad	4611686018427387904     # double 2
.LCPI6_5:
	.quad	4666723172467343360     # double 1.0E+4
.LCPI6_6:
	.quad	4613937818241073152     # double 3
.LCPI6_8:
	.quad	4599676359373071550     # double 0.33333000000000002
.LCPI6_10:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -48
.Lcfi8:
	.cfi_offset %r12, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$80000, %edx            # imm = 0x13880
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_66
# BB#1:
	movq	8(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_66
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$80000, %edx            # imm = 0x13880
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_66
# BB#3:                                 # %polybench_alloc_data.exit
	movq	8(%rsp), %r12
	testq	%r12, %r12
	je	.LBB6_66
# BB#4:                                 # %polybench_alloc_data.exit31
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$80000, %edx            # imm = 0x13880
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_66
# BB#5:                                 # %polybench_alloc_data.exit31
	movq	8(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_66
# BB#6:                                 # %polybench_alloc_data.exit33
	leaq	80000(%r14), %rax
	leaq	80000(%rbx), %r8
	cmpq	%r8, %r14
	jae	.LBB6_8
# BB#7:                                 # %polybench_alloc_data.exit33
	cmpq	%rax, %rbx
	jae	.LBB6_8
# BB#10:                                # %scalar.ph.preheader
	xorl	%ecx, %ecx
	movsd	.LCPI6_4(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI6_5(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB6_11:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%ecx, %xmm3
	movapd	%xmm3, %xmm4
	addsd	%xmm0, %xmm4
	divsd	%xmm1, %xmm4
	movsd	%xmm4, (%r14,%rcx,8)
	addsd	%xmm2, %xmm3
	divsd	%xmm1, %xmm3
	movsd	%xmm3, (%rbx,%rcx,8)
	incq	%rcx
	cmpq	$10000, %rcx            # imm = 0x2710
	jne	.LBB6_11
	jmp	.LBB6_12
.LBB6_8:                                # %vector.body.preheader
	movl	$1, %ecx
	movd	%rcx, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	xorl	%ecx, %ecx
	movapd	.LCPI6_0(%rip), %xmm1   # xmm1 = [2.000000e+00,2.000000e+00]
	movapd	.LCPI6_1(%rip), %xmm2   # xmm2 = [1.000000e+04,1.000000e+04]
	movapd	.LCPI6_2(%rip), %xmm3   # xmm3 = [3.000000e+00,3.000000e+00]
	movdqa	.LCPI6_3(%rip), %xmm4   # xmm4 = [2,2]
	.p2align	4, 0x90
.LBB6_9:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	pshufd	$232, %xmm0, %xmm5      # xmm5 = xmm0[0,2,2,3]
	cvtdq2pd	%xmm5, %xmm5
	movapd	%xmm5, %xmm6
	addpd	%xmm1, %xmm6
	divpd	%xmm2, %xmm6
	movupd	%xmm6, (%r14,%rcx,8)
	addpd	%xmm3, %xmm5
	divpd	%xmm2, %xmm5
	movupd	%xmm5, (%rbx,%rcx,8)
	addq	$2, %rcx
	paddq	%xmm4, %xmm0
	cmpq	$10000, %rcx            # imm = 0x2710
	jne	.LBB6_9
.LBB6_12:                               # %.preheader1.i.preheader
	leaq	8(%r14), %rcx
	leaq	79992(%r14), %rdx
	leaq	8(%rbx), %r9
	leaq	79992(%rbx), %r10
	cmpq	%rax, %r9
	sbbb	%al, %al
	cmpq	%r10, %r14
	sbbb	%r11b, %r11b
	andb	%al, %r11b
	andb	$1, %r11b
	cmpq	%r10, %rcx
	sbbb	%cl, %cl
	cmpq	%rdx, %r9
	sbbb	%bpl, %bpl
	andb	%cl, %bpl
	andb	$1, %bpl
	xorl	%esi, %esi
	movsd	.LCPI6_8(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	.LCPI6_7(%rip), %xmm1   # xmm1 = [3.333300e-01,3.333300e-01]
	.p2align	4, 0x90
.LBB6_13:                               # %.preheader1.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_16 Depth 2
                                        #     Child Loop BB6_19 Depth 2
                                        #     Child Loop BB6_23 Depth 2
                                        #     Child Loop BB6_26 Depth 2
                                        #     Child Loop BB6_29 Depth 2
	testb	%r11b, %r11b
	je	.LBB6_15
# BB#14:                                #   in Loop: Header=BB6_13 Depth=1
	movl	$1, %edi
	jmp	.LBB6_18
	.p2align	4, 0x90
.LBB6_15:                               # %vector.body97.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_16:                               # %vector.body97
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	(%r14,%rcx,8), %xmm2
	movupd	16(%r14,%rcx,8), %xmm3
	movupd	8(%r14,%rcx,8), %xmm4
	movupd	24(%r14,%rcx,8), %xmm5
	addpd	%xmm2, %xmm4
	addpd	%xmm3, %xmm5
	movupd	32(%r14,%rcx,8), %xmm2
	addpd	%xmm3, %xmm4
	addpd	%xmm5, %xmm2
	mulpd	%xmm1, %xmm4
	mulpd	%xmm1, %xmm2
	movupd	%xmm4, 8(%rbx,%rcx,8)
	movupd	%xmm2, 24(%rbx,%rcx,8)
	addq	$4, %rcx
	cmpq	$9996, %rcx             # imm = 0x270C
	jne	.LBB6_16
# BB#17:                                #   in Loop: Header=BB6_13 Depth=1
	movl	$9997, %edi             # imm = 0x270D
.LBB6_18:                               # %scalar.ph99.preheader.new
                                        #   in Loop: Header=BB6_13 Depth=1
	decq	%rdi
	.p2align	4, 0x90
.LBB6_19:                               # %scalar.ph99
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r14,%rdi,8), %xmm2    # xmm2 = mem[0],zero
	addsd	8(%r14,%rdi,8), %xmm2
	addsd	16(%r14,%rdi,8), %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, 8(%rbx,%rdi,8)
	movsd	8(%r14,%rdi,8), %xmm2   # xmm2 = mem[0],zero
	addsd	16(%r14,%rdi,8), %xmm2
	addsd	24(%r14,%rdi,8), %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, 16(%rbx,%rdi,8)
	addq	$2, %rdi
	cmpq	$9998, %rdi             # imm = 0x270E
	jne	.LBB6_19
# BB#20:                                # %vector.memcheck89
                                        #   in Loop: Header=BB6_13 Depth=1
	testb	%bpl, %bpl
	je	.LBB6_22
# BB#21:                                #   in Loop: Header=BB6_13 Depth=1
	movl	$1, %edi
	jmp	.LBB6_25
	.p2align	4, 0x90
.LBB6_22:                               # %vector.body76.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	$11, %ecx
	.p2align	4, 0x90
.LBB6_23:                               # %vector.body76
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-80(%rbx,%rcx,8), %xmm2
	movups	-64(%rbx,%rcx,8), %xmm3
	movups	%xmm2, -80(%r14,%rcx,8)
	movups	%xmm3, -64(%r14,%rcx,8)
	movups	-48(%rbx,%rcx,8), %xmm2
	movups	-32(%rbx,%rcx,8), %xmm3
	movups	%xmm2, -48(%r14,%rcx,8)
	movups	%xmm3, -32(%r14,%rcx,8)
	movupd	-16(%rbx,%rcx,8), %xmm2
	movupd	(%rbx,%rcx,8), %xmm3
	movupd	%xmm2, -16(%r14,%rcx,8)
	movupd	%xmm3, (%r14,%rcx,8)
	addq	$12, %rcx
	cmpq	$10007, %rcx            # imm = 0x2717
	jne	.LBB6_23
# BB#24:                                #   in Loop: Header=BB6_13 Depth=1
	movl	$9997, %edi             # imm = 0x270D
.LBB6_25:                               # %.preheader.i.prol.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	movl	$9998, %ecx             # imm = 0x270E
	subq	%rdi, %rcx
	movl	$9999, %edx             # imm = 0x270F
	subl	%edi, %edx
	andl	$6, %edx
	negq	%rdx
	.p2align	4, 0x90
.LBB6_26:                               # %.preheader.i.prol
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rdi,8), %rax
	movq	%rax, (%r14,%rdi,8)
	incq	%rdi
	incq	%rdx
	jne	.LBB6_26
# BB#27:                                # %.preheader.i.prol.loopexit
                                        #   in Loop: Header=BB6_13 Depth=1
	cmpq	$7, %rcx
	jb	.LBB6_30
# BB#28:                                # %.preheader.i.preheader.new
                                        #   in Loop: Header=BB6_13 Depth=1
	addq	$7, %rdi
	.p2align	4, 0x90
.LBB6_29:                               # %.preheader.i
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-56(%rbx,%rdi,8), %rax
	movq	%rax, -56(%r14,%rdi,8)
	movq	-48(%rbx,%rdi,8), %rax
	movq	%rax, -48(%r14,%rdi,8)
	movq	-40(%rbx,%rdi,8), %rax
	movq	%rax, -40(%r14,%rdi,8)
	movq	-32(%rbx,%rdi,8), %rax
	movq	%rax, -32(%r14,%rdi,8)
	movq	-24(%rbx,%rdi,8), %rax
	movq	%rax, -24(%r14,%rdi,8)
	movq	-16(%rbx,%rdi,8), %rax
	movq	%rax, -16(%r14,%rdi,8)
	movq	-8(%rbx,%rdi,8), %rax
	movq	%rax, -8(%r14,%rdi,8)
	movq	(%rbx,%rdi,8), %rax
	movq	%rax, (%r14,%rdi,8)
	addq	$8, %rdi
	cmpq	$10006, %rdi            # imm = 0x2716
	jne	.LBB6_29
.LBB6_30:                               # %.loopexit202
                                        #   in Loop: Header=BB6_13 Depth=1
	incl	%esi
	cmpl	$100, %esi
	jne	.LBB6_13
# BB#31:                                # %kernel_jacobi_1d_imper.exit
	leaq	80000(%r12), %rax
	cmpq	%r8, %r12
	jae	.LBB6_33
# BB#32:                                # %kernel_jacobi_1d_imper.exit
	cmpq	%rax, %rbx
	jae	.LBB6_33
# BB#35:                                # %scalar.ph128.preheader
	xorl	%ecx, %ecx
	movsd	.LCPI6_4(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI6_5(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_6(%rip), %xmm4   # xmm4 = mem[0],zero
	.p2align	4, 0x90
.LBB6_36:                               # %scalar.ph128
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%ecx, %xmm5
	movapd	%xmm5, %xmm6
	addsd	%xmm2, %xmm6
	divsd	%xmm3, %xmm6
	movsd	%xmm6, (%r12,%rcx,8)
	addsd	%xmm4, %xmm5
	divsd	%xmm3, %xmm5
	movsd	%xmm5, (%rbx,%rcx,8)
	incq	%rcx
	cmpq	$10000, %rcx            # imm = 0x2710
	jne	.LBB6_36
	jmp	.LBB6_37
.LBB6_33:                               # %vector.body126.preheader
	movl	$1, %ecx
	movd	%rcx, %xmm2
	pslldq	$8, %xmm2               # xmm2 = zero,zero,zero,zero,zero,zero,zero,zero,xmm2[0,1,2,3,4,5,6,7]
	xorl	%edx, %edx
	movapd	.LCPI6_0(%rip), %xmm8   # xmm8 = [2.000000e+00,2.000000e+00]
	movapd	.LCPI6_1(%rip), %xmm4   # xmm4 = [1.000000e+04,1.000000e+04]
	movapd	.LCPI6_2(%rip), %xmm5   # xmm5 = [3.000000e+00,3.000000e+00]
	movdqa	.LCPI6_3(%rip), %xmm6   # xmm6 = [2,2]
	.p2align	4, 0x90
.LBB6_34:                               # %vector.body126
                                        # =>This Inner Loop Header: Depth=1
	pshufd	$232, %xmm2, %xmm7      # xmm7 = xmm2[0,2,2,3]
	cvtdq2pd	%xmm7, %xmm3
	movapd	%xmm3, %xmm7
	addpd	%xmm8, %xmm7
	divpd	%xmm4, %xmm7
	movupd	%xmm7, (%r12,%rdx,8)
	addpd	%xmm5, %xmm3
	divpd	%xmm4, %xmm3
	movupd	%xmm3, (%rbx,%rdx,8)
	addq	$2, %rdx
	paddq	%xmm6, %xmm2
	cmpq	$10000, %rdx            # imm = 0x2710
	jne	.LBB6_34
.LBB6_37:                               # %.preheader1.i42.preheader
	leaq	8(%r12), %rcx
	leaq	79992(%r12), %rdx
	cmpq	%rax, %r9
	sbbb	%al, %al
	cmpq	%r10, %r12
	sbbb	%r8b, %r8b
	andb	%al, %r8b
	andb	$1, %r8b
	cmpq	%r10, %rcx
	sbbb	%cl, %cl
	cmpq	%rdx, %r9
	sbbb	%al, %al
	andb	%cl, %al
	andb	$1, %al
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_38:                               # %.preheader1.i42
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_41 Depth 2
                                        #     Child Loop BB6_44 Depth 2
                                        #     Child Loop BB6_48 Depth 2
                                        #     Child Loop BB6_51 Depth 2
                                        #     Child Loop BB6_54 Depth 2
	testb	%r8b, %r8b
	je	.LBB6_40
# BB#39:                                #   in Loop: Header=BB6_38 Depth=1
	movl	$1, %esi
	jmp	.LBB6_43
	.p2align	4, 0x90
.LBB6_40:                               # %vector.body173.preheader
                                        #   in Loop: Header=BB6_38 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_41:                               # %vector.body173
                                        #   Parent Loop BB6_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	(%r12,%rsi,8), %xmm2
	movupd	16(%r12,%rsi,8), %xmm3
	movupd	8(%r12,%rsi,8), %xmm4
	movupd	24(%r12,%rsi,8), %xmm5
	addpd	%xmm2, %xmm4
	addpd	%xmm3, %xmm5
	movupd	32(%r12,%rsi,8), %xmm2
	addpd	%xmm3, %xmm4
	addpd	%xmm5, %xmm2
	mulpd	%xmm1, %xmm4
	mulpd	%xmm1, %xmm2
	movupd	%xmm4, 8(%rbx,%rsi,8)
	movupd	%xmm2, 24(%rbx,%rsi,8)
	addq	$4, %rsi
	cmpq	$9996, %rsi             # imm = 0x270C
	jne	.LBB6_41
# BB#42:                                #   in Loop: Header=BB6_38 Depth=1
	movl	$9997, %esi             # imm = 0x270D
.LBB6_43:                               # %scalar.ph175.preheader.new
                                        #   in Loop: Header=BB6_38 Depth=1
	decq	%rsi
	.p2align	4, 0x90
.LBB6_44:                               # %scalar.ph175
                                        #   Parent Loop BB6_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r12,%rsi,8), %xmm2    # xmm2 = mem[0],zero
	addsd	8(%r12,%rsi,8), %xmm2
	addsd	16(%r12,%rsi,8), %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, 8(%rbx,%rsi,8)
	movsd	8(%r12,%rsi,8), %xmm2   # xmm2 = mem[0],zero
	addsd	16(%r12,%rsi,8), %xmm2
	addsd	24(%r12,%rsi,8), %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, 16(%rbx,%rsi,8)
	addq	$2, %rsi
	cmpq	$9998, %rsi             # imm = 0x270E
	jne	.LBB6_44
# BB#45:                                # %vector.memcheck160
                                        #   in Loop: Header=BB6_38 Depth=1
	testb	%al, %al
	je	.LBB6_47
# BB#46:                                #   in Loop: Header=BB6_38 Depth=1
	movl	$1, %esi
	jmp	.LBB6_50
	.p2align	4, 0x90
.LBB6_47:                               # %vector.body147.preheader
                                        #   in Loop: Header=BB6_38 Depth=1
	movl	$11, %edx
	.p2align	4, 0x90
.LBB6_48:                               # %vector.body147
                                        #   Parent Loop BB6_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-80(%rbx,%rdx,8), %xmm2
	movups	-64(%rbx,%rdx,8), %xmm3
	movups	%xmm2, -80(%r12,%rdx,8)
	movups	%xmm3, -64(%r12,%rdx,8)
	movups	-48(%rbx,%rdx,8), %xmm2
	movups	-32(%rbx,%rdx,8), %xmm3
	movups	%xmm2, -48(%r12,%rdx,8)
	movups	%xmm3, -32(%r12,%rdx,8)
	movupd	-16(%rbx,%rdx,8), %xmm2
	movupd	(%rbx,%rdx,8), %xmm3
	movupd	%xmm2, -16(%r12,%rdx,8)
	movupd	%xmm3, (%r12,%rdx,8)
	addq	$12, %rdx
	cmpq	$10007, %rdx            # imm = 0x2717
	jne	.LBB6_48
# BB#49:                                #   in Loop: Header=BB6_38 Depth=1
	movl	$9997, %esi             # imm = 0x270D
.LBB6_50:                               # %.preheader.i49.prol.preheader
                                        #   in Loop: Header=BB6_38 Depth=1
	movl	$9998, %edi             # imm = 0x270E
	subq	%rsi, %rdi
	movl	$9999, %edx             # imm = 0x270F
	subl	%esi, %edx
	andl	$6, %edx
	negq	%rdx
	.p2align	4, 0x90
.LBB6_51:                               # %.preheader.i49.prol
                                        #   Parent Loop BB6_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rsi,8), %rbp
	movq	%rbp, (%r12,%rsi,8)
	incq	%rsi
	incq	%rdx
	jne	.LBB6_51
# BB#52:                                # %.preheader.i49.prol.loopexit
                                        #   in Loop: Header=BB6_38 Depth=1
	cmpq	$7, %rdi
	jb	.LBB6_55
# BB#53:                                # %.preheader.i49.preheader.new
                                        #   in Loop: Header=BB6_38 Depth=1
	addq	$7, %rsi
	.p2align	4, 0x90
.LBB6_54:                               # %.preheader.i49
                                        #   Parent Loop BB6_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-56(%rbx,%rsi,8), %rdx
	movq	%rdx, -56(%r12,%rsi,8)
	movq	-48(%rbx,%rsi,8), %rdx
	movq	%rdx, -48(%r12,%rsi,8)
	movq	-40(%rbx,%rsi,8), %rdx
	movq	%rdx, -40(%r12,%rsi,8)
	movq	-32(%rbx,%rsi,8), %rdx
	movq	%rdx, -32(%r12,%rsi,8)
	movq	-24(%rbx,%rsi,8), %rdx
	movq	%rdx, -24(%r12,%rsi,8)
	movq	-16(%rbx,%rsi,8), %rdx
	movq	%rdx, -16(%r12,%rsi,8)
	movq	-8(%rbx,%rsi,8), %rdx
	movq	%rdx, -8(%r12,%rsi,8)
	movq	(%rbx,%rsi,8), %rdx
	movq	%rdx, (%r12,%rsi,8)
	addq	$8, %rsi
	cmpq	$10006, %rsi            # imm = 0x2716
	jne	.LBB6_54
.LBB6_55:                               # %.loopexit
                                        #   in Loop: Header=BB6_38 Depth=1
	incl	%ecx
	cmpl	$100, %ecx
	jne	.LBB6_38
# BB#56:                                # %kernel_jacobi_1d_imper.exit51.preheader
	movl	$1, %edx
	movapd	.LCPI6_9(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_10(%rip), %xmm3  # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB6_57:                               # %kernel_jacobi_1d_imper.exit51
                                        # =>This Inner Loop Header: Depth=1
	movsd	-8(%r14,%rdx,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%r12,%rdx,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_58
# BB#60:                                # %kernel_jacobi_1d_imper.exit51.1209
                                        #   in Loop: Header=BB6_57 Depth=1
	movsd	(%r14,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r12,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_59
# BB#61:                                #   in Loop: Header=BB6_57 Depth=1
	leaq	2(%rdx), %rax
	incq	%rdx
	cmpq	$10000, %rdx            # imm = 0x2710
	movq	%rax, %rdx
	jl	.LBB6_57
# BB#62:                                # %check_FP.exit
	movl	$160001, %edi           # imm = 0x27101
	callq	malloc
	movq	%rax, %r15
	movb	$0, 160000(%r15)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_63:                               # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rax), %rcx
	movl	%ecx, %edx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, (%r15,%rax,2)
	movb	%dl, 1(%r15,%rax,2)
	movq	%rcx, %rdx
	shrq	$8, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 2(%r15,%rax,2)
	movb	%dl, 3(%r15,%rax,2)
	movq	%rcx, %rdx
	shrq	$16, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 4(%r15,%rax,2)
	movb	%dl, 5(%r15,%rax,2)
	movl	%ecx, %edx
	shrl	$24, %edx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 6(%r15,%rax,2)
	movb	%dl, 7(%r15,%rax,2)
	movq	%rcx, %rdx
	shrq	$32, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 8(%r15,%rax,2)
	movb	%dl, 9(%r15,%rax,2)
	movq	%rcx, %rdx
	shrq	$40, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 10(%r15,%rax,2)
	movb	%dl, 11(%r15,%rax,2)
	movq	%rcx, %rdx
	shrq	$48, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 12(%r15,%rax,2)
	movb	%dl, 13(%r15,%rax,2)
	shrq	$56, %rcx
	andb	$15, %cl
	orb	$48, %cl
	movb	%cl, 14(%r15,%rax,2)
	movb	%cl, 15(%r15,%rax,2)
	addq	$8, %rax
	cmpq	$80000, %rax            # imm = 0x13880
	jne	.LBB6_63
# BB#64:                                # %print_array.exit
	movq	stderr(%rip), %rsi
	movq	%r15, %rdi
	callq	fputs
	movq	%r15, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_65
.LBB6_58:                               # %check_FP.exit.threadsplit
	decq	%rdx
.LBB6_59:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_10(%rip), %xmm2  # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %ecx
	callq	fprintf
	movl	$1, %eax
.LBB6_65:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_66:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d] = %lf and B[%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 68


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
