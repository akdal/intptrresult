	.text
	.file	"lmem.bc"
	.hidden	luaM_growaux_
	.globl	luaM_growaux_
	.p2align	4, 0x90
	.type	luaM_growaux_,@function
luaM_growaux_:                          # @luaM_growaux_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movq	%rcx, %r13
	movq	%rdx, %r14
	movq	%rsi, %r12
	movl	(%r14), %eax
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
	cmpl	%ecx, %eax
	jge	.LBB0_1
# BB#3:
	addl	%eax, %eax
	cmpl	$3, %eax
	movl	$4, %ebx
	cmovgl	%eax, %ebx
	jmp	.LBB0_4
.LBB0_1:
	cmpl	%ebx, %eax
	jl	.LBB0_4
# BB#2:
	xorl	%eax, %eax
	movq	%rdi, %rbp
	movq	%r9, %rsi
	callq	luaG_runerror
	movq	%rbp, %rdi
.LBB0_4:
	leal	1(%rbx), %eax
	movslq	%eax, %rcx
	movq	$-3, %rax
	xorl	%edx, %edx
	divq	%r13
	cmpq	%rax, %rcx
	jbe	.LBB0_5
# BB#9:
	xorl	%r12d, %r12d
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	luaG_runerror
	jmp	.LBB0_10
.LBB0_5:
	movslq	(%r14), %rbp
	imulq	%r13, %rbp
	movslq	%ebx, %r15
	imulq	%r13, %r15
	movq	%rdi, (%rsp)            # 8-byte Spill
	movq	32(%rdi), %r13
	movq	24(%r13), %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	movq	%r15, %rcx
	callq	*16(%r13)
	movq	%rax, %r12
	testq	%r15, %r15
	je	.LBB0_8
# BB#6:
	testq	%r12, %r12
	jne	.LBB0_8
# BB#7:
	movl	$4, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	luaD_throw
.LBB0_8:                                # %luaM_realloc_.exit
	subq	%rbp, %r15
	addq	%r15, 120(%r13)
.LBB0_10:
	movl	%ebx, (%r14)
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	luaM_growaux_, .Lfunc_end0-luaM_growaux_
	.cfi_endproc

	.hidden	luaM_realloc_
	.globl	luaM_realloc_
	.p2align	4, 0x90
	.type	luaM_realloc_,@function
luaM_realloc_:                          # @luaM_realloc_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -48
.Lcfi19:
	.cfi_offset %r12, -40
.Lcfi20:
	.cfi_offset %r13, -32
.Lcfi21:
	.cfi_offset %r14, -24
.Lcfi22:
	.cfi_offset %r15, -16
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rdi, %r15
	movq	32(%r15), %r13
	movq	24(%r13), %rdi
	callq	*16(%r13)
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.LBB1_3
# BB#1:
	testq	%r12, %r12
	jne	.LBB1_3
# BB#2:
	movl	$4, %esi
	movq	%r15, %rdi
	callq	luaD_throw
.LBB1_3:
	subq	%r14, %rbx
	addq	%rbx, 120(%r13)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	luaM_realloc_, .Lfunc_end1-luaM_realloc_
	.cfi_endproc

	.hidden	luaM_toobig
	.globl	luaM_toobig
	.p2align	4, 0x90
	.type	luaM_toobig,@function
luaM_toobig:                            # @luaM_toobig
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 16
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	luaG_runerror
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end2:
	.size	luaM_toobig, .Lfunc_end2-luaM_toobig
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"memory allocation error: block too big"
	.size	.L.str, 39

	.hidden	luaG_runerror
	.hidden	luaD_throw

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
