	.text
	.file	"beamformer.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_4:                                #   in Loop: Header=BB0_1 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, numiters(%rip)
.LBB0_1:                                # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str, %edx
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	getopt
	cmpl	$105, %eax
	je	.LBB0_4
# BB#2:                                 # %.backedge
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpl	$-1, %eax
	jne	.LBB0_1
# BB#3:
	callq	begin_StrictFP
	callq	begin
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_2:
	.quad	4607182418800017408     # double 1
	.text
	.globl	begin_StrictFP
	.p2align	4, 0x90
	.type	begin_StrictFP,@function
begin_StrictFP:                         # @begin_StrictFP
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	subq	$847128, %rsp           # imm = 0xCED18
.Lcfi11:
	.cfi_def_cfa_offset 847184
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_1:                                # %BeamFirSetup_StrictFP.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	$64, 912(%rsp,%rbp)
	movl	$0, 920(%rsp,%rbp)
	movl	$512, %edi              # imm = 0x200
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, 928(%rsp,%rbp)
	movl	$512, %edi              # imm = 0x200
	callq	malloc
	movq	%rax, 936(%rsp,%rbp)
	movl	$1065353216, (%rbx)     # imm = 0x3F800000
	leaq	4(%rbx), %rdi
	leaq	4(%rax), %rbx
	xorl	%esi, %esi
	movl	$508, %edx              # imm = 0x1FC
	callq	memset
	xorl	%esi, %esi
	movl	$508, %edx              # imm = 0x1FC
	movq	%rbx, %rdi
	callq	memset
	movq	$64, 528(%rsp,%rbp)
	movl	$0, 536(%rsp,%rbp)
	movl	$512, %edi              # imm = 0x200
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, 544(%rsp,%rbp)
	movl	$512, %edi              # imm = 0x200
	callq	malloc
	movq	%rax, 552(%rsp,%rbp)
	movl	$1065353216, (%rbx)     # imm = 0x3F800000
	leaq	4(%rbx), %rdi
	leaq	4(%rax), %rbx
	xorl	%esi, %esi
	movl	$508, %edx              # imm = 0x1FC
	callq	memset
	xorl	%esi, %esi
	movl	$508, %edx              # imm = 0x1FC
	movq	%rbx, %rdi
	callq	memset
	addq	$32, %rbp
	cmpq	$384, %rbp              # imm = 0x180
	jne	.LBB1_1
# BB#2:                                 # %.preheader96.preheader
	leaq	400(%rsp), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader96
                                        # =>This Inner Loop Header: Depth=1
	movq	$512, (%r14)            # imm = 0x200
	movl	$0, 8(%r14)
	movl	$4096, %edi             # imm = 0x1000
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, 16(%r14)
	movl	$4096, %edi             # imm = 0x1000
	callq	malloc
	movq	%rax, 24(%r14)
	movl	$1065353216, (%rbx)     # imm = 0x3F800000
	leaq	4(%rbx), %rdi
	leaq	4(%rax), %rbx
	xorl	%esi, %esi
	movl	$4092, %edx             # imm = 0xFFC
	callq	memset
	xorl	%esi, %esi
	movl	$4092, %edx             # imm = 0xFFC
	movq	%rbx, %rdi
	callq	memset
	testq	%rbp, %rbp
	movss	.LCPI1_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	je	.LBB1_5
# BB#4:                                 # %.preheader96
                                        #   in Loop: Header=BB1_3 Depth=1
	xorps	%xmm0, %xmm0
.LBB1_5:                                # %.preheader96
                                        #   in Loop: Header=BB1_3 Depth=1
	movss	%xmm0, 16(%rsp,%rbp)
	movl	$0, 20(%rsp,%rbp)
	cmpq	$96, %rbp
	movaps	%xmm1, %xmm0
	je	.LBB1_7
# BB#6:                                 # %.preheader96
                                        #   in Loop: Header=BB1_3 Depth=1
	xorps	%xmm0, %xmm0
.LBB1_7:                                # %.preheader96
                                        #   in Loop: Header=BB1_3 Depth=1
	movss	%xmm0, 24(%rsp,%rbp)
	movl	$0, 28(%rsp,%rbp)
	cmpq	$192, %rbp
	movaps	%xmm1, %xmm0
	je	.LBB1_9
# BB#8:                                 # %.preheader96
                                        #   in Loop: Header=BB1_3 Depth=1
	xorps	%xmm0, %xmm0
.LBB1_9:                                # %.preheader96
                                        #   in Loop: Header=BB1_3 Depth=1
	movss	%xmm0, 32(%rsp,%rbp)
	movl	$0, 36(%rsp,%rbp)
	cmpq	$288, %rbp              # imm = 0x120
	movaps	%xmm1, %xmm0
	je	.LBB1_11
# BB#10:                                # %.preheader96
                                        #   in Loop: Header=BB1_3 Depth=1
	xorps	%xmm0, %xmm0
.LBB1_11:                               # %.preheader96
                                        #   in Loop: Header=BB1_3 Depth=1
	movss	%xmm0, 40(%rsp,%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 92(%rsp,%rbp)
	movups	%xmm0, 76(%rsp,%rbp)
	movups	%xmm0, 60(%rsp,%rbp)
	movups	%xmm0, 44(%rsp,%rbp)
	movl	$0, 108(%rsp,%rbp)
	addq	$96, %rbp
	addq	$32, %r14
	cmpq	$384, %rbp              # imm = 0x180
	jne	.LBB1_3
# BB#12:                                # %.preheader94.preheader
	leaq	257296(%rsp), %r15
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_13:                               # %.preheader94
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_14 Depth 2
                                        #       Child Loop BB1_15 Depth 3
                                        #     Child Loop BB1_43 Depth 2
                                        #       Child Loop BB1_44 Depth 3
                                        #     Child Loop BB1_20 Depth 2
                                        #     Child Loop BB1_22 Depth 2
	xorl	%r14d, %r14d
	cmpq	$1, %r12
	jne	.LBB1_14
	.p2align	4, 0x90
.LBB1_43:                               # %.preheader94.split.us
                                        #   Parent Loop BB1_13 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_44 Depth 3
	movq	%r14, %rax
	shlq	$13, %rax
	leaq	60688(%rsp,%rax), %rbp
	xorl	%ebx, %ebx
	jmp	.LBB1_44
	.p2align	4, 0x90
.LBB1_51:                               # %.thread.us.split
                                        #   in Loop: Header=BB1_44 Depth=3
	addsd	.LCPI1_2(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 2052(%rbp)
	movl	$257, %ebx              # imm = 0x101
.LBB1_44:                               # %.lr.ph.split.us.i.us
                                        #   Parent Loop BB1_13 Depth=1
                                        #     Parent Loop BB1_43 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB1_46
# BB#45:                                # %call.sqrt
                                        #   in Loop: Header=BB1_44 Depth=3
	movapd	%xmm1, %xmm0
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	sqrt
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
.LBB1_46:                               # %.lr.ph.split.us.i.us.split
                                        #   in Loop: Header=BB1_44 Depth=3
	cmpq	$256, %rbx              # imm = 0x100
	cvtsd2ss	%xmm0, %xmm0
	jne	.LBB1_47
# BB#49:                                # %.thread.us
                                        #   in Loop: Header=BB1_44 Depth=3
	movss	%xmm0, 2048(%rbp)
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB1_51
# BB#50:                                # %call.sqrt279
                                        #   in Loop: Header=BB1_44 Depth=3
	movapd	%xmm1, %xmm0
	callq	sqrt
	jmp	.LBB1_51
	.p2align	4, 0x90
.LBB1_47:                               #   in Loop: Header=BB1_44 Depth=3
	movaps	%xmm0, %xmm1
	movaps	.LCPI1_1(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm1
	leal	(%rbx,%rbx), %eax
	cltq
	movss	%xmm1, (%rbp,%rax,4)
	addss	.LCPI1_0(%rip), %xmm0
	xorps	%xmm2, %xmm0
	leal	1(%rbx,%rbx), %eax
	cltq
	movss	%xmm0, (%rbp,%rax,4)
	incq	%rbx
	cmpq	$1024, %rbx             # imm = 0x400
	jne	.LBB1_44
# BB#48:                                # %InputGenerate_StrictFP.exit.loopexit.us
                                        #   in Loop: Header=BB1_43 Depth=2
	incq	%r14
	cmpq	$12, %r14
	jne	.LBB1_43
	jmp	.LBB1_19
	.p2align	4, 0x90
.LBB1_14:                               # %.preheader94.split
                                        #   Parent Loop BB1_13 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_15 Depth 3
	movq	%r14, %rax
	shlq	$13, %rax
	leaq	60688(%rsp,%rax), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_15:                               # %.lr.ph.split.i
                                        #   Parent Loop BB1_13 Depth=1
                                        #     Parent Loop BB1_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ebx, %eax
	imull	%r12d, %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB1_17
# BB#16:                                # %call.sqrt280
                                        #   in Loop: Header=BB1_15 Depth=3
	movapd	%xmm1, %xmm0
	callq	sqrt
.LBB1_17:                               # %.lr.ph.split.i.split
                                        #   in Loop: Header=BB1_15 Depth=3
	cvtsd2ss	%xmm0, %xmm0
	movaps	%xmm0, %xmm1
	movaps	.LCPI1_1(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm1
	movq	%rbx, %rax
	shlq	$33, %rax
	sarq	$30, %rax
	movss	%xmm1, (%rbp,%rax)
	addss	.LCPI1_0(%rip), %xmm0
	xorps	%xmm2, %xmm0
	leal	1(%rbx,%rbx), %eax
	cltq
	movss	%xmm0, (%rbp,%rax,4)
	incq	%rbx
	cmpq	$1024, %rbx             # imm = 0x400
	jne	.LBB1_15
# BB#18:                                # %InputGenerate_StrictFP.exit
                                        #   in Loop: Header=BB1_14 Depth=2
	incq	%r14
	cmpq	$12, %r14
	jne	.LBB1_14
.LBB1_19:                               # %.preheader93
                                        #   in Loop: Header=BB1_13 Depth=1
	movq	%r12, %r14
	shlq	$5, %r14
	leaq	912(%rsp,%r14), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_20:                               #   Parent Loop BB1_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	60688(%rsp,%rbp), %rcx
	leaq	158992(%rsp,%rbp), %r8
	movl	$1024, %esi             # imm = 0x400
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	BeamFirFilter_StrictFP
	addq	$8, %rbp
	cmpq	$8192, %rbp             # imm = 0x2000
	jne	.LBB1_20
# BB#21:                                # %.preheader92
                                        #   in Loop: Header=BB1_13 Depth=1
	leaq	528(%rsp,%r14), %r14
	xorl	%ebp, %ebp
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB1_22:                               #   Parent Loop BB1_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	158992(%rsp,%rbp), %rcx
	movl	$1024, %esi             # imm = 0x400
	movl	$2, %edx
	movq	%r14, %rdi
	movq	%rbx, %r8
	callq	BeamFirFilter_StrictFP
	addq	$8, %rbx
	addq	$16, %rbp
	cmpq	$8192, %rbp             # imm = 0x2000
	jne	.LBB1_22
# BB#23:                                #   in Loop: Header=BB1_13 Depth=1
	incq	%r12
	addq	$49152, %r15            # imm = 0xC000
	cmpq	$12, %r12
	jne	.LBB1_13
# BB#24:                                # %.preheader89.preheader
	leaq	257308(%rsp), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_25:                               # %.preheader89
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_26 Depth 2
	leaq	(%rcx,%rcx), %rdx
	movl	$100, %esi
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB1_26:                               #   Parent Loop BB1_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-12(%rdi), %ebp
	leaq	11536(%rsp,%rsi), %rbx
	movl	%ebp, -100(%rbx,%rdx,4)
	movl	-8(%rdi), %ebp
	movl	%ebp, -96(%rbx,%rdx,4)
	movl	-4(%rdi), %ebp
	movl	%ebp, -4(%rbx,%rdx,4)
	movl	(%rdi), %ebp
	movl	%ebp, (%rbx,%rdx,4)
	addq	$16, %rdi
	addq	$192, %rsi
	cmpq	$49252, %rsi            # imm = 0xC064
	jne	.LBB1_26
# BB#27:                                #   in Loop: Header=BB1_25 Depth=1
	incq	%rcx
	addq	$49152, %rax            # imm = 0xC000
	cmpq	$12, %rcx
	jne	.LBB1_25
# BB#28:                                # %.preheader87.preheader
	leaq	16(%rsp), %r12
	xorl	%r13d, %r13d
	leaq	1296(%rsp), %r14
	.p2align	4, 0x90
.LBB1_29:                               # %.preheader87
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_30 Depth 2
                                        #       Child Loop BB1_31 Depth 3
                                        #     Child Loop BB1_34 Depth 2
                                        #     Child Loop BB1_36 Depth 2
	movq	%r13, %rax
	shlq	$11, %rax
	leaq	detector_out_StrictFP(%rax), %r15
	leaq	11536(%rsp), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_30:                               #   Parent Loop BB1_29 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_31 Depth 3
	leaq	(%rcx,%rcx), %rdx
	xorps	%xmm0, %xmm0
	movq	%rax, %rsi
	movq	%r12, %rdi
	movl	$12, %ebx
	xorps	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB1_31:                               #   Parent Loop BB1_29 Depth=1
                                        #     Parent Loop BB1_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rdi), %xmm2           # xmm2 = mem[0],zero
	movq	(%rsi), %xmm3           # xmm3 = mem[0],zero
	pshufd	$229, %xmm3, %xmm4      # xmm4 = xmm3[1,1,2,3]
	mulss	%xmm2, %xmm4
	pshufd	$229, %xmm2, %xmm5      # xmm5 = xmm2[1,1,2,3]
	mulps	%xmm3, %xmm2
	movaps	%xmm2, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	subss	%xmm6, %xmm2
	addss	%xmm2, %xmm1
	mulss	%xmm3, %xmm5
	addss	%xmm4, %xmm5
	addss	%xmm5, %xmm0
	addq	$8, %rdi
	addq	$8, %rsi
	decq	%rbx
	jne	.LBB1_31
# BB#32:                                # %BeamForm_StrictFP.exit
                                        #   in Loop: Header=BB1_30 Depth=2
	movss	%xmm1, 7440(%rsp,%rdx,4)
	movss	%xmm0, 7444(%rsp,%rdx,4)
	incq	%rcx
	addq	$96, %rax
	cmpq	$512, %rcx              # imm = 0x200
	jne	.LBB1_30
# BB#33:                                # %.preheader
                                        #   in Loop: Header=BB1_29 Depth=1
	movq	%r13, %rax
	shlq	$5, %rax
	leaq	400(%rsp,%rax), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_34:                               #   Parent Loop BB1_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	7440(%rsp,%rbp), %rcx
	leaq	3344(%rsp,%rbp), %r8
	movl	$512, %esi              # imm = 0x200
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	BeamFirFilter_StrictFP
	addq	$8, %rbp
	cmpq	$4096, %rbp             # imm = 0x1000
	jne	.LBB1_34
# BB#35:                                # %.lr.ph.i81.preheader
                                        #   in Loop: Header=BB1_29 Depth=1
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB1_36:                               # %.lr.ph.i81
                                        #   Parent Loop BB1_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	3336(%rsp,%rbx,8), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	movss	3340(%rsp,%rbx,8), %xmm0 # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB1_38
# BB#37:                                # %call.sqrt281
                                        #   in Loop: Header=BB1_36 Depth=2
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB1_38:                               # %.lr.ph.i81.split
                                        #   in Loop: Header=BB1_36 Depth=2
	movss	%xmm1, 1292(%rsp,%rbx,4)
	movss	3344(%rsp,%rbx,8), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	movss	3348(%rsp,%rbx,8), %xmm0 # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB1_40
# BB#39:                                # %call.sqrt282
                                        #   in Loop: Header=BB1_36 Depth=2
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB1_40:                               # %.lr.ph.i81.split.split
                                        #   in Loop: Header=BB1_36 Depth=2
	movss	%xmm1, 1296(%rsp,%rbx,4)
	addq	$2, %rbx
	cmpq	$513, %rbx              # imm = 0x201
	jne	.LBB1_36
# BB#41:                                # %Magnitude_StrictFP.exit
                                        #   in Loop: Header=BB1_29 Depth=1
	movl	$2048, %edx             # imm = 0x800
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	memcpy
	incq	%r13
	addq	$96, %r12
	cmpq	$4, %r13
	jne	.LBB1_29
# BB#42:
	addq	$847128, %rsp           # imm = 0xCED18
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	begin_StrictFP, .Lfunc_end1-begin_StrictFP
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI2_2:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_3:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
.LCPI2_4:
	.quad	4607182418800017408     # double 1
	.text
	.globl	begin
	.p2align	4, 0x90
	.type	begin,@function
begin:                                  # @begin
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$855320, %rsp           # imm = 0xD0D18
.Lcfi24:
	.cfi_def_cfa_offset 855376
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_1:                                # %BeamFirSetup.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	$64, 912(%rsp,%rbp)
	movl	$0, 920(%rsp,%rbp)
	movl	$512, %edi              # imm = 0x200
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, 928(%rsp,%rbp)
	movl	$512, %edi              # imm = 0x200
	callq	malloc
	movq	%rax, 936(%rsp,%rbp)
	movl	$1065353216, (%rbx)     # imm = 0x3F800000
	leaq	4(%rbx), %rdi
	leaq	4(%rax), %rbx
	xorl	%esi, %esi
	movl	$508, %edx              # imm = 0x1FC
	callq	memset
	xorl	%esi, %esi
	movl	$508, %edx              # imm = 0x1FC
	movq	%rbx, %rdi
	callq	memset
	movq	$64, 528(%rsp,%rbp)
	movl	$0, 536(%rsp,%rbp)
	movl	$512, %edi              # imm = 0x200
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, 544(%rsp,%rbp)
	movl	$512, %edi              # imm = 0x200
	callq	malloc
	movq	%rax, 552(%rsp,%rbp)
	movl	$1065353216, (%rbx)     # imm = 0x3F800000
	leaq	4(%rbx), %rdi
	leaq	4(%rax), %rbx
	xorl	%esi, %esi
	movl	$508, %edx              # imm = 0x1FC
	callq	memset
	xorl	%esi, %esi
	movl	$508, %edx              # imm = 0x1FC
	movq	%rbx, %rdi
	callq	memset
	addq	$32, %rbp
	cmpq	$384, %rbp              # imm = 0x180
	jne	.LBB2_1
# BB#2:                                 # %.preheader109.preheader
	leaq	400(%rsp), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_3:                                # %.preheader109
                                        # =>This Inner Loop Header: Depth=1
	movq	$512, (%r14)            # imm = 0x200
	movl	$0, 8(%r14)
	movl	$4096, %edi             # imm = 0x1000
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, 16(%r14)
	movl	$4096, %edi             # imm = 0x1000
	callq	malloc
	movq	%rax, 24(%r14)
	movl	$1065353216, (%rbx)     # imm = 0x3F800000
	leaq	4(%rbx), %rdi
	leaq	4(%rax), %rbx
	xorl	%esi, %esi
	movl	$4092, %edx             # imm = 0xFFC
	callq	memset
	xorl	%esi, %esi
	movl	$4092, %edx             # imm = 0xFFC
	movq	%rbx, %rdi
	callq	memset
	testq	%rbp, %rbp
	movss	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	je	.LBB2_5
# BB#4:                                 # %.preheader109
                                        #   in Loop: Header=BB2_3 Depth=1
	xorps	%xmm0, %xmm0
.LBB2_5:                                # %.preheader109
                                        #   in Loop: Header=BB2_3 Depth=1
	movss	%xmm0, 16(%rsp,%rbp)
	movl	$0, 20(%rsp,%rbp)
	cmpq	$96, %rbp
	movaps	%xmm1, %xmm0
	je	.LBB2_7
# BB#6:                                 # %.preheader109
                                        #   in Loop: Header=BB2_3 Depth=1
	xorps	%xmm0, %xmm0
.LBB2_7:                                # %.preheader109
                                        #   in Loop: Header=BB2_3 Depth=1
	movss	%xmm0, 24(%rsp,%rbp)
	movl	$0, 28(%rsp,%rbp)
	cmpq	$192, %rbp
	movaps	%xmm1, %xmm0
	je	.LBB2_9
# BB#8:                                 # %.preheader109
                                        #   in Loop: Header=BB2_3 Depth=1
	xorps	%xmm0, %xmm0
.LBB2_9:                                # %.preheader109
                                        #   in Loop: Header=BB2_3 Depth=1
	movss	%xmm0, 32(%rsp,%rbp)
	movl	$0, 36(%rsp,%rbp)
	cmpq	$288, %rbp              # imm = 0x120
	movaps	%xmm1, %xmm0
	je	.LBB2_11
# BB#10:                                # %.preheader109
                                        #   in Loop: Header=BB2_3 Depth=1
	xorps	%xmm0, %xmm0
.LBB2_11:                               # %.preheader109
                                        #   in Loop: Header=BB2_3 Depth=1
	movss	%xmm0, 40(%rsp,%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 92(%rsp,%rbp)
	movups	%xmm0, 76(%rsp,%rbp)
	movups	%xmm0, 60(%rsp,%rbp)
	movups	%xmm0, 44(%rsp,%rbp)
	movl	$0, 108(%rsp,%rbp)
	addq	$96, %rbp
	addq	$32, %r14
	cmpq	$384, %rbp              # imm = 0x180
	jne	.LBB2_3
	jmp	.LBB2_12
	.p2align	4, 0x90
.LBB2_14:                               # %.preheader104.preheader
                                        #   in Loop: Header=BB2_12 Depth=1
	leaq	265488(%rsp), %r13
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB2_15:                               # %.preheader104
                                        #   Parent Loop BB2_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_16 Depth 3
                                        #         Child Loop BB2_17 Depth 4
                                        #       Child Loop BB2_50 Depth 3
                                        #         Child Loop BB2_51 Depth 4
                                        #       Child Loop BB2_22 Depth 3
                                        #       Child Loop BB2_24 Depth 3
	xorl	%r15d, %r15d
	cmpq	$1, %r14
	jne	.LBB2_16
	.p2align	4, 0x90
.LBB2_50:                               # %.preheader104.split.us
                                        #   Parent Loop BB2_12 Depth=1
                                        #     Parent Loop BB2_15 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_51 Depth 4
	movq	%r15, %rax
	shlq	$13, %rax
	leaq	68880(%rsp,%rax), %rbx
	xorl	%r12d, %r12d
	jmp	.LBB2_51
	.p2align	4, 0x90
.LBB2_58:                               # %.thread.us.split
                                        #   in Loop: Header=BB2_51 Depth=4
	addsd	.LCPI2_4(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 2052(%rbx)
	movl	$257, %r12d             # imm = 0x101
.LBB2_51:                               # %.lr.ph.split.us.i.us
                                        #   Parent Loop BB2_12 Depth=1
                                        #     Parent Loop BB2_15 Depth=2
                                        #       Parent Loop BB2_50 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r12d, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB2_53
# BB#52:                                # %call.sqrt
                                        #   in Loop: Header=BB2_51 Depth=4
	movapd	%xmm1, %xmm0
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	sqrt
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
.LBB2_53:                               # %.lr.ph.split.us.i.us.split
                                        #   in Loop: Header=BB2_51 Depth=4
	cmpq	$256, %r12              # imm = 0x100
	cvtsd2ss	%xmm0, %xmm0
	jne	.LBB2_54
# BB#56:                                # %.thread.us
                                        #   in Loop: Header=BB2_51 Depth=4
	movss	%xmm0, 2048(%rbx)
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB2_58
# BB#57:                                # %call.sqrt323
                                        #   in Loop: Header=BB2_51 Depth=4
	movapd	%xmm1, %xmm0
	callq	sqrt
	jmp	.LBB2_58
	.p2align	4, 0x90
.LBB2_54:                               #   in Loop: Header=BB2_51 Depth=4
	movaps	%xmm0, %xmm1
	movaps	.LCPI2_1(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm1
	leal	(%r12,%r12), %eax
	cltq
	movss	%xmm1, (%rbx,%rax,4)
	addss	.LCPI2_0(%rip), %xmm0
	xorps	%xmm2, %xmm0
	leal	1(%r12,%r12), %eax
	cltq
	movss	%xmm0, (%rbx,%rax,4)
	incq	%r12
	cmpq	$1024, %r12             # imm = 0x400
	jne	.LBB2_51
# BB#55:                                # %InputGenerate.exit.loopexit.us
                                        #   in Loop: Header=BB2_50 Depth=3
	incq	%r15
	cmpq	$12, %r15
	jne	.LBB2_50
	jmp	.LBB2_21
	.p2align	4, 0x90
.LBB2_16:                               # %.preheader104.split
                                        #   Parent Loop BB2_12 Depth=1
                                        #     Parent Loop BB2_15 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_17 Depth 4
	movq	%r15, %rax
	shlq	$13, %rax
	leaq	68880(%rsp,%rax), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_17:                               # %.lr.ph.split.i
                                        #   Parent Loop BB2_12 Depth=1
                                        #     Parent Loop BB2_15 Depth=2
                                        #       Parent Loop BB2_16 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%ebp, %eax
	imull	%r14d, %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB2_19
# BB#18:                                # %call.sqrt324
                                        #   in Loop: Header=BB2_17 Depth=4
	movapd	%xmm1, %xmm0
	callq	sqrt
.LBB2_19:                               # %.lr.ph.split.i.split
                                        #   in Loop: Header=BB2_17 Depth=4
	cvtsd2ss	%xmm0, %xmm0
	movaps	%xmm0, %xmm1
	movaps	.LCPI2_1(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm1
	movq	%rbp, %rax
	shlq	$33, %rax
	sarq	$30, %rax
	movss	%xmm1, (%rbx,%rax)
	addss	.LCPI2_0(%rip), %xmm0
	xorps	%xmm2, %xmm0
	leal	1(%rbp,%rbp), %eax
	cltq
	movss	%xmm0, (%rbx,%rax,4)
	incq	%rbp
	cmpq	$1024, %rbp             # imm = 0x400
	jne	.LBB2_17
# BB#20:                                # %InputGenerate.exit
                                        #   in Loop: Header=BB2_16 Depth=3
	incq	%r15
	cmpq	$12, %r15
	jne	.LBB2_16
.LBB2_21:                               # %.preheader103
                                        #   in Loop: Header=BB2_15 Depth=2
	movq	%r14, %r15
	shlq	$5, %r15
	leaq	912(%rsp,%r15), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_22:                               #   Parent Loop BB2_12 Depth=1
                                        #     Parent Loop BB2_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	68880(%rsp,%rbp), %rcx
	leaq	167184(%rsp,%rbp), %r8
	movl	$1024, %esi             # imm = 0x400
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	BeamFirFilter
	addq	$8, %rbp
	cmpq	$8192, %rbp             # imm = 0x2000
	jne	.LBB2_22
# BB#23:                                # %.preheader102
                                        #   in Loop: Header=BB2_15 Depth=2
	leaq	528(%rsp,%r15), %r15
	xorl	%ebp, %ebp
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB2_24:                               #   Parent Loop BB2_12 Depth=1
                                        #     Parent Loop BB2_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	167184(%rsp,%rbp), %rcx
	movl	$1024, %esi             # imm = 0x400
	movl	$2, %edx
	movq	%r15, %rdi
	movq	%rbx, %r8
	callq	BeamFirFilter
	addq	$8, %rbx
	addq	$16, %rbp
	cmpq	$8192, %rbp             # imm = 0x2000
	jne	.LBB2_24
# BB#25:                                # %.critedge
                                        #   in Loop: Header=BB2_15 Depth=2
	incq	%r14
	addq	$49152, %r13            # imm = 0xC000
	cmpq	$12, %r14
	jne	.LBB2_15
# BB#26:                                # %.preheader101.preheader
                                        #   in Loop: Header=BB2_12 Depth=1
	leaq	265500(%rsp), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_27:                               # %.preheader101
                                        #   Parent Loop BB2_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_28 Depth 3
	leaq	(%rcx,%rcx), %rdx
	movl	$100, %esi
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB2_28:                               #   Parent Loop BB2_12 Depth=1
                                        #     Parent Loop BB2_27 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-12(%rdi), %ebp
	leaq	19728(%rsp,%rsi), %rbx
	movl	%ebp, -100(%rbx,%rdx,4)
	movl	-8(%rdi), %ebp
	movl	%ebp, -96(%rbx,%rdx,4)
	movl	-4(%rdi), %ebp
	movl	%ebp, -4(%rbx,%rdx,4)
	movl	(%rdi), %ebp
	movl	%ebp, (%rbx,%rdx,4)
	addq	$16, %rdi
	addq	$192, %rsi
	cmpq	$49252, %rsi            # imm = 0xC064
	jne	.LBB2_28
# BB#29:                                #   in Loop: Header=BB2_27 Depth=2
	incq	%rcx
	addq	$49152, %rax            # imm = 0xC000
	cmpq	$12, %rcx
	jne	.LBB2_27
# BB#30:                                # %.preheader100.preheader
                                        #   in Loop: Header=BB2_12 Depth=1
	leaq	16(%rsp), %r14
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_31:                               # %.preheader100
                                        #   Parent Loop BB2_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_32 Depth 3
                                        #         Child Loop BB2_33 Depth 4
                                        #       Child Loop BB2_36 Depth 3
                                        #       Child Loop BB2_38 Depth 3
	movq	%r13, %rax
	shlq	$11, %rax
	leaq	11536(%rsp,%rax), %r15
	leaq	19728(%rsp), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_32:                               #   Parent Loop BB2_12 Depth=1
                                        #     Parent Loop BB2_31 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_33 Depth 4
	leaq	(%rcx,%rcx), %rdx
	xorps	%xmm0, %xmm0
	movq	%rax, %rsi
	movq	%r14, %rdi
	movl	$12, %ebx
	xorps	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB2_33:                               #   Parent Loop BB2_12 Depth=1
                                        #     Parent Loop BB2_31 Depth=2
                                        #       Parent Loop BB2_32 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%rdi), %xmm2           # xmm2 = mem[0],zero
	movq	(%rsi), %xmm3           # xmm3 = mem[0],zero
	pshufd	$229, %xmm3, %xmm4      # xmm4 = xmm3[1,1,2,3]
	mulss	%xmm2, %xmm4
	pshufd	$229, %xmm2, %xmm5      # xmm5 = xmm2[1,1,2,3]
	mulps	%xmm3, %xmm2
	movaps	%xmm2, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	subss	%xmm6, %xmm2
	addss	%xmm2, %xmm1
	mulss	%xmm3, %xmm5
	addss	%xmm4, %xmm5
	addss	%xmm5, %xmm0
	addq	$8, %rdi
	addq	$8, %rsi
	decq	%rbx
	jne	.LBB2_33
# BB#34:                                # %BeamForm.exit
                                        #   in Loop: Header=BB2_32 Depth=3
	movss	%xmm1, 7440(%rsp,%rdx,4)
	movss	%xmm0, 7444(%rsp,%rdx,4)
	incq	%rcx
	addq	$96, %rax
	cmpq	$512, %rcx              # imm = 0x200
	jne	.LBB2_32
# BB#35:                                # %.preheader99
                                        #   in Loop: Header=BB2_31 Depth=2
	movq	%r13, %rax
	shlq	$5, %rax
	leaq	400(%rsp,%rax), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_36:                               #   Parent Loop BB2_12 Depth=1
                                        #     Parent Loop BB2_31 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	7440(%rsp,%rbp), %rcx
	leaq	3344(%rsp,%rbp), %r8
	movl	$512, %esi              # imm = 0x200
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	BeamFirFilter
	addq	$8, %rbp
	cmpq	$4096, %rbp             # imm = 0x1000
	jne	.LBB2_36
# BB#37:                                # %.lr.ph.i92.preheader
                                        #   in Loop: Header=BB2_31 Depth=2
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB2_38:                               # %.lr.ph.i92
                                        #   Parent Loop BB2_12 Depth=1
                                        #     Parent Loop BB2_31 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	3336(%rsp,%rbx,8), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	movss	3340(%rsp,%rbx,8), %xmm0 # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB2_40
# BB#39:                                # %call.sqrt325
                                        #   in Loop: Header=BB2_38 Depth=3
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB2_40:                               # %.lr.ph.i92.split
                                        #   in Loop: Header=BB2_38 Depth=3
	movss	%xmm1, 1292(%rsp,%rbx,4)
	movss	3344(%rsp,%rbx,8), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	movss	3348(%rsp,%rbx,8), %xmm0 # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB2_42
# BB#41:                                # %call.sqrt326
                                        #   in Loop: Header=BB2_38 Depth=3
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB2_42:                               # %.lr.ph.i92.split.split
                                        #   in Loop: Header=BB2_38 Depth=3
	movss	%xmm1, 1296(%rsp,%rbx,4)
	addq	$2, %rbx
	cmpq	$513, %rbx              # imm = 0x201
	jne	.LBB2_38
# BB#43:                                # %Magnitude.exit
                                        #   in Loop: Header=BB2_31 Depth=2
	movl	$2048, %edx             # imm = 0x800
	movq	%r15, %rdi
	leaq	1296(%rsp), %rsi
	callq	memcpy
	incq	%r13
	addq	$96, %r14
	cmpq	$4, %r13
	jne	.LBB2_31
# BB#44:                                # %.preheader.preheader
                                        #   in Loop: Header=BB2_12 Depth=1
	xorl	%ebx, %ebx
	movaps	.LCPI2_2(%rip), %xmm3   # xmm3 = [nan,nan,nan,nan]
	.p2align	4, 0x90
.LBB2_45:                               # %.preheader
                                        #   Parent Loop BB2_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	11536(%rsp,%rbx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movss	detector_out_StrictFP(,%rbx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	subss	%xmm1, %xmm2
	andps	%xmm3, %xmm2
	cvtss2sd	%xmm2, %xmm2
	ucomisd	.LCPI2_3(%rip), %xmm2
	ja	.LBB2_59
# BB#46:                                # %check_FP.exit
                                        #   in Loop: Header=BB2_45 Depth=2
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	movl	$.L.str.1, %edi
	movb	$1, %al
	callq	printf
	movss	13584(%rsp,%rbx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movss	detector_out_StrictFP+2048(,%rbx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	subss	%xmm1, %xmm2
	andps	.LCPI2_2(%rip), %xmm2
	cvtss2sd	%xmm2, %xmm2
	ucomisd	.LCPI2_3(%rip), %xmm2
	ja	.LBB2_59
# BB#47:                                # %check_FP.exit.1
                                        #   in Loop: Header=BB2_45 Depth=2
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	movl	$.L.str.1, %edi
	movb	$1, %al
	callq	printf
	movss	15632(%rsp,%rbx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movss	detector_out_StrictFP+4096(,%rbx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	subss	%xmm1, %xmm2
	andps	.LCPI2_2(%rip), %xmm2
	cvtss2sd	%xmm2, %xmm2
	ucomisd	.LCPI2_3(%rip), %xmm2
	ja	.LBB2_59
# BB#48:                                # %check_FP.exit.2
                                        #   in Loop: Header=BB2_45 Depth=2
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	movl	$.L.str.1, %edi
	movb	$1, %al
	callq	printf
	movss	17680(%rsp,%rbx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movss	detector_out_StrictFP+6144(,%rbx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	subss	%xmm1, %xmm2
	andps	.LCPI2_2(%rip), %xmm2
	cvtss2sd	%xmm2, %xmm2
	ucomisd	.LCPI2_3(%rip), %xmm2
	ja	.LBB2_59
# BB#49:                                # %check_FP.exit.3
                                        #   in Loop: Header=BB2_45 Depth=2
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	movl	$.L.str.1, %edi
	movb	$1, %al
	callq	printf
	movaps	.LCPI2_2(%rip), %xmm3   # xmm3 = [nan,nan,nan,nan]
	incq	%rbx
	cmpq	$512, %rbx              # imm = 0x200
	jl	.LBB2_45
.LBB2_12:                               # %.preheader108
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_15 Depth 2
                                        #       Child Loop BB2_16 Depth 3
                                        #         Child Loop BB2_17 Depth 4
                                        #       Child Loop BB2_50 Depth 3
                                        #         Child Loop BB2_51 Depth 4
                                        #       Child Loop BB2_22 Depth 3
                                        #       Child Loop BB2_24 Depth 3
                                        #     Child Loop BB2_27 Depth 2
                                        #       Child Loop BB2_28 Depth 3
                                        #     Child Loop BB2_31 Depth 2
                                        #       Child Loop BB2_32 Depth 3
                                        #         Child Loop BB2_33 Depth 4
                                        #       Child Loop BB2_36 Depth 3
                                        #       Child Loop BB2_38 Depth 3
                                        #     Child Loop BB2_45 Depth 2
	movl	numiters(%rip), %eax
	cmpl	$-1, %eax
	je	.LBB2_14
# BB#13:                                #   in Loop: Header=BB2_12 Depth=1
	leal	-1(%rax), %ecx
	movl	%ecx, numiters(%rip)
	testl	%eax, %eax
	jg	.LBB2_14
# BB#60:
	addq	$855320, %rsp           # imm = 0xD0D18
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_59:
	movq	stderr(%rip), %rdi
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movsd	.LCPI2_3(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end2:
	.size	begin, .Lfunc_end2-begin
	.cfi_endproc

	.globl	BeamFirSetup
	.p2align	4, 0x90
	.type	BeamFirSetup,@function
BeamFirSetup:                           # @BeamFirSetup
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi35:
	.cfi_def_cfa_offset 48
.Lcfi36:
	.cfi_offset %rbx, -40
.Lcfi37:
	.cfi_offset %r12, -32
.Lcfi38:
	.cfi_offset %r14, -24
.Lcfi39:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	%esi, (%r14)
	movl	$0, 4(%r14)
	movl	$0, 8(%r14)
	movslq	%esi, %r12
	leaq	(,%r12,8), %r15
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, 16(%r14)
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, 24(%r14)
	movq	$1065353216, (%rbx)     # imm = 0x3F800000
	addl	%r12d, %r12d
	cmpl	$2, %r12d
	jl	.LBB3_14
# BB#1:                                 # %.lr.ph.preheader
	movl	%r12d, %ecx
	leaq	-1(%rcx), %rsi
	cmpq	$8, %rsi
	jae	.LBB3_3
# BB#2:
	movl	$1, %r9d
	jmp	.LBB3_12
.LBB3_3:                                # %min.iters.checked
	movq	%rsi, %r9
	andq	$-8, %r9
	andq	$-8, %rsi
	je	.LBB3_4
# BB#5:                                 # %vector.body.preheader
	leaq	-8(%rsi), %r8
	movl	%r8d, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB3_6
# BB#7:                                 # %vector.body.prol.preheader
	negq	%rdx
	xorl	%edi, %edi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB3_8:                                # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, 4(%rbx,%rdi,4)
	movups	%xmm0, 20(%rbx,%rdi,4)
	movups	%xmm0, 4(%rax,%rdi,4)
	movups	%xmm0, 20(%rax,%rdi,4)
	addq	$8, %rdi
	incq	%rdx
	jne	.LBB3_8
	jmp	.LBB3_9
.LBB3_4:
	movl	$1, %r9d
	jmp	.LBB3_12
.LBB3_6:
	xorl	%edi, %edi
.LBB3_9:                                # %vector.body.prol.loopexit
	orq	$1, %r9
	cmpq	$24, %r8
	jb	.LBB3_12
# BB#10:                                # %vector.body.preheader.new
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB3_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	leaq	(,%rdi,4), %rdx
	orq	$4, %rdx
	movups	%xmm0, (%rbx,%rdx)
	movups	%xmm0, 16(%rbx,%rdx)
	movups	%xmm0, (%rax,%rdx)
	movups	%xmm0, 16(%rax,%rdx)
	leaq	8(%rdi), %rdx
	orq	$1, %rdx
	movups	%xmm0, (%rbx,%rdx,4)
	movups	%xmm0, 16(%rbx,%rdx,4)
	movups	%xmm0, (%rax,%rdx,4)
	movups	%xmm0, 16(%rax,%rdx,4)
	leaq	16(%rdi), %rdx
	orq	$1, %rdx
	movups	%xmm0, (%rbx,%rdx,4)
	movups	%xmm0, 16(%rbx,%rdx,4)
	movups	%xmm0, (%rax,%rdx,4)
	movups	%xmm0, 16(%rax,%rdx,4)
	leaq	24(%rdi), %rdx
	orq	$1, %rdx
	movups	%xmm0, (%rbx,%rdx,4)
	movups	%xmm0, 16(%rbx,%rdx,4)
	movups	%xmm0, (%rax,%rdx,4)
	movups	%xmm0, 16(%rax,%rdx,4)
	addq	$32, %rdi
	cmpq	%rsi, %rdi
	jne	.LBB3_11
.LBB3_12:                               # %.lr.ph.preheader23
	leaq	(%rbx,%r9,4), %rdx
	leaq	(%rax,%r9,4), %rax
	subq	%r9, %rcx
	.p2align	4, 0x90
.LBB3_13:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%rdx)
	movl	$0, (%rax)
	addq	$4, %rdx
	addq	$4, %rax
	decq	%rcx
	jne	.LBB3_13
.LBB3_14:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	BeamFirSetup, .Lfunc_end3-BeamFirSetup
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1065353216              # float 1
	.text
	.globl	BeamFormWeights
	.p2align	4, 0x90
	.type	BeamFormWeights,@function
BeamFormWeights:                        # @BeamFormWeights
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	movss	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	je	.LBB4_2
# BB#1:
	xorps	%xmm1, %xmm1
.LBB4_2:
	movss	%xmm1, (%rsi)
	movl	$0, 4(%rsi)
	cmpl	$1, %edi
	movaps	%xmm0, %xmm1
	je	.LBB4_4
# BB#3:
	xorps	%xmm1, %xmm1
.LBB4_4:
	movss	%xmm1, 8(%rsi)
	movl	$0, 12(%rsi)
	cmpl	$2, %edi
	movaps	%xmm0, %xmm1
	je	.LBB4_6
# BB#5:
	xorps	%xmm1, %xmm1
.LBB4_6:
	movss	%xmm1, 16(%rsi)
	movl	$0, 20(%rsi)
	cmpl	$3, %edi
	movaps	%xmm0, %xmm1
	je	.LBB4_8
# BB#7:
	xorps	%xmm1, %xmm1
.LBB4_8:
	movss	%xmm1, 24(%rsi)
	movl	$0, 28(%rsi)
	cmpl	$4, %edi
	movaps	%xmm0, %xmm1
	je	.LBB4_10
# BB#9:
	xorps	%xmm1, %xmm1
.LBB4_10:
	movss	%xmm1, 32(%rsi)
	movl	$0, 36(%rsi)
	cmpl	$5, %edi
	movaps	%xmm0, %xmm1
	je	.LBB4_12
# BB#11:
	xorps	%xmm1, %xmm1
.LBB4_12:
	movss	%xmm1, 40(%rsi)
	movl	$0, 44(%rsi)
	cmpl	$6, %edi
	movaps	%xmm0, %xmm1
	je	.LBB4_14
# BB#13:
	xorps	%xmm1, %xmm1
.LBB4_14:
	movss	%xmm1, 48(%rsi)
	movl	$0, 52(%rsi)
	cmpl	$7, %edi
	movaps	%xmm0, %xmm1
	je	.LBB4_16
# BB#15:
	xorps	%xmm1, %xmm1
.LBB4_16:
	movss	%xmm1, 56(%rsi)
	movl	$0, 60(%rsi)
	cmpl	$8, %edi
	movaps	%xmm0, %xmm1
	je	.LBB4_18
# BB#17:
	xorps	%xmm1, %xmm1
.LBB4_18:
	movss	%xmm1, 64(%rsi)
	movl	$0, 68(%rsi)
	cmpl	$9, %edi
	movaps	%xmm0, %xmm1
	je	.LBB4_20
# BB#19:
	xorps	%xmm1, %xmm1
.LBB4_20:
	movss	%xmm1, 72(%rsi)
	movl	$0, 76(%rsi)
	cmpl	$10, %edi
	movaps	%xmm0, %xmm1
	je	.LBB4_22
# BB#21:
	xorps	%xmm1, %xmm1
.LBB4_22:
	movss	%xmm1, 80(%rsi)
	movl	$0, 84(%rsi)
	cmpl	$11, %edi
	je	.LBB4_24
# BB#23:
	xorps	%xmm0, %xmm0
.LBB4_24:
	movss	%xmm0, 88(%rsi)
	movl	$0, 92(%rsi)
	retq
.Lfunc_end4:
	.size	BeamFormWeights, .Lfunc_end4-BeamFormWeights
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_1:
	.long	1065353216              # float 1
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_2:
	.quad	4607182418800017408     # double 1
	.text
	.globl	InputGenerate
	.p2align	4, 0x90
	.type	InputGenerate,@function
InputGenerate:                          # @InputGenerate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 80
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	testl	%edx, %edx
	jle	.LBB5_15
# BB#1:                                 # %.lr.ph
	cmpl	$1, %edi
	jne	.LBB5_11
# BB#2:                                 # %.lr.ph.split.us.preheader
	movl	%edx, %r14d
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB5_5
# BB#4:                                 # %call.sqrt
                                        #   in Loop: Header=BB5_3 Depth=1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
.LBB5_5:                                # %.lr.ph.split.us.split
                                        #   in Loop: Header=BB5_3 Depth=1
	cmpq	$256, %rbx              # imm = 0x100
	cvtsd2ss	%xmm0, %xmm0
	jne	.LBB5_6
# BB#7:                                 #   in Loop: Header=BB5_3 Depth=1
	movss	%xmm0, 2048(%r15)
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB5_9
# BB#8:                                 # %call.sqrt45
                                        #   in Loop: Header=BB5_3 Depth=1
	movapd	%xmm1, %xmm0
	callq	sqrt
.LBB5_9:                                # %.split
                                        #   in Loop: Header=BB5_3 Depth=1
	addsd	.LCPI5_2(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movl	$512, %eax              # imm = 0x200
	jmp	.LBB5_10
	.p2align	4, 0x90
.LBB5_6:                                #   in Loop: Header=BB5_3 Depth=1
	movaps	%xmm0, %xmm1
	movaps	.LCPI5_0(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm1
	movslq	%ebp, %rax
	movss	%xmm1, (%r15,%rax,4)
	addss	.LCPI5_1(%rip), %xmm0
	xorps	%xmm2, %xmm0
	movl	%ebp, %eax
.LBB5_10:                               #   in Loop: Header=BB5_3 Depth=1
	orl	$1, %eax
	cltq
	movss	%xmm0, (%r15,%rax,4)
	incq	%rbx
	addl	$2, %ebp
	cmpq	%rbx, %r14
	jne	.LBB5_3
	jmp	.LBB5_15
.LBB5_11:                               # %.lr.ph.split.preheader
	movl	%edx, %r12d
	movl	$1, %ebp
	xorl	%ebx, %ebx
	movaps	.LCPI5_0(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movss	.LCPI5_1(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movabsq	$8589934592, %r14       # imm = 0x200000000
	xorl	%r13d, %r13d
	movq	%r15, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB5_12:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB5_14
# BB#13:                                # %call.sqrt46
                                        #   in Loop: Header=BB5_12 Depth=1
	movapd	%xmm1, %xmm0
	movl	%edi, %r15d
	callq	sqrt
	movl	%r15d, %edi
	movq	8(%rsp), %r15           # 8-byte Reload
	movss	.LCPI5_1(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movaps	.LCPI5_0(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
.LBB5_14:                               # %.lr.ph.split.split
                                        #   in Loop: Header=BB5_12 Depth=1
	cvtsd2ss	%xmm0, %xmm0
	movaps	%xmm0, %xmm1
	xorps	%xmm2, %xmm1
	movq	%r13, %rax
	sarq	$30, %rax
	movss	%xmm1, (%r15,%rax)
	addss	%xmm3, %xmm0
	xorps	%xmm2, %xmm0
	movslq	%ebp, %rbp
	movss	%xmm0, (%r15,%rbp,4)
	addl	$2, %ebp
	addq	%r14, %r13
	addl	%edi, %ebx
	decq	%r12
	jne	.LBB5_12
.LBB5_15:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	InputGenerate, .Lfunc_end5-InputGenerate
	.cfi_endproc

	.globl	BeamFirFilter
	.p2align	4, 0x90
	.type	BeamFirFilter,@function
BeamFirFilter:                          # @BeamFirFilter
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi59:
	.cfi_def_cfa_offset 64
.Lcfi60:
	.cfi_offset %rbx, -56
.Lcfi61:
	.cfi_offset %r12, -48
.Lcfi62:
	.cfi_offset %r13, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movq	%rdi, %r9
	movslq	(%r9), %r10
	leal	-1(%r10), %r11d
	leaq	(%r10,%r10), %r15
	movl	8(%r9), %r14d
	movl	%r11d, %ebx
	subl	%r14d, %ebx
	leal	(%rbx,%rbx), %ebp
	testq	%r10, %r10
	movl	(%rcx), %r12d
	movq	24(%r9), %rdi
	movslq	%ebp, %rax
	movl	%r12d, (%rdi,%rax,4)
	movl	4(%rcx), %eax
	leal	1(%rbx,%rbx), %ecx
	movslq	%ecx, %rcx
	movl	%eax, (%rdi,%rcx,4)
	jle	.LBB6_1
# BB#6:                                 # %.lr.ph86
	leal	-1(%r15), %r13d
	movd	%r12d, %xmm1
	movq	16(%r9), %rbx
	xorps	%xmm0, %xmm0
	movl	$2, %eax
	jmp	.LBB6_7
	.p2align	4, 0x90
.LBB6_8:                                # %._crit_edge89
                                        #   in Loop: Header=BB6_7 Depth=1
	addl	$2, %ebp
	andl	%r13d, %ebp
	movslq	%ebp, %rcx
	movss	(%rdi,%rcx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addq	$2, %rax
.LBB6_7:                                # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, %xmm2
	movslq	%ebp, %rcx
	movss	4(%rdi,%rcx,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	movss	-8(%rbx,%rax,4), %xmm4  # xmm4 = mem[0],zero,zero,zero
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movss	-4(%rbx,%rax,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm1, %xmm0
	addps	%xmm4, %xmm0
	addps	%xmm2, %xmm0
	cmpq	%r15, %rax
	jl	.LBB6_8
	jmp	.LBB6_2
.LBB6_1:
	xorps	%xmm0, %xmm0
.LBB6_2:                                # %._crit_edge
	incl	%r14d
	andl	%r11d, %r14d
	movl	%r14d, 8(%r9)
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	%xmm1, (%r8)
	movss	%xmm0, 4(%r8)
	addl	4(%r9), %edx
	movl	%edx, 4(%r9)
	cmpl	%esi, %edx
	jne	.LBB6_5
# BB#3:
	movq	$0, 4(%r9)
	testl	%r10d, %r10d
	jle	.LBB6_5
# BB#4:                                 # %.lr.ph
	testl	%r15d, %r15d
	movl	$1, %eax
	cmovgl	%r15d, %eax
	decl	%eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB6_5:                                # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	BeamFirFilter, .Lfunc_end6-BeamFirFilter
	.cfi_endproc

	.globl	BeamForm
	.p2align	4, 0x90
	.type	BeamForm,@function
BeamForm:                               # @BeamForm
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	xorl	%eax, %eax
	xorps	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	movsd	(%rsi,%rax,8), %xmm2    # xmm2 = mem[0],zero
	movq	(%rdx,%rax,8), %xmm3    # xmm3 = mem[0],zero
	pshufd	$229, %xmm3, %xmm4      # xmm4 = xmm3[1,1,2,3]
	mulss	%xmm2, %xmm4
	pshufd	$229, %xmm2, %xmm5      # xmm5 = xmm2[1,1,2,3]
	mulps	%xmm3, %xmm2
	movaps	%xmm2, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	subss	%xmm6, %xmm2
	addss	%xmm2, %xmm1
	mulss	%xmm3, %xmm5
	addss	%xmm4, %xmm5
	addss	%xmm5, %xmm0
	incq	%rax
	cmpq	$12, %rax
	jne	.LBB7_1
# BB#2:
	movss	%xmm1, (%rcx)
	movss	%xmm0, 4(%rcx)
	retq
.Lfunc_end7:
	.size	BeamForm, .Lfunc_end7-BeamForm
	.cfi_endproc

	.globl	Magnitude
	.p2align	4, 0x90
	.type	Magnitude,@function
Magnitude:                              # @Magnitude
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 48
.Lcfi71:
	.cfi_offset %rbx, -48
.Lcfi72:
	.cfi_offset %r12, -40
.Lcfi73:
	.cfi_offset %r14, -32
.Lcfi74:
	.cfi_offset %r15, -24
.Lcfi75:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %r15
	testl	%ebp, %ebp
	jle	.LBB8_13
# BB#1:                                 # %.lr.ph.preheader
	movl	%ebp, %r12d
	testb	$1, %r12b
	jne	.LBB8_3
# BB#2:
	xorl	%eax, %eax
	cmpl	$1, %ebp
	jne	.LBB8_7
	jmp	.LBB8_13
.LBB8_3:                                # %.lr.ph.prol
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB8_5
# BB#4:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB8_5:                                # %.lr.ph.prol.split
	movss	%xmm1, (%r14)
	movl	$1, %eax
	cmpl	$1, %ebp
	je	.LBB8_13
.LBB8_7:                                # %.lr.ph.preheader.new
	subq	%rax, %r12
	leaq	4(%r14,%rax,4), %rbp
	leaq	12(%r15,%rax,8), %rbx
	.p2align	4, 0x90
.LBB8_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	-12(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	-8(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB8_10
# BB#9:                                 # %call.sqrt23
                                        #   in Loop: Header=BB8_8 Depth=1
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB8_10:                               # %.lr.ph.split
                                        #   in Loop: Header=BB8_8 Depth=1
	movss	%xmm1, -4(%rbp)
	movss	-4(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB8_12
# BB#11:                                # %call.sqrt24
                                        #   in Loop: Header=BB8_8 Depth=1
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB8_12:                               # %.lr.ph.split.split
                                        #   in Loop: Header=BB8_8 Depth=1
	movss	%xmm1, (%rbp)
	addq	$8, %rbp
	addq	$16, %rbx
	addq	$-2, %r12
	jne	.LBB8_8
.LBB8_13:                               # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	Magnitude, .Lfunc_end8-Magnitude
	.cfi_endproc

	.globl	Detector
	.p2align	4, 0x90
	.type	Detector,@function
Detector:                               # @Detector
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	leaq	2048(%rsi), %rax
	cmpq	%rdx, %rax
	jbe	.LBB9_2
# BB#1:                                 # %min.iters.checked
	leaq	2048(%rdx), %rax
	cmpq	%rsi, %rax
	jbe	.LBB9_2
# BB#4:                                 # %scalar.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_5:                                # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rax,4), %ecx
	movl	%ecx, (%rdx,%rax,4)
	movl	4(%rsi,%rax,4), %ecx
	movl	%ecx, 4(%rdx,%rax,4)
	movl	8(%rsi,%rax,4), %ecx
	movl	%ecx, 8(%rdx,%rax,4)
	movl	12(%rsi,%rax,4), %ecx
	movl	%ecx, 12(%rdx,%rax,4)
	movl	16(%rsi,%rax,4), %ecx
	movl	%ecx, 16(%rdx,%rax,4)
	movl	20(%rsi,%rax,4), %ecx
	movl	%ecx, 20(%rdx,%rax,4)
	movl	24(%rsi,%rax,4), %ecx
	movl	%ecx, 24(%rdx,%rax,4)
	movl	28(%rsi,%rax,4), %ecx
	movl	%ecx, 28(%rdx,%rax,4)
	addq	$8, %rax
	cmpq	$512, %rax              # imm = 0x200
	jne	.LBB9_5
	jmp	.LBB9_6
.LBB9_2:                                # %vector.body.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_3:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rsi,%rax,4), %xmm0
	movups	16(%rsi,%rax,4), %xmm1
	movups	%xmm0, (%rdx,%rax,4)
	movups	%xmm1, 16(%rdx,%rax,4)
	movups	32(%rsi,%rax,4), %xmm0
	movups	48(%rsi,%rax,4), %xmm1
	movups	%xmm0, 32(%rdx,%rax,4)
	movups	%xmm1, 48(%rdx,%rax,4)
	movups	64(%rsi,%rax,4), %xmm0
	movups	80(%rsi,%rax,4), %xmm1
	movups	%xmm0, 64(%rdx,%rax,4)
	movups	%xmm1, 80(%rdx,%rax,4)
	movups	96(%rsi,%rax,4), %xmm0
	movups	112(%rsi,%rax,4), %xmm1
	movups	%xmm0, 96(%rdx,%rax,4)
	movups	%xmm1, 112(%rdx,%rax,4)
	addq	$32, %rax
	cmpq	$512, %rax              # imm = 0x200
	jne	.LBB9_3
.LBB9_6:                                # %middle.block
	retq
.Lfunc_end9:
	.size	Detector, .Lfunc_end9-Detector
	.cfi_endproc

	.globl	BeamFirSetup_StrictFP
	.p2align	4, 0x90
	.type	BeamFirSetup_StrictFP,@function
BeamFirSetup_StrictFP:                  # @BeamFirSetup_StrictFP
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi78:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi80:
	.cfi_def_cfa_offset 48
.Lcfi81:
	.cfi_offset %rbx, -40
.Lcfi82:
	.cfi_offset %r12, -32
.Lcfi83:
	.cfi_offset %r14, -24
.Lcfi84:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	%esi, (%r14)
	movl	$0, 4(%r14)
	movl	$0, 8(%r14)
	movslq	%esi, %r12
	leaq	(,%r12,8), %r15
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, 16(%r14)
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, 24(%r14)
	movq	$1065353216, (%rbx)     # imm = 0x3F800000
	addl	%r12d, %r12d
	cmpl	$2, %r12d
	jl	.LBB10_14
# BB#1:                                 # %.lr.ph.preheader
	movl	%r12d, %ecx
	leaq	-1(%rcx), %rsi
	cmpq	$8, %rsi
	jae	.LBB10_3
# BB#2:
	movl	$1, %r9d
	jmp	.LBB10_12
.LBB10_3:                               # %min.iters.checked
	movq	%rsi, %r9
	andq	$-8, %r9
	andq	$-8, %rsi
	je	.LBB10_4
# BB#5:                                 # %vector.body.preheader
	leaq	-8(%rsi), %r8
	movl	%r8d, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB10_6
# BB#7:                                 # %vector.body.prol.preheader
	negq	%rdx
	xorl	%edi, %edi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB10_8:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, 4(%rbx,%rdi,4)
	movups	%xmm0, 20(%rbx,%rdi,4)
	movups	%xmm0, 4(%rax,%rdi,4)
	movups	%xmm0, 20(%rax,%rdi,4)
	addq	$8, %rdi
	incq	%rdx
	jne	.LBB10_8
	jmp	.LBB10_9
.LBB10_4:
	movl	$1, %r9d
	jmp	.LBB10_12
.LBB10_6:
	xorl	%edi, %edi
.LBB10_9:                               # %vector.body.prol.loopexit
	orq	$1, %r9
	cmpq	$24, %r8
	jb	.LBB10_12
# BB#10:                                # %vector.body.preheader.new
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB10_11:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	leaq	(,%rdi,4), %rdx
	orq	$4, %rdx
	movups	%xmm0, (%rbx,%rdx)
	movups	%xmm0, 16(%rbx,%rdx)
	movups	%xmm0, (%rax,%rdx)
	movups	%xmm0, 16(%rax,%rdx)
	leaq	8(%rdi), %rdx
	orq	$1, %rdx
	movups	%xmm0, (%rbx,%rdx,4)
	movups	%xmm0, 16(%rbx,%rdx,4)
	movups	%xmm0, (%rax,%rdx,4)
	movups	%xmm0, 16(%rax,%rdx,4)
	leaq	16(%rdi), %rdx
	orq	$1, %rdx
	movups	%xmm0, (%rbx,%rdx,4)
	movups	%xmm0, 16(%rbx,%rdx,4)
	movups	%xmm0, (%rax,%rdx,4)
	movups	%xmm0, 16(%rax,%rdx,4)
	leaq	24(%rdi), %rdx
	orq	$1, %rdx
	movups	%xmm0, (%rbx,%rdx,4)
	movups	%xmm0, 16(%rbx,%rdx,4)
	movups	%xmm0, (%rax,%rdx,4)
	movups	%xmm0, 16(%rax,%rdx,4)
	addq	$32, %rdi
	cmpq	%rsi, %rdi
	jne	.LBB10_11
.LBB10_12:                              # %.lr.ph.preheader23
	leaq	(%rbx,%r9,4), %rdx
	leaq	(%rax,%r9,4), %rax
	subq	%r9, %rcx
	.p2align	4, 0x90
.LBB10_13:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%rdx)
	movl	$0, (%rax)
	addq	$4, %rdx
	addq	$4, %rax
	decq	%rcx
	jne	.LBB10_13
.LBB10_14:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	BeamFirSetup_StrictFP, .Lfunc_end10-BeamFirSetup_StrictFP
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI11_0:
	.long	1065353216              # float 1
	.text
	.globl	BeamFormWeights_StrictFP
	.p2align	4, 0x90
	.type	BeamFormWeights_StrictFP,@function
BeamFormWeights_StrictFP:               # @BeamFormWeights_StrictFP
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	movss	.LCPI11_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	je	.LBB11_2
# BB#1:
	xorps	%xmm1, %xmm1
.LBB11_2:
	movss	%xmm1, (%rsi)
	movl	$0, 4(%rsi)
	cmpl	$1, %edi
	movaps	%xmm0, %xmm1
	je	.LBB11_4
# BB#3:
	xorps	%xmm1, %xmm1
.LBB11_4:
	movss	%xmm1, 8(%rsi)
	movl	$0, 12(%rsi)
	cmpl	$2, %edi
	movaps	%xmm0, %xmm1
	je	.LBB11_6
# BB#5:
	xorps	%xmm1, %xmm1
.LBB11_6:
	movss	%xmm1, 16(%rsi)
	movl	$0, 20(%rsi)
	cmpl	$3, %edi
	movaps	%xmm0, %xmm1
	je	.LBB11_8
# BB#7:
	xorps	%xmm1, %xmm1
.LBB11_8:
	movss	%xmm1, 24(%rsi)
	movl	$0, 28(%rsi)
	cmpl	$4, %edi
	movaps	%xmm0, %xmm1
	je	.LBB11_10
# BB#9:
	xorps	%xmm1, %xmm1
.LBB11_10:
	movss	%xmm1, 32(%rsi)
	movl	$0, 36(%rsi)
	cmpl	$5, %edi
	movaps	%xmm0, %xmm1
	je	.LBB11_12
# BB#11:
	xorps	%xmm1, %xmm1
.LBB11_12:
	movss	%xmm1, 40(%rsi)
	movl	$0, 44(%rsi)
	cmpl	$6, %edi
	movaps	%xmm0, %xmm1
	je	.LBB11_14
# BB#13:
	xorps	%xmm1, %xmm1
.LBB11_14:
	movss	%xmm1, 48(%rsi)
	movl	$0, 52(%rsi)
	cmpl	$7, %edi
	movaps	%xmm0, %xmm1
	je	.LBB11_16
# BB#15:
	xorps	%xmm1, %xmm1
.LBB11_16:
	movss	%xmm1, 56(%rsi)
	movl	$0, 60(%rsi)
	cmpl	$8, %edi
	movaps	%xmm0, %xmm1
	je	.LBB11_18
# BB#17:
	xorps	%xmm1, %xmm1
.LBB11_18:
	movss	%xmm1, 64(%rsi)
	movl	$0, 68(%rsi)
	cmpl	$9, %edi
	movaps	%xmm0, %xmm1
	je	.LBB11_20
# BB#19:
	xorps	%xmm1, %xmm1
.LBB11_20:
	movss	%xmm1, 72(%rsi)
	movl	$0, 76(%rsi)
	cmpl	$10, %edi
	movaps	%xmm0, %xmm1
	je	.LBB11_22
# BB#21:
	xorps	%xmm1, %xmm1
.LBB11_22:
	movss	%xmm1, 80(%rsi)
	movl	$0, 84(%rsi)
	cmpl	$11, %edi
	je	.LBB11_24
# BB#23:
	xorps	%xmm0, %xmm0
.LBB11_24:
	movss	%xmm0, 88(%rsi)
	movl	$0, 92(%rsi)
	retq
.Lfunc_end11:
	.size	BeamFormWeights_StrictFP, .Lfunc_end11-BeamFormWeights_StrictFP
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI12_1:
	.long	1065353216              # float 1
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI12_2:
	.quad	4607182418800017408     # double 1
	.text
	.globl	InputGenerate_StrictFP
	.p2align	4, 0x90
	.type	InputGenerate_StrictFP,@function
InputGenerate_StrictFP:                 # @InputGenerate_StrictFP
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi88:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi89:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi91:
	.cfi_def_cfa_offset 80
.Lcfi92:
	.cfi_offset %rbx, -56
.Lcfi93:
	.cfi_offset %r12, -48
.Lcfi94:
	.cfi_offset %r13, -40
.Lcfi95:
	.cfi_offset %r14, -32
.Lcfi96:
	.cfi_offset %r15, -24
.Lcfi97:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	testl	%edx, %edx
	jle	.LBB12_15
# BB#1:                                 # %.lr.ph
	cmpl	$1, %edi
	jne	.LBB12_11
# BB#2:                                 # %.lr.ph.split.us.preheader
	movl	%edx, %r14d
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_3:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB12_5
# BB#4:                                 # %call.sqrt
                                        #   in Loop: Header=BB12_3 Depth=1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
.LBB12_5:                               # %.lr.ph.split.us.split
                                        #   in Loop: Header=BB12_3 Depth=1
	cmpq	$256, %rbx              # imm = 0x100
	cvtsd2ss	%xmm0, %xmm0
	jne	.LBB12_6
# BB#7:                                 #   in Loop: Header=BB12_3 Depth=1
	movss	%xmm0, 2048(%r15)
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB12_9
# BB#8:                                 # %call.sqrt45
                                        #   in Loop: Header=BB12_3 Depth=1
	movapd	%xmm1, %xmm0
	callq	sqrt
.LBB12_9:                               # %.split
                                        #   in Loop: Header=BB12_3 Depth=1
	addsd	.LCPI12_2(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movl	$512, %eax              # imm = 0x200
	jmp	.LBB12_10
	.p2align	4, 0x90
.LBB12_6:                               #   in Loop: Header=BB12_3 Depth=1
	movaps	%xmm0, %xmm1
	movaps	.LCPI12_0(%rip), %xmm2  # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm1
	movslq	%ebp, %rax
	movss	%xmm1, (%r15,%rax,4)
	addss	.LCPI12_1(%rip), %xmm0
	xorps	%xmm2, %xmm0
	movl	%ebp, %eax
.LBB12_10:                              #   in Loop: Header=BB12_3 Depth=1
	orl	$1, %eax
	cltq
	movss	%xmm0, (%r15,%rax,4)
	incq	%rbx
	addl	$2, %ebp
	cmpq	%rbx, %r14
	jne	.LBB12_3
	jmp	.LBB12_15
.LBB12_11:                              # %.lr.ph.split.preheader
	movl	%edx, %r12d
	movl	$1, %ebp
	xorl	%ebx, %ebx
	movaps	.LCPI12_0(%rip), %xmm2  # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movss	.LCPI12_1(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movabsq	$8589934592, %r14       # imm = 0x200000000
	xorl	%r13d, %r13d
	movq	%r15, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB12_12:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB12_14
# BB#13:                                # %call.sqrt46
                                        #   in Loop: Header=BB12_12 Depth=1
	movapd	%xmm1, %xmm0
	movl	%edi, %r15d
	callq	sqrt
	movl	%r15d, %edi
	movq	8(%rsp), %r15           # 8-byte Reload
	movss	.LCPI12_1(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movaps	.LCPI12_0(%rip), %xmm2  # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
.LBB12_14:                              # %.lr.ph.split.split
                                        #   in Loop: Header=BB12_12 Depth=1
	cvtsd2ss	%xmm0, %xmm0
	movaps	%xmm0, %xmm1
	xorps	%xmm2, %xmm1
	movq	%r13, %rax
	sarq	$30, %rax
	movss	%xmm1, (%r15,%rax)
	addss	%xmm3, %xmm0
	xorps	%xmm2, %xmm0
	movslq	%ebp, %rbp
	movss	%xmm0, (%r15,%rbp,4)
	addl	$2, %ebp
	addq	%r14, %r13
	addl	%edi, %ebx
	decq	%r12
	jne	.LBB12_12
.LBB12_15:                              # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	InputGenerate_StrictFP, .Lfunc_end12-InputGenerate_StrictFP
	.cfi_endproc

	.globl	BeamFirFilter_StrictFP
	.p2align	4, 0x90
	.type	BeamFirFilter_StrictFP,@function
BeamFirFilter_StrictFP:                 # @BeamFirFilter_StrictFP
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi101:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi102:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi104:
	.cfi_def_cfa_offset 64
.Lcfi105:
	.cfi_offset %rbx, -56
.Lcfi106:
	.cfi_offset %r12, -48
.Lcfi107:
	.cfi_offset %r13, -40
.Lcfi108:
	.cfi_offset %r14, -32
.Lcfi109:
	.cfi_offset %r15, -24
.Lcfi110:
	.cfi_offset %rbp, -16
	movq	%rdi, %r9
	movslq	(%r9), %r10
	leal	-1(%r10), %r11d
	leaq	(%r10,%r10), %r15
	movl	8(%r9), %r14d
	movl	%r11d, %ebx
	subl	%r14d, %ebx
	leal	(%rbx,%rbx), %ebp
	testq	%r10, %r10
	movl	(%rcx), %r12d
	movq	24(%r9), %rdi
	movslq	%ebp, %rax
	movl	%r12d, (%rdi,%rax,4)
	movl	4(%rcx), %eax
	leal	1(%rbx,%rbx), %ecx
	movslq	%ecx, %rcx
	movl	%eax, (%rdi,%rcx,4)
	jle	.LBB13_1
# BB#6:                                 # %.lr.ph86
	leal	-1(%r15), %r13d
	movd	%r12d, %xmm1
	movq	16(%r9), %rbx
	xorps	%xmm0, %xmm0
	movl	$2, %eax
	jmp	.LBB13_7
	.p2align	4, 0x90
.LBB13_8:                               # %._crit_edge89
                                        #   in Loop: Header=BB13_7 Depth=1
	addl	$2, %ebp
	andl	%r13d, %ebp
	movslq	%ebp, %rcx
	movss	(%rdi,%rcx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addq	$2, %rax
.LBB13_7:                               # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, %xmm2
	movslq	%ebp, %rcx
	movss	4(%rdi,%rcx,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	movss	-8(%rbx,%rax,4), %xmm4  # xmm4 = mem[0],zero,zero,zero
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movss	-4(%rbx,%rax,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm1, %xmm0
	addps	%xmm4, %xmm0
	addps	%xmm2, %xmm0
	cmpq	%r15, %rax
	jl	.LBB13_8
	jmp	.LBB13_2
.LBB13_1:
	xorps	%xmm0, %xmm0
.LBB13_2:                               # %._crit_edge
	incl	%r14d
	andl	%r11d, %r14d
	movl	%r14d, 8(%r9)
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	%xmm1, (%r8)
	movss	%xmm0, 4(%r8)
	addl	4(%r9), %edx
	movl	%edx, 4(%r9)
	cmpl	%esi, %edx
	jne	.LBB13_5
# BB#3:
	movq	$0, 4(%r9)
	testl	%r10d, %r10d
	jle	.LBB13_5
# BB#4:                                 # %.lr.ph
	testl	%r15d, %r15d
	movl	$1, %eax
	cmovgl	%r15d, %eax
	decl	%eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB13_5:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	BeamFirFilter_StrictFP, .Lfunc_end13-BeamFirFilter_StrictFP
	.cfi_endproc

	.globl	BeamForm_StrictFP
	.p2align	4, 0x90
	.type	BeamForm_StrictFP,@function
BeamForm_StrictFP:                      # @BeamForm_StrictFP
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	xorl	%eax, %eax
	xorps	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB14_1:                               # =>This Inner Loop Header: Depth=1
	movsd	(%rsi,%rax,8), %xmm2    # xmm2 = mem[0],zero
	movq	(%rdx,%rax,8), %xmm3    # xmm3 = mem[0],zero
	pshufd	$229, %xmm3, %xmm4      # xmm4 = xmm3[1,1,2,3]
	mulss	%xmm2, %xmm4
	pshufd	$229, %xmm2, %xmm5      # xmm5 = xmm2[1,1,2,3]
	mulps	%xmm3, %xmm2
	movaps	%xmm2, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	subss	%xmm6, %xmm2
	addss	%xmm2, %xmm1
	mulss	%xmm3, %xmm5
	addss	%xmm4, %xmm5
	addss	%xmm5, %xmm0
	incq	%rax
	cmpq	$12, %rax
	jne	.LBB14_1
# BB#2:
	movss	%xmm1, (%rcx)
	movss	%xmm0, 4(%rcx)
	retq
.Lfunc_end14:
	.size	BeamForm_StrictFP, .Lfunc_end14-BeamForm_StrictFP
	.cfi_endproc

	.globl	Magnitude_StrictFP
	.p2align	4, 0x90
	.type	Magnitude_StrictFP,@function
Magnitude_StrictFP:                     # @Magnitude_StrictFP
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi111:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi112:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi113:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi114:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 48
.Lcfi116:
	.cfi_offset %rbx, -48
.Lcfi117:
	.cfi_offset %r12, -40
.Lcfi118:
	.cfi_offset %r14, -32
.Lcfi119:
	.cfi_offset %r15, -24
.Lcfi120:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %r15
	testl	%ebp, %ebp
	jle	.LBB15_13
# BB#1:                                 # %.lr.ph.preheader
	movl	%ebp, %r12d
	testb	$1, %r12b
	jne	.LBB15_3
# BB#2:
	xorl	%eax, %eax
	cmpl	$1, %ebp
	jne	.LBB15_7
	jmp	.LBB15_13
.LBB15_3:                               # %.lr.ph.prol
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB15_5
# BB#4:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB15_5:                               # %.lr.ph.prol.split
	movss	%xmm1, (%r14)
	movl	$1, %eax
	cmpl	$1, %ebp
	je	.LBB15_13
.LBB15_7:                               # %.lr.ph.preheader.new
	subq	%rax, %r12
	leaq	4(%r14,%rax,4), %rbp
	leaq	12(%r15,%rax,8), %rbx
	.p2align	4, 0x90
.LBB15_8:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	-12(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	-8(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB15_10
# BB#9:                                 # %call.sqrt23
                                        #   in Loop: Header=BB15_8 Depth=1
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB15_10:                              # %.lr.ph.split
                                        #   in Loop: Header=BB15_8 Depth=1
	movss	%xmm1, -4(%rbp)
	movss	-4(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB15_12
# BB#11:                                # %call.sqrt24
                                        #   in Loop: Header=BB15_8 Depth=1
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB15_12:                              # %.lr.ph.split.split
                                        #   in Loop: Header=BB15_8 Depth=1
	movss	%xmm1, (%rbp)
	addq	$8, %rbp
	addq	$16, %rbx
	addq	$-2, %r12
	jne	.LBB15_8
.LBB15_13:                              # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	Magnitude_StrictFP, .Lfunc_end15-Magnitude_StrictFP
	.cfi_endproc

	.globl	Detector_StrictFP
	.p2align	4, 0x90
	.type	Detector_StrictFP,@function
Detector_StrictFP:                      # @Detector_StrictFP
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	leaq	2048(%rsi), %rax
	cmpq	%rdx, %rax
	jbe	.LBB16_2
# BB#1:                                 # %min.iters.checked
	leaq	2048(%rdx), %rax
	cmpq	%rsi, %rax
	jbe	.LBB16_2
# BB#4:                                 # %scalar.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB16_5:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rax,4), %ecx
	movl	%ecx, (%rdx,%rax,4)
	movl	4(%rsi,%rax,4), %ecx
	movl	%ecx, 4(%rdx,%rax,4)
	movl	8(%rsi,%rax,4), %ecx
	movl	%ecx, 8(%rdx,%rax,4)
	movl	12(%rsi,%rax,4), %ecx
	movl	%ecx, 12(%rdx,%rax,4)
	movl	16(%rsi,%rax,4), %ecx
	movl	%ecx, 16(%rdx,%rax,4)
	movl	20(%rsi,%rax,4), %ecx
	movl	%ecx, 20(%rdx,%rax,4)
	movl	24(%rsi,%rax,4), %ecx
	movl	%ecx, 24(%rdx,%rax,4)
	movl	28(%rsi,%rax,4), %ecx
	movl	%ecx, 28(%rdx,%rax,4)
	addq	$8, %rax
	cmpq	$512, %rax              # imm = 0x200
	jne	.LBB16_5
	jmp	.LBB16_6
.LBB16_2:                               # %vector.body.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB16_3:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rsi,%rax,4), %xmm0
	movups	16(%rsi,%rax,4), %xmm1
	movups	%xmm0, (%rdx,%rax,4)
	movups	%xmm1, 16(%rdx,%rax,4)
	movups	32(%rsi,%rax,4), %xmm0
	movups	48(%rsi,%rax,4), %xmm1
	movups	%xmm0, 32(%rdx,%rax,4)
	movups	%xmm1, 48(%rdx,%rax,4)
	movups	64(%rsi,%rax,4), %xmm0
	movups	80(%rsi,%rax,4), %xmm1
	movups	%xmm0, 64(%rdx,%rax,4)
	movups	%xmm1, 80(%rdx,%rax,4)
	movups	96(%rsi,%rax,4), %xmm0
	movups	112(%rsi,%rax,4), %xmm1
	movups	%xmm0, 96(%rdx,%rax,4)
	movups	%xmm1, 112(%rdx,%rax,4)
	addq	$32, %rax
	cmpq	$512, %rax              # imm = 0x200
	jne	.LBB16_3
.LBB16_6:                               # %middle.block
	retq
.Lfunc_end16:
	.size	Detector_StrictFP, .Lfunc_end16-Detector_StrictFP
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"i:"
	.size	.L.str, 3

	.type	numiters,@object        # @numiters
	.data
	.p2align	2
numiters:
	.long	100                     # 0x64
	.size	numiters, 4

	.type	detector_out_StrictFP,@object # @detector_out_StrictFP
	.comm	detector_out_StrictFP,8192,16
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"%f\n"
	.size	.L.str.1, 4

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A = %lf and B = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 60


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
