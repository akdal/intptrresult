	.text
	.file	"zchar.bc"
	.globl	zshow
	.p2align	4, 0x90
	.type	zshow,@function
zshow:                                  # @zshow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	movl	$-20, %ebp
	cmpl	$52, %ecx
	jne	.LBB0_7
# BB#1:
	movl	$-7, %ebp
	testb	$2, %ah
	je	.LBB0_7
# BB#2:
	movq	esp(%rip), %rax
	addq	$96, %rax
	movl	$-5, %ebp
	cmpq	estop(%rip), %rax
	ja	.LBB0_7
# BB#3:
	movl	gs_show_enum_sizeof(%rip), %esi
	movl	$1, %edi
	movl	$.L.str.12, %edx
	callq	alloc
	testq	%rax, %rax
	je	.LBB0_4
# BB#5:
	movq	esp(%rip), %rcx
	movw	$1, 16(%rcx)
	movw	$33, 24(%rcx)
	movq	$finish_show, 32(%rcx)
	movw	$37, 40(%rcx)
	movw	$0, 42(%rcx)
	movw	$0, 48(%rcx)
	movw	$32, 56(%rcx)
	leaq	64(%rcx), %rdx
	movq	%rdx, esp(%rip)
	movq	%rax, 64(%rcx)
	movw	$52, 72(%rcx)
	movzwl	gs_show_enum_sizeof(%rip), %eax
	movw	%ax, 74(%rcx)
	movq	64(%rcx), %rdi
	movq	igs(%rip), %rsi
	movq	(%rbx), %rdx
	movzwl	10(%rbx), %ecx
	callq	gs_show_n_init
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB0_6
# BB#8:
	addq	$-16, osp(%rip)
	addq	$-16, %rbx
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	show_continue           # TAILCALL
.LBB0_4:
	movl	$-25, %ebp
	jmp	.LBB0_7
.LBB0_6:
	movq	esp(%rip), %rax
	movq	(%rax), %rdi
	movl	gs_show_enum_sizeof(%rip), %edx
	movl	$1, %esi
	movl	$.L.str.13, %ecx
	callq	alloc_free
	addq	$-64, esp(%rip)
.LBB0_7:                                # %setup_show.exit.thread
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	zshow, .Lfunc_end0-zshow
	.cfi_endproc

	.globl	setup_show
	.p2align	4, 0x90
	.type	setup_show,@function
setup_show:                             # @setup_show
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movzwl	8(%rdi), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	movl	$-20, %ebp
	cmpl	$52, %ecx
	jne	.LBB1_6
# BB#1:
	movl	$-7, %ebp
	testb	$2, %ah
	je	.LBB1_6
# BB#2:
	movq	esp(%rip), %rax
	addq	$96, %rax
	movl	$-5, %ebp
	cmpq	estop(%rip), %rax
	ja	.LBB1_6
# BB#3:
	movl	gs_show_enum_sizeof(%rip), %esi
	movl	$1, %ebp
	movl	$1, %edi
	movl	$.L.str.12, %edx
	callq	alloc
	testq	%rax, %rax
	je	.LBB1_4
# BB#5:
	movq	esp(%rip), %rcx
	movw	$1, 16(%rcx)
	movw	$33, 24(%rcx)
	movq	%rbx, 32(%rcx)
	movw	$37, 40(%rcx)
	movw	$0, 42(%rcx)
	movw	$0, 48(%rcx)
	movw	$32, 56(%rcx)
	leaq	64(%rcx), %rdx
	movq	%rdx, esp(%rip)
	movq	%rax, 64(%rcx)
	movw	$52, 72(%rcx)
	movzwl	gs_show_enum_sizeof(%rip), %eax
	movw	%ax, 74(%rcx)
	jmp	.LBB1_6
.LBB1_4:
	movl	$-25, %ebp
.LBB1_6:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	setup_show, .Lfunc_end1-setup_show
	.cfi_endproc

	.globl	finish_show
	.p2align	4, 0x90
	.type	finish_show,@function
finish_show:                            # @finish_show
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	finish_show, .Lfunc_end2-finish_show
	.cfi_endproc

	.globl	show_continue
	.p2align	4, 0x90
	.type	show_continue,@function
show_continue:                          # @show_continue
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	esp(%rip), %rax
	movq	(%rax), %rbx
	movq	%rbx, %rdi
	callq	gs_show_next
	movl	%eax, %ebp
	cmpl	$2, %ebp
	je	.LBB3_4
# BB#1:
	cmpl	$1, %ebp
	je	.LBB3_7
# BB#2:
	testl	%ebp, %ebp
	jne	.LBB3_11
# BB#3:
	movq	esp(%rip), %rax
	movq	%r14, %rdi
	callq	*-32(%rax)
	movl	%eax, %ebp
	movq	esp(%rip), %rax
	movq	(%rax), %rdi
	movl	gs_show_enum_sizeof(%rip), %edx
	movl	$1, %ebx
	movl	$1, %esi
	movl	$.L.str.13, %ecx
	callq	alloc_free
	addq	$-64, esp(%rip)
	testl	%ebp, %ebp
	cmovnsl	%ebx, %ebp
	jmp	.LBB3_10
.LBB3_7:
	movq	igs(%rip), %rdi
	callq	gs_currentfont
	movq	32(%rax), %rbp
	leaq	32(%r14), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	ja	.LBB3_5
# BB#8:
	movups	(%rbp), %xmm0
	movups	%xmm0, 16(%r14)
	movq	%rbx, %rdi
	callq	gs_show_current_char
	movzbl	%al, %eax
	movq	%rax, 32(%r14)
	movw	$20, 40(%r14)
	movq	esp(%rip), %rax
	movq	$show_continue, 16(%rax)
	movw	$37, 24(%rax)
	movw	$0, 26(%rax)
	leaq	32(%rax), %rcx
	movq	%rcx, esp(%rip)
	movups	16(%rbp), %xmm0
	jmp	.LBB3_9
.LBB3_4:
	movq	esp(%rip), %rbp
	leaq	32(%r14), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB3_6
.LBB3_5:
	movq	%r14, osp(%rip)
	movl	$-16, %ebp
	jmp	.LBB3_10
.LBB3_11:
	movq	esp(%rip), %rax
	movq	(%rax), %rdi
	movl	gs_show_enum_sizeof(%rip), %edx
	movl	$1, %esi
	movl	$.L.str.13, %ecx
	callq	alloc_free
	addq	$-64, esp(%rip)
	jmp	.LBB3_10
.LBB3_6:
	movq	%rbx, %rdi
	callq	gs_kshow_previous_char
	movzbl	%al, %eax
	movq	%rax, 16(%r14)
	movw	$20, 24(%r14)
	movq	%rbx, %rdi
	callq	gs_kshow_next_char
	movzbl	%al, %eax
	movq	%rax, 32(%r14)
	movw	$20, 40(%r14)
	movq	esp(%rip), %rax
	movq	$show_continue, 16(%rax)
	movw	$37, 24(%rax)
	movw	$0, 26(%rax)
	leaq	32(%rax), %rcx
	movq	%rcx, esp(%rip)
	movups	-16(%rbp), %xmm0
.LBB3_9:
	movups	%xmm0, 32(%rax)
	movl	$1, %ebp
.LBB3_10:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	show_continue, .Lfunc_end3-show_continue
	.cfi_endproc

	.globl	zashow
	.p2align	4, 0x90
	.type	zashow,@function
zashow:                                 # @zashow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	-16(%rbx), %rdi
	movq	%rsp, %rdx
	movl	$2, %esi
	xorl	%eax, %eax
	callq	num_params
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB4_9
# BB#1:
	movzwl	8(%rbx), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	movl	$-20, %ebp
	cmpl	$52, %ecx
	jne	.LBB4_9
# BB#2:
	movl	$-7, %ebp
	testb	$2, %ah
	je	.LBB4_9
# BB#3:
	movq	esp(%rip), %rax
	addq	$96, %rax
	movl	$-5, %ebp
	cmpq	estop(%rip), %rax
	ja	.LBB4_9
# BB#4:
	movl	gs_show_enum_sizeof(%rip), %esi
	movl	$1, %edi
	movl	$.L.str.12, %edx
	callq	alloc
	testq	%rax, %rax
	je	.LBB4_5
# BB#6:
	movq	esp(%rip), %rcx
	movw	$1, 16(%rcx)
	movw	$33, 24(%rcx)
	movq	$finish_show, 32(%rcx)
	movw	$37, 40(%rcx)
	movw	$0, 42(%rcx)
	movw	$0, 48(%rcx)
	movw	$32, 56(%rcx)
	leaq	64(%rcx), %rdx
	movq	%rdx, esp(%rip)
	movq	%rax, 64(%rcx)
	movw	$52, 72(%rcx)
	movzwl	gs_show_enum_sizeof(%rip), %eax
	movw	%ax, 74(%rcx)
	movq	64(%rcx), %rdi
	movq	igs(%rip), %rsi
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movq	(%rbx), %rdx
	movzwl	10(%rbx), %ecx
	callq	gs_ashow_n_init
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB4_7
# BB#8:
	addq	$-48, osp(%rip)
	addq	$-48, %rbx
	movq	%rbx, %rdi
	callq	show_continue
	movl	%eax, %ebp
	jmp	.LBB4_9
.LBB4_5:
	movl	$-25, %ebp
	jmp	.LBB4_9
.LBB4_7:
	movq	esp(%rip), %rax
	movq	(%rax), %rdi
	movl	gs_show_enum_sizeof(%rip), %edx
	movl	$1, %esi
	movl	$.L.str.13, %ecx
	callq	alloc_free
	addq	$-64, esp(%rip)
.LBB4_9:                                # %setup_show.exit.thread
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	zashow, .Lfunc_end4-zashow
	.cfi_endproc

	.globl	zwidthshow
	.p2align	4, 0x90
	.type	zwidthshow,@function
zwidthshow:                             # @zwidthshow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	-8(%rbx), %eax
	andl	$252, %eax
	movl	$-20, %ebp
	cmpl	$20, %eax
	jne	.LBB5_11
# BB#1:
	movl	$-15, %ebp
	cmpq	$255, -16(%rbx)
	ja	.LBB5_11
# BB#2:
	leaq	-32(%rbx), %rdi
	movq	%rsp, %rdx
	movl	$2, %esi
	xorl	%eax, %eax
	callq	num_params
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB5_11
# BB#3:
	movzwl	8(%rbx), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	cmpl	$52, %ecx
	movl	$-20, %ebp
	jne	.LBB5_11
# BB#4:
	movl	$-7, %ebp
	testb	$2, %ah
	je	.LBB5_11
# BB#5:
	movq	esp(%rip), %rax
	addq	$96, %rax
	movl	$-5, %ebp
	cmpq	estop(%rip), %rax
	ja	.LBB5_11
# BB#6:
	movl	gs_show_enum_sizeof(%rip), %esi
	movl	$1, %edi
	movl	$.L.str.12, %edx
	callq	alloc
	testq	%rax, %rax
	je	.LBB5_7
# BB#8:
	movq	esp(%rip), %rcx
	movw	$1, 16(%rcx)
	movw	$33, 24(%rcx)
	movq	$finish_show, 32(%rcx)
	movw	$37, 40(%rcx)
	movw	$0, 42(%rcx)
	movw	$0, 48(%rcx)
	movw	$32, 56(%rcx)
	leaq	64(%rcx), %rdx
	movq	%rdx, esp(%rip)
	movq	%rax, 64(%rcx)
	movw	$52, 72(%rcx)
	movzwl	gs_show_enum_sizeof(%rip), %eax
	movw	%ax, 74(%rcx)
	movq	64(%rcx), %rdi
	movq	igs(%rip), %rsi
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movq	(%rbx), %rcx
	movzwl	10(%rbx), %r8d
	movsbl	-16(%rbx), %edx
	callq	gs_widthshow_n_init
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB5_9
# BB#10:
	addq	$-64, osp(%rip)
	addq	$-64, %rbx
	movq	%rbx, %rdi
	callq	show_continue
	movl	%eax, %ebp
	jmp	.LBB5_11
.LBB5_7:
	movl	$-25, %ebp
	jmp	.LBB5_11
.LBB5_9:
	movq	esp(%rip), %rax
	movq	(%rax), %rdi
	movl	gs_show_enum_sizeof(%rip), %edx
	movl	$1, %esi
	movl	$.L.str.13, %ecx
	callq	alloc_free
	addq	$-64, esp(%rip)
.LBB5_11:                               # %setup_show.exit.thread
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end5:
	.size	zwidthshow, .Lfunc_end5-zwidthshow
	.cfi_endproc

	.globl	zawidthshow
	.p2align	4, 0x90
	.type	zawidthshow,@function
zawidthshow:                            # @zawidthshow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi28:
	.cfi_def_cfa_offset 48
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	-40(%rbx), %eax
	andl	$252, %eax
	movl	$-20, %ebp
	cmpl	$20, %eax
	jne	.LBB6_12
# BB#1:
	movl	$-15, %ebp
	cmpq	$255, -48(%rbx)
	ja	.LBB6_12
# BB#2:
	leaq	-64(%rbx), %rdi
	leaq	16(%rsp), %rdx
	movl	$2, %esi
	xorl	%eax, %eax
	callq	num_params
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB6_12
# BB#3:
	leaq	-16(%rbx), %rdi
	leaq	8(%rsp), %rdx
	movl	$2, %esi
	xorl	%eax, %eax
	callq	num_params
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB6_12
# BB#4:
	movzwl	8(%rbx), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	cmpl	$52, %ecx
	movl	$-20, %ebp
	jne	.LBB6_12
# BB#5:
	movl	$-7, %ebp
	testb	$2, %ah
	je	.LBB6_12
# BB#6:
	movq	esp(%rip), %rax
	addq	$96, %rax
	movl	$-5, %ebp
	cmpq	estop(%rip), %rax
	ja	.LBB6_12
# BB#7:
	movl	gs_show_enum_sizeof(%rip), %esi
	movl	$1, %edi
	movl	$.L.str.12, %edx
	callq	alloc
	testq	%rax, %rax
	je	.LBB6_8
# BB#9:
	movq	esp(%rip), %rcx
	movw	$1, 16(%rcx)
	movw	$33, 24(%rcx)
	movq	$finish_show, 32(%rcx)
	movw	$37, 40(%rcx)
	movw	$0, 42(%rcx)
	movw	$0, 48(%rcx)
	movw	$32, 56(%rcx)
	leaq	64(%rcx), %rdx
	movq	%rdx, esp(%rip)
	movq	%rax, 64(%rcx)
	movw	$52, 72(%rcx)
	movzwl	gs_show_enum_sizeof(%rip), %eax
	movw	%ax, 74(%rcx)
	movq	64(%rcx), %rdi
	movq	igs(%rip), %rsi
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	cvtss2sd	%xmm3, %xmm3
	movq	(%rbx), %rcx
	movzwl	10(%rbx), %r8d
	movsbl	-48(%rbx), %edx
	callq	gs_awidthshow_n_init
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB6_10
# BB#11:
	addq	$-96, osp(%rip)
	addq	$-96, %rbx
	movq	%rbx, %rdi
	callq	show_continue
	movl	%eax, %ebp
	jmp	.LBB6_12
.LBB6_8:
	movl	$-25, %ebp
	jmp	.LBB6_12
.LBB6_10:
	movq	esp(%rip), %rax
	movq	(%rax), %rdi
	movl	gs_show_enum_sizeof(%rip), %edx
	movl	$1, %esi
	movl	$.L.str.13, %ecx
	callq	alloc_free
	addq	$-64, esp(%rip)
.LBB6_12:                               # %setup_show.exit.thread
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end6:
	.size	zawidthshow, .Lfunc_end6-zawidthshow
	.cfi_endproc

	.globl	zkshow
	.p2align	4, 0x90
	.type	zkshow,@function
zkshow:                                 # @zkshow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movw	-8(%rbx), %ax
	movl	%eax, %ecx
	shrb	$2, %cl
	movl	$-20, %ebp
	cmpb	$10, %cl
	je	.LBB7_2
# BB#1:
	testb	%cl, %cl
	jne	.LBB7_11
.LBB7_2:
	andl	$3, %eax
	cmpl	$3, %eax
	jne	.LBB7_3
# BB#4:
	movzwl	8(%rbx), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	cmpl	$52, %ecx
	jne	.LBB7_11
# BB#5:
	testb	$2, %ah
	movl	$-7, %ebp
	je	.LBB7_11
# BB#6:
	movq	esp(%rip), %rax
	addq	$96, %rax
	movl	$-5, %ebp
	cmpq	estop(%rip), %rax
	ja	.LBB7_11
# BB#7:
	movl	gs_show_enum_sizeof(%rip), %esi
	movl	$1, %edi
	movl	$.L.str.12, %edx
	callq	alloc
	testq	%rax, %rax
	je	.LBB7_8
# BB#9:
	movq	esp(%rip), %rcx
	movw	$1, 16(%rcx)
	movw	$33, 24(%rcx)
	movq	$finish_show, 32(%rcx)
	movw	$37, 40(%rcx)
	movw	$0, 42(%rcx)
	movw	$0, 48(%rcx)
	movw	$32, 56(%rcx)
	leaq	64(%rcx), %rdx
	movq	%rdx, esp(%rip)
	movq	%rax, 64(%rcx)
	movw	$52, 72(%rcx)
	movzwl	gs_show_enum_sizeof(%rip), %eax
	movw	%ax, 74(%rcx)
	movq	64(%rcx), %rdi
	movq	igs(%rip), %rsi
	movq	(%rbx), %rdx
	movzwl	10(%rbx), %ecx
	callq	gs_kshow_n_init
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB7_10
# BB#12:
	leaq	-16(%rbx), %rax
	movq	esp(%rip), %rcx
	movups	(%rax), %xmm0
	movups	%xmm0, -16(%rcx)
	addq	$-32, osp(%rip)
	addq	$-32, %rbx
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	show_continue           # TAILCALL
.LBB7_3:
	movl	$-7, %ebp
.LBB7_11:                               # %setup_show.exit.thread
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB7_8:
	movl	$-25, %ebp
	jmp	.LBB7_11
.LBB7_10:
	movq	esp(%rip), %rax
	movq	(%rax), %rdi
	movl	gs_show_enum_sizeof(%rip), %edx
	movl	$1, %esi
	movl	$.L.str.13, %ecx
	callq	alloc_free
	addq	$-64, esp(%rip)
	jmp	.LBB7_11
.Lfunc_end7:
	.size	zkshow, .Lfunc_end7-zkshow
	.cfi_endproc

	.globl	zstringwidth
	.p2align	4, 0x90
	.type	zstringwidth,@function
zstringwidth:                           # @zstringwidth
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 32
.Lcfi39:
	.cfi_offset %rbx, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	movl	$-20, %ebp
	cmpl	$52, %ecx
	jne	.LBB8_7
# BB#1:
	movl	$-7, %ebp
	testb	$2, %ah
	je	.LBB8_7
# BB#2:
	movq	esp(%rip), %rax
	addq	$96, %rax
	movl	$-5, %ebp
	cmpq	estop(%rip), %rax
	ja	.LBB8_7
# BB#3:
	movl	gs_show_enum_sizeof(%rip), %esi
	movl	$1, %edi
	movl	$.L.str.12, %edx
	callq	alloc
	testq	%rax, %rax
	je	.LBB8_4
# BB#5:
	movq	esp(%rip), %rcx
	movw	$1, 16(%rcx)
	movw	$33, 24(%rcx)
	movq	$finish_stringwidth, 32(%rcx)
	movw	$37, 40(%rcx)
	movw	$0, 42(%rcx)
	movw	$0, 48(%rcx)
	movw	$32, 56(%rcx)
	leaq	64(%rcx), %rdx
	movq	%rdx, esp(%rip)
	movq	%rax, 64(%rcx)
	movw	$52, 72(%rcx)
	movzwl	gs_show_enum_sizeof(%rip), %eax
	movw	%ax, 74(%rcx)
	movq	64(%rcx), %rdi
	movq	igs(%rip), %rsi
	movq	(%rbx), %rdx
	movzwl	10(%rbx), %ecx
	callq	gs_stringwidth_n_init
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB8_6
# BB#8:
	addq	$-16, osp(%rip)
	addq	$-16, %rbx
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	show_continue           # TAILCALL
.LBB8_4:
	movl	$-25, %ebp
	jmp	.LBB8_7
.LBB8_6:
	movq	esp(%rip), %rax
	movq	(%rax), %rdi
	movl	gs_show_enum_sizeof(%rip), %edx
	movl	$1, %esi
	movl	$.L.str.13, %ecx
	callq	alloc_free
	addq	$-64, esp(%rip)
.LBB8_7:                                # %setup_show.exit.thread
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end8:
	.size	zstringwidth, .Lfunc_end8-zstringwidth
	.cfi_endproc

	.globl	finish_stringwidth
	.p2align	4, 0x90
	.type	finish_stringwidth,@function
finish_stringwidth:                     # @finish_stringwidth
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	esp(%rip), %rax
	movq	(%rax), %rdi
	leaq	8(%rsp), %rsi
	callq	gs_show_width
	leaq	32(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB9_2
# BB#1:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB9_3
.LBB9_2:
	movl	8(%rsp), %eax
	movl	%eax, 16(%rbx)
	movw	$44, 24(%rbx)
	movl	12(%rsp), %eax
	movl	%eax, 32(%rbx)
	movw	$44, 40(%rbx)
	xorl	%eax, %eax
.LBB9_3:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end9:
	.size	finish_stringwidth, .Lfunc_end9-finish_stringwidth
	.cfi_endproc

	.globl	zcharpath
	.p2align	4, 0x90
	.type	zcharpath,@function
zcharpath:                              # @zcharpath
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %eax
	andl	$252, %eax
	movl	$-20, %ebp
	cmpl	$4, %eax
	jne	.LBB10_8
# BB#1:
	movzwl	-8(%rbx), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	cmpl	$52, %ecx
	jne	.LBB10_8
# BB#2:
	movl	$-7, %ebp
	testb	$2, %ah
	je	.LBB10_8
# BB#3:
	movq	esp(%rip), %rax
	addq	$96, %rax
	movl	$-5, %ebp
	cmpq	estop(%rip), %rax
	ja	.LBB10_8
# BB#4:
	movl	gs_show_enum_sizeof(%rip), %esi
	movl	$1, %edi
	movl	$.L.str.12, %edx
	callq	alloc
	testq	%rax, %rax
	je	.LBB10_5
# BB#6:
	movq	esp(%rip), %rcx
	movw	$1, 16(%rcx)
	movw	$33, 24(%rcx)
	movq	$finish_show, 32(%rcx)
	movw	$37, 40(%rcx)
	movw	$0, 42(%rcx)
	movw	$0, 48(%rcx)
	movw	$32, 56(%rcx)
	leaq	64(%rcx), %rdx
	movq	%rdx, esp(%rip)
	movq	%rax, 64(%rcx)
	movw	$52, 72(%rcx)
	movzwl	gs_show_enum_sizeof(%rip), %eax
	movw	%ax, 74(%rcx)
	movq	64(%rcx), %rdi
	movq	igs(%rip), %rsi
	movq	-16(%rbx), %rdx
	movzwl	-6(%rbx), %ecx
	movzwl	(%rbx), %r8d
	callq	gs_charpath_n_init
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB10_7
# BB#9:
	addq	$-32, osp(%rip)
	addq	$-32, %rbx
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	show_continue           # TAILCALL
.LBB10_5:
	movl	$-25, %ebp
	jmp	.LBB10_8
.LBB10_7:
	movq	esp(%rip), %rax
	movq	(%rax), %rdi
	movl	gs_show_enum_sizeof(%rip), %edx
	movl	$1, %esi
	movl	$.L.str.13, %ecx
	callq	alloc_free
	addq	$-64, esp(%rip)
.LBB10_8:                               # %setup_show.exit.thread
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end10:
	.size	zcharpath, .Lfunc_end10-zcharpath
	.cfi_endproc

	.globl	zsetcachedevice
	.p2align	4, 0x90
	.type	zsetcachedevice,@function
zsetcachedevice:                        # @zsetcachedevice
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi52:
	.cfi_def_cfa_offset 64
.Lcfi53:
	.cfi_offset %rbx, -32
.Lcfi54:
	.cfi_offset %r14, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	esp(%rip), %rax
	movl	$estack, %ecx
	.p2align	4, 0x90
.LBB11_1:                               # =>This Inner Loop Header: Depth=1
	movzwl	8(%rax), %edx
	andl	$252, %edx
	cmpl	$32, %edx
	jne	.LBB11_3
# BB#2:                                 #   in Loop: Header=BB11_1 Depth=1
	movzwl	(%rax), %edx
	cmpl	$1, %edx
	je	.LBB11_7
.LBB11_3:                               # %.critedge.i
                                        #   in Loop: Header=BB11_1 Depth=1
	addq	$-16, %rax
	cmpq	%rcx, %rax
	jae	.LBB11_1
# BB#4:                                 # %find_show.exit.thread
	movq	%rsp, %rdx
	movl	$6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	num_params
.LBB11_5:
	movl	$-21, %eax
.LBB11_6:
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB11_7:                               # %find_show.exit
	movq	48(%rax), %r14
	movq	%rsp, %rdx
	movl	$6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	num_params
	movl	%eax, %ebp
	testq	%r14, %r14
	je	.LBB11_5
# BB#8:
	testl	%ebp, %ebp
	js	.LBB11_13
# BB#9:                                 # %._crit_edge
	movl	$6, %ebx
.LBB11_10:
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movss	12(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movss	16(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movss	20(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	movq	%r14, %rdi
	callq	gs_setcachedevice
	testl	%eax, %eax
	js	.LBB11_6
# BB#11:
	shlq	$4, %rbx
	subq	%rbx, osp(%rip)
	xorl	%eax, %eax
	jmp	.LBB11_6
.LBB11_13:
	movb	8(%rbx), %cl
	shrb	$2, %cl
	cmpb	$10, %cl
	je	.LBB11_15
# BB#14:
	movl	$-20, %eax
	testb	%cl, %cl
	jne	.LBB11_6
.LBB11_15:
	movzwl	10(%rbx), %eax
	cmpl	$4, %eax
	jne	.LBB11_20
# BB#16:
	leaq	-16(%rbx), %rdi
	movq	%rsp, %rdx
	movl	$2, %esi
	xorl	%eax, %eax
	callq	num_params
	testl	%eax, %eax
	js	.LBB11_20
# BB#17:
	movq	(%rbx), %rdi
	addq	$48, %rdi
	leaq	8(%rsp), %rdx
	movl	$4, %esi
	xorl	%eax, %eax
	callq	num_params
	testl	%eax, %eax
	movl	%ebp, %eax
	js	.LBB11_6
# BB#18:
	movl	$3, %ebx
	jmp	.LBB11_10
.LBB11_20:
	movl	%ebp, %eax
	jmp	.LBB11_6
.Lfunc_end11:
	.size	zsetcachedevice, .Lfunc_end11-zsetcachedevice
	.cfi_endproc

	.globl	zsetcharwidth
	.p2align	4, 0x90
	.type	zsetcharwidth,@function
zsetcharwidth:                          # @zsetcharwidth
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi57:
	.cfi_def_cfa_offset 32
.Lcfi58:
	.cfi_offset %rbx, -16
	movq	esp(%rip), %rax
	movl	$estack, %ecx
	.p2align	4, 0x90
.LBB12_1:                               # =>This Inner Loop Header: Depth=1
	movzwl	8(%rax), %edx
	andl	$252, %edx
	cmpl	$32, %edx
	jne	.LBB12_3
# BB#2:                                 #   in Loop: Header=BB12_1 Depth=1
	movzwl	(%rax), %edx
	cmpl	$1, %edx
	je	.LBB12_7
.LBB12_3:                               # %.critedge.i
                                        #   in Loop: Header=BB12_1 Depth=1
	addq	$-16, %rax
	cmpq	%rcx, %rax
	jae	.LBB12_1
# BB#4:                                 # %find_show.exit.thread
	leaq	8(%rsp), %rdx
	movl	$2, %esi
	xorl	%eax, %eax
	callq	num_params
.LBB12_5:
	movl	$-21, %eax
.LBB12_6:
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB12_7:                               # %find_show.exit
	movq	48(%rax), %rbx
	leaq	8(%rsp), %rdx
	movl	$2, %esi
	xorl	%eax, %eax
	callq	num_params
	testq	%rbx, %rbx
	je	.LBB12_5
# BB#8:
	testl	%eax, %eax
	js	.LBB12_6
# BB#9:
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movq	%rbx, %rdi
	callq	gs_setcharwidth
	testl	%eax, %eax
	js	.LBB12_6
# BB#10:
	addq	$-32, osp(%rip)
	xorl	%eax, %eax
	jmp	.LBB12_6
.Lfunc_end12:
	.size	zsetcharwidth, .Lfunc_end12-zsetcharwidth
	.cfi_endproc

	.globl	ztype1addpath
	.p2align	4, 0x90
	.type	ztype1addpath,@function
ztype1addpath:                          # @ztype1addpath
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi65:
	.cfi_def_cfa_offset 176
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	esp(%rip), %rax
	movl	$estack, %ecx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB13_1:                               # =>This Inner Loop Header: Depth=1
	movzwl	8(%rax), %edx
	andl	$252, %edx
	cmpl	$32, %edx
	jne	.LBB13_3
# BB#2:                                 #   in Loop: Header=BB13_1 Depth=1
	movzwl	(%rax), %edx
	cmpl	$1, %edx
	je	.LBB13_4
.LBB13_3:                               # %.critedge.i
                                        #   in Loop: Header=BB13_1 Depth=1
	addq	$-16, %rax
	cmpq	%rcx, %rax
	jae	.LBB13_1
	jmp	.LBB13_5
.LBB13_4:
	movq	48(%rax), %rbx
.LBB13_5:                               # %find_show.exit
	movq	igs(%rip), %rdi
	callq	gs_currentfont
	movq	%rax, %rbp
	testq	%rbx, %rbx
	je	.LBB13_12
# BB#6:
	movzwl	8(%r12), %eax
	andl	$252, %eax
	movl	$-20, %r15d
	cmpl	$52, %eax
	jne	.LBB13_30
# BB#7:
	movq	32(%rbp), %r14
	movups	80(%r14), %xmm0
	movups	96(%r14), %xmm1
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm0, 80(%rsp)
	movq	%r14, 8(%rsp)
	leaq	24(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	8(%rsp), %rax
	movq	%rax, 96(%rsp)
	movzwl	10(%r12), %eax
	cmpl	104(%rsp), %eax
	jle	.LBB13_29
# BB#8:
	movl	gs_type1_state_sizeof(%rip), %esi
	movl	$1, %edi
	movl	$.L.str, %edx
	callq	alloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB13_13
# BB#9:
	movq	%rbx, %rdi
	callq	gs_show_in_charpath
	movl	156(%rbp), %ecx
	movq	(%r12), %r8
	leaq	80(%rsp), %r9
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movl	%eax, %edx
	callq	gs_type1_init
	movl	%eax, %r15d
	testl	%r15d, %r15d
	js	.LBB13_14
# BB#10:                                # %.thread68.preheader
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	gs_type1_interpret
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jle	.LBB13_28
# BB#11:                                # %.lr.ph.lr.ph
	leaq	48(%r14), %r12
	leaq	112(%rsp), %rbx
                                        # implicit-def: %AL
	movb	%al, 7(%rsp)            # 1-byte Spill
	jmp	.LBB13_16
.LBB13_12:
	movl	$-21, %r15d
	jmp	.LBB13_30
.LBB13_13:
	movl	$-25, %r15d
	jmp	.LBB13_30
.LBB13_14:
	movl	gs_type1_state_sizeof(%rip), %edx
	movl	$1, %esi
	movl	$.L.str, %ecx
	movq	%r13, %rdi
	callq	alloc_free
	jmp	.LBB13_30
.LBB13_15:                              # %.thread68.outer
                                        #   in Loop: Header=BB13_16 Depth=1
	movq	%rbp, %rdi
	leaq	64(%rsp), %rsi
	callq	gx_path_current_point
	movzbl	137(%rbp), %eax
	movb	%al, 7(%rsp)            # 1-byte Spill
	jmp	.LBB13_26
	.p2align	4, 0x90
.LBB13_16:                              # =>This Inner Loop Header: Depth=1
	testb	$1, %r15b
	jne	.LBB13_20
# BB#17:                                #   in Loop: Header=BB13_16 Depth=1
	movq	igs(%rip), %rax
	movq	256(%rax), %rbp
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	gs_type1_pop
	shrl	%r15d
	decl	%r15d
	cmpl	$3, %r15d
	ja	.LBB13_34
# BB#18:                                #   in Loop: Header=BB13_16 Depth=1
	xorl	%esi, %esi
	jmpq	*.LJTI13_0(,%r15,8)
.LBB13_19:                              #   in Loop: Header=BB13_16 Depth=1
	movq	%rbp, %rdi
	leaq	48(%rsp), %rsi
	callq	gx_path_current_point
	movq	64(%rsp), %rsi
	movq	72(%rsp), %rdx
	movq	%rbp, %rdi
	callq	gx_path_add_point
	movzbl	7(%rsp), %eax           # 1-byte Folded Reload
	movb	%al, 137(%rbp)
	movq	48(%rsp), %rsi
	movq	56(%rsp), %rdx
	movq	%rbp, %rdi
	callq	gx_path_add_line
	movq	%r13, %rdi
	leaq	24(%rsp), %rsi
	callq	gs_type1_pop
	movq	%r13, %rdi
	leaq	32(%rsp), %rsi
	callq	gs_type1_pop
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	gs_type1_pop
	leaq	40(%rsp), %rax
	jmp	.LBB13_25
	.p2align	4, 0x90
.LBB13_20:                              #   in Loop: Header=BB13_16 Depth=1
	shrl	%r15d
	movzwl	42(%r14), %eax
	cmpl	%eax, %r15d
	jae	.LBB13_31
# BB#21:                                #   in Loop: Header=BB13_16 Depth=1
	movl	%r15d, %edx
	shlq	$4, %rdx
	addq	32(%r14), %rdx
	movq	%r12, %rdi
	movq	%r12, %rsi
	leaq	40(%rsp), %rcx
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB13_32
# BB#22:                                #   in Loop: Header=BB13_16 Depth=1
	movq	40(%rsp), %rax
	movzwl	8(%rax), %ecx
	andl	$252, %ecx
	cmpl	$52, %ecx
	jne	.LBB13_33
# BB#23:                                #   in Loop: Header=BB13_16 Depth=1
	movq	(%rax), %rsi
	jmp	.LBB13_27
.LBB13_24:                              #   in Loop: Header=BB13_16 Depth=1
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	gs_type1_pop
	movq	$12288, 24(%rsp)        # imm = 0x3000
	leaq	32(%rsp), %rax
.LBB13_25:                              # %.thread68.backedge
                                        #   in Loop: Header=BB13_16 Depth=1
	movq	%rax, 16(%rsp)
.LBB13_26:                              # %.thread68.backedge
                                        #   in Loop: Header=BB13_16 Depth=1
	xorl	%esi, %esi
.LBB13_27:                              # %.thread68.backedge
                                        #   in Loop: Header=BB13_16 Depth=1
	movq	%r13, %rdi
	callq	gs_type1_interpret
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jg	.LBB13_16
.LBB13_28:                              # %.thread68.outer._crit_edge
	movl	gs_type1_state_sizeof(%rip), %edx
	movl	$1, %esi
	movl	$.L.str, %ecx
	movq	%r13, %rdi
	callq	alloc_free
	testl	%r15d, %r15d
	js	.LBB13_30
.LBB13_29:
	addq	$-16, osp(%rip)
	xorl	%r15d, %r15d
.LBB13_30:
	movl	%r15d, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_31:
	movl	$-15, %r15d
	jmp	.LBB13_30
.LBB13_32:
	movl	$-21, %r15d
	jmp	.LBB13_30
.LBB13_33:
	movl	$-10, %r15d
	jmp	.LBB13_30
.LBB13_34:                              # %.thread69
	movl	gs_type1_state_sizeof(%rip), %edx
	movl	$1, %esi
	movl	$.L.str, %ecx
	movq	%r13, %rdi
	callq	alloc_free
	movl	$-15, %r15d
	jmp	.LBB13_30
.Lfunc_end13:
	.size	ztype1addpath, .Lfunc_end13-ztype1addpath
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI13_0:
	.quad	.LBB13_19
	.quad	.LBB13_15
	.quad	.LBB13_27
	.quad	.LBB13_24

	.text
	.globl	ztype1imagepath
	.p2align	4, 0x90
	.type	ztype1imagepath,@function
ztype1imagepath:                        # @ztype1imagepath
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi74:
	.cfi_def_cfa_offset 48
.Lcfi75:
	.cfi_offset %rbx, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	-104(%rbx), %eax
	andl	$252, %eax
	movl	$-20, %ebp
	cmpl	$52, %eax
	jne	.LBB14_9
# BB#1:
	movzwl	-88(%rbx), %eax
	andl	$252, %eax
	cmpl	$20, %eax
	jne	.LBB14_9
# BB#2:
	movzwl	-72(%rbx), %eax
	andl	$252, %eax
	cmpl	$20, %eax
	jne	.LBB14_9
# BB#3:
	leaq	-16(%rbx), %rdi
	movq	%rsp, %rdx
	movl	$4, %esi
	xorl	%eax, %eax
	callq	num_params
	testl	%eax, %eax
	js	.LBB14_4
# BB#5:
	movzwl	8(%rbx), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	cmpl	$52, %ecx
	jne	.LBB14_9
# BB#6:
	movl	$-7, %ebp
	testb	$1, %ah
	je	.LBB14_9
# BB#7:
	movq	igs(%rip), %rdi
	movq	-112(%rbx), %rsi
	movl	-96(%rbx), %edx
	movl	-80(%rbx), %ecx
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movss	12(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movq	(%rbx), %r8
	movzwl	10(%rbx), %r9d
	callq	gs_type1imagepath
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB14_9
# BB#8:
	leaq	-112(%rbx), %rax
	movups	(%rbx), %xmm0
	movups	%xmm0, (%rax)
	movw	%bp, -102(%rbx)
	addq	$-112, osp(%rip)
	xorl	%ebp, %ebp
	jmp	.LBB14_9
.LBB14_4:
	movl	%eax, %ebp
.LBB14_9:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end14:
	.size	ztype1imagepath, .Lfunc_end14-ztype1imagepath
	.cfi_endproc

	.globl	z1_subr_proc
	.p2align	4, 0x90
	.type	z1_subr_proc,@function
z1_subr_proc:                           # @z1_subr_proc
	.cfi_startproc
# BB#0:
	movl	$-15, %eax
	testl	%esi, %esi
	js	.LBB15_4
# BB#1:
	movq	16(%rdi), %rcx
	movq	(%rcx), %rcx
	movzwl	74(%rcx), %edi
	cmpl	%esi, %edi
	jle	.LBB15_4
# BB#2:
	movq	64(%rcx), %rcx
	movslq	%esi, %rsi
	shlq	$4, %rsi
	movzwl	8(%rcx,%rsi), %edi
	andl	$252, %edi
	movl	$-20, %eax
	cmpl	$52, %edi
	jne	.LBB15_4
# BB#3:
	movq	(%rcx,%rsi), %rax
	movq	%rax, (%rdx)
	xorl	%eax, %eax
.LBB15_4:
	retq
.Lfunc_end15:
	.size	z1_subr_proc, .Lfunc_end15-z1_subr_proc
	.cfi_endproc

	.globl	z1_pop_proc
	.p2align	4, 0x90
	.type	z1_pop_proc,@function
z1_pop_proc:                            # @z1_pop_proc
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	movq	8(%rax), %rcx
	leaq	-8(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	-8(%rcx), %rax
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end16:
	.size	z1_pop_proc, .Lfunc_end16-z1_pop_proc
	.cfi_endproc

	.globl	zchar_op_init
	.p2align	4, 0x90
	.type	zchar_op_init,@function
zchar_op_init:                          # @zchar_op_init
	.cfi_startproc
# BB#0:
	movl	$zchar_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end17:
	.size	zchar_op_init, .Lfunc_end17-zchar_op_init
	.cfi_endproc

	.globl	find_show
	.p2align	4, 0x90
	.type	find_show,@function
find_show:                              # @find_show
	.cfi_startproc
# BB#0:
	movq	esp(%rip), %rcx
	movl	$estack, %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB18_1:                               # =>This Inner Loop Header: Depth=1
	movzwl	8(%rcx), %esi
	andl	$252, %esi
	cmpl	$32, %esi
	jne	.LBB18_3
# BB#2:                                 #   in Loop: Header=BB18_1 Depth=1
	movzwl	(%rcx), %esi
	cmpl	$1, %esi
	je	.LBB18_4
.LBB18_3:                               # %.critedge
                                        #   in Loop: Header=BB18_1 Depth=1
	addq	$-16, %rcx
	cmpq	%rdx, %rcx
	jae	.LBB18_1
	jmp	.LBB18_5
.LBB18_4:
	movq	48(%rcx), %rax
.LBB18_5:                               # %.loopexit
	retq
.Lfunc_end18:
	.size	find_show, .Lfunc_end18-find_show
	.cfi_endproc

	.globl	free_show
	.p2align	4, 0x90
	.type	free_show,@function
free_show:                              # @free_show
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi77:
	.cfi_def_cfa_offset 16
	movq	esp(%rip), %rax
	movq	(%rax), %rdi
	movl	gs_show_enum_sizeof(%rip), %edx
	movl	$1, %esi
	movl	$.L.str.13, %ecx
	callq	alloc_free
	addq	$-64, esp(%rip)
	popq	%rax
	retq
.Lfunc_end19:
	.size	free_show, .Lfunc_end19-free_show
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"type1addpath"
	.size	.L.str, 13

	.type	zchar_op_init.my_defs,@object # @zchar_op_init.my_defs
	.data
	.p2align	4
zchar_op_init.my_defs:
	.quad	.L.str.1
	.quad	zashow
	.quad	.L.str.2
	.quad	zawidthshow
	.quad	.L.str.3
	.quad	zcharpath
	.quad	.L.str.4
	.quad	zkshow
	.quad	.L.str.5
	.quad	zsetcachedevice
	.quad	.L.str.6
	.quad	zsetcharwidth
	.quad	.L.str.7
	.quad	zshow
	.quad	.L.str.8
	.quad	zstringwidth
	.quad	.L.str.9
	.quad	ztype1addpath
	.quad	.L.str.10
	.quad	ztype1imagepath
	.quad	.L.str.11
	.quad	zwidthshow
	.zero	16
	.size	zchar_op_init.my_defs, 192

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"3ashow"
	.size	.L.str.1, 7

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"6awidthshow"
	.size	.L.str.2, 12

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"2charpath"
	.size	.L.str.3, 10

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"2kshow"
	.size	.L.str.4, 7

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"3setcachedevice"
	.size	.L.str.5, 16

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"2setcharwidth"
	.size	.L.str.6, 14

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"1show"
	.size	.L.str.7, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"1stringwidth"
	.size	.L.str.8, 13

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"1type1addpath"
	.size	.L.str.9, 14

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"8type1imagepath"
	.size	.L.str.10, 16

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"4widthshow"
	.size	.L.str.11, 11

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"setup_show"
	.size	.L.str.12, 11

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"free_show"
	.size	.L.str.13, 10


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
