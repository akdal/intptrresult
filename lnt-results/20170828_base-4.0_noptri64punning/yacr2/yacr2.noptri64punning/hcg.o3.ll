; ModuleID = 'yacr2.noptri64punning/hcg.bc'
source_filename = "hcg.i"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct._nodeHCGType = type { i64*, i64, i64 }

@channelNets = external local_unnamed_addr global i64, align 8
@HCG = common local_unnamed_addr global %struct._nodeHCGType* null, align 8
@storageRootHCG = common local_unnamed_addr global i64* null, align 8
@storageHCG = common local_unnamed_addr global i64* null, align 8
@storageLimitHCG = common local_unnamed_addr global i64 0, align 8
@FIRST = external local_unnamed_addr global i64*, align 8
@LAST = external local_unnamed_addr global i64*, align 8
@.str = private unnamed_addr constant [6 x i8] c"[%d]\0A\00", align 1
@.str.1 = private unnamed_addr constant [4 x i8] c"%d \00", align 1
@channelTracks = external local_unnamed_addr global i64, align 8
@str = private unnamed_addr constant [2 x i8] c"\0A\00"

; Function Attrs: nounwind uwtable
define void @AllocHCG() local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %1 = mul i64 %0, 24
  %mul = add i64 %1, 24
  %call = tail call noalias i8* @malloc(i64 %mul) #3
  store i8* %call, i8** bitcast (%struct._nodeHCGType** @HCG to i8**), align 8, !tbaa !5
  %add1 = add i64 %0, 1
  %mul3 = shl i64 %add1, 3
  %mul4 = mul i64 %mul3, %add1
  %call5 = tail call noalias i8* @malloc(i64 %mul4) #3
  store i8* %call5, i8** bitcast (i64** @storageRootHCG to i8**), align 8, !tbaa !5
  store i8* %call5, i8** bitcast (i64** @storageHCG to i8**), align 8, !tbaa !5
  %mul8 = mul i64 %add1, %add1
  store i64 %mul8, i64* @storageLimitHCG, align 8, !tbaa !1
  ret void
}

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) local_unnamed_addr #1

; Function Attrs: nounwind uwtable
define void @FreeHCG() local_unnamed_addr #0 {
entry:
  %0 = load i8*, i8** bitcast (%struct._nodeHCGType** @HCG to i8**), align 8, !tbaa !5
  tail call void @free(i8* %0) #3
  %1 = load i8*, i8** bitcast (i64** @storageRootHCG to i8**), align 8, !tbaa !5
  tail call void @free(i8* %1) #3
  store i64 0, i64* @storageLimitHCG, align 8, !tbaa !1
  ret void
}

; Function Attrs: nounwind
declare void @free(i8* nocapture) local_unnamed_addr #1

; Function Attrs: nounwind uwtable
define void @BuildHCG() local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %1 = mul i64 %0, 24
  %mul.i = add i64 %1, 24
  %call.i = tail call noalias i8* @malloc(i64 %mul.i) #3
  store i8* %call.i, i8** bitcast (%struct._nodeHCGType** @HCG to i8**), align 8, !tbaa !5
  %add1.i = add i64 %0, 1
  %mul3.i = shl i64 %add1.i, 3
  %mul4.i = mul i64 %mul3.i, %add1.i
  %call5.i = tail call noalias i8* @malloc(i64 %mul4.i) #3
  store i8* %call5.i, i8** bitcast (i64** @storageRootHCG to i8**), align 8, !tbaa !5
  store i8* %call5.i, i8** bitcast (i64** @storageHCG to i8**), align 8, !tbaa !5
  %mul8.i = mul i64 %add1.i, %add1.i
  store i64 %mul8.i, i64* @storageLimitHCG, align 8, !tbaa !1
  %cmp35 = icmp eq i64 %0, 0
  br i1 %cmp35, label %for.end34, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  %2 = bitcast i8* %call.i to %struct._nodeHCGType*
  %3 = bitcast i8* %call5.i to i64*
  br label %for.body

for.body:                                         ; preds = %for.end30.for.body_crit_edge, %for.body.preheader
  %4 = phi i64 [ %22, %for.end30.for.body_crit_edge ], [ %0, %for.body.preheader ]
  %5 = phi %struct._nodeHCGType* [ %23, %for.end30.for.body_crit_edge ], [ %2, %for.body.preheader ]
  %6 = phi i64* [ %.pre, %for.end30.for.body_crit_edge ], [ %3, %for.body.preheader ]
  %net.036 = phi i64 [ %inc33, %for.end30.for.body_crit_edge ], [ 1, %for.body.preheader ]
  %7 = load i64*, i64** @FIRST, align 8, !tbaa !5
  %arrayidx = getelementptr inbounds i64, i64* %7, i64 %net.036
  %8 = load i64, i64* %arrayidx, align 8, !tbaa !1
  %9 = load i64*, i64** @LAST, align 8, !tbaa !5
  %arrayidx1 = getelementptr inbounds i64, i64* %9, i64 %net.036
  %10 = load i64, i64* %arrayidx1, align 8, !tbaa !1
  %netsHook = getelementptr inbounds %struct._nodeHCGType, %struct._nodeHCGType* %5, i64 %net.036, i32 0
  store i64* %6, i64** %netsHook, align 8, !tbaa !7
  %cmp431 = icmp eq i64 %4, 0
  br i1 %cmp431, label %for.end30, label %for.body5.preheader

for.body5.preheader:                              ; preds = %for.body
  %.pre38 = load i64*, i64** @FIRST, align 8, !tbaa !5
  br label %for.body5

for.body5:                                        ; preds = %for.body5.preheader, %for.inc28
  %11 = phi i64 [ %21, %for.inc28 ], [ %4, %for.body5.preheader ]
  %which.033 = phi i64 [ %inc29, %for.inc28 ], [ 1, %for.body5.preheader ]
  %constraint.032 = phi i64 [ %constraint.1, %for.inc28 ], [ 0, %for.body5.preheader ]
  %arrayidx6 = getelementptr inbounds i64, i64* %.pre38, i64 %which.033
  %12 = load i64, i64* %arrayidx6, align 8, !tbaa !1
  %cmp7 = icmp ult i64 %12, %8
  br i1 %cmp7, label %land.lhs.true, label %lor.lhs.false

land.lhs.true:                                    ; preds = %for.body5
  %13 = load i64*, i64** @LAST, align 8, !tbaa !5
  %arrayidx8 = getelementptr inbounds i64, i64* %13, i64 %which.033
  %14 = load i64, i64* %arrayidx8, align 8, !tbaa !1
  %cmp9 = icmp ult i64 %14, %8
  br i1 %cmp9, label %for.inc28, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true, %for.body5
  %cmp11 = icmp ugt i64 %12, %10
  br i1 %cmp11, label %land.lhs.true12, label %for.end

land.lhs.true12:                                  ; preds = %lor.lhs.false
  %15 = load i64*, i64** @LAST, align 8, !tbaa !5
  %arrayidx13 = getelementptr inbounds i64, i64* %15, i64 %which.033
  %16 = load i64, i64* %arrayidx13, align 8, !tbaa !1
  %cmp14 = icmp ugt i64 %16, %10
  br i1 %cmp14, label %for.inc28, label %for.end

for.end:                                          ; preds = %lor.lhs.false, %land.lhs.true12
  %17 = load %struct._nodeHCGType*, %struct._nodeHCGType** @HCG, align 8, !tbaa !5
  %netsHook24 = getelementptr inbounds %struct._nodeHCGType, %struct._nodeHCGType* %17, i64 %net.036, i32 0
  %18 = load i64*, i64** %netsHook24, align 8, !tbaa !7
  %arrayidx25 = getelementptr inbounds i64, i64* %18, i64 %constraint.032
  store i64 %which.033, i64* %arrayidx25, align 8, !tbaa !1
  %19 = load i64*, i64** @storageHCG, align 8, !tbaa !5
  %incdec.ptr = getelementptr inbounds i64, i64* %19, i64 1
  store i64* %incdec.ptr, i64** @storageHCG, align 8, !tbaa !5
  %20 = load i64, i64* @storageLimitHCG, align 8, !tbaa !1
  %dec = add i64 %20, -1
  store i64 %dec, i64* @storageLimitHCG, align 8, !tbaa !1
  %inc26 = add i64 %constraint.032, 1
  %.pre40 = load i64, i64* @channelNets, align 8, !tbaa !1
  br label %for.inc28

for.inc28:                                        ; preds = %for.end, %land.lhs.true12, %land.lhs.true
  %21 = phi i64 [ %11, %land.lhs.true ], [ %11, %land.lhs.true12 ], [ %.pre40, %for.end ]
  %constraint.1 = phi i64 [ %constraint.032, %land.lhs.true ], [ %constraint.032, %land.lhs.true12 ], [ %inc26, %for.end ]
  %inc29 = add i64 %which.033, 1
  %cmp4 = icmp ugt i64 %inc29, %21
  br i1 %cmp4, label %for.end30.loopexit, label %for.body5

for.end30.loopexit:                               ; preds = %for.inc28
  br label %for.end30

for.end30:                                        ; preds = %for.end30.loopexit, %for.body
  %22 = phi i64 [ 0, %for.body ], [ %21, %for.end30.loopexit ]
  %constraint.0.lcssa = phi i64 [ 0, %for.body ], [ %constraint.1, %for.end30.loopexit ]
  %23 = load %struct._nodeHCGType*, %struct._nodeHCGType** @HCG, align 8, !tbaa !5
  %nets = getelementptr inbounds %struct._nodeHCGType, %struct._nodeHCGType* %23, i64 %net.036, i32 1
  store i64 %constraint.0.lcssa, i64* %nets, align 8, !tbaa !9
  %inc33 = add i64 %net.036, 1
  %cmp = icmp ugt i64 %inc33, %22
  br i1 %cmp, label %for.end34.loopexit, label %for.end30.for.body_crit_edge

for.end30.for.body_crit_edge:                     ; preds = %for.end30
  %.pre = load i64*, i64** @storageHCG, align 8, !tbaa !5
  br label %for.body

for.end34.loopexit:                               ; preds = %for.end30
  br label %for.end34

for.end34:                                        ; preds = %for.end34.loopexit, %entry
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @DFSClearHCG(%struct._nodeHCGType* nocapture %HCG) local_unnamed_addr #2 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp4 = icmp eq i64 %0, 0
  br i1 %cmp4, label %for.end, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.body
  %net.05 = phi i64 [ %inc, %for.body ], [ 1, %for.body.preheader ]
  %netsReached = getelementptr inbounds %struct._nodeHCGType, %struct._nodeHCGType* %HCG, i64 %net.05, i32 2
  store i64 0, i64* %netsReached, align 8, !tbaa !10
  %inc = add i64 %net.05, 1
  %cmp = icmp ugt i64 %inc, %0
  br i1 %cmp, label %for.end.loopexit, label %for.body

for.end.loopexit:                                 ; preds = %for.body
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  ret void
}

; Function Attrs: nounwind uwtable
define void @DumpHCG(%struct._nodeHCGType* nocapture readonly %HCG) local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp12 = icmp eq i64 %0, 0
  br i1 %cmp12, label %for.end10, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.end
  %net.013 = phi i64 [ %inc9, %for.end ], [ 1, %for.body.preheader ]
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str, i64 0, i64 0), i64 %net.013)
  %nets = getelementptr inbounds %struct._nodeHCGType, %struct._nodeHCGType* %HCG, i64 %net.013, i32 1
  %1 = load i64, i64* %nets, align 8, !tbaa !9
  %cmp210 = icmp eq i64 %1, 0
  br i1 %cmp210, label %for.end, label %for.body3.lr.ph

for.body3.lr.ph:                                  ; preds = %for.body
  %netsHook = getelementptr inbounds %struct._nodeHCGType, %struct._nodeHCGType* %HCG, i64 %net.013, i32 0
  br label %for.body3

for.body3:                                        ; preds = %for.body3.lr.ph, %for.body3
  %which.011 = phi i64 [ 0, %for.body3.lr.ph ], [ %inc, %for.body3 ]
  %2 = load i64*, i64** %netsHook, align 8, !tbaa !7
  %arrayidx5 = getelementptr inbounds i64, i64* %2, i64 %which.011
  %3 = load i64, i64* %arrayidx5, align 8, !tbaa !1
  %call6 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.1, i64 0, i64 0), i64 %3)
  %inc = add i64 %which.011, 1
  %4 = load i64, i64* %nets, align 8, !tbaa !9
  %cmp2 = icmp ult i64 %inc, %4
  br i1 %cmp2, label %for.body3, label %for.end.loopexit

for.end.loopexit:                                 ; preds = %for.body3
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %for.body
  %puts = tail call i32 @puts(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @str, i64 0, i64 0))
  %inc9 = add i64 %net.013, 1
  %5 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp = icmp ugt i64 %inc9, %5
  br i1 %cmp, label %for.end10.loopexit, label %for.body

for.end10.loopexit:                               ; preds = %for.end
  br label %for.end10

for.end10:                                        ; preds = %for.end10.loopexit, %entry
  ret void
}

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) local_unnamed_addr #1

; Function Attrs: norecurse nounwind uwtable
define void @NoHCV(%struct._nodeHCGType* nocapture readonly %HCG, i64 %select, i64* nocapture readonly %netsAssign, i64* nocapture %tracksNoHCV) local_unnamed_addr #2 {
entry:
  %0 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %cmp22 = icmp eq i64 %0, 0
  br i1 %cmp22, label %for.end22, label %for.cond1.preheader.lr.ph

for.cond1.preheader.lr.ph:                        ; preds = %entry
  %nets = getelementptr inbounds %struct._nodeHCGType, %struct._nodeHCGType* %HCG, i64 %select, i32 1
  %netsHook = getelementptr inbounds %struct._nodeHCGType, %struct._nodeHCGType* %HCG, i64 %select, i32 0
  br label %for.cond1.preheader

for.cond1.preheader:                              ; preds = %for.cond1.preheader.lr.ph, %for.end18
  %track.023 = phi i64 [ 1, %for.cond1.preheader.lr.ph ], [ %inc21, %for.end18 ]
  %1 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp220 = icmp eq i64 %1, 0
  br i1 %cmp220, label %for.end18, label %for.body3.preheader

for.body3.preheader:                              ; preds = %for.cond1.preheader
  br label %for.body3

for.body3:                                        ; preds = %for.body3.preheader, %for.inc16
  %net.021 = phi i64 [ %inc17, %for.inc16 ], [ 1, %for.body3.preheader ]
  %arrayidx = getelementptr inbounds i64, i64* %netsAssign, i64 %net.021
  %2 = load i64, i64* %arrayidx, align 8, !tbaa !1
  %cmp4 = icmp eq i64 %2, %track.023
  br i1 %cmp4, label %for.cond5.preheader, label %for.inc16

for.cond5.preheader:                              ; preds = %for.body3
  %3 = load i64, i64* %nets, align 8, !tbaa !9
  %cmp718 = icmp eq i64 %3, 0
  br i1 %cmp718, label %for.inc16, label %for.body8.lr.ph

for.body8.lr.ph:                                  ; preds = %for.cond5.preheader
  %4 = load i64*, i64** %netsHook, align 8, !tbaa !7
  br label %for.body8

for.cond5:                                        ; preds = %for.body8
  %cmp7 = icmp ult i64 %inc, %3
  br i1 %cmp7, label %for.body8, label %for.inc16.loopexit

for.body8:                                        ; preds = %for.body8.lr.ph, %for.cond5
  %which.019 = phi i64 [ 0, %for.body8.lr.ph ], [ %inc, %for.cond5 ]
  %arrayidx10 = getelementptr inbounds i64, i64* %4, i64 %which.019
  %5 = load i64, i64* %arrayidx10, align 8, !tbaa !1
  %cmp11 = icmp eq i64 %5, %net.021
  %inc = add i64 %which.019, 1
  br i1 %cmp11, label %for.end18.loopexit, label %for.cond5

for.inc16.loopexit:                               ; preds = %for.cond5
  br label %for.inc16

for.inc16:                                        ; preds = %for.inc16.loopexit, %for.cond5.preheader, %for.body3
  %inc17 = add i64 %net.021, 1
  %cmp2 = icmp ugt i64 %inc17, %1
  br i1 %cmp2, label %for.end18.loopexit27, label %for.body3

for.end18.loopexit:                               ; preds = %for.body8
  br label %for.end18

for.end18.loopexit27:                             ; preds = %for.inc16
  br label %for.end18

for.end18:                                        ; preds = %for.end18.loopexit27, %for.end18.loopexit, %for.cond1.preheader
  %ok.3 = phi i64 [ 1, %for.cond1.preheader ], [ 0, %for.end18.loopexit ], [ 1, %for.end18.loopexit27 ]
  %arrayidx19 = getelementptr inbounds i64, i64* %tracksNoHCV, i64 %track.023
  store i64 %ok.3, i64* %arrayidx19, align 8, !tbaa !1
  %inc21 = add i64 %track.023, 1
  %6 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %cmp = icmp ugt i64 %inc21, %6
  br i1 %cmp, label %for.end22.loopexit, label %for.cond1.preheader

for.end22.loopexit:                               ; preds = %for.end18
  br label %for.end22

for.end22:                                        ; preds = %for.end22.loopexit, %entry
  ret void
}

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #3

attributes #0 = { nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { norecurse nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 804284fdd0ceb0378cda00e5ed5b8746805eda6b)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"long", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!6, !6, i64 0}
!6 = !{!"any pointer", !3, i64 0}
!7 = !{!8, !6, i64 0}
!8 = !{!"_nodeHCGType", !6, i64 0, !2, i64 8, !2, i64 16}
!9 = !{!8, !2, i64 8}
!10 = !{!8, !2, i64 16}
