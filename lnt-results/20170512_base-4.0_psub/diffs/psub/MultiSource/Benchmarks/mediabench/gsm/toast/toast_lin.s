	.text
	.file	"toast_lin.bc"
	.globl	linear_input
	.p2align	4, 0x90
	.type	linear_input,@function
linear_input:                           # @linear_input
	.cfi_startproc
# BB#0:
	movq	in(%rip), %rcx
	movl	$2, %esi
	movl	$160, %edx
	jmp	fread                   # TAILCALL
.Lfunc_end0:
	.size	linear_input, .Lfunc_end0-linear_input
	.cfi_endproc

	.globl	linear_output
	.p2align	4, 0x90
	.type	linear_output,@function
linear_output:                          # @linear_output
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movq	out(%rip), %rcx
	movl	$2, %esi
	movl	$160, %edx
	callq	fwrite
	xorl	%ecx, %ecx
	cmpq	$160, %rax
	movl	$-1, %eax
	cmovel	%ecx, %eax
	popq	%rcx
	retq
.Lfunc_end1:
	.size	linear_output, .Lfunc_end1-linear_output
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
