	.text
	.file	"libclamav_pe.bc"
	.globl	cli_scanpe
	.p2align	4, 0x90
	.type	cli_scanpe,@function
cli_scanpe:                             # @cli_scanpe
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$8872, %rsp             # imm = 0x22A8
.Lcfi6:
	.cfi_def_cfa_offset 8928
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	$0, 84(%rsp)
	testq	%rbx, %rbx
	je	.LBB0_12
# BB#1:
	leaq	198(%rsp), %rsi
	movl	$2, %edx
	movl	%ebp, %edi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB0_13
# BB#2:
	movzwl	198(%rsp), %eax
	cmpl	$19802, %eax            # imm = 0x4D5A
	je	.LBB0_4
# BB#3:
	movzwl	%ax, %eax
	cmpl	$23117, %eax            # imm = 0x5A4D
	jne	.LBB0_17
.LBB0_4:
	movl	$58, %esi
	movl	$1, %edx
	movl	%ebp, %edi
	callq	lseek
	leaq	192(%rsp), %rsi
	movl	$4, %edx
	movl	%ebp, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB0_14
# BB#5:
	movl	192(%rsp), %esi
	xorl	%r13d, %r13d
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	192(%rsp), %esi
	testq	%rsi, %rsi
	je	.LBB0_18
# BB#6:
	xorl	%r13d, %r13d
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	testq	%rax, %rax
	js	.LBB0_19
# BB#7:
	leaq	216(%rsp), %rsi
	movl	$24, %edx
	movl	%ebp, %edi
	callq	cli_readn
	cmpl	$24, %eax
	jne	.LBB0_20
# BB#8:
	cmpl	$17744, 216(%rsp)       # imm = 0x4550
	jne	.LBB0_21
# BB#9:
	movzwl	238(%rsp), %eax
	testb	$32, %ah
	jne	.LBB0_24
# BB#10:
	xorl	%r14d, %r14d
	testb	$1, %al
	je	.LBB0_25
# BB#11:
	xorl	%r14d, %r14d
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_25
.LBB0_12:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-111, %r13d
	jmp	.LBB0_23
.LBB0_13:
	xorl	%r13d, %r13d
	movl	$.L.str.1, %edi
	jmp	.LBB0_22
.LBB0_14:
	xorl	%r13d, %r13d
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testb	$64, 40(%rbx)
	je	.LBB0_23
# BB#15:
	movq	(%rbx), %rax
	movl	$1, %r13d
	testq	%rax, %rax
	je	.LBB0_23
# BB#16:
	movq	$.L.str.4, (%rax)
	jmp	.LBB0_23
.LBB0_17:
	xorl	%r13d, %r13d
	movl	$.L.str.2, %edi
	jmp	.LBB0_22
.LBB0_18:
	movl	$.L.str.6, %edi
	jmp	.LBB0_22
.LBB0_19:
	movl	$.L.str.7, %edi
	jmp	.LBB0_22
.LBB0_20:
	xorl	%r13d, %r13d
	movl	$.L.str.8, %edi
	jmp	.LBB0_22
.LBB0_21:
	xorl	%r13d, %r13d
	movl	$.L.str.9, %edi
.LBB0_22:                               # %.thread2694
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_23:                               # %.thread2694
	movl	%r13d, %eax
	addq	$8872, %rsp             # imm = 0x22A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_24:
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %r14d
.LBB0_25:
	movswl	220(%rsp), %eax
	movzwl	%ax, %esi
	cmpl	$511, %eax              # imm = 0x1FF
	jg	.LBB0_30
# BB#26:
	testw	%ax, %ax
	js	.LBB0_40
# BB#27:
	leal	-332(%rax), %ecx
	movzwl	%cx, %ecx
	cmpl	$164, %ecx
	ja	.LBB0_52
# BB#28:
	jmpq	*.LJTI0_0(,%rcx,8)
.LBB0_29:
	movl	$.L.str.13, %edi
	jmp	.LBB0_76
.LBB0_30:
	cwtl
	cmpl	$869, %eax              # imm = 0x365
	jle	.LBB0_36
# BB#31:
	cmpl	$1311, %eax             # imm = 0x51F
	jle	.LBB0_47
# BB#32:
	cmpl	$1312, %esi             # imm = 0x520
	je	.LBB0_59
# BB#33:
	cmpl	$3311, %esi             # imm = 0xCEF
	je	.LBB0_58
# BB#34:
	cmpl	$3772, %esi             # imm = 0xEBC
	jne	.LBB0_54
# BB#35:
	movl	$.L.str.38, %edi
	jmp	.LBB0_76
.LBB0_36:
	cmpl	$615, %eax              # imm = 0x267
	jg	.LBB0_44
# BB#37:
	cmpl	$512, %esi              # imm = 0x200
	je	.LBB0_55
# BB#38:
	cmpl	$614, %esi              # imm = 0x266
	jne	.LBB0_54
# BB#39:
	movl	$.L.str.25, %edi
	jmp	.LBB0_76
.LBB0_40:
	cmpl	$34404, %esi            # imm = 0x8664
	je	.LBB0_50
# BB#41:
	cmpl	$36929, %esi            # imm = 0x9041
	je	.LBB0_51
# BB#42:
	cmpl	$49390, %esi            # imm = 0xC0EE
	jne	.LBB0_54
# BB#43:
	movl	$.L.str.40, %edi
	jmp	.LBB0_76
.LBB0_44:
	cmpl	$616, %esi              # imm = 0x268
	je	.LBB0_56
# BB#45:
	cmpl	$644, %esi              # imm = 0x284
	jne	.LBB0_54
# BB#46:
	movl	$.L.str.21, %edi
	jmp	.LBB0_76
.LBB0_47:
	cmpl	$870, %esi              # imm = 0x366
	je	.LBB0_57
# BB#48:
	cmpl	$1126, %esi             # imm = 0x466
	jne	.LBB0_54
# BB#49:
	movl	$.L.str.27, %edi
	jmp	.LBB0_76
.LBB0_50:
	movl	$.L.str.41, %edi
	jmp	.LBB0_76
.LBB0_51:
	movl	$.L.str.39, %edi
	jmp	.LBB0_76
.LBB0_52:
	testw	%ax, %ax
	jne	.LBB0_54
# BB#53:
	movl	$.L.str.12, %edi
	jmp	.LBB0_76
.LBB0_54:
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB0_77
.LBB0_55:
	movl	$.L.str.23, %edi
	jmp	.LBB0_76
.LBB0_56:
	movl	$.L.str.24, %edi
	jmp	.LBB0_76
.LBB0_57:
	movl	$.L.str.26, %edi
	jmp	.LBB0_76
.LBB0_58:
	movl	$.L.str.37, %edi
	jmp	.LBB0_76
.LBB0_59:
	movl	$.L.str.36, %edi
	jmp	.LBB0_76
.LBB0_60:
	movl	$.L.str.22, %edi
	jmp	.LBB0_76
.LBB0_61:
	movl	$.L.str.34, %edi
	jmp	.LBB0_76
.LBB0_62:
	movl	$.L.str.29, %edi
	jmp	.LBB0_76
.LBB0_63:
	movl	$.L.str.18, %edi
	jmp	.LBB0_76
.LBB0_64:
	movl	$.L.str.35, %edi
	jmp	.LBB0_76
.LBB0_65:
	movl	$.L.str.16, %edi
	jmp	.LBB0_76
.LBB0_66:
	movl	$.L.str.28, %edi
	jmp	.LBB0_76
.LBB0_67:
	movl	$.L.str.15, %edi
	jmp	.LBB0_76
.LBB0_68:
	movl	$.L.str.32, %edi
	jmp	.LBB0_76
.LBB0_69:
	movl	$.L.str.14, %edi
	jmp	.LBB0_76
.LBB0_70:
	movl	$.L.str.31, %edi
	jmp	.LBB0_76
.LBB0_71:
	movl	$.L.str.19, %edi
	jmp	.LBB0_76
.LBB0_72:
	movl	$.L.str.17, %edi
	jmp	.LBB0_76
.LBB0_73:
	movl	$.L.str.30, %edi
	jmp	.LBB0_76
.LBB0_74:
	movl	$.L.str.33, %edi
	jmp	.LBB0_76
.LBB0_75:
	movl	$.L.str.20, %edi
.LBB0_76:
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_77:
	movzwl	222(%rsp), %r15d
	leal	-1(%r15), %eax
	movzwl	%ax, %eax
	cmpl	$96, %eax
	jb	.LBB0_81
# BB#78:
	testb	$64, 40(%rbx)
	jne	.LBB0_85
# BB#79:
	xorl	%r13d, %r13d
	testw	%r15w, %r15w
	je	.LBB0_96
# BB#80:
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	cli_warnmsg
	jmp	.LBB0_23
.LBB0_81:
	xorl	%r13d, %r13d
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	cli_dbgmsg
	movl	224(%rsp), %eax
	movq	%rax, 520(%rsp)
	leaq	520(%rsp), %rdi
	callq	ctime
	movq	%rax, %rcx
	movl	$.L.str.46, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_dbgmsg
	movzwl	236(%rsp), %esi
	movl	$.L.str.47, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	236(%rsp), %eax
	cmpl	$223, %eax
	ja	.LBB0_87
# BB#82:
	movl	$.L.str.48, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testb	$64, 40(%rbx)
	je	.LBB0_23
# BB#83:
	movq	(%rbx), %rax
	movl	$1, %r13d
	testq	%rax, %rax
	je	.LBB0_23
# BB#84:
	movq	$.L.str.4, (%rax)
	jmp	.LBB0_23
.LBB0_85:
	movq	(%rbx), %rax
	movl	$1, %r13d
	testq	%rax, %rax
	je	.LBB0_23
# BB#86:
	movq	$.L.str.4, (%rax)
	jmp	.LBB0_23
.LBB0_87:
	leaq	280(%rsp), %rsi
	movl	$224, %edx
	movl	%ebp, %edi
	callq	cli_readn
	cmpl	$224, %eax
	jne	.LBB0_93
# BB#88:
	movzwl	280(%rsp), %eax
	cmpl	$267, %eax              # imm = 0x10B
	je	.LBB0_99
# BB#89:
	movzwl	%ax, %eax
	cmpl	$523, %eax              # imm = 0x20B
	jne	.LBB0_97
# BB#90:
	movzwl	236(%rsp), %eax
	cmpl	$240, %eax
	jne	.LBB0_105
# BB#91:
	leaq	504(%rsp), %rsi
	movl	$16, %edx
	movl	%ebp, %edi
	callq	cli_readn
	cmpl	$16, %eax
	jne	.LBB0_110
# BB#92:
	movl	296(%rsp), %r12d
	movl	340(%rsp), %r13d
	movl	$0, 184(%rsp)           # 4-byte Folded Spill
	movl	$.L.str.68, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	282(%rsp), %esi
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	283(%rsp), %esi
	movl	$.L.str.55, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	284(%rsp), %esi
	movl	$.L.str.56, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	288(%rsp), %esi
	movl	$.L.str.57, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	292(%rsp), %esi
	movl	$.L.str.58, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.59, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	cli_dbgmsg
	movl	300(%rsp), %esi
	movl	$.L.str.60, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	312(%rsp), %esi
	movl	$.L.str.61, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	316(%rsp), %esi
	movl	$.L.str.62, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	328(%rsp), %esi
	movl	$.L.str.63, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	330(%rsp), %esi
	movl	$.L.str.64, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	336(%rsp), %esi
	movl	$.L.str.65, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.66, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_dbgmsg
	movl	388(%rsp), %esi
	movl	$.L.str.67, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	$1, %al
	movl	%eax, 48(%rsp)          # 4-byte Spill
	jmp	.LBB0_102
.LBB0_93:
	xorl	%r13d, %r13d
	movl	$.L.str.49, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testb	$64, 40(%rbx)
	je	.LBB0_23
# BB#94:
	movq	(%rbx), %rax
	movl	$1, %r13d
	testq	%rax, %rax
	je	.LBB0_23
# BB#95:
	movq	$.L.str.4, (%rax)
	jmp	.LBB0_23
.LBB0_96:
	movl	$.L.str.44, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB0_23
.LBB0_97:
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	testb	$64, 40(%rbx)
	jne	.LBB0_108
# BB#98:
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_99:
	movzwl	236(%rsp), %esi
	cmpq	$224, %rsi
	movw	$224, %ax
	je	.LBB0_101
# BB#100:
	addq	$-224, %rsi
	movl	$1, %edx
	movl	%ebp, %edi
	callq	lseek
	movzwl	236(%rsp), %eax
.LBB0_101:
	movq	56(%rbx), %rcx
	movl	$16384, %edx            # imm = 0x4000
	andl	(%rcx), %edx
	movzwl	%ax, %eax
	cmpl	$328, %eax              # imm = 0x148
	sete	%al
	shrl	$14, %edx
	andb	%al, %dl
	movzbl	%dl, %eax
	movl	%eax, 184(%rsp)         # 4-byte Spill
	movl	296(%rsp), %r12d
	movl	340(%rsp), %r13d
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	movl	$.L.str.53, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	282(%rsp), %esi
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	283(%rsp), %esi
	movl	$.L.str.55, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	284(%rsp), %esi
	movl	$.L.str.56, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	288(%rsp), %esi
	movl	$.L.str.57, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	292(%rsp), %esi
	movl	$.L.str.58, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.59, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	cli_dbgmsg
	movl	300(%rsp), %esi
	movl	$.L.str.60, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	312(%rsp), %esi
	movl	$.L.str.61, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	316(%rsp), %esi
	movl	$.L.str.62, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	328(%rsp), %esi
	movl	$.L.str.63, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	330(%rsp), %esi
	movl	$.L.str.64, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	336(%rsp), %esi
	movl	$.L.str.65, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.66, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_dbgmsg
	movl	372(%rsp), %esi
	movl	$.L.str.67, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_102:
	movl	%r14d, 96(%rsp)         # 4-byte Spill
	movzwl	348(%rsp), %esi
	cmpq	$12, %rsi
	ja	.LBB0_153
# BB#103:
	jmpq	*.LJTI0_1(,%rsi,8)
.LBB0_104:
	xorl	%r14d, %r14d
	movl	$.L.str.69, %edi
	jmp	.LBB0_123
.LBB0_105:
	xorl	%r13d, %r13d
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testb	$64, 40(%rbx)
	je	.LBB0_23
# BB#106:
	movq	(%rbx), %rax
	movl	$1, %r13d
	testq	%rax, %rax
	je	.LBB0_23
# BB#107:
	movq	$.L.str.4, (%rax)
	jmp	.LBB0_23
.LBB0_108:
	movq	(%rbx), %rax
	movl	$1, %r13d
	testq	%rax, %rax
	je	.LBB0_23
# BB#109:
	movq	$.L.str.4, (%rax)
	jmp	.LBB0_23
.LBB0_110:
	xorl	%r13d, %r13d
	movl	$.L.str.49, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testb	$64, 40(%rbx)
	je	.LBB0_23
# BB#111:
	movq	(%rbx), %rax
	movl	$1, %r13d
	testq	%rax, %rax
	je	.LBB0_23
# BB#112:
	movq	$.L.str.4, (%rax)
	jmp	.LBB0_23
.LBB0_113:
	xorl	%r14d, %r14d
	movl	$.L.str.79, %edi
	jmp	.LBB0_123
.LBB0_114:
	xorl	%r14d, %r14d
	movl	$.L.str.73, %edi
	jmp	.LBB0_123
.LBB0_115:
	xorl	%r14d, %r14d
	movl	$.L.str.74, %edi
	jmp	.LBB0_123
.LBB0_116:
	xorl	%r14d, %r14d
	movl	$.L.str.72, %edi
	jmp	.LBB0_123
.LBB0_117:
	xorl	%r14d, %r14d
	movl	$.L.str.71, %edi
	jmp	.LBB0_123
.LBB0_118:
	movl	$.L.str.70, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	$1, %r14b
	jmp	.LBB0_124
.LBB0_119:
	xorl	%r14d, %r14d
	movl	$.L.str.75, %edi
	jmp	.LBB0_123
.LBB0_120:
	xorl	%r14d, %r14d
	movl	$.L.str.76, %edi
	jmp	.LBB0_123
.LBB0_121:
	xorl	%r14d, %r14d
	movl	$.L.str.78, %edi
	jmp	.LBB0_123
.LBB0_122:
	xorl	%r14d, %r14d
	movl	$.L.str.77, %edi
.LBB0_123:
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_124:
	movl	$.L.str.81, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testb	%r14b, %r14b
	jne	.LBB0_130
# BB#125:
	movl	40(%rbx), %eax
	andl	$64, %eax
	je	.LBB0_130
# BB#126:
	movl	312(%rsp), %eax
	testl	%eax, %eax
	je	.LBB0_145
# BB#127:
	andl	$4095, %eax             # imm = 0xFFF
	jne	.LBB0_145
# BB#128:
	movl	316(%rsp), %eax
	testl	%eax, %eax
	je	.LBB0_149
# BB#129:
	andl	$511, %eax              # imm = 0x1FF
	jne	.LBB0_149
.LBB0_130:                              # %.thread3110
	leaq	528(%rsp), %rdx
	movl	$1, %edi
	movl	%ebp, %esi
	callq	__fxstat
	cmpl	$-1, %eax
	je	.LBB0_147
# BB#131:
	movq	576(%rsp), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	$40, %esi
	movq	%r15, %rdi
	callq	cli_calloc
	movq	%rax, 168(%rsp)         # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_148
# BB#132:
	movl	$36, %esi
	movq	%r15, %rdi
	callq	cli_calloc
	testq	%rax, %rax
	je	.LBB0_151
# BB#133:
	movl	%r13d, 156(%rsp)        # 4-byte Spill
	movq	%r12, 104(%rsp)         # 8-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	312(%rsp), %eax
	movl	%eax, 140(%rsp)         # 4-byte Spill
	movl	316(%rsp), %r14d
	movq	%r15, 88(%rsp)          # 8-byte Spill
	leal	(,%r15,8), %eax
	leal	(%rax,%rax,4), %r12d
	movl	%ebp, %edi
	movq	168(%rsp), %rsi         # 8-byte Reload
	movl	%r12d, %edx
	callq	cli_readn
	cmpl	%r12d, %eax
	jne	.LBB0_154
# BB#134:                               # %.preheader2824
	cmpl	$512, %r14d             # imm = 0x200
	je	.LBB0_157
# BB#135:                               # %.preheader2824
	cmpw	$0, 88(%rsp)            # 2-byte Folded Reload
	je	.LBB0_157
# BB#136:                               # %.lr.ph2972.preheader
	movq	168(%rsp), %rax         # 8-byte Reload
	leaq	20(%rax), %rsi
	movl	$1, %edi
	movq	88(%rsp), %r8           # 8-byte Reload
.LBB0_137:                              # %.lr.ph2972
                                        # =>This Inner Loop Header: Depth=1
	testl	%r14d, %r14d
	movl	$0, %eax
	je	.LBB0_143
# BB#138:                               #   in Loop: Header=BB0_137 Depth=1
	cmpl	$0, -4(%rsi)
	je	.LBB0_142
# BB#139:                               #   in Loop: Header=BB0_137 Depth=1
	movl	(%rsi), %ecx
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%r14d
	testl	%edx, %edx
	je	.LBB0_142
# BB#140:                               #   in Loop: Header=BB0_137 Depth=1
	andl	$511, %ecx              # imm = 0x1FF
	movl	%r14d, %eax
	jne	.LBB0_143
	jmp	.LBB0_141
.LBB0_142:                              #   in Loop: Header=BB0_137 Depth=1
	movl	%r14d, %eax
.LBB0_143:                              #   in Loop: Header=BB0_137 Depth=1
	cmpl	$512, %eax              # imm = 0x200
	je	.LBB0_158
# BB#144:                               #   in Loop: Header=BB0_137 Depth=1
	addq	$40, %rsi
	cmpq	%r8, %rdi
	leaq	1(%rdi), %rdi
	movl	%eax, %r14d
	jb	.LBB0_137
	jmp	.LBB0_158
.LBB0_145:
	movl	$.L.str.82, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	(%rbx), %rax
	movl	$1, %r13d
	testq	%rax, %rax
	je	.LBB0_23
# BB#146:
	movq	$.L.str.4, (%rax)
	jmp	.LBB0_23
.LBB0_147:
	movl	$.L.str.84, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-123, %r13d
	jmp	.LBB0_23
.LBB0_148:
	movl	$.L.str.85, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-114, %r13d
	jmp	.LBB0_23
.LBB0_149:
	movl	$.L.str.83, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	(%rbx), %rax
	movl	$1, %r13d
	testq	%rax, %rax
	je	.LBB0_23
# BB#150:
	movq	$.L.str.4, (%rax)
	jmp	.LBB0_23
.LBB0_151:
	movl	$.L.str.85, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	168(%rsp), %rdi         # 8-byte Reload
.LBB0_152:                              # %.thread2694
	callq	free
	movl	$-114, %r13d
	jmp	.LBB0_23
.LBB0_153:
	xorl	%r14d, %r14d
	movl	$.L.str.80, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_warnmsg
	jmp	.LBB0_124
.LBB0_154:
	xorl	%r13d, %r13d
	movl	$.L.str.86, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.87, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	168(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	testb	$64, 40(%rbx)
	je	.LBB0_23
# BB#155:
	movq	(%rbx), %rax
	movl	$1, %r13d
	testq	%rax, %rax
	je	.LBB0_23
# BB#156:
	movq	$.L.str.4, (%rax)
	jmp	.LBB0_23
.LBB0_157:
	movl	%r14d, %eax
	jmp	.LBB0_158
.LBB0_141:                              # %.thread3112
	movl	$.L.str.88, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$512, %eax              # imm = 0x200
.LBB0_158:                              # %.critedge
	movl	%eax, 120(%rsp)         # 4-byte Spill
	cmpl	$0, 140(%rsp)           # 4-byte Folded Reload
	je	.LBB0_160
# BB#159:
	xorl	%edx, %edx
	movl	156(%rsp), %eax         # 4-byte Reload
	movl	140(%rsp), %ecx         # 4-byte Reload
	divl	%ecx
	cmpl	$1, %edx
	sbbl	$-1, %eax
	imull	%ecx, %eax
	movl	%eax, 156(%rsp)         # 4-byte Spill
.LBB0_160:
	cmpw	$0, 88(%rsp)            # 2-byte Folded Reload
	je	.LBB0_224
# BB#161:                               # %.lr.ph2963
	xorl	%r15d, %r15d
	movl	$0, 160(%rsp)           # 4-byte Folded Spill
	movl	$0, 116(%rsp)           # 4-byte Folded Spill
	movl	$0, 180(%rsp)           # 4-byte Folded Spill
	movq	40(%rsp), %r13          # 8-byte Reload
.LBB0_162:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_195 Depth 2
	leaq	(%r15,%r15,4), %r14
	movq	168(%rsp), %r12         # 8-byte Reload
	leaq	(%r12,%r14,8), %rsi
	movl	$8, %edx
	leaq	247(%rsp), %rdi
	callq	strncpy
	movl	140(%rsp), %edi         # 4-byte Reload
	testl	%edi, %edi
	movb	$0, 255(%rsp)
	movl	12(%r12,%r14,8), %r9d
	je	.LBB0_164
# BB#163:                               #   in Loop: Header=BB0_162 Depth=1
	xorl	%edx, %edx
	movl	%r9d, %eax
	divl	%edi
	movl	%r9d, %eax
	subl	%edx, %eax
	leaq	(%r15,%r15,8), %rcx
	leaq	(%r13,%rcx,4), %rdx
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	movl	%eax, (%r13,%rcx,4)
	movl	8(%r12,%r14,8), %eax
	xorl	%edx, %edx
	movl	%eax, 56(%rsp)          # 4-byte Spill
	divl	%edi
	movl	%eax, %r8d
	cmpl	$1, %edx
	sbbl	$-1, %r8d
	imull	%edi, %r8d
	jmp	.LBB0_165
.LBB0_164:                              #   in Loop: Header=BB0_162 Depth=1
	leaq	(%r15,%r15,8), %rax
	leaq	(%r13,%rax,4), %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movl	%r9d, (%r13,%rax,4)
	movl	8(%r12,%r14,8), %r8d
	movl	%r8d, 56(%rsp)          # 4-byte Spill
.LBB0_165:                              #   in Loop: Header=BB0_162 Depth=1
	movl	120(%rsp), %edi         # 4-byte Reload
	movq	%r15, 72(%rsp)          # 8-byte Spill
	leaq	(%r15,%r15,8), %r15
	movl	%r8d, 4(%r13,%r15,4)
	movl	20(%r12,%r14,8), %ecx
	testl	%edi, %edi
	je	.LBB0_167
# BB#166:                               #   in Loop: Header=BB0_162 Depth=1
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%edi
	movl	%ecx, %r11d
	subl	%edx, %r11d
	leaq	8(%r13,%r15,4), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%r11d, 8(%r13,%r15,4)
	movl	16(%r12,%r14,8), %r10d
	xorl	%edx, %edx
	movl	%r10d, %eax
	divl	%edi
	movl	%eax, %esi
	cmpl	$1, %edx
	sbbl	$-1, %esi
	imull	%edi, %esi
	jmp	.LBB0_168
.LBB0_167:                              #   in Loop: Header=BB0_162 Depth=1
	leaq	8(%r13,%r15,4), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%ecx, 8(%r13,%r15,4)
	movl	16(%r12,%r14,8), %r10d
	movl	%ecx, %r11d
	movl	%r10d, %esi
.LBB0_168:                              #   in Loop: Header=BB0_162 Depth=1
	movq	%r13, %rdx
	leaq	4(%rdx,%r15,4), %r13
	movl	%esi, 12(%rdx,%r15,4)
	movl	36(%r12,%r14,8), %eax
	movl	%eax, 16(%rdx,%r15,4)
	movl	%r9d, 20(%rdx,%r15,4)
	movl	56(%rsp), %eax          # 4-byte Reload
	movl	%eax, 24(%rdx,%r15,4)
	movl	%ecx, 28(%rdx,%r15,4)
	movl	%r10d, 32(%rdx,%r15,4)
	testl	%r8d, %r8d
	movl	140(%rsp), %r14d        # 4-byte Reload
	jne	.LBB0_173
# BB#169:                               #   in Loop: Header=BB0_162 Depth=1
	testl	%esi, %esi
	movq	64(%rsp), %r12          # 8-byte Reload
	je	.LBB0_179
# BB#170:                               #   in Loop: Header=BB0_162 Depth=1
	testl	%r14d, %r14d
	je	.LBB0_172
# BB#171:                               #   in Loop: Header=BB0_162 Depth=1
	xorl	%edx, %edx
	movl	%r10d, %eax
	divl	%r14d
	cmpl	$1, %edx
	movl	%eax, %r10d
	sbbl	$-1, %r10d
	imull	%r14d, %r10d
.LBB0_172:                              #   in Loop: Header=BB0_162 Depth=1
	movl	%r10d, (%r13)
.LBB0_173:                              #   in Loop: Header=BB0_162 Depth=1
	testl	%esi, %esi
	movq	64(%rsp), %r12          # 8-byte Reload
	je	.LBB0_179
# BB#174:                               #   in Loop: Header=BB0_162 Depth=1
	movl	%r11d, %eax
	cmpq	%rax, 128(%rsp)         # 8-byte Folded Reload
	jbe	.LBB0_179
# BB#175:                               #   in Loop: Header=BB0_162 Depth=1
	cmpl	$0, 128(%rsp)           # 4-byte Folded Reload
	je	.LBB0_178
# BB#176:                               #   in Loop: Header=BB0_162 Depth=1
	cmpl	128(%rsp), %esi         # 4-byte Folded Reload
	ja	.LBB0_178
# BB#177:                               #   in Loop: Header=BB0_162 Depth=1
	leal	-1(%rsi,%r11), %ecx
	cmpl	128(%rsp), %ecx         # 4-byte Folded Reload
	jb	.LBB0_179
.LBB0_178:                              #   in Loop: Header=BB0_162 Depth=1
	movq	128(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%eax, %ecx
	movl	%ecx, 8(%r13)
.LBB0_179:                              # %.thread
                                        #   in Loop: Header=BB0_162 Depth=1
	movl	$.L.str.89, %edi
	xorl	%eax, %eax
	movq	72(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movl	$.L.str.90, %edi
	xorl	%eax, %eax
	leaq	247(%rsp), %rsi
	callq	cli_dbgmsg
	movl	$.L.str.91, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	20(%r13), %esi
	movl	(%r13), %edx
	movl	$.L.str.92, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	16(%r13), %esi
	movq	144(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %edx
	movl	$.L.str.93, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	28(%r13), %esi
	movl	8(%r13), %edx
	movl	$.L.str.94, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	24(%r13), %esi
	movl	(%r12), %edx
	movl	$.L.str.95, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testb	$32, 12(%r13)
	je	.LBB0_182
# BB#180:                               #   in Loop: Header=BB0_162 Depth=1
	movl	$.L.str.96, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	(%r13), %eax
	cmpl	8(%r13), %eax
	jae	.LBB0_182
# BB#181:                               #   in Loop: Header=BB0_162 Depth=1
	movl	$.L.str.97, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_182:                              #   in Loop: Header=BB0_162 Depth=1
	movl	12(%r13), %eax
	testl	$536870912, %eax        # imm = 0x20000000
	je	.LBB0_184
# BB#183:                               #   in Loop: Header=BB0_162 Depth=1
	movl	$.L.str.98, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	12(%r13), %eax
.LBB0_184:                              #   in Loop: Header=BB0_162 Depth=1
	testl	%eax, %eax
	jns	.LBB0_186
# BB#185:                               #   in Loop: Header=BB0_162 Depth=1
	movl	$.L.str.99, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_186:                              #   in Loop: Header=BB0_162 Depth=1
	movl	$.L.str.81, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	40(%rbx), %ecx
	testb	$64, %cl
	je	.LBB0_188
# BB#187:                               #   in Loop: Header=BB0_162 Depth=1
	movl	16(%r13), %eax
	xorl	%edx, %edx
	divl	%r14d
	testl	%edx, %edx
	jne	.LBB0_241
.LBB0_188:                              #   in Loop: Header=BB0_162 Depth=1
	movl	8(%r13), %eax
	testl	%eax, %eax
	je	.LBB0_215
# BB#189:                               #   in Loop: Header=BB0_162 Depth=1
	movl	(%r12), %edx
	cmpq	128(%rsp), %rdx         # 8-byte Folded Reload
	jae	.LBB0_238
# BB#190:                               #   in Loop: Header=BB0_162 Depth=1
	testb	$2, %ch
	movq	56(%rbx), %rdx
	movl	(%rdx), %edi
	jne	.LBB0_198
.LBB0_191:                              # %._crit_edge3072
                                        #   in Loop: Header=BB0_162 Depth=1
	movq	24(%rbx), %rdx
	movq	32(%rdx), %rsi
	testq	%rsi, %rsi
	je	.LBB0_215
# BB#192:                               # %._crit_edge3072
                                        #   in Loop: Header=BB0_162 Depth=1
	andl	$16, %edi
	je	.LBB0_215
# BB#193:                               # %.preheader2823
                                        #   in Loop: Header=BB0_162 Depth=1
	movl	32(%rsi), %edx
	testl	%edx, %edx
	je	.LBB0_215
# BB#194:                               # %.lr.ph2956
                                        #   in Loop: Header=BB0_162 Depth=1
	movq	24(%rsi), %rsi
	xorl	%edi, %edi
.LBB0_195:                              #   Parent Loop BB0_162 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%eax, (%rsi,%rdi,4)
	ja	.LBB0_215
# BB#196:                               #   in Loop: Header=BB0_195 Depth=2
	je	.LBB0_204
# BB#197:                               #   in Loop: Header=BB0_195 Depth=2
	incq	%rdi
	cmpl	%edx, %edi
	jb	.LBB0_195
	jmp	.LBB0_215
.LBB0_198:                              #   in Loop: Header=BB0_162 Depth=1
	testb	$8, %dil
	je	.LBB0_191
# BB#199:                               #   in Loop: Header=BB0_162 Depth=1
	movb	247(%rsp), %dl
	testb	%dl, %dl
	jne	.LBB0_191
# BB#200:                               #   in Loop: Header=BB0_162 Depth=1
	movl	(%r13), %edx
	movl	$-40001, %esi           # imm = 0xFFFF63BF
	addl	%esi, %edx
	cmpl	$29998, %edx            # imm = 0x752E
	ja	.LBB0_191
# BB#201:                               #   in Loop: Header=BB0_162 Depth=1
	cmpl	$-536870816, 12(%r13)   # imm = 0xE0000060
	movq	72(%rsp), %rdx          # 8-byte Reload
	movl	%edx, %esi
	je	.LBB0_203
# BB#202:                               #   in Loop: Header=BB0_162 Depth=1
	movl	160(%rsp), %edx         # 4-byte Reload
	movl	%edx, %esi
.LBB0_203:                              #   in Loop: Header=BB0_162 Depth=1
	movb	%sil, %dl
	movl	%edx, 160(%rsp)         # 4-byte Spill
	jmp	.LBB0_191
.LBB0_204:                              #   in Loop: Header=BB0_162 Depth=1
	cmpl	$184549377, %eax        # imm = 0xB000001
	jb	.LBB0_206
# BB#205:                               #   in Loop: Header=BB0_162 Depth=1
	movl	$.L.str.256, %edi
	jmp	.LBB0_213
.LBB0_206:                              #   in Loop: Header=BB0_162 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	8(%rax,%r15,4), %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_210
# BB#207:                               # %cli_seeksect.exit.i
                                        #   in Loop: Header=BB0_162 Depth=1
	movl	8(%r13), %edi
	callq	cli_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_211
# BB#208:                               #   in Loop: Header=BB0_162 Depth=1
	movl	8(%r13), %edx
	movl	%ebp, %edi
	movq	%r15, %rsi
	callq	cli_readn
	cmpl	8(%r13), %eax
	jne	.LBB0_212
# BB#209:                               #   in Loop: Header=BB0_162 Depth=1
	leaq	4768(%rsp), %r14
	movq	%r14, %rdi
	callq	cli_md5_init
	movl	8(%r13), %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	cli_md5_update
	movq	%r15, %rdi
	callq	free
	leaq	672(%rsp), %r15
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	cli_md5_final
	movq	(%rbx), %rdx
	movq	24(%rbx), %rax
	movq	32(%rax), %rcx
	movl	$-1, (%rsp)
	movl	$16, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r15, %rdi
	callq	cli_bm_scanbuff
	cmpl	$1, %eax
	jne	.LBB0_214
	jmp	.LBB0_283
.LBB0_210:                              # %cli_seeksect.exit.thread13.i
                                        #   in Loop: Header=BB0_162 Depth=1
	movl	$.L.str.259, %edi
	jmp	.LBB0_213
.LBB0_211:                              #   in Loop: Header=BB0_162 Depth=1
	movl	$.L.str.257, %edi
	jmp	.LBB0_213
.LBB0_212:                              #   in Loop: Header=BB0_162 Depth=1
	movl	$.L.str.258, %edi
.LBB0_213:                              #   in Loop: Header=BB0_162 Depth=1
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_214:                              #   in Loop: Header=BB0_162 Depth=1
	movl	40(%rbx), %ecx
.LBB0_215:                              # %.critedge33
                                        #   in Loop: Header=BB0_162 Depth=1
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %r15          # 8-byte Reload
	andl	$64, %ecx
	testq	%r15, %r15
	je	.LBB0_219
# BB#216:                               #   in Loop: Header=BB0_162 Depth=1
	testl	%ecx, %ecx
	je	.LBB0_218
# BB#217:                               #   in Loop: Header=BB0_162 Depth=1
	movl	16(%r13), %eax
	leal	-1(%r15), %ecx
	leaq	(%rcx,%rcx,8), %rcx
	movq	40(%rsp), %rdx          # 8-byte Reload
	subl	20(%rdx,%rcx,4), %eax
	cmpl	4(%rdx,%rcx,4), %eax
	jne	.LBB0_279
.LBB0_218:                              #   in Loop: Header=BB0_162 Depth=1
	movq	144(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %eax
	movl	116(%rsp), %ecx         # 4-byte Reload
	cmpl	%ecx, %eax
	cmovbl	%eax, %ecx
	movl	%ecx, 116(%rsp)         # 4-byte Spill
	addl	8(%r13), %eax
	movl	180(%rsp), %ecx         # 4-byte Reload
	cmpl	%ecx, %eax
	cmoval	%eax, %ecx
	jmp	.LBB0_222
.LBB0_219:                              #   in Loop: Header=BB0_162 Depth=1
	testl	%ecx, %ecx
	je	.LBB0_221
# BB#220:                               #   in Loop: Header=BB0_162 Depth=1
	movl	156(%rsp), %eax         # 4-byte Reload
	cmpl	%eax, 16(%r13)
	jne	.LBB0_280
.LBB0_221:                              #   in Loop: Header=BB0_162 Depth=1
	movq	144(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %eax
	movl	8(%r13), %ecx
	movl	%eax, 116(%rsp)         # 4-byte Spill
	addl	%eax, %ecx
.LBB0_222:                              #   in Loop: Header=BB0_162 Depth=1
	movl	%ecx, 180(%rsp)         # 4-byte Spill
	incq	%r15
	cmpq	%rsi, %r15
	movq	40(%rsp), %r13          # 8-byte Reload
	jb	.LBB0_162
# BB#223:                               # %._crit_edge2964.loopexit
	movq	%r15, 72(%rsp)          # 8-byte Spill
	jmp	.LBB0_225
.LBB0_224:
	movl	$0, 180(%rsp)           # 4-byte Folded Spill
	movl	$0, 116(%rsp)           # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	$0, 160(%rsp)           # 4-byte Folded Spill
.LBB0_225:                              # %._crit_edge2964
	movq	168(%rsp), %rdi         # 8-byte Reload
	callq	free
	movl	156(%rsp), %eax         # 4-byte Reload
	cmpl	%eax, 104(%rsp)         # 4-byte Folded Reload
	jae	.LBB0_227
# BB#226:
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	%eax, %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	cmpq	128(%rsp), %rcx         # 8-byte Folded Reload
	setae	%r8b
	cmovbl	%eax, %edx
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	testl	%r8d, %r8d
	jne	.LBB0_234
	jmp	.LBB0_243
.LBB0_227:
	xorl	%eax, %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	$1, %r8d
	cmpw	$0, 88(%rsp)            # 2-byte Folded Reload
	je	.LBB0_233
# BB#228:                               # %.lr.ph.i.preheader
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	1(%rcx), %rdx
	leaq	(%rcx,%rcx,8), %rcx
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	-24(%rsi,%rcx,4), %rcx
	movq	104(%rsp), %rax         # 8-byte Reload
.LBB0_229:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edi
	testl	%edi, %edi
	je	.LBB0_232
# BB#230:                               #   in Loop: Header=BB0_229 Depth=1
	movl	%eax, %esi
	subl	-12(%rcx), %esi
	jb	.LBB0_232
# BB#231:                               #   in Loop: Header=BB0_229 Depth=1
	cmpl	%esi, %edi
	ja	.LBB0_242
.LBB0_232:                              # %.backedge.i
                                        #   in Loop: Header=BB0_229 Depth=1
	decq	%rdx
	addq	$-36, %rcx
	cmpq	$1, %rdx
	jg	.LBB0_229
.LBB0_233:                              # %cli_rawaddr.exit
	testl	%r8d, %r8d
	je	.LBB0_243
.LBB0_234:                              # %cli_rawaddr.exit
	cmpl	$0, 144(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_243
# BB#235:
	xorl	%r13d, %r13d
	movl	$.L.str.104, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	testb	$64, 40(%rbx)
	je	.LBB0_23
# BB#236:
	movq	(%rbx), %rax
	movl	$1, %r13d
	testq	%rax, %rax
	je	.LBB0_23
# BB#237:
	movq	$.L.str.4, (%rax)
	jmp	.LBB0_23
.LBB0_238:
	xorl	%r13d, %r13d
	movl	$.L.str.101, %edi
	xorl	%eax, %eax
	movq	72(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	128(%rsp), %rcx         # 8-byte Reload
	callq	cli_dbgmsg
	movq	168(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	testb	$64, 40(%rbx)
	je	.LBB0_23
# BB#239:
	movq	(%rbx), %rax
	movl	$1, %r13d
	testq	%rax, %rax
	je	.LBB0_23
# BB#240:
	movq	$.L.str.4, (%rax)
	jmp	.LBB0_23
.LBB0_241:
	movl	$.L.str.100, %edi
	jmp	.LBB0_281
.LBB0_242:                              # %cli_rawaddr.exit.thread
	addl	-4(%rcx), %esi
	movl	%esi, %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
.LBB0_243:
	xorl	%r13d, %r13d
	movl	$.L.str.105, %edi
	xorl	%eax, %eax
	movq	144(%rsp), %rsi         # 8-byte Reload
	movl	%esi, %edx
	callq	cli_dbgmsg
	cmpb	$0, 48(%rsp)            # 1-byte Folded Reload
	je	.LBB0_245
.LBB0_244:
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	jmp	.LBB0_23
.LBB0_245:
	movl	144(%rsp), %esi         # 4-byte Reload
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	leaq	672(%rsp), %rsi
	movl	$4096, %edx             # imm = 0x1000
	movl	%ebp, %edi
	movq	%rsi, 168(%rsp)         # 8-byte Spill
	callq	cli_readn
	movl	%eax, 140(%rsp)         # 4-byte Spill
	testb	$2, 41(%rbx)
	je	.LBB0_255
# BB#246:
	cmpl	$4096, 140(%rsp)        # 4-byte Folded Reload
                                        # imm = 0x1000
	jne	.LBB0_255
# BB#247:
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_255
# BB#248:
	movq	56(%rbx), %rax
	movl	(%rax), %eax
	andl	$1, %eax
	je	.LBB0_255
# BB#249:
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rax,8), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	144(%rsp), %rdx         # 8-byte Reload
	cmpl	-28(%rcx,%rax,4), %edx
	jne	.LBB0_255
# BB#250:
	leaq	672(%rsp), %rdi
	movl	$4040, %esi             # imm = 0xFC8
	movl	$.L.str.106, %edx
	movl	$15, %ecx
	callq	cli_memstr
	testq	%rax, %rax
	je	.LBB0_255
# BB#251:
	movl	19(%rax), %ecx
	xorl	15(%rax), %ecx
	cmpl	$5265999, %ecx          # imm = 0x505A4F
	jne	.LBB0_255
# BB#252:
	movl	27(%rax), %ecx
	xorl	23(%rax), %ecx
	cmpl	$1048571, %ecx          # imm = 0xFFFFB
	jne	.LBB0_255
# BB#253:
	movl	35(%rax), %ecx
	xorl	31(%rax), %ecx
	cmpl	$184, %ecx
	jne	.LBB0_255
# BB#254:                               # %.critedge2509
	movq	(%rbx), %rax
	movq	$.L.str.107, (%rax)
	jmp	.LBB0_285
.LBB0_255:
	testb	$2, 41(%rbx)
	je	.LBB0_260
# BB#256:
	cmpl	$200, 140(%rsp)         # 4-byte Folded Reload
	jb	.LBB0_260
# BB#257:
	movq	56(%rbx), %rax
	movl	(%rax), %eax
	andl	$2, %eax
	je	.LBB0_260
# BB#258:
	movq	88(%rsp), %rax          # 8-byte Reload
	decq	%rax
	leaq	(%rax,%rax,8), %rcx
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	12(%rax,%rcx,4), %eax
	cmpl	$4050, %eax             # imm = 0xFD2
	jb	.LBB0_260
# BB#259:
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	8(%rdx,%rcx,4), %ecx
	cmpl	%ecx, 144(%rsp)         # 4-byte Folded Reload
	jae	.LBB0_293
.LBB0_260:
	testb	$2, 41(%rbx)
	je	.LBB0_265
# BB#261:
	cmpl	$2, 88(%rsp)            # 4-byte Folded Reload
	jb	.LBB0_265
# BB#262:
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_265
# BB#263:
	movq	56(%rbx), %rax
	movl	(%rax), %eax
	andl	$4, %eax
	je	.LBB0_265
# BB#264:
	movq	88(%rsp), %rax          # 8-byte Reload
	decq	%rax
	leaq	(%rax,%rax,8), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, 16(%rcx,%rax,4)
	js	.LBB0_287
.LBB0_265:                              # %.thread2666
	cmpl	$3, 88(%rsp)            # 4-byte Folded Reload
	jb	.LBB0_347
# BB#266:                               # %.thread2666
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_347
# BB#267:                               # %.thread2666
	cmpb	$0, 160(%rsp)           # 1-byte Folded Reload
	je	.LBB0_347
# BB#268:
	cmpl	$12, 88(%rsp)           # 4-byte Folded Reload
	ja	.LBB0_347
# BB#269:
	cmpl	$2048, 192(%rsp)        # imm = 0x800
	ja	.LBB0_347
# BB#270:
	movzwl	348(%rsp), %eax
	andl	$65534, %eax            # imm = 0xFFFE
	cmpl	$2, %eax
	jne	.LBB0_347
# BB#271:
	movzwl	220(%rsp), %eax
	cmpl	$332, %eax              # imm = 0x14C
	jne	.LBB0_347
# BB#272:
	cmpl	$524288, 352(%rsp)      # imm = 0x80000
	jb	.LBB0_347
# BB#273:
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	12(%rax), %eax
	decl	%eax
	cmpl	$184549375, %eax        # imm = 0xAFFFFFF
	ja	.LBB0_347
# BB#274:
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	8(%rax), %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_346
# BB#275:                               # %cli_seeksect.exit
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	12(%rax), %edi
	callq	cli_malloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_939
# BB#276:
	movq	40(%rsp), %r14          # 8-byte Reload
	movl	12(%r14), %edx
	movl	%ebp, %edi
	movq	%r13, %rsi
	callq	cli_readn
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	12(%r14), %eax
	jne	.LBB0_696
# BB#277:                               # %.preheader2822
	cmpl	$5, %eax
	jne	.LBB0_505
# BB#278:                               # %._crit_edge2949.thread
	movq	%r13, %rdi
	callq	free
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	jmp	.LBB0_347
.LBB0_279:
	movl	$.L.str.103, %edi
	jmp	.LBB0_281
.LBB0_280:
	movl	$.L.str.102, %edi
.LBB0_281:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_283
# BB#282:
	movq	$.L.str.4, (%rax)
.LBB0_283:
	movq	168(%rsp), %rdi         # 8-byte Reload
.LBB0_284:                              # %.thread2694
	callq	free
.LBB0_285:                              # %.thread2694
	movq	40(%rsp), %rdi          # 8-byte Reload
.LBB0_286:                              # %.thread2694
	callq	free
	movl	$1, %r13d
	jmp	.LBB0_23
.LBB0_287:
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	24(%rdx,%rax,4), %ecx
	movl	12(%rdx,%rax,4), %r14d
	movl	32(%rdx,%rax,4), %r15d
	cmpl	%r15d, %r14d
	movl	%r14d, %esi
	cmovbl	%r15d, %esi
	movzbl	%cl, %edx
	cmpl	$236, %edx
	jne	.LBB0_339
# BB#288:
	cmpl	$24876, %ecx            # imm = 0x612C
	jb	.LBB0_339
# BB#289:
	cmpl	$24876, %esi            # imm = 0x612C
	jb	.LBB0_339
# BB#290:
	cmpl	$28672, %esi            # imm = 0x7000
	movl	$28672, %ecx            # imm = 0x7000
	cmovbl	%esi, %ecx
	subl	%ecx, %esi
	movq	40(%rsp), %rcx          # 8-byte Reload
	addl	8(%rcx,%rax,4), %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	leaq	4768(%rsp), %rsi
	movl	$4096, %edx             # imm = 0x1000
	movl	%ebp, %edi
	callq	cli_readn
	cmpl	$4096, %eax             # imm = 0x1000
	jne	.LBB0_265
# BB#291:
	leaq	4768(%rsp), %rdi
	movl	$4091, %esi             # imm = 0xFFB
	movl	$.L.str.113, %edx
	movl	$5, %ecx
	callq	cli_memstr
	testq	%rax, %rax
	je	.LBB0_265
# BB#292:
	cmpl	%r15d, %r14d
	movl	$.L.str.114, %eax
	movl	$.L.str.115, %ecx
	jmp	.LBB0_345
.LBB0_293:
	movq	144(%rsp), %rdx         # 8-byte Reload
	leal	4050(%rdx), %edx
	cmpl	%ecx, %edx
	jbe	.LBB0_260
# BB#294:
	addl	%eax, %ecx
	cmpl	%ecx, %edx
	ja	.LBB0_260
# BB#295:
	cmpb	$-100, 673(%rsp)
	jne	.LBB0_260
# BB#296:
	cmpb	$96, 674(%rsp)
	jne	.LBB0_260
# BB#297:
	leaq	4768(%rsp), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movabsq	$1411772980527360, %rax # imm = 0x5040003020100
	movq	%rax, 4768(%rsp)
	movl	$134676486, 4776(%rsp)  # imm = 0x8070006
	leaq	675(%rsp), %r15
	movl	$.L.str.108, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-1, 212(%rsp)          # 4-byte Folded Spill
	movl	$65535, 208(%rsp)       # 4-byte Folded Spill
                                        # imm = 0xFFFF
	movl	$197, %r13d
	movb	$-1, 200(%rsp)          # 1-byte Folded Spill
	movb	$-1, 120(%rsp)          # 1-byte Folded Spill
	jmp	.LBB0_298
.LBB0_299:                              #   in Loop: Header=BB0_298 Depth=1
	movl	$14, %r12d
	cmpl	$7, %r13d
	jl	.LBB0_328
# BB#300:                               #   in Loop: Header=BB0_298 Depth=1
	leaq	1(%r15), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	leal	-1(%r13), %ecx
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	xorl	%r12d, %r12d
	cmpb	$7, %al
	ja	.LBB0_326
# BB#301:                               #   in Loop: Header=BB0_298 Depth=1
	movzbl	(%r15), %r14d
	movzbl	%r14b, %ecx
	movzbl	%al, %edx
	jmpq	*.LJTI0_2(,%rdx,8)
.LBB0_302:                              #   in Loop: Header=BB0_298 Depth=1
	movl	%r14d, %ecx
	addb	$127, %cl
	cmpb	$62, %cl
	ja	.LBB0_333
# BB#303:                               #   in Loop: Header=BB0_298 Depth=1
	movzbl	%cl, %ecx
	movabsq	$8610882487532388352, %rdx # imm = 0x7780000000000000
	btq	%rcx, %rdx
	jae	.LBB0_331
# BB#304:                               #   in Loop: Header=BB0_298 Depth=1
	movl	$4, %r12d
	cmpb	$3, %al
	jne	.LBB0_335
# BB#305:                               #   in Loop: Header=BB0_298 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpl	$4050, (%rax)           # imm = 0xFD2
	jne	.LBB0_335
# BB#306:                               #   in Loop: Header=BB0_298 Depth=1
	leal	-6(%r13), %eax
	movl	%eax, 208(%rsp)         # 4-byte Spill
	addb	$72, %r14b
	incq	64(%rsp)                # 8-byte Folded Spill
	movzbl	%r14b, %esi
	movl	$.L.str.109, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	%r14b, 200(%rsp)        # 1-byte Spill
	movb	$4, %r14b
	jmp	.LBB0_335
.LBB0_307:                              #   in Loop: Header=BB0_298 Depth=1
	cmpb	$-24, %r14b
	jne	.LBB0_318
# BB#308:                               #   in Loop: Header=BB0_298 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	cmpl	$254, %eax
	ja	.LBB0_318
# BB#309:                               #   in Loop: Header=BB0_298 Depth=1
	movzbl	%al, %eax
	leal	4(%rax), %ecx
	movl	48(%rsp), %edx          # 4-byte Reload
	subl	%ecx, %edx
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	4(%rcx,%rax), %r15
	incq	64(%rsp)                # 8-byte Folded Spill
	movl	%edx, %r13d
	jmp	.LBB0_328
.LBB0_310:                              #   in Loop: Header=BB0_298 Depth=1
	movl	%r14d, %eax
	andb	$-8, %al
	cmpb	$88, %al
	jne	.LBB0_318
# BB#311:                               #   in Loop: Header=BB0_298 Depth=1
	addb	$-88, %r14b
	movb	$4, 120(%rsp)           # 1-byte Folded Spill
	cmpb	$4, %r14b
	je	.LBB0_318
# BB#312:                               #   in Loop: Header=BB0_298 Depth=1
	movzbl	%r14b, %esi
	incq	64(%rsp)                # 8-byte Folded Spill
	xorl	%r12d, %r12d
	movl	$.L.str.110, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	48(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r13d
	movb	%r14b, 120(%rsp)        # 1-byte Spill
	jmp	.LBB0_327
.LBB0_313:                              #   in Loop: Header=BB0_298 Depth=1
	incq	64(%rsp)                # 8-byte Folded Spill
	cmpb	$62, %r14b
	je	.LBB0_326
.LBB0_314:                              #   in Loop: Header=BB0_298 Depth=1
	cmpb	$-128, %r14b
	jne	.LBB0_318
# BB#315:                               #   in Loop: Header=BB0_298 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax), %eax
	movzbl	120(%rsp), %ecx         # 1-byte Folded Reload
	addl	$176, %ecx
	cmpl	%ecx, %eax
	jne	.LBB0_318
# BB#316:                               #   in Loop: Header=BB0_298 Depth=1
	addq	$7, %r15
	addl	$-7, %r13d
	incq	64(%rsp)                # 8-byte Folded Spill
	movl	48(%rsp), %eax          # 4-byte Reload
	movl	%eax, 212(%rsp)         # 4-byte Spill
	jmp	.LBB0_328
.LBB0_317:                              #   in Loop: Header=BB0_298 Depth=1
	movzbl	120(%rsp), %eax         # 1-byte Folded Reload
	addl	$72, %eax
	cmpl	%eax, %ecx
	je	.LBB0_325
.LBB0_318:                              #   in Loop: Header=BB0_298 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movb	$8, (%rax)
	jmp	.LBB0_326
.LBB0_319:                              #   in Loop: Header=BB0_298 Depth=1
	movzbl	200(%rsp), %eax         # 1-byte Folded Reload
	addl	$72, %eax
	cmpl	%eax, %ecx
	jne	.LBB0_324
# BB#320:                               #   in Loop: Header=BB0_298 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpb	$117, (%rax)
	jne	.LBB0_324
# BB#321:                               #   in Loop: Header=BB0_298 Depth=1
	movsbl	2(%r15), %ecx
	movl	48(%rsp), %eax          # 4-byte Reload
	subl	%ecx, %eax
	cmpl	212(%rsp), %eax         # 4-byte Folded Reload
	jl	.LBB0_324
# BB#322:                               #   in Loop: Header=BB0_298 Depth=1
	addl	$-3, %eax
	cmpl	208(%rsp), %eax         # 4-byte Folded Reload
	jg	.LBB0_324
# BB#323:                               #   in Loop: Header=BB0_298 Depth=1
	movq	(%rbx), %rax
	movq	$.L.str.111, (%rax)
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	$1, %r12d
	jmp	.LBB0_326
.LBB0_324:                              #   in Loop: Header=BB0_298 Depth=1
	xorl	%r12d, %r12d
	movl	$.L.str.112, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_325:                              #   in Loop: Header=BB0_298 Depth=1
	incq	64(%rsp)                # 8-byte Folded Spill
.LBB0_326:                              #   in Loop: Header=BB0_298 Depth=1
	movl	48(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r13d
.LBB0_327:                              #   in Loop: Header=BB0_298 Depth=1
	movq	56(%rsp), %r15          # 8-byte Reload
.LBB0_328:                              #   in Loop: Header=BB0_298 Depth=1
	movl	%r12d, %eax
	andb	$15, %al
	je	.LBB0_298
	jmp	.LBB0_329
.LBB0_331:                              #   in Loop: Header=BB0_298 Depth=1
	testq	%rcx, %rcx
	jne	.LBB0_333
# BB#332:                               #   in Loop: Header=BB0_298 Depth=1
	addq	$6, %r15
	addl	$-6, %r13d
	xorl	%r12d, %r12d
	jmp	.LBB0_328
.LBB0_333:                              #   in Loop: Header=BB0_298 Depth=1
	movl	%r14d, %eax
	addb	$-72, %al
	cmpb	$7, %al
	ja	.LBB0_338
# BB#334:                               #   in Loop: Header=BB0_298 Depth=1
	xorl	%r12d, %r12d
	cmpb	$4, %al
	je	.LBB0_338
.LBB0_335:                              #   in Loop: Header=BB0_298 Depth=1
	andb	$7, %r14b
	cmpb	120(%rsp), %r14b        # 1-byte Folded Reload
	je	.LBB0_338
# BB#336:                               #   in Loop: Header=BB0_298 Depth=1
	cmpb	200(%rsp), %r14b        # 1-byte Folded Reload
	je	.LBB0_338
# BB#337:                               #   in Loop: Header=BB0_298 Depth=1
	movl	%r12d, %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	addq	%rax, %rcx
	movl	48(%rsp), %eax          # 4-byte Reload
	subl	%r12d, %eax
	xorl	%r12d, %r12d
	movl	%eax, %r13d
	movq	%rcx, %r15
	jmp	.LBB0_328
.LBB0_338:                              #   in Loop: Header=BB0_298 Depth=1
	incq	64(%rsp)                # 8-byte Folded Spill
	xorl	%r12d, %r12d
	jmp	.LBB0_328
.LBB0_298:                              # =>This Inner Loop Header: Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax), %eax
	cmpb	$8, %al
	jne	.LBB0_299
	jmp	.LBB0_260
.LBB0_339:
	cmpl	$237, %edx
	jne	.LBB0_265
# BB#340:
	cmpl	$28672, %ecx            # imm = 0x7000
	jb	.LBB0_265
# BB#341:
	cmpl	$28672, %esi            # imm = 0x7000
	jb	.LBB0_265
# BB#342:
	cmpl	$32768, %esi            # imm = 0x8000
	movl	$32768, %ecx            # imm = 0x8000
	cmovbl	%esi, %ecx
	subl	%ecx, %esi
	movq	40(%rsp), %rcx          # 8-byte Reload
	addl	8(%rcx,%rax,4), %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	leaq	4768(%rsp), %rsi
	movl	$4096, %edx             # imm = 0x1000
	movl	%ebp, %edi
	callq	cli_readn
	cmpl	$4096, %eax             # imm = 0x1000
	jne	.LBB0_265
# BB#343:
	leaq	4768(%rsp), %rdi
	movl	$4091, %esi             # imm = 0xFFB
	movl	$.L.str.116, %edx
	movl	$5, %ecx
	callq	cli_memstr
	testq	%rax, %rax
	je	.LBB0_265
# BB#344:
	cmpl	%r15d, %r14d
	movl	$.L.str.117, %eax
	movl	$.L.str.118, %ecx
.LBB0_345:                              # %.thread2694
	cmovbq	%rax, %rcx
	movq	(%rbx), %rax
	movq	%rcx, (%rax)
	jmp	.LBB0_285
.LBB0_346:                              # %cli_seeksect.exit.thread2668
	movl	$.L.str.259, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_347:                              # %.critedge52
	movq	56(%rbx), %rax
	movzwl	(%rax), %eax
	testw	$8288, %ax              # imm = 0x2060
	je	.LBB0_352
# BB#348:                               # %.preheader2821
	movq	88(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	decl	%eax
	je	.LBB0_365
# BB#349:                               # %.lr.ph2932.preheader
	movl	%eax, %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	48(%rcx), %r15
	xorl	%ecx, %ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
.LBB0_350:                              # %.lr.ph2932
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, -36(%r15)
	je	.LBB0_353
.LBB0_351:                              #   in Loop: Header=BB0_350 Depth=1
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rdx
	incq	%rdx
	addq	$36, %r15
	movq	%rdx, %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	cmpq	%rax, %rdx
	jb	.LBB0_350
	jmp	.LBB0_352
.LBB0_353:                              #   in Loop: Header=BB0_350 Depth=1
	cmpl	$0, -44(%r15)
	je	.LBB0_351
# BB#354:                               #   in Loop: Header=BB0_350 Depth=1
	cmpl	$0, (%r15)
	je	.LBB0_351
# BB#355:                               #   in Loop: Header=BB0_350 Depth=1
	cmpl	$0, -8(%r15)
	je	.LBB0_351
# BB#356:
	movl	$.L.str.121, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpb	$-23, 672(%rsp)
	jne	.LBB0_558
# BB#357:
	cmpl	$16, 140(%rsp)          # 4-byte Folded Reload
	jb	.LBB0_558
# BB#358:
	movq	56(%rbx), %rax
	movl	$8192, %ecx             # imm = 0x2000
	andl	(%rax), %ecx
	je	.LBB0_558
# BB#359:
	movl	673(%rsp), %esi
	movq	104(%rsp), %rax         # 8-byte Reload
	leal	(%rsi,%rax), %r12d
	cmpl	$335, %r12d             # imm = 0x14F
	je	.LBB0_361
# BB#360:
	cmpl	$339, %r12d             # imm = 0x153
	jne	.LBB0_558
.LBB0_361:
	movq	104(%rsp), %rdx         # 8-byte Reload
	leal	5(%rsi,%rdx), %r13d
	movl	$.L.str.122, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r13d, %ecx
	callq	cli_dbgmsg
	movl	%r13d, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_488
# BB#362:
	leaq	4768(%rsp), %rsi
	movl	$176, %edx
	movl	%ebp, %edi
	callq	read
	movq	%rax, %rcx
	cmpq	$176, %rcx
	jne	.LBB0_504
# BB#363:
	cmpl	$340, %r13d             # imm = 0x154
	jne	.LBB0_547
# BB#364:
	movl	$.L.str.125, %edi
	jmp	.LBB0_548
.LBB0_352:                              # %.thread2675.loopexit
	xorl	%r12d, %r12d
	jmp	.LBB0_366
.LBB0_365:
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
.LBB0_366:
	xorl	%r13d, %r13d
	cmpl	$167, 140(%rsp)         # 4-byte Folded Reload
	jbe	.LBB0_812
.LBB0_367:
	movl	%r13d, %eax
	orl	184(%rsp), %eax         # 4-byte Folded Reload
	je	.LBB0_412
# BB#368:
	cmpl	$3, 88(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_376
# BB#369:
	cmpl	$0, 184(%rsp)           # 4-byte Folded Reload
	je	.LBB0_376
# BB#370:
	cmpb	$-66, 672(%rsp)
	jne	.LBB0_412
# BB#371:
	movl	673(%rsp), %eax
	subl	308(%rsp), %eax
	cmpl	116(%rsp), %eax         # 4-byte Folded Reload
	jbe	.LBB0_412
# BB#372:
	cmpb	$-83, 677(%rsp)
	jne	.LBB0_374
# BB#373:
	cmpb	$80, 678(%rsp)
	je	.LBB0_382
.LBB0_374:
	cmpb	$-1, 677(%rsp)
	jne	.LBB0_412
# BB#375:
	cmpb	$54, 678(%rsp)
	je	.LBB0_382
	jmp	.LBB0_412
.LBB0_376:
	cmpl	$0, 184(%rsp)           # 4-byte Folded Reload
	sete	%al
	cmpl	$2, 88(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_412
# BB#377:
	testb	%al, %al
	je	.LBB0_412
# BB#378:
	movb	672(%rsp), %al
	cmpb	$-66, %al
	je	.LBB0_384
# BB#379:
	cmpb	$96, %al
	jne	.LBB0_412
# BB#380:
	cmpb	$-24, 673(%rsp)
	jne	.LBB0_412
# BB#381:
	cmpl	$9, 674(%rsp)
	jne	.LBB0_412
.LBB0_382:                              # %.critedge61
	movl	$0, 64(%rsp)            # 4-byte Folded Spill
	movl	$.L.str.140, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %ecx
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	movl	40(%rax), %r14d
	cmpl	$0, 184(%rsp)           # 4-byte Folded Reload
	je	.LBB0_389
# BB#383:
	movl	$.L.str.141, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	76(%rcx), %eax
	movl	(%rcx), %edx
	movl	28(%rcx), %r15d
	addl	32(%rcx), %r15d
	movl	308(%rsp), %ecx
	movl	%edx, 64(%rsp)          # 4-byte Spill
	addl	%edx, %ecx
	jmp	.LBB0_390
.LBB0_384:
	movl	673(%rsp), %eax
	subl	308(%rsp), %eax
	cmpl	116(%rsp), %eax         # 4-byte Folded Reload
	jae	.LBB0_412
# BB#385:
	testl	%eax, %eax
	je	.LBB0_412
# BB#386:
	cmpb	$-83, 677(%rsp)
	jne	.LBB0_412
# BB#387:
	cmpb	$-117, 678(%rsp)
	jne	.LBB0_412
# BB#388:
	cmpb	$-8, 679(%rsp)
	je	.LBB0_382
	jmp	.LBB0_412
.LBB0_389:
	movl	$.L.str.142, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	36(%rcx), %eax
	movl	64(%rcx), %r15d
	movl	%eax, %ecx
	subl	%r15d, %ecx
.LBB0_390:
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	addl	56(%rsp), %r14d         # 4-byte Folded Reload
	addl	%eax, %r14d
	movl	%r14d, 84(%rsp)
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_395
# BB#391:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB0_395
# BB#392:
	cmpl	%r15d, %r14d
	movl	%r15d, %esi
	cmoval	%r14d, %esi
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	68(%rax), %eax
	cmpl	%eax, %esi
	cmovbel	%eax, %esi
	cmpq	%rdx, %rsi
	jbe	.LBB0_395
# BB#393:
	xorl	%r13d, %r13d
	movl	$.L.str.143, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	testb	$1, 41(%rbx)
	je	.LBB0_23
# BB#394:
	movq	(%rbx), %rax
	movq	$.L.str.144, (%rax)
	movl	$1, %r13d
	jmp	.LBB0_23
.LBB0_395:
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	36(%rax), %eax
	subl	64(%rsp), %eax          # 4-byte Folded Reload
	cmpl	%r14d, %eax
	ja	.LBB0_411
# BB#396:
	movl	%r14d, %ecx
	movq	40(%rsp), %rdx          # 8-byte Reload
	subl	68(%rdx), %ecx
	cmpl	%ecx, %eax
	ja	.LBB0_411
# BB#397:
	cmpl	$0, 184(%rsp)           # 4-byte Folded Reload
	je	.LBB0_400
# BB#398:
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	72(%rcx), %eax
	subl	(%rcx), %eax
	cmpl	%r14d, %eax
	ja	.LBB0_411
# BB#399:
	movl	%r14d, %ecx
	subl	%r15d, %ecx
	cmpl	%ecx, %eax
	ja	.LBB0_411
.LBB0_400:
	cmpl	%r15d, %r14d
	jb	.LBB0_411
# BB#401:
	movl	%r14d, %edi
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, 56(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_939
# BB#402:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	movl	%r15d, %r14d
	movl	%ebp, %edi
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	callq	read
	cmpq	%r14, %rax
	jne	.LBB0_581
# BB#403:
	cmpl	$0, 184(%rsp)           # 4-byte Folded Reload
	je	.LBB0_405
# BB#404:
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %eax
	movl	72(%rcx), %edi
	movq	56(%rsp), %rsi          # 8-byte Reload
	addq	%rsi, %rdi
	subq	%rax, %rdi
	movq	%r14, %rdx
	callq	memmove
.LBB0_405:
	movq	40(%rsp), %r14          # 8-byte Reload
	movl	64(%r14), %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	movl	36(%r14), %eax
	movl	68(%r14), %edx
	movq	56(%rsp), %rsi          # 8-byte Reload
	addq	%rax, %rsi
	movl	64(%rsp), %eax          # 4-byte Reload
	subq	%rax, %rsi
	movl	%ebp, %edi
	callq	read
	movl	68(%r14), %ecx
	cmpq	%rcx, %rax
	jne	.LBB0_744
# BB#406:
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_965
# BB#407:
	movl	$578, %esi              # imm = 0x242
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	open
	movl	%eax, %r15d
	testl	%r15d, %r15d
	js	.LBB0_870
# BB#408:
	movl	84(%rsp), %edx
	movl	308(%rsp), %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %ecx
	movl	%r15d, 16(%rsp)
	movl	%ecx, 8(%rsp)
	movl	%eax, (%rsp)
	leaq	672(%rsp), %rcx
	movl	184(%rsp), %edi         # 4-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	movl	48(%rsp), %r8d          # 4-byte Reload
	movq	144(%rsp), %r9          # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	callq	unupack
	cmpl	$1, %eax
	jne	.LBB0_873
# BB#409:
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB0_922
# BB#410:
	movl	$.L.str.149, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	jmp	.LBB0_923
.LBB0_411:
	movl	$.L.str.145, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_412:                              # %.critedge63
	testb	%r12b, %r12b
	je	.LBB0_459
.LBB0_413:
	movq	56(%rbx), %rax
	movl	(%rax), %eax
	testb	$64, %al
	je	.LBB0_459
# BB#414:
	cmpb	$-121, 672(%rsp)
	jne	.LBB0_459
# BB#415:
	cmpb	$37, 673(%rsp)
	jne	.LBB0_459
# BB#416:
	movq	72(%rsp), %rax          # 8-byte Reload
	leal	1(%rax), %ecx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rcx,8), %r15
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	12(%rcx,%r15,4), %r12d
	movl	%eax, %eax
	leaq	(%rax,%rax,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	4(%rcx,%rax,4), %edx
	movl	%edx, 84(%rsp)
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_422
# BB#417:
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB0_422
# BB#418:
	movq	%r12, %rax
	cmpl	%eax, %edx
	movl	%eax, %esi
	cmoval	%edx, %esi
	cmpq	%rcx, %rsi
	ja	.LBB0_419
.LBB0_422:
	cmpl	$26, %r12d
	jb	.LBB0_457
# BB#423:
	cmpl	%r12d, %edx
	jbe	.LBB0_457
# BB#424:
	movl	674(%rsp), %r14d
	subl	308(%rsp), %r14d
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r15,4), %eax
	cmpl	%eax, %r14d
	jb	.LBB0_458
# BB#425:
	leal	4(%r14), %ecx
	cmpl	%eax, %ecx
	jbe	.LBB0_458
# BB#426:
	addl	%r12d, %eax
	cmpl	%eax, %ecx
	ja	.LBB0_458
# BB#427:
	movq	%r12, %rdi
	callq	cli_malloc
	movq	%rax, 48(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_939
# BB#428:
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	12(%rax,%r15,4), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	cmpl	$0, (%rax)
	je	.LBB0_578
# BB#429:
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	8(%rax,%r15,4), %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_577
# BB#430:                               # %cli_seeksect.exit2596
	movl	%ebp, %edi
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	movq	%rdx, 184(%rsp)         # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_readn
	cmpl	184(%rsp), %eax         # 4-byte Folded Reload
	jne	.LBB0_578
# BB#431:
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r15,4), %r15
	movl	(%r15), %eax
	cmpl	%eax, %r14d
	jb	.LBB0_731
# BB#432:
	movl	%r14d, %ecx
	movq	48(%rsp), %rdx          # 8-byte Reload
	addq	%rdx, %rcx
	subq	%rax, %rcx
	cmpq	%rdx, %rcx
	jb	.LBB0_731
# BB#433:
	leaq	4(%rcx), %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r12), %rdx
	cmpq	%rdx, %rsi
	ja	.LBB0_731
# BB#434:
	cmpq	48(%rsp), %rsi          # 8-byte Folded Reload
	jbe	.LBB0_731
# BB#435:
	movl	(%rcx), %esi
	movl	308(%rsp), %r9d
	subl	%r9d, %esi
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %edi
	cmpl	$4, %edi
	jb	.LBB0_840
# BB#436:
	cmpl	%eax, %esi
	jb	.LBB0_840
# BB#437:
	leal	4(%rsi), %ecx
	leal	(%rdi,%rax), %r8d
	cmpl	%r8d, %ecx
	ja	.LBB0_840
# BB#438:
	cmpl	%eax, %ecx
	jbe	.LBB0_840
# BB#439:
	cmpl	$32, %r12d
	jb	.LBB0_910
# BB#440:
	movq	%rax, %rcx
	negq	%rcx
	movl	%esi, %r10d
	movq	48(%rsp), %rsi          # 8-byte Reload
	addq	%rsi, %r10
	addq	%rcx, %r10
	cmpq	%rsi, %r10
	jb	.LBB0_910
# BB#441:
	leaq	32(%r10), %rcx
	cmpq	%rdx, %rcx
	ja	.LBB0_910
# BB#442:
	cmpq	48(%rsp), %rcx          # 8-byte Folded Reload
	jbe	.LBB0_910
# BB#443:
	movl	(%r10), %r14d
	subl	%r9d, %r14d
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	movl	(%rcx,%rdx,4), %edx
	cmpl	%edx, %r14d
	jne	.LBB0_967
# BB#444:
	movl	4(%r10), %ecx
	subl	%r9d, %ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%eax, %ecx
	jb	.LBB0_996
# BB#445:
	cmpl	%edi, %ecx
	jae	.LBB0_996
# BB#446:
	cmpl	$16, %edi
	jb	.LBB0_1007
# BB#447:
	movl	16(%r10), %edx
	subl	%r9d, %edx
	cmpl	%eax, %edx
	jb	.LBB0_1007
# BB#448:
	leal	16(%rdx), %ecx
	cmpl	%r8d, %ecx
	ja	.LBB0_1007
# BB#449:
	cmpl	%eax, %ecx
	jbe	.LBB0_1007
# BB#450:
	movl	$12, %ecx
	subl	%eax, %ecx
	addl	%edx, %ecx
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%rcx), %esi
	subl	%r9d, %esi
	movl	$.L.str.163, %edi
	xorl	%eax, %eax
	movl	%esi, 120(%rsp)         # 4-byte Spill
	callq	cli_dbgmsg
	movl	84(%rsp), %edi
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, 64(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_993
# BB#451:
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, 96(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_1039
# BB#452:
	movl	$578, %esi              # imm = 0x242
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	open
	movl	%eax, 160(%rsp)         # 4-byte Spill
	testl	%eax, %eax
	js	.LBB0_1040
# BB#453:
	movl	(%r15), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	subl	%ecx, %r12d
	subl	%eax, %ecx
	movq	48(%rsp), %rdi          # 8-byte Reload
	addq	%rcx, %rdi
	addl	%eax, %r12d
	movl	84(%rsp), %ecx
	movl	308(%rsp), %r9d
	movl	160(%rsp), %eax         # 4-byte Reload
	movl	%eax, 8(%rsp)
	movl	120(%rsp), %eax         # 4-byte Reload
	movl	%eax, (%rsp)
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r12d, %edx
	movl	%r14d, %r8d
	callq	unfsg_200
	testl	%eax, %eax
	je	.LBB0_1042
# BB#454:
	cmpl	$1, %eax
	jne	.LBB0_1043
# BB#455:
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB0_1046
# BB#456:
	movl	$.L.str.165, %edi
	xorl	%eax, %eax
	movq	96(%rsp), %rsi          # 8-byte Reload
	callq	cli_dbgmsg
	jmp	.LBB0_1047
.LBB0_457:
	xorl	%r13d, %r13d
	movl	$.L.str.154, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	cli_dbgmsg
	jmp	.LBB0_244
.LBB0_458:
	movl	$.L.str.155, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	cli_dbgmsg
.LBB0_459:                              # %.critedge71
	testl	%r13d, %r13d
	je	.LBB0_586
.LBB0_460:
	movq	56(%rbx), %rax
	movl	(%rax), %eax
	movb	672(%rsp), %cl
	testb	$64, %al
	je	.LBB0_479
# BB#461:
	cmpb	$-66, %cl
	jne	.LBB0_479
# BB#462:
	movl	673(%rsp), %edi
	subl	308(%rsp), %edi
	cmpl	116(%rsp), %edi         # 4-byte Folded Reload
	jae	.LBB0_585
# BB#463:
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rdx
	leal	1(%rdx), %eax
	leaq	(%rax,%rax,8), %r12
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	12(%rcx,%r12,4), %r14d
	movl	%edx, %eax
	leaq	(%rax,%rax,8), %r15
	movl	4(%rcx,%r15,4), %edx
	movl	%edx, 84(%rsp)
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_466
# BB#464:
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB0_466
# BB#465:
	cmpl	%r14d, %edx
	movl	%r14d, %esi
	cmoval	%edx, %esi
	cmpq	%rcx, %rsi
	jbe	.LBB0_466
.LBB0_419:
	xorl	%r13d, %r13d
	movl	$.L.str.152, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%rcx, %rdx
.LBB0_420:
	callq	cli_dbgmsg
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	testb	$1, 41(%rbx)
	je	.LBB0_23
# BB#421:
	movq	(%rbx), %rax
	movq	$.L.str.153, (%rax)
	movl	$1, %r13d
	jmp	.LBB0_23
.LBB0_466:
	cmpl	$26, %r14d
	jb	.LBB0_474
# BB#467:
	cmpl	%r14d, %edx
	jbe	.LBB0_474
# BB#468:
	cmpl	156(%rsp), %edi         # 4-byte Folded Reload
	jae	.LBB0_475
# BB#469:
	movl	%edi, %ecx
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	cmpq	128(%rsp), %rcx         # 8-byte Folded Reload
	setae	%al
	cmovbl	%edi, %r13d
	testl	%eax, %eax
	jne	.LBB0_476
	jmp	.LBB0_470
.LBB0_474:
	xorl	%r13d, %r13d
	movl	$.L.str.154, %edi
	jmp	.LBB0_627
.LBB0_475:
	xorl	%r13d, %r13d
	movl	$1, %eax
	testl	%eax, %eax
	je	.LBB0_470
.LBB0_476:                              # %cli_rawaddr.exit2602
	testl	%r13d, %r13d
	jne	.LBB0_470
# BB#477:
	movl	$.L.str.169, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_478:                              # %.critedge80..critedge80.thread2707_crit_edge
	movq	56(%rbx), %rax
	movl	(%rax), %eax
	movb	672(%rsp), %cl
.LBB0_479:                              # %.critedge80.thread2707
	testb	$64, %al
	je	.LBB0_585
# BB#480:                               # %.critedge80.thread2707
	cmpb	$-69, %cl
	jne	.LBB0_585
# BB#481:
	movl	673(%rsp), %edx
	movl	308(%rsp), %ecx
	subl	%ecx, %edx
	cmpl	116(%rsp), %edx         # 4-byte Folded Reload
	jae	.LBB0_585
# BB#482:
	cmpb	$-65, 677(%rsp)
	jne	.LBB0_585
# BB#483:
	cmpb	$-66, 682(%rsp)
	jne	.LBB0_585
# BB#484:
	movq	72(%rsp), %rsi          # 8-byte Reload
	leal	1(%rsi), %esi
	leaq	(%rsi,%rsi,8), %r15
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	(%rsi,%r15,4), %esi
	movq	104(%rsp), %rdi         # 8-byte Reload
	movl	%edi, %r14d
	subl	%esi, %r14d
	jb	.LBB0_585
# BB#485:
	addl	$-224, %esi
	cmpl	%esi, %r14d
	jbe	.LBB0_585
# BB#486:
	cmpl	156(%rsp), %edx         # 4-byte Folded Reload
	jae	.LBB0_562
# BB#487:
	movl	%edx, %esi
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	cmpq	128(%rsp), %rsi         # 8-byte Folded Reload
	setae	%al
	cmovbl	%edx, %r12d
	jmp	.LBB0_563
.LBB0_470:
	movl	%r13d, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	8(%rax,%r12,4), %eax
	subl	%r13d, %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_490
# BB#471:
	movq	24(%rax), %rdx
	movl	64(%rsp), %edi          # 4-byte Reload
	testq	%rdx, %rdx
	je	.LBB0_491
# BB#472:
	cmpq	%rdx, %rdi
	jbe	.LBB0_491
# BB#473:
	xorl	%r13d, %r13d
	movl	$.L.str.152, %edi
	xorl	%eax, %eax
	movl	64(%rsp), %esi          # 4-byte Reload
	jmp	.LBB0_420
.LBB0_488:
	movl	$.L.str.123, %edi
.LBB0_489:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_696
.LBB0_490:                              # %._crit_edge3093
	movl	64(%rsp), %edi          # 4-byte Reload
.LBB0_491:
	callq	cli_malloc
	movq	%rax, 56(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_939
# BB#492:
	movl	%ebp, %edi
	movq	56(%rsp), %rsi          # 8-byte Reload
	movl	64(%rsp), %r13d         # 4-byte Reload
	movl	%r13d, %edx
	callq	cli_readn
	cmpl	%r13d, %eax
	jne	.LBB0_546
# BB#493:
	movl	308(%rsp), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	8(%rcx), %ecx
	subl	%eax, %ecx
	movl	%ecx, 184(%rsp)         # 4-byte Spill
	movq	40(%rsp), %rdx          # 8-byte Reload
	subl	(%rdx,%r12,4), %ecx
	jb	.LBB0_559
# BB#494:
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	12(%rdx,%r12,4), %rdx
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	cmpl	(%rdx), %ecx
	jae	.LBB0_559
# BB#495:
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	4(%rcx), %ecx
	subl	%eax, %ecx
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r15,4), %edx
	movl	%ecx, 160(%rsp)         # 4-byte Spill
	cmpl	%edx, %ecx
	jne	.LBB0_743
# BB#496:                               # %.preheader2820
	movl	64(%rsp), %eax          # 4-byte Reload
	addl	$-4, %eax
	movl	%eax, %r13d
	cmpl	$13, %eax
	jb	.LBB0_560
# BB#497:                               # %.lr.ph2930.preheader
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	4(%rax,%r15,4), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	8(%rax,%r12,4), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movl	$12, %r15d
	movl	160(%rsp), %eax         # 4-byte Reload
	xorl	%ecx, %ecx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
.LBB0_498:                              # %.lr.ph2930
                                        # =>This Inner Loop Header: Depth=1
	movl	%r15d, %r15d
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%r15), %r12d
	testl	%r12d, %r12d
	je	.LBB0_855
# BB#499:                               #   in Loop: Header=BB0_498 Depth=1
	decl	%r12d
	subl	308(%rsp), %r12d
	movq	96(%rsp), %rcx          # 8-byte Reload
	leal	1(%rcx), %ecx
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	testw	$4095, %r12w            # imm = 0xFFF
	je	.LBB0_501
# BB#500:                               #   in Loop: Header=BB0_498 Depth=1
	movl	$.L.str.172, %edi
	xorl	%eax, %eax
	movl	64(%rsp), %esi          # 4-byte Reload
	callq	cli_dbgmsg
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	-4(%rax), %eax
.LBB0_501:                              #   in Loop: Header=BB0_498 Depth=1
	subl	%eax, %r12d
	jb	.LBB0_854
# BB#502:                               #   in Loop: Header=BB0_498 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx), %r12d
	jae	.LBB0_854
# BB#503:                               #   in Loop: Header=BB0_498 Depth=1
	addl	$4, %r15d
	cmpl	%r13d, %r15d
	movl	64(%rsp), %ecx          # 4-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	jb	.LBB0_498
	jmp	.LBB0_560
.LBB0_504:
	movl	$.L.str.124, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	movl	%r13d, %edx
	callq	cli_dbgmsg
	jmp	.LBB0_558
.LBB0_329:
	cmpb	$14, %al
	je	.LBB0_260
# BB#330:
	movl	$1, %r13d
	testl	%r12d, %r12d
	jne	.LBB0_23
	jmp	.LBB0_260
.LBB0_505:                              # %.lr.ph2948.split.preheader
	movzbl	160(%rsp), %ecx         # 1-byte Folded Reload
	leaq	(%rcx,%rcx,8), %rcx
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	12(%rdx,%rcx,4), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	1(%rcx), %rsi
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rcx,8), %rcx
	leaq	-24(%rdx,%rcx,4), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	$0, 96(%rsp)            # 4-byte Folded Spill
	xorl	%r14d, %r14d
.LBB0_506:                              # %.lr.ph2948.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_511 Depth 2
                                        #     Child Loop BB0_525 Depth 2
	movl	72(%rsp), %ecx          # 4-byte Reload
	movb	(%r13,%rcx), %cl
	andb	$-2, %cl
	cmpb	$-24, %cl
	jne	.LBB0_509
# BB#507:                               #   in Loop: Header=BB0_506 Depth=1
	movq	72(%rsp), %rsi          # 8-byte Reload
	leal	1(%rsi), %r12d
	movl	(%r13,%r12), %ecx
	movq	40(%rsp), %rdx          # 8-byte Reload
	addl	(%rdx), %esi
	leal	5(%rcx,%rsi), %r15d
	cmpl	156(%rsp), %r15d        # 4-byte Folded Reload
	jae	.LBB0_510
# BB#508:                               # %cli_rawaddr.exit2592
                                        #   in Loop: Header=BB0_506 Depth=1
	movl	%r15d, %ecx
	cmpq	128(%rsp), %rcx         # 8-byte Folded Reload
	jb	.LBB0_516
	jmp	.LBB0_532
.LBB0_509:                              # %.lr.ph2948.split.cli_rawaddr.exit2592.thread_crit_edge
                                        #   in Loop: Header=BB0_506 Depth=1
	movq	72(%rsp), %rcx          # 8-byte Reload
	incl	%ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	jmp	.LBB0_533
.LBB0_510:                              # %.lr.ph.i2587.preheader
                                        #   in Loop: Header=BB0_506 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
.LBB0_511:                              # %.lr.ph.i2587
                                        #   Parent Loop BB0_506 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edi
	testl	%edi, %edi
	je	.LBB0_514
# BB#512:                               #   in Loop: Header=BB0_511 Depth=2
	movl	%r15d, %esi
	subl	-12(%rcx), %esi
	jb	.LBB0_514
# BB#513:                               #   in Loop: Header=BB0_511 Depth=2
	cmpl	%esi, %edi
	ja	.LBB0_515
.LBB0_514:                              # %.backedge.i2589
                                        #   in Loop: Header=BB0_511 Depth=2
	decq	%rdx
	addq	$-36, %rcx
	cmpq	$1, %rdx
	jg	.LBB0_511
	jmp	.LBB0_532
.LBB0_515:                              # %cli_rawaddr.exit2592.thread2669
                                        #   in Loop: Header=BB0_506 Depth=1
	addl	-4(%rcx), %esi
	movl	%esi, %r15d
.LBB0_516:                              #   in Loop: Header=BB0_506 Depth=1
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %ecx
	cmpl	$9, %ecx
	jb	.LBB0_532
# BB#517:                               #   in Loop: Header=BB0_506 Depth=1
	movq	64(%rsp), %rdx          # 8-byte Reload
	movl	-4(%rdx), %edx
	cmpl	%edx, %r15d
	jb	.LBB0_532
# BB#518:                               #   in Loop: Header=BB0_506 Depth=1
	leal	9(%r15), %esi
	cmpl	%edx, %esi
	jbe	.LBB0_532
# BB#519:                               #   in Loop: Header=BB0_506 Depth=1
	addl	%ecx, %edx
	cmpl	%edx, %esi
	ja	.LBB0_532
# BB#520:                               #   in Loop: Header=BB0_506 Depth=1
	testb	$127, 96(%rsp)          # 1-byte Folded Reload
	jne	.LBB0_523
# BB#521:                               #   in Loop: Header=BB0_506 Depth=1
	cmpl	$1280, 96(%rsp)         # 4-byte Folded Reload
                                        # imm = 0x500
	je	.LBB0_916
# BB#522:                               #   in Loop: Header=BB0_506 Depth=1
	movl	96(%rsp), %eax          # 4-byte Reload
	movl	%eax, %esi
	subl	$-128, %esi
	shlq	$2, %rsi
	movq	%r14, %rdi
	callq	cli_realloc2
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_917
.LBB0_523:                              #   in Loop: Header=BB0_506 Depth=1
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	je	.LBB0_529
# BB#524:                               # %.lr.ph2942.preheader
                                        #   in Loop: Header=BB0_506 Depth=1
	movl	96(%rsp), %ecx          # 4-byte Reload
	xorl	%eax, %eax
.LBB0_525:                              # %.lr.ph2942
                                        #   Parent Loop BB0_506 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r14,%rax,4), %edx
	cmpl	%r15d, %edx
	jb	.LBB0_528
# BB#526:                               #   in Loop: Header=BB0_525 Depth=2
	je	.LBB0_530
# BB#527:                               #   in Loop: Header=BB0_525 Depth=2
	movl	%r15d, (%r14,%rax,4)
	movl	%edx, %r15d
.LBB0_528:                              #   in Loop: Header=BB0_525 Depth=2
	incq	%rax
	cmpq	%rcx, %rax
	jb	.LBB0_525
	jmp	.LBB0_531
.LBB0_529:                              #   in Loop: Header=BB0_506 Depth=1
	xorl	%eax, %eax
	movl	$0, 96(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_531
.LBB0_530:                              #   in Loop: Header=BB0_506 Depth=1
	decl	96(%rsp)                # 4-byte Folded Spill
.LBB0_531:                              # %.loopexit
                                        #   in Loop: Header=BB0_506 Depth=1
	movl	%eax, %eax
	movl	%r15d, (%r14,%rax,4)
	incl	96(%rsp)                # 4-byte Folded Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	12(%rax), %eax
.LBB0_532:                              # %cli_rawaddr.exit2592.thread
                                        #   in Loop: Header=BB0_506 Depth=1
                                        # kill: %R12D<def> %R12D<kill> %R12<kill> %R12<def>
	movq	%r12, 72(%rsp)          # 8-byte Spill
.LBB0_533:                              # %cli_rawaddr.exit2592.thread
                                        #   in Loop: Header=BB0_506 Depth=1
	leal	-5(%rax), %ecx
	cmpl	%ecx, 72(%rsp)          # 4-byte Folded Reload
	jb	.LBB0_506
# BB#534:                               # %._crit_edge2949
	movq	%r13, %rdi
	callq	free
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	je	.LBB0_347
.LBB0_535:                              # %.lr.ph2936
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	$.L.str.119, %edi
	xorl	%eax, %eax
	movl	96(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %esi
	callq	cli_dbgmsg
	movl	%r15d, %r15d
	leaq	4768(%rsp), %r13
.LBB0_536:                              # =>This Inner Loop Header: Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	(%r14,%rax,4), %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	movl	$9, %edx
	movl	%ebp, %edi
	movq	%r13, %rsi
	callq	cli_readn
	cmpl	$9, %eax
	jne	.LBB0_544
# BB#537:                               #   in Loop: Header=BB0_536 Depth=1
	movl	4768(%rsp), %eax
	cmpl	$1626114901, %eax       # imm = 0x60EC8B55
	je	.LBB0_542
# BB#538:                               #   in Loop: Header=BB0_536 Depth=1
	cmpb	$-20, 4772(%rsp)
	jne	.LBB0_544
# BB#539:                               #   in Loop: Header=BB0_536 Depth=1
	cmpl	$-2115204267, %eax      # imm = 0x81EC8B55
	je	.LBB0_543
# BB#540:                               #   in Loop: Header=BB0_536 Depth=1
	cmpl	$-2081649835, %eax      # imm = 0x83EC8B55
	jne	.LBB0_544
# BB#541:                               #   in Loop: Header=BB0_536 Depth=1
	cmpb	$96, 4774(%rsp)
	jne	.LBB0_544
	jmp	.LBB0_542
.LBB0_543:                              #   in Loop: Header=BB0_536 Depth=1
	movzbl	4775(%rsp), %eax
	orb	4776(%rsp), %al
	je	.LBB0_542
.LBB0_544:                              #   in Loop: Header=BB0_536 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	cmpq	%r15, %rcx
	jb	.LBB0_536
# BB#545:                               # %._crit_edge2937
	movq	%r14, %rdi
	callq	free
	jmp	.LBB0_347
.LBB0_546:
	movl	$.L.str.170, %edi
	xorl	%eax, %eax
	movl	64(%rsp), %esi          # 4-byte Reload
	callq	cli_dbgmsg
	jmp	.LBB0_746
.LBB0_547:
	movl	$.L.str.126, %edi
.LBB0_548:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	4769(%rsp), %eax
	subl	308(%rsp), %eax
	movl	-12(%r15), %ecx
	movl	%eax, %r14d
	subl	%ecx, %r14d
	jbe	.LBB0_557
# BB#549:
	movl	-4(%r15), %esi
	leal	-4(%rcx,%rsi), %ecx
	cmpl	%ecx, %eax
	jae	.LBB0_557
# BB#550:
	cmpl	$0, (%r15)
	je	.LBB0_696
# BB#551:
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_750
# BB#552:                               # %cli_seeksect.exit2594
	movl	-44(%r15), %edx
	movl	-8(%r15), %esi
	movl	%edx, 84(%rsp)
	movl	$.L.str.128, %edi
	xorl	%eax, %eax
	movl	%esi, 64(%rsp)          # 4-byte Spill
	movl	%r14d, %ecx
	callq	cli_dbgmsg
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_844
# BB#553:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB0_844
# BB#554:
	movl	84(%rsp), %esi
	movl	64(%rsp), %eax          # 4-byte Reload
	cmpl	%esi, %eax
	cmoval	%eax, %esi
	cmpq	%rdx, %rsi
	ja	.LBB0_555
# BB#843:
	movl	84(%rsp), %esi
	addl	64(%rsp), %esi          # 4-byte Folded Reload
	movl	(%r15), %eax
	cmpl	%eax, %esi
	cmovbel	%eax, %esi
	cmpq	%rdx, %rsi
	jbe	.LBB0_844
.LBB0_555:
	xorl	%r13d, %r13d
	movl	$.L.str.129, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	testb	$1, 41(%rbx)
	je	.LBB0_23
# BB#556:
	movq	(%rbx), %rax
	movq	$.L.str.130, (%rax)
	movl	$1, %r13d
	jmp	.LBB0_23
.LBB0_557:
	movl	$.L.str.127, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_558:                              # %..thread2675_crit_edge
	movl	$1, %r13d
	movb	$1, %r12b
	cmpl	$167, 140(%rsp)         # 4-byte Folded Reload
	ja	.LBB0_367
	jmp	.LBB0_812
.LBB0_559:
	movl	$.L.str.161, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_560:                              # %.critedge80..critedge80.thread2707_crit_edge
	movq	56(%rsp), %rdi          # 8-byte Reload
.LBB0_561:                              # %.critedge80..critedge80.thread2707_crit_edge
	callq	free
	jmp	.LBB0_478
.LBB0_562:
	xorl	%r12d, %r12d
	movl	$1, %eax
.LBB0_563:                              # %cli_rawaddr.exit2610
	movl	678(%rsp), %r8d
	movl	683(%rsp), %edi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	12(%rsi,%r15,4), %edx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movl	72(%rsp), %edx          # 4-byte Reload
	leaq	(%rdx,%rdx,8), %r13
	movl	4(%rsi,%r13,4), %edx
	movl	%edx, 84(%rsp)
	testl	%eax, %eax
	je	.LBB0_565
# BB#564:
	movl	$.L.str.169, %edi
	jmp	.LBB0_583
.LBB0_565:
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r15,4), %rsi
	subl	%ecx, %edi
	movl	%edi, 96(%rsp)          # 4-byte Spill
	movl	%edi, %eax
	movq	%rsi, 184(%rsp)         # 8-byte Spill
	subl	(%rsi), %eax
	jb	.LBB0_582
# BB#566:
	movq	40(%rsp), %rsi          # 8-byte Reload
	cmpl	8(%rsi,%r15,4), %eax
	jae	.LBB0_582
# BB#567:
	subl	%ecx, %r8d
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r13,4), %ecx
	movl	%r8d, 64(%rsp)          # 4-byte Spill
	cmpl	%ecx, %r8d
	jne	.LBB0_841
# BB#568:
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_571
# BB#569:
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB0_571
# BB#570:
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpl	%eax, %edx
	movl	%eax, %esi
	cmoval	%edx, %esi
	cmpq	%rcx, %rsi
	ja	.LBB0_419
.LBB0_571:
	cmpl	$26, 56(%rsp)           # 4-byte Folded Reload
	jb	.LBB0_894
# BB#572:
	cmpl	56(%rsp), %edx          # 4-byte Folded Reload
	jbe	.LBB0_894
# BB#573:
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax,%r15,4), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movl	%r12d, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	movq	160(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %eax
	subl	%r12d, %eax
	movl	%eax, 120(%rsp)         # 4-byte Spill
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_928
# BB#574:
	movq	24(%rax), %rdx
	movl	120(%rsp), %edi         # 4-byte Reload
	testq	%rdx, %rdx
	je	.LBB0_929
# BB#575:
	cmpq	%rdx, %rdi
	jbe	.LBB0_929
# BB#576:
	xorl	%r13d, %r13d
	movl	$.L.str.152, %edi
	xorl	%eax, %eax
	movl	120(%rsp), %esi         # 4-byte Reload
	jmp	.LBB0_420
.LBB0_577:                              # %cli_seeksect.exit2596.thread2695
	movl	$.L.str.259, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_578:                              # %cli_seeksect.exit2596.thread
	movl	$.L.str.156, %edi
	xorl	%eax, %eax
	movq	64(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
.LBB0_579:                              # %.thread2694
	callq	cli_dbgmsg
	movq	40(%rsp), %rdi          # 8-byte Reload
.LBB0_580:                              # %.thread2694
	callq	free
	movq	48(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_697
.LBB0_542:
	movq	(%rbx), %rax
	movq	$.L.str.120, (%rax)
	movq	%r14, %rdi
	jmp	.LBB0_284
.LBB0_581:
	movl	$.L.str.146, %edi
	jmp	.LBB0_745
.LBB0_582:
	movl	$.L.str.161, %edi
.LBB0_583:                              # %.critedge85..critedge85.thread_crit_edge
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_584:                              # %.critedge85..critedge85.thread_crit_edge
	movq	56(%rbx), %rax
	movl	(%rax), %eax
.LBB0_585:                              # %.critedge85.thread
	testb	$32, %al
	jne	.LBB0_620
.LBB0_586:                              # %.critedge85.thread2725.thread2779
	cmpl	$199, 140(%rsp)         # 4-byte Folded Reload
	jbe	.LBB0_812
.LBB0_587:
	movb	672(%rsp), %cl
	cmpb	$-72, %cl
	jne	.LBB0_595
# BB#588:
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rax,8), %rax
	movl	308(%rsp), %edx
	movq	40(%rsp), %rsi          # 8-byte Reload
	addl	-36(%rsi,%rax,4), %edx
	movl	$2, %r13d
	cmpl	%edx, 673(%rsp)
	je	.LBB0_592
# BB#589:
	cmpl	$2, 88(%rsp)            # 4-byte Folded Reload
	jb	.LBB0_595
# BB#590:
	cmpb	$-72, %cl
	jne	.LBB0_595
# BB#591:
	movl	308(%rsp), %ecx
	movq	40(%rsp), %rdx          # 8-byte Reload
	addl	-72(%rdx,%rax,4), %ecx
	movl	$1, %r13d
	cmpl	%ecx, 673(%rsp)
	jne	.LBB0_595
.LBB0_592:                              # %select.unfold2754
	movq	56(%rbx), %rax
	testb	$1, 1(%rax)
	je	.LBB0_595
# BB#593:
	movl	$.L.str.199, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_dbgmsg
	cmpl	$373069965, 800(%rsp)   # imm = 0x163C988D
	jne	.LBB0_642
# BB#594:
	movl	$.L.str.200, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_595:                              # %.thread2756
	cmpl	$2, 88(%rsp)            # 4-byte Folded Reload
	jb	.LBB0_600
.LBB0_596:                              # %.thread2756
	movq	56(%rbx), %rax
	movl	$512, %ecx              # imm = 0x200
	andl	(%rax), %ecx
	je	.LBB0_600
# BB#597:
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	-1(%rax), %r13
	leaq	(%r13,%r13,8), %rcx
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%rcx,4), %eax
	cmpl	%eax, 104(%rsp)         # 4-byte Folded Reload
	jb	.LBB0_600
# BB#598:
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	12(%rdx,%rcx,4), %ecx
	leal	-12827(%rax,%rcx), %eax
	cmpl	%eax, 104(%rsp)         # 4-byte Folded Reload
	jae	.LBB0_600
# BB#599:
	leaq	676(%rsp), %rdi
	movl	$.L.str.208, %esi
	movl	$10, %edx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_647
.LBB0_600:
	movq	56(%rbx), %rax
	movl	(%rax), %r14d
	cmpl	$2, 88(%rsp)            # 4-byte Folded Reload
	jb	.LBB0_605
# BB#601:
	movl	%r14d, %eax
	andl	$1024, %eax             # imm = 0x400
	je	.LBB0_605
# BB#602:
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	-1(%rax), %r13
	leaq	(%r13,%r13,8), %r15
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r15,4), %eax
	addl	$96, %eax
	cmpl	%eax, 296(%rsp)
	jne	.LBB0_605
# BB#603:
	leaq	672(%rsp), %rdi
	movl	$.L.str.218, %esi
	movl	$51, %edx
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB0_605
# BB#604:
	movl	$3165, %eax             # imm = 0xC5D
	movq	40(%rsp), %rcx          # 8-byte Reload
	addl	8(%rcx,%r15,4), %eax
	cmpq	%rax, 128(%rsp)         # 8-byte Folded Reload
	jae	.LBB0_652
.LBB0_605:
	cmpl	$2, 88(%rsp)            # 4-byte Folded Reload
	jb	.LBB0_612
.LBB0_606:
	andl	$2048, %r14d            # imm = 0x800
	je	.LBB0_612
# BB#607:
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	-1(%rax), %r13
	leaq	(%r13,%r13,8), %r12
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	8(%rax,%r12,4), %r14d
	cmpl	$690, %r14d             # imm = 0x2B2
	jb	.LBB0_612
# BB#608:
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	104(%rsp), %rcx         # 8-byte Reload
	cmpl	(%rax,%r12,4), %ecx
	jne	.LBB0_612
# BB#609:
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	12(%rax,%r12,4), %r15d
	movq	104(%rsp), %rax         # 8-byte Reload
	leal	(%r15,%rax), %eax
	cmpl	180(%rsp), %eax         # 4-byte Folded Reload
	jne	.LBB0_612
# BB#610:
	leaq	672(%rsp), %rdi
	movl	$.L.str.224, %esi
	movl	$7, %edx
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB0_612
# BB#611:
	leaq	776(%rsp), %rdi
	movl	$.L.str.225, %esi
	movl	$19, %edx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_710
.LBB0_612:
	movq	56(%rbx), %rax
	cmpw	$0, (%rax)
	jns	.LBB0_796
# BB#613:
	movq	144(%rsp), %rax         # 8-byte Reload
	leal	1864(%rax), %eax
	cmpq	128(%rsp), %rax         # 8-byte Folded Reload
	jae	.LBB0_796
# BB#614:
	movabsq	$-1447625805222647712, %rcx # imm = 0xEBE900000003E860
	xorl	%eax, %eax
	cmpq	%rcx, 672(%rsp)
	setne	%cl
	cmpl	$959, 140(%rsp)         # 4-byte Folded Reload
                                        # imm = 0x3BF
	jb	.LBB0_796
# BB#615:
	movb	%cl, %al
	testl	%eax, %eax
	jne	.LBB0_796
# BB#616:
	leaq	1625(%rsp), %rdi
	movl	$.L.str.237, %esi
	movl	$6, %edx
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB0_796
# BB#617:
	cmpw	$0, 88(%rsp)            # 2-byte Folded Reload
	je	.LBB0_796
# BB#618:                               # %.lr.ph2902.preheader
	testb	$1, 88(%rsp)            # 1-byte Folded Reload
	jne	.LBB0_681
# BB#619:
	xorl	%edx, %edx
                                        # implicit-def: %R12D
	xorl	%ecx, %ecx
	cmpl	$1, 88(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_682
	jmp	.LBB0_684
.LBB0_620:
	movq	72(%rsp), %rax          # 8-byte Reload
	leal	1(%rax), %r13d
	leaq	(%r13,%r13,8), %r15
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	12(%rcx,%r15,4), %r14d
	movl	%eax, %eax
	leaq	(%rax,%rax,8), %r12
	movl	4(%rcx,%r15,4), %edx
	addl	4(%rcx,%r12,4), %edx
	movl	%edx, 84(%rsp)
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_625
# BB#621:
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB0_625
# BB#622:
	cmpl	%r14d, %edx
	movl	%r14d, %esi
	cmoval	%edx, %esi
	cmpq	%rcx, %rsi
	jbe	.LBB0_625
# BB#623:
	xorl	%r13d, %r13d
	movl	$.L.str.175, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%rcx, %rdx
	callq	cli_dbgmsg
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	testb	$1, 41(%rbx)
	je	.LBB0_23
# BB#624:
	movq	(%rbx), %rax
	movq	$.L.str.176, (%rax)
	movl	$1, %r13d
	jmp	.LBB0_23
.LBB0_625:
	cmpl	$26, %r14d
	jae	.LBB0_628
.LBB0_626:
	xorl	%r13d, %r13d
	movl	$.L.str.177, %edi
.LBB0_627:                              # %.thread2694
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	cli_dbgmsg
	jmp	.LBB0_244
.LBB0_628:
	cmpl	%r14d, %edx
	jbe	.LBB0_626
# BB#629:
	cmpl	$184549377, %edx        # imm = 0xB000001
	jae	.LBB0_626
# BB#630:
	movq	%r14, %rdi
	callq	cli_malloc
	movq	%rax, 48(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_939
# BB#631:
	movl	84(%rsp), %edi
	addl	$8192, %edi             # imm = 0x2000
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, 64(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_993
# BB#632:
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	12(%rax,%r15,4), %rax
	cmpl	$0, (%rax)
	je	.LBB0_691
# BB#633:
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	8(%rax,%r15,4), %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_690
# BB#634:                               # %cli_seeksect.exit2614
	movl	%ebp, %edi
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	%r14d, %edx
	callq	cli_readn
	cmpl	%r14d, %eax
	jne	.LBB0_691
# BB#635:
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r15,4), %r13
	leaq	(%rax,%r12,4), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	777(%rsp), %r12
	movl	$.L.str.179, %edi
	movl	$24, %esi
	movl	$13, %ecx
	movq	%r12, %rdx
	callq	cli_memstr
	testq	%rax, %rax
	jne	.LBB0_637
# BB#636:
	leaq	785(%rsp), %r15
	movl	$.L.str.179, %edi
	movl	$24, %esi
	movl	$13, %ecx
	movq	%r15, %rdx
	callq	cli_memstr
	testq	%rax, %rax
	je	.LBB0_779
.LBB0_637:
	movl	$.L.str.180, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$upx_inflate2b, %r15d
.LBB0_638:
	movl	(%r13), %r9d
	cmpb	$-66, 673(%rsp)
	jne	.LBB0_714
# BB#639:
	movl	674(%rsp), %r12d
	subl	308(%rsp), %r12d
	subl	%r9d, %r12d
	leal	-1(%r12), %eax
	cmpl	$4095, %eax             # imm = 0xFFF
	jae	.LBB0_714
# BB#640:
	movl	$.L.str.185, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	cli_dbgmsg
	movslq	%r12d, %rdi
	addq	48(%rsp), %rdi          # 8-byte Folded Reload
	movl	%r14d, %esi
	subl	%r12d, %esi
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r8d
	movl	(%r13), %r9d
	movq	104(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%r12d, %eax
	movl	%eax, (%rsp)
	leaq	84(%rsp), %rcx
	movq	64(%rsp), %rdx          # 8-byte Reload
	callq	*%r15
	testl	%eax, %eax
	jns	.LBB0_716
# BB#641:
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r8d
	movl	(%r13), %r9d
	jmp	.LBB0_715
.LBB0_642:
	movl	180(%rsp), %eax         # 4-byte Reload
	subl	116(%rsp), %eax         # 4-byte Folded Reload
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movl	%eax, 84(%rsp)
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_666
# BB#643:
	movq	24(%rax), %rdx
	movl	64(%rsp), %edi          # 4-byte Reload
	testq	%rdx, %rdx
	je	.LBB0_667
# BB#644:
	cmpq	%rdx, %rdi
	jbe	.LBB0_667
# BB#645:
	xorl	%r13d, %r13d
	movl	$.L.str.201, %edi
	xorl	%eax, %eax
	movl	64(%rsp), %esi          # 4-byte Reload
	callq	cli_dbgmsg
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	testb	$1, 41(%rbx)
	je	.LBB0_23
# BB#646:
	movq	(%rbx), %rax
	movq	$.L.str.202, (%rax)
	movl	$1, %r13d
	jmp	.LBB0_23
.LBB0_647:
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_658
# BB#648:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB0_658
# BB#649:
	cmpq	%rdx, 128(%rsp)         # 8-byte Folded Reload
	jbe	.LBB0_658
# BB#650:
	xorl	%r13d, %r13d
	movl	$.L.str.209, %edi
	xorl	%eax, %eax
	movq	128(%rsp), %rsi         # 8-byte Reload
	callq	cli_dbgmsg
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	testb	$1, 41(%rbx)
	je	.LBB0_23
# BB#651:
	movq	(%rbx), %rax
	movq	$.L.str.210, (%rax)
	movl	$1, %r13d
	jmp	.LBB0_23
.LBB0_652:
	movq	128(%rsp), %rdi         # 8-byte Reload
	callq	cli_malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB0_939
# BB#653:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	movl	%ebp, %edi
	movq	%r12, %rsi
	movq	128(%rsp), %r14         # 8-byte Reload
	movl	%r14d, %edx
	callq	cli_readn
	cltq
	cmpq	%r14, %rax
	jne	.LBB0_692
# BB#654:
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_709
# BB#655:
	movl	$578, %esi              # imm = 0x242
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	open
	movl	%eax, %r14d
	testl	%r14d, %r14d
	js	.LBB0_739
# BB#656:
	movl	192(%rsp), %r8d
	movq	%r12, %rdi
	movq	128(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	%r13d, %ecx
	movl	%r14d, %r9d
	callq	yc_decrypt
	testl	%eax, %eax
	je	.LBB0_751
# BB#657:
	movl	$.L.str.223, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r14d, %edi
	callq	close
	movq	%r15, %rdi
	callq	unlink
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	cli_multifree
	movq	%r15, %rdi
	callq	free
	movq	56(%rbx), %rax
	movl	(%rax), %r14d
	cmpl	$2, 88(%rsp)            # 4-byte Folded Reload
	jae	.LBB0_606
	jmp	.LBB0_612
.LBB0_658:
	movq	128(%rsp), %rdi         # 8-byte Reload
	callq	cli_malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB0_939
# BB#659:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	movl	%ebp, %edi
	movq	%r12, %rsi
	movq	128(%rsp), %r14         # 8-byte Reload
	movl	%r14d, %edx
	callq	cli_readn
	cltq
	cmpq	%r14, %rax
	jne	.LBB0_693
# BB#660:
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_709
# BB#661:
	movl	$578, %esi              # imm = 0x242
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	open
	movl	%eax, %r14d
	testl	%r14d, %r14d
	js	.LBB0_748
# BB#662:
	movq	%rbx, (%rsp)
	movq	%r12, %rdi
	movq	128(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	%r13d, %ecx
	movq	104(%rsp), %r8          # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%r14d, %r9d
	callq	unspin
	cmpl	$2, %eax
	je	.LBB0_833
# BB#663:
	testl	%eax, %eax
	jne	.LBB0_835
# BB#664:
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB0_874
# BB#665:
	movl	$.L.str.213, %edi
	jmp	.LBB0_753
.LBB0_666:                              # %._crit_edge3095
	movl	64(%rsp), %edi          # 4-byte Reload
.LBB0_667:
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, 72(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_679
# BB#668:                               # %.preheader2818
	cmpw	$0, 88(%rsp)            # 2-byte Folded Reload
	je	.LBB0_675
# BB#669:                               # %.lr.ph2913
	movl	116(%rsp), %r15d        # 4-byte Reload
	negq	%r15
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	32(%rax), %r14
	xorl	%r12d, %r12d
.LBB0_670:                              # =>This Inner Loop Header: Depth=1
	movl	-24(%r14), %esi
	testq	%rsi, %rsi
	je	.LBB0_674
# BB#671:                               #   in Loop: Header=BB0_670 Depth=1
	cmpl	$0, -20(%r14)
	je	.LBB0_730
# BB#672:                               #   in Loop: Header=BB0_670 Depth=1
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_729
# BB#673:                               # %cli_seeksect.exit2616
                                        #   in Loop: Header=BB0_670 Depth=1
	movl	-32(%r14), %esi
	movl	(%r14), %edx
	addq	72(%rsp), %rsi          # 8-byte Folded Reload
	addq	%r15, %rsi
	movl	%ebp, %edi
	callq	cli_readn
	cmpl	(%r14), %eax
	jne	.LBB0_730
.LBB0_674:                              #   in Loop: Header=BB0_670 Depth=1
	incq	%r12
	addq	$36, %r14
	cmpq	88(%rsp), %r12          # 8-byte Folded Reload
	jb	.LBB0_670
.LBB0_675:                              # %._crit_edge2914
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_791
# BB#676:
	movl	$578, %esi              # imm = 0x242
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	open
	movl	%eax, %r15d
	testl	%r15d, %r15d
	js	.LBB0_727
# BB#677:
	xorl	%eax, %eax
	cmpl	$1, %r13d
	sete	%al
	movq	88(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %r8d
	subl	%eax, %r8d
	movl	308(%rsp), %r9d
	movl	392(%rsp), %eax
	movl	396(%rsp), %ecx
	movl	%ecx, 32(%rsp)
	movl	%eax, 24(%rsp)
	movl	%r13d, 16(%rsp)
	movl	%r15d, 8(%rsp)
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	%eax, (%rsp)
	movq	72(%rsp), %rdi          # 8-byte Reload
	movl	116(%rsp), %esi         # 4-byte Reload
	movl	64(%rsp), %edx          # 4-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	callq	petite_inflate2x_1to9
	testl	%eax, %eax
	je	.LBB0_740
# BB#678:
	movl	$.L.str.207, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r15d, %edi
	callq	close
	movq	%r14, %rdi
	callq	unlink
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	cli_multifree
	movq	%r14, %rdi
	callq	free
	cmpl	$2, 88(%rsp)            # 4-byte Folded Reload
	jae	.LBB0_596
	jmp	.LBB0_600
.LBB0_679:
	movl	84(%rsp), %esi
	movl	$.L.str.203, %edi
.LBB0_680:                              # %.thread2694
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_939
.LBB0_681:                              # %.lr.ph2902.prol
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %r12d
	addl	(%rax), %r12d
	movl	$1, %edx
	movl	%r12d, %ecx
	cmpl	$1, 88(%rsp)            # 4-byte Folded Reload
	je	.LBB0_684
.LBB0_682:                              # %.lr.ph2902.preheader.new
	movq	88(%rsp), %rax          # 8-byte Reload
	subq	%rdx, %rax
	leaq	(%rdx,%rdx,8), %rdx
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	40(%rsi,%rdx,4), %rdx
.LBB0_683:                              # %.lr.ph2902
                                        # =>This Inner Loop Header: Depth=1
	movl	-36(%rdx), %esi
	movl	(%rdx), %r12d
	addl	-40(%rdx), %esi
	cmpl	%esi, %ecx
	cmovael	%ecx, %esi
	addl	-4(%rdx), %r12d
	cmpl	%r12d, %esi
	cmovael	%esi, %r12d
	addq	$72, %rdx
	addq	$-2, %rax
	movl	%r12d, %ecx
	jne	.LBB0_683
.LBB0_684:                              # %._crit_edge
	testl	%r12d, %r12d
	je	.LBB0_796
# BB#685:
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_698
# BB#686:
	movq	24(%rax), %rdx
	movl	%r12d, %r13d
	testq	%rdx, %rdx
	je	.LBB0_699
# BB#687:
	cmpq	%rdx, %r13
	jbe	.LBB0_699
# BB#688:
	xorl	%r13d, %r13d
	movl	$.L.str.238, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	cli_dbgmsg
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	testb	$1, 41(%rbx)
	je	.LBB0_23
# BB#689:
	movq	(%rbx), %rax
	movq	$.L.str.239, (%rax)
	movl	$1, %r13d
	jmp	.LBB0_23
.LBB0_690:                              # %cli_seeksect.exit2614.thread2726
	movl	$.L.str.259, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_691:                              # %cli_seeksect.exit2614.thread
	movl	$.L.str.178, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_dbgmsg
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	64(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_697
.LBB0_692:
	movl	$.L.str.219, %edi
	jmp	.LBB0_694
.LBB0_693:
	movl	$.L.str.211, %edi
.LBB0_694:                              # %.thread2694
	xorl	%eax, %eax
	movq	128(%rsp), %rsi         # 8-byte Reload
	callq	cli_dbgmsg
	movq	%r12, %rdi
.LBB0_695:
	callq	free
.LBB0_696:
	movq	40(%rsp), %rdi          # 8-byte Reload
.LBB0_697:                              # %.thread2694
	callq	free
	movl	$-123, %r13d
	jmp	.LBB0_23
.LBB0_698:                              # %._crit_edge3096
	movl	%r12d, %r13d
.LBB0_699:
	movl	$1, %esi
	movq	%r13, %rdi
	callq	cli_calloc
	movq	%rax, 72(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_939
# BB#700:                               # %.preheader
	cmpw	$0, 88(%rsp)            # 2-byte Folded Reload
	je	.LBB0_793
# BB#701:                               # %.lr.ph.split.preheader
	addq	72(%rsp), %r13          # 8-byte Folded Reload
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	12(%rax), %r14
	xorl	%r15d, %r15d
.LBB0_702:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%r14)
	je	.LBB0_708
# BB#703:                               #   in Loop: Header=BB0_702 Depth=1
	movl	-4(%r14), %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_784
# BB#704:                               # %cli_seeksect.exit2622
                                        #   in Loop: Header=BB0_702 Depth=1
	movl	(%r14), %edx
	leal	-1(%rdx), %eax
	cmpl	%r12d, %eax
	jae	.LBB0_785
# BB#705:                               #   in Loop: Header=BB0_702 Depth=1
	movl	-12(%r14), %esi
	addq	72(%rsp), %rsi          # 8-byte Folded Reload
	leaq	(%rsi,%rdx), %rax
	cmpq	%r13, %rax
	ja	.LBB0_785
# BB#706:                               #   in Loop: Header=BB0_702 Depth=1
	cmpq	72(%rsp), %rax          # 8-byte Folded Reload
	jbe	.LBB0_785
# BB#707:                               #   in Loop: Header=BB0_702 Depth=1
	movl	%ebp, %edi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_readn
	cmpl	(%r14), %eax
	jne	.LBB0_785
.LBB0_708:                              #   in Loop: Header=BB0_702 Depth=1
	incq	%r15
	addq	$36, %r14
	cmpq	88(%rsp), %r15          # 8-byte Folded Reload
	jb	.LBB0_702
	jmp	.LBB0_785
.LBB0_709:
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	jmp	.LBB0_792
.LBB0_710:                              # %.preheader2817
	testl	%r13d, %r13d
	je	.LBB0_758
# BB#711:                               # %.lr.ph2909.preheader
	movl	%r13d, %eax
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB0_754
# BB#712:                               # %.lr.ph2909.prol.preheader
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	8(%rcx), %rdi
	xorl	%edx, %edx
.LBB0_713:                              # %.lr.ph2909.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi), %ecx
	cmpl	%r14d, %ecx
	cmovbl	%ecx, %r14d
	incq	%rdx
	addq	$36, %rdi
	cmpq	%rdx, %rsi
	jne	.LBB0_713
	jmp	.LBB0_755
.LBB0_714:
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r8d
.LBB0_715:
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	%eax, (%rsp)
	leaq	84(%rsp), %rcx
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%r14d, %esi
	movq	64(%rsp), %rdx          # 8-byte Reload
	callq	*%r15
	testl	%eax, %eax
	js	.LBB0_735
.LBB0_716:                              # %.thread2734
	movl	$.L.str.186, %edi
.LBB0_717:                              # %.thread2749
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_718:                              # %.thread2749
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_728
# BB#719:
	movl	$578, %esi              # imm = 0x242
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	open
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB0_742
# BB#720:
	movl	84(%rsp), %edx
	movl	%ebp, %edi
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	write
	movl	84(%rsp), %esi
	cmpl	%esi, %eax
	jne	.LBB0_747
# BB#721:
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	%ebp, %edi
	callq	fsync
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB0_723
# BB#722:
	movl	$.L.str.197, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
.LBB0_723:
	movl	$.L.str.198, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	cli_magic_scandesc
	movl	%eax, %r13d
	movl	%ebp, %edi
	callq	close
	movb	cli_leavetemps_flag(%rip), %al
	cmpl	$1, %r13d
	je	.LBB0_888
# BB#724:
	testb	%al, %al
	jne	.LBB0_726
# BB#725:
	movq	%r14, %rdi
	callq	unlink
.LBB0_726:
	movq	%r14, %rdi
	callq	free
	jmp	.LBB0_23
.LBB0_727:
	movl	$.L.str.204, %edi
	jmp	.LBB0_831
.LBB0_728:
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	cli_multifree
	movl	$-114, %r13d
	jmp	.LBB0_23
.LBB0_729:                              # %cli_seeksect.exit2616.thread2757
	movl	$.L.str.259, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_730:                              # %cli_seeksect.exit2616.thread
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	72(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_697
.LBB0_731:
	movl	$.L.str.157, %edi
.LBB0_732:                              # %.critedge71
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_733:                              # %.critedge71
	movq	48(%rsp), %rdi          # 8-byte Reload
.LBB0_734:                              # %.critedge71
	callq	free
	testl	%r13d, %r13d
	jne	.LBB0_460
	jmp	.LBB0_586
.LBB0_793:
	xorl	%r15d, %r15d
	cmpl	88(%rsp), %r15d         # 4-byte Folded Reload
	je	.LBB0_786
	jmp	.LBB0_794
.LBB0_735:
	movl	$.L.str.187, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$upx_inflate2b, %r12d
	cmpq	%r12, %r15
	je	.LBB0_898
.LBB0_736:                              # %.thread3121
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r8d
	movl	(%r13), %r9d
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	%eax, (%rsp)
	leaq	84(%rsp), %rcx
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%r14d, %esi
	movq	64(%rsp), %rdx          # 8-byte Reload
	callq	upx_inflate2b
	cmpl	$-1, %eax
	jne	.LBB0_738
# BB#737:
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	21(%rax), %rdi
	leal	-21(%r14), %esi
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r8d
	movl	(%r13), %r9d
	movq	104(%rsp), %rax         # 8-byte Reload
	leal	-21(%rax), %eax
	movl	%eax, (%rsp)
	leaq	84(%rsp), %rcx
	movq	64(%rsp), %rdx          # 8-byte Reload
	callq	upx_inflate2b
	cmpl	$-1, %eax
	je	.LBB0_897
.LBB0_738:
	movl	$.L.str.189, %edi
	jmp	.LBB0_717
.LBB0_739:
	movl	$.L.str.220, %edi
	jmp	.LBB0_749
.LBB0_740:
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB0_836
# BB#741:
	movl	$.L.str.205, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	jmp	.LBB0_837
.LBB0_742:
	movl	$.L.str.195, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	movq	%r14, %rdi
	callq	free
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	cli_multifree
	movl	$-123, %r13d
	jmp	.LBB0_23
.LBB0_743:
	movl	$.L.str.171, %edi
	xorl	%eax, %eax
	movl	160(%rsp), %esi         # 4-byte Reload
	callq	cli_dbgmsg
	jmp	.LBB0_560
.LBB0_744:
	movl	$.L.str.147, %edi
.LBB0_745:                              # %.thread2694
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_746:                              # %.thread2694
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	56(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_697
.LBB0_844:                              # %.thread2681
	movl	84(%rsp), %edi
	addl	64(%rsp), %edi          # 4-byte Folded Reload
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, 56(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_939
# BB#845:
	movl	(%r15), %edx
	leal	12(%r14), %eax
	cmpl	%eax, %edx
	jb	.LBB0_852
# BB#846:
	cmpl	64(%rsp), %edx          # 4-byte Folded Reload
	ja	.LBB0_852
# BB#847:
	movl	84(%rsp), %eax
	movq	56(%rsp), %rsi          # 8-byte Reload
	addq	%rax, %rsi
	movl	%ebp, %edi
	callq	read
	movq	%rax, %rcx
	movl	(%r15), %esi
	cmpq	%rsi, %rcx
	jne	.LBB0_884
# BB#848:
	xorl	%r15d, %r15d
	movl	$.L.str.133, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	movq	%rcx, %rdx
	callq	cli_dbgmsg
	cmpb	$-24, 4891(%rsp)
	jne	.LBB0_954
# BB#849:
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	40(%rax), %ecx
	cmpl	$4, %ecx
	jb	.LBB0_851
# BB#850:
	movl	4892(%rsp), %eax
	leal	128(%rax,%r13), %esi
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	36(%rdx), %edx
	cmpl	%edx, %esi
	jae	.LBB0_951
.LBB0_851:
	movl	$.L.str.134, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_853
.LBB0_747:
	movl	$.L.str.196, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r14, %rdi
	callq	free
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	%ebp, %edi
	callq	close
	movl	$-123, %r13d
	jmp	.LBB0_23
.LBB0_748:
	movl	$.L.str.212, %edi
.LBB0_749:                              # %.thread2694
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	movq	%r15, %rdi
	callq	free
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	jmp	.LBB0_832
.LBB0_750:                              # %cli_seeksect.exit2594.thread2679
	movl	$.L.str.259, %edi
	jmp	.LBB0_489
.LBB0_751:
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB0_842
# BB#752:
	movl	$.L.str.221, %edi
.LBB0_753:
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	jmp	.LBB0_876
.LBB0_754:
	xorl	%edx, %edx
.LBB0_755:                              # %.lr.ph2909.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB0_758
# BB#756:                               # %.lr.ph2909.preheader.new
	subq	%rdx, %rax
	leaq	(%rdx,%rdx,8), %rcx
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	116(%rdx,%rcx,4), %rcx
.LBB0_757:                              # %.lr.ph2909
                                        # =>This Inner Loop Header: Depth=1
	movl	-108(%rcx), %edx
	movl	-72(%rcx), %esi
	cmpl	%r14d, %edx
	cmovbl	%edx, %r14d
	cmpl	%r14d, %esi
	cmovbl	%esi, %r14d
	movl	-36(%rcx), %edx
	cmpl	%r14d, %edx
	cmovbl	%edx, %r14d
	movl	(%rcx), %edx
	cmpl	%r14d, %edx
	cmovbl	%edx, %r14d
	addq	$144, %rcx
	addq	$-4, %rax
	jne	.LBB0_757
.LBB0_758:                              # %._crit_edge2910
	movl	180(%rsp), %eax         # 4-byte Reload
	subl	116(%rsp), %eax         # 4-byte Folded Reload
	addl	%r14d, %eax
	subl	%r15d, %eax
	movl	%eax, %esi
	movl	%eax, 84(%rsp)
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_763
# BB#759:
	movq	24(%rax), %rdx
	movl	%esi, %edi
	testq	%rdx, %rdx
	je	.LBB0_764
# BB#760:
	cmpq	%rdx, %rdi
	jbe	.LBB0_764
# BB#761:
	xorl	%r13d, %r13d
	movl	$.L.str.226, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	testb	$1, 41(%rbx)
	je	.LBB0_23
# BB#762:
	movq	(%rbx), %rax
	movq	$.L.str.227, (%rax)
	movl	$1, %r13d
	jmp	.LBB0_23
.LBB0_763:                              # %._crit_edge2910._crit_edge
	movl	%esi, %edi
.LBB0_764:
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, 56(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_778
# BB#765:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	movl	%ebp, %edi
	movq	56(%rsp), %rsi          # 8-byte Reload
	movl	%r14d, %edx
	callq	cli_readn
	cltq
	movl	%r14d, %ecx
	cmpq	%rcx, %rax
	jne	.LBB0_782
# BB#766:                               # %.preheader2816
	testl	%r13d, %r13d
	je	.LBB0_772
# BB#767:                               # %.lr.ph2905
	movq	56(%rsp), %rcx          # 8-byte Reload
	addq	%rax, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	116(%rsp), %eax         # 4-byte Reload
	negq	%rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	%r13d, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	12(%rax), %r15
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
.LBB0_768:                              # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%r15)
	je	.LBB0_771
# BB#769:                               #   in Loop: Header=BB0_768 Depth=1
	movl	-4(%r15), %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_896
# BB#770:                               # %cli_seeksect.exit2618
                                        #   in Loop: Header=BB0_768 Depth=1
	movl	-12(%r15), %esi
	movl	(%r15), %edx
	addq	48(%rsp), %rsi          # 8-byte Folded Reload
	addq	96(%rsp), %rsi          # 8-byte Folded Reload
	movl	%ebp, %edi
	callq	cli_readn
	cmpl	(%r15), %eax
	jne	.LBB0_783
.LBB0_771:                              #   in Loop: Header=BB0_768 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	addq	$36, %r15
	movq	%rcx, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	cmpq	64(%rsp), %rcx          # 8-byte Folded Reload
	jb	.LBB0_768
.LBB0_772:                              # %._crit_edge2906
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	12(%rax,%r12,4), %r15
	movl	(%r15), %edi
	movl	$1, %esi
	callq	cli_calloc
	movl	(%r15), %esi
	movq	%rax, 72(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_937
# BB#773:
	testl	%esi, %esi
	je	.LBB0_914
# BB#774:
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax,%r12,4), %rax
	movl	(%rax), %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_924
# BB#775:                               # %cli_seeksect.exit2620
	movl	(%r15), %edx
	movl	%ebp, %edi
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	cli_readn
	cltq
	movl	(%r15), %esi
	cmpq	%rsi, %rax
	jne	.LBB0_925
# BB#776:
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r12,4), %rcx
	movl	84(%rsp), %eax
	movl	(%rcx), %r8d
	movl	192(%rsp), %r9d
	movzwl	%r13w, %ecx
	movl	%ecx, 16(%rsp)
	movl	%esi, 8(%rsp)
	movq	72(%rsp), %r15          # 8-byte Reload
	movq	%r15, (%rsp)
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	%eax, %esi
	movl	%r14d, %edx
	movl	116(%rsp), %ecx         # 4-byte Reload
	callq	wwunpack
	movl	%eax, %r14d
	movq	%r15, %rdi
	callq	free
	testl	%r14d, %r14d
	je	.LBB0_960
# BB#777:
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	$.L.str.235, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_612
.LBB0_778:
	movl	84(%rsp), %esi
	movl	$.L.str.228, %edi
	jmp	.LBB0_680
.LBB0_779:
	movl	$.L.str.181, %edi
	movl	$24, %esi
	movl	$13, %ecx
	movq	%r12, %rdx
	callq	cli_memstr
	testq	%rax, %rax
	jne	.LBB0_781
# BB#780:
	movl	$.L.str.181, %edi
	movl	$24, %esi
	movl	$13, %ecx
	movq	%r15, %rdx
	callq	cli_memstr
	testq	%rax, %rax
	je	.LBB0_911
.LBB0_781:
	movl	$.L.str.182, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$upx_inflate2d, %r15d
	jmp	.LBB0_638
.LBB0_782:
	movl	$.L.str.229, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	cli_dbgmsg
.LBB0_783:                              # %cli_seeksect.exit2618.thread
	movq	56(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_695
.LBB0_784:                              # %cli_seeksect.exit2622.thread2767
	movl	$.L.str.259, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_785:                              # %cli_seeksect.exit2622.thread
	cmpl	88(%rsp), %r15d         # 4-byte Folded Reload
	jne	.LBB0_794
.LBB0_786:
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_791
# BB#787:
	movl	$578, %esi              # imm = 0x242
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	open
	movl	%eax, %r13d
	testl	%r13d, %r13d
	js	.LBB0_830
# BB#788:
	movq	104(%rsp), %rax         # 8-byte Reload
	leal	-1(%rax), %r8d
	movl	308(%rsp), %r9d
	movl	%r13d, (%rsp)
	movq	72(%rsp), %rdi          # 8-byte Reload
	movl	%r12d, %esi
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	88(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	unaspack212
	cmpl	$1, %eax
	jne	.LBB0_839
# BB#789:
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB0_885
# BB#790:
	movl	$.L.str.242, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	jmp	.LBB0_886
.LBB0_791:
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	72(%rsp), %rdi          # 8-byte Reload
.LBB0_792:                              # %.thread2694
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	cli_multifree
	movl	$-114, %r13d
	jmp	.LBB0_23
.LBB0_794:
	movl	$.L.str.240, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	72(%rsp), %rdi          # 8-byte Reload
.LBB0_795:                              # %.critedge107
	callq	free
.LBB0_796:                              # %.critedge107
	movq	56(%rbx), %rax
	testb	$16, 1(%rax)
	je	.LBB0_812
# BB#797:
	cmpb	$-23, 672(%rsp)
	jne	.LBB0_811
# BB#798:
	movl	673(%rsp), %eax
	movq	104(%rsp), %rcx         # 8-byte Reload
	leal	5(%rcx,%rax), %ecx
	movq	%rcx, %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	cmpl	156(%rsp), %ecx         # 4-byte Folded Reload
	jae	.LBB0_800
# BB#799:
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rdx
	movl	%edx, %ecx
	xorl	%esi, %esi
	xorl	%eax, %eax
	cmpq	128(%rsp), %rcx         # 8-byte Folded Reload
	setae	%al
	cmovbl	%edx, %esi
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	testl	%eax, %eax
	jne	.LBB0_807
	jmp	.LBB0_809
.LBB0_800:
	xorl	%eax, %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	$1, %eax
	cmpw	$0, 88(%rsp)            # 2-byte Folded Reload
	je	.LBB0_806
# BB#801:                               # %.lr.ph.i2629.preheader
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	1(%rcx), %rdx
	leaq	(%rcx,%rcx,8), %rcx
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	-24(%rsi,%rcx,4), %rcx
.LBB0_802:                              # %.lr.ph.i2629
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edi
	testl	%edi, %edi
	je	.LBB0_805
# BB#803:                               #   in Loop: Header=BB0_802 Depth=1
	movq	104(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	subl	-12(%rcx), %esi
	jb	.LBB0_805
# BB#804:                               #   in Loop: Header=BB0_802 Depth=1
	cmpl	%esi, %edi
	ja	.LBB0_808
.LBB0_805:                              # %.backedge.i2631
                                        #   in Loop: Header=BB0_802 Depth=1
	decq	%rdx
	addq	$-36, %rcx
	cmpq	$1, %rdx
	jg	.LBB0_802
.LBB0_806:                              # %cli_rawaddr.exit2634
	testl	%eax, %eax
	je	.LBB0_809
.LBB0_807:                              # %cli_rawaddr.exit2634
	cmpl	$0, 144(%rsp)           # 4-byte Folded Reload
	je	.LBB0_812
	jmp	.LBB0_809
.LBB0_808:                              # %cli_rawaddr.exit2634.thread
	addl	-4(%rcx), %esi
	movl	%esi, %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
.LBB0_809:
	movl	144(%rsp), %esi         # 4-byte Reload
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_812
# BB#810:
	leaq	256(%rsp), %rsi
	movl	$24, %edx
	movl	%ebp, %edi
	movq	%rsi, 168(%rsp)         # 8-byte Spill
	callq	cli_readn
	cmpl	$24, %eax
	jne	.LBB0_812
.LBB0_811:
	movl	$.L.str.245, %esi
	movl	$13, %edx
	movq	168(%rsp), %rdi         # 8-byte Reload
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_814
.LBB0_812:
	movq	40(%rsp), %rdi          # 8-byte Reload
.LBB0_813:                              # %.thread2694
	callq	free
	xorl	%r13d, %r13d
	jmp	.LBB0_23
.LBB0_814:
	movl	$84, %r14d
	movq	168(%rsp), %rax         # 8-byte Reload
	subl	17(%rax), %r14d
	movl	$.L.str.246, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	cli_dbgmsg
	movq	144(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	subl	%r14d, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_812
# BB#815:
	leaq	256(%rsp), %rsi
	movl	$4, %edx
	movl	%ebp, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB0_812
# BB#816:
	movq	144(%rsp), %rsi         # 8-byte Reload
	addl	256(%rsp), %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	movq	%rsi, %r15
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_812
# BB#817:
	leaq	256(%rsp), %r14
	movl	$20, %edx
	movl	%ebp, %edi
	movq	%r14, %rsi
	callq	cli_readn
	cmpl	$20, %eax
	jne	.LBB0_812
# BB#818:
	leal	4(%r15), %eax
	cmpl	$0, 256(%rsp)
	leaq	260(%rsp), %rcx
	cmovel	%eax, %r15d
	movq	%r15, %rdi
	cmoveq	%rcx, %r14
	movl	$255, %r13d
	orl	5(%r14), %r13d
	orq	$9, %r14
	movl	(%r14), %r14d
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_823
# BB#819:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB0_823
# BB#820:
	cmpl	%r14d, %r13d
	movl	%r14d, %esi
	cmoval	%r13d, %esi
	cmpq	%rdx, %rsi
	jbe	.LBB0_823
# BB#821:
	xorl	%r13d, %r13d
	movl	$.L.str.247, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	testb	$1, 41(%rbx)
	je	.LBB0_23
# BB#822:
	movq	(%rbx), %rax
	movq	$.L.str.248, (%rax)
	movl	$1, %r13d
	jmp	.LBB0_23
.LBB0_823:
	testl	%r14d, %r14d
	je	.LBB0_812
# BB#824:
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpl	4(%rax), %r14d
	jne	.LBB0_812
# BB#825:
	movl	%edi, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_812
# BB#826:
	movq	%r14, %rdi
	callq	cli_malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_812
# BB#827:
	movl	%r13d, %edi
	callq	cli_malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB0_895
# BB#828:
	movl	%ebp, %edi
	movq	%r12, %rsi
	movl	%r13d, %edx
	callq	cli_readn
	movq	104(%rsp), %rax         # 8-byte Reload
	leal	634(%rax), %ecx
	cmpl	156(%rsp), %ecx         # 4-byte Folded Reload
	jae	.LBB0_901
# BB#829:
	movl	%ecx, %eax
	xorl	%r9d, %r9d
	cmpq	128(%rsp), %rax         # 8-byte Folded Reload
	setae	%r8b
	cmovbl	%ecx, %r9d
	testb	%r8b, %r8b
	jne	.LBB0_908
	jmp	.LBB0_942
.LBB0_830:
	movl	$.L.str.241, %edi
.LBB0_831:                              # %.thread2694
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	movq	%r14, %rdi
	callq	free
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	72(%rsp), %rdi          # 8-byte Reload
.LBB0_832:                              # %.thread2694
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	cli_multifree
	movl	$-123, %r13d
	jmp	.LBB0_23
.LBB0_833:
	movq	%r12, %rdi
	callq	free
	movl	%r14d, %edi
	callq	close
	movq	%r15, %rdi
	callq	unlink
	movl	$.L.str.215, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	40(%rbx), %r14d
	movq	%r15, %rdi
	callq	free
	movl	%r14d, %eax
	testb	$1, %ah
	je	.LBB0_600
# BB#834:
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	(%rbx), %rax
	movq	$.L.str.216, (%rax)
	movl	$1, %r13d
	jmp	.LBB0_23
.LBB0_835:
	movl	$.L.str.217, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r14d, %edi
	callq	close
	movq	%r15, %rdi
	callq	unlink
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	cli_multifree
	movq	%r15, %rdi
	callq	free
	jmp	.LBB0_600
.LBB0_836:
	movl	$.L.str.206, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_837:
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	72(%rsp), %rdi          # 8-byte Reload
.LBB0_838:
	callq	cli_multifree
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	%r15d, %edi
	callq	fsync
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r15d, %edi
	callq	lseek
	movl	$.L.str.138, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r15d, %edi
	movq	%rbx, %rsi
	callq	cli_magic_scandesc
	movl	%eax, %ebx
	movl	%r15d, %edi
	jmp	.LBB0_887
.LBB0_839:
	movl	$.L.str.244, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r13d, %edi
	callq	close
	movq	%r14, %rdi
	callq	unlink
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	cli_multifree
	movq	%r14, %rdi
	jmp	.LBB0_795
.LBB0_840:
	movl	$.L.str.158, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	jmp	.LBB0_733
.LBB0_841:
	movl	$.L.str.171, %edi
	xorl	%eax, %eax
	movl	64(%rsp), %esi          # 4-byte Reload
	movl	%ecx, %edx
	callq	cli_dbgmsg
	jmp	.LBB0_584
.LBB0_852:
	movl	$.L.str.131, %edi
	xorl	%eax, %eax
	movl	%edx, %esi
	callq	cli_dbgmsg
.LBB0_853:                              # %.thread2675
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	jmp	.LBB0_558
.LBB0_842:
	movl	$.L.str.222, %edi
	jmp	.LBB0_875
.LBB0_854:                              # %.thread2701
	movq	96(%rsp), %rsi          # 8-byte Reload
	incl	%esi
	movl	$.L.str.173, %edi
	xorl	%eax, %eax
	movq	%rsi, 96(%rsp)          # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpl	$0, (%rax,%r15)
	jne	.LBB0_560
.LBB0_855:                              # %.thread2701.thread
	movslq	96(%rsp), %rax          # 4-byte Folded Reload
	shlq	$2, %rax
	leaq	36(%rax,%rax,8), %rdi
	callq	cli_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_915
# BB#856:
	movl	160(%rsp), %eax         # 4-byte Reload
	movl	%eax, (%r15)
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	je	.LBB0_859
# BB#857:                               # %.lr.ph2926.preheader
	movl	$1, %eax
	movl	$4, %ecx
	movl	308(%rsp), %edx
.LBB0_858:                              # %.lr.ph2926
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %esi
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	8(%rdi,%rsi), %esi
	decl	%esi
	subl	%edx, %esi
	movl	%eax, %edi
	leaq	(%rdi,%rdi,8), %rdi
	movl	%esi, (%r15,%rdi,4)
	incl	%eax
	addl	$4, %ecx
	cmpl	96(%rsp), %eax          # 4-byte Folded Reload
	jbe	.LBB0_858
.LBB0_859:                              # %._crit_edge2927
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%r14, %rdi
	callq	cli_malloc
	movq	%rax, 48(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_926
# BB#860:
	movq	120(%rsp), %rax         # 8-byte Reload
	cmpl	$0, (%rax)
	je	.LBB0_950
# BB#861:
	movq	200(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_949
# BB#862:                               # %cli_seeksect.exit2604
	movl	%ebp, %edi
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	%r14d, %edx
	callq	cli_readn
	cmpl	%r14d, %eax
	jne	.LBB0_950
# BB#863:
	movl	84(%rsp), %edi
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, 64(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_997
# BB#864:
	movl	835(%rsp), %eax
	movq	104(%rsp), %rcx         # 8-byte Reload
	leal	167(%rcx,%rax), %r13d
	movl	$.L.str.163, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_dbgmsg
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB0_1011
# BB#865:
	movl	$578, %esi              # imm = 0x242
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	open
	movl	%eax, 56(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	js	.LBB0_1014
# BB#866:
	movl	184(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	addq	%rax, %rdi
	movq	200(%rsp), %rax         # 8-byte Reload
	movl	-8(%rax), %eax
	subq	%rax, %rdi
	subl	%ecx, %r14d
	addl	%eax, %r14d
	movl	84(%rsp), %ecx
	movl	308(%rsp), %eax
	movl	56(%rsp), %edx          # 4-byte Reload
	movl	%edx, 16(%rsp)
	movl	%r13d, 8(%rsp)
	movl	%eax, (%rsp)
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%r14d, %edx
	movq	%r15, %r8
	movq	96(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	callq	unfsg_133
	testl	%eax, %eax
	je	.LBB0_1026
# BB#867:
	cmpl	$1, %eax
	jne	.LBB0_1028
# BB#868:
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB0_1037
# BB#869:
	movl	$.L.str.165, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_dbgmsg
	jmp	.LBB0_1038
.LBB0_870:
	movl	$.L.str.148, %edi
.LBB0_871:                              # %.thread2694
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	movq	%r14, %rdi
.LBB0_872:                              # %.thread2694
	callq	free
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	56(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_832
.LBB0_873:
	movl	$.L.str.151, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r15d, %edi
	callq	close
	movq	%r14, %rdi
	callq	unlink
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	cli_multifree
	movq	%r14, %rdi
	callq	free
	testb	%r12b, %r12b
	jne	.LBB0_413
	jmp	.LBB0_459
.LBB0_874:
	movl	$.L.str.214, %edi
.LBB0_875:
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_876:
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	cli_multifree
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	%r14d, %edi
	callq	fsync
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r14d, %edi
	callq	lseek
	movl	$.L.str.138, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r14d, %edi
	movq	%rbx, %rsi
	callq	cli_magic_scandesc
	movl	%eax, %ebx
	movl	%r14d, %edi
.LBB0_877:
	callq	close
	movb	cli_leavetemps_flag(%rip), %al
	cmpl	$1, %ebx
	jne	.LBB0_881
# BB#878:
	testb	%al, %al
	jne	.LBB0_880
# BB#879:
	movq	%r15, %rdi
	callq	unlink
.LBB0_880:
	movq	%r15, %rdi
	jmp	.LBB0_286
.LBB0_881:
	testb	%al, %al
	jne	.LBB0_883
# BB#882:
	movq	%r15, %rdi
	callq	unlink
.LBB0_883:
	movq	%r15, %rdi
	jmp	.LBB0_813
.LBB0_884:
	movl	$.L.str.132, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%rcx, %rdx
	callq	cli_dbgmsg
	jmp	.LBB0_746
.LBB0_937:
	movl	$.L.str.228, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	56(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_938
.LBB0_885:
	movl	$.L.str.243, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_886:
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	cli_multifree
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	%r13d, %edi
	callq	fsync
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r13d, %edi
	callq	lseek
	movl	$.L.str.138, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r13d, %edi
	movq	%rbx, %rsi
	callq	cli_magic_scandesc
	movl	%eax, %ebx
	movl	%r13d, %edi
.LBB0_887:
	callq	close
	movb	cli_leavetemps_flag(%rip), %al
	cmpl	$1, %ebx
	jne	.LBB0_891
.LBB0_888:
	testb	%al, %al
	jne	.LBB0_890
.LBB0_889:
	movq	%r14, %rdi
	callq	unlink
.LBB0_890:
	movq	%r14, %rdi
	jmp	.LBB0_286
.LBB0_891:
	testb	%al, %al
	jne	.LBB0_893
# BB#892:
	movq	%r14, %rdi
	callq	unlink
.LBB0_893:
	movq	%r14, %rdi
	jmp	.LBB0_813
.LBB0_894:
	xorl	%r13d, %r13d
	movl	$.L.str.154, %edi
	xorl	%eax, %eax
	movq	56(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	jmp	.LBB0_244
.LBB0_895:
	movq	%r14, %rdi
	jmp	.LBB0_948
.LBB0_896:                              # %cli_seeksect.exit2618.thread2762
	movl	$.L.str.259, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB0_783
.LBB0_897:
	movl	$.L.str.188, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$upx_inflate2d, %eax
	cmpq	%rax, %r15
	movq	%r15, %r12
	je	.LBB0_919
.LBB0_898:                              # %.thread3124
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r8d
	movl	(%r13), %r9d
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	%eax, (%rsp)
	leaq	84(%rsp), %rcx
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%r14d, %esi
	movq	64(%rsp), %rdx          # 8-byte Reload
	callq	upx_inflate2d
	cmpl	$-1, %eax
	jne	.LBB0_900
# BB#899:
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	21(%rax), %rdi
	leal	-21(%r14), %esi
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r8d
	movl	(%r13), %r9d
	movq	104(%rsp), %rax         # 8-byte Reload
	leal	-21(%rax), %eax
	movl	%eax, (%rsp)
	leaq	84(%rsp), %rcx
	movq	64(%rsp), %rdx          # 8-byte Reload
	callq	upx_inflate2d
	cmpl	$-1, %eax
	je	.LBB0_918
.LBB0_900:
	movl	$.L.str.191, %edi
	jmp	.LBB0_717
.LBB0_901:
	xorl	%r9d, %r9d
	movb	$1, %r8b
	cmpw	$0, 88(%rsp)            # 2-byte Folded Reload
	je	.LBB0_907
# BB#902:                               # %.lr.ph.i2641.preheader
	movq	88(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rdx,8), %rax
	leaq	1(%rdx), %rdi
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	-24(%rdx,%rax,4), %rsi
.LBB0_903:                              # %.lr.ph.i2641
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %eax
	testl	%eax, %eax
	je	.LBB0_906
# BB#904:                               #   in Loop: Header=BB0_903 Depth=1
	movl	%ecx, %edx
	subl	-12(%rsi), %edx
	jb	.LBB0_906
# BB#905:                               #   in Loop: Header=BB0_903 Depth=1
	cmpl	%edx, %eax
	ja	.LBB0_941
.LBB0_906:                              # %.backedge.i2643
                                        #   in Loop: Header=BB0_903 Depth=1
	decq	%rdi
	addq	$-36, %rsi
	cmpq	$1, %rdi
	jg	.LBB0_903
.LBB0_907:                              # %cli_rawaddr.exit2646
	testb	%r8b, %r8b
	je	.LBB0_942
.LBB0_908:                              # %cli_rawaddr.exit2646
	testl	%r9d, %r9d
	je	.LBB0_909
	jmp	.LBB0_942
.LBB0_910:
	movl	$.L.str.159, %edi
	jmp	.LBB0_732
.LBB0_911:
	movl	$.L.str.183, %edi
	movl	$24, %esi
	movl	$13, %ecx
	movq	%r12, %rdx
	callq	cli_memstr
	testq	%rax, %rax
	jne	.LBB0_913
# BB#912:
	movl	$.L.str.183, %edi
	movl	$24, %esi
	movl	$13, %ecx
	movq	%r15, %rdx
	callq	cli_memstr
	testq	%rax, %rax
	je	.LBB0_970
.LBB0_913:
	movl	$.L.str.184, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$upx_inflate2e, %r15d
	jmp	.LBB0_638
.LBB0_914:
	xorl	%esi, %esi
	jmp	.LBB0_925
.LBB0_915:
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	56(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_152
.LBB0_916:                              # %.thread2671
	movq	%r13, %rdi
	callq	free
	movl	$1280, 96(%rsp)         # 4-byte Folded Spill
                                        # imm = 0x500
	jmp	.LBB0_535
.LBB0_917:                              # %.us-lcssa.us
	movq	%r13, %rdi
.LBB0_938:
	callq	free
.LBB0_939:
	movq	40(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_152
.LBB0_918:
	movl	$.L.str.190, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$upx_inflate2e, %eax
	cmpq	%rax, %r12
	je	.LBB0_972
.LBB0_919:                              # %.thread3127
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r8d
	movl	(%r13), %r9d
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	%eax, (%rsp)
	leaq	84(%rsp), %rcx
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%r14d, %esi
	movq	64(%rsp), %rdx          # 8-byte Reload
	callq	upx_inflate2e
	cmpl	$-1, %eax
	jne	.LBB0_921
# BB#920:
	movq	48(%rsp), %rdi          # 8-byte Reload
	addq	$21, %rdi
	addl	$-21, %r14d
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r8d
	movl	(%r13), %r9d
	movq	104(%rsp), %rax         # 8-byte Reload
	leal	-21(%rax), %eax
	movl	%eax, (%rsp)
	leaq	84(%rsp), %rcx
	movl	%r14d, %esi
	movq	64(%rsp), %rdx          # 8-byte Reload
	callq	upx_inflate2e
	cmpl	$-1, %eax
	je	.LBB0_971
.LBB0_921:
	movl	$.L.str.193, %edi
	jmp	.LBB0_717
.LBB0_922:
	movl	$.L.str.150, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_923:
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	56(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_838
.LBB0_924:                              # %cli_seeksect.exit2620.thread2763
	movl	$.L.str.259, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	(%r15), %esi
.LBB0_925:                              # %cli_seeksect.exit2620.thread
	movl	$.L.str.230, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	72(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_695
.LBB0_926:
	movq	40(%rsp), %rdi          # 8-byte Reload
.LBB0_927:                              # %.thread2694
	callq	free
	movq	%r15, %rdi
	jmp	.LBB0_152
.LBB0_928:                              # %._crit_edge3094
	movl	120(%rsp), %edi         # 4-byte Reload
.LBB0_929:
	callq	cli_malloc
	movq	%rax, 48(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_939
# BB#930:
	movl	%ebp, %edi
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	120(%rsp), %r12d        # 4-byte Reload
	movl	%r12d, %edx
	callq	cli_readn
	cmpl	%r12d, %eax
	jne	.LBB0_940
# BB#931:                               # %.preheader2819
	movl	120(%rsp), %eax         # 4-byte Reload
	addl	$-2, %eax
	je	.LBB0_973
# BB#932:                               # %.lr.ph2918
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	4(%rcx,%r13,4), %rcx
	xorl	%r12d, %r12d
	movl	308(%rsp), %r8d
	xorl	%r13d, %r13d
.LBB0_933:                              # =>This Inner Loop Header: Depth=1
	movl	%r12d, %esi
	movq	48(%rsp), %rdx          # 8-byte Reload
	movsbl	(%rdx,%rsi), %edi
	leal	1(%r12), %esi
	movsbl	(%rdx,%rsi), %esi
	shll	$8, %esi
	orl	%edi, %esi
	leal	-1(%rsi), %edi
	cmpl	$2, %edi
	jb	.LBB0_977
# BB#934:                               #   in Loop: Header=BB0_933 Depth=1
	shll	$12, %esi
	addl	$-8192, %esi            # imm = 0xE000
	subl	%r8d, %esi
	subl	-4(%rcx), %esi
	jb	.LBB0_976
# BB#935:                               #   in Loop: Header=BB0_933 Depth=1
	cmpl	(%rcx), %esi
	jae	.LBB0_976
# BB#936:                               #   in Loop: Header=BB0_933 Depth=1
	incl	%r13d
	addl	$2, %r12d
	cmpl	%eax, %r12d
	jb	.LBB0_933
	jmp	.LBB0_977
.LBB0_940:
	movl	$.L.str.170, %edi
	xorl	%eax, %eax
	movl	120(%rsp), %esi         # 4-byte Reload
	jmp	.LBB0_579
.LBB0_941:                              # %cli_rawaddr.exit2646.thread
	addl	-4(%rsi), %edx
	movl	%edx, %r9d
.LBB0_942:
	movl	%r9d, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_909
# BB#943:
	leaq	256(%rsp), %rsi
	movl	$5, %edx
	movl	%ebp, %edi
	callq	cli_readn
	cmpl	$5, %eax
	jne	.LBB0_909
# BB#944:
	movl	257(%rsp), %eax
	movq	104(%rsp), %rcx         # 8-byte Reload
	leal	639(%rcx,%rax), %ebp
	movl	$.L.str.249, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_968
# BB#945:
	movl	$578, %esi              # imm = 0x242
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	open
	movl	%eax, %r13d
	testl	%r13d, %r13d
	js	.LBB0_974
# BB#946:
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %ecx
	movl	308(%rsp), %r8d
	movl	%r13d, (%rsp)
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	movl	%ebp, %r9d
	callq	unspack
	testl	%eax, %eax
	je	.LBB0_1008
# BB#947:
	movl	$.L.str.253, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r13d, %edi
	callq	close
	movq	%r15, %rdi
	callq	unlink
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	cli_multifree
	movq	%r15, %rdi
	jmp	.LBB0_948
.LBB0_909:
	movq	%r14, %rdi
	callq	free
	movq	%r12, %rdi
.LBB0_948:                              # %.thread2774
	callq	free
	jmp	.LBB0_812
.LBB0_949:                              # %cli_seeksect.exit2604.thread2703
	movl	$.L.str.259, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_950:                              # %cli_seeksect.exit2604.thread
	movl	$.L.str.156, %edi
	xorl	%eax, %eax
	movq	72(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%r15, %rdi
	jmp	.LBB0_580
.LBB0_951:
	addl	%eax, %r13d
	addl	$132, %r13d
	addl	%edx, %ecx
	cmpl	%ecx, %r13d
	ja	.LBB0_851
# BB#952:
	cmpl	%edx, %r13d
	jbe	.LBB0_851
# BB#953:
	leal	133(%r12,%rax), %r15d
	movq	40(%rsp), %rax          # 8-byte Reload
	subl	(%rax), %r15d
.LBB0_954:
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB0_965
# BB#955:
	movl	$578, %esi              # imm = 0x242
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	open
	movl	%eax, %r13d
	testl	%r13d, %r13d
	js	.LBB0_959
# BB#956:
	movl	84(%rsp), %r8d
	movl	308(%rsp), %r9d
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	movl	%r13d, 32(%rsp)
	movl	%r15d, 8(%rsp)
	movl	%eax, (%rsp)
	movq	$0, 24(%rsp)
	movq	$0, 16(%rsp)
	movq	72(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	56(%rsp), %rsi          # 8-byte Reload
	movl	%r14d, %edx
	movl	64(%rsp), %ecx          # 4-byte Reload
	callq	unmew11
	cmpl	$1, %eax
	jne	.LBB0_966
# BB#957:
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB0_998
# BB#958:
	movl	$.L.str.136, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_dbgmsg
	jmp	.LBB0_999
.LBB0_959:
	movl	$.L.str.135, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_dbgmsg
	movq	%r12, %rdi
	jmp	.LBB0_872
.LBB0_960:
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_965
# BB#961:
	movl	$578, %esi              # imm = 0x242
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	open
	movl	%eax, %r13d
	testl	%r13d, %r13d
	js	.LBB0_1010
# BB#962:
	movl	84(%rsp), %edx
	movl	%r13d, %edi
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	write
	movl	84(%rsp), %esi
	cmpl	%esi, %eax
	jne	.LBB0_1013
# BB#963:
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB0_1029
# BB#964:
	movl	$.L.str.233, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	jmp	.LBB0_1030
.LBB0_965:
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	56(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_792
.LBB0_966:
	movl	$.L.str.139, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r13d, %edi
	callq	close
	movq	%r12, %rdi
	callq	unlink
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	cli_multifree
	movq	%r12, %rdi
	callq	free
	jmp	.LBB0_558
.LBB0_967:
	movl	$.L.str.160, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	cli_dbgmsg
	jmp	.LBB0_733
.LBB0_968:
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r14, %rsi
.LBB0_969:                              # %.thread2694
	movq	40(%rsp), %rdx          # 8-byte Reload
	callq	cli_multifree
	movl	$-114, %r13d
	jmp	.LBB0_23
.LBB0_970:
	xorl	%r15d, %r15d
	jmp	.LBB0_736
.LBB0_971:                              # %.thread2746
	movl	$.L.str.192, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_972:                              # %.thread2752
	movl	$.L.str.194, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	free
	cmpl	$199, 140(%rsp)         # 4-byte Folded Reload
	ja	.LBB0_587
	jmp	.LBB0_812
.LBB0_973:
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	jmp	.LBB0_977
.LBB0_974:
	movl	$.L.str.250, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	movq	%r15, %rdi
	callq	free
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r14, %rsi
.LBB0_975:                              # %.thread2694
	movq	40(%rsp), %rdx          # 8-byte Reload
	callq	cli_multifree
	movl	$-123, %r13d
	jmp	.LBB0_23
.LBB0_976:
	movq	%r13, %rsi
	incl	%esi
	movl	$.L.str.173, %edi
	xorl	%eax, %eax
	movq	%rsi, %r13
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
.LBB0_977:                              # %.thread2714
	movl	120(%rsp), %eax         # 4-byte Reload
	addl	$-10, %eax
	cmpl	%eax, %r12d
	jae	.LBB0_994
# BB#978:
	movl	%r12d, %eax
	movq	48(%rsp), %rcx          # 8-byte Reload
	cmpl	$2, 6(%rcx,%rax)
	jne	.LBB0_994
# BB#979:
	movslq	%r13d, %rax
	shlq	$2, %rax
	leaq	36(%rax,%rax,8), %rdi
	callq	cli_malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB0_993
# BB#980:
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	12(%rax,%r15,4), %r15
	movl	64(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%r12)
	leal	-1(%r13), %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
.LBB0_981:                              # =>This Inner Loop Header: Depth=1
	leal	-1(%rdx), %esi
	movq	48(%rsp), %rax          # 8-byte Reload
	movsbl	(%rax,%rsi), %esi
	movl	%edx, %edi
	movsbl	(%rax,%rdi), %edi
	shll	$8, %edi
	orl	%esi, %edi
	shll	$12, %edi
	movl	$-8192, %esi            # imm = 0xE000
	subl	308(%rsp), %esi
	addl	%edi, %esi
	incl	%ecx
	leaq	(%rcx,%rcx,8), %rdi
	movl	%esi, (%r12,%rdi,4)
	addl	$2, %edx
	cmpl	%r8d, %ecx
	jbe	.LBB0_981
# BB#982:
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	cli_malloc
	movq	%rax, 48(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_1024
# BB#983:
	cmpl	$0, (%r15)
	je	.LBB0_1036
# BB#984:
	movq	160(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB0_1035
# BB#985:                               # %cli_seeksect.exit2612
	movl	%ebp, %edi
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsp), %r15          # 8-byte Reload
	movl	%r15d, %edx
	callq	cli_readn
	cmpl	%r15d, %eax
	jne	.LBB0_1036
# BB#986:
	movl	84(%rsp), %edi
	movl	$1, %esi
	callq	cli_calloc
	movq	%rax, 64(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_1041
# BB#987:
	cmpb	$-24, 688(%rsp)
	movl	$224, %eax
	movl	$218, %ecx
	cmovel	%eax, %ecx
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx), %rax
	movl	%r14d, %edx
	movl	2(%rdx,%rax), %eax
	addl	104(%rsp), %eax         # 4-byte Folded Reload
	leal	6(%rcx,%rax), %r14d
	movl	$.L.str.163, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	cli_dbgmsg
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_1044
# BB#988:
	movl	$578, %esi              # imm = 0x242
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	open
	movl	%eax, 120(%rsp)         # 4-byte Spill
	testl	%eax, %eax
	js	.LBB0_1045
# BB#989:
	movl	96(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	addq	%rax, %rdi
	movq	184(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %eax
	subq	%rax, %rdi
	movq	56(%rsp), %rdx          # 8-byte Reload
	subl	%ecx, %edx
	addl	%eax, %edx
	movl	84(%rsp), %ecx
	movl	308(%rsp), %eax
	movl	120(%rsp), %esi         # 4-byte Reload
	movl	%esi, 16(%rsp)
	movl	%r14d, 8(%rsp)
	movl	%eax, (%rsp)
	movq	64(%rsp), %rsi          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%r12, %r8
	movl	%r13d, %r9d
	callq	unfsg_133
	testl	%eax, %eax
	je	.LBB0_1054
# BB#990:
	cmpl	$1, %eax
	jne	.LBB0_1055
# BB#991:
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB0_1056
# BB#992:
	movl	$.L.str.165, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	jmp	.LBB0_1057
.LBB0_993:
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	48(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_152
.LBB0_994:
	movq	48(%rsp), %rdi          # 8-byte Reload
.LBB0_995:                              # %.critedge85..critedge85.thread_crit_edge
	callq	free
	jmp	.LBB0_584
.LBB0_996:
	movl	$.L.str.161, %edi
	jmp	.LBB0_732
.LBB0_997:
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	48(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_927
.LBB0_998:
	movl	$.L.str.137, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_999:
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	cli_multifree
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	%r13d, %edi
	callq	fsync
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r13d, %edi
	callq	lseek
	movl	$.L.str.138, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r13d, %edi
	movq	%rbx, %rsi
	callq	cli_magic_scandesc
	movl	%eax, %ebx
	movl	%r13d, %edi
.LBB0_1000:
	callq	close
	movb	cli_leavetemps_flag(%rip), %al
	cmpl	$1, %ebx
	jne	.LBB0_1004
# BB#1001:
	testb	%al, %al
	jne	.LBB0_1003
# BB#1002:
	movq	%r12, %rdi
	callq	unlink
.LBB0_1003:
	movq	%r12, %rdi
	jmp	.LBB0_286
.LBB0_1004:
	testb	%al, %al
	jne	.LBB0_1006
# BB#1005:
	movq	%r12, %rdi
	callq	unlink
.LBB0_1006:
	movq	%r12, %rdi
	jmp	.LBB0_813
.LBB0_1007:
	movl	$.L.str.162, %edi
	jmp	.LBB0_732
.LBB0_1008:
	cmpb	$0, cli_leavetemps_flag(%rip)
	je	.LBB0_1016
# BB#1009:
	movl	$.L.str.251, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	jmp	.LBB0_1017
.LBB0_1010:
	movl	$.L.str.231, %edi
	jmp	.LBB0_871
.LBB0_1011:
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
.LBB0_1012:                             # %.thread2694
	movq	40(%rsp), %rcx          # 8-byte Reload
	callq	cli_multifree
	movl	$-114, %r13d
	jmp	.LBB0_23
.LBB0_1013:
	movl	$.L.str.232, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r13d, %edi
	callq	close
	movq	%r14, %rdi
	callq	free
	jmp	.LBB0_783
.LBB0_1014:
	movl	$.L.str.164, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_dbgmsg
	movq	%r12, %rdi
	callq	free
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
.LBB0_1015:                             # %.thread2694
	movq	40(%rsp), %rcx          # 8-byte Reload
	callq	cli_multifree
	movl	$-123, %r13d
	jmp	.LBB0_23
.LBB0_1016:
	movl	$.L.str.252, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_1017:
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	cli_multifree
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	%r13d, %edi
	callq	fsync
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r13d, %edi
	callq	lseek
	movl	$.L.str.138, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r13d, %edi
	movq	%rbx, %rsi
	callq	cli_magic_scandesc
	movl	%eax, %ebx
	movl	%r13d, %edi
	callq	close
	movb	cli_leavetemps_flag(%rip), %al
	cmpl	$1, %ebx
	jne	.LBB0_1021
# BB#1018:
	testb	%al, %al
	jne	.LBB0_1020
# BB#1019:
	movq	%r15, %rdi
	callq	unlink
.LBB0_1020:
	movq	%r15, %rdi
	jmp	.LBB0_286
.LBB0_1021:
	testb	%al, %al
	jne	.LBB0_1023
# BB#1022:
	movq	%r15, %rdi
	callq	unlink
.LBB0_1023:
	movq	%r15, %rdi
	jmp	.LBB0_813
.LBB0_1024:
	movq	40(%rsp), %rdi          # 8-byte Reload
.LBB0_1025:                             # %.thread2694
	callq	free
	movq	%r12, %rdi
	jmp	.LBB0_152
.LBB0_1026:
	movl	$.L.str.167, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	56(%rsp), %edi          # 4-byte Reload
	callq	close
	movq	%r12, %rdi
	callq	unlink
	movq	%r12, %rdi
	callq	free
	movq	%r15, %rdi
.LBB0_1027:                             # %.thread2749
	callq	free
	jmp	.LBB0_718
.LBB0_1028:
	movl	$.L.str.168, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	56(%rsp), %edi          # 4-byte Reload
	callq	close
	movq	%r12, %rdi
	callq	unlink
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	callq	cli_multifree
	movq	%r12, %rdi
	jmp	.LBB0_561
.LBB0_1029:
	movl	$.L.str.234, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_1030:
	movl	%r13d, %edi
	callq	fsync
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r13d, %edi
	callq	lseek
	movl	%r13d, %edi
	movq	%rbx, %rsi
	callq	cli_magic_scandesc
	cmpl	$1, %eax
	jne	.LBB0_1032
# BB#1031:
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	%r13d, %edi
	callq	close
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_890
	jmp	.LBB0_889
.LBB0_1032:
	movl	%r13d, %edi
	callq	close
	cmpb	$0, cli_leavetemps_flag(%rip)
	jne	.LBB0_1034
# BB#1033:
	movq	%r14, %rdi
	callq	unlink
.LBB0_1034:
	movq	%r14, %rdi
	callq	free
	jmp	.LBB0_612
.LBB0_1035:                             # %cli_seeksect.exit2612.thread2715
	movl	$.L.str.259, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_1036:                             # %cli_seeksect.exit2612.thread
	movl	$.L.str.174, %edi
	xorl	%eax, %eax
	movq	72(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%r12, %rdi
	jmp	.LBB0_580
.LBB0_1037:
	movl	$.L.str.166, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_1038:
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	callq	cli_multifree
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	56(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %edi
	callq	fsync
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	movl	$.L.str.138, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	cli_magic_scandesc
	movl	%eax, %ebx
	movl	%ebp, %edi
	jmp	.LBB0_1000
.LBB0_1039:
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB0_969
.LBB0_1040:
	movl	$.L.str.164, %edi
	xorl	%eax, %eax
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	free
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB0_975
.LBB0_1041:
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	48(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_1025
.LBB0_1042:                             # %.critedge71.thread
	movl	$.L.str.167, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	160(%rsp), %edi         # 4-byte Reload
	callq	close
	movq	96(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	unlink
	movq	%rbp, %rdi
	jmp	.LBB0_1027
.LBB0_1043:
	movl	$.L.str.168, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	160(%rsp), %edi         # 4-byte Reload
	callq	close
	movq	96(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	callq	unlink
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_multifree
	movq	%r14, %rdi
	jmp	.LBB0_734
.LBB0_1044:
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	jmp	.LBB0_1012
.LBB0_1045:
	movl	$.L.str.164, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_dbgmsg
	movq	%r15, %rdi
	callq	free
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	jmp	.LBB0_1015
.LBB0_1046:
	movl	$.L.str.166, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_1047:
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	cli_multifree
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	160(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, %edi
	callq	fsync
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	movl	$.L.str.138, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	cli_magic_scandesc
	movl	%eax, %ebx
	movl	%ebp, %edi
	callq	close
	movb	cli_leavetemps_flag(%rip), %al
	cmpl	$1, %ebx
	jne	.LBB0_1051
# BB#1048:
	testb	%al, %al
	jne	.LBB0_1050
# BB#1049:
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	unlink
.LBB0_1050:
	movq	96(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_286
.LBB0_1051:
	testb	%al, %al
	jne	.LBB0_1053
# BB#1052:
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	unlink
.LBB0_1053:
	movq	96(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_813
.LBB0_1054:
	movl	$.L.str.167, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	120(%rsp), %edi         # 4-byte Reload
	callq	close
	movq	%r15, %rdi
	callq	unlink
	movq	%r15, %rdi
	callq	free
	movq	%r12, %rdi
	jmp	.LBB0_1027
.LBB0_1055:
	movl	$.L.str.168, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	120(%rsp), %edi         # 4-byte Reload
	callq	close
	movq	%r15, %rdi
	callq	unlink
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	cli_multifree
	movq	%r15, %rdi
	jmp	.LBB0_995
.LBB0_1056:
	movl	$.L.str.166, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_1057:
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	cli_multifree
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	120(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, %edi
	callq	fsync
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%ebp, %edi
	callq	lseek
	movl	$.L.str.138, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	cli_magic_scandesc
	movl	%eax, %ebx
	movl	%ebp, %edi
	jmp	.LBB0_877
.Lfunc_end0:
	.size	cli_scanpe, .Lfunc_end0-cli_scanpe
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_29
	.quad	.LBB0_69
	.quad	.LBB0_67
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_65
	.quad	.LBB0_54
	.quad	.LBB0_72
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_63
	.quad	.LBB0_54
	.quad	.LBB0_71
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_75
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_66
	.quad	.LBB0_62
	.quad	.LBB0_73
	.quad	.LBB0_54
	.quad	.LBB0_70
	.quad	.LBB0_54
	.quad	.LBB0_68
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_74
	.quad	.LBB0_54
	.quad	.LBB0_61
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_64
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_54
	.quad	.LBB0_60
.LJTI0_1:
	.quad	.LBB0_104
	.quad	.LBB0_118
	.quad	.LBB0_117
	.quad	.LBB0_116
	.quad	.LBB0_153
	.quad	.LBB0_114
	.quad	.LBB0_153
	.quad	.LBB0_115
	.quad	.LBB0_119
	.quad	.LBB0_120
	.quad	.LBB0_122
	.quad	.LBB0_121
	.quad	.LBB0_113
.LJTI0_2:
	.quad	.LBB0_302
	.quad	.LBB0_307
	.quad	.LBB0_310
	.quad	.LBB0_302
	.quad	.LBB0_313
	.quad	.LBB0_314
	.quad	.LBB0_317
	.quad	.LBB0_319

	.text
	.p2align	4, 0x90
	.type	cli_multifree,@function
cli_multifree:                          # @cli_multifree
	.cfi_startproc
# BB#0:
	subq	$216, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 224
	testb	%al, %al
	je	.LBB1_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB1_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rsi, 40(%rsp)
	callq	free
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	224(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$8, (%rsp)
	jmp	.LBB1_3
	.p2align	4, 0x90
.LBB1_7:                                #   in Loop: Header=BB1_3 Depth=1
	callq	free
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movslq	(%rsp), %rcx
	cmpq	$40, %rcx
	ja	.LBB1_5
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=1
	movq	%rcx, %rax
	addq	16(%rsp), %rax
	leal	8(%rcx), %ecx
	movl	%ecx, (%rsp)
	jmp	.LBB1_6
	.p2align	4, 0x90
.LBB1_5:                                #   in Loop: Header=BB1_3 Depth=1
	movq	8(%rsp), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, 8(%rsp)
.LBB1_6:                                #   in Loop: Header=BB1_3 Depth=1
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_7
# BB#8:
	addq	$216, %rsp
	retq
.Lfunc_end1:
	.size	cli_multifree, .Lfunc_end1-cli_multifree
	.cfi_endproc

	.globl	cli_peheader
	.p2align	4, 0x90
	.type	cli_peheader,@function
cli_peheader:                           # @cli_peheader
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$440, %rsp              # imm = 0x1B8
.Lcfi20:
	.cfi_def_cfa_offset 496
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movl	%edi, %ebx
	movl	$.L.str.254, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	leaq	296(%rsp), %rdx
	movl	$1, %edi
	movl	%ebx, %esi
	callq	__fxstat
	cmpl	$-1, %eax
	je	.LBB2_1
# BB#3:
	movq	344(%rsp), %rbp
	subq	8(%r12), %rbp
	leaq	2(%rsp), %rsi
	movl	$2, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB2_4
# BB#5:
	movzwl	2(%rsp), %eax
	cmpl	$19802, %eax            # imm = 0x4D5A
	je	.LBB2_8
# BB#6:
	movzwl	%ax, %eax
	cmpl	$23117, %eax            # imm = 0x5A4D
	jne	.LBB2_7
.LBB2_8:
	movl	$58, %esi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek
	leaq	20(%rsp), %rsi
	movl	$4, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB2_9
# BB#10:
	movl	20(%rsp), %esi
	testq	%rsi, %rsi
	je	.LBB2_11
# BB#12:
	addq	8(%r12), %rsi
	xorl	%edx, %edx
	movl	%ebx, %edi
	callq	lseek
	testq	%rax, %rax
	js	.LBB2_13
# BB#14:
	leaq	32(%rsp), %rsi
	movl	$24, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$24, %eax
	jne	.LBB2_15
# BB#16:
	cmpl	$17744, 32(%rsp)        # imm = 0x4550
	jne	.LBB2_17
# BB#18:
	movzwl	38(%rsp), %eax
	movw	%ax, 4(%r12)
	decl	%eax
	movzwl	%ax, %eax
	movl	$-1, %r13d
	cmpl	$95, %eax
	ja	.LBB2_83
# BB#19:
	movzwl	52(%rsp), %eax
	cmpl	$223, %eax
	ja	.LBB2_22
# BB#20:
	movl	$.L.str.48, %edi
	jmp	.LBB2_21
.LBB2_1:
	movl	$.L.str.84, %edi
	jmp	.LBB2_2
.LBB2_4:
	movl	$.L.str.1, %edi
	jmp	.LBB2_2
.LBB2_7:
	movl	$.L.str.2, %edi
	jmp	.LBB2_2
.LBB2_9:
	movl	$.L.str.3, %edi
	jmp	.LBB2_2
.LBB2_11:
	movl	$.L.str.6, %edi
	jmp	.LBB2_2
.LBB2_13:
	movl	$.L.str.7, %edi
	jmp	.LBB2_2
.LBB2_15:
	movl	$.L.str.8, %edi
	jmp	.LBB2_2
.LBB2_17:
	movl	$.L.str.9, %edi
.LBB2_2:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-1, %r13d
.LBB2_83:
	movl	%r13d, %eax
	addq	$440, %rsp              # imm = 0x1B8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_22:
	leaq	56(%rsp), %rsi
	movl	$224, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$224, %eax
	jne	.LBB2_23
# BB#24:
	movzwl	52(%rsp), %esi
	movzwl	56(%rsp), %eax
	cmpl	$523, %eax              # imm = 0x20B
	jne	.LBB2_28
# BB#25:
	cmpl	$240, %esi
	jne	.LBB2_26
# BB#27:
	leaq	280(%rsp), %rsi
	movl	$16, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$16, %eax
	je	.LBB2_30
.LBB2_23:
	movl	$.L.str.49, %edi
.LBB2_21:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB2_83
.LBB2_28:
	cmpl	$224, %esi
	je	.LBB2_30
# BB#29:
	addq	$-224, %rsi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek
.LBB2_30:
	movl	116(%rsp), %eax
	movl	88(%rsp), %r14d
	movl	92(%rsp), %r15d
	testl	%r14d, %r14d
	je	.LBB2_32
# BB#31:
	xorl	%edx, %edx
	divl	%r14d
	cmpl	$1, %edx
	sbbl	$-1, %eax
	imull	%r14d, %eax
.LBB2_32:
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movzwl	4(%r12), %edi
	movl	$36, %esi
	callq	cli_calloc
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.LBB2_33
# BB#34:
	movzwl	4(%r12), %edi
	movl	$40, %esi
	callq	cli_calloc
	testq	%rax, %rax
	je	.LBB2_35
# BB#36:
	movzwl	4(%r12), %ecx
	shll	$3, %ecx
	leal	(%rcx,%rcx,4), %edx
	movl	%ebx, %edi
	movq	%rax, %rsi
	movq	%rax, %rbx
	callq	cli_readn
	cltq
	movzwl	4(%r12), %r8d
	leaq	(,%r8,8), %rcx
	leaq	(%rcx,%rcx,4), %rcx
	cmpq	%rcx, %rax
	jne	.LBB2_39
# BB#37:                                # %.preheader
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movl	$512, %r9d              # imm = 0x200
	cmpl	$512, %r15d             # imm = 0x200
	je	.LBB2_42
# BB#38:                                # %.lr.ph176
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	20(%rax), %rsi
	xorl	%edi, %edi
	jmp	.LBB2_48
.LBB2_49:                               #   in Loop: Header=BB2_40 Depth=2
	cmpl	$0, -4(%rbx)
	je	.LBB2_52
# BB#50:                                #   in Loop: Header=BB2_40 Depth=2
	movl	(%rbx), %ecx
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%r15d
	testl	%edx, %edx
	je	.LBB2_52
# BB#51:                                #   in Loop: Header=BB2_40 Depth=2
	andl	$511, %ecx              # imm = 0x1FF
	je	.LBB2_42
.LBB2_52:                               #   in Loop: Header=BB2_40 Depth=2
	incq	%rdi
	addq	$40, %rbx
	cmpl	$512, %r15d             # imm = 0x200
	jne	.LBB2_40
	jmp	.LBB2_42
.LBB2_47:                               #   in Loop: Header=BB2_48 Depth=1
	xorl	%r15d, %r15d
.LBB2_48:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_40 Depth 2
	leaq	(%rdi,%rdi,4), %rax
	incq	%rdi
	leaq	(%rsi,%rax,8), %rbx
.LBB2_40:                               #   Parent Loop BB2_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdi), %rax
	cmpq	%r8, %rax
	jge	.LBB2_41
# BB#46:                                #   in Loop: Header=BB2_40 Depth=2
	testl	%r15d, %r15d
	jne	.LBB2_49
	jmp	.LBB2_47
.LBB2_26:
	movl	$.L.str.50, %edi
	jmp	.LBB2_21
.LBB2_33:
	movl	$.L.str.85, %edi
	jmp	.LBB2_21
.LBB2_35:
	movl	$.L.str.85, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB2_80
.LBB2_39:
	movl	$.L.str.86, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.87, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	jmp	.LBB2_79
.LBB2_41:
	movl	%r15d, %r9d
.LBB2_42:                               # %.critedge.preheader
	testw	%r8w, %r8w
	movq	16(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	je	.LBB2_68
# BB#43:                                # %.lr.ph
	movq	8(%rsp), %rbx           # 8-byte Reload
	addq	$8, %rbx
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	12(%rax), %r10
	xorl	%r15d, %r15d
.LBB2_44:                               # =>This Inner Loop Header: Depth=1
	testl	%r14d, %r14d
	movl	4(%rbx), %ecx
	je	.LBB2_45
# BB#53:                                #   in Loop: Header=BB2_44 Depth=1
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%r14d
	subl	%edx, %ecx
	movl	%ecx, -12(%r10)
	movl	(%rbx), %eax
	xorl	%edx, %edx
	divl	%r14d
	movl	%eax, %edi
	cmpl	$1, %edx
	sbbl	$-1, %edi
	imull	%r14d, %edi
	jmp	.LBB2_54
.LBB2_45:                               #   in Loop: Header=BB2_44 Depth=1
	movl	%ecx, -12(%r10)
	movl	(%rbx), %edi
.LBB2_54:                               #   in Loop: Header=BB2_44 Depth=1
	movl	%edi, -8(%r10)
	movl	12(%rbx), %r11d
	testl	%r9d, %r9d
	je	.LBB2_55
# BB#56:                                #   in Loop: Header=BB2_44 Depth=1
	xorl	%edx, %edx
	movl	%r11d, %eax
	divl	%r9d
	subl	%edx, %r11d
	movl	%r11d, -4(%r10)
	movl	8(%rbx), %ecx
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%r9d
	movl	%eax, %esi
	cmpl	$1, %edx
	sbbl	$-1, %esi
	imull	%r9d, %esi
	jmp	.LBB2_57
.LBB2_55:                               #   in Loop: Header=BB2_44 Depth=1
	movl	%r11d, -4(%r10)
	movl	8(%rbx), %ecx
	movl	%ecx, %esi
.LBB2_57:                               #   in Loop: Header=BB2_44 Depth=1
	movl	%esi, (%r10)
	testl	%edi, %edi
	jne	.LBB2_62
# BB#58:                                #   in Loop: Header=BB2_44 Depth=1
	testl	%esi, %esi
	je	.LBB2_67
# BB#59:                                #   in Loop: Header=BB2_44 Depth=1
	testl	%r14d, %r14d
	je	.LBB2_61
# BB#60:                                #   in Loop: Header=BB2_44 Depth=1
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%r14d
	cmpl	$1, %edx
	movl	%eax, %ecx
	sbbl	$-1, %ecx
	imull	%r14d, %ecx
.LBB2_61:                               #   in Loop: Header=BB2_44 Depth=1
	movl	%ecx, -8(%r10)
.LBB2_62:                               #   in Loop: Header=BB2_44 Depth=1
	testl	%esi, %esi
	je	.LBB2_67
# BB#63:                                #   in Loop: Header=BB2_44 Depth=1
	testl	%ebp, %ebp
	je	.LBB2_66
# BB#64:                                #   in Loop: Header=BB2_44 Depth=1
	cmpl	%ebp, %esi
	ja	.LBB2_66
# BB#65:                                #   in Loop: Header=BB2_44 Depth=1
	movl	-4(%r10), %eax
	leal	-1(%rsi,%rax), %eax
	cmpl	%ebp, %eax
	jb	.LBB2_67
.LBB2_66:                               # %._crit_edge
                                        #   in Loop: Header=BB2_44 Depth=1
	movl	%r11d, %eax
	movl	%ebp, %ecx
	subl	%r11d, %ecx
	cmpq	%rax, %rbp
	movl	$0, %eax
	cmovbel	%eax, %ecx
	movl	%ecx, (%r10)
.LBB2_67:                               # %.critedge
                                        #   in Loop: Header=BB2_44 Depth=1
	incq	%r15
	addq	$40, %rbx
	addq	$36, %r10
	cmpq	%r8, %r15
	jl	.LBB2_44
.LBB2_68:                               # %.critedge._crit_edge
	movl	72(%rsp), %edx
	movl	%edx, (%r12)
	cmpl	4(%rsp), %edx           # 4-byte Folded Reload
	jae	.LBB2_70
# BB#69:
	xorl	%eax, %eax
	cmpq	%rbp, %rdx
	setae	%r9b
	cmovbl	%edx, %eax
	jmp	.LBB2_76
.LBB2_70:
	xorl	%eax, %eax
	movb	$1, %r9b
	testw	%r8w, %r8w
	je	.LBB2_76
# BB#71:                                # %.lr.ph.i.preheader
	leaq	(%r8,%r8,8), %rsi
	leaq	1(%r8), %rdi
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	-24(%rcx,%rsi,4), %rsi
.LBB2_72:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %ebp
	testl	%ebp, %ebp
	je	.LBB2_75
# BB#73:                                #   in Loop: Header=BB2_72 Depth=1
	movl	-12(%rsi), %ecx
	movl	%edx, %ebx
	subl	%ecx, %ebx
	jb	.LBB2_75
# BB#74:                                #   in Loop: Header=BB2_72 Depth=1
	cmpl	%ebx, %ebp
	ja	.LBB2_81
.LBB2_75:                               # %.backedge.i
                                        #   in Loop: Header=BB2_72 Depth=1
	decq	%rdi
	addq	$-36, %rsi
	cmpq	$1, %rdi
	jg	.LBB2_72
.LBB2_76:                               # %cli_rawaddr.exit
	movl	%eax, (%r12)
	testb	%r9b, %r9b
	je	.LBB2_82
# BB#77:                                # %cli_rawaddr.exit
	testl	%eax, %eax
	jne	.LBB2_82
# BB#78:
	movl	$.L.str.255, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB2_79:
	callq	free
.LBB2_80:
	movq	16(%r12), %rdi
	callq	free
	movq	$0, 16(%r12)
	jmp	.LBB2_83
.LBB2_81:                               # %cli_rawaddr.exit.thread
	addl	-4(%rsi), %ebx
	movl	%ebx, (%r12)
.LBB2_82:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	free
	xorl	%r13d, %r13d
	jmp	.LBB2_83
.Lfunc_end2:
	.size	cli_peheader, .Lfunc_end2-cli_peheader
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"cli_scanpe: ctx == NULL\n"
	.size	.L.str, 25

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Can't read DOS signature\n"
	.size	.L.str.1, 26

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Invalid DOS signature\n"
	.size	.L.str.2, 23

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Can't read new header address\n"
	.size	.L.str.3, 31

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Broken.Executable"
	.size	.L.str.4, 18

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"e_lfanew == %d\n"
	.size	.L.str.5, 16

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Not a PE file\n"
	.size	.L.str.6, 15

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Can't lseek to e_lfanew\n"
	.size	.L.str.7, 25

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Can't read file header\n"
	.size	.L.str.8, 24

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Invalid PE signature (probably NE file)\n"
	.size	.L.str.9, 41

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"File type: DLL\n"
	.size	.L.str.10, 16

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"File type: Executable\n"
	.size	.L.str.11, 23

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Machine type: Unknown\n"
	.size	.L.str.12, 23

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Machine type: 80386\n"
	.size	.L.str.13, 21

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Machine type: 80486\n"
	.size	.L.str.14, 21

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Machine type: 80586\n"
	.size	.L.str.15, 21

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Machine type: R30000 (big-endian)\n"
	.size	.L.str.16, 35

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Machine type: R3000\n"
	.size	.L.str.17, 21

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Machine type: R4000\n"
	.size	.L.str.18, 21

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Machine type: R10000\n"
	.size	.L.str.19, 22

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Machine type: DEC Alpha AXP\n"
	.size	.L.str.20, 29

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"Machine type: DEC Alpha AXP 64bit\n"
	.size	.L.str.21, 35

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Machine type: PowerPC\n"
	.size	.L.str.22, 23

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"Machine type: IA64\n"
	.size	.L.str.23, 20

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"Machine type: M68k\n"
	.size	.L.str.24, 20

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"Machine type: MIPS16\n"
	.size	.L.str.25, 22

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"Machine type: MIPS+FPU\n"
	.size	.L.str.26, 24

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"Machine type: MIPS16+FPU\n"
	.size	.L.str.27, 26

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Machine type: Hitachi SH3\n"
	.size	.L.str.28, 27

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"Machine type: Hitachi SH3-DSP\n"
	.size	.L.str.29, 31

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"Machine type: Hitachi SH3-E\n"
	.size	.L.str.30, 29

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Machine type: Hitachi SH4\n"
	.size	.L.str.31, 27

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"Machine type: Hitachi SH5\n"
	.size	.L.str.32, 27

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"Machine type: ARM\n"
	.size	.L.str.33, 19

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"Machine type: THUMB\n"
	.size	.L.str.34, 21

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"Machine type: AM33\n"
	.size	.L.str.35, 20

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"Machine type: Infineon TriCore\n"
	.size	.L.str.36, 32

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"Machine type: CEF\n"
	.size	.L.str.37, 19

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"Machine type: EFI Byte Code\n"
	.size	.L.str.38, 29

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"Machine type: M32R\n"
	.size	.L.str.39, 20

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"Machine type: CEE\n"
	.size	.L.str.40, 19

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"Machine type: AMD64\n"
	.size	.L.str.41, 21

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"Unknown machine type in PE header (0x%x)\n"
	.size	.L.str.42, 42

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"PE file contains %d sections\n"
	.size	.L.str.43, 30

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"PE file contains no sections\n"
	.size	.L.str.44, 30

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"NumberOfSections: %d\n"
	.size	.L.str.45, 22

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"TimeDateStamp: %s"
	.size	.L.str.46, 18

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"SizeOfOptionalHeader: %x\n"
	.size	.L.str.47, 26

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"SizeOfOptionalHeader too small\n"
	.size	.L.str.48, 32

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"Can't read optional file header\n"
	.size	.L.str.49, 33

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"Incorrect SizeOfOptionalHeader for PE32+\n"
	.size	.L.str.50, 42

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"Incorrect magic number in optional header\n"
	.size	.L.str.51, 43

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"9x compatibility mode\n"
	.size	.L.str.52, 23

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"File format: PE\n"
	.size	.L.str.53, 17

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"MajorLinkerVersion: %d\n"
	.size	.L.str.54, 24

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"MinorLinkerVersion: %d\n"
	.size	.L.str.55, 24

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"SizeOfCode: 0x%x\n"
	.size	.L.str.56, 18

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"SizeOfInitializedData: 0x%x\n"
	.size	.L.str.57, 29

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"SizeOfUninitializedData: 0x%x\n"
	.size	.L.str.58, 31

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"AddressOfEntryPoint: 0x%x\n"
	.size	.L.str.59, 27

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"BaseOfCode: 0x%x\n"
	.size	.L.str.60, 18

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"SectionAlignment: 0x%x\n"
	.size	.L.str.61, 24

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"FileAlignment: 0x%x\n"
	.size	.L.str.62, 21

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"MajorSubsystemVersion: %d\n"
	.size	.L.str.63, 27

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"MinorSubsystemVersion: %d\n"
	.size	.L.str.64, 27

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"SizeOfImage: 0x%x\n"
	.size	.L.str.65, 19

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"SizeOfHeaders: 0x%x\n"
	.size	.L.str.66, 21

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"NumberOfRvaAndSizes: %d\n"
	.size	.L.str.67, 25

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"File format: PE32+\n"
	.size	.L.str.68, 20

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"Subsystem: Unknown\n"
	.size	.L.str.69, 20

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"Subsystem: Native (svc)\n"
	.size	.L.str.70, 25

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"Subsystem: Win32 GUI\n"
	.size	.L.str.71, 22

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"Subsystem: Win32 console\n"
	.size	.L.str.72, 26

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"Subsystem: OS/2 console\n"
	.size	.L.str.73, 25

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"Subsystem: POSIX console\n"
	.size	.L.str.74, 26

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"Subsystem: Native Win9x driver\n"
	.size	.L.str.75, 32

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"Subsystem: WinCE GUI\n"
	.size	.L.str.76, 22

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"Subsystem: EFI application\n"
	.size	.L.str.77, 28

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"Subsystem: EFI driver\n"
	.size	.L.str.78, 23

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"Subsystem: EFI runtime driver\n"
	.size	.L.str.79, 31

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"Unknown subsystem in PE header (0x%x)\n"
	.size	.L.str.80, 39

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"------------------------------------\n"
	.size	.L.str.81, 38

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"Bad virtual alignemnt\n"
	.size	.L.str.82, 23

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"Bad file alignemnt\n"
	.size	.L.str.83, 20

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"fstat failed\n"
	.size	.L.str.84, 14

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"Can't allocate memory for section headers\n"
	.size	.L.str.85, 43

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"Can't read section header\n"
	.size	.L.str.86, 27

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"Possibly broken PE file\n"
	.size	.L.str.87, 25

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"Found misaligned section, using 0x200\n"
	.size	.L.str.88, 39

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"Section %d\n"
	.size	.L.str.89, 12

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"Section name: %s\n"
	.size	.L.str.90, 18

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"Section data (from headers - in memory)\n"
	.size	.L.str.91, 41

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"VirtualSize: 0x%x 0x%x\n"
	.size	.L.str.92, 24

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"VirtualAddress: 0x%x 0x%x\n"
	.size	.L.str.93, 27

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"SizeOfRawData: 0x%x 0x%x\n"
	.size	.L.str.94, 26

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"PointerToRawData: 0x%x 0x%x\n"
	.size	.L.str.95, 29

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"Section contains executable code\n"
	.size	.L.str.96, 34

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"Section contains free space\n"
	.size	.L.str.97, 29

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"Section's memory is executable\n"
	.size	.L.str.98, 32

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"Section's memory is writeable\n"
	.size	.L.str.99, 31

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"VirtualAddress is misaligned\n"
	.size	.L.str.100, 30

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"Broken PE file - Section %d starts beyond the end of file (Offset@ %d, Total filesize %d)\n"
	.size	.L.str.101, 91

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"First section is in the wrong place\n"
	.size	.L.str.102, 37

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"Virtually misplaced section (wrong order, overlapping, non contiguous)\n"
	.size	.L.str.103, 72

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"EntryPoint out of file\n"
	.size	.L.str.104, 24

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"EntryPoint offset: 0x%x (%d)\n"
	.size	.L.str.105, 30

	.type	.L.str.106,@object      # @.str.106
	.section	.rodata.cst16,"aM",@progbits,16
.L.str.106:
	.asciz	"GetProcAddress\000"
	.size	.L.str.106, 16

	.type	.L.str.107,@object      # @.str.107
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.107:
	.asciz	"W32.Parite.B"
	.size	.L.str.107, 13

	.type	.Lcli_scanpe.kzs,@object # @cli_scanpe.kzs
	.section	.rodata,"a",@progbits
.Lcli_scanpe.kzs:
	.ascii	"\000\001\002\003\000\004\005\000\006\000\007\b"
	.size	.Lcli_scanpe.kzs, 12

	.type	.L.str.108,@object      # @.str.108
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.108:
	.asciz	"in kriz\n"
	.size	.L.str.108, 9

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"kriz: using #%d as size counter\n"
	.size	.L.str.109, 33

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"kriz: using #%d as pointer\n"
	.size	.L.str.110, 28

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"W32.Kriz"
	.size	.L.str.111, 9

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"kriz: loop out of bounds, corrupted sample?\n"
	.size	.L.str.112, 45

	.type	.L.str.113,@object      # @.str.113
	.section	.rodata,"a",@progbits
.L.str.113:
	.asciz	"\350,a\000\000"
	.size	.L.str.113, 6

	.type	.L.str.114,@object      # @.str.114
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.114:
	.asciz	"W32.Magistr.A.dam"
	.size	.L.str.114, 18

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"W32.Magistr.A"
	.size	.L.str.115, 14

	.type	.L.str.116,@object      # @.str.116
	.section	.rodata,"a",@progbits
.L.str.116:
	.asciz	"\350\004r\000\000"
	.size	.L.str.116, 6

	.type	.L.str.117,@object      # @.str.117
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.117:
	.asciz	"W32.Magistr.B.dam"
	.size	.L.str.117, 18

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"W32.Magistr.B"
	.size	.L.str.118, 14

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"Polipos: Checking %d xsect jump(s)\n"
	.size	.L.str.119, 36

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"W32.Polipos.A"
	.size	.L.str.120, 14

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"UPX/FSG/MEW: empty section found - assuming compression\n"
	.size	.L.str.121, 57

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"MEW: found MEW characteristics %08X + %08X + 5 = %08X\n"
	.size	.L.str.122, 55

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"MEW: lseek() failed\n"
	.size	.L.str.123, 21

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"MEW: Can't read 0xb0 bytes at 0x%x (%d) %d\n"
	.size	.L.str.124, 44

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"MEW: Win9x compatibility was set!\n"
	.size	.L.str.125, 35

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"MEW: Win9x compatibility was NOT set!\n"
	.size	.L.str.126, 39

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"MEW: ESI is not in proper section\n"
	.size	.L.str.127, 35

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"MEW: ssize %08x dsize %08x offdiff: %08x\n"
	.size	.L.str.128, 42

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"MEW: Sizes exceeded (%lu > %lu)\n"
	.size	.L.str.129, 33

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"PE.MEW.ExceededFileSize"
	.size	.L.str.130, 24

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"MEW: Size mismatch: %08x\n"
	.size	.L.str.131, 26

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"MEW: Can't read %d bytes [read: %d]\n"
	.size	.L.str.132, 37

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"MEW: %d (%08x) bytes read\n"
	.size	.L.str.133, 27

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"MEW: lzma proc out of bounds!\n"
	.size	.L.str.134, 31

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"MEW: Can't create file %s\n"
	.size	.L.str.135, 27

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"MEW: Unpacked and rebuilt executable saved in %s\n"
	.size	.L.str.136, 50

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"MEW: Unpacked and rebuilt executable\n"
	.size	.L.str.137, 38

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"***** Scanning rebuilt PE file *****\n"
	.size	.L.str.138, 38

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"MEW: Unpacking failed\n"
	.size	.L.str.139, 23

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"Upack characteristics found.\n"
	.size	.L.str.140, 30

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"Upack: var set\n"
	.size	.L.str.141, 16

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"Upack: var NOT set\n"
	.size	.L.str.142, 20

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"Upack: Sizes exceeded (%lu > %lu)\n"
	.size	.L.str.143, 35

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"PE.Upack.ExceededFileSize"
	.size	.L.str.144, 26

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"Upack: probably malformed pe-header, skipping to next unpacker\n"
	.size	.L.str.145, 64

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"Upack: Can't read raw data of section 0\n"
	.size	.L.str.146, 41

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"Upack: Can't read raw data of section 1\n"
	.size	.L.str.147, 41

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	"Upack: Can't create file %s\n"
	.size	.L.str.148, 29

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"Upack: Unpacked and rebuilt executable saved in %s\n"
	.size	.L.str.149, 52

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"Upack: Unpacked and rebuilt executable\n"
	.size	.L.str.150, 40

	.type	.L.str.151,@object      # @.str.151
.L.str.151:
	.asciz	"Upack: Unpacking failed\n"
	.size	.L.str.151, 25

	.type	.L.str.152,@object      # @.str.152
.L.str.152:
	.asciz	"FSG: Sizes exceeded (%lu > %lu)\n"
	.size	.L.str.152, 33

	.type	.L.str.153,@object      # @.str.153
.L.str.153:
	.asciz	"PE.FSG.ExceededFileSize"
	.size	.L.str.153, 24

	.type	.L.str.154,@object      # @.str.154
.L.str.154:
	.asciz	"FSG: Size mismatch (ssize: %d, dsize: %d)\n"
	.size	.L.str.154, 43

	.type	.L.str.155,@object      # @.str.155
.L.str.155:
	.asciz	"FSG: xchg out of bounds (%x), giving up\n"
	.size	.L.str.155, 41

	.type	.L.str.156,@object      # @.str.156
.L.str.156:
	.asciz	"Can't read raw data of section %d\n"
	.size	.L.str.156, 35

	.type	.L.str.157,@object      # @.str.157
.L.str.157:
	.asciz	"FSG: New ESP out of bounds\n"
	.size	.L.str.157, 28

	.type	.L.str.158,@object      # @.str.158
.L.str.158:
	.asciz	"FSG: New ESP (%x) is wrong\n"
	.size	.L.str.158, 28

	.type	.L.str.159,@object      # @.str.159
.L.str.159:
	.asciz	"FSG: New stack out of bounds\n"
	.size	.L.str.159, 30

	.type	.L.str.160,@object      # @.str.160
.L.str.160:
	.asciz	"FSG: Bad destination buffer (edi is %x should be %x)\n"
	.size	.L.str.160, 54

	.type	.L.str.161,@object      # @.str.161
.L.str.161:
	.asciz	"FSG: Source buffer out of section bounds\n"
	.size	.L.str.161, 42

	.type	.L.str.162,@object      # @.str.162
.L.str.162:
	.asciz	"FSG: Array of functions out of bounds\n"
	.size	.L.str.162, 39

	.type	.L.str.163,@object      # @.str.163
.L.str.163:
	.asciz	"FSG: found old EP @%x\n"
	.size	.L.str.163, 23

	.type	.L.str.164,@object      # @.str.164
.L.str.164:
	.asciz	"FSG: Can't create file %s\n"
	.size	.L.str.164, 27

	.type	.L.str.165,@object      # @.str.165
.L.str.165:
	.asciz	"FSG: Unpacked and rebuilt executable saved in %s\n"
	.size	.L.str.165, 50

	.type	.L.str.166,@object      # @.str.166
.L.str.166:
	.asciz	"FSG: Unpacked and rebuilt executable\n"
	.size	.L.str.166, 38

	.type	.L.str.167,@object      # @.str.167
.L.str.167:
	.asciz	"FSG: Successfully decompressed\n"
	.size	.L.str.167, 32

	.type	.L.str.168,@object      # @.str.168
.L.str.168:
	.asciz	"FSG: Unpacking failed\n"
	.size	.L.str.168, 23

	.type	.L.str.169,@object      # @.str.169
.L.str.169:
	.asciz	"FSG: Support data out of padding area\n"
	.size	.L.str.169, 39

	.type	.L.str.170,@object      # @.str.170
.L.str.170:
	.asciz	"Can't read %d bytes from padding area\n"
	.size	.L.str.170, 39

	.type	.L.str.171,@object      # @.str.171
.L.str.171:
	.asciz	"FSG: Bad destination (is %x should be %x)\n"
	.size	.L.str.171, 43

	.type	.L.str.172,@object      # @.str.172
.L.str.172:
	.asciz	"FSG: Original section %d is misaligned\n"
	.size	.L.str.172, 40

	.type	.L.str.173,@object      # @.str.173
.L.str.173:
	.asciz	"FSG: Original section %d is out of bounds\n"
	.size	.L.str.173, 43

	.type	.L.str.174,@object      # @.str.174
.L.str.174:
	.asciz	"FSG: Can't read raw data of section %d\n"
	.size	.L.str.174, 40

	.type	.L.str.175,@object      # @.str.175
.L.str.175:
	.asciz	"UPX: Sizes exceeded (%lu > %lu)\n"
	.size	.L.str.175, 33

	.type	.L.str.176,@object      # @.str.176
.L.str.176:
	.asciz	"PE.UPX.ExceededFileSize"
	.size	.L.str.176, 24

	.type	.L.str.177,@object      # @.str.177
.L.str.177:
	.asciz	"UPX: Size mismatch or dsize too big (ssize: %d, dsize: %d)\n"
	.size	.L.str.177, 60

	.type	.L.str.178,@object      # @.str.178
.L.str.178:
	.asciz	"UPX: Can't read raw data of section %d\n"
	.size	.L.str.178, 40

	.type	.L.str.179,@object      # @.str.179
.L.str.179:
	.asciz	"\021\333\021\311\001\333u\007\213\036\203\356\374\021\333\021\311\021\311u A\001\333"
	.size	.L.str.179, 25

	.type	.L.str.180,@object      # @.str.180
.L.str.180:
	.asciz	"UPX: Looks like a NRV2B decompression routine\n"
	.size	.L.str.180, 47

	.type	.L.str.181,@object      # @.str.181
.L.str.181:
	.asciz	"\203\360\377tx\321\370\211\305\353\013\001\333u\007\213\036\203\356\374\021\333\021\311"
	.size	.L.str.181, 25

	.type	.L.str.182,@object      # @.str.182
.L.str.182:
	.asciz	"UPX: Looks like a NRV2D decompression routine\n"
	.size	.L.str.182, 47

	.type	.L.str.183,@object      # @.str.183
.L.str.183:
	.asciz	"\353R1\311\203\350\003r\021\301\340\b\212\006F\203\360\377tu\321\370\211\305"
	.size	.L.str.183, 25

	.type	.L.str.184,@object      # @.str.184
.L.str.184:
	.asciz	"UPX: Looks like a NRV2E decompression routine\n"
	.size	.L.str.184, 47

	.type	.L.str.185,@object      # @.str.185
.L.str.185:
	.asciz	"UPX: UPX1 seems skewed by %d bytes\n"
	.size	.L.str.185, 36

	.type	.L.str.186,@object      # @.str.186
.L.str.186:
	.asciz	"UPX: Successfully decompressed\n"
	.size	.L.str.186, 32

	.type	.L.str.187,@object      # @.str.187
.L.str.187:
	.asciz	"UPX: Preferred decompressor failed\n"
	.size	.L.str.187, 36

	.type	.L.str.188,@object      # @.str.188
.L.str.188:
	.asciz	"UPX: NRV2B decompressor failed\n"
	.size	.L.str.188, 32

	.type	.L.str.189,@object      # @.str.189
.L.str.189:
	.asciz	"UPX: Successfully decompressed with NRV2B\n"
	.size	.L.str.189, 43

	.type	.L.str.190,@object      # @.str.190
.L.str.190:
	.asciz	"UPX: NRV2D decompressor failed\n"
	.size	.L.str.190, 32

	.type	.L.str.191,@object      # @.str.191
.L.str.191:
	.asciz	"UPX: Successfully decompressed with NRV2D\n"
	.size	.L.str.191, 43

	.type	.L.str.192,@object      # @.str.192
.L.str.192:
	.asciz	"UPX: NRV2E decompressor failed\n"
	.size	.L.str.192, 32

	.type	.L.str.193,@object      # @.str.193
.L.str.193:
	.asciz	"UPX: Successfully decompressed with NRV2E\n"
	.size	.L.str.193, 43

	.type	.L.str.194,@object      # @.str.194
.L.str.194:
	.asciz	"UPX: All decompressors failed\n"
	.size	.L.str.194, 31

	.type	.L.str.195,@object      # @.str.195
.L.str.195:
	.asciz	"UPX/FSG: Can't create file %s\n"
	.size	.L.str.195, 31

	.type	.L.str.196,@object      # @.str.196
.L.str.196:
	.asciz	"UPX/FSG: Can't write %d bytes\n"
	.size	.L.str.196, 31

	.type	.L.str.197,@object      # @.str.197
.L.str.197:
	.asciz	"UPX/FSG: Decompressed data saved in %s\n"
	.size	.L.str.197, 40

	.type	.L.str.198,@object      # @.str.198
.L.str.198:
	.asciz	"***** Scanning decompressed file *****\n"
	.size	.L.str.198, 40

	.type	.L.str.199,@object      # @.str.199
.L.str.199:
	.asciz	"Petite: v2.%d compression detected\n"
	.size	.L.str.199, 36

	.type	.L.str.200,@object      # @.str.200
.L.str.200:
	.asciz	"Petite: level zero compression is not supported yet\n"
	.size	.L.str.200, 53

	.type	.L.str.201,@object      # @.str.201
.L.str.201:
	.asciz	"Petite: Sizes exceeded (%lu > %lu)\n"
	.size	.L.str.201, 36

	.type	.L.str.202,@object      # @.str.202
.L.str.202:
	.asciz	"PE.Petite.ExceededFileSize"
	.size	.L.str.202, 27

	.type	.L.str.203,@object      # @.str.203
.L.str.203:
	.asciz	"Petite: Can't allocate %d bytes\n"
	.size	.L.str.203, 33

	.type	.L.str.204,@object      # @.str.204
.L.str.204:
	.asciz	"Petite: Can't create file %s\n"
	.size	.L.str.204, 30

	.type	.L.str.205,@object      # @.str.205
.L.str.205:
	.asciz	"Petite: Unpacked and rebuilt executable saved in %s\n"
	.size	.L.str.205, 53

	.type	.L.str.206,@object      # @.str.206
.L.str.206:
	.asciz	"Petite: Unpacked and rebuilt executable\n"
	.size	.L.str.206, 41

	.type	.L.str.207,@object      # @.str.207
.L.str.207:
	.asciz	"Petite: Unpacking failed\n"
	.size	.L.str.207, 26

	.type	.L.str.208,@object      # @.str.208
	.section	.rodata,"a",@progbits
.L.str.208:
	.asciz	"\350\000\000\000\000\213\034$\203\303"
	.size	.L.str.208, 11

	.type	.L.str.209,@object      # @.str.209
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.209:
	.asciz	"PEspin: Sizes exceeded (%lu > %lu)\n"
	.size	.L.str.209, 36

	.type	.L.str.210,@object      # @.str.210
.L.str.210:
	.asciz	"PE.PEspin.ExceededFileSize"
	.size	.L.str.210, 27

	.type	.L.str.211,@object      # @.str.211
.L.str.211:
	.asciz	"PESpin: Can't read %d bytes\n"
	.size	.L.str.211, 29

	.type	.L.str.212,@object      # @.str.212
.L.str.212:
	.asciz	"PESpin: Can't create file %s\n"
	.size	.L.str.212, 30

	.type	.L.str.213,@object      # @.str.213
.L.str.213:
	.asciz	"PEspin: Unpacked and rebuilt executable saved in %s\n"
	.size	.L.str.213, 53

	.type	.L.str.214,@object      # @.str.214
.L.str.214:
	.asciz	"PEspin: Unpacked and rebuilt executable\n"
	.size	.L.str.214, 41

	.type	.L.str.215,@object      # @.str.215
.L.str.215:
	.asciz	"PESpin: Size exceeded\n"
	.size	.L.str.215, 23

	.type	.L.str.216,@object      # @.str.216
.L.str.216:
	.asciz	"PE.Pespin.ExceededFileSize"
	.size	.L.str.216, 27

	.type	.L.str.217,@object      # @.str.217
.L.str.217:
	.asciz	"PEspin: Unpacking failed\n"
	.size	.L.str.217, 26

	.type	.L.str.218,@object      # @.str.218
	.section	.rodata,"a",@progbits
.L.str.218:
	.asciz	"U\213\354SVW`\350\000\000\000\000]\201\355l(@\000\271]4@\000\201\351\306(@\000\213\325\201\302\306(@\000\215:\213\3673\300\353\004\220\353\001\302\254"
	.size	.L.str.218, 52

	.type	.L.str.219,@object      # @.str.219
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.219:
	.asciz	"yC: Can't read %d bytes\n"
	.size	.L.str.219, 25

	.type	.L.str.220,@object      # @.str.220
.L.str.220:
	.asciz	"yC: Can't create file %s\n"
	.size	.L.str.220, 26

	.type	.L.str.221,@object      # @.str.221
.L.str.221:
	.asciz	"yC: Unpacked and rebuilt executable saved in %s\n"
	.size	.L.str.221, 49

	.type	.L.str.222,@object      # @.str.222
.L.str.222:
	.asciz	"yC: Unpacked and rebuilt executable\n"
	.size	.L.str.222, 37

	.type	.L.str.223,@object      # @.str.223
.L.str.223:
	.asciz	"yC: Unpacking failed\n"
	.size	.L.str.223, 22

	.type	.L.str.224,@object      # @.str.224
.L.str.224:
	.asciz	"SU\213\3503\333\353"
	.size	.L.str.224, 8

	.type	.L.str.225,@object      # @.str.225
	.section	.rodata,"a",@progbits
.L.str.225:
	.asciz	"\350\000\000\000\000X-m\000\000\000P`3\311PXPP"
	.size	.L.str.225, 20

	.type	.L.str.226,@object      # @.str.226
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.226:
	.asciz	"WWPack: Sizes exceeded (%lu > %lu)\n"
	.size	.L.str.226, 36

	.type	.L.str.227,@object      # @.str.227
.L.str.227:
	.asciz	"PE.WWPack.ExceededFileSize"
	.size	.L.str.227, 27

	.type	.L.str.228,@object      # @.str.228
.L.str.228:
	.asciz	"WWPack: Can't allocate %d bytes\n"
	.size	.L.str.228, 33

	.type	.L.str.229,@object      # @.str.229
.L.str.229:
	.asciz	"WWPack: Can't read %d bytes from headers\n"
	.size	.L.str.229, 42

	.type	.L.str.230,@object      # @.str.230
.L.str.230:
	.asciz	"WWPack: Can't read %d bytes from wwpack sect\n"
	.size	.L.str.230, 46

	.type	.L.str.231,@object      # @.str.231
.L.str.231:
	.asciz	"WWPack: Can't create file %s\n"
	.size	.L.str.231, 30

	.type	.L.str.232,@object      # @.str.232
.L.str.232:
	.asciz	"WWPack: Can't write %d bytes\n"
	.size	.L.str.232, 30

	.type	.L.str.233,@object      # @.str.233
.L.str.233:
	.asciz	"WWPack: Unpacked and rebuilt executable saved in %s\n"
	.size	.L.str.233, 53

	.type	.L.str.234,@object      # @.str.234
.L.str.234:
	.asciz	"WWPack: Unpacked and rebuilt executable\n"
	.size	.L.str.234, 41

	.type	.L.str.235,@object      # @.str.235
.L.str.235:
	.asciz	"WWPpack: Decompression failed\n"
	.size	.L.str.235, 31

	.type	.L.str.236,@object      # @.str.236
	.section	.rodata,"a",@progbits
.L.str.236:
	.asciz	"`\350\003\000\000\000\351\353"
	.size	.L.str.236, 9

	.type	.L.str.237,@object      # @.str.237
.L.str.237:
	.asciz	"h\000\000\000\000\303"
	.size	.L.str.237, 7

	.type	.L.str.238,@object      # @.str.238
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.238:
	.asciz	"Aspack: Sizes exceeded (%lu > %lu)\n"
	.size	.L.str.238, 36

	.type	.L.str.239,@object      # @.str.239
.L.str.239:
	.asciz	"PE.Aspack.ExceededFileSize"
	.size	.L.str.239, 27

	.type	.L.str.240,@object      # @.str.240
.L.str.240:
	.asciz	"Aspack: Probably hacked/damaged Aspack file.\n"
	.size	.L.str.240, 46

	.type	.L.str.241,@object      # @.str.241
.L.str.241:
	.asciz	"Aspack: Can't create file %s\n"
	.size	.L.str.241, 30

	.type	.L.str.242,@object      # @.str.242
.L.str.242:
	.asciz	"Aspack: Unpacked and rebuilt executable saved in %s\n"
	.size	.L.str.242, 53

	.type	.L.str.243,@object      # @.str.243
.L.str.243:
	.asciz	"Aspack: Unpacked and rebuilt executable\n"
	.size	.L.str.243, 41

	.type	.L.str.244,@object      # @.str.244
.L.str.244:
	.asciz	"Aspack: Unpacking failed\n"
	.size	.L.str.244, 26

	.type	.L.str.245,@object      # @.str.245
	.section	.rodata,"a",@progbits
.L.str.245:
	.asciz	"\234`\350\000\000\000\000]\270\007\000\000\000"
	.size	.L.str.245, 14

	.type	.L.str.246,@object      # @.str.246
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.246:
	.asciz	"NsPack: Found *start_of_stuff @delta-%x\n"
	.size	.L.str.246, 41

	.type	.L.str.247,@object      # @.str.247
.L.str.247:
	.asciz	"NsPack: Sizes exceeded (%lu > %lu)\n"
	.size	.L.str.247, 36

	.type	.L.str.248,@object      # @.str.248
.L.str.248:
	.asciz	"PE.NsPack.ExceededFileSize"
	.size	.L.str.248, 27

	.type	.L.str.249,@object      # @.str.249
.L.str.249:
	.asciz	"NsPack: OEP = %08x\n"
	.size	.L.str.249, 20

	.type	.L.str.250,@object      # @.str.250
.L.str.250:
	.asciz	"NsPack: Can't create file %s\n"
	.size	.L.str.250, 30

	.type	.L.str.251,@object      # @.str.251
.L.str.251:
	.asciz	"NsPack: Unpacked and rebuilt executable saved in %s\n"
	.size	.L.str.251, 53

	.type	.L.str.252,@object      # @.str.252
.L.str.252:
	.asciz	"NsPack: Unpacked and rebuilt executable\n"
	.size	.L.str.252, 41

	.type	.L.str.253,@object      # @.str.253
.L.str.253:
	.asciz	"NsPack: Unpacking failed\n"
	.size	.L.str.253, 26

	.type	.L.str.254,@object      # @.str.254
.L.str.254:
	.asciz	"in cli_peheader\n"
	.size	.L.str.254, 17

	.type	.L.str.255,@object      # @.str.255
.L.str.255:
	.asciz	"Broken PE file\n"
	.size	.L.str.255, 16

	.type	.L.str.256,@object      # @.str.256
.L.str.256:
	.asciz	"cli_md5sect: skipping md5 calculation for too big section\n"
	.size	.L.str.256, 59

	.type	.L.str.257,@object      # @.str.257
.L.str.257:
	.asciz	"cli_md5sect: out of memory\n"
	.size	.L.str.257, 28

	.type	.L.str.258,@object      # @.str.258
.L.str.258:
	.asciz	"cli_md5sect: unable to read section data\n"
	.size	.L.str.258, 42

	.type	.L.str.259,@object      # @.str.259
.L.str.259:
	.asciz	"cli_seeksect: lseek() failed\n"
	.size	.L.str.259, 30


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
