	.text
	.file	"heapsort.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4684049276697837568     # double 139968
	.text
	.globl	_Z10gen_randomd
	.p2align	4, 0x90
	.type	_Z10gen_randomd,@function
_Z10gen_randomd:                        # @_Z10gen_randomd
	.cfi_startproc
# BB#0:
	imulq	$3877, _ZZ10gen_randomdE4last(%rip), %rcx # imm = 0xF25
	addq	$29573, %rcx            # imm = 0x7385
	movabsq	$4318579316753219217, %rdx # imm = 0x3BEEAD01FD6CBE91
	movq	%rcx, %rax
	imulq	%rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	imulq	$139968, %rdx, %rax     # imm = 0x222C0
	subq	%rax, %rcx
	movq	%rcx, _ZZ10gen_randomdE4last(%rip)
	cvtsi2sdq	%rcx, %xmm1
	mulsd	%xmm1, %xmm0
	divsd	.LCPI0_0(%rip), %xmm0
	retq
.Lfunc_end0:
	.size	_Z10gen_randomd, .Lfunc_end0-_Z10gen_randomd
	.cfi_endproc

	.globl	_Z8heapsortiPd
	.p2align	4, 0x90
	.type	_Z8heapsortiPd,@function
_Z8heapsortiPd:                         # @_Z8heapsortiPd
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movl	%edi, %r8d
	sarl	%r8d
	incl	%r8d
	cmpl	$2, %r8d
	jl	.LBB1_3
	jmp	.LBB1_2
	.p2align	4, 0x90
.LBB1_11:
	movl	%ecx, %edx
.LBB1_13:                               # %.outer._crit_edge
	movslq	%edx, %rax
	movsd	%xmm0, (%rsi,%rax,8)
	cmpl	$2, %r8d
	jl	.LBB1_3
.LBB1_2:
	movslq	%r8d, %rax
	decl	%r8d
	movsd	-8(%rsi,%rax,8), %xmm0  # xmm0 = mem[0],zero
	jmp	.LBB1_4
	.p2align	4, 0x90
.LBB1_3:
	movslq	%edi, %rdi
	movsd	(%rsi,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	movq	8(%rsi), %rax
	movq	%rax, (%rsi,%rdi,8)
	decl	%edi
	cmpl	$1, %edi
	je	.LBB1_14
.LBB1_4:
	leal	(%r8,%r8), %eax
	cmpl	%edi, %eax
	movl	%r8d, %edx
	jg	.LBB1_13
# BB#5:                                 # %.lr.ph.us.preheader
	movl	%r8d, %ecx
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph.us
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%edi, %eax
	jge	.LBB1_7
# BB#8:                                 #   in Loop: Header=BB1_6 Depth=1
	movslq	%eax, %r10
	movl	%eax, %r9d
	orl	$1, %r9d
	movslq	%r9d, %rdx
	movsd	(%rsi,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	(%rsi,%r10,8), %xmm1
	movl	%eax, %edx
	jbe	.LBB1_10
# BB#9:                                 #   in Loop: Header=BB1_6 Depth=1
	movl	%r9d, %edx
	jmp	.LBB1_10
	.p2align	4, 0x90
.LBB1_7:                                #   in Loop: Header=BB1_6 Depth=1
	movl	%eax, %edx
.LBB1_10:                               #   in Loop: Header=BB1_6 Depth=1
	movslq	%edx, %rax
	movsd	(%rsi,%rax,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB1_11
# BB#12:                                # %.us-lcssa.us.us
                                        #   in Loop: Header=BB1_6 Depth=1
	movslq	%ecx, %rax
	movsd	%xmm1, (%rsi,%rax,8)
	leal	(%rdx,%rdx), %eax
	cmpl	%edi, %eax
	movl	%edx, %ecx
	jle	.LBB1_6
	jmp	.LBB1_13
.LBB1_14:
	movsd	%xmm0, 8(%rsi)
	retq
.Lfunc_end1:
	.size	_Z8heapsortiPd, .Lfunc_end1-_Z8heapsortiPd
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4684049276697837568     # double 139968
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	$8000000, %r14d         # imm = 0x7A1200
	cmpl	$2, %edi
	jne	.LBB2_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r14
.LBB2_2:
	leal	1(%r14), %ebp
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, %rbx
	testl	%r14d, %r14d
	jle	.LBB2_10
# BB#3:                                 # %.lr.ph
	movq	_ZZ10gen_randomdE4last(%rip), %rcx
	testb	$1, %bpl
	jne	.LBB2_4
# BB#5:
	imulq	$3877, %rcx, %rcx       # imm = 0xF25
	addq	$29573, %rcx            # imm = 0x7385
	movabsq	$4318579316753219217, %rdx # imm = 0x3BEEAD01FD6CBE91
	movq	%rcx, %rax
	imulq	%rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	imulq	$139968, %rdx, %rax     # imm = 0x222C0
	subq	%rax, %rcx
	cvtsi2sdq	%rcx, %xmm0
	divsd	.LCPI2_0(%rip), %xmm0
	movsd	%xmm0, 8(%rbx)
	movl	$2, %eax
	cmpl	$2, %ebp
	jne	.LBB2_7
	jmp	.LBB2_9
.LBB2_4:
	movl	$1, %eax
	cmpl	$2, %ebp
	je	.LBB2_9
.LBB2_7:                                # %.lr.ph.new
	movl	%ebp, %esi
	subq	%rax, %rsi
	leaq	8(%rbx,%rax,8), %rdi
	movabsq	$4318579316753219217, %rbp # imm = 0x3BEEAD01FD6CBE91
	movsd	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB2_8:                                # =>This Inner Loop Header: Depth=1
	imulq	$3877, %rcx, %rcx       # imm = 0xF25
	addq	$29573, %rcx            # imm = 0x7385
	movq	%rcx, %rax
	imulq	%rbp
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	imulq	$139968, %rdx, %rax     # imm = 0x222C0
	subq	%rax, %rcx
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rcx, %xmm1
	divsd	%xmm0, %xmm1
	movsd	%xmm1, -8(%rdi)
	imulq	$3877, %rcx, %rcx       # imm = 0xF25
	addq	$29573, %rcx            # imm = 0x7385
	movq	%rcx, %rax
	imulq	%rbp
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	imulq	$139968, %rdx, %rax     # imm = 0x222C0
	subq	%rax, %rcx
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rcx, %xmm1
	divsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdi)
	addq	$16, %rdi
	addq	$-2, %rsi
	jne	.LBB2_8
.LBB2_9:                                # %._crit_edge
	movq	%rcx, _ZZ10gen_randomdE4last(%rip)
.LBB2_10:
	movl	%r14d, %r8d
	sarl	%r8d
	incl	%r8d
	movl	%r14d, %ecx
	cmpl	$2, %r8d
	jl	.LBB2_13
	jmp	.LBB2_12
	.p2align	4, 0x90
.LBB2_21:
	movl	%edx, %esi
.LBB2_23:                               # %.outer._crit_edge.i
	movslq	%esi, %rax
	movsd	%xmm0, (%rbx,%rax,8)
	cmpl	$2, %r8d
	jl	.LBB2_13
.LBB2_12:
	movslq	%r8d, %rdx
	decl	%r8d
	movsd	-8(%rbx,%rdx,8), %xmm0  # xmm0 = mem[0],zero
	jmp	.LBB2_14
	.p2align	4, 0x90
.LBB2_13:
	movslq	%ecx, %rcx
	movsd	(%rbx,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movq	8(%rbx), %rdx
	movq	%rdx, (%rbx,%rcx,8)
	decl	%ecx
	cmpl	$1, %ecx
	je	.LBB2_24
.LBB2_14:
	leal	(%r8,%r8), %edi
	cmpl	%ecx, %edi
	movl	%r8d, %esi
	jg	.LBB2_23
# BB#15:                                # %.lr.ph.us.i.preheader
	movl	%r8d, %edx
	.p2align	4, 0x90
.LBB2_16:                               # %.lr.ph.us.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%ecx, %edi
	jge	.LBB2_17
# BB#18:                                #   in Loop: Header=BB2_16 Depth=1
	movslq	%edi, %rsi
	movl	%edi, %ebp
	orl	$1, %ebp
	movslq	%ebp, %rax
	movsd	(%rbx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	(%rbx,%rsi,8), %xmm1
	movl	%edi, %esi
	jbe	.LBB2_20
# BB#19:                                #   in Loop: Header=BB2_16 Depth=1
	movl	%ebp, %esi
	jmp	.LBB2_20
	.p2align	4, 0x90
.LBB2_17:                               #   in Loop: Header=BB2_16 Depth=1
	movl	%edi, %esi
.LBB2_20:                               #   in Loop: Header=BB2_16 Depth=1
	movslq	%esi, %rax
	movsd	(%rbx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB2_21
# BB#22:                                # %.us-lcssa.us.us.i
                                        #   in Loop: Header=BB2_16 Depth=1
	movslq	%edx, %rax
	movsd	%xmm1, (%rbx,%rax,8)
	leal	(%rsi,%rsi), %edi
	cmpl	%ecx, %edi
	movl	%esi, %edx
	jle	.LBB2_16
	jmp	.LBB2_23
.LBB2_24:                               # %_Z8heapsortiPd.exit
	movsd	%xmm0, 8(%rbx)
	movslq	%r14d, %rax
	movsd	(%rbx,%rax,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movq	%rbx, %rdi
	callq	free
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_heapsort.ii,@function
_GLOBAL__sub_I_heapsort.ii:             # @_GLOBAL__sub_I_heapsort.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end3:
	.size	_GLOBAL__sub_I_heapsort.ii, .Lfunc_end3-_GLOBAL__sub_I_heapsort.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.type	_ZZ10gen_randomdE4last,@object # @_ZZ10gen_randomdE4last
	.data
	.p2align	3
_ZZ10gen_randomdE4last:
	.quad	42                      # 0x2a
	.size	_ZZ10gen_randomdE4last, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%.10f\n"
	.size	.L.str, 7

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_heapsort.ii

	.ident	"clang version 5.0.0 (https://github.com/aqjune/clang-intptr.git 2b963f0b79b3094bfc850c67ca5db086f5651807) (https://github.com/aqjune/llvm-intptr.git 90a84f9e4908c61106c413cb18b15a9bb8978e0d)"
	.section	".note.GNU-stack","",@progbits
