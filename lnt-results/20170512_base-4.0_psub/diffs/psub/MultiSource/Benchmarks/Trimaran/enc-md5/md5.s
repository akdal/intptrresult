	.text
	.file	"md5.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	1732584193              # 0x67452301
	.quad	4023233417              # 0xefcdab89
.LCPI0_1:
	.quad	2562383102              # 0x98badcfe
	.quad	271733878               # 0x10325476
	.text
	.globl	md5_starts
	.p2align	4, 0x90
	.type	md5_starts,@function
md5_starts:                             # @md5_starts
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [1732584193,4023233417]
	movups	%xmm0, 16(%rdi)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [2562383102,271733878]
	movups	%xmm0, 32(%rdi)
	retq
.Lfunc_end0:
	.size	md5_starts, .Lfunc_end0-md5_starts
	.cfi_endproc

	.globl	md5_process
	.p2align	4, 0x90
	.type	md5_process,@function
md5_process:                            # @md5_process
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$16, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 72
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movzbl	(%rsi), %eax
	movzbl	1(%rsi), %ecx
	shlq	$8, %rcx
	orq	%rax, %rcx
	movzbl	2(%rsi), %eax
	shlq	$16, %rax
	orq	%rcx, %rax
	movzbl	3(%rsi), %edx
	shlq	$24, %rdx
	orq	%rax, %rdx
	movq	%rdx, -56(%rsp)         # 8-byte Spill
	movq	16(%rdi), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	24(%rdi), %rax
	movq	%rdi, %rbp
	leaq	(%rdx,%rcx), %rcx
	movzbl	4(%rsi), %edx
	movzbl	5(%rsi), %edi
	shlq	$8, %rdi
	orq	%rdx, %rdi
	movzbl	6(%rsi), %edx
	shlq	$16, %rdx
	orq	%rdi, %rdx
	movzbl	7(%rsi), %edi
	shlq	$24, %rdi
	orq	%rdx, %rdi
	movq	%rdi, %r10
	movq	%r10, -128(%rsp)        # 8-byte Spill
	movq	40(%rbp), %r8
	movq	%rbp, %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	movzbl	8(%rsi), %edi
	movzbl	9(%rsi), %ebp
	shlq	$8, %rbp
	orq	%rdi, %rbp
	movzbl	10(%rsi), %edi
	shlq	$16, %rdi
	orq	%rbp, %rdi
	movzbl	11(%rsi), %ebp
	shlq	$24, %rbp
	orq	%rdi, %rbp
	movq	%rbp, %r9
	movq	%r9, -64(%rsp)          # 8-byte Spill
	movzbl	12(%rsi), %edi
	movzbl	13(%rsi), %ebp
	shlq	$8, %rbp
	orq	%rdi, %rbp
	movzbl	14(%rsi), %edi
	shlq	$16, %rdi
	orq	%rbp, %rdi
	movzbl	15(%rsi), %ebp
	shlq	$24, %rbp
	orq	%rdi, %rbp
	movq	%rbp, %r12
	movq	%r12, -72(%rsp)         # 8-byte Spill
	movzbl	16(%rsi), %edi
	movzbl	17(%rsi), %ebp
	shlq	$8, %rbp
	orq	%rdi, %rbp
	movzbl	18(%rsi), %edi
	shlq	$16, %rdi
	orq	%rbp, %rdi
	movzbl	19(%rsi), %ebp
	shlq	$24, %rbp
	orq	%rdi, %rbp
	movq	%rbp, -120(%rsp)        # 8-byte Spill
	movq	32(%rdx), %rbx
	movq	%r8, %rdx
	xorq	%rbx, %rdx
	andq	%rax, %rdx
	xorq	%r8, %rdx
	addq	%rcx, %rdx
	movl	$2240044497, %r14d      # imm = 0x85845DD1
	leaq	1374045863(%r14,%rdx), %rcx
	movq	%rcx, %rdx
	shlq	$7, %rdx
	shrl	$25, %ecx
	orq	%rdx, %rcx
	movzbl	20(%rsi), %edx
	movzbl	21(%rsi), %ebp
	shlq	$8, %rbp
	orq	%rdx, %rbp
	movzbl	22(%rsi), %edx
	shlq	$16, %rdx
	orq	%rbp, %rdx
	movzbl	23(%rsi), %edi
	shlq	$24, %rdi
	orq	%rdx, %rdi
	movq	%rdi, %r13
	movq	%r13, -104(%rsp)        # 8-byte Spill
	movzbl	25(%rsi), %edx
	shlq	$8, %rdx
	movzbl	24(%rsi), %ebp
	orq	%rbp, %rdx
	movzbl	26(%rsi), %ebp
	shlq	$16, %rbp
	orq	%rdx, %rbp
	movzbl	27(%rsi), %edx
	shlq	$24, %rdx
	orq	%rbp, %rdx
	movq	%rdx, %r15
	movq	%r15, -88(%rsp)         # 8-byte Spill
	leaq	(%r10,%r8), %rdx
	addq	%rax, %rcx
	movq	%rbx, %rdi
	xorq	%rax, %rdi
	andq	%rcx, %rdi
	xorq	%rbx, %rdi
	addq	%rdx, %rdi
	leaq	1665358213(%r14,%rdi), %rdx
	movq	%rdx, %rdi
	shlq	$12, %rdi
	shrl	$20, %edx
	orq	%rdi, %rdx
	addq	%rcx, %rdx
	movq	%rcx, %rdi
	xorq	%rax, %rdi
	andq	%rdx, %rdi
	xorq	%rax, %rdi
	addq	%r9, %rbx
	leaq	606105819(%rdi,%rbx), %rbp
	movq	%rbp, %rdi
	shlq	$17, %rdi
	shrl	$15, %ebp
	orq	%rdi, %rbp
	movzbl	29(%rsi), %edi
	shlq	$8, %rdi
	movzbl	28(%rsi), %ebx
	orq	%rbx, %rdi
	movzbl	30(%rsi), %ebx
	shlq	$16, %rbx
	orq	%rdi, %rbx
	movzbl	31(%rsi), %r11d
	shlq	$24, %r11
	orq	%rbx, %r11
	movq	%r11, -96(%rsp)         # 8-byte Spill
	leaq	(%r12,%rax), %rax
	addq	%rdx, %rbp
	movq	%rdx, %rdi
	xorq	%rcx, %rdi
	andq	%rbp, %rdi
	xorq	%rcx, %rdi
	addq	%rax, %rdi
	leaq	1010397469(%r14,%rdi), %r10
	movq	%r10, %rax
	shlq	$22, %rax
	shrl	$10, %r10d
	orq	%rax, %r10
	movzbl	33(%rsi), %eax
	shlq	$8, %rax
	movzbl	32(%rsi), %edi
	orq	%rdi, %rax
	movzbl	34(%rsi), %edi
	shlq	$16, %rdi
	orq	%rax, %rdi
	movzbl	35(%rsi), %eax
	shlq	$24, %rax
	orq	%rdi, %rax
	movq	%rax, -112(%rsp)        # 8-byte Spill
	movzbl	37(%rsi), %eax
	shlq	$8, %rax
	movzbl	36(%rsi), %edi
	orq	%rdi, %rax
	movzbl	38(%rsi), %edi
	shlq	$16, %rdi
	orq	%rax, %rdi
	movzbl	39(%rsi), %r12d
	shlq	$24, %r12
	orq	%rdi, %r12
	movq	%r12, -32(%rsp)         # 8-byte Spill
	movq	-120(%rsp), %rax        # 8-byte Reload
	leaq	(%rax,%rcx), %rax
	addq	%rbp, %r10
	movq	%rbp, %rcx
	xorq	%rdx, %rcx
	andq	%r10, %rcx
	xorq	%rdx, %rcx
	addq	%rax, %rcx
	leaq	1878503902(%r14,%rcx), %r8
	movq	%r8, %rcx
	shlq	$7, %rcx
	shrl	$25, %r8d
	orq	%rcx, %r8
	addq	%r10, %r8
	movq	%r10, %rcx
	xorq	%rbp, %rcx
	andq	%r8, %rcx
	xorq	%rbp, %rcx
	addq	%r13, %rdx
	leaq	1200080426(%rcx,%rdx), %r9
	movq	%r9, %rcx
	shlq	$12, %rcx
	shrl	$20, %r9d
	orq	%rcx, %r9
	movzbl	41(%rsi), %ecx
	shlq	$8, %rcx
	movzbl	40(%rsi), %edx
	orq	%rdx, %rcx
	movzbl	42(%rsi), %edx
	shlq	$16, %rdx
	orq	%rcx, %rdx
	movzbl	43(%rsi), %eax
	shlq	$24, %rax
	orq	%rdx, %rax
	movq	%rax, %rbx
	leaq	(%r15,%rbp), %rcx
	addq	%r8, %r9
	movq	%r8, %rdx
	xorq	%r10, %rdx
	andq	%r9, %rdx
	xorq	%r10, %rdx
	addq	%rcx, %rdx
	leaq	581691458(%r14,%rdx), %rdi
	movq	%rdi, %rcx
	shlq	$17, %rcx
	shrl	$15, %edi
	orq	%rcx, %rdi
	movzbl	45(%rsi), %ecx
	shlq	$8, %rcx
	movzbl	44(%rsi), %edx
	orq	%rdx, %rcx
	movzbl	46(%rsi), %edx
	shlq	$16, %rdx
	orq	%rcx, %rdx
	movzbl	47(%rsi), %eax
	shlq	$24, %rax
	orq	%rdx, %rax
	movq	%rax, %r15
	leaq	(%r11,%r10), %rcx
	addq	%r9, %rdi
	movq	%r9, %rdx
	xorq	%r8, %rdx
	andq	%rdi, %rdx
	xorq	%r8, %rdx
	addq	%rcx, %rdx
	leaq	2009216816(%r14,%rdx), %r10
	movq	%r10, %rcx
	shlq	$22, %rcx
	shrl	$10, %r10d
	orq	%rcx, %r10
	movzbl	49(%rsi), %ecx
	shlq	$8, %rcx
	movzbl	48(%rsi), %ebp
	orq	%rbp, %rcx
	movzbl	50(%rsi), %ebp
	shlq	$16, %rbp
	orq	%rcx, %rbp
	movzbl	51(%rsi), %r11d
	shlq	$24, %r11
	orq	%rbp, %r11
	movq	%r11, -24(%rsp)         # 8-byte Spill
	movzbl	53(%rsi), %ecx
	shlq	$8, %rcx
	movzbl	52(%rsi), %ebp
	orq	%rbp, %rcx
	movzbl	54(%rsi), %eax
	shlq	$16, %rax
	orq	%rcx, %rax
	movzbl	55(%rsi), %ecx
	shlq	$24, %rcx
	orq	%rax, %rcx
	movq	%rcx, %rbp
	addq	%rdi, %r10
	movq	%rdi, %rax
	xorq	%r9, %rax
	andq	%r10, %rax
	xorq	%r9, %rax
	addq	-112(%rsp), %r8         # 8-byte Folded Reload
	leaq	1770035416(%rax,%r8), %rcx
	movq	%rcx, %rax
	shlq	$7, %rax
	shrl	$25, %ecx
	orq	%rax, %rcx
	leaq	(%r12,%r9), %rax
	addq	%r10, %rcx
	movq	%r10, %rdx
	xorq	%rdi, %rdx
	andq	%rcx, %rdx
	xorq	%rdi, %rdx
	addq	%rax, %rdx
	leaq	96508382(%r14,%rdx), %r8
	movq	%r8, %rax
	shlq	$12, %rax
	shrl	$20, %r8d
	orq	%rax, %r8
	movzbl	57(%rsi), %eax
	shlq	$8, %rax
	movzbl	56(%rsi), %edx
	orq	%rdx, %rax
	movzbl	58(%rsi), %edx
	shlq	$16, %rdx
	orq	%rax, %rdx
	movzbl	59(%rsi), %eax
	shlq	$24, %rax
	orq	%rdx, %rax
	movq	%rax, %r12
	leaq	(%rbx,%rdi), %rax
	movq	%rbx, %r13
	movq	%r13, -40(%rsp)         # 8-byte Spill
	addq	%rcx, %r8
	movq	%rcx, %rdx
	xorq	%r10, %rdx
	andq	%r8, %rdx
	xorq	%r10, %rdx
	addq	%rax, %rdx
	leaq	2054880736(%r14,%rdx), %rdi
	movq	%rdi, %rax
	shlq	$17, %rax
	shrl	$15, %edi
	orq	%rax, %rdi
	movzbl	61(%rsi), %eax
	shlq	$8, %rax
	movzbl	60(%rsi), %edx
	orq	%rdx, %rax
	movzbl	62(%rsi), %edx
	shlq	$16, %rdx
	orq	%rax, %rdx
	movzbl	63(%rsi), %eax
	shlq	$24, %rax
	orq	%rdx, %rax
	movq	%rax, %rbx
	movq	%r15, %r9
	movq	%r9, -80(%rsp)          # 8-byte Spill
	leaq	(%r9,%r10), %rax
	addq	%r8, %rdi
	movq	%r8, %rdx
	xorq	%rcx, %rdx
	andq	%rdi, %rdx
	xorq	%rcx, %rdx
	addq	%rax, %rdx
	leaq	64518637(%r14,%rdx), %rax
	movq	%rax, %rdx
	shlq	$22, %rdx
	shrl	$10, %eax
	orq	%rdx, %rax
	addq	%rdi, %rax
	movq	%rdi, %rdx
	xorq	%r8, %rdx
	andq	%rax, %rdx
	xorq	%r8, %rdx
	addq	%r11, %rcx
	leaq	1804603682(%rdx,%rcx), %rdx
	movq	%rdx, %rcx
	shlq	$7, %rcx
	shrl	$25, %edx
	orq	%rcx, %rdx
	leaq	(%rbp,%r8), %rcx
	movq	%rbp, %r15
	addq	%rax, %rdx
	movq	%rax, %rsi
	xorq	%rdi, %rsi
	andq	%rdx, %rsi
	xorq	%rdi, %rsi
	addq	%rcx, %rsi
	leaq	2014581698(%r14,%rsi), %rcx
	movq	%rcx, %rsi
	shlq	$12, %rsi
	shrl	$20, %ecx
	orq	%rsi, %rcx
	leaq	(%r12,%rdi), %rsi
	movq	%r12, %r11
	addq	%rdx, %rcx
	movq	%rdx, %rdi
	xorq	%rax, %rdi
	andq	%rcx, %rdi
	xorq	%rax, %rdi
	addq	%rsi, %rdi
	leaq	552920509(%r14,%rdi), %rdi
	movq	%rdi, %rsi
	shlq	$17, %rsi
	shrl	$15, %edi
	orq	%rsi, %rdi
	addq	%rcx, %rdi
	movq	%rcx, %rsi
	xorq	%rdx, %rsi
	andq	%rdi, %rsi
	xorq	%rdx, %rsi
	addq	%rbx, %rax
	movq	%rbx, -8(%rsp)          # 8-byte Spill
	leaq	1236535329(%rsi,%rax), %r8
	movq	%r8, %rax
	shlq	$22, %rax
	shrl	$10, %r8d
	orq	%rax, %r8
	addq	%rdi, %r8
	movq	-128(%rsp), %rax        # 8-byte Reload
	addq	%rax, %rdx
	movq	%r8, %rax
	xorq	%rdi, %rax
	andq	%rcx, %rax
	xorq	%rdi, %rax
	addq	%rdx, %rax
	leaq	1889126289(%r14,%rax), %rdx
	movq	%rdx, %rax
	shlq	$5, %rax
	shrl	$27, %edx
	orq	%rax, %rdx
	movq	-88(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx), %rax
	addq	%r8, %rdx
	movq	%rdx, %rcx
	xorq	%r8, %rcx
	andq	%rdi, %rcx
	xorq	%r8, %rcx
	addq	%rax, %rcx
	leaq	985421167(%r14,%rcx), %rcx
	movq	%rcx, %rax
	shlq	$9, %rax
	shrl	$23, %ecx
	orq	%rax, %rcx
	addq	%rdx, %rcx
	movq	%rcx, %rax
	xorq	%rdx, %rax
	andq	%r8, %rax
	xorq	%rdx, %rax
	addq	%r9, %rdi
	leaq	643717713(%rax,%rdi), %rax
	movq	%rax, %rsi
	shlq	$14, %rsi
	shrl	$18, %eax
	orq	%rsi, %rax
	movq	-56(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%r8), %rsi
	addq	%rcx, %rax
	movq	%rax, %rdi
	xorq	%rcx, %rdi
	andq	%rdx, %rdi
	xorq	%rcx, %rdi
	addq	%rsi, %rdi
	leaq	1681025497(%r14,%rdi), %r8
	movq	%r8, %rsi
	shlq	$20, %rsi
	shrl	$12, %r8d
	orq	%rsi, %r8
	addq	%rax, %r8
	movq	-104(%rsp), %r10        # 8-byte Reload
	addq	%r10, %rdx
	movq	%r8, %rsi
	xorq	%rax, %rsi
	andq	%rcx, %rsi
	xorq	%rax, %rsi
	addq	%rdx, %rsi
	leaq	1353364108(%r14,%rsi), %rdx
	movq	%rdx, %rsi
	shlq	$5, %rsi
	shrl	$27, %edx
	orq	%rsi, %rdx
	addq	%r8, %rdx
	movq	%rdx, %rsi
	xorq	%r8, %rsi
	andq	%rax, %rsi
	xorq	%r8, %rsi
	addq	%r13, %rcx
	leaq	38016083(%rsi,%rcx), %rdi
	movq	%rdi, %rcx
	shlq	$9, %rcx
	shrl	$23, %edi
	orq	%rcx, %rdi
	addq	%rdx, %rdi
	addq	%rbx, %rax
	movq	%rdi, %rcx
	xorq	%rdx, %rcx
	andq	%r8, %rcx
	xorq	%rdx, %rcx
	addq	%rax, %rcx
	leaq	1394444464(%r14,%rcx), %rcx
	movq	%rcx, %rax
	shlq	$14, %rax
	shrl	$18, %ecx
	orq	%rax, %rcx
	movq	-120(%rsp), %rbx        # 8-byte Reload
	leaq	(%rbx,%r8), %rax
	addq	%rdi, %rcx
	movq	%rcx, %rsi
	xorq	%rdi, %rsi
	andq	%rdx, %rsi
	xorq	%rdi, %rsi
	addq	%rax, %rsi
	leaq	1649384951(%r14,%rsi), %rax
	movq	%rax, %rsi
	shlq	$20, %rsi
	shrl	$12, %eax
	orq	%rsi, %rax
	addq	%rcx, %rax
	movq	%rax, %rsi
	xorq	%rcx, %rsi
	andq	%rdi, %rsi
	xorq	%rcx, %rsi
	movq	-32(%rsp), %r12         # 8-byte Reload
	addq	%r12, %rdx
	leaq	568446438(%rsi,%rdx), %r8
	movq	%r8, %rdx
	shlq	$5, %rdx
	shrl	$27, %r8d
	orq	%rdx, %r8
	leaq	(%r11,%rdi), %rdx
	movq	%r11, -48(%rsp)         # 8-byte Spill
	addq	%rax, %r8
	movq	%r8, %rsi
	xorq	%rax, %rsi
	andq	%rcx, %rsi
	xorq	%rax, %rsi
	addq	%rdx, %rsi
	leaq	1035119109(%r14,%rsi), %rdi
	movq	%rdi, %rdx
	shlq	$9, %rdx
	shrl	$23, %edi
	orq	%rdx, %rdi
	addq	%r8, %rdi
	addq	-72(%rsp), %rcx         # 8-byte Folded Reload
	movq	%rdi, %rdx
	xorq	%r8, %rdx
	andq	%rax, %rdx
	xorq	%r8, %rdx
	addq	%rcx, %rdx
	leaq	1867558838(%r14,%rdx), %rcx
	movq	%rcx, %rdx
	shlq	$14, %rdx
	shrl	$18, %ecx
	orq	%rdx, %rcx
	addq	%rdi, %rcx
	movq	%rcx, %rdx
	xorq	%rdi, %rdx
	andq	%r8, %rdx
	xorq	%rdi, %rdx
	movq	-112(%rsp), %r9         # 8-byte Reload
	addq	%r9, %rax
	leaq	1163531501(%rdx,%rax), %rdx
	movq	%rdx, %rax
	shlq	$20, %rax
	shrl	$12, %edx
	orq	%rax, %rdx
	leaq	(%r15,%r8), %rax
	movq	%r15, -16(%rsp)         # 8-byte Spill
	addq	%rcx, %rdx
	movq	%rdx, %rsi
	xorq	%rcx, %rsi
	andq	%rdi, %rsi
	xorq	%rcx, %rsi
	addq	%rax, %rsi
	leaq	610241332(%r14,%rsi), %rax
	movq	%rax, %rsi
	shlq	$5, %rsi
	shrl	$27, %eax
	orq	%rsi, %rax
	movq	-64(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdi), %rsi
	addq	%rdx, %rax
	movq	%rax, %rdi
	xorq	%rdx, %rdi
	andq	%rcx, %rdi
	xorq	%rdx, %rdi
	addq	%rsi, %rdi
	leaq	2003519015(%r14,%rdi), %rdi
	movq	%rdi, %rsi
	shlq	$9, %rsi
	shrl	$23, %edi
	orq	%rsi, %rdi
	addq	%rax, %rdi
	movq	%rdi, %rsi
	xorq	%rax, %rsi
	andq	%rdx, %rsi
	xorq	%rax, %rsi
	movq	-96(%rsp), %rbp         # 8-byte Reload
	addq	%rbp, %rcx
	leaq	1735328473(%rsi,%rcx), %rcx
	movq	%rcx, %rsi
	shlq	$14, %rsi
	shrl	$18, %ecx
	orq	%rsi, %rcx
	movq	-24(%rsp), %r13         # 8-byte Reload
	leaq	(%r13,%rdx), %rdx
	addq	%rdi, %rcx
	movq	%rcx, %rsi
	xorq	%rdi, %rsi
	leaq	(%r10,%rax), %r8
	andq	%rsi, %rax
	xorq	%rdi, %rax
	addq	%rdx, %rax
	leaq	128315065(%r14,%rax), %rdx
	movq	%rdx, %rax
	shlq	$20, %rax
	shrl	$12, %edx
	orq	%rax, %rdx
	addq	%rcx, %rdx
	xorq	%rdx, %rsi
	addq	%r8, %rsi
	leaq	2054544241(%r14,%rsi), %rax
	movq	%rax, %rsi
	shlq	$4, %rsi
	shrl	$28, %eax
	orq	%rsi, %rax
	addq	%rdx, %rax
	addq	%r9, %rdi
	movq	%rax, %rsi
	xorq	%rdx, %rsi
	leaq	(%r11,%rdx), %r8
	xorq	%rcx, %rdx
	xorq	%rax, %rdx
	addq	%rdi, %rdx
	leaq	32348336(%r14,%rdx), %rdi
	movq	%rdi, %rdx
	shlq	$11, %rdx
	shrl	$21, %edi
	orq	%rdx, %rdi
	addq	%rax, %rdi
	xorq	%rdi, %rsi
	addq	-80(%rsp), %rcx         # 8-byte Folded Reload
	leaq	1839030562(%rsi,%rcx), %rdx
	movq	%rdx, %rcx
	shlq	$16, %rcx
	shrl	$16, %edx
	orq	%rcx, %rdx
	addq	%rdi, %rdx
	movq	-128(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rax), %rcx
	xorq	%rdi, %rax
	xorq	%rdx, %rax
	addq	%r8, %rax
	leaq	2019613243(%r14,%rax), %r8
	movq	%r8, %rax
	shlq	$23, %rax
	shrl	$9, %r8d
	orq	%rax, %r8
	addq	%rdx, %r8
	movq	%rdx, %rax
	xorq	%rdi, %rax
	xorq	%r8, %rax
	addq	%rcx, %rax
	leaq	523930739(%r14,%rax), %rcx
	movq	%rcx, %rax
	shlq	$4, %rax
	shrl	$28, %ecx
	orq	%rax, %rcx
	addq	%r8, %rcx
	movq	%r8, %rax
	xorq	%rdx, %rax
	xorq	%rcx, %rax
	addq	%rbx, %rdi
	leaq	1272893353(%rax,%rdi), %rax
	movq	%rax, %rsi
	shlq	$11, %rsi
	shrl	$21, %eax
	orq	%rsi, %rax
	addq	%rcx, %rax
	addq	%rbp, %rdx
	movq	%rcx, %rsi
	xorq	%r8, %rsi
	xorq	%rax, %rsi
	addq	%rdx, %rsi
	leaq	1899425167(%r14,%rsi), %rdi
	movq	%rdi, %rdx
	shlq	$16, %rdx
	shrl	$16, %edi
	orq	%rdx, %rdi
	addq	%rax, %rdi
	movq	-40(%rsp), %rbp         # 8-byte Reload
	addq	%rbp, %r8
	movq	%rdi, %rsi
	xorq	%rax, %rsi
	movq	-56(%rsp), %r9          # 8-byte Reload
	leaq	(%r9,%rax), %r10
	xorq	%rcx, %rax
	xorq	%rdi, %rax
	addq	%r8, %rax
	leaq	960192159(%r14,%rax), %rdx
	movq	%rdx, %rax
	shlq	$23, %rax
	shrl	$9, %edx
	orq	%rax, %rdx
	addq	%rdi, %rdx
	xorq	%rdx, %rsi
	addq	%r15, %rcx
	leaq	681279174(%rsi,%rcx), %rcx
	movq	%rcx, %rax
	shlq	$4, %rax
	shrl	$28, %ecx
	orq	%rax, %rcx
	addq	%rdx, %rcx
	movq	-72(%rsp), %r15         # 8-byte Reload
	leaq	(%r15,%rdi), %rsi
	xorq	%rdx, %rdi
	xorq	%rcx, %rdi
	addq	%r10, %rdi
	leaq	1696385577(%r14,%rdi), %rax
	movq	%rax, %rdi
	shlq	$11, %rdi
	shrl	$21, %eax
	orq	%rdi, %rax
	addq	%rcx, %rax
	movq	%rax, %rdi
	xorq	%rcx, %rdi
	leaq	(%r12,%rcx), %r8
	xorq	%rdx, %rcx
	xorq	%rax, %rcx
	addq	%rsi, %rcx
	leaq	1332400820(%r14,%rcx), %rcx
	movq	%rcx, %rsi
	shlq	$16, %rsi
	shrl	$16, %ecx
	orq	%rsi, %rcx
	addq	%rax, %rcx
	xorq	%rcx, %rdi
	movq	-88(%rsp), %r10         # 8-byte Reload
	addq	%r10, %rdx
	leaq	76029189(%rdi,%rdx), %rdx
	movq	%rdx, %rsi
	shlq	$23, %rsi
	shrl	$9, %edx
	orq	%rsi, %rdx
	addq	%rcx, %rdx
	movq	%rcx, %rsi
	xorq	%rax, %rsi
	xorq	%rdx, %rsi
	addq	%r8, %rsi
	leaq	1414558312(%r14,%rsi), %rdi
	movq	%rdi, %rsi
	shlq	$4, %rsi
	shrl	$28, %edi
	orq	%rsi, %rdi
	addq	%rdx, %rdi
	movq	%r13, %rbx
	addq	%rbx, %rax
	movq	%rdi, %rsi
	xorq	%rdx, %rsi
	movq	-64(%rsp), %r13         # 8-byte Reload
	leaq	(%r13,%rdx), %r8
	xorq	%rcx, %rdx
	xorq	%rdi, %rdx
	addq	%rax, %rdx
	leaq	1633106964(%r14,%rdx), %rdx
	movq	%rdx, %rax
	shlq	$11, %rax
	shrl	$21, %edx
	orq	%rax, %rdx
	addq	%rdi, %rdx
	xorq	%rdx, %rsi
	movq	-8(%rsp), %r11          # 8-byte Reload
	addq	%r11, %rcx
	leaq	530742520(%rsi,%rcx), %rax
	movq	%rax, %rcx
	shlq	$16, %rcx
	shrl	$16, %eax
	orq	%rcx, %rax
	addq	%rdi, %r9
	addq	%rdx, %rax
	xorq	%rdx, %rdi
	xorq	%rax, %rdi
	addq	%r8, %rdi
	leaq	1059584148(%r14,%rdi), %rdi
	movq	%rdi, %rcx
	shlq	$23, %rcx
	shrl	$9, %edi
	orq	%rcx, %rdi
	addq	%rax, %rdi
	movq	%rdx, %rcx
	notq	%rcx
	orq	%rdi, %rcx
	xorq	%rax, %rcx
	addq	%r9, %rcx
	leaq	1856291955(%r14,%rcx), %rcx
	movq	%rcx, %rsi
	shlq	$6, %rsi
	shrl	$26, %ecx
	orq	%rsi, %rcx
	addq	-96(%rsp), %rdx         # 8-byte Folded Reload
	addq	%rdi, %rcx
	movq	%rax, %rsi
	notq	%rsi
	orq	%rcx, %rsi
	xorq	%rdi, %rsi
	leaq	1126891415(%rsi,%rdx), %rdx
	movq	%rdx, %rsi
	shlq	$10, %rsi
	shrl	$22, %edx
	orq	%rsi, %rdx
	addq	-48(%rsp), %rax         # 8-byte Folded Reload
	addq	%rcx, %rdx
	movq	-104(%rsp), %rsi        # 8-byte Reload
	addq	%rdi, %rsi
	notq	%rdi
	orq	%rdx, %rdi
	xorq	%rcx, %rdi
	addq	%rax, %rdi
	leaq	638567894(%r14,%rdi), %rdi
	movq	%rdi, %rax
	shlq	$15, %rax
	shrl	$17, %edi
	orq	%rax, %rdi
	addq	%rdx, %rdi
	movq	%rcx, %rax
	notq	%rax
	orq	%rdi, %rax
	xorq	%rdx, %rax
	addq	%rsi, %rax
	leaq	1997488744(%r14,%rax), %rax
	movq	%rax, %rsi
	shlq	$21, %rsi
	shrl	$11, %eax
	orq	%rsi, %rax
	addq	%rbx, %rcx
	movq	%r15, %rsi
	addq	%rdx, %rsi
	addq	%rdi, %rax
	notq	%rdx
	orq	%rax, %rdx
	xorq	%rdi, %rdx
	leaq	1700485571(%rdx,%rcx), %rdx
	movq	%rdx, %rcx
	shlq	$6, %rcx
	shrl	$26, %edx
	orq	%rcx, %rdx
	addq	%rdi, %rbp
	addq	%rax, %rdx
	notq	%rdi
	orq	%rdx, %rdi
	xorq	%rax, %rdi
	addq	%rsi, %rdi
	leaq	159936193(%r14,%rdi), %rcx
	movq	%rcx, %rsi
	shlq	$10, %rsi
	shrl	$22, %ecx
	orq	%rsi, %rcx
	addq	%rdx, %rcx
	movq	%rax, %rsi
	notq	%rsi
	orq	%rcx, %rsi
	xorq	%rdx, %rsi
	addq	%rbp, %rsi
	leaq	2053871276(%r14,%rsi), %rdi
	movq	%rdi, %rsi
	shlq	$15, %rsi
	shrl	$17, %edi
	orq	%rsi, %rdi
	addq	-128(%rsp), %rax        # 8-byte Folded Reload
	addq	%rcx, %rdi
	movq	%rdx, %rsi
	notq	%rsi
	orq	%rdi, %rsi
	xorq	%rcx, %rsi
	addq	%rsi, %rax
	addq	%r14, %rax
	movq	%rax, %rsi
	shlq	$21, %rsi
	shrl	$11, %eax
	orq	%rsi, %rax
	addq	%rdi, %rax
	addq	-112(%rsp), %rdx        # 8-byte Folded Reload
	movq	%rcx, %rsi
	notq	%rsi
	orq	%rax, %rsi
	xorq	%rdi, %rsi
	leaq	1873313359(%rsi,%rdx), %rdx
	movq	%rdx, %rsi
	shlq	$6, %rsi
	shrl	$26, %edx
	orq	%rsi, %rdx
	addq	%r11, %rcx
	addq	%rax, %rdx
	addq	%rdi, %r10
	notq	%rdi
	orq	%rdx, %rdi
	xorq	%rax, %rdi
	addq	%rcx, %rdi
	leaq	2024311055(%r14,%rdi), %rsi
	movq	%rsi, %rcx
	shlq	$10, %rcx
	shrl	$22, %esi
	orq	%rcx, %rsi
	addq	%rdx, %rsi
	movq	%rax, %rcx
	notq	%rcx
	orq	%rsi, %rcx
	xorq	%rdx, %rcx
	addq	%r10, %rcx
	leaq	494724419(%r14,%rcx), %rcx
	movq	%rcx, %rdi
	shlq	$15, %rdi
	shrl	$17, %ecx
	orq	%rdi, %rcx
	addq	-16(%rsp), %rax         # 8-byte Folded Reload
	movq	-120(%rsp), %rdi        # 8-byte Reload
	addq	%rdx, %rdi
	addq	%rsi, %rcx
	notq	%rdx
	orq	%rcx, %rdx
	xorq	%rsi, %rdx
	leaq	1309151649(%rdx,%rax), %rdx
	movq	%rdx, %rax
	shlq	$21, %rax
	shrl	$11, %edx
	orq	%rax, %rdx
	addq	%rcx, %rdx
	movq	-80(%rsp), %rbp         # 8-byte Reload
	addq	%rsi, %rbp
	notq	%rsi
	orq	%rdx, %rsi
	xorq	%rcx, %rsi
	addq	%rdi, %rsi
	leaq	1909399729(%r14,%rsi), %rax
	movq	%rax, %rsi
	shlq	$6, %rsi
	shrl	$26, %eax
	orq	%rsi, %rax
	addq	%rdx, %rax
	movq	%rcx, %rsi
	notq	%rsi
	orq	%rax, %rsi
	xorq	%rdx, %rsi
	addq	%rbp, %rsi
	leaq	934712420(%r14,%rsi), %rsi
	movq	%rsi, %rdi
	shlq	$10, %rdi
	shrl	$22, %esi
	orq	%rdi, %rsi
	addq	%r13, %rcx
	addq	%rdx, %r12
	addq	%rax, %rsi
	notq	%rdx
	orq	%rsi, %rdx
	xorq	%rax, %rdx
	leaq	718787259(%rdx,%rcx), %rcx
	movq	%rcx, %rdx
	shlq	$15, %rdx
	shrl	$17, %ecx
	orq	%rdx, %rcx
	addq	%rsi, %rcx
	movq	%rax, %rdx
	notq	%rdx
	orq	%rcx, %rdx
	xorq	%rsi, %rdx
	addq	%r12, %rdx
	leaq	1711437248(%r14,%rdx), %rdx
	movq	%rdx, %rdi
	shlq	$21, %rdi
	shrl	$11, %edx
	orq	%rdi, %rdx
	addq	8(%rsp), %rax           # 8-byte Folded Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rax, 16(%rdi)
	movd	%rcx, %xmm0
	addq	24(%rdi), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 24(%rdi)
	movd	%rsi, %xmm1
	movdqu	32(%rdi), %xmm2
	punpcklqdq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	paddq	%xmm2, %xmm0
	movdqu	%xmm0, 32(%rdi)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	md5_process, .Lfunc_end1-md5_process
	.cfi_endproc

	.globl	md5_update
	.p2align	4, 0x90
	.type	md5_update,@function
md5_update:                             # @md5_update
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movq	%rdi, %r15
	testq	%r13, %r13
	je	.LBB2_11
# BB#1:
	movq	(%r15), %rbp
	leal	(%r13,%rbp), %eax
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
	andl	$63, %ebp
	movq	%rax, (%r15)
	cmpq	%r13, %rax
	jae	.LBB2_3
# BB#2:
	incq	8(%r15)
.LBB2_3:
	testq	%rbp, %rbp
	je	.LBB2_6
# BB#4:
	movl	$64, %r12d
	subq	%rbp, %r12
	cmpq	%r13, %r12
	ja	.LBB2_6
# BB#5:
	leaq	48(%r15), %rbx
	leaq	48(%r15,%rbp), %rdi
	movq	%rsi, %rbp
	movq	%r12, %rdx
	callq	memcpy
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	md5_process
	movq	%rbp, %rsi
	subq	%r12, %r13
	addq	%r12, %rsi
	xorl	%ebp, %ebp
.LBB2_6:                                # %.preheader
	cmpq	$64, %r13
	jb	.LBB2_10
# BB#7:                                 # %.lr.ph.preheader
	leaq	-64(%r13), %r14
	movq	%r14, %r12
	andq	$-64, %r12
	leaq	64(%r12), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	.p2align	4, 0x90
.LBB2_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	md5_process
	addq	$-64, %r13
	addq	$64, %rbx
	cmpq	$63, %r13
	ja	.LBB2_8
# BB#9:                                 # %._crit_edge.loopexit
	subq	%r12, %r14
	movq	16(%rsp), %rsi          # 8-byte Reload
	addq	8(%rsp), %rsi           # 8-byte Folded Reload
	movq	%r14, %r13
.LBB2_10:                               # %._crit_edge
	testq	%r13, %r13
	je	.LBB2_11
# BB#12:
	leaq	48(%r15,%rbp), %rdi
	movq	%r13, %rdx
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	memcpy                  # TAILCALL
.LBB2_11:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	md5_update, .Lfunc_end2-md5_update
	.cfi_endproc

	.globl	md5_finish
	.p2align	4, 0x90
	.type	md5_finish,@function
md5_finish:                             # @md5_finish
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 80
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	(%r15), %rcx
	movq	8(%r15), %rax
	movq	%rcx, %rsi
	shrq	$29, %rsi
	leaq	(,%rax,8), %rdx
	orq	%rsi, %rdx
	movl	%ecx, %ebx
	shlb	$3, %bl
	movb	%bl, (%rsp)
	movq	%rcx, %rsi
	shrq	$5, %rsi
	movb	%sil, 1(%rsp)
	movq	%rcx, %rsi
	shrq	$13, %rsi
	movb	%sil, 2(%rsp)
	movq	%rcx, %rsi
	shrq	$21, %rsi
	movb	%sil, 3(%rsp)
	movb	%dl, 4(%rsp)
	movb	%dh, 5(%rsp)  # NOREX
	movq	%rdx, %rsi
	shrq	$16, %rsi
	movb	%sil, 6(%rsp)
	shrq	$24, %rdx
	movb	%dl, 7(%rsp)
	movl	%ecx, %r12d
	andl	$63, %r12d
	cmpq	$56, %r12
	movl	$56, %esi
	movl	$120, %edx
	cmovbq	%rsi, %rdx
	subq	%r12, %rdx
	je	.LBB3_13
# BB#1:
	addl	%edx, %ecx
	movq	%rcx, (%r15)
	cmpq	%rdx, %rcx
	jae	.LBB3_3
# BB#2:
	incq	%rax
	movq	%rax, 8(%r15)
.LBB3_3:
	testq	%r12, %r12
	movl	$md5_padding, %esi
	je	.LBB3_7
# BB#4:
	movl	$64, %r13d
	subq	%r12, %r13
	movq	%rdx, %rbp
	subq	%r13, %rbp
	jb	.LBB3_7
# BB#5:
	leaq	48(%r15), %rbx
	leaq	48(%r15,%r12), %rdi
	movl	$md5_padding, %esi
	movq	%r13, %rdx
	callq	memcpy
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	md5_process
	leaq	md5_padding(%r13), %rsi
	xorl	%r12d, %r12d
	cmpq	$64, %rbp
	jae	.LBB3_8
	jmp	.LBB3_11
.LBB3_7:
	movq	%rdx, %rbp
	cmpq	$64, %rbp
	jb	.LBB3_11
.LBB3_8:                                # %.lr.ph.preheader.i
	leaq	-64(%rbp), %r13
	movq	%r13, 16(%rsp)          # 8-byte Spill
	andq	$-64, %r13
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rsi, %rbx
	.p2align	4, 0x90
.LBB3_9:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	md5_process
	addq	$-64, %rbp
	addq	$64, %rbx
	cmpq	$63, %rbp
	ja	.LBB3_9
# BB#10:                                # %._crit_edge.loopexit.i
	movq	16(%rsp), %rbp          # 8-byte Reload
	subq	%r13, %rbp
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	64(%rsi,%r13), %rsi
.LBB3_11:                               # %._crit_edge.i
	testq	%rbp, %rbp
	je	.LBB3_13
# BB#12:
	leaq	48(%r15,%r12), %rdi
	movq	%rbp, %rdx
	callq	memcpy
.LBB3_13:                               # %md5_update.exit
	movq	(%r15), %rcx
	movl	%ecx, %eax
	andl	$63, %eax
	addl	$8, %ecx
	movq	%rcx, (%r15)
	cmpl	$7, %ecx
	ja	.LBB3_15
# BB#14:
	incq	8(%r15)
.LBB3_15:
	movq	%rsp, %r12
	testq	%rax, %rax
	movl	$8, %ebx
	je	.LBB3_23
# BB#16:
	movl	$64, %r13d
	subq	%rax, %r13
	cmpq	$8, %r13
	ja	.LBB3_23
# BB#17:                                # %.preheader.i59
	leaq	48(%r15), %rbx
	leaq	48(%r15,%rax), %rdi
	movq	%rsp, %rsi
	movq	%r13, %rdx
	callq	memcpy
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	md5_process
	movl	$8, %ebx
	subq	%r13, %rbx
	leaq	(%rsp,%r13), %r12
	cmpq	$64, %rbx
	jb	.LBB3_21
# BB#18:                                # %.lr.ph.preheader.i60
	leaq	-64(%rbx), %r13
	movq	%r13, 8(%rsp)           # 8-byte Spill
	andq	$-64, %r13
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB3_19:                               # %.lr.ph.i63
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	md5_process
	addq	$-64, %rbx
	addq	$64, %rbp
	cmpq	$63, %rbx
	ja	.LBB3_19
# BB#20:                                # %._crit_edge.loopexit.i65
	movq	8(%rsp), %rbx           # 8-byte Reload
	subq	%r13, %rbx
	leaq	64(%r12,%r13), %r12
.LBB3_21:                               # %._crit_edge.i68
	testq	%rbx, %rbx
	je	.LBB3_24
# BB#22:
	xorl	%eax, %eax
.LBB3_23:                               # %._crit_edge.i68.thread
	leaq	48(%r15,%rax), %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	memcpy
.LBB3_24:                               # %md5_update.exit69
	movb	16(%r15), %al
	movb	%al, (%r14)
	movb	17(%r15), %al
	movb	%al, 1(%r14)
	movb	18(%r15), %al
	movb	%al, 2(%r14)
	movb	19(%r15), %al
	movb	%al, 3(%r14)
	movb	24(%r15), %al
	movb	%al, 4(%r14)
	movb	25(%r15), %al
	movb	%al, 5(%r14)
	movb	26(%r15), %al
	movb	%al, 6(%r14)
	movb	27(%r15), %al
	movb	%al, 7(%r14)
	movb	32(%r15), %al
	movb	%al, 8(%r14)
	movb	33(%r15), %al
	movb	%al, 9(%r14)
	movb	34(%r15), %al
	movb	%al, 10(%r14)
	movb	35(%r15), %al
	movb	%al, 11(%r14)
	movb	40(%r15), %al
	movb	%al, 12(%r14)
	movb	41(%r15), %al
	movb	%al, 13(%r14)
	movb	42(%r15), %al
	movb	%al, 14(%r14)
	movb	43(%r15), %al
	movb	%al, 15(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	md5_finish, .Lfunc_end3-md5_finish
	.cfi_endproc

	.globl	my_rand_r
	.p2align	4, 0x90
	.type	my_rand_r,@function
my_rand_r:                              # @my_rand_r
	.cfi_startproc
# BB#0:
	imull	$1664525, (%rdi), %eax  # imm = 0x19660D
	addl	$1013904223, %eax       # imm = 0x3C6EF35F
	movl	%eax, (%rdi)
	shrl	$16, %eax
	andl	$32767, %eax            # imm = 0x7FFF
	retq
.Lfunc_end4:
	.size	my_rand_r, .Lfunc_end4-my_rand_r
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.quad	1732584193              # 0x67452301
	.quad	4023233417              # 0xefcdab89
.LCPI5_1:
	.quad	2562383102              # 0x98badcfe
	.quad	271733878               # 0x10325476
.LCPI5_2:
	.zero	16
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$100184, %rsp           # imm = 0x18758
.Lcfi45:
	.cfi_def_cfa_offset 100240
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movl	$1, %ecx
	movl	$4, %eax
	.p2align	4, 0x90
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	imull	$1664525, %ecx, %ecx    # imm = 0x19660D
	addl	$1013904223, %ecx       # imm = 0x3C6EF35F
	imull	$1664525, %ecx, %edx    # imm = 0x19660D
	shrl	$16, %ecx
	movb	%cl, 172(%rsp,%rax)
	addl	$1013904223, %edx       # imm = 0x3C6EF35F
	imull	$1664525, %edx, %ecx    # imm = 0x19660D
	shrl	$16, %edx
	movb	%dl, 173(%rsp,%rax)
	addl	$1013904223, %ecx       # imm = 0x3C6EF35F
	imull	$1664525, %ecx, %edx    # imm = 0x19660D
	shrl	$16, %ecx
	movb	%cl, 174(%rsp,%rax)
	addl	$1013904223, %edx       # imm = 0x3C6EF35F
	imull	$1664525, %edx, %ecx    # imm = 0x19660D
	shrl	$16, %edx
	movb	%dl, 175(%rsp,%rax)
	addl	$1013904223, %ecx       # imm = 0x3C6EF35F
	movl	%ecx, %edx
	shrl	$16, %edx
	movb	%dl, 176(%rsp,%rax)
	addq	$5, %rax
	cmpq	$100004, %rax           # imm = 0x186A4
	jne	.LBB5_1
# BB#2:
	cmpl	$2, %edi
	jne	.LBB5_3
# BB#4:                                 # %.preheader
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	testl	%eax, %eax
	jne	.LBB5_5
	jmp	.LBB5_23
.LBB5_3:
	movl	$1, %eax
.LBB5_5:                                # %.lr.ph
	cltq
	leaq	64(%rsp), %r15
	.p2align	4, 0x90
.LBB5_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_8 Depth 2
                                        #       Child Loop BB5_16 Depth 3
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 64(%rsp)
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [1732584193,4023233417]
	movaps	%xmm0, 80(%rsp)
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [2562383102,271733878]
	movaps	%xmm0, 96(%rsp)
	movl	$100000, %r12d          # imm = 0x186A0
	movq	%rax, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	subq	%rax, %r12
	je	.LBB5_22
# BB#7:                                 # %.split.preheader
                                        #   in Loop: Header=BB5_6 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	176(%rsp,%rax), %r13
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	jmp	.LBB5_8
	.p2align	4, 0x90
.LBB5_21:                               # %md5_update.exit..split_crit_edge
                                        #   in Loop: Header=BB5_8 Depth=2
	movq	64(%rsp), %rax
.LBB5_8:                                # %.split
                                        #   Parent Loop BB5_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_16 Depth 3
	movl	%eax, %r14d
	andl	$63, %r14d
	leal	(%r12,%rax), %eax
	movq	%rax, 64(%rsp)
	cmpq	%r12, %rax
	jae	.LBB5_10
# BB#9:                                 #   in Loop: Header=BB5_8 Depth=2
	incq	72(%rsp)
.LBB5_10:                               #   in Loop: Header=BB5_8 Depth=2
	testq	%r14, %r14
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	je	.LBB5_11
# BB#12:                                #   in Loop: Header=BB5_8 Depth=2
	movl	$64, %ebx
	subq	%r14, %rbx
	movq	%r12, %rbp
	subq	%rbx, %rbp
	jb	.LBB5_11
# BB#13:                                #   in Loop: Header=BB5_8 Depth=2
	leaq	112(%rsp,%r14), %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%r15, %rdi
	leaq	112(%rsp), %rsi
	callq	md5_process
	addq	%r13, %rbx
	xorl	%r14d, %r14d
	cmpq	$64, %rbp
	jae	.LBB5_15
	jmp	.LBB5_18
	.p2align	4, 0x90
.LBB5_11:                               #   in Loop: Header=BB5_8 Depth=2
	movq	%r12, %rbp
	movq	%r13, %rbx
	cmpq	$64, %rbp
	jb	.LBB5_18
.LBB5_15:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB5_8 Depth=2
	leaq	-64(%rbp), %r13
	movq	%r13, 56(%rsp)          # 8-byte Spill
	andq	$-64, %r13
	movq	%r15, %r12
	movq	%rbx, %r15
	.p2align	4, 0x90
.LBB5_16:                               # %.lr.ph.i
                                        #   Parent Loop BB5_6 Depth=1
                                        #     Parent Loop BB5_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	md5_process
	addq	$-64, %rbp
	addq	$64, %r15
	cmpq	$63, %rbp
	ja	.LBB5_16
# BB#17:                                # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB5_8 Depth=2
	movq	56(%rsp), %rbp          # 8-byte Reload
	subq	%r13, %rbp
	leaq	64(%rbx,%r13), %rbx
	movq	%r12, %r15
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	40(%rsp), %r13          # 8-byte Reload
.LBB5_18:                               # %._crit_edge.i
                                        #   in Loop: Header=BB5_8 Depth=2
	testq	%rbp, %rbp
	je	.LBB5_20
# BB#19:                                #   in Loop: Header=BB5_8 Depth=2
	leaq	112(%rsp,%r14), %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
.LBB5_20:                               # %md5_update.exit
                                        #   in Loop: Header=BB5_8 Depth=2
	movl	28(%rsp), %ecx          # 4-byte Reload
	incl	%ecx
	cmpl	$512, %ecx              # imm = 0x200
	jne	.LBB5_21
.LBB5_22:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB5_6 Depth=1
	movq	%r15, %rdi
	movq	%rsp, %rsi
	callq	md5_finish
	movzbl	(%rsp), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movzbl	1(%rsp), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movzbl	2(%rsp), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movzbl	3(%rsp), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movzbl	4(%rsp), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movzbl	5(%rsp), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movzbl	6(%rsp), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movzbl	7(%rsp), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movzbl	8(%rsp), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movzbl	9(%rsp), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movzbl	10(%rsp), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movzbl	11(%rsp), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movzbl	12(%rsp), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movzbl	13(%rsp), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movzbl	14(%rsp), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movzbl	15(%rsp), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
	movq	32(%rsp), %rax          # 8-byte Reload
	decq	%rax
	testl	%eax, %eax
	jne	.LBB5_6
.LBB5_23:                               # %._crit_edge
	xorl	%eax, %eax
	addq	$100184, %rsp           # imm = 0x18758
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	main, .Lfunc_end5-main
	.cfi_endproc

	.type	md5_padding,@object     # @md5_padding
	.data
	.p2align	4
md5_padding:
	.asciz	"\200\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.size	md5_padding, 64

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%02x"
	.size	.L.str, 5


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
