	.text
	.file	"solution.bc"
	.globl	solution_alloc
	.p2align	4, 0x90
	.type	solution_alloc,@function
solution_alloc:                         # @solution_alloc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	$0, 8(%rbx)
	xorl	%eax, %eax
	callq	sm_row_alloc
	movq	%rax, (%rbx)
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	solution_alloc, .Lfunc_end0-solution_alloc
	.cfi_endproc

	.globl	solution_free
	.p2align	4, 0x90
	.type	solution_free,@function
solution_free:                          # @solution_free
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	xorl	%eax, %eax
	callq	sm_row_free
	testq	%rbx, %rbx
	je	.LBB1_1
# BB#2:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB1_1:
	popq	%rbx
	retq
.Lfunc_end1:
	.size	solution_free, .Lfunc_end1-solution_free
	.cfi_endproc

	.globl	solution_dup
	.p2align	4, 0x90
	.type	solution_dup,@function
solution_dup:                           # @solution_dup
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 32
.Lcfi7:
	.cfi_offset %rbx, -24
.Lcfi8:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	8(%r14), %eax
	movl	%eax, 8(%rbx)
	movq	(%r14), %rdi
	xorl	%eax, %eax
	callq	sm_row_dup
	movq	%rax, (%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	solution_dup, .Lfunc_end2-solution_dup
	.cfi_endproc

	.globl	solution_add
	.p2align	4, 0x90
	.type	solution_add,@function
solution_add:                           # @solution_add
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -32
.Lcfi13:
	.cfi_offset %r14, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%r14), %rdi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	sm_row_insert
	testq	%rbx, %rbx
	je	.LBB3_1
# BB#2:
	movslq	%ebp, %rax
	movl	(%rbx,%rax,4), %eax
	jmp	.LBB3_3
.LBB3_1:
	movl	$1, %eax
.LBB3_3:
	addl	%eax, 8(%r14)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	solution_add, .Lfunc_end3-solution_add
	.cfi_endproc

	.globl	solution_accept
	.p2align	4, 0x90
	.type	solution_accept,@function
solution_accept:                        # @solution_accept
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 48
.Lcfi20:
	.cfi_offset %rbx, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%r14), %rdi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	sm_row_insert
	testq	%r15, %r15
	movslq	%ebp, %rax
	je	.LBB4_1
# BB#2:
	movl	(%r15,%rax,4), %ecx
	jmp	.LBB4_3
.LBB4_1:
	movl	$1, %ecx
.LBB4_3:                                # %solution_add.exit
	addl	%ecx, 8(%r14)
	movq	16(%rbx), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.LBB4_5
	.p2align	4, 0x90
.LBB4_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rbp
	movl	(%rax), %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sm_delrow
	testq	%rbp, %rbp
	movq	%rbp, %rax
	jne	.LBB4_4
.LBB4_5:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	solution_accept, .Lfunc_end4-solution_accept
	.cfi_endproc

	.globl	solution_reject
	.p2align	4, 0x90
	.type	solution_reject,@function
solution_reject:                        # @solution_reject
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	movq	%rsi, %rdi
	movl	%ecx, %esi
	jmp	sm_delcol               # TAILCALL
.Lfunc_end5:
	.size	solution_reject, .Lfunc_end5-solution_reject
	.cfi_endproc

	.globl	solution_choose_best
	.p2align	4, 0x90
	.type	solution_choose_best,@function
solution_choose_best:                   # @solution_choose_best
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -24
.Lcfi28:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	movq	%rbx, %rax
	cmoveq	%r14, %rax
	je	.LBB6_5
# BB#1:
	testq	%r14, %r14
	je	.LBB6_5
# BB#2:
	movl	8(%rbx), %eax
	cmpl	8(%r14), %eax
	jle	.LBB6_3
# BB#4:                                 # %solution_free.exit10
	movq	(%rbx), %rdi
	xorl	%eax, %eax
	callq	sm_row_free
	movq	%rbx, %rdi
	callq	free
	movq	%r14, %rax
	jmp	.LBB6_5
.LBB6_3:                                # %solution_free.exit
	movq	(%r14), %rdi
	xorl	%eax, %eax
	callq	sm_row_free
	movq	%r14, %rdi
	callq	free
	movq	%rbx, %rax
.LBB6_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	solution_choose_best, .Lfunc_end6-solution_choose_best
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
