	.text
	.file	"7zFolderOutStream.bc"
	.globl	_ZN8NArchive3N7z16CFolderOutStreamC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CFolderOutStreamC2Ev,@function
_ZN8NArchive3N7z16CFolderOutStreamC2Ev: # @_ZN8NArchive3N7z16CFolderOutStreamC2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$0, 16(%r14)
	movl	$_ZTVN8NArchive3N7z16CFolderOutStreamE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive3N7z16CFolderOutStreamE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r14)
	movq	$0, 32(%r14)
	movq	$0, 56(%r14)
.Ltmp0:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp1:
# BB#1:
	movl	$0, 8(%rbx)
	movq	$_ZTV17COutStreamWithCRC+16, (%rbx)
	movq	$0, 16(%rbx)
	movq	%rbx, 24(%r14)
.Ltmp2:
	movq	%rbx, %rdi
	callq	*_ZTV17COutStreamWithCRC+24(%rip)
.Ltmp3:
# BB#2:                                 # %.noexc4
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp4:
	callq	*16(%rax)
.Ltmp5:
.LBB0_4:
	movq	%rbx, 32(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_5:
.Ltmp6:
	movq	%rax, %rbx
	movq	56(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_7
# BB#6:
	movq	(%rdi), %rax
.Ltmp7:
	callq	*16(%rax)
.Ltmp8:
.LBB0_7:                                # %_ZN9CMyComPtrI23IArchiveExtractCallbackED2Ev.exit
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_9
# BB#8:
	movq	(%rdi), %rax
.Ltmp9:
	callq	*16(%rax)
.Ltmp10:
.LBB0_9:                                # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_10:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN8NArchive3N7z16CFolderOutStreamC2Ev, .Lfunc_end0-_ZN8NArchive3N7z16CFolderOutStreamC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\257\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp5-.Ltmp0           #   Call between .Ltmp0 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp10-.Ltmp7          #   Call between .Ltmp7 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN8NArchive3N7z16CFolderOutStream4InitEPKNS0_18CArchiveDatabaseExEjjPK13CRecordVectorIbEP23IArchiveExtractCallbackbb
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CFolderOutStream4InitEPKNS0_18CArchiveDatabaseExEjjPK13CRecordVectorIbEP23IArchiveExtractCallbackbb,@function
_ZN8NArchive3N7z16CFolderOutStream4InitEPKNS0_18CArchiveDatabaseExEjjPK13CRecordVectorIbEP23IArchiveExtractCallbackbb: # @_ZN8NArchive3N7z16CFolderOutStream4InitEPKNS0_18CArchiveDatabaseExEjjPK13CRecordVectorIbEP23IArchiveExtractCallbackbb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%rdi, %rbx
	movb	48(%rsp), %bpl
	movb	56(%rsp), %r15b
	movq	%rsi, 40(%rbx)
	movl	%edx, 64(%rbx)
	movl	%ecx, 68(%rbx)
	movq	%r8, 48(%rbx)
	testq	%r14, %r14
	je	.LBB2_2
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB2_2:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB2_4:                                # %_ZN9CMyComPtrI23IArchiveExtractCallbackEaSEPS0_.exit
	movq	%r14, 56(%rbx)
	movb	%bpl, 76(%rbx)
	movb	%r15b, 77(%rbx)
	movl	$0, 72(%rbx)
	movb	$0, 78(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN8NArchive3N7z16CFolderOutStream17ProcessEmptyFilesEv # TAILCALL
.Lfunc_end2:
	.size	_ZN8NArchive3N7z16CFolderOutStream4InitEPKNS0_18CArchiveDatabaseExEjjPK13CRecordVectorIbEP23IArchiveExtractCallbackbb, .Lfunc_end2-_ZN8NArchive3N7z16CFolderOutStream4InitEPKNS0_18CArchiveDatabaseExEjjPK13CRecordVectorIbEP23IArchiveExtractCallbackbb
	.cfi_endproc

	.globl	_ZN8NArchive3N7z16CFolderOutStream17ProcessEmptyFilesEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CFolderOutStream17ProcessEmptyFilesEv,@function
_ZN8NArchive3N7z16CFolderOutStream17ProcessEmptyFilesEv: # @_ZN8NArchive3N7z16CFolderOutStream17ProcessEmptyFilesEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	jmp	.LBB3_1
	.p2align	4, 0x90
.LBB3_2:                                #   in Loop: Header=BB3_1 Depth=1
	movq	40(%rbx), %rdx
	addl	68(%rbx), %ecx
	movq	176(%rdx), %rdx
	movslq	%ecx, %rcx
	movq	(%rdx,%rcx,8), %rcx
	cmpq	$0, (%rcx)
	jne	.LBB3_11
# BB#3:                                 #   in Loop: Header=BB3_1 Depth=1
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z16CFolderOutStream8OpenFileEv
	testl	%eax, %eax
	jne	.LBB3_11
# BB#4:                                 #   in Loop: Header=BB3_1 Depth=1
	movq	40(%rbx), %rcx
	movl	72(%rbx), %eax
	movl	68(%rbx), %edx
	addl	%eax, %edx
	movq	176(%rcx), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rcx
	xorl	%r14d, %r14d
	cmpb	$0, 33(%rcx)
	je	.LBB3_5
.LBB3_8:                                #   in Loop: Header=BB3_1 Depth=1
	movq	24(%rbx), %rbp
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_1 Depth=1
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 16(%rbp)
	movl	72(%rbx), %eax
.LBB3_10:                               # %_ZN8NArchive3N7z16CFolderOutStream21CloseFileAndSetResultEv.exit
                                        #   in Loop: Header=BB3_1 Depth=1
	movb	$0, 78(%rbx)
	incl	%eax
	movl	%eax, 72(%rbx)
	movq	56(%rbx), %rdi
	movq	(%rdi), %rax
	movl	%r14d, %esi
	callq	*72(%rax)
	testl	%eax, %eax
	je	.LBB3_1
	jmp	.LBB3_11
	.p2align	4, 0x90
.LBB3_5:                                #   in Loop: Header=BB3_1 Depth=1
	cmpb	$0, 34(%rcx)
	je	.LBB3_8
# BB#6:                                 #   in Loop: Header=BB3_1 Depth=1
	cmpb	$0, 77(%rbx)
	je	.LBB3_8
# BB#7:                                 #   in Loop: Header=BB3_1 Depth=1
	movq	24(%rbx), %rdx
	movl	32(%rdx), %edx
	notl	%edx
	xorl	%esi, %esi
	cmpl	%edx, 12(%rcx)
	setne	%sil
	leal	(%rsi,%rsi,2), %r14d
	jmp	.LBB3_8
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movl	72(%rbx), %ecx
	movq	48(%rbx), %rdx
	xorl	%eax, %eax
	cmpl	12(%rdx), %ecx
	jl	.LBB3_2
.LBB3_11:                               # %.critedge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN8NArchive3N7z16CFolderOutStream17ProcessEmptyFilesEv, .Lfunc_end3-_ZN8NArchive3N7z16CFolderOutStream17ProcessEmptyFilesEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z16CFolderOutStream8OpenFileEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CFolderOutStream8OpenFileEv,@function
_ZN8NArchive3N7z16CFolderOutStream8OpenFileEv: # @_ZN8NArchive3N7z16CFolderOutStream8OpenFileEv
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 64
.Lcfi26:
	.cfi_offset %rbx, -48
.Lcfi27:
	.cfi_offset %r12, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	48(%rbx), %rcx
	movslq	72(%rbx), %rax
	movq	16(%rcx), %rcx
	cmpb	$0, (%rcx,%rax)
	je	.LBB4_1
# BB#2:
	movzbl	76(%rbx), %r14d
	jmp	.LBB4_3
.LBB4_1:
	movl	$2, %r14d
.LBB4_3:
	movq	$0, 8(%rsp)
	movl	68(%rbx), %r15d
	addl	%eax, %r15d
	movq	56(%rbx), %rdi
	movq	(%rdi), %rax
	movl	64(%rbx), %esi
	addl	%r15d, %esi
.Ltmp12:
	leaq	8(%rsp), %rdx
	movl	%r14d, %ecx
	callq	*56(%rax)
	movl	%eax, %ebp
.Ltmp13:
# BB#4:
	testl	%ebp, %ebp
	jne	.LBB4_15
# BB#5:
	movq	24(%rbx), %r12
	movq	8(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB4_7
# BB#6:
	movq	(%rbp), %rax
.Ltmp14:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp15:
.LBB4_7:                                # %.noexc31
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB4_9
# BB#8:
	movq	(%rdi), %rax
.Ltmp16:
	callq	*16(%rax)
.Ltmp17:
.LBB4_9:
	movq	%rbp, 16(%r12)
	movq	24(%rbx), %rax
	movb	77(%rbx), %cl
	movq	$0, 24(%rax)
	movb	%cl, 36(%rax)
	movl	$-1, 32(%rax)
	movb	$1, 78(%rbx)
	movq	40(%rbx), %rcx
	movq	176(%rcx), %rax
	movslq	%r15d, %rdx
	movq	(%rax,%rdx,8), %rax
	movq	(%rax), %rsi
	movq	%rsi, 80(%rbx)
	testl	%r14d, %r14d
	jne	.LBB4_14
# BB#10:
	xorl	%r14d, %r14d
	cmpq	$0, 8(%rsp)
	jne	.LBB4_14
# BB#11:
	cmpl	%r15d, 460(%rcx)
	jle	.LBB4_13
# BB#12:                                # %_ZNK8NArchive3N7z16CArchiveDatabase10IsItemAntiEi.exit
	movq	464(%rcx), %rcx
	cmpb	$0, (%rcx,%rdx)
	jne	.LBB4_14
.LBB4_13:                               # %_ZNK8NArchive3N7z16CArchiveDatabase10IsItemAntiEi.exit.thread
	xorl	%r14d, %r14d
	cmpb	$0, 33(%rax)
	sete	%r14b
	addl	%r14d, %r14d
.LBB4_14:
	movq	56(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp19:
	movl	%r14d, %esi
	callq	*64(%rax)
	movl	%eax, %ebp
.Ltmp20:
.LBB4_15:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_17
# BB#16:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB4_17:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit30
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_23:
.Ltmp21:
	jmp	.LBB4_19
.LBB4_18:
.Ltmp18:
.LBB4_19:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_21
# BB#20:
	movq	(%rdi), %rax
.Ltmp22:
	callq	*16(%rax)
.Ltmp23:
.LBB4_21:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB4_22:
.Ltmp24:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN8NArchive3N7z16CFolderOutStream8OpenFileEv, .Lfunc_end4-_ZN8NArchive3N7z16CFolderOutStream8OpenFileEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp17-.Ltmp12         #   Call between .Ltmp12 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin1   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin1   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp22-.Ltmp20         #   Call between .Ltmp20 and .Ltmp22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin1   #     jumps to .Ltmp24
	.byte	1                       #   On action: 1
	.long	.Ltmp23-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end4-.Ltmp23     #   Call between .Ltmp23 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3N7z16CFolderOutStream21CloseFileAndSetResultEi
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CFolderOutStream21CloseFileAndSetResultEi,@function
_ZN8NArchive3N7z16CFolderOutStream21CloseFileAndSetResultEi: # @_ZN8NArchive3N7z16CFolderOutStream21CloseFileAndSetResultEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	24(%rbx), %rbp
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 16(%rbp)
.LBB5_2:                                # %_ZN17COutStreamWithCRC13ReleaseStreamEv.exit
	movb	$0, 78(%rbx)
	incl	72(%rbx)
	movq	56(%rbx), %rdi
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	movl	%r14d, %esi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.Lfunc_end5:
	.size	_ZN8NArchive3N7z16CFolderOutStream21CloseFileAndSetResultEi, .Lfunc_end5-_ZN8NArchive3N7z16CFolderOutStream21CloseFileAndSetResultEi
	.cfi_endproc

	.globl	_ZN8NArchive3N7z16CFolderOutStream21CloseFileAndSetResultEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CFolderOutStream21CloseFileAndSetResultEv,@function
_ZN8NArchive3N7z16CFolderOutStream21CloseFileAndSetResultEv: # @_ZN8NArchive3N7z16CFolderOutStream21CloseFileAndSetResultEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 32
.Lcfi40:
	.cfi_offset %rbx, -32
.Lcfi41:
	.cfi_offset %r14, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	40(%rbx), %rcx
	movl	72(%rbx), %eax
	movl	68(%rbx), %edx
	addl	%eax, %edx
	movq	176(%rcx), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rcx
	xorl	%r14d, %r14d
	cmpb	$0, 33(%rcx)
	jne	.LBB6_4
# BB#1:
	cmpb	$0, 34(%rcx)
	je	.LBB6_4
# BB#2:
	cmpb	$0, 77(%rbx)
	je	.LBB6_4
# BB#3:
	movq	24(%rbx), %rdx
	movl	32(%rdx), %edx
	notl	%edx
	xorl	%esi, %esi
	cmpl	%edx, 12(%rcx)
	setne	%sil
	leal	(%rsi,%rsi,2), %r14d
.LBB6_4:
	movq	24(%rbx), %rbp
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_6
# BB#5:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 16(%rbp)
	movl	72(%rbx), %eax
.LBB6_6:                                # %_ZN8NArchive3N7z16CFolderOutStream21CloseFileAndSetResultEi.exit
	movb	$0, 78(%rbx)
	incl	%eax
	movl	%eax, 72(%rbx)
	movq	56(%rbx), %rdi
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	movl	%r14d, %esi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.Lfunc_end6:
	.size	_ZN8NArchive3N7z16CFolderOutStream21CloseFileAndSetResultEv, .Lfunc_end6-_ZN8NArchive3N7z16CFolderOutStream21CloseFileAndSetResultEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z16CFolderOutStream5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CFolderOutStream5WriteEPKvjPj,@function
_ZN8NArchive3N7z16CFolderOutStream5WriteEPKvjPj: # @_ZN8NArchive3N7z16CFolderOutStream5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi46:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi49:
	.cfi_def_cfa_offset 80
.Lcfi50:
	.cfi_offset %rbx, -56
.Lcfi51:
	.cfi_offset %r12, -48
.Lcfi52:
	.cfi_offset %r13, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movl	%edx, %ebp
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.LBB7_2
# BB#1:
	movl	$0, (%r12)
.LBB7_2:                                # %.preheader
	xorl	%r14d, %r14d
	testl	%ebp, %ebp
	je	.LBB7_32
# BB#3:                                 # %.outer..outer.split_crit_edge.lr.ph
                                        # implicit-def: %R15D
	jmp	.LBB7_8
.LBB7_4:                                # %.outer.backedge
                                        #   in Loop: Header=BB7_8 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	addq	%rcx, %rsi
	subl	%ecx, %ebp
	jne	.LBB7_8
	jmp	.LBB7_32
.LBB7_5:                                #   in Loop: Header=BB7_8 Depth=1
	cmpb	$0, 34(%rcx)
	je	.LBB7_20
# BB#6:                                 #   in Loop: Header=BB7_8 Depth=1
	cmpb	$0, 77(%rbx)
	je	.LBB7_20
# BB#7:                                 #   in Loop: Header=BB7_8 Depth=1
	movq	24(%rbx), %rdx
	movl	32(%rdx), %edx
	notl	%edx
	xorl	%esi, %esi
	cmpl	%edx, 12(%rcx)
	setne	%sil
	leal	(%rsi,%rsi,2), %esi
	jmp	.LBB7_20
	.p2align	4, 0x90
.LBB7_8:                                # %.outer..outer.split_crit_edge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_9 Depth 2
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movl	%r15d, %r13d
	.p2align	4, 0x90
.LBB7_9:                                #   Parent Loop BB7_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, 78(%rbx)
	jne	.LBB7_13
# BB#10:                                #   in Loop: Header=BB7_9 Depth=2
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z16CFolderOutStream17ProcessEmptyFilesEv
	testl	%eax, %eax
	cmovnel	%eax, %r13d
	jne	.LBB7_31
# BB#11:                                #   in Loop: Header=BB7_9 Depth=2
	movl	72(%rbx), %eax
	movq	48(%rbx), %rcx
	cmpl	12(%rcx), %eax
	je	.LBB7_29
# BB#12:                                #   in Loop: Header=BB7_9 Depth=2
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z16CFolderOutStream8OpenFileEv
	testl	%eax, %eax
	je	.LBB7_9
	jmp	.LBB7_31
	.p2align	4, 0x90
.LBB7_13:                               #   in Loop: Header=BB7_8 Depth=1
	movl	%ebp, %eax
	movq	80(%rbx), %rdx
	cmpq	%rdx, %rax
	cmovbl	%ebp, %edx
	movl	%edx, (%rsp)
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	movq	8(%rsp), %rsi           # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%rsp, %rcx
	callq	*40(%rax)
	testl	%eax, %eax
	movl	%eax, %r15d
	cmovel	%r13d, %r15d
	jne	.LBB7_31
# BB#14:                                #   in Loop: Header=BB7_8 Depth=1
	movl	(%rsp), %ecx
	testq	%rcx, %rcx
	je	.LBB7_32
# BB#15:                                #   in Loop: Header=BB7_8 Depth=1
	movq	80(%rbx), %rax
	subq	%rcx, %rax
	movq	%rax, 80(%rbx)
	testq	%r12, %r12
	je	.LBB7_17
# BB#16:                                #   in Loop: Header=BB7_8 Depth=1
	addl	%ecx, (%r12)
.LBB7_17:                               #   in Loop: Header=BB7_8 Depth=1
	testq	%rax, %rax
	je	.LBB7_19
# BB#18:                                # %.thread
                                        #   in Loop: Header=BB7_8 Depth=1
	movl	%r13d, %r15d
	jmp	.LBB7_4
.LBB7_19:                               #   in Loop: Header=BB7_8 Depth=1
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	40(%rbx), %rcx
	movl	72(%rbx), %eax
	movl	68(%rbx), %edx
	addl	%eax, %edx
	movq	176(%rcx), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rcx
	xorl	%esi, %esi
	cmpb	$0, 33(%rcx)
	je	.LBB7_5
.LBB7_20:                               #   in Loop: Header=BB7_8 Depth=1
	movq	24(%rbx), %r13
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB7_22
# BB#21:                                #   in Loop: Header=BB7_8 Depth=1
	movq	(%rdi), %rax
	movl	%esi, 4(%rsp)           # 4-byte Spill
	callq	*16(%rax)
	movl	4(%rsp), %esi           # 4-byte Reload
	movq	$0, 16(%r13)
	movl	72(%rbx), %eax
.LBB7_22:                               # %_ZN8NArchive3N7z16CFolderOutStream21CloseFileAndSetResultEv.exit
                                        #   in Loop: Header=BB7_8 Depth=1
	movb	$0, 78(%rbx)
	incl	%eax
	movl	%eax, 72(%rbx)
	movq	56(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*72(%rax)
	testl	%eax, %eax
	jne	.LBB7_31
# BB#23:                                #   in Loop: Header=BB7_8 Depth=1
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z16CFolderOutStream17ProcessEmptyFilesEv
	testl	%eax, %eax
	setne	%cl
	cmovnel	%eax, %r15d
	movb	$2, %al
	je	.LBB7_25
# BB#24:                                #   in Loop: Header=BB7_8 Depth=1
	movl	%ecx, %eax
.LBB7_25:                               #   in Loop: Header=BB7_8 Depth=1
	cmpb	$1, %al
	je	.LBB7_33
# BB#26:                                #   in Loop: Header=BB7_8 Depth=1
	cmpb	$3, %al
	movq	16(%rsp), %rcx          # 8-byte Reload
	jne	.LBB7_4
# BB#27:
	xorl	%r14d, %r14d
	jmp	.LBB7_32
.LBB7_31:
	movl	%eax, %r14d
	jmp	.LBB7_32
.LBB7_29:
	testq	%r12, %r12
	je	.LBB7_32
# BB#30:
	addl	%ebp, (%r12)
.LBB7_32:                               # %.loopexit
	movl	%r14d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_33:                               # %.loopexit.loopexit113
	movl	%r15d, %r14d
	jmp	.LBB7_32
.Lfunc_end7:
	.size	_ZN8NArchive3N7z16CFolderOutStream5WriteEPKvjPj, .Lfunc_end7-_ZN8NArchive3N7z16CFolderOutStream5WriteEPKvjPj
	.cfi_endproc

	.globl	_ZN8NArchive3N7z16CFolderOutStream16GetSubStreamSizeEyPy
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CFolderOutStream16GetSubStreamSizeEyPy,@function
_ZN8NArchive3N7z16CFolderOutStream16GetSubStreamSizeEyPy: # @_ZN8NArchive3N7z16CFolderOutStream16GetSubStreamSizeEyPy
	.cfi_startproc
# BB#0:
	movq	$0, (%rdx)
	movq	48(%rdi), %rcx
	movl	$1, %eax
	cmpl	12(%rcx), %esi
	jge	.LBB8_2
# BB#1:
	movq	40(%rdi), %rax
	addl	68(%rdi), %esi
	movq	176(%rax), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	xorl	%eax, %eax
.LBB8_2:
	retq
.Lfunc_end8:
	.size	_ZN8NArchive3N7z16CFolderOutStream16GetSubStreamSizeEyPy, .Lfunc_end8-_ZN8NArchive3N7z16CFolderOutStream16GetSubStreamSizeEyPy
	.cfi_endproc

	.globl	_ZThn8_N8NArchive3N7z16CFolderOutStream16GetSubStreamSizeEyPy
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3N7z16CFolderOutStream16GetSubStreamSizeEyPy,@function
_ZThn8_N8NArchive3N7z16CFolderOutStream16GetSubStreamSizeEyPy: # @_ZThn8_N8NArchive3N7z16CFolderOutStream16GetSubStreamSizeEyPy
	.cfi_startproc
# BB#0:
	movq	$0, (%rdx)
	movq	40(%rdi), %rcx
	movl	$1, %eax
	cmpl	12(%rcx), %esi
	jge	.LBB9_2
# BB#1:
	movq	32(%rdi), %rax
	addl	60(%rdi), %esi
	movq	176(%rax), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	xorl	%eax, %eax
.LBB9_2:                                # %_ZN8NArchive3N7z16CFolderOutStream16GetSubStreamSizeEyPy.exit
	retq
.Lfunc_end9:
	.size	_ZThn8_N8NArchive3N7z16CFolderOutStream16GetSubStreamSizeEyPy, .Lfunc_end9-_ZThn8_N8NArchive3N7z16CFolderOutStream16GetSubStreamSizeEyPy
	.cfi_endproc

	.globl	_ZN8NArchive3N7z16CFolderOutStream14FlushCorruptedEi
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CFolderOutStream14FlushCorruptedEi,@function
_ZN8NArchive3N7z16CFolderOutStream14FlushCorruptedEi: # @_ZN8NArchive3N7z16CFolderOutStream14FlushCorruptedEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 48
.Lcfi61:
	.cfi_offset %rbx, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movl	72(%rbx), %eax
	movq	48(%rbx), %rcx
	xorl	%r14d, %r14d
	cmpl	12(%rcx), %eax
	jl	.LBB10_2
	jmp	.LBB10_10
	.p2align	4, 0x90
.LBB10_7:                               # %.backedge
                                        #   in Loop: Header=BB10_2 Depth=1
	movl	72(%rbx), %eax
	movq	48(%rbx), %rcx
	cmpl	12(%rcx), %eax
	jge	.LBB10_10
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	cmpb	$0, 78(%rbx)
	je	.LBB10_8
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	24(%rbx), %rbp
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB10_5
# BB#4:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 16(%rbp)
	movl	72(%rbx), %eax
.LBB10_5:                               # %_ZN8NArchive3N7z16CFolderOutStream21CloseFileAndSetResultEi.exit
                                        #   in Loop: Header=BB10_2 Depth=1
	movb	$0, 78(%rbx)
	incl	%eax
	movl	%eax, 72(%rbx)
	movq	56(%rbx), %rdi
	movq	(%rdi), %rax
	movl	%r15d, %esi
	callq	*72(%rax)
	testl	%eax, %eax
	je	.LBB10_7
	jmp	.LBB10_9
	.p2align	4, 0x90
.LBB10_8:                               #   in Loop: Header=BB10_2 Depth=1
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z16CFolderOutStream8OpenFileEv
	testl	%eax, %eax
	je	.LBB10_7
.LBB10_9:
	movl	%eax, %r14d
.LBB10_10:                              # %._crit_edge
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZN8NArchive3N7z16CFolderOutStream14FlushCorruptedEi, .Lfunc_end10-_ZN8NArchive3N7z16CFolderOutStream14FlushCorruptedEi
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 32
.Lcfi68:
	.cfi_offset %rbx, -32
.Lcfi69:
	.cfi_offset %r14, -24
.Lcfi70:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB11_3
# BB#1:
	movl	$IID_ICompressGetSubStreamSize, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB11_2
.LBB11_3:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB11_4:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB11_2:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB11_4
.Lfunc_end11:
	.size	_ZN8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end11-_ZN8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z16CFolderOutStream6AddRefEv,"axG",@progbits,_ZN8NArchive3N7z16CFolderOutStream6AddRefEv,comdat
	.weak	_ZN8NArchive3N7z16CFolderOutStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CFolderOutStream6AddRefEv,@function
_ZN8NArchive3N7z16CFolderOutStream6AddRefEv: # @_ZN8NArchive3N7z16CFolderOutStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end12:
	.size	_ZN8NArchive3N7z16CFolderOutStream6AddRefEv, .Lfunc_end12-_ZN8NArchive3N7z16CFolderOutStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z16CFolderOutStream7ReleaseEv,"axG",@progbits,_ZN8NArchive3N7z16CFolderOutStream7ReleaseEv,comdat
	.weak	_ZN8NArchive3N7z16CFolderOutStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CFolderOutStream7ReleaseEv,@function
_ZN8NArchive3N7z16CFolderOutStream7ReleaseEv: # @_ZN8NArchive3N7z16CFolderOutStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi71:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB13_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB13_2:
	popq	%rcx
	retq
.Lfunc_end13:
	.size	_ZN8NArchive3N7z16CFolderOutStream7ReleaseEv, .Lfunc_end13-_ZN8NArchive3N7z16CFolderOutStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z16CFolderOutStreamD2Ev,"axG",@progbits,_ZN8NArchive3N7z16CFolderOutStreamD2Ev,comdat
	.weak	_ZN8NArchive3N7z16CFolderOutStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CFolderOutStreamD2Ev,@function
_ZN8NArchive3N7z16CFolderOutStreamD2Ev: # @_ZN8NArchive3N7z16CFolderOutStreamD2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi74:
	.cfi_def_cfa_offset 32
.Lcfi75:
	.cfi_offset %rbx, -24
.Lcfi76:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive3N7z16CFolderOutStreamE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive3N7z16CFolderOutStreamE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp25:
	callq	*16(%rax)
.Ltmp26:
.LBB14_2:                               # %_ZN9CMyComPtrI23IArchiveExtractCallbackED2Ev.exit
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp31:
	callq	*16(%rax)
.Ltmp32:
.LBB14_4:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB14_7:
.Ltmp33:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB14_5:
.Ltmp27:
	movq	%rax, %r14
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp28:
	callq	*16(%rax)
.Ltmp29:
.LBB14_8:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit6
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB14_9:
.Ltmp30:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end14:
	.size	_ZN8NArchive3N7z16CFolderOutStreamD2Ev, .Lfunc_end14-_ZN8NArchive3N7z16CFolderOutStreamD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp25-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin2   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin2   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp28-.Ltmp32         #   Call between .Ltmp32 and .Ltmp28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin2   #     jumps to .Ltmp30
	.byte	1                       #   On action: 1
	.long	.Ltmp29-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Lfunc_end14-.Ltmp29    #   Call between .Ltmp29 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z16CFolderOutStreamD0Ev,"axG",@progbits,_ZN8NArchive3N7z16CFolderOutStreamD0Ev,comdat
	.weak	_ZN8NArchive3N7z16CFolderOutStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CFolderOutStreamD0Ev,@function
_ZN8NArchive3N7z16CFolderOutStreamD0Ev: # @_ZN8NArchive3N7z16CFolderOutStreamD0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi79:
	.cfi_def_cfa_offset 32
.Lcfi80:
	.cfi_offset %rbx, -24
.Lcfi81:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive3N7z16CFolderOutStreamE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive3N7z16CFolderOutStreamE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp34:
	callq	*16(%rax)
.Ltmp35:
.LBB15_2:                               # %_ZN9CMyComPtrI23IArchiveExtractCallbackED2Ev.exit.i
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp40:
	callq	*16(%rax)
.Ltmp41:
.LBB15_4:                               # %_ZN8NArchive3N7z16CFolderOutStreamD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB15_8:
.Ltmp42:
	movq	%rax, %r14
	jmp	.LBB15_9
.LBB15_5:
.Ltmp36:
	movq	%rax, %r14
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_9
# BB#6:
	movq	(%rdi), %rax
.Ltmp37:
	callq	*16(%rax)
.Ltmp38:
.LBB15_9:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB15_7:
.Ltmp39:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZN8NArchive3N7z16CFolderOutStreamD0Ev, .Lfunc_end15-_ZN8NArchive3N7z16CFolderOutStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp34-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin3   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin3   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin3   #     jumps to .Ltmp39
	.byte	1                       #   On action: 1
	.long	.Ltmp38-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Lfunc_end15-.Ltmp38    #   Call between .Ltmp38 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZThn8_N8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi82:
	.cfi_def_cfa_offset 16
	addq	$-8, %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB16_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB16_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB16_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB16_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB16_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB16_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB16_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB16_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB16_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB16_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB16_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB16_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB16_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB16_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB16_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB16_32
.LBB16_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressGetSubStreamSize(%rip), %cl
	jne	.LBB16_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+1(%rip), %cl
	jne	.LBB16_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+2(%rip), %cl
	jne	.LBB16_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+3(%rip), %cl
	jne	.LBB16_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+4(%rip), %cl
	jne	.LBB16_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+5(%rip), %cl
	jne	.LBB16_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+6(%rip), %cl
	jne	.LBB16_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+7(%rip), %cl
	jne	.LBB16_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+8(%rip), %cl
	jne	.LBB16_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+9(%rip), %cl
	jne	.LBB16_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+10(%rip), %cl
	jne	.LBB16_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+11(%rip), %cl
	jne	.LBB16_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+12(%rip), %cl
	jne	.LBB16_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+13(%rip), %cl
	jne	.LBB16_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+14(%rip), %cl
	jne	.LBB16_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_ICompressGetSubStreamSize+15(%rip), %cl
	jne	.LBB16_33
.LBB16_32:
	leaq	8(%rdi), %rax
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB16_33:                              # %_ZN8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end16:
	.size	_ZThn8_N8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end16-_ZThn8_N8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive3N7z16CFolderOutStream6AddRefEv,"axG",@progbits,_ZThn8_N8NArchive3N7z16CFolderOutStream6AddRefEv,comdat
	.weak	_ZThn8_N8NArchive3N7z16CFolderOutStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3N7z16CFolderOutStream6AddRefEv,@function
_ZThn8_N8NArchive3N7z16CFolderOutStream6AddRefEv: # @_ZThn8_N8NArchive3N7z16CFolderOutStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end17:
	.size	_ZThn8_N8NArchive3N7z16CFolderOutStream6AddRefEv, .Lfunc_end17-_ZThn8_N8NArchive3N7z16CFolderOutStream6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive3N7z16CFolderOutStream7ReleaseEv,"axG",@progbits,_ZThn8_N8NArchive3N7z16CFolderOutStream7ReleaseEv,comdat
	.weak	_ZThn8_N8NArchive3N7z16CFolderOutStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3N7z16CFolderOutStream7ReleaseEv,@function
_ZThn8_N8NArchive3N7z16CFolderOutStream7ReleaseEv: # @_ZThn8_N8NArchive3N7z16CFolderOutStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi83:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB18_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB18_2:                               # %_ZN8NArchive3N7z16CFolderOutStream7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end18:
	.size	_ZThn8_N8NArchive3N7z16CFolderOutStream7ReleaseEv, .Lfunc_end18-_ZThn8_N8NArchive3N7z16CFolderOutStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive3N7z16CFolderOutStreamD1Ev,"axG",@progbits,_ZThn8_N8NArchive3N7z16CFolderOutStreamD1Ev,comdat
	.weak	_ZThn8_N8NArchive3N7z16CFolderOutStreamD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3N7z16CFolderOutStreamD1Ev,@function
_ZThn8_N8NArchive3N7z16CFolderOutStreamD1Ev: # @_ZThn8_N8NArchive3N7z16CFolderOutStreamD1Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi86:
	.cfi_def_cfa_offset 32
.Lcfi87:
	.cfi_offset %rbx, -24
.Lcfi88:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive3N7z16CFolderOutStreamE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive3N7z16CFolderOutStreamE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rbx)
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB19_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp43:
	callq	*16(%rax)
.Ltmp44:
.LBB19_2:                               # %_ZN9CMyComPtrI23IArchiveExtractCallbackED2Ev.exit.i
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB19_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp49:
	callq	*16(%rax)
.Ltmp50:
.LBB19_4:                               # %_ZN8NArchive3N7z16CFolderOutStreamD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB19_7:
.Ltmp51:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB19_5:
.Ltmp45:
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB19_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp46:
	callq	*16(%rax)
.Ltmp47:
.LBB19_8:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit6.i
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB19_9:
.Ltmp48:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end19:
	.size	_ZThn8_N8NArchive3N7z16CFolderOutStreamD1Ev, .Lfunc_end19-_ZThn8_N8NArchive3N7z16CFolderOutStreamD1Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp43-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin4   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin4   #     jumps to .Ltmp51
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp46-.Ltmp50         #   Call between .Ltmp50 and .Ltmp46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin4   #     jumps to .Ltmp48
	.byte	1                       #   On action: 1
	.long	.Ltmp47-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Lfunc_end19-.Ltmp47    #   Call between .Ltmp47 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZThn8_N8NArchive3N7z16CFolderOutStreamD0Ev,"axG",@progbits,_ZThn8_N8NArchive3N7z16CFolderOutStreamD0Ev,comdat
	.weak	_ZThn8_N8NArchive3N7z16CFolderOutStreamD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive3N7z16CFolderOutStreamD0Ev,@function
_ZThn8_N8NArchive3N7z16CFolderOutStreamD0Ev: # @_ZThn8_N8NArchive3N7z16CFolderOutStreamD0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi91:
	.cfi_def_cfa_offset 32
.Lcfi92:
	.cfi_offset %rbx, -24
.Lcfi93:
	.cfi_offset %r14, -16
	movq	%rdi, %rax
	movl	$_ZTVN8NArchive3N7z16CFolderOutStreamE+88, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN8NArchive3N7z16CFolderOutStreamE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rax)
	movq	48(%rax), %rdi
	leaq	-8(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB20_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp52:
	callq	*16(%rax)
.Ltmp53:
.LBB20_2:                               # %_ZN9CMyComPtrI23IArchiveExtractCallbackED2Ev.exit.i.i
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB20_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp58:
	callq	*16(%rax)
.Ltmp59:
.LBB20_4:                               # %_ZN8NArchive3N7z16CFolderOutStreamD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB20_8:
.Ltmp60:
	movq	%rax, %r14
	jmp	.LBB20_9
.LBB20_5:
.Ltmp54:
	movq	%rax, %r14
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB20_9
# BB#6:
	movq	(%rdi), %rax
.Ltmp55:
	callq	*16(%rax)
.Ltmp56:
.LBB20_9:                               # %.body.i
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB20_7:
.Ltmp57:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end20:
	.size	_ZThn8_N8NArchive3N7z16CFolderOutStreamD0Ev, .Lfunc_end20-_ZThn8_N8NArchive3N7z16CFolderOutStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp52-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin5   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin5   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin5   #     jumps to .Ltmp57
	.byte	1                       #   On action: 1
	.long	.Ltmp56-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Lfunc_end20-.Ltmp56    #   Call between .Ltmp56 and .Lfunc_end20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB21_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB21_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB21_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB21_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB21_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB21_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB21_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB21_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB21_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB21_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB21_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB21_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB21_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB21_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB21_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB21_16:
	xorl	%eax, %eax
	retq
.Lfunc_end21:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end21-_ZeqRK4GUIDS1_
	.cfi_endproc

	.type	_ZTVN8NArchive3N7z16CFolderOutStreamE,@object # @_ZTVN8NArchive3N7z16CFolderOutStreamE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN8NArchive3N7z16CFolderOutStreamE
	.p2align	3
_ZTVN8NArchive3N7z16CFolderOutStreamE:
	.quad	0
	.quad	_ZTIN8NArchive3N7z16CFolderOutStreamE
	.quad	_ZN8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive3N7z16CFolderOutStream6AddRefEv
	.quad	_ZN8NArchive3N7z16CFolderOutStream7ReleaseEv
	.quad	_ZN8NArchive3N7z16CFolderOutStreamD2Ev
	.quad	_ZN8NArchive3N7z16CFolderOutStreamD0Ev
	.quad	_ZN8NArchive3N7z16CFolderOutStream5WriteEPKvjPj
	.quad	_ZN8NArchive3N7z16CFolderOutStream16GetSubStreamSizeEyPy
	.quad	-8
	.quad	_ZTIN8NArchive3N7z16CFolderOutStreamE
	.quad	_ZThn8_N8NArchive3N7z16CFolderOutStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N8NArchive3N7z16CFolderOutStream6AddRefEv
	.quad	_ZThn8_N8NArchive3N7z16CFolderOutStream7ReleaseEv
	.quad	_ZThn8_N8NArchive3N7z16CFolderOutStreamD1Ev
	.quad	_ZThn8_N8NArchive3N7z16CFolderOutStreamD0Ev
	.quad	_ZThn8_N8NArchive3N7z16CFolderOutStream16GetSubStreamSizeEyPy
	.size	_ZTVN8NArchive3N7z16CFolderOutStreamE, 136

	.type	_ZTSN8NArchive3N7z16CFolderOutStreamE,@object # @_ZTSN8NArchive3N7z16CFolderOutStreamE
	.globl	_ZTSN8NArchive3N7z16CFolderOutStreamE
	.p2align	4
_ZTSN8NArchive3N7z16CFolderOutStreamE:
	.asciz	"N8NArchive3N7z16CFolderOutStreamE"
	.size	_ZTSN8NArchive3N7z16CFolderOutStreamE, 34

	.type	_ZTS20ISequentialOutStream,@object # @_ZTS20ISequentialOutStream
	.section	.rodata._ZTS20ISequentialOutStream,"aG",@progbits,_ZTS20ISequentialOutStream,comdat
	.weak	_ZTS20ISequentialOutStream
	.p2align	4
_ZTS20ISequentialOutStream:
	.asciz	"20ISequentialOutStream"
	.size	_ZTS20ISequentialOutStream, 23

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI20ISequentialOutStream,@object # @_ZTI20ISequentialOutStream
	.section	.rodata._ZTI20ISequentialOutStream,"aG",@progbits,_ZTI20ISequentialOutStream,comdat
	.weak	_ZTI20ISequentialOutStream
	.p2align	4
_ZTI20ISequentialOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ISequentialOutStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ISequentialOutStream, 24

	.type	_ZTS25ICompressGetSubStreamSize,@object # @_ZTS25ICompressGetSubStreamSize
	.section	.rodata._ZTS25ICompressGetSubStreamSize,"aG",@progbits,_ZTS25ICompressGetSubStreamSize,comdat
	.weak	_ZTS25ICompressGetSubStreamSize
	.p2align	4
_ZTS25ICompressGetSubStreamSize:
	.asciz	"25ICompressGetSubStreamSize"
	.size	_ZTS25ICompressGetSubStreamSize, 28

	.type	_ZTI25ICompressGetSubStreamSize,@object # @_ZTI25ICompressGetSubStreamSize
	.section	.rodata._ZTI25ICompressGetSubStreamSize,"aG",@progbits,_ZTI25ICompressGetSubStreamSize,comdat
	.weak	_ZTI25ICompressGetSubStreamSize
	.p2align	4
_ZTI25ICompressGetSubStreamSize:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS25ICompressGetSubStreamSize
	.quad	_ZTI8IUnknown
	.size	_ZTI25ICompressGetSubStreamSize, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN8NArchive3N7z16CFolderOutStreamE,@object # @_ZTIN8NArchive3N7z16CFolderOutStreamE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive3N7z16CFolderOutStreamE
	.p2align	4
_ZTIN8NArchive3N7z16CFolderOutStreamE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive3N7z16CFolderOutStreamE
	.long	1                       # 0x1
	.long	3                       # 0x3
	.quad	_ZTI20ISequentialOutStream
	.quad	2                       # 0x2
	.quad	_ZTI25ICompressGetSubStreamSize
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.size	_ZTIN8NArchive3N7z16CFolderOutStreamE, 72


	.globl	_ZN8NArchive3N7z16CFolderOutStreamC1Ev
	.type	_ZN8NArchive3N7z16CFolderOutStreamC1Ev,@function
_ZN8NArchive3N7z16CFolderOutStreamC1Ev = _ZN8NArchive3N7z16CFolderOutStreamC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
