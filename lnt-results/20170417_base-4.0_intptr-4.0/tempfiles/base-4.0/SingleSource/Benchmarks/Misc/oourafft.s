	.text
	.file	"oourafft.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182418800017408     # double 1
.LCPI0_2:
	.quad	4569220660278340888     # double 0.0030679615757712823
.LCPI0_3:
	.quad	4526168777503116145     # double 3.8580246913580248E-6
.LCPI0_4:
	.quad	-4661225614328463360    # double -9.765625E-4
.LCPI0_6:
	.quad	4457293557087583675     # double 1.0E-10
.LCPI0_8:
	.quad	4472406533629990549     # double 1.0000000000000001E-9
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_1:
	.quad	4604544271217802189     # double 0.70710678118654757
	.quad	4604544271217802189     # double 0.70710678118654757
.LCPI0_5:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
.LCPI0_7:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	leaq	24(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	leaq	24(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	movl	$16, %edi
	movl	$128, %esi
	callq	memalign
	movq	%rax, %r14
	movl	$16, %edi
	movl	$20480, %esi            # imm = 0x5000
	callq	memalign
	movq	%rax, %r15
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	atan
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movups	%xmm0, (%r15)
	movapd	.LCPI0_1(%rip), %xmm0   # xmm0 = [7.071068e-01,7.071068e-01]
	movupd	%xmm0, 2048(%r15)
	movq	%r15, %r12
	addq	$4088, %r12             # imm = 0xFF8
	movl	$2, %ebx
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph.i.1
                                        #   in Loop: Header=BB0_1 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	mulsd	.LCPI0_2(%rip), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	cos
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	%xmm1, 16(%r15,%rbx,8)
	movsd	%xmm0, 24(%r15,%rbx,8)
	movsd	%xmm0, -24(%r12)
	movsd	%xmm1, -16(%r12)
	addq	$-32, %r12
	addq	$2, %rbp
	movq	%rbp, %rbx
.LBB0_1:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	mulsd	.LCPI0_2(%rip), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	cos
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	%xmm1, (%r15,%rbx,8)
	movsd	%xmm0, 8(%r15,%rbx,8)
	movsd	%xmm0, -8(%r12)
	movsd	%xmm1, (%r12)
	leaq	2(%rbx), %rbp
	cmpq	$256, %rbp              # imm = 0x100
	jl	.LBB0_20
# BB#2:                                 # %makewt.exit
	movl	$512, %edi              # imm = 0x200
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	bitrv2
	movl	$16, %edi
	movl	$16384, %esi            # imm = 0x4000
	callq	memalign
	movq	%rax, %r13
	movl	$16, %edi
	movl	$16384, %esi            # imm = 0x4000
	callq	memalign
	movq	%rax, %rbx
	movl	$16, %edi
	movl	$16384, %esi            # imm = 0x4000
	callq	memalign
	movq	%rax, %r12
	xorl	%eax, %eax
	movsd	.LCPI0_3(%rip), %xmm1   # xmm1 = mem[0],zero
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph.i114
                                        # =>This Inner Loop Header: Depth=1
	imull	$7141, %ecx, %ecx       # imm = 0x1BE5
	leal	54773(%rcx), %edx
	movslq	%edx, %rdx
	imulq	$-2123092475, %rdx, %rsi # imm = 0x81742E05
	shrq	$32, %rsi
	leal	54773(%rsi,%rcx), %ecx
	movl	%ecx, %esi
	shrl	$31, %esi
	sarl	$17, %ecx
	addl	%esi, %ecx
	imull	$259200, %ecx, %ecx     # imm = 0x3F480
	subl	%ecx, %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%r13,%rax,8)
	imull	$7141, %edx, %edx       # imm = 0x1BE5
	leal	54773(%rdx), %ecx
	movslq	%ecx, %rcx
	imulq	$-2123092475, %rcx, %rsi # imm = 0x81742E05
	shrq	$32, %rsi
	leal	54773(%rsi,%rdx), %edx
	movl	%edx, %esi
	shrl	$31, %esi
	sarl	$17, %edx
	addl	%esi, %edx
	imull	$259200, %edx, %edx     # imm = 0x3F480
	subl	%edx, %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 8(%r13,%rax,8)
	addq	$2, %rax
	cmpq	$2048, %rax             # imm = 0x800
	jne	.LBB0_3
# BB#4:                                 # %putdata.exit
	movl	$2048, %edi             # imm = 0x800
	movq	%r14, %rsi
	movq	%r13, %rdx
	callq	bitrv2
	movl	$2048, %edi             # imm = 0x800
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	cftfsub
	movl	$2048, %edi             # imm = 0x800
	movl	$-1, %esi
	movq	%r13, %rdx
	movq	%r14, %rcx
	movq	%r15, %r8
	callq	cdft
	movsd	.LCPI0_3(%rip), %xmm5   # xmm5 = mem[0],zero
	xorl	%eax, %eax
	xorpd	%xmm0, %xmm0
	movsd	.LCPI0_4(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	.LCPI0_5(%rip), %xmm2   # xmm2 = [nan,nan]
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph.i117
                                        # =>This Inner Loop Header: Depth=1
	imull	$7141, %ecx, %edx       # imm = 0x1BE5
	leal	54773(%rdx), %ecx
	movslq	%ecx, %rcx
	imulq	$-2123092475, %rcx, %rsi # imm = 0x81742E05
	shrq	$32, %rsi
	leal	54773(%rsi,%rdx), %edx
	movl	%edx, %esi
	shrl	$31, %esi
	sarl	$17, %edx
	addl	%esi, %edx
	imull	$259200, %edx, %edx     # imm = 0x3F480
	subl	%edx, %ecx
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%ecx, %xmm3
	mulsd	%xmm5, %xmm3
	movsd	(%r13,%rax,8), %xmm4    # xmm4 = mem[0],zero
	mulsd	%xmm1, %xmm4
	addsd	%xmm3, %xmm4
	andpd	%xmm2, %xmm4
	maxsd	%xmm4, %xmm0
	incq	%rax
	cmpq	$2048, %rax             # imm = 0x800
	jne	.LBB0_5
# BB#6:                                 # %errorcheck.exit
	movapd	.LCPI0_5(%rip), %xmm1   # xmm1 = [nan,nan]
	andpd	%xmm0, %xmm1
	ucomisd	.LCPI0_6(%rip), %xmm1
	ja	.LBB0_21
# BB#7:
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$16384, %edx            # imm = 0x4000
	movq	%r13, %rdi
	callq	memset
	xorl	%eax, %eax
	movsd	.LCPI0_3(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph.i121
                                        # =>This Inner Loop Header: Depth=1
	imull	$7141, %eax, %eax       # imm = 0x1BE5
	leal	54773(%rax), %ecx
	movslq	%ecx, %rcx
	imulq	$-2123092475, %rcx, %rdx # imm = 0x81742E05
	shrq	$32, %rdx
	leal	54773(%rdx,%rax), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	sarl	$17, %eax
	addl	%edx, %eax
	imull	$259200, %eax, %eax     # imm = 0x3F480
	subl	%eax, %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%r13,%rbp,8)
	imull	$7141, %ecx, %ecx       # imm = 0x1BE5
	leal	54773(%rcx), %eax
	cltq
	imulq	$-2123092475, %rax, %rdx # imm = 0x81742E05
	shrq	$32, %rdx
	leal	54773(%rdx,%rcx), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	sarl	$17, %ecx
	addl	%edx, %ecx
	imull	$259200, %ecx, %ecx     # imm = 0x3F480
	subl	%ecx, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 8(%r13,%rbp,8)
	addq	$2, %rbp
	cmpq	$1024, %rbp             # imm = 0x400
	jne	.LBB0_8
# BB#9:                                 # %putdata.exit122
	movl	$2048, %edi             # imm = 0x800
	movq	%r14, %rsi
	movq	%r13, %rdx
	callq	bitrv2
	movl	$2048, %edi             # imm = 0x800
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	cftfsub
	movl	$56, %eax
	movapd	.LCPI0_7(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB0_10:                               # =>This Inner Loop Header: Depth=1
	movsd	-48(%r13,%rax), %xmm1   # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, -48(%r13,%rax)
	movsd	-32(%r13,%rax), %xmm1   # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, -32(%r13,%rax)
	movsd	-16(%r13,%rax), %xmm1   # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, -16(%r13,%rax)
	movsd	(%r13,%rax), %xmm1      # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, (%r13,%rax)
	addq	$64, %rax
	cmpq	$16440, %rax            # imm = 0x4038
	jne	.LBB0_10
# BB#11:
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$16384, %edx            # imm = 0x4000
	movq	%r12, %rdi
	callq	memset
	xorl	%eax, %eax
	movsd	.LCPI0_3(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB0_12:                               # %.lr.ph.i126
                                        # =>This Inner Loop Header: Depth=1
	imull	$7141, %eax, %eax       # imm = 0x1BE5
	leal	54773(%rax), %ecx
	movslq	%ecx, %rcx
	imulq	$-2123092475, %rcx, %rdx # imm = 0x81742E05
	shrq	$32, %rdx
	leal	54773(%rdx,%rax), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	sarl	$17, %eax
	addl	%edx, %eax
	imull	$259200, %eax, %eax     # imm = 0x3F480
	subl	%eax, %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%r12,%rbp,8)
	imull	$7141, %ecx, %ecx       # imm = 0x1BE5
	leal	54773(%rcx), %eax
	cltq
	imulq	$-2123092475, %rax, %rdx # imm = 0x81742E05
	shrq	$32, %rdx
	leal	54773(%rdx,%rcx), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	sarl	$17, %ecx
	addl	%edx, %ecx
	imull	$259200, %ecx, %ecx     # imm = 0x3F480
	subl	%ecx, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 8(%r12,%rbp,8)
	addq	$2, %rbp
	cmpq	$1024, %rbp             # imm = 0x400
	jne	.LBB0_12
# BB#13:                                # %putdata.exit127
	xorl	%ebp, %ebp
	leaq	24(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	.p2align	4, 0x90
.LBB0_14:                               # %min.iters.checked
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_15 Depth 2
	movl	$16384, %edx            # imm = 0x4000
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcpy
	movl	$2048, %edi             # imm = 0x800
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	bitrv2
	movl	$2048, %edi             # imm = 0x800
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	cftfsub
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_15:                               # %vector.body
                                        #   Parent Loop BB0_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	(%rbx,%rax), %xmm0
	movupd	16(%rbx,%rax), %xmm1
	movapd	%xmm0, %xmm2
	unpcklpd	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0]
	movapd	%xmm0, %xmm3
	unpckhpd	%xmm1, %xmm3    # xmm3 = xmm3[1],xmm1[1]
	movupd	(%r13,%rax), %xmm4
	movupd	16(%r13,%rax), %xmm5
	movapd	%xmm4, %xmm6
	unpcklpd	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0]
	mulpd	%xmm4, %xmm0
	unpckhpd	%xmm5, %xmm4    # xmm4 = xmm4[1],xmm5[1]
	mulpd	%xmm5, %xmm1
	movapd	%xmm0, %xmm5
	unpcklpd	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0]
	movhlps	%xmm0, %xmm1            # xmm1 = xmm0[1],xmm1[1]
	subpd	%xmm1, %xmm5
	mulpd	%xmm2, %xmm4
	mulpd	%xmm3, %xmm6
	addpd	%xmm4, %xmm6
	movapd	%xmm5, %xmm0
	unpcklpd	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0]
	movhlps	%xmm5, %xmm6            # xmm6 = xmm5[1],xmm6[1]
	movups	%xmm6, 16(%rbx,%rax)
	movupd	%xmm0, (%rbx,%rax)
	addq	$32, %rax
	cmpq	$16384, %rax            # imm = 0x4000
	jne	.LBB0_15
# BB#16:                                # %middle.block
                                        #   in Loop: Header=BB0_14 Depth=1
	movl	$2048, %edi             # imm = 0x800
	movl	$-1, %esi
	movq	%rbx, %rdx
	movq	%r14, %rcx
	movq	%r15, %r8
	callq	cdft
	incl	%ebp
	cmpl	$150000, %ebp           # imm = 0x249F0
	jne	.LBB0_14
# BB#17:
	xorl	%ebp, %ebp
	leaq	24(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	.p2align	4, 0x90
.LBB0_18:                               # =>This Inner Loop Header: Depth=1
	movsd	(%rbx,%rbp), %xmm1      # xmm1 = mem[0],zero
	movsd	8(%rbx,%rbp), %xmm2     # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	movapd	.LCPI0_5(%rip), %xmm0   # xmm0 = [nan,nan]
	movapd	%xmm0, %xmm4
	andpd	%xmm4, %xmm3
	movsd	.LCPI0_8(%rip), %xmm0   # xmm0 = mem[0],zero
	movaps	%xmm0, %xmm5
	cmpltsd	%xmm3, %xmm0
	andpd	%xmm1, %xmm0
	movapd	%xmm2, %xmm3
	andpd	%xmm4, %xmm3
	movaps	%xmm5, %xmm1
	cmpltsd	%xmm3, %xmm1
	andpd	%xmm2, %xmm1
	movl	$.L.str.1, %edi
	movb	$2, %al
	callq	printf
	addq	$16, %rbp
	cmpq	$16384, %rbp            # imm = 0x4000
	jne	.LBB0_18
# BB#19:
	movq	%r13, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_21:
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	callq	abort
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.text
	.globl	get_time
	.p2align	4, 0x90
	.type	get_time,@function
get_time:                               # @get_time
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 32
	leaq	8(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	cvtsi2sdq	8(%rsp), %xmm1
	cvtsi2sdq	16(%rsp), %xmm0
	mulsd	.LCPI1_0(%rip), %xmm0
	addsd	%xmm1, %xmm0
	addq	$24, %rsp
	retq
.Lfunc_end1:
	.size	get_time, .Lfunc_end1-get_time
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4607182418800017408     # double 1
.LCPI2_1:
	.quad	4605249457297304856     # double 0.78539816339744828
	.text
	.globl	makewt
	.p2align	4, 0x90
	.type	makewt,@function
makewt:                                 # @makewt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 96
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movq	%rsi, %rbp
	movl	%edi, %r15d
	cmpl	$3, %r15d
	jl	.LBB2_9
# BB#1:
	movl	%r15d, %ebx
	shrl	%ebx
	movsd	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	atan
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movsd	.LCPI2_1(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	.LCPI2_0(%rip), %xmm2   # xmm2 = mem[0],zero
	movupd	%xmm2, (%r13)
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	mulsd	%xmm1, %xmm0
	callq	cos
	movsd	%xmm0, (%r13,%rbx,8)
	movsd	%xmm0, 8(%r13,%rbx,8)
	cmpl	$6, %r15d
	jb	.LBB2_9
# BB#2:                                 # %.lr.ph.preheader
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movl	%ebx, %r12d
	movslq	%r15d, %rbp
	cmpl	$4, %ebx
	movl	$4, %eax
	cmovaq	%r12, %rax
	addq	$-3, %rax
	movq	%rax, %r14
	shrq	%r14
	btl	$1, %eax
	jb	.LBB2_3
# BB#4:                                 # %.lr.ph.prol
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	cos
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	%xmm1, 16(%r13)
	movsd	%xmm0, 24(%r13)
	movsd	%xmm0, -16(%r13,%rbp,8)
	movsd	%xmm1, -8(%r13,%rbp,8)
	movl	$4, %ebx
	jmp	.LBB2_5
.LBB2_9:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_3:
	movl	$2, %ebx
.LBB2_5:                                # %.lr.ph.prol.loopexit
	testq	%r14, %r14
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	je	.LBB2_8
# BB#6:                                 # %.lr.ph.preheader.new
	addq	$-2, %rbp
	subq	%rbx, %rbp
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	cos
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	%xmm1, (%r13,%rbx,8)
	movsd	%xmm0, 8(%r13,%rbx,8)
	movsd	%xmm0, 16(%r13,%rbp,8)
	movsd	%xmm1, 24(%r13,%rbp,8)
	leal	2(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	cos
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movsd	16(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	%xmm2, 16(%r13,%rbx,8)
	movsd	%xmm0, 24(%r13,%rbx,8)
	movsd	%xmm0, (%r13,%rbp,8)
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	%xmm2, 8(%r13,%rbp,8)
	addq	$-4, %rbp
	addq	$4, %rbx
	cmpq	%r12, %rbx
	jl	.LBB2_7
.LBB2_8:                                # %._crit_edge
	movl	%r15d, %edi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%r13, %rdx
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	bitrv2                  # TAILCALL
.Lfunc_end2:
	.size	makewt, .Lfunc_end2-makewt
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4526168777503116145     # double 3.8580246913580248E-6
	.text
	.globl	putdata
	.p2align	4, 0x90
	.type	putdata,@function
putdata:                                # @putdata
	.cfi_startproc
# BB#0:
	cmpl	%esi, %edi
	jg	.LBB3_3
# BB#1:                                 # %.lr.ph.preheader
	movslq	%edi, %rax
	movslq	%esi, %r8
	decq	%rax
	xorl	%esi, %esi
	movsd	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	imull	$7141, %esi, %edi       # imm = 0x1BE5
	leal	54773(%rdi), %esi
	movslq	%esi, %rsi
	imulq	$-2123092475, %rsi, %rcx # imm = 0x81742E05
	shrq	$32, %rcx
	leal	54773(%rcx,%rdi), %ecx
	movl	%ecx, %edi
	shrl	$31, %edi
	sarl	$17, %ecx
	addl	%edi, %ecx
	imull	$259200, %ecx, %ecx     # imm = 0x3F480
	subl	%ecx, %esi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rdx,%rax,8)
	incq	%rax
	cmpq	%r8, %rax
	jl	.LBB3_2
.LBB3_3:                                # %._crit_edge
	retq
.Lfunc_end3:
	.size	putdata, .Lfunc_end3-putdata
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	cdft
	.p2align	4, 0x90
	.type	cdft,@function
cdft:                                   # @cdft
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 112
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%r8, %rbx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %r13
	movl	%edi, %r15d
	cmpl	$5, %r15d
	jl	.LBB4_3
# BB#1:
	testl	%esi, %esi
	js	.LBB4_6
# BB#2:
	movl	%r15d, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r13, %rdx
	callq	bitrv2
	movl	%r15d, %edi
	jmp	.LBB4_5
.LBB4_3:
	cmpl	$4, %r15d
	jne	.LBB4_51
# BB#4:
	movl	$4, %edi
.LBB4_5:
	movq	%r13, %rsi
	movq	%rbx, %rdx
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	cftfsub                 # TAILCALL
.LBB4_6:
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	$0, (%rcx)
	cmpl	$9, %r15d
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	jl	.LBB4_26
# BB#7:                                 # %.lr.ph275.i.preheader
	leaq	16(%rcx), %r8
	leaq	12(%rcx), %r9
	movl	$1, %r11d
	movl	%r15d, %r12d
	movq	%rcx, %rbx
	.p2align	4, 0x90
.LBB4_8:                                # %.lr.ph275.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_17 Depth 2
                                        #     Child Loop BB4_21 Depth 2
                                        #     Child Loop BB4_24 Depth 2
	sarl	%r12d
	testl	%r11d, %r11d
	jle	.LBB4_25
# BB#9:                                 # %.lr.ph269.preheader.i
                                        #   in Loop: Header=BB4_8 Depth=1
	movslq	%r11d, %r14
	movl	%r11d, %eax
	cmpl	$8, %r11d
	jae	.LBB4_11
# BB#10:                                #   in Loop: Header=BB4_8 Depth=1
	xorl	%edx, %edx
	jmp	.LBB4_19
	.p2align	4, 0x90
.LBB4_11:                               # %min.iters.checked
                                        #   in Loop: Header=BB4_8 Depth=1
	movl	%r11d, %r10d
	andl	$7, %r10d
	movq	%rax, %rdx
	subq	%r10, %rdx
	je	.LBB4_15
# BB#12:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_8 Depth=1
	leaq	(%rbx,%r14,4), %rdi
	leaq	(%rbx,%rax,4), %rbp
	cmpq	%rbp, %rdi
	jae	.LBB4_16
# BB#13:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_8 Depth=1
	leaq	(%r14,%rax), %rdi
	leaq	(%rbx,%rdi,4), %rdi
	cmpq	%rbx, %rdi
	jbe	.LBB4_16
.LBB4_15:                               #   in Loop: Header=BB4_8 Depth=1
	xorl	%edx, %edx
	jmp	.LBB4_19
.LBB4_16:                               # %vector.ph
                                        #   in Loop: Header=BB4_8 Depth=1
	movd	%r12d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	%rdx, %rdi
	movq	%r8, %rbp
	.p2align	4, 0x90
.LBB4_17:                               # %vector.body
                                        #   Parent Loop BB4_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-16(%rbp), %xmm1
	movdqu	(%rbp), %xmm2
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm2
	movdqu	%xmm1, -16(%rbp,%r14,4)
	movdqu	%xmm2, (%rbp,%r14,4)
	addq	$32, %rbp
	addq	$-8, %rdi
	jne	.LBB4_17
# BB#18:                                # %middle.block
                                        #   in Loop: Header=BB4_8 Depth=1
	testl	%r10d, %r10d
	je	.LBB4_25
	.p2align	4, 0x90
.LBB4_19:                               # %.lr.ph269.i.preheader
                                        #   in Loop: Header=BB4_8 Depth=1
	movl	%eax, %edi
	subl	%edx, %edi
	leaq	-1(%rax), %r10
	subq	%rdx, %r10
	andq	$3, %rdi
	je	.LBB4_22
# BB#20:                                # %.lr.ph269.i.prol.preheader
                                        #   in Loop: Header=BB4_8 Depth=1
	leaq	(%rbx,%r14,4), %rbp
	negq	%rdi
	.p2align	4, 0x90
.LBB4_21:                               # %.lr.ph269.i.prol
                                        #   Parent Loop BB4_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx,%rdx,4), %esi
	addl	%r12d, %esi
	movl	%esi, (%rbp,%rdx,4)
	incq	%rdx
	incq	%rdi
	jne	.LBB4_21
.LBB4_22:                               # %.lr.ph269.i.prol.loopexit
                                        #   in Loop: Header=BB4_8 Depth=1
	cmpq	$3, %r10
	jb	.LBB4_25
# BB#23:                                # %.lr.ph269.i.preheader.new
                                        #   in Loop: Header=BB4_8 Depth=1
	subq	%rdx, %rax
	addq	%rdx, %r14
	leaq	(%r9,%r14,4), %rbp
	leaq	(%r9,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB4_24:                               # %.lr.ph269.i
                                        #   Parent Loop BB4_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-12(%rdx), %esi
	addl	%r12d, %esi
	movl	%esi, -12(%rbp)
	movl	-8(%rdx), %esi
	addl	%r12d, %esi
	movl	%esi, -8(%rbp)
	movl	-4(%rdx), %esi
	addl	%r12d, %esi
	movl	%esi, -4(%rbp)
	movl	(%rdx), %esi
	addl	%r12d, %esi
	movl	%esi, (%rbp)
	addq	$16, %rbp
	addq	$16, %rdx
	addq	$-4, %rax
	jne	.LBB4_24
.LBB4_25:                               # %._crit_edge270.i
                                        #   in Loop: Header=BB4_8 Depth=1
	leal	(%r11,%r11), %r14d
	movl	%r11d, %eax
	shll	$4, %eax
	cmpl	%r12d, %eax
	movl	%r14d, %r11d
	jl	.LBB4_8
	jmp	.LBB4_27
.LBB4_26:
	movl	$8, %eax
	movl	$1, %r14d
	movl	%r15d, %r12d
	movq	%rcx, %rbx
.LBB4_27:                               # %._crit_edge276.i
	leal	(%r14,%r14), %r9d
	cmpl	%r12d, %eax
	movl	%r15d, 36(%rsp)         # 4-byte Spill
	jne	.LBB4_35
# BB#28:                                # %.preheader258.i
	testl	%r14d, %r14d
	jle	.LBB4_40
# BB#29:                                # %.preheader.lr.ph.i
	movl	%r9d, %eax
	orl	$1, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movslq	%r9d, %rdi
	movl	%r14d, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	shll	$2, %r14d
	leaq	(,%rdi,8), %r10
	xorl	%r12d, %r12d
	movdqa	.LCPI4_0(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB4_30:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_32 Depth 2
	testq	%r12, %r12
	movslq	(%rbx,%r12,4), %rax
	leal	(%r12,%r12), %r11d
	jle	.LBB4_34
# BB#31:                                # %.lr.ph.i
                                        #   in Loop: Header=BB4_30 Depth=1
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(%r13,%rax,8), %rbp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_32:                               #   Parent Loop BB4_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rbx,%rax,4), %rdx
	movl	%r9d, %ecx
	movslq	%r11d, %r8
	addq	%rdx, %r8
	movq	(%rbp), %rdx
	movq	8(%rbp), %xmm1          # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movq	(%r13,%r8,8), %r9
	movslq	%r8d, %r15
	movq	8(%r13,%r15,8), %xmm2   # xmm2 = mem[0],zero
	pxor	%xmm0, %xmm2
	movq	%r9, (%rbp)
	movq	%xmm2, 8(%rbp)
	movq	%rdx, (%r13,%r8,8)
	movl	%ecx, %r9d
	movq	%xmm1, 8(%r13,%r15,8)
	addl	%r14d, %r15d
	leaq	(%rbp,%r10), %rdx
	movq	(%rbp,%rdi,8), %rsi
	movq	8(%rbp,%rdi,8), %xmm1   # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movslq	%r15d, %rbx
	movq	(%r13,%rbx,8), %rcx
	movq	8(%r13,%rbx,8), %xmm2   # xmm2 = mem[0],zero
	pxor	%xmm0, %xmm2
	movq	%rcx, (%rbp,%rdi,8)
	movq	%xmm2, 8(%rbp,%rdi,8)
	movq	%rsi, (%r13,%rbx,8)
	movq	%xmm1, 8(%r13,%rbx,8)
	subl	%r9d, %ebx
	movq	(%rdx,%rdi,8), %rcx
	movq	8(%rdx,%rdi,8), %xmm1   # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movslq	%ebx, %rsi
	movq	(%r13,%rsi,8), %rbx
	movq	8(%r13,%rsi,8), %xmm2   # xmm2 = mem[0],zero
	pxor	%xmm0, %xmm2
	movq	%rbx, (%rdx,%rdi,8)
	movq	%xmm2, 8(%rdx,%rdi,8)
	leaq	(%rdx,%r10), %rdx
	movq	%rcx, (%r13,%rsi,8)
	movq	%xmm1, 8(%r13,%rsi,8)
	addl	%r14d, %esi
	movq	(%rdx,%rdi,8), %rcx
	movq	8(%rdx,%rdi,8), %xmm1   # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movslq	%esi, %rsi
	movq	(%r13,%rsi,8), %rbx
	movq	8(%r13,%rsi,8), %xmm2   # xmm2 = mem[0],zero
	pxor	%xmm0, %xmm2
	movq	%rbx, (%rdx,%rdi,8)
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%xmm2, 8(%rdx,%rdi,8)
	movq	%rcx, (%r13,%rsi,8)
	movq	%xmm1, 8(%r13,%rsi,8)
	incq	%rax
	addq	$16, %rbp
	cmpq	%rax, %r12
	jne	.LBB4_32
# BB#33:                                #   in Loop: Header=BB4_30 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
.LBB4_34:                               # %._crit_edge.i
                                        #   in Loop: Header=BB4_30 Depth=1
	addl	%r11d, %eax
	cltq
	movq	8(%r13,%rax,8), %xmm1   # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movq	%xmm1, 8(%r13,%rax,8)
	addl	%r9d, %eax
	movslq	%eax, %rcx
	addl	%r9d, %eax
	movq	(%r13,%rcx,8), %rdx
	movq	8(%r13,%rcx,8), %xmm1   # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	cltq
	movq	(%r13,%rax,8), %rsi
	movq	8(%r13,%rax,8), %xmm2   # xmm2 = mem[0],zero
	pxor	%xmm0, %xmm2
	movq	%rsi, (%r13,%rcx,8)
	movq	%xmm2, 8(%r13,%rcx,8)
	movq	%rdx, (%r13,%rax,8)
	movq	%xmm1, 8(%r13,%rax,8)
	addl	16(%rsp), %eax          # 4-byte Folded Reload
	cltq
	movq	(%r13,%rax,8), %xmm1    # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movq	%xmm1, (%r13,%rax,8)
	incq	%r12
	cmpq	48(%rsp), %r12          # 8-byte Folded Reload
	jne	.LBB4_30
	jmp	.LBB4_40
.LBB4_35:
	movq	8(%r13), %xmm1          # xmm1 = mem[0],zero
	movdqa	.LCPI4_0(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00]
	pxor	%xmm0, %xmm1
	movq	%xmm1, 8(%r13)
	movl	%r9d, %r10d
	orl	$1, %r10d
	movslq	%r10d, %rax
	movq	(%r13,%rax,8), %xmm1    # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movq	%xmm1, (%r13,%rax,8)
	cmpl	$2, %r14d
	jl	.LBB4_40
# BB#36:                                # %.preheader259.lr.ph.i
	movslq	%r9d, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%r14d, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%r13), %r12
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB4_37:                               # %.lr.ph264.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_38 Depth 2
	movslq	(%rbx,%r15,4), %r11
	leal	(%r15,%r15), %edx
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r11), %rax
	leaq	(%r12,%rax,8), %rsi
	leaq	(%r12,%r11,8), %rdi
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_38:                               #   Parent Loop BB4_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rbx,%r14,4), %rcx
	movslq	%edx, %rax
	addq	%rax, %rcx
	movq	-8(%rdi), %rbx
	movq	(%rdi), %xmm1           # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movq	(%r13,%rcx,8), %rbp
	movslq	%ecx, %r8
	movq	8(%r13,%r8,8), %xmm2    # xmm2 = mem[0],zero
	pxor	%xmm0, %xmm2
	movq	%rbp, -8(%rdi)
	movq	%xmm2, (%rdi)
	movq	%rbx, (%r13,%rcx,8)
	movq	%xmm1, 8(%r13,%r8,8)
	addl	%r9d, %r8d
	movq	-8(%rsi), %rcx
	movq	(%rsi), %xmm1           # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movslq	%r8d, %rbp
	movq	(%r13,%rbp,8), %rbx
	movq	8(%r13,%rbp,8), %xmm2   # xmm2 = mem[0],zero
	pxor	%xmm0, %xmm2
	movq	%rbx, -8(%rsi)
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%xmm2, (%rsi)
	movq	%rcx, (%r13,%rbp,8)
	movq	%xmm1, 8(%r13,%rbp,8)
	incq	%r14
	addq	$16, %rsi
	addq	$16, %rdi
	cmpq	%r14, %r15
	jne	.LBB4_38
# BB#39:                                # %._crit_edge265.i
                                        #   in Loop: Header=BB4_37 Depth=1
	addq	%r11, %rax
	movq	8(%r13,%rax,8), %xmm1   # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movq	%xmm1, 8(%r13,%rax,8)
	leal	(%rax,%r10), %eax
	cltq
	movq	(%r13,%rax,8), %xmm1    # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movq	%xmm1, (%r13,%rax,8)
	incq	%r15
	cmpq	16(%rsp), %r15          # 8-byte Folded Reload
	jne	.LBB4_37
.LBB4_40:                               # %bitrv2conj.exit
	movl	$2, %ebp
	movl	36(%rsp), %ebx          # 4-byte Reload
	cmpl	$9, %ebx
	movq	40(%rsp), %r14          # 8-byte Reload
	jl	.LBB4_44
# BB#41:
	movl	%ebx, %edi
	movq	%r13, %rsi
	movq	%r14, %rdx
	callq	cft1st
	movl	$8, %ebp
	cmpl	$33, %ebx
	jl	.LBB4_44
# BB#42:                                # %.lr.ph148.i.preheader
	movl	$8, %esi
	movl	$32, %eax
	.p2align	4, 0x90
.LBB4_43:                               # %.lr.ph148.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebp
	movl	%ebx, %edi
	movq	%r13, %rdx
	movq	%r14, %rcx
	callq	cftmdl
	leal	(,%rbp,4), %eax
	cmpl	%ebx, %eax
	movl	%ebp, %esi
	jl	.LBB4_43
.LBB4_44:                               # %.loopexit143.i
	leal	(,%rbp,4), %eax
	cmpl	%ebx, %eax
	jne	.LBB4_48
# BB#45:                                # %.preheader.i14
	testl	%ebp, %ebp
	jle	.LBB4_51
# BB#46:                                # %.lr.ph.preheader.i
	movslq	%ebp, %rax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%r13,%rcx,8), %rcx
	movq	%rax, %rdx
	shlq	$4, %rdx
	leaq	8(%r13,%rdx), %rdx
	leaq	(%r13,%rax,8), %rsi
	xorl	%edi, %edi
	movapd	.LCPI4_0(%rip), %xmm8   # xmm8 = [-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB4_47:                               # %.lr.ph.i17
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r13,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	movsd	(%rsi,%rdi,8), %xmm4    # xmm4 = mem[0],zero
	movapd	%xmm1, %xmm3
	addsd	%xmm4, %xmm3
	movsd	8(%r13,%rdi,8), %xmm2   # xmm2 = mem[0],zero
	unpcklpd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0]
	xorpd	%xmm8, %xmm2
	movsd	8(%rsi,%rdi,8), %xmm5   # xmm5 = mem[0],zero
	subsd	%xmm5, %xmm2
	unpcklpd	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0]
	subpd	%xmm4, %xmm1
	movsd	-8(%rdx,%rdi,8), %xmm4  # xmm4 = mem[0],zero
	movsd	(%rcx,%rdi,8), %xmm5    # xmm5 = mem[0],zero
	movsd	(%rdx,%rdi,8), %xmm6    # xmm6 = mem[0],zero
	movsd	8(%rcx,%rdi,8), %xmm7   # xmm7 = mem[0],zero
	movapd	%xmm6, %xmm0
	addsd	%xmm7, %xmm0
	unpcklpd	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0]
	addsd	%xmm5, %xmm4
	unpcklpd	%xmm5, %xmm7    # xmm7 = xmm7[0],xmm5[0]
	subpd	%xmm7, %xmm6
	movapd	%xmm3, %xmm5
	addsd	%xmm4, %xmm5
	movsd	%xmm5, (%r13,%rdi,8)
	movapd	%xmm2, %xmm5
	subsd	%xmm0, %xmm5
	movsd	%xmm5, 8(%r13,%rdi,8)
	subsd	%xmm4, %xmm3
	movsd	%xmm3, -8(%rdx,%rdi,8)
	addsd	%xmm2, %xmm0
	movsd	%xmm0, (%rdx,%rdi,8)
	movapd	%xmm1, %xmm0
	subpd	%xmm6, %xmm0
	movupd	%xmm0, (%rsi,%rdi,8)
	addpd	%xmm1, %xmm6
	movupd	%xmm6, (%rcx,%rdi,8)
	addq	$2, %rdi
	cmpq	%rax, %rdi
	jl	.LBB4_47
	jmp	.LBB4_51
.LBB4_48:                               # %.preheader141.i
	testl	%ebp, %ebp
	jle	.LBB4_51
# BB#49:                                # %.lr.ph146.preheader.i
	movslq	%ebp, %rax
	leaq	8(%r13,%rax,8), %rcx
	xorl	%edx, %edx
	movapd	.LCPI4_0(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB4_50:                               # %.lr.ph146.i
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r13,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	movsd	-8(%rcx,%rdx,8), %xmm2  # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	movhpd	(%rcx,%rdx,8), %xmm3    # xmm3 = xmm3[0],mem[0]
	movsd	8(%r13,%rdx,8), %xmm4   # xmm4 = mem[0],zero
	addsd	%xmm2, %xmm1
	unpcklpd	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0]
	subpd	%xmm2, %xmm3
	movsd	%xmm1, (%r13,%rdx,8)
	xorpd	%xmm0, %xmm4
	subsd	(%rcx,%rdx,8), %xmm4
	movsd	%xmm4, 8(%r13,%rdx,8)
	movupd	%xmm3, -8(%rcx,%rdx,8)
	addq	$2, %rdx
	cmpq	%rax, %rdx
	jl	.LBB4_50
.LBB4_51:                               # %cftbsub.exit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	cdft, .Lfunc_end4-cdft
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4526168777503116145     # double 3.8580246913580248E-6
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_1:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	errorcheck
	.p2align	4, 0x90
	.type	errorcheck,@function
errorcheck:                             # @errorcheck
	.cfi_startproc
# BB#0:
	xorpd	%xmm1, %xmm1
	cmpl	%esi, %edi
	jg	.LBB5_3
# BB#1:                                 # %.lr.ph.preheader
	movslq	%edi, %rax
	movslq	%esi, %r8
	decq	%rax
	xorl	%esi, %esi
	xorpd	%xmm1, %xmm1
	movsd	.LCPI5_0(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	.LCPI5_1(%rip), %xmm3   # xmm3 = [nan,nan]
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	imull	$7141, %esi, %edi       # imm = 0x1BE5
	leal	54773(%rdi), %esi
	movslq	%esi, %rsi
	imulq	$-2123092475, %rsi, %rcx # imm = 0x81742E05
	shrq	$32, %rcx
	leal	54773(%rcx,%rdi), %ecx
	movl	%ecx, %edi
	shrl	$31, %edi
	sarl	$17, %ecx
	addl	%edi, %ecx
	imull	$259200, %ecx, %ecx     # imm = 0x3F480
	subl	%ecx, %esi
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%esi, %xmm4
	mulsd	%xmm2, %xmm4
	movsd	8(%rdx,%rax,8), %xmm5   # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	subsd	%xmm5, %xmm4
	andpd	%xmm3, %xmm4
	maxsd	%xmm4, %xmm1
	incq	%rax
	cmpq	%r8, %rax
	jl	.LBB5_2
.LBB5_3:                                # %._crit_edge
	movapd	%xmm1, %xmm0
	retq
.Lfunc_end5:
	.size	errorcheck, .Lfunc_end5-errorcheck
	.cfi_endproc

	.p2align	4, 0x90
	.type	bitrv2,@function
bitrv2:                                 # @bitrv2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movl	$0, (%rsi)
	cmpl	$9, %edi
	jl	.LBB6_20
# BB#1:                                 # %.lr.ph246.preheader
	leaq	16(%rsi), %r8
	leaq	12(%rsi), %r9
	movl	$1, %r10d
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph246
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_11 Depth 2
                                        #     Child Loop BB6_15 Depth 2
                                        #     Child Loop BB6_18 Depth 2
	sarl	%edi
	testl	%r10d, %r10d
	jle	.LBB6_19
# BB#3:                                 # %.lr.ph240.preheader
                                        #   in Loop: Header=BB6_2 Depth=1
	movslq	%r10d, %r11
	movl	%r10d, %ebx
	cmpl	$8, %r10d
	jae	.LBB6_5
# BB#4:                                 #   in Loop: Header=BB6_2 Depth=1
	xorl	%eax, %eax
	jmp	.LBB6_13
	.p2align	4, 0x90
.LBB6_5:                                # %min.iters.checked
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	%r10d, %r14d
	andl	$7, %r14d
	movq	%rbx, %rax
	subq	%r14, %rax
	je	.LBB6_9
# BB#6:                                 # %vector.memcheck
                                        #   in Loop: Header=BB6_2 Depth=1
	leaq	(%rsi,%r11,4), %rcx
	leaq	(%rsi,%rbx,4), %rbp
	cmpq	%rbp, %rcx
	jae	.LBB6_10
# BB#7:                                 # %vector.memcheck
                                        #   in Loop: Header=BB6_2 Depth=1
	leaq	(%r11,%rbx), %rcx
	leaq	(%rsi,%rcx,4), %rcx
	cmpq	%rsi, %rcx
	jbe	.LBB6_10
.LBB6_9:                                #   in Loop: Header=BB6_2 Depth=1
	xorl	%eax, %eax
	jmp	.LBB6_13
.LBB6_10:                               # %vector.ph
                                        #   in Loop: Header=BB6_2 Depth=1
	movd	%edi, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	%rax, %rbp
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB6_11:                               # %vector.body
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-16(%rcx), %xmm1
	movdqu	(%rcx), %xmm2
	paddd	%xmm0, %xmm1
	paddd	%xmm0, %xmm2
	movdqu	%xmm1, -16(%rcx,%r11,4)
	movdqu	%xmm2, (%rcx,%r11,4)
	addq	$32, %rcx
	addq	$-8, %rbp
	jne	.LBB6_11
# BB#12:                                # %middle.block
                                        #   in Loop: Header=BB6_2 Depth=1
	testl	%r14d, %r14d
	je	.LBB6_19
	.p2align	4, 0x90
.LBB6_13:                               # %.lr.ph240.preheader296
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	%ebx, %ecx
	subl	%eax, %ecx
	leaq	-1(%rbx), %r14
	subq	%rax, %r14
	andq	$3, %rcx
	je	.LBB6_16
# BB#14:                                # %.lr.ph240.prol.preheader
                                        #   in Loop: Header=BB6_2 Depth=1
	leaq	(%rsi,%r11,4), %r15
	negq	%rcx
	.p2align	4, 0x90
.LBB6_15:                               # %.lr.ph240.prol
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi,%rax,4), %ebp
	addl	%edi, %ebp
	movl	%ebp, (%r15,%rax,4)
	incq	%rax
	incq	%rcx
	jne	.LBB6_15
.LBB6_16:                               # %.lr.ph240.prol.loopexit
                                        #   in Loop: Header=BB6_2 Depth=1
	cmpq	$3, %r14
	jb	.LBB6_19
# BB#17:                                # %.lr.ph240.preheader296.new
                                        #   in Loop: Header=BB6_2 Depth=1
	subq	%rax, %rbx
	addq	%rax, %r11
	leaq	(%r9,%r11,4), %rbp
	leaq	(%r9,%rax,4), %rax
	.p2align	4, 0x90
.LBB6_18:                               # %.lr.ph240
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-12(%rax), %ecx
	addl	%edi, %ecx
	movl	%ecx, -12(%rbp)
	movl	-8(%rax), %ecx
	addl	%edi, %ecx
	movl	%ecx, -8(%rbp)
	movl	-4(%rax), %ecx
	addl	%edi, %ecx
	movl	%ecx, -4(%rbp)
	movl	(%rax), %ecx
	addl	%edi, %ecx
	movl	%ecx, (%rbp)
	addq	$16, %rbp
	addq	$16, %rax
	addq	$-4, %rbx
	jne	.LBB6_18
.LBB6_19:                               # %._crit_edge241
                                        #   in Loop: Header=BB6_2 Depth=1
	leal	(%r10,%r10), %r14d
	movl	%r10d, %eax
	shll	$4, %eax
	cmpl	%edi, %eax
	movl	%r14d, %r10d
	jl	.LBB6_2
	jmp	.LBB6_21
.LBB6_20:
	movl	$8, %eax
	movl	$1, %r14d
.LBB6_21:                               # %._crit_edge247
	leal	(%r14,%r14), %r9d
	cmpl	%edi, %eax
	movq	%rsi, -16(%rsp)         # 8-byte Spill
	movl	%r9d, -28(%rsp)         # 4-byte Spill
	jne	.LBB6_29
# BB#22:                                # %.preheader228
	testl	%r14d, %r14d
	jle	.LBB6_34
# BB#23:                                # %.preheader.lr.ph
	movslq	%r9d, %r10
	movl	%r14d, %eax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	shll	$2, %r14d
	leaq	(,%r10,8), %r12
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB6_24:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_26 Depth 2
	testq	%r11, %r11
	movslq	(%rsi,%r11,4), %rax
	leal	(%r11,%r11), %r15d
	jle	.LBB6_28
# BB#25:                                # %.lr.ph
                                        #   in Loop: Header=BB6_24 Depth=1
	movq	%rax, -24(%rsp)         # 8-byte Spill
	leaq	(%rdx,%rax,8), %rax
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_26:                               #   Parent Loop BB6_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rsi,%rbx,4), %rbp
	movslq	%r15d, %rcx
	addq	%rbp, %rcx
	movq	(%rax), %rbp
	movq	8(%rax), %r13
	movq	(%rdx,%rcx,8), %r8
	movq	%r11, %rdi
	movslq	%ecx, %r11
	movq	8(%rdx,%r11,8), %r9
	movq	%r8, (%rax)
	movq	%r9, 8(%rax)
	movq	%rbp, (%rdx,%rcx,8)
	movq	%r13, 8(%rdx,%r11,8)
	movl	-28(%rsp), %r9d         # 4-byte Reload
	movq	-16(%rsp), %rsi         # 8-byte Reload
	addl	%r14d, %r11d
	leaq	(%rax,%r12), %rcx
	movups	(%rax,%r10,8), %xmm0
	movslq	%r11d, %rbp
	movq	%rdi, %r11
	movq	(%rdx,%rbp,8), %r8
	movq	8(%rdx,%rbp,8), %rdi
	movq	%r8, (%rax,%r10,8)
	movq	%rdi, 8(%rax,%r10,8)
	movups	%xmm0, (%rdx,%rbp,8)
	subl	%r9d, %ebp
	movups	(%rcx,%r10,8), %xmm0
	movslq	%ebp, %rdi
	movq	(%rdx,%rdi,8), %r8
	movq	8(%rdx,%rdi,8), %rbp
	movq	%r8, (%rcx,%r10,8)
	movq	%rbp, 8(%rcx,%r10,8)
	leaq	(%rcx,%r12), %rcx
	movups	%xmm0, (%rdx,%rdi,8)
	addl	%r14d, %edi
	movdqu	(%rcx,%r10,8), %xmm0
	movslq	%edi, %rdi
	movq	(%rdx,%rdi,8), %r8
	movq	8(%rdx,%rdi,8), %rbp
	movq	%r8, (%rcx,%r10,8)
	movq	%rbp, 8(%rcx,%r10,8)
	movdqu	%xmm0, (%rdx,%rdi,8)
	incq	%rbx
	addq	$16, %rax
	cmpq	%rbx, %r11
	jne	.LBB6_26
# BB#27:                                #   in Loop: Header=BB6_24 Depth=1
	movq	-24(%rsp), %rax         # 8-byte Reload
.LBB6_28:                               # %._crit_edge
                                        #   in Loop: Header=BB6_24 Depth=1
	addl	%r9d, %r15d
	addl	%eax, %r15d
	movslq	%r15d, %rax
	addl	%r9d, %r15d
	movq	(%rdx,%rax,8), %rcx
	movq	8(%rdx,%rax,8), %r8
	movslq	%r15d, %rdi
	movq	(%rdx,%rdi,8), %rbp
	movq	8(%rdx,%rdi,8), %rbx
	movq	%rbp, (%rdx,%rax,8)
	movq	%rbx, 8(%rdx,%rax,8)
	movq	%rcx, (%rdx,%rdi,8)
	movq	%r8, 8(%rdx,%rdi,8)
	incq	%r11
	cmpq	-8(%rsp), %r11          # 8-byte Folded Reload
	jne	.LBB6_24
	jmp	.LBB6_34
.LBB6_29:                               # %.preheader230
	cmpl	$2, %r14d
	jl	.LBB6_34
# BB#30:                                # %.preheader229.preheader
	movslq	%r9d, %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movl	%r14d, %r10d
	leaq	8(%rdx), %r11
	movl	$1, %r9d
	.p2align	4, 0x90
.LBB6_31:                               # %.lr.ph235
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_32 Depth 2
	movslq	(%rsi,%r9,4), %rax
	leal	(%r9,%r9), %r14d
	movq	-24(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax), %rcx
	leaq	(%r11,%rcx,8), %rcx
	leaq	(%r11,%rax,8), %rax
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB6_32:                               #   Parent Loop BB6_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rsi,%rdi,4), %rsi
	movslq	%r14d, %rbx
	addq	%rsi, %rbx
	movq	-8(%rax), %r15
	movq	(%rax), %r12
	movq	(%rdx,%rbx,8), %r13
	movslq	%ebx, %rbp
	movq	8(%rdx,%rbp,8), %r8
	movq	%r13, -8(%rax)
	movq	-16(%rsp), %rsi         # 8-byte Reload
	movq	%r8, (%rax)
	movq	%r15, (%rdx,%rbx,8)
	movq	%r12, 8(%rdx,%rbp,8)
	addl	-28(%rsp), %ebp         # 4-byte Folded Reload
	movdqu	-8(%rcx), %xmm0
	movslq	%ebp, %rbx
	movq	(%rdx,%rbx,8), %r8
	movq	8(%rdx,%rbx,8), %rbp
	movq	%r8, -8(%rcx)
	movq	%rbp, (%rcx)
	movdqu	%xmm0, (%rdx,%rbx,8)
	incq	%rdi
	addq	$16, %rcx
	addq	$16, %rax
	cmpq	%rdi, %r9
	jne	.LBB6_32
# BB#33:                                # %._crit_edge236
                                        #   in Loop: Header=BB6_31 Depth=1
	incq	%r9
	cmpq	%r10, %r9
	jne	.LBB6_31
.LBB6_34:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	bitrv2, .Lfunc_end6-bitrv2
	.cfi_endproc

	.p2align	4, 0x90
	.type	cftfsub,@function
cftfsub:                                # @cftfsub
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 48
.Lcfi57:
	.cfi_offset %rbx, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movl	%edi, %r15d
	movl	$2, %ebp
	cmpl	$9, %r15d
	jl	.LBB7_4
# BB#1:
	movl	%r15d, %edi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	cft1st
	movl	$8, %ebp
	cmpl	$33, %r15d
	jl	.LBB7_4
# BB#2:                                 # %.lr.ph146.preheader
	movl	$8, %esi
	movl	$32, %eax
	.p2align	4, 0x90
.LBB7_3:                                # %.lr.ph146
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebp
	movl	%r15d, %edi
	movq	%rbx, %rdx
	movq	%r14, %rcx
	callq	cftmdl
	leal	(,%rbp,4), %eax
	cmpl	%r15d, %eax
	movl	%ebp, %esi
	jl	.LBB7_3
.LBB7_4:                                # %.loopexit141
	leal	(,%rbp,4), %eax
	cmpl	%r15d, %eax
	jne	.LBB7_5
# BB#8:                                 # %.preheader
	testl	%ebp, %ebp
	jle	.LBB7_11
# BB#9:                                 # %.lr.ph.preheader
	movslq	%ebp, %rax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rbx,%rcx,8), %rcx
	movq	%rax, %rdx
	shlq	$4, %rdx
	addq	%rbx, %rdx
	leaq	(%rbx,%rax,8), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB7_10:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rbx,%rdi,8), %xmm0
	movupd	(%rsi,%rdi,8), %xmm1
	movapd	%xmm0, %xmm2
	addpd	%xmm1, %xmm2
	subpd	%xmm1, %xmm0
	movupd	(%rdx,%rdi,8), %xmm1
	movupd	(%rcx,%rdi,8), %xmm3
	movapd	%xmm1, %xmm4
	addpd	%xmm3, %xmm4
	subpd	%xmm3, %xmm1
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	movapd	%xmm2, %xmm3
	addpd	%xmm4, %xmm3
	movupd	%xmm3, (%rbx,%rdi,8)
	subpd	%xmm4, %xmm2
	movupd	%xmm2, (%rdx,%rdi,8)
	movapd	%xmm0, %xmm2
	subpd	%xmm1, %xmm2
	addpd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1]
	movupd	%xmm0, (%rsi,%rdi,8)
	movsd	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1]
	movupd	%xmm2, (%rcx,%rdi,8)
	addq	$2, %rdi
	cmpq	%rax, %rdi
	jl	.LBB7_10
	jmp	.LBB7_11
.LBB7_5:                                # %.preheader139
	testl	%ebp, %ebp
	jle	.LBB7_11
# BB#6:                                 # %.lr.ph144.preheader
	movslq	%ebp, %rax
	leaq	(%rbx,%rax,8), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB7_7:                                # %.lr.ph144
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rbx,%rdx,8), %xmm0
	movupd	(%rcx,%rdx,8), %xmm1
	movapd	%xmm0, %xmm2
	subpd	%xmm1, %xmm2
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rbx,%rdx,8)
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	addsd	8(%rcx,%rdx,8), %xmm0
	movsd	%xmm0, 8(%rbx,%rdx,8)
	movupd	%xmm2, (%rcx,%rdx,8)
	addq	$2, %rdx
	cmpq	%rax, %rdx
	jl	.LBB7_7
.LBB7_11:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	cftfsub, .Lfunc_end7-cftfsub
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.p2align	4, 0x90
	.type	cft1st,@function
cft1st:                                 # @cft1st
	.cfi_startproc
# BB#0:
	movupd	(%rsi), %xmm0
	movupd	16(%rsi), %xmm1
	movupd	32(%rsi), %xmm2
	movupd	48(%rsi), %xmm3
	movapd	%xmm0, %xmm4
	addpd	%xmm1, %xmm4
	subpd	%xmm1, %xmm0
	movapd	%xmm2, %xmm1
	addpd	%xmm3, %xmm1
	subpd	%xmm3, %xmm2
	shufpd	$1, %xmm2, %xmm2        # xmm2 = xmm2[1,0]
	movapd	%xmm4, %xmm3
	addpd	%xmm1, %xmm3
	movupd	%xmm3, (%rsi)
	subpd	%xmm1, %xmm4
	movupd	%xmm4, 32(%rsi)
	movapd	%xmm0, %xmm1
	subpd	%xmm2, %xmm1
	addpd	%xmm0, %xmm2
	movapd	%xmm2, %xmm0
	movsd	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1]
	movupd	%xmm0, 16(%rsi)
	movsd	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1]
	movupd	%xmm1, 48(%rsi)
	movsd	16(%rdx), %xmm0         # xmm0 = mem[0],zero
	movupd	64(%rsi), %xmm2
	movupd	80(%rsi), %xmm3
	movupd	96(%rsi), %xmm1
	movupd	112(%rsi), %xmm4
	movapd	%xmm2, %xmm5
	addpd	%xmm3, %xmm5
	subpd	%xmm3, %xmm2
	movapd	%xmm1, %xmm6
	addpd	%xmm4, %xmm6
	subpd	%xmm4, %xmm1
	movapd	%xmm1, %xmm3
	shufpd	$1, %xmm3, %xmm3        # xmm3 = xmm3[1,0]
	movapd	%xmm5, %xmm4
	addpd	%xmm6, %xmm4
	movupd	%xmm4, 64(%rsi)
	movapd	%xmm6, %xmm4
	shufpd	$1, %xmm5, %xmm4        # xmm4 = xmm4[1],xmm5[0]
	shufpd	$1, %xmm6, %xmm5        # xmm5 = xmm5[1],xmm6[0]
	subpd	%xmm5, %xmm4
	movupd	%xmm4, 96(%rsi)
	movapd	%xmm2, %xmm4
	subpd	%xmm3, %xmm4
	addpd	%xmm2, %xmm3
	movapd	%xmm3, %xmm5
	movsd	%xmm4, %xmm5            # xmm5 = xmm4[0],xmm5[1]
	shufpd	$1, %xmm4, %xmm3        # xmm3 = xmm3[1],xmm4[0]
	movapd	%xmm5, %xmm4
	subpd	%xmm3, %xmm4
	addpd	%xmm5, %xmm3
	movsd	%xmm4, %xmm3            # xmm3 = xmm4[0],xmm3[1]
	movapd	%xmm1, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	addsd	%xmm2, %xmm4
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	subsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm2
	subsd	%xmm4, %xmm2
	mulsd	%xmm0, %xmm2
	addsd	%xmm4, %xmm1
	mulsd	%xmm0, %xmm1
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	%xmm3, %xmm0
	movupd	%xmm0, 80(%rsi)
	movsd	%xmm2, 112(%rsi)
	movsd	%xmm1, 120(%rsi)
	cmpl	$17, %edi
	jl	.LBB8_3
# BB#1:                                 # %.lr.ph.preheader
	movslq	%edi, %rax
	movl	$16, %ecx
	movapd	.LCPI8_0(%rip), %xmm8   # xmm8 = [-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB8_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdx,%rcx), %xmm11
	movsd	(%rdx,%rcx,2), %xmm5    # xmm5 = mem[0],zero
	movsd	8(%rdx,%rcx,2), %xmm10  # xmm10 = mem[0],zero
	movaps	%xmm11, %xmm9
	movhlps	%xmm9, %xmm9            # xmm9 = xmm9[1,1]
	movaps	%xmm9, %xmm3
	addsd	%xmm3, %xmm3
	movapd	%xmm3, %xmm0
	mulsd	%xmm10, %xmm0
	movapd	%xmm5, %xmm4
	subsd	%xmm0, %xmm4
	mulsd	%xmm5, %xmm3
	subsd	%xmm10, %xmm3
	movupd	(%rsi,%rcx,8), %xmm2
	movupd	16(%rsi,%rcx,8), %xmm7
	movapd	%xmm2, %xmm0
	addpd	%xmm7, %xmm0
	subpd	%xmm7, %xmm2
	movupd	32(%rsi,%rcx,8), %xmm6
	movupd	48(%rsi,%rcx,8), %xmm1
	movapd	%xmm6, %xmm7
	addpd	%xmm1, %xmm7
	subpd	%xmm1, %xmm6
	shufpd	$1, %xmm6, %xmm6        # xmm6 = xmm6[1,0]
	movapd	%xmm0, %xmm1
	addpd	%xmm7, %xmm1
	movupd	%xmm1, (%rsi,%rcx,8)
	movapd	%xmm0, %xmm1
	subsd	%xmm7, %xmm1
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	movhlps	%xmm7, %xmm7            # xmm7 = xmm7[1,1]
	subsd	%xmm7, %xmm0
	movaps	%xmm11, %xmm7
	shufpd	$1, %xmm7, %xmm7        # xmm7 = xmm7[1,0]
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	%xmm7, %xmm0
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm11, %xmm1
	movapd	%xmm1, %xmm7
	subpd	%xmm0, %xmm7
	addpd	%xmm0, %xmm1
	movsd	%xmm7, %xmm1            # xmm1 = xmm7[0],xmm1[1]
	movupd	%xmm1, 32(%rsi,%rcx,8)
	movapd	%xmm2, %xmm0
	subpd	%xmm6, %xmm0
	addpd	%xmm2, %xmm6
	movapd	%xmm6, %xmm1
	movsd	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1]
	movlhps	%xmm5, %xmm5            # xmm5 = xmm5[0,0]
	mulpd	%xmm1, %xmm5
	movlhps	%xmm10, %xmm10          # xmm10 = xmm10[0,0]
	movapd	%xmm6, %xmm1
	shufpd	$1, %xmm0, %xmm1        # xmm1 = xmm1[1],xmm0[0]
	mulpd	%xmm10, %xmm1
	movapd	%xmm5, %xmm2
	subpd	%xmm1, %xmm2
	addpd	%xmm5, %xmm1
	movsd	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1]
	movupd	%xmm1, 16(%rsi,%rcx,8)
	movapd	%xmm0, %xmm1
	movsd	%xmm6, %xmm1            # xmm1 = xmm6[0],xmm1[1]
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	mulpd	%xmm1, %xmm4
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	shufpd	$1, %xmm6, %xmm0        # xmm0 = xmm0[1],xmm6[0]
	mulpd	%xmm3, %xmm0
	movapd	%xmm4, %xmm1
	subpd	%xmm0, %xmm1
	addpd	%xmm4, %xmm0
	movsd	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1]
	movupd	%xmm0, 48(%rsi,%rcx,8)
	movsd	16(%rdx,%rcx,2), %xmm3  # xmm3 = mem[0],zero
	movsd	24(%rdx,%rcx,2), %xmm13 # xmm13 = mem[0],zero
	movupd	64(%rsi,%rcx,8), %xmm15
	movupd	80(%rsi,%rcx,8), %xmm10
	movapd	%xmm15, %xmm0
	addpd	%xmm10, %xmm0
	movupd	96(%rsi,%rcx,8), %xmm4
	movupd	112(%rsi,%rcx,8), %xmm12
	movapd	%xmm4, %xmm1
	addpd	%xmm12, %xmm1
	movapd	%xmm0, %xmm14
	addpd	%xmm1, %xmm14
	movapd	%xmm0, %xmm5
	subsd	%xmm1, %xmm5
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	subsd	%xmm1, %xmm0
	movapd	%xmm11, %xmm2
	mulsd	%xmm0, %xmm2
	mulsd	%xmm9, %xmm0
	xorpd	%xmm8, %xmm9
	mulsd	%xmm5, %xmm9
	mulsd	%xmm11, %xmm5
	movapd	%xmm11, %xmm7
	addsd	%xmm7, %xmm7
	movapd	%xmm7, %xmm6
	mulsd	%xmm13, %xmm6
	movapd	%xmm3, %xmm1
	subsd	%xmm6, %xmm1
	subpd	%xmm10, %xmm15
	mulsd	%xmm3, %xmm7
	subsd	%xmm13, %xmm7
	subpd	%xmm12, %xmm4
	shufpd	$1, %xmm4, %xmm4        # xmm4 = xmm4[1,0]
	movupd	%xmm14, 64(%rsi,%rcx,8)
	subsd	%xmm2, %xmm9
	movsd	%xmm9, 96(%rsi,%rcx,8)
	subsd	%xmm0, %xmm5
	movsd	%xmm5, 104(%rsi,%rcx,8)
	movapd	%xmm15, %xmm0
	subpd	%xmm4, %xmm0
	addpd	%xmm15, %xmm4
	movapd	%xmm4, %xmm2
	movsd	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1]
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	mulpd	%xmm2, %xmm3
	movlhps	%xmm13, %xmm13          # xmm13 = xmm13[0,0]
	movapd	%xmm4, %xmm2
	shufpd	$1, %xmm0, %xmm2        # xmm2 = xmm2[1],xmm0[0]
	mulpd	%xmm13, %xmm2
	movapd	%xmm3, %xmm5
	subpd	%xmm2, %xmm5
	addpd	%xmm3, %xmm2
	movsd	%xmm5, %xmm2            # xmm2 = xmm5[0],xmm2[1]
	movupd	%xmm2, 80(%rsi,%rcx,8)
	movapd	%xmm0, %xmm2
	movsd	%xmm4, %xmm2            # xmm2 = xmm4[0],xmm2[1]
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm2, %xmm1
	movlhps	%xmm7, %xmm7            # xmm7 = xmm7[0,0]
	shufpd	$1, %xmm4, %xmm0        # xmm0 = xmm0[1],xmm4[0]
	mulpd	%xmm7, %xmm0
	movapd	%xmm1, %xmm2
	subpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm0
	movsd	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1]
	movupd	%xmm0, 112(%rsi,%rcx,8)
	addq	$16, %rcx
	cmpq	%rax, %rcx
	jl	.LBB8_2
.LBB8_3:                                # %._crit_edge
	retq
.Lfunc_end8:
	.size	cft1st, .Lfunc_end8-cft1st
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.p2align	4, 0x90
	.type	cftmdl,@function
cftmdl:                                 # @cftmdl
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi64:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi65:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 56
.Lcfi67:
	.cfi_offset %rbx, -56
.Lcfi68:
	.cfi_offset %r12, -48
.Lcfi69:
	.cfi_offset %r13, -40
.Lcfi70:
	.cfi_offset %r14, -32
.Lcfi71:
	.cfi_offset %r15, -24
.Lcfi72:
	.cfi_offset %rbp, -16
	movq	%rcx, -40(%rsp)         # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	leal	(,%rsi,4), %r8d
	movq	%rsi, -56(%rsp)         # 8-byte Spill
	testl	%esi, %esi
	jle	.LBB9_3
# BB#1:                                 # %.lr.ph456.preheader
	movslq	-56(%rsp), %rax         # 4-byte Folded Reload
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdx,%rcx,8), %rcx
	movq	%rax, %rsi
	shlq	$4, %rsi
	addq	%rdx, %rsi
	leaq	(%rdx,%rax,8), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_2:                                # %.lr.ph456
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rdx,%rbx,8), %xmm0
	movupd	(%rbp,%rbx,8), %xmm1
	movapd	%xmm0, %xmm2
	addpd	%xmm1, %xmm2
	subpd	%xmm1, %xmm0
	movupd	(%rsi,%rbx,8), %xmm1
	movupd	(%rcx,%rbx,8), %xmm3
	movapd	%xmm1, %xmm4
	addpd	%xmm3, %xmm4
	subpd	%xmm3, %xmm1
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	movapd	%xmm2, %xmm3
	addpd	%xmm4, %xmm3
	movupd	%xmm3, (%rdx,%rbx,8)
	subpd	%xmm4, %xmm2
	movupd	%xmm2, (%rsi,%rbx,8)
	movapd	%xmm0, %xmm2
	subpd	%xmm1, %xmm2
	addpd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1]
	movupd	%xmm0, (%rbp,%rbx,8)
	movsd	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1]
	movupd	%xmm2, (%rcx,%rbx,8)
	addq	$2, %rbx
	cmpq	%rax, %rbx
	jl	.LBB9_2
.LBB9_3:                                # %._crit_edge457
	movq	-56(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rax,4), %eax
	cmpl	%eax, %r8d
	jge	.LBB9_6
# BB#4:                                 # %.lr.ph452.preheader
	movq	-40(%rsp), %rcx         # 8-byte Reload
	movsd	16(%rcx), %xmm0         # xmm0 = mem[0],zero
	movslq	%r8d, %rcx
	movslq	-56(%rsp), %rsi         # 4-byte Folded Reload
	movslq	%eax, %rbp
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%rdx,%rax,8), %rbx
	leaq	(%rdx,%rsi,8), %rax
	shlq	$4, %rsi
	addq	%rdx, %rsi
	.p2align	4, 0x90
.LBB9_5:                                # %.lr.ph452
                                        # =>This Inner Loop Header: Depth=1
	movupd	(%rdx,%rcx,8), %xmm3
	movupd	(%rax,%rcx,8), %xmm2
	movapd	%xmm3, %xmm4
	addpd	%xmm2, %xmm4
	subpd	%xmm2, %xmm3
	movupd	(%rsi,%rcx,8), %xmm2
	movupd	(%rbx,%rcx,8), %xmm5
	movapd	%xmm2, %xmm6
	addpd	%xmm5, %xmm6
	subpd	%xmm5, %xmm2
	movapd	%xmm2, %xmm5
	shufpd	$1, %xmm5, %xmm5        # xmm5 = xmm5[1,0]
	movapd	%xmm4, %xmm7
	addpd	%xmm6, %xmm7
	movupd	%xmm7, (%rdx,%rcx,8)
	movapd	%xmm6, %xmm7
	shufpd	$1, %xmm4, %xmm7        # xmm7 = xmm7[1],xmm4[0]
	shufpd	$1, %xmm6, %xmm4        # xmm4 = xmm4[1],xmm6[0]
	subpd	%xmm4, %xmm7
	movupd	%xmm7, (%rsi,%rcx,8)
	movapd	%xmm3, %xmm4
	subpd	%xmm5, %xmm4
	addpd	%xmm3, %xmm5
	movapd	%xmm5, %xmm6
	movsd	%xmm4, %xmm6            # xmm6 = xmm4[0],xmm6[1]
	shufpd	$1, %xmm4, %xmm5        # xmm5 = xmm5[1],xmm4[0]
	movapd	%xmm6, %xmm4
	subpd	%xmm5, %xmm4
	addpd	%xmm6, %xmm5
	movsd	%xmm4, %xmm5            # xmm5 = xmm4[0],xmm5[1]
	mulpd	%xmm1, %xmm5
	movupd	%xmm5, (%rax,%rcx,8)
	movapd	%xmm2, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	addsd	%xmm3, %xmm4
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	subsd	%xmm3, %xmm2
	movapd	%xmm2, %xmm3
	subsd	%xmm4, %xmm3
	mulsd	%xmm0, %xmm3
	movsd	%xmm3, (%rbx,%rcx,8)
	addsd	%xmm4, %xmm2
	mulsd	%xmm0, %xmm2
	movsd	%xmm2, 8(%rbx,%rcx,8)
	addq	$2, %rcx
	cmpq	%rbp, %rcx
	jl	.LBB9_5
.LBB9_6:                                # %._crit_edge453
	movq	-56(%rsp), %rax         # 8-byte Reload
	leal	(,%rax,8), %eax
	movl	%eax, -44(%rsp)         # 4-byte Spill
	cmpl	%edi, %eax
	jge	.LBB9_15
# BB#7:                                 # %.lr.ph448.preheader
	movslq	-44(%rsp), %r15         # 4-byte Folded Reload
	movslq	-56(%rsp), %rcx         # 4-byte Folded Reload
	leal	(%r8,%r8,2), %eax
	movl	%eax, -48(%rsp)         # 4-byte Spill
	movslq	%edi, %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movslq	%r8d, %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	leaq	(%rdx,%r15,8), %r12
	leaq	(%rcx,%rcx,2), %rdi
	leaq	(%rdi,%r15), %rax
	leaq	(%rdx,%rax,8), %r13
	leaq	(%r15,%rcx,2), %rax
	leaq	(%rdx,%rax,8), %rax
	leaq	(%rcx,%r15), %rsi
	leaq	(%rdx,%rsi,8), %rbx
	leaq	(%rdx,%rdi,8), %rdi
	movq	%rcx, %rbp
	shlq	$4, %rbp
	addq	%rdx, %rbp
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	leaq	(%rdx,%rcx,8), %r11
	xorl	%r14d, %r14d
	movaps	.LCPI9_0(%rip), %xmm8   # xmm8 = [-0.000000e+00,-0.000000e+00]
	xorl	%r9d, %r9d
	movq	%r15, %r8
	.p2align	4, 0x90
.LBB9_8:                                # %.lr.ph448
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_10 Depth 2
                                        #     Child Loop BB9_13 Depth 2
	leaq	2(%r9), %rcx
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	movq	-40(%rsp), %rcx         # 8-byte Reload
	movups	16(%rcx,%r9,8), %xmm5
	movaps	%xmm5, %xmm9
	movhlps	%xmm9, %xmm9            # xmm9 = xmm9[1,1]
	cmpl	$0, -56(%rsp)           # 4-byte Folded Reload
	jle	.LBB9_11
# BB#9:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB9_8 Depth=1
	movq	-24(%rsp), %rcx         # 8-byte Reload
	shlq	$4, %rcx
	movq	-40(%rsp), %rsi         # 8-byte Reload
	movsd	(%rsi,%rcx), %xmm11     # xmm11 = mem[0],zero
	movsd	8(%rsi,%rcx), %xmm10    # xmm10 = mem[0],zero
	movaps	%xmm9, %xmm0
	addsd	%xmm0, %xmm0
	movq	-32(%rsp), %rcx         # 8-byte Reload
	leaq	(%r8,%rcx), %rcx
	movapd	%xmm0, %xmm1
	mulsd	%xmm11, %xmm1
	subsd	%xmm10, %xmm1
	mulsd	%xmm10, %xmm0
	movapd	%xmm11, %xmm12
	subsd	%xmm0, %xmm12
	movlhps	%xmm11, %xmm11          # xmm11 = xmm11[0,0]
	movlhps	%xmm10, %xmm10          # xmm10 = xmm10[0,0]
	movlhps	%xmm12, %xmm12          # xmm12 = xmm12[0,0]
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movaps	%xmm5, %xmm7
	shufpd	$1, %xmm7, %xmm7        # xmm7 = xmm7[1,0]
	movq	%r14, %rsi
	.p2align	4, 0x90
.LBB9_10:                               # %.lr.ph
                                        #   Parent Loop BB9_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	(%r12,%rsi,8), %xmm4
	movupd	(%rbx,%rsi,8), %xmm0
	movapd	%xmm4, %xmm2
	addpd	%xmm0, %xmm2
	subpd	%xmm0, %xmm4
	movupd	(%rax,%rsi,8), %xmm6
	movupd	(%r13,%rsi,8), %xmm3
	movapd	%xmm6, %xmm0
	addpd	%xmm3, %xmm0
	subpd	%xmm3, %xmm6
	shufpd	$1, %xmm6, %xmm6        # xmm6 = xmm6[1,0]
	movapd	%xmm2, %xmm3
	addpd	%xmm0, %xmm3
	movupd	%xmm3, (%r12,%rsi,8)
	movapd	%xmm2, %xmm3
	subsd	%xmm0, %xmm3
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	subsd	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm7, %xmm2
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	mulpd	%xmm5, %xmm3
	movapd	%xmm3, %xmm0
	subpd	%xmm2, %xmm0
	addpd	%xmm2, %xmm3
	movsd	%xmm0, %xmm3            # xmm3 = xmm0[0],xmm3[1]
	movupd	%xmm3, (%rax,%rsi,8)
	movapd	%xmm4, %xmm2
	subpd	%xmm6, %xmm2
	addpd	%xmm4, %xmm6
	movapd	%xmm6, %xmm0
	movsd	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1]
	mulpd	%xmm11, %xmm0
	movapd	%xmm6, %xmm3
	shufpd	$1, %xmm2, %xmm3        # xmm3 = xmm3[1],xmm2[0]
	mulpd	%xmm10, %xmm3
	movapd	%xmm0, %xmm4
	subpd	%xmm3, %xmm4
	addpd	%xmm0, %xmm3
	movsd	%xmm4, %xmm3            # xmm3 = xmm4[0],xmm3[1]
	movupd	%xmm3, (%rbx,%rsi,8)
	movapd	%xmm2, %xmm0
	movsd	%xmm6, %xmm0            # xmm0 = xmm6[0],xmm0[1]
	mulpd	%xmm12, %xmm0
	shufpd	$1, %xmm6, %xmm2        # xmm2 = xmm2[1],xmm6[0]
	mulpd	%xmm1, %xmm2
	movapd	%xmm0, %xmm3
	subpd	%xmm2, %xmm3
	addpd	%xmm0, %xmm2
	movsd	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1]
	movupd	%xmm2, (%r13,%rsi,8)
	leaq	2(%r15,%rsi), %r10
	addq	$2, %rsi
	cmpq	%rcx, %r10
	jl	.LBB9_10
.LBB9_11:                               # %._crit_edge
                                        #   in Loop: Header=BB9_8 Depth=1
	cmpl	$0, -56(%rsp)           # 4-byte Folded Reload
	jle	.LBB9_14
# BB#12:                                # %.lr.ph443
                                        #   in Loop: Header=BB9_8 Depth=1
	leaq	4(%r9,%r9), %r9
	movslq	-48(%rsp), %rsi         # 4-byte Folded Reload
	movq	%r9, %rcx
	orq	$2, %rcx
	movq	-40(%rsp), %r10         # 8-byte Reload
	movsd	(%r10,%rcx,8), %xmm10   # xmm10 = mem[0],zero
	orq	$3, %r9
	movsd	(%r10,%r9,8), %xmm11    # xmm11 = mem[0],zero
	movapd	%xmm5, %xmm0
	addsd	%xmm0, %xmm0
	movq	-16(%rsp), %rcx         # 8-byte Reload
	leaq	(%r8,%rcx), %rcx
	addq	-32(%rsp), %rcx         # 8-byte Folded Reload
	movapd	%xmm0, %xmm6
	mulsd	%xmm10, %xmm6
	subsd	%xmm11, %xmm6
	mulsd	%xmm11, %xmm0
	movapd	%xmm10, %xmm12
	subsd	%xmm0, %xmm12
	xorps	%xmm8, %xmm9
	movlhps	%xmm10, %xmm10          # xmm10 = xmm10[0,0]
	movlhps	%xmm11, %xmm11          # xmm11 = xmm11[0,0]
	movlhps	%xmm12, %xmm12          # xmm12 = xmm12[0,0]
	movlhps	%xmm6, %xmm6            # xmm6 = xmm6[0,0]
	.p2align	4, 0x90
.LBB9_13:                               #   Parent Loop BB9_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	(%rdx,%rsi,8), %xmm2
	movupd	(%r11,%rsi,8), %xmm0
	movapd	%xmm2, %xmm7
	addpd	%xmm0, %xmm7
	subpd	%xmm0, %xmm2
	movupd	(%rbp,%rsi,8), %xmm4
	movupd	(%rdi,%rsi,8), %xmm3
	movapd	%xmm4, %xmm0
	addpd	%xmm3, %xmm0
	subpd	%xmm3, %xmm4
	shufpd	$1, %xmm4, %xmm4        # xmm4 = xmm4[1,0]
	movapd	%xmm7, %xmm3
	addpd	%xmm0, %xmm3
	movupd	%xmm3, (%rdx,%rsi,8)
	movapd	%xmm7, %xmm3
	subsd	%xmm0, %xmm3
	movhlps	%xmm7, %xmm7            # xmm7 = xmm7[1,1]
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	subsd	%xmm0, %xmm7
	movapd	%xmm5, %xmm0
	mulsd	%xmm3, %xmm0
	mulsd	%xmm9, %xmm3
	movapd	%xmm5, %xmm1
	mulsd	%xmm7, %xmm1
	subsd	%xmm1, %xmm3
	movsd	%xmm3, (%rbp,%rsi,8)
	mulsd	%xmm9, %xmm7
	addsd	%xmm0, %xmm7
	movsd	%xmm7, 8(%rbp,%rsi,8)
	movapd	%xmm2, %xmm7
	subpd	%xmm4, %xmm7
	addpd	%xmm2, %xmm4
	movapd	%xmm4, %xmm0
	movsd	%xmm7, %xmm0            # xmm0 = xmm7[0],xmm0[1]
	mulpd	%xmm10, %xmm0
	movapd	%xmm4, %xmm1
	shufpd	$1, %xmm7, %xmm1        # xmm1 = xmm1[1],xmm7[0]
	mulpd	%xmm11, %xmm1
	movapd	%xmm0, %xmm2
	subpd	%xmm1, %xmm2
	addpd	%xmm0, %xmm1
	movsd	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1]
	movupd	%xmm1, (%r11,%rsi,8)
	movapd	%xmm7, %xmm0
	movsd	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1]
	mulpd	%xmm12, %xmm0
	shufpd	$1, %xmm4, %xmm7        # xmm7 = xmm7[1],xmm4[0]
	mulpd	%xmm6, %xmm7
	movapd	%xmm0, %xmm1
	subpd	%xmm7, %xmm1
	addpd	%xmm0, %xmm7
	movsd	%xmm1, %xmm7            # xmm7 = xmm1[0],xmm7[1]
	movupd	%xmm7, (%rdi,%rsi,8)
	addq	$2, %rsi
	cmpq	%rcx, %rsi
	jl	.LBB9_13
.LBB9_14:                               # %._crit_edge444
                                        #   in Loop: Header=BB9_8 Depth=1
	addq	%r15, %r8
	movl	-48(%rsp), %ecx         # 4-byte Reload
	addl	-44(%rsp), %ecx         # 4-byte Folded Reload
	movl	%ecx, -48(%rsp)         # 4-byte Spill
	addq	%r15, %r14
	cmpq	-8(%rsp), %r8           # 8-byte Folded Reload
	movq	-24(%rsp), %r9          # 8-byte Reload
	jl	.LBB9_8
.LBB9_15:                               # %._crit_edge449
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	cftmdl, .Lfunc_end9-cftmdl
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"FFT sanity check failed! Difference is: %le\n"
	.size	.L.str, 45

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%e %e\n"
	.size	.L.str.1, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
