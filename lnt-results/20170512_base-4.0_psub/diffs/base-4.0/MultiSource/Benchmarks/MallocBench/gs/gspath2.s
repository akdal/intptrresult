	.text
	.file	"gspath2.bc"
	.globl	gs_flattenpath
	.p2align	4, 0x90
	.type	gs_flattenpath,@function
gs_flattenpath:                         # @gs_flattenpath
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$152, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 176
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	256(%rbx), %rdi
	xorl	%ebp, %ebp
	cmpl	$0, 112(%rdi)
	je	.LBB0_4
# BB#1:
	movss	440(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	leaq	8(%rsp), %rsi
	callq	gx_path_flatten
	testl	%eax, %eax
	js	.LBB0_2
# BB#3:
	movq	256(%rbx), %rdi
	callq	gx_path_release
	movq	256(%rbx), %rdi
	leaq	8(%rsp), %rsi
	movl	$144, %edx
	callq	memcpy
	jmp	.LBB0_4
.LBB0_2:
	movl	%eax, %ebp
.LBB0_4:
	movl	%ebp, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	gs_flattenpath, .Lfunc_end0-gs_flattenpath
	.cfi_endproc

	.globl	gs_reversepath
	.p2align	4, 0x90
	.type	gs_reversepath,@function
gs_reversepath:                         # @gs_reversepath
	.cfi_startproc
# BB#0:
	movl	$-21, %eax
	retq
.Lfunc_end1:
	.size	gs_reversepath, .Lfunc_end1-gs_reversepath
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4553139223271571456     # double 2.44140625E-4
	.text
	.globl	gs_pathbbox
	.p2align	4, 0x90
	.type	gs_pathbbox,@function
gs_pathbbox:                            # @gs_pathbbox
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	subq	$56, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 80
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	256(%rbx), %rdi
	leaq	24(%rsp), %rsi
	callq	gx_path_bbox
	testl	%eax, %eax
	js	.LBB2_2
# BB#1:
	cvtsi2sdq	24(%rsp), %xmm0
	movsd	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 8(%rsp)
	xorps	%xmm0, %xmm0
	cvtsi2sdq	32(%rsp), %xmm0
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 12(%rsp)
	xorps	%xmm0, %xmm0
	cvtsi2sdq	40(%rsp), %xmm0
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 16(%rsp)
	xorps	%xmm0, %xmm0
	cvtsi2sdq	48(%rsp), %xmm0
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 20(%rsp)
	addq	$24, %rbx
	leaq	8(%rsp), %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	gs_bbox_transform_inverse
.LBB2_2:
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	gs_pathbbox, .Lfunc_end2-gs_pathbbox
	.cfi_endproc

	.globl	gs_path_enum_init
	.p2align	4, 0x90
	.type	gs_path_enum_init,@function
gs_path_enum_init:                      # @gs_path_enum_init
	.cfi_startproc
# BB#0:
	movq	256(%rsi), %rax
	movq	88(%rax), %rax
	movq	%rax, (%rdi)
	movq	%rsi, 8(%rdi)
	retq
.Lfunc_end3:
	.size	gs_path_enum_init, .Lfunc_end3-gs_path_enum_init
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4553139223271571456     # double 2.44140625E-4
	.text
	.globl	gs_path_enum_next
	.p2align	4, 0x90
	.type	gs_path_enum_next,@function
gs_path_enum_next:                      # @gs_path_enum_next
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB4_7
# BB#1:
	movq	8(%rdi), %r15
	movq	8(%rbx), %rax
	movq	%rax, (%rdi)
	movl	$4, %eax
	cmpl	$2, 16(%rbx)
	je	.LBB4_8
# BB#2:
	cvtsi2sdq	24(%rbx), %xmm0
	movsd	.LCPI4_0(%rip), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	cvtsi2sdq	32(%rbx), %xmm1
	mulsd	%xmm2, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	cvtss2sd	%xmm1, %xmm1
	leaq	8(%rsp), %rsi
	movb	$2, %al
	movq	%r15, %rdi
	callq	gs_itransform
	testl	%eax, %eax
	js	.LBB4_8
# BB#3:
	movl	16(%rbx), %eax
	cmpl	$3, %eax
	je	.LBB4_9
# BB#4:
	cmpl	$1, %eax
	je	.LBB4_12
# BB#5:
	testl	%eax, %eax
	jne	.LBB4_14
# BB#6:
	movq	8(%rsp), %rax
	movq	%rax, (%r14)
	movl	$1, %eax
	jmp	.LBB4_8
.LBB4_9:
	xorps	%xmm0, %xmm0
	cvtsi2sdq	40(%rbx), %xmm0
	movsd	.LCPI4_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	mulsd	%xmm2, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdq	48(%rbx), %xmm1
	mulsd	%xmm2, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	cvtss2sd	%xmm1, %xmm1
	movb	$2, %al
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	gs_itransform
	testl	%eax, %eax
	js	.LBB4_7
# BB#10:
	xorps	%xmm0, %xmm0
	cvtsi2sdq	56(%rbx), %xmm0
	movsd	.LCPI4_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	mulsd	%xmm2, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdq	64(%rbx), %xmm1
	mulsd	%xmm2, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	cvtss2sd	%xmm1, %xmm1
	leaq	8(%r14), %rsi
	movb	$2, %al
	movq	%r15, %rdi
	callq	gs_itransform
	testl	%eax, %eax
	movl	$0, %eax
	js	.LBB4_8
# BB#11:
	movq	8(%rsp), %rax
	movq	%rax, 16(%r14)
	movl	$3, %eax
	jmp	.LBB4_8
.LBB4_7:
	xorl	%eax, %eax
.LBB4_8:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB4_12:
	movq	8(%rsp), %rax
	movq	%rax, (%r14)
	movl	$2, %eax
	jmp	.LBB4_8
.LBB4_14:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$125, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	16(%rbx), %edx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	gs_path_enum_next, .Lfunc_end4-gs_path_enum_next
	.cfi_endproc

	.globl	gs_clippath
	.p2align	4, 0x90
	.type	gs_clippath,@function
gs_clippath:                            # @gs_clippath
	.cfi_startproc
# BB#0:
	movq	256(%rdi), %rsi
	movq	264(%rdi), %rdi
	jmp	gx_path_copy            # TAILCALL
.Lfunc_end5:
	.size	gs_clippath, .Lfunc_end5-gs_clippath
	.cfi_endproc

	.globl	gs_initclip
	.p2align	4, 0x90
	.type	gs_initclip,@function
gs_initclip:                            # @gs_initclip
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 40
	subq	$152, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 192
.Lcfi22:
	.cfi_offset %rbx, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	448(%r14), %rax
	movq	(%rax), %rax
	movslq	24(%rax), %rbp
	shlq	$12, %rbp
	movslq	28(%rax), %rbx
	shlq	$12, %rbx
	leaq	8(%r14), %rsi
	leaq	8(%rsp), %r15
	movq	%r15, %rdi
	callq	gx_path_init
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rbp, %rcx
	movq	%rbx, %r8
	callq	gx_path_add_rectangle
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB6_1
# BB#2:
	movq	264(%r14), %rdi
	callq	gx_path_release
	leaq	64(%rsp), %rbp
	leaq	8(%rsp), %rdi
	movq	%rbp, %rsi
	callq	gx_path_is_rectangle
	testl	%eax, %eax
	jne	.LBB6_4
# BB#3:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
.LBB6_4:                                # %set_clip_path.exit.i
	leaq	24(%rsp), %rsi
	leaq	8(%rsp), %rbx
	movq	%rbx, %rdi
	callq	gx_path_bbox
	movq	264(%r14), %rdi
	movl	$144, %edx
	movq	%rbx, %rsi
	callq	memcpy
	movl	$-1, 272(%r14)
	xorl	%ebp, %ebp
	jmp	.LBB6_5
.LBB6_1:
	leaq	8(%rsp), %rdi
	callq	gx_path_release
.LBB6_5:                                # %gx_clip_to_rectangle.exit
	movl	%ebp, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	gs_initclip, .Lfunc_end6-gs_initclip
	.cfi_endproc

	.globl	gs_clip
	.p2align	4, 0x90
	.type	gs_clip,@function
gs_clip:                                # @gs_clip
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	subq	$152, %rsp
.Lcfi28:
	.cfi_def_cfa_offset 176
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	256(%rbx), %rdi
	movss	440(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	leaq	8(%rsp), %rsi
	callq	gx_path_flatten
	testl	%eax, %eax
	jne	.LBB7_4
# BB#1:
	leaq	64(%rsp), %r14
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	gx_path_is_rectangle
	testl	%eax, %eax
	jne	.LBB7_3
# BB#2:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r14)
	movups	%xmm0, (%r14)
.LBB7_3:                                # %set_clip_path.exit.i
	leaq	24(%rsp), %rsi
	leaq	8(%rsp), %r14
	movq	%r14, %rdi
	callq	gx_path_bbox
	movq	264(%rbx), %rdi
	movl	$144, %edx
	movq	%r14, %rsi
	callq	memcpy
	movl	$-1, 272(%rbx)
	xorl	%eax, %eax
.LBB7_4:                                # %common_clip.exit
	addq	$152, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	gs_clip, .Lfunc_end7-gs_clip
	.cfi_endproc

	.globl	common_clip
	.p2align	4, 0x90
	.type	common_clip,@function
common_clip:                            # @common_clip
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 32
	subq	$144, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 176
.Lcfi35:
	.cfi_offset %rbx, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	256(%rbx), %rdi
	movss	440(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movq	%rsp, %rsi
	callq	gx_path_flatten
	testl	%eax, %eax
	jne	.LBB8_4
# BB#1:
	leaq	56(%rsp), %r14
	movq	%rsp, %rdi
	movq	%r14, %rsi
	callq	gx_path_is_rectangle
	testl	%eax, %eax
	jne	.LBB8_3
# BB#2:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r14)
	movups	%xmm0, (%r14)
.LBB8_3:                                # %set_clip_path.exit
	leaq	16(%rsp), %rsi
	movq	%rsp, %r14
	movq	%r14, %rdi
	callq	gx_path_bbox
	movq	264(%rbx), %rdi
	movl	$144, %edx
	movq	%r14, %rsi
	callq	memcpy
	movl	%ebp, 272(%rbx)
	xorl	%eax, %eax
.LBB8_4:
	addq	$144, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end8:
	.size	common_clip, .Lfunc_end8-common_clip
	.cfi_endproc

	.globl	gs_eoclip
	.p2align	4, 0x90
	.type	gs_eoclip,@function
gs_eoclip:                              # @gs_eoclip
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 24
	subq	$152, %rsp
.Lcfi40:
	.cfi_def_cfa_offset 176
.Lcfi41:
	.cfi_offset %rbx, -24
.Lcfi42:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	256(%rbx), %rdi
	movss	440(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	leaq	8(%rsp), %rsi
	callq	gx_path_flatten
	testl	%eax, %eax
	jne	.LBB9_4
# BB#1:
	leaq	64(%rsp), %r14
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	gx_path_is_rectangle
	testl	%eax, %eax
	jne	.LBB9_3
# BB#2:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r14)
	movups	%xmm0, (%r14)
.LBB9_3:                                # %set_clip_path.exit.i
	leaq	24(%rsp), %rsi
	leaq	8(%rsp), %r14
	movq	%r14, %rdi
	callq	gx_path_bbox
	movq	264(%rbx), %rdi
	movl	$144, %edx
	movq	%r14, %rsi
	callq	memcpy
	movl	$1, 272(%rbx)
	xorl	%eax, %eax
.LBB9_4:                                # %common_clip.exit
	addq	$152, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	gs_eoclip, .Lfunc_end9-gs_eoclip
	.cfi_endproc

	.globl	gx_clip_to_rectangle
	.p2align	4, 0x90
	.type	gx_clip_to_rectangle,@function
gx_clip_to_rectangle:                   # @gx_clip_to_rectangle
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi46:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi49:
	.cfi_def_cfa_offset 208
.Lcfi50:
	.cfi_offset %rbx, -56
.Lcfi51:
	.cfi_offset %r12, -48
.Lcfi52:
	.cfi_offset %r13, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %r12
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	leaq	8(%r14), %rsi
	leaq	8(%rsp), %r13
	movq	%r13, %rdi
	callq	gx_path_init
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	%r12, %rcx
	movq	%r15, %r8
	callq	gx_path_add_rectangle
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB10_1
# BB#2:
	movq	264(%r14), %rdi
	callq	gx_path_release
	leaq	64(%rsp), %rbp
	leaq	8(%rsp), %rdi
	movq	%rbp, %rsi
	callq	gx_path_is_rectangle
	testl	%eax, %eax
	jne	.LBB10_4
# BB#3:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
.LBB10_4:                               # %set_clip_path.exit
	leaq	24(%rsp), %rsi
	leaq	8(%rsp), %rbx
	movq	%rbx, %rdi
	callq	gx_path_bbox
	movq	264(%r14), %rdi
	movl	$144, %edx
	movq	%rbx, %rsi
	callq	memcpy
	movl	$-1, 272(%r14)
	xorl	%ebp, %ebp
	jmp	.LBB10_5
.LBB10_1:
	leaq	8(%rsp), %rdi
	callq	gx_path_release
.LBB10_5:
	movl	%ebp, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	gx_clip_to_rectangle, .Lfunc_end10-gx_clip_to_rectangle
	.cfi_endproc

	.globl	set_clip_path
	.p2align	4, 0x90
	.type	set_clip_path,@function
set_clip_path:                          # @set_clip_path
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 48
.Lcfi61:
	.cfi_offset %rbx, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbp
	movq	%rdi, %r15
	leaq	56(%rbp), %rbx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	gx_path_is_rectangle
	testl	%eax, %eax
	jne	.LBB11_2
# BB#1:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
.LBB11_2:
	leaq	16(%rbp), %rsi
	movq	%rbp, %rdi
	callq	gx_path_bbox
	movq	264(%r15), %rdi
	movl	$144, %edx
	movq	%rbp, %rsi
	callq	memcpy
	movl	%r14d, 272(%r15)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	set_clip_path, .Lfunc_end11-set_clip_path
	.cfi_endproc

	.type	gs_path_enum_sizeof,@object # @gs_path_enum_sizeof
	.data
	.globl	gs_path_enum_sizeof
	.p2align	2
gs_path_enum_sizeof:
	.long	16                      # 0x10
	.size	gs_path_enum_sizeof, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s(%d): "
	.size	.L.str, 9

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Benchmarks/MallocBench/gs/gspath2.c"
	.size	.L.str.1, 86

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"bad type %x in gs_path_enum_next!\n"
	.size	.L.str.2, 35


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
