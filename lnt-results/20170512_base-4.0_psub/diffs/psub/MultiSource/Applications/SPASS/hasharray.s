	.text
	.file	"hasharray.bc"
	.globl	hsh_Create
	.p2align	4, 0x90
	.type	hsh_Create,@function
hsh_Create:                             # @hsh_Create
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$232, %edi
	callq	memory_Malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	$0, 224(%rax)
	popq	%rcx
	retq
.Lfunc_end0:
	.size	hsh_Create, .Lfunc_end0-hsh_Create
	.cfi_endproc

	.globl	hsh_Reset
	.p2align	4, 0x90
	.type	hsh_Reset,@function
hsh_Reset:                              # @hsh_Reset
	.cfi_startproc
# BB#0:
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB1_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_2 Depth 2
                                        #       Child Loop BB1_3 Depth 3
                                        #     Child Loop BB1_6 Depth 2
	movq	(%rdi,%r8,8), %r10
	testq	%r10, %r10
	je	.LBB1_7
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_3 Depth 3
	movq	8(%r10), %r9
	movq	(%r9), %rsi
	testq	%rsi, %rsi
	je	.LBB1_4
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph.i22
                                        #   Parent Loop BB1_1 Depth=1
                                        #     Parent Loop BB1_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB1_3
.LBB1_4:                                # %list_Delete.exit23
                                        #   in Loop: Header=BB1_2 Depth=2
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r9)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r9, (%rax)
	movq	(%r10), %r10
	testq	%r10, %r10
	jne	.LBB1_2
# BB#5:                                 # %._crit_edge
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	(%rdi,%r8,8), %rcx
	testq	%rcx, %rcx
	je	.LBB1_7
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph.i
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rax
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rcx, (%rdx)
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.LBB1_6
.LBB1_7:                                # %list_Delete.exit
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	$0, (%rdi,%r8,8)
	incq	%r8
	cmpq	$29, %r8
	jne	.LBB1_1
# BB#8:
	retq
.Lfunc_end1:
	.size	hsh_Reset, .Lfunc_end1-hsh_Reset
	.cfi_endproc

	.globl	hsh_Delete
	.p2align	4, 0x90
	.type	hsh_Delete,@function
hsh_Delete:                             # @hsh_Delete
	.cfi_startproc
# BB#0:
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB2_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_2 Depth 2
                                        #       Child Loop BB2_3 Depth 3
                                        #     Child Loop BB2_6 Depth 2
	movq	(%rdi,%r8,8), %r10
	testq	%r10, %r10
	je	.LBB2_7
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph.i
                                        #   Parent Loop BB2_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_3 Depth 3
	movq	8(%r10), %r9
	movq	(%r9), %rsi
	testq	%rsi, %rsi
	je	.LBB2_4
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph.i22.i
                                        #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB2_3
.LBB2_4:                                # %list_Delete.exit23.i
                                        #   in Loop: Header=BB2_2 Depth=2
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r9)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r9, (%rax)
	movq	(%r10), %r10
	testq	%r10, %r10
	jne	.LBB2_2
# BB#5:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB2_1 Depth=1
	movq	(%rdi,%r8,8), %rcx
	testq	%rcx, %rcx
	je	.LBB2_7
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph.i.i
                                        #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rax
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rcx, (%rdx)
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.LBB2_6
.LBB2_7:                                # %list_Delete.exit.i
                                        #   in Loop: Header=BB2_1 Depth=1
	movq	$0, (%rdi,%r8,8)
	incq	%r8
	cmpq	$29, %r8
	jne	.LBB2_1
# BB#8:                                 # %hsh_Reset.exit
	movq	memory_ARRAY+1856(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdi)
	movq	memory_ARRAY+1856(%rip), %rax
	movq	%rdi, (%rax)
	retq
.Lfunc_end2:
	.size	hsh_Delete, .Lfunc_end2-hsh_Delete
	.cfi_endproc

	.globl	hsh_GetAllEntries
	.p2align	4, 0x90
	.type	hsh_GetAllEntries,@function
hsh_GetAllEntries:                      # @hsh_GetAllEntries
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 48
.Lcfi6:
	.cfi_offset %rbx, -40
.Lcfi7:
	.cfi_offset %r12, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_3 Depth 2
                                        #       Child Loop BB3_7 Depth 3
	movq	(%r14,%r15,8), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_3
	jmp	.LBB3_10
	.p2align	4, 0x90
.LBB3_9:                                # %list_Nconc.exit
                                        #   in Loop: Header=BB3_3 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB3_10
.LBB3_3:                                # %.lr.ph
                                        #   Parent Loop BB3_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_7 Depth 3
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	callq	list_Copy
	testq	%r12, %r12
	je	.LBB3_4
# BB#5:                                 #   in Loop: Header=BB3_3 Depth=2
	testq	%rax, %rax
	je	.LBB3_9
# BB#6:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB3_3 Depth=2
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB3_7:                                # %.preheader.i
                                        #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_7
# BB#8:                                 #   in Loop: Header=BB3_3 Depth=2
	movq	%rax, (%rcx)
	jmp	.LBB3_9
	.p2align	4, 0x90
.LBB3_4:                                #   in Loop: Header=BB3_3 Depth=2
	movq	%rax, %r12
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_3
.LBB3_10:                               # %._crit_edge
                                        #   in Loop: Header=BB3_1 Depth=1
	incq	%r15
	cmpq	$29, %r15
	jne	.LBB3_1
# BB#11:
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	hsh_GetAllEntries, .Lfunc_end3-hsh_GetAllEntries
	.cfi_endproc

	.globl	hsh_Check
	.p2align	4, 0x90
	.type	hsh_Check,@function
hsh_Check:                              # @hsh_Check
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_3 Depth 2
                                        #       Child Loop BB4_4 Depth 3
	movq	(%rdi,%rax,8), %rcx
	testq	%rcx, %rcx
	jne	.LBB4_3
	jmp	.LBB4_6
	.p2align	4, 0x90
.LBB4_5:                                #   in Loop: Header=BB4_3 Depth=2
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB4_6
.LBB4_3:                                # %.lr.ph
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_4 Depth 3
	movq	8(%rcx), %rdx
	.p2align	4, 0x90
.LBB4_4:                                #   Parent Loop BB4_1 Depth=1
                                        #     Parent Loop BB4_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB4_4
	jmp	.LBB4_5
	.p2align	4, 0x90
.LBB4_6:                                # %._crit_edge
                                        #   in Loop: Header=BB4_1 Depth=1
	incq	%rax
	cmpq	$29, %rax
	jne	.LBB4_1
# BB#7:
	retq
.Lfunc_end4:
	.size	hsh_Check, .Lfunc_end4-hsh_Check
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
