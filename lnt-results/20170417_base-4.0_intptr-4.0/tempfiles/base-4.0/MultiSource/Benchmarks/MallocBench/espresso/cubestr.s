	.text
	.file	"cubestr.bc"
	.globl	cube_setup
	.p2align	4, 0x90
	.type	cube_setup,@function
cube_setup:                             # @cube_setup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	cube+8(%rip), %ebp
	testl	%ebp, %ebp
	js	.LBB0_2
# BB#1:
	movl	cube+4(%rip), %ebx
	cmpl	%ebp, %ebx
	jge	.LBB0_3
.LBB0_2:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	fatal
	movl	cube+4(%rip), %ebx
	movl	cube+8(%rip), %ebp
.LBB0_3:
	movl	%ebx, %eax
	subl	%ebp, %eax
	movl	%eax, cube+120(%rip)
	leal	-1(%rbx), %ecx
	testl	%eax, %eax
	movl	$-1, %eax
	cmovgl	%ecx, %eax
	movl	%eax, cube+124(%rip)
	movl	$0, cube(%rip)
	movslq	%ebx, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	(,%rax,4), %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, %r14
	movq	%r14, cube+16(%rip)
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, %r15
	movq	%r15, cube+24(%rip)
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, %r13
	movq	%r13, cube+40(%rip)
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, cube+48(%rip)
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB0_4
# BB#5:                                 # %.lr.ph73
	movq	cube+32(%rip), %rcx
	xorl	%edx, %edx
	jmp	.LBB0_6
	.p2align	4, 0x90
.LBB0_9:                                # %._crit_edge87
                                        #   in Loop: Header=BB0_6 Depth=1
	movl	cube+8(%rip), %ebp
.LBB0_6:                                # =>This Inner Loop Header: Depth=1
	movslq	%ebp, %rsi
	cmpq	%rsi, %rdx
	jge	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=1
	movl	$2, (%rcx,%rdx,4)
.LBB0_8:                                # %._crit_edge90
                                        #   in Loop: Header=BB0_6 Depth=1
	movl	cube(%rip), %esi
	movl	%esi, (%r14,%rdx,4)
	sarl	$5, %esi
	incl	%esi
	movl	%esi, (%r13,%rdx,4)
	movl	(%rcx,%rdx,4), %esi
	movl	%esi, %edi
	negl	%edi
	cmovll	%esi, %edi
	movl	cube(%rip), %esi
	leal	(%rdi,%rsi), %ebp
	movl	%ebp, cube(%rip)
	leal	-1(%rdi,%rsi), %esi
	movl	%esi, (%r15,%rdx,4)
	sarl	$5, %esi
	incl	%esi
	movl	%esi, (%rax,%rdx,4)
	incq	%rdx
	movslq	cube+4(%rip), %rbx
	cmpq	%rbx, %rdx
	jl	.LBB0_9
# BB#10:                                # %._crit_edge74.loopexit
	movl	cube(%rip), %r14d
	jmp	.LBB0_11
.LBB0_4:
	xorl	%r14d, %r14d
.LBB0_11:                               # %._crit_edge74
	movslq	%ebx, %rbx
	leaq	(,%rbx,8), %rdi
	callq	malloc
	movq	%rax, cube+72(%rip)
	shlq	$2, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, cube+112(%rip)
	cmpl	$33, %r14d
	jge	.LBB0_13
# BB#12:
	movl	$8, %edi
	jmp	.LBB0_14
.LBB0_13:
	leal	-1(%r14), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB0_14:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r14d, %esi
	callq	set_clear
	movq	%rax, cube+56(%rip)
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	jge	.LBB0_16
# BB#15:
	movl	$8, %edi
	jmp	.LBB0_17
.LBB0_16:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB0_17:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, cube+64(%rip)
	cmpl	$0, cube+4(%rip)
	jle	.LBB0_55
# BB#18:                                # %.lr.ph69.preheader
	movl	$1, %r14d
	movl	$2, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_19:                               # %.lr.ph69
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_22 Depth 2
                                        #     Child Loop BB0_48 Depth 2
                                        #     Child Loop BB0_52 Depth 2
                                        #     Child Loop BB0_33 Depth 2
                                        #     Child Loop BB0_37 Depth 2
	movl	cube(%rip), %r12d
	movl	$2, %eax
	cmpl	$33, %r12d
	jl	.LBB0_21
# BB#20:                                #   in Loop: Header=BB0_19 Depth=1
	leal	-1(%r12), %eax
	sarl	$5, %eax
	addl	$2, %eax
.LBB0_21:                               #   in Loop: Header=BB0_19 Depth=1
	movslq	%eax, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r12d, %esi
	callq	set_clear
	movq	cube+72(%rip), %rcx
	movq	%rax, (%rcx,%rbx,8)
	movq	cube+16(%rip), %rcx
	movl	(%rcx,%rbx,4), %ecx
	movq	cube+24(%rip), %rdx
	cmpl	(%rdx,%rbx,4), %ecx
	jg	.LBB0_23
	.p2align	4, 0x90
.LBB0_22:                               # %.lr.ph
                                        #   Parent Loop BB0_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %edi
	sarl	$5, %edi
	movslq	%edi, %rdi
	orl	%esi, 4(%rax,%rdi,4)
	cmpl	(%rdx,%rbx,4), %ecx
	leal	1(%rcx), %ecx
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	jl	.LBB0_22
.LBB0_23:                               # %._crit_edge
                                        #   in Loop: Header=BB0_19 Depth=1
	movslq	cube+8(%rip), %rcx
	cmpq	%rcx, %rbx
	jge	.LBB0_39
# BB#24:                                #   in Loop: Header=BB0_19 Depth=1
	movq	cube+56(%rip), %rcx
	movl	(%rcx), %esi
	movl	%esi, %edx
	andl	$1023, %edx             # imm = 0x3FF
	testw	$1023, %si              # imm = 0x3FF
	movq	%rdx, %r9
	cmoveq	%r14, %r9
	cmpq	$8, %r9
	jb	.LBB0_36
# BB#25:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_19 Depth=1
	movq	%r9, %r8
	andq	$1016, %r8              # imm = 0x3F8
	je	.LBB0_36
# BB#26:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_19 Depth=1
	leaq	1(%rdx), %rsi
	testl	%edx, %edx
	cmovneq	%r15, %rsi
	leaq	-4(%rcx,%rsi,4), %rdi
	leaq	4(%rax,%rdx,4), %rbp
	cmpq	%rbp, %rdi
	jae	.LBB0_28
# BB#27:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_19 Depth=1
	leaq	4(%rcx,%rdx,4), %rdi
	leaq	-4(%rax,%rsi,4), %rsi
	cmpq	%rdi, %rsi
	jb	.LBB0_36
.LBB0_28:                               # %vector.body.preheader
                                        #   in Loop: Header=BB0_19 Depth=1
	leaq	-8(%r8), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	btl	$3, %edi
	jb	.LBB0_29
# BB#30:                                # %vector.body.prol
                                        #   in Loop: Header=BB0_19 Depth=1
	movups	-12(%rcx,%rdx,4), %xmm0
	movups	-28(%rcx,%rdx,4), %xmm1
	movups	-12(%rax,%rdx,4), %xmm2
	movups	-28(%rax,%rdx,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rcx,%rdx,4)
	movups	%xmm3, -28(%rcx,%rdx,4)
	movl	$8, %ebp
	testq	%rsi, %rsi
	jne	.LBB0_32
	jmp	.LBB0_34
	.p2align	4, 0x90
.LBB0_39:                               #   in Loop: Header=BB0_19 Depth=1
	movq	cube+64(%rip), %rcx
	movl	(%rcx), %esi
	movl	%esi, %edx
	andl	$1023, %edx             # imm = 0x3FF
	testw	$1023, %si              # imm = 0x3FF
	movl	$1, %r9d
	cmovneq	%rdx, %r9
	cmpq	$8, %r9
	jb	.LBB0_51
# BB#40:                                # %min.iters.checked124
                                        #   in Loop: Header=BB0_19 Depth=1
	movq	%r9, %r8
	andq	$1016, %r8              # imm = 0x3F8
	je	.LBB0_51
# BB#41:                                # %vector.memcheck146
                                        #   in Loop: Header=BB0_19 Depth=1
	leaq	1(%rdx), %rsi
	testl	%edx, %edx
	cmovneq	%r15, %rsi
	leaq	-4(%rcx,%rsi,4), %rdi
	leaq	4(%rax,%rdx,4), %rbp
	cmpq	%rbp, %rdi
	jae	.LBB0_43
# BB#42:                                # %vector.memcheck146
                                        #   in Loop: Header=BB0_19 Depth=1
	leaq	4(%rcx,%rdx,4), %rdi
	leaq	-4(%rax,%rsi,4), %rsi
	cmpq	%rdi, %rsi
	jb	.LBB0_51
.LBB0_43:                               # %vector.body119.preheader
                                        #   in Loop: Header=BB0_19 Depth=1
	leaq	-8(%r8), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	btl	$3, %edi
	jb	.LBB0_44
# BB#45:                                # %vector.body119.prol
                                        #   in Loop: Header=BB0_19 Depth=1
	movups	-12(%rcx,%rdx,4), %xmm0
	movups	-28(%rcx,%rdx,4), %xmm1
	movups	-12(%rax,%rdx,4), %xmm2
	movups	-28(%rax,%rdx,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -12(%rcx,%rdx,4)
	movups	%xmm3, -28(%rcx,%rdx,4)
	movl	$8, %ebp
	testq	%rsi, %rsi
	jne	.LBB0_47
	jmp	.LBB0_49
.LBB0_29:                               #   in Loop: Header=BB0_19 Depth=1
	xorl	%ebp, %ebp
	testq	%rsi, %rsi
	je	.LBB0_34
.LBB0_32:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_19 Depth=1
	movq	%r9, %r10
	andq	$-8, %r10
	negq	%r10
	negq	%rbp
	leaq	-12(%rcx,%rdx,4), %rdi
	leaq	-12(%rax,%rdx,4), %rsi
	.p2align	4, 0x90
.LBB0_33:                               # %vector.body
                                        #   Parent Loop BB0_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rbp,4), %xmm0
	movups	-16(%rdi,%rbp,4), %xmm1
	movups	(%rsi,%rbp,4), %xmm2
	movups	-16(%rsi,%rbp,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rdi,%rbp,4)
	movups	%xmm3, -16(%rdi,%rbp,4)
	movups	-32(%rdi,%rbp,4), %xmm0
	movups	-48(%rdi,%rbp,4), %xmm1
	movups	-32(%rsi,%rbp,4), %xmm2
	movups	-48(%rsi,%rbp,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%rdi,%rbp,4)
	movups	%xmm3, -48(%rdi,%rbp,4)
	addq	$-16, %rbp
	cmpq	%rbp, %r10
	jne	.LBB0_33
.LBB0_34:                               # %middle.block
                                        #   in Loop: Header=BB0_19 Depth=1
	xorl	%esi, %esi
	cmpq	%r8, %r9
	je	.LBB0_54
# BB#35:                                #   in Loop: Header=BB0_19 Depth=1
	subq	%r8, %rdx
	.p2align	4, 0x90
.LBB0_36:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_19 Depth=1
	incq	%rdx
	.p2align	4, 0x90
.LBB0_37:                               # %scalar.ph
                                        #   Parent Loop BB0_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rax,%rdx,4), %esi
	orl	%esi, -4(%rcx,%rdx,4)
	decq	%rdx
	cmpq	$1, %rdx
	jg	.LBB0_37
# BB#38:                                #   in Loop: Header=BB0_19 Depth=1
	xorl	%esi, %esi
	jmp	.LBB0_54
.LBB0_44:                               #   in Loop: Header=BB0_19 Depth=1
	xorl	%ebp, %ebp
	testq	%rsi, %rsi
	je	.LBB0_49
.LBB0_47:                               # %vector.body119.preheader.new
                                        #   in Loop: Header=BB0_19 Depth=1
	movq	%r9, %r10
	andq	$-8, %r10
	negq	%r10
	negq	%rbp
	leaq	-12(%rcx,%rdx,4), %rdi
	leaq	-12(%rax,%rdx,4), %rsi
	.p2align	4, 0x90
.LBB0_48:                               # %vector.body119
                                        #   Parent Loop BB0_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rbp,4), %xmm0
	movups	-16(%rdi,%rbp,4), %xmm1
	movups	(%rsi,%rbp,4), %xmm2
	movups	-16(%rsi,%rbp,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rdi,%rbp,4)
	movups	%xmm3, -16(%rdi,%rbp,4)
	movups	-32(%rdi,%rbp,4), %xmm0
	movups	-48(%rdi,%rbp,4), %xmm1
	movups	-32(%rsi,%rbp,4), %xmm2
	movups	-48(%rsi,%rbp,4), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -32(%rdi,%rbp,4)
	movups	%xmm3, -48(%rdi,%rbp,4)
	addq	$-16, %rbp
	cmpq	%rbp, %r10
	jne	.LBB0_48
.LBB0_49:                               # %middle.block120
                                        #   in Loop: Header=BB0_19 Depth=1
	movl	$1, %esi
	cmpq	%r8, %r9
	je	.LBB0_54
# BB#50:                                #   in Loop: Header=BB0_19 Depth=1
	subq	%r8, %rdx
	.p2align	4, 0x90
.LBB0_51:                               # %scalar.ph121.preheader
                                        #   in Loop: Header=BB0_19 Depth=1
	incq	%rdx
	.p2align	4, 0x90
.LBB0_52:                               # %scalar.ph121
                                        #   Parent Loop BB0_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rax,%rdx,4), %esi
	orl	%esi, -4(%rcx,%rdx,4)
	decq	%rdx
	cmpq	$1, %rdx
	jg	.LBB0_52
# BB#53:                                #   in Loop: Header=BB0_19 Depth=1
	movl	$1, %esi
.LBB0_54:                               # %.loopexit
                                        #   in Loop: Header=BB0_19 Depth=1
	movq	cube+112(%rip), %rax
	movl	%esi, (%rax,%rbx,4)
	incq	%rbx
	movslq	cube+4(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_19
.LBB0_55:                               # %._crit_edge70
	movslq	cube+8(%rip), %rax
	testq	%rax, %rax
	je	.LBB0_56
# BB#57:
	movq	cube+48(%rip), %rcx
	movslq	-4(%rcx,%rax,4), %rax
	movl	%eax, cube+108(%rip)
	movq	cube+56(%rip), %rcx
	movl	$1431655765, %edx       # imm = 0x55555555
	andl	(%rcx,%rax,4), %edx
	movl	%edx, cube+104(%rip)
	jmp	.LBB0_58
.LBB0_56:
	movl	$-1, cube+108(%rip)
.LBB0_58:
	movl	$80, %edi
	callq	malloc
	movq	%rax, cube+80(%rip)
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	setl	%al
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_59:                               # =>This Inner Loop Header: Depth=1
	movl	$2, %ecx
	testb	$1, %al
	jne	.LBB0_61
# BB#60:                                #   in Loop: Header=BB0_59 Depth=1
	leal	-1(%rbx), %ecx
	sarl	$5, %ecx
	addl	$2, %ecx
.LBB0_61:                               #   in Loop: Header=BB0_59 Depth=1
	movslq	%ecx, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	cube+80(%rip), %rcx
	movq	%rax, (%rcx,%rbp,8)
	incq	%rbp
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	setl	%al
	cmpq	$10, %rbp
	jne	.LBB0_59
# BB#62:
	cmpl	$33, %ebx
	jge	.LBB0_64
# BB#63:
	movl	$8, %edi
	jmp	.LBB0_65
.LBB0_64:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB0_65:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, %rcx
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	set_fill
	movq	%rax, cube+88(%rip)
	movl	cube(%rip), %ebx
	cmpl	$33, %ebx
	jge	.LBB0_67
# BB#66:
	movl	$8, %edi
	jmp	.LBB0_68
.LBB0_67:
	leal	-1(%rbx), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB0_68:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebx, %esi
	callq	set_clear
	movq	%rax, cube+96(%rip)
	movslq	cube(%rip), %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, cdata(%rip)
	movslq	cube+4(%rip), %rbx
	shlq	$2, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, cdata+8(%rip)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, cdata+16(%rip)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, cdata+24(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	cube_setup, .Lfunc_end0-cube_setup
	.cfi_endproc

	.globl	setdown_cube
	.p2align	4, 0x90
	.type	setdown_cube,@function
setdown_cube:                           # @setdown_cube
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	cube+16(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	callq	free
	movq	$0, cube+16(%rip)
.LBB1_2:
	movq	cube+24(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#3:
	callq	free
	movq	$0, cube+24(%rip)
.LBB1_4:
	movq	cube+40(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_6
# BB#5:
	callq	free
	movq	$0, cube+40(%rip)
.LBB1_6:
	movq	cube+48(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_8
# BB#7:
	callq	free
	movq	$0, cube+48(%rip)
.LBB1_8:
	movq	cube+112(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_10
# BB#9:
	callq	free
	movq	$0, cube+112(%rip)
.LBB1_10:
	movq	cube+56(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_12
# BB#11:
	callq	free
	movq	$0, cube+56(%rip)
.LBB1_12:
	movq	cube+64(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_14
# BB#13:
	callq	free
	movq	$0, cube+64(%rip)
.LBB1_14:
	movq	cube+88(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_16
# BB#15:
	callq	free
	movq	$0, cube+88(%rip)
.LBB1_16:
	movq	cube+96(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_18
# BB#17:
	callq	free
	movq	$0, cube+96(%rip)
.LBB1_18:                               # %.preheader14
	movl	cube+4(%rip), %ecx
	movq	cube+72(%rip), %rdi
	testl	%ecx, %ecx
	jle	.LBB1_23
# BB#19:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_20:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB1_22
# BB#21:                                #   in Loop: Header=BB1_20 Depth=1
	movq	%rax, %rdi
	callq	free
	movq	cube+72(%rip), %rax
	movq	$0, (%rax,%rbx,8)
	movl	cube+4(%rip), %ecx
	movq	cube+72(%rip), %rdi
.LBB1_22:                               #   in Loop: Header=BB1_20 Depth=1
	incq	%rbx
	movslq	%ecx, %rax
	cmpq	%rax, %rbx
	jl	.LBB1_20
.LBB1_23:                               # %._crit_edge
	testq	%rdi, %rdi
	je	.LBB1_25
# BB#24:
	callq	free
	movq	$0, cube+72(%rip)
.LBB1_25:                               # %.preheader
	movq	cube+80(%rip), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.LBB1_27
# BB#26:
	movq	%rax, %rdi
	callq	free
	movq	cube+80(%rip), %rax
	movq	$0, (%rax)
	movq	cube+80(%rip), %rdi
.LBB1_27:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.LBB1_29
# BB#28:
	movq	%rax, %rdi
	callq	free
	movq	cube+80(%rip), %rax
	movq	$0, 8(%rax)
	movq	cube+80(%rip), %rdi
.LBB1_29:
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB1_31
# BB#30:
	movq	%rax, %rdi
	callq	free
	movq	cube+80(%rip), %rax
	movq	$0, 16(%rax)
	movq	cube+80(%rip), %rdi
.LBB1_31:
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.LBB1_33
# BB#32:
	movq	%rax, %rdi
	callq	free
	movq	cube+80(%rip), %rax
	movq	$0, 24(%rax)
	movq	cube+80(%rip), %rdi
.LBB1_33:
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.LBB1_35
# BB#34:
	movq	%rax, %rdi
	callq	free
	movq	cube+80(%rip), %rax
	movq	$0, 32(%rax)
	movq	cube+80(%rip), %rdi
.LBB1_35:
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.LBB1_37
# BB#36:
	movq	%rax, %rdi
	callq	free
	movq	cube+80(%rip), %rax
	movq	$0, 40(%rax)
	movq	cube+80(%rip), %rdi
.LBB1_37:
	movq	48(%rdi), %rax
	testq	%rax, %rax
	je	.LBB1_39
# BB#38:
	movq	%rax, %rdi
	callq	free
	movq	cube+80(%rip), %rax
	movq	$0, 48(%rax)
	movq	cube+80(%rip), %rdi
.LBB1_39:
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.LBB1_41
# BB#40:
	movq	%rax, %rdi
	callq	free
	movq	cube+80(%rip), %rax
	movq	$0, 56(%rax)
	movq	cube+80(%rip), %rdi
.LBB1_41:
	movq	64(%rdi), %rax
	testq	%rax, %rax
	je	.LBB1_43
# BB#42:
	movq	%rax, %rdi
	callq	free
	movq	cube+80(%rip), %rax
	movq	$0, 64(%rax)
	movq	cube+80(%rip), %rdi
.LBB1_43:
	movq	72(%rdi), %rax
	testq	%rax, %rax
	je	.LBB1_45
# BB#44:
	movq	%rax, %rdi
	callq	free
	movq	cube+80(%rip), %rax
	movq	$0, 72(%rax)
	movq	cube+80(%rip), %rdi
.LBB1_45:
	testq	%rdi, %rdi
	je	.LBB1_47
# BB#46:
	callq	free
	movq	$0, cube+80(%rip)
.LBB1_47:
	movq	cdata(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_49
# BB#48:
	callq	free
	movq	$0, cdata(%rip)
.LBB1_49:
	movq	cdata+8(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_51
# BB#50:
	callq	free
	movq	$0, cdata+8(%rip)
.LBB1_51:
	movq	cdata+16(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_53
# BB#52:
	callq	free
	movq	$0, cdata+16(%rip)
.LBB1_53:
	movq	cdata+24(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_55
# BB#54:
	callq	free
	movq	$0, cdata+24(%rip)
.LBB1_55:
	movq	$0, cube+112(%rip)
	xorps	%xmm0, %xmm0
	movups	%xmm0, cube+16(%rip)
	movups	%xmm0, cube+88(%rip)
	movups	%xmm0, cube+72(%rip)
	movups	%xmm0, cube+56(%rip)
	movups	%xmm0, cube+40(%rip)
	movups	%xmm0, cdata+16(%rip)
	movups	%xmm0, cdata(%rip)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	setdown_cube, .Lfunc_end1-setdown_cube
	.cfi_endproc

	.globl	save_cube_struct
	.p2align	4, 0x90
	.type	save_cube_struct,@function
save_cube_struct:                       # @save_cube_struct
	.cfi_startproc
# BB#0:
	movups	cube+112(%rip), %xmm0
	movups	%xmm0, temp_cube_save+112(%rip)
	movups	cube+96(%rip), %xmm0
	movups	%xmm0, temp_cube_save+96(%rip)
	movups	cube+80(%rip), %xmm0
	movups	%xmm0, temp_cube_save+80(%rip)
	movups	cube+64(%rip), %xmm0
	movups	%xmm0, temp_cube_save+64(%rip)
	movups	cube+48(%rip), %xmm0
	movups	%xmm0, temp_cube_save+48(%rip)
	movups	cube+32(%rip), %xmm0
	movups	%xmm0, temp_cube_save+32(%rip)
	movups	cube+16(%rip), %xmm0
	movups	%xmm0, temp_cube_save+16(%rip)
	movups	cube(%rip), %xmm0
	movups	%xmm0, temp_cube_save(%rip)
	movups	cdata+32(%rip), %xmm0
	movups	%xmm0, temp_cdata_save+32(%rip)
	movups	cdata+16(%rip), %xmm0
	movups	%xmm0, temp_cdata_save+16(%rip)
	movups	cdata(%rip), %xmm0
	movups	%xmm0, temp_cdata_save(%rip)
	xorps	%xmm0, %xmm0
	movups	%xmm0, cube+80(%rip)
	movups	%xmm0, cube+64(%rip)
	movups	%xmm0, cube+48(%rip)
	movups	%xmm0, cube+32(%rip)
	movups	%xmm0, cube+16(%rip)
	movq	$0, cube+96(%rip)
	movups	%xmm0, cdata+16(%rip)
	movups	%xmm0, cdata(%rip)
	retq
.Lfunc_end2:
	.size	save_cube_struct, .Lfunc_end2-save_cube_struct
	.cfi_endproc

	.globl	restore_cube_struct
	.p2align	4, 0x90
	.type	restore_cube_struct,@function
restore_cube_struct:                    # @restore_cube_struct
	.cfi_startproc
# BB#0:
	movups	temp_cube_save+112(%rip), %xmm0
	movups	%xmm0, cube+112(%rip)
	movups	temp_cube_save+96(%rip), %xmm0
	movups	%xmm0, cube+96(%rip)
	movups	temp_cube_save+80(%rip), %xmm0
	movups	%xmm0, cube+80(%rip)
	movups	temp_cube_save+64(%rip), %xmm0
	movups	%xmm0, cube+64(%rip)
	movups	temp_cube_save+48(%rip), %xmm0
	movups	%xmm0, cube+48(%rip)
	movups	temp_cube_save+32(%rip), %xmm0
	movups	%xmm0, cube+32(%rip)
	movups	temp_cube_save+16(%rip), %xmm0
	movups	%xmm0, cube+16(%rip)
	movups	temp_cube_save(%rip), %xmm0
	movups	%xmm0, cube(%rip)
	movups	temp_cdata_save+32(%rip), %xmm0
	movups	%xmm0, cdata+32(%rip)
	movups	temp_cdata_save+16(%rip), %xmm0
	movups	%xmm0, cdata+16(%rip)
	movups	temp_cdata_save(%rip), %xmm0
	movups	%xmm0, cdata(%rip)
	retq
.Lfunc_end3:
	.size	restore_cube_struct, .Lfunc_end3-restore_cube_struct
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"cube size is silly, error in .i/.o or .mv"
	.size	.L.str, 42


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
