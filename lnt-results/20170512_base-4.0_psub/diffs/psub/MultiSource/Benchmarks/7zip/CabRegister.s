	.text
	.file	"CabRegister.bc"
	.p2align	4, 0x90
	.type	_ZL9CreateArcv,@function
_ZL9CreateArcv:                         # @_ZL9CreateArcv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$144, %edi
	callq	_Znwm
	movl	$0, 8(%rax)
	movq	$_ZTVN8NArchive4NCab8CHandlerE+16, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rax)
	movq	$8, 40(%rax)
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE+16, 16(%rax)
	movups	%xmm0, 56(%rax)
	movq	$8, 72(%rax)
	movq	$_ZTV13CRecordVectorIN8NArchive4NCab7CMvItemEE+16, 48(%rax)
	movups	%xmm0, 88(%rax)
	movq	$4, 104(%rax)
	movq	$_ZTV13CRecordVectorIiE+16, 80(%rax)
	movups	%xmm0, 120(%rax)
	movq	$4, 136(%rax)
	movq	$_ZTV13CRecordVectorIiE+16, 112(%rax)
	popq	%rcx
	retq
.Lfunc_end0:
	.size	_ZL9CreateArcv, .Lfunc_end0-_ZL9CreateArcv
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev,@function
_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev: # @_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 32
.Lcfi4:
	.cfi_offset %rbx, -24
.Lcfi5:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE+16, (%rbx)
.Ltmp0:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp1:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB2_2:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp4:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_4:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev, .Lfunc_end2-_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED0Ev,@function
_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED0Ev: # @_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE+16, (%rbx)
.Ltmp6:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp7:
# BB#1:
.Ltmp12:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp13:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_5:
.Ltmp14:
	movq	%rax, %r14
	jmp	.LBB3_6
.LBB3_3:
.Ltmp8:
	movq	%rax, %r14
.Ltmp9:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp10:
.LBB3_6:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_4:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED0Ev, .Lfunc_end3-_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6DeleteEii
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 64
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB4_8
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB4_7
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_5
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	(%rdi), %rax
.Ltmp15:
	callq	*16(%rax)
.Ltmp16:
.LBB4_5:                                # %_ZN9CMyComPtrI9IInStreamED2Ev.exit.i
                                        #   in Loop: Header=BB4_2 Depth=1
.Ltmp21:
	movq	%rbp, %rdi
	callq	_ZN8NArchive4NCab9CDatabaseD2Ev
.Ltmp22:
# BB#6:                                 # %_ZN8NArchive4NCab11CDatabaseExD2Ev.exit
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB4_7:                                #   in Loop: Header=BB4_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB4_2
.LBB4_8:                                # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB4_9:
.Ltmp17:
	movq	%rax, %rbx
.Ltmp18:
	movq	%rbp, %rdi
	callq	_ZN8NArchive4NCab9CDatabaseD2Ev
.Ltmp19:
	jmp	.LBB4_12
.LBB4_10:
.Ltmp20:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB4_11:
.Ltmp23:
	movq	%rax, %rbx
.LBB4_12:                               # %.body
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6DeleteEii, .Lfunc_end4-_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin2   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin2   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp18-.Ltmp22         #   Call between .Ltmp22 and .Ltmp18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin2   #     jumps to .Ltmp20
	.byte	1                       #   On action: 1
	.long	.Ltmp19-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Lfunc_end4-.Ltmp19     #   Call between .Ltmp19 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NCab9CDatabaseD2Ev,"axG",@progbits,_ZN8NArchive4NCab9CDatabaseD2Ev,comdat
	.weak	_ZN8NArchive4NCab9CDatabaseD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab9CDatabaseD2Ev,@function
_ZN8NArchive4NCab9CDatabaseD2Ev:        # @_ZN8NArchive4NCab9CDatabaseD2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -32
.Lcfi28:
	.cfi_offset %r14, -24
.Lcfi29:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	%r15, %rbx
	subq	$-128, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE+16, 128(%r15)
.Ltmp24:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp25:
# BB#1:
.Ltmp30:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp31:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev.exit
	leaq	96(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE+16, 96(%r15)
.Ltmp42:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp43:
# BB#3:
.Ltmp48:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp49:
# BB#4:                                 # %_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev.exit
	movq	72(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_6
# BB#5:
	callq	_ZdaPv
.LBB5_6:                                # %_ZN11CStringBaseIcED2Ev.exit.i.i
	movq	56(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_8
# BB#7:
	callq	_ZdaPv
.LBB5_8:                                # %_ZN8NArchive4NCab13COtherArchiveD2Ev.exit.i
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_10
# BB#9:
	callq	_ZdaPv
.LBB5_10:                               # %_ZN11CStringBaseIcED2Ev.exit.i2.i
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_16
# BB#11:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZdaPv                  # TAILCALL
.LBB5_16:                               # %_ZN8NArchive4NCab12CArchiveInfoD2Ev.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB5_22:
.Ltmp50:
	movq	%rax, %r14
	jmp	.LBB5_23
.LBB5_14:
.Ltmp44:
	movq	%rax, %r14
.Ltmp45:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp46:
	jmp	.LBB5_23
.LBB5_15:
.Ltmp47:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB5_17:
.Ltmp32:
	movq	%rax, %r14
	jmp	.LBB5_18
.LBB5_12:
.Ltmp26:
	movq	%rax, %r14
.Ltmp27:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp28:
.LBB5_18:                               # %.body
	leaq	96(%r15), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE+16, 96(%r15)
.Ltmp33:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp34:
# BB#19:
.Ltmp39:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp40:
.LBB5_23:                               # %_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev.exit7
	movq	72(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_25
# BB#24:
	callq	_ZdaPv
.LBB5_25:                               # %_ZN11CStringBaseIcED2Ev.exit.i.i8
	movq	56(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_27
# BB#26:
	callq	_ZdaPv
.LBB5_27:                               # %_ZN8NArchive4NCab13COtherArchiveD2Ev.exit.i9
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_29
# BB#28:
	callq	_ZdaPv
.LBB5_29:                               # %_ZN11CStringBaseIcED2Ev.exit.i2.i10
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_31
# BB#30:
	callq	_ZdaPv
.LBB5_31:                               # %_ZN8NArchive4NCab12CArchiveInfoD2Ev.exit11
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_13:
.Ltmp29:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB5_32:
.Ltmp41:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB5_20:
.Ltmp35:
	movq	%rax, %r14
.Ltmp36:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp37:
# BB#33:                                # %.body5
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB5_21:
.Ltmp38:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN8NArchive4NCab9CDatabaseD2Ev, .Lfunc_end5-_ZN8NArchive4NCab9CDatabaseD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp24-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin3   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin3   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin3   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin3   #     jumps to .Ltmp50
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin3   #     jumps to .Ltmp47
	.byte	1                       #   On action: 1
	.long	.Ltmp27-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin3   #     jumps to .Ltmp29
	.byte	1                       #   On action: 1
	.long	.Ltmp33-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin3   #     jumps to .Ltmp35
	.byte	1                       #   On action: 1
	.long	.Ltmp39-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin3   #     jumps to .Ltmp41
	.byte	1                       #   On action: 1
	.long	.Ltmp40-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Ltmp36-.Ltmp40         #   Call between .Ltmp40 and .Ltmp36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin3   # >> Call Site 10 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin3   #     jumps to .Ltmp38
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev,@function
_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev: # @_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -24
.Lcfi34:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE+16, (%rbx)
.Ltmp51:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp52:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB6_2:
.Ltmp53:
	movq	%rax, %r14
.Ltmp54:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp55:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_4:
.Ltmp56:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev, .Lfunc_end6-_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp51-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin4   #     jumps to .Ltmp53
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp54-.Ltmp52         #   Call between .Ltmp52 and .Ltmp54
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin4   #     jumps to .Ltmp56
	.byte	1                       #   On action: 1
	.long	.Ltmp55-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Lfunc_end6-.Ltmp55     #   Call between .Ltmp55 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev,@function
_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev: # @_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE+16, (%rbx)
.Ltmp57:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp58:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB7_2:
.Ltmp59:
	movq	%rax, %r14
.Ltmp60:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp61:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB7_4:
.Ltmp62:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev, .Lfunc_end7-_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp57-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin5   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp60-.Ltmp58         #   Call between .Ltmp58 and .Ltmp60
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin5   #     jumps to .Ltmp62
	.byte	1                       #   On action: 1
	.long	.Ltmp61-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Lfunc_end7-.Ltmp61     #   Call between .Ltmp61 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab5CItemEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab5CItemEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab5CItemEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab5CItemEED0Ev,@function
_ZN13CObjectVectorIN8NArchive4NCab5CItemEED0Ev: # @_ZN13CObjectVectorIN8NArchive4NCab5CItemEED0Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -24
.Lcfi44:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE+16, (%rbx)
.Ltmp63:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp64:
# BB#1:
.Ltmp69:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp70:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB8_5:
.Ltmp71:
	movq	%rax, %r14
	jmp	.LBB8_6
.LBB8_3:
.Ltmp65:
	movq	%rax, %r14
.Ltmp66:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp67:
.LBB8_6:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_4:
.Ltmp68:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN13CObjectVectorIN8NArchive4NCab5CItemEED0Ev, .Lfunc_end8-_ZN13CObjectVectorIN8NArchive4NCab5CItemEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp63-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin6   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp70-.Ltmp69         #   Call between .Ltmp69 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin6   #     jumps to .Ltmp71
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin6   #     jumps to .Ltmp68
	.byte	1                       #   On action: 1
	.long	.Ltmp67-.Lfunc_begin6   # >> Call Site 4 <<
	.long	.Lfunc_end8-.Ltmp67     #   Call between .Ltmp67 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab5CItemEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab5CItemEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab5CItemEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab5CItemEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive4NCab5CItemEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive4NCab5CItemEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 64
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB9_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB9_6
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_5
# BB#4:                                 #   in Loop: Header=BB9_2 Depth=1
	callq	_ZdaPv
.LBB9_5:                                # %_ZN8NArchive4NCab5CItemD2Ev.exit
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB9_6:                                #   in Loop: Header=BB9_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB9_2
.LBB9_7:                                # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end9:
	.size	_ZN13CObjectVectorIN8NArchive4NCab5CItemEE6DeleteEii, .Lfunc_end9-_ZN13CObjectVectorIN8NArchive4NCab5CItemEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab7CFolderEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED0Ev,@function
_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED0Ev: # @_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 32
.Lcfi61:
	.cfi_offset %rbx, -24
.Lcfi62:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE+16, (%rbx)
.Ltmp72:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp73:
# BB#1:
.Ltmp78:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp79:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB10_5:
.Ltmp80:
	movq	%rax, %r14
	jmp	.LBB10_6
.LBB10_3:
.Ltmp74:
	movq	%rax, %r14
.Ltmp75:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp76:
.LBB10_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB10_4:
.Ltmp77:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end10:
	.size	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED0Ev, .Lfunc_end10-_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp72-.Lfunc_begin7   # >> Call Site 1 <<
	.long	.Ltmp73-.Ltmp72         #   Call between .Ltmp72 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin7   #     jumps to .Ltmp74
	.byte	0                       #   On action: cleanup
	.long	.Ltmp78-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Ltmp79-.Ltmp78         #   Call between .Ltmp78 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin7   #     jumps to .Ltmp80
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin7   # >> Call Site 3 <<
	.long	.Ltmp76-.Ltmp75         #   Call between .Ltmp75 and .Ltmp76
	.long	.Ltmp77-.Lfunc_begin7   #     jumps to .Ltmp77
	.byte	1                       #   On action: 1
	.long	.Ltmp76-.Lfunc_begin7   # >> Call Site 4 <<
	.long	.Lfunc_end10-.Ltmp76    #   Call between .Ltmp76 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NCab7CFolderEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NCab7CFolderEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive4NCab7CFolderEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive4NCab7CFolderEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi66:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi67:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi69:
	.cfi_def_cfa_offset 64
.Lcfi70:
	.cfi_offset %rbx, -56
.Lcfi71:
	.cfi_offset %r12, -48
.Lcfi72:
	.cfi_offset %r13, -40
.Lcfi73:
	.cfi_offset %r14, -32
.Lcfi74:
	.cfi_offset %r15, -24
.Lcfi75:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%esi, %r14d
	movq	%rdi, %r12
	leal	(%rdx,%r14), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	subl	%r14d, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB11_5
# BB#1:                                 # %.lr.ph
	movslq	%r14d, %rbp
	movslq	%r15d, %r13
	shlq	$3, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbp, %rax
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB11_4
# BB#3:                                 #   in Loop: Header=BB11_2 Depth=1
	callq	_ZdlPv
.LBB11_4:                               #   in Loop: Header=BB11_2 Depth=1
	incq	%rbx
	cmpq	%r13, %rbx
	jl	.LBB11_2
.LBB11_5:                               # %._crit_edge
	movq	%r12, %rdi
	movl	%r14d, %esi
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end11:
	.size	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEE6DeleteEii, .Lfunc_end11-_ZN13CObjectVectorIN8NArchive4NCab7CFolderEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIN8NArchive4NCab7CMvItemEED0Ev,"axG",@progbits,_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEED0Ev,comdat
	.weak	_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEED0Ev,@function
_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEED0Ev: # @_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEED0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 32
.Lcfi79:
	.cfi_offset %rbx, -24
.Lcfi80:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp81:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp82:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB12_2:
.Ltmp83:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEED0Ev, .Lfunc_end12-_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp81-.Lfunc_begin8   # >> Call Site 1 <<
	.long	.Ltmp82-.Ltmp81         #   Call between .Ltmp81 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin8   #     jumps to .Ltmp83
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin8   # >> Call Site 2 <<
	.long	.Lfunc_end12-.Ltmp82    #   Call between .Ltmp82 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIiED0Ev,"axG",@progbits,_ZN13CRecordVectorIiED0Ev,comdat
	.weak	_ZN13CRecordVectorIiED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIiED0Ev,@function
_ZN13CRecordVectorIiED0Ev:              # @_ZN13CRecordVectorIiED0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi83:
	.cfi_def_cfa_offset 32
.Lcfi84:
	.cfi_offset %rbx, -24
.Lcfi85:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp84:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp85:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB13_2:
.Ltmp86:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end13:
	.size	_ZN13CRecordVectorIiED0Ev, .Lfunc_end13-_ZN13CRecordVectorIiED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp84-.Lfunc_begin9   # >> Call Site 1 <<
	.long	.Ltmp85-.Ltmp84         #   Call between .Ltmp84 and .Ltmp85
	.long	.Ltmp86-.Lfunc_begin9   #     jumps to .Ltmp86
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin9   # >> Call Site 2 <<
	.long	.Lfunc_end13-.Ltmp85    #   Call between .Ltmp85 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_CabRegister.ii,@function
_GLOBAL__sub_I_CabRegister.ii:          # @_GLOBAL__sub_I_CabRegister.ii
	.cfi_startproc
# BB#0:
	movl	$_ZL9g_ArcInfo, %edi
	jmp	_Z11RegisterArcPK8CArcInfo # TAILCALL
.Lfunc_end14:
	.size	_GLOBAL__sub_I_CabRegister.ii, .Lfunc_end14-_GLOBAL__sub_I_CabRegister.ii
	.cfi_endproc

	.type	_ZL9g_ArcInfo,@object   # @_ZL9g_ArcInfo
	.data
	.p2align	3
_ZL9g_ArcInfo:
	.quad	.L.str
	.quad	.L.str.1
	.quad	0
	.byte	8                       # 0x8
	.asciz	"MSCF\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.zero	3
	.long	4                       # 0x4
	.byte	0                       # 0x0
	.zero	3
	.quad	_ZL9CreateArcv
	.quad	0
	.size	_ZL9g_ArcInfo, 80

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	67                      # 0x43
	.long	97                      # 0x61
	.long	98                      # 0x62
	.long	0                       # 0x0
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
	.p2align	2
.L.str.1:
	.long	99                      # 0x63
	.long	97                      # 0x61
	.long	98                      # 0x62
	.long	0                       # 0x0
	.size	.L.str.1, 16

	.type	_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE,@object # @_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive4NCab11CDatabaseExEE
	.quad	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NCab11CDatabaseExEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive4NCab11CDatabaseExEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive4NCab11CDatabaseExEE,@object # @_ZTS13CObjectVectorIN8NArchive4NCab11CDatabaseExEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive4NCab11CDatabaseExEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive4NCab11CDatabaseExEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive4NCab11CDatabaseExEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive4NCab11CDatabaseExEE:
	.asciz	"13CObjectVectorIN8NArchive4NCab11CDatabaseExEE"
	.size	_ZTS13CObjectVectorIN8NArchive4NCab11CDatabaseExEE, 47

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN8NArchive4NCab11CDatabaseExEE,@object # @_ZTI13CObjectVectorIN8NArchive4NCab11CDatabaseExEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive4NCab11CDatabaseExEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive4NCab11CDatabaseExEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive4NCab11CDatabaseExEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive4NCab11CDatabaseExEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive4NCab11CDatabaseExEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive4NCab11CDatabaseExEE, 24

	.type	_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE,@object # @_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive4NCab5CItemEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive4NCab5CItemEE
	.quad	_ZN13CObjectVectorIN8NArchive4NCab5CItemEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NCab5CItemEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NCab5CItemEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive4NCab5CItemEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive4NCab5CItemEE,@object # @_ZTS13CObjectVectorIN8NArchive4NCab5CItemEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive4NCab5CItemEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive4NCab5CItemEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive4NCab5CItemEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive4NCab5CItemEE:
	.asciz	"13CObjectVectorIN8NArchive4NCab5CItemEE"
	.size	_ZTS13CObjectVectorIN8NArchive4NCab5CItemEE, 40

	.type	_ZTI13CObjectVectorIN8NArchive4NCab5CItemEE,@object # @_ZTI13CObjectVectorIN8NArchive4NCab5CItemEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive4NCab5CItemEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive4NCab5CItemEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive4NCab5CItemEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive4NCab5CItemEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive4NCab5CItemEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive4NCab5CItemEE, 24

	.type	_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE,@object # @_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive4NCab7CFolderEE
	.quad	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NCab7CFolderEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive4NCab7CFolderEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive4NCab7CFolderEE,@object # @_ZTS13CObjectVectorIN8NArchive4NCab7CFolderEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive4NCab7CFolderEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive4NCab7CFolderEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive4NCab7CFolderEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive4NCab7CFolderEE:
	.asciz	"13CObjectVectorIN8NArchive4NCab7CFolderEE"
	.size	_ZTS13CObjectVectorIN8NArchive4NCab7CFolderEE, 42

	.type	_ZTI13CObjectVectorIN8NArchive4NCab7CFolderEE,@object # @_ZTI13CObjectVectorIN8NArchive4NCab7CFolderEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive4NCab7CFolderEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive4NCab7CFolderEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive4NCab7CFolderEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive4NCab7CFolderEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive4NCab7CFolderEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive4NCab7CFolderEE, 24

	.type	_ZTV13CRecordVectorIN8NArchive4NCab7CMvItemEE,@object # @_ZTV13CRecordVectorIN8NArchive4NCab7CMvItemEE
	.section	.rodata._ZTV13CRecordVectorIN8NArchive4NCab7CMvItemEE,"aG",@progbits,_ZTV13CRecordVectorIN8NArchive4NCab7CMvItemEE,comdat
	.weak	_ZTV13CRecordVectorIN8NArchive4NCab7CMvItemEE
	.p2align	3
_ZTV13CRecordVectorIN8NArchive4NCab7CMvItemEE:
	.quad	0
	.quad	_ZTI13CRecordVectorIN8NArchive4NCab7CMvItemEE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIN8NArchive4NCab7CMvItemEE, 40

	.type	_ZTS13CRecordVectorIN8NArchive4NCab7CMvItemEE,@object # @_ZTS13CRecordVectorIN8NArchive4NCab7CMvItemEE
	.section	.rodata._ZTS13CRecordVectorIN8NArchive4NCab7CMvItemEE,"aG",@progbits,_ZTS13CRecordVectorIN8NArchive4NCab7CMvItemEE,comdat
	.weak	_ZTS13CRecordVectorIN8NArchive4NCab7CMvItemEE
	.p2align	4
_ZTS13CRecordVectorIN8NArchive4NCab7CMvItemEE:
	.asciz	"13CRecordVectorIN8NArchive4NCab7CMvItemEE"
	.size	_ZTS13CRecordVectorIN8NArchive4NCab7CMvItemEE, 42

	.type	_ZTI13CRecordVectorIN8NArchive4NCab7CMvItemEE,@object # @_ZTI13CRecordVectorIN8NArchive4NCab7CMvItemEE
	.section	.rodata._ZTI13CRecordVectorIN8NArchive4NCab7CMvItemEE,"aG",@progbits,_ZTI13CRecordVectorIN8NArchive4NCab7CMvItemEE,comdat
	.weak	_ZTI13CRecordVectorIN8NArchive4NCab7CMvItemEE
	.p2align	4
_ZTI13CRecordVectorIN8NArchive4NCab7CMvItemEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIN8NArchive4NCab7CMvItemEE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIN8NArchive4NCab7CMvItemEE, 24

	.type	_ZTV13CRecordVectorIiE,@object # @_ZTV13CRecordVectorIiE
	.section	.rodata._ZTV13CRecordVectorIiE,"aG",@progbits,_ZTV13CRecordVectorIiE,comdat
	.weak	_ZTV13CRecordVectorIiE
	.p2align	3
_ZTV13CRecordVectorIiE:
	.quad	0
	.quad	_ZTI13CRecordVectorIiE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIiED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIiE, 40

	.type	_ZTS13CRecordVectorIiE,@object # @_ZTS13CRecordVectorIiE
	.section	.rodata._ZTS13CRecordVectorIiE,"aG",@progbits,_ZTS13CRecordVectorIiE,comdat
	.weak	_ZTS13CRecordVectorIiE
	.p2align	4
_ZTS13CRecordVectorIiE:
	.asciz	"13CRecordVectorIiE"
	.size	_ZTS13CRecordVectorIiE, 19

	.type	_ZTI13CRecordVectorIiE,@object # @_ZTI13CRecordVectorIiE
	.section	.rodata._ZTI13CRecordVectorIiE,"aG",@progbits,_ZTI13CRecordVectorIiE,comdat
	.weak	_ZTI13CRecordVectorIiE
	.p2align	4
_ZTI13CRecordVectorIiE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIiE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIiE, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_CabRegister.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
