	.text
	.file	"CalculateXS.bc"
	.globl	calculate_micro_xs
	.p2align	4, 0x90
	.type	calculate_micro_xs,@function
calculate_micro_xs:                     # @calculate_micro_xs
	.cfi_startproc
# BB#0:
	movq	8(%rsp), %r10
	movslq	%r9d, %rsi
	shlq	$4, %rsi
	movq	8(%rcx,%rsi), %rcx
	movslq	%edi, %rsi
	movslq	(%rcx,%rsi,4), %rdi
	decq	%rdx
	leaq	-1(%rdi), %rax
	cmpq	%rdx, %rdi
	movq	(%r8,%rsi,8), %rcx
	cmovneq	%rdi, %rax
	leaq	(%rax,%rax,2), %rdx
	shlq	$4, %rdx
	movsd	48(%rcx,%rdx), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	subsd	%xmm0, %xmm2
	subsd	(%rcx,%rdx), %xmm1
	divsd	%xmm1, %xmm2
	movupd	56(%rcx,%rdx), %xmm0
	movupd	8(%rcx,%rdx), %xmm1
	movapd	%xmm0, %xmm3
	subpd	%xmm1, %xmm3
	movapd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm1, %xmm3
	subpd	%xmm3, %xmm0
	movupd	%xmm0, (%r10)
	movupd	24(%rcx,%rdx), %xmm0
	movupd	72(%rcx,%rdx), %xmm3
	movapd	%xmm3, %xmm4
	subpd	%xmm0, %xmm4
	mulpd	%xmm1, %xmm4
	subpd	%xmm4, %xmm3
	movupd	%xmm3, 16(%r10)
	movsd	88(%rcx,%rdx), %xmm0    # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	subsd	40(%rcx,%rdx), %xmm1
	mulsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, 32(%r10)
	retq
.Lfunc_end0:
	.size	calculate_micro_xs, .Lfunc_end0-calculate_micro_xs
	.cfi_endproc

	.globl	calculate_macro_xs
	.p2align	4, 0x90
	.type	calculate_macro_xs,@function
calculate_macro_xs:                     # @calculate_macro_xs
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	48(%rsp), %r10
	xorpd	%xmm1, %xmm1
	movupd	%xmm1, 16(%r10)
	movupd	%xmm1, (%r10)
	movq	$0, 32(%r10)
	imulq	%rdx, %rsi
	decq	%rsi
	cmpq	$2, %rsi
	jl	.LBB1_1
# BB#2:                                 # %.lr.ph.i.preheader
	xorl	%ebx, %ebx
	movq	%rsi, %r14
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %r11
	shrq	%rsi
	leaq	(%rsi,%r11), %rbx
	movq	%rbx, %rax
	shlq	$4, %rax
	movsd	(%r9,%rax), %xmm1       # xmm1 = mem[0],zero
	movq	%r14, %rax
	subq	%rbx, %rax
	ucomisd	%xmm0, %xmm1
	cmovaq	%rbx, %r14
	cmovaq	%r11, %rbx
	cmovbeq	%rax, %rsi
	cmpq	$1, %rsi
	jg	.LBB1_3
# BB#4:                                 # %grid_search.exit.loopexit
	movslq	%ebx, %rsi
	jmp	.LBB1_5
.LBB1_1:
	xorl	%esi, %esi
.LBB1_5:                                # %grid_search.exit
	movslq	%edi, %rax
	movslq	(%rcx,%rax,4), %r15
	testq	%r15, %r15
	jle	.LBB1_9
# BB#6:                                 # %.lr.ph
	movq	40(%rsp), %rcx
	movq	32(%rsp), %r11
	movq	(%rcx,%rax,8), %r14
	movq	(%r8,%rax,8), %r8
	shlq	$4, %rsi
	movq	8(%r9,%rsi), %r9
	decq	%rdx
	xorpd	%xmm10, %xmm10
	xorpd	%xmm9, %xmm9
	xorl	%edi, %edi
	xorpd	%xmm11, %xmm11
	.p2align	4, 0x90
.LBB1_7:                                # =>This Inner Loop Header: Depth=1
	movslq	(%r14,%rdi,4), %rcx
	movslq	(%r9,%rcx,4), %rax
	leaq	-1(%rax), %rsi
	cmpq	%rdx, %rax
	movq	(%r11,%rcx,8), %rbx
	cmovneq	%rax, %rsi
	leaq	(%rsi,%rsi,2), %rcx
	shlq	$4, %rcx
	movsd	48(%rbx,%rcx), %xmm4    # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm5
	subsd	%xmm0, %xmm5
	subsd	(%rbx,%rcx), %xmm4
	divsd	%xmm4, %xmm5
	movupd	56(%rbx,%rcx), %xmm7
	movupd	8(%rbx,%rcx), %xmm6
	movupd	24(%rbx,%rcx), %xmm8
	movapd	%xmm7, %xmm4
	subpd	%xmm6, %xmm4
	movsd	88(%rbx,%rcx), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm6
	subsd	40(%rbx,%rcx), %xmm6
	mulsd	%xmm5, %xmm6
	movlhps	%xmm5, %xmm5            # xmm5 = xmm5[0,0]
	mulpd	%xmm5, %xmm4
	subpd	%xmm4, %xmm7
	movupd	72(%rbx,%rcx), %xmm2
	movapd	%xmm2, %xmm4
	subpd	%xmm8, %xmm4
	movsd	(%r8,%rdi,8), %xmm3     # xmm3 = mem[0],zero
	mulpd	%xmm5, %xmm4
	subpd	%xmm4, %xmm2
	subsd	%xmm6, %xmm1
	mulsd	%xmm3, %xmm1
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	mulpd	%xmm3, %xmm7
	addpd	%xmm7, %xmm10
	mulpd	%xmm2, %xmm3
	addpd	%xmm3, %xmm11
	addsd	%xmm1, %xmm9
	incq	%rdi
	cmpq	%r15, %rdi
	jl	.LBB1_7
# BB#8:                                 # %._crit_edge.loopexit
	movupd	%xmm10, (%r10)
	movupd	%xmm11, 16(%r10)
	movsd	%xmm9, 32(%r10)
.LBB1_9:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	calculate_macro_xs, .Lfunc_end1-calculate_macro_xs
	.cfi_endproc

	.globl	grid_search
	.p2align	4, 0x90
	.type	grid_search,@function
grid_search:                            # @grid_search
	.cfi_startproc
# BB#0:
	decq	%rdi
	xorl	%eax, %eax
	cmpq	$2, %rdi
	jl	.LBB2_3
# BB#1:                                 # %.lr.ph.preheader
	movq	%rdi, %r8
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	shrq	%rdi
	leaq	(%rdi,%rax), %rcx
	movq	%rcx, %rdx
	shlq	$4, %rdx
	movsd	(%rsi,%rdx), %xmm1      # xmm1 = mem[0],zero
	movq	%r8, %rdx
	subq	%rcx, %rdx
	ucomisd	%xmm0, %xmm1
	cmovaq	%rcx, %r8
	cmovbeq	%rcx, %rax
	cmovbeq	%rdx, %rdi
	cmpq	$1, %rdi
	jg	.LBB2_2
.LBB2_3:                                # %._crit_edge
	retq
.Lfunc_end2:
	.size	grid_search, .Lfunc_end2-grid_search
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
