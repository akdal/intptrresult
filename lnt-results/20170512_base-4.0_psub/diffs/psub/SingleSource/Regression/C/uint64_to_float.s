	.text
	.file	"uint64_to_float.bc"
	.globl	floatundisf
	.p2align	4, 0x90
	.type	floatundisf,@function
floatundisf:                            # @floatundisf
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB0_1
# BB#2:
	bsrq	%rdi, %rcx
	xorl	$63, %ecx
	movl	$64, %edx
	subl	%ecx, %edx
	movl	$63, %eax
	subl	%ecx, %eax
	cmpl	$25, %edx
	jb	.LBB0_8
# BB#3:
	movl	%edx, %ecx
	andb	$127, %cl
	cmpb	$26, %cl
	je	.LBB0_7
# BB#4:
	cmpb	$25, %cl
	jne	.LBB0_6
# BB#5:
	addq	%rdi, %rdi
	jmp	.LBB0_7
.LBB0_1:
	xorps	%xmm0, %xmm0
	retq
.LBB0_8:
	movl	$24, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rdi
	movq	%rdi, %rcx
	jmp	.LBB0_9
.LBB0_6:
	leal	-26(%rdx), %ecx
	movq	%rdi, %r8
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrq	%cl, %r8
	movl	$90, %ecx
	subl	%edx, %ecx
	movq	$-1, %rsi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrq	%cl, %rsi
	xorl	%ecx, %ecx
	testq	%rdi, %rsi
	setne	%cl
	orq	%r8, %rcx
	movq	%rcx, %rdi
.LBB0_7:
	movl	%edi, %ecx
	shrl	$2, %ecx
	andl	$1, %ecx
	orq	%rdi, %rcx
	incq	%rcx
	movq	%rcx, %rsi
	shrq	$2, %rsi
	shrq	$3, %rcx
	movq	%rsi, %rdi
	andq	$16777216, %rdi         # imm = 0x1000000
	cmoveq	%rsi, %rcx
	testq	%rdi, %rdi
	cmovnel	%edx, %eax
.LBB0_9:
	shll	$23, %eax
	andl	$8388607, %ecx          # imm = 0x7FFFFF
	leal	1065353216(%rax,%rcx), %eax
	movd	%eax, %xmm0
	retq
.Lfunc_end0:
	.size	floatundisf, .Lfunc_end0-floatundisf
	.cfi_endproc

	.globl	test
	.p2align	4, 0x90
	.type	test,@function
test:                                   # @test
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 48
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rdi, %rdx
	testq	%rdx, %rdx
	je	.LBB1_1
# BB#2:
	bsrq	%rdx, %rcx
	xorl	$63, %ecx
	movl	$64, %esi
	subl	%ecx, %esi
	movl	$63, %eax
	subl	%ecx, %eax
	cmpl	$25, %esi
	jb	.LBB1_8
# BB#3:
	movl	%esi, %ebx
	andb	$127, %bl
	cmpb	$26, %bl
	movq	%rdx, %rcx
	je	.LBB1_7
# BB#4:
	cmpb	$25, %bl
	jne	.LBB1_6
# BB#5:
	leaq	(%rdx,%rdx), %rcx
	jmp	.LBB1_7
.LBB1_1:
	xorpd	%xmm0, %xmm0
	xorl	%ebx, %ebx
	testq	%rdx, %rdx
	jns	.LBB1_12
	jmp	.LBB1_11
.LBB1_8:
	movl	$24, %ecx
	subl	%esi, %ecx
	movq	%rdx, %rdi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rdi
	jmp	.LBB1_9
.LBB1_6:
	leal	-26(%rsi), %ecx
	movq	%rdx, %rdi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrq	%cl, %rdi
	movl	$90, %ecx
	subl	%esi, %ecx
	movq	$-1, %rbp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrq	%cl, %rbp
	xorl	%ecx, %ecx
	testq	%rdx, %rbp
	setne	%cl
	orq	%rdi, %rcx
.LBB1_7:
	movl	%ecx, %edi
	shrl	$2, %edi
	andl	$1, %edi
	orq	%rcx, %rdi
	incq	%rdi
	movq	%rdi, %rcx
	shrq	$2, %rcx
	shrq	$3, %rdi
	movq	%rcx, %rbp
	andq	$16777216, %rbp         # imm = 0x1000000
	cmoveq	%rcx, %rdi
	testq	%rbp, %rbp
	cmovnel	%esi, %eax
.LBB1_9:
	shll	$23, %eax
	andl	$8388607, %edi          # imm = 0x7FFFFF
	leal	1065353216(%rax,%rdi), %ebx
	movd	%ebx, %xmm0
	cvtss2sd	%xmm0, %xmm0
	testq	%rdx, %rdx
	js	.LBB1_11
.LBB1_12:                               # %floatundisf.exit
	cvtsi2ssq	%rdx, %xmm1
	jmp	.LBB1_13
.LBB1_11:
	movq	%rdx, %rax
	shrq	%rax
	movl	%edx, %ecx
	andl	$1, %ecx
	orq	%rax, %rcx
	cvtsi2ssq	%rcx, %xmm1
	addss	%xmm1, %xmm1
.LBB1_13:                               # %floatundisf.exit
	movd	%xmm1, %ebp
	cmpl	%ebp, %ebx
	jne	.LBB1_15
# BB#14:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB1_15:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%rdx, %rsi
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	callq	printf
	movl	$.L.str.1, %edi
	movb	$1, %al
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movl	%ebx, %esi
	callq	printf
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.2, %edi
	movb	$1, %al
	movl	%ebp, %esi
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	jmp	printf                  # TAILCALL
.Lfunc_end1:
	.size	test, .Lfunc_end1-test
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi11:
	.cfi_def_cfa_offset 192
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_1:                                # %test.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_2 Depth 2
                                        #       Child Loop BB2_4 Depth 3
                                        #         Child Loop BB2_6 Depth 4
                                        #           Child Loop BB2_8 Depth 5
	xorl	%edi, %edi
	callq	fesetround
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	.Lmain.modeNames(,%rbx,8), %rsi
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_2:                                #   Parent Loop BB2_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_4 Depth 3
                                        #         Child Loop BB2_6 Depth 4
                                        #           Child Loop BB2_8 Depth 5
	movl	$1, %edi
	movl	%ebx, %ecx
	shlq	%cl, %rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	callq	test
	movq	$-1, %rdi
	movl	%ebx, %ecx
	shlq	%cl, %rdi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	callq	test
	testq	%rbx, %rbx
	jle	.LBB2_11
# BB#3:                                 # %.lr.ph135.preheader
                                        #   in Loop: Header=BB2_2 Depth=2
	xorl	%ebp, %ebp
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph135
                                        #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_2 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_6 Depth 4
                                        #           Child Loop BB2_8 Depth 5
	movl	$1, %r15d
	movl	%ebp, %ecx
	shlq	%cl, %r15
	movq	32(%rsp), %r14          # 8-byte Reload
	leaq	(%r15,%r14), %rdi
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	callq	test
	movq	$-1, %rbx
	movl	%ebp, %ecx
	shlq	%cl, %rbx
	leaq	(%rbx,%r14), %rdi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	callq	test
	movq	24(%rsp), %r14          # 8-byte Reload
	addq	%r14, %r15
	movq	%r15, 72(%rsp)          # 8-byte Spill
	movq	%r15, %rdi
	callq	test
	addq	%r14, %rbx
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	test
	testq	%rbp, %rbp
	jle	.LBB2_10
# BB#5:                                 # %.lr.ph131.preheader
                                        #   in Loop: Header=BB2_4 Depth=3
	xorl	%ebx, %ebx
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph131
                                        #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_2 Depth=2
                                        #       Parent Loop BB2_4 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB2_8 Depth 5
	movl	$1, %ebp
	movl	%ebx, %ecx
	shlq	%cl, %rbp
	movq	64(%rsp), %r14          # 8-byte Reload
	leaq	(%rbp,%r14), %r13
	movq	%r13, %rdi
	callq	test
	movq	$-1, %r15
	movl	%ebx, %ecx
	shlq	%cl, %r15
	leaq	(%r15,%r14), %rdi
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	callq	test
	movq	48(%rsp), %r14          # 8-byte Reload
	leaq	(%rbp,%r14), %rdi
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	callq	test
	leaq	(%r15,%r14), %rdi
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	callq	test
	movq	72(%rsp), %r14          # 8-byte Reload
	leaq	(%rbp,%r14), %rdi
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	callq	test
	leaq	(%r15,%r14), %rdi
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	callq	test
	movq	56(%rsp), %r14          # 8-byte Reload
	addq	%r14, %rbp
	movq	%rbp, 128(%rsp)         # 8-byte Spill
	movq	%rbp, %rdi
	callq	test
	addq	%r14, %r15
	movq	%r15, %rdi
	callq	test
	testq	%rbx, %rbx
	jle	.LBB2_9
# BB#7:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_6 Depth=4
	xorl	%r14d, %r14d
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_8:                                # %.lr.ph
                                        #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_2 Depth=2
                                        #       Parent Loop BB2_4 Depth=3
                                        #         Parent Loop BB2_6 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	$1, %r12d
	movl	%r14d, %ecx
	shlq	%cl, %r12
	leaq	(%r12,%r13), %rdi
	callq	test
	movq	$-1, %rbp
	movl	%r14d, %ecx
	shlq	%cl, %rbp
	leaq	(%rbp,%r13), %rdi
	callq	test
	movq	120(%rsp), %rbx         # 8-byte Reload
	leaq	(%r12,%rbx), %rdi
	callq	test
	leaq	(%rbp,%rbx), %rdi
	callq	test
	movq	112(%rsp), %rbx         # 8-byte Reload
	leaq	(%r12,%rbx), %rdi
	callq	test
	leaq	(%rbp,%rbx), %rdi
	callq	test
	movq	104(%rsp), %rbx         # 8-byte Reload
	leaq	(%r12,%rbx), %rdi
	callq	test
	leaq	(%rbp,%rbx), %rdi
	callq	test
	movq	96(%rsp), %rbx          # 8-byte Reload
	leaq	(%r12,%rbx), %rdi
	callq	test
	leaq	(%rbp,%rbx), %rdi
	callq	test
	movq	88(%rsp), %rbx          # 8-byte Reload
	leaq	(%r12,%rbx), %rdi
	callq	test
	leaq	(%rbp,%rbx), %rdi
	callq	test
	movq	%r15, %rbx
	movq	%r13, %r15
	movq	128(%rsp), %r13         # 8-byte Reload
	leaq	(%r12,%r13), %rdi
	callq	test
	leaq	(%rbp,%r13), %rdi
	movq	%r15, %r13
	movq	%rbx, %r15
	callq	test
	addq	%r15, %r12
	movq	%r12, %rdi
	movq	80(%rsp), %rbx          # 8-byte Reload
	callq	test
	addq	%r15, %rbp
	movq	%rbp, %rdi
	callq	test
	incq	%r14
	cmpq	%r14, %rbx
	jne	.LBB2_8
.LBB2_9:                                # %._crit_edge
                                        #   in Loop: Header=BB2_6 Depth=4
	incq	%rbx
	movq	40(%rsp), %rbp          # 8-byte Reload
	cmpq	%rbp, %rbx
	jne	.LBB2_6
.LBB2_10:                               # %._crit_edge132
                                        #   in Loop: Header=BB2_4 Depth=3
	incq	%rbp
	movq	16(%rsp), %rbx          # 8-byte Reload
	cmpq	%rbx, %rbp
	jne	.LBB2_4
.LBB2_11:                               # %._crit_edge136
                                        #   in Loop: Header=BB2_2 Depth=2
	incq	%rbx
	cmpq	$64, %rbx
	jne	.LBB2_2
# BB#12:                                #   in Loop: Header=BB2_1 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	incq	%rbx
	cmpq	$4, %rbx
	jne	.LBB2_1
# BB#13:
	movl	$.Lstr, %edi
	callq	puts
	xorl	%eax, %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Error detected @ 0x%016llx\n"
	.size	.L.str, 28

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\tExpected result: %a (0x%08x)\n"
	.size	.L.str.1, 31

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\tObserved result: %a (0x%08x)\n"
	.size	.L.str.2, 31

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"to nearest"
	.size	.L.str.3, 11

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"down"
	.size	.L.str.4, 5

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"up"
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"towards zero"
	.size	.L.str.6, 13

	.type	.Lmain.modeNames,@object # @main.modeNames
	.section	.rodata,"a",@progbits
	.p2align	4
.Lmain.modeNames:
	.quad	.L.str.3
	.quad	.L.str.4
	.quad	.L.str.5
	.quad	.L.str.6
	.size	.Lmain.modeNames, 32

	.type	.L.str.7,@object        # @.str.7
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.7:
	.asciz	"Testing uint64_t --> float conversions in round %s...\n"
	.size	.L.str.7, 55

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Finished Testing."
	.size	.Lstr, 18


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
