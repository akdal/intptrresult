	.text
	.file	"psqrt.bc"
	.globl	psqrt
	.p2align	4, 0x90
	.type	psqrt,@function
psqrt:                                  # @psqrt
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 64
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movq	$0, 8(%rsp)
	movq	$0, 16(%rsp)
	testq	%r12, %r12
	je	.LBB0_2
# BB#1:
	incw	(%r12)
.LBB0_2:
	movq	%r12, %rdi
	callq	pcmpz
	testl	%eax, %eax
	je	.LBB0_6
# BB#3:
	js	.LBB0_7
# BB#4:
	leaq	8(%rsp), %r14
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	psetq
	leaq	16(%rsp), %r15
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rsi
	movq	%r15, %rdi
	callq	psetq
	movq	8(%rsp), %rbx
	movq	$-1, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	pdivmod
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	padd
	movq	%rax, %rdi
	callq	phalf
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	8(%rsp), %rdi
	movq	16(%rsp), %rsi
	callq	pcmp
	testl	%eax, %eax
	js	.LBB0_5
	jmp	.LBB0_9
.LBB0_6:
	movq	pzero(%rip), %rsi
	leaq	16(%rsp), %rdi
	jmp	.LBB0_8
.LBB0_7:
	movl	$4, %edi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	callq	errorp
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
.LBB0_8:                                # %.loopexit
	callq	psetq
.LBB0_9:                                # %.loopexit
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_12
# BB#10:
	decw	(%rdi)
	jne	.LBB0_12
# BB#11:
	xorl	%eax, %eax
	callq	pfree
.LBB0_12:
	testq	%r12, %r12
	je	.LBB0_15
# BB#13:
	decw	(%r12)
	jne	.LBB0_15
# BB#14:
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	pfree
.LBB0_15:
	movq	16(%rsp), %rdi
	callq	presult
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	psqrt, .Lfunc_end0-psqrt
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"psqrt"
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"negative argument"
	.size	.L.str.1, 18


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
