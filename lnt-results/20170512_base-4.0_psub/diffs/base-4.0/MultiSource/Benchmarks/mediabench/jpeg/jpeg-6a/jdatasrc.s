	.text
	.file	"jdatasrc.bc"
	.globl	jpeg_stdio_src
	.p2align	4, 0x90
	.type	jpeg_stdio_src,@function
jpeg_stdio_src:                         # @jpeg_stdio_src
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	32(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB0_2
# BB#1:
	movq	8(%rbx), %rax
	xorl	%esi, %esi
	movl	$80, %edx
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, %r15
	movq	%r15, 32(%rbx)
	movq	8(%rbx), %rax
	xorl	%esi, %esi
	movl	$4096, %edx             # imm = 0x1000
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, 64(%r15)
	movq	32(%rbx), %rax
.LBB0_2:
	movq	$init_source, 16(%rax)
	movq	$fill_input_buffer, 24(%rax)
	movq	$skip_input_data, 32(%rax)
	movq	$jpeg_resync_to_restart, 40(%rax)
	movq	$term_source, 48(%rax)
	movq	%r14, 56(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	jpeg_stdio_src, .Lfunc_end0-jpeg_stdio_src
	.cfi_endproc

	.p2align	4, 0x90
	.type	init_source,@function
init_source:                            # @init_source
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rax
	movl	$1, 72(%rax)
	retq
.Lfunc_end1:
	.size	init_source, .Lfunc_end1-init_source
	.cfi_endproc

	.p2align	4, 0x90
	.type	fill_input_buffer,@function
fill_input_buffer:                      # @fill_input_buffer
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	32(%r14), %rbx
	movq	56(%rbx), %rcx
	movq	64(%rbx), %rdi
	movl	$1, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	fread
	testq	%rax, %rax
	jne	.LBB2_4
# BB#1:
	cmpl	$0, 72(%rbx)
	je	.LBB2_3
# BB#2:
	movq	(%r14), %rax
	movl	$41, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB2_3:
	movq	(%r14), %rax
	movl	$116, 40(%rax)
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	*8(%rax)
	movq	64(%rbx), %rax
	movb	$-1, (%rax)
	movq	64(%rbx), %rax
	movb	$-39, 1(%rax)
	movl	$2, %eax
.LBB2_4:                                # %._crit_edge
	movq	64(%rbx), %rcx
	movq	%rcx, (%rbx)
	movq	%rax, 8(%rbx)
	movl	$0, 72(%rbx)
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	fill_input_buffer, .Lfunc_end2-fill_input_buffer
	.cfi_endproc

	.p2align	4, 0x90
	.type	skip_input_data,@function
skip_input_data:                        # @skip_input_data
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 48
.Lcfi16:
	.cfi_offset %rbx, -40
.Lcfi17:
	.cfi_offset %r12, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	testq	%r12, %r12
	jle	.LBB3_10
# BB#1:                                 # %.preheader
	movq	32(%r14), %r15
	movq	8(%r15), %rax
	cmpq	%r12, %rax
	jge	.LBB3_9
# BB#2:                                 # %.lr.ph
	movq	%r15, %rbx
	jmp	.LBB3_3
	.p2align	4, 0x90
.LBB3_8:                                # %fill_input_buffer.exit._crit_edge
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	32(%r14), %rbx
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	subq	%rax, %r12
	movq	56(%rbx), %rcx
	movq	64(%rbx), %rdi
	movl	$1, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	fread
	testq	%rax, %rax
	jne	.LBB3_7
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, 72(%rbx)
	je	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	(%r14), %rax
	movl	$41, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB3_6:                                # %._crit_edge20.i
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	(%r14), %rax
	movl	$116, 40(%rax)
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	*8(%rax)
	movq	64(%rbx), %rax
	movb	$-1, (%rax)
	movq	64(%rbx), %rax
	movb	$-39, 1(%rax)
	movl	$2, %eax
.LBB3_7:                                # %fill_input_buffer.exit
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	64(%rbx), %rcx
	movq	%rcx, (%rbx)
	movq	%rax, 8(%rbx)
	movl	$0, 72(%rbx)
	movq	8(%r15), %rax
	cmpq	%rax, %r12
	jg	.LBB3_8
.LBB3_9:                                # %._crit_edge
	addq	%r12, (%r15)
	subq	%r12, %rax
	movq	%rax, 8(%r15)
.LBB3_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	skip_input_data, .Lfunc_end3-skip_input_data
	.cfi_endproc

	.p2align	4, 0x90
	.type	term_source,@function
term_source:                            # @term_source
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end4:
	.size	term_source, .Lfunc_end4-term_source
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
