	.text
	.file	"aha.bc"
	.globl	neg
	.p2align	4, 0x90
	.type	neg,@function
neg:                                    # @neg
	.cfi_startproc
# BB#0:
	negl	%edi
	movl	%edi, %eax
	retq
.Lfunc_end0:
	.size	neg, .Lfunc_end0-neg
	.cfi_endproc

	.globl	Not
	.p2align	4, 0x90
	.type	Not,@function
Not:                                    # @Not
	.cfi_startproc
# BB#0:
	notl	%edi
	movl	%edi, %eax
	retq
.Lfunc_end1:
	.size	Not, .Lfunc_end1-Not
	.cfi_endproc

	.globl	pop
	.p2align	4, 0x90
	.type	pop,@function
pop:                                    # @pop
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	shrl	%eax
	andl	$1431655765, %eax       # imm = 0x55555555
	subl	%eax, %edi
	movl	%edi, %eax
	andl	$858993459, %eax        # imm = 0x33333333
	shrl	$2, %edi
	andl	$858993459, %edi        # imm = 0x33333333
	addl	%eax, %edi
	movl	%edi, %eax
	shrl	$4, %eax
	addl	%edi, %eax
	andl	$252645135, %eax        # imm = 0xF0F0F0F
	imull	$16843009, %eax, %eax   # imm = 0x1010101
	shrl	$24, %eax
	retq
.Lfunc_end2:
	.size	pop, .Lfunc_end2-pop
	.cfi_endproc

	.globl	nlz
	.p2align	4, 0x90
	.type	nlz,@function
nlz:                                    # @nlz
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	je	.LBB3_1
# BB#2:
	movl	%edi, %eax
	shll	$16, %eax
	cmpl	$65536, %edi            # imm = 0x10000
	cmovael	%edi, %eax
	sbbl	%ecx, %ecx
	andl	$16, %ecx
	leal	8(%rcx), %edx
	movl	%eax, %esi
	shll	$8, %esi
	cmpl	$16777216, %eax         # imm = 0x1000000
	cmovael	%eax, %esi
	cmovael	%ecx, %edx
	movl	%edx, %ecx
	orl	$4, %ecx
	movl	%esi, %edi
	shll	$4, %edi
	cmpl	$268435456, %esi        # imm = 0x10000000
	cmovael	%esi, %edi
	cmovael	%edx, %ecx
	movl	%ecx, %edx
	orl	$2, %edx
	leal	(,%rdi,4), %eax
	cmpl	$1073741824, %edi       # imm = 0x40000000
	cmovael	%edi, %eax
	cmovael	%ecx, %edx
	shrl	$31, %eax
	xorl	$1, %eax
	addl	%edx, %eax
	retq
.LBB3_1:
	movl	$32, %eax
	retq
.Lfunc_end3:
	.size	nlz, .Lfunc_end3-nlz
	.cfi_endproc

	.globl	rev
	.p2align	4, 0x90
	.type	rev,@function
rev:                                    # @rev
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movl	%edi, %eax
	andl	$1431655765, %eax       # imm = 0x55555555
	shrl	%edi
	andl	$1431655765, %edi       # imm = 0x55555555
	leal	(%rdi,%rax,2), %eax
	movl	%eax, %ecx
	andl	$858993459, %ecx        # imm = 0x33333333
	shrl	$2, %eax
	andl	$858993459, %eax        # imm = 0x33333333
	leal	(%rax,%rcx,4), %eax
	movl	%eax, %ecx
	shll	$4, %ecx
	andl	$-252645136, %ecx       # imm = 0xF0F0F0F0
	shrl	$4, %eax
	andl	$252645135, %eax        # imm = 0xF0F0F0F
	orl	%ecx, %eax
	movl	%eax, %ecx
	shll	$24, %ecx
	movl	%eax, %edx
	shll	$8, %edx
	andl	$16711680, %edx         # imm = 0xFF0000
	movl	%eax, %esi
	shrl	$8, %esi
	andl	$65280, %esi            # imm = 0xFF00
	shrl	$24, %eax
	orl	%ecx, %eax
	orl	%edx, %eax
	orl	%esi, %eax
	retq
.Lfunc_end4:
	.size	rev, .Lfunc_end4-rev
	.cfi_endproc

	.globl	add
	.p2align	4, 0x90
	.type	add,@function
add:                                    # @add
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	leal	(%rdi,%rsi), %eax
	retq
.Lfunc_end5:
	.size	add, .Lfunc_end5-add
	.cfi_endproc

	.globl	sub
	.p2align	4, 0x90
	.type	sub,@function
sub:                                    # @sub
	.cfi_startproc
# BB#0:
	subl	%esi, %edi
	movl	%edi, %eax
	retq
.Lfunc_end6:
	.size	sub, .Lfunc_end6-sub
	.cfi_endproc

	.globl	mul
	.p2align	4, 0x90
	.type	mul,@function
mul:                                    # @mul
	.cfi_startproc
# BB#0:
	imull	%esi, %edi
	movl	%edi, %eax
	retq
.Lfunc_end7:
	.size	mul, .Lfunc_end7-mul
	.cfi_endproc

	.globl	divide
	.p2align	4, 0x90
	.type	divide,@function
divide:                                 # @divide
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	je	.LBB8_3
# BB#1:
	cmpl	$-2147483648, %edi      # imm = 0x80000000
	jne	.LBB8_4
# BB#2:
	cmpl	$-1, %esi
	jne	.LBB8_4
.LBB8_3:
	movl	$1, unacceptable(%rip)
	xorl	%eax, %eax
	retq
.LBB8_4:
	movl	%edi, %eax
	cltd
	idivl	%esi
	retq
.Lfunc_end8:
	.size	divide, .Lfunc_end8-divide
	.cfi_endproc

	.globl	divu
	.p2align	4, 0x90
	.type	divu,@function
divu:                                   # @divu
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	je	.LBB9_1
# BB#2:
	xorl	%edx, %edx
	movl	%edi, %eax
	divl	%esi
	retq
.LBB9_1:
	movl	$1, unacceptable(%rip)
	xorl	%eax, %eax
	retq
.Lfunc_end9:
	.size	divu, .Lfunc_end9-divu
	.cfi_endproc

	.globl	And
	.p2align	4, 0x90
	.type	And,@function
And:                                    # @And
	.cfi_startproc
# BB#0:
	andl	%esi, %edi
	movl	%edi, %eax
	retq
.Lfunc_end10:
	.size	And, .Lfunc_end10-And
	.cfi_endproc

	.globl	Or
	.p2align	4, 0x90
	.type	Or,@function
Or:                                     # @Or
	.cfi_startproc
# BB#0:
	orl	%esi, %edi
	movl	%edi, %eax
	retq
.Lfunc_end11:
	.size	Or, .Lfunc_end11-Or
	.cfi_endproc

	.globl	Xor
	.p2align	4, 0x90
	.type	Xor,@function
Xor:                                    # @Xor
	.cfi_startproc
# BB#0:
	xorl	%esi, %edi
	movl	%edi, %eax
	retq
.Lfunc_end12:
	.size	Xor, .Lfunc_end12-Xor
	.cfi_endproc

	.globl	rotl
	.p2align	4, 0x90
	.type	rotl,@function
rotl:                                   # @rotl
	.cfi_startproc
# BB#0:
	movl	%esi, %ecx
	roll	%cl, %edi
	movl	%edi, %eax
	retq
.Lfunc_end13:
	.size	rotl, .Lfunc_end13-rotl
	.cfi_endproc

	.globl	shl
	.p2align	4, 0x90
	.type	shl,@function
shl:                                    # @shl
	.cfi_startproc
# BB#0:
	andl	$63, %esi
	movl	%esi, %ecx
	shll	%cl, %edi
	xorl	%eax, %eax
	cmpl	$31, %esi
	cmovbel	%edi, %eax
	retq
.Lfunc_end14:
	.size	shl, .Lfunc_end14-shl
	.cfi_endproc

	.globl	shr
	.p2align	4, 0x90
	.type	shr,@function
shr:                                    # @shr
	.cfi_startproc
# BB#0:
	andl	$63, %esi
	movl	%esi, %ecx
	shrl	%cl, %edi
	xorl	%eax, %eax
	cmpl	$31, %esi
	cmovbel	%edi, %eax
	retq
.Lfunc_end15:
	.size	shr, .Lfunc_end15-shr
	.cfi_endproc

	.globl	shrs
	.p2align	4, 0x90
	.type	shrs,@function
shrs:                                   # @shrs
	.cfi_startproc
# BB#0:
	andl	$63, %esi
	cmpl	$31, %esi
	jb	.LBB16_2
# BB#1:
	movb	$31, %sil
.LBB16_2:
	movl	%esi, %ecx
	sarl	%cl, %edi
	movl	%edi, %eax
	retq
.Lfunc_end16:
	.size	shrs, .Lfunc_end16-shrs
	.cfi_endproc

	.globl	cmpeq
	.p2align	4, 0x90
	.type	cmpeq,@function
cmpeq:                                  # @cmpeq
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	%esi, %edi
	sete	%al
	retq
.Lfunc_end17:
	.size	cmpeq, .Lfunc_end17-cmpeq
	.cfi_endproc

	.globl	cmplt
	.p2align	4, 0x90
	.type	cmplt,@function
cmplt:                                  # @cmplt
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	%esi, %edi
	setl	%al
	retq
.Lfunc_end18:
	.size	cmplt, .Lfunc_end18-cmplt
	.cfi_endproc

	.globl	cmpltu
	.p2align	4, 0x90
	.type	cmpltu,@function
cmpltu:                                 # @cmpltu
	.cfi_startproc
# BB#0:
	cmpl	%esi, %edi
	sbbl	%eax, %eax
	andl	$1, %eax
	retq
.Lfunc_end19:
	.size	cmpltu, .Lfunc_end19-cmpltu
	.cfi_endproc

	.globl	seleq
	.p2align	4, 0x90
	.type	seleq,@function
seleq:                                  # @seleq
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	cmovnel	%edx, %esi
	movl	%esi, %eax
	retq
.Lfunc_end20:
	.size	seleq, .Lfunc_end20-seleq
	.cfi_endproc

	.globl	sellt
	.p2align	4, 0x90
	.type	sellt,@function
sellt:                                  # @sellt
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	cmovnsl	%edx, %esi
	movl	%esi, %eax
	retq
.Lfunc_end21:
	.size	sellt, .Lfunc_end21-sellt
	.cfi_endproc

	.globl	selle
	.p2align	4, 0x90
	.type	selle,@function
selle:                                  # @selle
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	cmovgl	%edx, %esi
	movl	%esi, %eax
	retq
.Lfunc_end22:
	.size	selle, .Lfunc_end22-selle
	.cfi_endproc

	.globl	print_expr
	.p2align	4, 0x90
	.type	print_expr,@function
print_expr:                             # @print_expr
	.cfi_startproc
# BB#0:
	cmpl	$10, %edi
	jg	.LBB23_5
# BB#1:
	movslq	%edi, %rax
	movl	r(,%rax,4), %esi
	leal	31(%rsi), %eax
	cmpl	$62, %eax
	ja	.LBB23_4
# BB#2:
	movl	$.L.str.28, %edi
	jmp	.LBB23_3
.LBB23_5:
	cmpl	$11, %edi
	jne	.LBB23_6
# BB#13:
	movl	$120, %edi
	jmp	putchar                 # TAILCALL
.LBB23_4:
	movl	$.L.str.29, %edi
.LBB23_3:
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	jmp	printf                  # TAILCALL
.LBB23_6:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movslq	%edi, %rbx
	addq	$-12, %rbx
	shlq	$4, %rbx
	movslq	pgm(%rbx), %rax
	imulq	$56, %rax, %r14
	movq	isa+40(%r14), %rsi
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, isa+8(%r14)
	jle	.LBB23_12
# BB#7:                                 # %.lr.ph
	leaq	isa+8(%r14), %r15
	leaq	pgm+4(%rbx), %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB23_8:                               # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rbx,4), %edi
	callq	print_expr
	movslq	(%r15), %rax
	decq	%rax
	cmpq	%rax, %rbx
	jge	.LBB23_10
# BB#9:                                 #   in Loop: Header=BB23_8 Depth=1
	movq	40(%r15), %rsi
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB23_11
	.p2align	4, 0x90
.LBB23_10:                              #   in Loop: Header=BB23_8 Depth=1
	movl	$41, %edi
	callq	putchar
.LBB23_11:                              #   in Loop: Header=BB23_8 Depth=1
	incq	%rbx
	movslq	(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB23_8
.LBB23_12:                              # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end23:
	.size	print_expr, .Lfunc_end23-print_expr
	.cfi_endproc

	.globl	print_pgm
	.p2align	4, 0x90
	.type	print_pgm,@function
print_pgm:                              # @print_pgm
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 48
.Lcfi11:
	.cfi_offset %rbx, -48
.Lcfi12:
	.cfi_offset %r12, -40
.Lcfi13:
	.cfi_offset %r13, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	cmpl	$0, numi(%rip)
	jle	.LBB24_16
# BB#1:                                 # %.lr.ph32.preheader
	movl	$pgm+4, %r15d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB24_2:                               # %.lr.ph32
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_4 Depth 2
	movq	%r14, %rax
	shlq	$4, %rax
	movslq	pgm(%rax), %rax
	imulq	$56, %rax, %rbx
	movq	isa+32(%rbx), %rsi
	incq	%r14
	movl	$.L.str.33, %edi
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	printf
	cmpl	$0, isa+8(%rbx)
	jle	.LBB24_15
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB24_2 Depth=1
	leaq	isa+8(%rbx), %r12
	movq	%r15, %r13
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB24_4:                               # %.lr.ph
                                        #   Parent Loop BB24_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%r13), %rsi
	cmpq	$10, %rsi
	jg	.LBB24_8
# BB#5:                                 #   in Loop: Header=BB24_4 Depth=2
	movl	r(,%rsi,4), %esi
	leal	31(%rsi), %eax
	cmpl	$62, %eax
	ja	.LBB24_7
# BB#6:                                 #   in Loop: Header=BB24_4 Depth=2
	movl	$.L.str.28, %edi
	jmp	.LBB24_11
	.p2align	4, 0x90
.LBB24_8:                               #   in Loop: Header=BB24_4 Depth=2
	cmpl	$11, %esi
	jne	.LBB24_10
# BB#9:                                 #   in Loop: Header=BB24_4 Depth=2
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB24_12
	.p2align	4, 0x90
.LBB24_7:                               #   in Loop: Header=BB24_4 Depth=2
	movl	$.L.str.29, %edi
	jmp	.LBB24_11
	.p2align	4, 0x90
.LBB24_10:                              #   in Loop: Header=BB24_4 Depth=2
	addl	$-11, %esi
	movl	$.L.str.35, %edi
.LBB24_11:                              #   in Loop: Header=BB24_4 Depth=2
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
.LBB24_12:                              #   in Loop: Header=BB24_4 Depth=2
	movslq	(%r12), %rax
	movq	%rax, %rcx
	decq	%rcx
	cmpq	%rcx, %rbx
	jge	.LBB24_14
# BB#13:                                #   in Loop: Header=BB24_4 Depth=2
	movl	$44, %edi
	callq	putchar
	movl	(%r12), %eax
.LBB24_14:                              #   in Loop: Header=BB24_4 Depth=2
	incq	%rbx
	cltq
	addq	$4, %r13
	cmpq	%rax, %rbx
	jl	.LBB24_4
.LBB24_15:                              # %._crit_edge
                                        #   in Loop: Header=BB24_2 Depth=1
	movl	$10, %edi
	callq	putchar
	movslq	numi(%rip), %rax
	addq	$16, %r15
	cmpq	%rax, %r14
	jl	.LBB24_2
.LBB24_16:                              # %._crit_edge33
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	callq	printf
	movl	numi(%rip), %edi
	addl	$11, %edi
	callq	print_expr
	movl	$10, %edi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	putchar                 # TAILCALL
.Lfunc_end24:
	.size	print_pgm, .Lfunc_end24-print_pgm
	.cfi_endproc

	.globl	check
	.p2align	4, 0x90
	.type	check,@function
check:                                  # @check
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 48
.Lcfi21:
	.cfi_offset %rbx, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movslq	%edi, %rbx
	leaq	(,%rbx,4), %rbp
	decq	%rbx
	.p2align	4, 0x90
.LBB25_1:                               # =>This Inner Loop Header: Depth=1
	movslq	pgm+4(,%rbp,4), %rax
	movl	r(,%rax,4), %edi
	movslq	pgm+8(,%rbp,4), %rax
	movl	r(,%rax,4), %esi
	movslq	pgm+12(,%rbp,4), %rax
	movl	r(,%rax,4), %edx
	movslq	pgm(,%rbp,4), %rax
	imulq	$56, %rax, %rax
	callq	*isa(%rax)
	movl	%eax, r+48(%rbp)
	incl	counter(%rbp)
	movl	numi(%rip), %ecx
	movslq	%ecx, %rax
	leaq	-1(%rax), %rdx
	addq	$4, %rbp
	incq	%rbx
	cmpq	%rdx, %rbx
	jl	.LBB25_1
# BB#2:
	cmpl	$0, unacceptable(%rip)
	je	.LBB25_5
.LBB25_3:
	movl	$0, unacceptable(%rip)
.LBB25_4:
	xorl	%eax, %eax
.LBB25_14:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB25_5:
	movl	r+44(,%rax,4), %edx
	xorl	%eax, %eax
	cmpl	corr_result(%rip), %edx
	jne	.LBB25_14
# BB#6:                                 # %.preheader.preheader
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB25_7:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_9 Depth 2
	movl	check.itrialx(%rip), %eax
	leal	1(%rax), %edx
	cmpl	$15, %eax
	cmovgl	%r14d, %edx
	movl	%edx, check.itrialx(%rip)
	movslq	%edx, %rax
	movl	trialx(,%rax,4), %edx
	movl	%edx, r+44(%rip)
	movl	correct_result(,%rax,4), %eax
	movl	%eax, corr_result(%rip)
	testl	%ecx, %ecx
	jle	.LBB25_11
# BB#8:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB25_7 Depth=1
	movl	$48, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB25_9:                               # %.lr.ph
                                        #   Parent Loop BB25_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	pgm-188(,%rbx,4), %rax
	movl	r(,%rax,4), %edi
	movslq	pgm-184(,%rbx,4), %rax
	movl	r(,%rax,4), %esi
	movslq	pgm-180(,%rbx,4), %rax
	movl	r(,%rax,4), %edx
	movslq	pgm-192(,%rbx,4), %rax
	imulq	$56, %rax, %rax
	callq	*isa(%rax)
	movl	%eax, r(%rbx)
	incl	counter-48(%rbx)
	incq	%rbp
	movslq	numi(%rip), %rcx
	addq	$4, %rbx
	cmpq	%rcx, %rbp
	jl	.LBB25_9
# BB#10:                                # %._crit_edge
                                        #   in Loop: Header=BB25_7 Depth=1
	cmpl	$0, unacceptable(%rip)
	jne	.LBB25_3
.LBB25_11:                              # %._crit_edge.thread
                                        #   in Loop: Header=BB25_7 Depth=1
	movslq	%ecx, %rax
	movl	r+44(,%rax,4), %eax
	cmpl	corr_result(%rip), %eax
	jne	.LBB25_4
# BB#12:                                #   in Loop: Header=BB25_7 Depth=1
	incl	%r15d
	cmpl	$15, %r15d
	jle	.LBB25_7
# BB#13:
	movl	$1, %eax
	jmp	.LBB25_14
.Lfunc_end25:
	.size	check, .Lfunc_end25-check
	.cfi_endproc

	.globl	fix_operands
	.p2align	4, 0x90
	.type	fix_operands,@function
fix_operands:                           # @fix_operands
	.cfi_startproc
# BB#0:
	movslq	%edi, %rax
	movq	%rax, %rdx
	shlq	$4, %rdx
	movslq	pgm(%rdx), %r9
	movl	numi(%rip), %esi
	leal	-1(%rsi), %r11d
	cmpl	%eax, %r11d
	jne	.LBB26_14
# BB#1:
	leal	10(%rsi), %ecx
	leaq	pgm+4(%rdx), %r8
	movl	pgm+8(%rdx), %r10d
	cmpl	%ecx, %r10d
	je	.LBB26_4
# BB#2:
	cmpl	%ecx, pgm+12(%rdx)
	je	.LBB26_4
# BB#3:
	movl	%ecx, (%r8)
.LBB26_4:
	addl	$9, %esi
	cmpl	$12, %esi
	jl	.LBB26_14
# BB#5:
	decq	%rax
	shlq	$4, %rax
	cmpl	%esi, pgm+4(%rax)
	je	.LBB26_14
# BB#6:
	cmpl	%esi, pgm+8(%rax)
	je	.LBB26_14
# BB#7:
	cmpl	%esi, pgm+12(%rax)
	je	.LBB26_14
# BB#8:
	cmpl	%esi, %r10d
	je	.LBB26_14
# BB#9:
	cmpl	%esi, pgm+12(%rdx)
	je	.LBB26_14
# BB#10:
	cmpl	%esi, (%r8)
	jge	.LBB26_12
# BB#11:
	movl	%esi, (%r8)
	jmp	.LBB26_14
.LBB26_12:
	imulq	$56, %r9, %rax
	cmpl	$2, isa+8(%rax)
	jl	.LBB26_14
# BB#13:
	leaq	pgm+8(%rdx), %rax
	movl	%esi, (%rax)
.LBB26_14:
	imulq	$56, %r9, %rax
	cmpl	$0, isa+12(%rax)
	je	.LBB26_17
# BB#15:
	movl	pgm+8(%rdx), %eax
	cmpl	%eax, pgm+4(%rdx)
	jge	.LBB26_22
# BB#16:
	leaq	pgm+4(%rdx), %rcx
	movl	%eax, (%rcx)
	retq
.LBB26_17:
	cmpl	%edi, %r11d
	je	.LBB26_22
# BB#18:
	cmpl	$10, pgm+4(%rdx)
	jg	.LBB26_22
# BB#19:
	cmpl	$10, pgm+8(%rdx)
	jg	.LBB26_22
# BB#20:
	cmpl	$10, pgm+12(%rdx)
	jg	.LBB26_22
# BB#21:
	leaq	pgm+4(%rdx), %rax
	movl	$11, (%rax)
.LBB26_22:
	retq
.Lfunc_end26:
	.size	fix_operands, .Lfunc_end26-fix_operands
	.cfi_endproc

	.globl	search
	.p2align	4, 0x90
	.type	search,@function
search:                                 # @search
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 64
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movl	trialx(%rip), %eax
	movl	%eax, r+44(%rip)
	movl	correct_result(%rip), %eax
	movl	%eax, corr_result(%rip)
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
.LBB27_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_6 Depth 2
                                        #       Child Loop BB27_8 Depth 3
	movl	%r15d, %edi
	callq	check
	testl	%eax, %eax
	je	.LBB27_4
# BB#2:                                 #   in Loop: Header=BB27_1 Depth=1
	incl	%r14d
	movl	numi(%rip), %esi
	movl	$.L.str.39, %edi
	xorl	%eax, %eax
	callq	printf
	callq	print_pgm
	cmpl	$5, %r14d
	je	.LBB27_3
.LBB27_4:                               #   in Loop: Header=BB27_1 Depth=1
	movslq	numi(%rip), %r12
	testq	%r12, %r12
	jle	.LBB27_23
# BB#5:                                 # %.lr.ph86.preheader.i
                                        #   in Loop: Header=BB27_1 Depth=1
	movq	%r12, %rax
	shlq	$4, %rax
	leaq	pgm-12(%rax), %r13
	.p2align	4, 0x90
.LBB27_6:                               # %.lr.ph86.i
                                        #   Parent Loop BB27_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB27_8 Depth 3
	leaq	-1(%r12), %r15
	movq	%r15, %rax
	shlq	$4, %rax
	movslq	pgm(%rax), %r9
	imulq	$56, %r9, %rsi
	movslq	isa+8(%rsi), %rdx
	testq	%rdx, %rdx
	jle	.LBB27_19
# BB#7:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB27_6 Depth=2
	leaq	pgm(%rax), %r8
	leal	10(%r12), %ecx
	leaq	isa+16(%rsi), %rbx
	movq	%r13, %rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB27_8:                               #   Parent Loop BB27_1 Depth=1
                                        #     Parent Loop BB27_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdi), %ebp
	cmpl	$5, %ebp
	jle	.LBB27_12
# BB#9:                                 #   in Loop: Header=BB27_8 Depth=3
	cmpl	$6, %ebp
	je	.LBB27_10
# BB#11:                                #   in Loop: Header=BB27_8 Depth=3
	cmpl	%ecx, %ebp
	jl	.LBB27_12
# BB#14:                                #   in Loop: Header=BB27_8 Depth=3
	movl	(%rbx,%rsi,4), %ebp
	movl	%ebp, (%rdi)
	incq	%rsi
	addq	$4, %rdi
	cmpq	%rdx, %rsi
	jl	.LBB27_8
# BB#15:                                #   in Loop: Header=BB27_6 Depth=2
	xorl	%ecx, %ecx
	testl	%esi, %esi
	jne	.LBB27_17
	jmp	.LBB27_19
	.p2align	4, 0x90
.LBB27_12:                              #   in Loop: Header=BB27_6 Depth=2
	incl	%ebp
	jmp	.LBB27_13
	.p2align	4, 0x90
.LBB27_10:                              #   in Loop: Header=BB27_6 Depth=2
	movl	$11, %ebp
.LBB27_13:                              # %.sink.split.i
                                        #   in Loop: Header=BB27_6 Depth=2
	movl	%ebp, (%rdi)
	movb	$1, %cl
	testl	%esi, %esi
	je	.LBB27_19
.LBB27_17:                              #   in Loop: Header=BB27_6 Depth=2
	testb	%cl, %cl
	jne	.LBB27_18
# BB#20:                                #   in Loop: Header=BB27_6 Depth=2
	cmpl	$11, %r9d
	jle	.LBB27_21
# BB#22:                                #   in Loop: Header=BB27_6 Depth=2
	movl	$0, (%r8)
	movl	isa+16(%rip), %ecx
	movl	%ecx, pgm+4(%rax)
	movl	isa+20(%rip), %ecx
	movl	%ecx, pgm+8(%rax)
	movl	isa+24(%rip), %ecx
	movl	%ecx, pgm+12(%rax)
	movl	%r15d, %edi
	callq	fix_operands
	addq	$-16, %r13
	cmpq	$1, %r12
	movq	%r15, %r12
	jg	.LBB27_6
	jmp	.LBB27_23
.LBB27_21:                              #   in Loop: Header=BB27_1 Depth=1
	incl	%r9d
	movl	%r9d, (%r8)
	movslq	%r9d, %rcx
	imulq	$56, %rcx, %rcx
	movl	isa+16(%rcx), %edx
	movl	%edx, pgm+4(%rax)
	movl	isa+20(%rcx), %edx
	movl	%edx, pgm+8(%rax)
	movl	isa+24(%rcx), %ecx
	movl	%ecx, pgm+12(%rax)
.LBB27_18:                              #   in Loop: Header=BB27_1 Depth=1
	movl	%r15d, %edi
	callq	fix_operands
.LBB27_19:                              # %increment.exit
                                        #   in Loop: Header=BB27_1 Depth=1
	testl	%r15d, %r15d
	jns	.LBB27_1
	jmp	.LBB27_23
.LBB27_3:
	movl	$5, %r14d
.LBB27_23:                              # %increment.exit.thread
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	search, .Lfunc_end27-search
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 32
.Lcfi41:
	.cfi_offset %rbx, -32
.Lcfi42:
	.cfi_offset %r14, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movl	$1, numi(%rip)
	movl	$1, %esi
	.p2align	4, 0x90
.LBB28_1:                               # %.preheader34
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB28_3 Depth 2
                                        #     Child Loop BB28_6 Depth 2
	movl	$.L.str.40, %edi
	xorl	%eax, %eax
	callq	printf
	movl	trialx(%rip), %edi
	callq	userfun
	movl	%eax, correct_result(%rip)
	movl	trialx+4(%rip), %edi
	callq	userfun
	movl	%eax, correct_result+4(%rip)
	movl	trialx+8(%rip), %edi
	callq	userfun
	movl	%eax, correct_result+8(%rip)
	movl	trialx+12(%rip), %edi
	callq	userfun
	movl	%eax, correct_result+12(%rip)
	movl	trialx+16(%rip), %edi
	callq	userfun
	movl	%eax, correct_result+16(%rip)
	movl	trialx+20(%rip), %edi
	callq	userfun
	movl	%eax, correct_result+20(%rip)
	movl	trialx+24(%rip), %edi
	callq	userfun
	movl	%eax, correct_result+24(%rip)
	movl	trialx+28(%rip), %edi
	callq	userfun
	movl	%eax, correct_result+28(%rip)
	movl	trialx+32(%rip), %edi
	callq	userfun
	movl	%eax, correct_result+32(%rip)
	movl	trialx+36(%rip), %edi
	callq	userfun
	movl	%eax, correct_result+36(%rip)
	movl	trialx+40(%rip), %edi
	callq	userfun
	movl	%eax, correct_result+40(%rip)
	movl	trialx+44(%rip), %edi
	callq	userfun
	movl	%eax, correct_result+44(%rip)
	movl	trialx+48(%rip), %edi
	callq	userfun
	movl	%eax, correct_result+48(%rip)
	movl	trialx+52(%rip), %edi
	callq	userfun
	movl	%eax, correct_result+52(%rip)
	movl	trialx+56(%rip), %edi
	callq	userfun
	movl	%eax, correct_result+56(%rip)
	movl	trialx+60(%rip), %edi
	callq	userfun
	movl	%eax, correct_result+60(%rip)
	movl	trialx+64(%rip), %edi
	callq	userfun
	movl	%eax, correct_result+64(%rip)
	cmpl	$0, numi(%rip)
	jle	.LBB28_4
# BB#2:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB28_1 Depth=1
	movl	$pgm+12, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB28_3:                               # %.lr.ph
                                        #   Parent Loop BB28_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$0, -12(%rbp)
	movl	isa+16(%rip), %eax
	movl	%eax, -8(%rbp)
	movl	isa+20(%rip), %eax
	movl	%eax, -4(%rbp)
	movl	isa+24(%rip), %eax
	movl	%eax, (%rbp)
	movl	%ebx, %edi
	callq	fix_operands
	incq	%rbx
	movslq	numi(%rip), %rax
	addq	$16, %rbp
	cmpq	%rax, %rbx
	jl	.LBB28_3
.LBB28_4:                               # %._crit_edge
                                        #   in Loop: Header=BB28_1 Depth=1
	callq	search
	movl	%eax, %r14d
	xorl	%ebp, %ebp
	movl	$.L.str.41, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, numi(%rip)
	jle	.LBB28_7
# BB#5:                                 # %.lr.ph31.preheader
                                        #   in Loop: Header=BB28_1 Depth=1
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB28_6:                               # %.lr.ph31
                                        #   Parent Loop BB28_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	counter(,%rbx,4), %esi
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	callq	printf
	addl	counter(,%rbx,4), %ebp
	incq	%rbx
	movslq	numi(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB28_6
.LBB28_7:                               # %._crit_edge32
                                        #   in Loop: Header=BB28_1 Depth=1
	movl	$.L.str.44, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	movl	numi(%rip), %esi
	incl	%esi
	movl	%esi, numi(%rip)
	testl	%r14d, %r14d
	jne	.LBB28_9
# BB#8:                                 # %._crit_edge32
                                        #   in Loop: Header=BB28_1 Depth=1
	cmpl	$6, %esi
	jl	.LBB28_1
.LBB28_9:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end28:
	.size	main, .Lfunc_end28-main
	.cfi_endproc

	.type	debug,@object           # @debug
	.section	.rodata,"a",@progbits
	.globl	debug
	.p2align	2
debug:
	.long	0                       # 0x0
	.size	debug, 4

	.type	counters,@object        # @counters
	.globl	counters
	.p2align	2
counters:
	.long	1                       # 0x1
	.size	counters, 4

	.type	trialx,@object          # @trialx
	.data
	.globl	trialx
	.p2align	4
trialx:
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	2147483648              # 0x80000000
	.long	2147483647              # 0x7fffffff
	.long	2147483649              # 0x80000001
	.long	2147483646              # 0x7ffffffe
	.long	19088743                # 0x1234567
	.long	2309737967              # 0x89abcdef
	.long	4294967294              # 0xfffffffe
	.long	2                       # 0x2
	.long	4294967293              # 0xfffffffd
	.long	3                       # 0x3
	.long	4294967232              # 0xffffffc0
	.long	64                      # 0x40
	.long	4294967291              # 0xfffffffb
	.long	4294935881              # 0xffff8549
	.size	trialx, 68

	.type	dummy1,@object          # @dummy1
	.globl	dummy1
	.p2align	4
dummy1:
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	1                       # 0x1
	.long	2147483648              # 0x80000000
	.long	4294967294              # 0xfffffffe
	.long	2                       # 0x2
	.long	3                       # 0x3
	.size	dummy1, 28

	.type	dummy2,@object          # @dummy2
	.globl	dummy2
	.p2align	4
dummy2:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	30                      # 0x1e
	.long	31                      # 0x1f
	.size	dummy2, 16

	.type	unacceptable,@object    # @unacceptable
	.comm	unacceptable,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"neg"
	.size	.L.str, 4

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"-("
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.zero	1
	.size	.L.str.2, 1

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"not"
	.size	.L.str.3, 4

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"~("
	.size	.L.str.4, 3

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"add"
	.size	.L.str.5, 4

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"("
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	" + "
	.size	.L.str.7, 4

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"sub"
	.size	.L.str.8, 4

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	" - "
	.size	.L.str.9, 4

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"mul"
	.size	.L.str.10, 4

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"*"
	.size	.L.str.11, 2

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"div"
	.size	.L.str.12, 4

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"/"
	.size	.L.str.13, 2

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"divu"
	.size	.L.str.14, 5

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	" /u "
	.size	.L.str.15, 5

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"and"
	.size	.L.str.16, 4

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	" & "
	.size	.L.str.17, 4

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"or"
	.size	.L.str.18, 3

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	" | "
	.size	.L.str.19, 4

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"xor"
	.size	.L.str.20, 4

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	" ^ "
	.size	.L.str.21, 4

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"shl"
	.size	.L.str.22, 4

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	" << "
	.size	.L.str.23, 5

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"shr"
	.size	.L.str.24, 4

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	" >>u "
	.size	.L.str.25, 6

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"shrs"
	.size	.L.str.26, 5

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	" >>s "
	.size	.L.str.27, 6

	.type	isa,@object             # @isa
	.data
	.globl	isa
	.p2align	4
isa:
	.quad	neg
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	11                      # 0xb
	.long	0                       # 0x0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str
	.quad	.L.str.1
	.quad	.L.str.2
	.quad	Not
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	11                      # 0xb
	.long	0                       # 0x0
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.3
	.quad	.L.str.4
	.quad	.L.str.2
	.quad	add
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	11                      # 0xb
	.long	2                       # 0x2
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.5
	.quad	.L.str.6
	.quad	.L.str.7
	.quad	sub
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.8
	.quad	.L.str.6
	.quad	.L.str.9
	.quad	mul
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	11                      # 0xb
	.long	3                       # 0x3
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.10
	.quad	.L.str.6
	.quad	.L.str.11
	.quad	divide
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.12
	.quad	.L.str.6
	.quad	.L.str.13
	.quad	divu
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.14
	.quad	.L.str.6
	.quad	.L.str.15
	.quad	And
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	11                      # 0xb
	.long	2                       # 0x2
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.16
	.quad	.L.str.6
	.quad	.L.str.17
	.quad	Or
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	11                      # 0xb
	.long	2                       # 0x2
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.18
	.quad	.L.str.6
	.quad	.L.str.19
	.quad	Xor
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	11                      # 0xb
	.long	2                       # 0x2
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.20
	.quad	.L.str.6
	.quad	.L.str.21
	.quad	shl
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.22
	.quad	.L.str.6
	.quad	.L.str.23
	.quad	shr
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.24
	.quad	.L.str.6
	.quad	.L.str.25
	.quad	shrs
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.26
	.quad	.L.str.6
	.quad	.L.str.27
	.size	isa, 728

	.type	r,@object               # @r
	.globl	r
	.p2align	4
r:
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	1                       # 0x1
	.long	2147483648              # 0x80000000
	.long	4294967294              # 0xfffffffe
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	30                      # 0x1e
	.long	31                      # 0x1f
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	r, 68

	.type	.L.str.28,@object       # @.str.28
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.28:
	.asciz	"%d"
	.size	.L.str.28, 3

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"0x%X"
	.size	.L.str.29, 5

	.type	pgm,@object             # @pgm
	.comm	pgm,80,16
	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"%s"
	.size	.L.str.31, 3

	.type	numi,@object            # @numi
	.comm	numi,4,4
	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"   %-5s r%d,"
	.size	.L.str.33, 13

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"rx"
	.size	.L.str.34, 3

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"r%d"
	.size	.L.str.35, 4

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"   Expr: "
	.size	.L.str.38, 10

	.type	check.itrialx,@object   # @check.itrialx
	.local	check.itrialx
	.comm	check.itrialx,4,4
	.type	corr_result,@object     # @corr_result
	.comm	corr_result,4,4
	.type	correct_result,@object  # @correct_result
	.comm	correct_result,68,16
	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"\nFound a %d-operation program:\n"
	.size	.L.str.39, 32

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"Searching for programs with %d operations.\n"
	.size	.L.str.40, 44

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"Found %d solutions.\n"
	.size	.L.str.41, 21

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"Counters = "
	.size	.L.str.42, 12

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"%d, "
	.size	.L.str.43, 5

	.type	counter,@object         # @counter
	.comm	counter,20,16
	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"total = %d\n"
	.size	.L.str.44, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
