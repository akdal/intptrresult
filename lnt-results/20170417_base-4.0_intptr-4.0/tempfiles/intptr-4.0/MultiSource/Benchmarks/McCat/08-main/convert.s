	.text
	.file	"convert.bc"
	.globl	PointToHPoint
	.p2align	4, 0x90
	.type	PointToHPoint,@function
PointToHPoint:                          # @PointToHPoint
	.cfi_startproc
# BB#0:
	movaps	8(%rsp), %xmm0
	movq	24(%rsp), %rax
	movups	%xmm0, (%rdi)
	movq	%rax, 16(%rdi)
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, 24(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end0:
	.size	PointToHPoint, .Lfunc_end0-PointToHPoint
	.cfi_endproc

	.globl	TPointToHPoint
	.p2align	4, 0x90
	.type	TPointToHPoint,@function
TPointToHPoint:                         # @TPointToHPoint
	.cfi_startproc
# BB#0:
	movups	32(%rsp), %xmm0
	movq	48(%rsp), %rax
	movups	%xmm0, (%rdi)
	movq	%rax, 16(%rdi)
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, 24(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end1:
	.size	TPointToHPoint, .Lfunc_end1-TPointToHPoint
	.cfi_endproc

	.globl	HPointToPoint
	.p2align	4, 0x90
	.type	HPointToPoint,@function
HPointToPoint:                          # @HPointToPoint
	.cfi_startproc
# BB#0:
	movaps	8(%rsp), %xmm0
	movq	24(%rsp), %rax
	movups	%xmm0, (%rdi)
	movq	%rax, 16(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end2:
	.size	HPointToPoint, .Lfunc_end2-HPointToPoint
	.cfi_endproc

	.globl	HPointToTPoint
	.p2align	4, 0x90
	.type	HPointToTPoint,@function
HPointToTPoint:                         # @HPointToTPoint
	.cfi_startproc
# BB#0:
	movaps	8(%rsp), %xmm0
	movq	24(%rsp), %rax
	movups	%xmm0, 24(%rdi)
	movq	%rax, 40(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end3:
	.size	HPointToTPoint, .Lfunc_end3-HPointToTPoint
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
