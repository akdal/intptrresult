	.text
	.file	"parse.bc"
	.globl	_ZN24lambda_expression_parserC2EP12token_stream
	.p2align	4, 0x90
	.type	_ZN24lambda_expression_parserC2EP12token_stream,@function
_ZN24lambda_expression_parserC2EP12token_stream: # @_ZN24lambda_expression_parserC2EP12token_stream
	.cfi_startproc
# BB#0:
	movq	%rsi, (%rdi)
	movl	$0, 8(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN24lambda_expression_parserC2EP12token_stream, .Lfunc_end0-_ZN24lambda_expression_parserC2EP12token_stream
	.cfi_endproc

	.globl	_ZN24lambda_expression_parserD2Ev
	.p2align	4, 0x90
	.type	_ZN24lambda_expression_parserD2Ev,@function
_ZN24lambda_expression_parserD2Ev:      # @_ZN24lambda_expression_parserD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	_ZN24lambda_expression_parserD2Ev, .Lfunc_end1-_ZN24lambda_expression_parserD2Ev
	.cfi_endproc

	.globl	_ZN24lambda_expression_parser10expressionEPP11arglst_node
	.p2align	4, 0x90
	.type	_ZN24lambda_expression_parser10expressionEPP11arglst_node,@function
_ZN24lambda_expression_parser10expressionEPP11arglst_node: # @_ZN24lambda_expression_parser10expressionEPP11arglst_node
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 64
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#1:
	leaq	8(%rsp), %rsi
	callq	_ZN12token_stream9get_tokenEPPc
	movl	%eax, %ebp
	movq	(%r15), %rdi
	movq	8(%rsp), %rdx
	movl	%ebp, %esi
	callq	_ZN12token_stream9is_headerENS_10token_typeEPc
	xorl	%r14d, %r14d
	cmpl	$7, %eax
	ja	.LBB2_47
# BB#2:
	movl	%eax, %eax
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_3:
	movq	(%r15), %rdi
	movq	8(%rsp), %rdx
	movl	%ebp, %esi
	callq	_ZN12token_stream10push_tokenENS_10token_typeEPc
	movq	%r15, %rdi
	callq	_ZN24lambda_expression_parser11applicationEv
	movq	%rax, %r14
	jmp	.LBB2_47
.LBB2_4:
	xorl	%r14d, %r14d
	jmp	.LBB2_50
.LBB2_5:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	_ZN24lambda_expression_parser10definitionEPP11arglst_node
	jmp	.LBB2_46
.LBB2_6:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	_ZN24lambda_expression_parser4loadEPP11arglst_node
	jmp	.LBB2_46
.LBB2_7:                                # %.preheader.preheader
	xorl	%eax, %eax
	leaq	8(%rsp), %r14
	xorl	%ebp, %ebp
	jmp	.LBB2_9
.LBB2_8:                                #   in Loop: Header=BB2_9 Depth=1
	xorl	%eax, %eax
	cmpl	$0, trace_lambda(%rip)
	sete	%al
	movl	%eax, trace_lambda(%rip)
	movl	%r12d, %eax
	.p2align	4, 0x90
.LBB2_9:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %ebx
	movl	%eax, %r12d
	movq	(%r15), %rdi
	movq	%r14, %rsi
	callq	_ZN12token_stream9get_tokenEPPc
	movl	$1, %ebp
	cmpl	$8, %eax
	je	.LBB2_11
# BB#10:                                # %.preheader
                                        #   in Loop: Header=BB2_9 Depth=1
	cmpl	$11, %eax
	movl	$1, %eax
	jne	.LBB2_9
	jmp	.LBB2_34
	.p2align	4, 0x90
.LBB2_11:                               #   in Loop: Header=BB2_9 Depth=1
	movq	8(%rsp), %rbx
	movl	$.L.str.1, %edi
	movq	%rbx, %rsi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB2_8
# BB#12:                                #   in Loop: Header=BB2_9 Depth=1
	movl	$.L.str.2, %edi
	movq	%rbx, %rsi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB2_22
# BB#13:                                #   in Loop: Header=BB2_9 Depth=1
	movl	$.L.str.3, %edi
	movq	%rbx, %rsi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB2_24
# BB#14:                                #   in Loop: Header=BB2_9 Depth=1
	movl	$.L.str.4, %edi
	movq	%rbx, %rsi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB2_26
# BB#15:                                #   in Loop: Header=BB2_9 Depth=1
	movl	$.L.str.5, %edi
	movq	%rbx, %rsi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB2_27
# BB#16:                                #   in Loop: Header=BB2_9 Depth=1
	movl	$.L.str.6, %edi
	movq	%rbx, %rsi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB2_28
# BB#17:                                #   in Loop: Header=BB2_9 Depth=1
	movl	$.L.str.7, %edi
	movq	%rbx, %rsi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB2_29
# BB#18:                                #   in Loop: Header=BB2_9 Depth=1
	movl	$.L.str.8, %edi
	movq	%rbx, %rsi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB2_30
# BB#19:                                #   in Loop: Header=BB2_9 Depth=1
	movl	$.L.str.9, %edi
	movq	%rbx, %rsi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB2_31
# BB#20:                                #   in Loop: Header=BB2_9 Depth=1
	movl	$.L.str.10, %edi
	movq	%rbx, %rsi
	callq	strcasecmp
	testl	%eax, %eax
	movl	$1, %eax
	jne	.LBB2_9
# BB#21:                                #   in Loop: Header=BB2_9 Depth=1
	xorl	%eax, %eax
	cmpl	$0, reduce_fully(%rip)
	sete	%al
	movl	%eax, reduce_fully(%rip)
	movl	%r12d, %eax
	jmp	.LBB2_9
.LBB2_22:                               #   in Loop: Header=BB2_9 Depth=1
	xorl	%eax, %eax
	cmpl	$0, step_lambda(%rip)
	sete	%al
	movl	%eax, step_lambda(%rip)
	movl	%r12d, %eax
	jne	.LBB2_9
# BB#23:                                #   in Loop: Header=BB2_9 Depth=1
	movl	$0, step_thru(%rip)
	movl	%r12d, %eax
	jmp	.LBB2_9
.LBB2_24:                               #   in Loop: Header=BB2_9 Depth=1
	xorl	%eax, %eax
	cmpl	$0, step_thru(%rip)
	sete	%al
	movl	%eax, step_thru(%rip)
	movl	%r12d, %eax
	jne	.LBB2_9
# BB#25:                                #   in Loop: Header=BB2_9 Depth=1
	movl	$0, step_lambda(%rip)
	movl	%r12d, %eax
	jmp	.LBB2_9
.LBB2_26:                               #   in Loop: Header=BB2_9 Depth=1
	xorl	%eax, %eax
	cmpl	$0, print_symbols(%rip)
	sete	%al
	movl	%eax, print_symbols(%rip)
	movl	%r12d, %eax
	jmp	.LBB2_9
.LBB2_27:                               #   in Loop: Header=BB2_9 Depth=1
	xorl	%eax, %eax
	cmpl	$0, applicative_order(%rip)
	sete	%al
	movl	%eax, applicative_order(%rip)
	movl	%r12d, %eax
	jmp	.LBB2_9
.LBB2_28:                               #   in Loop: Header=BB2_9 Depth=1
	xorl	%eax, %eax
	cmpl	$0, reduce_body(%rip)
	sete	%al
	movl	%eax, reduce_body(%rip)
	movl	%r12d, %eax
	jmp	.LBB2_9
.LBB2_29:                               #   in Loop: Header=BB2_9 Depth=1
	xorl	%eax, %eax
	cmpl	$0, brief_print(%rip)
	sete	%al
	movl	%eax, brief_print(%rip)
	movl	%r12d, %eax
	jmp	.LBB2_9
.LBB2_30:                               #   in Loop: Header=BB2_9 Depth=1
	xorl	%eax, %eax
	cmpl	$0, extract_eta(%rip)
	sete	%al
	movl	%eax, extract_eta(%rip)
	movl	%r12d, %eax
	jmp	.LBB2_9
.LBB2_31:                               #   in Loop: Header=BB2_9 Depth=1
	xorl	%eax, %eax
	cmpl	$0, extract_app(%rip)
	sete	%al
	movl	%eax, extract_app(%rip)
	movl	%r12d, %eax
	jmp	.LBB2_9
.LBB2_32:
	xorl	%eax, %eax
	cmpl	$0, extract_eta(%rip)
	setne	%al
	shll	$6, %eax
	leal	128(%rax), %edx
	cmpl	$0, extract_app(%rip)
	cmovel	%eax, %edx
	movq	%r15, %rdi
	callq	_ZN24lambda_expression_parser10extractionEPP11arglst_nodei
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB2_42
# BB#33:
	movq	(%rbx), %rsi
	testq	%r14, %r14
	jne	.LBB2_43
	jmp	.LBB2_46
.LBB2_34:
	testl	%r12d, %r12d
	jne	.LBB2_41
# BB#35:
	testl	%ebx, %ebx
	jne	.LBB2_46
.LBB2_41:
	movl	trace_lambda(%rip), %esi
	xorl	%r14d, %r14d
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
	movl	step_lambda(%rip), %esi
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	printf
	movl	step_thru(%rip), %esi
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	printf
	movl	applicative_order(%rip), %esi
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	printf
	movl	reduce_body(%rip), %esi
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	printf
	movl	brief_print(%rip), %esi
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	printf
	movl	print_symbols(%rip), %esi
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	printf
	movl	extract_eta(%rip), %esi
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	printf
	movl	extract_app(%rip), %esi
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	printf
	movl	reduce_fully(%rip), %esi
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB2_47
.LBB2_37:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp0:
	movl	$.L.str, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	callq	_ZN8arg_nodeC1EPKcPK8exp_nodes
.Ltmp1:
	jmp	.LBB2_47
.LBB2_38:
	testq	%rbx, %rbx
	je	.LBB2_46
# BB#39:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_46
# BB#40:
	movq	(%rdi), %rax
	callq	*136(%rax)
	jmp	.LBB2_46
.LBB2_42:
	xorl	%esi, %esi
	testq	%r14, %r14
	je	.LBB2_46
.LBB2_43:
	xorl	%edx, %edx
	cmpl	$0, brief_print(%rip)
	setne	%dl
	shll	$4, %edx
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*112(%rax)
	movl	$10, %edi
	callq	putchar
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
.LBB2_46:
	xorl	%r14d, %r14d
.LBB2_47:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB2_49
# BB#48:
	callq	_ZN12token_stream11reset_tokenEv
.LBB2_49:
	callq	_ZN4node5resetEv
.LBB2_50:
	movq	%r14, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_51:
.Ltmp2:
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN24lambda_expression_parser10expressionEPP11arglst_node, .Lfunc_end2-_ZN24lambda_expression_parser10expressionEPP11arglst_node
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_3
	.quad	.LBB2_5
	.quad	.LBB2_6
	.quad	.LBB2_47
	.quad	.LBB2_37
	.quad	.LBB2_38
	.quad	.LBB2_7
	.quad	.LBB2_32
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end2-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN24lambda_expression_parser10definitionEPP11arglst_node
	.p2align	4, 0x90
	.type	_ZN24lambda_expression_parser10definitionEPP11arglst_node,@function
_ZN24lambda_expression_parser10definitionEPP11arglst_node: # @_ZN24lambda_expression_parser10definitionEPP11arglst_node
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 64
.Lcfi16:
	.cfi_offset %rbx, -40
.Lcfi17:
	.cfi_offset %r12, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_1
# BB#3:
	leaq	16(%rsp), %rsi
	callq	_ZN12token_stream9get_tokenEPPc
	cmpl	$8, %eax
	jne	.LBB3_1
# BB#4:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %r14
	movq	16(%rsp), %rsi
.Ltmp3:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	callq	_ZN8arg_nodeC1EPKcPK8exp_nodes
.Ltmp4:
# BB#5:
	xorl	%r12d, %r12d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN24lambda_expression_parser10expressionEPP11arglst_node
	movq	%rax, 8(%rsp)
	testq	%r15, %r15
	je	.LBB3_2
# BB#6:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB3_13
# BB#7:
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*128(%rax)
	leaq	8(%rsp), %rsi
	testq	%rax, %rax
	je	.LBB3_9
# BB#8:
	movq	%rax, %rdi
	callq	_ZN8arg_node12import_valueEPP8exp_node
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
.LBB3_1:
	xorl	%r12d, %r12d
.LBB3_2:
	movq	%r12, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB3_13:
	leaq	8(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZN8arg_node12import_valueEPP8exp_node
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %r12
.Ltmp9:
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZN11arglst_nodeC1EP8arg_nodePS_s
.Ltmp10:
	jmp	.LBB3_10
.LBB3_9:
	movq	%r14, %rdi
	callq	_ZN8arg_node12import_valueEPP8exp_node
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %r12
	movq	(%r15), %rdx
.Ltmp6:
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZN11arglst_nodeC1EP8arg_nodePS_s
.Ltmp7:
.LBB3_10:
	movq	%r12, (%r15)
	movq	%r12, definition_env(%rip)
	jmp	.LBB3_2
.LBB3_11:
.Ltmp8:
	jmp	.LBB3_12
.LBB3_14:
.Ltmp11:
.LBB3_12:
	movq	%rax, %rbx
	movq	%r12, %rdi
	jmp	.LBB3_16
.LBB3_15:
.Ltmp5:
	movq	%rax, %rbx
	movq	%r14, %rdi
.LBB3_16:
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN24lambda_expression_parser10definitionEPP11arglst_node, .Lfunc_end3-_ZN24lambda_expression_parser10definitionEPP11arglst_node
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp3-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp9-.Ltmp4           #   Call between .Ltmp4 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 4 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp6-.Ltmp10          #   Call between .Ltmp10 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 6 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 7 <<
	.long	.Lfunc_end3-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN24lambda_expression_parser4loadEPP11arglst_node
	.p2align	4, 0x90
	.type	_ZN24lambda_expression_parser4loadEPP11arglst_node,@function
_ZN24lambda_expression_parser4loadEPP11arglst_node: # @_ZN24lambda_expression_parser4loadEPP11arglst_node
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 64
.Lcfi24:
	.cfi_offset %rbx, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB4_14
# BB#1:
	leaq	8(%rsp), %rsi
	callq	_ZN12token_stream9get_tokenEPPc
	cmpl	$8, %eax
	je	.LBB4_5
# BB#2:
	cmpl	$7, %eax
	jne	.LBB4_14
# BB#3:
	movq	8(%rsp), %rdi
	movl	$.L.str.27, %esi
	callq	strtok
	testq	%rax, %rax
	je	.LBB4_5
# BB#4:
	movq	%rax, 8(%rsp)
.LBB4_5:                                # %.thread
	movl	$8760, %edi             # imm = 0x2238
	callq	_Znwm
	movq	%rax, %rbx
	movq	8(%rsp), %rsi
.Ltmp12:
	movq	%rbx, %rdi
	callq	_ZN12token_streamC1EPKc
.Ltmp13:
# BB#6:
	movq	%rbx, 16(%rsp)
	movl	$0, 24(%rsp)
	leaq	16(%rsp), %r15
	.p2align	4, 0x90
.LBB4_7:                                # =>This Inner Loop Header: Depth=1
	cmpw	$0, 2(%rbx)
	jne	.LBB4_12
# BB#8:                                 #   in Loop: Header=BB4_7 Depth=1
	cmpw	$0, (%rbx)
	jne	.LBB4_12
# BB#9:                                 #   in Loop: Header=BB4_7 Depth=1
.Ltmp18:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	_ZN24lambda_expression_parser10expressionEPP11arglst_node
.Ltmp19:
	jmp	.LBB4_7
.LBB4_12:                               # %.critedge
.Ltmp15:
	movq	%rbx, %rdi
	callq	_ZN12token_streamD1Ev
.Ltmp16:
# BB#13:
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB4_14:
	xorl	%eax, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB4_17:
.Ltmp17:
	jmp	.LBB4_11
.LBB4_10:
.Ltmp14:
.LBB4_11:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB4_15:
.Ltmp20:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN24lambda_expression_parser4loadEPP11arglst_node, .Lfunc_end4-_ZN24lambda_expression_parser4loadEPP11arglst_node
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp12-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin2   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin2   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin2   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Lfunc_end4-.Ltmp16     #   Call between .Ltmp16 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN24lambda_expression_parser10extractionEPP11arglst_nodei
	.p2align	4, 0x90
	.type	_ZN24lambda_expression_parser10extractionEPP11arglst_nodei,@function
_ZN24lambda_expression_parser10extractionEPP11arglst_nodei: # @_ZN24lambda_expression_parser10extractionEPP11arglst_nodei
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 48
.Lcfi32:
	.cfi_offset %rbx, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_6
# BB#1:
	movq	%rsp, %rsi
	callq	_ZN12token_stream9get_tokenEPPc
	movl	%eax, %ecx
	orl	$1, %ecx
	cmpl	$9, %ecx
	jne	.LBB5_6
# BB#2:
	cmpl	$8, %eax
	jne	.LBB5_8
# BB#3:
	movq	(%rsp), %rbp
	movl	$.L.str.28, %esi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_8
# BB#4:
	movq	%rbp, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, %r15
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	strcpy
	jmp	.LBB5_9
.LBB5_6:
	xorl	%ebp, %ebp
	jmp	.LBB5_13
.LBB5_8:
	xorl	%r15d, %r15d
.LBB5_9:
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN24lambda_expression_parser10expressionEPP11arglst_node
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB5_11
# BB#10:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movl	%r14d, %edx
	callq	*208(%rax)
	movq	%rax, %rbp
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*16(%rax)
.LBB5_11:
	testq	%r15, %r15
	je	.LBB5_13
# BB#12:
	movq	%r15, %rdi
	callq	_ZdlPv
.LBB5_13:
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN24lambda_expression_parser10extractionEPP11arglst_nodei, .Lfunc_end5-_ZN24lambda_expression_parser10extractionEPP11arglst_nodei
	.cfi_endproc

	.globl	_ZN24lambda_expression_parser11applicationEv
	.p2align	4, 0x90
	.type	_ZN24lambda_expression_parser11applicationEv,@function
_ZN24lambda_expression_parser11applicationEv: # @_ZN24lambda_expression_parser11applicationEv
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 48
.Lcfi40:
	.cfi_offset %rbx, -32
.Lcfi41:
	.cfi_offset %r14, -24
.Lcfi42:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_1
# BB#2:
	leaq	8(%rsp), %rsi
	callq	_ZN12token_stream9get_tokenEPPc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-1(%rax), %ecx
	cmpl	$10, %ecx
	ja	.LBB6_9
# BB#3:
	xorl	%r14d, %r14d
	jmpq	*.LJTI6_0(,%rcx,8)
.LBB6_5:
	movq	(%rbx), %rdi
	movq	8(%rsp), %rdx
	movl	%eax, %esi
	callq	_ZN12token_stream10push_tokenENS_10token_typeEPc
	movq	%rbx, %rdi
	callq	_ZN24lambda_expression_parser5alistEv
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZN24lambda_expression_parser6lambdaEv
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB6_10
# BB#6:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp21:
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	_ZN8app_nodeC1EP8exp_nodeS1_s
.Ltmp22:
# BB#7:
	movq	%r15, %r14
	jmp	.LBB6_10
.LBB6_1:
	xorl	%r14d, %r14d
	jmp	.LBB6_10
.LBB6_9:
	movq	8(%rsp), %rax
	testq	%rax, %rax
	movl	$.L.str.23, %edx
	cmovneq	%rax, %rdx
	xorl	%r14d, %r14d
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	_ZN24lambda_expression_parser8dderrmsgEPcz
	jmp	.LBB6_10
.LBB6_4:
	movq	(%rbx), %rdi
	movq	8(%rsp), %rdx
	movl	$9, %esi
	callq	_ZN12token_stream10push_tokenENS_10token_typeEPc
	movq	%rbx, %rdi
	callq	_ZN24lambda_expression_parser6lambdaEv
	movq	%rax, %r14
.LBB6_10:
	movq	%r14, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB6_8:
.Ltmp23:
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN24lambda_expression_parser11applicationEv, .Lfunc_end6-_ZN24lambda_expression_parser11applicationEv
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI6_0:
	.quad	.LBB6_5
	.quad	.LBB6_9
	.quad	.LBB6_9
	.quad	.LBB6_9
	.quad	.LBB6_9
	.quad	.LBB6_9
	.quad	.LBB6_9
	.quad	.LBB6_5
	.quad	.LBB6_4
	.quad	.LBB6_9
	.quad	.LBB6_10
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp21-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin3   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Lfunc_end6-.Ltmp22     #   Call between .Ltmp22 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN24lambda_expression_parser6lambdaEv
	.p2align	4, 0x90
	.type	_ZN24lambda_expression_parser6lambdaEv,@function
_ZN24lambda_expression_parser6lambdaEv: # @_ZN24lambda_expression_parser6lambdaEv
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 48
.Lcfi47:
	.cfi_offset %rbx, -32
.Lcfi48:
	.cfi_offset %r14, -24
.Lcfi49:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB7_1
# BB#3:
	leaq	8(%rsp), %rsi
	callq	_ZN12token_stream9get_tokenEPPc
	cmpl	$9, %eax
	je	.LBB7_6
# BB#4:
	cmpl	$11, %eax
	je	.LBB7_1
# BB#5:
	movq	(%r14), %rdi
	movq	8(%rsp), %rdx
	movl	%eax, %esi
	callq	_ZN12token_stream10push_tokenENS_10token_typeEPc
.LBB7_1:
	xorl	%ebx, %ebx
.LBB7_2:
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB7_6:
	movq	(%r14), %rdi
	leaq	8(%rsp), %rsi
	callq	_ZN12token_stream9get_tokenEPPc
	cmpl	$8, %eax
	jne	.LBB7_7
# BB#9:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %r15
	movq	8(%rsp), %rsi
.Ltmp24:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	callq	_ZN8arg_nodeC1EPKcPK8exp_nodes
.Ltmp25:
# BB#10:
	movq	(%r14), %rdi
	leaq	8(%rsp), %rsi
	callq	_ZN12token_stream9get_tokenEPPc
	cmpl	$10, %eax
	jne	.LBB7_11
# BB#12:
	movq	%r14, %rdi
	callq	_ZN24lambda_expression_parser11applicationEv
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB7_13
# BB#14:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp27:
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	_ZN8lam_nodeC1EP8arg_nodeP8exp_nodes
.Ltmp28:
	jmp	.LBB7_2
.LBB7_7:
	movq	8(%rsp), %rax
	testq	%rax, %rax
	movl	$.L.str.23, %edx
	cmovneq	%rax, %rdx
	xorl	%ebx, %ebx
	movl	$.L.str.24, %esi
	jmp	.LBB7_8
.LBB7_11:
	movq	8(%rsp), %rax
	testq	%rax, %rax
	movl	$.L.str.23, %edx
	cmovneq	%rax, %rdx
	xorl	%ebx, %ebx
	movl	$.L.str.25, %esi
.LBB7_8:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	_ZN24lambda_expression_parser8dderrmsgEPcz
	jmp	.LBB7_2
.LBB7_13:
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*16(%rax)
	jmp	.LBB7_1
.LBB7_15:
.Ltmp29:
	movq	%rax, %r14
	movq	%rbx, %rdi
	jmp	.LBB7_17
.LBB7_16:
.Ltmp26:
	movq	%rax, %r14
	movq	%r15, %rdi
.LBB7_17:
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN24lambda_expression_parser6lambdaEv, .Lfunc_end7-_ZN24lambda_expression_parser6lambdaEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp24-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin4   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp27-.Ltmp25         #   Call between .Ltmp25 and .Ltmp27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin4   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Lfunc_end7-.Ltmp28     #   Call between .Ltmp28 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN24lambda_expression_parser5alistEv
	.p2align	4, 0x90
	.type	_ZN24lambda_expression_parser5alistEv,@function
_ZN24lambda_expression_parser5alistEv:  # @_ZN24lambda_expression_parser5alistEv
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi55:
	.cfi_def_cfa_offset 64
.Lcfi56:
	.cfi_offset %rbx, -48
.Lcfi57:
	.cfi_offset %r12, -40
.Lcfi58:
	.cfi_offset %r13, -32
.Lcfi59:
	.cfi_offset %r14, -24
.Lcfi60:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	cmpq	$0, (%r12)
	je	.LBB8_1
# BB#3:
	xorl	%ebx, %ebx
	leaq	8(%rsp), %r14
	jmp	.LBB8_4
	.p2align	4, 0x90
.LBB8_8:                                #   in Loop: Header=BB8_4 Depth=1
	testq	%r15, %r15
	je	.LBB8_4
# BB#9:                                 #   in Loop: Header=BB8_4 Depth=1
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp30:
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	_ZN8app_nodeC1EP8exp_nodeS1_s
.Ltmp31:
# BB#10:                                #   in Loop: Header=BB8_4 Depth=1
	movq	%r13, %rbx
.LBB8_4:                                # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_5 Depth 2
	movq	%rbx, %r15
	.p2align	4, 0x90
.LBB8_5:                                #   Parent Loop BB8_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12), %rdi
	movq	%r14, %rsi
	callq	_ZN12token_stream9get_tokenEPPc
	movl	%eax, %ebx
	movq	(%r12), %rdi
	movq	8(%rsp), %rdx
	movl	%ebx, %esi
	callq	_ZN12token_stream10push_tokenENS_10token_typeEPc
	cmpl	$8, %ebx
	je	.LBB8_7
# BB#6:                                 #   in Loop: Header=BB8_5 Depth=2
	cmpl	$1, %ebx
	jne	.LBB8_2
.LBB8_7:                                #   in Loop: Header=BB8_5 Depth=2
	movq	%r12, %rdi
	callq	_ZN24lambda_expression_parser4atomEv
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB8_5
	jmp	.LBB8_8
.LBB8_1:
	xorl	%r15d, %r15d
.LBB8_2:
	movq	%r15, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB8_11:
.Ltmp32:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN24lambda_expression_parser5alistEv, .Lfunc_end8-_ZN24lambda_expression_parser5alistEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp30-.Lfunc_begin5   #   Call between .Lfunc_begin5 and .Ltmp30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin5   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Lfunc_end8-.Ltmp31     #   Call between .Ltmp31 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN24lambda_expression_parser8dderrmsgEPcz
	.p2align	4, 0x90
	.type	_ZN24lambda_expression_parser8dderrmsgEPcz,@function
_ZN24lambda_expression_parser8dderrmsgEPcz: # @_ZN24lambda_expression_parser8dderrmsgEPcz
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 16
	subq	$208, %rsp
.Lcfi62:
	.cfi_def_cfa_offset 224
.Lcfi63:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	testb	%al, %al
	je	.LBB9_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB9_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	224(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$16, (%rsp)
	movq	(%rdi), %rax
	movl	8744(%rax), %esi
	testl	%esi, %esi
	js	.LBB9_4
# BB#3:
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	printf
.LBB9_4:
	movq	stdout(%rip), %rdi
	movq	%rsp, %rdx
	movq	%rbx, %rsi
	callq	vfprintf
	movl	$10, %edi
	callq	putchar
	movq	stderr(%rip), %rdi
	callq	fflush
	addq	$208, %rsp
	popq	%rbx
	retq
.Lfunc_end9:
	.size	_ZN24lambda_expression_parser8dderrmsgEPcz, .Lfunc_end9-_ZN24lambda_expression_parser8dderrmsgEPcz
	.cfi_endproc

	.globl	_ZN24lambda_expression_parser4atomEv
	.p2align	4, 0x90
	.type	_ZN24lambda_expression_parser4atomEv,@function
_ZN24lambda_expression_parser4atomEv:   # @_ZN24lambda_expression_parser4atomEv
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi66:
	.cfi_def_cfa_offset 32
.Lcfi67:
	.cfi_offset %rbx, -24
.Lcfi68:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_8
# BB#1:
	movq	%rsp, %rsi
	callq	_ZN12token_stream9get_tokenEPPc
	cmpl	$8, %eax
	je	.LBB10_5
# BB#2:
	cmpl	$1, %eax
	jne	.LBB10_7
# BB#3:
	movq	%rbx, %rdi
	callq	_ZN24lambda_expression_parser11applicationEv
	movq	%rax, %r14
	movq	(%rbx), %rdi
	movq	%rsp, %rsi
	callq	_ZN12token_stream9get_tokenEPPc
	cmpl	$2, %eax
	je	.LBB10_9
# BB#4:
	movq	(%rsp), %rax
	testq	%rax, %rax
	movl	$.L.str.23, %edx
	cmovneq	%rax, %rdx
	movl	$.L.str.26, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	_ZN24lambda_expression_parser8dderrmsgEPcz
	jmp	.LBB10_9
.LBB10_5:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r14
	movq	(%rsp), %rsi
.Ltmp33:
	movq	%r14, %rdi
	callq	_ZN8var_nodeC1EPKc
.Ltmp34:
	jmp	.LBB10_9
.LBB10_7:
	movq	(%rbx), %rdi
	movq	(%rsp), %rdx
	movl	%eax, %esi
	callq	_ZN12token_stream10push_tokenENS_10token_typeEPc
.LBB10_8:
	xorl	%r14d, %r14d
.LBB10_9:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB10_6:
.Ltmp35:
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZN24lambda_expression_parser4atomEv, .Lfunc_end10-_ZN24lambda_expression_parser4atomEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp33-.Lfunc_begin6   #   Call between .Lfunc_begin6 and .Ltmp33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin6   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Lfunc_end10-.Ltmp34    #   Call between .Ltmp34 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN24lambda_expression_parser11set_tok_strEP12token_stream
	.p2align	4, 0x90
	.type	_ZN24lambda_expression_parser11set_tok_strEP12token_stream,@function
_ZN24lambda_expression_parser11set_tok_strEP12token_stream: # @_ZN24lambda_expression_parser11set_tok_strEP12token_stream
	.cfi_startproc
# BB#0:
	movq	%rsi, (%rdi)
	retq
.Lfunc_end11:
	.size	_ZN24lambda_expression_parser11set_tok_strEP12token_stream, .Lfunc_end11-_ZN24lambda_expression_parser11set_tok_strEP12token_stream
	.cfi_endproc

	.globl	_ZN24lambda_expression_parser5resetEv
	.p2align	4, 0x90
	.type	_ZN24lambda_expression_parser5resetEv,@function
_ZN24lambda_expression_parser5resetEv:  # @_ZN24lambda_expression_parser5resetEv
	.cfi_startproc
# BB#0:
	movl	$0, 8(%rdi)
	retq
.Lfunc_end12:
	.size	_ZN24lambda_expression_parser5resetEv, .Lfunc_end12-_ZN24lambda_expression_parser5resetEv
	.cfi_endproc

	.type	trace_lambda,@object    # @trace_lambda
	.bss
	.globl	trace_lambda
	.p2align	2
trace_lambda:
	.long	0                       # 0x0
	.size	trace_lambda, 4

	.type	step_lambda,@object     # @step_lambda
	.globl	step_lambda
	.p2align	2
step_lambda:
	.long	0                       # 0x0
	.size	step_lambda, 4

	.type	print_symbols,@object   # @print_symbols
	.data
	.globl	print_symbols
	.p2align	2
print_symbols:
	.long	1                       # 0x1
	.size	print_symbols, 4

	.type	applicative_order,@object # @applicative_order
	.bss
	.globl	applicative_order
	.p2align	2
applicative_order:
	.long	0                       # 0x0
	.size	applicative_order, 4

	.type	reduce_body,@object     # @reduce_body
	.globl	reduce_body
	.p2align	2
reduce_body:
	.long	0                       # 0x0
	.size	reduce_body, 4

	.type	brief_print,@object     # @brief_print
	.data
	.globl	brief_print
	.p2align	2
brief_print:
	.long	1                       # 0x1
	.size	brief_print, 4

	.type	step_thru,@object       # @step_thru
	.bss
	.globl	step_thru
	.p2align	2
step_thru:
	.long	0                       # 0x0
	.size	step_thru, 4

	.type	extract_eta,@object     # @extract_eta
	.data
	.globl	extract_eta
	.p2align	2
extract_eta:
	.long	1                       # 0x1
	.size	extract_eta, 4

	.type	extract_app,@object     # @extract_app
	.bss
	.globl	extract_app
	.p2align	2
extract_app:
	.long	0                       # 0x0
	.size	extract_app, 4

	.type	reduce_fully,@object    # @reduce_fully
	.data
	.globl	reduce_fully
	.p2align	2
reduce_fully:
	.long	1                       # 0x1
	.size	reduce_fully, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"QUIT"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"trace"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"step"
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"thru"
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"sym"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"app"
	.size	.L.str.5, 4

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"body"
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"brief"
	.size	.L.str.7, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"eta"
	.size	.L.str.8, 4

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"xapp"
	.size	.L.str.9, 5

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"full"
	.size	.L.str.10, 5

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	">trace = %d\n"
	.size	.L.str.11, 13

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	">step  = %d\n"
	.size	.L.str.12, 13

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	">thru  = %d\n"
	.size	.L.str.13, 13

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	">app   = %d\n"
	.size	.L.str.14, 13

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	">body  = %d\n"
	.size	.L.str.15, 13

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	">brief = %d\n"
	.size	.L.str.16, 13

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	">sym   = %d\n"
	.size	.L.str.17, 13

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	">eta   = %d\n"
	.size	.L.str.18, 13

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	">xapp  = %d\n"
	.size	.L.str.19, 13

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	">full  = %d\n"
	.size	.L.str.20, 13

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"expresion expected: got %s"
	.size	.L.str.22, 27

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"(*null*)"
	.size	.L.str.23, 9

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"NAME expected: got %s"
	.size	.L.str.24, 22

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"PERIOD expected: got %s"
	.size	.L.str.25, 24

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	") expected: got %s"
	.size	.L.str.26, 19

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"\""
	.size	.L.str.27, 2

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"~"
	.size	.L.str.28, 2

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"\n*** line %d: "
	.size	.L.str.29, 15


	.globl	_ZN24lambda_expression_parserC1EP12token_stream
	.type	_ZN24lambda_expression_parserC1EP12token_stream,@function
_ZN24lambda_expression_parserC1EP12token_stream = _ZN24lambda_expression_parserC2EP12token_stream
	.globl	_ZN24lambda_expression_parserD1Ev
	.type	_ZN24lambda_expression_parserD1Ev,@function
_ZN24lambda_expression_parserD1Ev = _ZN24lambda_expression_parserD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
