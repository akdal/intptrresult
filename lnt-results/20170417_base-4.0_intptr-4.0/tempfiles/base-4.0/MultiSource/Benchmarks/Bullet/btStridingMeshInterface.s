	.text
	.file	"btStridingMeshInterface.bc"
	.globl	_ZN23btStridingMeshInterfaceD2Ev
	.p2align	4, 0x90
	.type	_ZN23btStridingMeshInterfaceD2Ev,@function
_ZN23btStridingMeshInterfaceD2Ev:       # @_ZN23btStridingMeshInterfaceD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	_ZN23btStridingMeshInterfaceD2Ev, .Lfunc_end0-_ZN23btStridingMeshInterfaceD2Ev
	.cfi_endproc

	.globl	_ZN23btStridingMeshInterfaceD0Ev
	.p2align	4, 0x90
	.type	_ZN23btStridingMeshInterfaceD0Ev,@function
_ZN23btStridingMeshInterfaceD0Ev:       # @_ZN23btStridingMeshInterfaceD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end1:
	.size	_ZN23btStridingMeshInterfaceD0Ev, .Lfunc_end1-_ZN23btStridingMeshInterfaceD0Ev
	.cfi_endproc

	.globl	_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_,@function
_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_: # @_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 176
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	(%r14), %rax
	callq	*56(%rax)
	movl	%eax, 108(%rsp)         # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB2_21
# BB#1:                                 # %.lr.ph232
	movss	8(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movss	12(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movss	16(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	xorl	%ebp, %ebp
	leaq	32(%rsp), %r13
	.p2align	4, 0x90
.LBB2_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_8 Depth 2
                                        #     Child Loop BB2_11 Depth 2
                                        #     Child Loop BB2_16 Depth 2
                                        #     Child Loop BB2_19 Depth 2
	movq	(%r14), %rax
	movq	%r14, %rdi
	leaq	96(%rsp), %rsi
	leaq	116(%rsp), %rdx
	leaq	112(%rsp), %rcx
	leaq	24(%rsp), %r8
	leaq	88(%rsp), %r9
	pushq	%rbp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	leaq	92(%rsp), %rbx
	pushq	%rbx
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	leaq	36(%rsp), %rbx
	pushq	%rbx
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	leaq	52(%rsp), %rbx
	pushq	%rbx
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	*32(%rax)
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
	movl	112(%rsp), %eax
	cmpl	$1, %eax
	je	.LBB2_12
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	testl	%eax, %eax
	jne	.LBB2_20
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	movl	84(%rsp), %eax
	cmpl	$2, %eax
	je	.LBB2_9
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
	cmpl	$3, %eax
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	jne	.LBB2_20
# BB#6:                                 # %.preheader213
                                        #   in Loop: Header=BB2_2 Depth=1
	cmpl	$0, 20(%rsp)
	jle	.LBB2_20
# BB#7:                                 # %.lr.ph223.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_8:                                # %.lr.ph223
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	88(%rsp), %rcx
	movslq	28(%rsp), %rdx
	movslq	%ebx, %rbx
	imulq	%rbx, %rdx
	movq	96(%rsp), %rax
	movzwl	(%rcx,%rdx), %edi
	movslq	24(%rsp), %rsi
	imulq	%rsi, %rdi
	movss	(%rax,%rdi), %xmm0      # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movss	4(%rax,%rdi), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	movss	8(%rax,%rdi), %xmm2     # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	movss	%xmm0, 32(%rsp)
	movss	%xmm1, 36(%rsp)
	movss	%xmm2, 40(%rsp)
	movl	$0, 44(%rsp)
	movzwl	2(%rcx,%rdx), %edi
	imulq	%rsi, %rdi
	movss	(%rax,%rdi), %xmm0      # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movss	4(%rax,%rdi), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	movss	8(%rax,%rdi), %xmm2     # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	movss	%xmm0, 48(%rsp)
	movss	%xmm1, 52(%rsp)
	movss	%xmm2, 56(%rsp)
	movl	$0, 60(%rsp)
	movzwl	4(%rcx,%rdx), %ecx
	imulq	%rsi, %rcx
	movss	(%rax,%rcx), %xmm0      # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movss	4(%rax,%rcx), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	movss	8(%rax,%rcx), %xmm2     # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	movss	%xmm0, 64(%rsp)
	movss	%xmm1, 68(%rsp)
	movss	%xmm2, 72(%rsp)
	movl	$0, 76(%rsp)
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	%ebp, %edx
	movl	%ebx, %ecx
	callq	*16(%rax)
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	incl	%ebx
	cmpl	20(%rsp), %ebx
	jl	.LBB2_8
	jmp	.LBB2_20
	.p2align	4, 0x90
.LBB2_12:                               #   in Loop: Header=BB2_2 Depth=1
	movl	84(%rsp), %eax
	cmpl	$2, %eax
	je	.LBB2_17
# BB#13:                                #   in Loop: Header=BB2_2 Depth=1
	cmpl	$3, %eax
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	jne	.LBB2_20
# BB#14:                                # %.preheader217
                                        #   in Loop: Header=BB2_2 Depth=1
	cmpl	$0, 20(%rsp)
	jle	.LBB2_20
# BB#15:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_16:                               # %.lr.ph
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	88(%rsp), %rcx
	movslq	28(%rsp), %rdx
	movslq	%r12d, %r12
	imulq	%r12, %rdx
	movq	96(%rsp), %rax
	movzwl	(%rcx,%rdx), %edi
	movslq	24(%rsp), %rsi
	imulq	%rsi, %rdi
	movsd	(%rax,%rdi), %xmm0      # xmm0 = mem[0],zero
	movsd	8(%rax,%rdi), %xmm1     # xmm1 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	mulss	%xmm3, %xmm0
	cvtsd2ss	%xmm1, %xmm1
	mulss	%xmm4, %xmm1
	movsd	16(%rax,%rdi), %xmm2    # xmm2 = mem[0],zero
	cvtsd2ss	%xmm2, %xmm2
	mulss	%xmm5, %xmm2
	movss	%xmm0, 32(%rsp)
	movss	%xmm1, 36(%rsp)
	movss	%xmm2, 40(%rsp)
	movl	$0, 44(%rsp)
	movzwl	2(%rcx,%rdx), %edi
	imulq	%rsi, %rdi
	movsd	(%rax,%rdi), %xmm0      # xmm0 = mem[0],zero
	movsd	8(%rax,%rdi), %xmm1     # xmm1 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	mulss	%xmm3, %xmm0
	cvtsd2ss	%xmm1, %xmm1
	mulss	%xmm4, %xmm1
	movsd	16(%rax,%rdi), %xmm2    # xmm2 = mem[0],zero
	cvtsd2ss	%xmm2, %xmm2
	mulss	%xmm5, %xmm2
	movss	%xmm0, 48(%rsp)
	movss	%xmm1, 52(%rsp)
	movss	%xmm2, 56(%rsp)
	movl	$0, 60(%rsp)
	movzwl	4(%rcx,%rdx), %ecx
	imulq	%rsi, %rcx
	movsd	(%rax,%rcx), %xmm0      # xmm0 = mem[0],zero
	movsd	8(%rax,%rcx), %xmm1     # xmm1 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	mulss	%xmm3, %xmm0
	cvtsd2ss	%xmm1, %xmm1
	mulss	%xmm4, %xmm1
	movsd	16(%rax,%rcx), %xmm2    # xmm2 = mem[0],zero
	cvtsd2ss	%xmm2, %xmm2
	mulss	%xmm5, %xmm2
	movss	%xmm0, 64(%rsp)
	movss	%xmm1, 68(%rsp)
	movss	%xmm2, 72(%rsp)
	movl	$0, 76(%rsp)
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	%ebp, %edx
	movl	%r12d, %ecx
	callq	*16(%rax)
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	incl	%r12d
	cmpl	20(%rsp), %r12d
	jl	.LBB2_16
	jmp	.LBB2_20
.LBB2_9:                                # %.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	cmpl	$0, 20(%rsp)
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	jle	.LBB2_20
# BB#10:                                # %.lr.ph225.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_11:                               # %.lr.ph225
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	88(%rsp), %rdx
	movslq	28(%rsp), %rsi
	movslq	%ebx, %rbx
	imulq	%rbx, %rsi
	movq	96(%rsp), %rax
	movl	24(%rsp), %ecx
	movl	(%rdx,%rsi), %edi
	imull	%ecx, %edi
	movss	(%rax,%rdi), %xmm0      # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movss	4(%rax,%rdi), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	movss	8(%rax,%rdi), %xmm2     # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	movss	%xmm0, 32(%rsp)
	movss	%xmm1, 36(%rsp)
	movss	%xmm2, 40(%rsp)
	movl	$0, 44(%rsp)
	movl	4(%rdx,%rsi), %edi
	imull	%ecx, %edi
	movss	(%rax,%rdi), %xmm0      # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movss	4(%rax,%rdi), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	movss	8(%rax,%rdi), %xmm2     # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	movss	%xmm0, 48(%rsp)
	movss	%xmm1, 52(%rsp)
	movss	%xmm2, 56(%rsp)
	movl	$0, 60(%rsp)
	imull	8(%rdx,%rsi), %ecx
	movss	(%rax,%rcx), %xmm0      # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movss	4(%rax,%rcx), %xmm1     # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	movss	8(%rax,%rcx), %xmm2     # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	movss	%xmm0, 64(%rsp)
	movss	%xmm1, 68(%rsp)
	movss	%xmm2, 72(%rsp)
	movl	$0, 76(%rsp)
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	%ebp, %edx
	movl	%ebx, %ecx
	callq	*16(%rax)
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	incl	%ebx
	cmpl	20(%rsp), %ebx
	jl	.LBB2_11
	jmp	.LBB2_20
.LBB2_17:                               # %.preheader215
                                        #   in Loop: Header=BB2_2 Depth=1
	cmpl	$0, 20(%rsp)
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	jle	.LBB2_20
# BB#18:                                # %.lr.ph221.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_19:                               # %.lr.ph221
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	88(%rsp), %rdx
	movslq	28(%rsp), %rsi
	movslq	%ebx, %rbx
	imulq	%rbx, %rsi
	movq	96(%rsp), %rax
	movl	24(%rsp), %ecx
	movl	(%rdx,%rsi), %edi
	imull	%ecx, %edi
	movsd	(%rax,%rdi), %xmm0      # xmm0 = mem[0],zero
	movsd	8(%rax,%rdi), %xmm1     # xmm1 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	mulss	%xmm3, %xmm0
	cvtsd2ss	%xmm1, %xmm1
	mulss	%xmm4, %xmm1
	movsd	16(%rax,%rdi), %xmm2    # xmm2 = mem[0],zero
	cvtsd2ss	%xmm2, %xmm2
	mulss	%xmm5, %xmm2
	movss	%xmm0, 32(%rsp)
	movss	%xmm1, 36(%rsp)
	movss	%xmm2, 40(%rsp)
	movl	$0, 44(%rsp)
	movl	4(%rdx,%rsi), %edi
	imull	%ecx, %edi
	movsd	(%rax,%rdi), %xmm0      # xmm0 = mem[0],zero
	movsd	8(%rax,%rdi), %xmm1     # xmm1 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	mulss	%xmm3, %xmm0
	cvtsd2ss	%xmm1, %xmm1
	mulss	%xmm4, %xmm1
	movsd	16(%rax,%rdi), %xmm2    # xmm2 = mem[0],zero
	cvtsd2ss	%xmm2, %xmm2
	mulss	%xmm5, %xmm2
	movss	%xmm0, 48(%rsp)
	movss	%xmm1, 52(%rsp)
	movss	%xmm2, 56(%rsp)
	movl	$0, 60(%rsp)
	imull	8(%rdx,%rsi), %ecx
	movsd	(%rax,%rcx), %xmm0      # xmm0 = mem[0],zero
	movsd	8(%rax,%rcx), %xmm1     # xmm1 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	mulss	%xmm3, %xmm0
	cvtsd2ss	%xmm1, %xmm1
	mulss	%xmm4, %xmm1
	movsd	16(%rax,%rcx), %xmm2    # xmm2 = mem[0],zero
	cvtsd2ss	%xmm2, %xmm2
	mulss	%xmm5, %xmm2
	movss	%xmm0, 64(%rsp)
	movss	%xmm1, 68(%rsp)
	movss	%xmm2, 72(%rsp)
	movl	$0, 76(%rsp)
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	%ebp, %edx
	movl	%ebx, %ecx
	callq	*16(%rax)
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	incl	%ebx
	cmpl	20(%rsp), %ebx
	jl	.LBB2_19
	.p2align	4, 0x90
.LBB2_20:                               # %.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	*48(%rax)
	incl	%ebp
	cmpl	108(%rsp), %ebp         # 4-byte Folded Reload
	jne	.LBB2_2
.LBB2_21:                               # %._crit_edge
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_, .Lfunc_end2-_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_
	.cfi_endproc

	.globl	_ZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_
	.p2align	4, 0x90
	.type	_ZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_,@function
_ZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_: # @_ZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 64
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	$_ZTVZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback+16, (%rsp)
	movl	$1566444395, 8(%rsp)    # imm = 0x5D5E0B6B
	movl	$1566444395, 12(%rsp)   # imm = 0x5D5E0B6B
	movl	$1566444395, 16(%rsp)   # imm = 0x5D5E0B6B
	movl	$0, 20(%rsp)
	movl	$-581039253, 24(%rsp)   # imm = 0xDD5E0B6B
	movl	$-581039253, 28(%rsp)   # imm = 0xDD5E0B6B
	movl	$-581039253, 32(%rsp)   # imm = 0xDD5E0B6B
	movl	$0, 36(%rsp)
	movl	$-581039253, (%rbx)     # imm = 0xDD5E0B6B
	movl	$-581039253, 4(%rbx)    # imm = 0xDD5E0B6B
	movl	$-581039253, 8(%rbx)    # imm = 0xDD5E0B6B
	movl	$0, 12(%rbx)
	movabsq	$6727827449093950315, %rax # imm = 0x5D5E0B6B5D5E0B6B
	movq	%rax, (%r14)
	movq	$1566444395, 8(%r14)    # imm = 0x5D5E0B6B
	movq	(%rdi), %rax
.Ltmp0:
	movq	%rsp, %rsi
	movq	%rbx, %rdx
	movq	%r14, %rcx
	callq	*16(%rax)
.Ltmp1:
# BB#1:
	leaq	8(%rsp), %rax
	movups	(%rax), %xmm0
	movups	%xmm0, (%rbx)
	movups	16(%rax), %xmm0
	movups	%xmm0, (%r14)
	movq	%rsp, %rdi
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB3_2:
.Ltmp2:
	movq	%rax, %rbx
.Ltmp3:
	movq	%rsp, %rdi
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp4:
# BB#3:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB3_4:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_, .Lfunc_end3-_ZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end4:
	.size	__clang_call_terminate, .Lfunc_end4-__clang_call_terminate

	.section	.text._ZNK23btStridingMeshInterface14hasPremadeAabbEv,"axG",@progbits,_ZNK23btStridingMeshInterface14hasPremadeAabbEv,comdat
	.weak	_ZNK23btStridingMeshInterface14hasPremadeAabbEv
	.p2align	4, 0x90
	.type	_ZNK23btStridingMeshInterface14hasPremadeAabbEv,@function
_ZNK23btStridingMeshInterface14hasPremadeAabbEv: # @_ZNK23btStridingMeshInterface14hasPremadeAabbEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	_ZNK23btStridingMeshInterface14hasPremadeAabbEv, .Lfunc_end5-_ZNK23btStridingMeshInterface14hasPremadeAabbEv
	.cfi_endproc

	.section	.text._ZNK23btStridingMeshInterface14setPremadeAabbERK9btVector3S2_,"axG",@progbits,_ZNK23btStridingMeshInterface14setPremadeAabbERK9btVector3S2_,comdat
	.weak	_ZNK23btStridingMeshInterface14setPremadeAabbERK9btVector3S2_
	.p2align	4, 0x90
	.type	_ZNK23btStridingMeshInterface14setPremadeAabbERK9btVector3S2_,@function
_ZNK23btStridingMeshInterface14setPremadeAabbERK9btVector3S2_: # @_ZNK23btStridingMeshInterface14setPremadeAabbERK9btVector3S2_
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end6:
	.size	_ZNK23btStridingMeshInterface14setPremadeAabbERK9btVector3S2_, .Lfunc_end6-_ZNK23btStridingMeshInterface14setPremadeAabbERK9btVector3S2_
	.cfi_endproc

	.section	.text._ZNK23btStridingMeshInterface14getPremadeAabbEP9btVector3S1_,"axG",@progbits,_ZNK23btStridingMeshInterface14getPremadeAabbEP9btVector3S1_,comdat
	.weak	_ZNK23btStridingMeshInterface14getPremadeAabbEP9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK23btStridingMeshInterface14getPremadeAabbEP9btVector3S1_,@function
_ZNK23btStridingMeshInterface14getPremadeAabbEP9btVector3S1_: # @_ZNK23btStridingMeshInterface14getPremadeAabbEP9btVector3S1_
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end7:
	.size	_ZNK23btStridingMeshInterface14getPremadeAabbEP9btVector3S1_, .Lfunc_end7-_ZNK23btStridingMeshInterface14getPremadeAabbEP9btVector3S1_
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallbackD0Ev,@function
_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallbackD0Ev: # @_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallbackD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp6:
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp7:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB8_2:
.Ltmp8:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallbackD0Ev, .Lfunc_end8-_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Lfunc_end8-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallback28internalProcessTriangleIndexEPS0_ii,@function
_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallback28internalProcessTriangleIndexEPS0_ii: # @_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallback28internalProcessTriangleIndexEPS0_ii
	.cfi_startproc
# BB#0:
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm8          # xmm8 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm8
	jbe	.LBB9_2
# BB#1:
	movss	%xmm1, 8(%rdi)
	movaps	%xmm1, %xmm8
.LBB9_2:                                # %_Z8btSetMinIfEvRT_RKS0_.exit.i
	movss	4(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	12(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm1
	jbe	.LBB9_4
# BB#3:
	movss	%xmm2, 12(%rdi)
	movaps	%xmm2, %xmm1
.LBB9_4:                                # %_Z8btSetMinIfEvRT_RKS0_.exit7.i
	movss	8(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	16(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm2
	jbe	.LBB9_6
# BB#5:
	movss	%xmm3, 16(%rdi)
	movaps	%xmm3, %xmm2
.LBB9_6:                                # %_Z8btSetMinIfEvRT_RKS0_.exit6.i
	movss	12(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	20(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm4, %xmm3
	jbe	.LBB9_8
# BB#7:
	movss	%xmm4, 20(%rdi)
	movaps	%xmm4, %xmm3
.LBB9_8:                                # %_ZN9btVector36setMinERKS_.exit
	movss	24(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm5           # xmm5 = mem[0],zero,zero,zero
	ucomiss	%xmm4, %xmm5
	jbe	.LBB9_10
# BB#9:
	movss	%xmm5, 24(%rdi)
	movaps	%xmm5, %xmm4
.LBB9_10:                               # %_Z8btSetMaxIfEvRT_RKS0_.exit.i19
	movss	28(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	ucomiss	%xmm5, %xmm6
	jbe	.LBB9_12
# BB#11:
	movss	%xmm6, 28(%rdi)
	movaps	%xmm6, %xmm5
.LBB9_12:                               # %_Z8btSetMaxIfEvRT_RKS0_.exit7.i20
	movss	32(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm7          # xmm7 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm7
	jbe	.LBB9_14
# BB#13:
	movss	%xmm7, 32(%rdi)
	movaps	%xmm7, %xmm6
.LBB9_14:                               # %_Z8btSetMaxIfEvRT_RKS0_.exit6.i21
	movss	36(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm7
	jbe	.LBB9_16
# BB#15:
	movss	%xmm7, 36(%rdi)
	movaps	%xmm7, %xmm0
.LBB9_16:                               # %_ZN9btVector36setMaxERKS_.exit22
	movss	16(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	ucomiss	%xmm7, %xmm8
	jbe	.LBB9_18
# BB#17:
	movss	%xmm7, 8(%rdi)
	movaps	%xmm7, %xmm8
.LBB9_18:                               # %_Z8btSetMinIfEvRT_RKS0_.exit.i15
	movss	20(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	ucomiss	%xmm7, %xmm1
	jbe	.LBB9_20
# BB#19:
	movss	%xmm7, 12(%rdi)
	movaps	%xmm7, %xmm1
.LBB9_20:                               # %_Z8btSetMinIfEvRT_RKS0_.exit7.i16
	movss	24(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	ucomiss	%xmm7, %xmm2
	jbe	.LBB9_22
# BB#21:
	movss	%xmm7, 16(%rdi)
	movaps	%xmm7, %xmm2
.LBB9_22:                               # %_Z8btSetMinIfEvRT_RKS0_.exit6.i17
	movss	28(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	ucomiss	%xmm7, %xmm3
	jbe	.LBB9_24
# BB#23:
	movss	%xmm7, 20(%rdi)
	movaps	%xmm7, %xmm3
.LBB9_24:                               # %_ZN9btVector36setMinERKS_.exit18
	movss	16(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	ucomiss	%xmm4, %xmm7
	jbe	.LBB9_26
# BB#25:
	movss	%xmm7, 24(%rdi)
	movaps	%xmm7, %xmm4
.LBB9_26:                               # %_Z8btSetMaxIfEvRT_RKS0_.exit.i11
	movss	20(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	ucomiss	%xmm5, %xmm7
	jbe	.LBB9_28
# BB#27:
	movss	%xmm7, 28(%rdi)
	movaps	%xmm7, %xmm5
.LBB9_28:                               # %_Z8btSetMaxIfEvRT_RKS0_.exit7.i12
	movss	24(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm7
	jbe	.LBB9_30
# BB#29:
	movss	%xmm7, 32(%rdi)
	movaps	%xmm7, %xmm6
.LBB9_30:                               # %_Z8btSetMaxIfEvRT_RKS0_.exit6.i13
	movss	28(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm7
	jbe	.LBB9_32
# BB#31:
	movss	%xmm7, 36(%rdi)
	movaps	%xmm7, %xmm0
.LBB9_32:                               # %_ZN9btVector36setMaxERKS_.exit14
	movss	32(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	ucomiss	%xmm7, %xmm8
	jbe	.LBB9_34
# BB#33:
	movss	%xmm7, 8(%rdi)
.LBB9_34:                               # %_Z8btSetMinIfEvRT_RKS0_.exit.i7
	movss	36(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	ucomiss	%xmm7, %xmm1
	jbe	.LBB9_36
# BB#35:
	movss	%xmm7, 12(%rdi)
.LBB9_36:                               # %_Z8btSetMinIfEvRT_RKS0_.exit7.i8
	movss	40(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jbe	.LBB9_38
# BB#37:
	movss	%xmm1, 16(%rdi)
.LBB9_38:                               # %_Z8btSetMinIfEvRT_RKS0_.exit6.i9
	movss	44(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm3
	jbe	.LBB9_40
# BB#39:
	movss	%xmm1, 20(%rdi)
.LBB9_40:                               # %_ZN9btVector36setMinERKS_.exit10
	movss	32(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm4, %xmm1
	jbe	.LBB9_42
# BB#41:
	movss	%xmm1, 24(%rdi)
.LBB9_42:                               # %_Z8btSetMaxIfEvRT_RKS0_.exit.i
	movss	36(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm5, %xmm1
	jbe	.LBB9_44
# BB#43:
	movss	%xmm1, 28(%rdi)
.LBB9_44:                               # %_Z8btSetMaxIfEvRT_RKS0_.exit7.i
	movss	40(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm6, %xmm1
	jbe	.LBB9_46
# BB#45:
	movss	%xmm1, 32(%rdi)
.LBB9_46:                               # %_Z8btSetMaxIfEvRT_RKS0_.exit6.i
	movss	44(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB9_48
# BB#47:
	movss	%xmm1, 36(%rdi)
.LBB9_48:                               # %_ZN9btVector36setMaxERKS_.exit
	retq
.Lfunc_end9:
	.size	_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallback28internalProcessTriangleIndexEPS0_ii, .Lfunc_end9-_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallback28internalProcessTriangleIndexEPS0_ii
	.cfi_endproc

	.type	_ZTV23btStridingMeshInterface,@object # @_ZTV23btStridingMeshInterface
	.section	.rodata,"a",@progbits
	.globl	_ZTV23btStridingMeshInterface
	.p2align	3
_ZTV23btStridingMeshInterface:
	.quad	0
	.quad	_ZTI23btStridingMeshInterface
	.quad	_ZN23btStridingMeshInterfaceD2Ev
	.quad	_ZN23btStridingMeshInterfaceD0Ev
	.quad	_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK23btStridingMeshInterface14hasPremadeAabbEv
	.quad	_ZNK23btStridingMeshInterface14setPremadeAabbERK9btVector3S2_
	.quad	_ZNK23btStridingMeshInterface14getPremadeAabbEP9btVector3S1_
	.size	_ZTV23btStridingMeshInterface, 120

	.type	_ZTS23btStridingMeshInterface,@object # @_ZTS23btStridingMeshInterface
	.globl	_ZTS23btStridingMeshInterface
	.p2align	4
_ZTS23btStridingMeshInterface:
	.asciz	"23btStridingMeshInterface"
	.size	_ZTS23btStridingMeshInterface, 26

	.type	_ZTI23btStridingMeshInterface,@object # @_ZTI23btStridingMeshInterface
	.globl	_ZTI23btStridingMeshInterface
	.p2align	3
_ZTI23btStridingMeshInterface:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS23btStridingMeshInterface
	.size	_ZTI23btStridingMeshInterface, 16

	.type	_ZTVZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback,@object # @_ZTVZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback
	.p2align	3
_ZTVZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback:
	.quad	0
	.quad	_ZTIZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback
	.quad	_ZN31btInternalTriangleIndexCallbackD2Ev
	.quad	_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallbackD0Ev
	.quad	_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallback28internalProcessTriangleIndexEPS0_ii
	.size	_ZTVZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback, 40

	.type	_ZTSZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback,@object # @_ZTSZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback
	.p2align	4
_ZTSZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback:
	.asciz	"ZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback"
	.size	_ZTSZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback, 94

	.type	_ZTIZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback,@object # @_ZTIZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback
	.p2align	4
_ZTIZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback
	.quad	_ZTI31btInternalTriangleIndexCallback
	.size	_ZTIZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback, 24


	.globl	_ZN23btStridingMeshInterfaceD1Ev
	.type	_ZN23btStridingMeshInterfaceD1Ev,@function
_ZN23btStridingMeshInterfaceD1Ev = _ZN23btStridingMeshInterfaceD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
