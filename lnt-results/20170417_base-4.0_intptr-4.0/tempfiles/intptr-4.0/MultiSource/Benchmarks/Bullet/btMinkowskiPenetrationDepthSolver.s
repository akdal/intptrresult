	.text
	.file	"btMinkowskiPenetrationDepthSolver.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_1:
	.long	1566444395              # float 9.99999984E+17
.LCPI0_3:
	.long	1056964608              # float 0.5
.LCPI0_4:
	.long	0                       # float 0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_2:
	.quad	4576918229304087675     # double 0.01
	.text
	.globl	_ZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAlloc
	.p2align	4, 0x90
	.type	_ZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAlloc,@function
_ZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAlloc: # @_ZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAlloc
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$4472, %rsp             # imm = 0x1178
.Lcfi6:
	.cfi_def_cfa_offset 4528
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %r12
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movq	%rsi, 200(%rsp)         # 8-byte Spill
	movl	8(%r14), %eax
	addl	$-17, %eax
	cmpl	$1, %eax
	ja	.LBB0_1
# BB#2:
	movl	8(%rbx), %eax
	addl	$-17, %eax
	cmpl	$2, %eax
	setb	%al
	movl	%eax, 8(%rsp)           # 4-byte Spill
	jmp	.LBB0_3
.LBB0_1:
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
.LBB0_3:                                # %.preheader549
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movsd	16(%r12), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movsd	32(%r12), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 176(%rsp)        # 16-byte Spill
	movss	8(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movss	24(%r12), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movss	40(%r12), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movsd	(%r15), %xmm13          # xmm13 = mem[0],zero
	movsd	16(%r15), %xmm15        # xmm15 = mem[0],zero
	movsd	32(%r15), %xmm11        # xmm11 = mem[0],zero
	movss	8(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	24(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	40(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movl	$8, %eax
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB0_4:                                # =>This Inner Loop Header: Depth=1
	movd	_ZL22sPenetrationDirections-8(%rax), %xmm7 # xmm7 = mem[0],zero,zero,zero
	movd	_ZL22sPenetrationDirections-4(%rax), %xmm8 # xmm8 = mem[0],zero,zero,zero
	movd	_ZL22sPenetrationDirections(%rax), %xmm10 # xmm10 = mem[0],zero,zero,zero
	movdqa	%xmm7, %xmm5
	pxor	%xmm0, %xmm5
	movdqa	%xmm8, %xmm6
	pxor	%xmm0, %xmm6
	pshufd	$224, %xmm5, %xmm9      # xmm9 = xmm5[0,0,2,3]
	mulps	16(%rsp), %xmm9         # 16-byte Folded Reload
	pshufd	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	32(%rsp), %xmm6         # 16-byte Folded Reload
	addps	%xmm9, %xmm6
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movdqa	%xmm10, %xmm4
	pxor	%xmm0, %xmm4
	pshufd	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	176(%rsp), %xmm4        # 16-byte Folded Reload
	addps	%xmm6, %xmm4
	mulss	12(%rsp), %xmm5         # 4-byte Folded Reload
	movaps	%xmm12, %xmm6
	mulss	%xmm8, %xmm6
	subss	%xmm6, %xmm5
	movaps	%xmm14, %xmm6
	mulss	%xmm10, %xmm6
	subss	%xmm6, %xmm5
	xorps	%xmm6, %xmm6
	movss	%xmm5, %xmm6            # xmm6 = xmm5[0],xmm6[1,2,3]
	movlps	%xmm4, 3464(%rsp,%rax)
	movlps	%xmm6, 3472(%rsp,%rax)
	movdqa	%xmm7, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm13, %xmm4
	movaps	%xmm8, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm15, %xmm5
	addps	%xmm4, %xmm5
	movaps	%xmm10, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm11, %xmm4
	addps	%xmm5, %xmm4
	mulss	%xmm1, %xmm7
	mulss	%xmm2, %xmm8
	addss	%xmm7, %xmm8
	mulss	%xmm3, %xmm10
	addss	%xmm8, %xmm10
	xorps	%xmm5, %xmm5
	movss	%xmm10, %xmm5           # xmm5 = xmm10[0],xmm5[1,2,3]
	movlps	%xmm4, 2472(%rsp,%rax)
	movlps	%xmm5, 2480(%rsp,%rax)
	addq	$16, %rax
	cmpq	$680, %rax              # imm = 0x2A8
	jne	.LBB0_4
# BB#5:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*128(%rax)
	testl	%eax, %eax
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	movq	%r14, 96(%rsp)          # 8-byte Spill
	jle	.LBB0_6
# BB#7:                                 # %.lr.ph572
	leaq	4152(%rsp), %rbx
	leaq	3160(%rsp), %r13
	movl	%eax, 32(%rsp)          # 4-byte Spill
	xorl	%ebp, %ebp
	movl	$_ZL22sPenetrationDirections+672, %r14d
	.p2align	4, 0x90
.LBB0_8:                                # =>This Inner Loop Header: Depth=1
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	%ebp, %esi
	leaq	208(%rsp), %rdx
	callq	*136(%rax)
	movss	208(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	212(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	216(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	16(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%r12), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	movaps	%xmm3, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	movss	20(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1]
	movaps	%xmm2, %xmm7
	mulps	%xmm4, %xmm6
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	movss	24(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulps	%xmm5, %xmm7
	unpcklps	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm6, %xmm7
	addps	%xmm7, %xmm1
	mulss	32(%r12), %xmm3
	mulss	36(%r12), %xmm2
	addss	%xmm3, %xmm2
	mulss	40(%r12), %xmm0
	addss	%xmm2, %xmm0
	movaps	%xmm1, %xmm2
	movaps	%xmm0, %xmm3
	movlps	%xmm1, 208(%rsp)
	xorps	%xmm4, %xmm4
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	movlps	%xmm3, 216(%rsp)
	movups	208(%rsp), %xmm3
	movups	%xmm3, (%r14)
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movaps	%xmm1, %xmm3
	movaps	.LCPI0_0(%rip), %xmm4   # xmm4 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm4, %xmm6
	xorps	%xmm6, %xmm3
	movaps	%xmm2, %xmm4
	xorps	%xmm6, %xmm4
	movaps	%xmm0, %xmm5
	xorps	%xmm6, %xmm5
	movsd	(%r12), %xmm8           # xmm8 = mem[0],zero
	pshufd	$224, %xmm3, %xmm7      # xmm7 = xmm3[0,0,2,3]
	movsd	16(%r12), %xmm6         # xmm6 = mem[0],zero
	pshufd	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm8, %xmm7
	mulps	%xmm6, %xmm4
	movsd	32(%r12), %xmm6         # xmm6 = mem[0],zero
	pshufd	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm6, %xmm5
	movss	8(%r12), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	movss	24(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addps	%xmm7, %xmm4
	mulss	%xmm2, %xmm3
	subss	%xmm3, %xmm6
	movss	40(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addps	%xmm4, %xmm5
	mulss	%xmm0, %xmm3
	subss	%xmm3, %xmm6
	xorps	%xmm3, %xmm3
	movss	%xmm6, %xmm3            # xmm3 = xmm6[0],xmm3[1,2,3]
	movlps	%xmm5, -8(%rbx)
	movlps	%xmm3, (%rbx)
	movsd	(%r15), %xmm3           # xmm3 = mem[0],zero
	movss	8(%r15), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movsd	16(%r15), %xmm5         # xmm5 = mem[0],zero
	mulps	%xmm3, %xmm1
	mulps	%xmm2, %xmm5
	movsd	32(%r15), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm0, %xmm6
	unpcklps	%xmm6, %xmm6    # xmm6 = xmm6[0,0,1,1]
	addps	%xmm1, %xmm5
	mulps	%xmm3, %xmm6
	movss	24(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	addps	%xmm5, %xmm6
	addss	%xmm4, %xmm1
	mulss	40(%r15), %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm6, -8(%r13)
	incl	%ebp
	movlps	%xmm1, (%r13)
	addq	$16, %r14
	addq	$16, %rbx
	addq	$16, %r13
	cmpl	%ebp, 32(%rsp)          # 4-byte Folded Reload
	jne	.LBB0_8
# BB#9:                                 # %.loopexit548.loopexit
	movl	32(%rsp), %ebp          # 4-byte Reload
	addl	$42, %ebp
	movq	104(%rsp), %rbx         # 8-byte Reload
	movq	96(%rsp), %r14          # 8-byte Reload
	jmp	.LBB0_10
.LBB0_6:
	movl	$42, %ebp
.LBB0_10:                               # %.loopexit548
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*128(%rax)
	testl	%eax, %eax
	jle	.LBB0_14
# BB#11:                                # %.lr.ph568
	movl	%ebp, 32(%rsp)          # 4-byte Spill
	movslq	%ebp, %rcx
	shlq	$4, %rcx
	leaq	_ZL22sPenetrationDirections(%rcx), %r14
	leaq	3480(%rsp,%rcx), %r13
	leaq	2488(%rsp,%rcx), %rcx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_12:                               # =>This Inner Loop Header: Depth=1
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rcx
	movl	%ebp, %esi
	leaq	208(%rsp), %rdx
	movq	%r12, %rbx
	movq	%r15, %r12
	movl	%eax, %r15d
	callq	*136(%rcx)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%r15d, %eax
	movq	%r12, %r15
	movq	%rbx, %r12
	movss	208(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	212(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	216(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	16(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%r15), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%r15), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	movaps	%xmm3, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	movss	20(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1]
	movaps	%xmm2, %xmm7
	mulps	%xmm4, %xmm6
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	movss	24(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	8(%r15), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulps	%xmm5, %xmm7
	unpcklps	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm6, %xmm7
	addps	%xmm7, %xmm1
	mulss	32(%r15), %xmm3
	mulss	36(%r15), %xmm2
	addss	%xmm3, %xmm2
	mulss	40(%r15), %xmm0
	addss	%xmm2, %xmm0
	movaps	%xmm1, %xmm2
	movaps	%xmm0, %xmm3
	movlps	%xmm1, 208(%rsp)
	xorps	%xmm4, %xmm4
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	movlps	%xmm3, 216(%rsp)
	movups	208(%rsp), %xmm3
	movups	%xmm3, (%r14)
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movaps	%xmm1, %xmm3
	movaps	.LCPI0_0(%rip), %xmm4   # xmm4 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm4, %xmm6
	xorps	%xmm6, %xmm3
	movaps	%xmm2, %xmm4
	xorps	%xmm6, %xmm4
	movaps	%xmm0, %xmm5
	xorps	%xmm6, %xmm5
	movsd	(%r12), %xmm8           # xmm8 = mem[0],zero
	pshufd	$224, %xmm3, %xmm7      # xmm7 = xmm3[0,0,2,3]
	movsd	16(%r12), %xmm6         # xmm6 = mem[0],zero
	pshufd	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm8, %xmm7
	mulps	%xmm6, %xmm4
	movsd	32(%r12), %xmm6         # xmm6 = mem[0],zero
	pshufd	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm6, %xmm5
	movss	8(%r12), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	movss	24(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addps	%xmm7, %xmm4
	mulss	%xmm2, %xmm3
	subss	%xmm3, %xmm6
	movss	40(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addps	%xmm4, %xmm5
	mulss	%xmm0, %xmm3
	subss	%xmm3, %xmm6
	xorps	%xmm3, %xmm3
	movss	%xmm6, %xmm3            # xmm3 = xmm6[0],xmm3[1,2,3]
	movlps	%xmm5, -8(%r13)
	movlps	%xmm3, (%r13)
	movsd	(%r15), %xmm3           # xmm3 = mem[0],zero
	movss	8(%r15), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movsd	16(%r15), %xmm5         # xmm5 = mem[0],zero
	mulps	%xmm3, %xmm1
	mulps	%xmm2, %xmm5
	movsd	32(%r15), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm0, %xmm6
	unpcklps	%xmm6, %xmm6    # xmm6 = xmm6[0,0,1,1]
	addps	%xmm1, %xmm5
	mulps	%xmm3, %xmm6
	movss	24(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	addps	%xmm5, %xmm6
	addss	%xmm4, %xmm1
	mulss	40(%r15), %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm6, -8(%rcx)
	incl	%ebp
	movlps	%xmm1, (%rcx)
	addq	$16, %r14
	addq	$16, %r13
	addq	$16, %rcx
	cmpl	%ebp, %eax
	jne	.LBB0_12
# BB#13:                                # %.loopexit.loopexit
	movl	32(%rsp), %ebp          # 4-byte Reload
	addl	%eax, %ebp
	movq	104(%rsp), %rbx         # 8-byte Reload
	movq	96(%rsp), %r14          # 8-byte Reload
.LBB0_14:                               # %.loopexit
	movq	(%r14), %rax
	leaq	3472(%rsp), %rsi
	leaq	1488(%rsp), %rdx
	movq	%r14, %rdi
	movl	%ebp, %ecx
	callq	*112(%rax)
	movq	(%rbx), %rax
	leaq	2480(%rsp), %rsi
	leaq	496(%rsp), %rdx
	movq	%rbx, %rdi
	movl	%ebp, %ecx
	callq	*112(%rax)
	testl	%ebp, %ebp
	jle	.LBB0_15
# BB#16:                                # %.lr.ph
	movss	(%r12), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movss	4(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 176(%rsp)        # 4-byte Spill
	movss	8(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movss	48(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 92(%rsp)         # 4-byte Spill
	movss	16(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 88(%rsp)         # 4-byte Spill
	movss	20(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 84(%rsp)         # 4-byte Spill
	movss	24(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 80(%rsp)         # 4-byte Spill
	movss	52(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 76(%rsp)         # 4-byte Spill
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 72(%rsp)         # 4-byte Spill
	movss	4(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 68(%rsp)         # 4-byte Spill
	movss	8(%r15), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 64(%rsp)         # 4-byte Spill
	movss	48(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 60(%rsp)         # 4-byte Spill
	movss	16(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 56(%rsp)         # 4-byte Spill
	movss	20(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 52(%rsp)         # 4-byte Spill
	movss	24(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
	movss	52(%r15), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movl	%ebp, %eax
	cmpb	$0, 8(%rsp)             # 1-byte Folded Reload
	je	.LBB0_17
# BB#22:                                # %.lr.ph.split.us.preheader
	xorps	%xmm15, %xmm15
	xorps	%xmm8, %xmm8
	movss	.LCPI0_1(%rip), %xmm13  # xmm13 = mem[0],zero,zero,zero
	movl	$8, %ecx
	movsd	.LCPI0_2(%rip), %xmm10  # xmm10 = mem[0],zero
	xorps	%xmm14, %xmm14
	.p2align	4, 0x90
.LBB0_23:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movaps	_ZL22sPenetrationDirections-8(%rcx), %xmm11
	movaps	%xmm11, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movaps	%xmm11, %xmm3
	mulss	%xmm3, %xmm3
	movaps	%xmm2, %xmm4
	mulss	%xmm4, %xmm4
	addss	%xmm3, %xmm4
	addss	%xmm8, %xmm4
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm4, %xmm3
	ucomisd	%xmm10, %xmm3
	jbe	.LBB0_26
# BB#24:                                #   in Loop: Header=BB0_23 Depth=1
	movss	1480(%rsp,%rcx), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movss	1484(%rsp,%rcx), %xmm6  # xmm6 = mem[0],zero,zero,zero
	movss	1488(%rsp,%rcx), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movss	488(%rsp,%rcx), %xmm12  # xmm12 = mem[0],zero,zero,zero
	movss	492(%rsp,%rcx), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	mulss	32(%rsp), %xmm3         # 4-byte Folded Reload
	movaps	%xmm6, %xmm1
	mulss	176(%rsp), %xmm1        # 4-byte Folded Reload
	addss	%xmm3, %xmm1
	movaps	%xmm4, %xmm7
	mulss	12(%rsp), %xmm7         # 4-byte Folded Reload
	addss	%xmm1, %xmm7
	movss	496(%rsp,%rcx), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	88(%rsp), %xmm0         # 4-byte Folded Reload
	mulss	84(%rsp), %xmm6         # 4-byte Folded Reload
	addss	%xmm0, %xmm6
	mulss	80(%rsp), %xmm4         # 4-byte Folded Reload
	addss	%xmm6, %xmm4
	movaps	%xmm12, %xmm0
	mulss	72(%rsp), %xmm0         # 4-byte Folded Reload
	movaps	%xmm5, %xmm1
	mulss	68(%rsp), %xmm1         # 4-byte Folded Reload
	addss	%xmm0, %xmm1
	movaps	%xmm3, %xmm0
	mulss	64(%rsp), %xmm0         # 4-byte Folded Reload
	addss	%xmm1, %xmm0
	mulss	56(%rsp), %xmm12        # 4-byte Folded Reload
	mulss	52(%rsp), %xmm5         # 4-byte Folded Reload
	addss	%xmm12, %xmm5
	mulss	48(%rsp), %xmm3         # 4-byte Folded Reload
	addss	%xmm5, %xmm3
	addss	92(%rsp), %xmm7         # 4-byte Folded Reload
	addss	60(%rsp), %xmm0         # 4-byte Folded Reload
	subss	%xmm7, %xmm0
	addss	76(%rsp), %xmm4         # 4-byte Folded Reload
	addss	%xmm9, %xmm3
	subss	%xmm4, %xmm3
	mulss	%xmm2, %xmm3
	mulss	%xmm11, %xmm0
	addss	%xmm0, %xmm3
	addss	%xmm8, %xmm3
	ucomiss	%xmm3, %xmm13
	jbe	.LBB0_26
# BB#25:                                #   in Loop: Header=BB0_23 Depth=1
	movss	_ZL22sPenetrationDirections+4(%rcx), %xmm14 # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm13
	movaps	%xmm11, %xmm15
.LBB0_26:                               #   in Loop: Header=BB0_23 Depth=1
	addq	$16, %rcx
	decq	%rax
	jne	.LBB0_23
# BB#27:
	movaps	%xmm13, 16(%rsp)        # 16-byte Spill
	movss	%xmm14, 4(%rsp)         # 4-byte Spill
	movaps	%xmm15, 160(%rsp)       # 16-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 144(%rsp)        # 16-byte Spill
	jmp	.LBB0_28
.LBB0_15:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 160(%rsp)        # 16-byte Spill
	movss	.LCPI0_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 144(%rsp)        # 16-byte Spill
	xorps	%xmm0, %xmm0
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	jmp	.LBB0_28
.LBB0_17:                               # %.lr.ph.split.preheader
	movss	32(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movss	36(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 140(%rsp)        # 4-byte Spill
	movss	40(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 136(%rsp)        # 4-byte Spill
	movss	56(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 132(%rsp)        # 4-byte Spill
	movss	32(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 128(%rsp)        # 4-byte Spill
	movss	36(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 124(%rsp)        # 4-byte Spill
	movss	40(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 120(%rsp)        # 4-byte Spill
	movss	56(%r15), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 116(%rsp)        # 4-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 160(%rsp)        # 16-byte Spill
	xorps	%xmm0, %xmm0
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movss	.LCPI0_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movl	$8, %ecx
	movsd	.LCPI0_2(%rip), %xmm15  # xmm15 = mem[0],zero
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 144(%rsp)        # 16-byte Spill
	.p2align	4, 0x90
.LBB0_18:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movaps	_ZL22sPenetrationDirections-8(%rcx), %xmm14
	movaps	%xmm14, %xmm12
	shufps	$229, %xmm12, %xmm12    # xmm12 = xmm12[1,1,2,3]
	movss	_ZL22sPenetrationDirections(%rcx), %xmm8 # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm1
	mulss	%xmm1, %xmm1
	movaps	%xmm12, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm8, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	cvtss2sd	%xmm1, %xmm1
	ucomisd	%xmm15, %xmm1
	jbe	.LBB0_21
# BB#19:                                #   in Loop: Header=BB0_18 Depth=1
	movss	1480(%rsp,%rcx), %xmm11 # xmm11 = mem[0],zero,zero,zero
	movss	1484(%rsp,%rcx), %xmm13 # xmm13 = mem[0],zero,zero,zero
	movss	1488(%rsp,%rcx), %xmm7  # xmm7 = mem[0],zero,zero,zero
	movss	488(%rsp,%rcx), %xmm6   # xmm6 = mem[0],zero,zero,zero
	movss	492(%rsp,%rcx), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movss	496(%rsp,%rcx), %xmm10  # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm1
	mulss	32(%rsp), %xmm1         # 4-byte Folded Reload
	movaps	%xmm13, %xmm2
	mulss	176(%rsp), %xmm2        # 4-byte Folded Reload
	addss	%xmm1, %xmm2
	movaps	%xmm7, %xmm1
	mulss	12(%rsp), %xmm1         # 4-byte Folded Reload
	addss	%xmm2, %xmm1
	movaps	%xmm11, %xmm2
	mulss	88(%rsp), %xmm2         # 4-byte Folded Reload
	movaps	%xmm13, %xmm3
	mulss	84(%rsp), %xmm3         # 4-byte Folded Reload
	addss	%xmm2, %xmm3
	movaps	%xmm7, %xmm4
	mulss	80(%rsp), %xmm4         # 4-byte Folded Reload
	addss	%xmm3, %xmm4
	mulss	8(%rsp), %xmm11         # 4-byte Folded Reload
	mulss	140(%rsp), %xmm13       # 4-byte Folded Reload
	addss	%xmm11, %xmm13
	mulss	136(%rsp), %xmm7        # 4-byte Folded Reload
	addss	%xmm13, %xmm7
	movaps	%xmm6, %xmm2
	mulss	72(%rsp), %xmm2         # 4-byte Folded Reload
	movaps	%xmm5, %xmm0
	mulss	68(%rsp), %xmm0         # 4-byte Folded Reload
	addss	%xmm2, %xmm0
	movaps	%xmm10, %xmm3
	mulss	64(%rsp), %xmm3         # 4-byte Folded Reload
	addss	%xmm0, %xmm3
	movaps	%xmm6, %xmm2
	mulss	56(%rsp), %xmm2         # 4-byte Folded Reload
	movaps	%xmm5, %xmm0
	mulss	52(%rsp), %xmm0         # 4-byte Folded Reload
	addss	%xmm2, %xmm0
	movaps	%xmm10, %xmm2
	mulss	48(%rsp), %xmm2         # 4-byte Folded Reload
	addss	%xmm0, %xmm2
	mulss	128(%rsp), %xmm6        # 4-byte Folded Reload
	mulss	124(%rsp), %xmm5        # 4-byte Folded Reload
	addss	%xmm6, %xmm5
	mulss	120(%rsp), %xmm10       # 4-byte Folded Reload
	addss	%xmm5, %xmm10
	addss	92(%rsp), %xmm1         # 4-byte Folded Reload
	addss	60(%rsp), %xmm3         # 4-byte Folded Reload
	subss	%xmm1, %xmm3
	addss	76(%rsp), %xmm4         # 4-byte Folded Reload
	addss	%xmm9, %xmm2
	subss	%xmm4, %xmm2
	addss	132(%rsp), %xmm7        # 4-byte Folded Reload
	addss	116(%rsp), %xmm10       # 4-byte Folded Reload
	subss	%xmm7, %xmm10
	mulss	%xmm12, %xmm2
	mulss	%xmm14, %xmm3
	addss	%xmm3, %xmm2
	mulss	%xmm8, %xmm10
	addss	%xmm2, %xmm10
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	ucomiss	%xmm10, %xmm0
	jbe	.LBB0_21
# BB#20:                                #   in Loop: Header=BB0_18 Depth=1
	movss	_ZL22sPenetrationDirections+4(%rcx), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movaps	%xmm8, 144(%rsp)        # 16-byte Spill
	movaps	%xmm10, 16(%rsp)        # 16-byte Spill
	movaps	%xmm14, 160(%rsp)       # 16-byte Spill
.LBB0_21:                               #   in Loop: Header=BB0_18 Depth=1
	addq	$16, %rcx
	decq	%rax
	jne	.LBB0_18
.LBB0_28:                               # %._crit_edge
	movq	%r14, %rdi
	callq	_ZNK13btConvexShape19getMarginNonVirtualEv
	movq	%rbx, %rdi
	callq	_ZNK13btConvexShape19getMarginNonVirtualEv
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jbe	.LBB0_30
# BB#29:
	xorl	%ecx, %ecx
	jmp	.LBB0_34
.LBB0_30:
	movq	%r14, %rdi
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	callq	_ZNK13btConvexShape19getMarginNonVirtualEv
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movq	%rbx, %rdi
	callq	_ZNK13btConvexShape19getMarginNonVirtualEv
	addss	32(%rsp), %xmm0         # 4-byte Folded Reload
	addss	.LCPI0_3(%rip), %xmm0
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	addss	%xmm0, %xmm1
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	leaq	400(%rsp), %rbp
	xorl	%r8d, %r8d
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	movq	200(%rsp), %rcx         # 8-byte Reload
	callq	_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm2, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movaps	160(%rsp), %xmm4        # 16-byte Reload
	mulps	%xmm4, %xmm0
	movaps	144(%rsp), %xmm5        # 16-byte Reload
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	movq	$0, 344(%rsp)
	movsd	48(%r12), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm0, %xmm2
	addss	56(%r12), %xmm1
	xorps	%xmm0, %xmm0
	xorps	%xmm3, %xmm3
	movss	%xmm1, %xmm3            # xmm3 = xmm1[0],xmm3[1,2,3]
	movups	(%r12), %xmm1
	movaps	%xmm1, 208(%rsp)
	movups	16(%r12), %xmm1
	movaps	%xmm1, 224(%rsp)
	movups	32(%r12), %xmm1
	movaps	%xmm1, 240(%rsp)
	movlps	%xmm2, 256(%rsp)
	movlps	%xmm3, 264(%rsp)
	movups	(%r15), %xmm1
	movaps	%xmm1, 272(%rsp)
	movups	16(%r15), %xmm1
	movaps	%xmm1, 288(%rsp)
	movups	32(%r15), %xmm1
	movaps	%xmm1, 304(%rsp)
	movups	48(%r15), %xmm1
	movaps	%xmm1, 320(%rsp)
	movl	$1566444395, 336(%rsp)  # imm = 0x5D5E0B6B
	movq	$_ZTVZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocE20btIntermediateResult+16, 352(%rsp)
	movb	$0, 396(%rsp)
	movaps	%xmm4, %xmm1
	movaps	.LCPI0_0(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm1
	xorps	%xmm5, %xmm2
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm1, 408(%rsp)
	movlps	%xmm0, 416(%rsp)
.Ltmp0:
	leaq	208(%rsp), %rsi
	leaq	352(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbp, %rdi
	movq	4552(%rsp), %rcx
	callq	_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
.Ltmp1:
# BB#31:
	movb	396(%rsp), %al
	testb	%al, %al
	movss	4(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	je	.LBB0_33
# BB#32:
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	subss	392(%rsp), %xmm2
	movaps	144(%rsp), %xmm5        # 16-byte Reload
	movaps	%xmm5, %xmm0
	mulss	%xmm2, %xmm0
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movaps	160(%rsp), %xmm3        # 16-byte Reload
	mulps	%xmm3, %xmm2
	movsd	376(%rsp), %xmm1        # xmm1 = mem[0],zero
	subps	%xmm2, %xmm1
	movss	384(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movq	4536(%rsp), %rcx
	movlps	%xmm1, (%rcx)
	movlps	%xmm0, 8(%rcx)
	movups	376(%rsp), %xmm0
	movq	4544(%rsp), %rcx
	movups	%xmm0, (%rcx)
	movq	4528(%rsp), %rcx
	movss	%xmm3, (%rcx)
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movss	%xmm3, 4(%rcx)
	movss	%xmm5, 8(%rcx)
	movss	%xmm4, 12(%rcx)
.LBB0_33:
	testb	%al, %al
	setne	%cl
.LBB0_34:
	movl	%ecx, %eax
	addq	$4472, %rsp             # imm = 0x1178
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_35:
.Ltmp2:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAlloc, .Lfunc_end0-_ZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAlloc
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev,"axG",@progbits,_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev,comdat
	.weak	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.p2align	4, 0x90
	.type	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev,@function
_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev: # @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev, .Lfunc_end1-_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.cfi_endproc

	.section	.text._ZN30btConvexPenetrationDepthSolverD2Ev,"axG",@progbits,_ZN30btConvexPenetrationDepthSolverD2Ev,comdat
	.weak	_ZN30btConvexPenetrationDepthSolverD2Ev
	.p2align	4, 0x90
	.type	_ZN30btConvexPenetrationDepthSolverD2Ev,@function
_ZN30btConvexPenetrationDepthSolverD2Ev: # @_ZN30btConvexPenetrationDepthSolverD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end2:
	.size	_ZN30btConvexPenetrationDepthSolverD2Ev, .Lfunc_end2-_ZN30btConvexPenetrationDepthSolverD2Ev
	.cfi_endproc

	.section	.text._ZN33btMinkowskiPenetrationDepthSolverD0Ev,"axG",@progbits,_ZN33btMinkowskiPenetrationDepthSolverD0Ev,comdat
	.weak	_ZN33btMinkowskiPenetrationDepthSolverD0Ev
	.p2align	4, 0x90
	.type	_ZN33btMinkowskiPenetrationDepthSolverD0Ev,@function
_ZN33btMinkowskiPenetrationDepthSolverD0Ev: # @_ZN33btMinkowskiPenetrationDepthSolverD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end3:
	.size	_ZN33btMinkowskiPenetrationDepthSolverD0Ev, .Lfunc_end3-_ZN33btMinkowskiPenetrationDepthSolverD0Ev
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResultD0Ev,@function
_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResultD0Ev: # @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResultD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end4:
	.size	_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResultD0Ev, .Lfunc_end4-_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResultD0Ev
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResult20setShapeIdentifiersAEii,@function
_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResult20setShapeIdentifiersAEii: # @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResult20setShapeIdentifiersAEii
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end5:
	.size	_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResult20setShapeIdentifiersAEii, .Lfunc_end5-_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResult20setShapeIdentifiersAEii
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResult20setShapeIdentifiersBEii,@function
_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResult20setShapeIdentifiersBEii: # @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResult20setShapeIdentifiersBEii
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end6:
	.size	_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResult20setShapeIdentifiersBEii, .Lfunc_end6-_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResult20setShapeIdentifiersBEii
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResult15addContactPointERKS8_SG_f,@function
_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResult15addContactPointERKS8_SG_f: # @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResult15addContactPointERKS8_SG_f
	.cfi_startproc
# BB#0:
	movups	(%rsi), %xmm1
	movups	%xmm1, 8(%rdi)
	movups	(%rdx), %xmm1
	movups	%xmm1, 24(%rdi)
	movss	%xmm0, 40(%rdi)
	movb	$1, 44(%rdi)
	retq
.Lfunc_end7:
	.size	_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResult15addContactPointERKS8_SG_f, .Lfunc_end7-_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResult15addContactPointERKS8_SG_f
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_btMinkowskiPenetrationDepthSolver.ii,@function
_GLOBAL__sub_I_btMinkowskiPenetrationDepthSolver.ii: # @_GLOBAL__sub_I_btMinkowskiPenetrationDepthSolver.ii
	.cfi_startproc
# BB#0:
	movl	$0, _ZL22sPenetrationDirections(%rip)
	movl	$-2147483648, _ZL22sPenetrationDirections+4(%rip) # imm = 0x80000000
	movl	$-1082130432, _ZL22sPenetrationDirections+8(%rip) # imm = 0xBF800000
	movl	$0, _ZL22sPenetrationDirections+12(%rip)
	movl	$1060716128, _ZL22sPenetrationDirections+16(%rip) # imm = 0x3F393E60
	movl	$-1090087446, _ZL22sPenetrationDirections+20(%rip) # imm = 0xBF0695EA
	movl	$-1092290076, _ZL22sPenetrationDirections+24(%rip) # imm = 0xBEE4F9E4
	movl	$0, _ZL22sPenetrationDirections+28(%rip)
	movl	$-1098022214, _ZL22sPenetrationDirections+32(%rip) # imm = 0xBE8D82BA
	movl	$-1084636126, _ZL22sPenetrationDirections+36(%rip) # imm = 0xBF59C422
	movl	$-1092290076, _ZL22sPenetrationDirections+40(%rip) # imm = 0xBEE4F9E4
	movl	$0, _ZL22sPenetrationDirections+44(%rip)
	movl	$-1083901670, _ZL22sPenetrationDirections+48(%rip) # imm = 0xBF64F91A
	movl	$-2147483648, _ZL22sPenetrationDirections+52(%rip) # imm = 0x80000000
	movl	$-1092290177, _ZL22sPenetrationDirections+56(%rip) # imm = 0xBEE4F97F
	movl	$0, _ZL22sPenetrationDirections+60(%rip)
	movl	$-1098022214, _ZL22sPenetrationDirections+64(%rip) # imm = 0xBE8D82BA
	movl	$1062847522, _ZL22sPenetrationDirections+68(%rip) # imm = 0x3F59C422
	movl	$-1092290043, _ZL22sPenetrationDirections+72(%rip) # imm = 0xBEE4FA05
	movl	$0, _ZL22sPenetrationDirections+76(%rip)
	movl	$1060716128, _ZL22sPenetrationDirections+80(%rip) # imm = 0x3F393E60
	movl	$1057396202, _ZL22sPenetrationDirections+84(%rip) # imm = 0x3F0695EA
	movl	$-1092290076, _ZL22sPenetrationDirections+88(%rip) # imm = 0xBEE4F9E4
	movl	$0, _ZL22sPenetrationDirections+92(%rip)
	movl	$1049461434, _ZL22sPenetrationDirections+96(%rip) # imm = 0x3E8D82BA
	movl	$-1084636126, _ZL22sPenetrationDirections+100(%rip) # imm = 0xBF59C422
	movl	$1055193605, _ZL22sPenetrationDirections+104(%rip) # imm = 0x3EE4FA05
	movl	$0, _ZL22sPenetrationDirections+108(%rip)
	movl	$-1086767520, _ZL22sPenetrationDirections+112(%rip) # imm = 0xBF393E60
	movl	$-1090087446, _ZL22sPenetrationDirections+116(%rip) # imm = 0xBF0695EA
	movl	$1055193572, _ZL22sPenetrationDirections+120(%rip) # imm = 0x3EE4F9E4
	movl	$0, _ZL22sPenetrationDirections+124(%rip)
	movl	$-1086767520, _ZL22sPenetrationDirections+128(%rip) # imm = 0xBF393E60
	movl	$1057396202, _ZL22sPenetrationDirections+132(%rip) # imm = 0x3F0695EA
	movl	$1055193572, _ZL22sPenetrationDirections+136(%rip) # imm = 0x3EE4F9E4
	movl	$0, _ZL22sPenetrationDirections+140(%rip)
	movl	$1049461434, _ZL22sPenetrationDirections+144(%rip) # imm = 0x3E8D82BA
	movl	$1062847522, _ZL22sPenetrationDirections+148(%rip) # imm = 0x3F59C422
	movl	$1055193572, _ZL22sPenetrationDirections+152(%rip) # imm = 0x3EE4F9E4
	movl	$0, _ZL22sPenetrationDirections+156(%rip)
	movl	$1063581978, _ZL22sPenetrationDirections+160(%rip) # imm = 0x3F64F91A
	movl	$0, _ZL22sPenetrationDirections+164(%rip)
	movl	$1055193471, _ZL22sPenetrationDirections+168(%rip) # imm = 0x3EE4F97F
	movl	$0, _ZL22sPenetrationDirections+172(%rip)
	movl	$-2147483648, _ZL22sPenetrationDirections+176(%rip) # imm = 0x80000000
	movl	$0, _ZL22sPenetrationDirections+180(%rip)
	movl	$1065353216, _ZL22sPenetrationDirections+184(%rip) # imm = 0x3F800000
	movl	$0, _ZL22sPenetrationDirections+188(%rip)
	movl	$1054458864, _ZL22sPenetrationDirections+192(%rip) # imm = 0x3ED9C3F0
	movl	$-1096927567, _ZL22sPenetrationDirections+196(%rip) # imm = 0xBE9E36B1
	movl	$-1084636042, _ZL22sPenetrationDirections+200(%rip) # imm = 0xBF59C476
	movl	$0, _ZL22sPenetrationDirections+204(%rip)
	movl	$-1104782626, _ZL22sPenetrationDirections+208(%rip) # imm = 0xBE265ADE
	movl	$-1090519208, _ZL22sPenetrationDirections+212(%rip) # imm = 0xBEFFFF58
	movl	$-1084636042, _ZL22sPenetrationDirections+216(%rip) # imm = 0xBF59C476
	movl	$0, _ZL22sPenetrationDirections+220(%rip)
	movl	$1049007812, _ZL22sPenetrationDirections+224(%rip) # imm = 0x3E8696C4
	movl	$-1085334679, _ZL22sPenetrationDirections+228(%rip) # imm = 0xBF4F1B69
	movl	$-1090087228, _ZL22sPenetrationDirections+232(%rip) # imm = 0xBF0696C4
	movl	$0, _ZL22sPenetrationDirections+236(%rip)
	movl	$1054458864, _ZL22sPenetrationDirections+240(%rip) # imm = 0x3ED9C3F0
	movl	$1050556081, _ZL22sPenetrationDirections+244(%rip) # imm = 0x3E9E36B1
	movl	$-1084636042, _ZL22sPenetrationDirections+248(%rip) # imm = 0xBF59C476
	movl	$0, _ZL22sPenetrationDirections+252(%rip)
	movl	$1062847505, _ZL22sPenetrationDirections+256(%rip) # imm = 0x3F59C411
	movl	$-2147483648, _ZL22sPenetrationDirections+260(%rip) # imm = 0x80000000
	movl	$-1090087262, _ZL22sPenetrationDirections+264(%rip) # imm = 0xBF0696A2
	movl	$0, _ZL22sPenetrationDirections+268(%rip)
	movl	$-1090087362, _ZL22sPenetrationDirections+272(%rip) # imm = 0xBF06963E
	movl	$-2147483648, _ZL22sPenetrationDirections+276(%rip) # imm = 0x80000000
	movl	$-1084636076, _ZL22sPenetrationDirections+280(%rip) # imm = 0xBF59C454
	movl	$0, _ZL22sPenetrationDirections+284(%rip)
	movl	$-1087361736, _ZL22sPenetrationDirections+288(%rip) # imm = 0xBF302D38
	movl	$-1090519141, _ZL22sPenetrationDirections+292(%rip) # imm = 0xBEFFFF9B
	movl	$-1090087262, _ZL22sPenetrationDirections+296(%rip) # imm = 0xBF0696A2
	movl	$0, _ZL22sPenetrationDirections+300(%rip)
	movl	$-1104782626, _ZL22sPenetrationDirections+304(%rip) # imm = 0xBE265ADE
	movl	$1056964440, _ZL22sPenetrationDirections+308(%rip) # imm = 0x3EFFFF58
	movl	$-1084636042, _ZL22sPenetrationDirections+312(%rip) # imm = 0xBF59C476
	movl	$0, _ZL22sPenetrationDirections+316(%rip)
	movl	$-1087361736, _ZL22sPenetrationDirections+320(%rip) # imm = 0xBF302D38
	movl	$1056964507, _ZL22sPenetrationDirections+324(%rip) # imm = 0x3EFFFF9B
	movl	$-1090087262, _ZL22sPenetrationDirections+328(%rip) # imm = 0xBF0696A2
	movl	$0, _ZL22sPenetrationDirections+332(%rip)
	movl	$1049007812, _ZL22sPenetrationDirections+336(%rip) # imm = 0x3E8696C4
	movl	$1062148969, _ZL22sPenetrationDirections+340(%rip) # imm = 0x3F4F1B69
	movl	$-1090087228, _ZL22sPenetrationDirections+344(%rip) # imm = 0xBF0696C4
	movl	$0, _ZL22sPenetrationDirections+348(%rip)
	movl	$1064532105, _ZL22sPenetrationDirections+352(%rip) # imm = 0x3F737889
	movl	$1050556148, _ZL22sPenetrationDirections+356(%rip) # imm = 0x3E9E36F4
	movl	$0, _ZL22sPenetrationDirections+360(%rip)
	movl	$0, _ZL22sPenetrationDirections+364(%rip)
	movl	$1064532105, _ZL22sPenetrationDirections+368(%rip) # imm = 0x3F737889
	movl	$-1096927500, _ZL22sPenetrationDirections+372(%rip) # imm = 0xBE9E36F4
	movl	$0, _ZL22sPenetrationDirections+376(%rip)
	movl	$0, _ZL22sPenetrationDirections+380(%rip)
	movl	$1058437413, _ZL22sPenetrationDirections+384(%rip) # imm = 0x3F167925
	movl	$-1085334595, _ZL22sPenetrationDirections+388(%rip) # imm = 0xBF4F1BBD
	movl	$0, _ZL22sPenetrationDirections+392(%rip)
	movl	$0, _ZL22sPenetrationDirections+396(%rip)
	movl	$0, _ZL22sPenetrationDirections+400(%rip)
	movl	$-1082130432, _ZL22sPenetrationDirections+404(%rip) # imm = 0xBF800000
	movl	$0, _ZL22sPenetrationDirections+408(%rip)
	movl	$0, _ZL22sPenetrationDirections+412(%rip)
	movl	$-1089046235, _ZL22sPenetrationDirections+416(%rip) # imm = 0xBF167925
	movl	$-1085334595, _ZL22sPenetrationDirections+420(%rip) # imm = 0xBF4F1BBD
	movl	$0, _ZL22sPenetrationDirections+424(%rip)
	movl	$0, _ZL22sPenetrationDirections+428(%rip)
	movl	$-1082951543, _ZL22sPenetrationDirections+432(%rip) # imm = 0xBF737889
	movl	$-1096927500, _ZL22sPenetrationDirections+436(%rip) # imm = 0xBE9E36F4
	movl	$-2147483648, _ZL22sPenetrationDirections+440(%rip) # imm = 0x80000000
	movl	$0, _ZL22sPenetrationDirections+444(%rip)
	movl	$-1082951543, _ZL22sPenetrationDirections+448(%rip) # imm = 0xBF737889
	movl	$1050556148, _ZL22sPenetrationDirections+452(%rip) # imm = 0x3E9E36F4
	movl	$-2147483648, _ZL22sPenetrationDirections+456(%rip) # imm = 0x80000000
	movl	$0, _ZL22sPenetrationDirections+460(%rip)
	movl	$-1089046235, _ZL22sPenetrationDirections+464(%rip) # imm = 0xBF167925
	movl	$1062149053, _ZL22sPenetrationDirections+468(%rip) # imm = 0x3F4F1BBD
	movl	$-2147483648, _ZL22sPenetrationDirections+472(%rip) # imm = 0x80000000
	movl	$0, _ZL22sPenetrationDirections+476(%rip)
	movl	$-2147483648, _ZL22sPenetrationDirections+480(%rip) # imm = 0x80000000
	movl	$1065353216, _ZL22sPenetrationDirections+484(%rip) # imm = 0x3F800000
	movl	$-2147483648, _ZL22sPenetrationDirections+488(%rip) # imm = 0x80000000
	movl	$0, _ZL22sPenetrationDirections+492(%rip)
	movl	$1058437413, _ZL22sPenetrationDirections+496(%rip) # imm = 0x3F167925
	movl	$1062149053, _ZL22sPenetrationDirections+500(%rip) # imm = 0x3F4F1BBD
	movl	$-2147483648, _ZL22sPenetrationDirections+504(%rip) # imm = 0x80000000
	movl	$0, _ZL22sPenetrationDirections+508(%rip)
	movl	$1060121912, _ZL22sPenetrationDirections+512(%rip) # imm = 0x3F302D38
	movl	$-1090519141, _ZL22sPenetrationDirections+516(%rip) # imm = 0xBEFFFF9B
	movl	$1057396386, _ZL22sPenetrationDirections+520(%rip) # imm = 0x3F0696A2
	movl	$0, _ZL22sPenetrationDirections+524(%rip)
	movl	$-1098475836, _ZL22sPenetrationDirections+528(%rip) # imm = 0xBE8696C4
	movl	$-1085334679, _ZL22sPenetrationDirections+532(%rip) # imm = 0xBF4F1B69
	movl	$1057396420, _ZL22sPenetrationDirections+536(%rip) # imm = 0x3F0696C4
	movl	$0, _ZL22sPenetrationDirections+540(%rip)
	movl	$-1084636143, _ZL22sPenetrationDirections+544(%rip) # imm = 0xBF59C411
	movl	$0, _ZL22sPenetrationDirections+548(%rip)
	movl	$1057396386, _ZL22sPenetrationDirections+552(%rip) # imm = 0x3F0696A2
	movl	$0, _ZL22sPenetrationDirections+556(%rip)
	movl	$-1098475836, _ZL22sPenetrationDirections+560(%rip) # imm = 0xBE8696C4
	movl	$1062148969, _ZL22sPenetrationDirections+564(%rip) # imm = 0x3F4F1B69
	movl	$1057396420, _ZL22sPenetrationDirections+568(%rip) # imm = 0x3F0696C4
	movl	$0, _ZL22sPenetrationDirections+572(%rip)
	movl	$1060121912, _ZL22sPenetrationDirections+576(%rip) # imm = 0x3F302D38
	movl	$1056964507, _ZL22sPenetrationDirections+580(%rip) # imm = 0x3EFFFF9B
	movl	$1057396386, _ZL22sPenetrationDirections+584(%rip) # imm = 0x3F0696A2
	movl	$0, _ZL22sPenetrationDirections+588(%rip)
	movl	$1057396286, _ZL22sPenetrationDirections+592(%rip) # imm = 0x3F06963E
	movl	$0, _ZL22sPenetrationDirections+596(%rip)
	movl	$1062847572, _ZL22sPenetrationDirections+600(%rip) # imm = 0x3F59C454
	movl	$0, _ZL22sPenetrationDirections+604(%rip)
	movl	$1042701022, _ZL22sPenetrationDirections+608(%rip) # imm = 0x3E265ADE
	movl	$-1090519208, _ZL22sPenetrationDirections+612(%rip) # imm = 0xBEFFFF58
	movl	$1062847606, _ZL22sPenetrationDirections+616(%rip) # imm = 0x3F59C476
	movl	$0, _ZL22sPenetrationDirections+620(%rip)
	movl	$-1093024784, _ZL22sPenetrationDirections+624(%rip) # imm = 0xBED9C3F0
	movl	$-1096927567, _ZL22sPenetrationDirections+628(%rip) # imm = 0xBE9E36B1
	movl	$1062847606, _ZL22sPenetrationDirections+632(%rip) # imm = 0x3F59C476
	movl	$0, _ZL22sPenetrationDirections+636(%rip)
	movl	$-1093024784, _ZL22sPenetrationDirections+640(%rip) # imm = 0xBED9C3F0
	movl	$1050556081, _ZL22sPenetrationDirections+644(%rip) # imm = 0x3E9E36B1
	movl	$1062847606, _ZL22sPenetrationDirections+648(%rip) # imm = 0x3F59C476
	movl	$0, _ZL22sPenetrationDirections+652(%rip)
	movabsq	$4539627703877655262, %rax # imm = 0x3EFFFF583E265ADE
	movq	%rax, _ZL22sPenetrationDirections+656(%rip)
	movq	$1062847606, _ZL22sPenetrationDirections+664(%rip) # imm = 0x3F59C476
	retq
.Lfunc_end8:
	.size	_GLOBAL__sub_I_btMinkowskiPenetrationDepthSolver.ii, .Lfunc_end8-_GLOBAL__sub_I_btMinkowskiPenetrationDepthSolver.ii
	.cfi_endproc

	.type	_ZL22sPenetrationDirections,@object # @_ZL22sPenetrationDirections
	.local	_ZL22sPenetrationDirections
	.comm	_ZL22sPenetrationDirections,992,16
	.type	_ZTV33btMinkowskiPenetrationDepthSolver,@object # @_ZTV33btMinkowskiPenetrationDepthSolver
	.section	.rodata,"a",@progbits
	.globl	_ZTV33btMinkowskiPenetrationDepthSolver
	.p2align	3
_ZTV33btMinkowskiPenetrationDepthSolver:
	.quad	0
	.quad	_ZTI33btMinkowskiPenetrationDepthSolver
	.quad	_ZN30btConvexPenetrationDepthSolverD2Ev
	.quad	_ZN33btMinkowskiPenetrationDepthSolverD0Ev
	.quad	_ZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAlloc
	.size	_ZTV33btMinkowskiPenetrationDepthSolver, 40

	.type	_ZTS33btMinkowskiPenetrationDepthSolver,@object # @_ZTS33btMinkowskiPenetrationDepthSolver
	.globl	_ZTS33btMinkowskiPenetrationDepthSolver
	.p2align	4
_ZTS33btMinkowskiPenetrationDepthSolver:
	.asciz	"33btMinkowskiPenetrationDepthSolver"
	.size	_ZTS33btMinkowskiPenetrationDepthSolver, 36

	.type	_ZTS30btConvexPenetrationDepthSolver,@object # @_ZTS30btConvexPenetrationDepthSolver
	.section	.rodata._ZTS30btConvexPenetrationDepthSolver,"aG",@progbits,_ZTS30btConvexPenetrationDepthSolver,comdat
	.weak	_ZTS30btConvexPenetrationDepthSolver
	.p2align	4
_ZTS30btConvexPenetrationDepthSolver:
	.asciz	"30btConvexPenetrationDepthSolver"
	.size	_ZTS30btConvexPenetrationDepthSolver, 33

	.type	_ZTI30btConvexPenetrationDepthSolver,@object # @_ZTI30btConvexPenetrationDepthSolver
	.section	.rodata._ZTI30btConvexPenetrationDepthSolver,"aG",@progbits,_ZTI30btConvexPenetrationDepthSolver,comdat
	.weak	_ZTI30btConvexPenetrationDepthSolver
	.p2align	3
_ZTI30btConvexPenetrationDepthSolver:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS30btConvexPenetrationDepthSolver
	.size	_ZTI30btConvexPenetrationDepthSolver, 16

	.type	_ZTI33btMinkowskiPenetrationDepthSolver,@object # @_ZTI33btMinkowskiPenetrationDepthSolver
	.section	.rodata,"a",@progbits
	.globl	_ZTI33btMinkowskiPenetrationDepthSolver
	.p2align	4
_ZTI33btMinkowskiPenetrationDepthSolver:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS33btMinkowskiPenetrationDepthSolver
	.quad	_ZTI30btConvexPenetrationDepthSolver
	.size	_ZTI33btMinkowskiPenetrationDepthSolver, 24

	.type	_ZTVZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocE20btIntermediateResult,@object # @_ZTVZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocE20btIntermediateResult
	.p2align	3
_ZTVZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocE20btIntermediateResult:
	.quad	0
	.quad	_ZTIZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocE20btIntermediateResult
	.quad	_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev
	.quad	_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResultD0Ev
	.quad	_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResult20setShapeIdentifiersAEii
	.quad	_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResult20setShapeIdentifiersBEii
	.quad	_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocEN20btIntermediateResult15addContactPointERKS8_SG_f
	.size	_ZTVZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocE20btIntermediateResult, 56

	.type	_ZTSZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocE20btIntermediateResult,@object # @_ZTSZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocE20btIntermediateResult
	.p2align	4
_ZTSZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocE20btIntermediateResult:
	.asciz	"ZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocE20btIntermediateResult"
	.size	_ZTSZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocE20btIntermediateResult, 186

	.type	_ZTSN36btDiscreteCollisionDetectorInterface6ResultE,@object # @_ZTSN36btDiscreteCollisionDetectorInterface6ResultE
	.section	.rodata._ZTSN36btDiscreteCollisionDetectorInterface6ResultE,"aG",@progbits,_ZTSN36btDiscreteCollisionDetectorInterface6ResultE,comdat
	.weak	_ZTSN36btDiscreteCollisionDetectorInterface6ResultE
	.p2align	4
_ZTSN36btDiscreteCollisionDetectorInterface6ResultE:
	.asciz	"N36btDiscreteCollisionDetectorInterface6ResultE"
	.size	_ZTSN36btDiscreteCollisionDetectorInterface6ResultE, 48

	.type	_ZTIN36btDiscreteCollisionDetectorInterface6ResultE,@object # @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE
	.section	.rodata._ZTIN36btDiscreteCollisionDetectorInterface6ResultE,"aG",@progbits,_ZTIN36btDiscreteCollisionDetectorInterface6ResultE,comdat
	.weak	_ZTIN36btDiscreteCollisionDetectorInterface6ResultE
	.p2align	3
_ZTIN36btDiscreteCollisionDetectorInterface6ResultE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN36btDiscreteCollisionDetectorInterface6ResultE
	.size	_ZTIN36btDiscreteCollisionDetectorInterface6ResultE, 16

	.type	_ZTIZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocE20btIntermediateResult,@object # @_ZTIZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocE20btIntermediateResult
	.section	.rodata,"a",@progbits
	.p2align	4
_ZTIZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocE20btIntermediateResult:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocE20btIntermediateResult
	.quad	_ZTIN36btDiscreteCollisionDetectorInterface6ResultE
	.size	_ZTIZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawP12btStackAllocE20btIntermediateResult, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_btMinkowskiPenetrationDepthSolver.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
