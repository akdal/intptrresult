	.text
	.file	"error.bc"
	.globl	_ZN14kc_filePrinterC2EP8_IO_FILE
	.p2align	4, 0x90
	.type	_ZN14kc_filePrinterC2EP8_IO_FILE,@function
_ZN14kc_filePrinterC2EP8_IO_FILE:       # @_ZN14kc_filePrinterC2EP8_IO_FILE
	.cfi_startproc
# BB#0:
	movq	$_ZTV14kc_filePrinter+16, (%rdi)
	movq	%rsi, 8(%rdi)
	leaq	40(%rdi), %rax
	movq	%rax, 24(%rdi)
	movq	$0, 32(%rdi)
	movb	$0, 40(%rdi)
	movl	$0, 16(%rdi)
	movl	$0, 56(%rdi)
	movb	$0, 60(%rdi)
	movb	$10, 61(%rdi)
	movq	$0, 80(%rdi)
	movw	$0, 88(%rdi)
	movq	$0, 70(%rdi)
	movq	$0, 64(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN14kc_filePrinterC2EP8_IO_FILE, .Lfunc_end0-_ZN14kc_filePrinterC2EP8_IO_FILE
	.cfi_endproc

	.section	.text._ZN14kc_filePrinterD2Ev,"axG",@progbits,_ZN14kc_filePrinterD2Ev,comdat
	.weak	_ZN14kc_filePrinterD2Ev
	.p2align	4, 0x90
	.type	_ZN14kc_filePrinterD2Ev,@function
_ZN14kc_filePrinterD2Ev:                # @_ZN14kc_filePrinterD2Ev
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movq	$_ZTV14kc_filePrinter+16, (%rax)
	movq	24(%rax), %rdi
	addq	$40, %rax
	cmpq	%rax, %rdi
	je	.LBB1_1
# BB#2:
	jmp	_ZdlPv                  # TAILCALL
.LBB1_1:                                # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
	retq
.Lfunc_end1:
	.size	_ZN14kc_filePrinterD2Ev, .Lfunc_end1-_ZN14kc_filePrinterD2Ev
	.cfi_endproc

	.section	.text._ZN2kc21printer_functor_classD2Ev,"axG",@progbits,_ZN2kc21printer_functor_classD2Ev,comdat
	.weak	_ZN2kc21printer_functor_classD2Ev
	.p2align	4, 0x90
	.type	_ZN2kc21printer_functor_classD2Ev,@function
_ZN2kc21printer_functor_classD2Ev:      # @_ZN2kc21printer_functor_classD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end2:
	.size	_ZN2kc21printer_functor_classD2Ev, .Lfunc_end2-_ZN2kc21printer_functor_classD2Ev
	.cfi_endproc

	.text
	.globl	_ZN14kc_filePrinter13check_keywordEPKc
	.p2align	4, 0x90
	.type	_ZN14kc_filePrinter13check_keywordEPKc,@function
_ZN14kc_filePrinter13check_keywordEPKc: # @_ZN14kc_filePrinter13check_keywordEPKc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	cmpb	$0, 89(%r15)
	je	.LBB3_3
# BB#1:
	movsbl	(%rbx), %edi
	callq	isspace
	testl	%eax, %eax
	je	.LBB3_13
# BB#2:
	xorl	%eax, %eax
	jmp	.LBB3_23
.LBB3_3:
	movq	80(%r15), %r14
	movsbl	(%rbx), %ebp
	testq	%r14, %r14
	je	.LBB3_14
# BB#4:
	movl	%ebp, %edi
	callq	isalnum
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpb	$95, %bpl
	je	.LBB3_23
# BB#5:
	testl	%ecx, %ecx
	jne	.LBB3_23
# BB#6:
	subq	%r14, %rbx
	cmpq	$5, %rbx
	je	.LBB3_19
# BB#7:
	cmpq	$4, %rbx
	je	.LBB3_20
# BB#8:
	cmpq	$2, %rbx
	jne	.LBB3_12
# BB#9:
	movl	$.L.str, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_21
# BB#10:
	movl	$.L.str.5, %esi
	movl	$2, %edx
	jmp	.LBB3_11
.LBB3_13:
	movb	$0, 89(%r15)
	xorl	%eax, %eax
	jmp	.LBB3_23
.LBB3_14:
	movl	%ebp, %eax
	addb	$-100, %al
	cmpb	$19, %al
	ja	.LBB3_17
# BB#15:
	movzbl	%al, %eax
	movl	$524323, %ecx           # imm = 0x80023
	btq	%rax, %rcx
	jae	.LBB3_17
# BB#16:
	movq	%rbx, 80(%r15)
	xorl	%eax, %eax
	jmp	.LBB3_23
.LBB3_17:
	cmpb	$35, %bpl
	jne	.LBB3_24
# BB#18:
	movb	$1, 89(%r15)
	xorl	%eax, %eax
	jmp	.LBB3_23
.LBB3_19:
	movl	$.L.str.6, %esi
	movl	$5, %edx
	jmp	.LBB3_11
.LBB3_20:
	movl	$.L.str.4, %esi
	movl	$4, %edx
.LBB3_11:
	movq	%r14, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB3_21
.LBB3_12:
	xorl	%eax, %eax
	jmp	.LBB3_22
.LBB3_21:
	movb	$1, %al
.LBB3_22:                               # %.thread20
	movq	$0, 80(%r15)
.LBB3_23:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_24:
	xorl	%eax, %eax
	jmp	.LBB3_23
.Lfunc_end3:
	.size	_ZN14kc_filePrinter13check_keywordEPKc, .Lfunc_end3-_ZN14kc_filePrinter13check_keywordEPKc
	.cfi_endproc

	.globl	_ZN14kc_filePrinterclEPKcRN2kc11uview_classE
	.p2align	4, 0x90
	.type	_ZN14kc_filePrinterclEPKcRN2kc11uview_classE,@function
_ZN14kc_filePrinterclEPKcRN2kc11uview_classE: # @_ZN14kc_filePrinterclEPKcRN2kc11uview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 64
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	(%r14), %eax
	cmpl	$19, %eax
	je	.LBB4_3
# BB#1:
	cmpl	$21, %eax
	jne	.LBB4_6
# BB#2:
	movl	$0, 56(%r12)
	jmp	.LBB4_106
.LBB4_3:
	movq	8(%r12), %rsi
	movl	$10, %edi
	callq	fputc
	movl	16(%r12), %ecx
	leal	1(%rcx), %eax
	movl	%eax, 16(%r12)
	cmpb	$0, g_options+114(%rip)
	je	.LBB4_5
# BB#4:
	movq	pg_line(%rip), %rdx
	addl	$2, %ecx
	movq	g_options+312(%rip), %r8
	movq	8(%r12), %rdi
	movq	24(%r12), %r9
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	16(%r12), %eax
.LBB4_5:
	incl	%eax
	movl	%eax, 16(%r12)
	movb	$10, 61(%r12)
	jmp	.LBB4_106
.LBB4_6:                                # %.preheader76
	movb	(%r15), %r13b
	testb	%r13b, %r13b
	je	.LBB4_105
# BB#7:
	movl	$10496, %ebp            # imm = 0x2900
	jmp	.LBB4_103
	.p2align	4, 0x90
.LBB4_8:                                #   in Loop: Header=BB4_103 Depth=1
	incl	16(%r12)
	movb	$1, 77(%r12)
.LBB4_9:                                #   in Loop: Header=BB4_103 Depth=1
	movl	(%r14), %eax
	cmpl	_ZN2kc17view_gen_unpstr_cE(%rip), %eax
	jne	.LBB4_14
# BB#10:                                #   in Loop: Header=BB4_103 Depth=1
	movl	56(%r12), %eax
	cmpl	$200, %eax
	jl	.LBB4_13
# BB#11:                                #   in Loop: Header=BB4_103 Depth=1
	cmpb	$0, 60(%r12)
	je	.LBB4_29
# BB#12:                                #   in Loop: Header=BB4_103 Depth=1
	movq	8(%r12), %rcx
	movl	$.L.str.9, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	movq	ug_viewnameopt(%rip), %rdi
	movq	(%rdi), %rax
	movl	$_ZN2kc28view_gen_unparsedefs_other_cE, %edx
	movq	%r12, %rsi
	callq	*72(%rax)
	movq	8(%r12), %rcx
	movl	$.L.str.10, %edi
	movl	$25, %esi
	movl	$1, %edx
	callq	fwrite
	incl	16(%r12)
	movl	$0, 56(%r12)
	movb	$0, 60(%r12)
	xorl	%eax, %eax
.LBB4_13:                               #   in Loop: Header=BB4_103 Depth=1
	incl	%eax
	movl	%eax, 56(%r12)
	cmpb	$0, 72(%r12)
	jne	.LBB4_18
	jmp	.LBB4_21
	.p2align	4, 0x90
.LBB4_14:                               #   in Loop: Header=BB4_103 Depth=1
	cmpb	$92, %r13b
	jne	.LBB4_17
# BB#15:                                #   in Loop: Header=BB4_103 Depth=1
	cmpl	_ZN2kc13view_filenameE(%rip), %eax
	jne	.LBB4_17
# BB#16:                                #   in Loop: Header=BB4_103 Depth=1
	movq	8(%r12), %rsi
	movl	%ebx, %edi
	callq	_IO_putc
.LBB4_17:                               #   in Loop: Header=BB4_103 Depth=1
	cmpb	$0, 72(%r12)
	je	.LBB4_21
.LBB4_18:                               #   in Loop: Header=BB4_103 Depth=1
	cmpb	$34, %r13b
	jne	.LBB4_37
# BB#19:                                #   in Loop: Header=BB4_103 Depth=1
	testb	$1, 68(%r12)
	jne	.LBB4_37
# BB#20:                                #   in Loop: Header=BB4_103 Depth=1
	movb	$0, 72(%r12)
	jmp	.LBB4_37
	.p2align	4, 0x90
.LBB4_21:                               #   in Loop: Header=BB4_103 Depth=1
	cmpb	$0, 73(%r12)
	je	.LBB4_25
# BB#22:                                #   in Loop: Header=BB4_103 Depth=1
	cmpb	$39, %r13b
	jne	.LBB4_37
# BB#23:                                #   in Loop: Header=BB4_103 Depth=1
	testb	$1, 68(%r12)
	jne	.LBB4_37
# BB#24:                                #   in Loop: Header=BB4_103 Depth=1
	movb	$0, 73(%r12)
	jmp	.LBB4_37
.LBB4_25:                               #   in Loop: Header=BB4_103 Depth=1
	cmpb	$0, 74(%r12)
	je	.LBB4_32
# BB#26:                                #   in Loop: Header=BB4_103 Depth=1
	cmpb	$47, %r13b
	jne	.LBB4_35
# BB#27:                                #   in Loop: Header=BB4_103 Depth=1
	cmpb	$42, 61(%r12)
	jne	.LBB4_35
# BB#28:                                #   in Loop: Header=BB4_103 Depth=1
	movb	$0, 74(%r12)
	cmpl	$13, %ebx
	jbe	.LBB4_36
	jmp	.LBB4_37
.LBB4_29:                               #   in Loop: Header=BB4_103 Depth=1
	cmpl	$10, %ebx
	je	.LBB4_13
# BB#30:                                #   in Loop: Header=BB4_103 Depth=1
	cmpl	$92, %ebx
	je	.LBB4_13
# BB#31:                                #   in Loop: Header=BB4_103 Depth=1
	movb	$1, 60(%r12)
	jmp	.LBB4_13
.LBB4_32:                               #   in Loop: Header=BB4_103 Depth=1
	cmpb	$0, 75(%r12)
	je	.LBB4_41
# BB#33:                                #   in Loop: Header=BB4_103 Depth=1
	cmpb	$10, %r13b
	jne	.LBB4_35
# BB#34:                                #   in Loop: Header=BB4_103 Depth=1
	movb	$0, 75(%r12)
.LBB4_35:                               #   in Loop: Header=BB4_103 Depth=1
	cmpl	$13, %ebx
	ja	.LBB4_37
.LBB4_36:                               #   in Loop: Header=BB4_103 Depth=1
	btl	%ebx, %ebp
	jb	.LBB4_38
	.p2align	4, 0x90
.LBB4_37:                               #   in Loop: Header=BB4_103 Depth=1
	movq	8(%r12), %rsi
	movl	%ebx, %edi
	callq	_IO_putc
	movb	%r13b, 61(%r12)
.LBB4_38:                               #   in Loop: Header=BB4_103 Depth=1
	incq	%r15
	xorl	%eax, %eax
	cmpb	$92, %r13b
	jne	.LBB4_40
# BB#39:                                #   in Loop: Header=BB4_103 Depth=1
	movl	68(%r12), %eax
	incl	%eax
.LBB4_40:                               #   in Loop: Header=BB4_103 Depth=1
	movl	%eax, 68(%r12)
	movb	(%r15), %r13b
	testb	%r13b, %r13b
	jne	.LBB4_103
	jmp	.LBB4_105
.LBB4_41:                               #   in Loop: Header=BB4_103 Depth=1
	movb	$1, %al
	cmpb	$0, 88(%r12)
	jne	.LBB4_44
# BB#42:                                #   in Loop: Header=BB4_103 Depth=1
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_ZN14kc_filePrinter13check_keywordEPKc
	movb	%al, 88(%r12)
	testb	%al, %al
	je	.LBB4_44
# BB#43:                                #   in Loop: Header=BB4_103 Depth=1
	incl	64(%r12)
.LBB4_44:                               #   in Loop: Header=BB4_103 Depth=1
	xorl	%edx, %edx
	leal	-8(%rbx), %ecx
	cmpl	$51, %ecx
	ja	.LBB4_48
# BB#45:                                #   in Loop: Header=BB4_103 Depth=1
	movl	%r13d, %ebx
	jmpq	*.LJTI4_0(,%rcx,8)
.LBB4_46:                               #   in Loop: Header=BB4_103 Depth=1
	movsbl	61(%r12), %ebx
	cmpl	$8, %ebx
	jne	.LBB4_58
# BB#47:                                #   in Loop: Header=BB4_103 Depth=1
	movl	$0, (%rsp)              # 4-byte Folded Spill
	jmp	.LBB4_77
.LBB4_48:                               #   in Loop: Header=BB4_103 Depth=1
	cmpl	$123, %ebx
	je	.LBB4_66
# BB#49:                                #   in Loop: Header=BB4_103 Depth=1
	cmpl	$125, %ebx
	jne	.LBB4_69
.LBB4_50:                               #   in Loop: Header=BB4_103 Depth=1
	movl	64(%r12), %eax
	testl	%eax, %eax
	je	.LBB4_69
# BB#51:                                #   in Loop: Header=BB4_103 Depth=1
	decl	%eax
	movl	%eax, 64(%r12)
	jmp	.LBB4_69
.LBB4_52:                               #   in Loop: Header=BB4_103 Depth=1
	movb	%r13b, 61(%r12)
	jmp	.LBB4_96
.LBB4_53:                               #   in Loop: Header=BB4_103 Depth=1
	incl	64(%r12)
	jmp	.LBB4_96
.LBB4_54:                               #   in Loop: Header=BB4_103 Depth=1
	movl	64(%r12), %eax
	testl	%eax, %eax
	je	.LBB4_96
# BB#55:                                #   in Loop: Header=BB4_103 Depth=1
	decl	%eax
	movl	%eax, 64(%r12)
	jmp	.LBB4_96
.LBB4_56:                               #   in Loop: Header=BB4_103 Depth=1
	testb	%al, %al
	je	.LBB4_69
# BB#57:                                #   in Loop: Header=BB4_103 Depth=1
	decl	64(%r12)
	movb	$0, 88(%r12)
	jmp	.LBB4_69
.LBB4_58:                               #   in Loop: Header=BB4_103 Depth=1
	xorl	%edx, %edx
	cmpb	$0, 77(%r12)
	movabsq	$2305843009213694113, %rbp # imm = 0x20000000000000A1
	je	.LBB4_69
# BB#59:                                #   in Loop: Header=BB4_103 Depth=1
	movl	%ebx, %edi
	callq	isspace
	testl	%eax, %eax
	jne	.LBB4_96
# BB#60:                                #   in Loop: Header=BB4_103 Depth=1
	movl	%ebx, %edi
	callq	isalnum
	testl	%eax, %eax
	jne	.LBB4_63
# BB#61:                                # %switch.early.test
                                        #   in Loop: Header=BB4_103 Depth=1
	movl	%ebx, %eax
	addb	$-34, %al
	cmpb	$61, %al
	ja	.LBB4_102
# BB#62:                                # %switch.early.test
                                        #   in Loop: Header=BB4_103 Depth=1
	movzbl	%al, %eax
	btq	%rax, %rbp
	jae	.LBB4_102
.LBB4_63:                               #   in Loop: Header=BB4_103 Depth=1
	movb	2(%r15), %bpl
	movsbl	%bpl, %edi
	callq	isalnum
	movb	$32, %bl
	testl	%eax, %eax
	je	.LBB4_99
# BB#64:                                #   in Loop: Header=BB4_103 Depth=1
	xorl	%edx, %edx
	jmp	.LBB4_70
.LBB4_66:                               #   in Loop: Header=BB4_103 Depth=1
	testb	%al, %al
	je	.LBB4_68
# BB#67:                                #   in Loop: Header=BB4_103 Depth=1
	decl	64(%r12)
	movb	$0, 88(%r12)
.LBB4_68:                               #   in Loop: Header=BB4_103 Depth=1
	movl	$1, %edx
.LBB4_69:                               #   in Loop: Header=BB4_103 Depth=1
	movl	%r13d, %ebx
.LBB4_70:                               #   in Loop: Header=BB4_103 Depth=1
	cmpb	$10, 61(%r12)
	movl	%edx, (%rsp)            # 4-byte Spill
	jne	.LBB4_76
# BB#71:                                # %switch.early.test72
                                        #   in Loop: Header=BB4_103 Depth=1
	cmpb	$10, %bl
	je	.LBB4_76
# BB#72:                                # %switch.early.test72
                                        #   in Loop: Header=BB4_103 Depth=1
	cmpb	$35, %bl
	je	.LBB4_76
# BB#73:                                #   in Loop: Header=BB4_103 Depth=1
	movl	_ZN14kc_filePrinter12indent_levelE(%rip), %ebp
	imull	64(%r12), %ebp
	testl	%ebp, %ebp
	jle	.LBB4_76
.LBB4_74:                               # %.lr.ph80
                                        #   Parent Loop BB4_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$7, %ebp
	jle	.LBB4_97
# BB#75:                                # %.backedge
                                        #   in Loop: Header=BB4_74 Depth=2
	movq	8(%r12), %rsi
	movl	$9, %edi
	callq	_IO_putc
	addl	$-8, %ebp
	jne	.LBB4_74
.LBB4_76:                               #   in Loop: Header=BB4_103 Depth=1
	movl	%ebx, %r13d
.LBB4_77:                               # %.loopexit75
                                        #   in Loop: Header=BB4_103 Depth=1
	movsbl	%r13b, %edi
	movl	%edi, 4(%rsp)           # 4-byte Spill
	callq	isspace
	testl	%eax, %eax
	jne	.LBB4_79
# BB#78:                                #   in Loop: Header=BB4_103 Depth=1
	movb	$0, 77(%r12)
.LBB4_79:                               #   in Loop: Header=BB4_103 Depth=1
	movl	%r13d, %ebx
	addb	$-34, %bl
	movzbl	%bl, %ebp
	cmpb	$13, %bpl
	ja	.LBB4_89
# BB#80:                                #   in Loop: Header=BB4_103 Depth=1
	jmpq	*.LJTI4_1(,%rbp,8)
.LBB4_81:                               #   in Loop: Header=BB4_103 Depth=1
	testb	$1, 68(%r12)
	jne	.LBB4_89
# BB#82:                                #   in Loop: Header=BB4_103 Depth=1
	movb	$1, 72(%r12)
	cmpb	$0, 76(%r12)
	jne	.LBB4_90
	jmp	.LBB4_95
.LBB4_83:                               #   in Loop: Header=BB4_103 Depth=1
	testb	$1, 68(%r12)
	jne	.LBB4_89
# BB#84:                                #   in Loop: Header=BB4_103 Depth=1
	movb	$1, 73(%r12)
	cmpb	$0, 76(%r12)
	jne	.LBB4_90
	jmp	.LBB4_95
.LBB4_85:                               #   in Loop: Header=BB4_103 Depth=1
	cmpb	$47, 61(%r12)
	jne	.LBB4_89
# BB#86:                                #   in Loop: Header=BB4_103 Depth=1
	movb	$1, 74(%r12)
	cmpb	$0, 76(%r12)
	jne	.LBB4_90
	jmp	.LBB4_95
.LBB4_87:                               #   in Loop: Header=BB4_103 Depth=1
	cmpb	$47, 61(%r12)
	jne	.LBB4_89
# BB#88:                                #   in Loop: Header=BB4_103 Depth=1
	movb	$1, 75(%r12)
.LBB4_89:                               # %.thread
                                        #   in Loop: Header=BB4_103 Depth=1
	cmpb	$0, 76(%r12)
	je	.LBB4_95
.LBB4_90:                               #   in Loop: Header=BB4_103 Depth=1
	movl	4(%rsp), %edi           # 4-byte Reload
	callq	isalnum
	testl	%eax, %eax
	jne	.LBB4_93
# BB#91:                                # %switch.early.test73
                                        #   in Loop: Header=BB4_103 Depth=1
	cmpb	$61, %bl
	ja	.LBB4_94
# BB#92:                                # %switch.early.test73
                                        #   in Loop: Header=BB4_103 Depth=1
	movabsq	$2305843009213693985, %rax # imm = 0x2000000000000021
	btq	%rbp, %rax
	jae	.LBB4_94
.LBB4_93:                               #   in Loop: Header=BB4_103 Depth=1
	movq	8(%r12), %rsi
	movl	$32, %edi
	callq	_IO_putc
.LBB4_94:                               #   in Loop: Header=BB4_103 Depth=1
	movb	$0, 76(%r12)
.LBB4_95:                               #   in Loop: Header=BB4_103 Depth=1
	movq	8(%r12), %rsi
	movl	4(%rsp), %edi           # 4-byte Reload
	callq	_IO_putc
	movb	%r13b, 61(%r12)
	movl	(%rsp), %eax            # 4-byte Reload
	addl	%eax, 64(%r12)
.LBB4_96:                               #   in Loop: Header=BB4_103 Depth=1
	movl	$10496, %ebp            # imm = 0x2900
	jmp	.LBB4_38
.LBB4_97:                               # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_103 Depth=1
	movl	%ebx, %r13d
	xorl	%ebx, %ebx
.LBB4_98:                               # %.lr.ph
                                        #   Parent Loop BB4_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r12), %rsi
	movl	$32, %edi
	callq	_IO_putc
	incl	%ebx
	cmpl	%ebx, %ebp
	jne	.LBB4_98
	jmp	.LBB4_77
.LBB4_99:                               # %switch.early.test71
                                        #   in Loop: Header=BB4_103 Depth=1
	addb	$-34, %bpl
	cmpb	$61, %bpl
	movl	$0, %edx
	ja	.LBB4_101
# BB#100:                               # %switch.early.test71
                                        #   in Loop: Header=BB4_103 Depth=1
	movzbl	%bpl, %eax
	movabsq	$2305843009213693985, %rcx # imm = 0x2000000000000021
	btq	%rax, %rcx
	jb	.LBB4_70
.LBB4_101:                              #   in Loop: Header=BB4_103 Depth=1
	movb	$1, 76(%r12)
	jmp	.LBB4_96
.LBB4_102:                              # %switch.early.test
                                        #   in Loop: Header=BB4_103 Depth=1
	cmpb	$125, %bl
	je	.LBB4_63
	jmp	.LBB4_96
	.p2align	4, 0x90
.LBB4_103:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_74 Depth 2
                                        #     Child Loop BB4_98 Depth 2
	movsbl	%r13b, %ebx
	cmpl	$10, %ebx
	je	.LBB4_8
# BB#104:                               #   in Loop: Header=BB4_103 Depth=1
	testl	%ebx, %ebx
	jne	.LBB4_9
	jmp	.LBB4_106
.LBB4_105:                              # %._crit_edge
	movq	$0, 80(%r12)
.LBB4_106:                              # %.loopexit77
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN14kc_filePrinterclEPKcRN2kc11uview_classE, .Lfunc_end4-_ZN14kc_filePrinterclEPKcRN2kc11uview_classE
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_52
	.quad	.LBB4_46
	.quad	.LBB4_70
	.quad	.LBB4_53
	.quad	.LBB4_70
	.quad	.LBB4_54
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_46
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_68
	.quad	.LBB4_50
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_70
	.quad	.LBB4_56
.LJTI4_1:
	.quad	.LBB4_81
	.quad	.LBB4_89
	.quad	.LBB4_89
	.quad	.LBB4_89
	.quad	.LBB4_89
	.quad	.LBB4_83
	.quad	.LBB4_89
	.quad	.LBB4_89
	.quad	.LBB4_85
	.quad	.LBB4_89
	.quad	.LBB4_89
	.quad	.LBB4_89
	.quad	.LBB4_89
	.quad	.LBB4_87

	.text
	.globl	_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align	4, 0x90
	.type	_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,@function
_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE: # @_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 64
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	%r15, %rdi
	movq	%rdx, %rsi
	callq	fopen
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	jne	.LBB5_2
# BB#1:
	callq	_ZN2kc10NoFileLineEv
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	(%r14), %r13
	movl	$.L.str.11, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %r12
	movl	$-1, %esi
	movq	%r13, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %r13
	movl	$.L.str.12, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rbp
	movl	$-1, %esi
	movq	%r15, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	movq	%rax, %rcx
	callq	_ZN2kc8Problem4EPNS_20impl_casestring__StrES1_S1_S1_
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rax, %rsi
	callq	_ZN2kc5FatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB5_2:
	movl	$1, 16(%rbx)
	leaq	24(%rbx), %rdi
	movq	%r14, %rsi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_
	movb	$10, 61(%rbx)
	movl	$0, 64(%rbx)
	movb	$0, 72(%rbx)
	movl	$0, 74(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .Lfunc_end5-_ZN14kc_filePrinter4initEPKcS1_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc

	.globl	_ZN2kc8v_reportEPNS_10impl_errorE
	.p2align	4, 0x90
	.type	_ZN2kc8v_reportEPNS_10impl_errorE,@function
_ZN2kc8v_reportEPNS_10impl_errorE:      # @_ZN2kc8v_reportEPNS_10impl_errorE
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 40
	subq	$56, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 96
.Lcfi40:
	.cfi_offset %rbx, -40
.Lcfi41:
	.cfi_offset %r12, -32
.Lcfi42:
	.cfi_offset %r14, -24
.Lcfi43:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	g_options+288(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB6_15
# BB#1:
	movl	$1, 16(%rsp)
	leaq	40(%rsp), %r12
	movq	%r12, 24(%rsp)
	movq	g_options+280(%rip), %r15
	testq	%r15, %r15
	je	.LBB6_16
# BB#2:
	movq	%rbx, 8(%rsp)
	cmpq	$15, %rbx
	jbe	.LBB6_3
# BB#4:                                 # %.noexc6.i.i
	leaq	24(%rsp), %rdi
	leaq	8(%rsp), %rsi
	xorl	%edx, %edx
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm
	movq	%rax, 24(%rsp)
	movq	8(%rsp), %rcx
	movq	%rcx, 40(%rsp)
	cmpq	$1, %rbx
	je	.LBB6_6
	jmp	.LBB6_7
.LBB6_15:
	movl	$_ZN2kcL16v_stderr_printerEPKcRNS_11uview_classE, %esi
	movl	$_ZN2kc10view_errorE, %edx
	movq	%r14, %rdi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kc20impl_abstract_phylum7unparseEPFvPKcRNS_11uview_classEES4_ # TAILCALL
.LBB6_3:                                # %._crit_edge.i.i.i.i.i
	movq	%r12, %rax
	cmpq	$1, %rbx
	jne	.LBB6_7
.LBB6_6:
	movb	(%r15), %cl
	movb	%cl, (%rax)
	jmp	.LBB6_8
.LBB6_7:
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	memcpy
.LBB6_8:                                # %_ZN2kc23view_error_format_classC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.exit
	movq	8(%rsp), %rax
	movq	%rax, 32(%rsp)
	movq	24(%rsp), %rcx
	movb	$0, (%rcx,%rax)
.Ltmp0:
	leaq	16(%rsp), %rdx
	movl	$_ZN2kcL16v_stderr_printerEPKcRNS_11uview_classE, %esi
	movq	%r14, %rdi
	callq	_ZN2kc20impl_abstract_phylum7unparseEPFvPKcRNS_11uview_classEES4_
.Ltmp1:
# BB#9:
	movq	24(%rsp), %rdi
	cmpq	%r12, %rdi
	je	.LBB6_11
# BB#10:
	callq	_ZdlPv
.LBB6_11:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB6_16:                               # %.noexc.i.i
	movl	$.L.str.13, %edi
	callq	_ZSt19__throw_logic_errorPKc
.LBB6_12:
.Ltmp2:
	movq	%rax, %rbx
	movq	24(%rsp), %rdi
	cmpq	%r12, %rdi
	je	.LBB6_14
# BB#13:
	callq	_ZdlPv
.LBB6_14:                               # %_ZN2kc23view_error_format_classD2Ev.exit3
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN2kc8v_reportEPNS_10impl_errorE, .Lfunc_end6-_ZN2kc8v_reportEPNS_10impl_errorE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end6-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN2kc9Problem4SEPKcS1_S1_S1_
	.p2align	4, 0x90
	.type	_ZN2kc9Problem4SEPKcS1_S1_S1_,@function
_ZN2kc9Problem4SEPKcS1_S1_S1_:          # @_ZN2kc9Problem4SEPKcS1_S1_S1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 48
.Lcfi49:
	.cfi_offset %rbx, -48
.Lcfi50:
	.cfi_offset %r12, -40
.Lcfi51:
	.cfi_offset %r13, -32
.Lcfi52:
	.cfi_offset %r14, -24
.Lcfi53:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %r12
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %r13
	movl	$-1, %esi
	movq	%r15, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rbx
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%rax, %rcx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	_ZN2kc8Problem4EPNS_20impl_casestring__StrES1_S1_S1_ # TAILCALL
.Lfunc_end7:
	.size	_ZN2kc9Problem4SEPKcS1_S1_S1_, .Lfunc_end7-_ZN2kc9Problem4SEPKcS1_S1_S1_
	.cfi_endproc

	.globl	_ZN2kc9Problem1SEPKc
	.p2align	4, 0x90
	.type	_ZN2kc9Problem1SEPKc,@function
_ZN2kc9Problem1SEPKc:                   # @_ZN2kc9Problem1SEPKc
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 16
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	popq	%rax
	jmp	_ZN2kc8Problem1EPNS_20impl_casestring__StrE # TAILCALL
.Lfunc_end8:
	.size	_ZN2kc9Problem1SEPKc, .Lfunc_end8-_ZN2kc9Problem1SEPKc
	.cfi_endproc

	.globl	_ZN2kc12Problem1S1weEPKcPNS_19impl_withexpressionE
	.p2align	4, 0x90
	.type	_ZN2kc12Problem1S1weEPKcPNS_19impl_withexpressionE,@function
_ZN2kc12Problem1S1weEPKcPNS_19impl_withexpressionE: # @_ZN2kc12Problem1S1weEPKcPNS_19impl_withexpressionE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 16
.Lcfi56:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	movq	%rbx, %rsi
	popq	%rbx
	jmp	_ZN2kc10Problem1weEPNS_20impl_casestring__StrEPNS_19impl_withexpressionE # TAILCALL
.Lfunc_end9:
	.size	_ZN2kc12Problem1S1weEPKcPNS_19impl_withexpressionE, .Lfunc_end9-_ZN2kc12Problem1S1weEPKcPNS_19impl_withexpressionE
	.cfi_endproc

	.globl	_ZN2kc12Problem1S1IDEPKcPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc12Problem1S1IDEPKcPNS_7impl_IDE,@function
_ZN2kc12Problem1S1IDEPKcPNS_7impl_IDE:  # @_ZN2kc12Problem1S1IDEPKcPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 16
.Lcfi58:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	movq	%rbx, %rsi
	popq	%rbx
	jmp	_ZN2kc10Problem1IDEPNS_20impl_casestring__StrEPNS_7impl_IDE # TAILCALL
.Lfunc_end10:
	.size	_ZN2kc12Problem1S1IDEPKcPNS_7impl_IDE, .Lfunc_end10-_ZN2kc12Problem1S1IDEPKcPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc13Problem1S1tIDEPKcPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc13Problem1S1tIDEPKcPNS_7impl_IDE,@function
_ZN2kc13Problem1S1tIDEPKcPNS_7impl_IDE: # @_ZN2kc13Problem1S1tIDEPKcPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 16
.Lcfi60:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	movq	%rbx, %rsi
	popq	%rbx
	jmp	_ZN2kc11Problem1tIDEPNS_20impl_casestring__StrEPNS_7impl_IDE # TAILCALL
.Lfunc_end11:
	.size	_ZN2kc13Problem1S1tIDEPKcPNS_7impl_IDE, .Lfunc_end11-_ZN2kc13Problem1S1tIDEPKcPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc17Problem1S1ID1S1IDEPKcPNS_7impl_IDES1_S3_
	.p2align	4, 0x90
	.type	_ZN2kc17Problem1S1ID1S1IDEPKcPNS_7impl_IDES1_S3_,@function
_ZN2kc17Problem1S1ID1S1IDEPKcPNS_7impl_IDES1_S3_: # @_ZN2kc17Problem1S1ID1S1IDEPKcPNS_7impl_IDES1_S3_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 48
.Lcfi66:
	.cfi_offset %rbx, -40
.Lcfi67:
	.cfi_offset %r12, -32
.Lcfi68:
	.cfi_offset %r14, -24
.Lcfi69:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r12
	movq	%rsi, %r15
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rbx
	movl	$-1, %esi
	movq	%r12, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	movq	%r14, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kc13Problem1ID1IDEPNS_20impl_casestring__StrEPNS_7impl_IDES1_S3_ # TAILCALL
.Lfunc_end12:
	.size	_ZN2kc17Problem1S1ID1S1IDEPKcPNS_7impl_IDES1_S3_, .Lfunc_end12-_ZN2kc17Problem1S1ID1S1IDEPKcPNS_7impl_IDES1_S3_
	.cfi_endproc

	.globl	_ZN2kc16Problem1S1t1S1IDEPKcPNS_11impl_IDtypeES1_PNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc16Problem1S1t1S1IDEPKcPNS_11impl_IDtypeES1_PNS_7impl_IDE,@function
_ZN2kc16Problem1S1t1S1IDEPKcPNS_11impl_IDtypeES1_PNS_7impl_IDE: # @_ZN2kc16Problem1S1t1S1IDEPKcPNS_11impl_IDtypeES1_PNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi74:
	.cfi_def_cfa_offset 48
.Lcfi75:
	.cfi_offset %rbx, -40
.Lcfi76:
	.cfi_offset %r12, -32
.Lcfi77:
	.cfi_offset %r14, -24
.Lcfi78:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r12
	movq	%rsi, %r15
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rbx
	movl	$-1, %esi
	movq	%r12, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	movq	%r14, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kc12Problem1t1IDEPNS_20impl_casestring__StrEPNS_11impl_IDtypeES1_PNS_7impl_IDE # TAILCALL
.Lfunc_end13:
	.size	_ZN2kc16Problem1S1t1S1IDEPKcPNS_11impl_IDtypeES1_PNS_7impl_IDE, .Lfunc_end13-_ZN2kc16Problem1S1t1S1IDEPKcPNS_11impl_IDtypeES1_PNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc13Problem1S1INTEPKcPNS_8impl_INTE
	.p2align	4, 0x90
	.type	_ZN2kc13Problem1S1INTEPKcPNS_8impl_INTE,@function
_ZN2kc13Problem1S1INTEPKcPNS_8impl_INTE: # @_ZN2kc13Problem1S1INTEPKcPNS_8impl_INTE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 16
.Lcfi80:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	movq	%rbx, %rsi
	popq	%rbx
	jmp	_ZN2kc11Problem1INTEPNS_20impl_casestring__StrEPNS_8impl_INTE # TAILCALL
.Lfunc_end14:
	.size	_ZN2kc13Problem1S1INTEPKcPNS_8impl_INTE, .Lfunc_end14-_ZN2kc13Problem1S1INTEPKcPNS_8impl_INTE
	.cfi_endproc

	.globl	_ZN2kc15Problem1S1int1SEPKciS1_
	.p2align	4, 0x90
	.type	_ZN2kc15Problem1S1int1SEPKciS1_,@function
_ZN2kc15Problem1S1int1SEPKciS1_:        # @_ZN2kc15Problem1S1int1SEPKciS1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 32
.Lcfi84:
	.cfi_offset %rbx, -32
.Lcfi85:
	.cfi_offset %r14, -24
.Lcfi86:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %ebp
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rbx
	movl	%ebp, %edi
	callq	_ZN2kc9mkintegerEi
	movq	%rax, %rbp
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	_ZN2kc12Problem1int1EPNS_20impl_casestring__StrEPNS_17impl_integer__IntES1_ # TAILCALL
.Lfunc_end15:
	.size	_ZN2kc15Problem1S1int1SEPKciS1_, .Lfunc_end15-_ZN2kc15Problem1S1int1SEPKciS1_
	.cfi_endproc

	.globl	_ZN2kc18Problem1S1INT1S1IDEPKcPNS_8impl_INTES1_PNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc18Problem1S1INT1S1IDEPKcPNS_8impl_INTES1_PNS_7impl_IDE,@function
_ZN2kc18Problem1S1INT1S1IDEPKcPNS_8impl_INTES1_PNS_7impl_IDE: # @_ZN2kc18Problem1S1INT1S1IDEPKcPNS_8impl_INTES1_PNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi89:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi91:
	.cfi_def_cfa_offset 48
.Lcfi92:
	.cfi_offset %rbx, -40
.Lcfi93:
	.cfi_offset %r12, -32
.Lcfi94:
	.cfi_offset %r14, -24
.Lcfi95:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r12
	movq	%rsi, %r15
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rbx
	movl	$-1, %esi
	movq	%r12, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	movq	%r14, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kc14Problem1INT1IDEPNS_20impl_casestring__StrEPNS_8impl_INTES1_PNS_7impl_IDE # TAILCALL
.Lfunc_end16:
	.size	_ZN2kc18Problem1S1INT1S1IDEPKcPNS_8impl_INTES1_PNS_7impl_IDE, .Lfunc_end16-_ZN2kc18Problem1S1INT1S1IDEPKcPNS_8impl_INTES1_PNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc22Problem1S1ID1S1ID1S1IDEPKcPNS_7impl_IDES1_S3_S1_S3_
	.p2align	4, 0x90
	.type	_ZN2kc22Problem1S1ID1S1ID1S1IDEPKcPNS_7impl_IDES1_S3_S1_S3_,@function
_ZN2kc22Problem1S1ID1S1ID1S1IDEPKcPNS_7impl_IDES1_S3_S1_S3_: # @_ZN2kc22Problem1S1ID1S1ID1S1IDEPKcPNS_7impl_IDES1_S3_S1_S3_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi100:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi102:
	.cfi_def_cfa_offset 64
.Lcfi103:
	.cfi_offset %rbx, -56
.Lcfi104:
	.cfi_offset %r12, -48
.Lcfi105:
	.cfi_offset %r13, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r12
	movq	%rcx, %r15
	movq	%rdx, %rbx
	movq	%rsi, %r13
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rbp
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rbx
	movl	$-1, %esi
	movq	%r12, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	movq	%rax, %r8
	movq	%r14, %r9
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc16Problem1ID1ID1IDEPNS_20impl_casestring__StrEPNS_7impl_IDES1_S3_S1_S3_ # TAILCALL
.Lfunc_end17:
	.size	_ZN2kc22Problem1S1ID1S1ID1S1IDEPKcPNS_7impl_IDES1_S3_S1_S3_, .Lfunc_end17-_ZN2kc22Problem1S1ID1S1ID1S1IDEPKcPNS_7impl_IDES1_S3_S1_S3_
	.cfi_endproc

	.globl	_ZN2kc23Problem1S1INT1S1ID1S1IDEPKcPNS_8impl_INTES1_PNS_7impl_IDES1_S5_
	.p2align	4, 0x90
	.type	_ZN2kc23Problem1S1INT1S1ID1S1IDEPKcPNS_8impl_INTES1_PNS_7impl_IDES1_S5_,@function
_ZN2kc23Problem1S1INT1S1ID1S1IDEPKcPNS_8impl_INTES1_PNS_7impl_IDES1_S5_: # @_ZN2kc23Problem1S1INT1S1ID1S1IDEPKcPNS_8impl_INTES1_PNS_7impl_IDES1_S5_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi112:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi113:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi115:
	.cfi_def_cfa_offset 64
.Lcfi116:
	.cfi_offset %rbx, -56
.Lcfi117:
	.cfi_offset %r12, -48
.Lcfi118:
	.cfi_offset %r13, -40
.Lcfi119:
	.cfi_offset %r14, -32
.Lcfi120:
	.cfi_offset %r15, -24
.Lcfi121:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r12
	movq	%rcx, %r15
	movq	%rdx, %rbx
	movq	%rsi, %r13
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rbp
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rbx
	movl	$-1, %esi
	movq	%r12, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	movq	%rax, %r8
	movq	%r14, %r9
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc17Problem1INT1ID1IDEPNS_20impl_casestring__StrEPNS_8impl_INTES1_PNS_7impl_IDES1_S5_ # TAILCALL
.Lfunc_end18:
	.size	_ZN2kc23Problem1S1INT1S1ID1S1IDEPKcPNS_8impl_INTES1_PNS_7impl_IDES1_S5_, .Lfunc_end18-_ZN2kc23Problem1S1INT1S1ID1S1IDEPKcPNS_8impl_INTES1_PNS_7impl_IDES1_S5_
	.cfi_endproc

	.globl	_ZN2kc28Problem1S1storageoption1S1IDEPKcPNS_18impl_storageoptionES1_PNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc28Problem1S1storageoption1S1IDEPKcPNS_18impl_storageoptionES1_PNS_7impl_IDE,@function
_ZN2kc28Problem1S1storageoption1S1IDEPKcPNS_18impl_storageoptionES1_PNS_7impl_IDE: # @_ZN2kc28Problem1S1storageoption1S1IDEPKcPNS_18impl_storageoptionES1_PNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi124:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi125:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi126:
	.cfi_def_cfa_offset 48
.Lcfi127:
	.cfi_offset %rbx, -40
.Lcfi128:
	.cfi_offset %r12, -32
.Lcfi129:
	.cfi_offset %r14, -24
.Lcfi130:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r12
	movq	%rsi, %r15
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rbx
	movl	$-1, %esi
	movq	%r12, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	movq	%r14, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN2kc24Problem1storageoption1IDEPNS_20impl_casestring__StrEPNS_18impl_storageoptionES1_PNS_7impl_IDE # TAILCALL
.Lfunc_end19:
	.size	_ZN2kc28Problem1S1storageoption1S1IDEPKcPNS_18impl_storageoptionES1_PNS_7impl_IDE, .Lfunc_end19-_ZN2kc28Problem1S1storageoption1S1IDEPKcPNS_18impl_storageoptionES1_PNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc9Problem2SEPKcS1_
	.p2align	4, 0x90
	.type	_ZN2kc9Problem2SEPKcS1_,@function
_ZN2kc9Problem2SEPKcS1_:                # @_ZN2kc9Problem2SEPKcS1_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi132:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi133:
	.cfi_def_cfa_offset 32
.Lcfi134:
	.cfi_offset %rbx, -24
.Lcfi135:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rbx
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rbx, %rdi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc8Problem2EPNS_20impl_casestring__StrES1_ # TAILCALL
.Lfunc_end20:
	.size	_ZN2kc9Problem2SEPKcS1_, .Lfunc_end20-_ZN2kc9Problem2SEPKcS1_
	.cfi_endproc

	.globl	_ZN2kc9ProblemSCEPKcPNS_20impl_casestring__StrE
	.p2align	4, 0x90
	.type	_ZN2kc9ProblemSCEPKcPNS_20impl_casestring__StrE,@function
_ZN2kc9ProblemSCEPKcPNS_20impl_casestring__StrE: # @_ZN2kc9ProblemSCEPKcPNS_20impl_casestring__StrE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi136:
	.cfi_def_cfa_offset 16
.Lcfi137:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	movq	%rbx, %rsi
	popq	%rbx
	jmp	_ZN2kc8Problem2EPNS_20impl_casestring__StrES1_ # TAILCALL
.Lfunc_end21:
	.size	_ZN2kc9ProblemSCEPKcPNS_20impl_casestring__StrE, .Lfunc_end21-_ZN2kc9ProblemSCEPKcPNS_20impl_casestring__StrE
	.cfi_endproc

	.globl	_ZN2kc9Problem3SEPKcS1_S1_
	.p2align	4, 0x90
	.type	_ZN2kc9Problem3SEPKcS1_S1_,@function
_ZN2kc9Problem3SEPKcS1_S1_:             # @_ZN2kc9Problem3SEPKcS1_S1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi138:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi139:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi140:
	.cfi_def_cfa_offset 32
.Lcfi141:
	.cfi_offset %rbx, -32
.Lcfi142:
	.cfi_offset %r14, -24
.Lcfi143:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %r15
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rbx
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN2kc8Problem3EPNS_20impl_casestring__StrES1_S1_ # TAILCALL
.Lfunc_end22:
	.size	_ZN2kc9Problem3SEPKcS1_S1_, .Lfunc_end22-_ZN2kc9Problem3SEPKcS1_S1_
	.cfi_endproc

	.globl	_ZN2kc15Problem3S1int1SEPKcS1_S1_iS1_
	.p2align	4, 0x90
	.type	_ZN2kc15Problem3S1int1SEPKcS1_S1_iS1_,@function
_ZN2kc15Problem3S1int1SEPKcS1_S1_iS1_:  # @_ZN2kc15Problem3S1int1SEPKcS1_S1_iS1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi144:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi145:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi146:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi147:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi148:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi149:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi150:
	.cfi_def_cfa_offset 64
.Lcfi151:
	.cfi_offset %rbx, -56
.Lcfi152:
	.cfi_offset %r12, -48
.Lcfi153:
	.cfi_offset %r13, -40
.Lcfi154:
	.cfi_offset %r14, -32
.Lcfi155:
	.cfi_offset %r15, -24
.Lcfi156:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movl	%ecx, %r15d
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %r12
	movl	$-1, %esi
	movq	%rbp, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %r13
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rbx
	movl	%r15d, %edi
	callq	_ZN2kc9mkintegerEi
	movq	%rax, %rbp
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%rbp, %rcx
	movq	%rax, %r8
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc12Problem3int1EPNS_20impl_casestring__StrES1_S1_PNS_17impl_integer__IntES1_ # TAILCALL
.Lfunc_end23:
	.size	_ZN2kc15Problem3S1int1SEPKcS1_S1_iS1_, .Lfunc_end23-_ZN2kc15Problem3S1int1SEPKcS1_S1_iS1_
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN2kcL16v_stderr_printerEPKcRNS_11uview_classE,@function
_ZN2kcL16v_stderr_printerEPKcRNS_11uview_classE: # @_ZN2kcL16v_stderr_printerEPKcRNS_11uview_classE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 16
.Lcfi158:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rsi
	movq	%rbx, %rdi
	callq	fputs
	movq	stderr(%rip), %rdi
	popq	%rbx
	jmp	fflush                  # TAILCALL
.Lfunc_end24:
	.size	_ZN2kcL16v_stderr_printerEPKcRNS_11uview_classE, .Lfunc_end24-_ZN2kcL16v_stderr_printerEPKcRNS_11uview_classE
	.cfi_endproc

	.section	.text._ZN14kc_filePrinterD0Ev,"axG",@progbits,_ZN14kc_filePrinterD0Ev,comdat
	.weak	_ZN14kc_filePrinterD0Ev
	.p2align	4, 0x90
	.type	_ZN14kc_filePrinterD0Ev,@function
_ZN14kc_filePrinterD0Ev:                # @_ZN14kc_filePrinterD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi159:
	.cfi_def_cfa_offset 16
.Lcfi160:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV14kc_filePrinter+16, (%rbx)
	movq	24(%rbx), %rdi
	leaq	40(%rbx), %rax
	cmpq	%rax, %rdi
	je	.LBB25_2
# BB#1:
	callq	_ZdlPv
.LBB25_2:                               # %_ZN14kc_filePrinterD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end25:
	.size	_ZN14kc_filePrinterD0Ev, .Lfunc_end25-_ZN14kc_filePrinterD0Ev
	.cfi_endproc

	.section	.text._ZN2kc21printer_functor_classclEPKcRNS_11uview_classE,"axG",@progbits,_ZN2kc21printer_functor_classclEPKcRNS_11uview_classE,comdat
	.weak	_ZN2kc21printer_functor_classclEPKcRNS_11uview_classE
	.p2align	4, 0x90
	.type	_ZN2kc21printer_functor_classclEPKcRNS_11uview_classE,@function
_ZN2kc21printer_functor_classclEPKcRNS_11uview_classE: # @_ZN2kc21printer_functor_classclEPKcRNS_11uview_classE
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end26:
	.size	_ZN2kc21printer_functor_classclEPKcRNS_11uview_classE, .Lfunc_end26-_ZN2kc21printer_functor_classclEPKcRNS_11uview_classE
	.cfi_endproc

	.section	.text._ZN2kc21printer_functor_classD0Ev,"axG",@progbits,_ZN2kc21printer_functor_classD0Ev,comdat
	.weak	_ZN2kc21printer_functor_classD0Ev
	.p2align	4, 0x90
	.type	_ZN2kc21printer_functor_classD0Ev,@function
_ZN2kc21printer_functor_classD0Ev:      # @_ZN2kc21printer_functor_classD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end27:
	.size	_ZN2kc21printer_functor_classD0Ev, .Lfunc_end27-_ZN2kc21printer_functor_classD0Ev
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_error.ii,@function
_GLOBAL__sub_I_error.ii:                # @_GLOBAL__sub_I_error.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi161:
	.cfi_def_cfa_offset 16
	movq	stdout(%rip), %rax
	movq	$_ZTV14kc_filePrinter+16, v_stdout_printer(%rip)
	movq	%rax, v_stdout_printer+8(%rip)
	movq	$v_stdout_printer+40, v_stdout_printer+24(%rip)
	movq	$0, v_stdout_printer+32(%rip)
	movb	$0, v_stdout_printer+40(%rip)
	movl	$0, v_stdout_printer+16(%rip)
	movl	$0, v_stdout_printer+56(%rip)
	movb	$0, v_stdout_printer+60(%rip)
	movb	$10, v_stdout_printer+61(%rip)
	movq	$0, v_stdout_printer+80(%rip)
	movw	$0, v_stdout_printer+88(%rip)
	movq	$0, v_stdout_printer+70(%rip)
	movq	$0, v_stdout_printer+64(%rip)
	movl	$_ZN14kc_filePrinterD2Ev, %edi
	movl	$v_stdout_printer, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movq	$_ZTV14kc_filePrinter+16, v_hfile_printer(%rip)
	movq	$0, v_hfile_printer+8(%rip)
	movq	$v_hfile_printer+40, v_hfile_printer+24(%rip)
	movq	$0, v_hfile_printer+32(%rip)
	movb	$0, v_hfile_printer+40(%rip)
	movl	$0, v_hfile_printer+16(%rip)
	movl	$0, v_hfile_printer+56(%rip)
	movb	$0, v_hfile_printer+60(%rip)
	movb	$10, v_hfile_printer+61(%rip)
	movq	$0, v_hfile_printer+80(%rip)
	movw	$0, v_hfile_printer+88(%rip)
	movq	$0, v_hfile_printer+70(%rip)
	movq	$0, v_hfile_printer+64(%rip)
	movl	$_ZN14kc_filePrinterD2Ev, %edi
	movl	$v_hfile_printer, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movq	$_ZTV14kc_filePrinter+16, v_ccfile_printer(%rip)
	movq	$0, v_ccfile_printer+8(%rip)
	movq	$v_ccfile_printer+40, v_ccfile_printer+24(%rip)
	movq	$0, v_ccfile_printer+32(%rip)
	movb	$0, v_ccfile_printer+40(%rip)
	movl	$0, v_ccfile_printer+16(%rip)
	movl	$0, v_ccfile_printer+56(%rip)
	movb	$0, v_ccfile_printer+60(%rip)
	movb	$10, v_ccfile_printer+61(%rip)
	movq	$0, v_ccfile_printer+80(%rip)
	movw	$0, v_ccfile_printer+88(%rip)
	movq	$0, v_ccfile_printer+70(%rip)
	movq	$0, v_ccfile_printer+64(%rip)
	movl	$_ZN14kc_filePrinterD2Ev, %edi
	movl	$v_ccfile_printer, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movq	$_ZTVN2kc21printer_functor_classE+16, v_null_printer(%rip)
	popq	%rax
	retq
.Lfunc_end28:
	.size	_GLOBAL__sub_I_error.ii, .Lfunc_end28-_GLOBAL__sub_I_error.ii
	.cfi_endproc

	.type	gp_no_fatal_problems,@object # @gp_no_fatal_problems
	.bss
	.globl	gp_no_fatal_problems
gp_no_fatal_problems:
	.byte	0                       # 0x0
	.size	gp_no_fatal_problems, 1

	.type	ug_viewnameopt,@object  # @ug_viewnameopt
	.globl	ug_viewnameopt
	.p2align	3
ug_viewnameopt:
	.quad	0
	.size	ug_viewnameopt, 8

	.type	_ZN14kc_filePrinter12indent_levelE,@object # @_ZN14kc_filePrinter12indent_levelE
	.data
	.globl	_ZN14kc_filePrinter12indent_levelE
	.p2align	2
_ZN14kc_filePrinter12indent_levelE:
	.long	4                       # 0x4
	.size	_ZN14kc_filePrinter12indent_levelE, 4

	.type	_ZTV14kc_filePrinter,@object # @_ZTV14kc_filePrinter
	.section	.rodata,"a",@progbits
	.globl	_ZTV14kc_filePrinter
	.p2align	3
_ZTV14kc_filePrinter:
	.quad	0
	.quad	_ZTI14kc_filePrinter
	.quad	_ZN14kc_filePrinterclEPKcRN2kc11uview_classE
	.quad	_ZN14kc_filePrinterD2Ev
	.quad	_ZN14kc_filePrinterD0Ev
	.size	_ZTV14kc_filePrinter, 40

	.type	v_stdout_printer,@object # @v_stdout_printer
	.bss
	.globl	v_stdout_printer
	.p2align	3
v_stdout_printer:
	.zero	96
	.size	v_stdout_printer, 96

	.type	v_hfile_printer,@object # @v_hfile_printer
	.globl	v_hfile_printer
	.p2align	3
v_hfile_printer:
	.zero	96
	.size	v_hfile_printer, 96

	.type	v_ccfile_printer,@object # @v_ccfile_printer
	.globl	v_ccfile_printer
	.p2align	3
v_ccfile_printer:
	.zero	96
	.size	v_ccfile_printer, 96

	.type	v_null_printer,@object  # @v_null_printer
	.globl	v_null_printer
	.p2align	3
v_null_printer:
	.zero	8
	.size	v_null_printer, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"if"
	.size	.L.str, 3

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"else"
	.size	.L.str.4, 5

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"do"
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"while"
	.size	.L.str.6, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%s %d \"%s%s\"\n"
	.size	.L.str.8, 14

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"\"), "
	.size	.L.str.9, 5

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	" );\n\t\t  kc_printer(kc_t(\""
	.size	.L.str.10, 26

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"cannot create temporary "
	.size	.L.str.11, 25

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	" file:"
	.size	.L.str.12, 7

	.type	_ZTS14kc_filePrinter,@object # @_ZTS14kc_filePrinter
	.section	.rodata,"a",@progbits
	.globl	_ZTS14kc_filePrinter
	.p2align	4
_ZTS14kc_filePrinter:
	.asciz	"14kc_filePrinter"
	.size	_ZTS14kc_filePrinter, 17

	.type	_ZTSN2kc21printer_functor_classE,@object # @_ZTSN2kc21printer_functor_classE
	.section	.rodata._ZTSN2kc21printer_functor_classE,"aG",@progbits,_ZTSN2kc21printer_functor_classE,comdat
	.weak	_ZTSN2kc21printer_functor_classE
	.p2align	4
_ZTSN2kc21printer_functor_classE:
	.asciz	"N2kc21printer_functor_classE"
	.size	_ZTSN2kc21printer_functor_classE, 29

	.type	_ZTIN2kc21printer_functor_classE,@object # @_ZTIN2kc21printer_functor_classE
	.section	.rodata._ZTIN2kc21printer_functor_classE,"aG",@progbits,_ZTIN2kc21printer_functor_classE,comdat
	.weak	_ZTIN2kc21printer_functor_classE
	.p2align	3
_ZTIN2kc21printer_functor_classE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN2kc21printer_functor_classE
	.size	_ZTIN2kc21printer_functor_classE, 16

	.type	_ZTI14kc_filePrinter,@object # @_ZTI14kc_filePrinter
	.section	.rodata,"a",@progbits
	.globl	_ZTI14kc_filePrinter
	.p2align	4
_ZTI14kc_filePrinter:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14kc_filePrinter
	.quad	_ZTIN2kc21printer_functor_classE
	.size	_ZTI14kc_filePrinter, 24

	.type	_ZTVN2kc21printer_functor_classE,@object # @_ZTVN2kc21printer_functor_classE
	.section	.rodata._ZTVN2kc21printer_functor_classE,"aG",@progbits,_ZTVN2kc21printer_functor_classE,comdat
	.weak	_ZTVN2kc21printer_functor_classE
	.p2align	3
_ZTVN2kc21printer_functor_classE:
	.quad	0
	.quad	_ZTIN2kc21printer_functor_classE
	.quad	_ZN2kc21printer_functor_classclEPKcRNS_11uview_classE
	.quad	_ZN2kc21printer_functor_classD2Ev
	.quad	_ZN2kc21printer_functor_classD0Ev
	.size	_ZTVN2kc21printer_functor_classE, 40

	.type	.L.str.13,@object       # @.str.13
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.13:
	.asciz	"basic_string::_M_construct null not valid"
	.size	.L.str.13, 42

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_error.ii

	.globl	_ZN14kc_filePrinterC1EP8_IO_FILE
	.type	_ZN14kc_filePrinterC1EP8_IO_FILE,@function
_ZN14kc_filePrinterC1EP8_IO_FILE = _ZN14kc_filePrinterC2EP8_IO_FILE
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
