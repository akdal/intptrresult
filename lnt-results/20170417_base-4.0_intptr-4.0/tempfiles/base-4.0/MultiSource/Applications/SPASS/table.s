	.text
	.file	"table.bc"
	.globl	table_Null
	.p2align	4, 0x90
	.type	table_Null,@function
table_Null:                             # @table_Null
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	table_Null, .Lfunc_end0-table_Null
	.cfi_endproc

	.globl	table_Create
	.p2align	4, 0x90
	.type	table_Create,@function
table_Create:                           # @table_Create
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %r12d
	movl	%edi, %r14d
	movl	$40, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	leal	1(%r14,%r12), %edi
	movl	$24, %esi
	callq	memory_Calloc
	movslq	%r12d, %rbp
	leaq	(%rbp,%rbp,2), %rcx
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, (%rbx)
	leal	1(%r15), %r12d
	leal	8(,%r15,8), %edi
	callq	memory_Malloc
	movq	%rax, 8(%rbx)
	movl	$4, %esi
	movl	%r12d, %edi
	callq	memory_Calloc
	movq	%rax, 16(%rbx)
	movl	%r14d, 28(%rbx)
	movl	%ebp, 32(%rbx)
	movl	%r15d, 36(%rbx)
	movl	$1, 24(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	table_Create, .Lfunc_end1-table_Create
	.cfi_endproc

	.globl	table_Free
	.p2align	4, 0x90
	.type	table_Free,@function
table_Free:                             # @table_Free
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB2_27
# BB#1:
	movslq	32(%r14), %rdx
	movq	%rdx, %rsi
	negq	%rsi
	movl	28(%r14), %ecx
	cmpl	%esi, %ecx
	movq	(%r14), %rax
	jl	.LBB2_5
# BB#2:                                 # %.lr.ph
	leaq	(%rsi,%rsi,2), %rcx
	leaq	-1(%rsi), %r15
	leaq	16(,%rcx,8), %rbx
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rbx), %rdi
	movl	36(%r14), %esi
	incl	%esi
	callq	table_FreeTermarray
	movslq	28(%r14), %rcx
	movq	(%r14), %rax
	incq	%r15
	addq	$24, %rbx
	cmpq	%rcx, %r15
	jl	.LBB2_3
# BB#4:                                 # %._crit_edge.loopexit
	movl	32(%r14), %edx
.LBB2_5:                                # %._crit_edge
	movslq	%edx, %rdx
	movq	%rdx, %rsi
	negq	%rsi
	leaq	(%rsi,%rsi,2), %rsi
	leaq	(%rax,%rsi,8), %rdi
	addl	%ecx, %edx
	leal	(%rdx,%rdx,2), %eax
	leal	24(,%rax,8), %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB2_6
# BB#11:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB2_12
.LBB2_6:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rdx, %rbx
	movq	%r8, (%rbx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB2_8
# BB#7:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB2_8:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB2_10
# BB#9:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB2_10:
	addq	$-16, %rdi
	callq	free
.LBB2_12:                               # %memory_Free.exit34
	movq	8(%r14), %rdi
	movl	36(%r14), %eax
	leal	8(,%rax,8), %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB2_13
# BB#18:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB2_19
.LBB2_13:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rdx, %rbx
	movq	%r8, (%rbx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB2_15
# BB#14:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB2_15:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB2_17
# BB#16:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB2_17:
	addq	$-16, %rdi
	callq	free
.LBB2_19:                               # %memory_Free.exit30
	movq	16(%r14), %rdi
	movl	36(%r14), %eax
	leal	4(,%rax,4), %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB2_20
# BB#25:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB2_26
.LBB2_20:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rdx, %rbx
	movq	%r8, (%rbx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB2_22
# BB#21:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB2_22:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB2_24
# BB#23:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB2_24:
	addq	$-16, %rdi
	callq	free
.LBB2_26:                               # %memory_Free.exit
	movq	memory_ARRAY+320(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+320(%rip), %rax
	movq	%r14, (%rax)
.LBB2_27:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	table_Free, .Lfunc_end2-table_Free
	.cfi_endproc

	.p2align	4, 0x90
	.type	table_FreeTermarray,@function
table_FreeTermarray:                    # @table_FreeTermarray
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 48
.Lcfi21:
	.cfi_offset %rbx, -40
.Lcfi22:
	.cfi_offset %r12, -32
.Lcfi23:
	.cfi_offset %r14, -24
.Lcfi24:
	.cfi_offset %r15, -16
	movl	%esi, %r15d
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB3_11
# BB#1:                                 # %.preheader
	testl	%r15d, %r15d
	jle	.LBB3_4
# BB#2:                                 # %.lr.ph.preheader
	movl	%r15d, %r12d
	leaq	16(%r14), %rbx
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movl	%r15d, %esi
	callq	table_FreeTermarray
	addq	$24, %rbx
	decq	%r12
	jne	.LBB3_3
.LBB3_4:                                # %._crit_edge
	shll	$3, %r15d
	leal	(%r15,%r15,2), %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB3_5
# BB#10:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%r14, (%rax)
.LBB3_11:                               # %memory_Free.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB3_5:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%r14, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %rdi
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rdx, %rbx
	movq	%rdi, (%rbx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB3_7
# BB#6:
	negq	%rax
	movq	-16(%r14,%rax), %rax
	movq	%rax, (%rcx)
.LBB3_7:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB3_9
# BB#8:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB3_9:
	addq	$-16, %r14
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.Lfunc_end3:
	.size	table_FreeTermarray, .Lfunc_end3-table_FreeTermarray
	.cfi_endproc

	.globl	table_Init
	.p2align	4, 0x90
	.type	table_Init,@function
table_Init:                             # @table_Init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 64
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movslq	28(%r14), %rax
	cmpl	%esi, %eax
	movl	%esi, %r12d
	cmovgel	%eax, %r12d
	movl	24(%r14), %edi
	movl	32(%r14), %r8d
	cmpl	%edx, %r8d
	movl	%edx, %r15d
	cmovgel	%r8d, %r15d
	movl	36(%r14), %ebp
	cmpl	%ecx, %ebp
	movl	%ecx, %r13d
	cmovgel	%ebp, %r13d
	leal	1(%rdi), %ebx
	movl	%ebx, 24(%r14)
	testl	%edi, %edi
	js	.LBB4_2
# BB#1:
	cmpl	%ecx, %ebp
	jl	.LBB4_2
# BB#3:
	addl	%esi, %edx
	leal	(%r8,%rax), %ecx
	movq	(%r14), %rbp
	cmpl	%ecx, %edx
	jle	.LBB4_17
# BB#4:
	leal	1(%r12,%r15), %edi
	movl	$24, %esi
	callq	memory_Calloc
	movslq	%r15d, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,8), %rdx
	movq	%rdx, (%r14)
	movslq	32(%r14), %rsi
	movq	%rsi, %rax
	negq	%rax
	movl	28(%r14), %ecx
	cmpl	%eax, %ecx
	jl	.LBB4_9
# BB#5:                                 # %.lr.ph.preheader
	leaq	(,%rax,8), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movq	16(%rbp,%rcx), %rsi
	movq	%rsi, 16(%rdx,%rcx)
	movl	28(%r14), %ecx
	cmpl	%eax, %ecx
	jle	.LBB4_8
# BB#6:                                 # %.lr.ph..lr.ph_crit_edge.preheader
	leaq	(%rax,%rax,2), %rcx
	leaq	40(,%rcx,8), %rdx
	.p2align	4, 0x90
.LBB4_7:                                # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	incq	%rax
	movq	(%r14), %rcx
	movq	(%rbp,%rdx), %rsi
	movq	%rsi, (%rcx,%rdx)
	movslq	28(%r14), %rcx
	addq	$24, %rdx
	cmpq	%rcx, %rax
	jl	.LBB4_7
.LBB4_8:                                # %._crit_edge.loopexit
	movl	32(%r14), %esi
.LBB4_9:                                # %._crit_edge
	movslq	%esi, %rax
	movq	%rax, %rdx
	negq	%rdx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rbp,%rdx,8), %rdi
	addl	%ecx, %eax
	leal	(%rax,%rax,2), %eax
	leal	24(,%rax,8), %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB4_10
# BB#15:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB4_16
.LBB4_2:
	movq	%r14, %rdi
	callq	table_Free
	movl	$40, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	leal	1(%r12,%r15), %edi
	movl	$24, %esi
	callq	memory_Calloc
	movslq	%r15d, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, (%r14)
	movq	%r12, %rbx
	leal	1(%r13), %r12d
	leal	8(,%r13,8), %edi
	callq	memory_Malloc
	movq	%rax, 8(%r14)
	movl	$4, %esi
	movl	%r12d, %edi
	callq	memory_Calloc
	movq	%rax, 16(%r14)
	movl	%ebx, 28(%r14)
	movl	%r15d, 32(%r14)
	movl	%r13d, 36(%r14)
	movl	$1, 24(%r14)
	jmp	.LBB4_18
.LBB4_17:
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rbp,%rcx,8), %rcx
	movslq	%esi, %rdx
	negq	%rdx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, (%r14)
	subl	%esi, %eax
	addl	%r8d, %eax
	movl	%eax, 32(%r14)
	movl	%esi, 28(%r14)
	jmp	.LBB4_18
.LBB4_10:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %rbp
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rdx, %rbx
	movq	%rbp, (%rbx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB4_12
# BB#11:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB4_12:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB4_14
# BB#13:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB4_14:
	addq	$-16, %rdi
	callq	free
.LBB4_16:                               # %memory_Free.exit
	movl	%r12d, 28(%r14)
	movl	%r15d, 32(%r14)
.LBB4_18:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	table_Init, .Lfunc_end4-table_Init
	.cfi_endproc

	.globl	table_QueryAndEnter
	.p2align	4, 0x90
	.type	table_QueryAndEnter,@function
table_QueryAndEnter:                    # @table_QueryAndEnter
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 64
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %r15
	movl	(%r14), %ecx
	movl	%ecx, %eax
	negl	%eax
	testl	%ecx, %ecx
	jle	.LBB5_1
# BB#2:
	xorl	%ecx, %ecx
	jmp	.LBB5_3
.LBB5_1:
	movb	symbol_TYPESTATBITS(%rip), %cl
.LBB5_3:
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	cltq
	leaq	(%rax,%rax,2), %r13
	shlq	$3, %r13
	addq	(%r15), %r13
	movq	16(%r14), %rbp
	testq	%rbp, %rbp
	jne	.LBB5_5
	jmp	.LBB5_8
	.p2align	4, 0x90
.LBB5_7:                                #   in Loop: Header=BB5_5 Depth=1
	movq	8(%rbp), %rax
	movl	28(%rax), %esi
	movq	%r12, %rdi
	callq	part_Find
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %r13
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB5_8
.LBB5_5:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	jne	.LBB5_7
# BB#6:                                 #   in Loop: Header=BB5_5 Depth=1
	movl	36(%r15), %edi
	incl	%edi
	movl	$24, %esi
	callq	memory_Calloc
	movq	%rax, %rbx
	movq	%rbx, 16(%r13)
	jmp	.LBB5_7
.LBB5_8:                                # %._crit_edge
	movl	8(%r13), %eax
	cmpl	24(%r15), %eax
	jne	.LBB5_9
# BB#10:                                # %table_DelayedInit.exit
	movq	(%r13), %rax
	testq	%rax, %rax
	jne	.LBB5_14
	jmp	.LBB5_11
.LBB5_9:                                # %table_DelayedInit.exit.thread
	movq	$0, (%r13)
	movl	24(%r15), %eax
	movl	%eax, 8(%r13)
.LBB5_11:
	movq	%r14, (%r13)
	movq	16(%r15), %rcx
	movslq	28(%r14), %rax
	movl	(%rcx,%rax,4), %ecx
	cmpl	24(%r15), %ecx
	je	.LBB5_13
# BB#12:
	movq	8(%r15), %rcx
	movq	$0, (%rcx,%rax,8)
	movl	24(%r15), %ecx
	movq	16(%r15), %rdx
	movl	%ecx, (%rdx,%rax,4)
.LBB5_13:                               # %table_DelayedPosInit.exit
	movq	8(%r15), %rcx
	movq	%r13, (%rcx,%rax,8)
	xorl	%eax, %eax
.LBB5_14:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	table_QueryAndEnter, .Lfunc_end5-table_QueryAndEnter
	.cfi_endproc

	.globl	table_Delete
	.p2align	4, 0x90
	.type	table_Delete,@function
table_Delete:                           # @table_Delete
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rcx
	movslq	28(%rsi), %rax
	movl	(%rcx,%rax,4), %ecx
	cmpl	24(%rdi), %ecx
	je	.LBB6_2
# BB#1:
	movq	8(%rdi), %rcx
	movq	$0, (%rcx,%rax,8)
	movl	24(%rdi), %ecx
	movq	16(%rdi), %rdx
	movl	%ecx, (%rdx,%rax,4)
.LBB6_2:                                # %table_DelayedPosInit.exit
	movq	8(%rdi), %rcx
	movq	(%rcx,%rax,8), %rcx
	testq	%rcx, %rcx
	je	.LBB6_4
# BB#3:
	movq	$0, (%rcx)
	movq	8(%rdi), %rcx
	movq	$0, (%rcx,%rax,8)
.LBB6_4:
	movq	%rdi, %rax
	retq
.Lfunc_end6:
	.size	table_Delete, .Lfunc_end6-table_Delete
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
