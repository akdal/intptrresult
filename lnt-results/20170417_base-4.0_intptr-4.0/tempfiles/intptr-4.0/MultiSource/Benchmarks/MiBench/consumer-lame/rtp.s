	.text
	.file	"rtp.bc"
	.globl	initrtp
	.p2align	4, 0x90
	.type	initrtp,@function
initrtp:                                # @initrtp
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movw	$-32754, 2(%rbx)        # imm = 0x800E
	callq	rand
	movw	%ax, (%rbx)
	callq	rand
	movl	%eax, 4(%rbx)
	callq	rand
	movl	%eax, 8(%rbx)
	movl	$0, 12(%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	initrtp, .Lfunc_end0-initrtp
	.cfi_endproc

	.globl	sendrtp
	.p2align	4, 0x90
	.type	sendrtp,@function
sendrtp:                                # @sendrtp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Lcfi4:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r12
	pushq	%rbx
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
	movq	%rsi, %r14
	movl	%edi, %r15d
	movslq	%r8d, %rax
	movq	%rsp, %r8
	leaq	31(%rax), %rdi
	andq	$-16, %rdi
	movq	%r8, %rbx
	subq	%rdi, %rbx
	movq	%rbx, %rsp
	negq	%rdi
	movl	(%rdx), %esi
	bswapl	%esi
	movl	%esi, (%rbx)
	movl	4(%rdx), %esi
	bswapl	%esi
	movl	%esi, 4(%rbx)
	movl	8(%rdx), %esi
	bswapl	%esi
	movl	%esi, 8(%rbx)
	movl	12(%rdx), %edx
	bswapl	%edx
	movl	%edx, 12(%r8,%rdi)
	leaq	16(%rbx), %rdi
	leaq	16(%rax), %r12
	movq	%rcx, %rsi
	movq	%rax, %rdx
	callq	memcpy
	xorl	%ecx, %ecx
	movl	$16, %r9d
	movl	%r15d, %edi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	movq	%r14, %r8
	callq	sendto
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	sendrtp, .Lfunc_end1-sendrtp
	.cfi_endproc

	.globl	makesocket
	.p2align	4, 0x90
	.type	makesocket,@function
makesocket:                             # @makesocket
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	$1, 4(%rsp)
	movb	%dl, 3(%rsp)
	movb	$0, 2(%rsp)
	movl	$2, %edi
	movl	$2, %esi
	xorl	%edx, %edx
	callq	socket
	movl	%eax, %r14d
	testl	%r14d, %r14d
	js	.LBB2_1
# BB#3:
	movq	%rbx, %rdi
	callq	inet_addr
	movl	%eax, %ebx
	movw	$2, (%r15)
	rolw	$8, %bp
	movw	%bp, 2(%r15)
	movl	%ebx, 4(%r15)
	leaq	4(%rsp), %rcx
	movl	$1, %esi
	movl	$2, %edx
	movl	$4, %r8d
	movl	%r14d, %edi
	callq	setsockopt
	testl	%eax, %eax
	js	.LBB2_4
# BB#5:
	bswapl	%ebx
	andl	$-268435456, %ebx       # imm = 0xF0000000
	cmpl	$-536870912, %ebx       # imm = 0xE0000000
	jne	.LBB2_10
# BB#6:
	leaq	3(%rsp), %rcx
	xorl	%esi, %esi
	movl	$33, %edx
	movl	$1, %r8d
	movl	%r14d, %edi
	callq	setsockopt
	testl	%eax, %eax
	js	.LBB2_7
# BB#8:
	movb	$1, 2(%rsp)
	leaq	2(%rsp), %rcx
	xorl	%esi, %esi
	movl	$34, %edx
	movl	$1, %r8d
	movl	%r14d, %edi
	callq	setsockopt
	testl	%eax, %eax
	js	.LBB2_9
.LBB2_10:
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_1:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$17, %esi
	jmp	.LBB2_2
.LBB2_4:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$31, %esi
	jmp	.LBB2_2
.LBB2_7:
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$58, %esi
	jmp	.LBB2_2
.LBB2_9:
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$59, %esi
.LBB2_2:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end2:
	.size	makesocket, .Lfunc_end2-makesocket
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"socket() failed.\n"
	.size	.L.str, 18

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"setsockopt SO_REUSEADDR failed\n"
	.size	.L.str.1, 32

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"setsockopt IP_MULTICAST_TTL failed.  multicast in kernel?\n"
	.size	.L.str.2, 59

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"setsockopt IP_MULTICAST_LOOP failed.  multicast in kernel?\n"
	.size	.L.str.3, 60


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
