	.text
	.file	"main.bc"
	.globl	rand
	.p2align	4, 0x90
	.type	rand,@function
rand:                                   # @rand
	.cfi_startproc
# BB#0:
	imulq	$1103515245, next(%rip), %rax # imm = 0x41C64E6D
	addq	$12345, %rax            # imm = 0x3039
	movq	%rax, next(%rip)
	shrq	$16, %rax
	movl	%eax, %ecx
	imulq	$131077, %rcx, %rcx     # imm = 0x20005
	shrq	$32, %rcx
	movl	%eax, %edx
	subl	%ecx, %edx
	shrl	%edx
	addl	%ecx, %edx
	shrl	$14, %edx
	movl	%edx, %ecx
	shll	$15, %ecx
	subl	%edx, %ecx
	negl	%ecx
	leal	1(%rax,%rcx), %eax
	retq
.Lfunc_end0:
	.size	rand, .Lfunc_end0-rand
	.cfi_endproc

	.globl	srand
	.p2align	4, 0x90
	.type	srand,@function
srand:                                  # @srand
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	movq	%rax, next(%rip)
	retq
.Lfunc_end1:
	.size	srand, .Lfunc_end1-srand
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_1:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 160
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	cmpl	$2, %edi
	jle	.LBB2_30
# BB#1:
	xorl	%ecx, %ecx
	cmpl	$4, %edi
	jne	.LBB2_3
# BB#2:
	movq	24(%rbx), %rdi
	movl	$.L.str.4, %esi
	movl	$2, %edx
	callq	strncmp
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
.LBB2_3:
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rbp
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r12
	movq	$1, next(%rip)
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	leaq	(,%rbp,4), %rbp
	movabsq	$17179869180, %rax      # imm = 0x3FFFFFFFC
	andq	%rax, %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, %r14
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, %r13
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r12, %r15
	shlq	$2, %r15
	movabsq	$17179869180, %rax      # imm = 0x3FFFFFFFC
	andq	%rax, %r15
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, %rbp
	movq	%r15, %rdi
	callq	malloc
	testl	%r12d, %r12d
	je	.LBB2_8
# BB#4:                                 # %.lr.ph138.preheader
	movl	%r12d, %edi
	testb	$1, %r12b
	jne	.LBB2_6
# BB#5:
	xorl	%ecx, %ecx
	cmpq	$1, %rdi
	jne	.LBB2_17
	jmp	.LBB2_8
.LBB2_6:                                # %.lr.ph138.prol
	movl	$0, (%rbp)
	movl	$0, (%rax)
	movl	$1, %ecx
	cmpq	$1, %rdi
	je	.LBB2_8
	.p2align	4, 0x90
.LBB2_17:                               # %.lr.ph138
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	imulq	$274877907, %rdx, %rdx  # imm = 0x10624DD3
	shrq	$38, %rdx
	imull	$1000, %edx, %edx       # imm = 0x3E8
	movl	%ecx, %esi
	subl	%edx, %esi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%esi, %xmm0
	movss	%xmm0, (%rbp,%rcx,4)
	movss	%xmm0, (%rax,%rcx,4)
	leaq	1(%rcx), %rdx
	movl	%edx, %esi
	imulq	$274877907, %rsi, %rsi  # imm = 0x10624DD3
	shrq	$38, %rsi
	imull	$1000, %esi, %esi       # imm = 0x3E8
	subl	%esi, %edx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	movss	%xmm0, 4(%rbp,%rcx,4)
	movss	%xmm0, 4(%rax,%rcx,4)
	addq	$2, %rcx
	cmpq	%rdi, %rcx
	jne	.LBB2_17
.LBB2_8:                                # %.preheader
	movq	32(%rsp), %r15          # 8-byte Reload
	testl	%r15d, %r15d
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	%rax, 56(%rsp)          # 8-byte Spill
	je	.LBB2_23
# BB#9:                                 # %.lr.ph134
	movq	%r13, 72(%rsp)          # 8-byte Spill
	testl	%r12d, %r12d
	je	.LBB2_18
# BB#10:                                # %.lr.ph134.split.us.preheader
	movl	%r12d, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	%r15d, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB2_11:                               # %.lr.ph134.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_12 Depth 2
	movl	$0, (%rbx,%r15,4)
	movl	%r15d, %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	movss	%xmm0, 44(%rsp)         # 4-byte Spill
	movq	88(%rsp), %r12          # 8-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_12:                               #   Parent Loop BB2_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imulq	$1103515245, next(%rip), %rax # imm = 0x41C64E6D
	addq	$12345, %rax            # imm = 0x3039
	movq	%rax, next(%rip)
	shrq	$16, %rax
	movl	%eax, %ecx
	imulq	$131077, %rcx, %rcx     # imm = 0x20005
	shrq	$32, %rcx
	movl	%eax, %edx
	subl	%ecx, %edx
	shrl	%edx
	addl	%ecx, %edx
	shrl	$14, %edx
	negl	%edx
	subl	%edx, %eax
	testb	$1, %al
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 96(%rsp)         # 8-byte Spill
	movss	(%r13), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	44(%rsp), %xmm0         # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	jne	.LBB2_13
# BB#14:                                #   in Loop: Header=BB2_12 Depth=2
	callq	cos
	jmp	.LBB2_15
	.p2align	4, 0x90
.LBB2_13:                               #   in Loop: Header=BB2_12 Depth=2
	callq	sin
.LBB2_15:                               #   in Loop: Header=BB2_12 Depth=2
	movsd	96(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movss	(%rbx,%r15,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, (%rbx,%r15,4)
	movl	$0, (%r14,%r15,4)
	addq	$4, %rbp
	addq	$4, %r13
	decq	%r12
	jne	.LBB2_12
# BB#16:                                # %._crit_edge132.us
                                        #   in Loop: Header=BB2_11 Depth=1
	incq	%r15
	cmpq	80(%rsp), %r15          # 8-byte Folded Reload
	jne	.LBB2_11
	jmp	.LBB2_19
.LBB2_23:                               # %._crit_edge128.critedge
	movl	%r15d, %edi
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %esi
	movq	%rbx, %rdx
	movq	%r14, %rcx
	movq	%r13, %r8
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	%r12, %r9
	callq	fft_float
	movl	%r15d, %edi
	movl	%ebp, %esi
	movq	%rbx, %rdx
	movq	%r14, %rcx
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	callq	fft_float_StrictFP
	movl	$.Lstr, %edi
	callq	puts
	jmp	.LBB2_24
.LBB2_18:                               # %.lr.ph134.split.preheader
	movabsq	$17179869180, %rax      # imm = 0x3FFFFFFFC
	leaq	(%rax,%r15,4), %rdx
	andq	%rax, %rdx
	addq	$4, %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	memset
.LBB2_19:                               # %._crit_edge135
	movq	32(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %edi
	movl	12(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %esi
	movq	%rbx, %rdx
	movq	%r14, %rcx
	movq	72(%rsp), %r13          # 8-byte Reload
	movq	%r13, %r8
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	%r12, %r9
	callq	fft_float
	movl	%ebp, %edi
	movl	%r15d, %esi
	movq	%rbx, %rdx
	movq	%r14, %rcx
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	callq	fft_float_StrictFP
	movl	$.Lstr, %edi
	callq	puts
	testl	%ebp, %ebp
	je	.LBB2_28
# BB#20:                                # %.lr.ph127.preheader
	movl	%ebp, %r15d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_21:                               # %.lr.ph127
                                        # =>This Inner Loop Header: Depth=1
	movss	(%r13,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	16(%rsp), %rax          # 8-byte Reload
	movss	(%rax,%rbp,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	subss	%xmm1, %xmm2
	andps	.LCPI2_0(%rip), %xmm2
	cvtss2sd	%xmm2, %xmm2
	ucomisd	.LCPI2_1(%rip), %xmm2
	ja	.LBB2_31
# BB#22:                                # %check_FP.exit120
                                        #   in Loop: Header=BB2_21 Depth=1
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	movl	$.L.str.6, %edi
	movb	$1, %al
	callq	printf
	incq	%rbp
	cmpq	%r15, %rbp
	jb	.LBB2_21
.LBB2_24:                               # %._crit_edge128
	movl	$10, %edi
	callq	putchar
	movl	$.Lstr.1, %edi
	callq	puts
	movq	32(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	je	.LBB2_29
# BB#25:                                # %.lr.ph.preheader
	movl	%eax, %r15d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_26:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%r12,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	24(%rsp), %rax          # 8-byte Reload
	movss	(%rax,%rbp,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	subss	%xmm1, %xmm2
	andps	.LCPI2_0(%rip), %xmm2
	cvtss2sd	%xmm2, %xmm2
	ucomisd	.LCPI2_1(%rip), %xmm2
	ja	.LBB2_31
# BB#27:                                # %check_FP.exit
                                        #   in Loop: Header=BB2_26 Depth=1
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	movl	$.L.str.6, %edi
	movb	$1, %al
	callq	printf
	incq	%rbp
	cmpq	%r15, %rbp
	jb	.LBB2_26
.LBB2_29:                               # %._crit_edge
	movl	$10, %edi
	callq	putchar
	movq	%rbx, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	xorl	%edi, %edi
	callq	exit
.LBB2_31:                               # %check_FP.exit120.thread
	movq	stderr(%rip), %rdi
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movsd	.LCPI2_1(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.9, %esi
	movb	$3, %al
	callq	fprintf
	movl	$1, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_30:
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$-1, %edi
	callq	exit
.LBB2_28:                               # %._crit_edge.critedge
	movl	$10, %edi
	callq	putchar
	movl	$.Lstr.1, %edi
	callq	puts
	jmp	.LBB2_29
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc

	.type	next,@object            # @next
	.data
	.p2align	3
next:
	.quad	1                       # 0x1
	.size	next, 8

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"make <waves> random sinusoids"
	.size	.L.str.2, 30

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"-i"
	.size	.L.str.4, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%f \t"
	.size	.L.str.6, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"A = %lf and B = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.9, 60

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"RealOut:"
	.size	.Lstr, 9

	.type	.Lstr.1,@object         # @str.1
.Lstr.1:
	.asciz	"ImagOut:"
	.size	.Lstr.1, 9

	.type	.Lstr.2,@object         # @str.2
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.2:
	.asciz	"Usage: fft <waves> <length> -i"
	.size	.Lstr.2, 31

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"-i performs an inverse fft"
	.size	.Lstr.3, 27

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"<length> is the number of samples"
	.size	.Lstr.4, 34


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
