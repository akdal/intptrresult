	.text
	.file	"btSimpleBroadphase.bc"
	.globl	_ZN18btSimpleBroadphase8validateEv
	.p2align	4, 0x90
	.type	_ZN18btSimpleBroadphase8validateEv,@function
_ZN18btSimpleBroadphase8validateEv:     # @_ZN18btSimpleBroadphase8validateEv
	.cfi_startproc
# BB#0:                                 # %._crit_edge
	retq
.Lfunc_end0:
	.size	_ZN18btSimpleBroadphase8validateEv, .Lfunc_end0-_ZN18btSimpleBroadphase8validateEv
	.cfi_endproc

	.globl	_ZN18btSimpleBroadphaseC2EiP22btOverlappingPairCache
	.p2align	4, 0x90
	.type	_ZN18btSimpleBroadphaseC2EiP22btOverlappingPairCache,@function
_ZN18btSimpleBroadphaseC2EiP22btOverlappingPairCache: # @_ZN18btSimpleBroadphaseC2EiP22btOverlappingPairCache
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movq	$_ZTV18btSimpleBroadphase+16, (%rbx)
	movq	%rdx, 48(%rbx)
	movb	$0, 56(%rbx)
	movl	$0, 60(%rbx)
	testq	%rdx, %rdx
	jne	.LBB1_2
# BB#1:
	movl	$128, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	_ZN28btHashedOverlappingPairCacheC1Ev
	movq	%rbp, 48(%rbx)
	movb	$1, 56(%rbx)
.LBB1_2:
	movslq	%r15d, %rbp
	movq	%rbp, %r14
	shlq	$6, %r14
	movl	$16, %esi
	movq	%r14, %rdi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, 32(%rbx)
	testl	%ebp, %ebp
	je	.LBB1_10
# BB#3:
	leaq	-64(%r14), %rdx
	movl	%edx, %esi
	shrl	$6, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB1_4
# BB#5:                                 # %.prol.preheader
	negq	%rsi
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB1_6:                                # =>This Inner Loop Header: Depth=1
	movq	$0, (%rcx)
	movq	$0, 16(%rcx)
	addq	$64, %rcx
	incq	%rsi
	jne	.LBB1_6
	jmp	.LBB1_7
.LBB1_4:
	movq	%rax, %rcx
.LBB1_7:                                # %.prol.loopexit
	cmpq	$448, %rdx              # imm = 0x1C0
	jb	.LBB1_10
# BB#8:                                 # %.new
	leaq	(%rax,%r14), %rdx
	.p2align	4, 0x90
.LBB1_9:                                # =>This Inner Loop Header: Depth=1
	movq	$0, (%rcx)
	movq	$0, 16(%rcx)
	movq	$0, 64(%rcx)
	movq	$0, 80(%rcx)
	movq	$0, 128(%rcx)
	movq	$0, 144(%rcx)
	movq	$0, 192(%rcx)
	movq	$0, 208(%rcx)
	movq	$0, 256(%rcx)
	movq	$0, 272(%rcx)
	movq	$0, 320(%rcx)
	movq	$0, 336(%rcx)
	movq	$0, 384(%rcx)
	movq	$0, 400(%rcx)
	movq	$0, 448(%rcx)
	movq	$0, 464(%rcx)
	addq	$512, %rcx              # imm = 0x200
	cmpq	%rdx, %rcx
	jne	.LBB1_9
.LBB1_10:                               # %.loopexit
	movq	%rax, 24(%rbx)
	movl	%r15d, 12(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, 40(%rbx)
	movl	$-1, 16(%rbx)
	testl	%r15d, %r15d
	jle	.LBB1_18
# BB#11:                                # %.lr.ph.preheader
	movl	%r15d, %ecx
	leaq	-1(%rcx), %rdx
	movq	%rcx, %rdi
	andq	$3, %rdi
	je	.LBB1_12
# BB#13:                                # %.lr.ph.prol.preheader
	leaq	60(%rax), %rbp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_14:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%rsi), %rbx
	movl	%ebx, (%rbp)
	addl	$2, %esi
	movl	%esi, -36(%rbp)
	addq	$64, %rbp
	cmpq	%rbx, %rdi
	movq	%rbx, %rsi
	jne	.LBB1_14
	jmp	.LBB1_15
.LBB1_12:
	xorl	%ebx, %ebx
.LBB1_15:                               # %.lr.ph.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB1_18
# BB#16:                                # %.lr.ph.preheader.new
	negq	%rcx
	leaq	2(%rbx,%rcx), %rcx
	leaq	2(%rbx), %rdx
	shlq	$6, %rbx
	leaq	252(%rax,%rbx), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_17:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rdi), %ebp
	leal	-1(%rdx,%rdi), %ebx
	movl	%ebx, -192(%rsi)
	movl	%ebp, -228(%rsi)
	movl	%ebp, -128(%rsi)
	leal	1(%rdx,%rdi), %ebp
	movl	%ebp, -164(%rsi)
	movl	%ebp, -64(%rsi)
	leal	2(%rdx,%rdi), %ebp
	movl	%ebp, -100(%rsi)
	movl	%ebp, (%rsi)
	leal	3(%rdx,%rdi), %ebp
	movl	%ebp, -36(%rsi)
	leaq	4(%rcx,%rdi), %rbp
	addq	$4, %rdi
	addq	$256, %rsi              # imm = 0x100
	cmpq	$2, %rbp
	jne	.LBB1_17
.LBB1_18:                               # %._crit_edge
	movl	$0, -4(%rax,%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN18btSimpleBroadphaseC2EiP22btOverlappingPairCache, .Lfunc_end1-_ZN18btSimpleBroadphaseC2EiP22btOverlappingPairCache
	.cfi_endproc

	.globl	_ZN18btSimpleBroadphaseD2Ev
	.p2align	4, 0x90
	.type	_ZN18btSimpleBroadphaseD2Ev,@function
_ZN18btSimpleBroadphaseD2Ev:            # @_ZN18btSimpleBroadphaseD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV18btSimpleBroadphase+16, (%rbx)
	movq	32(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	cmpb	$0, 56(%rbx)
	je	.LBB2_1
# BB#2:
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	48(%rbx), %rdi
	popq	%rbx
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB2_1:
	popq	%rbx
	retq
.Lfunc_end2:
	.size	_ZN18btSimpleBroadphaseD2Ev, .Lfunc_end2-_ZN18btSimpleBroadphaseD2Ev
	.cfi_endproc

	.globl	_ZN18btSimpleBroadphaseD0Ev
	.p2align	4, 0x90
	.type	_ZN18btSimpleBroadphaseD0Ev,@function
_ZN18btSimpleBroadphaseD0Ev:            # @_ZN18btSimpleBroadphaseD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV18btSimpleBroadphase+16, (%rbx)
	movq	32(%rbx), %rdi
.Ltmp0:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp1:
# BB#1:                                 # %.noexc
	cmpb	$0, 56(%rbx)
	je	.LBB3_4
# BB#2:
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp2:
	callq	*(%rax)
.Ltmp3:
# BB#3:                                 # %.noexc2
	movq	48(%rbx), %rdi
.Ltmp4:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp5:
.LBB3_4:                                # %_ZN18btSimpleBroadphaseD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_5:
.Ltmp6:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN18btSimpleBroadphaseD0Ev, .Lfunc_end3-_ZN18btSimpleBroadphaseD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp5-.Ltmp0           #   Call between .Ltmp0 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end3-.Ltmp5      #   Call between .Ltmp5 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN18btSimpleBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_
	.p2align	4, 0x90
	.type	_ZN18btSimpleBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_,@function
_ZN18btSimpleBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_: # @_ZN18btSimpleBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 40
.Lcfi20:
	.cfi_offset %rbx, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movl	8(%rdi), %eax
	cmpl	12(%rdi), %eax
	jge	.LBB4_1
# BB#2:
	movq	56(%rsp), %r10
	movzwl	40(%rsp), %r14d
	movl	40(%rdi), %r15d
	movq	24(%rdi), %r11
	movslq	%r15d, %rbx
	movq	%rbx, %rcx
	shlq	$6, %rcx
	movl	60(%r11,%rcx), %ebp
	movl	%ebp, 40(%rdi)
	incl	%eax
	movl	%eax, 8(%rdi)
	cmpl	16(%rdi), %ebx
	jle	.LBB4_4
# BB#3:
	movl	%r15d, 16(%rdi)
.LBB4_4:
	leaq	(%r11,%rcx), %rax
	movq	%r8, (%r11,%rcx)
	movw	%r9w, 8(%r11,%rcx)
	movw	%r14w, 10(%r11,%rcx)
	movups	(%rsi), %xmm0
	movups	%xmm0, 28(%r11,%rcx)
	movups	(%rdx), %xmm0
	movups	%xmm0, 44(%r11,%rcx)
	movq	%r10, 16(%r11,%rcx)
	jmp	.LBB4_5
.LBB4_1:
	xorl	%eax, %eax
.LBB4_5:
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN18btSimpleBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_, .Lfunc_end4-_ZN18btSimpleBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_
	.cfi_endproc

	.globl	_ZN18btSimpleBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN18btSimpleBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher,@function
_ZN18btSimpleBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher: # @_ZN18btSimpleBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.cfi_startproc
# BB#0:
	movq	%rsi, %rax
	subq	24(%rdi), %rax
	shrq	$6, %rax
	cmpl	16(%rdi), %eax
	jne	.LBB5_2
# BB#1:
	leal	-1(%rax), %ecx
	movl	%ecx, 16(%rdi)
.LBB5_2:                                # %_ZN18btSimpleBroadphase10freeHandleEP23btSimpleBroadphaseProxy.exit
	movl	40(%rdi), %ecx
	movl	%ecx, 60(%rsi)
	movl	%eax, 40(%rdi)
	movq	$0, (%rsi)
	decl	8(%rdi)
	movq	48(%rdi), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end5:
	.size	_ZN18btSimpleBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher, .Lfunc_end5-_ZN18btSimpleBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.cfi_endproc

	.globl	_ZNK18btSimpleBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_
	.p2align	4, 0x90
	.type	_ZNK18btSimpleBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_,@function
_ZNK18btSimpleBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_: # @_ZNK18btSimpleBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_
	.cfi_startproc
# BB#0:
	movups	28(%rsi), %xmm0
	movups	%xmm0, (%rdx)
	movups	44(%rsi), %xmm0
	movups	%xmm0, (%rcx)
	retq
.Lfunc_end6:
	.size	_ZNK18btSimpleBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_, .Lfunc_end6-_ZNK18btSimpleBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_
	.cfi_endproc

	.globl	_ZN18btSimpleBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher
	.p2align	4, 0x90
	.type	_ZN18btSimpleBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher,@function
_ZN18btSimpleBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher: # @_ZN18btSimpleBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher
	.cfi_startproc
# BB#0:
	movups	(%rdx), %xmm0
	movups	%xmm0, 28(%rsi)
	movups	(%rcx), %xmm0
	movups	%xmm0, 44(%rsi)
	retq
.Lfunc_end7:
	.size	_ZN18btSimpleBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher, .Lfunc_end7-_ZN18btSimpleBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher
	.cfi_endproc

	.globl	_ZN18btSimpleBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_
	.p2align	4, 0x90
	.type	_ZN18btSimpleBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_,@function
_ZN18btSimpleBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_: # @_ZN18btSimpleBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 48
.Lcfi29:
	.cfi_offset %rbx, -40
.Lcfi30:
	.cfi_offset %r12, -32
.Lcfi31:
	.cfi_offset %r14, -24
.Lcfi32:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdi, %r15
	movl	16(%r15), %eax
	testl	%eax, %eax
	js	.LBB8_5
# BB#1:                                 # %.lr.ph
	movq	$-1, %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %rsi
	cmpq	$0, (%rsi,%rbx)
	je	.LBB8_4
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	addq	%rbx, %rsi
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*16(%rax)
	movl	16(%r15), %eax
.LBB8_4:                                #   in Loop: Header=BB8_2 Depth=1
	movslq	%eax, %rcx
	incq	%r12
	addq	$64, %rbx
	cmpq	%rcx, %r12
	jl	.LBB8_2
.LBB8_5:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	_ZN18btSimpleBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_, .Lfunc_end8-_ZN18btSimpleBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_
	.cfi_endproc

	.globl	_ZN18btSimpleBroadphase11aabbOverlapEP23btSimpleBroadphaseProxyS1_
	.p2align	4, 0x90
	.type	_ZN18btSimpleBroadphase11aabbOverlapEP23btSimpleBroadphaseProxyS1_,@function
_ZN18btSimpleBroadphase11aabbOverlapEP23btSimpleBroadphaseProxyS1_: # @_ZN18btSimpleBroadphase11aabbOverlapEP23btSimpleBroadphaseProxyS1_
	.cfi_startproc
# BB#0:
	movss	44(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	28(%rdi), %xmm0
	jae	.LBB9_2
# BB#1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB9_2:
	movss	44(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	28(%rsi), %xmm0
	jae	.LBB9_4
# BB#3:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB9_4:
	movss	48(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	32(%rdi), %xmm0
	jae	.LBB9_6
# BB#5:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB9_6:
	movss	48(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	32(%rsi), %xmm0
	jae	.LBB9_8
# BB#7:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB9_8:
	movss	52(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	36(%rdi), %xmm0
	jae	.LBB9_10
# BB#9:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB9_10:
	movss	52(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	36(%rsi), %xmm0
	setae	%al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end9:
	.size	_ZN18btSimpleBroadphase11aabbOverlapEP23btSimpleBroadphaseProxyS1_, .Lfunc_end9-_ZN18btSimpleBroadphase11aabbOverlapEP23btSimpleBroadphaseProxyS1_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_0:
	.zero	16
	.text
	.globl	_ZN18btSimpleBroadphase25calculateOverlappingPairsEP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN18btSimpleBroadphase25calculateOverlappingPairsEP12btDispatcher,@function
_ZN18btSimpleBroadphase25calculateOverlappingPairsEP12btDispatcher: # @_ZN18btSimpleBroadphase25calculateOverlappingPairsEP12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 128
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r12
	cmpl	$0, 8(%r12)
	js	.LBB10_49
# BB#1:                                 # %.preheader98
	movl	16(%r12), %eax
	movl	$-1, %edx
	testl	%eax, %eax
	js	.LBB10_23
# BB#2:                                 # %.lr.ph109
	xorl	%r13d, %r13d
	movl	$64, %r15d
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB10_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_20 Depth 2
	movq	24(%r12), %rcx
	movq	%r13, %rsi
	shlq	$6, %rsi
	cmpq	$0, (%rcx,%rsi)
	je	.LBB10_22
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB10_3 Depth=1
	movslq	%eax, %rdx
	cmpq	%rdx, %r13
	jge	.LBB10_5
# BB#6:                                 # %.lr.ph105
                                        #   in Loop: Header=BB10_3 Depth=1
	leaq	(%rcx,%rsi), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	28(%rcx,%rsi), %rbp
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%r15, 24(%rsp)          # 8-byte Spill
	cmpq	$0, (%rcx,%r15)
	jne	.LBB10_8
	jmp	.LBB10_20
	.p2align	4, 0x90
.LBB10_29:                              # %.backedge._crit_edge
                                        #   in Loop: Header=BB10_20 Depth=2
	movq	24(%r12), %rcx
	addq	$64, %r15
	incq	%r14
	cmpq	$0, (%rcx,%r15)
	je	.LBB10_20
.LBB10_8:                               #   in Loop: Header=BB10_3 Depth=1
	leaq	(%rcx,%r15), %rbx
	movss	44(%rcx,%r15), %xmm0    # xmm0 = mem[0],zero,zero,zero
	ucomiss	(%rbp), %xmm0
	jb	.LBB10_13
# BB#9:                                 #   in Loop: Header=BB10_3 Depth=1
	movss	16(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	28(%rcx,%r15), %xmm0
	jb	.LBB10_13
# BB#10:                                #   in Loop: Header=BB10_3 Depth=1
	movss	48(%rcx,%r15), %xmm0    # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%rbp), %xmm0
	jb	.LBB10_13
# BB#11:                                #   in Loop: Header=BB10_3 Depth=1
	movss	20(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	32(%rcx,%r15), %xmm0
	jb	.LBB10_13
# BB#12:                                #   in Loop: Header=BB10_3 Depth=1
	movss	52(%rcx,%r15), %xmm0    # xmm0 = mem[0],zero,zero,zero
	ucomiss	8(%rbp), %xmm0
	jae	.LBB10_17
	.p2align	4, 0x90
.LBB10_13:                              # %_ZN18btSimpleBroadphase11aabbOverlapEP23btSimpleBroadphaseProxyS1_.exit.thread
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	48(%r12), %rdi
.LBB10_14:                              #   in Loop: Header=BB10_3 Depth=1
	movq	(%rdi), %rax
	callq	*112(%rax)
	testb	%al, %al
	jne	.LBB10_20
# BB#15:                                #   in Loop: Header=BB10_3 Depth=1
	movq	48(%r12), %rdi
	movq	(%rdi), %rax
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%rbx, %rdx
	callq	*104(%rax)
	testq	%rax, %rax
	je	.LBB10_20
# BB#16:                                #   in Loop: Header=BB10_3 Depth=1
	movq	48(%r12), %rdi
	movq	(%rdi), %rax
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%rbx, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	callq	*24(%rax)
	.p2align	4, 0x90
.LBB10_20:                              # %.backedge
                                        #   Parent Loop BB10_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	16(%r12), %rax
	cmpq	%rax, %r14
	jl	.LBB10_29
	jmp	.LBB10_21
.LBB10_17:                              # %_ZN18btSimpleBroadphase11aabbOverlapEP23btSimpleBroadphaseProxyS1_.exit
                                        #   in Loop: Header=BB10_3 Depth=1
	movss	24(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	36(%rcx,%r15), %xmm0
	movq	48(%r12), %rdi
	jb	.LBB10_14
# BB#18:                                #   in Loop: Header=BB10_3 Depth=1
	movq	(%rdi), %rax
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%rbx, %rdx
	callq	*104(%rax)
	testq	%rax, %rax
	jne	.LBB10_20
# BB#19:                                #   in Loop: Header=BB10_3 Depth=1
	movq	48(%r12), %rdi
	movq	(%rdi), %rax
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%rbx, %rdx
	callq	*16(%rax)
	jmp	.LBB10_20
	.p2align	4, 0x90
.LBB10_5:                               #   in Loop: Header=BB10_3 Depth=1
	movl	%r13d, %edx
	jmp	.LBB10_22
	.p2align	4, 0x90
.LBB10_21:                              #   in Loop: Header=BB10_3 Depth=1
	movl	%r13d, %edx
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB10_22:                              # %.loopexit
                                        #   in Loop: Header=BB10_3 Depth=1
	movslq	%eax, %rcx
	addq	$64, %r15
	incq	%r14
	cmpq	%rcx, %r13
	leaq	1(%r13), %r13
	jl	.LBB10_3
.LBB10_23:                              # %._crit_edge110
	movl	%edx, 16(%r12)
	cmpb	$0, 56(%r12)
	je	.LBB10_49
# BB#24:
	movq	48(%r12), %rdi
	movq	(%rdi), %rax
	callq	*112(%rax)
	testb	%al, %al
	je	.LBB10_49
# BB#25:
	movq	48(%r12), %rdi
	movq	(%rdi), %rax
	callq	*56(%rax)
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	4(%rax), %eax
	cmpl	$2, %eax
	jl	.LBB10_27
# BB#26:
	decl	%eax
	xorl	%esi, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%eax, %edx
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	movq	(%rsp), %rax            # 8-byte Reload
	movl	4(%rax), %eax
.LBB10_27:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvT_.exit73
	subl	60(%r12), %eax
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	leaq	32(%rsp), %rdx
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%eax, %esi
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	$0, 60(%r12)
	movl	4(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB10_28
# BB#30:                                # %.lr.ph
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB10_31:                              # =>This Inner Loop Header: Depth=1
	movq	%r14, %rsi
	movq	16(%rdi), %rbp
	movq	(%rbp,%rbx), %r14
	movq	8(%rbp,%rbx), %rdx
	cmpq	%rsi, %r14
	jne	.LBB10_33
# BB#32:                                #   in Loop: Header=BB10_31 Depth=1
	cmpq	%r15, %rdx
	je	.LBB10_44
.LBB10_33:                              # %_ZeqRK16btBroadphasePairS1_.exit.thread
                                        #   in Loop: Header=BB10_31 Depth=1
	movss	44(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	28(%r14), %xmm0
	jae	.LBB10_35
# BB#34:                                #   in Loop: Header=BB10_31 Depth=1
	movq	%rdx, %r15
	jmp	.LBB10_44
	.p2align	4, 0x90
.LBB10_35:                              #   in Loop: Header=BB10_31 Depth=1
	movss	44(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	28(%rdx), %xmm0
	jae	.LBB10_37
# BB#36:                                #   in Loop: Header=BB10_31 Depth=1
	movq	%rdx, %r15
	jmp	.LBB10_44
.LBB10_37:                              #   in Loop: Header=BB10_31 Depth=1
	movss	48(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	32(%r14), %xmm0
	jae	.LBB10_39
# BB#38:                                #   in Loop: Header=BB10_31 Depth=1
	movq	%rdx, %r15
	jmp	.LBB10_44
.LBB10_39:                              #   in Loop: Header=BB10_31 Depth=1
	movss	48(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	32(%rdx), %xmm0
	jae	.LBB10_41
# BB#40:                                #   in Loop: Header=BB10_31 Depth=1
	movq	%rdx, %r15
	jmp	.LBB10_44
.LBB10_41:                              #   in Loop: Header=BB10_31 Depth=1
	movss	52(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	36(%r14), %xmm0
	jae	.LBB10_43
# BB#42:                                #   in Loop: Header=BB10_31 Depth=1
	movq	%rdx, %r15
	jmp	.LBB10_44
.LBB10_43:                              # %_ZN18btSimpleBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_.exit
                                        #   in Loop: Header=BB10_31 Depth=1
	movss	52(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	36(%rdx), %xmm0
	movq	%rdx, %r15
	jae	.LBB10_45
	.p2align	4, 0x90
.LBB10_44:                              # %_ZN18btSimpleBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_.exit.thread
                                        #   in Loop: Header=BB10_31 Depth=1
	leaq	(%rbp,%rbx), %rsi
	movq	48(%r12), %rdi
	movq	(%rdi), %rax
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	*64(%rax)
	movq	(%rsp), %rdi            # 8-byte Reload
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp,%rbx)
	movl	60(%r12), %ecx
	incl	%ecx
	movl	%ecx, 60(%r12)
	decl	gOverlappingPairs(%rip)
	movl	4(%rdi), %eax
	movq	%r15, %rdx
.LBB10_45:                              # %.thread97
                                        #   in Loop: Header=BB10_31 Depth=1
	incq	%r13
	movslq	%eax, %rsi
	addq	$32, %rbx
	cmpq	%rsi, %r13
	movq	%rdx, %r15
	jl	.LBB10_31
# BB#46:                                # %._crit_edge
	cmpl	$2, %eax
	jl	.LBB10_48
# BB#47:
	decl	%eax
	xorl	%esi, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%eax, %edx
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	4(%rdi), %eax
	movl	60(%r12), %ecx
	jmp	.LBB10_48
.LBB10_28:
	xorl	%ecx, %ecx
.LBB10_48:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvT_.exit
	subl	%ecx, %eax
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	leaq	32(%rsp), %rdx
	movl	%eax, %esi
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	movl	$0, 60(%r12)
.LBB10_49:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZN18btSimpleBroadphase25calculateOverlappingPairsEP12btDispatcher, .Lfunc_end10-_ZN18btSimpleBroadphase25calculateOverlappingPairsEP12btDispatcher
	.cfi_endproc

	.section	.text._ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_,"axG",@progbits,_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_,comdat
	.weak	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	.p2align	4, 0x90
	.type	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_,@function
_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_: # @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 48
.Lcfi51:
	.cfi_offset %rbx, -48
.Lcfi52:
	.cfi_offset %r12, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %r14d
	movq	%rdi, %r15
	movl	4(%r15), %r12d
	cmpl	%r14d, %r12d
	jg	.LBB11_19
# BB#1:
	jge	.LBB11_19
# BB#2:
	cmpl	%r14d, 8(%r15)
	jge	.LBB11_14
# BB#3:
	testl	%r14d, %r14d
	je	.LBB11_4
# BB#5:
	movslq	%r14d, %rdi
	shlq	$5, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
	movl	4(%r15), %eax
	testl	%eax, %eax
	jg	.LBB11_7
	jmp	.LBB11_9
.LBB11_4:
	xorl	%ebp, %ebp
	movl	%r12d, %eax
	testl	%eax, %eax
	jle	.LBB11_9
.LBB11_7:                               # %.lr.ph.i.i
	cltq
	movl	$24, %ecx
	.p2align	4, 0x90
.LBB11_8:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rdx
	movups	-24(%rdx,%rcx), %xmm0
	movups	%xmm0, -24(%rbp,%rcx)
	movq	-8(%rdx,%rcx), %rsi
	movq	%rsi, -8(%rbp,%rcx)
	movq	(%rdx,%rcx), %rdx
	movq	%rdx, (%rbp,%rcx)
	addq	$32, %rcx
	decq	%rax
	jne	.LBB11_8
.LBB11_9:                               # %_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_.exit.i
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB11_13
# BB#10:
	cmpb	$0, 24(%r15)
	je	.LBB11_12
# BB#11:
	callq	_Z21btAlignedFreeInternalPv
.LBB11_12:
	movq	$0, 16(%r15)
.LBB11_13:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit.preheader
	movb	$1, 24(%r15)
	movq	%rbp, 16(%r15)
	movl	%r14d, 8(%r15)
	cmpl	%r14d, %r12d
	jge	.LBB11_19
.LBB11_14:                              # %.lr.ph
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	movl	%r14d, %ecx
	subl	%r12d, %ecx
	leaq	-1(%rax), %rsi
	testb	$1, %cl
	movq	%rdx, %rcx
	je	.LBB11_16
# BB#15:                                # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit.prol
	movq	16(%r15), %rcx
	movq	%rdx, %rdi
	shlq	$5, %rdi
	movups	(%rbx), %xmm0
	movups	%xmm0, (%rcx,%rdi)
	movq	16(%rbx), %rbp
	movq	%rbp, 16(%rcx,%rdi)
	movq	24(%rbx), %rbp
	movq	%rbp, 24(%rcx,%rdi)
	leaq	1(%rdx), %rcx
.LBB11_16:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit.prol.loopexit
	cmpq	%rdx, %rsi
	je	.LBB11_19
# BB#17:                                # %.lr.ph.new
	subq	%rcx, %rax
	shlq	$5, %rcx
	.p2align	4, 0x90
.LBB11_18:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rdx
	movups	(%rbx), %xmm0
	movups	%xmm0, (%rdx,%rcx)
	movq	16(%rbx), %rsi
	movq	%rsi, 16(%rdx,%rcx)
	movq	24(%rbx), %rsi
	movq	%rsi, 24(%rdx,%rcx)
	movq	16(%r15), %rdx
	movups	(%rbx), %xmm0
	movups	%xmm0, 32(%rdx,%rcx)
	movq	16(%rbx), %rsi
	movq	%rsi, 48(%rdx,%rcx)
	movq	24(%rbx), %rsi
	movq	%rsi, 56(%rdx,%rcx)
	addq	$64, %rcx
	addq	$-2, %rax
	jne	.LBB11_18
.LBB11_19:                              # %.loopexit
	movl	%r14d, 4(%r15)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_, .Lfunc_end11-_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	.cfi_endproc

	.text
	.globl	_ZN18btSimpleBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_
	.p2align	4, 0x90
	.type	_ZN18btSimpleBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_,@function
_ZN18btSimpleBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_: # @_ZN18btSimpleBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_
	.cfi_startproc
# BB#0:
	movss	44(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	28(%rsi), %xmm0
	jae	.LBB12_2
# BB#1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB12_2:
	movss	44(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	28(%rdx), %xmm0
	jae	.LBB12_4
# BB#3:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB12_4:
	movss	48(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	32(%rsi), %xmm0
	jae	.LBB12_6
# BB#5:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB12_6:
	movss	48(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	32(%rdx), %xmm0
	jae	.LBB12_8
# BB#7:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB12_8:
	movss	52(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	36(%rsi), %xmm0
	jae	.LBB12_10
# BB#9:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB12_10:
	movss	52(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	36(%rdx), %xmm0
	setae	%al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end12:
	.size	_ZN18btSimpleBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_, .Lfunc_end12-_ZN18btSimpleBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_
	.cfi_endproc

	.globl	_ZN18btSimpleBroadphase9resetPoolEP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN18btSimpleBroadphase9resetPoolEP12btDispatcher,@function
_ZN18btSimpleBroadphase9resetPoolEP12btDispatcher: # @_ZN18btSimpleBroadphase9resetPoolEP12btDispatcher
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end13:
	.size	_ZN18btSimpleBroadphase9resetPoolEP12btDispatcher, .Lfunc_end13-_ZN18btSimpleBroadphase9resetPoolEP12btDispatcher
	.cfi_endproc

	.section	.text._ZN18btSimpleBroadphase23getOverlappingPairCacheEv,"axG",@progbits,_ZN18btSimpleBroadphase23getOverlappingPairCacheEv,comdat
	.weak	_ZN18btSimpleBroadphase23getOverlappingPairCacheEv
	.p2align	4, 0x90
	.type	_ZN18btSimpleBroadphase23getOverlappingPairCacheEv,@function
_ZN18btSimpleBroadphase23getOverlappingPairCacheEv: # @_ZN18btSimpleBroadphase23getOverlappingPairCacheEv
	.cfi_startproc
# BB#0:
	movq	48(%rdi), %rax
	retq
.Lfunc_end14:
	.size	_ZN18btSimpleBroadphase23getOverlappingPairCacheEv, .Lfunc_end14-_ZN18btSimpleBroadphase23getOverlappingPairCacheEv
	.cfi_endproc

	.section	.text._ZNK18btSimpleBroadphase23getOverlappingPairCacheEv,"axG",@progbits,_ZNK18btSimpleBroadphase23getOverlappingPairCacheEv,comdat
	.weak	_ZNK18btSimpleBroadphase23getOverlappingPairCacheEv
	.p2align	4, 0x90
	.type	_ZNK18btSimpleBroadphase23getOverlappingPairCacheEv,@function
_ZNK18btSimpleBroadphase23getOverlappingPairCacheEv: # @_ZNK18btSimpleBroadphase23getOverlappingPairCacheEv
	.cfi_startproc
# BB#0:
	movq	48(%rdi), %rax
	retq
.Lfunc_end15:
	.size	_ZNK18btSimpleBroadphase23getOverlappingPairCacheEv, .Lfunc_end15-_ZNK18btSimpleBroadphase23getOverlappingPairCacheEv
	.cfi_endproc

	.section	.text._ZNK18btSimpleBroadphase17getBroadphaseAabbER9btVector3S1_,"axG",@progbits,_ZNK18btSimpleBroadphase17getBroadphaseAabbER9btVector3S1_,comdat
	.weak	_ZNK18btSimpleBroadphase17getBroadphaseAabbER9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK18btSimpleBroadphase17getBroadphaseAabbER9btVector3S1_,@function
_ZNK18btSimpleBroadphase17getBroadphaseAabbER9btVector3S1_: # @_ZNK18btSimpleBroadphase17getBroadphaseAabbER9btVector3S1_
	.cfi_startproc
# BB#0:
	movl	$-581039253, (%rsi)     # imm = 0xDD5E0B6B
	movl	$-581039253, 4(%rsi)    # imm = 0xDD5E0B6B
	movl	$-581039253, 8(%rsi)    # imm = 0xDD5E0B6B
	movl	$0, 12(%rsi)
	movabsq	$6727827449093950315, %rax # imm = 0x5D5E0B6B5D5E0B6B
	movq	%rax, (%rdx)
	movq	$1566444395, 8(%rdx)    # imm = 0x5D5E0B6B
	retq
.Lfunc_end16:
	.size	_ZNK18btSimpleBroadphase17getBroadphaseAabbER9btVector3S1_, .Lfunc_end16-_ZNK18btSimpleBroadphase17getBroadphaseAabbER9btVector3S1_
	.cfi_endproc

	.section	.text._ZN18btSimpleBroadphase10printStatsEv,"axG",@progbits,_ZN18btSimpleBroadphase10printStatsEv,comdat
	.weak	_ZN18btSimpleBroadphase10printStatsEv
	.p2align	4, 0x90
	.type	_ZN18btSimpleBroadphase10printStatsEv,@function
_ZN18btSimpleBroadphase10printStatsEv:  # @_ZN18btSimpleBroadphase10printStatsEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end17:
	.size	_ZN18btSimpleBroadphase10printStatsEv, .Lfunc_end17-_ZN18btSimpleBroadphase10printStatsEv
	.cfi_endproc

	.section	.text._ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii,"axG",@progbits,_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii,comdat
	.weak	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	.p2align	4, 0x90
	.type	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii,@function
_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii: # @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi62:
	.cfi_def_cfa_offset 64
.Lcfi63:
	.cfi_offset %rbx, -56
.Lcfi64:
	.cfi_offset %r12, -48
.Lcfi65:
	.cfi_offset %r13, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%esi, %r12d
	movq	%rdi, %r15
	movq	%rdx, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB18_1:                               # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_2 Depth 2
                                        #       Child Loop BB18_20 Depth 3
                                        #       Child Loop BB18_4 Depth 3
                                        #       Child Loop BB18_57 Depth 3
                                        #       Child Loop BB18_33 Depth 3
	movl	%r12d, %esi
	movq	16(%r15), %r8
	leal	(%rsi,%rdx), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	shlq	$5, %rax
	movq	(%r8,%rax), %r10
	movq	8(%r8,%rax), %r13
	movq	16(%r8,%rax), %r9
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	jmp	.LBB18_2
	.p2align	4, 0x90
.LBB18_48:                              # %._crit_edge
                                        #   in Loop: Header=BB18_2 Depth=2
	movq	16(%r15), %r8
.LBB18_2:                               #   Parent Loop BB18_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB18_20 Depth 3
                                        #       Child Loop BB18_4 Depth 3
                                        #       Child Loop BB18_57 Depth 3
                                        #       Child Loop BB18_33 Depth 3
	movslq	%r12d, %r12
	testq	%r10, %r10
	je	.LBB18_3
# BB#19:                                # %.split.preheader
                                        #   in Loop: Header=BB18_2 Depth=2
	movl	24(%r10), %r11d
	movq	%r12, %rax
	shlq	$5, %rax
	leaq	16(%r8,%rax), %rax
	jmp	.LBB18_20
	.p2align	4, 0x90
.LBB18_55:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread
                                        #   in Loop: Header=BB18_20 Depth=3
	incq	%r12
	addq	$32, %rax
.LBB18_20:                              # %.split
                                        #   Parent Loop BB18_1 Depth=1
                                        #     Parent Loop BB18_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%rax), %rdi
	movl	$-1, %ebx
	testq	%rdi, %rdi
	movl	$-1, %ebp
	je	.LBB18_22
# BB#21:                                #   in Loop: Header=BB18_20 Depth=3
	movl	24(%rdi), %ebp
.LBB18_22:                              #   in Loop: Header=BB18_20 Depth=3
	movq	-8(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB18_24
# BB#23:                                #   in Loop: Header=BB18_20 Depth=3
	movl	24(%rcx), %ebx
.LBB18_24:                              #   in Loop: Header=BB18_20 Depth=3
	testq	%r13, %r13
	je	.LBB18_25
# BB#26:                                #   in Loop: Header=BB18_20 Depth=3
	movl	24(%r13), %r14d
	cmpl	%r11d, %ebp
	jg	.LBB18_55
	jmp	.LBB18_28
	.p2align	4, 0x90
.LBB18_25:                              #   in Loop: Header=BB18_20 Depth=3
	movl	$-1, %r14d
	cmpl	%r11d, %ebp
	jg	.LBB18_55
.LBB18_28:                              #   in Loop: Header=BB18_20 Depth=3
	cmpq	%r10, %rdi
	setne	%bpl
	cmpl	%r14d, %ebx
	jg	.LBB18_53
# BB#29:                                #   in Loop: Header=BB18_20 Depth=3
	testb	%bpl, %bpl
	jne	.LBB18_53
# BB#30:                                #   in Loop: Header=BB18_20 Depth=3
	cmpq	%r13, %rcx
	jne	.LBB18_31
# BB#52:                                #   in Loop: Header=BB18_20 Depth=3
	cmpq	%r9, (%rax)
	ja	.LBB18_55
	jmp	.LBB18_31
	.p2align	4, 0x90
.LBB18_53:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit
                                        #   in Loop: Header=BB18_20 Depth=3
	cmpq	%r10, %rdi
	jne	.LBB18_31
# BB#54:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit
                                        #   in Loop: Header=BB18_20 Depth=3
	cmpl	%r14d, %ebx
	jg	.LBB18_55
	jmp	.LBB18_31
	.p2align	4, 0x90
.LBB18_3:                               # %.split.us.preheader
                                        #   in Loop: Header=BB18_2 Depth=2
	movq	%r12, %rax
	shlq	$5, %rax
	leaq	16(%r8,%rax), %rdi
	jmp	.LBB18_4
	.p2align	4, 0x90
.LBB18_18:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread.us
                                        #   in Loop: Header=BB18_4 Depth=3
	incq	%r12
	addq	$32, %rdi
.LBB18_4:                               # %.split.us
                                        #   Parent Loop BB18_1 Depth=1
                                        #     Parent Loop BB18_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%rdi), %rbx
	movl	$-1, %r11d
	testq	%rbx, %rbx
	movl	$-1, %eax
	je	.LBB18_6
# BB#5:                                 #   in Loop: Header=BB18_4 Depth=3
	movl	24(%rbx), %eax
.LBB18_6:                               #   in Loop: Header=BB18_4 Depth=3
	movq	-8(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB18_8
# BB#7:                                 #   in Loop: Header=BB18_4 Depth=3
	movl	24(%rcx), %r11d
.LBB18_8:                               #   in Loop: Header=BB18_4 Depth=3
	testq	%r13, %r13
	je	.LBB18_9
# BB#10:                                #   in Loop: Header=BB18_4 Depth=3
	movl	24(%r13), %ebp
	testl	%eax, %eax
	jns	.LBB18_18
	jmp	.LBB18_12
	.p2align	4, 0x90
.LBB18_9:                               #   in Loop: Header=BB18_4 Depth=3
	movl	$-1, %ebp
	testl	%eax, %eax
	jns	.LBB18_18
.LBB18_12:                              #   in Loop: Header=BB18_4 Depth=3
	testq	%rbx, %rbx
	setne	%al
	cmpl	%ebp, %r11d
	jg	.LBB18_16
# BB#13:                                #   in Loop: Header=BB18_4 Depth=3
	testb	%al, %al
	jne	.LBB18_16
# BB#14:                                #   in Loop: Header=BB18_4 Depth=3
	cmpq	%r13, %rcx
	jne	.LBB18_31
# BB#15:                                #   in Loop: Header=BB18_4 Depth=3
	cmpq	%r9, (%rdi)
	ja	.LBB18_18
	jmp	.LBB18_31
.LBB18_16:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.us
                                        #   in Loop: Header=BB18_4 Depth=3
	testq	%rbx, %rbx
	jne	.LBB18_31
# BB#17:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.us
                                        #   in Loop: Header=BB18_4 Depth=3
	cmpl	%ebp, %r11d
	jg	.LBB18_18
	.p2align	4, 0x90
.LBB18_31:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.preheader
                                        #   in Loop: Header=BB18_2 Depth=2
	movslq	%edx, %rdx
	testq	%r10, %r10
	je	.LBB18_32
# BB#56:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.preheader47
                                        #   in Loop: Header=BB18_2 Depth=2
	movl	24(%r10), %r11d
	movq	%rdx, %rax
	shlq	$5, %rax
	leaq	16(%r8,%rax), %rax
	jmp	.LBB18_57
	.p2align	4, 0x90
.LBB18_71:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.thread
                                        #   in Loop: Header=BB18_57 Depth=3
	decq	%rdx
	addq	$-32, %rax
.LBB18_57:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40
                                        #   Parent Loop BB18_1 Depth=1
                                        #     Parent Loop BB18_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%rax), %rdi
	movl	$-1, %r14d
	testq	%rdi, %rdi
	movl	$-1, %ecx
	je	.LBB18_59
# BB#58:                                #   in Loop: Header=BB18_57 Depth=3
	movl	24(%rdi), %ecx
.LBB18_59:                              #   in Loop: Header=BB18_57 Depth=3
	testq	%r13, %r13
	je	.LBB18_61
# BB#60:                                #   in Loop: Header=BB18_57 Depth=3
	movl	24(%r13), %r14d
.LBB18_61:                              #   in Loop: Header=BB18_57 Depth=3
	movq	-8(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB18_62
# BB#63:                                #   in Loop: Header=BB18_57 Depth=3
	movl	24(%rbp), %ebx
	cmpl	%ecx, %r11d
	jg	.LBB18_71
	jmp	.LBB18_65
	.p2align	4, 0x90
.LBB18_62:                              #   in Loop: Header=BB18_57 Depth=3
	movl	$-1, %ebx
	cmpl	%ecx, %r11d
	jg	.LBB18_71
.LBB18_65:                              #   in Loop: Header=BB18_57 Depth=3
	cmpq	%rdi, %r10
	setne	%cl
	cmpl	%ebx, %r14d
	jg	.LBB18_69
# BB#66:                                #   in Loop: Header=BB18_57 Depth=3
	testb	%cl, %cl
	jne	.LBB18_69
# BB#67:                                #   in Loop: Header=BB18_57 Depth=3
	cmpq	%rbp, %r13
	jne	.LBB18_45
# BB#68:                                #   in Loop: Header=BB18_57 Depth=3
	cmpq	(%rax), %r9
	ja	.LBB18_71
	jmp	.LBB18_45
	.p2align	4, 0x90
.LBB18_69:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32
                                        #   in Loop: Header=BB18_57 Depth=3
	cmpq	%rdi, %r10
	jne	.LBB18_45
# BB#70:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32
                                        #   in Loop: Header=BB18_57 Depth=3
	cmpl	%ebx, %r14d
	jg	.LBB18_71
	jmp	.LBB18_45
	.p2align	4, 0x90
.LBB18_32:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.us.preheader
                                        #   in Loop: Header=BB18_2 Depth=2
	movq	%rdx, %rax
	shlq	$5, %rax
	leaq	16(%r8,%rax), %rax
	jmp	.LBB18_33
	.p2align	4, 0x90
.LBB18_51:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.thread.us
                                        #   in Loop: Header=BB18_33 Depth=3
	decq	%rdx
	addq	$-32, %rax
.LBB18_33:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.us
                                        #   Parent Loop BB18_1 Depth=1
                                        #     Parent Loop BB18_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%rax), %rbx
	movl	$-1, %r11d
	testq	%rbx, %rbx
	movl	$-1, %ecx
	je	.LBB18_35
# BB#34:                                #   in Loop: Header=BB18_33 Depth=3
	movl	24(%rbx), %ecx
.LBB18_35:                              #   in Loop: Header=BB18_33 Depth=3
	testq	%r13, %r13
	je	.LBB18_37
# BB#36:                                #   in Loop: Header=BB18_33 Depth=3
	movl	24(%r13), %r11d
.LBB18_37:                              #   in Loop: Header=BB18_33 Depth=3
	movq	-8(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB18_38
# BB#39:                                #   in Loop: Header=BB18_33 Depth=3
	movl	24(%rbp), %edi
	cmpl	$-1, %ecx
	jl	.LBB18_51
	jmp	.LBB18_41
	.p2align	4, 0x90
.LBB18_38:                              #   in Loop: Header=BB18_33 Depth=3
	movl	$-1, %edi
	cmpl	$-1, %ecx
	jl	.LBB18_51
.LBB18_41:                              #   in Loop: Header=BB18_33 Depth=3
	testq	%rbx, %rbx
	setne	%cl
	cmpl	%edi, %r11d
	jg	.LBB18_49
# BB#42:                                #   in Loop: Header=BB18_33 Depth=3
	testb	%cl, %cl
	jne	.LBB18_49
# BB#43:                                #   in Loop: Header=BB18_33 Depth=3
	cmpq	%rbp, %r13
	jne	.LBB18_45
# BB#44:                                #   in Loop: Header=BB18_33 Depth=3
	cmpq	(%rax), %r9
	ja	.LBB18_51
	jmp	.LBB18_45
	.p2align	4, 0x90
.LBB18_49:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.us
                                        #   in Loop: Header=BB18_33 Depth=3
	testq	%rbx, %rbx
	jne	.LBB18_45
# BB#50:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.us
                                        #   in Loop: Header=BB18_33 Depth=3
	cmpl	%edi, %r11d
	jg	.LBB18_51
	.p2align	4, 0x90
.LBB18_45:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.thread41
                                        #   in Loop: Header=BB18_2 Depth=2
	cmpl	%edx, %r12d
	jg	.LBB18_47
# BB#46:                                #   in Loop: Header=BB18_2 Depth=2
	movq	%r12, %rax
	shlq	$5, %rax
	movups	(%r8,%rax), %xmm0
	movups	16(%r8,%rax), %xmm1
	movq	%rdx, %rcx
	shlq	$5, %rcx
	movups	(%r8,%rcx), %xmm2
	movups	16(%r8,%rcx), %xmm3
	movups	%xmm3, 16(%r8,%rax)
	movups	%xmm2, (%r8,%rax)
	movq	16(%r15), %rax
	movups	%xmm0, (%rax,%rcx)
	movups	%xmm1, 16(%rax,%rcx)
	leal	1(%r12), %r12d
	leal	-1(%rdx), %edx
.LBB18_47:                              #   in Loop: Header=BB18_2 Depth=2
	cmpl	%edx, %r12d
	jle	.LBB18_48
# BB#72:                                #   in Loop: Header=BB18_1 Depth=1
	cmpl	%esi, %edx
	jle	.LBB18_74
# BB#73:                                #   in Loop: Header=BB18_1 Depth=1
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
.LBB18_74:                              #   in Loop: Header=BB18_1 Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
	cmpl	%edx, %r12d
	jl	.LBB18_1
# BB#75:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii, .Lfunc_end18-_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	.cfi_endproc

	.type	_ZTV18btSimpleBroadphase,@object # @_ZTV18btSimpleBroadphase
	.section	.rodata,"a",@progbits
	.globl	_ZTV18btSimpleBroadphase
	.p2align	3
_ZTV18btSimpleBroadphase:
	.quad	0
	.quad	_ZTI18btSimpleBroadphase
	.quad	_ZN18btSimpleBroadphaseD2Ev
	.quad	_ZN18btSimpleBroadphaseD0Ev
	.quad	_ZN18btSimpleBroadphase11createProxyERK9btVector3S2_iPvssP12btDispatcherS3_
	.quad	_ZN18btSimpleBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.quad	_ZN18btSimpleBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher
	.quad	_ZNK18btSimpleBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_
	.quad	_ZN18btSimpleBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_
	.quad	_ZN18btSimpleBroadphase25calculateOverlappingPairsEP12btDispatcher
	.quad	_ZN18btSimpleBroadphase23getOverlappingPairCacheEv
	.quad	_ZNK18btSimpleBroadphase23getOverlappingPairCacheEv
	.quad	_ZNK18btSimpleBroadphase17getBroadphaseAabbER9btVector3S1_
	.quad	_ZN18btSimpleBroadphase9resetPoolEP12btDispatcher
	.quad	_ZN18btSimpleBroadphase10printStatsEv
	.size	_ZTV18btSimpleBroadphase, 120

	.type	_ZTS18btSimpleBroadphase,@object # @_ZTS18btSimpleBroadphase
	.globl	_ZTS18btSimpleBroadphase
	.p2align	4
_ZTS18btSimpleBroadphase:
	.asciz	"18btSimpleBroadphase"
	.size	_ZTS18btSimpleBroadphase, 21

	.type	_ZTS21btBroadphaseInterface,@object # @_ZTS21btBroadphaseInterface
	.section	.rodata._ZTS21btBroadphaseInterface,"aG",@progbits,_ZTS21btBroadphaseInterface,comdat
	.weak	_ZTS21btBroadphaseInterface
	.p2align	4
_ZTS21btBroadphaseInterface:
	.asciz	"21btBroadphaseInterface"
	.size	_ZTS21btBroadphaseInterface, 24

	.type	_ZTI21btBroadphaseInterface,@object # @_ZTI21btBroadphaseInterface
	.section	.rodata._ZTI21btBroadphaseInterface,"aG",@progbits,_ZTI21btBroadphaseInterface,comdat
	.weak	_ZTI21btBroadphaseInterface
	.p2align	3
_ZTI21btBroadphaseInterface:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS21btBroadphaseInterface
	.size	_ZTI21btBroadphaseInterface, 16

	.type	_ZTI18btSimpleBroadphase,@object # @_ZTI18btSimpleBroadphase
	.section	.rodata,"a",@progbits
	.globl	_ZTI18btSimpleBroadphase
	.p2align	4
_ZTI18btSimpleBroadphase:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18btSimpleBroadphase
	.quad	_ZTI21btBroadphaseInterface
	.size	_ZTI18btSimpleBroadphase, 24


	.globl	_ZN18btSimpleBroadphaseC1EiP22btOverlappingPairCache
	.type	_ZN18btSimpleBroadphaseC1EiP22btOverlappingPairCache,@function
_ZN18btSimpleBroadphaseC1EiP22btOverlappingPairCache = _ZN18btSimpleBroadphaseC2EiP22btOverlappingPairCache
	.globl	_ZN18btSimpleBroadphaseD1Ev
	.type	_ZN18btSimpleBroadphaseD1Ev,@function
_ZN18btSimpleBroadphaseD1Ev = _ZN18btSimpleBroadphaseD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
