	.text
	.file	"espresso.bc"
	.globl	espresso
	.p2align	4, 0x90
	.type	espresso,@function
espresso:                               # @espresso
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r14
	movq	%r14, (%rsp)
	leaq	24(%rsp), %rbx
	leaq	48(%rsp), %r13
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_27:                               # %.critedge1
                                        #   in Loop: Header=BB0_1 Depth=1
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r15, %rsi
	callq	sf_append
	movq	%rax, %rcx
	movq	%rcx, (%rsp)
	cmpl	$0, trace(%rip)
	je	.LBB0_29
# BB#28:                                #   in Loop: Header=BB0_1 Depth=1
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	size_stamp
.LBB0_29:                               #   in Loop: Header=BB0_1 Depth=1
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	(%rsp), %rdi
	cmpl	$0, skip_make_sparse(%rip)
	jne	.LBB0_31
# BB#30:                                #   in Loop: Header=BB0_1 Depth=1
	xorl	%eax, %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	make_sparse
	movq	%rax, %rdi
	movq	%rdi, (%rsp)
.LBB0_31:                               # %._crit_edge56
                                        #   in Loop: Header=BB0_1 Depth=1
	movl	12(%r14), %eax
	cmpl	12(%rdi), %eax
	jge	.LBB0_33
# BB#32:                                #   in Loop: Header=BB0_1 Depth=1
	xorl	%eax, %eax
	callq	sf_free
	movq	%r14, (%rsp)
	movl	$0, unwrap_onset(%rip)
.LBB0_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_14 Depth 2
                                        #     Child Loop BB0_20 Depth 2
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_save
	movq	%rax, %r14
	xorl	%eax, %eax
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	sf_save
	movq	%rax, 8(%rsp)
	cmpl	$0, recompute_onset(%rip)
	je	.LBB0_2
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r15
	movq	(%rsp), %rdi
	xorl	%eax, %eax
	callq	cube1list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	simplify
	movq	%rax, %rbp
	cmpl	$0, trace(%rip)
	je	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_1 Depth=1
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r15, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB0_5:                                #   in Loop: Header=BB0_1 Depth=1
	movq	(%rsp), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	%rbp, (%rsp)
	jmp	.LBB0_6
	.p2align	4, 0x90
.LBB0_2:                                # %._crit_edge53
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	(%rsp), %rbp
.LBB0_6:                                #   in Loop: Header=BB0_1 Depth=1
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	cover_cost
	cmpl	$0, unwrap_onset(%rip)
	je	.LBB0_12
# BB#7:                                 #   in Loop: Header=BB0_1 Depth=1
	movq	cube+32(%rip), %rax
	movslq	cube+4(%rip), %rcx
	movl	-4(%rax,%rcx,4), %eax
	cmpl	$2, %eax
	jl	.LBB0_12
# BB#8:                                 #   in Loop: Header=BB0_1 Depth=1
	movl	32(%rsp), %ecx
	cmpl	$4999, %ecx             # imm = 0x1387
	jg	.LBB0_12
# BB#9:                                 #   in Loop: Header=BB0_1 Depth=1
	imull	24(%rsp), %eax
	cmpl	%eax, %ecx
	je	.LBB0_12
# BB#10:                                #   in Loop: Header=BB0_1 Depth=1
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r15
	movq	(%rsp), %rdi
	movl	cube+4(%rip), %esi
	decl	%esi
	xorl	%eax, %eax
	callq	unravel
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_contain
	movq	%rax, %rbp
	movq	%rbp, (%rsp)
	cmpl	$0, trace(%rip)
	je	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_1 Depth=1
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r15, %rcx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	print_trace
	.p2align	4, 0x90
.LBB0_12:                               #   in Loop: Header=BB0_1 Depth=1
	movq	(%rsp), %rax
	movslq	(%rax), %rcx
	movslq	12(%rax), %rdx
	imulq	%rcx, %rdx
	testl	%edx, %edx
	jle	.LBB0_15
# BB#13:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	24(%rax), %rcx
	leaq	(%rcx,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB0_14:                               # %.lr.ph
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	andb	$127, 1(%rcx)
	movslq	(%rax), %rsi
	leaq	(%rcx,%rsi,4), %rcx
	cmpq	%rdx, %rcx
	jb	.LBB0_14
.LBB0_15:                               # %._crit_edge
                                        #   in Loop: Header=BB0_1 Depth=1
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	expand
	movq	%rax, %rcx
	movq	%rcx, (%rsp)
	movl	$4, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	movq	%rbx, %rcx
	callq	totals
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	movq	8(%rsp), %rsi
	xorl	%eax, %eax
	callq	irredundant
	movq	%rax, %rcx
	movq	%rcx, (%rsp)
	movl	$5, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	movq	%rbx, %rcx
	callq	totals
	cmpl	$0, single_expand(%rip)
	jne	.LBB0_29
# BB#16:                                #   in Loop: Header=BB0_1 Depth=1
	cmpl	$0, remove_essential(%rip)
	je	.LBB0_18
# BB#17:                                #   in Loop: Header=BB0_1 Depth=1
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbp
	xorl	%eax, %eax
	movq	%rsp, %rdi
	leaq	8(%rsp), %rsi
	callq	essential
	movq	%rax, %r15
	movl	$3, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, %rdx
	movq	%rbx, %rcx
	callq	totals
	jmp	.LBB0_19
.LBB0_18:                               #   in Loop: Header=BB0_1 Depth=1
	movl	cube(%rip), %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r15
.LBB0_19:                               #   in Loop: Header=BB0_1 Depth=1
	movq	(%rsp), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cover_cost
	.p2align	4, 0x90
.LBB0_20:                               #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	copy_cost
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	movq	8(%rsp), %rsi
	xorl	%eax, %eax
	callq	reduce
	movq	%rax, %rcx
	movq	%rcx, (%rsp)
	movl	$6, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	movq	%rbx, %rcx
	callq	totals
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	expand
	movq	%rax, %rcx
	movq	%rcx, (%rsp)
	movl	$4, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	movq	%rbx, %rcx
	callq	totals
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	movq	8(%rsp), %rsi
	xorl	%eax, %eax
	callq	irredundant
	movq	%rax, %rcx
	movq	%rcx, (%rsp)
	movl	$5, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	movq	%rbx, %rcx
	callq	totals
	movl	24(%rsp), %eax
	cmpl	48(%rsp), %eax
	jl	.LBB0_20
# BB#21:                                #   in Loop: Header=BB0_20 Depth=2
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	copy_cost
	cmpl	$0, use_super_gasp(%rip)
	movq	(%rsp), %rdi
	movq	8(%rsp), %rsi
	je	.LBB0_23
# BB#22:                                #   in Loop: Header=BB0_20 Depth=2
	xorl	%eax, %eax
	movq	%r12, %rdx
	movq	%rbx, %rcx
	callq	super_gasp
	movq	%rax, %rcx
	movq	%rcx, (%rsp)
	movl	24(%rsp), %eax
	movl	48(%rsp), %edx
	cmpl	%edx, %eax
	jl	.LBB0_24
	jmp	.LBB0_27
.LBB0_23:                               #   in Loop: Header=BB0_20 Depth=2
	xorl	%eax, %eax
	movq	%r12, %rdx
	movq	%rbx, %rcx
	callq	last_gasp
	movq	%rax, %rcx
	movq	%rcx, (%rsp)
	movl	24(%rsp), %eax
	movl	48(%rsp), %edx
.LBB0_24:                               #   in Loop: Header=BB0_20 Depth=2
	cmpl	%edx, %eax
	jl	.LBB0_20
# BB#25:                                #   in Loop: Header=BB0_20 Depth=2
	jne	.LBB0_27
# BB#26:                                #   in Loop: Header=BB0_20 Depth=2
	movl	40(%rsp), %eax
	cmpl	64(%rsp), %eax
	jl	.LBB0_20
	jmp	.LBB0_27
.LBB0_33:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_free
	movq	(%rsp), %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	espresso, .Lfunc_end0-espresso
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"SIMPLIFY   "
	.size	.L.str, 12

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"SETUP      "
	.size	.L.str.1, 12

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"ADJUST     "
	.size	.L.str.2, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
