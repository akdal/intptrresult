	.text
	.file	"ary.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI0_1:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI0_2:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	cmpl	$2, %edi
	jne	.LBB0_1
# BB#7:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r15
	testl	%r15d, %r15d
	js	.LBB0_9
# BB#8:
	movslq	%r15d, %rbx
	jmp	.LBB0_2
.LBB0_1:
	movl	$9000000, %r15d         # imm = 0x895440
	movl	$9000000, %ebx          # imm = 0x895440
.LBB0_2:                                # %_ZN9__gnu_cxx14__alloc_traitsISaIiEE8allocateERS1_m.exit.i.i.i.i
	leaq	(,%rbx,4), %r12
.Ltmp0:
	movq	%r12, %rdi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp1:
# BB#3:                                 # %_ZN9__gnu_cxx14__alloc_traitsISaIiEE8allocateERS1_m.exit.i.i.i.i30
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	callq	memset
.Ltmp2:
	movq	%r12, %rdi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp3:
# BB#4:                                 # %.lr.ph87.preheader
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	memset
	movl	%r15d, %eax
	cmpq	$8, %rax
	jb	.LBB0_14
# BB#5:                                 # %min.iters.checked
	andl	$7, %r15d
	movq	%rax, %rbp
	subq	%r15, %rbp
	je	.LBB0_6
# BB#11:                                # %vector.body.preheader
	leaq	16(%r13), %rcx
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI0_1(%rip), %xmm1   # xmm1 = [4,4,4,4]
	movdqa	.LCPI0_2(%rip), %xmm2   # xmm2 = [8,8,8,8]
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB0_12:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm3
	paddd	%xmm1, %xmm3
	movdqu	%xmm0, -16(%rcx)
	movdqu	%xmm3, (%rcx)
	paddd	%xmm2, %xmm0
	addq	$32, %rcx
	addq	$-8, %rdx
	jne	.LBB0_12
# BB#13:                                # %middle.block
	testq	%r15, %r15
	jne	.LBB0_14
	jmp	.LBB0_15
.LBB0_6:
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_14:                               # %.lr.ph87
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, (%r13,%rbp,4)
	incq	%rbp
	cmpq	%rbp, %rax
	jne	.LBB0_14
.LBB0_15:                               # %.lr.ph.preheader
	movq	%rbx, %rax
	notq	%rax
	cmpq	$-3, %rax
	movq	$-2, %rcx
	cmovgq	%rax, %rcx
	leaq	2(%rbx,%rcx), %rax
	cmpq	$7, %rax
	movq	%rbx, %rsi
	jbe	.LBB0_16
# BB#21:                                # %min.iters.checked104
	movq	%rax, %rcx
	andq	$-8, %rcx
	movq	%rax, %rdx
	andq	$-8, %rdx
	movq	%rbx, %rsi
	je	.LBB0_16
# BB#22:                                # %vector.body100.preheader
	leaq	-8(%rdx), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	btl	$3, %edi
	jb	.LBB0_23
# BB#24:                                # %vector.body100.prol
	movdqu	-16(%r13,%rbx,4), %xmm0
	movdqu	-32(%r13,%rbx,4), %xmm1
	movdqu	%xmm0, -16(%r14,%rbx,4)
	movq	%rbx, %r8
	movdqu	%xmm1, -32(%r14,%rbx,4)
	movl	$8, %edi
	testq	%rsi, %rsi
	jne	.LBB0_26
	jmp	.LBB0_28
.LBB0_23:
	movq	%rbx, %r8
	xorl	%edi, %edi
	testq	%rsi, %rsi
	je	.LBB0_28
.LBB0_26:                               # %vector.body100.preheader.new
	movq	%rdx, %rsi
	subq	%rdi, %rsi
	leaq	-16(,%r8,4), %rbp
	shlq	$2, %rdi
	subq	%rdi, %rbp
	movq	%r14, %rdi
	addq	%rbp, %rdi
	movq	%r13, %rbx
	addq	%rbp, %rbx
	.p2align	4, 0x90
.LBB0_27:                               # %vector.body100
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm1, (%rdi)
	movups	%xmm0, -16(%rdi)
	movdqu	-48(%rbx), %xmm0
	movdqu	-32(%rbx), %xmm1
	movdqu	%xmm1, -32(%rdi)
	movdqu	%xmm0, -48(%rdi)
	addq	$-64, %rdi
	addq	$-64, %rbx
	addq	$-16, %rsi
	jne	.LBB0_27
.LBB0_28:                               # %middle.block101
	cmpq	%rdx, %rax
	movq	%r8, %rbx
	je	.LBB0_18
# BB#29:
	movq	%rbx, %rsi
	subq	%rcx, %rsi
.LBB0_16:                               # %.lr.ph.preheader118
	incq	%rsi
	.p2align	4, 0x90
.LBB0_17:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-8(%r13,%rsi,4), %eax
	movl	%eax, -8(%r14,%rsi,4)
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB0_17
.LBB0_18:                               # %._crit_edge
	movl	-4(%r14,%rbx,4), %esi
.Ltmp5:
	movl	$_ZSt4cout, %edi
	callq	_ZNSolsEi
	movq	%rax, %r15
.Ltmp6:
# BB#19:
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %r12
	testq	%r12, %r12
	je	.LBB0_20
# BB#33:                                # %.noexc47
	cmpb	$0, 56(%r12)
	je	.LBB0_35
# BB#34:
	movb	67(%r12), %al
	jmp	.LBB0_37
.LBB0_35:
.Ltmp7:
	movq	%r12, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp8:
# BB#36:                                # %.noexc49
	movq	(%r12), %rax
.Ltmp9:
	movl	$10, %esi
	movq	%r12, %rdi
	callq	*48(%rax)
.Ltmp10:
.LBB0_37:                               # %.noexc42
.Ltmp11:
	movsbl	%al, %esi
	movq	%r15, %rdi
	callq	_ZNSo3putEc
.Ltmp12:
# BB#38:                                # %.noexc43
.Ltmp13:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp14:
# BB#39:                                # %_ZNSolsEPFRSoS_E.exit
	movq	%r14, %rdi
	callq	_ZdlPv
	movq	%r13, %rdi
	callq	_ZdlPv
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_20:
.Ltmp15:
	callq	_ZSt16__throw_bad_castv
.Ltmp16:
# BB#32:                                # %.noexc51
.LBB0_9:                                # %.noexc.i.i
.Ltmp18:
	callq	_ZSt17__throw_bad_allocv
.Ltmp19:
# BB#10:                                # %.noexc
.LBB0_40:                               # %_ZNSt6vectorIiSaIiEED2Ev.exit46.thread
.Ltmp4:
	movq	%rax, %r15
	jmp	.LBB0_41
.LBB0_30:
.Ltmp20:
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB0_31:                               # %_ZNSt6vectorIiSaIiEED2Ev.exit46
.Ltmp17:
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	_ZdlPv
.LBB0_41:
	movq	%r13, %rdi
	callq	_ZdlPv
	movq	%r15, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp2-.Ltmp1           #   Call between .Ltmp1 and .Ltmp2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp2-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp2           #   Call between .Ltmp2 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp5-.Ltmp3           #   Call between .Ltmp3 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp16-.Ltmp5          #   Call between .Ltmp5 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Lfunc_end0-.Ltmp19     #   Call between .Ltmp19 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_ary.ii,@function
_GLOBAL__sub_I_ary.ii:                  # @_GLOBAL__sub_I_ary.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end1:
	.size	_GLOBAL__sub_I_ary.ii, .Lfunc_end1-_GLOBAL__sub_I_ary.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_ary.ii

	.ident	"clang version 5.0.0 (https://github.com/aqjune/clang-intptr.git 8e2e88f063bfae95e3be980dbf26473d5a086d27) (https://github.com/aqjune/llvm-intptr.git 51d41dcad277119de970b737044ce4a7e91ad33d)"
	.section	".note.GNU-stack","",@progbits
