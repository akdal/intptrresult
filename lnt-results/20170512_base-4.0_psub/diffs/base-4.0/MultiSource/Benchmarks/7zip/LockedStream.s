	.text
	.file	"LockedStream.bc"
	.globl	_ZN15CLockedInStream4ReadEyPvjPj
	.p2align	4, 0x90
	.type	_ZN15CLockedInStream4ReadEyPvjPj,@function
_ZN15CLockedInStream4ReadEyPvjPj:       # @_ZN15CLockedInStream4ReadEyPvjPj
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movl	%ecx, %r15d
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	leaq	8(%rbp), %r13
	movq	%r13, %rdi
	callq	pthread_mutex_lock
	movq	(%rbp), %rdi
	movq	(%rdi), %rax
.Ltmp0:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	callq	*48(%rax)
	movl	%eax, %ebx
.Ltmp1:
# BB#1:
	testl	%ebx, %ebx
	jne	.LBB0_3
# BB#2:
	movq	(%rbp), %rdi
	movq	(%rdi), %rax
.Ltmp2:
	movq	%r12, %rsi
	movl	%r15d, %edx
	movq	%r14, %rcx
	callq	*40(%rax)
	movl	%eax, %ebx
.Ltmp3:
.LBB0_3:
	movq	%r13, %rdi
	callq	pthread_mutex_unlock
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_4:
.Ltmp4:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	pthread_mutex_unlock
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZN15CLockedInStream4ReadEyPvjPj, .Lfunc_end0-_ZN15CLockedInStream4ReadEyPvjPj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end0-.Ltmp3      #   Call between .Ltmp3 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN28CLockedSequentialInStreamImp4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN28CLockedSequentialInStreamImp4ReadEPvjPj,@function
_ZN28CLockedSequentialInStreamImp4ReadEPvjPj: # @_ZN28CLockedSequentialInStreamImp4ReadEPvjPj
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movl	$0, (%rsp)
	movq	16(%rbx), %r12
	movq	24(%rbx), %rbp
	leaq	8(%r12), %r15
	movq	%r15, %rdi
	callq	pthread_mutex_lock
	movq	(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp5:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbp, %rsi
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp6:
# BB#1:
	testl	%ebp, %ebp
	jne	.LBB1_3
# BB#2:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp7:
	movq	%rsp, %rcx
	movq	%r13, %rsi
	movl	4(%rsp), %edx           # 4-byte Reload
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp8:
.LBB1_3:                                # %_ZN15CLockedInStream4ReadEyPvjPj.exit
	movq	%r15, %rdi
	callq	pthread_mutex_unlock
	movl	(%rsp), %eax
	addq	%rax, 24(%rbx)
	testq	%r14, %r14
	je	.LBB1_5
# BB#4:
	movl	%eax, (%r14)
.LBB1_5:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_6:
.Ltmp9:
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	pthread_mutex_unlock
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZN28CLockedSequentialInStreamImp4ReadEPvjPj, .Lfunc_end1-_ZN28CLockedSequentialInStreamImp4ReadEPvjPj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp5-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp8-.Ltmp5           #   Call between .Ltmp5 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin1    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Lfunc_end1-.Ltmp8      #   Call between .Ltmp8 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN28CLockedSequentialInStreamImp14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN28CLockedSequentialInStreamImp14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN28CLockedSequentialInStreamImp14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN28CLockedSequentialInStreamImp14QueryInterfaceERK4GUIDPPv,@function
_ZN28CLockedSequentialInStreamImp14QueryInterfaceERK4GUIDPPv: # @_ZN28CLockedSequentialInStreamImp14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB2_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB2_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB2_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB2_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB2_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB2_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB2_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB2_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB2_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB2_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB2_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB2_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB2_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB2_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB2_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB2_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB2_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end2:
	.size	_ZN28CLockedSequentialInStreamImp14QueryInterfaceERK4GUIDPPv, .Lfunc_end2-_ZN28CLockedSequentialInStreamImp14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN28CLockedSequentialInStreamImp6AddRefEv,"axG",@progbits,_ZN28CLockedSequentialInStreamImp6AddRefEv,comdat
	.weak	_ZN28CLockedSequentialInStreamImp6AddRefEv
	.p2align	4, 0x90
	.type	_ZN28CLockedSequentialInStreamImp6AddRefEv,@function
_ZN28CLockedSequentialInStreamImp6AddRefEv: # @_ZN28CLockedSequentialInStreamImp6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end3:
	.size	_ZN28CLockedSequentialInStreamImp6AddRefEv, .Lfunc_end3-_ZN28CLockedSequentialInStreamImp6AddRefEv
	.cfi_endproc

	.section	.text._ZN28CLockedSequentialInStreamImp7ReleaseEv,"axG",@progbits,_ZN28CLockedSequentialInStreamImp7ReleaseEv,comdat
	.weak	_ZN28CLockedSequentialInStreamImp7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN28CLockedSequentialInStreamImp7ReleaseEv,@function
_ZN28CLockedSequentialInStreamImp7ReleaseEv: # @_ZN28CLockedSequentialInStreamImp7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB4_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB4_2:
	popq	%rcx
	retq
.Lfunc_end4:
	.size	_ZN28CLockedSequentialInStreamImp7ReleaseEv, .Lfunc_end4-_ZN28CLockedSequentialInStreamImp7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8IUnknownD2Ev,"axG",@progbits,_ZN8IUnknownD2Ev,comdat
	.weak	_ZN8IUnknownD2Ev
	.p2align	4, 0x90
	.type	_ZN8IUnknownD2Ev,@function
_ZN8IUnknownD2Ev:                       # @_ZN8IUnknownD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end5:
	.size	_ZN8IUnknownD2Ev, .Lfunc_end5-_ZN8IUnknownD2Ev
	.cfi_endproc

	.section	.text._ZN28CLockedSequentialInStreamImpD0Ev,"axG",@progbits,_ZN28CLockedSequentialInStreamImpD0Ev,comdat
	.weak	_ZN28CLockedSequentialInStreamImpD0Ev
	.p2align	4, 0x90
	.type	_ZN28CLockedSequentialInStreamImpD0Ev,@function
_ZN28CLockedSequentialInStreamImpD0Ev:  # @_ZN28CLockedSequentialInStreamImpD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end6:
	.size	_ZN28CLockedSequentialInStreamImpD0Ev, .Lfunc_end6-_ZN28CLockedSequentialInStreamImpD0Ev
	.cfi_endproc

	.type	_ZTV28CLockedSequentialInStreamImp,@object # @_ZTV28CLockedSequentialInStreamImp
	.section	.rodata,"a",@progbits
	.globl	_ZTV28CLockedSequentialInStreamImp
	.p2align	3
_ZTV28CLockedSequentialInStreamImp:
	.quad	0
	.quad	_ZTI28CLockedSequentialInStreamImp
	.quad	_ZN28CLockedSequentialInStreamImp14QueryInterfaceERK4GUIDPPv
	.quad	_ZN28CLockedSequentialInStreamImp6AddRefEv
	.quad	_ZN28CLockedSequentialInStreamImp7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN28CLockedSequentialInStreamImpD0Ev
	.quad	_ZN28CLockedSequentialInStreamImp4ReadEPvjPj
	.size	_ZTV28CLockedSequentialInStreamImp, 64

	.type	_ZTS28CLockedSequentialInStreamImp,@object # @_ZTS28CLockedSequentialInStreamImp
	.globl	_ZTS28CLockedSequentialInStreamImp
	.p2align	4
_ZTS28CLockedSequentialInStreamImp:
	.asciz	"28CLockedSequentialInStreamImp"
	.size	_ZTS28CLockedSequentialInStreamImp, 31

	.type	_ZTS19ISequentialInStream,@object # @_ZTS19ISequentialInStream
	.section	.rodata._ZTS19ISequentialInStream,"aG",@progbits,_ZTS19ISequentialInStream,comdat
	.weak	_ZTS19ISequentialInStream
	.p2align	4
_ZTS19ISequentialInStream:
	.asciz	"19ISequentialInStream"
	.size	_ZTS19ISequentialInStream, 22

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI19ISequentialInStream,@object # @_ZTI19ISequentialInStream
	.section	.rodata._ZTI19ISequentialInStream,"aG",@progbits,_ZTI19ISequentialInStream,comdat
	.weak	_ZTI19ISequentialInStream
	.p2align	4
_ZTI19ISequentialInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ISequentialInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19ISequentialInStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI28CLockedSequentialInStreamImp,@object # @_ZTI28CLockedSequentialInStreamImp
	.section	.rodata,"a",@progbits
	.globl	_ZTI28CLockedSequentialInStreamImp
	.p2align	4
_ZTI28CLockedSequentialInStreamImp:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS28CLockedSequentialInStreamImp
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI19ISequentialInStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI28CLockedSequentialInStreamImp, 56


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
