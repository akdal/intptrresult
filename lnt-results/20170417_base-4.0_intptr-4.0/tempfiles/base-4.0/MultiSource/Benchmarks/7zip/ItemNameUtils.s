	.text
	.file	"ItemNameUtils.bc"
	.globl	_ZN8NArchive9NItemName13MakeLegalNameERK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NArchive9NItemName13MakeLegalNameERK11CStringBaseIwE,@function
_ZN8NArchive9NItemName13MakeLegalNameERK11CStringBaseIwE: # @_ZN8NArchive9NItemName13MakeLegalNameERK11CStringBaseIwE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
	movslq	8(%r14), %r12
	leaq	1(%r12), %rbx
	testl	%ebx, %ebx
	je	.LBB0_1
# BB#2:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r15)
	movl	$0, (%rax)
	movl	%ebx, 12(%r15)
	jmp	.LBB0_3
.LBB0_1:
	xorl	%eax, %eax
.LBB0_3:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r14), %rcx
	.p2align	4, 0x90
.LBB0_4:                                # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB0_4
# BB#5:
	movl	%r12d, 8(%r15)
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	_ZN8NArchive9NItemName13MakeLegalNameERK11CStringBaseIwE, .Lfunc_end0-_ZN8NArchive9NItemName13MakeLegalNameERK11CStringBaseIwE
	.cfi_endproc

	.globl	_ZN8NArchive9NItemName9GetOSNameERK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NArchive9NItemName9GetOSNameERK11CStringBaseIwE,@function
_ZN8NArchive9NItemName9GetOSNameERK11CStringBaseIwE: # @_ZN8NArchive9NItemName9GetOSNameERK11CStringBaseIwE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r12, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
	movslq	8(%r14), %r12
	leaq	1(%r12), %rbx
	testl	%ebx, %ebx
	je	.LBB1_1
# BB#2:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r15)
	movl	$0, (%rax)
	movl	%ebx, 12(%r15)
	jmp	.LBB1_3
.LBB1_1:
	xorl	%eax, %eax
.LBB1_3:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r14), %rcx
	.p2align	4, 0x90
.LBB1_4:                                # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB1_4
# BB#5:
	movl	%r12d, 8(%r15)
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_ZN8NArchive9NItemName9GetOSNameERK11CStringBaseIwE, .Lfunc_end1-_ZN8NArchive9NItemName9GetOSNameERK11CStringBaseIwE
	.cfi_endproc

	.globl	_ZN8NArchive9NItemName10GetOSName2ERK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NArchive9NItemName10GetOSName2ERK11CStringBaseIwE,@function
_ZN8NArchive9NItemName10GetOSName2ERK11CStringBaseIwE: # @_ZN8NArchive9NItemName10GetOSName2ERK11CStringBaseIwE
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 48
.Lcfi23:
	.cfi_offset %rbx, -48
.Lcfi24:
	.cfi_offset %r12, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movslq	8(%r15), %r12
	testq	%r12, %r12
	je	.LBB2_1
# BB#2:
	movl	%r12d, %eax
	incl	%eax
	je	.LBB2_3
# BB#4:                                 # %._crit_edge16.i.i.i
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	movl	$0, (%rbx)
	jmp	.LBB2_5
.LBB2_1:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	$4, 12(%r14)
	jmp	.LBB2_14
.LBB2_3:
	xorl	%ebx, %ebx
.LBB2_5:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movq	(%r15), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_6:                                # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rcx), %edx
	movl	%edx, (%rbx,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB2_6
# BB#7:                                 # %_ZN8NArchive9NItemName9GetOSNameERK11CStringBaseIwE.exit
	cmpl	$47, -4(%rbx,%r12,4)
	jne	.LBB2_9
# BB#8:                                 # %_ZN11CStringBaseIwE6DeleteEii.exit
	movl	(%rbx,%r12,4), %eax
	movl	%eax, -4(%rbx,%r12,4)
	decq	%r12
                                        # kill: %R12D<def> %R12D<kill> %R12<kill> %R12<def>
.LBB2_9:                                # %._crit_edge16.i.i
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	leal	1(%r12), %ebp
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp0:
	callq	_Znam
.Ltmp1:
# BB#10:                                # %.noexc
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%ebp, 12(%r14)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_11:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB2_11
# BB#12:
	movl	%r12d, 8(%r14)
	testq	%rbx, %rbx
	je	.LBB2_14
# BB#13:
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB2_14:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_15:                               # %_ZN11CStringBaseIwED2Ev.exit3
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN8NArchive9NItemName10GetOSName2ERK11CStringBaseIwE, .Lfunc_end2-_ZN8NArchive9NItemName10GetOSName2ERK11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end2-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NArchive9NItemName12HasTailSlashERK11CStringBaseIcEj
	.p2align	4, 0x90
	.type	_ZN8NArchive9NItemName12HasTailSlashERK11CStringBaseIcEj,@function
_ZN8NArchive9NItemName12HasTailSlashERK11CStringBaseIcEj: # @_ZN8NArchive9NItemName12HasTailSlashERK11CStringBaseIcEj
	.cfi_startproc
# BB#0:
	movslq	8(%rdi), %rax
	testq	%rax, %rax
	je	.LBB3_1
# BB#2:
	movq	(%rdi), %rcx
	cmpb	$47, -1(%rcx,%rax)
	sete	%al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB3_1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end3:
	.size	_ZN8NArchive9NItemName12HasTailSlashERK11CStringBaseIcEj, .Lfunc_end3-_ZN8NArchive9NItemName12HasTailSlashERK11CStringBaseIcEj
	.cfi_endproc

	.globl	_ZN8NArchive9NItemName15WinNameToOSNameERK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NArchive9NItemName15WinNameToOSNameERK11CStringBaseIwE,@function
_ZN8NArchive9NItemName15WinNameToOSNameERK11CStringBaseIwE: # @_ZN8NArchive9NItemName15WinNameToOSNameERK11CStringBaseIwE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 48
.Lcfi33:
	.cfi_offset %rbx, -40
.Lcfi34:
	.cfi_offset %r12, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movslq	8(%r15), %r12
	leaq	1(%r12), %rbx
	testl	%ebx, %ebx
	je	.LBB4_1
# BB#2:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%ebx, 12(%r14)
	jmp	.LBB4_3
.LBB4_1:
	xorl	%eax, %eax
.LBB4_3:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB4_4:                                # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %esi
	addq	$4, %rcx
	movl	%esi, (%rdx)
	addq	$4, %rdx
	testl	%esi, %esi
	jne	.LBB4_4
# BB#5:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit
	movl	%r12d, 8(%r14)
	testl	%r12d, %r12d
	jle	.LBB4_13
# BB#6:                                 # %.lr.ph.i
	xorl	%edx, %edx
.LBB4_7:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_9 Depth 2
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,4), %rcx
	movl	(%rax,%rdx,4), %edx
	cmpl	$92, %edx
	jne	.LBB4_9
	jmp	.LBB4_11
	.p2align	4, 0x90
.LBB4_10:                               #   in Loop: Header=BB4_9 Depth=2
	movl	4(%rcx), %edx
	addq	$4, %rcx
	cmpl	$92, %edx
	je	.LBB4_11
.LBB4_9:                                # %.lr.ph.i.i
                                        #   Parent Loop BB4_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%edx, %edx
	jne	.LBB4_10
	jmp	.LBB4_13
	.p2align	4, 0x90
.LBB4_11:                               # %_ZNK11CStringBaseIwE4FindEwi.exit.i
                                        #   in Loop: Header=BB4_7 Depth=1
	subq	%rax, %rcx
	movq	%rcx, %rdx
	shrq	$2, %rdx
	testl	%edx, %edx
	js	.LBB4_13
# BB#12:                                #   in Loop: Header=BB4_7 Depth=1
	shlq	$30, %rcx
	sarq	$30, %rcx
	movl	$47, (%rax,%rcx)
	incl	%edx
	cmpl	%r12d, %edx
	jl	.LBB4_7
.LBB4_13:                               # %_ZN11CStringBaseIwE7ReplaceEww.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	_ZN8NArchive9NItemName15WinNameToOSNameERK11CStringBaseIwE, .Lfunc_end4-_ZN8NArchive9NItemName15WinNameToOSNameERK11CStringBaseIwE
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
