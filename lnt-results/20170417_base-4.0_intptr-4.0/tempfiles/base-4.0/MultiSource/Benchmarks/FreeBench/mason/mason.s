	.text
	.file	"mason.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 80
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	movl	$.L.str.3, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	8(%rbx), %rdi
	movl	$.L.str.4, %esi
	callq	fopen
	movq	%rax, %rdi
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#1:
	leaq	12(%rsp), %rcx
	leaq	16(%rsp), %r8
	leaq	20(%rsp), %r9
	leaq	24(%rsp), %r10
	leaq	28(%rsp), %r11
	leaq	32(%rsp), %r14
	leaq	36(%rsp), %r15
	leaq	40(%rsp), %rbx
	subq	$8, %rsp
.Lcfi7:
	.cfi_adjust_cfa_offset 8
	leaq	16(%rsp), %rdx
	movl	$.L.str.6, %esi
	movl	$0, %eax
	pushq	%rbx
.Lcfi8:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi9:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi10:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi11:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi12:
	.cfi_adjust_cfa_offset 8
	callq	fscanf
	addq	$48, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset -48
	movl	$2, %ebx
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	subq	$48, %rsp
.Lcfi14:
	.cfi_adjust_cfa_offset 48
	movl	88(%rsp), %eax
	movl	%eax, 32(%rsp)
	movups	56(%rsp), %xmm0
	movups	72(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	xorl	%esi, %esi
	movl	$-1, %edx
	movl	%ebx, %edi
	callq	mu
	addq	$48, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -48
	addl	$2, %ebx
	cmpl	$1, %eax
	jne	.LBB0_2
# BB#3:
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	xorl	%eax, %eax
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB0_4:
	movq	stderr(%rip), %rdi
	movq	(%rbx), %rdx
	movq	8(%rbx), %rcx
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.p2align	4, 0x90
	.type	mu,@function
mu:                                     # @mu
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 176
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %r14d
	movl	%edi, %r13d
	leaq	176(%rsp), %rbx
	leal	-4(%r13), %eax
	cmpl	%r14d, %eax
	jne	.LBB1_10
# BB#1:
	xorl	%ebp, %ebp
	movzbl	4(%rbx), %eax
	movl	$51087, %ecx            # imm = 0xC78F
	btq	%rax, %rcx
	jb	.LBB1_35
# BB#2:
	movzbl	12(%rbx), %eax
	movl	$115599, %ecx           # imm = 0x1C38F
	btq	%rax, %rcx
	jb	.LBB1_35
# BB#3:
	movzbl	16(%rbx), %eax
	movl	$243824, %ecx           # imm = 0x3B870
	btq	%rax, %rcx
	jb	.LBB1_35
# BB#4:
	movzbl	20(%rbx), %eax
	movl	$155761, %ecx           # imm = 0x26071
	btq	%rax, %rcx
	jb	.LBB1_35
# BB#5:
	movl	(%rbx), %eax
	movl	$50055, %ecx            # imm = 0xC387
	btq	%rax, %rcx
	jb	.LBB1_35
# BB#6:
	movzbl	8(%rbx), %ecx
	movl	$19335, %edx            # imm = 0x4B87
	btq	%rcx, %rdx
	jb	.LBB1_35
# BB#7:
	movzbl	24(%rbx), %ecx
	movl	$145520, %edx           # imm = 0x23870
	btq	%rcx, %rdx
	jb	.LBB1_35
# BB#8:
	movzbl	28(%rbx), %ecx
	movl	$145520, %edx           # imm = 0x23870
	btq	%rcx, %rdx
	jb	.LBB1_35
# BB#9:
	movzbl	32(%rbx), %ecx
	movl	$145520, %edx           # imm = 0x23870
	btq	%rcx, %rdx
	jae	.LBB1_11
	jmp	.LBB1_35
.LBB1_10:                               # %._crit_edge
	movl	(%rbx), %eax
.LBB1_11:
	cmpl	$5, %eax
	jne	.LBB1_19
# BB#12:
	cmpl	$6, 4(%rbx)
	jne	.LBB1_19
# BB#13:
	cmpl	$12, 8(%rbx)
	jne	.LBB1_19
# BB#14:
	cmpl	$13, 12(%rbx)
	jne	.LBB1_19
# BB#15:
	cmpl	$14, 16(%rbx)
	jne	.LBB1_19
# BB#16:
	cmpl	$15, 20(%rbx)
	jne	.LBB1_19
# BB#17:
	movl	24(%rbx), %esi
	movl	28(%rbx), %edx
	leal	(%rdx,%rsi), %edi
	movl	32(%rbx), %ecx
	addl	%ecx, %edi
	cmpl	$3, %edi
	jne	.LBB1_19
# BB#18:
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	printf
	movl	16(%rbx), %esi
	movl	20(%rbx), %edx
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB1_34
.LBB1_19:
	xorl	%ebp, %ebp
	cmpl	%r14d, %r13d
	jle	.LBB1_35
# BB#20:
	testl	%r15d, %r15d
	je	.LBB1_23
# BB#21:
	cltq
	movl	.Lm0u.m(,%rax,4), %r8d
	movslq	4(%rbx), %rcx
	movl	.Lm0u.m(,%rcx,4), %r9d
	movslq	8(%rbx), %rdx
	movl	.Lm0u.m(,%rdx,4), %r10d
	movslq	12(%rbx), %rsi
	movl	.Lm0u.m(,%rsi,4), %r11d
	movslq	16(%rbx), %rdi
	movl	.Lm0u.m(,%rdi,4), %edi
	movslq	20(%rbx), %rax
	movl	.Lm0u.m(,%rax,4), %eax
	movslq	24(%rbx), %rcx
	movl	.Lm0u.m(,%rcx,4), %ecx
	movslq	28(%rbx), %rdx
	movl	.Lm0u.m(,%rdx,4), %edx
	movslq	32(%rbx), %rsi
	movl	.Lm0u.m(,%rsi,4), %esi
	movl	%r8d, 80(%rsp)
	movl	%r9d, 84(%rsp)
	movl	%r10d, 88(%rsp)
	movl	%r11d, 92(%rsp)
	movl	%edi, 96(%rsp)
	movl	%eax, 100(%rsp)
	movl	%ecx, 104(%rsp)
	movl	%edx, 108(%rsp)
	movl	%esi, 112(%rsp)
	leal	1(%r14), %r12d
	movl	112(%rsp), %eax
	movl	%eax, 32(%rsp)
	movups	80(%rsp), %xmm0
	movups	96(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	xorl	%edx, %edx
	movl	%r13d, %edi
	movl	%r12d, %esi
	callq	md
	cmpl	$1, %eax
	jne	.LBB1_24
# BB#22:
	movq	stdout(%rip), %rsi
	movl	$48, %edi
	jmp	.LBB1_31
.LBB1_23:                               # %..thread77_crit_edge
	leal	1(%r14), %r12d
	jmp	.LBB1_25
.LBB1_24:
	cmpl	$1, %r15d
	je	.LBB1_29
.LBB1_25:                               # %.thread77
	movl	32(%rbx), %eax
	movl	%eax, 32(%rsp)
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$1, %edx
	movl	%r13d, %edi
	movl	%r12d, %esi
	callq	md
	cmpl	$1, %eax
	jne	.LBB1_28
# BB#26:
	movq	stdout(%rip), %rsi
	movl	$49, %edi
	callq	_IO_putc
	testb	$3, %r14b
	jne	.LBB1_34
# BB#33:
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
.LBB1_34:                               # %.thread80
	movl	$1, %ebp
	jmp	.LBB1_35
.LBB1_28:
	cmpl	$2, %r15d
	je	.LBB1_35
.LBB1_29:                               # %..thread79_crit_edge
	leaq	8(%rbx), %rbp
	leaq	12(%rbx), %rdi
	leaq	16(%rbx), %rsi
	leaq	20(%rbx), %rdx
	leaq	24(%rbx), %r9
	leaq	28(%rbx), %r8
	leaq	4(%rbx), %rcx
	movq	%rbx, %rax
	addq	$32, %rax
	movslq	(%rbx), %rbx
	movl	.Lm2u.m(,%rbx,4), %r10d
	movslq	(%rcx), %rcx
	movl	.Lm2u.m(,%rcx,4), %r11d
	movslq	(%rbp), %rbp
	movl	.Lm2u.m(,%rbp,4), %ebp
	movslq	(%rdi), %rdi
	movl	.Lm2u.m(,%rdi,4), %edi
	movslq	(%rsi), %rsi
	movl	.Lm2u.m(,%rsi,4), %esi
	movslq	(%rdx), %rdx
	movl	.Lm2u.m(,%rdx,4), %edx
	movslq	(%r9), %rbx
	movl	.Lm2u.m(,%rbx,4), %ebx
	movslq	(%r8), %rcx
	movl	.Lm2u.m(,%rcx,4), %ecx
	movslq	(%rax), %rax
	movl	.Lm2u.m(,%rax,4), %eax
	movl	%r10d, 40(%rsp)
	movl	%r11d, 44(%rsp)
	movl	%ebp, 48(%rsp)
	movl	%edi, 52(%rsp)
	movl	%esi, 56(%rsp)
	movl	%edx, 60(%rsp)
	movl	%ebx, 64(%rsp)
	movl	%ecx, 68(%rsp)
	movl	%eax, 72(%rsp)
	movl	72(%rsp), %eax
	movl	%eax, 32(%rsp)
	movups	40(%rsp), %xmm0
	movups	56(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$2, %edx
	movl	%r13d, %edi
	movl	%r12d, %esi
	callq	md
	movl	%eax, %ebp
	cmpl	$1, %ebp
	jne	.LBB1_35
# BB#30:
	movq	stdout(%rip), %rsi
	movl	$50, %edi
.LBB1_31:
	callq	_IO_putc
	movl	$1, %ebp
	testb	$3, %r14b
	jne	.LBB1_35
# BB#32:
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
.LBB1_35:                               # %.thread80
	movl	%ebp, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	mu, .Lfunc_end1-mu
	.cfi_endproc

	.p2align	4, 0x90
	.type	md,@function
md:                                     # @md
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 192
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movl	%esi, %r15d
	movl	%edi, %r12d
	xorl	%ebx, %ebx
	cmpl	%r15d, %r12d
	jle	.LBB2_18
# BB#1:
	leaq	192(%rsp), %rbp
	testl	%r13d, %r13d
	je	.LBB2_4
# BB#2:
	movslq	(%rbp), %rax
	movslq	4(%rbp), %rcx
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	.Lm0d.m(,%rax,4), %r8d
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movl	.Lm0d.m(,%rcx,4), %r9d
	movslq	8(%rbp), %rdx
	movl	.Lm0d.m(,%rdx,4), %r10d
	movslq	12(%rbp), %rsi
	movl	.Lm0d.m(,%rsi,4), %r11d
	movslq	16(%rbp), %rdi
	movl	.Lm0d.m(,%rdi,4), %edi
	movslq	20(%rbp), %rax
	movl	.Lm0d.m(,%rax,4), %eax
	movslq	24(%rbp), %rcx
	movl	.Lm0d.m(,%rcx,4), %ecx
	movslq	28(%rbp), %rdx
	movl	.Lm0d.m(,%rdx,4), %edx
	movslq	32(%rbp), %rsi
	movl	.Lm0d.m(,%rsi,4), %esi
	movl	%r8d, 80(%rsp)
	movl	%r9d, 84(%rsp)
	movl	%r10d, 88(%rsp)
	movl	%r11d, 92(%rsp)
	movl	%edi, 96(%rsp)
	movl	%eax, 100(%rsp)
	movl	%ecx, 104(%rsp)
	movl	%edx, 108(%rsp)
	movl	%esi, 112(%rsp)
	leal	1(%r15), %r14d
	movl	112(%rsp), %eax
	movl	%eax, 32(%rsp)
	movups	80(%rsp), %xmm0
	movups	96(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	xorl	%edx, %edx
	movl	%r12d, %edi
	movl	%r14d, %esi
	callq	mu
	cmpl	$1, %eax
	jne	.LBB2_5
# BB#3:
	movq	stdout(%rip), %rsi
	movl	$48, %edi
	jmp	.LBB2_14
.LBB2_4:                                # %..thread75_crit_edge
	leal	1(%r15), %r14d
	jmp	.LBB2_7
.LBB2_5:
	cmpl	$1, %r13d
	jne	.LBB2_7
# BB#6:
	leaq	12(%rbp), %rdi
	leaq	16(%rbp), %rsi
	leaq	20(%rbp), %rdx
	leaq	24(%rbp), %r9
	leaq	28(%rbp), %r8
	leaq	8(%rbp), %rbx
	addq	$32, %rbp
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	120(%rsp), %rcx         # 8-byte Reload
	jmp	.LBB2_12
.LBB2_7:                                # %.thread75
	movl	32(%rbp), %eax
	movl	%eax, 32(%rsp)
	movups	(%rbp), %xmm0
	movups	16(%rbp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$1, %edx
	movl	%r12d, %edi
	movl	%r14d, %esi
	callq	mu
	cmpl	$1, %eax
	jne	.LBB2_10
# BB#8:
	movq	stdout(%rip), %rsi
	movl	$49, %edi
	callq	_IO_putc
	testb	$3, %r15b
	jne	.LBB2_17
# BB#16:
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
.LBB2_17:                               # %.thread78
	movl	$1, %ebx
	jmp	.LBB2_18
.LBB2_10:
	cmpl	$2, %r13d
	je	.LBB2_18
# BB#11:                                # %..thread77_crit_edge
	movl	(%rbp), %eax
	movl	4(%rbp), %ecx
	leaq	12(%rbp), %rdi
	leaq	16(%rbp), %rsi
	leaq	20(%rbp), %rdx
	leaq	24(%rbp), %r9
	leaq	28(%rbp), %r8
	leaq	8(%rbp), %rbx
	addq	$32, %rbp
.LBB2_12:                               # %.thread77
	cltq
	movl	.Lm2d.m(,%rax,4), %r10d
	movslq	%ecx, %rcx
	movl	.Lm2d.m(,%rcx,4), %r11d
	movslq	(%rbx), %rbx
	movl	.Lm2d.m(,%rbx,4), %ebx
	movslq	(%rdi), %rdi
	movl	.Lm2d.m(,%rdi,4), %edi
	movslq	(%rsi), %rsi
	movl	.Lm2d.m(,%rsi,4), %esi
	movslq	(%rdx), %rdx
	movl	.Lm2d.m(,%rdx,4), %edx
	movslq	(%r9), %rax
	movl	.Lm2d.m(,%rax,4), %eax
	movslq	(%r8), %rcx
	movl	.Lm2d.m(,%rcx,4), %ecx
	movslq	(%rbp), %rbp
	movl	.Lm2d.m(,%rbp,4), %ebp
	movl	%r10d, 40(%rsp)
	movl	%r11d, 44(%rsp)
	movl	%ebx, 48(%rsp)
	movl	%edi, 52(%rsp)
	movl	%esi, 56(%rsp)
	movl	%edx, 60(%rsp)
	movl	%eax, 64(%rsp)
	movl	%ecx, 68(%rsp)
	movl	%ebp, 72(%rsp)
	movl	72(%rsp), %eax
	movl	%eax, 32(%rsp)
	movups	40(%rsp), %xmm0
	movups	56(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$2, %edx
	movl	%r12d, %edi
	movl	%r14d, %esi
	callq	mu
	movl	%eax, %ebx
	cmpl	$1, %ebx
	jne	.LBB2_18
# BB#13:
	movq	stdout(%rip), %rsi
	movl	$50, %edi
.LBB2_14:
	callq	_IO_putc
	movl	$1, %ebx
	testb	$3, %r15b
	jne	.LBB2_18
# BB#15:
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
.LBB2_18:                               # %.thread78
	movl	%ebx, %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	md, .Lfunc_end2-md
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Compile date: %s\n"
	.size	.L.str, 18

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"today"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Compiler switches: %s\n"
	.size	.L.str.2, 23

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.zero	1
	.size	.L.str.3, 1

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"r"
	.size	.L.str.4, 2

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"ERROR in %s: Could not open datafile %s\n"
	.size	.L.str.5, 41

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%d %d %d %d %d %d %d %d %d"
	.size	.L.str.6, 27

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Trying %d\n"
	.size	.L.str.7, 11

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Gul: %d %d %d\n"
	.size	.L.str.8, 15

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"bin+art: %d %d\n"
	.size	.L.str.9, 16

	.type	.Lm0d.m,@object         # @m0d.m
	.section	.rodata,"a",@progbits
	.p2align	4
.Lm0d.m:
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	5                       # 0x5
	.long	17                      # 0x11
	.long	7                       # 0x7
	.long	1                       # 0x1
	.long	9                       # 0x9
	.long	15                      # 0xf
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	14                      # 0xe
	.long	4                       # 0x4
	.long	16                      # 0x10
	.size	.Lm0d.m, 72

	.type	.Lm2d.m,@object         # @m2d.m
	.p2align	4
.Lm2d.m:
	.long	7                       # 0x7
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	16                      # 0x10
	.long	4                       # 0x4
	.long	12                      # 0xc
	.long	6                       # 0x6
	.long	14                      # 0xe
	.long	8                       # 0x8
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	15                      # 0xf
	.long	9                       # 0x9
	.long	17                      # 0x11
	.long	5                       # 0x5
	.size	.Lm2d.m, 72

	.type	.Lm0u.m,@object         # @m0u.m
	.p2align	4
.Lm0u.m:
	.long	0                       # 0x0
	.long	8                       # 0x8
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	16                      # 0x10
	.long	5                       # 0x5
	.long	13                      # 0xd
	.long	7                       # 0x7
	.long	14                      # 0xe
	.long	9                       # 0x9
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	15                      # 0xf
	.long	10                      # 0xa
	.long	17                      # 0x11
	.long	6                       # 0x6
	.size	.Lm0u.m, 72

	.type	.Lm2u.m,@object         # @m2u.m
	.p2align	4
.Lm2u.m:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	4                       # 0x4
	.long	17                      # 0x11
	.long	6                       # 0x6
	.long	0                       # 0x0
	.long	8                       # 0x8
	.long	15                      # 0xf
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	5                       # 0x5
	.long	13                      # 0xd
	.long	7                       # 0x7
	.long	14                      # 0xe
	.long	3                       # 0x3
	.long	16                      # 0x10
	.size	.Lm2u.m, 72


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
