	.text
	.file	"io.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4618216237887075123     # double 5.7999999999999998
.LCPI0_1:
	.quad	4648488871632306176     # double 600
	.text
	.globl	putlocalhom3
	.p2align	4, 0x90
	.type	putlocalhom3,@function
putlocalhom3:                           # @putlocalhom3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movl	%ecx, %r13d
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rdx, %rax
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	movq	8(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB0_1
# BB#2:
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	fprintf
	movb	(%rbp), %cl
	testb	%cl, %cl
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	je	.LBB0_3
# BB#4:                                 # %.lr.ph.preheader
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	xorl	%r12d, %r12d
	movl	$0, %eax
	movq	%rbx, %rdi
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	xorpd	%xmm1, %xmm1
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$1, %eax
	jne	.LBB0_14
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=1
	cmpb	$45, %cl
	je	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_5 Depth=1
	cmpb	$45, (%r15)
	jne	.LBB0_14
.LBB0_8:                                #   in Loop: Header=BB0_5 Depth=1
	leal	-1(%r13), %esi
	leal	-1(%rbx), %r14d
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdx)
	testl	%eax, %eax
	jle	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_5 Depth=1
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movl	$1, %edi
	movl	%r9d, 60(%rsp)          # 4-byte Spill
	movl	%esi, 52(%rsp)          # 4-byte Spill
	movl	$80, %esi
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	movl	%r8d, 56(%rsp)          # 4-byte Spill
	callq	calloc
	movl	52(%rsp), %esi          # 4-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movsd	32(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	60(%rsp), %r9d          # 4-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 8(%rcx)
	movq	$0, 8(%rax)
	movq	%rax, %rdi
.LBB0_10:                               #   in Loop: Header=BB0_5 Depth=1
	movl	%r9d, 24(%rdi)
	movl	%r8d, 32(%rdi)
	movl	%esi, 28(%rdi)
	movl	%r14d, 36(%rdi)
	cmpl	$0, divpairscore(%rip)
	je	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_5 Depth=1
	subl	%r8d, %r14d
	incl	%r14d
	movl	%r14d, 48(%rdi)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r14d, %xmm0
	divsd	%xmm0, %xmm1
	mulsd	.LCPI0_0(%rip), %xmm1
	divsd	.LCPI0_1(%rip), %xmm1
	movsd	%xmm1, 40(%rdi)
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_14:                               #   in Loop: Header=BB0_5 Depth=1
	cmpb	$45, %cl
	je	.LBB0_17
# BB#15:                                #   in Loop: Header=BB0_5 Depth=1
	movsbq	(%r15), %rdx
	cmpq	$45, %rdx
	je	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_5 Depth=1
	testl	%eax, %eax
	cmovel	%r13d, %r9d
	cmovel	%ebx, %r8d
	movl	$1, %esi
	cmovel	%esi, %eax
	movsbq	%cl, %rcx
	movslq	amino_n(,%rcx,4), %rcx
	movslq	amino_n(,%rdx,4), %rdx
	imulq	$104, %rcx, %rcx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	n_dis(%rcx,%rdx,4), %xmm0
	addsd	%xmm0, %xmm1
	jmp	.LBB0_17
.LBB0_12:                               #   in Loop: Header=BB0_5 Depth=1
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	addl	%ebx, %r12d
	addl	%r12d, 12(%rsp)         # 4-byte Folded Spill
.LBB0_13:                               #   in Loop: Header=BB0_5 Depth=1
	xorl	%eax, %eax
	xorpd	%xmm1, %xmm1
.LBB0_17:                               #   in Loop: Header=BB0_5 Depth=1
	xorl	%edx, %edx
	cmpb	$45, (%rbp)
	movzbl	1(%rbp), %ecx
	leaq	1(%rbp), %rbp
	setne	%dl
	addl	%edx, %r13d
	xorl	%edx, %edx
	cmpb	$45, (%r15)
	leaq	1(%r15), %r15
	setne	%dl
	addl	%edx, %ebx
	movl	%r8d, %r12d
	negl	%r12d
	testb	%cl, %cl
	jne	.LBB0_5
# BB#18:                                # %._crit_edge
	cmpb	$45, -1(%rbp)
	je	.LBB0_19
.LBB0_20:
	cmpb	$45, -1(%r15)
	jne	.LBB0_21
.LBB0_19:
	movl	12(%rsp), %ebp          # 4-byte Reload
.LBB0_26:
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
	movb	$1, %al
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	fprintf
	cmpl	$0, divpairscore(%rip)
	jne	.LBB0_31
# BB#27:
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	je	.LBB0_29
.LBB0_28:
	addq	$8, %rbx
	movq	(%rbx), %rbx
.LBB0_29:
	testq	%rbx, %rbx
	jne	.LBB0_30
.LBB0_31:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_21:
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdx)
	testl	%eax, %eax
	jle	.LBB0_23
# BB#22:
	movq	%rdi, %r14
	movl	$1, %edi
	movl	%r9d, %r15d
	movl	$80, %esi
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	movl	%r8d, %ebp
	callq	calloc
	movl	%ebp, %r8d
	movsd	32(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	%r15d, %r9d
	movq	%rax, 8(%r14)
	movq	$0, 8(%rax)
	movq	%rax, %rdi
.LBB0_23:
	decl	%r13d
	leal	-1(%rbx), %eax
	movl	%r9d, 24(%rdi)
	movl	%r8d, 32(%rdi)
	movl	%r13d, 28(%rdi)
	movl	%eax, 36(%rdi)
	cmpl	$0, divpairscore(%rip)
	jne	.LBB0_24
# BB#25:
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	addl	%r12d, %ebx
	addl	12(%rsp), %ebx          # 4-byte Folded Reload
	movl	%ebx, %ebp
	jmp	.LBB0_26
.LBB0_24:
	movl	12(%rsp), %ebp          # 4-byte Reload
	subl	%r8d, %eax
	incl	%eax
	movl	%eax, 48(%rdi)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	%xmm0, %xmm1
	mulsd	.LCPI0_0(%rip), %xmm1
	divsd	.LCPI0_1(%rip), %xmm1
	movsd	%xmm1, 40(%rdi)
	jmp	.LBB0_26
.LBB0_3:
	xorpd	%xmm1, %xmm1
	xorl	%r8d, %r8d
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movq	%rbx, %rdi
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	xorl	%r12d, %r12d
	movq	%r14, %rbx
	xorl	%r9d, %r9d
	cmpb	$45, -1(%rbp)
	jne	.LBB0_20
	jmp	.LBB0_19
.LBB0_30:
	movl	%ebp, %eax
	movl	%eax, 48(%rbx)
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	mulsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	divsd	.LCPI0_1(%rip), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	divsd	%xmm1, %xmm0
	movsd	%xmm0, 40(%rbx)
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
	movb	$1, %al
	callq	fprintf
	jmp	.LBB0_28
.Lfunc_end0:
	.size	putlocalhom3, .Lfunc_end0-putlocalhom3
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4618216237887075123     # double 5.7999999999999998
.LCPI1_1:
	.quad	4648488871632306176     # double 600
	.text
	.globl	putlocalhom_ext
	.p2align	4, 0x90
	.type	putlocalhom_ext,@function
putlocalhom_ext:                        # @putlocalhom_ext
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 96
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%r8d, %r15d
	movl	%ecx, %r12d
	movq	%rdx, %r14
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movb	(%rbx), %cl
	testb	%cl, %cl
	je	.LBB1_15
# BB#1:                                 # %.lr.ph146.preheader
	xorl	%eax, %eax
	movl	$0, %r10d
	movq	%r14, %rsi
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph146
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$1, %eax
	jne	.LBB1_9
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	cmpb	$45, %cl
	je	.LBB1_5
# BB#4:                                 #   in Loop: Header=BB1_2 Depth=1
	cmpb	$45, (%r13)
	jne	.LBB1_9
.LBB1_5:                                #   in Loop: Header=BB1_2 Depth=1
	leal	-1(%r12), %eax
	leal	-1(%r15), %ebp
	testl	%r10d, %r10d
	jle	.LBB1_7
# BB#6:                                 #   in Loop: Header=BB1_2 Depth=1
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movl	$1, %edi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	$80, %esi
	movl	%r8d, 36(%rsp)          # 4-byte Spill
	movl	%r9d, 32(%rsp)          # 4-byte Spill
	movl	%r10d, 28(%rsp)         # 4-byte Spill
	movl	%eax, 24(%rsp)          # 4-byte Spill
	callq	calloc
	movl	28(%rsp), %r10d         # 4-byte Reload
	movl	32(%rsp), %r9d          # 4-byte Reload
	movl	36(%rsp), %r8d          # 4-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 8(%rcx)
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	movl	24(%rsp), %eax          # 4-byte Reload
.LBB1_7:                                #   in Loop: Header=BB1_2 Depth=1
	incl	%r10d
	movl	%r8d, 24(%rsi)
	movl	%edi, 32(%rsi)
	movl	%eax, 28(%rsi)
	movl	%ebp, 36(%rsi)
	cmpl	$0, divpairscore(%rip)
	je	.LBB1_12
# BB#8:                                 #   in Loop: Header=BB1_2 Depth=1
	subl	%edi, %ebp
	incl	%ebp
	movl	%ebp, 48(%rsi)
	cvtsi2sdl	%r9d, %xmm0
	cvtsi2sdl	%ebp, %xmm1
	divsd	%xmm1, %xmm0
	mulsd	.LCPI1_0(%rip), %xmm0
	divsd	.LCPI1_1(%rip), %xmm0
	movsd	%xmm0, 40(%rsi)
	jmp	.LBB1_13
	.p2align	4, 0x90
.LBB1_9:                                #   in Loop: Header=BB1_2 Depth=1
	cmpb	$45, %cl
	je	.LBB1_14
# BB#10:                                #   in Loop: Header=BB1_2 Depth=1
	movsbq	(%r13), %rdx
	cmpq	$45, %rdx
	je	.LBB1_14
# BB#11:                                #   in Loop: Header=BB1_2 Depth=1
	testl	%eax, %eax
	cmovel	%r12d, %r8d
	cmovel	%r15d, %edi
	movl	$1, %ebp
	cmovel	%ebp, %eax
	movsbq	%cl, %rcx
	movslq	amino_n(,%rcx,4), %rcx
	movslq	amino_n(,%rdx,4), %rdx
	imulq	$104, %rcx, %rcx
	addl	n_dis(%rcx,%rdx,4), %r9d
	jmp	.LBB1_14
.LBB1_12:                               #   in Loop: Header=BB1_2 Depth=1
	movl	%r15d, %eax
	subl	%edi, %eax
	addl	%eax, 8(%rsp)           # 4-byte Folded Spill
.LBB1_13:                               #   in Loop: Header=BB1_2 Depth=1
	xorl	%r9d, %r9d
	xorl	%eax, %eax
.LBB1_14:                               #   in Loop: Header=BB1_2 Depth=1
	movzbl	1(%rbx), %ecx
	xorl	%edx, %edx
	cmpb	$45, (%rbx)
	leaq	1(%rbx), %rbx
	setne	%dl
	addl	%edx, %r12d
	xorl	%edx, %edx
	cmpb	$45, (%r13)
	leaq	1(%r13), %r13
	setne	%dl
	addl	%edx, %r15d
	testb	%cl, %cl
	jne	.LBB1_2
	jmp	.LBB1_16
.LBB1_15:
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movq	%r14, %rsi
	xorl	%r10d, %r10d
.LBB1_16:                               # %._crit_edge
	cmpb	$45, -1(%rbx)
	je	.LBB1_17
# BB#18:
	cmpb	$45, -1(%r13)
	jne	.LBB1_20
.LBB1_17:
	movl	8(%rsp), %ebx           # 4-byte Reload
	cmpl	$0, divpairscore(%rip)
	jne	.LBB1_28
.LBB1_25:
	testq	%r14, %r14
	je	.LBB1_28
# BB#26:                                # %.lr.ph.preheader
	movabsq	$4618216237887075123, %rax # imm = 0x4017333333333333
	.p2align	4, 0x90
.LBB1_27:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, 48(%r14)
	movq	%rax, 40(%r14)
	movq	8(%r14), %r14
	testq	%r14, %r14
	jne	.LBB1_27
.LBB1_28:                               # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_20:
	testl	%r10d, %r10d
	movl	8(%rsp), %ebx           # 4-byte Reload
	jle	.LBB1_22
# BB#21:
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movl	$1, %edi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	$80, %esi
	movl	%r8d, %r13d
	movl	%r9d, %ebp
	callq	calloc
	movl	%ebp, %r9d
	movl	%r13d, %r8d
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 8(%rcx)
	movq	$0, 8(%rax)
	movq	%rax, %rsi
.LBB1_22:
	decl	%r12d
	decl	%r15d
	movl	%r8d, 24(%rsi)
	movl	%edi, 32(%rsi)
	movl	%r12d, 28(%rsi)
	movl	%r15d, 36(%rsi)
	subl	%edi, %r15d
	incl	%r15d
	cmpl	$0, divpairscore(%rip)
	je	.LBB1_24
# BB#23:
	movl	%r15d, 48(%rsi)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r9d, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r15d, %xmm1
	divsd	%xmm1, %xmm0
	mulsd	.LCPI1_0(%rip), %xmm0
	divsd	.LCPI1_1(%rip), %xmm0
	movsd	%xmm0, 40(%rsi)
	cmpl	$0, divpairscore(%rip)
	je	.LBB1_25
	jmp	.LBB1_28
.LBB1_24:
	addl	%r15d, %ebx
	cmpl	$0, divpairscore(%rip)
	je	.LBB1_25
	jmp	.LBB1_28
.Lfunc_end1:
	.size	putlocalhom_ext, .Lfunc_end1-putlocalhom_ext
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4618216237887075123     # double 5.7999999999999998
.LCPI2_1:
	.quad	4648488871632306176     # double 600
	.text
	.globl	putlocalhom2
	.p2align	4, 0x90
	.type	putlocalhom2,@function
putlocalhom2:                           # @putlocalhom2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 112
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movl	%r8d, %r15d
	movl	%ecx, %r12d
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movb	(%rbx), %cl
	testb	%cl, %cl
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	je	.LBB2_1
# BB#2:                                 # %.lr.ph150.preheader
	xorl	%ebp, %ebp
	movl	$0, %eax
	movl	$0, %r10d
	movq	%rdx, %rsi
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph150
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$1, %eax
	jne	.LBB2_12
# BB#4:                                 #   in Loop: Header=BB2_3 Depth=1
	cmpb	$45, %cl
	je	.LBB2_6
# BB#5:                                 #   in Loop: Header=BB2_3 Depth=1
	cmpb	$45, (%r13)
	jne	.LBB2_12
.LBB2_6:                                #   in Loop: Header=BB2_3 Depth=1
	leal	-1(%r12), %eax
	leal	-1(%r15), %r14d
	testl	%r10d, %r10d
	jle	.LBB2_8
# BB#7:                                 #   in Loop: Header=BB2_3 Depth=1
	movl	%edi, 20(%rsp)          # 4-byte Spill
	movl	$1, %edi
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movl	$80, %esi
	movl	%r8d, 52(%rsp)          # 4-byte Spill
	movl	%r9d, 48(%rsp)          # 4-byte Spill
	movl	%r10d, 44(%rsp)         # 4-byte Spill
	movl	%eax, 40(%rsp)          # 4-byte Spill
	callq	calloc
	movl	44(%rsp), %r10d         # 4-byte Reload
	movl	48(%rsp), %r9d          # 4-byte Reload
	movl	52(%rsp), %r8d          # 4-byte Reload
	movl	20(%rsp), %edi          # 4-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 8(%rcx)
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	movl	40(%rsp), %eax          # 4-byte Reload
.LBB2_8:                                #   in Loop: Header=BB2_3 Depth=1
	incl	%r10d
	movl	%r8d, 24(%rsi)
	movl	%edi, 32(%rsi)
	movl	%eax, 28(%rsi)
	movl	%r14d, 36(%rsi)
	cmpl	$0, divpairscore(%rip)
	je	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_3 Depth=1
	subl	%edi, %r14d
	incl	%r14d
	movl	%r14d, 48(%rsi)
	cvtsi2sdl	%r9d, %xmm0
	cvtsi2sdl	%r14d, %xmm1
	divsd	%xmm1, %xmm0
	mulsd	.LCPI2_0(%rip), %xmm0
	divsd	.LCPI2_1(%rip), %xmm0
	movsd	%xmm0, 40(%rsi)
	jmp	.LBB2_11
	.p2align	4, 0x90
.LBB2_12:                               #   in Loop: Header=BB2_3 Depth=1
	cmpb	$45, %cl
	je	.LBB2_15
# BB#13:                                #   in Loop: Header=BB2_3 Depth=1
	movsbq	(%r13), %rdx
	cmpq	$45, %rdx
	je	.LBB2_15
# BB#14:                                #   in Loop: Header=BB2_3 Depth=1
	testl	%eax, %eax
	cmovel	%r12d, %r8d
	cmovel	%r15d, %edi
	movl	$1, %ebp
	cmovel	%ebp, %eax
	movsbq	%cl, %rcx
	movslq	amino_n(,%rcx,4), %rcx
	movslq	amino_n(,%rdx,4), %rdx
	imulq	$104, %rcx, %rcx
	addl	n_dis(%rcx,%rdx,4), %r9d
	jmp	.LBB2_15
.LBB2_10:                               #   in Loop: Header=BB2_3 Depth=1
	addl	%r9d, 16(%rsp)          # 4-byte Folded Spill
	addl	%r15d, %ebp
	addl	%ebp, 12(%rsp)          # 4-byte Folded Spill
.LBB2_11:                               #   in Loop: Header=BB2_3 Depth=1
	xorl	%r9d, %r9d
	xorl	%eax, %eax
.LBB2_15:                               #   in Loop: Header=BB2_3 Depth=1
	xorl	%edx, %edx
	cmpb	$45, (%rbx)
	movzbl	1(%rbx), %ecx
	leaq	1(%rbx), %rbx
	setne	%dl
	addl	%edx, %r12d
	xorl	%edx, %edx
	cmpb	$45, (%r13)
	leaq	1(%r13), %r13
	setne	%dl
	addl	%edx, %r15d
	movl	%edi, %ebp
	negl	%ebp
	testb	%cl, %cl
	jne	.LBB2_3
	jmp	.LBB2_16
.LBB2_1:
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movq	%rdx, %rsi
	xorl	%r10d, %r10d
	xorl	%ebp, %ebp
.LBB2_16:                               # %._crit_edge
	cmpb	$45, -1(%rbx)
	je	.LBB2_17
# BB#18:
	cmpb	$45, -1(%r13)
	jne	.LBB2_19
.LBB2_17:
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	12(%rsp), %ebx          # 4-byte Reload
	cmpl	$0, divpairscore(%rip)
	je	.LBB2_26
	jmp	.LBB2_29
.LBB2_19:
	testl	%r10d, %r10d
	movl	12(%rsp), %ebx          # 4-byte Reload
	jle	.LBB2_21
# BB#20:
	movl	%edi, 20(%rsp)          # 4-byte Spill
	movl	$1, %edi
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movl	$80, %esi
	movl	%r8d, %r13d
	movl	%r9d, %r14d
	callq	calloc
	movl	%r14d, %r9d
	movl	%r13d, %r8d
	movl	20(%rsp), %edi          # 4-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 8(%rcx)
	movq	$0, 8(%rax)
	movq	%rax, %rsi
.LBB2_21:
	decl	%r12d
	leal	-1(%r15), %eax
	movl	%r8d, 24(%rsi)
	movl	%edi, 32(%rsi)
	movl	%r12d, 28(%rsi)
	movl	%eax, 36(%rsi)
	cmpl	$0, divpairscore(%rip)
	je	.LBB2_23
# BB#22:
	subl	%edi, %eax
	incl	%eax
	movl	%eax, 48(%rsi)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r9d, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	divsd	%xmm1, %xmm0
	mulsd	.LCPI2_0(%rip), %xmm0
	divsd	.LCPI2_1(%rip), %xmm0
	movsd	%xmm0, 40(%rsi)
	jmp	.LBB2_24
.LBB2_23:
	addl	%r9d, 16(%rsp)          # 4-byte Folded Spill
	addl	%ebp, %r15d
	addl	%ebx, %r15d
	movl	%r15d, %ebx
.LBB2_24:
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, divpairscore(%rip)
	jne	.LBB2_29
.LBB2_26:
	testq	%rcx, %rcx
	je	.LBB2_29
# BB#27:                                # %.lr.ph
	movl	16(%rsp), %eax          # 4-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI2_0(%rip), %xmm0
	imull	$600, %ebx, %eax        # imm = 0x258
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	divsd	%xmm1, %xmm0
	.p2align	4, 0x90
.LBB2_28:                               # =>This Inner Loop Header: Depth=1
	movl	%ebx, 48(%rcx)
	movsd	%xmm0, 40(%rcx)
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB2_28
.LBB2_29:                               # %.loopexit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	putlocalhom2, .Lfunc_end2-putlocalhom2
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4618216237887075123     # double 5.7999999999999998
.LCPI3_1:
	.quad	4648488871632306176     # double 600
	.text
	.globl	putlocalhom
	.p2align	4, 0x90
	.type	putlocalhom,@function
putlocalhom:                            # @putlocalhom
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 128
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movl	%ecx, %ebp
	movq	%rsi, %r15
	movq	%rdi, %r12
	movb	(%r12), %cl
	testb	%cl, %cl
	je	.LBB3_1
# BB#2:                                 # %.lr.ph148.preheader
	incq	%r12
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
	movl	$0, %eax
	movl	$0, %r9d
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rdx, %rdi
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	xorpd	%xmm1, %xmm1
	xorl	%r11d, %r11d
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph148
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %esi
	movl	%ebx, %r14d
	cmpl	$1, %eax
	jne	.LBB3_12
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpb	$45, %cl
	je	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpb	$45, (%r15)
	jne	.LBB3_12
.LBB3_6:                                #   in Loop: Header=BB3_3 Depth=1
	leal	-1(%rsi), %eax
	leal	-1(%r14), %ebx
	testl	%r9d, %r9d
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	jle	.LBB3_8
# BB#7:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movl	$1, %edi
	movl	%r11d, 44(%rsp)         # 4-byte Spill
	movl	$80, %esi
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	movl	%r8d, %ebp
	movl	%r9d, 40(%rsp)          # 4-byte Spill
	movl	%eax, 36(%rsp)          # 4-byte Spill
	callq	calloc
	movl	40(%rsp), %r9d          # 4-byte Reload
	movl	%ebp, %r8d
	movl	44(%rsp), %r11d         # 4-byte Reload
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 8(%rcx)
	movq	$0, 8(%rax)
	movq	%rax, %rdi
	movl	36(%rsp), %eax          # 4-byte Reload
.LBB3_8:                                #   in Loop: Header=BB3_3 Depth=1
	incl	%r9d
	movl	%r8d, 24(%rdi)
	movl	%r11d, 32(%rdi)
	movl	%eax, 28(%rdi)
	movl	%ebx, 36(%rdi)
	cmpl	$0, divpairscore(%rip)
	je	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_3 Depth=1
	subl	%r11d, %ebx
	incl	%ebx
	movl	%ebx, 48(%rdi)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	divsd	%xmm0, %xmm1
	mulsd	.LCPI3_0(%rip), %xmm1
	divsd	.LCPI3_1(%rip), %xmm1
	movsd	%xmm1, 40(%rdi)
	jmp	.LBB3_11
	.p2align	4, 0x90
.LBB3_12:                               #   in Loop: Header=BB3_3 Depth=1
	cmpb	$45, %cl
	je	.LBB3_15
# BB#13:                                #   in Loop: Header=BB3_3 Depth=1
	movsbq	(%r15), %rdx
	cmpq	$45, %rdx
	je	.LBB3_15
# BB#14:                                #   in Loop: Header=BB3_3 Depth=1
	testl	%eax, %eax
	cmovel	%esi, %r8d
	cmovel	%r14d, %r11d
	movl	$1, %ebp
	cmovel	%ebp, %eax
	movsbq	%cl, %rcx
	movslq	amino_n(,%rcx,4), %rcx
	movslq	amino_n(,%rdx,4), %rdx
	imulq	$104, %rcx, %rcx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	n_dis(%rcx,%rdx,4), %xmm0
	addsd	%xmm0, %xmm1
	jmp	.LBB3_15
.LBB3_10:                               #   in Loop: Header=BB3_3 Depth=1
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	addl	%r14d, %r13d
	addl	%r13d, 12(%rsp)         # 4-byte Folded Spill
.LBB3_11:                               #   in Loop: Header=BB3_3 Depth=1
	xorl	%eax, %eax
	xorpd	%xmm1, %xmm1
	movq	56(%rsp), %rsi          # 8-byte Reload
.LBB3_15:                               #   in Loop: Header=BB3_3 Depth=1
	xorl	%ebp, %ebp
	cmpb	$45, -1(%r12)
	movzbl	(%r12), %ecx
	setne	%bpl
	addl	%esi, %ebp
	xorl	%ebx, %ebx
	cmpb	$45, (%r15)
	leaq	1(%r15), %r15
	setne	%bl
	addl	%r14d, %ebx
	movl	%r11d, %r13d
	negl	%r13d
	incq	%r12
	testb	%cl, %cl
	jne	.LBB3_3
# BB#16:                                # %._crit_edge
	testl	%r9d, %r9d
	jle	.LBB3_18
# BB#17:
	movq	%rdi, %r15
	movl	$1, %edi
	movl	%r11d, %r14d
	movl	$80, %esi
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	movl	%r8d, %r12d
	callq	calloc
	movl	%r12d, %r8d
	movl	%r14d, %r11d
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%rax, 8(%r15)
	movq	$0, 8(%rax)
	movq	%rax, %rdi
.LBB3_18:                               # %._crit_edge.thread
	movq	48(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB3_19
.LBB3_1:
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	xorpd	%xmm1, %xmm1
	xorl	%r11d, %r11d
	xorl	%r8d, %r8d
	movq	%rdx, %rdi
.LBB3_19:                               # %._crit_edge.thread
	decl	%ebp
	leal	-1(%rbx), %eax
	movl	%r8d, 24(%rdi)
	movl	%r11d, 32(%rdi)
	movl	%ebp, 28(%rdi)
	movl	%eax, 36(%rdi)
	cmpl	$0, divpairscore(%rip)
	je	.LBB3_21
# BB#20:                                # %.thread
	subl	%r11d, %eax
	incl	%eax
	movl	%eax, 48(%rdi)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	%xmm0, %xmm1
	mulsd	.LCPI3_0(%rip), %xmm1
	divsd	.LCPI3_1(%rip), %xmm1
	movsd	%xmm1, 40(%rdi)
	jmp	.LBB3_24
.LBB3_21:
	testq	%rdx, %rdx
	je	.LBB3_24
# BB#22:                                # %.lr.ph
	addl	%r13d, %ebx
	addl	12(%rsp), %ebx          # 4-byte Folded Reload
	addsd	16(%rsp), %xmm1         # 8-byte Folded Reload
	mulsd	.LCPI3_0(%rip), %xmm1
	divsd	.LCPI3_1(%rip), %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	divsd	%xmm0, %xmm1
	.p2align	4, 0x90
.LBB3_23:                               # =>This Inner Loop Header: Depth=1
	movl	%ebx, 48(%rdx)
	movsd	%xmm1, 40(%rdx)
	movq	8(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_23
.LBB3_24:                               # %.loopexit
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	putlocalhom, .Lfunc_end3-putlocalhom
	.cfi_endproc

	.globl	cutal
	.p2align	4, 0x90
	.type	cutal,@function
cutal:                                  # @cutal
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	cmpl	%edx, %esi
	cmoveq	%rdi, %rax
	cmpl	%ecx, %esi
	je	.LBB4_3
# BB#2:                                 #   in Loop: Header=BB4_1 Depth=1
	movzbl	(%rdi), %r8d
	xorl	%r9d, %r9d
	cmpb	$45, %r8b
	setne	%r9b
	addl	%r9d, %esi
	incq	%rdi
	testb	%r8b, %r8b
	jne	.LBB4_1
.LBB4_3:
	movb	$0, 1(%rdi)
	retq
.Lfunc_end4:
	.size	cutal, .Lfunc_end4-cutal
	.cfi_endproc

	.globl	ErrorExit
	.p2align	4, 0x90
	.type	ErrorExit,@function
ErrorExit:                              # @ErrorExit
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rcx
	movq	stderr(%rip), %rdi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	ErrorExit, .Lfunc_end5-ErrorExit
	.cfi_endproc

	.globl	strncpy_caseC
	.p2align	4, 0x90
	.type	strncpy_caseC,@function
strncpy_caseC:                          # @strncpy_caseC
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 32
.Lcfi56:
	.cfi_offset %rbx, -32
.Lcfi57:
	.cfi_offset %r14, -24
.Lcfi58:
	.cfi_offset %r15, -16
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %rbx
	cmpl	$100, dorp(%rip)
	jne	.LBB6_9
# BB#1:
	movl	upperCase(%rip), %eax
	testl	%eax, %eax
	jle	.LBB6_9
# BB#2:                                 # %.preheader
	testl	%r14d, %r14d
	je	.LBB6_8
# BB#3:                                 # %.lr.ph
	callq	__ctype_toupper_loc
	leal	-1(%r14), %r8d
	movl	%r14d, %edx
	andl	$3, %edx
	je	.LBB6_6
# BB#4:                                 # %.prol.preheader
	negl	%edx
	.p2align	4, 0x90
.LBB6_5:                                # =>This Inner Loop Header: Depth=1
	decl	%r14d
	movq	(%rax), %rsi
	movsbq	(%r15), %rdi
	incq	%r15
	movzbl	(%rsi,%rdi,4), %ecx
	movb	%cl, (%rbx)
	incq	%rbx
	incl	%edx
	jne	.LBB6_5
.LBB6_6:                                # %.prol.loopexit
	cmpl	$3, %r8d
	jb	.LBB6_8
	.p2align	4, 0x90
.LBB6_7:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movsbq	(%r15), %rdx
	movzbl	(%rcx,%rdx,4), %ecx
	movb	%cl, (%rbx)
	movq	(%rax), %rcx
	movsbq	1(%r15), %rdx
	movzbl	(%rcx,%rdx,4), %ecx
	movb	%cl, 1(%rbx)
	movq	(%rax), %rcx
	movsbq	2(%r15), %rdx
	movzbl	(%rcx,%rdx,4), %ecx
	movb	%cl, 2(%rbx)
	addl	$-4, %r14d
	movq	(%rax), %rcx
	movsbq	3(%r15), %rdx
	leaq	4(%r15), %r15
	movzbl	(%rcx,%rdx,4), %ecx
	movb	%cl, 3(%rbx)
	leaq	4(%rbx), %rbx
	jne	.LBB6_7
.LBB6_8:                                # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB6_9:
	movslq	%r14d, %rdx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	strncpy                 # TAILCALL
.Lfunc_end6:
	.size	strncpy_caseC, .Lfunc_end6-strncpy_caseC
	.cfi_endproc

	.globl	seqUpper
	.p2align	4, 0x90
	.type	seqUpper,@function
seqUpper:                               # @seqUpper
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 64
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	testl	%edi, %edi
	jle	.LBB7_10
# BB#1:                                 # %.lr.ph26.preheader
	movl	%edi, %r12d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph26
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_8 Depth 2
	movq	(%r13,%rbp,8), %r15
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r14
	testl	%r14d, %r14d
	jle	.LBB7_9
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB7_2 Depth=1
	callq	__ctype_toupper_loc
	movl	%r14d, %r8d
	movq	(%rax), %rdx
	movsbq	(%r15), %rsi
	movb	(%rdx,%rsi,4), %dl
	movb	%dl, (%r15)
	cmpq	$1, %r8
	je	.LBB7_9
# BB#4:                                 # %._crit_edge32.preheader
                                        #   in Loop: Header=BB7_2 Depth=1
	testb	$1, %r14b
	jne	.LBB7_5
# BB#6:                                 # %._crit_edge32.prol
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	(%r13,%rbp,8), %rsi
	movq	(%rax), %rdx
	movsbq	1(%rsi), %rdi
	movb	(%rdx,%rdi,4), %dl
	movb	%dl, 1(%rsi)
	movl	$2, %edx
	cmpq	$2, %r8
	jne	.LBB7_8
	jmp	.LBB7_9
.LBB7_5:                                #   in Loop: Header=BB7_2 Depth=1
	movl	$1, %edx
	cmpq	$2, %r8
	je	.LBB7_9
	.p2align	4, 0x90
.LBB7_8:                                # %._crit_edge32
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13,%rbp,8), %rsi
	movq	(%rax), %rdi
	movsbq	(%rsi,%rdx), %rcx
	movzbl	(%rdi,%rcx,4), %ecx
	movb	%cl, (%rsi,%rdx)
	movq	(%r13,%rbp,8), %rcx
	movq	(%rax), %rsi
	movsbq	1(%rcx,%rdx), %rdi
	movzbl	(%rsi,%rdi,4), %ebx
	movb	%bl, 1(%rcx,%rdx)
	addq	$2, %rdx
	cmpq	%rdx, %r8
	jne	.LBB7_8
	.p2align	4, 0x90
.LBB7_9:                                # %._crit_edge
                                        #   in Loop: Header=BB7_2 Depth=1
	incq	%rbp
	cmpq	%r12, %rbp
	jne	.LBB7_2
.LBB7_10:                               # %._crit_edge27
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	seqUpper, .Lfunc_end7-seqUpper
	.cfi_endproc

	.globl	seqLower
	.p2align	4, 0x90
	.type	seqLower,@function
seqLower:                               # @seqLower
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 64
.Lcfi79:
	.cfi_offset %rbx, -56
.Lcfi80:
	.cfi_offset %r12, -48
.Lcfi81:
	.cfi_offset %r13, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	testl	%edi, %edi
	jle	.LBB8_10
# BB#1:                                 # %.lr.ph26.preheader
	movl	%edi, %r12d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_2:                                # %.lr.ph26
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_8 Depth 2
	movq	(%r13,%rbp,8), %r15
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r14
	testl	%r14d, %r14d
	jle	.LBB8_9
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB8_2 Depth=1
	callq	__ctype_tolower_loc
	movl	%r14d, %r8d
	movq	(%rax), %rdx
	movsbq	(%r15), %rsi
	movb	(%rdx,%rsi,4), %dl
	movb	%dl, (%r15)
	cmpq	$1, %r8
	je	.LBB8_9
# BB#4:                                 # %._crit_edge32.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	testb	$1, %r14b
	jne	.LBB8_5
# BB#6:                                 # %._crit_edge32.prol
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	(%r13,%rbp,8), %rsi
	movq	(%rax), %rdx
	movsbq	1(%rsi), %rdi
	movb	(%rdx,%rdi,4), %dl
	movb	%dl, 1(%rsi)
	movl	$2, %edx
	cmpq	$2, %r8
	jne	.LBB8_8
	jmp	.LBB8_9
.LBB8_5:                                #   in Loop: Header=BB8_2 Depth=1
	movl	$1, %edx
	cmpq	$2, %r8
	je	.LBB8_9
	.p2align	4, 0x90
.LBB8_8:                                # %._crit_edge32
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13,%rbp,8), %rsi
	movq	(%rax), %rdi
	movsbq	(%rsi,%rdx), %rcx
	movzbl	(%rdi,%rcx,4), %ecx
	movb	%cl, (%rsi,%rdx)
	movq	(%r13,%rbp,8), %rcx
	movq	(%rax), %rsi
	movsbq	1(%rcx,%rdx), %rdi
	movzbl	(%rsi,%rdi,4), %ebx
	movb	%bl, 1(%rcx,%rdx)
	addq	$2, %rdx
	cmpq	%rdx, %r8
	jne	.LBB8_8
	.p2align	4, 0x90
.LBB8_9:                                # %._crit_edge
                                        #   in Loop: Header=BB8_2 Depth=1
	incq	%rbp
	cmpq	%r12, %rbp
	jne	.LBB8_2
.LBB8_10:                               # %._crit_edge27
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	seqLower, .Lfunc_end8-seqLower
	.cfi_endproc

	.globl	getaline_fp_eof
	.p2align	4, 0x90
	.type	getaline_fp_eof,@function
getaline_fp_eof:                        # @getaline_fp_eof
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi88:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 48
.Lcfi90:
	.cfi_offset %rbx, -48
.Lcfi91:
	.cfi_offset %r12, -40
.Lcfi92:
	.cfi_offset %r14, -32
.Lcfi93:
	.cfi_offset %r15, -24
.Lcfi94:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movl	%esi, %ebx
	movq	%rdi, %r15
	movl	$1, %r14d
	testl	%ebx, %ebx
	jle	.LBB9_7
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB9_6
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB9_2 Depth=1
	cmpl	$10, %eax
	je	.LBB9_5
# BB#4:                                 #   in Loop: Header=BB9_2 Depth=1
	movb	%al, (%r15,%rbp)
	incq	%rbp
	cmpl	%ebx, %ebp
	jl	.LBB9_2
.LBB9_5:                                # %.lr.ph..critedge.loopexit38_crit_edge
	addq	%rbp, %r15
	xorl	%r14d, %r14d
	jmp	.LBB9_7
.LBB9_6:                                # %.critedge.loopexit
	addq	%rbp, %r15
.LBB9_7:                                # %.critedge
	movb	$0, (%r15)
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	getaline_fp_eof, .Lfunc_end9-getaline_fp_eof
	.cfi_endproc

	.globl	getaline_fp_eof_new
	.p2align	4, 0x90
	.type	getaline_fp_eof_new,@function
getaline_fp_eof_new:                    # @getaline_fp_eof_new
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi98:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 48
.Lcfi100:
	.cfi_offset %rbx, -48
.Lcfi101:
	.cfi_offset %r12, -40
.Lcfi102:
	.cfi_offset %r14, -32
.Lcfi103:
	.cfi_offset %r15, -24
.Lcfi104:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %r12d
	movq	%rdi, %r14
	movq	%rbx, %rdi
	callq	feof
	movl	$1, %r15d
	testl	%eax, %eax
	jne	.LBB10_12
# BB#1:                                 # %.preheader27
	testl	%r12d, %r12d
	jle	.LBB10_13
# BB#2:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB10_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB10_10
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB10_3 Depth=1
	cmpl	$10, %eax
	je	.LBB10_9
# BB#5:                                 #   in Loop: Header=BB10_3 Depth=1
	movb	%al, (%r14,%rbp)
	incq	%rbp
	cmpl	%r12d, %ebp
	jl	.LBB10_3
# BB#6:                                 # %.critedge
	movb	$0, (%r14,%rbp)
	xorl	%r15d, %r15d
	cmpl	$10, %eax
	jne	.LBB10_8
	jmp	.LBB10_12
.LBB10_13:                              # %.critedge.thread51
	movb	$0, (%r14)
	movl	$1, %r15d
	jmp	.LBB10_8
.LBB10_10:                              # %.critedge.thread.loopexit
	movl	$1, %r15d
	jmp	.LBB10_11
.LBB10_9:                               # %.critedge.thread.loopexit59
	xorl	%r15d, %r15d
.LBB10_11:
	movb	$0, (%r14,%rbp)
	jmp	.LBB10_12
.LBB10_8:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	jne	.LBB10_8
.LBB10_12:
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	getaline_fp_eof_new, .Lfunc_end10-getaline_fp_eof_new
	.cfi_endproc

	.globl	myfgets
	.p2align	4, 0x90
	.type	myfgets,@function
myfgets:                                # @myfgets
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi107:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi109:
	.cfi_def_cfa_offset 48
.Lcfi110:
	.cfi_offset %rbx, -40
.Lcfi111:
	.cfi_offset %r14, -32
.Lcfi112:
	.cfi_offset %r15, -24
.Lcfi113:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %r15d
	movq	%rdi, %r14
	movq	%rbx, %rdi
	callq	feof
	movl	%eax, %ecx
	movl	$1, %eax
	testl	%ecx, %ecx
	jne	.LBB11_10
# BB#1:                                 # %.preheader
	testl	%r15d, %r15d
	jle	.LBB11_7
# BB#2:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB11_4
# BB#5:                                 #   in Loop: Header=BB11_3 Depth=1
	movb	%al, (%r14,%rbp)
	incq	%rbp
	cmpl	%r15d, %ebp
	jl	.LBB11_3
# BB#6:                                 # %.critedge.loopexit
	addq	%rbp, %r14
.LBB11_7:                               # %.critedge
	movb	$0, (%r14)
	.p2align	4, 0x90
.LBB11_8:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	jne	.LBB11_8
	jmp	.LBB11_9
.LBB11_4:                               # %.critedge.thread
	movb	$0, (%r14,%rbp)
.LBB11_9:
	xorl	%eax, %eax
.LBB11_10:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	myfgets, .Lfunc_end11-myfgets
	.cfi_endproc

	.globl	input_new
	.p2align	4, 0x90
	.type	input_new,@function
input_new:                              # @input_new
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi114:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi116:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi117:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi118:
	.cfi_def_cfa_offset 64
.Lcfi119:
	.cfi_offset %rbx, -40
.Lcfi120:
	.cfi_offset %r12, -32
.Lcfi121:
	.cfi_offset %r14, -24
.Lcfi122:
	.cfi_offset %r15, -16
	movl	%esi, %r14d
	movq	%rdi, %r12
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB12_2
# BB#1:
	movl	%eax, %edi
	movq	%r12, %rsi
	callq	ungetc
.LBB12_2:                               # %.preheader
	testl	%r14d, %r14d
	jle	.LBB12_3
# BB#4:                                 # %.lr.ph.preheader
	leal	-1(%r14), %r15d
	movl	%r14d, %r14d
	leaq	14(%rsp), %rbx
	.p2align	4, 0x90
.LBB12_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	_IO_getc
	movb	%al, (%rbx)
	incq	%rbx
	decq	%r14
	jne	.LBB12_5
# BB#6:                                 # %._crit_edge.loopexit
	shlq	$32, %r15
	movabsq	$4294967296, %rax       # imm = 0x100000000
	addq	%r15, %rax
	sarq	$32, %rax
	jmp	.LBB12_7
.LBB12_3:
	xorl	%eax, %eax
.LBB12_7:                               # %._crit_edge
	movb	$0, 14(%rsp,%rax)
	leaq	14(%rsp), %rdi
	xorl	%esi, %esi
	callq	strtod
	cvtsd2ss	%xmm0, %xmm0
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	input_new, .Lfunc_end12-input_new
	.cfi_endproc

	.globl	PreRead
	.p2align	4, 0x90
	.type	PreRead,@function
PreRead:                                # @PreRead
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi124:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi125:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi126:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi127:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi129:
	.cfi_def_cfa_offset 320
.Lcfi130:
	.cfi_offset %rbx, -56
.Lcfi131:
	.cfi_offset %r12, -48
.Lcfi132:
	.cfi_offset %r13, -40
.Lcfi133:
	.cfi_offset %r14, -32
.Lcfi134:
	.cfi_offset %r15, -24
.Lcfi135:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	%rsp, %r12
	movl	$255, %esi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r12, %rdi
	callq	strtol
	movl	%eax, (%r15)
	xorl	%ebp, %ebp
.LBB13_1:                               # %.outer.sink.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_2 Depth 2
	movl	%r13d, (%r14)
	.p2align	4, 0x90
.LBB13_2:                               #   Parent Loop BB13_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	(%r15), %ebp
	jge	.LBB13_5
# BB#3:                                 #   in Loop: Header=BB13_2 Depth=2
	movl	$255, %esi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	cmpb	$61, (%rsp)
	jne	.LBB13_2
# BB#4:                                 #   in Loop: Header=BB13_2 Depth=2
	incl	%ebp
	movl	$255, %esi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r12, %rdi
	callq	strtol
	movq	%rax, %r13
	cmpl	(%r14), %r13d
	jle	.LBB13_2
	jmp	.LBB13_1
.LBB13_5:
	cmpl	$5000001, (%r14)        # imm = 0x4C4B41
	jge	.LBB13_8
# BB#6:
	cmpl	$50001, njob(%rip)      # imm = 0xC351
	jge	.LBB13_9
# BB#7:
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_8:
	movq	stderr(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$19, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB13_9:
	movq	stderr(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$19, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	movl	njob(%rip), %edx
	movl	$.L.str.9, %esi
	movl	$50000, %ecx            # imm = 0xC350
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end13:
	.size	PreRead, .Lfunc_end13-PreRead
	.cfi_endproc

	.globl	allSpace
	.p2align	4, 0x90
	.type	allSpace,@function
allSpace:                               # @allSpace
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi136:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi138:
	.cfi_def_cfa_offset 32
.Lcfi139:
	.cfi_offset %rbx, -24
.Lcfi140:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movb	(%rbx), %bpl
	testb	%bpl, %bpl
	je	.LBB14_1
# BB#2:                                 # %.lr.ph
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	incq	%rbx
	movl	$1, %eax
	.p2align	4, 0x90
.LBB14_3:                               # =>This Inner Loop Header: Depth=1
	movsbq	%bpl, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	shrl	$11, %edx
	notl	%edx
	andl	$1, %edx
	negl	%edx
	andl	%edx, %eax
	movzbl	(%rbx), %ebp
	incq	%rbx
	testb	%bpl, %bpl
	jne	.LBB14_3
	jmp	.LBB14_4
.LBB14_1:
	movl	$1, %eax
.LBB14_4:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end14:
	.size	allSpace, .Lfunc_end14-allSpace
	.cfi_endproc

	.globl	Read
	.p2align	4, 0x90
	.type	Read,@function
Read:                                   # @Read
	.cfi_startproc
# BB#0:
	movq	%rdx, %rax
	movq	%rsi, %rcx
	movq	%rdi, %rdx
	movq	stdin(%rip), %rdi
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	%rax, %rcx
	jmp	FRead                   # TAILCALL
.Lfunc_end15:
	.size	Read, .Lfunc_end15-Read
	.cfi_endproc

	.globl	FRead
	.p2align	4, 0x90
	.type	FRead,@function
FRead:                                  # @FRead
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi141:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi142:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi143:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi144:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi145:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi146:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi147:
	.cfi_def_cfa_offset 336
.Lcfi148:
	.cfi_offset %rbx, -56
.Lcfi149:
	.cfi_offset %r12, -48
.Lcfi150:
	.cfi_offset %r13, -40
.Lcfi151:
	.cfi_offset %r14, -32
.Lcfi152:
	.cfi_offset %r15, -24
.Lcfi153:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %rbp
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %rbx
	leaq	16(%rsp), %r13
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$.L.str.10, %esi
	movq	%r13, %rdi
	callq	strstr
	testq	%rax, %rax
	je	.LBB16_2
# BB#1:
	movl	$1, scoremtx(%rip)
	jmp	.LBB16_13
.LBB16_2:
	leaq	16(%rsp), %rdi
	movl	$.L.str.11, %esi
	callq	strstr
	testq	%rax, %rax
	je	.LBB16_4
# BB#3:
	movl	$-1, scoremtx(%rip)
	movl	$-1, upperCase(%rip)
	jmp	.LBB16_13
.LBB16_4:
	leaq	16(%rsp), %rdi
	movl	$.L.str.12, %esi
	callq	strstr
	testq	%rax, %rax
	je	.LBB16_6
# BB#5:
	movl	$-1, scoremtx(%rip)
	movl	$0, upperCase(%rip)
	jmp	.LBB16_13
.LBB16_6:
	leaq	16(%rsp), %rdi
	movl	$.L.str.13, %esi
	callq	strstr
	testq	%rax, %rax
	je	.LBB16_8
# BB#7:
	movl	$-1, scoremtx(%rip)
	movl	$1, upperCase(%rip)
	jmp	.LBB16_13
.LBB16_8:
	leaq	16(%rsp), %rdi
	movl	$.L.str.14, %esi
	callq	strstr
	testq	%rax, %rax
	jne	.LBB16_11
# BB#9:
	leaq	16(%rsp), %rdi
	movl	$.L.str.15, %esi
	callq	strstr
	testq	%rax, %rax
	je	.LBB16_10
.LBB16_11:
	movl	$2, %eax
.LBB16_12:
	movl	%eax, scoremtx(%rip)
.LBB16_13:
	movl	$981668463, geta2(%rip) # imm = 0x3A83126F
	cmpl	$0, njob(%rip)
	jle	.LBB16_46
# BB#14:                                # %.lr.ph58.preheader
	xorl	%r14d, %r14d
	movq	%rbp, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB16_15:                              # %.lr.ph58
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_17 Depth 2
                                        #     Child Loop BB16_21 Depth 2
                                        #     Child Loop BB16_25 Depth 2
                                        #       Child Loop BB16_27 Depth 3
                                        #       Child Loop BB16_31 Depth 3
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB16_22
# BB#16:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB16_15 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB16_17:                              # %.lr.ph.i
                                        #   Parent Loop BB16_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB16_47
# BB#18:                                # %.lr.ph.i
                                        #   in Loop: Header=BB16_17 Depth=2
	cmpl	$10, %eax
	je	.LBB16_47
# BB#19:                                #   in Loop: Header=BB16_17 Depth=2
	movb	%al, 16(%rsp,%rbp)
	incq	%rbp
	cmpl	$255, %ebp
	jl	.LBB16_17
# BB#20:                                # %.critedge.i
                                        #   in Loop: Header=BB16_15 Depth=1
	movb	$0, 16(%rsp,%rbp)
	cmpl	$10, %eax
	movq	(%rsp), %rbp            # 8-byte Reload
	je	.LBB16_22
	.p2align	4, 0x90
.LBB16_21:                              # %.preheader.i
                                        #   Parent Loop BB16_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	jne	.LBB16_21
	jmp	.LBB16_22
.LBB16_47:                              # %.critedge.thread.i
                                        #   in Loop: Header=BB16_15 Depth=1
	movb	$0, 16(%rsp,%rbp)
	movq	(%rsp), %rbp            # 8-byte Reload
	.p2align	4, 0x90
.LBB16_22:                              # %getaline_fp_eof_new.exit
                                        #   in Loop: Header=BB16_15 Depth=1
	movq	%r14, %rdi
	shlq	$8, %rdi
	addq	8(%rsp), %rdi           # 8-byte Folded Reload
	leaq	16(%rsp), %r15
	movq	%r15, %rsi
	callq	strcpy
	movl	$255, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	fgets
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r15, %rdi
	callq	strtol
	movl	%eax, (%rbp,%r14,4)
	movq	(%r12,%r14,8), %rax
	movb	$0, (%rax)
	movl	(%rbp,%r14,4), %eax
	testl	%eax, %eax
	je	.LBB16_33
# BB#23:                                # %.preheader
                                        #   in Loop: Header=BB16_15 Depth=1
	cmpl	$-58, %eax
	movl	%eax, %r13d
	jl	.LBB16_33
# BB#24:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB16_15 Depth=1
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB16_25:                              # %.lr.ph
                                        #   Parent Loop BB16_15 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB16_27 Depth 3
                                        #       Child Loop BB16_31 Depth 3
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB16_32
# BB#26:                                # %.lr.ph.i32.preheader
                                        #   in Loop: Header=BB16_25 Depth=2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB16_27:                              # %.lr.ph.i32
                                        #   Parent Loop BB16_15 Depth=1
                                        #     Parent Loop BB16_25 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB16_48
# BB#28:                                # %.lr.ph.i32
                                        #   in Loop: Header=BB16_27 Depth=3
	cmpl	$10, %eax
	je	.LBB16_48
# BB#29:                                #   in Loop: Header=BB16_27 Depth=3
	movb	%al, 16(%rsp,%rbp)
	incq	%rbp
	cmpl	$255, %ebp
	jl	.LBB16_27
# BB#30:                                # %.critedge.i37
                                        #   in Loop: Header=BB16_25 Depth=2
	movb	$0, 16(%rsp,%rbp)
	cmpl	$10, %eax
	movq	(%rsp), %rbp            # 8-byte Reload
	je	.LBB16_32
	.p2align	4, 0x90
.LBB16_31:                              # %.preheader.i39
                                        #   Parent Loop BB16_15 Depth=1
                                        #     Parent Loop BB16_25 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	jne	.LBB16_31
	jmp	.LBB16_32
.LBB16_48:                              # %.critedge.thread.i35
                                        #   in Loop: Header=BB16_25 Depth=2
	movb	$0, 16(%rsp,%rbp)
	movq	(%rsp), %rbp            # 8-byte Reload
	.p2align	4, 0x90
.LBB16_32:                              # %getaline_fp_eof_new.exit43
                                        #   in Loop: Header=BB16_25 Depth=2
	movq	(%r12,%r14,8), %rdi
	leaq	16(%rsp), %rsi
	callq	strcat
	movl	(%rbp,%r14,4), %r13d
	leal	-1(%r13), %eax
	cltq
	imulq	$-2004318071, %rax, %rax # imm = 0x88888889
	shrq	$32, %rax
	leal	-1(%rax,%r13), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	sarl	$5, %eax
	addl	%ecx, %eax
	cmpl	%eax, %r15d
	leal	1(%r15), %eax
	movl	%eax, %r15d
	jl	.LBB16_25
.LBB16_33:                              # %.loopexit
                                        #   in Loop: Header=BB16_15 Depth=1
	movq	(%r12,%r14,8), %rax
	movslq	%r13d, %rcx
	movb	$0, (%rax,%rcx)
	incq	%r14
	movslq	njob(%rip), %rax
	cmpq	%rax, %r14
	jl	.LBB16_15
# BB#34:                                # %._crit_edge
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jle	.LBB16_46
# BB#35:                                # %._crit_edge
	cmpl	$-1, scoremtx(%rip)
	jne	.LBB16_46
# BB#36:                                # %._crit_edge
	cmpl	$-1, upperCase(%rip)
	je	.LBB16_46
# BB#37:                                # %.lr.ph26.preheader.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB16_38:                              # %.lr.ph26.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_44 Depth 2
	movq	(%r12,%rbp,8), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r14
	testl	%r14d, %r14d
	jle	.LBB16_45
# BB#39:                                # %.lr.ph.i44
                                        #   in Loop: Header=BB16_38 Depth=1
	callq	__ctype_tolower_loc
	movl	%r14d, %ecx
	movq	(%rax), %rdx
	movsbq	(%rbx), %rsi
	movb	(%rdx,%rsi,4), %dl
	movb	%dl, (%rbx)
	cmpq	$1, %rcx
	je	.LBB16_45
# BB#40:                                # %._crit_edge32.i.preheader
                                        #   in Loop: Header=BB16_38 Depth=1
	testb	$1, %r14b
	jne	.LBB16_41
# BB#42:                                # %._crit_edge32.i.prol
                                        #   in Loop: Header=BB16_38 Depth=1
	movq	(%r12,%rbp,8), %rdx
	movq	(%rax), %rsi
	movsbq	1(%rdx), %rdi
	movb	(%rsi,%rdi,4), %bl
	movb	%bl, 1(%rdx)
	movl	$2, %edx
	cmpq	$2, %rcx
	jne	.LBB16_44
	jmp	.LBB16_45
.LBB16_41:                              #   in Loop: Header=BB16_38 Depth=1
	movl	$1, %edx
	cmpq	$2, %rcx
	je	.LBB16_45
	.p2align	4, 0x90
.LBB16_44:                              # %._crit_edge32.i
                                        #   Parent Loop BB16_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%rbp,8), %rsi
	movq	(%rax), %rdi
	movsbq	(%rsi,%rdx), %rbx
	movzbl	(%rdi,%rbx,4), %ebx
	movb	%bl, (%rsi,%rdx)
	movq	(%r12,%rbp,8), %rsi
	movq	(%rax), %rdi
	movsbq	1(%rsi,%rdx), %rbx
	movzbl	(%rdi,%rbx,4), %ebx
	movb	%bl, 1(%rsi,%rdx)
	addq	$2, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB16_44
	.p2align	4, 0x90
.LBB16_45:                              # %._crit_edge.i
                                        #   in Loop: Header=BB16_38 Depth=1
	incq	%rbp
	cmpq	%r15, %rbp
	jne	.LBB16_38
.LBB16_46:                              # %seqLower.exit
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_10:
	xorl	%eax, %eax
	jmp	.LBB16_12
.Lfunc_end16:
	.size	FRead, .Lfunc_end16-FRead
	.cfi_endproc

	.globl	searchKUorWA
	.p2align	4, 0x90
	.type	searchKUorWA,@function
searchKUorWA:                           # @searchKUorWA
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi154:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi155:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi156:
	.cfi_def_cfa_offset 32
.Lcfi157:
	.cfi_offset %rbx, -32
.Lcfi158:
	.cfi_offset %r14, -24
.Lcfi159:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$10, %eax
	movabsq	$-4611686018427387903, %r14 # imm = 0xC000000000000001
	.p2align	4, 0x90
.LBB17_1:                               # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ecx
	incl	%ecx
	cmpl	$63, %ecx
	ja	.LBB17_1
# BB#2:                                 # %.critedge
                                        #   in Loop: Header=BB17_1 Depth=1
	btq	%rcx, %r14
	jae	.LBB17_1
# BB#3:                                 #   in Loop: Header=BB17_1 Depth=1
	cmpl	$10, %ebp
	jne	.LBB17_1
# BB#4:
	movl	%eax, %edi
	movq	%rbx, %rsi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	ungetc                  # TAILCALL
.Lfunc_end17:
	.size	searchKUorWA, .Lfunc_end17-searchKUorWA
	.cfi_endproc

	.globl	kake2hiku
	.p2align	4, 0x90
	.type	kake2hiku,@function
kake2hiku:                              # @kake2hiku
	.cfi_startproc
# BB#0:
	jmp	.LBB18_1
	.p2align	4, 0x90
.LBB18_5:                               # %.backedge.backedge
                                        #   in Loop: Header=BB18_1 Depth=1
	incq	%rdi
.LBB18_1:                               # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %eax
	cmpb	$42, %al
	je	.LBB18_4
# BB#2:                                 # %.backedge
                                        #   in Loop: Header=BB18_1 Depth=1
	testb	%al, %al
	jne	.LBB18_5
	jmp	.LBB18_3
	.p2align	4, 0x90
.LBB18_4:                               # %.thread
                                        #   in Loop: Header=BB18_1 Depth=1
	movb	$45, (%rdi)
	jmp	.LBB18_5
.LBB18_3:
	retq
.Lfunc_end18:
	.size	kake2hiku, .Lfunc_end18-kake2hiku
	.cfi_endproc

	.globl	load1SeqWithoutName_realloc
	.p2align	4, 0x90
	.type	load1SeqWithoutName_realloc,@function
load1SeqWithoutName_realloc:            # @load1SeqWithoutName_realloc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi161:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi162:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi163:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi164:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi166:
	.cfi_def_cfa_offset 64
.Lcfi167:
	.cfi_offset %rbx, -56
.Lcfi168:
	.cfi_offset %r12, -48
.Lcfi169:
	.cfi_offset %r13, -40
.Lcfi170:
	.cfi_offset %r14, -32
.Lcfi171:
	.cfi_offset %r15, -24
.Lcfi172:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	$5000001, %edi          # imm = 0x4C4B41
	callq	malloc
	movq	%rax, %rbx
	movl	$10, %r12d
	movl	$5000000, %r13d         # imm = 0x4C4B40
	movq	%rbx, %r14
	jmp	.LBB19_1
	.p2align	4, 0x90
.LBB19_16:                              #   in Loop: Header=BB19_1 Depth=1
	movq	stderr(%rip), %rcx
	movl	$.L.str.16, %edi
	movl	$16, %esi
	movl	$1, %edx
	callq	fwrite
	leaq	5000001(%r13), %rsi
	movq	%r14, %rdi
	callq	realloc
	movq	%rax, %r14
	movq	stderr(%rip), %rcx
	testq	%r14, %r14
	je	.LBB19_31
# BB#17:                                #   in Loop: Header=BB19_1 Depth=1
	addq	$5000000, %r13          # imm = 0x4C4B40
	movl	$.L.str.18, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	leaq	-5000000(%r14,%r13), %rbx
.LBB19_1:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_2 Depth 2
	incq	%rbx
	movl	%r12d, %ebp
	.p2align	4, 0x90
.LBB19_2:                               #   Parent Loop BB19_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %r12d
	leal	-61(%r12), %eax
	cmpl	$2, %eax
	jb	.LBB19_14
# BB#3:                                 #   in Loop: Header=BB19_2 Depth=2
	cmpl	$40, %r12d
	je	.LBB19_14
# BB#4:                                 #   in Loop: Header=BB19_2 Depth=2
	cmpl	$-1, %r12d
	jne	.LBB19_15
	jmp	.LBB19_5
	.p2align	4, 0x90
.LBB19_14:                              #   in Loop: Header=BB19_2 Depth=2
	cmpl	$10, %ebp
	je	.LBB19_6
.LBB19_15:                              # %.thread
                                        #   in Loop: Header=BB19_2 Depth=2
	movb	%r12b, -1(%rbx)
	movq	%rbx, %rax
	subq	%r14, %rax
	incq	%rbx
	cmpq	%r13, %rax
	movl	%r12d, %ebp
	jne	.LBB19_2
	jmp	.LBB19_16
.LBB19_5:
	movl	$-1, %r12d
.LBB19_6:                               # %.critedge
	movl	%r12d, %edi
	movq	%r15, %rsi
	callq	ungetc
	movb	$0, -1(%rbx)
	cmpl	$100, dorp(%rip)
	jne	.LBB19_7
# BB#18:                                # %.outer.i.preheader
	movabsq	$109951162777600, %r15  # imm = 0x640000000000
	movq	%r14, %r12
	movq	%r14, %r13
	jmp	.LBB19_19
	.p2align	4, 0x90
.LBB19_24:                              #   in Loop: Header=BB19_19 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movb	(%rax,%rbx,4), %al
	movb	%al, (%r12)
	incq	%r12
.LBB19_19:                              # %.outer.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_20 Depth 2
	incq	%r13
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB19_20:                              #   Parent Loop BB19_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %r13
	movsbq	-1(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB19_25
# BB#21:                                #   in Loop: Header=BB19_20 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rax
	testb	$4, 1(%rax,%rbx,2)
	jne	.LBB19_24
# BB#22:                                # %switch.early.test.i
                                        #   in Loop: Header=BB19_20 Depth=2
	leaq	1(%r13), %rax
	cmpb	$46, %bl
	ja	.LBB19_20
# BB#23:                                # %switch.early.test.i
                                        #   in Loop: Header=BB19_20 Depth=2
	movzbl	%bl, %ecx
	btq	%rcx, %r15
	jae	.LBB19_20
	jmp	.LBB19_24
.LBB19_7:
	movabsq	$109951162777600, %r15  # imm = 0x640000000000
	movq	%r14, %r12
	movq	%r14, %r13
	jmp	.LBB19_8
	.p2align	4, 0x90
.LBB19_13:                              #   in Loop: Header=BB19_8 Depth=1
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movb	(%rax,%rbx,4), %al
	movb	%al, (%r12)
	incq	%r12
.LBB19_8:                               # %.outer.i45
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_9 Depth 2
	incq	%r13
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB19_9:                               #   Parent Loop BB19_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %r13
	movsbq	-1(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB19_25
# BB#10:                                #   in Loop: Header=BB19_9 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rax
	testb	$4, 1(%rax,%rbx,2)
	jne	.LBB19_13
# BB#11:                                # %switch.early.test.i47
                                        #   in Loop: Header=BB19_9 Depth=2
	leaq	1(%r13), %rax
	cmpb	$46, %bl
	ja	.LBB19_9
# BB#12:                                # %switch.early.test.i47
                                        #   in Loop: Header=BB19_9 Depth=2
	movzbl	%bl, %ecx
	btq	%rcx, %r15
	jae	.LBB19_9
	jmp	.LBB19_13
.LBB19_25:                              # %.backedge.i.preheader
	movb	$0, (%r12)
	movq	%r14, %rax
	jmp	.LBB19_26
	.p2align	4, 0x90
.LBB19_30:                              # %.backedge.backedge.i
                                        #   in Loop: Header=BB19_26 Depth=1
	incq	%rax
.LBB19_26:                              # %.backedge.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	cmpb	$42, %cl
	je	.LBB19_29
# BB#27:                                # %.backedge.i
                                        #   in Loop: Header=BB19_26 Depth=1
	testb	%cl, %cl
	jne	.LBB19_30
	jmp	.LBB19_28
	.p2align	4, 0x90
.LBB19_29:                              # %.thread.i
                                        #   in Loop: Header=BB19_26 Depth=1
	movb	$45, (%rax)
	jmp	.LBB19_30
.LBB19_28:                              # %kake2hiku.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB19_31:
	movl	$.L.str.17, %edi
	movl	$49, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end19:
	.size	load1SeqWithoutName_realloc, .Lfunc_end19-load1SeqWithoutName_realloc
	.cfi_endproc

	.globl	load1SeqWithoutName_new
	.p2align	4, 0x90
	.type	load1SeqWithoutName_new,@function
load1SeqWithoutName_new:                # @load1SeqWithoutName_new
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi173:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi174:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi175:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi176:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi177:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi178:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi179:
	.cfi_def_cfa_offset 64
.Lcfi180:
	.cfi_offset %rbx, -56
.Lcfi181:
	.cfi_offset %r12, -48
.Lcfi182:
	.cfi_offset %r13, -40
.Lcfi183:
	.cfi_offset %r14, -32
.Lcfi184:
	.cfi_offset %r15, -24
.Lcfi185:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	movl	$10, %ebp
	movq	%r12, %rbx
	jmp	.LBB20_1
	.p2align	4, 0x90
.LBB20_14:                              # %.thread
                                        #   in Loop: Header=BB20_1 Depth=1
	movb	%al, (%rbx)
	incq	%rbx
	movl	%eax, %ebp
.LBB20_1:                               # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-61(%rax), %ecx
	cmpl	$2, %ecx
	jb	.LBB20_13
# BB#2:                                 #   in Loop: Header=BB20_1 Depth=1
	cmpl	$40, %eax
	je	.LBB20_13
# BB#3:                                 #   in Loop: Header=BB20_1 Depth=1
	cmpl	$-1, %eax
	jne	.LBB20_14
	jmp	.LBB20_4
	.p2align	4, 0x90
.LBB20_13:                              #   in Loop: Header=BB20_1 Depth=1
	cmpl	$10, %ebp
	jne	.LBB20_14
	jmp	.LBB20_5
.LBB20_4:
	movl	$-1, %eax
.LBB20_5:                               # %.critedge
	movl	%eax, %edi
	movq	%r14, %rsi
	callq	ungetc
	movb	$0, (%rbx)
	cmpl	$100, dorp(%rip)
	jne	.LBB20_6
# BB#15:                                # %.outer.i.preheader
	movabsq	$109951162777600, %r15  # imm = 0x640000000000
	movq	%r12, %r14
	movq	%r12, %r13
	jmp	.LBB20_16
	.p2align	4, 0x90
.LBB20_21:                              #   in Loop: Header=BB20_16 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movb	(%rax,%rbx,4), %al
	movb	%al, (%r14)
	incq	%r14
.LBB20_16:                              # %.outer.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_17 Depth 2
	incq	%r13
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB20_17:                              #   Parent Loop BB20_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %r13
	movsbq	-1(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB20_22
# BB#18:                                #   in Loop: Header=BB20_17 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rax
	testb	$4, 1(%rax,%rbx,2)
	jne	.LBB20_21
# BB#19:                                # %switch.early.test.i
                                        #   in Loop: Header=BB20_17 Depth=2
	leaq	1(%r13), %rax
	cmpb	$46, %bl
	ja	.LBB20_17
# BB#20:                                # %switch.early.test.i
                                        #   in Loop: Header=BB20_17 Depth=2
	movzbl	%bl, %ecx
	btq	%rcx, %r15
	jae	.LBB20_17
	jmp	.LBB20_21
.LBB20_6:
	movabsq	$109951162777600, %r15  # imm = 0x640000000000
	movq	%r12, %r14
	movq	%r12, %r13
	jmp	.LBB20_7
	.p2align	4, 0x90
.LBB20_12:                              #   in Loop: Header=BB20_7 Depth=1
	callq	__ctype_toupper_loc
	movq	(%rax), %rax
	movb	(%rax,%rbx,4), %al
	movb	%al, (%r14)
	incq	%r14
.LBB20_7:                               # %.outer.i26
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_8 Depth 2
	incq	%r13
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB20_8:                               #   Parent Loop BB20_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %r13
	movsbq	-1(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB20_22
# BB#9:                                 #   in Loop: Header=BB20_8 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rax
	testb	$4, 1(%rax,%rbx,2)
	jne	.LBB20_12
# BB#10:                                # %switch.early.test.i28
                                        #   in Loop: Header=BB20_8 Depth=2
	leaq	1(%r13), %rax
	cmpb	$46, %bl
	ja	.LBB20_8
# BB#11:                                # %switch.early.test.i28
                                        #   in Loop: Header=BB20_8 Depth=2
	movzbl	%bl, %ecx
	btq	%rcx, %r15
	jae	.LBB20_8
	jmp	.LBB20_12
.LBB20_22:                              # %.backedge.i.preheader
	movb	$0, (%r14)
	jmp	.LBB20_23
	.p2align	4, 0x90
.LBB20_27:                              # %.backedge.backedge.i
                                        #   in Loop: Header=BB20_23 Depth=1
	incq	%r12
.LBB20_23:                              # %.backedge.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r12), %eax
	cmpb	$42, %al
	je	.LBB20_26
# BB#24:                                # %.backedge.i
                                        #   in Loop: Header=BB20_23 Depth=1
	testb	%al, %al
	jne	.LBB20_27
	jmp	.LBB20_25
	.p2align	4, 0x90
.LBB20_26:                              # %.thread.i
                                        #   in Loop: Header=BB20_23 Depth=1
	movb	$45, (%r12)
	jmp	.LBB20_27
.LBB20_25:                              # %kake2hiku.exit
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	load1SeqWithoutName_new, .Lfunc_end20-load1SeqWithoutName_new
	.cfi_endproc

	.globl	readDataforgaln
	.p2align	4, 0x90
	.type	readDataforgaln,@function
readDataforgaln:                        # @readDataforgaln
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi186:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi187:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi188:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi189:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi190:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi191:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi192:
	.cfi_def_cfa_offset 64
.Lcfi193:
	.cfi_offset %rbx, -56
.Lcfi194:
	.cfi_offset %r12, -48
.Lcfi195:
	.cfi_offset %r13, -40
.Lcfi196:
	.cfi_offset %r14, -32
.Lcfi197:
	.cfi_offset %r15, -24
.Lcfi198:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %rbx
	callq	rewind
	movl	$10, %eax
	movabsq	$-4611686018427387903, %r14 # imm = 0xC000000000000001
	.p2align	4, 0x90
.LBB21_1:                               # %.critedge.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ecx
	incl	%ecx
	cmpl	$63, %ecx
	ja	.LBB21_1
# BB#2:                                 # %.critedge.i
                                        #   in Loop: Header=BB21_1 Depth=1
	btq	%rcx, %r14
	jae	.LBB21_1
# BB#3:                                 #   in Loop: Header=BB21_1 Depth=1
	cmpl	$10, %ebp
	jne	.LBB21_1
# BB#4:                                 # %searchKUorWA.exit
	movl	%eax, %edi
	movq	%rbx, %rsi
	callq	ungetc
	movl	njob(%rip), %eax
	testl	%eax, %eax
	jle	.LBB21_14
# BB#5:                                 # %.lr.ph.preheader
	xorl	%r13d, %r13d
	movq	(%rsp), %r14            # 8-byte Reload
	.p2align	4, 0x90
.LBB21_6:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_8 Depth 2
                                        #     Child Loop BB21_12 Depth 2
	movq	(%r15,%r13,8), %rax
	movb	$61, (%rax)
	movq	%rbx, %rdi
	callq	_IO_getc
	movq	(%r15,%r13,8), %rbp
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB21_13
# BB#7:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB21_6 Depth=1
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB21_8:                               # %.lr.ph.i
                                        #   Parent Loop BB21_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB21_9
# BB#10:                                #   in Loop: Header=BB21_8 Depth=2
	movb	%al, 1(%rbp,%r14)
	incq	%r14
	cmpl	$254, %r14d
	jl	.LBB21_8
# BB#11:                                # %.critedge.i19
                                        #   in Loop: Header=BB21_6 Depth=1
	movb	$0, 1(%rbp,%r14)
	movq	(%rsp), %r14            # 8-byte Reload
	.p2align	4, 0x90
.LBB21_12:                              #   Parent Loop BB21_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	jne	.LBB21_12
	jmp	.LBB21_13
.LBB21_9:                               # %.critedge.thread.i
                                        #   in Loop: Header=BB21_6 Depth=1
	movb	$0, 1(%rbp,%r14)
	movq	(%rsp), %r14            # 8-byte Reload
	.p2align	4, 0x90
.LBB21_13:                              # %myfgets.exit
                                        #   in Loop: Header=BB21_6 Depth=1
	movq	%rbx, %rdi
	callq	load1SeqWithoutName_realloc
	movq	%rax, readDataforgaln.tmpseq(%rip)
	movq	(%r12,%r13,8), %rdi
	movq	%rax, %rsi
	callq	strcpy
	movq	(%r12,%r13,8), %rdi
	callq	strlen
	movl	%eax, (%r14,%r13,4)
	movq	readDataforgaln.tmpseq(%rip), %rdi
	callq	free
	incq	%r13
	movslq	njob(%rip), %rax
	cmpq	%rax, %r13
	jl	.LBB21_6
.LBB21_14:                              # %._crit_edge
	testl	%eax, %eax
	jle	.LBB21_26
# BB#15:                                # %._crit_edge
	cmpl	$100, dorp(%rip)
	jne	.LBB21_26
# BB#16:                                # %._crit_edge
	cmpl	$-1, upperCase(%rip)
	je	.LBB21_26
# BB#17:                                # %.lr.ph26.preheader.i
	movl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB21_18:                              # %.lr.ph26.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_24 Depth 2
	movq	(%r12,%r15,8), %r13
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %r14
	testl	%r14d, %r14d
	jle	.LBB21_25
# BB#19:                                # %.lr.ph.i20
                                        #   in Loop: Header=BB21_18 Depth=1
	callq	__ctype_tolower_loc
	movl	%r14d, %r8d
	movq	(%rax), %rdx
	movsbq	(%r13), %rsi
	movb	(%rdx,%rsi,4), %dl
	movb	%dl, (%r13)
	cmpq	$1, %r8
	je	.LBB21_25
# BB#20:                                # %._crit_edge32.i.preheader
                                        #   in Loop: Header=BB21_18 Depth=1
	testb	$1, %r14b
	jne	.LBB21_21
# BB#22:                                # %._crit_edge32.i.prol
                                        #   in Loop: Header=BB21_18 Depth=1
	movq	(%r12,%r15,8), %rbp
	movq	(%rax), %rsi
	movsbq	1(%rbp), %rdi
	movb	(%rsi,%rdi,4), %dl
	movb	%dl, 1(%rbp)
	movl	$2, %edx
	cmpq	$2, %r8
	jne	.LBB21_24
	jmp	.LBB21_25
.LBB21_21:                              #   in Loop: Header=BB21_18 Depth=1
	movl	$1, %edx
	cmpq	$2, %r8
	je	.LBB21_25
	.p2align	4, 0x90
.LBB21_24:                              # %._crit_edge32.i
                                        #   Parent Loop BB21_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%r15,8), %rsi
	movq	(%rax), %rdi
	movsbq	(%rsi,%rdx), %rbp
	movzbl	(%rdi,%rbp,4), %ecx
	movb	%cl, (%rsi,%rdx)
	movq	(%r12,%r15,8), %rcx
	movq	(%rax), %rsi
	movsbq	1(%rcx,%rdx), %rdi
	movzbl	(%rsi,%rdi,4), %ebx
	movb	%bl, 1(%rcx,%rdx)
	addq	$2, %rdx
	cmpq	%rdx, %r8
	jne	.LBB21_24
	.p2align	4, 0x90
.LBB21_25:                              # %._crit_edge.i
                                        #   in Loop: Header=BB21_18 Depth=1
	incq	%r15
	cmpq	(%rsp), %r15            # 8-byte Folded Reload
	jne	.LBB21_18
.LBB21_26:                              # %seqLower.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	readDataforgaln, .Lfunc_end21-readDataforgaln
	.cfi_endproc

	.globl	readData_varlen
	.p2align	4, 0x90
	.type	readData_varlen,@function
readData_varlen:                        # @readData_varlen
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi199:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi200:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi201:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi202:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi203:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi204:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi205:
	.cfi_def_cfa_offset 80
.Lcfi206:
	.cfi_offset %rbx, -56
.Lcfi207:
	.cfi_offset %r12, -48
.Lcfi208:
	.cfi_offset %r13, -40
.Lcfi209:
	.cfi_offset %r14, -32
.Lcfi210:
	.cfi_offset %r15, -24
.Lcfi211:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r15
	callq	rewind
	movl	$10, %eax
	movabsq	$-4611686018427387903, %rbp # imm = 0xC000000000000001
	.p2align	4, 0x90
.LBB22_1:                               # %.critedge.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebx
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %ecx
	incl	%ecx
	cmpl	$63, %ecx
	ja	.LBB22_1
# BB#2:                                 # %.critedge.i
                                        #   in Loop: Header=BB22_1 Depth=1
	btq	%rcx, %rbp
	jae	.LBB22_1
# BB#3:                                 #   in Loop: Header=BB22_1 Depth=1
	cmpl	$10, %ebx
	jne	.LBB22_1
# BB#4:                                 # %searchKUorWA.exit
	movl	%eax, %edi
	movq	%r15, %rsi
	callq	ungetc
	movl	njob(%rip), %eax
	testl	%eax, %eax
	jle	.LBB22_14
# BB#5:                                 # %.lr.ph.preheader
	xorl	%r14d, %r14d
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB22_6:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_8 Depth 2
                                        #     Child Loop BB22_12 Depth 2
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp,%r14,8), %rax
	movb	$61, (%rax)
	movq	%r15, %rdi
	callq	_IO_getc
	movq	(%rbp,%r14,8), %rbp
	movq	%r15, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB22_13
# BB#7:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB22_6 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB22_8:                               # %.lr.ph.i
                                        #   Parent Loop BB22_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB22_9
# BB#10:                                #   in Loop: Header=BB22_8 Depth=2
	movb	%al, 1(%rbp,%rbx)
	incq	%rbx
	cmpl	$254, %ebx
	jl	.LBB22_8
# BB#11:                                # %.critedge.i21
                                        #   in Loop: Header=BB22_6 Depth=1
	movb	$0, 1(%rbp,%rbx)
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB22_12:                              #   Parent Loop BB22_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	jne	.LBB22_12
	jmp	.LBB22_13
.LBB22_9:                               # %.critedge.thread.i
                                        #   in Loop: Header=BB22_6 Depth=1
	movb	$0, 1(%rbp,%rbx)
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB22_13:                              # %myfgets.exit
                                        #   in Loop: Header=BB22_6 Depth=1
	movq	%r15, %rdi
	callq	load1SeqWithoutName_realloc
	movq	%rax, %r13
	movq	%r13, readData_varlen.tmpseq(%rip)
	movq	%r13, %rdi
	callq	strlen
	movl	%eax, (%rbx,%r14,4)
	shlq	$32, %rax
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	leaq	(%rax,%rcx), %rdi
	sarq	$32, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, (%r12,%r14,8)
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	strcpy
	movq	readData_varlen.tmpseq(%rip), %rdi
	callq	free
	incq	%r14
	movslq	njob(%rip), %rax
	cmpq	%rax, %r14
	jl	.LBB22_6
.LBB22_14:                              # %._crit_edge
	testl	%eax, %eax
	jle	.LBB22_26
# BB#15:                                # %._crit_edge
	cmpl	$100, dorp(%rip)
	jne	.LBB22_26
# BB#16:                                # %._crit_edge
	cmpl	$-1, upperCase(%rip)
	je	.LBB22_26
# BB#17:                                # %.lr.ph26.preheader.i
	movl	%eax, %r15d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB22_18:                              # %.lr.ph26.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_24 Depth 2
	movq	(%r12,%rbp,8), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r14
	testl	%r14d, %r14d
	jle	.LBB22_25
# BB#19:                                # %.lr.ph.i22
                                        #   in Loop: Header=BB22_18 Depth=1
	callq	__ctype_tolower_loc
	movl	%r14d, %ecx
	movq	(%rax), %rdx
	movsbq	(%rbx), %rsi
	movb	(%rdx,%rsi,4), %dl
	movb	%dl, (%rbx)
	cmpq	$1, %rcx
	je	.LBB22_25
# BB#20:                                # %._crit_edge32.i.preheader
                                        #   in Loop: Header=BB22_18 Depth=1
	testb	$1, %r14b
	jne	.LBB22_21
# BB#22:                                # %._crit_edge32.i.prol
                                        #   in Loop: Header=BB22_18 Depth=1
	movq	(%r12,%rbp,8), %rdx
	movq	(%rax), %rsi
	movsbq	1(%rdx), %rdi
	movb	(%rsi,%rdi,4), %bl
	movb	%bl, 1(%rdx)
	movl	$2, %edx
	cmpq	$2, %rcx
	jne	.LBB22_24
	jmp	.LBB22_25
.LBB22_21:                              #   in Loop: Header=BB22_18 Depth=1
	movl	$1, %edx
	cmpq	$2, %rcx
	je	.LBB22_25
	.p2align	4, 0x90
.LBB22_24:                              # %._crit_edge32.i
                                        #   Parent Loop BB22_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%rbp,8), %rsi
	movq	(%rax), %rdi
	movsbq	(%rsi,%rdx), %rbx
	movzbl	(%rdi,%rbx,4), %ebx
	movb	%bl, (%rsi,%rdx)
	movq	(%r12,%rbp,8), %rsi
	movq	(%rax), %rdi
	movsbq	1(%rsi,%rdx), %rbx
	movzbl	(%rdi,%rbx,4), %ebx
	movb	%bl, 1(%rsi,%rdx)
	addq	$2, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB22_24
	.p2align	4, 0x90
.LBB22_25:                              # %._crit_edge.i
                                        #   in Loop: Header=BB22_18 Depth=1
	incq	%rbp
	cmpq	%r15, %rbp
	jne	.LBB22_18
.LBB22_26:                              # %seqLower.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	readData_varlen, .Lfunc_end22-readData_varlen
	.cfi_endproc

	.globl	readData_pointer
	.p2align	4, 0x90
	.type	readData_pointer,@function
readData_pointer:                       # @readData_pointer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi212:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi213:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi214:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi215:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi216:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi217:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi218:
	.cfi_def_cfa_offset 64
.Lcfi219:
	.cfi_offset %rbx, -56
.Lcfi220:
	.cfi_offset %r12, -48
.Lcfi221:
	.cfi_offset %r13, -40
.Lcfi222:
	.cfi_offset %r14, -32
.Lcfi223:
	.cfi_offset %r15, -24
.Lcfi224:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %rbx
	callq	rewind
	movl	$10, %eax
	movabsq	$-4611686018427387903, %r14 # imm = 0xC000000000000001
	.p2align	4, 0x90
.LBB23_1:                               # %.critedge.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ecx
	incl	%ecx
	cmpl	$63, %ecx
	ja	.LBB23_1
# BB#2:                                 # %.critedge.i
                                        #   in Loop: Header=BB23_1 Depth=1
	btq	%rcx, %r14
	jae	.LBB23_1
# BB#3:                                 #   in Loop: Header=BB23_1 Depth=1
	cmpl	$10, %ebp
	jne	.LBB23_1
# BB#4:                                 # %searchKUorWA.exit
	movl	%eax, %edi
	movq	%rbx, %rsi
	callq	ungetc
	movl	njob(%rip), %eax
	testl	%eax, %eax
	jle	.LBB23_14
# BB#5:                                 # %.lr.ph.preheader
	xorl	%r13d, %r13d
	movq	(%rsp), %r14            # 8-byte Reload
	.p2align	4, 0x90
.LBB23_6:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_8 Depth 2
                                        #     Child Loop BB23_12 Depth 2
	movq	(%r15,%r13,8), %rax
	movb	$61, (%rax)
	movq	%rbx, %rdi
	callq	_IO_getc
	movq	(%r15,%r13,8), %rbp
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB23_13
# BB#7:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB23_6 Depth=1
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB23_8:                               # %.lr.ph.i
                                        #   Parent Loop BB23_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB23_9
# BB#10:                                #   in Loop: Header=BB23_8 Depth=2
	movb	%al, 1(%rbp,%r14)
	incq	%r14
	cmpl	$254, %r14d
	jl	.LBB23_8
# BB#11:                                # %.critedge.i19
                                        #   in Loop: Header=BB23_6 Depth=1
	movb	$0, 1(%rbp,%r14)
	movq	(%rsp), %r14            # 8-byte Reload
	.p2align	4, 0x90
.LBB23_12:                              #   Parent Loop BB23_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	jne	.LBB23_12
	jmp	.LBB23_13
.LBB23_9:                               # %.critedge.thread.i
                                        #   in Loop: Header=BB23_6 Depth=1
	movb	$0, 1(%rbp,%r14)
	movq	(%rsp), %r14            # 8-byte Reload
	.p2align	4, 0x90
.LBB23_13:                              # %myfgets.exit
                                        #   in Loop: Header=BB23_6 Depth=1
	movq	%rbx, %rdi
	callq	load1SeqWithoutName_realloc
	movq	%rax, readData_pointer.tmpseq(%rip)
	movq	(%r12,%r13,8), %rdi
	movq	%rax, %rsi
	callq	strcpy
	movq	readData_pointer.tmpseq(%rip), %rdi
	callq	free
	movq	(%r12,%r13,8), %rdi
	callq	strlen
	movl	%eax, (%r14,%r13,4)
	incq	%r13
	movslq	njob(%rip), %rax
	cmpq	%rax, %r13
	jl	.LBB23_6
.LBB23_14:                              # %._crit_edge
	testl	%eax, %eax
	jle	.LBB23_26
# BB#15:                                # %._crit_edge
	cmpl	$100, dorp(%rip)
	jne	.LBB23_26
# BB#16:                                # %._crit_edge
	cmpl	$-1, upperCase(%rip)
	je	.LBB23_26
# BB#17:                                # %.lr.ph26.preheader.i
	movl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB23_18:                              # %.lr.ph26.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_24 Depth 2
	movq	(%r12,%r15,8), %r13
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %r14
	testl	%r14d, %r14d
	jle	.LBB23_25
# BB#19:                                # %.lr.ph.i20
                                        #   in Loop: Header=BB23_18 Depth=1
	callq	__ctype_tolower_loc
	movl	%r14d, %r8d
	movq	(%rax), %rdx
	movsbq	(%r13), %rsi
	movb	(%rdx,%rsi,4), %dl
	movb	%dl, (%r13)
	cmpq	$1, %r8
	je	.LBB23_25
# BB#20:                                # %._crit_edge32.i.preheader
                                        #   in Loop: Header=BB23_18 Depth=1
	testb	$1, %r14b
	jne	.LBB23_21
# BB#22:                                # %._crit_edge32.i.prol
                                        #   in Loop: Header=BB23_18 Depth=1
	movq	(%r12,%r15,8), %rbp
	movq	(%rax), %rsi
	movsbq	1(%rbp), %rdi
	movb	(%rsi,%rdi,4), %dl
	movb	%dl, 1(%rbp)
	movl	$2, %edx
	cmpq	$2, %r8
	jne	.LBB23_24
	jmp	.LBB23_25
.LBB23_21:                              #   in Loop: Header=BB23_18 Depth=1
	movl	$1, %edx
	cmpq	$2, %r8
	je	.LBB23_25
	.p2align	4, 0x90
.LBB23_24:                              # %._crit_edge32.i
                                        #   Parent Loop BB23_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%r15,8), %rsi
	movq	(%rax), %rdi
	movsbq	(%rsi,%rdx), %rbp
	movzbl	(%rdi,%rbp,4), %ecx
	movb	%cl, (%rsi,%rdx)
	movq	(%r12,%r15,8), %rcx
	movq	(%rax), %rsi
	movsbq	1(%rcx,%rdx), %rdi
	movzbl	(%rsi,%rdi,4), %ebx
	movb	%bl, 1(%rcx,%rdx)
	addq	$2, %rdx
	cmpq	%rdx, %r8
	jne	.LBB23_24
	.p2align	4, 0x90
.LBB23_25:                              # %._crit_edge.i
                                        #   in Loop: Header=BB23_18 Depth=1
	incq	%r15
	cmpq	(%rsp), %r15            # 8-byte Folded Reload
	jne	.LBB23_18
.LBB23_26:                              # %seqLower.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	readData_pointer, .Lfunc_end23-readData_pointer
	.cfi_endproc

	.globl	readData
	.p2align	4, 0x90
	.type	readData,@function
readData:                               # @readData
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi225:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi226:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi227:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi228:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi229:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi230:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi231:
	.cfi_def_cfa_offset 64
.Lcfi232:
	.cfi_offset %rbx, -56
.Lcfi233:
	.cfi_offset %r12, -48
.Lcfi234:
	.cfi_offset %r13, -40
.Lcfi235:
	.cfi_offset %r14, -32
.Lcfi236:
	.cfi_offset %r15, -24
.Lcfi237:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %rbx
	callq	rewind
	movl	$10, %eax
	movabsq	$-4611686018427387903, %r14 # imm = 0xC000000000000001
	.p2align	4, 0x90
.LBB24_1:                               # %.critedge.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ecx
	incl	%ecx
	cmpl	$63, %ecx
	ja	.LBB24_1
# BB#2:                                 # %.critedge.i
                                        #   in Loop: Header=BB24_1 Depth=1
	btq	%rcx, %r14
	jae	.LBB24_1
# BB#3:                                 #   in Loop: Header=BB24_1 Depth=1
	cmpl	$10, %ebp
	jne	.LBB24_1
# BB#4:                                 # %searchKUorWA.exit
	movl	%eax, %edi
	movq	%rbx, %rsi
	callq	ungetc
	movl	njob(%rip), %eax
	testl	%eax, %eax
	jle	.LBB24_14
# BB#5:                                 # %.lr.ph.preheader
	xorl	%r13d, %r13d
	movq	(%rsp), %r14            # 8-byte Reload
	.p2align	4, 0x90
.LBB24_6:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_8 Depth 2
                                        #     Child Loop BB24_12 Depth 2
	movq	%r13, %rbp
	shlq	$8, %rbp
	movb	$61, (%r15,%rbp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB24_13
# BB#7:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB24_6 Depth=1
	leaq	1(%r15,%rbp), %rbp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB24_8:                               # %.lr.ph.i
                                        #   Parent Loop BB24_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB24_9
# BB#10:                                #   in Loop: Header=BB24_8 Depth=2
	movb	%al, (%rbp)
	incq	%rbp
	incl	%r14d
	cmpl	$254, %r14d
	jl	.LBB24_8
# BB#11:                                # %.critedge.i19
                                        #   in Loop: Header=BB24_6 Depth=1
	movb	$0, (%rbp)
	movq	(%rsp), %r14            # 8-byte Reload
	.p2align	4, 0x90
.LBB24_12:                              #   Parent Loop BB24_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	jne	.LBB24_12
	jmp	.LBB24_13
.LBB24_9:                               # %.critedge.thread.i
                                        #   in Loop: Header=BB24_6 Depth=1
	movb	$0, (%rbp)
	movq	(%rsp), %r14            # 8-byte Reload
	.p2align	4, 0x90
.LBB24_13:                              # %myfgets.exit
                                        #   in Loop: Header=BB24_6 Depth=1
	movq	%rbx, %rdi
	callq	load1SeqWithoutName_realloc
	movq	%rax, readData.tmpseq(%rip)
	movq	(%r12,%r13,8), %rdi
	movq	%rax, %rsi
	callq	strcpy
	movq	(%r12,%r13,8), %rdi
	callq	strlen
	movl	%eax, (%r14,%r13,4)
	movq	readData.tmpseq(%rip), %rdi
	callq	free
	incq	%r13
	movslq	njob(%rip), %rax
	cmpq	%rax, %r13
	jl	.LBB24_6
.LBB24_14:                              # %._crit_edge
	testl	%eax, %eax
	jle	.LBB24_26
# BB#15:                                # %._crit_edge
	cmpl	$100, dorp(%rip)
	jne	.LBB24_26
# BB#16:                                # %._crit_edge
	cmpl	$-1, upperCase(%rip)
	je	.LBB24_26
# BB#17:                                # %.lr.ph26.preheader.i
	movl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB24_18:                              # %.lr.ph26.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_24 Depth 2
	movq	(%r12,%r15,8), %r13
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %r14
	testl	%r14d, %r14d
	jle	.LBB24_25
# BB#19:                                # %.lr.ph.i20
                                        #   in Loop: Header=BB24_18 Depth=1
	callq	__ctype_tolower_loc
	movl	%r14d, %r8d
	movq	(%rax), %rdx
	movsbq	(%r13), %rsi
	movb	(%rdx,%rsi,4), %dl
	movb	%dl, (%r13)
	cmpq	$1, %r8
	je	.LBB24_25
# BB#20:                                # %._crit_edge32.i.preheader
                                        #   in Loop: Header=BB24_18 Depth=1
	testb	$1, %r14b
	jne	.LBB24_21
# BB#22:                                # %._crit_edge32.i.prol
                                        #   in Loop: Header=BB24_18 Depth=1
	movq	(%r12,%r15,8), %rbp
	movq	(%rax), %rsi
	movsbq	1(%rbp), %rdi
	movb	(%rsi,%rdi,4), %dl
	movb	%dl, 1(%rbp)
	movl	$2, %edx
	cmpq	$2, %r8
	jne	.LBB24_24
	jmp	.LBB24_25
.LBB24_21:                              #   in Loop: Header=BB24_18 Depth=1
	movl	$1, %edx
	cmpq	$2, %r8
	je	.LBB24_25
	.p2align	4, 0x90
.LBB24_24:                              # %._crit_edge32.i
                                        #   Parent Loop BB24_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%r15,8), %rsi
	movq	(%rax), %rdi
	movsbq	(%rsi,%rdx), %rbp
	movzbl	(%rdi,%rbp,4), %ecx
	movb	%cl, (%rsi,%rdx)
	movq	(%r12,%r15,8), %rcx
	movq	(%rax), %rsi
	movsbq	1(%rcx,%rdx), %rdi
	movzbl	(%rsi,%rdi,4), %ebx
	movb	%bl, 1(%rcx,%rdx)
	addq	$2, %rdx
	cmpq	%rdx, %r8
	jne	.LBB24_24
	.p2align	4, 0x90
.LBB24_25:                              # %._crit_edge.i
                                        #   in Loop: Header=BB24_18 Depth=1
	incq	%r15
	cmpq	(%rsp), %r15            # 8-byte Folded Reload
	jne	.LBB24_18
.LBB24_26:                              # %seqLower.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	readData, .Lfunc_end24-readData
	.cfi_endproc

	.globl	countATGC
	.p2align	4, 0x90
	.type	countATGC,@function
countATGC:                              # @countATGC
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi238:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi239:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi240:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi241:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi242:
	.cfi_def_cfa_offset 48
.Lcfi243:
	.cfi_offset %rbx, -40
.Lcfi244:
	.cfi_offset %r12, -32
.Lcfi245:
	.cfi_offset %r14, -24
.Lcfi246:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movb	(%r12), %bl
	testb	%bl, %bl
	je	.LBB25_1
# BB#2:                                 # %.preheader
	callq	__ctype_tolower_loc
	movq	(%rax), %r15
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	incq	%r12
	xorl	%eax, %eax
	movl	$1581125, %edx          # imm = 0x182045
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB25_3:                               # =>This Inner Loop Header: Depth=1
	movsbq	%bl, %rdi
	movsbl	(%r15,%rdi,4), %edi
	movslq	%edi, %rbx
	testb	$4, 1(%rcx,%rbx,2)
	je	.LBB25_7
# BB#4:                                 #   in Loop: Header=BB25_3 Depth=1
	incl	%esi
	addl	$-97, %edi
	cmpl	$20, %edi
	ja	.LBB25_7
# BB#5:                                 #   in Loop: Header=BB25_3 Depth=1
	btl	%edi, %edx
	jae	.LBB25_7
# BB#6:                                 #   in Loop: Header=BB25_3 Depth=1
	incl	%eax
.LBB25_7:                               #   in Loop: Header=BB25_3 Depth=1
	movzbl	(%r12), %ebx
	incq	%r12
	testb	%bl, %bl
	jne	.LBB25_3
# BB#8:
	movl	%esi, (%r14)
	jmp	.LBB25_9
.LBB25_1:
	xorl	%eax, %eax
.LBB25_9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end25:
	.size	countATGC, .Lfunc_end25-countATGC
	.cfi_endproc

	.globl	countATGCbk
	.p2align	4, 0x90
	.type	countATGCbk,@function
countATGCbk:                            # @countATGCbk
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi247:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi248:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi249:
	.cfi_def_cfa_offset 32
.Lcfi250:
	.cfi_offset %rbx, -24
.Lcfi251:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	callq	__ctype_tolower_loc
	movq	(%rax), %r14
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movb	(%rbx), %dl
	incq	%rbx
	xorl	%ecx, %ecx
	movl	$1581125, %r8d          # imm = 0x182045
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB26_1:                               # =>This Inner Loop Header: Depth=1
	movsbq	%dl, %rdx
	movsbl	(%r14,%rdx,4), %edx
	movslq	%edx, %rdi
	testb	$4, 1(%rax,%rdi,2)
	je	.LBB26_5
# BB#2:                                 #   in Loop: Header=BB26_1 Depth=1
	incl	%ecx
	addl	$-97, %edx
	cmpl	$20, %edx
	ja	.LBB26_5
# BB#3:                                 #   in Loop: Header=BB26_1 Depth=1
	btl	%edx, %r8d
	jae	.LBB26_5
# BB#4:                                 #   in Loop: Header=BB26_1 Depth=1
	incl	%esi
.LBB26_5:                               #   in Loop: Header=BB26_1 Depth=1
	movzbl	(%rbx), %edx
	incq	%rbx
	testb	%dl, %dl
	jne	.LBB26_1
# BB#6:
	cvtsi2sdl	%esi, %xmm0
	cvtsi2sdl	%ecx, %xmm1
	divsd	%xmm1, %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end26:
	.size	countATGCbk, .Lfunc_end26-countATGCbk
	.cfi_endproc

	.globl	countalpha
	.p2align	4, 0x90
	.type	countalpha,@function
countalpha:                             # @countalpha
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi252:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi253:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi254:
	.cfi_def_cfa_offset 32
.Lcfi255:
	.cfi_offset %rbx, -24
.Lcfi256:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movb	(%rbx), %bpl
	testb	%bpl, %bpl
	je	.LBB27_1
# BB#2:                                 # %.lr.ph
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	incq	%rbx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB27_3:                               # =>This Inner Loop Header: Depth=1
	movsbq	%bpl, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	shrl	$10, %edx
	andl	$1, %edx
	addl	%edx, %eax
	movzbl	(%rbx), %ebp
	incq	%rbx
	testb	%bpl, %bpl
	jne	.LBB27_3
	jmp	.LBB27_4
.LBB27_1:
	xorl	%eax, %eax
.LBB27_4:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end27:
	.size	countalpha, .Lfunc_end27-countalpha
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI28_0:
	.quad	4604930618986332160     # double 0.75
	.text
	.globl	getnumlen_nogap
	.p2align	4, 0x90
	.type	getnumlen_nogap,@function
getnumlen_nogap:                        # @getnumlen_nogap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi257:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi258:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi259:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi260:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi261:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi262:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi263:
	.cfi_def_cfa_offset 96
.Lcfi264:
	.cfi_offset %rbx, -56
.Lcfi265:
	.cfi_offset %r12, -48
.Lcfi266:
	.cfi_offset %r13, -40
.Lcfi267:
	.cfi_offset %r14, -32
.Lcfi268:
	.cfi_offset %r15, -24
.Lcfi269:
	.cfi_offset %rbp, -16
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r15
	movl	$5000000, %edi          # imm = 0x4C4B40
	callq	AllocateCharVec
	movq	%rax, %r14
	movq	%r15, %rdi
	callq	_IO_getc
	xorl	%ebp, %ebp
	cmpl	$-1, %eax
	je	.LBB28_3
# BB#1:                                 # %.lr.ph.i.preheader
	movl	$10, %ecx
	.p2align	4, 0x90
.LBB28_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebx
	cmpl	$10, %ecx
	sete	%al
	leal	-61(%rbx), %ecx
	cmpl	$2, %ecx
	sbbb	%cl, %cl
	andb	%al, %cl
	movzbl	%cl, %eax
	addl	%eax, %ebp
	movq	%r15, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	movl	%ebx, %ecx
	jne	.LBB28_2
.LBB28_3:                               # %countKUorWA.exit
	movq	%r15, %rdi
	callq	rewind
	movl	%ebp, njob(%rip)
	movl	$10, %eax
	movabsq	$-4611686018427387903, %rbx # imm = 0xC000000000000001
	.p2align	4, 0x90
.LBB28_4:                               # %.critedge.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebp
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %ecx
	incl	%ecx
	cmpl	$63, %ecx
	ja	.LBB28_4
# BB#5:                                 # %.critedge.i
                                        #   in Loop: Header=BB28_4 Depth=1
	btq	%rcx, %rbx
	jae	.LBB28_4
# BB#6:                                 #   in Loop: Header=BB28_4 Depth=1
	cmpl	$10, %ebp
	jne	.LBB28_4
# BB#7:                                 # %searchKUorWA.exit
	movl	%eax, %edi
	movq	%r15, %rsi
	callq	ungetc
	movl	$0, nlenmax(%rip)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	$99999999, (%rax)       # imm = 0x5F5E0FF
	cmpl	$0, njob(%rip)
	jle	.LBB28_8
# BB#9:                                 # %.lr.ph.preheader
	xorl	%r13d, %r13d
	movl	$0, (%rsp)              # 4-byte Folded Spill
	xorl	%eax, %eax
                                        # implicit-def: %EBP
	movq	%r14, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB28_10:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB28_12 Depth 2
                                        #     Child Loop BB28_16 Depth 2
                                        #     Child Loop BB28_20 Depth 2
                                        #     Child Loop BB28_28 Depth 2
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movq	%r15, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB28_17
# BB#11:                                # %.lr.ph.i38.preheader
                                        #   in Loop: Header=BB28_10 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB28_12:                              # %.lr.ph.i38
                                        #   Parent Loop BB28_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB28_13
# BB#14:                                #   in Loop: Header=BB28_12 Depth=2
	movb	%al, (%r14,%rbx)
	incq	%rbx
	cmpl	$4999999, %ebx          # imm = 0x4C4B3F
	jl	.LBB28_12
# BB#15:                                # %.critedge.i39
                                        #   in Loop: Header=BB28_10 Depth=1
	movb	$0, (%r14,%rbx)
	.p2align	4, 0x90
.LBB28_16:                              #   Parent Loop BB28_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	jne	.LBB28_16
	jmp	.LBB28_17
.LBB28_13:                              # %.critedge.thread.i
                                        #   in Loop: Header=BB28_10 Depth=1
	movb	$0, (%r14,%rbx)
	.p2align	4, 0x90
.LBB28_17:                              # %myfgets.exit
                                        #   in Loop: Header=BB28_10 Depth=1
	movq	%r15, %rdi
	callq	load1SeqWithoutName_realloc
	movq	%rax, %r12
	movb	(%r12), %bl
	testb	%bl, %bl
	je	.LBB28_18
# BB#19:                                # %.lr.ph.i40
                                        #   in Loop: Header=BB28_10 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	leaq	1(%r12), %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB28_20:                              #   Parent Loop BB28_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%bl, %rsi
	movzwl	(%rcx,%rsi,2), %esi
	shrl	$10, %esi
	andl	$1, %esi
	addl	%esi, %eax
	movzbl	(%rdx), %ebx
	incq	%rdx
	testb	%bl, %bl
	jne	.LBB28_20
	jmp	.LBB28_21
	.p2align	4, 0x90
.LBB28_18:                              #   in Loop: Header=BB28_10 Depth=1
	xorl	%eax, %eax
.LBB28_21:                              # %countalpha.exit
                                        #   in Loop: Header=BB28_10 Depth=1
	cmpl	nlenmax(%rip), %eax
	jle	.LBB28_23
# BB#22:                                #   in Loop: Header=BB28_10 Depth=1
	movl	%eax, nlenmax(%rip)
.LBB28_23:                              #   in Loop: Header=BB28_10 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx), %eax
	jge	.LBB28_25
# BB#24:                                #   in Loop: Header=BB28_10 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
.LBB28_25:                              #   in Loop: Header=BB28_10 Depth=1
	movb	(%r12), %r14b
	testb	%r14b, %r14b
	movq	%r13, 8(%rsp)           # 8-byte Spill
	je	.LBB28_26
# BB#27:                                # %.preheader.i41
                                        #   in Loop: Header=BB28_10 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %r13
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movq	%r12, %rdx
	incq	%rdx
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	movl	(%rsp), %ebx            # 4-byte Reload
	.p2align	4, 0x90
.LBB28_28:                              #   Parent Loop BB28_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%r14b, %rsi
	movsbl	(%r13,%rsi,4), %esi
	movslq	%esi, %rdi
	testb	$4, 1(%rcx,%rdi,2)
	je	.LBB28_32
# BB#29:                                #   in Loop: Header=BB28_28 Depth=2
	incl	%ebp
	addl	$-97, %esi
	cmpl	$20, %esi
	ja	.LBB28_32
# BB#30:                                #   in Loop: Header=BB28_28 Depth=2
	movl	$1581125, %edi          # imm = 0x182045
	btl	%esi, %edi
	jae	.LBB28_32
# BB#31:                                #   in Loop: Header=BB28_28 Depth=2
	incl	%eax
.LBB28_32:                              #   in Loop: Header=BB28_28 Depth=2
	movzbl	(%rdx), %r14d
	incq	%rdx
	testb	%r14b, %r14b
	jne	.LBB28_28
	jmp	.LBB28_33
	.p2align	4, 0x90
.LBB28_26:                              #   in Loop: Header=BB28_10 Depth=1
	xorl	%eax, %eax
	movl	(%rsp), %ebx            # 4-byte Reload
.LBB28_33:                              # %countATGC.exit
                                        #   in Loop: Header=BB28_10 Depth=1
	addl	%eax, %ebx
	movl	%ebx, (%rsp)            # 4-byte Spill
	movq	8(%rsp), %r13           # 8-byte Reload
	addl	%ebp, %r13d
	movq	%r12, %rdi
	callq	free
	movl	28(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	njob(%rip), %eax
	movq	32(%rsp), %r14          # 8-byte Reload
	jl	.LBB28_10
# BB#34:                                # %._crit_edge.loopexit
	movl	(%rsp), %eax            # 4-byte Reload
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
.LBB28_35:                              # %._crit_edge
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
	callq	free
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movq	stderr(%rip), %rdi
	movl	$.L.str.19, %esi
	movb	$1, %al
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	callq	fprintf
	cmpl	$100009, dorp(%rip)     # imm = 0x186A9
	jne	.LBB28_37
# BB#36:
	xorl	%eax, %eax
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	ucomisd	.LCPI28_0(%rip), %xmm0
	movl	$100, %ecx
	movl	$112, %edx
	cmoval	%ecx, %edx
	movl	$-1, %ecx
	cmovbel	%eax, %ecx
	movl	%edx, dorp(%rip)
	movl	%ecx, upperCase(%rip)
.LBB28_37:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB28_8:
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	xorpd	%xmm0, %xmm0
	jmp	.LBB28_35
.Lfunc_end28:
	.size	getnumlen_nogap, .Lfunc_end28-getnumlen_nogap
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI29_0:
	.quad	4604930618986332160     # double 0.75
	.text
	.globl	getnumlen
	.p2align	4, 0x90
	.type	getnumlen,@function
getnumlen:                              # @getnumlen
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi270:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi271:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi272:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi273:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi274:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi275:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi276:
	.cfi_def_cfa_offset 80
.Lcfi277:
	.cfi_offset %rbx, -56
.Lcfi278:
	.cfi_offset %r12, -48
.Lcfi279:
	.cfi_offset %r13, -40
.Lcfi280:
	.cfi_offset %r14, -32
.Lcfi281:
	.cfi_offset %r15, -24
.Lcfi282:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$5000000, %edi          # imm = 0x4C4B40
	callq	AllocateCharVec
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	_IO_getc
	xorl	%ebp, %ebp
	cmpl	$-1, %eax
	je	.LBB29_3
# BB#1:                                 # %.lr.ph.i.preheader
	movl	$10, %ecx
	.p2align	4, 0x90
.LBB29_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebx
	cmpl	$10, %ecx
	sete	%al
	leal	-61(%rbx), %ecx
	cmpl	$2, %ecx
	sbbb	%cl, %cl
	andb	%al, %cl
	movzbl	%cl, %eax
	addl	%eax, %ebp
	movq	%r14, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	movl	%ebx, %ecx
	jne	.LBB29_2
.LBB29_3:                               # %countKUorWA.exit
	movq	%r14, %rdi
	callq	rewind
	movl	%ebp, njob(%rip)
	movl	$10, %eax
	movabsq	$-4611686018427387903, %rbx # imm = 0xC000000000000001
	.p2align	4, 0x90
.LBB29_4:                               # %.critedge.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ecx
	incl	%ecx
	cmpl	$63, %ecx
	ja	.LBB29_4
# BB#5:                                 # %.critedge.i
                                        #   in Loop: Header=BB29_4 Depth=1
	btq	%rcx, %rbx
	jae	.LBB29_4
# BB#6:                                 #   in Loop: Header=BB29_4 Depth=1
	cmpl	$10, %ebp
	jne	.LBB29_4
# BB#7:                                 # %searchKUorWA.exit
	movl	%eax, %edi
	movq	%r14, %rsi
	callq	ungetc
	movl	$0, nlenmax(%rip)
	cmpl	$0, njob(%rip)
	jle	.LBB29_8
# BB#9:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	xorl	%eax, %eax
                                        # implicit-def: %R13D
	movq	%r15, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB29_10:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB29_12 Depth 2
                                        #     Child Loop BB29_16 Depth 2
                                        #     Child Loop BB29_22 Depth 2
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	%r14, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB29_17
# BB#11:                                # %.lr.ph.i33.preheader
                                        #   in Loop: Header=BB29_10 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB29_12:                              # %.lr.ph.i33
                                        #   Parent Loop BB29_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB29_13
# BB#14:                                #   in Loop: Header=BB29_12 Depth=2
	movb	%al, (%r15,%rbp)
	incq	%rbp
	cmpl	$4999999, %ebp          # imm = 0x4C4B3F
	jl	.LBB29_12
# BB#15:                                # %.critedge.i34
                                        #   in Loop: Header=BB29_10 Depth=1
	movb	$0, (%r15,%rbp)
	.p2align	4, 0x90
.LBB29_16:                              #   Parent Loop BB29_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	jne	.LBB29_16
	jmp	.LBB29_17
.LBB29_13:                              # %.critedge.thread.i
                                        #   in Loop: Header=BB29_10 Depth=1
	movb	$0, (%r15,%rbp)
	.p2align	4, 0x90
.LBB29_17:                              # %myfgets.exit
                                        #   in Loop: Header=BB29_10 Depth=1
	movq	%r14, %rdi
	callq	load1SeqWithoutName_realloc
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	strlen
	cmpl	nlenmax(%rip), %eax
	jle	.LBB29_19
# BB#18:                                #   in Loop: Header=BB29_10 Depth=1
	movl	%eax, nlenmax(%rip)
.LBB29_19:                              #   in Loop: Header=BB29_10 Depth=1
	movb	(%r15), %bpl
	testb	%bpl, %bpl
	movl	%r12d, 12(%rsp)         # 4-byte Spill
	je	.LBB29_20
# BB#21:                                # %.preheader.i35
                                        #   in Loop: Header=BB29_10 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %r12
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movq	%r15, %rdx
	incq	%rdx
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB29_22:                              #   Parent Loop BB29_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	%bpl, %rsi
	movsbl	(%r12,%rsi,4), %esi
	movslq	%esi, %rdi
	testb	$4, 1(%rcx,%rdi,2)
	je	.LBB29_26
# BB#23:                                #   in Loop: Header=BB29_22 Depth=2
	incl	%r13d
	addl	$-97, %esi
	cmpl	$20, %esi
	ja	.LBB29_26
# BB#24:                                #   in Loop: Header=BB29_22 Depth=2
	movl	$1581125, %edi          # imm = 0x182045
	btl	%esi, %edi
	jae	.LBB29_26
# BB#25:                                #   in Loop: Header=BB29_22 Depth=2
	incl	%eax
.LBB29_26:                              #   in Loop: Header=BB29_22 Depth=2
	movzbl	(%rdx), %ebp
	incq	%rdx
	testb	%bpl, %bpl
	jne	.LBB29_22
	jmp	.LBB29_27
	.p2align	4, 0x90
.LBB29_20:                              #   in Loop: Header=BB29_10 Depth=1
	xorl	%eax, %eax
.LBB29_27:                              # %countATGC.exit
                                        #   in Loop: Header=BB29_10 Depth=1
	movl	12(%rsp), %r12d         # 4-byte Reload
	addl	%eax, %r12d
	addl	%r13d, %ebx
	movq	%r15, %rdi
	callq	free
	movl	8(%rsp), %eax           # 4-byte Reload
	incl	%eax
	cmpl	njob(%rip), %eax
	movq	16(%rsp), %r15          # 8-byte Reload
	jl	.LBB29_10
# BB#28:                                # %._crit_edge.loopexit
	cvtsi2sdl	%r12d, %xmm0
	cvtsi2sdl	%ebx, %xmm1
	cmpl	$100009, dorp(%rip)     # imm = 0x186A9
	je	.LBB29_30
	jmp	.LBB29_31
.LBB29_8:
	xorpd	%xmm0, %xmm0
	xorpd	%xmm1, %xmm1
	cmpl	$100009, dorp(%rip)     # imm = 0x186A9
	jne	.LBB29_31
.LBB29_30:
	divsd	%xmm1, %xmm0
	xorl	%eax, %eax
	ucomisd	.LCPI29_0(%rip), %xmm0
	movl	$100, %ecx
	movl	$112, %edx
	cmoval	%ecx, %edx
	movl	$-1, %ecx
	cmovbel	%eax, %ecx
	movl	%edx, dorp(%rip)
	movl	%ecx, upperCase(%rip)
.LBB29_31:
	movq	%r15, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end29:
	.size	getnumlen, .Lfunc_end29-getnumlen
	.cfi_endproc

	.globl	WriteGapFill
	.p2align	4, 0x90
	.type	WriteGapFill,@function
WriteGapFill:                           # @WriteGapFill
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi283:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi284:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi285:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi286:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi287:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi288:
	.cfi_def_cfa_offset 56
	subq	$200024, %rsp           # imm = 0x30D58
.Lcfi289:
	.cfi_def_cfa_offset 200080
.Lcfi290:
	.cfi_offset %rbx, -56
.Lcfi291:
	.cfi_offset %r12, -48
.Lcfi292:
	.cfi_offset %r13, -40
.Lcfi293:
	.cfi_offset %r14, -32
.Lcfi294:
	.cfi_offset %r15, -24
.Lcfi295:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%esi, %r15d
	movq	%rdi, %r12
	movl	$0, nlenmax(%rip)
	testl	%r15d, %r15d
	jle	.LBB30_18
# BB#1:                                 # %.lr.ph46.preheader
	movl	%r15d, %ebp
	xorl	%r13d, %r13d
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB30_2:                               # %.lr.ph46
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	callq	strlen
	cmpl	%eax, %r13d
	jge	.LBB30_4
# BB#3:                                 #   in Loop: Header=BB30_2 Depth=1
	movl	%eax, nlenmax(%rip)
	movl	%eax, %r13d
.LBB30_4:                               #   in Loop: Header=BB30_2 Depth=1
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB30_2
# BB#5:                                 # %.preheader
	testl	%r13d, %r13d
	jle	.LBB30_7
# BB#6:                                 # %.lr.ph42
	leal	-1(%r13), %edx
	incq	%rdx
	movl	$WriteGapFill.gap, %edi
	movl	$45, %esi
	callq	memset
.LBB30_7:                               # %._crit_edge43
	movslq	%r13d, %rax
	movb	$0, WriteGapFill.gap(%rax)
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	%r15d, %edx
	callq	fprintf
	movl	$10, %edi
	movq	%r12, %rsi
	callq	fputc
	testl	%r15d, %r15d
	jle	.LBB30_19
# BB#8:                                 # %.lr.ph39.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB30_9:                               # %.lr.ph39
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB30_11 Depth 2
                                        #       Child Loop BB30_14 Depth 3
	movq	(%r14,%r15,8), %rsi
	movl	$WriteGapFill.buff, %edi
	callq	strcpy
	movslq	nlenmax(%rip), %rbx
	movq	(%r14,%r15,8), %rdi
	callq	strlen
	subq	%rax, %rbx
	movl	$WriteGapFill.buff, %edi
	movl	$WriteGapFill.gap, %esi
	movq	%rbx, %rdx
	callq	strncat
	movslq	nlenmax(%rip), %rax
	movb	$0, WriteGapFill.buff(%rax)
	movl	$WriteGapFill.buff, %edi
	callq	strlen
	movq	%rax, %rbp
	movl	%ebp, 16(%rsp,%r15,4)
	movq	%r15, %rdx
	shlq	$8, %rdx
	addq	8(%rsp), %rdx           # 8-byte Folded Reload
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	fprintf
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	%ebp, %edx
	callq	fprintf
	testl	%ebp, %ebp
	jle	.LBB30_17
# BB#10:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB30_9 Depth=1
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB30_11:                              # %.lr.ph
                                        #   Parent Loop BB30_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB30_14 Depth 3
	leaq	WriteGapFill.buff(%r13), %rbx
	cmpl	$100, dorp(%rip)
	jne	.LBB30_20
# BB#12:                                # %.lr.ph
                                        #   in Loop: Header=BB30_11 Depth=2
	movl	upperCase(%rip), %eax
	testl	%eax, %eax
	jle	.LBB30_20
# BB#13:                                # %.preheader.i
                                        #   in Loop: Header=BB30_11 Depth=2
	callq	__ctype_toupper_loc
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB30_14:                              #   Parent Loop BB30_9 Depth=1
                                        #     Parent Loop BB30_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rdx
	movsbq	WriteGapFill.buff(%rcx,%r13), %rsi
	movzbl	(%rdx,%rsi,4), %edx
	movb	%dl, WriteGapFill.b(%rcx)
	movq	(%rax), %rdx
	movsbq	1(%rbx), %rsi
	movzbl	(%rdx,%rsi,4), %edx
	movb	%dl, WriteGapFill.b+1(%rcx)
	movq	(%rax), %rdx
	movsbq	2(%rbx), %rsi
	movzbl	(%rdx,%rsi,4), %edx
	movb	%dl, WriteGapFill.b+2(%rcx)
	movq	(%rax), %rdx
	movsbq	3(%rbx), %rsi
	addq	$4, %rbx
	movzbl	(%rdx,%rsi,4), %edx
	movb	%dl, WriteGapFill.b+3(%rcx)
	addq	$4, %rcx
	cmpl	$60, %ecx
	jne	.LBB30_14
# BB#15:                                # %strncpy_caseC.exit.loopexit
                                        #   in Loop: Header=BB30_11 Depth=2
	movl	16(%rsp,%r15,4), %ebp
	jmp	.LBB30_16
	.p2align	4, 0x90
.LBB30_20:                              #   in Loop: Header=BB30_11 Depth=2
	movl	$WriteGapFill.b, %edi
	movl	$60, %edx
	movq	%rbx, %rsi
	callq	strncpy
.LBB30_16:                              # %strncpy_caseC.exit
                                        #   in Loop: Header=BB30_11 Depth=2
	movb	$0, WriteGapFill.b+60(%rip)
	movl	$.L.str.5, %esi
	movl	$WriteGapFill.b, %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	fprintf
	addq	$60, %r13
	movslq	%ebp, %rax
	cmpq	%rax, %r13
	jl	.LBB30_11
.LBB30_17:                              # %._crit_edge
                                        #   in Loop: Header=BB30_9 Depth=1
	incq	%r15
	cmpq	(%rsp), %r15            # 8-byte Folded Reload
	jne	.LBB30_9
	jmp	.LBB30_19
.LBB30_18:                              # %._crit_edge40.critedge
	movb	$0, WriteGapFill.gap(%rip)
	movl	$.L.str.20, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	%r15d, %edx
	callq	fprintf
	movl	$10, %edi
	movq	%r12, %rsi
	callq	fputc
.LBB30_19:                              # %._crit_edge40
	addq	$200024, %rsp           # imm = 0x30D58
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end30:
	.size	WriteGapFill, .Lfunc_end30-WriteGapFill
	.cfi_endproc

	.globl	writeDataforgaln
	.p2align	4, 0x90
	.type	writeDataforgaln,@function
writeDataforgaln:                       # @writeDataforgaln
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi296:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi297:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi298:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi299:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi300:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi301:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi302:
	.cfi_def_cfa_offset 80
.Lcfi303:
	.cfi_offset %rbx, -56
.Lcfi304:
	.cfi_offset %r12, -48
.Lcfi305:
	.cfi_offset %r13, -40
.Lcfi306:
	.cfi_offset %r14, -32
.Lcfi307:
	.cfi_offset %r15, -24
.Lcfi308:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r14
	testl	%esi, %esi
	jle	.LBB31_6
# BB#1:                                 # %.lr.ph23.preheader
	movl	%esi, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB31_2:                               # %.lr.ph23
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_4 Depth 2
	movq	(%r15,%r13,8), %rdi
	callq	strlen
	movq	%rax, %r12
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r13,8), %rdx
	incq	%rdx
	movl	$.L.str.23, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	testl	%r12d, %r12d
	jle	.LBB31_5
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB31_2 Depth=1
	movslq	%r12d, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB31_4:                               # %.lr.ph
                                        #   Parent Loop BB31_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15,%r13,8), %rcx
	addq	%rbp, %rcx
	movl	$.L.str.24, %esi
	movl	$60, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	addq	$60, %rbp
	cmpq	%rbx, %rbp
	jl	.LBB31_4
.LBB31_5:                               # %._crit_edge
                                        #   in Loop: Header=BB31_2 Depth=1
	incq	%r13
	cmpq	8(%rsp), %r13           # 8-byte Folded Reload
	jne	.LBB31_2
.LBB31_6:                               # %._crit_edge24
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	writeDataforgaln, .Lfunc_end31-writeDataforgaln
	.cfi_endproc

	.globl	writeData_pointer
	.p2align	4, 0x90
	.type	writeData_pointer,@function
writeData_pointer:                      # @writeData_pointer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi309:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi310:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi311:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi312:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi313:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi314:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi315:
	.cfi_def_cfa_offset 80
.Lcfi316:
	.cfi_offset %rbx, -56
.Lcfi317:
	.cfi_offset %r12, -48
.Lcfi318:
	.cfi_offset %r13, -40
.Lcfi319:
	.cfi_offset %r14, -32
.Lcfi320:
	.cfi_offset %r15, -24
.Lcfi321:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r14
	testl	%esi, %esi
	jle	.LBB32_6
# BB#1:                                 # %.lr.ph23.preheader
	movl	%esi, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB32_2:                               # %.lr.ph23
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_4 Depth 2
	movq	(%r15,%r13,8), %rdi
	callq	strlen
	movq	%rax, %r12
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r13,8), %rdx
	incq	%rdx
	movl	$.L.str.23, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	testl	%r12d, %r12d
	jle	.LBB32_5
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB32_2 Depth=1
	movslq	%r12d, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB32_4:                               # %.lr.ph
                                        #   Parent Loop BB32_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15,%r13,8), %rcx
	addq	%rbp, %rcx
	movl	$.L.str.24, %esi
	movl	$60, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	addq	$60, %rbp
	cmpq	%rbx, %rbp
	jl	.LBB32_4
.LBB32_5:                               # %._crit_edge
                                        #   in Loop: Header=BB32_2 Depth=1
	incq	%r13
	cmpq	8(%rsp), %r13           # 8-byte Folded Reload
	jne	.LBB32_2
.LBB32_6:                               # %._crit_edge24
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end32:
	.size	writeData_pointer, .Lfunc_end32-writeData_pointer
	.cfi_endproc

	.globl	writeData
	.p2align	4, 0x90
	.type	writeData,@function
writeData:                              # @writeData
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi322:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi323:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi324:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi325:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi326:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi327:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi328:
	.cfi_def_cfa_offset 80
.Lcfi329:
	.cfi_offset %rbx, -56
.Lcfi330:
	.cfi_offset %r12, -48
.Lcfi331:
	.cfi_offset %r13, -40
.Lcfi332:
	.cfi_offset %r14, -32
.Lcfi333:
	.cfi_offset %r15, -24
.Lcfi334:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r14
	testl	%esi, %esi
	jle	.LBB33_6
# BB#1:                                 # %.lr.ph23.preheader
	movl	%esi, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB33_2:                               # %.lr.ph23
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_4 Depth 2
	movq	(%r15,%r13,8), %rdi
	callq	strlen
	movq	%rax, %r12
	movq	%r13, %rax
	shlq	$8, %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	1(%rcx,%rax), %rdx
	movl	$.L.str.23, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	testl	%r12d, %r12d
	jle	.LBB33_5
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB33_2 Depth=1
	movslq	%r12d, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB33_4:                               # %.lr.ph
                                        #   Parent Loop BB33_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15,%r13,8), %rcx
	addq	%rbp, %rcx
	movl	$.L.str.24, %esi
	movl	$60, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	addq	$60, %rbp
	cmpq	%rbx, %rbp
	jl	.LBB33_4
.LBB33_5:                               # %._crit_edge
                                        #   in Loop: Header=BB33_2 Depth=1
	incq	%r13
	cmpq	8(%rsp), %r13           # 8-byte Folded Reload
	jne	.LBB33_2
.LBB33_6:                               # %._crit_edge24
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end33:
	.size	writeData, .Lfunc_end33-writeData
	.cfi_endproc

	.globl	write1seq
	.p2align	4, 0x90
	.type	write1seq,@function
write1seq:                              # @write1seq
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi335:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi336:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi337:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi338:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi339:
	.cfi_def_cfa_offset 48
.Lcfi340:
	.cfi_offset %rbx, -40
.Lcfi341:
	.cfi_offset %r12, -32
.Lcfi342:
	.cfi_offset %r14, -24
.Lcfi343:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	%r15, %rdi
	callq	strlen
	testl	%eax, %eax
	jle	.LBB34_3
# BB#1:                                 # %.lr.ph.preheader
	movslq	%eax, %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB34_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%r15,%rbx), %rcx
	movl	$.L.str.24, %esi
	movl	$60, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	addq	$60, %rbx
	cmpq	%r12, %rbx
	jl	.LBB34_2
.LBB34_3:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end34:
	.size	write1seq, .Lfunc_end34-write1seq
	.cfi_endproc

	.globl	readhat2_floathalf_pointer
	.p2align	4, 0x90
	.type	readhat2_floathalf_pointer,@function
readhat2_floathalf_pointer:             # @readhat2_floathalf_pointer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi344:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi345:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi346:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi347:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi348:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi349:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi350:
	.cfi_def_cfa_offset 368
.Lcfi351:
	.cfi_offset %rbx, -56
.Lcfi352:
	.cfi_offset %r12, -48
.Lcfi353:
	.cfi_offset %r13, -40
.Lcfi354:
	.cfi_offset %r14, -32
.Lcfi355:
	.cfi_offset %r15, -24
.Lcfi356:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%esi, %r15d
	movq	%rdi, %rbx
	leaq	48(%rsp), %r12
	movl	$256, %esi              # imm = 0x100
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$256, %esi              # imm = 0x100
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movb	$0, 53(%rsp)
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r12, %rdi
	callq	strtol
	cmpl	%r15d, %eax
	jne	.LBB35_20
# BB#1:
	leaq	48(%rsp), %rdi
	movl	$256, %esi              # imm = 0x100
	movq	%rbx, %rdx
	callq	fgets
	testl	%r15d, %r15d
	jle	.LBB35_19
# BB#2:                                 # %.lr.ph34.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB35_3:                               # %.lr.ph34
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_5 Depth 2
                                        #     Child Loop BB35_11 Depth 2
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB35_12
# BB#4:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB35_3 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB35_5:                               # %.lr.ph.i
                                        #   Parent Loop BB35_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB35_6
# BB#9:                                 #   in Loop: Header=BB35_5 Depth=2
	movb	%al, 48(%rsp,%rbp)
	incq	%rbp
	cmpl	$254, %ebp
	jl	.LBB35_5
# BB#10:                                # %.critedge.i
                                        #   in Loop: Header=BB35_3 Depth=1
	movb	$0, 48(%rsp,%rbp)
	.p2align	4, 0x90
.LBB35_11:                              #   Parent Loop BB35_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	jne	.LBB35_11
	jmp	.LBB35_12
.LBB35_6:                               # %.critedge.thread.i
                                        #   in Loop: Header=BB35_3 Depth=1
	movb	$0, 48(%rsp,%rbp)
	.p2align	4, 0x90
.LBB35_12:                              # %myfgets.exit
                                        #   in Loop: Header=BB35_3 Depth=1
	incl	%r12d
	cmpl	%r15d, %r12d
	jne	.LBB35_3
# BB#7:                                 # %.preheader
	cmpl	$2, %r15d
	jl	.LBB35_19
# BB#8:                                 # %.lr.ph31
	leal	-1(%r15), %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movslq	%r15d, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%r15d, %eax
	negq	%rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	leaq	6(%rsp), %r15
	.p2align	4, 0x90
.LBB35_14:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_16 Depth 2
	leaq	1(%rbp), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpq	24(%rsp), %rax          # 8-byte Folded Reload
	jge	.LBB35_13
# BB#15:                                # %.lr.ph
                                        #   in Loop: Header=BB35_14 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbp), %r13
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB35_16:                              #   Parent Loop BB35_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB35_18
# BB#17:                                #   in Loop: Header=BB35_16 Depth=2
	movl	%eax, %edi
	movq	%rbx, %rsi
	callq	ungetc
.LBB35_18:                              # %.lr.ph.i27.preheader
                                        #   in Loop: Header=BB35_16 Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 6(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 7(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 8(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 9(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 10(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 11(%rsp)
	movb	$0, 12(%rsp)
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	strtod
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r14,%rbp,8), %rax
	movss	%xmm0, (%rax,%r12,4)
	incq	%r12
	movq	%r13, %rax
	addq	%r12, %rax
	jne	.LBB35_16
.LBB35_13:                              # %.loopexit
                                        #   in Loop: Header=BB35_14 Depth=1
	movq	40(%rsp), %rbp          # 8-byte Reload
	cmpq	32(%rsp), %rbp          # 8-byte Folded Reload
	jne	.LBB35_14
.LBB35_19:                              # %._crit_edge
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB35_20:
	movl	$.L.str.25, %edi
	callq	ErrorExit
.Lfunc_end35:
	.size	readhat2_floathalf_pointer, .Lfunc_end35-readhat2_floathalf_pointer
	.cfi_endproc

	.globl	readhat2_floathalf
	.p2align	4, 0x90
	.type	readhat2_floathalf,@function
readhat2_floathalf:                     # @readhat2_floathalf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi357:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi358:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi359:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi360:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi361:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi362:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi363:
	.cfi_def_cfa_offset 368
.Lcfi364:
	.cfi_offset %rbx, -56
.Lcfi365:
	.cfi_offset %r12, -48
.Lcfi366:
	.cfi_offset %r13, -40
.Lcfi367:
	.cfi_offset %r14, -32
.Lcfi368:
	.cfi_offset %r15, -24
.Lcfi369:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%esi, %r15d
	movq	%rdi, %rbx
	leaq	48(%rsp), %r12
	movl	$256, %esi              # imm = 0x100
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$256, %esi              # imm = 0x100
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movb	$0, 53(%rsp)
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r12, %rdi
	callq	strtol
	cmpl	%r15d, %eax
	jne	.LBB36_20
# BB#1:
	leaq	48(%rsp), %rdi
	movl	$256, %esi              # imm = 0x100
	movq	%rbx, %rdx
	callq	fgets
	testl	%r15d, %r15d
	jle	.LBB36_19
# BB#2:                                 # %.lr.ph34.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB36_3:                               # %.lr.ph34
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB36_5 Depth 2
                                        #     Child Loop BB36_11 Depth 2
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB36_12
# BB#4:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB36_3 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB36_5:                               # %.lr.ph.i
                                        #   Parent Loop BB36_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB36_6
# BB#9:                                 #   in Loop: Header=BB36_5 Depth=2
	movb	%al, 48(%rsp,%rbp)
	incq	%rbp
	cmpl	$254, %ebp
	jl	.LBB36_5
# BB#10:                                # %.critedge.i
                                        #   in Loop: Header=BB36_3 Depth=1
	movb	$0, 48(%rsp,%rbp)
	.p2align	4, 0x90
.LBB36_11:                              #   Parent Loop BB36_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	jne	.LBB36_11
	jmp	.LBB36_12
.LBB36_6:                               # %.critedge.thread.i
                                        #   in Loop: Header=BB36_3 Depth=1
	movb	$0, 48(%rsp,%rbp)
	.p2align	4, 0x90
.LBB36_12:                              # %myfgets.exit
                                        #   in Loop: Header=BB36_3 Depth=1
	incl	%r12d
	cmpl	%r15d, %r12d
	jne	.LBB36_3
# BB#7:                                 # %.preheader
	cmpl	$2, %r15d
	jl	.LBB36_19
# BB#8:                                 # %.lr.ph31
	leal	-1(%r15), %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movslq	%r15d, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%r15d, %eax
	negq	%rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	leaq	6(%rsp), %r15
	.p2align	4, 0x90
.LBB36_14:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB36_16 Depth 2
	leaq	1(%rbp), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpq	24(%rsp), %rax          # 8-byte Folded Reload
	jge	.LBB36_13
# BB#15:                                # %.lr.ph
                                        #   in Loop: Header=BB36_14 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbp), %r13
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB36_16:                              #   Parent Loop BB36_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB36_18
# BB#17:                                #   in Loop: Header=BB36_16 Depth=2
	movl	%eax, %edi
	movq	%rbx, %rsi
	callq	ungetc
.LBB36_18:                              # %.lr.ph.i27.preheader
                                        #   in Loop: Header=BB36_16 Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 6(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 7(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 8(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 9(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 10(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 11(%rsp)
	movb	$0, 12(%rsp)
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	strtod
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r14,%rbp,8), %rax
	movss	%xmm0, (%rax,%r12,4)
	incq	%r12
	movq	%r13, %rax
	addq	%r12, %rax
	jne	.LBB36_16
.LBB36_13:                              # %.loopexit
                                        #   in Loop: Header=BB36_14 Depth=1
	movq	40(%rsp), %rbp          # 8-byte Reload
	cmpq	32(%rsp), %rbp          # 8-byte Folded Reload
	jne	.LBB36_14
.LBB36_19:                              # %._crit_edge
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB36_20:
	movl	$.L.str.25, %edi
	callq	ErrorExit
.Lfunc_end36:
	.size	readhat2_floathalf, .Lfunc_end36-readhat2_floathalf
	.cfi_endproc

	.globl	readhat2_float
	.p2align	4, 0x90
	.type	readhat2_float,@function
readhat2_float:                         # @readhat2_float
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi370:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi371:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi372:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi373:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi374:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi375:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi376:
	.cfi_def_cfa_offset 368
.Lcfi377:
	.cfi_offset %rbx, -56
.Lcfi378:
	.cfi_offset %r12, -48
.Lcfi379:
	.cfi_offset %r13, -40
.Lcfi380:
	.cfi_offset %r14, -32
.Lcfi381:
	.cfi_offset %r15, -24
.Lcfi382:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%esi, %r15d
	movq	%rdi, %rbx
	leaq	48(%rsp), %r12
	movl	$256, %esi              # imm = 0x100
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$256, %esi              # imm = 0x100
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movb	$0, 53(%rsp)
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r12, %rdi
	callq	strtol
	cmpl	%r15d, %eax
	jne	.LBB37_20
# BB#1:
	leaq	48(%rsp), %rdi
	movl	$256, %esi              # imm = 0x100
	movq	%rbx, %rdx
	callq	fgets
	testl	%r15d, %r15d
	jle	.LBB37_19
# BB#2:                                 # %.lr.ph33.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB37_3:                               # %.lr.ph33
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB37_5 Depth 2
                                        #     Child Loop BB37_11 Depth 2
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB37_12
# BB#4:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB37_3 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB37_5:                               # %.lr.ph.i
                                        #   Parent Loop BB37_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB37_6
# BB#9:                                 #   in Loop: Header=BB37_5 Depth=2
	movb	%al, 48(%rsp,%rbp)
	incq	%rbp
	cmpl	$254, %ebp
	jl	.LBB37_5
# BB#10:                                # %.critedge.i
                                        #   in Loop: Header=BB37_3 Depth=1
	movb	$0, 48(%rsp,%rbp)
	.p2align	4, 0x90
.LBB37_11:                              #   Parent Loop BB37_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	jne	.LBB37_11
	jmp	.LBB37_12
.LBB37_6:                               # %.critedge.thread.i
                                        #   in Loop: Header=BB37_3 Depth=1
	movb	$0, 48(%rsp,%rbp)
	.p2align	4, 0x90
.LBB37_12:                              # %myfgets.exit
                                        #   in Loop: Header=BB37_3 Depth=1
	incl	%r12d
	cmpl	%r15d, %r12d
	jne	.LBB37_3
# BB#7:                                 # %.preheader
	cmpl	$2, %r15d
	jl	.LBB37_19
# BB#8:                                 # %.lr.ph30
	leal	-1(%r15), %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movslq	%r15d, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%r15d, %ebp
	movl	$1, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	leaq	6(%rsp), %r15
	.p2align	4, 0x90
.LBB37_14:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB37_16 Depth 2
	leaq	1(%r13), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpq	24(%rsp), %rax          # 8-byte Folded Reload
	jge	.LBB37_13
# BB#15:                                # %.lr.ph
                                        #   in Loop: Header=BB37_14 Depth=1
	movq	16(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB37_16:                              #   Parent Loop BB37_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB37_18
# BB#17:                                #   in Loop: Header=BB37_16 Depth=2
	movl	%eax, %edi
	movq	%rbx, %rsi
	callq	ungetc
.LBB37_18:                              # %.lr.ph.i26.preheader
                                        #   in Loop: Header=BB37_16 Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 6(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 7(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 8(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 9(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 10(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 11(%rsp)
	movb	$0, 12(%rsp)
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	strtod
	cvtsd2ss	%xmm0, %xmm0
	movq	(%r14,%r13,8), %rax
	movss	%xmm0, (%rax,%r12,4)
	incq	%r12
	cmpq	%r12, %rbp
	jne	.LBB37_16
.LBB37_13:                              # %.loopexit
                                        #   in Loop: Header=BB37_14 Depth=1
	incq	16(%rsp)                # 8-byte Folded Spill
	movq	40(%rsp), %r13          # 8-byte Reload
	cmpq	32(%rsp), %r13          # 8-byte Folded Reload
	jne	.LBB37_14
.LBB37_19:                              # %._crit_edge
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB37_20:
	movl	$.L.str.25, %edi
	callq	ErrorExit
.Lfunc_end37:
	.size	readhat2_float, .Lfunc_end37-readhat2_float
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI38_0:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI38_1:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	readhat2_int
	.p2align	4, 0x90
	.type	readhat2_int,@function
readhat2_int:                           # @readhat2_int
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi383:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi384:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi385:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi386:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi387:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi388:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi389:
	.cfi_def_cfa_offset 368
.Lcfi390:
	.cfi_offset %rbx, -56
.Lcfi391:
	.cfi_offset %r12, -48
.Lcfi392:
	.cfi_offset %r13, -40
.Lcfi393:
	.cfi_offset %r14, -32
.Lcfi394:
	.cfi_offset %r15, -24
.Lcfi395:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%esi, %r15d
	movq	%rdi, %rbx
	leaq	48(%rsp), %r12
	movl	$256, %esi              # imm = 0x100
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$256, %esi              # imm = 0x100
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movb	$0, 53(%rsp)
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r12, %rdi
	callq	strtol
	cmpl	%r15d, %eax
	jne	.LBB38_20
# BB#1:
	leaq	48(%rsp), %rdi
	movl	$256, %esi              # imm = 0x100
	movq	%rbx, %rdx
	callq	fgets
	testl	%r15d, %r15d
	jle	.LBB38_19
# BB#2:                                 # %.lr.ph33.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB38_3:                               # %.lr.ph33
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB38_5 Depth 2
                                        #     Child Loop BB38_11 Depth 2
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB38_12
# BB#4:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB38_3 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB38_5:                               # %.lr.ph.i
                                        #   Parent Loop BB38_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB38_6
# BB#9:                                 #   in Loop: Header=BB38_5 Depth=2
	movb	%al, 48(%rsp,%rbp)
	incq	%rbp
	cmpl	$254, %ebp
	jl	.LBB38_5
# BB#10:                                # %.critedge.i
                                        #   in Loop: Header=BB38_3 Depth=1
	movb	$0, 48(%rsp,%rbp)
	.p2align	4, 0x90
.LBB38_11:                              #   Parent Loop BB38_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	jne	.LBB38_11
	jmp	.LBB38_12
.LBB38_6:                               # %.critedge.thread.i
                                        #   in Loop: Header=BB38_3 Depth=1
	movb	$0, 48(%rsp,%rbp)
	.p2align	4, 0x90
.LBB38_12:                              # %myfgets.exit
                                        #   in Loop: Header=BB38_3 Depth=1
	incl	%r12d
	cmpl	%r15d, %r12d
	jne	.LBB38_3
# BB#7:                                 # %.preheader
	cmpl	$2, %r15d
	jl	.LBB38_19
# BB#8:                                 # %.lr.ph30
	leal	-1(%r15), %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movslq	%r15d, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%r15d, %ebp
	movl	$1, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	leaq	6(%rsp), %r15
	.p2align	4, 0x90
.LBB38_14:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB38_16 Depth 2
	leaq	1(%r13), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpq	24(%rsp), %rax          # 8-byte Folded Reload
	jge	.LBB38_13
# BB#15:                                # %.lr.ph
                                        #   in Loop: Header=BB38_14 Depth=1
	movq	16(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB38_16:                              #   Parent Loop BB38_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB38_18
# BB#17:                                #   in Loop: Header=BB38_16 Depth=2
	movl	%eax, %edi
	movq	%rbx, %rsi
	callq	ungetc
.LBB38_18:                              # %.lr.ph.i26.preheader
                                        #   in Loop: Header=BB38_16 Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 6(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 7(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 8(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 9(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 10(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 11(%rsp)
	movb	$0, 12(%rsp)
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	strtod
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI38_0(%rip), %xmm0
	addsd	.LCPI38_1(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	movq	(%r14,%r13,8), %rcx
	movl	%eax, (%rcx,%r12,4)
	incq	%r12
	cmpq	%r12, %rbp
	jne	.LBB38_16
.LBB38_13:                              # %.loopexit
                                        #   in Loop: Header=BB38_14 Depth=1
	incq	16(%rsp)                # 8-byte Folded Spill
	movq	40(%rsp), %r13          # 8-byte Reload
	cmpq	32(%rsp), %r13          # 8-byte Folded Reload
	jne	.LBB38_14
.LBB38_19:                              # %._crit_edge
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB38_20:
	movl	$.L.str.25, %edi
	callq	ErrorExit
.Lfunc_end38:
	.size	readhat2_int, .Lfunc_end38-readhat2_int
	.cfi_endproc

	.globl	readhat2
	.p2align	4, 0x90
	.type	readhat2,@function
readhat2:                               # @readhat2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi396:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi397:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi398:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi399:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi400:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi401:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi402:
	.cfi_def_cfa_offset 368
.Lcfi403:
	.cfi_offset %rbx, -56
.Lcfi404:
	.cfi_offset %r12, -48
.Lcfi405:
	.cfi_offset %r13, -40
.Lcfi406:
	.cfi_offset %r14, -32
.Lcfi407:
	.cfi_offset %r15, -24
.Lcfi408:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%esi, %r15d
	movq	%rdi, %rbx
	leaq	48(%rsp), %r12
	movl	$256, %esi              # imm = 0x100
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$256, %esi              # imm = 0x100
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movb	$0, 53(%rsp)
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r12, %rdi
	callq	strtol
	cmpl	%r15d, %eax
	jne	.LBB39_20
# BB#1:
	leaq	48(%rsp), %rdi
	movl	$256, %esi              # imm = 0x100
	movq	%rbx, %rdx
	callq	fgets
	testl	%r15d, %r15d
	jle	.LBB39_19
# BB#2:                                 # %.lr.ph33.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB39_3:                               # %.lr.ph33
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB39_5 Depth 2
                                        #     Child Loop BB39_11 Depth 2
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB39_12
# BB#4:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB39_3 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB39_5:                               # %.lr.ph.i
                                        #   Parent Loop BB39_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB39_6
# BB#9:                                 #   in Loop: Header=BB39_5 Depth=2
	movb	%al, 48(%rsp,%rbp)
	incq	%rbp
	cmpl	$254, %ebp
	jl	.LBB39_5
# BB#10:                                # %.critedge.i
                                        #   in Loop: Header=BB39_3 Depth=1
	movb	$0, 48(%rsp,%rbp)
	.p2align	4, 0x90
.LBB39_11:                              #   Parent Loop BB39_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	jne	.LBB39_11
	jmp	.LBB39_12
.LBB39_6:                               # %.critedge.thread.i
                                        #   in Loop: Header=BB39_3 Depth=1
	movb	$0, 48(%rsp,%rbp)
	.p2align	4, 0x90
.LBB39_12:                              # %myfgets.exit
                                        #   in Loop: Header=BB39_3 Depth=1
	incl	%r12d
	cmpl	%r15d, %r12d
	jne	.LBB39_3
# BB#7:                                 # %.preheader
	cmpl	$2, %r15d
	jl	.LBB39_19
# BB#8:                                 # %.lr.ph30
	leal	-1(%r15), %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movslq	%r15d, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%r15d, %ebp
	movl	$1, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	leaq	6(%rsp), %r15
	.p2align	4, 0x90
.LBB39_14:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB39_16 Depth 2
	leaq	1(%r13), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpq	24(%rsp), %rax          # 8-byte Folded Reload
	jge	.LBB39_13
# BB#15:                                # %.lr.ph
                                        #   in Loop: Header=BB39_14 Depth=1
	movq	16(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB39_16:                              #   Parent Loop BB39_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$10, %eax
	je	.LBB39_18
# BB#17:                                #   in Loop: Header=BB39_16 Depth=2
	movl	%eax, %edi
	movq	%rbx, %rsi
	callq	ungetc
.LBB39_18:                              # %.lr.ph.i26.preheader
                                        #   in Loop: Header=BB39_16 Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 6(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 7(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 8(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 9(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 10(%rsp)
	movq	%rbx, %rdi
	callq	_IO_getc
	movb	%al, 11(%rsp)
	movb	$0, 12(%rsp)
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	strtod
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movq	(%r14,%r13,8), %rax
	movsd	%xmm0, (%rax,%r12,8)
	incq	%r12
	cmpq	%r12, %rbp
	jne	.LBB39_16
.LBB39_13:                              # %.loopexit
                                        #   in Loop: Header=BB39_14 Depth=1
	incq	16(%rsp)                # 8-byte Folded Spill
	movq	40(%rsp), %r13          # 8-byte Reload
	cmpq	32(%rsp), %r13          # 8-byte Folded Reload
	jne	.LBB39_14
.LBB39_19:                              # %._crit_edge
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB39_20:
	movl	$.L.str.25, %edi
	callq	ErrorExit
.Lfunc_end39:
	.size	readhat2, .Lfunc_end39-readhat2
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI40_0:
	.quad	4612811918334230528     # double 2.5
	.text
	.globl	WriteFloatHat2_pointer
	.p2align	4, 0x90
	.type	WriteFloatHat2_pointer,@function
WriteFloatHat2_pointer:                 # @WriteFloatHat2_pointer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi409:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi410:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi411:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi412:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi413:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi414:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi415:
	.cfi_def_cfa_offset 80
.Lcfi416:
	.cfi_offset %rbx, -56
.Lcfi417:
	.cfi_offset %r12, -48
.Lcfi418:
	.cfi_offset %r13, -40
.Lcfi419:
	.cfi_offset %r14, -32
.Lcfi420:
	.cfi_offset %r15, -24
.Lcfi421:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r12
	movl	%esi, %r15d
	movq	%rdi, %r13
	xorpd	%xmm2, %xmm2
	cmpl	$2, %r15d
	jl	.LBB40_12
# BB#1:                                 # %.preheader53.preheader
	leal	-1(%r15), %r9d
	movslq	%r15d, %r10
	movl	%r15d, %r8d
	movb	%r15b, %sil
	addb	$3, %sil
	xorpd	%xmm2, %xmm2
	xorl	%edi, %edi
	movl	%r15d, %ecx
	.p2align	4, 0x90
.LBB40_2:                               # %.preheader53
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB40_6 Depth 2
                                        #     Child Loop BB40_10 Depth 2
	movl	%ecx, %ecx
	andb	$3, %sil
	movzbl	%sil, %esi
	movq	%r10, %rax
	subq	%rdi, %rax
	cmpq	$2, %rax
	jl	.LBB40_11
# BB#3:                                 # %.lr.ph62
                                        #   in Loop: Header=BB40_2 Depth=1
	movq	%r8, %rax
	subq	%rdi, %rax
	movl	%eax, %ebp
	addq	$-2, %rbp
	addl	$3, %eax
	movq	(%r14,%rdi,8), %rdx
	testb	$3, %al
	je	.LBB40_4
# BB#5:                                 # %.prol.preheader
                                        #   in Loop: Header=BB40_2 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB40_6:                               #   Parent Loop BB40_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm2, %xmm0
	movss	4(%rdx,%rax,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	cvtss2sd	%xmm1, %xmm2
	maxsd	%xmm0, %xmm2
	incq	%rax
	cmpq	%rax, %rsi
	jne	.LBB40_6
# BB#7:                                 # %.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB40_2 Depth=1
	incq	%rax
	cmpq	$3, %rbp
	jae	.LBB40_9
	jmp	.LBB40_11
.LBB40_4:                               #   in Loop: Header=BB40_2 Depth=1
	movl	$1, %eax
	cmpq	$3, %rbp
	jb	.LBB40_11
.LBB40_9:                               # %.lr.ph62.new
                                        #   in Loop: Header=BB40_2 Depth=1
	movq	%rcx, %rbx
	subq	%rax, %rbx
	leaq	12(%rdx,%rax,4), %rax
	.p2align	4, 0x90
.LBB40_10:                              #   Parent Loop BB40_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-12(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	-8(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	maxsd	%xmm2, %xmm0
	cvtss2sd	%xmm1, %xmm1
	maxsd	%xmm0, %xmm1
	movss	-4(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	maxsd	%xmm1, %xmm0
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	cvtss2sd	%xmm1, %xmm2
	maxsd	%xmm0, %xmm2
	addq	$16, %rax
	addq	$-4, %rbx
	jne	.LBB40_10
.LBB40_11:                              # %._crit_edge63
                                        #   in Loop: Header=BB40_2 Depth=1
	incq	%rdi
	decl	%ecx
	addb	$3, %sil
	cmpq	%r9, %rdi
	jne	.LBB40_2
.LBB40_12:                              # %._crit_edge66
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movl	$.L.str.22, %esi
	movl	$1, %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%r15d, %edx
	callq	fprintf
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI40_0(%rip), %xmm0
	movl	$.L.str.26, %esi
	movb	$1, %al
	movq	%r13, %rdi
	callq	fprintf
	testl	%r15d, %r15d
	jle	.LBB40_24
# BB#13:                                # %.lr.ph59.preheader
	movl	%r15d, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB40_14:                              # %.lr.ph59
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbx,8), %rcx
	incq	%rbx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%ebx, %edx
	callq	fprintf
	cmpq	%rbx, %rbp
	jne	.LBB40_14
# BB#15:                                # %.preheader52
	testl	%r15d, %r15d
	jle	.LBB40_24
# BB#16:                                # %.preheader.preheader
	movslq	%r15d, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leal	-1(%r15), %ebx
	xorl	%r12d, %r12d
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB40_17:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB40_19 Depth 2
	movl	%ebx, %ebx
	movl	%r15d, %r15d
	movq	16(%rsp), %rax          # 8-byte Reload
	subq	%r12, %rax
	cmpq	$2, %rax
	jl	.LBB40_23
# BB#18:                                # %.lr.ph
                                        #   in Loop: Header=BB40_17 Depth=1
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB40_19:                              #   Parent Loop BB40_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%r12,8), %rax
	movss	(%rax,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.28, %esi
	movb	$1, %al
	movq	%r13, %rdi
	callq	fprintf
	cmpq	%rbp, %rbx
	je	.LBB40_21
# BB#20:                                #   in Loop: Header=BB40_19 Depth=2
	movslq	%ebp, %rax
	imulq	$715827883, %rax, %rcx  # imm = 0x2AAAAAAB
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$33, %rcx
	addl	%edx, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,2), %ecx
	subl	%ecx, %eax
	jne	.LBB40_22
.LBB40_21:                              #   in Loop: Header=BB40_19 Depth=2
	movl	$10, %edi
	movq	%r13, %rsi
	callq	fputc
.LBB40_22:                              #   in Loop: Header=BB40_19 Depth=2
	incq	%rbp
	cmpq	%rbp, %r15
	jne	.LBB40_19
.LBB40_23:                              # %._crit_edge
                                        #   in Loop: Header=BB40_17 Depth=1
	incq	%r12
	decl	%r15d
	decl	%ebx
	movq	8(%rsp), %rbp           # 8-byte Reload
	cmpq	%rbp, %r12
	jne	.LBB40_17
.LBB40_24:                              # %._crit_edge56
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end40:
	.size	WriteFloatHat2_pointer, .Lfunc_end40-WriteFloatHat2_pointer
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI41_0:
	.quad	4612811918334230528     # double 2.5
	.text
	.globl	WriteFloatHat2
	.p2align	4, 0x90
	.type	WriteFloatHat2,@function
WriteFloatHat2:                         # @WriteFloatHat2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi422:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi423:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi424:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi425:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi426:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi427:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi428:
	.cfi_def_cfa_offset 80
.Lcfi429:
	.cfi_offset %rbx, -56
.Lcfi430:
	.cfi_offset %r12, -48
.Lcfi431:
	.cfi_offset %r13, -40
.Lcfi432:
	.cfi_offset %r14, -32
.Lcfi433:
	.cfi_offset %r15, -24
.Lcfi434:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r12
	movl	%esi, %r15d
	movq	%rdi, %r13
	xorpd	%xmm2, %xmm2
	cmpl	$2, %r15d
	jl	.LBB41_12
# BB#1:                                 # %.preheader53.preheader
	leal	-1(%r15), %r9d
	movslq	%r15d, %r10
	movl	%r15d, %r8d
	movb	%r15b, %sil
	addb	$3, %sil
	xorpd	%xmm2, %xmm2
	xorl	%edi, %edi
	movl	%r15d, %ecx
	.p2align	4, 0x90
.LBB41_2:                               # %.preheader53
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB41_6 Depth 2
                                        #     Child Loop BB41_10 Depth 2
	movl	%ecx, %ecx
	andb	$3, %sil
	movzbl	%sil, %esi
	movq	%r10, %rax
	subq	%rdi, %rax
	cmpq	$2, %rax
	jl	.LBB41_11
# BB#3:                                 # %.lr.ph62
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	%r8, %rax
	subq	%rdi, %rax
	movl	%eax, %ebx
	addq	$-2, %rbx
	addl	$3, %eax
	movq	(%r14,%rdi,8), %rdx
	testb	$3, %al
	je	.LBB41_4
# BB#5:                                 # %.prol.preheader
                                        #   in Loop: Header=BB41_2 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB41_6:                               #   Parent Loop BB41_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm2, %xmm0
	movss	4(%rdx,%rax,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	cvtss2sd	%xmm1, %xmm2
	maxsd	%xmm0, %xmm2
	incq	%rax
	cmpq	%rax, %rsi
	jne	.LBB41_6
# BB#7:                                 # %.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB41_2 Depth=1
	incq	%rax
	cmpq	$3, %rbx
	jae	.LBB41_9
	jmp	.LBB41_11
.LBB41_4:                               #   in Loop: Header=BB41_2 Depth=1
	movl	$1, %eax
	cmpq	$3, %rbx
	jb	.LBB41_11
.LBB41_9:                               # %.lr.ph62.new
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	%rcx, %rbp
	subq	%rax, %rbp
	leaq	12(%rdx,%rax,4), %rax
	.p2align	4, 0x90
.LBB41_10:                              #   Parent Loop BB41_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-12(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	-8(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	maxsd	%xmm2, %xmm0
	cvtss2sd	%xmm1, %xmm1
	maxsd	%xmm0, %xmm1
	movss	-4(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	maxsd	%xmm1, %xmm0
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	cvtss2sd	%xmm1, %xmm2
	maxsd	%xmm0, %xmm2
	addq	$16, %rax
	addq	$-4, %rbp
	jne	.LBB41_10
.LBB41_11:                              # %._crit_edge63
                                        #   in Loop: Header=BB41_2 Depth=1
	incq	%rdi
	decl	%ecx
	addb	$3, %sil
	cmpq	%r9, %rdi
	jne	.LBB41_2
.LBB41_12:                              # %._crit_edge66
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movl	$.L.str.22, %esi
	movl	$1, %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%r15d, %edx
	callq	fprintf
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI41_0(%rip), %xmm0
	movl	$.L.str.26, %esi
	movb	$1, %al
	movq	%r13, %rdi
	callq	fprintf
	testl	%r15d, %r15d
	jle	.LBB41_24
# BB#13:                                # %.lr.ph59.preheader
	movl	%r15d, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB41_14:                              # %.lr.ph59
                                        # =>This Inner Loop Header: Depth=1
	incq	%rbx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%ebx, %edx
	movq	%r12, %rcx
	callq	fprintf
	addq	$256, %r12              # imm = 0x100
	cmpq	%rbx, %rbp
	jne	.LBB41_14
# BB#15:                                # %.preheader52
	testl	%r15d, %r15d
	jle	.LBB41_24
# BB#16:                                # %.preheader.preheader
	movslq	%r15d, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leal	-1(%r15), %ebx
	xorl	%r12d, %r12d
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB41_17:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB41_19 Depth 2
	movl	%ebx, %ebx
	movl	%r15d, %r15d
	movq	16(%rsp), %rax          # 8-byte Reload
	subq	%r12, %rax
	cmpq	$2, %rax
	jl	.LBB41_23
# BB#18:                                # %.lr.ph
                                        #   in Loop: Header=BB41_17 Depth=1
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB41_19:                              #   Parent Loop BB41_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%r12,8), %rax
	movss	(%rax,%rbp,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.28, %esi
	movb	$1, %al
	movq	%r13, %rdi
	callq	fprintf
	cmpq	%rbp, %rbx
	je	.LBB41_21
# BB#20:                                #   in Loop: Header=BB41_19 Depth=2
	movslq	%ebp, %rax
	imulq	$715827883, %rax, %rcx  # imm = 0x2AAAAAAB
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$33, %rcx
	addl	%edx, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,2), %ecx
	subl	%ecx, %eax
	jne	.LBB41_22
.LBB41_21:                              #   in Loop: Header=BB41_19 Depth=2
	movl	$10, %edi
	movq	%r13, %rsi
	callq	fputc
.LBB41_22:                              #   in Loop: Header=BB41_19 Depth=2
	incq	%rbp
	cmpq	%rbp, %r15
	jne	.LBB41_19
.LBB41_23:                              # %._crit_edge
                                        #   in Loop: Header=BB41_17 Depth=1
	incq	%r12
	decl	%r15d
	decl	%ebx
	movq	8(%rsp), %rbp           # 8-byte Reload
	cmpq	%rbp, %r12
	jne	.LBB41_17
.LBB41_24:                              # %._crit_edge56
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end41:
	.size	WriteFloatHat2, .Lfunc_end41-WriteFloatHat2
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI42_0:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI42_1:
	.quad	4612811918334230528     # double 2.5
	.text
	.globl	WriteHat2_int
	.p2align	4, 0x90
	.type	WriteHat2_int,@function
WriteHat2_int:                          # @WriteHat2_int
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi435:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi436:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi437:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi438:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi439:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi440:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi441:
	.cfi_def_cfa_offset 96
.Lcfi442:
	.cfi_offset %rbx, -56
.Lcfi443:
	.cfi_offset %r12, -48
.Lcfi444:
	.cfi_offset %r13, -40
.Lcfi445:
	.cfi_offset %r14, -32
.Lcfi446:
	.cfi_offset %r15, -24
.Lcfi447:
	.cfi_offset %rbp, -16
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rdx, %r12
	movl	%esi, %r15d
	movq	%rdi, %r13
	leal	-1(%r15), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	xorpd	%xmm1, %xmm1
	cmpl	$2, %r15d
	jl	.LBB42_12
# BB#1:                                 # %.lr.ph66.preheader
	movslq	%r15d, %r11
	movl	%r15d, %eax
	movl	8(%rsp), %r14d          # 4-byte Reload
	leaq	3(%rax), %r9
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	-2(%rax), %r10
	movl	%r15d, %ebx
	addb	$3, %bl
	xorpd	%xmm1, %xmm1
	movl	$1, %ebp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB42_3:                               # %.lr.ph66
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB42_7 Depth 2
                                        #     Child Loop BB42_11 Depth 2
	movq	%rcx, %rax
	leaq	1(%rax), %rcx
	cmpq	%r11, %rcx
	jge	.LBB42_2
# BB#4:                                 # %.lr.ph62
                                        #   in Loop: Header=BB42_3 Depth=1
	movq	%r10, %rdi
	subq	%rax, %rdi
	movl	%r9d, %edx
	subl	%eax, %edx
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi,%rax,8), %r8
	testb	$3, %dl
	je	.LBB42_5
# BB#6:                                 # %.prol.preheader
                                        #   in Loop: Header=BB42_3 Depth=1
	movl	%ebx, %eax
	andb	$3, %al
	movzbl	%al, %eax
	negq	%rax
	movq	%r8, %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB42_7:                               #   Parent Loop BB42_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	(%rsi,%rbp,4), %xmm1
	maxsd	%xmm0, %xmm1
	decq	%rdx
	addq	$4, %rsi
	cmpq	%rdx, %rax
	jne	.LBB42_7
# BB#8:                                 # %.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB42_3 Depth=1
	movq	%rbp, %rax
	subq	%rdx, %rax
	cmpq	$3, %rdi
	jae	.LBB42_10
	jmp	.LBB42_2
.LBB42_5:                               #   in Loop: Header=BB42_3 Depth=1
	movq	%rbp, %rax
	cmpq	$3, %rdi
	jb	.LBB42_2
.LBB42_10:                              # %.lr.ph62.new
                                        #   in Loop: Header=BB42_3 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	subq	%rax, %rdi
	leaq	12(%r8,%rax,4), %rsi
	.p2align	4, 0x90
.LBB42_11:                              #   Parent Loop BB42_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	-12(%rsi), %xmm0
	maxsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	-8(%rsi), %xmm1
	maxsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	-4(%rsi), %xmm0
	maxsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	(%rsi), %xmm1
	maxsd	%xmm0, %xmm1
	addq	$16, %rsi
	addq	$-4, %rdi
	jne	.LBB42_11
.LBB42_2:                               # %.loopexit52
                                        #   in Loop: Header=BB42_3 Depth=1
	incq	%rbp
	addb	$3, %bl
	cmpq	%r14, %rcx
	jne	.LBB42_3
.LBB42_12:                              # %._crit_edge67
	divsd	.LCPI42_0(%rip), %xmm1
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movl	$.L.str.22, %esi
	movl	$1, %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%r15d, %edx
	callq	fprintf
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI42_1(%rip), %xmm0
	movl	$.L.str.26, %esi
	movb	$1, %al
	movq	%r13, %rdi
	callq	fprintf
	testl	%r15d, %r15d
	jle	.LBB42_15
# BB#13:                                # %.lr.ph58.preheader
	movl	%r15d, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB42_14:                              # %.lr.ph58
                                        # =>This Inner Loop Header: Depth=1
	incq	%rbx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%ebx, %edx
	movq	%r12, %rcx
	callq	fprintf
	addq	$256, %r12              # imm = 0x100
	cmpq	%rbx, %rbp
	jne	.LBB42_14
.LBB42_15:                              # %.preheader
	cmpl	$2, %r15d
	jl	.LBB42_24
# BB#16:                                # %.lr.ph55.preheader
	movl	8(%rsp), %ebx           # 4-byte Reload
	movslq	%r15d, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%r15d, %ebp
	movl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB42_18:                              # %.lr.ph55
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB42_20 Depth 2
	leaq	1(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	jge	.LBB42_17
# BB#19:                                # %.lr.ph
                                        #   in Loop: Header=BB42_18 Depth=1
	movl	$1, %r14d
	movq	8(%rsp), %r15           # 8-byte Reload
	.p2align	4, 0x90
.LBB42_20:                              #   Parent Loop BB42_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r12,8), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	(%rax,%r15,4), %xmm0
	cvtss2sd	%xmm0, %xmm0
	divsd	.LCPI42_0(%rip), %xmm0
	movl	$.L.str.28, %esi
	movb	$1, %al
	movq	%r13, %rdi
	callq	fprintf
	cmpq	%r15, %rbx
	je	.LBB42_22
# BB#21:                                #   in Loop: Header=BB42_20 Depth=2
	movslq	%r14d, %rax
	imulq	$715827883, %rax, %rcx  # imm = 0x2AAAAAAB
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$33, %rcx
	addl	%edx, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,2), %ecx
	subl	%ecx, %eax
	jne	.LBB42_23
.LBB42_22:                              #   in Loop: Header=BB42_20 Depth=2
	movl	$10, %edi
	movq	%r13, %rsi
	callq	fputc
.LBB42_23:                              #   in Loop: Header=BB42_20 Depth=2
	incq	%r15
	incl	%r14d
	cmpq	%r15, %rbp
	jne	.LBB42_20
.LBB42_17:                              # %.loopexit
                                        #   in Loop: Header=BB42_18 Depth=1
	incq	8(%rsp)                 # 8-byte Folded Spill
	movq	16(%rsp), %r12          # 8-byte Reload
	cmpq	%rbx, %r12
	jne	.LBB42_18
.LBB42_24:                              # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end42:
	.size	WriteHat2_int, .Lfunc_end42-WriteHat2_int
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI43_0:
	.quad	4612811918334230528     # double 2.5
	.text
	.globl	WriteHat2
	.p2align	4, 0x90
	.type	WriteHat2,@function
WriteHat2:                              # @WriteHat2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi448:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi449:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi450:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi451:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi452:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi453:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi454:
	.cfi_def_cfa_offset 96
.Lcfi455:
	.cfi_offset %rbx, -56
.Lcfi456:
	.cfi_offset %r12, -48
.Lcfi457:
	.cfi_offset %r13, -40
.Lcfi458:
	.cfi_offset %r14, -32
.Lcfi459:
	.cfi_offset %r15, -24
.Lcfi460:
	.cfi_offset %rbp, -16
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rdx, %r12
	movl	%esi, %r15d
	movq	%rdi, %r13
	leal	-1(%r15), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	xorpd	%xmm2, %xmm2
	cmpl	$2, %r15d
	jl	.LBB43_12
# BB#1:                                 # %.lr.ph65.preheader
	movslq	%r15d, %r11
	movl	%r15d, %eax
	movl	8(%rsp), %r14d          # 4-byte Reload
	leaq	3(%rax), %r9
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	-2(%rax), %r10
	movl	%r15d, %ebx
	addb	$3, %bl
	xorpd	%xmm2, %xmm2
	movl	$1, %ebp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB43_3:                               # %.lr.ph65
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB43_7 Depth 2
                                        #     Child Loop BB43_11 Depth 2
	movq	%rcx, %rax
	leaq	1(%rax), %rcx
	cmpq	%r11, %rcx
	jge	.LBB43_2
# BB#4:                                 # %.lr.ph61
                                        #   in Loop: Header=BB43_3 Depth=1
	movq	%r10, %rdi
	subq	%rax, %rdi
	movl	%r9d, %edx
	subl	%eax, %edx
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi,%rax,8), %r8
	testb	$3, %dl
	je	.LBB43_5
# BB#6:                                 # %.prol.preheader
                                        #   in Loop: Header=BB43_3 Depth=1
	movl	%ebx, %eax
	andb	$3, %al
	movzbl	%al, %eax
	negq	%rax
	movq	%r8, %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB43_7:                               #   Parent Loop BB43_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm2, %xmm0
	movsd	(%rsi,%rbp,8), %xmm2    # xmm2 = mem[0],zero
	maxsd	%xmm0, %xmm2
	decq	%rdx
	addq	$8, %rsi
	cmpq	%rdx, %rax
	jne	.LBB43_7
# BB#8:                                 # %.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB43_3 Depth=1
	movq	%rbp, %rax
	subq	%rdx, %rax
	cmpq	$3, %rdi
	jae	.LBB43_10
	jmp	.LBB43_2
.LBB43_5:                               #   in Loop: Header=BB43_3 Depth=1
	movq	%rbp, %rax
	cmpq	$3, %rdi
	jb	.LBB43_2
.LBB43_10:                              # %.lr.ph61.new
                                        #   in Loop: Header=BB43_3 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	subq	%rax, %rdi
	leaq	24(%r8,%rax,8), %rsi
	.p2align	4, 0x90
.LBB43_11:                              #   Parent Loop BB43_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-24(%rsi), %xmm0        # xmm0 = mem[0],zero
	movsd	-16(%rsi), %xmm1        # xmm1 = mem[0],zero
	maxsd	%xmm2, %xmm0
	maxsd	%xmm0, %xmm1
	movsd	-8(%rsi), %xmm0         # xmm0 = mem[0],zero
	maxsd	%xmm1, %xmm0
	movsd	(%rsi), %xmm2           # xmm2 = mem[0],zero
	maxsd	%xmm0, %xmm2
	addq	$32, %rsi
	addq	$-4, %rdi
	jne	.LBB43_11
.LBB43_2:                               # %.loopexit51
                                        #   in Loop: Header=BB43_3 Depth=1
	incq	%rbp
	addb	$3, %bl
	cmpq	%r14, %rcx
	jne	.LBB43_3
.LBB43_12:                              # %._crit_edge66
	movsd	%xmm2, 16(%rsp)         # 8-byte Spill
	movl	$.L.str.22, %esi
	movl	$1, %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%r15d, %edx
	callq	fprintf
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI43_0(%rip), %xmm0
	movl	$.L.str.26, %esi
	movb	$1, %al
	movq	%r13, %rdi
	callq	fprintf
	testl	%r15d, %r15d
	jle	.LBB43_15
# BB#13:                                # %.lr.ph57.preheader
	movl	%r15d, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB43_14:                              # %.lr.ph57
                                        # =>This Inner Loop Header: Depth=1
	incq	%rbx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movl	%ebx, %edx
	movq	%r12, %rcx
	callq	fprintf
	addq	$256, %r12              # imm = 0x100
	cmpq	%rbx, %rbp
	jne	.LBB43_14
.LBB43_15:                              # %.preheader
	cmpl	$2, %r15d
	jl	.LBB43_24
# BB#16:                                # %.lr.ph54.preheader
	movl	8(%rsp), %ebx           # 4-byte Reload
	movslq	%r15d, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%r15d, %ebp
	movl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB43_18:                              # %.lr.ph54
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB43_20 Depth 2
	leaq	1(%r12), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	jge	.LBB43_17
# BB#19:                                # %.lr.ph
                                        #   in Loop: Header=BB43_18 Depth=1
	movl	$1, %r14d
	movq	8(%rsp), %r15           # 8-byte Reload
	.p2align	4, 0x90
.LBB43_20:                              #   Parent Loop BB43_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r12,8), %rax
	movsd	(%rax,%r15,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.28, %esi
	movb	$1, %al
	movq	%r13, %rdi
	callq	fprintf
	cmpq	%r15, %rbx
	je	.LBB43_22
# BB#21:                                #   in Loop: Header=BB43_20 Depth=2
	movslq	%r14d, %rax
	imulq	$715827883, %rax, %rcx  # imm = 0x2AAAAAAB
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$33, %rcx
	addl	%edx, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,2), %ecx
	subl	%ecx, %eax
	jne	.LBB43_23
.LBB43_22:                              #   in Loop: Header=BB43_20 Depth=2
	movl	$10, %edi
	movq	%r13, %rsi
	callq	fputc
.LBB43_23:                              #   in Loop: Header=BB43_20 Depth=2
	incq	%r15
	incl	%r14d
	cmpq	%r15, %rbp
	jne	.LBB43_20
.LBB43_17:                              # %.loopexit
                                        #   in Loop: Header=BB43_18 Depth=1
	incq	8(%rsp)                 # 8-byte Folded Spill
	movq	16(%rsp), %r12          # 8-byte Reload
	cmpq	%rbx, %r12
	jne	.LBB43_18
.LBB43_24:                              # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end43:
	.size	WriteHat2, .Lfunc_end43-WriteHat2
	.cfi_endproc

	.globl	WriteHat2plain
	.p2align	4, 0x90
	.type	WriteHat2plain,@function
WriteHat2plain:                         # @WriteHat2plain
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi461:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi462:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi463:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi464:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi465:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi466:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi467:
	.cfi_def_cfa_offset 80
.Lcfi468:
	.cfi_offset %rbx, -56
.Lcfi469:
	.cfi_offset %r12, -48
.Lcfi470:
	.cfi_offset %r13, -40
.Lcfi471:
	.cfi_offset %r14, -32
.Lcfi472:
	.cfi_offset %r15, -24
.Lcfi473:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r15
	cmpl	$2, %esi
	jl	.LBB44_6
# BB#1:                                 # %.lr.ph18.preheader
	leal	-1(%rsi), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movslq	%esi, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%esi, %r13d
	movl	$1, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB44_3:                               # %.lr.ph18
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB44_5 Depth 2
	movq	%rbp, %r12
	leaq	1(%r12), %rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jge	.LBB44_2
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB44_3 Depth=1
	movq	(%rsp), %rbx            # 8-byte Reload
	.p2align	4, 0x90
.LBB44_5:                               #   Parent Loop BB44_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%r12,8), %rax
	movsd	(%rax,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rbx
	movl	$.L.str.29, %esi
	movb	$1, %al
	movq	%r15, %rdi
	movl	%ebp, %edx
	movl	%ebx, %ecx
	callq	fprintf
	cmpq	%rbx, %r13
	jne	.LBB44_5
.LBB44_2:                               # %.loopexit
                                        #   in Loop: Header=BB44_3 Depth=1
	incq	(%rsp)                  # 8-byte Folded Spill
	cmpq	16(%rsp), %rbp          # 8-byte Folded Reload
	jne	.LBB44_3
.LBB44_6:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end44:
	.size	WriteHat2plain, .Lfunc_end44-WriteHat2plain
	.cfi_endproc

	.globl	ReadFasta_sub
	.p2align	4, 0x90
	.type	ReadFasta_sub,@function
ReadFasta_sub:                          # @ReadFasta_sub
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi474:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi475:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi476:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi477:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi478:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi479:
	.cfi_def_cfa_offset 56
	subq	$200280, %rsp           # imm = 0x30E58
.Lcfi480:
	.cfi_def_cfa_offset 200336
.Lcfi481:
	.cfi_offset %rbx, -56
.Lcfi482:
	.cfi_offset %r12, -48
.Lcfi483:
	.cfi_offset %r13, -40
.Lcfi484:
	.cfi_offset %r14, -32
.Lcfi485:
	.cfi_offset %r15, -24
.Lcfi486:
	.cfi_offset %rbp, -16
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	%edx, %r12d
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, %r14
	testl	%r12d, %r12d
	jle	.LBB45_13
# BB#1:                                 # %.lr.ph37
	xorl	%ebx, %ebx
	movl	$1, %ebp
	leaq	28(%rsp), %r13
	leaq	16(%rsp), %r15
	.p2align	4, 0x90
.LBB45_2:                               # =>This Inner Loop Header: Depth=1
	movl	$255, %esi
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	fgets
	movl	$.L.str.30, %edi
	movl	$12, %edx
	movq	%r15, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB45_4
# BB#3:                                 #   in Loop: Header=BB45_2 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movslq	%ebx, %rbx
	movl	%eax, 272(%rsp,%rbx,4)
	incl	%ebx
.LBB45_4:                               #   in Loop: Header=BB45_2 Depth=1
	cmpl	$9999999, %ebp          # imm = 0x98967F
	jg	.LBB45_6
# BB#5:                                 #   in Loop: Header=BB45_2 Depth=1
	incl	%ebp
	cmpl	%r12d, %ebx
	jl	.LBB45_2
.LBB45_6:                               # %.critedge.preheader
	testl	%r12d, %r12d
	jle	.LBB45_13
# BB#7:                                 # %.lr.ph.preheader
	leal	-1(%r12), %eax
	leaq	8(,%rax,8), %rdx
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	memset
	movl	$1, %r15d
	leaq	16(%rsp), %r13
	.p2align	4, 0x90
.LBB45_8:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	fgets
	testq	%rax, %rax
	jne	.LBB45_13
# BB#9:                                 #   in Loop: Header=BB45_8 Depth=1
	movslq	%ebp, %rax
	movslq	272(%rsp,%rax,4), %rbx
	movq	%rbx, %rdi
	shlq	$8, %rdi
	addq	8(%rsp), %rdi           # 8-byte Folded Reload
	movl	$20, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB45_11
# BB#10:                                #   in Loop: Header=BB45_8 Depth=1
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	fgets
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	strtod
	movq	(%rsp), %rax            # 8-byte Reload
	movsd	%xmm0, (%rax,%rbx,8)
	incl	%ebp
.LBB45_11:                              #   in Loop: Header=BB45_8 Depth=1
	cmpl	$99999, %r15d           # imm = 0x1869F
	jg	.LBB45_13
# BB#12:                                #   in Loop: Header=BB45_8 Depth=1
	incl	%r15d
	cmpl	%r12d, %ebp
	jl	.LBB45_8
.LBB45_13:                              # %.critedge1
	xorl	%eax, %eax
	addq	$200280, %rsp           # imm = 0x30E58
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end45:
	.size	ReadFasta_sub, .Lfunc_end45-ReadFasta_sub
	.cfi_endproc

	.globl	ReadSsearch
	.p2align	4, 0x90
	.type	ReadSsearch,@function
ReadSsearch:                            # @ReadSsearch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi487:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi488:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi489:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi490:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi491:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi492:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi493:
	.cfi_def_cfa_offset 336
.Lcfi494:
	.cfi_offset %rbx, -56
.Lcfi495:
	.cfi_offset %r12, -48
.Lcfi496:
	.cfi_offset %r13, -40
.Lcfi497:
	.cfi_offset %r14, -32
.Lcfi498:
	.cfi_offset %r15, -24
.Lcfi499:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r15
	testl	%ebx, %ebx
	jle	.LBB46_6
# BB#1:                                 # %.lr.ph
	movl	$1, %r12d
	leaq	16(%rsp), %rbp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB46_2:                               # =>This Inner Loop Header: Depth=1
	movl	$255, %esi
	movq	%rbp, %rdi
	movq	%r15, %rdx
	callq	fgets
	movl	$.L.str.30, %edi
	movl	$12, %edx
	movq	%rbp, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB46_4
# BB#3:                                 #   in Loop: Header=BB46_2 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	28(%rsp), %rdi
	callq	strtol
	movq	%rax, %r13
	movl	$.L.str.31, %esi
	xorl	%eax, %eax
	leaq	91(%rsp), %rdi
	leaq	4(%rsp), %rdx
	callq	sscanf
	cvtsi2sdl	4(%rsp), %xmm0
	movslq	%r13d, %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movsd	%xmm0, (%rcx,%rax,8)
	incl	%r14d
.LBB46_4:                               #   in Loop: Header=BB46_2 Depth=1
	cmpl	$9999999, %r12d         # imm = 0x98967F
	jg	.LBB46_6
# BB#5:                                 #   in Loop: Header=BB46_2 Depth=1
	incl	%r12d
	cmpl	%ebx, %r14d
	jl	.LBB46_2
.LBB46_6:                               # %.critedge
	xorl	%eax, %eax
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end46:
	.size	ReadSsearch, .Lfunc_end46-ReadSsearch
	.cfi_endproc

	.globl	ReadBlastm7_avscore
	.p2align	4, 0x90
	.type	ReadBlastm7_avscore,@function
ReadBlastm7_avscore:                    # @ReadBlastm7_avscore
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi500:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi501:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi502:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi503:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi504:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi505:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi506:
	.cfi_def_cfa_offset 352
.Lcfi507:
	.cfi_offset %rbx, -56
.Lcfi508:
	.cfi_offset %r12, -48
.Lcfi509:
	.cfi_offset %r13, -40
.Lcfi510:
	.cfi_offset %r14, -32
.Lcfi511:
	.cfi_offset %r15, -24
.Lcfi512:
	.cfi_offset %rbp, -16
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movslq	%edx, %rdi
	movl	$4, %esi
	callq	calloc
	movq	%rax, %r15
	leaq	57(%rsp), %r13
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	leaq	61(%rsp), %rbp
	xorl	%r12d, %r12d
	leaq	32(%rsp), %r14
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
.LBB47_1:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB47_2 Depth 2
                                        #       Child Loop BB47_3 Depth 3
                                        #       Child Loop BB47_8 Depth 3
                                        #       Child Loop BB47_11 Depth 3
                                        #       Child Loop BB47_13 Depth 3
                                        #       Child Loop BB47_15 Depth 3
                                        #       Child Loop BB47_17 Depth 3
                                        #       Child Loop BB47_19 Depth 3
                                        #       Child Loop BB47_22 Depth 3
                                        #       Child Loop BB47_25 Depth 3
                                        #       Child Loop BB47_27 Depth 3
                                        #       Child Loop BB47_30 Depth 3
                                        #       Child Loop BB47_32 Depth 3
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB47_2:                               #   Parent Loop BB47_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB47_3 Depth 3
                                        #       Child Loop BB47_8 Depth 3
                                        #       Child Loop BB47_11 Depth 3
                                        #       Child Loop BB47_13 Depth 3
                                        #       Child Loop BB47_15 Depth 3
                                        #       Child Loop BB47_17 Depth 3
                                        #       Child Loop BB47_19 Depth 3
                                        #       Child Loop BB47_22 Depth 3
                                        #       Child Loop BB47_25 Depth 3
                                        #       Child Loop BB47_27 Depth 3
                                        #       Child Loop BB47_30 Depth 3
                                        #       Child Loop BB47_32 Depth 3
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB47_37
	.p2align	4, 0x90
.LBB47_3:                               # %.preheader77
                                        #   Parent Loop BB47_1 Depth=1
                                        #     Parent Loop BB47_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB47_6
# BB#4:                                 #   in Loop: Header=BB47_3 Depth=3
	movl	$.L.str.32, %edi
	movl	$19, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB47_6
# BB#5:                                 #   in Loop: Header=BB47_3 Depth=3
	movl	$.L.str.33, %edi
	movl	$23, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB47_3
.LBB47_6:                               #   in Loop: Header=BB47_2 Depth=2
	movl	$.L.str.32, %edi
	movl	$19, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB47_8
# BB#7:                                 #   in Loop: Header=BB47_2 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	63(%rsp), %rdi
	callq	strtol
	movl	%eax, (%r15,%r12,4)
	.p2align	4, 0x90
.LBB47_8:                               # %.preheader76
                                        #   Parent Loop BB47_1 Depth=1
                                        #     Parent Loop BB47_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB47_10
# BB#9:                                 #   in Loop: Header=BB47_8 Depth=3
	movl	$.L.str.34, %edi
	movl	$25, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB47_8
.LBB47_10:                              #   in Loop: Header=BB47_2 Depth=2
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	strtod
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB47_11:                              #   Parent Loop BB47_1 Depth=1
                                        #     Parent Loop BB47_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB47_13
# BB#12:                                #   in Loop: Header=BB47_11 Depth=3
	movl	$.L.str.35, %edi
	movl	$30, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB47_11
	.p2align	4, 0x90
.LBB47_13:                              # %.preheader75
                                        #   Parent Loop BB47_1 Depth=1
                                        #     Parent Loop BB47_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB47_15
# BB#14:                                #   in Loop: Header=BB47_13 Depth=3
	movl	$.L.str.36, %edi
	movl	$28, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB47_13
	.p2align	4, 0x90
.LBB47_15:                              # %.preheader74
                                        #   Parent Loop BB47_1 Depth=1
                                        #     Parent Loop BB47_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB47_17
# BB#16:                                #   in Loop: Header=BB47_15 Depth=3
	movl	$.L.str.37, %edi
	movl	$28, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB47_15
	.p2align	4, 0x90
.LBB47_17:                              # %.preheader73
                                        #   Parent Loop BB47_1 Depth=1
                                        #     Parent Loop BB47_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB47_19
# BB#18:                                #   in Loop: Header=BB47_17 Depth=3
	movl	$.L.str.38, %edi
	movl	$26, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB47_17
	.p2align	4, 0x90
.LBB47_19:                              # %.preheader
                                        #   Parent Loop BB47_1 Depth=1
                                        #     Parent Loop BB47_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB47_21
# BB#20:                                #   in Loop: Header=BB47_19 Depth=3
	movl	$.L.str.39, %edi
	movl	$29, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB47_19
.LBB47_21:                              #   in Loop: Header=BB47_2 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbp, %rdi
	callq	strtol
	cvtsi2sdl	%eax, %xmm0
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB47_22:                              #   Parent Loop BB47_1 Depth=1
                                        #     Parent Loop BB47_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$ReadBlastm7_avscore.al, %edi
	movl	$4999900, %esi          # imm = 0x4C4ADC
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB47_24
# BB#23:                                #   in Loop: Header=BB47_22 Depth=3
	movl	$.L.str.40, %edi
	movl	$ReadBlastm7_avscore.al, %esi
	movl	$24, %edx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB47_22
.LBB47_24:                              #   in Loop: Header=BB47_2 Depth=2
	movl	$ReadBlastm7_avscore.qal, %edi
	movl	$ReadBlastm7_avscore.al+24, %esi
	callq	strcpy
	movl	$ReadBlastm7_avscore.qal+1, %eax
	.p2align	4, 0x90
.LBB47_25:                              #   Parent Loop BB47_1 Depth=1
                                        #     Parent Loop BB47_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$60, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB47_25
# BB#26:                                #   in Loop: Header=BB47_2 Depth=2
	movb	$0, -1(%rax)
	.p2align	4, 0x90
.LBB47_27:                              #   Parent Loop BB47_1 Depth=1
                                        #     Parent Loop BB47_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$ReadBlastm7_avscore.al, %edi
	movl	$4999900, %esi          # imm = 0x4C4ADC
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB47_29
# BB#28:                                #   in Loop: Header=BB47_27 Depth=3
	movl	$.L.str.41, %edi
	movl	$ReadBlastm7_avscore.al, %esi
	movl	$24, %edx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB47_27
.LBB47_29:                              #   in Loop: Header=BB47_2 Depth=2
	movl	$ReadBlastm7_avscore.tal, %edi
	movl	$ReadBlastm7_avscore.al+24, %esi
	callq	strcpy
	movl	$ReadBlastm7_avscore.tal+1, %eax
	.p2align	4, 0x90
.LBB47_30:                              #   Parent Loop BB47_1 Depth=1
                                        #     Parent Loop BB47_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$60, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB47_30
# BB#31:                                #   in Loop: Header=BB47_2 Depth=2
	movb	$0, -1(%rax)
	.p2align	4, 0x90
.LBB47_32:                              #   Parent Loop BB47_1 Depth=1
                                        #     Parent Loop BB47_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB47_34
# BB#33:                                #   in Loop: Header=BB47_32 Depth=3
	movl	$.L.str.42, %edi
	movl	$18, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB47_32
.LBB47_34:                              #   in Loop: Header=BB47_2 Depth=2
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$.L.str.43, %edi
	movl	$21, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB47_2
# BB#35:                                #   in Loop: Header=BB47_1 Depth=1
	movslq	(%r15,%r12,4), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, (%rcx,%rax,8)
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	xorpd	%xmm1, %xmm1
	divsd	(%rsp), %xmm1           # 8-byte Folded Reload
	cvttsd2si	%xmm1, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	ucomisd	%xmm0, %xmm1
	jne	.LBB47_38
	jp	.LBB47_38
# BB#36:                                #   in Loop: Header=BB47_1 Depth=1
	incq	%r12
	movl	$.L.str.45, %edi
	movl	$23, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB47_1
.LBB47_37:                              # %.loopexit
	movq	%r15, %rdi
	callq	free
	cvttsd2si	8(%rsp), %eax   # 8-byte Folded Reload
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB47_38:
	movq	stderr(%rip), %rdi
	movl	$.L.str.44, %esi
	xorps	%xmm0, %xmm0
	movb	$3, %al
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end47:
	.size	ReadBlastm7_avscore, .Lfunc_end47-ReadBlastm7_avscore
	.cfi_endproc

	.globl	ReadBlastm7_scoreonly
	.p2align	4, 0x90
	.type	ReadBlastm7_scoreonly,@function
ReadBlastm7_scoreonly:                  # @ReadBlastm7_scoreonly
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi513:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi514:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi515:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi516:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi517:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi518:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi519:
	.cfi_def_cfa_offset 336
.Lcfi520:
	.cfi_offset %rbx, -56
.Lcfi521:
	.cfi_offset %r12, -48
.Lcfi522:
	.cfi_offset %r13, -40
.Lcfi523:
	.cfi_offset %r14, -32
.Lcfi524:
	.cfi_offset %r15, -24
.Lcfi525:
	.cfi_offset %rbp, -16
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %rbx
	movslq	%edx, %rdi
	movl	$4, %esi
	callq	calloc
	movq	%rax, %r15
	leaq	47(%rsp), %r13
	leaq	41(%rsp), %rbp
	xorl	%r12d, %r12d
	leaq	16(%rsp), %r14
.LBB48_1:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB48_2 Depth 2
                                        #       Child Loop BB48_3 Depth 3
                                        #       Child Loop BB48_8 Depth 3
                                        #       Child Loop BB48_11 Depth 3
                                        #       Child Loop BB48_13 Depth 3
                                        #       Child Loop BB48_15 Depth 3
                                        #       Child Loop BB48_17 Depth 3
                                        #       Child Loop BB48_19 Depth 3
                                        #       Child Loop BB48_21 Depth 3
                                        #       Child Loop BB48_24 Depth 3
                                        #       Child Loop BB48_26 Depth 3
                                        #       Child Loop BB48_29 Depth 3
                                        #       Child Loop BB48_31 Depth 3
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB48_2:                               #   Parent Loop BB48_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB48_3 Depth 3
                                        #       Child Loop BB48_8 Depth 3
                                        #       Child Loop BB48_11 Depth 3
                                        #       Child Loop BB48_13 Depth 3
                                        #       Child Loop BB48_15 Depth 3
                                        #       Child Loop BB48_17 Depth 3
                                        #       Child Loop BB48_19 Depth 3
                                        #       Child Loop BB48_21 Depth 3
                                        #       Child Loop BB48_24 Depth 3
                                        #       Child Loop BB48_26 Depth 3
                                        #       Child Loop BB48_29 Depth 3
                                        #       Child Loop BB48_31 Depth 3
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB48_35
	.p2align	4, 0x90
.LBB48_3:                               # %.preheader63
                                        #   Parent Loop BB48_1 Depth=1
                                        #     Parent Loop BB48_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB48_6
# BB#4:                                 #   in Loop: Header=BB48_3 Depth=3
	movl	$.L.str.32, %edi
	movl	$19, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB48_6
# BB#5:                                 #   in Loop: Header=BB48_3 Depth=3
	movl	$.L.str.33, %edi
	movl	$23, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB48_3
.LBB48_6:                               #   in Loop: Header=BB48_2 Depth=2
	movl	$.L.str.32, %edi
	movl	$19, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB48_8
# BB#7:                                 #   in Loop: Header=BB48_2 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movl	%eax, (%r15,%r12,4)
	.p2align	4, 0x90
.LBB48_8:                               # %.preheader62
                                        #   Parent Loop BB48_1 Depth=1
                                        #     Parent Loop BB48_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB48_10
# BB#9:                                 #   in Loop: Header=BB48_8 Depth=3
	movl	$.L.str.34, %edi
	movl	$25, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB48_8
.LBB48_10:                              #   in Loop: Header=BB48_2 Depth=2
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	strtod
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB48_11:                              #   Parent Loop BB48_1 Depth=1
                                        #     Parent Loop BB48_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB48_13
# BB#12:                                #   in Loop: Header=BB48_11 Depth=3
	movl	$.L.str.35, %edi
	movl	$30, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB48_11
	.p2align	4, 0x90
.LBB48_13:                              # %.preheader61
                                        #   Parent Loop BB48_1 Depth=1
                                        #     Parent Loop BB48_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB48_15
# BB#14:                                #   in Loop: Header=BB48_13 Depth=3
	movl	$.L.str.36, %edi
	movl	$28, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB48_13
	.p2align	4, 0x90
.LBB48_15:                              # %.preheader60
                                        #   Parent Loop BB48_1 Depth=1
                                        #     Parent Loop BB48_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB48_17
# BB#16:                                #   in Loop: Header=BB48_15 Depth=3
	movl	$.L.str.37, %edi
	movl	$28, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB48_15
	.p2align	4, 0x90
.LBB48_17:                              # %.preheader59
                                        #   Parent Loop BB48_1 Depth=1
                                        #     Parent Loop BB48_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB48_19
# BB#18:                                #   in Loop: Header=BB48_17 Depth=3
	movl	$.L.str.38, %edi
	movl	$26, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB48_17
	.p2align	4, 0x90
.LBB48_19:                              # %.preheader58
                                        #   Parent Loop BB48_1 Depth=1
                                        #     Parent Loop BB48_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB48_21
# BB#20:                                #   in Loop: Header=BB48_19 Depth=3
	movl	$.L.str.39, %edi
	movl	$29, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB48_19
	.p2align	4, 0x90
.LBB48_21:                              # %.preheader
                                        #   Parent Loop BB48_1 Depth=1
                                        #     Parent Loop BB48_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$ReadBlastm7_scoreonly.al, %edi
	movl	$4999900, %esi          # imm = 0x4C4ADC
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB48_23
# BB#22:                                #   in Loop: Header=BB48_21 Depth=3
	movl	$.L.str.40, %edi
	movl	$ReadBlastm7_scoreonly.al, %esi
	movl	$24, %edx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB48_21
.LBB48_23:                              #   in Loop: Header=BB48_2 Depth=2
	movl	$ReadBlastm7_scoreonly.qal, %edi
	movl	$ReadBlastm7_scoreonly.al+24, %esi
	callq	strcpy
	movl	$ReadBlastm7_scoreonly.qal+1, %eax
	.p2align	4, 0x90
.LBB48_24:                              #   Parent Loop BB48_1 Depth=1
                                        #     Parent Loop BB48_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$60, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB48_24
# BB#25:                                #   in Loop: Header=BB48_2 Depth=2
	movb	$0, -1(%rax)
	.p2align	4, 0x90
.LBB48_26:                              #   Parent Loop BB48_1 Depth=1
                                        #     Parent Loop BB48_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$ReadBlastm7_scoreonly.al, %edi
	movl	$4999900, %esi          # imm = 0x4C4ADC
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB48_28
# BB#27:                                #   in Loop: Header=BB48_26 Depth=3
	movl	$.L.str.41, %edi
	movl	$ReadBlastm7_scoreonly.al, %esi
	movl	$24, %edx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB48_26
.LBB48_28:                              #   in Loop: Header=BB48_2 Depth=2
	movl	$ReadBlastm7_scoreonly.tal, %edi
	movl	$ReadBlastm7_scoreonly.al+24, %esi
	callq	strcpy
	movl	$ReadBlastm7_scoreonly.tal+1, %eax
	.p2align	4, 0x90
.LBB48_29:                              #   Parent Loop BB48_1 Depth=1
                                        #     Parent Loop BB48_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$60, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB48_29
# BB#30:                                #   in Loop: Header=BB48_2 Depth=2
	movb	$0, -1(%rax)
	.p2align	4, 0x90
.LBB48_31:                              #   Parent Loop BB48_1 Depth=1
                                        #     Parent Loop BB48_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB48_33
# BB#32:                                #   in Loop: Header=BB48_31 Depth=3
	movl	$.L.str.42, %edi
	movl	$18, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB48_31
.LBB48_33:                              #   in Loop: Header=BB48_2 Depth=2
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$.L.str.43, %edi
	movl	$21, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB48_2
# BB#34:                                #   in Loop: Header=BB48_1 Depth=1
	movslq	(%r15,%r12,4), %rax
	incq	%r12
	movq	8(%rsp), %rcx           # 8-byte Reload
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, (%rcx,%rax,8)
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$255, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$.L.str.45, %edi
	movl	$23, %edx
	movq	%r14, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB48_1
.LBB48_35:                              # %.loopexit
	movq	%r15, %rdi
	callq	free
	movl	%r12d, %eax
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end48:
	.size	ReadBlastm7_scoreonly, .Lfunc_end48-ReadBlastm7_scoreonly
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI49_0:
	.quad	4618216237887075123     # double 5.7999999999999998
.LCPI49_1:
	.quad	4648488871632306176     # double 600
	.text
	.globl	ReadBlastm7
	.p2align	4, 0x90
	.type	ReadBlastm7,@function
ReadBlastm7:                            # @ReadBlastm7
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi526:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi527:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi528:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi529:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi530:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi531:
	.cfi_def_cfa_offset 56
	subq	$376, %rsp              # imm = 0x178
.Lcfi532:
	.cfi_def_cfa_offset 432
.Lcfi533:
	.cfi_offset %rbx, -56
.Lcfi534:
	.cfi_offset %r12, -48
.Lcfi535:
	.cfi_offset %r13, -40
.Lcfi536:
	.cfi_offset %r14, -32
.Lcfi537:
	.cfi_offset %r15, -24
.Lcfi538:
	.cfi_offset %rbp, -16
	movq	%r8, 96(%rsp)           # 8-byte Spill
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%rdi, %rbp
	leaq	112(%rsp), %rbx
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	movq	%rbp, 88(%rsp)          # 8-byte Spill
.LBB49_1:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB49_2 Depth 2
                                        #       Child Loop BB49_3 Depth 3
                                        #       Child Loop BB49_9 Depth 3
                                        #       Child Loop BB49_12 Depth 3
                                        #       Child Loop BB49_15 Depth 3
                                        #       Child Loop BB49_17 Depth 3
                                        #       Child Loop BB49_20 Depth 3
                                        #       Child Loop BB49_22 Depth 3
                                        #       Child Loop BB49_24 Depth 3
                                        #       Child Loop BB49_27 Depth 3
                                        #       Child Loop BB49_29 Depth 3
                                        #       Child Loop BB49_32 Depth 3
                                        #       Child Loop BB49_38 Depth 3
                                        #       Child Loop BB49_40 Depth 3
                                        #       Child Loop BB49_45 Depth 3
                                        #       Child Loop BB49_63 Depth 3
                                        #       Child Loop BB49_65 Depth 3
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 72(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB49_2:                               #   Parent Loop BB49_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB49_3 Depth 3
                                        #       Child Loop BB49_9 Depth 3
                                        #       Child Loop BB49_12 Depth 3
                                        #       Child Loop BB49_15 Depth 3
                                        #       Child Loop BB49_17 Depth 3
                                        #       Child Loop BB49_20 Depth 3
                                        #       Child Loop BB49_22 Depth 3
                                        #       Child Loop BB49_24 Depth 3
                                        #       Child Loop BB49_27 Depth 3
                                        #       Child Loop BB49_29 Depth 3
                                        #       Child Loop BB49_32 Depth 3
                                        #       Child Loop BB49_38 Depth 3
                                        #       Child Loop BB49_40 Depth 3
                                        #       Child Loop BB49_45 Depth 3
                                        #       Child Loop BB49_63 Depth 3
                                        #       Child Loop BB49_65 Depth 3
	movq	%rbp, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB49_69
	.p2align	4, 0x90
.LBB49_3:                               # %.preheader64
                                        #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB49_6
# BB#4:                                 #   in Loop: Header=BB49_3 Depth=3
	movl	$.L.str.32, %edi
	movl	$19, %edx
	movq	%rbx, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB49_6
# BB#5:                                 #   in Loop: Header=BB49_3 Depth=3
	movl	$.L.str.33, %edi
	movl	$23, %edx
	movq	%rbx, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB49_3
.LBB49_6:                               #   in Loop: Header=BB49_2 Depth=2
	movl	$.L.str.32, %edi
	movl	$19, %edx
	movq	%rbx, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB49_8
# BB#7:                                 #   in Loop: Header=BB49_2 Depth=2
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	143(%rsp), %rdi
	callq	strtol
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%eax, ReadBlastm7.junban(,%rcx,4)
.LBB49_8:                               # %.preheader63
                                        #   in Loop: Header=BB49_2 Depth=2
	movl	%r14d, %r12d
	.p2align	4, 0x90
.LBB49_9:                               #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB49_11
# BB#10:                                #   in Loop: Header=BB49_9 Depth=3
	movl	$.L.str.34, %edi
	movl	$25, %edx
	movq	%rbx, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB49_9
.LBB49_11:                              #   in Loop: Header=BB49_2 Depth=2
	xorl	%esi, %esi
	leaq	137(%rsp), %rdi
	callq	strtod
	movsd	72(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 72(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB49_12:                              #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB49_14
# BB#13:                                #   in Loop: Header=BB49_12 Depth=3
	movl	$.L.str.35, %edi
	movl	$30, %edx
	movq	%rbx, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB49_12
.LBB49_14:                              #   in Loop: Header=BB49_2 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	142(%rsp), %rdi
	callq	strtol
	decl	%eax
	movq	%rax, %r13
	.p2align	4, 0x90
.LBB49_15:                              #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB49_17
# BB#16:                                #   in Loop: Header=BB49_15 Depth=3
	movl	$.L.str.36, %edi
	movl	$28, %edx
	movq	%rbx, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB49_15
	.p2align	4, 0x90
.LBB49_17:                              # %.preheader73
                                        #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB49_19
# BB#18:                                #   in Loop: Header=BB49_17 Depth=3
	movl	$.L.str.37, %edi
	movl	$28, %edx
	movq	%rbx, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB49_17
.LBB49_19:                              #   in Loop: Header=BB49_2 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	140(%rsp), %rdi
	callq	strtol
	movq	%rax, %r14
	decl	%r14d
	.p2align	4, 0x90
.LBB49_20:                              #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB49_22
# BB#21:                                #   in Loop: Header=BB49_20 Depth=3
	movl	$.L.str.38, %edi
	movl	$26, %edx
	movq	%rbx, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB49_20
	.p2align	4, 0x90
.LBB49_22:                              # %.preheader62
                                        #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB49_24
# BB#23:                                #   in Loop: Header=BB49_22 Depth=3
	movl	$.L.str.39, %edi
	movl	$29, %edx
	movq	%rbx, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB49_22
	.p2align	4, 0x90
.LBB49_24:                              # %.preheader
                                        #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$ReadBlastm7.al, %edi
	movl	$4999900, %esi          # imm = 0x4C4ADC
	movq	%rbp, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB49_26
# BB#25:                                #   in Loop: Header=BB49_24 Depth=3
	movl	$.L.str.40, %edi
	movl	$ReadBlastm7.al, %esi
	movl	$24, %edx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB49_24
.LBB49_26:                              #   in Loop: Header=BB49_2 Depth=2
	movl	$ReadBlastm7.qal, %edi
	movl	$ReadBlastm7.al+24, %esi
	callq	strcpy
	movl	$ReadBlastm7.qal+1, %eax
	.p2align	4, 0x90
.LBB49_27:                              #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$60, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB49_27
# BB#28:                                #   in Loop: Header=BB49_2 Depth=2
	movb	$0, -1(%rax)
	.p2align	4, 0x90
.LBB49_29:                              #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$ReadBlastm7.al, %edi
	movl	$4999900, %esi          # imm = 0x4C4ADC
	movq	%rbp, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB49_31
# BB#30:                                #   in Loop: Header=BB49_29 Depth=3
	movl	$.L.str.41, %edi
	movl	$ReadBlastm7.al, %esi
	movl	$24, %edx
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB49_29
.LBB49_31:                              #   in Loop: Header=BB49_2 Depth=2
	movl	$ReadBlastm7.tal, %edi
	movl	$ReadBlastm7.al+24, %esi
	callq	strcpy
	movl	$ReadBlastm7.tal+1, %eax
	.p2align	4, 0x90
.LBB49_32:                              #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$60, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB49_32
# BB#33:                                #   in Loop: Header=BB49_2 Depth=2
	movb	$0, -1(%rax)
	movq	40(%rsp), %rax          # 8-byte Reload
	movslq	ReadBlastm7.junban(,%rax,4), %rax
	leaq	(%rax,%rax,4), %r15
	shlq	$4, %r15
	addq	96(%rsp), %r15          # 8-byte Folded Reload
	movq	stderr(%rip), %rdi
	movl	$.L.str.104, %esi
	movl	$ReadBlastm7.qal, %edx
	movl	$ReadBlastm7.tal, %ecx
	xorl	%eax, %eax
	callq	fprintf
	testl	%r12d, %r12d
	movq	%r13, %rbp
	je	.LBB49_42
# BB#34:                                # %.preheader.i
                                        #   in Loop: Header=BB49_2 Depth=2
	cmpl	$2, %r12d
	jl	.LBB49_41
# BB#35:                                # %.lr.ph32.i.preheader
                                        #   in Loop: Header=BB49_2 Depth=2
	leal	7(%r12), %edx
	leal	-2(%r12), %ecx
	andl	$7, %edx
	je	.LBB49_36
# BB#37:                                # %.lr.ph32.i.prol.preheader
                                        #   in Loop: Header=BB49_2 Depth=2
	negl	%edx
	movl	%r12d, %eax
	.p2align	4, 0x90
.LBB49_38:                              # %.lr.ph32.i.prol
                                        #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	decl	%eax
	movq	8(%r15), %r15
	incl	%edx
	jne	.LBB49_38
	jmp	.LBB49_39
	.p2align	4, 0x90
.LBB49_36:                              #   in Loop: Header=BB49_2 Depth=2
	movl	%r12d, %eax
.LBB49_39:                              # %.lr.ph32.i.prol.loopexit
                                        #   in Loop: Header=BB49_2 Depth=2
	cmpl	$7, %ecx
	jb	.LBB49_41
	.p2align	4, 0x90
.LBB49_40:                              # %.lr.ph32.i
                                        #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%r15), %rcx
	movq	8(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	8(%rcx), %rcx
	addl	$-8, %eax
	movq	8(%rcx), %r15
	cmpl	$1, %eax
	jg	.LBB49_40
.LBB49_41:                              # %._crit_edge33.i
                                        #   in Loop: Header=BB49_2 Depth=2
	movl	$1, %edi
	movl	$80, %esi
	callq	calloc
	movq	%rax, %r13
	movq	%r13, 8(%r15)
	movq	stderr(%rip), %rdi
	movl	$.L.str.105, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%r13, %rcx
	callq	fprintf
	movq	%r13, %r15
.LBB49_42:                              #   in Loop: Header=BB49_2 Depth=2
	cmpb	$0, ReadBlastm7.qal(%rip)
	movq	%r12, 104(%rsp)         # 8-byte Spill
	xorpd	%xmm0, %xmm0
	jne	.LBB49_44
# BB#43:                                #   in Loop: Header=BB49_2 Depth=2
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	movl	$1, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	xorpd	%xmm0, %xmm0
	xorl	%ecx, %ecx
	xorl	%r12d, %r12d
	movq	%r15, %rax
	jmp	.LBB49_61
	.p2align	4, 0x90
.LBB49_44:                              # %.lr.ph19.i.preheader
                                        #   in Loop: Header=BB49_2 Depth=2
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	xorpd	%xmm1, %xmm1
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	movl	$0, (%rsp)              # 4-byte Folded Spill
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	jmp	.LBB49_45
.LBB49_49:                              #   in Loop: Header=BB49_45 Depth=3
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	4(%rsp), %eax           # 4-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
.LBB49_51:                              #   in Loop: Header=BB49_45 Depth=3
	movl	%eax, 4(%rsp)           # 4-byte Spill
	incl	%ebp
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movl	%eax, 24(%rcx)
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, 32(%rcx)
	movl	%ebx, 28(%rcx)
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	%r14d, 36(%rcx)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movl	20(%rsp), %ecx          # 4-byte Reload
	addl	%r13d, %ecx
	movl	%eax, %r13d
	movq	56(%rsp), %rbp          # 8-byte Reload
	addl	%ebp, %ecx
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movq	stderr(%rip), %rdi
	movl	$.L.str.108, %esi
	movb	$1, %al
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	%ebx, %ecx
	xorl	%ebx, %ebx
	movl	$.L.str.109, %esi
	xorl	%eax, %eax
	movl	4(%rsp), %edx           # 4-byte Reload
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.110, %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	movl	%r14d, %ecx
	callq	fprintf
	movq	%rbp, %rcx
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movzbl	ReadBlastm7.qal(%r12), %eax
	xorpd	%xmm1, %xmm1
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	movq	32(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB49_57
	.p2align	4, 0x90
.LBB49_45:                              # %.lr.ph19.i
                                        #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
	movq	%rbp, 32(%rsp)          # 8-byte Spill
                                        # kill: %R14D<def> %R14D<kill> %R14<kill> %R14<def>
	movq	stderr(%rip), %rcx
	movl	$.L.str.106, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	movsbl	ReadBlastm7.qal(%r12), %edx
	movsbl	ReadBlastm7.tal(%r12), %ecx
	movl	$.L.str.107, %esi
	xorl	%eax, %eax
	movl	%ebx, %ebp
	movl	%ebp, %r8d
	callq	fprintf
	movzbl	ReadBlastm7.qal(%r12), %eax
	cmpl	$1, %ebp
	jne	.LBB49_52
# BB#46:                                #   in Loop: Header=BB49_45 Depth=3
	cmpb	$45, %al
	je	.LBB49_48
# BB#47:                                #   in Loop: Header=BB49_45 Depth=3
	movzbl	ReadBlastm7.tal(%r12), %ecx
	cmpb	$45, %cl
	jne	.LBB49_56
.LBB49_48:                              #   in Loop: Header=BB49_45 Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %ebx
	movq	%r14, 56(%rsp)          # 8-byte Spill
	leal	-1(%r14), %r14d
	movq	64(%rsp), %rbp          # 8-byte Reload
	testl	%ebp, %ebp
	jle	.LBB49_49
# BB#50:                                #   in Loop: Header=BB49_45 Depth=3
	movl	$1, %edi
	movl	$80, %esi
	callq	calloc
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 8(%rcx)
	movq	$0, 8(%rax)
	movq	%rax, %rcx
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	4(%rsp), %eax           # 4-byte Reload
	jmp	.LBB49_51
	.p2align	4, 0x90
.LBB49_52:                              #   in Loop: Header=BB49_45 Depth=3
	cmpb	$45, %al
	jne	.LBB49_54
# BB#53:                                #   in Loop: Header=BB49_45 Depth=3
	movb	$45, %al
	jmp	.LBB49_55
	.p2align	4, 0x90
.LBB49_54:                              # %.thread.i
                                        #   in Loop: Header=BB49_45 Depth=3
	movzbl	ReadBlastm7.tal(%r12), %ecx
	cmpb	$45, %cl
	jne	.LBB49_56
.LBB49_55:                              #   in Loop: Header=BB49_45 Depth=3
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movl	(%rsp), %r13d           # 4-byte Reload
	movq	%r14, %rcx
	movq	32(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB49_57
	.p2align	4, 0x90
.LBB49_56:                              # %.thread.thread.i
                                        #   in Loop: Header=BB49_45 Depth=3
	movsbq	%al, %rdx
	testl	%ebp, %ebp
	movl	4(%rsp), %esi           # 4-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	cmovel	%edi, %esi
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movl	(%rsp), %r13d           # 4-byte Reload
	cmovel	%r14d, %r13d
	movl	$1, %esi
	cmovel	%esi, %ebp
	movl	%ebp, %ebx
	movslq	amino_n(,%rdx,4), %rdx
	movsbq	%cl, %rcx
	movslq	amino_n(,%rcx,4), %rcx
	imulq	$104, %rdx, %rdx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	n_dis(%rdx,%rcx,4), %xmm0
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%r14, %rcx
.LBB49_57:                              #   in Loop: Header=BB49_45 Depth=3
	xorl	%ebp, %ebp
	cmpb	$45, %al
	setne	%bpl
	addl	%edi, %ebp
	xorl	%r14d, %r14d
	cmpb	$45, ReadBlastm7.tal(%r12)
	setne	%r14b
	addl	%ecx, %r14d
	movl	%r13d, (%rsp)           # 4-byte Spill
	negl	%r13d
	cmpb	$0, ReadBlastm7.qal+1(%r12)
	leaq	1(%r12), %r12
	jne	.LBB49_45
# BB#58:                                # %._crit_edge20.i
                                        #   in Loop: Header=BB49_2 Depth=2
	movq	64(%rsp), %rcx          # 8-byte Reload
	leal	1(%rcx), %eax
	testl	%ecx, %ecx
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jle	.LBB49_59
# BB#60:                                #   in Loop: Header=BB49_2 Depth=2
	movl	$1, %edi
	movl	$80, %esi
	callq	calloc
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 8(%rcx)
	movq	$0, 8(%rax)
	movl	20(%rsp), %ebx          # 4-byte Reload
	movl	(%rsp), %ecx            # 4-byte Reload
	movl	4(%rsp), %r12d          # 4-byte Reload
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	jmp	.LBB49_61
	.p2align	4, 0x90
.LBB49_59:                              #   in Loop: Header=BB49_2 Depth=2
	movl	20(%rsp), %ebx          # 4-byte Reload
	movl	(%rsp), %ecx            # 4-byte Reload
	movl	4(%rsp), %r12d          # 4-byte Reload
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	48(%rsp), %rax          # 8-byte Reload
.LBB49_61:                              # %._crit_edge20.thread.i
                                        #   in Loop: Header=BB49_2 Depth=2
	movl	%ecx, (%rsp)            # 4-byte Spill
	decl	%ebp
	leal	-1(%r14), %edx
	movl	%edx, 32(%rsp)          # 4-byte Spill
	movl	%r12d, 24(%rax)
	movl	%ecx, 32(%rax)
	movl	%ebp, 28(%rax)
	movl	%edx, 36(%rax)
	movq	stderr(%rip), %rdi
	movl	$.L.str.111, %esi
	movb	$1, %al
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.109, %esi
	xorl	%eax, %eax
	movl	%r12d, %edx
	movl	%ebp, %ecx
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.110, %esi
	xorl	%eax, %eax
	movl	(%rsp), %edx            # 4-byte Reload
	movl	32(%rsp), %ecx          # 4-byte Reload
	callq	fprintf
	testq	%r15, %r15
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	je	.LBB49_64
# BB#62:                                # %.lr.ph.i
                                        #   in Loop: Header=BB49_2 Depth=2
	addl	%r13d, %ebx
	addl	%r14d, %ebx
	addsd	24(%rsp), %xmm1         # 8-byte Folded Reload
	mulsd	.LCPI49_0(%rip), %xmm1
	divsd	.LCPI49_1(%rip), %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	divsd	%xmm0, %xmm1
	.p2align	4, 0x90
.LBB49_63:                              #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ebx, 48(%r15)
	movsd	%xmm1, 40(%r15)
	movq	8(%r15), %r15
	testq	%r15, %r15
	jne	.LBB49_63
.LBB49_64:                              # %addlocalhom_r.exit
                                        #   in Loop: Header=BB49_2 Depth=2
	movq	56(%rsp), %r14          # 8-byte Reload
	addl	104(%rsp), %r14d        # 4-byte Folded Reload
	leaq	112(%rsp), %rbx
	movq	88(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB49_65:                              #   Parent Loop BB49_1 Depth=1
                                        #     Parent Loop BB49_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$255, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB49_67
# BB#66:                                #   in Loop: Header=BB49_65 Depth=3
	movl	$.L.str.42, %edi
	movl	$18, %edx
	movq	%rbx, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB49_65
.LBB49_67:                              #   in Loop: Header=BB49_2 Depth=2
	movl	$255, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	movl	$.L.str.43, %edi
	movl	$21, %edx
	movq	%rbx, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB49_2
# BB#68:                                #   in Loop: Header=BB49_1 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	movslq	ReadBlastm7.junban(,%rcx,4), %rax
	incq	%rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	80(%rsp), %rcx          # 8-byte Reload
	movsd	72(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, (%rcx,%rax,8)
	movl	$255, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	movl	$255, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	movl	$.L.str.45, %edi
	movl	$23, %edx
	movq	%rbx, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB49_1
.LBB49_69:                              # %.loopexit
	movq	40(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$376, %rsp              # imm = 0x178
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end49:
	.size	ReadBlastm7, .Lfunc_end49-ReadBlastm7
	.cfi_endproc

	.globl	ReadFasta34noalign
	.p2align	4, 0x90
	.type	ReadFasta34noalign,@function
ReadFasta34noalign:                     # @ReadFasta34noalign
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi539:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi540:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi541:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi542:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi543:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi544:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi545:
	.cfi_def_cfa_offset 352
.Lcfi546:
	.cfi_offset %rbx, -56
.Lcfi547:
	.cfi_offset %r12, -48
.Lcfi548:
	.cfi_offset %r13, -40
.Lcfi549:
	.cfi_offset %r14, -32
.Lcfi550:
	.cfi_offset %r15, -24
.Lcfi551:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	32(%rsp), %r12
	leaq	16(%rsp), %r15
	leaq	24(%rsp), %r13
	xorl	%ebp, %ebp
	jmp	.LBB50_1
.LBB50_3:                               #   in Loop: Header=BB50_1 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	44(%rsp), %rdi
	callq	strtol
	movl	%eax, ReadFasta34noalign.junban(,%rbp,4)
	movl	$41, %esi
	movq	%r12, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	leaq	12(%rsp), %rdx
	movq	%r15, %rcx
	movq	%r13, %r8
	callq	sscanf
	cvtsi2sdl	12(%rsp), %xmm0
	movslq	ReadFasta34noalign.junban(,%rbp,4), %rax
	movsd	%xmm0, (%r14,%rax,8)
	incq	%rbp
	.p2align	4, 0x90
.LBB50_1:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB50_4
# BB#2:                                 #   in Loop: Header=BB50_1 Depth=1
	movl	$255, %esi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$.L.str.30, %edi
	movl	$12, %edx
	movq	%r12, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB50_1
	jmp	.LBB50_3
.LBB50_4:
	movl	%ebp, %eax
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end50:
	.size	ReadFasta34noalign, .Lfunc_end50-ReadFasta34noalign
	.cfi_endproc

	.globl	ReadFasta34m10_nuc
	.p2align	4, 0x90
	.type	ReadFasta34m10_nuc,@function
ReadFasta34m10_nuc:                     # @ReadFasta34m10_nuc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi552:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi553:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi554:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi555:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi556:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi557:
	.cfi_def_cfa_offset 56
	subq	$360, %rsp              # imm = 0x168
.Lcfi558:
	.cfi_def_cfa_offset 416
.Lcfi559:
	.cfi_offset %rbx, -56
.Lcfi560:
	.cfi_offset %r12, -48
.Lcfi561:
	.cfi_offset %r13, -40
.Lcfi562:
	.cfi_offset %r14, -32
.Lcfi563:
	.cfi_offset %r15, -24
.Lcfi564:
	.cfi_offset %rbp, -16
	movq	%r8, %rbp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	feof
	movl	$-1, %r15d
	testl	%eax, %eax
	jne	.LBB51_11
# BB#1:                                 # %.lr.ph.lr.ph
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	leaq	108(%rsp), %r15
	leaq	96(%rsp), %rbp
	leaq	88(%rsp), %r12
	xorl	%r13d, %r13d
	jmp	.LBB51_2
.LBB51_4:                               # %.outer108
                                        #   in Loop: Header=BB51_2 Depth=1
	movl	$93, %esi
	movq	%rbp, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	leaq	20(%rsp), %rdx
	leaq	80(%rsp), %rcx
	movq	%r12, %r8
	callq	sscanf
	cvtsi2sdl	20(%rsp), %xmm0
	movslq	ReadFasta34m10_nuc.junban(,%r13,4), %rax
	movsd	%xmm0, (%rbx,%rax,8)
	incq	%r13
	jmp	.LBB51_6
	.p2align	4, 0x90
.LBB51_2:                               # =>This Inner Loop Header: Depth=1
	movl	$255, %esi
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	fgets
	movl	$.L.str.30, %edi
	movl	$12, %edx
	movq	%rbp, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB51_3
# BB#5:                                 #   in Loop: Header=BB51_2 Depth=1
	movl	$.L.str.47, %edi
	movl	$14, %edx
	movq	%rbp, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB51_6
	jmp	.LBB51_7
	.p2align	4, 0x90
.LBB51_3:                               #   in Loop: Header=BB51_2 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r15, %rdi
	callq	strtol
	movl	%eax, ReadFasta34m10_nuc.junban(,%r13,4)
	movl	$114, %esi
	movq	%rbp, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB51_4
.LBB51_6:                               # %.backedge
                                        #   in Loop: Header=BB51_2 Depth=1
	movq	%r14, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB51_2
.LBB51_7:                               # %.outer108._crit_edge
	testl	%r13d, %r13d
	movl	$-1, %r15d
	je	.LBB51_11
# BB#8:                                 # %.preheader105
	leaq	100(%rsp), %r12
	xorl	%r15d, %r15d
	leaq	96(%rsp), %r13
	jmp	.LBB51_9
.LBB51_50:                              # %cutal.exit102
                                        #   in Loop: Header=BB51_9 Depth=1
	movb	$0, 1(%rax)
	movslq	ReadFasta34m10_nuc.junban(,%r9,4), %rax
	leaq	(%rax,%rax,4), %rdx
	shlq	$4, %rdx
	addq	72(%rsp), %rdx          # 8-byte Folded Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	putlocalhom
	movq	56(%rsp), %r15          # 8-byte Reload
	leaq	100(%rsp), %r12
	.p2align	4, 0x90
.LBB51_9:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB51_13 Depth 2
                                        #     Child Loop BB51_17 Depth 2
                                        #     Child Loop BB51_19 Depth 2
                                        #     Child Loop BB51_22 Depth 2
                                        #     Child Loop BB51_25 Depth 2
                                        #     Child Loop BB51_28 Depth 2
                                        #     Child Loop BB51_32 Depth 2
                                        #     Child Loop BB51_35 Depth 2
                                        #     Child Loop BB51_38 Depth 2
                                        #     Child Loop BB51_41 Depth 2
                                        #     Child Loop BB51_45 Depth 2
                                        #     Child Loop BB51_48 Depth 2
	movl	$.L.str.47, %edi
	movl	$14, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB51_12
# BB#10:                                #   in Loop: Header=BB51_9 Depth=1
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	fgets
	movq	%r14, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB51_9
	jmp	.LBB51_11
.LBB51_12:                              #   in Loop: Header=BB51_9 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	110(%rsp), %rdi
	callq	strtol
	leaq	1(%r15), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	%eax, ReadFasta34m10_nuc.junban(,%r15,4)
	.p2align	4, 0x90
.LBB51_13:                              #   Parent Loop BB51_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB51_16
# BB#14:                                #   in Loop: Header=BB51_13 Depth=2
	movl	$.L.str.48, %edi
	movl	$9, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB51_16
# BB#15:                                #   in Loop: Header=BB51_13 Depth=2
	movl	$.L.str.49, %edi
	movl	$13, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB51_13
.LBB51_16:                              #   in Loop: Header=BB51_9 Depth=1
	movl	$58, %esi
	movq	%r13, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, 20(%rsp)
	.p2align	4, 0x90
.LBB51_17:                              #   Parent Loop BB51_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB51_19
# BB#18:                                #   in Loop: Header=BB51_17 Depth=2
	movl	$.L.str.51, %edi
	movl	$9, %edx
	movq	%r12, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB51_17
	.p2align	4, 0x90
.LBB51_19:                              # %.preheader
                                        #   Parent Loop BB51_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB51_21
# BB#20:                                #   in Loop: Header=BB51_19 Depth=2
	movl	$.L.str.52, %edi
	movl	$7, %edx
	movq	%r12, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB51_19
.LBB51_21:                              #   in Loop: Header=BB51_9 Depth=1
	movl	$58, %esi
	movq	%r13, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	decl	%eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB51_22:                              #   Parent Loop BB51_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB51_24
# BB#23:                                #   in Loop: Header=BB51_22 Depth=2
	movl	$.L.str.53, %edi
	movl	$6, %edx
	movq	%r12, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB51_22
.LBB51_24:                              #   in Loop: Header=BB51_9 Depth=1
	movq	%r15, 64(%rsp)          # 8-byte Spill
	movl	$58, %esi
	movq	%r13, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	decl	%eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB51_25:                              #   Parent Loop BB51_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB51_27
# BB#26:                                #   in Loop: Header=BB51_25 Depth=2
	movl	$.L.str.54, %edi
	movl	$15, %edx
	movq	%r12, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB51_25
.LBB51_27:                              #   in Loop: Header=BB51_9 Depth=1
	movl	$58, %esi
	movq	%r13, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r15
	decl	%r15d
	movl	$ReadFasta34m10_nuc.qal, %ebx
	jmp	.LBB51_28
.LBB51_53:                              #   in Loop: Header=BB51_28 Depth=2
	movb	%bpl, (%rbx)
	incq	%rbx
	.p2align	4, 0x90
.LBB51_28:                              #   Parent Loop BB51_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdi
	callq	fgetc
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB51_31
# BB#29:                                #   in Loop: Header=BB51_28 Depth=2
	cmpl	$62, %ebp
	je	.LBB51_30
# BB#51:                                #   in Loop: Header=BB51_28 Depth=2
	callq	__ctype_b_loc
	cmpl	$45, %ebp
	je	.LBB51_53
# BB#52:                                #   in Loop: Header=BB51_28 Depth=2
	movq	(%rax), %rax
	movslq	%ebp, %rcx
	movzwl	(%rax,%rcx,2), %eax
	andl	$1024, %eax             # imm = 0x400
	testw	%ax, %ax
	je	.LBB51_28
	jmp	.LBB51_53
.LBB51_30:                              #   in Loop: Header=BB51_9 Depth=1
	movl	$62, %edi
	movq	%r14, %rsi
	callq	ungetc
.LBB51_31:                              # %.loopexit103
                                        #   in Loop: Header=BB51_9 Depth=1
	movb	$0, (%rbx)
	.p2align	4, 0x90
.LBB51_32:                              #   Parent Loop BB51_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB51_34
# BB#33:                                #   in Loop: Header=BB51_32 Depth=2
	movl	$.L.str.52, %edi
	movl	$7, %edx
	movq	%r12, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB51_32
.LBB51_34:                              #   in Loop: Header=BB51_9 Depth=1
	movl	$58, %esi
	movq	%r13, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	decl	%eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB51_35:                              #   Parent Loop BB51_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB51_37
# BB#36:                                #   in Loop: Header=BB51_35 Depth=2
	movl	$.L.str.53, %edi
	movl	$6, %edx
	movq	%r12, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB51_35
.LBB51_37:                              #   in Loop: Header=BB51_9 Depth=1
	movl	$58, %esi
	movq	%r13, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	decl	%eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB51_38:                              #   Parent Loop BB51_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB51_40
# BB#39:                                #   in Loop: Header=BB51_38 Depth=2
	movl	$.L.str.54, %edi
	movl	$15, %edx
	movq	%r12, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB51_38
.LBB51_40:                              #   in Loop: Header=BB51_9 Depth=1
	movl	$58, %esi
	movq	%r13, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r12
	decl	%r12d
	movl	$ReadFasta34m10_nuc.tal, %ebx
	jmp	.LBB51_41
.LBB51_56:                              #   in Loop: Header=BB51_41 Depth=2
	movb	%bpl, (%rbx)
	incq	%rbx
	.p2align	4, 0x90
.LBB51_41:                              #   Parent Loop BB51_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdi
	callq	fgetc
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB51_44
# BB#42:                                #   in Loop: Header=BB51_41 Depth=2
	cmpl	$62, %ebp
	je	.LBB51_43
# BB#54:                                #   in Loop: Header=BB51_41 Depth=2
	callq	__ctype_b_loc
	cmpl	$45, %ebp
	je	.LBB51_56
# BB#55:                                #   in Loop: Header=BB51_41 Depth=2
	movq	(%rax), %rax
	movslq	%ebp, %rcx
	movzwl	(%rax,%rcx,2), %eax
	andl	$1024, %eax             # imm = 0x400
	testw	%ax, %ax
	je	.LBB51_41
	jmp	.LBB51_56
.LBB51_43:                              #   in Loop: Header=BB51_9 Depth=1
	movl	$62, %edi
	movq	%r14, %rsi
	callq	ungetc
.LBB51_44:                              # %.loopexit
                                        #   in Loop: Header=BB51_9 Depth=1
	movb	$0, (%rbx)
	movl	$ReadFasta34m10_nuc.qal, %eax
	xorl	%edi, %edi
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB51_45:                              #   Parent Loop BB51_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ecx, %r15d
	cmoveq	%rax, %rdi
	cmpl	%esi, %r15d
	je	.LBB51_47
# BB#46:                                #   in Loop: Header=BB51_45 Depth=2
	movzbl	(%rax), %ebx
	xorl	%edx, %edx
	cmpb	$45, %bl
	setne	%dl
	addl	%edx, %r15d
	incq	%rax
	testb	%bl, %bl
	jne	.LBB51_45
.LBB51_47:                              # %cutal.exit
                                        #   in Loop: Header=BB51_9 Depth=1
	movb	$0, 1(%rax)
	movl	$ReadFasta34m10_nuc.tal, %eax
	xorl	%esi, %esi
	movq	64(%rsp), %r9           # 8-byte Reload
	.p2align	4, 0x90
.LBB51_48:                              #   Parent Loop BB51_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%r8d, %r12d
	cmoveq	%rax, %rsi
	cmpl	%ebp, %r12d
	je	.LBB51_50
# BB#49:                                #   in Loop: Header=BB51_48 Depth=2
	movzbl	(%rax), %ebx
	xorl	%edx, %edx
	cmpb	$45, %bl
	setne	%dl
	addl	%edx, %r12d
	incq	%rax
	testb	%bl, %bl
	jne	.LBB51_48
	jmp	.LBB51_50
.LBB51_11:                              # %.loopexit106
	movl	%r15d, %eax
	addq	$360, %rsp              # imm = 0x168
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end51:
	.size	ReadFasta34m10_nuc, .Lfunc_end51-ReadFasta34m10_nuc
	.cfi_endproc

	.globl	ReadFasta34m10
	.p2align	4, 0x90
	.type	ReadFasta34m10,@function
ReadFasta34m10:                         # @ReadFasta34m10
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi565:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi566:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi567:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi568:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi569:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi570:
	.cfi_def_cfa_offset 56
	subq	$360, %rsp              # imm = 0x168
.Lcfi571:
	.cfi_def_cfa_offset 416
.Lcfi572:
	.cfi_offset %rbx, -56
.Lcfi573:
	.cfi_offset %r12, -48
.Lcfi574:
	.cfi_offset %r13, -40
.Lcfi575:
	.cfi_offset %r14, -32
.Lcfi576:
	.cfi_offset %r15, -24
.Lcfi577:
	.cfi_offset %rbp, -16
	movq	%r8, 72(%rsp)           # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %r12
	leaq	96(%rsp), %rbp
	leaq	80(%rsp), %r13
	leaq	88(%rsp), %r14
	xorl	%ebx, %ebx
	jmp	.LBB52_1
.LBB52_3:                               #   in Loop: Header=BB52_1 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	108(%rsp), %rdi
	callq	strtol
	movl	%eax, ReadFasta34m10.junban(,%rbx,4)
	movl	$41, %esi
	movq	%rbp, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	leaq	20(%rsp), %rdx
	movq	%r13, %rcx
	movq	%r14, %r8
	callq	sscanf
	cvtsi2sdl	20(%rsp), %xmm0
	movslq	ReadFasta34m10.junban(,%rbx,4), %rax
	movsd	%xmm0, (%r15,%rax,8)
	incq	%rbx
	.p2align	4, 0x90
.LBB52_1:                               # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB52_5
# BB#2:                                 #   in Loop: Header=BB52_1 Depth=1
	movl	$255, %esi
	movq	%rbp, %rdi
	movq	%r12, %rdx
	callq	fgets
	movl	$.L.str.30, %edi
	movl	$12, %edx
	movq	%rbp, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB52_3
# BB#4:                                 #   in Loop: Header=BB52_1 Depth=1
	movl	$.L.str.47, %edi
	movl	$14, %edx
	movq	%rbp, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB52_1
.LBB52_5:
	testl	%ebx, %ebx
	je	.LBB52_6
# BB#8:                                 # %.preheader105
	leaq	100(%rsp), %r15
	xorl	%ebx, %ebx
	leaq	96(%rsp), %r13
	jmp	.LBB52_9
.LBB52_50:                              # %cutal.exit102
                                        #   in Loop: Header=BB52_9 Depth=1
	movb	$0, 1(%rax)
	movslq	ReadFasta34m10.junban(,%r9,4), %rax
	leaq	(%rax,%rax,4), %rdx
	shlq	$4, %rdx
	addq	72(%rsp), %rdx          # 8-byte Folded Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	putlocalhom
	movq	56(%rsp), %rbx          # 8-byte Reload
	leaq	100(%rsp), %r15
	.p2align	4, 0x90
.LBB52_9:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB52_12 Depth 2
                                        #     Child Loop BB52_16 Depth 2
                                        #     Child Loop BB52_18 Depth 2
                                        #     Child Loop BB52_21 Depth 2
                                        #     Child Loop BB52_24 Depth 2
                                        #     Child Loop BB52_27 Depth 2
                                        #     Child Loop BB52_31 Depth 2
                                        #     Child Loop BB52_34 Depth 2
                                        #     Child Loop BB52_37 Depth 2
                                        #     Child Loop BB52_40 Depth 2
                                        #       Child Loop BB52_41 Depth 3
                                        #     Child Loop BB52_45 Depth 2
                                        #     Child Loop BB52_48 Depth 2
	movl	$.L.str.47, %edi
	movl	$14, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB52_11
# BB#10:                                #   in Loop: Header=BB52_9 Depth=1
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	callq	fgets
	movq	%r12, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB52_9
	jmp	.LBB52_7
.LBB52_11:                              #   in Loop: Header=BB52_9 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	110(%rsp), %rdi
	callq	strtol
	leaq	1(%rbx), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	%eax, ReadFasta34m10.junban(,%rbx,4)
	.p2align	4, 0x90
.LBB52_12:                              #   Parent Loop BB52_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB52_15
# BB#13:                                #   in Loop: Header=BB52_12 Depth=2
	movl	$.L.str.48, %edi
	movl	$9, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB52_15
# BB#14:                                #   in Loop: Header=BB52_12 Depth=2
	movl	$.L.str.49, %edi
	movl	$13, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB52_12
.LBB52_15:                              #   in Loop: Header=BB52_9 Depth=1
	movl	$58, %esi
	movq	%r13, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, 20(%rsp)
	.p2align	4, 0x90
.LBB52_16:                              #   Parent Loop BB52_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB52_18
# BB#17:                                #   in Loop: Header=BB52_16 Depth=2
	movl	$.L.str.51, %edi
	movl	$9, %edx
	movq	%r15, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB52_16
	.p2align	4, 0x90
.LBB52_18:                              # %.preheader
                                        #   Parent Loop BB52_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB52_20
# BB#19:                                #   in Loop: Header=BB52_18 Depth=2
	movl	$.L.str.52, %edi
	movl	$7, %edx
	movq	%r15, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB52_18
.LBB52_20:                              #   in Loop: Header=BB52_9 Depth=1
	movl	$58, %esi
	movq	%r13, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	decl	%eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB52_21:                              #   Parent Loop BB52_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB52_23
# BB#22:                                #   in Loop: Header=BB52_21 Depth=2
	movl	$.L.str.53, %edi
	movl	$6, %edx
	movq	%r15, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB52_21
.LBB52_23:                              #   in Loop: Header=BB52_9 Depth=1
	movl	$58, %esi
	movq	%r13, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	decl	%eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB52_24:                              #   Parent Loop BB52_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB52_26
# BB#25:                                #   in Loop: Header=BB52_24 Depth=2
	movl	$.L.str.54, %edi
	movl	$15, %edx
	movq	%r15, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB52_24
.LBB52_26:                              #   in Loop: Header=BB52_9 Depth=1
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	movl	$58, %esi
	movq	%r13, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rbp
	decl	%ebp
	movl	$ReadFasta34m10.qal, %r14d
	jmp	.LBB52_27
.LBB52_53:                              #   in Loop: Header=BB52_27 Depth=2
	movb	%bl, (%r14)
	incq	%r14
	.p2align	4, 0x90
.LBB52_27:                              #   Parent Loop BB52_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r12, %rdi
	callq	fgetc
	movl	%eax, %ebx
	testl	%ebx, %ebx
	je	.LBB52_30
# BB#28:                                #   in Loop: Header=BB52_27 Depth=2
	cmpl	$62, %ebx
	je	.LBB52_29
# BB#51:                                #   in Loop: Header=BB52_27 Depth=2
	callq	__ctype_b_loc
	cmpl	$45, %ebx
	je	.LBB52_53
# BB#52:                                #   in Loop: Header=BB52_27 Depth=2
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movzwl	(%rax,%rcx,2), %eax
	andl	$1024, %eax             # imm = 0x400
	testw	%ax, %ax
	je	.LBB52_27
	jmp	.LBB52_53
.LBB52_29:                              #   in Loop: Header=BB52_9 Depth=1
	movl	$62, %edi
	movq	%r12, %rsi
	callq	ungetc
.LBB52_30:                              # %.loopexit103
                                        #   in Loop: Header=BB52_9 Depth=1
	movb	$0, (%r14)
	.p2align	4, 0x90
.LBB52_31:                              #   Parent Loop BB52_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB52_33
# BB#32:                                #   in Loop: Header=BB52_31 Depth=2
	movl	$.L.str.52, %edi
	movl	$7, %edx
	movq	%r15, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB52_31
.LBB52_33:                              #   in Loop: Header=BB52_9 Depth=1
	movl	$58, %esi
	movq	%r13, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	decl	%eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB52_34:                              #   Parent Loop BB52_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB52_36
# BB#35:                                #   in Loop: Header=BB52_34 Depth=2
	movl	$.L.str.53, %edi
	movl	$6, %edx
	movq	%r15, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB52_34
.LBB52_36:                              #   in Loop: Header=BB52_9 Depth=1
	movl	$58, %esi
	movq	%r13, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r14
	decl	%r14d
	.p2align	4, 0x90
.LBB52_37:                              #   Parent Loop BB52_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB52_39
# BB#38:                                #   in Loop: Header=BB52_37 Depth=2
	movl	$.L.str.54, %edi
	movl	$15, %edx
	movq	%r15, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB52_37
.LBB52_39:                              #   in Loop: Header=BB52_9 Depth=1
	movl	$58, %esi
	movq	%r13, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r15
	decl	%r15d
	movl	$ReadFasta34m10.tal, %eax
	jmp	.LBB52_40
	.p2align	4, 0x90
.LBB52_56:                              #   in Loop: Header=BB52_40 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movb	%bl, (%rax)
	incq	%rax
.LBB52_40:                              # %.outer
                                        #   Parent Loop BB52_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB52_41 Depth 3
	movq	%rax, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB52_41:                              #   Parent Loop BB52_9 Depth=1
                                        #     Parent Loop BB52_40 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r12, %rdi
	callq	fgetc
	movl	%eax, %ebx
	testl	%ebx, %ebx
	je	.LBB52_44
# BB#42:                                #   in Loop: Header=BB52_41 Depth=3
	cmpl	$62, %ebx
	je	.LBB52_43
# BB#54:                                #   in Loop: Header=BB52_41 Depth=3
	callq	__ctype_b_loc
	cmpl	$45, %ebx
	je	.LBB52_56
# BB#55:                                #   in Loop: Header=BB52_41 Depth=3
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	movzwl	(%rax,%rcx,2), %eax
	andl	$1024, %eax             # imm = 0x400
	testw	%ax, %ax
	je	.LBB52_41
	jmp	.LBB52_56
.LBB52_43:                              #   in Loop: Header=BB52_9 Depth=1
	movl	$62, %edi
	movq	%r12, %rsi
	callq	ungetc
.LBB52_44:                              # %.loopexit
                                        #   in Loop: Header=BB52_9 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movb	$0, (%rax)
	movl	$ReadFasta34m10.qal, %eax
	xorl	%edi, %edi
	movq	64(%rsp), %r9           # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB52_45:                              #   Parent Loop BB52_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ecx, %ebp
	cmoveq	%rax, %rdi
	cmpl	%esi, %ebp
	je	.LBB52_47
# BB#46:                                #   in Loop: Header=BB52_45 Depth=2
	movzbl	(%rax), %ebx
	xorl	%edx, %edx
	cmpb	$45, %bl
	setne	%dl
	addl	%edx, %ebp
	incq	%rax
	testb	%bl, %bl
	jne	.LBB52_45
.LBB52_47:                              # %cutal.exit
                                        #   in Loop: Header=BB52_9 Depth=1
	movb	$0, 1(%rax)
	movl	$ReadFasta34m10.tal, %eax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB52_48:                              #   Parent Loop BB52_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%r8d, %r15d
	cmoveq	%rax, %rsi
	cmpl	%r14d, %r15d
	je	.LBB52_50
# BB#49:                                #   in Loop: Header=BB52_48 Depth=2
	movzbl	(%rax), %ebx
	xorl	%edx, %edx
	cmpb	$45, %bl
	setne	%dl
	addl	%edx, %r15d
	incq	%rax
	testb	%bl, %bl
	jne	.LBB52_48
	jmp	.LBB52_50
.LBB52_6:
	movl	$-1, %ebx
.LBB52_7:                               # %.loopexit106
	movl	%ebx, %eax
	addq	$360, %rsp              # imm = 0x168
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end52:
	.size	ReadFasta34m10, .Lfunc_end52-ReadFasta34m10
	.cfi_endproc

	.globl	ReadFasta34m10_scoreonly_nucbk
	.p2align	4, 0x90
	.type	ReadFasta34m10_scoreonly_nucbk,@function
ReadFasta34m10_scoreonly_nucbk:         # @ReadFasta34m10_scoreonly_nucbk
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi578:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi579:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi580:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi581:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi582:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi583:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi584:
	.cfi_def_cfa_offset 352
.Lcfi585:
	.cfi_offset %rbx, -56
.Lcfi586:
	.cfi_offset %r12, -48
.Lcfi587:
	.cfi_offset %r13, -40
.Lcfi588:
	.cfi_offset %r14, -32
.Lcfi589:
	.cfi_offset %r15, -24
.Lcfi590:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	feof
	xorl	%ebp, %ebp
	testl	%eax, %eax
	jne	.LBB53_7
# BB#1:                                 # %.lr.ph.lr.ph
	leaq	45(%rsp), %r13
	leaq	32(%rsp), %r12
	leaq	12(%rsp), %r15
	xorl	%ebp, %ebp
	jmp	.LBB53_2
.LBB53_4:                               # %.outer
                                        #   in Loop: Header=BB53_2 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%rax, %r13
	movl	$93, %esi
	movq	%r12, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	leaq	16(%rsp), %rcx
	leaq	24(%rsp), %r8
	callq	sscanf
	cvtsi2sdl	12(%rsp), %xmm0
	movslq	%r13d, %rax
	leaq	45(%rsp), %r13
	addsd	(%r14,%rax,8), %xmm0
	movsd	%xmm0, (%r14,%rax,8)
	incl	%ebp
	jmp	.LBB53_6
	.p2align	4, 0x90
.LBB53_2:                               # =>This Inner Loop Header: Depth=1
	movl	$255, %esi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$.L.str.55, %edi
	movl	$13, %edx
	movq	%r12, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB53_3
# BB#5:                                 #   in Loop: Header=BB53_2 Depth=1
	movl	$.L.str.56, %edi
	movl	$6, %edx
	movq	%r12, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB53_6
	jmp	.LBB53_7
	.p2align	4, 0x90
.LBB53_3:                               #   in Loop: Header=BB53_2 Depth=1
	movl	$114, %esi
	movq	%r12, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB53_4
.LBB53_6:                               # %.backedge
                                        #   in Loop: Header=BB53_2 Depth=1
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB53_2
.LBB53_7:                               # %.outer._crit_edge
	cmpl	$1, %ebp
	sbbl	%eax, %eax
	orl	%ebp, %eax
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end53:
	.size	ReadFasta34m10_scoreonly_nucbk, .Lfunc_end53-ReadFasta34m10_scoreonly_nucbk
	.cfi_endproc

	.globl	ReadFasta34m10_scoreonly_nuc
	.p2align	4, 0x90
	.type	ReadFasta34m10_scoreonly_nuc,@function
ReadFasta34m10_scoreonly_nuc:           # @ReadFasta34m10_scoreonly_nuc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi591:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi592:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi593:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi594:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi595:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi596:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi597:
	.cfi_def_cfa_offset 368
.Lcfi598:
	.cfi_offset %rbx, -56
.Lcfi599:
	.cfi_offset %r12, -48
.Lcfi600:
	.cfi_offset %r13, -40
.Lcfi601:
	.cfi_offset %r14, -32
.Lcfi602:
	.cfi_offset %r15, -24
.Lcfi603:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	%r12d, %edi
	callq	AllocateIntVec
	movq	%rax, %r14
	testl	%r12d, %r12d
	jle	.LBB54_2
# BB#1:                                 # %.lr.ph51.preheader
	leal	-1(%r12), %ebp
	leaq	4(,%rbp,4), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	memset
	leaq	8(,%rbp,8), %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	memset
.LBB54_2:                               # %.preheader40
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	feof
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jne	.LBB54_19
# BB#3:                                 # %.lr.ph43.lr.ph
	leal	-1(%r12), %eax
	leaq	4(,%rax,4), %rbp
	leaq	48(%rsp), %r13
	xorl	%r15d, %r15d
.LBB54_9:                               # %.lr.ph43
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB54_4 Depth 2
                                        #     Child Loop BB54_10 Depth 2
	testl	%r12d, %r12d
	jle	.LBB54_4
	.p2align	4, 0x90
.LBB54_10:                              # %.lr.ph43.split.us
                                        #   Parent Loop BB54_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$.L.str.55, %edi
	movl	$13, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB54_14
# BB#11:                                #   in Loop: Header=BB54_10 Depth=2
	movl	$.L.str.57, %edi
	movl	$3, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB54_12
# BB#13:                                #   in Loop: Header=BB54_10 Depth=2
	movl	$.L.str.56, %edi
	movl	$6, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB54_15
	jmp	.LBB54_19
	.p2align	4, 0x90
.LBB54_14:                              #   in Loop: Header=BB54_10 Depth=2
	movl	$114, %esi
	movq	%r13, %rdi
	callq	strchr
	testq	%rax, %rax
	jne	.LBB54_15
	jmp	.LBB54_6
	.p2align	4, 0x90
.LBB54_12:                              # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB54_10 Depth=2
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rbp, %rdx
	callq	memset
.LBB54_15:                              # %.backedge.us
                                        #   in Loop: Header=BB54_10 Depth=2
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB54_10
	jmp	.LBB54_19
	.p2align	4, 0x90
.LBB54_4:                               # %.lr.ph43.split
                                        #   Parent Loop BB54_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$.L.str.55, %edi
	movl	$13, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB54_5
# BB#16:                                #   in Loop: Header=BB54_4 Depth=2
	movl	$.L.str.57, %edi
	movl	$3, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB54_18
# BB#17:                                #   in Loop: Header=BB54_4 Depth=2
	movl	$.L.str.56, %edi
	movl	$6, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB54_18
	jmp	.LBB54_19
	.p2align	4, 0x90
.LBB54_5:                               #   in Loop: Header=BB54_4 Depth=2
	movl	$114, %esi
	movq	%r13, %rdi
	callq	strchr
	testq	%rax, %rax
	je	.LBB54_6
.LBB54_18:                              # %.backedge
                                        #   in Loop: Header=BB54_4 Depth=2
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB54_4
	jmp	.LBB54_19
.LBB54_6:                               # %.us-lcssa
                                        #   in Loop: Header=BB54_9 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	61(%rsp), %rdi
	callq	strtol
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$93, %esi
	movq	%r13, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	leaq	12(%rsp), %rdx
	leaq	32(%rsp), %rcx
	leaq	40(%rsp), %r8
	callq	sscanf
	movslq	16(%rsp), %rax          # 4-byte Folded Reload
	cmpl	$0, (%r14,%rax,4)
	jne	.LBB54_8
# BB#7:                                 #   in Loop: Header=BB54_9 Depth=1
	cvtsi2sdl	12(%rsp), %xmm0
	movq	24(%rsp), %rcx          # 8-byte Reload
	addsd	(%rcx,%rax,8), %xmm0
	movsd	%xmm0, (%rcx,%rax,8)
	movl	$1, (%r14,%rax,4)
.LBB54_8:                               # %.outer
                                        #   in Loop: Header=BB54_9 Depth=1
	incl	%r15d
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB54_9
.LBB54_19:                              # %.outer._crit_edge
	movq	%r14, %rdi
	callq	free
	cmpl	$1, %r15d
	sbbl	%eax, %eax
	orl	%r15d, %eax
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end54:
	.size	ReadFasta34m10_scoreonly_nuc, .Lfunc_end54-ReadFasta34m10_scoreonly_nuc
	.cfi_endproc

	.globl	ReadFasta34m10_scoreonly
	.p2align	4, 0x90
	.type	ReadFasta34m10_scoreonly,@function
ReadFasta34m10_scoreonly:               # @ReadFasta34m10_scoreonly
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi604:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi605:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi606:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi607:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi608:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi609:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi610:
	.cfi_def_cfa_offset 368
.Lcfi611:
	.cfi_offset %rbx, -56
.Lcfi612:
	.cfi_offset %r12, -48
.Lcfi613:
	.cfi_offset %r13, -40
.Lcfi614:
	.cfi_offset %r14, -32
.Lcfi615:
	.cfi_offset %r15, -24
.Lcfi616:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	%r12d, %edi
	callq	AllocateIntVec
	movq	%rax, %r14
	testl	%r12d, %r12d
	jle	.LBB55_2
# BB#1:                                 # %.lr.ph51.preheader
	leal	-1(%r12), %ebp
	leaq	4(,%rbp,4), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	memset
	leaq	8(,%rbp,8), %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	memset
.LBB55_2:                               # %.preheader40
	movq	%rbx, %rdi
	callq	feof
	xorl	%ebp, %ebp
	testl	%eax, %eax
	je	.LBB55_3
.LBB55_17:                              # %.outer._crit_edge
	movq	%r14, %rdi
	callq	free
	cmpl	$1, %ebp
	sbbl	%eax, %eax
	orl	%ebp, %eax
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB55_3:                               # %.lr.ph43.lr.ph
	leal	-1(%r12), %eax
	leaq	4(,%rax,4), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	48(%rsp), %r13
	xorl	%ebp, %ebp
.LBB55_8:                               # %.lr.ph43
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB55_4 Depth 2
                                        #     Child Loop BB55_9 Depth 2
	testl	%r12d, %r12d
	jle	.LBB55_4
	.p2align	4, 0x90
.LBB55_9:                               # %.lr.ph43.split.us
                                        #   Parent Loop BB55_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$.L.str.55, %edi
	movl	$13, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB55_5
# BB#10:                                #   in Loop: Header=BB55_9 Depth=2
	movl	$.L.str.57, %edi
	movl	$3, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB55_11
# BB#12:                                #   in Loop: Header=BB55_9 Depth=2
	movl	$.L.str.56, %edi
	movl	$6, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB55_13
	jmp	.LBB55_17
	.p2align	4, 0x90
.LBB55_11:                              # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB55_9 Depth=2
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	24(%rsp), %rdx          # 8-byte Reload
	callq	memset
.LBB55_13:                              # %.backedge.us
                                        #   in Loop: Header=BB55_9 Depth=2
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB55_9
	jmp	.LBB55_17
	.p2align	4, 0x90
.LBB55_4:                               # %.lr.ph43.split
                                        #   Parent Loop BB55_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$.L.str.55, %edi
	movl	$13, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB55_5
# BB#14:                                #   in Loop: Header=BB55_4 Depth=2
	movl	$.L.str.57, %edi
	movl	$3, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB55_16
# BB#15:                                #   in Loop: Header=BB55_4 Depth=2
	movl	$.L.str.56, %edi
	movl	$6, %edx
	movq	%r13, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB55_17
.LBB55_16:                              # %.backedge
                                        #   in Loop: Header=BB55_4 Depth=2
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB55_4
	jmp	.LBB55_17
	.p2align	4, 0x90
.LBB55_5:                               # %.us-lcssa
                                        #   in Loop: Header=BB55_8 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	61(%rsp), %rdi
	callq	strtol
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r15
	movl	$41, %esi
	movq	%r13, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	leaq	12(%rsp), %rdx
	leaq	32(%rsp), %rcx
	leaq	40(%rsp), %r8
	callq	sscanf
	movslq	%r15d, %rax
	movq	16(%rsp), %r15          # 8-byte Reload
	cmpl	$0, (%r14,%rax,4)
	jne	.LBB55_7
# BB#6:                                 #   in Loop: Header=BB55_8 Depth=1
	cvtsi2sdl	12(%rsp), %xmm0
	addsd	(%r15,%rax,8), %xmm0
	movsd	%xmm0, (%r15,%rax,8)
	movl	$1, (%r14,%rax,4)
.LBB55_7:                               # %.outer
                                        #   in Loop: Header=BB55_8 Depth=1
	incl	%ebp
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB55_8
	jmp	.LBB55_17
.Lfunc_end55:
	.size	ReadFasta34m10_scoreonly, .Lfunc_end55-ReadFasta34m10_scoreonly
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI56_0:
	.zero	16,255
	.text
	.globl	ReadFasta34
	.p2align	4, 0x90
	.type	ReadFasta34,@function
ReadFasta34:                            # @ReadFasta34
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi617:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi618:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi619:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi620:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi621:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi622:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi623:
	.cfi_def_cfa_offset 368
.Lcfi624:
	.cfi_offset %rbx, -56
.Lcfi625:
	.cfi_offset %r12, -48
.Lcfi626:
	.cfi_offset %r13, -40
.Lcfi627:
	.cfi_offset %r14, -32
.Lcfi628:
	.cfi_offset %r15, -24
.Lcfi629:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rsi, %r12
	movq	%rdi, %rbx
	leaq	48(%rsp), %rbp
	leaq	40(%rsp), %r13
	xorl	%r15d, %r15d
	jmp	.LBB56_1
.LBB56_3:                               #   in Loop: Header=BB56_1 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	60(%rsp), %rdi
	callq	strtol
	movl	%eax, ReadFasta34.junban(,%r15,4)
	movl	$41, %esi
	movq	%rbp, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	leaq	28(%rsp), %rdx
	leaq	32(%rsp), %rcx
	movq	%r13, %r8
	callq	sscanf
	cvtsi2sdl	28(%rsp), %xmm0
	movslq	ReadFasta34.junban(,%r15,4), %rax
	movsd	%xmm0, (%r12,%rax,8)
	incq	%r15
	.p2align	4, 0x90
.LBB56_1:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB56_5
# BB#2:                                 #   in Loop: Header=BB56_1 Depth=1
	movl	$255, %esi
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$.L.str.30, %edi
	movl	$12, %edx
	movq	%rbp, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB56_3
# BB#4:                                 #   in Loop: Header=BB56_1 Depth=1
	movl	$.L.str.47, %edi
	movl	$14, %edx
	movq	%rbp, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB56_1
.LBB56_5:
	testl	%r15d, %r15d
	je	.LBB56_6
# BB#7:                                 # %.preheader
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB56_8
# BB#9:                                 # %.lr.ph
	leaq	48(%rsp), %r12
	xorl	%ebp, %ebp
	jmp	.LBB56_10
.LBB56_11:                              #   in Loop: Header=BB56_10 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	62(%rsp), %rdi
	callq	strtol
	movslq	%ebp, %r13
	movl	%eax, ReadFasta34.junban(,%r13,4)
	leal	1(%r13), %ebp
	movl	$255, %esi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$.L.str.58, %esi
	movq	%r12, %rdi
	callq	strstr
	leaq	5(%rax), %rdi
	xorl	%esi, %esi
	callq	strtod
	movslq	ReadFasta34.junban(,%r13,4), %rax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	movsd	%xmm0, 40(%r14,%rax)
	movl	$255, %esi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$.L.str.59, %esi
	movq	%r12, %rdi
	callq	strstr
	movq	%rax, %r15
	addq	$13, %r15
	movl	$.L.str.31, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	leaq	8(%rsp), %rdx
	callq	sscanf
	movq	stderr(%rip), %rdi
	movl	8(%rsp), %ecx
	movl	$.L.str.60, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	callq	fprintf
	movl	$.L.str.61, %esi
	movq	%r12, %rdi
	callq	strstr
	leaq	8(%rax), %rdi
	movl	$.L.str.62, %esi
	xorl	%eax, %eax
	leaq	24(%rsp), %rdx
	leaq	20(%rsp), %rcx
	leaq	16(%rsp), %r8
	leaq	12(%rsp), %r9
	callq	sscanf
	movl	8(%rsp), %eax
	movslq	ReadFasta34.junban(,%r13,4), %rcx
	leaq	(%rcx,%rcx,4), %rcx
	shlq	$4, %rcx
	movl	%eax, 48(%r14,%rcx)
	movd	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movd	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movd	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movd	24(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	paddd	.LCPI56_0, %xmm2
	movdqu	%xmm2, 24(%r14,%rcx)
	jmp	.LBB56_12
	.p2align	4, 0x90
.LBB56_10:                              # =>This Inner Loop Header: Depth=1
	movl	$.L.str.47, %edi
	movl	$14, %edx
	movq	%r12, %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB56_11
.LBB56_12:                              #   in Loop: Header=BB56_10 Depth=1
	movl	$255, %esi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB56_10
.LBB56_13:                              # %._crit_edge
	movq	stderr(%rip), %rdi
	movl	$.L.str.63, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	fprintf
	jmp	.LBB56_14
.LBB56_6:
	movl	$-1, %ebp
.LBB56_14:
	movl	%ebp, %eax
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB56_8:
	xorl	%ebp, %ebp
	jmp	.LBB56_13
.Lfunc_end56:
	.size	ReadFasta34, .Lfunc_end56-ReadFasta34
	.cfi_endproc

	.globl	ReadFasta3
	.p2align	4, 0x90
	.type	ReadFasta3,@function
ReadFasta3:                             # @ReadFasta3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi630:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi631:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi632:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi633:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi634:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi635:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi636:
	.cfi_def_cfa_offset 352
.Lcfi637:
	.cfi_offset %rbx, -56
.Lcfi638:
	.cfi_offset %r12, -48
.Lcfi639:
	.cfi_offset %r13, -40
.Lcfi640:
	.cfi_offset %r14, -32
.Lcfi641:
	.cfi_offset %r15, -24
.Lcfi642:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	32(%rsp), %r12
	leaq	12(%rsp), %r15
	leaq	24(%rsp), %r13
	jmp	.LBB57_1
.LBB57_3:                               #   in Loop: Header=BB57_1 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	44(%rsp), %rdi
	callq	strtol
	movq	%rax, %rbp
	movl	$41, %esi
	movq	%r12, %rdi
	callq	strchr
	leaq	1(%rax), %rdi
	movl	$.L.str.64, %esi
	xorl	%eax, %eax
	leaq	20(%rsp), %rdx
	leaq	16(%rsp), %rcx
	movq	%r15, %r8
	movq	%r13, %r9
	callq	sscanf
	cvtsi2sdl	12(%rsp), %xmm0
	movslq	%ebp, %rax
	movsd	%xmm0, (%r14,%rax,8)
	.p2align	4, 0x90
.LBB57_1:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB57_4
# BB#2:                                 #   in Loop: Header=BB57_1 Depth=1
	movl	$255, %esi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$.L.str.30, %edi
	movl	$12, %edx
	movq	%r12, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB57_1
	jmp	.LBB57_3
.LBB57_4:
	xorl	%eax, %eax
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end57:
	.size	ReadFasta3, .Lfunc_end57-ReadFasta3
	.cfi_endproc

	.globl	ReadFasta
	.p2align	4, 0x90
	.type	ReadFasta,@function
ReadFasta:                              # @ReadFasta
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi643:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi644:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi645:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi646:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi647:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi648:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi649:
	.cfi_def_cfa_offset 352
.Lcfi650:
	.cfi_offset %rbx, -56
.Lcfi651:
	.cfi_offset %r12, -48
.Lcfi652:
	.cfi_offset %r13, -40
.Lcfi653:
	.cfi_offset %r14, -32
.Lcfi654:
	.cfi_offset %r15, -24
.Lcfi655:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %rbx
	testl	%r14d, %r14d
	jle	.LBB58_2
# BB#1:                                 # %.lr.ph.preheader
	leal	-1(%r14), %eax
	leaq	8(,%rax,8), %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	memset
.LBB58_2:                               # %.preheader
	testl	%r14d, %r14d
	jle	.LBB58_7
# BB#3:                                 # %.outer.split.us.preheader.preheader
	leaq	44(%rsp), %r13
	leaq	32(%rsp), %rbp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB58_4:                               # %.outer.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB58_8
# BB#5:                                 #   in Loop: Header=BB58_4 Depth=1
	movl	$255, %esi
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$.L.str.30, %edi
	movl	$12, %edx
	movq	%rbp, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB58_4
# BB#6:                                 # %.us-lcssa.us
                                        #   in Loop: Header=BB58_4 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strtol
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	%r13, %r15
	movq	%rax, %r13
	movl	$.L.str.65, %esi
	xorl	%eax, %eax
	leaq	82(%rsp), %rdi
	leaq	28(%rsp), %rdx
	leaq	24(%rsp), %rcx
	leaq	12(%rsp), %r8
	callq	sscanf
	cvtsi2sdl	12(%rsp), %xmm0
	movslq	%r13d, %rax
	movq	%r15, %r13
	movq	16(%rsp), %r15          # 8-byte Reload
	movsd	%xmm0, (%r15,%rax,8)
	incl	%r12d
	cmpl	%r14d, %r12d
	jl	.LBB58_4
.LBB58_7:                               # %.outer.split
	movq	%rbx, %rdi
	callq	feof
.LBB58_8:                               # %.critedge
	xorl	%eax, %eax
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end58:
	.size	ReadFasta, .Lfunc_end58-ReadFasta
	.cfi_endproc

	.globl	ReadOpt
	.p2align	4, 0x90
	.type	ReadOpt,@function
ReadOpt:                                # @ReadOpt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi656:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi657:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi658:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi659:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi660:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi661:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi662:
	.cfi_def_cfa_offset 352
.Lcfi663:
	.cfi_offset %rbx, -56
.Lcfi664:
	.cfi_offset %r12, -48
.Lcfi665:
	.cfi_offset %r13, -40
.Lcfi666:
	.cfi_offset %r14, -32
.Lcfi667:
	.cfi_offset %r15, -24
.Lcfi668:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r15
	testl	%ebx, %ebx
	jle	.LBB59_6
# BB#1:                                 # %.lr.ph
	movl	$1, %r12d
	leaq	32(%rsp), %rbp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB59_2:                               # =>This Inner Loop Header: Depth=1
	movl	$255, %esi
	movq	%rbp, %rdi
	movq	%r15, %rdx
	callq	fgets
	movl	$.L.str.30, %edi
	movl	$12, %edx
	movq	%rbp, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB59_4
# BB#3:                                 #   in Loop: Header=BB59_2 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	44(%rsp), %rdi
	callq	strtol
	movq	%rax, %r13
	movl	$.L.str.65, %esi
	xorl	%eax, %eax
	leaq	82(%rsp), %rdi
	leaq	28(%rsp), %rdx
	leaq	24(%rsp), %rcx
	leaq	12(%rsp), %r8
	callq	sscanf
	movl	12(%rsp), %eax
	movslq	%r13d, %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%eax, (%rdx,%rcx,4)
	incl	%r14d
.LBB59_4:                               #   in Loop: Header=BB59_2 Depth=1
	cmpl	$9999999, %r12d         # imm = 0x98967F
	jg	.LBB59_6
# BB#5:                                 #   in Loop: Header=BB59_2 Depth=1
	incl	%r12d
	cmpl	%ebx, %r14d
	jl	.LBB59_2
.LBB59_6:                               # %.critedge
	xorl	%eax, %eax
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end59:
	.size	ReadOpt, .Lfunc_end59-ReadOpt
	.cfi_endproc

	.globl	ReadOpt2
	.p2align	4, 0x90
	.type	ReadOpt2,@function
ReadOpt2:                               # @ReadOpt2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi669:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi670:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi671:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi672:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi673:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi674:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi675:
	.cfi_def_cfa_offset 336
.Lcfi676:
	.cfi_offset %rbx, -56
.Lcfi677:
	.cfi_offset %r12, -48
.Lcfi678:
	.cfi_offset %r13, -40
.Lcfi679:
	.cfi_offset %r14, -32
.Lcfi680:
	.cfi_offset %r15, -24
.Lcfi681:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r15
	testl	%ebx, %ebx
	jle	.LBB60_6
# BB#1:                                 # %.lr.ph
	xorl	%r13d, %r13d
	movl	$1, %r14d
	leaq	16(%rsp), %rbp
	.p2align	4, 0x90
.LBB60_2:                               # =>This Inner Loop Header: Depth=1
	movl	$255, %esi
	movq	%rbp, %rdi
	movq	%r15, %rdx
	callq	fgets
	movl	$.L.str.30, %edi
	movl	$12, %edx
	movq	%rbp, %rsi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB60_4
# BB#3:                                 #   in Loop: Header=BB60_2 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	28(%rsp), %rdi
	callq	strtol
	movq	%rax, %r12
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	81(%rsp), %rdi
	callq	strtol
	movslq	%r12d, %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%eax, (%rdx,%rcx,4)
	incl	%r13d
.LBB60_4:                               #   in Loop: Header=BB60_2 Depth=1
	cmpl	$9999999, %r14d         # imm = 0x98967F
	jg	.LBB60_6
# BB#5:                                 #   in Loop: Header=BB60_2 Depth=1
	incl	%r14d
	cmpl	%ebx, %r13d
	jl	.LBB60_2
.LBB60_6:                               # %.critedge
	xorl	%eax, %eax
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end60:
	.size	ReadOpt2, .Lfunc_end60-ReadOpt2
	.cfi_endproc

	.globl	writePre
	.p2align	4, 0x90
	.type	writePre,@function
writePre:                               # @writePre
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi682:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi683:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi684:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi685:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi686:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi687:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi688:
	.cfi_def_cfa_offset 80
.Lcfi689:
	.cfi_offset %rbx, -56
.Lcfi690:
	.cfi_offset %r12, -48
.Lcfi691:
	.cfi_offset %r13, -40
.Lcfi692:
	.cfi_offset %r14, -32
.Lcfi693:
	.cfi_offset %r15, -24
.Lcfi694:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	%edi, %ebp
	testl	%r8d, %r8d
	je	.LBB61_7
# BB#1:
	movq	prep_g(%rip), %rdi
	callq	rewind
	testl	%ebp, %ebp
	jle	.LBB61_7
# BB#2:                                 # %.lr.ph23.preheader.i
	movq	prep_g(%rip), %r14
	movl	%ebp, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB61_3:                               # %.lr.ph23.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB61_5 Depth 2
	movq	(%r15,%r13,8), %rdi
	callq	strlen
	movq	%rax, %r12
	movq	%r13, %rax
	shlq	$8, %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	1(%rcx,%rax), %rdx
	movl	$.L.str.23, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	testl	%r12d, %r12d
	jle	.LBB61_6
# BB#4:                                 # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB61_3 Depth=1
	movslq	%r12d, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB61_5:                               # %.lr.ph.i
                                        #   Parent Loop BB61_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15,%r13,8), %rcx
	addq	%rbp, %rcx
	movl	$.L.str.24, %esi
	movl	$60, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	addq	$60, %rbp
	cmpq	%rbx, %rbp
	jl	.LBB61_5
.LBB61_6:                               # %._crit_edge.i
                                        #   in Loop: Header=BB61_3 Depth=1
	incq	%r13
	cmpq	8(%rsp), %r13           # 8-byte Folded Reload
	jne	.LBB61_3
.LBB61_7:                               # %writeData.exit
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end61:
	.size	writePre, .Lfunc_end61-writePre
	.cfi_endproc

	.globl	readOtherOptions
	.p2align	4, 0x90
	.type	readOtherOptions,@function
readOtherOptions:                       # @readOtherOptions
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi695:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi696:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi697:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi698:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi699:
	.cfi_def_cfa_offset 48
	subq	$256, %rsp              # imm = 0x100
.Lcfi700:
	.cfi_def_cfa_offset 304
.Lcfi701:
	.cfi_offset %rbx, -48
.Lcfi702:
	.cfi_offset %r12, -40
.Lcfi703:
	.cfi_offset %r13, -32
.Lcfi704:
	.cfi_offset %r14, -24
.Lcfi705:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpl	$0, calledByXced(%rip)
	je	.LBB62_3
# BB#1:
	movl	$.L.str.66, %edi
	movl	$.L.str.67, %esi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB62_5
# BB#2:
	movq	%rsp, %r13
	movl	$255, %esi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$.L.str.65, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	callq	sscanf
	movq	%rbx, %rdi
	callq	fclose
	jmp	.LBB62_4
.LBB62_3:
	movl	$0, (%r12)
	movl	$80, (%r15)
	movl	dorp(%rip), %eax
	cmpl	$100, %eax
	movl	$20, %ecx
	cmovel	%eax, %ecx
	movl	%ecx, (%r14)
.LBB62_4:
	addq	$256, %rsp              # imm = 0x100
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB62_5:
	movl	$.L.str.68, %edi
	callq	ErrorExit
.Lfunc_end62:
	.size	readOtherOptions, .Lfunc_end62-readOtherOptions
	.cfi_endproc

	.globl	initSignalSM
	.p2align	4, 0x90
	.type	initSignalSM,@function
initSignalSM:                           # @initSignalSM
	.cfi_startproc
# BB#0:
	cmpl	$0, ppid(%rip)
	jne	.LBB63_2
# BB#1:
	movq	$0, signalSM(%rip)
.LBB63_2:
	retq
.Lfunc_end63:
	.size	initSignalSM, .Lfunc_end63-initSignalSM
	.cfi_endproc

	.globl	initFiles
	.p2align	4, 0x90
	.type	initFiles,@function
initFiles:                              # @initFiles
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi706:
	.cfi_def_cfa_offset 16
	subq	$112, %rsp
.Lcfi707:
	.cfi_def_cfa_offset 128
.Lcfi708:
	.cfi_offset %rbx, -16
	movl	ppid(%rip), %edx
	testl	%edx, %edx
	je	.LBB64_2
# BB#1:
	movq	%rsp, %rdi
	movl	$.L.str.69, %esi
	xorl	%eax, %eax
	callq	sprintf
	jmp	.LBB64_3
.LBB64_2:
	movl	$6648432, (%rsp)        # imm = 0x657270
.LBB64_3:
	movq	%rsp, %rdi
	movl	$.L.str.70, %esi
	callq	fopen
	movq	%rax, prep_g(%rip)
	testq	%rax, %rax
	je	.LBB64_6
# BB#4:
	movl	$.L.str.72, %edi
	movl	$.L.str.70, %esi
	callq	fopen
	movq	%rax, %rbx
	movq	%rbx, trap_g(%rip)
	testq	%rbx, %rbx
	je	.LBB64_7
# BB#5:
	callq	getpid
	movl	%eax, %ecx
	movl	$.L.str.74, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ecx, %edx
	callq	fprintf
	movq	trap_g(%rip), %rdi
	callq	fflush
	addq	$112, %rsp
	popq	%rbx
	retq
.LBB64_6:
	movl	$.L.str.71, %edi
	callq	ErrorExit
.LBB64_7:
	movl	$.L.str.73, %edi
	callq	ErrorExit
.Lfunc_end64:
	.size	initFiles, .Lfunc_end64-initFiles
	.cfi_endproc

	.globl	WriteForFasta
	.p2align	4, 0x90
	.type	WriteForFasta,@function
WriteForFasta:                          # @WriteForFasta
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi709:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi710:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi711:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi712:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi713:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi714:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi715:
	.cfi_def_cfa_offset 80
.Lcfi716:
	.cfi_offset %rbx, -56
.Lcfi717:
	.cfi_offset %r12, -48
.Lcfi718:
	.cfi_offset %r13, -40
.Lcfi719:
	.cfi_offset %r14, -32
.Lcfi720:
	.cfi_offset %r15, -24
.Lcfi721:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r14
	testl	%esi, %esi
	jle	.LBB65_6
# BB#1:                                 # %.lr.ph22.preheader
	movl	%esi, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB65_2:                               # %.lr.ph22
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB65_4 Depth 2
	movq	(%r15,%r13,8), %rdi
	callq	strlen
	movq	%rax, %r12
	movq	%r13, %rdx
	shlq	$8, %rdx
	addq	16(%rsp), %rdx          # 8-byte Folded Reload
	movl	$.L.str.23, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	testl	%r12d, %r12d
	jle	.LBB65_5
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB65_2 Depth=1
	movslq	%r12d, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB65_4:                               # %.lr.ph
                                        #   Parent Loop BB65_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15,%r13,8), %rsi
	addq	%rbp, %rsi
	movl	$WriteForFasta.b, %edi
	movl	$60, %edx
	callq	strncpy
	movb	$0, WriteForFasta.b+60(%rip)
	movl	$.L.str.5, %esi
	movl	$WriteForFasta.b, %edx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	addq	$60, %rbp
	cmpq	%rbx, %rbp
	jl	.LBB65_4
.LBB65_5:                               # %._crit_edge
                                        #   in Loop: Header=BB65_2 Depth=1
	incq	%r13
	cmpq	8(%rsp), %r13           # 8-byte Folded Reload
	jne	.LBB65_2
.LBB65_6:                               # %._crit_edge23
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end65:
	.size	WriteForFasta, .Lfunc_end65-WriteForFasta
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI66_0:
	.quad	4618216237887075123     # double 5.7999999999999998
.LCPI66_1:
	.quad	4648488871632306176     # double 600
.LCPI66_2:
	.quad	0                       # double 0
	.text
	.globl	readlocalhomtable2
	.p2align	4, 0x90
	.type	readlocalhomtable2,@function
readlocalhomtable2:                     # @readlocalhomtable2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi722:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi723:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi724:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi725:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi726:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi727:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi728:
	.cfi_def_cfa_offset 112
.Lcfi729:
	.cfi_offset %rbx, -56
.Lcfi730:
	.cfi_offset %r12, -48
.Lcfi731:
	.cfi_offset %r13, -40
.Lcfi732:
	.cfi_offset %r14, -32
.Lcfi733:
	.cfi_offset %r15, -24
.Lcfi734:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rdi, %rax
	movl	$readlocalhomtable2.buff, %edi
	movl	$255, %esi
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rax, %rdx
	jmp	.LBB66_1
	.p2align	4, 0x90
.LBB66_8:                               #   in Loop: Header=BB66_1 Depth=1
	movl	24(%rsp), %ecx
	movl	%ecx, 32(%rax)
	movl	16(%rsp), %ecx
	movl	%ecx, 24(%rax)
	movl	20(%rsp), %ecx
	movl	%ecx, 36(%rax)
	movl	12(%rsp), %ecx
	movl	%ecx, 28(%rax)
	movsd	%xmm0, 40(%rax)
	movl	28(%rsp), %ecx
	movl	%ecx, 48(%rax)
	movl	$readlocalhomtable2.buff, %edi
	movl	$255, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB66_1:                               # =>This Inner Loop Header: Depth=1
	callq	fgets
	testq	%rax, %rax
	je	.LBB66_9
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB66_1 Depth=1
	movl	$readlocalhomtable2.buff, %edi
	movl	$.L.str.75, %esi
	movl	$0, %eax
	leaq	8(%rsp), %rdx
	leaq	4(%rsp), %rcx
	leaq	28(%rsp), %r8
	leaq	48(%rsp), %r9
	leaq	12(%rsp), %rbp
	pushq	%rbp
.Lcfi735:
	.cfi_adjust_cfa_offset 8
	leaq	24(%rsp), %rbp
	pushq	%rbp
.Lcfi736:
	.cfi_adjust_cfa_offset 8
	leaq	36(%rsp), %rbp
	pushq	%rbp
.Lcfi737:
	.cfi_adjust_cfa_offset 8
	leaq	48(%rsp), %rbp
	pushq	%rbp
.Lcfi738:
	.cfi_adjust_cfa_offset 8
	callq	sscanf
	addq	$32, %rsp
.Lcfi739:
	.cfi_adjust_cfa_offset -32
	movslq	8(%rsp), %rax
	movq	(%r14,%rax,8), %rax
	movslq	4(%rsp), %rcx
	leaq	(%rcx,%rcx,4), %rcx
	shlq	$4, %rcx
	movl	(%rax,%rcx), %edx
	leal	1(%rdx), %esi
	movl	%esi, (%rax,%rcx)
	movslq	8(%rsp), %r15
	movq	(%r14,%r15,8), %rax
	movslq	4(%rsp), %r12
	testl	%edx, %edx
	jle	.LBB66_4
# BB#3:                                 #   in Loop: Header=BB66_1 Depth=1
	movq	%r12, %rcx
	shlq	$4, %rcx
	leaq	(%rcx,%rcx,4), %rbp
	movq	16(%rax,%rbp), %r13
	movl	$1, %edi
	movl	$80, %esi
	callq	calloc
	movq	%rax, 8(%r13)
	movl	$-1, 52(%rax)
	movq	$0, 8(%rax)
	movq	(%r14,%r15,8), %rcx
	movq	%rax, 16(%rcx,%rbp)
	jmp	.LBB66_5
	.p2align	4, 0x90
.LBB66_4:                               #   in Loop: Header=BB66_1 Depth=1
	leaq	(%r12,%r12,4), %rcx
	shlq	$4, %rcx
	addq	%rcx, %rax
.LBB66_5:                               #   in Loop: Header=BB66_1 Depth=1
	movl	24(%rsp), %ecx
	movl	%ecx, 24(%rax)
	movl	16(%rsp), %ecx
	movl	%ecx, 32(%rax)
	movl	20(%rsp), %ecx
	movl	%ecx, 28(%rax)
	movl	12(%rsp), %ecx
	movl	%ecx, 36(%rax)
	movsd	48(%rsp), %xmm0         # xmm0 = mem[0],zero
	addsd	.LCPI66_2, %xmm0
	divsd	.LCPI66_0(%rip), %xmm0
	mulsd	.LCPI66_1(%rip), %xmm0
	movsd	%xmm0, 40(%rax)
	movl	28(%rsp), %ecx
	movl	%ecx, 48(%rax)
	movq	(%r14,%r12,8), %rax
	leaq	(%r15,%r15,4), %rcx
	shlq	$4, %rcx
	movl	(%rax,%rcx), %edx
	leal	1(%rdx), %esi
	movl	%esi, (%rax,%rcx)
	movslq	4(%rsp), %rbp
	movq	(%r14,%rbp,8), %rax
	movslq	8(%rsp), %rcx
	testl	%edx, %edx
	jle	.LBB66_7
# BB#6:                                 #   in Loop: Header=BB66_1 Depth=1
	shlq	$4, %rcx
	leaq	(%rcx,%rcx,4), %rbx
	movq	16(%rax,%rbx), %r15
	movl	$1, %edi
	movl	$80, %esi
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	callq	calloc
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%rax, 8(%r15)
	movl	$-1, 52(%rax)
	movq	$0, 8(%rax)
	movq	(%r14,%rbp,8), %rcx
	movq	%rax, 16(%rcx,%rbx)
	jmp	.LBB66_8
	.p2align	4, 0x90
.LBB66_7:                               #   in Loop: Header=BB66_1 Depth=1
	leaq	(%rcx,%rcx,4), %rcx
	shlq	$4, %rcx
	addq	%rcx, %rax
	jmp	.LBB66_8
.LBB66_9:                               # %._crit_edge
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end66:
	.size	readlocalhomtable2, .Lfunc_end66-readlocalhomtable2
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI67_0:
	.quad	4618216237887075123     # double 5.7999999999999998
.LCPI67_1:
	.quad	4648488871632306176     # double 600
.LCPI67_2:
	.quad	0                       # double 0
	.text
	.globl	readlocalhomtable
	.p2align	4, 0x90
	.type	readlocalhomtable,@function
readlocalhomtable:                      # @readlocalhomtable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi740:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi741:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi742:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi743:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi744:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi745:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi746:
	.cfi_def_cfa_offset 112
.Lcfi747:
	.cfi_offset %rbx, -56
.Lcfi748:
	.cfi_offset %r12, -48
.Lcfi749:
	.cfi_offset %r13, -40
.Lcfi750:
	.cfi_offset %r14, -32
.Lcfi751:
	.cfi_offset %r15, -24
.Lcfi752:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %ebp
	movq	%rdi, %r15
	movl	%ebp, %edi
	callq	AllocateIntMtx
	movq	%rax, %rbx
	movl	$0, 16(%rsp)
	testl	%ebp, %ebp
	jle	.LBB67_5
# BB#1:                                 # %.preheader36.us.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB67_2:                               # %.preheader36.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB67_3 Depth 2
	movl	$0, 12(%rsp)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB67_3:                               #   Parent Loop BB67_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cltq
	movq	(%rbx,%rax,8), %rax
	movslq	%ecx, %rcx
	movl	$0, (%rax,%rcx,4)
	movl	12(%rsp), %ecx
	incl	%ecx
	movl	%ecx, 12(%rsp)
	cmpl	%ebp, %ecx
	movl	16(%rsp), %eax
	jl	.LBB67_3
# BB#4:                                 # %._crit_edge41.us
                                        #   in Loop: Header=BB67_2 Depth=1
	incl	%eax
	movl	%eax, 16(%rsp)
	cmpl	%ebp, %eax
	jl	.LBB67_2
.LBB67_5:                               # %.preheader
	movl	$readlocalhomtable.buff, %edi
	movl	$255, %esi
	movq	%r15, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB67_14
# BB#6:                                 # %.lr.ph.preheader
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB67_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$readlocalhomtable.buff, %edi
	movl	$.L.str.75, %esi
	movl	$0, %eax
	leaq	16(%rsp), %rdx
	leaq	12(%rsp), %rcx
	leaq	36(%rsp), %r8
	leaq	48(%rsp), %r9
	leaq	20(%rsp), %rbp
	pushq	%rbp
.Lcfi753:
	.cfi_adjust_cfa_offset 8
	leaq	32(%rsp), %rbp
	pushq	%rbp
.Lcfi754:
	.cfi_adjust_cfa_offset 8
	leaq	44(%rsp), %rbp
	pushq	%rbp
.Lcfi755:
	.cfi_adjust_cfa_offset 8
	leaq	56(%rsp), %rbp
	pushq	%rbp
.Lcfi756:
	.cfi_adjust_cfa_offset 8
	callq	sscanf
	addq	$32, %rsp
.Lcfi757:
	.cfi_adjust_cfa_offset -32
	movslq	16(%rsp), %rax
	movq	(%rbx,%rax,8), %rax
	movslq	12(%rsp), %rcx
	movl	(%rax,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, (%rax,%rcx,4)
	testl	%edx, %edx
	jle	.LBB67_9
# BB#8:                                 #   in Loop: Header=BB67_7 Depth=1
	movl	$1, %edi
	movl	$80, %esi
	callq	calloc
	movq	%rax, 8(%r12)
	movq	$0, 8(%rax)
	movl	12(%rsp), %edx
	movl	16(%rsp), %ecx
	movq	%rax, %r12
	jmp	.LBB67_10
	.p2align	4, 0x90
.LBB67_9:                               #   in Loop: Header=BB67_7 Depth=1
	movslq	16(%rsp), %rcx
	movslq	12(%rsp), %rdx
	leaq	(%rdx,%rdx,4), %r12
	shlq	$4, %r12
	addq	(%r14,%rcx,8), %r12
.LBB67_10:                              #   in Loop: Header=BB67_7 Depth=1
	movl	32(%rsp), %eax
	movl	%eax, 24(%r12)
	movl	24(%rsp), %eax
	movl	%eax, 32(%r12)
	movl	28(%rsp), %eax
	movl	%eax, 28(%r12)
	movl	20(%rsp), %eax
	movl	%eax, 36(%r12)
	movsd	48(%rsp), %xmm0         # xmm0 = mem[0],zero
	addsd	.LCPI67_2, %xmm0
	divsd	.LCPI67_0(%rip), %xmm0
	mulsd	.LCPI67_1(%rip), %xmm0
	movsd	%xmm0, 40(%r12)
	movl	36(%rsp), %eax
	movl	%eax, 48(%r12)
	movslq	%edx, %rax
	movq	(%rbx,%rax,8), %rax
	movslq	%ecx, %rcx
	movl	(%rax,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, (%rax,%rcx,4)
	testl	%edx, %edx
	jle	.LBB67_12
# BB#11:                                #   in Loop: Header=BB67_7 Depth=1
	movl	$1, %edi
	movl	$80, %esi
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	callq	calloc
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%rax, 8(%r13)
	movq	$0, 8(%rax)
	movq	%rax, %r13
	jmp	.LBB67_13
	.p2align	4, 0x90
.LBB67_12:                              #   in Loop: Header=BB67_7 Depth=1
	movslq	12(%rsp), %rax
	movslq	16(%rsp), %rcx
	leaq	(%rcx,%rcx,4), %r13
	shlq	$4, %r13
	addq	(%r14,%rax,8), %r13
.LBB67_13:                              #   in Loop: Header=BB67_7 Depth=1
	movl	32(%rsp), %eax
	movl	%eax, 32(%r13)
	movl	24(%rsp), %eax
	movl	%eax, 24(%r13)
	movl	28(%rsp), %eax
	movl	%eax, 36(%r13)
	movl	20(%rsp), %eax
	movl	%eax, 28(%r13)
	movsd	%xmm0, 40(%r13)
	movl	36(%rsp), %eax
	movl	%eax, 48(%r13)
	movl	$readlocalhomtable.buff, %edi
	movl	$255, %esi
	movq	%r15, %rdx
	callq	fgets
	testq	%rax, %rax
	jne	.LBB67_7
.LBB67_14:                              # %._crit_edge
	movq	%rbx, %rdi
	callq	FreeIntMtx
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end67:
	.size	readlocalhomtable, .Lfunc_end67-readlocalhomtable
	.cfi_endproc

	.globl	outlocalhom
	.p2align	4, 0x90
	.type	outlocalhom,@function
outlocalhom:                            # @outlocalhom
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi758:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi759:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi760:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi761:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi762:
	.cfi_def_cfa_offset 48
.Lcfi763:
	.cfi_offset %rbx, -48
.Lcfi764:
	.cfi_offset %r12, -40
.Lcfi765:
	.cfi_offset %r13, -32
.Lcfi766:
	.cfi_offset %r14, -24
.Lcfi767:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testl	%esi, %esi
	jg	.LBB68_1
.LBB68_7:                               # %._crit_edge26
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB68_1:                               # %.preheader.us.preheader
	movl	%esi, %r12d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB68_2:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB68_3 Depth 2
                                        #       Child Loop BB68_4 Depth 3
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB68_3:                               #   Parent Loop BB68_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB68_4 Depth 3
	leaq	(%r13,%r13,4), %rbx
	shlq	$4, %rbx
	addq	(%r14,%r15,8), %rbx
	movq	stderr(%rip), %rdi
	movl	$.L.str.76, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	movl	%r13d, %ecx
	callq	fprintf
	.p2align	4, 0x90
.LBB68_4:                               #   Parent Loop BB68_2 Depth=1
                                        #     Parent Loop BB68_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	stderr(%rip), %rdi
	movl	24(%rbx), %edx
	movl	28(%rbx), %ecx
	movl	32(%rbx), %r8d
	movl	36(%rbx), %r9d
	movsd	56(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	40(%rbx), %xmm1         # xmm1 = mem[0],zero
	movl	$.L.str.77, %esi
	movb	$2, %al
	callq	fprintf
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB68_4
# BB#5:                                 #   in Loop: Header=BB68_3 Depth=2
	incq	%r13
	cmpq	%r12, %r13
	jne	.LBB68_3
# BB#6:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB68_2 Depth=1
	incq	%r15
	cmpq	%r13, %r15
	jne	.LBB68_2
	jmp	.LBB68_7
.Lfunc_end68:
	.size	outlocalhom, .Lfunc_end68-outlocalhom
	.cfi_endproc

	.globl	outlocalhompt
	.p2align	4, 0x90
	.type	outlocalhompt,@function
outlocalhompt:                          # @outlocalhompt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi768:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi769:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi770:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi771:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi772:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi773:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi774:
	.cfi_def_cfa_offset 64
.Lcfi775:
	.cfi_offset %rbx, -56
.Lcfi776:
	.cfi_offset %r12, -48
.Lcfi777:
	.cfi_offset %r13, -40
.Lcfi778:
	.cfi_offset %r14, -32
.Lcfi779:
	.cfi_offset %r15, -24
.Lcfi780:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	testl	%esi, %esi
	jle	.LBB69_8
# BB#1:
	testl	%edx, %edx
	jg	.LBB69_2
.LBB69_8:                               # %._crit_edge27
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB69_2:                               # %.preheader.us.preheader
	movl	%edx, %r12d
	movl	%esi, %r13d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB69_3:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB69_4 Depth 2
                                        #       Child Loop BB69_5 Depth 3
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB69_4:                               #   Parent Loop BB69_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB69_5 Depth 3
	movq	(%r14,%r15,8), %rax
	movq	(%rax,%rbx,8), %rbp
	movq	stderr(%rip), %rdi
	movl	$.L.str.76, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	movl	%ebx, %ecx
	callq	fprintf
	.p2align	4, 0x90
.LBB69_5:                               #   Parent Loop BB69_3 Depth=1
                                        #     Parent Loop BB69_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	stderr(%rip), %rdi
	movl	24(%rbp), %edx
	movl	28(%rbp), %ecx
	movl	32(%rbp), %r8d
	movl	36(%rbp), %r9d
	movsd	56(%rbp), %xmm0         # xmm0 = mem[0],zero
	movsd	40(%rbp), %xmm1         # xmm1 = mem[0],zero
	movsd	72(%rbp), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str.78, %esi
	movb	$3, %al
	callq	fprintf
	movq	8(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB69_5
# BB#6:                                 #   in Loop: Header=BB69_4 Depth=2
	incq	%rbx
	cmpq	%r12, %rbx
	jne	.LBB69_4
# BB#7:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB69_3 Depth=1
	incq	%r15
	cmpq	%r13, %r15
	jne	.LBB69_3
	jmp	.LBB69_8
.Lfunc_end69:
	.size	outlocalhompt, .Lfunc_end69-outlocalhompt
	.cfi_endproc

	.globl	FreeLocalHomTable
	.p2align	4, 0x90
	.type	FreeLocalHomTable,@function
FreeLocalHomTable:                      # @FreeLocalHomTable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi781:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi782:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi783:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi784:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi785:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi786:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi787:
	.cfi_def_cfa_offset 64
.Lcfi788:
	.cfi_offset %rbx, -56
.Lcfi789:
	.cfi_offset %r12, -48
.Lcfi790:
	.cfi_offset %r13, -40
.Lcfi791:
	.cfi_offset %r14, -32
.Lcfi792:
	.cfi_offset %r15, -24
.Lcfi793:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r12
	movq	stdout(%rip), %rcx
	movl	$.L.str.79, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	testl	%ebp, %ebp
	jle	.LBB70_9
# BB#1:                                 # %.preheader.us.preheader
	movl	%ebp, %r14d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB70_2:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB70_3 Depth 2
                                        #       Child Loop BB70_4 Depth 3
	movq	(%r12,%r13,8), %rdi
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB70_3:                               #   Parent Loop BB70_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB70_4 Depth 3
	leaq	(%r15,%r15,4), %rbx
	shlq	$4, %rbx
	movq	%rdi, %rax
	addq	%rbx, %rax
	je	.LBB70_7
	.p2align	4, 0x90
.LBB70_4:                               # %.lr.ph.us
                                        #   Parent Loop BB70_2 Depth=1
                                        #     Parent Loop BB70_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rax), %rbp
	addq	%rbx, %rdi
	cmpq	%rdi, %rax
	je	.LBB70_6
# BB#5:                                 #   in Loop: Header=BB70_4 Depth=3
	movq	%rax, %rdi
	callq	free
.LBB70_6:                               # %.backedge.us
                                        #   in Loop: Header=BB70_4 Depth=3
	testq	%rbp, %rbp
	movq	(%r12,%r13,8), %rdi
	movq	%rbp, %rax
	jne	.LBB70_4
.LBB70_7:                               # %._crit_edge.us
                                        #   in Loop: Header=BB70_3 Depth=2
	incq	%r15
	cmpq	%r14, %r15
	jne	.LBB70_3
# BB#8:                                 # %._crit_edge30.us
                                        #   in Loop: Header=BB70_2 Depth=1
	callq	free
	incq	%r13
	cmpq	%r15, %r13
	jne	.LBB70_2
.LBB70_9:                               # %._crit_edge32
	movq	%r12, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end70:
	.size	FreeLocalHomTable, .Lfunc_end70-FreeLocalHomTable
	.cfi_endproc

	.globl	progName
	.p2align	4, 0x90
	.type	progName,@function
progName:                               # @progName
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi794:
	.cfi_def_cfa_offset 16
.Lcfi795:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$47, %esi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rax
	cmoveq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end71:
	.size	progName, .Lfunc_end71-progName
	.cfi_endproc

	.globl	clustalout_pointer
	.p2align	4, 0x90
	.type	clustalout_pointer,@function
clustalout_pointer:                     # @clustalout_pointer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi796:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi797:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi798:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi799:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi800:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi801:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi802:
	.cfi_def_cfa_offset 96
.Lcfi803:
	.cfi_offset %rbx, -56
.Lcfi804:
	.cfi_offset %r12, -48
.Lcfi805:
	.cfi_offset %r13, -40
.Lcfi806:
	.cfi_offset %r14, -32
.Lcfi807:
	.cfi_offset %r15, -24
.Lcfi808:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	%edx, %ebp
	movl	%esi, %ebx
	movq	%rdi, %r13
	movq	96(%rsp), %rdx
	testq	%rdx, %rdx
	je	.LBB72_1
# BB#2:
	movl	$.L.str.82, %esi
	movl	$.L.str.81, %ecx
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	testl	%ebp, %ebp
	jg	.LBB72_4
	jmp	.LBB72_9
.LBB72_1:
	movl	$.L.str.80, %esi
	movl	$.L.str.81, %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	testl	%ebp, %ebp
	jle	.LBB72_9
.LBB72_4:                               # %.lr.ph34
	testl	%ebx, %ebx
	movslq	%ebp, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jle	.LBB72_5
# BB#10:                                # %.lr.ph34.split.us.preheader
	movq	104(%rsp), %rbp
	movl	%ebx, %r14d
	xorl	%r15d, %r15d
	movq	%r12, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB72_11:                              # %.lr.ph34.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB72_12 Depth 2
                                        #       Child Loop BB72_14 Depth 3
                                        #       Child Loop BB72_17 Depth 3
                                        #         Child Loop BB72_18 Depth 4
	movl	$10, %edi
	movq	%r13, %rsi
	callq	fputc
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB72_12:                              #   Parent Loop BB72_11 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB72_14 Depth 3
                                        #       Child Loop BB72_17 Depth 3
                                        #         Child Loop BB72_18 Depth 4
	movslq	(%rbp,%r12,4), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rbx
	incq	%rbx
	jmp	.LBB72_14
	.p2align	4, 0x90
.LBB72_13:                              # %.lr.ph.i.i.us
                                        #   in Loop: Header=BB72_14 Depth=3
	movb	$32, (%rax)
.LBB72_14:                              # %.lr.ph.i.i.us
                                        #   Parent Loop BB72_11 Depth=1
                                        #     Parent Loop BB72_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$9, %esi
	movq	%rbx, %rdi
	callq	strchr
	testq	%rax, %rax
	jne	.LBB72_13
# BB#15:                                # %tabtospace.exit.preheader.i.us
                                        #   in Loop: Header=BB72_12 Depth=2
	movb	(%rbx), %al
	testb	%al, %al
	jne	.LBB72_17
	jmp	.LBB72_25
.LBB72_27:                              # %tabtospace.exit.outer.outer.i.us
                                        #   in Loop: Header=BB72_17 Depth=3
	movb	1(%rbx), %al
	incq	%rbx
	testb	%al, %al
	je	.LBB72_25
	.p2align	4, 0x90
.LBB72_17:                              # %.lr.ph.lr.ph.i.us
                                        #   Parent Loop BB72_11 Depth=1
                                        #     Parent Loop BB72_12 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB72_18 Depth 4
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB72_18:                              # %.lr.ph.i.us
                                        #   Parent Loop BB72_11 Depth=1
                                        #     Parent Loop BB72_12 Depth=2
                                        #       Parent Loop BB72_17 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	$1, %rcx
	je	.LBB72_23
# BB#19:                                # %.lr.ph.split.preheader.i.us
                                        #   in Loop: Header=BB72_18 Depth=4
	cmpb	$32, %al
	jne	.LBB72_24
	jmp	.LBB72_20
	.p2align	4, 0x90
.LBB72_23:                              # %.lr.ph.split.us.i.us
                                        #   in Loop: Header=BB72_18 Depth=4
	cmpb	$32, %al
	je	.LBB72_27
.LBB72_24:                              # %tabtospace.exit.outer.i.us
                                        #   in Loop: Header=BB72_18 Depth=4
	movzbl	(%rbx,%rcx), %eax
	incq	%rcx
	testb	%al, %al
	jne	.LBB72_18
	jmp	.LBB72_25
	.p2align	4, 0x90
.LBB72_20:                              # %tabtospace.exit.i.us
                                        #   in Loop: Header=BB72_12 Depth=2
	movb	$0, -1(%rbx,%rcx)
.LBB72_25:                              # %extractfirstword.exit.us
                                        #   in Loop: Header=BB72_12 Depth=2
	movl	$.L.str.83, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	fprintf
	movslq	(%rbp,%r12,4), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rdx
	addq	%r15, %rdx
	movl	$.L.str.84, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	incq	%r12
	cmpq	%r14, %r12
	jne	.LBB72_12
# BB#26:                                # %._crit_edge.us
                                        #   in Loop: Header=BB72_11 Depth=1
	movq	16(%rsp), %r12          # 8-byte Reload
	testq	%r12, %r12
	je	.LBB72_22
# BB#21:                                #   in Loop: Header=BB72_11 Depth=1
	movl	$.L.str.83, %esi
	movl	$.L.str.85, %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	leaq	(%r12,%r15), %rdx
	movl	$.L.str.84, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
.LBB72_22:                              #   in Loop: Header=BB72_11 Depth=1
	addq	$60, %r15
	cmpq	8(%rsp), %r15           # 8-byte Folded Reload
	jl	.LBB72_11
	jmp	.LBB72_9
.LBB72_5:                               # %.lr.ph34.split.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB72_6:                               # %.lr.ph34.split
                                        # =>This Inner Loop Header: Depth=1
	movl	$10, %edi
	movq	%r13, %rsi
	callq	fputc
	testq	%r12, %r12
	je	.LBB72_8
# BB#7:                                 #   in Loop: Header=BB72_6 Depth=1
	movl	$.L.str.83, %esi
	movl	$.L.str.85, %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	leaq	(%r12,%rbx), %rdx
	movl	$.L.str.84, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
.LBB72_8:                               #   in Loop: Header=BB72_6 Depth=1
	addq	$60, %rbx
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	jl	.LBB72_6
.LBB72_9:                               # %._crit_edge35
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end72:
	.size	clustalout_pointer, .Lfunc_end72-clustalout_pointer
	.cfi_endproc

	.globl	writeData_reorder_pointer
	.p2align	4, 0x90
	.type	writeData_reorder_pointer,@function
writeData_reorder_pointer:              # @writeData_reorder_pointer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi809:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi810:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi811:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi812:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi813:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi814:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi815:
	.cfi_def_cfa_offset 80
.Lcfi816:
	.cfi_offset %rbx, -56
.Lcfi817:
	.cfi_offset %r12, -48
.Lcfi818:
	.cfi_offset %r13, -40
.Lcfi819:
	.cfi_offset %r14, -32
.Lcfi820:
	.cfi_offset %r15, -24
.Lcfi821:
	.cfi_offset %rbp, -16
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movq	%r8, %r12
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r13
	testl	%esi, %esi
	jle	.LBB73_6
# BB#1:                                 # %.lr.ph27.preheader
	movl	%esi, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB73_2:                               # %.lr.ph27
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB73_4 Depth 2
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	(%rax,%r14,4), %r15
	movq	(%r12,%r15,8), %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax,%r15,8), %rdx
	incq	%rdx
	movl	$.L.str.23, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	testl	%ebx, %ebx
	jle	.LBB73_5
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB73_2 Depth=1
	movslq	%ebx, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB73_4:                               # %.lr.ph
                                        #   Parent Loop BB73_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%r15,8), %rcx
	addq	%rbx, %rcx
	movl	$.L.str.24, %esi
	movl	$60, %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	addq	$60, %rbx
	cmpq	%rbp, %rbx
	jl	.LBB73_4
.LBB73_5:                               # %._crit_edge
                                        #   in Loop: Header=BB73_2 Depth=1
	incq	%r14
	cmpq	(%rsp), %r14            # 8-byte Folded Reload
	jne	.LBB73_2
.LBB73_6:                               # %._crit_edge28
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end73:
	.size	writeData_reorder_pointer, .Lfunc_end73-writeData_reorder_pointer
	.cfi_endproc

	.globl	writeData_reorder
	.p2align	4, 0x90
	.type	writeData_reorder,@function
writeData_reorder:                      # @writeData_reorder
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi822:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi823:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi824:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi825:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi826:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi827:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi828:
	.cfi_def_cfa_offset 80
.Lcfi829:
	.cfi_offset %rbx, -56
.Lcfi830:
	.cfi_offset %r12, -48
.Lcfi831:
	.cfi_offset %r13, -40
.Lcfi832:
	.cfi_offset %r14, -32
.Lcfi833:
	.cfi_offset %r15, -24
.Lcfi834:
	.cfi_offset %rbp, -16
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movq	%r8, %r12
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r13
	testl	%esi, %esi
	jle	.LBB74_6
# BB#1:                                 # %.lr.ph27.preheader
	movl	%esi, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB74_2:                               # %.lr.ph27
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB74_4 Depth 2
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	(%rax,%r14,4), %r15
	movq	(%r12,%r15,8), %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	%r15, %rax
	shlq	$8, %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	1(%rcx,%rax), %rdx
	movl	$.L.str.23, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	testl	%ebx, %ebx
	jle	.LBB74_5
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB74_2 Depth=1
	movslq	%ebx, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB74_4:                               # %.lr.ph
                                        #   Parent Loop BB74_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%r15,8), %rcx
	addq	%rbx, %rcx
	movl	$.L.str.24, %esi
	movl	$60, %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	addq	$60, %rbx
	cmpq	%rbp, %rbx
	jl	.LBB74_4
.LBB74_5:                               # %._crit_edge
                                        #   in Loop: Header=BB74_2 Depth=1
	incq	%r14
	cmpq	(%rsp), %r14            # 8-byte Folded Reload
	jne	.LBB74_2
.LBB74_6:                               # %._crit_edge28
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end74:
	.size	writeData_reorder, .Lfunc_end74-writeData_reorder
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI75_0:
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.text
	.globl	loadaamtx
	.p2align	4, 0x90
	.type	loadaamtx,@function
loadaamtx:                              # @loadaamtx
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi835:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi836:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi837:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi838:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi839:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi840:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi841:
	.cfi_def_cfa_offset 96
.Lcfi842:
	.cfi_offset %rbx, -56
.Lcfi843:
	.cfi_offset %r12, -48
.Lcfi844:
	.cfi_offset %r13, -40
.Lcfi845:
	.cfi_offset %r14, -32
.Lcfi846:
	.cfi_offset %r15, -24
.Lcfi847:
	.cfi_offset %rbp, -16
	movl	$21, %edi
	movl	$20, %esi
	callq	AllocateDoubleMtx
	movq	%rax, %r15
	movl	$420, %edi              # imm = 0x1A4
	callq	AllocateDoubleVec
	movq	%rax, %r14
	movl	$20, %edi
	callq	AllocateIntVec
	movq	%rax, %r13
	cmpl	$112, dorp(%rip)
	jne	.LBB75_1
# BB#3:
	movl	$.L.str.87, %edi
	movl	$.L.str.67, %esi
	callq	fopen
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB75_4
# BB#5:
	movl	$1000, %edi             # imm = 0x3E8
	movl	$1, %esi
	callq	calloc
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$1000, %edi             # imm = 0x3E8
	movl	$1, %esi
	callq	calloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	24(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB75_6:                               # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB75_7
# BB#8:                                 #   in Loop: Header=BB75_6 Depth=1
	movl	$999, %esi              # imm = 0x3E7
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	movzbl	(%rbx), %r12d
	cmpb	$35, %r12b
	je	.LBB75_6
	jmp	.LBB75_9
.LBB75_7:                               # %..preheader176_crit_edge
	movq	8(%rsp), %rax           # 8-byte Reload
	movb	(%rax), %r12b
.LBB75_9:                               # %.preheader176
	movq	%r14, 32(%rsp)          # 8-byte Spill
	testb	%r12b, %r12b
	je	.LBB75_14
# BB#10:                                # %.lr.ph192
	callq	__ctype_b_loc
	movq	%rax, %rcx
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	%r14, %rbp
	incq	%rbp
	.p2align	4, 0x90
.LBB75_11:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rax
	movsbq	%r12b, %rbx
	testb	$4, 1(%rax,%rbx,2)
	je	.LBB75_13
# BB#12:                                #   in Loop: Header=BB75_11 Depth=1
	movq	%rcx, %r12
	callq	__ctype_toupper_loc
	movq	%r12, %rcx
	movq	(%rax), %rax
	movzbl	(%rax,%rbx,4), %eax
	movb	%al, (%r14)
	incq	%r14
.LBB75_13:                              #   in Loop: Header=BB75_11 Depth=1
	movzbl	(%rbp), %r12d
	incq	%rbp
	testb	%r12b, %r12b
	jne	.LBB75_11
.LBB75_14:                              # %._crit_edge193
	movq	8(%rsp), %rbx           # 8-byte Reload
	movb	$0, 20(%rbx)
	xorl	%ebp, %ebp
	movq	24(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB75_15:                              # =>This Inner Loop Header: Depth=1
	movsbl	.L.str.86(%rbp), %r14d
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	strchr
	testq	%rax, %rax
	je	.LBB75_47
# BB#16:                                #   in Loop: Header=BB75_15 Depth=1
	subl	%ebx, %eax
	movl	%eax, (%r13,%rbp,4)
	incq	%rbp
	cmpq	$20, %rbp
	jl	.LBB75_15
# BB#17:                                # %.outer.preheader
	xorl	%ebx, %ebx
	movq	16(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB75_18:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB75_22 Depth 2
                                        #       Child Loop BB75_23 Depth 3
	movq	%r12, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB75_29
# BB#19:                                #   in Loop: Header=BB75_18 Depth=1
	movl	$999, %esi              # imm = 0x3E7
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	fgets
	cmpb	$35, (%r14)
	je	.LBB75_18
# BB#20:                                # %.preheader172.lr.ph
                                        #   in Loop: Header=BB75_18 Depth=1
	callq	__ctype_b_loc
	xorl	%r12d, %r12d
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB75_22:                              # %.preheader172
                                        #   Parent Loop BB75_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB75_23 Depth 3
	movq	(%rbp), %rax
	decq	%r14
	.p2align	4, 0x90
.LBB75_23:                              #   Parent Loop BB75_18 Depth=1
                                        #     Parent Loop BB75_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbq	1(%r14), %rcx
	incq	%r14
	movl	%ecx, %edx
	addb	$-45, %dl
	cmpb	$2, %dl
	jb	.LBB75_25
# BB#24:                                #   in Loop: Header=BB75_23 Depth=3
	movzwl	(%rax,%rcx,2), %ecx
	andl	$2048, %ecx             # imm = 0x800
	testw	%cx, %cx
	je	.LBB75_23
.LBB75_25:                              # %.critedge
                                        #   in Loop: Header=BB75_22 Depth=2
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	strtod
	movq	(%r15,%rbx,8), %rax
	movsd	%xmm0, (%rax,%r12,8)
	movl	$32, %esi
	movq	%r14, %rdi
	callq	strchr
	movq	%rax, %r14
	cmpq	%rbx, %r12
	jge	.LBB75_21
# BB#26:                                # %.critedge
                                        #   in Loop: Header=BB75_22 Depth=2
	testq	%r14, %r14
	je	.LBB75_27
.LBB75_21:                              #   in Loop: Header=BB75_22 Depth=2
	cmpq	%rbx, %r12
	leaq	1(%r12), %r12
	jl	.LBB75_22
# BB#28:                                # %._crit_edge188
                                        #   in Loop: Header=BB75_18 Depth=1
	cmpq	$18, %rbx
	leaq	1(%rbx), %rbx
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	jle	.LBB75_18
.LBB75_29:                              # %.preheader171
	movq	160(%r15), %rax
	movapd	.LCPI75_0(%rip), %xmm0  # xmm0 = [-1.000000e+00,-1.000000e+00]
	movupd	%xmm0, (%rax)
	movupd	%xmm0, 16(%rax)
	movupd	%xmm0, 32(%rax)
	movupd	%xmm0, 48(%rax)
	movupd	%xmm0, 64(%rax)
	movupd	%xmm0, 80(%rax)
	movupd	%xmm0, 96(%rax)
	movupd	%xmm0, 112(%rax)
	movupd	%xmm0, 128(%rax)
	movupd	%xmm0, 144(%rax)
	.p2align	4, 0x90
.LBB75_30:                              # %.preheader169
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB75_38
# BB#31:                                #   in Loop: Header=BB75_30 Depth=1
	movl	$999, %esi              # imm = 0x3E7
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movq	%r12, %rdx
	callq	fgets
	cmpb	$102, (%rbx)
	jne	.LBB75_30
# BB#32:                                # %.preheader168
	xorl	%r14d, %r14d
	callq	__ctype_b_loc
	movq	%rax, %rbx
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB75_33:                              # %.preheader167
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB75_34 Depth 2
	movq	(%rbx), %rax
	decq	%rbp
	.p2align	4, 0x90
.LBB75_34:                              #   Parent Loop BB75_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	1(%rbp), %rcx
	incq	%rbp
	movl	%ecx, %edx
	addb	$-45, %dl
	cmpb	$2, %dl
	jb	.LBB75_36
# BB#35:                                #   in Loop: Header=BB75_34 Depth=2
	movzwl	(%rax,%rcx,2), %ecx
	andl	$2048, %ecx             # imm = 0x800
	testw	%cx, %cx
	je	.LBB75_34
.LBB75_36:                              # %.critedge159
                                        #   in Loop: Header=BB75_33 Depth=1
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	strtod
	movq	160(%r15), %rax
	movsd	%xmm0, (%rax,%r14,8)
	movl	$32, %esi
	movq	%rbp, %rdi
	callq	strchr
	movq	%rax, %rbp
	cmpq	$18, %r14
	jg	.LBB75_37
# BB#45:                                # %.critedge159
                                        #   in Loop: Header=BB75_33 Depth=1
	testq	%rbp, %rbp
	je	.LBB75_46
.LBB75_37:                              #   in Loop: Header=BB75_33 Depth=1
	incq	%r14
	cmpq	$20, %r14
	jl	.LBB75_33
.LBB75_38:                              # %.lr.ph.preheader
	movl	$1, %r8d
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	movq	32(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB75_39:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB75_40 Depth 2
	movl	(%r13,%rdx,4), %ecx
	movslq	%r9d, %r9
	leaq	(%r14,%r9,8), %rbp
	leaq	1(%rdx), %r10
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB75_40:                              #   Parent Loop BB75_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rsi, %rdx
	movl	%ecx, %ebx
	movl	%ecx, %edi
	je	.LBB75_42
# BB#41:                                #   in Loop: Header=BB75_40 Depth=2
	cmpl	(%r13,%rsi,4), %ecx
	movq	%rsi, %rdi
	cmovgq	%rdx, %rdi
	movslq	%edi, %rdi
	movl	(%r13,%rdi,4), %edi
	movq	%rsi, %rax
	cmovlq	%rdx, %rax
	cltq
	movl	(%r13,%rax,4), %ebx
.LBB75_42:                              #   in Loop: Header=BB75_40 Depth=2
	movslq	%edi, %rax
	movq	(%r15,%rax,8), %rax
	movslq	%ebx, %rdi
	movq	(%rax,%rdi,8), %rax
	movq	%rax, (%rbp,%rsi,8)
	incq	%rsi
	cmpq	%rsi, %r10
	jne	.LBB75_40
# BB#43:                                # %._crit_edge
                                        #   in Loop: Header=BB75_39 Depth=1
	addq	%r8, %r9
	incq	%r8
	cmpq	$20, %r10
	movq	%r10, %rdx
	jne	.LBB75_39
# BB#44:                                # %.preheader
	movq	160(%r15), %rax
	movslq	(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3200(%r14)
	movslq	4(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3208(%r14)
	movslq	8(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3216(%r14)
	movslq	12(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3224(%r14)
	movslq	16(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3232(%r14)
	movslq	20(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3240(%r14)
	movslq	24(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3248(%r14)
	movslq	28(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3256(%r14)
	movslq	32(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3264(%r14)
	movslq	36(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3272(%r14)
	movslq	40(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3280(%r14)
	movslq	44(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3288(%r14)
	movslq	48(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3296(%r14)
	movslq	52(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3304(%r14)
	movslq	56(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3312(%r14)
	movslq	60(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3320(%r14)
	movslq	64(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3328(%r14)
	movslq	68(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3336(%r14)
	movslq	72(%r13), %rcx
	movq	(%rax,%rcx,8), %rcx
	movq	%rcx, 3344(%r14)
	movslq	76(%r13), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 3352(%r14)
	movq	stderr(%rip), %rdi
	movl	$.L.str.91, %esi
	xorl	%eax, %eax
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rdx
	callq	fprintf
	movq	%r12, %rdi
	callq	fclose
	movq	%rbx, %rdi
	callq	free
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%r15, %rdi
	callq	FreeDoubleMtx
	movq	%r13, %rdi
	callq	free
	movq	%r14, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB75_47:
	movq	stderr(%rip), %rdi
	movl	$.L.str.90, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	fprintf
	callq	showaamtxexample
.LBB75_27:
	callq	showaamtxexample
.LBB75_46:
	callq	showaamtxexample
.LBB75_1:
	movq	stderr(%rip), %rcx
	movl	$.L.str.88, %edi
	movl	$45, %esi
	jmp	.LBB75_2
.LBB75_4:
	movq	stderr(%rip), %rcx
	movl	$.L.str.89, %edi
	movl	$28, %esi
.LBB75_2:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end75:
	.size	loadaamtx, .Lfunc_end75-loadaamtx
	.cfi_endproc

	.globl	miyataout_reorder_pointer
	.p2align	4, 0x90
	.type	miyataout_reorder_pointer,@function
miyataout_reorder_pointer:              # @miyataout_reorder_pointer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi848:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi849:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi850:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi851:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi852:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi853:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi854:
	.cfi_def_cfa_offset 80
.Lcfi855:
	.cfi_offset %rbx, -56
.Lcfi856:
	.cfi_offset %r12, -48
.Lcfi857:
	.cfi_offset %r13, -40
.Lcfi858:
	.cfi_offset %r14, -32
.Lcfi859:
	.cfi_offset %r15, -24
.Lcfi860:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%edx, %r12d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	$.L.str.22, %esi
	movl	$1, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.22, %esi
	movl	$1, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.92, %esi
	movl	$1, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%r12d, %ecx
	callq	fprintf
	movl	$.L.str.22, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	fprintf
	testl	%ebp, %ebp
	jle	.LBB76_6
# BB#1:                                 # %.lr.ph35.preheader
	movl	%ebp, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB76_2:                               # %.lr.ph35
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB76_4 Depth 2
	movq	80(%rsp), %rax
	movslq	(%rax,%r12,4), %r14
	movq	(%r15,%r14,8), %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r14,8), %rdx
	incq	%rdx
	movl	$.L.str.93, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ebp, %ecx
	callq	fprintf
	testl	%ebp, %ebp
	jle	.LBB76_5
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB76_2 Depth=1
	movslq	%ebp, %rbp
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB76_4:                               # %.lr.ph
                                        #   Parent Loop BB76_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15,%r14,8), %rcx
	addq	%r13, %rcx
	movl	$.L.str.24, %esi
	movl	$60, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	addq	$60, %r13
	cmpq	%rbp, %r13
	jl	.LBB76_4
.LBB76_5:                               # %._crit_edge
                                        #   in Loop: Header=BB76_2 Depth=1
	incq	%r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	jne	.LBB76_2
.LBB76_6:                               # %._crit_edge36
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end76:
	.size	miyataout_reorder_pointer, .Lfunc_end76-miyataout_reorder_pointer
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI77_0:
	.quad	4576918229304087675     # double 0.01
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI77_1:
	.long	0                       # float 0
	.text
	.globl	readmccaskill
	.p2align	4, 0x90
	.type	readmccaskill,@function
readmccaskill:                          # @readmccaskill
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi861:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi862:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi863:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi864:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi865:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi866:
	.cfi_def_cfa_offset 56
	subq	$1016, %rsp             # imm = 0x3F8
.Lcfi867:
	.cfi_def_cfa_offset 1072
.Lcfi868:
	.cfi_offset %rbx, -56
.Lcfi869:
	.cfi_offset %r12, -48
.Lcfi870:
	.cfi_offset %r13, -40
.Lcfi871:
	.cfi_offset %r14, -32
.Lcfi872:
	.cfi_offset %r15, -24
.Lcfi873:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movslq	%r15d, %rbp
	movl	$4, %esi
	movq	%rbp, %rdi
	callq	calloc
	movq	%rax, %r12
	testl	%ebp, %ebp
	jle	.LBB77_2
# BB#1:                                 # %.lr.ph50.preheader
	leal	-1(%r15), %eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	memset
.LBB77_2:                               # %._crit_edge51
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$62, %eax
	jne	.LBB77_14
# BB#3:
	leaq	16(%rsp), %rdi
	movl	$999, %esi              # imm = 0x3E7
	movq	%rbx, %rdx
	callq	fgets
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB77_13
# BB#4:                                 # %.lr.ph
	leaq	16(%rsp), %r13
	.p2align	4, 0x90
.LBB77_5:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	ungetc
	cmpl	$62, %ebp
	je	.LBB77_13
# BB#6:                                 #   in Loop: Header=BB77_5 Depth=1
	movl	$999, %esi              # imm = 0x3E7
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	fgets
	movl	$.L.str.95, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	leaq	12(%rsp), %rdx
	leaq	8(%rsp), %rcx
	leaq	4(%rsp), %r8
	callq	sscanf
	movslq	12(%rsp), %rax
	cmpl	%r15d, %eax
	jge	.LBB77_14
# BB#7:                                 #   in Loop: Header=BB77_5 Depth=1
	movl	8(%rsp), %ecx
	cmpl	%r15d, %ecx
	jge	.LBB77_14
# BB#8:                                 #   in Loop: Header=BB77_5 Depth=1
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	.LCPI77_1, %xmm0
	jbe	.LBB77_12
# BB#9:                                 #   in Loop: Header=BB77_5 Depth=1
	cmpl	%ecx, %eax
	je	.LBB77_12
# BB#10:                                #   in Loop: Header=BB77_5 Depth=1
	cvtss2sd	%xmm0, %xmm0
	movsd	.LCPI77_0(%rip), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB77_12
# BB#11:                                #   in Loop: Header=BB77_5 Depth=1
	movq	(%r14,%rax,8), %rdi
	movslq	(%r12,%rax,4), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	48(,%rax,8), %rsi
	callq	realloc
	movslq	12(%rsp), %rcx
	movq	%rax, (%r14,%rcx,8)
	movl	4(%rsp), %edx
	movslq	(%r12,%rcx,4), %rsi
	leaq	(%rsi,%rsi,2), %rsi
	movl	%edx, 20(%rax,%rsi,8)
	movslq	8(%rsp), %rdx
	movl	%edx, 16(%rax,%rsi,8)
	movslq	(%r12,%rcx,4), %rsi
	incq	%rsi
	movl	%esi, (%r12,%rcx,4)
	leaq	(%rsi,%rsi,2), %rcx
	movabsq	$-4647714811151384577, %rsi # imm = 0xBF800000FFFFFFFF
	movq	%rsi, %rbp
	movq	%rbp, 16(%rax,%rcx,8)
	movq	(%r14,%rdx,8), %rdi
	movslq	(%r12,%rdx,4), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	48(,%rax,8), %rsi
	callq	realloc
	movslq	8(%rsp), %rcx
	movq	%rax, (%r14,%rcx,8)
	movl	4(%rsp), %edx
	movslq	(%r12,%rcx,4), %rsi
	leaq	(%rsi,%rsi,2), %rsi
	movl	%edx, 20(%rax,%rsi,8)
	movl	12(%rsp), %edx
	movl	%edx, 16(%rax,%rsi,8)
	movslq	(%r12,%rcx,4), %rdx
	incq	%rdx
	movl	%edx, (%r12,%rcx,4)
	leaq	(%rdx,%rdx,2), %rcx
	movq	%rbp, 16(%rax,%rcx,8)
.LBB77_12:                              # %.backedge
                                        #   in Loop: Header=BB77_5 Depth=1
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB77_5
.LBB77_13:                              # %._crit_edge
	movq	%r12, %rdi
	callq	free
	addq	$1016, %rsp             # imm = 0x3F8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB77_14:
	movq	stderr(%rip), %rcx
	movl	$.L.str.94, %edi
	movl	$21, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end77:
	.size	readmccaskill, .Lfunc_end77-readmccaskill
	.cfi_endproc

	.globl	readpairfoldalign
	.p2align	4, 0x90
	.type	readpairfoldalign,@function
readpairfoldalign:                      # @readpairfoldalign
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi874:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi875:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi876:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi877:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi878:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi879:
	.cfi_def_cfa_offset 56
	subq	$2296, %rsp             # imm = 0x8F8
.Lcfi880:
	.cfi_def_cfa_offset 2352
.Lcfi881:
	.cfi_offset %rbx, -56
.Lcfi882:
	.cfi_offset %r12, -48
.Lcfi883:
	.cfi_offset %r13, -40
.Lcfi884:
	.cfi_offset %r14, -32
.Lcfi885:
	.cfi_offset %r15, -24
.Lcfi886:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rdi, %r15
	movq	2368(%rsp), %rax
	movq	2360(%rsp), %rcx
	movl	2352(%rsp), %ebp
	movl	2376(%rsp), %ebx
	movl	$-1, (%rcx)
	movl	$-1, (%rax)
	incl	%ebx
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, %r12
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, %r13
	leaq	288(%rsp), %rbx
	.p2align	4, 0x90
.LBB78_1:                               # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB78_3
# BB#2:                                 #   in Loop: Header=BB78_1 Depth=1
	movl	$999, %esi              # imm = 0x3E7
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	fgets
	movl	$.L.str.96, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB78_1
.LBB78_3:
	incl	%r14d
	incl	%ebp
	leaq	1296(%rsp), %rbx
	movl	$.L.str.97, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%r14d, %edx
	movl	%ebp, %ecx
	callq	sprintf
	leaq	288(%rsp), %rdi
	movq	%rbx, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB78_38
# BB#4:
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	leaq	288(%rsp), %rbx
	.p2align	4, 0x90
.LBB78_5:                               # %.preheader97
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB78_7
# BB#6:                                 #   in Loop: Header=BB78_5 Depth=1
	movl	$999, %esi              # imm = 0x3E7
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	fgets
	movl	$.L.str.100, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB78_5
.LBB78_7:                               # %.preheader96
	movq	%r15, %rdi
	callq	feof
	xorl	%r12d, %r12d
	testl	%eax, %eax
	jne	.LBB78_11
# BB#8:                                 # %.lr.ph118.preheader
	leaq	288(%rsp), %r13
	leaq	176(%rsp), %r14
	leaq	64(%rsp), %rbx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB78_9:                               # %.lr.ph118
                                        # =>This Inner Loop Header: Depth=1
	movl	$999, %esi              # imm = 0x3E7
	movq	%r13, %rdi
	movq	%r15, %rdx
	callq	fgets
	movl	$.L.str.101, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB78_11
# BB#10:                                #   in Loop: Header=BB78_9 Depth=1
	movl	$.L.str.102, %esi
	movl	$0, %eax
	movq	%r13, %rdi
	leaq	7(%rsp), %rdx
	movq	%rdx, %rcx
	movq	%r14, %r8
	movq	%rbx, %r9
	leaq	20(%rsp), %rbp
	pushq	%rbp
.Lcfi887:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi888:
	.cfi_adjust_cfa_offset 8
	callq	sscanf
	addq	$16, %rsp
.Lcfi889:
	.cfi_adjust_cfa_offset -16
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, %r12
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r14, %rdi
	callq	strtol
	decl	%eax
	movq	%r12, %rcx
	shlq	$32, %rcx
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	addq	%rdx, %rcx
	sarq	$30, %rcx
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	%eax, (%rdx,%rcx)
	movq	%r15, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB78_9
.LBB78_11:                              # %.preheader95
	leaq	288(%rsp), %rbx
	.p2align	4, 0x90
.LBB78_12:                              # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB78_14
# BB#13:                                #   in Loop: Header=BB78_12 Depth=1
	movl	$999, %esi              # imm = 0x3E7
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	fgets
	movl	$.L.str.100, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB78_12
.LBB78_14:                              # %.preheader94
	movq	%r15, %rdi
	callq	feof
	testl	%eax, %eax
	movl	%r12d, %r14d
	jne	.LBB78_18
# BB#15:                                # %.lr.ph112.preheader
	leaq	288(%rsp), %rbx
	movl	%r12d, %r14d
	.p2align	4, 0x90
.LBB78_16:                              # %.lr.ph112
                                        # =>This Inner Loop Header: Depth=1
	movl	$999, %esi              # imm = 0x3E7
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	fgets
	movl	$.L.str.101, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB78_18
# BB#17:                                #   in Loop: Header=BB78_16 Depth=1
	movl	$.L.str.102, %esi
	movl	$0, %eax
	movq	%rbx, %rdi
	leaq	7(%rsp), %rdx
	movq	%rdx, %rcx
	leaq	176(%rsp), %r13
	movq	%r13, %r8
	leaq	64(%rsp), %r14
	movq	%r14, %r9
	movq	%rbx, %rbp
	leaq	20(%rsp), %rbx
	pushq	%rbx
.Lcfi890:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	movq	%rbp, %rbx
.Lcfi891:
	.cfi_adjust_cfa_offset 8
	callq	sscanf
	addq	$16, %rsp
.Lcfi892:
	.cfi_adjust_cfa_offset -16
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	strtod
	cvttsd2si	%xmm0, %r14d
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	strtod
	cvttsd2si	%xmm0, %eax
	decl	%eax
	movslq	%r14d, %rcx
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	%eax, -4(%rdx,%rcx,4)
	movq	%r15, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB78_16
.LBB78_18:                              # %._crit_edge113
	cmpl	%r14d, %r12d
	jne	.LBB78_22
# BB#19:                                # %.preheader
	testl	%r12d, %r12d
	movq	2368(%rsp), %r11
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	40(%rsp), %r13          # 8-byte Reload
	jle	.LBB78_20
# BB#23:                                # %.lr.ph108.preheader
	leal	-1(%r12), %r8d
	movl	%r12d, %r14d
	movq	%r15, %rdx
	movq	%rdi, %rsi
	movq	%r13, %rcx
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB78_24:                              # %.lr.ph108
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rsi), %rbx
	testq	%rbx, %rbx
	movslq	(%rdx), %r10
	movb	$45, %r9b
	movb	$45, %al
	js	.LBB78_26
# BB#25:                                #   in Loop: Header=BB78_24 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax,%rbx), %eax
.LBB78_26:                              #   in Loop: Header=BB78_24 Depth=1
	movb	%al, (%rbp)
	testl	%r10d, %r10d
	js	.LBB78_28
# BB#27:                                #   in Loop: Header=BB78_24 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax,%r10), %r9d
.LBB78_28:                              #   in Loop: Header=BB78_24 Depth=1
	incq	%rbp
	movb	%r9b, (%rcx)
	incq	%rcx
	addq	$4, %rsi
	addq	$4, %rdx
	decq	%r14
	jne	.LBB78_24
# BB#29:                                # %._crit_edge109
	movq	8(%rsp), %rax           # 8-byte Reload
	movb	$0, 1(%rax,%r8)
	movb	$0, 1(%r13,%r8)
	movq	2360(%rsp), %rsi
	movl	$0, (%rsi)
	testl	%r12d, %r12d
	jle	.LBB78_21
# BB#30:                                # %.lr.ph102.preheader
	movslq	%r12d, %rax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB78_31:                              # %.lr.ph102
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rdi,%rcx,4), %edx
	movl	%edx, (%rsi)
	testl	%edx, %edx
	jns	.LBB78_33
# BB#32:                                # %.lr.ph102
                                        #   in Loop: Header=BB78_31 Depth=1
	cmpq	%rax, %rcx
	leaq	1(%rcx), %rcx
	jl	.LBB78_31
.LBB78_33:                              # %._crit_edge103
	movl	$0, (%r11)
	testl	%r12d, %r12d
	jle	.LBB78_37
# BB#34:                                # %.lr.ph.preheader
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB78_35:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r15,%rcx,4), %edx
	movl	%edx, (%r11)
	testl	%edx, %edx
	jns	.LBB78_37
# BB#36:                                # %.lr.ph
                                        #   in Loop: Header=BB78_35 Depth=1
	cmpq	%rax, %rcx
	leaq	1(%rcx), %rcx
	jl	.LBB78_35
	jmp	.LBB78_37
.LBB78_20:                              # %._crit_edge109.thread
	movq	8(%rsp), %rax           # 8-byte Reload
	movb	$0, (%rax)
	movb	$0, (%r13)
	movq	2360(%rsp), %rax
	movl	$0, (%rax)
.LBB78_21:                              # %._crit_edge103.thread
	movl	$0, (%r11)
.LBB78_37:                              # %._crit_edge
	callq	free
	movq	%r15, %rdi
	callq	free
	addq	$2296, %rsp             # imm = 0x8F8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB78_38:
	movq	stderr(%rip), %rcx
	movl	$.L.str.98, %edi
	movl	$19, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	leaq	1296(%rsp), %rdx
	leaq	288(%rsp), %rcx
	movl	$.L.str.99, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB78_22:
	movq	stderr(%rip), %rcx
	movl	$.L.str.103, %edi
	movl	$20, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end78:
	.size	readpairfoldalign, .Lfunc_end78-readpairfoldalign
	.cfi_endproc

	.p2align	4, 0x90
	.type	showaamtxexample,@function
showaamtxexample:                       # @showaamtxexample
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi893:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rcx
	movl	$.L.str.112, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.113, %edi
	movl	$11, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.114, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.115, %edi
	movl	$62, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.116, %edi
	movl	$62, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.117, %edi
	movl	$62, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.118, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.119, %edi
	movl	$62, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.120, %edi
	movl	$39, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.121, %edi
	movl	$14, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.122, %edi
	movl	$30, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.123, %edi
	movl	$39, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end79:
	.size	showaamtxexample, .Lfunc_end79-showaamtxexample
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"localhompt = %p\n"
	.size	.L.str, 17

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"tmppt = %p\n"
	.size	.L.str.1, 12

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"subnosento = %p\n"
	.size	.L.str.2, 17

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"sumscore = %f\n"
	.size	.L.str.3, 15

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"tmpptr->opt = %f\n"
	.size	.L.str.4, 18

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%s\n"
	.size	.L.str.5, 4

	.type	upperCase,@object       # @upperCase
	.local	upperCase
	.comm	upperCase,4,4
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"TOO LONG SEQUENCE!\n"
	.size	.L.str.7, 20

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"TOO MANY SEQUENCE!\n"
	.size	.L.str.8, 20

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%d > %d\n"
	.size	.L.str.9, 9

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"onnet"
	.size	.L.str.10, 6

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"DnA"
	.size	.L.str.11, 4

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"dna"
	.size	.L.str.12, 4

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"DNA"
	.size	.L.str.13, 4

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"M-Y"
	.size	.L.str.14, 4

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"iyata"
	.size	.L.str.15, 6

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"reallocating...\n"
	.size	.L.str.16, 17

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Allocation error in load1SeqWithoutName_realloc \n"
	.size	.L.str.17, 50

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"done.\n"
	.size	.L.str.18, 7

	.type	readDataforgaln.tmpseq,@object # @readDataforgaln.tmpseq
	.local	readDataforgaln.tmpseq
	.comm	readDataforgaln.tmpseq,8,8
	.type	readData_varlen.tmpseq,@object # @readData_varlen.tmpseq
	.local	readData_varlen.tmpseq
	.comm	readData_varlen.tmpseq,8,8
	.type	readData_pointer.tmpseq,@object # @readData_pointer.tmpseq
	.local	readData_pointer.tmpseq
	.comm	readData_pointer.tmpseq,8,8
	.type	readData.tmpseq,@object # @readData.tmpseq
	.local	readData.tmpseq
	.comm	readData.tmpseq,8,8
	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"##### atgcfreq = %f\n"
	.size	.L.str.19, 21

	.type	WriteGapFill.b,@object  # @WriteGapFill.b
	.local	WriteGapFill.b
	.comm	WriteGapFill.b,5000000,16
	.type	WriteGapFill.gap,@object # @WriteGapFill.gap
	.local	WriteGapFill.gap
	.comm	WriteGapFill.gap,5000000,16
	.type	WriteGapFill.buff,@object # @WriteGapFill.buff
	.local	WriteGapFill.buff
	.comm	WriteGapFill.buff,5000000,16
	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"%5d"
	.size	.L.str.20, 4

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"%5d\n"
	.size	.L.str.22, 5

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	">%s\n"
	.size	.L.str.23, 5

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"%.*s\n"
	.size	.L.str.24, 6

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"hat2 is wrong."
	.size	.L.str.25, 15

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	" %#6.3f\n"
	.size	.L.str.26, 9

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"%4d. %s\n"
	.size	.L.str.27, 9

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"%#6.3f"
	.size	.L.str.28, 7

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"%d-%d d=%.3f\n"
	.size	.L.str.29, 14

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"+==========+"
	.size	.L.str.30, 13

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"%d"
	.size	.L.str.31, 3

	.type	ReadBlastm7_avscore.qal,@object # @ReadBlastm7_avscore.qal
	.local	ReadBlastm7_avscore.qal
	.comm	ReadBlastm7_avscore.qal,5000000,16
	.type	ReadBlastm7_avscore.tal,@object # @ReadBlastm7_avscore.tal
	.local	ReadBlastm7_avscore.tal
	.comm	ReadBlastm7_avscore.tal,5000000,16
	.type	ReadBlastm7_avscore.al,@object # @ReadBlastm7_avscore.al
	.local	ReadBlastm7_avscore.al
	.comm	ReadBlastm7_avscore.al,5000000,16
	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"          <Hit_def>"
	.size	.L.str.32, 20

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"              <Hsp_num>"
	.size	.L.str.33, 24

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"              <Hsp_score>"
	.size	.L.str.34, 26

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"              <Hsp_query-from>"
	.size	.L.str.35, 31

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"              <Hsp_query-to>"
	.size	.L.str.36, 29

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"              <Hsp_hit-from>"
	.size	.L.str.37, 29

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"              <Hsp_hit-to>"
	.size	.L.str.38, 27

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"              <Hsp_align-len>"
	.size	.L.str.39, 30

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"              <Hsp_qseq>"
	.size	.L.str.40, 25

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"              <Hsp_hseq>"
	.size	.L.str.41, 25

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"            </Hsp>:"
	.size	.L.str.42, 20

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"          </Hit_hsps>"
	.size	.L.str.43, 22

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"ERROR! sumscore=%f, sumlen=%f, and scorepersite=%f\n"
	.size	.L.str.44, 52

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"      </Iteration_hits>"
	.size	.L.str.45, 24

	.type	ReadBlastm7_scoreonly.qal,@object # @ReadBlastm7_scoreonly.qal
	.local	ReadBlastm7_scoreonly.qal
	.comm	ReadBlastm7_scoreonly.qal,5000000,16
	.type	ReadBlastm7_scoreonly.tal,@object # @ReadBlastm7_scoreonly.tal
	.local	ReadBlastm7_scoreonly.tal
	.comm	ReadBlastm7_scoreonly.tal,5000000,16
	.type	ReadBlastm7_scoreonly.al,@object # @ReadBlastm7_scoreonly.al
	.local	ReadBlastm7_scoreonly.al
	.comm	ReadBlastm7_scoreonly.al,5000000,16
	.type	ReadBlastm7.junban,@object # @ReadBlastm7.junban
	.local	ReadBlastm7.junban
	.comm	ReadBlastm7.junban,200000,16
	.type	ReadBlastm7.qal,@object # @ReadBlastm7.qal
	.local	ReadBlastm7.qal
	.comm	ReadBlastm7.qal,5000000,16
	.type	ReadBlastm7.tal,@object # @ReadBlastm7.tal
	.local	ReadBlastm7.tal
	.comm	ReadBlastm7.tal,5000000,16
	.type	ReadBlastm7.al,@object  # @ReadBlastm7.al
	.local	ReadBlastm7.al
	.comm	ReadBlastm7.al,5000000,16
	.type	ReadFasta34noalign.junban,@object # @ReadFasta34noalign.junban
	.local	ReadFasta34noalign.junban
	.comm	ReadFasta34noalign.junban,200000,16
	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"%d %lf %lf"
	.size	.L.str.46, 11

	.type	ReadFasta34m10_nuc.junban,@object # @ReadFasta34m10_nuc.junban
	.local	ReadFasta34m10_nuc.junban
	.comm	ReadFasta34m10_nuc.junban,200000,16
	.type	ReadFasta34m10_nuc.qal,@object # @ReadFasta34m10_nuc.qal
	.local	ReadFasta34m10_nuc.qal
	.comm	ReadFasta34m10_nuc.qal,5000000,16
	.type	ReadFasta34m10_nuc.tal,@object # @ReadFasta34m10_nuc.tal
	.local	ReadFasta34m10_nuc.tal
	.comm	ReadFasta34m10_nuc.tal,5000000,16
	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	">>+==========+"
	.size	.L.str.47, 15

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"; fa_opt:"
	.size	.L.str.48, 10

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"; sw_s-w opt:"
	.size	.L.str.49, 14

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"_overlap:"
	.size	.L.str.51, 10

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"_start:"
	.size	.L.str.52, 8

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"_stop:"
	.size	.L.str.53, 7

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"_display_start:"
	.size	.L.str.54, 16

	.type	ReadFasta34m10.junban,@object # @ReadFasta34m10.junban
	.local	ReadFasta34m10.junban
	.comm	ReadFasta34m10.junban,200000,16
	.type	ReadFasta34m10.qal,@object # @ReadFasta34m10.qal
	.local	ReadFasta34m10.qal
	.comm	ReadFasta34m10.qal,5000000,16
	.type	ReadFasta34m10.tal,@object # @ReadFasta34m10.tal
	.local	ReadFasta34m10.tal
	.comm	ReadFasta34m10.tal,5000000,16
	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"+===========+"
	.size	.L.str.55, 14

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	">>><<<"
	.size	.L.str.56, 7

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	">>>"
	.size	.L.str.57, 4

	.type	ReadFasta34.junban,@object # @ReadFasta34.junban
	.local	ReadFasta34.junban
	.comm	ReadFasta34.junban,200000,16
	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"opt: "
	.size	.L.str.58, 6

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"ungapped) in "
	.size	.L.str.59, 14

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"pt = %s, overlapaa = %d\n"
	.size	.L.str.60, 25

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"overlap ("
	.size	.L.str.61, 10

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"(%d-%d:%d-%d)"
	.size	.L.str.62, 14

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"count = %d\n"
	.size	.L.str.63, 12

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"%d %d %d %lf"
	.size	.L.str.64, 13

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"%d %d %d"
	.size	.L.str.65, 9

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"pre"
	.size	.L.str.66, 4

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"r"
	.size	.L.str.67, 2

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"Cannot open pre.\n"
	.size	.L.str.68, 18

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"/tmp/pre.%d"
	.size	.L.str.69, 12

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"w"
	.size	.L.str.70, 2

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"Cannot open pre"
	.size	.L.str.71, 16

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"trace"
	.size	.L.str.72, 6

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"cannot open trace"
	.size	.L.str.73, 18

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"PID = %d\n"
	.size	.L.str.74, 10

	.type	WriteForFasta.b,@object # @WriteForFasta.b
	.local	WriteForFasta.b
	.comm	WriteForFasta.b,5000000,16
	.type	readlocalhomtable2.buff,@object # @readlocalhomtable2.buff
	.local	readlocalhomtable2.buff
	.comm	readlocalhomtable2.buff,256,16
	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"%d %d %d %lf %d %d %d %d"
	.size	.L.str.75, 25

	.type	readlocalhomtable.buff,@object # @readlocalhomtable.buff
	.local	readlocalhomtable.buff
	.comm	readlocalhomtable.buff,256,16
	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"%d-%d\n"
	.size	.L.str.76, 7

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"reg1=%d-%d, reg2=%d-%d, imp=%f, opt=%f\n"
	.size	.L.str.77, 40

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"reg1=%d-%d, reg2=%d-%d, imp=%f, opt=%f, wimp=%f\n"
	.size	.L.str.78, 49

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"freeing localhom\n"
	.size	.L.str.79, 18

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"CLUSTAL format alignment by MAFFT (v%s)\n\n"
	.size	.L.str.80, 42

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"6.624b"
	.size	.L.str.81, 7

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"CLUSTAL format alignment by MAFFT %s (v%s)\n\n"
	.size	.L.str.82, 45

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"%-15.15s "
	.size	.L.str.83, 10

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"%.60s\n"
	.size	.L.str.84, 7

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.zero	1
	.size	.L.str.85, 1

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"ARNDCQEGHILKMFPSTWYV"
	.size	.L.str.86, 21

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"_aamtx"
	.size	.L.str.87, 7

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"User-defined matrix is not supported for DNA\n"
	.size	.L.str.88, 46

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"Cannot open the _aamtx file\n"
	.size	.L.str.89, 29

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"%c: not found in the first 20 letters.\n"
	.size	.L.str.90, 40

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"inorder = %s\n"
	.size	.L.str.91, 14

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"%5d%5d\n"
	.size	.L.str.92, 8

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"=%s\n%d\n"
	.size	.L.str.93, 8

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"format error in hat4\n"
	.size	.L.str.94, 22

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"%d %d %f"
	.size	.L.str.95, 9

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"; ALIGNING"
	.size	.L.str.96, 11

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"; ALIGNING            %d against %d\n"
	.size	.L.str.97, 37

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"Error in FOLDALIGN\n"
	.size	.L.str.98, 20

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"qstr = %s, but gett = %s\n"
	.size	.L.str.99, 26

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"; --------"
	.size	.L.str.100, 11

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"; ********"
	.size	.L.str.101, 11

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"%c %c %s %s %d %d"
	.size	.L.str.102, 18

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"Error in foldalign?\n"
	.size	.L.str.103, 21

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"pt1 = \n%s\n, pt2 = \n%s\n"
	.size	.L.str.104, 23

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"tmppt = %p, localhompt = %p\n"
	.size	.L.str.105, 29

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"In in while loop\n"
	.size	.L.str.106, 18

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"pt = %c, %c, st=%d\n"
	.size	.L.str.107, 20

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"score (1)= %f\n"
	.size	.L.str.108, 15

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"al1: %d - %d\n"
	.size	.L.str.109, 14

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"al2: %d - %d\n"
	.size	.L.str.110, 14

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"score (2)= %f\n"
	.size	.L.str.111, 15

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"Format error in aa matrix\n"
	.size	.L.str.112, 27

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"# Example:\n"
	.size	.L.str.113, 12

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"# comment\n"
	.size	.L.str.114, 11

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"   A  R  N  D  C  Q  E  G  H  I  L  K  M  F  P  S  T  W  Y  V\n"
	.size	.L.str.115, 63

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"A  4 -1 -2 -2  0 -1 -1  0 -2 -1 -1 -1 -1 -2 -1  1  0 -3 -2  0\n"
	.size	.L.str.116, 63

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"R -1  5  0 -2 -3  1  0 -2  0 -3 -2  2 -1 -3 -2 -1 -1 -3 -2 -3\n"
	.size	.L.str.117, 63

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"...\n"
	.size	.L.str.118, 5

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"V  0 -3 -3 -3 -1 -2 -2 -3 -3  3  1 -2  1 -1 -2 -2  0 -3 -1  4\n"
	.size	.L.str.119, 63

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"frequency 0.07 0.05 0.04 0.05 0.02 .. \n"
	.size	.L.str.120, 40

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"# Example end\n"
	.size	.L.str.121, 15

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"Only the lower half is loaded\n"
	.size	.L.str.122, 31

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"The last line (frequency) is optional.\n"
	.size	.L.str.123, 40


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
