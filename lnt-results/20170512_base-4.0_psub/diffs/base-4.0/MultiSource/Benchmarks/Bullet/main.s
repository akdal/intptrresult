	.text
	.file	"main.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$1056, %rsp             # imm = 0x420
.Lcfi3:
	.cfi_def_cfa_offset 1088
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movb	$1, gDisableDeactivation(%rip)
	movb	$1, 960(%rsp)
	movq	$0, 952(%rsp)
	movl	$0, 940(%rsp)
	movl	$0, 944(%rsp)
	movb	$1, 992(%rsp)
	movq	$0, 984(%rsp)
	movl	$0, 972(%rsp)
	movl	$0, 976(%rsp)
	movl	$1, 1032(%rsp)
	movq	$_ZTV14BenchmarkDemo1+16, 920(%rsp)
	movb	$1, 824(%rsp)
	movq	$0, 816(%rsp)
	movl	$0, 804(%rsp)
	movl	$0, 808(%rsp)
	movb	$1, 856(%rsp)
	movq	$0, 848(%rsp)
	movl	$0, 836(%rsp)
	movl	$0, 840(%rsp)
	movl	$2, 896(%rsp)
	movq	$_ZTV14BenchmarkDemo2+16, 784(%rsp)
	movb	$1, 688(%rsp)
	movq	$0, 680(%rsp)
	movl	$0, 668(%rsp)
	movl	$0, 672(%rsp)
	movb	$1, 720(%rsp)
	movq	$0, 712(%rsp)
	movl	$0, 700(%rsp)
	movl	$0, 704(%rsp)
	movl	$3, 760(%rsp)
	movq	$_ZTV14BenchmarkDemo3+16, 648(%rsp)
	movb	$1, 552(%rsp)
	movq	$0, 544(%rsp)
	movl	$0, 532(%rsp)
	movl	$0, 536(%rsp)
	movb	$1, 584(%rsp)
	movq	$0, 576(%rsp)
	movl	$0, 564(%rsp)
	movl	$0, 568(%rsp)
	movl	$4, 624(%rsp)
	movq	$_ZTV14BenchmarkDemo4+16, 512(%rsp)
	movb	$1, 416(%rsp)
	movq	$0, 408(%rsp)
	movl	$0, 396(%rsp)
	movl	$0, 400(%rsp)
	movb	$1, 448(%rsp)
	movq	$0, 440(%rsp)
	movl	$0, 428(%rsp)
	movl	$0, 432(%rsp)
	movl	$5, 488(%rsp)
	movq	$_ZTV14BenchmarkDemo5+16, 376(%rsp)
	movb	$1, 280(%rsp)
	movq	$0, 272(%rsp)
	movl	$0, 260(%rsp)
	movl	$0, 264(%rsp)
	movb	$1, 312(%rsp)
	movq	$0, 304(%rsp)
	movl	$0, 292(%rsp)
	movl	$0, 296(%rsp)
	movl	$6, 352(%rsp)
	movq	$_ZTV14BenchmarkDemo6+16, 240(%rsp)
	movb	$1, 144(%rsp)
	movq	$0, 136(%rsp)
	movl	$0, 124(%rsp)
	movl	$0, 128(%rsp)
	movb	$1, 176(%rsp)
	movq	$0, 168(%rsp)
	movl	$0, 156(%rsp)
	movl	$0, 160(%rsp)
	movl	$7, 216(%rsp)
	movq	$_ZTV14BenchmarkDemo7+16, 104(%rsp)
	leaq	920(%rsp), %r14
	movq	%r14, 48(%rsp)
	leaq	784(%rsp), %rax
	movq	%rax, 56(%rsp)
	leaq	648(%rsp), %rax
	movq	%rax, 64(%rsp)
	leaq	512(%rsp), %rax
	movq	%rax, 72(%rsp)
	leaq	376(%rsp), %rax
	movq	%rax, 80(%rsp)
	leaq	240(%rsp), %rax
	movq	%rax, 88(%rsp)
	leaq	104(%rsp), %rax
	movq	%rax, 96(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 28(%rsp)
	movaps	%xmm0, 16(%rsp)
	xorl	%ebx, %ebx
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_8:                                #   in Loop: Header=BB0_1 Depth=1
	leaq	1(%rbx), %rax
	cmpq	$6, %rax
	jg	.LBB0_13
# BB#9:                                 # %._crit_edge
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	56(%rsp,%rbx,8), %r14
	movq	%rax, %rbx
.LBB0_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_3 Depth 2
.Ltmp0:
	movq	%r14, %rdi
	callq	_ZN13BenchmarkDemo11initPhysicsEv
.Ltmp1:
# BB#2:                                 # %.preheader
                                        #   in Loop: Header=BB0_1 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_3:                                #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rax
.Ltmp3:
	movq	%r14, %rdi
	callq	*32(%rax)
.Ltmp4:
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=2
.Ltmp5:
	callq	_ZN15CProfileManager20Get_Time_Since_ResetEv
.Ltmp6:
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=2
	movslq	%ebp, %rax
	imulq	$1374389535, %rax, %rcx # imm = 0x51EB851F
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	leal	(%rcx,%rcx,4), %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	jne	.LBB0_7
# BB#6:                                 #   in Loop: Header=BB0_3 Depth=2
	movq	.L_ZZ4mainE9demoNames(,%rbx,8), %rsi
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movl	%ebp, %edx
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	callq	printf
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
.LBB0_7:                                #   in Loop: Header=BB0_3 Depth=2
	addss	16(%rsp,%rbx,4), %xmm0
	movss	%xmm0, 16(%rsp,%rbx,4)
	incl	%ebp
	cmpl	$100, %ebp
	jl	.LBB0_3
	jmp	.LBB0_8
.LBB0_13:
.Ltmp10:
	leaq	104(%rsp), %rdi
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp11:
# BB#14:
.Ltmp15:
	leaq	240(%rsp), %rdi
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp16:
# BB#15:
.Ltmp20:
	leaq	376(%rsp), %rdi
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp21:
# BB#16:
.Ltmp25:
	leaq	512(%rsp), %rdi
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp26:
# BB#17:
.Ltmp30:
	leaq	648(%rsp), %rdi
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp31:
# BB#18:
.Ltmp35:
	leaq	784(%rsp), %rdi
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp36:
# BB#19:
	leaq	920(%rsp), %rdi
	callq	_ZN13BenchmarkDemoD2Ev
	xorl	%eax, %eax
	addq	$1056, %rsp             # imm = 0x420
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB0_28:
.Ltmp37:
	movq	%rax, %rbx
	jmp	.LBB0_31
.LBB0_29:
.Ltmp32:
	movq	%rax, %rbx
	jmp	.LBB0_30
.LBB0_26:
.Ltmp27:
	movq	%rax, %rbx
	jmp	.LBB0_27
.LBB0_24:
.Ltmp22:
	movq	%rax, %rbx
	jmp	.LBB0_25
.LBB0_22:
.Ltmp17:
	movq	%rax, %rbx
	jmp	.LBB0_23
.LBB0_20:
.Ltmp12:
	movq	%rax, %rbx
	jmp	.LBB0_21
.LBB0_11:
.Ltmp2:
	jmp	.LBB0_12
.LBB0_10:
.Ltmp7:
.LBB0_12:
	movq	%rax, %rbx
.Ltmp8:
	leaq	104(%rsp), %rdi
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp9:
.LBB0_21:
.Ltmp13:
	leaq	240(%rsp), %rdi
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp14:
.LBB0_23:
.Ltmp18:
	leaq	376(%rsp), %rdi
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp19:
.LBB0_25:
.Ltmp23:
	leaq	512(%rsp), %rdi
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp24:
.LBB0_27:
.Ltmp28:
	leaq	648(%rsp), %rdi
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp29:
.LBB0_30:
.Ltmp33:
	leaq	784(%rsp), %rdi
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp34:
.LBB0_31:
.Ltmp38:
	leaq	920(%rsp), %rdi
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp39:
# BB#32:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_33:
.Ltmp40:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp3           #   Call between .Ltmp3 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin0   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin0   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin0   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp8-.Ltmp36          #   Call between .Ltmp36 and .Ltmp8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 10 <<
	.long	.Ltmp39-.Ltmp8          #   Call between .Ltmp8 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin0   #     jumps to .Ltmp40
	.byte	1                       #   On action: 1
	.long	.Ltmp39-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Lfunc_end0-.Ltmp39     #   Call between .Ltmp39 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.section	.text._ZN13BenchmarkDemoD2Ev,"axG",@progbits,_ZN13BenchmarkDemoD2Ev,comdat
	.weak	_ZN13BenchmarkDemoD2Ev
	.p2align	4, 0x90
	.type	_ZN13BenchmarkDemoD2Ev,@function
_ZN13BenchmarkDemoD2Ev:                 # @_ZN13BenchmarkDemoD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13BenchmarkDemo+16, (%rbx)
.Ltmp41:
	callq	_ZN13BenchmarkDemo11exitPhysicsEv
.Ltmp42:
# BB#1:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_5
# BB#2:
	cmpb	$0, 72(%rbx)
	je	.LBB2_4
# BB#3:
.Ltmp46:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp47:
.LBB2_4:                                # %.noexc
	movq	$0, 64(%rbx)
.LBB2_5:
	movb	$1, 72(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 52(%rbx)
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_9
# BB#6:
	cmpb	$0, 40(%rbx)
	je	.LBB2_8
# BB#7:
	callq	_Z21btAlignedFreeInternalPv
.LBB2_8:
	movq	$0, 32(%rbx)
.LBB2_9:                                # %_ZN20btAlignedObjectArrayIP16btCollisionShapeED2Ev.exit
	movb	$1, 40(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 20(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_15:
.Ltmp48:
	movq	%rax, %r14
	jmp	.LBB2_16
.LBB2_10:
.Ltmp43:
	movq	%rax, %r14
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_14
# BB#11:
	cmpb	$0, 72(%rbx)
	je	.LBB2_13
# BB#12:
.Ltmp44:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp45:
.LBB2_13:                               # %.noexc5
	movq	$0, 64(%rbx)
.LBB2_14:                               # %_ZN20btAlignedObjectArrayIP7RagDollED2Ev.exit6
	movb	$1, 72(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 52(%rbx)
.LBB2_16:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_20
# BB#17:
	cmpb	$0, 40(%rbx)
	je	.LBB2_19
# BB#18:
.Ltmp49:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp50:
.LBB2_19:                               # %.noexc8
	movq	$0, 32(%rbx)
.LBB2_20:
	movb	$1, 40(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 20(%rbx)
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_21:
.Ltmp51:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN13BenchmarkDemoD2Ev, .Lfunc_end2-_ZN13BenchmarkDemoD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp41-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin1   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin1   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp44-.Ltmp47         #   Call between .Ltmp47 and .Ltmp44
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp50-.Ltmp44         #   Call between .Ltmp44 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin1   #     jumps to .Ltmp51
	.byte	1                       #   On action: 1
	.long	.Ltmp50-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end2-.Ltmp50     #   Call between .Ltmp50 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN15DemoApplication6myinitEv,"axG",@progbits,_ZN15DemoApplication6myinitEv,comdat
	.weak	_ZN15DemoApplication6myinitEv
	.p2align	4, 0x90
	.type	_ZN15DemoApplication6myinitEv,@function
_ZN15DemoApplication6myinitEv:          # @_ZN15DemoApplication6myinitEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end3:
	.size	_ZN15DemoApplication6myinitEv, .Lfunc_end3-_ZN15DemoApplication6myinitEv
	.cfi_endproc

	.section	.text._ZN15DemoApplication16getDynamicsWorldEv,"axG",@progbits,_ZN15DemoApplication16getDynamicsWorldEv,comdat
	.weak	_ZN15DemoApplication16getDynamicsWorldEv
	.p2align	4, 0x90
	.type	_ZN15DemoApplication16getDynamicsWorldEv,@function
_ZN15DemoApplication16getDynamicsWorldEv: # @_ZN15DemoApplication16getDynamicsWorldEv
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	retq
.Lfunc_end4:
	.size	_ZN15DemoApplication16getDynamicsWorldEv, .Lfunc_end4-_ZN15DemoApplication16getDynamicsWorldEv
	.cfi_endproc

	.section	.text._ZN14BenchmarkDemo1D0Ev,"axG",@progbits,_ZN14BenchmarkDemo1D0Ev,comdat
	.weak	_ZN14BenchmarkDemo1D0Ev
	.p2align	4, 0x90
	.type	_ZN14BenchmarkDemo1D0Ev,@function
_ZN14BenchmarkDemo1D0Ev:                # @_ZN14BenchmarkDemo1D0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -24
.Lcfi16:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp52:
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp53:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB5_2:
.Ltmp54:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZN14BenchmarkDemo1D0Ev, .Lfunc_end5-_ZN14BenchmarkDemo1D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp52-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin2   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end5-.Ltmp53     #   Call between .Ltmp53 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN14BenchmarkDemo2D0Ev,"axG",@progbits,_ZN14BenchmarkDemo2D0Ev,comdat
	.weak	_ZN14BenchmarkDemo2D0Ev
	.p2align	4, 0x90
	.type	_ZN14BenchmarkDemo2D0Ev,@function
_ZN14BenchmarkDemo2D0Ev:                # @_ZN14BenchmarkDemo2D0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -24
.Lcfi21:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp55:
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp56:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB6_2:
.Ltmp57:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN14BenchmarkDemo2D0Ev, .Lfunc_end6-_ZN14BenchmarkDemo2D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp55-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin3   #     jumps to .Ltmp57
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Lfunc_end6-.Ltmp56     #   Call between .Ltmp56 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN14BenchmarkDemo3D0Ev,"axG",@progbits,_ZN14BenchmarkDemo3D0Ev,comdat
	.weak	_ZN14BenchmarkDemo3D0Ev
	.p2align	4, 0x90
	.type	_ZN14BenchmarkDemo3D0Ev,@function
_ZN14BenchmarkDemo3D0Ev:                # @_ZN14BenchmarkDemo3D0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -24
.Lcfi26:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp58:
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp59:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB7_2:
.Ltmp60:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN14BenchmarkDemo3D0Ev, .Lfunc_end7-_ZN14BenchmarkDemo3D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp58-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin4   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end7-.Ltmp59     #   Call between .Ltmp59 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN14BenchmarkDemo4D0Ev,"axG",@progbits,_ZN14BenchmarkDemo4D0Ev,comdat
	.weak	_ZN14BenchmarkDemo4D0Ev
	.p2align	4, 0x90
	.type	_ZN14BenchmarkDemo4D0Ev,@function
_ZN14BenchmarkDemo4D0Ev:                # @_ZN14BenchmarkDemo4D0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -24
.Lcfi31:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp61:
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp62:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB8_2:
.Ltmp63:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN14BenchmarkDemo4D0Ev, .Lfunc_end8-_ZN14BenchmarkDemo4D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp61-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin5   #     jumps to .Ltmp63
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Lfunc_end8-.Ltmp62     #   Call between .Ltmp62 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN14BenchmarkDemo5D0Ev,"axG",@progbits,_ZN14BenchmarkDemo5D0Ev,comdat
	.weak	_ZN14BenchmarkDemo5D0Ev
	.p2align	4, 0x90
	.type	_ZN14BenchmarkDemo5D0Ev,@function
_ZN14BenchmarkDemo5D0Ev:                # @_ZN14BenchmarkDemo5D0Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -24
.Lcfi36:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp64:
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp65:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB9_2:
.Ltmp66:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end9:
	.size	_ZN14BenchmarkDemo5D0Ev, .Lfunc_end9-_ZN14BenchmarkDemo5D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp64-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp65-.Ltmp64         #   Call between .Ltmp64 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin6   #     jumps to .Ltmp66
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Lfunc_end9-.Ltmp65     #   Call between .Ltmp65 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN14BenchmarkDemo6D0Ev,"axG",@progbits,_ZN14BenchmarkDemo6D0Ev,comdat
	.weak	_ZN14BenchmarkDemo6D0Ev
	.p2align	4, 0x90
	.type	_ZN14BenchmarkDemo6D0Ev,@function
_ZN14BenchmarkDemo6D0Ev:                # @_ZN14BenchmarkDemo6D0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 32
.Lcfi40:
	.cfi_offset %rbx, -24
.Lcfi41:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp67:
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp68:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB10_2:
.Ltmp69:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZN14BenchmarkDemo6D0Ev, .Lfunc_end10-_ZN14BenchmarkDemo6D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp67-.Lfunc_begin7   # >> Call Site 1 <<
	.long	.Ltmp68-.Ltmp67         #   Call between .Ltmp67 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin7   #     jumps to .Ltmp69
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Lfunc_end10-.Ltmp68    #   Call between .Ltmp68 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN14BenchmarkDemo7D0Ev,"axG",@progbits,_ZN14BenchmarkDemo7D0Ev,comdat
	.weak	_ZN14BenchmarkDemo7D0Ev
	.p2align	4, 0x90
	.type	_ZN14BenchmarkDemo7D0Ev,@function
_ZN14BenchmarkDemo7D0Ev:                # @_ZN14BenchmarkDemo7D0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 32
.Lcfi45:
	.cfi_offset %rbx, -24
.Lcfi46:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp70:
	callq	_ZN13BenchmarkDemoD2Ev
.Ltmp71:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB11_2:
.Ltmp72:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_ZN14BenchmarkDemo7D0Ev, .Lfunc_end11-_ZN14BenchmarkDemo7D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp70-.Lfunc_begin8   # >> Call Site 1 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp72-.Lfunc_begin8   #     jumps to .Ltmp72
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin8   # >> Call Site 2 <<
	.long	.Lfunc_end11-.Ltmp71    #   Call between .Ltmp71 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"3000 fall"
	.size	.L.str, 10

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"1000 stack"
	.size	.L.str.1, 11

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"136 ragdolls"
	.size	.L.str.2, 13

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"1000 convex"
	.size	.L.str.3, 12

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"prim-trimesh"
	.size	.L.str.4, 13

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"convex-trimesh"
	.size	.L.str.5, 15

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"raytests"
	.size	.L.str.6, 9

	.type	.L_ZZ4mainE9demoNames,@object # @_ZZ4mainE9demoNames
	.section	.rodata,"a",@progbits
	.p2align	4
.L_ZZ4mainE9demoNames:
	.quad	.L.str
	.quad	.L.str.1
	.quad	.L.str.2
	.quad	.L.str.3
	.quad	.L.str.4
	.quad	.L.str.5
	.quad	.L.str.6
	.size	.L_ZZ4mainE9demoNames, 56

	.type	.L.str.7,@object        # @.str.7
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.7:
	.asciz	"BenchmarkDemo: %s, Frame: %d\n"
	.size	.L.str.7, 30

	.type	_ZTV14BenchmarkDemo1,@object # @_ZTV14BenchmarkDemo1
	.section	.rodata._ZTV14BenchmarkDemo1,"aG",@progbits,_ZTV14BenchmarkDemo1,comdat
	.weak	_ZTV14BenchmarkDemo1
	.p2align	3
_ZTV14BenchmarkDemo1:
	.quad	0
	.quad	_ZTI14BenchmarkDemo1
	.quad	_ZN15DemoApplication6myinitEv
	.quad	_ZN15DemoApplication16getDynamicsWorldEv
	.quad	_ZN13BenchmarkDemoD2Ev
	.quad	_ZN14BenchmarkDemo1D0Ev
	.quad	_ZN13BenchmarkDemo20clientMoveAndDisplayEv
	.quad	_ZN13BenchmarkDemo15displayCallbackEv
	.size	_ZTV14BenchmarkDemo1, 64

	.type	_ZTS14BenchmarkDemo1,@object # @_ZTS14BenchmarkDemo1
	.section	.rodata._ZTS14BenchmarkDemo1,"aG",@progbits,_ZTS14BenchmarkDemo1,comdat
	.weak	_ZTS14BenchmarkDemo1
	.p2align	4
_ZTS14BenchmarkDemo1:
	.asciz	"14BenchmarkDemo1"
	.size	_ZTS14BenchmarkDemo1, 17

	.type	_ZTI14BenchmarkDemo1,@object # @_ZTI14BenchmarkDemo1
	.section	.rodata._ZTI14BenchmarkDemo1,"aG",@progbits,_ZTI14BenchmarkDemo1,comdat
	.weak	_ZTI14BenchmarkDemo1
	.p2align	4
_ZTI14BenchmarkDemo1:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14BenchmarkDemo1
	.quad	_ZTI13BenchmarkDemo
	.size	_ZTI14BenchmarkDemo1, 24

	.type	_ZTV14BenchmarkDemo2,@object # @_ZTV14BenchmarkDemo2
	.section	.rodata._ZTV14BenchmarkDemo2,"aG",@progbits,_ZTV14BenchmarkDemo2,comdat
	.weak	_ZTV14BenchmarkDemo2
	.p2align	3
_ZTV14BenchmarkDemo2:
	.quad	0
	.quad	_ZTI14BenchmarkDemo2
	.quad	_ZN15DemoApplication6myinitEv
	.quad	_ZN15DemoApplication16getDynamicsWorldEv
	.quad	_ZN13BenchmarkDemoD2Ev
	.quad	_ZN14BenchmarkDemo2D0Ev
	.quad	_ZN13BenchmarkDemo20clientMoveAndDisplayEv
	.quad	_ZN13BenchmarkDemo15displayCallbackEv
	.size	_ZTV14BenchmarkDemo2, 64

	.type	_ZTS14BenchmarkDemo2,@object # @_ZTS14BenchmarkDemo2
	.section	.rodata._ZTS14BenchmarkDemo2,"aG",@progbits,_ZTS14BenchmarkDemo2,comdat
	.weak	_ZTS14BenchmarkDemo2
	.p2align	4
_ZTS14BenchmarkDemo2:
	.asciz	"14BenchmarkDemo2"
	.size	_ZTS14BenchmarkDemo2, 17

	.type	_ZTI14BenchmarkDemo2,@object # @_ZTI14BenchmarkDemo2
	.section	.rodata._ZTI14BenchmarkDemo2,"aG",@progbits,_ZTI14BenchmarkDemo2,comdat
	.weak	_ZTI14BenchmarkDemo2
	.p2align	4
_ZTI14BenchmarkDemo2:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14BenchmarkDemo2
	.quad	_ZTI13BenchmarkDemo
	.size	_ZTI14BenchmarkDemo2, 24

	.type	_ZTV14BenchmarkDemo3,@object # @_ZTV14BenchmarkDemo3
	.section	.rodata._ZTV14BenchmarkDemo3,"aG",@progbits,_ZTV14BenchmarkDemo3,comdat
	.weak	_ZTV14BenchmarkDemo3
	.p2align	3
_ZTV14BenchmarkDemo3:
	.quad	0
	.quad	_ZTI14BenchmarkDemo3
	.quad	_ZN15DemoApplication6myinitEv
	.quad	_ZN15DemoApplication16getDynamicsWorldEv
	.quad	_ZN13BenchmarkDemoD2Ev
	.quad	_ZN14BenchmarkDemo3D0Ev
	.quad	_ZN13BenchmarkDemo20clientMoveAndDisplayEv
	.quad	_ZN13BenchmarkDemo15displayCallbackEv
	.size	_ZTV14BenchmarkDemo3, 64

	.type	_ZTS14BenchmarkDemo3,@object # @_ZTS14BenchmarkDemo3
	.section	.rodata._ZTS14BenchmarkDemo3,"aG",@progbits,_ZTS14BenchmarkDemo3,comdat
	.weak	_ZTS14BenchmarkDemo3
	.p2align	4
_ZTS14BenchmarkDemo3:
	.asciz	"14BenchmarkDemo3"
	.size	_ZTS14BenchmarkDemo3, 17

	.type	_ZTI14BenchmarkDemo3,@object # @_ZTI14BenchmarkDemo3
	.section	.rodata._ZTI14BenchmarkDemo3,"aG",@progbits,_ZTI14BenchmarkDemo3,comdat
	.weak	_ZTI14BenchmarkDemo3
	.p2align	4
_ZTI14BenchmarkDemo3:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14BenchmarkDemo3
	.quad	_ZTI13BenchmarkDemo
	.size	_ZTI14BenchmarkDemo3, 24

	.type	_ZTV14BenchmarkDemo4,@object # @_ZTV14BenchmarkDemo4
	.section	.rodata._ZTV14BenchmarkDemo4,"aG",@progbits,_ZTV14BenchmarkDemo4,comdat
	.weak	_ZTV14BenchmarkDemo4
	.p2align	3
_ZTV14BenchmarkDemo4:
	.quad	0
	.quad	_ZTI14BenchmarkDemo4
	.quad	_ZN15DemoApplication6myinitEv
	.quad	_ZN15DemoApplication16getDynamicsWorldEv
	.quad	_ZN13BenchmarkDemoD2Ev
	.quad	_ZN14BenchmarkDemo4D0Ev
	.quad	_ZN13BenchmarkDemo20clientMoveAndDisplayEv
	.quad	_ZN13BenchmarkDemo15displayCallbackEv
	.size	_ZTV14BenchmarkDemo4, 64

	.type	_ZTS14BenchmarkDemo4,@object # @_ZTS14BenchmarkDemo4
	.section	.rodata._ZTS14BenchmarkDemo4,"aG",@progbits,_ZTS14BenchmarkDemo4,comdat
	.weak	_ZTS14BenchmarkDemo4
	.p2align	4
_ZTS14BenchmarkDemo4:
	.asciz	"14BenchmarkDemo4"
	.size	_ZTS14BenchmarkDemo4, 17

	.type	_ZTI14BenchmarkDemo4,@object # @_ZTI14BenchmarkDemo4
	.section	.rodata._ZTI14BenchmarkDemo4,"aG",@progbits,_ZTI14BenchmarkDemo4,comdat
	.weak	_ZTI14BenchmarkDemo4
	.p2align	4
_ZTI14BenchmarkDemo4:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14BenchmarkDemo4
	.quad	_ZTI13BenchmarkDemo
	.size	_ZTI14BenchmarkDemo4, 24

	.type	_ZTV14BenchmarkDemo5,@object # @_ZTV14BenchmarkDemo5
	.section	.rodata._ZTV14BenchmarkDemo5,"aG",@progbits,_ZTV14BenchmarkDemo5,comdat
	.weak	_ZTV14BenchmarkDemo5
	.p2align	3
_ZTV14BenchmarkDemo5:
	.quad	0
	.quad	_ZTI14BenchmarkDemo5
	.quad	_ZN15DemoApplication6myinitEv
	.quad	_ZN15DemoApplication16getDynamicsWorldEv
	.quad	_ZN13BenchmarkDemoD2Ev
	.quad	_ZN14BenchmarkDemo5D0Ev
	.quad	_ZN13BenchmarkDemo20clientMoveAndDisplayEv
	.quad	_ZN13BenchmarkDemo15displayCallbackEv
	.size	_ZTV14BenchmarkDemo5, 64

	.type	_ZTS14BenchmarkDemo5,@object # @_ZTS14BenchmarkDemo5
	.section	.rodata._ZTS14BenchmarkDemo5,"aG",@progbits,_ZTS14BenchmarkDemo5,comdat
	.weak	_ZTS14BenchmarkDemo5
	.p2align	4
_ZTS14BenchmarkDemo5:
	.asciz	"14BenchmarkDemo5"
	.size	_ZTS14BenchmarkDemo5, 17

	.type	_ZTI14BenchmarkDemo5,@object # @_ZTI14BenchmarkDemo5
	.section	.rodata._ZTI14BenchmarkDemo5,"aG",@progbits,_ZTI14BenchmarkDemo5,comdat
	.weak	_ZTI14BenchmarkDemo5
	.p2align	4
_ZTI14BenchmarkDemo5:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14BenchmarkDemo5
	.quad	_ZTI13BenchmarkDemo
	.size	_ZTI14BenchmarkDemo5, 24

	.type	_ZTV14BenchmarkDemo6,@object # @_ZTV14BenchmarkDemo6
	.section	.rodata._ZTV14BenchmarkDemo6,"aG",@progbits,_ZTV14BenchmarkDemo6,comdat
	.weak	_ZTV14BenchmarkDemo6
	.p2align	3
_ZTV14BenchmarkDemo6:
	.quad	0
	.quad	_ZTI14BenchmarkDemo6
	.quad	_ZN15DemoApplication6myinitEv
	.quad	_ZN15DemoApplication16getDynamicsWorldEv
	.quad	_ZN13BenchmarkDemoD2Ev
	.quad	_ZN14BenchmarkDemo6D0Ev
	.quad	_ZN13BenchmarkDemo20clientMoveAndDisplayEv
	.quad	_ZN13BenchmarkDemo15displayCallbackEv
	.size	_ZTV14BenchmarkDemo6, 64

	.type	_ZTS14BenchmarkDemo6,@object # @_ZTS14BenchmarkDemo6
	.section	.rodata._ZTS14BenchmarkDemo6,"aG",@progbits,_ZTS14BenchmarkDemo6,comdat
	.weak	_ZTS14BenchmarkDemo6
	.p2align	4
_ZTS14BenchmarkDemo6:
	.asciz	"14BenchmarkDemo6"
	.size	_ZTS14BenchmarkDemo6, 17

	.type	_ZTI14BenchmarkDemo6,@object # @_ZTI14BenchmarkDemo6
	.section	.rodata._ZTI14BenchmarkDemo6,"aG",@progbits,_ZTI14BenchmarkDemo6,comdat
	.weak	_ZTI14BenchmarkDemo6
	.p2align	4
_ZTI14BenchmarkDemo6:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14BenchmarkDemo6
	.quad	_ZTI13BenchmarkDemo
	.size	_ZTI14BenchmarkDemo6, 24

	.type	_ZTV14BenchmarkDemo7,@object # @_ZTV14BenchmarkDemo7
	.section	.rodata._ZTV14BenchmarkDemo7,"aG",@progbits,_ZTV14BenchmarkDemo7,comdat
	.weak	_ZTV14BenchmarkDemo7
	.p2align	3
_ZTV14BenchmarkDemo7:
	.quad	0
	.quad	_ZTI14BenchmarkDemo7
	.quad	_ZN15DemoApplication6myinitEv
	.quad	_ZN15DemoApplication16getDynamicsWorldEv
	.quad	_ZN13BenchmarkDemoD2Ev
	.quad	_ZN14BenchmarkDemo7D0Ev
	.quad	_ZN13BenchmarkDemo20clientMoveAndDisplayEv
	.quad	_ZN13BenchmarkDemo15displayCallbackEv
	.size	_ZTV14BenchmarkDemo7, 64

	.type	_ZTS14BenchmarkDemo7,@object # @_ZTS14BenchmarkDemo7
	.section	.rodata._ZTS14BenchmarkDemo7,"aG",@progbits,_ZTS14BenchmarkDemo7,comdat
	.weak	_ZTS14BenchmarkDemo7
	.p2align	4
_ZTS14BenchmarkDemo7:
	.asciz	"14BenchmarkDemo7"
	.size	_ZTS14BenchmarkDemo7, 17

	.type	_ZTI14BenchmarkDemo7,@object # @_ZTI14BenchmarkDemo7
	.section	.rodata._ZTI14BenchmarkDemo7,"aG",@progbits,_ZTI14BenchmarkDemo7,comdat
	.weak	_ZTI14BenchmarkDemo7
	.p2align	4
_ZTI14BenchmarkDemo7:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14BenchmarkDemo7
	.quad	_ZTI13BenchmarkDemo
	.size	_ZTI14BenchmarkDemo7, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
