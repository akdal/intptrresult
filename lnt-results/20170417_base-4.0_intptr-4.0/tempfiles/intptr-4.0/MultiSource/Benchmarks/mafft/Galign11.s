	.text
	.file	"Galign11.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4608533498688228557     # double 1.3
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_1:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI0_2:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI0_3:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.text
	.globl	G__align11
	.p2align	4, 0x90
	.type	G__align11,@function
G__align11:                             # @G__align11
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 192
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edx, 100(%rsp)         # 4-byte Spill
	movq	%rsi, %r14
	cvtsi2ssl	penalty(%rip), %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2ssl	penalty_ex(%rip), %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	cmpl	$0, G__align11.orlgth1(%rip)
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	jne	.LBB0_2
# BB#1:
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, G__align11.mseq1(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rax, G__align11.mseq2(%rip)
.LBB0_2:
	movq	(%rdi), %rdi
	callq	strlen
	movq	%rax, %r13
	movq	(%r14), %rdi
	callq	strlen
	movq	%rax, %r8
	testl	%r13d, %r13d
	movq	%r8, 64(%rsp)           # 8-byte Spill
	jle	.LBB0_4
# BB#3:
	testl	%r8d, %r8d
	jle	.LBB0_4
.LBB0_5:
	movl	G__align11.orlgth1(%rip), %r15d
	cmpl	%r15d, %r13d
	movl	G__align11.orlgth2(%rip), %esi
	movq	%r14, 88(%rsp)          # 8-byte Spill
	jg	.LBB0_7
# BB#6:
	cmpl	%esi, %r8d
	jle	.LBB0_11
.LBB0_7:
	testl	%r15d, %r15d
	jle	.LBB0_10
# BB#8:
	testl	%esi, %esi
	jle	.LBB0_10
# BB#9:
	movq	G__align11.w1(%rip), %rdi
	callq	FreeFloatVec
	movq	G__align11.w2(%rip), %rdi
	callq	FreeFloatVec
	movq	G__align11.match(%rip), %rdi
	callq	FreeFloatVec
	movq	G__align11.initverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	G__align11.lastverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	G__align11.m(%rip), %rdi
	callq	FreeFloatVec
	movq	G__align11.mp(%rip), %rdi
	callq	FreeIntVec
	movq	G__align11.mseq(%rip), %rdi
	callq	FreeCharMtx
	movq	G__align11.floatwork(%rip), %rdi
	callq	FreeFloatMtx
	movq	G__align11.intwork(%rip), %rdi
	callq	FreeIntMtx
	movq	64(%rsp), %r8           # 8-byte Reload
	movl	G__align11.orlgth1(%rip), %r15d
	movl	G__align11.orlgth2(%rip), %esi
.LBB0_10:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r15d, %eax
	cmovgel	%eax, %r15d
	movq	%r13, 16(%rsp)          # 8-byte Spill
	leal	100(%r15), %r13d
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r8d, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%esi, %eax
	cmovgel	%eax, %esi
	leal	100(%rsi), %ebx
	leal	102(%rsi), %ebp
	movl	%ebp, %edi
	movq	%rsi, %r14
	callq	AllocateFloatVec
	movq	%rax, G__align11.w1(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, G__align11.w2(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, G__align11.match(%rip)
	leal	102(%r15), %r12d
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, G__align11.initverticalw(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, G__align11.lastverticalw(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, G__align11.m(%rip)
	movl	%ebp, %edi
	callq	AllocateIntVec
	movq	%rax, G__align11.mp(%rip)
	movl	njob(%rip), %edi
	leal	200(%r14,%r15), %esi
	callq	AllocateCharMtx
	movq	%rax, G__align11.mseq(%rip)
	cmpl	%ebx, %r13d
	cmovgel	%r13d, %ebx
	movq	16(%rsp), %r13          # 8-byte Reload
	addl	$2, %ebx
	movl	$26, %edi
	movl	%ebx, %esi
	callq	AllocateFloatMtx
	movq	%rax, G__align11.floatwork(%rip)
	movl	$26, %edi
	movl	%ebx, %esi
	callq	AllocateIntMtx
	movq	%r14, %rsi
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	%rax, G__align11.intwork(%rip)
	movl	%r15d, G__align11.orlgth1(%rip)
	movl	%esi, G__align11.orlgth2(%rip)
.LBB0_11:
	movq	24(%rsp), %r9           # 8-byte Reload
	movaps	32(%rsp), %xmm3         # 16-byte Reload
	movss	8(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movq	G__align11.mseq(%rip), %rax
	movq	(%rax), %rcx
	movq	G__align11.mseq1(%rip), %rdx
	movq	%rcx, (%rdx)
	movq	8(%rax), %rax
	movq	G__align11.mseq2(%rip), %rcx
	movq	%rax, (%rcx)
	movl	commonAlloc1(%rip), %ebx
	cmpl	%ebx, %r15d
	movl	commonAlloc2(%rip), %ebp
	jg	.LBB0_14
# BB#12:
	cmpl	%ebp, %esi
	jg	.LBB0_14
# BB#13:                                # %._crit_edge287
	movq	commonIP(%rip), %r14
	jmp	.LBB0_18
.LBB0_14:                               # %._crit_edge281
	testl	%ebx, %ebx
	je	.LBB0_17
# BB#15:                                # %._crit_edge281
	testl	%ebp, %ebp
	je	.LBB0_17
# BB#16:
	movq	commonIP(%rip), %rdi
	callq	FreeIntMtx
	movl	G__align11.orlgth1(%rip), %r15d
	movl	commonAlloc1(%rip), %ebx
	movl	G__align11.orlgth2(%rip), %esi
	movl	commonAlloc2(%rip), %ebp
.LBB0_17:
	cmpl	%ebx, %r15d
	cmovgel	%r15d, %ebx
	cmpl	%ebp, %esi
	cmovgel	%esi, %ebp
	leal	10(%rbx), %edi
	leal	10(%rbp), %esi
	callq	AllocateIntMtx
	movq	%rax, %r14
	movq	%r14, commonIP(%rip)
	movl	%ebx, commonAlloc1(%rip)
	movl	%ebp, commonAlloc2(%rip)
	movq	24(%rsp), %r9           # 8-byte Reload
	movaps	32(%rsp), %xmm3         # 16-byte Reload
	movss	8(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movq	64(%rsp), %r8           # 8-byte Reload
.LBB0_18:
	movq	%r14, G__align11.ijp(%rip)
	movq	G__align11.w1(%rip), %r15
	movq	G__align11.w2(%rip), %r10
	movq	G__align11.initverticalw(%rip), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	(%r9), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	je	.LBB0_24
# BB#19:                                # %.lr.ph.i.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	movsbq	(%rax), %rax
	testb	$1, %r13b
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%r13d, %edx
	je	.LBB0_21
# BB#20:                                # %.lr.ph.i.prol
	leal	-1(%r13), %edx
	movq	56(%rsp), %rsi          # 8-byte Reload
	leaq	1(%rsi), %rcx
	movsbq	(%rsi), %rsi
	movq	%rax, %rdi
	shlq	$9, %rdi
	cvtsi2ssl	amino_dis(%rdi,%rsi,4), %xmm0
	movq	32(%rsp), %rdi          # 8-byte Reload
	leaq	4(%rdi), %rsi
	movss	%xmm0, (%rdi)
.LBB0_21:                               # %.lr.ph.i.prol.loopexit
	cmpl	$1, %r13d
	je	.LBB0_24
# BB#22:                                # %.lr.ph.i.preheader.new
	shlq	$9, %rax
	.p2align	4, 0x90
.LBB0_23:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movsbq	(%rcx), %rdi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rax,%rdi,4), %xmm0
	movss	%xmm0, (%rsi)
	addl	$-2, %edx
	movsbq	1(%rcx), %rdi
	leaq	2(%rcx), %rcx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rax,%rdi,4), %xmm0
	movss	%xmm0, 4(%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB0_23
.LBB0_24:                               # %match_calc.exit
	testl	%r8d, %r8d
	je	.LBB0_30
# BB#25:                                # %.lr.ph.i210.preheader
	movq	56(%rsp), %rax          # 8-byte Reload
	movsbq	(%rax), %rax
	testb	$1, %r8b
	movq	%r15, %rsi
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%r8d, %edx
	je	.LBB0_27
# BB#26:                                # %.lr.ph.i210.prol
	leal	-1(%r8), %edx
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	1(%rsi), %rcx
	movsbq	(%rsi), %rsi
	movq	%rax, %rdi
	shlq	$9, %rdi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rdi,%rsi,4), %xmm0
	leaq	4(%r15), %rsi
	movss	%xmm0, (%r15)
.LBB0_27:                               # %.lr.ph.i210.prol.loopexit
	cmpl	$1, %r8d
	je	.LBB0_30
# BB#28:                                # %.lr.ph.i210.preheader.new
	shlq	$9, %rax
	.p2align	4, 0x90
.LBB0_29:                               # %.lr.ph.i210
                                        # =>This Inner Loop Header: Depth=1
	movsbq	(%rcx), %rdi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rax,%rdi,4), %xmm0
	movss	%xmm0, (%rsi)
	addl	$-2, %edx
	movsbq	1(%rcx), %rdi
	leaq	2(%rcx), %rcx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rax,%rdi,4), %xmm0
	movss	%xmm0, 4(%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB0_29
.LBB0_30:                               # %match_calc.exit212
	cmpl	$1, outgap(%rip)
	jne	.LBB0_43
# BB#31:                                # %.preheader220
	testl	%r13d, %r13d
	jle	.LBB0_40
# BB#32:                                # %.lr.ph246
	leal	1(%r13), %eax
	leaq	-1(%rax), %rdx
	cmpq	$7, %rdx
	jbe	.LBB0_33
# BB#36:                                # %min.iters.checked
	movl	%r13d, %esi
	andl	$7, %esi
	movq	%rdx, %rcx
	subq	%rsi, %rcx
	subq	%rsi, %rdx
	je	.LBB0_33
# BB#37:                                # %vector.ph
	incq	%rcx
	movaps	%xmm3, %xmm0
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	32(%rsp), %rdi          # 8-byte Reload
	leaq	20(%rdi), %rdi
	.p2align	4, 0x90
.LBB0_38:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdi), %xmm1
	movups	(%rdi), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	%xmm1, -16(%rdi)
	movups	%xmm2, (%rdi)
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB0_38
# BB#39:                                # %middle.block
	testq	%rsi, %rsi
	jne	.LBB0_34
	jmp	.LBB0_40
.LBB0_33:
	movl	$1, %ecx
.LBB0_34:                               # %scalar.ph.preheader
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,4), %rdx
	subq	%rcx, %rax
	.p2align	4, 0x90
.LBB0_35:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm0
	movss	%xmm0, (%rdx)
	addq	$4, %rdx
	decq	%rax
	jne	.LBB0_35
.LBB0_40:                               # %.preheader219
	testl	%r8d, %r8d
	jle	.LBB0_58
# BB#41:                                # %.lr.ph244.preheader
	leal	1(%r8), %eax
	leaq	-1(%rax), %rdx
	cmpq	$7, %rdx
	jbe	.LBB0_42
# BB#64:                                # %min.iters.checked315
	movl	%r8d, %esi
	andl	$7, %esi
	movq	%rdx, %rcx
	subq	%rsi, %rcx
	subq	%rsi, %rdx
	je	.LBB0_42
# BB#65:                                # %vector.ph319
	incq	%rcx
	movaps	%xmm3, %xmm0
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	20(%r15), %rdi
	.p2align	4, 0x90
.LBB0_66:                               # %vector.body311
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdi), %xmm1
	movups	(%rdi), %xmm2
	addps	%xmm0, %xmm1
	addps	%xmm0, %xmm2
	movups	%xmm1, -16(%rdi)
	movups	%xmm2, (%rdi)
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB0_66
# BB#67:                                # %middle.block312
	testq	%rsi, %rsi
	jne	.LBB0_68
	jmp	.LBB0_43
.LBB0_42:
	movl	$1, %ecx
.LBB0_68:                               # %.lr.ph244.preheader380
	leaq	(%r15,%rcx,4), %rdx
	subq	%rcx, %rax
	.p2align	4, 0x90
.LBB0_69:                               # %.lr.ph244
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm0
	movss	%xmm0, (%rdx)
	addq	$4, %rdx
	decq	%rax
	jne	.LBB0_69
.LBB0_43:                               # %.preheader
	testl	%r8d, %r8d
	jle	.LBB0_58
# BB#44:                                # %.lr.ph241
	movq	G__align11.m(%rip), %rcx
	movq	G__align11.mp(%rip), %rdx
	leaq	1(%r8), %rbp
	movl	%ebp, %r12d
	leaq	-1(%r12), %r11
	cmpq	$7, %r11
	ja	.LBB0_51
# BB#45:
	movl	$1, %ebx
	jmp	.LBB0_46
.LBB0_51:                               # %min.iters.checked338
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movl	%r8d, %r9d
	andl	$7, %r9d
	movq	%r11, %r13
	subq	%r9, %r13
	je	.LBB0_52
# BB#53:                                # %vector.memcheck
	leaq	4(%rcx), %rax
	leaq	-4(%r15,%r12,4), %rsi
	cmpq	%rsi, %rax
	jae	.LBB0_55
# BB#54:                                # %vector.memcheck
	leaq	(%rcx,%r12,4), %rax
	cmpq	%rax, %r15
	jae	.LBB0_55
.LBB0_52:
	movl	$1, %ebx
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %r13          # 8-byte Reload
	jmp	.LBB0_46
.LBB0_55:                               # %vector.body334.preheader
	leaq	16(%r15), %rax
	leaq	20(%rcx), %rsi
	leaq	20(%rdx), %rdi
	xorpd	%xmm0, %xmm0
	leaq	1(%r13), %rbx
	.p2align	4, 0x90
.LBB0_56:                               # %vector.body334
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rax), %xmm1
	movups	(%rax), %xmm2
	movupd	%xmm1, -16(%rsi)
	movups	%xmm2, (%rsi)
	movupd	%xmm0, -16(%rdi)
	movupd	%xmm0, (%rdi)
	addq	$32, %rax
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-8, %r13
	jne	.LBB0_56
# BB#57:                                # %middle.block335
	testq	%r9, %r9
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %r13          # 8-byte Reload
	je	.LBB0_58
.LBB0_46:                               # %scalar.ph336.preheader
	subl	%ebx, %ebp
	subq	%rbx, %r11
	andq	$3, %rbp
	je	.LBB0_49
# BB#47:                                # %scalar.ph336.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB0_48:                               # %scalar.ph336.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r15,%rbx,4), %eax
	movl	%eax, (%rcx,%rbx,4)
	movl	$0, (%rdx,%rbx,4)
	incq	%rbx
	incq	%rbp
	jne	.LBB0_48
.LBB0_49:                               # %scalar.ph336.prol.loopexit
	cmpq	$3, %r11
	jb	.LBB0_58
	.p2align	4, 0x90
.LBB0_50:                               # %scalar.ph336
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r15,%rbx,4), %eax
	movl	%eax, (%rcx,%rbx,4)
	movl	$0, (%rdx,%rbx,4)
	movl	(%r15,%rbx,4), %eax
	movl	%eax, 4(%rcx,%rbx,4)
	movl	$0, 4(%rdx,%rbx,4)
	movl	4(%r15,%rbx,4), %eax
	movl	%eax, 8(%rcx,%rbx,4)
	movl	$0, 8(%rdx,%rbx,4)
	movl	8(%r15,%rbx,4), %eax
	movl	%eax, 12(%rcx,%rbx,4)
	movl	$0, 12(%rdx,%rbx,4)
	addq	$4, %rbx
	cmpq	%rbx, %r12
	jne	.LBB0_50
.LBB0_58:                               # %._crit_edge242
	testl	%r8d, %r8d
	xorps	%xmm5, %xmm5
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	xorpd	%xmm0, %xmm0
	je	.LBB0_60
# BB#59:
	movq	%r8, %rcx
	shlq	$32, %rcx
	addq	%rax, %rcx
	sarq	$30, %rcx
	movss	(%r15,%rcx), %xmm0      # xmm0 = mem[0],zero,zero,zero
.LBB0_60:
	movq	G__align11.lastverticalw(%rip), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movss	%xmm0, (%rcx)
	cmpl	$1, outgap(%rip)
	sbbl	$-1, %r13d
	cmpl	$2, %r13d
	movq	%r14, 80(%rsp)          # 8-byte Spill
	jl	.LBB0_84
# BB#61:                                # %.lr.ph236
	movq	%r8, %rbx
	shlq	$32, %rbx
	addq	%rax, %rbx
	sarq	$32, %rbx
	testl	%r8d, %r8d
	je	.LBB0_62
# BB#75:                                # %.lr.ph236.split.preheader
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	movq	G__align11.m(%rip), %rbx
	movq	G__align11.mp(%rip), %r11
	movq	%r13, %rax
	leal	1(%r8), %r13d
	movl	%eax, %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	%r8d, %eax
	andl	$1, %eax
	movl	%eax, 108(%rsp)         # 4-byte Spill
	leal	-1(%r8), %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	incq	%rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	addq	$-2, %r13
	xorps	%xmm5, %xmm5
	movl	$1, %r14d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_76:                               # %.lr.ph236.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_79 Depth 2
                                        #     Child Loop BB0_96 Depth 2
	movq	%r15, %r8
	cmpl	$0, 108(%rsp)           # 4-byte Folded Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	-4(%rax,%r14,4), %eax
	movl	%eax, (%r8)
	movq	56(%rsp), %rax          # 8-byte Reload
	movsbq	(%rax,%r14), %rdx
	movq	%r10, %rcx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	64(%rsp), %r9           # 8-byte Reload
	movl	%r9d, %ebp
	je	.LBB0_78
# BB#77:                                # %.lr.ph.i216.prol
                                        #   in Loop: Header=BB0_76 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movsbq	(%rax), %rcx
	movq	%rdx, %rsi
	shlq	$9, %rsi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rsi,%rcx,4), %xmm0
	leaq	4(%r10), %rcx
	movss	%xmm0, (%r10)
	movq	112(%rsp), %rsi         # 8-byte Reload
	movl	104(%rsp), %ebp         # 4-byte Reload
.LBB0_78:                               # %.lr.ph.i216.prol.loopexit
                                        #   in Loop: Header=BB0_76 Depth=1
	cmpl	$1, %r9d
	je	.LBB0_80
	.p2align	4, 0x90
.LBB0_79:                               # %.lr.ph.i216
                                        #   Parent Loop BB0_76 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	(%rsi), %rdi
	movq	%rdx, %rax
	shlq	$9, %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rax,%rdi,4), %xmm0
	movss	%xmm0, (%rcx)
	addl	$-2, %ebp
	movsbq	1(%rsi), %rdi
	leaq	2(%rsi), %rsi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rax,%rdi,4), %xmm0
	movss	%xmm0, 4(%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB0_79
.LBB0_80:                               # %match_calc.exit218
                                        #   in Loop: Header=BB0_76 Depth=1
	testl	%r9d, %r9d
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r14,4), %eax
	movl	%eax, (%r10)
	movl	(%r8), %eax
	movl	%eax, G__align11.mi(%rip)
	jle	.LBB0_81
# BB#95:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_76 Depth=1
	leaq	-1(%r14), %rsi
	movd	%eax, %xmm0
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r14,8), %rax
	xorl	%ecx, %ecx
	movl	$-1, %ebp
	movdqa	%xmm0, %xmm1
	xorl	%r9d, %r9d
	jmp	.LBB0_96
	.p2align	4, 0x90
.LBB0_103:                              # %..lr.ph_crit_edge
                                        #   in Loop: Header=BB0_96 Depth=2
	movss	4(%r8,%rcx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	incq	%rcx
	decl	%ebp
.LBB0_96:                               # %.lr.ph
                                        #   Parent Loop BB0_76 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm3, %xmm5
	addss	%xmm0, %xmm5
	leal	(%rbp,%r9), %edx
	ucomiss	%xmm1, %xmm5
	cmovbel	%r12d, %edx
	maxss	%xmm1, %xmm5
	movl	%edx, 4(%rax,%rcx,4)
	ucomiss	%xmm0, %xmm1
	jb	.LBB0_98
# BB#97:                                #   in Loop: Header=BB0_96 Depth=2
	movss	%xmm1, G__align11.mi(%rip)
	movaps	%xmm1, %xmm0
	movl	%ecx, %r9d
.LBB0_98:                               #   in Loop: Header=BB0_96 Depth=2
	addss	%xmm4, %xmm0
	movss	%xmm0, G__align11.mi(%rip)
	movss	4(%rbx,%rcx,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	addss	%xmm1, %xmm2
	ucomiss	%xmm5, %xmm2
	jbe	.LBB0_100
# BB#99:                                #   in Loop: Header=BB0_96 Depth=2
	movl	%r14d, %edx
	subl	4(%r11,%rcx,4), %edx
	movl	%edx, 4(%rax,%rcx,4)
	movaps	%xmm2, %xmm5
.LBB0_100:                              #   in Loop: Header=BB0_96 Depth=2
	movss	(%r8,%rcx,4), %xmm2     # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jb	.LBB0_102
# BB#101:                               #   in Loop: Header=BB0_96 Depth=2
	movss	%xmm2, 4(%rbx,%rcx,4)
	movl	%esi, 4(%r11,%rcx,4)
.LBB0_102:                              #   in Loop: Header=BB0_96 Depth=2
	movss	4(%rbx,%rcx,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm1
	movss	%xmm1, 4(%rbx,%rcx,4)
	movss	4(%r10,%rcx,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm1
	movss	%xmm1, 4(%r10,%rcx,4)
	cmpq	%rcx, %r13
	jne	.LBB0_103
	jmp	.LBB0_82
	.p2align	4, 0x90
.LBB0_81:                               #   in Loop: Header=BB0_76 Depth=1
	xorl	%r9d, %r9d
.LBB0_82:                               # %._crit_edge
                                        #   in Loop: Header=BB0_76 Depth=1
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	(%r10,%rax,4), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx,%r14,4)
	incq	%r14
	cmpq	120(%rsp), %r14         # 8-byte Folded Reload
	movq	%r10, %r15
	movq	%r8, %r10
	jne	.LBB0_76
	jmp	.LBB0_83
.LBB0_62:                               # %._crit_edge.us.preheader
	testb	$1, %r13b
	jne	.LBB0_63
# BB#70:                                # %._crit_edge.us.prol
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %eax
	movl	%eax, (%r15)
	movl	4(%rcx), %eax
	movl	%eax, (%r10)
	movl	(%r15), %ecx
	movl	(%r10,%rbx,4), %eax
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%eax, 4(%rdx)
	movl	$2, %edi
	movq	%r10, %rax
	movq	%r15, %r10
	cmpl	$2, %r13d
	jne	.LBB0_72
	jmp	.LBB0_74
.LBB0_63:
	movl	$1, %edi
                                        # implicit-def: %ECX
	movq	%r15, %rax
	cmpl	$2, %r13d
	je	.LBB0_74
.LBB0_72:                               # %._crit_edge.us.preheader.new
	movl	%r13d, %edx
	subq	%rdi, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdi,4), %rsi
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	4(%rcx,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB0_73:                               # %._crit_edge.us
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rsi), %ecx
	movl	%ecx, (%rax)
	movl	(%rsi), %ecx
	movl	%ecx, (%r10)
	movl	(%r10,%rbx,4), %ecx
	movl	%ecx, -4(%rdi)
	movl	(%rsi), %ecx
	movl	%ecx, (%r10)
	movl	4(%rsi), %ecx
	movl	%ecx, (%rax)
	movl	(%r10), %ecx
	movl	(%rax,%rbx,4), %ebp
	movl	%ebp, (%rdi)
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rdx
	jne	.LBB0_73
.LBB0_74:                               # %._crit_edge237.loopexit
	movl	%ecx, G__align11.mi(%rip)
	xorps	%xmm5, %xmm5
	xorl	%r9d, %r9d
.LBB0_83:                               # %._crit_edge237
	movl	%r9d, G__align11.mpi(%rip)
	movq	80(%rsp), %r14          # 8-byte Reload
.LBB0_84:
	movaps	%xmm5, 64(%rsp)         # 16-byte Spill
	movq	G__align11.mseq1(%rip), %r13
	movq	G__align11.mseq2(%rip), %rbp
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	strlen
	movq	%rax, %rbx
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	strlen
	testl	%ebx, %ebx
	movq	88(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
	js	.LBB0_90
# BB#85:                                # %.lr.ph19.preheader.i
	movq	%rbx, %rdi
	incq	%rdi
	movl	%edi, %ecx
	leaq	-1(%rcx), %r8
	xorl	%esi, %esi
	andq	$7, %rdi
	je	.LBB0_87
	.p2align	4, 0x90
.LBB0_86:                               # %.lr.ph19.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rsi,8), %rdx
	incq	%rsi
	movl	%esi, (%rdx)
	cmpq	%rsi, %rdi
	jne	.LBB0_86
.LBB0_87:                               # %.lr.ph19.i.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB0_90
# BB#88:                                # %.lr.ph19.preheader.i.new
	negq	%rcx
	leaq	4(%rsi,%rcx), %r8
	leaq	56(%r14,%rsi,8), %rdx
	leaq	4(%rsi), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_89:                               # %.lr.ph19.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rdx,%rdi,8), %r10
	leal	(%rsi,%rdi), %r9d
	leal	-3(%rsi,%rdi), %ecx
	movl	%ecx, (%r10)
	movq	-48(%rdx,%rdi,8), %r10
	leal	-2(%rsi,%rdi), %ecx
	movl	%ecx, (%r10)
	movq	-40(%rdx,%rdi,8), %r10
	leal	-1(%rsi,%rdi), %ecx
	movl	%ecx, (%r10)
	movq	-32(%rdx,%rdi,8), %rcx
	movl	%r9d, (%rcx)
	movq	-24(%rdx,%rdi,8), %r9
	leal	1(%rsi,%rdi), %ecx
	movl	%ecx, (%r9)
	movq	-16(%rdx,%rdi,8), %r9
	leal	2(%rsi,%rdi), %ecx
	movl	%ecx, (%r9)
	movq	-8(%rdx,%rdi,8), %r9
	leal	3(%rsi,%rdi), %ecx
	movl	%ecx, (%r9)
	movq	(%rdx,%rdi,8), %r9
	leal	4(%rsi,%rdi), %ecx
	movl	%ecx, (%r9)
	leaq	8(%r8,%rdi), %rcx
	addq	$8, %rdi
	cmpq	$4, %rcx
	jne	.LBB0_89
.LBB0_90:                               # %.preheader.i
	testl	%eax, %eax
	js	.LBB0_108
# BB#91:                                # %.lr.ph15.i
	movq	(%r14), %r8
	movq	%rax, %rdi
	incq	%rdi
	movl	%edi, %ecx
	cmpq	$7, %rcx
	jbe	.LBB0_92
# BB#104:                               # %min.iters.checked365
	andl	$7, %edi
	movq	%rcx, %r9
	subq	%rdi, %r9
	je	.LBB0_92
# BB#105:                               # %vector.body361.preheader
	leaq	16(%r8), %rdx
	movdqa	.LCPI0_1(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI0_2(%rip), %xmm1   # xmm1 = [4,4,4,4]
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI0_3(%rip), %xmm3   # xmm3 = [8,8,8,8]
	movq	%r9, %rsi
	.p2align	4, 0x90
.LBB0_106:                              # %vector.body361
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	pxor	%xmm2, %xmm5
	pxor	%xmm2, %xmm4
	movdqu	%xmm5, -16(%rdx)
	movdqu	%xmm4, (%rdx)
	paddd	%xmm3, %xmm0
	addq	$32, %rdx
	addq	$-8, %rsi
	jne	.LBB0_106
# BB#107:                               # %middle.block362
	testq	%rdi, %rdi
	jne	.LBB0_93
	jmp	.LBB0_108
.LBB0_92:
	xorl	%r9d, %r9d
.LBB0_93:                               # %scalar.ph363.preheader
	movl	%r9d, %edx
	notl	%edx
	leaq	(%r8,%r9,4), %rsi
	subq	%r9, %rcx
	.p2align	4, 0x90
.LBB0_94:                               # %scalar.ph363
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rsi)
	decl	%edx
	addq	$4, %rsi
	decq	%rcx
	jne	.LBB0_94
.LBB0_108:                              # %._crit_edge16.i
	leal	(%rax,%rbx), %edx
	movq	(%r13), %rcx
	movl	%edx, 32(%rsp)          # 4-byte Spill
	movslq	%edx, %rdx
	leaq	(%rcx,%rdx), %rsi
	movq	%rsi, (%r13)
	movb	$0, (%rcx,%rdx)
	movq	(%rbp), %rcx
	leaq	(%rcx,%rdx), %rsi
	movq	%rsi, (%rbp)
	movb	$0, (%rcx,%rdx)
	testl	%edx, %edx
	js	.LBB0_132
# BB#109:                               # %.lr.ph11.i.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_110:                              # %.lr.ph11.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_120 Depth 2
                                        #     Child Loop BB0_127 Depth 2
	movslq	%ebx, %rcx
	movq	(%r14,%rcx,8), %rcx
	movslq	%eax, %rsi
	movl	(%rcx,%rsi,4), %r14d
	testl	%r14d, %r14d
	js	.LBB0_111
# BB#112:                               #   in Loop: Header=BB0_110 Depth=1
	je	.LBB0_114
# BB#113:                               #   in Loop: Header=BB0_110 Depth=1
	movl	%ebx, %r10d
	subl	%r14d, %r10d
	jmp	.LBB0_115
	.p2align	4, 0x90
.LBB0_111:                              #   in Loop: Header=BB0_110 Depth=1
	leal	-1(%rbx), %r10d
	jmp	.LBB0_116
	.p2align	4, 0x90
.LBB0_114:                              #   in Loop: Header=BB0_110 Depth=1
	leal	-1(%rbx), %r10d
.LBB0_115:                              #   in Loop: Header=BB0_110 Depth=1
	movl	$-1, %r14d
.LBB0_116:                              #   in Loop: Header=BB0_110 Depth=1
	movl	%ebx, %ecx
	subl	%r10d, %ecx
	decl	%ecx
	je	.LBB0_122
# BB#117:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_110 Depth=1
	movslq	%ecx, %rcx
	movslq	%r10d, %rsi
	leal	-2(%rbx), %r9d
	movq	%rbx, %r8
	testb	$1, %cl
	je	.LBB0_119
# BB#118:                               # %.lr.ph.i206.prol
                                        #   in Loop: Header=BB0_110 Depth=1
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %r11
	leaq	(%rcx,%rsi), %rdi
	movb	(%r11,%rdi), %r11b
	movq	(%r13), %r15
	leaq	-1(%r15), %rdi
	movq	%rdi, (%r13)
	movb	%r11b, -1(%r15)
	movq	88(%rsp), %r15          # 8-byte Reload
	movq	(%rbp), %r11
	leaq	-1(%r11), %rdi
	movq	%rdi, (%rbp)
	movb	$45, -1(%r11)
	movq	24(%rsp), %r11          # 8-byte Reload
	decq	%rcx
.LBB0_119:                              # %.lr.ph.i206.prol.loopexit
                                        #   in Loop: Header=BB0_110 Depth=1
	cmpl	%r10d, %r9d
	je	.LBB0_121
	.p2align	4, 0x90
.LBB0_120:                              # %.lr.ph.i206
                                        #   Parent Loop BB0_110 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r11), %rdi
	addq	%rsi, %rdi
	movzbl	(%rcx,%rdi), %edx
	movq	(%r13), %rdi
	leaq	-1(%rdi), %rbx
	movq	%rbx, (%r13)
	movb	%dl, -1(%rdi)
	movq	(%rbp), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, (%rbp)
	movb	$45, -1(%rdx)
	movq	(%r11), %rdx
	addq	%rsi, %rdx
	movzbl	-1(%rcx,%rdx), %edx
	movq	(%r13), %rdi
	leaq	-1(%rdi), %rbx
	movq	%rbx, (%r13)
	movb	%dl, -1(%rdi)
	movq	(%rbp), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, (%rbp)
	movb	$45, -1(%rdx)
	addq	$-2, %rcx
	testl	%ecx, %ecx
	jne	.LBB0_120
.LBB0_121:                              # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB0_110 Depth=1
	movq	%r8, %rbx
	leal	-1(%r12,%rbx), %r12d
	subl	%r10d, %r12d
.LBB0_122:                              # %._crit_edge.i
                                        #   in Loop: Header=BB0_110 Depth=1
	leal	(%r14,%rax), %r9d
	cmpl	$-1, %r14d
	je	.LBB0_123
# BB#124:                               # %.lr.ph4.preheader.i
                                        #   in Loop: Header=BB0_110 Depth=1
	movq	%rbx, %r8
	movl	%r14d, %r11d
	notl	%r11d
	movslq	%r11d, %rsi
	movslq	%r9d, %rcx
	testb	$1, %sil
	je	.LBB0_126
# BB#125:                               # %.lr.ph4.i.prol
                                        #   in Loop: Header=BB0_110 Depth=1
	movq	(%r13), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, (%r13)
	movb	$45, -1(%rdx)
	movq	(%r15), %rdx
	leaq	(%rsi,%rcx), %rdi
	movb	(%rdx,%rdi), %dl
	movq	(%rbp), %rdi
	leaq	-1(%rdi), %rbx
	movq	%rbx, (%rbp)
	movb	%dl, -1(%rdi)
	decq	%rsi
.LBB0_126:                              # %.lr.ph4.i.prol.loopexit
                                        #   in Loop: Header=BB0_110 Depth=1
	cmpl	$-2, %r14d
	je	.LBB0_128
	.p2align	4, 0x90
.LBB0_127:                              # %.lr.ph4.i
                                        #   Parent Loop BB0_110 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, (%r13)
	movb	$45, -1(%rdx)
	movq	(%r15), %rdx
	addq	%rcx, %rdx
	movzbl	(%rsi,%rdx), %edx
	movq	(%rbp), %rdi
	leaq	-1(%rdi), %rbx
	movq	%rbx, (%rbp)
	movb	%dl, -1(%rdi)
	movq	(%r13), %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, (%r13)
	movb	$45, -1(%rdx)
	movq	(%r15), %rdx
	addq	%rcx, %rdx
	movzbl	-1(%rsi,%rdx), %edx
	movq	(%rbp), %rdi
	leaq	-1(%rdi), %rbx
	movq	%rbx, (%rbp)
	movb	%dl, -1(%rdi)
	addq	$-2, %rsi
	testl	%esi, %esi
	jne	.LBB0_127
.LBB0_128:                              # %._crit_edge5.loopexit.i
                                        #   in Loop: Header=BB0_110 Depth=1
	addl	%r11d, %r12d
	movq	24(%rsp), %r11          # 8-byte Reload
	movq	80(%rsp), %r14          # 8-byte Reload
	movq	%r8, %rbx
	testl	%ebx, %ebx
	jg	.LBB0_130
	jmp	.LBB0_132
	.p2align	4, 0x90
.LBB0_123:                              #   in Loop: Header=BB0_110 Depth=1
	movq	80(%rsp), %r14          # 8-byte Reload
	testl	%ebx, %ebx
	jle	.LBB0_132
.LBB0_130:                              # %._crit_edge5.i
                                        #   in Loop: Header=BB0_110 Depth=1
	testl	%eax, %eax
	jle	.LBB0_132
# BB#131:                               #   in Loop: Header=BB0_110 Depth=1
	movq	(%r11), %rax
	movslq	%r10d, %rcx
	movb	(%rax,%rcx), %al
	movq	(%r13), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%r13)
	movb	%al, -1(%rcx)
	movq	(%r15), %rax
	movslq	%r9d, %rcx
	movb	(%rax,%rcx), %al
	movq	(%rbp), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%rbp)
	movb	%al, -1(%rcx)
	addl	$2, %r12d
	cmpl	32(%rsp), %r12d         # 4-byte Folded Reload
	movl	%r10d, %ebx
	movl	%r9d, %eax
	jle	.LBB0_110
.LBB0_132:                              # %Atracking.exit
	movq	(%r13), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %rcx
	movl	100(%rsp), %edx         # 4-byte Reload
	cmpl	%edx, %ecx
	jg	.LBB0_134
# BB#133:                               # %Atracking.exit
	cmpl	$5000001, %ecx          # imm = 0x4C4B41
	jge	.LBB0_134
.LBB0_135:
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	%rbp, %rsi
	callq	strcpy
	movq	(%r15), %rdi
	movq	G__align11.mseq2(%rip), %rax
	movq	(%rax), %rsi
	callq	strcpy
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_4:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	movl	%r8d, %ecx
	callq	fprintf
	movq	64(%rsp), %r8           # 8-byte Reload
	jmp	.LBB0_5
.LBB0_134:
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	movl	$5000000, %r8d          # imm = 0x4C4B40
	xorl	%eax, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$.L.str.2, %edi
	callq	ErrorExit
	movq	G__align11.mseq1(%rip), %rax
	movq	(%rax), %rbp
	jmp	.LBB0_135
.Lfunc_end0:
	.size	G__align11, .Lfunc_end0-G__align11
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4608533498688228557     # double 1.3
	.text
	.globl	G__align11_noalign
	.p2align	4, 0x90
	.type	G__align11_noalign,@function
G__align11_noalign:                     # @G__align11_noalign
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 128
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r8, %rbx
	movl	%edx, 56(%rsp)          # 4-byte Spill
	movl	%esi, %r14d
	movq	%rdi, %r15
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	(%rcx), %rdi
	callq	strlen
	movq	%rax, %r13
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	(%rbx), %rdi
	callq	strlen
	movq	%rax, %r12
	testl	%r13d, %r13d
	jle	.LBB1_2
# BB#1:
	testl	%r12d, %r12d
	jle	.LBB1_2
.LBB1_3:
	movl	G__align11_noalign.orlgth1(%rip), %ebx
	cmpl	%ebx, %r13d
	movl	G__align11_noalign.orlgth2(%rip), %ecx
	jg	.LBB1_5
# BB#4:
	cmpl	%ecx, %r12d
	jle	.LBB1_9
.LBB1_5:
	testl	%ebx, %ebx
	movl	%r14d, 40(%rsp)         # 4-byte Spill
	jle	.LBB1_8
# BB#6:
	testl	%ecx, %ecx
	jle	.LBB1_8
# BB#7:
	movq	G__align11_noalign.w1(%rip), %rdi
	callq	FreeFloatVec
	movq	G__align11_noalign.w2(%rip), %rdi
	callq	FreeFloatVec
	movq	G__align11_noalign.match(%rip), %rdi
	callq	FreeFloatVec
	movq	G__align11_noalign.initverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	G__align11_noalign.lastverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	G__align11_noalign.m(%rip), %rdi
	callq	FreeFloatVec
	movq	G__align11_noalign.floatwork(%rip), %rdi
	callq	FreeFloatMtx
	movq	G__align11_noalign.intwork(%rip), %rdi
	callq	FreeIntMtx
	movl	G__align11_noalign.orlgth1(%rip), %ebx
	movl	G__align11_noalign.orlgth2(%rip), %ecx
.LBB1_8:
	cvtsi2sdl	%r13d, %xmm0
	movsd	.LCPI1_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%ebx, %eax
	cmovgel	%eax, %ebx
	leal	100(%rbx), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%ecx, %eax
	cmovgel	%eax, %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%r13, 24(%rsp)          # 8-byte Spill
	leal	100(%rcx), %r13d
	leal	102(%rcx), %r14d
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, G__align11_noalign.w1(%rip)
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, G__align11_noalign.w2(%rip)
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, G__align11_noalign.match(%rip)
	leal	102(%rbx), %ebp
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, G__align11_noalign.initverticalw(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, G__align11_noalign.lastverticalw(%rip)
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, G__align11_noalign.m(%rip)
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	%r13d, %eax
	cmovgel	%eax, %r13d
	addl	$2, %r13d
	movl	$26, %edi
	movl	%r13d, %esi
	callq	AllocateFloatMtx
	movq	%rax, G__align11_noalign.floatwork(%rip)
	movl	$26, %edi
	movl	%r13d, %esi
	movq	24(%rsp), %r13          # 8-byte Reload
	callq	AllocateIntMtx
	movq	%rax, G__align11_noalign.intwork(%rip)
	movl	%ebx, G__align11_noalign.orlgth1(%rip)
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	%eax, G__align11_noalign.orlgth2(%rip)
	movl	40(%rsp), %r14d         # 4-byte Reload
.LBB1_9:
	movq	G__align11_noalign.w1(%rip), %r11
	movq	G__align11_noalign.w2(%rip), %r9
	movq	G__align11_noalign.initverticalw(%rip), %rbp
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	je	.LBB1_15
# BB#10:                                # %.lr.ph.i.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	movsbq	(%rax), %rcx
	testb	$1, %r13b
	movq	%rbp, %rdi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	%r13d, %esi
	je	.LBB1_12
# BB#11:                                # %.lr.ph.i.prol
	leal	-1(%r13), %esi
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	1(%rax), %rdx
	movsbq	(%rax), %rax
	movq	%rcx, %rdi
	shlq	$9, %rdi
	addq	%r15, %rdi
	cvtsi2ssl	(%rdi,%rax,4), %xmm0
	leaq	4(%rbp), %rdi
	movss	%xmm0, (%rbp)
.LBB1_12:                               # %.lr.ph.i.prol.loopexit
	cmpl	$1, %r13d
	je	.LBB1_15
# BB#13:                                # %.lr.ph.i.preheader.new
	shlq	$9, %rcx
	addq	%r15, %rcx
	.p2align	4, 0x90
.LBB1_14:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movsbq	(%rdx), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	(%rcx,%rax,4), %xmm0
	movss	%xmm0, (%rdi)
	addl	$-2, %esi
	movsbq	1(%rdx), %rax
	leaq	2(%rdx), %rdx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	(%rcx,%rax,4), %xmm0
	movss	%xmm0, 4(%rdi)
	leaq	8(%rdi), %rdi
	jne	.LBB1_14
.LBB1_15:                               # %match_calc_mtx.exit
	testl	%r12d, %r12d
	je	.LBB1_21
# BB#16:                                # %.lr.ph.i171.preheader
	movq	32(%rsp), %rax          # 8-byte Reload
	movsbq	(%rax), %rcx
	testb	$1, %r12b
	movq	%r11, %rdi
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%r12d, %esi
	je	.LBB1_18
# BB#17:                                # %.lr.ph.i171.prol
	leal	-1(%r12), %esi
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	1(%rax), %rdx
	movsbq	(%rax), %rax
	movq	%rcx, %rdi
	shlq	$9, %rdi
	addq	%r15, %rdi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	(%rdi,%rax,4), %xmm0
	leaq	4(%r11), %rdi
	movss	%xmm0, (%r11)
.LBB1_18:                               # %.lr.ph.i171.prol.loopexit
	cmpl	$1, %r12d
	je	.LBB1_21
# BB#19:                                # %.lr.ph.i171.preheader.new
	shlq	$9, %rcx
	addq	%r15, %rcx
	.p2align	4, 0x90
.LBB1_20:                               # %.lr.ph.i171
                                        # =>This Inner Loop Header: Depth=1
	movsbq	(%rdx), %rax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	(%rcx,%rax,4), %xmm0
	movss	%xmm0, (%rdi)
	addl	$-2, %esi
	movsbq	1(%rdx), %rax
	leaq	2(%rdx), %rdx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	(%rcx,%rax,4), %xmm0
	movss	%xmm0, 4(%rdi)
	leaq	8(%rdi), %rdi
	jne	.LBB1_20
.LBB1_21:                               # %match_calc_mtx.exit172
	cvtsi2ssl	%r14d, %xmm1
	movl	outgap(%rip), %r8d
	cmpl	$1, %r8d
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	jne	.LBB1_34
# BB#22:                                # %.preheader174
	testl	%r13d, %r13d
	jle	.LBB1_31
# BB#23:                                # %.lr.ph195
	leal	1(%r13), %ecx
	leaq	-1(%rcx), %rsi
	cmpq	$7, %rsi
	jbe	.LBB1_24
# BB#27:                                # %min.iters.checked
	movl	%r13d, %edi
	andl	$7, %edi
	movq	%rsi, %rdx
	subq	%rdi, %rdx
	subq	%rdi, %rsi
	je	.LBB1_24
# BB#28:                                # %vector.ph
	incq	%rdx
	movaps	%xmm1, %xmm0
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	20(%rbp), %rax
	.p2align	4, 0x90
.LBB1_29:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rax), %xmm2
	movups	(%rax), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm0, %xmm3
	movups	%xmm2, -16(%rax)
	movups	%xmm3, (%rax)
	addq	$32, %rax
	addq	$-8, %rsi
	jne	.LBB1_29
# BB#30:                                # %middle.block
	testq	%rdi, %rdi
	jne	.LBB1_25
	jmp	.LBB1_31
.LBB1_24:
	movl	$1, %edx
.LBB1_25:                               # %scalar.ph.preheader
	leaq	(%rbp,%rdx,4), %rax
	subq	%rdx, %rcx
	.p2align	4, 0x90
.LBB1_26:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rax)
	addq	$4, %rax
	decq	%rcx
	jne	.LBB1_26
.LBB1_31:                               # %.preheader173
	testl	%r12d, %r12d
	jle	.LBB1_49
# BB#32:                                # %.lr.ph193.preheader
	leal	1(%r12), %ecx
	leaq	-1(%rcx), %rsi
	cmpq	$7, %rsi
	jbe	.LBB1_33
# BB#55:                                # %min.iters.checked237
	movl	%r12d, %edi
	andl	$7, %edi
	movq	%rsi, %rdx
	subq	%rdi, %rdx
	subq	%rdi, %rsi
	je	.LBB1_33
# BB#56:                                # %vector.ph241
	incq	%rdx
	movaps	%xmm1, %xmm0
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	20(%r11), %rax
	.p2align	4, 0x90
.LBB1_57:                               # %vector.body233
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rax), %xmm2
	movups	(%rax), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm0, %xmm3
	movups	%xmm2, -16(%rax)
	movups	%xmm3, (%rax)
	addq	$32, %rax
	addq	$-8, %rsi
	jne	.LBB1_57
# BB#58:                                # %middle.block234
	testq	%rdi, %rdi
	jne	.LBB1_59
	jmp	.LBB1_34
.LBB1_33:
	movl	$1, %edx
.LBB1_59:                               # %.lr.ph193.preheader284
	leaq	(%r11,%rdx,4), %rax
	subq	%rdx, %rcx
	.p2align	4, 0x90
.LBB1_60:                               # %.lr.ph193
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rax)
	addq	$4, %rax
	decq	%rcx
	jne	.LBB1_60
.LBB1_34:                               # %.preheader
	testl	%r12d, %r12d
	jle	.LBB1_49
# BB#35:                                # %.lr.ph190
	movq	G__align11_noalign.m(%rip), %r14
	leaq	1(%r12), %rdi
	movl	%edi, %ecx
	leaq	-1(%rcx), %rbx
	cmpq	$7, %rbx
	jbe	.LBB1_36
# BB#43:                                # %min.iters.checked260
	movl	%r12d, %r10d
	andl	$7, %r10d
	movq	%rbx, %rsi
	subq	%r10, %rsi
	je	.LBB1_36
# BB#44:                                # %vector.memcheck
	leaq	4(%r14), %rax
	leaq	-4(%r11,%rcx,4), %rdx
	cmpq	%rdx, %rax
	jae	.LBB1_46
# BB#45:                                # %vector.memcheck
	leaq	(%r14,%rcx,4), %rax
	cmpq	%rax, %r11
	jae	.LBB1_46
.LBB1_36:
	movl	$1, %ebp
.LBB1_37:                               # %scalar.ph258.preheader
	subl	%ebp, %edi
	subq	%rbp, %rbx
	andq	$3, %rdi
	je	.LBB1_40
# BB#38:                                # %scalar.ph258.prol.preheader
	negq	%rdi
	.p2align	4, 0x90
.LBB1_39:                               # %scalar.ph258.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r11,%rbp,4), %eax
	movl	%eax, (%r14,%rbp,4)
	incq	%rbp
	incq	%rdi
	jne	.LBB1_39
.LBB1_40:                               # %scalar.ph258.prol.loopexit
	cmpq	$3, %rbx
	jb	.LBB1_49
# BB#41:                                # %scalar.ph258.preheader.new
	subq	%rbp, %rcx
	leaq	12(%r14,%rbp,4), %rdx
	leaq	(%r11,%rbp,4), %rsi
	.p2align	4, 0x90
.LBB1_42:                               # %scalar.ph258
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rsi), %eax
	movl	%eax, -12(%rdx)
	movl	(%rsi), %eax
	movl	%eax, -8(%rdx)
	movl	4(%rsi), %eax
	movl	%eax, -4(%rdx)
	movl	8(%rsi), %eax
	movl	%eax, (%rdx)
	addq	$16, %rdx
	addq	$16, %rsi
	addq	$-4, %rcx
	jne	.LBB1_42
.LBB1_49:                               # %._crit_edge191
	testl	%r12d, %r12d
	xorpd	%xmm0, %xmm0
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	xorps	%xmm2, %xmm2
	je	.LBB1_51
# BB#50:
	movq	%r12, %rax
	shlq	$32, %rax
	addq	%rcx, %rax
	sarq	$30, %rax
	movss	(%r11,%rax), %xmm2      # xmm2 = mem[0],zero,zero,zero
.LBB1_51:
	movq	G__align11_noalign.lastverticalw(%rip), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movss	%xmm2, (%rax)
	cmpl	$1, %r8d
	sbbl	$-1, %r13d
	cmpl	$2, %r13d
	jl	.LBB1_80
# BB#52:                                # %.lr.ph186
	movq	%r12, %r14
	shlq	$32, %r14
	addq	%rcx, %r14
	sarq	$32, %r14
	testl	%r12d, %r12d
	je	.LBB1_53
# BB#66:                                # %.lr.ph186.split.preheader
	movl	56(%rsp), %eax          # 4-byte Reload
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%eax, %xmm2
	movq	G__align11_noalign.m(%rip), %rcx
	leal	1(%r12), %edx
	movl	%r13d, %r13d
	movl	%r12d, %r10d
	andl	$1, %r10d
	leal	-1(%r12), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	incq	%rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	addq	$4, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	addq	$-2, %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movl	$1, %r8d
	movq	24(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_67:                               # %.lr.ph186.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_70 Depth 2
                                        #     Child Loop BB1_73 Depth 2
	movq	%r9, %rdi
	testl	%r10d, %r10d
	movl	-4(%rbx,%r8,4), %ecx
	movl	%ecx, (%r11)
	movq	32(%rsp), %rax          # 8-byte Reload
	movsbq	(%rax,%r8), %rbx
	movq	%rdi, %rdx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%r12d, %ebp
	je	.LBB1_69
# BB#68:                                # %.lr.ph.i166.prol
                                        #   in Loop: Header=BB1_67 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movsbq	(%rax), %rdx
	movq	%rbx, %rsi
	shlq	$9, %rsi
	addq	%r15, %rsi
	xorps	%xmm3, %xmm3
	cvtsi2ssl	(%rsi,%rdx,4), %xmm3
	leaq	4(%rdi), %rdx
	movss	%xmm3, (%rdi)
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	20(%rsp), %ebp          # 4-byte Reload
.LBB1_69:                               # %.lr.ph.i166.prol.loopexit
                                        #   in Loop: Header=BB1_67 Depth=1
	cmpl	$1, %r12d
	je	.LBB1_71
	.p2align	4, 0x90
.LBB1_70:                               # %.lr.ph.i166
                                        #   Parent Loop BB1_67 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	(%rsi), %rax
	movq	%rbx, %rcx
	shlq	$9, %rcx
	addq	%r15, %rcx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	(%rcx,%rax,4), %xmm3
	movss	%xmm3, (%rdx)
	addl	$-2, %ebp
	movsbq	1(%rsi), %rax
	leaq	2(%rsi), %rsi
	xorps	%xmm3, %xmm3
	cvtsi2ssl	(%rcx,%rax,4), %xmm3
	movss	%xmm3, 4(%rdx)
	leaq	8(%rdx), %rdx
	jne	.LBB1_70
.LBB1_71:                               # %match_calc_mtx.exit167
                                        #   in Loop: Header=BB1_67 Depth=1
	testl	%r12d, %r12d
	movq	24(%rsp), %rbx          # 8-byte Reload
	movl	(%rbx,%r8,4), %eax
	movl	%eax, (%rdi)
	movl	(%r11), %ecx
	movl	%ecx, G__align11_noalign.mi(%rip)
	jle	.LBB1_79
# BB#72:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_67 Depth=1
	movd	%ecx, %xmm3
	leaq	4(%rdi), %rsi
	leaq	4(%r11), %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
	movdqa	%xmm3, %xmm0
	jmp	.LBB1_73
	.p2align	4, 0x90
.LBB1_78:                               # %..lr.ph_crit_edge
                                        #   in Loop: Header=BB1_73 Depth=2
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rbp
	decq	%rcx
	addq	$4, %rsi
	addq	$4, %rdx
.LBB1_73:                               # %.lr.ph
                                        #   Parent Loop BB1_67 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, %xmm4
	addss	%xmm3, %xmm4
	maxss	%xmm0, %xmm4
	ucomiss	%xmm3, %xmm0
	jb	.LBB1_75
# BB#74:                                #   in Loop: Header=BB1_73 Depth=2
	movss	%xmm0, G__align11_noalign.mi(%rip)
	movaps	%xmm0, %xmm3
.LBB1_75:                               #   in Loop: Header=BB1_73 Depth=2
	addss	%xmm2, %xmm3
	movss	%xmm3, G__align11_noalign.mi(%rip)
	movss	(%rbp), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	addss	%xmm5, %xmm0
	maxss	%xmm4, %xmm0
	movss	-4(%rdx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm5, %xmm4
	jb	.LBB1_77
# BB#76:                                #   in Loop: Header=BB1_73 Depth=2
	movss	%xmm4, (%rbp)
.LBB1_77:                               #   in Loop: Header=BB1_73 Depth=2
	movss	(%rbp), %xmm4           # xmm4 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm4
	movss	%xmm4, (%rbp)
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	movss	%xmm4, (%rsi)
	testq	%rcx, %rcx
	jne	.LBB1_78
.LBB1_79:                               # %._crit_edge
                                        #   in Loop: Header=BB1_67 Depth=1
	movl	(%rdi,%r14,4), %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx,%r8,4)
	incq	%r8
	cmpq	%r13, %r8
	movq	%r11, %r9
	movq	%rdi, %r11
	jne	.LBB1_67
	jmp	.LBB1_80
.LBB1_53:                               # %._crit_edge.us.preheader
	testb	$1, %r13b
	jne	.LBB1_54
# BB#61:                                # %._crit_edge.us.prol
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	(%rdi), %eax
	movl	%eax, (%r11)
	movl	4(%rdi), %eax
	movl	%eax, (%r9)
	movl	(%r11), %edx
	movl	(%r9,%r14,4), %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 4(%rcx)
	movl	$2, %ebp
	movq	%r11, %rcx
	movq	%r9, %r11
	cmpl	$2, %r13d
	jne	.LBB1_63
	jmp	.LBB1_65
.LBB1_54:
	movl	$1, %ebp
                                        # implicit-def: %EDX
	movq	%r9, %rcx
	movq	24(%rsp), %rdi          # 8-byte Reload
	cmpl	$2, %r13d
	je	.LBB1_65
.LBB1_63:                               # %._crit_edge.us.preheader.new
	movl	%r13d, %esi
	subq	%rbp, %rsi
	leaq	(%rdi,%rbp,4), %rdi
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	4(%rax,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB1_64:                               # %._crit_edge.us
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rdi), %eax
	movl	%eax, (%r11)
	movl	(%rdi), %eax
	movl	%eax, (%rcx)
	movl	(%rcx,%r14,4), %eax
	movl	%eax, -4(%rbp)
	movl	(%rdi), %eax
	movl	%eax, (%rcx)
	movl	4(%rdi), %eax
	movl	%eax, (%r11)
	movl	(%rcx), %edx
	movl	(%r11,%r14,4), %eax
	movl	%eax, (%rbp)
	addq	$8, %rdi
	addq	$8, %rbp
	addq	$-2, %rsi
	jne	.LBB1_64
.LBB1_65:                               # %._crit_edge187.loopexit
	movl	%edx, G__align11_noalign.mi(%rip)
.LBB1_80:                               # %._crit_edge187
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_46:                               # %vector.body256.preheader
	leaq	16(%r11), %rdx
	leaq	20(%r14), %rax
	leaq	1(%rsi), %rbp
	.p2align	4, 0x90
.LBB1_47:                               # %vector.body256
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rdx), %xmm0
	movups	(%rdx), %xmm2
	movupd	%xmm0, -16(%rax)
	movups	%xmm2, (%rax)
	addq	$32, %rdx
	addq	$32, %rax
	addq	$-8, %rsi
	jne	.LBB1_47
# BB#48:                                # %middle.block257
	testq	%r10, %r10
	jne	.LBB1_37
	jmp	.LBB1_49
.LBB1_2:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	movl	%r12d, %ecx
	callq	fprintf
	jmp	.LBB1_3
.Lfunc_end1:
	.size	G__align11_noalign, .Lfunc_end1-G__align11_noalign
	.cfi_endproc

	.type	G__align11.mi,@object   # @G__align11.mi
	.local	G__align11.mi
	.comm	G__align11.mi,4,4
	.type	G__align11.m,@object    # @G__align11.m
	.local	G__align11.m
	.comm	G__align11.m,8,8
	.type	G__align11.ijp,@object  # @G__align11.ijp
	.local	G__align11.ijp
	.comm	G__align11.ijp,8,8
	.type	G__align11.mpi,@object  # @G__align11.mpi
	.local	G__align11.mpi
	.comm	G__align11.mpi,4,4
	.type	G__align11.mp,@object   # @G__align11.mp
	.local	G__align11.mp
	.comm	G__align11.mp,8,8
	.type	G__align11.w1,@object   # @G__align11.w1
	.local	G__align11.w1
	.comm	G__align11.w1,8,8
	.type	G__align11.w2,@object   # @G__align11.w2
	.local	G__align11.w2
	.comm	G__align11.w2,8,8
	.type	G__align11.match,@object # @G__align11.match
	.local	G__align11.match
	.comm	G__align11.match,8,8
	.type	G__align11.initverticalw,@object # @G__align11.initverticalw
	.local	G__align11.initverticalw
	.comm	G__align11.initverticalw,8,8
	.type	G__align11.lastverticalw,@object # @G__align11.lastverticalw
	.local	G__align11.lastverticalw
	.comm	G__align11.lastverticalw,8,8
	.type	G__align11.mseq1,@object # @G__align11.mseq1
	.local	G__align11.mseq1
	.comm	G__align11.mseq1,8,8
	.type	G__align11.mseq2,@object # @G__align11.mseq2
	.local	G__align11.mseq2
	.comm	G__align11.mseq2,8,8
	.type	G__align11.mseq,@object # @G__align11.mseq
	.local	G__align11.mseq
	.comm	G__align11.mseq,8,8
	.type	G__align11.intwork,@object # @G__align11.intwork
	.local	G__align11.intwork
	.comm	G__align11.intwork,8,8
	.type	G__align11.floatwork,@object # @G__align11.floatwork
	.local	G__align11.floatwork
	.comm	G__align11.floatwork,8,8
	.type	G__align11.orlgth1,@object # @G__align11.orlgth1
	.local	G__align11.orlgth1
	.comm	G__align11.orlgth1,4,4
	.type	G__align11.orlgth2,@object # @G__align11.orlgth2
	.local	G__align11.orlgth2
	.comm	G__align11.orlgth2,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"WARNING (g11): lgth1=%d, lgth2=%d\n"
	.size	.L.str, 35

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"alloclen=%d, resultlen=%d, N=%d\n"
	.size	.L.str.1, 33

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"LENGTH OVER!\n"
	.size	.L.str.2, 14

	.type	G__align11_noalign.mi,@object # @G__align11_noalign.mi
	.local	G__align11_noalign.mi
	.comm	G__align11_noalign.mi,4,4
	.type	G__align11_noalign.m,@object # @G__align11_noalign.m
	.local	G__align11_noalign.m
	.comm	G__align11_noalign.m,8,8
	.type	G__align11_noalign.w1,@object # @G__align11_noalign.w1
	.local	G__align11_noalign.w1
	.comm	G__align11_noalign.w1,8,8
	.type	G__align11_noalign.w2,@object # @G__align11_noalign.w2
	.local	G__align11_noalign.w2
	.comm	G__align11_noalign.w2,8,8
	.type	G__align11_noalign.match,@object # @G__align11_noalign.match
	.local	G__align11_noalign.match
	.comm	G__align11_noalign.match,8,8
	.type	G__align11_noalign.initverticalw,@object # @G__align11_noalign.initverticalw
	.local	G__align11_noalign.initverticalw
	.comm	G__align11_noalign.initverticalw,8,8
	.type	G__align11_noalign.lastverticalw,@object # @G__align11_noalign.lastverticalw
	.local	G__align11_noalign.lastverticalw
	.comm	G__align11_noalign.lastverticalw,8,8
	.type	G__align11_noalign.intwork,@object # @G__align11_noalign.intwork
	.local	G__align11_noalign.intwork
	.comm	G__align11_noalign.intwork,8,8
	.type	G__align11_noalign.floatwork,@object # @G__align11_noalign.floatwork
	.local	G__align11_noalign.floatwork
	.comm	G__align11_noalign.floatwork,8,8
	.type	G__align11_noalign.orlgth1,@object # @G__align11_noalign.orlgth1
	.local	G__align11_noalign.orlgth1
	.comm	G__align11_noalign.orlgth1,4,4
	.type	G__align11_noalign.orlgth2,@object # @G__align11_noalign.orlgth2
	.local	G__align11_noalign.orlgth2
	.comm	G__align11_noalign.orlgth2,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
