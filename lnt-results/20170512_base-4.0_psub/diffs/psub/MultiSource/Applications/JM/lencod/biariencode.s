	.text
	.file	"biariencode.bc"
	.globl	reset_pic_bin_count
	.p2align	4, 0x90
	.type	reset_pic_bin_count,@function
reset_pic_bin_count:                    # @reset_pic_bin_count
	.cfi_startproc
# BB#0:
	movl	$0, pic_bin_count(%rip)
	retq
.Lfunc_end0:
	.size	reset_pic_bin_count, .Lfunc_end0-reset_pic_bin_count
	.cfi_endproc

	.globl	get_pic_bin_count
	.p2align	4, 0x90
	.type	get_pic_bin_count,@function
get_pic_bin_count:                      # @get_pic_bin_count
	.cfi_startproc
# BB#0:
	movl	pic_bin_count(%rip), %eax
	retq
.Lfunc_end1:
	.size	get_pic_bin_count, .Lfunc_end1-get_pic_bin_count
	.cfi_endproc

	.globl	arienco_create_encoding_environment
	.p2align	4, 0x90
	.type	arienco_create_encoding_environment,@function
arienco_create_encoding_environment:    # @arienco_create_encoding_environment
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	$1, %edi
	movl	$48, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB2_2
# BB#1:
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB2_2:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	arienco_create_encoding_environment, .Lfunc_end2-arienco_create_encoding_environment
	.cfi_endproc

	.globl	arienco_delete_encoding_environment
	.p2align	4, 0x90
	.type	arienco_delete_encoding_environment,@function
arienco_delete_encoding_environment:    # @arienco_delete_encoding_environment
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB3_1
# BB#2:
	jmp	free                    # TAILCALL
.LBB3_1:
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 16
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$200, %esi
	popq	%rax
	jmp	error                   # TAILCALL
.Lfunc_end3:
	.size	arienco_delete_encoding_environment, .Lfunc_end3-arienco_delete_encoding_environment
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	0                       # 0x0
	.long	510                     # 0x1fe
	.long	0                       # 0x0
	.long	9                       # 0x9
	.text
	.globl	arienco_start_encoding
	.p2align	4, 0x90
	.type	arienco_start_encoding,@function
arienco_start_encoding:                 # @arienco_start_encoding
	.cfi_startproc
# BB#0:
	movl	$0, 16(%rdi)
	movq	%rsi, 24(%rdi)
	movq	%rdx, 32(%rdi)
	movaps	.LCPI4_0(%rip), %xmm0   # xmm0 = [0,510,0,9]
	movups	%xmm0, (%rdi)
	movq	$0, 40(%rdi)
	retq
.Lfunc_end4:
	.size	arienco_start_encoding, .Lfunc_end4-arienco_start_encoding
	.cfi_endproc

	.globl	arienco_bits_written
	.p2align	4, 0x90
	.type	arienco_bits_written,@function
arienco_bits_written:                   # @arienco_bits_written
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rax
	movl	(%rax), %eax
	movl	16(%rdi), %ecx
	leal	8(%rcx,%rax,8), %eax
	subl	12(%rdi), %eax
	retq
.Lfunc_end5:
	.size	arienco_bits_written, .Lfunc_end5-arienco_bits_written
	.cfi_endproc

	.globl	arienco_done_encoding
	.p2align	4, 0x90
	.type	arienco_done_encoding,@function
arienco_done_encoding:                  # @arienco_done_encoding
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	movl	8(%rdi), %ecx
	shrl	$9, %eax
	andl	$1, %eax
	leal	(%rax,%rcx,2), %ecx
	movl	%ecx, 8(%rdi)
	movl	12(%rdi), %eax
	jmp	.LBB6_1
	.p2align	4, 0x90
.LBB6_3:                                #   in Loop: Header=BB6_1 Depth=1
	decl	%ecx
	movl	%ecx, 16(%rdi)
	movl	(%rdi), %ecx
	movl	8(%rdi), %edx
	shrl	$9, %ecx
	andl	$1, %ecx
	leal	(%rcx,%rdx,2), %ecx
	xorl	$1, %ecx
	movl	%ecx, 8(%rdi)
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	decl	%eax
	movl	%eax, 12(%rdi)
	jne	.LBB6_2
# BB#4:                                 #   in Loop: Header=BB6_1 Depth=1
	movq	24(%rdi), %r8
	movq	32(%rdi), %rdx
	movslq	(%rdx), %rsi
	leal	1(%rsi), %eax
	movl	%eax, (%rdx)
	movb	%cl, (%r8,%rsi)
	movl	$8, 12(%rdi)
	movl	40(%rdi), %ecx
	movl	$8, %eax
	cmpl	$8, %ecx
	jl	.LBB6_2
# BB#5:                                 # %.lr.ph77
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	44(%rdi), %r8d
	addl	$-8, %ecx
	movl	%ecx, %esi
	shrl	$3, %esi
	leal	(,%rsi,8), %edx
	subl	%edx, %ecx
	leal	1(%r8,%rsi), %edx
	movl	%ecx, 40(%rdi)
	movl	%edx, 44(%rdi)
	.p2align	4, 0x90
.LBB6_2:                                # %.preheader
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	16(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.LBB6_3
# BB#6:                                 # %._crit_edge
	movl	(%rdi), %ecx
	movl	8(%rdi), %edx
	shrl	$8, %ecx
	andl	$1, %ecx
	leal	(%rcx,%rdx,2), %ecx
	movl	%ecx, 8(%rdi)
	decl	%eax
	movl	%eax, 12(%rdi)
	jne	.LBB6_9
# BB#7:
	movq	24(%rdi), %r8
	movq	32(%rdi), %rdx
	movslq	(%rdx), %rsi
	leal	1(%rsi), %eax
	movl	%eax, (%rdx)
	movb	%cl, (%r8,%rsi)
	movl	$8, 12(%rdi)
	movl	40(%rdi), %ecx
	movl	$8, %eax
	cmpl	$8, %ecx
	jl	.LBB6_9
# BB#8:                                 # %.lr.ph65
	movl	44(%rdi), %r8d
	addl	$-8, %ecx
	movl	%ecx, %esi
	shrl	$3, %esi
	leal	(,%rsi,8), %edx
	subl	%edx, %ecx
	leal	1(%r8,%rsi), %edx
	movl	%ecx, 40(%rdi)
	movl	%edx, 44(%rdi)
.LBB6_9:                                # %.loopexit57
	movl	8(%rdi), %ecx
	leal	1(%rcx,%rcx), %ecx
	movl	%ecx, 8(%rdi)
	decl	%eax
	movl	%eax, 12(%rdi)
	leaq	24(%rdi), %r8
	leaq	32(%rdi), %r9
	je	.LBB6_11
# BB#10:                                # %.loopexit57..loopexit_crit_edge
	leaq	40(%rdi), %r11
	jmp	.LBB6_13
.LBB6_11:
	movq	24(%rdi), %r10
	movq	32(%rdi), %rsi
	movslq	(%rsi), %rdx
	leal	1(%rdx), %eax
	movl	%eax, (%rsi)
	movb	%cl, (%r10,%rdx)
	movl	$8, 12(%rdi)
	leaq	40(%rdi), %r11
	movl	40(%rdi), %ecx
	movl	$8, %eax
	cmpl	$8, %ecx
	jl	.LBB6_13
# BB#12:                                # %.lr.ph60
	movl	44(%rdi), %r10d
	addl	$-8, %ecx
	movl	%ecx, %edx
	shrl	$3, %edx
	leal	(,%rdx,8), %esi
	subl	%esi, %ecx
	leal	1(%r10,%rdx), %edx
	movl	%ecx, 40(%rdi)
	movl	%edx, 44(%rdi)
.LBB6_13:                               # %.loopexit
	movl	$8, %ecx
	subl	%eax, %ecx
	movq	stats(%rip), %rdx
	movq	img(%rip), %rsi
	movslq	20(%rsi), %rsi
	addq	%rcx, 1960(%rdx,%rsi,8)
	jmp	.LBB6_14
	.p2align	4, 0x90
.LBB6_17:                               #   in Loop: Header=BB6_14 Depth=1
	movq	(%r8), %r10
	movq	(%r9), %rdx
	movslq	(%rdx), %rsi
	leal	1(%rsi), %eax
	movl	%eax, (%rdx)
	movb	%cl, (%r10,%rsi)
	movl	$8, 12(%rdi)
	movl	(%r11), %ecx
	movl	$8, %eax
	cmpl	$8, %ecx
	jl	.LBB6_14
# BB#18:                                # %.lr.ph
                                        #   in Loop: Header=BB6_14 Depth=1
	movl	44(%rdi), %r10d
	addl	$-8, %ecx
	movl	%ecx, %esi
	shrl	$3, %esi
	leal	(,%rsi,8), %edx
	subl	%edx, %ecx
	leal	1(%r10,%rsi), %edx
	movl	%ecx, (%r11)
	movl	%edx, 44(%rdi)
.LBB6_14:                               # %thread-pre-split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_15 Depth 2
	decl	%eax
	.p2align	4, 0x90
.LBB6_15:                               #   Parent Loop BB6_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$7, %eax
	je	.LBB6_19
# BB#16:                                #   in Loop: Header=BB6_15 Depth=2
	movl	8(%rdi), %ecx
	addl	%ecx, %ecx
	movl	%ecx, 8(%rdi)
	movl	%eax, 12(%rdi)
	decl	%eax
	cmpl	$-1, %eax
	jne	.LBB6_15
	jmp	.LBB6_17
.LBB6_19:
	movl	44(%rdi), %eax
	shll	$3, %eax
	addl	(%r11), %eax
	addl	%eax, pic_bin_count(%rip)
	retq
.Lfunc_end6:
	.size	arienco_done_encoding, .Lfunc_end6-arienco_done_encoding
	.cfi_endproc

	.globl	biari_encode_symbol
	.p2align	4, 0x90
	.type	biari_encode_symbol,@function
biari_encode_symbol:                    # @biari_encode_symbol
	.cfi_startproc
# BB#0:
	movl	(%rdi), %r11d
	movl	4(%rdi), %r10d
	movzwl	(%rdx), %r9d
	movl	%r10d, %ecx
	shrl	$6, %ecx
	andl	$3, %ecx
	movzbl	rLPS_table_64x4(%rcx,%r9,4), %r8d
	subl	%r8d, %r10d
	movslq	cabac_encoding(%rip), %rcx
	addq	%rcx, 8(%rdx)
	xorl	%ecx, %ecx
	testw	%si, %si
	setne	%cl
	movzbl	2(%rdx), %esi
	cmpl	%esi, %ecx
	jne	.LBB7_1
# BB#4:
	movzwl	AC_next_state_MPS_64(%r9,%r9), %ecx
	movw	%cx, (%rdx)
	cmpl	$256, %r10d             # imm = 0x100
	movl	%r10d, %r8d
	jb	.LBB7_5
	jmp	.LBB7_24
.LBB7_1:
	testw	%r9w, %r9w
	jne	.LBB7_3
# BB#2:
	xorb	$1, %sil
	movb	%sil, 2(%rdx)
.LBB7_3:                                # %.thread
	addl	%r10d, %r11d
	movzwl	AC_next_state_LPS_64(%r9,%r9), %ecx
	movw	%cx, (%rdx)
.LBB7_5:                                # %.lr.ph111
	movl	%r8d, %r10d
	.p2align	4, 0x90
.LBB7_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_17 Depth 2
                                        #     Child Loop BB7_8 Depth 2
	cmpl	$512, %r11d             # imm = 0x200
	jb	.LBB7_15
# BB#7:                                 #   in Loop: Header=BB7_6 Depth=1
	movl	8(%rdi), %ecx
	leal	1(%rcx,%rcx), %esi
	movl	%esi, 8(%rdi)
	movl	12(%rdi), %edx
	decl	%edx
	movl	%edx, 12(%rdi)
	jne	.LBB7_8
# BB#12:                                #   in Loop: Header=BB7_6 Depth=1
	movq	24(%rdi), %r8
	movq	32(%rdi), %rdx
	movslq	(%rdx), %r9
	leal	1(%r9), %ecx
	movl	%ecx, (%rdx)
	movb	%sil, (%r8,%r9)
	movl	$8, 12(%rdi)
	movl	40(%rdi), %esi
	movl	$8, %edx
	cmpl	$8, %esi
	jl	.LBB7_8
# BB#13:                                # %.lr.ph97
                                        #   in Loop: Header=BB7_6 Depth=1
	movl	44(%rdi), %r8d
	addl	$-8, %esi
	movl	%esi, %r9d
	shrl	$3, %r9d
	leal	(,%r9,8), %ecx
	subl	%ecx, %esi
	leal	1(%r8,%r9), %ecx
	movl	%esi, 40(%rdi)
	movl	%ecx, 44(%rdi)
	jmp	.LBB7_8
.LBB7_11:                               # %.lr.ph102
                                        #   in Loop: Header=BB7_8 Depth=2
	movl	44(%rdi), %r8d
	addl	$-8, %esi
	movl	%esi, %ecx
	shrl	$3, %ecx
	leal	(,%rcx,8), %eax
	subl	%eax, %esi
	leal	1(%r8,%rcx), %eax
	movl	%esi, 40(%rdi)
	movl	%eax, 44(%rdi)
	.p2align	4, 0x90
.LBB7_8:                                # %.preheader
                                        #   Parent Loop BB7_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	16(%rdi), %esi
	testl	%esi, %esi
	je	.LBB7_14
# BB#9:                                 # %.lr.ph107
                                        #   in Loop: Header=BB7_8 Depth=2
	decl	%esi
	movl	%esi, 16(%rdi)
	movl	8(%rdi), %esi
	addl	%esi, %esi
	movl	%esi, 8(%rdi)
	decl	%edx
	movl	%edx, 12(%rdi)
	jne	.LBB7_8
# BB#10:                                #   in Loop: Header=BB7_8 Depth=2
	movq	24(%rdi), %r8
	movq	32(%rdi), %rdx
	movslq	(%rdx), %rax
	leal	1(%rax), %ecx
	movl	%ecx, (%rdx)
	movb	%sil, (%r8,%rax)
	movl	$8, 12(%rdi)
	movl	40(%rdi), %esi
	movl	$8, %edx
	cmpl	$8, %esi
	jl	.LBB7_8
	jmp	.LBB7_11
	.p2align	4, 0x90
.LBB7_15:                               #   in Loop: Header=BB7_6 Depth=1
	cmpl	$255, %r11d
	ja	.LBB7_22
# BB#16:                                #   in Loop: Header=BB7_6 Depth=1
	movl	8(%rdi), %esi
	addl	%esi, %esi
	movl	%esi, 8(%rdi)
	movl	12(%rdi), %edx
	jmp	.LBB7_17
	.p2align	4, 0x90
.LBB7_19:                               # %.lr.ph96
                                        #   in Loop: Header=BB7_17 Depth=2
	decl	%esi
	movl	%esi, 16(%rdi)
	movl	8(%rdi), %eax
	leal	1(%rax,%rax), %esi
	movl	%esi, 8(%rdi)
.LBB7_17:                               #   Parent Loop BB7_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%edx
	movl	%edx, 12(%rdi)
	jne	.LBB7_18
# BB#20:                                #   in Loop: Header=BB7_17 Depth=2
	movq	24(%rdi), %r8
	movq	32(%rdi), %rcx
	movslq	(%rcx), %rdx
	leal	1(%rdx), %eax
	movl	%eax, (%rcx)
	movb	%sil, (%r8,%rdx)
	movl	$8, 12(%rdi)
	movl	40(%rdi), %esi
	movl	$8, %edx
	cmpl	$8, %esi
	jl	.LBB7_18
# BB#21:                                # %.lr.ph
                                        #   in Loop: Header=BB7_17 Depth=2
	movl	44(%rdi), %r8d
	addl	$-8, %esi
	movl	%esi, %ecx
	shrl	$3, %ecx
	leal	(,%rcx,8), %eax
	subl	%eax, %esi
	leal	1(%r8,%rcx), %eax
	movl	%esi, 40(%rdi)
	movl	%eax, 44(%rdi)
	.p2align	4, 0x90
.LBB7_18:                               # %.preheader87
                                        #   in Loop: Header=BB7_17 Depth=2
	movl	16(%rdi), %esi
	testl	%esi, %esi
	jne	.LBB7_19
	jmp	.LBB7_23
	.p2align	4, 0x90
.LBB7_14:                               # %._crit_edge
                                        #   in Loop: Header=BB7_6 Depth=1
	addl	$-512, %r11d            # imm = 0xFE00
	jmp	.LBB7_23
	.p2align	4, 0x90
.LBB7_22:                               #   in Loop: Header=BB7_6 Depth=1
	incl	16(%rdi)
	addl	$-256, %r11d
.LBB7_23:                               # %.loopexit88
                                        #   in Loop: Header=BB7_6 Depth=1
	addl	%r11d, %r11d
	addl	%r10d, %r10d
	cmpl	$256, %r10d             # imm = 0x100
	jb	.LBB7_6
.LBB7_24:                               # %._crit_edge112
	movl	%r10d, 4(%rdi)
	movl	%r11d, (%rdi)
	incl	40(%rdi)
	retq
.Lfunc_end7:
	.size	biari_encode_symbol, .Lfunc_end7-biari_encode_symbol
	.cfi_endproc

	.globl	biari_encode_symbol_eq_prob
	.p2align	4, 0x90
	.type	biari_encode_symbol_eq_prob,@function
biari_encode_symbol_eq_prob:            # @biari_encode_symbol_eq_prob
	.cfi_startproc
# BB#0:
	movl	(%rdi), %r10d
	addl	%r10d, %r10d
	testw	%si, %si
	je	.LBB8_2
# BB#1:
	addl	4(%rdi), %r10d
.LBB8_2:
	cmpl	$1024, %r10d            # imm = 0x400
	jb	.LBB8_11
# BB#3:
	movl	8(%rdi), %ecx
	leal	1(%rcx,%rcx), %edx
	movl	%edx, 8(%rdi)
	movl	12(%rdi), %ecx
	decl	%ecx
	movl	%ecx, 12(%rdi)
	jne	.LBB8_4
# BB#9:
	movq	24(%rdi), %r8
	movq	32(%rdi), %rsi
	movslq	(%rsi), %r9
	leal	1(%r9), %ecx
	movl	%ecx, (%rsi)
	movb	%dl, (%r8,%r9)
	movl	$8, 12(%rdi)
	movl	40(%rdi), %edx
	movl	$8, %ecx
	cmpl	$8, %edx
	jl	.LBB8_4
# BB#10:                                # %.lr.ph62
	movl	44(%rdi), %r8d
	addl	$-8, %edx
	movl	%edx, %r9d
	shrl	$3, %r9d
	leal	(,%r9,8), %esi
	subl	%esi, %edx
	leal	1(%r8,%r9), %esi
	movl	%edx, 40(%rdi)
	movl	%esi, 44(%rdi)
	jmp	.LBB8_4
.LBB8_7:                                # %.lr.ph
                                        #   in Loop: Header=BB8_4 Depth=1
	movl	44(%rdi), %r8d
	addl	$-8, %edx
	movl	%edx, %esi
	shrl	$3, %esi
	leal	(,%rsi,8), %eax
	subl	%eax, %edx
	leal	1(%r8,%rsi), %eax
	movl	%edx, 40(%rdi)
	movl	%eax, 44(%rdi)
	.p2align	4, 0x90
.LBB8_4:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	16(%rdi), %edx
	testl	%edx, %edx
	je	.LBB8_8
# BB#5:                                 #   in Loop: Header=BB8_4 Depth=1
	decl	%edx
	movl	%edx, 16(%rdi)
	movl	8(%rdi), %edx
	addl	%edx, %edx
	movl	%edx, 8(%rdi)
	decl	%ecx
	movl	%ecx, 12(%rdi)
	jne	.LBB8_4
# BB#6:                                 #   in Loop: Header=BB8_4 Depth=1
	movq	24(%rdi), %r8
	movq	32(%rdi), %rsi
	movslq	(%rsi), %rax
	leal	1(%rax), %ecx
	movl	%ecx, (%rsi)
	movb	%dl, (%r8,%rax)
	movl	$8, 12(%rdi)
	movl	40(%rdi), %edx
	movl	$8, %ecx
	cmpl	$8, %edx
	jl	.LBB8_4
	jmp	.LBB8_7
.LBB8_11:
	cmpl	$511, %r10d             # imm = 0x1FF
	ja	.LBB8_18
# BB#12:
	movl	8(%rdi), %edx
	addl	%edx, %edx
	movl	%edx, 8(%rdi)
	movl	12(%rdi), %ecx
	jmp	.LBB8_13
	.p2align	4, 0x90
.LBB8_15:                               #   in Loop: Header=BB8_13 Depth=1
	decl	%edx
	movl	%edx, 16(%rdi)
	movl	8(%rdi), %eax
	leal	1(%rax,%rax), %edx
	movl	%edx, 8(%rdi)
.LBB8_13:                               # =>This Inner Loop Header: Depth=1
	decl	%ecx
	movl	%ecx, 12(%rdi)
	jne	.LBB8_14
# BB#16:                                #   in Loop: Header=BB8_13 Depth=1
	movq	24(%rdi), %r8
	movq	32(%rdi), %rcx
	movslq	(%rcx), %rsi
	leal	1(%rsi), %eax
	movl	%eax, (%rcx)
	movb	%dl, (%r8,%rsi)
	movl	$8, 12(%rdi)
	movl	40(%rdi), %edx
	movl	$8, %ecx
	cmpl	$8, %edx
	jl	.LBB8_14
# BB#17:                                # %.lr.ph75
                                        #   in Loop: Header=BB8_13 Depth=1
	movl	44(%rdi), %r8d
	addl	$-8, %edx
	movl	%edx, %esi
	shrl	$3, %esi
	leal	(,%rsi,8), %eax
	subl	%eax, %edx
	leal	1(%r8,%rsi), %eax
	movl	%edx, 40(%rdi)
	movl	%eax, 44(%rdi)
	.p2align	4, 0x90
.LBB8_14:                               # %.preheader56
                                        #   in Loop: Header=BB8_13 Depth=1
	movl	16(%rdi), %edx
	testl	%edx, %edx
	jne	.LBB8_15
	jmp	.LBB8_19
.LBB8_8:                                # %._crit_edge
	addl	$-1024, %r10d           # imm = 0xFC00
	jmp	.LBB8_19
.LBB8_18:
	incl	16(%rdi)
	addl	$-512, %r10d            # imm = 0xFE00
.LBB8_19:                               # %.loopexit57
	movl	%r10d, (%rdi)
	incl	40(%rdi)
	retq
.Lfunc_end8:
	.size	biari_encode_symbol_eq_prob, .Lfunc_end8-biari_encode_symbol_eq_prob
	.cfi_endproc

	.globl	biari_encode_symbol_final
	.p2align	4, 0x90
	.type	biari_encode_symbol_final,@function
biari_encode_symbol_final:              # @biari_encode_symbol_final
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 24
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movl	4(%rdi), %edx
	addl	$-2, %edx
	xorl	%ebp, %ebp
	testw	%si, %si
	movl	$2, %r11d
	cmovel	%edx, %r11d
	cmovnel	%edx, %ebp
	addl	(%rdi), %ebp
	cmpl	$256, %r11d             # imm = 0x100
	jae	.LBB9_1
	.p2align	4, 0x90
.LBB9_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_13 Depth 2
                                        #     Child Loop BB9_4 Depth 2
	cmpl	$512, %ebp              # imm = 0x200
	jb	.LBB9_11
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	movl	8(%rdi), %edx
	leal	1(%rdx,%rdx), %esi
	movl	%esi, 8(%rdi)
	movl	12(%rdi), %edx
	decl	%edx
	movl	%edx, 12(%rdi)
	jne	.LBB9_4
# BB#8:                                 #   in Loop: Header=BB9_2 Depth=1
	movq	24(%rdi), %r8
	movq	32(%rdi), %r10
	movslq	(%r10), %r9
	leal	1(%r9), %edx
	movl	%edx, (%r10)
	movb	%sil, (%r8,%r9)
	movl	$8, 12(%rdi)
	movl	40(%rdi), %r10d
	movl	$8, %edx
	cmpl	$8, %r10d
	jl	.LBB9_4
# BB#9:                                 # %.lr.ph77
                                        #   in Loop: Header=BB9_2 Depth=1
	movl	44(%rdi), %r8d
	addl	$-8, %r10d
	movl	%r10d, %r9d
	shrl	$3, %r9d
	leal	(,%r9,8), %esi
	subl	%esi, %r10d
	leal	1(%r8,%r9), %esi
	movl	%r10d, 40(%rdi)
	movl	%esi, 44(%rdi)
	jmp	.LBB9_4
.LBB9_7:                                # %.lr.ph82
                                        #   in Loop: Header=BB9_4 Depth=2
	movl	44(%rdi), %eax
	addl	$-8, %esi
	movl	%esi, %ecx
	shrl	$3, %ecx
	leal	(,%rcx,8), %ebx
	subl	%ebx, %esi
	leal	1(%rax,%rcx), %eax
	movl	%esi, 40(%rdi)
	movl	%eax, 44(%rdi)
	.p2align	4, 0x90
.LBB9_4:                                # %.preheader
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	16(%rdi), %esi
	testl	%esi, %esi
	je	.LBB9_10
# BB#5:                                 # %.lr.ph87
                                        #   in Loop: Header=BB9_4 Depth=2
	decl	%esi
	movl	%esi, 16(%rdi)
	movl	8(%rdi), %esi
	addl	%esi, %esi
	movl	%esi, 8(%rdi)
	decl	%edx
	movl	%edx, 12(%rdi)
	jne	.LBB9_4
# BB#6:                                 #   in Loop: Header=BB9_4 Depth=2
	movq	24(%rdi), %rdx
	movq	32(%rdi), %rcx
	movslq	(%rcx), %rax
	leal	1(%rax), %ebx
	movl	%ebx, (%rcx)
	movb	%sil, (%rdx,%rax)
	movl	$8, 12(%rdi)
	movl	40(%rdi), %esi
	movl	$8, %edx
	cmpl	$8, %esi
	jl	.LBB9_4
	jmp	.LBB9_7
	.p2align	4, 0x90
.LBB9_11:                               #   in Loop: Header=BB9_2 Depth=1
	cmpl	$255, %ebp
	ja	.LBB9_18
# BB#12:                                #   in Loop: Header=BB9_2 Depth=1
	movl	8(%rdi), %esi
	addl	%esi, %esi
	movl	%esi, 8(%rdi)
	movl	12(%rdi), %edx
	jmp	.LBB9_13
	.p2align	4, 0x90
.LBB9_15:                               # %.lr.ph76
                                        #   in Loop: Header=BB9_13 Depth=2
	decl	%esi
	movl	%esi, 16(%rdi)
	movl	8(%rdi), %eax
	leal	1(%rax,%rax), %esi
	movl	%esi, 8(%rdi)
.LBB9_13:                               #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%edx
	movl	%edx, 12(%rdi)
	jne	.LBB9_14
# BB#16:                                #   in Loop: Header=BB9_13 Depth=2
	movq	24(%rdi), %rax
	movq	32(%rdi), %rcx
	movslq	(%rcx), %rdx
	leal	1(%rdx), %ebx
	movl	%ebx, (%rcx)
	movb	%sil, (%rax,%rdx)
	movl	$8, 12(%rdi)
	movl	40(%rdi), %esi
	movl	$8, %edx
	cmpl	$8, %esi
	jl	.LBB9_14
# BB#17:                                # %.lr.ph
                                        #   in Loop: Header=BB9_13 Depth=2
	movl	44(%rdi), %eax
	addl	$-8, %esi
	movl	%esi, %ecx
	shrl	$3, %ecx
	leal	(,%rcx,8), %ebx
	subl	%ebx, %esi
	leal	1(%rax,%rcx), %eax
	movl	%esi, 40(%rdi)
	movl	%eax, 44(%rdi)
	.p2align	4, 0x90
.LBB9_14:                               # %.preheader67
                                        #   in Loop: Header=BB9_13 Depth=2
	movl	16(%rdi), %esi
	testl	%esi, %esi
	jne	.LBB9_15
	jmp	.LBB9_19
	.p2align	4, 0x90
.LBB9_10:                               # %._crit_edge
                                        #   in Loop: Header=BB9_2 Depth=1
	addl	$-512, %ebp             # imm = 0xFE00
	jmp	.LBB9_19
	.p2align	4, 0x90
.LBB9_18:                               #   in Loop: Header=BB9_2 Depth=1
	incl	16(%rdi)
	addl	$-256, %ebp
.LBB9_19:                               # %select.unfold
                                        #   in Loop: Header=BB9_2 Depth=1
	addl	%ebp, %ebp
	addl	%r11d, %r11d
	cmpl	$256, %r11d             # imm = 0x100
	jb	.LBB9_2
	jmp	.LBB9_20
.LBB9_1:
	movl	%edx, %r11d
.LBB9_20:                               # %select.unfold._crit_edge
	movl	%r11d, 4(%rdi)
	movl	%ebp, (%rdi)
	incl	40(%rdi)
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end9:
	.size	biari_encode_symbol_final, .Lfunc_end9-biari_encode_symbol_final
	.cfi_endproc

	.globl	biari_init_context
	.p2align	4, 0x90
	.type	biari_init_context,@function
biari_init_context:                     # @biari_init_context
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	movq	14216(%rax), %rax
	movl	4(%rax), %eax
	xorl	%ecx, %ecx
	testl	%eax, %eax
	cmovnsl	%eax, %ecx
	imull	(%rsi), %ecx
	sarl	$4, %ecx
	addl	4(%rsi), %ecx
	testl	%ecx, %ecx
	movl	$1, %eax
	cmovgl	%ecx, %eax
	cmpl	$127, %eax
	movl	$126, %ecx
	cmovll	%eax, %ecx
	movl	$63, %eax
	subl	%ecx, %eax
	leal	65472(%rcx), %edx
	cmpl	$63, %ecx
	cmovgw	%dx, %ax
	movw	%ax, (%rdi)
	setg	2(%rdi)
	movq	$0, 8(%rdi)
	retq
.Lfunc_end10:
	.size	biari_init_context, .Lfunc_end10-biari_init_context
	.cfi_endproc

	.type	rLPS_table_64x4,@object # @rLPS_table_64x4
	.section	.rodata,"a",@progbits
	.globl	rLPS_table_64x4
	.p2align	4
rLPS_table_64x4:
	.ascii	"\200\260\320\360"
	.ascii	"\200\247\305\343"
	.ascii	"\200\236\273\330"
	.ascii	"{\226\262\315"
	.ascii	"t\216\251\303"
	.ascii	"o\207\240\271"
	.ascii	"i\200\230\257"
	.ascii	"dz\220\246"
	.ascii	"_t\211\236"
	.ascii	"Zn\202\226"
	.ascii	"Uh{\216"
	.ascii	"Qcu\207"
	.ascii	"M^o\200"
	.ascii	"IYiz"
	.ascii	"EUdt"
	.ascii	"BP_n"
	.ascii	">LZh"
	.ascii	";HVc"
	.ascii	"8EQ^"
	.ascii	"5AMY"
	.ascii	"3>IU"
	.ascii	"0;EP"
	.ascii	".8BL"
	.ascii	"+5?H"
	.ascii	")2;E"
	.ascii	"'08A"
	.ascii	"%-6>"
	.ascii	"#+3;"
	.ascii	"!)08"
	.ascii	" '.5"
	.ascii	"\036%+2"
	.ascii	"\035#)0"
	.ascii	"\033!'-"
	.ascii	"\032\037%+"
	.ascii	"\030\036#)"
	.ascii	"\027\034!'"
	.ascii	"\026\033 %"
	.ascii	"\025\032\036#"
	.ascii	"\024\030\035!"
	.ascii	"\023\027\033\037"
	.ascii	"\022\026\032\036"
	.ascii	"\021\025\031\034"
	.ascii	"\020\024\027\033"
	.ascii	"\017\023\026\031"
	.ascii	"\016\022\025\030"
	.ascii	"\016\021\024\027"
	.ascii	"\r\020\023\026"
	.ascii	"\f\017\022\025"
	.ascii	"\f\016\021\024"
	.ascii	"\013\016\020\023"
	.ascii	"\013\r\017\022"
	.ascii	"\n\f\017\021"
	.ascii	"\n\f\016\020"
	.ascii	"\t\013\r\017"
	.ascii	"\t\013\f\016"
	.ascii	"\b\n\f\016"
	.ascii	"\b\t\013\r"
	.ascii	"\007\t\013\f"
	.ascii	"\007\t\n\f"
	.ascii	"\007\b\n\013"
	.ascii	"\006\b\t\013"
	.ascii	"\006\007\t\n"
	.ascii	"\006\007\b\t"
	.zero	4,2
	.size	rLPS_table_64x4, 256

	.type	AC_next_state_MPS_64,@object # @AC_next_state_MPS_64
	.globl	AC_next_state_MPS_64
	.p2align	4
AC_next_state_MPS_64:
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	10                      # 0xa
	.short	11                      # 0xb
	.short	12                      # 0xc
	.short	13                      # 0xd
	.short	14                      # 0xe
	.short	15                      # 0xf
	.short	16                      # 0x10
	.short	17                      # 0x11
	.short	18                      # 0x12
	.short	19                      # 0x13
	.short	20                      # 0x14
	.short	21                      # 0x15
	.short	22                      # 0x16
	.short	23                      # 0x17
	.short	24                      # 0x18
	.short	25                      # 0x19
	.short	26                      # 0x1a
	.short	27                      # 0x1b
	.short	28                      # 0x1c
	.short	29                      # 0x1d
	.short	30                      # 0x1e
	.short	31                      # 0x1f
	.short	32                      # 0x20
	.short	33                      # 0x21
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	36                      # 0x24
	.short	37                      # 0x25
	.short	38                      # 0x26
	.short	39                      # 0x27
	.short	40                      # 0x28
	.short	41                      # 0x29
	.short	42                      # 0x2a
	.short	43                      # 0x2b
	.short	44                      # 0x2c
	.short	45                      # 0x2d
	.short	46                      # 0x2e
	.short	47                      # 0x2f
	.short	48                      # 0x30
	.short	49                      # 0x31
	.short	50                      # 0x32
	.short	51                      # 0x33
	.short	52                      # 0x34
	.short	53                      # 0x35
	.short	54                      # 0x36
	.short	55                      # 0x37
	.short	56                      # 0x38
	.short	57                      # 0x39
	.short	58                      # 0x3a
	.short	59                      # 0x3b
	.short	60                      # 0x3c
	.short	61                      # 0x3d
	.short	62                      # 0x3e
	.short	62                      # 0x3e
	.short	63                      # 0x3f
	.size	AC_next_state_MPS_64, 128

	.type	AC_next_state_LPS_64,@object # @AC_next_state_LPS_64
	.globl	AC_next_state_LPS_64
	.p2align	4
AC_next_state_LPS_64:
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	5                       # 0x5
	.short	6                       # 0x6
	.short	7                       # 0x7
	.short	8                       # 0x8
	.short	9                       # 0x9
	.short	9                       # 0x9
	.short	11                      # 0xb
	.short	11                      # 0xb
	.short	12                      # 0xc
	.short	13                      # 0xd
	.short	13                      # 0xd
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	16                      # 0x10
	.short	16                      # 0x10
	.short	18                      # 0x12
	.short	18                      # 0x12
	.short	19                      # 0x13
	.short	19                      # 0x13
	.short	21                      # 0x15
	.short	21                      # 0x15
	.short	22                      # 0x16
	.short	22                      # 0x16
	.short	23                      # 0x17
	.short	24                      # 0x18
	.short	24                      # 0x18
	.short	25                      # 0x19
	.short	26                      # 0x1a
	.short	26                      # 0x1a
	.short	27                      # 0x1b
	.short	27                      # 0x1b
	.short	28                      # 0x1c
	.short	29                      # 0x1d
	.short	29                      # 0x1d
	.short	30                      # 0x1e
	.short	30                      # 0x1e
	.short	30                      # 0x1e
	.short	31                      # 0x1f
	.short	32                      # 0x20
	.short	32                      # 0x20
	.short	33                      # 0x21
	.short	33                      # 0x21
	.short	33                      # 0x21
	.short	34                      # 0x22
	.short	34                      # 0x22
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	35                      # 0x23
	.short	36                      # 0x24
	.short	36                      # 0x24
	.short	36                      # 0x24
	.short	37                      # 0x25
	.short	37                      # 0x25
	.short	37                      # 0x25
	.short	38                      # 0x26
	.short	38                      # 0x26
	.short	63                      # 0x3f
	.size	AC_next_state_LPS_64, 128

	.type	binCount,@object        # @binCount
	.bss
	.globl	binCount
	.p2align	2
binCount:
	.long	0                       # 0x0
	.size	binCount, 4

	.type	pic_bin_count,@object   # @pic_bin_count
	.comm	pic_bin_count,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"arienco_create_encoding_environment: eep"
	.size	.L.str, 41

	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Error freeing eep (NULL pointer)"
	.size	.L.str.1, 33

	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
