	.text
	.file	"obsequi.bc"
	.globl	getline_llvm
	.p2align	4, 0x90
	.type	getline_llvm,@function
getline_llvm:                           # @getline_llvm
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$4096, %rsp             # imm = 0x1000
.Lcfi3:
	.cfi_def_cfa_offset 4128
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rdi, %r14
	movq	%rsp, %r15
	xorl	%esi, %esi
	movl	$4096, %edx             # imm = 0x1000
	movq	%r15, %rdi
	callq	memset
	movl	$4095, %esi             # imm = 0xFFF
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB0_1
# BB#2:
	movq	%rsp, %rdi
	callq	strlen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	js	.LBB0_7
# BB#3:
	movq	(%r14), %rdi
	movq	%rbx, %r15
	incq	%r15
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#5:
	movq	%r15, %rsi
	callq	realloc
	jmp	.LBB0_6
.LBB0_1:
	movq	$-1, %rbx
	jmp	.LBB0_7
.LBB0_4:
	movq	%r15, %rdi
	callq	malloc
.LBB0_6:
	movq	%rax, (%r14)
	movq	%rsp, %rsi
	movq	%rax, %rdi
	movq	%r15, %rdx
	callq	memcpy
.LBB0_7:                                # %.thread
	movq	%rbx, %rax
	addq	$4096, %rsp             # imm = 0x1000
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	getline_llvm, .Lfunc_end0-getline_llvm
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	subq	$7736, %rsp             # imm = 0x1E38
.Lcfi13:
	.cfi_def_cfa_offset 7792
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %ebx
	jmp	.LBB1_2
	.p2align	4, 0x90
.LBB1_1:                                #   in Loop: Header=BB1_2 Depth=1
	movl	$.L.str.2, %edi
	movl	$378, %esi              # imm = 0x17A
	movl	$1, %edx
	movl	$.L.str.18, %ecx
	xorl	%eax, %eax
	movl	%ebp, %r8d
	callq	_fatal_error_aux
.LBB1_2:                                # %.backedge.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.15, %edx
	movl	%ebx, %edi
	movq	%r14, %rsi
	callq	getopt
	movl	%eax, %ebp
	leal	-101(%rbp), %eax
	cmpl	$18, %eax
	ja	.LBB1_5
# BB#3:                                 # %.backedge.i
                                        #   in Loop: Header=BB1_2 Depth=1
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_4:                                #   in Loop: Header=BB1_2 Depth=1
	movq	optarg(%rip), %rdi
	callq	__strdup
	movq	%rax, lock_file(%rip)
	jmp	.LBB1_2
	.p2align	4, 0x90
.LBB1_5:                                # %.backedge.i
                                        #   in Loop: Header=BB1_2 Depth=1
	cmpl	$-1, %ebp
	je	.LBB1_8
	jmp	.LBB1_1
	.p2align	4, 0x90
.LBB1_6:                                #   in Loop: Header=BB1_2 Depth=1
	movq	optarg(%rip), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	cltq
	movq	%rax, stop_minutes(%rip)
	jmp	.LBB1_2
	.p2align	4, 0x90
.LBB1_7:                                #   in Loop: Header=BB1_2 Depth=1
	movb	$1, main_batch(%rip)
	jmp	.LBB1_2
.LBB1_8:
	cmpl	%ebx, optind(%rip)
	jge	.LBB1_10
# BB#9:
	movl	$.L.str.2, %edi
	movl	$383, %esi              # imm = 0x17F
	movl	$1, %edx
	movl	$.L.str.19, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB1_10:                               # %decode_switches.exit
	movq	lock_file(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_52
# BB#11:
	movb	$0, 14(%rsp)
	movl	$.L.str.23, %esi
	callq	fopen
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB1_13
# BB#12:
	movl	$.L.str.2, %edi
	movl	$563, %esi              # imm = 0x233
	movl	$1, %edx
	movl	$.L.str.24, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB1_13:
	movq	lock_file(%rip), %rdi
	callq	puts
	xorl	%ebx, %ebx
	leaq	3632(%rsp), %r12
	.p2align	4, 0x90
.LBB1_14:                               # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	ftell
	movl	%eax, lock_file_offset(%rip)
	xorl	%esi, %esi
	movl	$4096, %edx             # imm = 0x1000
	movq	%r12, %rdi
	callq	memset
	movl	$4095, %esi             # imm = 0xFFF
	movq	%r12, %rdi
	movq	%r14, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB1_54
# BB#15:                                #   in Loop: Header=BB1_14 Depth=1
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %r15
	testq	%r15, %r15
	js	.LBB1_18
# BB#16:                                #   in Loop: Header=BB1_14 Depth=1
	leaq	1(%r15), %rbp
	testq	%rbx, %rbx
	je	.LBB1_19
# BB#17:                                #   in Loop: Header=BB1_14 Depth=1
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	realloc
	jmp	.LBB1_20
	.p2align	4, 0x90
.LBB1_18:                               # %getline_llvm.exit.i
                                        #   in Loop: Header=BB1_14 Depth=1
	cmpq	$-1, %r15
	jne	.LBB1_21
	jmp	.LBB1_54
	.p2align	4, 0x90
.LBB1_19:                               #   in Loop: Header=BB1_14 Depth=1
	movq	%rbp, %rdi
	callq	malloc
.LBB1_20:                               # %getline_llvm.exit.thread34.i
                                        #   in Loop: Header=BB1_14 Depth=1
	movq	%rax, %rbx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	memcpy
.LBB1_21:                               #   in Loop: Header=BB1_14 Depth=1
	cmpb	$65, (%rbx)
	jne	.LBB1_14
# BB#22:
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	cmpq	$18, %r15
	ja	.LBB1_24
# BB#23:
	movl	$.L.str.2, %edi
	movl	$577, %esi              # imm = 0x241
	movl	$1, %edx
	xorl	%eax, %eax
	movq	%rbx, %rcx
	callq	_fatal_error_aux
.LBB1_24:
	leaq	18(%rbx), %rdi
	leaq	3632(%rsp), %rdx
	leaq	20(%rsp), %rcx
	movl	$.L.str.27, %esi
	xorl	%eax, %eax
	callq	sscanf
	cmpl	$2, %eax
	je	.LBB1_26
# BB#25:
	movl	$.L.str.2, %edi
	movl	$580, %esi              # imm = 0x244
	movl	$1, %edx
	movl	$.L.str.28, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	_fatal_error_aux
.LBB1_26:
	movl	3632(%rsp), %r8d
	cmpl	$31, %r8d
	jb	.LBB1_28
# BB#27:
	movl	$.L.str.2, %edi
	movl	$583, %esi              # imm = 0x247
	movl	$1, %edx
	movl	$.L.str.6, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB1_28:
	movl	20(%rsp), %r8d
	cmpl	$31, %r8d
	jb	.LBB1_30
# BB#29:
	movl	$.L.str.2, %edi
	movl	$585, %esi              # imm = 0x249
	movl	$1, %edx
	movl	$.L.str.7, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
	movl	20(%rsp), %r8d
.LBB1_30:
	imull	3632(%rsp), %r8d
	cmpl	$129, %r8d
	jb	.LBB1_32
# BB#31:
	movl	$.L.str.2, %edi
	movl	$587, %esi              # imm = 0x24B
	movl	$1, %edx
	movl	$.L.str.8, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB1_32:                               # %.preheader40.i
	leaq	32(%rsp), %rdi
	xorl	%esi, %esi
	movl	$3600, %edx             # imm = 0xE10
	callq	memset
	movl	$30, 24(%rsp)
	movl	$30, 16(%rsp)
	movl	$18, %r13d
	movabsq	$4294967296, %rbp       # imm = 0x100000000
	leaq	24(%rsp), %r12
	jmp	.LBB1_34
	.p2align	4, 0x90
.LBB1_33:                               #   in Loop: Header=BB1_34 Depth=1
	movslq	%ecx, %rcx
	imulq	$120, %rcx, %rcx
	leaq	32(%rsp,%rcx), %rcx
	cltq
	movl	$1, 4(%rcx,%rax,4)
	movl	$1, (%rcx,%rax,4)
.LBB1_34:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_36 Depth 2
	movq	%r13, %rdi
	shlq	$32, %rdi
	movslq	%r13d, %r13
	xorl	%ecx, %ecx
	jmp	.LBB1_36
	.p2align	4, 0x90
.LBB1_35:                               #   in Loop: Header=BB1_36 Depth=2
	incq	%r13
	addq	%rbp, %rdi
	movl	%eax, %ecx
.LBB1_36:                               #   Parent Loop BB1_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbx,%r13), %eax
	cmpb	$32, %al
	je	.LBB1_39
# BB#37:                                #   in Loop: Header=BB1_36 Depth=2
	testb	%al, %al
	je	.LBB1_40
# BB#38:                                #   in Loop: Header=BB1_36 Depth=2
	movl	$1, %eax
	cmpl	$2, %ecx
	jne	.LBB1_35
	jmp	.LBB1_40
	.p2align	4, 0x90
.LBB1_39:                               #   in Loop: Header=BB1_36 Depth=2
	xorl	%eax, %eax
	testl	%ecx, %ecx
	setne	%al
	addl	%eax, %eax
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_40:                               # %next_valid_pos.exit.i
                                        #   in Loop: Header=BB1_34 Depth=1
	sarq	$32, %rdi
	cmpq	%r15, %rdi
	jae	.LBB1_55
# BB#41:                                #   in Loop: Header=BB1_34 Depth=1
	addq	%rbx, %rdi
	movl	$.L.str.29, %esi
	xorl	%eax, %eax
	leaq	14(%rsp), %rdx
	leaq	28(%rsp), %rcx
	leaq	16(%rsp), %r8
	movq	%r12, %r9
	callq	sscanf
	cmpl	$4, %eax
	jne	.LBB1_55
# BB#42:                                #   in Loop: Header=BB1_34 Depth=1
	movsbl	14(%rsp), %esi
	movl	16(%rsp), %edx
	movl	24(%rsp), %ecx
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	printf
	movb	14(%rsp), %al
	cmpb	$72, %al
	je	.LBB1_48
# BB#43:                                #   in Loop: Header=BB1_34 Depth=1
	cmpb	$86, %al
	jne	.LBB1_51
# BB#44:                                #   in Loop: Header=BB1_34 Depth=1
	movl	24(%rsp), %eax
	movslq	%eax, %rsi
	movl	16(%rsp), %ecx
	movslq	%ecx, %rdx
	imulq	$120, %rsi, %rsi
	leaq	32(%rsp,%rsi), %rsi
	cmpl	$1, (%rsi,%rdx,4)
	je	.LBB1_46
# BB#45:                                #   in Loop: Header=BB1_34 Depth=1
	cmpl	$1, 120(%rsi,%rdx,4)
	jne	.LBB1_47
.LBB1_46:                               #   in Loop: Header=BB1_34 Depth=1
	movl	$.L.str.2, %edi
	movl	$609, %esi              # imm = 0x261
	movl	$1, %edx
	xorl	%eax, %eax
	movq	%rbx, %rcx
	callq	_fatal_error_aux
	movl	24(%rsp), %eax
	movl	16(%rsp), %ecx
.LBB1_47:                               #   in Loop: Header=BB1_34 Depth=1
	leal	1(%rax), %edx
	movslq	%edx, %rdx
	movslq	%ecx, %rcx
	imulq	$120, %rdx, %rdx
	leaq	32(%rsp,%rdx), %rdx
	movl	$1, (%rdx,%rcx,4)
	cltq
	imulq	$120, %rax, %rax
	leaq	32(%rsp,%rax), %rax
	movl	$1, (%rax,%rcx,4)
	jmp	.LBB1_34
	.p2align	4, 0x90
.LBB1_48:                               #   in Loop: Header=BB1_34 Depth=1
	movl	16(%rsp), %ecx
	movslq	%ecx, %rsi
	movl	24(%rsp), %eax
	movslq	%eax, %rdx
	imulq	$120, %rsi, %rsi
	leaq	32(%rsp,%rsi), %rsi
	cmpl	$1, (%rsi,%rdx,4)
	je	.LBB1_50
# BB#49:                                #   in Loop: Header=BB1_34 Depth=1
	cmpl	$1, 4(%rsi,%rdx,4)
	jne	.LBB1_33
.LBB1_50:                               #   in Loop: Header=BB1_34 Depth=1
	movl	$.L.str.2, %edi
	movl	$613, %esi              # imm = 0x265
	movl	$1, %edx
	xorl	%eax, %eax
	movq	%rbx, %rcx
	callq	_fatal_error_aux
	movl	16(%rsp), %ecx
	movl	24(%rsp), %eax
	jmp	.LBB1_33
	.p2align	4, 0x90
.LBB1_51:                               #   in Loop: Header=BB1_34 Depth=1
	movl	$.L.str.2, %edi
	movl	$615, %esi              # imm = 0x267
	movl	$1, %edx
	movl	$.L.str.31, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
	jmp	.LBB1_34
.LBB1_52:
	movq	stdin(%rip), %rbx
	leaq	3632(%rsp), %rbp
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	movl	$4096, %edx             # imm = 0x1000
	movq	%rbp, %rdi
	callq	memset
	movl	$4095, %esi             # imm = 0xFFF
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB1_66
# BB#53:                                # %.lr.ph85.i.preheader
	leaq	3632(%rsp), %r13
	leaq	15(%rsp), %r12
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	jmp	.LBB1_78
.LBB1_54:                               # %.loopexit.i
	movl	$.L.str.2, %edi
	movl	$624, %esi              # imm = 0x270
	movl	$1, %edx
	movl	$.L.str.14, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
	jmp	.LBB1_56
.LBB1_55:
	movl	$.Lstr.3, %edi
	callq	puts
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB1_56:
	movslq	lock_file_offset(%rip), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	fseek
	movl	$87, %edi
	movq	%r14, %rsi
	callq	fputc
	movq	%r14, %rdi
	callq	fflush
	movb	14(%rsp), %al
	cmpb	$72, %al
	je	.LBB1_59
# BB#57:
	cmpb	$86, %al
	jne	.LBB1_60
# BB#58:
	movb	$72, main_whos_turn(%rip)
	jmp	.LBB1_61
.LBB1_59:
	movb	$86, main_whos_turn(%rip)
	jmp	.LBB1_61
.LBB1_60:
	movl	$.L.str.2, %edi
	movl	$632, %esi              # imm = 0x278
	movl	$1, %edx
	movl	$.L.str.31, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB1_61:                               # %get_solve_command_from_lock_file.exit
	movl	3632(%rsp), %edi
	movl	20(%rsp), %esi
	leaq	32(%rsp), %rdx
	callq	initialize_board
	movq	%rbx, %rdi
	callq	free
	movq	%r14, %rdi
	callq	fclose
	movq	stop_minutes(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB1_109
# BB#62:
	leaq	3640(%rsp), %rdi
	callq	sigfillset
	movl	$0, 3768(%rsp)
	movq	$stop_alrm_handler, 3632(%rsp)
	leaq	3632(%rsp), %rsi
	movl	$26, %edi
	xorl	%edx, %edx
	callq	sigaction
	testl	%eax, %eax
	je	.LBB1_64
# BB#63:
	movl	$.L.str.2, %edi
	movl	$449, %esi              # imm = 0x1C1
	movl	$1, %edx
	movl	$.L.str.20, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB1_64:
	imulq	$60, %rbx, %rax
	movq	%rax, 32(%rsp)
	movq	$0, 40(%rsp)
	movq	%rax, 48(%rsp)
	movq	$0, 56(%rsp)
	leaq	32(%rsp), %rsi
	movl	$1, %edi
	xorl	%edx, %edx
	callq	setitimer
	testl	%eax, %eax
	je	.LBB1_109
# BB#65:
	movl	$.L.str.2, %edi
	movl	$457, %esi              # imm = 0x1C9
	movl	$1, %edx
	movl	$.L.str.22, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
	jmp	.LBB1_109
.LBB1_66:
	xorl	%r14d, %r14d
	jmp	.LBB1_107
.LBB1_67:                               #   in Loop: Header=BB1_78 Depth=1
	movsbl	%al, %edx
	cmpl	$86, %edx
	je	.LBB1_106
# BB#68:                                #   in Loop: Header=BB1_78 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB1_69:                               #   in Loop: Header=BB1_78 Depth=1
	leaq	15(%rsp), %r12
	jmp	.LBB1_76
.LBB1_70:                               #   in Loop: Header=BB1_78 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	callq	fprintf
	jmp	.LBB1_76
.LBB1_71:                               #   in Loop: Header=BB1_78 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movl	%ecx, %edx
	jmp	.LBB1_75
.LBB1_72:                               #   in Loop: Header=BB1_78 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.7, %esi
	jmp	.LBB1_74
.LBB1_73:                               #   in Loop: Header=BB1_78 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
.LBB1_74:                               # %.thread53.backedge.i
                                        #   in Loop: Header=BB1_78 Depth=1
	xorl	%eax, %eax
.LBB1_75:                               # %.thread53.backedge.i
                                        #   in Loop: Header=BB1_78 Depth=1
	callq	fprintf
	.p2align	4, 0x90
.LBB1_76:                               # %.thread53.backedge.i
                                        #   in Loop: Header=BB1_78 Depth=1
	movq	stdin(%rip), %rbx
	xorl	%esi, %esi
	movl	$4096, %edx             # imm = 0x1000
	movq	%r13, %rdi
	callq	memset
	movl	$4095, %esi             # imm = 0xFFF
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	fgets
	testq	%rax, %rax
	jne	.LBB1_78
	jmp	.LBB1_107
.LBB1_77:                               #   in Loop: Header=BB1_78 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	callq	fprintf
	leaq	15(%rsp), %r12
	jmp	.LBB1_76
	.p2align	4, 0x90
.LBB1_78:                               # %.lr.ph85.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_97 Depth 2
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	js	.LBB1_83
# BB#79:                                #   in Loop: Header=BB1_78 Depth=1
	leaq	1(%rbp), %rbx
	testq	%r15, %r15
	je	.LBB1_81
# BB#80:                                #   in Loop: Header=BB1_78 Depth=1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	realloc
	jmp	.LBB1_82
.LBB1_81:                               #   in Loop: Header=BB1_78 Depth=1
	movq	%rbx, %rdi
	callq	malloc
.LBB1_82:                               #   in Loop: Header=BB1_78 Depth=1
	movq	%rax, %r15
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	callq	memcpy
.LBB1_83:                               # %getline_llvm.exit.i18
                                        #   in Loop: Header=BB1_78 Depth=1
	testq	%rbp, %rbp
	je	.LBB1_76
# BB#84:                                # %getline_llvm.exit.i18
                                        #   in Loop: Header=BB1_78 Depth=1
	cmpq	$-1, %rbp
	je	.LBB1_107
# BB#85:                                #   in Loop: Header=BB1_78 Depth=1
	testq	%r14, %r14
	je	.LBB1_87
# BB#86:                                #   in Loop: Header=BB1_78 Depth=1
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	realloc
	jmp	.LBB1_88
.LBB1_87:                               #   in Loop: Header=BB1_78 Depth=1
	xorl	%edi, %edi
	callq	malloc
.LBB1_88:                               #   in Loop: Header=BB1_78 Depth=1
	movq	%rax, %r14
	movb	$0, -1(%r15,%rbp)
	movq	%r12, (%rsp)
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	leaq	20(%rsp), %rdx
	leaq	16(%rsp), %rcx
	leaq	14(%rsp), %r8
	movq	%r14, %r9
	callq	sscanf
	movl	%eax, %ebx
	cmpl	$3, %ebx
	je	.LBB1_90
# BB#89:                                #   in Loop: Header=BB1_78 Depth=1
	cmpl	$5, %ebx
	jne	.LBB1_70
.LBB1_90:                               #   in Loop: Header=BB1_78 Depth=1
	movl	20(%rsp), %ecx
	cmpl	$31, %ecx
	jae	.LBB1_71
# BB#91:                                #   in Loop: Header=BB1_78 Depth=1
	movl	16(%rsp), %edx
	cmpl	$31, %edx
	jae	.LBB1_72
# BB#92:                                #   in Loop: Header=BB1_78 Depth=1
	imull	%ecx, %edx
	cmpl	$256, %edx              # imm = 0x100
	ja	.LBB1_73
# BB#93:                                # %.preheader64.i
                                        #   in Loop: Header=BB1_78 Depth=1
	xorl	%esi, %esi
	movl	$3600, %edx             # imm = 0xE10
	leaq	32(%rsp), %rdi
	callq	memset
	movl	$30, 28(%rsp)
	movl	$30, 24(%rsp)
	cmpl	$5, %ebx
	jne	.LBB1_102
# BB#94:                                #   in Loop: Header=BB1_78 Depth=1
	callq	__ctype_toupper_loc
	movq	%rax, %r12
	movq	(%r12), %rax
	movsbq	14(%rsp), %rcx
	movzbl	(%rax,%rcx,4), %eax
	movb	%al, 14(%rsp)
	cmpl	$66, %eax
	jne	.LBB1_77
# BB#95:                                # %.preheader63.i
                                        #   in Loop: Header=BB1_78 Depth=1
	movq	%r13, %rbx
	movl	$.L.str.9, %esi
	movq	%r14, %rdi
	jmp	.LBB1_97
	.p2align	4, 0x90
.LBB1_96:                               #   in Loop: Header=BB1_97 Depth=2
	imulq	$120, %rax, %rax
	leaq	32(%rsp,%rax), %rax
	movl	$1, (%rax,%rcx,4)
	xorl	%edi, %edi
	movl	$.L.str.9, %esi
.LBB1_97:                               # %.preheader63.i
                                        #   Parent Loop BB1_78 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	callq	strtok
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB1_103
# BB#98:                                # %.lr.ph.i
                                        #   in Loop: Header=BB1_97 Depth=2
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	leaq	24(%rsp), %rdx
	leaq	28(%rsp), %rcx
	callq	sscanf
	cmpl	$2, %eax
	jne	.LBB1_101
# BB#99:                                #   in Loop: Header=BB1_97 Depth=2
	movslq	24(%rsp), %rax
	cmpl	20(%rsp), %eax
	jae	.LBB1_101
# BB#100:                               #   in Loop: Header=BB1_97 Depth=2
	movslq	28(%rsp), %rcx
	cmpl	16(%rsp), %ecx
	jb	.LBB1_96
.LBB1_101:                              #   in Loop: Header=BB1_78 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%r13, %rdx
	callq	fprintf
	movb	$0, 14(%rsp)
	movq	%rbx, %r13
	leaq	15(%rsp), %r12
	jmp	.LBB1_76
.LBB1_102:                              #   in Loop: Header=BB1_78 Depth=1
	movb	14(%rsp), %bl
	movb	%bl, 15(%rsp)
	callq	__ctype_toupper_loc
	movq	%rax, %r12
	jmp	.LBB1_105
.LBB1_103:                              # %._crit_edge.i
                                        #   in Loop: Header=BB1_78 Depth=1
	cmpb	$0, 14(%rsp)
	movq	%rbx, %r13
	je	.LBB1_69
# BB#104:                               # %._crit_edge._crit_edge.i
                                        #   in Loop: Header=BB1_78 Depth=1
	movb	15(%rsp), %bl
.LBB1_105:                              #   in Loop: Header=BB1_78 Depth=1
	movq	(%r12), %rax
	movsbq	%bl, %rcx
	movl	(%rax,%rcx,4), %eax
	movb	%al, 15(%rsp)
	cmpb	$72, %al
	jne	.LBB1_67
.LBB1_106:
	movl	$.Lstr.3, %edi
	callq	puts
	movq	stdout(%rip), %rdi
	callq	fflush
	cmpq	$-1, %rbp
	jne	.LBB1_108
.LBB1_107:                              # %.thread56.i
	movl	$.L.str.2, %edi
	movl	$275, %esi              # imm = 0x113
	movl	$1, %edx
	movl	$.L.str.14, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB1_108:                              # %get_solve_command.exit
	movb	15(%rsp), %al
	movb	%al, main_whos_turn(%rip)
	movl	20(%rsp), %edi
	movl	16(%rsp), %esi
	leaq	32(%rsp), %rdx
	callq	initialize_board
	movq	%r15, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
.LBB1_109:
	leaq	3640(%rsp), %rdi
	callq	sigfillset
	movl	$0, 3768(%rsp)
	movq	$sig_int_handler, 3632(%rsp)
	leaq	3632(%rsp), %rsi
	movl	$2, %edi
	xorl	%edx, %edx
	callq	sigaction
	testl	%eax, %eax
	je	.LBB1_111
# BB#110:
	movl	$.L.str.2, %edi
	movl	$418, %esi              # imm = 0x1A2
	movl	$1, %edx
	movl	$.L.str.20, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB1_111:                              # %sig_int_setup.exit
	movsbl	main_whos_turn(%rip), %edi
	leaq	20(%rsp), %rsi
	leaq	16(%rsp), %rdx
	leaq	32(%rsp), %rcx
	callq	search_for_move
	movl	%eax, %ebp
	leaq	3632(%rsp), %rbx
	movq	%rbx, %rdi
	callq	sigemptyset
	movl	$14, %esi
	movq	%rbx, %rdi
	callq	sigaddset
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	sigaddset
	xorl	%edi, %edi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	callq	sigprocmask
	movq	32(%rsp), %rdi
	callq	u64bit_to_string
	movq	%rax, %rbx
	movsbl	main_whos_turn(%rip), %esi
	cmpl	$5000, %ebp             # imm = 0x1388
	jge	.LBB1_113
# BB#112:
	cmpb	$86, %sil
	movl	$72, %eax
	movl	$86, %esi
	cmovel	%eax, %esi
.LBB1_113:
	movl	20(%rsp), %edx
	movl	16(%rsp), %ecx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	lock_file(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_123
# BB#114:
	cmpl	$5000, %ebp             # imm = 0x1388
	jl	.LBB1_116
# BB#115:
	movb	main_whos_turn(%rip), %r14b
	jmp	.LBB1_120
.LBB1_116:
	cmpl	$-5000, %ebp            # imm = 0xEC78
	jg	.LBB1_119
# BB#117:
	cmpb	$86, main_whos_turn(%rip)
	movb	$72, %r14b
	je	.LBB1_120
# BB#118:
	movb	$86, %r14b
	jmp	.LBB1_120
.LBB1_119:
	xorl	%r14d, %r14d
	movl	$.L.str.2, %edi
	movl	$149, %esi
	movl	$1, %edx
	movl	$.L.str.3, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
	movq	lock_file(%rip), %rdi
.LBB1_120:
	movl	$.L.str.23, %esi
	callq	fopen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB1_122
# BB#121:
	movl	$.L.str.2, %edi
	movl	$530, %esi              # imm = 0x212
	movl	$1, %edx
	movl	$.L.str.24, %ecx
	xorl	%eax, %eax
	callq	_fatal_error_aux
.LBB1_122:                              # %write_to_lock_file.exit
	movq	lock_file(%rip), %rdi
	callq	puts
	movslq	lock_file_offset(%rip), %rsi
	xorl	%edx, %edx
	movq	%rbp, %rdi
	callq	fseek
	movsbl	%r14b, %edx
	movl	$.L.str.25, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rcx
	callq	fprintf
	movq	%rbp, %rdi
	callq	fflush
	movq	%rbp, %rdi
	callq	fclose
.LBB1_123:
	xorl	%edi, %edi
	movl	$4, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	fcntl
	cmpb	$1, main_batch(%rip)
	jne	.LBB1_125
	.p2align	4, 0x90
.LBB1_124:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	stdin(%rip), %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB1_124
.LBB1_125:                              # %.loopexit
	xorl	%eax, %eax
	addq	$7736, %rsp             # imm = 0x1E38
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_126:
	movl	$option_string, %edi
	xorl	%eax, %eax
	callq	printf
	xorl	%edi, %edi
	callq	exit
.LBB1_127:
	movl	$.Lstr.1, %edi
	jmp	.LBB1_129
.LBB1_128:
	movl	$.Lstr, %edi
.LBB1_129:
	callq	puts
	xorl	%edi, %edi
	callq	exit
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_126
	.quad	.LBB1_1
	.quad	.LBB1_1
	.quad	.LBB1_127
	.quad	.LBB1_1
	.quad	.LBB1_1
	.quad	.LBB1_1
	.quad	.LBB1_4
	.quad	.LBB1_1
	.quad	.LBB1_1
	.quad	.LBB1_1
	.quad	.LBB1_1
	.quad	.LBB1_1
	.quad	.LBB1_1
	.quad	.LBB1_1
	.quad	.LBB1_6
	.quad	.LBB1_1
	.quad	.LBB1_128
	.quad	.LBB1_7

	.text
	.p2align	4, 0x90
	.type	sig_int_handler,@function
sig_int_handler:                        # @sig_int_handler
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 16
	xorl	%eax, %eax
	callq	current_search_state
	movq	%rax, %rcx
	movq	stderr(%rip), %rdi
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	fprintf
	movq	stderr(%rip), %rdi
	popq	%rax
	jmp	fflush                  # TAILCALL
.Lfunc_end2:
	.size	sig_int_handler, .Lfunc_end2-sig_int_handler
	.cfi_endproc

	.p2align	4, 0x90
	.type	stop_alrm_handler,@function
stop_alrm_handler:                      # @stop_alrm_handler
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 16
	xorl	%edi, %edi
	callq	exit
.Lfunc_end3:
	.size	stop_alrm_handler, .Lfunc_end3-stop_alrm_handler
	.cfi_endproc

	.type	option_string,@object   # @option_string
	.data
	.globl	option_string
	.p2align	4
option_string:
	.asciz	"-Wall -O2\n-DCOUNTBITS16\n-DLASTBIT16\n-DCOUNTMOVES_TABLE\n-DHASHCODEBITS=23\n-DTWO_STAGE_GENERATION\n"
	.size	option_string, 97

	.type	lock_file,@object       # @lock_file
	.local	lock_file
	.comm	lock_file,8,8
	.type	stop_minutes,@object    # @stop_minutes
	.local	stop_minutes
	.comm	stop_minutes,8,8
	.type	main_whos_turn,@object  # @main_whos_turn
	.local	main_whos_turn
	.comm	main_whos_turn,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"winner %c, move (%d,%d), nodes %s.\n"
	.size	.L.str, 36

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/obsequi/obsequi.c"
	.size	.L.str.2, 81

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Undecided.\n"
	.size	.L.str.3, 12

	.type	main_batch,@object      # @main_batch
	.local	main_batch
	.comm	main_batch,1,4
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"solve rows %u cols %u %c%s %c"
	.size	.L.str.4, 30

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Invalid command: '%s'.\n"
	.size	.L.str.5, 24

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Too many rows: %u > 30.\n"
	.size	.L.str.6, 25

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Too many cols: %u > 30.\n"
	.size	.L.str.7, 25

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Search space too large: %u > 256.\n"
	.size	.L.str.8, 35

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	";"
	.size	.L.str.9, 2

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%u,%u"
	.size	.L.str.10, 6

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Invalid block string: '%s'.\n"
	.size	.L.str.11, 29

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Invalid players turn: %c.\n"
	.size	.L.str.12, 27

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"No valid command given.\n"
	.size	.L.str.14, 25

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"wehl:t:v"
	.size	.L.str.15, 9

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Invalid option: '-%c'.\n"
	.size	.L.str.18, 24

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Extra unknown options on command line.\n"
	.size	.L.str.19, 40

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"`sigaction' failed."
	.size	.L.str.20, 20

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"%s\n"
	.size	.L.str.21, 4

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"`setitimer' failed.\n"
	.size	.L.str.22, 21

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"r+"
	.size	.L.str.23, 3

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"Can't open file.\n"
	.size	.L.str.24, 18

	.type	lock_file_offset,@object # @lock_file_offset
	.local	lock_file_offset
	.comm	lock_file_offset,4,4
	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"%c %15s"
	.size	.L.str.25, 8

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"%s"
	.size	.L.str.26, 3

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"(%d,%d)"
	.size	.L.str.27, 8

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Invalid row and columns.\n%s\n"
	.size	.L.str.28, 29

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	":%c:%d(%d,%d)"
	.size	.L.str.29, 14

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"%c %d %d\n"
	.size	.L.str.30, 10

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Invalid player.\n"
	.size	.L.str.31, 17

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"No version info available."
	.size	.Lstr, 27

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"No help available in this version."
	.size	.Lstr.1, 35

	.type	.Lstr.3,@object         # @str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.3:
	.asciz	"Starting"
	.size	.Lstr.3, 9


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
