	.text
	.file	"ldo.bc"
	.hidden	luaD_seterrorobj
	.globl	luaD_seterrorobj
	.p2align	4, 0x90
	.type	luaD_seterrorobj,@function
luaD_seterrorobj:                       # @luaD_seterrorobj
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdx, %rbx
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r14
	leal	-2(%rsi), %eax
	cmpl	$2, %eax
	jb	.LBB0_6
# BB#1:
	cmpl	$5, %esi
	je	.LBB0_5
# BB#2:
	cmpl	$4, %esi
	jne	.LBB0_8
# BB#3:
	movl	$.L.str, %esi
	movl	$17, %edx
	jmp	.LBB0_4
.LBB0_6:
	movq	16(%r14), %rax
	movq	-16(%rax), %rcx
	movq	%rcx, (%rbx)
	movl	-8(%rax), %eax
	jmp	.LBB0_7
.LBB0_5:
	movl	$.L.str.1, %esi
	movl	$23, %edx
.LBB0_4:                                # %.sink.split
	movq	%r14, %rdi
	callq	luaS_newlstr
	movq	%rax, (%rbx)
	movl	$4, %eax
.LBB0_7:                                # %.sink.split
	movl	%eax, 8(%rbx)
.LBB0_8:
	addq	$16, %rbx
	movq	%rbx, 16(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	luaD_seterrorobj, .Lfunc_end0-luaD_seterrorobj
	.cfi_endproc

	.hidden	luaD_throw
	.globl	luaD_throw
	.p2align	4, 0x90
	.type	luaD_throw,@function
luaD_throw:                             # @luaD_throw
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -40
.Lcfi11:
	.cfi_offset %r12, -32
.Lcfi12:
	.cfi_offset %r14, -24
.Lcfi13:
	.cfi_offset %r15, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	168(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_15
# BB#1:
	movb	%r14b, 10(%rbx)
	movq	32(%rbx), %rax
	cmpq	$0, 152(%rax)
	je	.LBB1_14
# BB#2:
	movq	80(%rbx), %rax
	movq	%rax, 40(%rbx)
	movq	(%rax), %rsi
	movq	%rsi, 24(%rbx)
	movq	%rbx, %rdi
	callq	luaF_close
	movq	24(%rbx), %r15
	leal	-2(%r14), %eax
	cmpl	$2, %eax
	jb	.LBB1_8
# BB#3:
	cmpl	$5, %r14d
	je	.LBB1_7
# BB#4:
	cmpl	$4, %r14d
	jne	.LBB1_10
# BB#5:
	movl	$.L.str, %esi
	movl	$17, %edx
	jmp	.LBB1_6
.LBB1_15:
	movl	%r14d, 208(%rdi)
	addq	$8, %rdi
	movl	$1, %esi
	callq	_longjmp
.LBB1_8:
	movq	16(%rbx), %rax
	movq	-16(%rax), %rcx
	movq	%rcx, (%r15)
	movl	-8(%rax), %eax
	jmp	.LBB1_9
.LBB1_7:
	movl	$.L.str.1, %esi
	movl	$23, %edx
.LBB1_6:                                # %.sink.split.i.i
	movq	%rbx, %rdi
	callq	luaS_newlstr
	movq	%rax, (%r15)
	movl	$4, %eax
.LBB1_9:                                # %.sink.split.i.i
	movl	%eax, 8(%r15)
.LBB1_10:                               # %luaD_seterrorobj.exit.i
	leaq	168(%rbx), %r12
	addq	$16, %r15
	movq	%r15, 16(%rbx)
	movzwl	98(%rbx), %eax
	movw	%ax, 96(%rbx)
	movb	$1, 101(%rbx)
	movslq	92(%rbx), %rax
	cmpq	$20001, %rax            # imm = 0x4E21
	jl	.LBB1_13
# BB#11:
	movq	40(%rbx), %rcx
	movq	80(%rbx), %r14
	subq	%r14, %rcx
	shrq	$3, %rcx
	imull	$-858993459, %ecx, %ecx # imm = 0xCCCCCCCD
	incl	%ecx
	cmpl	$19999, %ecx            # imm = 0x4E1F
	jg	.LBB1_13
# BB#12:
	shlq	$3, %rax
	leaq	(%rax,%rax,4), %rdx
	movl	$800000, %ecx           # imm = 0xC3500
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	luaM_realloc_
	movq	%rax, 80(%rbx)
	movl	$20000, 92(%rbx)        # imm = 0x4E20
	movq	40(%rbx), %rcx
	subq	%r14, %rcx
	addq	%rax, %rcx
	movq	%rcx, 40(%rbx)
	addq	$799960, %rax           # imm = 0xC34D8
	movq	%rax, 72(%rbx)
.LBB1_13:                               # %resetstack.exit
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	movq	32(%rbx), %rax
	movq	%rbx, %rdi
	callq	*152(%rax)
.LBB1_14:
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	luaD_throw, .Lfunc_end1-luaD_throw
	.cfi_endproc

	.hidden	luaD_rawrunprotected
	.globl	luaD_rawrunprotected
	.p2align	4, 0x90
	.type	luaD_rawrunprotected,@function
luaD_rawrunprotected:                   # @luaD_rawrunprotected
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 32
	subq	$224, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 256
.Lcfi18:
	.cfi_offset %rbx, -32
.Lcfi19:
	.cfi_offset %r14, -24
.Lcfi20:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$0, 216(%rsp)
	movq	168(%rbx), %rax
	movq	%rax, 8(%rsp)
	leaq	8(%rsp), %rax
	movq	%rax, 168(%rbx)
	leaq	16(%rsp), %rdi
	callq	_setjmp
	testl	%eax, %eax
	jne	.LBB2_2
# BB#1:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	*%r14
.LBB2_2:
	movq	8(%rsp), %rax
	movq	%rax, 168(%rbx)
	movl	216(%rsp), %eax
	addq	$224, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	luaD_rawrunprotected, .Lfunc_end2-luaD_rawrunprotected
	.cfi_endproc

	.hidden	luaD_reallocstack
	.globl	luaD_reallocstack
	.p2align	4, 0x90
	.type	luaD_reallocstack,@function
luaD_reallocstack:                      # @luaD_reallocstack
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 48
.Lcfi26:
	.cfi_offset %rbx, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r14
	movq	64(%r14), %rbx
	leal	6(%r15), %ebp
	movl	%r15d, %eax
	addl	$7, %eax
	js	.LBB3_2
# BB#1:
	movslq	88(%r14), %rdx
	shlq	$4, %rdx
	movslq	%ebp, %rcx
	shlq	$4, %rcx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	luaM_realloc_
	jmp	.LBB3_3
.LBB3_2:
	movq	%r14, %rdi
	callq	luaM_toobig
.LBB3_3:
	movq	%rax, 64(%r14)
	movl	%ebp, 88(%r14)
	movslq	%r15d, %rcx
	shlq	$4, %rcx
	addq	%rax, %rcx
	movq	%rcx, 56(%r14)
	movq	16(%r14), %rcx
	subq	%rbx, %rcx
	addq	%rax, %rcx
	movq	%rcx, 16(%r14)
	movq	152(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB3_6
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph39.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rcx), %rdx
	subq	%rbx, %rdx
	addq	%rax, %rdx
	movq	%rdx, 16(%rcx)
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB3_4
.LBB3_6:                                # %._crit_edge40.i
	movq	40(%r14), %rcx
	movq	80(%r14), %rdx
	cmpq	%rcx, %rdx
	ja	.LBB3_9
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rdx), %rsi
	subq	%rbx, %rsi
	addq	%rax, %rsi
	movq	%rsi, 16(%rdx)
	movq	(%rdx), %rsi
	subq	%rbx, %rsi
	addq	%rax, %rsi
	movq	%rsi, (%rdx)
	movq	8(%rdx), %rsi
	subq	%rbx, %rsi
	addq	%rax, %rsi
	movq	%rsi, 8(%rdx)
	addq	$40, %rdx
	cmpq	%rcx, %rdx
	jbe	.LBB3_7
.LBB3_9:                                # %correctstack.exit
	movq	24(%r14), %rcx
	subq	%rbx, %rcx
	addq	%rcx, %rax
	movq	%rax, 24(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	luaD_reallocstack, .Lfunc_end3-luaD_reallocstack
	.cfi_endproc

	.hidden	luaD_reallocCI
	.globl	luaD_reallocCI
	.p2align	4, 0x90
	.type	luaD_reallocCI,@function
luaD_reallocCI:                         # @luaD_reallocCI
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 48
.Lcfi35:
	.cfi_offset %rbx, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	80(%rbx), %r14
	cmpl	$-1, %ebp
	jl	.LBB4_2
# BB#1:
	movslq	92(%rbx), %rax
	shlq	$3, %rax
	leaq	(%rax,%rax,4), %rdx
	movslq	%ebp, %r15
	leaq	(,%r15,8), %rax
	leaq	(%rax,%rax,4), %rcx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	luaM_realloc_
	jmp	.LBB4_3
.LBB4_2:
	movq	%rbx, %rdi
	callq	luaM_toobig
	movslq	%ebp, %r15
.LBB4_3:
	movq	%rax, 80(%rbx)
	movl	%ebp, 92(%rbx)
	movq	40(%rbx), %rcx
	subq	%r14, %rcx
	addq	%rax, %rcx
	movq	%rcx, 40(%rbx)
	leaq	(%r15,%r15,4), %rcx
	leaq	-40(%rax,%rcx,8), %rax
	movq	%rax, 72(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	luaD_reallocCI, .Lfunc_end4-luaD_reallocCI
	.cfi_endproc

	.hidden	luaD_growstack
	.globl	luaD_growstack
	.p2align	4, 0x90
	.type	luaD_growstack,@function
luaD_growstack:                         # @luaD_growstack
	.cfi_startproc
# BB#0:
	movl	%esi, %eax
	movl	88(%rdi), %esi
	cmpl	%eax, %esi
	jge	.LBB5_1
# BB#2:
	addl	%eax, %esi
	jmp	luaD_reallocstack       # TAILCALL
.LBB5_1:
	addl	%esi, %esi
	jmp	luaD_reallocstack       # TAILCALL
.Lfunc_end5:
	.size	luaD_growstack, .Lfunc_end5-luaD_growstack
	.cfi_endproc

	.hidden	luaD_callhook
	.globl	luaD_callhook
	.p2align	4, 0x90
	.type	luaD_callhook,@function
luaD_callhook:                          # @luaD_callhook
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 40
	subq	$120, %rsp
.Lcfi43:
	.cfi_def_cfa_offset 160
.Lcfi44:
	.cfi_offset %rbx, -40
.Lcfi45:
	.cfi_offset %r12, -32
.Lcfi46:
	.cfi_offset %r14, -24
.Lcfi47:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	112(%rbx), %r14
	testq	%r14, %r14
	je	.LBB6_10
# BB#1:
	cmpb	$0, 101(%rbx)
	je	.LBB6_10
# BB#2:
	movq	16(%rbx), %rax
	movq	40(%rbx), %rdi
	movq	64(%rbx), %rcx
	movq	16(%rdi), %r15
	movl	%esi, (%rsp)
	movl	%edx, 40(%rsp)
	xorl	%edx, %edx
	cmpl	$4, %esi
	je	.LBB6_4
# BB#3:
	subq	80(%rbx), %rdi
	shrq	$3, %rdi
	imull	$-858993459, %edi, %edx # imm = 0xCCCCCCCD
.LBB6_4:
	movq	%rax, %r12
	subq	%rcx, %r12
	subq	%rcx, %r15
	movl	%edx, 116(%rsp)
	movq	56(%rbx), %rcx
	subq	%rax, %rcx
	cmpq	$320, %rcx              # imm = 0x140
	jg	.LBB6_9
# BB#5:
	movl	88(%rbx), %esi
	cmpl	$20, %esi
	jge	.LBB6_6
# BB#7:
	addl	$20, %esi
	jmp	.LBB6_8
.LBB6_6:
	addl	%esi, %esi
.LBB6_8:                                # %luaD_growstack.exit
	movq	%rbx, %rdi
	callq	luaD_reallocstack
.LBB6_9:                                # %luaD_growstack.exit
	movl	$320, %eax              # imm = 0x140
	addq	16(%rbx), %rax
	movq	40(%rbx), %rcx
	movq	%rax, 16(%rcx)
	movb	$0, 101(%rbx)
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	*%r14
	movb	$1, 101(%rbx)
	movq	40(%rbx), %rax
	movq	64(%rbx), %rcx
	addq	%rcx, %r15
	movq	%r15, 16(%rax)
	addq	%rcx, %r12
	movq	%r12, 16(%rbx)
.LBB6_10:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	luaD_callhook, .Lfunc_end6-luaD_callhook
	.cfi_endproc

	.hidden	luaD_precall
	.globl	luaD_precall
	.p2align	4, 0x90
	.type	luaD_precall,@function
luaD_precall:                           # @luaD_precall
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi54:
	.cfi_def_cfa_offset 240
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	cmpl	$6, 8(%rbp)
	jne	.LBB7_2
# BB#1:                                 # %._crit_edge114
	leaq	64(%rbx), %r13
	jmp	.LBB7_13
.LBB7_2:
	movl	$16, %edx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	luaT_gettmbyobj
	movq	%rax, %r15
	movq	%rbp, %r14
	subq	64(%rbx), %r14
	cmpl	$6, 8(%r15)
	je	.LBB7_4
# BB#3:
	movl	$.L.str.5, %edx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	luaG_typeerror
.LBB7_4:
	movq	16(%rbx), %rax
	cmpq	%rbp, %rax
	jbe	.LBB7_7
	.p2align	4, 0x90
.LBB7_5:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-16(%rax), %rcx
	movq	%rcx, (%rax)
	movl	-8(%rax), %ecx
	movl	%ecx, 8(%rax)
	leaq	-16(%rax), %rax
	cmpq	%rbp, %rax
	ja	.LBB7_5
# BB#6:                                 # %._crit_edge.loopexit.i
	movq	16(%rbx), %rax
.LBB7_7:                                # %._crit_edge.i
	movq	56(%rbx), %rcx
	subq	%rax, %rcx
	cmpq	$16, %rcx
	jg	.LBB7_12
# BB#8:
	movl	88(%rbx), %esi
	testl	%esi, %esi
	jle	.LBB7_10
# BB#9:
	addl	%esi, %esi
	jmp	.LBB7_11
.LBB7_10:
	incl	%esi
.LBB7_11:                               # %tryfuncTM.exit
	movq	%rbx, %rdi
	callq	luaD_reallocstack
.LBB7_12:                               # %tryfuncTM.exit
	leaq	64(%rbx), %r13
	addq	$16, 16(%rbx)
	movq	64(%rbx), %rax
	leaq	(%rax,%r14), %rbp
	movq	(%r15), %rcx
	movq	%rcx, (%rax,%r14)
	movl	8(%r15), %ecx
	movl	%ecx, 8(%rax,%r14)
.LBB7_13:
	movq	%rbp, %r14
	subq	64(%rbx), %r14
	movq	(%rbp), %rax
	movq	40(%rbx), %rcx
	movq	48(%rbx), %rdx
	movq	%rdx, 24(%rcx)
	cmpb	$0, 10(%rax)
	je	.LBB7_14
# BB#69:
	movq	56(%rbx), %rax
	subq	16(%rbx), %rax
	cmpq	$320, %rax              # imm = 0x140
	jg	.LBB7_74
# BB#70:
	movl	88(%rbx), %esi
	cmpl	$20, %esi
	jge	.LBB7_71
# BB#72:
	addl	$20, %esi
	jmp	.LBB7_73
.LBB7_14:
	movq	32(%rax), %rdi
	movq	56(%rbx), %rcx
	subq	16(%rbx), %rcx
	movzbl	115(%rdi), %eax
	movq	%rax, %rdx
	shlq	$4, %rdx
	cmpq	%rdx, %rcx
	jg	.LBB7_19
# BB#15:
	movl	88(%rbx), %esi
	cmpl	%eax, %esi
	jge	.LBB7_16
# BB#17:
	addl	%eax, %esi
	jmp	.LBB7_18
.LBB7_71:
	addl	%esi, %esi
.LBB7_73:                               # %luaD_growstack.exit107
	movq	%rbx, %rdi
	callq	luaD_reallocstack
.LBB7_74:                               # %luaD_growstack.exit107
	movq	40(%rbx), %rax
	cmpq	72(%rbx), %rax
	je	.LBB7_75
# BB#76:
	addq	$40, %rax
	movq	%rax, 40(%rbx)
	jmp	.LBB7_77
.LBB7_75:
	movq	%rbx, %rdi
	callq	growCI
.LBB7_77:
	movq	(%r13), %rdx
	leaq	(%rdx,%r14), %rcx
	movq	%rcx, 8(%rax)
	leaq	16(%rdx,%r14), %rcx
	movq	%rcx, (%rax)
	movq	%rcx, 24(%rbx)
	movq	16(%rbx), %rcx
	leaq	320(%rcx), %rsi
	movq	%rsi, 16(%rax)
	movl	%r12d, 32(%rax)
	testb	$1, 100(%rbx)
	je	.LBB7_86
# BB#78:
	movq	112(%rbx), %r15
	testq	%r15, %r15
	je	.LBB7_86
# BB#79:
	cmpb	$0, 101(%rbx)
	je	.LBB7_86
# BB#80:
	movq	%rcx, %r14
	subq	%rdx, %r14
	movq	40(%rbx), %rax
	movq	16(%rax), %rbp
	subq	%rdx, %rbp
	movl	$0, 64(%rsp)
	movl	$-1, 104(%rsp)
	subq	80(%rbx), %rax
	shrq	$3, %rax
	imull	$-858993459, %eax, %eax # imm = 0xCCCCCCCD
	movl	%eax, 180(%rsp)
	movq	56(%rbx), %rax
	subq	%rcx, %rax
	cmpq	$320, %rax              # imm = 0x140
	jg	.LBB7_85
# BB#81:
	movl	88(%rbx), %esi
	cmpl	$20, %esi
	jge	.LBB7_82
# BB#83:
	addl	$20, %esi
	jmp	.LBB7_84
.LBB7_16:
	addl	%esi, %esi
.LBB7_18:                               # %luaD_growstack.exit
	movq	%rdi, %rbp
	movq	%rbx, %rdi
	callq	luaD_reallocstack
	movq	%rbp, %rdi
.LBB7_19:                               # %luaD_growstack.exit
	movq	(%r13), %rbp
	addq	%r14, %rbp
	movb	114(%rdi), %al
	testb	%al, %al
	je	.LBB7_20
# BB#22:
	movq	16(%rbx), %r8
	movq	%r8, %rdx
	subq	%rbp, %rdx
	shrq	$4, %rdx
	leal	-1(%rdx), %r15d
	movzbl	113(%rdi), %ebp
	cmpl	%edx, %ebp
	jl	.LBB7_31
# BB#23:                                # %.lr.ph84.i
	movl	$1, %ecx
	subl	%edx, %ecx
	leal	-1(%rbp,%rcx), %ecx
	leal	1(%rbp), %esi
	subl	%edx, %esi
	andl	$7, %esi
	je	.LBB7_24
# BB#25:                                # %.prol.preheader
	negl	%esi
	movq	%r8, %rdx
	.p2align	4, 0x90
.LBB7_26:                               # =>This Inner Loop Header: Depth=1
	movl	$0, 8(%rdx)
	addq	$16, %rdx
	incl	%r15d
	incl	%esi
	jne	.LBB7_26
	jmp	.LBB7_27
.LBB7_20:
	leaq	16(%rbp), %r8
	movzbl	113(%rdi), %eax
	shlq	$4, %rax
	leaq	16(%rbp,%rax), %rax
	cmpq	%rax, 16(%rbx)
	jbe	.LBB7_50
# BB#21:
	movq	%rax, 16(%rbx)
	jmp	.LBB7_50
.LBB7_24:
	movq	%r8, %rdx
.LBB7_27:                               # %.prol.loopexit
	cmpl	$7, %ecx
	jb	.LBB7_30
# BB#28:                                # %.lr.ph84.i.new
	addq	$120, %rdx
	movl	%ebp, %esi
	subl	%r15d, %esi
	.p2align	4, 0x90
.LBB7_29:                               # =>This Inner Loop Header: Depth=1
	movl	$0, -112(%rdx)
	movl	$0, -96(%rdx)
	movl	$0, -80(%rdx)
	movl	$0, -64(%rdx)
	movl	$0, -48(%rdx)
	movl	$0, -32(%rdx)
	movl	$0, -16(%rdx)
	movl	$0, (%rdx)
	subq	$-128, %rdx
	addl	$-8, %esi
	jne	.LBB7_29
.LBB7_30:                               # %._crit_edge85.i
	shlq	$4, %rcx
	leaq	16(%r8,%rcx), %r8
	movq	%r8, 16(%rbx)
	movl	%ebp, %r15d
.LBB7_31:
	testb	$4, %al
	jne	.LBB7_33
# BB#32:
	xorl	%r9d, %r9d
	testb	%bpl, %bpl
	jne	.LBB7_40
	jmp	.LBB7_47
.LBB7_33:
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movq	%r14, 48(%rsp)          # 8-byte Spill
	movl	%r12d, 12(%rsp)         # 4-byte Spill
	movq	%r13, 56(%rsp)          # 8-byte Spill
	movslq	%r15d, %r14
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	subq	%rbp, %r14
	movq	32(%rbx), %rax
	movq	120(%rax), %rcx
	cmpq	112(%rax), %rcx
	jb	.LBB7_35
# BB#34:
	movq	%rbx, %rdi
	callq	luaC_step
.LBB7_35:
	movl	$1, %edx
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	luaH_new
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
	testl	%r14d, %r14d
	jle	.LBB7_38
# BB#36:                                # %.lr.ph80.i
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r13d
	shlq	$4, %rax
	movl	$8, %r12d
	subq	%rax, %r12
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_37:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %r14
	incq	%rbp
	movq	%rbx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%ebp, %edx
	callq	luaH_setnum
	movq	-8(%r14,%r12), %rcx
	movq	%rcx, (%rax)
	movl	(%r14,%r12), %ecx
	movl	%ecx, 8(%rax)
	addq	$16, %r12
	cmpq	%rbp, %r13
	jne	.LBB7_37
.LBB7_38:                               # %._crit_edge81.i
	movl	$.L.str.6, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	luaS_newlstr
	movq	%rbx, %rdi
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rsi
	movq	%rax, %rdx
	callq	luaH_setstr
	movq	16(%rsp), %rcx          # 8-byte Reload
	cvtsi2sdl	%ecx, %xmm0
	movsd	%xmm0, (%rax)
	movl	$3, 8(%rax)
	movq	16(%rbx), %r8
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	12(%rsp), %r12d         # 4-byte Reload
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%r13, %r9
	movq	%rax, %r13
	testb	%bpl, %bpl
	je	.LBB7_47
.LBB7_40:                               # %.lr.ph.preheader.i
	movslq	%r15d, %rdx
	movq	%rdx, %rax
	negq	%rax
	shlq	$4, %rdx
	movq	%r8, %rcx
	subq	%rdx, %rcx
	leaq	16(%r8), %rdx
	movq	%rdx, 16(%rbx)
	movq	(%rcx), %rdx
	movq	%rdx, (%r8)
	movq	%rax, %rdx
	shlq	$4, %rdx
	movl	8(%r8,%rdx), %esi
	movl	%esi, 8(%r8)
	movl	$0, 8(%r8,%rdx)
	cmpb	$1, %bpl
	je	.LBB7_47
# BB#41:                                # %.lr.ph..lr.ph_crit_edge.i.preheader
	testb	$1, %bpl
	jne	.LBB7_42
# BB#43:                                # %.lr.ph..lr.ph_crit_edge.i.prol
	movq	16(%rbx), %rdx
	leaq	16(%rdx), %rsi
	movq	%rsi, 16(%rbx)
	movq	16(%rcx), %rsi
	movq	%rsi, (%rdx)
	movl	24(%rcx), %esi
	movl	%esi, 8(%rdx)
	movl	$0, 24(%rcx)
	movl	$2, %ecx
	cmpb	$2, %bpl
	jne	.LBB7_45
	jmp	.LBB7_47
.LBB7_42:
	movl	$1, %ecx
	cmpb	$2, %bpl
	je	.LBB7_47
.LBB7_45:                               # %.lr.ph..lr.ph_crit_edge.i.preheader.new
	subq	%rcx, %rbp
	leaq	1(%rcx,%rax), %rax
	shlq	$4, %rax
	leaq	8(%r8,%rax), %rax
	.p2align	4, 0x90
.LBB7_46:                               # %.lr.ph..lr.ph_crit_edge.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rcx
	leaq	16(%rcx), %rdx
	movq	%rdx, 16(%rbx)
	movq	-24(%rax), %rdx
	movq	%rdx, (%rcx)
	movl	-16(%rax), %edx
	movl	%edx, 8(%rcx)
	movl	$0, -16(%rax)
	movq	16(%rbx), %rcx
	leaq	16(%rcx), %rdx
	movq	%rdx, 16(%rbx)
	movq	-8(%rax), %rdx
	movq	%rdx, (%rcx)
	movl	(%rax), %edx
	movl	%edx, 8(%rcx)
	movl	$0, (%rax)
	addq	$32, %rax
	addq	$-2, %rbp
	jne	.LBB7_46
.LBB7_47:                               # %._crit_edge.i106
	testq	%r9, %r9
	je	.LBB7_49
# BB#48:
	movq	16(%rbx), %rax
	leaq	16(%rax), %rcx
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	movl	$5, 8(%rax)
.LBB7_49:                               # %adjust_varargs.exit
	addq	(%r13), %r14
	movq	%r14, %rbp
.LBB7_50:
	movq	40(%rbx), %rax
	cmpq	72(%rbx), %rax
	je	.LBB7_51
# BB#52:
	addq	$40, %rax
	movq	%rax, 40(%rbx)
	jmp	.LBB7_53
.LBB7_51:
	movq	%rdi, %r14
	movq	%rbx, %rdi
	movq	%r8, %r15
	callq	growCI
	movq	%r15, %r8
	movq	%r14, %rdi
.LBB7_53:
	movq	%rbp, 8(%rax)
	movq	%r8, (%rax)
	movq	%r8, 24(%rbx)
	movzbl	115(%rdi), %ecx
	shlq	$4, %rcx
	addq	%r8, %rcx
	movq	%rcx, 16(%rax)
	movq	24(%rdi), %rdx
	movq	%rdx, 48(%rbx)
	movl	$0, 36(%rax)
	movl	%r12d, 32(%rax)
	movq	16(%rbx), %rax
	cmpq	%rcx, %rax
	jae	.LBB7_59
# BB#54:                                # %.lr.ph.preheader
	movq	%rax, %rsi
	notq	%rsi
	addq	%rcx, %rsi
	movl	%esi, %edi
	shrl	$4, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB7_57
# BB#55:                                # %.lr.ph.prol.preheader
	negq	%rdi
	.p2align	4, 0x90
.LBB7_56:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, 8(%rax)
	addq	$16, %rax
	incq	%rdi
	jne	.LBB7_56
.LBB7_57:                               # %.lr.ph.prol.loopexit
	cmpq	$112, %rsi
	jb	.LBB7_59
	.p2align	4, 0x90
.LBB7_58:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, 8(%rax)
	movl	$0, 24(%rax)
	movl	$0, 40(%rax)
	movl	$0, 56(%rax)
	movl	$0, 72(%rax)
	movl	$0, 88(%rax)
	movl	$0, 104(%rax)
	movl	$0, 120(%rax)
	subq	$-128, %rax
	cmpq	%rcx, %rax
	jb	.LBB7_58
.LBB7_59:                               # %._crit_edge
	movq	%rcx, 16(%rbx)
	xorl	%ebp, %ebp
	testb	$1, 100(%rbx)
	je	.LBB7_89
# BB#60:
	addq	$4, %rdx
	movq	%rdx, 48(%rbx)
	movq	112(%rbx), %r14
	testq	%r14, %r14
	je	.LBB7_68
# BB#61:
	cmpb	$0, 101(%rbx)
	je	.LBB7_68
# BB#62:
	movq	40(%rbx), %rax
	movq	64(%rbx), %rdx
	movq	%rcx, %r15
	subq	%rdx, %r15
	movq	16(%rax), %r12
	subq	%rdx, %r12
	movl	$0, 64(%rsp)
	movl	$-1, 104(%rsp)
	subq	80(%rbx), %rax
	shrq	$3, %rax
	imull	$-858993459, %eax, %eax # imm = 0xCCCCCCCD
	movl	%eax, 180(%rsp)
	movq	56(%rbx), %rax
	subq	%rcx, %rax
	cmpq	$320, %rax              # imm = 0x140
	jg	.LBB7_67
# BB#63:
	movl	88(%rbx), %esi
	cmpl	$20, %esi
	jge	.LBB7_64
# BB#65:
	addl	$20, %esi
	jmp	.LBB7_66
.LBB7_82:
	addl	%esi, %esi
.LBB7_84:                               # %luaD_growstack.exit.i108
	movq	%rbx, %rdi
	callq	luaD_reallocstack
.LBB7_85:                               # %luaD_growstack.exit.i108
	movl	$320, %eax              # imm = 0x140
	addq	16(%rbx), %rax
	movq	40(%rbx), %rcx
	movq	%rax, 16(%rcx)
	movb	$0, 101(%rbx)
	leaq	64(%rsp), %rsi
	movq	%rbx, %rdi
	callq	*%r15
	movb	$1, 101(%rbx)
	movq	(%r13), %rax
	addq	%rax, %rbp
	movq	40(%rbx), %rcx
	movq	%rbp, 16(%rcx)
	addq	%rax, %r14
	movq	%r14, 16(%rbx)
.LBB7_86:                               # %luaD_callhook.exit109
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	testl	%eax, %eax
	js	.LBB7_87
# BB#88:
	movq	16(%rbx), %rsi
	cltq
	shlq	$4, %rax
	subq	%rax, %rsi
	movq	%rbx, %rdi
	callq	luaD_poscall
	movl	$1, %ebp
	jmp	.LBB7_89
.LBB7_87:
	movl	$2, %ebp
	jmp	.LBB7_89
.LBB7_64:
	addl	%esi, %esi
.LBB7_66:                               # %luaD_growstack.exit.i
	movq	%rbx, %rdi
	callq	luaD_reallocstack
.LBB7_67:                               # %luaD_growstack.exit.i
	movl	$320, %eax              # imm = 0x140
	addq	16(%rbx), %rax
	movq	40(%rbx), %rcx
	movq	%rax, 16(%rcx)
	movb	$0, 101(%rbx)
	leaq	64(%rsp), %rsi
	movq	%rbx, %rdi
	callq	*%r14
	movb	$1, 101(%rbx)
	movq	(%r13), %rax
	addq	%rax, %r12
	movq	40(%rbx), %rcx
	movq	%r12, 16(%rcx)
	addq	%rax, %r15
	movq	%r15, 16(%rbx)
	movq	48(%rbx), %rdx
.LBB7_68:                               # %luaD_callhook.exit
	addq	$-4, %rdx
	movq	%rdx, 48(%rbx)
.LBB7_89:
	movl	%ebp, %eax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	luaD_precall, .Lfunc_end7-luaD_precall
	.cfi_endproc

	.p2align	4, 0x90
	.type	growCI,@function
growCI:                                 # @growCI
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 48
.Lcfi66:
	.cfi_offset %rbx, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movslq	92(%rbx), %rax
	cmpq	$20001, %rax            # imm = 0x4E21
	jge	.LBB8_7
# BB#1:
	leal	(%rax,%rax), %ebp
	movq	80(%rbx), %r14
	cmpl	$-1, %ebp
	jl	.LBB8_3
# BB#2:
	shlq	$3, %rax
	leaq	(%rax,%rax,4), %rdx
	movslq	%ebp, %r15
	leaq	(,%r15,8), %rax
	leaq	(%rax,%rax,4), %rcx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	luaM_realloc_
	jmp	.LBB8_4
.LBB8_3:
	movq	%rbx, %rdi
	callq	luaM_toobig
	movslq	%ebp, %r15
.LBB8_4:                                # %luaD_reallocCI.exit
	movq	%rax, 80(%rbx)
	movl	%ebp, 92(%rbx)
	movq	40(%rbx), %rcx
	subq	%r14, %rcx
	addq	%rax, %rcx
	movq	%rcx, 40(%rbx)
	leaq	(%r15,%r15,4), %rdx
	leaq	-40(%rax,%rdx,8), %rax
	movq	%rax, 72(%rbx)
	cmpl	$20001, %ebp            # imm = 0x4E21
	jl	.LBB8_6
# BB#5:
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaG_runerror
	movq	40(%rbx), %rcx
.LBB8_6:
	addq	$40, %rcx
	movq	%rcx, 40(%rbx)
	movq	%rcx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_7:
	movl	$5, %esi
	movq	%rbx, %rdi
	callq	luaD_throw
.Lfunc_end8:
	.size	growCI, .Lfunc_end8-growCI
	.cfi_endproc

	.hidden	luaD_poscall
	.globl	luaD_poscall
	.p2align	4, 0x90
	.type	luaD_poscall,@function
luaD_poscall:                           # @luaD_poscall
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi76:
	.cfi_def_cfa_offset 176
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testb	$2, 100(%r14)
	jne	.LBB9_2
# BB#1:                                 # %._crit_edge
	movq	40(%r14), %rcx
	jmp	.LBB9_26
.LBB9_2:
	movq	64(%r14), %r15
	movq	112(%r14), %r13
	testq	%r13, %r13
	je	.LBB9_8
# BB#3:
	cmpb	$0, 101(%r14)
	je	.LBB9_8
# BB#4:
	movq	16(%r14), %r12
	movq	40(%r14), %rax
	movq	16(%rax), %rbp
	subq	%r15, %rbp
	movl	$1, (%rsp)
	movl	$-1, 40(%rsp)
	subq	80(%r14), %rax
	shrq	$3, %rax
	imull	$-858993459, %eax, %eax # imm = 0xCCCCCCCD
	movl	%eax, 116(%rsp)
	movq	56(%r14), %rax
	subq	%r12, %rax
	subq	%r15, %r12
	cmpq	$320, %rax              # imm = 0x140
	jg	.LBB9_11
# BB#5:
	movl	88(%r14), %esi
	cmpl	$20, %esi
	jge	.LBB9_9
# BB#6:
	addl	$20, %esi
	jmp	.LBB9_10
.LBB9_8:
	movq	%r15, %rax
	jmp	.LBB9_12
.LBB9_9:
	addl	%esi, %esi
.LBB9_10:                               # %luaD_growstack.exit.i.i
	movq	%r14, %rdi
	callq	luaD_reallocstack
.LBB9_11:                               # %luaD_growstack.exit.i.i
	movl	$320, %eax              # imm = 0x140
	addq	16(%r14), %rax
	movq	40(%r14), %rcx
	movq	%rax, 16(%rcx)
	movb	$0, 101(%r14)
	movq	%rsp, %rsi
	movq	%r14, %rdi
	callq	*%r13
	movb	$1, 101(%r14)
	movq	40(%r14), %rcx
	movq	64(%r14), %rax
	addq	%rax, %rbp
	movq	%rbp, 16(%rcx)
	addq	%rax, %r12
	movq	%r12, 16(%r14)
.LBB9_12:                               # %luaD_callhook.exit.i
	subq	%r15, %rbx
	movq	40(%r14), %rcx
	movq	8(%rcx), %rdx
	movq	(%rdx), %rdx
	cmpb	$0, 10(%rdx)
	jne	.LBB9_25
# BB#13:                                # %luaD_callhook.exit10.preheader.i
	movb	100(%r14), %dl
	testb	$2, %dl
	je	.LBB9_25
# BB#14:                                # %.lr.ph.i
	movq	%rsp, %r15
	.p2align	4, 0x90
.LBB9_15:                               # =>This Inner Loop Header: Depth=1
	movl	36(%rcx), %esi
	leal	-1(%rsi), %edi
	movl	%edi, 36(%rcx)
	testl	%esi, %esi
	je	.LBB9_25
# BB#16:                                #   in Loop: Header=BB9_15 Depth=1
	movq	112(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB9_24
# BB#17:                                #   in Loop: Header=BB9_15 Depth=1
	cmpb	$0, 101(%r14)
	je	.LBB9_24
# BB#18:                                #   in Loop: Header=BB9_15 Depth=1
	movq	16(%r14), %r12
	movq	16(%rcx), %r13
	movl	$4, (%rsp)
	movl	$-1, 40(%rsp)
	movl	$0, 116(%rsp)
	movq	56(%r14), %rcx
	subq	%r12, %rcx
	subq	%rax, %r12
	subq	%rax, %r13
	cmpq	$320, %rcx              # imm = 0x140
	jg	.LBB9_23
# BB#19:                                #   in Loop: Header=BB9_15 Depth=1
	movl	88(%r14), %esi
	cmpl	$20, %esi
	jge	.LBB9_21
# BB#20:                                #   in Loop: Header=BB9_15 Depth=1
	addl	$20, %esi
	jmp	.LBB9_22
.LBB9_21:                               #   in Loop: Header=BB9_15 Depth=1
	addl	%esi, %esi
.LBB9_22:                               # %luaD_growstack.exit.i9.i
                                        #   in Loop: Header=BB9_15 Depth=1
	movq	%r14, %rdi
	callq	luaD_reallocstack
.LBB9_23:                               # %luaD_growstack.exit.i9.i
                                        #   in Loop: Header=BB9_15 Depth=1
	movq	16(%r14), %rax
	movl	$320, %ecx              # imm = 0x140
	addq	%rcx, %rax
	movq	40(%r14), %rcx
	movq	%rax, 16(%rcx)
	movb	$0, 101(%r14)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	*%rbp
	movb	$1, 101(%r14)
	movq	40(%r14), %rcx
	movq	64(%r14), %rax
	addq	%rax, %r13
	movq	%r13, 16(%rcx)
	addq	%rax, %r12
	movq	%r12, 16(%r14)
	movzbl	100(%r14), %edx
.LBB9_24:                               # %luaD_callhook.exit10.backedge.i
                                        #   in Loop: Header=BB9_15 Depth=1
	testb	$2, %dl
	jne	.LBB9_15
.LBB9_25:                               # %callrethooks.exit
	addq	%rax, %rbx
.LBB9_26:
	leaq	-40(%rcx), %rax
	movq	%rax, 40(%r14)
	movq	-40(%rcx), %rsi
	movq	8(%rcx), %rdx
	movl	32(%rcx), %eax
	movq	%rsi, 24(%r14)
	movq	-16(%rcx), %rcx
	movq	%rcx, 48(%r14)
	testl	%eax, %eax
	je	.LBB9_33
# BB#27:
	movl	%eax, %ecx
	.p2align	4, 0x90
.LBB9_28:                               # %.lr.ph44
                                        # =>This Inner Loop Header: Depth=1
	cmpq	16(%r14), %rbx
	jae	.LBB9_30
# BB#29:                                #   in Loop: Header=BB9_28 Depth=1
	movq	(%rbx), %rsi
	movq	%rsi, (%rdx)
	movl	8(%rbx), %esi
	movl	%esi, 8(%rdx)
	leaq	16(%rdx), %rdx
	addq	$16, %rbx
	decl	%ecx
	jne	.LBB9_28
	jmp	.LBB9_33
.LBB9_30:                               # %.critedge.preheader
	testl	%ecx, %ecx
	jle	.LBB9_33
# BB#31:
	incl	%ecx
	.p2align	4, 0x90
.LBB9_32:                               # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, 8(%rdx)
	addq	$16, %rdx
	decl	%ecx
	cmpl	$1, %ecx
	jg	.LBB9_32
.LBB9_33:                               # %.critedge._crit_edge
	movq	%rdx, 16(%r14)
	incl	%eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	luaD_poscall, .Lfunc_end9-luaD_poscall
	.cfi_endproc

	.hidden	luaD_call
	.globl	luaD_call
	.p2align	4, 0x90
	.type	luaD_call,@function
luaD_call:                              # @luaD_call
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 32
.Lcfi86:
	.cfi_offset %rbx, -32
.Lcfi87:
	.cfi_offset %r14, -24
.Lcfi88:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movzwl	96(%rbx), %eax
	incl	%eax
	movw	%ax, 96(%rbx)
	movzwl	%ax, %eax
	cmpl	$200, %eax
	jb	.LBB10_4
# BB#1:
	jne	.LBB10_3
# BB#2:
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaG_runerror
	jmp	.LBB10_4
.LBB10_3:
	cmpl	$225, %eax
	jae	.LBB10_8
.LBB10_4:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movl	%r14d, %edx
	callq	luaD_precall
	testl	%eax, %eax
	jne	.LBB10_6
# BB#5:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	luaV_execute
.LBB10_6:
	decw	96(%rbx)
	movq	32(%rbx), %rax
	movq	120(%rax), %rcx
	cmpq	112(%rax), %rcx
	jae	.LBB10_9
# BB#7:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB10_9:
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	luaC_step               # TAILCALL
.LBB10_8:
	movl	$5, %esi
	movq	%rbx, %rdi
	callq	luaD_throw
.Lfunc_end10:
	.size	luaD_call, .Lfunc_end10-luaD_call
	.cfi_endproc

	.globl	lua_resume
	.p2align	4, 0x90
	.type	lua_resume,@function
lua_resume:                             # @lua_resume
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi91:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi93:
	.cfi_def_cfa_offset 48
.Lcfi94:
	.cfi_offset %rbx, -40
.Lcfi95:
	.cfi_offset %r12, -32
.Lcfi96:
	.cfi_offset %r14, -24
.Lcfi97:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movb	10(%r12), %al
	testb	%al, %al
	je	.LBB11_3
# BB#1:
	cmpb	$1, %al
	je	.LBB11_10
# BB#2:                                 # %._crit_edge
	movq	40(%r12), %rax
	jmp	.LBB11_4
.LBB11_3:
	movq	40(%r12), %rax
	cmpq	80(%r12), %rax
	je	.LBB11_10
.LBB11_4:
	movq	(%rax), %rbx
	movq	%rbx, 16(%r12)
	movl	$.L.str.3, %esi
	movl	$37, %edx
	movq	%r12, %rdi
	callq	luaS_newlstr
	movq	%rax, (%rbx)
	movl	$4, 8(%rbx)
	movq	56(%r12), %rax
	subq	16(%r12), %rax
	cmpq	$16, %rax
	jg	.LBB11_9
# BB#5:
	movl	88(%r12), %esi
	testl	%esi, %esi
	jle	.LBB11_7
# BB#6:
	addl	%esi, %esi
	jmp	.LBB11_8
.LBB11_10:
	movzwl	96(%r12), %eax
	cmpl	$200, %eax
	jb	.LBB11_14
# BB#11:
	movq	40(%r12), %rax
	movq	(%rax), %rbx
	movq	%rbx, 16(%r12)
	movl	$.L.str.2, %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	luaS_newlstr
	movq	%rax, (%rbx)
	movl	$4, 8(%rbx)
	movq	56(%r12), %rax
	subq	16(%r12), %rax
	cmpq	$16, %rax
	jg	.LBB11_9
# BB#12:
	movl	88(%r12), %esi
	testl	%esi, %esi
	jle	.LBB11_7
# BB#13:
	addl	%esi, %esi
	jmp	.LBB11_8
.LBB11_14:
	incl	%eax
	movw	%ax, 96(%r12)
	movw	%ax, 98(%r12)
	movq	16(%r12), %rdx
	movslq	%esi, %rax
	shlq	$4, %rax
	subq	%rax, %rdx
	movl	$resume, %esi
	movq	%r12, %rdi
	callq	luaD_rawrunprotected
	movl	%eax, %r14d
	testl	%r14d, %r14d
	je	.LBB11_24
# BB#15:
	movb	%r14b, 10(%r12)
	movq	16(%r12), %r15
	leal	-2(%r14), %eax
	cmpl	$2, %eax
	jb	.LBB11_21
# BB#16:
	cmpl	$5, %r14d
	je	.LBB11_20
# BB#17:
	cmpl	$4, %r14d
	jne	.LBB11_23
# BB#18:
	movl	$.L.str, %esi
	movl	$17, %edx
	jmp	.LBB11_19
.LBB11_7:
	incl	%esi
.LBB11_8:                               # %resume_error.exit
	movq	%r12, %rdi
	callq	luaD_reallocstack
.LBB11_9:                               # %resume_error.exit
	addq	$16, 16(%r12)
	movl	$2, %r14d
	jmp	.LBB11_26
.LBB11_24:
	movzbl	10(%r12), %r14d
	jmp	.LBB11_25
.LBB11_21:
	movq	-16(%r15), %rax
	movq	%rax, (%r15)
	movl	-8(%r15), %eax
	jmp	.LBB11_22
.LBB11_20:
	movl	$.L.str.1, %esi
	movl	$23, %edx
.LBB11_19:                              # %.sink.split.i
	movq	%r12, %rdi
	callq	luaS_newlstr
	movq	%rax, (%r15)
	movl	$4, %eax
.LBB11_22:                              # %.sink.split.i
	movl	%eax, 8(%r15)
.LBB11_23:                              # %luaD_seterrorobj.exit
	addq	$16, %r15
	movq	%r15, 16(%r12)
	movq	40(%r12), %rax
	movq	%r15, 16(%rax)
.LBB11_25:
	decw	96(%r12)
.LBB11_26:
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	lua_resume, .Lfunc_end11-lua_resume
	.cfi_endproc

	.p2align	4, 0x90
	.type	resume,@function
resume:                                 # @resume
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 16
.Lcfi99:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpb	$0, 10(%rbx)
	je	.LBB12_1
# BB#3:
	movq	40(%rbx), %rax
	movb	$0, 10(%rbx)
	movq	8(%rax), %rcx
	movq	(%rcx), %rcx
	cmpb	$0, 10(%rcx)
	je	.LBB12_6
# BB#4:
	movq	%rbx, %rdi
	callq	luaD_poscall
	testl	%eax, %eax
	je	.LBB12_7
# BB#5:
	movq	40(%rbx), %rax
	movq	16(%rax), %rax
	movq	%rax, 16(%rbx)
	jmp	.LBB12_7
.LBB12_1:
	addq	$-16, %rsi
	movl	$-1, %edx
	movq	%rbx, %rdi
	callq	luaD_precall
	testl	%eax, %eax
	je	.LBB12_7
# BB#2:
	popq	%rbx
	retq
.LBB12_6:
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.LBB12_7:
	movq	40(%rbx), %rax
	subq	80(%rbx), %rax
	shrq	$3, %rax
	imull	$-858993459, %eax, %esi # imm = 0xCCCCCCCD
	movq	%rbx, %rdi
	popq	%rbx
	jmp	luaV_execute            # TAILCALL
.Lfunc_end12:
	.size	resume, .Lfunc_end12-resume
	.cfi_endproc

	.globl	lua_yield
	.p2align	4, 0x90
	.type	lua_yield,@function
lua_yield:                              # @lua_yield
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi100:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi102:
	.cfi_def_cfa_offset 32
.Lcfi103:
	.cfi_offset %rbx, -24
.Lcfi104:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movzwl	96(%rbx), %eax
	cmpw	98(%rbx), %ax
	jbe	.LBB13_2
# BB#1:
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaG_runerror
.LBB13_2:
	movq	16(%rbx), %rax
	movslq	%ebp, %rcx
	shlq	$4, %rcx
	subq	%rcx, %rax
	movq	%rax, 24(%rbx)
	movb	$1, 10(%rbx)
	movl	$-1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end13:
	.size	lua_yield, .Lfunc_end13-lua_yield
	.cfi_endproc

	.hidden	luaD_pcall
	.globl	luaD_pcall
	.p2align	4, 0x90
	.type	luaD_pcall,@function
luaD_pcall:                             # @luaD_pcall
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi107:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi108:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi109:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi111:
	.cfi_def_cfa_offset 80
.Lcfi112:
	.cfi_offset %rbx, -56
.Lcfi113:
	.cfi_offset %r12, -48
.Lcfi114:
	.cfi_offset %r13, -40
.Lcfi115:
	.cfi_offset %r14, -32
.Lcfi116:
	.cfi_offset %r15, -24
.Lcfi117:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdi, %rbx
	movzwl	96(%rbx), %eax
	movw	%ax, 14(%rsp)           # 2-byte Spill
	movq	40(%rbx), %r13
	movq	80(%rbx), %rbp
	movb	101(%rbx), %r12b
	movq	176(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r8, 176(%rbx)
	callq	luaD_rawrunprotected
	movl	%eax, %r14d
	testl	%r14d, %r14d
	je	.LBB14_12
# BB#1:
	subq	%rbp, %r13
	addq	64(%rbx), %r15
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	luaF_close
	leal	-2(%r14), %eax
	cmpl	$2, %eax
	jb	.LBB14_6
# BB#2:
	cmpl	$5, %r14d
	je	.LBB14_5
# BB#3:
	cmpl	$4, %r14d
	movzwl	14(%rsp), %ebp          # 2-byte Folded Reload
	jne	.LBB14_9
# BB#4:
	movl	$.L.str, %esi
	movl	$17, %edx
	movq	%rbx, %rdi
	callq	luaS_newlstr
	movq	%rax, (%r15)
	movl	$4, %eax
	jmp	.LBB14_8
.LBB14_6:
	movq	16(%rbx), %rax
	movq	-16(%rax), %rcx
	movq	%rcx, (%r15)
	movl	-8(%rax), %eax
	jmp	.LBB14_7
.LBB14_5:
	movl	$.L.str.1, %esi
	movl	$23, %edx
	movq	%rbx, %rdi
	callq	luaS_newlstr
	movq	%rax, (%r15)
	movl	$4, %eax
.LBB14_7:                               # %.sink.split.i
	movzwl	14(%rsp), %ebp          # 2-byte Folded Reload
.LBB14_8:                               # %.sink.split.i
	movl	%eax, 8(%r15)
.LBB14_9:                               # %luaD_seterrorobj.exit
	addq	$16, %r15
	movq	%r15, 16(%rbx)
	movw	%bp, 96(%rbx)
	movq	80(%rbx), %r15
	leaq	(%r15,%r13), %rax
	movq	%rax, 40(%rbx)
	movq	(%r15,%r13), %rax
	movq	%rax, 24(%rbx)
	movq	24(%r15,%r13), %rax
	movq	%rax, 48(%rbx)
	movb	%r12b, 101(%rbx)
	movslq	92(%rbx), %rax
	cmpq	$20001, %rax            # imm = 0x4E21
	jl	.LBB14_12
# BB#10:
	shrq	$3, %r13
	imull	$-858993459, %r13d, %ecx # imm = 0xCCCCCCCD
	incl	%ecx
	cmpl	$19999, %ecx            # imm = 0x4E1F
	jg	.LBB14_12
# BB#11:
	shlq	$3, %rax
	leaq	(%rax,%rax,4), %rdx
	movl	$800000, %ecx           # imm = 0xC3500
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	luaM_realloc_
	movq	%rax, 80(%rbx)
	movl	$20000, 92(%rbx)        # imm = 0x4E20
	movq	40(%rbx), %rcx
	subq	%r15, %rcx
	addq	%rax, %rcx
	movq	%rcx, 40(%rbx)
	addq	$799960, %rax           # imm = 0xC34D8
	movq	%rax, 72(%rbx)
.LBB14_12:                              # %restore_stack_limit.exit
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 176(%rbx)
	movl	%r14d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	luaD_pcall, .Lfunc_end14-luaD_pcall
	.cfi_endproc

	.hidden	luaD_protectedparser
	.globl	luaD_protectedparser
	.p2align	4, 0x90
	.type	luaD_protectedparser,@function
luaD_protectedparser:                   # @luaD_protectedparser
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi118:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi119:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi120:
	.cfi_def_cfa_offset 64
.Lcfi121:
	.cfi_offset %rbx, -24
.Lcfi122:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	%rsi, (%rsp)
	movq	%rdx, 32(%rsp)
	movq	$0, 8(%rsp)
	movq	$0, 24(%rsp)
	movq	16(%rbx), %rcx
	movq	176(%rbx), %r8
	subq	64(%rbx), %rcx
	movq	%rsp, %rdx
	movl	$f_parser, %esi
	callq	luaD_pcall
	movl	%eax, %ebp
	movq	8(%rsp), %rsi
	movq	24(%rsp), %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	luaM_realloc_
	movl	%ebp, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end15:
	.size	luaD_protectedparser, .Lfunc_end15-luaD_protectedparser
	.cfi_endproc

	.p2align	4, 0x90
	.type	f_parser,@function
f_parser:                               # @f_parser
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi124:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi125:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi127:
	.cfi_def_cfa_offset 48
.Lcfi128:
	.cfi_offset %rbx, -40
.Lcfi129:
	.cfi_offset %r14, -32
.Lcfi130:
	.cfi_offset %r15, -24
.Lcfi131:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%rbx), %rdi
	callq	luaZ_lookahead
	movl	%eax, %ebp
	movq	32(%r14), %rax
	movq	120(%rax), %rcx
	cmpq	112(%rax), %rcx
	jb	.LBB16_2
# BB#1:
	movq	%r14, %rdi
	callq	luaC_step
.LBB16_2:
	cmpl	$27, %ebp
	movl	$luaU_undump, %eax
	movl	$luaY_parser, %ebp
	cmoveq	%rax, %rbp
	movq	(%rbx), %rsi
	movq	32(%rbx), %rcx
	leaq	8(%rbx), %rdx
	movq	%r14, %rdi
	callq	*%rbp
	movq	%rax, %r15
	movzbl	112(%r15), %esi
	movq	120(%r14), %rdx
	movq	%r14, %rdi
	callq	luaF_newLclosure
	movq	%rax, %rbx
	movq	%r15, 32(%rbx)
	cmpb	$0, 112(%r15)
	je	.LBB16_5
# BB#3:                                 # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB16_4:                               # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	luaF_newupval
	movq	%rax, 40(%rbx,%rbp,8)
	incq	%rbp
	movzbl	112(%r15), %eax
	cmpq	%rax, %rbp
	jl	.LBB16_4
.LBB16_5:                               # %._crit_edge
	movq	16(%r14), %rax
	movq	%rbx, (%rax)
	movl	$6, 8(%rax)
	movq	56(%r14), %rax
	subq	16(%r14), %rax
	cmpq	$16, %rax
	jg	.LBB16_10
# BB#6:
	movl	88(%r14), %esi
	testl	%esi, %esi
	jle	.LBB16_8
# BB#7:
	addl	%esi, %esi
	jmp	.LBB16_9
.LBB16_8:
	incl	%esi
.LBB16_9:                               # %luaD_growstack.exit
	movq	%r14, %rdi
	callq	luaD_reallocstack
.LBB16_10:                              # %luaD_growstack.exit
	addq	$16, 16(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	f_parser, .Lfunc_end16-f_parser
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"not enough memory"
	.size	.L.str, 18

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"error in error handling"
	.size	.L.str.1, 24

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"C stack overflow"
	.size	.L.str.2, 17

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"cannot resume non-suspended coroutine"
	.size	.L.str.3, 38

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"attempt to yield across metamethod/C-call boundary"
	.size	.L.str.4, 51

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"call"
	.size	.L.str.5, 5

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"n"
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"stack overflow"
	.size	.L.str.7, 15

	.hidden	luaS_newlstr
	.hidden	luaM_realloc_
	.hidden	luaM_toobig
	.hidden	luaG_runerror
	.hidden	luaV_execute
	.hidden	luaC_step
	.hidden	luaF_close
	.hidden	luaT_gettmbyobj
	.hidden	luaG_typeerror
	.hidden	luaH_new
	.hidden	luaH_setnum
	.hidden	luaH_setstr
	.hidden	luaZ_lookahead
	.hidden	luaU_undump
	.hidden	luaY_parser
	.hidden	luaF_newLclosure
	.hidden	luaF_newupval

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
