	.text
	.file	"ZDecoder.bc"
	.globl	_ZN9NCompress2NZ8CDecoder4FreeEv
	.p2align	4, 0x90
	.type	_ZN9NCompress2NZ8CDecoder4FreeEv,@function
_ZN9NCompress2NZ8CDecoder4FreeEv:       # @_ZN9NCompress2NZ8CDecoder4FreeEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	callq	MyFree
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
	callq	MyFree
	movq	$0, 32(%rbx)
	movq	40(%rbx), %rdi
	callq	MyFree
	movq	$0, 40(%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN9NCompress2NZ8CDecoder4FreeEv, .Lfunc_end0-_ZN9NCompress2NZ8CDecoder4FreeEv
	.cfi_endproc

	.globl	_ZN9NCompress2NZ8CDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress2NZ8CDecoderD2Ev,@function
_ZN9NCompress2NZ8CDecoderD2Ev:          # @_ZN9NCompress2NZ8CDecoderD2Ev
	.cfi_startproc
# BB#0:                                 # %.noexc
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress2NZ8CDecoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress2NZ8CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	24(%rbx), %rdi
	callq	MyFree
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
	callq	MyFree
	movq	$0, 32(%rbx)
	movq	40(%rbx), %rdi
	callq	MyFree
	movq	$0, 40(%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN9NCompress2NZ8CDecoderD2Ev, .Lfunc_end1-_ZN9NCompress2NZ8CDecoderD2Ev
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZThn8_N9NCompress2NZ8CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress2NZ8CDecoderD1Ev,@function
_ZThn8_N9NCompress2NZ8CDecoderD1Ev:     # @_ZThn8_N9NCompress2NZ8CDecoderD1Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 16
.Lcfi5:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress2NZ8CDecoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress2NZ8CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rbx)
	movq	16(%rbx), %rdi
	callq	MyFree
	movq	$0, 16(%rbx)
	movq	24(%rbx), %rdi
	callq	MyFree
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
	callq	MyFree
	movq	$0, 32(%rbx)
	popq	%rbx
	retq
.Lfunc_end3:
	.size	_ZThn8_N9NCompress2NZ8CDecoderD1Ev, .Lfunc_end3-_ZThn8_N9NCompress2NZ8CDecoderD1Ev
	.cfi_endproc

	.globl	_ZN9NCompress2NZ8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress2NZ8CDecoderD0Ev,@function
_ZN9NCompress2NZ8CDecoderD0Ev:          # @_ZN9NCompress2NZ8CDecoderD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress2NZ8CDecoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress2NZ8CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	24(%rbx), %rdi
.Ltmp0:
	callq	MyFree
.Ltmp1:
# BB#1:                                 # %.noexc
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
.Ltmp2:
	callq	MyFree
.Ltmp3:
# BB#2:                                 # %.noexc2
	movq	$0, 32(%rbx)
	movq	40(%rbx), %rdi
.Ltmp4:
	callq	MyFree
.Ltmp5:
# BB#3:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB4_4:
.Ltmp6:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN9NCompress2NZ8CDecoderD0Ev, .Lfunc_end4-_ZN9NCompress2NZ8CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp5-.Ltmp0           #   Call between .Ltmp0 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end4-.Ltmp5      #   Call between .Ltmp5 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn8_N9NCompress2NZ8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress2NZ8CDecoderD0Ev,@function
_ZThn8_N9NCompress2NZ8CDecoderD0Ev:     # @_ZThn8_N9NCompress2NZ8CDecoderD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %r14, -16
	movl	$_ZTVN9NCompress2NZ8CDecoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress2NZ8CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rdi)
	movq	16(%rdi), %rax
	leaq	-8(%rdi), %rbx
.Ltmp7:
	movq	%rax, %rdi
	callq	MyFree
.Ltmp8:
# BB#1:                                 # %.noexc.i
	movq	$0, 24(%rbx)
	movq	32(%rbx), %rdi
.Ltmp9:
	callq	MyFree
.Ltmp10:
# BB#2:                                 # %.noexc2.i
	movq	$0, 32(%rbx)
	movq	40(%rbx), %rdi
.Ltmp11:
	callq	MyFree
.Ltmp12:
# BB#3:                                 # %_ZN9NCompress2NZ8CDecoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB5_4:
.Ltmp13:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZThn8_N9NCompress2NZ8CDecoderD0Ev, .Lfunc_end5-_ZThn8_N9NCompress2NZ8CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp12-.Ltmp7          #   Call between .Ltmp7 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end5-.Ltmp12     #   Call between .Ltmp12 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN9NCompress2NZ8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress2NZ8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN9NCompress2NZ8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN9NCompress2NZ8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi22:
	.cfi_def_cfa_offset 336
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%r9, %r13
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r14
	leaq	88(%rsp), %rbp
	movq	%rbp, %rdi
	callq	_ZN9CInBufferC1Ev
	movq	$0, 168(%rsp)
	movl	$0, 176(%rsp)
	movq	$0, 192(%rsp)
	movq	$0, 208(%rsp)
.Ltmp14:
	movl	$1048576, %esi          # imm = 0x100000
	movq	%rbp, %rdi
	callq	_ZN9CInBuffer6CreateEj
.Ltmp15:
# BB#1:
	testb	%al, %al
	je	.LBB6_15
# BB#2:
.Ltmp16:
	leaq	88(%rsp), %rdi
	movq	%r12, %rsi
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
.Ltmp17:
# BB#3:
.Ltmp18:
	leaq	88(%rsp), %rdi
	callq	_ZN9CInBuffer4InitEv
.Ltmp19:
# BB#4:
.Ltmp20:
	leaq	168(%rsp), %rdi
	movl	$1048576, %esi          # imm = 0x100000
	callq	_ZN10COutBuffer6CreateEj
.Ltmp21:
# BB#5:
	testb	%al, %al
	je	.LBB6_15
# BB#6:
.Ltmp22:
	leaq	168(%rsp), %rdi
	movq	%r15, %rsi
	callq	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
.Ltmp23:
# BB#7:
.Ltmp24:
	leaq	168(%rsp), %rdi
	callq	_ZN10COutBuffer4InitEv
.Ltmp25:
# BB#8:
	movzbl	48(%r14), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movl	%eax, %ebx
	andl	$31, %ebx
	leal	-9(%rbx), %eax
	movl	$1, %r12d
	cmpl	$7, %eax
	ja	.LBB6_16
# BB#9:
	movl	$1, %r12d
	movl	%ebx, %ecx
	shll	%cl, %r12d
	cmpl	52(%r14), %ebx
	movl	%r12d, 24(%rsp)         # 4-byte Spill
	jne	.LBB6_23
# BB#10:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB6_24
# BB#11:
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.LBB6_24
# BB#12:
	cmpq	$0, 40(%r14)
	je	.LBB6_24
# BB#13:
	movq	%rbx, 136(%rsp)         # 8-byte Spill
	movq	%r13, 48(%rsp)          # 8-byte Spill
	leaq	32(%r14), %rcx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	leaq	40(%r14), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	jmp	.LBB6_34
.LBB6_23:                               # %._crit_edge225
	movq	24(%r14), %rdi
.LBB6_24:
.Ltmp27:
	callq	MyFree
.Ltmp28:
# BB#25:                                # %.noexc
	movq	$0, 24(%r14)
	movq	32(%r14), %rdi
.Ltmp29:
	callq	MyFree
.Ltmp30:
# BB#26:                                # %.noexc146
	movq	$0, 32(%r14)
	movq	40(%r14), %rdi
.Ltmp31:
	callq	MyFree
.Ltmp32:
# BB#27:
	leaq	40(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	$0, (%rax)
	movl	%r12d, %ebp
	leaq	(%rbp,%rbp), %rdi
.Ltmp33:
	callq	MyAlloc
.Ltmp34:
# BB#28:
	movq	%rax, 24(%r14)
	testq	%rax, %rax
	je	.LBB6_15
# BB#29:
.Ltmp35:
	movq	%rbp, %rdi
	callq	MyAlloc
.Ltmp36:
# BB#30:
	leaq	32(%r14), %rcx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movq	%rax, (%rcx)
	testq	%rax, %rax
	je	.LBB6_15
# BB#31:
.Ltmp37:
	movq	%rbp, %rdi
	callq	MyAlloc
.Ltmp38:
# BB#32:
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	testq	%rax, %rax
	movl	$-2147024882, %r12d     # imm = 0x8007000E
	je	.LBB6_16
# BB#33:
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%rbx, 136(%rsp)         # 8-byte Spill
	movl	%ebx, 52(%r14)
	movq	24(%r14), %rdi
	movq	32(%r14), %rax
	movl	24(%rsp), %r12d         # 4-byte Reload
.LBB6_34:
	xorl	%ebx, %ebx
	cmpb	$0, 28(%rsp)            # 1-byte Folded Reload
	sets	%bl
	orl	$256, %ebx              # imm = 0x100
	movw	$0, 512(%rdi)
	movb	$0, 256(%rax)
	movl	$9, %ecx
	xorl	%eax, %eax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	xorl	%edi, %edi
	jmp	.LBB6_38
.LBB6_15:
	movl	$-2147024882, %r12d     # imm = 0x8007000E
.LBB6_16:
.Ltmp63:
	leaq	168(%rsp), %rdi
	callq	_ZN10COutBuffer4FreeEv
.Ltmp64:
# BB#17:
	movq	192(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_19
# BB#18:
	movq	(%rdi), %rax
.Ltmp69:
	callq	*16(%rax)
.Ltmp70:
.LBB6_19:                               # %_ZN10COutBufferD2Ev.exit
.Ltmp81:
	leaq	88(%rsp), %rdi
	callq	_ZN9CInBuffer4FreeEv
.Ltmp82:
# BB#20:
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_22
# BB#21:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB6_22:                               # %_ZN9CInBufferD2Ev.exit
	movl	%r12d, %eax
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_37:                               #   in Loop: Header=BB6_38 Depth=1
	xorl	%edi, %edi
	movl	$257, %ebx              # imm = 0x101
	movl	$9, %ecx
.LBB6_38:                               # %.thread162.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_40 Depth 2
                                        #       Child Loop BB6_44 Depth 3
                                        #         Child Loop BB6_42 Depth 4
                                        #         Child Loop BB6_62 Depth 4
                                        #         Child Loop BB6_65 Depth 4
                                        #         Child Loop BB6_51 Depth 4
                                        #         Child Loop BB6_82 Depth 4
                                        #         Child Loop BB6_87 Depth 4
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.LBB6_40
.LBB6_39:                               #   in Loop: Header=BB6_40 Depth=2
	movq	24(%r14), %rax
	movl	%ebx, %ecx
	incl	%ebx
	movw	%bp, (%rax,%rcx,2)
	cmpl	%ebx, 232(%rsp)         # 4-byte Folded Reload
	setb	%al
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmpl	136(%rsp), %ecx         # 4-byte Folded Reload
	setl	%dl
	andb	%al, %dl
	movzbl	%dl, %eax
	movl	$0, %edx
	cmovnel	%edx, %r15d
	cmovnel	%edx, %r13d
	addl	%eax, %ecx
	movb	$1, %dil
.LBB6_40:                               # %.thread162.outer
                                        #   Parent Loop BB6_38 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_44 Depth 3
                                        #         Child Loop BB6_42 Depth 4
                                        #         Child Loop BB6_62 Depth 4
                                        #         Child Loop BB6_65 Depth 4
                                        #         Child Loop BB6_51 Depth 4
                                        #         Child Loop BB6_82 Depth 4
                                        #         Child Loop BB6_87 Depth 4
	movl	%ecx, %edx
	movl	$1, %eax
	shll	%cl, %eax
	movq	%rax, 232(%rsp)         # 8-byte Spill
	leal	-1(%rax), %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	leal	-1(%rbx), %eax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	leaq	64(%rsp,%rdx), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	leaq	-1(%rdx), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	%ecx, %eax
	andl	$31, %eax
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	movq	%rax, 224(%rsp)         # 8-byte Spill
	subq	%rax, %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	jmp	.LBB6_44
.LBB6_41:                               # %vector.body.preheader
                                        #   in Loop: Header=BB6_44 Depth=3
	leaq	16(%rax), %rcx
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	80(%rsp), %rsi
	.p2align	4, 0x90
.LBB6_42:                               # %vector.body
                                        #   Parent Loop BB6_38 Depth=1
                                        #     Parent Loop BB6_40 Depth=2
                                        #       Parent Loop BB6_44 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movaps	%xmm0, -16(%rsi)
	movaps	%xmm1, (%rsi)
	addq	$32, %rcx
	addq	$32, %rsi
	addq	$-32, %rdx
	jne	.LBB6_42
# BB#43:                                # %middle.block
                                        #   in Loop: Header=BB6_44 Depth=3
	cmpl	$0, 224(%rsp)           # 4-byte Folded Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rbp, %rdx
	jne	.LBB6_60
	jmp	.LBB6_68
	.p2align	4, 0x90
.LBB6_44:                               # %.thread162
                                        #   Parent Loop BB6_38 Depth=1
                                        #     Parent Loop BB6_40 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB6_42 Depth 4
                                        #         Child Loop BB6_62 Depth 4
                                        #         Child Loop BB6_65 Depth 4
                                        #         Child Loop BB6_51 Depth 4
                                        #         Child Loop BB6_82 Depth 4
                                        #         Child Loop BB6_87 Depth 4
	cmpl	%r15d, %r13d
	jne	.LBB6_76
# BB#45:                                #   in Loop: Header=BB6_44 Depth=3
	movq	88(%rsp), %rax
	movq	96(%rsp), %rcx
	movl	%ecx, %edx
	subl	%eax, %edx
	movl	12(%rsp), %r15d         # 4-byte Reload
	cmpl	%r15d, %edx
	leaq	88(%rsp), %rbp
	jae	.LBB6_54
# BB#46:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB6_44 Depth=3
	movl	%edi, %r12d
	xorl	%r13d, %r13d
	movq	144(%rsp), %rbx         # 8-byte Reload
	cmpq	%rcx, %rax
	jb	.LBB6_51
	.p2align	4, 0x90
.LBB6_48:                               #   in Loop: Header=BB6_44 Depth=3
.Ltmp40:
	movq	%rbp, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp41:
# BB#49:                                # %.noexc148
                                        #   in Loop: Header=BB6_44 Depth=3
	testb	%al, %al
	je	.LBB6_53
# BB#50:                                # %._crit_edge36.i
                                        #   in Loop: Header=BB6_44 Depth=3
	movq	88(%rsp), %rax
	jmp	.LBB6_51
	.p2align	4, 0x90
.LBB6_47:                               # %..lr.ph_crit_edge.i
                                        #   in Loop: Header=BB6_51 Depth=4
	movq	96(%rsp), %rcx
	cmpq	%rcx, %rax
	jae	.LBB6_48
.LBB6_51:                               #   Parent Loop BB6_38 Depth=1
                                        #     Parent Loop BB6_40 Depth=2
                                        #       Parent Loop BB6_44 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rax, %rcx
	leaq	1(%rcx), %rax
	movq	%rax, 88(%rsp)
	movzbl	(%rcx), %ecx
	movb	%cl, 64(%rsp,%r13)
	incq	%r13
	cmpq	%rbx, %r13
	jb	.LBB6_47
# BB#52:                                #   in Loop: Header=BB6_44 Depth=3
	movl	%r15d, %r13d
.LBB6_53:                               # %.noexc148._ZN9CInBuffer9ReadBytesEPhj.exit.loopexit_crit_edge
                                        #   in Loop: Header=BB6_44 Depth=3
	movl	%r12d, %ebp
	jmp	.LBB6_69
.LBB6_54:                               # %.preheader24.i
                                        #   in Loop: Header=BB6_44 Depth=3
	testl	%r15d, %r15d
	movq	144(%rsp), %rbp         # 8-byte Reload
	je	.LBB6_67
# BB#55:                                # %.lr.ph29.i.preheader
                                        #   in Loop: Header=BB6_44 Depth=3
	cmpl	$31, %r15d
	jbe	.LBB6_59
# BB#56:                                # %min.iters.checked
                                        #   in Loop: Header=BB6_44 Depth=3
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	je	.LBB6_59
# BB#57:                                # %vector.memcheck
                                        #   in Loop: Header=BB6_44 Depth=3
	leaq	(%rax,%rbp), %rcx
	leaq	64(%rsp), %rdx
	cmpq	%rcx, %rdx
	jae	.LBB6_41
# BB#58:                                # %vector.memcheck
                                        #   in Loop: Header=BB6_44 Depth=3
	cmpq	240(%rsp), %rax         # 8-byte Folded Reload
	jae	.LBB6_41
.LBB6_59:                               #   in Loop: Header=BB6_44 Depth=3
	xorl	%ecx, %ecx
.LBB6_60:                               # %.lr.ph29.i.preheader277
                                        #   in Loop: Header=BB6_44 Depth=3
	movl	%edi, %r8d
	movl	%ebp, %esi
	subl	%ecx, %esi
	movq	248(%rsp), %rdx         # 8-byte Reload
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB6_63
# BB#61:                                # %.lr.ph29.i.prol.preheader
                                        #   in Loop: Header=BB6_44 Depth=3
	negq	%rsi
	.p2align	4, 0x90
.LBB6_62:                               # %.lr.ph29.i.prol
                                        #   Parent Loop BB6_38 Depth=1
                                        #     Parent Loop BB6_40 Depth=2
                                        #       Parent Loop BB6_44 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rax,%rcx), %ebx
	movb	%bl, 64(%rsp,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB6_62
.LBB6_63:                               # %.lr.ph29.i.prol.loopexit
                                        #   in Loop: Header=BB6_44 Depth=3
	cmpq	$7, %rdx
	movq	%rbp, %rdx
	movl	%r8d, %edi
	jb	.LBB6_68
# BB#64:                                # %.lr.ph29.i.preheader277.new
                                        #   in Loop: Header=BB6_44 Depth=3
	movq	%rbp, %rdx
	subq	%rcx, %rdx
	leaq	71(%rsp), %rsi
	leaq	(%rsi,%rcx), %rsi
	leaq	7(%rax,%rcx), %rcx
	.p2align	4, 0x90
.LBB6_65:                               # %.lr.ph29.i
                                        #   Parent Loop BB6_38 Depth=1
                                        #     Parent Loop BB6_40 Depth=2
                                        #       Parent Loop BB6_44 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rsi)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rsi)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rsi)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rsi)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rsi)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rsi)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rsi)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rsi)
	addq	$8, %rsi
	addq	$8, %rcx
	addq	$-8, %rdx
	jne	.LBB6_65
# BB#66:                                #   in Loop: Header=BB6_44 Depth=3
	movq	%rbp, %rdx
	movl	%r8d, %edi
	jmp	.LBB6_68
.LBB6_67:                               #   in Loop: Header=BB6_44 Depth=3
	xorl	%edx, %edx
.LBB6_68:                               # %._crit_edge30.i
                                        #   in Loop: Header=BB6_44 Depth=3
	movl	%edi, %ebp
	addq	%rdx, %rax
	movq	%rax, 88(%rsp)
	movl	%r15d, %r13d
.LBB6_69:                               # %_ZN9CInBuffer9ReadBytesEPhj.exit
                                        #   in Loop: Header=BB6_44 Depth=3
.Ltmp43:
	leaq	168(%rsp), %rdi
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
	movq	%rax, %r15
.Ltmp44:
# BB#70:                                #   in Loop: Header=BB6_44 Depth=3
	movq	%r15, 272(%rsp)
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB6_74
# BB#71:                                #   in Loop: Header=BB6_44 Depth=3
	movq	%r15, %rax
	subq	152(%rsp), %rax         # 8-byte Folded Reload
	cmpq	$262144, %rax           # imm = 0x40000
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	%ebp, %edi
	jb	.LBB6_75
# BB#72:                                #   in Loop: Header=BB6_44 Depth=3
	movq	88(%rsp), %rax
	addq	120(%rsp), %rax
	subq	104(%rsp), %rax
	movq	%rax, 264(%rsp)
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp46:
	leaq	264(%rsp), %rsi
	leaq	272(%rsp), %rdx
	callq	*40(%rax)
	movl	%eax, %r12d
.Ltmp47:
# BB#73:                                #   in Loop: Header=BB6_44 Depth=3
	testl	%r12d, %r12d
	movq	%r15, 152(%rsp)         # 8-byte Spill
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	%ebp, %edi
	je	.LBB6_75
	jmp	.LBB6_16
.LBB6_74:                               #   in Loop: Header=BB6_44 Depth=3
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	%ebp, %edi
.LBB6_75:                               # %.thread
                                        #   in Loop: Header=BB6_44 Depth=3
	shll	$3, %r13d
	xorl	%r15d, %r15d
	movl	24(%rsp), %r12d         # 4-byte Reload
.LBB6_76:                               #   in Loop: Header=BB6_44 Depth=3
	movl	%r15d, %eax
	shrl	$3, %eax
	movzbl	64(%rsp,%rax), %ecx
	movzbl	65(%rsp,%rax), %edx
	shll	$8, %edx
	orl	%ecx, %edx
	movzbl	66(%rsp,%rax), %ebp
	shll	$16, %ebp
	orl	%edx, %ebp
	movl	%r15d, %ecx
	andb	$7, %cl
	shrl	%cl, %ebp
	addl	12(%rsp), %r15d         # 4-byte Folded Reload
	cmpl	%r13d, %r15d
	ja	.LBB6_91
# BB#77:                                #   in Loop: Header=BB6_44 Depth=3
	andl	60(%rsp), %ebp          # 4-byte Folded Reload
	cmpl	%ebx, %ebp
	jae	.LBB6_92
# BB#78:                                #   in Loop: Header=BB6_44 Depth=3
	cmpb	$0, 28(%rsp)            # 1-byte Folded Reload
	jns	.LBB6_80
# BB#79:                                #   in Loop: Header=BB6_44 Depth=3
	cmpl	$256, %ebp              # imm = 0x100
	je	.LBB6_37
.LBB6_80:                               # %.preheader164
                                        #   in Loop: Header=BB6_44 Depth=3
	xorl	%eax, %eax
	cmpl	$256, %ebp              # imm = 0x100
	movl	%ebp, %ecx
	jb	.LBB6_83
# BB#81:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB6_44 Depth=3
	xorl	%eax, %eax
	movl	%ebp, %ecx
	.p2align	4, 0x90
.LBB6_82:                               # %.lr.ph
                                        #   Parent Loop BB6_38 Depth=1
                                        #     Parent Loop BB6_40 Depth=2
                                        #       Parent Loop BB6_44 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%ecx, %ecx
	movq	32(%r14), %rdx
	movq	40(%r14), %rsi
	movzbl	(%rdx,%rcx), %edx
	movb	%dl, (%rsi,%rax)
	incq	%rax
	movq	24(%r14), %rdx
	movzwl	(%rdx,%rcx,2), %ecx
	cmpl	$255, %ecx
	ja	.LBB6_82
.LBB6_83:                               # %._crit_edge
                                        #   in Loop: Header=BB6_44 Depth=3
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rdx
	movslq	%eax, %rsi
	incl	%eax
	movb	%cl, (%rdx,%rsi)
	testb	%dil, %dil
	je	.LBB6_86
# BB#84:                                #   in Loop: Header=BB6_44 Depth=3
	movq	160(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx), %rdx
	movq	256(%rsp), %rsi         # 8-byte Reload
	movb	%cl, (%rdx,%rsi)
	cmpl	%esi, %ebp
	jne	.LBB6_86
# BB#85:                                #   in Loop: Header=BB6_44 Depth=3
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rdx
	movb	%cl, (%rdx)
.LBB6_86:                               # %.preheader.preheader
                                        #   in Loop: Header=BB6_44 Depth=3
	movslq	%eax, %rbx
	incq	%rbx
	.p2align	4, 0x90
.LBB6_87:                               # %.preheader
                                        #   Parent Loop BB6_38 Depth=1
                                        #     Parent Loop BB6_40 Depth=2
                                        #       Parent Loop BB6_44 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movzbl	-2(%rax,%rbx), %eax
	movq	168(%rsp), %rcx
	movl	176(%rsp), %edx
	leal	1(%rdx), %esi
	movl	%esi, 176(%rsp)
	movb	%al, (%rcx,%rdx)
	movl	176(%rsp), %eax
	cmpl	180(%rsp), %eax
	jne	.LBB6_89
# BB#88:                                #   in Loop: Header=BB6_87 Depth=4
.Ltmp49:
	leaq	168(%rsp), %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp50:
.LBB6_89:                               # %_ZN10COutBuffer9WriteByteEh.exit
                                        #   in Loop: Header=BB6_87 Depth=4
	decq	%rbx
	cmpq	$1, %rbx
	jg	.LBB6_87
# BB#90:                                #   in Loop: Header=BB6_44 Depth=3
	xorl	%edi, %edi
	movq	32(%rsp), %rbx          # 8-byte Reload
	cmpl	%r12d, %ebx
	jae	.LBB6_44
	jmp	.LBB6_39
.LBB6_91:
.Ltmp52:
	leaq	168(%rsp), %rdi
	callq	_ZN10COutBuffer5FlushEv
	movl	%eax, %r12d
.Ltmp53:
	jmp	.LBB6_16
.LBB6_92:
	movl	$1, %r12d
	jmp	.LBB6_16
.LBB6_93:                               # %.loopexit.split-lp.loopexit.split-lp
.Ltmp54:
	jmp	.LBB6_107
.LBB6_94:
.Ltmp48:
	jmp	.LBB6_107
.LBB6_95:                               # %.loopexit.split-lp.loopexit
.Ltmp45:
	jmp	.LBB6_107
.LBB6_96:
.Ltmp39:
	jmp	.LBB6_107
.LBB6_97:
.Ltmp71:
	movq	%rax, %rbx
	jmp	.LBB6_110
.LBB6_98:
.Ltmp83:
	movq	%rax, %rbx
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_113
# BB#99:
	movq	(%rdi), %rax
.Ltmp84:
	callq	*16(%rax)
.Ltmp85:
	jmp	.LBB6_113
.LBB6_100:
.Ltmp86:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_101:
.Ltmp65:
	movq	%rax, %rbx
	movq	192(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_110
# BB#102:
	movq	(%rdi), %rax
.Ltmp66:
	callq	*16(%rax)
.Ltmp67:
	jmp	.LBB6_110
.LBB6_103:
.Ltmp68:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_104:                              # %.loopexit
.Ltmp42:
	jmp	.LBB6_107
.LBB6_105:
.Ltmp26:
	jmp	.LBB6_107
.LBB6_106:
.Ltmp51:
.LBB6_107:
	movq	%rax, %rbx
.Ltmp55:
	leaq	168(%rsp), %rdi
	callq	_ZN10COutBuffer4FreeEv
.Ltmp56:
# BB#108:
	movq	192(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_110
# BB#109:
	movq	(%rdi), %rax
.Ltmp61:
	callq	*16(%rax)
.Ltmp62:
.LBB6_110:
.Ltmp72:
	leaq	88(%rsp), %rdi
	callq	_ZN9CInBuffer4FreeEv
.Ltmp73:
# BB#111:
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_113
# BB#112:
	movq	(%rdi), %rax
.Ltmp78:
	callq	*16(%rax)
.Ltmp79:
.LBB6_113:                              # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB6_114:
.Ltmp57:
	movq	%rax, %rbx
	movq	192(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_120
# BB#115:
	movq	(%rdi), %rax
.Ltmp58:
	callq	*16(%rax)
.Ltmp59:
	jmp	.LBB6_120
.LBB6_116:
.Ltmp60:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_117:
.Ltmp80:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB6_118:
.Ltmp74:
	movq	%rax, %rbx
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_120
# BB#119:
	movq	(%rdi), %rax
.Ltmp75:
	callq	*16(%rax)
.Ltmp76:
.LBB6_120:                              # %.body153
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB6_121:
.Ltmp77:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN9NCompress2NZ8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end6-_ZN9NCompress2NZ8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\232\202\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\221\002"              # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp14-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp25-.Ltmp14         #   Call between .Ltmp14 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin2   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp38-.Ltmp27         #   Call between .Ltmp27 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin2   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin2   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp70-.Ltmp69         #   Call between .Ltmp69 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin2   #     jumps to .Ltmp71
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp82-.Ltmp81         #   Call between .Ltmp81 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin2   #     jumps to .Ltmp83
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp40-.Ltmp82         #   Call between .Ltmp82 and .Ltmp40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin2   #     jumps to .Ltmp42
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin2   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin2   # >> Call Site 10 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin2   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin2   # >> Call Site 11 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin2   #     jumps to .Ltmp51
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin2   # >> Call Site 12 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin2   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp84-.Lfunc_begin2   # >> Call Site 13 <<
	.long	.Ltmp85-.Ltmp84         #   Call between .Ltmp84 and .Ltmp85
	.long	.Ltmp86-.Lfunc_begin2   #     jumps to .Ltmp86
	.byte	1                       #   On action: 1
	.long	.Ltmp66-.Lfunc_begin2   # >> Call Site 14 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin2   #     jumps to .Ltmp68
	.byte	1                       #   On action: 1
	.long	.Ltmp55-.Lfunc_begin2   # >> Call Site 15 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin2   #     jumps to .Ltmp57
	.byte	1                       #   On action: 1
	.long	.Ltmp61-.Lfunc_begin2   # >> Call Site 16 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp80-.Lfunc_begin2   #     jumps to .Ltmp80
	.byte	1                       #   On action: 1
	.long	.Ltmp72-.Lfunc_begin2   # >> Call Site 17 <<
	.long	.Ltmp73-.Ltmp72         #   Call between .Ltmp72 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin2   #     jumps to .Ltmp74
	.byte	1                       #   On action: 1
	.long	.Ltmp78-.Lfunc_begin2   # >> Call Site 18 <<
	.long	.Ltmp79-.Ltmp78         #   Call between .Ltmp78 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin2   #     jumps to .Ltmp80
	.byte	1                       #   On action: 1
	.long	.Ltmp79-.Lfunc_begin2   # >> Call Site 19 <<
	.long	.Ltmp58-.Ltmp79         #   Call between .Ltmp79 and .Ltmp58
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin2   # >> Call Site 20 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin2   #     jumps to .Ltmp60
	.byte	1                       #   On action: 1
	.long	.Ltmp75-.Lfunc_begin2   # >> Call Site 21 <<
	.long	.Ltmp76-.Ltmp75         #   Call between .Ltmp75 and .Ltmp76
	.long	.Ltmp77-.Lfunc_begin2   #     jumps to .Ltmp77
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress2NZ8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress2NZ8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN9NCompress2NZ8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN9NCompress2NZ8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 16
.Lcfi30:
	.cfi_offset %rbx, -16
.Ltmp87:
	callq	_ZN9NCompress2NZ8CDecoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	movl	%eax, %ebx
.Ltmp88:
.LBB7_6:
	movl	%ebx, %eax
	popq	%rbx
	retq
.LBB7_1:
.Ltmp89:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	cmpl	$3, %ebx
	jne	.LBB7_3
# BB#2:
	callq	__cxa_begin_catch
	jmp	.LBB7_4
.LBB7_3:
	callq	__cxa_begin_catch
	cmpl	$2, %ebx
	jne	.LBB7_5
.LBB7_4:
	movl	(%rax), %ebx
	callq	__cxa_end_catch
	jmp	.LBB7_6
.LBB7_5:
	callq	__cxa_end_catch
	movl	$1, %ebx
	jmp	.LBB7_6
.Lfunc_end7:
	.size	_ZN9NCompress2NZ8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end7-_ZN9NCompress2NZ8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\256\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp87-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp88-.Ltmp87         #   Call between .Ltmp87 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin3   #     jumps to .Ltmp89
	.byte	5                       #   On action: 3
	.long	.Ltmp88-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Lfunc_end7-.Ltmp88     #   Call between .Ltmp88 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
	.byte	3                       # >> Action Record 3 <<
                                        #   Catch TypeInfo 3
	.byte	125                     #   Continue to action 2
                                        # >> Catch TypeInfos <<
	.long	_ZTI18CInBufferException # TypeInfo 3
	.long	_ZTI19COutBufferException # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress2NZ8CDecoder21SetDecoderProperties2EPKhj
	.p2align	4, 0x90
	.type	_ZN9NCompress2NZ8CDecoder21SetDecoderProperties2EPKhj,@function
_ZN9NCompress2NZ8CDecoder21SetDecoderProperties2EPKhj: # @_ZN9NCompress2NZ8CDecoder21SetDecoderProperties2EPKhj
	.cfi_startproc
# BB#0:
	testl	%edx, %edx
	je	.LBB8_1
# BB#2:
	movb	(%rsi), %al
	movb	%al, 48(%rdi)
	xorl	%eax, %eax
	retq
.LBB8_1:
	movl	$-2147024809, %eax      # imm = 0x80070057
	retq
.Lfunc_end8:
	.size	_ZN9NCompress2NZ8CDecoder21SetDecoderProperties2EPKhj, .Lfunc_end8-_ZN9NCompress2NZ8CDecoder21SetDecoderProperties2EPKhj
	.cfi_endproc

	.globl	_ZThn8_N9NCompress2NZ8CDecoder21SetDecoderProperties2EPKhj
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress2NZ8CDecoder21SetDecoderProperties2EPKhj,@function
_ZThn8_N9NCompress2NZ8CDecoder21SetDecoderProperties2EPKhj: # @_ZThn8_N9NCompress2NZ8CDecoder21SetDecoderProperties2EPKhj
	.cfi_startproc
# BB#0:
	testl	%edx, %edx
	je	.LBB9_1
# BB#2:
	movb	(%rsi), %al
	movb	%al, 40(%rdi)
	xorl	%eax, %eax
	retq
.LBB9_1:
	movl	$-2147024809, %eax      # imm = 0x80070057
	retq
.Lfunc_end9:
	.size	_ZThn8_N9NCompress2NZ8CDecoder21SetDecoderProperties2EPKhj, .Lfunc_end9-_ZThn8_N9NCompress2NZ8CDecoder21SetDecoderProperties2EPKhj
	.cfi_endproc

	.section	.text._ZN9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB10_3
# BB#1:
	movl	$IID_ICompressSetDecoderProperties2, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB10_2
.LBB10_3:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB10_4:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB10_2:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB10_4
.Lfunc_end10:
	.size	_ZN9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end10-_ZN9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress2NZ8CDecoder6AddRefEv,"axG",@progbits,_ZN9NCompress2NZ8CDecoder6AddRefEv,comdat
	.weak	_ZN9NCompress2NZ8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress2NZ8CDecoder6AddRefEv,@function
_ZN9NCompress2NZ8CDecoder6AddRefEv:     # @_ZN9NCompress2NZ8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end11:
	.size	_ZN9NCompress2NZ8CDecoder6AddRefEv, .Lfunc_end11-_ZN9NCompress2NZ8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress2NZ8CDecoder7ReleaseEv,"axG",@progbits,_ZN9NCompress2NZ8CDecoder7ReleaseEv,comdat
	.weak	_ZN9NCompress2NZ8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress2NZ8CDecoder7ReleaseEv,@function
_ZN9NCompress2NZ8CDecoder7ReleaseEv:    # @_ZN9NCompress2NZ8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB12_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB12_2:
	popq	%rcx
	retq
.Lfunc_end12:
	.size	_ZN9NCompress2NZ8CDecoder7ReleaseEv, .Lfunc_end12-_ZN9NCompress2NZ8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 16
	addq	$-8, %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB13_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB13_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB13_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB13_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB13_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB13_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB13_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB13_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB13_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB13_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB13_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB13_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB13_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB13_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB13_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB13_32
.LBB13_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressSetDecoderProperties2(%rip), %cl
	jne	.LBB13_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+1(%rip), %cl
	jne	.LBB13_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+2(%rip), %cl
	jne	.LBB13_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+3(%rip), %cl
	jne	.LBB13_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+4(%rip), %cl
	jne	.LBB13_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+5(%rip), %cl
	jne	.LBB13_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+6(%rip), %cl
	jne	.LBB13_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+7(%rip), %cl
	jne	.LBB13_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+8(%rip), %cl
	jne	.LBB13_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+9(%rip), %cl
	jne	.LBB13_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+10(%rip), %cl
	jne	.LBB13_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+11(%rip), %cl
	jne	.LBB13_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+12(%rip), %cl
	jne	.LBB13_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+13(%rip), %cl
	jne	.LBB13_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+14(%rip), %cl
	jne	.LBB13_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+15(%rip), %cl
	jne	.LBB13_33
.LBB13_32:
	leaq	8(%rdi), %rax
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB13_33:                              # %_ZN9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end13:
	.size	_ZThn8_N9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end13-_ZThn8_N9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress2NZ8CDecoder6AddRefEv,"axG",@progbits,_ZThn8_N9NCompress2NZ8CDecoder6AddRefEv,comdat
	.weak	_ZThn8_N9NCompress2NZ8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress2NZ8CDecoder6AddRefEv,@function
_ZThn8_N9NCompress2NZ8CDecoder6AddRefEv: # @_ZThn8_N9NCompress2NZ8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end14:
	.size	_ZThn8_N9NCompress2NZ8CDecoder6AddRefEv, .Lfunc_end14-_ZThn8_N9NCompress2NZ8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress2NZ8CDecoder7ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress2NZ8CDecoder7ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress2NZ8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress2NZ8CDecoder7ReleaseEv,@function
_ZThn8_N9NCompress2NZ8CDecoder7ReleaseEv: # @_ZThn8_N9NCompress2NZ8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB15_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB15_2:                               # %_ZN9NCompress2NZ8CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end15:
	.size	_ZThn8_N9NCompress2NZ8CDecoder7ReleaseEv, .Lfunc_end15-_ZThn8_N9NCompress2NZ8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB16_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB16_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB16_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB16_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB16_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB16_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB16_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB16_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB16_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB16_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB16_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB16_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB16_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB16_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB16_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB16_16:
	xorl	%eax, %eax
	retq
.Lfunc_end16:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end16-_ZeqRK4GUIDS1_
	.cfi_endproc

	.type	_ZTVN9NCompress2NZ8CDecoderE,@object # @_ZTVN9NCompress2NZ8CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress2NZ8CDecoderE
	.p2align	3
_ZTVN9NCompress2NZ8CDecoderE:
	.quad	0
	.quad	_ZTIN9NCompress2NZ8CDecoderE
	.quad	_ZN9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress2NZ8CDecoder6AddRefEv
	.quad	_ZN9NCompress2NZ8CDecoder7ReleaseEv
	.quad	_ZN9NCompress2NZ8CDecoderD2Ev
	.quad	_ZN9NCompress2NZ8CDecoderD0Ev
	.quad	_ZN9NCompress2NZ8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.quad	_ZN9NCompress2NZ8CDecoder21SetDecoderProperties2EPKhj
	.quad	-8
	.quad	_ZTIN9NCompress2NZ8CDecoderE
	.quad	_ZThn8_N9NCompress2NZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress2NZ8CDecoder6AddRefEv
	.quad	_ZThn8_N9NCompress2NZ8CDecoder7ReleaseEv
	.quad	_ZThn8_N9NCompress2NZ8CDecoderD1Ev
	.quad	_ZThn8_N9NCompress2NZ8CDecoderD0Ev
	.quad	_ZThn8_N9NCompress2NZ8CDecoder21SetDecoderProperties2EPKhj
	.size	_ZTVN9NCompress2NZ8CDecoderE, 136

	.type	_ZTS18CInBufferException,@object # @_ZTS18CInBufferException
	.section	.rodata._ZTS18CInBufferException,"aG",@progbits,_ZTS18CInBufferException,comdat
	.weak	_ZTS18CInBufferException
	.p2align	4
_ZTS18CInBufferException:
	.asciz	"18CInBufferException"
	.size	_ZTS18CInBufferException, 21

	.type	_ZTS16CSystemException,@object # @_ZTS16CSystemException
	.section	.rodata._ZTS16CSystemException,"aG",@progbits,_ZTS16CSystemException,comdat
	.weak	_ZTS16CSystemException
	.p2align	4
_ZTS16CSystemException:
	.asciz	"16CSystemException"
	.size	_ZTS16CSystemException, 19

	.type	_ZTI16CSystemException,@object # @_ZTI16CSystemException
	.section	.rodata._ZTI16CSystemException,"aG",@progbits,_ZTI16CSystemException,comdat
	.weak	_ZTI16CSystemException
	.p2align	3
_ZTI16CSystemException:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16CSystemException
	.size	_ZTI16CSystemException, 16

	.type	_ZTI18CInBufferException,@object # @_ZTI18CInBufferException
	.section	.rodata._ZTI18CInBufferException,"aG",@progbits,_ZTI18CInBufferException,comdat
	.weak	_ZTI18CInBufferException
	.p2align	4
_ZTI18CInBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18CInBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI18CInBufferException, 24

	.type	_ZTS19COutBufferException,@object # @_ZTS19COutBufferException
	.section	.rodata._ZTS19COutBufferException,"aG",@progbits,_ZTS19COutBufferException,comdat
	.weak	_ZTS19COutBufferException
	.p2align	4
_ZTS19COutBufferException:
	.asciz	"19COutBufferException"
	.size	_ZTS19COutBufferException, 22

	.type	_ZTI19COutBufferException,@object # @_ZTI19COutBufferException
	.section	.rodata._ZTI19COutBufferException,"aG",@progbits,_ZTI19COutBufferException,comdat
	.weak	_ZTI19COutBufferException
	.p2align	4
_ZTI19COutBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19COutBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI19COutBufferException, 24

	.type	_ZTSN9NCompress2NZ8CDecoderE,@object # @_ZTSN9NCompress2NZ8CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTSN9NCompress2NZ8CDecoderE
	.p2align	4
_ZTSN9NCompress2NZ8CDecoderE:
	.asciz	"N9NCompress2NZ8CDecoderE"
	.size	_ZTSN9NCompress2NZ8CDecoderE, 25

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTS30ICompressSetDecoderProperties2,@object # @_ZTS30ICompressSetDecoderProperties2
	.section	.rodata._ZTS30ICompressSetDecoderProperties2,"aG",@progbits,_ZTS30ICompressSetDecoderProperties2,comdat
	.weak	_ZTS30ICompressSetDecoderProperties2
	.p2align	4
_ZTS30ICompressSetDecoderProperties2:
	.asciz	"30ICompressSetDecoderProperties2"
	.size	_ZTS30ICompressSetDecoderProperties2, 33

	.type	_ZTI30ICompressSetDecoderProperties2,@object # @_ZTI30ICompressSetDecoderProperties2
	.section	.rodata._ZTI30ICompressSetDecoderProperties2,"aG",@progbits,_ZTI30ICompressSetDecoderProperties2,comdat
	.weak	_ZTI30ICompressSetDecoderProperties2
	.p2align	4
_ZTI30ICompressSetDecoderProperties2:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS30ICompressSetDecoderProperties2
	.quad	_ZTI8IUnknown
	.size	_ZTI30ICompressSetDecoderProperties2, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN9NCompress2NZ8CDecoderE,@object # @_ZTIN9NCompress2NZ8CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress2NZ8CDecoderE
	.p2align	4
_ZTIN9NCompress2NZ8CDecoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress2NZ8CDecoderE
	.long	1                       # 0x1
	.long	3                       # 0x3
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI30ICompressSetDecoderProperties2
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.size	_ZTIN9NCompress2NZ8CDecoderE, 72


	.globl	_ZN9NCompress2NZ8CDecoderD1Ev
	.type	_ZN9NCompress2NZ8CDecoderD1Ev,@function
_ZN9NCompress2NZ8CDecoderD1Ev = _ZN9NCompress2NZ8CDecoderD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
