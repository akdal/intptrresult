	.text
	.file	"libclamav_ole2_extract.bc"
	.globl	cli_ole2_extract
	.p2align	4, 0x90
	.type	cli_ole2_extract,@function
cli_ole2_extract:                       # @cli_ole2_extract
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$712, %rsp              # imm = 0x2C8
.Lcfi4:
	.cfi_def_cfa_offset 752
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movl	%edi, %ebp
	movl	$0, 12(%rsp)
	xorl	%ebx, %ebx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	$0, 536(%rsp)
	leaq	568(%rsp), %rdx
	movl	$1, %edi
	movl	%ebp, %esi
	callq	__fxstat
	testl	%eax, %eax
	je	.LBB0_2
# BB#1:                                 # %._crit_edge
	movq	536(%rsp), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_8
	jmp	.LBB0_7
.LBB0_2:
	movq	616(%rsp), %rsi
	cmpq	$520, %rsi              # imm = 0x208
	jl	.LBB0_24
# BB#3:
	movq	%rsi, 544(%rsp)
	xorl	%edi, %edi
	movl	$1, %edx
	movl	$2, %ecx
	xorl	%r9d, %r9d
	movl	%ebp, %r8d
	callq	mmap
	movq	%rax, 536(%rsp)
	cmpq	$-1, %rax
	je	.LBB0_4
# BB#5:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	536(%rsp), %rbx
	leaq	16(%rsp), %rdi
	movl	$520, %edx              # imm = 0x208
	movq	%rbx, %rsi
	callq	memcpy
	testq	%rbx, %rbx
	jne	.LBB0_8
	jmp	.LBB0_7
.LBB0_4:                                # %.thread
	movq	$0, 536(%rsp)
.LBB0_7:
	leaq	16(%rsp), %rsi
	movl	$520, %edx              # imm = 0x208
	movl	%ebp, %edi
	callq	cli_readn
	xorl	%ebx, %ebx
	cmpl	$520, %eax              # imm = 0x208
	jne	.LBB0_24
.LBB0_8:
	movl	$-1, 528(%rsp)
	callq	cli_bitset_init
	movq	%rax, 552(%rsp)
	movl	$-107, %ebx
	testq	%rax, %rax
	je	.LBB0_24
# BB#9:
	movq	16(%rsp), %rax
	cmpq	magic_id(%rip), %rax
	je	.LBB0_13
# BB#10:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	536(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_12
# BB#11:
	movq	544(%rsp), %rsi
	callq	munmap
.LBB0_12:
	movq	552(%rsp), %rdi
	callq	cli_bitset_free
	jmp	.LBB0_24
.LBB0_13:
	movzwl	46(%rsp), %eax
	cmpl	$9, %eax
	jne	.LBB0_14
# BB#16:
	cmpl	$6, 48(%rsp)
	jne	.LBB0_17
# BB#18:
	cmpl	$4096, 72(%rsp)         # imm = 0x1000
	jne	.LBB0_19
# BB#20:
	movabsq	$2049638230412172402, %rax # imm = 0x1C71C71C71C71C72
	imulq	616(%rsp)
	movq	%rdx, %rax
	shrq	$63, %rax
	addl	%edx, %eax
	leal	8(,%rax,8), %eax
	movl	%eax, 560(%rsp)
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	16(%rsp), %esi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	17(%rsp), %esi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	18(%rsp), %esi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	19(%rsp), %esi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	20(%rsp), %esi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	21(%rsp), %esi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	22(%rsp), %esi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	23(%rsp), %esi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	24(%rsp), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	25(%rsp), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	26(%rsp), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	27(%rsp), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	28(%rsp), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	29(%rsp), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	30(%rsp), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	31(%rsp), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	32(%rsp), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	33(%rsp), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	34(%rsp), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	35(%rsp), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	36(%rsp), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	37(%rsp), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	38(%rsp), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	39(%rsp), %esi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	40(%rsp), %esi
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	42(%rsp), %esi
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movswl	44(%rsp), %esi
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	46(%rsp), %esi
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	48(%rsp), %esi
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	60(%rsp), %esi
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	64(%rsp), %esi
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	72(%rsp), %esi
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	76(%rsp), %esi
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	80(%rsp), %esi
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	84(%rsp), %esi
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	88(%rsp), %esi
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	560(%rsp), %esi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r15, (%rsp)
	leaq	16(%rsp), %rsi
	leaq	12(%rsp), %r9
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	%ebp, %edi
	movq	%r14, %rdx
	callq	ole2_walk_property_tree
	jmp	.LBB0_21
.LBB0_14:
	movl	$.L.str.3, %edi
	jmp	.LBB0_15
.LBB0_17:
	movl	$.L.str.4, %edi
	jmp	.LBB0_15
.LBB0_19:
	movl	$.L.str.5, %edi
.LBB0_15:
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB0_21:
	movq	536(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_23
# BB#22:
	movq	544(%rsp), %rsi
	callq	munmap
.LBB0_23:
	movq	552(%rsp), %rdi
	callq	cli_bitset_free
	xorl	%ebx, %ebx
.LBB0_24:
	movl	%ebx, %eax
	addq	$712, %rsp              # imm = 0x2C8
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	cli_ole2_extract, .Lfunc_end0-cli_ole2_extract
	.cfi_endproc

	.p2align	4, 0x90
	.type	ole2_walk_property_tree,@function
ole2_walk_property_tree:                # @ole2_walk_property_tree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$1128, %rsp             # imm = 0x468
.Lcfi15:
	.cfi_def_cfa_offset 1184
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%r9, 80(%rsp)           # 8-byte Spill
	movl	%r8d, 36(%rsp)          # 4-byte Spill
	movl	%ecx, %r14d
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	%rsi, %r15
	movl	%edi, %r13d
	testl	%r14d, %r14d
	js	.LBB1_123
# BB#1:
	cmpl	$100, 36(%rsp)          # 4-byte Folded Reload
	ja	.LBB1_123
# BB#2:
	cmpl	%r14d, 544(%r15)
	jb	.LBB1_123
# BB#3:
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %ecx
	cmpl	$100000, %ecx           # imm = 0x186A0
	ja	.LBB1_123
# BB#4:
	movq	1184(%rsp), %rdx
	movl	48(%r15), %eax
	testq	%rdx, %rdx
	je	.LBB1_11
# BB#5:
	movl	4(%rdx), %esi
	testl	%esi, %esi
	je	.LBB1_8
# BB#6:
	cmpl	%esi, %ecx
	jbe	.LBB1_8
# BB#7:
	movl	$.L.str.25, %edi
	jmp	.LBB1_121
.LBB1_8:
	movl	(%rdx), %esi
	testl	%esi, %esi
	je	.LBB1_11
# BB#9:
	cmpl	36(%rsp), %esi          # 4-byte Folded Reload
	jae	.LBB1_11
# BB#10:
	movl	$.L.str.26, %edi
.LBB1_121:                              # %ole2_read_block.exit.thread
	xorl	%eax, %eax
.LBB1_122:                              # %ole2_read_block.exit.thread
	callq	cli_dbgmsg
	jmp	.LBB1_123
.LBB1_11:                               # %.critedge
	movl	%r14d, %ebp
	shrl	$2, %ebp
	je	.LBB1_15
# BB#12:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_14:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%r13d, %edi
	movq	%r15, %rsi
	movl	%eax, %edx
	callq	ole2_get_next_block_number
	testl	%eax, %eax
	js	.LBB1_123
# BB#13:                                #   in Loop: Header=BB1_14 Depth=1
	incl	%ebx
	cmpl	%ebp, %ebx
	jl	.LBB1_14
	jmp	.LBB1_16
.LBB1_15:                               # %._crit_edge
	testl	%eax, %eax
	js	.LBB1_123
.LBB1_16:                               # %._crit_edge.thread
	movzwl	30(%r15), %ecx
	shll	%cl, %eax
	addl	$512, %eax              # imm = 0x200
	movslq	%eax, %r12
	movq	520(%r15), %rsi
	testq	%rsi, %rsi
	je	.LBB1_17
# BB#19:
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movslq	%eax, %rdx
	movq	%r12, %rax
	addq	%rdx, %rax
	jle	.LBB1_123
# BB#20:
	cmpq	528(%r15), %rax
	jg	.LBB1_123
# BB#21:
	addq	%r12, %rsi
	leaq	96(%rsp), %rdi
	callq	memcpy
	jmp	.LBB1_22
.LBB1_17:
	xorl	%edx, %edx
	movl	%r13d, %edi
	movq	%r12, %rsi
	callq	lseek
	cmpq	%r12, %rax
	jne	.LBB1_123
# BB#18:
	movb	30(%r15), %cl
	movl	$1, %ebx
	movl	$1, %edx
	shll	%cl, %edx
	leaq	96(%rsp), %rsi
	movl	%r13d, %edi
	callq	cli_readn
	movb	30(%r15), %cl
	shll	%cl, %ebx
	cmpl	%ebx, %eax
	jne	.LBB1_123
.LBB1_22:                               # %ole2_read_block.exit
	movl	%r14d, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%r14d, %eax
	andl	$-4, %eax
	movl	%r14d, %ecx
	subl	%eax, %ecx
	movslq	%ecx, %rbp
	shlq	$7, %rbp
	cmpb	$0, 162(%rsp,%rbp)
	je	.LBB1_123
# BB#23:
	leaq	96(%rsp,%rbp), %rbx
	movzwl	160(%rsp,%rbp), %esi
	cmpl	$65, %esi
	jb	.LBB1_25
# BB#24:
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_41
.LBB1_25:
	movq	%rbx, %rdi
	callq	get_property_name
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB1_27
# BB#26:
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_dbgmsg
	movq	%r12, %rdi
	callq	free
.LBB1_27:                               # %print_property_name.exit.i
	movzbl	66(%rbx), %esi
	cmpl	$1, %esi
	je	.LBB1_32
# BB#28:                                # %print_property_name.exit.i
	cmpb	$5, %sil
	je	.LBB1_33
# BB#29:                                # %print_property_name.exit.i
	cmpb	$2, %sil
	jne	.LBB1_34
# BB#30:
	movl	$.L.str.35, %edi
	jmp	.LBB1_31
.LBB1_32:
	movl	$.L.str.36, %edi
	jmp	.LBB1_31
.LBB1_33:
	movl	$.L.str.37, %edi
.LBB1_31:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_35
.LBB1_34:
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_35:
	movb	163(%rsp,%rbp), %al
	cmpb	$1, %al
	je	.LBB1_38
# BB#36:
	testb	%al, %al
	jne	.LBB1_39
# BB#37:
	movl	$.L.str.39, %edi
	jmp	.LBB1_40
.LBB1_38:
	movl	$.L.str.40, %edi
	jmp	.LBB1_40
.LBB1_39:
	movl	$.L.str.41, %edi
.LBB1_40:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	192(%rsp,%rbp), %edx
	movl	216(%rsp,%rbp), %esi
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_41:                               # %print_ole2_property.exit
	movq	536(%r15), %rdi
	movslq	%r14d, %r12
	movq	%r12, %rsi
	callq	cli_bitset_test
	testl	%eax, %eax
	je	.LBB1_43
# BB#42:
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	jmp	.LBB1_122
.LBB1_43:
	movq	536(%r15), %rdi
	movq	%r12, %rsi
	callq	cli_bitset_set
	testl	%eax, %eax
	je	.LBB1_123
# BB#44:
	movq	%rbx, %rax
	leaq	164(%rsp,%rbp), %rbx
	movq	%rax, %r12
	movzbl	66(%rax), %esi
	cmpl	$1, %esi
	je	.LBB1_116
# BB#45:
	leaq	212(%rsp,%rbp), %rdx
	cmpb	$2, %sil
	je	.LBB1_51
# BB#46:
	cmpb	$5, %sil
	jne	.LBB1_120
# BB#47:
	orl	36(%rsp), %r14d         # 4-byte Folded Reload
	jne	.LBB1_49
# BB#48:
	movq	80(%rsp), %rax          # 8-byte Reload
	cmpl	$0, (%rax)
	je	.LBB1_50
.LBB1_49:
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_123
.LBB1_116:
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	strlen
	leaq	8(%rax), %rdi
	callq	cli_malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB1_123
# BB#117:
	movq	%rbx, %r12
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	strlen
	leaq	8(%rax), %rsi
	movl	$.L.str.30, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rcx
	movl	%r14d, %r8d
	callq	snprintf
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbp, %rdi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB1_119
# BB#118:
	movq	%rbp, %rdi
	callq	free
	jmp	.LBB1_123
.LBB1_51:
	leaq	160(%rsp,%rbp), %rax
	movq	80(%rsp), %rcx          # 8-byte Reload
	incl	(%rcx)
	movzwl	(%rax), %esi
	cmpl	$65, %esi
	jb	.LBB1_53
# BB#52:
	movl	$.L.str.44, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_64
.LBB1_120:
	movl	$.L.str.32, %edi
	jmp	.LBB1_121
.LBB1_53:
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%r12, %rdi
	callq	get_property_name
	movq	%rax, %rbp
	testq	%rbp, %rbp
	movq	72(%rsp), %r14          # 8-byte Reload
	je	.LBB1_54
# BB#56:
	movq	%rbp, %rdi
	callq	sanitiseName
	jmp	.LBB1_57
.LBB1_119:
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_dbgmsg
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movl	(%r12), %ecx
	movl	36(%rsp), %ebp          # 4-byte Reload
	incl	%ebp
	movq	1184(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r13d, %edi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	movl	%ebp, %r8d
	movq	80(%rsp), %r14          # 8-byte Reload
	movq	%r14, %r9
	callq	ole2_walk_property_tree
	movl	4(%r12), %ecx
	movq	1184(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r13d, %edi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	movl	%ebp, %r8d
	movq	%r14, %r9
	callq	ole2_walk_property_tree
	movl	8(%r12), %ecx
	movq	1184(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r13d, %edi
	movq	%r15, %rsi
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movl	%ebp, %r8d
	movq	%r14, %r9
	callq	ole2_walk_property_tree
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB1_123
.LBB1_54:
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%r13d, %edi
	callq	lseek
	movq	%rax, %r14
	movl	$11, %edi
	callq	cli_malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB1_64
# BB#55:                                # %.critedge.i
	addq	%r12, %r14
	movl	$11, %esi
	movl	$.L.str.45, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r14, %rcx
	callq	snprintf
	movq	72(%rsp), %r14          # 8-byte Reload
.LBB1_57:
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %r12
	movq	%r14, %rdi
	callq	strlen
	leaq	2(%r12,%rax), %rdi
	callq	cli_malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB1_58
# BB#59:
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%rbp, %rcx
	callq	sprintf
	movq	%rbp, %rdi
	callq	free
	movl	$577, %esi              # imm = 0x241
	movl	$448, %edx              # imm = 0x1C0
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	open
	movl	%eax, 20(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	js	.LBB1_60
# BB#61:
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movq	%r12, %rdi
	callq	free
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %ebx
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	216(%rsp,%rax), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movb	30(%r15), %cl
	movl	$1, %eax
	shll	%cl, %eax
	movslq	%eax, %rdi
	callq	cli_malloc
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB1_62
# BB#65:
	callq	cli_bitset_init
	movq	%rax, 56(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB1_124
# BB#66:                                # %.preheader.i
	movl	%ebx, %esi
	testl	%esi, %esi
	js	.LBB1_113
# BB#67:                                # %.preheader.i
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_113
# BB#68:
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	216(%rsp,%rax), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
.LBB1_69:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_81 Depth 2
                                        #     Child Loop BB1_93 Depth 2
	cmpl	544(%r15), %esi
	ja	.LBB1_70
# BB#71:                                #   in Loop: Header=BB1_69 Depth=1
	movl	%esi, 40(%rsp)          # 4-byte Spill
	movslq	%esi, %rbp
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	cli_bitset_test
	testl	%eax, %eax
	jne	.LBB1_72
# BB#73:                                #   in Loop: Header=BB1_69 Depth=1
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	cli_bitset_set
	testl	%eax, %eax
	je	.LBB1_74
# BB#76:                                #   in Loop: Header=BB1_69 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	cmpl	56(%r15), %eax
	jae	.LBB1_103
# BB#77:                                #   in Loop: Header=BB1_69 Depth=1
	movl	512(%r15), %eax
	testl	%eax, %eax
	js	.LBB1_78
# BB#79:                                #   in Loop: Header=BB1_69 Depth=1
	movl	40(%rsp), %ebx          # 4-byte Reload
	shrl	$3, %ebx
	je	.LBB1_83
# BB#80:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB1_69 Depth=1
	incl	%ebx
.LBB1_81:                               # %.lr.ph.i.i
                                        #   Parent Loop BB1_69 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r13d, %edi
	movq	%r15, %rsi
	movl	%eax, %edx
	callq	ole2_get_next_block_number
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB1_81
# BB#82:                                # %._crit_edge.i.i
                                        #   in Loop: Header=BB1_69 Depth=1
	testl	%eax, %eax
	js	.LBB1_86
.LBB1_83:                               # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB1_69 Depth=1
	movzwl	30(%r15), %ecx
	shll	%cl, %eax
	addl	$512, %eax              # imm = 0x200
	movslq	%eax, %rbp
	movq	520(%r15), %rsi
	testq	%rsi, %rsi
	je	.LBB1_84
# BB#87:                                #   in Loop: Header=BB1_69 Depth=1
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movslq	%eax, %rdx
	movq	%rbp, %rax
	addq	%rdx, %rax
	jle	.LBB1_86
# BB#88:                                #   in Loop: Header=BB1_69 Depth=1
	cmpq	528(%r15), %rax
	jg	.LBB1_86
# BB#89:                                #   in Loop: Header=BB1_69 Depth=1
	addq	%rbp, %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	memcpy
	jmp	.LBB1_90
.LBB1_103:                              #   in Loop: Header=BB1_69 Depth=1
	movzwl	30(%r15), %ecx
	movl	40(%rsp), %eax          # 4-byte Reload
	shll	%cl, %eax
	addl	$512, %eax              # imm = 0x200
	movslq	%eax, %rbp
	movq	520(%r15), %rsi
	testq	%rsi, %rsi
	je	.LBB1_104
# BB#106:                               #   in Loop: Header=BB1_69 Depth=1
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movslq	%eax, %rdx
	movq	%rbp, %rax
	addq	%rdx, %rax
	jle	.LBB1_74
# BB#107:                               #   in Loop: Header=BB1_69 Depth=1
	cmpq	528(%r15), %rax
	jg	.LBB1_74
# BB#108:                               #   in Loop: Header=BB1_69 Depth=1
	addq	%rbp, %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	memcpy
	movzwl	30(%r15), %ecx
	jmp	.LBB1_109
.LBB1_104:                              #   in Loop: Header=BB1_69 Depth=1
	xorl	%edx, %edx
	movl	%r13d, %edi
	movq	%rbp, %rsi
	callq	lseek
	cmpq	%rbp, %rax
	jne	.LBB1_74
# BB#105:                               #   in Loop: Header=BB1_69 Depth=1
	movb	30(%r15), %cl
	movl	$1, %edx
	shll	%cl, %edx
	movl	%r13d, %edi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	cli_readn
	movzwl	30(%r15), %ecx
	movl	$1, %edx
	shll	%cl, %edx
	cmpl	%edx, %eax
	jne	.LBB1_74
.LBB1_109:                              # %ole2_read_block.exit.i
                                        #   in Loop: Header=BB1_69 Depth=1
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %CX<kill>
	shll	%cl, %edx
	movl	64(%rsp), %ebx          # 4-byte Reload
	cmpl	%edx, %ebx
	cmovlel	%ebx, %edx
	movl	20(%rsp), %edi          # 4-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	cli_writen
	movb	30(%r15), %cl
	movl	$1, %edx
	shll	%cl, %edx
	cmpl	%edx, %ebx
	cmovlel	%ebx, %edx
	cmpl	%edx, %eax
	jne	.LBB1_74
# BB#110:                               #   in Loop: Header=BB1_69 Depth=1
	movl	%r13d, %edi
	movq	%r15, %rsi
	movl	40(%rsp), %edx          # 4-byte Reload
	callq	ole2_get_next_block_number
	movb	30(%r15), %cl
	movl	$1, %r14d
	shll	%cl, %r14d
	movl	64(%rsp), %ecx          # 4-byte Reload
	cmpl	%r14d, %ecx
	cmovlel	%ecx, %r14d
	jmp	.LBB1_111
.LBB1_84:                               #   in Loop: Header=BB1_69 Depth=1
	xorl	%edx, %edx
	movl	%r13d, %edi
	movq	%rbp, %rsi
	callq	lseek
	cmpq	%rbp, %rax
	jne	.LBB1_86
# BB#85:                                #   in Loop: Header=BB1_69 Depth=1
	movb	30(%r15), %cl
	movl	$1, %edx
	shll	%cl, %edx
	movl	%r13d, %edi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	cli_readn
	movb	30(%r15), %cl
	movl	$1, %edx
	shll	%cl, %edx
	cmpl	%edx, %eax
	jne	.LBB1_86
.LBB1_90:                               # %ole2_get_sbat_data_block.exit.i
                                        #   in Loop: Header=BB1_69 Depth=1
	movl	40(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %r12d
	sarl	$31, %r12d
	movl	%r12d, %eax
	shrl	$29, %eax
	addl	%ecx, %eax
	andl	$67108856, %eax         # imm = 0x3FFFFF8
	subl	%eax, %ecx
	shll	$6, %ecx
	movslq	%ecx, %rsi
	addq	24(%rsp), %rsi          # 8-byte Folded Reload
	movl	64(%rsp), %eax          # 4-byte Reload
	cmpl	$65, %eax
	movl	$64, %r14d
	cmovll	%eax, %r14d
	movl	20(%rsp), %edi          # 4-byte Reload
	movl	%r14d, %edx
	callq	cli_writen
	cmpl	%r14d, %eax
	jne	.LBB1_74
# BB#91:                                #   in Loop: Header=BB1_69 Depth=1
	movl	60(%r15), %eax
	movl	40(%rsp), %ebp          # 4-byte Reload
	shrl	$7, %ebp
	je	.LBB1_94
# BB#92:                                # %.lr.ph.i122.i.preheader
                                        #   in Loop: Header=BB1_69 Depth=1
	incl	%ebp
.LBB1_93:                               # %.lr.ph.i122.i
                                        #   Parent Loop BB1_69 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r13d, %edi
	movq	%r15, %rsi
	movl	%eax, %edx
	callq	ole2_get_next_block_number
	decl	%ebp
	cmpl	$1, %ebp
	jg	.LBB1_93
.LBB1_94:                               # %._crit_edge.i123.i
                                        #   in Loop: Header=BB1_69 Depth=1
	movl	$-1, %ebx
	testl	%eax, %eax
	js	.LBB1_102
# BB#95:                                #   in Loop: Header=BB1_69 Depth=1
	movzwl	30(%r15), %ecx
	shll	%cl, %eax
	addl	$512, %eax              # imm = 0x200
	movslq	%eax, %rbp
	movq	520(%r15), %rsi
	testq	%rsi, %rsi
	je	.LBB1_96
# BB#98:                                #   in Loop: Header=BB1_69 Depth=1
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movslq	%eax, %rdx
	movq	%rbp, %rax
	addq	%rdx, %rax
	jle	.LBB1_102
# BB#99:                                #   in Loop: Header=BB1_69 Depth=1
	cmpq	528(%r15), %rax
	jg	.LBB1_102
# BB#100:                               #   in Loop: Header=BB1_69 Depth=1
	addq	%rbp, %rsi
	leaq	608(%rsp), %rdi
	callq	memcpy
	jmp	.LBB1_101
.LBB1_96:                               #   in Loop: Header=BB1_69 Depth=1
	xorl	%edx, %edx
	movl	%r13d, %edi
	movq	%rbp, %rsi
	callq	lseek
	cmpq	%rbp, %rax
	jne	.LBB1_102
# BB#97:                                #   in Loop: Header=BB1_69 Depth=1
	movb	30(%r15), %cl
	movl	$1, %edx
	shll	%cl, %edx
	movl	%r13d, %edi
	leaq	608(%rsp), %rsi
	callq	cli_readn
	movb	30(%r15), %cl
	movl	$1, %edx
	shll	%cl, %edx
	cmpl	%edx, %eax
	jne	.LBB1_102
.LBB1_101:                              # %ole2_read_block.exit.i.i
                                        #   in Loop: Header=BB1_69 Depth=1
	shrl	$25, %r12d
	movl	40(%rsp), %eax          # 4-byte Reload
	addl	%eax, %r12d
	andl	$-128, %r12d
	subl	%r12d, %eax
	cltq
	movl	608(%rsp,%rax,4), %ebx
.LBB1_102:                              # %ole2_get_next_sbat_block.exit.i
                                        #   in Loop: Header=BB1_69 Depth=1
	movl	%ebx, %eax
.LBB1_111:                              # %.backedge.i
                                        #   in Loop: Header=BB1_69 Depth=1
	subl	%r14d, 64(%rsp)         # 4-byte Folded Spill
	movl	%eax, %esi
	jle	.LBB1_113
# BB#112:                               # %.backedge.i
                                        #   in Loop: Header=BB1_69 Depth=1
	testl	%esi, %esi
	jns	.LBB1_69
.LBB1_113:                              # %._crit_edge.i
	movl	20(%rsp), %edi          # 4-byte Reload
	callq	close
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	cli_bitset_free
	movq	48(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB1_114
.LBB1_58:
	movq	%rbp, %rdi
	callq	free
	jmp	.LBB1_64
.LBB1_50:
	movl	(%rdx), %eax
	movl	%eax, 512(%r15)
	movl	(%rbx), %ecx
	movl	36(%rsp), %ebp          # 4-byte Reload
	incl	%ebp
	movq	1184(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r13d, %edi
	movq	%r15, %rsi
	movq	72(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdx
	movl	%ebp, %r8d
	movq	%rbx, %r12
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %r9
	callq	ole2_walk_property_tree
	movl	4(%r12), %ecx
	movq	1184(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r13d, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	movl	%ebp, %r8d
	movq	%rbx, %r9
	callq	ole2_walk_property_tree
	movl	8(%r12), %ecx
	movq	1184(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r13d, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	jmp	.LBB1_115
.LBB1_60:
	movl	$.L.str.47, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cli_errmsg
	movq	%r12, %rdi
	callq	free
	jmp	.LBB1_64
.LBB1_124:
	movl	$.L.str.48, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB1_62:
	movl	20(%rsp), %edi          # 4-byte Reload
	callq	close
.LBB1_63:
	movq	48(%rsp), %rbx          # 8-byte Reload
.LBB1_64:
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_114:                              # %handler_writefile.exit
	movl	(%rbx), %ecx
	movq	1184(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r13d, %edi
	movq	%r15, %rsi
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdx
	movl	36(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %r8d
	movq	%rbx, %r14
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %r9
	callq	ole2_walk_property_tree
	movl	4(%r14), %ecx
	movq	1184(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r13d, %edi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movl	%ebp, %r8d
	movq	%rbx, %r9
	callq	ole2_walk_property_tree
	movl	8(%r14), %ecx
	movq	1184(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r13d, %edi
	movq	%r15, %rsi
	movq	%r12, %rdx
.LBB1_115:                              # %ole2_read_block.exit.thread
	movl	%ebp, %r8d
	movq	%rbx, %r9
	callq	ole2_walk_property_tree
.LBB1_123:                              # %ole2_read_block.exit.thread
	addq	$1128, %rsp             # imm = 0x468
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_70:
	movl	$.L.str.49, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_74
.LBB1_72:
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	20(%rsp), %edi          # 4-byte Reload
	callq	close
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%rbx, %rdi
	callq	cli_bitset_free
	jmp	.LBB1_63
.LBB1_78:
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB1_86:                               # %.loopexit.i
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_74:
	movl	20(%rsp), %edi          # 4-byte Reload
	callq	close
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	cli_bitset_free
	jmp	.LBB1_63
.Lfunc_end1:
	.size	ole2_walk_property_tree, .Lfunc_end1-ole2_walk_property_tree
	.cfi_endproc

	.p2align	4, 0x90
	.type	ole2_get_next_block_number,@function
ole2_get_next_block_number:             # @ole2_get_next_block_number
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	subq	$1048, %rsp             # imm = 0x418
.Lcfi28:
	.cfi_def_cfa_offset 1104
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movq	%rsi, %r14
	movl	%edi, %r15d
	testl	%r13d, %r13d
	js	.LBB2_1
# BB#2:
	cmpl	$13952, %r13d           # imm = 0x3680
	jl	.LBB2_29
# BB#3:
	movl	68(%r14), %eax
	movl	$-1, %ebx
	testl	%eax, %eax
	js	.LBB2_39
# BB#4:
	movzwl	30(%r14), %r12d
	movl	%r12d, %ecx
	shll	%cl, %eax
	addl	$512, %eax              # imm = 0x200
	movslq	%eax, %rbp
	movq	520(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB2_5
# BB#7:
	movzwl	%r12w, %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movslq	%eax, %rdx
	movq	%rbp, %rax
	addq	%rdx, %rax
	jle	.LBB2_39
# BB#8:
	cmpq	528(%r14), %rax
	jg	.LBB2_39
# BB#9:
	addq	%rbp, %rsi
	leaq	16(%rsp), %rdi
	callq	memcpy
	jmp	.LBB2_10
.LBB2_1:
	movl	$-1, %ebx
	jmp	.LBB2_39
.LBB2_29:
	movl	%r13d, %ebp
	shrl	$7, %ebp
	cmpl	44(%r14), %ebp
	jle	.LBB2_31
# BB#30:
	movl	$.L.str.33, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-10, %ebx
	jmp	.LBB2_39
.LBB2_31:
	movl	%ebp, %eax
	movl	76(%r14,%rax,4), %eax
	movl	$-1, %ebx
	testl	%eax, %eax
	js	.LBB2_39
# BB#32:
	movzwl	30(%r14), %ecx
	shll	%cl, %eax
	addl	$512, %eax              # imm = 0x200
	movslq	%eax, %r12
	movq	520(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB2_33
# BB#35:
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movslq	%eax, %rdx
	movq	%r12, %rax
	addq	%rdx, %rax
	jle	.LBB2_39
# BB#36:
	cmpq	528(%r14), %rax
	jg	.LBB2_39
# BB#37:
	addq	%r12, %rsi
	leaq	16(%rsp), %rdi
	callq	memcpy
	jmp	.LBB2_38
.LBB2_5:
	xorl	%edx, %edx
	movl	%r15d, %edi
	movq	%rbp, %rsi
	callq	lseek
	cmpq	%rbp, %rax
	jne	.LBB2_39
# BB#6:
	movb	30(%r14), %cl
	movl	$1, %ebp
	movl	$1, %edx
	shll	%cl, %edx
	leaq	16(%rsp), %rsi
	movl	%r15d, %edi
	callq	cli_readn
	movzwl	30(%r14), %r12d
	movl	%r12d, %ecx
	shll	%cl, %ebp
	cmpl	%ebp, %eax
	jne	.LBB2_39
.LBB2_10:                               # %ole2_read_block.exit.preheader.i
	movl	%r13d, %eax
	shrl	$7, %eax
	leal	-109(%rax), %edx
	movslq	%edx, %rcx
	imulq	$-2130574327, %rcx, %rcx # imm = 0x81020409
	shrq	$32, %rcx
	leal	-109(%rcx,%rax), %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	sarl	$6, %ecx
	addl	%eax, %ecx
	movq	%rcx, %rax
	movl	%edx, 4(%rsp)           # 4-byte Spill
	cmpl	$127, %edx
	movl	%r15d, (%rsp)           # 4-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jl	.LBB2_21
# BB#11:                                # %.lr.ph.i
	leal	1(%rax), %r15d
	.p2align	4, 0x90
.LBB2_12:                               # =>This Inner Loop Header: Depth=1
	movl	524(%rsp), %eax
	testl	%eax, %eax
	js	.LBB2_39
# BB#13:                                #   in Loop: Header=BB2_12 Depth=1
	movl	%r12d, %ecx
	shll	%cl, %eax
	addl	$512, %eax              # imm = 0x200
	movslq	%eax, %rbp
	movq	520(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB2_14
# BB#17:                                #   in Loop: Header=BB2_12 Depth=1
	movzwl	%r12w, %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movslq	%eax, %rdx
	movq	%rbp, %rax
	addq	%rdx, %rax
	jle	.LBB2_39
# BB#18:                                #   in Loop: Header=BB2_12 Depth=1
	cmpq	528(%r14), %rax
	jg	.LBB2_39
# BB#19:                                #   in Loop: Header=BB2_12 Depth=1
	addq	%rbp, %rsi
	leaq	16(%rsp), %rdi
	callq	memcpy
	jmp	.LBB2_20
.LBB2_14:                               #   in Loop: Header=BB2_12 Depth=1
	xorl	%edx, %edx
	movl	(%rsp), %ebx            # 4-byte Reload
	movl	%ebx, %edi
	movq	%rbp, %rsi
	callq	lseek
	cmpq	%rbp, %rax
	jne	.LBB2_15
# BB#16:                                #   in Loop: Header=BB2_12 Depth=1
	movzbl	30(%r14), %ecx
	movl	$1, %edx
	shll	%cl, %edx
	movl	%ebx, %edi
	leaq	16(%rsp), %rsi
	callq	cli_readn
	movzwl	30(%r14), %r12d
	movl	$1, %edx
	movl	%r12d, %ecx
	shll	%cl, %edx
	cmpl	%edx, %eax
	movl	$-1, %ebx
	jne	.LBB2_39
.LBB2_20:                               # %ole2_read_block.exit23.i
                                        #   in Loop: Header=BB2_12 Depth=1
	decl	%r15d
	cmpl	$1, %r15d
	jg	.LBB2_12
.LBB2_21:                               # %ole2_read_block.exit._crit_edge.i
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ecx, %eax
	shll	$7, %eax
	subl	%ecx, %eax
	movl	4(%rsp), %ecx           # 4-byte Reload
	subl	%eax, %ecx
	movslq	%ecx, %rax
	movl	16(%rsp,%rax,4), %eax
	testl	%eax, %eax
	movl	(%rsp), %r15d           # 4-byte Reload
	js	.LBB2_39
# BB#22:
	movl	%r12d, %ecx
	shll	%cl, %eax
	addl	$512, %eax              # imm = 0x200
	movslq	%eax, %rbp
	movq	520(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB2_23
# BB#25:
	movzwl	%r12w, %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movslq	%eax, %rdx
	movq	%rbp, %rax
	addq	%rdx, %rax
	jle	.LBB2_39
# BB#26:
	cmpq	528(%r14), %rax
	jg	.LBB2_39
# BB#27:
	addq	%rbp, %rsi
	leaq	528(%rsp), %rdi
	callq	memcpy
	jmp	.LBB2_28
.LBB2_33:
	xorl	%edx, %edx
	movl	%r15d, %edi
	movq	%r12, %rsi
	callq	lseek
	cmpq	%r12, %rax
	jne	.LBB2_39
# BB#34:
	movb	30(%r14), %cl
	movl	%r15d, %edi
	movl	$1, %r15d
	movl	$1, %edx
	shll	%cl, %edx
	leaq	16(%rsp), %rsi
	callq	cli_readn
	movb	30(%r14), %cl
	shll	%cl, %r15d
	cmpl	%r15d, %eax
	jne	.LBB2_39
.LBB2_38:                               # %ole2_read_block.exit.i
	shll	$7, %ebp
	subl	%ebp, %r13d
	movslq	%r13d, %rax
	movl	16(%rsp,%rax,4), %ebx
.LBB2_39:
	movl	%ebx, %eax
	addq	$1048, %rsp             # imm = 0x418
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_23:
	xorl	%edx, %edx
	movl	%r15d, %edi
	movq	%rbp, %rsi
	callq	lseek
	cmpq	%rbp, %rax
	jne	.LBB2_39
# BB#24:
	movb	30(%r14), %cl
	movl	$1, %ebp
	movl	$1, %edx
	shll	%cl, %edx
	leaq	528(%rsp), %rsi
	movl	%r15d, %edi
	callq	cli_readn
	movb	30(%r14), %cl
	shll	%cl, %ebp
	cmpl	%ebp, %eax
	jne	.LBB2_39
.LBB2_28:                               # %ole2_read_block.exit25.i
	andl	$127, %r13d
	movl	%r13d, %eax
	movl	528(%rsp,%rax,4), %ebx
	jmp	.LBB2_39
.LBB2_15:
	movl	$-1, %ebx
	jmp	.LBB2_39
.Lfunc_end2:
	.size	ole2_get_next_block_number, .Lfunc_end2-ole2_get_next_block_number
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_property_name,@function
get_property_name:                      # @get_property_name
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 64
.Lcfi42:
	.cfi_offset %rbx, -56
.Lcfi43:
	.cfi_offset %r12, -48
.Lcfi44:
	.cfi_offset %r13, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r14
	leal	-1(%r15), %eax
	xorl	%r12d, %r12d
	cmpl	$63, %eax
	ja	.LBB3_16
# BB#1:
	movb	(%r14), %al
	testb	%al, %al
	je	.LBB3_16
# BB#2:
	leal	(,%r15,8), %eax
	subl	%r15d, %eax
	movslq	%eax, %rdi
	callq	cli_malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB3_15
# BB#3:                                 # %.preheader
	xorl	%ebx, %ebx
	cmpl	$3, %r15d
	jl	.LBB3_13
# BB#4:                                 # %.lr.ph.preheader
	addl	$-2, %r15d
	movslq	%r15d, %r15
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbq	(%r14,%r13), %rbp
	testq	%rbp, %rbp
	js	.LBB3_9
# BB#6:                                 #   in Loop: Header=BB3_5 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	testb	$64, 1(%rax,%rbp,2)
	jne	.LBB3_11
# BB#7:                                 #   in Loop: Header=BB3_5 Depth=1
	cmpb	$9, %bpl
	ja	.LBB3_9
# BB#8:                                 #   in Loop: Header=BB3_5 Depth=1
	leal	1(%rbx), %eax
	movslq	%ebx, %rcx
	movb	$95, (%r12,%rcx)
	movzbl	(%r14,%r13), %ecx
	addb	$48, %cl
	movl	$2, %edx
	jmp	.LBB3_10
	.p2align	4, 0x90
.LBB3_9:                                # %.thread
                                        #   in Loop: Header=BB3_5 Depth=1
	shll	$8, %ebp
	movsbl	1(%r14,%r13), %eax
	orl	%eax, %ebp
	movslq	%ebx, %rcx
	movb	$95, (%r12,%rcx)
	movl	%eax, %edx
	andb	$15, %dl
	addb	$97, %dl
	movb	%dl, 1(%r12,%rcx)
	shrl	$4, %eax
	andl	$15, %eax
	addl	$97, %eax
	movb	%al, 2(%r12,%rcx)
	shrl	$8, %ebp
	andl	$15, %ebp
	addl	$97, %ebp
	movb	%bpl, 3(%r12,%rcx)
	leal	5(%rbx), %eax
	movb	$97, 4(%r12,%rcx)
	movb	$97, %cl
	movl	$6, %edx
.LBB3_10:                               #   in Loop: Header=BB3_5 Depth=1
	addl	%ebx, %edx
	cltq
	movb	%cl, (%r12,%rax)
	movb	$95, %bpl
	movl	%edx, %ebx
.LBB3_11:                               #   in Loop: Header=BB3_5 Depth=1
	movslq	%ebx, %rax
	incl	%ebx
	movb	%bpl, (%r12,%rax)
	addq	$2, %r13
	cmpq	%r15, %r13
	jl	.LBB3_5
# BB#12:                                # %._crit_edge.loopexit
	movslq	%ebx, %rbx
.LBB3_13:                               # %._crit_edge
	movb	$0, (%r12,%rbx)
	cmpb	$0, (%r12)
	jne	.LBB3_16
# BB#14:
	movq	%r12, %rdi
	callq	free
.LBB3_15:
	xorl	%r12d, %r12d
.LBB3_16:
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	get_property_name, .Lfunc_end3-get_property_name
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"in cli_ole2_extract()\n"
	.size	.L.str, 23

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"mmap'ed file\n"
	.size	.L.str.1, 14

	.type	magic_id,@object        # @magic_id
	.data
magic_id:
	.ascii	"\320\317\021\340\241\261\032\341"
	.size	magic_id, 8

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"OLE2 magic failed!\n"
	.size	.L.str.2, 20

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"WARNING: not scanned; untested big block size - please report\n"
	.size	.L.str.3, 63

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"WARNING: not scanned; untested small block size - please report\n"
	.size	.L.str.4, 65

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"WARNING: not scanned; untested sbat cutoff - please report\n"
	.size	.L.str.5, 60

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Max block number: %lu\n"
	.size	.L.str.6, 23

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\nMagic:\t\t\t0x"
	.size	.L.str.7, 13

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%x"
	.size	.L.str.8, 3

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"\n"
	.size	.L.str.9, 2

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"CLSID:\t\t\t{"
	.size	.L.str.10, 11

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%x "
	.size	.L.str.11, 4

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"}\n"
	.size	.L.str.12, 3

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Minor version:\t\t0x%x\n"
	.size	.L.str.13, 22

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"DLL version:\t\t0x%x\n"
	.size	.L.str.14, 20

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Byte Order:\t\t%d\n"
	.size	.L.str.15, 17

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Big Block Size:\t\t%i\n"
	.size	.L.str.16, 21

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Small Block Size:\t%i\n"
	.size	.L.str.17, 22

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"BAT count:\t\t%d\n"
	.size	.L.str.18, 16

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Prop start:\t\t%d\n"
	.size	.L.str.19, 17

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"SBAT cutoff:\t\t%d\n"
	.size	.L.str.20, 18

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"SBat start:\t\t%d\n"
	.size	.L.str.21, 17

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"SBat block count:\t%d\n"
	.size	.L.str.22, 22

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"XBat start:\t\t%d\n"
	.size	.L.str.23, 17

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"XBat block count:\t%d\n\n"
	.size	.L.str.24, 23

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"OLE2: File limit reached (max: %d)\n"
	.size	.L.str.25, 36

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"OLE2: Recursion limit reached (max: %d)\n"
	.size	.L.str.26, 41

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"OLE2: Property tree loop detected at index %d\n"
	.size	.L.str.27, 47

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"ERROR: illegal Root Entry\n"
	.size	.L.str.28, 27

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"ERROR: handler failed\n"
	.size	.L.str.29, 23

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"%s/%.6d"
	.size	.L.str.30, 8

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"OLE2 dir entry: %s\n"
	.size	.L.str.31, 20

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"ERROR: unknown OLE2 entry type: %d\n"
	.size	.L.str.32, 36

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"bat_array index error\n"
	.size	.L.str.33, 23

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"[err name len: %d]\n"
	.size	.L.str.34, 20

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	" [file] "
	.size	.L.str.35, 9

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	" [dir ] "
	.size	.L.str.36, 9

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	" [root] "
	.size	.L.str.37, 9

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	" [%d]"
	.size	.L.str.38, 6

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	" r "
	.size	.L.str.39, 4

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	" b "
	.size	.L.str.40, 4

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	" u "
	.size	.L.str.41, 4

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	" 0x%.8x 0x%.8x\n"
	.size	.L.str.42, 16

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"%34s "
	.size	.L.str.43, 6

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"\nERROR: property name too long: %d\n"
	.size	.L.str.44, 36

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"%.10ld"
	.size	.L.str.45, 7

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"%s/%s"
	.size	.L.str.46, 6

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"ERROR: failed to create file: %s\n"
	.size	.L.str.47, 34

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"ERROR [handler_writefile]: init bitset failed\n"
	.size	.L.str.48, 47

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"OLE2: Max block number for file size exceeded: %d\n"
	.size	.L.str.49, 51

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"OLE2: Block list loop detected\n"
	.size	.L.str.50, 32

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"ole2_get_sbat_data_block failed\n"
	.size	.L.str.51, 33

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"No root start block\n"
	.size	.L.str.52, 21


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
