	.text
	.file	"lists1.bc"
	.globl	_Z12list_print_nNSt7__cxx114listIiSaIiEEEi
	.p2align	4, 0x90
	.type	_Z12list_print_nNSt7__cxx114listIiSaIiEEEi,@function
_Z12list_print_nNSt7__cxx114listIiSaIiEEEi: # @_Z12list_print_nNSt7__cxx114listIiSaIiEEEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r14
	testl	%r15d, %r15d
	jle	.LBB0_7
# BB#1:
	movq	(%r14), %rbx
	cmpq	%r14, %rbx
	je	.LBB0_7
# BB#2:                                 # %.lr.ph.preheader
	leal	-1(%r15), %r12d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	16(%rbx), %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSolsEi
	cmpl	%r12d, %ebp
	jge	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	movl	$_ZSt4cout, %edi
	movl	$.L.str, %esi
	movl	$1, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.LBB0_5:                                #   in Loop: Header=BB0_3 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	jge	.LBB0_7
# BB#6:                                 #   in Loop: Header=BB0_3 Depth=1
	movq	(%rbx), %rbx
	cmpq	%r14, %rbx
	jne	.LBB0_3
.LBB0_7:                                # %.critedge
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_12
# BB#8:                                 # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit
	cmpb	$0, 56(%rbx)
	je	.LBB0_10
# BB#9:
	movb	67(%rbx), %al
	jmp	.LBB0_11
.LBB0_10:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_11:                               # %_ZNKSt5ctypeIcE5widenEc.exit
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZNSo5flushEv           # TAILCALL
.LBB0_12:
	callq	_ZSt16__throw_bad_castv
.Lfunc_end0:
	.size	_Z12list_print_nNSt7__cxx114listIiSaIiEEEi, .Lfunc_end0-_Z12list_print_nNSt7__cxx114listIiSaIiEEEi
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 160
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movl	$1000000, %r13d         # imm = 0xF4240
	cmpl	$2, %edi
	jne	.LBB1_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	testl	%eax, %eax
	movl	$1, %r13d
	cmovgl	%eax, %r13d
.LBB1_2:                                # %.lr.ph.i.i36.preheader
	leaq	32(%rsp), %rbx
	movq	%rbx, 32(%rsp)
	movq	%rbx, 40(%rsp)
	movq	$0, 48(%rsp)
	leaq	8(%rsp), %r14
	movq	%r14, 8(%rsp)
	movq	%r14, 16(%rsp)
	movq	$0, 24(%rsp)
	movl	%r13d, %ebp
	negq	%rbp
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph.i.i36
                                        # =>This Inner Loop Header: Depth=1
.Ltmp0:
	movl	$24, %edi
	callq	_Znwm
.Ltmp1:
# BB#4:                                 # %.noexc.i
                                        #   in Loop: Header=BB1_3 Depth=1
	movl	$0, 16(%rax)
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	incq	24(%rsp)
	incq	%rbp
	jne	.LBB1_3
# BB#5:                                 # %.loopexit159
	movq	8(%rsp), %rbp
	cmpq	%r14, %rbp
	je	.LBB1_11
# BB#6:                                 # %.lr.ph.preheader.i
	movl	$1, %eax
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB1_7:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rdx
	movl	%eax, 16(%rcx)
	incl	%eax
	cmpq	%r14, %rdx
	movq	%rdx, %rcx
	jne	.LBB1_7
	jmp	.LBB1_8
	.p2align	4, 0x90
.LBB1_10:                               # %_Z4iotaISt14_List_iteratorIiEiEvT_S2_T0_.exit
                                        #   in Loop: Header=BB1_8 Depth=1
	movl	16(%rbp), %ecx
	movl	%ecx, 16(%rax)
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	incq	48(%rsp)
	movq	8(%rsp), %r15
	decq	24(%rsp)
	movq	%r15, %rdi
	callq	_ZNSt8__detail15_List_node_base9_M_unhookEv
	movq	%r15, %rdi
	callq	_ZdlPv
	movq	8(%rsp), %rbp
.LBB1_8:                                # %_Z4iotaISt14_List_iteratorIiEiEvT_S2_T0_.exit.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r14, %rbp
	je	.LBB1_11
# BB#9:                                 # %.lr.ph168
                                        #   in Loop: Header=BB1_8 Depth=1
	movq	32(%rsp), %r15
.Ltmp3:
	movl	$24, %edi
	callq	_Znwm
.Ltmp4:
	jmp	.LBB1_10
.LBB1_11:                               # %_Z4iotaISt14_List_iteratorIiEiEvT_S2_T0_.exit._crit_edge
	leaq	80(%rsp), %rax
	movq	%rax, 80(%rsp)
	movq	%rax, 88(%rsp)
	movq	$0, 96(%rsp)
	movq	32(%rsp), %rbp
	cmpq	%rbx, %rbp
	je	.LBB1_15
# BB#12:                                # %.lr.ph.i.i48
	leaq	80(%rsp), %r12
	.p2align	4, 0x90
.LBB1_13:                               # =>This Inner Loop Header: Depth=1
.Ltmp6:
	movl	$24, %edi
	callq	_Znwm
.Ltmp7:
# BB#14:                                # %.noexc.i49
                                        #   in Loop: Header=BB1_13 Depth=1
	movl	16(%rbp), %ecx
	movl	%ecx, 16(%rax)
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	incq	96(%rsp)
	movq	(%rbp), %rbp
	cmpq	%rbx, %rbp
	jne	.LBB1_13
.LBB1_15:                               # %_ZNSt7__cxx114listIiSaIiEEC2ERKS2_.exit
.Ltmp9:
	leaq	80(%rsp), %r12
	movl	$2, %esi
	movq	%r12, %rdi
	callq	_Z12list_print_nNSt7__cxx114listIiSaIiEEEi
.Ltmp10:
# BB#16:
	movq	80(%rsp), %rdi
	cmpq	%r12, %rdi
	je	.LBB1_18
	.p2align	4, 0x90
.LBB1_17:                               # %.lr.ph.i.i56
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%r12, %rbp
	movq	%rbp, %rdi
	jne	.LBB1_17
.LBB1_18:                               # %.loopexit157
	movq	%rbx, %rdi
	callq	_ZNSt8__detail15_List_node_base10_M_reverseEv
	movq	32(%rsp), %rdx
	cmpq	%rbx, %rdx
	movq	%rbx, %rax
	je	.LBB1_23
# BB#19:                                # %.lr.ph.i.i.i58
	leaq	32(%rsp), %rcx
	movq	%rdx, %rax
	.p2align	4, 0x90
.LBB1_20:                               # =>This Inner Loop Header: Depth=1
	cmpl	$0, 16(%rax)
	je	.LBB1_23
# BB#21:                                #   in Loop: Header=BB1_20 Depth=1
	movq	(%rax), %rax
	cmpq	%rcx, %rax
	jne	.LBB1_20
# BB#22:
	movq	%rcx, %rax
.LBB1_23:                               # %_ZSt4findISt14_List_iteratorIiEiET_S2_S2_RKT0_.exit
	xorl	%edx, %edx
	cmpq	%rbx, %rax
	sete	%dl
	movl	$.L.str.1, %eax
	movl	$.L.str.2, %esi
	cmoveq	%rax, %rsi
	orq	$4, %rdx
.Ltmp12:
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp13:
# BB#24:                                # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %r15
	testq	%r15, %r15
	je	.LBB1_25
# BB#32:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit.i.i62
	cmpb	$0, 56(%r15)
	je	.LBB1_34
# BB#33:
	movb	67(%r15), %al
	jmp	.LBB1_36
.LBB1_34:
.Ltmp14:
	movq	%r15, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp15:
# BB#35:                                # %.noexc66
	movq	(%r15), %rax
.Ltmp16:
	movl	$10, %esi
	movq	%r15, %rdi
	callq	*48(%rax)
.Ltmp17:
.LBB1_36:                               # %_ZNKSt9basic_iosIcSt11char_traitsIcEE5widenEc.exit.i64
.Ltmp18:
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
.Ltmp19:
# BB#37:                                # %.noexc68
.Ltmp20:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp21:
# BB#38:
	leaq	32(%rsp), %rax
	movq	32(%rsp), %rsi
	cmpq	%rax, %rsi
	movq	%rax, %rcx
	je	.LBB1_43
# BB#39:                                # %.lr.ph.i.i.i71
	leaq	32(%rsp), %rdx
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB1_40:                               # =>This Inner Loop Header: Depth=1
	cmpl	%r13d, 16(%rcx)
	je	.LBB1_43
# BB#41:                                #   in Loop: Header=BB1_40 Depth=1
	movq	(%rcx), %rcx
	cmpq	%rdx, %rcx
	jne	.LBB1_40
# BB#42:
	movq	%rdx, %rcx
.LBB1_43:                               # %_ZSt4findISt14_List_iteratorIiEiET_S2_S2_RKT0_.exit74
	xorl	%edx, %edx
	cmpq	%rax, %rcx
	sete	%dl
	movl	$.L.str.1, %eax
	movl	$.L.str.2, %esi
	cmoveq	%rax, %rsi
	orq	$4, %rdx
.Ltmp22:
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp23:
# BB#44:                                # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit76
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %r15
	testq	%r15, %r15
	je	.LBB1_45
# BB#47:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit.i.i79
	cmpb	$0, 56(%r15)
	je	.LBB1_49
# BB#48:
	movb	67(%r15), %al
	jmp	.LBB1_51
.LBB1_49:
.Ltmp24:
	movq	%r15, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp25:
# BB#50:                                # %.noexc83
	movq	(%r15), %rax
.Ltmp26:
	movl	$10, %esi
	movq	%r15, %rdi
	callq	*48(%rax)
.Ltmp27:
.LBB1_51:                               # %_ZNKSt9basic_iosIcSt11char_traitsIcEE5widenEc.exit.i81
.Ltmp28:
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
.Ltmp29:
# BB#52:                                # %.noexc85
.Ltmp30:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp31:
# BB#53:
	movq	32(%rsp), %rbp
	leaq	32(%rsp), %r12
	cmpq	%r12, %rbp
	je	.LBB1_59
# BB#54:                                # %.lr.ph166.preheader
	shrl	%r13d
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB1_55:                               # %.lr.ph166
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r13d, 16(%rbp)
	jge	.LBB1_58
# BB#56:                                #   in Loop: Header=BB1_55 Depth=1
.Ltmp32:
	movl	$24, %edi
	callq	_Znwm
.Ltmp33:
# BB#57:                                # %_ZNSt7__cxx114listIiSaIiEE9push_backERKi.exit
                                        #   in Loop: Header=BB1_55 Depth=1
	movl	16(%rbp), %ecx
	movl	%ecx, 16(%rax)
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	incq	24(%rsp)
.LBB1_58:                               #   in Loop: Header=BB1_55 Depth=1
	movq	(%rbp), %rbp
	cmpq	%r12, %rbp
	jne	.LBB1_55
.LBB1_59:                               # %._crit_edge167
	movq	%r12, %r13
	leaq	56(%rsp), %rax
	movq	%rax, 56(%rsp)
	movq	%rax, 64(%rsp)
	movq	$0, 72(%rsp)
	movq	8(%rsp), %rbp
	cmpq	%r14, %rbp
	je	.LBB1_63
# BB#60:                                # %.lr.ph.i.i92
	leaq	56(%rsp), %r12
	.p2align	4, 0x90
.LBB1_61:                               # =>This Inner Loop Header: Depth=1
.Ltmp35:
	movl	$24, %edi
	callq	_Znwm
.Ltmp36:
# BB#62:                                # %.noexc.i94
                                        #   in Loop: Header=BB1_61 Depth=1
	movl	16(%rbp), %ecx
	movl	%ecx, 16(%rax)
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	incq	72(%rsp)
	movq	(%rbp), %rbp
	cmpq	%r14, %rbp
	jne	.LBB1_61
.LBB1_63:                               # %_ZNSt7__cxx114listIiSaIiEEC2ERKS2_.exit100
.Ltmp38:
	leaq	56(%rsp), %r12
	movl	$10, %esi
	movq	%r12, %rdi
	callq	_Z12list_print_nNSt7__cxx114listIiSaIiEEEi
.Ltmp39:
# BB#64:
	movq	56(%rsp), %rdi
	cmpq	%r12, %rdi
	je	.LBB1_66
	.p2align	4, 0x90
.LBB1_65:                               # %.lr.ph.i.i102
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%r12, %rbp
	movq	%rbp, %rdi
	jne	.LBB1_65
.LBB1_66:                               # %.loopexit
	movq	8(%rsp), %rcx
	cmpq	%r14, %rcx
	je	.LBB1_67
# BB#74:                                # %.lr.ph.preheader
	xorl	%eax, %eax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_75:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	16(%rcx), %edx
	cmpl	$1000, %edx             # imm = 0x3E8
	cmovgel	%eax, %edx
	addl	%edx, %esi
	movq	(%rcx), %rcx
	cmpq	%r14, %rcx
	jne	.LBB1_75
	jmp	.LBB1_76
.LBB1_67:
	xorl	%esi, %esi
.LBB1_76:                               # %._crit_edge
.Ltmp41:
	movl	$_ZSt4cout, %edi
	callq	_ZNSolsEi
	movq	%rax, %r15
.Ltmp42:
# BB#77:
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %r12
	testq	%r12, %r12
	je	.LBB1_78
# BB#83:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit.i.i112
	cmpb	$0, 56(%r12)
	je	.LBB1_85
# BB#84:
	movb	67(%r12), %al
	jmp	.LBB1_87
.LBB1_85:
.Ltmp43:
	movq	%r12, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp44:
# BB#86:                                # %.noexc116
	movq	(%r12), %rax
.Ltmp45:
	movl	$10, %esi
	movq	%r12, %rdi
	callq	*48(%rax)
.Ltmp46:
.LBB1_87:                               # %_ZNKSt9basic_iosIcSt11char_traitsIcEE5widenEc.exit.i114
.Ltmp47:
	movsbl	%al, %esi
	movq	%r15, %rdi
	callq	_ZNSo3putEc
.Ltmp48:
# BB#88:                                # %.noexc118
.Ltmp49:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp50:
# BB#89:                                # %_ZNSolsEPFRSoS_E.exit111
	movq	32(%rsp), %rsi
	movq	%r13, %rax
	cmpq	%rax, %rsi
	je	.LBB1_90
# BB#91:
	leaq	8(%rsp), %rdi
	leaq	32(%rsp), %rdx
	callq	_ZNSt8__detail15_List_node_base11_M_transferEPS0_S1_
	movq	24(%rsp), %rsi
	addq	48(%rsp), %rsi
	movq	%rsi, 24(%rsp)
	movq	$0, 48(%rsp)
	jmp	.LBB1_92
.LBB1_90:                               # %_ZNSolsEPFRSoS_E.exit111._crit_edge
	movq	24(%rsp), %rsi
.LBB1_92:
.Ltmp51:
	movl	$_ZSt4cout, %edi
	callq	_ZNSo9_M_insertImEERSoT_
	movq	%rax, %r15
.Ltmp52:
# BB#93:                                # %_ZNSolsEm.exit
.Ltmp53:
	movl	$.L.str, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp54:
# BB#94:
	movq	16(%rsp), %rax
	movl	16(%rax), %esi
.Ltmp55:
	movq	%r15, %rdi
	callq	_ZNSolsEi
	movq	%rax, %r15
.Ltmp56:
# BB#95:
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %r12
	testq	%r12, %r12
	je	.LBB1_96
# BB#98:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit.i.i
	cmpb	$0, 56(%r12)
	je	.LBB1_100
# BB#99:
	movb	67(%r12), %al
	jmp	.LBB1_102
.LBB1_100:
.Ltmp57:
	movq	%r12, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp58:
# BB#101:                               # %.noexc43
	movq	(%r12), %rax
.Ltmp59:
	movl	$10, %esi
	movq	%r12, %rdi
	callq	*48(%rax)
.Ltmp60:
.LBB1_102:                              # %_ZNKSt9basic_iosIcSt11char_traitsIcEE5widenEc.exit.i
.Ltmp61:
	movsbl	%al, %esi
	movq	%r15, %rdi
	callq	_ZNSo3putEc
.Ltmp62:
# BB#103:                               # %.noexc45
.Ltmp63:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp64:
# BB#104:                               # %_ZNSolsEPFRSoS_E.exit
	movq	8(%rsp), %rdi
	cmpq	%r14, %rdi
	je	.LBB1_106
	.p2align	4, 0x90
.LBB1_105:                              # %.lr.ph.i.i41
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	_ZdlPv
	cmpq	%r14, %rbx
	movq	%rbx, %rdi
	jne	.LBB1_105
.LBB1_106:                              # %_ZNSt7__cxx1110_List_baseIiSaIiEED2Ev.exit42
	movq	32(%rsp), %rdi
	cmpq	%r13, %rdi
	je	.LBB1_108
	.p2align	4, 0x90
.LBB1_107:                              # %.lr.ph.i.i38
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	_ZdlPv
	cmpq	%r13, %rbx
	movq	%rbx, %rdi
	jne	.LBB1_107
.LBB1_108:                              # %_ZNSt7__cxx1110_List_baseIiSaIiEED2Ev.exit39
	xorl	%eax, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_25:
.Ltmp73:
	callq	_ZSt16__throw_bad_castv
.Ltmp74:
# BB#31:                                # %.noexc65
.LBB1_45:
.Ltmp70:
	callq	_ZSt16__throw_bad_castv
.Ltmp71:
# BB#46:                                # %.noexc82
.LBB1_78:
.Ltmp67:
	callq	_ZSt16__throw_bad_castv
.Ltmp68:
# BB#82:                                # %.noexc115
.LBB1_96:
.Ltmp65:
	callq	_ZSt16__throw_bad_castv
.Ltmp66:
# BB#97:                                # %.noexc
.LBB1_79:
.Ltmp40:
	movq	%rax, %r15
	movq	56(%rsp), %rdi
	cmpq	%r12, %rdi
	je	.LBB1_111
.LBB1_80:                               # %.lr.ph.i.i107
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%r12, %rbp
	movq	%rbp, %rdi
	jne	.LBB1_80
	jmp	.LBB1_111
.LBB1_68:
.Ltmp11:
	movq	%rax, %r15
	movq	80(%rsp), %rdi
	cmpq	%r12, %rdi
	je	.LBB1_111
.LBB1_69:                               # %.lr.ph.i.i90
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%r12, %rbp
	movq	%rbp, %rdi
	jne	.LBB1_69
	jmp	.LBB1_111
.LBB1_70:
.Ltmp72:
	jmp	.LBB1_110
.LBB1_109:
.Ltmp75:
	jmp	.LBB1_110
.LBB1_81:
.Ltmp69:
	jmp	.LBB1_110
.LBB1_71:
.Ltmp34:
	jmp	.LBB1_110
.LBB1_72:
.Ltmp37:
	movq	%rax, %r15
	movq	56(%rsp), %rdi
	cmpq	%r12, %rdi
	je	.LBB1_111
	.p2align	4, 0x90
.LBB1_73:                               # %.lr.ph.i.i.i96
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%r12, %rbp
	movq	%rbp, %rdi
	jne	.LBB1_73
	jmp	.LBB1_111
.LBB1_28:
.Ltmp5:
.LBB1_110:                              # %_ZNSt7__cxx1110_List_baseIiSaIiEED2Ev.exit91
	movq	%rax, %r15
	jmp	.LBB1_111
.LBB1_29:
.Ltmp8:
	movq	%rax, %r15
	movq	80(%rsp), %rdi
	cmpq	%r12, %rdi
	je	.LBB1_111
	.p2align	4, 0x90
.LBB1_30:                               # %.lr.ph.i.i.i51
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%r12, %rbp
	movq	%rbp, %rdi
	jne	.LBB1_30
.LBB1_111:                              # %_ZNSt7__cxx1110_List_baseIiSaIiEED2Ev.exit91
	movq	8(%rsp), %rdi
	cmpq	%r14, %rdi
	je	.LBB1_113
	.p2align	4, 0x90
.LBB1_112:                              # %.lr.ph.i.i34
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%r14, %rbp
	movq	%rbp, %rdi
	jne	.LBB1_112
	jmp	.LBB1_113
.LBB1_26:
.Ltmp2:
	movq	%rax, %r15
	movq	8(%rsp), %rdi
	cmpq	%r14, %rdi
	je	.LBB1_113
	.p2align	4, 0x90
.LBB1_27:                               # %.lr.ph.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%r14, %rbp
	movq	%rbp, %rdi
	jne	.LBB1_27
.LBB1_113:
	movq	32(%rsp), %rdi
	cmpq	%rbx, %rdi
	je	.LBB1_115
	.p2align	4, 0x90
.LBB1_114:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%rbx, %rbp
	movq	%rbp, %rdi
	jne	.LBB1_114
.LBB1_115:                              # %_ZNSt7__cxx1110_List_baseIiSaIiEED2Ev.exit
	movq	%r15, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\271\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\266\001"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp21-.Ltmp12         #   Call between .Ltmp12 and .Ltmp21
	.long	.Ltmp75-.Lfunc_begin0   #     jumps to .Ltmp75
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp31-.Ltmp22         #   Call between .Ltmp22 and .Ltmp31
	.long	.Ltmp72-.Lfunc_begin0   #     jumps to .Ltmp72
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin0   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin0   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin0   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp64-.Ltmp41         #   Call between .Ltmp41 and .Ltmp64
	.long	.Ltmp69-.Lfunc_begin0   #     jumps to .Ltmp69
	.byte	0                       #   On action: cleanup
	.long	.Ltmp73-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp74-.Ltmp73         #   Call between .Ltmp73 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin0   #     jumps to .Ltmp75
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp72-.Lfunc_begin0   #     jumps to .Ltmp72
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp66-.Ltmp67         #   Call between .Ltmp67 and .Ltmp66
	.long	.Ltmp69-.Lfunc_begin0   #     jumps to .Ltmp69
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Lfunc_end1-.Ltmp66     #   Call between .Ltmp66 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_lists1.ii,@function
_GLOBAL__sub_I_lists1.ii:               # @_GLOBAL__sub_I_lists1.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end2:
	.size	_GLOBAL__sub_I_lists1.ii, .Lfunc_end2-_GLOBAL__sub_I_lists1.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" "
	.size	.L.str, 2

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"false"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"true"
	.size	.L.str.2, 5

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_lists1.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
