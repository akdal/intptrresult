	.text
	.file	"emfloat.bc"
	.globl	SetupCPUEmFloatArrays
	.p2align	4, 0x90
	.type	SetupCPUEmFloatArrays,@function
SetupCPUEmFloatArrays:                  # @SetupCPUEmFloatArrays
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %r15
	movl	$13, %edi
	callq	randnum
	testq	%rbx, %rbx
	je	.LBB0_33
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	leaq	16(%rsp), %r12
	movq	%rsp, %r13
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_9 Depth 2
                                        #     Child Loop BB0_19 Depth 2
                                        #     Child Loop BB0_29 Depth 2
	movl	$50000, %edi            # imm = 0xC350
	callq	randwc
	testl	%eax, %eax
	js	.LBB0_3
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	movb	$0, 17(%rsp)
	jmp	.LBB0_5
	.p2align	4, 0x90
.LBB0_3:                                #   in Loop: Header=BB0_2 Depth=1
	movb	$1, 17(%rsp)
	negl	%eax
.LBB0_5:                                #   in Loop: Header=BB0_2 Depth=1
	movb	$2, 16(%rsp)
	movq	$0, 20(%rsp)
	testl	%eax, %eax
	je	.LBB0_6
# BB#7:                                 #   in Loop: Header=BB0_2 Depth=1
	movw	$32, 18(%rsp)
	movl	%eax, %edx
	shrl	$16, %edx
	movw	%dx, 20(%rsp)
	movw	%ax, 22(%rsp)
	testw	%dx, %dx
	js	.LBB0_12
# BB#8:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movw	$32, %cx
	.p2align	4, 0x90
.LBB0_9:                                # %.lr.ph.i.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%ax, %si
	leal	(%rsi,%rsi), %eax
	andl	$32768, %esi            # imm = 0x8000
	addl	%edx, %edx
	movl	%edx, %edi
	shrl	$15, %esi
	decl	%ecx
	movw	%si, %dx
	orw	%di, %dx
	jns	.LBB0_9
# BB#10:                                # %._crit_edge.i.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movw	$0, 26(%rsp)
	movw	$0, 24(%rsp)
	movw	%ax, 22(%rsp)
	movw	%dx, 20(%rsp)
	jmp	.LBB0_11
	.p2align	4, 0x90
.LBB0_6:                                #   in Loop: Header=BB0_2 Depth=1
	movb	$0, 16(%rsp)
	xorl	%ecx, %ecx
.LBB0_11:                               # %Int32ToInternalFPF.exit.sink.split
                                        #   in Loop: Header=BB0_2 Depth=1
	movw	%cx, 18(%rsp)
.LBB0_12:                               # %Int32ToInternalFPF.exit
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	$50000, %edi            # imm = 0xC350
	callq	randwc
	cmpl	$-2, %eax
	jg	.LBB0_14
# BB#13:                                #   in Loop: Header=BB0_2 Depth=1
	movb	$1, 1(%rsp)
	notl	%eax
	jmp	.LBB0_15
	.p2align	4, 0x90
.LBB0_14:                               #   in Loop: Header=BB0_2 Depth=1
	incl	%eax
	movb	$0, 1(%rsp)
.LBB0_15:                               #   in Loop: Header=BB0_2 Depth=1
	movb	$2, (%rsp)
	movq	$0, 4(%rsp)
	testl	%eax, %eax
	je	.LBB0_16
# BB#17:                                #   in Loop: Header=BB0_2 Depth=1
	movw	$32, 2(%rsp)
	movl	%eax, %edx
	shrl	$16, %edx
	movw	%dx, 4(%rsp)
	movw	%ax, 6(%rsp)
	testw	%dx, %dx
	js	.LBB0_22
# BB#18:                                # %.lr.ph.i.i9.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movw	$32, %cx
	.p2align	4, 0x90
.LBB0_19:                               # %.lr.ph.i.i9
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%ax, %si
	leal	(%rsi,%rsi), %eax
	andl	$32768, %esi            # imm = 0x8000
	addl	%edx, %edx
	movl	%edx, %edi
	shrl	$15, %esi
	decl	%ecx
	movw	%si, %dx
	orw	%di, %dx
	jns	.LBB0_19
# BB#20:                                # %._crit_edge.i.i12
                                        #   in Loop: Header=BB0_2 Depth=1
	movw	$0, 10(%rsp)
	movw	$0, 8(%rsp)
	movw	%ax, 6(%rsp)
	movw	%dx, 4(%rsp)
	jmp	.LBB0_21
	.p2align	4, 0x90
.LBB0_16:                               #   in Loop: Header=BB0_2 Depth=1
	movb	$0, (%rsp)
	xorl	%ecx, %ecx
.LBB0_21:                               # %Int32ToInternalFPF.exit13.sink.split
                                        #   in Loop: Header=BB0_2 Depth=1
	movw	%cx, 2(%rsp)
.LBB0_22:                               # %Int32ToInternalFPF.exit13
                                        #   in Loop: Header=BB0_2 Depth=1
	leaq	(%rbp,%rbp,2), %r14
	leaq	(%r15,%r14,4), %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	DivideInternalFPF
	movl	$50000, %edi            # imm = 0xC350
	callq	randwc
	cmpl	$-2, %eax
	jg	.LBB0_24
# BB#23:                                #   in Loop: Header=BB0_2 Depth=1
	movb	$1, 1(%rsp)
	notl	%eax
	jmp	.LBB0_25
	.p2align	4, 0x90
.LBB0_24:                               #   in Loop: Header=BB0_2 Depth=1
	incl	%eax
	movb	$0, 1(%rsp)
.LBB0_25:                               #   in Loop: Header=BB0_2 Depth=1
	movb	$2, (%rsp)
	movq	$0, 4(%rsp)
	testl	%eax, %eax
	je	.LBB0_26
# BB#27:                                #   in Loop: Header=BB0_2 Depth=1
	movw	$32, 2(%rsp)
	movl	%eax, %edx
	shrl	$16, %edx
	movw	%dx, 4(%rsp)
	movw	%ax, 6(%rsp)
	testw	%dx, %dx
	js	.LBB0_32
# BB#28:                                # %.lr.ph.i.i16.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movw	$32, %cx
	.p2align	4, 0x90
.LBB0_29:                               # %.lr.ph.i.i16
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%ax, %si
	leal	(%rsi,%rsi), %eax
	andl	$32768, %esi            # imm = 0x8000
	addl	%edx, %edx
	movl	%edx, %edi
	shrl	$15, %esi
	decl	%ecx
	movw	%si, %dx
	orw	%di, %dx
	jns	.LBB0_29
# BB#30:                                # %._crit_edge.i.i19
                                        #   in Loop: Header=BB0_2 Depth=1
	movw	$0, 10(%rsp)
	movw	$0, 8(%rsp)
	movw	%ax, 6(%rsp)
	movw	%dx, 4(%rsp)
	jmp	.LBB0_31
	.p2align	4, 0x90
.LBB0_26:                               #   in Loop: Header=BB0_2 Depth=1
	movb	$0, (%rsp)
	xorl	%ecx, %ecx
.LBB0_31:                               # %Int32ToInternalFPF.exit20.sink.split
                                        #   in Loop: Header=BB0_2 Depth=1
	movw	%cx, 2(%rsp)
.LBB0_32:                               # %Int32ToInternalFPF.exit20
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r14,4), %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	DivideInternalFPF
	incq	%rbp
	cmpq	%rbx, %rbp
	jne	.LBB0_2
.LBB0_33:                               # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	SetupCPUEmFloatArrays, .Lfunc_end0-SetupCPUEmFloatArrays
	.cfi_endproc

	.p2align	4, 0x90
	.type	DivideInternalFPF,@function
DivideInternalFPF:                      # @DivideInternalFPF
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 96
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movzbl	(%rdi), %eax
	leal	(%rax,%rax,4), %ebp
	movzbl	(%rsi), %ecx
	addl	%ebp, %ecx
	cmpl	$24, %ecx
	ja	.LBB1_38
# BB#1:
	jmpq	*.LJTI1_0(,%rcx,8)
.LBB1_37:
	movq	(%rdx), %rax
	movl	8(%rdx), %ecx
	movl	%ecx, 8(%rsi)
	movq	%rax, (%rsi)
	jmp	.LBB1_38
.LBB1_6:
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movzwl	4(%rdi), %ebp
	movzwl	6(%rdi), %ecx
	movzwl	8(%rdi), %r15d
	movw	10(%rdi), %dx
	movl	%ecx, %ebx
	movl	%ebp, %r8d
	orl	%ebp, %ebx
	orl	%r15d, %ebx
	movzwl	6(%rsi), %ebp
	orw	4(%rsi), %bp
	orw	8(%rsi), %bp
	orw	10(%rsi), %bp
	orw	%dx, %bx
	je	.LBB1_7
# BB#13:
	testw	%bp, %bp
	je	.LBB1_14
# BB#15:                                # %.lr.ph
	movq	8(%rsp), %rbp           # 8-byte Reload
	movb	%al, (%rbp)
	movb	1(%rsi), %al
	xorb	1(%rdi), %al
	movb	%al, 1(%rbp)
	movzwl	2(%rdi), %r10d
	movzwl	2(%rsi), %eax
	movl	%r10d, %edi
	subl	$-128, %edi
	subl	%eax, %edi
	movw	%di, 2(%rbp)
	movq	$0, 4(%rbp)
	addl	$127, %r10d
	subl	%eax, %r10d
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	xorl	%edi, %edi
	xorl	%r12d, %r12d
	xorl	%r11d, %r11d
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movl	%r8d, %ebx
	jmp	.LBB1_16
.LBB1_19:                               # %.preheader117.1129
                                        #   in Loop: Header=BB1_16 Depth=1
	cmpw	%r11w, 6(%rsi)
	ja	.LBB1_18
# BB#20:                                #   in Loop: Header=BB1_16 Depth=1
	jb	.LBB1_25
# BB#21:                                # %.preheader117.2130
                                        #   in Loop: Header=BB1_16 Depth=1
	cmpw	%r13w, 8(%rsi)
	ja	.LBB1_18
# BB#22:                                #   in Loop: Header=BB1_16 Depth=1
	jb	.LBB1_25
# BB#23:                                # %.preheader117.3131
                                        #   in Loop: Header=BB1_16 Depth=1
	movl	20(%rsp), %ebp          # 4-byte Reload
	cmpw	%bp, 10(%rsi)
	ja	.LBB1_18
	jmp	.LBB1_25
	.p2align	4, 0x90
.LBB1_16:                               # =>This Inner Loop Header: Depth=1
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movl	%r12d, %r8d
	leal	(%rdx,%rdx), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	shldw	$1, %r11w, %r12w
	shldw	$1, %r13w, %r11w
	shldw	$1, %bp, %r13w
	shldw	$1, %bx, %bp
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	movzwl	4(%rsi), %eax
	testw	%r8w, %r8w
	js	.LBB1_25
# BB#17:                                # %.preheader117.preheader
                                        #   in Loop: Header=BB1_16 Depth=1
	cmpw	%r12w, %ax
	jbe	.LBB1_24
.LBB1_18:                               #   in Loop: Header=BB1_16 Depth=1
	xorl	%eax, %eax
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB1_26
	.p2align	4, 0x90
.LBB1_24:                               #   in Loop: Header=BB1_16 Depth=1
	cmpw	%r12w, %ax
	jae	.LBB1_19
.LBB1_25:                               # %.preheader.preheader
                                        #   in Loop: Header=BB1_16 Depth=1
	movl	20(%rsp), %ebp          # 4-byte Reload
	movzwl	%bp, %ebp
	movl	%ebx, %r8d
	movq	%rsi, %rbx
	movzwl	10(%rbx), %esi
	subl	%esi, %ebp
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	movl	%ebp, %esi
	shrl	$16, %esi
	andl	$1, %esi
	movzwl	%r13w, %r13d
	movzwl	8(%rbx), %ebp
	subl	%ebp, %r13d
	subl	%esi, %r13d
	movl	%r13d, %esi
	shrl	$16, %esi
	andl	$1, %esi
	movzwl	%r11w, %r11d
	movzwl	6(%rbx), %ebp
	movl	%r8d, %ebx
	subl	%ebp, %r11d
	movq	8(%rsp), %rbp           # 8-byte Reload
	subl	%esi, %r11d
	movl	%r11d, %esi
	shrl	$16, %esi
	andl	$1, %esi
	subl	%eax, %r12d
	subl	%esi, %r12d
	movw	$1, %ax
.LBB1_26:                               # %.loopexit
                                        #   in Loop: Header=BB1_16 Depth=1
	shrdw	$15, %r15w, %dx
	shrdw	$15, %cx, %r15w
	shldw	$1, %cx, %bx
	movzwl	%ax, %esi
	leal	(%rsi,%rdi,2), %esi
	movw	%si, 10(%rbp)
	shrdw	$15, %r14w, %di
	movw	%di, 8(%rbp)
	movl	28(%rsp), %eax          # 4-byte Reload
	shrdw	$15, %ax, %r14w
	movw	%r14w, 6(%rbp)
	shldw	$1, %ax, %r9w
	movw	%r9w, 4(%rbp)
	movw	%r10w, 2(%rbp)
	decl	%r10d
	testw	%r9w, %r9w
	movw	%r14w, %ax
	movw	%di, %r14w
	movw	%si, %di
	movw	%r15w, %cx
	movw	%dx, %r15w
	movl	24(%rsp), %edx          # 4-byte Reload
                                        # kill: %DX<def> %DX<kill> %EDX<kill> %RDX<def>
	movq	32(%rsp), %rsi          # 8-byte Reload
	movl	20(%rsp), %ebp          # 4-byte Reload
	jns	.LBB1_16
# BB#27:
	movq	8(%rsp), %rdx           # 8-byte Reload
	jmp	.LBB1_38
.LBB1_2:
	movzwl	6(%rsi), %eax
	orw	4(%rsi), %ax
	orw	8(%rsi), %ax
	orw	10(%rsi), %ax
	je	.LBB1_9
.LBB1_3:
	movb	1(%rsi), %al
	xorb	1(%rdi), %al
	movb	$0, (%rdx)
	movb	%al, 1(%rdx)
	jmp	.LBB1_12
.LBB1_4:
	movzwl	6(%rdi), %eax
	orw	4(%rdi), %ax
	orw	8(%rdi), %ax
	orw	10(%rdi), %ax
	je	.LBB1_9
.LBB1_5:
	movb	$3, (%rdx)
	movb	$0, 1(%rdx)
	movw	$-32767, 2(%rdx)        # imm = 0x8001
	movq	$0, 4(%rdx)
	movb	1(%rsi), %al
	xorb	1(%rdi), %al
	movb	%al, 1(%rdx)
	jmp	.LBB1_38
.LBB1_7:
	testw	%bp, %bp
	je	.LBB1_8
# BB#10:
	movq	8(%rsp), %rdx           # 8-byte Reload
	movb	$0, (%rdx)
	jmp	.LBB1_11
.LBB1_35:
	movzwl	4(%rsi), %eax
	cmpw	%ax, 4(%rdi)
	jbe	.LBB1_36
.LBB1_34:
	movq	(%rdx), %rax
	movl	8(%rdx), %ecx
	movl	%ecx, 8(%rdi)
	movq	%rax, (%rdi)
	jmp	.LBB1_38
.LBB1_14:
	movq	8(%rsp), %rdx           # 8-byte Reload
	movb	$3, (%rdx)
.LBB1_11:                               # %choose_nan.exit
	movb	$0, 1(%rdx)
.LBB1_12:                               # %choose_nan.exit
	movw	$-32767, 2(%rdx)        # imm = 0x8001
	movq	$0, 4(%rdx)
	jmp	.LBB1_38
.LBB1_8:
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB1_9:
	movb	$4, (%rdx)
	movw	$32767, 2(%rdx)         # imm = 0x7FFF
	movb	$1, 1(%rdx)
	movw	$16384, 4(%rdx)         # imm = 0x4000
	movw	$0, 10(%rdx)
	movl	$0, 6(%rdx)
.LBB1_38:                               # %choose_nan.exit
	movb	(%rdx), %al
	decb	%al
	cmpb	$1, %al
	ja	.LBB1_47
# BB#39:
	movzwl	6(%rdx), %eax
	orw	4(%rdx), %ax
	orw	8(%rdx), %ax
	orw	10(%rdx), %ax
	jne	.LBB1_41
# BB#40:
	movl	$.Lstr, %edi
	movq	%rdx, %rbx
	callq	puts
	movq	%rbx, %rdx
.LBB1_41:
	movzwl	2(%rdx), %eax
	cmpl	$32768, %eax            # imm = 0x8000
	jne	.LBB1_44
# BB#42:
	movw	$-32767, 2(%rdx)        # imm = 0x8001
	cmpb	$0, (%rdx)
	je	.LBB1_47
# BB#43:                                # %denormalize.exit.thread6.i
	movzwl	4(%rdx), %ecx
	movzwl	6(%rdx), %ebx
	movzwl	8(%rdx), %esi
	movzwl	10(%rdx), %eax
	movl	%ecx, %edi
	shrl	%edi
	shldw	$15, %bx, %cx
	shldw	$15, %si, %bx
	movl	%eax, %ebp
	andl	$1, %ebp
	shrdw	$1, %si, %ax
	orl	%ebp, %eax
	movw	%di, 4(%rdx)
	movw	%cx, 6(%rdx)
	movw	%bx, 8(%rdx)
	movw	%ax, 10(%rdx)
	jmp	.LBB1_46
.LBB1_44:                               # %denormalize.exit.i
	cmpb	$0, (%rdx)
	je	.LBB1_47
# BB#45:                                # %denormalize.exit.i._crit_edge
	movw	10(%rdx), %ax
.LBB1_46:
	andl	$65528, %eax            # imm = 0xFFF8
	movw	%ax, 10(%rdx)
.LBB1_47:                               # %RoundInternalFPF.exit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_36:
	jb	.LBB1_37
# BB#28:
	movzwl	6(%rsi), %eax
	cmpw	%ax, 6(%rdi)
	ja	.LBB1_34
# BB#29:
	jb	.LBB1_37
# BB#30:
	movzwl	8(%rsi), %eax
	cmpw	%ax, 8(%rdi)
	ja	.LBB1_34
# BB#31:
	jb	.LBB1_37
# BB#32:
	movzwl	10(%rsi), %eax
	cmpw	%ax, 10(%rdi)
	ja	.LBB1_34
# BB#33:
	jb	.LBB1_37
	jmp	.LBB1_34
.Lfunc_end1:
	.size	DivideInternalFPF, .Lfunc_end1-DivideInternalFPF
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_9
	.quad	.LBB1_2
	.quad	.LBB1_2
	.quad	.LBB1_3
	.quad	.LBB1_37
	.quad	.LBB1_4
	.quad	.LBB1_6
	.quad	.LBB1_6
	.quad	.LBB1_3
	.quad	.LBB1_37
	.quad	.LBB1_4
	.quad	.LBB1_6
	.quad	.LBB1_6
	.quad	.LBB1_3
	.quad	.LBB1_37
	.quad	.LBB1_5
	.quad	.LBB1_5
	.quad	.LBB1_5
	.quad	.LBB1_9
	.quad	.LBB1_37
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_35

	.text
	.globl	DoEmFloatIteration
	.p2align	4, 0x90
	.type	DoEmFloatIteration,@function
DoEmFloatIteration:                     # @DoEmFloatIteration
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 176
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%r8, %rbx
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %rbp
	movq	%rdi, %r14
	xorl	%eax, %eax
	callq	StartStopwatch
	movq	%r14, %r9
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rbx, %r14
	testq	%rbx, %rbx
	je	.LBB2_52
# BB#1:
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%r15, 88(%rsp)          # 8-byte Spill
	movq	%r12, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_3:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_5 Depth 2
                                        #       Child Loop BB2_21 Depth 3
                                        #       Child Loop BB2_26 Depth 3
	decq	%r14
	testq	%r15, %r15
	je	.LBB2_2
# BB#4:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	xorl	%r10d, %r10d
	movq	%r14, 64(%rsp)          # 8-byte Spill
	jmp	.LBB2_5
.LBB2_15:                               #   in Loop: Header=BB2_5 Depth=2
	movw	4(%rbp,%rcx), %di
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movw	6(%rbp,%rcx), %di
	movl	%edi, 16(%rsp)          # 4-byte Spill
	movw	8(%rbp,%rcx), %r13w
	movw	10(%rbp,%rcx), %r11w
	movzwl	6(%r9,%rcx), %edi
	orw	4(%r9,%rcx), %di
	orw	8(%r9,%rcx), %di
	orw	10(%r9,%rcx), %di
	je	.LBB2_17
# BB#16:                                #   in Loop: Header=BB2_5 Depth=2
	movl	16(%rsp), %edi          # 4-byte Reload
	orl	12(%rsp), %edi          # 4-byte Folded Reload
	movl	%r13d, %ebp
	orl	%edi, %ebp
	orw	%r11w, %bp
	movq	48(%rsp), %rbp          # 8-byte Reload
	jne	.LBB2_18
.LBB2_17:                               #   in Loop: Header=BB2_5 Depth=2
	movb	$3, (%r8)
	leaq	(%r10,%r10,2), %rsi
	movb	$0, 1(%r12,%rsi,4)
	movw	$-32767, 2(%r12,%rsi,4) # imm = 0x8001
	movq	$0, 4(%r12,%rsi,4)
	movb	(%rdx), %sil
.LBB2_18:                               #   in Loop: Header=BB2_5 Depth=2
	movq	%r10, 104(%rsp)         # 8-byte Spill
	movb	$1, %dl
	cmpb	$1, %sil
	je	.LBB2_20
# BB#19:                                #   in Loop: Header=BB2_5 Depth=2
	cmpb	$1, (%rax)
	setne	%dl
	incb	%dl
.LBB2_20:                               # %.preheader118.i
                                        #   in Loop: Header=BB2_5 Depth=2
	leaq	4(%r9,%rcx), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	%r8, 96(%rsp)           # 8-byte Spill
	movb	%dl, (%r8)
	movb	1(%rbp,%rcx), %al
	xorb	1(%r9,%rcx), %al
	movb	%al, 1(%r12,%rcx)
	movw	2(%rbp,%rcx), %dx
	addw	2(%r9,%rcx), %dx
	leaq	2(%r12,%rcx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%edx, 56(%rsp)          # 4-byte Spill
	movw	%dx, 2(%r12,%rcx)
	movq	$0, 4(%r12,%rcx)
	movl	$64, 60(%rsp)           # 4-byte Folded Spill
	xorl	%r10d, %r10d
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	xorl	%esi, %esi
	movl	$0, 32(%rsp)            # 4-byte Folded Spill
	movl	$0, 36(%rsp)            # 4-byte Folded Spill
	movl	%r11d, %r12d
	movl	%r13d, %eax
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	12(%rsp), %r13d         # 4-byte Reload
	.p2align	4, 0x90
.LBB2_21:                               #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%edx, %r14d
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movl	%r13d, %edx
	andl	$65534, %edx            # imm = 0xFFFE
	shrl	%edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	testb	$1, %r12b
	jne	.LBB2_23
# BB#22:                                #   in Loop: Header=BB2_21 Depth=3
	xorl	%ebp, %ebp
	movq	40(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB2_24
	.p2align	4, 0x90
.LBB2_23:                               # %.preheader117.preheader.i
                                        #   in Loop: Header=BB2_21 Depth=3
	movzwl	%di, %edx
	movq	112(%rsp), %rbp         # 8-byte Reload
	movzwl	6(%rbp), %edi
	addl	%edx, %edi
	movl	%edi, %edx
	shrl	$16, %edx
	movq	40(%rsp), %rsi          # 8-byte Reload
	movw	%di, 8(%rsi)
	movzwl	%cx, %ecx
	movl	%eax, %r8d
	movzwl	4(%rbp), %eax
	addl	%edx, %ecx
	addl	%eax, %ecx
	movl	%ecx, %edx
	shrl	$16, %edx
	andl	$1, %edx
	movw	%cx, 6(%rsi)
	movzwl	%bx, %eax
	movzwl	2(%rbp), %ebx
	addl	%eax, %ebx
	addl	%edx, %ebx
	movl	%ebx, %edx
	shrl	$16, %edx
	andl	$1, %edx
	movw	%bx, 4(%rsi)
	movzwl	%r10w, %eax
	movzwl	(%rbp), %r10d
	addl	%eax, %r10d
	movl	%r8d, %eax
	addl	%edx, %r10d
	movl	%r10d, %ebp
	shrl	$16, %ebp
	andl	$1, %ebp
	movw	%r10w, 2(%rsi)
.LBB2_24:                               # %.loopexit.i
                                        #   in Loop: Header=BB2_21 Depth=3
	movl	%edi, %r15d
	movl	%ecx, %edi
	movl	%ebx, %ecx
	movl	%r13d, %edx
	shldw	$15, %r14w, %dx
	movl	%r14d, %r13d
	shldw	$15, %ax, %r13w
	shrdw	$1, %ax, %r12w
	movl	%r10d, %r8d
	andl	$65534, %r8d            # imm = 0xFFFE
	shrl	%r8d
	movw	%r10w, %bx
	movl	%r8d, %eax
	orl	$32768, %eax            # imm = 0x8000
	movl	%ebp, %r9d
	testw	%bp, %bp
	cmovew	%r8w, %ax
	movw	%ax, 2(%rsi)
	shldw	$15, %cx, %bx
	movw	%bx, 4(%rsi)
                                        # kill: %CX<def> %CX<kill> %ECX<def>
	shldw	$15, %di, %cx
	movw	%cx, 6(%rsi)
                                        # kill: %DI<def> %DI<kill> %EDI<def>
	shldw	$15, %r15w, %di
	movw	%di, 8(%rsi)
	movl	36(%rsp), %esi          # 4-byte Reload
	shldw	$15, %si, %r15w
	movw	%si, %r14w
	movl	32(%rsp), %esi          # 4-byte Reload
	shldw	$15, %si, %r14w
	movw	%si, %r11w
	movl	16(%rsp), %ebp          # 4-byte Reload
	shldw	$15, %bp, %r11w
	movl	20(%rsp), %esi          # 4-byte Reload
	shrdw	$1, %bp, %si
	movl	%esi, 20(%rsp)          # 4-byte Spill
	decl	60(%rsp)                # 4-byte Folded Spill
	movw	%ax, %r10w
	movw	%r11w, %si
	movw	%r14w, %ax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movw	%r15w, %ax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movw	%r13w, %ax
                                        # kill: %DX<def> %DX<kill> %EDX<def>
	movl	12(%rsp), %r13d         # 4-byte Reload
                                        # kill: %R13W<def> %R13W<kill> %R13D<kill> %R13D<def>
	jne	.LBB2_21
# BB#25:                                # %.preheader.i
                                        #   in Loop: Header=BB2_5 Depth=2
	testw	%r9w, %r9w
	movq	104(%rsp), %r10         # 8-byte Reload
	movl	56(%rsp), %ebp          # 4-byte Reload
	movl	20(%rsp), %r9d          # 4-byte Reload
	jne	.LBB2_28
	.p2align	4, 0x90
.LBB2_26:                               # %.lr.ph.i
                                        #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movw	%r9w, %ax
	movl	%ecx, %edx
	movl	%edi, %ecx
	movl	%r15d, %esi
	movl	%r14d, %r15d
	movl	%r11d, %edi
	movw	%ax, %r11w
	leal	(%rax,%rax), %r9d
	shrdw	$15, %di, %r11w
	movw	%di, %r14w
	shrdw	$15, %r15w, %r14w
	shrdw	$15, %si, %r15w
	movw	%si, %di
	shrdw	$15, %cx, %di
                                        # kill: %CX<def> %CX<kill> %ECX<def>
	shrdw	$15, %dx, %cx
	movl	%ebx, %eax
	andl	$32768, %eax            # imm = 0x8000
	shldw	$1, %dx, %bx
	addl	%r8d, %r8d
	movl	%r8d, %edx
	shrl	$15, %eax
	decl	%ebp
	movw	%ax, %r8w
	orw	%dx, %r8w
	jns	.LBB2_26
# BB#27:                                # %._crit_edge.i
                                        #   in Loop: Header=BB2_5 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	movw	%di, 8(%rax)
	movw	%cx, 6(%rax)
	movw	%bx, 4(%rax)
	movw	%r8w, 2(%rax)
	movw	%bp, (%rax)
.LBB2_28:                               #   in Loop: Header=BB2_5 Depth=2
	orl	%r9d, %r11d
	orl	%r11d, %r14d
	orw	%r15w, %r14w
	movq	64(%rsp), %r14          # 8-byte Reload
	movq	88(%rsp), %r15          # 8-byte Reload
	movq	80(%rsp), %r12          # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	96(%rsp), %r8           # 8-byte Reload
	jne	.LBB2_40
# BB#29:                                #   in Loop: Header=BB2_5 Depth=2
	orl	$1, %edi
	movq	40(%rsp), %rax          # 8-byte Reload
	movw	%di, 8(%rax)
	jmp	.LBB2_40
.LBB2_13:                               #   in Loop: Header=BB2_5 Depth=2
	movq	(%r8), %rdx
	movl	8(%r8), %esi
	movl	%esi, 8(%rax)
	movq	%rdx, (%rax)
	movb	1(%r9,%rcx), %al
	xorb	%al, 1(%r12,%rcx)
	jmp	.LBB2_40
.LBB2_14:                               #   in Loop: Header=BB2_5 Depth=2
	movb	$4, (%r8)
	leaq	(%r10,%r10,2), %rax
	movw	$32767, 2(%r12,%rax,4)  # imm = 0x7FFF
	movb	$1, 1(%r12,%rax,4)
	movw	$16384, 4(%r12,%rax,4)  # imm = 0x4000
	movw	$0, 10(%r12,%rax,4)
	movl	$0, 6(%r12,%rax,4)
	jmp	.LBB2_40
.LBB2_37:                               #   in Loop: Header=BB2_5 Depth=2
	movzwl	4(%rbp,%rcx), %esi
	cmpw	%si, 4(%r9,%rcx)
	jbe	.LBB2_38
.LBB2_36:                               #   in Loop: Header=BB2_5 Depth=2
	movq	(%r8), %rax
	movl	8(%r8), %ecx
	movl	%ecx, 8(%rdx)
	movq	%rax, (%rdx)
	jmp	.LBB2_40
.LBB2_38:                               #   in Loop: Header=BB2_5 Depth=2
	jae	.LBB2_30
.LBB2_39:                               #   in Loop: Header=BB2_5 Depth=2
	movq	(%r8), %rcx
	movl	8(%r8), %edx
	movl	%edx, 8(%rax)
	movq	%rcx, (%rax)
	.p2align	4, 0x90
.LBB2_40:                               # %choose_nan.exit.i
                                        #   in Loop: Header=BB2_5 Depth=2
	movb	(%r8), %al
	decb	%al
	cmpb	$1, %al
	ja	.LBB2_51
# BB#41:                                #   in Loop: Header=BB2_5 Depth=2
	leaq	(%r10,%r10,2), %rbx
	movzwl	6(%r12,%rbx,4), %eax
	orw	4(%r12,%rbx,4), %ax
	orw	8(%r12,%rbx,4), %ax
	orw	10(%r12,%rbx,4), %ax
	jne	.LBB2_43
# BB#42:                                #   in Loop: Header=BB2_5 Depth=2
	movl	$.Lstr, %edi
	movq	%r10, %r13
	movq	%r8, %r14
	callq	puts
	movq	%r14, %r8
	movq	64(%rsp), %r14          # 8-byte Reload
	movq	%r13, %r10
	movq	24(%rsp), %r9           # 8-byte Reload
.LBB2_43:                               #   in Loop: Header=BB2_5 Depth=2
	leaq	10(%r12,%rbx,4), %rax
	movzwl	2(%r12,%rbx,4), %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	jne	.LBB2_46
# BB#44:                                #   in Loop: Header=BB2_5 Depth=2
	leaq	2(%r12,%rbx,4), %rcx
	movw	$-32767, (%rcx)         # imm = 0x8001
	cmpb	$0, (%r8)
	je	.LBB2_51
# BB#45:                                # %denormalize.exit.thread6.i.i
                                        #   in Loop: Header=BB2_5 Depth=2
	leaq	4(%r12,%rbx,4), %rdx
	movzwl	(%rdx), %esi
	movzwl	2(%rdx), %edi
	movzwl	4(%rdx), %r8d
	movzwl	(%rax), %ecx
	movl	%esi, %ebx
	shrl	%ebx
	shldw	$15, %di, %si
	shldw	$15, %r8w, %di
	movl	%ecx, %ebp
	andl	$1, %ebp
	shrdw	$1, %r8w, %cx
	orl	%ebp, %ecx
	movq	48(%rsp), %rbp          # 8-byte Reload
	movw	%bx, (%rdx)
	movw	%si, 2(%rdx)
	movw	%di, 4(%rdx)
	movw	%cx, (%rax)
	jmp	.LBB2_48
.LBB2_46:                               # %denormalize.exit.i.i
                                        #   in Loop: Header=BB2_5 Depth=2
	cmpb	$0, (%r8)
	je	.LBB2_51
# BB#47:                                # %denormalize.exit.i._crit_edge.i
                                        #   in Loop: Header=BB2_5 Depth=2
	movw	(%rax), %cx
.LBB2_48:                               #   in Loop: Header=BB2_5 Depth=2
	andl	$65528, %ecx            # imm = 0xFFF8
	movw	%cx, (%rax)
	jmp	.LBB2_51
.LBB2_30:                               #   in Loop: Header=BB2_5 Depth=2
	movzwl	6(%rbp,%rcx), %esi
	cmpw	%si, 6(%r9,%rcx)
	ja	.LBB2_36
# BB#31:                                #   in Loop: Header=BB2_5 Depth=2
	jb	.LBB2_39
# BB#32:                                #   in Loop: Header=BB2_5 Depth=2
	movzwl	8(%rbp,%rcx), %esi
	cmpw	%si, 8(%r9,%rcx)
	ja	.LBB2_36
# BB#33:                                #   in Loop: Header=BB2_5 Depth=2
	jb	.LBB2_39
# BB#34:                                #   in Loop: Header=BB2_5 Depth=2
	movzwl	10(%rbp,%rcx), %esi
	cmpw	%si, 10(%r9,%rcx)
	ja	.LBB2_36
# BB#35:                                #   in Loop: Header=BB2_5 Depth=2
	jae	.LBB2_36
	jmp	.LBB2_39
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_21 Depth 3
                                        #       Child Loop BB2_26 Depth 3
	movl	%r10d, %eax
	andl	$15, %eax
	movzbl	DoEmFloatIteration.jtable(%rax), %eax
	cmpq	$3, %rax
	ja	.LBB2_51
# BB#6:                                 # %.lr.ph
                                        #   in Loop: Header=BB2_5 Depth=2
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_7:                                #   in Loop: Header=BB2_5 Depth=2
	leaq	(,%r10,4), %rax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%r9,%rcx), %rsi
	leaq	(%rbp,%rcx), %rdx
	addq	%r12, %rcx
	xorl	%edi, %edi
	jmp	.LBB2_8
	.p2align	4, 0x90
.LBB2_9:                                #   in Loop: Header=BB2_5 Depth=2
	leaq	(,%r10,4), %rax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%r9,%rcx), %rsi
	leaq	(%rbp,%rcx), %rdx
	addq	%r12, %rcx
	movl	$1, %edi
.LBB2_8:                                # %MultiplyInternalFPF.exit
                                        #   in Loop: Header=BB2_5 Depth=2
	movq	%r10, %rbx
	callq	AddSubInternalFPF
	jmp	.LBB2_50
	.p2align	4, 0x90
.LBB2_10:                               #   in Loop: Header=BB2_5 Depth=2
	leaq	(,%r10,4), %rax
	leaq	(%rax,%rax,2), %rcx
	leaq	(%r12,%rcx), %r8
	movzbl	(%r9,%rcx), %esi
	leal	(%rsi,%rsi,4), %eax
	movzbl	(%rbp,%rcx), %edi
	addl	%eax, %edi
	cmpl	$24, %edi
	ja	.LBB2_40
# BB#11:                                #   in Loop: Header=BB2_5 Depth=2
	leaq	(%r9,%rcx), %rdx
	leaq	(%rbp,%rcx), %rax
	jmpq	*.LJTI2_1(,%rdi,8)
.LBB2_12:                               #   in Loop: Header=BB2_5 Depth=2
	movq	(%r8), %rax
	movl	8(%r8), %esi
	movl	%esi, 8(%rdx)
	movq	%rax, (%rdx)
	movb	1(%rbp,%rcx), %al
	xorb	%al, 1(%r12,%rcx)
	jmp	.LBB2_40
	.p2align	4, 0x90
.LBB2_49:                               #   in Loop: Header=BB2_5 Depth=2
	leaq	(,%r10,4), %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%r9,%rdx), %rdi
	leaq	(%rbp,%rdx), %rsi
	addq	%r12, %rdx
	movq	%r10, %rbx
	callq	DivideInternalFPF
.LBB2_50:                               # %MultiplyInternalFPF.exit
                                        #   in Loop: Header=BB2_5 Depth=2
	movq	%rbx, %r10
	movq	24(%rsp), %r9           # 8-byte Reload
.LBB2_51:                               # %MultiplyInternalFPF.exit
                                        #   in Loop: Header=BB2_5 Depth=2
	incq	%r10
	cmpq	%r15, %r10
	jne	.LBB2_5
.LBB2_2:                                # %.loopexit
                                        #   in Loop: Header=BB2_3 Depth=1
	testq	%r14, %r14
	jne	.LBB2_3
.LBB2_52:                               # %._crit_edge
	movq	72(%rsp), %rdi          # 8-byte Reload
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	StopStopwatch           # TAILCALL
.Lfunc_end2:
	.size	DoEmFloatIteration, .Lfunc_end2-DoEmFloatIteration
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_7
	.quad	.LBB2_9
	.quad	.LBB2_10
	.quad	.LBB2_49
.LJTI2_1:
	.quad	.LBB2_12
	.quad	.LBB2_12
	.quad	.LBB2_12
	.quad	.LBB2_14
	.quad	.LBB2_39
	.quad	.LBB2_13
	.quad	.LBB2_15
	.quad	.LBB2_15
	.quad	.LBB2_13
	.quad	.LBB2_39
	.quad	.LBB2_13
	.quad	.LBB2_15
	.quad	.LBB2_15
	.quad	.LBB2_13
	.quad	.LBB2_39
	.quad	.LBB2_14
	.quad	.LBB2_12
	.quad	.LBB2_12
	.quad	.LBB2_12
	.quad	.LBB2_39
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_36
	.quad	.LBB2_37

	.text
	.p2align	4, 0x90
	.type	AddSubInternalFPF,@function
AddSubInternalFPF:                      # @AddSubInternalFPF
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 96
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movzbl	(%rsi), %eax
	leal	(%rax,%rax,4), %ecx
	movzbl	(%rdx), %eax
	addl	%ecx, %eax
	cmpl	$24, %eax
	ja	.LBB3_56
# BB#1:
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_48:
	movq	(%r15), %rax
	movl	8(%r15), %ecx
	movl	%ecx, 8(%rsi)
	movq	%rax, (%rsi)
	jmp	.LBB3_56
.LBB3_4:
	movq	(%r15), %rax
	movl	8(%r15), %ecx
	movl	%ecx, 8(%rdx)
	movq	%rax, (%rdx)
	xorb	%dil, 1(%r15)
	jmp	.LBB3_56
.LBB3_5:
	movl	8(%rsi), %eax
	movl	%eax, 32(%rsp)
	movq	(%rsi), %rax
	movq	%rax, 24(%rsp)
	movl	8(%rdx), %eax
	movl	%eax, 16(%rsp)
	movq	(%rdx), %rax
	movq	%rax, 8(%rsp)
	movw	26(%rsp), %r14w
	movswl	%r14w, %r11d
	movzwl	10(%rsp), %r9d
	movswl	%r9w, %r10d
	movl	%r11d, %ebx
	subl	%r10d, %ebx
	je	.LBB3_6
# BB#10:
	testl	%ebx, %ebx
	jle	.LBB3_22
# BB#11:
	cmpb	$0, 8(%rsp)
	je	.LBB3_21
# BB#12:
	cmpl	$64, %ebx
	jl	.LBB3_14
# BB#13:                                # %.preheader.preheader.i
	leaq	12(%rsp), %rax
	movw	$0, 4(%rax)
	movl	$0, (%rax)
	movw	$1, 18(%rsp)
	jmp	.LBB3_21
.LBB3_6:
	movb	24(%rsp), %r8b
	movb	$1, %al
	cmpb	$1, %r8b
	je	.LBB3_8
# BB#7:
	cmpb	$1, 8(%rsp)
	setne	%al
	incb	%al
.LBB3_8:
	movb	%al, (%r15)
	jmp	.LBB3_9
.LBB3_2:
	movq	(%r15), %rax
	movl	8(%r15), %ecx
	movl	%ecx, 8(%rsi)
	movq	%rax, (%rsi)
	movb	1(%rdx), %al
	xorb	1(%rsi), %al
	cmpb	%dil, %al
	je	.LBB3_56
# BB#3:
	movb	$0, 1(%r15)
	jmp	.LBB3_56
.LBB3_46:
	movb	$4, (%r15)
	movw	$32767, 2(%r15)         # imm = 0x7FFF
	movb	$1, 1(%r15)
	movw	$16384, 4(%r15)         # imm = 0x4000
	movw	$0, 10(%r15)
	movl	$0, 6(%r15)
	jmp	.LBB3_56
.LBB3_49:
	movzwl	4(%rdx), %eax
	cmpw	%ax, 4(%rsi)
	ja	.LBB3_48
# BB#50:
	jae	.LBB3_47
.LBB3_55:
	movq	(%r15), %rax
	movl	8(%r15), %ecx
	movl	%ecx, 8(%rdx)
	movq	%rax, (%rdx)
.LBB3_56:                               # %normalize.exit
	movb	(%r15), %al
	decb	%al
	cmpb	$1, %al
	ja	.LBB3_65
# BB#57:
	movzwl	6(%r15), %eax
	orw	4(%r15), %ax
	orw	8(%r15), %ax
	orw	10(%r15), %ax
	jne	.LBB3_59
# BB#58:
	movl	$.Lstr, %edi
	callq	puts
.LBB3_59:
	movzwl	2(%r15), %eax
	cmpl	$32768, %eax            # imm = 0x8000
	jne	.LBB3_62
# BB#60:
	movw	$-32767, 2(%r15)        # imm = 0x8001
	cmpb	$0, (%r15)
	je	.LBB3_65
# BB#61:                                # %denormalize.exit.thread6.i
	movzwl	4(%r15), %ecx
	movzwl	6(%r15), %edx
	movzwl	8(%r15), %esi
	movzwl	10(%r15), %eax
	movl	%ecx, %edi
	shrl	%edi
	shldw	$15, %dx, %cx
	shldw	$15, %si, %dx
	movl	%eax, %ebp
	andl	$1, %ebp
	shrdw	$1, %si, %ax
	orl	%ebp, %eax
	movw	%di, 4(%r15)
	movw	%cx, 6(%r15)
	movw	%dx, 8(%r15)
	movw	%ax, 10(%r15)
	jmp	.LBB3_64
.LBB3_62:                               # %denormalize.exit.i
	cmpb	$0, (%r15)
	je	.LBB3_65
# BB#63:                                # %denormalize.exit.i._crit_edge
	movw	10(%r15), %ax
.LBB3_64:
	andl	$65528, %eax            # imm = 0xFFF8
	movw	%ax, 10(%r15)
.LBB3_65:                               # %RoundInternalFPF.exit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_22:
	movb	24(%rsp), %r8b
	testb	%r8b, %r8b
	je	.LBB3_32
# BB#23:
	cmpl	$-64, %ebx
	jg	.LBB3_25
# BB#24:                                # %.preheader.preheader.i79
	leaq	28(%rsp), %rax
	movw	$0, 4(%rax)
	movl	$0, (%rax)
	movw	$1, 34(%rsp)
	jmp	.LBB3_32
.LBB3_14:                               # %.lr.ph.i
	movw	12(%rsp), %ax
	movw	14(%rsp), %cx
	movw	16(%rsp), %si
	movw	18(%rsp), %r12w
	leal	-1(%r11), %r9d
	testb	$1, %bl
	jne	.LBB3_16
# BB#15:
	xorl	%edx, %edx
                                        # implicit-def: %R13W
                                        # implicit-def: %R8W
                                        # implicit-def: %BX
	cmpl	%r10d, %r9d
	jne	.LBB3_18
	jmp	.LBB3_20
.LBB3_25:                               # %.lr.ph.i85
	movw	28(%rsp), %dx
	movw	30(%rsp), %ax
	movw	32(%rsp), %cx
	movw	34(%rsp), %r13w
	leal	-1(%r10), %esi
	testb	$1, %bl
	jne	.LBB3_27
# BB#26:
	xorl	%ebp, %ebp
                                        # implicit-def: %R12W
                                        # implicit-def: %R14W
                                        # implicit-def: %BX
	cmpl	%r11d, %esi
	jne	.LBB3_29
	jmp	.LBB3_31
.LBB3_47:
	movzwl	6(%rdx), %eax
	cmpw	%ax, 6(%rsi)
	ja	.LBB3_48
# BB#51:
	jb	.LBB3_55
# BB#52:
	movzwl	8(%rdx), %eax
	cmpw	%ax, 8(%rsi)
	ja	.LBB3_48
# BB#53:
	jb	.LBB3_55
# BB#54:
	movzwl	10(%rdx), %eax
	cmpw	%ax, 10(%rsi)
	ja	.LBB3_48
	jmp	.LBB3_55
.LBB3_16:
	movl	%eax, %r13d
	andl	$65534, %r13d           # imm = 0xFFFE
	shrl	%r13d
	shldw	$15, %cx, %ax
	shldw	$15, %si, %cx
	movl	%r12d, %edx
	andl	$1, %edx
	andl	$65534, %r12d           # imm = 0xFFFE
	shrl	%r12d
	shll	$15, %esi
	orl	%edx, %r12d
	orl	%esi, %r12d
	movl	$1, %edx
	movl	%eax, %r8d
	movl	%ecx, %ebx
	movw	%cx, %si
	movw	%ax, %cx
	movw	%r13w, %ax
	cmpl	%r10d, %r9d
	je	.LBB3_20
.LBB3_18:                               # %.lr.ph.i.new
	subl	%edx, %r11d
	subl	%r10d, %r11d
	.p2align	4, 0x90
.LBB3_19:                               # =>This Inner Loop Header: Depth=1
	movl	%eax, %r8d
	andl	$-2, %r8d
	shll	$14, %r8d
	movl	%eax, %r13d
	andl	$65532, %r13d           # imm = 0xFFFC
	movl	%ecx, %edx
	andl	$65534, %edx            # imm = 0xFFFE
	shrl	%edx
	shll	$15, %eax
	orl	%edx, %eax
	andl	$65534, %eax            # imm = 0xFFFE
	movl	%esi, %ebp
	andl	$65534, %ebp            # imm = 0xFFFE
	shrl	%ebp
	shll	$15, %ecx
	orl	%ebp, %ecx
	movl	%r12d, %ebx
	andl	$65534, %ebx            # imm = 0xFFFE
	shrl	%ebx
	shll	$15, %esi
	orl	%ebx, %r12d
	orl	%ebx, %esi
	andl	$65534, %esi            # imm = 0xFFFE
	shrl	$2, %r13d
	shrl	%eax
	orl	%r8d, %eax
	movl	%eax, %r8d
	movl	%ecx, %ebx
	shrdw	$1, %dx, %bx
	andl	$1, %r12d
	shrl	%esi
	shll	$15, %ebp
	orl	%r12d, %esi
	orl	%ebp, %esi
	movw	%si, %r12w
	addl	$-2, %r11d
	movw	%bx, %si
	movw	%r8w, %cx
	movw	%r13w, %ax
	jne	.LBB3_19
.LBB3_20:                               # %..loopexit_crit_edge.i
	movw	%r13w, 12(%rsp)
	movw	%r8w, 14(%rsp)
	movw	%bx, 16(%rsp)
	movw	%r12w, 18(%rsp)
.LBB3_21:                               # %StickyShiftRightMant.exit
	movb	24(%rsp), %r8b
	movb	%r8b, (%r15)
.LBB3_9:
	movb	25(%rsp), %al
	movl	%eax, %ecx
	jmp	.LBB3_33
.LBB3_27:
	movl	%edx, %r12d
	andl	$65534, %r12d           # imm = 0xFFFE
	shrl	%r12d
	shldw	$15, %ax, %dx
	shldw	$15, %cx, %ax
	movl	%r13d, %ebx
	andl	$1, %ebx
	andl	$65534, %r13d           # imm = 0xFFFE
	shrl	%r13d
	shll	$15, %ecx
	orl	%ebx, %r13d
	orl	%ecx, %r13d
	movl	$1, %ebp
	movl	%edx, %r14d
	movl	%eax, %ebx
	movw	%ax, %cx
	movw	%dx, %ax
	movw	%r12w, %dx
	cmpl	%r11d, %esi
	je	.LBB3_31
.LBB3_29:                               # %.lr.ph.i85.new
	subl	%ebp, %r10d
	subl	%r11d, %r10d
.LBB3_30:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %r11d
	andl	$-2, %r11d
	shll	$14, %r11d
	movl	%edx, %r12d
	andl	$65532, %r12d           # imm = 0xFFFC
	movl	%eax, %esi
	andl	$65534, %esi            # imm = 0xFFFE
	shrl	%esi
	shll	$15, %edx
	orl	%esi, %edx
	andl	$65534, %edx            # imm = 0xFFFE
	movl	%ecx, %ebp
	andl	$65534, %ebp            # imm = 0xFFFE
	shrl	%ebp
	shll	$15, %eax
	orl	%ebp, %eax
	movl	%r13d, %ebx
	andl	$65534, %ebx            # imm = 0xFFFE
	shrl	%ebx
	shll	$15, %ecx
	orl	%ebx, %r13d
	orl	%ebx, %ecx
	andl	$65534, %ecx            # imm = 0xFFFE
	shrl	$2, %r12d
	shrl	%edx
	orl	%r11d, %edx
	movl	%edx, %r14d
	movl	%eax, %ebx
	shrdw	$1, %si, %bx
	andl	$1, %r13d
	shrl	%ecx
	shll	$15, %ebp
	orl	%r13d, %ecx
	orl	%ebp, %ecx
	movw	%cx, %r13w
	addl	$-2, %r10d
	movw	%bx, %cx
	movw	%r14w, %ax
	movw	%r12w, %dx
	jne	.LBB3_30
.LBB3_31:                               # %..loopexit_crit_edge.i88
	movw	%r12w, 28(%rsp)
	movw	%r14w, 30(%rsp)
	movw	%bx, 32(%rsp)
	movw	%r13w, 34(%rsp)
.LBB3_32:                               # %StickyShiftRightMant.exit89
	movb	8(%rsp), %al
	movb	%al, (%r15)
	movb	9(%rsp), %cl
	xorb	%dil, %cl
	movb	25(%rsp), %al
	movw	%r9w, %r14w
.LBB3_33:
	movb	%cl, 1(%r15)
	movw	%r14w, 2(%r15)
	movb	9(%rsp), %r9b
	xorb	%r9b, %al
	cmpb	%dil, %al
	movzwl	34(%rsp), %edx
	movzwl	18(%rsp), %eax
	jne	.LBB3_34
# BB#43:                                # %.preheader.preheader
	addl	%eax, %edx
	movl	%edx, %ecx
	shrl	$16, %ecx
	movw	%dx, 10(%r15)
	movzwl	32(%rsp), %eax
	movzwl	16(%rsp), %esi
	addl	%ecx, %eax
	addl	%esi, %eax
	movl	%eax, %esi
	shrl	$16, %esi
	andl	$1, %esi
	movw	%ax, 8(%r15)
	movzwl	30(%rsp), %ecx
	movzwl	14(%rsp), %edi
	addl	%esi, %ecx
	addl	%edi, %ecx
	movl	%ecx, %edi
	shrl	$16, %edi
	andl	$1, %edi
	movw	%cx, 6(%r15)
	movzwl	28(%rsp), %esi
	movzwl	12(%rsp), %ebp
	addl	%edi, %esi
	addl	%ebp, %esi
	movw	%si, 4(%r15)
	testl	$65536, %esi            # imm = 0x10000
	jne	.LBB3_42
# BB#44:
	testw	%si, %si
	jns	.LBB3_56
# BB#45:
	movb	$2, (%r15)
	jmp	.LBB3_56
.LBB3_34:                               # %.preheader107.preheader
	subl	%eax, %edx
	movl	%edx, %eax
	shrl	$16, %eax
	andl	$1, %eax
	movw	%dx, 10(%r15)
	movzwl	32(%rsp), %ecx
	movzwl	16(%rsp), %esi
	subl	%esi, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	shrl	$16, %eax
	andl	$1, %eax
	movw	%cx, 8(%r15)
	movzwl	30(%rsp), %r10d
	movzwl	14(%rsp), %ebp
	subl	%ebp, %r10d
	subl	%eax, %r10d
	movl	%r10d, %ebp
	shrl	$16, %ebp
	andl	$1, %ebp
	movw	%r10w, 6(%r15)
	movzwl	28(%rsp), %eax
	movzwl	12(%rsp), %ebx
	subl	%ebx, %eax
	subl	%ebp, %eax
	movw	%ax, 4(%r15)
	testl	$65536, %eax            # imm = 0x10000
	je	.LBB3_36
# BB#35:                                # %.loopexit.loopexit134
	xorb	%dil, %r9b
	movb	%r9b, 1(%r15)
	movzwl	%dx, %edx
	negl	%edx
	movl	%edx, %edi
	shrl	$16, %edi
	andl	$1, %edi
	movw	%dx, 10(%r15)
	movzwl	%cx, %ecx
	addl	%edi, %ecx
	negl	%ecx
	movl	%ecx, %edi
	shrl	$16, %edi
	andl	$1, %edi
	movw	%cx, 8(%r15)
	movzwl	%r10w, %r10d
	addl	%edi, %r10d
	negl	%r10d
	movl	%r10d, %edi
	shrl	$16, %edi
	andl	$1, %edi
	movw	%r10w, 6(%r15)
	addl	%edi, %eax
	negl	%eax
	movw	%ax, 4(%r15)
.LBB3_36:                               # %.loopexit
	movl	%ecx, %edi
	orl	%r10d, %edi
	orl	%eax, %edi
	orw	%dx, %di
	je	.LBB3_37
# BB#38:
	cmpb	$2, %r8b
	sete	%dil
	cmpb	$2, 8(%rsp)
	sete	%bl
	orb	%dil, %bl
	cmpb	$1, %bl
	jne	.LBB3_56
# BB#39:
	testw	%ax, %ax
	js	.LBB3_56
	.p2align	4, 0x90
.LBB3_40:                               # %.lr.ph.i91
                                        # =>This Inner Loop Header: Depth=1
                                        # kill: %DX<def> %DX<kill> %EDX<kill> %RDX<def>
	movl	%ecx, %edi
	movw	%dx, %cx
	leal	(%rdx,%rdx), %edx
	shrdw	$15, %di, %cx
	movl	%r10d, %esi
	andl	$32768, %esi            # imm = 0x8000
	shldw	$1, %di, %r10w
	addl	%eax, %eax
	movl	%eax, %edi
	shrl	$15, %esi
	decl	%r14d
	movw	%si, %ax
	orw	%di, %ax
	jns	.LBB3_40
# BB#41:                                # %._crit_edge.i
	movw	%dx, 10(%r15)
	movw	%cx, 8(%r15)
	movw	%r10w, 6(%r15)
	movw	%ax, 4(%r15)
	movw	%r14w, 2(%r15)
	jmp	.LBB3_56
.LBB3_42:
	incl	%r14d
	movw	%r14w, 2(%r15)
	movl	%esi, %edi
	andl	$65534, %edi            # imm = 0xFFFE
	shrl	%edi
	shldw	$15, %cx, %si
	movw	%si, 6(%r15)
	shldw	$15, %ax, %cx
	movw	%cx, 8(%r15)
	shrdw	$1, %ax, %dx
	movw	%dx, 10(%r15)
	orl	$32768, %edi            # imm = 0x8000
	movw	%di, 4(%r15)
	movb	$2, (%r15)
	jmp	.LBB3_56
.LBB3_37:
	movw	$0, (%r15)
	jmp	.LBB3_56
.Lfunc_end3:
	.size	AddSubInternalFPF, .Lfunc_end3-AddSubInternalFPF
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_2
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_55
	.quad	.LBB3_48
	.quad	.LBB3_5
	.quad	.LBB3_5
	.quad	.LBB3_4
	.quad	.LBB3_55
	.quad	.LBB3_48
	.quad	.LBB3_5
	.quad	.LBB3_5
	.quad	.LBB3_4
	.quad	.LBB3_55
	.quad	.LBB3_48
	.quad	.LBB3_48
	.quad	.LBB3_48
	.quad	.LBB3_46
	.quad	.LBB3_55
	.quad	.LBB3_48
	.quad	.LBB3_48
	.quad	.LBB3_48
	.quad	.LBB3_48
	.quad	.LBB3_49

	.text
	.globl	RoundInternalFPF
	.p2align	4, 0x90
	.type	RoundInternalFPF,@function
RoundInternalFPF:                       # @RoundInternalFPF
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 16
.Lcfi53:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	(%rbx), %al
	decb	%al
	cmpb	$1, %al
	ja	.LBB4_8
# BB#1:
	movzwl	6(%rbx), %eax
	orw	4(%rbx), %ax
	orw	8(%rbx), %ax
	orw	10(%rbx), %ax
	jne	.LBB4_3
# BB#2:
	movl	$.Lstr, %edi
	callq	puts
.LBB4_3:
	movzwl	2(%rbx), %eax
	cmpl	$32768, %eax            # imm = 0x8000
	jne	.LBB4_6
# BB#4:
	movw	$-32767, 2(%rbx)        # imm = 0x8001
	cmpb	$0, (%rbx)
	je	.LBB4_8
# BB#5:                                 # %denormalize.exit.thread6
	movzwl	4(%rbx), %eax
	movzwl	6(%rbx), %ecx
	movzwl	8(%rbx), %r8d
	movzwl	10(%rbx), %esi
	movl	%eax, %edi
	shrl	%edi
	shldw	$15, %cx, %ax
	shldw	$15, %r8w, %cx
	movl	%esi, %edx
	andl	$1, %edx
	shrdw	$1, %r8w, %si
	orl	%edx, %esi
	movw	%di, 4(%rbx)
	movw	%ax, 6(%rbx)
	movw	%cx, 8(%rbx)
	movw	%si, 10(%rbx)
	jmp	.LBB4_7
.LBB4_6:                                # %denormalize.exit
	cmpb	$0, (%rbx)
	je	.LBB4_8
.LBB4_7:
	andb	$-8, 10(%rbx)
.LBB4_8:                                # %denormalize.exit.thread
	popq	%rbx
	retq
.Lfunc_end4:
	.size	RoundInternalFPF, .Lfunc_end4-RoundInternalFPF
	.cfi_endproc

	.type	DoEmFloatIteration.jtable,@object # @DoEmFloatIteration.jtable
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
DoEmFloatIteration.jtable:
	.ascii	"\000\000\000\000\001\001\001\001\002\002\002\002\002\003\003\003"
	.size	DoEmFloatIteration.jtable, 16

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Error:  zero significand in denormalize"
	.size	.Lstr, 40


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
