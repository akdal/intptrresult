	.text
	.file	"btGeneric6DofSpringConstraint.bc"
	.globl	_ZN29btGeneric6DofSpringConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	.p2align	4, 0x90
	.type	_ZN29btGeneric6DofSpringConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b,@function
_ZN29btGeneric6DofSpringConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b: # @_ZN29btGeneric6DofSpringConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movzbl	%r9b, %r9d
	callq	_ZN23btGeneric6DofConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	movq	$_ZTV29btGeneric6DofSpringConstraint+16, (%rbx)
	movb	$0, 1266(%rbx)
	movl	$1065353216, 1320(%rbx) # imm = 0x3F800000
	movb	$0, 1267(%rbx)
	movl	$1065353216, 1324(%rbx) # imm = 0x3F800000
	movb	$0, 1268(%rbx)
	movl	$1065353216, 1328(%rbx) # imm = 0x3F800000
	movb	$0, 1269(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 1272(%rbx)
	movups	%xmm0, 1296(%rbx)
	movl	$1065353216, 1332(%rbx) # imm = 0x3F800000
	movb	$0, 1270(%rbx)
	movl	$0, 1288(%rbx)
	movl	$0, 1312(%rbx)
	movl	$1065353216, 1336(%rbx) # imm = 0x3F800000
	movb	$0, 1271(%rbx)
	movl	$0, 1292(%rbx)
	movl	$0, 1316(%rbx)
	movl	$1065353216, 1340(%rbx) # imm = 0x3F800000
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN29btGeneric6DofSpringConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b, .Lfunc_end0-_ZN29btGeneric6DofSpringConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	.cfi_endproc

	.globl	_ZN29btGeneric6DofSpringConstraint12enableSpringEib
	.p2align	4, 0x90
	.type	_ZN29btGeneric6DofSpringConstraint12enableSpringEib,@function
_ZN29btGeneric6DofSpringConstraint12enableSpringEib: # @_ZN29btGeneric6DofSpringConstraint12enableSpringEib
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	movb	%dl, 1266(%rdi,%rax)
	cmpl	$2, %eax
	jg	.LBB1_2
# BB#1:
	movb	%dl, 788(%rdi,%rax)
	retq
.LBB1_2:
	addl	$-3, %esi
	movslq	%esi, %rax
	imulq	$56, %rax, %rax
	movb	%dl, 904(%rdi,%rax)
	retq
.Lfunc_end1:
	.size	_ZN29btGeneric6DofSpringConstraint12enableSpringEib, .Lfunc_end1-_ZN29btGeneric6DofSpringConstraint12enableSpringEib
	.cfi_endproc

	.globl	_ZN29btGeneric6DofSpringConstraint12setStiffnessEif
	.p2align	4, 0x90
	.type	_ZN29btGeneric6DofSpringConstraint12setStiffnessEif,@function
_ZN29btGeneric6DofSpringConstraint12setStiffnessEif: # @_ZN29btGeneric6DofSpringConstraint12setStiffnessEif
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	movss	%xmm0, 1296(%rdi,%rax,4)
	retq
.Lfunc_end2:
	.size	_ZN29btGeneric6DofSpringConstraint12setStiffnessEif, .Lfunc_end2-_ZN29btGeneric6DofSpringConstraint12setStiffnessEif
	.cfi_endproc

	.globl	_ZN29btGeneric6DofSpringConstraint10setDampingEif
	.p2align	4, 0x90
	.type	_ZN29btGeneric6DofSpringConstraint10setDampingEif,@function
_ZN29btGeneric6DofSpringConstraint10setDampingEif: # @_ZN29btGeneric6DofSpringConstraint10setDampingEif
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	movss	%xmm0, 1320(%rdi,%rax,4)
	retq
.Lfunc_end3:
	.size	_ZN29btGeneric6DofSpringConstraint10setDampingEif, .Lfunc_end3-_ZN29btGeneric6DofSpringConstraint10setDampingEif
	.cfi_endproc

	.globl	_ZN29btGeneric6DofSpringConstraint19setEquilibriumPointEv
	.p2align	4, 0x90
	.type	_ZN29btGeneric6DofSpringConstraint19setEquilibriumPointEv,@function
_ZN29btGeneric6DofSpringConstraint19setEquilibriumPointEv: # @_ZN29btGeneric6DofSpringConstraint19setEquilibriumPointEv
	.cfi_startproc
# BB#0:                                 # %.preheader.preheader16
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN23btGeneric6DofConstraint19calculateTransformsEv
	movl	1232(%rbx), %eax
	movl	%eax, 1272(%rbx)
	movl	1236(%rbx), %eax
	movl	%eax, 1276(%rbx)
	movl	1240(%rbx), %eax
	movl	%eax, 1280(%rbx)
	movl	1168(%rbx), %eax
	movl	%eax, 1284(%rbx)
	movl	1172(%rbx), %eax
	movl	%eax, 1288(%rbx)
	movl	1176(%rbx), %eax
	movl	%eax, 1292(%rbx)
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZN29btGeneric6DofSpringConstraint19setEquilibriumPointEv, .Lfunc_end4-_ZN29btGeneric6DofSpringConstraint19setEquilibriumPointEv
	.cfi_endproc

	.globl	_ZN29btGeneric6DofSpringConstraint19setEquilibriumPointEi
	.p2align	4, 0x90
	.type	_ZN29btGeneric6DofSpringConstraint19setEquilibriumPointEi,@function
_ZN29btGeneric6DofSpringConstraint19setEquilibriumPointEi: # @_ZN29btGeneric6DofSpringConstraint19setEquilibriumPointEi
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 32
.Lcfi7:
	.cfi_offset %rbx, -24
.Lcfi8:
	.cfi_offset %r14, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	callq	_ZN23btGeneric6DofConstraint19calculateTransformsEv
	leal	3(%rbx), %eax
	cmpl	$3, %ebx
	movslq	%ebx, %rcx
	leaq	1168(%r14,%rcx,4), %rdx
	leaq	1232(%r14,%rcx,4), %rsi
	cmovgel	%eax, %ecx
	cmovgeq	%rdx, %rsi
	movl	(%rsi), %eax
	movslq	%ecx, %rcx
	movl	%eax, 1272(%r14,%rcx,4)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	_ZN29btGeneric6DofSpringConstraint19setEquilibriumPointEi, .Lfunc_end5-_ZN29btGeneric6DofSpringConstraint19setEquilibriumPointEi
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI6_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN29btGeneric6DofSpringConstraint21internalUpdateSpringsEPN17btTypedConstraint17btConstraintInfo2E
	.p2align	4, 0x90
	.type	_ZN29btGeneric6DofSpringConstraint21internalUpdateSpringsEPN17btTypedConstraint17btConstraintInfo2E,@function
_ZN29btGeneric6DofSpringConstraint21internalUpdateSpringsEPN17btTypedConstraint17btConstraintInfo2E: # @_ZN29btGeneric6DofSpringConstraint21internalUpdateSpringsEPN17btTypedConstraint17btConstraintInfo2E
	.cfi_startproc
# BB#0:
	cmpb	$0, 1266(%rdi)
	je	.LBB6_2
# BB#1:
	movss	1232(%rdi), %xmm0       # xmm0 = mem[0],zero,zero,zero
	subss	1272(%rdi), %xmm0
	mulss	1296(%rdi), %xmm0
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	1320(%rdi), %xmm1
	cvtsi2ssl	88(%rsi), %xmm2
	divss	%xmm2, %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, 792(%rdi)
	andps	.LCPI6_0(%rip), %xmm0
	divss	(%rsi), %xmm0
	movss	%xmm0, 808(%rdi)
.LBB6_2:
	cmpb	$0, 1267(%rdi)
	je	.LBB6_4
# BB#3:
	movss	1236(%rdi), %xmm0       # xmm0 = mem[0],zero,zero,zero
	subss	1276(%rdi), %xmm0
	mulss	1300(%rdi), %xmm0
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	1324(%rdi), %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2ssl	88(%rsi), %xmm2
	divss	%xmm2, %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, 796(%rdi)
	andps	.LCPI6_0(%rip), %xmm0
	divss	(%rsi), %xmm0
	movss	%xmm0, 812(%rdi)
.LBB6_4:
	cmpb	$0, 1268(%rdi)
	je	.LBB6_6
# BB#5:
	movss	1240(%rdi), %xmm0       # xmm0 = mem[0],zero,zero,zero
	subss	1280(%rdi), %xmm0
	mulss	1304(%rdi), %xmm0
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	1328(%rdi), %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2ssl	88(%rsi), %xmm2
	divss	%xmm2, %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, 800(%rdi)
	andps	.LCPI6_0(%rip), %xmm0
	divss	(%rsi), %xmm0
	movss	%xmm0, 816(%rdi)
.LBB6_6:                                # %.preheader55
	cmpb	$0, 1269(%rdi)
	je	.LBB6_8
# BB#7:
	movss	1168(%rdi), %xmm0       # xmm0 = mem[0],zero,zero,zero
	subss	1284(%rdi), %xmm0
	mulss	1308(%rdi), %xmm0
	movaps	.LCPI6_1(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	1332(%rdi), %xmm3       # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	cvtsi2ssl	88(%rsi), %xmm4
	divss	%xmm4, %xmm3
	mulss	%xmm1, %xmm3
	movss	%xmm3, 876(%rdi)
	andps	.LCPI6_0(%rip), %xmm0
	divss	%xmm2, %xmm0
	movss	%xmm0, 880(%rdi)
.LBB6_8:
	cmpb	$0, 1270(%rdi)
	je	.LBB6_10
# BB#9:
	movss	1172(%rdi), %xmm0       # xmm0 = mem[0],zero,zero,zero
	subss	1288(%rdi), %xmm0
	mulss	1312(%rdi), %xmm0
	movaps	.LCPI6_1(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	1336(%rdi), %xmm3       # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	xorps	%xmm4, %xmm4
	cvtsi2ssl	88(%rsi), %xmm4
	divss	%xmm4, %xmm3
	mulss	%xmm1, %xmm3
	movss	%xmm3, 932(%rdi)
	andps	.LCPI6_0(%rip), %xmm0
	divss	%xmm2, %xmm0
	movss	%xmm0, 936(%rdi)
.LBB6_10:
	cmpb	$0, 1271(%rdi)
	je	.LBB6_12
# BB#11:
	movss	1176(%rdi), %xmm0       # xmm0 = mem[0],zero,zero,zero
	subss	1292(%rdi), %xmm0
	mulss	1316(%rdi), %xmm0
	movaps	.LCPI6_1(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	1340(%rdi), %xmm3       # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	xorps	%xmm4, %xmm4
	cvtsi2ssl	88(%rsi), %xmm4
	divss	%xmm4, %xmm3
	mulss	%xmm1, %xmm3
	movss	%xmm3, 988(%rdi)
	andps	.LCPI6_0(%rip), %xmm0
	divss	%xmm2, %xmm0
	movss	%xmm0, 992(%rdi)
.LBB6_12:
	retq
.Lfunc_end6:
	.size	_ZN29btGeneric6DofSpringConstraint21internalUpdateSpringsEPN17btTypedConstraint17btConstraintInfo2E, .Lfunc_end6-_ZN29btGeneric6DofSpringConstraint21internalUpdateSpringsEPN17btTypedConstraint17btConstraintInfo2E
	.cfi_endproc

	.globl	_ZN29btGeneric6DofSpringConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.p2align	4, 0x90
	.type	_ZN29btGeneric6DofSpringConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E,@function
_ZN29btGeneric6DofSpringConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E: # @_ZN29btGeneric6DofSpringConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -24
.Lcfi13:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZN29btGeneric6DofSpringConstraint21internalUpdateSpringsEPN17btTypedConstraint17btConstraintInfo2E
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN23btGeneric6DofConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E # TAILCALL
.Lfunc_end7:
	.size	_ZN29btGeneric6DofSpringConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E, .Lfunc_end7-_ZN29btGeneric6DofSpringConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.cfi_endproc

	.section	.text._ZN17btTypedConstraintD2Ev,"axG",@progbits,_ZN17btTypedConstraintD2Ev,comdat
	.weak	_ZN17btTypedConstraintD2Ev
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraintD2Ev,@function
_ZN17btTypedConstraintD2Ev:             # @_ZN17btTypedConstraintD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end8:
	.size	_ZN17btTypedConstraintD2Ev, .Lfunc_end8-_ZN17btTypedConstraintD2Ev
	.cfi_endproc

	.section	.text._ZN29btGeneric6DofSpringConstraintD0Ev,"axG",@progbits,_ZN29btGeneric6DofSpringConstraintD0Ev,comdat
	.weak	_ZN29btGeneric6DofSpringConstraintD0Ev
	.p2align	4, 0x90
	.type	_ZN29btGeneric6DofSpringConstraintD0Ev,@function
_ZN29btGeneric6DofSpringConstraintD0Ev: # @_ZN29btGeneric6DofSpringConstraintD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end9:
	.size	_ZN29btGeneric6DofSpringConstraintD0Ev, .Lfunc_end9-_ZN29btGeneric6DofSpringConstraintD0Ev
	.cfi_endproc

	.section	.text._ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,"axG",@progbits,_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,comdat
	.weak	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,@function
_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif: # @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end10:
	.size	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif, .Lfunc_end10-_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_endproc

	.type	_ZTV29btGeneric6DofSpringConstraint,@object # @_ZTV29btGeneric6DofSpringConstraint
	.section	.rodata,"a",@progbits
	.globl	_ZTV29btGeneric6DofSpringConstraint
	.p2align	3
_ZTV29btGeneric6DofSpringConstraint:
	.quad	0
	.quad	_ZTI29btGeneric6DofSpringConstraint
	.quad	_ZN17btTypedConstraintD2Ev
	.quad	_ZN29btGeneric6DofSpringConstraintD0Ev
	.quad	_ZN23btGeneric6DofConstraint13buildJacobianEv
	.quad	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.quad	_ZN23btGeneric6DofConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.quad	_ZN29btGeneric6DofSpringConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.quad	_ZN23btGeneric6DofConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.quad	_ZN23btGeneric6DofConstraint13calcAnchorPosEv
	.size	_ZTV29btGeneric6DofSpringConstraint, 80

	.type	_ZTS29btGeneric6DofSpringConstraint,@object # @_ZTS29btGeneric6DofSpringConstraint
	.globl	_ZTS29btGeneric6DofSpringConstraint
	.p2align	4
_ZTS29btGeneric6DofSpringConstraint:
	.asciz	"29btGeneric6DofSpringConstraint"
	.size	_ZTS29btGeneric6DofSpringConstraint, 32

	.type	_ZTI29btGeneric6DofSpringConstraint,@object # @_ZTI29btGeneric6DofSpringConstraint
	.globl	_ZTI29btGeneric6DofSpringConstraint
	.p2align	4
_ZTI29btGeneric6DofSpringConstraint:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS29btGeneric6DofSpringConstraint
	.quad	_ZTI23btGeneric6DofConstraint
	.size	_ZTI29btGeneric6DofSpringConstraint, 24


	.globl	_ZN29btGeneric6DofSpringConstraintC1ER11btRigidBodyS1_RK11btTransformS4_b
	.type	_ZN29btGeneric6DofSpringConstraintC1ER11btRigidBodyS1_RK11btTransformS4_b,@function
_ZN29btGeneric6DofSpringConstraintC1ER11btRigidBodyS1_RK11btTransformS4_b = _ZN29btGeneric6DofSpringConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
